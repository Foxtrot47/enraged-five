// 
// parser/psorscparserbuilder.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSORSCPARSERBUILDER_H
#define PARSER_PSORSCPARSERBUILDER_H

#include "manager.h"

namespace rage {

class parStructure;
class psoRscBuilder;
class psoRscBuilderInstance;
class psoRscBuilderStructSchema;

// PURPOSE: Saves an object as a PSO file
// PARAMS:
//		filename - name of the file to create
//		structure - The parStructure that describes obj
//		obj - The object to save out
#if __RESOURCECOMPILER
bool psoRscSaveFromStructure(const char* filename, const parStructure& structure, parConstPtrToStructure obj, bool includeChecksum = false);
#endif

// PURPOSE: Builds a PSO schema object based on a parStructure
psoRscBuilderStructSchema&  psoBuildSchemaFromStructure(psoRscBuilder& builder, parStructure& str);

// PURPOSE: Adds an instance of a parsable class to the PSO file
template<typename _Type>
psoRscBuilderInstance* psoAddParsableObject(psoRscBuilder& builder, const _Type* obj);

psoRscBuilderInstance* psoAddParsableObjectFromStructure(psoRscBuilder& builder, const parStructure& structure, parConstPtrToStructure obj);

template<typename _Type>
inline psoRscBuilderInstance* psoAddParsableObject(psoRscBuilder& builder, const _Type* obj)
{
	const parStructure* str = PARSER.GetStructure(obj);
	FastAssert(str);
	return psoAddParsableObjectFromStructure(builder, *str, obj->parser_GetPointer());
}


} // namespace rage

#endif // PARSER_PSORSCPARSERBUILDER_H

