// 
// parser/codegenutils.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_CODEGENUTILS_H 
#define PARSER_CODEGENUTILS_H 

#include "delegateholder.h"
#include "macros.h"
#include "system/new.h"

namespace rage {

// This is just for parCodeGen use
// DOM-IGNORE-BEGIN

class datResource;
struct parEnumData;
class parStructure;
class parAttributeList;
struct parStructureStaticData;
template<typename T> class atDelegate;

// This is now an empty function to save on code space
__forceinline void parCguWarnOnMultipleRegistration(parStructureStaticData&) {}

void parCguRegisterStructure(parStructure* structure);
void parCguRegisterEnum(parEnumData* enumData);

void parCguSignalOutOfOrderRegistration(const char* structureName, const char* requiredBy);

template<typename _Type>
parStructure* parCguGetRequiredStructure(const char* DEV_ONLY(structureName), const char* DEV_ONLY(requiredBy))
{
	if (!_Type::parser_GetStaticStructure())
	{
#if __DEV
		parCguSignalOutOfOrderRegistration(structureName, requiredBy);
#endif
		_Type::parser_Register();
	}
	return _Type::parser_GetStaticStructure();
}

// PURPOSE: A template function for creating GetStructureCBs for any type.
// PARAMS:
//		base - assumed to be an instance of the described class or any derived class.
template<typename _Type>
static parStructure* parCguGetStructureCB(parConstPtrToStructure structAddr) {
	return reinterpret_cast<const _Type*>(structAddr)->parser_GetStructure();
}


// PURPOSE: This is a function template that can be used to create destructor callbacks
// When called, the function will call the destructor on the argument type
template<class _Type>
void parCguDestructor(parPtrToStructure t)
{
	(void)t;
	reinterpret_cast<_Type*>(t)->~_Type();
}


template<class _Base, class _Type>
void parCguPlacementCtor(_Base obj)
{
	::new(obj) _Type();
}

template<class _Base, class _Type>
void parCguRscPlacementCtor(_Base obj, datResource& rsc)
{
	::new(obj) _Type(rsc);
}

namespace parCguStructure
{
	enum Flags
	{
		HAS_VIRTUALS,
		HAS_NAMES,
		ALWAYS_HAS_NAMES,
		IS_EXTERNAL,
	}; // This needs to match parstructure.h!

	typedef ::rage::atDelegate<void*()> FactoryDelegate;
	typedef ::rage::atDelegate<void(void*)> PlacementDelegate;
	typedef ::rage::atDelegate< ::rage::parStructure*(parConstPtrToStructure) > GetStructureDelegate;
	typedef ::rage::atDelegate< void(parPtrToStructure) > DestructorDelegate;

	typedef void* (*FactoryFunction)();
	typedef void (*PlacementFunction)(void*);
	typedef void (*DestructorFunction)(parPtrToStructure);
	typedef ::rage::parStructure*(*GetStructureFunction)(parConstPtrToStructure);

	parStructure* CreateStructure(size_t size);
	parStructure* CreateDiStructure(size_t size);

	parStructure*  SetFactories(parStructure*, FactoryFunction create, PlacementFunction place, DestructorFunction destroy);
	parStructure*  SetGetStructureCB(parStructure*, GetStructureFunction fn );

	parStructure*  SetBaseClass(parStructure*, parStructure* baseMetadata, int baseOffset);

	u32 GetMajorVersion(parStructure*);

	parStructure* BeginAddingInitialDelegates(parStructure*, int numToAdd);

	parStructure* AddDelegateHolder(parStructure* str, const char* name, parDelegateHolderBase* del);

	template<typename _Type>
	PPU_ONLY(__attribute__((noinline)))
	parStructure* AddDelegate(parStructure* str, const char* name, _Type del)
	{
		parDelegateHolder<_Type>* fh = rage_new parDelegateHolder<_Type>;
		fh->Delegate = del;
		return AddDelegateHolder(str, name, fh);
	}

	parStructure* EndAddingInitialDelegates(parStructure*);

	parStructure* BuildStructureFromStaticData(void* dummy, parStructureStaticData& staticDataRef, u32 majorVersion, u32 minorVersion);

	parStructure* CreateExtraAttributes(parStructure*);
	parStructure* AddExtraAttribute(parStructure*, const char* key, const char* value); // key and value must be static strings
	parStructure* SortExtraAttributes(parStructure*);

	parAttributeList** AddOneExtraAttribute(parAttributeList** attr, const char* key, const char* value);
	parAttributeList** AddOneExtraAttribute(parAttributeList** attr, const char* key, int value);

}


// PURPOSE: For internal use only.
// NOTES: AutoRegistrationNodes form a linked list of parser_Register() functions to
// call when initializing the parser
struct parCguAutoRegistrationNode
{
	friend class parManager;
	parCguAutoRegistrationNode(void(*regFn)())
		: m_RegistrationFunction(regFn)
		, m_Next(sm_Head)
	{
		sm_Head = this;
	}

protected:
	void(*m_RegistrationFunction)();
	parCguAutoRegistrationNode* m_Next;
	static parCguAutoRegistrationNode* sm_Head;
};

// DOM-IGNORE-END

} // namespace rage

#endif // PARSER_CODEGENUTILS_H 
