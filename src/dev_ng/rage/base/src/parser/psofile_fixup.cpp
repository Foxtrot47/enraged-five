// 
// parser/psofile_fixup.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psofile.h"

#include "manager.h"
#include "optimisations.h"
#include "psobuilder.h"
#include "psobyteswap.h"
#include "psofaketypes.h"
#include "psoparserbuilder.h"
#include "structure.h"

#include "diag/output.h"
#include "file/asset.h"
#include "file/stream.h"
#include "math/simplemath.h"
#include "parsercore/utils.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "vectormath/vec4v.h"

using namespace rage;

PARSER_OPTIMISATIONS();

#if PSO_USING_NATIVE_BIT_SIZE

namespace rage
{

class psoObjectFixup
{
public:
	static const psoObjectFixup* FindObjectFixup(parStructure& str);

	static size_t FindVptr(parStructure& str);
	
	void Init(parStructure& str);

	bool HasFixups() const {return m_CommandBuffer.GetCount() > 0; }

	void Apply(psoFile& file, parPtrToStructure baseAddress) const;

	typedef atDelegate<void (void*)> PostPsoPlaceDel;

protected:
	/* Don't need 32 bit vs. 64 bit handling here, since it's fixups we can always assume native sizes */
	enum OpCode
	{
		/* offset is always 24bit, other args are 32bit */
		SET_U32,				/* offset, new value */
		SET_U64,				/* offset, new value high word, new value low word */
		ID_TO_POINTER,			/* offset */
		FIXUP_ATARRAY,			/* offset */
		FIXUP_ATARRAY32,		/* offset */
		FIXUP_ATSTRING,			/* offset */
		FIXUP_ATWIDESTRING,		/* offset */
		STARTLOOP,				/* offset, loop count, stride */
		STARTLOOP_WITH_COUNT32,	/* offset, loop count offset, stride */ /* loop count offset is a _signed_ offset, relative to the start of the array (i.e. relative to the base address + offset) */
		ENDLOOP,				/* */
		CALL_FN					/* unused, parDelegateHolderBase high word, low word */ /* Calls the delegate, and passes in the 'this' base ptr */
	};

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4201)
#endif
	struct Item {
		union {
			struct {
				u32 m_OpCode: 8;
				u32 m_Offset: 24;
			};
			u32 m_Payload;
			s32 m_PayloadS32;
		};
	};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

	atArray<Item> m_CommandBuffer;

	void BuildFromStructure(parStructure& str, u32 offset);
	void BuildFromMember(parStructure& str, parMember& mem, u32 offset);

	inline void Push(OpCode opcode, u32 offset) 
	{
		Item& newItem = m_CommandBuffer.Grow();
		newItem.m_OpCode = opcode;
		newItem.m_Offset = offset;
	}

	inline void Push(u32 payload)
	{
		m_CommandBuffer.Grow().m_Payload = payload;
	}

	inline void Push(s32 payload)
	{
		m_CommandBuffer.Grow().m_PayloadS32 = payload;
	}

	struct StackFrame
	{
		u32 m_LoopCount;
		u32 m_Stride;
		parPtrToStructure m_OldAddress;
		const Item* m_OldItem;
	};

	template<typename psoFakePtrType>
	inline void Fixup(psoFile& file, psoFakePtrType& ptr) const
	{
		typedef typename psoFakePtrType::PtrType PtrType;
		if (Verifyf(ptr.m_StructId.IsNull() || ptr.m_StructId.GetTableIndex() < file.GetNumStructArrays(),"If this fails, the pointer was probably already fixed up")) // If this fails, the pointer was probably already fixed up
			ptr.m_Pointer = file.GetInstanceDataAs<PtrType>(ptr.m_StructId);
	}

#if 0
	void PrintCommandBuffer() const;
#endif
};

void psoObjectFixup::Apply(psoFile& file, parPtrToStructure baseAddress) const
{
	atFixedArray<StackFrame, 32> m_Stack;

	const Item* item = m_CommandBuffer.begin();

	while(item != m_CommandBuffer.end())
	{
		Item curr = *item;
		item++;
		parPtrToStructure currAddress = baseAddress + curr.m_Offset;
		switch((OpCode)curr.m_OpCode)
		{
		case SET_U32:
			{
				*reinterpret_cast<u32*>(currAddress) = item->m_Payload;
				item++;
			}
			break;
		case SET_U64:
			{
				u32 highWord = item->m_Payload;
				item++;
				u32 lowWord = item->m_Payload;
				item++;
				*reinterpret_cast<u64*>(currAddress) = ((u64)highWord << 32) | lowWord;
			}
			break;
		case ID_TO_POINTER:
			{
				psoFakePtr& ptr = *reinterpret_cast<psoFakePtr*>(currAddress);
				Fixup(file, ptr);
			}
			break;
		case FIXUP_ATARRAY:
			{
				psoFakeAtArray16& arr = *reinterpret_cast<psoFakeAtArray16*>(currAddress);
				Fixup(file, arr.m_Elements);
				arr.m_Capacity = arr.m_Count;
			}
			break;
		case FIXUP_ATARRAY32:
			{
				psoFakeAtArray32& arr = *reinterpret_cast<psoFakeAtArray32*>(currAddress);
				Fixup(file, arr.m_Elements);
				arr.m_Capacity = arr.m_Count;
			}
			break;
		case FIXUP_ATSTRING:
			{
				psoFakeAtString& str = *reinterpret_cast<psoFakeAtString*>(currAddress);
				if (str.m_Data.m_StructId.IsNull())
				{
					str.m_Length = str.m_Allocated = 0;
				}
				else
				{
					Fixup(file, str.m_Data);
					u16 len = (u16)strlen(str.m_Data.m_Pointer);
					str.m_Length = len;
					str.m_Allocated = len + 1;
				}
			}
			break;
		case FIXUP_ATWIDESTRING:
			{
				psoFakeAtString& str = *reinterpret_cast<psoFakeAtString*>(currAddress);
				if (str.m_Data.m_StructId.IsNull())
				{
					str.m_Length = str.m_Allocated = 0;
				}
				else
				{
					Fixup(file, str.m_Data);
					u16 len = (u16)wcslen(reinterpret_cast<char16*>(str.m_Data.m_Pointer));
					str.m_Length = len;
					str.m_Allocated = len + 1;
				}
			}
			break;
		case STARTLOOP:
			{
				StackFrame& newFrame = m_Stack.Append();
				newFrame.m_OldAddress = baseAddress;
				baseAddress = currAddress;
				newFrame.m_LoopCount = item->m_Payload;
				item++;
				newFrame.m_Stride = item->m_Payload;
				item++;
				newFrame.m_OldItem = item;
			}
			break;
		case STARTLOOP_WITH_COUNT32:
			{
				StackFrame& newFrame = m_Stack.Append();
				newFrame.m_OldAddress = baseAddress;
				baseAddress = currAddress;
				int* loopCountAddr = reinterpret_cast<int*>(currAddress + item->m_PayloadS32);
				newFrame.m_LoopCount = *loopCountAddr;
				item++;
				newFrame.m_Stride = item->m_Payload;
				item++;
				newFrame.m_OldItem = item;
			}
		case ENDLOOP:
			{
				StackFrame& frame = m_Stack.Top();
				if (--frame.m_LoopCount == 0)
				{
					baseAddress = frame.m_OldAddress;
					m_Stack.Pop();
				}
				else
				{
					baseAddress += frame.m_Stride;
					item = frame.m_OldItem;
				}
			}
			break;
		case CALL_FN:
			{

				u32 highWord = item->m_Payload;
				item++;
				u32 lowWord = item->m_Payload;
				item++;
				size_t delegateHolderAddr = (size_t)(((u64)highWord << 32) | lowWord);

				parDelegateHolder<PostPsoPlaceDel>* realDelHolder = reinterpret_cast<parDelegateHolder<PostPsoPlaceDel>* >(delegateHolderAddr);

				PostPsoPlaceDel del = realDelHolder->Delegate;
				del(currAddress);
			}
			break;
		}
	}
}

void psoObjectFixup::Init(parStructure& str)
{
	m_CommandBuffer.Reset();
	BuildFromStructure(str, 0);
}

void psoObjectFixup::BuildFromStructure(parStructure& str, u32 offset)
{
	if (str.GetFlags().IsSet(parStructure::HAS_VIRTUALS))
	{
		size_t vptr = FindVptr(str);
#if __64BIT
		Push(SET_U64, offset);
		Push((u32)(vptr >> 32));
		Push((u32)(vptr & 0xFFFFFFFFF));
#else
		Push(SET_U32, offset);
		Push(vptr);
#endif
	}

	parStructure::BaseList bases;
	bases.Init(&str, NULL);

	for(int baseIdx = bases.GetNumBases()-1; baseIdx >= 0; baseIdx --)
	{
		parStructure& baseStr = *bases.GetBaseStructure(baseIdx);
		for(int memIdx = 0; memIdx < baseStr.GetNumMembers(); memIdx++)
		{
			parMember& mem = *baseStr.GetMember(memIdx);
			BuildFromMember(baseStr, mem, offset);
		}
	}

	parDelegateHolderBase* delegateHolder = str.FindDelegate("PostPsoPlace");
	if (delegateHolder)
	{
		Push(CALL_FN, offset);
		u64 delegateAsU64 = static_cast<u64>(reinterpret_cast<size_t>(delegateHolder));
		u32 highWord = (u32)(delegateAsU64 >> 32);
		u32 lowWord = (u32)(delegateAsU64);
		Push(highWord);
		Push(lowWord);
	}

#if 0
	Displayf("Fixups for %s:", str.GetName());
	PrintCommandBuffer();
#endif
}

void psoObjectFixup::BuildFromMember(parStructure& str, parMember& mem, u32 offset)
{
	size_t memOffsetFull = offset + mem.GetOffset();
	FastAssert(memOffsetFull < (1 << 24)-1); // Make sure the offset fits in 24 bits
	u32 memOffset = (u32)memOffsetFull;

	switch(mem.GetType())
	{
	case parMemberType::TYPE_ARRAY:
		{
			parMemberArray& arrMem = mem.AsArrayRef();
			switch(arrMem.GetSubtype())
			{
			case parMemberArraySubType::SUBTYPE_ATARRAY:
				Push(FIXUP_ATARRAY, memOffset);
				break;
			case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
				Push(FIXUP_ATARRAY32, memOffset);
				break;
			case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
			case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
			case parMemberArraySubType::SUBTYPE_MEMBER:
				{
					if (arrMem.GetSubtype() == parMemberArraySubType::SUBTYPE_ATFIXEDARRAY)
					{
						Push(STARTLOOP_WITH_COUNT32, memOffset);
						int* absAddrOfLoopCountVar = arrMem.GetFixedArrayCountAddress(reinterpret_cast<parPtrToStructure>(NULL));
						ptrdiff_t relAddrOfLoopCountVar = reinterpret_cast<size_t>(absAddrOfLoopCountVar) - memOffset;
						FastAssert(relAddrOfLoopCountVar > INT_MIN && relAddrOfLoopCountVar < INT_MAX);
						Push((int)relAddrOfLoopCountVar);
					}
					else
					{
						Push(STARTLOOP, memOffset);
						Push(arrMem.GetData()->m_NumElements);
					}
					Push((u32)arrMem.GetPrototypeMember()->GetSize());
					int commandCount = m_CommandBuffer.GetCount();
					BuildFromMember(str, *arrMem.GetPrototypeMember(), memOffset);
					if (commandCount == m_CommandBuffer.GetCount())
					{
						m_CommandBuffer.Pop();
						m_CommandBuffer.Pop();
						m_CommandBuffer.Pop();
					}
					else
					{
						Push(ENDLOOP, 0);
					}
				}
				break;
			case parMemberArraySubType::SUBTYPE_POINTER:
			case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
			case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
			case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
				Push(ID_TO_POINTER, memOffset);
				break;
			default:
				break;
			}
		}
		break;
	case parMemberType::TYPE_BITSET:
		{
			parMemberBitset& bitMem = mem.AsBitsetRef();
			switch(bitMem.GetSubtype())
			{
			case parMemberBitsetSubType::SUBTYPE_ATBITSET:
				{
					psoFakeAtBitset* fakebitset = reinterpret_cast<psoFakeAtBitset*>(memOffset);
					Push(ID_TO_POINTER, (u32)reinterpret_cast<size_t>(&fakebitset->m_Bits));
				}
				break;
			default:
				break;
			}
		}
		break;
	case parMemberType::TYPE_MAP:
		{
			parMemberMap& mapMem = mem.AsMapRef();
			switch(mapMem.GetSubtype())
			{
			case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
				{
					psoFakeAtBinMap* fakemap = reinterpret_cast<psoFakeAtBinMap*>(memOffset);
					Push(FIXUP_ATARRAY, (u32)reinterpret_cast<size_t>(&fakemap->m_Data));
				}
				break;
			default:
				break;
			}
		}
		break;
	case parMemberType::TYPE_STRING:
		{
			parMemberString& strMem = mem.AsStringRef();
			switch(strMem.GetSubtype())
			{
			case parMemberStringSubType::SUBTYPE_ATSTRING:
				Push(FIXUP_ATSTRING, memOffset);
				break;
			case parMemberStringSubType::SUBTYPE_ATWIDESTRING:
				Push(FIXUP_ATWIDESTRING, memOffset);
				break;
			case parMemberStringSubType::SUBTYPE_CONST_STRING:
			case parMemberStringSubType::SUBTYPE_POINTER:
			case parMemberStringSubType::SUBTYPE_WIDE_POINTER:
				Push(ID_TO_POINTER, memOffset);
				break;
			default:
				break;
			}
		}
		break;
	case parMemberType::TYPE_STRUCT:
		{
			parMemberStruct& strMem = mem.AsStructOrPointerRef();
			switch(strMem.GetSubtype())
			{
			case parMemberStructSubType::SUBTYPE_POINTER:
			case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
				Push(ID_TO_POINTER, memOffset);
				break;
			case parMemberStructSubType::SUBTYPE_STRUCTURE:
				BuildFromStructure(*strMem.GetBaseStructure(), memOffset);
				break;
			default:
				break;
			}
		}
		break;
	default:
		// No fixup needed;
		break;
	}
}

#if 0
void psoObjectFixup::PrintCommandBuffer() const
{
	const Item* item = m_CommandBuffer.begin();
	while(item != m_CommandBuffer.end())
	{
		Item curr = *item;
		item++;
		switch((OpCode)curr.m_OpCode)
		{
		case SET_U32:
			Displayf("SET_U32 (+0x%x) 0x%08x", curr.m_Offset, item->m_Payload);
			item++;
			break;
		case SET_U64:
			{
				u32 highWord = item->m_Payload;
				item++;
				u32 lowWord = item->m_Payload;
				item++;
				Displayf("SET_U64 (+0x%x) 0x%08x%08x", curr.m_Offset, highWord, lowWord);
			}
			break;
		case ID_TO_POINTER:
			Displayf("ID_TO_POINTER (+0x%x)", curr.m_Offset);
			break;
		case FIXUP_ATARRAY:
			Displayf("FIXUP_ATARRAY (+0x%x)", curr.m_Offset);
			break;
		case FIXUP_ATARRAY32:
			Displayf("FIXUP_ATARRAY32 (+0x%x)", curr.m_Offset);
			break;
		case FIXUP_ATSTRING:
			Displayf("FIXUP_ATSTRING (+0x%x)", curr.m_Offset);
			break;
		case FIXUP_ATWIDESTRING:
			Displayf("FIXUP_ATWIDESTRING (+0x%x)", curr.m_Offset);
			break;
		case STARTLOOP:
			{
				u32 loopCount = item->m_Payload;
				item++;
				u32 stride = item->m_Payload;
				item++;
				Displayf("STARTLOOP (+0x%x) %dx + 0x%x", curr.m_Offset, loopCount, stride);
			}
			break;
		case STARTLOOP_WITH_COUNT32:
			{
				u32 loopCountOffset = item->m_Payload;
				item++;
				u32 stride = item->m_Payload;
				item++;
				Displayf("STARTLOOP_WITH_COUNT32 (+0x%x) (+0x%x)x + 0x%x", curr.m_Offset, loopCountOffset, stride);
			}
		case ENDLOOP:
			{
				Displayf("ENDLOOP");
			}
		}
	}
}	 
#endif

// Note! C++ voodoo goes here! 
// This function gets the virtual table pointer for a given parStructure. It does this by creating an instance of the
// structure, reading the first 4 or 8 bytes, then destroying that instance. To make sure we don't have to pay the cost of construction
// every time we cache the vptr value inside the parStructure's extra attribute list
size_t psoObjectFixup::FindVptr(parStructure& str)
{
	sysMemAutoUseTempMemory useTempHeap;

	if (str.GetFlags().IsClear(parStructure::HAS_VIRTUALS))
	{
		return 0;
	}

	parAttributeList*& attrs = str.GetExtraAttributes();
	if (attrs)
	{
		size_t vptrVal = (size_t)attrs->FindAttributeInt64Value("vptr", 0);
		if (vptrVal)
		{
			return vptrVal;
		}
	}
	else
	{
		attrs = rage_new parAttributeList;
	}

	// Create a new instance of the structure
	parPtrToStructure instance = str.AllocAndPlace();
	if (instance)
	{

		size_t* instanceAsSizets = reinterpret_cast<size_t*>(instance);
		size_t vptrVal = *instanceAsSizets; // get the first 4 or 8 bytes (the vptr) as an integer type
		attrs->AddAttribute("vptr", (s64)vptrVal, false);

		str.Destroy(instance);
		delete [] (char*) instance;

		return vptrVal;
	}

	return 0;
}

const psoObjectFixup* psoObjectFixup::FindObjectFixup(parStructure& str)
{
	sysMemAutoUseTempMemory useTempHeap;

	parAttributeList*& attrs = str.GetExtraAttributes();
	if (attrs)
	{
		parAttribute* fixupAttr = attrs->FindAttribute("psofixup");
		if (fixupAttr)
		{
			const char* fixupInstAsStr = fixupAttr->GetStringValue();
			return reinterpret_cast<const psoObjectFixup*>(fixupInstAsStr);			
		}
	}
	else
	{
		attrs = rage_new parAttributeList;
	}

	psoObjectFixup* newFixup = rage_new psoObjectFixup;
	newFixup->Init(str);
	if (!newFixup->HasFixups())
	{
		delete newFixup;
		newFixup = NULL;
	}
	attrs->AddAttribute("psofixup", reinterpret_cast<char*>(newFixup), false, false);
	return newFixup;
}

} // namespace rage

namespace psoFileReader
{

void ReadParMemberFromPso( parMember* destMemberDesc, parPtrToStructure destStructAddr, psoStruct& srcStructure, psoMember& srcMember );
void ReadParStructureFromPso(parStructure& destStructDesc, parPtrToStructure destStructAddr, psoStruct& srcStructure, bool verifyTypes);
void FixupPsoMembers(psoStruct& destStructure);
void FixupArrayWithExternalStorage( psoStruct &destStructure, psoMember &mem, psoMemberArrayInterface &memArr );
psoLoadInPlaceResult CopyAndFixupMatchingPsoContents(psoFile* file);
void FixupPsoMembersInPlace(psoStruct& str, psoStructureSchema& schema, size_t vptrVal);


// Use this so the compiler needs to reserve space for the array cookie
class EmptyClass16
{
public:
	~EmptyClass16() {}

	Vec4V v;
};
CompileTimeAssert(sizeof(EmptyClass16) == 16);


void FixupPsoMember(psoStruct& destStructure, psoMember& mem)
{
	psoMemberSchema memSchema = mem.GetSchema();
	psoFile& file = destStructure.GetFile();

	// All of the pointer types are doubled up here. Why? Because since we are doing fixups we know we are really only dealing with native types.
	// In other words we only go through the case psoType::TYPE_POINTER64 on a true 64-bit build, and psoFakePtr is 64-bits, and likewise we only
	// go through case psoType::TYPE_POINTER32 on a 32-bit build and psoFakePtr is 32-bits.
	FastAssert(memSchema.GetType().IsNative()); // Lets make sure just in case

	switch(memSchema.GetType().GetEnum())
	{
	case psoType::TYPE_STRING_POINTER32:
	case psoType::TYPE_STRING_POINTER64:
		{
			psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
			fakePtr.m_Pointer = StringDuplicate(file.GetInstanceDataAs<char*>(fakePtr.m_StructId));
		}
		break;
	case psoType::TYPE_WIDE_STRING_POINTER32:
	case psoType::TYPE_WIDE_STRING_POINTER64:
		{
			psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
			fakePtr.m_Pointer = WideStringDuplicate(file.GetInstanceDataAs<char16*>(fakePtr.m_StructId));
		}
		break;
	case psoType::TYPE_ATSTRING32:
	case psoType::TYPE_ATSTRING64:
		{
			psoFakeAtString& fakeStr = mem.GetDataAs<psoFakeAtString>();
			if (fakeStr.m_Data.m_StructId.IsNull())
			{
				fakeStr.m_Allocated = 0;
				fakeStr.m_Length = 0;
				fakeStr.m_Data.m_Pointer = NULL;
			}
			else
			{
				char* stringData = file.GetInstanceDataAs<char*>(fakeStr.m_Data.m_StructId);
				int length = (int)strlen(stringData);
				char* newData = rage_new char[length+1];
				sysMemCpy(newData, stringData, length+1);
				fakeStr.m_Allocated = (u16)(length+1);
				fakeStr.m_Length = (u16)length;
				fakeStr.m_Data.m_Pointer = newData;
			}
		}
		break;
	case psoType::TYPE_ATWIDESTRING32:
	case psoType::TYPE_ATWIDESTRING64:
		{
			psoFakeAtString& fakeStr = mem.GetDataAs<psoFakeAtString>();
			if (fakeStr.m_Data.m_StructId.IsNull())
			{
				fakeStr.m_Allocated = 0;
				fakeStr.m_Length = 0;
				fakeStr.m_Data.m_Pointer = NULL;
			}
			else
			{
				char16* stringData = file.GetInstanceDataAs<char16*>(fakeStr.m_Data.m_StructId);
				int length = (int)wcslen(stringData);
				char16* newData = rage_new char16[length+1];
				sysMemCpy(newData, stringData, (length+1)*sizeof(char16));
				fakeStr.m_Allocated = (u16)(length+1);
				fakeStr.m_Length = (u16)length;
				fakeStr.m_Data.m_Pointer = reinterpret_cast<char*>(newData);
			}
		}
		break;
	case psoType::TYPE_STRUCT:
		{
			psoStruct subStruct = mem.GetSubStructure();
			FixupPsoMembers(subStruct);
		}
		break;
	case psoType::TYPE_POINTER32:
	case psoType::TYPE_POINTER64:
		{
			psoStruct subStruct = file.GetInstance(mem.GetDataAs<psoFakePtr>().m_StructId);
			if (!subStruct.IsNull())
			{
				psoCreateAndLoadFromStructure(subStruct, NULL, mem.GetDataAs<parPtrToStructure>());
			}
		}
		break;
	case psoType::TYPE_ARRAY_MEMBER:
	case psoType::TYPE_ATFIXEDARRAY:
		{
			psoMemberArrayInterface memArr(destStructure, mem);
			psoMemberSchema eltSchema = memArr.GetArrayElementSchema();
			if (!eltSchema.GetType().IsPod())
			{
				size_t count = memArr.GetArrayCount();
				for(size_t i = 0; i < count; i++)
				{
					psoMember arrayElt = memArr.GetElement(i);
					FixupPsoMember(destStructure, arrayElt);
				}
			}
		}
		break;
	case psoType::TYPE_ATARRAY32:
	case psoType::TYPE_ATARRAY32_32BITIDX:
	case psoType::TYPE_ARRAY_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
	case psoType::TYPE_ATARRAY64:
	case psoType::TYPE_ATARRAY64_32BITIDX:
	case psoType::TYPE_ARRAY_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		{
			psoMemberArrayInterface memArr(destStructure, mem);
			FixupArrayWithExternalStorage(destStructure, mem, memArr);
		}
		break;
	case psoType::TYPE_ATBITSET32:
	case psoType::TYPE_ATBITSET64:
		{
			psoFakeAtBitset& fakeBits = mem.GetDataAs<psoFakeAtBitset>();
			u32* newData = rage_new u32[fakeBits.m_Size];
			sysMemCpy(newData, file.GetInstanceDataAs<u32*>(fakeBits.m_Bits.m_StructId), fakeBits.m_Size * sizeof(u32));
			fakeBits.m_Bits.m_Pointer = newData;
		}
		break;
	case psoType::TYPE_ATBINARYMAP32:
	case psoType::TYPE_ATBINARYMAP64:
		{
			parErrorf("Not implemented yet");
			// We need to allocate new space for the map contents, then fix up all the values, maybe.
		}
		break;

	default:
		parAssertf(memSchema.GetType().IsPod(), "Didn't do a fixup for type %d, but it's not POD", memSchema.GetType().GetRaw());
		break;
	}
}

void FixupArrayWithExternalStorage( psoStruct &destStructure, psoMember &mem, psoMemberArrayInterface &memArr ) 
{
	size_t count = memArr.GetArrayCount();
	psoFile& file = destStructure.GetFile();
	psoMemberSchema& memSchema = mem.GetSchema();
	psoMemberSchema eltSchema = memArr.GetArrayElementSchema();

	if (count > 0)
	{
		// If this is an array of structs, check to make sure the structs can use fast loading. If not,
		// do slow loading on them. Note that we can't do the normal slow loading here where we'd call
		// ReadArrayFromPso(), because that function expects a freshly constructed instance of the runtime structure,
		// in particular one with an initial array count of 0, instead of one that's been copied from the PSO
		// and has an existing count, and a structId in place of an array pointer. So we have to do a hybrid of in-place fixups
		// (for the array container object) and slow loading (for the array contents) below.
		if (eltSchema.GetType() == psoType::TYPE_STRUCT)
		{
			psoStructureSchema eltStructSchema = file.GetSchemaCatalog().FindStructureSchema(eltSchema.GetReferentHash());
			if (!eltStructSchema.CanUseFastLoading())
			{
				// Get a psoStruct that points to the first element of the source array. We're going to stomp on the 
				// array member, so using memArr won't work after that.
				psoStruct srcArrayData = memArr.GetElement(0).GetSubStructure(); 

				// NULL out the array pointer and the counts, so this looks like a newly constructed array
				// See above for why we can double up pointer types
				switch(memSchema.GetType().GetEnum())
				{
				case psoType::TYPE_ATARRAY32:
				case psoType::TYPE_ATARRAY64:
					{
						psoFakeAtArray16& fakeArray = mem.GetDataAs<psoFakeAtArray16>();
						fakeArray.m_Elements.m_Pointer = NULL;
						fakeArray.m_Count = fakeArray.m_Capacity = 0;
					}
					break;
				case psoType::TYPE_ATARRAY32_32BITIDX:
				case psoType::TYPE_ATARRAY64_32BITIDX:
					{
						psoFakeAtArray32& fakeArray = mem.GetDataAs<psoFakeAtArray32>();
						fakeArray.m_Elements.m_Pointer = NULL;
						fakeArray.m_Count = fakeArray.m_Capacity = 0;
					}
					break;
				case psoType::TYPE_ARRAY_POINTER32:
				case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
				case psoType::TYPE_ARRAY_POINTER64:
				case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
					{
						psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
						fakePtr.m_Pointer = NULL;
					}
					break;
				default:
					parErrorf("Shouldn't get here!");
				}

				// Start with the structure that contains the array, find the array member
				parStructure* destContainerStruct = PARSER.FindStructure(destStructure.GetSchema().GetNameHash());
				parAssertf(destContainerStruct, "Couldn't find parStructure for 0x%x, which is weird because we were using fast loading", destStructure.GetSchema().GetNameHash().GetHash());

				parMemberArray& destArrayMem = destContainerStruct->FindMember(memSchema.GetNameHash(), true)->AsArrayRef();
				parAssertf(destArrayMem.GetType() == parMemberType::TYPE_ARRAY, "Couldn't find parMember for struct 0x%x, member 0x%x, which is weird because we were using fast loading", destStructure.GetSchema().GetNameHash().GetHash(), memSchema.GetNameHash().GetHash());

				// Now find the parStructure that each array element will contain
				parMemberStruct& destArrayProtoMem = destArrayMem.GetPrototypeMember()->AsStructOrPointerRef();
				parAssertf(destArrayProtoMem.GetSubtype() == parMemberStructSubType::SUBTYPE_STRUCTURE, "Array isn't an array of structs, even though we're using fast path loading and PSO says it is!");

				parStructure* destStruct = destArrayProtoMem.GetBaseStructure();

				destArrayMem.SetArraySize(destStructure.GetInstanceData(), count);

				parPtrToArray destArrayContents;
				size_t destArrayCount;
				destArrayMem.GetArrayContentsAndCountFromStruct(destStructure.GetInstanceData(), destArrayContents, destArrayCount);

				parAssertf(count == destArrayCount, "Counts in PSO (%" SIZETFMT "d) and just-created storage (%" SIZETFMT "d) don't match", count, destArrayCount);

				size_t srcElementSize = srcArrayData.GetSchema().GetSize();

				for(size_t i = 0; i < count; i++)
				{
					// TODO: Could be sped up, GetAddressOfNthMember and GetPointerToStructure always read the stride / offset but its the same for all elements.
					parPtrToStructure arrayElt = destArrayMem.GetAddressOfNthElement(destArrayContents, i);
					psoLoadFromStructure(srcArrayData, *destStruct, arrayElt);

					// "Increment" the arrayData struct (make it point to the next block of memory in the structarray)
					srcArrayData.SetInstanceDataPtr(srcArrayData.GetInstanceData() + srcElementSize);
				}

				return;
			}
		}

		// allocate new storage
		size_t eltSize = file.GetSchemaCatalog().FindMemberSize(destStructure.GetSchema(), eltSchema);
		size_t storageSize = count * eltSize;

		bool isPodArray = eltSchema.GetType().IsPod();

		size_t alignment = eltSchema.GetType().GetAlign();
		if (alignment == 0)
		{
			alignment = 16; // default to 16 if we don t have any better info
		}
		alignment = Max(alignment, (size_t)1 << memSchema.GetArrayAlignmentPower());

		bool isPhysical = memSchema.GetArrayIsPhysical();

		char* storage = NULL;

		bool needsCookie = false;

		// If this is a C-style array of structures, we need an array cookie
		if (memSchema.GetType().IsCArray() && eltSchema.GetType() == psoType::TYPE_STRUCT)
		{
			needsCookie = true;
		}

		// When do we need to allocate space for the array cookie too? Only for pointer-style arrays?
		if (needsCookie)
		{
			// Allocate a class with a destructor, so we leave room for the array size cookie
			FastAssert(alignment == 16); 
			int numQuadWords = RoundUp<16>(int(storageSize)) / 16;
			EmptyClass16* storageWithCookie = NULL;
			if (isPhysical)
			{
				// This usage is technically incorrect - it's not possible to know how much memory is required for 
				// an array-new allocation. See the discussion here:
				// http://stackoverflow.com/questions/8720425/array-placement-new-requires-unspecified-overhead-in-the-buffer
				// But on all the platforms we've worked with so far, 16 bytes is sufficient to hold the array cookie. 

				void* physicalMem = parUtils::PhysicalAllocate((numQuadWords+1)*16, 16); // Add 16b for cookie overhead
				storageWithCookie = rage_placement_new(physicalMem) EmptyClass16[numQuadWords];
			}
			else
			{
				storageWithCookie = rage_aligned_new(16) EmptyClass16[numQuadWords];
			}
			parUtils::SetArrayCookie(storageWithCookie, 16, numQuadWords, count);
			storage = reinterpret_cast<char*>(storageWithCookie);
		}
		else
		{
			if (isPhysical)
			{
				storage = (char*)parUtils::PhysicalAllocate(storageSize, alignment);
			}
			else
			{
			storage = rage_aligned_new(alignment) char[storageSize];
		}
		}

		// Copy data into storage and set the array contents
		// See above for why we can double up pointer types
		switch(memSchema.GetType().GetEnum())
		{
		case psoType::TYPE_ATARRAY32:
		case psoType::TYPE_ATARRAY64:
			{
				psoFakeAtArray16& fakeArray = mem.GetDataAs<psoFakeAtArray16>();
				sysMemCpy(storage, file.GetInstanceDataAs<char*>(fakeArray.m_Elements.m_StructId), storageSize);
				fakeArray.m_Elements.m_Pointer = storage;
			}
			break;
		case psoType::TYPE_ATARRAY32_32BITIDX:
		case psoType::TYPE_ATARRAY64_32BITIDX:
			{
				psoFakeAtArray32& fakeArray = mem.GetDataAs<psoFakeAtArray32>();
				sysMemCpy(storage, file.GetInstanceDataAs<char*>(fakeArray.m_Elements.m_StructId), storageSize);
				fakeArray.m_Elements.m_Pointer = storage;
			}
			break;
		case psoType::TYPE_ARRAY_POINTER32:
		case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
		case psoType::TYPE_ARRAY_POINTER64:
		case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
			{
				psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
				sysMemCpy(storage, file.GetInstanceDataAs<char*>(fakePtr.m_StructId), storageSize);
				fakePtr.m_Pointer = storage;
			}
			break;
		default:
			parErrorf("Shouldn't get here!");
		}


		// Now fixup all the elements
		if (!isPodArray)
		{
			for(int i = 0; i < (int)count; i++)
			{
				psoMember arrayElt(eltSchema, reinterpret_cast<parPtrToMember>(reinterpret_cast<char*>(storage) + i * eltSize), &file);
				FixupPsoMember(destStructure, arrayElt);
			}
		}

	}
}

void FixupVptr(psoStruct& str, size_t vptrVal = 0)
{
	FastAssert(str.GetSchema().GetFlags().IsSet(psoSchemaStructureData::FLAG_NEEDS_VPTR_FIXUP));

	if (vptrVal == 0)
	{
		vptrVal = psoObjectFixup::FindVptr(*PARSER.FindStructure(str.GetSchema().GetNameHash()));
	}

	size_t* contentsAsSizet = reinterpret_cast<size_t*>(str.GetInstanceData());
	*contentsAsSizet = vptrVal;
}

void FixupPsoMembers(psoStruct& destStructure)
{
	psoStructureSchema& schema = destStructure.GetSchema();

	if (schema.GetFlags().IsSet(psoSchemaStructureData::FLAG_NEEDS_VPTR_FIXUP))
	{
		FixupVptr(destStructure);
	}

	for(int i = 0; i < schema.GetNumMembers(); i++)
	{
		psoMemberSchema memSchema = schema.GetMemberByIndex(i);
		if (psoConstants::IsReserved(memSchema.GetNameHash()) || memSchema.GetType().IsPod())
		{
			// Only fix up named, non-POD members
			continue;
		}

		psoMember mem = destStructure.GetMemberByIndex(i);

		FixupPsoMember(destStructure, mem);
	}

	parStructure* parStruct = PARSER.FindStructure(schema.GetNameHash());
	parDelegateHolderBase* delegateHolder = parStruct->FindDelegate("PostPsoPlace");
	parDelegateHolder<psoObjectFixup::PostPsoPlaceDel>* realDelHolder = reinterpret_cast<parDelegateHolder<psoObjectFixup::PostPsoPlaceDel>*> (delegateHolder);
	if (realDelHolder)
	{
		realDelHolder->Delegate(destStructure.GetInstanceData());
	}
}

// The embarrassing similarities in fixup code for POD arrays and members and between fixups and byte swaps is not lost on me.
void FixupPodArrayInPlace(psoFile& file, psoType type, parPtrToArray startOfArrayPtr, parPtrToArray endOfArrayPtr)
{
	char* startOfArray = reinterpret_cast<char*>(startOfArrayPtr);
	char* endOfArray = reinterpret_cast<char*>(endOfArrayPtr);

	size_t stride = type.GetSize();

	// All of the pointer types are doubled up here. Why? Because since we are doing fixups we know we are really only dealing with native types.
	// In other words we only go through the case psoType::TYPE_POINTER64 on a true 64-bit build, and psoFakePtr is 64-bits, and likewise we only
	// go through case psoType::TYPE_POINTER32 on a 32-bit build and psoFakePtr is 32-bits.
	FastAssert(type.IsNative()); // Lets make sure just in case

	switch (type.GetEnum())
	{
	case psoType::TYPE_STRING_POINTER32:
	case psoType::TYPE_STRING_POINTER64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakePtr& fakePtr = *reinterpret_cast<psoFakePtr*>(instPtr);
			fakePtr.m_Pointer = file.GetInstanceDataAs<char*>(fakePtr.m_StructId);
		}
		break;
	case psoType::TYPE_WIDE_STRING_POINTER32:
	case psoType::TYPE_WIDE_STRING_POINTER64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakePtr& fakePtr = *reinterpret_cast<psoFakePtr*>(instPtr);
			fakePtr.m_Pointer = file.GetInstanceDataAs<char16*>(fakePtr.m_StructId);
		}
		break;
	case psoType::TYPE_ATSTRING32:
	case psoType::TYPE_ATSTRING64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakeAtString& fakeStr = *reinterpret_cast<psoFakeAtString*>(instPtr);
			if (fakeStr.m_Data.m_StructId.IsNull())
			{
				fakeStr.m_Allocated = 0;
				fakeStr.m_Length = 0;
				fakeStr.m_Data.m_Pointer = NULL;
			}
			else
			{
				char* stringData = file.GetInstanceDataAs<char*>(fakeStr.m_Data.m_StructId);
				int length = (int)strlen(stringData);
				fakeStr.m_Data.m_Pointer = stringData;
				fakeStr.m_Allocated = (u16)(length+1);
				fakeStr.m_Length = (u16)length;
			}
		}
		break;
	case psoType::TYPE_ATWIDESTRING32:
	case psoType::TYPE_ATWIDESTRING64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakeAtString& fakeStr = *reinterpret_cast<psoFakeAtString*>(instPtr);
			if (fakeStr.m_Data.m_StructId.IsNull())
			{
				fakeStr.m_Allocated = 0;
				fakeStr.m_Length = 0;
				fakeStr.m_Data.m_Pointer = NULL;
			}
			else
			{
				char16* stringData = file.GetInstanceDataAs<char16*>(fakeStr.m_Data.m_StructId);
				int length = (int)wcslen(stringData);
				fakeStr.m_Allocated = (u16)(length+1);
				fakeStr.m_Length = (u16)length;
				fakeStr.m_Data.m_Pointer = reinterpret_cast<char*>(stringData);
			}
		}
		break;
	case psoType::TYPE_POINTER32:
	case psoType::TYPE_POINTER64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakePtr& fakePtr = *reinterpret_cast<psoFakePtr*>(instPtr);
			fakePtr.m_Pointer = file.GetInstanceDataAs<char*>(fakePtr.m_StructId);
		}
		break;
	case psoType::TYPE_ATARRAY32:
	case psoType::TYPE_ATARRAY64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakeAtArray16& fakeArray = *reinterpret_cast<psoFakeAtArray16*>(instPtr);
			fakeArray.m_Elements.m_Pointer = file.GetInstanceDataAs<char*>(fakeArray.m_Elements.m_StructId);
			fakeArray.m_Capacity = fakeArray.m_Count;
		}
		break;
	case psoType::TYPE_ATARRAY32_32BITIDX:
	case psoType::TYPE_ATARRAY64_32BITIDX:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakeAtArray32& fakeArray = *reinterpret_cast<psoFakeAtArray32*>(instPtr);
			fakeArray.m_Elements.m_Pointer = file.GetInstanceDataAs<char*>(fakeArray.m_Elements.m_StructId);
			fakeArray.m_Capacity = fakeArray.m_Count;
		}
		break;
	case psoType::TYPE_ARRAY_POINTER32:
	case psoType::TYPE_ARRAY_POINTER64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakePtr& fakePtr = *reinterpret_cast<psoFakePtr*>(instPtr);
			fakePtr.m_Pointer = file.GetInstanceDataAs<void*>(fakePtr.m_StructId);
		}
		break;

	case psoType::TYPE_ATBINARYMAP32:
	case psoType::TYPE_ATBINARYMAP64:
		for(char* instPtr = startOfArray; instPtr < endOfArray; instPtr += stride)
		{
			psoFakeAtBinMap& fakeMap = *reinterpret_cast<psoFakeAtBinMap*>(instPtr);
			fakeMap.m_Data.m_Elements.m_Pointer = file.GetInstanceDataAs<char*>(fakeMap.m_Data.m_Elements.m_StructId);
			fakeMap.m_Data.m_Capacity = fakeMap.m_Data.m_Count;
		}
		break;
	default:
		parAssertf(!type.HasPointers(), "Type has pointers, but we didn't do a fixup?");
		break;
	}
}


void FixupPsoMemberInPlace(psoStruct& destStructure, psoMember& mem)
{
	psoMemberSchema memSchema = mem.GetSchema();
	psoFile& file = destStructure.GetFile();

	// All of the pointer types are doubled up here. Why? Because since we are doing fixups we know we are really only dealing with native types.
	// In other words we only go through the case psoType::TYPE_POINTER64 on a true 64-bit build, and psoFakePtr is 64-bits, and likewise we only
	// go through case psoType::TYPE_POINTER32 on a 32-bit build and psoFakePtr is 32-bits.
	FastAssert(memSchema.GetType().IsNative()); // Lets make sure just in case


	switch (memSchema.GetType().GetEnum())
	{
	case psoType::TYPE_STRING_POINTER32:
	case psoType::TYPE_STRING_POINTER64:
		{
			psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
			fakePtr.m_Pointer = file.GetInstanceDataAs<char*>(fakePtr.m_StructId);
		}
		break;
	case psoType::TYPE_WIDE_STRING_POINTER32:
	case psoType::TYPE_WIDE_STRING_POINTER64:
		{
			psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
			fakePtr.m_Pointer = file.GetInstanceDataAs<char16*>(fakePtr.m_StructId);
		}
		break;
	case psoType::TYPE_ATSTRING32:
	case psoType::TYPE_ATSTRING64:
		{
			psoFakeAtString& fakeStr = mem.GetDataAs<psoFakeAtString>();
			if (fakeStr.m_Data.m_StructId.IsNull())
			{
				fakeStr.m_Allocated = 0;
				fakeStr.m_Length = 0;
				fakeStr.m_Data.m_Pointer = NULL;
			}
			else
			{
				char* stringData = file.GetInstanceDataAs<char*>(fakeStr.m_Data.m_StructId);
				int length = (int)strlen(stringData);
				fakeStr.m_Data.m_Pointer = stringData;
				fakeStr.m_Allocated = (u16)(length+1);
				fakeStr.m_Length = (u16)length;
			}
		}
		break;
	case psoType::TYPE_ATWIDESTRING32:
	case psoType::TYPE_ATWIDESTRING64:
		{
			psoFakeAtString& fakeStr = mem.GetDataAs<psoFakeAtString>();
			if (fakeStr.m_Data.m_StructId.IsNull())
			{
				fakeStr.m_Allocated = 0;
				fakeStr.m_Length = 0;
				fakeStr.m_Data.m_Pointer = NULL;
			}
			else
			{
				char16* stringData = file.GetInstanceDataAs<char16*>(fakeStr.m_Data.m_StructId);
				int length = (int)wcslen(stringData);
				fakeStr.m_Allocated = (u16)(length+1);
				fakeStr.m_Length = (u16)length;
				fakeStr.m_Data.m_Pointer = reinterpret_cast<char*>(stringData);
			}
		}
		break;
	case psoType::TYPE_STRUCT:
		{
			psoStruct subStruct = mem.GetSubStructure();
			psoStructureSchema& subStructSchema = subStruct.GetSchema();
			size_t vptrVal = 0;
			if (subStructSchema.GetFlags().IsSet(psoSchemaStructureData::FLAG_NEEDS_VPTR_FIXUP))
			{
				vptrVal = psoObjectFixup::FindVptr(*PARSER.FindStructure(subStructSchema.GetNameHash()));
			}
			FixupPsoMembersInPlace(subStruct, subStructSchema, vptrVal);
		}
		break;
	case psoType::TYPE_POINTER32:
	case psoType::TYPE_POINTER64:
		{
			psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
			fakePtr.m_Pointer = file.GetInstanceDataAs<char*>(fakePtr.m_StructId);
		}
		break;
	case psoType::TYPE_ARRAY_MEMBER:
	case psoType::TYPE_ATFIXEDARRAY:
		{
			psoMemberArrayInterface memArr(destStructure, mem);
			psoMemberSchema eltSchema = memArr.GetArrayElementSchema();

			if (!eltSchema.GetType().IsPod())
			{
				size_t count = memArr.GetArrayCount();
				for(size_t i = 0; i < count; i++)
				{
					psoMember arrayElt = memArr.GetElement(i);
					FixupPsoMemberInPlace(destStructure, arrayElt);
				}
			}
		}
		break;
	case psoType::TYPE_ATARRAY32:
	case psoType::TYPE_ATARRAY64:
		{
			psoFakeAtArray16& fakeArray = mem.GetDataAs<psoFakeAtArray16>();
			fakeArray.m_Elements.m_Pointer = file.GetInstanceDataAs<char*>(fakeArray.m_Elements.m_StructId);
			fakeArray.m_Capacity = fakeArray.m_Count;
		}
		break;
	case psoType::TYPE_ATARRAY32_32BITIDX:
	case psoType::TYPE_ATARRAY64_32BITIDX:
		{
			psoFakeAtArray32& fakeArray = mem.GetDataAs<psoFakeAtArray32>();
			fakeArray.m_Elements.m_Pointer = file.GetInstanceDataAs<char*>(fakeArray.m_Elements.m_StructId);
			fakeArray.m_Capacity = fakeArray.m_Count;
		}
		break;
	case psoType::TYPE_ARRAY_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
	case psoType::TYPE_ARRAY_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		{
			psoFakePtr& fakePtr = mem.GetDataAs<psoFakePtr>();
			fakePtr.m_Pointer = file.GetInstanceDataAs<void*>(fakePtr.m_StructId);
		}
		break;

	case psoType::TYPE_ATBITSET32:
	case psoType::TYPE_ATBITSET64:
		{
			psoFakeAtBitset& fakeBits = mem.GetDataAs<psoFakeAtBitset>();
			fakeBits.m_Bits.m_Pointer = file.GetInstanceDataAs<u32*>(fakeBits.m_Bits.m_StructId);
		}
		break;

	case psoType::TYPE_ATBINARYMAP32:
	case psoType::TYPE_ATBINARYMAP64:
		{
			psoFakeAtBinMap& fakeMap = mem.GetDataAs<psoFakeAtBinMap>();
			fakeMap.m_Data.m_Elements.m_Pointer = file.GetInstanceDataAs<char*>(fakeMap.m_Data.m_Elements.m_StructId);
			fakeMap.m_Data.m_Capacity = fakeMap.m_Data.m_Count;
		}
		break;
	default:
		parAssertf(0, "Shouldn't get here");
		break;
	}
}

void FixupPsoMembersInPlace(psoStruct& str, psoStructureSchema& schema, size_t vptrVal)
{
	if (vptrVal)
	{
		size_t* contentsAsSizet = reinterpret_cast<size_t*>(str.GetInstanceData());
		*contentsAsSizet = vptrVal;
	}

	for(int i = 0; i < schema.GetNumMembers(); i++)
	{
		psoMemberSchema memSchema = schema.GetMemberByIndex(i);
		if (psoConstants::IsReserved(memSchema.GetNameHash()) || memSchema.GetType().IsPod())
		{
			// Only fix up named, non-POD members
			continue;
		}

		psoMember mem = str.GetMemberByIndex(i);

		FixupPsoMemberInPlace(str, mem);
	}

	parStructure* parStruct = PARSER.FindStructure(schema.GetNameHash());
	parDelegateHolderBase* delegateHolder = parStruct->FindDelegate("PostPsoPlace");
	parDelegateHolder<psoObjectFixup::PostPsoPlaceDel>* realDelHolder = reinterpret_cast<parDelegateHolder<psoObjectFixup::PostPsoPlaceDel>*> (delegateHolder);
	if (realDelHolder)
	{
		realDelHolder->Delegate(str.GetInstanceData());
	}
}
} // namespace psoFileReader

psoLoadInPlaceResult psoFile::DirectFixupMatchingPsoContents()
{
	// Loop over all of the entries in the structure map (i.e. loop over all structarrays)

	for(u32 i = 0; i < GetNumStructArrays(); i++)
	{
		psoRscStructArrayTableData& mapEntry = GetStructArrayEntry(i);
		parPtrToArray startOfStructArray = reinterpret_cast<parPtrToArray>(mapEntry.m_Data.GetPtr());
		parPtrToArray endOfStructArray = startOfStructArray + mapEntry.m_Size;

		if (psoConstants::IsPsoType(mapEntry.m_NameHash))
		{
			psoType type = (psoType::Type)mapEntry.m_NameHash.GetHash();
			psoFileReader::FixupPodArrayInPlace(*this, type, startOfStructArray, endOfStructArray);
		}
		else
		{
			// Generate a psoStructId that gets us the first element of the ith structarray
			psoStructId id = psoStructId::Create(i, 0);

			psoStruct str = GetInstance(id);
			psoStructureSchema& schema = str.GetSchema();

			parAssertf(str.IsValid(), "Couldn't get a valid structure for maptable entry %d", i);

			u32 size = (u32)schema.GetSize();

			parAssertf(reinterpret_cast<parPtrToStructure>(startOfStructArray) == str.GetInstanceData(), "Unexpected instance data pointer for structure");

			parStructure* parStruct = PARSER.FindStructure(schema.GetNameHash());


#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
			// If the parStructure that the psoStruct corresponds to is external, we need to register
			// all of the addresses for the new objects with the external structure manager, so the 
			// external structure manager can get the right derived types later on
			parStructure* extParStruct = parStruct;
			if (extParStruct && extParStruct->GetFlags().IsClear(parStructure::IS_EXTERNAL))
			{
				extParStruct = NULL;
			}
#endif


			const psoObjectFixup* objFixup = psoObjectFixup::FindObjectFixup(*parStruct);
			if (objFixup)
			{
				while(startOfStructArray < endOfStructArray)
				{
					parPtrToStructure instance = reinterpret_cast<parPtrToStructure>(startOfStructArray);
					objFixup->Apply(*this, instance);
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
					if (extParStruct)
					{
						PARSER.GetExternalStructureManager().AddInstance(instance, extParStruct);
					}
#endif
					startOfStructArray += size;
				}
			}
		}
	}

	psoLoadInPlaceResult result;
	result.m_InplaceBuffer = NULL;
	result.m_InplaceBufferSize = 0;
	result.m_RootObject = reinterpret_cast<parPtrToStructure>(GetRootInstance().GetInstanceData());
	result.m_RootObjectType = PARSER.FindStructure(GetRootInstance().GetSchema().GetNameHash());
	result.m_Status = psoLoadInPlaceResult::PSOLOAD_IN_PLACE;
	return result;
}

// This function is for when you know that every structure in a psoFile exactly matches a runtime structure.
// In that case the psoFile effectively contains a resourced version of the object you are trying to load, so we make a full copy
// of all of the instance data in the psoFile and do a quick fixup pass on it
psoLoadInPlaceResult psoFile::CopyAndFixupMatchingPsoContents()
{
	char* newContents = rage_aligned_new(16) char[m_IffInstanceDataSize];
	sysMemCpy(newContents, m_IffInstanceData, m_IffInstanceDataSize);

	ptrdiff_t adjustmentFromOldToNewPtr = newContents - m_IffInstanceData;

	// Update all the structarray pointers
	for(u32 i = 0; i < GetNumStructArrays(); i++)
	{
		psoRscStructArrayTableData& mapEntry = GetStructArrayEntry(i);
		mapEntry.m_Data = mapEntry.m_Data.GetPtr() + adjustmentFromOldToNewPtr;
	}

	psoLoadInPlaceResult result = DirectFixupMatchingPsoContents();

	// Restore the structarray pointers
	for(u32 i = 0; i < GetNumStructArrays(); i++)
	{
		psoRscStructArrayTableData& mapEntry = GetStructArrayEntry(i);
		mapEntry.m_Data = mapEntry.m_Data.GetPtr() - adjustmentFromOldToNewPtr;
	}

	result.m_InplaceBuffer = newContents;
	result.m_InplaceBufferSize = m_IffInstanceDataSize;
	return result;
}


#endif // PSO_USING_NATIVE_BIT_SIZE

