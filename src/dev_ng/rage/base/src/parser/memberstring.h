// 
// parser/memberstring.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERSTRING_H
#define PARSER_MEMBERSTRING_H

#include "member.h"

#include "memberstringdata.h"

#include "data/callback.h"

namespace rage {

typedef u16	char16; // avoids including unicode.h

// PURPOSE: A parMember subclass for describing string data.
class parMemberString : public parMember
{
public:
	typedef parMemberStringSubType::Enum SubType;
	typedef parMemberStringData Data;
	parMemberString() {}
	explicit parMemberString(Data& data) : parMember(&data) {}

	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;

	bool IsWide() {
		return (GetSubtype() == parMemberStringSubType::SUBTYPE_WIDE_MEMBER || GetSubtype() == parMemberStringSubType::SUBTYPE_WIDE_POINTER || GetSubtype() == parMemberStringSubType::SUBTYPE_ATWIDESTRING);
	}

	void SetFromString(parPtrToStructure containingStructureAddr, const char* name) const;

	const char* GetStringContentsFromStruct(parPtrToStructure containingStructureAddr) const;
	const char16* GetWideStringContentsFromStruct(parPtrToStructure containingStructureAddr) const;

	virtual size_t GetSize() const;
	virtual size_t FindAlign() const;

	const char* GetInitValue();

	atHashStringNamespaces GetNamespaceIndex() const;

	bool HasExternalStorage() const
	{
		return (
			GetSubtype() == parMemberStringSubType::SUBTYPE_POINTER ||
			GetSubtype() == parMemberStringSubType::SUBTYPE_CONST_STRING ||
			GetSubtype() == parMemberStringSubType::SUBTYPE_ATSTRING ||
			GetSubtype() == parMemberStringSubType::SUBTYPE_WIDE_POINTER ||
			GetSubtype() == parMemberStringSubType::SUBTYPE_ATWIDESTRING
			);
	}

public:
	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberString);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberString*	parMember::AsString()			{ return parMemberType::IsString(GetType()) ? smart_cast<parMemberString*>(this) : NULL; }

}

#endif
