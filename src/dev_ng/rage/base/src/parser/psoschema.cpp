// 
// parser/psoschema.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psoschema.h"

#include "memberdata.h"
#include "memberarraydata.h"
#include "memberenumdata.h"
#include "membermapdata.h"
#include "memberstringdata.h"
#include "memberstructdata.h"
#include "optimisations.h"
#include "psobyteswap.h"
#include "psoconsts.h"
#include "psodata.h"
#include "psoresource.h"

#include "math/simplemath.h"
#include "string/unicode.h"

PARSER_OPTIMISATIONS();

using namespace rage;

psoRscMemberSchemaData psoMemberSchema::sm_NullData;
psoRscStructSchemaData psoStructureSchema::sm_NullData;
psoRscEnumSchemaData psoEnumSchema::sm_NullData;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool psoMemberSchema::IsEquivalentTo(const psoMemberSchema& other, bool considerNames, bool considerOffsets) const
{
	if (memcmp(&m_MemberData->Generic, &other.m_MemberData->Generic, sizeof(m_MemberData->Generic)) != 0)
	{
		return false;
	}
	if (considerNames && (GetNameHash() != other.GetNameHash()))
	{
		return false;
	}
	if (considerOffsets && (GetOffset() != other.GetOffset()))
	{
		return false;
	}

	return true;
}

size_t psoSchemaCatalog::FindMemberSize(psoStructureSchema& str, psoMemberSchema& mem)
{
	size_t simpleSize = mem.GetType().GetSize();
	if (simpleSize > 0)
	{
		return simpleSize;
	}

	// else need to do some more work to get the real size
	switch(mem.GetType().GetEnum())
	{
	case psoType::TYPE_STRUCT:
		{
			psoStructureSchema refStruct = FindStructureSchema(mem.GetReferentHash());

			// For the time being, it's OK if refStruct is invalid - if we have a PSO file that contains an array of structures 
			// where the array is length 0 we call this function to get the size of each member. But since
			// there were no members, we have a valid referent hash but there's no need to actually
			// define the referent type in the PSO file (since... no members).
			// rsTODO: Maybe we should define the referent type anyway, so we could add members? Or only define
			// the referent hash if there are any members in the array?
			return refStruct.IsValid() ? refStruct.GetSize() : 0;
		}
	case psoType::TYPE_STRING_MEMBER:		return sizeof(char) * mem.GetStringCount();
	case psoType::TYPE_WIDE_STRING_MEMBER:	return sizeof(char16) * mem.GetStringCount();
	case psoType::TYPE_ARRAY_MEMBER:
		{
			psoMemberSchema elementDesc = str.GetMemberByIndex(mem.GetArrayElementSchemaIndex());
			size_t eltSize = FindMemberSize(str, elementDesc);
			return eltSize * mem.GetFixedArrayCount();
		}
	case psoType::TYPE_ATFIXEDARRAY:
		{
			psoMemberSchema elementDesc = str.GetMemberByIndex(mem.GetArrayElementSchemaIndex());
			size_t eltSize = FindMemberSize(str, elementDesc);
			return RoundUp<4>(eltSize * mem.GetFixedArrayCount()) + sizeof(u32);
		}
	case psoType::TYPE_BITSET8:				return RoundUp<8>(mem.GetBitsetCount())/8;
	case psoType::TYPE_BITSET16:			return RoundUp<16>(mem.GetBitsetCount())/8;
	case psoType::TYPE_BITSET32:			return RoundUp<32>(mem.GetBitsetCount())/8;
	default:
		break;
	}

	mem.GetType().PrintUnexpectedTypeError("FindMemberSize", "a member type");
	return 0;
}

psoStructureSchema psoSchemaCatalog::FindStructureSchema( atLiteralHashValue schemaName ) const
{
	// rsTODO: Linear search sucks. replace it
	for(u32 i = 0; i < m_ResourceData.m_NumStructSchemas; i++)
	{
		if (schemaName == m_ResourceData.m_StructSchemaTable[i].m_NameHash)
		{
			return psoStructureSchema(m_ResourceData.m_StructSchemaTable[i]);
		}
	}

	return psoStructureSchema();
}

psoEnumSchema psoSchemaCatalog::FindEnumSchema( atLiteralHashValue schemaName ) const
{
	// rsTODO: Linear search sucks. replace it
	for(u32 i = 0; i < m_ResourceData.m_NumEnumSchemas; i++)
	{
		if (schemaName == m_ResourceData.m_EnumSchemaTable[i].m_NameHash)
		{
			return psoEnumSchema(m_ResourceData.m_EnumSchemaTable[i]);
		}
	}

	return psoEnumSchema();
}

// This may be unnecessarily recursive, but we don't know what order we'll be computing signatures in so we don't know
// if this child has a valid sig or not in it's m_Signature field.
// The 'forRuntimeDestination' flag lets us make signatures that are not symmetrical. For example if you have a type that
// you can't fast load, you want a way to make sure the signature as stored in the file will be different from the one
// that we compute for the runtime destination data type
u32 psoSchemaCatalog::ComputeMemberSignature(const psoStructureSchema& str, const psoMemberSchema& mem, bool forRuntimeDestination) const
{
	psoType type = mem.GetType();
	if (type == psoType::TYPE_STRUCT)
	{
		psoStructureSchema subSchema = FindStructureSchema(mem.GetReferentHash());

		return ComputeSignature(subSchema, forRuntimeDestination);
	}
	else if (type.IsEnum())
	{
		return FindEnumSchema(mem.GetReferentHash()).ComputeBaseSignature();
	}
	else if (type.IsBitset())
	{
		if (psoConstants::IsReserved(mem.GetBitsetEnumHash()))
		{
			return 0;
		}
		return FindEnumSchema(mem.GetBitsetEnumHash()).ComputeBaseSignature();
	}
	else if (type.IsArray() && type.IsDirectContainer()) // only care about array members that directly contain their data, not through a pointer
	{
		psoMemberSchema kidMember = str.GetMemberByIndex(mem.GetArrayElementSchemaIndex());
		return ComputeMemberSignature(str, kidMember, forRuntimeDestination);
	}
	else if (type.IsMap() && forRuntimeDestination)
	{
		return 1; // return something non zero that will change the sig
	}

	return 0;
}

u32 psoSchemaCatalog::ComputeSignature(const psoStructureSchema& str, bool forRuntimeDestination) const
{
	// The full signature for a single schema is the base signature for that schema, 
	// xored with any necessary referenced schema.
	// (generally that means any schema where changing that schema would change the memory
	// layout for this object. So it applies for a nested strucure, but not for a pointer)

	u32 sig = str.ComputeBaseSignature();

	for(int i = 0; i < str.GetNumMembers(); i++)
	{
		psoMemberSchema mem = str.GetMemberByIndex(i);

		if (!psoConstants::IsAnonymous(mem.GetNameHash()))
		{
			u32 memberSig = NtoB(ComputeMemberSignature(str, mem, forRuntimeDestination));
			if (memberSig != 0)
			{
				sig = atPartialDataHash(reinterpret_cast<char*>(&memberSig), sizeof(memberSig), sig);
			}
		}
	}

	return atFinalizeHash(sig);
}


void psoSchemaCatalog::ComputeAllSignatures(bool forRuntimeDestination)
{
	for(int i = 0; i < m_ResourceData.m_NumEnumSchemas; i++)
	{
		m_ResourceData.m_EnumSchemaTable[i].m_Signature = m_ResourceData.m_EnumSchemaTable[i].ComputeBaseSignature();
	}
	for(int i = 0; i < m_ResourceData.m_NumStructSchemas; i++)
	{
		psoStructureSchema schemaWrapper(m_ResourceData.m_StructSchemaTable[i]);
		m_ResourceData.m_StructSchemaTable[i].m_Signature = ComputeSignature(psoStructureSchema(m_ResourceData.m_StructSchemaTable[i]), forRuntimeDestination);
	}
}

psoStructureSchema psoSchemaCatalog::GetStructureSchema( int i )
{
	FastAssert(i >= 0 && i < m_ResourceData.m_NumStructSchemas);
	return psoStructureSchema(m_ResourceData.m_StructSchemaTable[i]);
}

psoEnumSchema psoSchemaCatalog::GetEnumSchema( int i )
{
	FastAssert(i >= 0 && i < m_ResourceData.m_NumEnumSchemas);
	return psoEnumSchema(m_ResourceData.m_EnumSchemaTable[i]);
}

psoMemberSchema psoStructureSchema::GetMemberByIndex( int i ) const
{
	FastAssert(i >= 0 && i < GetNumMembers());
	return psoMemberSchema(m_SchemaData->m_Members[i]);
}

psoMemberSchema psoStructureSchema::GetMemberByName(atLiteralHashValue hash) const
{
	for(int i = 0; i < GetNumMembers(); i++)
	{
		psoMemberSchema mem = GetMemberByIndex(i);
		if (mem.GetNameHash() == hash)
		{
			return mem;
		}
	}

	return psoMemberSchema();
}

u32 psoStructureSchema::ComputeBaseSignature() const
{
	return m_SchemaData->ComputeBaseSignature();
}

bool psoEnumSchema::ValueFromName(atLiteralHashValue hash, int& outResult) const
{
	for(int i = 0; i < GetNumEnums(); i++)
	{
		if (m_SchemaData->m_Values[i].m_NameHash == hash)
		{
			outResult = m_SchemaData->m_Values[i].m_Value;
			return true;
		}
	}
	return false;
}

bool psoEnumSchema::NameFromValue(int value, atLiteralHashValue& outHash) const
{
	for(int i = 0; i < GetNumEnums(); i++)
	{
		if (m_SchemaData->m_Values[i].m_Value == value)
		{
			outHash = m_SchemaData->m_Values[i].m_NameHash;
			return true;
		}
	}
	return false;
}

u32 psoEnumSchema::ComputeBaseSignature() const
{
	return m_SchemaData->ComputeBaseSignature();
}
