// 
// parser/macros.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MACROS_H 
#define PARSER_MACROS_H 

#include "diag/channel.h"

#include <cstddef>

// PURPOSE: If true - all classes and member variables will have names. If false, only the ones with preserveNames="true" will.
#define PARSER_ALL_METADATA_HAS_NAMES (__BANK || __DEV || __RESOURCECOMPILER || !__NO_OUTPUT || __TOOL)

// PURPOSE: Do the parStructures and parMembers themselves have reflection info
#define PARSER_USES_EXTERNAL_STRUCTURE_DEFNS ((__DEV || __RESOURCECOMPILER || __TOOL) && !__SPU)

// PURPOSE: Define this to be able to load 32-bit PSO files in 64-bit builds
#define PSO_FORCE_32BIT		0

// PURPOSE: Convenience macro, for #ifing stuff out that won't work based on PSO_FORCE_32BIT
#define PSO_USING_NATIVE_BIT_SIZE (!(__64BIT) || !(PSO_FORCE_32BIT))

#if __SPU  // No parser on SPU
#define PARSER
#define INIT_PARSER_WITH_SETTINGS(settings)
#define INIT_PARSER
#define SHUTDOWN_PARSER
#define REGISTER_PARSABLE_CLASS(x)
#define UNREGISTER_PARSABLE_CLASS(x)
#define PAR_PARSABLE
#define PAR_SIMPLE_PARSABLE
#define PAR_CHECK_OUT_OF_DATE_HEADER(includeGuardName, fileGuid)
#define PAR_HEADER_GUID(includeGuardName, fileGuid)
#else

namespace rage {
	// Some forward declarations that the macros below will need
	class parManager;
	class parStructure;

	// These are some placeholder types. Client code should not need to use them directly, they are only used internally for additional type safety.

	// Represents a pointer to some parsable data, so the parser can distinguish pointers to structures from pointers to members
	class parParsableStructure { parParsableStructure(); char c; }; // shouldn't construct these, inherit from them, etc.
	typedef parParsableStructure* parPtrToStructure;
	typedef const parParsableStructure* parConstPtrToStructure;

	// Represents a pointer to a member variable
	class parParsableMember { parParsableMember(); char c; };
	typedef parParsableMember* parPtrToMember;
	typedef const parParsableMember* parConstPtrToMember;

	// Represents a pointer to an array of some data
	class  parParsableArrayData { parParsableArrayData(); char c;};
	typedef parParsableArrayData* parPtrToArray;
	typedef const parParsableArrayData* parConstPtrToArray;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
// Macros for using the parser.
// These are used to access the parser manager, init and shutdown the system,
// declare classes as parsable, and manually register classes.
//
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// PURPOSE: Access the main parser manager object
#define PARSER (*::rage::parManager::sm_Instance)

// PURPOSE: Call this before using the PARSER object if you need to specify
// non-standard settings for initialization (you can still change settings
// later with either INIT_PARSER macro)
#define INIT_PARSER_WITH_SETTINGS(settings)						\
	do {														\
		if (!::rage::parManager::sm_Instance) {	\
			::rage::parManager::sm_Instance = rage_new ::rage::parManager;			\
		}														\
		::rage::parSettings::CreateStandardSettings();			\
		PARSER.Initialize(settings, true);						\
	} while(0)

// PURPOSE: Call this before using the PARSER object
#define INIT_PARSER  INIT_PARSER_WITH_SETTINGS(::rage::parSettings::sm_StandardSettings)

// PURPOSE: Cleans up after the parser, unregistering all registered objects
#define SHUTDOWN_PARSER										\
	do {													\
		bool shouldDelete = false;							\
		{													\
			shouldDelete = PARSER.Uninitialize();			\
		}													\
		if (shouldDelete) {									\
			delete ::rage::parManager::sm_Instance;			\
			::rage::parManager::sm_Instance = NULL;			\
		}													\
	}														\
	while(0)

// PURPOSE: Registers a class with the PARSER. If you aren't using automatic registration
// you need to call this before using this class with the parser.
#define REGISTER_PARSABLE_CLASS(x)							\
	do {													\
	parManager::AutoUseTempMemory temp;						\
	x::parser_Register();									\
	}														\
	while(0)

// PURPOSE: Removes a class from the PARSER registry
#define UNREGISTER_PARSABLE_CLASS(x)								\
	do {															\
	parManager::AutoUseTempMemory temp;								\
	if (x::parser_GetStaticStructure()) {							\
	PARSER.UnregisterStructure(x::parser_GetStaticStructure());		\
	x::parser_GetStaticStructureRef() = NULL;						\
	}																\
		else {														\
		Errorf("Class \"%s\" wasn't registered with the parser", #x);	\
		}															\
	}																\
	while(0)

// PURPOSE: Add this to your class if you want the class to be parsable.
// NOTES: Adds two static members to the class and adds a virtual function to return the
//   derived type's parStructure* given a base type.
#define PAR_PARSABLE															\
	friend class ::rage::parManager;											\
	friend class ::rage::parStructure;											\
public:																			\
	static PS3_ONLY(__attribute__((noinline))) WIN32_ONLY(__declspec(noinline))  void parser_Register();					\
	virtual ::rage::parStructure* parser_GetStructure() const {					\
		Assertf(parser_Data.m_Structure, "Class hasn't been registered with parser"); \
		return parser_Data.m_Structure;											\
	}																			\
	::rage::parPtrToStructure parser_GetPointer() { return reinterpret_cast<::rage::parPtrToStructure>(this); } \
	::rage::parConstPtrToStructure parser_GetPointer() const { return reinterpret_cast<::rage::parConstPtrToStructure>(this); } \
	static ::rage::parStructure* parser_GetStaticStructure() {return parser_Data.m_Structure;} \
	static ::rage::parStructure*& parser_GetStaticStructureRef() {return parser_Data.m_Structure;} \
	__forceinline static bool parser_IsInheritable() { return true; }			\
protected:																		\
	static ::rage::parStructureStaticData	parser_Data;						\
	static ::rage::u32 const				parser_MemberOffsets[];				\
//END

// PURPOSE: Add this to your class if you want the class to be parsable
// NOTES: Same as PAR_PARSABLE, but does not create a virtual function. If your instances
//   don't already have a vtable you may want to use this macro instead. If you use
//   this macro everything will work as normal except that you won't be able to 
//   use this class as a base class for any parsable objects
#define PAR_SIMPLE_PARSABLE														\
	friend class ::rage::parManager;											\
	friend class ::rage::parStructure;											\
public:																			\
	static PS3_ONLY(__attribute__((noinline))) WIN32_ONLY(__declspec(noinline)) void parser_Register();			\
	/*Note this is non-virtual*/												\
	::rage::parStructure* parser_GetStructure() const {							\
		Assertf(parser_Data.m_Structure, "Class hasn't been registered with parser"); \
		return parser_Data.m_Structure;											\
	}																			\
	::rage::parPtrToStructure parser_GetPointer() { return reinterpret_cast<::rage::parPtrToStructure>(this); } \
	::rage::parConstPtrToStructure parser_GetPointer() const { return reinterpret_cast<::rage::parConstPtrToStructure>(this); } \
	static ::rage::parStructure* parser_GetStaticStructure() {return parser_Data.m_Structure;} \
	static ::rage::parStructure*& parser_GetStaticStructureRef() {return parser_Data.m_Structure;} \
	__forceinline static bool parser_IsInheritable() { return false; }			\
protected:																		\
	static ::rage::parStructureStaticData	parser_Data;						\
	static ::rage::u32 const				parser_MemberOffsets[];				\
//END


// PURPOSE: Helper macro to forward declare one of the generated parser enum objects (so you can access it from other code)
// PARAMS: __type - MANGLED name of the enum type
// NOTES: The __type parameter needs to be the mangled name of the enum, not the actual name. Just replace :: with __. 
//		  So if your enum were rage::ScriptSettings::Flags the mangled name would be rage__ScriptSettings__Flags
#define EXTERN_PARSER_ENUM(__type) \
	extern ::rage::parEnumData parser_##__type##_Data

// PURPOSE: Macro to get one of the generated parser enum objects (an instance of parEnumData that describes one enum)
// PARAMS: __type - MANGLED name of the enum type
// NOTES: The __type parameter needs to be the mangled name of the enum, not the actual name. Just replace :: with __. 
//		  So if your enum were rage::ScriptSettings::Flags the mangled name would be rage__ScriptSettings__Flags
#define PARSER_ENUM(__type) (::parser_##__type##_Data)

// PURPOSE: Converts a token into a hash key at compile time. PAR_STRINGKEY(blah) is 
// gets defined as the value of atStringHash("blah"), but since its computed at compile
// time (well, code generation time) it's usable in switch statements and other places
// a compile-time constant value is required
#define PAR_STRINGKEY(x)		(PAR_sk_##x)

#define PAR_USE_UP_TO_DATE_CHECKS (1 && !__FINAL && !RSG_ORBIS)

#if PAR_USE_UP_TO_DATE_CHECKS

// PURPOSE: Used to check for out of date headers - this bit of code will cause link errors if the matching
// symbol doesn't exist in some .cpp file
#define PAR_CHECK_OUT_OF_DATE_HEADER(includeGuardName, fileGuid) \
	namespace rage { namespace parser { extern int UpToDateCheck_Failed_Try_Building_Again_ ## includeGuardName ## __ ## fileGuid ; } } \
	namespace { \
	static int parser_upToDateVal_ ## includeGuardName = ::rage::parser::UpToDateCheck_Failed_Try_Building_Again_ ## includeGuardName ## __ ## fileGuid ;  \
	inline bool parser_upToDateFn_ ## includeGuardName () { return parser_upToDateVal_ ## includeGuardName == 0; } \
	}

#define PAR_HEADER_GUID(includeGuardName, fileGuid) \
	namespace rage { namespace parser { int UpToDateCheck_Failed_Try_Building_Again_ ## includeGuardName ## __ ## fileGuid = 0; } }

#else
#define PAR_CHECK_OUT_OF_DATE_HEADER(includeGuardName, fileGuid)
#define PAR_HEADER_GUID(includeGuardName, fileGuid)
#endif

//PURPOSE: A container for some of the static data that a parsable class needs
namespace rage
{
	struct parStructureStaticData
	{
		u32					m_StructureNameHash;
		const char*			m_StructureName;
		parStructure*		m_Structure;
		void* const*		m_MemberData;
		u32 const*			m_MemberOffsets;
		const char* const*	m_MemberNames;
		bool				m_IsInheritable;
		bool				m_AlwaysHasNames;
	};
}

#endif // __SPU

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Parser)
#define parAssertf(cond,fmt,...)			RAGE_ASSERTF(Parser,cond,fmt,##__VA_ARGS__)
#define parFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(Parser,cond,fmt,##__VA_ARGS__)
#define parVerifyf(cond,fmt,...)			RAGE_VERIFYF(Parser,cond,fmt,##__VA_ARGS__)
#define parErrorf(fmt,...)					RAGE_ERRORF(Parser,fmt,##__VA_ARGS__)
#define parWarningf(fmt,...)				RAGE_WARNINGF(Parser,fmt,##__VA_ARGS__)
#define parDisplayf(fmt,...)				RAGE_DISPLAYF(Parser,fmt,##__VA_ARGS__)
#define parDebugf1(fmt,...)					RAGE_DEBUGF1(Parser,fmt,##__VA_ARGS__)
#define parDebugf2(fmt,...)					RAGE_DEBUGF2(Parser,fmt,##__VA_ARGS__)
#define parDebugf3(fmt,...)					RAGE_DEBUGF3(Parser,fmt,##__VA_ARGS__)
#define parLogf(severity,fmt,...)			RAGE_LOGF(Parser,severity,fmt,##__VA_ARGS__)
#define parCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Parser,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END

#if __ASSERT
inline void parAssertAligned(size_t value, size_t alignment, const char* message) 
{
	parAssertf((value & (alignment - 1)) == 0, "Incorrect alignment for %s, expected %" SIZETFMT "d. Value is 0x%" SIZETFMT "x", message, alignment, value);
}
#else
#define parAssertAligned(x,y,z)
#endif


#endif // PARSER_MACROS_H 
