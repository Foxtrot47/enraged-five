// 
// parser/manager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "manager.h"

#include "codegenutils.h"
#include "formatinfo.h"
#include "optimisations.h"
#include "tree.h"
#include "treenode.h"
#include "visitorinit.h"
#include "visitortree.h"

#include "atl/map.h"
#include "atl/string.h"
#include "data/marker.h"
#include "diag/output.h"
#include "file/device.h"
#include "file/stream.h"
#include "parsercore/streamrbf.h"
#include "parsercore/streamxml.h"
#include "system/platform.h"
#include "system/param.h"
#include "system/timer.h"
#include "diag/restservice.h"
#include "parser/restparserservices.h"
#include "data/growbuffer.h"
#include "file/token.h"

using namespace rage;

PARSER_OPTIMISATIONS();

RAGE_DEFINE_CHANNEL(Parser);

PARAM(parsercatalog, "[startup] Creates a catalog of all auto-registered parsable classes at startup");
PARAM(debugpatch, "Prints a message every time an item has been successfully patched and outputs a \"<filename>.patch.result\" file located next to the target file that can be used to verify the effect of the patch file.");

CompileTimeAssert(sizeof(parParsableStructure) == 1 && __alignof(parParsableStructure) == 1);
CompileTimeAssert(sizeof(parParsableMember) == 1 && __alignof(parParsableMember) == 1);
CompileTimeAssert(sizeof(parParsableArrayData) == 1 && __alignof(parParsableArrayData) == 1);

parManager* parManager::sm_Instance = NULL;

parManager::parManager()
	: m_InitializationCount(0)
	, m_IsAutoregistering(false)
{
}

void parManager::Initialize(parSettings settings, bool registerStatics) 
{
	if (m_InitializationCount == 0)
	{
		parDebugf1("Initializing parManager");
		sysTimer timer;
		Settings() = settings;

		{
			AutoUseTempMemory temp;
			if (registerStatics)
			{
				parStreamInXml::InitClass();
			}
			RegisterBuiltinFormats();
			RegisterAutoStructs();

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
			const char* defaultParserCatalogName = "$\\parsercatalog";
			const char* parserCatalogName = NULL;
			if (PARAM_parsercatalog.Get(parserCatalogName))
			{
				if (!parserCatalogName || parserCatalogName[0] == '\0')
				{
					parserCatalogName = defaultParserCatalogName;
				}
				SaveStructureDefns(parserCatalogName);
			}

			m_ExternalStructures.Init();
#endif
		}
		parDebugf1("Done Initializing parManager - took %fms", timer.GetMsTime());
	}

	m_InitializationCount++;
}

bool parManager::Uninitialize()
{
	m_InitializationCount--;

	if (m_InitializationCount == 0)
	{
		parDebugf1("Uninitializing parManager");
		AutoUseTempMemory temp;
		m_InputFormatInfo.Kill();
		m_OutputFormatInfo.Kill();

		for(StructureMap::Iterator iter = m_Structures.CreateIterator(); iter; ++iter)
		{
			if (--((*iter)->GetRegistrationCount()) == 0)
			{
				delete *iter;
			}
			*iter = NULL;
		}
		m_Structures.Kill();

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
		m_ExternalStructures.Shutdown();
#endif

		parStreamInXml::ShutdownClass();
		parDebugf1("Done Uninitializing parManager");
	}

	return m_InitializationCount == 0;
}


void parManager::AddPatchFileToList(const char* filename)
{
	char name[RAGE_MAX_PATH];	
	ASSET.RemoveExtensionFromPath(name, sizeof(name), filename);
	if(!patchFileList.Access(name))
	{
		patchFileList.Insert((atHashString)name, (atHashString)name);
	}
}

parManager::~parManager()
{
}

void parManager::RegisterBuiltinFormats()
{
	AutoUseTempMemory temp;

	// XML
	parInputFormatInfo xmlin;
	xmlin.m_FormatName = "xml";
	xmlin.m_StreamFactory = parStreamInFactoryFn(&parInputFormatInfo::SimpleFactory<parStreamInXml>);
	xmlin.m_TreeLoader = parTreeLoaderFn(&parInputFormatInfo::GenericStreamTreeLoader<parInputFormatInfo::SimpleFactory<parStreamInXml> >);
	xmlin.m_FileHeaderTest = parFileHeaderTestFn(&parStreamInXml::IsValidXmlStart);
	// XML doesn't have a file header test function, because it's our default format.
	RegisterInputFormat(XML, xmlin);

	parOutputFormatInfo xmlout;
	xmlout.m_FormatName = "xml";
	xmlout.m_StreamFactory = parStreamOutFactoryFn(&parOutputFormatInfo::SimpleFactory<parStreamOutXml>);
	xmlout.m_TreeSaver = parTreeSaverFn(&parOutputFormatInfo::GenericStreamTreeSaver<parOutputFormatInfo::SimpleFactory<parStreamOutXml> >);
	RegisterOutputFormat(XML, xmlout);


	// RBF
	parInputFormatInfo rbfin;
	rbfin.m_FormatName = "rbf";
	rbfin.m_FileHeaderTest = parFileHeaderTestFn(&parInputFormatInfo::SimpleHeaderTest<('R') | ('B' << 8) | ('F' << 16) | ('0' << 24)>);
	rbfin.m_StreamFactory = parStreamInFactoryFn(&parInputFormatInfo::SimpleFactory<parStreamInRbf>);
	rbfin.m_TreeLoader = parTreeLoaderFn(&parInputFormatInfo::GenericStreamTreeLoader<parInputFormatInfo::SimpleFactory<parStreamInRbf> >);
	RegisterInputFormat(BINARY, rbfin);

	parOutputFormatInfo rbfout;
	rbfout.m_FormatName = "rbf";
	rbfout.m_StreamFactory = parStreamOutFactoryFn(&parOutputFormatInfo::SimpleFactory<parStreamOutRbf>);
	rbfout.m_TreeSaver = parTreeSaverFn(&parOutputFormatInfo::GenericStreamTreeSaver<parOutputFormatInfo::SimpleFactory<parStreamOutRbf> >);
	RegisterOutputFormat(BINARY, rbfout);

	// PSO file
	parInputFormatInfo psoin;
	psoin.m_FormatName = "pso";
	psoin.m_FileHeaderTest = parFileHeaderTestFn(&parInputFormatInfo::SimpleHeaderTest<('P') | ('S' << 8) | ('I' << 16) | ('N' << 24)>);
	RegisterInputFormat(PSO, psoin);
}

void parManager::RegisterAutoStructs()
{
	RAGE_FUNC();

	m_IsAutoregistering = true;

	AutoUseTempMemory temp;

	// Temporarily turn off some warnings
	parSettings oldSettings = Settings();

	Settings().SetFlag(parSettings::WARN_ON_OUT_OF_ORDER_REGISTRATION, false);
	Settings().SetFlag(parSettings::WARN_ON_MULTIPLE_REGISTRATION, false);
	Settings().SetFlag(parSettings::ASSERT_ON_OUT_OF_ORDER_REGISTRATION, false);

	parStreamInXml::BeginAddingStaticStrings();

	parCguAutoRegistrationNode* regNode = parCguAutoRegistrationNode::sm_Head;
	int regCount = 0;
	while(regNode)
	{
		regNode->m_RegistrationFunction();
		regNode = regNode->m_Next;
		regCount++;
	}

	parDebugf2("Auto-registered %d classes", regCount);

	parStreamInXml::EndAddingStaticStrings();

	parCguAutoRegistrationNode::sm_Head = NULL;

	Settings() = oldSettings;

	m_IsAutoregistering = false;
}

void parManager::RegisterInputFormat(int format, parInputFormatInfo& info)
{
	m_InputFormatInfo.Insert(format, info);
}

void parManager::RegisterOutputFormat(int format, parOutputFormatInfo& info)
{
	m_OutputFormatInfo.Insert(format, info);
}

// Helper function for printing nice error messages when we can't write a file
namespace 
{
#if __ASSERT
	void WriteFailedError(const char* file, const char* ext)
	{
		char debugFilename[RAGE_MAX_PATH];
		debugFilename[0] = '\0';

		ASSET.AddExtension(debugFilename, RAGE_MAX_PATH, file, ext);

		char fullPath[RAGE_MAX_PATH];
		ASSET.FullWritePath(fullPath, RAGE_MAX_PATH, debugFilename, "");

		if (ASSET.Exists(fullPath, ""))
		{
			u32 attrs = ASSET.GetAttributes(fullPath, "");
			if (!parVerifyf(!(attrs & FILE_ATTRIBUTE_READONLY), "Couldn't write to file '%s' - it is read only. Does it need to be checked out?", fullPath))
			{
				return;
			}
			if (!parVerifyf(!(attrs & FILE_ATTRIBUTE_DIRECTORY), "Couldn't write to file '%s' - there is already a directory of the same name", fullPath))
			{
				return;
			}
			parAssertf(attrs, "Couldn't write to file '%s' - file attributes are 0x%08x", fullPath, attrs);
		}
		else
		{
			parAssertf(0, "Couldn't write to file '%s', does the path exist?", fullPath);
		}
	}
#endif
}

// Helper function for preloading - checks to make sure there is enough contiguous space first
fiStream* parManager::TryPreLoad(fiStream *S) {
	if (!S)
		return 0;

	unsigned size = S->Size();
	sysMemStartTemp();
	char *myBuffer = (char*)sysMemAllocator::GetCurrent().TryAllocate(size, 1);
	sysMemEndTemp();
	if (myBuffer)
	{
		S->Read(myBuffer, size);

		char buf[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(buf,sizeof(buf),myBuffer,size,true,S->GetName());
		S->Close();
		S = fiStream::Open(buf,fiDeviceMemory::GetInstance(),true);
		if (!S)
			delete myBuffer;
	}
	else
	{
		parWarningf("Couldn't preload file %s (%d bytes) for parsing - memory too low or fragmented?", S->GetName(), size);
	}
	return S;
}


parTree* parManager::LoadTree(parStreamIn* stream) const
{
	parTree* tree = parTree::LoadFromStream(stream);
	if (stream->GetSettings().GetFlag(parSettings::PROCESS_XINCLUDES) && stream->GetHasXInclude() && tree->GetRoot())
	{
		parTreeNodeUtils::FindAndResolveAllXIncludes(*tree->GetRoot());
	}

	if(stream->GetSettings().GetFlag(parSettings::CULL_OTHER_PLATFORM_DATA))
	{
		CullPlatformSpecificNodes(tree);
	}
	return tree;
}

parTree* parManager::LoadTree(fiStream* stream, const parSettings* settingsPtr) const
{
	RAGE_FUNC();
	parDebugf2("Loading tree from stream '%s'", stream ? stream->GetName() : "(null)");

	AutoUseTempMemory temp;

	int fmt;
	u32 firstBytes;
	FindInputFormat(stream, fmt, firstBytes);
#if __DEV || __TOOL
	parAssertf(fmt != INVALID, "File (%s) isn't in a parsable format (or file is too short)", stream->GetName());
#else
	parAssertf(fmt != INVALID, "File isn't in a parsable format (or file is too short)");
#endif
	if (fmt == INVALID || fmt == PSO)
	{
		return NULL;
	}

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	const parInputFormatInfo* info = m_InputFormatInfo.Access(fmt);

	if (info)
	{
		parStreamIn* pstream = info->m_StreamFactory(stream, settings);
		if (pstream)
		{
			pstream->SetHeaderBytes(firstBytes);

			parTree* ret = parTree::LoadFromStream(pstream);
			if (pstream->GetSettings().GetFlag(parSettings::PROCESS_XINCLUDES) && pstream->GetHasXInclude() && ret->GetRoot())
			{
				parTreeNodeUtils::FindAndResolveAllXIncludes(*ret->GetRoot());
			}

			if(pstream->GetSettings().GetFlag(parSettings::CULL_OTHER_PLATFORM_DATA))
			{
				CullPlatformSpecificNodes(ret);
			}

			delete pstream;
			return ret;
		}
		else
		{
			// maybe we can't load this format using the stream-oriented loader
			parWarningf("This will NOT apply the settings that we apply when loading from a stream. Fix the code!!!!");
			return info->m_TreeLoader(stream, settings, firstBytes);
		}
	}
	return NULL;
}

parTree* parManager::LoadTree(const char* file, const char* ext, const parSettings* settingsPtr) const
{
#if !__NO_OUTPUT
	char debugFilename[RAGE_MAX_PATH];
	debugFilename[0] = '\0';

	ASSET.AddExtension(debugFilename, RAGE_MAX_PATH, file, ext);
	parDebugf1("Loading tree from '%s'", debugFilename);
	DIAG_CONTEXT_MESSAGE("Loading parTree from file '%s'", debugFilename);
#endif

	sysTimer timer;

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	fiStream* S = ASSET.Open(file, ext);
	if (S 
		&& m_Settings.GetFlag(parSettings::PRELOAD_FILE) 
		&& strncmp(file, "memory:", 7) != 0
		&& strncmp(file, "growbuffer:", 11) != 0) {
			S = TryPreLoad(S);
	}
	if (S) {
		parTree* tree = LoadTree(S, &settings);
		S->Sanitize();
		S->Close();
		parDebugf2("Finished in %fms", timer.GetMsTime());
		return tree;
	}
	return NULL;
}

bool parManager::SaveTree(fiStream* stream, parTree* tree, int fmt, const parSettings* settingsPtr) const
{
	FastAssert(tree);
	return SaveTree(stream, tree->GetRoot(), fmt, settingsPtr);
}

bool parManager::SaveTree(fiStream* stream, parTreeNode* treenode, int fmt, const parSettings* settingsPtr) const
{
	RAGE_FUNC();
	parDebugf2("Saving tree to stream '%s'", stream ? stream->GetName() : "(null)");

	AutoUseTempMemory temp;

	if (fmt == UNKNOWN)
	{
		fmt = XML;
	}

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	const parOutputFormatInfo* info = m_OutputFormatInfo.Access(fmt);
	if (info)
	{
		return info->m_TreeSaver(stream, settings, treenode);
	}
	return false;
}

bool parManager::SaveTree(parStreamOut* stream, parTree* tree, int fmt) const
{
	FastAssert(tree);
	return SaveTree(stream, tree->GetRoot(), fmt); 
}

bool parManager::SaveTree(parStreamOut* stream, parTreeNode* treenode, int /*fmt*/) const
{
	RAGE_FUNC();
	parDebugf2("Saving tree to stream '%s'", stream ? stream->GetFiStream()->GetName() : "(null)");

	AutoUseTempMemory temp;

	return parTree::SaveToStream(stream, treenode);
}

bool parManager::SaveTree(const char* file, const char* ext, parTree* tree, int fmt, const parSettings* settingsPtr) const
{
	FastAssert(tree);
	return SaveTree(file, ext, tree->GetRoot(), fmt, settingsPtr);
}

bool parManager::SaveTree(const char* file, const char* ext, parTreeNode* treenode, int fmt, const parSettings* settingsPtr) const
{
#if !__NO_OUTPUT
	char debugFilename[RAGE_MAX_PATH];
	debugFilename[0] = '\0';

	ASSET.AddExtension(debugFilename, RAGE_MAX_PATH, file, ext);
	parDebugf1("Saving tree to '%s'", debugFilename);
	DIAG_CONTEXT_MESSAGE("Saving parTree to file '%s'", debugFilename);
#endif
	if (fmt == UNKNOWN)
	{
		fmt = XML;
	}

	u32 addBinaryFlag = (fmt == XML) ? 0 : parUtils::ADD_AS_BINARY;
	fiSafeStream S(parUtils::OpenWritableFileWithOptionalCheckout(file, ext, parUtils::PROMPT_FOR_CHECKOUT_DONT_ADD | addBinaryFlag));
	if (S)
	{
		return SaveTree(S, treenode, fmt, settingsPtr);
	}
#if __ASSERT
	else {
		WriteFailedError(file, ext);
	}
#endif
	return false;
}

parStreamIn* parManager::OpenInputStream(fiStream* stream, const parSettings* settingsPtr) const
{
	AutoUseTempMemory temp;

	int fmt;
	u32 firstBytes;
	FindInputFormat(stream, fmt, firstBytes);
	if (fmt == INVALID || fmt == PSO)
	{
		return NULL;
	}

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	const parInputFormatInfo* info = m_InputFormatInfo.Access(fmt);
	if (info)
	{
		parStreamIn* pstream = info->m_StreamFactory(stream, settings);
		pstream->SetHeaderBytes(firstBytes);
		
		return pstream;
	}
	return NULL;
}

parStreamIn* parManager::OpenInputStream(const char* file, const char* ext, const parSettings* settingsPtr) const
{
	fiStream* S = ASSET.Open(file, ext);

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	if (S 
		&& settings.GetFlag(parSettings::PRELOAD_FILE) 
		&& strncmp(file, "memory:", 7) != 0
		&& strncmp(file, "growbuffer:", 11) != 0)
	{
		S = TryPreLoad(S);
	}
	if (S)
	{
		return OpenInputStream(S, &settings);
	}
	return NULL;
}

parStreamOut* parManager::OpenOutputStream(fiStream* stream, int fmt, const parSettings* settingsPtr) const
{
	AutoUseTempMemory temp;

	if (fmt == UNKNOWN) {
		fmt = XML;
	}

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	const parOutputFormatInfo* info = m_OutputFormatInfo.Access(fmt);
	if (info)
	{
		parStreamOut* pstream = info->m_StreamFactory(stream, settings);
		return pstream;
	}
	return NULL;
}

parStreamOut* parManager::OpenOutputStream(const char* file, const char* ext, int fmt, const parSettings* settingsPtr) const
{
	u32 addBinaryFlag = (fmt == XML) ? 0 : parUtils::ADD_AS_BINARY;
	fiStream* S = parUtils::OpenWritableFileWithOptionalCheckout(file, ext, parUtils::PROMPT_FOR_CHECKOUT_DONT_ADD | addBinaryFlag);
	if (S) {
		return OpenOutputStream(S, fmt, settingsPtr);
	}
#if __ASSERT
	else
	{
		WriteFailedError(file, ext);
	}
#endif
	return NULL;
}

int parManager::FindInputFormat(u32 firstBytesAsU32) const
{
	for(atMap<int, parInputFormatInfo>::ConstIterator iter = m_InputFormatInfo.CreateIterator(); iter; ++iter) {
		bool match = (*iter).m_FileHeaderTest(firstBytesAsU32);
		if (match) {
			return iter.GetKey();
		}
	}
	return INVALID;
}

void parManager::FindInputFormat(fiStream* stream, int& outFormat, u32& outHeaderBytes) const
{
	int amtRead = stream->Read(&outHeaderBytes, sizeof(outHeaderBytes));
	if (amtRead < 4)
	{
		outFormat = INVALID;
		return;
	}
	outFormat = FindInputFormat(outHeaderBytes);
	if (outFormat != INVALID)
	{
		return; 
	}
	// else complain...
#if !__NO_OUTPUT
	char* bytes = reinterpret_cast<char*>(&outHeaderBytes);
	parErrorf("First four bytes of file %s don't match a known pattern. Bytes: '%c%c%c%c' (0x%x, 0x%x, 0x%x, 0x%x)",
		stream->GetName(),
		bytes[0], bytes[1], bytes[2], bytes[3],
		bytes[0], bytes[1], bytes[2], bytes[3]);
#endif

	outFormat = INVALID;
}

parStructure* parManager::FindStructure(const char* name) const
{
	FastAssert(name);
	const char* strName = name;
	if (name[0] == '_' && name[1] == '_') {
		strName = name + 2;
	}

	u32 key = atLiteralStringHash(strName);

	parStructure* const * str = m_Structures.Access(key);
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	if (str)
	{
		return *str;
	}
	return m_ExternalStructures.BuildFromStructdef(key); // Maybe we can create a parStructure on demand from a structdef
#else
	return str ? *str : NULL;
#endif
}

parStructure* parManager::FindStructure(u32 key) const
{
	parStructure* const * str = m_Structures.Access(key);
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	if (str)
	{
		return *str;
	}
	return m_ExternalStructures.BuildFromStructdef(key);
#else
	return str ? *str : NULL;
#endif
}

parEnumData* parManager::FindEnum(u32 key) const
{
	parEnumData* const * enumData = m_Enums.Access(key);
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	if (enumData)
	{
		return *enumData;
	}
	return m_ExternalStructures.FindOrBuildFromEnumdef(key);
#else
	return enumData ? *enumData : NULL;
#endif
}

void parManager::InitFromStructure(parStructure& structure, parPtrToStructure obj /* = NULL */) const
{
	parInitVisitor initter;
	initter.VisitToplevelStructure(obj, structure);
}

bool parManager::LoadFromStructure(parTreeNode* node, parStructure& structure, parPtrToStructure obj, bool verifyTypes) const
{
	RAGE_FUNC();
	parDebugf2("Loading tree into structure '%s' at %p", structure.GetName(), obj);

	parAssertf(node, "Can't load an empty node");
	if (!node) {
		return false;
	}
	parStructure* fileStr = FindStructure(node->GetElement().GetName());
	if (!verifyTypes || (Settings().GetFlag(parSettings::LOAD_UNKNOWN_TYPES) && !fileStr))
	{
		// do nothing
	}
	else
	{
		if (!fileStr)
		{
			parErrorf("File element %s isn't a name registered with the parser", node->GetElement().GetName());
			return false;
		}
		else if (!structure.IsSubclassOf(fileStr))
		{
			parErrorf("Can't load file element %s into an object of type %s", node->GetElement().GetName(), structure.GetName());
			return false;
		}
	}

	structure.ReadMembersFromTree(node, obj);
	return true;
}

bool parManager::LoadFromStructure(fiStream* stream, parStructure& structure, parPtrToStructure obj, bool verifyTypes, const parSettings* settingsPtr) const
{
	parTree* tree = LoadTree(stream, settingsPtr);
	parAssertf(tree && tree->GetRoot(), "Couldn't load from file '%s'. File was empty or incorrect format", stream->GetName());
	if (!tree) 
	{
		return false;
	}
	parTreeNode* node = tree->GetRoot();
	bool ret = LoadFromStructure(node, structure, obj, verifyTypes);
	{
		AutoUseTempMemory temp;
		delete tree;
	}
	parDebugf2("Done loading from stream '%s'", stream->GetName());
	return ret;
}

bool parManager::LoadFromStructure(const char* file, const char* ext, parStructure& structure, parPtrToStructure obj, bool verifyTypes, const parSettings* settingsPtr) const
{
#if !__NO_OUTPUT
	char debugFilename[RAGE_MAX_PATH];
	debugFilename[0] = '\0';

	ASSET.AddExtension(debugFilename, RAGE_MAX_PATH, file, ext);
	parDebugf1("Loading file '%s' into structure '%s'", debugFilename, structure.GetName());
	DIAG_CONTEXT_MESSAGE("Loading file '%s' into structure '%s'", debugFilename, structure.GetName());
#endif

	sysTimer timer;
	parTree* tree = LoadTree(file, ext, settingsPtr);

	parAssertf(tree && tree->GetRoot(), "Couldn't load from file '%s'. File was empty or incorrect format", file);
	if (!tree) 
	{
		return false;
	}
	parTreeNode* node = tree->GetRoot();
	bool ret = LoadFromStructure(node, structure, obj, verifyTypes);
	{
		AutoUseTempMemory temp;
		delete tree;
	}
	parDebugf2("Done loading from file '%s', finished in %fms", debugFilename, timer.GetMsTime());

#if !__RESOURCECOMPILER
	LoadPatchFile(file, ext, obj, &structure);
#endif // !__RESOURCECOMPILER

	return ret;
}

bool parManager::LoadPatchFile(const char* filename, const char* UNUSED_PARAM(ext), parPtrToStructure UNUSED_PARAM(obj), parStructure* UNUSED_PARAM(type)) const
{
	//bool r = true;

	//don't patch files starting with  "growbuffer:" or "memory:"
	if(strstr(filename, "growbuffer:") || strstr(filename, "memory:"))
	{
		return false;
	}

	//resolve filenames provided together with or separate from the extension
	char filenameNoExt[RAGE_MAX_PATH];	//file name with any extension removed
	char patchFileName[RAGE_MAX_PATH];	//modified filename to find patch
	filenameNoExt[0] = '\0';
	patchFileName[0] = '\0';
	ASSET.RemoveExtensionFromPath(filenameNoExt, sizeof(filenameNoExt), filename);
	if(!patchFileList.Access((atHashString)filenameNoExt))
	{
		return false;
	}


	parAssertf(0, "Patchfile %s found in content.xml. We don't want to use patch files anymore, integrate the changes into the original file and remove the patch file. IGNORING PATCH FILE!", filename);
	return false;	//we don't want patching to be applied on ng anymore

	/*
	//for meta files on platform(crc): device, look for patchfile in common(crc): to avoid having to maintain multiple identical copies of the patchfile in each platform folders.
	//make sure to watch out for differing folder structures - e.g. platform:/levels/ becomes common:/data/levels/
	//if path starts with "platform"
	if(strstr(filenameNoExt, "platform"))
	{	
		//replace it with "common"
		strcpy(patchFileName, "common");
		//append "crc:" if present in original path (+8 to find the end of the word "platform")
		if(strstr(filenameNoExt+8, "crc"))
			strcat(patchFileName, "crc");
		//end device name with ":"
		strcat(patchFileName, ":");
		//if filepath is "platform(crc):/levels replace it with common(crc):/data/levels
		if(strstr(strchr(filenameNoExt, '/'), "/levels"))
			strcat(patchFileName, "/data");
		//attach the rest of the filename (which shouldn't need any additional changes)
		strcat(patchFileName, strchr(filenameNoExt, '/'));
		//add ".patch" extension
		strcat(patchFileName, ".patch");
	}
	else
	{
		ASSET.AddExtension(patchFileName, sizeof(patchFileName), filenameNoExt, "patch");
	}



	parAssertf(ASSET.Exists(patchFileName, ""), "%s found in patchfile list in content.xml, but no matching patch file was found. Check the filepath in content.xml is correct, including crc.", patchFileName);
		if (ASSET.Exists(patchFileName, ""))
		{
			fiStream* patchStream = ASSET.Open(patchFileName, "");
			parTree* patchTree = PARSER.LoadTree(patchStream);
			if (patchTree && patchTree->GetRoot())
			{
				parTreeNode::ChildNodeIterator i = patchTree->GetRoot()->BeginChildren();
				for(; i != patchTree->GetRoot()->EndChildren(); ++i)
				{
					restCommand command;
					parTreeNode* currentNode = (*i);
					//get command verb
					const char* verb = currentNode->GetElement().FindAttribute("verb")->GetStringValue();
					if (!strcmp(verb, "GET"))
					{
						command.m_Verb = restCommand::REST_GET;
					}
					else if (!strcmp(verb, "POST"))
					{
						command.m_Verb = restCommand::REST_POST;
					}
					else if (!strcmp(verb, "PUT"))
					{
						command.m_Verb = restCommand::REST_PUT;
					}
					else if (!strcmp(verb, "DELETE"))
					{
						command.m_Verb = restCommand::REST_DELETE;
					}
					else
					{
						Quitf(ERR_SYS_PAR_1,"Invalid verb %s", verb);
					}

					//set data uri
					const char* uri = currentNode->GetElement().FindAttribute("uri")->GetStringValue();
					if(!command.SetUri(uri)) 
					{
						Quitf(ERR_SYS_PAR_2,"Couldn't parse URI %s in patch file %s", uri, patchFileName);
						r = false;
					}
					

					//set output stream
					fiSafeStream str(ASSET.Create("count:", ""));
					command.m_OutputStream = str;

					datGrowBuffer gb;

					//set command data - convert XML tree to text to pass it to command
					if (currentNode->GetChild())
					{
						parTreeNode* commandNode = currentNode->GetChild();
						commandNode->RemoveSelf(); // detach the node from the patch file tree...
						parTree* commandTree = rage_new parTree();
						commandTree->SetRoot(commandNode); // ... and add it to the new tree
						gb.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL), datGrowBuffer::NULL_TERMINATE);
						char gbName[RAGE_MAX_PATH];
						fiDevice::MakeGrowBufferFileName(gbName, RAGE_MAX_PATH, &gb);
						fiStream* stream = ASSET.Create(gbName, "");
						PARSER.SaveTree(stream, commandTree, parManager::XML);
						stream->Flush();
						stream->Close();
						command.m_Data = (const char*)gb.GetBuffer();
						delete commandTree;
					}
					else if (currentNode->GetData())
					{
						command.m_Data = currentNode->GetData();
					}


					int response = parRestProcessParsableObject(obj, type, command);
					if(response >= 400) //http response error codes are all 4xx or 5xx
					{
						Quitf(ERR_SYS_PAR_3,"Couldn't patch item %s in %s. Response code: %d", uri, patchFileName, response);
						r = false;
					}
#if __BANK
					else
					{
						if(PARAM_debugpatch.Get()) 
						Displayf("PATCHING SUCCESSFULLY COMPLETED AT %s IN %s", uri, filename);
					}
#endif //__BANK
				}
			}
			delete patchTree;
			patchStream->Close();
#if __BANK && !__TOOL
			if(PARAM_debugpatch.Get()) 
			{
				char debugFilePath[RAGE_MAX_PATH];
				strcpy(debugFilePath, "common:/patch_results/");

				char* deviceName = strchr(patchFileName, ':');
				strncat(debugFilePath, patchFileName, (int)(deviceName-patchFileName));

				char * nameStart = strrchr(patchFileName, '/');
				strcat(debugFilePath,&patchFileName[nameStart-patchFileName]);
				ASSET.CreateLeadingPath(debugFilePath);
				ASSERT_ONLY(parBuildTreeVisitor::sm_AssertOnUnsavableTypes = false);
				PARSER.SaveFromStructure(debugFilePath, "result", *type, obj, parManager::XML, NULL);
				ASSERT_ONLY(parBuildTreeVisitor::sm_AssertOnUnsavableTypes = true);
			}
				
#endif //__BANK
		}

	return r;
	*/
}

bool parManager::CreateAndLoadAnyType(const char* file, const char* ext, parStructure*& inOutStructure, parPtrToStructure& outObj, const parSettings* settingsPtr /* = NULL */) const
{
#if !__NO_OUTPUT
	char debugFilename[RAGE_MAX_PATH];
	debugFilename[0] = '\0';

	ASSET.AddExtension(debugFilename, RAGE_MAX_PATH, file, ext);
	parDebugf1("Creating data from file '%s'", debugFilename);
	DIAG_CONTEXT_MESSAGE("Creating data from file '%s'", debugFilename);
#endif

	parTree* tree = LoadTree(file, ext, settingsPtr);
	bool result = true;
	parSettings settings = settingsPtr ? *settingsPtr : Settings();
	if (tree && tree->GetRoot())
	{
		result = CreateAndLoadAnyType(tree->GetRoot(), inOutStructure, outObj, !settings.GetFlag(parSettings::LOAD_UNKNOWN_TYPES));
	}
	{
		AutoUseTempMemory temp;
		delete tree;
	}

	LoadPatchFile(file, ext, outObj, inOutStructure);

	return result;
}

bool parManager::CreateAndLoadAnyType(fiStream* stream, parStructure*& inOutStructure, parPtrToStructure& outObj, const parSettings* settingsPtr /* = NULL */) const
{
	parTree* tree = LoadTree(stream, settingsPtr);
	bool result = true;
	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	if (tree && tree->GetRoot())
	{
		result = CreateAndLoadAnyType(tree->GetRoot(), inOutStructure, outObj, !settings.GetFlag(parSettings::LOAD_UNKNOWN_TYPES));
	}
	{
		AutoUseTempMemory temp;
		delete tree;
	}
	return result;

}

bool parManager::CreateAndLoadAnyType(parStreamIn* stream, parStructure*& structure, parPtrToStructure& obj) const
{
	parTree* tree = LoadTree(stream);
	FastAssert(tree);
	parTreeNode* node = tree->GetRoot();
	FastAssert(node);
	
	bool result = CreateAndLoadAnyType(node, structure, obj, !stream->GetSettings().GetFlag(parSettings::LOAD_UNKNOWN_TYPES));

	{
		AutoUseTempMemory temp;
		delete tree;
	}

	return result;
}

bool parManager::CreateAndLoadAnyType(parTreeNode* node, parStructure*& inOutStructure, parPtrToStructure& outObj, bool verifyTypes) const
{
	// Cases (Type A, B are unrelated, Aa is a subclass of A. Type U is unknown to the parser)
	//   inOutStr	node		verifyTypes		|	result		outObj		inOutStr	invalid

	//	 NULL		NULL		false			|	true		NULL		NULL
	//   NULL		<A>			false			|	true		A			A			
	//	 NULL		<Aa>		false			|	true		Aa			Aa
	//	 NULL		<B>			false			|	true		B			B
	//	 NULL		<U>			false			|	false		NULL		NULL		

	//	 NULL		NULL		true			|	true		NULL		NULL		INVALID
	//	 NULL		<A>			true			|	false		NULL		NULL		INVALID
	//	 NULL		<Aa>		true			|	false		NULL		NULL		INVALID
	//	 NULL		<B>			true			|	false		NULL		NULL		INVALID
	//	 NULL		<U>			true			|	false		NULL		NULL		INVALID

	//	 A			NULL		false			|	true		NULL		A
	//   A			<A>			false			|	true		A			A
	//	 A			<Aa>		false			|	true		Aa			Aa
	//	 A			<B>			false			|	true		A			A
	//	 A			<U>			false			|	true		A			A

	//	 A			NULL		true			|	true		NULL		A
	//	 A			<A>			true			|	true		A			A
	//	 A			<Aa>		true			|	true		Aa			Aa
	//	 A			<B>			true			|	false		NULL		NULL
	//	 A			<U>			true			|	false		NULL		NULL

	//	 Aa			NULL		false			|	true		NULL		Aa
	//   Aa			<A>			false			|	true		Aa			Aa
	//	 Aa			<Aa>		false			|	true		Aa			Aa
	//	 Aa			<B>			false			|	true		Aa			Aa
	//	 Aa			<U>			false			|	true		Aa			Aa

	//	 Aa			NULL		true			|	true		NULL		Aa
	//	 Aa			<A>			true			|	false		NULL		NULL
	//	 Aa			<Aa>		true			|	true		Aa			Aa
	//	 Aa			<B>			true			|	false		NULL		NULL
	//	 Aa			<U>			true			|	false		NULL		NULL

	if (!Verifyf(inOutStructure != NULL || !verifyTypes, "verifyTypes must be false if you want to pass in a NULL parStructure"))
	{
		outObj = NULL;
		return false;
	}

	if (!node)
	{
		outObj = NULL;
		return true;
	}

	parStructure* fileStructure = FindStructure(node->GetElement().GetName());

	if (!inOutStructure)
	{
		if (fileStructure)
		{
			// Load whatever the file says to load
			inOutStructure = fileStructure;
		}
		else
		{
			outObj = NULL;
			return false;
		}

	}
	else
	{
		// "Normal" case
		if (fileStructure && fileStructure->IsSubclassOf(inOutStructure))
		{
			inOutStructure = fileStructure;
		}
		else if (!verifyTypes)
		{
			// keep inOutStructure as it is
		}
		else if (fileStructure)
		{
			parErrorf("Type %s is not a subclass of %s", fileStructure->GetName(), inOutStructure->GetName());
			inOutStructure = NULL;
			outObj = NULL;
			return false;
		}
		else
		{
			parErrorf("Type %s hasn't been registered with the parser", node->GetElement().GetName());
			inOutStructure = NULL;
			outObj = NULL;
			return false;
		}
	}

	outObj = inOutStructure->Create();
	inOutStructure->ReadMembersFromTree(node, outObj);

	return true;
}


parTreeNode* parManager::BuildTreeNodeFromStructure(parStructure& structure, parConstPtrToStructure obj, const char* overrideTopLevelName /* = NULL */, const parSettings* settingsPtr /* = NULL */) const
{
	RAGE_FUNC();
	parDebugf2("Building tree from structure '%s' at %p", structure.GetName(), obj);

	parSettings settings = settingsPtr ? *settingsPtr : Settings();

	parBuildTreeVisitor treeBuilder(settings);
	treeBuilder.m_TopLevelName = overrideTopLevelName;
	treeBuilder.VisitToplevelStructure(const_cast<parPtrToStructure>(obj), structure);
	parTreeNode* retVal = treeBuilder.m_Tree.GetRoot();
	treeBuilder.m_Tree.SetRoot(NULL); // so the builder doesn't free the old tree
	return retVal;
}

bool parManager::SaveFromStructure(fiStream* stream, parStructure& structure, parConstPtrToStructure obj, int fmt, const parSettings* settingsPtr) const
{
	AutoUseTempMemory temp;
	parTree* tree = rage_new parTree;
	tree->SetRoot(BuildTreeNodeFromStructure(structure, obj, NULL, settingsPtr));
	bool ret = SaveTree(stream, tree, fmt, settingsPtr);
	delete tree;
	parDebugf2("Done saving to stream '%s'", stream->GetName());
	return ret;
}


bool parManager::SaveFromStructure(parStreamOut* stream, parStructure& structure, parConstPtrToStructure obj, int fmt, const char* overrideTopLevelName /* = NULL */) const
{
	AutoUseTempMemory temp;
	parTree* tree = rage_new parTree;
	parSettings settings = stream->GetSettings();
	tree->SetRoot(BuildTreeNodeFromStructure(structure, obj, overrideTopLevelName, &settings));
	bool ret = SaveTree(stream, tree, fmt);
	delete tree;
	parDebugf2("Done saving to stream '%s'", stream->GetFiStream()->GetName());
	return ret;
}

bool parManager::SaveFromStructure(const char* file, const char* ext, parStructure& structure, parConstPtrToStructure obj, int fmt, const parSettings* settingsPtr) const
{
#if !__NO_OUTPUT
	char debugFilename[RAGE_MAX_PATH];
	debugFilename[0] = '\0';

	ASSET.AddExtension(debugFilename, RAGE_MAX_PATH, file, ext);
	parDebugf1("Saving to file '%s' with structure '%s'", debugFilename, structure.GetName());
	DIAG_CONTEXT_MESSAGE("Saving to file '%s' with structure '%s'", debugFilename, structure.GetName());
#endif

	u32 addBinaryFlag = (fmt == XML) ? 0 : parUtils::ADD_AS_BINARY;
	fiSafeStream S(parUtils::OpenWritableFileWithOptionalCheckout(file, ext, parUtils::PROMPT_FOR_CHECKOUT_DONT_ADD | addBinaryFlag));
	if (S) {
		return SaveFromStructure(S, structure, obj, fmt, settingsPtr);
	}
#if __ASSERT
	else
	{
		WriteFailedError(file, ext);
	}
#endif
	return false;
}


void parManager::UnregisterStructure(const char* name)
{
	AutoUseTempMemory temp;
	FastAssert(name);
	const char* strName = name;
	if (name[0] == '_' && name[1] == '_') {
		strName = name + 2;
	}

	u32 key = atLiteralStringHash(strName);
	parStructure** str = m_Structures.Access(key);
	parAssertf(str , "Can't find structure in map");
	if (--(*str)->GetRegistrationCount() == 0)
	{
		delete *str;
	}
	*str = NULL;
	m_Structures.Delete(key);
}

void parManager::RegisterStructure(u32 nameHash, parStructure* structure) 
{
	// const char* strName = name;
	// Remove any leading "__" (from when we mangled "::rage" into "__rage". Now we leave off the "::")
	// if (name[0] == '_' && name[1] == '_') {
	// 	strName = name + 2;
	// }

	u32 key = nameHash;

	m_Structures.Insert(key, structure);
	structure->GetRegistrationCount()++;
}

void parManager::RegisterEnum(parEnumData* enumData)
{
	m_Enums.Insert(enumData->m_NameHash, enumData);
	if (m_IsAutoregistering)
	{
		// Create literal hashstring objects for all the names, so that we can do reverse lookups later if necessary
#if PARSER_ALL_METADATA_HAS_NAMES
		atLiteralHashString enumName(enumData->m_EnumName);
#endif
		if (enumData->m_Names)
		{
			for(int i = 0; i < enumData->m_NumEnums; i++)
			{
				atLiteralHashString enumValName(enumData->m_Names[i]);
			}
		}
	}
}

void parManager::UnregisterStructure(parStructure* str)
{
	if (!str)
	{
		return;
	}
	AutoUseTempMemory temp;
	for(StructureMap::Iterator iter = m_Structures.CreateIterator(); iter; ++iter)
	{
		parStructure* structPtr = (*iter);
		if (structPtr == str)
		{
			u32 key = iter.GetKey();
			if (--structPtr->GetRegistrationCount() == 0)
			{
				delete structPtr;
			}
			m_Structures.Delete(key);
			return;
		}
	}
}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
void parManager::SaveStructureDefns(const char* filename)
{
	parStreamOut* stream = OpenOutputStream(filename, "", parManager::XML);

	if (!stream)
	{
		parWarningf("Couldn't write external structure definition file %s", filename);
		return;
	}
	
	parHeaderInfo inf;
	stream->WriteHeader(inf);

	parElement root("ExternalStructureDefinitions", false);
	stream->WriteBeginElement(root);

	// don't write these again for each call to PARSER.SaveObject	
	parSettings settings = stream->GetSettings();
	settings.SetFlag(parSettings::WRITE_UTF8_BOM, false);
	settings.SetFlag(parSettings::WRITE_XML_HEADER, false);
	stream->SetSettings(settings);

	for(StructureMap::Iterator mapIter = m_Structures.CreateIterator(); !mapIter.AtEnd(); mapIter.Next())
	{
		parElement structure(mapIter.GetData()->GetName(), false);
		stream->WriteBeginElement(structure);

		PARSER.SaveObject(stream, mapIter.GetData(), parManager::XML);

		stream->WriteEndElement(structure);
	}

	stream->WriteEndElement(root);

	stream->Close();
}

parExternalStructureManager& parManager::GetExternalStructureManager() 
{
	return m_ExternalStructures;
}

#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS


void rage::parManager::CullPlatformSpecificNodes( parTree* tree ) const
{
	if(tree == NULL)
	{
		return;
	}

	parTreeNode *node = tree->GetRoot();
	if(node != NULL)
	{
		atArray<parTreeNode*> removalList;
		atArray<atString>     platforms;
		char                  tmpBuffer[50];
		bool                  thisPlatform;

		Assertf(sysGetPlatform() != platform::UNKNOWN, "Could not retrieve this platform id!");
		
		while(node)
		{
			tmpBuffer[0] = '\0';
			platforms.ResetCount();
			const char* platformStr = node->GetElement().FindAttributeStringValue("platform", "", tmpBuffer, 50);

			// If there is a platform string.
			if(platformStr != NULL && platformStr[0] != '\0')
			{
				atString attValue = atString(platformStr);
				attValue.Split(platforms, "|", true);
				thisPlatform = false;

				for(s32 i = 0; !thisPlatform && i < platforms.size(); ++i)
				{
					platforms[i].Lowercase();
					platforms[i].Trim();

					if(    parUtils::IsCurrentPlatform(platforms[i].c_str()) 
						|| (platforms[i].StartsWith("!") && !parUtils::IsCurrentPlatform(&platforms[i].c_str()[1])) )
					{
						thisPlatform = true;
					}
				}
			}
			else
			{
				thisPlatform = true;
			}


			parTreeNode *next = NULL;
			if(!thisPlatform)
			{
				removalList.PushAndGrow(node);
			}
			else
			{
				next = node->GetChild();
			}

			// there are no parents or siblings so go back up the tree.
			while(next == NULL && node != NULL)
			{
				next = node->GetSibling();
				if(next == NULL)
				{
					node = node->GetParent();
				}
			}
			node = next;
		}

		// removeList now contains all unneeded nodes.
		for(s32 i = 0; i < removalList.size(); ++i)
		{
			delete removalList[i];
			removalList[i] = NULL;
		}
	}

}
