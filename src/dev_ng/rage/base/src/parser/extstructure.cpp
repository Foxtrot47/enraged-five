// 
// parser/extstructure.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "extstructure.h"

#include "manager.h"
#include "optimisations.h"
#include "psodata.h"
#include "structdefs.h"
#include "treenode.h"
#include "visitorinit.h"
#include "visitorutils.h"

#include "atl/delegate.h"
#include "atl/inlist.h"
#include "math/simplemath.h"

#include "extstructure_parser.h"

PARSER_OPTIMISATIONS();

namespace rage {

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS 

#define REG_STRUCTURE(name) mgr.RegisterStructure(name::parser_GetStaticStructure()->GetNameHash(), name::parser_GetStaticStructure());

	void parAddReflectionClassesToManager(parManager& mgr)
	{
		REG_STRUCTURE(rage::parStructure);
		REG_STRUCTURE(rage::parMember);
		REG_STRUCTURE(rage::parMemberArrayData);
		REG_STRUCTURE(rage::parMemberArray);
		REG_STRUCTURE(rage::parEnumListEntry);
		REG_STRUCTURE(rage::parEnumData);
		REG_STRUCTURE(rage::parMemberEnumData);
		REG_STRUCTURE(rage::parMemberEnum);
		REG_STRUCTURE(rage::parMemberBitset);
		REG_STRUCTURE(rage::parMemberMapData);
		REG_STRUCTURE(rage::parMemberMap);
		REG_STRUCTURE(rage::parMemberMatrixData);
		REG_STRUCTURE(rage::parMemberMatrix);
		REG_STRUCTURE(rage::parMemberSimpleData);
		REG_STRUCTURE(rage::parMemberSimple);
		REG_STRUCTURE(rage::parMemberStringData);
		REG_STRUCTURE(rage::parMemberString);
		REG_STRUCTURE(rage::parMemberStructData);
		REG_STRUCTURE(rage::parMemberStruct);
		REG_STRUCTURE(rage::parMemberVectorData);
		REG_STRUCTURE(rage::parMemberVector);
	}

#undef REG_STRUCTURE


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// parExtStructure

	class parExtStructure : public parStructure
	{
		// The only real difference is that an ext structure needs to do a little cleanup
		~parExtStructure()
		{
		}
	};


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Array construction functions

	// The external structures may contain an array member. The array member needs to define a callback that
	// can create an array of a specified size. Unfortunately all we get for those callbacks are a void*
	// pointer to the array member and a size in elements (not bytes). So we need a function object that
	// carries a little extra information to properly create the array. Also note that we don't know
	// what alignment we'll need - so do 16 byte aligned allocations just to be safe.

	struct ArrayBuilder
	{
		ArrayBuilder(size_t elementSize, parMemberArray& arrayMetadata) : m_ElementSize((u32)elementSize), m_ArrayMetadata(&arrayMetadata) {}


		static void InvalidArrayBuilder(parPtrToMember, size_t, size_t)
		{
			parErrorf("Can't allocate storage for this type of array when using external structures");
		}

		u32 m_ElementSize;
		parMemberArray* m_ArrayMetadata;
		inlist_node<ArrayBuilder> m_ListNode;

		static void SetArrayBuilderCb(parMemberArray& arrayMember);
	};

	typedef inlist<ArrayBuilder, &ArrayBuilder::m_ListNode> BuilderList;
	BuilderList g_Builders;

	void ArrayBuilder::SetArrayBuilderCb(parMemberArray& arrayMember)
	{
		ArrayBuilder* newBuilder = NULL;

		switch(arrayMember.GetSubtype())
		{
		case parMemberArraySubType::SUBTYPE_ATARRAY:
			arrayMember.GetData()->m_ResizeArray = rage_new atDelegate<void (parPtrToMember, size_t, size_t)>(&ArrayBuilder::InvalidArrayBuilder);
			break;
		case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
			arrayMember.GetData()->m_ResizeArray = rage_new atDelegate<void (parPtrToMember, size_t, size_t)>(&ArrayBuilder::InvalidArrayBuilder);
			break;
		case parMemberArraySubType::SUBTYPE_POINTER:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
			arrayMember.GetData()->m_ResizeArray = rage_new atDelegate<void (parPtrToMember, size_t, size_t)>(&ArrayBuilder::InvalidArrayBuilder);
			break;
		case parMemberArraySubType::SUBTYPE_VIRTUAL:
			arrayMember.GetData()->m_ResizeArray = rage_new atDelegate<void (parPtrToMember, size_t, size_t)>(&ArrayBuilder::InvalidArrayBuilder);
			break;
		case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
		case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
		case parMemberArraySubType::SUBTYPE_MEMBER:
			// Nothing to do here - storage already exists.
			break;
		}

		if (newBuilder)
		{
			g_Builders.push_front(newBuilder);
		}
	}




//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Map interface classes

	// This is a 'closure' class, just here so we can capture the parMemberMap and pass it along to
	// the parMemberAtBinaryMapGenericInterface constructor
	class parMemberAtBinaryMapGenericInterfaceFactory 
	{
	public:
		parMemberAtBinaryMapGenericInterfaceFactory(parMemberMap* map) : m_MapMember(map) {}

		parMemberMapInterface* Create(parMemberMap&, void* mapInstance)
		{
			return rage_new parMemberAtBinaryMapGenericInterface(*m_MapMember, mapInstance);
		}

		parMemberMap* m_MapMember;
	};


	// This is a 'closure' class, just here so we can capture the parMemberMap and pass it along to
	// the parMemberAtBinaryMapGenericInterface constructor
	class parMemberAtBinaryMapGenericIteratorFactory 
	{
	public:
		parMemberAtBinaryMapGenericIteratorFactory(parMemberMap* map) : m_MapMember(map) {}

		parMemberMapIterator* Create(parMemberMap&, void* mapInstance)
		{
			return rage_new parMemberAtBinaryMapGenericIterator(*m_MapMember, mapInstance);
		}

		parMemberMap* m_MapMember;
	};

	void SetMapInterfaceCbs(parMemberMap& mapMember)
	{
		switch(mapMember.GetSubtype())
		{
		case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
			{
				// TODO: Make this not leak memory some day

				parMemberAtBinaryMapGenericInterfaceFactory* interfaceFactory = rage_new parMemberAtBinaryMapGenericInterfaceFactory(&mapMember);
				mapMember.GetData()->m_CreateInterfaceDelegate = rage_new parMemberMapData::CreateInterfaceDelegate(interfaceFactory, &parMemberAtBinaryMapGenericInterfaceFactory::Create);

				parMemberAtBinaryMapGenericIteratorFactory* iteratorFactory = rage_new parMemberAtBinaryMapGenericIteratorFactory(&mapMember);
				mapMember.GetData()->m_CreateIteratorDelegate = rage_new parMemberMapData::CreateIteratorDelegate(iteratorFactory, &parMemberAtBinaryMapGenericIteratorFactory::Create);
			}

			break;
		case parMemberMapSubType::SUBTYPE_ATMAP:
			parAssertf(0, "Can't create interface callbacks for atMap yet, so don't use external structures containing maps");
			break;
		}
	}

	namespace parStructdefBuilder
	{
		extern void* DummyNameToPtrCB(const char* name);
		extern const char* DummyPtrToNameCB(const void* ptr);
	}

	void SetExternalNamedPtrCbs(parMemberStruct& structMember)
	{
		if (structMember.IsExternalPointer())
		{
			structMember.GetData()->m_NameToPtrCB = parStructdefBuilder::DummyNameToPtrCB;
			structMember.GetData()->m_PtrToNameCB = parStructdefBuilder::DummyPtrToNameCB;
		}
	}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// parExternalStructureManager::ExternalStructure

	struct parExternalStructureManager::ExternalStructure
	{
		parStructure* m_Structure;
		atDelegate<parStructure* (parConstPtrToStructure)> m_OldGetStructureCB;
		bool m_OwnsStructure;

		ExternalStructure()
			: m_Structure(NULL)
			, m_OwnsStructure(false)
		{
		}

		~ExternalStructure()
		{
			if (m_OwnsStructure)
			{
//				delete m_Structure;
			}
		}

		void Initialize(bool ownsEnumData)
		{
			// For every member we just created, mark the data non-static so it gets freed when the member does.
			MarkMemberDataNonStatic(*m_Structure, ownsEnumData);

			// For every parMemberArray that we just created, give it an array builder callback so we can create storage for the array
			parForEachInstance(*m_Structure, atDelegate<void (parMemberArray&)>(&ArrayBuilder::SetArrayBuilderCb));

			// For every parMemberMap that we just created, give it the map interface and map iterator creation callbacks so we can perform operations on the map
			parForEachInstance(*m_Structure, atDelegate<void (parMemberMap&)>(&SetMapInterfaceCbs));

			// For every parMemberStruct that we just created, make sure external_named pointers have name->pointer and pointer->name callbacks
			parForEachInstance(*m_Structure, atDelegate<void (parMemberStruct&)>(&SetExternalNamedPtrCbs));

			m_Structure->GetFlags().Set(parStructure::IS_EXTERNAL);

			PatchBaseClassDelegates();

			// For the new parStructure we just created (in LoadObjectPtr), give it some useful
			// delegates so we can create new instances and find the correct parStructure for 
			// instances of the new structure.

			if (m_Structure->IsConstructible())
			{
				parStructure::FactoryDelegate factoryDelegate(this, &ExternalStructure::Create);
				m_Structure->SetFactory(factoryDelegate);
			}

			parStructure::PlacementDelegate placementDelegate(this, &ExternalStructure::Place);
			m_Structure->SetPlacementFactory(placementDelegate);

			parStructure::GetStructureDelegate getStructureDelegate(this, &ExternalStructure::GetStructure);
			m_Structure->SetGetStructureCB(getStructureDelegate);

			parStructure::DestructorDelegate destructorDelegate(this, &ExternalStructure::Delete);
			m_Structure->SetDestructor(destructorDelegate);
		}

		static void MarkNonStatic(parMember& mem)
		{
			mem.GetCommonData()->m_Flags &= ~(1 << parMemberFlags::MEMBER_STATIC);
		}

		static void MarkEnumDataNonStatic(parMemberEnum& mem)
		{
			// If this enum points to empty data, replace it with a pointer to the static parEnumData::Empty
			parEnumData* edata = mem.GetData()->m_EnumData;
			if (parVerifyf(edata, "Didn't expect a NULL enumData pointer here"))
			{
				if (edata->m_NumEnums == 0 && edata->m_NameHash == 0)
				{
					delete edata;
					mem.GetData()->m_EnumData = &parEnumData::Empty;
				}
				else
				{
					mem.GetData()->m_EnumData->m_Flags &= ~(1 << parEnumFlags::ENUM_STATIC);
				}
			}
			else
			{
				mem.GetData()->m_EnumData = &parEnumData::Empty;
			}
		}

		static void MarkMemberDataNonStatic(parStructure& str, bool ownsEnumData)
		{
			parForEachInstance(str, atDelegate<void (parMember&)>(&parExternalStructureManager::ExternalStructure::MarkNonStatic));
			if (ownsEnumData)
			{
				parForEachInstance(str, atDelegate<void (parMemberEnum&)>(&parExternalStructureManager::ExternalStructure::MarkEnumDataNonStatic));
			}
		}

		void* Create()
		{
			void* buffer = m_Structure->AllocAndPlace();

			sysMemStartTemp();

#if __ASSERT
			parStructure** oldStructure = PARSER.GetExternalStructureManager().m_Instances.Access(buffer);
			parAssertf(!oldStructure || !*oldStructure, "Memory at location 0x%p was already registered as an instance of %s", buffer, (*oldStructure)->GetName());
#endif

			// Register this new instance in the instances map, so we know which parStructure was used
			// to create the instance.
			PARSER.GetExternalStructureManager().AddInstance(buffer, m_Structure);
			sysMemEndTemp();

			return buffer;
		}


		void Place(void* buffer)
		{
			// These generic structures don't really have useful constructors, but they may be derived
			// from classes that do have constructors, or classes that have virtual function tables.
			// So call the base class Place() function here, which will eventually percolate up to a
			// real placement function and give us the constructor and virtual table for the most
			// derived class that's an actual C++ class (not an external structure)
			if (m_Structure->GetBaseStructure())
			{
				m_Structure->GetBaseStructure()->Place(buffer);
			}

			// Now "Place" all of the members of the structure. We're probably going to run the parInitVisitor after
			// we call place, but some of the objects would normally have had default constructors that never got run here.
			if (PARSER.Settings().GetFlag(parSettings::INITIALIZE_NEW_DATA))
			{
				parMemberArray::sm_NoSanityChecks++;
				parMemberPlacementVisitor placer;
				for(int i = 0; i < m_Structure->GetNumMembers(); i++)
				{
					placer.VisitMember(reinterpret_cast<parPtrToStructure>(buffer), *m_Structure->GetMember(i));
				}
				parMemberArray::sm_NoSanityChecks--;
			}

		}

		void Delete(parPtrToStructure buffer)
		{
			sysMemStartTemp();
			PARSER.GetExternalStructureManager().RemoveInstance(buffer);
			sysMemEndTemp();

			for(int i = 0; i < m_Structure->GetNumMembers(); i++)
			{
				// We've previously allocated char*s for any external named pointers, so free them here.
				parMember* mem = m_Structure->GetMember(i);
				if (mem->GetType() == parMemberType::TYPE_STRUCT)
				{
					parMemberStruct& memStr = mem->AsStructOrPointerRef();
					if (memStr.IsExternalPointer())
					{
						StringFree(memStr.GetMemberFromStruct<char*>(buffer));
					}
				}

				parDeleteMember(buffer, *mem);
			}

			// These generic structures don't really have useful destructors, but they may be derived
			// from classes that do have destructor, or classes that have virtual function tables.
			// So call the base class Delete() function here

			if (m_Structure->GetBaseStructure())
			{
				m_Structure->GetBaseStructure()->Destroy(buffer);
			}
		}

		parStructure* GetStructure(parConstPtrToStructure addr)
		{
			// Check the instances map first, in case 'addr' points to an external structure
			parStructure** str = PARSER.GetExternalStructureManager().m_Instances.Access(addr);
			if (str) 
			{
				return *str;
			}
			// Otherwise, if this is the callback for a real C++ class whose GetStructureCB we've 
			// patched, call the real GetStructureCB.
			if (m_OldGetStructureCB.IsBound())
			{
				return m_OldGetStructureCB(addr);
			}
			return m_Structure;
		}

		void PatchBaseClassDelegates()
		{
			parExternalStructureManager* mgr = &PARSER.GetExternalStructureManager();
			parStructure* s = m_Structure->GetBaseStructure();
			while(s)
			{
				if (mgr->m_PatchedStructures.Access(s)) // Already patched it (and therefore its base classes too) so return
				{
					return;
				}

				parDebugf1("Patching getstructure delegate for %s", atLiteralHashString::TryGetString(s->GetNameHash()));

				ExternalStructure* str = rage_new ExternalStructure;
				str->m_Structure = s;
				str->m_OwnsStructure = false;

				str->m_OldGetStructureCB = s->GetGetStructureCB();

				atDelegate<parStructure* (parConstPtrToStructure)> getStructureDelegate(str,&ExternalStructure::GetStructure );
				str->m_Structure->SetGetStructureCB(getStructureDelegate);

				mgr->m_PatchedStructures.Insert(s, str);

				s = s->GetBaseStructure();
			}
		}
	};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// parExternalStructureManager

	parExternalStructureManager::parExternalStructureManager()
	{
	}

	void parExternalStructureManager::Init()
	{

	}

	void parExternalStructureManager::Shutdown()
	{
		for(int i = 0; i < m_ExternalStructures.GetCount(); i++)
		{
			delete m_ExternalStructures[i];
			m_ExternalStructures[i] = 0;
		}

		for(PatchMap::Iterator iter = m_PatchedStructures.CreateIterator(); iter; ++iter)
		{
			delete iter.GetData();
		}

		while(g_Builders.begin() != g_Builders.end())
		{
			ArrayBuilder* builder = *g_Builders.begin();
			g_Builders.erase(g_Builders.begin());
			delete builder;
		}

		for(EnumMap::Iterator iter = m_Enums.CreateIterator(); iter; ++iter)
		{
			parEnumData* enumData = iter.GetData();
			// Strings here are duplicated so need to be freed
			for(int i = 0; i < enumData->m_NumEnums; i++)
			{
				delete [] enumData->m_Names[i];
			}
			delete [] enumData->m_Names;
			delete [] enumData->m_Enums;

			delete iter.GetData();
		}
	}

	void parExternalStructureManager::LoadStructdefs(const char* rootPath)
	{
		sysMemStartTemp();
		m_Structdefs.LoadAllPscFiles(rootPath);
		sysMemEndTemp();
	}

	void parExternalStructureManager::LoadExternalStructureDefns(const char* filename)
	{
		sysMemStartTemp();
		m_Structdefs.LoadEsdFile(filename);
		sysMemEndTemp();
	}

	void parExternalStructureManager::AddInstance(const void* address, parStructure* type)
	{
		m_Instances[address] = type;
	}

	void parExternalStructureManager::RemoveInstance(const void* address)
	{
		m_Instances.Delete(address);
	}


	parStructure* parExternalStructureManager::BuildFromStructdef(u32 structhash)
	{
		parTreeNode* structdef = m_Structdefs.FindStructdef(atLiteralHashValue(structhash));

		if (!structdef)
		{
			return NULL;
		}

		sysMemStartTemp();

		ExternalStructure* str = rage_new ExternalStructure;
		m_ExternalStructures.PushAndGrow(str);

		str->m_Structure = rage_new parExtStructure;
		str->m_OwnsStructure = true;

		// Register it first so that if there is a circular reference we can still find a valid pointer
		// and don't get into an infinite loop.
		PARSER.RegisterStructure(structhash, str->m_Structure);

		const char* structName = NULL;
		bool ownsEnumData = false;

		// Check to see if we're loading a structdef or a parStructure
		if (!strcmp(structdef->GetElement().GetName(), "structdef"))
		{
			structdef->FindValueFromPath("@type", structName);
			parDebugf1("Loading external structure %s from <structdef>", structName);
			m_Structdefs.BuildStructure(*str->m_Structure, *structdef);
			ownsEnumData = false; // EnumData is added to a separate map
		}
		else
		{
			structdef->FindValueFromPath("Name", structName);
			parDebugf1("Loading external structure %s from <parStructure>", structName);
			PARSER.LoadObject(structdef, *str->m_Structure);
			ownsEnumData = true; // EnumData is written out as a child of the parStructure
		}

		str->Initialize(ownsEnumData);

		// We don't need the XML data any more, we have the real deal now
		m_Structdefs.DeleteStructdef(atLiteralHashValue(structhash));

		sysMemEndTemp();

		return str->m_Structure;
	}

	parEnumData* parExternalStructureManager::FindOrBuildFromEnumdef(u32 enumdefHash)
	{
		parEnumData** enums = m_Enums.Access(enumdefHash);
		if (enums)
		{
			return *enums;
		}

		parTreeNode* enumdef = m_Structdefs.FindEnumdef(atLiteralHashValue(enumdefHash));

		if (!enumdef)
		{
			return NULL;
		}

		sysMemStartTemp();

		parEnumData* newEnums = m_Structdefs.BuildEnumData(*enumdef);
		if (newEnums)
		{
			m_Enums[enumdefHash] = newEnums;
		}

		// We don't need the XML data any more, we have the real deal now
		m_Structdefs.DeleteEnumdef(atLiteralHashValue(enumdefHash));

		sysMemEndTemp();

		return newEnums;
	}	

#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS


} // namespace rage
