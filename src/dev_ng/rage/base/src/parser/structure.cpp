// 
// parser/structure.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "structure.h"

#include "manager.h"
#include "member.h"
#include "memberarray.h"
#include "memberenum.h"
#include "membermatrix.h"
#include "membermap.h"
#include "membersimple.h"
#include "memberstring.h"
#include "memberstruct.h"
#include "membervector.h"
#include "optimisations.h"
#include "psodata.h"
#include "tree.h"
#include "treenode.h"
#include "visitorinit.h"

#include "atl/creator.h"
#include "bank/bank.h"
#include "parsercore/attribute.h"
#include "parsercore/streamxml.h"

#include <float.h>

PARSER_OPTIMISATIONS();

using namespace rage;

__THREAD bool parStructure::sm_SkipPostloadCallbacks = false;

void* DummyFactory() {
	return NULL;
}

parStructure* DummyGetStructure(parConstPtrToStructure) {
	return NULL;
}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
const char* parStructure::internal_GetStructureName(parStructure* structure)
{
	return structure ? structure->GetName() : "NULL";
}

parStructure* parStructure::internal_FindNamedStructure(const char* name)
{
	return PARSER.FindStructure(name);
}
#endif

parStructure::parStructure()
: m_Base(NULL)
, m_OffsetOfBase(0)
, m_ExtraAttributes(NULL)
, m_RegistrationCount(0)
, m_CachedAlign(0)
{
	m_Factory.Bind(&DummyFactory);
	// Leave m_PlacementFactory unbound
	m_GetStructure.Bind(&DummyGetStructure);
}

parStructure::~parStructure()
{
	for(int i = 0; i < m_Members.GetCount(); i++)
	{
		delete m_Members[i];
	}

    for(atStringMap<parDelegateHolderBase*>::Iterator it = m_Delegates.Begin(); it != m_Delegates.End(); ++it)
    {
        delete *it;
    }

	delete m_ExtraAttributes;
}

void parStructure::SetBaseClass(parStructure* baseMetadata, int baseOffset)
{
	m_Base = baseMetadata;
	m_OffsetOfBase = baseOffset;
}

void parStructure::SetMaxMembers(int max)
{
	m_Members.Reserve(max);
}

void parStructure::AddMember(parMember* member)
{
#if __DEV
	// Make sure that there are no hash collisions with any members in this class or in any base class
	parStructure* currentStructure = this;

	u32 literalHash = member->GetNameHash();

	while(currentStructure)
	{
		for(int i = 0; i < currentStructure->GetNumMembers(); i++)
		{
			parMember* otherMember = currentStructure->GetMember(i);

			parAssertf(literalHash != otherMember->GetNameHash(), "Two members of a structure hash to the same value(%x). One is %s::%s, the other is %s::%s", 
				literalHash,
				GetName(), member->GetName(),
				currentStructure->GetName(), otherMember->GetName());
		}

		currentStructure = currentStructure->GetBaseStructure();
	}
#endif

	m_Members.Append() = member;
}

void parStructure::BuildStructureFromStaticData(parStructureStaticData& data)
{
	bool addStrings = PARSER.m_IsAutoregistering;
	bool hasNames = data.m_MemberNames != NULL;

	m_NameHash.SetHash(data.m_StructureNameHash);
	if (hasNames)
	{
		parAssertf(data.m_StructureName, "Couldn't find the name of the structure");
		atLiteralHashString hashStr(data.m_StructureName); // Doing this registers the hash->name pair
		parAssertf(hashStr.GetHash() == m_NameHash.GetHash(), "Hashes don't match!");

		if (addStrings)
		{
			parStreamInXml::AddStaticString(data.m_StructureName);
		}

		for(int i = 0; data.m_MemberNames[i] != NULL; i++)
		{
			// Create a literalhashstring object, so that the string gets added to the map
			atLiteralHashString lit(data.m_MemberNames[i]);
			if (addStrings)
			{
				// TODO: Get rid of this and the mininameable in parStreamInXml - use the atLiteralHashString map instead
				parStreamInXml::AddStaticString(data.m_MemberNames[i]);
			}
		}
	}

	atFixedBitSet8 bitset;
	bitset.Reset();
	bitset.Set(parStructure::HAS_VIRTUALS, data.m_IsInheritable);
	bitset.Set(parStructure::HAS_NAMES, hasNames);
	bitset.Set(parStructure::ALWAYS_HAS_NAMES, data.m_AlwaysHasNames);
	bitset.Set(parStructure::IS_CONSTRUCTIBLE, GetFlags().IsSet(parStructure::IS_CONSTRUCTIBLE)); // keep this one the way it is, since we've already called SetFactories to set this flag
	SetFlags(bitset);

	// All members required to start with (name, offset, flags, type, subtype)
	// extract type
	parMember::CommonData* const * dataTable = reinterpret_cast<parMember::CommonData* const *>(data.m_MemberData);
	// count members
	int i = 0;
	for(i = 0; dataTable[i] != NULL; i++)
	{
		// do nothing, just counting
	}
	SetMaxMembers(i);
	for(i = 0; dataTable[i] != NULL; i++)
	{
		parMember* mem = CreateMemberFromMemberData(dataTable[i]);
		if (data.m_MemberOffsets)
		{
			mem->SetOffset(data.m_MemberOffsets[i]);
		}
		AddMember(mem);
	}
}

parMember* parStructure::CreateMemberFromMemberData(parMember::CommonData* memberData)
{
	parMember* newMem = NULL;
	switch(parMemberType::GetClass((parMember::Type)memberData->m_Type))
	{
	case parMemberType::CLASS_SIMPLE:
		newMem = rage_new parMemberSimple(*(reinterpret_cast<parMemberSimple::Data*>(memberData)));
		break;
	case parMemberType::CLASS_VECTOR:
		newMem = rage_new parMemberVector(*(reinterpret_cast<parMemberVector::Data*>(memberData)));
		break;
	case parMemberType::CLASS_MATRIX:
		newMem = rage_new parMemberMatrix(*(reinterpret_cast<parMemberMatrix::Data*>(memberData)));
		break;
	case parMemberType::CLASS_STRUCT:
		newMem = rage_new parMemberStruct(*(reinterpret_cast<parMemberStruct::Data*>(memberData)));
		break;
	case parMemberType::CLASS_STRING:
		newMem = rage_new parMemberString(*(reinterpret_cast<parMemberString::Data*>(memberData)));
		break;
	case parMemberType::CLASS_ARRAY:
		newMem = rage_new parMemberArray(*(reinterpret_cast<parMemberArray::Data*>(memberData)));
		break;
	case parMemberType::CLASS_ENUM:
		newMem = rage_new parMemberEnum(*(reinterpret_cast<parMemberEnum::Data*>(memberData)));
		break;
	case parMemberType::CLASS_BITSET:
		newMem = rage_new parMemberBitset(*(reinterpret_cast<parMemberEnum::Data*>(memberData))); // Note bitsets use parMemberEnum::Data
		break;
    case parMemberType::CLASS_MAP:
        newMem = rage_new parMemberMap(*(reinterpret_cast<parMemberMap::Data*>(memberData)));
        break;
	case parMemberType::INVALID_CLASS:
		{
			Quitf(ERR_PAR_INVALID_3,"Invalid type detected");
		}
		break;
	}
	return newMem;
}

parPtrToStructure parStructure::Create()
{
	parPtrToStructure data = reinterpret_cast<parPtrToStructure>(m_Factory());
	if (!data)
	{
		parErrorf("Couldn't create an instance of type %s. Is the class abstract?", GetName());
		return NULL;
	}
	if (PARSER.Settings().GetFlag(parSettings::INITIALIZE_NEW_DATA))
	{
		PARSER.InitFromStructure(*this, data);
	}
	return data;
}

parPtrToStructure parStructure::Place(void* position)
{
	if (!m_PlacementFactory.IsBound())
	{
		parErrorf("Couldn't place an instance of type %s at 0x%p", GetName(), position);
		return NULL;
	}
	m_PlacementFactory(position);
	if (PARSER.Settings().GetFlag(parSettings::INITIALIZE_NEW_DATA))
	{
		PARSER.InitFromStructure(*this, reinterpret_cast<parPtrToStructure>(position));
	}
	return reinterpret_cast<parPtrToStructure>(position);
}

parPtrToStructure parStructure::AllocAndPlace()
{
	parPtrToStructure data = reinterpret_cast<parPtrToStructure>(rage_aligned_new(FindAlign()) char[GetSize()]);
	sysMemSet(data, 0x0, GetSize());
	return Place(data);
}

void parStructure::Destroy(parPtrToStructure structAddr)
{
	m_Destructor(structAddr);
}

void parStructure::BaseList::Init(parStructure* structMetadata, parPtrToStructure structAddr)
{
	parPtrToStructure currStructAddr = structAddr;
	parStructure* currStructMetadata = structMetadata;
	while(currStructMetadata)
	{
		m_Addresses.Append() = currStructAddr;
		m_Metadata.Append() = currStructMetadata;
		currStructAddr = currStructMetadata->GetBaseAddress(currStructAddr);
		currStructMetadata = currStructMetadata->GetBaseStructure();
	}
}

bool parStructure::internal_CheckVersionForLoad(parVersionInfo fileVersion)
{
	parVersionInfo::Status status = parVersionInfo::CompareVersions(fileVersion, m_Version);
	switch (status)
	{
	case parVersionInfo::EXACT_MATCH:
		break;
	case parVersionInfo::CODE_IS_NEWER:
	case parVersionInfo::FILE_IS_NEWER:
	case parVersionInfo::FILE_UNVERSIONED:
		if (PARSER.Settings().GetFlag(parSettings::WARN_ON_MINOR_VERSION_MISMATCH))
		{
			parWarningf("Minor version number mismatch in structure %s: File version %d.%d, code version %d.%d",
				GetName(),
				fileVersion.GetMajor(),
				fileVersion.GetMinor(),
				m_Version.GetMajor(),
				m_Version.GetMinor());
		}
		break;
	case parVersionInfo::INCOMPATABLE:
		parAssertf(0, "File and code version numbers are incompatable in structure %s. File version %d.%d, code version %d.%d",
			GetName(),
			fileVersion.GetMajor(),
			fileVersion.GetMinor(),
			m_Version.GetMajor(),
			m_Version.GetMinor());
		return false;
	}
	return true;
}

void parStructure::ReadMembersFromTree(parTreeNode* node, parPtrToStructure structAddr)
{
	if (m_Version.GetMajor() > 0)
	{
		parAttribute* versAttr = node->GetElement().FindAttribute("v");
		parVersionInfo vers;
		if (versAttr)
		{
			vers = parVersionInfo(versAttr->GetStringValue());
		}
		if (!internal_CheckVersionForLoad(vers))
		{
			return;
		}
	}

	Invoke<parTreeNode*>(structAddr, "PreLoad", node);

	// make an array of all base types, traverse the array in backwards order.
	BaseList baseInfo;
	baseInfo.Init(this, structAddr);

	parTreeNode::ChildNodeIterator startNode = node->BeginChildren();

	if (*startNode) {
		parTreeNode::ChildNodeIterator nodeListBegin = node->BeginChildren();
		parTreeNode::ChildNodeIterator nodeListEnd = node->EndChildren();

		bool checkForUnusedMembers = PARSER.Settings().GetFlag(parSettings::WARN_ON_UNUSED_DATA);

		if (checkForUnusedMembers)
		{
			for(parTreeNode::ChildNodeIterator ci = nodeListBegin; ci != nodeListEnd; ++ci)
			{
				(*ci)->GetElement().SetUserFlag(false);
			}
		}

		for(int baseNum = baseInfo.GetNumBases()-1; baseNum >= 0; baseNum--) {
			parStructure* currStruct = baseInfo.GetBaseStructure(baseNum);
			parPtrToStructure currAddr = baseInfo.GetBaseAddress(baseNum);

			// currStruct is the current structure we're trying to load.
			// Loop through each one of its members. For each member, loop through
			// each node in the linked list (starting at one past the previous node)

			for(int i = 0; i < currStruct->m_Members.GetCount(); i++)
			{
				parMember* currMember = currStruct->m_Members[i];
				bool foundMemberData = false;
				bool looped = false;
				parTreeNode::ChildNodeIterator currNode = startNode;
				while(!looped || startNode != currNode)
				{
					u32 nodeHash = atLiteralStringHash((*currNode)->GetElement().GetName());

					if (nodeHash == currMember->GetNameHash()) {
						parAssertf(parUtils::StringEquals((*currNode)->GetElement().GetName(), currMember->GetName()), "Hash collision: %s and %s hash to the same value", (*currNode)->GetElement().GetName(), currMember->GetName());
						parPtrToMember memberOffset = currMember->GetPointerToMember(currAddr);
						Invoke<parTreeNode*, parMember*, parPtrToMember>(currAddr, "PreSet", *currNode, currMember, memberOffset);
						currMember->ReadTreeNode((*currNode), currAddr);
						Invoke<parTreeNode*, parMember*, parPtrToMember>(currAddr, "PostSet", *currNode, currMember, memberOffset);

						if (checkForUnusedMembers)
						{
							(*currNode)->GetElement().SetUserFlag(true);
						}

						foundMemberData = true;
						break;
					}
					looped |= parTreeNode::FindNextNodeCircular(currNode, nodeListBegin, nodeListEnd);
				}
				parTreeNode::FindNextNodeCircular(startNode, nodeListBegin, nodeListEnd);

				if (!foundMemberData && PARSER.Settings().GetFlag(parSettings::WARN_ON_MISSING_DATA))
				{
					parWarningf("No data found for member %s in structure %s", currMember->GetName(), GetName());
				}
				if (!foundMemberData && PARSER.Settings().GetFlag(parSettings::INITIALIZE_MISSING_DATA))
				{
					parInitVisitor visitor;
					visitor.VisitMember(structAddr, *currMember);
				}
			}
		}

		if (checkForUnusedMembers)
		{
			for(parTreeNode::ChildNodeIterator ci = nodeListBegin; ci != nodeListEnd; ++ci)
			{
				if (!(*ci)->GetElement().GetUserFlag())
				{
					parWarningf("Unused data '%s' found when reading structure '%s'", (*ci)->GetElement().GetName(), GetName());
				}
			}
		}
	}

	if (!sm_SkipPostloadCallbacks)
	{
		Invoke(structAddr, "PostLoad");
	}
}


bool parStructure::IsSubclassOf(const parStructure* str) const
{
#if __DEV
	int sanity = 1000;
#endif
	const parStructure* derived = this;
	while(derived)
	{
		if (derived == str)
		{
			return true;
		}
		derived = derived->m_Base;
#if __DEV
		sanity--;
		if (!sanity)
		{
			parErrorf("Found an infinite loop in a class hierarchy, starting at %s", GetName());
			return false;
		}
#endif
	}
	return false;
}

bool parStructure::IsSubclassOf(const char* str) const
{
#if __DEV
	int sanity = 1000;
#endif
	const char* strName = str;
	if (str[0] == '_' && str[1] == '_') {
		strName = str + 2;
	}

	u32 hash = atLiteralStringHash(strName);

	const parStructure* derived = this;
	while(derived)
	{
		if (hash == derived->GetNameHash())
		{
			return true;
		}
		derived = derived->m_Base;
#if __DEV
		sanity--;
		if (!sanity)
		{
			parErrorf("Found an infinite loop in a class hierarchy, starting at %s", GetName());
			return false;
		}
#endif
	}
	return false;
}

parMember* parStructure::FindMember(atLiteralHashValue hash, bool searchBaseClasses)
{
	for(int i = 0; i < m_Members.GetCount(); i++) {
		if (hash.GetHash() == m_Members[i]->GetNameHash()) {
			return m_Members[i];
		}
	}
	if (m_Base && searchBaseClasses)
	{
		return m_Base->FindMember(hash, true);
	}
	return NULL;
}

parMember* parStructure::FindMember(const char* name, bool searchBaseClasses)
{
	return FindMember(atLiteralHashValue(name), searchBaseClasses);
}

parAttributeList* parStructure::GetOrCreateExtraAttributes()
{
	if (!m_ExtraAttributes)
	{
		m_ExtraAttributes = rage_new parAttributeList;
	}
	return m_ExtraAttributes;
}

void parStructure::LoadExtraAttributes(parTreeNode* node)
{
	parTreeNode* attrs = node->FindChildWithName("Attributes");
	parAttributeList* list = NULL;
	PARSER.LoadObjectPtr(attrs, list);

	GetOrCreateExtraAttributes();

	if (list)
	{
		m_ExtraAttributes->MergeFrom(*list);
	}
	delete list;

	parTreeNode* members = node->FindChildWithName("Members");
	if (members)
	{
		for(parTreeNode::ChildNodeIterator iter = members->BeginChildren(); iter != members->EndChildren(); ++members)
		{
			const char* name = (*iter)->GetElement().GetName();
			parMember* member = FindMember(name);
			if (member)
			{
				member->LoadExtraAttributes(*iter);
			}
		}
	}
}

bool g_BatchAddingDelegates = false;

void rage::parStructure::BeginAddingInitialDelegates( int numToAdd )
{
	FastAssert(m_Delegates.GetCount() == 0);
	m_Delegates.Reserve(numToAdd);
	g_BatchAddingDelegates = true;
}

void rage::parStructure::EndAddingInitialDelegates()
{
	g_BatchAddingDelegates = false;
	m_Delegates.FinishInsertion();
}

void rage::parStructure::AddDelegateHolder( const char* name, parDelegateHolderBase* base )
{
	if (g_BatchAddingDelegates)
	{
		m_Delegates.Insert(name, base);
	}
	else
	{
		m_Delegates.InsertSorted(name, base);
	}
}

size_t rage::parStructure::FindAlign() const
{
	if (m_CachedAlign != 0)
	{
		return m_CachedAlign;
	}

	size_t align = 1;
	if (GetFlags().IsSet(HAS_VIRTUALS))
	{
		align = __alignof(char*);
	}

	if (GetBaseStructure())
	{
		align = Max(align, GetBaseStructure()->FindAlign());
	}

	for(int i = 0; i < GetNumMembers(); i++)
	{
		const parMember* mem = GetMember(i);
		align = Max(align, mem->FindAlign());		
	}
	m_CachedAlign = (u16)align;
	return align;
}

#if PARSER_ALL_METADATA_HAS_NAMES
const char* rage::parStructure::GetName() const
{
	parAssertf(m_Flags.IsSet(HAS_NAMES), "This structure doesn't have names. If you need names for saving, use preserveNames=\"true\" in the structdef. See https://devstar.rockstargames.com/wiki/index.php/Structdef_PSC_tag");
	return m_NameHash.GetCStr();
}
#endif

const char* rage::parStructure::GetNameUnsafe() const
{
	parAssertf(m_Flags.IsSet(HAS_NAMES), "This structure doesn't have names. If you need names for saving, use preserveNames=\"true\" in the structdef. See https://devstar.rockstargames.com/wiki/index.php/Structdef_PSC_tag");
	return atLiteralHashString(GetNameHash()).GetCStr();
}

void rage::parStructure::SetName( const char* name )
{
	if (m_Flags.IsSet(HAS_NAMES))
	{
		atLiteralHashString hashStr(name); // creating an instance registers the string so we can find it later	 
		parAssertf(!PARSER.m_IsAutoregistering, "Shouldn't be setting the name twice!");
	}
	m_NameHash.SetFromString(name);
}

bool parStructure::VerifyNamesExist(bool inAllBuilds, const char* OUTPUT_ONLY(action))
{
	if (inAllBuilds)
	{
		parAssertf(GetFlags().IsSet(parStructure::ALWAYS_HAS_NAMES), "While %s: Structure %s doesn't have names in all builds. Add preserveNames=\"true\" to keep the names. See https://devstar.rockstargames.com/wiki/index.php/Structdef_PSC_tag", action, GetNameUnsafe());
	}
	if (GetFlags().IsClear(parStructure::HAS_NAMES))
	{
		parErrorf("While %s: Structure 0x%x doesn't have names", action, GetNameHash());
		return false;
	}
	return true;
}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
void parStructure::PreLoad(parTreeNode* node)
{
#if PARSER_ALL_METADATA_HAS_NAMES
	// Need to register all the names in the tree with the literalhashstring table

	const char* s = NULL;
	atLiteralHashString hs;

	if (node->FindValueFromPath("Name", s))
	{
		hs = s;
	}

	// Loop over all member vars too (names are stored as ./Members/Item[n]/Data/Name)
	parTreeNode* members = node->FindFromXPath("Members");

	for(parTreeNode::ChildNodeIterator kids = members->BeginChildren(); kids != members->EndChildren(); ++kids)
	{
		if ((*kids)->FindValueFromPath("Data/Name", s))
		{
			hs = s;
		}
	}
#endif
}

void parStructure::PostSave(parTreeNode* node)
{
	node->InsertStdLeafChild("Align", (s64)FindAlign());
#if PARSER_ALL_METADATA_HAS_NAMES
	// Add "Name" nodes to the tree so we know the real names of the members.
	// Note that I could also add atLiteralHashString as a parsable data type, but I'm the only user of it now so this
	// is more self-contained.

	node->InsertStdLeafChild("Name", GetName());

	parTreeNode* memberNodes = node->FindChildWithName("Members");
	parTreeNode* member = memberNodes->GetChild();

	for(int i = 0; i < GetNumMembers(); i++)
	{
		int nameHash;
		member->FindValueFromPath("Data/NameHash", nameHash);
		parAssertf(GetMember(i)->GetNameHash() == (u32)nameHash, "Members showed up in an unexpected order!");
		member->FindChildWithName("Data")->InsertStdLeafChild("Name", GetMember(i)->GetName(), false);
		member = member->GetSibling();
	}
#endif
}

#endif