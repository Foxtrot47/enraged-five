// 
// parser/psoschemabuilder.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOSCHEMABUILDER_H
#define PARSER_PSOSCHEMABUILDER_H

namespace rage {

struct parEnumData;
class parMember;
class parStructure;
class psoSchemaCatalog;
struct psoSchemaEnumData;
struct psoSchemaMemberData;
class psoSchemaStructure;
struct psoSchemaStructureData;

// PURPOSE: Saves a parsable object as a PSO file
// PARAMS:
//		_Type - must be a type registered with the parser
//		filename - name of the file to create
//		obj - the object to save
template<typename _Type>
bool psoSaveObject(const char* filename, const _Type* obj);


// PURPOSE: Saves an object as a PSO file
// PARAMS:
//		filename - name of the file to create
//		structure - The parStructure that describes obj
//		obj - The object to save out
bool psoSaveFromStructure(const char* filename, const parStructure& structure, parConstPtrToStructure obj, bool includeChecksum = false);

// PURPOSE: Fills out a psoSchemaMemberData object based on the contents of a parMember
void psoBuildSchemaMemberData(const parMember& mem, psoSchemaMemberData& outMemData);

// PURPOSE: Fills out a psoSchemaStructureData object based on the contents of a parStructure
void psoBuildSchemaStructData(const parStructure& str, psoSchemaStructureData& outStrData);

// PURPOSE: Fills out a psoSchemaEnumData object based on the contents of a parEnumData
void psoBuildSchemaEnumData(const parEnumData& str, psoSchemaEnumData& outStrData);

// PURPOSE: Computes and returns the 'layout hash' for a given psoSchemaStructure
u32 psoComputeLayoutHash(psoSchemaStructure& str, psoSchemaCatalog& catalog);

// PURPOSE: Computes and returns the 'layout hash' for a given parStructure
u32 psoComputeLayoutHash(parStructure& str);

//////////////////////////////////////////////////////////////////////////
// Implementations:

template<typename _Type>
bool psoSaveObject(const char* filename, const _Type* obj)
{
	const parStructure* str = PARSER.GetStructure(obj);
	FastAssert(str);
	return psoSaveFromStructure(filename, *str, reinterpret_cast<const void*>(obj));
}



} // namespace rage

#endif // PARSER_PSOSCHEMABUILDER_H

