// 
// parser/visitorxml.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "visitorxml.h"

#include "manager.h"
#include "optimisations.h"

#include "diag/output.h"
#include "math/float16.h"
#include "parsercore/streamxml.h"

#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

using namespace rage;

PARSER_OPTIMISATIONS();

rage::parXmlWriterVisitor::parXmlWriterVisitor( fiStream* stream, parSettings* settings /* = NULL */ )
: m_Indent(0)
, m_Stream(stream)
, m_OverrideName(NULL)
{
    m_KeyValue[0] = 0;
	m_Settings = settings ? *settings : PARSER.Settings();
}

void parXmlWriterVisitor::SimpleMember(parPtrToMember dataPtr, parMemberSimple& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s value=\"", metadata.GetNameUnsafe());
	switch(metadata.GetType())
	{
	case parMemberType::TYPE_BOOL:		fprintf(m_Stream, *reinterpret_cast<bool*>(dataPtr) ? "true" : "false"); break;
	case parMemberType::TYPE_CHAR:		fprintf(m_Stream, "%d", *reinterpret_cast<s8*>(dataPtr)); break;
	case parMemberType::TYPE_UCHAR:		fprintf(m_Stream, "%u", *reinterpret_cast<u8*>(dataPtr)); break;
	case parMemberType::TYPE_SHORT:		fprintf(m_Stream, "%d", *reinterpret_cast<s16*>(dataPtr)); break;
	case parMemberType::TYPE_USHORT:	fprintf(m_Stream, "%u", *reinterpret_cast<u16*>(dataPtr)); break;
	case parMemberType::TYPE_INT:		fprintf(m_Stream, "%d", *reinterpret_cast<u32*>(dataPtr)); break;
	case parMemberType::TYPE_FLOAT16:	fprintf(m_Stream, "%f", reinterpret_cast<Float16*>(dataPtr)->GetFloat32_FromFloat16()); break;
	case parMemberType::TYPE_UINT:
		if (metadata.GetSubtype() != parMemberSimpleSubType::SUBTYPE_COLOR)
		{
			fprintf(m_Stream, "%u", *reinterpret_cast<s32*>(dataPtr));
		}
		else
		{
			fprintf(m_Stream, "0x%08X", *reinterpret_cast<u32*>(dataPtr));
		}
		break;
	// http://en.wikipedia.org/wiki/Printf_format_string
	// Length can be omitted or be any of:
	// z	For integer types, causes printf to expect a size_t sized integer argument.
	// t	For integer types, causes printf to expect a ptrdiff_t sized integer argument.
	case parMemberType::TYPE_PTRDIFFT:
		fprintf(m_Stream, "%" PTRDIFFTFMT "d", *reinterpret_cast<size_t*>(dataPtr));
		break;
	case parMemberType::TYPE_SIZET:
		fprintf(m_Stream, "0x%08" SIZETFMT "X", *reinterpret_cast<size_t*>(dataPtr));
		break;
	case parMemberType::TYPE_FLOAT:		
		{
			const char* fmtString = (metadata.GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION)) ? "%#.9g" : "%f";
			fprintf(m_Stream, fmtString, *reinterpret_cast<float*>(dataPtr));
			break;
		}
	case parMemberType::TYPE_SCALARV:	
		{
			const char* fmtString = (metadata.GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION)) ? "%#.9g" : "%f";
			fprintf(m_Stream, fmtString, reinterpret_cast<ScalarV*>(dataPtr)->Getf()); 
			break;
		}
	case parMemberType::TYPE_BOOLV:		fprintf(m_Stream, reinterpret_cast<BoolV*>(dataPtr)->Getb() ? "true" : "false"); break;
	case parMemberType::TYPE_INT64:		fprintf(m_Stream, "%" I64FMT "d", *reinterpret_cast<s64*>(dataPtr)); break;
	case parMemberType::TYPE_UINT64:	fprintf(m_Stream, "0x%" I64FMT "x", *reinterpret_cast<u64*>(dataPtr)); break;
	case parMemberType::TYPE_DOUBLE:	fprintf(m_Stream, "%#.17g",  *reinterpret_cast<double*>(dataPtr)); break;
	default:
		{
			Quitf(ERR_PAR_INVALID_9,"Invalid type for simple member");
		}
	}
	fprintf(m_Stream, "\"");
    addKeyValue();
    fprintf(m_Stream, " />\r\n");
}

void parXmlWriterVisitor::VecBoolVMember(VecBoolV& data, parMemberVector& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s x=\"%s\" y=\"%s\" z=\"%s\" w=\"%s\"", metadata.GetNameUnsafe(),
		data.GetX().Getb() ? "true" : "false",
		data.GetY().Getb() ? "true" : "false",
		data.GetZ().Getb() ? "true" : "false",
		data.GetW().Getb() ? "true" : "false");
    addKeyValue();
    fprintf(m_Stream, " />\r\n");
}

void parXmlWriterVisitor::Vec2VMember(Vec2V& data, parMemberVector& metadata)
{
	Indent();
	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION));
	if (highPrec)
	{
		fprintf(m_Stream, "<%s x=\"%#.9g\" y=\"%#.9g\"", metadata.GetNameUnsafe(), data.GetXf(), data.GetYf());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s x=\"%f\" y=\"%f\"", metadata.GetNameUnsafe(), data.GetXf(), data.GetYf());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::Vec3VMember(Vec3V& data, parMemberVector& metadata)
{
	Indent();
	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION));
	if (highPrec)
	{
		fprintf(m_Stream, "<%s x=\"%#.9g\" y=\"%#.9g\" z=\"%#.9g\"", metadata.GetNameUnsafe(), data.GetXf(), data.GetYf(), data.GetZf());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s x=\"%f\" y=\"%f\" z=\"%f\"", metadata.GetNameUnsafe(), data.GetXf(), data.GetYf(), data.GetZf());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::Vec4VMember(Vec4V& data, parMemberVector& metadata)
{
	Indent();
	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION));
	if (highPrec)
	{
		fprintf(m_Stream, "<%s x=\"%#.9g\" y=\"%#.9g\" z=\"%#.9g\" w=\"%#.9g\"", metadata.GetNameUnsafe(), data.GetXf(), data.GetYf(), data.GetZf(), data.GetWf());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s x=\"%f\" y=\"%f\" z=\"%f\" w=\"%f\"", metadata.GetNameUnsafe(), data.GetXf(), data.GetYf(), data.GetZf(), data.GetWf());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::Vector2Member(Vector2& data, parMemberVector& metadata)
{
	Indent();
	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION));
	if (highPrec)
	{
		fprintf(m_Stream, "<%s x=\"%#.9g\" y=\"%#.9g\"", metadata.GetNameUnsafe(), data.x, data.y);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s x=\"%f\" y=\"%f\"", metadata.GetNameUnsafe(), data.x, data.y);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::Vector3Member(Vector3& data, parMemberVector& metadata)
{
	Indent();
	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION));
	if (highPrec)
	{
		fprintf(m_Stream, "<%s x=\"%#.9g\" y=\"%#.9g\" z=\"%#.9g\"", metadata.GetNameUnsafe(), data.x, data.y, data.z);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s x=\"%f\" y=\"%f\" z=\"%f\"", metadata.GetNameUnsafe(), data.x, data.y, data.z);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::Vector4Member(Vector4& data, parMemberVector& metadata)
{
	Indent();
	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION));
	if (highPrec)
	{
		fprintf(m_Stream, "<%s x=\"%#.9g\" y=\"%#.9g\" z=\"%#.9g\" w=\"%#.9g\"", metadata.GetNameUnsafe(), data.x, data.y, data.z, data.w);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s x=\"%f\" y=\"%f\" z=\"%f\" w=\"%f\"", metadata.GetNameUnsafe(), data.x, data.y, data.z, data.w);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::Mat33VMember(Mat33V& data, parMemberMatrix& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s content=\"matrix33\"", metadata.GetNameUnsafe());
    addKeyValue();
    fprintf(m_Stream, ">\r\n");
	m_Indent++;

	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION));
	const char* format = highPrec ? "%#.9g\t%#.9g\t%#.9g\t\r\n" : "%f\t%f\t%f\t\r\n";

	Indent();
	fprintf(m_Stream, format, data[0].GetXf(), data[1].GetXf(), data[2].GetXf());
	Indent();
	fprintf(m_Stream, format, data[0].GetYf(), data[1].GetYf(), data[2].GetYf());
	Indent();
	fprintf(m_Stream, format, data[0].GetZf(), data[1].GetZf(), data[2].GetZf());
	m_Indent--;
	Indent();
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

void parXmlWriterVisitor::Mat34VMember(Mat34V& data, parMemberMatrix& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s content=\"matrix34\"", metadata.GetNameUnsafe());
    addKeyValue();
    fprintf(m_Stream, ">\r\n");
	m_Indent++;

	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION));
	const char* format = highPrec ? "%#.9g\t%#.9g\t%#.9g\t%#.9g\t\r\n" : "%f\t%f\t%f\t%f\t\r\n";

	Indent();
	fprintf(m_Stream, format, data[0].GetXf(), data[1].GetXf(), data[2].GetXf(), data[3].GetXf());
	Indent();
	fprintf(m_Stream, format, data[0].GetYf(), data[1].GetYf(), data[2].GetYf(), data[3].GetYf());
	Indent();
	fprintf(m_Stream, format, data[0].GetZf(), data[1].GetZf(), data[2].GetZf(), data[3].GetZf());
	m_Indent--;
	Indent();
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

void parXmlWriterVisitor::Mat44VMember(Mat44V& data, parMemberMatrix& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s content=\"matrix44\"", metadata.GetNameUnsafe());
    addKeyValue();
    fprintf(m_Stream, ">\r\n");
	m_Indent++;

	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION));
	const char* format = highPrec ? "%#.9g\t%#.9g\t%#.9g\t%#.9g\t\r\n" : "%f\t%f\t%f\t%f\t\r\n";

	Indent();
	fprintf(m_Stream, format, data[0].GetXf(), data[1].GetXf(), data[2].GetXf(), data[3].GetXf());
	Indent();
	fprintf(m_Stream, format, data[0].GetYf(), data[1].GetYf(), data[2].GetYf(), data[3].GetYf());
	Indent();
	fprintf(m_Stream, format, data[0].GetZf(), data[1].GetZf(), data[2].GetZf(), data[3].GetZf());
	Indent();
	fprintf(m_Stream, format, data[0].GetWf(), data[1].GetWf(), data[2].GetWf(), data[3].GetWf());
	m_Indent--;
	Indent();
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

void parXmlWriterVisitor::Matrix34Member(Matrix34& data, parMemberMatrix& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s content=\"vector3_array\"", metadata.GetNameUnsafe());
    addKeyValue();
    fprintf(m_Stream, ">\r\n");
	m_Indent++;

	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION));
	const char* format = highPrec ? "%#.9g\t%#.9g\t%#.9g\t\r\n" : "%f\t%f\t%f\t\r\n";

	Indent();
	fprintf(m_Stream, format, data.a.x, data.a.y, data.a.z);
	Indent();
	fprintf(m_Stream, format, data.b.x, data.b.y, data.b.z);
	Indent();
	fprintf(m_Stream, format, data.c.x, data.c.y, data.c.z);
	Indent();
	fprintf(m_Stream, format, data.d.x, data.d.y, data.d.z);
	m_Indent--;
	Indent();
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

void parXmlWriterVisitor::Matrix44Member(Matrix44& data, parMemberMatrix& metadata)
{
	Indent();
	fprintf(m_Stream, "<%s content=\"vector4_array\"", metadata.GetNameUnsafe());
    addKeyValue();
    fprintf(m_Stream, ">\r\n");
	m_Indent++;

	bool highPrec = (metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION));
	const char* format = highPrec ? "%#.9g\t%#.9g\t%#.9g\t%#.9g\t\r\n" : "%f\t%f\t%f\t%f\t\r\n";

	Indent();
	fprintf(m_Stream, format, data.a.x, data.a.y, data.a.z, data.a.w);
	Indent();
	fprintf(m_Stream, format, data.b.x, data.b.y, data.b.z, data.b.w);
	Indent();
	fprintf(m_Stream, format, data.c.x, data.c.y, data.c.z, data.c.w);
	Indent();
	fprintf(m_Stream, format, data.d.x, data.d.y, data.d.z, data.d.w);
	m_Indent--;
	Indent();
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

void parXmlWriterVisitor::EnumMember(int& data, parMemberEnum& metadata)
{
	if (!metadata.GetEnum()->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "writing XML"))
	{
		return;
	}

	Indent();
	const char* valName = metadata.NameFromValueUnsafe(data);
	if(valName)
	{
		fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, ">%s</%s>\r\n", valName, metadata.GetNameUnsafe());
	}
	else
	{
		fprintf(m_Stream, "<%s value=\"%d\"", metadata.GetNameUnsafe(), data);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
}

void parXmlWriterVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata)
{
	if (metadata.AreBitsNamed() && !metadata.GetEnum()->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "writing XML"))
	{
		return;
	}

	// There's a better way to do this, but for now just generate the string value.

	using namespace parMemberBitsetSubType;

	Indent();
	fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
    addKeyValue();
	if (!metadata.IsFixedSize())
	{
		fprintf(m_Stream, " bits=\"%d\"", numBits);
	}

	parStreamOut::WriteDataOptions options;
	options.m_ValuesPerLine = 4;
	options.m_Hexadecimal = true;

	int blocks = metadata.GetNumBlocks(m_ContainingStructureAddress);
	if (blocks == 0)
	{
		// special case - empty bitset
		fprintf(m_Stream, " />\r\n");
		return;
	}

	atString stringData;
	parMemberBitset::StringReprStatus status = metadata.GenerateStringRepr(m_ContainingStructureAddress, stringData, INT_MAX, options.m_ValuesPerLine);
	if (status == parMemberBitset::SR_EMPTY)
	{
		fprintf(m_Stream, " />\r\n");
	}
	else if (status == parMemberBitset::SR_ALL_NAMED || status == parMemberBitset::SR_MIXED)
	{
		fprintf(m_Stream, ">%s</%s>\r\n", stringData.c_str(), metadata.GetNameUnsafe());
	}
	else
	{
		switch(metadata.GetSubtype())
		{
		case SUBTYPE_8BIT_FIXED:
			fprintf(m_Stream, " content=\"char_array\">");
			parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent+1, reinterpret_cast<u8*>(ptrToBits), blocks, &options);
			break;
		case SUBTYPE_16BIT_FIXED:
			fprintf(m_Stream, " content=\"short_array\">");
			parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent+1, reinterpret_cast<u16*>(ptrToBits), blocks, &options);
			break;
		case SUBTYPE_32BIT_FIXED:
		case SUBTYPE_ATBITSET:
			fprintf(m_Stream, " content=\"int_array\">");
			parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent+1, reinterpret_cast<u32*>(ptrToBits), blocks, &options);
			break;
		}
		fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
	}
}

void parXmlWriterVisitor::StringMember(parPtrToMember /*ptrToMember*/, const char* ptrToString, parMemberString& metadata)
{
	Indent();
	if (!ptrToString || ptrToString[0] == '\0') // empty string
	{
		fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else if (parUtils::IsAllWhitespace(ptrToString))
	{
		fprintf(m_Stream, "<%s content=\"ascii\"", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, ">%s</%s>\r\n", ptrToString, metadata.GetNameUnsafe());
	}
	else
	{
		fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, ">%s</%s>\r\n", ptrToString ? ptrToString : "", metadata.GetNameUnsafe());
	}
}

void parXmlWriterVisitor::WideStringMember(parPtrToMember /*ptrToMember*/, const char16* ptrToString, parMemberString& metadata)
{
	USES_CONVERSION;
	Indent();
	if (!ptrToString || ptrToString[0] == '\0')
	{
		fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
	}
	else
	{
		fprintf(m_Stream, "<%s content=\"utf16\"", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, ">%s</%s>\r\n", WIDE_TO_UTF8(ptrToString), metadata.GetNameUnsafe());
	}
}

void parXmlWriterVisitor::ExternalPointerMember(void*& data, parMemberStruct& metadata)
{
	using namespace parMemberStructSubType;
	Indent();
	const char* str = NULL;
	switch(metadata.GetSubtype())
	{
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
		if (!data && metadata.GetSubtype() != SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL)
		{
			str = "NULL";
		}
		else
		{
			str = metadata.GetData()->m_PtrToNameCB(data);
		}
		fprintf(m_Stream, "<%s ref=\"%s\"", metadata.GetNameUnsafe(), str);
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
		break;
	case SUBTYPE_POINTER:
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_STRUCTURE:
		parErrorf("Shouldn't get here");
	}
}

bool parXmlWriterVisitor::BeginStructMember(parPtrToStructure structAddr, parMemberStruct& metadata)
{
	parStructure* structureMetadata = metadata.GetConcreteStructure(structAddr);
	if (!structureMetadata->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "writing XML"))
	{
		return false;
	}

	Indent();

	parVersionInfo vers = structureMetadata->GetVersion();
	if (vers.GetMajor() > 0)
	{
		fprintf(m_Stream, "<%s v=\"%d.%d\"", metadata.GetNameUnsafe(), vers.GetMajor(), vers.GetMinor());
	}
	else
	{
		fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
	}
    addKeyValue();
    fprintf(m_Stream, ">\r\n");

	m_Indent++;
	return true;
}

void parXmlWriterVisitor::EndStructMember(parPtrToStructure UNUSED_PARAM(structAddr), parMemberStruct& metadata)
{
	m_Indent--;
	Indent();
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

bool parXmlWriterVisitor::BeginToplevelStruct(parPtrToStructure /*structAddr*/, parStructure& metadata)
{
	if (!metadata.VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "writing XML"))
	{
		return false;
	}

	if (m_Settings.GetFlag(parSettings::WRITE_XML_HEADER))
	{
		fprintf(m_Stream, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n\r\n");
	}
	if (m_OverrideName)
	{
		fprintf(m_Stream, "<%s type=\"%s\"", m_OverrideName, metadata.GetNameUnsafe());
	}
	else
	{
		fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
	}
	parVersionInfo vers = metadata.GetVersion();
	if (vers.GetMajor() > 0)
	{
		fprintf(m_Stream, " v=\"%d.%d\"", vers.GetMajor(), vers.GetMinor());
	}
    addKeyValue();
	fprintf(m_Stream, ">\r\n");
	m_Indent = 1;
	return true;
}

void parXmlWriterVisitor::EndToplevelStruct(parPtrToStructure UNUSED_PARAM(structAddr), parStructure& metadata)
{
	fprintf(m_Stream, "</%s>", metadata.GetNameUnsafe());
	m_Indent = 0;
}

bool parXmlWriterVisitor::BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
{
	Indent();
	if (ptrRef == NULL)
	{
		fprintf(m_Stream, "<%s type=\"NULL\"", metadata.GetNameUnsafe());
        addKeyValue();
        fprintf(m_Stream, " />\r\n");
		m_Indent++;
		return false;
	}

	fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());

	parStructure* structMd = metadata.GetConcreteStructure(ptrRef);

	if (!structMd->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "writing XML"))
	{
		return false;
	}

	if (metadata.CanDerive())
	{
		fprintf(m_Stream, " type=\"%s\"", structMd->GetNameUnsafe());
	}

	parVersionInfo vers = structMd->GetVersion();
	if (vers.GetMajor() > 0)
	{
		fprintf(m_Stream, " v=\"%d.%d\"", vers.GetMajor(), vers.GetMinor());
	}

    addKeyValue();
	fprintf(m_Stream, ">\r\n");

	m_Indent++;
	return true;
}

void parXmlWriterVisitor::EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
{
	m_Indent--;
	if (ptrRef != NULL)
	{
		Indent();
		fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
	}
}

bool parXmlWriterVisitor::BeginArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
{
	bool recurse = false;

	Indent();

	m_Indent++;

	parMember* proto = metadata.GetPrototypeMember();
	parStreamOut::WriteDataOptions options;

	parMember::Type protoType = proto->GetType();
	if (protoType == parMemberType::TYPE_UCHAR ||
		protoType == parMemberType::TYPE_USHORT ||
		protoType == parMemberType::TYPE_UINT ||
		protoType == parMemberType::TYPE_UINT64)
	{
		options.m_Unsigned = true;
	}
	options.m_HighPrecision = proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION);

	if (metadata.GetExtraAttributes())
	{
		int valuesPerLine = metadata.GetExtraAttributes()->FindAttributeIntValue("xValuesPerLine", 1);
		parAssertf(1 <= valuesPerLine && valuesPerLine <= UCHAR_MAX, "Invalid value for xValuesPerLine: %d", valuesPerLine);
		options.m_ValuesPerLine = (u8)valuesPerLine;
	}

	fprintf(m_Stream, "<%s", metadata.GetNameUnsafe());
	switch(proto->GetType())
	{
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_UCHAR:
		fprintf(m_Stream, " content=\"char_array\""); break;
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_USHORT:
		fprintf(m_Stream, " content=\"short_array\""); break;
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
		fprintf(m_Stream, " content=\"int_array\""); break;
	case parMemberType::TYPE_FLOAT:
	case parMemberType::TYPE_FLOAT16:
		fprintf(m_Stream, " content=\"float_array\""); break;
	case parMemberType::TYPE_VEC2V:
		fprintf(m_Stream, " content=\"vec2v_array\""); break;
	case parMemberType::TYPE_VECTOR2:
		fprintf(m_Stream, " content=\"vector2_array\""); break;
	case parMemberType::TYPE_VECTOR3:
	case parMemberType::TYPE_VEC3V:
		fprintf(m_Stream, " content=\"vector3_array\""); break;
	case parMemberType::TYPE_VECTOR4:
	case parMemberType::TYPE_VEC4V:
		fprintf(m_Stream, " content=\"vector4_array\""); break;
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
		fprintf(m_Stream, " content=\"int64_array\""); break;
	case parMemberType::TYPE_DOUBLE:
		fprintf(m_Stream, " content=\"double_array\""); break;
	default:
		break;
	}
	addKeyValue();

	if (!arrayContents || numElements == 0)
	{
		fprintf(m_Stream, " />\r\n");

		return false;
	}

	fprintf(m_Stream, ">");

	switch(proto->GetType())
	{
	case parMemberType::TYPE_CHAR:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<char*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_UCHAR:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<unsigned char*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_SHORT:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<short*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_USHORT:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<unsigned short*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_INT:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<int*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_INT64:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<s64*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_UINT:
		if (proto->AsSimpleRef().GetSubtype() == parMemberSimpleSubType::SUBTYPE_COLOR)
		{
			options.m_Hexadecimal = true;
		}
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<unsigned int*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_UINT64:
		parStreamOutXml::WriteIntArrayString(m_Stream, m_Indent, reinterpret_cast<u64*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_FLOAT:
		parStreamOutXml::WriteFloatArrayString(m_Stream, m_Indent, reinterpret_cast<float*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_DOUBLE:
		parStreamOutXml::WriteFloatArrayString(m_Stream, m_Indent, reinterpret_cast<double*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_FLOAT16:
		parStreamOutXml::WriteFloat16ArrayString(m_Stream, m_Indent, reinterpret_cast<Float16*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_VEC2V:
		parStreamOutXml::WriteVec2VArrayString(m_Stream, m_Indent, reinterpret_cast<Vec2V*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_VECTOR2:
		parStreamOutXml::WriteVector2ArrayString(m_Stream, m_Indent, reinterpret_cast<Vector2*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_VECTOR3:
	case parMemberType::TYPE_VEC3V:
		parStreamOutXml::WriteVector3ArrayString(m_Stream, m_Indent, vector_cast<Vector3*>(arrayContents), numElements, &options);
		break;
	case parMemberType::TYPE_VECTOR4:
	case parMemberType::TYPE_VEC4V:
		parStreamOutXml::WriteVector4ArrayString(m_Stream, m_Indent, vector_cast<Vector4*>(arrayContents), numElements, &options);
		break;
	default:
		// write each element individually
		fprintf(m_Stream, "\r\n");
		recurse = true;
		break;
	}

	return recurse;
}

void parXmlWriterVisitor::EndArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
{
	m_Indent--;

	if (!arrayContents || numElements == 0)
	{
		// already closed the tag
		return;
	}
	switch(metadata.GetPrototypeMember()->GetType())
	{
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_UCHAR:
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_USHORT:
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
	case parMemberType::TYPE_FLOAT:
	case parMemberType::TYPE_FLOAT16:
	case parMemberType::TYPE_VECTOR2:
	case parMemberType::TYPE_VECTOR3:
	case parMemberType::TYPE_VECTOR4:
	case parMemberType::TYPE_VEC2V:
	case parMemberType::TYPE_VEC3V:
	case parMemberType::TYPE_VEC4V:
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
	case parMemberType::TYPE_DOUBLE:
		// Write*Array already indents for us
		break;
	default:
		Indent();
		break;
	}
	fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

bool parXmlWriterVisitor::BeginMapMember (parPtrToStructure /*structAddr*/,	parMemberMap& metadata)
{
    bool recurse = true;
    Indent();
    m_Indent++;

    fprintf(m_Stream, "<%s",metadata.GetNameUnsafe());
    addKeyValue();
    fprintf(m_Stream, " >\r\n");
    return recurse;
}

void parXmlWriterVisitor::EndMapMember (parPtrToStructure /*structAddr*/, parMemberMap& metadata)
{
    m_Indent--;
    Indent();
    fprintf(m_Stream, "</%s>\r\n", metadata.GetNameUnsafe());
}

void parXmlWriterVisitor::VisitMapMember(parPtrToStructure /*structAddr*/, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata)
{
    m_KeyValue[0] = 0;

    //Figure out and store the key value
    switch (metadata.GetKeyMember()->GetType())
    {
        //These are the limited supported types for key values
    case parMemberType::TYPE_CHAR:
        {
            s8 value = *((s8*)mapKeyAddress);
            sprintf(m_KeyValue,"%d", value);
        }
        break;
    case parMemberType::TYPE_UCHAR:
        {
            u8 value = *((u8*)mapKeyAddress);
            sprintf(m_KeyValue,"%u", value);
        }
        break;
    case parMemberType::TYPE_SHORT:
        {
            s16 value = *((s16*)mapKeyAddress);
            sprintf(m_KeyValue,"%d", value);
        }
        break;
    case parMemberType::TYPE_USHORT:
        {
            u16 value = *((u16*)mapKeyAddress);
            sprintf(m_KeyValue,"%u", value);
        }
        break;
    case parMemberType::TYPE_INT:
        {
            s32 value = *((s32*)mapKeyAddress);
            sprintf(m_KeyValue,"%d", value);
        }
        break;
    case parMemberType::TYPE_UINT:
        {
            int value = *((int*)mapKeyAddress);
            sprintf(m_KeyValue,"%u", value);
        }
        break;
	case parMemberType::TYPE_PTRDIFFT:
		{
			ptrdiff_t value = *((ptrdiff_t*)mapKeyAddress);
			sprintf(m_KeyValue,"%" PTRDIFFTFMT "d", value);
		}
		break;
	case parMemberType::TYPE_SIZET:
		{
			size_t value = *((size_t*)mapKeyAddress);
			sprintf(m_KeyValue,"%" SIZETFMT "u", value);
		}
		break;
	case parMemberType::TYPE_INT64:
		{
			ptrdiff_t value = *((ptrdiff_t*)mapKeyAddress);
			sprintf(m_KeyValue,"%" I64FMT "d", value);
		}
		break;
	case parMemberType::TYPE_UINT64:
		{
			size_t value = *((size_t*)mapKeyAddress);
			sprintf(m_KeyValue,"0x%" I64FMT "x", value);
		}
		break;
	case parMemberType::TYPE_ENUM:
		{
			parMemberEnum& enumParMember = metadata.GetKeyMember()->AsEnumRef();
			int value = enumParMember.GetValue(mapKeyAddress);
			if (enumParMember.GetEnum()->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "writing XML"))
			{
				safecpy(m_KeyValue, enumParMember.NameFromValueUnsafe(value));
			}
			else
			{
				formatf(m_KeyValue, "%d", value);
			}
		}
		break;
    case parMemberType::TYPE_STRING:
        {
            parMemberString& strParMember = metadata.GetKeyMember()->AsStringRef();
            switch (strParMember.GetSubtype())
            {
#if !__FINAL
            case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
                sprintf(m_KeyValue,"%s", ((atHashString*)mapKeyAddress)->GetCStr());
                break;
#endif
			case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				sprintf(m_KeyValue,"%s", ((atFinalHashString*)mapKeyAddress)->GetCStr());
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
				sprintf(m_KeyValue,"%s", atHashStringNamespaceSupport::GetString(strParMember.GetData()->GetNamespaceIndex(), *((u32*)mapKeyAddress)));
				break;
			case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				parAssertf(0, "Can't create an XML tree with a map (%s) that contains atHashValue keys", metadata.GetName());
				break;
			case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				parAssertf(0, "Can't create an XML tree with a map (%s) that contains atPartialHashValue keys", metadata.GetName());
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
				parAssertf(0, "Can't create an XML tree with a map (%s) that contains %s keys", metadata.GetName(), atHashStringNamespaceSupport::GetNamespaceValueName(strParMember.GetData()->GetNamespaceIndex()));
				break;
            default:
                parAssertf(0,"Unsupported key string type %d being used for map %s", strParMember.GetSubtype(), metadata.GetName());
                break;
            };
        }
        break;
    default:
        parAssertf(0,"Unsupported key type %d being used for map %s", metadata.GetKeyMember()->GetType(), metadata.GetName());
        break;
    };

    VisitMember(mapDataAddress, *(metadata.GetDataMember()));
}

void parXmlWriterVisitor::addKeyValue()
{
    if (m_KeyValue[0])
    {
        fprintf(m_Stream, " key=\"%s\"",m_KeyValue);
        m_KeyValue[0] = 0;//after the key has been added it is no longer valid
    }
}


