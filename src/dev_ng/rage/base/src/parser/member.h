// 
// parser/member.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBER_H
#define PARSER_MEMBER_H

#include "macros.h"

#include "memberdata.h"

#include "atl/bitset.h"
#include "atl/hashstring.h"
#include "parsercore/attribute.h"
#include "parsercore/utils.h"

namespace rage {

class bkBank;
class parManager;
class parStructure;
class parTreeNode;

// PURPOSE:
//	parMember contains the runtime metadata for a member variable. There are
//	subclasses to represent POD data, strings, arrays and structures.
// NOTES:
//	All parMembers have a nested Data structure, and an m_Data member that points to
//	an instance of that structure. The reason for this is so we can create the parMember???::Data
//  object as static data, then just make a parMember instance that points to the static data.
class parMember
{
public:

	typedef parMemberType::Enum Type;
	typedef parMemberFlags::Enum Flags;
	typedef parMemberVisitorFlags::Enum VisitorFlags;
	typedef parMemberCommonData CommonData;

	parMember() : m_Data(NULL) {}

	virtual ~parMember();

	// RETURNS: true if FLAG_STATIC is set.
	bool IsStatic() const;

	// RETURNS: true if FLAG_NO_INIT is clear.
	bool CanInit() const;

	// RETURNS: true if FLAG_ADD_GROUP_WIDGET is set.
	bool CanAddGroupWidget() const;

	/*
	// RETURNS: true if FLAG_HIDE_WIDGETS is set
	bool ShouldHideWidgets() const;
	*/

	// since all parMembers are required to begin with STANDARD_PARMEMBER_DATA_CONTENTS,
	// we could do some clever casting (or unioning) and speed this up instead of using the 
	// virtual function calls. But having virtual function calls lets us dynamic_cast too...

#if PARSER_ALL_METADATA_HAS_NAMES
	// RETURNS: The name of the member. Only for builds where all structures and members have names.
	const char* GetName() const {return atLiteralHashString(GetNameHash()).GetCStr();}
#endif

	// RETURNS: The name of the member, but MAY NOT WORK if the structure doesn't have strings (only hashes)
	// NOTES: You have to check the containing structure to see if there are strings or not.
	const char* GetNameUnsafe() const {return atLiteralHashString(GetNameHash()).GetCStr();}

	// RETURNS: The hash value of the member name
	u32 GetNameHash() const {return GetCommonData()->m_NameHash;}

	// RETURNS: The byte offset into the C++ structure where the member begins.
	// NOTES: In the case of arrays, the offset for the "prototype member" that we
	// use for loading/saving can actually represent a location in memory 
	// lower than the base address. In this case GetOffset will be large and
	// the sum (base + offset) will wrap around to get the correct address.
	size_t GetOffset() const {return GetCommonData()->m_Offset;}

	// RETURNS: The flag bitset.
	atFixedBitSet16 GetFlags() const {atFixedBitSet16 ret; ret.SetBits(GetCommonData()->m_Flags); return ret;}

	// RETURNS: The visit flag bitset
	atFixedBitSet16 GetVisitFlags() const {atFixedBitSet16 ret; ret.SetBits(GetCommonData()->m_VisitFlags); return ret;}

	// RETURNS: The type flags. These are flags whose use is determined by the type
	// (kind of like subtype, but with flags instead of seperate subtypes)
	atFixedBitSet16 GetTypeFlags() const {atFixedBitSet16 ret; ret.SetBits(GetCommonData()->m_TypeFlags); return ret;}

	// RETURNS: The type of this member.
	// NOTES: There is no GetSubtype() function here. You must have an instance of a
	//	subclass of parMember to be able to use GetSubtype();
	Type GetType() const {return (Type)GetCommonData()->m_Type;}

	// RETURNS: The extra attribute list
	parAttributeList*&	GetExtraAttributes() {return GetCommonData()->m_ExtraAttributes;}

	// PURPOSE: Initializes the extra attribute list if it doesn't already exist
	void InitExtraAttributes();

	// PURPOSE: Given a tree node, read the data into a strucutre.
	// PARAMS:
	//		node - The node representing an object.
	//		containingStructureAddr - A pointer to the containing object. In other words, this member should begin at base + GetOffset()
	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure containingStructureAddr) const = 0;

	// PURPOSE: Loads extra attributes from a parTreeNode
	virtual void LoadExtraAttributes(parTreeNode* node);

	// PURPOSE: Gets the size of the member variable
	// NOTES: Basically, if you had an array of this member, how much space would each array element take up.
	// It does not include referenced data (e.g. GetSize() on a parMemberString representing a char* is 4, it doesn't include
	// the string itself)
	virtual size_t GetSize() const {return 0;}

	// PURPOSE: Finds the alignment of a member variable (unlike size - this may need to be computed)
	// NOTES: Default implementation returns the alignment for POD types - or 0 if alignment isn't known based just on the type.
	virtual size_t FindAlign() const { return parMemberType::GetAlign(GetType()); }

	// PURPOSE: Gets a reference to the member variable given a pointer to a structure that contains the member variable.
	// Primarily for internal use. Casts (base + GetOffset()) to a _Type&
	// PARAMS:
	//		_Type - The type of object that this member represents.
	//		containingStructureAddr - A pointer to the containing object. In other words, this member should begin at base + GetOffset()
	template<typename _Type> _Type& GetMemberFromStruct(parPtrToStructure containingStructureAddr) const;

	// PURPOSE: Gets a reference to the member variable given a pointer to a structure that contains the member variable.
	// Primarily for internal use. Casts (base + GetOffset()) to a _Type&
	// PARAMS:
	//		_Type - The type of object that this member represents.
	//		containingStructureAddr - A pointer to the containing object. In other words, this member should begin at base + GetOffset()
	template<typename _Type> const _Type& GetMemberFromStruct(parConstPtrToStructure containingStructureAddr) const;

	CommonData* GetCommonData() {FastAssert(m_Data); return reinterpret_cast<CommonData*>(m_Data);}
	const CommonData* GetCommonData() const {FastAssert(m_Data); return reinterpret_cast<CommonData*>(m_Data);}

	// PURPOSE: Downcast functions, to convert your parMember to a more derived type. Returns NULL if the parMember isn't of the correct type
	class parMemberSimple*	AsSimple();
	class parMemberArray*	AsArray();
	class parMemberMatrix*	AsMatrix();
	class parMemberString*	AsString();
	class parMemberStruct*	AsStructOrPointer();
	class parMemberVector*	AsVector();
	class parMemberEnum*		AsEnum();
	class parMemberBitset*	AsBitset();
	class parMemberMap*		AsMap();

	const class parMemberSimple*	AsSimple()			const { return const_cast<parMember*>(this)->AsSimple(); }
	const class parMemberArray*		AsArray()			const { return const_cast<parMember*>(this)->AsArray(); }
	const class parMemberMatrix*	AsMatrix()			const { return const_cast<parMember*>(this)->AsMatrix(); }
	const class parMemberString*	AsString()			const { return const_cast<parMember*>(this)->AsString(); }
	const class parMemberStruct*	AsStructOrPointer()	const { return const_cast<parMember*>(this)->AsStructOrPointer(); }
	const class parMemberVector*	AsVector()			const { return const_cast<parMember*>(this)->AsVector(); }
	const class parMemberEnum*		AsEnum()			const { return const_cast<parMember*>(this)->AsEnum(); }
	const class parMemberBitset*	AsBitset()			const { return const_cast<parMember*>(this)->AsBitset(); }
	const class parMemberMap*		AsMap()				const { return const_cast<parMember*>(this)->AsMap(); }

	class parMemberSimple&	AsSimpleRef()			{ parMemberSimple* ptr = AsSimple(); 	FastAssert(ptr); return *ptr; }
	class parMemberArray&	AsArrayRef()			{ parMemberArray* ptr = AsArray(); 		FastAssert(ptr); return *ptr; }
	class parMemberMatrix&	AsMatrixRef()			{ parMemberMatrix* ptr = AsMatrix(); 	FastAssert(ptr); return *ptr; }
	class parMemberString&	AsStringRef()			{ parMemberString* ptr = AsString(); 	FastAssert(ptr); return *ptr; }
	class parMemberStruct&	AsStructOrPointerRef()	{ parMemberStruct* ptr = AsStructOrPointer(); FastAssert(ptr); return *ptr; }
	class parMemberVector&	AsVectorRef()			{ parMemberVector* ptr = AsVector(); 	FastAssert(ptr); return *ptr; }
	class parMemberEnum&	AsEnumRef()				{ parMemberEnum* ptr = AsEnum(); 		FastAssert(ptr); return *ptr; }
	class parMemberBitset&	AsBitsetRef()			{ parMemberBitset* ptr = AsBitset(); 	FastAssert(ptr); return *ptr; }
	class parMemberMap&		AsMapRef()				{ parMemberMap* ptr = AsMap(); 			FastAssert(ptr); return *ptr; }

	const class parMemberSimple&	AsSimpleRef()			const { return const_cast<parMember*>(this)->AsSimpleRef(); }
	const class parMemberArray&		AsArrayRef()			const { return const_cast<parMember*>(this)->AsArrayRef(); }
	const class parMemberMatrix&	AsMatrixRef()			const { return const_cast<parMember*>(this)->AsMatrixRef(); }
	const class parMemberString&	AsStringRef()			const { return const_cast<parMember*>(this)->AsStringRef(); }
	const class parMemberStruct&	AsStructOrPointerRef()	const { return const_cast<parMember*>(this)->AsStructOrPointerRef(); }
	const class parMemberVector&	AsVectorRef()			const { return const_cast<parMember*>(this)->AsVectorRef(); }
	const class parMemberEnum&		AsEnumRef()				const { return const_cast<parMember*>(this)->AsEnumRef(); }
	const class parMemberBitset&	AsBitsetRef()			const { return const_cast<parMember*>(this)->AsBitsetRef(); }
	const class parMemberMap&		AsMapRef()				const { return const_cast<parMember*>(this)->AsMapRef(); }

	// PURPOSE: Conversion routines that take a pointer to a structure and return a pointer to the member described by 'this', and 
	// vice versa, given a pointer to this member, returns a pointer to the containing structure.
	parPtrToMember GetPointerToMember(parPtrToStructure containingStructureAddr) const { return reinterpret_cast<parPtrToMember>(reinterpret_cast<char*>(containingStructureAddr) + GetOffset()); }
	parConstPtrToMember GetPointerToMember(parConstPtrToStructure containingStructureAddr) const { return reinterpret_cast<parConstPtrToMember>(reinterpret_cast<const char*>(containingStructureAddr) + GetOffset()); }

protected:

	// Each of the subclasses has a different parMemberFoo::Data that it stores here. They can all be cast to a 
	// parMember::CommonData though.
	union {
		void*	m_Data;
#if !__FINAL
		struct parMemberDebugHashContainer* m_DebugName; // This is for the debugger - it works because the name hash is the first thing in STANDARD_PARMEMBER_DATA_CONTENTS 
		struct parMemberCommonData* m_CommonData; // This is mainly for debugger support - so you don't have to cast from the void*
		struct parMemberArrayData* m_ArrayData; // Again for debugger. Not for real use
		struct parMemberStructData* m_StructData; // More debugger info. Do not use.
#endif
	};

	explicit parMember(void* data) : m_Data(data) {};

private:
	friend class parStructure;
	// PURPOSE: Sets the byte offset into the C++ structure where the member begins.
	void SetOffset(size_t off) {GetCommonData()->m_Offset = off;}


#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	public:
		void PostSave(parTreeNode* node);
	PAR_PARSABLE;
#endif
};

#define STANDARD_PARMEMBER_DATA_FUNCS_DECL(type) \
	SubType			GetSubtype() const; \
	const Data*		GetData() const;\
	Data*			GetData();\
	//END

#define STANDARD_PARMEMBER_DATA_FUNCS_DEFN(type)																\
	type::SubType	type::GetSubtype() const	{return (SubType)GetCommonData()->m_Subtype;}					\
	type::Data*		type::GetData() {FastAssert(m_Data); return reinterpret_cast<Data*>(m_Data);}				\
	const type::Data*		type::GetData() const		{FastAssert(m_Data); return reinterpret_cast<const Data*>(m_Data);}	\
	void			type::Data::StdInit() {																		\
		m_NameHash = 0;																							\
		m_Offset = 0;																							\
		m_Type = parMemberType::INVALID_TYPE;																	\
		m_Subtype = 0;																							\
		m_Flags = 0;																							\
		m_VisitFlags = 0xFFFF;																					\
		m_TypeFlags = 0;																						\
		m_ExtraAttributes = NULL;																				\
	}																											\
//END

inline bool parMember::IsStatic() const
{
	return GetFlags().IsSet(parMemberFlags::MEMBER_STATIC);
}

inline bool parMember::CanInit() const
{
	return GetFlags().IsClear(parMemberFlags::MEMBER_NO_INIT);
}

inline bool parMember::CanAddGroupWidget() const
{
	return GetFlags().IsSet(parMemberFlags::MEMBER_ADD_GROUP_WIDGET);
}
/*
inline bool parMember::ShouldHideWidgets() const
{
	return GetFlags().IsSet(MEMBER_HIDE_WIDGETS);
}
*/

template<typename _Type> 
__forceinline _Type& parMember::GetMemberFromStruct(parPtrToStructure containingStructureAddr) const
{
	return *reinterpret_cast<_Type*>(GetPointerToMember(containingStructureAddr));
}

template<typename _Type> 
__forceinline const _Type& parMember::GetMemberFromStruct(parConstPtrToStructure containingStructureAddr) const
{
	return *reinterpret_cast<const _Type*>(GetPointerToMember(containingStructureAddr));
}


} // namespace rage

#endif // PARSER_MEMBER_H
