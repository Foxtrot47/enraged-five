// 
// parser/visitorwidgets.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORWIDGETS_H
#define PARSER_VISITORWIDGETS_H

#include "visitor.h"

#if __BANK && PARSER_ALL_METADATA_HAS_NAMES

namespace rage {

class bkBank;

// PURPOSE: A visitor that iterates over a structure and creates bank widgets for all the members within
class parBuildWidgetsVisitor : public parInstanceVisitor
{
public:
	parBuildWidgetsVisitor()
	{
		m_VisitMask.Reset();
		m_VisitMask.Set(parMemberVisitorFlags::VISIT_BANK);
	}

	virtual ~parBuildWidgetsVisitor() {}

	virtual void SimpleMember			(parPtrToMember ptrToMember,	parMemberSimple& metadata);
	virtual void VectorMember			(parPtrToMember ptrToMember,	parMemberVector& metadata);
	virtual void MatrixMember			(parPtrToMember ptrToMember,	parMemberMatrix& metadata);
	virtual void EnumMember				(int& data,			parMemberEnum& metadata);
	virtual void BitsetMember			(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);
	virtual void StringMember			(parPtrToMember ptrToMember,	const char* ptrToString, parMemberString& metadata);
#if !__FINAL
	virtual void AtNonFinalHashStringMember(atHashString& stringData, parMemberString& metadata);
#endif	//	!__FINAL
	virtual void AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& metadata);
	virtual void AtHashValueMember		(atHashValue& stringData, parMemberString& metadata);
	virtual void AtPartialHashValueMember	(u32& stringData, parMemberString& metadata);
	virtual void AtNamespacedHashStringMember(atNamespacedHashStringBase& stringData, parMemberString& metadata);
	virtual void AtNamespacedHashValueMember(atNamespacedHashValueBase& stringData, parMemberString& metadata);

	virtual bool BeginStructMember		(parPtrToStructure ptrToStruct,	parMemberStruct& metadata);
	virtual void EndStructMember		(parPtrToStructure ptrToStruct,	parMemberStruct& metadata);

	virtual bool BeginToplevelStruct	(parPtrToStructure ptrToStruct,	parStructure& metadata);
	virtual void EndToplevelStruct		(parPtrToStructure ptrToStruct,	parStructure& metadata);

	virtual bool BeginArrayMember		(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);
	virtual void EndArrayMember			(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

    virtual bool BeginMapMember(parPtrToStructure structAddr, parMemberMap& metadata);
    virtual void EndMapMember(parPtrToStructure structAddr, parMemberMap& metadata);
    virtual void VisitMapMember(parPtrToStructure structAddr, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata);

	virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);
	virtual void EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);

	virtual void ExternalPointerMember	(void*& data,		parMemberStruct& metadata);

	virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata);

	bkBank*		m_CurrBank;

};

}

#endif	// __BANK

#endif
