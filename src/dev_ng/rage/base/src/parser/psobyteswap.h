// 
// parser/psobyteswap.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOBYTESWAP_H
#define PARSER_PSOBYTESWAP_H

#include "psofaketypes.h"

#include "system/endian.h"

namespace rage {
	class psoResourceData;
	class psoSchemaCatalog;
	struct psoSchemaCatalogData;
	struct psoSchemaEnumData;
	struct psoSchemaStructureData;
	struct psoStructureMapData;


	// These functions are for byteswapping the IFF format data. Byteswapping for RSC format is done in either the
	// declarestruct functions or in the resource construction
#if __BE
	inline void psoSwapToNative(psoSchemaStructureData& ) {}
	inline void psoSwapFromNative(psoSchemaStructureData& ) {}
	inline void psoSwapToNative(psoSchemaEnumData& ) {}
	inline void psoSwapFromNative(psoSchemaEnumData& ) {}
	inline void psoSwapSchemaCatalog(psoSchemaCatalogData& , bool ) {}
	inline void psoSwapStructureMap(psoStructureMapData& , bool ) {}
#else
	void psoSwapSchemaCatalog(psoSchemaCatalogData& cat, bool toNative);
	void psoSwapStructureMap(psoStructureMapData& map, bool toNative);
#endif

	void psoSwapStructArrays(char* instanceData, psoSchemaCatalog& cat, psoStructureMapData& map, bool toNative);
	void psoSwapStructArrays(psoResourceData& rsc, bool toNative);

	namespace sysEndian
	{
		template<typename _Type> inline void SwapMe(_Type&);

		template<> inline void SwapMe(psoStructId& d)
		{
			SwapMe(reinterpret_cast<u32&>(d));
		}
	}

} // namespace rage

#endif


