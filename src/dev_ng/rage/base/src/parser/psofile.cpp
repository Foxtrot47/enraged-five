// 
// parser/psofile.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psofile.h"

#include "manager.h"
#include "optimisations.h"
#include "psobuilder.h"
#include "psobyteswap.h"
#include "psoparserbuilder.h"
#include "psorscbuilder.h"
#include "psorscparserbuilder.h"
#include "structure.h"

#include "data/aes.h"
#include "diag/output.h"
#include "file/asset.h"
#include "file/stream.h"
#include "math/amath.h"
#include "paging/rscbuilder.h"
#include "profile/timebars.h"
#include "system/endian.h"
#include "system/magicnumber.h"

PARSER_OPTIMISATIONS();

PARAM(psoNoDebugStrings, "[parser] Skip registration of all the debug strings");

#define SET_RESULT(x) do { if(outResult) *outResult = (x); } while(0)

using namespace rage;

namespace rage {
#if !__NO_OUTPUT
	psoLoadStats g_psoLoadStats;
#endif
}


// Forward declarations for some reader helper functions
namespace psoFileReader
{
	void RegisterHashStrings(const char* buffer, int size);
	void RegisterDebugHashStrings(const char* buffer, int size);
#if PSO_USING_NATIVE_BIT_SIZE
	char* atou(const char *s);
	void FixupPsoMembers(psoStruct& destStructure);
	psoFile* OpenAndPrepareFileForParLoading(const char* filename, const parSettings* settingsPtr, bool& allSchemasMatch);

// psoTODO: this
//	psoLoadInPlaceResult CopyAndFixupMatchingPsoContents(psoFile* file);
//	psoLoadInPlaceResult DirectFixupMatchingPsoContents(psoFile* file);

	bool CompareSchemasWithRuntime(psoFile& file);
	void AssumeAllSchemasMatchRuntime(psoSchemaCatalog& schemas);
#endif
}


psoFile::psoFile() 
: m_Storage(NULL)
, m_ResourceData(NULL)
, m_IffInstanceData(NULL)
, m_IffInstanceDataSize(0)
, m_SchemaCatalogData(NULL)
, m_SchemaCatalogDataSize(0)
, m_StructureMapData(NULL)
, m_StructureMapDataSize(0)
, m_CheckSumData(NULL)
, m_CheckSumDataSize(0)
, m_EncryptedStringData(NULL)
, m_EncryptedStringDataSize(0)
, m_StorageDataSize(0)
{
#if !__NO_OUTPUT
	m_FileName[0] = '\0';
#endif
}

psoFile::~psoFile()
{
	if (m_Flags.IsSet(FLAG_IFF_FORMAT))
	{
		m_ResourceData->DeleteSchemaData();
		m_ResourceData->DeleteStructArrays();
		// not deleting instances here. The structarrays pointed into m_Storage, they didn't own the instance data
		delete m_ResourceData; // This was temp data, not part of storage
	}
	delete m_Storage;
}

bool psoFile::Init(psoResourceData* resourceData, bool ownsData, bool registerStrings /* = true */)
{
	if (ownsData)
	{
		m_Storage = reinterpret_cast<char*>(resourceData);
	}
	m_ResourceData = resourceData;

	if (registerStrings)
	{
		if (resourceData->m_FinalHashStrings)
		{

			char* buffer = resourceData->m_FinalHashStrings.GetPtr();
			while(*buffer)
			{
				atFinalHashString s(buffer);
				buffer += strlen(buffer) + 1;
			}
		}

#if !__FINAL
		if (resourceData->m_NonFinalHashStrings)
		{
			char* buffer = resourceData->m_NonFinalHashStrings.GetPtr();

			if (resourceData->m_Flags.IsSet(psoResourceData::ENCRYPT_NONFINAL_STRINGS))
			{
				// First 4 bytes, big endian, are the length
				u32 stringLen = 0;
				stringLen = (u8)(*buffer);
				buffer++;
				stringLen = (stringLen << 8) | ((u8)(*buffer));
				buffer++;
				stringLen = (stringLen << 8) | ((u8)(*buffer));
				buffer++;
				stringLen = (stringLen << 8) | ((u8)(*buffer));
				buffer++;

				AES aes(psoResourceData::NonfinalStringKey);
				aes.Decrypt(buffer, stringLen);
			}

			while(*buffer)
			{
				atHashString s(buffer);
				buffer += strlen(buffer) + 1;
			}
		}
#endif

		// Check the structarrays to see if there are any strings that should be hashed in there.
		for(int i = 0; i < resourceData->m_NumStructArrayEntries; i++)
		{
			atLiteralHashValue structArrayName = resourceData->m_StructArrayTable[i].m_NameHash;
			if (psoConstants::IsHashstringId(structArrayName))
			{
				atHashStringNamespaces nsIndex = (atHashStringNamespaces)(structArrayName.GetHash() - psoConstants::FIRST_HASHSTRING_ID);
				char* buffer = resourceData->m_StructArrayTable[i].m_Data.GetPtr();
				char* end = buffer + resourceData->m_StructArrayTable[i].m_Size;
				char* readPtr = buffer + 16; // First 16b are reserved for future use
				while(*buffer && readPtr < end)
				{
					u32 hash = atHashStringNamespaceSupport::ComputeHash(nsIndex, buffer);
					atHashStringNamespaceSupport::AddString(nsIndex, hash, buffer);
					buffer += strlen(buffer) + 1;
				}
			}
		}
	}

	return true;
}

bool psoFile::Init( char* memory, u32 memorySize, bool ownsData, atFixedBitSet32 flags, psoLoadFileStatus* outResult )
{
	// Assume success, set to a bad value if we hit any errors
	SET_RESULT(PSOLOAD_SUCCESS);

	if (ownsData)
	{
		m_Storage = memory;
	}

	if (!memory || memorySize == 0)
	{
		SET_RESULT(PSOLOAD_EMPTY);
		return false;
	}

	// get the child IFF chunk
	parIffFileReadIterator children(memory, memory+memorySize);

	bool foundPSCH = false;
	bool foundPMAP = false;
	bool foundPSIN = false;
	bool doneLoading = false;
	bool firstChunk = true;

	psoChecksumData* checksumDataPtr = NULL;
	psoChecksumData2* newChecksumDataPtr = NULL;

	psoSchemaCatalogData* schemaCatalogData = NULL;
	psoStructureMapData* structureMapData = NULL;
	psoStructureData* instanceData = NULL;

#if !__FINAL
	char* encryptedStringData = NULL;
	u32 encryptedStringSize = 0;
#endif

	u32 sanityCheck = 0;

	while(children.GetChunk() && !doneLoading) 
	{
		sanityCheck++;
		if (sanityCheck >= 100)
		{
			parErrorf("Found too many chunks in the file, it's probably broken");
			SET_RESULT(PSOLOAD_UNKNOWN_DATA);
			return false;
		}
		switch(children.GetMagicNumber())
		{
		case psoConstants::PSCH_MAGIC_NUMBER:
			if (!parVerifyf(!schemaCatalogData && !foundPSCH, "Already found some schema data in this file!"))
			{
				SET_RESULT(PSOLOAD_REDUNDANT_DATA);
			}
			schemaCatalogData = reinterpret_cast<psoSchemaCatalogData*>(children.GetChunk());
			m_SchemaCatalogData = children.GetChunk();
			m_SchemaCatalogDataSize = children.GetChunkSize();
			foundPSCH = true;
			break;
		case psoConstants::PMAP_MAGIC_NUMBER:
			if (!parVerifyf(!structureMapData && !foundPMAP, "Already found some map data in this file!"))
			{
				SET_RESULT(PSOLOAD_REDUNDANT_DATA);
			}
			structureMapData = reinterpret_cast<psoStructureMapData*>(children.GetChunk());
			m_StructureMapData = children.GetChunk();
			m_StructureMapDataSize = children.GetChunkSize();
			foundPMAP = true;
			break;
		case psoConstants::PSIN_MAGIC_NUMBER:
			if (!parVerifyf(!instanceData && !foundPSIN, "Already found some instance data in this file!"))
			{
				SET_RESULT(PSOLOAD_REDUNDANT_DATA);
			}
			instanceData = reinterpret_cast<psoStructureData*>(children.GetChunk());
			m_IffInstanceData = children.GetChunk();
			m_IffInstanceDataSize = children.GetChunkSize();
			foundPSIN = true;
			break;
		case psoConstants::STRS_MAGIC_NUMBER:
			// Found a table of strings - add them to the atNonFinalHashString map
			if (!flags.IsSet(NO_INIT_HASH_STRING))
			{
				psoFileReader::RegisterDebugHashStrings(children.GetChunkData(), children.GetChunkSize() - sizeof(parIffHeader));
			}
			break;
		case psoConstants::STRE_MAGIC_NUMBER:
#if !__FINAL
			// Found a table of strings - add them to the atNonFinalHashString map
			if (!flags.IsSet(NO_INIT_HASH_STRING))
			{
				encryptedStringData = children.GetChunkData();
				encryptedStringSize = children.GetChunkSize() - sizeof(parIffHeader);

				m_EncryptedStringData = encryptedStringData;
				m_EncryptedStringDataSize = encryptedStringSize;
			}
#endif
			break;
		case psoConstants::STRF_MAGIC_NUMBER:
			// Found a table of final strings - add them to the atFinalHashString map
			if (!flags.IsSet(NO_INIT_HASH_STRING))
			{
				psoFileReader::RegisterHashStrings(children.GetChunkData(), children.GetChunkSize() - sizeof(parIffHeader));
			}
			break;
		case psoConstants::PSIG_MAGIC_NUMBER:
			// Not using the signatures from the file anymore
			break;
		case psoConstants::CHKS_MAGIC_NUMBER:
			{
				if (children.GetChunkSize() == sizeof(psoChecksumData))
				{
					checksumDataPtr = (psoChecksumData*)children.GetChunk();
				}
				else if (children.GetChunkSize() == sizeof(psoChecksumData2))
				{
					newChecksumDataPtr = (psoChecksumData2*)children.GetChunk();
					checksumDataPtr = newChecksumDataPtr;
				}

				m_CheckSumData = children.GetChunk();
				m_CheckSumDataSize = children.GetChunkSize();
			}
			break;
		default:
			if (foundPMAP && foundPSIN && foundPSCH)
			{
				parDisplayf("Found unknown chunk %x, got enough info to load PSO.", children.GetMagicNumber()); 
				doneLoading = true;
			}
			else
			{
				parDebugf1("Ignoring unknown chunk %x", children.GetMagicNumber());
				if (firstChunk)
				{
					parDisplayf("Found an unknown chunk %d before any known ones, this probably isn't a PSO file. Not opening it.", children.GetMagicNumber());
					SET_RESULT(PSOLOAD_COULDNT_READ_FILE);
					doneLoading = true;
				}
				else
				{
					SET_RESULT(PSOLOAD_UNKNOWN_DATA);
				}
			}
			break;
		}
		children.NextChunk();
		firstChunk = false;
	}

	if (flags.IsSet(REQUIRE_CHECKSUM) && !checksumDataPtr)
	{
		parAssertf(0, "A checksum section was required, but it was not found!");
		SET_RESULT(PSOLOAD_CHECKSUM_MISSING);
		return false;
	}

	if(checksumDataPtr && !flags.IsSet(REQUIRE_CHECKSUM) & !flags.IsSet(IGNORE_CHECKSUM))
	{
		parAssertf(0, "Checksum section found in pso but no flags were set to ignore or require it. We should explicitly tell the loader whether to require or ignore it.");
	}

	char sourcePlatform = g_sysPlatform;
	if (newChecksumDataPtr)
	{
		sourcePlatform = newChecksumDataPtr->m_PlatformId;
	}

	if (checksumDataPtr && !flags.IsSet(IGNORE_CHECKSUM))
	{
		u32 lastOffset = children.GetEndOfLastChunkRead() ? (u32)(children.GetEndOfLastChunkRead() - memory) : memorySize;

		u32 fileChecksum = sysEndian::BtoN(checksumDataPtr->m_Checksum);
		u32 fileSize = sysEndian::BtoN(checksumDataPtr->m_Filesize);

		// Zero these out to match the state of the file when we computed the checksums
		checksumDataPtr->m_Checksum = 0;
		checksumDataPtr->m_Filesize = 0;

		if (lastOffset != fileSize)
		{
			SET_RESULT(PSOLOAD_FILESIZE_MISMATCH);
			parWarningf("Warning: Found %d bytes of IFF data in the file, but CHKS section claimed there were %d bytes", lastOffset, fileSize);
		}

		u32 amountToHash = Max(fileSize, lastOffset);
		amountToHash = Min(amountToHash, memorySize);
		u32 actualChecksum = atDataHash(memory, amountToHash, newChecksumDataPtr ? psoChecksumData2::ChecksumSalt : 0);

		// put the file back the way it was
		checksumDataPtr->m_Checksum = sysEndian::NtoB(fileChecksum);
		checksumDataPtr->m_Filesize = sysEndian::NtoB(fileSize);

		if (!parVerifyf(actualChecksum == fileChecksum, "Computed checksum 0x%08x doesn't match the checksum in the file 0x%08x", actualChecksum, fileChecksum))
		{
			SET_RESULT(PSOLOAD_CHECKSUM_MISMATCH);
			return false;
		}
	}
	
	if (checksumDataPtr)
	{
		// Use the checksum size, this is not rounded like the file size
		m_StorageDataSize = sysEndian::BtoN(checksumDataPtr->m_Filesize);
	}

	if (!parVerifyf(schemaCatalogData && structureMapData && instanceData, "Serialized object file needs schema information, a structure map, and instance data!"))
	{
		SET_RESULT(PSOLOAD_MISSING_REQUIRED_DATA);
		return false;
	}

#if !__FINAL
	// Need to do this AFTER checksum checks
	if (encryptedStringData && encryptedStringSize > 0)
	{
		AES aes(psoResourceData::NonfinalStringKey);
		aes.Decrypt(encryptedStringData, encryptedStringSize);
		psoFileReader::RegisterDebugHashStrings(encryptedStringData, encryptedStringSize);
	}
#endif


	// Swap all the schema information
	if (!flags.IsSet(NO_BYTE_SWAP_DATA))
	{
		psoSwapSchemaCatalog(*schemaCatalogData, true);
		psoSwapStructureMap(*structureMapData, true);
	}

	m_ResourceData = rage_new psoResourceData;
	m_ResourceData->m_RootObjectId = structureMapData->m_RootObject;

	psoIffToRscConversion::ConvertToNewFormat(*schemaCatalogData, *m_ResourceData, sourcePlatform);
	psoIffToRscConversion::ConvertToNewFormat(*structureMapData, instanceData, *m_ResourceData, sourcePlatform);

	// Forget all the old signatures, lets compute new ones that will match the new runtime data. Otherwise we 
	// wouldn't be able to in-place load any old-school IFF data
	GetSchemaCatalog().ComputeAllSignatures(false);

	// Now swap the instance data
	if (!flags.IsSet(NO_BYTE_SWAP_DATA) && !__BE)
	{
		psoSchemaCatalog schemaCat = GetSchemaCatalog();
		psoSwapStructArrays((char*)instanceData, schemaCat, *structureMapData, true);
	}

	m_Flags.Set(FLAG_IFF_FORMAT);

	return true;
}

bool psoFile::SaveFile(const char* filename)
{
	psoSchemaCatalogData* schemaCatalogData = reinterpret_cast<psoSchemaCatalogData*>(m_SchemaCatalogData);
	psoStructureMapData* structureMapData = reinterpret_cast<psoStructureMapData*>(m_StructureMapData);

	psoSchemaCatalog schemaCat = GetSchemaCatalog();
	psoSwapStructArrays((char*)m_IffInstanceData, schemaCat, *structureMapData, false);

	psoSwapSchemaCatalog(*schemaCatalogData, false);
	psoSwapStructureMap(*structureMapData, false);

#if !__FINAL
	// Need to do this BEFORE checksum checks
	if (m_EncryptedStringData && m_EncryptedStringDataSize > 0)
	{
		AES aes(psoResourceData::NonfinalStringKey);
		aes.Encrypt(m_EncryptedStringData, m_EncryptedStringDataSize);
	}
#endif	

	psoChecksumData* checksum = (psoChecksumData*)m_CheckSumData;

	// Zero these out to match the state of the file when we computed the checksums
	checksum->m_Checksum = 0;
	checksum->m_Filesize = 0;

	u32 sum = atDataHash(m_Storage, m_StorageDataSize, psoChecksumData2::ChecksumSalt);
	checksum->m_Checksum = sysEndian::NtoB(sum);
	checksum->m_Filesize = sysEndian::NtoB(m_StorageDataSize);

	rage::fiStream* f(ASSET.Create(filename, ""));

	if(!f)
	{
		return false;
	}

	f->Write((char*)m_Storage, m_StorageDataSize);
	f->Close();

	return true;
}

psoFile* rage::psoLoadFile(const char* filename, psoLoadAction loadAction, atFixedBitSet32 initFlags, psoLoadFileStatus* outResult)
{
	SET_RESULT(PSOLOAD_SUCCESS);

	char* storage = NULL;
	int size = 0;

	bool allocatedData = parUtils::MemoryMapFile(filename, storage, size);

	if (!storage)
	{
		SET_RESULT(PSOLOAD_COULDNT_READ_FILE);
		return NULL;
	}

	psoFile* objFile = psoLoadFileFromBuffer(storage, size, filename, loadAction, allocatedData, initFlags, outResult);

	return objFile;
}

psoFile* rage::psoLoadFileFromBuffer(char* storage, int size, const char* OUTPUT_ONLY(debugName),
									 psoLoadAction	loadAction /* = PSO_PREP_FOR_PARSER_LOADING */, 
									 bool psoOwnsData /* = false */, atFixedBitSet32 initFlags /* = 0 */,
									 psoLoadFileStatus* outResult /* = NULL */)
{
	SET_RESULT(PSOLOAD_SUCCESS);

	psoFile* objFile = rage_new psoFile;
#if !__NO_OUTPUT
	safecpy(objFile->m_FileName, debugName);
#endif
	if (!objFile->Init(storage, size, psoOwnsData, initFlags, outResult))
	{
		delete objFile;
		return NULL;
	}
	
	if (loadAction == PSOLOAD_PREP_FOR_PARSER_LOADING)
	{
		bool schemasMatch = psoFileReader::CompareSchemasWithRuntime(*objFile);

		objFile->GetFlagsRef().Set(psoFile::FLAG_COMPARED_SCHEMAS_WITH_RUNTIME, true);
		objFile->GetFlagsRef().Set(psoFile::FLAG_ALL_SCHEMAS_MATCH, schemasMatch);
	}
	else if (loadAction == PSOLOAD_ASSUME_IN_PLACE_LOADING)
	{
		psoSchemaCatalog cat = objFile->GetSchemaCatalog();
		psoFileReader::AssumeAllSchemasMatchRuntime(cat);
		objFile->GetFlagsRef().Set(psoFile::FLAG_COMPARED_SCHEMAS_WITH_RUNTIME, true);
		objFile->GetFlagsRef().Set(psoFile::FLAG_ALL_SCHEMAS_MATCH, true);
	}

	return objFile;
}

psoFile* rage::psoLoadResource(const char* filename, const char* ext, psoLoadAction loadAction /* = PSOLOAD_PREP_FOR_PARSER_LOADING */, atFixedBitSet32 initFlags /* = atFixedBitSet32 */, psoLoadFileStatus* outResult /* = NULL */)
{
	SET_RESULT(PSOLOAD_SUCCESS);

	psoResourceData* resource;
	ASSERT_ONLY(sysMemStreamingCount++);
	pgRscBuilder::Load(resource, filename, ext, psoResourceData::RESOURCE_VERSION);
	ASSERT_ONLY(sysMemStreamingCount--);

	if (resource)
	{
		return psoInitFromResource(resource, filename, loadAction, initFlags, outResult);
	}
	else
	{
		SET_RESULT(PSOLOAD_COULDNT_READ_FILE);
	}
	return NULL;
}

psoFile* rage::psoInitFromResource(psoResourceData* resource, const char* OUTPUT_ONLY(debugFilename), psoLoadAction loadAction /* = PSOLOAD_PREP_FOR_PARSER_LOADING */, atFixedBitSet32 /*initFlags*/ /* = atFixedBitSet32 */, psoLoadFileStatus* outResult /* = NULL */)
{
	SET_RESULT(PSOLOAD_SUCCESS);

	psoFile* objFile = rage_new psoFile;
#if !__NO_OUTPUT
	safecpy(objFile->m_FileName, debugFilename);
#endif

	objFile->Init(resource, false);
	objFile->GetFlagsRef().Clear(psoFile::FLAG_IFF_FORMAT);

	if (loadAction == PSOLOAD_PREP_FOR_PARSER_LOADING)
	{
		bool schemasMatch = psoFileReader::CompareSchemasWithRuntime(*objFile);
		objFile->GetFlagsRef().Set(psoFile::FLAG_COMPARED_SCHEMAS_WITH_RUNTIME, true);
		objFile->GetFlagsRef().Set(psoFile::FLAG_ALL_SCHEMAS_MATCH, schemasMatch);
	}
	else if (loadAction == PSOLOAD_ASSUME_IN_PLACE_LOADING)
	{
		psoSchemaCatalog cat = objFile->GetSchemaCatalog();
		psoFileReader::AssumeAllSchemasMatchRuntime(cat);
		objFile->GetFlagsRef().Set(psoFile::FLAG_COMPARED_SCHEMAS_WITH_RUNTIME, true);
		objFile->GetFlagsRef().Set(psoFile::FLAG_ALL_SCHEMAS_MATCH, true);
	}

	if (sysGetByteSwap(objFile->m_ResourceData->m_PlatformId))
	{
		psoSwapStructArrays(*objFile->m_ResourceData, true);
	}

	return objFile;
}

namespace psoFileReader
{

void RegisterHashStrings(const char* buffer, int size)
{
	const char* endOfBuffer = buffer + size;
	while(buffer < endOfBuffer && *buffer)
	{
		atFinalHashString s(buffer);

		buffer += strlen(buffer) + 1;
	}
}

void RegisterDebugHashStrings(const char* NOTFINAL_ONLY(buffer), int NOTFINAL_ONLY(size))
{
#if !__FINAL

#if !__BANK && !__DEV // Bank and dev builds fall over with -psoNoDebugStrings, so don't even try turning them off
	if (!PARAM_psoNoDebugStrings.Get())
#endif
	{
		const char* endOfBuffer = buffer + size;
		while(buffer < endOfBuffer && *buffer)
		{
			atNonFinalHashString s(buffer);

			buffer += strlen(buffer) + 1;
		}
	}
#endif
}

u32 FindNewRuntimeStructSignature(parStructure* runtimeStruct)
{
	// There are a couple reasons we don't support fast-loading even though the signatures may match, check them here.

	// If the runtime struct isn't constructible, we shouldn't fast-load instances of it (since fast-loading would 
	// circumvent the normal constructors and we'd end up with instances of the thing we said was unconstructible)
	if (!runtimeStruct->IsConstructible())
	{
		return 0;
	}

	// If someone has explicitly said not to load instances of this type in place...
	if (runtimeStruct->GetFlags().IsSet(parStructure::NOT_IN_PLACE_LOADABLE))
	{
		return 0;
	}

	// compute a sig, and store it.
	psoRscBuilder sigBuilder;
	psoRscBuilderStructSchema& runtimeSchemaBldr = psoBuildSchemaFromStructure(sigBuilder, *runtimeStruct);
	atLiteralHashValue name = runtimeSchemaBldr.GetName();
	sigBuilder.FinishBuilding();

	psoResourceData* rsc = sigBuilder.ConstructResourceData();
	psoSchemaCatalog cat(*rsc);

	u32 sig = cat.ComputeSignature(cat.FindStructureSchema(name), true);
	
	rsc->DeleteAll();
	delete rsc;
	return sig;
}

u32 FindOrCreateRuntimeStructSignature(parStructure* runtimeStruct)
{
	sysMemAutoUseTempMemory useTempHeap;

	// See if we've already computed a signature
	parAttributeList* extraAttrs = runtimeStruct->GetOrCreateExtraAttributes();
	parAttribute* psoSigAttr = extraAttrs->FindAttribute("psosig");
	if (psoSigAttr)
	{
		return psoSigAttr->GetIntValue();
	}

	u32 runtimeSig = FindNewRuntimeStructSignature(runtimeStruct);

	extraAttrs->AddAttribute("psosig", (s64)runtimeSig);
	return runtimeSig;
}

parStructure* FindRuntimeStruct( atLiteralHashValue hash ) 
{
	parStructure* runtimeStruct = PARSER.FindStructure(hash);
	if (!runtimeStruct)
	{
#if !__NO_OUTPUT
#if __DEV
		const char* name = atLiteralHashString::TryGetString(hash.GetHash());
		if (name)
		{
			parWarningf("Couldn't find structure definition for %s", name);
		}
		else
#endif
		{
			parWarningf("Couldn't find structure definition for hash 0x%08x", hash.GetHash());
		}
#endif
	}
	return runtimeStruct;
}

// RETURNS: true if ALL schemas in the PSO correspond to a runtime schema and have a matching layout
bool CompareSchemasWithRuntime(psoFile& file)
{
	psoSchemaCatalog schemas = file.GetSchemaCatalog();

	bool allSchemasMatch = true;

#if PARSER_ALL_METADATA_HAS_NAMES
	int numMismatches = 0;
#endif
	OUTPUT_ONLY(sysMemSet(&g_psoLoadStats, 0x0, sizeof(psoLoadStats)));

	for(psoSchemaCatalog::StructureSchemaIterator ssi = schemas.BeginSchemas(); ssi != schemas.EndSchemas(); ++ssi)
	{
		psoStructureSchema psoSchema = *ssi;
		// If the structure name is anonymous...
		if (psoConstants::IsAnonymous(psoSchema.GetNameHash()))
		{
			// Anonymous structures are going to suck to validate. Imagine we made an anon structure because someone is storing
			// at atBinaryMap<CFoo, u32> and CFoo contains an eEnum. We need to not do fast loading if the enum doesn't match,
			// which we'd normally do signature checks to confirm, but there is no runtime version of the anon structure to check with
			// psoTODO: deal with this.
			continue;
		}

		u32 psoSig = psoSchema.GetSignature();
		parAssertf(psoSig != 0, "Got a bad signature for structure 0x%08x", psoSchema.GetNameHash().GetHash());

		parStructure* runtimeStruct = FindRuntimeStruct(psoSchema.GetNameHash());
		u32 runtimeSig = 0;
		if (runtimeStruct)
		{
			runtimeSig = FindOrCreateRuntimeStructSignature(runtimeStruct);
		}

		if (runtimeSig == psoSig && runtimeSig != 0)
		{
			psoSchema.GetFlagsRef().Set(psoSchemaStructureData::FLAG_USE_FAST_LOADING);

			if (runtimeStruct && runtimeStruct->GetFlags().IsSet(parStructure::HAS_VIRTUALS))
			{
				psoSchema.GetFlagsRef().Set(psoSchemaStructureData::FLAG_NEEDS_VPTR_FIXUP);
			}

			OUTPUT_ONLY(g_psoLoadStats.m_LayoutMatchingLoads++);
		}
		else
		{
#if PARSER_ALL_METADATA_HAS_NAMES
			if (numMismatches < g_psoLoadStats.m_MismatchedStructures.GetMaxCount())
			{
				g_psoLoadStats.m_MismatchedStructures[numMismatches] = runtimeStruct;
				numMismatches++;
			}
#endif
			OUTPUT_ONLY(g_psoLoadStats.m_LayoutMismatchLoads++);

			allSchemasMatch = false;
			parDebugf2("Mismatched structure definition for %s (0x%08x) in file %s", atLiteralHashString::TryGetString(psoSchema.GetNameHash().GetHash()), psoSchema.GetNameHash().GetHash(), file.GetFileName());
		}
	}

	return allSchemasMatch;
}

void AssumeAllSchemasMatchRuntime(psoSchemaCatalog& schemas)
{
	for(psoSchemaCatalog::StructureSchemaIterator ssi = schemas.BeginSchemas(); ssi != schemas.EndSchemas(); ++ssi)
	{
		psoStructureSchema psoSchema = *ssi;

		// If the structure name is anonymous...
		if (psoConstants::IsAnonymous(psoSchema.GetNameHash()))
		{
			continue;
		}

		// we only need this to know if we need vtable fixup
		parStructure* runtimeStruct = PARSER.FindStructure(psoSchema.GetNameHash());
		parAssertf(runtimeStruct, "Hey! You're assuming a pso is in place loadable, and the structure with hash 0x%x doesn't even exist in the runtime!", psoSchema.GetNameHash().GetHash());

		psoSchema.GetFlagsRef().Set(psoSchemaStructureData::FLAG_USE_FAST_LOADING);
		if (runtimeStruct->GetFlags().IsSet(parStructure::HAS_VIRTUALS))
		{
			psoSchema.GetFlagsRef().Set(psoSchemaStructureData::FLAG_NEEDS_VPTR_FIXUP);
		}
	}
}

int hexval(int c) {
	if (c >= '0' && c <= '9')
		return c-'0';
	else if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	else
		return -1;
}

// Stolen from device_common.cpp
// Some addresses have their sign bit set, and atoi on Xenon at least will clamp!
char* atou(const char *s) {
	size_t value = 0;
	while (hexval(*s) != -1)
		value = (value * 16) + (hexval(*s++));
	return (char*) value;
}

} // namespace psoFileReader

#if PSO_USING_NATIVE_BIT_SIZE

////////////////////////////////////////
// Rules for srcParStructure ('actual type') and destParStructure ('declared type')
// 
//		src		dest	subclass?		used:
//		--------------------------------------
//		NULL	NULL	N/A				error
//		NULL	dest	N/A				warn, dest
//		src		NULL	N/A				src
//		src		dest	true			src
//		src		dest	false			warn, dest

bool rage::psoCreateAndLoadFromStructure(psoStruct& srcStruct, parStructure* destParStructure, parPtrToStructure& outObject)
{
	using namespace parMemberType;

	// Determine the right parStructure to use to load from. See the table above, the parStructure depends on the
	// actual type of object in the PSO file and the declared type of the pointer (specified by destParStructure)

	u32 srcHash = srcStruct.GetSchema().GetNameHash().GetHash();

	parStructure* srcParStructure = PARSER.FindStructure(srcHash);

	if (!srcParStructure)
	{
		if (!destParStructure)
		{
			parErrorf("Couldn't find a parStructure to use for loading. Hash was 0x%08x", srcHash);
			outObject = NULL;
			return false;
		}
		else
		{
			parWarningf("Found a hash 0x%08x in a PSO that doesn't match an existing parStructure, using declared type %s instead", srcHash, destParStructure->GetName());
			srcParStructure = destParStructure;
		}
	}
	else
	{
		if (destParStructure)
		{
			if (!srcParStructure->IsSubclassOf(destParStructure))
			{
				parWarningf("Found a hash 0x%08x for structure %s that's not a subclass of %s, using the latter type instead", srcHash, srcParStructure->GetName(), destParStructure->GetName());
				srcParStructure = destParStructure;
			}
		}
	}

	if (!srcParStructure->IsConstructible())
	{
		parErrorf("Can't load an object from the PSO file (with hash 0x%08x) into the non-constructible type %s", srcHash, srcParStructure->GetName());
		outObject = NULL;
		return false;
	}


	// Find out if we can use the fast path for loading, and if so, do it!
	if (srcStruct.GetSchema().CanUseFastLoading())
	{
		outObject = reinterpret_cast<parPtrToStructure>(rage_aligned_new(16) char[srcStruct.GetSchema().GetSize()]);

		psoStructureSchema& srcSchema = srcStruct.GetSchema();

		sysMemCpy(outObject, srcStruct.GetInstanceData(), srcSchema.GetSize());

		srcStruct.SetInstanceDataPtr(outObject);

		psoFileReader::FixupPsoMembers(srcStruct);

		return true;
	}
	else
	{
		outObject = srcParStructure->Create();
		psoLoadFromStructure(srcStruct, *srcParStructure, outObject, false); // false because we already verified the types above

		return true;
	}

}


bool rage::psoLoadFromStructure(psoFile& file, parStructure& destParStructure, parPtrToStructure destStructAddr, bool verifyTypes /* = false */)
{
	FastAssert(destStructAddr);

#if !__NO_OUTPUT
	parDebugf1("Loading file '%s' into structure '%s'", file.GetFileName(), destParStructure.GetName());
	DIAG_CONTEXT_MESSAGE("Loading file '%s' into structure '%s'", file.GetFileName(), destParStructure.GetName());
#endif
	RAGE_TIMEBARS_ONLY(pfAutoMarker marker("PSO Load: Not in place", 32));

	psoStruct s = file.GetRootInstance();
	if (!s.IsValid())
	{
		return false;
	} 
	return psoLoadFromStructure(s, destParStructure, destStructAddr, verifyTypes);
}


bool rage::psoCreateAndLoadFromStructure(psoFile& file, parStructure* destParStructure, parPtrToStructure& destStructAddr)
{
#if !__NO_OUTPUT
	if (destParStructure)
	{
		parDebugf1("Loading file '%s' into structure '%s'", file.GetFileName(), destParStructure->GetName());
		DIAG_CONTEXT_MESSAGE("Loading file '%s' into structure '%s'", file.GetFileName(), destParStructure->GetName());
	}
#endif

	psoStruct s = file.GetRootInstance();
	if (!s.IsValid())
	{
		return false;
	}
	return psoCreateAndLoadFromStructure(s, destParStructure, destStructAddr);
}

rage::psoLoadInPlaceResult::psoLoadInPlaceResult()
: m_Status(PSOLOAD_ERROR)
, m_RootObject(NULL)
, m_RootObjectType(NULL)
, m_InplaceBuffer(NULL)
, m_InplaceBufferSize(0)
{
}

psoLoadInPlaceResult rage::psoLoadObjectInPlace(psoFile& file, psoHeap inPlaceHeap /*= PSO_HEAP_CURRENT*/, psoHeap notInPlaceHeap /*= PSO_HEAP_CURRENT*/)
{
#if !__NO_OUTPUT
	DIAG_CONTEXT_MESSAGE("Loading file '%s.pso' in place", file.GetFileName());
#endif

	// psoTODO: restore fixup code
	bool allSchemasMatch = file.GetFlagsRef().IsSet(psoFile::FLAG_ALL_SCHEMAS_MATCH);

	if (allSchemasMatch)
	{
		RAGE_TIMEBARS_ONLY(pfAutoMarker marker("PSO Load: In-place", 32));

#if !__NO_OUTPUT
		parDebugf3("Fixing up file '%s'", file.GetFileName());
#endif

		psoLoadInPlaceResult result;

		if (inPlaceHeap == PSO_HEAP_DEBUG)
		{
			sysMemStartDebug();
		}
		else if (inPlaceHeap == PSO_HEAP_TEMP)
		{
			sysMemStartTemp();
		}

		if (inPlaceHeap == PSO_DONT_ALLOCATE)
		{
			result = file.DirectFixupMatchingPsoContents();
		}
		else
		{
			if (parVerifyf(file.GetFlagsRef().IsSet(psoFile::FLAG_IFF_FORMAT), "Doing in-place loading into a different heap (i.e. making a copy) isn't supported with resourced PSOs"))
			{	
				result = file.CopyAndFixupMatchingPsoContents();
			}
		}

		if (inPlaceHeap == PSO_HEAP_DEBUG)
		{
			sysMemEndDebug();
		}
		else if (inPlaceHeap == PSO_HEAP_TEMP)
		{
			sysMemEndTemp();
		}

		return result;
	}
	else
	{
		RAGE_TIMEBARS_ONLY(pfAutoMarker marker("PSO Load: In-place failed", 32));

		psoLoadInPlaceResult result;

		if (notInPlaceHeap == PSO_DONT_ALLOCATE)
		{
			result.m_Status = psoLoadInPlaceResult::PSOLOAD_ERROR;
			return result;
		}

		if (notInPlaceHeap == PSO_HEAP_DEBUG)
		{
			sysMemStartDebug();
		}
		else if (notInPlaceHeap == PSO_HEAP_TEMP)
		{
			sysMemStartTemp();
		}

		psoStruct rootSrcStruct = file.GetRootInstance();

		result.m_RootObjectType = PARSER.FindStructure(rootSrcStruct.GetSchema().GetNameHash());
		if (result.m_RootObjectType)
		{
			bool success = psoCreateAndLoadFromStructure(file, result.m_RootObjectType, result.m_RootObject);
			result.m_Status = success ? psoLoadInPlaceResult::PSOLOAD_NOT_IN_PLACE : psoLoadInPlaceResult::PSOLOAD_ERROR;
		}
		else
		{
			parErrorf("Couldn't find root level parStructure for hash 0x%x in PSO", rootSrcStruct.GetSchema().GetNameHash().GetHash());
			result.m_Status = psoLoadInPlaceResult::PSOLOAD_ERROR;
		}

		if (notInPlaceHeap == PSO_HEAP_DEBUG)
		{
			sysMemEndDebug();
		}
		else if (notInPlaceHeap == PSO_HEAP_TEMP)
		{
			sysMemEndTemp();
		}

		return result;
	}
}

#endif // PSO_USING_NATIVE_BIT_SIZE

#undef SET_RESULT


