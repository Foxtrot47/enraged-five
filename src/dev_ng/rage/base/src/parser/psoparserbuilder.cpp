// 
// parser/psoparserbuilder.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psoparserbuilder.h"

#include "psobuilder.h"
#include "member.h"
#include "memberarray.h"
#include "memberenum.h"
#include "membermap.h"
#include "memberstring.h"
#include "memberstruct.h"
#include "optimisations.h"
#include "structure.h"
#include "visitor.h"

#include "diag/output.h"
#include "file/asset.h"
#include "file/stream.h"

PARSER_OPTIMISATIONS();

#if PSO_USING_NATIVE_BIT_SIZE

using namespace rage;

psoBuilderEnumSchema& BuildSchemaFromEnum(psoBuilder& builder, parEnumData& enumData);

int AddMemberToSchema(psoBuilder& builder, psoBuilderStructSchema& schema, parMember& mem, bool isAnon, bool autoPack, bool addNewSchemas)
{
	atLiteralHashValue nameHash(isAnon ? psoConstants::FIRST_ANONYMOUS_ID : mem.GetNameHash());
	parMemberType::Enum type = mem.GetType();
	size_t offset = isAnon ? 0 : (autoPack ? (size_t)-1 : mem.GetOffset());

	switch(type)
	{
	case parMemberType::TYPE_ARRAY:
		{
			// Get the prototype member info
			parMemberArray& arrMember = mem.AsArrayRef();
			parMember* proto = arrMember.GetPrototypeMember();
			FastAssert(proto);
			// If the array has external storage, we don't need a schema for the array elements yet, we'll add those when 
			// we come to them - that way we don't add schemas for empty arrays.
			bool usesExternalStorage = arrMember.HasExternalStorage();
			int memberIndex = AddMemberToSchema(builder, schema, *proto, true, autoPack, !usesExternalStorage);
			FastAssert(memberIndex >= 0 && memberIndex < USHRT_MAX);
			FastAssert(arrMember.GetData()->m_NumElements < USHRT_MAX);
			// NOTE! For POINTER_WITH_COUNT style arrays, we don't care about m_NumElements but we do need
			// m_CountMemberOffset - as it happens these are stored in a union together, so we'll
			// just write out m_NumElements and be done with it.
			return schema.AddMemberArray(nameHash, arrMember.GetSubtype(), memberIndex, (u16)arrMember.GetData()->m_NumElements, offset, arrMember.GetAlignmentPower());
		}
	case parMemberType::TYPE_MAP:
		{
			// Get the prototype members
			parMemberMap& mapMember = mem.AsMapRef();

			parMember* keyProto = mapMember.GetKeyMember();
			FastAssert(keyProto);
			int keyIndex = AddMemberToSchema(builder, schema, *keyProto, true, autoPack, false);
			FastAssert(keyIndex >= 0 && keyIndex < USHRT_MAX);

			parMember* valueProto = mapMember.GetDataMember();
			FastAssert(valueProto);
			int valueIndex = AddMemberToSchema(builder, schema, *valueProto, true, autoPack, false);
			FastAssert(valueIndex >= 0 && valueIndex < USHRT_MAX);
	
			return schema.AddMemberMap(nameHash, mapMember.GetSubtype(), keyIndex, valueIndex, offset);
		}
	case parMemberType::TYPE_ENUM:
		{
			const parMemberEnum& enumMember = mem.AsEnumRef();

			BuildSchemaFromEnum(builder, *(enumMember.GetData()->m_EnumData));

			return schema.AddMemberEnum(nameHash, enumMember.GetSubtype(), atLiteralHashValue(enumMember.GetData()->m_EnumData->m_NameHash), offset);
		}
	case parMemberType::TYPE_STRUCT:
		{
			const parMemberStruct& structMember = mem.AsStructOrPointerRef();
			switch(structMember.GetSubtype())
			{
			case parMemberStructSubType::SUBTYPE_STRUCTURE:
				{
					if (addNewSchemas)
					{
						psoBuilderStructSchema& structSchema = psoBuildSchemaFromStructure(builder, *structMember.GetBaseStructure());
						return schema.AddMemberStruct(nameHash, structSchema, offset);
					}
					else
					{
						return schema.AddMemberStruct(nameHash, atLiteralHashValue(structMember.GetBaseStructure()->GetNameHash()), offset);
					}
				}
			case parMemberStructSubType::SUBTYPE_POINTER:
			case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
				return schema.AddMemberPointer(nameHash, structMember.GetSubtype(), offset);
			default:
				parWarningf("Couldn't add struct member with subtype %d", structMember.GetSubtype());
				return -1;
			}
		}
	case parMemberType::TYPE_BITSET:
		{
			const parMemberBitset& bitsetMember = mem.AsBitsetRef();

			atLiteralHashValue schemaName;
			int bitsetDescIdx = psoSchemaMemberData::NO_MEMBER_INDEX;
			if (bitsetMember.AreBitsNamed())
			{
				BuildSchemaFromEnum(builder, *(bitsetMember.GetData()->m_EnumData));
				schemaName = atLiteralHashValue(bitsetMember.GetData()->m_EnumData ? bitsetMember.GetData()->m_EnumData->m_NameHash : 0);
				bitsetDescIdx = schema.AddMemberEnum(atLiteralHashValue(psoConstants::FIRST_ANONYMOUS_ID), parMemberEnumSubType::SUBTYPE_32BIT, schemaName, 0);
			}

			return schema.AddMemberBitset(nameHash, bitsetMember.GetSubtype(), bitsetDescIdx, bitsetMember.GetData()->m_NumBits, offset);
		}
	case parMemberType::TYPE_STRING:
		{
			const parMemberString& stringMember = mem.AsStringRef();
			parAssertf(stringMember.GetData()->m_Length < USHRT_MAX, "Too many elements for a PSO array. See https://devstar.rockstargames.com/wiki/index.php/ParserSerializedObjectFile#Schema_.28.27PSCH.27.29_Section");
			u16 count = (u16)stringMember.GetData()->m_Length;
			u8 namespaceIndex = 0;
			if (stringMember.GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHSTRING ||
				stringMember.GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHVALUE)
			{
				namespaceIndex = (u8)stringMember.GetNamespaceIndex();
			}
			if (stringMember.GetSubtype() == parMemberStringSubType::SUBTYPE_ATHASHVALUE) // Treat hash values like they were originally hashstrings
			{
				return schema.AddMemberString(nameHash, parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING, count, offset, namespaceIndex); 
			}
			else
			{
				return schema.AddMemberString(nameHash, stringMember.GetSubtype(), count, offset, namespaceIndex);
			}

		}
	default:
		{
			return schema.AddMemberSimple(nameHash, type, mem.GetCommonData()->m_Subtype, offset);
		}
	}
}

psoBuilderStructSchema&  rage::psoBuildSchemaFromStructure(psoBuilder& builder, parStructure& str)
{
	psoBuilderStructSchema* schema = builder.FindStructSchema(atLiteralHashValue(str.GetNameHash()));
	if (schema)
	{
		return *schema;
	}

	bool autoPack = (str.GetSize() == (size_t)-1);

	schema = &builder.CreateStructSchema(atLiteralHashValue(str.GetNameHash()), str.GetSize(), str.FindAlign());

	// TODO: Fill out other data here - flags, major version

	parStructure::BaseList bases;
	bases.Init(&str, NULL);
	for(int baseIdx = bases.GetNumBases() - 1; baseIdx >= 0; baseIdx--)
	{
		parStructure* currStruct = bases.GetBaseStructure(baseIdx);

		for(int memIdx = 0; memIdx < currStruct->GetNumMembers(); memIdx++)
		{
			parMember* mem = currStruct->GetMember(memIdx);

			AddMemberToSchema(builder, *schema, *mem, false, autoPack, true);
		}
	}

	schema->FinishBuilding();

	return *schema;
}

psoBuilderEnumSchema& BuildSchemaFromEnum(psoBuilder& builder, parEnumData& enumData)
{
	psoBuilderEnumSchema* schema = builder.FindEnumSchema(atLiteralHashValue(enumData.m_NameHash));
	if (schema)
	{
		return *schema;
	}

	schema = &builder.CreateEnumSchema(atLiteralHashValue(enumData.m_NameHash));

	for(int i = 0; i < enumData.m_NumEnums; i++)
	{
		schema->AddValue(atLiteralHashValue(enumData.m_Enums[i].m_NameKey), enumData.m_Enums[i].m_Value);
	}

	schema->FinishBuilding();

	return *schema;
}



// This one iterates over all of the sub-objects in a structure graph and builds up the instance catalog
// containing a description of all of our instance data
class BuildPsoVisitor : public parInstanceVisitor
{
public:
	BuildPsoVisitor(psoBuilder& builder)
		: m_Builder(&builder)
	{
	}

	// NOTE: Must be called after VisitToplevelStructure
	psoBuilderInstance* GetToplevelInstance()
	{
		FastAssert(m_InstanceStack.GetCount() > 0);
		return m_InstanceStack[0];
	}

	virtual void VisitToplevelStructure(parPtrToStructure instanceDataPtr, parStructure& metadata)
	{
		psoBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, metadata);

		psoBuilderInstance& inst = m_Builder->AddStructInstances(schema, instanceDataPtr, 1);

		m_InstanceStack.PushAndGrow(&inst);
		m_SchemaStack.PushAndGrow(&schema);

		parInstanceVisitor::VisitToplevelStructure(instanceDataPtr, metadata);
	}

	virtual bool BeginStructMember(parPtrToStructure /*ptrToStruct*/, parMemberStruct& metadata)
	{
		psoBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, *metadata.GetBaseStructure());
		m_SchemaStack.PushAndGrow(&schema);
		return true;
	}

	virtual void EndStructMember(parPtrToStructure /*ptrToStruct*/, parMemberStruct& /*metadata*/)
	{
		m_SchemaStack.Pop();
	}

	virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
	{
		if (ptrRef)
		{
			parStructure* concreteStruct = metadata.GetConcreteStructure(ptrRef);

			psoBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, *concreteStruct);

			psoBuilderInstance& ptrTarget = m_Builder->AddStructInstances(schema, ptrRef, 1);
			m_InstanceStack.Top()->AddFixup(ptrRef, &ptrTarget);
			m_InstanceStack.PushAndGrow(&ptrTarget);
			m_SchemaStack.PushAndGrow(&schema);
		}
		else
		{
			m_InstanceStack.PushAndGrow(NULL); // push something so we pop something later
			m_SchemaStack.PushAndGrow(NULL);
		}

		return true;
	}

	virtual void EndPointerMember(parPtrToStructure& /*ptrRef*/, parMemberStruct& /*metadata*/)
	{
		m_InstanceStack.Pop();
		m_SchemaStack.Pop();
	}

	virtual bool BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
	{
		using namespace parMemberType;

		if (!metadata.HasExternalStorage())
		{
			return true; // Don't have to do anything special for these
		}

		// The 'parent' here is the thing that contains the array 'header'. The child is the thing that contains the array data.
		psoBuilderInstance* parentInstance = m_InstanceStack.Top();

		// Add a placeholder for the instance data for this array (though we may not actually use it, we need to make sure something's
		// on the stack to pop off in EndArrayMember
		psoBuilderInstance*& childInstances = m_InstanceStack.Grow();
		childInstances = NULL;

		char** addressOfPtr = NULL; // the address of the pointer, which will also be the address of the structId we'll store here
		switch(metadata.GetSubtype())
		{
		case parMemberArraySubType::SUBTYPE_POINTER:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
			addressOfPtr = reinterpret_cast<char**>(ptrToMember);
			break;
		case parMemberArraySubType::SUBTYPE_ATARRAY:
			{
				psoFakeAtArray16* fakeArray = reinterpret_cast<psoFakeAtArray16*>(ptrToMember);
				addressOfPtr = &fakeArray->m_Elements.m_Pointer;
			}
			break;
		case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
			{
				psoFakeAtArray32* fakeArray = reinterpret_cast<psoFakeAtArray32*>(ptrToMember);
				addressOfPtr = &fakeArray->m_Elements.m_Pointer;
			}
			break;
		case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
		case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
		case parMemberArraySubType::SUBTYPE_MEMBER:
			parErrorf("Shouldn't get here - has internal storage!");
			return true;

		case parMemberArraySubType::SUBTYPE_VIRTUAL:
			parErrorf("Arrays of type %d can't go in PSO files", metadata.GetSubtype());
			return false;
		}

		if (numElements == 0)
		{
			// Need to make sure we're actually pointing to NULL - not just an empty array
			parentInstance->AddFixup(*addressOfPtr, NULL);
			return false;
		}

		size_t arraySizeInBytes = numElements * metadata.GetPrototypeMember()->GetSize();

		size_t contentAlignment = metadata.FindContentAlign();

		bool visitKids = false;

		// This switch has to do a couple things...
		// 1) Add any array data to the instance catalog, and set the 'childInstances' pointer
		// 2) Add any schemas to the schema catalog
		// 3) Determine if we need to recurse into the child elements

		switch(parMemberType::GetClass(metadata.GetPrototypeMember()->GetType()))
		{
		case parMemberType::CLASS_SIMPLE:
		case parMemberType::CLASS_VECTOR:
		case parMemberType::CLASS_MATRIX:
			childInstances = &m_Builder->AddPodArray(metadata.GetPrototypeMember()->GetType(), arrayContents, arraySizeInBytes, contentAlignment);
			break;

		case parMemberType::CLASS_STRUCT:
			{
				parMemberStruct& protoMember = metadata.GetPrototypeMember()->AsStructOrPointerRef();

				switch(protoMember.GetSubtype())
				{
				case parMemberStructSubType::SUBTYPE_STRUCTURE:
					{
						psoBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, *protoMember.GetBaseStructure());
						childInstances = &m_Builder->AddStructInstances(schema, reinterpret_cast<parPtrToStructure>(arrayContents), numElements, contentAlignment);
						visitKids = true;
					}
					break;
				case parMemberStructSubType::SUBTYPE_POINTER:
				case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
					{
						// HACK - write these out as a POD array of TYPE_STRUCT for now. They're really an array of psoStructIds.
						childInstances = &m_Builder->AddPodArray(TYPE_STRUCT, arrayContents, arraySizeInBytes, contentAlignment);
						visitKids = true;
					}
					break;
				default:
					parErrorf("Not yet supported");
					break;
				}
			}
			break;

		case parMemberType::CLASS_BITSET:
			{
				parMemberBitset& protoMember = metadata.GetPrototypeMember()->AsBitsetRef();

				parMemberType::Enum podType = INVALID_TYPE;
				switch((parMemberBitsetSubType::Enum)protoMember.GetSubtype())
				{
				case parMemberBitsetSubType::SUBTYPE_32BIT_FIXED: podType = TYPE_UINT; break;
				case parMemberBitsetSubType::SUBTYPE_16BIT_FIXED: podType = TYPE_USHORT; break;
				case parMemberBitsetSubType::SUBTYPE_8BIT_FIXED: podType = TYPE_UCHAR; break;
				case parMemberBitsetSubType::SUBTYPE_ATBITSET:
					parErrorf("Not yet supported");
					break;
				}

				if (podType != parMemberType::INVALID_TYPE)
				{
					childInstances = &m_Builder->AddPodArray(podType, arrayContents, arraySizeInBytes, contentAlignment);
				}
			}
			break;

		case parMemberType::CLASS_ENUM:
			{
				parMemberEnum& protoMember = metadata.GetPrototypeMember()->AsEnumRef();

				parMemberType::Enum podType = INVALID_TYPE;
				switch((parMemberEnumSubType::Enum)protoMember.GetSubtype())
				{
				case parMemberEnumSubType::SUBTYPE_32BIT: podType = TYPE_UINT; break;
				case parMemberEnumSubType::SUBTYPE_16BIT: podType = TYPE_USHORT; break;
				case parMemberEnumSubType::SUBTYPE_8BIT: podType = TYPE_UCHAR; break;
				}

				childInstances = &m_Builder->AddPodArray(podType, arrayContents, arraySizeInBytes, contentAlignment);
			}
			break;

		case parMemberType::CLASS_STRING:
			{
				parMemberString& protoMember = metadata.GetPrototypeMember()->AsStringRef();

				switch((parMemberStringSubType::Enum)protoMember.GetSubtype())
				{
				case parMemberStringSubType::SUBTYPE_MEMBER:
					childInstances = &m_Builder->AddPodArray(TYPE_CHAR, arrayContents, arraySizeInBytes, contentAlignment);
					break;
				case parMemberStringSubType::SUBTYPE_WIDE_MEMBER:
					childInstances = &m_Builder->AddPodArray(TYPE_UCHAR, arrayContents, arraySizeInBytes, contentAlignment);
					break;
				case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
				case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
				case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
					childInstances = &m_Builder->AddPodArray(TYPE_UINT, arrayContents, arraySizeInBytes, contentAlignment);
					visitKids = true;
					break;	  
				case parMemberStringSubType::SUBTYPE_POINTER:
				case parMemberStringSubType::SUBTYPE_CONST_STRING:
				case parMemberStringSubType::SUBTYPE_WIDE_POINTER:
					// Child data is an array of pointers - which becomes an array of psoStructIds - aka TYPE_STRUCT
					childInstances = &m_Builder->AddPodArray(TYPE_STRUCT, arrayContents, arraySizeInBytes, contentAlignment);
					visitKids = true; // to add pointer fixups
					break;
				case parMemberStringSubType::SUBTYPE_ATSTRING:
					// As above, doing kind of a dodgy cast because we don't have a 'type' that means atString
					childInstances = &m_Builder->AddPodArray((parMemberType::Enum)psoConstants::TYPE_ATSTRING_IN_ARRAY, arrayContents, arraySizeInBytes, contentAlignment);
					visitKids = true;
					break;
				case parMemberStringSubType::SUBTYPE_ATWIDESTRING:
					// As above, doing kind of a dodgy cast because we don't have a 'type' that means atWideString
					childInstances = &m_Builder->AddPodArray((parMemberType::Enum)psoConstants::TYPE_ATWIDESTRING_IN_ARRAY, arrayContents, arraySizeInBytes, contentAlignment);
					visitKids = true;
					break;
				}
				break;
			}
		case parMemberType::CLASS_ARRAY:
			parErrorf("Arrays of arrays don't work in PSO files yet. Use an array of structures that contain these things instead.");
			break;
        case parMemberType::CLASS_MAP:
            parErrorf("Arrays of MAP types don't work in PSO files yet.");
            break;
		case parMemberType::INVALID_CLASS:
			parErrorf("Shouldn't get here");
			break;
		}

		// Wherever the array pointer was (addressOfPtr), replace that with a psoStructId that points to childInstances
		parentInstance->AddFixup(*addressOfPtr, childInstances);

		return visitKids;
	}

	virtual void EndArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
	{
		if (!metadata.HasExternalStorage())
		{
			return; // Don't have to do anything special for these
		}

		m_InstanceStack.Pop();
	}

	psoBuilderStructSchema* FindOrCreateAtBinaryMapElementSchema(psoSchemaMemberData& keySchemaData, psoSchemaMemberData& valueSchemaData)
	{
		// See if any of the schemas we've added already fit the bill
		for(int i = 0; i < m_AtBinaryMapStructures.GetCount(); i++)
		{
			psoBuilderStructSchema* mapBuildSchema = m_AtBinaryMapStructures[i];
			psoSchemaStructureData* mapSchema = mapBuildSchema->GetSchemaData();

			psoSchemaMemberData& schemaMember0 = mapSchema->GetMember(0);
			psoSchemaMemberData& schemaMember1 = mapSchema->GetMember(1);

			if (mapSchema->m_NumMembers == 2 && 
				keySchemaData.IsEquivalentTo(schemaMember0, false, false) &&
				valueSchemaData.IsEquivalentTo(schemaMember1, false, false))
			{
				return mapBuildSchema;
			}
		}

		// None of the existing map schemas matched, so build a new one
		psoBuilderStructSchema* mapBuildSchema = &m_Builder->CreateStructSchema(atLiteralHashValue::Null());

		// Use AddMemberRaw to copy all the data, regardless of type
		mapBuildSchema->CopyMember(keySchemaData, atLiteralHashValue("Key"));
		mapBuildSchema->CopyMember(valueSchemaData, atLiteralHashValue("Item"));
		mapBuildSchema->FinishBuilding();
		mapBuildSchema->GetSchemaData()->m_Flags |= psoSchemaStructureData::FLAG_MAP_CONTENTS;

		m_AtBinaryMapStructures.PushAndGrow(mapBuildSchema);

		return mapBuildSchema;
	}

	psoBuilderStructSchema* FindOrCreateAtMapElementSchema(psoSchemaMemberData& keySchemaData, psoSchemaMemberData& valueSchemaData)
	{
		// See if any of the schemas we've added already fit the bill
		for(int i = 0; i < m_AtMapStructures.GetCount(); i++)
		{
			psoBuilderStructSchema* mapBuildSchema = m_AtBinaryMapStructures[i];
			psoSchemaStructureData* mapSchema = mapBuildSchema->GetSchemaData();

			psoSchemaMemberData& schemaMember0 = mapSchema->GetMember(0);
			psoSchemaMemberData& schemaMember1 = mapSchema->GetMember(1);

			if (mapSchema->m_NumMembers == 3 && 
				keySchemaData.IsEquivalentTo(schemaMember0, false, false) &&
				valueSchemaData.IsEquivalentTo(schemaMember1, false, false))
			{
				return mapBuildSchema;
			}
		}

		// None of the existing map schemas matched, so build a new one
		psoBuilderStructSchema* mapBuildSchema = &m_Builder->CreateStructSchema(atLiteralHashValue::Null());

		// Use AddMemberRaw to copy all the data, regardless of type
		mapBuildSchema->CopyMember(keySchemaData, atLiteralHashValue("Key"));
		mapBuildSchema->CopyMember(valueSchemaData, atLiteralHashValue("Item"));
		mapBuildSchema->AddMemberPointer(atLiteralHashValue("next"), parMemberStructSubType::SUBTYPE_SIMPLE_POINTER, -1);
		mapBuildSchema->FinishBuilding();
		mapBuildSchema->GetSchemaData()->m_Flags |= psoSchemaStructureData::FLAG_MAP_CONTENTS;

		m_AtMapStructures.PushAndGrow(mapBuildSchema);

		return mapBuildSchema;
	}

	virtual bool BeginMapMember(parPtrToStructure structAddr, parMemberMap& metadata)
	{
		// The 'parent' here is the structure that contains the map "header". The child is the map contents.
		psoBuilderInstance* parentInstance = m_InstanceStack.Top();

		// Add a placeholder for the instance data for this map
		psoBuilderInstance*& childInstances = m_InstanceStack.Grow();
		childInstances = NULL;

		parMemberStruct* structMetadata = metadata.GetDataMember()->AsStructOrPointer();
		if (structMetadata)
		{
			if (structMetadata->GetSubtype() == parMemberStructSubType::SUBTYPE_STRUCTURE)
			{
				psoBuildSchemaFromStructure(*m_Builder, *structMetadata->GetBaseStructure());
			}
		}

		// Find this member in the PSO schema, so we can get to the key and value members
		psoBuilderStructSchema* bldSchema = m_SchemaStack.Top(); // note that this can be different from parentInstance->GetSchema(), b/c the parentInstance might be a structure that contains a structure that contains the map
		psoSchemaStructureData* schemaData = bldSchema->GetSchemaData();

		psoSchemaMemberData* mapMemberData = NULL;
		for(int i = 0; i < schemaData->m_NumMembers; i++)
		{
			psoSchemaMemberData& memberData = schemaData->GetMember(i);
			if (memberData.m_NameHash == metadata.GetNameHash())
			{
				mapMemberData = &memberData;
				break;
			}
		}

		parAssertf(mapMemberData, "Couldn't find a PSO schema for this map!");

		psoSchemaMemberData& keySchemaData = schemaData->GetMember(mapMemberData->m_KeyIndex);
		psoSchemaMemberData& valueSchemaData = schemaData->GetMember(mapMemberData->m_ValueIndex);

		switch(metadata.GetSubtype())
		{
		case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
			{
				psoBuilderStructSchema* mapElementContainerSchema = FindOrCreateAtBinaryMapElementSchema(keySchemaData, valueSchemaData);
				psoFakeAtBinMap& binmap = metadata.GetMemberFromStruct<psoFakeAtBinMap>(structAddr);
				// Add the array of map data to the PSO
				childInstances = &m_Builder->AddStructInstances(*mapElementContainerSchema, reinterpret_cast<parPtrToStructure>(binmap.m_Data.m_Elements.m_Pointer), binmap.m_Data.m_Count);
				parentInstance->AddFixup(binmap.m_Data.m_Elements.m_Pointer, childInstances);
			}
			break;
		case parMemberMapSubType::SUBTYPE_ATMAP:
			// Add an anonymous structure that contains {key, value, next}. Then add an array of those?
			// TODO: atMap support
			parErrorf("Can't add atMaps to PSO files yet (can you use atBinaryMap?)");
			return false;
		}

		return true;
	}

	virtual void EndMapMember(parPtrToStructure /*structAddr*/, parMemberMap& /*metadata*/)
	{
		m_InstanceStack.Pop();
	}

	virtual void PointerStringMember(char*& stringData, parMemberString& /*metadata*/)
	{
		psoBuilderInstance* arrContentsInfo = stringData ? &m_Builder->AddPodArray(parMemberType::TYPE_CHAR, reinterpret_cast<parPtrToArray>(stringData), strlen(stringData) + 1) : NULL;
		m_InstanceStack.Top()->AddFixup(stringData, arrContentsInfo);
	}

	virtual void ConstStringMember(ConstString& stringData, parMemberString& /*metadata*/)
	{
		psoBuilderInstance* arrContentsInfo = stringData ? &m_Builder->AddPodArray(parMemberType::TYPE_CHAR, reinterpret_cast<parConstPtrToArray>(stringData.c_str()), strlen(stringData.c_str()) + 1) : NULL;

		psoFakeConstString& fakeString = reinterpret_cast<psoFakeConstString&>(stringData);

		m_InstanceStack.Top()->AddFixup(fakeString.m_Data.m_Pointer, arrContentsInfo);
	}

#if !__FINAL
	virtual void AtNonFinalHashStringMember(atHashString& stringData, parMemberString& /*metadata*/)
	{
		m_Builder->AddExtraDebugString(stringData.GetHash(), stringData.GetCStr());
	}
#endif	//	!__FINAL

	virtual void AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& /*metadata*/)
	{
		m_Builder->AddExtraString(stringData.GetHash(), stringData.GetCStr());
	}

	virtual void WidePointerStringMember(char16*& stringData, parMemberString& /*metadata*/)
	{
		psoBuilderInstance* arrContentsInfo = stringData ? &m_Builder->AddPodArray(parMemberType::TYPE_USHORT, reinterpret_cast<parPtrToArray>(stringData), sizeof(char16) * ( (int) wcslen(stringData) + 1)) : NULL;
		m_InstanceStack.Top()->AddFixup(stringData, arrContentsInfo);
	}

	virtual void AtStringMember(atString& stringData, parMemberString& /*metadata*/)
	{
		psoFakeAtString& fakeString = reinterpret_cast<psoFakeAtString&>(stringData);
		if (stringData.GetLength())
		{
			psoBuilderInstance* arrContentsInfo = &m_Builder->AddPodArray(parMemberType::TYPE_CHAR, reinterpret_cast<parConstPtrToArray>(stringData.c_str()), stringData.GetLength()+1);
			m_InstanceStack.Top()->AddFixup(fakeString.m_Data.m_Pointer, arrContentsInfo);
		}
		else
		{
			// Need to actually point to NULL, not to an empty string
			m_InstanceStack.Top()->AddFixup(fakeString.m_Data.m_Pointer, NULL);
		}
	}

	virtual void AtWideStringMember(atWideString& stringData, parMemberString& /*metadata*/)
	{
		psoFakeAtString& fakeString = reinterpret_cast<psoFakeAtString&>(stringData);
		if (stringData.GetLength())
		{
			const char16* realStringData = stringData;
			char16* nonConstStringData = const_cast<char16*>(realStringData);
			psoBuilderInstance* arrContentsInfo = &m_Builder->AddPodArray(parMemberType::TYPE_USHORT, reinterpret_cast<parPtrToArray>(nonConstStringData), sizeof(char16) * (stringData.GetLength()+1));
			m_InstanceStack.Top()->AddFixup(fakeString.m_Data.m_Pointer, arrContentsInfo);
		}
		else
		{
			// Need to actually point to NULL, not to an empty string
			m_InstanceStack.Top()->AddFixup(fakeString.m_Data.m_Pointer, NULL);
		}
	}

	virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata)
	{
		if (metadata.GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
		{
			psoFakeAtBitset* fakeBitset = reinterpret_cast<psoFakeAtBitset*>(ptrToMember);
			if (numBits > 0)
			{
				size_t numWords = (numBits + 31) / 32;
				psoBuilderInstance* arrContentsInfo = &m_Builder->AddPodArray(parMemberType::TYPE_UINT, reinterpret_cast<parPtrToArray>(ptrToBits), numWords * sizeof(u32));
				m_InstanceStack.Top()->AddFixup(fakeBitset->m_Bits.m_Pointer, arrContentsInfo);
			}
			else
			{
				m_InstanceStack.Top()->AddFixup(fakeBitset->m_Bits.m_Pointer, NULL);
			}
		}
	}

private:
	psoBuilder* m_Builder;
	atArray<psoBuilderInstance*>	m_InstanceStack;
	atArray<psoBuilderStructSchema*>	m_SchemaStack;
	atArray<psoBuilderStructSchema*>	m_AtBinaryMapStructures;
	atArray<psoBuilderStructSchema*>	m_AtMapStructures;
};

bool rage::psoSaveFromStructure(const char* filename, const parStructure& structure, parConstPtrToStructure obj, bool includeChecksum /* = false */)
{
	psoBuilder builder;

	if (includeChecksum)
	{
		builder.IncludeChecksum();
	}

	// Creation happens in a few stages...
	// 1: Build the schema and instance catalogs
	// 2: Find all the sizes and offsets of all the member objects that will be in those catalogs
	// 3: All the objects write out data to their sections of the file
	// 4: Convert what used to be pointers in the file data into offsets or objectIDs, and byte swap to little endian

	BuildPsoVisitor psoVisitor(builder);
	psoVisitor.VisitToplevelStructure(const_cast<parPtrToStructure>(obj), const_cast<parStructure&>(structure));

	builder.SetRootObject(*psoVisitor.GetToplevelInstance());

	builder.Save(filename);

	return true;
}

psoBuilderInstance* rage::psoAddParsableObjectFromStructure(psoBuilder& builder, const parStructure& structure, parConstPtrToStructure obj)
{
	BuildPsoVisitor psoVisitor(builder);
	psoVisitor.VisitToplevelStructure(const_cast<parPtrToStructure>(obj), const_cast<parStructure&>(structure));
	
	return psoVisitor.GetToplevelInstance();
}

#endif // PSO_USING_NATIVE_BIT_SIZE
