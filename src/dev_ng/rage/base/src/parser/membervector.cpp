// 
// parser/membervector.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "membervector.h"

#include "optimisations.h"

#include "treenode.h"
#include "bank/bank.h"
#include "parsercore/attribute.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

#include <limits.h>
#include <math.h>

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberVector);

void parMemberVector::Data::Init()
{
	StdInit();
	m_InitX = m_InitY = m_InitZ = m_InitW = 0.0f;
#if __BANK
	m_Description = NULL;
	m_Min = -FLT_MAX;
	m_Max = FLT_MAX;
	m_Step = 0.01f;
	m_WidgetCb = NULL;
#endif
}

void parMemberVector::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	if (!node)
	{
		parErrorf("node must be specified");
		return;
	}

	switch(GetType())
	{
	case parMemberType::TYPE_VECTOR2:	GetMemberFromStruct<Vector2>(structAddr) = node->ReadStdLeafVector2(); break;
	case parMemberType::TYPE_VECTOR3:	GetMemberFromStruct<Vector3>(structAddr) = node->ReadStdLeafVector3(); break;
	case parMemberType::TYPE_VECTOR4:	GetMemberFromStruct<Vector4>(structAddr) = node->ReadStdLeafVector4(); break;
	case parMemberType::TYPE_VEC2V:		GetMemberFromStruct<Vec2V>(structAddr) = node->ReadStdLeafVec2(); break;
	case parMemberType::TYPE_VEC3V:		GetMemberFromStruct<Vec3V>(structAddr) = node->ReadStdLeafVec3(); break;
	case parMemberType::TYPE_VEC4V:		GetMemberFromStruct<Vec4V>(structAddr) = node->ReadStdLeafVec4(); break;
	case parMemberType::TYPE_VECBOOLV:	
		{
			VecBoolV& dest = GetMemberFromStruct<VecBoolV>(structAddr);
			BoolV t(V_TRUE);
			BoolV f(V_FALSE);

			dest.SetX(node->GetElement().FindAttributeBoolValue("x", false) ? t : f);
			dest.SetY(node->GetElement().FindAttributeBoolValue("y", false) ? t : f);
			dest.SetZ(node->GetElement().FindAttributeBoolValue("z", false) ? t : f);
			dest.SetW(node->GetElement().FindAttributeBoolValue("w", false) ? t : f);
		}
		break;
	default:
		{
			Quitf(ERR_PAR_INVALID_2,"Invalid type for vector member");
		}
	}
}

size_t rage::parMemberVector::GetSize() const
{
	return parMemberType::GetSize(GetType());
}
