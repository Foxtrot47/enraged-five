// 
// parser/delegateholder.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_DELEGATEHOLDER_H 
#define PARSER_DELEGATEHOLDER_H 

#include "atl/delegate.h"

namespace rage {

	// PURPOSE:
	// For internal parser use only!
	class parDelegateHolderBase
	{
	};

	template<typename _Type>
	class parDelegateHolder : public parDelegateHolderBase
	{
	public:
		_Type	Delegate;
	};


} // namespace rage

#endif // PARSER_DELEGATEHOLDER_H 
