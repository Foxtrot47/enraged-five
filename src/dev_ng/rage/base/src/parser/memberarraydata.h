// 
// parser/memberarraydata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERARRAYDATA_H 
#define PARSER_MEMBERARRAYDATA_H 

#include "memberdata.h"

#include "atl/array.h"
#include "atl/delegate.h"
#include "parser/macros.h"

#include <vector>

namespace rage {
class parStructure;
class parMember;
class parMemberArray;

namespace parMemberArraySubType
{
	// PURPOSE: What kind of array is this?
	enum Enum {
		SUBTYPE_ATARRAY,			// atArray<foo> m_Member;
		SUBTYPE_ATFIXEDARRAY,		// atFixedArray<foo, size> m_Member;
		SUBTYPE_ATRANGEARRAY,		// atRangeArray<foo, size> m_Member;
		SUBTYPE_POINTER,			// foo* m_Member;
		SUBTYPE_MEMBER,				// foo m_Member[size];
		SUBTYPE_ATARRAY_32BIT_IDX,		// atArray<foo, u32> m_Member;
		SUBTYPE_POINTER_WITH_COUNT,	// foo* m_Member; int m_NumItems;
		SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX,	// foo* m_Member; int m_NumItems;
		SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX,	// foo* m_Member; int m_NumItems;
		SUBTYPE_VIRTUAL,			// special - doesn't have storage, depends on PreLoad or PostLoad callbacks
	};
}


struct parVirtualReadData 
{
	int m_Index;
	const parMember* m_MemberMetadata;
	void* m_Cookie;
};

// PURPOSE: All of the data that a parMemberArray needs to describe the array member.
struct parMemberArrayData
{
	enum TypeFlags
	{
		FLAG_ALLOCATE_IN_PHYSICAL_MEMORY,
	};

	typedef atDelegate<void (parPtrToMember, size_t, size_t)> ResizeArrayDelegate; // (ptrToArrayObject, newSize, oldSize)
	typedef atDelegate<bool (parPtrToStructure, parVirtualReadData&)> VirtualReadDelegate;

	STANDARD_PARMEMBER_DATA_CONTENTS;
	size_t m_ElementSize;						// The size of each element in the array
	union {
		u32 m_NumElements;						// The number of elements in the array (necessary for atFixedArray, atRangeArray and member arrays.
		u32 m_CountMemberOffset;				// the offset of the member that gives the number of elements
	};
	void* m_PrototypeData;		// The parMember???::Data describing an element in the array
	union {
		ResizeArrayDelegate* m_ResizeArray;				// A function that creates an sizes the array to the specified size.
		// This should effectively work like C's realloc:
		// If size = 0 - free the array
		// If size < currSize - truncate the array
		// If size > currSize - grow the array, possibly allocating new space and copying the array contents

		VirtualReadDelegate* m_VirtualReadCallback;	// A function that gets called whenever a virtual array element is loaded.
	};
	void Init();
	void PreLoad(parTreeNode*) {Init();}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

// TODO: Only use these and #include the appropriate headers if necessary?
namespace parMemberArrayResizers
{
	// PURPOSE: A function that reinterprets base as an atArray and resizes the array to the specified number of elements
	// PARAMS:
	//		T - The type of object that the array contains.
	//		_CounterType - The type to use for indexing into the array 
	//		atArrayAddr - The address of the atArray (an atArray<T>*)
	//		size - The number of elements in the array.
	template<typename T, typename _CounterType>
	static void ResizeAtArray(parPtrToMember atArrayAddr, size_t size, size_t oldSize);

	// PURPOSE: A function that reinterprets base as a pointer to an array and points it to a new array of size elements.
	// PARAMS:
	//		T - The type of object that the array contains.
	//		arrayAddr - The address of the pointer to the first element of the array (a T**)
	//		size - The number of elements in the array.
	template<typename T>
	static void ResizeRawArray(parPtrToMember arrayAddr, size_t size, size_t oldSize);

	template<typename T, int Align>
	static void ResizeRawArrayWithAlign(parPtrToMember arrayAddr, size_t size, size_t oldSize);
}

template<typename _Type, typename _CounterType>
inline void parMemberArrayResizers::ResizeAtArray(parPtrToMember /*atArrayAddr*/, size_t /*newSize*/, size_t /*oldSize*/)
{
	FastAssert(0); // This shouldn't get called
}

template<typename T, int Align>
inline void parMemberArrayResizers::ResizeRawArrayWithAlign(parPtrToMember /*arrayAddr*/, size_t /*size*/, size_t /*oldSize*/)
{
	FastAssert(0); // This shouldn't get called
}

template<typename T>
inline void parMemberArrayResizers::ResizeRawArray(parPtrToMember /*arrayAddr*/, size_t /*size*/, size_t /*oldSize*/)
{
	FastAssert(0); // This shouldn't get called
}

} // namespace rage

#endif // PARSER_MEMBERARRAYDATA_H 
