// 
// parser/visitortree.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "visitortree.h"
#include "visitorinit.h"

#include "optimisations.h"
#include "structure.h"
#include "treenode.h"

#include "math/float16.h"
#include "vectormath/boolv.h"
#include "vectormath/scalarv.h"
#include "vectormath/vecboolv.h"

PARSER_OPTIMISATIONS();

using namespace rage;

#if RSG_ASSERT
bool parBuildTreeVisitor::sm_AssertOnUnsavableTypes = true;
#endif


void parBuildTreeVisitor::VisitMember(parPtrToStructure containingStructPtr, parMember& metadata)
{
	if (m_Settings.GetFlag(parSettings::WRITE_IGNORE_DEFAULTS))
	{
		// We need to remove default fields from the the tree.
		// This code will launch a recursive subtree comparison.
		// Even though there will be an early traversal exit if we found mismatch with the default value - 
		// this function can potentially be quite slow for runtime.
		// This mode is usually used only from RAG.
		parInitVisitor initVisitor(parInitVisitor::COMPARE);
		initVisitor.VisitMember(containingStructPtr, metadata);
		if (!initVisitor.HasNonDefaultMembers())
			return;
	}

	parInstanceVisitor::VisitMember(containingStructPtr, metadata);
}


void parNeedsTreeForOutputVisitor::VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
{
	if (metadata.HasDelegate("PreSave") || metadata.HasDelegate("PostSave"))
	{
		m_Result = true;
		EndTraversal();
		return;
	}

	parInstanceVisitor::VisitStructure(dataPtr, metadata);
}

void parBuildTreeVisitor::SimpleMember(parPtrToMember dataPtr, parMemberSimple& metadata)
{
	parTreeNode* node;
	switch(metadata.GetType())
	{
	case parMemberType::TYPE_BOOL:	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), *reinterpret_cast<bool*>(dataPtr), false); break;
	case parMemberType::TYPE_CHAR:	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<s8*>(dataPtr), false); break;
	case parMemberType::TYPE_UCHAR:	
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<u8*>(dataPtr), false); 
		node->GetElement().FindAttribute("value")->SetUnsigned(true);
		break;
	case parMemberType::TYPE_SHORT:	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<s16*>(dataPtr), false); break;
	case parMemberType::TYPE_USHORT:	
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<u16*>(dataPtr), false); 
		node->GetElement().FindAttribute("value")->SetUnsigned(true);
		break;
	case parMemberType::TYPE_INT:	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<s32*>(dataPtr), false); break;
	case parMemberType::TYPE_UINT:	
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<u32*>(dataPtr), false);
		if (metadata.GetSubtype() == parMemberSimpleSubType::SUBTYPE_COLOR)
		{
			node->GetElement().FindAttribute("value")->SetHexadecimal(true);
		}
		else
		{
			node->GetElement().FindAttribute("value")->SetUnsigned(true);
		}
		break;
	case parMemberType::TYPE_PTRDIFFT:	
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<ptrdiff_t*>(dataPtr), false);
		break;
	case parMemberType::TYPE_SIZET:	
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<size_t*>(dataPtr), false);
		node->GetElement().FindAttribute("value")->SetUnsigned(true);
		break;
	case parMemberType::TYPE_FLOAT:
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), *reinterpret_cast<float*>(dataPtr), false); 
		node->GetElement().FindAttribute("value")->SetHighPrecision(metadata.GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
		break;
	case parMemberType::TYPE_FLOAT16:
		{
			float f = reinterpret_cast<Float16*>(dataPtr)->GetFloat32_FromFloat16();
			node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), f, false); 
		}
		break;
	case parMemberType::TYPE_SCALARV:
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (*reinterpret_cast<ScalarV*>(dataPtr)).Getf(), false); 
		node->GetElement().FindAttribute("value")->SetHighPrecision(metadata.GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
		break;
	case parMemberType::TYPE_BOOLV:
		m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (*reinterpret_cast<BoolV*>(dataPtr)).Getb(), false);
		break;
	case parMemberType::TYPE_INT64:	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<s64*>(dataPtr), false); break;
	case parMemberType::TYPE_UINT64:	
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)*reinterpret_cast<u64*>(dataPtr), false); 
		node->GetElement().FindAttribute("value")->SetUnsigned(true);
		break;
	case parMemberType::TYPE_DOUBLE:
		node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), *reinterpret_cast<double*>(dataPtr), false); 
		node->GetElement().FindAttribute("value")->SetHighPrecision(true);
		break;
	default:
		{
			Quitf(ERR_PAR_INVALID_7,"Invalid type for simple member");
		}
	}
}

void parBuildTreeVisitor::VecBoolVMember(VecBoolV& data, parMemberVector& metadata)
{
	parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);
	node->GetElement().AddAttribute("x", data.GetX().Getb(), false);
	node->GetElement().AddAttribute("y", data.GetY().Getb(), false);
	node->GetElement().AddAttribute("z", data.GetZ().Getb(), false);
	node->GetElement().AddAttribute("w", data.GetW().Getb(), false);
	node->AppendAsChildOf(m_CurrContainer);
}

void parBuildTreeVisitor::Vec2VMember(Vec2V& data, parMemberVector& metadata)
{
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION);
	if (highPrec)
	{
		node->GetElement().FindAttribute("x")->SetHighPrecision(true);
		node->GetElement().FindAttribute("y")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Vec3VMember(Vec3V& data, parMemberVector& metadata)
{
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION);
	if (highPrec)
	{
		node->GetElement().FindAttribute("x")->SetHighPrecision(true);
		node->GetElement().FindAttribute("y")->SetHighPrecision(true);
		node->GetElement().FindAttribute("z")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Vec4VMember(Vec4V& data, parMemberVector& metadata)
{
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION);
	if (highPrec)
	{
		node->GetElement().FindAttribute("x")->SetHighPrecision(true);
		node->GetElement().FindAttribute("y")->SetHighPrecision(true);
		node->GetElement().FindAttribute("z")->SetHighPrecision(true);
		node->GetElement().FindAttribute("w")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Vector2Member(Vector2& data, parMemberVector& metadata)
{
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION);
	if (highPrec)
	{
		node->GetElement().FindAttribute("x")->SetHighPrecision(true);
		node->GetElement().FindAttribute("y")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Vector3Member(Vector3& data, parMemberVector& metadata)
{
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION);
	if (highPrec)
	{
		node->GetElement().FindAttribute("x")->SetHighPrecision(true);
		node->GetElement().FindAttribute("y")->SetHighPrecision(true);
		node->GetElement().FindAttribute("z")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Vector4Member(Vector4& data, parMemberVector& metadata)
{
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberVectorData::FLAG_HIGH_PRECISION);
	if (highPrec)
	{
		node->GetElement().FindAttribute("x")->SetHighPrecision(true);
		node->GetElement().FindAttribute("y")->SetHighPrecision(true);
		node->GetElement().FindAttribute("z")->SetHighPrecision(true);
		node->GetElement().FindAttribute("w")->SetHighPrecision(true);
	}
}
void parBuildTreeVisitor::Mat33VMember(Mat33V& data, parMemberMatrix& metadata)
{
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION);
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	if (highPrec)
	{
		node->GetElement().FindAttribute("content")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Mat34VMember(Mat34V& data, parMemberMatrix& metadata)
{
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION);
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	if (highPrec)
	{
		node->GetElement().FindAttribute("content")->SetHighPrecision(true);
	}
}
void parBuildTreeVisitor::Mat44VMember(Mat44V& data, parMemberMatrix& metadata)
{
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION);
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	if (highPrec)
	{
		node->GetElement().FindAttribute("content")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Matrix34Member(Matrix34& data, parMemberMatrix& metadata)
{
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION);
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	if (highPrec)
	{
		node->GetElement().FindAttribute("content")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::Matrix44Member(Matrix44& data, parMemberMatrix& metadata)
{
	bool highPrec = metadata.GetTypeFlags().IsSet(parMemberMatrixData::FLAG_HIGH_PRECISION);
	parTreeNode* node = m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), data, false);
	if (highPrec)
	{
		node->GetElement().FindAttribute("content")->SetHighPrecision(true);
	}
}

void parBuildTreeVisitor::EnumMember(int& data, parMemberEnum& metadata)
{
	if (!metadata.GetEnum()->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "building tree"))
	{
		return;
	}

	const char* valName = metadata.NameFromValueUnsafe(data);
	if (valName)
	{
		m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), valName, false);
	}
	else
	{
		m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), (s64)data, false); // write it as an int
	}
}

void parBuildTreeVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata)
{
	if (metadata.AreBitsNamed() && !metadata.GetEnum()->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "building tree"))
	{
		return;
	}

	// fast path - no names specified just write this out as a block of hex data

	parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);
	node->AppendAsChildOf(m_CurrContainer);
	node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HEXADECIMAL, true);
	node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_UNSIGNED, true);
	node->GetDataWriteValsPerLine() = 4;
	if (!metadata.IsFixedSize())
	{
		node->GetElement().AddAttribute("bits", (s64)numBits, false);
	}

	atString dataString;

	parMemberBitset::StringReprStatus status;
	status = metadata.GenerateStringRepr(m_ContainingStructureAddress, dataString, INT_MAX, node->GetDataWriteValsPerLine());

	if (status != parMemberBitset::SR_NONE_NAMED)
	{
		node->SetData(dataString.c_str(), dataString.GetLength()); // no explcit encoding so we don't get unnecessary content= attributes
 	}
	else
	{
		// Easier case - just write the values out in the native format
		parStream::DataEncoding encoding = parStream::UNSPECIFIED;
		int bytes = metadata.GetNumBlocks(m_ContainingStructureAddress) * metadata.GetBlockSize();

		switch(metadata.GetBlockSize())
		{
		case 1: encoding = parStream::CHAR_ARRAY;	break;
		case 2: encoding = parStream::SHORT_ARRAY;	break;
		case 4: encoding = parStream::INT_ARRAY;	break;
		}

		node->SetData((char*)ptrToBits, bytes, encoding);
		return;
	}
}

void parBuildTreeVisitor::StringMember(parPtrToMember /*ptrToMember*/, const char* ptrToString, parMemberString& metadata)
{
	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), ptrToString, false);
}

void parBuildTreeVisitor::AtHashValueMember(atHashValue&, parMemberString& ASSERT_ONLY(metadata))
{
	parAssertf(!sm_AssertOnUnsavableTypes, "Can't build XML tree with an atHashValue member (%s)", metadata.GetName());
}

void parBuildTreeVisitor::AtPartialHashValueMember(u32&, parMemberString& ASSERT_ONLY(metadata))
{
	parAssertf(!sm_AssertOnUnsavableTypes, "Can't build XML tree with an atPartialHashValue member (%s)", metadata.GetName());
}

void parBuildTreeVisitor::WideStringMember(parPtrToMember /*ptrToMember*/, const char16* ptrToString, parMemberString& metadata)
{
	m_CurrContainer->AppendStdLeafChild(metadata.GetNameUnsafe(), ptrToString, false);
}

void parBuildTreeVisitor::ExternalPointerMember(void*& data, parMemberStruct& metadata)
{
	using namespace parMemberStructSubType;

	parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);

	const char* str = NULL;
	switch(metadata.GetSubtype())
	{
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
		if (!data && metadata.GetSubtype() == SUBTYPE_EXTERNAL_NAMED_POINTER)
		{
			str = "NULL";
		}
		else
		{
			str = metadata.GetData()->m_PtrToNameCB(data);
		}
		node->GetElement().AddAttribute("ref", str, false, true);
		break;
	case SUBTYPE_POINTER:
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_STRUCTURE:
		break;
	}

	node->AppendAsChildOf(m_CurrContainer);
}

bool parBuildTreeVisitor::BeginStructMember(parPtrToStructure structAddr, parMemberStruct& metadata)
{
	parStructure* structureMetadata = metadata.GetConcreteStructure(structAddr);
	if (!structureMetadata->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "building tree"))
	{
		return false;
	}
	parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);
	parVersionInfo vers = structureMetadata->GetVersion();
	if (vers.GetMajor() > 0)
	{
		char buf[32];
		formatf(buf, 32, "%d.%d", vers.GetMajor(), vers.GetMinor());
		node->GetElement().AddAttribute("v", buf, false, true);
	}
	node->AppendAsChildOf(m_CurrContainer);

	m_CurrContainer = node;

	return true;
}

void parBuildTreeVisitor::EndStructMember(parPtrToStructure UNUSED_PARAM(structAddr), parMemberStruct& UNUSED_PARAM(metadata))
{
	m_CurrContainer = m_CurrContainer->GetParent();
}

bool parBuildTreeVisitor::BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
{
	parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);
	node->AppendAsChildOf(m_CurrContainer);

	m_CurrContainer = node;

	if (ptrRef == NULL)
	{
		node->GetElement().AddAttribute("type", "NULL", false, false);
		return false;
	}
	else {
		parStructure* structureMetadata = metadata.GetConcreteStructure(ptrRef);

		if (!structureMetadata->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "building tree"))
		{
			return false;
		}

		parVersionInfo vers = structureMetadata->GetVersion();
		if (vers.GetMajor() > 0)
		{
			char buf[32];
			formatf(buf, 32, "%d.%d", vers.GetMajor(), vers.GetMinor());
			node->GetElement().AddAttribute("v", buf, false, true);
		}

		if (metadata.CanDerive())
		{
			node->GetElement().AddAttribute("type", structureMetadata->GetNameUnsafe(), false, false);
		}
	}

	return true;
}

void parBuildTreeVisitor::EndPointerMember(parPtrToStructure& /*ptrRef*/, parMemberStruct& /*metadata*/)
{
	m_CurrContainer = m_CurrContainer->GetParent();
}

bool parBuildTreeVisitor::BeginArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
{
	parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);
	node->AppendAsChildOf(m_CurrContainer);

	parMember* proto = metadata.GetPrototypeMember();

	parMember::Type protoType = (parMember::Type)proto->GetType();

	if (metadata.GetExtraAttributes())
	{
		int valuesPerLine = metadata.GetExtraAttributes()->FindAttributeIntValue("xValuesPerLine", 1);
		parAssertf(1 <= valuesPerLine && valuesPerLine <= UCHAR_MAX, "Invalid value for xValuesPerLine: %d", valuesPerLine);
		node->GetDataWriteValsPerLine() = (u8)valuesPerLine;
	}

	if (protoType == parMemberType::TYPE_UINT ||
		protoType == parMemberType::TYPE_UCHAR ||
		protoType == parMemberType::TYPE_USHORT ||
		protoType == parMemberType::TYPE_UINT64)
	{
		node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_UNSIGNED, true);
	}

	size_t dataBytes = numElements * metadata.GetData()->m_ElementSize;
	FastAssert(dataBytes < UINT_MAX);
	u32 dataBytes32 = (u32)dataBytes;

	switch(protoType)
	{
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_UINT:
		if (proto->AsSimpleRef().GetSubtype() == parMemberSimpleSubType::SUBTYPE_COLOR)
		{
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HEXADECIMAL, true);
		}
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
		node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32, parStream::INT_ARRAY);
		return false;
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_UCHAR:
		node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32, parStream::CHAR_ARRAY);
		return false;
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_USHORT:
		node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32, parStream::SHORT_ARRAY);
		return false;
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
		node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32, parStream::INT64_ARRAY);
		return false;
	case parMemberType::TYPE_FLOAT:
		{
			node->GetElement().AddAttribute("content", "float_array");
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION, proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
			node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32);
		}
		return false;
	case parMemberType::TYPE_DOUBLE:
		node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32, parStream::DOUBLE_ARRAY);
		return false;
	case parMemberType::TYPE_FLOAT16:
		{
			node->GetElement().AddAttribute("content", "float_array");
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION, proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
			
			float* floats = rage_new float[numElements];
			Float16* halfs = reinterpret_cast<Float16*>(arrayContents);
			for(u32 i = 0; i < numElements; i++)
			{
				floats[i] = halfs[i].GetFloat32_FromFloat16();
			}
			node->AssumeData(reinterpret_cast<char*>(floats), (u32)(numElements * sizeof(float)));
		}
		return false;
	case parMemberType::TYPE_VEC2V:
		{
			node->GetElement().AddAttribute("content", "vec2v_array");
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION, proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
			node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32);
		}
		return false;
	case parMemberType::TYPE_VECTOR2:
		{
			node->GetElement().AddAttribute("content", "vector2_array");
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION, proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
			node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32);
		}
		return false;
	case parMemberType::TYPE_VEC3V:
	case parMemberType::TYPE_VECTOR3:
		{
			node->GetElement().AddAttribute("content", "vector3_array");
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION, proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
			node->SetData(reinterpret_cast<char*>(arrayContents),dataBytes32);
		}
		return false;
	case parMemberType::TYPE_VEC4V:
	case parMemberType::TYPE_VECTOR4:
		{
			node->GetElement().AddAttribute("content", "vector4_array");
			node->SetDataWriteFlag(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION, proto->GetTypeFlags().IsSet(parMemberSimpleData::FLAG_HIGH_PRECISION));
			node->SetData(reinterpret_cast<char*>(arrayContents), dataBytes32);
		}
		return false;
	case parMemberType::TYPE_BOOL:
	case parMemberType::TYPE_STRUCT:
	case parMemberType::TYPE_ARRAY:
	case parMemberType::TYPE_STRING:
	case parMemberType::TYPE_ENUM:
	case parMemberType::TYPE_BITSET:
    case parMemberType::TYPE_MAP:
	case parMemberType::TYPE_MATRIX34:
	case parMemberType::TYPE_MATRIX44:
	case parMemberType::TYPE_MAT33V:
	case parMemberType::TYPE_MAT34V:
	case parMemberType::TYPE_MAT44V:
	case parMemberType::TYPE_BOOLV:
	case parMemberType::TYPE_VECBOOLV:
	case parMemberType::TYPE_SCALARV:
		m_CurrContainer = node;
		return true;
	case parMemberType::INVALID_TYPE:
		{
			Quitf(ERR_PAR_INVALID_8,"Invalid type detected!");
		}
		break;
	}
	return false;
}

void parBuildTreeVisitor::EndArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
{
	switch((parMember::Type)metadata.GetPrototypeMember()->GetType())
	{
	case parMemberType::TYPE_BOOL:
	case parMemberType::TYPE_STRUCT:
	case parMemberType::TYPE_ARRAY:
	case parMemberType::TYPE_STRING:
	case parMemberType::TYPE_ENUM:
	case parMemberType::TYPE_BITSET:
	case parMemberType::TYPE_MATRIX34:
	case parMemberType::TYPE_MATRIX44:
	case parMemberType::TYPE_MAT33V:
	case parMemberType::TYPE_MAT34V:
	case parMemberType::TYPE_MAT44V:
	case parMemberType::TYPE_BOOLV:
	case parMemberType::TYPE_VECBOOLV:
	case parMemberType::TYPE_SCALARV:
		m_CurrContainer = m_CurrContainer->GetParent();
		break;
	default:
		// don't need to do anything, since no child node was added
		break;
	}
}

bool parBuildTreeVisitor::BeginMapMember(parPtrToStructure /*structAddr*/, parMemberMap& metadata)
{
    parTreeNode* node = rage_new parTreeNode(metadata.GetNameUnsafe(), false);
    node->AppendAsChildOf(m_CurrContainer);
    m_CurrContainer = node;

    return true;
}

void parBuildTreeVisitor::EndMapMember(parPtrToStructure /*structAddr*/, parMemberMap& metadata)
{
	parAttribute::Type sortType = parAttribute::STRING;
	bool unsignedSort = false;
	switch(metadata.GetKeyMember()->GetType())
	{
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_INT64:
		sortType = parAttribute::INT64;
		unsignedSort = false;
		break;
	case parMemberType::TYPE_UCHAR:
	case parMemberType::TYPE_USHORT:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_UINT64:
		sortType = parAttribute::INT64;
		unsignedSort = true;
		break;
	default:
		sortType = parAttribute::STRING;
		break;
	}
	m_CurrContainer->SortChildrenByAttribute("key", sortType, unsignedSort);
    m_CurrContainer = m_CurrContainer->GetParent();
}

void parBuildTreeVisitor::VisitMapMember(parPtrToStructure /*structAddr*/, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata)
{
    //Get the current count of all children of m_CurrContainer
    //  This done just in case we happen to NOT add a new node to the tree
    //      (ie unsupported type)
    parTreeNode* curChild = m_CurrContainer->GetChild();
    int precount = 0;
    while(curChild)
    {
        precount++;
        curChild = curChild->GetSibling();
    }

    VisitMember(mapDataAddress, *(metadata.GetDataMember()));

    //Find the last added sibling
    curChild = m_CurrContainer->GetChild();
    parTreeNode* prevChild = NULL;
    int postcount = 0;
    while(curChild)
    {
        postcount++;
        prevChild = curChild;
        curChild = curChild->GetSibling();
    }

    if (postcount && postcount != precount)
    {
        parAssertf(prevChild, "Iteration parTreeNode over linked list resulted in an invalid node.");

        switch (metadata.GetKeyMember()->GetType())
        {
            //These are the limited supported types for key values
        case parMemberType::TYPE_CHAR:
            {
                s8 value = *((s8*)mapKeyAddress);
                prevChild->GetElement().AddAttribute("key", value);
            }
            break;
        case parMemberType::TYPE_UCHAR:
            {
                u8 value = *((u8*)mapKeyAddress);
                parAttribute* attr = prevChild->GetElement().AddAttribute("key", value);
				attr->SetUnsigned(true);
            }
            break;
        case parMemberType::TYPE_SHORT:
            {
                s16 value = *((s16*)mapKeyAddress);
                prevChild->GetElement().AddAttribute("key", value);
            }
            break;
        case parMemberType::TYPE_USHORT:
            {
                u16 value = *((u16*)mapKeyAddress);
				parAttribute* attr = prevChild->GetElement().AddAttribute("key", value);
				attr->SetUnsigned(true);
            }
            break;
		case parMemberType::TYPE_INT:
			{
				s32 value = *((s32*)mapKeyAddress);
				prevChild->GetElement().AddAttribute("key", value);
			}
			break;
		case parMemberType::TYPE_PTRDIFFT:
            {
                ptrdiff_t value = *((ptrdiff_t*)mapKeyAddress);
                prevChild->GetElement().AddAttribute("key", (s64)value);
            }
            break;
        case parMemberType::TYPE_UINT:
			{
				u32 value = *((u32*)mapKeyAddress);
				parAttribute* attr = prevChild->GetElement().AddAttribute("key", (s64)value);
				attr->SetUnsigned(true);
			}
			break;
		case parMemberType::TYPE_SIZET:
            {
                size_t value = *((size_t*)mapKeyAddress);
                parAttribute* attr = prevChild->GetElement().AddAttribute("key", (s64)value);
				attr->SetUnsigned(true);
            }
			break;
		case parMemberType::TYPE_INT64:
			{
				s64 value = *((s64*)mapKeyAddress);
				prevChild->GetElement().AddAttribute("key", value);
			}
			break;
		case parMemberType::TYPE_UINT64:
			{
				u64 value = *((u64*)mapKeyAddress);
				parAttribute* attr = prevChild->GetElement().AddAttribute("key", (s64)value);
				attr->SetUnsigned(true);
			}
			break;
		case parMemberType::TYPE_ENUM:
			{
				parMemberEnum& enumParMember = metadata.GetKeyMember()->AsEnumRef();
				int value = enumParMember.GetValue(mapKeyAddress);
				if (enumParMember.GetEnum()->VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "building tree"))
				{
					prevChild->GetElement().AddAttribute("key", enumParMember.NameFromValueUnsafe(value), false, false);
				}
				else
				{
					prevChild->GetElement().AddAttribute("key", value, false);
				}
			}
			break;
        case parMemberType::TYPE_STRING:
            {
                parMemberString& strParMember = metadata.GetKeyMember()->AsStringRef();
                switch (strParMember.GetSubtype())
                {
#if !__FINAL
                case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
                     prevChild->GetElement().AddAttribute("key", ((atHashString*)mapKeyAddress)->GetCStr());
					 break;
#endif
				case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
					prevChild->GetElement().AddAttribute("key", ((atFinalHashString*)mapKeyAddress)->GetCStr());
					break;
				case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
					prevChild->GetElement().AddAttribute("key", atHashStringNamespaceSupport::GetString(strParMember.GetData()->GetNamespaceIndex(), *((u32*)mapKeyAddress)));
					break;
				case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
					parAssertf(!sm_AssertOnUnsavableTypes, "Can't create an XML tree with a map (%s) that contains atHashValue keys", metadata.GetName());
					break;
				case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
					parAssertf(!sm_AssertOnUnsavableTypes, "Can't create an XML tree with a map (%s) that contains atPartialHashValue keys", metadata.GetName());
					break;
				case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
					parAssertf(0, "Can't create an XML tree with a map (%s) that contains %s keys", metadata.GetName(), atHashStringNamespaceSupport::GetNamespaceValueName(strParMember.GetData()->GetNamespaceIndex()));
					break;
                default:
                    parAssertf(!sm_AssertOnUnsavableTypes,"Unsupported key string type %d being used for map %s", strParMember.GetSubtype(), metadata.GetName());
                    break;
                };
            }
            break;
        default:
            parAssertf(!sm_AssertOnUnsavableTypes,"Unsupported key type %d being used for map %s", metadata.GetKeyMember()->GetType(), metadata.GetName());
            break;
        };
    }
}

bool parBuildTreeVisitor::BeginToplevelStruct(parPtrToStructure /*ptrToStruct*/, parStructure& metadata)
{
	if (!metadata.VerifyNamesExist(m_Settings.GetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS), "building tree"))
	{
		return false;
	}

	if (m_TopLevelName)
	{
		m_CurrContainer = rage_new parTreeNode(m_TopLevelName);
		m_CurrContainer->GetElement().AddAttribute("type", metadata.GetNameUnsafe());
	}
	else
	{
		m_CurrContainer = rage_new parTreeNode(metadata.GetNameUnsafe());
	}
	parVersionInfo vers = metadata.GetVersion();
	if (vers.GetMajor() > 0)
	{
		char buf[32];
		formatf(buf, 32, "%d.%d", vers.GetMajor(), vers.GetMinor());
		m_CurrContainer->GetElement().AddAttribute("v", buf, false, true);
	}

	m_Tree.SetRoot(m_CurrContainer);

	return true;
}

void parBuildTreeVisitor::EndToplevelStruct(parPtrToStructure /*ptrToStruct*/, parStructure& /*metadata*/)
{

}

void parBuildTreeVisitor::VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
{
	metadata.Invoke(dataPtr, "PreSave");
	parInstanceVisitor::VisitStructure(dataPtr, metadata);
	metadata.Invoke<parTreeNode*>(dataPtr, "PostSave", m_CurrContainer);
}

