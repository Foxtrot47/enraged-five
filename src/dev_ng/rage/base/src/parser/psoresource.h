// 
// parser/psoresource.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSORESOURCE_H
#define PARSER_PSORESOURCE_H

#include "psofaketypes.h"
#include "psoptr.h"

#include "atl/bitset.h"
#include "paging/base.h"
#include "vectormath/vec4v.h"

namespace rage
{

struct psoRscEnumSchemaData;
struct psoEnumTableData2;
struct psoRscStructArrayTableData;
struct psoRscStructSchemaData;
struct psoStructTableData2;

class psoResourceData : public pgBasePlatformNeutral
{
public:
	psoResourceData();
	psoResourceData(datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize origSize);

	void DeleteSchemaData();
	void DeleteInstances(); // Deletes the instance data from the structarrays, but NOT the structarrays themselves
	void DeleteStructArrays();
	void DeleteAll() { DeleteInstances(); DeleteStructArrays(); DeleteSchemaData(); }

	static void Place(psoResourceData* that, datResource& rsc);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	void CopySchemaDataTo(psoResourceData& dest);
	void CreateStructArrayTable(size_t count);
	void CreateEnumSchemaTable(size_t count);
	void CreateStructSchemaTable(size_t count);

	enum Flags
	{
		ENCRYPT_NONFINAL_STRINGS,
	};

#if !__FINAL
	static const u8 NonfinalStringKey[32];
#endif

	// 8b
	u32 m_Identifier;		// 'PRD0'
	u8 m_PlatformId; // Creator platform - specifies endian, bit size.
	datPadding<1> m_Pad;
	atFixedBitSet16 m_Flags;

	//8b
	u32 m_Checksum;			// Hash of the contents of the file (still necessary? Maybe just hash the instance data?)
	psoStructId m_RootObjectId;

	// 3x8b
	psoPtr<psoRscStructSchemaData> m_StructSchemaTable;
	psoPtr<psoRscEnumSchemaData> m_EnumSchemaTable;
	psoPtr<psoRscStructArrayTableData> m_StructArrayTable;

	// 2x8b
	psoPtr<char> m_FinalHashStrings; // Null terminated string sequence, ending in an empty string
	psoPtr<char> m_NonFinalHashStrings; // Ditto, but if ENCRYPT_NONFINAL_STRINGS is set, the first 4 bytes are the big-endian size of the remaining string

	// 32b
	u16 m_NumStructSchemas;	
	u16 m_NumEnumSchemas;
	u16 m_NumStructArrayEntries;
	datPadding<26> m_Reserved; // for future use... If using this, try to make sure we don't run out of reserved space or ways to add more data w/o bumping resource versions

	static const u32 ChecksumSalt = 0xae54fef0; // Doesn't matter, some random number

	static const int RESOURCE_VERSION = 2;

private:
	static void CopyStructSchema(psoRscStructSchemaData& src, psoRscStructSchemaData& dest);
	static void CopyEnumSchema(psoRscEnumSchemaData& src, psoRscEnumSchemaData& dest);
};

}

#endif // PARSER_PSORESOURCE_H