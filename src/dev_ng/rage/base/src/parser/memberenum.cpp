// 
// parser/memberenum.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "memberenum.h"

#include "optimisations.h"
#include "treenode.h"

#include "atl/string.h"
#include "bank/bank.h"
#include "diag/output.h"
#include "parsercore/streamxml.h"
#include "system/alloca.h"

#include <cerrno>

PARSER_OPTIMISATIONS();

using namespace rage;

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
// parMemberEnum
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberEnum);

parEnumData parEnumData::Empty = {
	NULL,
	NULL,
	0,
	(1 << parEnumFlags::ENUM_STATIC)
};

void parMemberEnum::Data::Init()
{
	StdInit();
	m_Init = 0;
	m_EnumData = NULL;
#if __BANK
	m_Description = NULL;
	m_WidgetCb = NULL;
#endif
}


parMemberEnum::~parMemberEnum()
{
	// TODO: This should really be done in a parEnum object of some sort.
	parEnumData* enumData = GetData()->m_EnumData;
	if (enumData && !(enumData->m_Flags & (1 << parEnumFlags::ENUM_STATIC)))
	{
		delete [] enumData->m_Enums;
		for(int i = 0; i < enumData->m_NumEnums; i++)
		{
			delete enumData->m_Names[i];
		}
		delete [] enumData->m_Names;
		delete enumData;
	}
}

void parMemberEnum::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	using namespace parMemberEnumSubType;

	int value = -1;
	parEnumData::ValueFromNameStatus status = parEnumData::VFN_ERROR;
	if (node->HasData())
	{
		value = ValueFromName(node->GetData(), NULL, &status);
	}
	else
	{
		// Check for a value attribute and convert it
		parAttribute* attr = node->GetElement().FindAttribute("value");
		if (attr)
		{
			// FindAttributeIntValue can't return an error code if the value isn't really an int - so use 
			// ValueFromName instead, which should give better data
			if (attr->GetType() == parAttribute::STRING)
			{										
				// rsTODO: We should only treat the string as a stringified integer - don't try to hash it first.
				value = ValueFromName(attr->GetStringValue(), NULL, &status);
			}
			else
			{
				value = attr->GetIntValue();
				status = parEnumData::VFN_NUMERIC;
			}
		}
	}

	if (status == parEnumData::VFN_ERROR)
	{
		if (node->HasData())
		{
			parErrorf("Couldn't find enum value for node %s, value %s", node->GetElement().GetName(), node->GetData());
		}
		else
		{
			parErrorf("Couldn't find enum value for node %s", node->GetElement().GetName());
		}
		return;
	}

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT:
		GetMemberFromStruct<u8>(structAddr) = (u8)value; break;
	case SUBTYPE_16BIT:
		GetMemberFromStruct<u16>(structAddr) = (u16)value; break;
	case SUBTYPE_32BIT:
		GetMemberFromStruct<u32>(structAddr) = (u32)value; break;
	}
}

int parEnumData::ValueFromName(const char* name, const char** nameEndOut, parEnumData::ValueFromNameStatus* result) const
{
	// skip leading whitespace, and hash up to the next ws char
	const char* qualNameStart = name;
	while(*qualNameStart && !parStreamInXml::IsXmlNameCharacter(*qualNameStart))
	{
		qualNameStart++;
	}
	const char* nameEnd = qualNameStart;
	const char* nameStart = qualNameStart; // qualNameStart is the start of the possibly namespace qualified name, nameStart is the start of the local part of the name
	while(*nameEnd && parStreamInXml::IsXmlNameCharacter(*nameEnd))
	{
		if (*nameEnd == ':') // If we see a ':', the local name must start after that.
		{
			nameStart = nameEnd+1;
		}
		nameEnd++;
	}

	if (nameStart == nameEnd) 
	{
		// couldn't find any data here - maybe just whitespace?
		if (result)
		{
			*result = VFN_EMPTY;
		}
		if (nameEndOut)
		{
			*nameEndOut = nameEnd;
		}
		return -1;
	}
	u32 hash = atDataHash(nameStart, (int) (nameEnd - nameStart));

	if (nameEndOut)
	{
		*nameEndOut = nameEnd;
	}

	// Loop over all of the hashes and see if we find a matching one
	for(int i = 0; i < m_NumEnums; i++)
	{
		if ((u32)m_Enums[i].m_NameKey == hash)
		{
			parAssertf(!strncmp(nameStart, m_Names[i], nameEnd - nameStart), "Strings didn't match, hash collision? \"%s\" != \"%s\"", name, m_Names[i]);
			if (result)
			{
				*result = VFN_NAMED;
			}
			return m_Enums[i].m_Value;
		}
	}

	// Couldn't find a matching name - so try to read the string as an integer (decimal or hex)
	int out = 0;

	// couldn't read full string
	if (!parUtils::StringToIntSafe(nameStart, out, nameEnd))
	{
		if (result) 
		{
			*result = VFN_ERROR;
		}
		return -1;
	}

	if (result)
	{
		*result = VFN_NUMERIC;
	}

	return out;
}

int parEnumData::ValueFromName(u32 namehash, parEnumData::ValueFromNameStatus* result) const
{
	// Loop over all of the hashes and see if we find a matching one
	for(int i = 0; i < m_NumEnums; i++)
	{
		if ((u32)m_Enums[i].m_NameKey == namehash)
		{
			if (result)
			{
				*result = VFN_NAMED;
			}
			return m_Enums[i].m_Value;
		}
	}

	if (result)
	{
		*result = VFN_ERROR;
	}
	return -1;
}

const char* parEnumData::NameFromValueUnsafe(int val) const
{
	parAssertf(BIT_ISSET(parEnumFlags::ENUM_HAS_NAMES, m_Flags), "This structure doesn't have names. If you need names for saving, use preserveNames=\"true\" in the structdef. See https://devstar.rockstargames.com/wiki/index.php/Enumdef_PSC_tag");
	if (!m_Names) 
	{ 
		return NULL; 
	}
	for(int i = 0; i < m_NumEnums; i++)
	{
		if (val == m_Enums[i].m_Value)
		{
			return m_Names[i];
		}
	}
	return NULL;
}

bool parEnumData::VerifyNamesExist(bool inAllBuilds, const char* OUTPUT_ONLY(action)) const 
{
	if (inAllBuilds)
	{
		parAssertf(BIT_ISSET(parEnumFlags::ENUM_ALWAYS_HAS_NAMES, m_Flags), "While %s: Enum doesn't have names in all builds. Add preserveNames=\"true\" to keep the names. See https://devstar.rockstargames.com/wiki/index.php/Enumdef_PSC_tag", action);
	}
	if (!BIT_ISSET(parEnumFlags::ENUM_HAS_NAMES, m_Flags))
	{
		parErrorf("While %s: Enum 0x%x doesn't have names", action, m_NameHash);
		return false;
	}
	return true;
}

u32 parEnumData::HashFromValue(int val) const
{
	for(int i = 0; i < m_NumEnums; i++)
	{
		if (val == m_Enums[i].m_Value)
		{
			return m_Enums[i].m_NameKey;
		}
	}
	return 0;
}

int parEnumData::MinValue() const
{
	int val = INT_MAX;
	for(int i = 0; i < m_NumEnums; i++)
	{
		val = Min(val, m_Enums[i].m_Value);
	}
	return val;
}


int parEnumData::MaxValue() const
{
	int val = INT_MIN;
	for(int i = 0; i < m_NumEnums; i++)
	{
		val = Max(val, m_Enums[i].m_Value);
	}
	return val;
}


void parMemberEnum::SetFromString(parPtrToStructure structAddr, const char* newVal) const
{
	using namespace parMemberEnumSubType;

	parAssertf(GetType() == parMemberType::TYPE_ENUM, "Only call this SetFromString function with a real enum, not type=%d", (int)GetType());

	parEnumData::ValueFromNameStatus status;
	int value = ValueFromName(newVal, NULL, &status);
	if (status == parEnumData::VFN_ERROR)
	{
		parErrorf("Couldn't convert \"%s\" to a valid enum value", newVal);
		return;
	}
	else if (status == parEnumData::VFN_EMPTY)
	{
		parWarningf("No value name for enum - string was \"%s\".", newVal);
		return;
	}
	switch(GetSubtype())
	{
	case SUBTYPE_8BIT:
		GetMemberFromStruct<u8>(structAddr) = (u8)value; break;
	case SUBTYPE_16BIT:
		GetMemberFromStruct<u16>(structAddr) = (u16)value; break;
	case SUBTYPE_32BIT:
		GetMemberFromStruct<u32>(structAddr) = (u32)value; break;
	}
}

int parMemberEnum::GetValue(parPtrToStructure structAddr) const
{
	using namespace parMemberEnumSubType;

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT:
		return GetMemberFromStruct<u8>(structAddr);
	case SUBTYPE_16BIT:
		return GetMemberFromStruct<u16>(structAddr);
	case SUBTYPE_32BIT:
		return GetMemberFromStruct<int>(structAddr);
	}
	return 0;
}	

size_t rage::parMemberEnum::GetSize() const
{
	using namespace parMemberEnumSubType;

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT: return 1;
	case SUBTYPE_16BIT: return 2;
	case SUBTYPE_32BIT: return 4;
	}
	return 0;
}

size_t rage::parMemberEnum::FindAlign() const
{
	using namespace parMemberEnumSubType;

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT: return 1;
	case SUBTYPE_16BIT: return 2;
	case SUBTYPE_32BIT: return 4;
	}
	return 0;
}

parEnumData* rage::parMemberEnum::GetEnum()
{
	return GetData()->m_EnumData;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
// parMemberBitset
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Type>
void parMemberBitset::SetFromNumericArray(parPtrToStructure structAddr, const char* data, u32 sizeInBytes, int ASSERT_ONLY(encoding), const char* OUTPUT_ONLY(nodeName)) const
{
	parAssertf(GetBlockSize() == sizeof(_Type), "Block size mismatch. File contained a %s, structure expects %d byte blocks", parStream::FindEncodingName((parStream::DataEncoding)encoding), GetBlockSize());

	parPtrToArray bitsDest;
	u32 numBits;
	GetBitsetContentsAndCountFromStruct(structAddr, bitsDest, numBits);

	u32 blocks = sizeInBytes / sizeof(_Type);

	u32 numDestBlocks = GetNumBlocks(structAddr);

	if (blocks < numDestBlocks)
	{
		parErrorf("Not enough bits specified in the file for element %s. Found %" SIZETFMT "d and expected %d", nodeName, sizeInBytes * sizeof(_Type), numBits);
	}
	else if (blocks > numDestBlocks)
	{
		parErrorf("Too many bits specified in the file for element %s. Found %" SIZETFMT "d and expected %d. Ignoring bits on the end", nodeName, sizeInBytes * sizeof(_Type), numBits);
		blocks = numDestBlocks;
	}

	sysMemCpy(bitsDest, data, blocks * sizeof(_Type));
}

void parMemberBitset::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	ClearBits(structAddr);

	if(GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
	{
		// set the number of bits
		atBitSet& bitset = GetMemberFromStruct<atBitSet>(structAddr);
		int numBits = node->GetElement().FindAttributeIntValue("bits", 0);
		bitset.Init(numBits);
	}

	if (node->HasData())
	{
		const char* data = node->GetData();
		u32 dataSize = node->GetDataSize();
		const char* nodeName = node->GetElement().GetName();

		// check the encoding
		parStream::DataEncoding encoding = node->FindDataEncoding();
		switch(encoding)
		{
		case parStream::UNSPECIFIED:
		case parStream::ASCII:
			SetFromString(structAddr, node->GetData());
			break;
		case parStream::CHAR_ARRAY:
			SetFromNumericArray<u8>(structAddr, data, dataSize, encoding, nodeName);
			break;
		case parStream::SHORT_ARRAY:
			SetFromNumericArray<u16>(structAddr, data, dataSize, encoding, nodeName);
			break;
		case parStream::INT_ARRAY:
			SetFromNumericArray<u32>(structAddr, data, dataSize, encoding, nodeName);
			break;
		default:
			parErrorf("Invalid content type (%s) for bitset '%s'", parStream::FindEncodingName(encoding), nodeName);
		}
	}
	else if (node->GetElement().FindAttribute("value"))
	{
		// make sure we're of the right size and read the value node instead
		parAssertf(GetNumBlocks(structAddr) <= 1, "Can only read from a value attribute if the number of blocks is 1");
		int value = node->ReadStdLeafInt();
		UnionBlock(structAddr, 0, value);
	}

}

size_t rage::parMemberBitset::GetSize() const
{

	if (GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
	{
		return sizeof(atBitSet);
	}
	else
	{
		return GetNumBlocks(NULL) * GetBlockSize(); // Safe to pass NULL because we don't really use structAddr
	}
}
size_t rage::parMemberBitset::FindAlign() const
{
	using namespace parMemberBitsetSubType;

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT_FIXED: return __alignof(u8);
	case SUBTYPE_16BIT_FIXED: return __alignof(u16);
	case SUBTYPE_32BIT_FIXED: return __alignof(u32);
	case SUBTYPE_ATBITSET: return __alignof(atBitSet);
	}
	return 0;
}

int parMemberBitset::GetNumBlocks(parPtrToStructure structAddr) const
{
	using namespace parMemberBitsetSubType;

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT_FIXED: return (GetData()->m_NumBits + 7) / 8;
	case SUBTYPE_16BIT_FIXED: return (GetData()->m_NumBits + 15) / 16;
	case SUBTYPE_32BIT_FIXED: return (GetData()->m_NumBits + 31) / 32;
	case SUBTYPE_ATBITSET: return GetMemberFromStruct<atBitSet>(structAddr).GetNumBitWords();
	}
	return 0;
}

int parMemberBitset::GetBlockSize() const
{
	using namespace parMemberBitsetSubType;

	switch(GetSubtype())
	{
	case SUBTYPE_8BIT_FIXED:	return 1;
	case SUBTYPE_16BIT_FIXED:	return 2;
	case SUBTYPE_32BIT_FIXED:	return 4;
	case SUBTYPE_ATBITSET:		return 4;
	}
	return 0;
}

int	parMemberBitset::GetBitsPerBlock() const
{
    // NOTE: GetBlockSize is expected to be in Bytes
    return GetBlockSize() * 8;
}

typedef atFixedBitSet<USHRT_MAX, u8> MaxFixedBitset8;
typedef atFixedBitSet<USHRT_MAX, u16> MaxFixedBitset16;
typedef atFixedBitSet<USHRT_MAX, u32> MaxFixedBitset32;


void parMemberBitset::GetBitsetContentsAndCountFromStruct(parPtrToStructure structAddr, parPtrToArray& outAddr, u32& outBits) const
{
	using namespace parMemberBitsetSubType;

	u32 junk;
	switch(GetSubtype())
	{
	case SUBTYPE_8BIT_FIXED:
		GetMemberFromStruct<MaxFixedBitset8>(structAddr).GetRaw((u8**)&outAddr, &junk);
		outBits = GetData()->m_NumBits;
		break;
	case SUBTYPE_16BIT_FIXED:
		GetMemberFromStruct<MaxFixedBitset16>(structAddr).GetRaw((u16**)&outAddr, &junk);
		outBits = GetData()->m_NumBits;
		break;
	case SUBTYPE_32BIT_FIXED:
		GetMemberFromStruct<MaxFixedBitset32>(structAddr).GetRaw((u32**)&outAddr, &junk);
		outBits = GetData()->m_NumBits;
		break;
	case SUBTYPE_ATBITSET:
		{
			const atBitSet& bitset = GetMemberFromStruct<atBitSet>(structAddr);
			outAddr = reinterpret_cast<parPtrToArray>(bitset.GetRawBitMask());
			outBits = bitset.GetNumBits();
		}
		break;
	}
}

void parMemberBitset::SetBit(parPtrToStructure structAddr, int bitToSet, bool value/* =true */) const
{
	using namespace parMemberBitsetSubType;

	parAssertf(GetSubtype() == SUBTYPE_ATBITSET || (0 <= bitToSet && bitToSet < GetData()->m_NumBits), "Bit to set is out of range. Val = %d, Max = %d", bitToSet, GetData()->m_NumBits);
	switch(GetSubtype())
	{
	case SUBTYPE_8BIT_FIXED:
		GetMemberFromStruct< MaxFixedBitset8 >(structAddr).Set(bitToSet, value);	break;
	case SUBTYPE_16BIT_FIXED:
		GetMemberFromStruct< MaxFixedBitset16 >(structAddr).Set(bitToSet, value);	break;
	case SUBTYPE_32BIT_FIXED:
		GetMemberFromStruct< MaxFixedBitset32 >(structAddr).Set(bitToSet, value);	break;
	case SUBTYPE_ATBITSET:
		GetMemberFromStruct< atBitSet >(structAddr).Set(bitToSet, value);	break;
	}
}

bool parMemberBitset::GetBit(parPtrToStructure structAddr, int bitToGet) const
{
	using namespace parMemberBitsetSubType;

	parAssertf(GetSubtype() == SUBTYPE_ATBITSET || (0 <= bitToGet && bitToGet < GetData()->m_NumBits), "Bit to get is out of range. Val = %d, Max = %d", bitToGet, GetData()->m_NumBits);
	switch(GetSubtype())
	{
	case SUBTYPE_8BIT_FIXED:
		return GetMemberFromStruct< MaxFixedBitset8 >(structAddr).IsSet(bitToGet);
	case SUBTYPE_16BIT_FIXED:
		return GetMemberFromStruct< MaxFixedBitset16 >(structAddr).IsSet(bitToGet);
	case SUBTYPE_32BIT_FIXED:
		return GetMemberFromStruct< MaxFixedBitset32 >(structAddr).IsSet(bitToGet);
	case SUBTYPE_ATBITSET:
		return GetMemberFromStruct<atBitSet>(structAddr).IsSet(bitToGet);
	}
	return false;
}

void parMemberBitset::UnionBlock(parPtrToStructure structAddr, int blockToSet, u32 blockValue) const
{
	using namespace parMemberBitsetSubType;

	if (blockToSet < GetNumBlocks(structAddr))
	{
		switch(GetSubtype())
		{
		case SUBTYPE_8BIT_FIXED:
			{
				parAssertf(blockValue <= UCHAR_MAX, "Invalid value for a block: %d", blockValue);
				u8* data=NULL;
				u32 count;
				GetMemberFromStruct<MaxFixedBitset8>(structAddr).GetRaw(&data, &count);
				data[blockToSet] |= (u8)blockValue;
			}
			break;
		case SUBTYPE_16BIT_FIXED:
			{
				parAssertf(blockValue <= USHRT_MAX, "Invalid value for a block: %d", blockValue);
				u16* data=NULL;
				u32 count;
				GetMemberFromStruct<MaxFixedBitset16>(structAddr).GetRaw(&data, &count);
				data[blockToSet] |= (u16)blockValue;
			}
			break;
		case SUBTYPE_32BIT_FIXED:
			{
				u32* data=NULL;
				u32 count;
				GetMemberFromStruct<MaxFixedBitset32>(structAddr).GetRaw(&data, &count);
				data[blockToSet] |= blockValue;
			}
			break;
		case SUBTYPE_ATBITSET:
			{
				u32* data = GetMemberFromStruct<atBitSet>(structAddr).GetRawBitMask();
				data[blockToSet] |= blockValue;
			}
		}
	}
	else
	{
		parErrorf("Invalid block number specified for member %s - block=%d, max=%d", GetName(), blockToSet, GetNumBlocks(structAddr));
	}
}

void parMemberBitset::ClearBits(parPtrToStructure structAddr) const
{
	parPtrToArray outAddr;
	u32 numBits;
	GetBitsetContentsAndCountFromStruct(structAddr, outAddr, numBits);

	sysMemSet(reinterpret_cast<char*>(outAddr), 0, GetBlockSize() * GetNumBlocks(structAddr));
}


void parMemberBitset::SetFromString(parPtrToStructure structAddr, const char* newVal) const
{
	// tokenize and build up the enum string
	const char* nameEnd;
	const char* name = newVal;
	parEnumData::ValueFromNameStatus status;

	int blockIndex = 0;
	bool hadABlock = false;

	int maxBlock = GetNumBlocks(structAddr);

	u32 numBits;
	parPtrToArray unused;
	GetBitsetContentsAndCountFromStruct(structAddr, unused, numBits);

	while(*name)
	{
		int value = ValueFromName(name, &nameEnd, &status);
		switch(status)
		{
		case parEnumData::VFN_NAMED:
			if (parVerifyf(value >= 0 && (u32)value < numBits, "Can't set bit %d in a bitset (%s) that only has %d bits", value, GetName(), numBits))
			{
				SetBit(structAddr, value, true);
			}
			break;
		case parEnumData::VFN_NUMERIC:
			if (blockIndex < maxBlock)
			{
				UnionBlock(structAddr, blockIndex, (u32)value);
				blockIndex++;
				hadABlock = true;
			}
			break;
		case parEnumData::VFN_EMPTY:
			// do nothing
			break;
		case parEnumData::VFN_ERROR:
			parErrorf("Couldn't convert \"%s\" to an enum value", name);
			break;
		}
		name = nameEnd;
	}

	if (hadABlock && blockIndex != maxBlock)
	{
		parWarningf("Wrong number of numeric blocks specified. Expected 0 or %d, but found %d", maxBlock, blockIndex);
	}
}

void parMemberBitset::SetFromInt(parPtrToStructure structAddr, u32 newVal) const
{
	u32 blockMask = 0;
	switch (GetSubtype())
	{
	case parMemberBitsetSubType::SUBTYPE_8BIT_FIXED:
		blockMask = 0xFF;
		break;
	case parMemberBitsetSubType::SUBTYPE_16BIT_FIXED:
		blockMask = 0xFFFF;
		break;
	case parMemberBitsetSubType::SUBTYPE_32BIT_FIXED:
	case parMemberBitsetSubType::SUBTYPE_ATBITSET:
		blockMask = 0xFFFFFFFF;
		break;
	}

	int bitsPerBlock = GetBitsPerBlock();
	int blockCount = GetNumBlocks(structAddr);
	int blocksToSet = sizeof(newVal) / GetBlockSize();
	for (int i = 0; i < blocksToSet; ++i)
	{
		u32 block = blockMask & (newVal >> (i * bitsPerBlock));
		if (i < blockCount)
		{
			UnionBlock(structAddr, i, block);
		}
		else if (block)
		{
			parWarningf("Found data for block %d, but max blocks is %d.", i, blockCount);
		}
	}
}


parMemberBitset::StringReprStatus parMemberBitset::GenerateStringRepr(parPtrToStructure structAddr, atString& stringOut, int namesPerLine /* = 6 */, int intsPerLine /* = 4 */, int indent /* = 0 */) const
{
	bool hasResiduals = false;
	bool hadNamed = false;

	int writtenPerLine = 0;

	atBitSet residualBits;

	int blockSize = GetBlockSize();

	u32 numBits = 0;
	parPtrToArray junk = NULL;
	GetBitsetContentsAndCountFromStruct(structAddr, junk, numBits);

	bool needsSpace = false;

	if (GetData()->m_EnumData->m_NumEnums > 0)
	{
		residualBits.Init(numBits);
		residualBits.Reset();

		for(u32 i = 0; i < numBits; i++)
		{
			if (GetBit(structAddr, i))
			{
				// this bit is set... is it in the enum?
				const char* name = NameFromValueUnsafe(i);
				if (name)
				{
					hadNamed = true;
					if (needsSpace)
					{
						stringOut += " ";
					}
					needsSpace = true;
					stringOut += name;
					writtenPerLine++;
					if (writtenPerLine == namesPerLine) 
					{
						stringOut += "\n";
						for(int iden = 0; iden < indent; iden++)
						{
							stringOut += "\t";
						}
						writtenPerLine = 0;
					}
				}
				else
				{
					residualBits.Set(i, true);
					hasResiduals = true;
				}
			}
		}
	}
	else
	{
		return SR_NONE_NAMED;
	}

	if (hadNamed && hasResiduals)
	{
		// This is kind of dangerous to do... part of the point of symbolic names is that you can change what bits they represent.
		// But using a mixture of symbolic and numeric values means its harder to change the symbols.
		// Save it anyway but print a warning
		parWarningf("Found some bits in bitset '%s' that couldn't be represented with symbolic names. Writing anyway but this can be dangerous if the symbols change values", GetName());

		if (GetNumBlocks(structAddr) > 1)
		{
			// If we need to write more than one hex number of residual data, start it on its own line. Otherwise keep going
			writtenPerLine = 0;
			stringOut += "\n";
			for(int iden = 0; iden < indent; iden++)
			{
				stringOut += "\t";
			}
		}

		int bitsPerBlock = blockSize * 8;
		char buf[32];
		const char* format = NULL;
		switch(blockSize)
		{
		case 1: format = "0x%02X"; break;
		case 2: format = "0x%04X"; break;
		case 4: format = "0x%08X"; break;
		}

		// break up the residual bits into blocks and write them out...
		u32 blockData = 0;
		u32 bit = 0;
		while(bit < numBits)
		{
			blockData = 0;
			for(int bitInBlock = 0; bitInBlock < bitsPerBlock && bit < numBits; bitInBlock++, bit++)
			{
				if (residualBits.IsSet(bit))
				{
					blockData |= 1 << bitInBlock;
				}
			}
			if (needsSpace)
			{
				stringOut += " ";
			}
			needsSpace = true;
			formatf(buf, format, blockData);
			stringOut += buf;
			writtenPerLine++;
			if (writtenPerLine == intsPerLine && GetNumBlocks(structAddr) > 1)
			{
				stringOut += "\n";
				for(int iden = 0; iden < indent; iden++)
				{
					stringOut += "\t";
				}
				writtenPerLine = 0;
			}
		}
	}

	if (hadNamed)
	{
		if (hasResiduals)
		{
			return SR_MIXED;
		}
		else
		{
			return SR_ALL_NAMED;
		}
	}
	else
	{
		if (hasResiduals)
		{
			return SR_NONE_NAMED;
		}
		else
		{
			return SR_EMPTY;
		}
	}
}


