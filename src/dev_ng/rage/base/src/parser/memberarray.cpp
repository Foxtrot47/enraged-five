// 
// parser/memberarray.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "memberarray.h"

#include "manager.h"
#include "optimisations.h"
#include "psofaketypes.h"
#include "structure.h"
#include "treenode.h"
#include "visitorinit.h"
#include "visitorutils.h"

#include "bank/bank.h"
#include "math/float16.h"
#include "math/simplemath.h"
#include "parsercore/utils.h"

#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberArray);			

int parMemberArray::sm_NoSanityChecks = 0;

void DefaultAllocateDelegate(void*, u32)
{
	parErrorf("Every parMemberArray must specify an allocation delegate (usually one of the parMemberArray::Create* functions");
}

void parMemberArray::Data::Init()
{
	StdInit();
	m_ElementSize = 0;
	m_NumElements = 0;
	m_PrototypeData = NULL;
	m_ResizeArray = NULL;
}

parMemberArray::parMemberArray(Data& data, parMember* prototype /* = NULL */)
: parMember(&data)
{
	if (prototype)
	{
		m_PrototypeMember = prototype;
		// this is a cheat: every parMember subclass has a Data* m_Data as its first member,
		// but the Data's aren't the same type. 
		GetData()->m_PrototypeData = prototype->GetCommonData();
	}
	else
	{
		parAssertf(m_Data, "Passed in a NULL-reference to data?");
		m_PrototypeMember = parStructure::CreateMemberFromMemberData(reinterpret_cast<parMember::CommonData*>(GetData()->m_PrototypeData));
	}
}

parMemberArray::~parMemberArray()
{
	using namespace parMemberArraySubType;
	if (GetSubtype() == SUBTYPE_VIRTUAL)
	{
		delete GetData()->m_VirtualReadCallback;
	}
	else if (!IsStatic())
	{
		delete GetData()->m_ResizeArray;
	}
	delete m_PrototypeMember;
}

u32 parMemberArray::GetCountFromPointerWithCount(parConstPtrToStructure structAddr) const
{
	using namespace parMemberArraySubType;
	switch((SubType)GetSubtype())
	{
	case SUBTYPE_POINTER_WITH_COUNT:
		return *reinterpret_cast<const u32*>(reinterpret_cast<const char*>(structAddr) + GetData()->m_CountMemberOffset);
	case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		return *reinterpret_cast<const u8*>(reinterpret_cast<const char*>(structAddr) + GetData()->m_CountMemberOffset);
	case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		return *reinterpret_cast<const u16*>(reinterpret_cast<const char*>(structAddr) + GetData()->m_CountMemberOffset);
	default:
		parErrorf("Invalid type for GetCountFromPointerWithCount");
		return 0;
	}

}

void parMemberArray::SetCountForPointerWithCount(parPtrToStructure structAddr, size_t newCount) const
{
	using namespace parMemberArraySubType;
	switch((SubType)GetSubtype())
	{
	case SUBTYPE_POINTER_WITH_COUNT:
		*reinterpret_cast<u32*>(reinterpret_cast<char*>(structAddr) + GetData()->m_CountMemberOffset) = (u32)newCount;
		break;
	case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		parAssertf(newCount < 256, "New count is too big (%" SIZETFMT "d) >= 256", newCount);
		*reinterpret_cast<u8*>(reinterpret_cast<char*>(structAddr) + GetData()->m_CountMemberOffset) = (u8)newCount;
		break;
	case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		parAssertf(newCount < 65536, "New count is too big (%" SIZETFMT "d) >= 256", newCount);
		*reinterpret_cast<u16*>(reinterpret_cast<char*>(structAddr) + GetData()->m_CountMemberOffset) = (u16)newCount;
		break;
	default:
		parErrorf("Invalid type for SetCountForPointerWithCount");
	}
}

int* parMemberArray::GetFixedArrayCountAddress(parPtrToStructure structAddr) const
{
	parAssertf((SubType)GetSubtype() == parMemberArraySubType::SUBTYPE_ATFIXEDARRAY, "Invalid type %d", GetSubtype());

	atFixedArray<char, 65535>& fakeFixedArray = GetMemberFromStruct<atFixedArray<char, 65535> >(structAddr);
	char* arrayAddr = fakeFixedArray.begin();

	// need to find the address of the counter (since the counter is stored after all the array elements)
	size_t arrayBytes = GetData()->m_ElementSize * GetData()->m_NumElements;
	arrayBytes = RoundUp<4>((int)arrayBytes);

	int* elementAddr = reinterpret_cast<int*>(arrayAddr + arrayBytes);

	return elementAddr;
}

const int* parMemberArray::GetFixedArrayCountAddress(parConstPtrToStructure structAddr) const
{
	return GetFixedArrayCountAddress(const_cast<parPtrToStructure>(structAddr)); 
}

void parMemberArray::GetArrayContentsAndCountFromStruct(parConstPtrToStructure structAddr, parConstPtrToArray& outAddr, size_t& outElements) const
{
	GetArrayContentsAndCountFromStruct(const_cast<parPtrToStructure>(structAddr), const_cast<parPtrToArray&>(outAddr), outElements);
}

void parMemberArray::GetArrayContentsAndCountFromStruct(parPtrToStructure structAddr, parPtrToArray& outAddr, size_t& outElements) const
{
	using namespace parMemberArraySubType;
	parPtrToArray arrayAddr = NULL;
	size_t numElements = 0;

	switch((SubType)GetSubtype())
	{
	case SUBTYPE_ATARRAY: 
		{
			parFake::AtArray16& arr = GetMemberFromStruct<parFake::AtArray16>(structAddr);
			numElements = arr.m_Count;
			if (numElements > 0) {
				arrayAddr = reinterpret_cast<parPtrToArray>(arr.m_Elements.m_Pointer);
			}
			else {
				arrayAddr = NULL;
			}
		}
		break;
	case SUBTYPE_ATARRAY_32BIT_IDX:
		{
			parFake::AtArray32& arr = GetMemberFromStruct<parFake::AtArray32 >(structAddr);
			numElements = arr.m_Count;
			if (numElements > 0) {
				arrayAddr = reinterpret_cast<parPtrToArray>(arr.m_Elements.m_Pointer);
			}
			else {
				arrayAddr = NULL;
			}
		}
		break;
	case SUBTYPE_ATFIXEDARRAY:
		{
			arrayAddr = reinterpret_cast<parPtrToArray>(GetPointerToMember(structAddr));
			const int* eltAddr = GetFixedArrayCountAddress(structAddr);
			numElements = (size_t)(*eltAddr);
			// Sanity check to make sure we're reading the right size value
			parAssertf(sm_NoSanityChecks || numElements <= GetData()->m_NumElements, "Sanity check failed, expected elements between 0 and %d and got %" SIZETFMT "d from array. Is the array size correct?", GetData()->m_NumElements, numElements);
		}
		break;
	case SUBTYPE_ATRANGEARRAY:
		{
			atRangeArray<char, 65535>& arr = GetMemberFromStruct<atRangeArray<char, 65535> >(structAddr);
			arrayAddr = reinterpret_cast<parPtrToArray>(&arr[0]);
			numElements = GetData()->m_NumElements;
		}
		break;
	case SUBTYPE_POINTER:
		{
			arrayAddr = GetMemberFromStruct<parPtrToArray>(structAddr);
			numElements = arrayAddr ? GetData()->m_NumElements : 0;
		}
		break;
	case SUBTYPE_POINTER_WITH_COUNT: 
	case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
	case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		{
			arrayAddr = GetMemberFromStruct<parPtrToArray>(structAddr);
			numElements = arrayAddr ? GetCountFromPointerWithCount(structAddr) : 0;
		}
		break;
	case SUBTYPE_MEMBER:
		{
			arrayAddr = reinterpret_cast<parPtrToArray>(GetPointerToMember(structAddr));
			numElements = GetData()->m_NumElements;
		}
		break;
	case SUBTYPE_VIRTUAL:
		parErrorf("Virtual arrays don't have array addresses or element counts");
		arrayAddr = NULL;
		numElements = 0;
		break;
	}

	outAddr = arrayAddr;
	outElements = numElements;
}

int parMemberArray::CountKids(parTreeNode* node) const
{
	switch((Type)m_PrototypeMember->GetType()) {
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_FLOAT:
	case parMemberType::TYPE_VECTOR2:
	case parMemberType::TYPE_VECTOR3:
	case parMemberType::TYPE_VECTOR4:
	case parMemberType::TYPE_VEC2V:
	case parMemberType::TYPE_VEC3V:
	case parMemberType::TYPE_VEC4V:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_UCHAR:
	case parMemberType::TYPE_USHORT:
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
	case parMemberType::TYPE_DOUBLE:
		if (node->HasData() && !node->GetChild())
		{
			return (int)(node->GetDataSize() / GetData()->m_ElementSize);
		}
		break;
	case parMemberType::TYPE_FLOAT16:
		if (node->HasData() && !node->GetChild())
		{
			// The XML will contain an array of floats, not an array of float16s
			return (int)(node->GetDataSize() / sizeof(float));
		}
		break;
	default:
		break;
	}

	// If we get here, just count child nodes
	int kidNum = 0;
	for(parTreeNode* kid = node->GetChild(); kid; kid = kid->GetSibling()) {
		kidNum++;
	}
	return kidNum;
}

bool parMemberArray::SetArraySize(parPtrToStructure structAddr, size_t size, u8 rawFlags) const
{
	using namespace parMemberArraySubType;
	parPtrToArray arrayAddr = NULL;
	bool success = true;

	atFixedBitSet8 flags;
	flags.SetBits(rawFlags);

	size_t oldNumElements = INT_MAX;
	size_t numElements = 0;

	if (GetSubtype() == SUBTYPE_VIRTUAL)
	{
		parErrorf("Can't set the size of a virtual array");
		return false;
	}

	if (!IsFixedSize() || GetSubtype() == SUBTYPE_POINTER)
	{
		GetArrayContentsAndCountFromStruct(structAddr, arrayAddr, oldNumElements);
	}

	if (GetSubtype() == SUBTYPE_POINTER)
	{
		if (size != 0 && size != (int)GetData()->m_NumElements)
		{
			parErrorf("Pointer-type array %s can have exactly 0 or %d elements, but %" SIZETFMT "d were specified", GetName(), (int)GetData()->m_NumElements, size);
			success = false;
		}
	}
	else if (IsFixedSize() && (int)GetData()->m_NumElements != size)
	{
		if (PARSER.Settings().GetFlag(parSettings::WARN_ON_FIXED_SIZE_ARRAY_MISMATCH))
		{
			parWarningf("Array %s is a fixed size and needs exactly %d elements, but %" SIZETFMT "d were specified", GetName(), (int)GetData()->m_NumElements, size);
			success = false;
		}
		else
		{
			return true;
		}
	}
	else if (!IsFixedSize() && HasMaxSize() && (int)GetData()->m_NumElements < size)
	{
		parErrorf("Array %s has a max size of %d elements, but %" SIZETFMT "d were in the file", GetName(), (int)GetData()->m_NumElements, size);
		success = false;
	}

	parAssertf(!HasExternalStorage() || GetData()->m_ResizeArray, "Every array with external storage must specify an allocation delegate");

	switch(GetSubtype())
	{
	case SUBTYPE_ATARRAY:
		Resize16BitAtArray(GetPointerToMember(structAddr), size, oldNumElements, rawFlags);
		break;
	case SUBTYPE_ATARRAY_32BIT_IDX:
		Resize32BitAtArray(GetPointerToMember(structAddr), size, oldNumElements, rawFlags);
		break;
	case SUBTYPE_POINTER:
		ResizeRawArray(GetPointerToMember(structAddr), size, oldNumElements, rawFlags);
		break;
	case SUBTYPE_POINTER_WITH_COUNT:
	case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
	case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		ResizeRawArray(GetPointerToMember(structAddr), size, oldNumElements, rawFlags);
		SetCountForPointerWithCount(structAddr, size);
		break;
	case SUBTYPE_ATFIXEDARRAY:
		{
			int* elementAddr = GetFixedArrayCountAddress(structAddr);
			*elementAddr = Min((int)size, (int)GetData()->m_NumElements);
		}
		break;
	default:
		// Don't do any allocation
		break;
	}

	// if the array grew, init the new elements
	if (flags.IsSet(INITIALIZE_NEW_ELEMENTS) && (!IsFixedSize() || GetSubtype() == SUBTYPE_POINTER))
	{
		// Count the real number of elements
		GetArrayContentsAndCountFromStruct(structAddr, arrayAddr, numElements);

		for(size_t i = oldNumElements; i < numElements; i++)
		{
			parPtrToStructure elementAddr = GetAddressOfNthElement(arrayAddr, i);
			parInitVisitor initter;
			initter.VisitMember(elementAddr, *m_PrototypeMember);
		}
	}

	return success;
}

void parMemberArray::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	using namespace parMemberArraySubType;
	size_t numElements = 0;
	parPtrToArray arrayAddr = NULL;

	bool isVirtual = (GetSubtype() == SUBTYPE_VIRTUAL);
	parPtrToArray virtualStorage = NULL;

	// create array if necessary, init new members
	if (!isVirtual)
	{
		int kids = CountKids(node);
		if (!SetArraySize(structAddr, kids))
		{
			parErrorf("Couldn't set the array size for node %s, child of %s (#kids = %d, structAddr = %p)", node->GetElement().GetName(), node->GetParent() ? node->GetParent()->GetElement().GetName() : "(null)", kids, structAddr);
		}

		GetArrayContentsAndCountFromStruct(structAddr, arrayAddr, numElements);
	}
	else
	{
		virtualStorage = reinterpret_cast<parPtrToArray>(AllocaAligned(char, m_PrototypeMember->GetSize(), 16));
	}

	// Check the encoding type to make sure it's what we expect
#if __ASSERT
	if (node->HasData())
	{
		parStream::DataEncoding actualEncoding = node->FindDataEncoding();
		parStream::DataEncoding expectedEncoding = parStream::UNSPECIFIED;
		switch((Type)m_PrototypeMember->GetType())
		{
		case parMemberType::TYPE_INT:		expectedEncoding = parStream::INT_ARRAY;		break;
		case parMemberType::TYPE_UINT:		expectedEncoding = parStream::INT_ARRAY;		break;
		case parMemberType::TYPE_PTRDIFFT:	expectedEncoding = parStream::INT_ARRAY;		break;
		case parMemberType::TYPE_SIZET:		expectedEncoding = parStream::INT_ARRAY;		break;
		case parMemberType::TYPE_CHAR:		expectedEncoding = parStream::CHAR_ARRAY;		break;
		case parMemberType::TYPE_SHORT:		expectedEncoding = parStream::SHORT_ARRAY;		break;
		case parMemberType::TYPE_UCHAR:		expectedEncoding = parStream::CHAR_ARRAY;		break;
		case parMemberType::TYPE_USHORT:	expectedEncoding = parStream::SHORT_ARRAY;		break;
		case parMemberType::TYPE_FLOAT:		expectedEncoding = parStream::FLOAT_ARRAY;		break;
		case parMemberType::TYPE_FLOAT16:	expectedEncoding = parStream::FLOAT_ARRAY;		break;
		case parMemberType::TYPE_VECTOR2:	expectedEncoding = parStream::VECTOR2_ARRAY;	break;
		case parMemberType::TYPE_VECTOR3:	expectedEncoding = parStream::VECTOR3_ARRAY;	break;
		case parMemberType::TYPE_VECTOR4:	expectedEncoding = parStream::VECTOR4_ARRAY;	break;
		case parMemberType::TYPE_VEC2V:		expectedEncoding = parStream::VEC2V_ARRAY;		break;
		case parMemberType::TYPE_VEC3V:		expectedEncoding = parStream::VECTOR3_ARRAY;	break;
		case parMemberType::TYPE_VEC4V:		expectedEncoding = parStream::VECTOR4_ARRAY;	break;
		case parMemberType::TYPE_INT64:		expectedEncoding = parStream::INT64_ARRAY;		break;
		case parMemberType::TYPE_UINT64:	expectedEncoding = parStream::INT64_ARRAY;		break;
		case parMemberType::TYPE_DOUBLE:	expectedEncoding = parStream::DOUBLE_ARRAY;		break;
		default:
			// nothing
			break;
		}
		parAssertf(actualEncoding == expectedEncoding, "Found an unexpected encoding for node %s, child of %s. Expected \"%s\" and found \"%s\" in the file", node->GetElement().GetName(), node->GetParent() ? node->GetParent()->GetElement().GetName() : "null", parStream::FindEncodingName(expectedEncoding), parStream::FindEncodingName(actualEncoding));
	}
#endif

	bool readFromDataArray = false;

	switch((Type)m_PrototypeMember->GetType())
	{
	case parMemberType::TYPE_FLOAT16:
		// Special case here - the data is encoded as 32 bit floats, we need to re-convert to 16 bit
		if (node->HasData() && !node->GetChild())
		{
			size_t allocatedSpace = numElements * GetData()->m_ElementSize;
			float* floats = reinterpret_cast<float*>(node->GetData());
			Float16* halfs = reinterpret_cast<Float16*>(arrayAddr);
			for(u32 i = 0; i < numElements; i++)
			{
				halfs[i] = Float16(floats[i]);
			}

			if (IsFixedSize() && node->GetDataSize() < allocatedSpace)
			{
				for(int i = node->GetDataSize() / (int)GetData()->m_ElementSize; i < (int)GetData()->m_NumElements; i++) 
				{
					parPtrToStructure elementAddr = GetAddressOfNthElement(arrayAddr, i);
					parInitVisitor initter;
					initter.VisitMember(elementAddr, *m_PrototypeMember);
				}
			}
			readFromDataArray = true;
		}
		break;
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_UCHAR:
	case parMemberType::TYPE_USHORT:
	case parMemberType::TYPE_FLOAT:
	case parMemberType::TYPE_VECTOR2:
	case parMemberType::TYPE_VECTOR3:
	case parMemberType::TYPE_VECTOR4:
	case parMemberType::TYPE_VEC2V:
	case parMemberType::TYPE_VEC3V:
	case parMemberType::TYPE_VEC4V:
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
	case parMemberType::TYPE_DOUBLE:
		if (node->HasData() && !node->GetChild())
		{
			size_t allocatedSpace = numElements * GetData()->m_ElementSize;
			sysMemCpy(arrayAddr, node->GetData(), Min<size_t>(node->GetDataSize(), allocatedSpace));
			if (IsFixedSize() && node->GetDataSize() < allocatedSpace)
			{
				for(int i = node->GetDataSize() / (int)GetData()->m_ElementSize; i < (int)GetData()->m_NumElements; i++) 
				{
					parPtrToStructure elementAddr = GetAddressOfNthElement(arrayAddr, i);
					parInitVisitor initter;
					initter.VisitMember(elementAddr, *m_PrototypeMember);
				}
			}
			readFromDataArray = true;
		}
		break;
	default:
		break;
	}

	if (!readFromDataArray)
	{
		int i = 0;
		if (isVirtual)
		{
			parPtrToStructure elementAddr = GetAddressOfNthElement(virtualStorage, 0);
			parVirtualReadData cbData;
			cbData.m_MemberMetadata = m_PrototypeMember;
			cbData.m_Cookie = NULL;

			parInitVisitor initter;
			parDeleteAllVisitor deleter(true);

			for(parTreeNode* kid = node->GetChild(); kid != NULL; kid = kid->GetSibling())
			{
				sysMemSet(virtualStorage, 0xa1, m_PrototypeMember->GetSize());

				// need to do placement new for contained structures, or else trying to init the structure will call
				// GetConcreteStructure, calling a virtual function where the vptr comes from this allocaed block, and
				// the game crashes.
				if (m_PrototypeMember->GetType() == parMemberType::TYPE_STRUCT)
				{
					parMemberStruct& memStr = m_PrototypeMember->AsStructOrPointerRef();
					if (memStr.GetSubtype() == parMemberStructSubType::SUBTYPE_STRUCTURE)
					{
						memStr.GetBaseStructure()->Place(memStr.GetObjAddr(elementAddr));
					}
				}


				initter.VisitMember(elementAddr, *m_PrototypeMember);
				m_PrototypeMember->ReadTreeNode(kid, elementAddr);
				GetData()->m_VirtualReadCallback->Retarget(elementAddr);
				cbData.m_Index = i;
				i++;
				bool deleteWhenFinished = (*(GetData()->m_VirtualReadCallback))(elementAddr, cbData); 
				if (deleteWhenFinished)
				{
					deleter.VisitMember(elementAddr, *m_PrototypeMember);
				}
			}
		}
		else
		{
			for(parTreeNode* kid = node->GetChild(); kid != NULL; kid = kid->GetSibling())
			{
				parPtrToStructure elementAddr = GetAddressOfNthElement(arrayAddr, i);
				m_PrototypeMember->ReadTreeNode(kid, elementAddr);
				i++;
				if (HasMaxSize() && i == (int)GetData()->m_NumElements) {
					break; // stop now or we might overwrite memory.
				}
			}
		}
		/* Zero out any missing elements */
		if (IsFixedSize() && !isVirtual) {
			for(; i < (int)GetData()->m_NumElements; i++) 
			{
				parPtrToStructure elementAddr = GetAddressOfNthElement(arrayAddr, i);
				parInitVisitor initter;
				initter.VisitMember(elementAddr, *m_PrototypeMember);
			}
		}
	}

}

// Requirements for a 2d table:
//   PrototypeMember must be a struct or a simple_owner pointer (we'd need to handle NULL pointers)
//   The struct must be composed of 'POD' types. For purposes of the table POD types are:
//		Float, Int, Bool, enum
//		Vector2, 3, 4
//		String or Wide string (utf-8 encoded)
//		external_named pointers
//	 In the future we may allow:
//		Matrices?
//		Other POD structures
//	  It also must use less than PAR_MAX_TABLE_COLUMNS columns
bool parMemberArray::Is2dTable()
{
	if (m_PrototypeMember->GetType() != parMemberType::TYPE_STRUCT) {
		return false;
	}

	parMemberStruct& strMem = m_PrototypeMember->AsStructOrPointerRef();

	if (!(strMem.GetSubtype() == parMemberStructSubType::SUBTYPE_STRUCTURE || 
		strMem.GetSubtype() == parMemberStructSubType::SUBTYPE_SIMPLE_POINTER))
	{
		return false;
	}

	parStructure* str = strMem.GetBaseStructure();

	// check the members of str:
	int columns = 0;
	for(int i = 0; i < str->GetNumMembers(); i++)
	{
		switch(str->GetMember(i)->GetType())
		{
			// OK types:
		case parMemberType::TYPE_BOOL:
		case parMemberType::TYPE_CHAR:
		case parMemberType::TYPE_ENUM:
		case parMemberType::TYPE_BITSET:
		case parMemberType::TYPE_FLOAT:
		case parMemberType::TYPE_FLOAT16:
		case parMemberType::TYPE_INT:
		case parMemberType::TYPE_SHORT:
		case parMemberType::TYPE_STRING:
		case parMemberType::TYPE_UCHAR:
		case parMemberType::TYPE_UINT:
		case parMemberType::TYPE_PTRDIFFT:
		case parMemberType::TYPE_SIZET:
		case parMemberType::TYPE_USHORT:
		case parMemberType::TYPE_SCALARV:
		case parMemberType::TYPE_BOOLV:
		case parMemberType::TYPE_INT64:
		case parMemberType::TYPE_UINT64:
		case parMemberType::TYPE_DOUBLE:
			columns++;
			break;
		case parMemberType::TYPE_VECTOR2:
		case parMemberType::TYPE_VEC2V:
			columns += 2;
			break;
		case parMemberType::TYPE_VECTOR3:
		case parMemberType::TYPE_VEC3V:
			columns += 3;
			break;
		case parMemberType::TYPE_VECTOR4:
		case parMemberType::TYPE_VEC4V:
		case parMemberType::TYPE_VECBOOLV:
			columns += 4;
			break; // keep going
		case parMemberType::TYPE_STRUCT: // external_named pointers are OK
			{
				parMemberStruct& memStr = str->GetMember(i)->AsStructOrPointerRef();
				if (!memStr.IsExternalPointer())
				{
					return false;
				}
			}
			columns++;
			break;
		default:
			// assume everything else causes a problem
			return false;
		}
	}

	return columns < MAX_TABLE_COLUMNS;
}



void parMemberArray::SkipWhitespace(const char*& csvData, bool skipNl)
{
	while(*csvData != '\0' && (*csvData == ' ' || *csvData == '\t' || (skipNl && *csvData == '\n')))
	{
		++csvData;
	}
}

bool parMemberArray::ReadCsvDatum(char* buf, unsigned* bufLen, const char*& csvData)
{
	const unsigned maxLen = *bufLen;
	SkipWhitespace(csvData);
	*bufLen = 0;

	// bool quoted = false;
	if (*csvData == '"')
	{
		// quoted = true;
		++csvData;
		while(*csvData != '\0')
		{
			if (*csvData == '"')
			{
				// is the next char a quote too?
				if(*(csvData+1) != '"')
				{
					++csvData; // skip the end quote
					break;
				}
				else if(!AssertVerify(*bufLen < maxLen-1))
				{
					break;
				}
				else
				{
					++csvData; // skip the extra quote
					buf[(*bufLen)++] = '"';
				}
			}
			else if(!AssertVerify(*bufLen < maxLen-1))
			{
				break;
			}
			else
			{
				buf[(*bufLen)++] = *csvData;
			}
			++csvData;
		}
	}
	else
	{
		// read up to \n or ,
		while(*csvData != '\0')
		{
			if (*csvData == ',' || *csvData == '\n')
			{
				break;
			}
			else if(!AssertVerify(*bufLen < maxLen-1))
			{
				break;
			}
			else
			{
				buf[(*bufLen)++] = *csvData;
			}
			++csvData;
		}
	}

	buf[*bufLen] = '\0';

	SkipWhitespace(csvData);

	switch(*csvData)
	{
	case ',':  ++csvData; return false;
	case '\n': SkipWhitespace(csvData, true); return true; // following a \n, skip any empty rows
	case '\0': return true;
	default:
		parErrorf("Badly formed CSV");
		return true;
	}
}


void parMemberArray::LoadExtraAttributes(parTreeNode* node)
{
	parMember::LoadExtraAttributes(node);

	parTreeNode* contentsNode = node->FindChildWithName("Contents");
	if (contentsNode && m_PrototypeMember)
	{
		m_PrototypeMember->LoadExtraAttributes(contentsNode);
	}
}

size_t rage::parMemberArray::GetSize() const
{
	using namespace parMemberArraySubType;
	switch(GetSubtype())
	{
	case SUBTYPE_ATARRAY:
		return sizeof(atArray<char>);
	case SUBTYPE_ATARRAY_32BIT_IDX:
		return sizeof(atArray<char, 0, u32>);
	case SUBTYPE_ATFIXEDARRAY:
		return AlignPow2(GetData()->m_ElementSize * GetData()->m_NumElements + sizeof(int), m_PrototypeMember->FindAlign());
	case SUBTYPE_ATRANGEARRAY:
	case SUBTYPE_MEMBER:
		return GetData()->m_ElementSize * GetData()->m_NumElements;
	case SUBTYPE_POINTER:
		return sizeof(char*);
	case SUBTYPE_POINTER_WITH_COUNT:
	case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
	case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		return sizeof(char*);
	case SUBTYPE_VIRTUAL:
		return 0;
	}
	return 0;
}

size_t rage::parMemberArray::FindAlign() const
{
	using namespace parMemberArraySubType;
	switch(GetSubtype())
	{
	case SUBTYPE_ATARRAY:
		return __alignof(atArray<char>);
	case SUBTYPE_ATARRAY_32BIT_IDX:
		return __alignof(atArray<char, 0, u32>);
	case SUBTYPE_ATFIXEDARRAY:
		return Max(__alignof(int), m_PrototypeMember->FindAlign());
	case SUBTYPE_ATRANGEARRAY:
	case SUBTYPE_MEMBER:
		return m_PrototypeMember->FindAlign();
	case SUBTYPE_POINTER:
	case SUBTYPE_POINTER_WITH_COUNT:
	case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
	case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		return __alignof(char*);
	case SUBTYPE_VIRTUAL:
		return 0;
	}
	return 0;
}

u32 parMemberArray::GetAlignmentPower() const
{
	u16 flags = GetData()->m_Flags;
	return (flags >> parMemberFlags::MEMBERARRAY_ALIGN_SHIFT) & (parMemberFlags::MEMBERARRAY_ALIGN_MASK);
}

size_t parMemberArray::FindContentAlign() const
{
	size_t nativeContentAlignment = m_PrototypeMember->FindAlign();
	size_t specifiedAlignment = 1 << (size_t)GetAlignmentPower();

	return Max(nativeContentAlignment, specifiedAlignment);
}


parPtrToStructure parMemberArray::GetAddressOfNthElement(parPtrToArray arrayContentsAddr, size_t i) const
{
	return reinterpret_cast<parPtrToStructure>(arrayContentsAddr + (i * GetData()->m_ElementSize));
}

bool parMemberArray::DeleteItem(parPtrToStructure structAddr, size_t i)
{
	if (IsFixedSize())
	{
		parAssertf(0, "Can't delete an item from a fixed size array (%s)", GetName());
		return false;
	}

	parPtrToArray arrayData = NULL;
	size_t arrayCount = 0;
	GetArrayContentsAndCountFromStruct(structAddr, arrayData, arrayCount);

	if (i >= (int)arrayCount)
	{
		parErrorf("Index %" SIZETFMT "d is out of bounds!", i);
		return false;
	}

	parPtrToStructure elementAddr = GetAddressOfNthElement(arrayData, i);

	parDeleteMember(elementAddr, *GetPrototypeMember());

	// shift everything down one, modify the count
	char* startOfDeletedItem = reinterpret_cast<char*>(arrayData) + (i * m_PrototypeMember->GetSize());
	char* endOfDeletedItem = reinterpret_cast<char*>(arrayData) + ((i+1) * m_PrototypeMember->GetSize());
	char* endOfArray = reinterpret_cast<char*>(arrayData) + (arrayCount * m_PrototypeMember->GetSize());

	memmove(startOfDeletedItem, endOfDeletedItem, endOfArray - endOfDeletedItem);

	SetArraySize(structAddr, arrayCount-1, 0); // flags = 0 => do not run destructor on the last element (since we just shifted everything down)

	return true;
}

bool parMemberArray::InsertItem(parPtrToStructure structAddr, size_t i, bool initNewElement /* = true */)
{
	if (IsFixedSize())
	{
		parAssertf(0, "Can't insert an item from a fixed size array (%s)", GetName());
		return false;
	}

	parPtrToArray arrayData = NULL;
	size_t origArrayCount = 0;
	GetArrayContentsAndCountFromStruct(structAddr, arrayData, origArrayCount);

	if (i > (int)origArrayCount)
	{
		if (i == INSERT_AT_END)
		{
			i = origArrayCount;
		}
		else
		{
			parErrorf("Index %" SIZETFMT "d is out of bounds!", i);
			return false;
		}
	}

	// grow the array and shift all the elements up to insert the new item
	SetArraySize(structAddr, origArrayCount+1, (1 << CONSTRUCT_NEW_ELEMENTS));

	// arrayData may have changed, so re-get it.
	size_t newArrayCount = 0;
	GetArrayContentsAndCountFromStruct(structAddr, arrayData, newArrayCount);

	char* startOfFirstMovedItem = reinterpret_cast<char*>(arrayData) + (i * m_PrototypeMember->GetSize());
	char* endOfMovedItems = reinterpret_cast<char*>(arrayData) + (origArrayCount * m_PrototypeMember->GetSize());
	char* newLocationForMovedItem = reinterpret_cast<char*>(arrayData) + ((i+1) * m_PrototypeMember->GetSize());

	memmove(newLocationForMovedItem, startOfFirstMovedItem, endOfMovedItems-startOfFirstMovedItem);

	parPtrToStructure elementAddr = GetAddressOfNthElement(arrayData, i);

	if (initNewElement)
	{
		parInitVisitor vis;
		vis.VisitMember(elementAddr, *m_PrototypeMember);
	}

	return true;
}

parPtrToArray parMemberArray::AllocStorage(size_t size, size_t align) const
{
	size_t sizeInBytes = AlignPow2(size, align);

	parPtrToArray storage = NULL;

	if (GetTypeFlags().IsSet(parMemberArrayData::FLAG_ALLOCATE_IN_PHYSICAL_MEMORY))
	{
		storage = reinterpret_cast<parPtrToArray>(parUtils::PhysicalAllocate(sizeInBytes, align));
		memset(storage, 0x0, sizeInBytes); // can't use sysMemSet here, ZeroDC() crashes when hitting uncached mem
	}
	else
	{
		storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(align) char[sizeInBytes]);
		sysMemSet(storage, 0x0, sizeInBytes);
	}

	return storage;
}

void parMemberArray::FreeStorage(parPtrToArray ptr) const
{
	if (GetTypeFlags().IsSet(parMemberArrayData::FLAG_ALLOCATE_IN_PHYSICAL_MEMORY))
	{
		parUtils::PhysicalFree(reinterpret_cast<char*>(ptr));
	}
	else
	{
		delete [] reinterpret_cast<char*>(ptr);
	}
}

void parMemberArray::Resize16BitAtArray(parPtrToMember arrayAddr, size_t count, size_t /*oldcount*/, u8 rawFlags) const
{
	FastAssert(count <= UINT_MAX);
	parFake::AtArray16& fakeArray = *reinterpret_cast<parFake::AtArray16*>(arrayAddr);
	parPtrToArray arrayContents = reinterpret_cast<parPtrToArray>(fakeArray.m_Elements.m_Pointer);

	atFixedBitSet8 flags;
	flags.SetBits(rawFlags);

	if (count == fakeArray.m_Count)
	{
		// do nothing
	}
	else if (count == 0)
	{
		if (flags.IsSet(DESTRUCT_OLD_ELEMENTS))
		{
			parDeleteAllVisitor::DestructArrayElements(arrayContents, 0, fakeArray.m_Count, *this);
		}
		FreeStorage(arrayContents);
		fakeArray.m_Elements.m_Pointer = NULL;
		fakeArray.m_Capacity = fakeArray.m_Count = 0;
	}
	else if (count < fakeArray.m_Count)
	{
		if (flags.IsSet(DESTRUCT_OLD_ELEMENTS))
		{
			parDeleteAllVisitor::DestructArrayElements(arrayContents, count, fakeArray.m_Count, *this);
}
		fakeArray.m_Count = (u16)count;
	}
	else if (count <= fakeArray.m_Capacity)
	{
		if (flags.IsSet(CONSTRUCT_NEW_ELEMENTS))
		{
			parMemberPlacementVisitor::ConstructArrayElements(arrayContents, fakeArray.m_Count, count, *this);
		}
		fakeArray.m_Count = (u16)count;
	}
	else
	{
		// Reallocate array

		parPtrToArray storage = AllocStorage(GetData()->m_ElementSize * count, FindContentAlign());

		if (arrayContents)
		{
			memcpy(storage, arrayContents, fakeArray.m_Count * GetData()->m_ElementSize);
		}
		if (flags.IsSet(CONSTRUCT_NEW_ELEMENTS))
		{
			parMemberPlacementVisitor::ConstructArrayElements(storage, fakeArray.m_Count, count, *this);
		}

		FreeStorage(arrayContents);

		// Init the array variables (not really sure why placement_new here and not just setting psoFakeArray fields)
		rage_placement_new(arrayAddr) atUserArray<char, 16, u16>(reinterpret_cast<char*>(storage), (u16)count, true);
	}
}

void parMemberArray::Resize32BitAtArray(parPtrToMember arrayAddr, size_t count, size_t /*oldcount*/, u8 rawFlags) const
{
	parFake::AtArray32& fakeArray = *reinterpret_cast<parFake::AtArray32*>(arrayAddr);
	parPtrToArray arrayContents = reinterpret_cast<parPtrToArray>(fakeArray.m_Elements.m_Pointer);

	atFixedBitSet8 flags;
	flags.SetBits(rawFlags);

	if (count == fakeArray.m_Count)
	{
		// do nothing
	}
	else if (count == 0)
	{
		if (flags.IsSet(DESTRUCT_OLD_ELEMENTS))
		{
			parDeleteAllVisitor::DestructArrayElements(arrayContents, 0, fakeArray.m_Count, *this);
		}
		FreeStorage(arrayContents);
		fakeArray.m_Elements.m_Pointer = NULL;
		fakeArray.m_Capacity = fakeArray.m_Count = 0;
	}
	else if (count < fakeArray.m_Count)
	{
		if (flags.IsSet(DESTRUCT_OLD_ELEMENTS))
		{
			parDeleteAllVisitor::DestructArrayElements(arrayContents, count, fakeArray.m_Count, *this);
		}
		fakeArray.m_Count = (u32)count;
	}
	else if (count <= fakeArray.m_Capacity)
	{
		if (flags.IsSet(CONSTRUCT_NEW_ELEMENTS))
		{
			parMemberPlacementVisitor::ConstructArrayElements(arrayContents, fakeArray.m_Count, count, *this);
		}
		fakeArray.m_Count = (u32)count;
	}
	else
	{
		// Reallocate array
		parPtrToArray storage = AllocStorage(GetData()->m_ElementSize * count, FindContentAlign());

		if (arrayContents)
		{
			memcpy(storage, arrayContents, fakeArray.m_Count * GetData()->m_ElementSize);
		}
		if (flags.IsSet(CONSTRUCT_NEW_ELEMENTS))
		{
			parMemberPlacementVisitor::ConstructArrayElements(storage, fakeArray.m_Count, count, *this);
		}

		FreeStorage(arrayContents);

		// Sets elements to storage, count and capacity to count
		rage_placement_new(arrayAddr) atUserArray<char, 16, u32>(reinterpret_cast<char*>(storage), (u16)count, true);
	}
}

bool parMemberArray::NeedsArrayCookie(parMemberType::Enum type, u16 subtype)
{
	// TODO: I really need some kind of traits table for this kind of stuff.
	switch(type)
	{
	case parMemberType::TYPE_ARRAY:
		return subtype == parMemberArraySubType::SUBTYPE_ATARRAY ||
			subtype == parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX;
	case parMemberType::TYPE_BITSET:
		return subtype == parMemberBitsetSubType::SUBTYPE_ATBITSET;
	case parMemberType::TYPE_MAP:
		return true;
	case parMemberType::TYPE_STRING:
		return subtype == parMemberStringSubType::SUBTYPE_ATSTRING ||
			subtype == parMemberStringSubType::SUBTYPE_ATWIDESTRING;
	case parMemberType::TYPE_STRUCT:
		return subtype == parMemberStructSubType::SUBTYPE_STRUCTURE;
	default:
		return false;
	}
}

void parMemberArray::ResizeRawArray(parPtrToMember arrayAddr, size_t count, size_t oldcount, u8 rawFlags) const
{
	parPtrToArray& arrRef = *(parPtrToArray*)arrayAddr;
	atFixedBitSet8 flags;
	flags.SetBits(rawFlags);

	// If this type needs an array cookie, we need to create and delete the array using one of the "EmptyN" 
	// instances below, or else the memory system won't know about the cookie.
	bool needsCookie = NeedsArrayCookie(m_PrototypeMember->GetType(), m_PrototypeMember->GetCommonData()->m_Subtype);

	// TODO: This needs to be fixed. FindContentAlign() will return the alignment based on the parser's best information
	// but we should be able to specify or compute stricter alignment for structures. For example consider a structure that has one serialized u8
	// and one non-serialized Vec4V. The parser will think the alignment is 1 when it should be 16. We need to detect this somehow,
	// and possibly store alignment as a structure property the way size is.
	size_t contentAlignment = FindContentAlign();  


	struct Empty1 { ~Empty1(){} u8 a; };
	struct Empty2 { ~Empty2(){} u16 a; };
	struct Empty4 { ~Empty4(){} u32 a; };
	struct Empty8 { ~Empty8(){} u64 a; };
	struct Empty16 { ~Empty16(){} u128 a;	};

	if (count == 0)
	{
		if (arrRef)
		{
			if (flags.IsSet(DESTRUCT_OLD_ELEMENTS))
			{
				parDeleteAllVisitor::DestructArrayElements(arrRef, 0, oldcount, *this);
			}

			if (needsCookie)
			{
				// TODO: What do I do about freeing from physical memory?!
				switch(contentAlignment)
				{
				case 1:
					delete [] reinterpret_cast<Empty1*>(arrRef);
					break;
				case 2:
					delete [] reinterpret_cast<Empty2*>(arrRef);
					break;
				case 4:
					delete [] reinterpret_cast<Empty4*>(arrRef);
					break;
				case 8:
					delete [] reinterpret_cast<Empty8*>(arrRef);
					break;
				case 16:
					delete [] reinterpret_cast<Empty16*>(arrRef);
					break;
				}
			}
			else
			{
				FreeStorage(arrRef);
			}

			arrRef = NULL;
		}
	}
	else if (count != oldcount)
	{
		size_t storageSize = GetData()->m_ElementSize * count;

		parPtrToArray storage = NULL;
		if (needsCookie)
		{
			size_t sizeInBytes = AlignPow2(storageSize, contentAlignment);
			size_t allocUnits = 0;


			switch(contentAlignment)
			{
			case 1:
				allocUnits = sizeInBytes / sizeof(Empty1);
				storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(contentAlignment) Empty1[allocUnits]);
				break;
			case 2:
				allocUnits = sizeInBytes / sizeof(Empty2);
				storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(contentAlignment) Empty2[allocUnits]);
				break;
			case 4:
				allocUnits = sizeInBytes / sizeof(Empty4);
				storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(contentAlignment) Empty4[allocUnits]);
				break;
			case 8:
				allocUnits = sizeInBytes / sizeof(Empty8);
				storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(contentAlignment) Empty8[allocUnits]);
				break;
			case 16:
				allocUnits = sizeInBytes / sizeof(Empty16);
				storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(contentAlignment) Empty16[allocUnits]);
				break;
			default:
				Assertf(0, "This function doesn't handle alignment of %" SIZETFMT "d yet", contentAlignment);
			}

			sysMemSet(storage, 0x0, sizeInBytes);
			parUtils::SetArrayCookie(storage, contentAlignment, allocUnits, count);
		}
		else
		{
			storage = AllocStorage(storageSize, contentAlignment);
		}


		if (count < oldcount)
		{
			sysMemCpy(storage, arrRef, storageSize); // copy the first 'count' elements from the old array to the new
		}
		else
		{
			sysMemCpy(storage, arrRef, GetData()->m_ElementSize * oldcount); // copy all the 'oldcount' elements from the old array to the new
			if (flags.IsSet(CONSTRUCT_NEW_ELEMENTS))
			{
				parMemberPlacementVisitor::ConstructArrayElements(storage, oldcount, count, *this); // and init the rest of the elements
			}
		}

		if (arrRef)
		{
			if (flags.IsSet(DESTRUCT_OLD_ELEMENTS))
			{
				parDeleteAllVisitor::DestructArrayElements(arrRef, 0, count, *this);
			}
			FreeStorage(arrRef);
		}

		arrRef = storage;
	}
}
