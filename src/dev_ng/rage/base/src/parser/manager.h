// 
// parser/manager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MANAGER_H
#define PARSER_MANAGER_H

#include "macros.h"

#if __SPU
namespace rage
{
	class parTreeNode {};
}
#else
#include "formatinfo.h"
#include "extstructure.h"
#include "rtstructure.h"
#include "structure.h"
#include "tree.h"
#include "treenode.h"
#include "visitortree.h"
#include "visitorwidgets.h"

#include "atl/hashstring.h"
#include "atl/map.h"
#include "atl/singleton.h"
#include "diag/channel.h"
#include "file/asset.h"
#include "file/stream.h"
#include "parsercore/settings.h"
#include "string/string.h"
#include "system/memory.h"
#include "system/typeinfo.h"

namespace rage {
	class bkBank;
	class fiStream;
	class parTree;
	class parTreeNode;
	class bkBank;
	class parStreamIn;
	class parStreamOut;

class parManager;
class parExternalStructureManager;

// PURPOSE:
//	parManager is the main point of access into the parser system. To load or save files, to register
//	structures for serialization or for registering new file formats.
//  Most of the I/O methods in parManager come are overload to, for example, operate on a filename, stream, or parTree.
class parManager {
public:
	friend class parStructure;

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
// Raw DOM-style access to documents
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Loads a file and creates a tree representation of the file.
	// PARAMS:
	//		file - The name of the file to load
	//		ext - The extention of the file to load. Use "" if the file parameter already contains the extension.
	//		stream - an fiStream opened for reading.
	parTree* LoadTree(const char* file, const char* ext, const parSettings* settings = NULL) const;
	parTree* LoadTree(fiStream* stream, const parSettings* settings = NULL) const;		//	<COMBINE parManager::LoadTree@const char*@const char*>
	parTree* LoadTree(parStreamIn* stream) const;		//	<COMBINE parManager::LoadTree@const char*@const char*>

	// PURPOSE: Saves a tree out to the specified file or stream
	// PARAMS:
	//		file - The name of the file to save to
	//		ext - The extension of the file. Use "" if the file parameter already contains the extension.
	//		stream - A fiStream opened for writing.
	//		tree - The parTree to save
	//		fmt - The format to use when saving.
	bool SaveTree(const char* file, const char* ext, parTree* tree, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	bool SaveTree(fiStream* stream, parTree* tree, int fmt = UNKNOWN, const parSettings* settings = NULL) const; // <COMBINE parManager::SaveTree@const char*@const char*@parTree*@int>
	bool SaveTree(parStreamOut* stream, parTree* tree, int fmt = UNKNOWN) const; // <COMBINE parManager::SaveTree@const char*@const char*@parTree*@int>

	bool SaveTree(const char* file, const char* ext, parTreeNode* treenode, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	bool SaveTree(fiStream* stream, parTreeNode* treenode, int fmt = UNKNOWN, const parSettings* settings = NULL) const; // <COMBINE parManager::SaveTree@const char*@const char*@parTree*@int>
	bool SaveTree(parStreamOut* stream, parTreeNode* treenode, int fmt = UNKNOWN) const; // <COMBINE parManager::SaveTree@const char*@const char*@parTree*@int>


	// PURPOSE: Given a file name, create and return a new input stream object.
	// PARAMS:
	//		file - The name of the file to load
	//		ext - The extention of the file to load. Use "" if the file parameter already contains the extension.
	//		stream - an fiStream opened for reading.
	parStreamIn* OpenInputStream(const char* file, const char* ext, const parSettings* settings = NULL) const;
	parStreamIn* OpenInputStream(fiStream* stream, const parSettings* settings = NULL) const; // <COMBINE parManager::OpenInputStream@const char*@const char*>

	// PURPOSE: Create and return a new output stream object (a new instance of a parStreamOut subclass)
	// PARAMS:
	//		file - The name of the file to save to
	//		ext - The extension of the file. Use "" if the file parameter already contains the extension.
	//		stream - A fiStream opened for writing.
	//		fmt - The format to use when saving.
	parStreamOut* OpenOutputStream(const char* file, const char* ext, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	parStreamOut* OpenOutputStream(fiStream* stream, int fmt = UNKNOWN, const parSettings* settings = NULL) const; // <COMBINE parManager::OpenOutputStream@const char*@const char*@int>

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
// Loading and saving objects using reflection information
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Load an object from a file. The object in the file  must be of type _Type or any subclass.
	// PARAMS:
	//		_Type - The base class to load. Must be registered with the parManager first.
	//		file - The name of the file to load from
	//		ext - The file extension (or "" if the extension is already part of the file name)
	//		stream - The stream to load from.
	//		node - The node to load from.
	//		obj - The pointer to load the object into.
	// RETURNS:
	//		true on success.
	// NOTES:
	//		This function will allocate space for the object it's loading, so obj shouldn't point to any allocated data
	//		before LoadObjectPtr is called.
	template<typename _Type> bool LoadObjectPtr(const char* file, const char* ext, _Type*& obj, const parSettings* settings = NULL) const;
	template<typename _Type> bool LoadObjectPtr(fiStream* stream, _Type*& obj, const parSettings* settings = NULL) const;  // <COMBINE parManager::LoadObjectPtr@const char*@const char*@_Type*&>
	template<typename _Type> bool LoadObjectPtr(parStreamIn* stream, _Type*& obj) const; // <COMBINE parManager::LoadObjectPtr@const char*@const char*@_Type*&>
	template<typename _Type> bool LoadObjectPtr(parTreeNode* node, _Type*& obj) const; // <COMBINE parManager::LoadObjectPtr@const char*@const char*@_Type*&>

	// PURPOSE: Load an object from a file. The object in the file must be exactly of type _Type.
	// PARAMS:
	//		_Type - The type of object to load.  Must be registered with the parManager first.
	//		file - The name of the file to load from
	//		ext - The file extension (or "" if the extension is already part of the file name)
	//		stream - The stream to load from.
	//		node - The node to load from.
	//		obj - A reference to the object to fill out
	// RETURNS:
	//		true on success.
	// NOTES:
	//		Because this function takes an object reference, obj must already be allocated.
	//		There is a specialization of this template that takes a parRTStructure& instead of a
	//		reference to an instance of a parsable type.
	template<typename _Type> bool LoadObject(const char* file, const char* ext, _Type& obj, const parSettings* settings = NULL) const;
	template<typename _Type> bool LoadObject(fiStream* stream, _Type& obj, const parSettings* settings = NULL) const; // <COMBINE parManager::LoadObject@const char*@const char*@_Type&>
	template<typename _Type> bool LoadObject(parStreamIn* stream, _Type& obj) const; // <COMBINE parManager::LoadObject@const char*@const char*@_Type&>
	template<typename _Type> bool LoadObject(parTreeNode* node, _Type& obj) const;// <COMBINE parManager::LoadObject@const char*@const char*@_Type&>

	//Load patchfile
	bool LoadPatchFile(const char* filename, const char* ext, parPtrToStructure obj, parStructure* structure) const;

	// PURPOSE: Initializes an object using the parser metadata
	// PARAMS:
	//		_Type - The type of object to initialize. Must be registered with the parManager first.
	//		obj - A reference to the object ot initialize
	// NOTES:
	//		data members will be initialized to whatever is in their init= attributes. Pointers
	//		will be initialized to NULL.
	template<typename _Type> void InitObject(_Type& obj) const;

	// PURPOSE: Initializes an object first, then loads data into it.
	//		_Type - The type of object to load.  Must be registered with the parManager first.
	//		file - The name of the file to load from
	//		ext - The file extension (or "" if the extension is already part of the file name)
	//		stream - The stream to load from.
	//		node - The node to load from.
	//		obj - A reference to the object to fill out
	// RETURNS:
	//		true on success.
	// NOTES:
	//		Because this function takes an object reference, obj must already be allocated.
	template<typename _Type> bool InitAndLoadObject(const char* file, const char* ext, _Type& obj, const parSettings* settings = NULL) const;
	template<typename _Type> bool InitAndLoadObject(fiStream* stream, _Type& obj, const parSettings* settings = NULL) const;
	template<typename _Type> bool InitAndLoadObject(parStreamIn* stream, _Type& obj) const;
	template<typename _Type> bool InitAndLoadObject(parTreeNode* node, _Type& obj) const;

#if PARSER_ALL_METADATA_HAS_NAMES
	// PURPOSE: Saves an object to a file.
	// PARAMS:
	//		_Type - The base type of the object to save. Must be registered with the parManager first.
	//		file - The name of the file to save to
	//		ext - The extension of the file. Use "" if the file parameter already contains the extension.
	//		stream - A fiStream opened for writing.
	//		obj - Pointer to the object to save.
	//		overrideTopLevelName - Special case for writing to parStreamOut, see notes
	// RETURNS:
	//		true on success.
	// NOTES:
	//		This function only exists in builds that have names for all parsable metadata
	//		There is a specialization of this template that takes a parRTStructure& instead of a
	//		pointer to an instance of a parsable type.
	//		overrideTopLevelName is for when you are building a file containing a parsable structure
	//		piecemeal. Normally if we write a top level instance of class Foo in XML we would begin
	//		the file with <Foo>. If overrideTopLevelName were "Item", we would instead write <Item type="Foo"/>
	//		(so that you could generate a number of array elements with separate SaveFromStructure calls)
	template<typename _Type> bool SaveObject(const char* file, const char* ext, const _Type* obj, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	template<typename _Type> bool SaveObject(fiStream* stream, const _Type* obj, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	template<typename _Type> bool SaveObject(parStreamOut* stream, const _Type* obj, int fmt = UNKNOWN, const char* overrideTopLevelName = NULL) const;

	// PURPOSE: Builds a tree node based on an object.
	// PARAMS:
	//		_Type - The base type of the object. Must be registered with the parManager first.
	//		obj - Pointer to the object to build a tree from.
	// RETURNS: A new parTreeNode that represents obj.
	template<typename _Type> parTreeNode* BuildTreeNode(const _Type* obj, const char* overrideTopLevelName = NULL) const;
#endif // PARSER_ALL_METADATA_HAS_NAMES

	// PURPOSE: Saves an object to a file.
	// SEE ALSO: SaveObject
	// NOTES: 
	//		The difference between this and SaveObject is that this can be used in builds where only
	//		a subset of the parsable types have names (i.e. !PARSER_ALL_METADATA_HAS_NAMES)
	template<typename _Type> bool SaveObjectAnyBuild(const char* file, const char* ext, const _Type* obj, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	template<typename _Type> bool SaveObjectAnyBuild(fiStream* stream, const _Type* obj, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	template<typename _Type> bool SaveObjectAnyBuild(parStreamOut* stream, const _Type* obj, int fmt = UNKNOWN, const char* overrideTopLevelName = NULL) const;

	// PURPOSE: Builds a tree node based on an object
	// SEE ALSO: BuildTreeNode
	// NOTES:
	//		The difference between this and BuildTreeNode is that this can be used in builds where only
	//		a subset of the parsable types have names (i.e. !PARSER_ALL_METADATA_HAS_NAMES)
	template<typename _Type> parTreeNode* BuildTreeNodeAnyBuild(const _Type* obj, const char* overrideTopLevelName = NULL) const;

	// PURPOSE: Builds a widget bank for an object.
	// PARAMS:
	//		_Type - The base type of the object. Must be registered with the parManager first.
	//		obj - The object to build widgets for.
	template<typename _Type> void AddWidgets(bkBank& bank, const _Type* obj) const;






//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
// From here on down are less common parser functions. These are mainly internal use or special cases
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////



	// PURPOSE: An enumerated type representing each loadable or savable format
	enum Format {
		INVALID = -1,			// When loading, used to indicate no suitable format for a file
		UNKNOWN,				// Used to indicate no preference for saving. parManager will pick a save format.
		XML,					// Rockstar's XML subset.
		BINARY,					// Rockstar Binary Format
		PSO,					// Parser Seriaized Object format - NOTE! these files have their own API for loading/saving. This is just for FindInputFormat()'s return value
		GAME_SPECIFIC_FORMATS	// Any game specific formats should start at this constant.
	};

	// PURPOSE: Adds a new input format, given the input format description and type ID
	// PARAMS:
	//		format - The ID of the input format. One of the parManager::Format values (or a game specific one)
	//		info - A parInputFormatInfo structure with all of its fields filled out.
	void RegisterInputFormat(int format, parInputFormatInfo& info);

	// PURPOSE: Adds a new output format, given the output format description and type ID
	// PARAMS:
	//		format - The ID of the output format. One of the parManager::Format values (or a game specific one)
	//		info - A parOutputFormatInfo structure with all of its fields filled out.
	void RegisterOutputFormat(int format, parOutputFormatInfo& info);

	// PURPOSE: Registers all built-in formats. Currently they are XML read and write and Binary read and write.
	void RegisterBuiltinFormats();


	// PURPOSE: Initialized an object from a structure
	// PARAMS: 
	//		structure - The parStructure that describes the object to init
	//		obj - Pointer to the object to init
	void InitFromStructure(parStructure& structure, parPtrToStructure obj = NULL) const;

	// PURPOSE: Loads an object using the object description in structure.
	// PARAMS:
	//		file - The name of the file to load from
	//		ext - The file extension (or "" if the extension is already part of the file name)
	//		stream - The stream to load from.
	//		node - The node to load from.
	//		structure - The parStructure that describes the object to load.
	//		obj - Pointer to the object to load.
	// RETURNS:
	//		true on success.
	// NOTES:
	//		Will not allocate space for the object. obj can be NULL, if parStructure contains 
	//		specific memory addresses to load into.
	bool LoadFromStructure(const char* file, const char* ext, parStructure& structure, parPtrToStructure obj, bool verifyTypes = false, const parSettings* settings = NULL) const;
	bool LoadFromStructure(fiStream* stream, parStructure& structure, parPtrToStructure obj, bool verifyTypes = false, const parSettings* settings = NULL) const;
	bool LoadFromStructure(parStreamIn* stream, parStructure& structure, parPtrToStructure obj, bool verifyTypes = false) const;
	bool LoadFromStructure(parTreeNode* node, parStructure& structure, parPtrToStructure obj, bool verifyTypes = false) const;

	// PURPOSE: Instantiates whatever is in the file - use with caution!
	// PARAMS:
	//		file - The name of the file to load from
	//		ext - The file extension (or "" if the extension is already part of the file name)
	//		stream - The stream to load from.
	//		node - The node to load from.
	//		inOutStructure - If non-NULL, the required base class of the object to load.
	//		outObj - Pointer to the object that got loaded.
	// RETURNS:
	//		true on success, inOutStructure will contain the actual type that got created, outObj contains a pointer to the new object
	bool CreateAndLoadAnyType(const char* file, const char* ext, parStructure*& inOutStructure, parPtrToStructure& outObj, const parSettings* settings = NULL) const;
	bool CreateAndLoadAnyType(fiStream* stream, parStructure*& inOutStructure, parPtrToStructure& outObj, const parSettings* settings = NULL) const;
	bool CreateAndLoadAnyType(parStreamIn* stream, parStructure*& structure, parPtrToStructure& obj) const;
	bool CreateAndLoadAnyType(parTreeNode* node, parStructure*& inOutStructure, parPtrToStructure& outObj, bool verifyTypes) const;

	// PURPOSE: Saves an object using the object description in structure.
	// PARAMS:
	//		file - The name of the file to save to
	//		ext - The extension of the file. Use "" if the file parameter already contains the extension.
	//		stream - The stream to save to.
	//		structure - The parStructure that describes the object to save.
	//		obj - Pointer to the object to save.
	//		overrideTopLevelName - Special case for writing to parStreamOut, see notes
	// RETURNS:
	//		true on success.
	// NOTES:
	//		obj can be NULL if structure contains specific memory addresses to save from.
	//		overrideTopLevelName is for when you are building a file containing a parsable structure
	//		piecemeal. Normally if we write a top level instance of class Foo in XML we would begin
	//		the file with <Foo>. If overrideTopLevelName were "Item", we would instead write <Item type="Foo"/>
	//		(so that you could generate a number of array elements with seperate SaveFromStructure calls)
	bool SaveFromStructure(const char* file, const char* ext, parStructure& structure, parConstPtrToStructure obj = NULL, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	bool SaveFromStructure(fiStream* stream, parStructure& structure, parConstPtrToStructure obj = NULL, int fmt = UNKNOWN, const parSettings* settings = NULL) const;
	bool SaveFromStructure(parStreamOut* stream, parStructure& structure, parConstPtrToStructure obj = NULL, int fmt = UNKNOWN, const char* overrideTopLevelName = NULL) const;

	// PURPOSE: Builds a tree using the object description in structure.
	// PARAMS:
	//		structure - The parStructure that describes the object to save.
	//		obj - Pointer to the object to save.
	//		overrideTopLevelName - see notes below.
	// RETURNS:
	//		A new parTreeNode representing obj.
	// NOTES:
	//		obj can be NULL if structure contains specific memory addresses to save from.
	//		overrideTopLevelName is for when you are building a file containing a parsable structure
	//		piecemeal. Normally if we write a top level instance of class Foo in XML we would begin
	//		the file with <Foo>. If overrideTopLevelName were "Item", we would instead write <Item type="Foo"/>
	//		(so that you could generate a number of array elements with seperate SaveFromStructure calls)
	parTreeNode* BuildTreeNodeFromStructure(parStructure& structure, parConstPtrToStructure obj = NULL, const char* overrideTopLevelName = NULL, const parSettings* settings = NULL) const;

	// PURPOSE: Register a structure for loading/saving.
	// PARAMS:
	//		name - The name of the structure.
	//		structure - A parStructure containing a description of all the structure's members.
	void RegisterStructure(u32 nameHash, parStructure* structure);

	// PURPOSE: Registers an enum for later lookup
	// PARAMS:
	//		enumData - a parEnumData that's been filled out with all it's data.
	void RegisterEnum(parEnumData* enumData);

	// PURPOSE: Unregisters a previously registered structure
	// PARAMS:
	//		name - The name of the structure
	//		structure - A parStructure that has been previously registered
	void UnregisterStructure(const char* name);
	void UnregisterStructure(parStructure* structure);

	// PURPOSE: Locates a registered structure object.
	// PARAMS:
	//		name - The structure name to look for.
	// RETURNS: A pointer to the registered structure, or NULL if nothing was found.
	parStructure* FindStructure(const char* name) const;
	parStructure* FindStructure(u32 hash) const;
	parStructure* FindStructure(atLiteralHashValue hash) const { return FindStructure(hash.GetHash()); }
	parStructure* FindStructure(atLiteralHashString hash) const { return FindStructure(hash.GetHash()); }

	parEnumData* FindEnum(u32 hash) const;
	parEnumData* FindEnum(atLiteralHashValue hash) const {return FindEnum(hash.GetHash()); }
	parEnumData* FindEnum(const char* name) const { return FindEnum(atLiteralHashValue(name)); }

	// PURPOSE: Returns the current parser settings
	parSettings& Settings();
	const parSettings& Settings() const;

	// PURPOSE: For internal use only.
	// NOTES: Finds a parStructure for _Type (registering it if it's not registered yet)
	template<typename _Type>
	static parStructure* GetRequiredStructure(const char* structure, const char* requiredBy);

	// PURPOSE: Returns the parStructure that obj uses. Asserts that _Type has been registered with the parser.
	template <typename _Type>
	parStructure* GetStructure(const _Type* obj) const;

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	// PURPOSE: Writes all of the structure metadata (parStructure objects, a.k.a. <structdef>s) to a file
	// NOTES: Saves to parsable format, _not_ <structdef> format.
	void SaveStructureDefns(const char* filename);

	parExternalStructureManager& GetExternalStructureManager();
#endif

	// PURPOSE: Loads a set of extra attributes for the registered metadata
	void LoadExtraMetadataAttributes(const char* filename);

	// PURPOSE: Given 4 bytes (reinterpreted as a u32) - checks to see if its a known input format
	int FindInputFormat(u32 firstBytesAsU32) const;

	// PURPOSE: For internal use only
	// NOTES: Creating an instance of AutoUseTempMemory will start using the temp heap for allocations
	//   When the instance is destroyed it'll stop using the temp heap;
	class AutoUseTempMemory
	{
	public:
		AutoUseTempMemory()
		{
			m_UsingTempMemory = PARSER.Settings().GetFlag(parSettings::USE_TEMPORARY_HEAP);
			if (m_UsingTempMemory)
			{
				sysMemStartTemp();
			}
		}

		~AutoUseTempMemory()
		{
			if (m_UsingTempMemory)
			{
				sysMemEndTemp();
			}
		}


	private:
		bool m_UsingTempMemory;
	};


	// PURPOSE: Initializes a parser manager object.
	// NOTES: Sets the default settings, registers known file formats, and registers autoregisterable types.
	void Initialize(parSettings settings, bool registerStatics);

	// PURPOSE: Uninitialze a parser manager object
	// NOTES: Returns true if it's OK to delete the object
	bool Uninitialize();

	void AddPatchFileToList(const char* filename);

	typedef atMap<u32, parStructure*> StructureMap;
	typedef atMap<u32, parEnumData*> EnumMap;

#if __BANK
	// PURPOSE: Const access to the structure map for bank builds. Allows iterating over structures for dev purposes.
	const StructureMap& GetStructureMap() const { return m_Structures; }
	const EnumMap& GetEnumMap() const { return m_Enums; }
#endif //__BANK

	static parManager* sm_Instance;
	atMap<atHashString, atHashString> patchFileList;

	parManager();
	~parManager();
protected:

	static fiStream* TryPreLoad(fiStream *S);

	void FindInputFormat(fiStream* stream, int& outFormat, u32& outHeaderBytes) const;

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS

	mutable parExternalStructureManager m_ExternalStructures;
#endif

	void RegisterAutoStructs();

	template<typename _Type>
	bool CheckInheritance(); // makes sure nothing in the inheritance chain uses PAR_SIMPLE_PARSABLE

	// PURPOSE: Removes platform specific nodes that are irrelevant for this platform.
	void CullPlatformSpecificNodes(parTree* tree) const;

	atMap<int, parInputFormatInfo> m_InputFormatInfo;
	atMap<int, parOutputFormatInfo> m_OutputFormatInfo;
	
	StructureMap m_Structures;
	EnumMap m_Enums;

	parSettings	m_Settings;

	int m_InitializationCount;
	bool m_IsAutoregistering;

};

template<typename _Type>
parStructure* parManager::GetStructure(const _Type* obj) const 
{
	parAssertf(_Type::parser_GetStaticStructure(), "Type %s hasn't been registered with the parser. Do you have autoregister=\"false\" in the PSC file?", typeid(_Type).name());
	parStructure* ret = NULL;
	if (obj)
	{
		ret = obj->parser_GetStructure();
#if __ASSERT
		if(!ret)	// This extra check was added because GetRttiName() can be expensive, so we shouldn't do it if we won't display it. /FF
		{
			const char* rttiName = GetRttiName(obj);
			if (!rttiName)
			{
				rttiName = "<unknown>";
			}
			parAssertf(ret, "Type %s hasn't been registered with the parser", rttiName);
		}
#endif
	}
	else
	{
		ret = _Type::parser_GetStaticStructure();
	}
	return ret;
}

template<typename _Type>
bool parManager::LoadObjectPtr(parTreeNode* node, _Type*& obj) const {
	FastAssert(node);
	parAssertf(obj == NULL, "Don't pass in an allocated object to LoadObjectPtr, the parser will do the allocation based on what's in the file");
	parStructure* structure = _Type::parser_GetStaticStructure();
	return CreateAndLoadAnyType(node, structure, reinterpret_cast<parPtrToStructure&>(obj), !Settings().GetFlag(parSettings::LOAD_UNKNOWN_TYPES));
}

template<typename _Type>
bool parManager::LoadObjectPtr(fiStream* stream, _Type*& obj, const parSettings* settingsPtr) const {
	parAssertf(obj == NULL, "Don't pass in an allocated object to LoadObjectPtr, the parser will do the allocation based on what's in the file");
	parStructure* structure = _Type::parser_GetStaticStructure();
	return CreateAndLoadAnyType(stream, structure, reinterpret_cast<parPtrToStructure&>(obj), settingsPtr);
}

template<typename _Type>
bool parManager::LoadObjectPtr(parStreamIn* stream, _Type*& obj) const {

	parAssertf(obj == NULL, "Don't pass in an allocated object to LoadObjectPtr, the parser will do the allocation based on what's in the file");
	parStructure* structure = _Type::parser_GetStaticStructure();
	return CreateAndLoadAnyType(stream, structure, reinterpret_cast<parPtrToStructure&>(obj));
}

template<typename _Type>
bool parManager::LoadObjectPtr(const char* file, const char* ext, _Type*& obj, const parSettings* settingsPtr) const {
	parAssertf(obj == NULL, "Don't pass in an allocated object to LoadObjectPtr, the parser will do the allocation based on what's in the file");
	parStructure* structure = _Type::parser_GetStaticStructure();
	return CreateAndLoadAnyType(file, ext, structure, reinterpret_cast<parPtrToStructure&>(obj), settingsPtr);
}

template<typename _Type>
bool parManager::LoadObject(parTreeNode* node, _Type& obj) const {
	FastAssert(node);
	return LoadFromStructure(node, *GetStructure(&obj), obj.parser_GetPointer(), true);
}

template<typename _Type>
bool parManager::LoadObject(fiStream* stream, _Type& obj, const parSettings* settings) const {
	return LoadFromStructure(stream, *GetStructure(&obj), obj.parser_GetPointer(), true, settings);
}

template<typename _Type>
bool parManager::LoadObject(parStreamIn* stream, _Type& obj) const {
	return LoadFromStructure(stream, *GetStructure(&obj), obj.parser_GetPointer(), true);
}

template<typename _Type>
bool parManager::LoadObject(const char* file, const char* ext, _Type& obj, const parSettings* settings) const {
	return LoadFromStructure(file, ext, *GetStructure(&obj), obj.parser_GetPointer(), true, settings);
}

// Specializations for parRTStructure
template<>
inline bool parManager::LoadObject<parRTStructure>(parTreeNode* node, parRTStructure& obj) const
{
	return LoadFromStructure(node, obj, NULL, false); 
}

template<>
inline bool parManager::LoadObject<parRTStructure>(fiStream* stream, parRTStructure& obj, const parSettings* settings) const
{
	return LoadFromStructure(stream, obj, NULL, false, settings); 
}

template<>
inline bool parManager::LoadObject<parRTStructure>(parStreamIn* stream, parRTStructure& obj) const
{
	return LoadFromStructure(stream, obj, NULL, false); 
}

template<>
inline bool parManager::LoadObject<parRTStructure>(const char* file, const char* ext, parRTStructure& obj, const parSettings* settings) const
{
	return LoadFromStructure(file, ext, obj, NULL, false, settings); 
}

template<typename _Type>
void parManager::InitObject(_Type& obj) const {
	InitFromStructure(*GetStructure(&obj), obj.parser_GetPointer());
}

template<typename _Type>
bool parManager::InitAndLoadObject(const char* file, const char* ext, _Type& obj, const parSettings* settings) const {
	InitObject(obj);
	return LoadObject(file, ext, obj, settings);
}

template<typename _Type>
bool parManager::InitAndLoadObject(fiStream* stream, _Type& obj, const parSettings* settings) const {
	InitObject(obj);
	return LoadObject(stream, obj, settings);
}

template<typename _Type>
bool parManager::InitAndLoadObject(parStreamIn* stream, _Type& obj) const {
	InitObject(obj);
	return LoadObject(stream, obj);
}

template<typename _Type>
bool parManager::InitAndLoadObject(parTreeNode* node, _Type& obj) const {
	InitObject(obj.parser_GetPointer());
	return LoadObject(node, obj);
}

#if PARSER_ALL_METADATA_HAS_NAMES
template<typename _Type>
parTreeNode* parManager::BuildTreeNode(const _Type* obj, const char* overrideTopLevelName /* = NULL */) const {
	FastAssert(obj);
	AutoUseTempMemory temp;
	return BuildTreeNodeFromStructure(*GetStructure(obj), obj->parser_GetPointer(), overrideTopLevelName);
}

template<typename _Type>
bool parManager::SaveObject(fiStream* stream, const _Type* obj, int fmt /*= UNKNOWN*/, const parSettings* settings) const {
	AutoUseTempMemory temp;
	return SaveFromStructure(stream, *GetStructure(obj), obj->parser_GetPointer(), fmt, settings);
}

template<typename _Type>
bool parManager::SaveObject(parStreamOut* stream, const _Type* obj, int fmt /*= UNKNOWN*/, const char* overrideTopLevelName /* = NULL */) const {
	AutoUseTempMemory temp;
	return SaveFromStructure(stream, *GetStructure(obj), obj->parser_GetPointer(), fmt, overrideTopLevelName);
}

template<typename _Type>
bool parManager::SaveObject(const char* file, const char* ext, const _Type* obj, int fmt /*= UNKNOWN*/, const parSettings* settings) const {
	return SaveFromStructure(file, ext, *GetStructure(obj), obj->parser_GetPointer(), fmt, settings);
}

template<>
inline bool parManager::SaveObject<parRTStructure>(fiStream* stream, const parRTStructure* obj, int fmt, const parSettings* settings) const
{
	if (obj)
	{
		return SaveFromStructure(stream, const_cast<parRTStructure&>(*obj), NULL, fmt, settings);
	}
	return false;
}

template<>
inline bool parManager::SaveObject<parRTStructure>(parStreamOut* stream, const parRTStructure* obj, int fmt, const char* overrideTopLevelName /* = NULL */) const
{
	if (obj)
	{
		return SaveFromStructure(stream, const_cast<parRTStructure&>(*obj), NULL, fmt, overrideTopLevelName);
	}
	return false;
}

template<>
inline bool parManager::SaveObject<parRTStructure>(const char* file, const char* ext, const parRTStructure* obj, int fmt /*= UNKNOWN*/, const parSettings* settings) const
{
	if (obj)
	{
		return SaveFromStructure(file, ext, const_cast<parRTStructure&>(*obj), NULL, fmt, settings);
	}
	return false;
}
#endif // PARSER_ALL_METADATA_HAS_NAMES

template<typename _Type>
parTreeNode* parManager::BuildTreeNodeAnyBuild(const _Type* obj, const char* overrideTopLevelName /* = NULL */) const {
	FastAssert(obj);
	AutoUseTempMemory temp;
	parSettings settings = Settings();
	settings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
	return BuildTreeNodeFromStructure(*GetStructure(obj), obj->parser_GetPointer(), overrideTopLevelName, &settings);
}

template<typename _Type>
bool parManager::SaveObjectAnyBuild(fiStream* stream, const _Type* obj, int fmt /*= UNKNOWN*/, const parSettings* settingsPtr) const {
	AutoUseTempMemory temp;
	parSettings settings = settingsPtr ? *settingsPtr : Settings();
	settings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
	return SaveFromStructure(stream, *GetStructure(obj), obj->parser_GetPointer(), fmt, &settings);
}

template<typename _Type>
bool parManager::SaveObjectAnyBuild(parStreamOut* stream, const _Type* obj, int fmt /*= UNKNOWN*/, const char* overrideTopLevelName /* = NULL */) const {
	AutoUseTempMemory temp;
	parSettings oldSettings = stream->GetSettings();
	parSettings newSettings = oldSettings;
	newSettings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
	stream->SetSettings(newSettings);
	bool result = SaveFromStructure(stream, *GetStructure(obj), obj->parser_GetPointer(), fmt, overrideTopLevelName);
	stream->SetSettings(oldSettings);
	return result;
}

template<typename _Type>
bool parManager::SaveObjectAnyBuild(const char* file, const char* ext, const _Type* obj, int fmt /*= UNKNOWN*/, const parSettings* settingsPtr) const {
	parSettings settings = settingsPtr ? *settingsPtr : Settings();
	settings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
	return SaveFromStructure(file, ext, *GetStructure(obj), obj->parser_GetPointer(), fmt, &settings);
}

template<>
inline bool parManager::SaveObjectAnyBuild<parRTStructure>(fiStream* stream, const parRTStructure* obj, int fmt, const parSettings* settingsPtr) const
{
	if (obj)
	{
		parSettings settings = settingsPtr ? *settingsPtr : Settings();
		settings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
		return SaveFromStructure(stream, const_cast<parRTStructure&>(*obj), NULL, fmt, &settings);
	}
	return false;
}

template<>
inline bool parManager::SaveObjectAnyBuild<parRTStructure>(parStreamOut* stream, const parRTStructure* obj, int fmt, const char* overrideTopLevelName /* = NULL */) const
{
	if (obj)
	{
		parSettings oldSettings = stream->GetSettings();
		parSettings newSettings = oldSettings;
		newSettings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
		stream->SetSettings(newSettings);

		bool status = SaveFromStructure(stream, const_cast<parRTStructure&>(*obj), NULL, fmt, overrideTopLevelName);

		stream->SetSettings(oldSettings);

		return status;
	}
	return false;
}

template<>
inline bool parManager::SaveObjectAnyBuild<parRTStructure>(const char* file, const char* ext, const parRTStructure* obj, int fmt /*= UNKNOWN*/, const parSettings* settingsPtr) const
{
	if (obj)
	{
		parSettings settings = settingsPtr ? *settingsPtr : Settings();
		settings.SetFlag(parSettings::ENSURE_NAMES_IN_ALL_BUILDS, true);
		return SaveFromStructure(file, ext, const_cast<parRTStructure&>(*obj), NULL, fmt, &settings);
	}
	return false;
}


#if __BANK
template<typename _Type> 
inline void parManager::AddWidgets(bkBank& bank, const _Type* obj) const
{
	if (obj != NULL)
	{
		parBuildWidgetsVisitor widgets;
		widgets.m_CurrBank = &bank;
		widgets.Visit(*const_cast<_Type*>(obj));
	}
}
#endif	// __BANK

inline parSettings& parManager::Settings()
{
	return m_Settings;
}

inline const parSettings& parManager::Settings() const
{
	return m_Settings;
}


} // namespace rage
#endif // __SPU

#endif // PARSER_MANAGER_H
