// 
// parser/tree.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tree.h"

#include "optimisations.h"
#include "treenode.h"

#include "file/stream.h"
#include "math/amath.h"
#include "parsercore/element.h"
#include "parsercore/streamxml.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parTree::parTree() 
: m_Root(NULL)
, m_Current(NULL)
, m_Sibling(NULL)
{
}

parTree::~parTree()
{
	for(int i = 0; i < m_NameTable.GetCount(); i++) {
		delete m_NameTable[i];
	}
	delete m_Root;
}

parTreeNode* parTree::CreateRoot()
{
	m_Root = rage_new parTreeNode;
	return m_Root;
}

parTree* parTree::LoadFromStream(parStreamIn* stream)
{
	if (!parVerifyf(stream, "Can't load from a NULL stream")) 
	{
		return NULL;
	}
	parTree* tree = rage_new parTree;
	stream->SetBeginElementCallback(parStreamIn::BeginElementCB(tree, &parTree::ReadBeginElement));
	stream->SetEndElementCallback(parStreamIn::EndElementCB(tree, &parTree::ReadEndElement));
	stream->SetDataCallback(parStreamIn::DataCB(tree, &parTree::ReadData));
	stream->SetNameTable(&tree->m_NameTable);

	bool success = stream->ReadWithCallbacks();
	if (Unlikely(!success))
	{
		delete tree;
		return NULL;
	}
	return tree;
}


void parTree::ReadBeginElement(parElement& elt, bool )
{
	parTreeNode* newNode = rage_new parTreeNode;
	newNode->m_Element.SwapWith(elt);
	newNode->GetDataWriteValsPerLine() = 1; // set in the parTreeNode c'tor but clobbered during the swap
	if (m_Root == NULL) {
		parAssertf(m_Current == NULL, "Tree construction is in a bad state (no root but has a current element)");
		m_Current = m_Root = newNode;
	}
	else {
		parAssertf(m_Current, "Can't find a parent node");
		if (m_Sibling) {
			newNode->InsertAsSiblingOf(m_Sibling);
			m_Sibling = NULL;
		}
		else {
			newNode->InsertAsChildOf(m_Current);
		}
		m_Current = newNode;
	}
}

void parTree::ReadEndElement(bool)
{
	m_Sibling = m_Current;
	if (m_Current) {
		m_Current = m_Current->m_Parent;
	}
}

void parTree::ReadData(char* data, int size, bool /*moreData*/)
{
/*
If there is a current node
	Current node's data = data
*/
	parAssertf(m_Current && !m_Current->m_Child, "Can't have data and child elements. See https://devstar.rockstargames.com/wiki/index.php/Supported_XML_features");
	m_Current->GetFlags().Set(parTreeNode::FLAG_HAS_DATA);

	u32 oldSize = m_Current->GetDataSize();
	u32 newSize = oldSize + size;

	// is it w/i capacity?
	if (oldSize > 0 && (u32)m_Current->m_DataArray.GetCapacity() <= newSize)
	{
		//make a new array twice as big
		atArray<char, 0, u32> newArray;
		newArray.Reserve(Max(m_Current->m_DataArray.GetCapacity() * 2, (int)newSize));
		newArray.Resize(oldSize);
		sysMemCpy(&newArray[0], m_Current->GetData(), m_Current->GetDataSize());
		m_Current->m_DataArray.Assume(newArray);
	}
	m_Current->m_DataArray.Resize(newSize);
	sysMemCpy(m_Current->GetData() + oldSize, data, size);
}

bool parTree::SaveToStream(parStreamOut* stream, parTree* tree)
{
	parTreeNode* curr = tree->GetRoot();
	return SaveToStream(stream, curr);
}

bool parTree::SaveToStream(parStreamOut* stream, parTreeNode* root)
{
	bool justGotParent = false;

	parTreeNode* curr = root;

	parHeaderInfo info;
	stream->WriteHeader(info);
	
	while(curr)
	{
		if (!justGotParent && curr->GetChild()) {
			stream->WriteBeginElement(curr->GetElement());
			curr = curr->GetChild();
			justGotParent = false;
		}
		else
		{
			if (!justGotParent)
			{
				if (curr->HasData() && curr->GetDataSize() > 0)
				{
					stream->WriteBeginElement(curr->GetElement());

					parStreamOut::WriteDataOptions options;
					options.m_ValuesPerLine = curr->GetDataWriteValsPerLine();
					options.m_Hexadecimal = curr->GetFlags().IsSet(parTreeNode::FLAG_DATA_WRITE_HEXADECIMAL);
					options.m_HighPrecision = curr->GetFlags().IsSet(parTreeNode::FLAG_DATA_WRITE_HIGH_PRECISION);
					options.m_Unsigned = curr->GetFlags().IsSet(parTreeNode::FLAG_DATA_WRITE_UNSIGNED);

					stream->WriteData(curr->GetData(), curr->GetDataSize(), true, parStream::UNSPECIFIED, &options);

					stream->WriteEndElement(curr->GetElement());
				}
				else
				{
					stream->WriteLeafElement(curr->GetElement());
				}
			}
			if (curr->GetSibling()) {
				curr = curr->GetSibling();
				justGotParent = false;
			}
			else {
				curr = curr->GetParent();
				if (curr)
				{
					stream->WriteEndElement(curr->GetElement());
				}
				if (curr == root)
				{
					curr = NULL; // END the loop
				}
				justGotParent = true;
			}
		}
	}

	return true;
}
