// 
// parser/visitor.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITOR_H
#define PARSER_VISITOR_H

#include "memberarray.h"
#include "memberenum.h"
#include "membermap.h"
#include "membermatrix.h"
#include "membersimple.h"
#include "memberstring.h"
#include "memberstruct.h"
#include "membervector.h"
#include "structure.h"

#include "atl/hashstring.h"
#include "atl/string.h"
#include "atl/wstring.h"

// Calling hierarchy. The visitor starts at the deepest level possible in the hierarchy
// and progresses up the hierarchy calling the Member() virtual functions. 
// So for a Char member, it will first call CharMember, then SimpleMember, and then AnyMember,
// stopping when there is a user defined override.
/*
Any
  Simple
    Bool
	Char
	UChar
	Short
	UShort
	Int
	UInt
	Float
	Double
  Enum
  Bitset
  Vector
    Vector2
	Vector3
	Vector4
  Matrix
    Matrix34
	Matrix44	
  Struct*		
    Pointer**	
  String
    ConstString			// ConstString example;
	PointerString		// char* example;
	MemberString		// char example[32];
	AtString			// atString example;
  WideString
    WidePointerString	// char16* example;
	WideMemberString	// char16 example[32];
	AtWideString		// atWideString example;
  Array
	PointerArray
  ExternalPointer

* - Only BeginStructMember calls AnyMember, not EndStructMember
** - PointerMember only calls StructMember if pointer is non-null.

*/

namespace rage
{
	class Float16;
	class BoolV;
	class VecBoolV;
	class ScalarV;
	class Mat33V;
	class Mat34V;
	class Mat44V;
	class Vec2V;
	class Vec3V;
	class Vec4V;
	class Matrix34;
	class Matrix44;
	class Vector2;
	class Vector3;
	class Vector4;

// PURPOSE: parInstanceVisitor is a base class that uses can derive from to create custom
// visitors that can visit all members in any parsable structure. Possible uses for the
// visitors are to initialize data fields, add widgets, save files, validate the data, etc.
// NOTES:
// To create a custom visitor you would typically create a new subclass of parInstanceVisitor
// and override some of the *Member() functions. The default implementations generally
// have the most specific function (e.g. IntMember) call a less specific funcition (SimpleMember).
class parInstanceVisitor
{
public:
	parInstanceVisitor();
	virtual ~parInstanceVisitor() {}

	// PURPOSE: Visits all members in a parsable class or structure. It will also
	// visit members contained in arrays or referenced through pointers.
	// PARAMS:	instance - An instance of a parsable class or structure
	template<typename _Type>
	void Visit(_Type& instance);


	// PURPOSE: Visits all members in a parsable structure. It will also
	// visit members contained in arrays or referenced through pointers.
	// Calls the BeginToplevelStructure and EndToplevelStructure functions
	// PARAMS:	dataPtr - A pointer to the instance of the parsable data
	//			metadata - The parStructure containing the metadata for the instance.
	void VisitToplevelStructure(parPtrToStructure instanceDataPtr, parStructure& metadata);

	// PURPOSE: Visits all members in a parsable structure. It will also
	// visit members contained in arrays or referenced through pointers.
	// PARAMS:	dataPtr - A pointer to the instance of the parsable data
	//			metadata - The parStructure containing the metadata for the instance.
	virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata);

	// PURPOSE: This virtual function gets called for a member when no other
	//   more specific function handles the member.
	// PARAMS:
	//		ptrToMember - A pointer to the member being visited
	//		metadata - The metadata description of the member being visited.
	virtual void AnyMember				(parPtrToMember ptrToMember,	parMember& metadata);

	// PURPOSE: This virtual function gets called for a simple (numeric) member
	//   when no more specific function handles the member.
	// PARAMS:
	//		ptrToMember - A pointer to the member being visited
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		Default implementation calls AnyMember()
	virtual void SimpleMember			(parPtrToMember ptrToMember,	parMemberSimple& metadata);

	// PURPOSE: This virtual function gets called for any vector member
	//   when no more specific function handles the member.
	// PARAMS:
	//		ptrToMember - A pointer to the member being visited
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		Default implementation calls AnyMember()
	virtual void VectorMember			(parPtrToMember ptrToMember,	parMemberVector& metadata);

	// PURPOSE: This virtual function gets called for any matrix member
	//   when no more specific function handles the member.
	// PARAMS:
	//		ptrToMember - A pointer to the member being visited
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		Default implementation calls AnyMember()
	virtual void MatrixMember			(parPtrToMember ptrToMember,	parMemberMatrix& metadata);

	// PURPOSE: These virtual functions are called for simple member data.
	// PARAMS:
	//		data - A reference to the member variable
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		Default implementation calls SimpleMember()
	virtual void BoolMember				(bool& data,		parMemberSimple& metadata);
	virtual void CharMember				(s8& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void UCharMember			(u8& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void ShortMember			(s16& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void UShortMember			(u16& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void IntMember				(s32& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void IntMember				(s64& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void UIntMember				(u32& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void UIntMember				(u64& data,			parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void FloatMember			(float& data,		parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void Float16Member			(Float16& data,		parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void ScalarVMember			(ScalarV& data,		parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void BoolVMember			(BoolV& data,		parMemberSimple& metadata); // <ALIAS BoolMember>
	virtual void DoubleMember			(double& data,		parMemberSimple& metadata); // <ALIAS BoolMember>

	// PURPOSE: These virtual functions are called for vector member data.
	// PARAMS:
	//		data - A reference to the member variable
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		Default implementation calls VectorMember()
	virtual void Vector2Member			(Vector2& data,		parMemberVector& metadata);
	virtual void Vector3Member			(Vector3& data,		parMemberVector& metadata); // <ALIAS Vector2Member>
	virtual void Vector4Member			(Vector4& data,		parMemberVector& metadata); // <ALIAS Vector2Member>
	virtual void Vec2VMember			(Vec2V& data,		parMemberVector& metadata); // <ALIAS Vector2Member>
	virtual void Vec3VMember			(Vec3V& data,		parMemberVector& metadata); // <ALIAS Vector2Member>
	virtual void Vec4VMember			(Vec4V& data,		parMemberVector& metadata); // <ALIAS Vector2Member>
	virtual void VecBoolVMember			(VecBoolV& data,	parMemberVector& metadata); // <ALIAS Vector2Member>

	// PURPOSE: These virtual functions are called for matrix member data.
	// PARAMS:
	//		data - A reference to the member variable
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		Default implementation calls MatrixMember()
	virtual void Matrix34Member			(Matrix34& data,	parMemberMatrix& metadata);
	virtual void Matrix44Member			(Matrix44& data,	parMemberMatrix& metadata); // <ALIAS Matrix34Member>
	virtual void Mat33VMember			(Mat33V& data,		parMemberMatrix& metadata); // <ALIAS Matrix34Member>
	virtual void Mat34VMember			(Mat34V& data,		parMemberMatrix& metadata); // <ALIAS Matrix34Member>
	virtual void Mat44VMember			(Mat44V& data,		parMemberMatrix& metadata); // <ALIAS Matrix34Member>

	// PURPOSE: This virutal function is called for an enum member
	// PARAMS:
	//		data - A reference to the member variable
	//		metadata - The metadata description of the member being visited.
	// NOTES:
	//		This function is called regardless of the actual storage size of the enum (8, 16, or 32 bit).
	//		If the data is changed from any derived class' EnumMember method, we write that new value
	//		back to the structure using the correct storage size.
	//		Also note that this means that data is a temporary variable, and &data is not the address of the
	//		data in the structure.
	//		Default implementation calls AnyMember()
	virtual void EnumMember				(int& data,			parMemberEnum& metadata);

	// PURPOSE: This virtual function is called for a bitset member
	// PARAMS:
	//		data - A reference to the member variable
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Returns a pointer to the bitset's storage, but it's likely to be easier to call metadata methods
	//		passing m_ContainingStructureAddress than it is to operate directly on the storage so you don't
	//		have to deal with storage format differences.
	//		Default implementation calls AnyMember()
	virtual void BitsetMember			(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);

	// PURPOSE: This virtual function gets called for any string type member (not wide strings)
	// PARAMS:
	//		ptrToMember - A pointer to the member data
	//		ptrToString - A pointer to the beginning of the string
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation calls AnyMember
	virtual void StringMember			(parPtrToMember ptrToMember,	const char* ptrToString, parMemberString& metadata);

	// PURPOSE: These virtual functions are called for string member data
	// PARAMS:
	//		stringData - A reference to the member variable
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation calls StringMember
	virtual void MemberStringMember		(char* stringData,	parMemberString& metadata);
	virtual void PointerStringMember	(char*& stringData,	parMemberString& metadata);			// <ALIAS MemberStringMember>
	virtual void ConstStringMember		(ConstString& stringData, parMemberString& metadata);	// <ALIAS MemberStringMember>
	virtual void AtStringMember			(atString& stringData, parMemberString& metadata);		// <ALIAS MemberStringMember>
#if !__FINAL
	virtual void AtNonFinalHashStringMember		(atHashString& stringData, parMemberString& metadata);  // <ALIAS MemberStringMember>
#endif	//	!__FINAL
	virtual void AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& metadata);  // <ALIAS MemberStringMember>
	virtual void AtHashValueMember		(atHashValue& stringData, parMemberString& metadata);  // <ALIAS MemberStringMember>
	virtual void AtPartialHashValueMember	(u32& stringData, parMemberString& metadata);  // <ALIAS MemberStringMember>
	virtual void AtNamespacedHashStringMember(atNamespacedHashStringBase& stringData, parMemberString& metadata);
	virtual void AtNamespacedHashValueMember(atNamespacedHashValueBase& stringData, parMemberString& metadata);

	// PURPOSE: This virtual function gets called for any wide (16 bit) string type member
	// PARAMS:
	//		ptrToMember - A pointer to the member data
	//		ptrToString - A pointer to the beginning of the wide string
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation calls AnyMember
	virtual void WideStringMember		(parPtrToMember ptrToMember, const char16* ptrToString, parMemberString& metadata);

	// PURPOSE: These virtual functions are called for wide string member data
	// PARAMS:
	//		stringData - A reference to the member variable
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation calls WideStringMember
	virtual void WideMemberStringMember	(char16* stringData, parMemberString& metadata);
	virtual void WidePointerStringMember(char16*& stringData, parMemberString& metadata);		// <ALIAS WideMemberStringMember>
	virtual void AtWideStringMember		(atWideString& stringData, parMemberString& metadata);	// <ALIAS WideMemberStringMember>

	// PURPOSE: This function is called for any pointers with the "external_named" policy
	// PARAMS:
	//		data - A reference to the external named pointer
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation calls AnyMember
	virtual void ExternalPointerMember	(void*& data,		parMemberStruct& metadata);

	// PURPOSE: This function is called whenever the visitor encounters a struct member
	// PARAMS:
	//		ptrToStruct - A pointer to the struct that's about to be visited
	//		metadata - The metadata description of the member being visited
	// RETURNS:
	//		true if the visitor should visit members of the structure, false to skip them
	// NOTES:
	//		Default implementation calls AnyMember, returns true.
	virtual bool BeginStructMember		(parPtrToStructure ptrToStruct,	parMemberStruct& metadata);

	// PURPOSE: This function is called after the visitor has finished visiting a struct member
	// PARAMS:
	//		ptrToStruct - A pointer to the struct that's been visited
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation does nothing. This function gets called for any 
	//		members whose BeginStructMember functions were called, even if 
	//		EndTraversal() was called.
	virtual void EndStructMember		(parPtrToStructure ptrToStruct,	parMemberStruct& metadata);

	// PURPOSE: This function is called whenever the visitor encounters a pointer to parsable data
	// PARAMS:
	//		ptrRef - a reference to the pointer member
	//		metadata - The metadata description of the member being visited
	// RETURNS:
	//		true if the visitor should dereference the pointer and visit the pointed-to structure
	// NOTES:
	//		Default implementation calls BeginStructMember on the referenced struct
	virtual bool BeginPointerMember		(parPtrToStructure& ptrRef,		parMemberStruct& metadata);

	// PURPOSE: This function is called after the visitor as visited a pointer to parsable data
	// PARAMS:
	//		ptrRef - a reference to the pointer member
	//		metadata - The metadata description of the member being visited
	// NOTES:
	//		Default implementation calls EndStructMember on the referenced struct
	virtual void EndPointerMember		(parPtrToStructure& ptrRef,		parMemberStruct& metadata);

	// PURPOSE: This function is called for the first struct the parser visits
	// PARAMS:
	//		ptrToStruct - A pointer to the struct that's about to be visited
	//		metadata - The metadata description of the member being visited
	// RETURNS:
	//		true if the visitor should visit members of the structure, false to skip them
	// NOTES:
	//		The reason this is seperate from BeginStructMember is that the first struct
	//		visited isn't necessarily a member of a parsable structure, so there is no
	//		parMemberStruct metadata.
	//		Default implementation returns true.
	virtual bool BeginToplevelStruct	(parPtrToStructure ptrToStruct,	parStructure& metadata);

	// PURPOSE: This function is called for the first struct the parser visits
	// PARAMS:
	//		ptrToStruct - A pointer to the struct that's about to be visited
	//		metadata - The metadata description of the member being visited
	// RETURNS:
	//		true if the visitor should visit members of the structure, false to skip them
	// NOTES:
	//		Default implementation does nothing.
	virtual void EndToplevelStruct		(parPtrToStructure ptrToStruct,	parStructure& metadata);

	// PURPOSE: This function is called for array members that the visitor visits.
	// PARAMS:
	//		ptrToMember - A pointer to the array member
	//		arrayContents - A pointer to the beginning of the array
	//		numElements - The number of items in the array
	//		metadata - The metadata description of the member being visited
	// RETURNS:
	//		true if the visitor should visit each member of the array
	// NOTES: Default implementation calls AnyMember, returns true
	virtual bool BeginArrayMember		(parPtrToMember ptrToMember,	parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

	// PURPOSE: This function is called when the visitor finishes visiting an array member
	// PARAMS:
	//		ptrToMember - A pointer to the array member
	//		arrayContents - A pointer to the beginning of the array
	//		numElements - The number of items in the array
	//		metadata - The metadata description of the member being visited
	//	NOTES: Default implementation does nothing.
	virtual void EndArrayMember			(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

    // PURPOSE: This function is called for map members that the visitor visits.
    // PARAMS:
    //		structAddr - pointer to the struct being visited
    //		metadata - The metadata description of the member being visited
    // RETURNS:
    //		true if the visitor should visit each member of the map
    // NOTES: Default implementation calls AnyMember, returns true
    virtual bool BeginMapMember	(parPtrToStructure structAddr,	parMemberMap& metadata);

    // PURPOSE: This function is called when the visitor finishes visiting a map member
    // PARAMS:
    //		structAddr - pointer to the struct being visited
    //		metadata - The metadata description of the member being visited
    //	NOTES: Default implementation does nothing.
    virtual void EndMapMember (parPtrToStructure structAddr, parMemberMap& metadata);

    // PURPOSE: This function is called for visiting a map data pair
    // PARAMS:
    //		structAddr - pointer to the struct being visited
    //      mapKeyAddress - A pointer to the key value
    //      mapDataAddress - A pointer to the data value
    //		metadata - The metadata description of the member being visited
    //	NOTES: Default implementation does nothing.
    virtual void VisitMapMember (parPtrToStructure structAddr, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata);

	// PURPOSE: This is the main function that, given a member calls the appropriate *Member function
	//		and recurses if necessary.
	// PARAMS:
	//		containingStructPtr - pointer to the struct being visited
	//		metadata - metadata for the member being visited
	// NOTES:
	//		You normally shouldn't need to override this function, but it's available for special cases.
	//		If you do override it, call the base class implementation to make sure that all the
	//		appropriate *Member functions are called.
	virtual void VisitMember(parPtrToStructure containingStructPtr, parMember& metadata);

	atFixedBitSet16 GetVisitMask() {return m_VisitMask;}
	void SetVisitMask(atFixedBitSet16 newMask) {m_VisitMask = newMask;}

	// PURPOSE: The type mask is an optional optimization. VisitMember will
	// only be called on members whose type is set in the mask. By default all member types are visited.
	// NOTES:
	//		Normally when visiting structure members we'll call VisitMember on every one, 
	//		and VisitMember will call one of the *Member() functions, which may call 
	//		another more general one and so on. This can stack up to quite a few virtual
	//		function calls that don't do anything. So you can set the MemberTypeMask 
	//		(using parMember::Type enumerated values) and restict which members get
	//		VisitMember called on them.
	//		Also note that arrays and structures are ALWAYS visited, because they could
	//		contain visitable items.
	typedef atFixedBitSet<parMemberType::LAST_TYPE+1,u32> TypeMask;

	TypeMask GetMemberTypeMask() {return m_TypeMask;}
	void SetMemberTypeMask(const TypeMask& newMask) {
		m_TypeMask = newMask;
		m_TypeMask.Set(parMemberType::TYPE_ARRAY);
		m_TypeMask.Set(parMemberType::TYPE_STRUCT);
        m_TypeMask.Set(parMemberType::TYPE_MAP);
	}

protected:
	// PURPOSE:	While a visitor is iterating, calling this function will cause it to skip 
	//		over all sibling members
	// NOTES:
	//		This will skip over both sibling members of a structure and array members
	void SkipSiblings() {m_SkipSiblings = true;}

	// PURPOSE: While a visitor is iterating, calling this function will cause it to end
	//		its iteration.
	// NOTES:
	//		Any Begin* functions will have their matching End* functions called before
	//		iteration is finished.
	void EndTraversal() {m_EndTraversal = true;}

	// The *Member functions can use this to get the address of the containing structure
	// It's the same value that gets passed to VisitMember
	parPtrToStructure m_ContainingStructureAddress;
	parStructure* m_ContainingStructureMetadata;

	atFixedBitSet16 m_VisitMask; // only visit members with one of the flags in this mask set.

#if PARSER_ALL_METADATA_HAS_NAMES
	atFixedArray<atLiteralHashString, 32> m_Path;
	static const char* PrintContext(void* arg1, void* arg2);
#endif

private:
	// (private so subclasses can't get around the restrictions on visitable types)
	TypeMask m_TypeMask; // only visit members whose type is specified in this mask

	bool	m_SkipSiblings;
	bool	m_EndTraversal;
};

template<typename _Type>
void parInstanceVisitor::Visit(_Type& instance)
{
	parStructure* structure = instance.parser_GetStructure();
	FastAssert(structure);
	VisitToplevelStructure(instance.parser_GetPointer(), *structure);
}

}

#endif
