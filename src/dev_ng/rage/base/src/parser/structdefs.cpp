// 
// parser/structdefs.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "structdefs.h"

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS

#include "manager.h"
#include "membermap.h"
#include "optimisations.h"
#include "treenode.h"

#include "file/asset.h"
#include "file/device.h"
#include "math/simplemath.h"
#include "system/nelem.h"
#include "system/platform.h"

PARSER_OPTIMISATIONS();

using namespace rage;

void parMangleTypeName(const char* src, char* dest)
{
	// Skip any initial ::
	if (*src == ':')
		src++;
	if (*src == ':')
		src++;

	while(*src)
	{
		if (*src == ':')
		{
			*dest = '_';
		}
		else
		{
			*dest = *src;
		}
		dest++;
		src++;
	}
	*dest = '\0';
}

enum parLookupIntStatus
{
	PAR_LOOKUP_INT_LITERAL,
	PAR_LOOKUP_NAMED_CONSTANT,
	PAR_LOOKUP_NOT_FOUND,
};

parLookupIntStatus parLookupIntAttribute(parTreeNode& node, const char* attributeName, const parStructdefCatalog::ConstTable* constTable, int& outValue)
{
	const char* strValue = NULL;
	char buf[64];   // Needs to be at least 50 to hold any converted non-string value
	node.FindValueFromPath(attributeName, strValue, buf, NELEM(buf));
	if (strValue)
	{
		int intValue = 0;
		if (parUtils::StringToIntSafe(strValue, intValue))
		{
			outValue = intValue;
			return PAR_LOOKUP_INT_LITERAL;
		}
		else if (constTable)
		{
			const int* tableValue = constTable->SafeGet(strValue);
			if (tableValue)
			{
				outValue = *tableValue;
				return PAR_LOOKUP_NAMED_CONSTANT;
			}
			else
			{
				Errorf("Couldn't convert attribute %s (\"%s\") to an integer. Are you missing a <const> declaration?", attributeName, strValue);
			}
		}
	}
	return PAR_LOOKUP_NOT_FOUND;
}

parStructdefCatalog::~parStructdefCatalog()
{
	for(int i = 0; i < m_FileInfos.GetCount(); i++)
	{
		delete m_FileInfos[i]->m_Root;
		delete m_FileInfos[i];
	}
}

#define PSC_FILE_INDEX_ATTR "_pscFileIndex"
	
void parStructdefCatalog::LoadPscFile(const char* filename)
{
	parSettings s = PARSER.Settings();
	s.SetFlag(parSettings::CULL_OTHER_PLATFORM_DATA, true);
	s.SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, false);
	s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
	parTree* tree = PARSER.LoadTree(filename, "psc", &s);

	if (!tree)
	{
		parWarningf("Couldn't load PSC file %s", filename);
		return;
	}
	
	PscFileInfo* fileInfo = rage_new PscFileInfo;
	fileInfo->m_Root = tree;

	m_FileInfos.PushAndGrow(fileInfo);
	int fileIndex = m_FileInfos.GetCount()-1;

	for(parTreeNode::ChildNodeIterator iter = tree->GetRoot()->BeginChildren(); iter != tree->GetRoot()->EndChildren(); ++iter)
	{
		parTreeNode* node = *iter;

		if (!strcmp(node->GetElement().GetName(), "enumdef"))
		{
			const char* enumdefType;
			if (node->FindValueFromPath("@type", enumdefType))
			{
				char mangledName[256];
				parMangleTypeName(enumdefType, mangledName);

				atLiteralHashString nameHash(mangledName);

				m_Enumdefs[nameHash.GetHash()] = node;
				node->GetElement().AddAttribute(PSC_FILE_INDEX_ATTR , fileIndex, false);
			}
		}
		else if (!strcmp(node->GetElement().GetName(), "structdef"))
		{
			const char* structdefType;
			if (node->FindValueFromPath("@type", structdefType))
			{
				char mangledTypeName[256];
				parMangleTypeName(structdefType, mangledTypeName);

				const char* structdefName = "";
				atLiteralHashString nameHash;

				if (node->FindValueFromPath("@name", structdefName))
				{
					nameHash.SetFromString(structdefName);
					// We have a structure whose name and type don't match. Add a map entry from the type string to the
					// name string so we can search by type later
					// And use the MANGLED type name here because "::rage::foo" and "rage::foo" mangle to the same thing ("rage__foo")
					m_TypeHashToNameHash[atLiteralStringHash(mangledTypeName)] = nameHash.GetHash();
				}
				else
				{
					nameHash.SetFromString(mangledTypeName);
				}

				m_Structdefs[nameHash.GetHash()] = node;
				node->GetElement().AddAttribute(PSC_FILE_INDEX_ATTR , fileIndex, false);
			}
		}
		else if (!strcmp(node->GetElement().GetName(), "const"))
		{
			const char* constName = node->GetElement().FindAttributeStringValue("name", "", NULL, 0, false, true);
			int constValue = node->GetElement().FindAttributeIntValue("value", 0, true);
			fileInfo->m_ConstTable.Insert(constName, constValue);
		}
	}
	fileInfo->m_ConstTable.FinishInsertion();
}

void parStructdefCatalog::LoadEsdFile(const char* filename)
{
	parTree* tree = PARSER.LoadTree(filename, "");

	if (!tree)
	{
		Errorf("Error loading ESD file %s", filename);
		return;
	}

	PscFileInfo* fileInfo = rage_new PscFileInfo;
	fileInfo->m_Root = tree;
	m_FileInfos.PushAndGrow(fileInfo);

	for(parTreeNode::ChildNodeIterator iter = tree->GetRoot()->BeginChildren(); iter != tree->GetRoot()->EndChildren(); ++iter)
	{
		parTreeNode* node = *iter;

		m_Structdefs[atLiteralStringHash(node->GetElement().GetName())] = node->GetChild();
	}
}

namespace {
	class EnumFilesParams
	{
	public:
		EnumFilesParams() : m_Catalog(NULL), m_Path(NULL) {}

		parStructdefCatalog* m_Catalog;
		const char*        m_Path;
		int					m_NumFiles;
	};

void EnumFilesCB(const fiFindData& data, void* userCB)
{
	EnumFilesParams* params = reinterpret_cast<EnumFilesParams*>(userCB);

	if (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		const char* oldPath = params->m_Path;

		atVarString newPath("%s/%s", oldPath, data.m_Name);

		params->m_Path = newPath.c_str();
		
		ASSET.EnumFiles(newPath, &EnumFilesCB, params);

		params->m_Path = oldPath;
	}
	else // not a directory, assume normal file
	{
		const char* extn = ASSET.FindExtensionInPath(data.m_Name);
		if (extn && !strcmp(extn, ".psc"))
		{
			ASSET.PushFolder(params->m_Path);
			params->m_NumFiles++;
			params->m_Catalog->LoadPscFile(data.m_Name);
			ASSET.PopFolder();
		}
	}
}
}

void parStructdefCatalog::LoadAllPscFiles(const char* rootPath)
{
	EnumFilesParams params;
	params.m_Catalog = this;
	params.m_Path = rootPath;
	params.m_NumFiles = 0;

	int oldNumStructdefs = m_Structdefs.GetNumUsed();
	int oldNumEnumdefs = m_Enumdefs.GetNumUsed();

	ASSET.EnumFiles(rootPath, &EnumFilesCB, &params);

	parDisplayf("Loaded %d PSC files containing %d structdefs and %d enumdefs", params.m_NumFiles, m_Structdefs.GetNumUsed() - oldNumStructdefs, m_Enumdefs.GetNumUsed() - oldNumEnumdefs);
}

parTreeNode* parStructdefCatalog::FindStructdef(atLiteralHashValue name)
{
	parTreeNode** node = m_Structdefs.Access(name.GetHash());
	return node ? *node : NULL;
}

void parStructdefCatalog::DeleteStructdef(atLiteralHashValue namehash)
{
	parTreeNode** node = m_Structdefs.Access(namehash.GetHash());
	if (!node)
	{
		return;
	}

	(*node)->RemoveSelf();
	delete *node;

	m_Structdefs.Delete(namehash.GetHash());
}

void parStructdefCatalog::DeleteEnumdef(atLiteralHashValue namehash)
{
	parTreeNode** node = m_Enumdefs.Access(namehash.GetHash());
	if (!node)
	{
		return;
	}

	(*node)->RemoveSelf();
	delete *node;

	m_Enumdefs.Delete(namehash.GetHash());
}

parTreeNode* parStructdefCatalog::FindEnumdef(atLiteralHashValue name)
{
	parTreeNode** node = m_Enumdefs.Access(name.GetHash());
	return node ? *node : NULL;
}

namespace rage
{
namespace parStructdefBuilder
{
	using namespace parMemberType;

	struct parSdTypeMap
	{
		const char* m_Name;
		const char* m_SubtypeAttr;
		const char* m_SubtypeValue;
		parMemberType::Enum m_Type;
		s32	m_Subtype;
		bool m_Simple;
	};

	// This is the state that we need to pass around during the process for building one particular structdef or enumdef
	struct BuilderInfo
	{
		BuilderInfo(parStructdefCatalog& cat) : m_Catalog(cat), m_ConstTable(NULL) {}

		parStructdefCatalog& m_Catalog;
		const parStructdefCatalog::ConstTable* m_ConstTable;
	};

	const s32 IGNORED_NODE = -4;
	const s32 PADDING_PLACEHOLDER = -3;
	const s32 MEMBER_NOT_ALLOWED = -2;
	const s32 SUBTYPE_NEEDS_MORE_INFO = -1;

	// Note - matching happens from top down - so something with a non-NULL subtypeAttr needs to be placed before
	// something with a NULL subtypeAttr.
	const parSdTypeMap g_StructdefTypeMap[] = 
	{
	//  name		subtypeAttr,	subtypeVal,		type			subtype										simple		extraData
		{"float",	"type",			"angle",		TYPE_FLOAT,		parMemberSimpleSubType::SUBTYPE_ANGLE,		true},
		{"float",	NULL,			NULL,			TYPE_FLOAT,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"Float16",	NULL,			NULL,			TYPE_FLOAT16,	parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"int",		NULL,			NULL,			TYPE_INT,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"s32",		NULL,			NULL,			TYPE_INT,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"u32",		"type",			"color",		TYPE_UINT,		parMemberSimpleSubType::SUBTYPE_COLOR,		true},
		{"u32",		NULL,			NULL,			TYPE_UINT,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"Color32",	NULL,			NULL,			TYPE_UINT,		parMemberSimpleSubType::SUBTYPE_COLOR,		true},
		{"char",	NULL,			NULL,			TYPE_CHAR,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"s8",		NULL,			NULL,			TYPE_CHAR,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"u8",		NULL,			NULL,			TYPE_UCHAR,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"short",	NULL,			NULL,			TYPE_SHORT,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"s16",		NULL,			NULL,			TYPE_SHORT,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"u16",		NULL,			NULL,			TYPE_USHORT,	parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"bool",	NULL,			NULL,			TYPE_BOOL,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"ScalarV",	NULL,			NULL,			TYPE_SCALARV,	parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"BoolV",	NULL,			NULL,			TYPE_BOOLV,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"size_t",	NULL,			NULL,			TYPE_SIZET,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"ptrdiff_t",	NULL,		NULL,			TYPE_PTRDIFFT,	parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"s64",		NULL,			NULL,			TYPE_INT64,		parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"u64",		NULL,			NULL,			TYPE_UINT64,	parMemberSimpleSubType::SUBTYPE_NORMAL,		true},
		{"double",	NULL,			NULL,			TYPE_DOUBLE,	parMemberSimpleSubType::SUBTYPE_NORMAL,		true},

		{"Vector2", NULL,			NULL,			TYPE_VECTOR2,	parMemberVectorSubType::SUBTYPE_NORMAL,		true},
		{"Vector3", "type",			"color",		TYPE_VECTOR3,	parMemberVectorSubType::SUBTYPE_COLOR,		true},
		{"Vector3", NULL,			NULL,			TYPE_VECTOR3,	parMemberVectorSubType::SUBTYPE_NORMAL,		true},
		{"Vector4", NULL,			NULL,			TYPE_VECTOR4,	parMemberVectorSubType::SUBTYPE_NORMAL,		true},
		{"Vec2V",	NULL,			NULL,			TYPE_VEC2V,		parMemberVectorSubType::SUBTYPE_NORMAL,		true},
		{"Vec3V",	"type",			"color",		TYPE_VEC3V,		parMemberVectorSubType::SUBTYPE_COLOR,		true},
		{"Vec3V",	NULL,			NULL,			TYPE_VEC3V,		parMemberVectorSubType::SUBTYPE_NORMAL,		true},
		{"Vec4V",	NULL,			NULL,			TYPE_VEC4V,		parMemberVectorSubType::SUBTYPE_NORMAL,		true},
		{"VecBoolV",NULL,			NULL,			TYPE_VECBOOLV,	parMemberVectorSubType::SUBTYPE_NORMAL,		true},

		{"Matrix34", NULL,			NULL,			TYPE_MATRIX34,	0,											true},
		{"Matrix44", NULL,			NULL,			TYPE_MATRIX44,	0,											true},
		{"Mat33V",	NULL,			NULL,			TYPE_MAT33V,	0,											true},
		{"Mat34V",	NULL,			NULL,			TYPE_MAT34V,	0,											true},
		{"Mat44V",	NULL,			NULL,			TYPE_MAT44V,	0,											true},

		{"string",	"type",			"member",			TYPE_STRING,	parMemberStringSubType::SUBTYPE_MEMBER,				false},
		{"string",	"type",			"pointer",			TYPE_STRING,	parMemberStringSubType::SUBTYPE_POINTER,			false},
		{"string",	"type",			"ConstString",		TYPE_STRING,	parMemberStringSubType::SUBTYPE_CONST_STRING,		false},
		{"string",	"type",			"atString",			TYPE_STRING,	parMemberStringSubType::SUBTYPE_ATSTRING,			false},
		{"string",	"type",			"wide_member",		TYPE_STRING,	parMemberStringSubType::SUBTYPE_WIDE_MEMBER,		false},
		{"string",	"type",			"wide_pointer",		TYPE_STRING,	parMemberStringSubType::SUBTYPE_WIDE_POINTER,		false},
		{"string",	"type",			"atWideString",		TYPE_STRING,	parMemberStringSubType::SUBTYPE_ATWIDESTRING,		false},
		{"string",  "type",			"atHashValue",		TYPE_STRING,	parMemberStringSubType::SUBTYPE_ATHASHVALUE,		false},
		{"string",  "type",			"atPartialHashValue", TYPE_STRING,	parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE,	false},
		{"string",	"type",			"atFinalHashString",TYPE_STRING,	parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING,	false},

		{"string",	"type",			"atHashWithStringDev",	TYPE_STRING,		__DEV ? parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : parMemberStringSubType::SUBTYPE_ATHASHVALUE,		false},
		{"string",	"type",			"atHashWithStringBank",	TYPE_STRING,		__BANK ? parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : parMemberStringSubType::SUBTYPE_ATHASHVALUE,	false},
		{"string",	"type",			"atHashString",	TYPE_STRING,	!__FINAL ? parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : parMemberStringSubType::SUBTYPE_ATHASHVALUE,	false},
		{"string",	"type",			"atNonFinalHashString",	TYPE_STRING,	!__FINAL ? parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : parMemberStringSubType::SUBTYPE_ATHASHVALUE,	false},
		{"string",	"type",			"atHashWithStringNotFinal",	TYPE_STRING,	!__FINAL ? parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : parMemberStringSubType::SUBTYPE_ATHASHVALUE,	false},

		{"string",	NULL,			NULL,			TYPE_STRING,	SUBTYPE_NEEDS_MORE_INFO,					false}, // If none of the other strings matched, we need to check the namespaces

		{"enum",	"size",			"32",			TYPE_ENUM,		parMemberEnumSubType::SUBTYPE_32BIT,		false},
		{"enum",	"size",			"16",			TYPE_ENUM,		parMemberEnumSubType::SUBTYPE_16BIT,		false},
		{"enum",	"size",			"8",			TYPE_ENUM,		parMemberEnumSubType::SUBTYPE_8BIT,			false},
		{"enum",	NULL,			NULL,			TYPE_ENUM,		parMemberEnumSubType::SUBTYPE_32BIT,		false},
		
		{"bitset",	"type",			"fixed",		TYPE_BITSET,	parMemberBitsetSubType::SUBTYPE_32BIT_FIXED,	false},
		{"bitset",	"type",			"fixed32",		TYPE_BITSET,	parMemberBitsetSubType::SUBTYPE_32BIT_FIXED,	false},
		{"bitset",	"type",			"fixed16",		TYPE_BITSET,	parMemberBitsetSubType::SUBTYPE_16BIT_FIXED,	false},
		{"bitset",	"type",			"fixed8",		TYPE_BITSET,	parMemberBitsetSubType::SUBTYPE_8BIT_FIXED,		false},
		{"bitset",	"type",			"atBitSet",		TYPE_BITSET,	parMemberBitsetSubType::SUBTYPE_ATBITSET,		false},
		{"bitset",  "type",			"generated",	TYPE_BITSET,	parMemberBitsetSubType::SUBTYPE_32BIT_FIXED,	false},

		{"map",		"type",			"atMap",		TYPE_MAP,		parMemberMapSubType::SUBTYPE_ATMAP,			false},
		{"map",		"type",			"atBinaryMap",	TYPE_MAP,		parMemberMapSubType::SUBTYPE_ATBINARYMAP,	false},

		{"array",	"type",			"atArray",		TYPE_ARRAY,		parMemberArraySubType::SUBTYPE_ATARRAY,		false},
		{"array",	"type",			"atFixedArray",	TYPE_ARRAY,		parMemberArraySubType::SUBTYPE_ATFIXEDARRAY,false},
		{"array",	"type",			"atRangeArray",	TYPE_ARRAY,		parMemberArraySubType::SUBTYPE_ATRANGEARRAY,false},
		{"array",	"type",			"pointer",		TYPE_ARRAY,		SUBTYPE_NEEDS_MORE_INFO,					false},	// Need to check sizeVar to see the subtype
		{"array",	"type",			"member",		TYPE_ARRAY,		parMemberArraySubType::SUBTYPE_MEMBER,		false},
		{"array",	"type",			"virtual",		TYPE_ARRAY,		parMemberArraySubType::SUBTYPE_VIRTUAL,		false},

		{"struct",	NULL,			NULL,			TYPE_STRUCT,	parMemberStructSubType::SUBTYPE_STRUCTURE,	false},
		{"class",	NULL,			NULL,			TYPE_STRUCT,	parMemberStructSubType::SUBTYPE_STRUCTURE,	false},

		{"pointer",	"policy",		"owner",		TYPE_STRUCT,	parMemberStructSubType::SUBTYPE_POINTER,				false},
		{"pointer",	"policy",		"simple_owner",	TYPE_STRUCT,	parMemberStructSubType::SUBTYPE_SIMPLE_POINTER,			false},
		{"pointer",	"policy",		"external_named", TYPE_STRUCT,	SUBTYPE_NEEDS_MORE_INFO,								false},
		
		{"pad",		NULL,			NULL,			INVALID_TYPE,	PADDING_PLACEHOLDER,						false},

		{"hinsert",	NULL,			NULL,			INVALID_TYPE,	IGNORED_NODE,								false},
		{"assert",	NULL,			NULL,			INVALID_TYPE,	IGNORED_NODE,								false},
		{"report",	NULL,			NULL,			INVALID_TYPE,	IGNORED_NODE,								false},

		{NULL,		NULL,			NULL,			INVALID_TYPE,	MEMBER_NOT_ALLOWED,							false},
	};

	parMember* CreateMemberFromXml( const BuilderInfo& cat, parTreeNode& memberXml );

	const parSdTypeMap* FindMemberTypeInfo(parTreeNode& member)
	{
		const parSdTypeMap* item = &g_StructdefTypeMap[0];

		const char* memberType = member.GetElement().GetName();

		bool found = false;

		while(item->m_Name)
		{
			if (!strcmp(memberType, item->m_Name))
			{
				// is there a subtype attr we need to check?
				if (item->m_SubtypeAttr)
				{
					char buf[50];
					const char* subtypeValue = member.GetElement().FindAttributeStringValue(item->m_SubtypeAttr, NULL, buf, sizeof(buf), false, false);
					if (subtypeValue && !strcmp(subtypeValue, item->m_SubtypeValue))
					{
						found = true;
						break;
					}
				}
				else
				{
					found = true;
					break;
				}
			}

			item++;
		}

		if (found)
		{
			if (item->m_Subtype == MEMBER_NOT_ALLOWED)
			{
				parErrorf("Couldn't convert <%s> to a parMember, type or subtype is not allowed", memberType);
				return NULL;
			}
			else
			{
				return item;
			}
		}
		else
		{
			parErrorf("Couldn't find parMember info for <%s>", memberType);
			return NULL;
		}
	}

	template<typename _Type>
	void SetCommonData(_Type& data, const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		const char* name = "Item"; // If it's unnamed, use "Item" instead - currently works for array and map prototype members
		if (!memberXml.FindValueFromPath("@parName", name))
		{
			memberXml.FindValueFromPath("@name", name);
			if (!strncmp(name, "m_", 2))
			{
				name += 2;
			}
		}

		atLiteralHashString hashStr(name); // 'register' the name

		data.Init(); // Zeroes everything

		data.m_NameHash = atLiteralHashString(name).GetHash();
		data.m_Type = (u8)memInfo.m_Type;
		FastAssert(memInfo.m_Subtype >= 0 && memInfo.m_Subtype < UCHAR_MAX);
		data.m_Subtype = (u8)memInfo.m_Subtype;
	}

	parMemberSimple* InitSimpleMember(const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parMemberSimpleData* data = rage_new parMemberSimpleData;
		SetCommonData(*data, memInfo, memberXml);
		
		parMemberSimple* member = rage_new parMemberSimple(*data);

		if (memInfo.m_Type == TYPE_BOOL || memInfo.m_Type == TYPE_BOOLV)
		{
			bool initVal = false;
			memberXml.FindValueFromPath("@init", initVal);
			data->m_Init = initVal ? 1.0f : 0.0f;
		}
		else
		{
			memberXml.FindValueFromPath("@init", data->m_Init);
		}

		return member;
	}

	parMemberVector* InitVectorMember(const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parMemberVectorData* data = rage_new parMemberVectorData;
		SetCommonData(*data, memInfo, memberXml);

		parMemberVector* member = rage_new parMemberVector(*data);

		// TODO: Handle init attributes here?

		return member;
	}

	parMemberMatrix* InitMatrixMember(const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parMemberMatrixData* data = rage_new parMemberMatrixData;
		SetCommonData(*data, memInfo, memberXml);

		parMemberMatrix* member = rage_new parMemberMatrix(*data);

		// TODO: Handle init attributes here?

		return member;
	}

	void* DummyNameToPtrCB(const char* name) 
	{
		return StringDuplicate(name);
	}

	const char* DummyPtrToNameCB(const void* ptr)
	{
		return (const char*)ptr;
	}

	parMemberStruct* InitStructMember(const BuilderInfo& builder, const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parSdTypeMap localMemInfo = memInfo; // make a copy, so we can modify it before calling SetCommonData

		bool isExternalNamed = false;

		if (localMemInfo.m_Subtype == SUBTYPE_NEEDS_MORE_INFO)
		{
			// its external_named - but is it usernull or not?
			isExternalNamed = true;
			bool userNull = false;
			memberXml.FindValueFromPath("@userHandlesNull", userNull);
			localMemInfo.m_Subtype = userNull ? (u8)parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL : (u8)parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER;
		}

		parMemberStructData* data = rage_new parMemberStructData;
		SetCommonData(*data, localMemInfo, memberXml);

		const char* baseTypeName = "";
		memberXml.FindValueFromPath("@type", baseTypeName);

		// This may cause a recursion, if the basetype itself needs to be created from a structdef
		char mangledBaseName[RAGE_MAX_PATH];
		parMangleTypeName(baseTypeName, mangledBaseName);

		data->m_StructurePtr = builder.m_Catalog.FindStructureWithTypeHash(atLiteralHashValue(mangledBaseName));

		if (isExternalNamed)
		{
			data->m_NameToPtrCB = DummyNameToPtrCB;
			data->m_PtrToNameCB = DummyPtrToNameCB;
		}

		parFatalAssertf(data->m_StructurePtr, "Couldn't find the type definition for a <struct type=\"%s\"/> node", baseTypeName);

		parMemberStruct* member = rage_new parMemberStruct(*data);

		return member;
	}

	parMemberString* InitStringMember(const BuilderInfo& builder, const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parSdTypeMap localMemInfo = memInfo; // make a copy, so we can modify it before calling SetCommonData
		int nsIndex = -1;

		if (localMemInfo.m_Subtype == SUBTYPE_NEEDS_MORE_INFO)
		{
			// Check the namespace names to see if it's listed there
			const char* typeName = "";
			if (memberXml.FindValueFromPath("@type", typeName))
			{
				for(int i = 0; i < HSNS_NUM_NAMESPACES; i++)
				{
					if (!strcmp(typeName, atHashStringNamespaceSupport::GetNamespaceName((atHashStringNamespaces)i))) {
						localMemInfo.m_Subtype = parMemberStringSubType::SUBTYPE_ATNSHASHSTRING;
						nsIndex = i;
						break;
					}
					else if (!strcmp(typeName, atHashStringNamespaceSupport::GetNamespaceValueName((atHashStringNamespaces)i))) {
						localMemInfo.m_Subtype = parMemberStringSubType::SUBTYPE_ATNSHASHVALUE;
						nsIndex = i;
						break;
					}
				}

				if (!parVerifyf(nsIndex >= 0, "A string with type=\"%s\" isn't valid, it's not a standard type name or the name of a hash string namespace", typeName))
				{
					return NULL;
				}
			}
			else {
				parAssertf(0, "Couldn't find required 'type' attribute in a <string> node. See https://devstar.rockstargames.com/wiki/index.php/String_PSC_tag");
				return NULL;
			}
		}
		else
		{
   			nsIndex = 0;	
		}

		parMemberStringData* data = rage_new parMemberStringData;
		SetCommonData(*data, localMemInfo, memberXml);
		data->SetNamespaceIndex((atHashStringNamespaces)nsIndex);

		int length = 0;
		parLookupIntAttribute(memberXml, "@size", builder.m_ConstTable, length);

		data->m_Length = (u32)length;

		parMemberString* member = rage_new parMemberString(*data);

		const char* initStr = NULL;
		char initStrBuf[RAGE_MAX_PATH];
		if (memberXml.FindValueFromPath("@init", initStr, initStrBuf, RAGE_MAX_PATH))
		{
			member->InitExtraAttributes();
			member->GetExtraAttributes()->AddAttribute("initValue", initStr, false, true);
		}

		return member;
	}

	parMemberArray* InitArrayMember(const BuilderInfo& builder, const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parSdTypeMap localMemInfo = memInfo; // make a copy, so we can modify it before calling SetCommonData

		parAttributeList* attrs = NULL;

		if (localMemInfo.m_Subtype == SUBTYPE_NEEDS_MORE_INFO)
		{
			if(memberXml.FindAttributeFromXPath("@sizeVar"))
			{
				localMemInfo.m_Subtype = parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT;

				const char* sizeVar = "";
				memberXml.FindValueFromPath("@sizeVar", sizeVar);

				attrs = rage_new parAttributeList;
				attrs->AddAttribute("sizeVar", sizeVar, false, true);
			}
			else
			{
				localMemInfo.m_Subtype = parMemberArraySubType::SUBTYPE_POINTER;
			}
		}		

		parMemberArrayData* data = rage_new parMemberArrayData;
		SetCommonData(*data, localMemInfo, memberXml);

		data->m_ExtraAttributes = attrs;

		parMember* prototypeMember = parStructdefBuilder::CreateMemberFromXml(builder, *memberXml.GetChild());

		data->m_ElementSize = prototypeMember->GetSize();

		parMemberArray* member = rage_new parMemberArray(*data, prototypeMember);

		member->SetPrototypeMember(prototypeMember);

		if (member->HasMaxSize())
		{
			int elts = 0;
			parLookupIntAttribute(memberXml, "@size", builder.m_ConstTable, elts);
			data->m_NumElements = (u32)elts;
		}

		int align = 0;
		memberXml.FindValueFromPath("@align", align);

		const char* heap = NULL;
		if (memberXml.FindValueFromPath("@heap", heap, NULL, 0))
		{
			if (!strcmp(heap, "physical"))
			{
				member->GetCommonData()->m_TypeFlags |= (1 << parMemberArrayData::FLAG_ALLOCATE_IN_PHYSICAL_MEMORY);
			}
		}

		u16 alignPower = 0;

		while(align>1)
		{
			alignPower++;
			align >>= 1;
		}

		// clear any existing alignment, then set the new alignment
		u16 clearMask = (u16)(~(parMemberFlags::MEMBERARRAY_ALIGN_MASK << parMemberFlags::MEMBERARRAY_ALIGN_SHIFT));
		member->GetCommonData()->m_Flags &= clearMask;

		u16 shiftedAlign = (alignPower& parMemberFlags::MEMBERARRAY_ALIGN_MASK) << parMemberFlags::MEMBERARRAY_ALIGN_SHIFT;
		member->GetCommonData()->m_Flags |= shiftedAlign;

		return member;
	}

	parMemberEnum* InitEnumMember(const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parMemberEnumData* data = rage_new parMemberEnumData;
		SetCommonData(*data, memInfo, memberXml);

		parMemberEnum* member = rage_new parMemberEnum(*data);

		const char* enumType = "";
		memberXml.FindValueFromPath("@type", enumType);

		char mangledEnumType[256];
		parMangleTypeName(enumType, mangledEnumType);

		data->m_EnumData = PARSER.GetExternalStructureManager().FindOrBuildFromEnumdef(atLiteralStringHash(mangledEnumType));

		const char* initString = "";
		memberXml.FindValueFromPath("@init", initString);
		data->m_Init = data->m_EnumData->ValueFromName(initString);

		return member;
	}

	parMemberBitset* InitBitsetMember(const BuilderInfo& builder, const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parMemberEnumData* data = rage_new parMemberEnumData;
		SetCommonData(*data, memInfo, memberXml);
		data->m_EnumData = &parEnumData::Empty;

		parMemberBitset* member = rage_new parMemberBitset(*data);

		// TODO: Handle init attributes here?

		const char* valuesType = NULL;
		memberXml.FindValueFromPath("@values", valuesType);
		if (valuesType)
		{
			char mangledValuesType[256];
			parMangleTypeName(valuesType, mangledValuesType);

			data->m_EnumData = PARSER.GetExternalStructureManager().FindOrBuildFromEnumdef(atLiteralStringHash(mangledValuesType));
			if (!data->m_EnumData)
			{
				parWarningf("Couldn't find enum definition for enum \"%s\"", valuesType);
			}
		}

		int numBits = 0;
		// Find the default value for numbits
		const char* sizeType = "fixed";
		memberXml.FindValueFromPath("@type", sizeType);
		if (!strcmp(sizeType, "fixed") || !strcmp(sizeType, "fixed32"))
		{
			numBits = 32;
		}
		else if (!strcmp(sizeType, "fixed16"))
		{
			numBits = 16;
		}
		else if (!strcmp(sizeType, "fixed8"))
		{
			numBits = 8;
		}

		// Check for special cases: 
		// type="generated" => numBits comes from values_MAX_VALUE + 1
		// numBits="use_values" => numBits comes from values_MAX_VALUE + 1
		// type="atBitSet" => numBits = 0

		const char* numBitsStr = NULL;
		memberXml.FindValueFromPath("@numBits", numBitsStr);

		if (!strcmp(sizeType, "generated") || (numBitsStr && !strcmp(numBitsStr, "use_values")))
		{
			if (parVerifyf(data->m_EnumData, "Couldn't find required enum definition %s", valuesType))
			{
				parAssertf(data->m_EnumData->MinValue() >= 0, "All values must be positive to use enum %s for bitset values", valuesType);
				numBits = data->m_EnumData->MaxValue() + 1;
			}
		}
		else if (numBitsStr)
		{
			int specifiedBits = 0;
			parLookupIntStatus status = parLookupIntAttribute(memberXml, "@numBits", builder.m_ConstTable, specifiedBits);
			if (status == PAR_LOOKUP_NOT_FOUND)
			{
				parErrorf("numBits attribute must be an integer literal or \"use_values\", got \"%s\". See https://devstar.rockstargames.com/wiki/index.php/Enum_PSC_tag", numBitsStr);
			}
			else
			{
				numBits = specifiedBits;
			}
		}

		if (!strcmp(sizeType, "atBitSet"))
		{
			numBits = 0;
		}
		


		data->m_NumBits = (u16)numBits;

		return member;
	}

	parMemberMap* InitMapMember(const BuilderInfo& builder, const parSdTypeMap& memInfo, parTreeNode& memberXml)
	{
		parMemberMapData* data = rage_new parMemberMapData;
		SetCommonData(*data, memInfo, memberXml);

		parMemberMap* member = rage_new parMemberMap(*data);

		member->SetDataMember( CreateMemberFromXml(builder, *memberXml.GetChild()));

		// Make a parTreeNode so we can create a member for the key too.
		parTreeNode* keyNode = rage_new parTreeNode;
		const char* keyTypeName = "";
		memberXml.FindValueFromPath("@key", keyTypeName);
		if (!strcmp(keyTypeName, "atHashString"))
		{
			keyNode->GetElement().SetName("string", false);
			keyNode->GetElement().AddAttribute("type", "atHashString", false, false);
		}
		else if (!strcmp(keyTypeName, "atHashValue"))
		{
			keyNode->GetElement().SetName("string", false);
			keyNode->GetElement().AddAttribute("type", "atHashValue", false, false);
		}
		else if (!strcmp(keyTypeName, "atFinalHashString"))
		{
			keyNode->GetElement().SetName("string", false);
			keyNode->GetElement().AddAttribute("type", "atFinalHashString", false, false);
		}
		else if (!strcmp(keyTypeName, "atHashWithStringDev"))
		{
			keyNode->GetElement().SetName("string", false);
			keyNode->GetElement().AddAttribute("type", "atHashWithStringDev", false, false);
		}
		else if (!strcmp(keyTypeName, "atHashWithStringBank"))
		{
			keyNode->GetElement().SetName("string", false);
			keyNode->GetElement().AddAttribute("type", "atHashWithStringBank", false, false);
		}
		else if (!strcmp(keyTypeName, "atHashWithStringNotFinal"))
		{
			keyNode->GetElement().SetName("string", false);
			keyNode->GetElement().AddAttribute("type", "atHashWithStringNotFinal", false, false);
		}
		else if (!strcmp(keyTypeName, "enum"))
		{
			keyNode->GetElement().SetName("enum", false);
			
			const char* enumKeyType = "";
			memberXml.FindValueFromPath("@enumKeyType", enumKeyType);

			int size = 32;
			memberXml.FindValueFromPath("@enumKeySize", size);

			keyNode->GetElement().AddAttribute("type", enumKeyType, true);
			keyNode->GetElement().AddAttribute("size", size);
		}
		else
		{
			// Assume the type name is the same as the XML element name - works for the other integral types
			keyNode->GetElement().SetName(keyTypeName, true);
		}
		keyNode->GetElement().AddAttribute("name", "Key");

		member->SetKeyMember( CreateMemberFromXml(builder, *keyNode) );
		
		return member;
	}


	parMember* CreateMemberFromXml(const BuilderInfo& builder, parTreeNode& memberXml )
	{
		using namespace parMemberType;

		const parSdTypeMap* memInfo = FindMemberTypeInfo(memberXml);
		if (!memInfo)
		{
			parErrorf("Couldn't find type info for member %s", memberXml.GetElement().GetName());
			return NULL;
		}

		if (memInfo->m_Subtype == PADDING_PLACEHOLDER || memInfo->m_Subtype == IGNORED_NODE)
		{
			return NULL;
		}

		parMember* member = NULL;

		switch(parMemberType::GetClass(memInfo->m_Type))
		{
		case CLASS_SIMPLE:
			member = InitSimpleMember(*memInfo, memberXml);
			break;
		case CLASS_VECTOR:
			member = InitVectorMember(*memInfo, memberXml);
			break;
		case CLASS_MATRIX:
			member = InitMatrixMember(*memInfo, memberXml);
			break;
		case CLASS_STRUCT:
			member = InitStructMember(builder, *memInfo, memberXml);
			break;
		case CLASS_STRING:
			member = InitStringMember(builder, *memInfo, memberXml);
			break;
		case CLASS_ARRAY:
			member = InitArrayMember(builder, *memInfo, memberXml);
			break;
		case CLASS_ENUM:
			member = InitEnumMember(*memInfo, memberXml);
			break;
		case CLASS_BITSET:
			member = InitBitsetMember(builder, *memInfo, memberXml);
			break;
		case CLASS_MAP:
			member = InitMapMember(builder, *memInfo, memberXml);
			break;
		case INVALID_CLASS:
			parErrorf("Shouldn't get here");
			return NULL;
		}
		
		return member;
	}

} // namespace parStructdefBuilder
} // namespace rage

parStructure* parStructdefCatalog::FindStructureWithTypeHash(atLiteralHashValue typeHash)
{
	// Check m_TypeHashToNameHashMap
	// If found, we know typeHash was the hash of a type name where the structdef specified a different 
	// name attribute, so get the hash of the name and go from there.

	u32 hash = typeHash.GetHash();

	u32* mapEntry = m_TypeHashToNameHash.Access(typeHash.GetHash());
	if (mapEntry)
	{
		hash = *mapEntry;
	}

	return PARSER.FindStructure(hash);
}

void parStructdefCatalog::BuildStructure(parStructure& structure, parTreeNode& structdefXml)
{
	const char* rawTypeName = "";
	structdefXml.FindValueFromPath("@type", rawTypeName);

	char mangledTypeName[256];
	parMangleTypeName(rawTypeName, mangledTypeName);

	// See if there's a name attribute - use that instead for the rest of the parser (outside of structdef interpretation)
	const char* name = mangledTypeName;
	structdefXml.FindValueFromPath("@name", name);

	structure.SetName(name);

	// 'register' this name
	atLiteralHashString nameHash(name);

	structure.GetFlags().Set(parStructure::HAS_NAMES);
	structure.GetFlags().Set(parStructure::ALWAYS_HAS_NAMES);

	parStructdefBuilder::BuilderInfo builder(*this);
	int fileIdx = -1;
	if (structdefXml.FindValueFromPath("@" PSC_FILE_INDEX_ATTR, fileIdx))
	{
		builder.m_ConstTable = &m_FileInfos[fileIdx]->m_ConstTable;
	}

	bool baseHasVirtuals = false; 
	bool simple = false;
	structdefXml.FindValueFromPath("@simple", simple);

	const char* baseType = NULL;
	structdefXml.FindValueFromPath("@base", baseType);
	if (baseType)
	{
		char mangledBaseType[256];
		parMangleTypeName(baseType, mangledBaseType);
		parStructure* base = FindStructureWithTypeHash(atLiteralHashValue(mangledBaseType));

		if (base)
		{
			structure.SetBaseClass(base);
			baseHasVirtuals = base->GetFlags().IsSet(parStructure::HAS_VIRTUALS);

			if (simple && baseHasVirtuals)
			{
				Errorf("Type %s is declared as simple, but the base class %s is non-simple (i.e. it has virtuals)", rawTypeName, baseType);
			}
		}
		else
		{
			Errorf("Type %s specified a base class %s, but there's no definition for that class", rawTypeName, baseType);
		}
	}

	// we have virtuals, unless there's a simple="true" attribute or the base class has virtuals
	structure.GetFlags().Set(parStructure::HAS_VIRTUALS, baseHasVirtuals || !simple);

	bool constructible = structdefXml.GetElement().FindAttributeBoolValue("constructable", true);
	constructible = structdefXml.GetElement().FindAttributeBoolValue("constructible", constructible);
	structure.GetFlags().Set(parStructure::IS_CONSTRUCTIBLE, constructible);

	//////////////////////////////////////////////////////////////////////////
	// Add child members

	structure.SetMaxMembers(structdefXml.FindNumChildren());

	for(parTreeNode::ChildNodeIterator kids = structdefXml.BeginChildren(); kids != structdefXml.EndChildren(); ++kids)
	{
		parMember* member = parStructdefBuilder::CreateMemberFromXml(builder, *(*kids));
		if (member)
		{
			structure.AddMember(member);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Compute sizes and offsets - the technique we use depends on our target platform!

	int padding = 0;

	if (g_sysPlatform == platform::PS3 || g_sysPlatform == platform::PSP2 || g_sysPlatform == platform::ORBIS)
	{
		padding = ComputeSizeAndOffset_GCC(structure, structdefXml);
	}
	else if (g_sysPlatform == platform::WIN32PC || g_sysPlatform == platform::XENON || g_sysPlatform == platform::WIN64PC || g_sysPlatform == platform::DURANGO)
	{
		padding = ComputeSizeAndOffset_MSVC(structure, structdefXml);
	}
	else
	{
		parErrorf("Couldn't determine packing method for platform '%c'", g_sysPlatform);
		padding = ComputeSizeAndOffset_MSVC(structure, structdefXml);
	}

	if (padding > 32)
	{
		parWarningf("Warning: Structure %s has %d bytes of undeclared padding. Can this be reduced?", rawTypeName, padding);
	}

	//////////////////////////////////////////////////////////////////////////
	// Fixup any pointers

	for(int i = 0; i < structure.GetNumMembers(); i++)
	{
		parMember* member = structure.GetMember(i);
		if (member->GetType() == parMemberType::TYPE_ARRAY)
		{
			parMemberArray& memberArr = member->AsArrayRef();
			if (memberArr.GetSubtype() == parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT)
			{
				FastAssert(memberArr.GetData()->m_ExtraAttributes);
				const char* countVarName = memberArr.GetData()->m_ExtraAttributes->FindAttribute("sizeVar")->GetStringValue();

				if (!strncmp(countVarName, "m_", 2))
				{
					countVarName += 2;
				}

				parMember* countVar = structure.FindMember(countVarName, true);
				if (countVar)
				{
					parAssertf(countVar->GetType() >= parMemberType::TYPE_CHAR && countVar->GetType() <= parMemberType::TYPE_UINT,
						"Found a sizeVar=\"%s\" variable, but it's not an integral type", countVarName);

					size_t offset = countVar->GetOffset();
					size_t size = countVar->GetSize();

					memberArr.GetData()->m_CountMemberOffset = (int)offset;
					
					// change types if necessary
					if (size == 1)
					{
						memberArr.GetData()->m_Subtype = parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX;
					}
					else if (size == 2)
					{
						memberArr.GetData()->m_Subtype = parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX;
					}
				}
				else
				{
					parErrorf("Couldn't find the count variable for a pointer-with-count array. Make sure sizeVar=\"%s\" is correct and the size variable itself is described.", countVarName);
				}
			}
		}
	}
}


void AddPadding( parTreeNode* &kid, int &size, parTreeNode &ASSERT_ONLY(xml), parMember* member ) 
{
	// Loop over any <pad> nodes, make sure the next non-pad one we hit is member[i]
	while(kid)
	{
		if (!strcmp(kid->GetElement().GetName(), "pad"))
		{
			int bytes = 0;
			kid->FindValueFromPath("@bytes", bytes);
			size += bytes;
		}
		else
		{
			if (member)
			{
#if __ASSERT
				const char* name = NULL;
				if (!kid->FindValueFromPath("@parName", name))
				{
					kid->FindValueFromPath("@name", name);
					if (!strncmp(name, "m_", 2))
					{
						name += 2;
					}
				}
				bool hasName = (name != NULL);

				const char* typeName = NULL;
				xml.FindValueFromPath("@type", typeName);
				Assertf(hasName && member->GetNameHash() == atLiteralStringHash(name), "While looking for padding in structure %s, found a member var in an unexpected place", typeName);
#endif				
				kid = kid->GetSibling();
			}
			else
			{
				Assertf(0, "Found an unexpected node at the end of the member list: %s", kid->GetElement().GetName());
			}
			return;
		}
		kid = kid->GetSibling();
	}
}


// This is the more straightforward one. Use whatever data for size and alignment we already computed for the base class.
// The derived class members start at sizeof(Base) 
int parStructdefCatalog::ComputeSizeAndOffset_MSVC( parStructure &structure, parTreeNode& xml ) 
{
	int size = 0;
	size_t maxAlign = 0;
	int padding = 0;

	if (structure.GetBaseStructure())
	{
		parStructure* base = structure.GetBaseStructure();

		size = (int)base->GetSize();
		maxAlign = base->FindAlign();
	}
	else
	{
		bool isSimple = false;
		xml.FindValueFromPath("@simple", isSimple);

		if (isSimple)
		{
			// Don't leave space for a vptr
		}
		else
		{
			size = sizeof(char*);
			maxAlign = __alignof(char*);
		}
	}

	parTreeNode* kid = xml.GetChild();

	for(int i = 0; i < structure.GetNumMembers(); i++)
	{
		parMember* member = structure.GetMember(i);

		AddPadding(kid, size, xml, member); // adds _declared_ padding, so no need to increment 'padding'

		size_t align = member->FindAlign();
		maxAlign = Max(align, maxAlign);
		int newSize = (int)AlignPow2(size, align); // bump size up to the next usable offset
		padding += newSize - size;
		size = newSize;
		member->GetCommonData()->m_Offset = size;
		size += (int)member->GetSize();
	}

	AddPadding(kid, size, xml, NULL);

	int newSize = (int)AlignPow2(size, maxAlign);
	padding += newSize - size;
	size = newSize;

	// Last requirement, size must be >= 0
	size = Max(size, 1);

	structure.SetSize(size);

	return padding;
}

// This one is a little trickier, because GCC can use padding at the end of the base class to store derived class data.
// For example if there were 12 bytes of padding at the end of the base class, the first 3 floats in the dervied class would fit 
// there, which is technically within sizeof(Base).
// We also need to distinguish between _declared_ padding (using the <pad> XML node) and _implicit_ padding that could be at the 
// end of the struct. Declared padding could be hiding a member variable, so we want to treat that as used data and don't want the
// dervied class to try to reuse that space. Only implicit padding can be reused.
int parStructdefCatalog::ComputeSizeAndOffset_GCC( parStructure &structure, parTreeNode& xml ) 
{
	int size = 0;
	size_t maxAlign = 0;
	int padding = 0;

	// Find the most-dervied base class that has member variables
	parStructure* base = &structure;
	do 
	{
		base = base->GetBaseStructure();
	} while (base && base->GetNumMembers() == 0);

	if (base)
	{
		bool searchForLastAddr = true;

		// Maybe we've computed the last used address already...
		if (base->GetExtraAttributes())
		{
			int lastAddr = base->GetExtraAttributes()->FindAttributeIntValue("lastDeclaredAddr", (int)base->GetSize());
			if (lastAddr >= 0)
			{
				size = lastAddr;
				searchForLastAddr = false;
			}
		}

		// Start at the end of the base class' last member variable - meaning we can reuse the padding that's at the end of the class
		if (searchForLastAddr)
		{
			for(int i = 0; i < base->GetNumMembers(); i++)
			{
				parMember* member = base->GetMember(i);
				size = Max(size, (int)(member->GetOffset() + member->GetSize()));
			}
		}
		maxAlign = base->FindAlign();
	}
	else
	{
		bool isSimple = false;
		xml.FindValueFromPath("@simple", isSimple);

		if (isSimple)
		{
			// Don't leave space for a vptr
		}
		else
		{
			size = sizeof(char*);
			maxAlign = __alignof(char*);
		}
	}

	parTreeNode* kid = xml.GetChild();

	for(int i = 0; i < structure.GetNumMembers(); i++)
	{
		parMember* member = structure.GetMember(i);

		AddPadding(kid, size, xml, member); // adds _declared_ padding, so no need to increment 'padding'

		size_t align = member->FindAlign();
		maxAlign = Max(align, maxAlign);
		int newSize = (int)AlignPow2(size, align); // bump size up to the next usable offset
		padding += newSize - size;
		size = newSize;
		member->GetCommonData()->m_Offset = size;
		size += (int)member->GetSize();
	}

	AddPadding(kid, size, xml, NULL);

	// Store the last address that was declared to be part of the structure. There may be implicit padding after this point
	// and it's only the _implicit_ padding that we're letting derived classes reuse.
	parAttributeList* extraAttrs = structure.GetOrCreateExtraAttributes();
	extraAttrs->AddAttribute("lastDeclaredAddr", (s64)size, false);

	int newSize = (int)AlignPow2(size, maxAlign);
	padding += newSize - size;
	size = newSize;

	// Last requirement, size must be >= 0
	size = Max(size, 1);

	structure.SetSize(size);

	return padding;
}


parEnumData* parStructdefCatalog::BuildEnumData(parTreeNode& enumdefXml)
{
	parEnumData* enm = rage_new parEnumData;
	*enm = parEnumData::Empty; // Copy the empty data to init fields

	const char* typeName = "";
	enumdefXml.FindValueFromPath("@type", typeName);

	parStructdefBuilder::BuilderInfo builder(*this);
	int fileIdx = -1;
	if (enumdefXml.FindValueFromPath("@" PSC_FILE_INDEX_ATTR, fileIdx))
	{
		builder.m_ConstTable = &m_FileInfos[fileIdx]->m_ConstTable;
	}

	char mangledName[256];
	parMangleTypeName(typeName, mangledName);

	atLiteralHashString hashStr(mangledName);

	enm->m_NameHash = hashStr.GetHash();

	enm->m_NumEnums = (u16)enumdefXml.FindNumChildren();
	enm->m_Names = rage_new const char*[enm->m_NumEnums];
	enm->m_Enums = rage_new parEnumListEntry[enm->m_NumEnums];
	enm->m_Flags |= (1 << parEnumFlags::ENUM_HAS_NAMES);
	enm->m_Flags |= (1 << parEnumFlags::ENUM_ALWAYS_HAS_NAMES);

	int currEnumVal = -1;

	int enumIdx = 0;
	for(parTreeNode::ChildNodeIterator kid = enumdefXml.BeginChildren(); kid != enumdefXml.EndChildren(); ++kid)
	{
		currEnumVal++; // default is to increment value

		parTreeNode& enumValXml = *(*kid);
		const char* enumName = "";
		enumValXml.FindValueFromPath("@name", enumName);

		parLookupIntAttribute(enumValXml, "@value", builder.m_ConstTable, currEnumVal);

		enm->m_Names[enumIdx] = StringDuplicate(enumName);
		enm->m_Enums[enumIdx].m_NameKey = (int)atLiteralStringHash(enumName);
		enm->m_Enums[enumIdx].m_Value = currEnumVal;

		enumIdx++;
	}

	return enm;
}

#endif // PARSER_USING_EXTERNAL_STRUCTURE_DEFNS
