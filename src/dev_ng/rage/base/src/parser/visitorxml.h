// 
// parser/visitorxml.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORXML_H
#define PARSER_VISITORXML_H

#include "visitor.h"

#include "file/stream.h"
#include "parsercore/settings.h"

namespace rage
{

// PURPOSE: A visitor that writes XML text representing a parsable object to a stream
class parXmlWriterVisitor : public parInstanceVisitor
{
public:
	parXmlWriterVisitor(fiStream* stream, parSettings* settings = NULL);

	virtual ~parXmlWriterVisitor() {}

	void OverrideToplevelName(char* newToplevelName) {m_OverrideName = newToplevelName;}

	virtual void SimpleMember(parPtrToMember dataPtr, parMemberSimple& metadata);

	virtual void Vector2Member(Vector2& data, parMemberVector& metadata);
	virtual void Vector3Member(Vector3& data, parMemberVector& metadata);
	virtual void Vector4Member(Vector4& data, parMemberVector& metadata);
	virtual void VecBoolVMember(VecBoolV& data, parMemberVector& metadata);
	virtual void Vec2VMember(Vec2V& data, parMemberVector& metadata);
	virtual void Vec3VMember(Vec3V& data, parMemberVector& metadata);
	virtual void Vec4VMember(Vec4V& data, parMemberVector& metadata);

	virtual void Matrix34Member(Matrix34& data, parMemberMatrix& metadata);
	virtual void Matrix44Member(Matrix44& data, parMemberMatrix& metadata);
	virtual void Mat33VMember(Mat33V& data, parMemberMatrix& metadata);
	virtual void Mat34VMember(Mat34V& data, parMemberMatrix& metadata);
	virtual void Mat44VMember(Mat44V& data, parMemberMatrix& metadata);

	virtual void EnumMember(int& data, parMemberEnum& metadata);
	virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);

	virtual void StringMember(parPtrToMember ptrToMember, const char* ptrToString, parMemberString& metadata);

	virtual void WideStringMember(parPtrToMember ptrToMember, const char16* ptrToString, parMemberString& metadata);

	virtual void ExternalPointerMember(void*& data, parMemberStruct& metadata);

	virtual bool BeginStructMember(parPtrToStructure structAddr, parMemberStruct& metadata);
	virtual void EndStructMember(parPtrToStructure structAddr, parMemberStruct& metadata);

	virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);
	virtual void EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);

	virtual bool BeginToplevelStruct(parPtrToStructure structAddr, parStructure& metadata);
	virtual void EndToplevelStruct(parPtrToStructure structAddr, parStructure& metadata);

	virtual bool BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);
	virtual void EndArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

    virtual bool BeginMapMember	(parPtrToStructure structAddr,	parMemberMap& metadata);
    virtual void EndMapMember (parPtrToStructure structAddr, parMemberMap& metadata);
    virtual void VisitMapMember(parPtrToStructure structAddr, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata);

protected:
	void Indent();
	int m_Indent;
	fiStream* m_Stream;
	parSettings m_Settings;
	char* m_OverrideName;
    char m_KeyValue[128];
private:
    void addKeyValue();
};

inline void parXmlWriterVisitor::Indent()
{
	for(int i = 0; i < m_Indent; i++) {
		m_Stream->FastPutCh(' ');
		m_Stream->FastPutCh(' ');
	}
}

}

#endif
