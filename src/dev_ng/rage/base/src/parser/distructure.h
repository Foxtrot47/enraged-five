// 
// parser/distructure.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_DISTRUCTURE_H
#define PARSER_DISTRUCTURE_H

#include "structure.h"

namespace rage {

// PURPOSE:
//		parDIStructure is a subclass of parStructure that uses data inheritance.
//		This means that each structure that gets loaded in via a parDIStructure
//		object can have a "parent" attribute that specifies another file to load
//		in first.
class parDIStructure : public parStructure
{
public:
	parDIStructure() {}
	virtual ~parDIStructure() {}

	virtual void ReadMembersFromTree(parTreeNode* node, parPtrToStructure structAddr);

	void CreatePointerMembers(parTreeNode* node, parPtrToStructure structAddr);
};

} // namespace rage

#endif
