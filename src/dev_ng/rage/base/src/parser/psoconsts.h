// 
// parser/psoconsts.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOCONSTS_H
#define PARSER_PSOCONSTS_H

#include "memberdata.h"

#include "atl/hashstring.h"
#include "system/magicnumber.h"

namespace rage
{

// Make a big-endian magic number (it looks a little nicer in a hex dump of the file)
#define MAKE_MAGIC_NUMBER_BE(a,b,c,d)		(((a) << 24) | ((b) << 16) | ((c) << 8) | ((d)))

namespace psoConstants { 
	const u32 MAX_RESERVED_HASHES = 4096; // These are reserved for internal use. No hashes below this will be allowed.

	// ID "namespace" map:
	// 0x00000000 - 0x000000FF		psoType, for POD structarrays
	// 0x00000100 - 0x00000CFF		Anonymous types, ones that are only meaningful within one PSO file
	// 0x00000D00 - 0x00000DFF		Namespaced hash strings
	// 0x00000E00 - 0x00000FFF		Unassigned, reserved for future use
	// 0x00001000 - 0xFFFFFFFF		Literal hash of a type name

	const u32 MAX_PARMEMBER_TYPE = 255;
	const u32 MAX_PSO_TYPE = 255;
	const u32 FIRST_ANONYMOUS_ID = 256;
	const u32 NUM_ANONYMOUS_IDS = 3072;
	const u32 FIRST_HASHSTRING_ID = FIRST_ANONYMOUS_ID + NUM_ANONYMOUS_IDS; // 3328
	const u32 NUM_HASHSTRING_IDS = 256;
	const u32 FIRST_UNASSIGNED_ID = FIRST_HASHSTRING_ID + NUM_HASHSTRING_IDS; // 3584

	// Special extensions to the parMember type enum
	const u32 TYPE_ATSTRING_IN_ARRAY = 200;
	const u32 TYPE_ATWIDESTRING_IN_ARRAY = 201;

	const u32 OBJECT_IDENTIFIER_STRUCTARRAYTABLE_INDEX_BITS = 12;	// The number of bits in an objectId that represents the structarray index
	const u32 OBJECT_IDENTIFIER_ARRAY_OFFSET_BITS = 20;		// The number of bits in an objectId that represents the offset (in bytes) into the array that this object starts at
	

	enum SchemaType {
		ST_STRUCTURE,
		ST_ENUM
	};

	const u32 PSCH_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('P', 'S', 'C', 'H'); // PSO Schemas
	const u32 PSIN_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('P', 'S', 'I', 'N'); // PSO Serialized Instances
	const u32 PMAP_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('P', 'M', 'A', 'P'); // PSO Map section
	const u32 PSIG_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('P', 'S', 'I', 'G'); // Parser signatures
	const u32 STRS_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('S', 'T', 'R', 'S'); // Debug strings
	const u32 STRE_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('S', 'T', 'R', 'E'); // Debug strings (encryped)
	const u32 STRF_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('S', 'T', 'R', 'F'); // Final strings
	const u32 CHKS_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('C', 'H', 'K', 'S'); // Checksum

	const u32 PRD0_MAGIC_NUMBER = MAKE_MAGIC_NUMBER_BE('P', 'R', 'D', '0');

	// Hash values could be reserved or not. If its reserved it's either a ParMemberType or an Anonymous ID.
	inline bool IsReserved(u32 hash) {return hash < MAX_RESERVED_HASHES;}
	inline bool IsReserved(atLiteralHashValue hash) {return IsReserved(hash.GetHash()); }

	inline bool IsAnonymous(u32 hash) { return hash >= FIRST_ANONYMOUS_ID && hash < (FIRST_ANONYMOUS_ID + NUM_ANONYMOUS_IDS); }
	inline bool IsAnonymous(atLiteralHashValue hash) {return IsAnonymous(hash.GetHash()); }

	inline bool IsParMemberType(u32 hash) {return hash <= MAX_PARMEMBER_TYPE;}
	inline bool IsParMemberType(atLiteralHashValue hash) {return IsParMemberType(hash.GetHash()); }

	inline bool IsHashstringId(u32 hash) { return hash >= FIRST_HASHSTRING_ID && hash < (FIRST_HASHSTRING_ID + NUM_HASHSTRING_IDS); }
	inline bool IsHashstringId(atLiteralHashValue hash) { return IsHashstringId(hash.GetHash()); }

	inline bool IsPsoType(u32 hash) {return hash <= MAX_PSO_TYPE;}
	inline bool IsPsoType(atLiteralHashValue hash) {return IsPsoType(hash.GetHash()); }
}

}

#endif // PARSER_PSOCONSTS_H
