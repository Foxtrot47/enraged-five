// 
// parser/psobuilder.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psobuilder.h"

#include "manager.h"
#include "memberdata.h"
#include "optimisations.h"
#include "psobyteswap.h"
#include "psoconsts.h"
#include "psodata.h"
#include "psofile.h"
#include "psoresource.h"
#include "psoschema.h"

#include "atl/array.h"
#include "atl/map.h"
#include "data/aes.h"
#include "diag/output.h"
#include "file/asset.h"
#include "math/simplemath.h"
#include "system/endian.h"
#include "system/memops.h"

PARSER_OPTIMISATIONS();

using namespace rage;

// PURPOSE: Reallocates a data pointer - used to add extra memory at the end of one of hte objects under construction to put them
// into PSO format
template<typename _Type> void ReallocateData(_Type*& ptrToStorage, size_t oldSize, size_t newSize)
{
	FastAssert(newSize >= oldSize);
	char* newStorage = rage_aligned_new( __alignof(_Type) ) char [ newSize ];
	if (ptrToStorage)
	{
		sysMemCpy(newStorage, ptrToStorage, oldSize);
		delete [] reinterpret_cast<char*>(ptrToStorage);
	}
	ptrToStorage = reinterpret_cast<_Type*>(newStorage);
}

psoBuilderStructSchema::psoBuilderStructSchema(psoBuilder* builder)
: m_Builder(builder)
, m_UnderConstruction(true)
, m_AutoPack(false)
, m_Structure(NULL)
, m_Alignment(1)
{
	ReallocateData(m_Structure, 0, sizeof(psoSchemaStructureData));
	sysMemSet(m_Structure, 0x0, sizeof(psoSchemaStructureData));
	m_Structure->m_Type = psoConstants::ST_STRUCTURE;
}

psoBuilderStructSchema::~psoBuilderStructSchema()
{
	delete [] reinterpret_cast<char*>(m_Structure);
}

void psoBuilderStructSchema::ComputeOffsetAndUpdateSize(psoSchemaMemberData& mem, ptrdiff_t offset)
{
	if (psoConstants::IsReserved(mem.m_NameHash))
	{
		return;
	}

	FastAssert((m_AutoPack && offset < 0) ||
		(!m_AutoPack && offset >= 0)); // If you choose to use autopack, ALL members have to be autopacked.

	size_t size = 0;
	size_t align = 0;
	FindMemberTraits(mem, size, align);

	if (m_AutoPack)
	{
		// Round up to alignment
		offset = AlignPow2(m_Structure->m_Size, align);
		mem.SetOffset((u32)offset);
		m_Structure->m_Size = ptrdiff_t_to_int(offset + size);
	}

	m_Alignment = Max(m_Alignment, (u16)align);

	FastAssert(offset >= 0 && 
		offset < psoSchemaMemberData::MAX_OFFSET && 
		((u32)offset < m_Structure->m_Size || (u32)offset == m_Structure->m_Size && size == 0));  // special case for 0-byte structures at the end of a class

	return;
}

ptrdiff_t psoBuilderStructSchema::AddPadding(size_t pad, size_t align)
{
	FastAssert(m_AutoPack);

	ptrdiff_t offset = AlignPow2(m_Structure->m_Size, align);
	m_Structure->m_Size += ptrdiff_t_to_int(offset + pad);

	return offset;
}

void psoBuilderStructSchema::FindMemberTraits(psoSchemaMemberData& mem, size_t& size, size_t& align)
{
	size = parMemberType::GetSize((parMemberType::Enum)mem.m_Type);
	align = parMemberType::GetAlign((parMemberType::Enum)mem.m_Type);

	if (size != 0)
	{
		return;
	}

	switch(mem.m_Type)
	{
	case parMemberType::TYPE_ARRAY:
		switch((parMemberArraySubType::Enum)mem.GetSubType())
		{
		case parMemberArraySubType::SUBTYPE_ATARRAY:
			size = sizeof(psoFakeAtArray16);
			align = __alignof(psoFakeAtArray16);
			return;
		case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
			{
				size_t eltSize;
				size_t eltAlign;
				FindMemberTraits(m_Members[mem.GetMemberIndex()], eltSize, eltAlign);
				size = RoundUp<sizeof(int)>(eltSize * mem.m_Count) + sizeof(int); // round up to align the m_Count variable
				align = Max(eltAlign, __alignof(int));
			}
			return;
		case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
		case parMemberArraySubType::SUBTYPE_MEMBER:
			{
				FindMemberTraits(m_Members[mem.GetMemberIndex()], size, align);
				size *= mem.m_Count;
			}
			return;
		case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
			size = sizeof(psoFakeAtArray32);
			align = __alignof(psoFakeAtArray32);
			return;
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		case parMemberArraySubType::SUBTYPE_POINTER:
			size = sizeof(char*);
			align = __alignof(char*);
			return;
		case parMemberArraySubType::SUBTYPE_VIRTUAL:	// Not actually valid for a PSO, but I can still find a reasonable size/alignment
			size = 0;
			align = 0;
			return;
		}
	case parMemberType::TYPE_MAP:
		switch((parMemberMapSubType::Enum)mem.GetSubType())
		{
		case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
			size = sizeof(psoFakeAtBinMap);
			align = __alignof(psoFakeAtBinMap);
			return;
		case parMemberMapSubType::SUBTYPE_ATMAP:
			parAssertf(0, "Can't save atMaps into a PSO file yet (can you use an atBinaryMap?)");
			size = 0;
			align = 0;
			return;
		}
	case parMemberType::TYPE_BITSET:
		switch((parMemberBitsetSubType::Enum)mem.GetSubType())
		{
		case parMemberBitsetSubType::SUBTYPE_8BIT_FIXED:
			size = (RoundUp<8>(mem.m_Count) / 8) * sizeof(u8);
			align = __alignof(u8);
			return;
		case parMemberBitsetSubType::SUBTYPE_16BIT_FIXED:
			size = (RoundUp<16>(mem.m_Count) / 16) * sizeof(u16);
			align = __alignof(u16);
			return;
		case parMemberBitsetSubType::SUBTYPE_32BIT_FIXED:
			size = (RoundUp<32>(mem.m_Count) / 32) * sizeof(u32);
			align = __alignof(u32);
			return;
		case parMemberBitsetSubType::SUBTYPE_ATBITSET:
			size = sizeof(psoFakeAtBitset);
			align = __alignof(psoFakeAtBitset);
			return;
		}
	case parMemberType::TYPE_ENUM:
		switch((parMemberEnumSubType::Enum)mem.GetSubType())
		{
		case parMemberEnumSubType::SUBTYPE_8BIT:
			size = sizeof(char);
			align = __alignof(char);
			return;
		case parMemberEnumSubType::SUBTYPE_16BIT:
			size = sizeof(s16);
			align = __alignof(s16);
			return;
		case parMemberEnumSubType::SUBTYPE_32BIT:
			size = sizeof(s32);
			align = __alignof(s32);
			return;
		}
	case parMemberType::TYPE_STRING:
		switch((parMemberStringSubType::Enum)mem.GetSubType())
		{
		case parMemberStringSubType::SUBTYPE_MEMBER:
			size = sizeof(char) * mem.m_Count;
			align = __alignof(char);
			return;
		case parMemberStringSubType::SUBTYPE_POINTER:
		case parMemberStringSubType::SUBTYPE_CONST_STRING:
		case parMemberStringSubType::SUBTYPE_WIDE_POINTER:
			size = sizeof(char*);
			align = __alignof(char*);
			return;
		case parMemberStringSubType::SUBTYPE_ATSTRING:
		case parMemberStringSubType::SUBTYPE_ATWIDESTRING:
			size = sizeof(psoFakeAtString);
			align = __alignof(psoFakeAtString);
			return;
		case parMemberStringSubType::SUBTYPE_WIDE_MEMBER:
			size = sizeof(char16) * mem.m_Count;
			align = __alignof(char16);
			return;
		case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
		case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
		case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
		case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
		case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
		case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
			size = sizeof(u32);
			align = __alignof(u32);
			return;
		}
	case parMemberType::TYPE_STRUCT:
		switch((parMemberStructSubType::Enum)mem.GetSubType())
		{
		case parMemberStructSubType::SUBTYPE_STRUCTURE:
			{
				psoBuilderStructSchema* referentSchema = m_Builder->FindStructSchema(atLiteralHashValue(mem.m_ReferentHash));
				parAssertf(referentSchema, "Couldn't find referent schema - make sure you add the referent before the referrer");
				size = referentSchema->m_Structure->m_Size;
				align = referentSchema->m_Alignment;
			}
			return;
		case parMemberStructSubType::SUBTYPE_POINTER:
		case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
			size = sizeof(char*);
			align = __alignof(char*);
			return;

		case parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER:				// external named pointers and link pointers aren't parsable
		case parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:		// but we can still return reasonable size/align for them
			size = sizeof(char*);
			align = __alignof(char*);
			return;
		}
	}

	parAssertf(0, "Shouldn't get here");
	size = 0;
	align = 0;
}


int psoBuilderStructSchema::AddMemberRawMemIdxCount(atLiteralHashValue namehash, parMemberType::Enum type, int subtype, ptrdiff_t offset, u16 memIdx, u16 count, u32 alignPower)
{
	FastAssert((subtype & (~0xFF)) == 0); // Make sure it's between 0 and 0xFF (all other bits are 0)

	psoSchemaMemberData& mem = m_Members.Grow();
	mem.m_NameHash = namehash.GetHash();
	mem.m_Type = (u8)type;
	mem.SetSubType((u8)subtype);
	mem.SetOffset((u32)Max(offset, ptrdiff_t(0)));
	mem.m_AlignmentAndMemberIndex = 0;
	mem.SetMemberIndex(memIdx);
	mem.SetAlignmentPower(alignPower);
	mem.m_Count = count;

	ComputeOffsetAndUpdateSize(mem, offset);

	return m_Members.GetCount()-1;
}

int psoBuilderStructSchema::AddMemberRawKeyValueMemIndices(atLiteralHashValue namehash, parMemberType::Enum type, int subtype, ptrdiff_t offset, u16 keyMemIdx, u16 valueMemIdx)
{
	FastAssert((subtype & (~0xFF)) == 0); // Make sure it's between 0 and 0xFF (all other bits are 0)

	psoSchemaMemberData& mem = m_Members.Grow();
	mem.m_NameHash = namehash.GetHash();
	mem.m_Type = (u8)type;
	mem.SetSubType((u8)subtype);
	mem.SetOffset((u32)Max(offset, ptrdiff_t(0)));
	mem.m_KeyIndex = keyMemIdx;
	mem.m_ValueIndex = valueMemIdx;

	ComputeOffsetAndUpdateSize(mem, offset);

	return m_Members.GetCount()-1;
}

int psoBuilderStructSchema::AddMemberRawRefHash(atLiteralHashValue namehash, parMemberType::Enum type, int subtype, ptrdiff_t offset, atLiteralHashValue refHash)
{
	FastAssert((subtype & (~0xFF)) == 0); // Make sure it's between 0 and 0xFF (all other bits are 0)

	psoSchemaMemberData& mem = m_Members.Grow();
	mem.m_NameHash = namehash.GetHash();
	mem.m_Type = (u8)type;
	mem.SetSubType((u8)subtype);
	mem.SetOffset((u32)Max(offset, ptrdiff_t(0)));
	mem.m_ReferentHash = refHash.GetHash();

	ComputeOffsetAndUpdateSize(mem, offset);

	return m_Members.GetCount()-1;
}

int psoBuilderStructSchema::AddMemberSimple(atLiteralHashValue namehash, parMemberType::Enum type, u8 subtype, ptrdiff_t offset /* = -1 */)
{
	using namespace parMemberType;

	return AddMemberRawRefHash(namehash, type, subtype, offset, atLiteralHashValue::Null());
}

int psoBuilderStructSchema::AddMemberString(atLiteralHashValue namehash, parMemberStringSubType::Enum subtype, u16 fixedSizeCount, ptrdiff_t offset, u8 namespaceIndex)
{
	return AddMemberRawMemIdxCount(namehash, parMemberType::TYPE_STRING, subtype, offset, namespaceIndex, fixedSizeCount, 0);
}

int psoBuilderStructSchema::AddMemberEnum(atLiteralHashValue namehash, parMemberEnumSubType::Enum subtype, atLiteralHashValue enumSchemaNamehash, ptrdiff_t offset /* = -1 */)
{
	return AddMemberRawRefHash(namehash, parMemberType::TYPE_ENUM, subtype, offset, enumSchemaNamehash);
}	

int psoBuilderStructSchema::AddMemberStruct(atLiteralHashValue namehash, psoBuilderStructSchema& memberSchema, ptrdiff_t offset /* = -1 */)
{
	return AddMemberRawRefHash(namehash, parMemberType::TYPE_STRUCT, parMemberStructSubType::SUBTYPE_STRUCTURE, offset, memberSchema.m_NameHash);
}

int psoBuilderStructSchema::AddMemberStruct(atLiteralHashValue namehash, atLiteralHashValue nameOfTheStruct, ptrdiff_t offset /* = -1 */)
{
	return AddMemberRawRefHash(namehash, parMemberType::TYPE_STRUCT, parMemberStructSubType::SUBTYPE_STRUCTURE, offset, nameOfTheStruct);
}

int psoBuilderStructSchema::AddMemberPointer(atLiteralHashValue namehash, parMemberStructSubType::Enum subtype, ptrdiff_t offset /* = -1 */ )
{
	return AddMemberRawRefHash(namehash, parMemberType::TYPE_STRUCT, subtype, offset, atLiteralHashValue::Null());
}

int psoBuilderStructSchema::AddMemberArray(atLiteralHashValue namehash, parMemberArraySubType::Enum subtype, int indexOfArrayContentMember, u16 fixedSizeCount, ptrdiff_t offset /* = -1 */, u32 alignmentPower /* = 0 */)
{
	FastAssert(indexOfArrayContentMember >= 0 && indexOfArrayContentMember < m_Members.GetCount());
	FastAssert(psoConstants::IsAnonymous(m_Members[indexOfArrayContentMember].m_NameHash));

	return AddMemberRawMemIdxCount(namehash, parMemberType::TYPE_ARRAY, subtype, offset, (u16)indexOfArrayContentMember, fixedSizeCount, alignmentPower);
}

int psoBuilderStructSchema::AddMemberBitset(atLiteralHashValue namehash, parMemberBitsetSubType::Enum subtype, int indexOfBitsetEnumMember, u16 numBits, ptrdiff_t offset /* = -1 */)
{
	FastAssert(indexOfBitsetEnumMember == psoSchemaMemberData::NO_MEMBER_INDEX || (indexOfBitsetEnumMember >= 0 && indexOfBitsetEnumMember < m_Members.GetCount()));
	FastAssert(indexOfBitsetEnumMember == psoSchemaMemberData::NO_MEMBER_INDEX || psoConstants::IsAnonymous(m_Members[indexOfBitsetEnumMember].m_NameHash));
	FastAssert(indexOfBitsetEnumMember == psoSchemaMemberData::NO_MEMBER_INDEX || m_Members[indexOfBitsetEnumMember].m_Type == parMemberType::TYPE_ENUM);

	return AddMemberRawMemIdxCount(namehash, parMemberType::TYPE_BITSET, subtype, offset, (u16)indexOfBitsetEnumMember, numBits, 0);
}

int psoBuilderStructSchema::AddMemberMap(atLiteralHashValue namehash, parMemberMapSubType::Enum subtype, int indexOfKeyMember, int indexOfValueMember, ptrdiff_t offset /* = -1 */)
{
	FastAssert(indexOfKeyMember >= 0 && indexOfKeyMember < m_Members.GetCount());
	FastAssert(psoConstants::IsAnonymous(m_Members[indexOfKeyMember].m_NameHash));
	FastAssert(indexOfValueMember >= 0 && indexOfValueMember < m_Members.GetCount());
	FastAssert(psoConstants::IsAnonymous(m_Members[indexOfValueMember].m_NameHash));

	return AddMemberRawKeyValueMemIndices(namehash, parMemberType::TYPE_MAP, subtype, offset, (u16)indexOfKeyMember, (u16)indexOfValueMember);
}

int psoBuilderStructSchema::CopyMember(psoSchemaMemberData& member, atLiteralHashValue newname /* = atLiteralHashValue::Null */, ptrdiff_t offset /* = -1 */)
{
	psoSchemaMemberData& mem = m_Members.Grow();
	mem.m_NameHash = newname.GetHash();
	mem.m_Type = member.m_Type;
	mem.SetSubType(member.GetSubType());
	mem.SetOffset(member.GetOffset());
	mem.m_ReferentHash = member.m_ReferentHash;

	ComputeOffsetAndUpdateSize(mem, offset);

	return m_Members.GetCount()-1;
}

void psoBuilderStructSchema::FinishBuilding()
{
	FastAssert(m_UnderConstruction);
	m_UnderConstruction = false;

	// This would be the time to reorganize the members if necessary

	// Make sure the size we have is a multiple of alignment, boost the size if not
	if (m_AutoPack)
	{
		m_Structure->m_Size = (u32)AlignPow2(m_Structure->m_Size, m_Alignment);
	}
	else
	{
		parAssertAligned(m_Structure->m_Size, m_Alignment, "size of PSO schema structure");
	}

	// Reallocate m_Structure with enough storage following it to hold all the member variables
	size_t memberDataSize = sizeof(psoSchemaMemberData) * m_Members.GetCount();
	ReallocateData(m_Structure, sizeof(psoSchemaStructureData), sizeof(psoSchemaStructureData) + memberDataSize);
	sysMemCpy(m_Structure + 1, m_Members.GetElements(), memberDataSize);

	m_Structure->m_NumMembers = (u16)m_Members.GetCount();

	m_Members.Reset(); // Don't need this array anymore
}


u32 psoBuilderStructSchema::ComputeFileLocation(u32 prevOffset) {
	FastAssert(!m_UnderConstruction);
	parAssertAligned(prevOffset, 4, "psoSchemaStructureData");
	m_Location.m_Offset = prevOffset;
	m_Location.m_Size = sizeof(psoSchemaStructureData) + m_Structure->m_NumMembers * sizeof(psoSchemaMemberData);
	return m_Location.m_Size;
}

u32 psoBuilderStructSchema::ComputeSize( ) {
	return sizeof(psoSchemaStructureData) + m_Structure->m_NumMembers * sizeof(psoSchemaMemberData);;
}

void psoBuilderStructSchema::Serialize(char* dest) {
	FastAssert(!m_UnderConstruction);
	
	// Now that it's no longer under constriction we have the schema data plus the member data all packed nicely together
	sysMemCpy(dest + m_Location.m_Offset, m_Structure, m_Location.m_Size);
}




psoBuilderEnumSchema::psoBuilderEnumSchema() 
: m_Enum(NULL)
, m_UnderConstruction(true)
{
	ReallocateData(m_Enum, 0, sizeof(psoSchemaEnumData));
	m_Enum->m_Type = psoConstants::ST_ENUM;
	m_Enum->m_Flags = 0;
	m_Enum->m_NumEnums = 0;
}

psoBuilderEnumSchema::~psoBuilderEnumSchema()
{
	delete [] reinterpret_cast<char*>(m_Enum);
}

void psoBuilderEnumSchema::AddValue(atLiteralHashValue namehash, int value)
{
	psoEnumTableEntry entry;
	entry.m_NameHash = namehash.GetHash();
	entry.m_Value = value;

	m_Values.PushAndGrow(entry);
}

void psoBuilderEnumSchema::FinishBuilding()
{
	FastAssert(m_UnderConstruction);
	m_UnderConstruction = false;

	size_t valuesSize = sizeof(psoEnumTableEntry) * m_Values.GetCount();
	ReallocateData(m_Enum, sizeof(psoSchemaEnumData), sizeof(psoSchemaEnumData) + valuesSize);
	sysMemCpy(m_Enum + 1, m_Values.GetElements(), valuesSize);

	m_Enum->m_NumEnums = (u16)m_Values.GetCount();

	m_Values.Reset();
}

u32 psoBuilderEnumSchema::ComputeFileLocation(u32 prevOffset) { 
	FastAssert(!m_UnderConstruction);
	parAssertAligned(prevOffset, 4, "psoSchemaEnumData");
	m_Location.m_Offset = prevOffset;
	m_Location.m_Size = sizeof(psoSchemaEnumData) + sizeof(psoEnumTableEntry) * m_Enum->m_NumEnums; 
	return m_Location.m_Size;
}

u32 psoBuilderEnumSchema::ComputeSize() { 
	return sizeof(psoSchemaEnumData) + sizeof(psoEnumTableEntry) * m_Enum->m_NumEnums;;
}

void psoBuilderEnumSchema::Serialize(char* dest) {
	FastAssert(!m_UnderConstruction);

	// Now that it's no longer under constriction we have the schema data plus the value data all packed nicely together
	sysMemCpy(dest + m_Location.m_Offset, m_Enum, m_Location.m_Size);
}

psoBuilderSchemaCatalog::~psoBuilderSchemaCatalog()
{
	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		delete m_StructSchemas[i];
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		delete m_EnumSchemas[i];
	}
}


// Note this doesn't take an offset parameter because all offsets for all child objects are
// supposed to be relative to this object (which also means relative to this IFF chunk)
void psoBuilderSchemaCatalog::ComputeFileLocations() 
{
	u32 offset = 0;
	u32 headerSize = sizeof(psoSchemaCatalogData);
	u32 typeTableSize = sizeof(psoTypeTableData) * (m_StructSchemas.GetCount() + m_EnumSchemas.GetCount());
	offset += headerSize + typeTableSize;

	// rsTODO: If we ever sort these by ID, make sure that we combine the struct schemas and the enum schemas into a single list.

	u32 childSizes = 0;
	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		u32 newSize = m_StructSchemas[i]->ComputeFileLocation(offset);
		offset += newSize;
		childSizes += newSize;
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		u32 newSize = m_EnumSchemas[i]->ComputeFileLocation(offset);
		offset += newSize;
		childSizes += newSize;
	}
	
	m_Location.m_Size = RoundUp<4>(headerSize + typeTableSize + childSizes);

	// The PSIG section will immediately follow the PSCH section
	m_SignatureLocation.m_Offset = m_Location.m_Size;
	m_SignatureLocation.m_Size = sizeof(parIffHeader) + sizeof(u32)*(m_StructSchemas.GetCount() + m_EnumSchemas.GetCount());
}

u32 psoBuilderSchemaCatalog::ComputeSize()
{
	u32 offset = 0;
	u32 headerSize = sizeof(psoSchemaCatalogData);
	u32 typeTableSize = sizeof(psoTypeTableData) * (m_StructSchemas.GetCount() + m_EnumSchemas.GetCount());
	offset += headerSize + typeTableSize;

	u32 childSizes = 0;
	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		u32 newSize = m_StructSchemas[i]->ComputeSize();
		offset += newSize;
		childSizes += newSize;
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		u32 newSize = m_EnumSchemas[i]->ComputeSize();
		offset += newSize;
		childSizes += newSize;
	}

	return ((RoundUp<4>(headerSize + typeTableSize + childSizes))
			+ sizeof(parIffHeader) 
			+ sizeof(u32)*(m_StructSchemas.GetCount() + m_EnumSchemas.GetCount()));
}

size_t psoBuilderSchemaCatalog::Serialize(psoBuilder& /*builder*/, char* dest, psoSchemaCatalogData*& outSchemaCat, parIffHeader*& outSignatures)
{
	psoSchemaCatalogData* catBuffer = reinterpret_cast<psoSchemaCatalogData*>(dest);

	catBuffer->m_IffTag.SetMagic( psoConstants::PSCH_MAGIC_NUMBER); 
	catBuffer->m_IffTag.SetSize(m_Location.m_Size);

	catBuffer->m_NumTypes = m_StructSchemas.GetCount() + m_EnumSchemas.GetCount();

	// build up the type table now
	atArray<psoTypeTableData> typeTable;
	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		psoTypeTableData entry;
		entry.m_TypeHash = m_StructSchemas[i]->m_NameHash.GetHash();
		entry.m_Offset = m_StructSchemas[i]->m_Location.m_Offset;
		typeTable.PushAndGrow(entry);
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		psoTypeTableData entry;
		entry.m_TypeHash = m_EnumSchemas[i]->m_NameHash.GetHash();
		entry.m_Offset = m_EnumSchemas[i]->m_Location.m_Offset;
		typeTable.PushAndGrow(entry);
	}

	// rsTODO: Sort the type table here?
	int totalTypes = m_EnumSchemas.GetCount() + m_StructSchemas.GetCount();

	sysMemCpy(catBuffer+1, typeTable.GetElements(), sizeof(psoTypeTableData) * totalTypes	);

	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		m_StructSchemas[i]->Serialize(dest);
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		m_EnumSchemas[i]->Serialize(dest);
	}

	parIffHeader* psigIff = reinterpret_cast<parIffHeader*>(dest + m_SignatureLocation.m_Offset);
	psigIff->SetMagic(psoConstants::PSIG_MAGIC_NUMBER);
	psigIff->SetSize(m_SignatureLocation.m_Size);

	u32* signaturePtr = reinterpret_cast<u32*>(psigIff + 1); // signatures start right after the header, we'll fill them in later
	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		*signaturePtr = 0;
		signaturePtr++;
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		*signaturePtr = 0;
		signaturePtr++;
	}

	outSchemaCat = catBuffer;
	outSignatures = psigIff;

	return m_Location.m_Size + m_SignatureLocation.m_Size;
}

psoBuilderInstance::psoBuilderInstance() 
	: m_Data(NULL)
	, m_StructSchema(NULL)
	, m_Count(0)
	, m_PodType(parMemberType::INVALID_TYPE)
	, m_OwnsData(false)
{
	m_Id = psoStructId::Null();
}

psoBuilderInstance::~psoBuilderInstance()
{
	if (m_OwnsData)
	{
		delete [] m_Data;
	}
}

u32 psoBuilderInstance::ComputeSize()
{
	u32 resultSize = 0;

	if (m_PodType == parMemberType::INVALID_TYPE)
	{
		resultSize = m_StructSchema->m_Structure->m_Size * m_Count;
	}
	else
	{
		resultSize = m_Location.m_Size;
	}

	return resultSize;
}

u32 psoBuilderInstance::ComputeFileLocation(u32 prevOffset)
{
	if (m_PodType == parMemberType::INVALID_TYPE)
	{
		parAssertAligned(prevOffset, m_StructSchema->m_Alignment, atLiteralHashString::TryGetString(m_StructSchema->m_NameHash.GetHash()));
		m_Location.m_Offset = prevOffset;
		m_Location.m_Size = (int) m_StructSchema->m_Structure->m_Size * m_Count;
	}
	else
	{
#if __ASSERT
		u32 alignment = 0;
		switch(m_PodType)
		{
		case parMemberType::TYPE_STRUCT:
		case psoConstants::TYPE_ATSTRING_IN_ARRAY:
		case psoConstants::TYPE_ATWIDESTRING_IN_ARRAY:
			alignment = __alignof(char*);
			break;
		default:
			alignment = (u32)parMemberType::GetAlign((parMemberType::Enum)m_PodType);
			break;
		}
#endif
		parAssertAligned(prevOffset, alignment, "POD array");
		m_Location.m_Offset = prevOffset;
		parAssertf(m_Location.m_Size, "Size must be specified in advance for POD instances");
	}
	return m_Location.m_Size;
}

void psoBuilderInstance::Serialize(char* dest)
{
	sysMemCpy(dest + m_Location.m_Offset, m_Data, m_Location.m_Size);

	for(int i = 0; i < m_IdFixups.GetCount(); i++)
	{
		// find the new address of the offset variable
		ptrdiff_t offsetOfStructIdVar = reinterpret_cast<size_t>(m_IdFixups[i].m_AddressOfStructId) - reinterpret_cast<size_t>(m_Data);
		parAssertf(offsetOfStructIdVar >= 0 && offsetOfStructIdVar < (ptrdiff_t)m_Location.m_Size, "The offset variable is not within this set of instances!");
		psoFakePtr* addrOfPsoStructId = reinterpret_cast<psoFakePtr*>(dest + m_Location.m_Offset + offsetOfStructIdVar);

		if (m_IdFixups[i].m_ReferentData)
		{
			addrOfPsoStructId->m_StructId = m_IdFixups[i].m_ReferentData->m_Id;
		}
		else
		{
			addrOfPsoStructId->m_StructId = psoStructId::Null();
		}
	}
}

// PURPOSE: Records a fixup for later use
void psoBuilderInstance::AddFixupRaw(parPtrToStructure& offsetVar, psoBuilderInstance* referentData)
{
	IdFixupInfo fixup;
	fixup.m_AddressOfStructId = reinterpret_cast<psoFakePtr*>(&offsetVar);
	fixup.m_ReferentData = referentData;
	m_IdFixups.PushAndGrow(fixup);
}

void psoBuilderInstance::AddFixup(int index, const char* memberName, psoBuilderInstance* targetOfThePointer)
{
	psoBuilderStructSchema* schema = GetSchema();
	psoSchemaStructureData* schemaData = schema->GetSchemaData();

	atLiteralHashValue nameHash(memberName);

	for(int i = 0; i < schemaData->m_NumMembers; i++)
	{
		psoSchemaMemberData& member = schemaData->GetMember(i);
		if (member.m_NameHash == nameHash.GetHash())
		{
			parAssertf(member.m_Type == parMemberType::TYPE_STRUCT && 
				(member.GetSubType() == parMemberStructSubType::SUBTYPE_POINTER ||
				member.GetSubType() == parMemberStructSubType::SUBTYPE_SIMPLE_POINTER), 
				"Can only call this function for pointer-type members - type=%d, subtype=%d", (int)member.m_Type, member.GetSubType());
			// Get the instance in the array
			parPtrToStructure instance = reinterpret_cast<parPtrToStructure>(m_Data + (index * schemaData->m_Size));
			// Get the addr of the pointer inside this instance
			parPtrToStructure* addressOfPointer = reinterpret_cast<parPtrToStructure*>(instance + member.GetOffset());
			AddFixupRaw(*addressOfPointer, targetOfThePointer);
		}
	}
}

psoBuilderInstanceSet::~psoBuilderInstanceSet()
{
	for(int i = 0; i < m_Instances.GetCount(); i++)
	{
		delete m_Instances[i];
	}
}

u32 psoBuilderInstanceSet::ComputeSize()
{
	u32 resultSize = 0;
	for(int i = 0; i < m_Instances.GetCount(); i++)
	{
		resultSize += m_Instances[i]->ComputeSize( );
	}

	return resultSize;
}

u32 psoBuilderInstanceSet::ComputeFileLocations(u32 prevOffset, u32 tableIndex)
{
	// All instance sets should be 16 byte aligned (indiv. instances may have even stricter alignment)
	parAssertAligned(prevOffset, 16, "Instance set");
	m_Location.m_Offset = prevOffset;

	int childSizes = 0;
	for(int i = 0; i < m_Instances.GetCount(); i++)
	{
		u32 newSize = m_Instances[i]->ComputeFileLocation(m_Location.m_Offset + childSizes);
		m_Instances[i]->m_Id = psoStructId::Create(tableIndex, childSizes); // 'childSizes' here is also the offset into this structArray
		childSizes += newSize;
	}

	m_Location.m_Size = childSizes;
	return m_Location.m_Size;
}

void psoBuilderInstanceSet::Serialize(char* dest)
{
	for(int i = 0; i < m_Instances.GetCount(); i++)
	{
		m_Instances[i]->Serialize(dest);
	}
}


psoBuilderInstanceDataCatalog::psoBuilderInstanceDataCatalog()
	: m_RootObject(NULL)
	, m_InstanceSize(0)
	, m_MapSize(0)
{}

psoBuilderInstanceDataCatalog::~psoBuilderInstanceDataCatalog()
{
	atMap<u32, atArray<psoBuilderInstanceSet*> >::Iterator instSetIter = m_Instances.CreateIterator();
	while(!instSetIter.AtEnd())
	{
		for(int i = 0; i < instSetIter.GetData().GetCount(); i++)
		{
			delete instSetIter.GetData()[i];
		}

		instSetIter.Next();
	}
	for(int i = 0; i < m_NonNativeAlignedInstances.GetCount(); i++)
	{
		delete m_NonNativeAlignedInstances[i].m_Instances;
	}
}

psoBuilderStructSchema* psoBuilder::FindStructSchema(atLiteralHashValue hash)
{
	for(int i = 0; i < m_Schemas.m_StructSchemas.GetCount(); i++)
	{
		psoBuilderStructSchema* sch = m_Schemas.m_StructSchemas[i];
		if (sch->m_NameHash == hash)
		{
			return sch;
		}
	}

	return NULL;
}

psoBuilderEnumSchema* psoBuilder::FindEnumSchema(atLiteralHashValue hash)
{
	for(int i = 0; i < m_Schemas.m_EnumSchemas.GetCount(); i++)
	{
		psoBuilderEnumSchema* enm = m_Schemas.m_EnumSchemas[i];
		if (enm->m_NameHash == hash)
		{
			return enm;
		}
	}

	return NULL;
}


psoBuilderInstanceSet& psoBuilderInstanceDataCatalog::FindOrCreateInstanceSet(u32 namehashOrPodType, size_t sizeInBytes, size_t alignment, bool useNonNativeAlignment)
{
	if (useNonNativeAlignment)
	{
		NonNativeAlignedInstance& nnaInst = m_NonNativeAlignedInstances.Grow();
		nnaInst.m_Type = namehashOrPodType;
		nnaInst.m_Alignment = (u32)alignment;
		nnaInst.m_Instances = rage_new psoBuilderInstanceSet;
		return *(nnaInst.m_Instances);
	}
	else
	{
		atArray<psoBuilderInstanceSet*>& array = m_Instances[namehashOrPodType];
		for(int i = 0; i < array.GetCount(); i++)
		{
			if (array[i]->CanHold(sizeInBytes))
			{
				return *array[i];
			}
		}
		psoBuilderInstanceSet* newInst = rage_new psoBuilderInstanceSet;
		array.PushAndGrow(newInst);
		return *newInst;
	}
}

// Note doesn't take an offset parameter here because all of the offsets we compute for the
// child objects should be relative to this one
void psoBuilderInstanceDataCatalog::ComputeFileLocations()
{
	u32 mapHeaderSize = sizeof(psoStructureMapData);
	u32 structArrayTableSize = 0;
	atMap<u32, atArray<psoBuilderInstanceSet*> >::Iterator instSetIter = m_Instances.CreateIterator();
	for(instSetIter.Start(); !instSetIter.AtEnd(); instSetIter.Next())
	{
		structArrayTableSize += instSetIter.GetData().GetCount();
	}
	structArrayTableSize += m_NonNativeAlignedInstances.GetCount();

	structArrayTableSize *= sizeof(psoStructArrayTableData);

	m_MapSize = RoundUp<4>(mapHeaderSize + structArrayTableSize);

	u32 instHeaderSize = sizeof(psoStructureData);

	u32 instanceOffset = instHeaderSize;

	u32 tableIndex = 0;
	instSetIter = m_Instances.CreateIterator();
	for(instSetIter.Start(); !instSetIter.AtEnd(); instSetIter.Next())
	{
		atArray<psoBuilderInstanceSet*>& array = instSetIter.GetData();
		for(int i = 0; i < array.GetCount(); i++)
		{
			instanceOffset = RoundUp<16>(instanceOffset); // All instance sets need to start on a 16 byte boundary
			psoBuilderInstanceSet& set = *array[i];
			u32 instanceSetSize = set.ComputeFileLocations(instanceOffset, tableIndex);
			instanceOffset += instanceSetSize;
			tableIndex++;
		}
	}

	for(int i = 0; i < m_NonNativeAlignedInstances.GetCount(); i++)
	{
		psoBuilderInstanceSet& set = *(m_NonNativeAlignedInstances[i].m_Instances);
		instanceOffset = (u32)AlignPow2(instanceOffset, Max(16u, m_NonNativeAlignedInstances[i].m_Alignment));
		u32 instanceSetSize = set.ComputeFileLocations(instanceOffset, tableIndex);
		instanceOffset += instanceSetSize;
		tableIndex++;
	}

	m_InstanceSize = RoundUp<4>(instanceOffset);

	if (m_HashStrings.GetNumUsed() > 0)
	{
		m_HashStringSize = sizeof(parIffHeader);
		atMap<u32, const char*>::Iterator stringIter = m_HashStrings.CreateIterator();
		for(stringIter.Start(); !stringIter.AtEnd(); stringIter.Next())
		{
			if (stringIter.GetData())
			{
				m_HashStringSize += (u32)strlen(stringIter.GetData()) + 1;
			}
		}
		m_HashStringSize = RoundUp<4>(m_HashStringSize);
	}
	else
	{
		m_HashStringSize = 0;
	}

	if (m_DebugHashStrings.GetNumUsed() > 0)
	{
		m_DebugHashStringSize = 0;
		atMap<u32, const char*>::Iterator stringIter = m_DebugHashStrings.CreateIterator();
		for(stringIter.Start(); !stringIter.AtEnd(); stringIter.Next())
		{
			if (stringIter.GetData())
			{
				m_DebugHashStringSize += (u32)strlen(stringIter.GetData()) + 1;
			}
		}
		m_DebugHashStringSize = RoundUp<16>(m_DebugHashStringSize) + sizeof(parIffHeader);
	}
	else
	{
		m_DebugHashStringSize = 0;
	}
}

u32 psoBuilderInstanceDataCatalog::ComputeSize()
{
	u32 resultSize = 0;

	u32 mapHeaderSize = sizeof(psoStructureMapData);
	u32 structArrayTableSize = 0;
	atMap<u32, atArray<psoBuilderInstanceSet*> >::Iterator instSetIter = m_Instances.CreateIterator();
	for(instSetIter.Start(); !instSetIter.AtEnd(); instSetIter.Next())
	{
		structArrayTableSize += instSetIter.GetData().GetCount();
	}
	structArrayTableSize += m_NonNativeAlignedInstances.GetCount();

	structArrayTableSize *= sizeof(psoStructArrayTableData);

	resultSize += RoundUp<4>(mapHeaderSize + structArrayTableSize);

	u32 instHeaderSize = sizeof(psoStructureData);

	u32 instanceOffset = instHeaderSize;

	u32 tableIndex = 0;
	instSetIter = m_Instances.CreateIterator();
	for(instSetIter.Start(); !instSetIter.AtEnd(); instSetIter.Next())
	{
		atArray<psoBuilderInstanceSet*>& array = instSetIter.GetData();
		for(int i = 0; i < array.GetCount(); i++)
		{
			instanceOffset = RoundUp<16>(instanceOffset); // All instance sets need to start on a 16 byte boundary
			psoBuilderInstanceSet& set = *array[i];
			u32 instanceSetSize = set.ComputeSize( );
			instanceOffset += instanceSetSize;
			tableIndex++;
		}
	}

	for(int i = 0; i < m_NonNativeAlignedInstances.GetCount(); i++)
	{
		psoBuilderInstanceSet& set = *(m_NonNativeAlignedInstances[i].m_Instances);
		instanceOffset = (u32)AlignPow2(instanceOffset, Max(16u, m_NonNativeAlignedInstances[i].m_Alignment));
		u32 instanceSetSize = set.ComputeSize( );
		instanceOffset += instanceSetSize;
		tableIndex++;
	}

	resultSize += RoundUp<4>(instanceOffset);

	u32 hashStringSize = 0;
	if (m_HashStrings.GetNumUsed() > 0)
	{
		hashStringSize = sizeof(parIffHeader);
		atMap<u32, const char*>::Iterator stringIter = m_HashStrings.CreateIterator();
		for(stringIter.Start(); !stringIter.AtEnd(); stringIter.Next())
		{
			if (stringIter.GetData())
			{
				hashStringSize += (u32)strlen(stringIter.GetData()) + 1;
			}
		}
		hashStringSize = RoundUp<4>(m_HashStringSize);
	}
	resultSize += hashStringSize;

	hashStringSize = 0;
	if (m_DebugHashStrings.GetNumUsed() > 0)
	{
		atMap<u32, const char*>::Iterator stringIter = m_DebugHashStrings.CreateIterator();
		for(stringIter.Start(); !stringIter.AtEnd(); stringIter.Next())
		{
			if (stringIter.GetData())
			{
				hashStringSize += (u32)strlen(stringIter.GetData()) + 1;
			}
		}
		hashStringSize = RoundUp<16>(hashStringSize) + sizeof(parIffHeader);
	}
	resultSize += hashStringSize;

	return resultSize;
}

size_t psoBuilderInstanceDataCatalog::SerializeMap(char* dest, psoStructureMapData*& outMap)
{
	psoStructureMapData* mapHeader = reinterpret_cast<psoStructureMapData*>(dest);
	mapHeader->m_Header.SetMagic(psoConstants::PMAP_MAGIC_NUMBER);
	mapHeader->m_Header.SetSize(m_MapSize);

	if (m_RootObject)
	{
		mapHeader->m_RootObject = m_RootObject->m_Id;
	}
	else
	{
		mapHeader->m_RootObject = psoStructId::Null();
	}


	psoStructArrayTableData* structArrayTableData = reinterpret_cast<psoStructArrayTableData*>(dest + sizeof(psoStructureMapData));

	atMap<u32, atArray<psoBuilderInstanceSet*> >::Iterator iter = m_Instances.CreateIterator();

	u16 numArrays = 0;

	for(iter.Start(); !iter.AtEnd(); iter.Next())
	{
		atArray<psoBuilderInstanceSet*>& array = iter.GetData();
		for(int i = 0; i < array.GetCount(); i++)
		{
			psoBuilderInstanceSet& set = *array[i];
			structArrayTableData->m_TypeHash = iter.GetKey();
			structArrayTableData->m_Offset = set.m_Location.m_Offset;
			structArrayTableData->m_Pad = 0;
			structArrayTableData->m_Size = set.m_Location.m_Size;
			structArrayTableData++;
			numArrays++;
		}
	}

	for(int i = 0; i < m_NonNativeAlignedInstances.GetCount(); i++)
	{
		psoBuilderInstanceSet& set = *m_NonNativeAlignedInstances[i].m_Instances;
		structArrayTableData->m_TypeHash = m_NonNativeAlignedInstances[i].m_Type;
		structArrayTableData->m_Offset = set.m_Location.m_Offset;
		structArrayTableData->m_Pad = 0;
		structArrayTableData->m_Size = set.m_Location.m_Size;
		structArrayTableData++;
		numArrays++;
	}

	mapHeader->m_NumStructArrays = numArrays;

	outMap = mapHeader;
	return m_MapSize;
}


size_t psoBuilderInstanceDataCatalog::SerializeInstances(char* dest, psoStructureData*& outInstances)
{
	psoStructureData* instHeader = reinterpret_cast<psoStructureData*>(dest);
	instHeader->m_Header.SetMagic( psoConstants::PSIN_MAGIC_NUMBER ); 
	instHeader->m_Header.SetSize( m_InstanceSize );

	atMap<u32, atArray<psoBuilderInstanceSet*> >::Iterator iter = m_Instances.CreateIterator();

	for(iter.Start(); !iter.AtEnd(); iter.Next())
	{
		atArray<psoBuilderInstanceSet*>& array = iter.GetData();
		for(int i = 0; i < array.GetCount(); i++)
		{
			array[i]->Serialize(dest);
		}
	}

	for(int i = 0; i < m_NonNativeAlignedInstances.GetCount(); i++)
	{
		m_NonNativeAlignedInstances[i].m_Instances->Serialize(dest);
	}

	outInstances = instHeader;

	return m_InstanceSize;
}

size_t psoBuilderInstanceDataCatalog::SerializeHashStrings(char* dest)
{

	if (m_HashStrings.GetNumUsed() > 0)
	{
		char* currPos = dest;

		parIffHeader* stringsHeader = reinterpret_cast<parIffHeader*>(dest);
		stringsHeader->SetMagic(psoConstants::STRF_MAGIC_NUMBER);
		stringsHeader->SetSize(m_HashStringSize);
		atMap<u32, const char*>::Iterator iter = m_HashStrings.CreateIterator();

		currPos += sizeof(parIffHeader);

		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			if (iter.GetData())
			{
				size_t len = strlen(iter.GetData()) + 1;
				sysMemCpy(currPos, iter.GetData(), len);
				currPos += len;
			}
		}

		// zero out the rest of the memory set aside for this section, so we don't treat padding bytes as if they are another string.
		memset(currPos, 0, (dest + m_HashStringSize) - currPos);
	}


	if (m_DebugHashStrings.GetNumUsed() > 0)
	{
		char* currPos = dest + m_HashStringSize;

		parIffHeader* stringsHeader = reinterpret_cast<parIffHeader*>(currPos);
		stringsHeader->SetMagic(psoConstants::STRE_MAGIC_NUMBER);
		stringsHeader->SetSize(m_DebugHashStringSize);
		atMap<u32, const char*>::Iterator iter = m_DebugHashStrings.CreateIterator();

		currPos += sizeof(parIffHeader);
#if !__FINAL
		char* startOfStrings = currPos;
#endif

		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			if (iter.GetData())
			{
				size_t len = strlen(iter.GetData()) + 1;
				sysMemCpy(currPos, iter.GetData(), len);
				currPos += len;
			}
		}

		// zero out the rest of the memory set aside for this section, so we don't treat padding bytes as if they are another string.
		memset(currPos, 0, (dest + m_HashStringSize + m_DebugHashStringSize) - currPos);

#if !__FINAL
		AES aes(psoResourceData::NonfinalStringKey);
		aes.Encrypt(startOfStrings, m_DebugHashStringSize - sizeof(parIffHeader) ); 
#endif

	}

	return m_HashStringSize + m_DebugHashStringSize;
}


psoBuilder::psoBuilder()
: m_UnderConstruction(true)
, m_NextAnonId(psoConstants::FIRST_ANONYMOUS_ID)
, m_IncludeChecksum(false)
, m_FileSize(0)
{
}

psoBuilderStructSchema& psoBuilder::CreateStructSchema(atLiteralHashValue namehash, size_t size /* = -1 */, size_t align /* = 1 */)
{
	if (namehash.IsNull())
	{
		parAssertf(psoConstants::IsAnonymous(m_NextAnonId), "Ran out of anonymous structure IDs");
		namehash = atLiteralHashValue(m_NextAnonId);
		++m_NextAnonId;
	}

	parAssertf(!FindStructSchema(namehash), "Can't add a schema that's already been added!");
	psoBuilderStructSchema* sch = rage_new psoBuilderStructSchema(this);
	sch->m_NameHash = namehash;
	sch->m_Alignment = (u16)align;
	if ((int)size < 0)
	{
		sch->m_AutoPack = true;
		sch->m_Structure->m_Size = 0;
	}
	else
	{
		sch->m_Structure->m_Size = (u32)size;
	}

	m_Schemas.m_StructSchemas.PushAndGrow(sch);

	return *sch;
}

psoBuilderEnumSchema& psoBuilder::CreateEnumSchema(atLiteralHashValue namehash)
{
	if (namehash.IsNull())
	{
		parAssertf(psoConstants::IsAnonymous(m_NextAnonId), "Ran out of anonymous structure IDs");
		namehash = atLiteralHashValue(m_NextAnonId);
		++m_NextAnonId;
	}

	parAssertf(!FindEnumSchema(namehash), "Can't add a schema that's already been added!");
	psoBuilderEnumSchema* enm = rage_new psoBuilderEnumSchema;

	enm->m_NameHash = namehash;

	m_Schemas.m_EnumSchemas.PushAndGrow(enm);

	return *enm;
}

// Adds a single instance (or single array of instances) to the instance data catalog
psoBuilderInstance& psoBuilder::AddStructInstances(psoBuilderStructSchema& schema, parConstPtrToStructure instanceData, size_t count, size_t align)
{
	u32 realAlignment = Max((u32)align, (u32)schema.m_Alignment);
	size_t sizeInBytes = count * schema.GetSchemaData()->m_Size;
	psoBuilderInstanceSet& set = m_Instances.FindOrCreateInstanceSet(schema.m_NameHash.GetHash(), sizeInBytes, realAlignment, align > schema.m_Alignment);

	psoBuilderInstance* newInst = rage_new psoBuilderInstance;
	newInst->m_Data = const_cast<parPtrToArray>(reinterpret_cast<parConstPtrToArray>(instanceData));
	newInst->m_StructSchema = &schema;
	newInst->m_Count = (u16)count;
	newInst->m_PodType = parMemberType::INVALID_TYPE;
	newInst->m_OwnsData = false;

	set.m_Instances.PushAndGrow(newInst);
	set.m_Size += sizeInBytes;
	return *set.m_Instances.Top();
}

// Adds a single instance (or single array of instances) to the instance data catalog
psoBuilderInstance& psoBuilder::CreateStructInstances(psoBuilderStructSchema& schema, size_t count, size_t align)
{
	size_t storageSize = schema.m_Structure->m_Size * count;
	parPtrToStructure storage = reinterpret_cast<parPtrToStructure>(rage_aligned_new(align) char[storageSize]);

	psoBuilderInstance& newInst = AddStructInstances(schema, storage, count, align);
	newInst.m_OwnsData = true;

	return newInst;
}

psoBuilderInstance& psoBuilder::AddPodArray(parMemberType::Enum type, parConstPtrToArray object, size_t sizeInBytes, size_t align)
{
	u32 nativeAlign = 0;

	// Special cases that parMemberType can't handle:
	if (type == parMemberType::TYPE_STRUCT)
	{
		nativeAlign = __alignof(void*); 	// special case - TYPE_STRUCT => array of pointers
	}
	else if (type == psoConstants::TYPE_ATSTRING_IN_ARRAY)
	{
		nativeAlign = __alignof(atString);
	}
	else if (type == psoConstants::TYPE_ATWIDESTRING_IN_ARRAY)
	{
		nativeAlign = __alignof(atWideString);
	}
	else
	{
		// Normal case
		nativeAlign = (u32)parMemberType::GetAlign((parMemberType::Enum)type);
	}

	u32 realAlignment = Max((u32)align, nativeAlign);
	psoBuilderInstanceSet& set = m_Instances.FindOrCreateInstanceSet(type, sizeInBytes, realAlignment, align > nativeAlign);

	psoBuilderInstance* newInst = rage_new psoBuilderInstance;
	newInst->m_Data = const_cast<parPtrToArray>(object);
	newInst->m_Location.m_Size = (u32)sizeInBytes;
	newInst->m_PodType = (u8)type;
	newInst->m_Align = realAlignment;

	set.m_Instances.PushAndGrow(newInst);
	set.m_Size += sizeInBytes;

	return *set.m_Instances.Top();
}

psoBuilderInstance& psoBuilder::CreatePodArray(parMemberType::Enum type, size_t sizeInBytes, size_t align)
{
	size_t naturalAlignment = 0;
	// Check for weird special extensions
	if (type == parMemberType::TYPE_STRUCT || // TYPE_STRUCT means pointer here
		type == psoConstants::TYPE_ATSTRING_IN_ARRAY || // array of atstrings
		type == psoConstants::TYPE_ATWIDESTRING_IN_ARRAY) // array of atwidestrings
	{
		naturalAlignment = __alignof(char*);
	}
	else
	{
		naturalAlignment = parMemberType::GetAlign(type);
	}

	size_t realAlign = Max(naturalAlignment, align);

	FastAssert(sizeInBytes % naturalAlignment == 0);

	parPtrToArray storage = reinterpret_cast<parPtrToArray>(rage_aligned_new(realAlign) char[sizeInBytes]);
	psoBuilderInstance& newInst = AddPodArray(type, storage, sizeInBytes, realAlign);
	newInst.m_OwnsData = true;

	return newInst;
}


void psoBuilder::AddExtraString(u32 hash, const char* strData)
{
	if (hash != 0)
	{
		m_Instances.m_HashStrings[hash] = strData;
	}
}


void psoBuilder::AddExtraDebugString(u32 hash, const char* strData)
{
	if (hash != 0)
	{
		m_Instances.m_DebugHashStrings[hash] = strData;
	}
}

void psoBuilder::SetRootObject(psoBuilderInstance& inst)
{
	m_Instances.m_RootObject = &inst;
}

void psoBuilder::FinishBuilding()
{
	FastAssert(m_UnderConstruction);

	//Compute the final size before we change the m_UnderConstruction flag.
	ComputeSize();

	m_UnderConstruction = false;

	m_Schemas.ComputeFileLocations();
	m_Instances.ComputeFileLocations();

	Assert(GetFileSize() == m_FileSize);
}

u32 psoBuilder::ComputeSize()
{
	if (m_UnderConstruction)
	{
		m_FileSize = m_Schemas.ComputeSize() + m_Instances.ComputeSize();
		if (m_IncludeChecksum)
		{
			m_FileSize += sizeof(psoChecksumData2); // CHKS
		}
	}

	return m_FileSize;
}

u32 psoBuilder::GetFileSize()
{
	FastAssert(!m_UnderConstruction);
	u32 fileSize = 0;
	fileSize += m_Schemas.m_Location.m_Size;			// PSCH
	fileSize += m_Schemas.m_SignatureLocation.m_Size;	// PSIG
	fileSize +=	m_Instances.m_InstanceSize;				// PSIN 
	fileSize +=	m_Instances.m_MapSize;					// PMAP 
	fileSize +=	m_Instances.m_HashStringSize;			// STRF 
	fileSize += m_Instances.m_DebugHashStringSize;		// STRS
	if (m_IncludeChecksum)
	{
		fileSize += sizeof(psoChecksumData2); // CHKS
	}
	return fileSize;
}

bool psoBuilder::Save(const char* filename)
{
	parDebugf1("Saving PSO to file '%s'", filename);
	DIAG_CONTEXT_MESSAGE("Saving PSO to file '%s'", filename);
	fiSafeStream S(ASSET.Create(filename, ""));
	if (!S)
	{
		return false;
	}

	if (m_UnderConstruction)
	{
		FinishBuilding();
	}

	if (m_Instances.m_DebugHashStringSize > 10 * 1024)
	{
		parWarningf("PSO file %s includes %u bytes of debug strings", filename, m_Instances.m_DebugHashStringSize);
	}

	// Note! If the instance data section is ever not at the beginning
	// of the file, make sure it starts on a 16 byte boundary

	u32 totalSize = GetFileSize();

	char* buff = rage_aligned_new(16) char[totalSize];

	SaveToBuffer(buff, totalSize);
	
	S->Write(buff, totalSize);

	S->Flush();

	delete [] buff;

	return true;
}

bool psoBuilder::SaveToBuffer(char* buff, size_t sizeInBytes)
{
	if (m_UnderConstruction)
	{
		FinishBuilding();
	}

	u32 totalSize = GetFileSize();

	if (!parVerifyf(sizeInBytes >= totalSize, "Buffer for storing the PSO data wasn't big enough!"))
	{
		return false;
	}

	sysMemSet(buff, 0x70, sizeInBytes);

	char* currSection = buff;
	psoSchemaCatalogData* schemaCat = NULL;
	parIffHeader* signaturesHeader = NULL;
	psoStructureMapData* map = NULL;
	psoStructureData* instances = NULL;

	currSection += m_Instances.SerializeInstances(currSection, instances);
	currSection += m_Instances.SerializeMap(currSection, map);
	currSection += m_Schemas.Serialize(*this, currSection, schemaCat, signaturesHeader);
	currSection += m_Instances.SerializeHashStrings(currSection);

	u32* signatures = reinterpret_cast<u32*>(signaturesHeader+1);
	// Make a temporary psoResourceData, so that we can compute signatures and maybe byte swap (since those operations depend on new-style data)
	psoResourceData* tempResourceData = rage_new psoResourceData;
	psoIffToRscConversion::ConvertToNewFormat(*schemaCat, *tempResourceData, g_sysPlatform);
	psoIffToRscConversion::ConvertToNewFormat(*map, instances, *tempResourceData, g_sysPlatform);
	psoSchemaCatalog tempCat(*tempResourceData);

	tempCat.ComputeAllSignatures(false);

	for(u32 i =0; i < schemaCat->m_NumTypes; i++)
	{
		psoTypeTableData& type = schemaCat->GetType(i);
		atLiteralHashValue hash = atLiteralHashValue(type.m_TypeHash);
		psoStructureSchema structSchema = tempCat.FindStructureSchema(hash);
		if (structSchema.IsValid())
		{
			u32 sig = structSchema.GetSignature();
			signatures[i] = sig;
		}
		else
		{
			psoEnumSchema enumSchema = tempCat.FindEnumSchema(hash);
			if (enumSchema.IsValid())
			{
				signatures[i] = enumSchema.GetSignature();
			}
			else
			{
				parAssertf(0, "Not a struct and not an enum, what is it?");
				signatures[i] = 0;
			}
		}
	}


	psoChecksumData2* checksum = NULL;
	if (m_IncludeChecksum)
	{
		// Set up the checksum fields now, but don't actually write the checksum value until after the byte swap, since we need to compute the checksum with byte swapped data
		checksum = (psoChecksumData2*)currSection;
		checksum->m_IffTag.SetMagic(psoConstants::CHKS_MAGIC_NUMBER);
		checksum->m_IffTag.SetSize(sizeof(psoChecksumData2));
		checksum->m_Checksum = 0; // Set to 0 before computing the checksum. We also set this to 0 before computing the checksum on load
		checksum->m_Filesize =  0; // ditto
		checksum->m_PlatformId = g_sysPlatform;
	}

	// Swap to little endian format
#if !__BE
	psoSwapStructArrays((char*)instances, tempCat, *map, false);

	u32 numSigs = schemaCat->m_NumTypes;
	for(u32 i = 0; i < numSigs; i++)
	{
		SwapMe(signatures[i]);
	}
	psoSwapStructureMap(*map, false);
	psoSwapSchemaCatalog(*schemaCat, false);
#endif

	tempResourceData->DeleteSchemaData();
	tempResourceData->DeleteStructArrays();
	delete tempResourceData;

	if (m_IncludeChecksum)
	{
		u32 sum = atDataHash(buff, totalSize, psoChecksumData2::ChecksumSalt);
		checksum->m_Checksum = sysEndian::NtoB(sum);
		checksum->m_Filesize = sysEndian::NtoB(totalSize);
	}


	return true;
}

