// 
// parser/psoschemadata.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "psoschemadata.h"

#include "macros.h"
#include "psoconsts.h"
#include "psoresource.h"
#include "psotypes.h"

#include "data/safestruct.h"
#include "math/intrinsics.h"
#include "system/platform.h"

#include "math/intrinsics.h"

using namespace rage;

template<typename Type>
void BigEndianHash(const Type& t, u32& inOutHash)
{
	Type tBig = NtoB(t);
	inOutHash = atPartialDataHash(reinterpret_cast<char*>(&tBig), sizeof(Type), inOutHash);
}

// Specialization for padding arrays
template<int N>
void BigEndianHash(const char(&pad)[N], u32& inOutHash) // pad is a reference to an array of N const chars 
{
  parErrorf(0, "Don't hash padding, it wasn't working before and we want to keep the hashes consistent");
}

u8 psoRscMemberSchemaData::FindAlignPower(size_t alignment)
{
	FastAssert(alignment > 0);
	FastAssert(_IsPowerOfTwo((int)alignment));
	return (u8)(_FloorLog2((int)alignment));
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// psoSchemaMemberData2

rage::psoRscMemberSchemaData::psoRscMemberSchemaData( datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize /*size*/ )
	: m_NameHash(rsc)
{
	if (swap == PSOPTR_SWAP)
	{
		m_NameHash.SetHash(sysEndian::Swap(m_NameHash.GetHash()));
		sysEndian::SwapMe(m_Offset);
		sysEndian::SwapMe(Generic.m_Type);

		switch(GetMemberDataType(GetType()))
		{
		case MEM_GENERIC:
			break;
		case MEM_ENUM_OR_STRUCT:
			sysEndian::SwapMe(EnumOrStruct.m_ReferentHash);
			break;
		case MEM_STRING:
			sysEndian::SwapMe(String.m_NamespaceIndex);
			sysEndian::SwapMe(String.m_Count);
			break;
		case MEM_ARRAY: 
			sysEndian::SwapMe(Array.m_AlignPowerAndFlags);
			sysEndian::SwapMe(Array.m_ContentsIndex);
			sysEndian::SwapMe(Array.m_Count);
			break;
		case MEM_ARRAY_WITH_COUNTER: 
			sysEndian::SwapMe(Array.m_AlignPowerAndFlags);
			sysEndian::SwapMe(Array.m_ContentsIndex);
			sysEndian::SwapMe(Array.m_CounterIndex);
			break;
		case MEM_BITSET:
			sysEndian::SwapMe(EnumOrStruct.m_ReferentHash);
			break;
		case MEM_MAP:
			sysEndian::SwapMe(Map.m_KeyIndex);
			sysEndian::SwapMe(Map.m_ValueIndex);
			break;
		}
	}
}

rage::psoRscMemberSchemaData::psoRscMemberSchemaData() 
	: m_Offset(0)
{
	Generic.m_Type = psoType::TYPE_INVALID;
	for(int i = 0; i < NELEM(Generic.m_Pad); i++) { 
		Generic.m_Pad[i] = 0; 
	}
}

#if __DECLARESTRUCT
void rage::psoRscMemberSchemaData::DeclareStruct( datTypeStruct& s )
{
	STRUCT_BEGIN(psoRscMemberSchemaData);
	STRUCT_FIELD((u32&)m_NameHash);
	STRUCT_FIELD(m_Offset);

	switch(GetMemberDataType(GetType()))
	{
	case MEM_GENERIC:
		STRUCT_FIELD(Generic.m_Type);
		STRUCT_CONTAINED_ARRAY_COUNT(Generic.m_Pad, NELEM(Generic.m_Pad));
		break;
	case MEM_ENUM_OR_STRUCT:
		STRUCT_FIELD(EnumOrStruct.m_Type);
		STRUCT_CONTAINED_ARRAY_COUNT(EnumOrStruct.m_Pad, NELEM(EnumOrStruct.m_Pad));
		STRUCT_FIELD(EnumOrStruct.m_ReferentHash);
		break;
	case MEM_STRING:
		STRUCT_FIELD(String.m_Type);
		STRUCT_FIELD(String.m_NamespaceIndex);
		STRUCT_CONTAINED_ARRAY_COUNT(String.m_Pad, NELEM(String.m_Pad));
		STRUCT_FIELD(String.m_Count);
		break;
	case MEM_ARRAY:
		STRUCT_FIELD(Array.m_Type);
		STRUCT_FIELD(Array.m_AlignPowerAndFlags);
		STRUCT_FIELD(Array.m_ContentsIndex);
		STRUCT_FIELD(Array.m_Count);
		break;
	case MEM_ARRAY_WITH_COUNTER:
		STRUCT_FIELD(Array.m_Type);
		STRUCT_FIELD(Array.m_AlignPowerAndFlags);
		STRUCT_FIELD(Array.m_ContentsIndex);
		STRUCT_FIELD(Array.m_CounterIndex);
		STRUCT_CONTAINED_ARRAY_COUNT(Array.m_Pad, NELEM(Array.m_Pad));
		break;
	case MEM_BITSET:
		STRUCT_FIELD(Bitset.m_Type);
		STRUCT_CONTAINED_ARRAY_COUNT(Bitset.m_Pad, NELEM(Bitset.m_Pad));
		STRUCT_FIELD(Bitset.m_BitCount);
		STRUCT_FIELD(Bitset.m_EnumHash);
		break;
	case MEM_MAP:
		STRUCT_FIELD(Map.m_Type);
		STRUCT_CONTAINED_ARRAY_COUNT(Map.m_Pad, NELEM(Map.m_Pad));
		STRUCT_FIELD(Map.m_KeyIndex);
		STRUCT_FIELD(Map.m_ValueIndex);
		break;
	}
	STRUCT_END();
}
#endif // __DECLARESTRUCT

void rage::psoRscMemberSchemaData::Place( psoRscMemberSchemaData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
{
	::new(ptr) psoRscMemberSchemaData(rsc, swap, size);
}

u32 rage::psoRscMemberSchemaData::ComputePartialSignature(u32 partialSig)
{
	u32 sig = partialSig;
	BigEndianHash(m_NameHash.GetHash(), sig);
	BigEndianHash(m_Offset, sig);

	// Padding isn't hashed. So if you want to turn the padding into something meaningful
	// in the future, and maintain backwards compatibility of signatures, don't hash your new
	// data when it's 0x0 (see String.m_NamespaceIndex below

	switch(GetMemberDataType(GetType()))
	{
	case MEM_GENERIC:
		BigEndianHash(Generic.m_Type, sig);
		// + Unhashed padding
		break;
	case MEM_ENUM_OR_STRUCT:
		BigEndianHash(EnumOrStruct.m_Type, sig);
		// + Unhashed padding
		BigEndianHash(EnumOrStruct.m_ReferentHash, sig);
		break;
	case MEM_STRING:
		BigEndianHash(String.m_Type, sig);
		if (String.m_NamespaceIndex != 0) // Only hash 
		{
			BigEndianHash(String.m_NamespaceIndex, sig);
		}
		// + Unhashed padding
		BigEndianHash(String.m_Count, sig);
		break;
	case MEM_ARRAY:
		BigEndianHash(Array.m_Type, sig);
		BigEndianHash(Array.m_AlignPowerAndFlags, sig);
		BigEndianHash(Array.m_ContentsIndex, sig);
		BigEndianHash(Array.m_Count, sig);
		break;
	case MEM_ARRAY_WITH_COUNTER:
		BigEndianHash(Array.m_Type, sig);
		BigEndianHash(Array.m_AlignPowerAndFlags, sig);
		BigEndianHash(Array.m_ContentsIndex, sig);
		BigEndianHash(Array.m_CounterIndex, sig);
		break;
	case MEM_BITSET:
		BigEndianHash(Bitset.m_Type, sig);
		// + Unhashed padding
		BigEndianHash(Bitset.m_BitCount, sig);
		BigEndianHash(Bitset.m_EnumHash, sig);
		break;
	case MEM_MAP:
		BigEndianHash(Map.m_Type, sig);
		// + Unhashed padding
		BigEndianHash(Map.m_KeyIndex, sig);
		BigEndianHash(Map.m_ValueIndex, sig);
		break;
	}

	return sig;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// psoStructSchemaData2

rage::psoRscStructSchemaData::psoRscStructSchemaData( datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
: m_NameHash(rsc)
, m_Members(rsc, swap, size)
{
	if (swap == PSOPTR_SWAP)
	{
		m_NameHash.SetHash(sysEndian::Swap(m_NameHash.GetHash()));
		sysEndian::SwapMe(m_Signature);
		sysEndian::SwapMe(m_Flags);
		sysEndian::SwapMe(m_AlignPower);
		sysEndian::SwapMe(m_Size);
		sysEndian::SwapMe(m_MajorVersion);
		sysEndian::SwapMe(m_NumMembers);
	}

	for(int i = 0; i < m_NumMembers; i++)
	{
		psoRscMemberSchemaData::Place(&m_Members[i], rsc, swap, size);
	}
}

rage::psoRscStructSchemaData::psoRscStructSchemaData()
	: m_Signature(0)
	, m_Flags(0)
	, m_AlignPower(0)
	, m_Size(0)
	, m_MajorVersion(0)
	, m_NumMembers(0)
{
}

#if __DECLARESTRUCT
void rage::psoRscStructSchemaData::DeclareStruct( datTypeStruct& s )
{
	for(int i = 0; i < m_NumMembers; i++)
	{
		datTypeStruct s2;
		m_Members[i].DeclareStruct(s2);
	}

	SSTRUCT_BEGIN(psoRscStructSchemaData)
		SSTRUCT_FIELD_AS(psoRscStructSchemaData, m_NameHash, u32)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_Signature)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_Flags)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_AlignPower)
		SSTRUCT_IGNORE(psoRscStructSchemaData, m_Pad)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_Members)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_Size)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_MajorVersion)
		SSTRUCT_FIELD(psoRscStructSchemaData, m_NumMembers)
	SSTRUCT_END(psoRscStructSchemaData)
}
#endif // __DECLARESTRUCT

void rage::psoRscStructSchemaData::Place( psoRscStructSchemaData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
{
	::new(ptr) psoRscStructSchemaData(rsc, swap, size);
}

u32 rage::psoRscStructSchemaData::ComputeBaseSignature(bool considerName /* = false */)
{
	u32 sig = HashSalt;
	BigEndianHash(considerName ? m_NameHash.GetHash() : 0U, sig);
	BigEndianHash(m_Flags, sig);
	BigEndianHash(m_AlignPower, sig);
	BigEndianHash(m_Size, sig);
	BigEndianHash(m_MajorVersion, sig);
	BigEndianHash(m_NumMembers, sig);

	for(int i = 0; i < m_NumMembers; i++)
	{
		sig = m_Members[i].ComputePartialSignature(sig);
	}

	return atFinalizeHash(sig);
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// psoEnumTableEntry2

rage::psoRscEnumSchemaValueData::psoRscEnumSchemaValueData( datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize /*size*/ )
	: m_NameHash(rsc)
{
	if (swap)
	{
		m_NameHash.SetHash(sysEndian::Swap(m_NameHash.GetHash()));
		sysEndian::SwapMe(m_Value);
	}
}

#if __DECLARESTRUCT
void rage::psoRscEnumSchemaValueData::DeclareStruct( datTypeStruct& s )
{
	SSTRUCT_BEGIN(psoRscEnumSchemaValueData)
		SSTRUCT_FIELD_AS(psoRscEnumSchemaValueData, m_NameHash, u32)
		SSTRUCT_FIELD(psoRscEnumSchemaValueData, m_Value)
	SSTRUCT_END(psoRscEnumSchemaValueData)
}
#endif // __DECLARESTRUCT

void rage::psoRscEnumSchemaValueData::Place( psoRscEnumSchemaValueData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
{
	::new(ptr) psoRscEnumSchemaValueData(rsc, swap, size);
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// psoEnumSchemaData2

rage::psoRscEnumSchemaData::psoRscEnumSchemaData( datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
: m_NameHash(rsc)
, m_Values(rsc, swap, size)
{
	if (swap)
	{
		m_NameHash.SetHash(sysEndian::Swap(m_NameHash.GetHash()));
		sysEndian::SwapMe(m_Signature);
		sysEndian::SwapMe(m_NumValues);
	}

	for(int i = 0; i < m_NumValues; i++)
	{
		psoRscEnumSchemaValueData::Place(&m_Values[i], rsc, swap, size);
	}
}

#if __DECLARESTRUCT
void rage::psoRscEnumSchemaData::DeclareStruct( datTypeStruct& s )
{
	for(int i = 0; i < m_NumValues; i++)
	{
		datTypeStruct s2;
		m_Values[i].DeclareStruct(s2);
	}
	SSTRUCT_BEGIN(psoRscEnumSchemaData)
		SSTRUCT_FIELD_AS(psoRscEnumSchemaData, m_NameHash, u32)
		SSTRUCT_FIELD(psoRscEnumSchemaData, m_Signature)
		SSTRUCT_FIELD(psoRscEnumSchemaData, m_Values)
		SSTRUCT_FIELD(psoRscEnumSchemaData, m_NumValues)
		SSTRUCT_FIELD(psoRscEnumSchemaData, m_Flags)
		SSTRUCT_IGNORE(psoRscEnumSchemaData, m_Pad)
	SSTRUCT_END(psoRscEnumSchemaData)
}
#endif // __DECLARESTRUCT

void rage::psoRscEnumSchemaData::Place( psoRscEnumSchemaData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
{
	::new(ptr) psoRscEnumSchemaData(rsc, swap, size);
}

u32 rage::psoRscEnumSchemaData::ComputeBaseSignature()
{
	u32 sig = HashSalt;
	BigEndianHash(m_NameHash.GetHash(), sig);
	BigEndianHash(m_NumValues, sig);
	BigEndianHash(m_Flags, sig);

	for(int i = 0; i < m_NumValues; i++)
	{
		BigEndianHash(m_Values[i].m_NameHash.GetHash(), sig);
		BigEndianHash(m_Values[i].m_Value, sig);
	}
	return atFinalizeHash(sig);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// psoStructArrayTableData2

rage::psoRscStructArrayTableData::psoRscStructArrayTableData( datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
	: m_NameHash(rsc)
	, m_Data(rsc, swap, size)
{
	if (swap)
	{
		m_NameHash.SetHash(sysEndian::Swap(m_NameHash.GetHash()));
		sysEndian::SwapMe(m_Size);
	}
}

#if __DECLARESTRUCT
void rage::psoRscStructArrayTableData::DeclareStruct( datTypeStruct& s )
{
	SSTRUCT_BEGIN(psoRscStructArrayTableData)
		SSTRUCT_FIELD_AS(psoRscStructArrayTableData, m_NameHash, u32)
		SSTRUCT_FIELD(psoRscStructArrayTableData, m_Size)
		SSTRUCT_FIELD(psoRscStructArrayTableData, m_Data)
	SSTRUCT_END(psoRscStructArrayTableData)
}
#endif // __DECLARESTRUCT

void rage::psoRscStructArrayTableData::Place( psoRscStructArrayTableData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size )
{
	::new(ptr) psoRscStructArrayTableData(rsc, swap, size);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
/// All the backwards compatibility code goes here:

void psoIffToRscConversion::ConvertToNewFormat(const psoSchemaMemberData& oldMember, const psoSchemaStructureData& oldStructure, psoRscMemberSchemaData& newMember, char sourcePlatform)
{
	newMember.m_NameHash.SetHash( oldMember.m_NameHash );
	newMember.m_Offset = oldMember.GetOffset();
	psoType t = psoType::ConvertParserTypeToPsoType(
		(parMemberType::Enum)oldMember.m_Type, 
		oldMember.GetSubType(), 
		sysIs64Bit(sourcePlatform));
	newMember.SetType(t);
	switch(psoRscMemberSchemaData::GetMemberDataType(t))
	{
	case psoRscMemberSchemaData::MEM_GENERIC:
		break;
	case psoRscMemberSchemaData::MEM_ENUM_OR_STRUCT:
		newMember.EnumOrStruct.m_ReferentHash = oldMember.m_ReferentHash;
		break;
	case psoRscMemberSchemaData::MEM_STRING:
		newMember.String.m_Count = oldMember.m_Count;
		newMember.String.m_NamespaceIndex = (u8)oldMember.GetMemberIndex();
		break;
	case psoRscMemberSchemaData::MEM_ARRAY:
		newMember.Array.SetAlignPowerAndFlags((u8)oldMember.GetAlignmentPower(), 0);
		newMember.Array.m_ContentsIndex = (u16)oldMember.GetMemberIndex();
		newMember.Array.m_Count = oldMember.m_Count;
		break;
	case psoRscMemberSchemaData::MEM_ARRAY_WITH_COUNTER:
		{
			newMember.Array.SetAlignPowerAndFlags((u8)oldMember.GetAlignmentPower(), 0);
			newMember.Array.m_ContentsIndex = (u16)oldMember.GetMemberIndex();
			// find the member with the correct offset... we used to store the offset of the counter in the 'm_Count' field
			u32 counterOffset = oldMember.m_Count;
			for(u16 i = 0; i < oldStructure.m_NumMembers; i++)
			{
				const psoSchemaMemberData& oldCounterMember = oldStructure.GetMember(i);
				if (!psoConstants::IsReserved(oldCounterMember.m_NameHash) && // if its named...
					(oldCounterMember.m_Type == parMemberType::TYPE_UCHAR ||  /// ...and of the right type...
					oldCounterMember.m_Type == parMemberType::TYPE_USHORT ||
					oldCounterMember.m_Type == parMemberType::TYPE_UINT ||
					oldCounterMember.m_Type == parMemberType::TYPE_UINT64) &&
					oldCounterMember.GetOffset() == counterOffset) {		  // ...and has the correct offset
					newMember.Array.m_CounterIndex = i;
				}
			}
		}
		break;
	case psoRscMemberSchemaData::MEM_BITSET:
		{
			newMember.Bitset.m_BitCount = oldMember.m_Count;
			u32 memberIndex = oldMember.GetMemberIndex();
			if (memberIndex == psoSchemaMemberData::NO_MEMBER_INDEX)
			{
				newMember.Bitset.m_EnumHash = 0;
			}
			else
			{
				newMember.Bitset.m_EnumHash = oldStructure.GetMember(oldMember.GetMemberIndex()).m_ReferentHash;
			}
		}
		break;
	case psoRscMemberSchemaData::MEM_MAP:
		newMember.Map.m_KeyIndex = oldMember.m_KeyIndex;
		newMember.Map.m_ValueIndex = oldMember.m_ValueIndex;
		break;
	}
}

void psoIffToRscConversion::ConvertToNewFormat(const psoSchemaStructureData& oldStructure, atLiteralHashValue nameHash, psoRscStructSchemaData& newStructure, char sourcePlatform)
{
	newStructure.m_NameHash = nameHash;
	newStructure.m_Signature = 0;
	newStructure.m_Flags = oldStructure.m_Flags;
	newStructure.m_Size = oldStructure.m_Size;
	newStructure.m_MajorVersion = oldStructure.m_MajorVersion;
	newStructure.m_NumMembers = oldStructure.m_NumMembers;

	size_t maxAlign = 1;

	if (newStructure.m_NumMembers)
	{
		newStructure.m_Members.NewArrayNoCookie(newStructure.m_NumMembers);

		for(int i = 0; i < newStructure.m_NumMembers; i++)
		{
			ConvertToNewFormat(oldStructure.GetMember(i), oldStructure, newStructure.m_Members[i], sourcePlatform);
			maxAlign = Max(maxAlign, newStructure.m_Members[i].GetType().GetAlign());			
		}
											   
		// The new format only needs one entry for every bitset, the old format needed two. 
		// We copied both of them up above, so delete all the old enum members that we don't 
		// need anymore, so that when its time to compute signatures we get the right number
		// of members.
		bool* entriesToDelete = Alloca(bool, oldStructure.m_NumMembers);

		for(int i = 0; i < oldStructure.m_NumMembers; i++)
		{
			entriesToDelete[i] = false;
		}

		bool deleteAny = false;
		for(int i = 0; i < oldStructure.m_NumMembers; i++)
		{
			const psoSchemaMemberData& oldMember = oldStructure.GetMember(i);
			if (oldMember.m_Type == parMemberType::TYPE_BITSET)
			{
				int enumIndex = oldMember.GetMemberIndex();
				if (enumIndex != psoSchemaMemberData::NO_MEMBER_INDEX)
				{
					const psoSchemaMemberData& enumMember = oldStructure.GetMember(enumIndex);
					if (psoConstants::IsAnonymous(enumMember.m_NameHash) && enumMember.m_Type == parMemberType::TYPE_ENUM)
					{
						entriesToDelete[enumIndex] = true;
						deleteAny = true;
					}
				}
			}
		}

		if (deleteAny)
		{
			int src = 0;
			int dest = 0;
			int end = oldStructure.m_NumMembers;
			u16* remapTable = Alloca(u16, oldStructure.m_NumMembers);
			while(src < end)
			{
				remapTable[src] = (u16)dest;
				if (!entriesToDelete[src])
				{
					newStructure.m_Members.GetPtr()[dest] = newStructure.m_Members.GetPtr()[src];
					++dest;
				}
				++src;
			}
			newStructure.m_NumMembers = (u16)dest;

			for(int i = 0; i < newStructure.m_NumMembers; i++)
			{
				psoRscMemberSchemaData& newMember = newStructure.m_Members[i];
				switch(psoRscMemberSchemaData::GetMemberDataType(newMember.GetType()))
				{
				case psoRscMemberSchemaData::MEM_ARRAY:
					newMember.Array.m_ContentsIndex = remapTable[newMember.Array.m_ContentsIndex];
					break;
				case psoRscMemberSchemaData::MEM_ARRAY_WITH_COUNTER:
					newMember.Array.m_ContentsIndex = remapTable[newMember.Array.m_ContentsIndex];
					newMember.Array.m_CounterIndex= remapTable[newMember.Array.m_CounterIndex];
					break;
				case psoRscMemberSchemaData::MEM_MAP:
					newMember.Map.m_KeyIndex = remapTable[newMember.Map.m_KeyIndex];
					newMember.Map.m_ValueIndex = remapTable[newMember.Map.m_ValueIndex];
					break;
				default:
					break;
				}
			}
		}
		
	}
	newStructure.m_AlignPower = psoRscMemberSchemaData::FindAlignPower(maxAlign);
}

void psoIffToRscConversion::ConvertToNewFormat(const psoSchemaEnumData& oldEnum, atLiteralHashValue nameHash, psoRscEnumSchemaData& newEnum)
{
	newEnum.m_NameHash = nameHash;
	newEnum.m_Signature = 0;
	newEnum.m_NumValues = oldEnum.m_NumEnums;
	newEnum.m_Flags = oldEnum.m_Flags;

	newEnum.m_Values.NewArrayNoCookie(newEnum.m_NumValues);
	for(int i = 0; i < newEnum.m_NumValues; i++)
	{
		newEnum.m_Values[i].m_NameHash = atLiteralHashValue(oldEnum.GetEnum(i).m_NameHash);
		newEnum.m_Values[i].m_Value = oldEnum.GetEnum(i).m_Value;
	}
}

void psoIffToRscConversion::ConvertToNewFormat(const psoSchemaCatalogData& schemaCatalog, psoResourceData& newData, char sourcePlatform)
{
	const char* schemaDataStart = reinterpret_cast<const char*>(&schemaCatalog);

	u16 numStructSchemas = 0;
	u16 numEnumSchemas = 0;
	for(u32 i = 0; i < schemaCatalog.m_NumTypes; i++)
	{
		const psoTypeTableData& typeData = schemaCatalog.GetType(i);
		const psoSchemaStructureData* oldStructureSchema = reinterpret_cast<const psoSchemaStructureData*>(schemaDataStart + typeData.m_Offset);
		switch(oldStructureSchema->m_Type)
		{
		case psoConstants::ST_STRUCTURE:
			numStructSchemas++;
			break;
		case psoConstants::ST_ENUM:
			numEnumSchemas++;
			break;
		default:
			parAssertf(0, "Unknown type %d in type table", oldStructureSchema->m_Type);
		}
	}

	newData.m_NumStructSchemas = numStructSchemas;
	if (numStructSchemas > 0)
	{
		newData.m_StructSchemaTable.NewArrayNoCookie(numStructSchemas);
	}

	newData.m_NumEnumSchemas = numEnumSchemas;
	if (numEnumSchemas > 0)
	{
		newData.m_EnumSchemaTable.NewArrayNoCookie(numEnumSchemas);
	}

	int enumIndex = 0;
	int structIndex = 0;
	for(u32 i = 0; i < schemaCatalog.m_NumTypes; i++)
	{
		const psoTypeTableData& typeData = schemaCatalog.GetType(i);
		atLiteralHashValue typeHash(typeData.m_TypeHash);

		const psoSchemaStructureData* oldStructureSchema = reinterpret_cast<const psoSchemaStructureData*>(schemaDataStart + typeData.m_Offset);
		switch(oldStructureSchema->m_Type)
		{
		case psoConstants::ST_STRUCTURE:
			{
				psoRscStructSchemaData& destStruct = newData.m_StructSchemaTable[structIndex];
				psoIffToRscConversion::ConvertToNewFormat(*oldStructureSchema, typeHash, destStruct, sourcePlatform);
				structIndex++;
			}
			break;
		case psoConstants::ST_ENUM:
			{
				const psoSchemaEnumData* oldEnumSchema = reinterpret_cast<const psoSchemaEnumData*>(oldStructureSchema);
				psoRscEnumSchemaData& destEnum = newData.m_EnumSchemaTable[enumIndex];
				psoIffToRscConversion::ConvertToNewFormat(*oldEnumSchema, typeHash, destEnum);
				enumIndex++;
			}
			break;
		default:
			parAssertf(0, "Unknown type %d in type table", oldStructureSchema->m_Type);
		}
	}
}
