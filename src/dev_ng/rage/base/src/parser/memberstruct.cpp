// 
// parser/memberstruct.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "memberstruct.h"

#include "manager.h"
#include "optimisations.h"

#include "bank/bank.h"
#include "parsercore/attribute.h"
#include "system/bit.h"

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberStruct);

u16 g_TraitTable[] = {
	//SUBTYPE_STRUCTURE
	1 << parMemberStruct::TRAIT_OWNER |
	1 << parMemberStruct::TRAIT_PARSABLE_DATA |
	0,

	//SUBTYPE_EXTERNAL_NAMED_POINTER
	1 << parMemberStruct::TRAIT_EXTERNAL_POINTER |
	0,

	//SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL
	1 << parMemberStruct::TRAIT_EXTERNAL_POINTER |
	0,

	//SUBTYPE_POINTER
	1 << parMemberStruct::TRAIT_CAN_DERIVE |
	1 << parMemberStruct::TRAIT_OWNER |
	1 << parMemberStruct::TRAIT_PARSABLE_DATA |
	1 << parMemberStruct::TRAIT_OWNER_POINTER |
	0,

	//SUBTYPE_SIMPLE_POINTER
	1 << parMemberStruct::TRAIT_OWNER |
	1 << parMemberStruct::TRAIT_PARSABLE_DATA |
	1 << parMemberStruct::TRAIT_OWNER_POINTER |
	0,
};

bool parMemberStruct::GetTrait(SubType subtype, SubTypeTrait trait)
{
	return BIT_ISSET(trait, g_TraitTable[subtype]);
}


void parMemberStruct::Data::Init()
{
	StdInit();
#if __BANK
	m_Description = NULL;
#endif
	m_StructurePtr = NULL;
	m_NameToPtrCB = NULL;
	m_PtrToNameCB = NULL;
	m_UnknownTypeCB = NULL;
}

parStructure* parMemberStruct::GetBaseStructure() const
{
	parAssertf(GetData()->m_StructurePtr, "All parMemberStructs must have a structure pointer");
	return GetData()->m_StructurePtr;
}

parStructure* parMemberStruct::GetConcreteStructure(parConstPtrToStructure structureAddress) const
{
	parStructure* str = GetBaseStructure();
	parAssertf(str, "Base structure is missing - was it registered?");

	if (IsParsableData() && structureAddress != NULL)
	{
		if (!CanDerive())
		{
#if __ASSERT
			parStructure* concreteType = str->GetConcreteStructure(structureAddress);
			// concreteType can be NULL or incorrect in some unusual but valid circumstances.
			// For example when dealing with an instance of an external structure, if the instance
			// was a member of an array we don't add each of the array elements to the address-to-type map
			// since we know they are all the same type (and all the same as the declared type of the array element)
			// Likewise if a structure begins with a nested structure, and we're trying to get the concrete type of the 
			// nested structure we'll check the map and get the parent structure's type out. This is OK since by being
			// a nested structure we already know what the concrete type is.
			parAssertf(!concreteType || concreteType->GetFlags().IsSet(parStructure::IS_EXTERNAL) || concreteType == str, "The type of object being pointed to is %s and it should be %s (and not a subclass)", concreteType->GetName(), str->GetName());
#endif
			return str;
		}
		else
		{
			parStructure* ptrStruct = str->GetConcreteStructure(structureAddress);
			if (parVerifyf(ptrStruct, "Couldn't find structure metadata for object %s. Is the type registered?", GetName()))
			{
				if (!ptrStruct->IsSubclassOf(str))
				{
					parErrorf("The type of object being pointed to, %s, isn't a subclass of %s", ptrStruct->GetName(), str->GetName());
					return str;
				}
			}
			return ptrStruct;
		}
	}

	return str;
}

/*
parStructure* parMemberStruct::GetConcreteStructureFromData(const void* base) const
{
	parStructure* str = GetBaseStructure();
	parAssertf(str);

	if (IsPointer()) {
		// follow the pointer (could be NULL)
		void* ptr = GetMemberFromStruct<void*>(base);
		if (ptr) {
			parStructure* ptrStruct = str->GetConcreteStructure(ptr);
			parAssertf(ptrStruct, "Couldn't find structure metadata for object. Is the type registered?");
			if (CanDerive()) {
				parAssertf(ptrStruct->IsSubclassOf(str), "The type of object being pointed to, %s, isn't a subclass of %s", ptrStruct->GetName(), str->GetName());
			}
			else {
				parAssertf(ptrStruct == str, "The type of object being pointed to is %s and it should be %s", ptrStruct->GetName(), str->GetName());
			}
			return ptrStruct;
		}
		else {
			return NULL;
		}
	}
	return str;
}
*/

parPtrToStructure parMemberStruct::GetObjAddr(parPtrToStructure structAddr) const
{
	using namespace parMemberStructSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_STRUCTURE:
		return reinterpret_cast<parPtrToStructure>(GetPointerToMember(structAddr));
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
		return GetMemberFromStruct<parPtrToStructure>(structAddr);
	}
	return NULL;
}

parConstPtrToStructure parMemberStruct::GetObjAddr(parConstPtrToStructure structAddr) const
{
	using namespace parMemberStructSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_STRUCTURE:
		return reinterpret_cast<parConstPtrToStructure>(GetPointerToMember(structAddr));
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
		return GetMemberFromStruct<parConstPtrToStructure>(structAddr);
	}
	return NULL;
}

void parMemberStruct::SetObjAddr(parPtrToStructure containingStructureAddr, parPtrToStructure newPtrValue) const
{
	FastAssert(IsOwnerPointer());
	GetMemberFromStruct<parPtrToStructure>(containingStructureAddr) = newPtrValue;
}

void parMemberStruct::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	using namespace parMemberStructSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_STRUCTURE:
		{
			parStructure* baseStruct = GetBaseStructure();
			if (!parVerifyf(baseStruct, "Couldn't find base structure pointer"))
			{
				return;
			}
			baseStruct->ReadMembersFromTree(node, GetObjAddr(structAddr));
		}
		break;
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_POINTER:
		{
			parPtrToStructure newObj = GetObjAddr(structAddr);
			parStructure* concreteStructure = GetBaseStructure();
			if (newObj == NULL) {
				newObj = CreateNewData(node, 0x0, &concreteStructure);
				SetObjAddr(structAddr, newObj);
			}
			else if (CanDerive())
			{
				// If the object already existed, figure out what the derived type was
				concreteStructure = GetConcreteStructure(newObj);
			}
			if (newObj != NULL) {
				concreteStructure->ReadMembersFromTree(node, newObj);
			}
		}
		break;
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
		{
			void*& newObj = GetMemberFromStruct<void*>(structAddr);
			parAttribute* attr = node->GetElement().FindAttribute("ref");
			if (attr && (GetSubtype() == SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL || strcmp(attr->GetStringValue(), "NULL"))) { // if attr exists and is non-NULL
				newObj = GetData()->m_NameToPtrCB(attr->GetStringValue());
			}
			else {
				newObj = NULL;
			}
		}
		break;
	}
}

parPtrToStructure parMemberStruct::CreateNewData(parTreeNode* node, u32 typeHash, parStructure** outCreatedStruct) const 
{
	using namespace parMemberStructSubType;
	if (outCreatedStruct)
	{
		*outCreatedStruct = GetBaseStructure(); // catch early returns
	}
	switch (GetSubtype())
	{
	case SUBTYPE_STRUCTURE:
		parErrorf("Can't create non-pointer structures, shouldn't get here.");
		break;
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
		parErrorf("Can't create external named pointers, shouldn't get here.");
		break;
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_POINTER:
		{
			parAttribute* attr = NULL;
			const char* typeName = NULL;
			if (node)
			{
				attr = node->GetElement().FindAttribute("type");
				if (attr)
				{
					if (!strcmp(attr->GetStringValue(), "NULL")) {
						return NULL;
					}
					else
					{
						// Ignore any passed-in value for typeHash, use the computed one instead
						typeName = attr->GetStringValue();
						typeHash = atLiteralStringHash(typeName);
					}
				}
			}

			parStructure* baseStr = GetBaseStructure();
			parAssertf(baseStr, "Couldn't find base structure");

			parStructure* derivedStr = NULL;

			derivedStr = baseStr; // default, create pointer type

			// Need to set derivedStr based on what's in the type= attribute
			if (GetSubtype() == SUBTYPE_SIMPLE_POINTER)
			{
				// if a type attribute is specified, make sure it's the right type
#if __ASSERT
				if (attr)
				{
					parStructure* specifiedStr = PARSER.FindStructure(typeHash);
					parAssertf(specifiedStr == baseStr, "File type (%s) must match pointer type (%s)", typeName, baseStr->GetName());
				}
#endif
			}
			else // SUBTYPE_POINTER
			{
				if (node && !attr)
				{
					parErrorf("Type attribute is missing for derivable pointer %s of type %s", GetName(), baseStr->GetName());
				}

				derivedStr = PARSER.FindStructure(typeHash);

				if (!derivedStr) // if can't find the type from the type="foo" attribute
				{
					// one final case, if there's an unknown type callback use that to create the type
					if (GetData()->m_UnknownTypeCB && node) {
						return reinterpret_cast<parPtrToStructure>(GetData()->m_UnknownTypeCB(node));
					}
					if (PARSER.Settings().GetFlag(parSettings::LOAD_UNKNOWN_TYPES) && !derivedStr)
					{
						// do nothing
					}
					else
					{
						if (!derivedStr)
						{
							parErrorf("Couldn't find a registered class with name specified in the file (\"%s\") hash=0x%x", attr ? attr->GetStringValue() : "", typeHash);
						}
					}
					derivedStr = baseStr;
				}

				if (!parVerifyf(derivedStr->IsSubclassOf(baseStr), "File type %s is not derived from pointer type %s", derivedStr->GetName(), baseStr->GetName()))
				{
					derivedStr = baseStr;
				}
			}

			if (derivedStr->IsConstructible())
			{
				// Now derivedStr points to the type we want to create, actually do the creation.
				parPtrToStructure newStructure = derivedStr->Create();
				parAssertf(newStructure != NULL, "Creation failed for a new object of type %s", derivedStr->GetName());
				if (outCreatedStruct)
				{
					*outCreatedStruct = derivedStr;
				}
				return newStructure;
			}
			else
			{
				parErrorf("Can't create an instance of type %s because it's declared non-constructible.", derivedStr->GetName());
				return NULL;
			}
		}
		/*NOTREACHED*/
		break;
	}
	return NULL;
}

void parMemberStruct::SetFromString(parPtrToStructure structAddr, const char* name) const
{
	using namespace parMemberStructSubType;
	parAssertf(IsExternalPointer(), "Invalid subtype for SetFromString");

	if (name)
	{  
		parPtrToStructure newObj = NULL;
		switch(GetSubtype())
		{
		case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
			newObj = reinterpret_cast<parPtrToStructure>(GetData()->m_NameToPtrCB(name));
			break;
		case SUBTYPE_EXTERNAL_NAMED_POINTER:
			if (!strcmp(name, "NULL"))
			{					 
				newObj = NULL;
			}
			else
			{
				newObj = reinterpret_cast<parPtrToStructure>(GetData()->m_NameToPtrCB(name));
			}
			break;
		default:
			parErrorf("Invalid subtype %d", GetSubtype());
		}	 
		SetObjAddr(structAddr, newObj);
	}
}

size_t rage::parMemberStruct::GetSize() const
{
	using namespace parMemberStructSubType;
	switch (GetSubtype())
	{
	case SUBTYPE_STRUCTURE:
		return GetBaseStructure()->GetSize();
	case SUBTYPE_POINTER:
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
		return sizeof(char*);
	}
	return 0;
}

size_t parMemberStruct::FindAlign() const
{
	using namespace parMemberStructSubType;
	switch (GetSubtype())
	{
	case SUBTYPE_STRUCTURE:
		return GetBaseStructure()->FindAlign();
	case SUBTYPE_POINTER:
	case SUBTYPE_SIMPLE_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER:
	case SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
		return __alignof(char*);
	}
	return 0;
}