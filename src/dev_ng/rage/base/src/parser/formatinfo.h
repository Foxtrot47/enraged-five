// 
// parser/formatinfo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_FORMATINFO_H
#define PARSER_FORMATINFO_H

#include "tree.h"

#include "atl/delegate.h"
#include "parsercore/settings.h"
#include "parsercore/stream.h"
#include "system/endian.h"

namespace rage {

typedef atDelegate<bool ( u32 )>					parFileHeaderTestFn;
typedef atDelegate<parStreamIn* ( fiStream*, const parSettings& settings ) >		parStreamInFactoryFn;
typedef atDelegate<parStreamOut* ( fiStream*, const parSettings& settings ) >	parStreamOutFactoryFn;
typedef atDelegate<parTree* ( fiStream*, const parSettings& settings, u32 ) >	parTreeLoaderFn;
typedef atDelegate<bool ( fiStream*, const parSettings& settings, parTreeNode* ) >	parTreeSaverFn;

// PURPOSE: parInputFormatInfo is a structure that describes a loadable format 
class parInputFormatInfo {
public:
	parInputFormatInfo();

	// RETURNS: true if a file beginning with the u32 argument can be read by this format
	parFileHeaderTestFn		m_FileHeaderTest;

	// RETURNS: a new parStream sub that wraps an fiStream*
	parStreamInFactoryFn	m_StreamFactory; 

	// PURPOSE: Given an fiStream opened for reading, and a u32 file header,
	// reads the file and returns a parTree.
	parTreeLoaderFn			m_TreeLoader;

	// PURPOSE: The name of the file format.
	const char*				m_FormatName;


	// PURPOSE: Given a parStreamIn subclass T, and an fiStream* stream, returns a new instance of T that wraps stream.
	template<typename T> static parStreamIn*	SimpleFactory(fiStream* stream, const parSettings& settings);

	// PURPOSE: Reads from the stream, creating a tree, using the parStreamIn "push" method of reading.
	template<parStreamIn* _StreamFactory(fiStream*, const parSettings&)> static parTree*		GenericStreamTreeLoader(fiStream* stream, const parSettings& settings, u32 firstBytes);

	// PURPOSE: Checks that the header bytes exactly match the u32 template argument.
	template<u32 Val> static bool				SimpleHeaderTest(u32 firstBytes);
};

// PURPOSE: parOutputFormatInfo is a structure that describes a savable format.
class parOutputFormatInfo {
public:
	parOutputFormatInfo();

	// RETURNS: a new parStream* that wraps an fiStream*
	parStreamOutFactoryFn		m_StreamFactory; 

	// PURPOSE: Given an fiStream opened for writing and a parTree,
	// saves the parTree to the stream and returns true on success.
	parTreeSaverFn				m_TreeSaver;

	// PURPOSE: The name of the file format.
	const char*					m_FormatName;

	// PURPOSE: Given a parStreamOut subclass T and an fiStream* stream, returns a new instance of T that wraps stream.
	template<typename T> static parStreamOut*	SimpleFactory(fiStream* stream, const parSettings& settings);

	// PURPOSE: Uses a generic tree traversal to save to the stream using the parStreamOut write methods.
	template<parStreamOut* _StreamFactory(fiStream*, const parSettings&)> static bool			GenericStreamTreeSaver(fiStream* stream, const parSettings& settings, parTreeNode* tree);
};

template<typename T>  
inline parStreamIn* parInputFormatInfo::SimpleFactory(fiStream* stream, const parSettings& settings)
{
	T* pstream = rage_new T(stream, settings);
	return pstream;
}

template<parStreamIn* _StreamFactory(fiStream*, const parSettings&)>
inline parTree* parInputFormatInfo::GenericStreamTreeLoader(fiStream* stream, const parSettings& settings, u32 firstBytes)
{
	parStreamIn* parstream = _StreamFactory(stream, settings);
	parstream->SetHeaderBytes(firstBytes);
	parTree* tree = parTree::LoadFromStream(parstream);
	delete parstream;
	return tree;
}

template<u32 Val>
inline bool parInputFormatInfo::SimpleHeaderTest(u32 firstBytes)
{
	return Val == sysEndian::NtoL(firstBytes); // Val is little endian
}

template<typename T>
inline parStreamOut* parOutputFormatInfo::SimpleFactory(fiStream* stream, const parSettings& settings)
{
	T* pstream = rage_new T(stream, settings);
	return pstream;
}

template<parStreamOut* _StreamFactory(fiStream*, const parSettings&)>
inline bool parOutputFormatInfo::GenericStreamTreeSaver(fiStream* stream, const parSettings& settings, parTreeNode* tree)
{
	parStreamOut* parstream = _StreamFactory(stream, settings);
	bool result = parTree::SaveToStream(parstream, tree);
	delete parstream;
	return result;
}

} // namespace rage

#endif // PARSER_FORMATINFO_H
