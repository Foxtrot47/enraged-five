// 
// parser/visitorutils.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORUTILS_H 
#define PARSER_VISITORUTILS_H 

#include "visitor.h"

#include "atl/delegate.h"
#include "system/miniheap.h"

namespace rage {

	// PURPOSE: Given a query type, count the number of instances of the type within this object
	//			and any contained objects
	// PARAMS:
	//			_Query - the type of object to search for
	//			_Container - The type of the top level object where the search starts
	//			obj - A reference to the top level object to search in
	// NOTES:
	//			You can only query for parsable _structures_ (you can't count the number of ints)
	//			This function follows owning pointers to the referent objects and considers all subclasses of _Query
	//			to be a match for _Query
	template<typename _Query, typename _Container> 
	int parCountInstances(_Container& obj);

	// PURPOSE: Call a delegate function for each instance of a given type inside an object and any contained objects
	// PARAMS:
	//			_Query - the type of object to search for
	//			_Container - The type of the top level object where the search starts
	//			obj - A reference to the top level object to search in
	//			callback - The atDelegate to call on each instance of a _Query object
	// NOTES:
	//			This function follows owning pointers to the referent objects and considers all subclasses of _Query
	//			to be a match for _Query
	template<typename _Query, typename _Container> 
	void parForEachInstance(_Container& obj, atDelegate<void (_Query&)> callback);

	// PURPOSE: Find the total size required for this structure, including all contained objects
	template<typename _Container>
	size_t parFindTotalSize(_Container& obj);

	// PURPOSE: Create and return a copy of this structure, including all contained objects
	// PARAMS:
	//			_Container - the type of the top level object to copy
	//			obj - the top level object to copy
	//			contiguous - if true the new object will be at the start of a contiguous block of memory
	//			numContigBytesOut - If non-NULL and contiguous is true - will hold number of contiguous bytes that were allocated when the function returns.
	//			pObjToCopyInto - if non-NULL, this is the destination object we will copy into, instead of allocating a new object
	// RETURNS: A copy of obj and all of the contained structures in obj.
	// NOTES:	Calls constructors on the new objects, but does not call copy constructors or op=
	template<typename _Container>
	_Container& parDeepCopy(_Container& obj, bool contiguous = false, size_t* numContigBytesOut = NULL, _Container* pObjToCopyInto = NULL);

	// PURPOSE: Deletes a member variable (calling the normal destructor if appropriate)
	// PARAMS:
	//			object - the address of the object that contains the member variable
	//			metadata - description of the member variable to delete
	void		parDeleteMember(parPtrToStructure object, parMember& metadata);

	// PURPOSE: Deletes an object tree (does not invoke regular destructors)
	// PARAMS:
	//			_Container - the type of the top level object to delete
	//			obj - the top level object to delete
	template<typename _Container>
	void		parDeleteAllContents(_Container& obj);

	struct parFindMemberData
	{
		parFindMemberData() : m_ContainingStructureAddress(NULL), m_ContainingStructureType(NULL), m_MemberAddress(NULL), m_MemberType(NULL) {}
		parPtrToStructure	m_ContainingStructureAddress;
		parStructure*		m_ContainingStructureType;
		
		parPtrToMember	m_MemberAddress;
		parMember*		m_MemberType;
	};

	// PURPOSE: Finds a member from a URI-style path
	// PARAMS: 
	//			object - The root object to start the search in
	//			path - The path to use.
	//			outType - The parStructure that contains the found member
	//			outMember - The parMember that describes the found member
	//			outAddr - The address of the found member
	// RETURNS: 
	//			true if the member variable was found, false otherwise.
	// NOTES:
	//			The path format is URI style.
	//			X/Y - Finds the Y member of X, '/' also follows pointers
	//			X/N - If X is an array, finds the Nth member. N must be an integer
	//			So "MyTable/7/2/Position" is like MyTable[7][2].Position in C/C++.
	bool parFindMemberFromUriPath(parPtrToStructure object, parStructure& rootType, const char* path, parFindMemberData& outData);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	// PURPOSE: Compares the in-memory layout of an object to the memory layout specified by another parManager
	// PARAMS:
	//			object - the address of the root object to start comparing, or NULL to compare just the specified structures
	//			structure - the parStructure that describes the type of object
	//			thisManager - The manager that has all the type info for the object we're checking (and its subobjects)
	//			otherManager - The manager to compare against
	void parCompareMemoryLayouts(parPtrToStructure objectData, parStructure* structure, 
		parManager* thisManager, const char* thisDescrption,
		parManager* otherManager, const char* otherDescription,
		bool verbose);
#endif


	// PURPOSE: parNonGenericVisitor is a used as an optimization of parInstanceVisitor. 
	// Use it if you're not interested in overriding any of the 'generic' versions of 
	// the member functions (e.g. SimpleMember, StringMember, AnyMember, etc). 
	// NOTES:It will save some function calls by not calling these generic functions
	// from the specific members.
	class parNonGenericVisitor : public parInstanceVisitor
	{
	public:
		virtual void AnyMember			(parPtrToMember, parMember&)					{ parErrorf("This should never be called. Problem in iteration?"); }
		virtual void SimpleMember		(parPtrToMember, parMemberSimple&)				{ parErrorf("This should never be called. Problem in iteration?"); }
		virtual void VectorMember		(parPtrToMember, parMemberVector&)				{ parErrorf("This should never be called. Problem in iteration?"); }
		virtual void MatrixMember		(parPtrToMember, parMemberMatrix&)				{ parErrorf("This should never be called. Problem in iteration?"); }
		virtual void StringMember		(parPtrToMember, const char*, parMemberString&)	{ parErrorf("This should never be called. Problem in iteration?"); }
		virtual void WideStringMember	(parPtrToMember, const char16*, parMemberString&){ parErrorf("This should never be called. Problem in iteration?"); }

		virtual void BoolMember		(bool& ,	parMemberSimple& ) {}
		virtual void CharMember		(s8& ,		parMemberSimple& ) {}
		virtual void UCharMember	(u8& ,		parMemberSimple& ) {}
		virtual void ShortMember	(s16& ,		parMemberSimple& ) {}
		virtual void UShortMember	(u16& ,		parMemberSimple& ) {}
		virtual void IntMember		(s32& ,		parMemberSimple& ) {}
		virtual void IntMember		(s64& ,		parMemberSimple& ) {}
		virtual void UIntMember		(u32& ,		parMemberSimple& ) {}
		virtual void UIntMember		(u64& ,		parMemberSimple& ) {}
		virtual void FloatMember	(float& ,	parMemberSimple& ) {}
		virtual void Float16Member	(Float16& ,	parMemberSimple& ) {}
		virtual void ScalarVMember	(ScalarV&,	parMemberSimple& ) {}
		virtual void BoolVMember	(BoolV&,	parMemberSimple& ) {}
		virtual void DoubleMember	(double&,	parMemberSimple& ) {}

		virtual void Vector2Member	(Vector2& ,	parMemberVector& ) {}
		virtual void Vector3Member	(Vector3& ,	parMemberVector& ) {}
		virtual void Vector4Member	(Vector4& ,	parMemberVector& ) {}
		virtual void VecBoolVMember	(VecBoolV&, parMemberVector& ) {}
		virtual void Vec2VMember	(Vec2V& ,	parMemberVector& ) {}
		virtual void Vec3VMember	(Vec3V& ,	parMemberVector& ) {}
		virtual void Vec4VMember	(Vec4V& ,	parMemberVector& ) {}

		virtual void Matrix34Member	(Matrix34& ,	parMemberMatrix& ) {}
		virtual void Matrix44Member	(Matrix44& ,	parMemberMatrix& ) {}
		virtual void Mat33VMember	(Mat33V& ,		parMemberMatrix& ) {}
		virtual void Mat34VMember	(Mat34V& ,		parMemberMatrix& ) {}
		virtual void Mat44VMember	(Mat44V& ,		parMemberMatrix& ) {}

		virtual void EnumMember		(int& ,		parMemberEnum& ) {}
		virtual void BitsetMember	(parPtrToMember , parPtrToArray , size_t, parMemberBitset& ) {}

		virtual void MemberStringMember		(char* ,		parMemberString& )	{}
		virtual void PointerStringMember	(char*& ,		parMemberString& )	{}
		virtual void ConstStringMember		(ConstString& , parMemberString& )	{}
		virtual void AtStringMember			(atString& ,	parMemberString& )	{}
#if !__FINAL
		virtual void AtNonFinalHashStringMember(atHashString&, parMemberString& )  {}
#endif	//	!__FINAL
		virtual void AtFinalHashStringMember(atFinalHashString&, parMemberString& )  {}
		virtual void AtHashValueMember		(atHashValue&,	parMemberString& )	{}
		virtual void AtPartialHashValueMember	(u32&,	parMemberString& )	{}
		virtual void AtNamespacedHashStringMember(atNamespacedHashStringBase& , parMemberString& ) {}
		virtual void AtNamespacedHashValueMember(atNamespacedHashValueBase& , parMemberString& ) {}

		virtual void WideMemberStringMember	(char16* , parMemberString& )			{}
		virtual void WidePointerStringMember(char16*& , parMemberString& )			{}
		virtual void AtWideStringMember		(atWideString& , parMemberString& )	{}

		virtual void ExternalPointerMember	(void*& ,		parMemberStruct& ) {}

		virtual bool BeginArrayMember	(parPtrToMember, parPtrToArray , size_t, parMemberArray&)	{ return true; }
		virtual bool BeginStructMember	(parPtrToStructure, parMemberStruct& )				{ return true; }
        virtual bool BeginMapMember	    (parPtrToStructure,	parMemberMap&)					    { return true; }
	};

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	/// parMemberPlacementVisitor

	// Say you have a block of memory that's supposed to be some instance of a structure. Normally the 
	// structure's constructor would get run on that block of memory, and call the constructors for some of the members.
	// For example any atStrings in there would get their atString() constructor called, NULL-ing the string.
	// But if you're working with external structures and raw blocks of memory, you don't have a class constructor to call 
	// so this is there in its place, looping over all the member variables and calling their default constructors.
	// This is a lower level operation than what parInitVisitor does, though it is similar.
	//		The parInitVisitor initializes a properly constructed object - making sure that anything the constructor didn't initialize gets set.
	//		The parMemberPlacementVisitor initializes a generic block of memory, making it look like a properly constructed object
	class parMemberPlacementVisitor : public parInstanceVisitor
	{
	public:
		virtual bool BeginArrayMember( parPtrToMember ptrToMember, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata );
		virtual bool BeginPointerMember( parPtrToStructure& /*ptrRef*/, parMemberStruct& /*metadata*/ );
		virtual bool BeginStructMember( parPtrToStructure ptrToStruct, parMemberStruct& metadata );
		virtual void BitsetMember( parPtrToMember ptrToMember, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata );
		virtual void ConstStringMember( ConstString& stringData, parMemberString& /*metadata*/ );
		virtual void AtStringMember( atString& stringData, parMemberString& /*metadata*/ );
		virtual void MemberStringMember(char* stringData, parMemberString& metadata);
		virtual void WideMemberStringMember(char16* stringData, parMemberString& metadata);
#if !__FINAL
		virtual void AtNonFinalHashStringMember( atHashString& stringData, parMemberString& /*metadata*/ );
#endif	//	!__FINAL
		virtual void AtFinalHashStringMember( atFinalHashString& stringData, parMemberString& /*metadata*/ );
		virtual void AtHashValueMember( atHashValue& stringData, parMemberString& /*metadata*/ );
		virtual void AtWideStringMember( atWideString& stringData, parMemberString& /*metadata*/ );
		virtual bool BeginMapMember( parPtrToStructure structAddr, parMemberMap& metadata );

		// Helper function for running placement new on all elements in an array
		static void ConstructArrayElements(parPtrToArray arrayPointer, size_t startIndex, size_t endIndex, const parMemberArray& arrayMetadata);
	};




	/////////////////////////////////////////////////////////////////////////////////////////////
	// Implementations below

	// DOM-IGNORE-BEGIN

	class parCountInstancesVisitor : public parNonGenericVisitor
	{
	public:
		explicit parCountInstancesVisitor(parStructure& queryStruct);

		virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata);

		parStructure& m_QueryStruct;
		int m_Count;

	private:
		void operator=(const parCountInstancesVisitor&) {};
	};

	template<typename _Query, typename _Container> 
	int parCountInstances(_Container& obj)
	{
		parCountInstancesVisitor vis(*_Query::parser_GetStaticStructure());
		vis.Visit(obj);
		return vis.m_Count;
	}


	template<typename _Query, typename _Container> 
	void parForEachInstance(_Container& obj, atDelegate<void (_Query&)> callback)
	{
		class ForEachVisitor : public parNonGenericVisitor
		{
		public:
			ForEachVisitor()
			{
				parInstanceVisitor::TypeMask typeMask(false);
				typeMask.Set(parMemberType::TYPE_STRUCT);
				SetMemberTypeMask(typeMask);
			}

			atDelegate<void (_Query&)> m_Callback;
			virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
			{
				if (metadata.GetConcreteStructure(dataPtr)->IsSubclassOf(_Query::parser_GetStaticStructure()))
				{
					_Query* str = reinterpret_cast<_Query*>(dataPtr);
					if (str)
					{
						m_Callback(*str);
					}
				}
				parNonGenericVisitor::VisitStructure(dataPtr, metadata);
			}
		};

		ForEachVisitor vis;
		vis.m_Callback = callback;
		vis.Visit(obj);
	}


	class parFindTotalSizeVisitor : public parInstanceVisitor
	{
	public:
		parFindTotalSizeVisitor();

		virtual bool BeginToplevelStruct(parPtrToStructure dataPtr, parStructure& metadata);
		virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);
		virtual bool BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

		virtual void StringMember(parPtrToMember, const char*, parMemberString&);
		virtual void WideStringMember(parPtrToMember, const char16*, parMemberString&);

		virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);

        virtual bool BeginMapMember (parPtrToStructure structAddr,	parMemberMap& metadata);

		size_t m_Size;
		size_t m_NumAllocs;
	};

	template<typename _Container>
	size_t parFindTotalSize(_Container& obj)
	{
		parFindTotalSizeVisitor vis;
		vis.Visit(obj);
		return vis.m_Size;
	}

	class parDeepCopyVisitor : public parInstanceVisitor
	{
	public:
		parDeepCopyVisitor();

		virtual bool BeginToplevelStruct(parPtrToStructure ptrToStruct, parStructure& metadata);
		virtual bool BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);
		virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);
		virtual void EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);

		virtual bool BeginStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata);
		virtual void EndStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata);

		virtual void StringMember(parPtrToMember ptrToMember, const char* ptrToString, parMemberString& metadata);
		virtual void WideStringMember(parPtrToMember ptrToMember, const char16* ptrToString, parMemberString& metadata);

        virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);

		virtual bool BeginMapMember	(parPtrToStructure structAddr,	parMemberMap& metadata);

		parPtrToStructure GetRootPointer() {return m_BuildingStack[0];}

		// PURPOSE:	Pointer to an existing object we should copy into.
		parPtrToStructure					m_ExistingObjectPtr;

		atFixedArray<parPtrToStructure, 64> m_BuildingStack;
	};

	template<typename _Container>
	_Container& parDeepCopy(_Container& obj, bool contiguous /* = false */, size_t* numContigBytesOut /* = NULL */, _Container* pObjToLoadInto /* = NULL */)
	{
		if (contiguous)
		{
			parFindTotalSizeVisitor vis;
			vis.Visit(obj);
			// reserve enough space for alignment too, assume worst case everything gets 16 byte aligned, so add an addtl. 16b times the number of allocations
			size_t reserveSize = vis.m_Size + 16 * vis.m_NumAllocs;
			char* memBuffer = rage_new char[reserveSize];
			sysMemStartMiniHeap(memBuffer, reserveSize);
			if (numContigBytesOut)
			{
				*numContigBytesOut = reserveSize;
			}
		}
		parDeepCopyVisitor vis;

		// If the user passed in an object, release any memory it has already allocated for its parts.
		if(pObjToLoadInto)
		{
			parAssertf(pObjToLoadInto->parser_GetStructure() == obj.parser_GetStructure(), "Object types need to match, if passing in a preexisting object");
			parDeleteAllContents(*pObjToLoadInto);
		}

		vis.m_ExistingObjectPtr = pObjToLoadInto->parser_GetPointer();

		vis.Visit(obj);
		_Container* newContainer = reinterpret_cast<_Container*>(vis.m_BuildingStack[0]);
		if (contiguous)
		{
			parDebugf1("Miniheap had %" SIZETFMT "d bytes free", sysMemGetFreeMiniHeap());
			sysMemEndMiniHeap();
		}
		return *newContainer;
	}

	class parDeleteAllVisitor : public parInstanceVisitor
	{
	public:
		explicit parDeleteAllVisitor(bool runDestructors);

		virtual void EndArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);
		virtual void EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);

        virtual void EndMapMember (parPtrToStructure structAddr, parMemberMap& metadata);

		virtual bool BeginStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata);
		virtual void EndStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata);

		virtual void PointerStringMember	(char*& stringData,	parMemberString& metadata);			
		virtual void ConstStringMember		(ConstString& stringData, parMemberString& metadata);	
		virtual void AtStringMember			(atString& stringData, parMemberString& metadata);		
#if !__FINAL
		virtual void AtNonFinalHashStringMember(atHashString& stringData, parMemberString& metadata);  
#endif	//	!__FINAL
		virtual void AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& metadata);  

		virtual void WidePointerStringMember(char16*& stringData, parMemberString& metadata);		
		virtual void AtWideStringMember		(atWideString& stringData, parMemberString& metadata);	

		virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);

		static void DestructArrayElements(parPtrToArray arrayPointer, size_t startIndex, size_t endIndex, const parMemberArray& metadata);

		bool m_RunDestructors;
	};

	template<typename _Container>
	void parDeleteAllContents(_Container& obj)
	{
		parDeleteAllVisitor vis(false);
		vis.Visit(obj);
	}

	// DOM-IGNORE-END

} // namespace rage


#endif // PARSER_VISITORUTILS_H 
