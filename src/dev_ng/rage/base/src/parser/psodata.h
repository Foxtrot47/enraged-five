// 
// parser/psodata.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSODATA_H
#define PARSER_PSODATA_H

#include "memberdata.h"
#include "memberstringdata.h"
#include "psofaketypes.h"
#include "psoschema.h"

#include "string/unicode.h"
#include "system/endian.h"

namespace rage {
class psoFile;
class psoStruct;



struct psoStructArrayTableData
{
	u32 m_TypeHash;
	u32 m_Offset;		// In the file, values are stored as an offset
	// This is padding for now, but may be converted to an address or something later
	u32 m_Pad;
	u32	m_Size; // in bytes
};


struct psoStructureMapData
{
	parIffHeader m_Header;
	psoStructId	 m_RootObject;
	u16			 m_NumStructArrays;
	u16			 m_Flags;

	psoStructArrayTableData& GetStructArrayEntry(u32 i) {
		psoStructArrayTableData* table = reinterpret_cast<psoStructArrayTableData*>(this+1); // structarray table always immediately follows the header
		return table[i];
	}
	const psoStructArrayTableData& GetStructArrayEntry(u32 i) const {
		const psoStructArrayTableData* table = reinterpret_cast<const psoStructArrayTableData*>(this+1); // structarray table always immediately follows the header
		return table[i];
	}
};

struct psoStructureData
{
	parIffHeader m_Header;
	char m_Pad[8];
};

class psoStructureMap
{
	friend class psoFile;
public:
	psoStructureMap(psoStructureMapData* data)
		: m_MapHeader(data)
	{
	}

	const psoStructArrayTableData& GetStructArrayEntry(u32 i) const {
		return m_MapHeader->GetStructArrayEntry(i);
	}

	u32 GetNumStructArrays() const { return m_MapHeader->m_NumStructArrays; }

	psoStructId GetRootStructId() const { return m_MapHeader->m_RootObject; }

protected:
	psoStructureMapData* m_MapHeader;
};

namespace psoIffToRscConversion
{
	void ConvertToNewFormat(const psoStructureMapData& map, psoStructureData* startOfInstanceData, psoResourceData& newData, char sourcePlatform);
}

class psoMember
{
public:
	psoMember(psoMemberSchema schema, parPtrToMember data, psoFile* file)
		: m_Schema(schema)
		, m_MemberData(data)
		, m_File(file)
	{
	}

	bool IsValid()
	{
		return m_Schema.IsValid() && m_MemberData;
	}

	psoType GetType() {return m_Schema.GetType(); }

	psoMemberSchema& GetSchema() { return m_Schema; }

	template<typename _Type> _Type& GetDataAs() { return *reinterpret_cast<_Type*>(m_MemberData); }

	parPtrToMember GetRawDataPtr() { return m_MemberData; }

	// Only valid for STRUCTURE types
	psoStructureSchema GetReferentStructure();
	psoStruct GetSubStructure();

	// Only valid for ENUM types
	psoEnumSchema GetReferentEnum();
	bool GetEnumValueAndName(int& outVal, atLiteralHashValue& outName);

	// Only valid for STRING types
	const char* GetStringData();
	const char16* GetWideStringData();

	// Only support basic string types at present TYPE_STRING_MEMBER
	void SetStringData(const char* value);

	psoFile& GetFile() { return *m_File; }

protected:
	psoMemberSchema m_Schema;
	psoFile*	m_File;
	parPtrToMember m_MemberData;

	friend class psoMemberArrayInterface;
};

// Interface class that takes a psoMember which describes an array and provides some array operations for it
class psoMemberArrayInterface
{
public:
	psoMemberArrayInterface(psoStruct& str, psoMember& mem);

	size_t GetArrayCount();
	inline psoMemberSchema GetArrayElementSchema() {return m_ElementSchema;}

	bool IsPodArray();

	parPtrToArray GetArrayContents();

	psoMember GetElement(size_t i);

protected:
	psoMember	m_Member;
	psoStructureSchema m_MembersStructure;
	psoMemberSchema m_ElementSchema;
	size_t		m_ElementSize;
};

// Interface class that takes a psoMember which describes a map and provides some map operations for it
class psoMemberMapInterface
{
public:
	psoMemberMapInterface(psoStruct& str, psoMember& mem);

	size_t GetMapCount();

	inline psoMemberSchema GetKeySchema() { return m_KeySchema; }
	inline psoMemberSchema GetValueSchema() { return m_ValueSchema; }

	psoMember GetKey(size_t i);
	psoMember GetValue(size_t i);

	psoStruct GetKeyValueStruct(size_t i);

protected:
	psoStructId GetIdForElement(size_t i);

	psoMember	m_Member;
	psoMemberSchema m_KeySchema;
	psoMemberSchema m_ValueSchema;
	size_t		m_ElementSize;
};

// Interface class that takes a psoMember which describes a bitset and provides bitset operations for it
class psoMemberBitsetInterface
{
public:
	explicit psoMemberBitsetInterface(psoStruct& str, psoMember& mem);

	size_t GetBitCount();

	bool AreBitsNamed() { return m_Enums.IsValid(); }

	void SetBit(size_t i, bool newValue = true);
	bool GetBit(size_t i);
	
	atLiteralHashValue GetBitName(size_t i);

protected:
	psoMember	m_Member;
	psoEnumSchema m_Enums;
};

class psoStruct
{
public:
	explicit psoStruct(psoFile& file)
		: m_InstanceData(NULL)
		, m_File(&file)
	{}

	psoStruct()
		: m_InstanceData(NULL)
		, m_File(NULL)
	{}


	parPtrToStructure	GetInstanceData() {return m_InstanceData;}

	psoStructureSchema& GetSchema() {return m_Schema;}

	psoMember GetMemberByIndex(int i);
	psoMember GetMemberByName(atLiteralHashValue name);

	psoMember operator[](atLiteralHashValue nameHash) {return GetMemberByName(nameHash); }
	psoMember operator[](const char* name) {return GetMemberByName(atLiteralHashValue(name)); }

	bool IsNull() { return m_InstanceData == NULL; }

	bool IsValid()
	{
		return IsNull() || m_Schema.IsValid(); // NULLs are still valid
	}

	psoFile& GetFile() {return *m_File;}

	void SetInstanceDataPtr(parPtrToStructure newData) { m_InstanceData = newData; }

protected:
	friend class psoFile;
	friend class psoMember;
	friend class psoMemberStruct;
	friend class psoBuilderInstance;

	psoStructureSchema	m_Schema;
	parPtrToStructure	m_InstanceData;
	psoFile*			m_File;
};


inline psoStructId psoStructId::Create( u32 tableIndex, u32 arrayOffset )
{
	psoStructId newId; 
	newId.m_TableIndex = tableIndex+1; 
	newId.m_ArrayOffset = arrayOffset; 
	return newId;
}

} // namespace rage

#endif
