// 
// parser/formatinfo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "formatinfo.h"
#include "manager.h"
#include "optimisations.h"

using namespace rage;

PARSER_OPTIMISATIONS();

parInputFormatInfo::parInputFormatInfo()
: m_FileHeaderTest()
, m_StreamFactory()
, m_TreeLoader()
, m_FormatName(NULL)
{
}

parOutputFormatInfo::parOutputFormatInfo()
: m_StreamFactory()
, m_TreeSaver()
, m_FormatName(NULL)
{
}

