// 
// parser/structure.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_STRUCTURE_H
#define PARSER_STRUCTURE_H

#include "delegateholder.h"
#include "member.h"
#include "membersimple.h"

#include "atl/array.h"
#include "atl/binmap.h"
#include "parsercore/versioninfo.h"

namespace rage {

class bkBank;
struct parStructureStaticData;
class parTreeNode;
class Vector3;


// PURPOSE: A parStructure contains the runtime metadata for a struct or class. It has a list of
// parMembers describing each member, as well as a (single) pointer to a base class.
// parStructures are normally automatically generated, but could be created at runtime as well.
// Typically, each parStructure desribes a C++ struct or class. The struct or class that the
// parStructure describes to is called the parStructure's "described class" for purposes of the documentation.
class parStructure
{
public:
	parStructure();
	virtual ~parStructure();

	enum Flags
	{
		HAS_VIRTUALS,
		HAS_NAMES,
		ALWAYS_HAS_NAMES,
		IS_EXTERNAL,
		IS_CONSTRUCTIBLE,
		NOT_IN_PLACE_LOADABLE,  // When loading the PSO files, never consider this structure in-place loadable
	}; // This needs to match codegenutils.h!

	// PURPOSE: Sets the type name for this structure. This doesn't necessarily match the stringified described class name.
	void SetName(const char* name);

#if PARSER_ALL_METADATA_HAS_NAMES
	// RETURNS: The type name of this structure. This doesn't necessarily match the stringified described class name.
	const char* GetName() const;
#endif

	// RETURNS: The type name of this structure, but MAY NOT WORK if the structure doesn't have strings (only hashes)
	const char* GetNameUnsafe() const;

	// PURPOSE: Displays an error message if names don't exist - optionally checks if they will exist in all builds
	// RETURNS: True if names exist, false otherwise
	bool VerifyNamesExist(bool inAllBuilds, const char* action);

	// RETURNS: The hash of this structure's name.
	u32 GetNameHash() const {return m_NameHash.GetHash();}

	// PURPOSE: Sets the base class for this structure.
	// PARAMS:
	//		baseMetadata - the structure metadata of the base class
	//		baseOffset - an offset from the beginning of the base structure to the beginning of this structure. 
	//			normally 0, but in some cases with multiple inheritance may be non-0
	// NOTES:
	//		To find the baseOffset given a Base and Derived class, use this:
	//		Derived* derivedPtr = /* something */
	//		baseOffset = reinterpret_cast<int>(static_cast<Base*>(derivedPtr)) - reinterpret_cast<int>(derivedPtr)
	void SetBaseClass(parStructure* baseMetadata, int baseOffset = 0);

	// PURPOSE: Set the version number for this structure. Version numbers will be checked during load.
	void SetVersion(parVersionInfo vers) {m_Version = vers;}

	bool IsConstructible() { return m_Flags.IsSet(IS_CONSTRUCTIBLE); }

	// RETURNS: The version number for this structure.
	parVersionInfo GetVersion() const {return m_Version;}

	typedef ::rage::atDelegate<void*()> FactoryDelegate;
	typedef ::rage::atDelegate<void(void*v)> PlacementDelegate;
	typedef ::rage::atDelegate< ::rage::parStructure*(parConstPtrToStructure) > GetStructureDelegate;
	typedef ::rage::atDelegate< void(parPtrToStructure) > DestructorDelegate;

	// PURPOSE: Sets a callback that creates a new instance of the described class.
	void SetFactory(FactoryDelegate fn) {m_Factory = fn; m_Flags.Set(IS_CONSTRUCTIBLE, true); }

	// PURPOSE: Sets a callback that calls placement new for a new instance of the described class
	void SetPlacementFactory(PlacementDelegate fn) { m_PlacementFactory = fn; }

	// PURPOSE: Sets a callback that calls the destructor (not delete!) for an instance of the described class
	void SetDestructor(DestructorDelegate fn) {m_Destructor = fn;}

	// PURPOSE: Sets the size of the described class
	void SetSize(size_t size) {m_Size = size;}

	// PURPOSE: Returns the size of the described class
	size_t GetSize() const {return m_Size;}

	// PURPOSE: Finds the alignment of the described class
	size_t FindAlign() const;

	void SetFlags(atFixedBitSet8 flags) { m_Flags = flags; }

	atFixedBitSet8& GetFlags() { return m_Flags; }
	const atFixedBitSet8& GetFlags() const { return m_Flags; }

	// PURPOSE: Returns the callback that, given a parPtrToStructure pointer to an instance of the described class or any derived class,
	// returns the parStructure that describes the concrete class of the instance.
	GetStructureDelegate GetGetStructureCB() {return m_GetStructure;}

	// PURPOSE: Sets a callback that, given a parPtrToStructure pointer to an instance of the described class or any derived class,
	// returns the parStructure that describes the concrete class of the instance.
	void SetGetStructureCB(GetStructureDelegate fn) {m_GetStructure = fn;}

	// PURPOSE: Given a parPtrToStructure pointer to an instance of the described class or any derived class,
	// returns the parStructure that describes the concrete class of the instance.
	virtual parStructure* GetConcreteStructure(parConstPtrToStructure structAddr) {
		return m_GetStructure(structAddr);
	}

	// PURPOSE: Removes a named member from the structure.
	void RemoveMember(const char* name);

	// PURPOSE: Sets the maximum number of members in the structure.
	void SetMaxMembers(int max);

	// PURPOSE: Adds a new member to the structure.
	void AddMember(parMember* member);

	// PURPOSE: Reads data from a parTreeNode and stores it in the structure pointed at the specified address
	// PARAMS:
	//		structAddr - assumed to be an instance of the described class.
	virtual void ReadMembersFromTree(parTreeNode* node, parPtrToStructure structAddr);

	// PURPOSE: Returns true if this is a subclass of str
	bool IsSubclassOf(const parStructure* str) const;

	// PURPOSE: Returns true if this is a subclass of the registered structure named str.
	bool IsSubclassOf(const char* str) const;

	// PURPOSE: Returns true if this is a subclass of parsable type Parent
	template<typename Parent>
	bool IsSubclassOf() const { return IsSubclassOf(Parent::parser_GetStaticStructure()); }
	
	// PURPOSE: Creates a new instance of the described class.
	parPtrToStructure Create();

	// PURPOSE: Destroys an existing instance of the described class
	void Destroy(parPtrToStructure inst);

	// PURPOSE: Places a new instance of the described class at the specified address
	parPtrToStructure Place(void*);

	// PURPOSE: Allocates new data on the heap (not by calling the factory delegate) and then calls the placement delegate.
	// NOTES: The memory should be freed with delete [] (char*)(mem)
	parPtrToStructure AllocAndPlace();

	// RETURNS: the base class parStructure 
	parStructure* GetBaseStructure() {
		return m_Base;
	}

	const parStructure* GetBaseStructure() const {
		return m_Base;
	}

	// RETURNS: The starting address of the base class (typically the same as addr, unless the classes use multiple inheritance)
	parPtrToStructure GetBaseAddress(parPtrToStructure structAddr) const {
		return (parPtrToStructure)((size_t)structAddr + m_OffsetOfBase);
	}
	parConstPtrToStructure GetBaseAddress(parConstPtrToStructure structAddr) const {
		return (parConstPtrToStructure)((size_t)structAddr + m_OffsetOfBase);
	}

	// RETURNS: The number of members in this structure.
	int GetNumMembers() const {
		return m_Members.GetCount();
	}

	// RETURNS: The ith member
	parMember* GetMember(int i) {
		return m_Members[i];
	}

	// RETURNS: The ith member
	const parMember* GetMember(int i) const {
		return m_Members[i];
	}

	// RETURNS: A member named 'name'
	parMember* FindMember(const char* name, bool searchBaseClasses = false);

	// RETURNS: A member with the specified name hash
	parMember* FindMember(atLiteralHashValue hash, bool searchBaseClasses = false);

	// PURPOSE: Primarily for internal use. Given a table of parMemberSimple::Data pointers,
	// reads the Type field, recasts the pointer to be of the appropriate parMember???::Data type,
	// creates the corresponding parMember??? subclass, and adds it to the member list.
	void BuildStructureFromStaticData(parStructureStaticData& data);

	// PURPOSE: Given a parMemberSimple::Data pointer that could actually be pointing to any
	// parMember???::Data object, reads the type field, casts it to the appropriate parMember???::Data
	// type, and creates and returns a new instance of the corresponding parMember subclass.
	static parMember* CreateMemberFromMemberData(parMember::CommonData* memberData);

	parAttributeList*& GetExtraAttributes() {return m_ExtraAttributes;}

	parAttributeList* GetOrCreateExtraAttributes();

	void LoadExtraAttributes(parTreeNode* node);

	// PURPOSE: Primarily for internal use. Given the address of an instance of this structure,
	// builds an array of all base parStructures and an array of all the corresponding addresses
	// to use.
	struct BaseList
	{
		void Init(parStructure* structMetadata, parPtrToStructure structAddr);
		static const int MAX_NUM_BASES = 16;
		int GetNumBases() {return m_Addresses.GetCount();}
		parStructure* GetBaseStructure(int i) {return m_Metadata[i];}
		parPtrToStructure GetBaseAddress(int i) {return m_Addresses[i];}
	private:
		atFixedArray<parPtrToStructure, MAX_NUM_BASES> m_Addresses;
		atFixedArray<parStructure*,	MAX_NUM_BASES> m_Metadata;
	};


	template<typename _Type>
	void AddDelegate(const char* name, _Type del)
	{
		parDelegateHolder<_Type>* fh = rage_new parDelegateHolder<_Type>;
		fh->Delegate = del;
		AddDelegateHolder(name, fh);
	}

	bool HasDelegate(const char* name)
	{
		return m_Delegates.Has(name);
	}

	void BeginAddingInitialDelegates(int numToAdd);

	void EndAddingInitialDelegates();

	// Similar to instPtr->name()
	void Invoke(parPtrToStructure instPtr, const char* name) const;
	void Invoke(parConstPtrToStructure instPtr, const char* name) const
	{ Invoke(const_cast<parPtrToStructure>(instPtr), name); }

	// Similar to instPtr->name(arg1)
	template<typename _Arg1>
	void Invoke(parPtrToStructure instPtr, const char* name, _Arg1 arg1) const;

	template<typename _Arg1>
	void Invoke(parConstPtrToStructure instPtr, const char* name, _Arg1 arg1) const
	{ Invoke(const_cast<parPtrToStructure>(instPtr), name, arg1); }

	// Similar to instPtr->name(arg1, arg2)
	template<typename _Arg1, typename _Arg2>
	void Invoke(parPtrToStructure instPtr, const char* name, _Arg1 arg1, _Arg2 arg2) const;

	template<typename _Arg1, typename _Arg2>
	void Invoke(parConstPtrToStructure instPtr, const char* name, _Arg1 arg1, _Arg2 arg2) const
	{ Invoke(const_cast<parPtrToStructure>(instPtr), name, arg1, arg2); }

	// Similar to instPtr->name(arg1, arg2, arg3)
	template<typename _Arg1, typename _Arg2, typename _Arg3>
	void Invoke(parPtrToStructure instPtr, const char* name, _Arg1 arg1, _Arg2 arg2, _Arg3 arg3) const;

	template<typename _Arg1, typename _Arg2, typename _Arg3>
	void Invoke(parConstPtrToStructure instPtr, const char* name, _Arg1 arg1, _Arg2 arg2, _Arg3 arg3) const
	{ Invoke(const_cast<parPtrToStructure>(instPtr), name, arg1, arg2, arg3); }

	parDelegateHolderBase* FindDelegate(const char* name);

	// PURPOSE: For internal use only
	static const char* internal_GetStructureName(parStructure* structure);

	// PURPOSE: For internal use only
	static parStructure* internal_FindNamedStructure(const char* name);

	bool internal_CheckVersionForLoad(parVersionInfo m_FileVersion);

	void AddDelegateHolder(const char* name, parDelegateHolderBase* base);

	u8& GetRegistrationCount() { return m_RegistrationCount; }

	// TODO: This should  be a setting, but parStructure doesn't do settings, and settings aren't threadsafe.
	__THREAD static bool sm_SkipPostloadCallbacks; // If true, we will not call postload callbacks - used for loading on non-main threads. Make sure to call the postload callbacks yourself later.

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	void PreLoad(parTreeNode* node);
	void PostSave(parTreeNode* node);

	PAR_PARSABLE;
#endif

protected:

#if PARSER_ALL_METADATA_HAS_NAMES
	atLiteralHashString			m_NameHash;
#else
	atLiteralHashValue			m_NameHash;
#endif

	parStructure* m_Base;
	// For structures that use C++ multiple inheritance, the base class' "this" pointer
	// might not always be in the same place as this class's this pointer.
	size_t m_OffsetOfBase; 
	size_t m_Size;
	atFixedBitSet8 m_Flags;
	u8		m_RegistrationCount;			// Tracks how many times this has been registered
	mutable u16		m_CachedAlign;					// 0, until we call FindAlign
	parVersionInfo m_Version;
	atArray<parMember*> m_Members;
	parAttributeList* m_ExtraAttributes;

	// Core delegates
	FactoryDelegate m_Factory;
	PlacementDelegate m_PlacementFactory;
	GetStructureDelegate m_GetStructure;
	DestructorDelegate m_Destructor;

	atStringMap<parDelegateHolderBase*> m_Delegates;
};

inline parDelegateHolderBase* parStructure::FindDelegate(const char* name)
{
	parDelegateHolderBase* const * basePtrPtr = m_Delegates.SafeGet(name);
	return basePtrPtr ? *basePtrPtr : NULL;
}

inline void parStructure::Invoke(parPtrToStructure instPtr, const char* name) const
{
	parDelegateHolderBase* const * basePtrPtr = m_Delegates.SafeGet(name);
	if (basePtrPtr && *basePtrPtr)
	{
		// Should really use SafeCast here, but parDelegateHolderBase would need a vtable for it to work right
		parDelegateHolder<atDelegate<void()> >* holder = (parDelegateHolder<atDelegate<void()> >*)(*basePtrPtr);
		if (holder)
		{
			holder->Delegate.Retarget(instPtr);
			holder->Delegate();
		}
	}

	if (m_Base)
	{
		m_Base->Invoke(instPtr, name);
	}
}

template<typename _Arg1>
void parStructure::Invoke(parPtrToStructure instPtr, const char* name, _Arg1 arg1) const
{
	parDelegateHolderBase* const * basePtrPtr = m_Delegates.SafeGet(name);
	if (basePtrPtr && *basePtrPtr)
	{
		// Should really use SafeCast here, but parDelegateHolderBase would need a vtable for it to work right
		parDelegateHolder<atDelegate<void(_Arg1)> >* holder = (parDelegateHolder<atDelegate<void(_Arg1)> >*)(*basePtrPtr);
		if (holder)			
		{
			holder->Delegate.Retarget(instPtr);
			holder->Delegate(arg1);
		}
	}

	if (m_Base)
	{
		m_Base->Invoke<_Arg1>(instPtr, name, arg1);
	}

}

template<typename _Arg1, typename _Arg2>
void parStructure::Invoke(parPtrToStructure instPtr, const char* name, _Arg1 arg1, _Arg2 arg2) const
{
	parDelegateHolderBase* const * basePtrPtr = m_Delegates.SafeGet(name);
	if (basePtrPtr && *basePtrPtr)
	{
		// Should really use SafeCast here, but parDelegateHolderBase would need a vtable for it to work right
		parDelegateHolder<atDelegate<void(_Arg1, _Arg2)> >* holder = (parDelegateHolder<atDelegate<void (_Arg1, _Arg2)> >*)(*basePtrPtr);
		if (holder)
		{
			holder->Delegate.Retarget(instPtr);
			holder->Delegate(arg1, arg2);
		}
	}

	if (m_Base)
	{
		m_Base->Invoke<_Arg1, _Arg2>(instPtr, name, arg1, arg2);
	}
}

template<typename _Arg1, typename _Arg2, typename _Arg3>
void parStructure::Invoke(parPtrToStructure instPtr, const char* name, _Arg1 arg1, _Arg2 arg2, _Arg3 arg3) const
{
	parDelegateHolderBase* const * basePtrPtr = m_Delegates.SafeGet(name);
	if (basePtrPtr && *basePtrPtr)
	{
		// Should really use SafeCast here, but parDelegateHolderBase would need a vtable for it to work right
		parDelegateHolder<atDelegate<void (_Arg1, _Arg2, _Arg3)> >* holder = (parDelegateHolder<atDelegate<void (_Arg1, _Arg2, _Arg3)> >*)(*basePtrPtr);
		if (holder)
		{
			holder->Delegate.Retarget(instPtr);
			holder->Delegate(arg1, arg2, arg3);
		}
	}

	if (m_Base)
	{
		m_Base->Invoke<_Arg1, _Arg2, _Arg3>(instPtr, name, arg1, arg2, arg3);
	}
}


} // namespace rage

#endif // PARSER_STRUCTURE_H
