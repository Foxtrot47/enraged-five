// 
// grshadowmap/spotlightshadowmap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/light.h"
#include "grcore/quads.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "system/xtl.h"
#include "grcore/device.h"
#include "grcore/im.h"
#include "spotlightshadowmap.h"

using namespace rage;

#if __XENON
#include "embedded_rage_spotshadowmap_fxl_final.h"
#elif __PPU
#include "embedded_rage_spotshadowmap_psn.h"
#elif __WIN32PC
#include "embedded_rage_spotshadowmap_win32_30.h"
#endif


void CShadowedSpotLight::Begin( bool performClear )
{
	m_oldView = grcViewport::GetCurrent();
	grcViewport::SetCurrent( &m_shadowView );

	//PIXBegin( m_shadowMap ? m_shadowMap->GetName() : m_shadowDepthMap->GetName() );

	grcTextureFactory::GetInstance().LockRenderTarget(0, m_shadowMap, m_shadowDepthMap);
	
	if ( performClear )
	{
		GRCDEVICE.Clear(  m_type == VARIANCE , Color32( 1.0f, 1.0f, 1.0f, 1.0f ), true, 1.0f, 0);
	}


	if ( m_type == VARIANCE )		// set up the parameters for this shadow map
	{
		Assert( m_PointLightPositionID != grcevNONE );
		Assert( m_LightAttenuationEndID != grcevNONE);

		// set the spot light position
	
		m_DepthShader->SetVar( m_LightAttenuationStartID,  GetStart()/GetRange() );
		m_DepthShader->SetVar( m_PointLightPositionID,  VEC3V_TO_VECTOR3(m_shadowView.GetCameraPosition())  );
		m_DepthShader->SetVar( m_LightAttenuationEndID,  1.0f/GetRange()  );
	}
	

	// force depth shader
	grmModel::SetForceShader(m_DepthShader);
}

void CShadowedSpotLight::End( bool blurResult )
{
	// undo the force of the shader
	grmModel::SetForceShader(0);

	grcResolveFlags	clearParams;
	// try to get the free clear from the resolve
	clearParams.ClearColor=  m_type == VARIANCE;
	clearParams.Color = Color32( 1.0f, 1.0f, 1.0f, 1.0f );
	clearParams.ClearDepthStencil= m_type == VARIANCE;;
	clearParams.BlurResult = blurResult && m_type == VARIANCE && m_filterSize > 0.0f;
	clearParams.BlurKernelSize = m_filterSize;


	grcTextureFactory::GetInstance().UnlockRenderTarget(0, &clearParams, m_atlasIndex );
	
	if ( m_type == STANARD )
	{
		// clear depth buffer only
		GRCDEVICE.Clear(false, Color32(0.0f, 0.0f, 0.0f, 1.0f), true, 1.0f, 0);
	}
	grcViewport::SetCurrent( m_oldView );

	ClearDirty();

//	PIXEnd();
}
void CShadowedSpotLight::ResetShadowTextureSize( int size , const char* name )
{
	if ( m_useAtlas )
	{
		return;
	}
	size = Clamp( size, ShadowMapMinSize, ShadowMapMaxSize );		

	if ( ( GetTexture() && size ==GetTexture()->GetWidth() ) )	// created texture is fine
	{
		return;
	}
	CreateShadowRenderTarget( size, name );
}
void CShadowedSpotLight::Init( int size, const char* name, ShadowType type, bool useAtlas, int atlasIndex )
{
	m_useAtlas = useAtlas;
	m_atlasIndex = atlasIndex;
	m_type = type;

	ResetShadowTextureSize( size, name );

	if ( m_DepthShader )
	{
		delete m_DepthShader;
	}
	const char *shaderName = m_type == VARIANCE ? "rage_spotshadowmap" : "rage_shadowdepth";

	m_DepthShader = grmShaderFactory::GetInstance().Create();
	m_DepthShader->Load(shaderName);


	if ( m_type == VARIANCE )
	{
		// get handle to the shared variable that holds the point light position
		m_PointLightPositionID = m_DepthShader->LookupVar( "spotPointLightPosition" );
		m_LightAttenuationEndID = m_DepthShader->LookupVar( "spotLightAttenuationEnd");
		m_LightAttenuationStartID = m_DepthShader->LookupVar( "spotLightAttenuationStart" );
	}
}
void CShadowedSpotLight::SetRenderTargets( const grcRenderTarget*  shadowMap, const grcRenderTarget* shadowDepthMap )
{
	Assert( m_useAtlas == true );
	m_shadowMap = const_cast<grcRenderTarget*>( shadowMap );
	m_shadowDepthMap = const_cast<grcRenderTarget*>( shadowDepthMap );
}
void CShadowCache::CreateShadowAtlas( int size , int amt ,CShadowedSpotLight::ShadowType& type )
{

	grcTextureFactory::CreateParams paramsDepth;

	paramsDepth.IsResolvable = type != CShadowedSpotLight::VARIANCE;	
	paramsDepth.UseHierZ = true;
	paramsDepth.HasParent = true;
	

	char name[512] = "shadowMapAtlas";

#if	__XENON
	if ( type == CShadowedSpotLight::VARIANCE || type == CShadowedSpotLight::MULTISAMPLED )
	{	
		paramsDepth.Multisample = 4;
	}
#endif


	if ( type == CShadowedSpotLight::VARIANCE )
	{	grcTextureFactory::CreateParams cp;

		cp.Multisample = paramsDepth.Multisample;
		cp.Format = grctfG16R16;
		cp.Parent = NULL;
		cp.HasParent = true;
		//cp.MipLevels = 4;
		char shadowName[128];

		cp.TextureAtlas = amt;

		// for cube maps we use G16R16 render target with a value range of -32..32 to store 
		// depth^2 data in the second channel for variance shadow maps
		sprintf(shadowName,"%s Color", name );
		m_shadowMap = grcTextureFactory::GetInstance().CreateRenderTarget(shadowName, grcrtPermanent, size, size, 32, &cp);

		paramsDepth.Parent = m_shadowMap.GetPtr();
	}
	m_shadowDepthMap = grcTextureFactory::GetInstance().CreateRenderTarget( name, grcrtDepthBuffer, size, size, 32, &paramsDepth);
}
void CShadowedSpotLight::CreateShadowRenderTarget( int size, const char* name)
{
	
	grcTextureFactory::CreateParams paramsDepth;

	paramsDepth.IsResolvable = m_type != VARIANCE;	
	paramsDepth.UseHierZ = true;
	paramsDepth.HasParent = true;

	if ( name == 0)
	{
		name = "SpotShadowMap";
	}

#if	__XENON
	if ( m_type == VARIANCE || m_type == MULTISAMPLED )
	{	
		paramsDepth.Multisample = 4;
	}
#endif
	
	
	if ( m_type == VARIANCE )
	{	
		grcTextureFactory::CreateParams cp;

		cp.Multisample = paramsDepth.Multisample;
		cp.Format = grctfG16R16;
		cp.Parent = NULL;
		cp.HasParent = true;
		cp.MipLevels = 2;
		char shadowName[128];

			// for cube maps we use G16R16 render target with a value range of -32..32 to store 
			// depth^2 data in the second channel for variance shadow maps
		sprintf(shadowName,"%s Color", name );
		m_shadowMap = grcTextureFactory::GetInstance().CreateRenderTarget(shadowName, grcrtPermanent, size, size, 32, &cp);

		paramsDepth.Parent = m_shadowMap;
	}
	m_shadowDepthMap = grcTextureFactory::GetInstance().CreateRenderTarget( name, grcrtDepthBuffer, size, size, 32, &paramsDepth);
}

Matrix44 SetupShadowViewPort( grcViewport& vp,  const Vector3& position, const Vector3& direction, float fov,  const Vector2& depthRange, int texWidth, 
										const Vector2& atlasOffset, const Vector2& atlasScale, Vector4& shadowCompressed, float& yOffset )
{
	// setup orthographic or perspective depending on fov value
	fov = Min( fov, 170.0f );
	if ( fov == 0 )
	{
		// dynamic generation of the orthographic projection matrix
		vp.Ortho( -0.5f,  0.5f, -0.5f, 0.5f, depthRange.x, depthRange.y );
	}
	else
	{
		vp.Perspective( fov, 1.0f, depthRange.x, depthRange.y );//0.0001f, 20.0f);
	}

	Matrix34 camera;
	if ( fabs( direction.y ) < 0.99f)		// check so mess up cross product.
	{
		camera.LookAt(VEC3_ZERO, direction, YAXIS);
	}
	else
	{
		camera.LookAt(VEC3_ZERO, direction, XAXIS);
	}
	camera.a.Negate();
	camera.d = position;
	vp.SetCameraMtx(RCC_MAT34V(camera));


	// keep viewport class happy by sending down an identity matrix
	vp.SetWorldIdentity();

	Matrix44 view;
	Matrix44 projection; //, comp;
	Matrix44  res;
	view.Set( RCC_MATRIX44(vp.GetViewMtx()));
	projection.Set( RCC_MATRIX44(vp.GetProjection()));
	//projection.c = Vector4( 0.0f, 0.0f, 1.0f, 0.0f );  // change z to before post projective space
	res.Dot(projection,view);

	// convert from view to texture space
	Matrix44	shadowViewToTextureMtx;

	shadowViewToTextureMtx.Identity();
	float z = 1.0f;
	shadowViewToTextureMtx.MakeScale( 0.5f * atlasScale.x, -0.5f * atlasScale.y, z );	

	//add a half pixel offset due to multisampling ( I think)
	float halfTexel = 0.5f/(float) texWidth;
	shadowViewToTextureMtx.d = Vector4( ( 0.5f + halfTexel ) * atlasScale.x + atlasOffset.x, (0.5f + halfTexel ) * atlasScale.y + atlasOffset.y, 0.0f, 1.0f );

	Matrix44  res2;
	res2.Dot(shadowViewToTextureMtx,res);


	// Calculate compressed matrix for spotlights
	Matrix44 viewTrans;
	viewTrans.Transpose( view );
	Vector4	xAxias = viewTrans.a * projection.a.x * atlasScale.x * 0.5f;
	xAxias.w = ( 0.5f + halfTexel ) * atlasScale.x + atlasOffset.x;
	yOffset = (0.5f + halfTexel ) * atlasScale.y + atlasOffset.y;
	shadowCompressed = xAxias;
	//Assert( atlasScale.x == atlasScale.y );
	return res2;
}
Matrix44 SetupDirectionalShadowViewPort( grcViewport& vp,  const Vector3& position, const Vector3& direction, const Vector3& rmin, const Vector3& rmax,
													const Vector2& atlasOffset, const Vector2& atlasScale, int texWidth )
{
	// setup orthographic 
	vp.Ortho( rmin.x,  rmax.x, rmin.y, rmax.y, rmin.z, rmax.z );


	Matrix34 camera;
	if ( fabs( direction.y ) < 0.99f)		// check so mess up cross product.
	{
		camera.LookAt(VEC3_ZERO, direction, YAXIS);
	}
	else
	{
		camera.LookAt(VEC3_ZERO, direction, XAXIS);
	}
	camera.a.Negate();
	camera.d = position;
	vp.SetCameraMtx(RCC_MAT34V(camera));


	// keep viewport class happy by sending down an identity matrix
	vp.SetWorldIdentity();

	Matrix44 view;
	Matrix44 projection; //, comp;
	Matrix44  res;
	view.Set( RCC_MATRIX44(vp.GetViewMtx()));
	projection.Set( RCC_MATRIX44(vp.GetProjection()));
	//projection.c = Vector4( 0.0f, 0.0f, 1.0f, 0.0f );  // change z to before post projective space
	res.Dot(projection,view);

	// convert from view to texture space
	Matrix44	shadowViewToTextureMtx;

	shadowViewToTextureMtx.Identity();
	float z = 1.0f;
	shadowViewToTextureMtx.MakeScale( 0.5f * atlasScale.x, -0.5f * atlasScale.y, z );	

	//add a half pixel offset due to multisampling ( I think)
	float halfTexel = 0.5f/(float) texWidth;
	shadowViewToTextureMtx.d = Vector4( ( 0.5f + halfTexel ) * atlasScale.x + atlasOffset.x, (0.5f + halfTexel ) * atlasScale.y + atlasOffset.y, 0.0f, 1.0f );

	Matrix44  res2;
	res2.Dot(shadowViewToTextureMtx,res);
	return res2;
}


Vector2 GetTexCoordsComp(  const Vector4& compMat, float yOffset, const Vector3& spotDir, const Vector3& dir, const Vector2&  )
{
	Vector2 res;
	Vector3 xAxias(compMat.x, compMat.y, compMat.z  );

	Vector3 matY;
	Vector2 offset( compMat.w, yOffset );
	matY.Cross( xAxias, spotDir );
	
	res.x = xAxias.Dot( dir );
	res.y = matY.Dot( dir );
	res.x = res.x/ dir.Dot( spotDir )  + offset.x;
	res.y = res.y / dir.Dot( spotDir )  + offset.y;
	return res;
}
Vector2 GetTexCoordsMat(  const Matrix44& Mat, const Vector4& pos )
{
	Matrix44	transMat;
	transMat.Transpose( Mat );

	Vector2 res;

	res.x = pos.Dot( transMat.a);
	res.y = pos.Dot( transMat.b);
	float w = pos.Dot( transMat.d );
	res *=1.0f/w;
	return res;
}

void testCompressedShadowMatrix( const Matrix44& shadowMatrix, const Vector4& compMat, const Vector3& spotDir, const Vector3& spotPos , Vector2 atlasScale, float yOffset )
{
	Vector3 dir;
	Vector3	testPoint = Vector3( 10.0f, 10.0f, 10.0f );
	Vector4	testPoint4 = Vector4( 10.0f, 10.0f, 10.0f, 1.0f );
	 
	Vector2 resComp = GetTexCoordsComp( compMat, yOffset, spotDir, testPoint - spotPos, atlasScale );
	Vector2 resMat = GetTexCoordsMat( shadowMatrix, testPoint4);

	Assert( (resComp - resMat).Mag() < 0.0001f );
}
void CShadowedSpotLight::CreateSpotLight( const Vector3& position, const Vector3& direction, float fov , const Vector2& depthRange )
{
	// store these for caching purposes
	m_position = position;
	m_direction = direction;

	Vector2 atlasOffset;
	Vector2 atlasScale;

	m_range = depthRange;
	
	m_shadowMap->GetAtlasTextureOffsets( m_atlasIndex, atlasOffset, atlasScale);

	m_ShadowViewProj  = SetupShadowViewPort( m_shadowView, position, direction, fov , depthRange, m_shadowMap->GetWidth() , atlasOffset, atlasScale , m_ShadowCompressed, m_yOffset);// RAY - temp GetTexture()->GetWidth());

	atlasScale.x *= 0.5f;
	atlasScale.y *= -0.5f;

	MarkDirty();		// mark for future render
	//testCompressedShadowMatrix( m_ShadowViewProj, m_ShadowCompressed, direction, position, atlasScale, m_yOffset);
}
void CShadowedSpotLight::CreateDirectionalLight( const Vector3& position, const Vector3& direction, const Vector3& rmin, const Vector3& rmax )
{
	// store these for caching purposes
	m_position = position;
	m_direction = direction;

	m_range = Vector2( rmin.z, rmax.z );
	
	Vector2 atlasOffset;
	Vector2 atlasScale;
	if ( m_shadowMap )
	{
		m_shadowMap->GetAtlasTextureOffsets( m_atlasIndex, atlasOffset, atlasScale);
	}
	else
	{
		atlasOffset = Vector2( 0.0f, 0.0f);
		atlasScale = Vector2( 1.0f, 1.0f);
	}

	m_ShadowViewProj  = SetupDirectionalShadowViewPort( m_shadowView, position, direction, rmin, rmax,
														atlasOffset, atlasScale, GetTexture()->GetWidth());

	atlasScale.x *= 0.5f;
	atlasScale.y *= -0.5f;

	MarkDirty();		// mark for future render
	//testCompressedShadowMatrix( m_ShadowViewProj, m_ShadowCompressed, direction, position, atlasScale, m_yOffset);
}
	

void CShadowedSpotLight::Show(  int DestX, int DestY, int SizeX, int SizeY )
{
	PUSH_DEFAULT_SCREEN();
	grcBindTexture( GetTexture() );
	
	grcBeginQuads(1);
	grcDrawQuadf( (float)DestX, (float)DestY, (float)SizeX, (float)SizeY, 0.f, 0, 0, 1, 1, Color32(255,255,255));
	grcEndQuads();
	POP_DEFAULT_SCREEN();
}
void CShadowCache::Show(int OriginX, int OriginY, int Width , int NumPerRow)
{
	const int RectWidth = Width;
	const int RectHeight = Width;

	if ( m_useAtlas )
	{
		PUSH_DEFAULT_SCREEN();
		
		grcBindTexture( GetTexture() );
		
		grcBeginQuads(1);
		grcDrawQuadf( (float)OriginX, (float)OriginY, Width * 4.0f, Width * 2.0f, 0.f, 0, 0, 1, 1, Color32(255,255,255));
		grcEndQuads();

		POP_DEFAULT_SCREEN();
		return;
	}
	for ( int  i = 0; i < m_usedCnt; i++)
	{
		int widthOffset = i / NumPerRow;
		int heightOffset = i % NumPerRow;
		int ox = OriginX + widthOffset * Width;
		int oy =OriginY + heightOffset * Width;

		if ( !m_useAtlas )
		{
			m_shadows[i]->Show(		ox,		// top left x
								oy ,	// top left y 
								ox + RectWidth,		// bottom right x
								oy + RectHeight );		// bottom right y
		}
	}
	return;
}
Vector2 CShadowCache::GetShadowTexScale()
{
	Vector2 atlasOffset;
	Vector2 atlasScale;

	m_shadowMap->GetAtlasTextureOffsets( 0, atlasOffset, atlasScale);
	atlasScale.x *= 0.5f;
	atlasScale.y *= -0.5f;
	return atlasScale;
}
