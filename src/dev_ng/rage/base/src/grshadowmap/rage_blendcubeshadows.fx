//-----------------------------------------------------
//
// Cube shadow map
//
//-----------------------------------------------------

// Get the globals
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

float fLightVSMEpsilon;
float fDepthBias;

struct vertexOutputComposite 
{
    float4 hPosition        : DX_POS;
    float3 vLight			: TEXCOORD5;
};

//------------------------------------------------------
// Vertex Shader skinned geometry
//------------------------------------------------------
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
{
    vertexOutputComposite OUT = (vertexOutputComposite)0;

	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
    float3 pos = rageSkinTransform(IN.pos, boneMtx);

    OUT.hPosition = mul(float4(pos,1), gWorldViewProj);
    
	OUT.vLight = -((gPointLightPosition - pos) * gGeometryScale);   
	    
    return OUT;
}

//------------------------------------------------------
// Vertex Shader non-skinned geometry
//------------------------------------------------------
vertexOutputComposite VS_Composite(rageVertexInputBump IN) 
{
    vertexOutputComposite OUT = (vertexOutputComposite)0;
    OUT.hPosition = mul(float4(IN.pos, 1.0), gWorldViewProj);
        
    float4 temp = mul(float4(IN.pos,1), gWorld);
    
	OUT.vLight = -((gPointLightPosition - temp.xyz) * gGeometryScale);   

    return OUT;
}





//--------------------------------------------------------------------------------------
//
// Fetch cube map and compare ... barebone version
//
//--------------------------------------------------------------------------------------
float CompareDepth(samplerCUBE inShadowMap, float3 inTex, float inDepth)
{
	return inDepth < texCUBE(inShadowMap, inTex).r;
}


//--------------------------------------------------------------------------------------
//
// Cube map filtering is based on the article "Floating Point Cube Maps" by
// Arkadiusz Waliszewski in ShaderX2
//
//--------------------------------------------------------------------------------------
float CompareDepthFiltered(samplerCUBE inShadowMap, float3 inTex, float inDepth)
{
    float4 vShadowDepth[2];
    
    // compute a bunch of texture coordinates per-pixel
    // a simple unweighted jittered lookup could be computed in the vertex shader
    // and interpolated per-pixel, but it wouldn't look as good.
    float fMa = max(abs(inTex.x), max(abs(inTex.y), abs(inTex.z)));
    float3 vProjCoord = (inTex / fMa) * 512.0f;
    float3 vWeights = frac(vProjCoord);
    
    vProjCoord = floor(vProjCoord);
    
    float3 tcShadowCoord[8] = 
    {
        vProjCoord + float3( 0, 0, 0),  //0
        vProjCoord + float3( 0, 0, 1),  //1
        vProjCoord + float3( 0, 1, 0),  //2
        vProjCoord + float3( 0, 1, 1),  //3
        vProjCoord + float3( 1, 0, 0),  //4
        vProjCoord + float3( 1, 0, 1),  //5
        vProjCoord + float3( 1, 1, 0),  //6
        vProjCoord + float3( 1, 1, 1)   //7
    };
        
    //  vectorize the lerps
    vShadowDepth[0].x = texCUBE(inShadowMap, tcShadowCoord[0]).r;
    vShadowDepth[0].y = texCUBE(inShadowMap, tcShadowCoord[2]).r;
    vShadowDepth[0].z = texCUBE(inShadowMap, tcShadowCoord[4]).r;
    vShadowDepth[0].w = texCUBE(inShadowMap, tcShadowCoord[6]).r;
    vShadowDepth[1].x = texCUBE(inShadowMap, tcShadowCoord[1]).r;
    vShadowDepth[1].y = texCUBE(inShadowMap, tcShadowCoord[3]).r;
    vShadowDepth[1].z = texCUBE(inShadowMap, tcShadowCoord[5]).r;
    vShadowDepth[1].w = texCUBE(inShadowMap, tcShadowCoord[7]).r;

    float4 vVisibility[2];
    vVisibility[0] = inDepth.xxxx < vShadowDepth[0];
    vVisibility[1] = inDepth.xxxx < vShadowDepth[1];
    
    vVisibility[0] = lerp(vVisibility[0], vVisibility[1], vWeights.z);
    vVisibility[0].xy = lerp(vVisibility[0].xz, vVisibility[0].yw, vWeights.y);
        
    return lerp(vVisibility[0].x, vVisibility[0].y, vWeights.x);
}

//--------------------------------------------------------------------------------------
//
// Cube map filtering is based on the article "Floating Point Cube Maps" by
// Arkadiusz Waliszewski in ShaderX2
// keeps track of maximum depth value as well to restrict variance shadow maps
//
//--------------------------------------------------------------------------------------
float3 CubeBlurFilter(samplerCUBE inShadowMap, float3 inTex)
{
    //float4 vShadowDepth[2];
    float2 vShadowDepth[8];
    
    // compute a bunch of texture coordinates per-pixel
    // a simple unweighted jittered lookup could be computed in the vertex shader
    // and interpolated per-pixel, but it wouldn't look as good.
    float fMa = max(abs(inTex.x), max(abs(inTex.y), abs(inTex.z)));
    float3 vProjCoord = (inTex / fMa) * 512.0f;
    float3 vWeights = frac(vProjCoord);
    
    vProjCoord = floor(vProjCoord);
    
    float3 tcShadowCoord[8] = 
    {
        vProjCoord + float3( 0, 0, 0),  //0
        vProjCoord + float3( 0, 0, 1),  //1
        vProjCoord + float3( 0, 1, 0),  //2
        vProjCoord + float3( 0, 1, 1),  //3
        vProjCoord + float3( 1, 0, 0),  //4
        vProjCoord + float3( 1, 0, 1),  //5
        vProjCoord + float3( 1, 1, 0),  //6
        vProjCoord + float3( 1, 1, 1)   //7
    };
        
    //  this needs heavy optimization!!
    vShadowDepth[0] = texCUBE(inShadowMap, tcShadowCoord[0]).xy;
    vShadowDepth[1] = texCUBE(inShadowMap, tcShadowCoord[2]).xy;
    vShadowDepth[2] = texCUBE(inShadowMap, tcShadowCoord[4]).xy;
    vShadowDepth[3] = texCUBE(inShadowMap, tcShadowCoord[6]).xy;
    vShadowDepth[4] = texCUBE(inShadowMap, tcShadowCoord[1]).xy;
    vShadowDepth[5] = texCUBE(inShadowMap, tcShadowCoord[3]).xy;
    vShadowDepth[6] = texCUBE(inShadowMap, tcShadowCoord[5]).xy;
    vShadowDepth[7] = texCUBE(inShadowMap, tcShadowCoord[7]).xy;
    
    float maxDepth = max(max(vShadowDepth[0].x, vShadowDepth[1].x), max(vShadowDepth[2].x, vShadowDepth[3].x));
		  maxDepth = max(maxDepth,max(max(vShadowDepth[4].x, vShadowDepth[5].x), max(vShadowDepth[6].x, vShadowDepth[7].x)));

	// lerp in y direction 
	return float3(lerp(
				  lerp(lerp(vShadowDepth[0], vShadowDepth[1], vWeights.y),
					  lerp(vShadowDepth[2], vShadowDepth[3], vWeights.y), vWeights.x),
				  lerp(lerp(vShadowDepth[4], vShadowDepth[5], vWeights.y),
						lerp(vShadowDepth[6], vShadowDepth[7], vWeights.y), vWeights.x),
						vWeights.z), maxDepth);
}

//--------------------------------------------------------------------------------------
//
// Cube map filtering is based on the article "Floating Point Cube Maps" by
// Arkadiusz Waliszewski in ShaderX2
// keeps track of maximum depth value as well to restrict variance shadow maps
//
//--------------------------------------------------------------------------------------
float4 CubeBlurFilterChernoff(samplerCUBE inShadowMap, float3 inTex)
{
    float4 vShadowDepth[8];
    
    // compute a bunch of texture coordinates per-pixel
    // a simple unweighted jittered lookup could be computed in the vertex shader
    // and interpolated per-pixel, but it wouldn't look as good.
    float fMa = max(abs(inTex.x), max(abs(inTex.y), abs(inTex.z)));
    float3 vProjCoord = (inTex / fMa) * 512.0f;
    float3 vWeights = frac(vProjCoord);
    
    vProjCoord = floor(vProjCoord);
    
    float3 tcShadowCoord[8] = 
    {
        vProjCoord + float3( 0, 0, 0),  //0
        vProjCoord + float3( 0, 0, 1),  //1
        vProjCoord + float3( 0, 1, 0),  //2
        vProjCoord + float3( 0, 1, 1),  //3
        vProjCoord + float3( 1, 0, 0),  //4
        vProjCoord + float3( 1, 0, 1),  //5
        vProjCoord + float3( 1, 1, 0),  //6
        vProjCoord + float3( 1, 1, 1)   //7
    };
        
    vShadowDepth[0] = texCUBE(inShadowMap, tcShadowCoord[0]);
    vShadowDepth[1] = texCUBE(inShadowMap, tcShadowCoord[2]);
    vShadowDepth[2] = texCUBE(inShadowMap, tcShadowCoord[4]);
    vShadowDepth[3] = texCUBE(inShadowMap, tcShadowCoord[6]);
    vShadowDepth[4] = texCUBE(inShadowMap, tcShadowCoord[1]);
    vShadowDepth[5] = texCUBE(inShadowMap, tcShadowCoord[3]);
    vShadowDepth[6] = texCUBE(inShadowMap, tcShadowCoord[5]);
    vShadowDepth[7] = texCUBE(inShadowMap, tcShadowCoord[7]);
    
    //float maxDepth = max(max(vShadowDepth[0].x, vShadowDepth[1].x), max(vShadowDepth[2].x, vShadowDepth[3].x));
	//	  maxDepth = max(maxDepth,max(max(vShadowDepth[4].x, vShadowDepth[5].x), max(vShadowDepth[6].x, vShadowDepth[7].x)));

	// lerp in y direction 
	return float4(lerp(
				  lerp(lerp(vShadowDepth[0], vShadowDepth[1], vWeights.y),
					  lerp(vShadowDepth[2], vShadowDepth[3], vWeights.y), vWeights.x),
				  lerp(lerp(vShadowDepth[4], vShadowDepth[5], vWeights.y),
						lerp(vShadowDepth[6], vShadowDepth[7], vWeights.y), vWeights.x),
						vWeights.z));
}


//--------------------------------------------------------------------------------------
//
// Variance Shadow maps based on the papers at
// http://www.punkuser.net/vsm/
//
//--------------------------------------------------------------------------------------
float CompareDepthVarianceShadow(samplerCUBE inShadowMap, float3 inTex, float fDistToLight)
{
	float3 moments;

	// cube map look-up
	//moments = texCUBE(inShadowMap, inTex).xy;
	moments = CubeBlurFilter(inShadowMap, inTex);

	// Rescale light distance and check if we're in shadow
	float RescaledDistToLight = fDistToLight / gLightAttenuationEnd.x;

	RescaledDistToLight -= fDepthBias;
	
	// Variance shadow mapping
	// this is the power of 2 depth value
	float E_x2 = moments.y;
	
	// this is the original depth value taken to power of 2
	float Ex_2 = moments.x * moments.x;

	// subtract those from each other to get the variance and add a epsilon value to overcome precision problems
	float variance = (E_x2 - Ex_2) + fLightVSMEpsilon;

	// subtract the current depth value from the previous one
	float m_d = (moments.x - RescaledDistToLight);

	// Chebyshev's inequality
	float p = variance / (variance + m_d * m_d);
		
	// pick the result from a standard shadow map test or the variance shadow map
	// runs p_max only on the penumbra
	return max(m_d > 0.0, p);
}

//--------------------------------------------------------------------------------------
//
// Variance Shadow maps based on Chernoff
//
//--------------------------------------------------------------------------------------
float CompareDepthVarianceShadowChernoff(samplerCUBE inShadowMap, float3 inTex, float fDistToLight)
{
	float4 moments;

	// cube map look-up
	//moments = texCUBE(inShadowMap, inTex);
	//moments = CubeBlurFilter(inShadowMap, inTex);
	moments = CubeBlurFilterChernoff(inShadowMap, inTex);

	// Rescale light distance and check if we're in shadow
	float RescaledDistToLight = fDistToLight / gLightAttenuationEnd.x;

	RescaledDistToLight -= fDepthBias;

	float epsilon = max(0, RescaledDistToLight - moments.x);
	
	// size of filter kernel (if you multiply it by 2 it looks cooler)
#define PIPPO (8)
	
	return exp(-epsilon * epsilon * (PIPPO + 1)*(PIPPO + 1) * 8);	
}

//------------------------------------------------------
// Cube map Depth Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( vertexOutputComposite IN ) : COLOR
{

	float fDist = length(IN.vLight);

	if(gPointLightAttenuation > fDist)
	{
	#if 0
		float fDepth = fDist * fDepthBias;
		
		// compare shadow map content with light distance without any filter
	//	return CompareDepth(CubeShadowMapSampler, IN.vLight.xyz, fDepth);
		
		// compare shadow map content with light distance with a 8-tap filter
		return CompareDepthFiltered(CubeShadowMapSampler, IN.vLight.xyz, fDepth);
	#else
		//return CompareDepthVarianceShadow(CubeShadowMapSampler, IN.vLight.xyz, fDist);
		return CompareDepthVarianceShadowChernoff(CubeShadowMapSampler, IN.vLight.xyz, fDist);
	#endif
	}
	else
		return float4(1.0, 1.0, 1.0, 1.0);
}


technique drawskinned
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
//        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CW;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
//        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CW;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_Composite();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique unlit_drawskinned
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
//        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CW;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
//        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CW;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_Composite();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}
