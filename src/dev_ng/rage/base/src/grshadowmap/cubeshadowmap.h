// 
// grshadowmap/cubeshadowmap.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: Cubic Shadow maps: store distance between light and vertex + 8-tap filter for nice penumbra
//
#ifndef GRSHADOWMAP__CUBESHADOWMAP_H
#define GRSHADOWMAP__CUBESHADOWMAP_H

#include "atl/array.h"
#include "atl/ownedptr.h"
#include "bank/bkmgr.h"
#include "data/base.h"
#include "grcore/effect.h"
#include "parser/manager.h"
#include "vector/matrix44.h"
#include "grcore/viewport.h"
#include "grcore/texture.h"
#include "grcore/im.h"

#include "shadowcollector.h"

namespace rage
{
	class bkBank;
	class grcRenderTarget;
	class grcViewport;
	class grmShader;
	class Color32;

	 // enum that holds the size of the cube shadow map for the PC and 360
	enum 
	{
		PC_CUBESHADOWMAP_SIZE		= 1024,
		// only power of 2 allowed
		PS3_CUBESHADOWMAP_SIZE		= 512,
		XENON_CUBESHADOWMAP_SIZE	= 256,
	};


class CShadowedPointLight
{
public:
	enum {MAX_LIGHTS=8};

	static const char* sm_NumberNames[MAX_LIGHTS];

	CShadowedPointLight();

	CShadowedPointLight(int whichLight);

	void SetDefaults(int whichLight);

	const char* GetNumberName() const;

	void AddWidgets(bkBank& bank);

	// which light is used
	int	m_WhichLight;

	// this is a neat trick to use the shadow map precision better
	// we scale the size of the geometry that gets drawn into the map a bit
	float m_GeometryScale;

	// attenuation end of the shadow fade out
	float m_LightAttenuationEnd;

	// depth bias for the cube shadow maps
	float m_fDepthBias;

	// variance shadow map 
	float m_fLightVSMEpsilon;

    

	// PURPOSE
	//	color buffer used in conjunction to the depth render target
	grcRenderTarget* m_pCubeShadowMap;

	PAR_SIMPLE_PARSABLE;
};


class CCubeShadowMap : public datBase
{
public:
	//
	// PURPOSE
	//	constructor
	// PARAMS
	//	
	//
	CCubeShadowMap();
	CCubeShadowMap(int,bool);

	// destructor
	virtual	~CCubeShadowMap(void);

	//
	// PURPOSE
	//	creates and loads the shaders
	// PARAMS
	//	path - the path to load the shaders from.  If it == NULL, use the currently set path.
	//
	void InitShaders(const char *path=NULL); 

	void SetLightIndexOffset( int idx ) 
	{ 
		FastAssert( idx >= 0 && idx <= 3 );
		m_LightIndexOffset = idx; 
	}

	// 
	// PURPOSE
	//	allocates the shadow map render targets
	// PARAMS
	//	
	//	
	// NOTES
	//	if the device changes or the user wants a different resolution: 
	// delete and create new shadow map
	void	CreateRenderTargets();

	//
	// PURPOSE
	//	frees resources that were occupied by the CreateRenderTargets() call
	// RETURNS
	//	none
	void	DeleteRenderTargets(void);

	//
	// PURPOSE
	//	renders into all faces of a cube shadowmap tiles.
	// PARAMS
	// the light number
	void	RenderIntoCubeShadowMap();


	// PURPOSE
	//	get a pointer to the shadow depth shader
	// this shader does not render depth values, it renders distance values that are compared similar to depth values later
	// RETURNS
	//	a pointer to a grmShader object describing the depth shader
	grmShader* GetCubeDepthShader() const { return m_CubeDepthShader; }

	// PURPOSE
	//	get a pointer to the cube shadow map preview shader
	// RETURNS
	//	a pointer to a grmShader object describing the cube shadow map preview shader
	grmShader* GetCubeDepthPreviewShader() const { return m_CubeDepthPreviewShader; }

	// PURPOSE
	//	get a pointer to the shader that renders into a screenspace 2D texture
	// RETURNS
	//	a pointer to a grmShader object describing the shader that renders/alpha blends into a 2D texture
	grmShader* GetCubeBlendShader() const { return m_CubeBlendShader; }

	// array of viewports
	atArray<grcViewport*> m_Viewports;

	// PURPOSE
	//	get a pointer to a viewport
	// RETURNS
	//	a pointer to a grcViewport object that comes from an array of viewports
	grcViewport* GetShadowViewports(int i) { return m_Viewports[i]; }

	// PURPOSE
	//	old fashioned widget code ... will be removed by the new widget class
#if __BANK
	virtual void AddSaveWidgets(bkBank& bk);
	virtual void AddWidgets(bkBank &bk);
#endif

#if __DEV
	template <typename _Type> void SaveTunablesCB()
	{
		char fileName[256];
		strcpy(fileName,"CubeShadowMap");
		if (BANKMGR.OpenFile(fileName,256,"*.xml", true, "Cube Shadow Map Tunables (*.xml)"))
		{
			SaveTunables<_Type>(fileName);
		}
	}

	template <typename _Type> void LoadTunablesCB()
	{
		char fileName[256];
		strcpy(fileName,"CubeShadowMap");
		if (BANKMGR.OpenFile(fileName,256,"*.xml", false, "Cube Shadow Map Tunables (*.xml)"))
		{
			LoadTunables<_Type>(fileName);
		}
	}
#endif

	// PURPOSE: function that renders all shadow casters.  Derived class needs 
	// to override this function.
#if __XENON
	virtual void RenderShadowCasters(grcViewport &vp, bool /*isCasting*/) {vp.SetNormWindow(0, 0, XENON_CUBESHADOWMAP_SIZE, XENON_CUBESHADOWMAP_SIZE);};
#else
	virtual void RenderShadowCasters(grcViewport &vp, bool /*isCasting*/) {vp.SetNormWindow(0, 0, PC_CUBESHADOWMAP_SIZE, PC_CUBESHADOWMAP_SIZE);};
#endif

	template <typename _Type> bool LoadTunables(const char* filename)
	{
		return PARSER.LoadObject(filename,"xml",reinterpret_cast<_Type&>(*this));
	}

#if __DEV
	template <typename _Type> bool SaveTunables(const char* filename)
	{
		return PARSER.SaveObject(filename,"xml",reinterpret_cast<const _Type*>(this),parManager::XML);
	}
#endif

	// PURPOSE
	//	renders objects into a 2D screensize texture ... projects from the light point of view and renders depth values only
	// PARAMS
	//	NONE
	void RenderShadowsIntoShadowCollector();

	// PURPOSE
	//	used inside of RenderShadowsIntoShadowCollector(); forces the usage of 
	//  the m_CubeBlendShader shader and calls RenderShadowCasters()
	// PARAMS
	//	NONE
	void RenderCubeShadowMapIntoShadowCollector();

	// PURPOSE
	//	used inside of RenderShadowsIntoShadowCollector(); corresponding call to BeginRenderCubeShadowMapIntoColorBuffer()
	// PARAMS
	//	NONE
	void EndRenderCubeShadowMapIntoShadowCollector();

	// PURPOSE
	//	used inside of RenderShadowsIntoShadowCollector(); sets all the variables and render states
	// PARAMS
	//	the light
	void BeginRenderCubeShadowMapIntoShadowCollector(int whichLight);

	// PURPOSE
	//	previews the content of the cube shadow map by projecting it into a cube and displaying it full screen
	// PARAMS
	//	NONE
	void ShowCubeShadowRenderTargets();

	// PURPOSE
	//	blurs the cube map
	// PARAMS
	//	NONE
	void BlurCubeShadowRenderTargets();

	// toggle shadow map preview
	bool m_ShowCubeShadowMap;
	bool m_BlackNWhiteMap;
	bool m_bShadowCollector;

private:

	// PURPOSE
	//	provides a preview on the 2D screen-space black & white texture
	// RETURNS
	//	nothing
	void ShowShadowMap(grcTexture * texture, 
						grcEffectTechnique technique,					// technique in FX file
						float DestX, float DestY, float SizeX, float SizeY);	

	// PURPOSE
	//	draws into the faces of the cube shadow map; uses m_CubeDepthShader
	// this shader does not render depth values, it renders distance values
	// RETURNS
	//	nothing
	void DrawShadow(grcCubeFace face, int whichLight);

	// PURPOSE
	//	blits into a cube map
	// RETURNS
	//	nothing
	void BlitIntoFace(grcRenderTarget *CubeRenderTarget,
							grcCubeFace face, 
							grmShader *Shader,
							grcEffectTechnique technique);

	// PURPOSE
	//	renders the distance from the light to the vertex into the cube map
	grmShader * m_CubeDepthShader;	

	// PURPOSE
	//	renders into a cube to give the user a preview
	grmShader * m_CubeDepthPreviewShader;

	// PURPOSE
	//	renders into a 2D texture
	grmShader * m_CubeBlendShader;


	// light matrix
	// holds light direction in c
	// holds light position in d
	Matrix34 m_LightMatrix;

	// array of point lights that get cube shadow map applied to
	atArray<atOwnedPtr<CShadowedPointLight> > m_ShadowedPointLights;

	// temp pointer to store original viewport
	const grcViewport*	m_OldVP;

	// scale preview
	float m_fScalePreview;

	// handles for global and local variables
	grcEffectGlobalVar	m_CubeShadowMapPreviewID;	// handle for the preview
	grcEffectVar		m_RenderMapId;				// handle for workhorse render target sampler stages
	grcEffectTechnique	m_CopyTechnique;			// technique that just copies 1:1 render targets
	grcEffectTechnique	m_DrawTechnique;			// technique for the cube shadow map preview
	grcEffectTechnique	m_CubeBlurTechnique;		// technique to blur a cube map

	// position of light source
	Vector3 m_lightPos;

	// attenuation of point light
	float m_PointLightFalloff;

//	grcEffectGlobalVar	m_LightMatrixID;			// view and projection matrix

	grcEffectGlobalVar	m_PointLightFalloffID;		// handle to point light falloff
	grcEffectGlobalVar	m_PointLightPositionID;		// handle to the point light position
	grcEffectGlobalVar	m_GeometryScaleID;			// scaling value to scale geometry	
	grcEffectGlobalVar	m_LightAttenuationEndID;	// scaling value to scale geometry	
//	grcEffectGlobalVar	m_ShadowColorID;			// shadow color
	grcEffectVar		m_CubeShadowMapID;			// handle for the cube shadow map render target
	grcEffectVar		m_DepthBiasID;				// handle for the cube shadow map render target
	grcEffectVar		m_LightVSMEpsilonID;				// handle for the cube shadow map render target

	grcResolveFlags clearParams;

	// PURPOSE
	//	depth buffer used in conjunction to the color render target
	grcRenderTarget* m_DepthBuffer;

	int					m_LightIndexOffset;


	// this defines the parsable class
	PAR_SIMPLE_PARSABLE;
};

}	// name space range
#endif
