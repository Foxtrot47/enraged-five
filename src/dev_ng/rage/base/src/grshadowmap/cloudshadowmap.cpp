#include "cloudshadowmap.h"
#include "grprofile/pix.h"
#include "file/asset.h"
#include "grcore/device.h"
#include "grcore/gprallocation.h"
#include "grcore/state.h"
#include "vector/Matrix34.h"
#include "bank/bank.h"


#if __XENON
#include "embedded_rage_cloudshadows_fxl_final.h"
#elif __PPU
#include "embedded_rage_cloudshadows_psn.h"
#elif __WIN32PC
#include "embedded_rage_cloudshadows_win32_30.h"
#endif



using namespace rage;


// Static variables
CloudShadowMap* CloudShadowMap::sm_instance = NULL;
grcRenderTarget* CloudShadowMap::sm_ExternalCloudShadowRT = NULL;
char* CloudShadowMap::sm_AssetPath = NULL;

// Other variables
Color32 white(1.0f,1.0f,1.0f,1.0f);



//---------------------------------------------------------------------------
//	Static method that allows an external texture to be used for cloud cover
//
//---------------------------------------------------------------------------
void CloudShadowMap::SetBaseTexture(grcRenderTarget* newTexture)
{
	sm_ExternalCloudShadowRT = newTexture;
}



//---------------------------------------------------------------------------
//
//
//---------------------------------------------------------------------------
#if __BANK
void CloudShadowParms::AddWidgets(bkBank &bank)
{
	bank.AddSlider("Cloud pattern scale", &CloudPatternScale, 0.0f, 10.0f, 0.001f, NullCB, "Scales the cloud pattern");
	bank.AddSlider("Cloud move speed X", &CloudPatternSpeed.x, -10.0f, 10.0f, 0.01f, NullCB, "Move speed X");
	bank.AddSlider("Cloud move speed Z", &CloudPatternSpeed.y, -10.0f, 10.0f, 0.01f, NullCB, "Move speed Z");
	bank.AddSlider("Cloud shadow strength", &CloudShadowStrength, 0.0f, 1.0f, 0.01f, NullCB, "Cloud shadow strength");
}
#endif

//---------------------------------------------------------------------------
//
//
//---------------------------------------------------------------------------
CloudShadowParms::CloudShadowParms()
{
	CloudPatternScale = 0.0f;
	CloudPatternSpeed.Set(0.0f, 0.0f);
	CloudShadowStrength = 0.0f;
}


//---------------------------------------------------------------------------
// CloudShadowParms operators
//
//---------------------------------------------------------------------------
namespace rage
{
	CloudShadowParms& CloudShadowParms::operator*=(float mag)
	{
		*this = *this * mag;
		return *this;
	}

	CloudShadowParms& CloudShadowParms::operator+=(const CloudShadowParms& other)
	{
		*this = *this + other;
		return *this;
	}

	CloudShadowParms operator+(const CloudShadowParms& a, const CloudShadowParms& b)
	{
		CloudShadowParms result;
		result.CloudPatternScale = a.CloudPatternScale + b.CloudPatternScale;
		result.CloudPatternSpeed = a.CloudPatternSpeed + b.CloudPatternSpeed;
		result.CloudShadowStrength = a.CloudShadowStrength + b.CloudShadowStrength;
		return result;
	}

	CloudShadowParms operator-(const CloudShadowParms& a, const CloudShadowParms& b)
	{
		return (a + (b * -1.0f));
	}

	CloudShadowParms operator*(const CloudShadowParms& a, const float mag)
	{
		CloudShadowParms result;
		result.CloudPatternScale = a.CloudPatternScale * mag;
		result.CloudPatternSpeed = a.CloudPatternSpeed * mag;
		result.CloudShadowStrength = a.CloudShadowStrength * mag;
		return result;
	}

	CloudShadowParms operator*(const float mag, const CloudShadowParms& b)
	{
		return (b * mag);
	}
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
CloudShadowMap::CloudShadowMap()
{
	AssertMsg(!sm_instance, "CloudShadowMap: Only one instance of this class is allowed!");
	if (!sm_instance)
	{
		sm_instance = this;
	}

	sm_AssetPath = NULL;
	m_CloudShadowShader = NULL;
	m_CloudShadowRT = NULL;
	m_DepthBufferRT = NULL;
	m_DepthBuffer = NULL;
	m_externalCopyTech = grcetNONE;
	m_blurTech = grcetNONE;
	m_drawIntoCollectorTech = grcetNONE;
	m_CloudSamplerId = grcevNONE;
	m_DepthBufferId = grcevNONE;
	m_ProjInverseTransposeId = grcevNONE;
	m_CloudPatternScaleId = grcevNONE;

	m_cloudTexture = NULL;
	m_CloudScrollXZ.Set(0.0f, 0.0f);
	m_Settings.CloudPatternSpeed.Set(0.0f, 0.0f);
	m_Settings.CloudPatternScale = 1.0f;
	m_Settings.CloudShadowStrength = 1.0f;


	// $DEBUG$
	sm_AssetPath = "$/tune/fx";
	m_Settings.CloudPatternScale = 0.077f;
	m_Settings.CloudPatternSpeed.Set(0.01f, 0.03f);
	m_Settings.CloudShadowStrength = 1.0f;
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
CloudShadowMap::~CloudShadowMap()
{
	DeleteRenderTargets();
}


//---------------------------------------------------------------------------
//
//
//---------------------------------------------------------------------------
void CloudShadowMap::ReloadTextures()
{
	if (sm_AssetPath)
	{
		if (m_cloudTexture)
		{
			m_cloudTexture->Release();
			m_cloudTexture = NULL;
		}

		ASSET.PushFolder(sm_AssetPath);
		if (ASSET.Exists("CloudShadows", "dds"))
		{
			m_cloudTexture = grcTextureFactory::GetInstance().Create("CloudShadows.dds");
		}
		ASSET.PopFolder();
	}
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
#if __BANK
void CloudShadowMap::AddWidgets(bkBank& bank)
{
	bank.PushGroup("Cloud shadows");
	bank.AddSlider("Cloud pattern scale", &m_Settings.CloudPatternScale, 0.0f, 10.0f, 0.001f, NullCB, "Scales the cloud pattern");
	bank.AddSlider("Cloud move speed X", &m_Settings.CloudPatternSpeed.x, -10.0f, 10.0f, 0.01f, NullCB, "Move speed X");
	bank.AddSlider("Cloud move speed Z", &m_Settings.CloudPatternSpeed.y, -10.0f, 10.0f, 0.01f, NullCB, "Move speed Z");
	bank.AddSlider("Cloud shadow strength", &m_Settings.CloudShadowStrength, 0.0f, 1.0f, 0.01f, NullCB, "Cloud shadow strength");
	bank.AddButton("Reload textures", datCallback(MFA(CloudShadowMap::ReloadTextures), this));
	bank.PopGroup();
}
#endif

//---------------------------------------------------------------------------
// Handy blitting routine lifted from postfx.  Thanks Wolf!
//
//---------------------------------------------------------------------------
void CloudShadowMap::BlitToRT(
	grcRenderTarget* target,		// Dest
	grcTexture* texture,			// Source texture
	grcEffectTechnique technique,	// Technique to use
	grcEffectVar textureId,			// Sampler to rig source texture to (see your technique)
	const Color32* color,			// vertex color (NULL will use white)
	float x1,						// x1 - Destination base x
	float y1,						// y1 - Destination base y
	float x2,						// x2 - Destination opposite-corner x
	float y2)						// y2 - Destination opposite-corner y
{
	if (color == NULL)
	{
		color = &white;
	}

	// If we're using a target render target other than the full screen, we need to lock (and unlock) it
	if (target)
	{
		grcTextureFactory::GetInstance().LockRenderTarget(0, target, NULL);
	}

	// Junction the source texture to the sampler
	if ((textureId != grcevNONE) && (texture != NULL))
	{
		m_CloudShadowShader->SetVar(textureId, texture);
	}

	if (texture != NULL)
	{
		// Provide the size of the target or source render target to the shader
		float width = float(GRCDEVICE.GetWidth());
		float height = float(GRCDEVICE.GetHeight());
		m_CloudShadowShader->SetVar(m_TexelSizeId, Vector4(1.0f/width, 1.0f/height, width, height));
	}

	// Blit
	m_CloudShadowShader->TWODBlit(
		x1,				// x1 - Destination base x
		y1,				// y1 - Destination base y
		x2,				// x2 - Destination opposite-corner x
		y2,				// y2 - Destination opposite-corner y
		0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
		0.0f,			// u1 - Source texture base u (in normalized texels)
		0.0f,			// v1 - Source texture base v (in normalized texels)
		1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
		1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
		*color,			// color - reference to Packed color value
		technique);

	// Release texture
	if ((textureId != grcevNONE) && (texture != NULL))
	{
		m_CloudShadowShader->SetVar(textureId, (grcTexture*)grcTexture::None);
	}

	// Unlock the target render target
	if (target)
	{
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
	}
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CloudShadowMap::InitShaders(const char* /*path*/)
{
	m_CloudShadowShader = grmShaderFactory::GetInstance().Create();
	m_CloudShadowShader->Load("rage_cloudshadows");

	m_drawIntoCollectorTech = m_CloudShadowShader->LookupTechnique("DrawIntoCollector");
	m_CloudSamplerId = m_CloudShadowShader->LookupVar("CloudShadowTex");
	m_DepthBufferId = m_CloudShadowShader->LookupVar("DepthMapTex");
	m_ProjInverseTransposeId = m_CloudShadowShader->LookupVar("WorldViewProjInverse");
	m_CloudPatternScaleId = m_CloudShadowShader->LookupVar("CloudPatternScale");
	m_NearFarClipPlaneQId = m_CloudShadowShader->LookupVar("NearFarClipPlaneQ");
	m_TexelSizeId = m_CloudShadowShader->LookupVar("TexelSize");
	m_ScrollXZId = m_CloudShadowShader->LookupVar("ScrollXZ");
	m_CloudShadowStrengthId = m_CloudShadowShader->LookupVar("CloudShadowStrength");
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CloudShadowMap::CreateRenderTargets()
{
	// Set up the main render target
	grcTextureFactory::CreateParams cp;
	int legSize;
#if __XENON
	legSize = XBOX360_CLOUDSHADOWMAP_SIZE;
#else
	legSize = PS3_CLOUDSHADOWMAP_SIZE;
#endif
	cp.Multisample = 0;
	cp.IsResolvable = true;
	cp.HasParent = true;
	cp.Parent = NULL;
	cp.Format = grctfL8;
	cp.UseFloat = false;
#if __XENON
	if (legSize <= 1344)	// Magic from MC's collisionmap.cpp
		cp.UseHierZ = true;
	else
		cp.UseHierZ = false;
#endif
	grcRenderTargetType type = grcrtPermanent;
	m_CloudShadowRT = grcTextureFactory::GetInstance().CreateRenderTarget("Cloud shadow map", type, legSize, legSize, 32, &cp);
	AssertMsg(m_CloudShadowRT, "Could not create cloud shadows render target.");


	// Load a dummy cloud texture
	ReloadTextures();
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CloudShadowMap::RenderIntoShadowMap()
{
	grcViewport *vp = grcViewport::GetCurrent();
	//grcEffectTechnique tech;

	PIXBegin(0, "Render into cloud shadow map");

	// set the pixel shader threads to 112 -> vertex shader can only use 16 threads
#if __XENON
//	GRCGPRALLOCATION->SetGPRAllocation(16);
#endif

	// Set up render states
	grcViewport Viewport;
	Viewport.SetCameraIdentity();
	Viewport.SetWorldIdentity();
	grcViewport::SetCurrent(&Viewport);			// Orthogonal space

	if (sm_ExternalCloudShadowRT)
	{
		//m_CloudShadowShader->SetVar(textureID, sm_ExternalCloudShadowRT);

		m_CloudShadowShader->TWODBlit(
			-1.0f, 1.0f, 1.0f, -1.0f,		// Destination coordinates
			0.5f,							// Destination z-value
			0.0f, 0.0f, 1.0f, 1.0f,			// Source texture coordinates
			white,
			m_externalCopyTech);

		//	release texture again
		//m_CloudShadowShader->SetVar(textureID, (grcTexture*)grcTexture::None);
	}
	else
	{
		// Fall back on procedural cloud shadows if no external texture is provided?
	}


	// Restore old states
	grcViewport::SetCurrent(vp);


	// set the ratio between vertex and pixel shaders back to default
#if __XENON
//	GRCGPRALLOCATION->SetGPRAllocation(0); 
#endif

	PIXEnd();
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CloudShadowMap::DeleteRenderTargets()
{
	if(m_CloudShadowRT)
	{
		m_CloudShadowRT->Release();
		m_CloudShadowRT = NULL;	
	}

	if (m_cloudTexture)
	{
		m_cloudTexture->Release();
		m_cloudTexture = NULL;
	}
}


//---------------------------------------------------------------------------
//
//
//---------------------------------------------------------------------------
void CloudShadowMap::GetDepthBuffer()
{
#if __XENON
	m_DepthBuffer = (grcRenderTarget*)grcTextureFactory::GetInstance().GetBackBufferDepthForceRealize();
#else
	#if __PPU
		m_DepthBuffer = (grcRenderTarget*)grcTextureFactory::GetInstance().GetBackBufferDepth();
	#endif
#endif
}


//---------------------------------------------------------------------------
//
//
//---------------------------------------------------------------------------
void CloudShadowMap::ForceSettings(CloudShadowParms* parms)
{
	m_Settings = *parms;
}


//---------------------------------------------------------------------------
//
//
//---------------------------------------------------------------------------
void CloudShadowMap::RenderShadowsIntoShadowCollector(bool paused)
{
	// Early exit if shadows are too weak.  We could use FLT_EPSILON, but choosing our own value lets us optimize this for practical shadowing.
	float CloudEpsilon = 0.01f;
	if (m_Settings.CloudShadowStrength < CloudEpsilon)
	{
		m_Settings.CloudShadowStrength = 0.0f;
		// Shadow collector isn't being cleared every frame any longer (in WR 216)
		//return;
	}

	PIXBegin(0, "Render cloud shadows into Shadow Collector");


	// Cloud shadows draw into the second channel of our shadow collector (regular cast
	// shadows draw into the first channel) so mask all the other channels before
	// we draw.
	grcColorWrite oldColorWrite = grcState::GetColorWrite();

	grcState::SetColorWrite(grccwGreen);


	// Grab the depth buffer
	GetDepthBuffer();
	m_CloudShadowShader->SetVar(m_DepthBufferId, m_DepthBuffer); 	

	// Pass down the inverse full composite matrix, so we can determine worldspace coordinates in the pixel shader.
	const grcViewport *vp = grcViewport::GetCurrent();
	grcViewport::SetCurrentWorldIdentity();
	Mat44V modelViewInv;
	InvertFull(modelViewInv,vp->GetFullCompositeMtx());		// DANGER, hope this is correct

	m_CloudShadowShader->SetVar(m_ProjInverseTransposeId, MAT44V_TO_MATRIX44(modelViewInv));

	// for the Z-buffer support get near and far clipping plane from the viewport
	//
	// What we do here is transform the Z value in the Z buffer from NDC to worldviewproj = camera space
	// To achieve this, we implement the following formula in the pixel shader
	// z = -(n * f) / (z_p * (f - n) - f)
	//
	// Q = Zf / Zf - Zn
	// z = Zn * Q / Zd - Q
	// this value is positive because we have an inverted z axis ... so the z axis is opengl style while the rest
	// is D3D style
	// whereas Zd is the value coming from the depth buffer
	Vector3 PlanesAndQ(vp->GetNearClip(), vp->GetFarClip(), vp->GetFarClip() / (vp->GetFarClip() - vp->GetNearClip()));
	m_CloudShadowShader->SetVar(m_NearFarClipPlaneQId, PlanesAndQ);

	// Set other important variables
	if (!paused)
	{
		m_CloudScrollXZ += (m_Settings.CloudPatternSpeed * 0.01f); // Order of magnitude down!
	}
	m_CloudShadowShader->SetVar(m_CloudPatternScaleId, m_Settings.CloudPatternScale * m_Settings.CloudPatternScale * m_Settings.CloudPatternScale);	// To give us finer control at low levels
	m_CloudShadowShader->SetVar(m_ScrollXZId, m_CloudScrollXZ);
	m_CloudShadowShader->SetVar(m_CloudShadowStrengthId, m_Settings.CloudShadowStrength);


	// Draw
	BlitToRT(NULL, m_cloudTexture, m_drawIntoCollectorTech, m_CloudSamplerId);


	// Reset old states
	grcState::SetColorWrite(oldColorWrite);
	PIXEnd();
}
