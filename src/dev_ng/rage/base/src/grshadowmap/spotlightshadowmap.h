// 
// grshadowmap/spotlightshadowmap.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 




#ifndef SPOT_SHADOW_MAP_H
#define SPOT_SHADOW_MAP_H
	

#include <vector>

#include "atl/ptr.h"
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "bank/bank.h"
#include "grcore/viewport.h"

namespace rage
{


	// PURPOSE
	//	renders out a shadow map for a spot light
	//
	class CShadowedSpotLight
	{
		static const int ShadowMapMinSize = 64;
		static const int ShadowMapMaxSize = 512;
	public:

		enum ShadowType
		{
			STANARD,
			VARIANCE,
			COMPRESSED,
			MULTISAMPLED
		};

		CShadowedSpotLight() 
			: m_shadowMap(0), m_oldView(0), m_DepthShader(0), m_type( STANARD ), m_shadowDepthMap(0), m_filterSize( 1.0f ),
				m_direction( VEC3_ZERO), m_position( VEC3_ZERO ),m_dirty( false )
		{}

		~CShadowedSpotLight()
		{
			delete m_DepthShader;
		}
		void Init( int size = 256, const char* name = 0, ShadowType m_type =  STANARD, bool  useAtlas= false, int atlasIndex = 0 );

		void SetRenderTargets( const grcRenderTarget*  shadowMap, const grcRenderTarget* shadowDepthMap );
		
		void CreateSpotLight( const Vector3& position, const Vector3& direction, float fov , const Vector2& depthRange );
		void CreateDirectionalLight( const Vector3& position, const Vector3& direction, const Vector3& rmin, const Vector3& rmax );

		void SetFilterSize( float filterSize )	{	m_filterSize = filterSize; }

		void Begin( bool performClear = false );
		void End( bool blur = true );

		void Show(  int DestX, int DestY, int SizeX, int SizeY );

		const grcTexture*			GetTexture()	const	{ return m_shadowMap != 0 ? m_shadowMap : m_shadowDepthMap; }
		const Matrix44&		GetMatrix()		const	{ return m_ShadowViewProj; }
		const Vector4&		GetCompressedSpotMatrix( float& v ) const { v = m_yOffset; return m_ShadowCompressed; }

		const grcViewport&	GetViewport()	const	{ return m_shadowView; }
		grcViewport&		GetViewport()			{ return m_shadowView; }	// non const version for shadow cache usage
		float				GetRange()		const	
		{ 
			return m_range.y - m_range.x;
		}
		float	GetStart()	const
		{ 
			return  m_range.x;
		}

		void MarkDirty() { m_dirty = true; }
		void ClearDirty() { m_dirty = false; }
		bool IsDirty() const { return m_dirty; }

		void ResetShadowTextureSize( int size, const char* name );

		bool IsSame( const Vector3& pos, const Vector3& dir )	{ return pos == m_position &&  dir == m_direction; }
	private:

		const grcRenderTarget*			GetRenderTarget()	const	{ return m_shadowMap != 0 ? m_shadowMap : m_shadowDepthMap; }

		grcViewport*			m_oldView;
		grcViewport				m_shadowView;
		Matrix44				m_ShadowViewProj;
		Vector4					m_ShadowCompressed;
		float					m_yOffset;

		ShadowType				m_type;
		Vector2					m_range;
		float					m_filterSize;

		grcRenderTarget*		m_shadowMap;
		grcRenderTarget*		m_shadowDepthMap;

		int						m_atlasIndex;
		bool					m_useAtlas;

		// The shader used for rendering to depth targets
		grmShader*				m_DepthShader;	

		grcEffectVar	m_PointLightPositionID;		// handle to the point light position
		//grcEffectGlobalVar	m_GeometryScaleID;			// scaling value to scale geometry	
		grcEffectVar	m_LightAttenuationEndID;	// Light falloff
		grcEffectVar		m_LightAttenuationStartID;	// Light falloff
	

		Vector3				m_position;			// store these for caching purposes
		Vector3				m_direction;		//


		bool				m_dirty;			// dirty flag

		void CreateShadowRenderTarget( int size, const char* name= 0 );
	};

#ifndef SHADCACHE_SETLIGHTIDTYPE
typedef	int LightIDType;
#endif


	// PURPOSE
	//	stores generated shadow maps in a cache
	//
	class CShadowCache
	{
		typedef std::vector<CShadowedSpotLight*>	ShadowList;
		ShadowList								m_shadows;
		int										m_usedCnt;
		bool									m_useAtlas;
	
		typedef std::vector<LightIDType>		LightLookupList;
		LightLookupList							m_lightToCacheBucket;

		bool									m_show;
		bool									m_blur;
		bool									m_showShadowCacheView;
		int										m_viewNumber;
		
		CShadowedSpotLight::ShadowType			m_type;

		atScopedPtr<grcRenderTarget>		m_shadowMap;
		atScopedPtr<grcRenderTarget>		m_shadowDepthMap;


		Vector2								m_maxSpotShadowRange;

		void CreateShadowAtlas( int size , int amt ,CShadowedSpotLight::ShadowType& type );

		struct MatchLightShadow
		{
			CShadowedSpotLight*	m_shadow;
			MatchLightShadow( CShadowedSpotLight*  spot ) : m_shadow( spot) {}

			template<class T> 
			bool operator()( const T& light )
			{
				return m_shadow->IsSame( light.GetPosition(), light.GetDirection() );
			}
		};
	public:
		CShadowCache() 
			: m_viewNumber(0), 
			m_usedCnt(0), 
			m_show( false ), m_showShadowCacheView( false ), m_blur( true ),
			m_maxSpotShadowRange( 145.0f,  10.0f)
		{}

	
		void Init( int MaxShadows = 16, int width = 256 , CShadowedSpotLight::ShadowType type  = CShadowedSpotLight::STANARD, bool useAtlas = false )
		{
			Remove();

			m_useAtlas = useAtlas;
			m_type = type;
			if ( m_useAtlas )
			{
				CreateShadowAtlas( width , MaxShadows , type);
			}
			m_lightToCacheBucket.reserve( MaxShadows);
			m_shadows.resize( MaxShadows );

			for ( int i = 0; i < MaxShadows; i++ )
			{
				char depthName[128];
				sprintf(depthName,"Cached Depth Buffer%d",i);
				CShadowedSpotLight* shad = rage_new CShadowedSpotLight();
				
				shad->Init( width,  depthName, type, m_useAtlas, i  );
				if ( m_useAtlas )
				{
					shad->SetRenderTargets( m_shadowMap.GetPtr(), m_shadowDepthMap.GetPtr() );
				}

				m_shadows[i] = shad;
			}
			Reset();
		}
		void Remove()
		{
			for ( size_t i = 0; i < m_shadows.size(); i++ )
			{
				delete m_shadows[i];
			}
			m_shadows.clear();
		}
		int size() { return (int)m_shadows.size(); }

		const grcTexture*			GetTexture()	const	{ return m_shadowMap.GetPtr() ? m_shadowMap.GetPtr() : m_shadowDepthMap.GetPtr(); }


		~CShadowCache()	
		{ 
			Remove(); 
		}
		void Reset()		
		{  
			m_usedCnt = 0; 
			m_lightToCacheBucket.resize( 0);	// doesn't reclaim memory
		}


		int GetLightShadowMapIndex( LightIDType lightId ) const
		{
			AssertMsg( std::count( m_lightToCacheBucket.begin(), m_lightToCacheBucket.end(), lightId ) > 0 , "Light not in cache" );
			return std::distance( m_lightToCacheBucket.begin(),
				std::find( m_lightToCacheBucket.begin(), m_lightToCacheBucket.end(), lightId  ));
		}

		const CShadowedSpotLight&	operator[]( int lightId ) const
		{
			return *m_shadows[ GetLightShadowMapIndex(lightId) ];
		}
		// add a spot light to cache
		bool AddSpot( int lightId, const Vector3& position, const Vector3& direction, float fov, 
							const Vector2& depthRange, int shadowSize = 256 ,
							float filterSize = 1.0f)
		{
			int id = m_usedCnt;
			LightLookupList::iterator itor = std::find( m_lightToCacheBucket.begin(), m_lightToCacheBucket.end(), lightId );
			if ( itor == m_lightToCacheBucket.end() )	
			{
				//  not in cache so add it in
				if ( m_usedCnt >= (int)m_shadows.size() )
				{
					Warningf( "Number of shadows required exceeds shadow cache size" );
					return false;
				}
				m_lightToCacheBucket.push_back( lightId );
				m_usedCnt++;
				FastAssert( m_lightToCacheBucket.size() == (size_t)m_usedCnt);

			}
			else
			{
				id = std::distance( m_lightToCacheBucket.begin(), itor );
			}
			

			// clamp fov  to maximum
			fov = Min( fov, m_maxSpotShadowRange.x );
			m_shadows[id]->ResetShadowTextureSize( shadowSize, "shadowCache" );
			m_shadows[id]->SetFilterSize( filterSize );
			m_shadows[ id]->CreateSpotLight( position, direction, fov, depthRange);

			return true;
		}
		// add a spot light to cache
		template<class T>
		void UseCachedShadowsWithLightList(T& lights )
		{

			m_lightToCacheBucket.resize(0);

			// loop through 
			FastAssert( lights.GetCount() < 128 );
			bool hasShadow[ 128];

			memset( hasShadow, 0, sizeof( bool ) * lights.GetCount());

			// go through all the shadows and see if they match any lights
			int sameCnt = 0;
			for ( int i =0; i < m_usedCnt; i++ )
			{
				typename T::iterator itor = std::find_if( lights.begin(), lights.end(), MatchLightShadow( m_shadows[i] ));
				if ( itor != lights.end() )
				{
					// found a match so use that 
					std::swap( m_shadows[sameCnt], m_shadows[i] );	// move to front of list
					
					int lightIdx = std::distance( lights.begin(), itor );
					m_lightToCacheBucket.push_back( lightIdx ) ;
					hasShadow[ lightIdx ] = true;
					sameCnt++;
				}
			}
			// now reset all the unused shadows 
			m_usedCnt = sameCnt;
		
			for ( int i = 0; i < lights.GetCount(); i++ )
			{
				if ( !hasShadow[ i ]  && ( m_usedCnt < (int)m_shadows.size() ) )	// doesn't have a shadow so must add it
				{
					AddSpot( i, lights[i].GetPosition(), lights[i].GetDirection(), lights[i].GetAngleRange(), 
						lights[i].GetShadowRange(), lights[i].GetShadowSize() ,
						lights[i].GetShadowFilterSize() );
				}
			}
		}

		const grcTexture*	GetShadowMap( LightIDType lightId )
		{
			return m_shadows[ GetLightShadowMapIndex( lightId ) ]->GetTexture();
		}
		const Matrix44&	GetMatrix( LightIDType lightId )
		{
			return m_shadows[ GetLightShadowMapIndex( lightId ) ]->GetMatrix();
		}

		template<class FUNCTORTYPE>
		void RenderRequiredShadowMaps( FUNCTORTYPE& renderShadowCasters )
		{
		//	PIXBegin( "Shadow Cache -> creating shadow maps" );
			

			int FrameLimit = 8;
			int amtDone = 0;
			for ( size_t i = 0; i < m_shadows.size() && amtDone < FrameLimit; i++ )
			{
				if ( m_shadows[i]->IsDirty() )		// needs updating
				{
					m_shadows[i]->Begin( amtDone == 0 );

					renderShadowCasters( m_shadows[i]->GetViewport() );

					m_shadows[i]->End( m_blur );
					amtDone++;
				}
			}
		
		//	PIXEnd();
		}

		void	Show(int OriginX, int OriginY, int Width, int NumPerRow = 4  );	// for debugging
		Vector2 GetShadowTexScale();

#if __BANK
		void	AddWidgets( bkBank& bank )
		{
			bank.PushGroup("Shadow Cache");			// add lighting group

			bank.AddToggle( "Show Spot Light View", &m_showShadowCacheView );
			bank.AddToggle( "View Textures", &m_show );
			bank.AddToggle( "Apply Shadow Blur", &m_blur );
			
			bank.AddSlider( "Max Spot Shadow Range", &m_maxSpotShadowRange.x, 10.0f, 200.0f, 0.01f );
			bank.AddSlider( "Max Spot Shadow Fade", &m_maxSpotShadowRange.y, 0.0f, 60.0f, 0.01f );

			bank.AddSlider( "Shadow Number", &m_viewNumber, 0, 32 , 1);


			bank.PopGroup();
		}
#endif

		Vector2 GetSpotShadowMaxFade()
		{
			// convert to rads
			float angStart = (0.5f * ( m_maxSpotShadowRange.x )/360.0f) * 2.0f * 3.14f;
			float angEnd = (0.5f * ( m_maxSpotShadowRange.y )/360.0f) * 2.0f * 3.14f;

			angStart -= angEnd;
			
			Vector2 fadeIn;
			fadeIn.x  = 1.0f/( cos( angStart + angEnd) - cos( angStart  ) );
			fadeIn.y =	-cos( angStart  ) * fadeIn.x;
			return fadeIn;
		}

		void DebugDraw()
		{
			if ( m_show )
			{
				Show( 228, 10, 228 );
			}
			if ( m_showShadowCacheView )
			{
				if ( m_viewNumber < m_usedCnt )
				grcViewport::SetCurrent( &m_shadows[m_viewNumber]->GetViewport() );
			}
		}

	};



};



#endif
		
