// 
// grshadowmap/shadowcollector.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#include "shadowcollector.h"
#include "cascadedshadowmap.h"
#include "grprofile/pix.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/light.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "system/xtl.h"
#include "grcore/device.h"

#if __XENON
#include "embedded_rage_shadowcollector_fxl_final.h"
#include "embedded_rage_shadowdepth_fxl_final.h"
#elif __PPU
#include "embedded_rage_shadowcollector_psn.h"
#include "embedded_rage_shadowdepth_psn.h"
#elif __WIN32PC
#include "embedded_rage_shadowcollector_win32_30.h"
#include "embedded_rage_shadowdepth_win32_30.h"
#endif

namespace rage {

CShadowCollector *CShadowCollector::sm_instance = NULL;

//---------------------------------------------------------------------------
//  Constructor: initialize member variables
//
//---------------------------------------------------------------------------
CShadowCollector::CShadowCollector() :
	m_bShadowCollectorMap(false),
	m_ShadowCollector(NULL),
	m_ShadowCollectorDepthBuffer(NULL),
	m_iShadowCollectorChannel(0)
{
}

//static functions
void CShadowCollector::Init()
{
	Assert(sm_instance == NULL);
	sm_instance = rage_new CShadowCollector;

}

void CShadowCollector::Terminate()
{
	Assert(sm_instance);
	delete sm_instance;
	sm_instance = NULL;
}

//---------------------------------------------------------------------------
//  
// InitShaders()
// load shaders and shader variables
//
//---------------------------------------------------------------------------
void CShadowCollector::InitShaders(const char *path)
{
	if (path)
		ASSET.PushFolder(path);

	// 360 - fast blit into depth buffer. The depth buffer is then used as a shadow map
	// PC - a R32F shadow map is used to store the depth values
	const char *shaderName = "rage_shadowcollector";
	m_ShadowCollectorShader = grmShaderFactory::GetInstance().Create();
	m_ShadowCollectorShader->Load(shaderName);

	if (path)
		ASSET.PopFolder();

	// grab handle to copy technique
	m_CopyTechnique = m_ShadowCollectorShader->LookupTechnique("CopyRT");
	m_CopyTechnique1 = m_ShadowCollectorShader->LookupTechnique("CopyRTx");
	m_CopyTechnique2 = m_ShadowCollectorShader->LookupTechnique("CopyRTy");
	m_CopyTechnique3 = m_ShadowCollectorShader->LookupTechnique("CopyRTz");
	m_CopyTechnique4 = m_ShadowCollectorShader->LookupTechnique("CopyRTw");

	// lookup texture stages
	m_RenderMapId = m_ShadowCollectorShader->LookupVar("RenderMap");

	// lookup black and white map texture stages
	m_ShadowCollectorID = grcEffect::LookupGlobalVar("ShadowCollectorTex");
}


//---------------------------------------------------------------------------
//
//  ~CShadowCollector
//  deconstructor free shader resources
//
//---------------------------------------------------------------------------
CShadowCollector::~CShadowCollector(void)
{
	delete m_ShadowCollectorShader;
}

//---------------------------------------------------------------------------
//
//  CreateRenderTargets()
//  Allocate the memory for a cube depth map and a cube color map
//  Additionally allocate the memory for the 2D screenspace texture that 
//  is applied to the scene later
//
//---------------------------------------------------------------------------
#if __XENON
void CShadowCollector::CreateRenderTargets(bool predicatedTiling, grcRenderTarget* depthBuffer , bool multisample, bool /*msaa*/, bool useCompressed  )
#else
void CShadowCollector::CreateRenderTargets()
#endif
{
	int rezx,rezy;

	grcTextureFactory::CreateParams para2;

	//
	// set up the depth buffer for the shadow collector
	//
#if __XENON	
	para2.IsResolvable = false;  // don't need to resolve the depth buffer, so don't allocate the backing store in main memory	
	m_PredicatedTiled = predicatedTiling;

	if (!predicatedTiling)
	{
		para2.UseHierZ = true;
	}

	para2.HasParent = true;
	para2.Parent = 0;

	if (predicatedTiling)
		para2.IsRenderable = false;

	if (predicatedTiling || multisample)
		para2.Multisample = 2;
#elif __PPU
	para2.InTiledMemory = true;
	para2.UseHierZ = true;
#endif

//#if __PPU
	// TODO: We should be able to do this on Xenon too
//	rezx = rezy = PS3_SHADOWMAP_SIZE;
//#else
	rezx = GRCDEVICE.GetWidth();
	rezy = GRCDEVICE.GetHeight();
//#endif

#if 0
	// For some unknown reason, using the back buffer depth render target is about 3x slower than creating our own.
	// Both render targets live in local memory and are tiled, so I can only presume this has to to with memory
	// locality, but I can't be sure without further investigation
 	m_ShadowCollectorDepthBuffer = grcTextureFactory::GetInstance().GetBackBufferDepth();
 	Assert(m_ShadowCollectorDepthBuffer->GetWidth() == rezx);
 	Assert(m_ShadowCollectorDepthBuffer->GetHeight() == rezy);
#else
	m_ShadowCollectorDepthBuffer= grcTextureFactory::GetInstance().CreateRenderTarget("Depth Buffer", grcrtDepthBuffer, rezx, rezy, 32, &para2);
#endif

	//
	// set up the shadow collector render target that will hold grey-scale shadow values in screen-space
	//
	grcTextureFactory::CreateParams params;
	params.Multisample = 0;


#if __XENON	
	params.HasParent = true;
	params.Parent = m_ShadowCollectorDepthBuffer; 

	if(depthBuffer != NULL)
		params.Parent = depthBuffer; 

	params.UseFloat = false;

	if (predicatedTiling || multisample )
		params.Multisample = 2;
#elif __PPU
	params.InTiledMemory = true;
#endif

	//
	// To-Do: add multi-sampling for the PS3 version here
	//

	// this needs to be 8-bit per channel to get all those 
	// different grayscale values for nice looking shadow penumbras
	params.Format = grctfA8R8G8B8;

	// no support for one channel 8-bit render targets on PS3 ... not quite sure if they are possible at all

#if __XENON
	// please leave this off by default for RAGE ... game teams can set it if they want
	// Reason: we want to use the shadow collector for cube shadow maps as well ... so we need more than one color channel
	if ( useCompressed )
	{
		params.Format = grctfL8;
	}
#endif // __XENON


#if __PPU
//	params.Format = grctfR5G6B5;
#endif // __PPU

	m_ShadowCollector = grcTextureFactory::GetInstance().CreateRenderTarget("Shadow Collector", grcrtPermanent, rezx, rezy, 32, &params);
}

//---------------------------------------------------------------------------
//  DeleteRenderTargets()
//  Undo what the CreateRenderTargets() function did
//
//---------------------------------------------------------------------------
void CShadowCollector::DeleteRenderTargets(void)
{
	if(m_ShadowCollectorDepthBuffer && m_ShadowCollectorDepthBuffer != grcTextureFactory::GetInstance().GetBackBufferDepth())
	{
		m_ShadowCollectorDepthBuffer->Release();
		m_ShadowCollectorDepthBuffer = NULL;	
	}

	// release the black & white texture that holds the results from the depth texture
	if(m_ShadowCollector)
	{
		m_ShadowCollector->Release();
		m_ShadowCollector = NULL;	
	}
}


//---------------------------------------------------------------------------
//  EnableZBufferWrite()
//  undo what DisableZBufferWrite() did
//
//---------------------------------------------------------------------------
void CShadowCollector::EnableZBufferWrite(void)
{
	grcState::SetState(grcsDepthWrite, true);
}

//---------------------------------------------------------------------------
//  DisableZBufferWrite()
//  disable Z writes
//
//---------------------------------------------------------------------------
void CShadowCollector::DisableZBufferWrite(void)
{
	grcState::SetState(grcsDepthWrite, false);
}


//---------------------------------------------------------------------------
//  DisableAlphaBlend()
//  disable alpha blending
//
//---------------------------------------------------------------------------
void CShadowCollector::DisableAlphaBlend(void)
{
	grcState::SetState(grcsAlphaBlend, false);
}

//---------------------------------------------------------------------------
//  EnableAlphaBlend()
//  enable alpha blending
//
//---------------------------------------------------------------------------
void CShadowCollector::EnableAlphaBlend(void)
{
	grcState::SetState(grcsAlphaBlend, true);
	grcState::SetBlendSet(grcbsMultiplySrcDest);
}

//---------------------------------------------------------------------------
//  LockShadowCollector()
//  Lock shadow collector render target
//
//---------------------------------------------------------------------------
void CShadowCollector::LockShadowCollector(void)
{
#if __XENON
	int tileRectCount;
	GRCDEVICE.GetTileRects(tileRectCount);
	if (tileRectCount>0)
	{
		// hmm, we don't need to do anything here
	}
	else
	{	
		if (m_PredicatedTiled)
		{
			GRCDEVICE.BeginTiledRendering(m_ShadowCollector, m_ShadowCollectorDepthBuffer, 0, 4, grcDevice::TILE_HORIZONTAL); 
			grcViewport::SetCurrent(grcViewport::GetCurrent());
		}
		else
		{
			grcTextureFactory::GetInstance().LockRenderTarget(0, m_ShadowCollector, m_ShadowCollectorDepthBuffer, grcPositiveX, true);
		}
	}
#else
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_ShadowCollector, m_ShadowCollectorDepthBuffer, grcPositiveX, true);
#endif

	// Clear depth buffer - don't clear colour since we about to twat over it anyway
	GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);
}

//---------------------------------------------------------------------------
//  UnlockShadowCollector()
//  Unlock shadow collector render target
//
//---------------------------------------------------------------------------
void CShadowCollector::UnlockShadowCollector(grcResolveFlags * WIN32_ONLY(resolveFlags))
{

#if __XENON
	int tileRectCount;
	GRCDEVICE.GetTileRects(tileRectCount);
	if (tileRectCount>0)
	{
		// just save the active tiling part
		GRCDEVICE.SaveBackBuffer(m_ShadowCollector, NULL, resolveFlags); // NOTE: m_ShadowCollector must equal the screen size!
	}
	else
	{
		if (m_PredicatedTiled)
		{
			GRCDEVICE.EndTiledRendering(); 
		}
		else
		{
			grcTextureFactory::GetInstance().UnlockRenderTarget(0, resolveFlags);
		}
	}
#elif __PPU
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);
/*
	grcImage *image = GRCDEVICE.CaptureRenderTargetScreenshot(m_ShadowCollector);
	//
	const char *shotName = "t:/ShadowColector.jpg";
	if (image) 
	{
	 	image->SaveJPEG(shotName);
	 	image->Release();
	}
	*/
#else 
	//just unlock the render target
	grcTextureFactory::GetInstance().UnlockRenderTarget(0,resolveFlags);
#endif
}

//---------------------------------------------------------------------------
//  UnlockShadowCollector()
//  Unlock shadow collector render target
//
//---------------------------------------------------------------------------
void CShadowCollector::SetShadowCollectorMap(void)
{
	if(m_ShadowCollectorID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_ShadowCollectorID, m_ShadowCollector );
}


//---------------------------------------------------------------------------
// visualize render targets
//
//---------------------------------------------------------------------------
void CShadowCollector::ShowShadowRenderTargets()
{
	// first get screen size
	float PositionUpperLeftCornerX = 0.0f;
	float PositionUpperLeftCornerY = 0.0f;

	// the full-screen is -1..1 in both directions ... which is normalized to 0..2
	const float OriginX = (PositionUpperLeftCornerX) - (1.0f);
	const float OriginY =  (1.0f) - (PositionUpperLeftCornerY);

	// the preview window follows the screen ratio ... how cool is that?
	const float RectWidth = 2.0f;
	const float RectHeight = 2.0f;

	// show black & white texture
	if(m_bShadowCollectorMap)
	{
		grcEffectTechnique* whichTech = NULL;

		switch (m_iShadowCollectorChannel)
		{
			case 1:
				whichTech = &m_CopyTechnique1;
				break;

			case 2:
				whichTech = &m_CopyTechnique2;
				break;

			case 3:
				whichTech = &m_CopyTechnique3;
				break;

			case 4:
				whichTech = &m_CopyTechnique4;
				break;

			default:
				whichTech = &m_CopyTechnique;
				break;
		}

		ShowShadowMap(m_ShadowCollector, *whichTech,
					OriginX,				// top left x
					OriginY,			    // top left y 
					OriginX + RectWidth,    // bottom right x
					OriginY - RectHeight);	// bottom right y
	}
}


//---------------------------------------------------------------------------
//
// ShowShadowMap(..)
// shows the shadow map as a quad on the screen for debugging purposes
// should be relative to the screen size
//---------------------------------------------------------------------------
void CShadowCollector::ShowShadowMap(grcTexture * texture, 
					  grcEffectTechnique technique,					// technique in FX file
					  float DestX, float DestY, float SizeX, float SizeY)								
{
	texture = texture;
	Color32 white(1.0f,1.0f,1.0f,1.0f);

	// set the chosen render target sampler stages; default is m_RenderMapId
	m_ShadowCollectorShader->SetVar(m_RenderMapId, texture);

	// blit
	m_ShadowCollectorShader->TWODBlit(DestX,			// x1 - Destination base x
										DestY,			// y1 - Destination base y
										SizeX,			// x2 - Destination opposite-corner x
										SizeY,			// y1 - Destination opposite-corner y
										0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
										0.0f,			// u1 - Source texture base u (in normalized texels)
										0.0f,			// v1 - Source texture base v (in normalized texels)
										1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
										1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
										(Color32)white,		// color - reference to Packed color value
										technique);

	//	release texture again
	m_ShadowCollectorShader->SetVar(m_RenderMapId, (grcTexture*)grcTexture::None);
}


} // namespace rage
