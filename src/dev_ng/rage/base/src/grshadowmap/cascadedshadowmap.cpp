// 
// shadowmap/characterhadowmap.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
// Character Shadow maps for 360 and PS3
//
//

#include "cascadedshadowmap.h"
#include "grprofile/pix.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/im.h"
#include "grcore/light.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "spatialdata/shaftdebug.h"


#include "system/param.h"


#include "system/xtl.h"
#include "grcore/device.h"

#if __XENON
#include "embedded_rage_blendshadows_fxl_final.h"
#elif __PPU
#include "embedded_rage_blendshadows_psn.h"
#elif __WIN32PC
#include "embedded_rage_blendshadows_win32_30.h"
#endif

namespace rage 
{
	static Color32 white(1.0f,1.0f,1.0f,1.0f);

CShadowMapTile::CShadowMapTile()
{
	SetDefaults(-1);
}

CShadowMapTile::CShadowMapTile(int whichTile)
	
{
	SetDefaults(whichTile);
}	

void CShadowMapTile::SetDefaults(int whichTile)
{
	m_WhichTile=whichTile;
	const float TILE_INCREMENT = 10.0f;

	Assert(whichTile<MAX_TILES);
	m_NearSlice			= 0.0f;
	m_FarSlice			= TILE_INCREMENT+(TILE_INCREMENT*whichTile);

#if __XENON
	m_DepthBias			=  0.0000f;
		m_BiasSlope			= 0.0000f;
#elif __PPU
	m_DepthBias			=  0.0008f;
		m_BiasSlope			= 0.0000f;
		m_BiasSlope			= 0.0000f;
#elif __WIN32
	m_DepthBias			=  0.0008f;
		m_BiasSlope			= 0.0000f;
#endif

#if __XENON
	m_FovYDiv			= 0.96f;
#elif __PPU
	m_FovYDiv			= 0.75f;
#elif __WIN32
	m_FovYDiv			= 0.75f;
#endif

	m_LightDepthScale = 20.0f;
	m_CameraOffset = 0.0f;

	m_ViewportZShift = 1.0f;

	m_ViewProj.Identity();
}

const char* CShadowMapTile::GetNumberName() const
{
	return sm_NumberNames[m_WhichTile];
}

#if __BANK
void CShadowMapTile::AddWidgets(bkBank& bank)
{
	bank.AddSlider("Light Depth Scale", &m_LightDepthScale, 0.0f, 10000.0f, 0.01f);
	bank.AddSlider("Camera Z axis Offset", &m_CameraOffset, -300.0f, 300.0f, 0.1f);
		bank.AddSlider("Depth Bias", &m_DepthBias, -1.0f, 1.0f, 0.000001f);


#if __XENON || __PPU
		bank.AddSlider("Slope Scale Bias", &m_BiasSlope, -15.0f, 15.0f, 0.01f);
#endif
	bank.AddSlider("light frustum viewer Z Offset", &m_ViewportZShift, -100.0f, 1000.0f, 0.1f);
	bank.AddSlider("light frustum near", &m_NearSlice, 0.0f, 100.0f, 0.1f);
	bank.AddSlider("light frustum far", &m_FarSlice, 1.0f, 10000.0f, 0.1f);
	bank.AddSlider("+/- light view frust", &m_FovYDiv, 0.0f, 10.0f, 0.01f);
}
#endif

};

// this must be here so that it finds rage::CShadowMapTile:
#include "cascadedshadowmap_parser.h"

namespace rage {

// if this triggers, add the required tile names to sm_NumberNames:
CompileTimeAssert(CShadowMapTile::MAX_TILES==8);

const char* CShadowMapTile::sm_NumberNames[MAX_TILES] = 
{
	"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eight"
};

	CShadowMap *CShadowMap::sm_instance = NULL;


	//static functions
	void CShadowMap::Init(int numTiles, bool bLoadShadowData)
	{
		Assert(sm_instance == NULL);
		sm_instance = rage_new CShadowMap(numTiles, bLoadShadowData);
	}
	void CShadowMap::Terminate()
	{
		Assert(sm_instance);
		delete sm_instance;
		sm_instance = NULL;
	}




//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
CShadowMap::CShadowMap(int numTiles, bool bLoadShadowData) :
#if __XENON
	m_XenonShadowmapSizeX(XENON_SHADOWMAP_SIZEX),
	m_XenonShadowmapSizeY(XENON_SHADOWMAP_SIZEY),
#elif __PPU
	m_PS3ShadowmapSizeX(PS3_SHADOWMAP_SIZEX),
	m_PS3ShadowmapSizeY(PS3_SHADOWMAP_SIZEY),
#elif __WIN32PC
	m_PCShadowmapSizeX(PC_SHADOWMAP_SIZEX),
	m_PCShadowmapSizeY(PC_SHADOWMAP_SIZEY),
#endif
	m_DepthBuffer(NULL),
#if __DEV
	m_FirstShaftDebugIndex(0),
#endif // __DEV
	m_ShowShadowMap(false),
	m_bShadowCollector(false),
	m_iShadowCollectorChannel(0)
{
#if __XENON
	m_XenonNoOfTextureAtlasFaces = numTiles;
		Assertf(!((m_XenonNoOfTextureAtlasFaces > 1) && (numTiles & 1)), "Trying to use a shadowmap with %d tiles, but it must be an even number.", numTiles);
#elif __PPU
	m_PS3NoOfTextureAtlasFaces = numTiles;
		Assertf(!((m_PS3NoOfTextureAtlasFaces > 1) && (numTiles & 1)), "Trying to use a shadowmap with %d tiles, but it must be an even number.", numTiles);
#endif

#if __XENON
		m_bUsesHierZ = false;
#endif

	m_FadeOutSMap = 25.0f;
	m_TexelSizeID		= grcegvNONE;
	m_DepthTextureID	= grcegvNONE;

	// initialize tiles:
	Assert(numTiles>0);
	Assert(numTiles<=CShadowMapTile::MAX_TILES);
	m_Tiles.Resize(numTiles);

	m_Viewports.Resize(numTiles);

	for (int i=0;i < m_Tiles.GetCount();i++)
	{
			m_Tiles[i]=rage_new CShadowMapTile(i);
	}

	for (int i=0;i<m_Viewports.GetCount();i++)
	{
			m_Viewports[i]= rage_new grcViewport;
	}

	INIT_PARSER;

	if(bLoadShadowData)
	{
		// get default asset path -> root of asset directory
		ASSET.PushFolder("$/tune");

		// load tunables file from asset path
#if __XENON
		LoadTunables<CShadowMap>("360CascadedShadowMaps"); 
#elif __PPU
		LoadTunables<CShadowMap>("PS3CascadedShadowMaps"); 
#endif
		ASSET.PopFolder();
	}
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::InitShaders(const char *path)
{	
	if (path)
		ASSET.PushFolder(path);

	// 360/PS3 - fast blit into depth buffer. The depth buffer is then used as a shadow map
	const char *shaderName = "rage_shadowdepth";
		m_DepthShader = grmShaderFactory::GetInstance().Create();
		m_DepthShader->Load(shaderName);

	// writes shadow data into shadow collector
	shaderName = "rage_blendshadows";
		m_RenderIntoCollectorShader = grmShaderFactory::GetInstance().Create();
		m_RenderIntoCollectorShader->Load(shaderName);

	if (path)
		ASSET.PopFolder();

		// grab handle to copy technique
		m_CopyTechnique = m_DepthShader->LookupTechnique("CopyRT");	

#if __PPU || __WIN32PC
		// color for the PS3 clear
		m_ColorClearId = m_DepthShader->LookupVar("ColorClear");	
#endif

	// texel size
	m_TexelSizeID = grcegvNONE; // grcEffect::LookupGlobalVar("Shadowmap_PixelSize0"); // Unused globals are now filtered in fx2cg

	// handle for camera matrix
		// 		m_CameraMatrixID = grcEffect::LookupGlobalVar("CameraMatrix");

	// depth texture
	m_DepthTextureID = grcEffect::LookupGlobalVar("DepthTexture0");


	#if USE_GLOBAL_SHADOWLIGHTMTX
		m_FadeOutSMapID		= grcEffect::LookupGlobalVar("FadeOutLastSMap");	// fade out distance of shadow map
		m_LightMatrixArrID	= grcEffect::LookupGlobalVar("LightMatrixArr");		// light camera projection matrix
	#else
		m_FadeOutSMapID		= m_RenderIntoCollectorShader->LookupVar("FadeOutLastSMap");	// fade out distance of shadow map
		m_LightMatrixArrID	= m_RenderIntoCollectorShader->LookupVar("LightMatrixArr");		// light camera projection matrix
	#endif
		m_SpheresID			= grcEffect::LookupGlobalVar("ShadowSpheres");


#if __DEV
//		spdShaftDebug::InitClass();
#endif
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
CShadowMap::~CShadowMap(void)
{
	SHUTDOWN_PARSER;

#if __DEV
//		spdShaftDebug::ShutdownClass();
#endif

	delete m_DepthShader;
	delete m_RenderIntoCollectorShader;
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::CreateRenderTargets()
{
#if __XENON || __PPU
	int rezx = 0;
	int rezy = 0;
#endif

	//Create the offscreen target to hold depth information (Note: on XENON it can be the actual z-buffer)
	grcTextureFactory::CreateParams cp;
	
	cp.Multisample = 0;

#if __XENON
	rezx = m_XenonShadowmapSizeX;
	rezy = m_XenonShadowmapSizeY;

	// switch on shadow map mip-mapping
	//cp.MipLevels = SHADOW_MAP_LEVELS;
	cp.IsResolvable = true;	

	cp.HasParent = true;
	cp.Parent = NULL;
	cp.TextureAtlas = m_XenonNoOfTextureAtlasFaces;

		// new Chernoff bounds shadow approach
		cp.Multisample = grcDevice::MSAA_Centered4xMS;

	// calculate size of render target when it is MSAA'ed
	int MSAAWidth = 1;
	int	MSAAHeight = 1;
	if(cp.Multisample == grcDevice::MSAA_Centered4xMS)
	{
		MSAAWidth = 2;
		MSAAHeight = 2;
	}
	else if(cp.Multisample == grcDevice::MSAA_2xMS)
	{
		MSAAWidth = 2;
		MSAAHeight = 1;
	}

	// Hierarchical Z only supports up to 3600 tiles, whereas one tile is 32x16. 
	if (((m_XenonShadowmapSizeX * MSAAWidth) / 32)*((m_XenonShadowmapSizeY * MSAAHeight) / 16) <= 3600)
			m_bUsesHierZ = true;
	else
			m_bUsesHierZ = false;

		cp.UseHierZ = m_bUsesHierZ;

		grcRenderTargetType type = grcrtShadowMap;

		cp.UseFloat = false;
		cp.IsResolvable = true; // if we're doing color buffer version, we don't need to resolve this one...
		cp.Format = grctfD24S8;
		m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Shadow Map Depth Map", type, rezx, rezy, 32, &cp);
#elif __PPU
	// put shadow map in tiled memory pool
	cp.InTiledMemory = true;
	//cp.InLocalMemory = false; // Testing has shown this to be a major performance killer, so leave it in VRAM :-)
	cp.TextureAtlas = m_PS3NoOfTextureAtlasFaces;
		//cp.TextureAtlas = 1;
	cp.UseHierZ = true;
		cp.IsResolvable = true;

	// size of the map
	rezx = m_PS3ShadowmapSizeX;
	rezy = m_PS3ShadowmapSizeY;

	// needs to be grcrtShadowMap to be treated as a D24_S8 texture .. grcrtDepthMap might be 8:8:8:8 all the time
	grcRenderTargetType type = grcrtShadowMap;
		// #if SIXTEENBITDEPTH
		// 		cp.Format = grctfD16;
		// #else
		cp.Format = grctfD24S8;
		// #endif
	// shadow map texture atlas
		m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Shadow Map Texture Atlas", type, rezx, rezy, 16, &cp);
#endif
		}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::DeleteRenderTargets(void)
{
#if __XENON || __PPU
		if(m_DepthBuffer)
		{
			m_DepthBuffer->Release();
			m_DepthBuffer = NULL;	
		}
#endif
}

//---------------------------------------------------------------------------
//  DebugOrthoViewport{}
//  PURPOSE:
//	Shows the orthographic viewport for a shadow map
//---------------------------------------------------------------------------
	void CShadowMap::DebugOrthoViewport(bool bOn)
	{
		if(bOn)
{
 #if __DEV
			// because every tile has its own shadow map, the NumTiles == number of shadow maps
			int NumTiles = GetTileCount();

			// loop through the shadow map viewports
			for (int i = 0; i < NumTiles; i++)
			{
				// get viewport
				grcViewport* pViewports = GetShadowViewports(i);

				// debug draw viewport
				(*pViewports).DebugDraw();
			}

//			if (spdShaftDebug::IsInstantiated())
//				spdShaftDebug::GetInstance().DebugDraw();

#if 1
//	grcDrawSphere(1.0f, Vector3(0.0f, 0.0f, 0.0f), 25, true);
/*
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		grcDrawPolygon(m_Tiles[3]->DebugVectors, 4, NULL, true, Color32(0.8f, 0.0f, 0.0f, 1.0f));
	}
*/
// 	grcColor4f(1.0f,0.0f,0.0f,1.0f);
// 	for (int i=0;i<m_Tiles.GetCount();i++)
// 	{
// 		grcDrawSphere(1.0f, m_Tiles[i]->SphereMatrix, 25, true);
// 	}
// 
#endif

#endif
}
	}



//---------------------------------------------------------------------------
	//  CalculateSphereInFrustumSlice{}
//  PURPOSE:
//	
//---------------------------------------------------------------------------
	void CShadowMap::CalculateSphereAroundFrustumSlice(CShadowMapTile& tile, 
		grcViewport& inVP, 
													  const Matrix34& camMatrix, 
													  Vector3 extents[], 
													  const float m_FovYDiv,
													  Vector3& nearCenter,
													  Vector3& farCenter)
{
	// 
	// Get the position and the "basis" vectors from the viewer camera matrix 
	//      camPos - d 
	//      camDir - c 
	//      camA - a - "x axis" 
	//      camB - b - "y axis" 
	Vector3 camPos(camMatrix.d); 
	Vector3 camDir(-camMatrix.c); 
	Vector3 camA(camMatrix.a); 
	Vector3 camB(camMatrix.b); 


		// 
		// note to myself: double check the following fov calculation ...
		// this is probably not right. In RegeneratePerspective() in viewport.cpp
		// is better code

	// get the fov x and y value 
		//	float fovY = (grcViewport::GetCurrent()->GetFOVY()/2) * m_FovYDiv;
		float fovY = inVP.GetFOVY();
		fovY /= 2;
		fovY *= m_FovYDiv;

	// grcViewport::GetCurrent()->GetAspectRatio() gives you 1.77f for a 1280x720 resolution
	// if I reconstruct a near or far plane with this, the ratio is not right ...
	// 1.53 seems to be perfectly right ... so I did not want to mess with the viewport class
	// and hard coded it.
		float ratio =  inVP.GetAspectRatio(); //1.53f;

	float fovX;

#if __PPU // 2x AA	
		if(GRCDEVICE.GetMSAA() == grcDevice::MSAA_2xMS)
		fovX = fovY * ratio * 2; 
		else
		fovX = fovY * ratio; 
	#else 
		fovX = fovY * ratio; 
	#endif		

	// convert from degrees to radians 
	fovX *= DtoR; 
	fovY *= DtoR; 

	// 
	// this is the width and height of the rectangle that fits into the near
	// and far slice from the point of the viewer
	// 
	//			\											 /
	//			 \	   D*tan(fov / 2)						/
	//			  \	  |				  |					   /
	//			   \  |				  |					  /
	//				\ |				  |					 /
	//				 \|				  |					/
	//	--------------\--------------------------------/
	//				   \			  |				  /
	//		  			\	 angle    |				 /
	//					 \	  is	  |				/
	//					  \	 fov / 2  |			   /
	//			/\		   \ <------> |			  /
	//			|			\		  |			 /
	//			D			 \------------------/ 
	//			|			  \		  |		   /
	//			\/			   \	  |		  /
	//						    \	  |		 /
	//							 \	  |		/
	//							  \--------/ 
	//							   \  |   /
	//								\ |  /
	//								 \| /
	//								  \/
	//------------------------------------------------------------- 
	// - D is the far or near slice plane
	// - fov / 2 is half of the vertical angle of the perspective view frustum
	// - the triangle consists of the left side of the perspective view frustum,
	// the line in the middle and D
	// - the result is the opposite side of the triangle.
	// - to get the width of the perspective frustum we have to multiply by 2
	// 
	// - camViewWidth stores half the width of the near slice plane and half of the width of 
	// the far slice plane
	// - camViewHeight does the same for the height of those planes
	Vector2 camViewWidth;  
	Vector2 camViewHeight; 
	camViewWidth.Set(tanf(fovX) * tile.m_NearSlice, tanf(fovX) * tile.m_FarSlice); 

		// scale this value
	camViewHeight.Set(tanf(fovY) * tile.m_NearSlice, tanf(fovY) * tile.m_FarSlice); 

	// 
	// get two vectors on the near and two vectors to build the future near and far plane
	// of the orthographic view frustum
	// 
	// ------------- 
	// |     ^     | 
	// |     |     | 
	// |  <--      | 
	// |           | 
	// |           | 
	// ------------- 
	// 
	// left near point from viewer camera frustum 
	Vector3 leftNear(camA); 
	leftNear.Scale(camViewWidth.x); 

	// up near point from viewer camera frustum 
	Vector3 upNear(-camB); 
	upNear.Scale(camViewHeight.x); 

	// left far point from viewer camera frustum 
	Vector3 leftFar(camA); 
	leftFar.Scale(camViewWidth.y); 

	// up far point from viewer camera frustum 
	Vector3 upFar(-camB); 
	upFar.Scale(camViewHeight.y); 

	// 
	// get two vectors that point in the light view direction 
	// to the near and far slice plane 
	// 
	nearCenter.Set(camPos); 
	// add a vector that points in the camera direction and 
	// is scaled to get the near center of the frustum 
	nearCenter.AddScaled(camDir, tile.m_NearSlice); 
	nearCenter.AddScaled(camDir, -tile.m_ViewportZShift); 

	farCenter.Set(camPos); 
	// add a vector that points in the camera direction and 
	// is scaled to get the far center of the frustum 
	farCenter.AddScaled(camDir, tile.m_FarSlice); 
	farCenter.AddScaled(camDir, -tile.m_ViewportZShift); 

	// 
	// Write the far center and near center value in a box with eight edges
	// This box does not have any width and height so far, only length
	// 
	int i; 
	for(i = 0; i < 4; i++) 
	{ 
		// fill up with the near and far center points 
		extents[i].Set(nearCenter); 
		extents[i + 4].Set(farCenter); 
	} 

	//
	// now add width and height to this box
	//
	// store the four points for the near plane 
	// creates based on the near center point the four points that form 
	// the near plane of a viewing frustum 
	// 
	// x           x 
	// 
	//            
	//           
	//           
	//           
	// x           x 
	//      
	// go from near center point to the left and then up 
	extents[0].Add(-leftNear);  extents[0].Add( upNear); 
	// go from near center point to the right and then up 
	extents[1].Add(leftNear);  extents[1].Add( upNear); 
	// go from the near center point to the left and then down 
	extents[2].Add( leftNear);  extents[2].Add(-upNear); 
	// go from the near center point to the right and then down 
	extents[3].Add(-leftNear);  extents[3].Add(-upNear); 

	// Create far plane 
	// creates based on the far center point the four points that form 
	// the far plane of a viewing frustum 
	// go from the far center point to the left and then up 
	extents[4].Add(-leftFar);  extents[4].Add( upFar); 
	// go from the far center point to the right and then up 
	extents[5].Add(leftFar);  extents[5].Add( upFar); 
	// go from the far center point to the left and then down 
	extents[6].Add( leftFar);  extents[6].Add(-upFar); 
	// go from the far center point to the right and then down 
	extents[7].Add(-leftFar);  extents[7].Add(-upFar); 

		// build sphere
		Vector3 MiddleofLine, MiddleofLine2;
		Vector3 SphereCenter;
		float	SphereRadius;
		Vector4 Sphere;

		// near right top to far left bottom
		MiddleofLine.Lerp(0.5f, extents[1], extents[7]);
		// near left bottom to far right up
		MiddleofLine2.Lerp(0.5f, extents[3], extents[5]);
		// between those should be the center of the sphere
		SphereCenter.Lerp(0.5f, MiddleofLine, MiddleofLine2);

		// v is the vector from near top left corner to the far right down corner
		// the sphere radius is just half of this
		Vector3 v = extents[1] - extents[7]; 
		SphereRadius = v.Mag(); 
		SphereRadius /= 2.0f; // only half the length because we are looking for a radius.

		// fill-up the sphere data
		Sphere.SetVector3(SphereCenter);
		Sphere.SetW(SphereRadius);

		tile.m_SphereAroundFrustumSlice = Sphere;
	}

	void CShadowMap::GetOrthoViewport(grcViewport& inVP, grcViewport& outVP, int whichTile, const Matrix34& camMatrix, Vector3::Vector3Param LightDir)
	{
		CalcOrthoViewportAroundSphere(*m_Tiles[whichTile], inVP, outVP, camMatrix, LightDir);
}

	void CShadowMap::CalcOrthoViewportAroundSphere(CShadowMapTile& tile, 
		grcViewport& inVP, 
		grcViewport& outVP, 
		const Matrix34& camMatrix, Vector3::Vector3Param LightDir)
{
	// keep the compiler happy
#if __WIN32PC
	tile = tile;
#endif

	Vector3 Extents[8];
	Vector3 NearCenter;
	Vector3 FarCenter;

	// calculate the view frustum slice in camera space
		CalculateSphereAroundFrustumSlice(tile, 
			inVP,
										camMatrix, 
										Extents, 
										tile.m_FovYDiv,
										NearCenter,
										FarCenter);

		// setup orthographic 
		// void Ortho(float left,float right,float bottom,float top,float znear,float zfar)
		float radius = tile.m_SphereAroundFrustumSlice.GetW();
		outVP.Ortho(-radius, radius, -radius, radius, tile.m_LightDepthScale * -radius, tile.m_LightDepthScale * radius);

	//
		// build up the light view matrix
	//

		// grab current group of lights
		// 		grcLightGroup * LightGroup = grcState::GetLightingGroup();

		Vector3 LightDirection = LightDir; //LightGroup->GetDirection(0);
		LightDirection.Normalize();

	//
		Vector3 SphereCenter = tile.m_SphereAroundFrustumSlice.GetVector3();
		//SphereCenter.y = 0.0f;

		Matrix34 LightMatrix;
		LightMatrix.Identity();

		if ( fabs( LightDirection.y ) < 0.99f)		// check so mess up cross product.
	{ 
			LightMatrix.LookAt(VEC3_ZERO, LightDirection, YAXIS);
	} 
		else
	{ 
			LightMatrix.LookAt(VEC3_ZERO, LightDirection, XAXIS);
		}
		//LightMatrix.a.Negate();
		LightMatrix.d = SphereCenter;
		outVP.SetCameraMtx(RCC_MAT34V(LightMatrix));
/*
		// round the shadow map image translation (panning) to texel borders
		//Vector2 ShadowMapSize;
		//ShadowMapSize.Set((float)m_XenonShadowmapSizeX, (float)m_XenonShadowmapSizeY);

		Vector2 OriginShadow;
		OriginShadow.Set(SphereCenter.x, SphereCenter.z);

		int OriginRoundedX;
		int OriginRoundedY;
		OriginRoundedX = round(OriginShadow.x * ((float)(1.0f / XENON_SHADOWMAP_SIZEX) / 2));
		OriginRoundedY = round(OriginShadow.y * ((float)(1.0f / XENON_SHADOWMAP_SIZEX) / 2));

//		Vector2 Result;
//		Result = Vector2((float)OriginRoundedX, (float)OriginRoundedY) - (OriginShadow * ((float)(1.0f / XENON_SHADOWMAP_SIZEX) / 2));

		Matrix44 MRound;
		MRound.Identity();
		MRound.d.SetX((float)OriginRoundedX);
		MRound.d.SetY(1.0f);
		MRound.d.SetZ((float)OriginRoundedY);

		Matrix44 Projection;
		Matrix44 MatrixRoundProj; // we go from right to left 
		Projection.Set(outVP.GetProjection());
		MatrixRoundProj.Dot(MRound, Projection);
		outVP.SetProjection(MatrixRoundProj);

		grcViewport::SetCurrent(&outVP);
*/
		// keep viewport class happy by sending down an identity matrix
		outVP.SetWorldIdentity();
	} 

	void CShadowMap::BeginTileAroundSphere(int whichTile, bool /* clear Buffers */)
	{
		// measure the time-frame a shadow map draw call takes
		PIXBeginN(0, "Render into Shadow Map %d", whichTile);

		Assert(whichTile<m_Tiles.GetCount());

		// keep the compiler happy
#if __WIN32PC
		whichTile = whichTile;
#endif

		CShadowMapTile& tile=*m_Tiles[whichTile];
		grcViewport& vp=*m_Viewports[whichTile];

		// grab current viewport and save it
		m_OldVP = grcViewport::GetCurrent();

		// set new light view frustum viewport
		grcViewport::SetCurrent(&vp);

		// keep the compiler happy
#if __WIN32PC
		tile = tile;
		vp = vp;
#endif

	// 
		// set all the device states and set render target
	// 
#if __XENON  || __PPU
		// depth bias & slope scale depth bias
		// yep the type cast from a float value to the address u32 pointer type casted 
		grcState::SetState(grcsDepthBias, *(u32*)&tile.m_DepthBias);
		grcState::SetState(grcsSlopeScaleDepthBias, *(u32*)&tile.m_BiasSlope);
#endif

		// lock correct depth buffer
#if __XENON
		grcTextureFactory::GetInstance().LockRenderTarget(0, NULL, m_DepthBuffer);
		// ok this looks strange ... I know
		// if using hier z clear all tiles, otherwise we only clear the first tile
		if(whichTile == 0 || m_bUsesHierZ) 
		{
			// clear depth buffer only
			GRCDEVICE.Clear(false, Color32(0.0f, 0.0f, 0.0f, 1.0f), true, 1.0f, 0);
		}
#elif __PPU
		// 	// depth-only writes on the PS3
		// 	// Requirements:
		// 	// 1) Render target is not swizzled.
		// 	// 2) Alpha test is disabled or ALWAYS
		// 	// 3) No discard instruction in the fragment shader
		// 	// 4) No user clip planes are enabled
		// 	// 5) No color RT or color writes disabled.
	//
		// 	// just to be sure
		// 	grcState::SetState(grcsAlphaTest, false);
	// 
		// 	u32 ColorWriteState = grcState::GetState(grcsColorWrite);
		// 	grcState::SetState(grcsColorWrite, grccwNone);

		if(m_PS3NoOfTextureAtlasFaces > 1)
		{
			// lock depth buffer
			grcTextureFactory::GetInstance().LockRenderTarget(0, NULL, m_DepthBuffer, grcPositiveX, true, whichTile);
		}
		else
		{
			grcTextureFactory::GetInstance().LockRenderTarget(0, NULL, m_DepthBuffer, grcPositiveX, true);
		}
#endif

#if __XENON
		Vector2 texSize = Vector2((float)m_XenonShadowmapSizeX, (float)m_XenonShadowmapSizeY) ;
#elif __PPU
		Vector2 texSize = Vector2((float)m_PS3ShadowmapSizeX, (float)m_PS3ShadowmapSizeY) ;
#elif __WIN32PC
		Vector2 texSize = Vector2((float)m_PCShadowmapSizeX, (float)m_PCShadowmapSizeY) ;
#endif

		// texel size - in the shader this is Shadowmap_PixelSize0
		if(m_TexelSizeID!=grcegvNONE)
		{
			Vec4f v4(1.0f / texSize.x, 1.0f / texSize.y, texSize.x, texSize.y);
			grcEffect::SetGlobalVar(m_TexelSizeID, v4);
		}

		// those are the spheres used to construct the light view frusta around them
		// radius is here power of two to make some of the calculations in the pixel shader easier
		m_ShadowSpheres[0] = m_Tiles[0]->m_SphereAroundFrustumSlice;
		m_ShadowSpheres[0].w = m_Tiles[0]->m_SphereAroundFrustumSlice.w * m_Tiles[0]->m_SphereAroundFrustumSlice.w;
		m_ShadowSpheres[1] = m_Tiles[1]->m_SphereAroundFrustumSlice;
		m_ShadowSpheres[1].w = m_Tiles[1]->m_SphereAroundFrustumSlice.w * m_Tiles[1]->m_SphereAroundFrustumSlice.w;
		m_ShadowSpheres[2] = m_Tiles[2]->m_SphereAroundFrustumSlice;
		m_ShadowSpheres[2].w = m_Tiles[2]->m_SphereAroundFrustumSlice.w * m_Tiles[2]->m_SphereAroundFrustumSlice.w;
		m_ShadowSpheres[3] = m_Tiles[3]->m_SphereAroundFrustumSlice;
		m_ShadowSpheres[3].w = m_Tiles[3]->m_SphereAroundFrustumSlice.w * m_Tiles[3]->m_SphereAroundFrustumSlice.w;
	
#if __DEV
		// If we have shaft debugging enabled, store this debugging shaft.
//		if (spdShaftDebug::IsInstantiated())
//			spdShaftDebug::GetInstance().StoreDebugShaft(m_FirstShaftDebugIndex + whichTile, vp);
#endif // __DEV

		Matrix44 View;
		Matrix44 Projection;
		Matrix44  ProjView; // we go from right to left 
		View.Set( RCC_MATRIX44(vp.GetViewMtx()));
		Projection.Set( RCC_MATRIX44(vp.GetProjection()));
		ProjView.Dot(Projection, View);

		// convert from view to texture space
		Matrix44 TextureMatrix;
		TextureMatrix.Identity();

		Vector2 Scale = Vector2(1.0f, 1.0f);
		Vector2 Offset = Vector2(0.0f, 0.0f);

		// is the depth buffer the texture atlas or the color buffer?
		m_DepthBuffer->GetAtlasTextureOffsets( whichTile, Offset, Scale);

		float z = 1.0f;
		TextureMatrix.MakeScale( 0.5f * Scale.x, -0.5f * Scale.y, z );	

#if __XENON
		TextureMatrix.d = Vector4(((0.5f  + (0.5f / texSize.x)) * Scale.x) + Offset.x, ((0.5f  + (0.5f / texSize.y)) * Scale.y) + Offset.y, 0.0f, 1.0f );
#elif __PPU
		TextureMatrix.d = Vector4((0.5f * Scale.x) + Offset.x, (0.5f * Scale.y) + Offset.y, 0.0f, 1.0f );
#endif

		Matrix44  TexProjView;
		TexProjView.Dot(TextureMatrix, ProjView);
/*
		//
		// need to figure out why this is not working
		//
		// adjust for sub-texel accuracy
		Vector4 OriginShadow(0, 0, 0, 1.0f);
		OriginShadow.Dot(OriginShadow, TexProjView);
#if __XENON
		float TexCoordX = OriginShadow.x * ((float)m_XenonShadowmapSizeX) * 0.5f;
		float TexCoordY = OriginShadow.y * ((float)m_XenonShadowmapSizeY) * 0.5f;
#elif __PSN
		float TexCoordX = OriginShadow.x * ((float)m_PS3ShadowmapSizeX) * 0.5f;
		float TexCoordY = OriginShadow.y * ((float)m_PS3ShadowmapSizeY) * 0.5f;
#elif __WIN32PC
		float TexCoordX = OriginShadow.x * ((float)m_PCShadowmapSizeX) * 0.5f;
		float TexCoordY = OriginShadow.y * ((float)m_PCShadowmapSizeY) * 0.5f;
#endif

		// round to the nearest "whole" texel
		float TexCoordRoundedX = (float)round(TexCoordX);
		float TexCoordRoundedY = (float)round(TexCoordY);
		// 
		float dx = TexCoordRoundedX - TexCoordX;
		float dy = TexCoordRoundedY - TexCoordY;
		// transform dx, dy back to homogenous light space
#if __XENON
		dx /= ((float)m_XenonShadowmapSizeX) * 0.5f;
		dy /= ((float)m_XenonShadowmapSizeY) * 0.5f;
#elif __PSN
		dx /= ((float)m_PS3ShadowmapSizeX) * 0.5f;
		dy /= ((float)m_PS3ShadowmapSizeY) * 0.5f;
#elif __WIN32PC
		dx /= ((float)m_PCShadowmapSizeX) * 0.5f;
		dy /= ((float)m_PCShadowmapSizeY) * 0.5f;
#endif
		Matrix44 xRounded;
		xRounded.Identity();
		xRounded.MakePos(dx, dy, 0);
		tile.m_ViewProj = TexProjView.Dot(xRounded);
*/
		tile.m_ViewProj = TexProjView;
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderIntoShadowMaps(bool clearBuffers)
{
	// grab current group of lights
	grcLightGroup * LightGroup = grcState::GetLightingGroup();

	// force the first light to be a directional light
	// TODO: We might as well assert here. If we change the light type,
	// we might be messing with what the users intended to use here.
	LightGroup->SetLightType(0, grcLightGroup::LTTYPE_DIR);


		//		Vector3 lightPos = LightGroup->GetPosition(0);
	Vector3 lightDir = VEC3V_TO_VECTOR3(LightGroup->GetDirection(0));

		RenderIntoShadowMaps(clearBuffers, lightDir);
}

	//---------------------------------------------------------------------------
	//  
	//
	//---------------------------------------------------------------------------
	void CShadowMap::RenderIntoShadowMaps(bool clearBuffers, Vector3& lightDir)
{

	// set the pixel shader threads to 112 -> vertex shader can only use 16 threads
#if __XENON
		GRCGPRALLOCATION->SetGPRAllocation(22);
#endif
	PIXBegin(0, "CShadowMap::RenderIntoShadowMaps");
	
#if __PPU
 	// Clear depth buffer
 	grcTextureFactory::GetInstance().LockRenderTarget(0, NULL, m_DepthBuffer);
 	GRCDEVICE.Clear(false, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);
 	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

	// depth-only writes on the PS3
	// Requirements:
	// 1) Render target is not swizzled.
	// 2) Alpha test is disabled or ALWAYS
	// 3) No discard instruction in the fragment shader
	// 4) No user clip planes are enabled
	// 5) No color RT or color writes disabled.
	// just to be sure
	u32 AlphaTestState = grcState::GetState(grcsAlphaTest);
	grcState::SetState(grcsAlphaTest, false);

	u32 ColorWriteState = grcState::GetState(grcsColorWrite);
	grcState::SetState(grcsColorWrite, grccwNone);
#endif // __PPU
	// 
		// 		m_lightPos = lightPos;
		// 		m_lightDir = lightDir;

	// get the viewer's camera matrix
	m_CameraMatrix = MAT34V_TO_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
	m_CameraMatrix.NormalizeSafe();

		// this setups a matrix -> shadow map culling has function that this as well
		// you want to use this in case the cascaded shadow map culling is used
		//		SetupLightMatrix(m_CameraMatrix, m_LightMatrix);
		//		m_LightMatrix.NormalizeSafe();

	// render into the shadow map tiles
	for (int i=0;i< m_Tiles.GetCount();i++)
	{

			// calculate the ortho viewports
			// you want the shadow map culling functionality in case cascaded shadow map culling is used
			//			CalcAndSetOrthoViewport(i, m_CameraMatrix, m_LightMatrix);
			CalcAndSetOrthoViewportAroundSphere(i, m_CameraMatrix, lightDir);

			// begin rendering
			//			BeginTile(i, clearBuffers);
			BeginTileAroundSphere(i, clearBuffers);

#if __DEV && 0
/*
		grcColor4f(1.0f,1.0f,1.0f,1.0f);
		m_Tiles[i]->SphereMatrix.d.z += 2.0f;
		grcDrawSphere(3.0f, m_Tiles[i]->SphereMatrix, 25, true);
		tile.MinMax[0] = Extents[0];
		tile.MinMax[1] = Extents[7];
*/
		Vector3 size = m_Tiles[i]->MinMax[1] - m_Tiles[i]->MinMax[0];
		Matrix34 mtx;
		mtx.Identity();
		mtx.d = ( m_Tiles[i]->MinMax[1] + m_Tiles[i]->MinMax[0] ) * 0.5f;
		grcDrawSolidBox( size, mtx , Color32(1.0f,1.0f,1.0f,1.0f) );

//		grcDrawSolidBox(m_Tiles[i]->MinMax[0],m_Tiles[i]->MinMax[1], Color32(1.0f,1.0f,1.0f,1.0f));

#endif

		RenderIntoShadowMap();

		EndTile(
#if __XENON || __PPU
				i
#endif
			);
	}

#if __PPU
	// set old render states again
		grcState::SetState(grcsAlphaTest, AlphaTestState);
		grcState::SetState(grcsColorWrite, ColorWriteState);
#endif
	PIXEnd();

	// set the ratio between vertex and pixel shaders back to default
#if __XENON
	GRCGPRALLOCATION->SetGPRAllocation(0); 
#endif

	}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderShadowsIntoShadowCollector()
{
	// measure the time it takes to render into the black and white texture
	PIXBegin(0, "Render into Shadow Collector");
		// #if __XENON
		// 		grcState::SetState( grcsHalfPixelOffset, true );
		// #endif
	BeginRenderShadowsIntoShadowCollector();
	RenderTilesIntoShadowCollector();
	EndRenderShadowsIntoShadowCollector();
		// #if __XENON
		// 		grcState::SetState( grcsHalfPixelOffset, false );
		// #endif
	// measure the time-frame a black & white draw call takes
	PIXEnd();
}


	void CShadowMap::CalcAndSetOrthoViewportAroundSphere(int whichTile, const Matrix34& camMatrix, Vector3::Vector3Param LightDirection)
{
	// measure the time-frame a shadow map draw call takes
		PIXBeginN(0, "Calculate and Set Ortho Viewport %d", whichTile);

	Assert(whichTile<m_Tiles.GetCount());
	Assert(whichTile<m_Viewports.GetCount());

		grcViewport&	vp = *m_Viewports[whichTile];
	CShadowMapTile& tile=*m_Tiles[whichTile];

	// keep the compiler happy
#if __WIN32PC
		whichTile = whichTile;
	tile = tile;
	vp = vp;
	Matrix34 tempMatrix = camMatrix;
#endif
		grcViewport&	CurrViewport = *grcViewport::GetCurrent();


	// Set up a viewport for one of the tiles == slices of the viewer's viewport
		CalcOrthoViewportAroundSphere(tile, CurrViewport, vp, camMatrix, LightDirection);

	tile.m_Shaft.Set(vp, RCC_MATRIX34(vp.GetCameraMtx()));

#if __DEV
	// If we have shaft debugging enabled, store this debugging shaft.
//		if (spdShaftDebug::IsInstantiated())
//		{
//			spdShaftDebug::GetInstance().StoreDebugShaft(m_FirstShaftDebugIndex + whichTile, vp);
//		}
#endif // __DEV

		// measure the time-frame a shadow map draw call takes
		PIXEnd();
	}


#if __PPU
	void CShadowMap::ClearPS3ShadowMap()
 	{
		PIXBegin(0, "Clear the whole shadow map atlas for the PS3");

		// Clear depth buffer
		grcTextureFactory::GetInstance().LockRenderTarget(0, NULL, m_DepthBuffer);
		GRCDEVICE.Clear(false, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		PIXEnd();
	}
#endif

	//---------------------------------------------------------------------------
	//  
	//
	//---------------------------------------------------------------------------
	void CShadowMap::SetupLightMatrix(Matrix34& CameraMatrix, Matrix34& lightMatrix)
{
	// grab current group of lights
	grcLightGroup * LightGroup = grcState::GetLightingGroup();

	Vector3 lightDir = VEC3V_TO_VECTOR3(LightGroup->GetDirection(0));

		SetupLightMatrix(CameraMatrix, lightMatrix, lightDir);
}

	//---------------------------------------------------------------------------
	//  
	//
	//---------------------------------------------------------------------------
	void CShadowMap::SetupLightMatrix(Matrix34& CameraMatrix, Matrix34& lightMatrix, Vector3& lightDir)
{
		Matrix34 lightM;
		Vector3 camPos(CameraMatrix.d); 
		Vector3 camDir(-CameraMatrix.c); 

		// setup real light matrix
		lightM.c = lightDir;
		lightM.b = camDir;

		lightM.b.y = 0;
		if (lightM.b.Mag2()>0.0001f)
			lightM.b.Normalize();
		else
			lightM.b=YAXIS;	// looking straight up or down?

		lightM.a.Cross(lightM.b,lightM.c);
		lightM.a.Normalize();
		lightM.b.Cross(lightM.c,lightM.a);

		lightM.d = camPos;

		lightMatrix = (Matrix34&)lightM;
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderIntoShadowMap()
{
	// force depth shader
	grmModel::SetForceShader(m_DepthShader);

#if __XENON || __PPU
	RenderShadowCasters(*grcViewport::GetCurrent(), true );
#endif

	// undo the force of the shader
	grmModel::SetForceShader(0);
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
#if !__XENON
	void CShadowMap::EndTile( int UNUSED_PARAM(whichTile))
#else
	void CShadowMap::EndTile( int whichTile)
#endif
{
#if __XENON
	Assert(whichTile<m_Tiles.GetCount());
	Assert(whichTile<m_Viewports.GetCount());

	// try to get the free clear from the resolve
	clearParams.ClearColor=false;

		// can't clear depth during depth only resolve if using hierZ
		clearParams.ClearDepthStencil= !m_bUsesHierZ; 

	if(m_XenonNoOfTextureAtlasFaces > 1)
	{
		grcTextureFactory::GetInstance().UnlockRenderTarget(0, &clearParams, whichTile);
		}
	else
	{
		grcTextureFactory::GetInstance().UnlockRenderTarget(0, &clearParams);
		}
#elif __PPU
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);
#endif

#if __PPU
		//		CShadowMapTile& tile=*m_Tiles[whichTile];
		//		tile.m_RatioTilesCulled = GRCDEVICE.EndAdaptiveZCulling(/* fuck tile.m_NumTilesTested, tile.m_NumTilesCulled */);
#endif

#if __XENON || __PPU
		// depth bias & slope scale depth bias
		float fTemp = 0.0f;
		grcState::SetState(grcsDepthBias, *(u32*)&fTemp);
		grcState::SetState(grcsSlopeScaleDepthBias, *(u32*)&fTemp);
#endif

	// set old viewport
	grcViewport::SetCurrent(m_OldVP);

	// measure the time-frame a shadow map draw call takes
	PIXEnd();
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
	void CShadowMap::BeginRenderShadowsIntoShadowCollector( bool useMapsWithoutCollector, bool XENON_ONLY(skipClear)) 
{
	Vector2 texSize;

#if __XENON
		skipClear = false;
#endif

#if __PPU
	texSize = Vector2((float)m_PS3ShadowmapSizeX, (float)m_PS3ShadowmapSizeY);
#elif __XENON
	texSize = Vector2((float)m_XenonShadowmapSizeX, (float)m_XenonShadowmapSizeY);
#endif 

	// texel size - in the shader this is Shadowmap_PixelSize0
	if(m_TexelSizeID!=grcegvNONE)
	{
		Vec4f v4(1.0f / texSize.x, 1.0f / texSize.y, texSize.x, texSize.y);
		grcEffect::SetGlobalVar(m_TexelSizeID, v4);
	}

#if __PPU || __XENON
		// 	int tiles = 1;  
		// 
		// 	if(!GetTopDownFlag())
	int tiles = m_Tiles.GetCount();
#endif

	Vector4 TempDepthBias = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
	Vector4 TempBounds = Vector4(0.0f, 0.0f, 0.0f, 0.0f);

#if USE_GLOBAL_SHADOWLIGHTMTX
		// send down the top, right and left plane for the plane - pixel comparison
		// 		if(m_TopPlanesID)
		// 			grcEffect::SetGlobalVar(m_TopPlanesID, m_TopPlanes,3);
		// 		if(m_RightPlanesID)
		// 			grcEffect::SetGlobalVar(m_RightPlanesID, m_RightPlanes,3);
		// 		if(m_LeftPlanesID)
		// 			grcEffect::SetGlobalVar(m_LeftPlanesID, m_LeftPlanes,3);

		// 		if(m_SpheresID)
		// 			grcEffect::SetGlobalVar(m_SpheresID, m_ShadowSpheres,4);
#else
		// 		if(m_TopPlanesID)
		// 			m_RenderIntoCollectorShader->SetVar(m_TopPlanesID, m_TopPlanes,3);
		// 		if(m_RightPlanesID)
		// 			m_RenderIntoCollectorShader->SetVar(m_RightPlanesID, m_RightPlanes,3);
		// 		if(m_LeftPlanesID)
		// 			m_RenderIntoCollectorShader->SetVar(m_LeftPlanesID, m_LeftPlanes,3);
#endif
		//if(m_SpheresID)
		//	m_RenderIntoCollectorShader->SetVar(m_SpheresID, m_ShadowSpheres,4);
		if(m_SpheresID)
			grcEffect::SetGlobalVar(m_SpheresID, (Vec4V*)m_ShadowSpheres, 4);


#if __XENON || __PPU
	if(m_LightMatrixArrID)
	{
		Matrix44 LightViewProjArr[4];

		for (int x=0; x<tiles; x++)
		{
			LightViewProjArr[x] = m_Tiles[x]->m_ViewProj;
		}
	#if USE_GLOBAL_SHADOWLIGHTMTX
		grcEffect::SetGlobalVar(m_LightMatrixArrID, (Mat44V*)LightViewProjArr, tiles);
	#else
		m_RenderIntoCollectorShader->SetVar(m_LightMatrixArrID, LightViewProjArr, tiles);
	#endif
	}
#endif

		// 		// provide camera position
		// 		Matrix44 Cam;
		// 		Cam.FromMatrix34(grcViewport::GetCurrent()->GetCameraMtx());
		// 
		// 		if(m_CameraMatrixID!=grcegvNONE)
		// 			grcEffect::SetGlobalVar(m_CameraMatrixID, Cam);

	// always set into first depth shadow map
#if __PPU
#if USE_GLOBAL_SHADOWLIGHTMTX
		grcEffect::SetGlobalVar(m_DepthTextureID, m_DepthBuffer);
#else
		m_RenderIntoCollectorShader->SetVar((grcEffectVar)m_DepthTextureID, m_DepthBuffer);
#endif
#elif __XENON
	if(m_DepthTextureID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_DepthTextureID, m_DepthBuffer);
#endif

		//		AssertMsg( m_Tiles.GetCount() == 4, "cascaded shadow maps are optimized == hard-coded for 4 shadow maps" );

	// fade out starting point of last shadow map
	if(m_FadeOutSMapID!=0)
	{
			// fadeOutLastSMap.y - shadow fade distance
			// fadeOutLastSMap.x - "length" of the last tile -minus the fade out range
			int lastTile = m_Tiles.GetCount() - 1;

			// new version is not based on the near plane of the last time (we don't send the last tile near plane, so the shader used the far plane of the send to the last tile, which is frequently quite different!)

			// sigh -- global variables use vectormath but instance variables do not yet use it
	#if USE_GLOBAL_SHADOWLIGHTMTX
			Vec2V fadeOut(m_FadeOutSMap, (m_Tiles[lastTile]->m_FarSlice)-m_FadeOutSMap); 
			grcEffect::SetGlobalVar(m_FadeOutSMapID, fadeOut);
	#else
			Vector2 fadeOut(m_FadeOutSMap, (m_Tiles[lastTile]->m_FarSlice)-m_FadeOutSMap); 
			m_RenderIntoCollectorShader->SetVar(m_FadeOutSMapID, fadeOut);
#endif
	} 

	if ( !useMapsWithoutCollector )
	{
		// set the shadow map preview flag in the shadow collector class
		GRSHADOWCOLLECTOR->SetShadowPreviewFlag(m_bShadowCollector, m_iShadowCollectorChannel);

#if __XENON || __PPU
			// Clear depth buffer
			// #if __XENON
			// 			if (!skipClear)
			// #endif
			GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);
#endif
		}

	// Don't set colour mask on PS3 because it actually runs slower.
#if !__PPU 
		// only write into the red channel of the collector map 
		//grcState::SetColorWrite( grccwRed );
#endif
	}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderTilesIntoShadowCollector()
{
	// force depth shader
	grmModel::SetForceShader(m_RenderIntoCollectorShader);

/*
	grcImage *image = GRCDEVICE.CaptureDepthBufferScreenshot(m_DepthBuffer);

	const char *shotName = "t:/depthbuffer.jpg";
	if (image) 
	{
		image->SaveJPEG(shotName);
		image->Release();
	}
*/

#if __XENON || __PPU
	RenderShadowCasters(*grcViewport::GetCurrent(), false );
#endif

	// undo the force of the shader
	grmModel::SetForceShader(0);
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::EndRenderShadowsIntoShadowCollector()
{
#if __PPU
#if USE_GLOBAL_SHADOWLIGHTMTX
		grcEffect::SetGlobalVar(m_DepthTextureID, (grcTexture*)grcTexture::None);
#else
		m_RenderIntoCollectorShader->SetVar((grcEffectVar)m_DepthTextureID, (grcTexture*)grcTexture::None);
#endif

#elif __XENON
		grcEffect::SetGlobalVar(m_DepthTextureID, (grcTexture*)grcTexture::None);
#endif

// seems to be broken on PS3
#if !__PPU 
	// now set the state back
//		grcState::SetColorWrite( grccwRGBA );
#endif
}



//---------------------------------------------------------------------------
// shows the shadow map as a quad on the screen for debugging purposes
// should be relative to the screen size
//---------------------------------------------------------------------------
void CShadowMap::ShowShadowMap(grcRenderTarget* m_Buffer, 
					  grcEffectTechnique technique,					// technique in FX file
					  float DestX, float DestY, float SizeX, float SizeY)								
{
	Color32 white(1.0f,1.0f,1.0f,1.0f);

	grcEffect::SetGlobalVar(m_DepthTextureID, m_Buffer); 

	// blit
	m_DepthShader->TWODBlit(DestX,			// x1 - Destination base x
						DestY,			// y1 - Destination base y
						SizeX,			// x2 - Destination opposite-corner x
						SizeY,			// y1 - Destination opposite-corner y
						0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
						0.0f,			// u1 - Source texture base u (in normalized texels)
						0.0f,			// v1 - Source texture base v (in normalized texels)
						1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
						1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
						(Color32)white,		// color - reference to Packed color value
						technique);

	grcEffect::SetGlobalVar(m_DepthTextureID, (grcTexture*)grcTexture::None);
}

//---------------------------------------------------------------------------
// visualize render targets
//
//---------------------------------------------------------------------------
void CShadowMap::ShowShadowRenderTargets()
{
		// measure the time-frame a shadow map draw call takes
		PIXBegin(0, "Shadow Map Preview");

#if __XENON || __PPU
	// first get screen size
	float PositionUpperLeftCornerX = 0.1f;
	float PositionUpperLeftCornerY = 0.1777f;

	const float OriginX = (PositionUpperLeftCornerX) - (1.0f);
	const float OriginY =  (1.0f) - (PositionUpperLeftCornerY);

	// the preview window follows the screen ratio ... how cool is that?
	const float RectWidth = 3.0f / 16.0f;
	const float RectHeight = 3.0f / 9.0f;
#endif

	if(m_ShowShadowMap)
	{
#if __XENON
			ShowShadowMap(m_DepthBuffer, 
							m_CopyTechnique,  
							OriginX,						// top left x
							OriginY,								// top left y 
							OriginX + 4 * RectWidth,				// bottom right x
							OriginY - 4 * RectHeight);				// bottom right y

#elif __PPU
		u8 format = GRCDEVICE.PatchShadowToDepthBuffer(m_DepthBuffer, true);
			ShowShadowMap(	m_DepthBuffer, m_CopyTechnique,  
							OriginX,								// top left x
							OriginY,								// top left y 
							OriginX + 4 * RectWidth,				// bottom right x
							OriginY - 4 * RectHeight);				// bottom right y
		GRCDEVICE.PatchDepthToShadowBuffer(m_DepthBuffer, format, true);
#endif
	}
		PIXEnd();
}

#if __BANK
typedef void (CShadowMap::*CShadowMapMember0)();

void CShadowMap::AddSaveWidgets(bkBank& DEV_ONLY(bk))
{
#if __DEV
	// gcc wants a double cast to remove all doubt as to how you want this to work ...
	bk.AddButton("Save Tunables...",datCallback((Member0)(CShadowMapMember0)&CShadowMap::SaveTunablesCB<CShadowMap>,this));
	bk.AddButton("Load Tunables...",datCallback((Member0)(CShadowMapMember0)&CShadowMap::LoadTunablesCB<CShadowMap>,this));
#endif
}

void CShadowMap::AddWidgets(bkBank &bk)
{
#if __DEV
//	if(spdShaftDebug::IsInstantiated())
//		spdShaftDebug::GetInstance().AddWidgets(bk);
#endif
		//		bk.AddSlider("Top Down Fade out range", &m_TopDownFadeOutSMap, 0.0f, 1000.0f, 1.0f);
	bk.AddSlider("Fade out range", &m_FadeOutSMap, 0.0f, 1000.0f, 1.0f);

	const int NUM_SHADOWCOLLECTOR_CHOICES = 5;
	const char* ShadowCollectorChoices[NUM_SHADOWCOLLECTOR_CHOICES] =
	{
		"All channels",
		"Channel 1 only",
		"Channel 2 only",
		"Channel 3 only",
		"Channel 4 only"		
	};


	bk.AddToggle("Show Shadow Map", &m_ShowShadowMap);
	bk.AddToggle("Shadow Collector Map", &m_bShadowCollector);
	bk.AddCombo("    Show collector channel:", &m_iShadowCollectorChannel, NUM_SHADOWCOLLECTOR_CHOICES, ShadowCollectorChoices, 0);

	// subtract one for the bounding volume viewport
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		const int GROUP_NAME_LEN=128;
		char groupName[GROUP_NAME_LEN];
		bk.PushGroup(formatf(groupName,GROUP_NAME_LEN,"Tile #%d",i));
		m_Tiles[i]->AddWidgets(bk);
		bk.PopGroup();
	}
}
#endif

} // namespace rage
