#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

BeginSampler(sampler2D, CloudShadowTex, CloudShadowSampler, CloudShadowTex)
ContinueSampler(sampler2D, CloudShadowTex, CloudShadowSampler, CloudShadowTex)
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = WRAP;
   AddressV  = WRAP;
EndSampler;

BeginSampler(sampler2D, DepthMapTex, DepthMapSampler, DepthMapTex)
ContinueSampler(sampler2D, DepthMapTex, DepthMapSampler, DepthMapTex)
   MinFilter = POINT;
   MagFilter = POINT;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;



// Variables
float4x4	WorldViewProjInverse;
float		CloudPatternScale;
float3		NearFarClipPlaneQ;
float2		ScrollXZ;
float4		TexelSize;
float		CloudShadowStrength;


/*
As defined in shaderlib/rage_common.fxh:

struct rageVertexOutputPassThrough {
	// This is used for the simple vertex and pixel shader routines
    float4 pos			: DX_POS;
    float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
	float2 texCoord1	: TEXCOORD1;
	float3 normal		: COLOR1;
};
*/




// -------------------------------------------------------------
// Vertex shader that passes the current screen position as TEXCOORD2, useful for mapping back into worldspace.
// this is currently for the 360 only
// -------------------------------------------------------------
rageVertexOutputPassThrough VS_ragePassThroughNoXform4TexAndScrPos(rageVertexInput IN)
{
 	rageVertexOutputPassThrough OUT = (rageVertexOutputPassThrough)0;
	OUT.pos = float4(IN.pos, 1.0f);

	float2 halfscres = TexelSize.zw * 0.5f;
	OUT.texCoord0 = IN.texCoord0.xy;
    OUT.texCoord1 = float2(IN.pos.x * halfscres.x + halfscres.x, -IN.pos.y * halfscres.y + halfscres.y);

	OUT.diffuse = float4(1.0f, 1.0f, 1.0f, 1.0f);	// Unused
	OUT.normal = float3(0.0f, 0.0f, -1.0f);			// Unused
    return OUT;
}




float GetCamDepth(float2 texCoord)
{
	float camDepth = 0.0f;
	float curDepth = 0.0f;

#if __XENON
	curDepth = tex2D(DepthMapSampler, texCoord).x;

	// Q = Zf / Zf - Zn
	// z = -Zn * Q / Zd - Q
	camDepth = -(NearFarClipPlaneQ[0] * NearFarClipPlaneQ[2]) / ((1.0f - curDepth) - NearFarClipPlaneQ[2]);
#endif

#if __PS3
	curDepth = texDepth2D(DepthMapSampler, texCoord);

	// Q = Zf / Zf - Zn
	// z = -Zn * Q / Zd - Q
	camDepth = -(NearFarClipPlaneQ[0] * NearFarClipPlaneQ[2]) / (curDepth - NearFarClipPlaneQ[2]);
#endif

//	return camDepth;
	return (1.0f - curDepth);
}


float4 PS_drawIntoCollector(rageVertexOutputPassThrough IN) : COLOR
{
	float4 result = float4(0.0f, 0.0f, 0.0f, 0.0f);


	// We have to convert to world coordinates in order to figure out how the cloud shadow texture lays across the land.
	float curDepth = GetCamDepth(IN.texCoord0.xy);
	float4 screenPos = float4(IN.texCoord1.xy, curDepth, 1.0f);
	float4 worldPos = mul(screenPos, WorldViewProjInverse);
	worldPos.xyz /= worldPos.w;

	// Scroll
	worldPos.xz *= CloudPatternScale;
	worldPos.xz += ScrollXZ;

	// Convert to UV's and sample
	float2 finalUV = worldPos.xz;
	float texel = tex2D(CloudShadowSampler, finalUV).x;

	// Strength
	texel = (1.0f - texel);
	texel *= CloudShadowStrength;
	texel = (1.0f - texel);

	// Cloud shadows only draw into the green channel (may need to remap for PS3)
	result.y = texel;

	return result;
}


float4 PS_CopyRT(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	return float4(1.0f, 1.0f, 1.0f, 1.0f);
//     return h4tex2D(ToneMapSampler, IN.texCoord0.xy);
}

technique draw
{

    pass P0
    {
		CullMode = NONE;
		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER	PS_CopyRT();
    }
}

technique unlit_draw
{

    pass P0
    {
		CullMode = NONE;
		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER	PS_CopyRT();
    }
}

technique CopyRT
{

    pass P0
    {
		CullMode = NONE;
		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER	PS_CopyRT();
    }
}

technique DrawIntoCollector
{
	pass P0
	{
		CullMode = NONE;
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER	VS_ragePassThroughNoXform4TexAndScrPos();
		PixelShader	 = compile PIXELSHADER	PS_drawIntoCollector();
	}
}
// 		VertexShader = compile VERTEXSHADER	VS_ragePassThroughNoXform();

