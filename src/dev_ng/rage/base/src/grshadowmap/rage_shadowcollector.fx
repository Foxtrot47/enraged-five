#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

#if !__FXL
// the "workhorse" sampler for all render target operations
texture RenderMap;
sampler RenderMapSampler = sampler_state
{
   Texture = <RenderMap>;
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
};
#else
// the "workhorse" sampler for all render target operations
sampler RenderMapSampler : RenderMap = sampler_state
{
//   Texture = <RenderMap>;
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
};

#endif


// -------------------------------------------------------------
// PSCopyRTx
// - Blit of channel 1
// -------------------------------------------------------------
float4 PSCopyRTx( rageVertexOutputPassThrough IN): COLOR
{
	float4 pixel = tex2D(RenderMapSampler, IN.texCoord0);
	return float4(pixel.xxx, 1.0f);
}

// -------------------------------------------------------------
// PSCopyRTy
// - Blit of channel 2
// -------------------------------------------------------------
float4 PSCopyRTy( rageVertexOutputPassThrough IN): COLOR
{
	float4 pixel = tex2D(RenderMapSampler, IN.texCoord0);
	return float4(pixel.yyy, 1.0f);
}

// -------------------------------------------------------------
// PSCopyRTz
// - Blit of channel 3
// -------------------------------------------------------------
float4 PSCopyRTz( rageVertexOutputPassThrough IN): COLOR
{
	float4 pixel = tex2D(RenderMapSampler, IN.texCoord0);
	return float4(pixel.zzz, 1.0f);
}

// -------------------------------------------------------------
// PSCopyRTw
// - Blit of channel 4
// -------------------------------------------------------------
float4 PSCopyRTw( rageVertexOutputPassThrough IN): COLOR
{
	float4 pixel = tex2D(RenderMapSampler, IN.texCoord0);
	return float4(pixel.www, 1.0f);
}

// -------------------------------------------------------------
// PSCopyRT
// - plain copy
// -------------------------------------------------------------
float4 PSCopyRT( rageVertexOutputPassThrough IN): COLOR
{
     return tex2D(RenderMapSampler, IN.texCoord0);
}

// -------------------------------------------------------------
// PSUnusedTechniqueToSetupInGlobals
// - pure bollocks, but means that the important globals are referenced
// -------------------------------------------------------------
float4 PSUnusedTechniqueToSetupInGlobals( rageVertexOutputPassThrough IN): COLOR
{
     return tex2D(ShadowCollectorSampler, IN.texCoord0) * gShadowCollectorTexelSize.xyxy;
} 

technique CopyRT
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRT();
    }
}

technique CopyRTx
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRTx();
    }
}

technique CopyRTy
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRTy();
    }
}

technique CopyRTz
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRTz();
    }
}

technique CopyRTw
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRTw();
    }
}

technique draw
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRT();
	}
}

technique UnusedTechniqueToSetupInGlobals
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSUnusedTechniqueToSetupInGlobals();
    }
}
