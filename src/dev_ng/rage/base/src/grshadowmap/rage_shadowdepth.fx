#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

// PC only
#if __WIN32PC
	float3 ClipPlanes;
#endif

#if __PS3 || __WIN32PC
	float4 ColorClear;
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct vertexOutputDepthSmall 
{
    float4 pos		        : DX_POS;
#if __WIN32PC
	float depth				: TEXCOORD0;
#endif
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputDepthSmall VSSkin(rageSkinVertexInput IN) 
{
	vertexOutputDepthSmall OUT = (vertexOutputDepthSmall)0;
	
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
	float3 pos = rageSkinTransform(IN.pos, boneMtx);
	
	//
	// XBOX 360, PSN and PC
	//
	// Write out final position
	OUT.pos =  mul(float4(pos,1), gWorldViewProj);
	// clamp Z to 0 if it's negative ... this will push everything behind the camera onto the near plane	
	OUT.pos.z = max(0, OUT.pos.z);

// #if __PS3 || __XENON
// 	// part of the linearize my depth buffer campaign
// 	OUT.pos.z = OUT.pos.z * OUT.pos.w;
// #endif	

	//
	// PC
	//
	// this is the PC path
#if __WIN32PC //|| __XENON
	OUT.depth = (OUT.pos.z / OUT.pos.w); // * gPointLightPositiongGeometryScale.w;

	// linear distribution
//	OUT.depth = ((OUT.pos.z + ClipPlanes.x) / (ClipPlanes.y - ClipPlanes.x));
//	OUT.pos.z = OUT.depth * OUT.pos.w;
#endif
    return OUT;    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputDepthSmall VS(rageVertexInput IN)
{
	vertexOutputDepthSmall OUT = (vertexOutputDepthSmall)0;

	//
	// XBOX 360, PSN and PC
	//
	// Write out final position
	OUT.pos =  mul(float4(IN.pos,1), gWorldViewProj);
	
	// clamp Z to 0 if it's negative ... this will push everything behind the camera onto the near plane
	OUT.pos.z = max(0, OUT.pos.z);

// #if __PS3  || __XENON
// 	// part of the linearize my depth buffer campaign
// 	OUT.pos.z = OUT.pos.z * OUT.pos.w;
// #endif	
	
	//
	// PC
	//
	// this is the PC path
#if __WIN32PC //|| __XENON
	OUT.depth = (OUT.pos.z / OUT.pos.w); // * gPointLightPositiongGeometryScale.w;

	// linear distribution
//	OUT.depth = ((OUT.pos.z + ClipPlanes.x) / (ClipPlanes.y - ClipPlanes.x));
//	OUT.pos.z = OUT.depth * OUT.pos.w;
#endif
    return OUT;    
}

// -------------------------------------------------------------
// PSDrawDepth
// - plain depth write for PC and PS3
// -------------------------------------------------------------
#if __PS3    
float4 PSDrawDepth( vertexOutputDepthSmall IN ): COLOR
{
	return float4(0.0, 0.0, 0.0, 0.0);
	//return (unpack_4ubyte(pack_2half(half2(IN.depth, 0.0f))));
	//return (IN.depth * gPointLightPositiongGeometryScale.w);

}
#elif __WIN32PC
float4 PSDrawDepth( vertexOutputDepthSmall IN ): COLOR
{
	return IN.depth;
}
// #elif __XENON
// float4 PSDrawDepth( vertexOutputDepthSmall IN ): COLOR
// {
// 	return IN.depth;
// }
#endif



// -------------------------------------------------------------
// PSCopyRT
// - plain copy
// -------------------------------------------------------------
float4 PSCopyRT( rageVertexOutputPassThrough IN): COLOR0
{
#if __WIN32PC || __XENON
    return tex2D(DepthTextureSampler0, IN.texCoord0);
#elif __PS3
	return texDepth2D(DepthTextureSampler0, IN.texCoord0);    
//    return tex2D(DepthTextureSampler0, IN.texCoord0).x;
	 
#endif
}

#if __PS3 || __WIN32PC
// -------------------------------------------------------------
// PS_Clear
// - setting up stencil mask
// -------------------------------------------------------------
float4 PS_Clear(): COLOR
{
	return ColorClear;
}
#endif


technique draw
{
	pass p0 
	{       
		AlphaBlendEnable = false;
		AlphaTestEnable = false;
        CullMode = CCW;	
		ZEnable = true;
		ZWriteEnable = true;		
		VertexShader = compile VERTEXSHADER VS();
#if !__XENON    
		PixelShader  = compile PIXELSHADER PSDrawDepth();
#else
		PixelShader  = NULL;
#endif		
	}
}
technique drawskinned
{
	pass p0 
	{ 
		AlphaBlendEnable = false;
		AlphaTestEnable = false;
        CullMode = CCW;	
		ZEnable = true;
		ZWriteEnable = true;		
		VertexShader = compile VERTEXSHADER VSSkin();
#if !__XENON    
		PixelShader  = compile PIXELSHADER PSDrawDepth();
#else
		PixelShader  = NULL;
#endif		
		
	}
}

technique CopyRT
{

    pass P0
    {
		CullMode = NONE;
		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRT();
    }
}

technique Stencilmask
{
	pass p0
	{
		CullMode = NONE; 	
		ZEnable = true;
		ZWriteEnable = false;
		ZFunc =	ALWAYS;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;		

		VertexShader = compile VERTEXSHADER	VS_ragePassThroughNoXform();
	#if __XENON
		PixelShader  = NULL;
	#elif __PS3 || __WIN32PC
		PixelShader  = compile PIXELSHADER	PS_Clear();
	#endif
	}
}

