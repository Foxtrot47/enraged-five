<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<autoregister allInFile="true"/>
<structdef constructable="false" type="rage::CCubeShadowMap">
<array name="m_ShadowedPointLights" type="atArray">
<pointer policy="owner" type="rage::CShadowedPointLight"/>
</array>
<bool name="m_ShowCubeShadowMap"/>
<float name="m_fScalePreview"/>
<bool name="m_bShadowCollector"/>
</structdef>

<structdef type="rage::CShadowedPointLight">
<float name="m_GeometryScale"/>
<float name="m_LightAttenuationEnd"/>
<float name="m_fDepthBias"/>
<float name="m_fLightVSMEpsilon"/>
</structdef>

</ParserSchema>