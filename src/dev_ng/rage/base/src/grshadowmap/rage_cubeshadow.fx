#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

struct vertexOutputComposite 
{
    float4 hPosition        : DX_POS;
    float3 vLight			: TEXCOORD5;
};

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
{
    vertexOutputComposite OUT = (vertexOutputComposite)0;

    rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
    float3 pos = rageSkinTransform(IN.pos, boneMtx);

    OUT.hPosition = mul(float4(pos, 1), gWorldViewProj);
	OUT.vLight =(pos.xyz - gPointLightPosition) * gGeometryScale; 

    return OUT;
}

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
vertexOutputComposite VS_Composite(rageVertexInputBump IN) 
{
    vertexOutputComposite OUT = (vertexOutputComposite)0;

    OUT.hPosition = mul(float4(IN.pos, 1.0), gWorldViewProj);

    float4 pos = mul(float4(IN.pos,1), gWorld);
	OUT.vLight =(pos.xyz - gPointLightPosition) * gGeometryScale;
    return OUT;
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( vertexOutputComposite IN ) : COLOR
{
	// fill up the cube shadow map

	// Work out the depth of this fragment from the light, normalized to 0->1
	half2 depth = 0.0f;
    depth.x = length(IN.vLight) / gLightAttenuationEnd;

#if __PS3
	return CubeShadowMapPackMoments(depth);
#else	
	return depth.xyxy;
#endif	
}


//------------------------------------------------------
// techniques
//------------------------------------------------------
technique drawskinned
{
    pass p0 
    {
    
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = NONE;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = NONE;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_Composite();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}
technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = NONE;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_Composite();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique unlit_drawskinned
{
    pass p0 
    {
    
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = NONE;
        ZWriteEnable = true;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}
