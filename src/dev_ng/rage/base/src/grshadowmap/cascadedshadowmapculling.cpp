// 
// grshadowmap/cascadedshadowmapculling.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


#include "cascadedshadowmapculling.h"

#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"



using namespace rage;

CCascadedShadowCulling *CCascadedShadowCulling::sm_instance = NULL;


//static functions
void CCascadedShadowCulling::Init()
{
	Assert(sm_instance == NULL);
	sm_instance = rage_new CCascadedShadowCulling;
}

void CCascadedShadowCulling::Terminate()
{
	Assert(sm_instance);
	delete sm_instance;
	sm_instance = NULL;
}

CCascadedShadowCulling::CCascadedShadowCulling()
{
	m_pLightPlanes = &m_LightPlanes;
}




CCascadedShadowCulling::~CCascadedShadowCulling(){};



//
// So the idea here is that we get the camera matrix from the viewer's camera first, then
// we generate one light matrix and use it to generate the four viewports and store them in the shadow map class
// 

void CCascadedShadowCulling::GetCameraMatrix(Matrix34& CameraMatrix)
{
	// get the viewer's camera matrix
	CameraMatrix = RCC_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
}

bool CCascadedShadowCulling::GenerateLightMatrixAndShadowMapViewportsForCulling(Matrix34& CameraMatrix, CShadowMap* pShadowMap, Vector3::Vector3Param LightDirection)
{
	Matrix34 LightMatrix;

	pShadowMap->SetupLightMatrix(CameraMatrix, LightMatrix);

	for(int i= 0; i < pShadowMap->GetTileCount(); i++)
	{
		// this also generates the culling planes
		pShadowMap->CalcAndSetOrthoViewportAroundSphere(i, CameraMatrix, LightDirection);
	}

	return true;
}


grcCullStatus CCascadedShadowCulling::PlaneSphereTest(Vector4& Plane, Vector4& CullSphere)
{
#if __XENON || __PPU
	// positive values mean that it is in the view frustum
	return (Plane.DistanceToPlane(CullSphere.GetVector3()) > CullSphere.w)? cullOutside : cullInside;
#else	// win32 cant convert vector4 to vector3
	return (Plane.DistanceToPlane(Vector3(CullSphere.x, CullSphere.y, CullSphere.z)) > CullSphere.w)? cullOutside : cullInside;
#endif

	// #if __XENON || __PPU
	// 	// positive values mean that it is in the view frustum
	// 	float PlaneDist = Plane.DistanceToPlane((Vector3)CullSphere);
	// #else	// win32 cant convert vector4 to vector3
	// 	float PlaneDist = Plane.DistanceToPlane(Vector3(CullSphere.x, CullSphere.y, CullSphere.z));
	// #endif


	// 	// check out if it is in front or behind the view plane
	// 	if(PlaneDist > CullSphere.w) // outside
	// 		return cullOutside;
	// 	else if(fabsf(PlaneDist) < CullSphere.w)
	// 		return cullClipped;
	// 	else
	// 		return cullInside;
}

// 
// PlaneSphereAABBTest() can only have two different return values: cullInside or cullOutside
//
grcCullStatus CCascadedShadowCulling::PlaneSphereAABBTest(Vector4& Plane, Vector4& CullSphere, spdAABB& AAbox)
{
	AAbox = AAbox;

	grcCullStatus CullStat = PlaneSphereTest(Plane, CullSphere);

	// we do not want AABB <-> plane comparisons here ... it increases the number of visible objects
	/*
	// test AABB if the sphere intersects the plane
	if(CullStat == cullClipped)
	{
	Vector3 Min;
	Vector3 Max;
	Min = AAbox.GetMin();
	Max = AAbox.GetMax();

	Vector3 verts[8] = {
	Vector3(Min.x, Min.y, Min.z),
	Vector3(Max.x, Max.y, Max.z),
	Vector3(Min.x, Max.y, Max.z),
	Vector3(Max.x, Min.y, Min.z),
	Vector3(Min.x, Min.y, Max.z),
	Vector3(Min.x, Max.y, Min.z),
	Vector3(Max.x, Min.y, Max.z),
	Vector3(Max.x, Max.y, Min.z),
	};

	int test = 0;
	for (int i = 0; i < 8; i++)
	{
	#if __XENON || __PPU
	// positive values mean that it is outside the view frustum
	float PlaneDist = Plane.DistanceToPlane(verts[i]);
	#else		// win32 cant convert vector4 to vector3
	float PlaneDist = Plane.DistanceToPlane(verts[i].x, verts[i].y, verts[i].z);
	#endif
	if(PlaneDist <= 0.0)
	test |= 1 << i;
	}
	if(test)
	return cullInside; // is inside or intersects
	else
	return cullOutside;
	}
	else
	*/
	return CullStat;
}


//
// cull object against the viewers viewport first
//
u8 CCascadedShadowCulling::CullBoundingSphereAABBAgainstCullingPlanes(grcViewport *pVP, Vector4& vCullSphere, Vector3& lightDir)
{
	//
	// if the sun is facing one of those planes, than everything that is behind it can be culled ....
	// we consider only the left, right and near plane from this frustum .. this is obviously a coarse way to cull
	//
	// holds a bit mask
	u8 CullSphereInCullingPlanes = 0;

	// the normal coming from this near plane is inverted compared to the one above ... so it points out instead of in
	Vector4 NearPlane = VEC4V_TO_VECTOR4(pVP->GetFrustumClipPlane(grcViewport::CLIP_PLANE_NEAR));
	NearPlane.SetVector3LeaveW(-NearPlane.GetVector3()); 	// invert the normal

	Vector4 LeftPlane = VEC4V_TO_VECTOR4(pVP->GetFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT));
	LeftPlane.SetVector3LeaveW(-LeftPlane.GetVector3()); 	// invert the normal

	Vector4 RightPlane = VEC4V_TO_VECTOR4(pVP->GetFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT));
	RightPlane.SetVector3LeaveW(-RightPlane.GetVector3()); 	// invert the normal

	// 	Vector4 NearPlane;
	// 	NearPlane.ComputePlane(CameraMatrix.d, CameraMatrix.c);

	// get the near plane normal
	Vector3 NearPlaneNormal = NearPlane.GetVector3();

	// get the left plane normal
	Vector3 LeftPlaneNormal = LeftPlane.GetVector3();

	// get the right plane normal
	Vector3 RightPlaneNormal = RightPlane.GetVector3();

	// see how the near plane normal compares -> if they are bigger than 0, we now the sun is facing this plane
	float Near = NearPlaneNormal.Dot(lightDir);
	float Left = LeftPlaneNormal.Dot(lightDir);
	float Right = RightPlaneNormal.Dot(lightDir);


	if(Near > 0.0f)
		//if(cullOutside == PlaneSphereTest(NearPlane, vCullSphere))
		if(cullOutside == PlaneSphereTest(NearPlane, vCullSphere))
			CullSphereInCullingPlanes |= CullingPlaneNear; 

	// if it is in front of the near plane do all the other tests
	if(!(CullSphereInCullingPlanes & CullingPlaneNear)) 
	{
		// sun is facing the left plane
		if(Left > 0.0f)
			if(cullOutside == PlaneSphereTest(LeftPlane, vCullSphere))
				CullSphereInCullingPlanes |= CullingPlaneLeft; 

		// sun is facing the right plane
		if(Right > 0.0f)
			if(cullOutside == PlaneSphereTest(RightPlane, vCullSphere))
				CullSphereInCullingPlanes |= CullingPlaneRight; 
	}
	return CullSphereInCullingPlanes;
}


//
// Pix tells me that retrieving the planes for each object is really expensive ...
//
void CCascadedShadowCulling::GetLightViewFrustumPlanes(CShadowMap* pShadowMap)
{
	const spdShaft& Shaft = pShadowMap->GetTile(0)->GetShaft();
	const spdShaft& Shaft2 = pShadowMap->GetTile(1)->GetShaft();
	const spdShaft& Shaft3 = pShadowMap->GetTile(2)->GetShaft();
	const spdShaft& Shaft4 = pShadowMap->GetTile(3)->GetShaft();
	m_pLightPlanes->TopPlane	=  Shaft.GetPlane(spdShaft::PLANE_TOP).GetCoefficientVector();
	m_pLightPlanes->RightPlane	=  Shaft.GetPlane(spdShaft::PLANE_RIGHT).GetCoefficientVector();
	m_pLightPlanes->LeftPlane	=  Shaft.GetPlane(spdShaft::PLANE_LEFT).GetCoefficientVector();
	m_pLightPlanes->BottomPlane =  Shaft.GetPlane(spdShaft::PLANE_BOTTOM).GetCoefficientVector();
	m_pLightPlanes->FarPlane	=  Shaft.GetPlane(spdShaft::PLANE_FAR).GetCoefficientVector();

	m_pLightPlanes->TopPlane2	=  Shaft2.GetPlane(spdShaft::PLANE_TOP).GetCoefficientVector();
	m_pLightPlanes->RightPlane2	=  Shaft2.GetPlane(spdShaft::PLANE_RIGHT).GetCoefficientVector();
	m_pLightPlanes->LeftPlane2	=  Shaft2.GetPlane(spdShaft::PLANE_LEFT).GetCoefficientVector();
	m_pLightPlanes->BottomPlane2 =  Shaft2.GetPlane(spdShaft::PLANE_BOTTOM).GetCoefficientVector();
	m_pLightPlanes->FarPlane2	=  Shaft2.GetPlane(spdShaft::PLANE_FAR).GetCoefficientVector();

	m_pLightPlanes->TopPlane3	=  Shaft3.GetPlane(spdShaft::PLANE_TOP).GetCoefficientVector();
	m_pLightPlanes->RightPlane3	=  Shaft3.GetPlane(spdShaft::PLANE_RIGHT).GetCoefficientVector();
	m_pLightPlanes->LeftPlane3	=  Shaft3.GetPlane(spdShaft::PLANE_LEFT).GetCoefficientVector();
	m_pLightPlanes->BottomPlane3 =  Shaft3.GetPlane(spdShaft::PLANE_BOTTOM).GetCoefficientVector();
	m_pLightPlanes->FarPlane3	=  Shaft3.GetPlane(spdShaft::PLANE_FAR).GetCoefficientVector();

	m_pLightPlanes->TopPlane4	=  Shaft4.GetPlane(spdShaft::PLANE_TOP).GetCoefficientVector();
	m_pLightPlanes->RightPlane4	=  Shaft4.GetPlane(spdShaft::PLANE_RIGHT).GetCoefficientVector();
	m_pLightPlanes->LeftPlane4	=  Shaft4.GetPlane(spdShaft::PLANE_LEFT).GetCoefficientVector();
	m_pLightPlanes->BottomPlane4 =  Shaft4.GetPlane(spdShaft::PLANE_BOTTOM).GetCoefficientVector();
	m_pLightPlanes->FarPlane4	=  Shaft4.GetPlane(spdShaft::PLANE_FAR).GetCoefficientVector();

#if INTRINSICVERSION

#if __XENON || __PPU
	m_Plane0x.Set(m_pLightPlanes->TopPlane4.x, m_pLightPlanes->RightPlane4.x, m_pLightPlanes->LeftPlane4.x, m_pLightPlanes->BottomPlane4.x);
	m_Plane0y.Set(m_pLightPlanes->TopPlane4.y, m_pLightPlanes->RightPlane4.y, m_pLightPlanes->LeftPlane4.y, m_pLightPlanes->BottomPlane4.y);
	m_Plane0z.Set(m_pLightPlanes->TopPlane4.z, m_pLightPlanes->RightPlane4.z, m_pLightPlanes->LeftPlane4.z, m_pLightPlanes->BottomPlane4.z);
	m_Plane0w.Set(m_pLightPlanes->TopPlane4.w, m_pLightPlanes->RightPlane4.w, m_pLightPlanes->LeftPlane4.w, m_pLightPlanes->BottomPlane4.w);
	m_Plane1x.Set(m_pLightPlanes->TopPlane3.x, m_pLightPlanes->RightPlane3.x, m_pLightPlanes->LeftPlane3.x, m_pLightPlanes->BottomPlane3.x);
	m_Plane1y.Set(m_pLightPlanes->TopPlane3.y, m_pLightPlanes->RightPlane3.y, m_pLightPlanes->LeftPlane3.y, m_pLightPlanes->BottomPlane3.y);
	m_Plane1z.Set(m_pLightPlanes->TopPlane3.z, m_pLightPlanes->RightPlane3.z, m_pLightPlanes->LeftPlane3.z, m_pLightPlanes->BottomPlane3.z);
	m_Plane1w.Set(m_pLightPlanes->TopPlane3.w, m_pLightPlanes->RightPlane3.w, m_pLightPlanes->LeftPlane3.w, m_pLightPlanes->BottomPlane3.w);
	m_Plane2x.Set(m_pLightPlanes->TopPlane2.x, m_pLightPlanes->RightPlane2.x, m_pLightPlanes->LeftPlane2.x, m_pLightPlanes->BottomPlane2.x);
	m_Plane2y.Set(m_pLightPlanes->TopPlane2.y, m_pLightPlanes->RightPlane2.y, m_pLightPlanes->LeftPlane2.y, m_pLightPlanes->BottomPlane2.y);
	m_Plane2z.Set(m_pLightPlanes->TopPlane2.z, m_pLightPlanes->RightPlane2.z, m_pLightPlanes->LeftPlane2.z, m_pLightPlanes->BottomPlane2.z);
	m_Plane2w.Set(m_pLightPlanes->TopPlane2.w, m_pLightPlanes->RightPlane2.w, m_pLightPlanes->LeftPlane2.w, m_pLightPlanes->BottomPlane2.w);
	m_Plane3x.Set(m_pLightPlanes->TopPlane.x, m_pLightPlanes->RightPlane.x, m_pLightPlanes->LeftPlane.x, m_pLightPlanes->BottomPlane.x);
	m_Plane3y.Set(m_pLightPlanes->TopPlane.y, m_pLightPlanes->RightPlane.y, m_pLightPlanes->LeftPlane.y, m_pLightPlanes->BottomPlane.y);
	m_Plane3z.Set(m_pLightPlanes->TopPlane.z, m_pLightPlanes->RightPlane.z, m_pLightPlanes->LeftPlane.z, m_pLightPlanes->BottomPlane.z);
	m_Plane3w.Set(m_pLightPlanes->TopPlane.w, m_pLightPlanes->RightPlane.w, m_pLightPlanes->LeftPlane.w, m_pLightPlanes->BottomPlane.w);
#endif

#endif
}



//
// cull objects against light view frusta
//
u8 CCascadedShadowCulling::CullBoundingSphereAABBAgainstViewPorts(CShadowMap* pShadowMap, Vector4& vCullSphere)
{
#if INTRINSICVERSION

	pShadowMap = pShadowMap;
	u8 ObjectOutsideOfLightViewFrustums = 0;

#if __WIN32PC
	vCullSphere = vCullSphere;
#endif

	// all the planes would need to be transposed to end up in m_Plane0x, m_Plane1x etc.
#if __XENON || __PPU

	//
	//
	//
	//
	__vector4 xxxx = __vspltw(vCullSphere,0);	// replicate .x through a vector this is called splatted: the vector holds in all four channels the same value
	__vector4 yyyy = __vspltw(vCullSphere,1);	// ...and y, etc
	__vector4 zzzz = __vspltw(vCullSphere,2);

	// __vsubfp: Vector Subtract Single-Precision
	// _vzerofp: filling a vector register with 0
	// __vspltw: vector splat word
	__vector4 neg_wwww = __vsubfp(_vzerofp,__vspltw(vCullSphere,3));	// compute 0-w through a vector

	//	return Plane.xyz.Sphere.xyz - Plane.w;

	// I know it should be + but I follow just the rest of RAGE here ...
	// __vmaddfp: Vector Multiply-Add Single-Precision
	// this is how it works:
	// Plane.x dot Sphere.x + Plane.y dot Sphere.y + Plane.z dot Sphere.z + Plane.w == Plane.xyz dot Sphere.xyz + Plane.w
	__vector4 sum0 = __vmaddfp(xxxx, m_Plane0x, __vmaddfp(yyyy, m_Plane0y, __vmaddfp(zzzz, m_Plane0z, m_Plane0w)));
	__vector4 sum1 = __vmaddfp(xxxx, m_Plane1x, __vmaddfp(yyyy, m_Plane1y, __vmaddfp(zzzz, m_Plane1z, m_Plane1w)));
	__vector4 sum2 = __vmaddfp(xxxx, m_Plane2x, __vmaddfp(yyyy, m_Plane2y, __vmaddfp(zzzz, m_Plane2z, m_Plane2w)));
	__vector4 sum3 = __vmaddfp(xxxx, m_Plane3x, __vmaddfp(yyyy, m_Plane3y, __vmaddfp(zzzz, m_Plane3z, m_Plane3w)));

	//	return (Plane.DistanceToPlane((Vector3)CullSphere) > CullSphere.w)? cullOutside : cullInside;

	// Returns 1 if a <= b for all components.
	if(_vlequalfp(sum0, neg_wwww))
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
	if(_vlequalfp(sum1, neg_wwww))
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
	if(_vlequalfp(sum2, neg_wwww))
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
	if(_vlequalfp(sum3, neg_wwww))
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
#endif

#else
	/*
	// shaft class AABB <-> plane comparison
	if (cullOutside == pShadowMap->GetTile(3)->GetShaft().IsVisible(AABB))
	ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
	if (cullOutside == pShadowMap->GetTile(2)->GetShaft().IsVisible(AABB))
	ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
	if (cullOutside == pShadowMap->GetTile(1)->GetShaft().IsVisible(AABB))
	ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
	if (cullOutside == pShadowMap->GetTile(0)->GetShaft().IsVisible(AABB))
	ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
	*/

	if((cullOutside == PlaneSphereTest(m_pLightPlanes->TopPlane4, vCullSphere)))
	{
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
	}
	else if((cullOutside == PlaneSphereTest(m_pLightPlanes->RightPlane4, vCullSphere)))
	{
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
	}
	else if((cullOutside == PlaneSphereTest(m_pLightPlanes->LeftPlane4, vCullSphere)))
	{
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
	}
	else if((cullOutside == PlaneSphereTest(m_pLightPlanes->BottomPlane, vCullSphere)))
	{
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
	}
	else
	{
		if((cullOutside == PlaneSphereTest(m_pLightPlanes->BottomPlane4, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->FarPlane4, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport4;

		if((cullOutside == PlaneSphereTest(m_pLightPlanes->TopPlane3, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->BottomPlane3, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->RightPlane3, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->LeftPlane3, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->FarPlane3, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport3;

		if((cullOutside == PlaneSphereTest(m_pLightPlanes->TopPlane2, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->BottomPlane2, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->RightPlane2, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->LeftPlane2, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->FarPlane2, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport2;

		if((cullOutside == PlaneSphereTest(m_pLightPlanes->TopPlane, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->RightPlane, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->LeftPlane, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
		else if((cullOutside == PlaneSphereTest(m_pLightPlanes->FarPlane, vCullSphere)))
			ObjectOutsideOfLightViewFrustums |= NotVisibleInViewport;
	}

#endif

	return ObjectOutsideOfLightViewFrustums;

	/*	
	AABB = AABB;
	u8 CullSphereInViewPorts = 0;

	//
	// this code checks if the mesh is outside of the light view frustum ... if it is not (that means
	// it is in the frustum or intersects), it is visible
	//
	if(!pShadowMap->GetShadowViewports(3)->IsSphereVisible(vCullSphere))
	// we might add a bounding box <-> viewport test here
	CullSphereInViewPorts |= NotVisibleInViewport4;
	if(!pShadowMap->GetShadowViewports(2)->IsSphereVisible(vCullSphere))
	// we might add a bounding box <-> viewport test here
	CullSphereInViewPorts |= NotVisibleInViewport3;
	if(!pShadowMap->GetShadowViewports(1)->IsSphereVisible(vCullSphere))
	// we might add a bounding box <-> viewport test here
	CullSphereInViewPorts |= NotVisibleInViewport2;
	if(!pShadowMap->GetShadowViewports(0)->IsSphereVisible(vCullSphere))
	// we might add a bounding box <-> viewport test here
	CullSphereInViewPorts |= NotVisibleInViewport;

	return CullSphereInViewPorts;
	*/
}

