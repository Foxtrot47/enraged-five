// 
// grshadowmap/cubeshadowmap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Cube Shadow maps
//
// Note: cool stuff :-)
//
#include "cubeshadowmap.h"
#include "grprofile/pix.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/light.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "system/xtl.h"
#include "grcore/device.h"

#define WRITEOUTCUBEMAPFACES

#if __XENON
#include "embedded_rage_blendcubeshadows_fxl_final.h"
#include "embedded_rage_cubeshadowpreview_fxl_final.h"
#include "embedded_rage_cubeshadow_fxl_final.h"
#elif __PPU
#include "embedded_rage_blendcubeshadows_psn.h"
#include "embedded_rage_cubeshadowpreview_psn.h"
#include "embedded_rage_cubeshadow_psn.h"
#elif __WIN32PC
#include "embedded_rage_blendcubeshadows_win32_30.h"
#include "embedded_rage_cubeshadowpreview_win32_30.h"
#include "embedded_rage_cubeshadow_win32_30.h"
#endif

namespace rage 
{

// constructor for the point light class
CShadowedPointLight::CShadowedPointLight()
{
	SetDefaults(-1);
}

// here we can set which light is currently used
CShadowedPointLight::CShadowedPointLight(int whichLight)

{
	SetDefaults(whichLight);
}	

// sets default values
void CShadowedPointLight::SetDefaults(int whichLight)
{
	m_GeometryScale = 0.040f;
	m_LightAttenuationEnd = 0.710f;
	m_fDepthBias = 0.0f;
	m_fLightVSMEpsilon = 0.001f;
	m_pCubeShadowMap = NULL;
	m_WhichLight = whichLight;
}

// return a string
const char* CShadowedPointLight::GetNumberName() const
{
	return sm_NumberNames[m_WhichLight];
}


#if __BANK
void CShadowedPointLight::AddWidgets(bkBank& bank)
{
	bank.AddSlider("Light Attenuation", &m_LightAttenuationEnd, 0.0f, 10.0f, 0.0001f);
	bank.AddSlider("Scale Geometry", &m_GeometryScale, 0.0f, 10.0f, 0.0001f);
	bank.AddSlider("Cure Shadow Acne", &m_fDepthBias, 0.0f, 1.0f, 0.0001f);
	bank.AddSlider("Variance Shadow Map Epsilon Value", &m_fLightVSMEpsilon, 0.0f, 1.0f, 0.0001f);
}
#endif

};
 // end name space

// this must be here so that it finds rage::CShadowedPointLight:
#include "cubeshadowmap_parser.h"

namespace rage 
{
// if this triggers, add the required tile names to sm_NumberNames:
CompileTimeAssert(CShadowedPointLight::MAX_LIGHTS==8);

const char* CShadowedPointLight::sm_NumberNames[MAX_LIGHTS] = 
{
	"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eight"
};


//---------------------------------------------------------------------------
//  Constructor: initialize member variables
//
//---------------------------------------------------------------------------
CCubeShadowMap::CCubeShadowMap(int numLights, bool bLoadLightData) :
	m_ShowCubeShadowMap(false),
	m_bShadowCollector(false),
	m_LightIndexOffset(0)
{
	m_GeometryScaleID = grcegvNONE;
	m_LightAttenuationEndID = grcegvNONE;

	// initialize with meaningful default values
	m_lightPos = Vector3(0.0, 0.0, 0.0);
	m_fScalePreview = 100.0f;

	m_DepthBuffer = NULL;

	// initialize tiles:
	Assert(numLights>0);
	Assert(numLights<=CShadowedPointLight::MAX_LIGHTS);
	m_ShadowedPointLights.Resize(numLights);

	for (int i=0;i<m_ShadowedPointLights.GetCount();i++)
	{
		m_ShadowedPointLights[i]=rage_new CShadowedPointLight(i);
	}

	//
	// Remove the comments below to make xml files load during startup
	//
	if(bLoadLightData)
	{
		// get default asset path -> root of asset directory
		ASSET.PushFolder("$/tune");

		// load tunables file from asset path
		LoadTunables<CCubeShadowMap>("CubeShadowMaps"); 

		ASSET.PopFolder();
	}
 
	INIT_PARSER; // Creates the PARSER object and registers any autoregisterable classes
}

//---------------------------------------------------------------------------
//  
// InitShaders()
// load shaders and shader variables
//
//---------------------------------------------------------------------------
void CCubeShadowMap::InitShaders(const char *path)
{	
	if (path!=NULL)
		ASSET.PushFolder(path);

	//
	// load the shaders
	//
	m_CubeDepthShader = grmShaderFactory::GetInstance().Create();
	m_CubeDepthShader->Load("rage_cubeshadow");

	// 
	m_CubeDepthPreviewShader = grmShaderFactory::GetInstance().Create();
	m_CubeDepthPreviewShader->Load("rage_cubeshadowpreview");

	// blend shadow maps into color buffer
	m_CubeBlendShader = grmShaderFactory::GetInstance().Create();
	m_CubeBlendShader->Load("rage_blendcubeshadows");

	// cube texture
	m_CubeShadowMapPreviewID = grcEffect::LookupGlobalVar("CubeShadowMap");

	// handle for shared geometry scale variable
	m_GeometryScaleID = grcEffect::LookupGlobalVar("GeometryScale");

	// handle for shared light attenuation value
	m_LightAttenuationEndID = grcEffect::LookupGlobalVar("LightAttenuationEnd");

	// get handle to the shared variable that holds the point light position
	m_PointLightPositionID = grcEffect::LookupGlobalVar( "PointLightPosition" );

	// get handle to the shared variable that holds the falloff / attenuation of the point light
	m_PointLightFalloffID = grcEffect::LookupGlobalVar( "PointLightAttenuation" );

	// get handle depth bias
	m_DepthBiasID = m_CubeBlendShader->LookupVar("fDepthBias");

	// get handle for variance shadow map epsilon value
	m_LightVSMEpsilonID = m_CubeBlendShader->LookupVar("fLightVSMEpsilon");

	// technique to copy previews onto the screen
	m_DrawTechnique = m_CubeDepthPreviewShader->LookupTechnique("draw");	

	// lookup texture stages
	m_RenderMapId = m_CubeDepthPreviewShader->LookupVar("RenderMap");

	if (path)
		ASSET.PopFolder();
}


//---------------------------------------------------------------------------
//
//  ~CCubeShadowMap
//  deconstructor free shader resources
//
//---------------------------------------------------------------------------
CCubeShadowMap::~CCubeShadowMap(void)
{
	SHUTDOWN_PARSER;

	delete m_CubeDepthShader;
	delete m_CubeDepthPreviewShader;
	delete m_CubeBlendShader;
}

//---------------------------------------------------------------------------
//
//  CreateRenderTargets()
//  Allocate the memory for a cube depth map and a cube color map
//
//---------------------------------------------------------------------------
void CCubeShadowMap::CreateRenderTargets()
{
	int rezx,rezy;

	// color render target == shadow map
	grcTextureFactory::CreateParams cp;
	cp.Multisample = 0;
#if __XENON
	cp.Multisample = 4;
	rezx = XENON_CUBESHADOWMAP_SIZE;
	rezy = XENON_CUBESHADOWMAP_SIZE;
#elif __PPU
	rezx = PS3_CUBESHADOWMAP_SIZE;
	rezy = PS3_CUBESHADOWMAP_SIZE;
#else
	rezx = PC_CUBESHADOWMAP_SIZE;
	rezy = PC_CUBESHADOWMAP_SIZE;
#endif

	// as a render target it is a 8:8:8:8 ... as a texture it is 16:16
#if __PPU
	cp.Format = grctfG16R16F;
#elif __XENON
	cp.Format = grctfG16R16;
#endif
	cp.Parent = NULL;

	// depth buffer
	grcTextureFactory::CreateParams para;
	para.IsResolvable = true;	
	para.UseHierZ = true;
	para.HasParent = true;

#if __XENON //|| __PPU
	para.Multisample = 4;
#endif

	char shadowName[128];

	m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Depth Buffer", grcrtDepthBuffer, rezx, rezy, 32, &para);

	for (int i = 0;i < m_ShadowedPointLights.GetCount();i++)
	{
		//
		// for cube maps we use G16R16 render target with a value range of -32..32 to store 
		// depth^2 data in the second channel for variance shadow maps
		//
		cp.Parent = m_DepthBuffer;

		sprintf(shadowName,"Cube Shadow Map%d",i);
		m_ShadowedPointLights[i]->m_pCubeShadowMap = grcTextureFactory::GetInstance().CreateRenderTarget(shadowName, grcrtCubeMap, rezx, rezy, 32, &cp);

	}
}

//---------------------------------------------------------------------------
//  DeleteRenderTargets()
//  Undo what the CreateRenderTargets() function did
//
//---------------------------------------------------------------------------
void CCubeShadowMap::DeleteRenderTargets(void)
{
	for (int i=0;i<m_ShadowedPointLights.GetCount();i++)
	{
		if (m_ShadowedPointLights[i])
		{
			m_ShadowedPointLights[i]->m_pCubeShadowMap->Release();
			m_ShadowedPointLights[i]->m_pCubeShadowMap=NULL;
		}
	}
	if(m_DepthBuffer)
	{
		m_DepthBuffer->Release();
		m_DepthBuffer = NULL;	
	}
}

//---------------------------------------------------------------------------
//
// DrawShadow(grcCubeFace face, int whichLight)
// render workhorse that draws into one cube shadow map face
//
//---------------------------------------------------------------------------
void CCubeShadowMap::DrawShadow(grcCubeFace face, int whichLight)
{
	// Lock the render target
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_ShadowedPointLights[whichLight]->m_pCubeShadowMap, 
														m_DepthBuffer, face);

	// Clear the render target
//  	if(face == grcNegativeX)
  //    	GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);

//#if !__XENON
	GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);
//#endif

	// force cube depth shader
	grmModel::SetForceShader(m_CubeDepthShader);

	RenderShadowCasters(*grcViewport::GetCurrent(), true /* not used */);

	// undo the force of the shader
	grmModel::SetForceShader(0);

#if __XENON
	// try to get the free clear from the resolve
	clearParams.ClearColor=true;
	clearParams.Color.Set(1,1,1,1);
	clearParams.ClearDepthStencil=true;
	grcTextureFactory::GetInstance().UnlockRenderTarget(0, &clearParams);
#else
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);
#endif
}

//---------------------------------------------------------------------------
//  
//  RenderIntoCubeShadowMap()
//  Renders into each of the faces by setting the right view and projection matrix
//  Note the 90 degree angle of the projection
//
//---------------------------------------------------------------------------
void CCubeShadowMap::RenderIntoCubeShadowMap()
{

	// measure the time-frame a shadow map draw call takes
	PIXBegin(0, "Render into Cube Shadow Map");

	grcViewport* pOld = grcViewport::GetCurrent();

	for (int i = 0;i < m_ShadowedPointLights.GetCount();i++)
	{
		if(m_GeometryScaleID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_GeometryScaleID, m_ShadowedPointLights[i]->m_GeometryScale);

		if(m_LightAttenuationEndID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_LightAttenuationEndID, m_ShadowedPointLights[i]->m_LightAttenuationEnd);

		// grab current group of lights
		grcLightGroup * LightGroup = grcState::GetLightingGroup();

		// get light direction and position
		m_lightPos = VEC3V_TO_VECTOR3(LightGroup->GetPosition(i + m_LightIndexOffset));
		m_PointLightFalloff = LightGroup->GetFalloff( i + m_LightIndexOffset );

		// set attenuation
		grcEffect::SetGlobalVar(m_PointLightFalloffID, m_PointLightFalloff);

		grcViewport view;

		Matrix34 camera;
		view.Perspective(90.0f, 1.0f, 0.0001f, 20.0f);
		view.SetWorldIdentity();

		// Draw -X
		camera.LookAt(VEC3_ZERO, Vector3(-1.0f, 0.0f, 0.0f), YAXIS);
		camera.a.Negate();
		camera.d = m_lightPos;
		view.SetCameraMtx(RCC_MAT34V(camera));
		grcViewport::SetCurrent(&view);
		DrawShadow(grcNegativeX, i);

		// Draw +X
		camera.LookAt(VEC3_ZERO, Vector3(1.0f, 0.0f, 0.0f), YAXIS);
		camera.a.Negate();
		camera.d = m_lightPos;
		view.SetCameraMtx(RCC_MAT34V(camera));
		grcViewport::SetCurrent(&view);
		DrawShadow(grcPositiveX, i);

		// Draw -Y
		camera.LookAt(VEC3_ZERO, Vector3(0.0f, -1.0f, 0.0f), ZAXIS);
		camera.a.Negate();
		camera.d = m_lightPos;
		view.SetCameraMtx(RCC_MAT34V(camera));
		grcViewport::SetCurrent(&view);
		DrawShadow(grcNegativeY, i);

		// Draw +Y
		camera.LookAt(VEC3_ZERO, Vector3(0.0f, 1.0f, 0.0f), ZAXIS);
		camera.b.Negate();
		camera.d = m_lightPos;
		view.SetCameraMtx(RCC_MAT34V(camera));
		grcViewport::SetCurrent(&view);
		DrawShadow(grcPositiveY, i);

		// Draw -Z
		camera.LookAt(VEC3_ZERO, Vector3(0.0f, 0.0f, -1.0f), YAXIS);
		camera.a.Negate();
		camera.d = m_lightPos;
		view.SetCameraMtx(RCC_MAT34V(camera));
		grcViewport::SetCurrent(&view);
		DrawShadow(grcNegativeZ, i);

		// Draw +Z
		camera.LookAt(VEC3_ZERO, Vector3(0.0f, 0.0f, 1.0f), YAXIS);
		camera.a.Negate();
		camera.d = m_lightPos;
		view.SetCameraMtx(RCC_MAT34V(camera));
		grcViewport::SetCurrent(&view);
		DrawShadow(grcPositiveZ, i);
	}

	// Restore the viewport
	grcViewport::SetCurrent(pOld);

	// measure the time-frame a shadow map draw call takes
	PIXEnd();
}


//---------------------------------------------------------------------------
//  
// RenderShadowsIntoShadowCollector()
// Renders shadow data into the 2D screenspace Black & White texture
//
//---------------------------------------------------------------------------
void CCubeShadowMap::RenderShadowsIntoShadowCollector()
{
	for (int i = 0;i < m_ShadowedPointLights.GetCount();i++)
	{
		// first draw into the shadow map with Z buffer on
		BeginRenderCubeShadowMapIntoShadowCollector(i);
		RenderCubeShadowMapIntoShadowCollector();
		EndRenderCubeShadowMapIntoShadowCollector();
	}
}

//---------------------------------------------------------------------------
//  BeginRenderTileIntoShadowCollector()
//  Prepares the render call
//
//---------------------------------------------------------------------------
void CCubeShadowMap::BeginRenderCubeShadowMapIntoShadowCollector(int whichLight)
{
	// measure the time-frame a shadow map draw call takes
	PIXBegin(0, "Render into Shadow Collector");

	// send the flag to the shadow collector class
	GRSHADOWCOLLECTOR->SetShadowPreviewFlag(m_bShadowCollector, 0);
	
	// set depth bias value
	m_CubeBlendShader->SetVar(m_DepthBiasID, m_ShadowedPointLights[whichLight]->m_fDepthBias);

	// set variance shadow map epsilon value
	m_CubeBlendShader->SetVar(m_LightVSMEpsilonID, m_ShadowedPointLights[whichLight]->m_fLightVSMEpsilon);

    // set cube shadow map
	grcEffect::SetGlobalVar(m_CubeShadowMapPreviewID, m_ShadowedPointLights[whichLight]->m_pCubeShadowMap);

	// check out if it is still in there
#ifndef WRITEOUTCUBEMAPFACES
	// make a screenshot of each face ...
	grcImage *image;
	char shotName[64];
	for(int i = 0; i < 6; i++)
	{
		image = GRCDEVICE.CaptureCubeFaceScreenshot(m_ShadowedPointLights[0]->m_pCubeShadowMap, i) ;
		sprintf(shotName,"t:/CubeMapFace%d.jpg", i);
		if (image) 
		{
			image->SaveJPEG(shotName);
			image->Release();
		}
	}
#endif

	// set the point light position
	grcEffect::SetGlobalVar(m_PointLightPositionID, RCC_VEC3V(m_lightPos));

	// PS3 won't work without this ...
#if __PPU
	GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, 0);
#endif
}

//---------------------------------------------------------------------------
//  RenderCubeShadowMapIntoShadowCollector()
//  involves the virtual call to RenderShadowCasters() that uses 
//  the DrawNoShaders* functions and forces the right shader upfront.
//
//---------------------------------------------------------------------------
void CCubeShadowMap::RenderCubeShadowMapIntoShadowCollector()
{
	// force depth shader
	grmModel::SetForceShader(m_CubeBlendShader);

	RenderShadowCasters(*grcViewport::GetCurrent(), true /* not used */);

	// undo the force of the shader
	grmModel::SetForceShader(0);
}

//---------------------------------------------------------------------------
//  
// EndRenderTileIntoShadowCollector()
// ends the render call above
//
//---------------------------------------------------------------------------
void CCubeShadowMap::EndRenderCubeShadowMapIntoShadowCollector()
{
	// measure the time-frame the shadow collector draw call takes
	PIXEnd();
}


//---------------------------------------------------------------------------
// ShowCubeShadowRenderTargets()
// renders into a cube a cube shadow preview .. the cube is situated around the camera
//
// Note: this only supports the first point light / cube shadow map
//
//---------------------------------------------------------------------------
void CCubeShadowMap::ShowCubeShadowRenderTargets()
{

	if( m_ShowCubeShadowMap )
	{
		grcState::SetCullMode(grccmNone);

		Matrix34 draw = M34_IDENTITY;
		draw.Scale(m_fScalePreview);
		Vector3 center = Vector3(0.0, 0.0, 0.0);
		draw.Translate(m_lightPos);
		grcWorldMtx(draw);

		grcEffect::SetGlobalVar(m_CubeShadowMapPreviewID, m_ShadowedPointLights[0]->m_pCubeShadowMap);	

		for(int i = 0; i < 6; i++)
		{
			m_CubeDepthPreviewShader->BlitIntoCubeMapFace((grcCubeFace)i, m_DrawTechnique);
		}

		grcState::SetCullMode(grccmBack);
	}
}


#if __BANK
typedef void (CCubeShadowMap::*CCubeShadowMapMember0)();

void CCubeShadowMap::AddSaveWidgets(bkBank& DEV_ONLY(bk))
{
#if __DEV
	// gcc wants a double cast to remove all doubt as to how you want this to work ...
	bk.AddButton("Save Tunables...",datCallback((Member0)(CCubeShadowMapMember0)&CCubeShadowMap::SaveTunablesCB<CCubeShadowMap>,this));
	bk.AddButton("Load Tunables...",datCallback((Member0)(CCubeShadowMapMember0)&CCubeShadowMap::LoadTunablesCB<CCubeShadowMap>,this));
#endif
}

void CCubeShadowMap::AddWidgets(bkBank &bk)
{
	bk.AddToggle("Show Cube Shadow Map", &m_ShowCubeShadowMap);
	bk.AddToggle("Shadow Collector Map", &m_bShadowCollector);

	for (int i=0; i <m_ShadowedPointLights.GetCount();i++)
	{
		const int GROUP_NAME_LEN=128;
		char groupName[GROUP_NAME_LEN];
		bk.PushGroup(formatf(groupName,GROUP_NAME_LEN,"Cube Map #%d",i));
		m_ShadowedPointLights[i]->AddWidgets(bk);
		bk.PopGroup();
	}
}
#endif
} // namespace rage
