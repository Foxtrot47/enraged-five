// 
// grshadowmap/cascadedshadowmapculling.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifndef GRSHADOWMAP__CASCADEDSHADOWMAPCULLING_H
#define GRSHADOWMAP__CASCADEDSHADOWMAPCULLING_H

#include "cascadedshadowmap.h"


#define INTRINSICVERSION 1


namespace rage
{
	class grcViewport;
	class CShadowMap;


	class CCascadedShadowCulling : public datBase
	{
	private:

	public:

		enum eCullCameraViewPortVisibilityBitMask
		{
			NotVisibleInViewport = 2,
			NotVisibleInViewport2 = 4,
			NotVisibleInViewport3 = 8,
			NotVisibleInViewport4 = 16,
		};

		enum eCullSphereInCullingPlanesBitMask
		{
			CullingPlaneLeft = 2,
			CullingPlaneRight = 4,
			CullingPlaneNear = 8,
		};


		//	u8 CullSphereInViewPorts;

		// Constructor/Destructor
		CCascadedShadowCulling();
		~CCascadedShadowCulling();

		void GetCameraMatrix(Matrix34& CameraMatrix);

		bool GenerateLightMatrixAndShadowMapViewportsForCulling(Matrix34& CameraMatrix, CShadowMap* pShadowMap, Vector3::Vector3Param LightDirection);

		//
		// PURPOSE
		//	culls objects against culling planes that are parallel to the viewport planes
		// PARAMS
		//	expects the bounding sphere as a parameter
		// RETURN
		//	returns true if the sphere is outside of the culling planes
		// NOTES
		//	
		u8 CullBoundingSphereAABBAgainstCullingPlanes(grcViewport *pVP, Vector4& vCullSphere, Vector3& lightDir);

		//
		// PURPOSE
		//	culls objects against the light view frustum viewports
		// PARAMS
		//	expects the bounding sphere as a parameter
		// RETURN
		//	returns bool values to show if a culling sphere is inside one or several viewport
		// NOTES
		//	
		u8 CullBoundingSphereAABBAgainstViewPorts(CShadowMap* pShadowMap, Vector4& vCullSphere);

		void GetLightViewFrustumPlanes(CShadowMap* pShadowMap);


		// static functions
		// keep track of the instance
		static void Init();
		static void Terminate();
		static CCascadedShadowCulling *GetInstance() {return sm_instance;}
	private:

		struct LightViewFrustumPlanes
		{
			Vector4 TopPlane;
			Vector4 RightPlane; 
			Vector4 LeftPlane;
			Vector4 BottomPlane;
			Vector4 FarPlane;

			Vector4 TopPlane2;
			Vector4 RightPlane2; 
			Vector4 LeftPlane2;
			Vector4 BottomPlane2;
			Vector4 FarPlane2;

			Vector4 TopPlane3;
			Vector4 RightPlane3; 
			Vector4 LeftPlane3;
			Vector4 BottomPlane3;
			Vector4 FarPlane3;

			Vector4 TopPlane4;
			Vector4 RightPlane4; 
			Vector4 LeftPlane4;
			Vector4 BottomPlane4;
			Vector4 FarPlane4;
		};

		LightViewFrustumPlanes m_LightPlanes;

		LightViewFrustumPlanes* m_pLightPlanes;

		// just test the bounding sphere against a plane
		grcCullStatus PlaneSphereTest(Vector4& Plane, Vector4& CullSphere);

		// just test a bounding sphere and a AABB box against the plane
		grcCullStatus PlaneSphereAABBTest(Vector4& Plane, Vector4& CullSphere, spdAABB& AAbox);

		static CCascadedShadowCulling *sm_instance;			// instance
#if INTRINSICVERSION
		// transposed culling planes for the Intrinsic based version
		Vector4 m_Plane0x, m_Plane0y, m_Plane0z, m_Plane0w;
		Vector4 m_Plane1x, m_Plane1y, m_Plane1z ,m_Plane1w;
		Vector4 m_Plane2x, m_Plane2y, m_Plane2z ,m_Plane2w;
		Vector4 m_Plane3x, m_Plane3y, m_Plane3z ,m_Plane3w;
#endif
	}; // end of class


#define CASCADEDSHADOWMAPCULLING (rage::CCascadedShadowCulling::GetInstance())
}	// name space range


#endif // end include guards
