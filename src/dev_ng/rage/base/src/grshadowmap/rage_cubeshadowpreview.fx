#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

#if !__FXL
// the "workhorse" sampler for all render target operations
texture RenderMap;
sampler RenderMapSampler = sampler_state
{
   Texture = <RenderMap>;
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
};
#else
// the "workhorse" sampler for all render target operations
sampler RenderMapSampler : RenderMap = sampler_state
{
//   Texture = <RenderMap>;
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
};

#endif

struct vertexInput 
{
    float4 pos				: POSITION0;
    float3 normal 			: NORMAL;
};

struct vertexOutput  
{
   float4 pos:		DX_POS;
   float3 worldPos:	TEXCOORD0;
   float3 normal:	TEXCOORD1;
};

vertexOutput VS_Cube( vertexInput IN ) 
{	
	vertexOutput OUT;
	OUT.pos = mul(IN.pos, gWorldViewProj);
	OUT.worldPos = (float3)mul(IN.pos, gWorld);
	OUT.normal = (float3)mul(IN.normal, (float3x3)gWorldView);

	return OUT;
}

float4 PS_Cube( vertexOutput IN ) : COLOR
{
	float4 cubeColor = texCUBE(CubeShadowMapSampler, IN.worldPos);
	return cubeColor;
}

// -------------------------------------------------------------
// PSCopyRT
// - plain copy
// -------------------------------------------------------------
/*
float4 PSCopyRT( rageVertexOutputPassThrough IN): COLOR
{
     return tex2D(RenderMapSampler, IN.texCoord0);
}
*/

technique draw
{
	pass p0 
	{        
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Cube();
		PixelShader  = compile PIXELSHADER PS_Cube();
	}
}

technique drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_Cube();
		PixelShader  = compile PIXELSHADER PS_Cube();
	}

}

technique unlit_draw
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_Cube();
		PixelShader  = compile PIXELSHADER PS_Cube();
	}

}

technique unlit_drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_Cube();
		PixelShader  = compile PIXELSHADER PS_Cube();
	}

}

/*
technique CopyRT
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
        PixelShader  = compile PIXELSHADER PSCopyRT();
    }
}
*/
