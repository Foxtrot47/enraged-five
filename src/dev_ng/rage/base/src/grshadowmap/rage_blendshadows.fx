//-----------------------------------------------------
// Rage Diffuse shadow map shader
//
 
#include "../../../../rage/base/src/shaderlib/rage_shadowmap_common.fxh"
#include "../../../../rage/base/src/shaderlib/rage_skin.fxh"
#include "../../../../rage/base/src/shaderlib/rage_blendshadows.fxh"
#include "../shaderlib/rage_xplatformtexturefetchmacros.fxh"



//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
vertexOutputShadow VS_ShadowCompositeSkin(rageSkinVertexInputBump IN) 
{
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
	float4 worldPos = float4(rageSkinTransform(IN.pos, boneMtx), 1);
	float3 worldNormal = mul(mul(IN.normal, (float3x3)boneMtx), (float3x3)gWorld); // this seems like a bit of a waist, we could combine the skina nd wold once and use it for both pos and normal
	return VS_ShadowCompositeCommon( mul(worldPos, gWorldViewProj), worldPos, worldNormal); 
}

vertexOutputShadow VS_ShadowComposite(rageVertexInputBump IN) 
{
	float4 posHom = float4(IN.pos, 1);
	float4 worldPos = mul(posHom, gWorld);
	float3 worldNormal = mul(IN.normal, (float3x3)gWorld);
	return VS_ShadowCompositeCommon( mul(posHom, gWorldViewProj), worldPos, worldNormal); 
}

float2 direction;

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
half4 PS_ShadowCompositeLighting(vertexOutputShadow IN) : COLOR
{
	return PS_ShadowCompositeCommon(IN);
}

float2 TSize = float2(1.0f / 640.0f, 1.0f / 640.0f);

float log_convol(float x0, float x, float y0, float y)
{
	return x + rageLog(y0 * exp(y - x) + x0);
}

float4 BlurLogX1(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	float2 s = IN.texCoord0.xy - TSize  * 4 * 0.5 * float2(1.0, 0.0);
	float a,b,a0,b0;
	a0 = 0.5;
	b0 = 0.5;

   	a = tex2D(DepthTextureSampler0, s + TSize * 0 * float2(1.0, 0.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 1 * float2(1.0, 0.0)).x;    	
   	float intermed0 =  log_convol( a0, a, b0, b );
	
   	a = tex2D(DepthTextureSampler0, s + TSize * 2 * float2(1.0, 0.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 3 * float2(1.0, 0.0)).x;    	
   	float intermed1 =  log_convol( a0, a, b0, b );
	
#if __XENON	
	return log_convol( a0, intermed0, b0, intermed1 );
#elif __PS3
	float2 Pack = 0;
	Pack.x = log_convol( a0, intermed0, b0, intermed1 );
	return (unpack_4ubyte(pack_2half(Pack)));
	//return Pack.x;
#elif __WIN32PC
	return 1.0f;	
#endif	
}

float4 BlurLogY1(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	float2 s = IN.texCoord0.xy - TSize  * 4 * 0.5 * float2(0.0, 1.0);
	float a,b,a0,b0;
	a0 = 0.5;
	b0 = 0.5;

   	a = tex2D(DepthTextureSampler0, s + TSize * 0 * float2(0.0, 1.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 1 * float2(0.0, 1.0)).x;    	
   	float intermed0 =  log_convol( a0, a, b0, b );
	
   	a = tex2D(DepthTextureSampler0, s + TSize * 2 * float2(0.0, 1.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 3 * float2(0.0, 1.0)).x;    	
   	float intermed1 =  log_convol( a0, a, b0, b );
	
#if __XENON	
	return log_convol( a0, intermed0, b0, intermed1 );
#elif __PS3
	float2 Pack = 0;
	Pack.x = log_convol( a0, intermed0, b0, intermed1 );
	return (unpack_4ubyte(pack_2half(Pack)));
//	return Pack.x;
#elif __WIN32PC
	return 1.0f;	
#endif	
}

float4 BlurLog2X1(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	float2 s = IN.texCoord0.xy - TSize  * 4 * 0.5 * float2(-1.0, 0.0);
	float a,b,a0,b0;
	a0 = 0.5;
	b0 = 0.5;

   	a = tex2D(DepthTextureSampler0, s + TSize * 0 * float2(-1.0, 0.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 1 * float2(-1.0, 0.0)).x;    	
   	float intermed0 =  log_convol( a0, a, b0, b );
	
   	a = tex2D(DepthTextureSampler0, s + TSize * 2 * float2(-1.0, 0.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 3 * float2(-1.0, 0.0)).x;    	
   	float intermed1 =  log_convol( a0, a, b0, b );
	
#if __XENON	
	return log_convol( a0, intermed0, b0, intermed1 );
#elif __PS3
	float2 Pack = 0;
	Pack.x = log_convol( a0, intermed0, b0, intermed1 );
	return (unpack_4ubyte(pack_2half(Pack)));
//	return Pack.x;
#elif __WIN32PC
	return 1.0f;	
#endif	
}

float4 BlurLog2Y1(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	float2 s = IN.texCoord0.xy - TSize  * 4 * 0.5 * float2(0.0, -1.0);
	float a,b,a0,b0;
	a0 = 0.5;
	b0 = 0.5;

   	a = tex2D(DepthTextureSampler0, s + TSize * 0 * float2(0.0, -1.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 1 * float2(0.0, -1.0)).x;    	
   	float intermed0 =  log_convol( a0, a, b0, b );
	
   	a = tex2D(DepthTextureSampler0, s + TSize * 2 * float2(0.0, -1.0)).x;
	b = tex2D(DepthTextureSampler0, s + TSize * 3 * float2(0.0, -1.0)).x;    	
   	float intermed1 =  log_convol( a0, a, b0, b );
#if __XENON	
	return log_convol( a0, intermed0, b0, intermed1 );
#elif __PS3
	float2 Pack = 0;
	Pack.x = log_convol( a0, intermed0, b0, intermed1 );
	return (unpack_4ubyte(pack_2half(Pack)));
//	return Pack.x;
#elif __WIN32PC
	return 1.0f;		
#endif	
}


#if __XENON
struct VSOutput
{
    float4 Position   : DX_POS;
    float4 TexCoord[8]: TEXCOORD0;
};

VSOutput VSBlur(rageVertexInput IN)
{
 	VSOutput OUT =(VSOutput)0;
 	
	const int taps = 5;
 	
	OUT.Position = float4(IN.pos, 1.0);
    float2 step = direction * TSize;
    float2 base = IN.texCoord0 - ((((float)taps - 1)*0.5f) * step);
      
    for (int i = 0; i < taps; i++)
    {
        OUT.TexCoord[i].xy = base;
        base += step;
    }
    
    return OUT;
}

VSOutput VSBlurXY9tap(rageVertexInput IN)
{
 	VSOutput OUT =(VSOutput)0;
 	
	//const float2 step[9] = {float2(-TSize, -TSize), float2(0.0f, -TSize), float2(TSize, -TSize), 
	//					    float2(-TSize,   0.0f), float2(0.0f,   0.0f), float2(TSize,   0.0f), 
	//					    float2(-TSize,  TSize), float2(0.0f,  TSize), float2(TSize,  TSize)};
 	
	OUT.Position = float4(IN.pos, 1.0);

    OUT.TexCoord[0].xy = IN.texCoord0 + float2(-TSize.x, -TSize.y);
    OUT.TexCoord[0].zw = IN.texCoord0 + float2( 0.0f,	 -TSize.y);
    OUT.TexCoord[1].xy = IN.texCoord0 + float2( TSize.x, -TSize.y);
    OUT.TexCoord[1].zw = IN.texCoord0 + float2(-TSize.x,  0.0f);
    OUT.TexCoord[2].xy = IN.texCoord0 + float2( 0.0f,     0.0f);
    OUT.TexCoord[2].zw = IN.texCoord0 + float2( TSize.x,  0.0f);
    OUT.TexCoord[3].xy = IN.texCoord0 + float2(-TSize.x,  TSize.y);
    OUT.TexCoord[3].zw = IN.texCoord0 + float2( 0.0f,     TSize.y);
    OUT.TexCoord[4].xy = IN.texCoord0 + float2( TSize.x,  TSize.y);

    return OUT;
}

VSOutput VSBlurXY25tap(rageVertexInput IN)
{
 	VSOutput OUT =(VSOutput)0;
 	
	OUT.Position = float4(IN.pos, 1.0);
	
	OUT.TexCoord[0].xy = IN.texCoord0;

    return OUT;
}


//------------------------------------------------------------------------------
// Logarithmic filtering
//------------------------------------------------------------------------------

float log_conv ( float x0, float X, float y0, float Y )
{
    return (X + rageLog(x0 + (y0 * exp(Y - X))));
}

float4 LogBox1D_5Taps_PS( VSOutput input)	: COLOR					                    
{
	const int taps = 5;
    float  sample[5];
    for (int i = 0; i < taps; i++)
    {
        sample[i] = tex2D( DepthTextureSampler0, input.TexCoord[i] ).x;
    }
 
    const float c = (1.f/float(taps));    
    
    float accum;
    accum = log_conv( c, sample[0], c, sample[1] );
    for (int i = 2; i < taps; i++)
    {
        accum = log_conv( 1.f, accum, c, sample[i] );
    }    
    
//    float4 accum4 = log_conv3( 1.f, accum, c, float3(sample[2], sample[3], sample[4]) );
        
    return accum; //dot(accum, 1/4.0f);
}

float4 LogBox2D_9Taps_PS( VSOutput input)	: COLOR					                    
{
    float  sample[9];

    sample[0] = tex2D( DepthTextureSampler0, input.TexCoord[0].xy ).x;
    sample[1] = tex2D( DepthTextureSampler0, input.TexCoord[0].zw ).x;
    sample[2] = tex2D( DepthTextureSampler0, input.TexCoord[1].xy ).x;
    sample[3] = tex2D( DepthTextureSampler0, input.TexCoord[1].zw ).x;
    sample[4] = tex2D( DepthTextureSampler0, input.TexCoord[2].xy ).x;
    sample[5] = tex2D( DepthTextureSampler0, input.TexCoord[2].zw ).x;
    sample[6] = tex2D( DepthTextureSampler0, input.TexCoord[3].xy ).x;
    sample[7] = tex2D( DepthTextureSampler0, input.TexCoord[3].zw ).x;
    sample[8] = tex2D( DepthTextureSampler0, input.TexCoord[4].xy ).x;
    
    float3 accum;
    float3 accum2;
    
    const float c = (1.f/float(3.0f));    
    
    // 0 - 1 - 2
    // 3 - 4 - 5
    // 6 - 7 - 8
    
    // in x direction
    accum.x = log_conv( c, sample[0], c, sample[1] );
    accum.x = log_conv( 1.f, accum.x, c, sample[2] );

    accum.y = log_conv( c, sample[3], c, sample[4] );
    accum.y = log_conv( 1.f, accum.y, c, sample[5] );

    accum.z = log_conv( c, sample[6], c, sample[7] );
    accum.z = log_conv( 1.f, accum.z, c, sample[8] );

    // in y direction
    accum2.x = log_conv( c, sample[0], c, sample[3] );
    accum2.x = log_conv( 1.f, accum2.x, c, sample[6] );

    accum2.y = log_conv( c, sample[1], c, sample[4] );
    accum2.y = log_conv( 1.f, accum2.y, c, sample[7] );

    accum2.z = log_conv( c, sample[2], c, sample[5] );
    accum2.z = log_conv( 1.f, accum2.z, c, sample[8] );

	// now combine both directions
    accum.x = log_conv( c, accum.x, c, accum.y );
    accum.x = log_conv( 1.f, accum.x, c, accum.z );

    accum2.x = log_conv( c, accum2.x, c, accum2.y );
    accum2.x = log_conv( 1.f, accum2.x, c, accum2.z );
    
    return log_conv( 0.5f, accum.x, 0.5f, accum2.x );
}

float4 log_conv4 ( float4 x0, float4 X, float4 y0, float4 Y )
{
    return (X + rageLog(x0 + (y0 * exp(Y - X))));
}
float4 LogBox2D_16Taps_PS( VSOutput IN)	: COLOR					                    
{
    //  0 -  1 -  2 -  3
    //  5 -  6 -  7 -  8
    //  9 - 10 - 11 - 12
    // 13 - 14 - 15 - 16
    
    float4 sample, sample1, sample2, sample3;
  	float2 TexC = IN.TexCoord[0].xy; 


	// upper left corner
	sample.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,  -3.0f));
	sample.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,  -3.0f));
	sample.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,  -3.0f));
	sample.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,  -3.0f));
	
	sample1.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,  -1.0f));
	sample1.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,  -1.0f));
	sample1.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,  -1.0f));
	sample1.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,  -1.0f));
	
	sample2.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,   1.0f));
	sample2.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,   1.0f));
	sample2.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,   1.0f));
	sample2.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,   1.0f));
	
	sample3.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,   3.0f));
	sample3.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,   3.0f));
	sample3.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,   3.0f));
	sample3.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,   3.0f));
	
    float accum0, accum1, accum2, accum3, accum4, accum5, accum6, accum7;
    const float c = (1.f/float(4.0f));   
    
    // we can vectorize the following to three log_conv's 
	
	//float4 log_conv4 ( float4 x0, float4 X, float4 y0, float4 Y )
	
	// in x direction
    accum0 = log_conv( c, sample.x, c, sample.y );
    accum0 = log_conv( 1.f, accum0,   c, sample.z );
    accum0 = log_conv( 1.f, accum0,   c, sample.w );
    
    accum1 = log_conv( c, sample1.x, c, sample1.y );
    accum1 = log_conv( 1.f, accum1,   c, sample1.z );
    accum1 = log_conv( 1.f, accum1,   c, sample1.w );

    accum2 = log_conv( c, sample2.x, c, sample2.y );
    accum2 = log_conv( 1.f, accum2,   c, sample2.z );
    accum2 = log_conv( 1.f, accum2,   c, sample2.w );

    accum3 = log_conv( c, sample3.x, c, sample3.y );
    accum3 = log_conv( 1.f, accum3,   c, sample3.z );
    accum3 = log_conv( 1.f, accum3,   c, sample3.w );
    
    //  0 -  1 -  2 -  3
    //  5 -  6 -  7 -  8
    //  9 - 10 - 11 - 12
    // 13 - 14 - 15 - 16
    

	// in y direction
    accum4 = log_conv( c, sample.x, c, sample1.x );
    accum4 = log_conv( 1.f, accum4,   c, sample2.x );
    accum4 = log_conv( 1.f, accum4,   c, sample3.z );
    
    accum5 = log_conv( c, sample.y, c, sample1.y );
    accum5 = log_conv( 1.f, accum5,   c, sample2.y );
    accum5 = log_conv( 1.f, accum5,   c, sample3.y );

    accum6 = log_conv( c, sample.z, c, sample1.z );
    accum6 = log_conv( 1.f, accum6,   c, sample2.z );
    accum6 = log_conv( 1.f, accum6,   c, sample3.z );

    accum7 = log_conv( c, sample.w, c, sample1.w );
    accum7 = log_conv( 1.f, accum7,   c, sample2.w );
    accum7 = log_conv( 1.f, accum7,   c, sample3.w );
    
	// now combine both directions
    accum0 = log_conv( c, accum0, c, accum1 );
    accum0 = log_conv( 1.f, accum0, c, accum2 );
    accum0 = log_conv( 1.f, accum0, c, accum3 );

    accum1 = log_conv( c, accum4, c, accum5 );
    accum1 = log_conv( 1.f, accum1, c, accum6 );
    accum1 = log_conv( 1.f, accum1, c, accum7 );
    
    
    return log_conv( 0.5f, accum0, 0.5f, accum1 );
}

/*
float4 log_conv4 ( float4 x0, float4 X, float4 y0, float4 Y )
{
    return (X + rageLog(x0 + (y0 * exp(Y - X))));
}
float4 LogBox2D_16Taps_PS( VSOutput IN)	: COLOR					                    
{
    //  0 -  1 -  2 -  3
    //  5 -  6 -  7 -  8
    //  9 - 10 - 11 - 12
    // 13 - 14 - 15 - 16
    
    float4 sample, sample1, sample2, sample3;
  	float2 TexC = IN.TexCoord[0].xy; 


	// upper left corner
	sample.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,  -3.0f));
	sample.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,  -3.0f));
	sample.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,  -3.0f));
	sample.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,  -3.0f));
	
	sample1.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,  -1.0f));
	sample1.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,  -1.0f));
	sample1.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,  -1.0f));
	sample1.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,  -1.0f));
	
	sample2.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,   1.0f));
	sample2.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,   1.0f));
	sample2.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,   1.0f));
	sample2.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,   1.0f));
	
	sample3.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,   3.0f));
	sample3.y = _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,   3.0f));
	sample3.z = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,   3.0f));
	sample3.w = _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,   3.0f));
	
    float accum0, accum1, accum2, accum3, accum4, accum5, accum6, accum7;
    const float c = (1.f/float(4.0f));   
    
    // we can vectorize the following to three log_conv's 
	
	// in x direction
    accum0 = log_conv( c, sample.x, c, sample.y );
    accum0 = log_conv( 1.f, accum0,   c, sample.z );
    accum0 = log_conv( 1.f, accum0,   c, sample.w );
    
    accum1 = log_conv( c, sample1.x, c, sample1.y );
    accum1 = log_conv( 1.f, accum1,   c, sample1.z );
    accum1 = log_conv( 1.f, accum1,   c, sample1.w );

    accum2 = log_conv( c, sample2.x, c, sample2.y );
    accum2 = log_conv( 1.f, accum2,   c, sample2.z );
    accum2 = log_conv( 1.f, accum2,   c, sample2.w );

    accum3 = log_conv( c, sample3.x, c, sample3.y );
    accum3 = log_conv( 1.f, accum3,   c, sample3.z );
    accum3 = log_conv( 1.f, accum3,   c, sample3.w );
    
    //  0 -  1 -  2 -  3
    //  5 -  6 -  7 -  8
    //  9 - 10 - 11 - 12
    // 13 - 14 - 15 - 16
    

	// in y direction
    accum4 = log_conv( c, sample.x, c, sample1.x );
    accum4 = log_conv( 1.f, accum4,   c, sample2.x );
    accum4 = log_conv( 1.f, accum4,   c, sample3.z );
    
    accum5 = log_conv( c, sample.y, c, sample1.y );
    accum5 = log_conv( 1.f, accum5,   c, sample2.y );
    accum5 = log_conv( 1.f, accum5,   c, sample3.y );

    accum6 = log_conv( c, sample.z, c, sample1.z );
    accum6 = log_conv( 1.f, accum6,   c, sample2.z );
    accum6 = log_conv( 1.f, accum6,   c, sample3.z );

    accum7 = log_conv( c, sample.w, c, sample1.w );
    accum7 = log_conv( 1.f, accum7,   c, sample2.w );
    accum7 = log_conv( 1.f, accum7,   c, sample3.w );
    
	// now combine both directions
    accum0 = log_conv( c, accum0, c, accum1 );
    accum0 = log_conv( 1.f, accum0, c, accum2 );
    accum0 = log_conv( 1.f, accum0, c, accum3 );

    accum1 = log_conv( c, accum4, c, accum5 );
    accum1 = log_conv( 1.f, accum1, c, accum6 );
    accum1 = log_conv( 1.f, accum1, c, accum7 );
    
    
    return log_conv( 0.5f, accum0, 0.5f, accum1 );
}
*/

float4 LogBox2D_25Taps_PS( VSOutput IN)	: COLOR					                    
{
    float  sample[25];
    
    //  0 -  1 -  2 -  3 -  4
    //  5 -  6 -  7 -  8 -  9
    // 10 - 11 - 12 - 13 - 14
    // 15 - 16 - 17 - 18 - 19
    // 20 - 21 - 22 - 23 - 24
    sample[0] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-2.0f * TSize.x, -2.0f * TSize.y) ).x;
    sample[1] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-1.0f * TSize.x, -2.0f * TSize.y) ).x;
    sample[2] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 0.0f * TSize.x, -2.0f * TSize.y) ).x;
    sample[3] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 1.0f * TSize.x, -2.0f * TSize.y) ).x;
    sample[4] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 2.0f * TSize.x, -2.0f * TSize.y) ).x;

    sample[5] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-2.0f * TSize.x, -1.0f * TSize.y) ).x;
    sample[6] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-1.0f * TSize.x, -1.0f * TSize.y) ).x;
    sample[7] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 0.0f * TSize.x, -1.0f * TSize.y) ).x;
    sample[8] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 1.0f * TSize.x, -1.0f * TSize.y) ).x;
    sample[9] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 2.0f * TSize.x, -1.0f * TSize.y) ).x;
											 
    sample[10] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-2.0f * TSize.x,  0.0f * TSize.y) ).x;
    sample[11] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-1.0f * TSize.x,  0.0f * TSize.y) ).x;
    sample[12] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 0.0f * TSize.x,  0.0f * TSize.y) ).x;
    sample[13] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 1.0f * TSize.x,  0.0f * TSize.y) ).x;
    sample[14] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 2.0f * TSize.x,  0.0f * TSize.y) ).x;

    sample[15] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-2.0f * TSize.x,  1.0f * TSize.y) ).x;
    sample[16] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-1.0f * TSize.x,  1.0f * TSize.y) ).x;
    sample[17] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 0.0f * TSize.x,  1.0f * TSize.y) ).x;
    sample[18] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 1.0f * TSize.x,  1.0f * TSize.y) ).x;
    sample[19] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 2.0f * TSize.x,  1.0f * TSize.y) ).x;

    sample[20] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-2.0f * TSize.x,  2.0f * TSize.y) ).x;
    sample[21] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2(-1.0f * TSize.x,  2.0f * TSize.y) ).x;
    sample[22] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 0.0f * TSize.x,  2.0f * TSize.y) ).x;
    sample[23] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 1.0f * TSize.x,  2.0f * TSize.y) ).x;
    sample[24] = tex2D( DepthTextureSampler0, IN.TexCoord[0].xy + float2( 2.0f * TSize.x,  2.0f * TSize.y) ).x;
    
    float accum0, accum1, accum2, accum3, accum4, accum5, accum6, accum7, accum8 , accum9;
    
    const float c = (1.f/float(5.0f));    
    
    //  0 -  1 -  2 -  3 -  4
    //  5 -  6 -  7 -  8 -  9
    // 10 - 11 - 12 - 13 - 14
    // 15 - 16 - 17 - 18 - 19
    // 20 - 21 - 22 - 23 - 24
    
    // in x direction
    accum0 = log_conv( c, sample[0], c, sample[1] );
    accum0 = log_conv( 1.f, accum0,   c, sample[2] );
    accum0 = log_conv( 1.f, accum0,   c, sample[3] );
    accum0 = log_conv( 1.f, accum0,   c, sample[4] );
    
    accum1 = log_conv( c, sample[5], c, sample[6] );
    accum1 = log_conv( 1.f, accum1,   c, sample[7] );
    accum1 = log_conv( 1.f, accum1,   c, sample[8] );
    accum1 = log_conv( 1.f, accum1,   c, sample[9] );

    accum2 = log_conv( c, sample[10], c, sample[11] );
    accum2 = log_conv( 1.f, accum2,   c, sample[12] );
    accum2 = log_conv( 1.f, accum2,   c, sample[13] );
    accum2 = log_conv( 1.f, accum2,   c, sample[14] );
    
    accum3 = log_conv( c, sample[15], c, sample[16] );
    accum3 = log_conv( 1.f, accum3,   c, sample[17] );
    accum3 = log_conv( 1.f, accum3,   c, sample[18] );
    accum3 = log_conv( 1.f, accum3,   c, sample[19] );

    accum4 = log_conv( c, sample[20], c, sample[21] );
    accum4 = log_conv( 1.f, accum4,   c, sample[22] );
    accum4 = log_conv( 1.f, accum4,   c, sample[23] );
    accum4 = log_conv( 1.f, accum4,   c, sample[24] );

    //  0 -  1 -  2 -  3 -  4
    //  5 -  6 -  7 -  8 -  9
    // 10 - 11 - 12 - 13 - 14
    // 15 - 16 - 17 - 18 - 19
    // 20 - 21 - 22 - 23 - 24

	// now in y direction
    accum5 = log_conv( c, sample[0], c, sample[5] );
    accum5 = log_conv( 1.f, accum5,   c, sample[10] );
    accum5 = log_conv( 1.f, accum5,   c, sample[15] );
    accum5 = log_conv( 1.f, accum5,   c, sample[20] );
    
    accum6 = log_conv( c, sample[1], c, sample[6] );
    accum6 = log_conv( 1.f, accum6,   c, sample[11] );
    accum6 = log_conv( 1.f, accum6,   c, sample[16] );
    accum6 = log_conv( 1.f, accum6,   c, sample[21] );

    accum7 = log_conv( c, sample[2], c, sample[7] );
    accum7 = log_conv( 1.f, accum7,   c, sample[12] );
    accum7 = log_conv( 1.f, accum7,   c, sample[17] );
    accum7 = log_conv( 1.f, accum7,   c, sample[22] );
    
    accum8 = log_conv( c, sample[3], c, sample[8] );
    accum8 = log_conv( 1.f, accum8,   c, sample[13] );
    accum8 = log_conv( 1.f, accum8,   c, sample[18] );
    accum8 = log_conv( 1.f, accum8,   c, sample[23] );

    accum9 = log_conv( c, sample[4], c, sample[9] );
    accum9 = log_conv( 1.f, accum9,   c, sample[14] );
    accum9 = log_conv( 1.f, accum9,   c, sample[19] );
    accum9 = log_conv( 1.f, accum9,   c, sample[24] );
    
	// now combine both directions
    accum0 = log_conv( c, accum0, c, accum1 );
    accum0 = log_conv( 1.f, accum0, c, accum2 );
    accum0 = log_conv( 1.f, accum0, c, accum3 );
    accum0 = log_conv( 1.f, accum0, c, accum4 );

    accum1 = log_conv( c, accum5, c, accum6 );
    accum1 = log_conv( 1.f, accum1, c, accum7 );
    accum1 = log_conv( 1.f, accum1, c, accum8 );
    accum1 = log_conv( 1.f, accum1, c, accum9 );
    
    
    return log_conv( 0.5f, accum0, 0.5f, accum1 );
}

//------------------------------------------------------------------------------
// generic separable box filter ... 7 taps
//------------------------------------------------------------------------------

float4 LogBox1D_NTaps_PS( VSOutput input): COLOR						                    
{
	const int taps = 7;
    float  sample[8] = {0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f};
    for (int i = 0; i < taps; i++)
    {
        sample[i] = tex2D( DepthTextureSampler0, input.TexCoord[i] ).x;
    }
 
    const float c = (1.f/float(taps));    
    
    float accum;
    accum = log_conv( c, sample[0], c, sample[1] );
    for (int i = 2; i < taps; i++)
    {
        accum = log_conv( 1.f, accum, c, sample[i] );
    }    
        
    return accum;
}

#endif

float4 PS_ShadowBlur(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	float2 TexC = IN.texCoord0.xy; 
	
	float4 depth = 0;
	
// 	depth  = _H4Tex2DOffset(DepthTextureSampler0, TexC, float2(-2.0f,  2.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2( 0.0f,  2.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2( 2.0f,  2.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2(-2.0f,  0.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2( 0.0f,  0.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2( 2.0f,  0.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2(-2.0,  -2.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2( 0.0,  -2.0f));
// 	depth += _H4Tex2DOffset(DepthTextureSampler0, TexC, float2( 2.0f, -2.0f));
// 
// 	return depth / 9.0f;
#if __PS3
	float2 Pack = 0;
	Pack.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(0.0f, 0.0f)).x;
	return (unpack_4ubyte(pack_2half(Pack)));
#elif __XENON

	// upper left corner
	depth = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-3.0f,  -3.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,  -3.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-3.0f,  -1.0f));
	
	// this is the center
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,  -1.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,  -1.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,   1.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,   1.0f));

	// upper right corner
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,  -3.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,  -3.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,  -1.0f));
	
	// lower right corner
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,   1.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 3.0f,   3.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 1.0f,   3.0f));
	
	// lower left corner
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-3.0f,   1.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-3.0f,   3.0f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-1.0f,   3.0f));
	
	return depth / 16.0f;
#elif __WIN32PC
    return 1.0f;	
	
#endif	
}

float4 PS_ShadowSmallBlur(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	float2 TexC = IN.texCoord0.xy; 
#if __PS3
	float2 Pack = 0;
	Pack.x = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(0.0f, 0.0f)).x;
	return (unpack_4ubyte(pack_2half(Pack)));
#elif __XENON	
	float4 depth = 0;

	depth = _Tex2DOffset(DepthTextureSampler0, TexC,  float2(-0.5f,  -0.5f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 0.5f,  -0.5f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2(-0.5f,   0.5f));
	depth += _Tex2DOffset(DepthTextureSampler0, TexC, float2( 0.5f,   0.5f));
	
	return depth / 4.0f;
	
#elif __WIN32PC
    return 1.0f;	
#endif
}

// -------------------------------------------------------------
// PSGaussX5
// Note: Like PSGaussX, but less blur and less taps.
// -------------------------------------------------------------
half4 PSGaussX5(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    half4 Color = 0.0f;
    
    // this is a 360.0f friendly Gauss-kind-of filter
    Color  = h4tex2D( DepthTextureSampler0, IN.texCoord0.xy) * 0.334f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2( -2.0f,  0.0f )) * 0.133f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2( -1.0f,  0.0f )) * 0.2f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2(  1.0f,  0.0f )) * 0.2f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2(  2.0f,  0.0f )) * 0.133f;

    return Color;
}


// -------------------------------------------------------------
// PSGaussY5
// Note: Like PSGaussY, but less blur and less taps.
// -------------------------------------------------------------
half4 PSGaussY5(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    half4 Color = 0.0f;
    
    // this is a 360.0f friendly Gauss-kind-of filter
    Color  = h4tex2D( DepthTextureSampler0, IN.texCoord0.xy) * 0.334f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2( 0.0f,  -2.0f )) * 0.133f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2( 0.0f,  -1.0f )) * 0.2f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2( 0.0f,  1.0f )) * 0.2f;
    Color += _H4Tex2DOffset( DepthTextureSampler0, IN.texCoord0.xy, half2( 0.0f,  2.0f )) * 0.133f;

    return Color;
}

technique drawskinned
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_ShadowCompositeSkin();
        PixelShader  = compile PIXELSHADER PS_ShadowCompositeLighting();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS_ShadowComposite();
        PixelShader  = compile PIXELSHADER PS_ShadowCompositeLighting();
    }
}


technique Blur
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PS_ShadowBlur();
    }
}

technique SmallBlur
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PS_ShadowSmallBlur();
    }
}



technique BlurGaussX5
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussX5();
    }
}


technique BlurGaussY5
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussY5();
    }
}

technique BlurLogX
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER BlurLogX1();
    }
}


technique BlurLogY
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER BlurLogY1();
    }
}

technique BlurLog2X
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER BlurLog2X1();
    }
}


technique BlurLog2Y
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER BlurLog2Y1();
    }
}

#if __XENON

technique Box
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VSBlur();
        PixelShader  = compile PIXELSHADER LogBox1D_5Taps_PS();
    }
}
technique BoxXY9tap
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VSBlurXY9tap();
        PixelShader  = compile PIXELSHADER LogBox2D_9Taps_PS();
    }
}

technique BoxXY
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;		
		ZEnable = true;
		ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VSBlurXY25tap();
        PixelShader  = compile PIXELSHADER LogBox2D_16Taps_PS();
    }
}


#endif
