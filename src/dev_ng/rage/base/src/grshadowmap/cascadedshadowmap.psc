<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef constructable="false" type="rage::CShadowMap">
<array name="m_Tiles" type="atArray">
<pointer policy="owner" type="rage::CShadowMapTile"/>
</array>
<float name="m_FadeOutSMap"/>
<bool name="m_ShowShadowMap"/>
</structdef>

<structdef type="rage::CShadowMapTile">
<int name="m_WhichTile"/>
<float name="m_NearSlice"/>
<float name="m_FarSlice"/>
<float name="m_LightDepthScale"/>
<float name="m_DepthBias"/>
<float name="m_BiasSlope"/>
<float name="m_ViewportZShift"/>
<float name="m_FovYDiv"/>
</structdef>

</ParserSchema>