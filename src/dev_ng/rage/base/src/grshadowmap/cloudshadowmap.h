#ifndef _CLOUDSHADOWMAP_H_
#define _CLOUDSHADOWMAP_H_

#include "data/base.h"
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "vector/color32.h"
#include "grcore/viewport.h"


namespace rage
{

// This structure contains settings for cloud shadows.  
struct CloudShadowParms : public datBase
{
public:
	float		CloudPatternScale;
	Vector2		CloudPatternSpeed;
	float		CloudShadowStrength;

#if __BANK
	void AddWidgets(bkBank &bank);
#endif

	CloudShadowParms();
	CloudShadowParms& operator*=(float mag);
	CloudShadowParms& operator+=(const CloudShadowParms& other);
};
CloudShadowParms operator+(const CloudShadowParms& a, const CloudShadowParms& b);
CloudShadowParms operator-(const CloudShadowParms& a, const CloudShadowParms& b);
CloudShadowParms operator*(const CloudShadowParms& a, const float mag);
CloudShadowParms operator*(const float mag, const CloudShadowParms& b);


class CloudShadowMap : public datBase
{
public:

	enum 
	{
		XBOX360_CLOUDSHADOWMAP_SIZE				= 256,
		PS3_CLOUDSHADOWMAP_SIZE					= 256
	};

	// Constructor/Destructor
	CloudShadowMap();
	~CloudShadowMap();

	// Creates and loads the shaders
	void InitShaders(const char* path = NULL); 

	// Allocates all render targets associated with cloud shadow maps
	void CreateRenderTargets();

	// Frees resources that were occupied by the CreateRenderTargets() call
	void DeleteRenderTargets();

	// Draws the cloud shadow casters
	void RenderIntoShadowMap();

	// Renders into a screenspace 2D texture the results of the depth comparison
	void RenderShadowsIntoShadowCollector(bool paused);

	// Forces these external settings
	void ForceSettings(CloudShadowParms* parms);

#if __BANK
	// Widgets
	void AddWidgets(bkBank& bank);
#endif

	// Static members to manage the instance
	static CloudShadowMap* GetInstance() {return sm_instance;}
	static void SetBaseTexture(grcRenderTarget* newTexture);

protected:

	// Helper functions
	void BlitToRT(
		grcRenderTarget* target,		// Dest
		grcTexture* texture,			// Source texture
		grcEffectTechnique technique,	// Technique to use
		grcEffectVar textureId = grcevNONE,	// Sampler to rig source texture to (see your technique)
		const Color32* color = NULL,	// vertex color (NULL will use white)
		float x1 = -1.0f,				// x1 - Destination base x
		float y1 = 1.0f,				// y1 - Destination base y
		float x2 = 1.0f,				// x2 - Destination opposite-corner x
		float y2 = -1.0f				// y2 - Destination opposite-corner y
		);

	void GetDepthBuffer();
	void ReloadTextures();

	// Main shadow map render target for 360 and PS3
	grcRenderTarget* m_CloudShadowRT;
	grcRenderTarget* m_DepthBufferRT;
	grcRenderTarget* m_DepthBuffer;		// 360 can use the depth buffer as a texture

	// Our lovely shader
	grmShader *m_CloudShadowShader;

	// Various techniques
	grcEffectTechnique m_externalCopyTech;
	grcEffectTechnique m_blurTech;
	grcEffectTechnique m_drawIntoCollectorTech;

	// Shader variables
	grcEffectVar m_CloudSamplerId;
	grcEffectVar m_DepthBufferId;
	grcEffectVar m_ProjInverseTransposeId;		// handle for InverseProjection matrix to go from screen back to world space.
	grcEffectVar m_CloudPatternScaleId;
	grcEffectVar m_NearFarClipPlaneQId;			// handle for the near and far clipping plane and the Q parameter for the depth of field calculation
	grcEffectVar m_ScrollXZId;
	grcEffectVar m_TexelSizeId;
	grcEffectVar m_CloudShadowStrengthId;

	// Textures
	grcTexture* m_cloudTexture;

	// Variables
	CloudShadowParms m_Settings;
	Vector2		m_CloudScrollXZ;

	// Statics
	static CloudShadowMap* sm_instance;
	static grcRenderTarget* sm_ExternalCloudShadowRT;
	static char* sm_AssetPath;

};

} // namespace rage


#define CLOUDSHADOWMAP (rage::CloudShadowMap::GetInstance())

#endif
