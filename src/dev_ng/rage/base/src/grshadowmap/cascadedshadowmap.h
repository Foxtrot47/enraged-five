// 
// grshadowmap/cascadedshadowmap.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifndef GRSHADOWMAP__CASCADEDSHADOWMAP_H
#define GRSHADOWMAP__CASCADEDSHADOWMAP_H

#include "atl/array.h"
#include "atl/ownedptr.h"
#include "bank/bkmgr.h"
#include "data/base.h"
#include "grcore/effect.h"
#include "parser/manager.h"
#include "vector/vector3.h"
#include "vector/matrix44.h"
#include "grcore/viewport.h"
#include "grcore/gprallocation.h"
#include "spatialdata/sphere.h"
#include "spatialdata/plane.h"
#include "spatialdata/polygon.h"


#include "shadowcollector.h"

#include "shaderlib/rage_shadow_config.h"

namespace rage
{
	class bkBank;
	class grcRenderTarget;
	class grcViewport;
	class grmShader;
	class grmShaderVar;
	class Color32;
	class CShadowMapTile;
	class grcGPRAllocation;

	enum 
	{
		PC_SHADOWMAP_SIZEX						= 640,
		PC_SHADOWMAP_SIZEY						= 640,

		XENON_SHADOWMAP_SIZEX					= 800,
		XENON_SHADOWMAP_SIZEY					= 800,

		PS3_SHADOWMAP_SIZEX						= 640,
		PS3_SHADOWMAP_SIZEY						= 640
	};


	// number of mip-map levels for shadow map
#define SHADOW_MAP_LEVELS 8

	class CShadowMapTile
	{
	public:
		enum {MAX_TILES=8};

		static const char* sm_NumberNames[MAX_TILES];

		CShadowMapTile();

		CShadowMapTile(int whichTile);

		void SetDefaults(int whichTile);

		const char* GetNumberName() const;

		// get near and far slice value
		float GetNearSlice() const {return m_NearSlice;}
		float GetFarSlice() const {return m_FarSlice;}

		// set near and far slice value
		void SetNearSlice(float Near) {m_NearSlice = Near;}
		void SetFarSlice(float Far) {m_FarSlice = Far;}

#if __BANK
		void AddWidgets(bkBank& bank);
#endif
		int					m_WhichTile;

		Matrix44			m_ViewProj;
		float				m_NearSlice;
		float				m_FarSlice;
		float				m_LightDepthScale;
		float				m_DepthBias;
		float				m_CameraOffset;
		float				m_ViewportZShift;
		float				m_BiasSlope;
		Vector4				m_SphereAroundFrustumSlice;

#if __DEV
		// Debug sphere
		spdSphere	sphere;

		// Debug a polygon
		Vector3		DebugVectors[4];

		Matrix34	SphereMatrix;

		Vector3		MinMax[2];
#endif

		const spdShaft &GetShaft() const {return m_Shaft;}

		spdShaft	m_Shaft;

		// these are only valid if they are not the first tile:
		// 		grcEffectGlobalVar	m_FadeInSMapID;
		// 		float				m_FadeInSMap;
		float				m_FovYDiv;

		PAR_SIMPLE_PARSABLE;
	};

	//
	// upper left corner of face in texture atlas
	//
	typedef struct OffsetTextureAtlas
	{
		float OffsetWidthUpperLeftCorner;	
		float OffsetHeightUpperLeftCorner;	
	} s_OffsetTextureAtlas;

	// this is the shadow map atlas
	class CCharacterTextureAtlasOffsets
	{
	public:

		int WidthMulti;
		int HeightMulti;

		s_OffsetTextureAtlas m_OffsetTextureAtlas;
	};

	class CShadowMap : public datBase
	{
	public:
		//
		// PURPOSE
		//	constructor
		// PARAMS
		//	numTiles - # of tiles in the cascaded tile map.  The maximum # is 4.
		//
		CShadowMap(int numTiles, bool bLoadShadowData = false);

		// destructor
		virtual	~CShadowMap(void);

		//
		// PURPOSE
		//	creates and loads the shaders
		// PARAMS
		//	path - the path to load the shaders from.  If it == NULL, use the currently set path.
		//
		void InitShaders(const char *path=NULL); 

		// 
		// PURPOSE
		//	allocates the shadow map render targets
		// PARAMS
		//	createBlurTarget - automatically create the blur target (if false, it's up to the user to call CreateBlurRenderTarget())
		// NONE
		// NOTES
		//	if the device changes or the user wants a different resolution: 
		// delete and create new shadow map
		void CreateRenderTargets();

		//
		// PURPOSE
		//	frees resources that were occupied by the CreateRenderTargets() call
		// RETURNS
		//	none
		void DeleteRenderTargets(void);

		// PURPOSE
		//	get the number of shadow tiles
		// RETURNS
		//	an integer containing the number of shadow tiles
		int GetTileCount() const { return m_Tiles.GetCount(); }

		//
		// PURPOSE
		//	renders into all shadowmap tiles.
		// PARAMS
		//	clearBuffers - if true, this function clears the back buffer
		//  and the z buffer.  If false, you should call clear yourself
		//  (otherwise you'll get corruption from the rendered shadowmap
		//  because it uses the same area of memory as the backbuffer or 
		//	z-buffer.)
		void RenderIntoShadowMaps(bool clearBuffers=true);
		void RenderIntoShadowMaps(bool clearBuffers, Vector3& lightDir);

		// PURPOSE
		//	shows a preview of the ortho viewport used to render into the shadow map
		// RETURNS
		//	nothing
		void DebugOrthoViewport(bool bOn);

		// PURPOSE
		//	begin shadow rendering of the specified tile
		// PARAMS
		//	whichTile - the zero based index of the tile to render
		//	clearBuffers - if true, this function clears the back buffer
		//  and the z buffer.  If false, you should call clear yourself
		//  (otherwise you'll get corruption from the rendered shadowmap
		//  because it uses the same area of memory as the backbuffer or 
		//	z-buffer.)
		void BeginTileAroundSphere(int whichTile, bool /* clear Buffers */);


		void SetupLightMatrix(Matrix34& CameraMatrix, Matrix34& lightMatrix);
		void SetupLightMatrix(Matrix34& CameraMatrix, Matrix34& lightMatrix, Vector3& lightDir);

		void GetOrthoViewport(grcViewport& inVP, grcViewport& outVP, int whichTile, const Matrix34& camMatrix, Vector3::Vector3Param lightDir);


		//
		void CalcAndSetOrthoViewportAroundSphere(int whichTile, const Matrix34& camMatrix, Vector3::Vector3Param LightDirection);


		// PURPOSE
		//	end shadow rendering of the specified tile
		// PARAMS
		//	whichTile - the zero based index of the tile to render
		//	clearBuffers - if true, this function clears the back buffer
		//  and the z buffer.  If false, you should call clear yourself
		//  (otherwise you'll get corruption from the rendered shadowmap
		//  because it uses the same area of memory as the backbuffer or 
		//	z-buffer.)
		//  
		//	maxExtraBlurTile - passed to BlurShadowMap() for the last tile, 
		//  if doing high quality shadows
		void EndTile(int whichTile = 0);

		//
		// PURPOSE
		//	renders into the shadowmap (calls overloaded RenderShadowCasters())
		//  to render objects into shadowmap.  
		// PARAMS
		//	whichTile - which tile to render
		//	clearBuffers - if true, this function clears the back buffer
		//  and the z buffer.  If false, you should call clear yourself
		//  (otherwise you'll get corruption from the rendered shadowmap
		//  because it uses the same area of memory as the backbuffer or 
		//	z-buffer.)
		void RenderIntoShadowMap();

		//
		// PURPOSE
		//	renders into a screenspace 2D texture the results of the depth comparison
		// PARAMS
		// NONE
		void RenderShadowsIntoShadowCollector();

		//
		// PURPOSE
		//	begins rendering into the 2D screenspace texture
		//  wraps the BeginRenderTileIntoShadowCollector() call
		// PARAMS
		//  useMapsWithoutCollector - if true means that you are not using a full screen collector
		void BeginRenderShadowsIntoShadowCollector( bool useMapsWithoutCollector = false, bool skipClear=false );

		//
		// PURPOSE
		//	ends rendering into the 2D screenspace texture
		//  wraps the EndRenderTileIntoShadowCollector() call
		// PARAMS
		//  expects the tile number == shadow map number that needs to be rendered
		void EndRenderShadowsIntoShadowCollector();

		//
		// PURPOSE
		//	calls the virtual function to draw shadow casters
		// PARAMS
		//  NONE
		void RenderTilesIntoShadowCollector();

		//
		// PURPOSE
		//	renders all shadow casters. This is very engine specific and needs to be adjusted depending on the underlying game.
		// PARAMS
		//	viewport for viewport culling == sphere
#if __XENON
		virtual void RenderShadowCasters(grcViewport &vp, bool /*isCasting */) {vp.SetNormWindow(0, 0, XENON_SHADOWMAP_SIZEX, XENON_SHADOWMAP_SIZEY);};
#elif __PPU
		virtual void RenderShadowCasters(grcViewport &vp, bool  /*isCasting */) {vp.SetNormWindow(0, 0, PS3_SHADOWMAP_SIZEX, PS3_SHADOWMAP_SIZEY);};
#endif


		template <typename _Type> bool LoadTunables(const char* filename)
		{
			return PARSER.LoadObject(filename,"xml",reinterpret_cast<_Type&>(*this));
		}

#if __DEV
		template <typename _Type> bool SaveTunables(const char* filename)
		{
			return PARSER.SaveObject(filename,"xml",reinterpret_cast<const _Type*>(this),parManager::XML);
		}
#endif

		//
		// PURPOSE
		//	show shadow render target
		// PARAMS
		//	NONE
		void ShowShadowRenderTargets();

		// PURPOSE
		//	get a pointer to the shadow depth shader
		// RETURNS
		//	a pointer to a grmShader object describing the depth shader
		grmShader* GetDepthShader() const	{ return m_DepthShader; }

		// PURPOSE
		//	get a pointer to the shadow blend shader
		// RETURNS
		//	a pointer to a grmShader object describing the blend shader
		grmShader* GetCollectorShader() const	{ return m_RenderIntoCollectorShader; }


		// get shadow map viewport
		grcViewport* GetShadowViewports(int i) 
		{ 
			// just in case the viewport is not intialized ... or shadows off
			if(m_Viewports[i] != NULL)
				return m_Viewports[i]; 
			else
				return NULL;
		}

		// get a pointer to a single tile == shadow map
		CShadowMapTile* GetTile(int i)	
		{ 
			return m_Tiles[i];
		}

#if __BANK
		virtual void AddSaveWidgets(bkBank& bk);
		virtual void AddWidgets(bkBank &bk);
#endif

#if __DEV
		template <typename _Type> void SaveTunablesCB()
		{
			char fileName[256];
			strcpy(fileName,"shadowmap");
			if (BANKMGR.OpenFile(fileName,256,"*.xml", true, "Cascaded ShadowMap Tunables (*.xml)"))
			{
				SaveTunables<_Type>(fileName);
			}
		}

		template <typename _Type> void LoadTunablesCB()
		{
			char fileName[256];
			strcpy(fileName,"shadowmap");
			if (BANKMGR.OpenFile(fileName,256,"*.xml", false, "Cascaded ShadowMap Tunables (*.xml)"))
			{
				LoadTunables<_Type>(fileName);
			}
		}

		void SetFirstShaftDebugIndex(int index)			{ m_FirstShaftDebugIndex = index; }
#else
		void SetFirstShaftDebugIndex(int /*index*/)		{};
#endif // __DEV

		// we need to set these parameters for command buffers explicitely ... it seems like they do not update correctly otherwise
		float m_TempDepthBias;

		// get/set the depth bias value for shadow map rendering
		float GetDepthBias() const { return m_TempDepthBias; }
		void SetDepthBias(float f) { m_TempDepthBias = f; }

		float m_TempDepthBiasSlope;

		// get/set the depth bias slope value for shadow map rendering
		float GetDepthSlopeBias() const { return m_TempDepthBiasSlope; }
		void SetDepthSlopeBias(float f) { m_TempDepthBiasSlope = f; }

#if __PPU
		void ClearPS3ShadowMap();
#endif

		// static functions
		// keep track of the instance
		static void Init(int numTiles, bool bLoadShadowData);
		static void Terminate();
		static CShadowMap *GetInstance() {return sm_instance;}

		static CShadowMap *sm_instance;			// instance of post processing effects


	private:

		//---------------------------------------------------------------------------------
		// private functions
		//---------------------------------------------------------------------------------

		// PURPOSE
		//	
		// RETURNS
		//	
		// calculate a sphere around the view frustum slice
		void CalculateSphereAroundFrustumSlice(CShadowMapTile& tile, 
			grcViewport& inVP, 
			const Matrix34& camMatrix, 
			Vector3 extents[], 
			const float m_FovYDiv,
			Vector3& nearCenter,
			Vector3& farCenter);

		// PURPOSE
		//	calculates a light viewport based on the viewer viewport like this
		// RETURNS
		//	a light viewport
		void CalcOrthoViewportAroundSphere(CShadowMapTile& tile, 
			grcViewport& inVP, 
			grcViewport& outVP, 
			const Matrix34& camMatrix, Vector3::Vector3Param LightDir);

		// PURPOSE
		//	provides a shadow map preview
		// RETURNS
		//	nothing
		void ShowShadowMap(grcRenderTarget* m_DepthBuffer, 
						grcEffectTechnique technique,					// technique in FX file
						float DestX, 
						float DestY, 
						float SizeX, 
						float SizeY);	

		// array of viewports
		atArray<grcViewport*> m_Viewports;

		// this holds the number of shadow map tiles == shadow maps per viewport slice
		atArray<atOwnedPtr<CShadowMapTile> > m_Tiles;

		// The shader used for rendering to depth targets
		grmShader * m_DepthShader;				

		// the shader used for render into the color buffer and alpha blend the shadows
		grmShader * m_RenderIntoCollectorShader;				

		// shader parameter handles
		grcEffectGlobalVar m_TexelSizeID;


#if __PPU || __WIN32PC
		// handle for clear color
		grcEffectVar m_ColorClearId;
#endif

#if USE_GLOBAL_SHADOWLIGHTMTX
		grcEffectGlobalVar	m_FadeOutSMapID;
#else
		grcEffectVar		m_FadeOutSMapID;
#endif

		grcEffectGlobalVar	m_CameraMatrixID;
		grcEffectGlobalVar	m_DepthTextureID;

#if USE_GLOBAL_SHADOWLIGHTMTX
		grcEffectGlobalVar m_LightMatrixArrID;
		grcEffectGlobalVar m_BoundsID;
		// 		grcEffectGlobalVar m_TopPlanesID;
		// 		grcEffectGlobalVar m_RightPlanesID;
		// 		grcEffectGlobalVar m_LeftPlanesID;
		//		grcEffectGlobalVar m_BlurTextureID;
		// 		grcEffectGlobalVar m_SpheresID;
#else
		grcEffectVar	m_LightMatrixArrID;
		grcEffectVar	m_BoundsID;
		// 		grcEffectVar	m_TopPlanesID;
		// 		grcEffectVar	m_RightPlanesID;
		// 		grcEffectVar	m_LeftPlanesID;
		//		grcEffectVar	m_BlurTextureID;
		// 		grcEffectVar	m_SpheresID;
#endif
		//		grcEffectVar	m_SpheresID;
		grcEffectGlobalVar m_SpheresID;


		// fade out value for last shadow map
		float	m_FadeOutSMap;

		Vector3 m_lightDir;
		Vector3 m_lightPos;

		// light matrix
		// holds light direction in c
		// holds light position in d
		Matrix34 m_LightMatrix;
		Matrix34 m_CameraMatrix;

		// planes for distance measurements in the shader
		// 		Vector4 m_TopPlanes[3];
		// 		Vector4 m_RightPlanes[3];
		// 		Vector4 m_LeftPlanes[3];

		Vector4 m_ShadowSpheres[4];

#if __XENON
		unsigned int m_XenonShadowmapSizeX;
		unsigned int m_XenonShadowmapSizeY;
		unsigned int m_XenonNoOfTextureAtlasFaces;
#elif __PPU
		unsigned int m_PS3ShadowmapSizeX;
		unsigned int m_PS3ShadowmapSizeY;
		unsigned int m_PS3NoOfTextureAtlasFaces;
#elif __WIN32PC
		unsigned int m_PCShadowmapSizeX;
		unsigned int m_PCShadowmapSizeY;
#endif

		// main shadow map render target for 360 and PS3
		grcRenderTarget* m_DepthBuffer;

		// technique that is used to view the depth map
		grcEffectTechnique m_CopyTechnique;	

#if __XENON
		bool m_bUsesHierZ;
#endif

		// toggle shadow map preview
		bool m_ShowShadowMap;

		// toggle black & white map preview
		bool m_bShadowCollector;
		int m_iShadowCollectorChannel;

		// temp pointer to store original viewport
		const grcViewport*	m_OldVP;

#if __XENON
		// stores the number of pixel threads currently used by the 360
		grcResolveFlags clearParams;
#endif

#if __DEV
		int				m_FirstShaftDebugIndex;	// This is the index of the first shadow shaft in the shaft debugger
#endif // __DEV

		PAR_PARSABLE;
	};

}	// name space range

#define	CASCADEDSHADOWMAP	CShadowMap::GetInstance()

#endif
