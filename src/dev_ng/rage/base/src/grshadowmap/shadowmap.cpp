// 
// shadowmap/shadowmap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Cascaded Shadow maps
//
// Note: cool stuff :-)
//
#include "shadowmap.h"
#include "grprofile/pix.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/light.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "system/xtl.h"
#include "grcore/device.h"

namespace rage {


CShadowMapTile::CShadowMapTile()
{
	SetDefaults(-1);
}

CShadowMapTile::CShadowMapTile(int whichTile)
	
{
	SetDefaults(whichTile);
}	

void CShadowMapTile::SetDefaults(int whichTile)
{
	m_WhichTile=whichTile;
	const float TILE_INCREMENT=10.0f;

	Assert(whichTile<MAX_TILES);
	//m_NearPlane			= m_WhichTile==0?0.0f:(TILE_INCREMENT*whichTile);
	m_NearPlane			= 0.0f;
	m_FarPlane			= TILE_INCREMENT+(TILE_INCREMENT*whichTile);

	m_DepthTextureID	= grcegvNONE;
	m_LightMatrixID		= grcegvNONE;

	m_FadeStartID		= grcegvNONE;
	m_FadeInSMapID		= grcegvNONE;

	m_DepthBias			= 0.0000f;
	m_BiasSlope			= -2.8f;

	m_FadeStart			= (TILE_INCREMENT-1)+(TILE_INCREMENT*(m_WhichTile-1)) + 1;
	//m_FadeOutSMap		= TILE_INCREMENT*m_WhichTile;
	//m_FadeInSMap		= 0.250f*m_WhichTile;
	m_FovYDiv			= 0.6f;


	m_ViewProj.Identity();
}

const char* CShadowMapTile::GetNumberName() const
{
	return sm_NumberNames[m_WhichTile];
}

void CShadowMapTile::GetGlobalShaderVars()
{
	const int VAR_NAME_LENGTH=256;
	char varName[VAR_NAME_LENGTH];

	// depth texture
	m_DepthTextureID = grcEffect::LookupGlobalVar(formatf(varName,VAR_NAME_LENGTH,"DepthTexture%d",m_WhichTile));

	// light camera projection matrix
	m_LightMatrixID = grcEffect::LookupGlobalVar(formatf(varName,VAR_NAME_LENGTH,"Shadowmap_LightCompositMatrix%d",m_WhichTile));

	// only available for tiles other than the first one:
	if (m_WhichTile>0)
	{
		// starting point of shadow map in the view frustum ... actually this slices the view frustum
		m_FadeStartID = grcEffect::LookupGlobalVar(formatf(varName,VAR_NAME_LENGTH,"%sShadowMapStart",sm_NumberNames[m_WhichTile]));
	}
}

void CShadowMapTile::SetGlobalShaderVars(float ViewportZShift)
{
#if !__XENON
	if(m_DepthTextureID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_DepthTextureID, m_DepthMap);
#else
	if(m_DepthTextureID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_DepthTextureID, m_DepthBuffer);
#endif
	// set this here for now
	if(m_LightMatrixID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_LightMatrixID, m_ViewProj);

	if (m_WhichTile>0)
	{
		// point where shadow map should start to show up based on camera distance
		if(m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_FadeStartID, m_FadeStart - ViewportZShift);
	}
}

#if __BANK
void CShadowMapTile::AddWidgets(bkBank& bank)
{
	bank.AddSlider("Depth Bias", &m_DepthBias, -1.0f, 1.0f, 0.0001f);
	bank.AddSlider("Depth Bias Slope", &m_BiasSlope, -100.0f, 100.0f, 0.1f);

	bank.AddSlider("light frustum near", &m_NearPlane, 0.0f, 100.0f, 1.0f);
	bank.AddSlider("light frustum far", &m_FarPlane, 1.0000f, 10000.0f, 1.0f);

	if (m_WhichTile>0)
		bank.AddSlider("Shadow Map Starting point", &m_FadeStart, 0.0f, 500.0f, 1.0f);
	bank.AddSlider("+/- light view frust", &m_FovYDiv, 0.0f, 500.0f, 1.0f);
}
#endif

};

// this must be here so that it finds rage::CShadowMapTile:
#include "shadowmap_parser.h"


namespace rage {
// if this triggers, add the required tile names to sm_NumberNames:
CompileTimeAssert(CShadowMapTile::MAX_TILES==8);

const char* CShadowMapTile::sm_NumberNames[MAX_TILES] = 
{
	"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eight"
};


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
CShadowMap::CShadowMap(int numTiles) :
#if __XENON
	m_PredicatedTiled(false),
	m_XenonShadowmapSize(XENON_SHADOWMAP_SIZE),
#else
	m_DepthBuffer(NULL),
#endif
	m_DepthBufferPreview(NULL),
	m_BlackNWhite(NULL),
	m_ShowShadowMap(false),
	m_BlackNWhiteMap(false)
{
	m_ShadowColor.Set(0.1f, 0.1f, 0.1f, 1.0f);
	m_FadeOutSMap = 120.0f;

//	m_RandomTextureID	= grcegvNONE;
	m_ShadowColorID		= grcegvNONE;
	m_PixelBlurID		= grcegvNONE;

#if __XENON
	// sets distribution of hardware shader units between the vertex and pixel shader to default
	m_CurrentPixelThreads = -1;
#endif

//	m_RandomTexture = NULL;

	// initialize tiles:
	Assert(numTiles>0);
	Assert(numTiles<=CShadowMapTile::MAX_TILES);
	m_Tiles.Resize(numTiles);

	m_Viewports.Resize(numTiles);

	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		m_Tiles[i]=rage_new CShadowMapTile(i);
	}

	for (int i=0;i<m_Viewports.GetCount();i++)
	{
		m_Viewports[i]= rage_new grcViewport;
	}

	if(!parManagerSingleton::IsInstantiated())
	{
		INIT_PARSER;
	}

	// register parsable classes:
	REGISTER_PARSABLE_CLASS(CShadowMapTile);
	REGISTER_PARSABLE_CLASS(CShadowMap);
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::InitShaders(const char *path)
{	
	if (path)
		ASSET.PushFolder(path);

	// 360 - fast blit into depth buffer. The depth buffer is then used as a shadow map
	// PC - a R32F shadow map is used to store the depth values
	const char *shaderName = __XENON?"rage_shadowdepth_360":"rage_shadowdepth";
	m_DepthShader = grmShaderFactory::GetInstance().Create(shaderName);
	m_DepthShader->Load(shaderName, 0 ,0);

	// blend shadow maps into color buffer
	shaderName = "rage_blendshadows";
	m_BlendShader = grmShaderFactory::GetInstance().Create(shaderName);
	m_BlendShader->Load(shaderName, 0 ,0);

	if (path)
		ASSET.PopFolder();

	m_ViewportZShift = 4.5f;

	// texel size
	m_PixelBlurID = grcEffect::LookupGlobalVar("Shadowmap_PixelSize0");

	// shadow color
	m_ShadowColorID = grcEffect::LookupGlobalVar("Shadowmap_ShadowColor0");

	m_CameraPositionID = grcEffect::LookupGlobalVar("CameraMatrix");

	// random texture ID:
//	m_RandomTextureID = grcEffect::LookupGlobalVar("RandomTex",false);

	// grab handel to copy technique
	m_CopyTechnique = m_DepthShader->GetEffect().LookupTechnique("CopyRT");	

	// fade out distance of shadow map
	m_FadeOutSMapID = grcEffect::LookupGlobalVar("FadeOutLastSMap");


	// get shader variables for tiles:
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		m_Tiles[i]->GetGlobalShaderVars();
	}
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
CShadowMap::~CShadowMap(void)
{
	delete m_DepthShader;
	delete m_BlendShader;

/*
	if (m_RandomTexture)
		m_RandomTexture->Release();
		*/
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
#if __XENON
void CShadowMap::CreateRenderTargets(bool predicatedTiling)
#else
void CShadowMap::CreateRenderTargets()
#endif
{
	int rezx,rezy;

	//Create the offscreen target to hold depth information (Note: on XENON it can be the actual z-buffer)
	grcTextureFactory::CreateParams cp;
	
	cp.Multisample = 0;
#if !__XENON
	// PC uses a R32F render target to store the shadow map + a z-buffer for rendering
	rezx = PC_SHADOWMAP_SIZE;
	rezy = PC_SHADOWMAP_SIZE;
	m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Depth Buffer", grcrtDepthBuffer, rezx, rezy, 32);
	cp.Format = grctfR32F;
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		char depthName[128];
		sprintf(depthName,"Light Depth Buffer%d",i);
		m_Tiles[i]->m_DepthMap = grcTextureFactory::GetInstance().CreateRenderTarget(depthName, grcrtPermanent, rezx, rezy, 32, &cp);
	}
#else
	m_PredicatedTiled=predicatedTiling;
	if (predicatedTiling)
	{
		m_XenonShadowmapSize=PREDICATED_TILED_XENON_SHADOWMAP_SIZE;
	}

	// 360 uses the depth buffer as the shadow map
	// as long as we store everything in EDRAM we can not store more than 1600x1600 pixels for a 32-bit depth buffer at once
	// this is why we need to utilize predicated tiling to get higher resolutions
	rezx = m_XenonShadowmapSize;
	rezy = m_XenonShadowmapSize;
	cp.IsResolvable = true;	
	cp.UseHierZ = true;
	cp.HasParent = true;
	cp.Parent = NULL;
	if (predicatedTiling)
		cp.IsRenderable = false;

	// depth buffers
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		char depthName[128];
		sprintf(depthName,"Depth Buffer%d",i);
		m_Tiles[i]->m_DepthBuffer= grcTextureFactory::GetInstance().CreateRenderTarget(depthName, grcrtDepthBuffer, rezx, rezy, 32, &cp);
	}
#endif

	//
	// intermediate shadow texture that holds the results from the depth comparison == black & white values
	//
	// set render target descriptions
	grcTextureFactory::CreateParams params;
	params.Multisample = 0;
	params.HasParent = true;
	params.Parent = NULL; 
	params.UseFloat = false;
	rezx = GRCDEVICE.GetWidth();
	rezy = GRCDEVICE.GetHeight();
	params.Format = grctfA8R8G8B8;
	m_BlackNWhite = grcTextureFactory::GetInstance().CreateRenderTarget("Shadow Texture", grcrtPermanent, rezx, rezy, 32, &params);

	grcTextureFactory::CreateParams para;
	para.IsResolvable = true;	
	para.UseHierZ = false;
	para.HasParent = true;
	//para.Parent = NULL;
	para.Parent = m_BlackNWhite;
	m_DepthBufferPreview= grcTextureFactory::GetInstance().CreateRenderTarget("Depth Buffer Preview", grcrtDepthBuffer, rezx, rezy, 32, &para);
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::DeleteRenderTargets(void)
{
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		if (m_Tiles[i])
		{
#if !__XENON
			m_Tiles[i]->m_DepthMap->Release();
			m_Tiles[i]->m_DepthMap=NULL;
#else
			m_Tiles[i]->m_DepthBuffer->Release();
			m_Tiles[i]->m_DepthBuffer=NULL;
#endif
		}
	}

#if !__XENON
	if(m_DepthBuffer)
	{
		m_DepthBuffer->Release();
		m_DepthBuffer = NULL;	
	}
#endif

	if(m_DepthBufferPreview)
	{
		m_DepthBufferPreview->Release();
		m_DepthBufferPreview = NULL;	
	}


	// release the black & white texture that holds the results from the depth texture
	if(m_BlackNWhite)
	{
		m_BlackNWhite->Release();
		m_BlackNWhite = NULL;	
	}
}

//---------------------------------------------------------------------------
//  CalcOrthoViewport{}
//  1. calculates the four extrema points that enclose the near and far plane of the view frustum
//  2. calculate the center point of the future near and far planes of the light view frustum
//  3. starting from these center points, we can construct two vectors for each plane that point up and left. These vectors are scaled with the extremas from 1.
//  4. with the vectors that point left and up, we can construct the four edges of the near and far plane
//  5. the resulting eight points are then transformed into light space
//  6. in light space we store now two 2D vectors that hold the x and y extrema of this volume. These are used to construct the orthographic light view frustum.
//  7. calculate half width and height to provide it to the orthographic light view frustum function
//  8. transform camera back to world space before providing it to the viewport class as a camera matrix
//---------------------------------------------------------------------------
void CShadowMap::CalcOrthoViewport(grcViewport& vp, float nearPlane, float farPlane, const Matrix34& lightMatrix, const float FovYDiv)
{
	// Snap the ortho viewport based on what the camera can see
	// Calc far points
	// get the view matrix from the camera
	//	camPos - d
	//	camDir - c
	//	camA - a - "x axis"
	//	camB - b - "y axis"
	Vector3 camPos(grcViewport::GetCurrent()->GetCameraMtx().d);
	Vector3 camDir(-grcViewport::GetCurrent()->GetCameraMtx().c); 
	Vector3 camA(grcViewport::GetCurrent()->GetCameraMtx().a);
	Vector3 camB(grcViewport::GetCurrent()->GetCameraMtx().b);

	// forget about the position ... we deal with a directional light
	// normalize vectors
	camDir.Normalize();
	camA.Normalize();
	camB.Normalize();

	// get the fov x and y value
	float fovY = grcViewport::GetCurrent()->GetFOVY() * FovYDiv;
#if __XENON
	float fovX = fovY * grcViewport::GetCurrent()->GetAspectRatio();
#else
	float fovX = fovY * grcViewport::GetCurrent()->GetAspectRatio();
#endif

	// convert from degrees to radians
	fovX *= DtoR;
	fovY *= DtoR;

	// enclosing rectangles
	Vector2 camViewWidth;  
	Vector2 camViewHeight; 
	camViewWidth.Set(tanf(fovX) * nearPlane, tanf(fovX) * farPlane);
	camViewHeight.Set(tanf(fovY) * nearPlane, tanf(fovY) * farPlane);

	//
	// get two vectors that point in the view direction to the near and far plane
	//
	Vector3 nearCenter(camPos);
	// add a vector that points in the camera direction and 
	// is scaled to get the near center of the frustum
	nearCenter.AddScaled(camDir, nearPlane);
	nearCenter.AddScaled(camDir, -m_ViewportZShift);
	Vector3 farCenter(camPos);
	// add a vector that points in the camera direction and 
	// is scaled to get the far center of the frustum
	farCenter.AddScaled(camDir, farPlane);
	farCenter.AddScaled(camDir, -m_ViewportZShift);

	//
	// get two vectors on the near and two vectors on the far plane
	// each pair points up and left
	//
	// -------------
	// |     ^     |
	// |     |     |
	// |  <--      |
	// |           |
	// |           |
	// -------------
	//
	// left near point from viewer camera frustum
	Vector3 leftNear(camA);
	leftNear.Scale(camViewWidth.x * 0.5f);

	// up near point from viewer camera frustum
	Vector3 upNear(-camB);
	upNear.Scale(camViewHeight.x * 0.5f);

	// left far point from viewer camera frustum
	Vector3 leftFar(camA);
	leftFar.Scale(camViewWidth.y * 0.5f);

	// up far point from viewer camera frustum
	Vector3 upFar(-camB);
	upFar.Scale(camViewHeight.y * 0.5f);


	//
	// Generate visibility box
	//
	int i;
	Vector3 extents[8];
	for(i = 0; i < 4; i++)
	{
		// fill up with the near and far center points
		extents[i].Set(nearCenter);
		extents[i + 4].Set(farCenter);
	}

	// store the four points for the near plane
	// creates based on the near center point the four points that form 
	// the near plane of a viewing frustum
	//
	// x           x
	// 
	//            
	//           
	//           
	//           
	// x           x
	//	
	// go from near center point to the left and then up
	extents[0].Add( leftNear);  extents[0].Add( upNear);
	// go from near center point to the right and then up 
	extents[1].Add(-leftNear);  extents[1].Add( upNear);
	// go from the near center point to the left and then down
	extents[2].Add( leftNear);  extents[2].Add(-upNear);
	// go from the near center point to the right and then down
	extents[3].Add(-leftNear);  extents[3].Add(-upNear);

	// Create far plane
	// creates based on the far center point the four points that form
	// the far plane of a viewing frustum
	// go from the far center point to the left and then up
	extents[4].Add( leftFar);  extents[4].Add( upFar);
	// go from the far center point to the right and then up
	extents[5].Add(-leftFar);  extents[5].Add( upFar);
	// go from the far center point to the left and then down
	extents[6].Add( leftFar);  extents[6].Add(-upFar);
	// go from the far center point to the right and then down
	extents[7].Add(-leftFar);  extents[7].Add(-upFar);

	// grab view matrix constructed from the light direction 
	// == light space matrix
	Matrix34 lightOrientation(lightMatrix);
	lightOrientation.a.Normalize();
	lightOrientation.b.Normalize();
	lightOrientation.c.Normalize();

	// Transforms the visbility box into light space
	// UnTransform() - Transform a vector by the inverse of the current matrix
	for(i = 0; i < 8; i++)
		lightOrientation.UnTransform3x3(extents[i]);

	// Create axis aligned bounding box in light space
	Vector3 minInLight,maxInLight;
	minInLight.Set(extents[0]);
	maxInLight.Set(extents[0]);
	for(i = 0; i < 8; i++)
	{
		// stores the smallest and largest numbers -> extents
		// in light view space
		if(extents[i].x < minInLight.x)
			minInLight.x = extents[i].x;
		if(extents[i].y < minInLight.y)
			minInLight.y = extents[i].y;
		if(extents[i].z < minInLight.z)
			minInLight.z = extents[i].z;
		if(extents[i].x > maxInLight.x)
			maxInLight.x = extents[i].x;
		if(extents[i].y > maxInLight.y)
			maxInLight.y = extents[i].y;
		if(extents[i].z > maxInLight.z)
			maxInLight.z = extents[i].z;
	}
	extents[0].Set(minInLight.x, maxInLight.y, minInLight.z);
	extents[1].Set(maxInLight.x, maxInLight.y, minInLight.z);
	extents[2].Set(minInLight.x, minInLight.y, minInLight.z);
	extents[3].Set(maxInLight.x, minInLight.y, minInLight.z);
	extents[4].Set(minInLight.x, maxInLight.y, maxInLight.z);
	extents[5].Set(maxInLight.x, maxInLight.y, maxInLight.z);
	extents[6].Set(minInLight.x, minInLight.y, maxInLight.z);
	extents[7].Set(maxInLight.x, minInLight.y, maxInLight.z);

	// Camera position is in the center of the near plane
	Vector3 vCameraPos;
	vCameraPos.Lerp(0.5f, extents[0], extents[3]);
	
	// Width is the magnitude of the vector from the near top left corner to the near top right corner
	Vector3 vWidth = extents[1] - extents[0];

	// Height is the magnitude of the vector from the near top left corner to the near bottom left corner
	Vector3 vHeight = extents[2] - extents[0];

	// Depth is the magnitude of the vector from the near top left corner to the far top left corner
	Vector3 vDepth = extents[4] - extents[0];

	float width = vWidth.Mag();
	float height = vHeight.Mag();
	float depth = vDepth.Mag();
	float halfWidth = width * 0.5f;
	float halfHeight = height * 0.5f;

	// Transform the camera position from camera space back into world space
	lightOrientation.Transform3x3(vCameraPos);	
	lightOrientation.d = vCameraPos;
	
	// keep viewport class happy by sending down an identity matrix
	vp.SetWorldMtx(M34_IDENTITY);

	// send down the light camera view matrix
	vp.SetCameraMtx(lightOrientation);

	// dynamic generation of the orthographic projection matrix
	vp.Ortho(halfWidth, -halfWidth, -halfHeight, halfHeight, 0, -depth);
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderIntoShadowMaps(bool clearBuffers)
{
#if __XENON
	// set the pixel shader threads to 112 -> vertex shader can only use 16 threads
	SetGPRAllocation(112);
#endif

	// render into the shadow map tiles
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		BeginTile(i, clearBuffers);
		RenderIntoShadowMap();
		EndTile(i);
	}
	GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.8f,0.f),true,1.0f,0);

#if __XENON
	// set the ratio between vertex and pixel shaders back to default
	SetGPRAllocation(0); 
#endif
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::BlendShadowsIntoColorBuffer(bool clearBuffers)
{
clearBuffers = clearBuffers;
	// lock color black and white buffer
//	grcTextureFactory::GetInstance().LockRenderTarget(0, m_BlackNWhite, m_DepthBufferPreview, grcPositiveX, true);

	// clear render target
//	GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, false);
#if !__PPU & __XENON
	// then draw into all other shadow maps with Z buffer writes off and alpha blending on
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ZWRITEENABLE, false);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);

	// set the pixel shader threads to 112 -> vertex shader can only use 16 threads
	SetGPRAllocation(112);
#endif

	// blend into the framebuffer
	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		BeginRenderTileIntoColorBuffer(i);
		RenderTileIntoColorBuffer();
		EndRenderTileIntoColorBuffer(i);
	}

#if !__PPU & __XENON
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ZWRITEENABLE, true);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

	// set the ratio between vertex and pixel shaders back to default
	SetGPRAllocation(0); 
#endif

//	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

	// clear render target
//	GRCDEVICE.Clear(false, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, false);
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderShadowsIntoColorBuffer(bool clearBuffers)
{
clearBuffers = clearBuffers;
	// lock color black and white buffer
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_BlackNWhite, m_DepthBufferPreview, grcPositiveX, true);

	// clear render target
	GRCDEVICE.Clear(true, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, false);

	// first draw into the shadow map with Z buffer on
	BeginRenderTileIntoColorBuffer(0);
	RenderTileIntoColorBuffer();
	EndRenderTileIntoColorBuffer(0);

#if !__PPU & __XENON
	// then draw into all other shadow maps with Z buffer writes off and alpha blending on
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ZWRITEENABLE, false);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
#endif

	// render into the shadow map tiles
	for (int i=1;i<m_Tiles.GetCount();i++)
	{
		BeginRenderTileIntoColorBuffer(i);
		RenderTileIntoColorBuffer();
		EndRenderTileIntoColorBuffer(i);
	}

#if !__PPU & __XENON
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ZWRITEENABLE, true);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
#endif

	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

	// clear render target
	GRCDEVICE.Clear(false, Color32(1.0f, 1.0f, 1.0f, 1.0f), true, 1.0f, false);

}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::BeginTile(int whichTile, bool /*clearBuffers*/)
{
	// measure the time-frame a shadow map draw call takes
	PIXBegin(0, "Render into Shadow Map");

	Assert(whichTile<m_Tiles.GetCount());
	Assert(whichTile<m_Viewports.GetCount());

	CShadowMapTile& tile=*m_Tiles[whichTile];
	grcViewport&	vp=*m_Viewports[whichTile];

#if !__PPU & __XENON
	// Depth bias
	// On the PS3 you need to use glPolygonOffset(), which should have the same functionality
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_DEPTHBIAS, *(DWORD*)&tile.m_DepthBias);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, *(DWORD*)&tile.m_BiasSlope);
#endif

	// grab current viewport and save it
	m_OldVP = grcViewport::GetCurrent();

	// lock correct depth buffer
#if __XENON
	// first shadow map for near objects
	#define TILE_OVERDRAW 0

	if (m_PredicatedTiled)
	{
		GRCDEVICE.BeginTiledRenderingDepthBuffer(tile.m_DepthBuffer, 0, 0, grcDevice::TILE_HORIZONTAL, m_XenonShadowmapSize, m_XenonShadowmapSize); 
		grcViewport::SetCurrent(grcViewport::GetCurrent());
	}
	else
	{
		// lock depth buffer
		grcTextureFactory::GetInstance().LockRenderTarget(0, NULL, tile.m_DepthBuffer);

		// ok this looks strange ... I know
		if(whichTile == 0)
		{
			// clear depth buffer only
			GRCDEVICE.Clear(false, Color32(0.0f, 0.0f, 0.0f, 1.0f), true, 1.0f, false);
		}
	}
// PC version
#else
	// lock R32F shadow map + depth buffer
	grcTextureFactory::GetInstance().LockRenderTarget(0, tile.m_DepthMap, m_DepthBuffer);

	// Clear R32F shadow map + depth buffer
	GRCDEVICE.Clear(true, Color32(0.0f, 0.0f, 0.0f, 1.0f), true, 1.0f, false);
#endif
	//grcViewport vp;

	// grab current group of lights
	grcLightGroup * LightGroup = grcState::GetLightingGroup();

	// force the first light to be a directional light
	LightGroup->SetLightType(0, grcLightGroup::LTTYPE_DIR);

	// get light direction and position
	m_lightDir = LightGroup->GetDirection(0);
	m_lightPos = LightGroup->GetPosition(0);

	// build up a view matrix
	// store light's position in row d and light's direction in c
	Vector3 A, B, C(m_lightDir);
	A.Set(0.0f,0.0f,-1.0f);
	C.Normalize();
	B.Cross(A,C);
	A.Cross(C,B);
	m_LightMatrix.a.Set(A);
	m_LightMatrix.b.Set(B);
	m_LightMatrix.c.Set(-C);
	m_LightMatrix.d.Set(m_lightPos);  

	m_lightDir.Normalize();
	Vector3 up = g_UnitUp;
	float fDot = 1.0f - fabsf(m_lightDir.Dot(up));
	if( fDot < SMALL_FLOAT && fDot > -SMALL_FLOAT )
		up = XAXIS;

	m_LightMatrix.LookAt(m_lightPos, m_lightPos - m_lightDir, up);

#if __XENON
	// Set up light's viewport
	// calculate light view frustum for near objects
	CalcOrthoViewport(vp, tile.m_NearPlane, tile.m_FarPlane, m_LightMatrix, tile.m_FovYDiv);

	// set new view port
	grcViewport::SetCurrent(&vp);

	Matrix44 view, projection; //, comp;
	view.Set(grcViewport::GetCurrent()->GetViewMtx());
	projection.Set(grcViewport::GetCurrent()->GetProjection());
	tile.m_ViewProj.Dot(projection,view);

// PC Version
#else
	// calculate light view frustum for near objects
	CalcOrthoViewport( vp, tile.m_NearPlane, tile.m_FarPlane, m_LightMatrix, tile.m_FovYDiv);

	// set new view port
	grcViewport::SetCurrent(&vp);

	Matrix44 view, projection; //, comp;
	view.Set(grcViewport::GetCurrent()->GetViewMtx());
	projection.Set(grcViewport::GetCurrent()->GetProjection());
	tile.m_ViewProj.Dot(projection,view);
#endif
}
//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderIntoShadowMap()
{
	// force depth shader
	grmModel::SetForceShader(m_DepthShader);

	RenderShadowCasters(*grcViewport::GetCurrent());

	// undo the force of the shader
	grmModel::SetForceShader(0);
}
//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::EndTile(int XENON_ONLY(whichTile))
{
#if __XENON
	Assert(whichTile<m_Tiles.GetCount());
	Assert(whichTile<m_Viewports.GetCount());

	CShadowMapTile& tile=*m_Tiles[whichTile];

	if (m_PredicatedTiled)
	{
		GRCDEVICE.EndTiledRenderingDepthBuffer(tile.m_DepthBuffer); 
	}
	else
#endif

#if __XENON
	// try to get the free clear from the resolve
	clearParams.ClearColor=false;
	clearParams.ClearDepthStencil=true;
	grcTextureFactory::GetInstance().UnlockRenderTarget(0, &clearParams);
#else
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);
#endif

#if !__PPU & __XENON
		//depth bias
		float fTemp = 0.0f;
		GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_DEPTHBIAS, *(DWORD*)&fTemp);
		GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, *(DWORD*)&fTemp);
#endif

	// set old viewport
	grcViewport::SetCurrent(m_OldVP);

#if __XENON
	// this are variables for the following shaders
	tile.SetGlobalShaderVars(m_ViewportZShift);
#endif

	// measure the time-frame a shadow map draw call takes
	PIXEnd();
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::BeginRenderTileIntoColorBuffer(int whichTile)
{
	// measure the time-frame a shadow map draw call takes
	PIXBegin(0, "Render into Black and White Buffer");

	Assert(whichTile<m_Tiles.GetCount());

#if !__XENON
	// texel size - in the shader this is Shadowmap_PixelSize0
	if(m_PixelBlurID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_PixelBlurID, 1.0f / PC_SHADOWMAP_SIZE);
#else
	// texel size - in the shader this is Shadowmap_PixelSize0
	if(m_PixelBlurID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_PixelBlurID, 1.0f / m_XenonShadowmapSize);
#endif

	// always write first shadow map transformation registers
	// set this here for now
	if(m_Tiles[0]->m_LightMatrixID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_Tiles[0]->m_LightMatrixID, m_Tiles[whichTile]->m_ViewProj);

	// provide camera position
	if(m_CameraPositionID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_CameraPositionID, grcViewport::GetCurrent()->GetCameraMtx());

	// always set into first depth shadow map
#if !__XENON
	if(m_Tiles[0]->m_DepthTextureID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_Tiles[0]->m_DepthTextureID, m_Tiles[whichTile]->m_DepthMap);
#else
	if(m_Tiles[0]->m_DepthTextureID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_Tiles[0]->m_DepthTextureID, m_Tiles[whichTile]->m_DepthBuffer);
#endif

	//
	// the pixel shader has an if statement in there that looks like this
	//
	// if( camDistance > ShadowMapStart && camDistance < NextShadowMapStart)
	// {
	// we need to provide the right values

	// for the first tile we only need an end value
	if (whichTile == 0)
	{
		// point where shadow map should start to show up based on camera distance
		if(m_Tiles[1]->m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_Tiles[1]->m_FadeStartID, 0.0f - m_ViewportZShift);

		// point where shadow map should start to show up based on camera distance
		if(m_Tiles[2]->m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_Tiles[2]->m_FadeStartID, m_Tiles[whichTile + 1]->m_FadeStart - m_ViewportZShift);
	}
	// for the last tile we need only a starting value
	else if(whichTile == m_Tiles.GetCount() - 1)
	{
		// point where shadow map should start to show up based on camera distance
		if(m_Tiles[1]->m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_Tiles[1]->m_FadeStartID, m_Tiles[whichTile]->m_FadeStart - m_ViewportZShift);

		// point where next shadow map should start to show up based on camera distance
		if(m_Tiles[2]->m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_Tiles[2]->m_FadeStartID, 1500.0f);

	}
	// for the all in-between tiles, we need a starting and a end value
	else if((whichTile > 0) && (whichTile < m_Tiles.GetCount() - 1))
	{
		// starting
		if(m_Tiles[1]->m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_Tiles[1]->m_FadeStartID, m_Tiles[whichTile]->m_FadeStart - m_ViewportZShift);

		// point where next shadow map should start to show up based on camera distance
		if(m_Tiles[2]->m_FadeStartID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_Tiles[2]->m_FadeStartID, m_Tiles[whichTile + 1]->m_FadeStart - m_ViewportZShift);
	}

/*
	// apply fade out only to last shadow map
	if (whichTile ==  m_Tiles.GetCount() - 1)
	{
		// fade out starting point of second shadow map
		if(m_FadeOutSMapID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_FadeOutSMapID, m_FadeOutSMap);
	}
	else
	{*/
		// fade out starting point of second shadow map
		if(m_FadeOutSMapID!=grcegvNONE)
			grcEffect::SetGlobalVar(m_FadeOutSMapID, 0.0f);
//	}
}
//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::RenderTileIntoColorBuffer()
{
	// force depth shader
	grmModel::SetForceShader(m_BlendShader);

	RenderShadowCasters(*grcViewport::GetCurrent());

	// undo the force of the shader
	grmModel::SetForceShader(0);
}
//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::EndRenderTileIntoColorBuffer(int XENON_ONLY(whichTile))
{
	// measure the time-frame a black & white draw call takes
	PIXEnd();
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
/*
void CShadowMap::SetRandomTexture(const grcTexture& texture)
{
	if (m_RandomTexture)
		m_RandomTexture->Release();
	m_RandomTexture=&texture;
	m_RandomTexture->AddRef();
}
*/
//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void CShadowMap::SetGlobals()
{
#if !__XENON
	// texel size - in the shader this is Shadowmap_PixelSize0
	if(m_PixelBlurID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_PixelBlurID, 1.0f / PC_SHADOWMAP_SIZE);
#else
	// texel size - in the shader this is Shadowmap_PixelSize0
	if(m_PixelBlurID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_PixelBlurID, 1.0f / m_XenonShadowmapSize);
#endif

//	if(m_RandomTexture!=NULL && m_RandomTextureID!=grcegvNONE) 
//		grcEffect::SetGlobalVar(m_RandomTextureID, m_RandomTexture);

	// shadow color 
	if(m_ShadowColorID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_ShadowColorID, m_ShadowColor);

	// fade out starting point of second shadow map
	if(m_FadeOutSMapID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_FadeOutSMapID, m_FadeOutSMap);

	// provide camera position
	if(m_CameraPositionID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_CameraPositionID, grcViewport::GetCurrent()->GetCameraMtx());
}

//---------------------------------------------------------------------------
// shows the shadow map as a quad on the screen for debugging purposes
// should be relative to the screen size
//---------------------------------------------------------------------------
void CShadowMap::ShowShadowMap(grcTexture * texture, 
					  grcEffectTechnique technique,					// technique in FX file
					  int DestX, int DestY, int SizeX, int SizeY)								
{
	Color32 white(1.0f,1.0f,1.0f,1.0f);

	grcEffect::SetGlobalVar(m_Tiles[0]->m_DepthTextureID, texture); //grcTextureFactory::GetInstance().GetNotImplementedTexture());

	// blit
	m_DepthShader->Blit(DestX,			// x1 - Destination base x
						DestY,			// y1 - Destination base y
						SizeX,			// x2 - Destination opposite-corner x
						SizeY,			// y1 - Destination opposite-corner y
						0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
						0.0f,			// u1 - Source texture base u (in normalized texels)
						0.0f,			// v1 - Source texture base v (in normalized texels)
						1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
						1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
						(Color32)white,		// color - reference to Packed color value
						technique);

	grcEffect::SetGlobalVar(m_Tiles[0]->m_DepthTextureID, (grcTexture*)grcTexture::None);
}
//---------------------------------------------------------------------------
// visualize render targets
//
//---------------------------------------------------------------------------
void CShadowMap::ShowShadowRenderTargets()
{

	const int OriginX = 32;
	const int OriginY = 96;
	const int RectWidth = 96;
	const int RectHeight = 96;

	if(m_ShowShadowMap)
	{
		for (int i=0;i<m_Tiles.GetCount();i++)
		{
#if !__XENON
			ShowShadowMap(m_Tiles[i]->m_DepthMap, m_CopyTechnique,  
							OriginX,					// top left x
							OriginY,					// top left y 
							OriginX + RectWidth,		// bottom right x
							OriginY + RectHeight);		// bottom right y
#else
			ShowShadowMap(m_Tiles[i]->m_DepthBuffer, m_CopyTechnique,  
							OriginX,						// top left x
							OriginY + i*RectHeight,			// top left y 
							OriginX + RectWidth,			// bottom right x
							OriginY + (i+1) * RectHeight);	// bottom right y
#endif
		}
	}

	// show black & white texture
	if(m_BlackNWhiteMap)
	{
		ShowShadowMap(m_BlackNWhite, m_CopyTechnique,
						OriginX + RectWidth,				// top left x
						OriginY + RectHeight,			// top left y 
						// 16:9 ratio
						OriginX + 16 * (RectWidth / 2),		// bottom right x
						OriginY + 11 * (RectHeight / 2));	// bottom right y
	}

}

#if __BANK
typedef void (CShadowMap::*CShadowMapMember0)();

void CShadowMap::AddSaveWidgets(bkBank& bk)
{
	// gcc wants a double cast to remove all doubt as to how you want this to work ...
	bk.AddButton("Save Tunables...",datCallback((Member0)(CShadowMapMember0)&CShadowMap::SaveTunablesCB<CShadowMap>,this));
	bk.AddButton("Load Tunables...",datCallback((Member0)(CShadowMapMember0)&CShadowMap::LoadTunablesCB<CShadowMap>,this));

}

void CShadowMap::AddWidgets(bkBank &bk)
{
	bk.AddSlider("Fade out last Shadow map", &m_FadeOutSMap, 0.0f, 1000.0f, 1.0f);

	if (bkRemotePacket::IsConnectedToRag())
	{
		bk.AddColor("Shadow Color", &m_ShadowColor);
	}
	else
	{
		bk.AddSlider("Shadow Color Red", &m_ShadowColor.x, 0.0f, 1.0f, 0.1f);
		bk.AddSlider("Shadow Color Green", &m_ShadowColor.y, 0.0f, 1.0f, 0.1f);
		bk.AddSlider("Shadow Color Blue", &m_ShadowColor.z, 0.0f, 1.0f, 0.1f);
	}
	bk.AddToggle("Show Shadow Map", &m_ShowShadowMap);
	bk.AddToggle("Black & White Map", &m_BlackNWhiteMap);
	bk.AddSlider("Viewport Z Offset", &m_ViewportZShift, -100.0f, 1000.0f, 0.1f);

	for (int i=0;i<m_Tiles.GetCount();i++)
	{
		const int GROUP_NAME_LEN=128;
		char groupName[GROUP_NAME_LEN];
		bk.PushGroup(formatf(groupName,GROUP_NAME_LEN,"Tile #%d",i));
		m_Tiles[i]->AddWidgets(bk);
		bk.PopGroup();
	}
}
#endif

//
// set the vertex/pixel shader threads on 360
//
#if __XENON

// by default we want to use 96 pixel shader threads
static int s_DefaultGPRAllocation = 96;

void CShadowMap::SetGPRAllocation(int pixelThreads)
{

	if (pixelThreads<0) // -1 means use defaults
		pixelThreads = s_DefaultGPRAllocation;

	// check if it is not already set
	if (pixelThreads != m_CurrentPixelThreads)
	{

		m_CurrentPixelThreads = pixelThreads;

		if (m_CurrentPixelThreads == 0) // 0 mean system defaults...
			GRCDEVICE.SetShaderGPRAllocation(0,0);
		else
			GRCDEVICE.SetShaderGPRAllocation(128 - m_CurrentPixelThreads, m_CurrentPixelThreads);
	}
}
#endif

} // namespace rage
