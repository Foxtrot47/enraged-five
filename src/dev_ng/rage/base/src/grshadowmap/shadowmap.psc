<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef constructable="false" type="rage::CShadowMap">
<array name="m_Tiles" type="atArray">
<pointer policy="owner" type="rage::CShadowMapTile"/>
</array>
<float name="m_FadeOutSMap"/>
</structdef>

<structdef type="rage::CShadowMapTile">
<int name="m_WhichTile"/>
<float name="m_NearPlane"/>
<float name="m_FarPlane"/>
<float name="m_DepthBias"/>
<float name="m_BiasSlope"/>
<float name="m_FadeStart"/>
<float name="m_FadeInSMap"/>
<float name="m_FovYDiv"/>
</structdef>

</ParserSchema>