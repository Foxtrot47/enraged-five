// 
// grshadowmap/shadowcollector.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#ifndef GRSHADOWMAP__SHADOWCOLLECTOR_H
#define GRSHADOWMAP__SHADOWCOLLECTOR_H

#include "atl/array.h"
#include "atl/ownedptr.h"
#include "bank/bkmgr.h"
#include "data/base.h"
#include "grcore/effect.h"
#include "parser/manager.h"
#include "vector/matrix44.h"
#include "grcore/viewport.h"
#include "grcore/texture.h"
#include "grcore/im.h"


namespace rage
{
	class bkBank;
	class grcRenderTarget;
	class grcViewport;
	class grmShader;
	class Color32;


class CShadowCollector : public datBase
{
public:
	//
	// PURPOSE
	//	constructor
	// PARAMS
	//	
	//
	CShadowCollector();

	// destructor
	virtual	~CShadowCollector(void);

	//
	// PURPOSE
	//	creates and loads the shaders
	// PARAMS
	//	path - the path to load the shaders from.  If it == NULL, use the currently set path.
	//
	void InitShaders(const char *path=NULL); 

	// 
	// PURPOSE
	//	allocates the shadow map render targets
	// PARAMS
	//	
	//	
	// NOTES
	//	if the device changes or the user wants a different resolution: 
	// delete and create new shadow map
#if __XENON
	void CreateRenderTargets(bool predicatedTiling, grcRenderTarget* depthBuffer = 0,bool multisample = false , bool msaa = false, bool useCompressed = false );
#else
	void CreateRenderTargets();
#endif

	//
	// PURPOSE
	//	frees resources that were occupied by the CreateRenderTargets() call
	// RETURNS
	//	none
	void	DeleteRenderTargets(void);

	//
	// PURPOSE
	//	locks shadow collector shadow map
	// RETURNS
	//	none
	void	LockShadowCollector(void);

	//
	// PURPOSE
	//	unlocks shadow collector shadow map
	// PARAMS
	//	resolveFlags - optional clear flags for the final resolve of the collected shadow texture, default is NULL = no clearing during resolve.
	// RETURNS
	//	none
	void  UnlockShadowCollector(grcResolveFlags * resolveFlags=NULL);
	//
	// PURPOSE
	//	set shadow collector map
	// RETURNS
	//	none
	void SetShadowCollectorMap(void);

	//
	// PURPOSE
	//	preview shadow collector 2D screenspace map
	// RETURNS
	//	none
	void ShowShadowRenderTargets();

	void EnableZBufferWrite(void);

	void DisableZBufferWrite(void);

	void DisableAlphaBlend(void);

	void EnableAlphaBlend(void);


	// PURPOSE
	//	old fashioned widget code ... will be removed by the new widget class
	void AddWidgets(bkBank& bank);
	void AddSaveWidgets(bkBank& bk);

	// get/set the preview flag for the shadow collector map
	bool GetShadowPreviewFlag() const { return m_bShadowCollectorMap; }
	void SetShadowPreviewFlag(bool b, int channel) { m_bShadowCollectorMap = b; m_iShadowCollectorChannel = channel;}

	// static functions
	// keep track of the instance
	static void Init();
	static void Terminate();
	static CShadowCollector *GetInstance() {return sm_instance;}

private:
	// PURPOSE
	//	provides a preview on the 2D screen-space black & white texture
	// RETURNS
	//	nothing
	void ShowShadowMap(grcTexture * texture, 
						grcEffectTechnique technique,					// technique in FX file
						float DestX, float DestY, float SizeX, float SizeY);	



	// the shader used for render into the color buffer and alpha blend the shadows
	grmShader * m_ShadowCollectorShader;			

	// technique that is used to view the depth map
	grcEffectTechnique m_CopyTechnique;
	grcEffectTechnique m_CopyTechnique1;	// These techniques do per-channel copying
	grcEffectTechnique m_CopyTechnique2;
	grcEffectTechnique m_CopyTechnique3;
	grcEffectTechnique m_CopyTechnique4;

	// handle for workhorse render target sampler stages
	grcEffectVar		m_RenderMapId;				

	// handle to shadow collector texture 
	grcEffectGlobalVar m_ShadowCollectorID;

	// handle to store the texel size of the shadow collector map
	grcEffectGlobalVar	m_ShadowCollectorTexelSizeID;

	// toggle black & white map preview
	bool			m_bShadowCollectorMap;
	int				m_iShadowCollectorChannel;
	int				m_iShadowCollectorMapHeight;
	int				m_iShadowCollectorMapWidth;

#if __XENON
	bool m_PredicatedTiled;
#endif

	// render target that holds black & white depth comparison results
	// this is applied to the scene later as a texture
	grcRenderTarget* m_ShadowCollector;
	grcRenderTarget* m_ShadowCollectorDepthBuffer;

	static CShadowCollector *sm_instance;			// instance
};

#define GRSHADOWCOLLECTOR (CShadowCollector::GetInstance())

}	// name space range
#endif
