Project rage_lib_psc
ProjectType util
ForceLanguage Cpp

ParserExtract

Files {
	Directory {
		..\..\..\3rdParty\curl-7.28.0
#		..\..\..\3rdParty\wolfssl-3.14.0
		..\..\..\3rdParty\miniupnpc-2.0
		..\..\..\3rdParty\portcullis
		..\atl
		..\audioasiolib
		..\audiodata
		..\audioeffecttypes
		..\audioengine
		..\audiohardware
		..\audioscriptsoundtypes
		..\audiosoundtypes
		..\audiosynth
		..\bank
		..\cranimation
		..\creature
		..\crmetadata
		..\crskeleton
		..\curve
#		..\curvelib
		..\data
		..\diag
		..\file
		..\forceinclude
		..\grcore
		..\grmodel
		..\grprofile
		..\input
		..\jpeg
		..\math
		..\mathext
		..\mesh
		..\net
		..\output
		..\paging
		..\parser
		..\parsercore
		..\pharticulated
		..\phbound
		..\phbullet
		..\phcore
		..\pheffects
#		..\phsoft
		..\phsolver
		..\physics
		..\profile
		..\qa
		..\rline
		..\rmcore
		..\rmocclude
#		..\sga
		..\shaderlib
		..\spatialdata
		..\string
		..\system
		..\text
		..\vector
		..\vectormath
		..\zlib
	}
}
