#ifndef AUD_SCRIPTSOUNDVM_H
#define AUD_SCRIPTSOUNDVM_H

#include "atl/functor.h"

namespace rage
{

class audScriptSoundVM;
typedef void (*opfuncptr)(audScriptSoundVM *, float &, float, float);
#define DECLARE_OPCODE(x) static void x(audScriptSoundVM *, float &,float,float)
#define IMPLEMENT_UNARY_OPCODE(x) void audScriptSoundVM::x(audScriptSoundVM *,float &dest, float, float)
#define IMPLEMENT_BINARY_OPCODE(x) void audScriptSoundVM::x(audScriptSoundVM *,float &dest, float op1, float)
#define IMPLEMENT_TENARY_OPCODE(x) void audScriptSoundVM::x(audScriptSoundVM *,float &dest, float op1, float op2)


class audScriptSoundVM
{
public:
	audScriptSoundVM(const u32 *byteCode, const u32 numCodeWords);

	void Set_GetVariableFunctor(Functor1Ret<f32*,const u32> getVariableFunc) { m_GetVariableFunctor = getVariableFunc; }

	void Run();

private:

	enum Flags
	{
		LessThan = 1,
		GreaterThan = 2,
		EqualTo = 4,
	};
	enum { kNumRegisters = 32};
	DECLARE_OPCODE(NOP);
	
	DECLARE_OPCODE(MOV);
	DECLARE_OPCODE(ABS);
	DECLARE_OPCODE(FLOOR);
	DECLARE_OPCODE(CEIL);
	DECLARE_OPCODE(SIN);
	DECLARE_OPCODE(COS);
	DECLARE_OPCODE(SSIN);
	DECLARE_OPCODE(SCOS);
	DECLARE_OPCODE(SSAW);
	DECLARE_OPCODE(STRI);
	DECLARE_OPCODE(SSQR);
	DECLARE_OPCODE(LTDB);
	DECLARE_OPCODE(DBTL);
	DECLARE_OPCODE(PTR);
	DECLARE_OPCODE(RTP);
	DECLARE_OPCODE(CMP);

	// tenary
	DECLARE_OPCODE(ADD);
	DECLARE_OPCODE(SUB);
	DECLARE_OPCODE(MIN);
	DECLARE_OPCODE(MAX);
	DECLARE_OPCODE(RAND);
	DECLARE_OPCODE(FSEL);
	DECLARE_OPCODE(CLAMP);
	DECLARE_OPCODE(LERP);

	DECLARE_OPCODE(MUL);
	DECLARE_OPCODE(DIV);

	// unary opcodes
	DECLARE_OPCODE(INC);
	DECLARE_OPCODE(DEC);

	// branching
	DECLARE_OPCODE(JL);
	DECLARE_OPCODE(JLE);
	DECLARE_OPCODE(JG);
	DECLARE_OPCODE(JGE);
	DECLARE_OPCODE(JE);

	u32 m_Flags;
	const u32 *m_ByteCode;
	u32 m_NumCodeWords;

	Functor1Ret<f32 *, const u32> m_GetVariableFunctor;

	static opfuncptr optable[];
};

} // namespace rage
#endif // AUD_SCRIPTSOUNDVM_H