#ifndef AUD_SSVM_OPCODES_H
#define AUD_SSVM_OPCODES_H
namespace Opcodes
{

enum Opcode
{
	NOP = 0,

	//binary operators
	FIRST_BINARY_OPCODE,
	LOADI = FIRST_BINARY_OPCODE,
	LOAD,
	STORE,
	MOV,
	ABS,
	FLOOR,
	CEIL,
	SIN,
	COS,
	SSIN,
	SCOS,
	SSAW,
	STRI,
	SSQR,
	LTDB,
	DBTL,
	PTR,
	RTP,

	CMP,
	LAST_BINARY_OPCODE = CMP,

	// tenary operators
	FIRST_TENARY_OPCODE,
	ADD = FIRST_TENARY_OPCODE,
	SUB,
	MIN,
	MAX,
	RAND,
	FSEL,
	CLAMP,
	LERP,
	MUL,
	DIV,
	LAST_TENARY_OPCODE = DIV,

	// unary opcodes
	FIRST_UNARY_OPCODE,
	INC = FIRST_UNARY_OPCODE,
	DEC,

	JL,
	JLE,
	JG,
	JGE,
	JMP,
	JE,
	LAST_UNARY_OPCODE = JE,
};
}
#endif // AUD_SSSVM_OPCODES_H