
#include "audiohardware/channel.h"
#include "scriptsoundvm.h"
#include "opcodes.h"
#include "audioengine/engineutil.h"
#include "audiohardware/driverutil.h"
#include "math/amath.h"

namespace rage
{
	
audScriptSoundVM::audScriptSoundVM(const u32 *byteCode, const u32 numCodeWords)
: m_ByteCode(byteCode)
, m_NumCodeWords(numCodeWords)
{

}

#define ADDRESS_OF_OPCODE(x) &audScriptSoundVM::x

opfuncptr audScriptSoundVM::optable[] = 
{
	ADDRESS_OF_OPCODE(NOP),
	ADDRESS_OF_OPCODE(NOP),// loadi is handled as a special case
	ADDRESS_OF_OPCODE(NOP),//load
	ADDRESS_OF_OPCODE(NOP),//store
	ADDRESS_OF_OPCODE(MOV),
	ADDRESS_OF_OPCODE(ABS),
	ADDRESS_OF_OPCODE(FLOOR),
	ADDRESS_OF_OPCODE(CEIL),
	ADDRESS_OF_OPCODE(SIN),
	ADDRESS_OF_OPCODE(COS),
	ADDRESS_OF_OPCODE(SSIN),
	ADDRESS_OF_OPCODE(SCOS),
	ADDRESS_OF_OPCODE(SSAW),
	ADDRESS_OF_OPCODE(STRI),
	ADDRESS_OF_OPCODE(SSQR),
	ADDRESS_OF_OPCODE(LTDB),
	ADDRESS_OF_OPCODE(DBTL),
	ADDRESS_OF_OPCODE(PTR),
	ADDRESS_OF_OPCODE(RTP),

	ADDRESS_OF_OPCODE(CMP),

	ADDRESS_OF_OPCODE(ADD),
	ADDRESS_OF_OPCODE(SUB),
	ADDRESS_OF_OPCODE(MIN),
	ADDRESS_OF_OPCODE(MAX),
	ADDRESS_OF_OPCODE(RAND),
	ADDRESS_OF_OPCODE(FSEL),
	ADDRESS_OF_OPCODE(CLAMP),
	ADDRESS_OF_OPCODE(LERP),
	ADDRESS_OF_OPCODE(MUL),
	ADDRESS_OF_OPCODE(DIV),

	// unary opcodes
	ADDRESS_OF_OPCODE(INC),
	ADDRESS_OF_OPCODE(DEC),
//	ADDRESS_OF_OPCODE(JL),
//	ADDRESS_OF_OPCODE(JLE),
//	ADDRESS_OF_OPCODE(JG),
//	ADDRESS_OF_OPCODE(JGE),
//	ADDRESS_OF_OPCODE(JE),
};

void audScriptSoundVM::NOP(audScriptSoundVM*,float&,float,float)
{

}

IMPLEMENT_BINARY_OPCODE(MOV)
{
	dest = op1;
}

IMPLEMENT_BINARY_OPCODE(ABS)
{
	dest = Abs(op1);
}

IMPLEMENT_BINARY_OPCODE(FLOOR)
{
	dest = Floorf(op1);
}

IMPLEMENT_BINARY_OPCODE(CEIL)
{
	dest = ceilf(op1);
}

IMPLEMENT_BINARY_OPCODE(SIN)
{
	dest = Sinf(op1);
}

IMPLEMENT_BINARY_OPCODE(COS)
{
	dest = Cosf(op1);
}

IMPLEMENT_BINARY_OPCODE(SSIN)
{
	dest = (1.f + Sinf(op1 * 2.f * PI)) * 0.5f;
}
IMPLEMENT_BINARY_OPCODE(SCOS)
{
	dest = (1.f + Cosf(op1 * 2.f * PI)) * 0.5f;
}

IMPLEMENT_BINARY_OPCODE(SSAW)
{
	dest =  op1 - floorf(op1);
}

IMPLEMENT_BINARY_OPCODE(STRI)
{
	const f32 x = op1 - Floorf(op1);
	const f32 risingVal = x * 2.f;
	const f32 fallingVal = 1.f - ((x-0.5f) * 2.f);
	dest = Selectf(x - 0.5f, fallingVal, risingVal);
}

IMPLEMENT_BINARY_OPCODE(SSQR)
{
	const f32 x = op1 - Floorf(op1);
	dest = Selectf(x - 0.5f, 0.0f, 1.0f);
}

IMPLEMENT_BINARY_OPCODE(LTDB)
{
	dest = audDriverUtil::ComputeDbVolumeFromLinear(op1);
}

IMPLEMENT_BINARY_OPCODE(DBTL)
{
	dest = audDriverUtil::ComputeLinearVolumeFromDb(op1);
}

IMPLEMENT_BINARY_OPCODE(PTR)
{
	dest = audDriverUtil::ConvertPitchToRatio(op1);
}

IMPLEMENT_BINARY_OPCODE(RTP)
{
	dest = static_cast<f32>(audDriverUtil::ConvertRatioToPitch(op1));
}

IMPLEMENT_TENARY_OPCODE(ADD)
{
	dest = op1 + op2;
}

IMPLEMENT_TENARY_OPCODE(SUB)
{
	dest = op1 - op2;
}

IMPLEMENT_TENARY_OPCODE(MUL)
{
	dest = op1 * op2;
}

IMPLEMENT_UNARY_OPCODE(INC)
{
	dest += 1.f;
}

IMPLEMENT_UNARY_OPCODE(DEC)
{
	dest -= 1.f;
}

IMPLEMENT_TENARY_OPCODE(DIV)
{
	dest = op1 / op2;
}

IMPLEMENT_TENARY_OPCODE(MIN)
{
	dest = Min(op1,op2);
}
IMPLEMENT_TENARY_OPCODE(MAX)
{
	dest = Max(op1,op2);
}
IMPLEMENT_TENARY_OPCODE(RAND)
{
	dest = audEngineUtil::GetRandomNumberInRange(op1,op2);
}
IMPLEMENT_TENARY_OPCODE(FSEL)
{
	// NOTE: reuse of op0
	dest = Selectf(dest,op1,op2);
}
IMPLEMENT_TENARY_OPCODE(CLAMP)
{
	// NOTE: reuse of op0
	dest = Clamp(dest, op1, op2);
}
IMPLEMENT_TENARY_OPCODE(LERP)
{
	// NOTE: reuse of op0
	dest = Lerp(dest, op1, op2);
}

void audScriptSoundVM::CMP(audScriptSoundVM *_this,float &dest, float op1, float)
{
	_this->m_Flags = 0;
	
	if(dest > op1)
	{
		_this->m_Flags |= GreaterThan;
	}
	else if(dest < op1)
	{
		_this->m_Flags |= LessThan;
	}
	else if(dest == op1)
	{
		_this->m_Flags |= EqualTo;
	}
}

void audScriptSoundVM::Run()
{
	const u32 *pc = m_ByteCode;
	float registers[kNumRegisters] = {0.f};

	bool isFinished = false;
	while(!isFinished)
	{
		const u32 instruction = *pc++;
		if(pc >= m_ByteCode + m_NumCodeWords)
		{
			isFinished = true;
			continue;
		}
		else
		{
			const u32 opcode = (instruction & 0xff);
			const u32 operand0 = (instruction>>8) & 0xff;

			if(opcode == Opcodes::LOADI)
			{
				registers[operand0] = *((float*)pc++);			
			}
			else if(opcode == Opcodes::LOAD || opcode == Opcodes::STORE)
			{
				const u32 variableIndex = instruction>>16;
				f32 *var = m_GetVariableFunctor(variableIndex);
				if(var)
				{
					if(opcode == Opcodes::LOAD)
					{
						registers[operand0] = *var;
					}
					else
					{
						*var = registers[operand0];
					}
				}
			}
			else if(opcode >= Opcodes::JL && opcode <= Opcodes::JE)
			{
				bool takeJump = false;
				switch(opcode)
				{
				case Opcodes::JL:
					takeJump = (m_Flags & LessThan) != 0;
					break;
				case Opcodes::JG:
					takeJump = (m_Flags & GreaterThan) != 0;
					break;
				case Opcodes::JGE:
					takeJump = ((m_Flags & GreaterThan) | (m_Flags & EqualTo)) != 0;
					break;
				case Opcodes::JLE:
					takeJump = ((m_Flags & LessThan) | (m_Flags & EqualTo)) != 0;
					break;
				case Opcodes::JE:
					takeJump = (m_Flags & EqualTo) != 0;
					break;
				case Opcodes::JMP:
					takeJump = true;
				}
				if(takeJump)
				{
					// decode jump words
					s32 jumpWords = static_cast<s32>(instruction >> 8);
					jumpWords &= ~(1<<23);
					if((instruction & (1U<<31U)) != 0U)
					{
						jumpWords *= -1;
					}
					
					pc += jumpWords;
				}
			}
			else
			{
				const u32 operand1 = (instruction>>16) & 0xff;
				const u32 operand2 = (instruction>>24) & 0xff;
				optable[opcode](this,registers[operand0],registers[operand1],registers[operand2]);
			}
		}
	}

	for(int i = 0; i < kNumRegisters; i+=4)
	{
		audDisplayf("r%d: %f r%d: %f r%d: %f r%d: %f", i, registers[i], i+1, registers[i+1], i+2, registers[i+2], i+3, registers[i+3]);
	}
}

} // namespace rage
