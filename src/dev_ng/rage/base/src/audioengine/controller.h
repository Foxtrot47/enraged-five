//
// audioengine/controller.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_CONTROLLER_H
#define AUD_CONTROLLER_H

#include "audioengine/requestedsettings.h"
#include "audiosoundtypes/sound.h"
#include "atl/bitset.h"
#include "atl/pool.h"
#include "bank/bank.h"
#include "system/criticalsection.h"

namespace rage {

#define MAX_AUDIO_ENTITIES 2048
#define MAX_CONTROLLER_SOUNDS 2048
#define MAX_ENVIRONMENT_GROUPS 512

#ifndef AUD_INVALID_CONTROLLER_ENTITY_ID
#define AUD_INVALID_CONTROLLER_ENTITY_ID 0xFFFF
#endif
	
class audController;
extern audController* g_Controller;
class audMetadataRef;
struct audMetadataChunk;

#if __ASSERT
class audControllerThreadVerifier
{
public:
	virtual ~audControllerThreadVerifier(){}
	virtual void OnCreateSound(const audMetadataRef &soundRef) const = 0;
};
#endif

struct audControllerEntityRef
{
	audControllerEntityRef()
	{
		entity = NULL;
		nextEntity = 0xffff;
		prevEntity = 0xffff;
		firstSoundRef = 0xffff;
	}
	audEntity *entity;
	u16 nextEntity;
	u16 prevEntity;
	u16 firstSoundRef;
};

struct audControllerSoundRef
{
	audControllerSoundRef()
	{
		nextSoundRef = 0xffff;
		bucketId = slotId = 0xff;
	}
	u16 nextSoundRef;
	u8 bucketId;
	u8 slotId;
};
//
// PURPOSE
//  A single audController lives in each thread that can create sounds. audEntities register with their thread's
//  Controller and request sounds via that, rather than directly. It manages the game-side lifecycle of sounds,
//  and will update them, mark them for deletion, etc, as appropriate. It also updates the audEntities that are
//  registered with it.
//
class audController
{
public:
	audController();
	~audController();

	// PURPOSE
	//  Statically initialise the class, including setting up thread-safety sysIpcSema.
	// RETURNS
	//  True if initialisation was successful.
	static bool InitClass();
	// PURPOSE
	//  Shutdown the static aspect of the class.
	static void ShutdownClass();

	// PURPOSE
	//  Returns the audController initialised in the current thread. 
	// RETURNS
	//  The thread's audController.
	static audController* FindControllerForThread();
	__forceinline static audController *GetController()
	{
		return g_Controller;
	}
	// PURPOSE
	//  Returns the audController initialised in the passed-in thread.
	// PARAMS
	//  threadID - the thread whose audController is wanted.
	// RETURNS
	//  The thread's audController.
//	static audController* FindControllerForThread(sysIpcThreadId threadID);
	// PURPOSE
	//  Stores the mapping between audController and current thread - this is now handled with TLS rather than a static
	//  array of threadIDs and controllers.
	// PARAMS
	//  controller - the audController to register.
	static void RegisterControllerForThisThread(audController* controller);
	// PURPOSE
	//  Removes the mapping between this thread and its associated audController
	static void UnregisterControllerForThisThread();
	// PURPOSE
	// Returns true if ANY of the registered audControllers are active. Safe to restart if FALSE.
	//static bool AreControllersActive();
	// PURPOSE
	//  Sets a static flag to tell all Controllers when serviced to clear all sounds and go inactive, or go active again.
	// PARAMS
	//  active - boolean flag determining whether controllers should be active.
	static void FlagControllersActive(bool active);
	// PURPOSE
	// RETURNS
	//  true if audControllers are flagged to go active on next Update()
	static bool AreControllersFlaggedActive();

	// PURPOSE
	//  Initialises a Controller. Clears out its Sound and audEntity lists, and registers itself as the thread's Controller.
	// RETURNS
	//  true if initialisation is successful. 
	bool Init();
	// PURPOSE
	//  Shuts down a Controller - currently does little in the way of cleaning up.
	void Shutdown();

	// PURPOSE
	// Called in main game-thread update loop, to update each audEntity (via PreUpdateService()
	void PreUpdate(const u32 timeInMs);

	// PURPOSE
	//  Called in main game-thread update loop, to update each audEntity and sound
	// PARAMS
	//  timeInMs - the current game time.
	void Update(const u32 timeInMs);

	// PURPOSE
	// Called after the audio and game audio updates on the main thread (e.g. for redgref handling)
	void PostUpdate();

	// PURPOSE
	//  Called from Update, update each sound in this entity
	// PARAMS
	//  timeInMs	- the current game time.
	//	entRef		- the entity ref to update
	//	indices		- the requested settings indices for thsi frame
	void UpdateEntityRef(audControllerEntityRef& entRef, const u32 timeInMs, const audRequestedSettings::Indices &indices);

	// PURPOSE
	//  Called in an audEntity's constructor, to register the entity with this thread's Controller.
	// RETURNS
	//	The controllerId for this entity
	// PARAMS
	//  entity - the audEntity to register.
	u32 RegisterEntity(audEntity* entity);

	// PURPOSE
	//  Called in audEntity's destructors, to remove it from the controller's list of current audio entities.
	// PARAMS
	//  entity - the audEntity to remove from the list.
	void UnregisterEntity(audEntity* entity);

	// PURPOSE
	//  Calls StopAllSounds() on all registered entities 
	void StopAllSounds();

	// PURPOSE
	//  Call when the entity wants to simply orphan or stop all the sounds it has playing without unregistering the entity
	// PARAMS
	//  entity - the audEntity to remove from the list.
	//	allowRelease - allow the sounds to go through their release phase
	void StopAllSounds(audEntity* entity, bool allowRelease = true);

	// PURPOSE
	//  When a metadata chunk is unloaded, we need to clear out all references to objects in that chunk.
	void RegisterGameObjectMetadataUnloading(const audMetadataChunk& chunk);

	// PURPOSE
	//  Called by audio entities when creating new environment groups - this registers the group with the thread's
	//  controller, which then services it, and deletes it when no entities or sounds are using it.
	void RegisterEnvironmentGroup(audEnvironmentGroupInterface* environmentGroup);

	// PURPOSE
	//  Cleans-up all Sounds that would be orphaned by the specified tracker being deleted.
	//  This will be called in the game thread, but so will anything that relies on m_Tracker, so this is safe.
	// PARAMS
	//  tracker - the tracker that is being deleted.
	void ReportTrackerDeletion(audTracker *tracker);

	// PURPOSE
	//  Used by audEntity base class fns to request sounds from their controllers, not directly from the soundFactory.
	//  Does not automatically set up a soundReference, so the returned audSound* should not be used across
	//  game frames - GetPersistentSoundInstance() deals with that case.
	// NOTE
	//  Game audio code should not use this directly, but instead should request sounds using the audEntity base class
	//  functions CreateSound and CreateAndPlaySound fns. 
	// PARAMS
	//  soundName - name of the sound to be created
	//  entity - the audio entity to be associated with this sound
	//  initParams - a structure containing various creation parameters
	// RETURNS
	//  A ptr to the created sound.
	audSound* GetSoundInstance(const char *soundName, audEntity* entity, audSound **entitySoundRef, const audSoundInitParams* initParams);
	audSound* GetSoundInstance(const u32 soundNameHash, audEntity* entity, audSound **entitySoundRef, const audSoundInitParams* initParams);
	// PURPOSE
	//  Used by audEntity base class fns to request sounds from their controllers, not directly from the soundFactory.
	//  Does not automatically set up a soundReference, so the returned audSound* should not be used across
	//  game frames - GetPersistentSoundInstance() deals with that case.
	// NOTE
	//  Game audio code should not use this directly, but instead should request sounds using the audEntity base class
	//  functions CreateSound and CreateAndPlaySound fns. 
	// PARAMS
	//  soundRef - hash or offset of the sound to be created
	//  entity - the audio entity to be associated with this sound
	//  initParams - a structure containing various creation parameters
	// RETURNS
	//  A ptr to the created sound.
	audSound* GetSoundInstance(const audMetadataRef soundRef, audEntity* entity, audSound **entitySoundRef, const audSoundInitParams* initParams);

	// PURPOSE
	//  Used by audEntity base class fns to request sounds from their controllers, not directly from the soundFactory.
	//  Automatically sets up a soundReference, so the audSound* stored in soundReference may be used across
	//  game frames.
	// NOTE
	//  Game audio code should not use this directly, but instead should request sounds using the audEntity base class
	//  functions CreateSound and CreateAndPlaySound fns. 
	// PARAMS
	//  soundName - name of the sound to be created
	//  entity - the audio entity to be associated with this sound
	//  soundReference - a ptr to a member variable in the creating audio entity that points to the created sound
	//  initParams - a structure containing various creation parameters
	void GetPersistentSoundInstance(const char *soundName, audEntity* entity, audSound** soundReference, const audSoundInitParams* initParams);
	void GetPersistentSoundInstance(const u32 soundNameHash, audEntity* entity, audSound** soundReference, const audSoundInitParams* initParams);
	void GetPersistentSoundInstance(const audMetadataRef soundMetadataRef, audEntity* entity, audSound** soundReference, const audSoundInitParams* initParams);

	// PURPOSE
	//  Works through the controller's list of sounds, and immediately stops them, requesting no further updates. 
	//  Sound pts are nulled in associated audEntities, with no finishing notification (could change that?).
	//  This should never be called from game code, instead (if you really need this!) use SetActive(false) to 
	//  disable the controller.
	void ClearAllSounds();
	void ClearAllSounds(audControllerEntityRef& entityRef);

	// PURPOSE
	//  Dump all our sounds and go inactive, or start allowing new sound requests again.
	// PARAMS
	//  active - true to activate the controller, false to deactivate it.
	void SetActive(bool active);

	// PURPOSE
	//  Returns whether or not this audController is currently active and accepting new, and updating existing, sounds.
	// RETURNS
	//  true if active.
	bool IsActive();

#if __DEV
	u32 ComputeNumFreeEntries() const;
	u32 ComputeNumAllocatedEntries() const;
	u32 ComputeNumAllocatedEntries(const audControllerEntityRef& entRef) const;
#endif
    
#if __BANK
	static void AddWidgets(bkBank &bank);
#endif

	// PURPOSE
	//	To determine the number of currently active controllers
	// RETURNS
	//  The number of active controllers.
	static u32 GetNumberOfActiveControllers();

	// PURPOSE
	//  Sets whether audController will update environment groups.
	//  This is overridden by -audioocclusion or -noaudioocclusion command line flags.
	// PARAM
	//  enable - true to update environment groups, false to not bother.
	static void UpdateEnvironmentGroups(bool update);

	// PURPOSE
	//  Returns true if audControllers should update environment groups, false if not.
	static bool AreEnvironmentGroupsUpdated()
	{
		return sm_ShouldUpdateEnvironmentGroups;
	}

	void SetEnvironmentGroupManager(audEnvironmentGroupManager* manager)
	{
		m_EnvironmentGroupManager = manager;
	}

	audEnvironmentGroupManager* GetEnvironmentGroupManager()
	{
		return m_EnvironmentGroupManager;
	}

	audEnvironmentGroupInterface** GetEnvironmentGroupList()
	{
		return m_EnvironmentGroupList;
	}

	sysCriticalSectionToken& GetEnvironmentLock()
	{
		return m_LockEnvironment;
	}

	u32 GetEnvironmentGroupHeadIndex() const
	{
		return m_EnvironmentListHead;
	}
	u32 GetNumEnvironmentGroupsInPool() const
	{
		return m_NumUsedEnvironment;
	}

	// This is public because the actual use of the metric is in project-specific environment groups, we just define this
	// here so it's next to ShouldUpdateEnvironmentGroup
	static bool sm_ShouldUseEnvironmentMetric;
	static bool sm_ShouldUpdateEnvironmentGroups;
	void DebugPrintControllerSoundList() const;
	void DebugPrintControllerSoundList(const audControllerEntityRef& entRef) const;

#if __ASSERT
	void SetThreadVerifier(const audControllerThreadVerifier *verifier) { m_ThreadVerifier = verifier; }
	const audControllerThreadVerifier *GetThreadVerifier() const { return m_ThreadVerifier; }
#endif
	
private:

	void FreeSoundRef(const u32 soundId);
	u32 AllocateFreeSoundRef();

	// PURPOSE
	//  Are we allowed to accept new sound requests from audEntities. 
	// RETURNS
	//  true if new requests are allowed
	bool AreNewSoundRequestsAllowed();

	// PURPOSE
	//	Increments the number of active controllers in a thread safe way
	static void IncrementNumActiveControllers();

	// PURPOSE
	//	Decrements the number of active controllers in a thread safe way
	static void DecrementNumActiveControllers();

	// PURPOSE
	 //Maintain a list of all audSounds currently triggered from this thread
	audControllerSoundRef m_SoundListStore[MAX_CONTROLLER_SOUNDS];

	// PURPOSE
	 //Maintain a list of all audEntities currently running in this thread
	audControllerEntityRef m_EntityList[MAX_AUDIO_ENTITIES];


	// PURPOSE
	//Store all the free entity indices for O(1) insertion / deletion
	u16	m_FreeEntityIndexStack[MAX_AUDIO_ENTITIES];

	// PURPOSE
	//Maintain a list of all audEnvironmentGroupInterface currently managed in this thread
	audEnvironmentGroupInterface* m_EnvironmentGroupList[MAX_ENVIRONMENT_GROUPS];

	// PURPOSE
	//Store all the free environment indices for O(1) insertion / deletion
	u16	m_FreeEnvironmentIndexStack[MAX_AUDIO_ENTITIES];

	// PURPOSE
	// ptr to the (likely game-specific) function to call to update this controller's environment groups
	audEnvironmentGroupManager* m_EnvironmentGroupManager;

	ASSERT_ONLY(const audControllerThreadVerifier *m_ThreadVerifier);

	audControllerEntityRef m_OrphanEntityRef;

	u32 m_FirstFreeSound;

	sysCriticalSectionToken m_LockEntities;
	sysCriticalSectionToken m_LockEnvironment;

	u16 m_EntityListHead;
	u16	m_NumUsedEntities;
	u16 m_EnvironmentListHead;
	u16	m_NumUsedEnvironment;

	// PURPOSE
	 // Don't do anything other than initialise if we're not initialised yet.
	bool m_IsInitialized;

	// PURPOSE
	 // Unset this during resets and the like, when we're clearing all sounds out, and the audio thread's dead
	bool m_IsActive;

	// PURPOSE
	//	Set to true when iterating over the entity list, at which point it is not valid to register or unregister entities
	bool m_IsLocked;

	static bool sm_ShouldBeActive;

	// PURPOSE
	//	This stores the number of currently active controllers
	static u32					sm_NumActiveControllers;
};

} // namespace rage


#endif // AUD_CONTROLLER_H
