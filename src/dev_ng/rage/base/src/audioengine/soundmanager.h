//
// audioengine/soundmanager.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SOUND_MANAGER_H
#define AUD_SOUND_MANAGER_H

#include "atl/array.h"
#include "atl/pool.h"
#include "bank/bank.h"
#include "system/spinlock.h"
#if __PS3
#include "system/taskheader.h"
#endif

#include "enginedefs.h"
#include "soundfactory.h"
#include "soundpool.h"
#include "tracker.h"
#include "widgets.h"

namespace rage {

// PURPOSE
//  The SoundManager runs in the audio thread, and manages the pool of active sounds. It manages the lifecycle 
//  of the sounds, calling AudioPlay/Update/Stop on them as appropriate, and setting their play state in response 
//  to game requests. It also deals with deleting sounds when they have been freed by any game entities. 
class audSoundManager
{
public:
	audSoundManager();
	~audSoundManager();

	// PURPOSE
	//  Statically initialize the class
	static bool InitClass();

	// PURPOSE
	//  Statically shutdown the class
	static void ShutdownClass();

	void InitCommandBuffers();


	// PURPOSE
	//  Initialize the object
	bool Init();

	// PURPOSE
	//  Shuts down all active sounds
	void Shutdown();

	// PURPOSE
	// Main audio-thread update loop, to manage each Sound
	void Update(u32 timeInMs);

	// PURPOSE
	//  Gets the address of the named (well, hashed) variable. Use this for global variables, not ones defined in a
	//  VariableBlockSound.
	f32* GetVariableAddress(const u32 nameHash);

	// PURPOSE
	//  Gets the address of the named variable. Use this for global variables, not ones defined in a
	//  VariableBlockSound.
	f32* GetVariableAddress(const char *name);

	float GetVariableByIndex(const u32 index) const { return m_Variables[index].value; }

	// PURPOSE
	//  Sets the named (hashed) variable to this value. Creates that variable if it doesn't exist. Use this for global variables, not ones defined in a
	//  VariableBlockSound.
	// RETURNS the address of the variable if successfully set, so you can set it directly later
	f32* SetVariableValue(const u32 nameHash, const f32 value);

	// PURPOSE
	//  Sets the named variable to this value. Creates that variable if it doesn't exist. Use this for global variables, not ones defined in a
	//  VariableBlockSound.
	// RETURNS the address of the variable if successfully set, so you can set it directly later
	f32* SetVariableValue(const char *name, const f32 value);

#if __BANK
	// PURPOSE
	//  Displays the names and values of all the global variables
	void DebugDrawVariableValues();
	void DebugDrawSounds();
	static bool ShouldMuteSound(audSound* sound);
	static bool MatchesNameFilter(audSound* sound);
	static bool MatchesNameFilter(const char* soundName, audEntity* parentEntity);
	static bool ShouldBreakOnStop(audSound* sound);
	static bool ShouldBreakOnCreate(audSound* sound);
	static bool ShouldBreakOnCreate(audMetadataRef soundRef, audEntity* parentEntity);
#endif

	// PURPOSE
	//  Pauses the audio engine - pausing all pausable sounds. If audio engine is already paused, this will have no effect,
	//  hence it is safe to call Pause() every frame while the game is paused.
	void Pause(const u32 timerId)
	{
		m_Timers[timerId].isPaused = true;
	}

	// PURPOSE
	//  Unpauses the audio engine - unpausing all pausable sounds. If audio engine is not paused, this will have no effect,
	//  hence it is safe to call UnPause() every frame while the game is not paused.
	void UnPause(const u32 timerId)
	{
		m_Timers[timerId].isPaused = false;
	}

	bool IsPaused(const u32 timerId) const
	{
		return m_Timers[timerId].isPaused;
	}

	u32 GetTimeInMilliseconds(const u32 timerId) const
	{
		// convert to ms
		return m_Timers[timerId].timeInMs;
	}


	// PURPOSE
	//	This alters the speed of time, 1 = real time
	void SetTimeScale(const u32 timerId, const f32 timeScale)
	{
		m_Timers[timerId].timeScale = timeScale;
	}

	f32 GetTimeScale(const u32 timerId) const
	{
		return m_Timers[timerId].timeScale;
	}

#if __BANK && !__SPU
	void AddWidgets(bkBank &bank);
	static void AddSoundDebuggingWidgets(bkBank &bank);
	void DrawOverriddenSounds() const
	{
		return m_SoundFactory.DrawOverriddenSounds();
	}
#endif

	static void ProcessHierarchy(audEngineContext *context, audSound *parentSound);

	audSoundFactory &GetFactory()
	{
		return m_SoundFactory;
	}

	u32 GetGameUpdateIndex() const { return m_GameUpdateIndex; }
	void CommitGameSettings();

	void SetVariableAlias(const u32 variableNameHash, const u32 aliasNameHash);

	float GetMasterRMSSmoothed() const { return m_MasterRMSSmoother.GetLastValue(); }

	void StartUnloadingChunk(const char *chunkName);
	void CancelUnloadingChunk(const char *chunkName);
	void ClearDisabledMetadata()
	{
		m_DisabledMetadataPtr = NULL;
		m_DisabledMetadataSize = 0;
	}

	bool IsMetadataDisabled(const void *metadataPtr)
	{
		return (metadataPtr >= m_DisabledMetadataPtr && metadataPtr < m_DisabledMetadataPtr+m_DisabledMetadataSize);	
	}

	bool IsAnyChunkBeingUnloaded() const
	{
		return m_DisabledMetadataPtr != NULL;
	}

private:

	// PURPOSE
	//  Array of variables, accessible by game code and logic sounds
	atRangeArray<variableNameValuePair, g_MaxSoundManagerVariables>  m_Variables ;
	atRangeArray<audTimerState, g_NumAudioTimers> m_Timers ;
	atRangeArray<u32, g_MaxSoundManagerVariables> m_VariableAliases;

	audSoundFactory m_SoundFactory;	

	audSimpleSmootherDiscrete m_MasterRMSSmoother;
	audSimpleSmootherDiscrete m_SfxRMSSmoother;
	f32 *m_SFXRMSSmoothedVariable, *m_MasterRMSSmoothedVariable;
	f32 *m_SFXRMSVariable, *m_MasterRMSVariable;
	u32 m_LastUpdateTime;

	u32 m_NumSoundsDormant;
	u32 m_NumSoundsPreparing;
	u32 m_NumSoundsWaitingToBeDeleted;
	u32 m_NumSoundsPlaying;

	const u8* m_DisabledMetadataPtr;
	u32 m_DisabledMetadataSize;

	u32 m_GameUpdateIndex;
} ;

} // namespace rage

#endif // AUD_SOUND_MANAGER_H
