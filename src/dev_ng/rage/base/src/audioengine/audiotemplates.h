// 
// sagaudio/AudioTemplates.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// The following useful templates are not necessarily audio specific and can be used by anyone.

#ifndef SAGAUDIO_AUDIOTEMPLATES_H 
#define SAGAUDIO_AUDIOTEMPLATES_H 

#include "diag/trap.h"
#include "system/debugmemoryfill.h"

namespace rage {


////////////////////////////////////////////////////////////////
// atPersistentPool

//
// PURPOSE:
//	Simply contains an owned array of objects that are constructed when the pool is.
//	The user will be responsible for knowing when the objects are available or not.
template <class _Type, rage::u32 _MaxEntries>
class atPersistentPool
{
public:	
	// Default constructor
	atPersistentPool()
	{
		m_NumUsedEntries = 0;

		//	Sequence the indices
		for (u32 i = 0; i < _MaxEntries; ++i)
		{
			m_FreeIndexStack[i] = (u16)i;
		}
	}

	// PURPOSE: Get the max number of entries that can be in the pool
	// RETURN : the max number of entries that can be in the pool
	u32 GetMaxEntries() const { return _MaxEntries; }

	// PURPOSE: Get the number of entries currently in use
	// RETURN : the number of entries currently in use
	u32 GetNumUsedEntries() const { return m_NumUsedEntries; }

	// PURPOSE: Gets a pointer to the next instance marked as free
	// RETURN : the pointer to the next free entry, or NULL if all are in use
	_Type* GetFreeInstance()
	{
		if (m_NumUsedEntries != _MaxEntries)
		{
			Assert(m_FreeIndexStack[m_NumUsedEntries] != 0xffff);
			TrapEQ(m_FreeIndexStack[m_NumUsedEntries], 0xffff);
			_Type* pType = &m_Pool[ m_FreeIndexStack[m_NumUsedEntries] ];
			m_FreeIndexStack[m_NumUsedEntries] = 0xffff; // Mark this one as used for debug purposes
			++m_NumUsedEntries;
			return pType;
		}
		return NULL;
	}

	// PURPOSE: Gets a pointer to the next instance marked as free, but doesn't mark it as used yet
	// RETURN : the pointer to the next free entry, or NULL if all are in use
	_Type* GetNextFreeInstance()
	{
		if (m_NumUsedEntries != _MaxEntries)
		{
			Assert(m_FreeIndexStack[m_NumUsedEntries] != 0xffff);
			TrapEQ(m_FreeIndexStack[m_NumUsedEntries], 0xffff);
			_Type* pType = &m_Pool[ m_FreeIndexStack[m_NumUsedEntries] ];
			return pType;
		}
		return NULL;
	}

	// PURPOSE: When you're finished using one particular instance of an object, free it up here
	// NOTES  : Pretty much assumes that you know what you're doing, and not trying to return
	//			an instance more than once, or a pointer to an object outside the bounds of our array,
	//			although there are some assertions to warn you if you might be.
	void ReturnInstance(_Type* pInstance)
	{
		TrapGT((unsigned)pInstance, (unsigned)&m_Pool[_MaxEntries-1]);
		TrapLT((unsigned)pInstance, (unsigned)&m_Pool[0]);
		Assert(m_NumUsedEntries != 0);
		if (m_NumUsedEntries != 0)
		{
			--m_NumUsedEntries;
			TrapNE(m_FreeIndexStack[m_NumUsedEntries], 0xffff);
			m_FreeIndexStack[m_NumUsedEntries] = u16(pInstance - &m_Pool[0]);
		}
	}

	// PURPOSE: Useful when you want to take control of the contained objects directly
	_Type& operator[](unsigned uIndex)
	{
		TrapGE(uIndex, _MaxEntries);
		return m_Pool[uIndex];
	}
	const _Type& operator[](unsigned uIndex) const
	{
		TrapGE(uIndex, _MaxEntries);
		return m_Pool[uIndex];
	}

#if __BANK
	//	Return the address of the number of used entries. USE CAUTION! Meant for widgets, but do NOT change this value!
	const u16* GetNumUsedEntriesPtr() const { return &m_NumUsedEntries; }
#endif

private:
	_Type	m_Pool[_MaxEntries];
	u16		m_FreeIndexStack[_MaxEntries];
	u16		m_NumUsedEntries;
};


////////////////////////////////////////////////////////////////
// atFixedMemoryPool

//
// PURPOSE:
//	A memory pool of fixed size known at compile time.
//	All objects in the pool will be allocated a single fixed memory size,
//	so _AllocSize must be greater than or equal to the largest inherited class.
//	We force the pool start alignment to 16 because we have no way of
//	knowing the natural alignment of the allocation unless we pass it in,
//	but 16 should cover all natural cases.
template <class _BaseType, u32 _MaxEntries, u32 _AllocSize=sizeof(_BaseType), bool _AllowPlacementConstructor=true>
class atFixedMemoryPool
{
public:
	// Default constructor
	atFixedMemoryPool()
	{
		CompileTimeAssert(_AllocSize >= (u32)sizeof(_BaseType));
		m_NumUsedEntries = 0;

		//	Sequence the indices
		for (u32 i = 0; i < _MaxEntries; ++i)
		{
			m_FreeIndexStack[i] = (u16)i;
		}

		//	0xad will be the audio signature
		IF_DEBUG_MEMORY_FILL_N(sysMemSet(m_Pool, 0xad, _AllocSize*_MaxEntries),DMF_AUDIOPOOL);
	}

	// PURPOSE: Gets a pointer to the next free memory space. If _AllowPlacementConstructor
	//			is set to true, the default constructor of the inherited type will automatically be called.
	// RETURN : Pointer to the newly allocated memory, or NULL if the pool is full
	template <class _InheritedType> _BaseType* CreateObject()
	{
		CompileTimeAssert(_AllocSize >= (u32)sizeof(_InheritedType));

		if (m_NumUsedEntries != _MaxEntries)
		{
			TrapGE((u32)m_NumUsedEntries, _MaxEntries);
			TrapGE((u32)m_FreeIndexStack[m_NumUsedEntries], _MaxEntries);
			_BaseType* pType = reinterpret_cast<_BaseType*>(&m_Pool[ m_FreeIndexStack[m_NumUsedEntries] ]);
			m_FreeIndexStack[m_NumUsedEntries] = 0xffff; // Mark this one as used for debug purposes
			++m_NumUsedEntries;
			if (_AllowPlacementConstructor)
			{
				::new(pType) _InheritedType;
			}
			return pType;
		}
		return NULL;
	}

	// PURPOSE: Pretty much much the same as the templated _InheritedType
	//			version except it assumes _BaseType and does not require
	//			the template class arg.
	_BaseType* CreateObject()
	{
		return CreateObject<_BaseType>();
	}

	// PURPOSE: Marks the allocated space at the pointer location as free.
	//			Calls its destructor if _AllowPlacementConstructor is true.
	// NOTES  : Pretty much assumes that you know what you're doing, and not trying to return
	//			an instance more than once, or a pointer to an object outside the bounds of our array,
	//			although there are some assertions to warn you if you might be.
	void DestroyObject(_BaseType* pInstance)
	{
		TrapGT((unsigned)pInstance, (unsigned)&m_Pool[_MaxEntries-1]);
		TrapLT((unsigned)pInstance, (unsigned)&m_Pool[0]);
		Assert(m_NumUsedEntries != 0);
		if (m_NumUsedEntries != 0)
		{
			--m_NumUsedEntries;
			TrapNE(m_FreeIndexStack[m_NumUsedEntries], 0xffff);
			m_FreeIndexStack[m_NumUsedEntries] = u16((tAllocStub*)pInstance - m_Pool);
			if (_AllowPlacementConstructor)
			{
				pInstance->~_BaseType();
			}
			//	0xad will be the audio signature
			IF_DEBUG_MEMORY_FILL_N(sysMemSet(pInstance, 0xad, _AllocSize),DMF_AUDIOPOOL);
		}
	}

	// RETURNS:	True if this is a valid heap pointer owned by this heap.
	// Note that this only performs a range check to see if this is within
	// our heap, as if this pointer has already been freed, it will still
	// return true as long as it's within the heap bounds.
	bool IsValidPointer(_BaseType* pInstance) const
	{
		return (pInstance >= &m_Pool[0]) && (pInstance <= &m_Pool[_MaxEntries-1]);
	}

	// RETURN : true if pool is full
	bool GetIsFull() const { return m_NumUsedEntries != _MaxEntries; }

	// RETURN : The Fixed allocation size
	u32 GetAllocSize() const { return _AllocSize; }

	// PURPOSE: Get the number of entries currently in use
	// RETURN : the number of entries currently in use
	u32 GetNumUsedEntries() const { return m_NumUsedEntries; }

	// PURPOSE: Get the number of entries remaining that can be allocated
	// RETURN : the number of entries currently in use
	u32 GetNumEntriesAvailable() const { return _MaxEntries - m_NumUsedEntries; }

	// PURPOSE: Get the amount of memory currently consumed so far
	// RETURN : Memory in bytes
	u32 GetMemoryUsed() const
	{
		return m_NumUsedEntries*_AllocSize;
	}

	// PURPOSE: Get the amount of memory remaining in the pool
	// RETURN : Memory in bytes
	u32 GetMemoryAvailable() const
	{
		return GetNumEntriesAvailable()*_AllocSize;
	}

	// PURPOSE: Get the max number of entries that can be in the pool
	// RETURN : the max number of entries that can be in the pool
	u32 GetMaxEntries() const
	{
		return _MaxEntries;
	}

	// PURPOSE: Get the total size of the heap
	// RETURN : Memory in bytes
	u32 GetHeapSize() const
	{
		return _MaxEntries*_AllocSize;
	}

#if __BANK
	//	Return the address of the number of used entries. USE CAUTION! Meant for widgets, but do NOT change this value!
	const u16* GetNumUsedEntriesPtr() const { return &m_NumUsedEntries; }
#endif

private:
	struct tAllocStub { u8 m_Bytes[_AllocSize]; };

	tAllocStub	ALIGNAS(16) m_Pool[_MaxEntries] ;
	u16			m_FreeIndexStack[_MaxEntries];
	u16			m_NumUsedEntries;
};


//
// PURPOSE:
//	A memory pool of fixed size NOT known at compile time.
//	All objects in the pool will be allocated a single fixed memory size,
//	so _AllocSize must be greater than or equal to the largest inherited class.
template <class _BaseType, bool _AllowPlacementConstructor=true>
class atFixedMemoryPoolRuntimeSized
{
public:
	
	//	Default constructor
	atFixedMemoryPoolRuntimeSized() :
	m_Pool(NULL),
	m_FreeIndexStack(NULL),
	_AllocSize(0),
	m_NumUsedEntries(0),
	_MaxEntries(0)
	{
#if __BANK
		m_uHighWaterMark = 0;
#endif
	}
	
	//	You must pass down a pointer to your memory heap which needs to be pre-sized
	//	to be _AllocSize * count, and pFreeIndexStackBase must be an array of u16's
	//	of size count.
	atFixedMemoryPoolRuntimeSized(void* pHeapBase, u16* pFreeIndexStackBase, u32 allocSize, u16 count)
	{
		Init(pHeapBase, pFreeIndexStackBase, allocSize, count);
	}

	//	You must pass down a pointer to your memory heap which needs to be pre-sized
	//	to be _AllocSize * count, and pFreeIndexStackBase must be an array of u16's
	//	of size count.
	void Init(void* pHeapBase, u16* pFreeIndexStackBase, u32 allocSize, u16 count)
	{
		m_NumUsedEntries = 0;

		m_Pool = (u8*)pHeapBase;
		m_FreeIndexStack = pFreeIndexStackBase;
		_AllocSize = allocSize;
		_MaxEntries = count;
#if __BANK
		m_uNumAllocs = 0;
		m_uNumFrees = 0;
		m_uHighWaterMark = 0;
#endif
		TrapLT(_AllocSize, (u32)sizeof(_BaseType));
		
		//	Sequence the indices
		for (u32 i = 0; i < _MaxEntries; ++i)
		{
			m_FreeIndexStack[i] = (u16)i;
		}

		//	0xad will be the audio signature
		IF_DEBUG_MEMORY_FILL_N(sysMemSet(m_Pool, 0xad, _AllocSize*_MaxEntries),DMF_AUDIOPOOL);
	}

	// PURPOSE: Gets a pointer to the next free memory space. If _AllowPlacementConstructor
	//			is set to true, the default constructor of the inherited type will automatically be called.
	// RETURN : Pointer to the newly allocated memory, or NULL if the pool is full
	template <class _InheritedType> _BaseType* CreateObject()
	{
		if (m_NumUsedEntries != _MaxEntries)
		{
			TrapGE((u32)m_NumUsedEntries, (u32)_MaxEntries);
			TrapGE((u32)m_FreeIndexStack[m_NumUsedEntries], (u32)_MaxEntries);
			_BaseType* pType = reinterpret_cast<_BaseType*>(&m_Pool[ _AllocSize*m_FreeIndexStack[m_NumUsedEntries] ]);
			m_FreeIndexStack[m_NumUsedEntries] = 0xffff; // Mark this one as used for debug purposes
			++m_NumUsedEntries;
			if (_AllowPlacementConstructor)
			{
				::new(pType) _InheritedType;
			}
#if __BANK
			m_uHighWaterMark = Max(m_uHighWaterMark, m_NumUsedEntries);
			++m_uNumAllocs;
#endif
			return pType;
		}
		return NULL;
	}

	// PURPOSE: Pretty much much the same as the templated _InheritedType
	//			version except it assumes _BaseType and does not require
	//			the template class arg.
	_BaseType* CreateObject()
	{
		return CreateObject<_BaseType>();
	}

	// PURPOSE: Marks the allocated space at the pointer location as free.
	//			Calls its destructor if _AllowPlacementConstructor is true.
	// NOTES  : Pretty much assumes that you know what you're doing, and not trying to return
	//			an instance more than once, or a pointer to an object outside the bounds of our array,
	//			although there are some assertions to warn you if you might be.
	void DestroyObject(_BaseType* pInstance)
	{
		TrapGT((unsigned)pInstance, (unsigned)&m_Pool[_AllocSize*(_MaxEntries-1)]);
		TrapLT((unsigned)pInstance, (unsigned)m_Pool);
		TrapEQ((unsigned)m_NumUsedEntries, 0);
		--m_NumUsedEntries;
		TrapNE(m_FreeIndexStack[m_NumUsedEntries], 0xffff);
		m_FreeIndexStack[m_NumUsedEntries] = u16( ((u8*)pInstance - m_Pool) / _AllocSize);
		if (_AllowPlacementConstructor)
		{
			pInstance->~_BaseType();
		}
#if __BANK
		++m_uNumFrees;
#endif
		//	0xad will be the audio signature
		IF_DEBUG_MEMORY_FILL_N(sysMemSet(pInstance, 0xad, _AllocSize),DMF_AUDIOPOOL);
	}

	// RETURNS:	True if this is a valid heap pointer owned by this heap.
	// Note that this only performs a range check to see if this is within
	// our heap, as if this pointer has already been freed, it will still
	// return true as long as it's within the heap bounds.
	bool IsValidPointer(const _BaseType* pInstance) const
	{
		return ((u8*)pInstance >= m_Pool) && ((u8*)pInstance < &m_Pool[_AllocSize*_MaxEntries]);
	}

	// RETURNS:	0 if the passed in pointer can be valid in this heap
	//			+1 if it's somewhere greater than the heap bounds
	//			-1 if it's somewhere lower than the heap bounds
	int GetPointerInRange(const _BaseType* pInstance) const
	{
		return (u8*)pInstance < m_Pool ? -1 : (u8*)pInstance >= &m_Pool[_AllocSize*_MaxEntries] ? +1 : 0;
	}

	// RETURN : true if pool is full
	bool GetIsFull() const { return m_NumUsedEntries != _MaxEntries; }

	// RETURN : The Fixed allocation size
	u32 GetAllocSize() const { return _AllocSize; }
	
	// PURPOSE: Get the number of entries currently in use
	// RETURN : the number of entries currently in use
	u32 GetNumUsedEntries() const { return m_NumUsedEntries; }

	// PURPOSE: Get the number of entries remaining that can be allocated
	// RETURN : the number of entries currently in use
	u32 GetNumEntriesAvailable() const { return _MaxEntries - m_NumUsedEntries; }

	// PURPOSE: Get the amount of memory currently consumed so far
	// RETURN : Memory in bytes
	u32 GetMemoryUsed() const
	{
		return m_NumUsedEntries*_AllocSize;
	}

	// PURPOSE: Get the amount of memory remaining in the pool
	// RETURN : Memory in bytes
	u32 GetMemoryAvailable() const
	{
		return GetNumEntriesAvailable()*_AllocSize;
	}

	// PURPOSE: Get the max number of entries that can be in the pool
	// RETURN : the max number of entries that can be in the pool
	u32 GetMaxEntries() const
	{
		return _MaxEntries;
	}

	// PURPOSE: Get the total size of the heap
	// RETURN : Memory in bytes
	u32 GetHeapSize() const
	{
		return _MaxEntries*_AllocSize;
	}

#if __BANK
	//	Return the address of the number of used entries. USE CAUTION! Meant for widgets, but do NOT change this value!
	const u16* GetNumUsedEntriesPtr() const { return &m_NumUsedEntries; }
#endif

private:
	u8*			m_Pool;
	u16*		m_FreeIndexStack;
	u32			_AllocSize;
	u16			m_NumUsedEntries;
	u16			_MaxEntries;

#if __BANK
	u32			m_uNumAllocs;
	u32			m_uNumFrees;
	u16			m_uHighWaterMark;
#endif
};


} // namespace rage

#endif // SAGAUDIO_AUDIOTEMPLATES_H 
