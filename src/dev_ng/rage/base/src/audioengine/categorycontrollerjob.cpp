// 
// audioengine/categorycontrollerjob.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __SPU

#include "vector/vector3_consts_spu.cpp"

#include "audiohardware/driverutil.h"
#include "system/dma.h"

#include "categorycontrollerjob.h"
#include "categorycontroller.cpp"

using namespace rage;

void CategoryControllerJob(sysTaskParameters &p)
{
	// User data
	const f32 fTimeStep = p.UserData[0].asFloat;	
	void *pControllerOutputArrayEA = p.UserData[1].asPtr;
	const u32 numCategories = p.UserData[2].asUInt;
	void* pControllerSettingsPoolEA = p.UserData[3].asPtr;
	const u32 uConrollerSettingsSize = p.UserData[4].asUInt;

	//	audCategoryController pool comes in as an input and we don't write to it
	//	Settings pool also comes in as an input, but we write to it, and DMA it back
	audCategoryControllerPools* pPools = static_cast<audCategoryControllerPools*>(p.Input.Data);
	
	audCategoryController* pControllerPool = reinterpret_cast<audCategoryController*>(&pPools->m_ControllerPool);
	audCategoryControllerSettings* pControllerSettingsPool = reinterpret_cast<audCategoryControllerSettings*>(&pPools->m_ControllerSettingsPool);

	//	Re-set our static global pool pointers
	audCategoryController::SetPools(pControllerPool, pControllerSettingsPool);

	// Read Only
	const s16* pControllerListIndexArray = (s16*)p.ReadOnly[0].Data;

	// Scratch data for category output results
	audCategoryControllerOutput *categoryOutputs = (audCategoryControllerOutput*)p.Scratch.Data;

	// update all controllers
	for(u32 i = 0; i < numCategories; i++)
	{
		audCategoryControllerOutput& results = categoryOutputs[i];
		if (pControllerListIndexArray[i] != -1)
			audCategoryController::ComputeOutput(results, pControllerListIndexArray[i], fTimeStep);
		else
			results.SetDefaults();
	}

	const s32 TAG = 1;

	//	Output size is 12 bytes, we need 16 byte alignment, so need to have a multiple of 4 outputs
	const u32 numCategoriesAligned = (numCategories+15) & ~15;
	sysDmaPut(categoryOutputs, (uint64_t)pControllerOutputArrayEA, numCategoriesAligned * sizeof(audCategoryControllerOutput), TAG);
	
	//	Copy our new controller settings back over the pipe
	sysDmaPut(pControllerSettingsPool, (uint64_t)pControllerSettingsPoolEA, uConrollerSettingsSize, TAG);

	sysDmaWaitTagStatusAll(1<<TAG);
}


#endif // __SPU

