//
// audioengine/spuutil.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SPUUTIL_H
#define AUD_SPUUTIL_H

#if __SPU

#include "system/dma.h"
#include <cell/dma.h>
#include <cell/atomic.h>

namespace rage
{
	u8 *g_ScratchBuf = NULL;
	size_t g_ScratchLeft = 0;

	u8 *g_ScratchBufBookmark = NULL;
	size_t g_ScratchLeftBookmark = 0;

	BANK_ONLY(bool g_DisplayAllocations = false);

	void InitScratchBuffer(void *p, size_t size)
	{
		// align to 16 bytes
		g_ScratchBuf = (u8*)(((size_t)p + 15) & ~15);
		g_ScratchLeft = size - ((size_t)g_ScratchBuf-(size_t)p);

		g_ScratchBufBookmark = NULL;
		g_ScratchLeftBookmark = 0;
	}

	void SetScratchBookmark()
	{
		g_ScratchLeftBookmark = g_ScratchLeft;
		g_ScratchBufBookmark = g_ScratchBuf;
	}

	void ResetScratchToBookmark()
	{
		TrapZ((size_t)g_ScratchBufBookmark);
		TrapZ(g_ScratchLeftBookmark);
		g_ScratchBuf = g_ScratchBufBookmark;
		g_ScratchLeft = g_ScratchLeftBookmark;
	}

	const int DEFAULT_TAG = 12;
	void blockOnDmaOperation(bool isPut, void *localPtr, const void *mainPtr, const size_t size, const bool shouldBlock, const u32 tag)
	{
		TrapNZ((size & 15));
		TrapNZ((reinterpret_cast<size_t>(localPtr) & 15));
		TrapNZ((reinterpret_cast<size_t>(mainPtr) & 15));

		// 0-byte DMAs and DMAs to/from NULL are indicative of an error
		TrapZ(reinterpret_cast<size_t>(localPtr));
		TrapZ(reinterpret_cast<size_t>(mainPtr));
		TrapZ(size);
		
		if(size > 0x4000)
		{
			if(isPut)
			{
				sysDmaLargePut(localPtr, (uint64_t)mainPtr, size, tag);
			}
			else
			{
				sysDmaLargeGet(localPtr, (uint64_t)mainPtr, size, tag);
			}
		}
		else
		{
			if(isPut)
			{
				sysDmaPut(localPtr, (uint64_t)mainPtr, size, tag);
			}
			else
			{
				sysDmaGet(localPtr, (uint64_t)mainPtr, size, tag);
			}
		}

		if(shouldBlock)
		{
			sysDmaWaitTagStatusAll(1<<tag);
		}
	}

	void dmaWait(const u32 tag = DEFAULT_TAG)
	{
		sysDmaWaitTagStatusAll(1<<tag);
	}

	void dmaPut(void *localPtr, const void *mainPtr, u32 size, bool shouldBlock = true, u32 tag = DEFAULT_TAG)
	{
		blockOnDmaOperation(true, localPtr, mainPtr, size, shouldBlock, tag);
	}

	void dmaGet(void *localPtr, const void *mainPtr, u32 size, bool shouldBlock = true, u32 tag = DEFAULT_TAG)
	{
		blockOnDmaOperation(false, localPtr, mainPtr, size, shouldBlock, tag);
	}

	void dmaUnalignedGet(void *tempBuffer, const size_t tempBufferSize, void *destBuffer, const size_t fetchSize, const u32 unalignedEA)
	{		
		const u32 alignedEA = unalignedEA&~15;
		const u32 padding = unalignedEA - alignedEA;

		const size_t dmaSizeBytes = (15 + padding + fetchSize) & ~15;
		TrapLT((u32)tempBufferSize, (u32)dmaSizeBytes);
		sysDmaGetAndWait(tempBuffer, alignedEA, dmaSizeBytes, 6);

		sysMemCpy(destBuffer, (u8*)tempBuffer + padding, fetchSize);
	}

	void *AllocateFromScratch(const size_t size, const char *message)
	{
		if(g_ScratchLeft>=size)
		{
			void *ret = g_ScratchBuf;
			g_ScratchLeft -= size;
			g_ScratchBuf += size;

#if __BANK
			if(g_DisplayAllocations)
			{
				audDisplayf("AllocateFromScratch: %s %zu, scratchLeft: %zu", message, size, g_ScratchLeft);
			}
#endif
			return ret;
		}
		else
		{
			audErrorf("SPU AllocateFromScratch for %s requested %zu with only %zu free", message, size, g_ScratchLeft);
			return NULL;
		}
	}

	void *AllocateFromScratch(const size_t size, const size_t align, const char *message)
	{
		size_t currentPtr = (size_t)g_ScratchBuf;
		size_t alignedPtr = (currentPtr+align-1)&~(align-1);
		size_t padding = alignedPtr - currentPtr;
		if(g_ScratchLeft >= size + padding)
		{
			if(padding)
			{
				AllocateFromScratch(padding, "(alignment padding)");
			}
			void *ret = AllocateFromScratch(size, message);
			AlignedAssert(ret, align);
			return ret;
		}
		else
		{
			audErrorf("SPU AllocateFromScratch for %s requested %zu with alignment %zu (%zu padding) with only %zu free", message, size, align, padding, g_ScratchLeft);
			return NULL;		
		}
	}

	template<class _T> _T*AllocateFromScratch(const size_t num, const size_t align, const char *message)
	{
		return reinterpret_cast<_T*>(AllocateFromScratch(num * sizeof(_T), align, message));
	}

	void *grabData(const void *ea, const size_t size, const char *message) 
	{
		void *ret = AllocateFromScratch(size, message);
		if(!ret)
		{
			return NULL;
		}
		dmaGet(ret, ea, size, false);
		return ret;
	}

	struct audAutoScratchBookmark
	{
		u8 *m_PrevBufBookmark;
		size_t m_PrevLeftBookmark;

		audAutoScratchBookmark()
		{
			m_PrevBufBookmark = g_ScratchBufBookmark;
			m_PrevLeftBookmark = g_ScratchLeftBookmark;
			SetScratchBookmark();
		}

		~audAutoScratchBookmark()
		{
			ResetScratchToBookmark();
			g_ScratchBufBookmark = m_PrevBufBookmark;
			g_ScratchLeftBookmark = m_PrevLeftBookmark;
		}
	};
}
#endif // __SPU
#endif // AUD_SPUUTIL_H

