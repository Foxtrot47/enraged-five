// 
// audioengine/compressedvolume.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOENGINE_COMPRESSEDVOLUME_H 
#define AUDIOENGINE_COMPRESSEDVOLUME_H 

#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "math/float16.h"

namespace rage {

	extern bool g_AssertOnVolumeClamp;
	class audCompressedVolume32;
	class audCompressedVolume16;
	class audCompressedVolume8;
	class audCompressedVolume32
	{
	public:

		audCompressedVolume32()
		{
			m_Data = 0.0f;
		}

		audCompressedVolume32(const f32 linearVolume)
		{
			m_Data = linearVolume;
		}

		audCompressedVolume32& operator=(const audCompressedVolume32 rhs)
		{
			m_Data = rhs.m_Data;
			return *this;
		}
		audCompressedVolume32& operator=(const audCompressedVolume16 &rhs);
		audCompressedVolume32& operator=(const audCompressedVolume8 &rhs);

		audCompressedVolume32& operator=(const f32 rhs)
		{
			m_Data = rhs;
			return *this;
		}

		bool operator<(const audCompressedVolume32 rhs) const
		{
			return m_Data < rhs.m_Data;
		}
		bool operator>(const audCompressedVolume32 rhs) const
		{
			return m_Data > rhs.m_Data;
		}
		bool operator<=(const audCompressedVolume32 rhs) const
		{
			return m_Data < rhs.m_Data;
		}
		bool operator>=(const audCompressedVolume32 rhs) const
		{
			return m_Data >= rhs.m_Data;
		}
		bool operator==(const audCompressedVolume32 rhs) const
		{
			return m_Data == rhs.m_Data;
		}
		bool operator!=(const audCompressedVolume32 rhs) const
		{
			return m_Data != rhs.m_Data;
		}


		// PURPOSE
		//	Sets this volume to the specified dB value
		void SetDb(const f32 dbVolume)
		{
			Assert(!g_AssertOnVolumeClamp || (dbVolume <= 0.0f));
			m_Data = audDriverUtil::ComputeLinearVolumeFromDb(dbVolume);
		}

		// PURPOSE
		//	Returns the set volumes in dBs
		f32 GetDb() const
		{
			return audDriverUtil::ComputeDbVolumeFromLinear(m_Data);
		}

		// PURPOSE
		//	Sets this volume to the specified linear value
		void SetLinear(const f32 linVolume)
		{
			Assert(!g_AssertOnVolumeClamp || (linVolume <= 1.0f));
			m_Data = linVolume;
		}

		// PURPOSE
		//	Returns a linear volume
		f32 GetLinear() const
		{
			return m_Data;
		}

		void ScaleLinear(const f32 scale)
		{
			m_Data *= scale;
		}

		// PURPOSE
		//	Returns a u32 representation of this volume suitable for comparison/sorting - ie larger is bigger
		u32 GetIntRepresentation() const
		{
			return static_cast<u32>(m_Data * 65535.f);
		}
	private:
		f32 m_Data;
	};

	// PURPOSE
	// This class encapsulates compressed volumes, and can be overridden per game
	// NOTES
	//	This implementation stores volumes as 16 bit linear values
	class audCompressedVolume16
	{
	public:

		audCompressedVolume16()
		{
			m_Data.SetFloat16_Zero();
		}

		audCompressedVolume16(const f32 linearVolume)
		{
			SetLinear(linearVolume);
		}

		audCompressedVolume16& operator=(const audCompressedVolume32 &rhs);
		audCompressedVolume16& operator=(const audCompressedVolume8 &rhs);

		audCompressedVolume16& operator=(const audCompressedVolume16 rhs)
		{
			m_Data = rhs.m_Data;
			return *this;
		}

		audCompressedVolume16& operator=(const f32 rhs)
		{
			SetLinear(rhs);
			return *this;
		}

		bool operator<(const audCompressedVolume16 rhs) const
		{
			return GetLinear() < rhs.GetLinear();
		}
		bool operator>(const audCompressedVolume16 rhs) const
		{
			return GetLinear() > rhs.GetLinear();
		}
		bool operator<=(const audCompressedVolume16 rhs) const
		{
			return GetLinear() < rhs.GetLinear();
		}
		bool operator>=(const audCompressedVolume16 rhs) const
		{
			return GetLinear() >= rhs.GetLinear();
		}
		bool operator==(const audCompressedVolume16 rhs) const
		{
			return GetLinear() == rhs.GetLinear();
		}
		bool operator!=(const audCompressedVolume16 rhs) const
		{
			return GetLinear() != rhs.GetLinear();
		}

		// PURPOSE
		//	Sets this volume to the specified dB value
		void SetDb(const f32 dbVolume)
		{
			Assert(!g_AssertOnVolumeClamp || (dbVolume <= 0.0f));
			SetLinear(audDriverUtil::ComputeLinearVolumeFromDb(dbVolume));
		}

		// PURPOSE
		//	Returns the set volumes in dBs
		f32 GetDb() const
		{
			return audDriverUtil::ComputeDbVolumeFromLinear(m_Data.GetFloat32_FromFloat16());
		}

		// PURPOSE
		//	Sets this volume to the specified linear value
		void SetLinear(f32 linVolume)
		{
			Assert(!g_AssertOnVolumeClamp || linVolume <= 1.0f);
			linVolume = Selectf(linVolume, linVolume, 0.0f);
			audAssertf(linVolume >= 0.0f && linVolume <= 63000.0f, "linVolume: %f", linVolume);
			m_Data.SetFloat16_FromFloat32(linVolume);
		}

		// PURPOSE
		//	Returns a linear volume
		f32 GetLinear() const
		{
			return m_Data.GetFloat32_FromFloat16();
		}

		void ScaleLinear(const f32 scale)
		{
			m_Data.SetFloat16_FromFloat32(m_Data.GetFloat32_FromFloat16()*scale);
		}

		// PURPOSE
		//	Returns a u32 representation of this volume suitable for comparison/sorting - ie larger is bigger
		u32 GetIntRepresentation() const
		{
			return static_cast<u32>(m_Data.GetFloat32_FromFloat16() * 65535.f); // could just return m_Data.GetBinaryData(), if value is non-negative
		}
	private:
		Float16 m_Data;
	};

	// PURPOSE
	// This class encapsulates compressed volumes, and can be overridden per game
	// NOTES
	//	This implementation stores volumes as 8bit FCL values
	class audCompressedVolume8
	{
	public:

		audCompressedVolume8()
		{
			m_PackedFCL = 0;
		}

		audCompressedVolume8(const f32 linearVolume)
		{
			SetLinear(linearVolume);
		}

		audCompressedVolume8& operator=(const audCompressedVolume8 rhs)
		{
			m_PackedFCL = rhs.m_PackedFCL;
			return *this;
		}

		audCompressedVolume8& operator=(const audCompressedVolume32 &rhs);
		audCompressedVolume8& operator=(const audCompressedVolume16 &rhs);

		audCompressedVolume8& operator=(const f32 rhs)
		{
			SetLinear(rhs);
			return *this;
		}

		bool operator<(const audCompressedVolume8 rhs) const
		{
			return GetLinear() < rhs.GetLinear();
		}
		bool operator>(const audCompressedVolume8 rhs) const
		{
			return GetLinear() > rhs.GetLinear();
		}
		bool operator<=(const audCompressedVolume8 rhs) const
		{
			return GetLinear() < rhs.GetLinear();
		}
		bool operator>=(const audCompressedVolume8 rhs) const
		{
			return GetLinear() >= rhs.GetLinear();
		}
		bool operator==(const audCompressedVolume8 rhs) const
		{
			return GetLinear() == rhs.GetLinear();
		}
		bool operator!=(const audCompressedVolume8 rhs) const
		{
			return GetLinear() != rhs.GetLinear();
		}

		// PURPOSE
		//	Sets this volume to the specified dB value
		void SetDb(const f32 dbVolume)
		{
			Assert(!g_AssertOnVolumeClamp || (dbVolume <= 0.0f));
			Pack(Max(0.f, audDriverUtil::ComputeFCLVolumeFromDb(dbVolume)));
		}

		// PURPOSE
		//	Returns the set volumes in dBs
		f32 GetDb() const
		{
			return audDriverUtil::ComputeDbVolumeFromFCL(Unpack());
		}

		// PURPOSE
		//	Sets this volume to the specified linear value
		void SetLinear(const f32 linearVolume)
		{
			Assert(!g_AssertOnVolumeClamp || (linearVolume >= 0.0f && linearVolume <= 1.0f));
			// we really dont want to sqrt a -ve number
			Pack(sqrt(Max(0.f, linearVolume)));
		}

		// PURPOSE
		//	Returns a linear volume
		f32 GetLinear() const
		{
			const f32 sq = Unpack();
			return (sq * sq);
		}

		void ScaleLinear(const f32 scale)
		{
			// TODO: Optimise!
			const f32 current = GetLinear();
			SetLinear(current * scale);
		}

		// PURPOSE
		//	Returns a u32 representation of this volume suitable for comparison/sorting - ie larger is bigger
		u32 GetIntRepresentation() const
		{
			return (m_PackedFCL*m_PackedFCL);
		}
	private:
		void Pack(const f32 scaledVal)
		{
			Assert(!g_AssertOnVolumeClamp || (scaledVal <= 1.0f && scaledVal >= 0.0f));
			// we ensure that scaledVal is +ve above, just make sure its not above 1 here
			m_PackedFCL = static_cast<u8>(255 * Min(scaledVal, 1.0f));
		}

		f32 Unpack() const
		{
			return m_PackedFCL / 255.f;
		}
		u8 m_PackedFCL;
	};

} // namespace rage

#endif // AUDIOENGINE_COMPRESSEDVOLUME_H 
