// 
// audioengine/remotecontrol.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_REMOTECONTROL_H
#define AUD_REMOTECONTROL_H

#if __BANK
#include "system/criticalsection.h"
#include "system/namedpipe.h"
#include "atl/array.h"
#include "audiohardware/debug.h"

namespace rage
{
	class audSound;
	class bkBank;
	class sysPipePacket;
	class audMetadataManager;
	class audWaveSlot;

		class audRemoteControlSerializer
		{
		public:

			audRemoteControlSerializer(void *buffer, const size_t bufferSize) 
				: m_Buffer(buffer)
				, m_BufferSize(bufferSize)		
			{
				Reset();
			}

			void Reset()
			{
				m_Ptr = (u32*)m_Buffer;
			}

			void Write(const u32 val)
			{
				if(m_BufferSize - GetBytesWritten() >= sizeof(u32))
				{
					*m_Ptr++ = val;
				}
			}

			void Write(const float val)
			{
				if(m_BufferSize - GetBytesWritten() >= sizeof(float))
				{
					*((float*)m_Ptr++) = val;
				}		
			}

			u32 GetBytesWritten() const { return u32((u8*)m_Ptr - (u8*)m_Buffer);}
			const void *GetBuffer() const { return m_Buffer; }

		private:

			u32 *m_Ptr;
			void *m_Buffer;
			size_t m_BufferSize;
		};


		class audSoundDebugDrawManager : public audDebugDrawManager
		{
		public:

			audSoundDebugDrawManager(const float startX, const float startY, const float clipMinY, const float clipMaxY)
				: m_X(startX)
				, m_Y(startY)
				, m_MinClipY(clipMinY)
				, m_MaxClipY(clipMaxY)
				, m_OffsetY(0.f)
			{

			}

			virtual void PushSection(const char *title);

			virtual void PopSection();

			virtual void DrawLine(const char *text);

			void SetOffsetY(const float yOffset)
			{
				m_OffsetY = yOffset;
			}

			void SkipLine();

		private:

			enum { lineHeight = 10 };
			enum { tabWidth = 20 };

			float m_X;
			float m_Y;

			float m_MinClipY;
			float m_MaxClipY;
			float m_OffsetY;

		};

		enum audRemoteControlCommands
		{
			// PURPOSE
			//	Plays the sound specified by name
			// PARAMS
			//	sound name (string)
			PLAYSOUND,
			// PURPOSE
			//	Stops the last sound played
			// PARAMS
			// (none)
			STOPSOUND,

			// NOTES
			//  No longer used; see WRITEMETADATACHUNK
			__DEPRECATED_WRITEMETADATA,


			// NOTES
			//  No longer used; see OVERRIDEOBJECTCHUNK
			__DEPRECATED_OVERRIDEOBJECT,

			// PURPOSE
			//	Sent by the game on boot up to set RAVE's current platform
			SETRUNTIMEPLATFORM,

			// NOTES
			//	No longer used; see REQUESTMETADATACHUNK
			__DEPRECATED_REQUESTMETADATA,


			// PURPOSE
			//	Sent by the game to keep the connection alive
			PING,

			// PURPOSE
			//  Informs the game that an object has been viewed in RAVE
			// PARAMS
			//  metadataTypeHash
			//  objectNameHash
			VIEWOBJECT,

			// PURPOSE
			//  Toggles whether the runtime engine should do any rendering for viewed objects
			// PARAMS
			//  metadataTypeHash
			TOGGLEVIEWOBJECT,

			// PURPOSE
			//    Sent by the game to update RAVE with resolved category settings
			// PARAMS
			//    u32 numCategories
			//    tCategorySettings categories[]

			RESOLVEDCATEGORYDATA,


			// PURPOSE
			//  Sent by the remote client to RAVE to update an objects Xml
			// PARAMS
			//  u32 stringLengthBytes
			//  char XmlString[stringLengthBytes]
			EDITOBJECT,

			// PURPOSE
			//	Sent by the game to request latest in-memory metadata
			// PARAMS
			//  metadataTypeHash
			//	chunkNameHash
			REQUESTMETADATACHUNK,

			// PURPOSE
			//	Overrides an objects metadata - the data specified will be returned in place of
			//	the original metadata
			// PARAMS
			//  metadataTypeHash
			//  chunkName/episode hash
			//	hash - the hashed name of the object 
			//	size (bytes) - the number of bytes to store
			//	data (byte[]) - data to store in metadata
			__DEPRECATED_OVERRIDEOBJECTCHUNK,

			// PURPOSE
			//	Writes the specified data into metadata
			// PARAMS
			//  metdataTypeHash
			//  chunkNameHash
			//	offset (bytes) - the offset into the metadata chunk to start writing
			//	size (bytes) - number of bytes to write
			//	data (byte[]) - data to store into metadata
			__DEPRECATED_WRITEMETADATACHUNK,

			// PURPOSE
			//  Sent from RAVE to the game to acknowledge successful processing of an Xml message
			MESSAGERECEIPT,

			// PURPOSE
			//	Sent from RAVE to the game to request auditioning of the specified object
			AUDITION_START,
			AUDITION_STOP,

			OBJECT_CHANGED,

			CATEGORY_TELEMETRY_HEADER,
			CATEGORY_TELEMETRY_UPDATE,

			SET_MUTE_LIST,
			SET_SOLO_LIST,
			AUDIO_ENGINE_LOADED,
		};

// PURPOSE
//	This class implements RPC functionality allowing rave to audition sounds,
//	tweak metadata values etc.
class audRemoteControl
{
public:
	audRemoteControl();
	~audRemoteControl();

	// PURPOSE
	//	Should be called only once on Init. Generates a random number which will subsequently be passed down to the pipe
	void GenerateUniqueId();

	// PURPOSE
	//	This should be called once per audio frame - it polls the
	//	pipe for commands and executes them
	// PARAMS
	//	time - current time in milliseconds
	void Update(u32 time);
	
	// PURPOSE
	//	Initializes the named pipe
	bool Init();

	// PURPOSE
	//	Closes the connection
	void Shutdown();

	// PURPOSE
	//	Returns true if currently connected to RAVE
	bool IsConnected() const;

	// PURPOSE
	//	Returns true if running -rave (whether currently connected or not)
	static bool IsPresent();

	// PURPOSE
	//	Returns compiled metadata chunk fetched from RAVE
	// RETURNS
	//	True if successful, false otherwise, buf points to metadata
	// NOTES
	//	buf is allocated with g_AudioEngine.AllocateVirtual and should be free'd with g_AudioEngine.FreeVirtual
	bool RequestMetadataChunk(const u32 metadataTypeHash, const u32 chunkNameHash, const u32 schemaVersion, u8 *&buf, u32 &len);

	void RegisterMetadataManager(const u32 metadataTypeHash, audMetadataManager *manager);
	
	audSound **GetAuditionSoundPtr()
	{
		return &m_LastSoundPlayed;
	}

	void DrawDebug() const;

	// PURPOSE
	//	Adds widgets
	static void AddWidgets(bkBank &bank);

	// PURPOSE
	//	Sends an Xml message to RAVE
	// RETURNS
	//	True if the message was successfully sent to RAVE, false otherwise
	bool SendXmlMessage(const char *xmlMessage);
	bool SendXmlMessage(const char *xmlMessage, const u32 length);

	// PURPOSE
	//	Sends a binary serialized command to RAVE
	// RETURNS
	//	True if the message was successfully sent to RAVE, false otherwise
	bool SendCommand(const audRemoteControlSerializer &data, const audRemoteControlCommands command);

	u32 GetLastMessageReceiptTime() const
	{
		return m_LastMessageReceiptTime;
	}

	enum { kRemoteControlPayloadSizeBytes = 128 * 1024 };
	void *GetPayloadBuffer() { return m_PayloadBuf; };

	// PURPOSE
	//	Returns true if the specified metadata type should be grabbed from RAVE
	bool ShouldUseRAVEForMetadataType(const u32 typeNameHash) const;

	// PURPOSE
	//	Returns true if the specified metadata chunk should be grabbed from RAVE, rather than loaded from disc
	bool ShouldUseRAVEForMetadataChunk(const char *chunkName) const;

	enum AuditionSoundDrawMode 
	{
		kNothing = 0,
		kEverything, 
		kHierarchyOnly,
		kNoDynamicChildren, 
		kVariablesOnly, 
		kNumDrawModes
	};

	void SetAuditionSoundDrawMode(const AuditionSoundDrawMode mode)
	{
		m_AuditionSoundDrawMode = mode;
	}

	void SetAuditionSoundDrawOffsetY(const float offsetY)
	{
		m_AuditionSoundDrawOffsetY = offsetY;
	}

	void DrawDebugSound(audSound *sound, const AuditionSoundDrawMode drawMode) const;

	inline void SetOutgoingMessagesEnabled(bool enabled) { m_OutgoingMessagesEnabled = enabled; }

private:

	// PURPOSE
	//	Renders the currently selected curve, if there is one
	void RenderCurve() const;

	void ObjectEdit(const u32 metadataTypeHash, const u32 chunkNameHash, const char *objectName, const u32 size, u8 *data);
	void ViewObject(const u32 metadataTypeHash, const u32 objectName);
	void ToggleViewObject(const u32 metadataTypeHash);
	void AuditionStart(const u32 metadataTypeHash, const u32 objectNameHash);
	void AuditionStop(const u32 metadataTypeHash, const u32 objectNameHash);
	
	void HandleReceivedPacket(const u8 packetType, const u32 payloadSize, u8 *payload);

	void SetRuntimePlatform();

	static void PlaySoundWidget();

	audSound *m_LastSoundPlayed;

	sysNamedPipe m_Pipe;
	sysPipePacket *m_PipePacket;
	s32 m_UniqueId;

	u8 *m_PayloadBuf;
	
	u32 m_ViewObjectNameHash;
	u32 m_ViewMetadataTypeHash;
	u32 m_LastMessageReceiptTime;
	sysCriticalSectionToken m_NetworkCS;

	AuditionSoundDrawMode m_AuditionSoundDrawMode;
	float m_AuditionSoundDrawOffsetY;
	bool m_OutgoingMessagesEnabled;

	atMap<u32,audMetadataManager *> m_MetadataMgrMap;
	atArray<u32> m_RaveMetadataTypes;
	atArray<u32> m_RaveMetadataChunks;

	static f32 sm_MinCurveInput, sm_MaxCurveInput;
	
	static bool sm_ShouldRenderCurve;
	static u32 	sm_LastSentMessage;	
	
};

} // namespace rage

#endif // BANK
#endif // AUD_REMOTECONTROL_H
