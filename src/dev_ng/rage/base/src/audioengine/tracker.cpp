//
// audioengine/tracker.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "tracker.h"

#include "controller.h"

namespace rage {

audTracker::audTracker()
{
	m_ReferenceCount = 0;
}

audTracker::~audTracker()
{
	// Don't bother telling the controller if we've got no referencing sounds
	audController* controller = audController::FindControllerForThread();
	if (controller && m_ReferenceCount>0)
	{
		controller->ReportTrackerDeletion(this);
	}
}

} // namespace rage
