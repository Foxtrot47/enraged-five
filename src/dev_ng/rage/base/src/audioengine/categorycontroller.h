// 
// audioengine/categorycontroller.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_CATEGORYCONTROLLER_H
#define AUD_CATEGORYCONTROLLER_H


#include "data/base.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/driverutil.h"

namespace rage 
{


const u32 g_MaxCategoryControllers = 768;

//	Make sure the number of controllers is always aligned to 16
CompileTimeAssert(g_MaxCategoryControllers%16 == 0);

#if __ASSERT
enum audCategorySettingType
{
	CategorySettingGainLinear,
	CategorySettingDistRolloff,
	CategorySettingFrequencyRatio,
	CategorySettingLPFCuttOff,
	CategorySettingHPFCuttOff
};
#endif

struct audCategoryControllerOutput
{
	f32 VolumeDb;
	f32 DistRolloffScale;
	s16 PitchCents;
	u16 LPFCutoff;
	u16 HPFCutoff;

	void SetDefaults();
};

// Writable from audio thread only
struct audCategoryControllerSettings
{
	audCategoryControllerSettings() :
	m_fGainLinear(1.f),
	m_fDistRolloff(1.f),
	m_fFreqRatio(1.f),
	m_fLPFCutoffOct(0.f),
	m_fHPFCutoffOct(g_OctavesBelowFilterMaxAt1Hz)
	{
	}
	~audCategoryControllerSettings(){}

	bool IsAtUnity() const
	{
		return (m_fLPFCutoffOct == 0.f &&
			m_fHPFCutoffOct <= g_OctavesBelowFilterMaxAt1Hz &&
			m_fFreqRatio == 1.0f &&
			m_fDistRolloff == 1.0f &&
			m_fGainLinear == 1.0f);
	}

	f32						m_fGainLinear;
	f32						m_fDistRolloff;
	f32						m_fFreqRatio;
	f32						m_fLPFCutoffOct;
	f32						m_fHPFCutoffOct;
};


//////////////////////////////////////////////////////////////////////////
//	Forward Declarations
class audCategory;
class bkBank;
class bkGroup;
/*************************************************************************************
Class:		audCategoryController
Purpose:	Allows additional user control over category volume and pitch.
			If multiple game modules want to have control over a specific category's
			volume or pitch, each one must register a category controller using the
			class audCategoryControllerManager 
*************************************************************************************/

class audCategoryController
#if __BANK
	: public datBase
#endif
{
public:
	friend class audCategoryControllerManager;
	audCategoryController(u16 iPoolIndex);
	~audCategoryController();

	// PURPOSE
	// Loops through every category in the passed in list and computes the results
	static void ComputeOutput(audCategoryControllerOutput& results, const s32 iHeadIndex, const f32 fTimeStep);

	
	const audCategoryControllerSettings* GetSettings() const;
private:
	audCategoryControllerSettings* GetSettings();
public:

	// PURPOSE
	// Returns the volume in dBs
	f32 GetVolumeDB() const;
	f32 GetVolumeDBDest() const { return audDriverUtil::ComputeDbVolumeFromLinear(m_fGainLinearDest); }

	// PURPOSE
	//	Returns the volume in linear terms
	f32 GetVolumeLinear() const;
	f32 GetVolumeLinearDest() const { return m_fGainLinearDest; }

	// PURPOSE
	//	Returns the distance rolloff scale in scalar units
	f32 GetDistanceRolloffScale() const;
	f32 GetDistanceRolloffScaleDest() const { return m_fDistRolloffDest; }

	// PURPOSE
	//	Returns the pitch in cents
	f32 GetPitchCents() const;
	f32 GetPitchCentsDest() const { return audDriverUtil::ConvertRatioToPitch_Float(m_fFreqDest); }

	// PURPOSE
	//	Returns the pitch as a frequency ratio
	f32 GetFrequencyRatio() const;
	f32 GetFrequencyDest() const { return m_fFreqDest; }

	// PURPOSE
	//	Returns the LPF cutoff frequency in hertz
	f32 GetLPFCutoffHz() const;
	f32 GetLPFCutoffHzDest() const;

	// PURPOSE
	//	Returns the LPF cutoff frequency in octaves
	f32 GetLPFCutoffOctaves() const;
	f32 GetLPFCutoffOctavesDest() const;

	// PURPOSE
	//	Returns the HPF cutoff frequency in hertz
	f32 GetHPFCutoffHz() const;
	f32 GetHPFCutoffHzDest() const;

	// PURPOSE
	//	Returns the HPF cutoff frequency in octaves
	f32 GetHPFCutoffOctaves() const;
	f32 GetHPFCutoffOctavesDest() const;

	// PURPOSE
	//	Returns the Game set durations
	f32 GetGainDuration() const { return m_fGainDuration; }
	f32 GetDistRolloffDuration() const { return m_fDistRolloffDuration; }
	f32 GetFreqDuration() const { return m_fFreqDuration; }
	f32 GetLPFDuration() const { return m_fLPFDuration; }
	f32 GetHPFDuration() const { return m_fHPFDuration; }

	/*************************************************************************************
	Function:	GetCategory
	Return:		Pointer to the associated audCategory
	*************************************************************************************/
	const audCategory* GetCategory() const;
	audCategory* GetCategory();
	s32 GetCategoryId() const { return m_iCategoryIndex; }

	/*************************************************************************************
	Function:	IsAtUnityAndStatic
	Return:		True if all values are at their defaults and it's not moving
	*************************************************************************************/
	bool IsAtUnity() const
	{
		return GetSettings()->IsAtUnity();
	}

	/*************************************************************************************
	Function:	SetPauseGroup
	In:			bPauseGroup:	The pause group to assign this controller to
	*************************************************************************************/
	void SetPauseGroup(const s8 iPauseGroup) { m_iPauseGroup = iPauseGroup; }

	/*************************************************************************************
	Function:	SetVolumeDB
	In:			fDB:	The desired volume in decibels
	*************************************************************************************/
	void SetVolumeDB(const f32 fDB) { return SetVolumeLinear(audDriverUtil::ComputeLinearVolumeFromDb(fDB)); }

	/*************************************************************************************
	Function:	SetVolumeLinear
	In:			fLinear:	The desired volume in linear scale 0 = silence, 1 = unity gain
	*************************************************************************************/
	void SetVolumeLinear(const f32 fLinear);

	/*************************************************************************************
	Function:	SetDistanceRolloffScale
	In:			fScale:	The distance rolloff in scalar units
	*************************************************************************************/
	void SetDistanceRolloffScale(const f32 fScale);

	/*************************************************************************************
	Function:	SetPitchCents
	In:			iCents:	Pitch in cents
	*************************************************************************************/
	void SetPitchCents(const f32 fCents);
	void SetPitchCents(const s32 iCents) { return SetPitchCents((f32)iCents); }

	/*************************************************************************************
	Function:	SetPitchCents
	In:			fRatio:	Pitch in linear ratio, 1 = normal, 2 = one octave up, 0.5 = one octave down
	*************************************************************************************/
	void SetFrequencyRatio(f32 fRatio);

	/*************************************************************************************
	Function:	SetLPFCutoffHz
	In:			fHz:	Filter frequency in Hz
	*************************************************************************************/
	void SetLPFCutoffHz(f32 fHz);

	/*************************************************************************************
	Function:	SetLPFCutoffHz
	In:			fOctaves:	Filter frequency in octaves relative to constant 23900.0
	*************************************************************************************/
	void SetLPFCutoffOctaves(f32 fOctaves);

	/*************************************************************************************
	Function:	SetHPFCutoffHz
	In:			fHz:	Filter frequency in Hz
	*************************************************************************************/
	void SetHPFCutoffHz(f32 fHz);

	/*************************************************************************************
	Function:	SetHPFCutoffOctaves
	In:			fOctaves:	Filter frequency in octaves relative to constant 23900.0
	*************************************************************************************/
	void SetHPFCutoffOctaves(f32 fOctaves);

	/*************************************************************************************
	Function:	BeginGainFadeDB
	In:			fGainDestinationDB:	The destination of the fade in dB's
				fDuration:	The desired duration of the fade
	*************************************************************************************/
	void BeginGainFadeDB(f32 fGainDestinationDB, f32 fDuration);

	/*************************************************************************************
	Function:	BeginGainFadeLinear
	In:			fGainDestinationLinear:	The destination of the fade in linear scale
	fDuration:	The desired duration of the fade
	*************************************************************************************/
	void BeginGainFadeLinear(f32 fGainDestinationLinear, f32 fDuration);

	/*************************************************************************************
	Function:	BeginDistRolloffScaleApproach
	In:			fScale:	The destination of the approach in scalar units
				fDuration:	The desired duration of the approach
	*************************************************************************************/
	void BeginDistRolloffScaleApproach(f32 fScale, f32 fDuration);

	/*************************************************************************************
	Function:	BeginPitchShiftLinear
	In:			fFreqDestination:	The destination of the shift in linear ratio
				fDuration:	The desired duration of the shift
	*************************************************************************************/
	void BeginPitchShiftLinear(f32 fFreqDestination, f32 fDuration);

	/*************************************************************************************
	Function:	BeginLPFSweep
	In:			fDestinationHz:	The destination of the shift in Hz
				fDuration:	The desired duration of the sweep
	Purpose:	Sweeps the cutoff filter on a logarithmic scale
	*************************************************************************************/
	void BeginLPFSweep(f32 fDestinationHz, f32 fDuration);
	void BeginLPFSweepOctaves(f32 fDestinationOctaves, f32 fDuration);

	/*************************************************************************************
	Function:	BeginHPFSweep
	In:			fDestinationHz:	The destination of the shift in Hz
				fDuration:	The desired duration of the sweep
	Purpose:	Sweeps the cutoff filter on a logarithmic scale
	*************************************************************************************/
	void BeginHPFSweep(f32 fDestinationHz, f32 fDuration);
	void BeginHPFSweepOctaves(f32 fDestinationHz, f32 fDuration);

	/*************************************************************************************
	Function:	BeginLPFSweepOctaves
	In:			fDestinationOctaves:	The destination filter frequency in octaves relative to constant 23900.0
	fDuration:	The desired duration of the sweep
	Purpose:	Sweeps the cutoff filter 
	*************************************************************************************/
	bool BeginLPFSweepOctaves(f32 fDestinationOctaves, f32 fDuration) const;

	/*************************************************************************************
	Function:	BeginHPFSweepOctaves
	In:			fDestinationOctaves:	The destination filter frequency in octaves relative to constant 23900.0
	fDuration:	The desired duration of the sweep
	Purpose:	Sweeps the cutoff filter
	*************************************************************************************/
	bool BeginHPFSweepOctaves(f32 fDestinationOctaves, f32 fDuration) const;


#if __BANK
	void AddWidgets(rage::bkBank& bank, bool bPopGroup = true);
	//static void Solo(audCategoryController* pController);
	static void SetDisableSceneControllers(bool disable)	{ sm_DisableSceneControllers = disable; }
	static void SetDisableDynamicControllers(bool disable)	{ sm_DisableDynamicControllers = disable; }

#endif


	// PURPOSE
	//	Computes the interpolated values
	// NOTES
	//	Internal to audio thread
	void Update(f32 timeStep);

	audCategoryController *GetNext() const;
	audCategoryController *GetPrev() const;
	u32 GetPoolIndex() const	{ return m_uPoolIndex; }
	s32 GetCategoryIndex() const{ return m_iCategoryIndex; }
	s32 GetNextIndex() const	{ return m_iNextControllerIndex; }
	s32 GetPrevIndex() const	{ return m_iPreviousControllerIndex; }
	void SetNext(const audCategoryController *next);
	void SetPrev(const audCategoryController *prev);

	static void SetPools(audCategoryController* pCtrlPool, audCategoryControllerSettings* pSettingsPool)
	{
		sm_ControllerPool = pCtrlPool;
		sm_ControllerSettingsPool = pSettingsPool;
	}


	// PURPOSE
	//	Parses the buffered command list and modifies controller state accordingly
//	static void ParseCommandList(const audCCBEntry *list, const u32 numEntries, audCategoryController *controllers);

private:

	void UpdateSetting(f32& fSetting, const f32 fDestination, const f32 fDuration, const f32 fBaseScaleValue, const f32 timeStep ASSERT_ONLY(, audCategorySettingType settingType)) const;

#if __BANK
	void WidgetUpdateDbFromLinear()			{ SetVolumeLinear(GetVolumeLinear());}
	void WidgetUpdateCentsFromFrequency()	{ SetFrequencyRatio(GetFrequencyRatio()); }
	void WidgetUpdateHzFromOctaves()		{ SetLPFCutoffOctaves(GetLPFCutoffOctaves()); SetHPFCutoffOctaves(GetHPFCutoffOctaves()); }
	void WidgetUpdateLinearFromDb()			{ SetVolumeDB(m_fBankGainDB); }
	void WidgetUpdateFrequencyFromCents()	{ SetPitchCents(m_fBankPitchCents); }
	void WidgetUpdateOctavesFromHz()		{ SetLPFCutoffHz(m_fBankLPFCutoffHz); SetHPFCutoffHz(m_fBankHPFCutoffHz); }
	void Print();
	void SetIsSceneController( bool isSceneController );
	void SetIsDynamic( bool isDynamic );
#endif

	//	Data Members
	f32						m_fGainLinearDest;			// Writable from game thread only
	f32						m_fGainDuration;			// Writable from game thread only

	f32						m_fDistRolloffDest;			// Writable from game thread only
	f32						m_fDistRolloffDuration;		// Writable from game thread only
	
	f32						m_fFreqDest;				// Writable from game thread only
	f32						m_fFreqDuration;			// Writable from game thread only

	f32						m_fLPFOctDest;				// Writable from game thread only
	f32						m_fLPFDuration;				// Writable from game thread only

	f32						m_fHPFOctDest;				// Writable from game thread only
	f32						m_fHPFDuration;				// Writable from game thread only

	u16						m_uPoolIndex;
	s16						m_iPreviousControllerIndex;
	s16						m_iNextControllerIndex;
	s16						m_iCategoryIndex;

	s8						m_iPauseGroup;

#if __BANK
	bkGroup*				m_pWidgetGroup;
	f32 					m_fBankGainDB;
	f32 					m_fBankPitchCents;
	f32 					m_fBankLPFCutoffHz;
	f32 					m_fBankHPFCutoffHz;

	f32						m_fCatVolume;
	f32						m_fCatDistRolloff;
	s32						m_iCatPitch;
	u32						m_uCatLPF;
	u32						m_uCatHPF;
	u32						m_TimerId;
	bool					m_bMute;
	bool					m_bSolo;
	bool					m_IsSceneController;
	bool					m_IsDynamic;
#endif

	static audCategoryController* sm_ControllerPool;
	static audCategoryControllerSettings* sm_ControllerSettingsPool;

#if __BANK
	static bool sm_DisableSceneControllers;
	static bool sm_DisableDynamicControllers;
#endif
};

struct audCategoryControllerPools
{
	u8	ALIGNAS(16)		m_ControllerPool[sizeof(audCategoryController) * g_MaxCategoryControllers] ;
	u8	ALIGNAS(16)		m_ControllerSettingsPool[sizeof(audCategoryControllerSettings) * g_MaxCategoryControllers] ;
};


#define CATCTRL_INVLOG2 (1.4426950408889634073599246810019f)
#define CATCTRL_LOG2(target) ( log(target) * CATCTRL_INVLOG2 )


inline void audCategoryController::SetVolumeLinear(const f32 fLinear)
{
	m_fGainLinearDest = Selectf(fLinear - g_SilenceVolumeLin, fLinear, 0.f);
	m_fGainDuration = 0.f;
}

inline void audCategoryController::SetDistanceRolloffScale(const f32 fScale)
{
	m_fDistRolloffDest = fScale;
	m_fDistRolloffDuration = 0.f;
}

inline void audCategoryController::SetFrequencyRatio(const f32 ratio)
{
	m_fFreqDest = ratio;
	m_fFreqDuration = 0.f;
}

inline void audCategoryController::SetPitchCents(const f32 cents)
{
	SetFrequencyRatio(audDriverUtil::ConvertPitchToRatio(cents));
}

inline void audCategoryController::SetLPFCutoffHz(f32 destinationHz)
{
	const f32 cutoffOctaves = CATCTRL_LOG2(Clamp(destinationHz, static_cast<f32>(kVoiceFilterLPFMinCutoff), static_cast<f32>(kVoiceFilterLPFMaxCutoff)) * g_InvSourceLPFilterCutoff);
	SetLPFCutoffOctaves(cutoffOctaves);
}

inline void audCategoryController::SetLPFCutoffOctaves(const f32 cutoffOctaves)
{
	m_fLPFOctDest = cutoffOctaves;
	m_fLPFDuration = 0.f;
}

inline void audCategoryController::SetHPFCutoffHz(f32 destinationHz)
{
	const f32 cutoffOctaves = CATCTRL_LOG2(Clamp(destinationHz, static_cast<f32>(kVoiceFilterHPFMinCutoff), static_cast<f32>(kVoiceFilterHPFMaxCutoff)) * g_InvSourceHPFilterCutoff);
	SetHPFCutoffOctaves(cutoffOctaves);
}

inline void audCategoryController::SetHPFCutoffOctaves(const f32 cutoffOctaves)
{
	m_fHPFOctDest = cutoffOctaves;
	m_fHPFDuration = 0.f;
}

inline void audCategoryController::BeginGainFadeDB(f32 volumeDb, f32 fDuration)
{
	BeginGainFadeLinear(audDriverUtil::ComputeLinearVolumeFromDb(volumeDb), fDuration);
}

inline void audCategoryController::BeginLPFSweep(f32 destinationHz, f32 fDuration)
{
	const f32 cutoffOctaves = CATCTRL_LOG2(Clamp(destinationHz, static_cast<f32>(kVoiceFilterLPFMinCutoff), static_cast<f32>(kVoiceFilterLPFMaxCutoff)) * g_InvSourceLPFilterCutoff);
	BeginLPFSweepOctaves(cutoffOctaves, fDuration);
}

inline void audCategoryController::BeginHPFSweep(f32 destinationHz, f32 fDuration)
{
	const f32 cutoffOctaves = CATCTRL_LOG2(Clamp(destinationHz, static_cast<f32>(kVoiceFilterHPFMinCutoff), static_cast<f32>(kVoiceFilterHPFMaxCutoff)) * g_InvSourceHPFilterCutoff);
	BeginHPFSweepOctaves(cutoffOctaves, fDuration);
}

inline f32 audCategoryController::GetVolumeDB() const
{
	return audDriverUtil::ComputeDbVolumeFromLinear(GetVolumeLinear());
}

inline f32 audCategoryController::GetVolumeLinear() const
{
	return GetSettings()->m_fGainLinear;
}

inline f32 audCategoryController::GetDistanceRolloffScale() const
{
	return GetSettings()->m_fDistRolloff;
}

inline f32 audCategoryController::GetPitchCents() const
{
	return audDriverUtil::ConvertRatioToPitch_Float(GetFrequencyRatio());
}

inline f32 audCategoryController::GetFrequencyRatio() const
{
	return GetSettings()->m_fFreqRatio;
}

inline f32 audCategoryController::GetLPFCutoffHz() const
{
	const f32 fLPFCutoffOct = GetLPFCutoffOctaves();
	//return (fLPFCutoffOct > g_fOctavesBelowFilterMaxAt20Hz ? static_cast<f32>(kVoiceFilterLPFMinCutoff) : Powf(2.f, fLPFCutoffOct) * static_cast<f32>(g_audVoiceFilterMaxCutoff));
	return (Selectf(g_OctavesBelowFilterMaxAt100Hz - fLPFCutoffOct, static_cast<f32>(kVoiceFilterLPFMinCutoff), Powf(2.f, fLPFCutoffOct) * static_cast<f32>(kVoiceFilterLPFMaxCutoff)));
}

inline f32 audCategoryController::GetLPFCutoffOctaves() const
{
	return GetSettings()->m_fLPFCutoffOct;
}

inline f32 audCategoryController::GetHPFCutoffHz() const
{
	const f32 fHPFCutoffOct = GetHPFCutoffOctaves();
	//return (g_fOctavesBelowFilterMaxAt20Hz > fHPFCutoffOct ? 20.f : Powf(2.f, fHPFCutoffOct) * static_cast<f32>(g_audVoiceFilterMaxCutoff));
	return (Selectf(g_OctavesBelowFilterMaxAt1Hz - fHPFCutoffOct, static_cast<f32>(kVoiceFilterHPFMinCutoff), Powf(2.f, fHPFCutoffOct) * static_cast<f32>(kVoiceFilterHPFMaxCutoff)));
}

inline f32 audCategoryController::GetHPFCutoffOctaves() const
{
	return GetSettings()->m_fHPFCutoffOct;
}

inline f32 audCategoryController::GetLPFCutoffHzDest() const
{
	return (m_fLPFOctDest <= g_OctavesBelowFilterMaxAt100Hz ? static_cast<f32>(kVoiceFilterLPFMinCutoff) : Powf(2.f, m_fLPFOctDest) * static_cast<f32>(kVoiceFilterLPFMaxCutoff));
}

inline f32 audCategoryController::GetLPFCutoffOctavesDest() const
{
	return m_fLPFOctDest;
}

inline f32 audCategoryController::GetHPFCutoffHzDest() const
{
	return (m_fHPFOctDest <= g_OctavesBelowFilterMaxAt1Hz ? static_cast<f32>(kVoiceFilterHPFMinCutoff) : Powf(2.f, m_fHPFOctDest) * static_cast<f32>(kVoiceFilterHPFMaxCutoff));
}

inline f32 audCategoryController::GetHPFCutoffOctavesDest() const
{
	return m_fHPFOctDest;
}




} // namespace rage

#endif // AUD_CATEGORYCONTROLLER_H


