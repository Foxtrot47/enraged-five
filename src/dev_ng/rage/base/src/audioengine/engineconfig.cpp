// 
// audioengine/engineconfig.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "audiohardware/config.h"
#include "engineconfig.h"

namespace rage
{
	bool audEngineConfig::Init()
	{
		audConfig::GetData("engineSettings_OrientationUp", m_GameOrientationUp);
		audConfig::GetData("engineSettings_OrientationForward", m_GameOrientationForward);
		audConfig::GetData("engineSettings_MaxRelativeVelocityForDoppler", m_MaxRelativeVelocityForDoppler);
		audConfig::GetData("engineSettings_SpeedOfSound", m_SpeedOfSound);		

		audConfig::GetData("metadataPaths_Sounds", m_SoundMetadata);	
		audConfig::GetData("metadataPaths_Curves", m_CurveMetadata);
		audConfig::GetData("metadataPaths_Categories", m_CategoryMetadata);
		audConfig::GetData("metadataPaths_Effects", m_EffectMetadata);		
		audConfig::GetData("metadataPaths_Speech", m_SpeechLookupMetadata);
		audConfig::GetData("metadataPaths_Speech2", m_SpeechMetadata);

		audConfig::GetData("engineSettings_NumSoundsPerBucket", m_NumSoundsPerBucket);
		audConfig::GetData("engineSettings_NumBuckets", m_NumBuckets);
		audConfig::GetData("engineSettings_NumRequestedSettingsPerBucket", m_NumRequestedSettingsPerBucket);
		audConfig::GetData("engineSettings_NumReservedBuckets", m_NumReservedBuckets);

		return true;
	}
}
