// 
// audioengine/categoryupdatejob.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "vector/vector3_consts_spu.cpp"

#include "categoryupdatejob.h"

using namespace rage;
  
#if __SPU

#include "category.cpp"

namespace rage
{
	tCategorySettings *g_ResolvedCategorySettings = NULL;
	audCategory *g_CategoryStoreMem = NULL;

}
#else

#include "category.h"

namespace rage
{
	extern tCategorySettings *g_ResolvedCategorySettings;
	extern audCategory *g_CategoryStoreMem;
}
#endif


void CategoryUpdateJob(sysTaskParameters &p)
{
	u16 audioThreadIndex = 0;
	Assign(audioThreadIndex, p.UserData[0].asInt);
#if __DEV
	u32 audCategorySize = p.UserData[1].asInt;
	Assert(audCategorySize == sizeof(audCategory));

	if(audCategorySize == sizeof(audCategory))
#endif
	{
		g_ResolvedCategorySettings = (tCategorySettings*)p.Output.Data;
		g_CategoryStoreMem = (audCategory*)p.Input.Data;
		// base category is first in store, calling update will cascade down hierarchy
		g_CategoryStoreMem->AudioThreadUpdate(audioThreadIndex, true);

		int numDynamicHeirachies = p.UserData[3].asInt;
		s16 *dynamicHeiracies = (s16 *)p.ReadOnly[0].Data;

		for(int i=0; i<numDynamicHeirachies; i++)
		{
			if(dynamicHeiracies[i] >= 0)
			{
				g_CategoryStoreMem[dynamicHeiracies[i]].AudioThreadUpdate(audioThreadIndex,false);
			}
		}
	}
}
