//
// audioengine/dynamicmixmgr.h
//
// Copyright (C) 1999-2010 Rockstar Games. All rights reserved.
//

#ifndef DYNAMIC_MIX_MGR_H
#define DYNAMIC_MIX_MGR_H

#include "dynamixdefs.h"
#include "metadatamanager.h"

#include "atl/pool.h"

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif

enum
{
	NUM_AUD_MIXGROUPS = 16 //must be kept in line with NUM_DYNAMIC_HEIRACHIES
};

#define AUD_DM_NAME(x) (g_AudioEngine.GetDynamicMixManager().GetMetadataManager().GetObjectNameFromNameTableOffset(x->NameTableOffset))
#define AUD_DM_HASH_NAME(x) (g_AudioEngine.GetDynamicMixManager().GetMetadataManager().GetObjectName(x))

#define DEBUG_MIXGROUPS 0

#if DEBUG_MIXGROUPS
#	define DEBUG_MIXGROUPS_ONLY(...)    __VA_ARGS__
#else
#	define DEBUG_MIXGROUPS_ONLY(...)
#endif

namespace rage
{

class audMixGroup;
class audDynamicMixMgr;
class audCategory;
class bkGroup;

extern atPool<audMixGroup> *g_MixGroupPool;
extern audMixGroup * g_MixGroupPoolMem;
extern u32 * g_MixGroupReferenceCounts;
extern u32 * g_MixGroupInstanceIds;


class ALIGNAS(16) audMixGroup
{
	friend class audDynamicMixMgr;
public:
	enum MixGroupRefType
	{
		SCENE_REF,
		SOUND_REF,
		ENTITY_REF,
		PATCH_REF,
		NUM_REF_TYPES,
		NULL_REF
	};

#if DEBUG_MIXGROUPS
	static const char* k_RefTypeNames[NUM_REF_TYPES];
	static void FormatRefCountStr(char* strBuf, int bufLen, int mixGroupIndex);
#endif

	s32 GetCategoryIndex() { return m_CategoryIndex; }
	s32 GetIndex() { return m_MixGroupIndex; }
	const s16* GetMap() const { return m_Map; }
	u32 GetObjectHash() {return m_MixGroupHash; }
	void AddReference(MixGroupRefType type = NULL_REF);
	void RemoveReference(MixGroupRefType type = NULL_REF);
	u32  GetReferenceCount() const;
	float GetFadeTime() { return m_FadeTime; }
	static u32 AddMixGroupReference(s32 mixGroupIndex, MixGroupRefType type = NULL_REF);
	static u32 RemoveMixGroupReference(s32 mixGroupIndex, MixGroupRefType type = NULL_REF);
	static u32 GetReferenceCount(s32 mixGroupIndex);

	audMixGroup(); 
	~audMixGroup();

	bool Init(MixGroup * mixGroupSettings);
	void Shutdown();


private:

	float m_FadeTime;

	// hash of the name of this mix group
	u32 m_MixGroupHash;

	// Maps global categories onto the dynamic heirachy of this mix group
	s16* m_Map;

	// index into the mixgroup static arrays: NOT the g_MixGroupPoolMem!!!
	s16 m_MixGroupIndex;

	// root index of this dynamic heirachy
	s16 m_CategoryIndex;

}
;

//ALIGNAS(16)
class audDynamicMixMgr 
{
	friend class audMixGroup;

public:
	audDynamicMixMgr();
	~audDynamicMixMgr();

	// PURPOSE
	// Frees all dynamic mix metadata memory
	void Shutdown();
	
	// PURPOSE
	// Does garbage collection on the mixgroups in case
	// any have achieved zero references during the sound 
	// update
	void PostUpdateProcess();

	// PURPOSE
	// Turn on all debug printouts
	void ActivateDebugPrinting();

	//PURPOSE
	// Print out active debug.
	void PrintDebug();


	// PURPOSE
	// Add a reference to a mixgroup creating a new one if necessary
	// PARAMS
	// nameHash - the hashed name of the mixgroup to aquired
	void GetMixGroupReference(u32 nameHash, audMixGroup::MixGroupRefType type = audMixGroup::NULL_REF);

	// PURPOSE
	// Decrements the reference count of a mix group and shuts down if necessary
	// PARAMS
	// nameHash - the hashed name of the mixgroup to release.
	void ReleaseMixGroupReference(u32 nameHash, audMixGroup::MixGroupRefType type = audMixGroup::NULL_REF);

	template <class _T> _T *GetObject(const char *name)
	{
		return m_MetadataMgr.GetObject<_T>(name);
	}

	template <class _T> _T *GetObject(const u32 nameHash)
	{
		return m_MetadataMgr.GetObject<_T>(nameHash);
	}
  
	const audMetadataManager &GetMetadataManager() const
	{
		return m_MetadataMgr;
	}

	audMetadataManager &GetMetadataManager()
	{
		return m_MetadataMgr;
	}

#if	__DEV
	const char * GetMixGroupName(const u32 mixGroupHash) const;
	const char * GetMixGroupNameFromIndex(const int index) const
	{
		return GetMixGroupName(GetMixGroupHash(index));
	}
		const char * GetMixModuleName(const u32 moduleHash) const;
#endif
	// PURPOSE
	// Gets a named mix group from the pool. Used only internally within audDynamicMixManager
	// and audDynamicMixer as it does not increment the reference count.
	// PARAMS
	// mixGroupSettings - a pointer to the mix group metadata object
	// RETURNS
	// A pointer to the mix group if it is found in the pool or NULL otherwise
	audMixGroup* GetMixGroup(u32 mixGroupHash);
	s32 GetMixGroupIndex(u32 mixGroupHash);
	u32 GetMixGroupHash(s32 index) const;
	static u32 GetMixGroupInstIndex(const audMixGroup * mixGroup);

	// PURPOSE
	// Creates and initialises a mixgroup
	//PARAMS
	// nameHash - hashed name of the mixgroup to create
	//NOTES
	// Does NOT add a reference to the mix group
	audMixGroup * CreateMixGroup(u32 nameHash);

	//PURPOSE
	//Gets number of mix groups defined in metadata
	//u32 GetNumMixGroups() { return m_NumMixGroups; }
	
	// PURPOSE
	// Initializes the metadata manager by loading dyanamix.dat. Run directly after creation.
	// RETURNS
	//  True if successful, false otherwise
	bool Init();

	// PURPOSE
	// Sets up mixgroups using the MixGroupList from the specified chunk
	void InitMixgroupsForChunk(int chunkId);

	// PURPOSE
	// Mark chunk as disabled so metadata can't be used while we're shutting it down
	void StartUnloadingChunk(const char *chunkName);
	
	// PURPOSE
	//	Re-enable chunk previously marked as disabled
	void CancelUnloadingChunk(const char *chunkName);


private:

	// PURPOSE
	// Recurses through the category heirarchy setting up the mix group map as it goes
	// PARAMS
	// map - the MixGroupMap that we want to set up
	// cat - the next category to check: this is the BASE category in the initial call
	// mapIndex - tracks which index we're at in the map's array
	// mixGroup - metadata for the mixgroup we're mapping
	// nodeHash - once the recursion finds a heirachy, categories below will be mapped upwards
	// hierarchy - the category hierarchy described by this mixgroup
	void SetupMixGroupMap(atArray<const audCategory *> &map, audCategory * cat, u32 & mapIndex, MixGroupCategoryMap * hierarchy, u32 nodeHash);
	
	audMetadataManager	m_MetadataMgr;
	
};

#define DYNAMICMIXMGR (g_AudioEngine.GetDynamicMixManager())


}

#if __WIN32
#pragma warning(pop)
#endif

#endif
