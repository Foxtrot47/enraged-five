// 
// audioengine/requestedsettings.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_REQUESTEDSETTINGS_H
#define AUD_REQUESTEDSETTINGS_H

#include "vector/vector3.h"
#include "vectormath/quatv.h"
#include "system/ipc.h"
#include "system/spinlock.h"
#include "audioengine/entity.h"
#include "audioengine/environmentgroup.h"
#include "audioengine/soundpool.h"
#include "audioengine/environment.h"
#include "audioengine/enginedefs.h"
#include "audioengine/widgets.h"
namespace rage
{

#define AUD_ENABLE_RECORDER_INTERFACE (RSG_PC || RSG_DURANGO || RSG_ORBIS)

#if AUD_ENABLE_RECORDER_INTERFACE
#define AUD_RECORDER_ONLY(...)			__VA_ARGS__

	class audSoundRecorderInterface
	{
	public:
		virtual void OnDelete(audSound*){}

		virtual void OnStop(audSound*){}
		virtual void OnStopAndForget(audSound*){}

		virtual void OnSetClientVariable(audSound*, f32){}
		virtual void OnSetClientVariable(audSound*, u32){}

		virtual void OnSetRequestedVolume(audSound*, const float){}
		virtual void OnSetRequestedPostSubmixVolumeAttenuation(audSound*, const f32){}
		virtual void OnSetRequestedVolumeCurveScale(audSound*, const f32){}
		virtual void OnSetRequestedPitch(audSound*, const s32){}
		virtual void OnSetRequestedFrequencyRatio(audSound*, const f32){}
		virtual void OnSetRequestedPan(audSound*, const s32){}
		virtual void OnSetRequestedPosition(audSound*, Vec3V_In){}

		virtual void OnSetRequestedOrientation(audSound*, QuatV_In){}
		virtual void OnSetRequestedOrientation(audSound*, audCompressedQuat){}
		virtual void OnSetRequestedOrientation(audSound*, Mat34V_In){}
		virtual void OnSetRequestedSourceEffectMix(audSound*, const f32, const f32){}
		virtual void OnSetRequestedShouldAttenuateOverDistance(audSound*, TristateValue){}
		virtual void OnSetRequestedQuadSpeakerLevels(audSound*, const f32*){}
		virtual void OnSetRequestedDopplerFactor(audSound*, const f32){}
		virtual void OnSetRequestedVirtualisationScoreOffset(audSound*, const u32){}
		virtual void OnSetRequestedSpeakerMask(audSound*, const u8){}
		virtual void OnSetRequestedListenerMask(audSound*, const u8){}
		virtual void OnSetRequestedEnvironmentalLoudness(audSound*, const u8){}
		virtual void OnSetRequestedEnvironmentalLoudnessFloat(audSound*, const f32){}
		virtual void OnSetRequestedLPFCutoff(audSound*, const u32){}
		virtual void OnSetRequestedLPFCutoff(audSound*, const f32){}
		virtual void OnSetRequestedHPFCutoff(audSound*, const u32){}
		virtual void OnSetRequestedHPFCutoff(audSound*, const f32){}
		virtual void OnSetRequestedFrequencySmoothingRate(audSound*, const f32){}
	};	 

#else
#define AUD_RECORDER_ONLY(...)
#endif // AUD_ENABLE_RECORDER_INTERFACE


struct tRequestedSettings
{
	// PURPOSE
	//	All requested settings, i.e. settings that come from code, that can be varied during the life of the sound
	//  are cached three times to allow lock-free thread safe updates.
	f32				Volume;	
	f32				PostSubmixVolumeAttenuation;
	f32				SourceEffectWetMix;
	f32				SourceEffectDryMix;
	
	s16				Pitch;
	
	f32				DopplerFactor;
	f32				VolumeCurveScale;
	u16				LPFCutoff;
	u16				HPFCutoff;
	u8				SpeakerMask;
	u8				EnvironmentalLoudness;
	u8				ListenerMask;

	u16				Pan : 9;
	u16				UpdateFrame : 4;
	
	u16			IsSetToPlay : 1;
	u16			IsSetToStop : 1;
	u16			HasQuadSpeakerLevels : 1;

	enum {InvalidPanValue = 0x1FF};
};

class audSound;
// Sounds that can be accessed by game-code (top level, code-triggered sounds) have a RequestedSettings structure. 
class audRequestedSettings
{
public:

	struct Indices
	{
		u32 readIndex;
		u32 writeIndex;
		u32 nextWriteIndex;
		u32 previousWriteIndex;
	};

	AUD_RECORDER_ONLY(static audSoundRecorderInterface* sm_pRecorder);
	AUD_RECORDER_ONLY(static void SetRecorder(audSoundRecorderInterface* pRecorder)	{	sm_pRecorder = pRecorder;	});

#if !__SPU
	// PURPOSE
	//	Allocates memory for the requested settings pool
	static void InitClass();

	// PURPOSE
	//	Frees memory used by the requested settings pool
	static void ShutdownClass();

	// PURPOSE
	//	Commits the requested settings by advancing the active game write index.
	//  We update the audio thread index from the Audio thread, so it stays constant during an audio frame
	// NOTES
	//	Called from the game thread
	void Commit(const u32 timeInMs, const Indices &indices);
#endif // !__SPU

	audRequestedSettings(audSound* pSnd);

	// Use this fn when you're using a slot for some nefarious purpose, and need to leave the ReqSet in a state that's safe to have it's destructor run.
	// eg the MathOperationSound uses a ReqSet/Sound pool slot for extra storage, and casts that slot to a ReqSet for deletion. As it's full of
	// non-ReqSet data, this can look like it has an environment group when it doesn't, and either screw up the ref count, or assert.
	void ShutdownWhenNotReallyARequestedSetting();

	~audRequestedSettings();

	// PURPOSE
	//  Gets our environment group to update our environment metric 
	void UpdateEnvironmentGameMetric();

	// All Set functions will only be called from the game thread
	void SetPosition(Vec3V_In pos);
	void SetPosition(const Vector3 &position);
	void SetPitch(const s32 pitch);
	void SetPan(const s32 pan);
	void SetVolume(const f32 volume);
	void SetPostSubmixVolumeAttenuation(const f32 volumeAttenuation);
	void SetSourceEffectMix(const f32 wet, const f32 dry);
	void SetShouldAttenuateOverDistance(const TristateValue state);
	void SetShouldDisableRearAttenuation(const TristateValue state);
	void SetShouldDisableRearFiltering(const TristateValue state);
	void SetLowPassFilterCutoff(const u32 cutoff);
	void SetHighPassFilterCutoff(const u32 cutoff);
	void SetFrequencySmoothingRate(const f32 rate);
	void SetIsSetToPlay(const bool setToPlay);
	void SetIsSetToStop(const bool setToStop);
	// PURPOSE
	//	Deprecated; do not use.
	void SetVirtualisationScoreOffset(const u32 UNUSED_PARAM(virtualisationScoreOffset)) {}
	void SetDopplerFactor(const f32 dopplerFactor);
	void SetVolumeCurveScale(const f32 volumeCurveScale);
	void SetSpeakerMask(const u8 speakerMask);
	void SetQuadSpeakerLevels(const f32* speakerLevels);
	void SetEnvironmentalLoudness(const u8 environmentalLoudness);
	void SetListenerMask(const u8 listenerMask);
	void SetEnvironmentalLoudnessFloat(const f32 environmentalLoudness); // call it something different to match the Getter
	
	// These Get functions must only be called from the game thread
	Vec3V_Out GetPosition() const;
	s32 GetPitch() const;
	s32 GetPan() const;
	f32 GetVolume() const;
	f32 GetPostSubmixVolumeAttenuation() const;
	f32 GetSourceEffectWetMix() const;
	f32 GetSourceEffectDryMix() const;
	bool GetHasQuadSpeakerLevels() const;
	u32 GetLowPassFilterCutoff() const;
	u32 GetHighPassFilterCutoff() const;
	bool GetIsSetToPlay() const;
	f32 GetDopplerFactor() const;
	f32 GetVolumeCurveScale() const;
	u8 GetSpeakerMask() const;
	u8 GetEnvironmentalLoudness() const;
	u8 GetListenerMask() const;
	f32 GetEnvironmentalLoudnessFloat() const;
	bool GetIsSetToStop() const;
	u32 GetVirtualisationScoreOffset() const { return 0; }

	// These Get functions must only be called from the audio thread

	const tRequestedSettings &GetSettings_AudioThread() const
	{
		return m_Settings[GetReadIndex()];
	}

	const Vec3V &GetPosition_AudioThread() const
	{
		return m_SettingsPosition[GetReadIndex()];
	}

	f32 GetFrequencySmoothingRate() const
	{
		return m_FrequencySmoothingRate;
	}

	__forceinline TristateValue GetShouldAttenuateOverDistance() const { return static_cast<TristateValue>(m_ShouldAttenuateOverDistance); }
	__forceinline TristateValue GetShouldDisableRearFiltering() const { return static_cast<TristateValue>(m_ShouldDisableRearFiltering); }
	__forceinline TristateValue GetShouldDisableRearAttenuation() const { return static_cast<TristateValue>(m_ShouldDisableRearAttenuation); }

	void SetEnvironmentGroup(audEnvironmentGroupInterface* environmentGroup);
	audEnvironmentGroupInterface* GetEnvironmentGroup() const;
	// We now have an environment metric in the reqSets itself, linked to the game-specific version
	// by perforce dir mapping. This is to ease spu porting, as we need the metric on an spu.
	audEnvironmentGameMetric &GetEnvironmentGameMetric() { return m_EnvironmentGameMetric; }

	void SetEntityVariableBlock(const int index)
	{
		Assign(m_EntityVariableBlockIndex, index);
		audEntity::AddVariableBlockRef(index);
	}

	int GetEntityVariableBlock(void)
	{
		return m_EntityVariableBlockIndex;
	}
	
	void RequestPrepare(const s16 waveSlotIndex, const bool isAllowedToLoad, const u32 prepareTimeLimit, const u32 priority)
	{
		m_IsAllowedToLoad = isAllowedToLoad;
		if(waveSlotIndex == -1)
		{
			m_WaveSlotIndex = 0xff;
		}
		else
		{
			Assign(m_WaveSlotIndex, (u32)waveSlotIndex);
		}
		
		// Prepare time limit is absolute time, not relative to request time so we need the full 32bits.
		m_PrepareTimeLimit = prepareTimeLimit;
		audAssertf(priority < 4, "Invalid wave load priority: %u", priority);
		m_WaveLoadPriority = priority;
		
		sys_lwsync();
		m_IsPrepareRequested = true;
	}

	bool IsPrepareRequested() const
	{
		return m_IsPrepareRequested;
	}

	bool IsAllowedToLoad() const
	{
		return m_IsAllowedToLoad;
	}

	s16 GetWaveSlotIndex() const
	{
		return m_WaveSlotIndex == 0xff ? -1 : static_cast<s16>(m_WaveSlotIndex);
	}

	u32 GetWaveLoadPriority() const
	{
		return m_WaveLoadPriority;
	}

	u32 GetPrepareTimeLimit() const
	{
		return m_PrepareTimeLimit;
	}

	void SetUpdateEntity(const bool shouldUpate)
	{
		m_ShouldUpdateEntity = shouldUpate;
	}

	bool ShouldUpdateEntity() const
	{
		return m_ShouldUpdateEntity;
	}

	bool IsAllowOrphaned() const
	{
		return m_AllowOrphaned;
	}

	void SetAllowOrphaned(const bool allowOrphaned)
	{
		m_AllowOrphaned = allowOrphaned;
	}

	void InvalidateEntityPtr()
	{
		m_IsEntityPtrInvalid = true;
	}

	bool IsEntityPtrInvalid() const
	{
		return m_IsEntityPtrInvalid;
	}

	void InvalidateEntityRefPtr()
	{
		m_IsEntityRefPtrInvalid	 = true;
	}

	bool IsEntityRefPtrInvalid() const
	{
		return m_IsEntityRefPtrInvalid;
	}

	void InvalidateTrackerPtr()
	{
		m_IsTrackerPtrInvalid = true;
	}

	bool IsTrackerPtrInvalid() const
	{
		return m_IsTrackerPtrInvalid;
	}

	void SetReleaseTime(const s32 releaseTime)
	{
		Assign(m_ReleaseTime, releaseTime);
	}

	s32 GetReleaseTime() const
	{
		return m_ReleaseTime;
	}

	bool IsReferencedDirectlyByGame() const
	{
		return m_IsReferencedDirectlyByGame;
	}

	void SetIsReferencedDirectlyByGame(const bool isReferenced)
	{
		m_IsReferencedDirectlyByGame = isReferenced;
	}

	void GetVelocity(Vec3V_InOut velocity) const
	{
		m_Velocity.Get(velocity);
	}

	void SetMixerPlayTime(const u32 playTimeMixerFrames, const u16 subFrameOffset)
	{
		m_PlayTimeMixerFrames = playTimeMixerFrames;
		m_PlayTimeMixerSubFrameOffset = subFrameOffset;
	}

	bool HasMixerPlayTime(u32& playTime, u16& subFrameOffset);

	u32 GetGameTimeStep() const
	{
		return m_TimeStep;
	}

	void GetOrientation(QuatV_InOut quat) const
	{
		m_Orientation.Get(quat);
	}

	void SetOrientation(QuatV_In ori)
	{
		m_Orientation.Set(ori);
		AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedOrientation(m_ParentSound, ori); })
	}
	void SetOrientation(audCompressedQuat ori)
	{
		m_Orientation = ori;
		AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedOrientation(m_ParentSound, ori); })
	}
	void SetClientVariable(f32 val)
	{
		m_ClientVariable.f32Val = val;
		AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetClientVariable(m_ParentSound, val); })
	}
	void SetClientVariable(u32 val)
	{
		m_ClientVariable.u32Val = val;
		AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetClientVariable(m_ParentSound, val); })
	}
	void GetClientVariable(f32 &val) const 
	{
		val = m_ClientVariable.f32Val;
	}
	void GetClientVariable(u32 &val) const
	{
		val = m_ClientVariable.u32Val;
	}
	
	void SetSyncMasterId(const u32 syncId);
	void SetSyncSlaveId(const u32 syncId);

	u32 GetSyncMasterId() const
	{
		return m_SyncMasterId;
	}

	u32 GetSyncSlaveId() const
	{
		return m_SyncSlaveId;
	}

	void SetPositionRelativePanDamping(f32 positionRelativePanDamping)
	{
		m_PositionRelativePanDamping = positionRelativePanDamping;
	}
	f32 GetPositionRelativePanDamping() const
	{
		return m_PositionRelativePanDamping;
	}

	void SetPositionRelativePan(bool positionRelativePan)
	{
		m_PositionRelativePan = positionRelativePan;
	}
	bool ShouldPositionRelativePan() const
	{
		return m_PositionRelativePan;
	}

	void operator delete(void *ptr);
	
	static void IncrementWriteIndex()
	{
		// Update this controller's RequestedSetting write index - this will get picked up at the next SoundManager Update()
		// 05/09/06 MdS - this is the new way of doing this, to ensure atomic changes across sounds, and so solve a bunch
		// of game-referenced-child-sound issues. 
		// This needs to be done AFTER the sounds are individually updated, as CommitRequestedSettings needs to have
		// been called on them first.
		u32 writeIndex = GetWriteIndex();
		writeIndex = (writeIndex + 1) % g_audRequestedSettingsBuffers;
		SetWriteIndex(writeIndex);
	}

	static u32 GetWriteIndex()
	{
		return sm_RequestedSettingsWriteIndex;
	}

	static u32 GetReadIndex()
	{
		return sm_RequestedSettingsReadIndex;
	}

	static void UpdateReadIndex(const u32 unwrappedWriteIndex)
	{
		u32 newReadIndex = (unwrappedWriteIndex + g_audRequestedSettingsBuffers - 1) % g_audRequestedSettingsBuffers;
		SetReadIndex(newReadIndex);
	}

	void SetShouldPersistOverClears(const bool persist) { m_SkipClearAllSounds = persist; }
	bool ShouldPersistOverClears() const { return m_SkipClearAllSounds; }

#if !__SPU
	
	static void ComputeIndices(audRequestedSettings::Indices &reqSetIndices)
	{
		reqSetIndices.writeIndex = GetWriteIndex();
		reqSetIndices.nextWriteIndex = (reqSetIndices.writeIndex+1)%g_audRequestedSettingsBuffers;
		reqSetIndices.previousWriteIndex = (reqSetIndices.writeIndex-1+g_audRequestedSettingsBuffers)%g_audRequestedSettingsBuffers;
		reqSetIndices.readIndex = GetReadIndex();
	}

	private:
#endif

	static void SetReadIndex(const u32 readIndex)
	{
		sm_RequestedSettingsReadIndex = readIndex;
	}
	
private:
	
	static void SetWriteIndex(const u32 writeIndex)
	{
		sm_RequestedSettingsWriteIndex = writeIndex;
	}

	audSound *m_ParentSound; 

	Vec3V			m_SettingsPosition[g_audRequestedSettingsBuffers];	
	audEnvironmentGameMetric m_EnvironmentGameMetric;
	tRequestedSettings m_Settings[g_audRequestedSettingsBuffers];

	audCompressedVelocity m_Velocity; // In meters per 30ms - we do all velocity time adjustments high up on the game frame.	
	audCompressedQuat m_Orientation; 
	
	u32 m_TimeStep; //The duration of the current timestep

	// don't need this to be atomic across the game frame
	f32				m_FrequencySmoothingRate;

	union
	{
		u32 u32Val;
		f32 f32Val;
	}m_ClientVariable;

	f32 m_PositionRelativePanDamping;
	u32 m_PrepareTimeLimit;
	u32 m_LastTimeUpdated;
	u32 m_PlayTimeMixerFrames;
	u16 m_PlayTimeMixerSubFrameOffset;
	audEnvironmentGroupInterface* m_EnvironmentGroup;
	
	s16 m_ReleaseTime;	
	u16 m_SyncMasterId;
	u16 m_SyncSlaveId;
    s16 m_EntityVariableBlockIndex;

	u8 m_WaveSlotIndex;

	u8 m_WaveLoadPriority : 2;
	u8 m_ShouldAttenuateOverDistance : 2;	
	u8 m_ShouldDisableRearFiltering : 2;
	u8 m_ShouldDisableRearAttenuation : 2;
	bool m_IsPrepareRequested : 1;
	bool m_AllowOrphaned : 1;
	bool m_ShouldUpdateEntity : 1;
	bool m_IsAllowedToLoad : 1;
	bool m_IsEntityPtrInvalid : 1;
	bool m_IsEntityRefPtrInvalid : 1;
	bool m_IsTrackerPtrInvalid : 1;
	bool m_IsReferencedDirectlyByGame : 1;
	bool m_PositionRelativePan : 1;
	bool m_SkipClearAllSounds : 1;

	static bool sm_IgnoreBlockedAudioThread;

	// PURPOSE
	//  The write index is updated once per game-frame, and the read index updated to be one behind, at the start of each audio frame.
	//  This ensures atomic changes across sounds. There's no nice tls way of doing this, as we need to be updating
	//  the read index on the audio thread.
	static u32 sm_RequestedSettingsReadIndex;
	static u32 sm_RequestedSettingsWriteIndex;

};

}//namespace rage

#endif // AUD_REQUESTEDSETTINGS_H
