//
// audioengine/metadatamanager.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_METADATA_MANAGER_H
#define AUD_METADATA_MANAGER_H

#include "file/asset.h"
#include "string/stringhash.h"
#include "atl/array.h"
#include "atl/map.h"
#include "system/criticalsection.h"
#include "metadataref.h"

namespace rage {

#if __BANK
	struct audObjectDataOverride
	{
		audObjectDataOverride(void *_ptr, const u32 _hash, const u32 _size)
		{
			ptr = _ptr;
			hash = _hash;
			size = _size;
			actualSize = _size;
		}
		audObjectDataOverride(){}
		void *ptr;
		u32 hash;
		u32 size;
		u32 actualSize;
	};
#endif

// PURPOSE
//	Callback interface for RAVE edit notifications
class audObjectModifiedInterface
{
public:
	virtual ~audObjectModifiedInterface(){}
	// PURPOSE
	//	Called when the specified object is modified, but any pointers to that object are still valid
	virtual void OnObjectModified(const u32 UNUSED_PARAM(nameHash)){}
	// PURPOSE
	//	Called when the specified object has been modified in such a way that any pointers are now stale; ie
	//	its size has changed
	virtual void OnObjectOverridden(const u32 UNUSED_PARAM(nameHash)){}
	// PURPOSE
	//	Called when the specified object has been viewed in RAVE (ie displayed in the properties editor)
	virtual void OnObjectViewed(const u32 UNUSED_PARAM(nameHash)){}

	virtual void OnObjectAuditionStart(const u32 UNUSED_PARAM(nameHash)){}
	virtual void OnObjectAuditionStop(const u32 UNUSED_PARAM(nameHash)){}

	virtual void OnMuteListModified() {}
	virtual void OnSoloListModified() {}
};

struct audMetadataChunk
{
	audMetadataChunk()
	{
		chunkNameHash = 0;
		BANK_ONLY(chunkName = NULL);
		metadataPath = NULL;		
		data = NULL;
		dataSize = 0;
		numObjects = 0;
		map = NULL;
		stringTableData = NULL;
		numStrings = 0;
		stringTableSize = 0;
		stringOffsets = NULL;
		stringsBasePtr = NULL;
		objectNameTable = NULL;	
		objectNameTableSize = 0;
		BANK_ONLY(objectSizes = NULL);

		mapBins = NULL;
		ownsStringTable = true;
		enabled = true;
	}

	u32 chunkNameHash;
	BANK_ONLY(const char *chunkName);
	const char *metadataPath;
	u8 *data;
	u32 dataSize;
	u32 numObjects;
	struct audObjectMapEntry
	{
		u32 offset;
		u32 key;
	};
	audObjectMapEntry *map;
	struct MapBin
	{
		u32 startIndex;
		u32 numEntries;
	};
	atRangeArray<MapBin, 256> *mapBins;
	struct audMetadataRefHashEntry
	{
		audMetadataRef metadataRef;
		u32 hash;
	};
	atArray<audMetadataRefHashEntry> metadataRefHashList;
	void *stringTableData;
	u32 numStrings;
	const u32 *stringOffsets;
	const char *stringsBasePtr;
	char *objectNameTable;
	u32 objectNameTableSize;
	u32 stringTableSize;
	bool ownsStringTable;
	bool enabled;
	BANK_ONLY(u32 *objectSizes);
	BANK_ONLY(atArray<audObjectDataOverride> overriddenObjects);

	bool IsObjectInChunk(const void* objectPtr) const
	{
		if(objectPtr && data && objectPtr >= data && objectPtr < data+dataSize)
		{
			return true;
		}
		return false;
	}
};

class audMetadataManager;
class audMetadataObjectInfo
{
public:
	audMetadataObjectInfo() : m_ObjectPtr(NULL), m_MetadataManager(NULL) {};

	friend class audMetadataManager;
	audMetadataObjectInfo(const void *objectPtr, const audMetadataManager *mgr) : m_ObjectPtr(objectPtr), m_MetadataManager(mgr)
	{
	}

	u32 GetType() const { return *((u8*)m_ObjectPtr); }
	
	template <class _T> const _T *GetObject() const
	{
		if( GetType() == _T::TYPE_ID )
		{
			return (_T*)m_ObjectPtr;
		}
		return NULL;
	}

	u32 GetChunkId() const;

	const void *GetObjectUntyped() const
	{
		return m_ObjectPtr;
	}

#if !__FINAL
	const char *GetName_Debug() const;
#endif

	BANK_ONLY(const char *GetName() const);
private:
	const void *m_ObjectPtr;
	const audMetadataManager *m_MetadataManager;
};


// PURPOSE
//	This class takes care of loading object metadata. This includes all object metadata lookup functionality.
//  An instance of audMetadataManager is created for all the various object-metadata-based systems, currently:
//  audSounds, audCurves, audCategories and audEffects.
class audMetadataManager
{
	friend class audMetadataObjectInfo;

public:

	// PURPOSE
	//	Iterates over all metadata objects of a specified type
	// NOTES
	//	This is slow; generate object lists offline via XSL transforms if performance is important.
	class ObjectIterator
	{
	public:
		ObjectIterator(const audMetadataManager &mgr, const u32 typeId) : m_MetadataManager(mgr), m_TypeFilter(typeId), m_CurrentIndex(0) { }
		u32 Next();
		void Reset()	{ m_CurrentIndex = 0; }

	private:
		const audMetadataManager &m_MetadataManager;
		u32 m_CurrentIndex;
		u32 m_TypeFilter;
	};

	audMetadataManager();
	~audMetadataManager();

	// PURPOSE
	//	Initialises the manager by loading the object metadata into memory and building
	//	the object lookup hash map and bank name table.
	// PARAMS
	//	metadataType			- unique identified for this metadata type, used for remote control/RAVE comms
	//	metadataPath			- Path to the metadata file to load.
	//	keepObjectNamesInMemory	- If true, then object names can be resolved, incurs memory hit.
	//	schemaVersion			- Version number that defines the object data format
	//	optimisedMap			- If true then the object map should be sorted by LSB, increasing searhc efficiency
	//	baseChunkName			- the metadata chunk to load as the 'base' chunk: NULL specifies default value "BASE"
	// RETURNS
	//	True if object metadata was successfully loaded, false otherwise.
	enum NameTableOption
	{
		NameTable_Always = 0,
		NameTable_BankOnly,
		NameTable_Never,

		External_NameTable_Always,
		External_NameTable_BankOnly,
		External_NameTable_Never,
	};
	bool Init(const char *metadataType, const char* metadataPath, const NameTableOption nameTableOption, const u32 schemaVersion, const bool optmisedMap = false, const char *baseChunkName = NULL);

	// PURPOSE
	//	Load an extra chunk of metadata
	bool LoadMetadataChunk(const char *chunkName, const char *metadataPath, u8 *dataBuffer = NULL, const u32 dataBufferSize = 0);

	// PURPOSE
	//	Unload the specified metadata chunk
	// RETURNS
	//	True if the specified chunk was found and unloaded, false otherwise
	bool UnloadMetadataChunk(const char *chunkName, const bool deleteDataBuffer = true);

	// PURPOSE
	//	Enables/disables the specified metadata chunk.
	//	NOTES
	//	A disabled chunk will not be searched in GetObject queries
	void SetChunkEnabled(const s32 chunkId, const bool enabled)
	{
		m_Chunks[chunkId].enabled = enabled;
	}

	// PURPOSE
	//	Reserve a slot in the chunk list for the specified name; allowing control over object search order before loading
	void ReserveChunk(const char *chunkName);

	// PURPOSE
	//	Frees all memory used by object metadata and lookup structures.
	void Shutdown();

	//Purpose
	//Can be used to populate an object info that can be used for type checking the matadata (e.g. to switch on the metadata type)
	bool GetObjectInfo(const u32 objectHash, audMetadataObjectInfo & info) const;
	bool GetObjectInfo(const audMetadataRef objectRef, audMetadataObjectInfo &info) const;
	bool GetObjectInfo(const u32 objectNameHash, const u32 chunkNameHash, audMetadataObjectInfo &info) const;
	bool GetObjectInfoFromObjectIndex(const u32 objectIndex, audMetadataObjectInfo &info) const;
	bool GetObjectInfoFromSpecificChunk(const u32 objectNameHash, const u32 chunkId, audMetadataObjectInfo &info) const;

	// PURPOSE
	//	Strongly typed metadata accessors
	//	Asserts that the object in memory has the requested type, and returns NULL if not
	template <class _T> _T *GetObject(const char *objectName) const
	{
		return GetObject<_T>(atStringHash(objectName));
	}

	template <class _T> _T *GetObject(const u32 objectNameHash) const
	{
		return static_cast<_T*>(GetObject_Verify(objectNameHash, _T::TYPE_ID));
	}

	template <class _T> _T *GetObject(const audMetadataRef objectRef) const
	{
		return static_cast<_T*>(GetObject_Verify(objectRef, _T::TYPE_ID));
	}

	u32 GetAssetChangelist(const s32 chunkId = 0) const;

	// PURPOSE
	//	Returns a pointer to the metadata chunk that describes the object specified by name.
	// PARAMS
	//	objectName - The unique string name of the object.
	// RETURNS
	//	Pointer to metadata, or NULL if the requested object doesn't exist.
	void *GetObjectMetadataPtr(const char *objectName) const;

	// PURPOSE
	//	Returns a pointer to the metadata chunk that describes the object specified by name hash.
	// PARAMS
	//	hashValue	- The hash of the unique string name of the object.
	// RETURNS
	//	Pointer to metadata, or NULL if the requested sound doesn't exist.
	void *GetObjectMetadataPtr(const u32 hashValue) const;
	void *GetObjectMetadataPtr(const audMetadataRef metadataRef) const;
	void *GetObjectMetadataPtrFromObjectIndex(const u32 index) const;
	void *GetObjectMetadataPtr(const u32 nameHash, const u32 chunkNameHash) const;
	void *GetObjectMetadataPtrFromSpecificChunk(const u32 hashValue, u32 chunkId) const;
	void *GetObjectMetadataPtrAndChunkId(const u32 hashValue, u32& chunkId) const;

	// PURPOSE
	//	Returns a metadata reference to the object requested by hash
	audMetadataRef GetObjectMetadataRefFromHash(const u32 hashValue) const;

	// PURPOSE
	//	Returns a pointer to the bank name at the specified index in the bank name table.
	// PARAMS
	//	index - index into the bank name table. This can be stored as a reference in object metadata.
	const char *GetStringFromTableIndex(const u32 index) const;

	// PURPOSE
	//	Returns a pointer to the bank name at the specified index in the specified chunk's string table.
	const char *GetStringFromChunkTableFromIndex(const s32 chunkId, const u32 index) const;

	// PURPOSE
	//	Returns the number of strings in the specified chunk's string table
	u32 GetNumStringsInChunkTable(const s32 chunkId) const;

	// PURPOSE
	//	Returns the ID of the specified chunk, -1 if it isn't found
	s32 FindChunkId(const char *chunkName) const;
	s32 FindChunkId(const u32 chunkNameHash) const;

	// PURPOSE
	//	Returns the chunk Id containing the specified object pointer, or -1 if it isn't found
	s32 FindChunkIdForObjectPtr(const void *objPtr) const;

	// PURPOSE
	//	Returns a bank index from the specified reference id
	// NOTES
	//	When running -rave, the parameter is interpreted as a hash, otherwise its treated as an index
	u32 GetStringTableIndexFromMetadataRef(const u32 ref) const;

	// PURPOSE
	//	Returns a bank name index from the specified hash
	u32 GetStringTableIndexFromHashPrecomputed(const u32 hash, const u32* precomputedHashes, const u32 numPrecomputedHashes) const;

	bool AddExtraStringTable(const audMetadataChunk &chunk);
	void RemoveExtraStringTable(u32 chunkNameHash);
	bool LoadExtraStringTable(const char *chunkName, fiStream *stream, const u32 size);

	s32 GetNumChunks() const { return m_Chunks.GetCount(); }
	const audMetadataChunk &GetChunk(const s32 index) const { return m_Chunks[index]; }

	bool IsRAVEConnected() const
	{
#if __BANK
		return m_IsUsingRAVEData;
#else
		return false;
#endif
	}

#if __BANK
	// PURPOSE
	//	Overrides an object's metadata - whenever this object is referenced (by hash) the overridden 
	//	data will be provided instead of the original data.
	// NOTES
	//	If this object is found in the overridden object list then the old buffer will
	//	be delete[]'d.
	void AddObjectOverride(const char *objectName, const u32 objectNameHash, const u32 chunkNameHash, void *ptr, const u32 size);

	void DrawOverriddenObjects() const;

#endif

	// PURPOSE
	//	Specify the object that should receive notifications for RAVE object editing actions
	void SetObjectModifiedCallback(audObjectModifiedInterface *BANK_ONLY(notifier))
	{
#if __BANK
		m_ObjectModifiedCallback = notifier;
#endif
	}

	// PURPOSE
	// Return the hash of the metadata type name
	u32 GetMetadataTypeHash() { return m_MetadataTypeHash; }

	// PURPOSE
	//  Returns the number of objects in the metadata file. For sounds/curves/? this isn't useful, as we may instantiate
	//  lots of any one definition, but for categories, we create just one of each, hence need to know this to accurately
	//  size our array.
	// This will be 0 if we've not yet initialised.
	// NOTES
	//	This will change as chunks are loaded/unloaded, and will count 'shadowed' objects as unique objects
	u32 ComputeNumberOfObjects() const;

	// PURPOSE
	//	Returns the name of an object from the name offset stored in metadata.
	// NOTES
	//	In a release build we may not keep this lookup in memory for objects, in that case this function will
	//	always return NULL.
#if __BANK
	const char *GetObjectName(const audMetadataRef metadataRef) const;
	const char *GetObjectName(const u32 nameHash) const;
#endif

	// PURPOSE
	// Get the object hash from the given metadata ref
	u32 GetObjectHashFromMetadataRef(const audMetadataRef metadataRef) const;

	// PURPOSE
	// Deprecated - here only to aid integration back into V
	const char *GetObjectName(const u32 nto, const u32 chunkId) const;


	// PURPOSE
	//	Version of GetObjectNameFromNameTableOffset that Asserts is available in all builds and asserts that the names are always available
	const char *GetNameFromNTO_Always(const u32 nameTableOffset) const;
	

#if !RSG_FINAL || __FINAL_LOGGING
	// PURPOSE
	//	Version of GetObjectNameFromNameTableOffset that is available in all builds but can return NULL if the name table is not available
	const char *GetNameFromNTO_Debug(const u32 nameTableOffset) const;
#endif

	// PURPOSE
	//	Deprecated; code should use GetNameFromNTO_Debug or GetNameFromNTO_Always
	const char *GetObjectNameFromNameTableOffset(const u32 nameTableOffset) const;

	bool AreObjectNamesAvailable() const { return m_NameTableOption == NameTable_Always || (__BANK&&m_NameTableOption == NameTable_BankOnly)
													|| m_NameTableOption == External_NameTable_Always || (__BANK&&m_NameTableOption == External_NameTable_BankOnly); }

	bool AreObjectNamesAlwaysAvailable() const { return m_NameTableOption == NameTable_Always || m_NameTableOption == External_NameTable_Always; }
	// PURPOSE
	//	Returns the bank id for the specified bank name
	u32 GetStringIdFromName(const char *string) const;
	u32 GetStringIdFromHash(const u32 hash) const;

#if __BANK
	
	void OnObjectViewCallback(const u32 hash) const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnObjectViewed(hash);
		}
	}

	void OnObjectModifiedCallback(const u32 hash) const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnObjectModified(hash);
		}
	}

	void OnObjectOveriddenCallback(const u32 hash) const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnObjectOverridden(hash);
		}
	}

	void OnObjectAuditionStartCallback(const u32 hash) const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnObjectAuditionStart(hash);
		}
	}

	void OnObjectAuditionStopCallback(const u32 hash) const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnObjectAuditionStop(hash);
		}
	}

	void OnSoloListModifiedCallback() const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnSoloListModified();
		}
	}

	void OnMuteListModifiedCallback() const
	{
		if(m_ObjectModifiedCallback)
		{
			m_ObjectModifiedCallback->OnMuteListModified();
		}
	}

	// PURPOSE
	//	Update in-memory data with this object edit
	bool OnObjectEdit(const char *objectName, const u32 chunkNameHash, const u32 objectSize, u8 *objectData);

	u32 GetObjectSize(const char *objectName, const u32 chunkNameHash) const;

	void DisableEditsForObjectType(const u32 typeId);

	bool IsObjectMuted(const u32 objectNameHash) const { return m_MuteList.Find(objectNameHash) != -1; }
	bool IsObjectSoloed(const u32 objectNameHash) const { return m_SoloList.Find(objectNameHash) != -1; }
	bool AreAnyObjectsSoloed() const { return m_SoloList.GetCount() > 0; }

	void ResetSoloList() { m_SoloList.Reset(); OnSoloListModifiedCallback(); }
	void AddToSoloList(const u32 nameHash) { m_SoloList.PushAndGrow(nameHash); OnSoloListModifiedCallback(); }

	void ResetMuteList() { m_MuteList.Reset(); OnMuteListModifiedCallback(); }
	void AddToMuteList(const u32 nameHash) { m_MuteList.PushAndGrow(nameHash); OnMuteListModifiedCallback(); }
#endif

	u32 GetLoadedDataSize() const;
	u32 GetLoadedNameTableSize() const;

	bool HasExternalNameTable() const { return m_NameTableOption >= External_NameTable_Always; }
private:
	bool LoadMetadataChunk_Internal(const char *chunkName, const char *metadataPath, u8 *dataBuffer = NULL, const u32 dataBufferSize = 0);
	void AddMetadataRefHashEntry(audMetadataChunk& chunk, audMetadataRef metadataRef, u32 hash);
	void LoadExternalNameTable(audMetadataChunk &chunk, fiStream *stream, const u32 nameTableSize);

	void ComputeStringTableMap();
	void ComputeMap(audMetadataChunk &chunk);

	bool SearchForObjectFromHash(const u32 hash, u32 &chunkId, u32 &offset) const;
	bool SearchForObjectFromHash(const u32 hash, const audMetadataChunk &chunk, u32 &offset) const;
	u32 SearchForObjectIndexFromHash(const u32 hash, const audMetadataChunk &chunk) const;

	void *GetObject_Verify(const u32 objectNameHash, const u32 typeId) const;
	void *GetObject_Verify(const audMetadataRef objectMetadataRef, const u32 typeId) const;

	// PURPOSE
	//	Add an entry into the object lookup hash map.
	// PARAMS
	//	objectName			- Key.
	//	p					- Value.
	//	currentHashIndex	- The index into the hash table that will store this value.
	void AddObjectPtrMapEntry(const char *objectName, void *p, u32 &currentHashIndex);

	// PURPOSE
	//	Parse object map and build hash table.
	// PARAMS
	//	stream - stream containing the data, this should already be at the correct stream position.
	// RETURNS
	//	True if sound map was successfully loaded, false otherwise.
	bool BuildObjectMap(audMetadataChunk &chunk, fiStream *stream);
	bool BuildObjectMap_External(audMetadataChunk &chunk, fiStream *stream);

	// PURPOSE
	//	Load object metadata into memory.
	// PARAMS
	//	stream - stream containing the metadata, this should already be at the correct stream position.
	// RETURNS
	//	True on success, false otherwise.
	bool LoadMetadata(audMetadataChunk &chunk, fiStream *stream,  u8 *dataBuffer, const u32 dataBufferSize);

	// PURPOSE
	//	Builds bank name lookup table.
	// PARAMS
	//	stream - stream containing the metadata, this should already be at the correct stream position.
	// RETURNS
	//	True on success, false otherwise.
	bool LoadStringTable(audMetadataChunk &chunk, fiStream *stream);
	bool LoadStringTable(audMetadataChunk &chunk, fiStream *stream, s32 size);

	// PURPOSE
	//	Sorts the hash table in ascending hash order.
	void SortHashTable(audMetadataChunk &chunk);

	// PURPOSE
	//	Deletes all memory allocated for the specified chunk
	void DeleteChunk(audMetadataChunk &chunk, const bool deleteDataBuffer);

	void ComputeCrossChunkReferences();

	// PURPOSE
	//	Reserve space in the global (for this metadata manager) string table.  The block of strings will be reserved against this chunkId.
	// RETURNS
	//	The start ID to be used for strings in this chunk
	// NOTES
	//	String IDs must remain valid regardless of other chunks loading in/unloading, so unloading a chunk does not free up the table reservation.  When a
	//	 chunk is loaded again it's string table will be mapped to the same range.  As part of this we rely on chunk IDs being preserved.
	u32 ReserveStringsForChunk(const u32 chunkId, const u32 numStrings);

	// PURPOSE
	//	Scans the string table reservations and returns the global ID of the first string in this chunk
	u32 FindFirstStringIdForChunk(const u32 chunkId) const;

	// PURPOSE
	//	Reserve space in the global (for this metadata manager) name table.  The block of virtual name heap space will be reserved against this chunk Id
	// RETURNS
	//	The starting virt offset for this chunk
	u32 ReserveNameHeapForChunk(const u32 chunkId, const u32 nameTableSize);

	// PURPOSE
	//	Scans the virtual name table reservations and returns the starting offset for this chunk
	u32 FindFirstNameTableOffsetForChunk(const u32 chunkId) const;

#if __BANK
	// PURPOSE
	//	Returns a pointer to overridden metadata, NULL if this object hasn't been
	//	overridden.
	// NOTES
	//	Only present on BANK builds
	void *FindOverriddenObjectDataByHash(const u32 hash) const;
	void *FindOverriddenObjectDataByHash(const u32 hash, const u32 chunkId) const;
	bool ShouldIgnoreEditForObjectTypeId(const u32 typeId) const;

	audObjectModifiedInterface *m_ObjectModifiedCallback;
	atArray<const char *> m_NewObjectNameList;

	atArray<u32> m_SoloList;
	atArray<u32> m_MuteList;

	atArray<u8> m_IgnoreEditTypeIds;
	bool m_IsUsingRAVEData;
#endif // __BANK

	atArray<audMetadataChunk> m_Chunks;

	struct StringTableReservation
	{
		u32 chunkId;
		u32 numStrings;
	};
	atArray<StringTableReservation> m_StringTableReservations;

	struct NameTableReservation
	{
		u32 chunkId;
		u32 nameTableSize;
	};
	atArray<NameTableReservation> m_NameTableReservations;

	atMap<u32, u16> m_StringTableIdMap;
	
	mutable sysCriticalSectionToken m_CritSec;
	u32						m_MetadataTypeHash;
	u32						m_SchemaVersion;
	NameTableOption			m_NameTableOption;
	bool					m_OptimisedMap;
	bool					m_BuildMetadataRefHashList;

};

} // namespace rage

#endif // AUD_METADATA_MANAGER_H
