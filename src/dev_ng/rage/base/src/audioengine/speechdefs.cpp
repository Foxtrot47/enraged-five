// 
// speechdefs.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure functions - do not edit.
// 

#include "speechdefs.h"
#include "string/stringhash.h"

namespace rage
{
	// 
	// VariationGroupInfoData
	// 
	void* VariationGroupInfoData::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1872784924U: return &VariationData;
			default: return NULL;
		}
	}
	
	// 
	// VoiceContextInfo
	// 
	void* VoiceContextInfo::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3166033013U: return &numVariations;
			case 1736835748U: return &bankNameTableIndex;
			default: return NULL;
		}
	}
	
	// 
	// VoiceContextInfoVarGroupData
	// 
	void* VoiceContextInfoVarGroupData::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3147167891U: return &ref;
			default: return NULL;
		}
	}
	
	// 
	// Voice
	// 
	void* Voice::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1721230814U: return &painType;
			default: return NULL;
		}
	}
	
	// 
	// BankNameTable
	// 
	void* BankNameTable::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1980270498U: return &bankName;
			default: return NULL;
		}
	}
	
	// 
	// VoiceContextVariationMask
	// 
	void* VoiceContextVariationMask::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1311063357U: return &variationMask;
			default: return NULL;
		}
	}
	
	// 
	// Enumeration Conversion
	// 
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gSpeechGetBaseTypeId(const rage::u32 classId)
	{
		switch(classId)
		{
			case VariationGroupInfoData::TYPE_ID: return VariationGroupInfoData::BASE_TYPE_ID;
			case VoiceContextInfo::TYPE_ID: return VoiceContextInfo::BASE_TYPE_ID;
			case VoiceContextInfoVarGroupData::TYPE_ID: return VoiceContextInfoVarGroupData::BASE_TYPE_ID;
			case Voice::TYPE_ID: return Voice::BASE_TYPE_ID;
			case BankNameTable::TYPE_ID: return BankNameTable::BASE_TYPE_ID;
			case VoiceContextVariationMask::TYPE_ID: return VoiceContextVariationMask::BASE_TYPE_ID;
			default: return 0xFFFFFFFF;
		}
	}
	
	// PURPOSE - Determines if a type inherits from another type
	bool gSpeechIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId)
	{
		if(objectTypeId == 0xFFFFFFFF)
		{
			return false;
		}
		else if(objectTypeId == baseTypeId)
		{
			return true;
		}
		else
		{
			return gSpeechIsOfType(gSpeechGetBaseTypeId(objectTypeId), baseTypeId);
		}
	}
} // namespace rage
