//
// audioengine/widgets.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "widgets.h"

#include "engineutil.h"
#include "environment.h"
#include "engine.h"
#include "audiohardware/config.h"
#include "math/amath.h"
#include "vectormath/legacyconvert.h"

namespace rage 
{

audSmoother::audSmoother()
{
	m_Initialised = false;
}

void audSmoother::Init(float increaseRate, float decreaseRate)
{
	m_IncreaseRate = increaseRate;
	m_DecreaseRate = decreaseRate;

	m_FirstRequest = true;
	m_BoundsSet = false;
	m_Initialised = true;
	m_Minimum = 0.0f;
	m_Maximum = 0.0f;

	m_LastValue = 0.0f;
	m_LastTime = 0;
}

void audSmoother::Init(float increaseRate, float decreaseRate, float minimum, float maximum)
{
	m_IncreaseRate = increaseRate;
	m_DecreaseRate = decreaseRate;
	m_Minimum = minimum;
	m_Maximum = maximum;
	m_LastValue = 0.0f;
	m_LastTime = 0;
	m_FirstRequest = true;
	m_BoundsSet = true;
	m_Initialised = true;
}

void audSmoother::SetRates(float increaseRate, float decreaseRate)
{
	m_IncreaseRate = increaseRate;
	m_DecreaseRate = decreaseRate;
}

void audSmoother::SetBounds(float minimum, float maximum)
{
	m_Minimum = minimum;
	m_Maximum = maximum;
	m_BoundsSet = true;
}
void audSmoother::SetLastTime(u32 lastTime)
{
	m_LastTime = lastTime;
}

float audSmoother::CalculateValue(float input)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_FirstRequest = false;
		return input;
	}

	const f32 newValueIncreasing = Min(input, (m_LastValue + m_IncreaseRate));
	const f32 newValueDecreasing = Max(input, (m_LastValue - m_DecreaseRate));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_BoundsSet)
	{
		newValue = Clamp(newValue, m_Minimum, m_Maximum);
	}

	m_LastValue = newValue;
	
	return newValue;
}

float audSmoother::CalculateValue(float input, u32 time)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_LastTime = time;
		m_FirstRequest = false;
		return input;
	}

	// Treat time going backwards as time standing still for a frame
	if(m_LastTime > time)
	{
		m_LastTime = time;
	}
	
	const f32 maxIncrease = (time - m_LastTime) * m_IncreaseRate;
	const f32 maxDecrease = (time - m_LastTime) * m_DecreaseRate;
	const f32 newValueIncreasing = Min(input, (m_LastValue + maxIncrease));
	const f32 newValueDecreasing = Max(input, (m_LastValue - maxDecrease));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_BoundsSet)
	{
		newValue = Clamp(newValue, m_Minimum, m_Maximum);
	}

	m_LastValue = newValue;
	m_LastTime = time;
	
	return newValue;
}

float audSmoother::CalculateValue(const float input, const f32 timeStepS)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_FirstRequest = false;
		return input;
	}

	const f32 maxIncrease = timeStepS * 1000.f * m_IncreaseRate;
	const f32 maxDecrease = timeStepS * 1000.f * m_DecreaseRate;
	const f32 newValueIncreasing = Min(input, (m_LastValue + maxIncrease));
	const f32 newValueDecreasing = Max(input, (m_LastValue - maxDecrease));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_BoundsSet)
	{
		newValue = Clamp(newValue, m_Minimum, m_Maximum);
	}

	m_LastValue = newValue;

	return newValue;
}

audSimpleSmoother::audSimpleSmoother()
{
	m_Initialised = false;
	m_FirstRequest = true;
	m_Clamp01 = false;

	m_ChangeRate = 0.0f;
	
	m_LastValue = 0.0f;
	m_LastTime = 0;
}

void audSimpleSmoother::Init(const float changeRate, const bool clamp01)
{
	m_ChangeRate = changeRate;
	m_Clamp01 = clamp01;

	m_FirstRequest = true;
	m_Initialised = true;
}

void audSimpleSmoother::SetRate(const float changeRate)
{
	m_ChangeRate = changeRate;
}

float audSimpleSmoother::CalculateValue(const float input)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_FirstRequest = false;
		return input;
	}

	const f32 newValueIncreasing = Min(input, (m_LastValue + m_ChangeRate));
	const f32 newValueDecreasing = Max(input, (m_LastValue - m_ChangeRate));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_Clamp01)
	{
		newValue = Clamp(newValue, 0.f, 1.f);
	}

	m_LastValue = newValue;

	return newValue;
}

float audSimpleSmoother::CalculateValue(const float input, const u32 time)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_LastTime = time;
		m_FirstRequest = false;
		return input;
	}

	// Treat time going backwards as time standing still for a frame
	if(m_LastTime > time)
	{
		m_LastTime = time;
	}

	const f32 maxChange = (time - m_LastTime) * m_ChangeRate;
	
	const f32 newValueIncreasing = Min(input, (m_LastValue + maxChange));
	const f32 newValueDecreasing = Max(input, (m_LastValue - maxChange));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_Clamp01)
	{
		newValue = Clamp(newValue, 0.f, 1.f);
	}

	m_LastValue = newValue;
	m_LastTime = time;

	return newValue;
}

float audSimpleSmoother::CalculateValue(const float input, const f32 timeStepS)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_FirstRequest = false;
		return input;
	}

	const f32 maxChange = timeStepS * 1000.f * m_ChangeRate; // m_ChangeRate is specified in milliseconds

	const f32 newValueIncreasing = Min(input, (m_LastValue + maxChange));
	const f32 newValueDecreasing = Max(input, (m_LastValue - maxChange));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_Clamp01)
	{
		newValue = Clamp(newValue, 0.f, 1.f);
	}

	m_LastValue = newValue;
	
	return newValue;
}

audSimpleSmootherDiscrete::audSimpleSmootherDiscrete()
{
	m_Initialised = false;
	m_FirstRequest = true;
	m_Clamp01 = false;

	m_IncreaseRate = 1.0f;
	m_DecreaseRate = 1.0f;

	m_LastValue = 0.0f;
	m_LastTime = 0;
}

void audSimpleSmootherDiscrete::Init(const float increaseRate, const float decreaseRate, const bool clamp01)
{
	m_IncreaseRate = increaseRate;
	m_DecreaseRate = decreaseRate;
	m_Clamp01 = clamp01;

	m_FirstRequest = true;
	m_Initialised = true;
}

void audSimpleSmootherDiscrete::SetRates(const float increaseRate, const float decreaseRate)
{
	m_IncreaseRate = increaseRate;
	m_DecreaseRate = decreaseRate;
}

float audSimpleSmootherDiscrete::CalculateValue(const float input)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_FirstRequest = false;
		return input;
	}

	const f32 newValueIncreasing = Min(input, (m_LastValue + m_IncreaseRate));
	const f32 newValueDecreasing = Max(input, (m_LastValue - m_DecreaseRate));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_Clamp01)
	{
		newValue = Clamp(newValue, 0.f, 1.f);
	}

	m_LastValue = newValue;

	return newValue;
}

float audSimpleSmootherDiscrete::CalculateValue(const float input, const u32 time)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_LastTime = time;
		m_FirstRequest = false;
		return input;
	}

	// Treat time going backwards as time standing still for a frame
	if(m_LastTime > time)
	{
		m_LastTime = time;
	}

	const f32 maxIncrease = (time - m_LastTime) * m_IncreaseRate;
	const f32 maxDecrease = (time - m_LastTime) * m_DecreaseRate;
	const f32 newValueIncreasing = Min(input, (m_LastValue + maxIncrease));
	const f32 newValueDecreasing = Max(input, (m_LastValue - maxDecrease));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_Clamp01)
	{
		newValue = Clamp(newValue, 0.f, 1.f);
	}

	m_LastValue = newValue;
	m_LastTime = time;

	return newValue;
}


float audSimpleSmootherDiscrete::CalculateValue(const float input, const f32 timeStepS)
{
	// Ensure we're initialised
	if (!m_Initialised)
	{
		Assert(0);
		return 0.0f;
	}

	// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
	// CalculateValue(0.0f - or whatever) straight after initialisation.
	if (m_FirstRequest)
	{
		m_LastValue = input;
		m_FirstRequest = false;
		return input;
	}

	const f32 maxIncrease = timeStepS * 1000.f * m_IncreaseRate;
	const f32 maxDecrease = timeStepS * 1000.f * m_DecreaseRate;
	const f32 newValueIncreasing = Min(input, (m_LastValue + maxIncrease));
	const f32 newValueDecreasing = Max(input, (m_LastValue - maxDecrease));
	f32 newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

	if (m_Clamp01)
	{
		newValue = Clamp(newValue, 0.f, 1.f);
	}

	m_LastValue = newValue;

	return newValue;
}

void audFluctuatorProbBased::Init(float increaseRate, float decreaseRate,
			float bandOneMinimum, float bandOneMaximum,
			float bandTwoMinimum, float bandTwoMaximum,
			float intrabandFlipProbability, float interbandFlipProbability,
			float initialValue)
{
	m_SmootherIncreaseRate = increaseRate;
	m_SmootherDecreaseRate = decreaseRate;

	m_SmootherLastValue = 0.0f;

	m_BandOneMinimum = bandOneMinimum;
	m_BandOneMaximum = bandOneMaximum;
	m_BandTwoMinimum = bandTwoMinimum;
	m_BandTwoMaximum = bandTwoMaximum;
	m_IntrabandFlipProbability = intrabandFlipProbability;
	m_InterbandFlipProbability = interbandFlipProbability;
	m_InitialValue = initialValue;

	m_IntrabandState = audEngineUtil::ResolveProbability(0.5f);
	m_InterbandState = audEngineUtil::ResolveProbability(0.5f);

	m_FirstRequest = true;
}

void audFluctuatorProbBased::SetRates(float increaseRate, float decreaseRate)
{
	m_SmootherIncreaseRate = increaseRate;
	m_SmootherDecreaseRate = decreaseRate;
}

void audFluctuatorProbBased::SetBands(float bandOneMinimum, float bandOneMaximum,
				float bandTwoMinimum, float bandTwoMaximum)
{
	m_BandOneMinimum = bandOneMinimum;
	m_BandOneMaximum = bandOneMaximum;
	m_BandTwoMinimum = bandTwoMinimum;
	m_BandTwoMaximum = bandTwoMaximum;
}

void audFluctuatorProbBased::SetFlipProbabilities(float intrabandFlipProbability, float interbandFlipProbability)
{
	m_IntrabandFlipProbability = intrabandFlipProbability;
	m_InterbandFlipProbability = interbandFlipProbability;
}

float audFluctuatorProbBased::CalculateValue()
{
	// If it's our first request, return the requested first value, as it's hard to know what's a good default.
	if (m_FirstRequest)
	{
		m_SmootherLastValue = m_InitialValue;
		m_FirstRequest = false;
		return m_SmootherLastValue;
	}

	// Work out what our new states are
	if (audEngineUtil::ResolveProbability(m_IntrabandFlipProbability))
	{
		m_IntrabandState = !m_IntrabandState;
	}
	if (audEngineUtil::ResolveProbability(m_InterbandFlipProbability))
	{
		m_InterbandState = !m_InterbandState;
	}

	// Calculate our desired value
	float desiredValue = 0.0f;
	if (m_InterbandState)
	{
		if (m_IntrabandState)
		{
			desiredValue = m_BandTwoMaximum;
		}
		else
		{
			desiredValue = m_BandTwoMinimum;
		}
	}
	else
	{
		if (m_IntrabandState)
		{
			desiredValue = m_BandOneMaximum;
		}
		else
		{
			desiredValue = m_BandOneMinimum;
		}
	}

	const f32 newValueIncreasing = Min(desiredValue, (m_SmootherLastValue + m_SmootherIncreaseRate));
	const f32 newValueDecreasing = Max(desiredValue, (m_SmootherLastValue - m_SmootherDecreaseRate));
	f32 newValue = Selectf(desiredValue - m_SmootherLastValue, newValueIncreasing, newValueDecreasing);
	m_SmootherLastValue = newValue;
	return m_SmootherLastValue;
}





void audFluctuatorTimeBased::Init(float increaseRate, float decreaseRate,
								  float minimumValue, float maximumValue,
								  u32 minimumSwitchTime, u32 maximumSwitchTime,
								  float initialValue)
{
	m_MinimumValue = minimumValue;
	m_MaximumValue = maximumValue;

	m_SmootherIncreaseRate = increaseRate;
	m_SmootherDecreaseRate = decreaseRate;

	m_MinimumSwitchTime = minimumSwitchTime;
	m_MaximumSwitchTime = maximumSwitchTime;
	m_Value = initialValue;

	m_NextSwitchTime = 0;
	m_SmootherLastValue = 0.0f;
	m_SmootherLastTime = 0;

	m_FirstRequest = true;
}

void audFluctuatorTimeBased::SetRates(float increaseRate, float decreaseRate)
{
	m_SmootherIncreaseRate = increaseRate;
	m_SmootherDecreaseRate = decreaseRate;
}

void audFluctuatorTimeBased::SetValues(float minimumValue, float maximumValue)
{
	m_MinimumValue = minimumValue;
	m_MaximumValue = maximumValue;
}

void audFluctuatorTimeBased::SetSwitchingTimes(u32 minimumSwitchTime, u32 maximumSwitchTime)
{
	m_MinimumSwitchTime = minimumSwitchTime;
	m_MaximumSwitchTime = maximumSwitchTime;
}

float audFluctuatorTimeBased::CalculateValue(u32 timeInMs)
{
	// If it's our first request, return the requested first value, as it's hard to know what's a good default.
	if (m_FirstRequest)
	{
		m_FirstRequest = false;
		m_NextSwitchTime = timeInMs + (u32)audEngineUtil::GetRandomNumberInRange((s32)m_MinimumSwitchTime,(s32)m_MaximumSwitchTime);
		m_SmootherLastValue = m_Value;
		m_SmootherLastTime = timeInMs;
		return m_SmootherLastValue;
	}

	// Check we haven't reach the time to change.
	if(timeInMs >= m_NextSwitchTime)
	{
		// Calculate our desired value
		m_Value = audEngineUtil::GetRandomNumberInRange(m_MinimumValue,m_MaximumValue);
		m_NextSwitchTime = timeInMs + (u32)audEngineUtil::GetRandomNumberInRange((s32)m_MinimumSwitchTime,(s32)m_MaximumSwitchTime);
	}

	// Treat time going backwards as time standing still for a frame
	if(m_SmootherLastTime > timeInMs)
	{
		m_SmootherLastTime = timeInMs;
	}

	const f32 maxIncrease = (timeInMs - m_SmootherLastTime) * m_SmootherIncreaseRate;
	const f32 maxDecrease = (timeInMs - m_SmootherLastTime) * m_SmootherDecreaseRate;
	const f32 newValueIncreasing = Min(m_Value, (m_SmootherLastValue + maxIncrease));
	const f32 newValueDecreasing = Max(m_Value, (m_SmootherLastValue - maxDecrease));
	f32 newValue = Selectf(m_Value - m_SmootherLastValue, newValueIncreasing, newValueDecreasing);
	newValue = Clamp(newValue, m_MinimumValue, m_MaximumValue);

	m_SmootherLastValue = newValue;
	m_SmootherLastTime = timeInMs;
	return m_SmootherLastValue;
}

audVolumeCone::audVolumeCone()
{
	
}

audVolumeCone::~audVolumeCone()
{
}

#if !__SPU
void audVolumeCone::Init(const u32 curveNameHash, Vec3V_In coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle)
{
	m_Curve.Init(curveNameHash);
	m_ConeDirection = NormalizeFast(coneDirection);
	m_RearAttenuation = rearAttenuation;
	m_InnerConeAngle = innerAngle;
	m_OuterConeAngle = outerAngle;
}
#endif

f32 audVolumeCone::ComputeAttenuation(Mat34V_In matrix, u32 listenerId) const
{
	// Get the position of the microphone
	Vec3V listenerPosition = g_audEnvironment->GetPanningListenerPosition(listenerId);
	// Turn that into a position relative to the sound source
	Vec3V translatedPosition = listenerPosition - matrix.GetCol3();

	Vec3V forwards =  Scale(m_ConeDirection.GetX(), matrix.GetCol0()) + 
						Scale(m_ConeDirection.GetY(), matrix.GetCol1()) +
						Scale(m_ConeDirection.GetZ(), matrix.GetCol2());
	float dotProduct = Dot(translatedPosition, forwards).Getf();
	float lengths = Mag(translatedPosition).Getf() * Mag(matrix.GetCol1()).Getf();
	float angle = 0.0f;
	if (lengths != 0.0f)
	{
		angle = AcosfSafe(dotProduct/lengths);
	}
	// This'll be in radians, turn it into degrees
	angle *= RtoD;
	while (angle<0.0f)
	{
		angle += 360.0f;
	}

	float attenuation = m_Curve.CalculateRescaledValue(0.0f, m_RearAttenuation, 
																m_InnerConeAngle,
																m_OuterConeAngle, 
																angle);
	return attenuation;
}


audVolumeConeLite::audVolumeConeLite()
{

}

audVolumeConeLite::~audVolumeConeLite()
{
}

#if !__SPU
void audVolumeConeLite::Init(Vec3V_In coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle)
{
	m_ConeDirection = NormalizeFast(coneDirection);
	m_RearAttenuation = rearAttenuation;
	m_InnerConeAngle = innerAngle;
	m_OuterConeAngle = outerAngle;
}
#endif

f32 audVolumeConeLite::ComputeAttenuation(const Mat34V &matrix, u32 listenerId) const
{
	// Get the position of the microphone
	Vec3V listenerPosition = g_audEnvironment->GetPanningListenerPosition(listenerId);
	// Turn that into a position relative to the sound source
	Vec3V translatedPosition = Subtract(listenerPosition, matrix.GetCol3());

	Vec3V forwards =  Scale(m_ConeDirection.GetX(), matrix.GetCol0()) + 
		Scale(m_ConeDirection.GetY(), matrix.GetCol1()) +
		Scale(m_ConeDirection.GetZ(), matrix.GetCol2());
	float dotProduct = (Dot(translatedPosition,forwards).Getf());
	float lengths = Mag(translatedPosition).Getf() * Mag(matrix.GetCol1()).Getf();
	float angle = 0.0f;
	if (lengths != 0.0f)
	{
		angle = AcosfSafe(dotProduct/lengths);
	}
	// This'll be in radians, turn it into degrees
	angle *= RtoD;
	while (angle<0.0f)
	{
		angle += 360.0f;
	}

	f32 output = ClampRange (angle, m_InnerConeAngle, m_OuterConeAngle); // this puts it in range 0-1
	return output * m_RearAttenuation;

}

//////////////////
//audVariableBlock
//////////////////

bool audVariableBlock::Init(const char *variableListName)
{
	return Init(atStringHash(variableListName));
}

bool audVariableBlock::Init(const u32 variableListNameHash)
{
	ptrdiff_t index = this - &(audEntity::sm_EntityVariableBlocks[0]);
	m_Values = &(audEntity::sm_EntityVariableBlockValues[0]) + (index*kNumVariablesPerBlock);

	m_Metadata = audConfig::GetMetadataObject<ravesimpletypes::VariableList>(variableListNameHash);
	if(audVerifyf(m_Metadata, "Attempting to initialise variable block with invalid variable list (hash: %u)", variableListNameHash))
	{
		if(audVerifyf(m_Metadata->numVariables <= kNumVariablesPerBlock, "Variable block has too many variables! (hash: %u)", variableListNameHash))
		{
			for(u32 i = 0; i < m_Metadata->numVariables; i++)
			{	
				m_Values[i] = m_Metadata->Variable[i].InitialValue;
			}

			return true;
		}
	}

	return false;
}

float *audVariableBlock::FindVariableAddress(const char *name)
{
	return FindVariableAddress(atStringHash(name));
}

float *audVariableBlock::FindVariableAddress(const u32 hash)
{
	for(u32 i = 0; i < m_Metadata->numVariables; i++)
	{
		if(m_Metadata->Variable[i].Name == hash)
		{
			return &m_Values[i];
		}
	}
	return NULL;
}

void audVariableBlock::SetVariable(const u32 hash, const f32 value)
{
	for(u32 i = 0; i < m_Metadata->numVariables; i++)
	{
		if(m_Metadata->Variable[i].Name == hash)
		{
			m_Values[i] = value;
			return;
		}
	}
	audAssertf(0, "Trying to set variable (hash: %u) but it's not present in this variable block", hash);
}

void audEntity::RemoveVariableBlockRef(int index)
{
	if(index >= 0)
	{
		FastAssert(sm_EntityVariableBlockRefs[index]>0);
		sysInterlockedDecrement(&sm_EntityVariableBlockRefs[index]);
	}
}

}
