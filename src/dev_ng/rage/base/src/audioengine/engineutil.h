//
// audioengine/engineutil.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_ENGINE_UTIL_H
#define AUD_ENGINE_UTIL_H

#include "math/random.h"
#include "audiohardware/mixer.h"

namespace rage {

// PURPOSE
//	This class contains general utility functions used throughout the audio engine
class audEngineUtil
{
public:

	// PURPOSE
	//	Initialises the random number stream
	// NOTES
	//	This must be called before any of the random number functions are called
	static void InitClass();

	// PURPOSE
	//	Shuts down the random number stream
	static void ShutdownClass();

	// PURPOSE
	//	Returns a random integer
	// RETURNS
	//	random integer
	static s32 GetRandomInteger();

	// PURPOSE
	//	Returns a random float
	// RETURNS
	//	random float between 0 and 1
	static f32 GetRandomFloat();
	
	// PURPOSE
	//	Returns a random integer within the specified range
	// PARAMS
	//	min - the minimum value that can be returned
	//	max - the maximum value that can be returned
	// RETURNS
	//	random integer within the specified range
	// NOTES
	//	range is inclusive
	static s32 GetRandomNumberInRange(s32 min, s32 max);

	// PURPOSE
	//	Returns a random f32 within the specified range
	// PARAMS
	//	min - the minimum value that can be returned
	//	max - the maximum value that can be returned
	// RETURNS
	//	random f32 within the specified range
	// NOTES
	//	range is inclusive?
	static f32 GetRandomNumberInRange(f32 min, f32 max);

	// PURPOSE
	//	Returns a random ScalarV within the specified range
	// PARAMS
	//	min - the minimum value that can be returned
	//	max - the maximum value that can be returned
	// RETURNS
	//	random ScalarV within the specified range
	// NOTES
	//	range is inclusive?
	static ScalarV_Out GetRandomNumberInRangeV(ScalarV_In min, ScalarV_In max);

	// PURPOSE
	//	Returns a random Vec3V within the specified range
	// PARAMS
	//	min - the minimum value that can be returned
	//	max - the maximum value that can be returned
	// RETURNS
	//	random Vec3V within the specified range
	// NOTES
	//	range is inclusive?
	static Vec3V_Out GetRandomVectorInRange(Vec3V_In min, Vec3V_In max);

	// PURPOSE: Returns a Gaussian random number with a given mean and variance.
	// PARAMS
	//   mean - float, average returned value will match this
	//   variance - float, about 2 out of 3 of the numbers with appear in the range
	//              [mean-variance,mean+variance]
	// RETURNS: 
	//   This function returns a random number clustered around the given mean.  About 2/3rds
	//   of the time the number will be within [mean-variance, mean+variance], distributed in
	//   a bell curve.
	static f32 GetGaussian(f32 mean, f32 variance);

	// PURPOSE
	//	Returns the seed from the PRNG
	static s32 GetRandomSeed();

	// PURPOSE
	//	Resets the PRNG with the supplied seed
	static void SetRandomSeed(s32 seed);

	// PURPOSE
	//	Resolves the specified probability
	// RETURNS
	//	True with probability prob
	static bool	ResolveProbability(f32 prob);

	// PURPOSE
	//  Increment a bunch of variables to provide unique (til it loops!) ids for things.
	// PARAMS
	//  index - which series of unique ids we are requesting a new one from. This list id #defined at the top
	//          of this file. Add new #defines as new groups are needed.
	static u32 GetUniqueId(u16 index);

	// PURPOSE
	//  Match an object name to a test string.
	// Tests to see whether the test string exists as a substring of a name. eg. 'AM_BASE_DEFAULT' would match with 'DEFAULT', 'BASE', 'AM_', etc.
	// Nb. The test string can consist of multiple tokens, separated by spaces or commas, and the code will test for any of them.
	// PARAMS
	//  name - the name of the object
	//	testString - the substring that we are searching for
	static bool MatchName(const char *name, const char *testString);

#if !__SPU
	// PURPOSE
	//	Returns the current game time in milliseconds
	static u32 GetCurrentTimeInMilliseconds();
#endif //!__SPU

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
	static void SetCaptureTimeStep( u32 const timeStep)	{ sm_CaptureTimeStep = timeStep; }
	static u32  GetCaptureTimeStep()						{ return sm_CaptureTimeStep; }

	static void SetCaptureTimeInMS( u32 const timeMs)		{ sm_CaptureTimeInMS = timeMs; }
	static u32  GetCaptureTimeInMS()						{ return sm_CaptureTimeInMS; }

#endif // defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

private:
	static mthRandom	sm_Random;
	static u64			sm_BootTimeTicks;

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

	static u32			sm_CaptureTimeInMSSystemSetTime;		// this was the real time when we set the start of the frame with SetVideoCaptureTimeInMS()
	static u32			sm_CaptureTimeStep;
	static u32			sm_CaptureTimeInMS;

#endif // defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
};



inline s32 audEngineUtil::GetRandomInteger()
{
	return audEngineUtil::sm_Random.GetInt();
}

inline f32 audEngineUtil::GetRandomFloat()
{
	return audEngineUtil::sm_Random.GetFloat();
}

inline s32 audEngineUtil::GetRandomNumberInRange(s32 minValue, s32 maxValue)
{
	return audEngineUtil::sm_Random.GetRanged(minValue, maxValue);
}

inline f32 audEngineUtil::GetRandomNumberInRange(f32 minValue, f32 maxValue)
{
	return audEngineUtil::sm_Random.GetRanged(minValue, maxValue); 
}

inline ScalarV_Out audEngineUtil::GetRandomNumberInRangeV(ScalarV_In minValue, ScalarV_In maxValue)
{
	return audEngineUtil::sm_Random.GetRangedV(minValue, maxValue); 
}

inline Vec3V_Out audEngineUtil::GetRandomVectorInRange(Vec3V_In minValue, Vec3V_In maxValue)
{
	return audEngineUtil::sm_Random.GetRangedV(minValue, maxValue);
}

inline f32 audEngineUtil::GetGaussian(f32 mean, f32 variance)
{
	return audEngineUtil::sm_Random.GetGaussian(mean, variance);
}

inline bool audEngineUtil::ResolveProbability(f32 Probability)
{
	if (Probability >= 1.0f)
		return(true);

	return ((GetRandomNumberInRange(0.0f, 1.0f)) < Probability);
}

inline s32 audEngineUtil::GetRandomSeed()
{
	return audEngineUtil::sm_Random.GetSeed();
}

inline void audEngineUtil::SetRandomSeed(s32 seed)
{
	audEngineUtil::sm_Random.Reset(seed);
}


} // rage namespace

#endif // AUD_ENGINE_UTIL_H
