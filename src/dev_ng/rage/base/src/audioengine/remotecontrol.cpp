// 
// audioengine/remotecontrol.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK
#include "curverepository.h"
#include "categorymanager.h"

#include "engine.h"
#include "engineutil.h"
#include "remotecontrol.h"
#include "soundfactory.h"
#include "widgets.h"

#include "audiohardware/debug.h"
#include "audiohardware/driver.h"
#include "audiohardware/waveslot.h"
#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/variableblocksound.h"

#include "bank/bkmgr.h"
#include "diag/output.h"
#include "grcore/im.h"
#include "grcore/viewport.h"
#include "grcore/font.h"
#include "system/namedpipe.h"
#include "system/param.h"
#include "system/pipepacket.h"

#include "bank/bank.h"

namespace rage
{
	audCurve g_CurveToRender;

	bool audRemoteControl::sm_ShouldRenderCurve = false;
	u32 audRemoteControl::sm_LastSentMessage = 0;
	
	// PURPOSE
	//	When true, a sound being auditioned from RAVE will be audible even when another sound is soloed
	bool g_AuditioningOverridesSolo = true;


	PARAM(rave, "[RAGE Audio] Connect to RAVE for the specified metadata types; comma delimited (eg -rave=Sounds,Curves,Weapons");
	PARAM(raveskipchunks, "[RAGE Audio] Don't request the specified chunks from RAVE; comma delimited (eg -raveskipchunks=BASE,SinglePlayer to request only DLC");
	PARAM(ravehost, "[RAGE Audio] Manually specify the remote RAVE host (defaults to rfs.dat IP)");

	
enum
{
	AUD_REMOTE_PORT = 48000,
};

audRemoteControl::audRemoteControl() :
m_LastSoundPlayed(NULL)
{
	m_OutgoingMessagesEnabled = true;
	m_UniqueId = 0;
	m_LastMessageReceiptTime = 0;
}

audRemoteControl::~audRemoteControl()
{
	
}

void audRemoteControl::GenerateUniqueId()
{
	m_UniqueId = audEngineUtil::GetRandomInteger();
}

bool audRemoteControl::Init()
{
	USE_DEBUG_MEMORY();

	m_PayloadBuf = rage_new u8[kRemoteControlPayloadSizeBytes];

	sysCriticalSection lock(m_NetworkCS);
	if(IsPresent())
	{
		// create an async pipe
		m_Pipe.SetNoDelay(true);
		const char *remoteHost;
		if(PARAM_ravehost.Get(remoteHost) && remoteHost && strlen(remoteHost) > 0)
		{
			while(!m_Pipe.Open(remoteHost, AUD_REMOTE_PORT))
			{
				audWarningf("Couldn't connect to RAVE at %s - retrying ...", remoteHost);
				sysIpcSleep(10);
			}
		}
		else
		{
			while(!m_Pipe.Open(AUD_REMOTE_PORT))
			{
				audWarningf("Couldn't connect to RAVE - retrying ...");
				sysIpcSleep(10);
			}
		}
		audWarningf("Reconnecting to RAVE\n");
		sm_LastSentMessage = 0;
		

		SetRuntimePlatform();

		// parse specified metadata types
		const char *metadataTypeListRO;
		if(PARAM_rave.Get(metadataTypeListRO) && metadataTypeListRO && strlen(metadataTypeListRO) > 0)
		{
			char *metadataTypeList = StringDuplicate(metadataTypeListRO);

			const char* token = strtok(metadataTypeList, ",");
			
			for (; token; token = strtok( NULL, ","))
			{
				if(!token)
				{
					break;
				}
				m_RaveMetadataTypes.PushAndGrow(atStringHash(token));
				audDisplayf("Using RAVE for metadata type %s", token);
			}

			StringFree(metadataTypeList);
		}

		if(PARAM_raveskipchunks.Get(metadataTypeListRO) && metadataTypeListRO && strlen(metadataTypeListRO) > 0)
		{
			char *metadataTypeList = StringDuplicate(metadataTypeListRO);

			const char* token = strtok(metadataTypeList, ",");

			for (; token; token = strtok( NULL, ","))
			{
				if(!token)
				{
					break;
				}
				m_RaveMetadataChunks.PushAndGrow(atStringHash(token));
				audDisplayf("Not using RAVE for metadata chunk %s", token);
			}

			StringFree(metadataTypeList);
		}
	}
	return true;
}

void audRemoteControl::Shutdown()
{
	delete[] m_PayloadBuf;
	sysCriticalSection lock(m_NetworkCS);
	m_Pipe.Close();

	m_RaveMetadataTypes.Reset();
	m_RaveMetadataChunks.Reset();
	m_MetadataMgrMap.Reset();
}

void audRemoteControl::Update(u32 time)
{
	USE_DEBUG_MEMORY();

	sysCriticalSection lock(m_NetworkCS);
	u8 headerBuf[5];
	u32 *packetSize = (u32*)&headerBuf[1];

	if(m_Pipe.IsValid())
	{
		if(m_OutgoingMessagesEnabled && sm_LastSentMessage < time - 1000)
		{	
			// send a keep-alive signal to RAVE
			headerBuf[0] = PING;
			*((u32*)(&headerBuf[1])) = 0;
			const bool error = (m_Pipe.Write(headerBuf, 5) != 5);
			
			if(error)
			{
				audWarningf("Disconnected from RAVE\n");
				m_Pipe.Close();

				// reconnect to rave
				Init();
			}
			sm_LastSentMessage = time;
		}

		if(m_Pipe.Read(&headerBuf, 1) == 1)
		{
			m_Pipe.SafeRead(&headerBuf[1], 4);

			Assert(*packetSize <= kRemoteControlPayloadSizeBytes);
			if(*packetSize > kRemoteControlPayloadSizeBytes)
			{
				// throw away this packet
				u32 bytesToFlush = *packetSize;
				audWarningf("RemoteControl packet too big: %u", bytesToFlush);
				while(bytesToFlush>0)
				{
					const s32 bytesReadThisTime = m_Pipe.Read(m_PayloadBuf, Min<u32>(kRemoteControlPayloadSizeBytes,bytesToFlush));
					if(bytesReadThisTime < 0)
					{
						// bail out - connection has been terminated
						audErrorf("Failed to flush remote control connection");
						return;
					}
					bytesToFlush -= bytesReadThisTime;
				}
			}
			else if(*packetSize != 0)
			{
				m_Pipe.SafeRead(m_PayloadBuf, *packetSize);
			}			
			HandleReceivedPacket(headerBuf[0], *packetSize, m_PayloadBuf);
		}
	}
}

void audRemoteControl::RegisterMetadataManager(const u32 metadataTypeHash, audMetadataManager *manager)
{
	m_MetadataMgrMap.Insert(metadataTypeHash, manager);
}

void audRemoteControl::HandleReceivedPacket(const u8 packetType, const u32 payloadSize, u8 *payload)
{
	u32 *ptr, metadataTypeHash, size, hash, chunkNameHash;

	switch(packetType)
	{
	case PLAYSOUND:
		break;
	case STOPSOUND:
		// Preserve old behaviour
		AuditionStop(ATSTRINGHASH("Sounds", 0x94FA081B), 0);
		break;
	case OBJECT_CHANGED:
		{
			ptr = (u32*)payload;
			metadataTypeHash = *ptr;
			ptr++;
			chunkNameHash = *ptr;
			ptr++;
			size = *ptr++;
			const char *objectName = (const char*)ptr;
			const size_t len = strlen(objectName);
			ptr = (u32*)((u8*)ptr + len + 1);			
			ObjectEdit(metadataTypeHash, chunkNameHash, objectName, size, (u8*)ptr);
		}
		break;
	case VIEWOBJECT:
		ptr = (u32*)payload;
		metadataTypeHash = *ptr;
		ptr++;
		hash = *ptr;
		ptr++;
		ViewObject(metadataTypeHash, hash);
		break;
	case TOGGLEVIEWOBJECT:
		ptr = (u32*)payload;
		metadataTypeHash = *ptr;
		ptr++;
		ToggleViewObject(metadataTypeHash);
		break;
	case MESSAGERECEIPT:
		m_LastMessageReceiptTime = g_AudioEngine.GetTimeInMilliseconds();
		break;
	case AUDITION_START:
		ptr = (u32*)payload;
		metadataTypeHash = *ptr;
		ptr++;
		hash = *ptr;
		ptr++;
		AuditionStart(metadataTypeHash, hash);
		break;
	case AUDITION_STOP:
		ptr = (u32*)payload;
		metadataTypeHash = *ptr;
		ptr++;
		hash = *ptr;
		ptr++;
		AuditionStop(metadataTypeHash, hash);
		break;
	case SET_MUTE_LIST:
		{
			audMetadataManager &categories = g_AudioEngine.GetCategoryManager().GetMetadataManager();
			audMetadataManager &sounds = SOUNDFACTORY.GetMetadataManager();

			categories.ResetMuteList();
			sounds.ResetMuteList();

			ptr = (u32*)payload;		
			// two dwords per entry
			u32 count = payloadSize >> 3;
			for(u32 i = 0; i < count; i++)
			{
				u32 typeHash = *ptr++;
				u32 nameHash = *ptr++;
				if(typeHash == ATSTRINGHASH("Category", 0x7A5C560D))
				{
					categories.AddToMuteList(nameHash);
				}
				else
				{
					sounds.AddToMuteList(nameHash);
				}
			}
		}
		break;
	case SET_SOLO_LIST:
		{
			audMetadataManager &categories = g_AudioEngine.GetCategoryManager().GetMetadataManager();
			audMetadataManager &sounds = SOUNDFACTORY.GetMetadataManager();

			categories.ResetSoloList();
			sounds.ResetSoloList();

			ptr = (u32*)payload;		
			// two dwords per entry
			u32 count = payloadSize >> 3;
			for(u32 i = 0; i < count; i++)
			{
				u32 typeHash = *ptr++;
				u32 nameHash = *ptr++;
				if(typeHash == ATSTRINGHASH("Category", 0x7A5C560D))
				{
					categories.AddToSoloList(nameHash);
				}
				else
				{
					sounds.AddToSoloList(nameHash);
				}
			}
		}
		break;
	}
}

void audRemoteControl::AuditionStart(const u32 metadataTypeHash, const u32 objectNameHash)
{
	audMetadataManager **mgr = m_MetadataMgrMap.Access(metadataTypeHash);
	if(mgr)
	{
		(*mgr)->OnObjectAuditionStartCallback(objectNameHash);
	}
}

void audRemoteControl::AuditionStop(const u32 metadataTypeHash, const u32 objectNameHash)
{
	audMetadataManager **mgr = m_MetadataMgrMap.Access(metadataTypeHash);
	if(mgr)
	{
		(*mgr)->OnObjectAuditionStopCallback(objectNameHash);
	}
}

void audRemoteControl::ViewObject(const u32 metadataTypeHash, const u32 objectNameHash)
{
	m_ViewMetadataTypeHash = metadataTypeHash;
	m_ViewObjectNameHash = objectNameHash;

	audMetadataManager **mgr = m_MetadataMgrMap.Access(metadataTypeHash);
	if(mgr)
	{
		(*mgr)->OnObjectViewCallback(objectNameHash);
	}
}

void audRemoteControl::ToggleViewObject(const u32 metadataTypeHash)
{
	static const audStringHash curve("CURVES");
	if(metadataTypeHash == curve)
	{
		sm_ShouldRenderCurve = !sm_ShouldRenderCurve;
	}
}

void audRemoteControl::ObjectEdit(const u32 metadataTypeHash, const u32 chunkNameHash, const char *objectName, const u32 size, u8 *data)
{
	if(ShouldUseRAVEForMetadataType(metadataTypeHash))
	{
		audMetadataManager **mgr = m_MetadataMgrMap.Access(metadataTypeHash);
		if(mgr)
		{
			(*mgr)->OnObjectEdit(objectName, chunkNameHash, size, (u8*)data);
		}
	}
}

bool audRemoteControl::IsPresent()
{
	return __BANK ? PARAM_rave.Get() : false;
}

bool audRemoteControl::IsConnected() const
{
	return m_Pipe.IsValid();
}

bool audRemoteControl::RequestMetadataChunk(const u32 metadataTypeHash, const u32 chunkNameHash, const u32 schemaVersion, u8 *&buf, u32 &len)
{
	sysCriticalSection lock(m_NetworkCS);

	u32 retries = 0;
	bool finished = true;
	do 
	{
		finished = true;
		len = 0;
		s32 bytesTransferred = -1;
		u8 commandBuffer[17];
		commandBuffer[0] = REQUESTMETADATACHUNK;
		
		*(u32*)(&commandBuffer[1]) = sizeof(commandBuffer) - 5; // payload size 
		*(u32*)(&commandBuffer[5]) = metadataTypeHash;
		*(u32*)(&commandBuffer[9]) = chunkNameHash;
		*(u32*)(&commandBuffer[13]) = schemaVersion;

		bytesTransferred = m_Pipe.Write(commandBuffer, sizeof(commandBuffer));
		if(bytesTransferred != sizeof(commandBuffer))
		{
			audWarningf("Wrote %d bytes (expected %" SIZETFMT "d)", bytesTransferred, sizeof(commandBuffer));
			finished = false;
		}
		m_Pipe.SafeRead(&len, 4);
		if(!finished || len <= 0)	
		{
			finished = false;
			retries++;
			audWarningf("Failed to receive metadata from RAVE (type hash: %u, chunk %u) - retrying (%u)...", metadataTypeHash, chunkNameHash, retries);
			Shutdown();
			sysIpcSleep(1500);
			Init();
		}
		else
		{
			buf = (u8*)g_AudioEngine.AllocateVirtual(len);
			m_Pipe.SafeRead(buf, len);
			finished = true;
		}
	}while (!finished);
	return true;
}

bool audRemoteControl::SendXmlMessage(const char *xmlMessage)
{
	return SendXmlMessage(xmlMessage, istrlen(xmlMessage));
}

bool audRemoteControl::SendXmlMessage(const char *xmlMessage, const u32 length)
{
	if(!m_OutgoingMessagesEnabled)
	{
		return false;
	}

	sysCriticalSection lock(m_NetworkCS);

	u32 actualLength = length;
	for(u32 i = 0; i < length; i++)
	{
		if(!xmlMessage[i])
		{
			actualLength = i;
			break;
		}
	}

	// 5 byte header: command id, size
	u8 *buffer = rage_new u8[actualLength + 5];
	if(buffer)
	{
		buffer[0] = EDITOBJECT;
		*((u32*)&buffer[1]) = actualLength;
		sysMemCpy(&buffer[5], xmlMessage, actualLength);
		u32 sent = m_Pipe.Write(buffer, actualLength+5);
		delete[] buffer;
		return (sent == actualLength+5);
	}
	return false;
}

bool audRemoteControl::SendCommand(const audRemoteControlSerializer &data, const audRemoteControlCommands command)
{
	if(!m_OutgoingMessagesEnabled)
	{
		return false;
	}

	sysCriticalSection lock(m_NetworkCS);

	const u32 actualLength = data.GetBytesWritten();
	
	// 5 byte header: command id, size
	u8 *buffer = rage_new u8[actualLength + 5];
	if(buffer)
	{
		buffer[0] = (u8)command;
		*((u32*)&buffer[1]) = actualLength;
		sysMemCpy(&buffer[5], data.GetBuffer(), actualLength);
		u32 sent = m_Pipe.Write(buffer, actualLength+5);
		delete[] buffer;
		return (sent == actualLength+5);
	}
	return false;
}

void audRemoteControl::SetRuntimePlatform()
{
#if __XENON
	const char *platform = "XBOX360";
#endif
#if RSG_PC
	const char *platform =  "PC";
#endif
#if RSG_DURANGO
	const char *platform =  "XBOXONE";
#endif
#if RSG_ORBIS
	const char *platform =  "PS4";
#endif
#if __PPU
	const char *platform =  "PS3";
#endif

	u8 buf[128];
	size_t stringLen = strlen(platform);
	Assert(stringLen < sizeof(buf)-9);

	buf[0] = SETRUNTIMEPLATFORM;
	// Note: this is sent big endian, regardless of platform - RAVE can't perform endian conversion until after it has processed
	// the set runtime platform command.
	*((u32*)(&buf[1])) = sysEndian::NtoB((u32)stringLen + 4); // payload size: string length + u32 clientUID

	sysMemCpy(&buf[5], platform, stringLen);
	*(s32*)(&buf[5+stringLen]) = m_UniqueId;

	m_Pipe.SafeWrite(buf, stringLen + 4 + 5);
	m_Pipe.SafeRead(&buf, 5);
}

bool audRemoteControl::ShouldUseRAVEForMetadataChunk(const char *chunkName) const
{
	const u32 chunkNameHash = atStringHash(chunkName);
	if(m_RaveMetadataChunks.GetCount() == 0)
	{
		// none were specified on the command line; use RAVE for all metadata
		return true;
	}
	else
	{
		for(atArray<u32>::const_iterator iter = m_RaveMetadataChunks.begin(); iter != m_RaveMetadataChunks.end(); iter++)
		{
			if((*iter) == chunkNameHash)
			{
				return false;
			}
		}
		return true;
	}
}

bool audRemoteControl::ShouldUseRAVEForMetadataType(const u32 typeNameHash) const
{
	if(m_RaveMetadataTypes.GetCount() == 0)
	{
		// none were specified on the command line; use RAVE for all metadata
		return true;
	}
	else
	{
		for(atArray<u32>::const_iterator iter = m_RaveMetadataTypes.begin(); iter != m_RaveMetadataTypes.end(); iter++)
		{
			if((*iter) == typeNameHash)
			{
				return true;
			}
		}
		return false;
	}
}

void audRemoteControl::DrawDebug() const
{
	if(sm_ShouldRenderCurve)
	{
		RenderCurve();
	}

	audSound *auditionSound = m_LastSoundPlayed;
	if(auditionSound)
	{
		DrawDebugSound(auditionSound, m_AuditionSoundDrawMode);
	}
}

void audSoundDebugDrawManager::PushSection(const char *title)
{
	const float offsetY = m_Y - m_OffsetY;
	if(offsetY >= m_MinClipY && offsetY < m_MaxClipY)
	{
		grcDraw2dText(m_X, offsetY, title, true, GetTextScale(), GetTextScale());
	}
	m_Y += lineHeight * GetTextScale();
	m_X += tabWidth;
}

void audSoundDebugDrawManager::PopSection()
{
	m_X -= tabWidth * grcFont::GetCurrent().GetInternalScale();
	Assert(m_X >= 0.f);
}

void audSoundDebugDrawManager::DrawLine(const char *text)
{
	const float offsetY = m_Y - m_OffsetY;
	if(offsetY >= m_MinClipY && offsetY < m_MaxClipY)
	{
		grcDraw2dText(m_X, offsetY, text, true, GetTextScale(), GetTextScale());
	}
	m_Y += lineHeight * GetTextScale();
}	

void audSoundDebugDrawManager::SkipLine()
{
	m_Y += lineHeight;
}

void audRemoteControl::DrawDebugSound(audSound *sound, const AuditionSoundDrawMode drawMode) const
{
	PUSH_DEFAULT_SCREEN();

	grcColor(Color32(255,255,255,255));
	audSound::GetStaticPool().LockBucket(sound->GetBucketId());

	if(drawMode == kEverything || drawMode == kNoDynamicChildren || drawMode == kHierarchyOnly)
	{
		audSoundDebugDrawManager drawMgr(100.f, 50.f, 50.f, 720.f);
		drawMgr.SetOffsetY(m_AuditionSoundDrawOffsetY);
		sound->DebugDraw(drawMgr, drawMode != kNoDynamicChildren, drawMode == kHierarchyOnly);
	}
	else if(drawMode == kVariablesOnly)
	{
		grcDraw2dText(100.f,65.f, sound->GetName(), true);

		const s32 maxVariableBlocks = 8;
		audSound *sounds[maxVariableBlocks] = {0};
		sound->_SearchForSoundsByType(VariableBlockSound::TYPE_ID, &sounds[0], maxVariableBlocks, true);

		float y = 80.f;
		const float yIncr = 10.f;

		grcColor(Color32(255,255,255,255));
		for(s32 i = 0; i < maxVariableBlocks; i++)
		{
			if(sounds[i])
			{
				audVariableBlockSound *variableBlock = (audVariableBlockSound*)sounds[i];

				grcDraw2dText(120.f, y, variableBlock->GetName(), true);
				y += yIncr;

				if(variableBlock->GetNumAudioVariables())
				{
					grcDraw2dText(130.f, y, "Audio variables:", true);
					y += yIncr;
					for(u32 j = 0; j < variableBlock->GetNumAudioVariables(); j++)
					{
						u32 nameHash = 0;
						float value = 0.f;
						variableBlock->GetAudioVariableFromIndex(j, nameHash, value);
						char buf[64];
						formatf(buf, "%u (%u): %f", j+1, nameHash, value);
						grcDraw2dText(140.f, y, buf, true);
						y += yIncr;
					}
				}

				if(variableBlock->GetNumGameVariables())
				{
					grcDraw2dText(130.f, y, "Game variables:", true);
					y += yIncr;
					for(u32 j = 0; j < variableBlock->GetNumGameVariables(); j++)
					{
						u32 nameHash = 0;
						float value = 0.f;
						variableBlock->GetGameVariableFromIndex(j, nameHash, value);
						char buf[64];
						formatf(buf, "%u (%u): %f", j+1, nameHash, value);
						grcDraw2dText(140.f, y, buf, true);
						y += yIncr;
					}
				}
			}
		}
	}
	audSound::GetStaticPool().UnlockBucket(sound->GetBucketId());
	POP_DEFAULT_SCREEN();
}

void audRemoteControl::RenderCurve() const
{
#if __DEV
	f32 minInput = 0.f,maxInput=1.0f;

	PUSH_DEFAULT_SCREEN();
	audDriver::SetDebugDrawRenderStates();

	bool haveCurve = g_CurveToRender.Init(m_ViewObjectNameHash);

	if(haveCurve)
	{
		minInput = g_CurveToRender.GetMetadataMinInput();
		maxInput = g_CurveToRender.GetMetadataMaxInput();
	}
	
	const u32 width = 600;
	const u32 height = 350;
	const u32 left = 200;
	const u32 top = 100;
	const u32 yborder = 5;

	// draw alpha'd background
	grcBegin(drawTriStrip, 4);
		grcColor(Color32(0.1f,0.1f,0.1f,0.9f));
		grcVertex2i(left,top+height);	
		grcVertex2i(left,top);
		grcVertex2i(left+width,top+height);	
		grcVertex2i(left+width,top);
	grcEnd();

	// find curve range
	f32 minOutput = LARGE_FLOAT;
	f32 maxOutput = SMALL_FLOAT;

	for(u32 i = 0; i < width; i++)
	{
		f32 result = g_CurveToRender.CalculateValue(minInput + ((maxInput-minInput)*(i/(f32)width)));
		if(result < minOutput)
		{
			minOutput = result;
		}
		if(result > maxOutput)
		{
			maxOutput = result;
		}
	}

	// render curve
	grcBegin(rage::drawPoints, width);
		grcColor(Color32(0.3f,0.3f,0.6f));
		for(u32 i = 0; i < width; i++)
		{
			f32 output = g_CurveToRender.CalculateValue(minInput + ((maxInput-minInput)*i/(f32)width));
			u32 y = top + height - (u32)(height * (output - minOutput)/(maxOutput-minOutput));
			grcVertex2i(left + i,y);
		}
	grcEnd();

	// render axes
	grcBegin(drawLineStrip, 3);
		grcColor(Color32(0.7f,0.7f,0.7f));
		grcVertex2i(left, top);
		grcVertex2i(left, top+height);
		grcVertex2i(left+width, top+height);
	grcEnd();

	// draw curve name
	if(haveCurve)
	{
		grcDraw2dText(left + width - (strlen(g_CurveToRender.GetName())*8.f),(f32)top,g_CurveToRender.GetName());
	}

	// draw curve limits
	char buf[128];
	formatf(buf, "%.3f", minInput);
	grcDraw2dText((f32)left, top + height + yborder, buf);
	formatf(buf, "%.3f", minOutput);
	grcDraw2dText(left-strlen(buf)*8.f, top + height - yborder, buf);
	formatf(buf, "%.3f", maxInput);
	grcDraw2dText(left+width,top+height+yborder,buf);
	formatf(buf, "%.3f", maxOutput);
	grcDraw2dText(left-strlen(buf)*8.f,(f32)top,buf);
	if(haveCurve && !g_CurveToRender.IsValidInSoundTask())
	{
		static const char *warningStr = "(non-optimised curve: not valid for direct sound use)";
		grcColor(Color32(0.7f,0.5f,0.f));
		grcDraw2dText(left+2.f,(f32)(top+height-10), warningStr);
	}
	POP_DEFAULT_SCREEN();
#endif
}

void audRemoteControl::AddWidgets(bkBank &bank)
{
	bank.AddToggle("Audition Overrides Solo", &g_AuditioningOverridesSolo);
	bank.AddToggle("Draw Selected Curve", &sm_ShouldRenderCurve);
}

} // namespace rage

#endif // BANK
