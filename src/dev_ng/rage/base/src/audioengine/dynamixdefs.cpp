// 
// dynamixdefs.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure functions - do not edit.
// 

#include "dynamixdefs.h"
#include "string/stringhash.h"

namespace rage
{
	// 
	// MixerPatch
	// 
	void* MixerPatch::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1758134309U: return &FadeIn;
			case 1163102853U: return &FadeOut;
			case 1610825783U: return &PreDelay;
			case 3674968789U: return &Duration;
			case 3967395526U: return &ApplyFactorCurve;
			case 2696969138U: return &ApplyVariable;
			case 4048554612U: return &ApplySmoothRate;
			case 2401644316U: return &MixCategories;
			default: return NULL;
		}
	}
	
	// 
	// AudSceneState
	// 
	void* AudSceneState::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2007517383U: return &State;
			default: return NULL;
		}
	}
	
	// 
	// MixerScene
	// 
	void* MixerScene::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2273862419U: return &ReferenceCount;
			case 1192938879U: return &PatchGroup;
			default: return NULL;
		}
	}
	
	// 
	// MixGroup
	// 
	void* MixGroup::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2273862419U: return &ReferenceCount;
			case 3571135883U: return &FadeTime;
			case 2252780680U: return &Map;
			default: return NULL;
		}
	}
	
	// 
	// MixGroupList
	// 
	void* MixGroupList::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2152740626U: return &MixGroup;
			default: return NULL;
		}
	}
	
	// 
	// DynamicMixModuleSettings
	// 
	void* DynamicMixModuleSettings::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1758134309U: return &FadeIn;
			case 1163102853U: return &FadeOut;
			case 2696969138U: return &ApplyVariable;
			case 3674968789U: return &Duration;
			case 3457750374U: return &ModuleTypeSettings;
			default: return NULL;
		}
	}
	
	// 
	// SceneVariableModuleSettings
	// 
	void* SceneVariableModuleSettings::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3661929030U: return &SceneVariable;
			case 566774767U: return &InputOutputCurve;
			case 3018672514U: return &Input;
			case 2195548119U: return &ScaleMin;
			case 3531270156U: return &ScaleMax;
			default: return NULL;
		}
	}
	
	// 
	// SceneTransitionModuleSettings
	// 
	void* SceneTransitionModuleSettings::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3018672514U: return &Input;
			case 1573986338U: return &Threshold;
			case 3113433489U: return &Transition;
			default: return NULL;
		}
	}
	
	// 
	// VehicleCollisionModuleSettings
	// 
	void* VehicleCollisionModuleSettings::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3018672514U: return &Input;
			case 3113433489U: return &Transition;
			default: return NULL;
		}
	}
	
	// 
	// MixGroupCategoryMap
	// 
	void* MixGroupCategoryMap::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2320145025U: return &Entry;
			default: return NULL;
		}
	}
	
	// 
	// Enumeration Conversion
	// 
	
	// PURPOSE - Convert a MixModuleInput value into its string representation.
	const char* MixModuleInput_ToString(const MixModuleInput value)
	{
		switch(value)
		{
			case INPUT_NONE: return "INPUT_NONE";
			case INPUT_PLAYER_VEH_VELOCITY: return "INPUT_PLAYER_VEH_VELOCITY";
			case INPUT_PLAYER_VEH_AIRTIME: return "INPUT_PLAYER_VEH_AIRTIME";
			case INPUT_PLAYER_VEH_ROLL: return "INPUT_PLAYER_VEH_ROLL";
			case INPUT_PLAYER_WANTED: return "INPUT_PLAYER_WANTED";
			case VEH_VEH_SIDES: return "VEH_VEH_SIDES";
			case VEH_BUILDING_SIDES: return "VEH_BUILDING_SIDES";
			case NUM_MIXMODULEINPUT: return "NUM_MIXMODULEINPUT";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a MixModuleInput value.
	MixModuleInput MixModuleInput_Parse(const char* str, const MixModuleInput defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 1801226947U: return INPUT_NONE;
			case 1701385811U: return INPUT_PLAYER_VEH_VELOCITY;
			case 1174623416U: return INPUT_PLAYER_VEH_AIRTIME;
			case 2029603773U: return INPUT_PLAYER_VEH_ROLL;
			case 960769670U: return INPUT_PLAYER_WANTED;
			case 3845453656U: return VEH_VEH_SIDES;
			case 290497960U: return VEH_BUILDING_SIDES;
			case 2502350671U:
			case 1599458899U: return NUM_MIXMODULEINPUT;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a VolumeInvertInput value into its string representation.
	const char* VolumeInvertInput_ToString(const VolumeInvertInput value)
	{
		switch(value)
		{
			case INPUT_INVERT: return "INPUT_INVERT";
			case INPUT_DO_NOTHING: return "INPUT_DO_NOTHING";
			case NUM_VOLUMEINVERTINPUT: return "NUM_VOLUMEINVERTINPUT";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a VolumeInvertInput value.
	VolumeInvertInput VolumeInvertInput_Parse(const char* str, const VolumeInvertInput defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 3040494059U: return INPUT_INVERT;
			case 540674265U: return INPUT_DO_NOTHING;
			case 1921142727U:
			case 140578772U: return NUM_VOLUMEINVERTINPUT;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gDynamicMixerGetBaseTypeId(const rage::u32 classId)
	{
		switch(classId)
		{
			case MixerPatch::TYPE_ID: return MixerPatch::BASE_TYPE_ID;
			case AudSceneState::TYPE_ID: return AudSceneState::BASE_TYPE_ID;
			case MixerScene::TYPE_ID: return MixerScene::BASE_TYPE_ID;
			case MixGroup::TYPE_ID: return MixGroup::BASE_TYPE_ID;
			case MixGroupList::TYPE_ID: return MixGroupList::BASE_TYPE_ID;
			case DynamicMixModuleSettings::TYPE_ID: return DynamicMixModuleSettings::BASE_TYPE_ID;
			case SceneVariableModuleSettings::TYPE_ID: return SceneVariableModuleSettings::BASE_TYPE_ID;
			case SceneTransitionModuleSettings::TYPE_ID: return SceneTransitionModuleSettings::BASE_TYPE_ID;
			case VehicleCollisionModuleSettings::TYPE_ID: return VehicleCollisionModuleSettings::BASE_TYPE_ID;
			case MixGroupCategoryMap::TYPE_ID: return MixGroupCategoryMap::BASE_TYPE_ID;
			default: return 0xFFFFFFFF;
		}
	}
	
	// PURPOSE - Determines if a type inherits from another type
	bool gDynamicMixerIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId)
	{
		if(objectTypeId == 0xFFFFFFFF)
		{
			return false;
		}
		else if(objectTypeId == baseTypeId)
		{
			return true;
		}
		else
		{
			return gDynamicMixerIsOfType(gDynamicMixerGetBaseTypeId(objectTypeId), baseTypeId);
		}
	}
} // namespace rage
