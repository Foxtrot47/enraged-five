//
// audioengine/tracker.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_TRACKER_H
#define AUD_TRACKER_H

#include "vector/vector3.h"
#include "widgets.h"

namespace rage {

// PURPOSE
//  audTrackers enable sounds to track the position of game entities in an automated, low-effort way.
//  They are inherited on a game-specific basis, and a gameSpecificTracker embedded in the lowest level
//  entity within a game that represents a physical object with a position. The tracker can then return
//  its game-world position.
//  audTrackers inform their audController when they are deleted, to allow for thread-safe 
//  updating of sounds, and for orphan control.
class audTracker
{
public:
	audTracker();
	virtual ~audTracker();

	// PURPOSE
	//  Returns the parent's Position vector.
	virtual const Vector3 GetPosition() const = 0;

	// PURPOSE
	//  Returns the parent's Orientation quaternion.
	virtual audCompressedQuat GetOrientation() const = 0;

	void DecrementReferenceCount() const
	{
		Assert(m_ReferenceCount > 0);
		m_ReferenceCount--;
	}
	void IncrementReferenceCount() const
	{
		Assert(m_ReferenceCount < 65535);
		m_ReferenceCount++;	
	}

	bool IsReferenced() const { return (m_ReferenceCount > 0); }

private:

	mutable u16 m_ReferenceCount;
};

} // namespace rage

#endif // AUD_TRACKER_H
