//
// audioengine/categorymanager.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "audiohardware/channel.h"
#include "audiohardware/driver.h"
#include "categorymanager.h"

#include "category.h"
#include "categorydefs.h"
#include "engine.h"
#include "metadatamanager.h"
#include "remotecontrol.h"

#include "diag/output.h"
#include "file/device.h"
#include "string/stringhash.h"
#include "system/memory.h"
#include "system/param.h"

namespace rage {

tCategorySettings *g_ResolvedCategorySettings = NULL;
audCategory* g_CategoryStoreMem = NULL;
atPool<audCategory > *g_CategoryStore = NULL;
s16 *g_DynamicHeirachies= NULL;

BANK_ONLY(PARAM(disablecategorytelemetry, "RAGE Audio - disable RAVE category telemetry"));

audCategoryManager::audCategoryManager()
{
	m_CategoryMap = NULL;
	m_CurrentHashIndex = 0;
	m_CurrentStoreIndex = 0; 
	m_CategorySettingsGameThreadIndex = 1;
	m_CategorySettingsAudioThreadIndex = 0;
#if __BANK
	m_IsSoloActive = false;
	m_HaveSentTelemetryHeader = false;
	m_RecursionDebugIndex = -1;
	m_TelemetryEnabled = false;
#endif
}

audCategoryManager::~audCategoryManager()
{
}

bool audCategoryManager::Init()
{
	const char *metadataFileName = g_AudioEngine.GetConfig().GetCategoryMetadata();

	audDisplayf("sizeof(audCategory): %" SIZETFMT "d", sizeof(audCategory));

	if (!m_MetadataMgr.Init("Categories", metadataFileName, audMetadataManager::External_NameTable_BankOnly, CATEGORYDEFS_SCHEMA_VERSION, true))
	{
		return false;
	}	

	m_MetadataMgr.SetObjectModifiedCallback(this);

	// Now we instantiate the top level category, which will in turn new all down the structure.
	// We also need to build an ordered map of category pointers and hashed names, to make finding the instantiated
	// categories speedy.
	if (!BuildCategoryMap())
	{
		return false;
	}

	return true;
}

void audCategoryManager::Shutdown()
{
	if (m_CategoryMap)
	{
		g_AudioEngine.FreeVirtual(m_CategoryMap);
		m_CategoryMap = NULL;
	}
	else
	{
		Assert(0);
	}
	if(g_CategoryStore)
	{
		delete g_CategoryStore;
		g_CategoryStore = NULL;
	}
	else 
	{
		Assert(0);
	}
	if (g_CategoryStoreMem)
	{
		g_AudioEngine.FreeVirtual(g_CategoryStoreMem);
		g_CategoryStoreMem = NULL;
	}
	else
	{
		Assert(0);
	}
	if(g_DynamicHeirachies)
	{
		delete [] g_DynamicHeirachies;
		g_DynamicHeirachies = NULL;
	}
	else
	{
		Assert(0);
	}
	if(g_ResolvedCategorySettings)
	{
		g_AudioEngine.FreeVirtual(g_ResolvedCategorySettings);
	}
	else
	{
		Assert(0);
	}
	m_MetadataMgr.Shutdown();
}

bool audCategoryManager::BuildCategoryMap()
{
	m_CurrentHashIndex = 0;
	m_CurrentStoreIndex = 0;
	m_NumberOfStaticCategories = m_MetadataMgr.ComputeNumberOfObjects();
	m_NumberOfCategories = m_NumberOfStaticCategories + NUM_DYNAMIC_CATEGORIES;

	if(!audVerifyf(m_NumberOfCategories > 0, "Failed to get a valid category tree."))
	{		
		return false;
	}

	// Create the correctly sized category map, and for safety, clear it out.
	m_CategoryMap = (audCategoryMapEntry*)g_AudioEngine.AllocateVirtual((m_NumberOfStaticCategories) * sizeof(audCategoryMapEntry));
	
	// aligned to 128 bytes so that we can prefetch
	g_CategoryStoreMem = (audCategory*)g_AudioEngine.AllocateVirtual(m_NumberOfCategories * sizeof(audCategory),128);
	Assert(g_CategoryStoreMem);
	sysMemSet(g_CategoryStoreMem, 0, sizeof(audCategory)*m_NumberOfCategories);

	g_CategoryStore = rage_new atPool<audCategory>((u8*)g_CategoryStoreMem, m_NumberOfCategories*sizeof(audCategory));
	Assert(g_CategoryStore);

	g_ResolvedCategorySettings = (tCategorySettings*)g_AudioEngine.AllocateVirtual(m_NumberOfCategories * sizeof(tCategorySettings),128);

	for (u32 i = 0; i<(m_NumberOfStaticCategories); i++)
	{
		m_CategoryMap[i].key = 0;
		m_CategoryMap[i].ptr = NULL;
	}

	// Instantiate the first (base) category. This will cascade down to create all other categories. We request this
	// from a CategoryManager function, in order to keep track of the current index.
	// Actually, we're not instantiating them, we're just Init-ing them, they were already new'ed when we declared the array.
	InitCategory(ATSTRINGHASH("BASE", 0x44E21C90), NULL);

	// Sort our HashTable, so we can quickly look up categories from their hash.
	SortHashTable();


	//To keep track of dynamic heirachies on the spu
	g_DynamicHeirachies = rage_aligned_new(16) s16[NUM_DYNAMIC_HEIRACHIES];
	sysMemSet(g_DynamicHeirachies, -1, sizeof(s16)*NUM_DYNAMIC_HEIRACHIES);

	return true;
}

audCategory* audCategoryManager::InitCategory(u32 nameHash, audCategory* parent)
{
	if (m_CurrentHashIndex>=(int)(m_NumberOfStaticCategories))
	{
		Assert(0);
		return NULL;
	}
	u32 thisIndex = m_CurrentHashIndex++;

	audCategory * cat = static_cast<audCategory *>(g_CategoryStore->New());
	  
	cat->Init(nameHash, &m_MetadataMgr, parent);

	m_CategoryMap[thisIndex].key = nameHash;
	m_CategoryMap[thisIndex].ptr = cat;

	return cat;
}

void audCategoryManager::RegisterDynamicHeirachy(s32 index)
{
	sys_lwsync();
	for(int i=0; i<NUM_DYNAMIC_HEIRACHIES; i++)
	{
		audAssertf(g_DynamicHeirachies[i] != index, "Registering a dynamic category heirachy that's already registered; g_DynamicHeirachies will be broken");
		if(g_DynamicHeirachies[i] == -1)
		{
			Assign(g_DynamicHeirachies[i], index);
			return;
		}
	}

	audAssertf(0, "In ResisterDynamicHeirachy: ran out of space in g_DynamicHeirachies");
}

void audCategoryManager::UnregisterDynamicHeirachy(s32 index)
{
	for(int i=0; i<NUM_DYNAMIC_HEIRACHIES; i++)
	{
		if(g_DynamicHeirachies[i] == index)
		{
			g_DynamicHeirachies[i] = -1;
			sys_lwsync();
			return;
		}
	}
	audWarningf("Tried to unregister heirachy with index %d but it's not been registered", index); 
}

s32 audCategoryManager::InitDynamicHeirarchy(u32 categoryNameHash, const MixGroupCategoryMap * map)
{
	if(g_CategoryStore->IsFull())
	{
		audAssertf(0, "In InitDynamicHeirachy: ran out of space in g_CategoryStore");
		return -1;
	}

	s32 ret = InitDynamicCategory(categoryNameHash, NULL, map);

	if(ret >= 0)
	{
		RegisterDynamicHeirachy(ret);
	}

	return ret;
}

void audCategoryManager::ShutdownDynamicHeirachy(s32 index)
{
	SYS_CS_SYNC(m_CategoryUpdateCS);

	if(index < m_CurrentHashIndex)
	{
		audAssertf(0, "Trying to ShutdownDynamicHeirarchy on a category that isn't dynamic!");
		return;
	} 
	UnregisterDynamicHeirachy(index);

	ShutdownDynamicCategory(index);
}

void audCategoryManager::ShutdownDynamicCategory(s32 index)
{
	if(index < m_CurrentHashIndex)
	{
		audAssertf(0, "Trying to ShutdownDynamicCategory on a category that isn't dynamic!");
		return;
	}
	audCategory * cat = GetCategoryFromIndex(index);

	if(!cat)
	{
		audWarningf("Tried to shutdown dynamic category with index %i but it wasn't in th category store", index);
		return;
	}

	for(int i=0; i< Category::MAX_CATEGORYREFS; i++)
	{
		s32 childIndex = cat->GetChildCategoryIndex(i);
		if(childIndex > -1)
		{	
			ShutdownDynamicCategory(childIndex);
		}
	}

	g_CategoryStore->Delete(cat);	
	//The categorystore delete trashes the cat memory in !FINAL which confuses
	//the category when it comes back to being initialized again so we do this after
	//the delete (the memory itself isn't freed)
	cat->Shutdown();
}

s32 audCategoryManager::InitDynamicCategory(u32 categoryNameHash, audCategory * parent, const MixGroupCategoryMap * map)
{
	if(g_CategoryStore->IsFull())
	{
		audAssertf(0, "In InitDynamicCategory: Ran out of space in g_CategoryStore");
		return -1;
	}

	audCategory * cat = static_cast<audCategory *>(g_CategoryStore->New());

	cat->InitDynamic(categoryNameHash, &m_MetadataMgr, parent, map);

	return GetCategoryIndex(cat);
}	

//N.B. if this is too slow, we could consider dynamic category maps too if the initial overhead isn't too much...maybe we could
//extract them quickly from the main category map to save having to sort them
audCategory *audCategoryManager::GetDynamicCategoryPtr(s32 index, u32 hashValue)
{
	Category * metadata = m_MetadataMgr.GetObject<Category>(hashValue);
	return GetDynamicCategoryPtr(index, metadata); 
}

audCategory *audCategoryManager::GetDynamicCategoryPtr(s32 catIndex, const Category * metadata)
{
	audAssertf(catIndex >= m_CurrentHashIndex && catIndex < (int)m_NumberOfCategories, "Invalid catIndex (%i) passed into GetDynamicCategoryPtr", catIndex);
	if(catIndex < 0)
	{
		return NULL;
	} 

	audCategory * cat = GetCategoryFromIndex(catIndex);

	Assert(cat);
	if(!cat)
	{
		return NULL;
	}

	if(metadata == cat->GetMetadata())
	{
		return cat;
	}

	for(int i=0; i < Category::MAX_CATEGORYREFS; i++)
	{
		s32 childIndex = cat->GetChildCategoryIndex(i);
		if(childIndex > -1)
		{
			audCategory * ccat = GetDynamicCategoryPtr(childIndex, metadata);
			if(ccat)
			{
				return ccat;
			}
		}
	}

	return NULL;
}


s32 audCategoryManager::GetDynamicCategoryIndex(s32 catIndex, const Category * metadata)
{
	audAssertf(catIndex >= m_CurrentHashIndex && catIndex < (int)m_NumberOfCategories, "Invalid catIndex (%i) passed into GetDynamicCategoryPtr", catIndex);
	if(catIndex < 0)
	{
		return -1;
	} 

	audCategory * cat = GetCategoryFromIndex(catIndex);

	Assert(cat);
	if(!cat)
	{
		return -1;
	}

	if(metadata == cat->GetMetadata())
	{
		return catIndex;
	}

	for(int i=0; i < Category::MAX_CATEGORYREFS; i++)
	{
		s32 childIndex = cat->GetChildCategoryIndex(i);
		if(childIndex > -1)
		{
			s32 icat = GetDynamicCategoryIndex(childIndex, metadata);
			if(icat >= 0)
			{
				return icat;
			}
		}
	}

	return -1;
}


audCategory *audCategoryManager::GetCategoryPtr(const u32 hashValue)
{
	s32 start = 0, middle, end = (s32)m_NumberOfStaticCategories;
	audCategory *ptr = NULL;

	// check for null hash - saves searching
	if(hashValue == 0)
	{
		return NULL;
	}

	//Binary search for matching wave name hash;
	while(start <= end)
	{
		middle = (start + end) / 2;	//Compute mid-point.
		if(hashValue > m_CategoryMap[middle].key)
		{
			start = middle + 1;		//Repeat search in top half.
		}
		else if(hashValue < m_CategoryMap[middle].key)
		{
			end = middle - 1;		//Repeat search in bottom half.
		}
		else
		{
			//Found it!
			ptr = m_CategoryMap[middle].ptr;
			break;
		}
	}

	return ptr;
}

void audCategoryManager::SortHashTable()
{
	u32 gap = m_NumberOfStaticCategories;
	u32 tempHash;
	audCategory *tempPtr;

	for (;;) 
	{
		gap = (gap * 10) / 13;
		if (gap == 9 || gap == 10)
			gap = 11;
		if (gap < 1)
			gap = 1;

		bool swapped = false;
		for (u32 i = 0; i < m_NumberOfStaticCategories - gap; i++) 
		{
			u32 j = i + gap;
			if (m_CategoryMap[i].key > m_CategoryMap[j].key) 
			{
				tempHash = m_CategoryMap[j].key;
				tempPtr = m_CategoryMap[j].ptr;
				
				m_CategoryMap[j].key = m_CategoryMap[i].key;
				m_CategoryMap[j].ptr = m_CategoryMap[i].ptr;

				m_CategoryMap[i].key = tempHash;
				m_CategoryMap[i].ptr = tempPtr;

				swapped = true;
			}
		}
		if(gap == 1 && !swapped)
		{
			break;
		}
	}
}

void audCategoryManager::SetVolume(const char* category, float volume)
{
	SetVolume((const u32)atStringHash(category), volume);
}
void audCategoryManager::SetPitch(const char* category, s32 pitch)
{
	SetPitch((const u32)atStringHash(category), pitch);
}
void audCategoryManager::SetDistanceRollOffScale(const char* category, float distanceRollOffScale)
{
	SetDistanceRollOffScale((const u32)atStringHash(category), distanceRollOffScale);
}

void audCategoryManager::SetVolume(const u32 categoryHash, float volume)
{
	audCategory* category = GetCategoryPtr(categoryHash);
	if (!category)
	{
		return;
	}
	category->SetVolume(volume);
}

void audCategoryManager::SetPitch(const u32 categoryHash, s32 pitch)
{
	audCategory* category = GetCategoryPtr(categoryHash);
	if (!category)
	{
		return;
	}
	category->SetPitch(pitch);
}

void audCategoryManager::SetDistanceRollOffScale(const u32 categoryHash, float distanceRollOffScale)
{
	audCategory* category = GetCategoryPtr(categoryHash);
	if (!category)
	{
		return;
	}
	category->SetDistanceRollOffScale(distanceRollOffScale);
}
#if AUD_ATOMIC_CATEGORIES
void audCategoryManager::CommitCategorySettings()
{
	// Unlike sounds, we have a global GameThreadIndex, and just update it here, rather than in every category.
	// (This assumes we only update categories from one thread, which is the rule - don't want CategoryControllers.)
	// We do however have to ripple through every category and copy the current settings over to the next index,
	// or any changes would be lost.

	u16 nextCategorySettingsGameThreadIndex = (m_CategorySettingsGameThreadIndex+1)%3;
	audCategory *baseCategory = GetCategoryFromIndex(0);
	if (!baseCategory)
	{
		Assert(baseCategory);
		return;
	}
	baseCategory->CommitCategorySettings(m_CategorySettingsGameThreadIndex, nextCategorySettingsGameThreadIndex);

	for(int i=0; i<NUM_DYNAMIC_HEIRACHIES; i++)
	{
		if(g_DynamicHeirachies[i] >= 0)
		{
			audCategory *dynamicBase = GetCategoryFromIndex(g_DynamicHeirachies[i]);
			if(!dynamicBase)
			{
				Assert(dynamicBase);
				continue;
			}
			dynamicBase->CommitCategorySettings(m_CategorySettingsGameThreadIndex, nextCategorySettingsGameThreadIndex);
		}
	}
	m_CategorySettingsGameThreadIndex = nextCategorySettingsGameThreadIndex;
}
#endif // AUD_ATOMIC_CATEGORIES

u16 audCategoryManager::GetGameThreadCategoryIndex()
{
	return m_CategorySettingsGameThreadIndex;
}

void audCategoryManager::UpdateAudioThreadCategoryIndex()
{
	m_CategorySettingsAudioThreadIndex = (m_CategorySettingsGameThreadIndex+2)%3;
}

void audCategoryManager::Update()
{
	SYS_CS_SYNC(m_CategoryUpdateCS);

	// Work down to each category, calculating the new volume, pitch, etc. This can be done just once,
	// instead of calculating it for every category every time a sound requests it. By working down the hierarchy,
	// a category can work out and store its parameters, and then respond when its children ask.
	audCategory *baseCategory = GetCategoryFromIndex(0);
	if (!baseCategory)
	{
		Assert(baseCategory);
		return;
	}

#if __BANK
	m_IsSoloActive = baseCategory->IsSoloActive();
	m_RecursionDebugIndex = 0;
#endif
	baseCategory->AudioThreadUpdate(m_CategorySettingsAudioThreadIndex, true);

	for(int i=0; i<NUM_DYNAMIC_HEIRACHIES; i++)
	{
		if(g_DynamicHeirachies[i] >= 0)
		{
			audCategory *dynamicBase = GetCategoryFromIndex(g_DynamicHeirachies[i]);
			if(!dynamicBase)
			{
				Assert(dynamicBase);
				continue;
			}
#if __BANK
			m_RecursionDebugIndex = i;
#endif
			dynamicBase->AudioThreadUpdate(m_CategorySettingsGameThreadIndex,false);
		}
	} 
}

void audCategoryManager::OnObjectModified(const u32 BANK_ONLY(nameHash))
{
#if __BANK
	audCategory *baseCategory = GetCategoryPtr(ATSTRINGHASH("BASE", 0x44E21C90));
	audCategory *cat = GetCategoryPtr(nameHash);
	if(cat && baseCategory)
	{
		// First update flags on the edited category
		cat->PullMetadataValues(m_MetadataMgr.GetObject<Category>(nameHash));

		// Ensure we have an up to date 'is solo active' flag (need to check the entire hierarchy)
		m_IsSoloActive = baseCategory->IsSoloActive();

		// Then ripple down and update the entire hierarchy.  To correctly process mute/solo we have to
		// update the entire hierarchy, not just the edited category.
		baseCategory->PullMetadataValues();
	}
#endif
}

void audCategoryManager::OnMuteListModified()
{
	OnObjectModified(ATSTRINGHASH("BASE", 0x44E21C90));
}

void audCategoryManager::OnSoloListModified()
{
	OnObjectModified(ATSTRINGHASH("BASE", 0x44E21C90));
}

#if __BANK

void audCategoryManager::PrintResolvedVolumes() const
{
	for(u32 i = 0; i < m_NumberOfCategories; i++)
	{
		const audCategory *category = GetCategoryFromIndex((s32)i);
		if(category)
		{
			const char *name = category->GetNameString();
			audDisplayf("%u: %s,Vol %.2f, LPF %u", i, name ? name : "(unknown)", g_ResolvedCategorySettings[i].Volume.GetFloat32_FromFloat16(), g_ResolvedCategorySettings[i].LPFCutoff);
		}
	}
}

Category *audCategoryManager::GetMetadataPtr(const u32 hash)
{
	return m_MetadataMgr.GetObject<Category>(hash);
}

void audCategoryManager::PullMetadataValues()
{
	m_IsSoloActive = GetCategoryFromIndex(0)->IsSoloActive();
	GetCategoryFromIndex(0)->PullMetadataValues();
}

bool audCategoryManager::IsMuteActive() const
{
	return GetCategoryFromIndex(0)->IsMuteActive();
}

void audCategoryManager::UpdateTelemetry()
{
	if(m_TelemetryEnabled && !PARAM_disablecategorytelemetry.Get() && m_MetadataMgr.IsRAVEConnected() && (audDriver::GetFrameCount() & 31) == 31)
	{
		if(!m_HaveSentTelemetryHeader)
		{
			audRemoteControlSerializer serializer(g_AudioEngine.GetRemoteControl().GetPayloadBuffer(), audRemoteControl::kRemoteControlPayloadSizeBytes);

			const u32 numStaticCategories = GetNumberOfStaticCategories();
			serializer.Write(numStaticCategories);

			const u32 propertyNames[] = {
				ATSTRINGHASH("Volume", 0x691362F2),
				ATSTRINGHASH("DistanceRolloffScale", 0xB8A085BA),
				ATSTRINGHASH("OcclusionDamping", 0xE082168C),
				ATSTRINGHASH("EnvironmentalFilterDamping", 0xE8624852),
				ATSTRINGHASH("SourceReverbDamping", 0xF56B1419),
				ATSTRINGHASH("DistanceReverbDamping", 0xDA63917),
				ATSTRINGHASH("InteriorReverbDamping", 0x977E538D),
				ATSTRINGHASH("EnvironmentalLoudness", 0xF26B51E8),
				ATSTRINGHASH("UnderwaterWetLevel", 0x35E7A631),
				ATSTRINGHASH("StonedWetLevel", 0xFFD95E1A),
				ATSTRINGHASH("Pitch", 0x3F4BB8CC),
				ATSTRINGHASH("LPFCutoff", 0xED44A39),
				ATSTRINGHASH("HPFCutoff", 0x655570E9)
			};
			const u32 numProperties = sizeof(propertyNames) / sizeof(propertyNames[0]);
			serializer.Write(numProperties);
			for(u32 i = 0; i < numProperties; i++)
			{
				serializer.Write(propertyNames[i]);
			}

			// category names
			for(s32 i = 0; i < (s32)numStaticCategories; i++)
			{
				serializer.Write(atStringHash(GetCategoryFromIndex(i)->GetNameString()));
			}
			m_HaveSentTelemetryHeader = g_AudioEngine.GetRemoteControl().SendCommand(serializer, CATEGORY_TELEMETRY_HEADER);
		} // if !sentTelemetryHeader

		if(m_HaveSentTelemetryHeader)
		{
			audRemoteControlSerializer serializer(g_AudioEngine.GetRemoteControl().GetPayloadBuffer(), audRemoteControl::kRemoteControlPayloadSizeBytes);
			const s32 numStaticCategories = (s32)GetNumberOfStaticCategories();
			for(s32 i = 0; i < numStaticCategories; i++)
			{
				serializer.Write(g_ResolvedCategorySettings[i].Volume.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].DistanceRollOffScale.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].OcclusionDamping.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].EnvironmentalFilterDamping.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].SourceReverbDamping.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].DistanceReverbDamping.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].InteriorReverbDamping.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].EnvironmentalLoudness.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].UnderwaterWetLevel.GetFloat32_FromFloat16());
				serializer.Write(g_ResolvedCategorySettings[i].StonedWetLevel.GetFloat32_FromFloat16());
				serializer.Write((float)g_ResolvedCategorySettings[i].Pitch);
				serializer.Write((float)g_ResolvedCategorySettings[i].LPFCutoff);
				serializer.Write((float)g_ResolvedCategorySettings[i].HPFCutoff);
			}

			g_AudioEngine.GetRemoteControl().SendCommand(serializer, CATEGORY_TELEMETRY_UPDATE);
		}
	} // IsRaveConnected
}

#endif // __BANK

} // namespace rage
