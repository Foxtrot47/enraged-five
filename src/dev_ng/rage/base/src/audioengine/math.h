// 
// audioengine/math.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOENGINE_MATH_H 
#define AUDIOENGINE_MATH_H 

namespace rage {

class audFixed16Scalar // similar to Float16 class, but stores linear [0..1] values mapped to [0x0000..0xffff]
{
public:

	// PURPOSE:	Default ctor
	audFixed16Scalar() { }

	// PURPOSE: Ctor
	audFixed16Scalar(float f) { Set(f); }

	// PURPOSE:	Assignment operator
	audFixed16Scalar& operator=(float f) { Set(f); return *this; }

	// PURPOSE:	Initialize value from float
	void Set(float f) { FastAssert(f<=1.f); FastAssert(f>=0.f); m_Data = u16(f * 0xffff); } // should be u16(f * 0xffff + 0.5f)

	// PURPOSE:	Return current value as float
	float Get() const { return f32(m_Data / (f32)0xffff); }

	// PURPOSE:	Test for equality
	bool operator==(const audFixed16Scalar that) const { return m_Data == that.m_Data; }

	// PURPOSE:	Test for inequality
	bool operator!=(const audFixed16Scalar that) const { return m_Data != that.m_Data; }

	/*audFixed16Scalar& operator+=(float f) { Set(Get() + f); return *this; }
	audFixed16Scalar& operator-=(float f) { Set(Get() - f); return *this; }
	audFixed16Scalar& operator*=(float f) { Set(Get() * f); return *this; }
	audFixed16Scalar& operator/=(float f) { Set(Get() / f); return *this; }

	audFixed16Scalar& operator+=(audFixed16Scalar f) { Set(Get() + f.Get()); return *this; }
	audFixed16Scalar& operator-=(audFixed16Scalar f) { Set(Get() - f.Get()); return *this; }
	audFixed16Scalar& operator*=(audFixed16Scalar f) { Set(Get() * f.Get()); return *this; }
	audFixed16Scalar& operator/=(audFixed16Scalar f) { Set(Get() / f.Get()); return *this; }*/

	operator float() const { return Get(); }
	u16 GetData() const		{ return m_Data; }
	void SetData(u16 u)		{ m_Data = u; }

private:
	u16 m_Data;
};


} // namespace rage

#endif // AUDIOENGINE_MATH_H 
