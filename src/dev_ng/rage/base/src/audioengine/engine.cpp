//
// audioengine/engine.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "system/timemgr.h"
#include "system/param.h"
#include "system/performancetimer.h"
#include "profile/cputrace.h"
#include "profile/profiler.h"
#include "grprofile/pix.h"

#if __BANK
#include "grcore/im.h"
#include "grcore/viewport.h"
#endif

#include "audiohardware/config.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/voicemgr.h"
#include "audioeffecttypes/reverbeffect.h"
#include "audiosoundtypes/environmentsound.h"
#include "audiosynth/synthesizer.h"

#include "categorymanager.h"
#include "controller.h"
#include "curverepository.h"
#include "engine.h"
#include "engineutil.h"
#include "environment.h"
#include "remotecontrol.h"
#include "soundfactory.h"
#include "soundmanager.h"

#if __BANK && RSG_DURANGO
#include "audiohardware/decoder_xbonexma.h"
#endif
#if __BANK && RSG_PC
#include "audiohardware/device_xaudio_pc.h"
#endif


#if __XENON && !__FINAL
#include "system/xtl.h"
#include "tracerecording.h"
#pragma comment(lib, "xbdm.lib")
#pragma comment(lib, "tracerecording.lib")
#endif

#if __PS3
#include <sn/libsntuner.h>
#endif // __PS3

#define TIME_AUDIO_ENGINE 1

XPARAM(audiodesigner);

#if RSG_PC
NOSTRIP_PC_PARAM(benchmarknoaudio, "Disable audio processing for graphics benchmark purposes");
NOSTRIP_PC_XPARAM(benchmark);
#endif // RSG_PC

namespace rage
{
	audEngine g_AudioEngine;

#if __BANK
	extern bool g_PrintMixgroupCatMaps;
	extern bool g_PrintMixgroupHeirarchies;
	extern bool g_PrintMixGroupPool;
	extern bool g_PrintMixgroupHeirarchyCats;
	extern int g_DebugCategoryIndex;
#endif

	PARAM(noaudio, "[RAGE Audio] Disable audio (prevent sound creation).");
	PARAM(audio, "[RAGE Audio] Enable audio.");
	
	PARAM(audiowidgets, "Turns on audio widgets by default");
	PARAM(cheapaudioeffects,"[RAGE Audio] Don't instantiate any effects).");
	PARAM(nodynamicmixer, "[RAGE Audio] bypass dynamic mixer initialisation");
	PARAM(noaudiogap, "[RAGE Audio] Disable warning on long audio frame gaps");
#if __PS3
	PARAM(triggeredaudio, "[RAGE Audio - PS3] Trigger audio updates at fixed points in the frame (for profiling)");
#endif

	const char *g_DefaultConfigPath = "config/";

	// PURPOSE
	//	Defines the priority that the audio thread should run at
	const sysIpcPriority audThreadPriority = PRIO_ABOVE_NORMAL;	

	// PURPOSE
	//	The CPU on which the audio thread should run
#if RSG_DURANGO
	const int audThreadCpu = 5; //1
#else
	const int audThreadCpu = 1;
#endif

	// target audio frame time in milliseconds - the audio thread will sleep to try and maintain this
	const f32 g_TargetAudioFrameTime = 15.f;
	const f32 g_TargetPausedAudioFrameTime = 10.f;
	bool g_audUseFrameLimiter = true;


	PF_PAGE(AudioEnginePage, "RAGE Audio Engine");
	PF_GROUP(AudioEngineTiming);
	PF_LINK(AudioEnginePage, AudioEngineTiming);

	PF_VALUE_INT(FrameTime, AudioEngineTiming);
	PF_VALUE_FLOAT(FrameLength, AudioEngineTiming);

	PF_PAGE(AudioTimingPage, "RAGEAudio Timing");
	PF_GROUP(AudioTiming);
	PF_LINK(AudioTimingPage, AudioTiming);

	PF_TIMER(AudioEngine, AudioTiming);
	PF_TIMER(AudioEngine_NoWait, AudioTiming);
	PF_TIMER(WaitOnThreadCommandBuffer, AudioTiming);
	PF_TIMER(VoiceManager, AudioTiming);
	PF_TIMER(SoundManager, AudioTiming);
	PF_TIMER(AudioDriver, AudioTiming);
	PF_TIMER(WaveSlotUpdate, AudioTiming);
	PF_TIMER(UserGameUpdate, AudioTiming);

	sysCriticalSectionToken audEngine::sm_EngineUpdateLock;

#if __BANK
	u32  audEngine::sm_NumberOfPhysicalChannels;
	bool audEngine::sm_OverrideChannels;
	bool g_DrawOverriddenSounds = false;
	bool g_DrawBucketInfo = false;
	f32 g_DrawBucketYScroll = 0.0f;
	u32 g_DrawBucketMaxSounds = 5;
	bool g_TraceAudioFrame = false;
	bool g_DrawMuteSolo = false;

#if RSG_PC
	bool g_ShowDeviceStats = false;
#endif
#if RSG_DURANGO || RSG_PC
	bool g_DumpAudioHardwareOutput = false;
#endif 
#if RSG_DURANGO
	bool g_ShowXMAStats = false;
	bool g_PrintXMAStats = false;
	bool g_PrintUsedXMAContexts = false;
	extern bool g_ShowOutputBufferQueue;
#endif 
#endif // __BANK

bool InitializeAudio(audController &controller, sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator,
	const char *configPath, const char* relativeAudioPath, void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize)
{
	RAGE_TRACK(Rage_InitializeAudio);

	if(audVerifyf(g_AudioEngine.Init(physicalAllocator, virtualAllocator, configPath, relativeAudioPath, externallyAllocatedBankHeap, externallyAllocatedBankHeapSize),"Failed to initialise audio engine - check assets"))
	{	
		// initialise the audController for this thread
		if(audVerifyf(controller.Init(), "Failed to initialise audio controller"))
		{
			// ensure controller is immediately active
			controller.PreUpdate(g_AudioEngine.GetTimeInMilliseconds());
			controller.Update(g_AudioEngine.GetTimeInMilliseconds());
			return true;
		}
	}
	return false;
}

audEngine::audEngine() 
{
#if __PS3
	m_TriggeredUpdates = PARAM_triggeredaudio.Get() && snIsTunerRunning();
	m_EnableTriggeredWait = false;
#else
	m_TriggeredUpdates = false;
	m_EnableTriggeredWait = false;
#endif
}

audEngine::~audEngine()
{

}

bool audEngine::Init(sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator, const char *configPath, const char* relativeAudioPath, void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize)
{
#if __BANK
	sm_NumberOfPhysicalChannels = 0;
	sm_OverrideChannels = false;
#endif
	m_MusicVolume = 1.0f;
	m_SfxVolume = 1.0f;
	m_MasterVolume = 1.0f;
	m_IsAudioEnabled = true;
	m_EngineTimeFrames = 0;
	m_IsGamePaused = false;

	m_VirtualAllocator = m_PhysicalAllocator = NULL;

	m_TimeInMs = 0;
	m_PrevTimeInMs = 0;
	m_TimeDeltaInSeconds = 0.0f;

	//	Initialize the user set audio thread function to DoNothing()
	m_pfUpdateAudioThread = &audEngine::DoNothing;

	//Ensure that the config path is safely defaulted if necessary.
	if(configPath)
	{
		strcpy(m_ConfigPath, configPath);
	}
	else
	{
		strcpy(m_ConfigPath, g_DefaultConfigPath);
	}

	m_ExternallyAllocatedBankHeap = externallyAllocatedBankHeap;
	m_ExternallyAllocatedBankHeapSize = externallyAllocatedBankHeapSize;

	if (relativeAudioPath)
	{
		strcpy(m_RelativeAudioPath, relativeAudioPath);
		const size_t pathLen = strlen(m_RelativeAudioPath);
		if (m_RelativeAudioPath[pathLen-1] != '/')
		{
			m_RelativeAudioPath[pathLen] = '/';
			m_RelativeAudioPath[pathLen+1] = '\0';
		}
	}
	else
	{
		m_RelativeAudioPath[0] = '\0';
	}

	m_VirtualAllocator = virtualAllocator;
	m_PhysicalAllocator = physicalAllocator;


	m_ShouldUseCheapAudioEffects = (PARAM_cheapaudioeffects.Get());
	if(ShouldUseCheapAudioEffects())
	{
		Printf("Using cheap audio effects\n");
	}

	audEntity::InitClass();
	audPcmSource::InitClass();

	Printf("Statically initializing audController...  \n");
	audController::InitClass();
	bool hasSucceeded = PartialInit();

	return hasSucceeded;
}

bool audEngine::PartialInit()
{
	m_ShouldBeVirtualisingEverything = m_CurrentlyVirtualisingEverything = false;

	audDisplayf("Initializing audEngineUtil ...");
	audEngineUtil::InitClass();

#if __BANK
	m_RemoteControl.GenerateUniqueId();
	m_RemoteDebug.GenerateUniqueId();
	
	audDisplayf("Initializing audRemoteControl ... ");
	if(!m_RemoteControl.Init())
	{
		audWarningf("Couldn't connect to rave, will keep trying ...\n");
	}
	audDisplayf("Initializing audRemoteDebug ... ");
	if(!m_RemoteDebug.Init())
	{
		audWarningf("Couldn't connect to audiodebugger, will keep trying ...\n");
	}
#endif

	char configPath[128];
	formatf(configPath, "%s%saudioconfig.dat", m_RelativeAudioPath, m_ConfigPath);
	audConfig::InitClass(configPath);
	
	if(!audDriver::GetConfig().Init())
	{
		audErrorf("Failed to load driver configuration");
		return false;
	}

	audDisplayf("Loading audio engine configuration ...");
	m_Config.Init();

#if !__FINAL
	if(!audDriver::GetConfig().IsAudioProcessingEnabled())
	{
		PARAM_noaudio.Set("yes");
		PARAM_cheapaudioeffects.Set("yes");
	}
#endif

	EnableAudio(true);

	if(!m_CurveRepository.Init())
	{
		audErrorf("audCurveRepository initialize failed!\n");
		return false;
	}

	if(!m_CategoryManager.Init())
	{
		audErrorf("audCategoryManager initialize failed!\n");
		return false;
	}

	if(!PARAM_nodynamicmixer.Get())
	{
		if(!m_DynamicMixMgr.Init())  
		{
			audErrorf("audDynamic initialize failed!");
			return false;
		}
	}

	Printf("Statically Initializing audSoundManager ... \n");
	if(!audSoundManager::InitClass())
	{
		audErrorf("audSoundManager failed to statically initialise!\n");
		return false;
	}

	Printf("Initializing audSoundManager instance...  \n");
	if(!m_SoundManager.Init())
	{
		audErrorf("audSoundManager failed to initialise!\n");
		return false;
	}
	
	audDisplayf("Initializing AMP runtime... ");
	if(!synthSynthesizer::InitClass())
	{
		audErrorf("Failed to initialise AMP runtime");
		return false;
	}

	if(audDriver::GetConfig().IsAudioProcessingEnabled())
	{
		Printf("Initializing audDriver ...  \n");
		if(!audDriver::InitClass(m_PhysicalAllocator, m_VirtualAllocator, m_ExternallyAllocatedBankHeap, m_ExternallyAllocatedBankHeapSize))
		{
			audErrorf("audDriver failed to initialise!\n");
			return false;
		}

	#if __BANK
		sm_NumberOfPhysicalChannels = audDriver::GetConfig().GetNumPhysicalVoices()-audDriver::GetConfig().GetNumNewPhysicalVoiceBufferVoices();
	#endif
		m_NumPhysicalVoices = audDriver::GetVoiceManager().GetNumberOfPhysicalVoicesAvailable();

		// Initialise a command buffer for the game thread
		const u32 commandBufferSize = audConfig::GetValue("engineSettings_GameThreadCommandBuffer", 32U * 1024U) WIN32PC_ONLY( *COMMAND_BUFFER_SIZE_MUTIPLIER );
		audDriver::GetMixer()->InitClientThread("GameThread", commandBufferSize);

		Printf("Initialising audEnvironment ...  \n");
		m_Environment.Init();
	}

	Printf("Initialising audEnvironmentSound ... \n");
	audEnvironmentSound::InitClass();

	if(audDriver::GetConfig().IsAudioProcessingEnabled())
	{
		Printf("Starting audio thread ...  \n");
		m_ShouldThreadBeRunning = true;
		if(!InitAudioThread())
		{
			return false;
		}
	}
	
	Printf("Flagging Controllers to go active ...\n");
	audController::FlagControllersActive(true);

	return true;
}

void audEngine::Shutdown()
{
	PartialShutdown();

	audEntity::ShutdownClass();

	Printf("Statically shutting down audController ...\n");
	audController::ShutdownClass();
}

void audEngine::PartialShutdown()
{
	audDisplayf("num active controllers: %d", audController::GetNumberOfActiveControllers());

	// We stop the audController in this thread directly, since it'll never get serviced as we sit here waiting.
	audController* controller;
	controller=audController::FindControllerForThread();
	Assert(controller); // For now, we really should have a controller in this thread - remove this later
	if (controller)
	{
		controller->SetActive(false);
	}

	// We clear out the sounds in audControllers first
	audController::FlagControllersActive(false);

	while(audController::GetNumberOfActiveControllers() != 0)
	{
		sysIpcSleep(10);
	}

#if __BANK
	Printf("Shutting down audRemoteControl ...\n");
	m_RemoteControl.Shutdown();
	Printf("Shutting down audRemoteDebug ...\n");
	m_RemoteDebug.Shutdown();
#endif

	Printf("Stopping audio thread ...\n");
	// signal that our other thread should exit
	m_ShouldThreadBeRunning = false;

	while(m_IsThreadRunning)
	{
		sysIpcSleep(10);
	}

	sysIpcWaitThreadExit(m_ThreadId);

#if RSG_PC || RSG_ORBIS || RSG_DURANGO || __PPU
	// on PC and PS3 shut down driver before killing effects etc...
	Printf("Shutting down audDriver ...\n");
	audDriver::ShutdownClass();
#endif

	audDisplayf("Shutting down audSoundManager ...\n");
	m_SoundManager.Shutdown();
	audSoundManager::ShutdownClass();

	audDisplayf("Shutting down audEnvironment ...\n");
	m_Environment.Shutdown();

	audDisplayf("Shutting down curve repository...\n");
	m_CurveRepository.Shutdown();

	audDisplayf("Shutting down dynamic mix nanager...\n");
	m_DynamicMixMgr.Shutdown();

	audDisplayf("Shutting down category manager...\n");
	m_CategoryManager.Shutdown();

	audDisplayf("Shutting down modular synthesizer...");
	synthSynthesizer::ShutdownClass();

	audDisplayf("Shutting down audEngineUtil ...\n");
	audEngineUtil::ShutdownClass();

#if !(RSG_PC || RSG_ORBIS || RSG_DURANGO || __PPU)
	audDisplayf("Shutting down audDriver ...\n");
	audDriver::ShutdownClass();
#endif

	audConfig::ShutdownClass();
}

bool audEngine::Reset()
{
	audEngine::PartialShutdown();

	if(!audEngine::PartialInit())
	{
		return false;
	}

	return true;
}

DECLARE_THREAD_FUNC(audEngine::ThreadEntryProc)
{
	// disables the warning that ptr is unreferenced 
	(void)ptr;
	g_AudioEngine.m_AudioThreadIdForComparison = sysIpcGetCurrentThreadId();
	g_AudioEngine.m_IsThreadRunning = true;

	g_AudioEngine.m_SoundManager.InitCommandBuffers();

	while(g_AudioEngine.m_ShouldThreadBeRunning)
	{
		if(!g_AudioEngine.m_ShouldThreadBePaused)
		{
			EnterUpdateLock();
			g_AudioEngine.AudioFrame();
			ExitUpdateLock();
		}
		else
		{
			sysIpcSleep(1);
		}
	}

	g_AudioEngine.m_IsThreadRunning = false;
}

void audEngine::AudioFrame()
{
	RAGETRACE_INITTHREAD("AudioEngine", 256, 1);

	if (m_TriggeredUpdates)
	{
		sysIpcWaitSema(m_UpdateSema);
		m_EnableTriggeredWait = true;
	}

	if(audDriver::GetMixer())
	{
		audDriver::GetMixer()->EnterReplaySwitchLock();
	}

	PIXBegin(0, "AudioFrame");

	sysPerformanceTimer audioFrameTimer("audioFrameTimer");
	audioFrameTimer.Start();


	{
		PF_FUNC(AudioEngine);

		{
			PF_FUNC(WaitOnThreadCommandBuffer);
			if(audDriver::GetMixer() && audDriver::GetMixer()->IsCapturing() && audDriver::GetMixer()->IsFrameRendering() && m_EnableTriggeredWait)
			{
				audDriver::GetMixer()->TriggerUpdate();
				audDriver::GetMixer()->WaitOnMixBuffers();
			}
			audDriver::GetMixer()->WaitOnThreadCommandBufferProcessing();
		}

		PF_START(AudioEngine_NoWait);

		audDriver::IncrementFrameCount();

		m_PrevTimeInMs = m_TimeInMs;

		m_MixerTimeFrames = audDriver::GetMixer()->GetMixerTimeFrames();

		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);

		// 128/48 = 2.66666667ms per frame
		// multiply by a fixed power of two and round to maintain reasonable accuracy
		// * 2^29 = 1431655765.3333333333333333333333
		// Error is ~0.02ms over a 24 hour period
		// 1431655765 / 2^29 = 2.66666666604578495025634765625
		const u64 mixBufferLengthMsScaledUp = 1431655765;
		const u64 shiftDown = kMixBufNumSamples == 256 ? 28ULL : 29ULL;
		const u64 mixerTimeScaledUp = m_MixerTimeFrames * mixBufferLengthMsScaledUp;
		const u64 mixerTimeMs = mixerTimeScaledUp >> shiftDown;
		m_TimeInMs = static_cast<u32>(mixerTimeMs);
		
		m_TimeDeltaInSeconds = (m_TimeInMs-m_PrevTimeInMs) * (1.0f / 1000.0f);

#if __STATS
		// frame time is milliseconds between updates
		const u32 frameTimeMs = (m_TimeInMs-m_PrevTimeInMs);
		PF_SET(FrameTime,frameTimeMs);
		if(PARAM_audiodesigner.Get() && frameTimeMs >= 25 &&!PARAM_noaudiogap.Get())
		{
			audWarningf("Long gap between audio frames - %ums", frameTimeMs);
		}
#endif // __STATS
		
		if(m_ShouldBeVirtualisingEverything && !m_CurrentlyVirtualisingEverything)
		{
			m_NumPhysicalVoices = audDriver::GetVoiceManager().GetNumberOfPhysicalVoicesAvailable();
			audDriver::GetVoiceManager().SetNumberOfPhysicalVoices(0);
			m_CurrentlyVirtualisingEverything = true;
		}
		else if(!m_ShouldBeVirtualisingEverything && m_CurrentlyVirtualisingEverything)
		{
			audDriver::GetVoiceManager().SetNumberOfPhysicalVoices(m_NumPhysicalVoices);
			m_CurrentlyVirtualisingEverything = false;
		}

		{
			PF_FUNC(SoundManager);
			m_SoundManager.Update(m_TimeInMs);
		}

		{
			PF_FUNC(AudioDriver);
			audDriver::Update(m_TimeInMs);
		}

		{
			PF_FUNC(UserGameUpdate);
			(*m_pfUpdateAudioThread)();
		}

#if AUD_ENABLE_AMBISONICS
		m_Ambisonics.Update();
#endif
		
	#if __BANK
		m_RemoteControl.Update(m_TimeInMs);
		m_RemoteDebug.Update(m_TimeInMs);
		if (sm_OverrideChannels && !(m_CurrentlyVirtualisingEverything ||m_ShouldBeVirtualisingEverything))
		{
			audDriver::GetVoiceManager().SetNumberOfPhysicalVoices(sm_NumberOfPhysicalChannels);
		}
	#endif

		audDriver::GetMixer()->FlagThreadCommandBufferReadyToProcess(m_MixerTimeFrames);

		PF_STOP(AudioEngine_NoWait);
	}
	audioFrameTimer.Stop();
	PIXEnd();

	if(audDriver::GetMixer())
	{
		audDriver::GetMixer()->ExitReplaySwitchLock();
	}

	if(m_EnableTriggeredWait)
	{
		if(!m_TriggeredUpdates)
		{
			m_EnableTriggeredWait = false;
		}
		sysIpcSignalSema(m_UpdateFinishSema);
	}
	else
	if(g_audUseFrameLimiter)
	{
#if __WIN32PC && !__TOOL
		if(m_IsGamePaused)
		{
			sysIpcSleep((u32)(Clamp(g_TargetPausedAudioFrameTime - audioFrameTimer.GetTimeMS(), 0.0f, g_TargetPausedAudioFrameTime)));
		}
		else
		{
			float fMaxTime = Clamp(g_TargetAudioFrameTime - audioFrameTimer.GetTimeMS(), 0.0f, g_TargetAudioFrameTime);
			audioFrameTimer.Reset();
			audioFrameTimer.Start();
			static u32 uLastFrameCount = 0;
			uLastFrameCount = GRCDEVICE.GetFrameCounter();

			while(GRCDEVICE.GetFrameCounter() == uLastFrameCount)
			{
				if(audioFrameTimer.GetElapsedTimeMS() > fMaxTime)
				{
					break;
				}
				sysIpcSleep(1);
			}
		}
#else
		sysIpcSleep((u32)(Clamp(g_TargetAudioFrameTime - audioFrameTimer.GetTimeMS(), 0.0f, g_TargetAudioFrameTime)));
#endif
	}

	m_EngineTimeFrames++;
}

void audEngine::TriggerUpdate()
{
	sysIpcSignalSema(m_UpdateSema);
}

void audEngine::WaitForAudioFrame()
{
	sysIpcWaitSema(m_UpdateFinishSema);
}

void audEngine::PollForAudioFrame()
{
	sysIpcPollSema(m_UpdateFinishSema);
}

bool audEngine::InitAudioThread()
{
	m_UpdateSema = sysIpcCreateSema(false);
	m_UpdateFinishSema = sysIpcCreateSema(false);

	m_ThreadId = sysIpcCreateThread(audEngine::ThreadEntryProc, NULL, RSG_ORBIS ? 36864 : 18432, audThreadPriority,"[RAGE Audio] - Engine Thread", audThreadCpu, "RageAudioEngineThread");
	audAssertf(m_ThreadId != sysIpcThreadIdInvalid, "Couldn't create audio thread!");

	u32 uCountDown = 1000;
	while(!m_IsThreadRunning && uCountDown != 0)
	{
		uCountDown--;
		sysIpcSleep(10);
	}
	if (uCountDown == 0)
	{
		Quitf(ERR_AUD_INIT_1,"Audio hardware failed to initialize");
	}
	return true;
}

void audEngine::CommitGameSettings(u32 timeInMs)
{
	if (audDriver::GetConfig().IsAudioProcessingEnabled())
	{
		m_Environment.CommitListenerSettings(timeInMs);
	}
#if AUD_ATOMIC_CATEGORIES
	m_CategoryManager.CommitCategorySettings();
#endif
	m_SoundManager.CommitGameSettings();
}

u32 audEngine::GetNumberOfActivePhysicalVoices() const
{
	return audDriver::GetVoiceManager().GetNumberOfActivePhysicalVoices();
}

bool audEngine::IsGameInControlOfMusicPlayback() const
{
	return audDriver::IsGameInControlOfMusicPlayback();
}

void audEngine::EnableAudio(const bool enable)
{
#if RSG_PC
	if(PARAM_benchmark.Get() && PARAM_benchmarknoaudio.Get())
	{
		m_IsAudioEnabled = false;
	}
	else
#endif // RSG_PC
	if (!audDriver::GetConfig().IsAudioProcessingEnabled())
	{
		m_IsAudioEnabled = false;
	}
	else
	{
		if(PARAM_noaudio.Get())
		{
			m_IsAudioEnabled = false;
		}
		else if(PARAM_audio.Get())
		{
			m_IsAudioEnabled = true;
		}
		else
		{
			m_IsAudioEnabled = enable;
		}
	}

	if (m_IsAudioEnabled)
	{
		Printf("Audio Enabled\n");
	}
	else
	{
		Printf("Audio Disabled\n");
	}
}

bool audEngine::IsUserMusicPlaying() const
{
	return audDriver::GetVoiceManager().IsUserMusicPlaying();
}

void audEngine::OverrideUserMusic()
{
	audDriver::GetVoiceManager().RequestUserMusicOverride();
}

void audEngine::RestoreUserMusic()
{
	audDriver::GetVoiceManager().RequestUserMusicRestore();
}

void* audEngine::AllocateVirtual(const size_t size, const size_t align)
{
	//sysMemUseMemoryBucket bucket(MEMBUCKET_AUDIO);
	Assert(m_VirtualAllocator);
	return m_VirtualAllocator->RAGE_LOG_ALLOCATE(size,align);
}

void audEngine::FreeVirtual(const void *ptr)
{
	//sysMemUseMemoryBucket bucket(MEMBUCKET_AUDIO);
	Assert(m_VirtualAllocator);
	m_VirtualAllocator->Free(ptr);
}

void* audEngine::AllocatePhysical(const size_t size, const size_t align)
{
	//sysMemUseMemoryBucket bucket(MEMBUCKET_AUDIO);
	Assert(m_PhysicalAllocator);
	return m_PhysicalAllocator->RAGE_LOG_ALLOCATE(size, align);
}

void audEngine::FreePhysical(const void *ptr)
{
	//sysMemUseMemoryBucket bucket(MEMBUCKET_AUDIO);
	Assert(m_PhysicalAllocator);
	m_PhysicalAllocator->Free(ptr);
}

void audEngine::DrawDebug()
{
#if __BANK
	m_RemoteControl.DrawDebug();
	if(g_DrawOverriddenSounds)
	{
		m_SoundManager.DrawOverriddenSounds();
	}
	if(g_DrawBucketInfo)
	{
		audSound::GetStaticPool().DebugDrawBuckets(g_DrawBucketYScroll, g_DrawBucketMaxSounds);
	}
	if(g_DrawMuteSolo)
	{
		PUSH_DEFAULT_SCREEN();
		if(m_CategoryManager.IsSoloActive())
		{
			grcColor(Color32(255,0,0));
			grcDraw2dText(70.f, 120.f, "SOLO ACTIVE");
		}
		else if(m_CategoryManager.IsMuteActive())
		{
			grcColor(Color32(255,120,65));
			grcDraw2dText(70.f, 120.f, "MUTE ACTIVE");
		}
		POP_DEFAULT_SCREEN();
	}
#if RSG_DURANGO
	if(g_ShowXMAStats)
	{
		audDecoderXboxOneXma::UpdateDecoderStats();
	}
	if(g_PrintXMAStats)
	{
		g_PrintXMAStats = false;
		audDecoderXboxOneXma::PrintDecoderStats();
	}
	if(g_PrintUsedXMAContexts)
	{
		g_PrintUsedXMAContexts = false;
		audDecoderXboxOneXma::PrintUsedXmaContexts();
	}
#endif
//#if RSG_PC
//	if(g_ShowDeviceStats)
//	{
//		audMixerDeviceXAudio2::ShowDeviceStats();
//	}
//#endif

	audDriver::GetVoiceManager().DrawDebug();
	audDriver::GetMixer()->DrawDebug();
#endif
}

#if __BANK

void ResetAudioEngineCallback()
{
	g_AudioEngine.Reset();
}

void PrintCategoryVolsCB()
{
	g_AudioEngine.GetCategoryManager().PrintResolvedVolumes();
}

extern bool g_FullCurveTweakAbility;
#if RSG_PC
bool g_ShowAudioFrameSkipDebugInfo = false;
#endif

void audEngine::AddWidgets(bkBank &bank)
{
	if(!audDriver::GetConfig().IsAudioProcessingEnabled())
	{
		return;
	}

	bank.PushGroup("audEngine");
	bank.AddToggle("AudioEnabled", &m_IsAudioEnabled);
	bank.AddToggle("FrameLimiter", &g_audUseFrameLimiter);
#if RSG_PC
	bank.AddToggle("Show Audio Frame Skip Debug Info", &g_ShowAudioFrameSkipDebugInfo);
#endif
	bank.AddButton("Reset Audio Engine", datCallback(CFA(ResetAudioEngineCallback)));
	bank.AddSlider("Channels", &sm_NumberOfPhysicalChannels, 0, audDriver::GetConfig().GetNumPhysicalVoices()-audDriver::GetConfig().GetNumNewPhysicalVoiceBufferVoices(), 1);
	bank.AddToggle("Override channels", &sm_OverrideChannels);
	bank.AddToggle("FullCurveTweakAbility",&g_FullCurveTweakAbility);
	bank.AddToggle("DrawOverriddenSounds", &g_DrawOverriddenSounds);
	bank.AddToggle("DrawBucketInfo", &g_DrawBucketInfo);
	bank.AddSlider("DrawBucketYScroll", &g_DrawBucketYScroll, 0.0f, 5.0f, 0.02f);
	bank.AddSlider("DrawBucketMaxSounds", &g_DrawBucketMaxSounds, 0, 500, 1);
	bank.AddToggle("DrawMuteSolo", &g_DrawMuteSolo);
	//bank.AddToggle("TriggeredUpdate", &m_TriggeredUpdates);
#if RSG_PC
	bank.AddToggle("Show Device Stats", &g_ShowDeviceStats);
#endif
#if RSG_DURANGO || RSG_PC
	bank.AddToggle("Dump hardware output", &g_DumpAudioHardwareOutput);
#endif
#if RSG_DURANGO
	bank.AddToggle("Show XMA stats", &g_ShowXMAStats);
	bank.AddToggle("Print XMA stats", &g_PrintXMAStats);
	bank.AddToggle("Print used XMA contexts", &g_PrintUsedXMAContexts);
	bank.AddToggle("Show Output Buffer Q", &g_ShowOutputBufferQueue);
#endif
	m_SoundManager.AddWidgets(bank);
	audDriver::AddWidgets(bank);
	audController::AddWidgets(bank);
	audWaveSlot::AddWidgetsStatic(bank);
	m_Environment.AddWidgets(bank);

#if AUD_ENABLE_AMBISONICS
	m_Ambisonics.AddWidgets(bank);
#endif

	audRemoteDebug::AddWidgets(bank);

	bank.AddButton("PrintCategoryVols", CFA(PrintCategoryVolsCB));

	bank.AddToggle("g_PrintMixgroupCatMaps", &g_PrintMixgroupCatMaps);
	bank.AddToggle("g_PrintMixgroupHeirarchies", &g_PrintMixgroupHeirarchies);
	bank.AddToggle("g_PrintMixgroupPool", &g_PrintMixGroupPool);
	bank.AddToggle("g_PrintMixgroupHeirarchyCats", &g_PrintMixgroupHeirarchyCats);
	bank.AddSlider("g_DebugCategoryIndex", &g_DebugCategoryIndex, -1, 500, 1);

	bank.PopGroup();
}

#endif

} // namespace rage
