//
// audioengine/curverepository.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
#include "curverepository.h"

#include "curve.h"
#include "curvedefs.h"
#include "engine.h"
#include "metadatamanager.h"

#include "file/device.h"
#include "math/amath.h"

namespace rage {

audCurveRepository::audCurveRepository()
{
}

audCurveRepository::~audCurveRepository()
{
}

bool audCurveRepository::Init()
{
	const char *metadataFileName = g_AudioEngine.GetConfig().GetCurveMetadata();
	const bool ret = m_MetadataMgr.Init("Curves", metadataFileName, audMetadataManager::External_NameTable_BankOnly, CURVEDEFS_SCHEMA_VERSION, true);

#if RSG_BANK
	m_MetadataMgr.SetObjectModifiedCallback(this);
#endif

	return ret;
}

void audCurveRepository::Shutdown()
{
	m_MetadataMgr.Shutdown();
}

bool audCurveRepository::GetCurveMetadataPtr(u32 curveNameHash, void** metadata)
{
	*metadata = m_MetadataMgr.GetObjectMetadataPtr(curveNameHash);

	if ((*metadata)==NULL)
	{
		return false;
	}
	return true;
}

#if RSG_BANK
void audCurveRepository::OnObjectModified(const u32 nameHash)
{
	g_AudioEngine.GetEnvironment().OnCurveModified(nameHash);
}

void audCurveRepository::OnObjectOverridden(const u32 nameHash)
{
	g_AudioEngine.GetEnvironment().OnCurveModified(nameHash);
}
#endif // RSG_BANK
} // namespace rage
