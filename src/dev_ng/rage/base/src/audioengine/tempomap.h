#ifndef AUD_TEMPOMAP_H
#define AUD_TEMPOMAP_H

#include "atl/array.h"
#include "audiohardware/channel.h"
#include "math/amath.h"

namespace rage
{

class audTempoMap
{
public:
	enum {MaxTempoChanges = 16};

	void AddTempoChange(const float timeOffsetSeconds, const float newTempo, const u32 newMeter) 
	{
		float lengthS = timeOffsetSeconds;
		// Convert zero-based time into a time relative to last marker
		// NOTE: relies on markers being added in order, which should always be the case.
		for(s32 i = 0; i < m_TempoMapEntries.GetCount(); i++)
		{
			lengthS -= m_TempoMapEntries[i].LengthSeconds;
		}
		m_TempoMapEntries.Append().Init(lengthS, newTempo, newMeter); 
	}	

	void Reset()
	{
		m_TempoMapEntries.Reset();
	}

	bool HasEntries() const { return m_TempoMapEntries.GetCount() > 0; }

	static float ComputeBeatLength(const float tempo)
	{
		return 60.f / tempo;
	}

	static float ComputeBarLength(const float tempo, const u32 beatsPerBar)
	{
		return ComputeBeatLength(tempo) * static_cast<float>(beatsPerBar);
	}

	static float ComputeNumBeats(const float time, const float tempo)
	{
		return time / ComputeBeatLength(tempo);
	}

	struct TimelinePosition
	{
		TimelinePosition()
			: Bar(1)
			, Beat(1)
			, BeatOffsetS(0.f)
			, Tempo(0.f)
			, Meter(4)
		{

		}
		u32 Bar;
		u32 Beat;
		float BeatOffsetS;
		float Tempo;
		float BeatLength;
		u32 Meter;

		void IncrementBeat()
		{
			Beat++;

			if(Beat > Meter)
			{
				Bar++;
				Beat = 1;
			}
		}

		void DecrementBeat()
		{
			if(Beat > 1 || Bar > 1)
			{
				Beat--;

				if(Beat < 1)
				{
					Beat = Meter;
					Bar--;
				}
			}			
		}
	};

	// PURPOSE
	//	Compute the musical timeline position from the specified time in seconds
	void ComputeTimelinePosition(const float timeS, TimelinePosition &pos) const;

	// PURPOSE
	//	Returns the real time in seconds for the specified musical timeline position
	float ComputeTimeForPosition(const TimelinePosition &inputPos) const;

	// PURPOSE
	//	Compute the musical timeline position for the previous beat
	f32 ComputeNextBeatTime(const f32 timeMs) const;

	// PURPOSE
	//	Compute the musical timeline position for the next beat
	f32 ComputePrevBeatTime(const f32 timeMs) const;

private:

	struct TempoChange
	{
		void Init(const float thisLengthSeconds, const float newTempo, const u32 newMeter)
		{
			LengthSeconds = thisLengthSeconds;
			Tempo = newTempo;
			// Default to 4/4
			Meter = (newMeter == 0 ? 4 : newMeter);

			const s32 numBeats = rage::round(ComputeNumBeats(LengthSeconds, Tempo));
			LengthBars = numBeats / Meter;
			audAssertf(numBeats == static_cast<s32>(LengthBars*Meter), "LengthBars: %u, numBeats: %u meter: %u", LengthBars, numBeats, Meter);
		}

		float LengthSeconds;
		float Tempo;
		u32 Meter;
		u32 LengthBars;
	};
	atFixedArray<TempoChange, MaxTempoChanges> m_TempoMapEntries;
};

} // namespace rage

#endif // AUD_TEMPOMAP_H
