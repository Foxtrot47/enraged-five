// 
// audioengine/categorycontroller.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "categorycontroller.h"

#include "categorycontrollermgr.h"

#include "audiohardware/driverdefs.h"
#include "audiohardware/driver.h"
#include "audioengine/engine.h"
#include "math/simplemath.h"


#if __BANK
#include "bank/bank.h"
#endif

namespace rage
{

void audCategoryControllerOutput::SetDefaults()
{
	VolumeDb = 0.f;
	DistRolloffScale = 1.f;
	PitchCents = 0;
	LPFCutoff = kVoiceFilterLPFMaxCutoff;
	HPFCutoff = kVoiceFilterHPFMinCutoff;
}

audCategoryController* audCategoryController::sm_ControllerPool = NULL;
audCategoryControllerSettings* audCategoryController::sm_ControllerSettingsPool = NULL;

#if __BANK
bool audCategoryController::sm_DisableSceneControllers = false;
bool audCategoryController::sm_DisableDynamicControllers = false;
#endif

audCategoryController::audCategoryController(u16 uPoolIndex) :
m_fGainLinearDest(1.f),
m_fGainDuration(0.f),
m_fDistRolloffDest(1.f),
m_fDistRolloffDuration(0.f),
m_fFreqDest(1.f),
m_fFreqDuration(0.f),
m_fLPFOctDest(0.f),
m_fLPFDuration(0.f),
m_fHPFOctDest(g_OctavesBelowFilterMaxAt1Hz),
m_fHPFDuration(0.f),
m_uPoolIndex(uPoolIndex),
m_iPreviousControllerIndex(-1),
m_iNextControllerIndex(-1),
m_iCategoryIndex(-1),
m_iPauseGroup(-1)
{
#if __BANK
	m_fCatVolume = 0.f;
	m_fCatDistRolloff = 1.f;
	m_iCatPitch = 0;
	m_uCatLPF = (u16)kVoiceFilterLPFMaxCutoff;
	m_uCatHPF = (u16)kVoiceFilterHPFMinCutoff;
	m_TimerId = 0;
	m_bMute = false;
	m_bSolo = false;
	m_IsSceneController = false;
	m_IsDynamic = false;

	m_pWidgetGroup = NULL;
	m_fBankGainDB = 0.f;
	m_fBankPitchCents = 0.f;
	m_fBankLPFCutoffHz = kVoiceFilterLPFMaxCutoff;
	m_fBankHPFCutoffHz = kVoiceFilterHPFMinCutoff;
#endif
}

//	Destructor
audCategoryController::~audCategoryController()
{
#if __BANK && !__SPU
	if (m_pWidgetGroup)
		m_pWidgetGroup->Destroy();
#endif
}

void audCategoryController::ComputeOutput(audCategoryControllerOutput& results, const s32 iHeadIndex, const f32 fTimeStep)
{
	f32 fGainLin = 1.f;
	f32 fDistRolloffScale = 1.f;
	f32 fPitchRatio = 1.f;
	f32 fLPFCutoffOcts = 0.f;
	f32 fHPFCutoffOcts = g_OctavesBelowFilterMaxAt1Hz;
#if __BANK
	bool bMute = false;
#endif
	
	s32 iCurIndex = iHeadIndex;
	while (iCurIndex != -1)
	{
		audCategoryController& rController = sm_ControllerPool[iCurIndex];
		rController.Update(fTimeStep);

#if __BANK
		if((sm_DisableSceneControllers && rController.m_IsSceneController && !rController.m_IsDynamic) || (sm_DisableDynamicControllers && rController.m_IsDynamic))
		{
			iCurIndex = rController.GetNextIndex();
			continue;
		}
#endif

		fGainLin *= rController.GetVolumeLinear();
		fDistRolloffScale *= rController.GetDistanceRolloffScale();
		fPitchRatio *= rController.GetFrequencyRatio();
		fLPFCutoffOcts = Min(fLPFCutoffOcts, rController.GetLPFCutoffOctaves());
		fHPFCutoffOcts = Max(fHPFCutoffOcts, rController.GetHPFCutoffOctaves());
#if __BANK
		bMute |= rController.m_bMute;
#if !__SPU
		{
			audCategory* RESTRICT pCat		= rController.GetCategory();		
			rController.m_fCatVolume		= pCat->GetVolume();
			rController.m_fCatDistRolloff	= pCat->GetDistanceRollOffScale();
			rController.m_iCatPitch			= pCat->GetPitch();
			rController.m_uCatLPF			= pCat->GetLPFCutoff();
			rController.m_uCatHPF			= pCat->GetHPFCutoff();
			rController.m_TimerId			= pCat->GetTimerId();
		}
#endif
#endif
		iCurIndex = rController.GetNextIndex();
	}

	results.VolumeDb = audDriverUtil::ComputeDbVolumeFromLinear(fGainLin);
	results.DistRolloffScale = fDistRolloffScale;
	results.PitchCents = (s16)Clamp(audDriverUtil::ConvertRatioToPitch(fPitchRatio), SHRT_MIN, SHRT_MAX);
	const f32 fHPFSelect = fHPFCutoffOcts - (g_OctavesBelowFilterMaxAt1Hz+SMALL_FLOAT);
	const f32 fLPFCutoff = (fLPFCutoffOcts <= g_OctavesBelowFilterMaxAt100Hz ? static_cast<f32>(kVoiceFilterLPFMinCutoff) : Powf(2.f, fLPFCutoffOcts) * kVoiceFilterLPFMaxCutoff);
	results.HPFCutoff = (u16)Selectf(fHPFSelect, Powf(2.f, fHPFCutoffOcts) * kVoiceFilterHPFMaxCutoff, 0.f);
	results.LPFCutoff = (u16)(fLPFCutoff<=static_cast<f32>(kVoiceFilterLPFMinCutoff) ? 0.f : fLPFCutoff);
#if __BANK
	if (bMute)
		results.VolumeDb = -100.f;
#endif
}

const audCategoryControllerSettings* audCategoryController::GetSettings() const
{
	TrapGE((u32)m_uPoolIndex, g_MaxCategoryControllers);
	TrapLT(m_uPoolIndex, 0);
	return &sm_ControllerSettingsPool[m_uPoolIndex];
}
audCategoryControllerSettings* audCategoryController::GetSettings()
{
	TrapGE((u32)m_uPoolIndex, g_MaxCategoryControllers);
	TrapLT(m_uPoolIndex, 0);
	return &sm_ControllerSettingsPool[m_uPoolIndex];
}

audCategoryController *audCategoryController::GetNext() const
{
	return m_iNextControllerIndex == -1 ? NULL : AUDCATEGORYCONTROLLERMANAGER.GetControllerPtrFromId(m_iNextControllerIndex);
}

void audCategoryController::SetNext(const audCategoryController* pNext)
{
	Assign(m_iNextControllerIndex, pNext ? (s32)pNext->GetPoolIndex() : -1);
}

audCategoryController *audCategoryController::GetPrev() const
{
	return m_iPreviousControllerIndex == -1 ? NULL : AUDCATEGORYCONTROLLERMANAGER.GetControllerPtrFromId(m_iPreviousControllerIndex);
}

void audCategoryController::SetPrev(const audCategoryController *pPrev)
{
	Assign(m_iPreviousControllerIndex, pPrev ? (s32)pPrev->GetPoolIndex() : -1);
}

const audCategory *audCategoryController::GetCategory() const
{
	if(m_iCategoryIndex == -1)
	{
		return NULL;
	}
	return g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(m_iCategoryIndex);
}

audCategory *audCategoryController::GetCategory()
{
	if(m_iCategoryIndex == -1)
	{
		return NULL;
	}
	return g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(m_iCategoryIndex);
}

void audCategoryController::BeginGainFadeLinear(f32 fGainLinearDest, f32 fDuration)
{
	audAssertf(fGainLinearDest <= 1.f, "Invalid gain: %f (duration %f)", fGainLinearDest, fDuration);
	if (fDuration <= 0.f)
	{
		SetVolumeLinear(fGainLinearDest);
		return;
	}
	
	//	Nothing longer than 60 seconds
	fDuration = Min(fDuration, 60.f);


	//const f32 fPrevLinearDest = m_fGainLinearDest;
	fGainLinearDest = Selectf(fGainLinearDest - g_SilenceVolumeLin, fGainLinearDest, 0.f);

	m_fGainLinearDest = Min(1.f, fGainLinearDest);
	m_fGainDuration = fDuration;

}

void audCategoryController::BeginDistRolloffScaleApproach(f32 fScale, f32 fDuration)
{
	if (fDuration <= 0.f)
	{
		SetDistanceRolloffScale(fScale);
		return;
	}

	//	Nothing longer than 60 seconds
	fDuration = Min(fDuration, 60.f);

	m_fDistRolloffDest = fScale;
	m_fDistRolloffDuration = fDuration;
}

void audCategoryController::BeginPitchShiftLinear(f32 fFreqDestination, f32 fDuration)
{
	if (fDuration <= 0.f)
	{
		SetFrequencyRatio(fFreqDestination);
		return;
	}

	//	Nothing longer than 60 seconds
	fDuration = Min(fDuration, 60.f);

	m_fFreqDest = fFreqDestination;
	m_fFreqDuration = fDuration;
}

void audCategoryController::BeginLPFSweepOctaves(f32 destinationOctaves, f32 fDuration)
{
	if (fDuration <= 0.f)
	{
		SetLPFCutoffOctaves(destinationOctaves);
		return;
	}

	//	Nothing longer than 60 seconds
	fDuration = Min(fDuration, 60.f);

	m_fLPFOctDest = destinationOctaves;
	m_fLPFDuration = fDuration;
}

/*************************************************************************************
Function:	BeginHPFSweep
*************************************************************************************/
void audCategoryController::BeginHPFSweepOctaves(f32 destinationOctaves, f32 fDuration)
{
	if (fDuration <= 0.f)
	{
		SetHPFCutoffOctaves(destinationOctaves);
		return;
	}

	//	Nothing longer than 60 seconds
	fDuration = Min(fDuration, 60.f);

	m_fHPFOctDest = destinationOctaves;
	m_fHPFDuration = fDuration;
}

void audCategoryController::Update(f32 timeStep)
{
	if(m_iPauseGroup != -1 && audDriver::GetMixer()->IsPaused(m_iPauseGroup))
	{
		timeStep = 0.0f;
	}

	audCategoryControllerSettings* RESTRICT pSettings = GetSettings();
	UpdateSetting(pSettings->m_fGainLinear, m_fGainLinearDest, m_fGainDuration, 1.f, timeStep ASSERT_ONLY(, audCategorySettingType::CategorySettingGainLinear));
	UpdateSetting(pSettings->m_fDistRolloff, m_fDistRolloffDest, m_fDistRolloffDuration, 1.f, timeStep ASSERT_ONLY(, audCategorySettingType::CategorySettingDistRolloff));
	UpdateSetting(pSettings->m_fFreqRatio, m_fFreqDest, m_fFreqDuration, 1.f, timeStep ASSERT_ONLY(, audCategorySettingType::CategorySettingFrequencyRatio));
	UpdateSetting(pSettings->m_fLPFCutoffOct, m_fLPFOctDest, m_fLPFDuration, -g_OctavesBelowFilterMaxAt100Hz, timeStep ASSERT_ONLY(, audCategorySettingType::CategorySettingLPFCuttOff));
	UpdateSetting(pSettings->m_fHPFCutoffOct, m_fHPFOctDest, m_fHPFDuration, -g_OctavesBelowFilterMaxAt1Hz, timeStep ASSERT_ONLY(, audCategorySettingType::CategorySettingHPFCuttOff));
}

#if __ASSERT
const char* GetCategorySettingTypeName(audCategorySettingType settingType)
{
	switch(settingType)
	{
		case audCategorySettingType::CategorySettingGainLinear: return "CategorySettingGainLinear";
		case audCategorySettingType::CategorySettingDistRolloff: return "CategorySettingDistRolloff";
		case audCategorySettingType::CategorySettingFrequencyRatio: return "CategorySettingFrequencyRatio";
		case audCategorySettingType::CategorySettingLPFCuttOff: return "CategorySettingLPFCuttOff";
		case audCategorySettingType::CategorySettingHPFCuttOff: return "CategorySettingHPFCuttOff";
		default: return NULL;
	}
}
#endif

void audCategoryController::UpdateSetting(f32& fSetting, const f32 fDestination, const f32 fDuration, const f32 fBaseScaleValue, const f32 timeStep ASSERT_ONLY(, audCategorySettingType settingType)) const
{
	const f32 fDiff = fDestination - fSetting;
	if (fDiff != 0.f)
	{
		if (fDuration != 0.f)
		{
			Assertf(fabs(fDiff) <= 1.f,"Fade on category controller only can manage linear values from 0 to 1: fDiff %f, fDestination %f, fSetting %f, category %s, setting type %s", fDiff, fDestination, fSetting, GetCategory()? GetCategory()->GetNameString() : "NULL", GetCategorySettingTypeName(settingType));
			const f32 fRate = (timeStep * fBaseScaleValue) / fDuration;

			fSetting = Selectf(fDiff,
				Min(fSetting + fRate, fDestination),
				Max(fSetting - fRate, fDestination) );
			/*if (fDiff > 0.f)
				fSetting = Min(fSetting + fRate, fDestination);
			else
				fSetting = Max(fSetting - fRate, fDestination);*/
		}
		else
			fSetting = fDestination;
	}
}
#if __BANK && !__SPU
/*************************************************************************************
Function:	AddWidgets
*************************************************************************************/
void audCategoryController::AddWidgets(rage::bkBank& bank, bool bPopGroup)
{
	audCategoryControllerSettings* RESTRICT pSettings = GetSettings();

	m_pWidgetGroup = bank.PushGroup(GetCategory()->GetNameString());
	{
		bank.AddToggle("  Mute  ", &m_bMute);
		//bank.AddToggle("Solo  ", &m_bSolo, datCallback(CFA1(audCategoryController::Solo), this));
		bank.PushGroup(" Category Status", false);
		{
			bank.AddText("Volume", &m_fCatVolume, true);
			bank.AddText("DistanceRolloffScale", &m_fCatDistRolloff, true);
			bank.AddText("Pitch", &m_iCatPitch, true);
			bank.AddText("LPF", (int*)&m_uCatLPF, true);
			bank.AddText("HPF", (int*)&m_uCatHPF, true);
			bank.AddText("TimerId", (int*)&m_TimerId, true);
		}bank.PopGroup();
		bank.PushGroup(" Controller Status", false);
		{
			bank.AddText("m_fGainLinear", &pSettings->m_fGainLinear, true);
			bank.AddText("m_fGainLinearDest", &m_fGainLinearDest, true);
			bank.AddText("m_fGainDuration", &m_fGainDuration, true);

			bank.AddText("m_fDistRolloff", &pSettings->m_fDistRolloff, true);
			bank.AddText("m_fDistRolloffDest", &m_fDistRolloffDest, true);
			bank.AddText("m_fDistRolloffDuration", &m_fDistRolloffDuration, true);

			bank.AddText("m_fFreqRatio", &pSettings->m_fFreqRatio, true);
			bank.AddText("m_fFreqDestination", &m_fFreqDest, true);
			bank.AddText("m_fFreqDuration", &m_fFreqDuration, true);

			bank.AddText("m_fLPFCutoffOct", &pSettings->m_fLPFCutoffOct, true);
			bank.AddText("m_fLPFOctDest", &m_fLPFOctDest, true);
			bank.AddText("m_fLPFDuration", &m_fLPFDuration, true);

			bank.AddText("m_fHPFCutoffOct", &pSettings->m_fHPFCutoffOct, true);
			bank.AddText("m_fHPFOctDest", &m_fHPFOctDest, true);
			bank.AddText("m_fHPFDuration", &m_fHPFDuration, true);

			bank.AddSlider("m_iPreviousControllerIndex", &m_iPreviousControllerIndex, 0, SHRT_MAX, 0);
			bank.AddSlider("m_iNextControllerIndex", &m_iNextControllerIndex, 0, SHRT_MAX, 0);
			bank.AddSlider("m_iCategoryIndex", &m_iCategoryIndex, 0, SHRT_MAX, 0);
		}bank.PopGroup();
		bank.PushGroup(" Overrides", false);
		{
			bank.AddSlider("Volume", &m_fBankGainDB, -100.f, 24.0f, 1.f, datCallback(MFA(audCategoryController::WidgetUpdateLinearFromDb), this));
			bank.AddSlider("Pitch ", &m_fBankPitchCents, (f32)SHRT_MIN, (f32)SHRT_MAX, 1.f, datCallback(MFA(audCategoryController::WidgetUpdateFrequencyFromCents), this));
			bank.AddSlider("LPF   ", &m_fBankLPFCutoffHz, static_cast<f32>(kVoiceFilterLPFMinCutoff), static_cast<f32>(kVoiceFilterLPFMaxCutoff), 1.f, datCallback(MFA(audCategoryController::WidgetUpdateOctavesFromHz), this));
			bank.AddSlider("HPF   ", &m_fBankHPFCutoffHz, static_cast<f32>(kVoiceFilterHPFMinCutoff), static_cast<f32>(kVoiceFilterLPFMaxCutoff), 1.f, datCallback(MFA(audCategoryController::WidgetUpdateOctavesFromHz), this));
		}bank.PopGroup();
	}if (bPopGroup) bank.PopGroup();
}

void audCategoryController::Print()
{
#if __BANK
#define BOOL2STR(x) x ? "True" : "False"

	audCategoryControllerSettings* RESTRICT pSettings = GetSettings();
	if(pSettings)
	{
		audErrorf("Controller for category %s, IsScene %s, IsDynamic %s gain %f, lpf %f, hpf %f, freqRatio %f, distRolloff %f", g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(m_iCategoryIndex)->GetNameString(), BOOL2STR(m_IsSceneController), BOOL2STR(m_IsDynamic), audDriverUtil::ComputeDbVolumeFromLinear(pSettings->m_fGainLinear), pSettings->m_fLPFCutoffOct, pSettings->m_fHPFCutoffOct, pSettings->m_fFreqRatio, pSettings->m_fDistRolloff);
	}
	else
	{
		audErrorf("No settings for controller with category %s", g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(m_iCategoryIndex)->GetNameString());
	}
#endif
}

void audCategoryController::SetIsSceneController( bool isSceneController )
{
	m_IsSceneController = isSceneController;
}

void audCategoryController::SetIsDynamic( bool isDynamic )
{
	m_IsDynamic = isDynamic;
}

//void audCategoryController::Solo(audCategoryController* pController)
//{
//	if (pController->m_bSolo)
//	{
//		bool bUnmute = false;
//		AUDCATEGORYCONTROLLERMANAGER.MuteAllExcept(g_AudioEngine.GetCategoryManager().GetCategoryPtr(ATSTRINGHASH("BASE", 0x44E21C90)), pController->GetCategory(), bUnmute);
//	}
//	else
//		AUDCATEGORYCONTROLLERMANAGER.UnmuteAll();
//}
#endif

} // namespace rage

