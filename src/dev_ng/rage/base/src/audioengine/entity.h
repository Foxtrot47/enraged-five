//
// audioengine/entity.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_ENTITY_H
#define AUD_ENTITY_H

#include "enginedefs.h"
#include "metadataref.h"
#include "widgets.h"
#include "atl/array.h"
#include "audiosoundtypes/soundcontrol.h"
#include "data/base.h"
#include "string/stringhash.h"
#include "system/interlocked.h"

namespace rage 
{

class audController;
class audSound;
class audRequestedSettings;
class audWaveSlot;
class audTracker;
class audEffect;
class audEntity;
struct audSoundInitParams;
class audEnvironmentGroupInterface;

#if __BANK 
#define AUDIO_ENTITY_NAME(x) virtual const char *GetEntityName() const { return #x; }
#else
#define AUDIO_ENTITY_NAME(x)
#endif


// PURPOSE
//  An audEntity is the only thing that can create sounds. Held within game entities, these contain the game-specific
//  audio code, and create and manipulate sounds. The set of sound creation functions that should be used from game 
//  audio code is clearly commented below.
class audEntity : public datBase // Inheriting from datBase in the rage level class is better in RDR to avoid multiple inheritance down the line
{
public:
	audEntity();
	virtual ~audEntity();
#if __BANK
	//PURPOSE
	//Pure virtual function for getting name of entity.
	virtual const char *GetEntityName() const = 0;
#endif

	// PURPOSE
	// Initialize/Shutdown static members of the class
	static void InitClass();
	static void ShutdownClass();

	// PURPOSE
	//  Initialize an instance of this class
	virtual void Init();

	// PURPOSE
	//	Shuts down this entity, removing it from the controllers list (and therefore killing all non-orphanable sounds)
	virtual void Shutdown();

	// PURPOSE
	//	Stops all non-orphanable sounds
	virtual void StopAllSounds(bool allowRelease = true);

	// PURPOSE
	// Called in main game-thread update loop, too allow the audEntity to update its occlusion group(s).
//	virtual void UpdateOcclusionGroups(u32 UNUSED_PARAM(timeInMs)){};
	// PURPOSE
	// Called in main game-thread update loop, to service audEntity before audSounds are updated
	virtual void PreUpdateService(u32 timeInMs);

	// PURPOSE
	// Called after the audio and game audio updates on the main thread (e.g. for redgref handling)
	virtual void PostUpdate();

	// PURPOSE
	// Called by an audSound that has an audEntity and is set to UpdateEntity
	virtual void UpdateSound(audSound* sound, audRequestedSettings *reqSets, u32 timeInMs);

	//**************************************
	//  The functions below are those that should be called by game audio code for creating, playing, and finding sounds.

	// PURPOSE
	//  Instantiates a sound, and stores a ptr to it in a LOCAL variable, soundRef.
	//  Be sure to only use this if you don't need to keep a permanent reference to the sound you are creating.
	//  If you do need a persistent reference to a sound, use CreateSound_PersistentReference() instead.
	void CreateSound_LocalReference(const char *soundName, audSound **soundRef, const audSoundInitParams *initParams = NULL);
	void CreateSound_LocalReference(const u32 soundHash, audSound **soundRef, const audSoundInitParams *initParams = NULL);
	void CreateSound_LocalReference(const audMetadataRef soundMetadataRef, audSound **soundRef, const audSoundInitParams *initParams = NULL);

	//  Instantiates a sound, and stores a ptr to it in a MEMBER variable, soundRef.
	//  Be sure to only use this if you need to keep a permanent reference to the sound you are creating.
	//  If you don't need a persistent reference to a sound, use CreateSound_LocalReference() instead.
	void CreateSound_PersistentReference(const char *soundName, audSound **soundRef, const audSoundInitParams *initParams = NULL);
	void CreateSound_PersistentReference(const u32 soundHash, audSound **soundRef, const audSoundInitParams *initParams = NULL);
	void CreateSound_PersistentReference(const audMetadataRef soundMetadataRef, audSound **soundRef, const audSoundInitParams *initParams = NULL);


	// PURPOSE
	//  Create, prepare and play a sound, without ever grabbing a reference to it.
	// RETURNS
	//	True if the sound was played, false otherwise (ie sound doesn't exist)
	bool CreateAndPlaySound(const char *soundName, const audSoundInitParams *initParams = NULL);
	bool CreateAndPlaySound(const u32 soundHash, const audSoundInitParams *initParams = NULL);
	bool CreateAndPlaySound(const audMetadataRef metadataRef, const audSoundInitParams *initParams = NULL);

	// PURPOSE
	//  Create, prepare and play a sound, storing a persistent reference to it.
	void CreateAndPlaySound_Persistent(const char *soundName, audSound **soundRef, const audSoundInitParams *initParams = NULL);
	void CreateAndPlaySound_Persistent(const u32 soundHash, audSound **soundRef, const audSoundInitParams *initParams = NULL);
	void CreateAndPlaySound_Persistent(const audMetadataRef metadataRef, audSound **soundRef, const audSoundInitParams *initParams = NULL);

	// PURPOSE
	// If passed in a non-null audSound*, calls StopAndForget on it. Overloaded to allow you to stop
	// a bunch of them at once.
	// Means you don't need to check each one's non-null first, for us lazy folk.
	inline void StopAndForgetSounds(audSound* sound1=NULL, audSound* sound2=NULL, audSound* sound3=NULL, audSound* sound4=NULL, 
									audSound* sound5=NULL, audSound* sound6=NULL, audSound* sound7=NULL, audSound* sound8=NULL, 
									audSound* sound9=NULL, audSound* sound10=NULL, audSound* sound11=NULL, audSound* sound12=NULL)
	{
		StopAndForgetSoundsPrivate(sound1, sound2, sound3, sound4, sound5, sound6, sound7, sound8, sound9, sound10, sound11, sound12, false);
	}

	inline void StopAndForgetSoundsContinueUpdating(audSound* sound1=NULL, audSound* sound2=NULL, audSound* sound3=NULL, audSound* sound4=NULL, 
													audSound* sound5=NULL, audSound* sound6=NULL, audSound* sound7=NULL, audSound* sound8=NULL, 
													audSound* sound9=NULL, audSound* sound10=NULL, audSound* sound11=NULL, audSound* sound12=NULL)
	{
		StopAndForgetSoundsPrivate(sound1, sound2, sound3, sound4, sound5, sound6, sound7, sound8, sound9, sound10, sound11, sound12, true);
	}

	//**************************************

	// PURPOSE
	//	Useful for debugging
	void SetName(const char *DEV_ONLY(name))
	{
#if __DEV
		m_Name = name;
#endif
	}

	// PURPOSE
	//	Useful for debugging
	const char *GetName() const
	{
#if __DEV
		return m_Name;
#else
		return NULL;
#endif
	}

#if __DEV
	bool IsADebugEntity()
	{
		return m_IsADebugEntity;
	}

	void SetAsDebugEntity(bool isADebugEntity)
	{
		m_IsADebugEntity = isADebugEntity;
	}
#endif

	u32 GetControllerId() const
	{
		return m_ControllerEntityId;
	}

	bool HasRegisteredWithController() const
	{
		return m_ControllerEntityId != 0xFFFF;
	}

	audVariableBlock * GetVariableBlock()
	{
		if(m_EntityVariableBlock >= 0)
		{
			return &sm_EntityVariableBlocks[m_EntityVariableBlock];
		}
		//Assertf(m_EntityVariableBlock>=0, "Trying to get a variable block but entity doesn't have one allocated");
		return NULL;
	}

	void SetEntityVariable(u32 nameHash, f32 value);
	f32 GetEntityVariableValue(u32 nameHash);


	static audVariableBlock * GetVariableBlock(const int index)
	{
		if(index >= 0)
		{
			return &sm_EntityVariableBlocks[index];
		}
		return NULL;
	}

	int GetVariableBlockIndex() const
	{
		return m_EntityVariableBlock;
	}

	const audVariableBlock *GetVariableBlock() const 
	{ 
		if(HasEntityVariableBlock())
		{
			return GetVariableBlock(GetVariableBlockIndex()); 
		}
		return NULL;
	}

	bool HasEntityVariableBlock() const
	{
		return m_EntityVariableBlock != -1; 
	} 

	static void AddVariableBlockRef(int index)
	{
		if(index >= 0)
		{
			if(Verifyf(index < (int)g_MaxAudEntityVariableBlocks, "Index: %d", index))
			{
				sysInterlockedIncrement(&sm_EntityVariableBlockRefs[index]);
			}
		}
	}

	static void RemoveVariableBlockRef(int index);

	// PURPOSE
	//	If this function returns true then PreupdateService() will be called when the game is paused
	virtual bool IsUnpausable() const
	{
		return false;
	}

	// PURPOSE
	//	Can be overridden by an entity to support audDynamicEntitySound queries
	virtual u32 QuerySoundNameFromObjectAndField(const u32 * UNUSED_PARAM(objectRefs), u32 UNUSED_PARAM(numRefs)){ return 0; }

	// PURPOSE
	//	Can be overridden by an entity to support dynamic speech sound queries
	virtual void QuerySpeechVoiceAndContextFromField(const u32 UNUSED_PARAM(field), u32& UNUSED_PARAM(voiceNameHash), u32& UNUSED_PARAM(contextNamePartialHash)) {}

	void ReleaseVariableBlock()
	{
		if(m_EntityVariableBlock >= 0)
		{
			FastAssert(sm_EntityVariableBlockRefs[m_EntityVariableBlock] > 0);
			RemoveVariableBlockRef(m_EntityVariableBlock);
			m_EntityVariableBlock = -1; 
		}
	}

	// PURPOSE
	//	Can be implemented as an alternative to audTracker-based automatic sound positioning
	virtual Vec3V_Out GetPosition() const { return Vec3V(V_ZERO); }
	virtual audCompressedQuat GetOrientation() const { return audCompressedQuat(); }

	//This is used to bring the entity variables onto the spu
	static float * GetEntityVariableValuePoolAddress()
	{
		return (float *)&sm_EntityVariableBlockValues[0];
	}

	//This is used to bring the entity variable refs address onto the spu
	static u32 * GetEntityVariableRefPoolAddress()
	{
		return sm_EntityVariableBlockRefs;
	}

	static void InitializeVariableBlocks();

	static void ProcessBatchedSoundRequests(const s32 timeStepMs);
	static void ReportTrackerDeletion(const audTracker *tracker);
	static void ResetDeletedObjectLists();

#if __BANK
	virtual void DrawBoundingBox(const Color32 ) {};
#endif


private:

	bool AddBatchedSoundRequest(audMetadataRef soundMetadataRef, const audSoundInitParams *initParams);
	void AddToDeletedList();
	void StopAndForgetSoundsPrivate(audSound* sound1=NULL, audSound* sound2=NULL, audSound* sound3=NULL, audSound* sound4=NULL, 
									audSound* sound5=NULL, audSound* sound6=NULL, audSound* sound7=NULL, audSound* sound8=NULL, 
									audSound* sound9=NULL, audSound* sound10=NULL, audSound* sound11=NULL, audSound* sound12=NULL, bool continueUpdatingEntity=false);
	

protected:
	void SetInitialSoundParams(audSound **soundRef, const audSoundInitParams *initParams);

	virtual void InitializeEntityVariables()
	{
		m_EntityVariableBlock = -1;
	}

	bool AllocateEntityVariableBlock(const char * mapName);
	bool AllocateEntityVariableBlock(u32 mapNameHash);

	enum { kMaxDeletedEntities = 128 };
	typedef atFixedArray<const audEntity *, kMaxDeletedEntities> audDeletedEntityList;
	static audDeletedEntityList sm_DeletedEntities;

	enum { kMaxDeletedTrackers = 128 };
	typedef atFixedArray<const audTracker *, kMaxDeletedEntities> audDeletedTrackerList;
	static audDeletedTrackerList sm_DeletedTrackers;

#if __DEV
	const char *m_Name;	
	bool m_IsADebugEntity;
#endif

private:
	u16 m_ControllerEntityId;
	s16 m_EntityVariableBlock;

	struct audBatchedSoundRequest
	{
		audSoundInitParams initParams;
		audEntity *entity;
		audMetadataRef soundMetadataRef;
	};
	enum { kMaxBatchedSoundRequests = 256 };
	typedef atFixedArray<audBatchedSoundRequest, kMaxBatchedSoundRequests> audBatchedSoundRequestList;
	static audBatchedSoundRequestList sm_BatchedSoundRequests;
	BANK_ONLY(static bool sm_PrintedFullBatchList);

public: //so we can declare it in the sound update job;
	static u32* sm_EntityVariableBlockRefs;
	
	//PURPOSE
	//Pool of audentity variable 
	static atRangeArray<audVariableBlock, g_MaxAudEntityVariableBlocks> sm_EntityVariableBlocks;
	ALIGNAS(16) static atRangeArray<float, g_MaxAudEntityVariableBlocks*audVariableBlock::kNumVariablesPerBlock> sm_EntityVariableBlockValues ;
private:

};

#if !__SPU
const u32 g_NullSoundHash = ATSTRINGHASH("NULL_SOUND", 3817852694U);
extern audMetadataRef g_NullSoundRef;
#endif
 
} // namespace rage


#endif // AUD_ENTITY_H
