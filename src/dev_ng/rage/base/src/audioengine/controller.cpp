//
// audioengine/controller.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "categorymanager.h"
#include "controller.h"
#include "audiosoundtypes/sound.h"
#include "engine.h"
#include "entity.h"
#include "soundfactory.h"
#include "audiohardware/channel.h"
#include "audiohardware/waveslot.h"

#include "atl/pool.h"
#include "bank/bank.h"
#include "profile/profiler.h"
#include "system/ipc.h"
#include "system/param.h"
#include "system/performancetimer.h"

namespace rage {

#define AUD_CHECK_THREADED_ENTITY_ALLOCATION (__ASSERT && 0)

CompileTimeAssert(AUD_INVALID_CONTROLLER_ENTITY_ID == 0xFFFF);

PARAM(audioocclusion, "[RAGE Audio] Enable audio occlusion.");
PARAM(noaudioocclusion, "[RAGE Audio] Disable audio occlusion.");

audController *g_Controller;

bool audController::sm_ShouldBeActive;
u32	audController::sm_NumActiveControllers;

bool audController::sm_ShouldUpdateEnvironmentGroups = false;
bool audController::sm_ShouldUseEnvironmentMetric = true;

u32 g_MaxEnvironmentGroupsToUpdate = 10;

#define AUDCONTROLLER_USER_PROFILER 1

#if AUDCONTROLLER_USER_PROFILER
#define AUDCONTROLLER_PF_FUNC(x) PF_FUNC(x)
#define AUDCONTROLLER_PF_SET(x,y) PF_SET(x,y)

PF_PAGE(AudioControllerPage, "audController");
PF_GROUP(AudioControllerTiming);
PF_LINK(AudioControllerPage, AudioControllerTiming);

PF_TIMER(ControllerUpdate, AudioControllerTiming);
PF_TIMER(EnvironmentGroupUpdate, AudioControllerTiming);
PF_TIMER(PreUpdateService, AudioControllerTiming);
PF_TIMER(ControllerOrphaning, AudioControllerTiming);
PF_TIMER(ControllerUpdateSounds, AudioControllerTiming);
PF_TIMER(CommitRequestedSettings, AudioControllerTiming);
PF_TIMER(ReportTrackerDeletion, AudioControllerTiming);

PF_PAGE(AudioEntitiesPage, "RAGE Audio Entities");
PF_GROUP(AudioEntities);
PF_LINK(AudioEntitiesPage, AudioEntities);

PF_VALUE_INT(ActiveEntities, AudioEntities);
PF_VALUE_INT(ActiveOcclGroups, AudioEntities);

PF_VALUE_INT(ParentSoundsFromDebugEntity, AudioEntities);
PF_VALUE_INT(EntityVariableBlocksAllocated, AudioEntities);
#else
#define AUDCONTROLLER_PF_FUNC(x)
#define AUDCONTROLLER_PF_SET(x,y)
#endif // AUDCONTROLLER_USER_PROFILER

#if __DEV
u32 g_MaxActiveEnvironmentGroups = 0;
u32 g_MaxActiveAudioEntities = 0;
u32 g_ParentSoundsFromDebugEntity = 0;
#endif

bool audController::InitClass()
{
	sm_ShouldBeActive = false;
	sm_NumActiveControllers = 0;
	g_Controller = NULL;

	return true;
}

void audController::ShutdownClass()
{
}

audController* audController::FindControllerForThread()
{
	return g_Controller;
}

void audController::RegisterControllerForThisThread(audController* controller)
{
	Assert(g_Controller == NULL);
	g_Controller = controller;
}

void audController::UnregisterControllerForThisThread()
{
	g_Controller = NULL;
}

void audController::FlagControllersActive(bool active)
{
	sm_ShouldBeActive = active;
}

bool audController::AreControllersFlaggedActive()
{
	return sm_ShouldBeActive;
}

void audController::IncrementNumActiveControllers()
{
	sysInterlockedIncrement(&sm_NumActiveControllers);
}

void audController::DecrementNumActiveControllers()
{
	sysInterlockedDecrement(&sm_NumActiveControllers);
}

u32 audController::GetNumberOfActiveControllers()
{
	return sysInterlockedRead(&sm_NumActiveControllers);
}

void audController::UpdateEnvironmentGroups(bool update)
{
	if(!g_AudioEngine.IsAudioEnabled() || g_AudioEngine.ShouldUseCheapAudioEffects() || PARAM_noaudioocclusion.Get())
	{
		sm_ShouldUpdateEnvironmentGroups = false;
	}
	else if(PARAM_audioocclusion.Get())
	{
		sm_ShouldUpdateEnvironmentGroups = true;
	}
	else
	{
		sm_ShouldUpdateEnvironmentGroups = update;
	}
	if (sm_ShouldUpdateEnvironmentGroups)
	{
		Printf("Audio Occlusion Enabled\n");
	}
	else
	{
		Printf("Audio Occlusion Disabled\n");
	}
}

// end of static functions
///////////////////////////////////////////////////////////////////////

audController::audController()
{
	m_IsInitialized = false;
	m_IsActive = false;
	m_IsLocked = false;
	m_EnvironmentGroupManager = NULL;
	ASSERT_ONLY(m_ThreadVerifier = NULL);
	
}

audController::~audController()
{
	
}

bool audController::Init()
{
	if (m_IsInitialized)
	{
		Assert(0);
		return true; // Don't risk doing anything if we're already initialized.
	}

	m_EntityListHead = 0xffff;
	m_NumUsedEntities = 0;
	m_EnvironmentListHead = 0xffff;
	m_NumUsedEnvironment = 0;
	
	//	Sequence the free indices
	for (u32 i = 0; i < MAX_AUDIO_ENTITIES; i++)
		m_FreeEntityIndexStack[i] = (u16)i;
	for (u32 i = 0; i < MAX_ENVIRONMENT_GROUPS; i++)
		m_FreeEnvironmentIndexStack[i] = (u16)i;

	m_FirstFreeSound = 0;
	
	for(u32 i = 0; i < MAX_CONTROLLER_SOUNDS - 1; i++)
	{
		Assign(m_SoundListStore[i].nextSoundRef, i + 1);
	}
	m_SoundListStore[MAX_CONTROLLER_SOUNDS-1].nextSoundRef = 0xffff;

	for (u32 i=0; i<MAX_ENVIRONMENT_GROUPS; i++)
	{
		m_EnvironmentGroupList[i] = NULL;
	}

	m_IsInitialized = true;

	audController::RegisterControllerForThisThread(this);

	return true;
}

void audController::Shutdown()
{
	if (!m_IsInitialized)
	{
		Assert(0);
		return; // Don't risk closing stuff down if we don't think we're initialized - could shut down invalid stuff.
	}

	SetActive(false);

	m_IsInitialized = false;
}

void audController::ClearAllSounds()
{
	Assert(IsActive()); // doesn't really matter, but it'd be weird to do this if we were already inactive

	// We want to go through all our entities, clear their sounds and remove refs, and cancel them - null everything we can, and remove audEntity sound refs
	u16 entityId = m_EntityListHead;
	while (entityId != 0xffff)
	{
		audControllerEntityRef& entRef = m_EntityList[entityId];
		Assert(entRef.entity);
		Assert(entRef.entity->GetControllerId() == entityId);
		Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
		Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
		ClearAllSounds(entRef);
		entityId = entRef.nextEntity;
	}

	//	Now clear the orphaned sounds
	ClearAllSounds(m_OrphanEntityRef);
}

void audController::ClearAllSounds(audControllerEntityRef& entityRef)
{
	u32 nextSoundId = entityRef.firstSoundRef;
	u32 prevSoundId = 0xffff;
	u32 firstPersistentSoundID = 0xffff;
	u32 prevPersistentSoundID = 0xffff;

	while(nextSoundId != 0xffff)
	{
		const u32 soundId = nextSoundId;
		nextSoundId = m_SoundListStore[soundId].nextSoundRef;
		audSound *sound = (audSound*)audSound::GetStaticPool().GetSoundSlot(m_SoundListStore[soundId].bucketId, m_SoundListStore[soundId].slotId);
		Assert(sound);

		audRequestedSettings *reqSets = sound->GetRequestedSettings();

		if(reqSets->ShouldPersistOverClears())
		{
			// Remember the first persistent sound we come across, as it'll 
			// become the new first item in the list
			if(firstPersistentSoundID == 0xffff)
			{				
				firstPersistentSoundID = soundId;
			}

			// If we have multiple persistent sounds, link 'em together
			if(prevPersistentSoundID != 0xffff)
			{
				Assign(m_SoundListStore[prevPersistentSoundID].nextSoundRef, soundId);
			}
			
			prevPersistentSoundID = soundId;
			continue;
		}

		// Stop it
		sound->Kill();
		
		// Stop caring about it
		reqSets->SetUpdateEntity(false);
		reqSets->InvalidateEntityPtr();
		audSound **entitySoundRef = sound->GetEntitySoundReference();
		if (entitySoundRef)
		{
			*(entitySoundRef) = NULL;
		}
		reqSets->InvalidateEntityRefPtr();
		sound->InvalidateTracker(reqSets);

		//sound->ResetNumChildReferences();

		// important that we never use this ptr after calling SetIsReferenced(false)!
		sound->SetIsReferencedByGame(false);

		if(prevSoundId == 0xffff)
		{
			Assign(entityRef.firstSoundRef, nextSoundId);
		}
		FreeSoundRef(soundId);		
		prevSoundId = soundId;
	}

	// Final persistent sound should link to nothing
	if(prevPersistentSoundID != 0xffff)
	{
		Assign(m_SoundListStore[prevPersistentSoundID].nextSoundRef, 0xffff);
	}

	Assign(entityRef.firstSoundRef, firstPersistentSoundID);
}

void audController::FreeSoundRef(const u32 soundId)
{
	sysCriticalSection lock(m_LockEntities);

	Assert(soundId != 0xffff);
	Assert(soundId != m_FirstFreeSound);
	if(soundId != 0xffff)
	{
		Assign(m_SoundListStore[soundId].nextSoundRef, m_FirstFreeSound);
		Assign(m_FirstFreeSound, soundId);
	}
}

u32 audController::AllocateFreeSoundRef()
{
	sysCriticalSection lock(m_LockEntities);

#if AUD_CHECK_THREADED_ENTITY_ALLOCATION
	const u32 numFreeAtStart = ComputeNumFreeEntries();
	const u32 numAllocatedAtStart = ComputeNumAllocatedEntries();
	Assert(numFreeAtStart + numAllocatedAtStart == MAX_CONTROLLER_SOUNDS);
#endif
	const u32 ret = m_FirstFreeSound;
	if(m_FirstFreeSound != 0xffff)
	{
		m_FirstFreeSound = m_SoundListStore[ret].nextSoundRef;
	}
	else
	{
		audErrorf("Controller sound list full!");
		DebugPrintControllerSoundList();
		Assert(0);
	}
	return ret;
}

#if __DEV
u32 audController::ComputeNumAllocatedEntries() const
{
	u32 numAllocated = 0;
	
	u16 entityId = m_EntityListHead;
	while (entityId != 0xffff)
	{
		const audControllerEntityRef& entRef = m_EntityList[entityId];
		Assert(entRef.entity);
		Assert(entRef.entity->GetControllerId() == entityId);
		Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
		Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
		numAllocated += ComputeNumAllocatedEntries(entRef);
		entityId = entRef.nextEntity;
	}	

	//	Now add the orphaned list
	numAllocated += ComputeNumAllocatedEntries(m_OrphanEntityRef);

	return numAllocated;
}

u32 audController::ComputeNumAllocatedEntries(const audControllerEntityRef& entRef) const
{
	u32 numAllocated = 0;
	u32 nextSound = entRef.firstSoundRef;
	while(nextSound != 0xffff)
	{
		numAllocated++;
		nextSound = m_SoundListStore[nextSound].nextSoundRef;
	}

	return numAllocated;
}

u32 audController::ComputeNumFreeEntries() const
{
	u32 numFree = 0;
	u32 nextSound = m_FirstFreeSound;
	while(nextSound != 0xffff)
	{
		numFree++;
		nextSound = m_SoundListStore[nextSound].nextSoundRef;
	}
	return numFree;
}
#endif

bool audController::IsActive()
{
	return m_IsActive;
}

void audController::SetActive(bool active)
{
	if (!active && m_IsActive)
	{
		// we used to be active, now we're not
		ClearAllSounds();
		DecrementNumActiveControllers();
	}

	if(active && !m_IsActive)
	{
		// we used to be inactive, now we are active
		IncrementNumActiveControllers();
	}

	m_IsActive = active;
}

void audController::PreUpdate(const u32 timeInMs)
{
	AUDCONTROLLER_PF_FUNC(PreUpdateService);

	if (!m_IsInitialized)
	{
		return; // We just don't do anything if we're not initialized.
	}

	// See if we're currently active, what we're flagged to be, and act accordingly
	if (IsActive())
	{
		// We're active
		// See if we're meant to be going inactive - if so, clear everything out right now, and mark ourselves inactive
		if (!AreControllersFlaggedActive())
		{
			// We're flagged to go inactive
			SetActive(false);
			return;
		}
	}
	else // We're inactive
	{
		if (AreControllersFlaggedActive())
		{
			// We're flagged to go active - just start allowing new sounds
			SetActive(true);
		}
		else
		{
			return;
		}
	}

	Assert(!m_IsLocked);
	m_IsLocked = true;

	// Go through each of the audEntities registered with us and do a PreUpdateService()
	u32 numberOfEntities = 0;
	// assume timer 0 is the one we care about for entities...should really allow entities to specify the timer to use for them
	const bool isEnginePaused = g_AudioEngine.GetSoundManager().IsPaused(0);
	
	{
		sysCriticalSection lock(m_LockEntities);
		u16 entityId = m_EntityListHead;
		while (entityId != 0xffff)
		{
			audControllerEntityRef& entRef = m_EntityList[entityId];
			// only update unpausable entities when the audio engine is paused
			if(!isEnginePaused || entRef.entity->IsUnpausable())
			{
				numberOfEntities++;
				Assert(entRef.entity);
				Assert(entRef.entity->GetControllerId() == entityId);
				Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
				Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
				entRef.entity->PreUpdateService(timeInMs);
			}
			entityId = entRef.nextEntity;
		}
	}
#if __DEV
	g_MaxActiveAudioEntities = Max(g_MaxActiveAudioEntities, (u32)numberOfEntities);
	g_ParentSoundsFromDebugEntity = 0;
#endif

	m_IsLocked = false;

	AUDCONTROLLER_PF_SET(ActiveEntities, numberOfEntities);
}

void audController::Update(const u32 timeInMs)
{
	AUDCONTROLLER_PF_FUNC(ControllerUpdate);

	if (!m_IsInitialized || !IsActive())
	{
		return; // We just don't do anything if we're not initialized.
	}

	Assert(!m_IsLocked);
	m_IsLocked = true;

	{
		AUDCONTROLLER_PF_FUNC(EnvironmentGroupUpdate);
		int numEnvGroups = 0;
		
		u16 envGroupId = m_EnvironmentListHead;
		while (envGroupId != 0xffff)
		{
			audEnvironmentGroupInterface*& rpGroup = m_EnvironmentGroupList[envGroupId];
			const u16 nextId = rpGroup->m_NextIndex;
			numEnvGroups++;
			// Check and see if it's in use - either has an entity, or has sounds using it.
			// If not, delete it.
			if (!(rpGroup->GetEntity()) && (rpGroup->GetNumberOfSoundReference() == 0))
			{
				//	We're about to delete a controller, so apply a lock
				sysCriticalSection lock(m_LockEnvironment);

				// Let the environment group manager know about this delete so we don't try and use this group as a shared/linked update
				m_EnvironmentGroupManager->RegisterDeletedEnvironmentGroup(rpGroup);

				--m_NumUsedEnvironment;
				Assert(m_FreeEnvironmentIndexStack[m_NumUsedEnvironment] == 0xffff);
				m_FreeEnvironmentIndexStack[m_NumUsedEnvironment] = envGroupId;
			
				if (envGroupId == m_EnvironmentListHead)
					m_EnvironmentListHead = rpGroup->m_NextIndex;
				if (rpGroup->m_PrevIndex != 0xffff)
					m_EnvironmentGroupList[rpGroup->m_PrevIndex]->m_NextIndex = rpGroup->m_NextIndex;
				if (rpGroup->m_NextIndex != 0xffff)
					m_EnvironmentGroupList[rpGroup->m_NextIndex]->m_PrevIndex = rpGroup->m_PrevIndex;
				delete rpGroup;
				rpGroup = NULL;
			}
			envGroupId = nextId;
		}

	#if __DEV
		g_MaxActiveEnvironmentGroups = Max(g_MaxActiveEnvironmentGroups, (u32)numEnvGroups);
	#endif
		AUDCONTROLLER_PF_SET(ActiveOcclGroups, numEnvGroups);

		if (sm_ShouldUpdateEnvironmentGroups && m_EnvironmentGroupManager)
		{
			m_EnvironmentGroupManager->UpdateEnvironmentGroups(m_EnvironmentGroupList, g_MaxEnvironmentGroupsToUpdate, timeInMs);
		}

	}	

	// Go through each of the audSounds registered with us and do an Update()
	// (this will include telling audEntities of WAITING_TO_BE_DELETED ones
	{
		sysCriticalSection lock(m_LockEntities);

		audRequestedSettings::Indices reqSetIndices;
		audRequestedSettings::ComputeIndices(reqSetIndices);
		
		u32 entityId = m_EntityListHead;
		while (entityId != 0xffff)
		{
			audControllerEntityRef& entRef = m_EntityList[entityId];
			Assert(entRef.entity);
			Assert(entRef.entity->GetControllerId() == entityId);
			UpdateEntityRef(entRef, timeInMs, reqSetIndices);

			Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
			Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
			entityId = entRef.nextEntity;
		}
		
		//	Update the orphaned sounds
		UpdateEntityRef(m_OrphanEntityRef, timeInMs, reqSetIndices);
	}

#if __DEV
	AUDCONTROLLER_PF_SET(ParentSoundsFromDebugEntity, g_ParentSoundsFromDebugEntity);
#endif
#if __BANK
	u32 numEntityVariableBlocksAllocated = 0;
	for(s16 i=0; i< g_MaxAudEntityVariableBlocks; i++)
	{
		if(audEntity::sm_EntityVariableBlockRefs[i])
		{
			numEntityVariableBlocksAllocated++;
		}
	}
	AUDCONTROLLER_PF_SET(EntityVariableBlocksAllocated,numEntityVariableBlocksAllocated);
#endif

	m_IsLocked = false;
}

void audController::PostUpdate()
{
	AUDCONTROLLER_PF_FUNC(PreUpdateService);

	if (!m_IsInitialized)
	{
		return; // We just don't do anything if we're not initialized.
	}

	// See if we're currently active, what we're flagged to be, and act accordingly
	if (!IsActive())
	{
		return;
	}

	Assert(!m_IsLocked);
	m_IsLocked = true;

	// Go through each of the audEntities registered with us and do a PreUpdateService()
	u32 numberOfEntities = 0;
	// assume timer 0 is the one we care about for entities...should really allow entities to specify the timer to use for them

	const bool isEnginePaused = g_AudioEngine.GetSoundManager().IsPaused(0);

	{
		sysCriticalSection lock(m_LockEntities);
		u32 entityId = m_EntityListHead;
		while (entityId != 0xffff)
		{
			audControllerEntityRef& entRef = m_EntityList[entityId];
			// only update unpausable entities when the audio engine is paused
			if(!isEnginePaused || entRef.entity->IsUnpausable())
			{
				numberOfEntities++;

				if(audVerifyf(entRef.entity, "audControllerEntityRef has a NULL entity"))
				{
					Assert(entRef.entity->GetControllerId() == entityId);
					Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
					Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
					entRef.entity->PostUpdate();
				}
			}
			entityId = entRef.nextEntity;
		}
	}

	m_IsLocked = false;
}

void audController::UpdateEntityRef(audControllerEntityRef& entRef, const u32 timeInMs, const audRequestedSettings::Indices &reqSetIndices)
{
	u32 nextSoundId = entRef.firstSoundRef;
	u32 prevSoundId = 0xffff;

	while(nextSoundId != 0xffff)
	{
		bool deletedSound = false;
		const u32 soundId = nextSoundId;
		nextSoundId = m_SoundListStore[soundId].nextSoundRef;
		Assert(soundId != prevSoundId);
		Assert(nextSoundId == 0xffff || nextSoundId != prevSoundId);
		Assert(soundId != nextSoundId);
		audSound *sound = (audSound*)audSound::GetStaticPool().GetSoundSlot(m_SoundListStore[soundId].bucketId, m_SoundListStore[soundId].slotId);
		Assert(sound);

		{
			AUDCONTROLLER_PF_FUNC(ControllerUpdateSounds);
			sound->Update(timeInMs, reqSetIndices);
		}
		
		// Also, if they're marked for deletion, and don't have audEntity ptrs, forget them.
		// If they're being deleted and do have audEntity, entity's had plenty of chances to do something
		// about it, so we null everything possible, and forget anyway.
		// 14/02/06 MdS - we now also check that top level sounds don't have any game-referenced children,
		// because it's not safe to delete if they do. This will mean sounds kicking about for an extra frame
		// or two, which is fine.
		{
			AUDCONTROLLER_PF_FUNC(ControllerOrphaning);

			audRequestedSettings *reqSets = sound->GetRequestedSettings();

			if (sound->GetPlayState() == AUD_SOUND_WAITING_TO_BE_DELETED)
			{				
								
				if (sound->_GetEntity(reqSets) == NULL && sound->_GetEntitySoundReference(reqSets) == NULL)
				{
					sound->InvalidateTracker(reqSets);
					// important that we never use this ptr after calling SetIsReferenced(false)!
					sound->SetIsReferencedByGame(false);
					if(prevSoundId == 0xffff)
					{
						Assign(entRef.firstSoundRef, nextSoundId);
					}
					else
					{
						Assign(m_SoundListStore[prevSoundId].nextSoundRef, nextSoundId);
					}
					FreeSoundRef(soundId);
					deletedSound = true;
				}
				else
				{
					// It's waiting to be deleted, but audEntity's not given it up. Remove reference to it from
					// the audEntity (if it has one), and stop it pointing at the audEntity.
					audSound **entitySoundRef = sound->GetEntitySoundReference();
					if (entitySoundRef)
					{
						*(entitySoundRef) = NULL;
					}

					reqSets->SetUpdateEntity(false);
					reqSets->InvalidateEntityPtr();
					reqSets->InvalidateEntityRefPtr();
					reqSets->InvalidateTrackerPtr();

					// important that we never use this ptr after calling SetIsReferenced(false)!
					sound->SetIsReferencedByGame(false);
					if(prevSoundId == 0xffff)
					{
						Assign(entRef.firstSoundRef, nextSoundId);
					}
					else
					{
						Assign(m_SoundListStore[prevSoundId].nextSoundRef, nextSoundId);
					}
					FreeSoundRef(soundId);
					deletedSound = true;
				}
			}
			// If they're mid playing, but have been orphaned and aren't allowed to be, stop them.
			// Don't do this check for child sounds - parent's orphan control will sort it out.
			else if ((sound->GetPlayState() != AUD_SOUND_WAITING_TO_BE_DELETED) && (!sound->_GetParent()))
			{
				if ((sound->_GetEntity(reqSets) == NULL) &&
					(sound->_GetTracker(reqSets) == NULL) &&
					(!reqSets->IsAllowOrphaned()))
				{
					// Nothing's controlling the sound, and it shouldn't be orphaned, so stop it.
					sound->Stop();
				}
			}
			else if((sound->GetPlayState() == AUD_SOUND_DORMANT) && (sound->_GetParent()))
			{
				// We're a dormant referenced child sound. Check if our top level parent is WTBD, and kill ourselves if so.
				// (You get this if you create a sound, reference it's child, then Stop it without ever playing.)
				Assert(sound->GetTopLevelParent());
				if (sound->GetTopLevelParent()->GetPlayState() == AUD_SOUND_WAITING_TO_BE_DELETED)
				{
					if (sound->_GetEntity(reqSets) == NULL)
					{
						Assert(sound->_GetEntitySoundReference(reqSets) == NULL);
						sound->InvalidateTracker(reqSets);
						// important that we never use this ptr after calling SetIsReferenced(false)!
						sound->SetIsReferencedByGame(false);
						if(prevSoundId == 0xffff)
						{
							Assign(entRef.firstSoundRef, nextSoundId);
						}
						else
						{
							Assign(m_SoundListStore[prevSoundId].nextSoundRef, nextSoundId);
						}
						FreeSoundRef(soundId);
						deletedSound = true;
					}
					else
					{
						// It's waiting to be deleted, but audEntity's not given it up. Remove reference to it from
						// the audEntity (if it has one), and stop it pointing at the audEntity.
						audSound **entitySoundRef = sound->GetEntitySoundReference();
						if (entitySoundRef)
						{
							*(entitySoundRef) = NULL;
						}
						reqSets->InvalidateEntityRefPtr();
						reqSets->SetUpdateEntity(false);
						reqSets->InvalidateEntityPtr();
						sound->InvalidateTracker(reqSets);

						// important that we never use this ptr after calling SetIsReferenced(false)!
						sound->SetIsReferencedByGame(false);
						if(prevSoundId == 0xffff)
						{
							Assign(entRef.firstSoundRef, nextSoundId);
						}
						else
						{
							Assign(m_SoundListStore[prevSoundId].nextSoundRef, nextSoundId);
						}
						FreeSoundRef(soundId);
						deletedSound = true;
					}
				}
			}
		}
		// only update prev if we didn't delete the cur sound
		if(!deletedSound)
		{
			prevSoundId = soundId;
		}	
	}
}

u32 audController::RegisterEntity(audEntity* entity)
{
#if !__FINAL
	if (!m_IsInitialized)
	{
		return AUD_INVALID_CONTROLLER_ENTITY_ID;
	}
#endif // !__FINAL

	//Assert(!m_IsLocked);
	{		
		sysCriticalSection lock(m_LockEntities);
		if (m_NumUsedEntities != MAX_AUDIO_ENTITIES)
		{
			// Add it to our list
			u16& newEntry = m_FreeEntityIndexStack[m_NumUsedEntities++];
			Assert(newEntry != 0xffff);
			audControllerEntityRef& entRef = m_EntityList[newEntry];
			Assert(entRef.entity == NULL);
			entRef.entity = entity;
			entRef.firstSoundRef = 0xffff;
			
			if (m_EntityListHead != 0xffff)
			{
				audControllerEntityRef& headEntRef = m_EntityList[m_EntityListHead];
				Assert(headEntRef.prevEntity == 0xffff);
				headEntRef.prevEntity = newEntry;
				entRef.nextEntity = m_EntityListHead;
			}
			m_EntityListHead = newEntry;
			newEntry = 0xffff; // Mark this one as used for debug purposes
			return m_EntityListHead;
		}
		AssertMsg(0,"Out of room in controller entity list");
		return AUD_INVALID_CONTROLLER_ENTITY_ID;
	}
}

void audController::UnregisterEntity(audEntity* entity)
{
#if !__FINAL
	if (!m_IsInitialized)
	{
		return;
	}
#endif // !__FINAL

	//Assert(!m_IsLocked);
	{		
		sysCriticalSection lock(m_LockEntities);

		Assert(entity);
		if (entity == NULL)
		{
			return;
		}

		// Make sure we actually found it
		Assert(entity->GetControllerId() != 0xffff);
		const u32 entityId = entity->GetControllerId();
		
		// And now the really handy bit - we look through all our audSounds, and if any of them are pointing
		// at the audEntity that's just been removed, we remove that reference, so they can never call back 
		// on an invalid pointer.
	#if AUD_CHECK_THREADED_ENTITY_ALLOCATION
		const u32 numFreeAtStart = ComputeNumFreeEntries();
		const u32 numAllocatedAtStart = ComputeNumAllocatedEntries();
		Assert(numFreeAtStart + numAllocatedAtStart == MAX_CONTROLLER_SOUNDS);
	#endif

		audControllerEntityRef& entRef = m_EntityList[entityId];
		
		u32 nextSoundId = entRef.firstSoundRef;
		while(nextSoundId != 0xffff)
		{
			const u32 soundId = nextSoundId;
			nextSoundId = m_SoundListStore[soundId].nextSoundRef;

			audSound *sound = (audSound*)audSound::GetStaticPool().GetSoundSlot(m_SoundListStore[soundId].bucketId, m_SoundListStore[soundId].slotId);
			Assert(sound);
			audRequestedSettings *reqSets = sound->GetRequestedSettings();
			// The sound belonged to this entity, so null it
			Assert(sound->_GetEntity(reqSets) == entity || sound->_GetEntity(reqSets) == NULL);
			reqSets->InvalidateEntityPtr();
			reqSets->SetUpdateEntity(false);
			audSound **entitySoundRef = sound->GetEntitySoundReference();
			if (entitySoundRef)
			{
				*(entitySoundRef) = NULL;
			}
			reqSets->InvalidateEntityRefPtr();

			// move this sound to the orphaned list
			m_SoundListStore[soundId].nextSoundRef = m_OrphanEntityRef.firstSoundRef;
			Assign(m_OrphanEntityRef.firstSoundRef, soundId);
		}

		Assert(entityId < MAX_AUDIO_ENTITIES);
		Assert(m_NumUsedEntities != 0);
		if (m_NumUsedEntities != 0)
		{
			--m_NumUsedEntities;
			Assert(m_FreeEntityIndexStack[m_NumUsedEntities] == 0xffff);
			m_FreeEntityIndexStack[m_NumUsedEntities] = (u16)entityId;

			if (entityId == m_EntityListHead)
			{
				Assert(entRef.prevEntity==0xffff);
				m_EntityListHead = entRef.nextEntity;
			}
			if (entRef.nextEntity != 0xffff)
			{
				Assert(m_EntityList[entRef.nextEntity].prevEntity == entityId);
				m_EntityList[entRef.nextEntity].prevEntity = entRef.prevEntity;
			}
			if (entRef.prevEntity != 0xffff)
			{
				Assert(m_EntityList[entRef.prevEntity].nextEntity == entityId);
				m_EntityList[entRef.prevEntity].nextEntity = entRef.nextEntity;
			}
		}

		entRef.nextEntity = 0xffff;
		entRef.prevEntity = 0xffff;

		entRef.entity = NULL;

	#if AUD_CHECK_THREADED_ENTITY_ALLOCATION
		{
			const u32 numFreeAtEnd = ComputeNumFreeEntries();
			const u32 numAllocatedAtEnd = ComputeNumAllocatedEntries();
			Assert(numFreeAtEnd + numAllocatedAtEnd == MAX_CONTROLLER_SOUNDS);
		}
	#endif

	#if 0
		// Also go through all our environment groups, and mark that their entity has been deleted. We could maybe
		// do without this, and insist entities clean up their environment groups - it'd be more prone to bugs, but
		// this could be expensive - need to benchmark it and see.
		for (u32 i=0; i<MAX_ENVIRONMENT_GROUPS; i++)
		{
			if (m_EnvironmentGroupList[i] && (m_EnvironmentGroupList[i]->GetEntity() == entity))
			{
				// The environment group belonged to this entity, mark that it's gone
				Assert(!"entity didn't clean up its environment group");
				m_EnvironmentGroupList[i]->SetEntity(NULL);
			}
		}
	#endif
	}

	return;
}

void audController::StopAllSounds()
{
	u16 entityId = m_EntityListHead;
	while (entityId != 0xffff)
	{
		audControllerEntityRef& entRef = m_EntityList[entityId];
		StopAllSounds(entRef.entity, false);
		entityId = entRef.nextEntity;
	}
}

void audController::StopAllSounds(audEntity* entity, bool allowRelease /* = true */)
{
	sysCriticalSection lock(m_LockEntities);

	Assert(entity);
	if (entity == NULL)
	{
		return;
	}

	// Make sure we actually found it
	Assert(entity->GetControllerId() != 0xffff);
	const u32 entityId = entity->GetControllerId();
	
	// And now the really handy bit - we look through all our audSounds, and if any of them are pointing
	// at the audEntity that's just been removed, we remove that reference, so they can never call back 
	// on an invalid pointer.
#if AUD_CHECK_THREADED_ENTITY_ALLOCATION
	const u32 numFreeAtStart = ComputeNumFreeEntries();
	const u32 numAllocatedAtStart = ComputeNumAllocatedEntries();
	Assert(numFreeAtStart + numAllocatedAtStart == MAX_CONTROLLER_SOUNDS);
#endif

	u32 nextSoundId = m_EntityList[entityId].firstSoundRef;
	while(nextSoundId != 0xffff)
	{
		const u32 soundId = nextSoundId;
		nextSoundId = m_SoundListStore[soundId].nextSoundRef;

		audSound *sound = (audSound*)audSound::GetStaticPool().GetSoundSlot(m_SoundListStore[soundId].bucketId, m_SoundListStore[soundId].slotId);
		Assert(sound);
		
		audRequestedSettings *reqSets = sound->GetRequestedSettings();

		if(reqSets->ShouldPersistOverClears())
		{
			continue;
		}
		
		// The sound belonged to this entity, so null it
		Assert(sound->_GetEntity(reqSets) == entity || sound->_GetEntity(reqSets) == NULL);

		//sound->InvalidateEntity();
		reqSets->SetUpdateEntity(false);
		if (!allowRelease && !sound->IsBeingReleased())
		{
				reqSets->SetReleaseTime(0);
		}
		sound->StopAndForget(false);

		/*audSound **entitySoundRef = sound->GetEntitySoundReference();
		if (entitySoundRef)
		{
			*(entitySoundRef) = NULL;
		}
		sound->InvalidateEntitySoundReference();*/

		// move this sound to the orphaned list
		//m_SoundListStore[soundId].nextSoundRef = m_OrphanEntityRef.firstSoundRef;
		//Assign(m_OrphanEntityRef.firstSoundRef, soundId);
	}

#if AUD_CHECK_THREADED_ENTITY_ALLOCATION
	{
		const u32 numFreeAtEnd = ComputeNumFreeEntries();
		const u32 numAllocatedAtEnd = ComputeNumAllocatedEntries();
		Assert(numFreeAtEnd + numAllocatedAtEnd == MAX_CONTROLLER_SOUNDS);
	}
#endif
}

void audController::RegisterGameObjectMetadataUnloading(const audMetadataChunk& chunk)
{
	if(m_EnvironmentGroupManager)
	{
		m_EnvironmentGroupManager->RegisterGameObjectMetadataUnloading(m_EnvironmentGroupList, chunk);
	}
}

void audController::RegisterEnvironmentGroup(audEnvironmentGroupInterface* environmentGroup)
{
	if (!environmentGroup)
	{
		return;
	}
	
	if (m_NumUsedEnvironment != MAX_ENVIRONMENT_GROUPS)
	{
		//	We're about to add a controller, so apply a lock
		sysCriticalSection lock(m_LockEnvironment);
		
		// Add it to our list
		u16& newEntry = m_FreeEnvironmentIndexStack[m_NumUsedEnvironment++];
		Assert(newEntry != 0xffff);
		Assert(m_EnvironmentGroupList[newEntry] == NULL);
		m_EnvironmentGroupList[newEntry] = environmentGroup;

		if (m_EnvironmentListHead != 0xffff)
		{
			audEnvironmentGroupInterface*& headOcclRef = m_EnvironmentGroupList[m_EnvironmentListHead];
			Assert(headOcclRef->m_PrevIndex == 0xffff);
			headOcclRef->m_PrevIndex = newEntry;
			environmentGroup->m_NextIndex = m_EnvironmentListHead;
		}
		m_EnvironmentListHead = newEntry;
		newEntry = 0xffff; // Mark this one as used for debug purposes
	}
}

void audController::ReportTrackerDeletion(audTracker *tracker)
{
	AUDCONTROLLER_PF_FUNC(ReportTrackerDeletion);
	sysCriticalSection lock(m_LockEntities);

	Assert(tracker);
	// And now the really handy bit - we look through all our audSounds, and if any of them are pointing
	// at the audTracker that's just been removed, we remove that reference, so they can never call back 
	// on an invalid pointer.

	audEntity::ReportTrackerDeletion(tracker);

	u32 entityId = m_EntityListHead;
	while (entityId != 0xffff)
	{
		audControllerEntityRef& entRef = m_EntityList[entityId];
		Assert(entRef.entity);
		Assert(entRef.entity->GetControllerId() == entityId);
		Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
		Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
		u32 nextSoundId = entRef.firstSoundRef;
		while(nextSoundId!=0xffff)
		{
			audSound *sound = (audSound*)audSound::sm_Pool.GetSoundSlot(m_SoundListStore[nextSoundId].bucketId, m_SoundListStore[nextSoundId].slotId);
			Assert(sound);
			audRequestedSettings *reqSets = sound->GetRequestedSettings();
			if(sound->_GetTracker(reqSets) == tracker)
			{
				reqSets->InvalidateTrackerPtr();
			}
			nextSoundId = m_SoundListStore[nextSoundId].nextSoundRef;
		}
		entityId = entRef.nextEntity;
	}

	// Also check orphaned sounds
	u32 nextSoundId = m_OrphanEntityRef.firstSoundRef;
	while(nextSoundId != 0xffff)
	{
		audSound *sound = (audSound*)audSound::sm_Pool.GetSoundSlot(m_SoundListStore[nextSoundId].bucketId, m_SoundListStore[nextSoundId].slotId);
		Assert(sound);
		audRequestedSettings *reqSets = sound->GetRequestedSettings();
		if(sound->_GetTracker(reqSets) == tracker)
		{
			sound->InvalidateTracker(reqSets);
		}
		nextSoundId = m_SoundListStore[nextSoundId].nextSoundRef;
	}
}

audSound* audController::GetSoundInstance(const char *soundName, audEntity* entity, audSound **entitySoundRef, const audSoundInitParams* initParams)
{
	return GetSoundInstance(atStringHash(soundName), entity, entitySoundRef, initParams);
}
audSound* audController::GetSoundInstance(const u32 soundNameHash, audEntity* entity, audSound **entitySoundRef, const audSoundInitParams* initParams)
{
	return GetSoundInstance(SOUNDFACTORY.GetMetadataManager().GetObjectMetadataRefFromHash(soundNameHash), entity, entitySoundRef, initParams);
}

audSound* audController::GetSoundInstance(const audMetadataRef soundRef, audEntity* entity, audSound **entitySoundRef, const audSoundInitParams* initParams)
{
	sysCriticalSection lock(m_LockEntities);

#if __ASSERT
	if(m_ThreadVerifier)
	{
		m_ThreadVerifier->OnCreateSound(soundRef);
	}
#endif // __ASSERT

	if(!g_AudioEngine.IsAudioEnabled() && (!initParams || !initParams->IsAuditioning))
	{
		return NULL;
	}

	// Don't accept any new sounds if we're not currently active.
	if (!IsActive())
	{
		return NULL;
	}

	// Make sure that if we get a sound, we've room to store it in our list
	const u32 freeSlot = AllocateFreeSoundRef();
	const u32 entityId = entity->GetControllerId();

	if (freeSlot == 0xffff)
	{
		BANK_ONLY(audWarningf("Not enough room in Controller's sound list to store sound %s (entity: %s)", SOUNDFACTORY.GetMetadataManager().GetObjectName(soundRef), entity->GetName()));
		DebugPrintControllerSoundList();
		return NULL;
	}
	// choose a bucket for this hierarchy, unless we have one explicitly passed in, probably one reserved from game code for shadow sounds
	// the non-primary shadows get given the bucket if of their primary, so you don't need to explicitly pass anything in.
	audSoundInternalInitParams internalInitParams;

	internalInitParams.AllowLoad = initParams->AllowLoad;
	
	if (initParams->BucketId != 0xff)
	{
		Assign(internalInitParams.BucketId, initParams->BucketId);
	}
	else
	{
		Assign(internalInitParams.BucketId, audSound::GetStaticPool().GetEmptiestBucketId());
	}
	
	internalInitParams.RemoveHierarchy = initParams->RemoveHierarchy;
	Assign(internalInitParams.WaveSlotIndex, audWaveSlot::GetWaveSlotIndex(initParams->WaveSlot));

	if(initParams->TimerId == ~0U)
	{
		internalInitParams.TimerId = 0x1F;
	}
	else
	{
		internalInitParams.TimerId = initParams->TimerId;
		// we only use 5 bits internally so check for overflow
		Assert(internalInitParams.TimerId == initParams->TimerId);
	}

	internalInitParams.IsAuditioning = initParams->IsAuditioning;

	audSoundScratchInitParams scratchInitParams;
	
	Assign(scratchInitParams.shadowPcmSourceId, initParams->ShadowPcmSourceId);
	Assign(scratchInitParams.pcmSourceChannelId, initParams->PcmSourceChannelId);
	Assign(scratchInitParams.sourceEffectSubmixId, initParams->SourceEffectSubmixId);
	if(initParams->Category)
	{
		Assign(scratchInitParams.categoryIndex, audCategoryManager::GetCategoryIndex(initParams->Category));
		scratchInitParams.overriddenCategory = true;
	}
	scratchInitParams.effectRoute = initParams->EffectRoute;
	scratchInitParams.shouldPlayPhysically = initParams->ShouldPlayPhysically;
	
	audSoundParentInitParams parentInitParams;

	parentInitParams.Entity = entity;
	parentInitParams.Tracker = const_cast<audTracker*>(initParams->Tracker);
	parentInitParams.AttackTime = initParams->AttackTime;
	parentInitParams.IsStartOffsetPercentage = initParams->IsStartOffsetPercentage;
	parentInitParams.StartOffset = initParams->StartOffset;
	parentInitParams.Predelay = initParams->Predelay;
	parentInitParams.ShouldUpdateEntity = initParams->UpdateEntity;
	parentInitParams.EntitySoundRef = entitySoundRef;
	parentInitParams.TrackEntityPosition = initParams->TrackEntityPosition;
	
	audSound* sound = SOUNDFACTORY.GetInstance(soundRef, &internalInitParams, &scratchInitParams, &parentInitParams);
	if(!sound)
	{
		FreeSoundRef(freeSlot);
		return NULL;
	}

	// We've got a sound, so we store it in our list, and return it back to the audEntity
	m_SoundListStore[freeSlot].bucketId = sound->GetBucketId();
	m_SoundListStore[freeSlot].slotId = audSound::GetStaticPool().GetSoundSlotIndex(sound->GetBucketId(), sound);
	m_SoundListStore[freeSlot].nextSoundRef = m_EntityList[entityId].firstSoundRef;
	
	Assign(m_EntityList[entityId].firstSoundRef, freeSlot);
#if AUD_CHECK_THREADED_ENTITY_ALLOCATION
	const u32 numFreeAtStart = ComputeNumFreeEntries();
	const u32 numAllocatedAtStart = ComputeNumAllocatedEntries();
	Assert(numFreeAtStart + numAllocatedAtStart == MAX_CONTROLLER_SOUNDS);
#endif

	sound->SetIsReferencedByGame(true);

	if(entitySoundRef)
	{
		*entitySoundRef = sound;
	}

	return sound;
}

void audController::GetPersistentSoundInstance(const char *soundName, audEntity* entity, audSound** soundReference, const audSoundInitParams* initParams)
{
	return GetPersistentSoundInstance(atStringHash(soundName), entity, soundReference, initParams);
}
void audController::GetPersistentSoundInstance(const u32 soundNameHash, audEntity* entity, audSound** soundReference, const audSoundInitParams* initParams)
{
	return GetPersistentSoundInstance(SOUNDFACTORY.GetMetadataManager().GetObjectMetadataRefFromHash(soundNameHash), entity, soundReference, initParams);
}

void audController::GetPersistentSoundInstance(const audMetadataRef soundMetadataRef, audEntity* entity, audSound** soundReference, const audSoundInitParams* initParams)
{
	if (!entity || !soundReference)
	{
		if(soundReference)
		{
			*soundReference = NULL;
		}
		return;
	}

	audAssertf(!sysIpcIsStackAddress(soundReference), "Invalid sound reference address: %p", soundReference);

	// Get the sound and store it in our list
	audSound* sound = GetSoundInstance(soundMetadataRef, entity, soundReference, initParams);
	if (!sound)
	{
		// if GetSoundInstance() fails it won't set the sound ref for us
		*soundReference = NULL;
		return;
	}

	Assert(sound->GetRequestedSettings());
	return;
}

void audController::DebugPrintControllerSoundList() const
{
#if __BANK
	audDisplayf("audController sound list:");

	u32 entityId = m_EntityListHead;
	while (entityId != 0xffff)
	{
		const audControllerEntityRef& entRef = m_EntityList[entityId];
		Assert(entRef.entity);
		Assert(entRef.entity->GetControllerId() == entityId);
		Assert(entRef.nextEntity == 0xffff || m_EntityList[entRef.nextEntity].prevEntity == entityId);
		Assert(entRef.prevEntity == 0xffff || m_EntityList[entRef.prevEntity].nextEntity == entityId);
		DebugPrintControllerSoundList(entRef);
		entityId = entRef.nextEntity;
	}

	//	And now the orphaned list
	DebugPrintControllerSoundList(m_OrphanEntityRef);
#endif
}

void audController::DebugPrintControllerSoundList(const audControllerEntityRef& entRef) const
{
	u32 nextSoundId = entRef.firstSoundRef;
	while(nextSoundId!=0xffff)
	{
#if __DEV
		audSound *sound = (audSound*)audSound::sm_Pool.GetSoundSlot(m_SoundListStore[nextSoundId].bucketId, m_SoundListStore[nextSoundId].slotId);
		Assert(sound);
		audDisplayf("%s,%s,%u", sound->GetName(), (sound->GetEntity()?sound->GetEntity()->GetName():"no entity"), sound->GetPlayState());
#endif
		nextSoundId = m_SoundListStore[nextSoundId].nextSoundRef;
	}
}

#if __BANK
void PrintControllerSoundList()
{
	g_Controller->DebugPrintControllerSoundList();
}

void audController::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Audio Environment");
	bank.AddToggle("Update environment groups", &sm_ShouldUpdateEnvironmentGroups);
	bank.AddToggle("Use environment metric", &sm_ShouldUseEnvironmentMetric);
	bank.AddSlider("Max env groups to Update", &g_MaxEnvironmentGroupsToUpdate, 1, 100, 1);
	bank.AddButton("PrintControllerSoundList", CFA(PrintControllerSoundList));
	bank.PopGroup();
}
#endif

} // namespace rage
