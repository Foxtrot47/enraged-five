// 
// audioengine/ambisonics.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
// 

#ifndef AUD_AMBISONICS_H
#define AUD_AMBISONICS_H

#define AUD_ENABLE_AMBISONICS 1 // HACK_GTA4

#if AUD_ENABLE_AMBISONICS
namespace rage
{
	class bkBank;

	enum {
		AMBISONICS_SPEAKER_L_FRONT=0, //Left front
		AMBISONICS_SPEAKER_R_FRONT,	 //Right Front
		AMBISONICS_SPEAKER_CENTER,	 //Center
		AMBISONICS_SPEAKER_L_BACK,	 //Left Surround
		AMBISONICS_SPEAKER_R_BACK,	 //Right Surround
		AMBISONICS_SPEAKER_L_EX,	 //Left Back (7.0/7.1)
		AMBISONICS_SPEAKER_R_EX,	 //Right Back (7.0/7.1)
		AMBISONICS_SPEAKER_MAX
	};

	enum {
		FITNESS_WEIGHT_V=0,	//Velocity Volume
		FITNESS_WEIGHT_MV,	//Velocity Magnitude
		FITNESS_WEIGHT_AV,	//Velocity Angle
		FITNESS_WEIGHT_E,	//Energy Volume
		FITNESS_WEIGHT_ME,	//Energy Magnitude
		FITNESS_WEIGHT_AE,	//Energy Angle
		FITNESS_WEIGHT_COUNT
	};

	enum EDecoderType
	{
		NO_DECODER,
		SQUARE_DECODER,
		ITU_DECODER
	};

	enum
	{
		NO_STEREO_DECODE,
		MID_SIDE_DECODE,
		COINCIDENT_PAIR_DECODE
	};


	//Carries the encoded bFormat signal
	typedef struct  
	{
		f32 x, y, z, w, u, v, p, q, c4, s4, c5, s5;
	} bFormatParms;

	//The maximum ambisonic order that the encode/decode supports: NB not necessarily the order that will be used
#define AMB_ORDER 5 

	//The decoder coefficients for a speaker (also used to keep track of steps, direction etc during tabu search)
	typedef struct  
	{
		f32 Co[AMB_ORDER];
		f32 Si[AMB_ORDER];
		f32 w;
	}bFormatCoefficients;

	struct ambisonicDecodeData
	{
		ambisonicDecodeData();

		bFormatCoefficients decoder[AMBISONICS_SPEAKER_MAX];
		bFormatParms bFormat;
		f32 speakerGains[AMBISONICS_SPEAKER_MAX];
		f32 proportionOut;
		f32 elevationComponent;
		f32 occlusionLeak;
		EDecoderType decoderType;
	};

	//Data regarding the position of the speaker
	struct speakerSetup
	{	
		speakerSetup();

		f32 angle; //in degrees
		f32 angleCos;
		f32 angleSin;
		bool isActive;
	};

	struct TabuSpuOutput
	{
		TabuSpuOutput();
		void Init();

		bFormatCoefficients localBest[AMBISONICS_SPEAKER_MAX];
		bFormatCoefficients localUp[AMBISONICS_SPEAKER_MAX];
		bFormatCoefficients localDown[AMBISONICS_SPEAKER_MAX];
		bFormatCoefficients direction[AMBISONICS_SPEAKER_MAX];
		u32 fitSpeaker;
		u32 fitOrder;
		f32 fitDir;
		f32 bestLocalFit;
	};

	struct ambisonicDrawData
	{
		f32 incidentAng;
		f32 reproducedAngE;
		f32 reproducedAngV;
		f32 vecLengthE;
		f32 vecLengthV;
		f32 pressure;
		f32 energy;
	};

	typedef void (*drawFuncPtr) (ambisonicDrawData data[90]);

	enum AmbisonicDecoderMode 
	{
		kAmbisonicsSquare = 0,
		kAmbisonicsItu,
		kAmbisonicsItuSide,
		kAmbisonicsHeadphones,
		kAmbisonicsStereo,
		kAmbisonicsItuFront15,
		kAmbisonicsFwRr,
		kAmbisonicsFwRm,
		kAmbisonicsFwRs,
		kAmbisonicsFnRr,
		kAmbisonicsFnRm,
		kAmbisonicsFnRs,
		kAmbisonicsFmRr,
		kAmbisonicsFmRm,
		kAmbisonicsFmRs,
		kNumAmbisonicsModes
	};

	class audAmbisonics
	{
	public:
		audAmbisonics();

		audAmbisonics(audAmbisonics *cpy);

		void Init();
		void InitSearchParams();

		//Functions used in callbacks from the user interface
		void RandomizeSpeakerWeights(bFormatCoefficients inCo[AMBISONICS_SPEAKER_MAX]);
		void RandomizeSpeakerWeights();
		void GenerateOptimizedDecoder();
		void DecoderStep() { m_IterationCount = 1;}
		void SetupSpeakers(const f32 angles[AMBISONICS_SPEAKER_MAX]);
		void GetDecoder(ambisonicDecodeData &data);
		void SetEnvironmentDecoder(ambisonicDecodeData &data);
		void SetEnvironmentDecoder();
		void SetEnvironmentDecoder(AmbisonicDecoderMode mode);
		void DefaultSquare();
		void DefaultItu();
		void ItuNC();
		void FwrrSpeakerSetup();
		void FwrmSpeakerSetup();
		void SetupDecode(const f32 speakerAngles[AMBISONICS_SPEAKER_MAX]);
		void tabuSquareDecode();
		void EnergyDecode();
		void HeadphoneDecode();
		void SideDecode();
		void MeMv2Decode();
		void ituNc1Decode();
		void itu15Decode();
		void fnrrDecode();
		void fnrsDecode();
		void fwrsDecode();
		void fwrmDecode();
		void fwrrDecode();
		void SetTemp1Decoder();
		void SetTemp2Decoder();
		void SetTemp3Decoder();
		void Temp1Decode();
		void Temp2Decode();
		void Temp3Decode();
		void ApplySpeakerSettingsToDecoder();
		void SetupFitnessWeights(const f32 weights[FITNESS_WEIGHT_COUNT]);

		//Interface for other modules using ambisonics (e.g. environment_game)
		static void ZeroSpeakerWeights(bFormatCoefficients inCo[AMBISONICS_SPEAKER_MAX]);
		static void SetupBFormatCoefficients(bFormatCoefficients outBF[AMBISONICS_SPEAKER_MAX], f32 inCo[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1]);
		static void CalculateBFormatEncode(ambisonicDecodeData &data, f32 angleCos, f32 angleSin);
		static void CalculateSpeakerGains(ambisonicDecodeData &data);
		static void CopySpeakerWeights(bFormatCoefficients toCoefficients[AMBISONICS_SPEAKER_MAX], const bFormatCoefficients fromCoefficients[AMBISONICS_SPEAKER_MAX]);
		void LocalCopySpeakerWeights(bFormatCoefficients toCoefficients[AMBISONICS_SPEAKER_MAX], const bFormatCoefficients fromCoefficients[AMBISONICS_SPEAKER_MAX]);
		void LocalCalculateBFormatEncode(ambisonicDecodeData &data, f32 angleCos, f32 angleSin);
		void LocalCalculateSpeakerGains(ambisonicDecodeData &data);
		void Update();
		void SaveDecoder(ambisonicDecodeData &data);
		void LoadDecoder(ambisonicDecodeData &data);
		void SetJobOutput(TabuSpuOutput *output); //Used in ambisonic optimization job
		void SetJobOutput(TabuSpuOutput *output, audAmbisonics *inAmb); //for 360 which is flakier with the threading (at least I can't find out what I've done wrong)
		void TabuCore(int speaker, int j); //Used in ambisonic optimization job; j controls up/down for the search
		static void SetDrawFunction(void (*callback)(ambisonicDrawData data[90]))
		{
			audAmbisonics::drawFunc = callback;
		}



#if __BANK

		static void DefaultSquareCallback(audAmbisonics *_this);
		static void DefaultItuCallback(audAmbisonics *_this);
		static void NCItuCallback(audAmbisonics *_this);
		static void FwrrCallback(audAmbisonics *_this);
		static void FwrmCallback(audAmbisonics *_this);
		static void MaxMeMv1(audAmbisonics *_this);
		static void MaxMeMv2(audAmbisonics *_this);
		static void MaxAeAv(audAmbisonics *_this);

		static void PrintDecoderCallback(audAmbisonics *_this)
		{
			_this->PrintDecoder();
		}

		static void ApplySpeakerCallback(audAmbisonics *_this)
		{
			_this->ApplySpeakerSettingsToDecoder();
		}

		static void MeMv2DecodeCallback(audAmbisonics *_this)
		{
			_this->MeMv2Decode();
		}

		static void ituNc1DecodeCallback(audAmbisonics *_this)
		{
			_this->ituNc1Decode();
		}

		static void itu15DecodeCallback(audAmbisonics *_this)
		{
			_this->itu15Decode();
		}

		static void  tabuSquareDecodeCallback(audAmbisonics *_this)
		{
			_this->tabuSquareDecode();
		}

		static void EnergyDecodeCallback(audAmbisonics *_this)
		{
			_this->EnergyDecode();
		}

		static void HeadphoneDecodeCallback(audAmbisonics *_this)
		{
			_this->HeadphoneDecode();
		}

		static void SideDecodeCallback(audAmbisonics *_this)
		{
			_this->SideDecode();
		}

		static void Temp1DecodeCallback(audAmbisonics *_this)
		{
			_this->Temp1Decode();
		}

		static void Temp2DecodeCallback(audAmbisonics *_this)
		{
			_this->Temp2Decode();
		}

		static void Temp3DecodeCallback(audAmbisonics *_this)
		{
			_this->Temp3Decode();
		}

		static void SetTemp1DecoderCallback(audAmbisonics *_this)
		{
			_this->SetTemp1Decoder();
		}

		static void SetTemp2DecoderCallback(audAmbisonics *_this)
		{
			_this->SetTemp2Decoder();
		}

		static void SetTemp3DecoderCallback(audAmbisonics *_this)
		{
			_this->SetTemp3Decoder();
		}

		static void RandomizeSpeakerWeightsCallback(audAmbisonics *_this)
		{
			_this->RandomizeSpeakerWeights();
		}

		static void GenerateOptimizedDecoderCallback(audAmbisonics *_this)
		{
			_this->SetSearchMetrics();
			_this->GenerateOptimizedDecoder();
		}

		static void DecoderStepCallback(audAmbisonics *_this)
		{
			_this->DecoderStep();
		}

		static void SetEnvironmentDecoderCallback(audAmbisonics *_this)
		{
			_this->SetEnvironmentDecoder();
		}

		void AddWidgets(bkBank &bank);

#endif

		static void SetDecoderMode(AmbisonicDecoderMode mode)
		{
			sm_DecoderMode = mode;
		}

		static AmbisonicDecoderMode GetDecoderMode()
		{
			return sm_DecoderMode;
		}

		static AmbisonicDecoderMode sm_DecoderMode;

	private:

		void Tabu();
		void TabuSpu();
		void CalculateFitnessCoefficients(bool draw = false);
		void CalculateFitnessCoefficientsDrawHack(bool draw = false);
		f32	 GetFitness();
		f32  GetDebugFitness();
		void OptimizeDecoder();
		void IncrementTabuDirection( f32 increment);
		void UpdateSearchParameters();
		void MirrorSpeakerWeights(); //Optimization for arrays symmetrical about y axis (eg square itu)
		void SetDeltaLimits(int speaker, int order);
		void ApplyJobOutput(TabuSpuOutput *output);
		void SetSearchMetrics();
		void PrintDecoder();

		static drawFuncPtr drawFunc;


		bFormatCoefficients tabuWeights[AMBISONICS_SPEAKER_MAX]; //Optimized speaker weights one set of coefficients for each speaker
		bFormatCoefficients localUp[AMBISONICS_SPEAKER_MAX]; //Speaker weights one set of coefficients for each speaker
		bFormatCoefficients localDown[AMBISONICS_SPEAKER_MAX]; //Speaker weights one set of coefficients for each speaker
		bFormatCoefficients direction[AMBISONICS_SPEAKER_MAX]; //Speaker weights one set of coefficients for each speaker
		bFormatCoefficients localBest[AMBISONICS_SPEAKER_MAX]; //Speaker weights one set of coefficients for each speaker

		//These are used for A/B'ing decoders
		bFormatCoefficients tempDecoder1[AMBISONICS_SPEAKER_MAX];
		bFormatCoefficients tempDecoder2[AMBISONICS_SPEAKER_MAX];
		bFormatCoefficients tempDecoder3[AMBISONICS_SPEAKER_MAX];
		EDecoderType tempDecoder1Type;
		EDecoderType tempDecoder2Type;
		EDecoderType tempDecoder3Type;


		ambisonicDecodeData decodeData; //Stores the decoder coefficients and associated data
		ambisonicDrawData drawData[90]; //Used to draw the fitness
		speakerSetup speakers[AMBISONICS_SPEAKER_MAX]; //setup information regarding the speakers


		//Fitness variables calculated from source angle and speaker setup
		f32 m_PFit;
		f32 m_EFit;
		f32 m_MvFit;
		f32 m_MeFit;
		f32 m_AvFit;
		f32 m_AeFit;
		f32 m_Delta;  //Size of steps the tabu search will move in
		f32 fitnessWeights[FITNESS_WEIGHT_COUNT]; //Control what the decoder will be optimized for
		u32 fitSpeaker, fitOrder;
		u32 m_AmbOrder; //Current amb order being used in the search
		u32 m_IterationCount; //keeps track of how many iterations the optimize has left
		f32 m_TabuSteps; //number of steps that tabu will lock a direction
		u32 m_StuckCount; //number of times we've gone without an improvement in fitness
		f32 bestFitness; //keeps track of the best fitness found during the optimize;
		f32 bestLocalFit; //keeps track of the best local fitness
		f32 fitDir;	//keeps track of the direction the best fitness was found in
		u32 m_SearchLevel; //keeps track of what stage the search is at
		u32 m_StartTime;
		f32 m_CosMax, m_CosMin, m_SinMax, m_SinMin; //Used to exclude search space we know isn't viable
		f32 m_UpperBand; //The upper band of fitness used to update search parameters
		f32 m_MidBand; //The middle band of fitness used to update search parameters
		f32 m_LowBand; //The lower (final) band of fitness used to update search parameters
		u32 m_TotalCount; //how many iterations this search
		u32 m_TargetOrder; //this is used during the search to store the final order we want to include up to
		bool m_DoFineTuning; //Do the final stage of the search
		bool m_UseSpuOptimize; //Use the task version of the decoder optimize
		bool calculatenonstop; //enables/disables the search
		bool m_DoDraw; //Turn on fitness display graphics
		bool m_DrawBestDecoder; //fitness display shows the best current decoder

	};

}

#endif // AUD_ENABLE_AMBISONICS
#endif