// 
// categorydefs.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure definitions - do not edit.
// 

#ifndef AUD_CATEGORYDEFS_H
#define AUD_CATEGORYDEFS_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "audioengine/metadataref.h"

// macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
	#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
	#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))
	namespace rage
	{
		enum TristateValue
		{
			AUD_TRISTATE_FALSE = 0,
			AUD_TRISTATE_TRUE,
			AUD_TRISTATE_UNSPECIFIED,
		}; // enum TristateValue
	} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE

namespace rage
{
	#define CATEGORYDEFS_SCHEMA_VERSION 22
	
	#define CATEGORYDEFS_METADATA_COMPILER_VERSION 2
	
	// NOTE: doesn't include abstract objects
	#define AUD_NUM_CATEGORYDEFS 1
	
	#define AUD_DECLARE_CATEGORYDEFS_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);
	
	#define AUD_DEFINE_CATEGORYDEFS_FUNCTION(fn) \
	void* GetAddressOf_##fn(rage::u8 classId) \
	{ \
		switch(classId) \
		{ \
			case Category::TYPE_ID: return (void*)&Category::s##fn; \
			default: return NULL; \
		} \
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gCategoriesGetBaseTypeId(const rage::u32 classId);
	
	// PURPOSE - Determines if a type inherits from another type
	bool gCategoriesIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId);
	
	// PURPOSE - Determines if a type inherits from another type
	template<class _ObjectType>
	bool gCategoriesIsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)
	{
		return gCategoriesIsOfType(obj->ClassId, baseTypeId);
	}
	
	// 
	// Enumerations
	// 
	enum SoundTimer
	{
		kSoundTimerNormal = 0,
		kSoundTimerUnpausable,
		kSoundTimerRadio,
		kSoundTimerMusic,
		kSoundTimerUnspecified,
		kSoundTimerScriptedSpeech,
		kSoundTimerSlowMo,
		NUM_SOUNDTIMER,
		SOUNDTIMER_MAX = NUM_SOUNDTIMER,
	}; // enum SoundTimer
	const char* SoundTimer_ToString(const SoundTimer value);
	SoundTimer SoundTimer_Parse(const char* str, const SoundTimer defaultValue);
	
	// disable struct alignment
	#if !__SPU
	#pragma pack(push, r1, 1)
	#endif // !__SPU
		// 
		// Category
		// 
		enum CategoryFlagIds
		{
			FLAG_ID_CATEGORY_MUTEONUSERMUSIC = 0,
			FLAG_ID_CATEGORY_UNPAUSABLE,
			FLAG_ID_CATEGORY_DISABLEREARATTEN,
			FLAG_ID_CATEGORY_MUTE,
			FLAG_ID_CATEGORY_SOLO,
			FLAG_ID_CATEGORY_MAX,
		}; // enum CategoryFlagIds
		
		struct Category
		{
			static const rage::u32 TYPE_ID = 0;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			Category() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				Volume(0),
				Pitch(0),
				LPFCutoff(23900),
				LPFDistanceCurve(1757063444U), // DEFAULT_LPF_CURVE
				HPFCutoff(0),
				HPFDistanceCurve(741353067U), // DEFAULT_HPF_CURVE
				DistanceRollOffScale(100),
				PlateauRollOffScale(100),
				OcclusionDamping(100),
				EnvironmentalFilterDamping(100),
				SourceReverbDamping(100),
				DistanceReverbDamping(100),
				InteriorReverbDamping(100),
				EnvironmentalLoudness(0),
				UnderwaterWetLevel(0),
				StonedWetLevel(100),
				Timer(kSoundTimerUnspecified)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			struct tParentOverrides
			{
				union
				{
					rage::u16 Value;
					struct
					{
						bool VolumeOverridesParent:1;
						bool PitchOverridesParent:1;
						bool LPFCutoffOverridesParent:1;
						bool HPFCutoffOverridesParent:1;
						bool DistanceRollOffScaleOverridesParent:1;
						bool PlateauRollOffScaleOverridesParent:1;
						bool OcclusionDampingOverridesParent:1;
						bool EnvironmentalFilterDampingOverridesParent:1;
						bool SourceReverbDampingOverridesParent:1;
						bool DistanceReverbDampingOverridesParent:1;
						bool InteriorReverbDampingOverridesParent:1;
						bool EnvironmentalLoudnessOverridesParent:1;
						bool UnderwaterWetLevelOverridesParent:1;
						bool StonedWetLevelOverridesParent:1;
						bool padding:2; // padding to next byte boundary						
					} BitFields;
				};
			} ParentOverrides; // struct tParentOverrides
			
			rage::s16 Volume;
			rage::s16 Pitch;
			rage::u16 LPFCutoff;
			rage::u32 LPFDistanceCurve;
			rage::u16 HPFCutoff;
			rage::u32 HPFDistanceCurve;
			rage::u16 DistanceRollOffScale;
			rage::u16 PlateauRollOffScale;
			rage::u16 OcclusionDamping;
			rage::u16 EnvironmentalFilterDamping;
			rage::u16 SourceReverbDamping;
			rage::u16 DistanceReverbDamping;
			rage::u16 InteriorReverbDamping;
			rage::u16 EnvironmentalLoudness;
			rage::u16 UnderwaterWetLevel;
			rage::u16 StonedWetLevel;
			rage::u8 Timer;
			
			static const rage::u8 MAX_CATEGORYREFS = 24;
			rage::u8 numCategoryRefs;
			struct tCategoryRef
			{
				rage::u32 CategoryId;
			} CategoryRef[MAX_CATEGORYREFS]; // struct tCategoryRef
			
		} SPU_ONLY(__attribute__((packed))); // struct Category
		
	// enable struct alignment
	#if !__SPU
	#pragma pack(pop, r1)
	#endif // !__SPU
} // namespace rage
#endif // AUD_CATEGORYDEFS_H
