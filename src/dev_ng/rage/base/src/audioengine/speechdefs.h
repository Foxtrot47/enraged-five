// 
// speechdefs.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure definitions - do not edit.
// 

#ifndef AUD_SPEECHDEFS_H
#define AUD_SPEECHDEFS_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "audioengine/metadataref.h"

// macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
	#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
	#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))
	namespace rage
	{
		enum TristateValue
		{
			AUD_TRISTATE_FALSE = 0,
			AUD_TRISTATE_TRUE,
			AUD_TRISTATE_UNSPECIFIED,
		}; // enum TristateValue
	} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE

namespace rage
{
	#define SPEECHDEFS_SCHEMA_VERSION 4
	
	#define SPEECHDEFS_METADATA_COMPILER_VERSION 2
	
	// NOTE: doesn't include abstract objects
	#define AUD_NUM_SPEECHDEFS 6
	
	#define AUD_DECLARE_SPEECHDEFS_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);
	
	#define AUD_DEFINE_SPEECHDEFS_FUNCTION(fn) \
	void* GetAddressOf_##fn(rage::u8 classId) \
	{ \
		switch(classId) \
		{ \
			case VariationGroupInfoData::TYPE_ID: return (void*)&VariationGroupInfoData::s##fn; \
			case VoiceContextInfo::TYPE_ID: return (void*)&VoiceContextInfo::s##fn; \
			case VoiceContextInfoVarGroupData::TYPE_ID: return (void*)&VoiceContextInfoVarGroupData::s##fn; \
			case Voice::TYPE_ID: return (void*)&Voice::s##fn; \
			case BankNameTable::TYPE_ID: return (void*)&BankNameTable::s##fn; \
			case VoiceContextVariationMask::TYPE_ID: return (void*)&VoiceContextVariationMask::s##fn; \
			default: return NULL; \
		} \
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gSpeechGetBaseTypeId(const rage::u32 classId);
	
	// PURPOSE - Determines if a type inherits from another type
	bool gSpeechIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId);
	
	// PURPOSE - Determines if a type inherits from another type
	template<class _ObjectType>
	bool gSpeechIsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)
	{
		return gSpeechIsOfType(obj->ClassId, baseTypeId);
	}
	
	// disable struct alignment
	#if !__SPU
	#pragma pack(push, r1, 1)
	#endif // !__SPU
		// 
		// VariationGroupInfoData
		// 
		enum VariationGroupInfoDataFlagIds
		{
			FLAG_ID_VARIATIONGROUPINFODATA_MAX = 0,
		}; // enum VariationGroupInfoDataFlagIds
		
		struct VariationGroupInfoData
		{
			static const rage::u32 TYPE_ID = 0;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			
			static const rage::u8 MAX_VARIATIONDATAS = 255;
			rage::u8 numVariationDatas;
			struct tVariationData
			{
				rage::u8 val;
			} VariationData[MAX_VARIATIONDATAS]; // struct tVariationData
			
		} SPU_ONLY(__attribute__((packed))); // struct VariationGroupInfoData
		
		// 
		// VoiceContextInfo
		// 
		enum VoiceContextInfoFlagIds
		{
			FLAG_ID_VOICECONTEXTINFO_MAX = 0,
		}; // enum VoiceContextInfoFlagIds
		
		struct VoiceContextInfo
		{
			static const rage::u32 TYPE_ID = 1;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			VoiceContextInfo() :
				numVariations(0),
				bankNameTableIndex(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			rage::u8 numVariations;
			rage::u16 bankNameTableIndex;
		} SPU_ONLY(__attribute__((packed))); // struct VoiceContextInfo
		
		// 
		// VoiceContextInfoVarGroupData
		// 
		enum VoiceContextInfoVarGroupDataFlagIds
		{
			FLAG_ID_VOICECONTEXTINFOVARGROUPDATA_MAX = 0,
		}; // enum VoiceContextInfoVarGroupDataFlagIds
		
		struct VoiceContextInfoVarGroupData
		{
			static const rage::u32 TYPE_ID = 2;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			rage::audMetadataRef ref;
		} SPU_ONLY(__attribute__((packed))); // struct VoiceContextInfoVarGroupData
		
		// 
		// Voice
		// 
		enum VoiceFlagIds
		{
			FLAG_ID_VOICE_MAX = 0,
		}; // enum VoiceFlagIds
		
		struct Voice
		{
			static const rage::u32 TYPE_ID = 3;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			Voice() :
				painType(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			rage::u8 painType;
		} SPU_ONLY(__attribute__((packed))); // struct Voice
		
		// 
		// BankNameTable
		// 
		enum BankNameTableFlagIds
		{
			FLAG_ID_BANKNAMETABLE_MAX = 0,
		}; // enum BankNameTableFlagIds
		
		struct BankNameTable
		{
			static const rage::u32 TYPE_ID = 4;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			BankNameTable() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				bankName(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			
			rage::u32 bankName;
		} SPU_ONLY(__attribute__((packed))); // struct BankNameTable
		
		// 
		// VoiceContextVariationMask
		// 
		enum VoiceContextVariationMaskFlagIds
		{
			FLAG_ID_VOICECONTEXTVARIATIONMASK_MAX = 0,
		}; // enum VoiceContextVariationMaskFlagIds
		
		struct VoiceContextVariationMask
		{
			static const rage::u32 TYPE_ID = 5;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			VoiceContextVariationMask() :
				variationMask(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			rage::u16 variationMask;
		} SPU_ONLY(__attribute__((packed))); // struct VoiceContextVariationMask
		
	// enable struct alignment
	#if !__SPU
	#pragma pack(pop, r1)
	#endif // !__SPU
} // namespace rage
#endif // AUD_SPEECHDEFS_H
