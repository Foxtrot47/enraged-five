// 
// entityvariablecache.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
// 

#ifndef AUD_ENTITYVARIABLECACHE_H
#define AUD_ENTITYVARIABLECACHE_H

#include "widgets.h"

namespace rage
{

class audEntityVariableCache
{
public:

	void Init();
	float * ResolveVariablePtr(float * ea);
	void SetVariableValue(float * ea, float value);

private:

	enum { kNumCacheBlocks = 2};

	ALIGNAS(16) atRangeArray<float, kNumCacheBlocks * audVariableBlock::kNumVariablesPerBlock> m_Cache ;
	atRangeArray<size_t, kNumCacheBlocks> m_CacheEa;
	u32 m_NextIndex;
};

}
#endif