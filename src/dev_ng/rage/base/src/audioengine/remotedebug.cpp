// 
// audioengine/remotedebugger.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK
#include "curverepository.h"
#include "categorymanager.h"
#include "engine.h"
#include "entity.h"
#include "engineutil.h"
#include "remotedebug.h"
#include "widgets.h"
#include "audiohardware/channel.h"

#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "grcore/viewport.h"
#include "system/namedpipe.h"
#include "system/param.h"
#include "system/pipepacket.h"
#include "system/timemgr.h"

#include "bank/bank.h"

namespace rage
{
	bool audRemoteDebug::sm_ShouldSendCategoryData = true;
	u32 audRemoteDebug::sm_LastSentMessage = 0;

	PARAM(audiodebugger, "[RAGE Audio] Connect to audiodebugger");
	PARAM(audiodebuggerhost, "[RAGE Audio] Manually specify the remote audiodebugger host (defaults to rfs.dat IP)");

	enum
	{
		AUD_DEBUGGER_PORT = 48001,
	};

	u8 audRemoteDebug::sm_PayloadBuf[audRemoteDebug::kRemoteDebugPayloadSizeBytes];
	u8 g_CategorySendBuffer[64*1024];

	audRemoteDebug::audRemoteDebug()
	{
		m_LastMessageReceiptTime = 0;
		m_ActiveTypes = NULL_DEBUG_TYPE;
	}

	audRemoteDebug::~audRemoteDebug()
	{
	}

	void audRemoteDebug::GenerateUniqueId()
	{
		m_UniqueId = audEngineUtil::GetRandomInteger();
	}

	bool audRemoteDebug::Init()
	{
		sysCriticalSection lock(m_NetworkCS);
		if(IsPresent())
		{
			ActivateDebugType(ENTITY_DEBUG);

			// create an async pipe
			m_Pipe.SetNoDelay(true);
			const char *remoteHost;
			if(PARAM_audiodebuggerhost.Get(remoteHost) && remoteHost && strlen(remoteHost) > 0)
			{
				while(!m_Pipe.Open(remoteHost, AUD_DEBUGGER_PORT))
				{
					audWarningf("Couldn't connect to audiodebugger at %s - retrying ...", remoteHost);
					sysIpcSleep(10);
				}
			}
			else
			{
				while(!m_Pipe.Open(AUD_DEBUGGER_PORT))
				{
					audWarningf("Couldn't connect to audiuodebugger - retrying ...");
					sysIpcSleep(10);
				}
			}
			audWarningf("Reconnecting to audiodebugger\n");
			sm_LastSentMessage = 0;

			SetRuntimePlatform();

			// if we want commandline activation of specific debug features, add that here
			/*if(PARAM_audiodebugger.Get(metadataTypeListRO) && metadataTypeListRO && strlen(metadataTypeListRO) > 0)
			{
			char *metadataTypeList = StringDuplicate(metadataTypeListRO);

			const char* token = strtok(metadataTypeList, ",");

			for (; token; token = strtok( NULL, ","))
			{
			if(!token)
			{
			break;
			}
			m_RaveMetadataTypes.PushAndGrow(atStringHash(token));
			audDisplayf("Using RAVE for metadata type %s", token);
			}

			StringFree(metadataTypeList);
			}*/
		}
		return true;
	}

	void audRemoteDebug::Shutdown()
	{
		sysCriticalSection lock(m_NetworkCS);
		if (IsPresent())
		{
			m_Pipe.Close();
		}
	}

	void audRemoteDebug::Update(u32 time)
	{
		sysCriticalSection lock(m_NetworkCS);
		if (IsPresent())
		{
			if(IsConnected())
			{
				u8 headerBuf[8];
				u16 *packetSize = (u16*)&headerBuf[5];
				if(sm_LastSentMessage < time - 1000)
				{
					// resolved category settings
					bool success = false;
					if(sm_ShouldSendCategoryData || IsDebugTypeActive(CATEGORY_DEBUG))
					{
						success = SendCategoryData();
					}
					else 
					{
						// send a keep-alive signal to audiodebugger
						headerBuf[0] = DEBUG_PING;
						*((u32*)&headerBuf[1]) = time;
						*((u16*)(&headerBuf[5])) = 0;
						headerBuf[7] = 0; //use this for time length or userdata?
						success = SafeWrite(headerBuf, 8);
					}

					if(!success)
					{
						audWarningf("Disconnected from audiodebugger\n");
						m_Pipe.Close();

						// reconnect to audiodebugger
						Init();
					}
					sm_LastSentMessage = time;
				}

				if(m_Pipe.Read(&headerBuf, 1) == 1)
				{
					m_Pipe.SafeRead(&headerBuf[1], 7);

					Assert(*packetSize <= sizeof(sm_PayloadBuf));
					if(*packetSize > sizeof(sm_PayloadBuf))
					{
						// throw away this packet
						u32 bytesToFlush = *packetSize;
						audWarningf("RemoteDebug packet too big: %u", bytesToFlush);
						while(bytesToFlush>0)
						{
							const s32 bytesReadThisTime = m_Pipe.Read(&sm_PayloadBuf, Min<u32>(kRemoteDebugPayloadSizeBytes,bytesToFlush));
							if(bytesReadThisTime < 0)
							{
								// bail out - connection has been terminated
								audErrorf("Failed to flush remote debug connection");
								return;
							}
							bytesToFlush -= bytesReadThisTime;
						}
					}
					else if(*packetSize != 0)
					{
						m_Pipe.SafeRead(&sm_PayloadBuf, *packetSize);
					}			
					HandleReceivedPacket(headerBuf[0], *packetSize, (u8*)&sm_PayloadBuf);
				}
			}
		}
	}

	bool audRemoteDebug::SendCategoryData()
	{
		u8 *ptr = &g_CategorySendBuffer[0];
		*ptr++ = DEBUG_RESOLVEDCATEGORYDATA;

		*((u32*)ptr) = g_AudioEngine.GetTimeInMilliseconds();
		ptr+= 4;

		// packet size, will rewrite further down
		u16* sizePtr = (u16*)ptr;
		*sizePtr = 0;
		ptr += 2;

		*ptr++ = 0; //use this for userdata?

		u16 numCategories = (u16)g_AudioEngine.GetCategoryManager().GetNumberOfStaticCategories();
		*((u16*)ptr) = numCategories;
		ptr += 2;

		// need to send name table offset -> index map
		for(u32 i = 0; i < numCategories; i++)
		{
			audCategory *category = g_AudioEngine.GetCategoryManager().GetCategoryFromIndex((s16)i);
			u32 nto = category->GetNameTableOffset();
			*((u32*)ptr) = nto;
			ptr += 4;
		}

		tCategorySettings* settingsPtr = &g_ResolvedCategorySettings[0];
		for (u32 i = 0; i < numCategories; i++, settingsPtr++)
		{
			*((f32*)ptr) = settingsPtr->Volume.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->DistanceRollOffScale.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->OcclusionDamping.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->EnvironmentalFilterDamping.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->SourceReverbDamping.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->DistanceReverbDamping.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->InteriorReverbDamping.GetFloat32_FromFloat16();
			ptr += 4;

			*((f32*)ptr) = settingsPtr->EnvironmentalLoudness.GetFloat32_FromFloat16();
			ptr += 4;

			*((s16*)ptr) = settingsPtr->Pitch;
			ptr += 2;

			*((u16*)ptr) = settingsPtr->LPFCutoff;
			ptr += 2;

			*((u16*)ptr) = settingsPtr->HPFCutoff;
			ptr += 2;
		}

		size_t totalBufSize = ((size_t)(ptr - &g_CategorySendBuffer[0]));
		Assert(totalBufSize < sizeof(g_CategorySendBuffer));

		// write packet size, don't include the size of the 8 byte header
		*sizePtr = (u16) totalBufSize - 8;

		return SafeWrite(&g_CategorySendBuffer[0], totalBufSize);
	}

	void audRemoteDebug::HandleReceivedPacket(const u8 packetType, const u32 UNUSED_PARAM(payloadSize), u8 *UNUSED_PARAM(payload))
	{
		//u32 *ptr, metadataTypeHash, size, hash, chunkNameHash;
		switch(packetType)
		{
		case MESSAGERECEIPT:
			m_LastMessageReceiptTime = g_AudioEngine.GetTimeInMilliseconds();
			break;
		}
	}

	bool audRemoteDebug::IsPresent()
	{
		return __BANK ? PARAM_audiodebugger.Get() : false;
	}

	bool audRemoteDebug::IsConnected() const
	{
		return m_Pipe.IsValid();
	}

	bool audRemoteDebug::SendMessage(audRemoteDebugCommands command, const u8 *message, const u16 length)
	{
		sysCriticalSection lock(m_NetworkCS);
		if (IsPresent())
		{
			if (IsConnected())
			{
				// 8 byte header: Type, timestamp, size, timelength/userdata?
				u8 *buffer = rage_new u8[length + 8];
				if(buffer)
				{
					buffer[0] = (u8)command;
					*((u32*)&buffer[1]) = g_AudioEngine.GetTimeInMilliseconds();
					*((u16*)&buffer[5]) = length;
					buffer[7] = 0; //use this for time length or userdata?
					sysMemCpy(&buffer[8], message, length);
					bool ret = SafeWrite(buffer, length + 8);
					delete[] buffer;
					return ret;
				}
			}
		}
		return false;
	}

	void audRemoteDebug::AddEntity(const audEntity * entity)
	{
		u32 data[2];
		data[0] = entity->GetControllerId();
		data[1] = audStringHash(entity->GetEntityName());
		SendMessage(DEBUG_ADDENTITY, (u8 *)data, 8);
	}

	void audRemoteDebug::RemoveEntity(const audEntity * entity)
	{
		u32 data = entity->GetControllerId();
		SendMessage(DEBUG_REMOVEENTITY, (u8 *)&data, 4);
	}

	void audRemoteDebug::SetRuntimePlatform()
	{
#if __XENON
		const char *platform = "XBOX360";
#endif
#if RSG_PC
		const char *platform =  "PC";
#endif
#if RSG_DURANGO
		const char *platform =  "XBOXONE";
#endif
#if RSG_ORBIS
		const char *platform =  "PS4";
#endif
#if __PPU
		const char *platform =  "PS3";
#endif

		u16 stringLen = (u16)strlen(platform);

		u8 buf[20];
		buf[0] = DEBUG_SETRUNTIMEPLATFORM;

		*((u32*)(&buf[1])) = sysEndian::NtoB(g_AudioEngine.GetTimeInMilliseconds());

		// Note: this is sent big endian, regardless of platform - audiodebugger can't perform endian conversion until after it has processed
		// the set runtime platform command.
		u16* sizePtr = (u16*)(&buf[5]);
		*sizePtr = sysEndian::NtoB(stringLen); // payload size: string length
		buf[7] = 0;

		sysMemCpy(&buf[8], platform, stringLen);

		m_Pipe.SafeWrite(buf, stringLen + 8);
		m_Pipe.SafeRead(&buf, 8);
		HandleReceivedPacket(buf[0], *sizePtr, NULL); 
	}

	bool audRemoteDebug::SafeWrite(const void* buffer, const size_t size)
	{
		size_t totalBytesWritten = 0;
		while (totalBytesWritten < size)
		{
			int bytesWritten = m_Pipe.Write(((u8*)buffer) + totalBytesWritten, size - totalBytesWritten);
			if (bytesWritten < 0)
			{
				return false;
			}
			else
			{
				totalBytesWritten += bytesWritten;
			}
		}
		return true;
	}

	void WidgetActivateEntityDebug()
	{
		g_AudioEngine.GetRemoteDebug().ActivateDebugType(ENTITY_DEBUG);
		audDisplayf("Activating Entity Debug");
	}

	void WidgetDeactivateEntityDebug()
	{
		g_AudioEngine.GetRemoteDebug().DeactivateDebugType(ENTITY_DEBUG);
		audDisplayf("Deactivating Entity debug");
	}

	void audRemoteDebug::AddWidgets(bkBank &bank)
	{
		bank.PushGroup("audRemoteDebug");
		bank.AddToggle("SendCategoryData", &sm_ShouldSendCategoryData);
		bank.AddButton("ActivateEntityDebug", datCallback(WidgetActivateEntityDebug));
		bank.AddButton("DeactivateEntityDebug", datCallback(WidgetDeactivateEntityDebug));
		bank.PopGroup();
	}
} // namespace rage

#endif // BANK
