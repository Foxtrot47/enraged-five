// 
// audioengine/remotecontrol.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_REMOTEDEBUG_H
#define AUD_REMOTEDEBUG_H

#if __BANK
#include "system/criticalsection.h"
#include "system/namedpipe.h"
#include "atl/array.h"


namespace rage
{
	class bkBank;
	class sysPipePacket;
	class audEntity;

	enum audRemoteDebugType
	{
		NULL_DEBUG_TYPE = 0, 
		ENTITY_DEBUG = BIT(0),
		CATEGORY_DEBUG = BIT(1),
	};

	enum audRemoteDebugCommands
	{
		// PURPOSE
		//	Sent by the game to keep the connection alive
		DEBUG_PING = 0,

		// PURPOSE
		//  Sent from audiodebugger to the game to acknowledge succesful processing of an xml message
		DEBUG_MESSAGERECEIPT,

		// PURPOSE
		//  Sent from the game to let the audiodebugger tool know the format of data that is being sent
		// PARAMS
		//	string platformTag
		DEBUG_SETRUNTIMEPLATFORM,

		// PURPOSE
		//    Sent by the game to update audiodebugger with resolved category settings
		// PARAMS
		//    u32 numCategories
		//    tCategorySettings categories[]
		DEBUG_RESOLVEDCATEGORYDATA,

		//PURPOSE
		//Sent from game to tell audiodebugger entity has been created
		DEBUG_ADDENTITY,

		//PURPOSE
		//Sent from game to tell audiodebugger entity has been removed
		DEBUG_REMOVEENTITY,

		// PURPOSE
		// Sent from the Visualizer to the game when an audio entity is selected
		DEBUG_ENTITY_SELECTED
	};

	// PURPOSE
	//	This class implements RPC functionality allowing the external debugger to analyse and,
	//	present audio debug information
	class audRemoteDebug
	{
	public:
		audRemoteDebug();
		~audRemoteDebug();

		// PURPOSE
		//	Should be called only once on Init. Generates a random number which will subsequently be passed down to the pipe
		void GenerateUniqueId();

		// PURPOSE
		//	This should be called once per audio frame - it polls the
		//	pipe for commands and executes them
		// PARAMS
		//	time - current time in milliseconds
		void Update(u32 time);

		// PURPOSE
		//	Initializes the named pipe
		bool Init();

		// PURPOSE
		//	Closes the connection
		void Shutdown();

		// PURPOSE
		//	Returns true if currently connected to audiodebugger
		bool IsConnected() const;

		// PURPOSE
		//	Returns true if running -audiodebugger (whether currently connected or not)
		static bool IsPresent();

		//PURPOSE
		//Checking, setting and unsetting which debug types are currently active
		//for remote debugging
		bool IsDebugTypeActive(audRemoteDebugType type)
		{
			return (type & m_ActiveTypes) != 0;
		}

		void ActivateDebugType(audRemoteDebugType type)
		{
			m_ActiveTypes = (audRemoteDebugType)((u32)m_ActiveTypes | (u32)type);
		}

		void DeactivateDebugType(audRemoteDebugType type)
		{
			m_ActiveTypes = (audRemoteDebugType)((u32)m_ActiveTypes & ~(u32)type);
		}

		//PURPOSE
		//Sends add/remove entity message to remote debugger
		void AddEntity(const audEntity * entity);
		void RemoveEntity(const audEntity * entity);

		// PURPOSE
		//	Adds widgets
		static void AddWidgets(bkBank &bank);

		// PURPOSE
		//	Sends a message to audiodebugger
		// RETURNS
		//	True if the message was succesfully sent to audiodebugger, false otherwise
		bool SendMessage(audRemoteDebugCommands command, const u8 *message, const u16 length);

		u32 GetLastMessageReceiptTime() const
		{
			return m_LastMessageReceiptTime;
		}

		enum { kRemoteDebugPayloadSizeBytes =  128 };

	private:

		// PURPOSE
		//	Packages and sends static resolved category data to the Visualizer tool
		bool SendCategoryData();

		void HandleReceivedPacket(const u8 packetType, const u32 payloadSize, u8 *payload);

		void SetRuntimePlatform();

		bool SafeWrite(const void* buffer, const size_t size);

		sysNamedPipe m_Pipe;
		// sysPipePacket *m_PipePacket;
		s32 m_UniqueId;
		audRemoteDebugType m_ActiveTypes;

		static u8 sm_PayloadBuf[kRemoteDebugPayloadSizeBytes];

		u32 m_LastMessageReceiptTime;
		sysCriticalSectionToken m_NetworkCS;

		static u32 	sm_LastSentMessage;
		static bool sm_ShouldSendCategoryData;
	};

} // namespace rage

#endif // BANK
#endif // AUD_REMOTEDEBUG_H
