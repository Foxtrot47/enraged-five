// 
// dynamixdefs.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure definitions - do not edit.
// 

#ifndef AUD_DYNAMIXDEFS_H
#define AUD_DYNAMIXDEFS_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "audioengine/metadataref.h"

// macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
	#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
	#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))
	namespace rage
	{
		enum TristateValue
		{
			AUD_TRISTATE_FALSE = 0,
			AUD_TRISTATE_TRUE,
			AUD_TRISTATE_UNSPECIFIED,
		}; // enum TristateValue
	} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE

namespace rage
{
	#define DYNAMIXDEFS_SCHEMA_VERSION 15
	
	#define DYNAMIXDEFS_METADATA_COMPILER_VERSION 2
	
	// NOTE: doesn't include abstract objects
	#define AUD_NUM_DYNAMIXDEFS 10
	
	#define AUD_DECLARE_DYNAMIXDEFS_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);
	
	#define AUD_DEFINE_DYNAMIXDEFS_FUNCTION(fn) \
	void* GetAddressOf_##fn(rage::u8 classId) \
	{ \
		switch(classId) \
		{ \
			case MixerPatch::TYPE_ID: return (void*)&MixerPatch::s##fn; \
			case AudSceneState::TYPE_ID: return (void*)&AudSceneState::s##fn; \
			case MixerScene::TYPE_ID: return (void*)&MixerScene::s##fn; \
			case MixGroup::TYPE_ID: return (void*)&MixGroup::s##fn; \
			case MixGroupList::TYPE_ID: return (void*)&MixGroupList::s##fn; \
			case DynamicMixModuleSettings::TYPE_ID: return (void*)&DynamicMixModuleSettings::s##fn; \
			case SceneVariableModuleSettings::TYPE_ID: return (void*)&SceneVariableModuleSettings::s##fn; \
			case SceneTransitionModuleSettings::TYPE_ID: return (void*)&SceneTransitionModuleSettings::s##fn; \
			case VehicleCollisionModuleSettings::TYPE_ID: return (void*)&VehicleCollisionModuleSettings::s##fn; \
			case MixGroupCategoryMap::TYPE_ID: return (void*)&MixGroupCategoryMap::s##fn; \
			default: return NULL; \
		} \
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gDynamicMixerGetBaseTypeId(const rage::u32 classId);
	
	// PURPOSE - Determines if a type inherits from another type
	bool gDynamicMixerIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId);
	
	// PURPOSE - Determines if a type inherits from another type
	template<class _ObjectType>
	bool gDynamicMixerIsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)
	{
		return gDynamicMixerIsOfType(obj->ClassId, baseTypeId);
	}
	
	// 
	// Enumerations
	// 
	enum MixModuleInput
	{
		INPUT_NONE = 0,
		INPUT_PLAYER_VEH_VELOCITY,
		INPUT_PLAYER_VEH_AIRTIME,
		INPUT_PLAYER_VEH_ROLL,
		INPUT_PLAYER_WANTED,
		VEH_VEH_SIDES,
		VEH_BUILDING_SIDES,
		NUM_MIXMODULEINPUT,
		MIXMODULEINPUT_MAX = NUM_MIXMODULEINPUT,
	}; // enum MixModuleInput
	const char* MixModuleInput_ToString(const MixModuleInput value);
	MixModuleInput MixModuleInput_Parse(const char* str, const MixModuleInput defaultValue);
	
	enum VolumeInvertInput
	{
		INPUT_INVERT = 0,
		INPUT_DO_NOTHING,
		NUM_VOLUMEINVERTINPUT,
		VOLUMEINVERTINPUT_MAX = NUM_VOLUMEINVERTINPUT,
	}; // enum VolumeInvertInput
	const char* VolumeInvertInput_ToString(const VolumeInvertInput value);
	VolumeInvertInput VolumeInvertInput_Parse(const char* str, const VolumeInvertInput defaultValue);
	
	// disable struct alignment
	#if !__SPU
	#pragma pack(push, r1, 1)
	#endif // !__SPU
		// 
		// MixerPatch
		// 
		enum MixerPatchFlagIds
		{
			FLAG_ID_MIXERPATCH_DISABLEDINPAUSEMENU = 0,
			FLAG_ID_MIXERPATCH_DOESPAUSEDTRANSITIONS,
			FLAG_ID_MIXERPATCH_MAX,
		}; // enum MixerPatchFlagIds
		
		struct MixerPatch
		{
			static const rage::u32 TYPE_ID = 0;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			MixerPatch() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				FadeIn(0),
				FadeOut(0),
				PreDelay(0.0f),
				Duration(0.0f),
				ApplyFactorCurve(219049753U), // LINEAR_RISE
				ApplyVariable(3898985960U), // apply
				ApplySmoothRate(0.0f)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::u16 FadeIn;
			rage::u16 FadeOut;
			float PreDelay;
			float Duration;
			rage::u32 ApplyFactorCurve;
			rage::u32 ApplyVariable;
			float ApplySmoothRate;
			
			static const rage::u8 MAX_MIXCATEGORIES = 32;
			rage::u8 numMixCategories;
			struct tMixCategories
			{
				rage::u32 Name;
				rage::s16 Volume;
				rage::u8 VolumeInvert;
				rage::u16 LPFCutoff;
				rage::u16 HPFCutoff;
				rage::s16 Pitch;
				float Frequency;
				rage::u8 PitchInvert;
				float Rolloff;
			} MixCategories[MAX_MIXCATEGORIES]; // struct tMixCategories
			
		} SPU_ONLY(__attribute__((packed))); // struct MixerPatch
		
		// 
		// AudSceneState
		// 
		enum AudSceneStateFlagIds
		{
			FLAG_ID_AUDSCENESTATE_MAX = 0,
		}; // enum AudSceneStateFlagIds
		
		struct AudSceneState
		{
			static const rage::u32 TYPE_ID = 1;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			AudSceneState() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			
			static const rage::u8 MAX_STATES = 8;
			rage::u8 numStates;
			struct tState
			{
				rage::u32 Name;
				rage::u32 Scene;
			} State[MAX_STATES]; // struct tState
			
		} SPU_ONLY(__attribute__((packed))); // struct AudSceneState
		
		// 
		// MixerScene
		// 
		enum MixerSceneFlagIds
		{
			FLAG_ID_MIXERSCENE_PLAYERCARDIALOGUEFRONTEND = 0,
			FLAG_ID_MIXERSCENE_STEALTHSCENE,
			FLAG_ID_MIXERSCENE_MUTEUSERMUSIC,
			FLAG_ID_MIXERSCENE_OVERRIDEFRONTENDSCENE,
			FLAG_ID_MIXERSCENE_CUSCENEAUDIOOVERRUNS,
			FLAG_ID_MIXERSCENE_CUTSCENEQUICKRELEASE,
			FLAG_ID_MIXERSCENE_CUTSCENETRIMMEDASSET,
			FLAG_ID_MIXERSCENE_KEEPMOBILERADIOACTIVE,
			FLAG_ID_MIXERSCENE_MAX,
		}; // enum MixerSceneFlagIds
		
		struct MixerScene
		{
			static const rage::u32 TYPE_ID = 2;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			MixerScene() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				ReferenceCount(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::s32 ReferenceCount;
			
			static const rage::u8 MAX_PATCHGROUPS = 16;
			rage::u8 numPatchGroups;
			struct tPatchGroup
			{
				rage::u32 Patch;
				rage::u32 MixGroup;
			} PatchGroup[MAX_PATCHGROUPS]; // struct tPatchGroup
			
		} SPU_ONLY(__attribute__((packed))); // struct MixerScene
		
		// 
		// MixGroup
		// 
		enum MixGroupFlagIds
		{
			FLAG_ID_MIXGROUP_OVERRIDEPARENT = 0,
			FLAG_ID_MIXGROUP_MAX,
		}; // enum MixGroupFlagIds
		
		struct MixGroup
		{
			static const rage::u32 TYPE_ID = 3;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			MixGroup() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				ReferenceCount(0),
				FadeTime(0.0f),
				Map(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::s32 ReferenceCount;
			float FadeTime;
			rage::u32 Map;
		} SPU_ONLY(__attribute__((packed))); // struct MixGroup
		
		// 
		// MixGroupList
		// 
		enum MixGroupListFlagIds
		{
			FLAG_ID_MIXGROUPLIST_MAX = 0,
		}; // enum MixGroupListFlagIds
		
		struct MixGroupList
		{
			static const rage::u32 TYPE_ID = 4;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			MixGroupList() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			
			static const rage::u8 MAX_MIXGROUPS = 255;
			rage::u8 numMixGroups;
			struct tMixGroup
			{
				rage::u32 MixGroupHash;
			} MixGroup[MAX_MIXGROUPS]; // struct tMixGroup
			
		} SPU_ONLY(__attribute__((packed))); // struct MixGroupList
		
		// 
		// DynamicMixModuleSettings
		// 
		enum DynamicMixModuleSettingsFlagIds
		{
			FLAG_ID_DYNAMICMIXMODULESETTINGS_MAX = 0,
		}; // enum DynamicMixModuleSettingsFlagIds
		
		struct DynamicMixModuleSettings
		{
			static const rage::u32 TYPE_ID = 5;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			DynamicMixModuleSettings() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				FadeIn(0),
				FadeOut(0),
				ApplyVariable(3898985960U), // apply
				Duration(0.0f),
				ModuleTypeSettings(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::u16 FadeIn;
			rage::u16 FadeOut;
			rage::u32 ApplyVariable;
			float Duration;
			rage::u32 ModuleTypeSettings;
		} SPU_ONLY(__attribute__((packed))); // struct DynamicMixModuleSettings
		
		// 
		// SceneVariableModuleSettings
		// 
		enum SceneVariableModuleSettingsFlagIds
		{
			FLAG_ID_SCENEVARIABLEMODULESETTINGS_MAX = 0,
		}; // enum SceneVariableModuleSettingsFlagIds
		
		struct SceneVariableModuleSettings
		{
			static const rage::u32 TYPE_ID = 6;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			SceneVariableModuleSettings() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				SceneVariable(3898985960U), // apply
				InputOutputCurve(219049753U), // LINEAR_RISE
				Input(INPUT_NONE),
				ScaleMin(0.0f),
				ScaleMax(10.0f)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::u32 SceneVariable;
			rage::u32 InputOutputCurve;
			rage::u8 Input;
			float ScaleMin;
			float ScaleMax;
		} SPU_ONLY(__attribute__((packed))); // struct SceneVariableModuleSettings
		
		// 
		// SceneTransitionModuleSettings
		// 
		enum SceneTransitionModuleSettingsFlagIds
		{
			FLAG_ID_SCENETRANSITIONMODULESETTINGS_MAX = 0,
		}; // enum SceneTransitionModuleSettingsFlagIds
		
		struct SceneTransitionModuleSettings
		{
			static const rage::u32 TYPE_ID = 7;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			SceneTransitionModuleSettings() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				Input(INPUT_NONE),
				Threshold(0.0f),
				Transition(3817852694U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::u8 Input;
			float Threshold;
			rage::u32 Transition;
		} SPU_ONLY(__attribute__((packed))); // struct SceneTransitionModuleSettings
		
		// 
		// VehicleCollisionModuleSettings
		// 
		enum VehicleCollisionModuleSettingsFlagIds
		{
			FLAG_ID_VEHICLECOLLISIONMODULESETTINGS_MAX = 0,
		}; // enum VehicleCollisionModuleSettingsFlagIds
		
		struct VehicleCollisionModuleSettings
		{
			static const rage::u32 TYPE_ID = 8;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			VehicleCollisionModuleSettings() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				Input(INPUT_NONE),
				Transition(3817852694U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::u8 Input;
			rage::u32 Transition;
		} SPU_ONLY(__attribute__((packed))); // struct VehicleCollisionModuleSettings
		
		// 
		// MixGroupCategoryMap
		// 
		enum MixGroupCategoryMapFlagIds
		{
			FLAG_ID_MIXGROUPCATEGORYMAP_MAX = 0,
		}; // enum MixGroupCategoryMapFlagIds
		
		struct MixGroupCategoryMap
		{
			static const rage::u32 TYPE_ID = 9;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			MixGroupCategoryMap() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			
			static const rage::u16 MAX_ENTRYS = 1024;
			rage::u16 numEntrys;
			struct tEntry
			{
				rage::u32 CategoryHash;
				rage::u32 ParentHash;
			} Entry[MAX_ENTRYS]; // struct tEntry
			
		} SPU_ONLY(__attribute__((packed))); // struct MixGroupCategoryMap
		
	// enable struct alignment
	#if !__SPU
	#pragma pack(pop, r1)
	#endif // !__SPU
} // namespace rage
#endif // AUD_DYNAMIXDEFS_H
