//
// audioengine/environment.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "engineconfig.h"
#include "curverepository.h"
#include "engine.h"
#include "enginedefs.h"
#include "environment.h"
#include "categorymanager.h"
#include "widgets.h"

#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/mixervoice.h"
#include "audiohardware/submix.h" 
#include "audiohardware/voicesettings.h"

#include "audioeffecttypes/delayeffect.h"
#include "audioeffecttypes/biquadfiltereffect.h"
#include "audioeffecttypes/reverbeffect.h"

#include "vectormath/vectormath.h"

#include "system/timemgr.h"
namespace rage
{
	using namespace Vec;
//
// PURPOSE
//	Defines the names of the curves used to control the mix to the source reverbs.
//
const char *g_SourceReverbCurveNames[g_NumSourceReverbs] =
{
	"REVERB_SMALL",
	"REVERB_MEDIUM",
	"REVERB_LARGE"
};

__THREAD u32 g_ThreadSpecificListenerIndex;

f32 g_VelocitySmoothRate = 0.1f;

f32 audEnvironment::sm_CloseAttenuationDistance = 10.f;
f32 audEnvironment::sm_FarAttenuationDistance = 30.f;

audEnvironment::audEnvironment()
{
}

audEnvironment::~audEnvironment()
{
}
#if !__SPU
void audEnvironment::Init()
{
	InitRoutes();
	InitRouteEffects();
	
	m_SpeedOfSoundMps = g_AudioEngine.GetConfig().GetSpeedOfSound();
	m_MaxDopplerRelativeVelMps = g_AudioEngine.GetConfig().GetMaximumRelativeVelocityForDoppler();

	m_ListenerGameThreadIndex = 1;
	m_ListenerAudioThreadIndex = 0;
	g_ThreadSpecificListenerIndex = m_ListenerGameThreadIndex;

	GameInit();
}

void audEnvironment::InitReverbCurves()
{
	//Prepare crossfade curves for source reverb submixes.
	for(u32 i=0; i<g_NumSourceReverbs; i++)
	{
		m_SourceReverbCrossfadeCurves[i].Init(g_SourceReverbCurveNames[i]);
	}
}

void audEnvironment::InitVolumeCurve(const char *curveName, const u32 curveId)
{
	Assert(curveId > 0);
	Assert(curveId < g_MaxCustomVolumeCurves);
	const u32 hash = atStringHash(curveName);
	m_VolumeCurves[curveId-1].Init(hash);
	m_VolumeCurveNameHashes[curveId-1] = hash;
}

u32 audEnvironment::FindVolumeCurveId(const u32 curveHash)
{	
	if(curveHash == ATSTRINGHASH("DEFAULT_ROLLOFF", 0x3BD35057))
	{
		return 0;
	}
		
	for(u32 i = 0; i < g_MaxCustomVolumeCurves; i++)
	{
		if(m_VolumeCurveNameHashes[i] == curveHash)
		{
			return i+1;
		}
	}
	return ~0U;
}

u32 audEnvironment::FindLPFCurveId(const u32 curveHash)
{
	if(curveHash == ATSTRINGHASH("DEFAULT_LPF_CURVE", 0x68BAA914))
	{
		return 0;
	}

	for(s32 i = 0; i < m_LPFCurveNames.GetCount(); i++)
	{
		if(m_LPFCurveNames[i] == curveHash)
		{
			return (u32)i+1;
		}
	}

	m_LPFCurveNames.Append() = curveHash;
	SetupCustomCurve(m_LPFCurves.Append(), curveHash);

	// We want to use index+1 as the Id
	return (u32)m_LPFCurves.GetCount();
}

u32 audEnvironment::FindHPFCurveId(const u32 curveHash)
{
	if(curveHash == ATSTRINGHASH("DEFAULT_HPF_CURVE", 0x2C30266B))
	{
		return 0;
	}

	for(s32 i = 0; i < m_HPFCurveNames.GetCount(); i++)
	{
		if(m_HPFCurveNames[i] == curveHash)
		{
			return (u32)i+1;
		}
	}

	m_HPFCurveNames.Append() = curveHash;
	SetupCustomCurve(m_HPFCurves.Append(), curveHash);

	// We want to use index+1 as the Id
	return (u32)m_HPFCurves.GetCount();
}

void audEnvironment::SetupCustomCurve(audThreePointPiecewiseLinearCurve &curve, const u32 curveNameHash)
{
	const PiecewiseLinear *curveData = g_AudioEngine.GetCurveRepository().GetMetadataManager().GetObject<PiecewiseLinear>(curveNameHash);
	if(audVerifyf(curveData, "Invalid custom curve requested with hash %u", curveNameHash))
	{
		if(audVerifyf(curveData->numPoints == 3, "Invalid custom curve requested with hash %u; has %u points instead of 3", curveNameHash, curveData->numPoints))
		{
			curve.Init( curveData->Point[0].x, curveData->Point[0].y,
						curveData->Point[1].x, curveData->Point[1].y,
						curveData->Point[2].x, curveData->Point[2].y);
		}
	}
}

bool audEnvironment::SetVolumeListenerMatrix(Mat34V_In listener, const u32 listenerId)
{
	Assert(listenerId<g_maxListeners);

	// Quite likely that this listener transform shinanigans will be done in most games' camera code,
	// but from an audio pov we're likely to move the mike, and hence need to calculate it again, so do
	// it here.;
	// TODO: (08/02/06 - MdS) check this works correctly once we have a happily running game with obvious co-ords,
	// I threw it together quite quickly, and my vector math's not what it once was... 
	GetListener(listenerId).VolumeMatrix = listener;
	if (!FPIsFinite(MagSquared(listener.GetCol0()).Getf()) || !FPIsFinite(MagSquared(listener.GetCol1()).Getf()) || !FPIsFinite(MagSquared(listener.GetCol2()).Getf()) || !FPIsFinite(MagSquared(listener.GetCol3()).Getf()))
	{
		AssertMsg(0, "Volume Listener non-finite - camera is almost certainly non-finite. This audio assert is ignorable");

		GetListener(listenerId).Contribution = 0.01f;// don't want to totally disable it, incase it's the only listener.
		return false;
	}
	return true;
}

bool audEnvironment::SetPanningListenerMatrix(Mat34V_In listener, const u32 listenerId)
{
	Assert(listenerId<g_maxListeners);
	Mat34V finalTransform = listener;
	if (FPIsFinite(MagSquared(listener.GetCol0()).Getf()) && FPIsFinite(MagSquared(listener.GetCol1()).Getf()) && FPIsFinite(MagSquared(listener.GetCol2()).Getf()) && FPIsFinite(MagSquared(listener.GetCol3()).Getf()))
	{
		Mat34V volList = listener;
		finalTransform.SetCol3(Vec3V(Dot(volList.GetCol0(), -volList.GetCol3()),
			Dot(volList.GetCol1(), -volList.GetCol3()),
			Dot(volList.GetCol2(), -volList.GetCol3())));
		Mat33V tempIn, tempOut;
		tempIn.SetCol0(finalTransform.GetCol0()); tempIn.SetCol1(finalTransform.GetCol1()); tempIn.SetCol2(finalTransform.GetCol2());
		Transpose(tempOut, tempIn);
		finalTransform.SetCol0(tempOut.GetCol0()); finalTransform.SetCol1(tempOut.GetCol1()); finalTransform.SetCol2(tempOut.GetCol2());
		GetListener(listenerId).PanningTransform = finalTransform;
		GetListener(listenerId).PanningMatrix = listener;
	}
	else
	{
		AssertMsg(0, "Panning Listener non-finite - camera is almost certainly non-finite. This audio assert is ignorable");

		finalTransform.SetIdentity3x3();
		GetListener(listenerId).PanningTransform = finalTransform;
		GetListener(listenerId).PanningMatrix = listener;
		GetListener(listenerId).Contribution =  0.01f; // don't want to totally disable it, incase it's the only listener.
		return false;
	}

	return true;
}

void audEnvironment::SetDecoder(const ambisonicDecodeData &data)
{
	g_AudioEngine.GetAmbisonics().CopySpeakerWeights(m_DecodeData.decoder, data.decoder);
}

void audEnvironment::CommitListenerSettings(u32 timeInMs)
{
	GameUpdate(timeInMs);

	u32 nextIndex = (m_ListenerGameThreadIndex+1)%g_audListenerBuffers;
	u32 previousIndex = (m_ListenerGameThreadIndex-1+g_audListenerBuffers)%g_audListenerBuffers;

	for (u32 i=0; i<g_maxListeners; i++)
	{
		audEnvironmentListenerData &gameThreadListener = m_Listeners[m_ListenerGameThreadIndex][i];
		audEnvironmentListenerData &gameThreadPreviousListener = m_Listeners[previousIndex][i];

		m_Listeners[nextIndex][i].Contribution = 0.f;		
		m_Listeners[nextIndex][i].CosConeAngle = 0.707f;		
		m_Listeners[nextIndex][i].CosRearConeAngle = -0.707f;		
		m_Listeners[nextIndex][i].CloseDirectionalMicAttenuation = -0.0f;	
		m_Listeners[nextIndex][i].FarDirectionalMicAttenuation = -0.0f;	
		m_Listeners[nextIndex][i].RearAttenuationType = 0;//REAR_ATTENUATION_DEFAULT;	
		m_Listeners[nextIndex][i].RollOff = 1.f;
		m_Listeners[nextIndex][i].JumpedThisFrame = false;
		gameThreadListener.UpdatedTime = timeInMs;
		// Set up the velocity in the current one.
		if ( gameThreadListener.JumpedThisFrame || 
			(gameThreadListener.Contribution == 0.0f) || 
			 (gameThreadPreviousListener.Contribution == 0.0f) || (gameThreadListener.UpdatedTime <= gameThreadPreviousListener.UpdatedTime))
		{
			// We don't have valid history, or time's moving backwards (network game) so give it a zero velocity
			gameThreadListener.Velocity.ZeroComponents();
			gameThreadListener.ActualSpeed = 0.0f;
		}
		else
		{
			// Work out our new velocity unless we've been told not to, otherwise, simply use the last frame's velocity
			const Vec3V& lastVelocity = gameThreadPreviousListener.Velocity;
			Vec3V velocity;
			/*if (ignoreVelocity)
				velocity = lastVelocity;
			else
			{
				velocity = m_VolumeListenerMatrices[i][m_ListenerGameThreadIndex].d - m_VolumeListenerPositionLastFrame[i];
				u32 timeStep = m_ListenerUpdatedTime[i][m_ListenerGameThreadIndex] - m_ListenerUpdatedTime[i][previousIndex];
				velocity.Scale(30.0f/((f32)timeStep));
			}*/

			velocity = gameThreadListener.VolumeMatrix.GetCol3() - 
				m_VolumeListenerPositionLastFrame[i];
			u32 timeStep = gameThreadListener.UpdatedTime - gameThreadPreviousListener.UpdatedTime;
			gameThreadListener.ActualSpeed = Mag(velocity).Getf() * TIME.GetInvSeconds();
			velocity = Scale(velocity, ScalarV(30.0f/((f32)timeStep)));

			//	If our actual speed difference between now and the last time is over 50 m/s, ignore the current velocity
			f32 fSpeedDiff = gameThreadListener.ActualSpeed - gameThreadPreviousListener.ActualSpeed;
			if (Abs(fSpeedDiff) > 50.0f)
				velocity = lastVelocity;

			gameThreadListener.Velocity = velocity;
			m_VolumeListenerPositionLastFrame[i] = gameThreadListener.VolumeMatrix.GetCol3();
		}
	}

	// We normalise the Contributions - have it so that the maximum contribution is 1, and all others are scaled in proportion.
	// We'll then just multiply this into the volume-adjusted contribution.
	f32 maxContribution = 0.0f;
	for (u32 i=0; i<g_maxListeners; i++)
	{
		maxContribution = Max(maxContribution, m_Listeners[m_ListenerGameThreadIndex][i].Contribution);
	}
	// Now go through again and scale them.
	Assert(maxContribution>0.0f);
	for (u32 i=0; i<g_maxListeners; i++)
	{

		m_Listeners[m_ListenerGameThreadIndex][i].Contribution /= maxContribution;
	}

	// We need to copy over a few of the things that are read on the game thread
	for (u32 i=0; i<g_maxListeners; i++)
	{
		audEnvironmentListenerData &currentListener = m_Listeners[m_ListenerGameThreadIndex][i];
		audEnvironmentListenerData &nextListener = m_Listeners[nextIndex][i];

		nextListener.PanningMatrix = currentListener.PanningMatrix;
		nextListener.PanningTransform = currentListener.PanningTransform;
		nextListener.VolumeMatrix = currentListener.VolumeMatrix;
		nextListener.Velocity = currentListener.Velocity;
		nextListener.ActualSpeed = currentListener.ActualSpeed;
		nextListener.Contribution = currentListener.Contribution;
		nextListener.CosConeAngle = currentListener.CosConeAngle;
		nextListener.CosRearConeAngle = currentListener.CosRearConeAngle;
		nextListener.CloseDirectionalMicAttenuation = currentListener.CloseDirectionalMicAttenuation;
		nextListener.FarDirectionalMicAttenuation = currentListener.FarDirectionalMicAttenuation;
		nextListener.RollOff = currentListener.RollOff;
		nextListener.RearAttenuationType = currentListener.RearAttenuationType;
	}

	m_ListenerGameThreadIndex = nextIndex;
	g_ThreadSpecificListenerIndex = m_ListenerGameThreadIndex;
}

#endif //!__SPU

Vec3V_Out audEnvironment::ComputePositionRelativeToPanningListener(Vec3V_In worldPosition, const u32 listenerId, const f32 positionRelativePan) const
{
	Assert(listenerId<g_maxListeners);

	Vec3V relativePosition = Transform(GetListener(listenerId).PanningTransform, worldPosition);;
	
	// now want to rotate that relative position around the vertical axis
	if (positionRelativePan >= 0.0f)
	{
		f32 panRadians = positionRelativePan * PI/180.0f; 
		relativePosition = RotateAboutZAxis(relativePosition, ScalarV(panRadians));
	}
	return relativePosition;
}
f32 audEnvironment::ComputeSqdDistanceToPanningListener(Vec3V_In worldPosition, const u32 listenerId)const
{
	Vec3V relativePosition = worldPosition - GetVolumeListenerMatrix(listenerId).GetCol3();
	return MagSquared(relativePosition).Getf();
}
f32 audEnvironment::ComputeDistanceToClosestPanningListener(Vec3V_In worldPosition) const
{
	// Loop through all active listeners and work out the distance, returning the smallest one.
	f32 smallestDistanceSqd = -1.0f;
	for (u32 i=0; i<g_maxListeners; i++)
	{
		if (IsListenerInUse(i))
		{
			f32 distanceSqd = ComputeSqdDistanceToPanningListener(worldPosition, i);
			smallestDistanceSqd = Selectf(smallestDistanceSqd, smallestDistanceSqd, distanceSqd); // makes sure it's not -1
			smallestDistanceSqd = Selectf(distanceSqd - smallestDistanceSqd, smallestDistanceSqd, distanceSqd); // takes the min
		}
	}
	// We must have at least one listener (must we?)
	Assert(smallestDistanceSqd >= 0.0f);
	return sqrt(smallestDistanceSqd);
}

f32 audEnvironment::ComputeDistanceToPanningListener(Vec3V_In worldPosition, const u32 listenerId) const
{
	Assert(listenerId<g_maxListeners);
	f32 distanceSqd = ComputeSqdDistanceToPanningListener(worldPosition, listenerId);
	return sqrt(distanceSqd);
}

f32 audEnvironment::GetDistanceAttenuation(const u32 curveId, f32 curveScale, f32 distance) const
{
	if (curveScale!=0.0f)
	{
		// If we have a >1 roll-off, we leave the 5m plateau, and only scale after that. 
		// For <1, we scale back the plateau too.
		// Lets you use long roll-offs without everything close being all mushy.
		// Use fsels for speed, and work out all three options.
		f32 tightNormalisedDistance = distance / curveScale;
		f32 wideFarExtraDistance = distance - 5.0f;
		f32 wideFarNormalisedDistance = 5.0f + (wideFarExtraDistance / curveScale);
		f32 wideCloseNormalisedDistance = distance;

		f32 wideNormalisedDistance = Selectf(distance-5.0f, wideFarNormalisedDistance, wideCloseNormalisedDistance);

		f32 normalizedDistance = Selectf(curveScale - 1.0f, wideNormalisedDistance, tightNormalisedDistance);

		if(curveId == 0)
		{
			// default, optimised polynomial
			return audCurve::DefaultDistanceAttenuation_CalculateValue(normalizedDistance);
		}
		else
		{
			// custom volume curve
			return m_VolumeCurves[curveId-1].CalculateValue(normalizedDistance);
		}
	}
	return 0.0f;
}
f32 audEnvironment::GetRollOff(u32 listenerId) const
{
	Assert(listenerId<g_maxListeners);
	return	GetListener(listenerId).RollOff;
}

f32 audEnvironment::GetDirectionalMicAttenuation(u32 listenerId,Vec3V_In relativePosition,f32 distanceToPanningListener, f32 *frontBackRatio) const
{
	Vec3V vFront = Vec3V(V_Y_AXIS_WZERO);
	Vec3V relativePositionNormalised = Normalize(relativePosition);

	f32 cosAngle = Dot(relativePositionNormalised, vFront).Getf();
	if (GetListener(listenerId).CosConeAngle == -1)
	{
		// We don't really have a cone at all, just normal non-directional style. 
		if (frontBackRatio)
		{
			*frontBackRatio = 1.0f;
		}
		return (0.0);
	}
	else if (cosAngle >= GetListener(listenerId).CosConeAngle)
	{
		// We're inside the cone, so no attenuation.
		if (frontBackRatio)
		{
			*frontBackRatio = 1.0f;
		}
		return (0.0);
	}
	else if (cosAngle <= GetListener(listenerId).CosRearConeAngle)
	{
		// We're inside the rear cone, so max attenuation.
		if (frontBackRatio)
		{
			*frontBackRatio = 0.0f;
		}
		f32 directionalMicAtt = Lerp(ClampRange(distanceToPanningListener,sm_CloseAttenuationDistance,sm_FarAttenuationDistance)
			,audDriverUtil::ComputeLinearVolumeFromDb(GetListener(listenerId).CloseDirectionalMicAttenuation)
			,audDriverUtil::ComputeLinearVolumeFromDb(GetListener(listenerId).FarDirectionalMicAttenuation));
		return audDriverUtil::ComputeDbVolumeFromLinear(directionalMicAtt); 
	}
	else
	{
		// Inbetween the front and rear cones.
		// Scale our cos to be in range [1, 0], between rear cone and front cone.  
		f32 translatedRatio = (GetListener(listenerId).CosConeAngle - cosAngle)/(GetListener(listenerId).CosConeAngle - GetListener(listenerId).CosRearConeAngle);
		if (frontBackRatio)
		{
			*frontBackRatio = (1.0f - translatedRatio);
		}
		f32 closeDistAtt = (translatedRatio * GetListener(listenerId).CloseDirectionalMicAttenuation);
		f32 farDistAtt = (translatedRatio * GetListener(listenerId).FarDirectionalMicAttenuation);
		f32 directionalMicAtt = Lerp(ClampRange(distanceToPanningListener,sm_CloseAttenuationDistance,sm_FarAttenuationDistance)
			,audDriverUtil::ComputeLinearVolumeFromDb(closeDistAtt)
			,audDriverUtil::ComputeLinearVolumeFromDb(farDistAtt));
		return audDriverUtil::ComputeDbVolumeFromLinear(directionalMicAtt); 
	}
}

void audEnvironment::SetDirectionalMicParameters(f32 frontCosConeAngle, f32 rearCosConeAngle, f32 closeAttenuation, f32 farAttenuation,const u32 rearAttenuationType,u32 listenerId)
{
	Assert(listenerId<g_maxListeners);
	GetListener(listenerId).CosConeAngle = frontCosConeAngle;
	GetListener(listenerId).CosRearConeAngle = rearCosConeAngle;
	GetListener(listenerId).CloseDirectionalMicAttenuation = closeAttenuation;
	GetListener(listenerId).FarDirectionalMicAttenuation = farAttenuation;
	GetListener(listenerId).RearAttenuationType = rearAttenuationType;
}
void audEnvironment::SetRollOff(f32 rollOff,u32 listenerId)
 {
	 Assert(listenerId<g_maxListeners);
	 GetListener(listenerId).RollOff = rollOff;
}
f32 audEnvironment::ComputeDopplerFrequencyScalingFactor(f32 relativeDistanceDelta, u32 timeStepMs, f32 dopplerFactor) const
{
	if(timeStepMs == 0)
	{
		return 1.f;
	}
	//TODO: Deal with large frequency offsets caused by camera position switching.
	f32 relativeFrequencyGT, relativeFrequencyLT;
	f32 relativeVelocityMps = dopplerFactor * relativeDistanceDelta / (timeStepMs / 1000.f);

	relativeFrequencyGT = (m_SpeedOfSoundMps / (m_SpeedOfSoundMps + Min(m_MaxDopplerRelativeVelMps,
		relativeVelocityMps)));

	relativeFrequencyLT = (m_SpeedOfSoundMps / (m_SpeedOfSoundMps + Max(-m_MaxDopplerRelativeVelMps,
		relativeVelocityMps)));
	
	return Selectf(relativeVelocityMps, relativeFrequencyGT, relativeFrequencyLT);
}

// PURPOSE
// Describes in 3D space the positions of 5 virtual speakers
// that "float" around the listener on the circumference of unit circle
// currently all the speakers are positioned so they exist on the same horizontal
// plane but that they can be portioned anywhere to create interesting panning effects.
// The number of speakers the algorithm can pan between is variable. 
typedef struct  
{
	f32 x, y, z;
} audSpeakerPosition;

#if !AUD_GAME_OVERRIDDEN_ENVIRONMENT
#if !__PPU
static const audSpeakerPosition g_SpeakerPositions[] =
{
	{ -0.7071f,  0.7071f, 0.0f}, // FL
	{  0.7071f,  0.7071f, 0.0f}, // FR
						
	{  0.0f,     1.0f   , 0.0f}, // C
	{  0.0f,	 0.0f	, 0.0f}, // LFE
						
	{ -0.7071f, -0.7071f, 0.0f}, // BL
	{  0.7071f, -0.7071f, 0.0f},  // BR
						
	{ -0.7071f, -0.7071f, 0.0f}, // BL
	{  0.7071f, -0.7071f, 0.0f},  // BR
};
#else
// PSN speakers are in a weird order
static const audSpeakerPosition g_SpeakerPositions[] =
{
	{ -0.7071f,   0.7071f, 0.0f}, // FL
	{  0.7071f,   0.7071f, 0.0f}, // FR
	{  0.0f,      1.0f   , 0.0f}, // C
						  
	{ -0.7071f,   0.7071f, 0.0f}, // ML
	{  0.7071f,   0.7071f, 0.0f}, // MR
						  
	{ -0.7071f,  -0.7071f, 0.0f}, // BL
	{  0.7071f,  -0.7071f, 0.0f},  // BR
	{  0.0f,		0.0f , 0.0f	}, // LFE
};

#endif

// We also need a 2-channel mapping, otherwise sounds in the rear are quieter
static const audSpeakerPosition g_SpeakerPositionsTwoChannel[] =
{
	{ -1.0f, 0.0f,  0.0f }, // FL
	{  1.0f, 0.0f,  0.0f }, // FR
};
#endif  //!AUD_GAME_OVERRIDDEN_ENVIRONMENT

#if !AUD_GAME_OVERRIDDEN_ROUTING

void audEnvironment::InitRoutes()
{

	audDriver::GetMixer()->EnterBML();

	audMixerSubmix *SFXSubmix = audDriver::GetMixer()->CreateSubmix("SFX", audDriver::GetNumOutputChannels(), false);
	Assign(m_SFXSubmixId, audDriver::GetMixer()->GetSubmixIndex(SFXSubmix));
	SFXSubmix->AddOutput(audDriver::GetMixer()->GetMasterSubmix());	

	audMixerSubmix *musicSubmix = audDriver::GetMixer()->CreateSubmix("Music", Min(4, audDriver::GetNumOutputChannels()), true);
	Assign(m_MusicSubmixId, audDriver::GetMixer()->GetSubmixIndex(musicSubmix));
	musicSubmix->AddOutput(audDriver::GetMixer()->GetMasterSubmix());	

	audMixerSubmix *frontendSubmix = audDriver::GetMixer()->CreateSubmix("Frontend", audDriver::GetNumOutputChannels(), true);
	Assign(m_FrontendSubmixId, audDriver::GetMixer()->GetSubmixIndex(frontendSubmix));
	frontendSubmix->AddOutput(SFXSubmix);
	
	audMixerSubmix *listenerSubmix = audDriver::GetMixer()->CreateSubmix("Listener", audDriver::GetNumOutputChannels(), false);
	Assign(m_ListenerSubmixId, audDriver::GetMixer()->GetSubmixIndex(listenerSubmix));
	listenerSubmix->AddOutput(SFXSubmix);

	audMixerSubmix *reverbSubmixes[3];
	reverbSubmixes[0] = audDriver::GetMixer()->CreateSubmix("Source Reverb Small", Min(4, audDriver::GetNumOutputChannels()), true);
	reverbSubmixes[1] = audDriver::GetMixer()->CreateSubmix("Source Reverb Medium", Min(4, audDriver::GetNumOutputChannels()), true);
	reverbSubmixes[2] = audDriver::GetMixer()->CreateSubmix("Source Reverb Large", Min(4, audDriver::GetNumOutputChannels()), true);	

	for(s32 i = 0; i < 3; i++)
	{
		Assign(m_ReverbSubmixIds[i], audDriver::GetMixer()->GetSubmixIndex(reverbSubmixes[i]));
		reverbSubmixes[i]->AddOutput(listenerSubmix);
	}

	audDriver::GetMixer()->ExitBML();
}

#if !AUD_GAME_OVERRIDDEN_ROUTE_EFFECTS

void audEnvironment::InitRouteEffects()
{

}

#endif // AUD_GAME_OVERRIDDEN_ROUTE_EFFECTS

void audEnvironment::ComputeVoiceRoutes(audEnvironmentMetricsInternal *metrics) const
{
	const f32 linearVolume = Clamp(metrics->volume.GetLinear() * g_HeadroomOffsetCompensationLin, 0.f, 1.f);
	
	const f32 sourceEffectWetMix = linearVolume * metrics->environmentSoundMetrics->routingMetric.sourceEffectWetMix;
	const f32 sourceEffectDryMix = linearVolume * metrics->environmentSoundMetrics->routingMetric.sourceEffectDryMix;

	const f32 postSubmixAttenLin = metrics->postSubmixVolumeAttenuation.GetLinear();
	//Apply 2dB offset to compensate for headroom adjustment in EnvSound.
	const f32 dryMix = sourceEffectDryMix * g_HeadroomOffsetCompensationLin;//audDriverUtil::SmoothVolume(voiceSettings->SourceEffectDryMix.GetLinear() * g_HeadroomOffsetCompensationLin, m_DryMix, shouldSmooth); 
	//const f32 wetMix = sourceEffectWetMix * g_HeadroomOffsetCompensationLin;//audDriverUtil::SmoothVolume(voiceSettings->SourceEffectWetMix.GetLinear() * g_HeadroomOffsetCompensationLin, m_WetMix, shouldSmooth);


	f32 dryChannelVolumes[g_MaxOutputChannels];
	f32 wetChannelVolumes[g_MaxOutputChannels][g_NumSourceReverbs];

	// Calculate our new channel volumes
	for(u32 i=0, reverbIndex = 0; i<g_MaxOutputChannels; i++)
	{
		f32 newDryChannelVolume = 0.0f;
		f32 newWetChannelVolume[g_NumSourceReverbs];
		for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
		{
			newWetChannelVolume[j] = 0.0f;
		}
		// Initialise the new ones to our desired volumes
		newDryChannelVolume = postSubmixAttenLin * metrics->relativeDryChannelVolumes[i].GetLinear();
		if(i != RAGE_SPEAKER_ID_FRONT_CENTER && i != RAGE_SPEAKER_ID_LOW_FREQUENCY)
		{
			for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
			{
				newWetChannelVolume[j] = postSubmixAttenLin * metrics->relativeWetChannelVolumes[reverbIndex][j].GetLinear();
			}
			reverbIndex++;
		}

		dryChannelVolumes[i] = newDryChannelVolume;
		for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
		{
			wetChannelVolumes[i][j] = newWetChannelVolume[j];
		}
	}

	u32 numDirectDestinations = 0;
	s32 directDestinationSubmixIds[g_MaxVoiceRouteDestinations];
	sysMemSet(directDestinationSubmixIds, 0xff, sizeof(directDestinationSubmixIds));
	f32 directChannelVolumes[g_MaxVoiceRouteDestinations][g_MaxOutputChannels];
	sysMemSet(directChannelVolumes, 0, g_MaxVoiceRouteDestinations * g_MaxOutputChannels * sizeof(float));

	s32 sourceEffectDestinationSubmixIds[g_MaxVoiceRouteDestinations];
	sysMemSet(sourceEffectDestinationSubmixIds, 0xff, sizeof(sourceEffectDestinationSubmixIds));
	f32 sourceEffectOutputChanVols[g_MaxVoiceRouteDestinations][g_MaxOutputChannels];
	sysMemSet(sourceEffectOutputChanVols, 0, g_MaxVoiceRouteDestinations * g_MaxOutputChannels * sizeof(float));

	s32 destinationRouteId = -1;

	const u32 effectRoute = metrics->environmentSoundMetrics->routingMetric.effectRoute;
	
	switch(effectRoute)
	{
	case EFFECT_ROUTE_MUSIC:
		destinationRouteId = m_MusicSubmixId;
		if(destinationRouteId != -1)
		{
			numDirectDestinations = 1;

			directDestinationSubmixIds[0] = m_MusicSubmixId;
			// NOTE:  this is routing sourceEffectDestination to the same directDestination bus
			sourceEffectDestinationSubmixIds[0] = directDestinationSubmixIds[0];

			for(u32 i=0; i<g_MaxOutputChannels; i++)
			{
				// if we are NOT routing to a custom submix then apply postsubmix to direct
				directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
				sourceEffectOutputChanVols[0][i] = dryChannelVolumes[i];
			}
		}
		break;

	case EFFECT_ROUTE_FRONT_END:
		destinationRouteId = m_FrontendSubmixId;
		if(destinationRouteId != -1)
		{
			numDirectDestinations = 1;

			// if we are NOT routing to a custom submix then apply postsubmix to direct
			directDestinationSubmixIds[0] = destinationRouteId;
			// NOTE: this is routing sourceEffectDestination to the same bus as directDestination
			sourceEffectDestinationSubmixIds[0] = directDestinationSubmixIds[0];

			for(u32 i=0; i<g_MaxOutputChannels; i++)
			{
				directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
				sourceEffectOutputChanVols[0][i] = dryChannelVolumes[i];
			}
		}
		break;

	case EFFECT_ROUTE_POSITIONED: //Intentional fall-through.
	default:
		s32 destSubmixIds[g_NumSourceReverbs + 1] = 
		{
			m_ListenerSubmixId,
			m_ReverbSubmixIds[0],
			m_ReverbSubmixIds[1],
			m_ReverbSubmixIds[2]
		};

		for(u32 i=0; i<g_NumSourceReverbs + 1; i++)
		{
			destinationRouteId = destSubmixIds[i];
			if(destinationRouteId != -1)
			{
				directDestinationSubmixIds[i] =	destinationRouteId;
				sourceEffectDestinationSubmixIds[i] = directDestinationSubmixIds[i];
			}
		}

		numDirectDestinations = g_NumSourceReverbs + 1;

		// the PRE_LISTENER route:
		for(u32 i=0; i<g_MaxOutputChannels; i++)
		{
			// if we are NOT routing to a custom submix then apply postSubmixAtten on the direct route
			directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
			sourceEffectOutputChanVols[0][i] = dryChannelVolumes[i];
		}

		// the three SOURCE_REVERB routes
		for(u32 i=1; i<g_NumSourceReverbs + 1; i++)
		{
			for(u32 j=0; j<g_MaxOutputChannels; j++)
			{
				// if we are NOT routing to a custom submix then apply postSubmixAtten on the direct route
				directChannelVolumes[i][j] = dryMix * wetChannelVolumes[j][i - 1];
				sourceEffectOutputChanVols[i][j] = wetChannelVolumes[j][i - 1];
			}
		}
		break;
	}

	Assert(numDirectDestinations<=audVoiceSettings::kMaxVoiceRouteDestinations);
	for(u32 i=0; i< audVoiceSettings::kMaxVoiceRouteDestinations; i++)
	{
		if(i < numDirectDestinations)
		{
			metrics->environmentSoundMetrics->voiceSettings->Routes[i].SetSubmixId(directDestinationSubmixIds[i]);
			metrics->environmentSoundMetrics->voiceSettings->Routes[i].invertPhase = metrics->environmentSoundMetrics->shouldInvertPhase;
			ALIGNAS(16) f32 gainMatrix[8] ;
			for(u32 channelIndex = 0; channelIndex < g_MaxOutputChannels; channelIndex++)
			{
				gainMatrix[channelIndex] = directChannelVolumes[routeIndex][channelIndex];			
			}

			metrics->environmentSoundMetrics->voiceSettings->RouteChannelVolumes[routeIndex] = 
				audMixerConnection::PackVolumeMatrix(gainMatrix);

		}
		else
		{
			metrics->environmentSoundMetrics->voiceSettings->Routes[i].SetSubmixId(-1);
		}
	}

	// also compute the virtualisation score
	const f32 totalVolume =  (sourceEffectDryMix + sourceEffectWetMix) * postSubmixAttenLin;
	metrics->virtualisationScore = ~0U - (u32)(totalVolume* f32(~0U));
}

audMixerSubmix *audEnvironment::GetMasterSubmix() const
{
	return audDriver::GetMixer()->GetMasterSubmix();
}

audMixerSubmix *audEnvironment::GetSfxSubmix() const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_SFXSubmixId);
}

audMixerSubmix *audEnvironment::GetMusicSubmix() const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_MusicSubmixId);
}

audMixerSubmix *audEnvironment::GetSourceReverbSubmix(const u32 index) const
{
	Assert(index < 3);
	return audDriver::GetMixer()->GetSubmixFromIndex(m_ReverbSubmixIds[index]);
}
#endif

#if !AUD_GAME_OVERRIDDEN_ENVIRONMENT

void audEnvironment::GameInit()
{
	
}

#if RSG_BANK
void audEnvironment::AddGameWidgets(bkBank &UNUSED_PARAM(bank))
{

}
#endif // RSG_BANK

void audEnvironment::ComputeSpeakerVolumesFromPosition(Vec3V_In relativePosition, audEnvironmentMetricsInternal *metrics, const u32 numChannels)
{
	const audSpeakerPosition*	speakers			= g_SpeakerPositions;
	Vec3V						normPos;
	Vec3V						speakerNorm;
	f32							speakerMag;
	f32							speakerFactor;

	// Use the appropriate speaker positions - different for different numbers of channels.
	if (numChannels == 2)
	{
		speakers = g_SpeakerPositionsTwoChannel;
	}

	// This Assert's no longer valid, as we just have a ptr to an array now, and can't tell its size.
	// Assert(numChannels <= sizeof(*speakers) / sizeof(audSpeakerPosition));

	normPos = Normalize(relativePosition);

	// Calculate volume for each speaker.
	// Distance and rolloff attenuation are taken care of in a different
	// function using different data members, so this is strictly pan. 
	// Note that this particular implementation calculates speaker volumes
	// independently of each other. For example when the source is directly in front
	// of the listener, we don't bleed volume from the front left and right speakers
	for(u32 i = 0; i < numChannels; i++) 
	{
		if(i == RAGE_SPEAKER_ID_LOW_FREQUENCY)
		{
			// LFE is handled as a special case
			speakerFactor = 1.0f;
		}
		else if(i == RAGE_SPEAKER_ID_FRONT_CENTER)
		{
			speakerFactor = 0.0f;
		}
		else
		{
			speakerNorm.SetXf((normPos.GetXf() - speakers[i].x ) / 2);
			speakerNorm.SetYf((normPos.GetYf() - speakers[i].y ) / 2);
			speakerNorm.SetYf((normPos.GetZf() - speakers[i].z ) / 2);

			speakerMag =(f32)(speakerNorm.GetXf() * speakerNorm.GetXf() + 
				speakerNorm.GetYf() * speakerNorm.GetYf() + 
				speakerNorm.GetZf() * speakerNorm.GetZf());
			speakerFactor = fabsf((1.0f - speakerMag));

			Assert((speakerFactor >= 0.0f) && (speakerFactor <= 1.0f));
		}

		metrics->relativeChannelVolumes[i] = speakerFactor;
	}
}


void audEnvironment::ComputeSpeakerVolumesFromPan(audEnvironmentMetricsInternal *metrics, const u32 numChannels)
{
	Vector3 vec;
	vec = ComputePositionFromPan(metrics->environmentSoundMetrics->pan);
	ComputeSpeakerVolumesFromPosition(vec, metrics, numChannels);
}

void audEnvironment::CalculateEnvironmentEffectsMetricsForSingleListener(audEnvironmentMetricsInternal *UNUSED_PARAM(metrics), u32 UNUSED_PARAM(listenerId))
{
	return;
}

void audEnvironment::CalculateEnvironmentSoundMetricsForSingleListener(audEnvironmentMetricsInternal *metrics, u32 listenerId)
{
	// Do all the regular stuff - volume attenuation, doppler calculation, etc. Then call another overridable function for the fun occlusion/reverb stuff.
	// We create a new inputMetric and outputMetric, which we pass into that other fn, then combine.
	// I don't particularly like that. (MdS)

	//Convert world-relative position to listener-relative.
	Vector3 positionRelativeToVolumeListener = ComputePositionRelativeToVolumeListener(metrics->environmentSoundMetrics->position, listenerId);
	Vector3 positionRelativeToPanningListener = ComputePositionRelativeToPanningListener(metrics->environmentSoundMetrics->position, listenerId);
	f32 distanceRelativeToVolumeListener = positionRelativeToVolumeListener.MagSafe();

	f32 distanceAttenuation = 0.0f;
	f32 directionalMicAttenuation = 0.0f;

	if(metrics->environmentSoundMetrics->shouldAttenuateOverDistance)
	{
		f32 scale = metrics->environmentSoundMetrics->volumeCurveScale;
		scale *= g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].DistanceRollOffScale.GetFloat32_FromFloat16();

		distanceAttenuation = GetDistanceAttenuation(metrics->environmentSoundMetrics->volumeCurveId, scale,distanceRelativeToVolumeListener);
		// Also work out directional mic attenuation here, as the two sensibly go together
		directionalMicAttenuation = GetDirectionalMicAttenuation(positionRelativeToPanningListener);
	}

	// Update our metrics with the calculated volume changes
	metrics->postSubmixVolumeAttenuation.ScaleLinear(audDriverUtil::ComputeLinearVolumeFromDb(distanceAttenuation + directionalMicAttenuation));

	// Now we do the fancy environmental stuff
	if (metrics->environmentSoundMetrics->shouldApplyEnvironmentalEffects)
	{
		CalculateEnvironmentEffectsMetricsForSingleListener(metrics, listenerId);
	}

	// Calculate the channel volumes last, because it might depend on source reverb levels, final volume, etc
	if(metrics->environmentSoundMetrics->speakerMask != 0)
	{
		ComputeSpeakerVolumesFromSpeakerMask(metrics, audDriver::GetNumOutputChannels());
		metrics->environmentSoundMetrics->dopplerFrequencyScalingFactor = 1.0f;
	}
	else if(metrics->environmentSoundMetrics->pan >= 0)
	{
		ComputeSpeakerVolumesFromPan(metrics, audDriver::GetNumOutputChannels());
		metrics->environmentSoundMetrics->dopplerFrequencyScalingFactor = 1.0f;
	}
	else
	{
		ComputeSpeakerVolumesFromPosition(positionRelativeToPanningListener, metrics, audDriver::GetNumOutputChannels());
	}

	CopyDryVolumesToReverbs(metrics);

	//Compute doppler frequency scaling.		
	if(metrics->environmentSoundMetrics->positionHasUpdated && metrics->environmentSoundMetrics->shouldAttenuateOverDistance)
	{
		// Work out our estimated position last frame 30ms ago
		Vec3V position30msAgo = metrics->environmentSoundMetrics->position - metrics->environmentSoundMetrics->velocity;

		Vec3V cameraVelocity = GetListener(listenerId).Velocity;
		Vec3V cameraPosition = GetListener(listenerId).VolumeMatrix.GetCol3();
		Vec3V cameraPosition30msAgo = cameraPosition - cameraVelocity;

		f32 distanceForDoppler = MagSafe(metrics->environmentSoundMetrics->position - cameraPosition).Getf();
		f32 distanceForDopplerLastFrame = MagSafe(position30msAgo - cameraPosition30msAgo).Getf();

		f32 relativeDistanceDelta = distanceForDoppler - distanceForDopplerLastFrame;
		metrics->environmentSoundMetrics->dopplerFrequencyScalingFactor = ComputeDopplerFrequencyScalingFactor(relativeDistanceDelta, 30,
			metrics->environmentSoundMetrics->dopplerFactor);
	}

	// turn volume routing metrics into actual route volumes
	ComputeVoiceRoutes(metrics);
}

#endif // !AUD_GAME_OVERRIDDEN_ENVIRONMENT

Vec3V_Out audEnvironment::ComputePositionFromPan(const s32 panInDegrees)
{	
#if 0
	if(panInDegrees == 90)
	{
		return Vec3V(V4VConstant<U32_ONE, U32_ZERO, U32_ZERO, U32_ZERO>());
	}
	else if(panInDegrees == 270)
	{
		return Vec3V(V4VConstant<U32_NEGONE, U32_ZERO, U32_ZERO, U32_ZERO>());
	}
	else if(panInDegrees == 307)
	{
		return Vec3V(V4VConstant<0xBF4C7360, 0x3F1A108D, U32_ZERO, U32_ZERO>());
	}
	else if(panInDegrees == 53)
	{
		return Vec3V(V4VConstant<0x3F4C7360, 0x3F1A108D, U32_ZERO, U32_ZERO>());
	}
#endif

	ScalarV pan(V4IntToFloatRaw<0>(V4LoadScalar32IntoSplatted(panInDegrees)));
	// convert pan to position
	// angle = (450 - pan) % 360 * PI/180
	// x = cos(angle)
	// y = sin(angle)
	const ScalarV fourFifty(V4VConstantSplat<0x43E10000>());
	const ScalarV threeSixty(V4VConstantSplat<0x43B40000>());

	ScalarV angle = Modulus(fourFifty - pan, threeSixty) * ScalarV(V_TO_RADIANS);

	ScalarV vSin, vCos;
	SinAndCos(vSin, vCos, angle);

	Vector_4V v = V4PermuteTwo<X1,X2,W1,W1>(V4And(V4VConstant(V_MASKXY), vCos.GetIntrin128ConstRef()), vSin.GetIntrin128ConstRef());
	
	return Vec3V(v);
}


Vec3V_Out audEnvironment::ComputePositionFromPan(const f32 panInDegrees)
{	
	ScalarV pan(panInDegrees);
	// convert pan to position
	// angle = (450 - pan) % 360 * PI/180
	// x = cos(angle)
	// y = sin(angle)
	const ScalarV fourFifty(V4VConstantSplat<0x43E10000>());
	const ScalarV threeSixty(V4VConstantSplat<0x43B40000>());

	ScalarV angle = Modulus(fourFifty - pan, threeSixty) * ScalarV(V_TO_RADIANS);

	ScalarV vSin, vCos;
	SinAndCos(vSin, vCos, angle);

	Vector_4V v = V4PermuteTwo<X1,X2,W1,W1>(V4And(V4VConstant(V_MASKXY), vCos.GetIntrin128ConstRef()), vSin.GetIntrin128ConstRef());

	return Vec3V(v);
}

#ifndef AUD_GAME_OVERRIDE_ComputeSpeakerVolumesFromSpeakerMask
void audEnvironment::ComputeSpeakerVolumesFromSpeakerMask(audEnvironmentMetricsInternal *metrics, const u32 numChannels)
{
	f32 vols[g_MaxOutputChannels];
	u32 speakersUsed = 0;

	u32 speakerMask = metrics->environmentSoundMetrics->speakerMask;

	u32 mask = 1;
 
	// Map SpeakerMask to RAGE speaker order: FL,FR,C,LFE,SL,SR -> FL,FR,SL,SR,C,LFE
	const s32 speakerMap[] = {	RAGE_SPEAKER_ID_FRONT_LEFT,
								RAGE_SPEAKER_ID_FRONT_RIGHT,
								RAGE_SPEAKER_ID_FRONT_CENTER,
								RAGE_SPEAKER_ID_LOW_FREQUENCY,
								RAGE_SPEAKER_ID_BACK_LEFT,
								RAGE_SPEAKER_ID_BACK_RIGHT
	};
	for(u32 i = 0 ; i < numChannels; i++)
	{
		if(speakerMask & mask)
		{
			vols[speakerMap[i]] = 1.0f;
			speakersUsed++;
		}
		else
		{
			vols[speakerMap[i]] = 0.0f;
		}
		mask <<= 1;
	}

	// Special case two-channel setup, when routed to the center speaker
	if (numChannels == 2)
	{
		// NOTE: Metadata speaker mask doesn't match RAGE speaker masks
		const u32 centreSpeakerMask = 1 << 2;
	
		if(speakerMask & centreSpeakerMask)
		{
			vols[RAGE_SPEAKER_ID_FRONT_LEFT] = 1.0f;
			vols[RAGE_SPEAKER_ID_FRONT_RIGHT] = 1.0f;
			speakersUsed = 2;
		}
	}

	f32 sqrtSpeakers = sqrt((f32)speakersUsed);

	for(u32 i = 0; i < g_MaxOutputChannels; i++)
	{
		if(speakersUsed > 0 && i < numChannels)
		{
			metrics->relativeChannelVolumes[i] = vols[i] / sqrtSpeakers;
		}
		else
		{
			metrics->relativeChannelVolumes[i] = 0.f;
		}	
	}
}
#endif // AUD_GAME_OVERRIDE_ComputeSpeakerVolumesFromSpeakerMask

void audEnvironment::ComputeSpeakerVolumesFromQuadLevels(audEnvironmentMetricsInternal *metrics, const u32 numChannels)
{
	const Vec3V &positionData = metrics->environmentSoundMetrics->position;

	for(u32 i = 0; i < g_MaxOutputChannels; i++)
	{
		if(i < numChannels)
		{
			switch(i)
			{
			case RAGE_SPEAKER_ID_FRONT_LEFT:
				metrics->relativeChannelVolumes[i] = 1.0f * Clamp(positionData.GetXf(), 0.0f, 1.0f);
				break;
			case RAGE_SPEAKER_ID_FRONT_RIGHT:
				metrics->relativeChannelVolumes[i] = 1.0f * Clamp(positionData.GetYf(), 0.0f, 1.0f);
				break;
			case RAGE_SPEAKER_ID_BACK_LEFT:
				metrics->relativeChannelVolumes[i] = 1.0f * Clamp(positionData.GetZf(), 0.0f, 1.0f);
				break;
			case RAGE_SPEAKER_ID_BACK_RIGHT:
				metrics->relativeChannelVolumes[i] = 1.0f * Clamp(positionData.GetWf(), 0.0f, 1.0f);
				break;
			default:
				metrics->relativeChannelVolumes[i] = 0.f;
			}
		}
		else
		{
			metrics->relativeChannelVolumes[i] = 0.f;
		}
	}
}

void audEnvironment::CopyDryVolumesToReverbs(audEnvironmentMetricsInternal *metrics)
{
	// Copy the channel volumes to all the dry and reverbed channel volumes
	for(u32 i = 0; i < g_MaxOutputChannels; i++)
	{
		const f32 relativeChannelVol = metrics->relativeChannelVolumes[i].GetLinear() * metrics->reverbDry.GetLinear();
		metrics->relativeDryChannelVolumes[i].SetLinear(relativeChannelVol);
	}
	for(u32 j = 0; j < g_NumSourceReverbs; j++)
	{
		for(u32 i = 0,reverbIndex = 0; i < g_MaxOutputChannels; i++)
		{
			if(i != RAGE_SPEAKER_ID_FRONT_CENTER && i != RAGE_SPEAKER_ID_LOW_FREQUENCY)
			{
				const f32 relativeChannelVol = metrics->relativeChannelVolumes[i].GetLinear() * metrics->reverbWets[j].GetLinear();
				metrics->relativeWetChannelVolumes[reverbIndex++][j].SetLinear(relativeChannelVol);
			}
		}
	}
}

void audEnvironment::CalculateReverbSends(f32 *sends, f32 reverbWet, f32 reverbSize)
{
	for(u32 i=0; i<g_NumSourceReverbs; i++)
	{
		sends[i] = reverbWet * m_SourceReverbCrossfadeCurves[i].CalculateValue(reverbSize);
	}
}
void audEnvironment::CalculateEnvironmentSoundMetrics(audEnvironmentSoundMetrics *metricsInOut, u32 &virtualisationScore)
{
	// We go through each listener, and get metrics for that one. Then we combine those metrics into the final answer.
	audEnvironmentMetricsInternal metricsPerListener[g_maxListeners];
	audEnvironmentMetricsInternal metrics;
	metrics.Init(metricsInOut);

	f32 listenerContribution[g_maxListeners];
	u32 listenersInUse = 0;
	for (u32 i=0; i<g_maxListeners; i++)
	{
		if(IsListenerInUse(i) && (metricsInOut->listenerMask&(1<<i)))
		{
			// copy over the metrics into the per-listener one we're about to pass down
			metricsPerListener[i] = metrics; 
			//sysMemCpy(&metricsPerListener[i], metrics, sizeof(metricsPerListener[i]));
			listenerContribution[i] = GetListenerContribution(i);
			listenersInUse++;
			// Get the listener metrics for that one listener
			CalculateEnvironmentSoundMetricsForSingleListener(&metricsPerListener[i], i);
		}
		else
		{
			listenerContribution[i] = 0.0f;
		}
	}

	// Now combine all our listener results into one
	CombineEnvironmentalSoundMetrics(&metrics, listenerContribution, listenersInUse, metricsPerListener);

	// turn volume routing metrics into actual route volumes
	ComputeVoiceRoutes(&metrics);

	virtualisationScore = metrics.virtualisationScore;

	// Copy any local changes back to the main metrics
	metricsInOut->dopplerFrequencyScalingFactor = metrics.dopplerFrequencyScalingFactor;
	metricsInOut->HPFCutoff = metrics.HPFCutoff;
	metricsInOut->LPFCutoff = metrics.LPFCutoff;
}

void audEnvironment::CombineEnvironmentalSoundMetrics(audEnvironmentMetricsInternal *metrics, 
													  f32 *listenerContribution, u32 numListenersInUse,const audEnvironmentMetricsInternal *individualMetrics)
{
	Assert(numListenersInUse>0); // let's not allow no listeners - maybe allow it later, but it'll fail all over the shop I guess,
								// and is more likely a threading issue with counting them.

	// Special case that there's just one in use, and return that
	if (numListenersInUse==1)
	{
		// Look through the list til we find it. (even though it's obviously the first one :-)
		for (u32 i=0; i<g_maxListeners; i++)
		{
			if (listenerContribution[i] != 0.0f)
			{
				*metrics = individualMetrics[i];  
				//sysMemCpy(metrics, &individualMetrics[i], sizeof(individualMetrics[i]));
				return;
			}
		}
		Assert(0); // apparently we have a listener, but we've failed to find it.
	}
	// We have genuine multiple listeners. Work out their volume ratios, as that determines their contribution to every factor.
	f32 relativeContribution[g_maxListeners];
	// Whip through all out listeners and see which is the loudest
	f32 loudestListenerLinear = g_SilenceVolumeLin;
	u32 loudestListener = 0;
	for (u32 i=0; i<g_maxListeners; i++)
	{
		relativeContribution[i] = 0.f;
		if(IsListenerInUse(i) && (metrics->environmentSoundMetrics->listenerMask&(1<<i)))
		{
			relativeContribution[i] = 0.0f;
			if (listenerContribution[i] != 0.0f)
			{
				const f32 thisListenerLin = individualMetrics[i].volume.GetLinear() * 
											individualMetrics[i].requestedVolume.GetLinear() *
											individualMetrics[i].postSubmixVolumeAttenuation.GetLinear();
				if (thisListenerLin > loudestListenerLinear)
				{
					loudestListenerLinear = thisListenerLin;
					loudestListener = i;
				}
			}
		}
	}

	const f32 loudestListenerdB = Max(-100.f, audDriverUtil::ComputeDbVolumeFromLinear(loudestListenerLinear));
	Assertf(loudestListenerdB >= -100.f, "Bad loudest listener volume. Value :%f",loudestListenerdB);
	// Now whip through all the listeners, and work out their relative linear contribution. For channel volumes, we take the max, but for (eg) doppler
	// we take a scaled contribution, so we need to know the total linear volume, annoyingly.
	f32 totalContributions = 0.0f;
//	audWarningf("New envSound");
	for (u32 i=0; i<g_maxListeners; i++)
	{
		if(IsListenerInUse(i) && (metrics->environmentSoundMetrics->listenerMask&(1<<i)))
		{
			if (listenerContribution[i] != 0.0f)
			{
				// Work out our linear contribution.
				// Sometimes we get <g_SileneceVolume volumes, which can give us zero contributions. So for this fn, we clip to g_SilenceVolume
				f32 clipVolume = Max(g_SilenceVolume, individualMetrics[i].volume.GetDb() + individualMetrics[i].requestedVolume.GetDb() + individualMetrics[i].postSubmixVolumeAttenuation.GetDb());
				f32 dBsDown = clipVolume - loudestListenerdB;
				relativeContribution[i] = Min (1.f,audDriverUtil::ComputeLinearVolumeFromDb(dBsDown));
				Assertf(relativeContribution[i] <= 1.f, "Bad relative contribution. Value :%f",relativeContribution[i]);
				// Factor in our pre-set listener contributions
				relativeContribution[i] *= listenerContribution[i];
				totalContributions += relativeContribution[i];
	//			audWarningf("i: %d; loud: %f; vol: %f; bDdown: %f; rc: %f; tc: %f", i, loudestListenerdB, individualMetrics[i].volume, dBsDown, relativeContribution[i], totalContributions);
	//			audWarningf("v:%f fc:%f rs:%f rw:%f rcv0:%f rcv1:%f", individualMetrics[i].volume, individualMetrics[i].filterCutoff, individualMetrics[i].reverbSize, individualMetrics[i].reverbWet, individualMetrics[i].relativeChannelVolumes[0], individualMetrics[i].relativeChannelVolumes[1]);
			}
		}
	}
	if (totalContributions<=0.0f)
	{
		Assert(0);
		return;
	}

	// We now know how much linear contribution we have in total, so for most factors, we scale by their relativeContribution/totalContrib
	// Channel volumes we do differently, because we're just taking the max (but scaled) for each individual channel.
	// First up, prep out output metrics appropriately for the way we combine them
	metrics->dopplerFrequencyScalingFactor = 0.f;
	metrics->volume = individualMetrics[loudestListener].volume;
	metrics->postSubmixVolumeAttenuation = individualMetrics[loudestListener].postSubmixVolumeAttenuation;

	metrics->LPFCutoff = 0; 
	metrics->HPFCutoff = 0;

	for (u32 j=0; j<g_NumSourceReverbChannels; j++)
	{
		for(u32 reverbNum=0; reverbNum<g_NumSourceReverbs; reverbNum++)
		{
			metrics->relativeWetChannelVolumes[j][reverbNum] = 0.f;
		}
	}
	//	audWarningf("v:%f fc:%f rs:%f rw:%f rcv0:%f rcv1:%f", metrics->volume, metrics->filterCutoff, metrics->reverbSize, metrics->reverbWet, metrics->relativeChannelVolumes[0], metrics->relativeChannelVolumes[1]);

	
	f32 combinedDopplerFrequencyScalingFactor = 0.f;
	f32 combinedReverbDry = 0.f;

	f32 combinedReverbWets[g_NumSourceReverbs];
	f32 combinedRelativeChannelVolumes[g_MaxOutputChannels];
	f32 combinedRelativeDryChannelVolumes[g_MaxOutputChannels];
	f32 combinedRelativeWetChannelVolumes[g_NumSourceReverbChannels][g_NumSourceReverbs];
	sysMemZeroBytes<sizeof(combinedRelativeChannelVolumes)>(combinedRelativeChannelVolumes);
	sysMemZeroBytes<sizeof(combinedRelativeWetChannelVolumes)>(combinedRelativeWetChannelVolumes);
	sysMemZeroBytes<sizeof(combinedReverbWets)>(combinedReverbWets);
	sysMemZeroBytes<sizeof(combinedRelativeDryChannelVolumes)>(combinedRelativeDryChannelVolumes);

	for (u32 listenerIdx=0; listenerIdx<g_maxListeners; listenerIdx++)
	{
		if(IsListenerInUse(listenerIdx) && (metrics->environmentSoundMetrics->listenerMask&(1<<listenerIdx)) && listenerContribution[listenerIdx] != 0.0f)
		{
			f32 scaledContribution = relativeContribution[listenerIdx] / totalContributions;
			// Go through each factor, and add in this listener's contribution
			combinedDopplerFrequencyScalingFactor += (scaledContribution * individualMetrics[listenerIdx].dopplerFrequencyScalingFactor);
			combinedReverbDry += (scaledContribution * individualMetrics[listenerIdx].reverbDry.GetLinear());
			for(u32 reverbNum=0; reverbNum<g_NumSourceReverbs; reverbNum++)
			{
				combinedReverbWets[reverbNum] += (scaledContribution * individualMetrics[listenerIdx].reverbWets[reverbNum].GetLinear());
			}
//			metrics->reverbSize += (scaledContribution * individualMetrics[i].reverbSize);
//			metrics->reverbWet += (scaledContribution * individualMetrics[i].reverbWet);
			metrics->LPFCutoff += (u16)(scaledContribution * (f32)individualMetrics[listenerIdx].LPFCutoff);
			metrics->HPFCutoff += (u16)(scaledContribution * (f32)individualMetrics[listenerIdx].HPFCutoff);
			// Our channel volumes are a little different - we just take the max of the scaled volumes, not the sum. Could try doing it the other way too?
			for (u32 channelIdx=0,reverbIndex=0; channelIdx<g_MaxOutputChannels; channelIdx++)
			{
				combinedRelativeChannelVolumes[channelIdx] = Max(combinedRelativeChannelVolumes[channelIdx], 
														 individualMetrics[listenerIdx].relativeChannelVolumes[channelIdx].GetLinear() * relativeContribution[listenerIdx]);
				combinedRelativeDryChannelVolumes[channelIdx] = Max(combinedRelativeDryChannelVolumes[channelIdx], 
					individualMetrics[listenerIdx].relativeDryChannelVolumes[channelIdx].GetLinear() * relativeContribution[listenerIdx]);
				// no reverb for centre or lfe
				if(channelIdx != RAGE_SPEAKER_ID_FRONT_CENTER && channelIdx != RAGE_SPEAKER_ID_LOW_FREQUENCY)
				{
					for(u32 reverbNum=0; reverbNum<g_NumSourceReverbs; reverbNum++)
					{
						combinedRelativeWetChannelVolumes[reverbIndex][reverbNum] = Max(combinedRelativeWetChannelVolumes[reverbIndex][reverbNum], 
							individualMetrics[listenerIdx].relativeWetChannelVolumes[reverbIndex][reverbNum].GetLinear() * relativeContribution[listenerIdx]);
					}
					reverbIndex++;
				}
				Assertf(combinedRelativeChannelVolumes[channelIdx]<=1.05f ,"Channel :%d, combine relative vol :%f",channelIdx,combinedRelativeChannelVolumes[channelIdx]);
				Assertf(combinedRelativeChannelVolumes[channelIdx]>=0.0f ,"Channel :%d, combine relative vol :%f",channelIdx,combinedRelativeChannelVolumes[channelIdx]);
			}
			///////////////////////////////////////
			// MdS 22/10/06 I've not tried this, but it's an alternative way of combining the individual channel volumes, just the same way
			// as I've combined all the other factors. I've never even built it, but whoever first tries multi-listeners out in a real situation
			// may want to give this a go, and see how it sounds.
			/*
			for (u32 j=0; j<g_MaxOutputChannels; j++)
			{
				metrics->relativeChannelVolumes[j] = scaledContribution * individualMetrics[i]->relativeChannelVolumes[j];
			}
			*/
			///////////////////////////////////////
		}
	}
	metrics->reverbDry.SetLinear(combinedReverbDry);
	metrics->dopplerFrequencyScalingFactor = combinedDopplerFrequencyScalingFactor;
	for(u32 i = 0; i < g_NumSourceReverbs; i++)
	{
		metrics->reverbWets[i].SetLinear(combinedReverbWets[i]);
		for(u32 j = 0; j < g_NumSourceReverbChannels; j++)
		{
			metrics->relativeWetChannelVolumes[j][i].SetLinear(combinedRelativeWetChannelVolumes[j][i]);
		}
	}
	for(u32 i = 0; i < g_MaxOutputChannels; i++)
	{
		metrics->relativeChannelVolumes[i].SetLinear(combinedRelativeChannelVolumes[i]);
		metrics->relativeDryChannelVolumes[i].SetLinear(combinedRelativeDryChannelVolumes[i]);
	}
	//	audWarningf("v:%f fc:%f rs:%f rw:%f rcv0:%f rcv1:%f", metrics->volume, metrics->filterCutoff, metrics->reverbSize, metrics->reverbWet, metrics->relativeChannelVolumes[0], metrics->relativeChannelVolumes[1]);
}
#if __BANK && !__SPU

atRangeArray<bool, kMaxSubmixes> g_SubmixRecording;

void SubmixRecordingStart()
{
	const u32 numSubmixes = audDriver::GetMixer()->GetNumSubmixes();
	for(u32 i = 0; i < numSubmixes; i++)
	{
		audMixerSubmix *submix = audDriver::GetMixer()->GetSubmixFromIndex(i);
		if(g_SubmixRecording[i])
		{
			if(!submix->IsRecordingOutput())
			{				
				submix->SetDumpOutput(submix->GetName());
			}
		}
	}
}

void SubmixRecordingStop()
{
	const u32 numSubmixes = audDriver::GetMixer()->GetNumSubmixes();
	for(u32 i = 0; i < numSubmixes; i++)
	{
		audMixerSubmix *submix = audDriver::GetMixer()->GetSubmixFromIndex(i);
		
		if(submix->IsRecordingOutput())
		{				
			submix->SetDumpOutput(NULL);
		}
	}
}

void audEnvironment::AddWidgets(bkBank &bank)
{
	bank.PushGroup("audEnvironment");
		bank.PushGroup("Rear attenuation");
			bank.AddSlider("Close distance:",&sm_CloseAttenuationDistance,0.f,1000.f,0.1f);
			bank.AddSlider("Far distance:",&sm_FarAttenuationDistance,0.f,1000.f,0.1f);
		bank.PopGroup();
		bank.AddSlider("Channels", &g_VelocitySmoothRate, 0.0f, 10.0f, 0.01f);

		bank.PushGroup("Submix Recording", false);
			const u32 numSubmixes = audDriver::GetMixer()->GetNumSubmixes();
			for(u32 i = 0; i < numSubmixes; i++)
			{
				audMixerSubmix *submix = audDriver::GetMixer()->GetSubmixFromIndex(i);
				Assert(submix);

				bank.AddToggle(submix->GetName(), &g_SubmixRecording[i], NullCB);

			}

			bank.AddButton("Start Recording", CFA(SubmixRecordingStart));
			bank.AddButton("Stop Recording", CFA(SubmixRecordingStop));
		bank.PopGroup();

		AddGameWidgets(bank);
	bank.PopGroup();
}

void audEnvironment::OnCurveModified(const u32 nameHash)
{
	// Update our cached LPF/HPF curves in response to a RAVE edit

	for(s32 i = 0; i < m_LPFCurveNames.GetCount(); i++)
	{
		if(m_LPFCurveNames[i] == nameHash)
		{
			SetupCustomCurve(m_LPFCurves[i], m_LPFCurveNames[i]);
		}
	}
	for(s32 i = 0; i < m_HPFCurveNames.GetCount(); i++)
	{
		if(m_HPFCurveNames[i] == nameHash)
		{
			SetupCustomCurve(m_HPFCurves[i], m_HPFCurveNames[i]);
		}
	}
}

#endif

} // namespace rage

