//
// audioengine/categorymanager.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_CATEGORYMANAGER_H
#define AUD_CATEGORYMANAGER_H

#include "audiohardware/channel.h"
#include "category.h"
#include "metadatamanager.h"
#include "atl/pool.h"
#include "system/criticalsection.h"

namespace rage {

enum tNumDynamicHeirachies
{	
	NUM_DYNAMIC_HEIRACHIES =  16
};
enum tNumDynamicCategories
{
	NUM_DYNAMIC_CATEGORIES = 120
};

extern atPool<audCategory> *g_CategoryStore;
extern audCategory* g_CategoryStoreMem;
extern s16* g_DynamicHeirachies;

// PURPOSE
//  The Category Manager initialises the BASE category at game startup, which in turn initialises all other categories in
//  the hierarchy. It then maintains a sorted map of these categories, for speedy searching.
//  The manager also schedules the updating of the requested settings structures. It also provides accessor
//  functions for game-code to control category settings, and to grap pointers to categories. 
class audCategoryManager : public audObjectModifiedInterface
{
public:
	audCategoryManager();
	~audCategoryManager();

	// PURPOSE
	//	Initializes the metadata manager by loading categories.dat into memory and building lookup structures
	// RETURNS
	//	True if successful, false otherwise
	bool Init();

	// PURPOSE
	//	Frees all category metadata memory
	void Shutdown();

	// PURPOSE
	//  Initialises a category at startup - Categories themselves call this, as it sets up various maps and indexes.
	// PARAMS
	//  nameHash - the hashed name of the category to initialise.
	//  parent - the parent category, to enable hierarchy navigation.
	// RETURNS
	//  A ptr to the initialised category. NULL if not found.
	audCategory* InitCategory(u32 nameHash, audCategory* parent);


	//PURPOSE
	// Initialises a category at runtime - used for dynamic mixing and set up by mixgroups
	// Params
	// categoryNameHash - the hashed name of the category
	// parent - the parent heirachy, to enable heirachy navigation
	// hierarchy - the category hierarchy described by this mixgroup
	s32 InitDynamicCategory(u32 categoryNameHash, audCategory * parent, const MixGroupCategoryMap * map);
	
	//PURPOSE
	// Calls InitDynamicCategory to begin the cascade that generates the heirachy and registers the root category
	// so the update will know to process it
	// Params
	// categoryNameHash - the hashed name of the category
	// hierarchy - the category hierarchy described by this mixgroup
	s32 InitDynamicHeirarchy(u32 categoryNameHash, const MixGroupCategoryMap * map);


	//Purpose
	//Releases a dynamically created category heirachy and frees the category slots to be used again 
	//Params
	//index - index of the root category
	void ShutdownDynamicHeirachy(s32 index);

	//Purpose
	// Iteratively shuts down and releases categories in a dynamic heirachy
	//Params
	//index - index of the root category
	void ShutdownDynamicCategory(s32 index);

	// PURPOSE
	//  Returns a ptr to a category, given it's hashed name. Sounds do this once on instantiation, and cache the result.
	//  The categories are static, and do not change, hence this is fine, and means only one lookup per sound creation.
	// PARAMS
	//  hashValue - the category's hashed name
	// RETURNS
	//  A ptr to the category. NULL if not found.
	audCategory *GetCategoryPtr(const u32 hashValue);


	// PURPOSE
	//  Searches through a dynamic category heirachy and returms the requested category if present
	// PARAMS
	//  index - index of the root dynamic category to search from
	//  hashValue = a hash of the category name 
	// RETURNS
	//  A ptr to the category. NULL if not found.
	audCategory *GetDynamicCategoryPtr(s32 index, u32 hashValue);

	// PURPOSE
	//  As above but takes a pointer to the category metadata rather than a name hash
	audCategory *GetDynamicCategoryPtr(s32 catIndex, const Category * metadata);

	// PURPOSE
	// As above but returns the category index	
	s32 GetDynamicCategoryIndex(s32 catIndex, const Category * metadata);	

	// NOTE
	//  All the following setter methods on categories use a triple buffered system to ensure thread safety. Only one
	//  master thread is allowed to set category values, and commits these with a CommitCategorySettings call at the end
	//  of its game loop. The audio thread is the only thing that reads these settings, and does so from the cached
	//  buffer set on UpdateAudioThreadCategoryIndex(). It's pretty much like Sounds' RequestedSettings.

#if AUD_ATOMIC_CATEGORIES
	// PURPOSE
	//  Updates the index into the categorySettings array that we write to from the game thread. The reading audio thread
	//  index trails this by one or two.
	void CommitCategorySettings();
#endif // AUD_ATOMIC_CATEGORIES

	// PURPOSE
	//  Returns the current index to use for writing category settings in the game thread. We use this in all the 
	//  category setters.
	// RETURNS
	//  The game thread index.
	u16 GetGameThreadCategoryIndex();

	// PURPOSE
	//  Updates the index into the categorySettings array that we read from the audio thread. This index trails the 
	//  writing game thread by one or two.
	void UpdateAudioThreadCategoryIndex();

	// PURPOSE
	//	Updates all categories, combining their settings down the category hierarchy and populating the
	//	g_ResolvedCategorySettings array
	void Update();

	// PURPOSE
	//  All the setters take their appropriate parameter, and the category name this is to be set on. The manager
	//  looks this category up, and performs the appropriate set fn. If calling this a great deal (>>1 per frame), 
	//  consider using GetCategoryPtr() which will perform one search, and then access that category directly.

	// PURPOSE
	//  Sets the volume of a category.
	void SetVolume(const char* category, float volume);
	void SetVolume(const u32 categoryHash, float volume);

	// PURPOSE
	//  Sets the pitch of a category.
	void SetPitch(const char* category, s32 pitch);
	void SetPitch(const u32 categoryHash, s32 pitch);

	// PURPOSE
	//  Sets the distance roll off factor of a category.
	void SetDistanceRollOffScale(const char* category, float distanceRollOffScale);
	void SetDistanceRollOffScale(const u32 categoryHash, float distanceRollOffScale);

	const audMetadataManager &GetMetadataManager() const
	{
		return m_MetadataMgr;
	}

	audMetadataManager &GetMetadataManager()
	{
		return m_MetadataMgr;
	}

	// Object edit notification support
	virtual void OnObjectModified(const u32 nameHash);
	// PURPOSE
	//	Called when the specified object has been modified in such a way that any pointers are now stale; ie
	//	its size has changed
	virtual void OnObjectOverridden(const u32 nameHash) { OnObjectModified(nameHash); }
	// PURPOSE
	//	Called when the specified object has been viewed in RAVE (ie displayed in the properties editor)
	virtual void OnObjectViewed(const u32 UNUSED_PARAM(nameHash)){}

	virtual void OnMuteListModified();
	virtual void OnSoloListModified();

#if __BANK

	void PrintResolvedVolumes() const;

	// PURPOSE
	//	Returns the pointer to metadata specified by the hash
	Category *GetMetadataPtr(const u32 hash);

	// PURPOSE
	//	Refreshes all cached metadata values to grab RAVE tweaks
	void PullMetadataValues();

	void UpdateTelemetry();

#endif // __BANK

	// PURPOSE
	//	Returns an index that can be used as a unique identifier for this category instance
	static __forceinline s32 GetCategoryIndex(const audCategory *category)
	{
		audAssertf( !category || g_CategoryStore->IsInPool(category), "Category ptr %p isn't in pool", category);
		if(category != NULL)
		{
			return (category->IsActive() || true) ? (s16)(category - g_CategoryStoreMem) : -1;
		}
		else
		{
			return -1;
		}
	}

	// PURPOSE
	//	Returns a category pointer from a specified index
	static __forceinline audCategory *GetCategoryFromIndex(const s32 categoryIndex)
	{
		if(categoryIndex != -1)
		{
			return g_CategoryStoreMem[categoryIndex].IsActive() ? &g_CategoryStoreMem[categoryIndex] : NULL;
		}
		else
		{
			return NULL;
		}
	}

	u32 GetNumberOfCategories()
	{
		return m_NumberOfCategories;
	}

	u32 GetNumberOfStaticCategories()
	{
		return m_NumberOfStaticCategories;
	}

	u32 GetCategorySettingsAudioThreadIndex()
	{
		return m_CategorySettingsAudioThreadIndex;
	}

#if RSG_PS3
	void LockUpdate()
	{
		m_CategoryUpdateCS.Lock();
	}

	void UnlockUpdate()
	{
		m_CategoryUpdateCS.Unlock();
	}
#endif // RSG_PS3

#if __BANK
	bool IsSoloActive() const
	{
		return m_IsSoloActive;
	}

	bool IsMuteActive() const;

	void SetTelemetryEnabled(const bool enabled) { m_TelemetryEnabled = enabled; }
#endif
protected:

private:

	// PURPOSE
	//  Allocates space for all the categories, initialises the BASE category, which in turn initialise all the categories.
	//  Then references these categories in a category map, for easy access, and then calls SortHashTable() to allow
	//  this map to be searched quickly.
	// RETURNS
	//  True if successfully built the map.
	bool BuildCategoryMap();
	// PURPOSE
	//  Sorts the CategoryMap, so that it may be quickly searched when access to a category is required.
	void SortHashTable();

	//PURPOSE
	//Register/unregister the root category of a heirachy so that we know
	//what to update
	void RegisterDynamicHeirachy(s32 index);
	void UnregisterDynamicHeirachy(s32 index);


	audMetadataManager	m_MetadataMgr;

	u32			m_NumberOfCategories; 
	u32			m_NumberOfStaticCategories;		

	struct audCategoryMapEntry
	{
		audCategory *ptr;
		u32 key;
	};

	audCategoryMapEntry*		m_CategoryMap;
	s32							m_CurrentHashIndex; 
	// audCategory*				m_CategoryStore;
	u32							m_CurrentStoreIndex;
	
	u16							m_CategorySettingsGameThreadIndex;
	u16							m_CategorySettingsAudioThreadIndex;

	sysCriticalSectionToken		m_CategoryUpdateCS;

#if __BANK
	bool						m_TelemetryEnabled;
	bool						m_IsSoloActive;
	bool						m_HaveSentTelemetryHeader;
	s32							m_RecursionDebugIndex;
#endif

};

} // namespace rage

#endif //AUD_CATEGORYMANAGER_H
