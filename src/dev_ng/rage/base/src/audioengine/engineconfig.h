// 
// audioengine/engineconfig.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_ENGINECONFIG_H
#define AUD_ENGINECONFIG_H

#include "vectormath/vec3v.h"
#include "enginedefs.h"

namespace rage
{
	class audEngineConfig
	{
	public:
		audEngineConfig()
		{
			m_GameOrientationForward = -Vec3V(V_Z_AXIS_WZERO);
			m_GameOrientationUp = Vec3V(V_Y_AXIS_WZERO);

			m_SpeedOfSound = 340.f;
			m_MaxRelativeVelocityForDoppler = 35.f;

			m_SoundMetadata = "config/sounds.dat";
			m_CurveMetadata = "config/curves.dat";
			m_CategoryMetadata = "config/categories.dat";
			m_EffectMetadata = "config/effects.dat";
			m_SpeechLookupMetadata = "config/speech.dat";
			m_SpeechMetadata = "config/speech2.dat";

			m_NumSoundsPerBucket = g_audMaxSoundSlotsPerBucket;
			m_NumRequestedSettingsPerBucket = g_audMaxRequestedSettingsSlotsPerBucket;
			m_NumBuckets = 12;
			m_NumReservedBuckets = 4;
		}
		~audEngineConfig(){}

		// PURPOSE
		//	Parses config file
		// RETURNS
		//	True if successful, false otherwise
		bool Init();

		// PURPOSE
		//	Returns game world orientation
		// NOTES
		//	Defaults to Up (0,1,0) Forward (0,0,1)
		void GetOrientation(Vec3V &gameUp, Vec3V &gameForward) const
		{
			gameUp = m_GameOrientationUp;
			gameForward = m_GameOrientationForward;
		}

		// PURPOSE
		//	Returns speed of sound in meters per second
		// NOTES
		//	Defaults to 340.f
		f32 GetSpeedOfSound() const
		{
			return m_SpeedOfSound;
		}

		// PURPOSE
		//	Returns maximum (scaled) relative velocity supported in the calculation of doppler frequency scaling.
		// NOTES
		//	Defaults to 35.f
		f32 GetMaximumRelativeVelocityForDoppler() const
		{
			return m_MaxRelativeVelocityForDoppler;
		}

		// PURPOSE
		//	Returns number of slots per sound pool bucket
		//	NOTES
		//		Defaults to 255
		u32 GetNumSoundSlotsPerBucket() const
		{
			return m_NumSoundsPerBucket;
		}

		// PURPOSE
		//	Returns number of requested settings slots per sound bucket
		// NOTES
		//	Defaults to 128
		u32 GetNumRequestedSettingsSlotsPerBucket() const
		{
			return m_NumRequestedSettingsPerBucket;
		}

		// PURPOSE
		//	Returns number of buckets in sound pool
		// NOTES
		//	Defaults to 48
		u32 GetNumBuckets() const
		{
			return m_NumBuckets;
		}

		// PURPOSE
		//	Returns the path to the sound metadata file
		// NOTES
		//	Defaults to config/sounds.dat
		const char *GetSoundMetadata() const
		{
			return m_SoundMetadata;
		}

		// PURPOSE
		//	Returns the path to the curve metadata file
		// NOTES
		//	Defaults to config/curves.dat
		const char *GetCurveMetadata() const
		{
			return m_CurveMetadata;
		}

		// PURPOSE
		//	Returns the path to the category metadata file
		// NOTES
		//	Defaults to config/categories.dat
		const char *GetCategoryMetadata() const
		{
			return m_CategoryMetadata;
		}

		// PURPOSE
		//	Returns the path to the effect metadata file
		// NOTES
		//	Defaults to config/effects.dat
		const char *GetEffectMetadata() const
		{
			return m_EffectMetadata;
		}

		// PURPOSE
		//	Returns the path to the speech lookup metadata file
		// NOTES
		//	Defaults to config/speech.dat
		const char *GetSpeechLookupMetadata() const
		{
			return m_SpeechLookupMetadata;
		}

		// PURPOSE
		//	Returns the path to the speech lookup metadata file
		// NOTES
		//	Defaults to config/speech.dat
		const char *GetSpeechMetadata() const
		{
			return m_SpeechMetadata;
		}

		// PURPOSE
		//	Returns the maximum number of audRequestedSettings instances
		u32 GetRequestedSettingsPoolSize() const
		{
			return 3000;
		}

		u32 GetNumReservedBuckets() const
		{
			return m_NumReservedBuckets;
		}

	private:

		Vec3V m_GameOrientationUp, m_GameOrientationForward;

		f32 m_SpeedOfSound, m_MaxRelativeVelocityForDoppler;
		const char *m_SoundMetadata, *m_CurveMetadata, *m_CategoryMetadata, *m_EffectMetadata;
		const char *m_SpeechLookupMetadata;
		const char *m_SpeechMetadata;
		u32 m_NumSoundsPerBucket, m_NumBuckets;
		u32 m_NumRequestedSettingsPerBucket;
		u32 m_NumReservedBuckets;
	};
}

#endif 
