//
// audioengine/engineutil.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "engineutil.h"

#include "audiohardware/driver.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/device.h"

#include "math.h"
#include "math/random.h"
#include "system/timer.h"

namespace rage 
{

// static instance
mthRandom audEngineUtil::sm_Random;

u64 audEngineUtil::sm_BootTimeTicks = 0;

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

u32 audEngineUtil::sm_CaptureTimeInMSSystemSetTime = 0;
u32 audEngineUtil::sm_CaptureTimeStep = 0;
u32 audEngineUtil::sm_CaptureTimeInMS = 0;

#endif // defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

#if !__SPU
void audEngineUtil::InitClass()
{
	audEngineUtil::sm_Random.Reset(sysTimer::GetSystemMsTime());
	sm_BootTimeTicks = sysTimer::GetTicks();
}

void audEngineUtil::ShutdownClass()
{

}

bool audEngineUtil::MatchName(const char *name, const char *testString)
{
	char seperators[] = " ,;";
	char *pToken = NULL;
	bool bReadFirstToken = false;

	char FullLine[256];	
	formatf(FullLine, "%s", testString); //	make a copy of this string as strtok will destroy it

	while (!bReadFirstToken || pToken)
	{
		if (bReadFirstToken == false)
		{
			bReadFirstToken = true;
			pToken = strtok( FullLine, seperators );
		}
		else
		{
			pToken = strtok( NULL, seperators );
		}

		if(pToken)
		{
			char buf1[128];
			char buf2[128];
			const s32 len1 = istrlen(name);
			const s32 len2 = istrlen(pToken);

			Assert(len1 < sizeof(buf1));
			Assert(len2 < sizeof(buf2));

			for(s32 i = 0; i < len1; i++)
			{
				buf1[i] = static_cast<char>( toupper(name[i]) );
			}
			buf1[len1] = 0;

			for(s32 i = 0; i < len2; i++)
			{
				buf2[i] = static_cast<char>( toupper(pToken[i]) );
			}
			buf2[len2] = 0;

			if(strstr(buf1,buf2) != NULL)
			{
				return true;
			}
		}		
	}

	return false;
}

u32 audEngineUtil::GetCurrentTimeInMilliseconds()
{
#if 1
	if(audDriver::GetMixer())
	{
		return audDriver::GetMixer()->GetMixerTimeMs();
	}
	else
	{
		return 0;
	}

#else
#if __PS3
	return sysTimer::GetSystemMsTime();
#else
	return static_cast<u32>((sysTimer::GetTicks()-sm_BootTimeTicks) * sysTimer::GetTicksToMilliseconds());
#endif
#endif
}

#endif // !__SPU
}
