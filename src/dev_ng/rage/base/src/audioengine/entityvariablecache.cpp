// 
// entityvariablecache.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
// 

#include "entityvariablecache.h"
#include "engine.h"
#if __SPU
#include "system/dma.h"
#include <cell/atomic.h>
#endif

namespace rage
{

#if __DEV
extern u32 g_EntityVariableCacheHits;
extern u32 g_EntityVariableCacheMisses;
#endif

void audEntityVariableCache::Init()
{
	sysMemSet(&m_Cache[0], 0, sizeof(m_Cache));
	sysMemSet(&m_CacheEa[0], 0, sizeof(m_CacheEa));
	m_NextIndex = 0;
}

#if __SPU
float * audEntityVariableCache::ResolveVariablePtr(float * ea)
{
	size_t maskedEa = (size_t)ea&~0x1f;

	int cacheIndex = -1, cacheTouse = m_NextIndex;
	
	for(int i=0; i < kNumCacheBlocks; i++)
	{
		if(maskedEa == (size_t)m_CacheEa[i])
		{
#if __DEV
			g_EntityVariableCacheHits++;
#endif
			cacheIndex = i;
			cacheTouse = cacheIndex;
			break;
		}
	}

	if(cacheIndex == -1)
	{
		m_CacheEa[cacheTouse] = maskedEa;

#if __DEV
		g_EntityVariableCacheMisses++;
#endif

		static const size_t cacheSize = sizeof(m_Cache)/kNumCacheBlocks;

		sysDmaGetAndWait(&m_Cache[cacheTouse*audVariableBlock::kNumVariablesPerBlock], m_CacheEa[cacheTouse], cacheSize , 5);

		CompileTimeAssert(kNumCacheBlocks == 2); //if kNumCacheBlocks changes we need to change this line
		m_NextIndex = (m_NextIndex+1)&1;
	}
	
	return (float *)(&m_Cache[cacheTouse*audVariableBlock::kNumVariablesPerBlock + (ea-(float*)maskedEa)]);
}
void audEntityVariableCache::SetVariableValue(float * ea, float value)
{
	union
	{
		f32 fVal;
		u32 iVal;
	}floatToInt;
	floatToInt.fVal = value;
	sysDmaPutUInt32((uint32_t)floatToInt.iVal, (uint32_t)ea, 7);

	size_t masked = (size_t)ea&~0x1f;
	// Update cache if necessary

	int cacheIndex = -1;
	for(int i=0; i < kNumCacheBlocks; i++)
	{
		if(masked == m_CacheEa[i])
		{
			cacheIndex = i;
			break;
		}
	}

	if(cacheIndex >= 0)
	{
		m_Cache[cacheIndex*audVariableBlock::kNumVariablesPerBlock + (ea-(float*)m_CacheEa[cacheIndex])] = value;
	}
}
#endif

} //namespace rage