/******************************************************************

curvedefs.h - automatically generated metadata structure definitions

Generated on 31/05/2010 14:47:07 by alastair.macgregor on machine EDIW-AMACGRE2

*******************************************************************/

#ifndef AUD_CURVEDEFS_H
#define AUD_CURVEDEFS_H

#include "vector/vector3.h"
#include "vector/vector4.h"

// handy macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))

namespace rage
{
// tristate values
enum TristateValue
{
		AUD_TRISTATE_FALSE,
		AUD_TRISTATE_TRUE,
		AUD_TRISTATE_UNSPECIFIED,
};
} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE
namespace rage
{
#define CURVEDEFS_SCHEMA_VERSION 16
// NOTE: doesn't include base object
#define AUD_NUM_CURVEDEFS 15


enum BaseCurveFlagIds
{
		FLAG_ID_BASECURVE_CLAMPINPUT, // ClampInput
};

struct BaseCurve
{
	static const rage::u32 TYPE_ID = 0;
	static const rage::u32 BASE_TYPE_ID = ~0U;

	BaseCurve()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	,MinInput(0.0f)
	,MaxInput(1.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::u32 ClassID : 8;
	rage::u32 NameTableOffset : 24;
	rage::u32 Flags;
	rage::f32 MinInput;
	rage::f32 MaxInput;
};

enum ConstantFlagIds
{
		FLAG_ID_CONSTANT_CLAMPINPUT, // ClampInput
};

struct Constant : BaseCurve
{
	static const rage::u32 TYPE_ID = 1;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	Constant()
	:Value(1.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::f32 Value;
};

enum LinearFlagIds
{
		FLAG_ID_LINEAR_CLAMPINPUT, // ClampInput
};

struct Linear : BaseCurve
{
	static const rage::u32 TYPE_ID = 2;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	Linear()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	struct tLeftHandPair
	{
		rage::f32 x;
		rage::f32 y;
	}LeftHandPair;

	struct tRightHandPair
	{
		rage::f32 x;
		rage::f32 y;
	}RightHandPair;

};

enum LinearDbFlagIds
{
		FLAG_ID_LINEARDB_CLAMPINPUT, // ClampInput
};

struct LinearDb : BaseCurve
{
	static const rage::u32 TYPE_ID = 3;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	LinearDb()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	struct tLeftHandPair
	{
		rage::f32 x;
		rage::f32 y;
	}LeftHandPair;

	struct tRightHandPair
	{
		rage::f32 x;
		rage::f32 y;
	}RightHandPair;

};

enum PiecewiseLinearFlagIds
{
		FLAG_ID_PIECEWISELINEAR_CLAMPINPUT, // ClampInput
};

struct PiecewiseLinear : BaseCurve
{
	static const rage::u32 TYPE_ID = 4;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	PiecewiseLinear()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	static const rage::u32 MAX_POINTS = 65537;
	rage::u32 numPoints;
	struct tPoint
	{
		rage::f32 x;
		rage::f32 y;
	}Point[MAX_POINTS];

};

enum EqualPowerFlagIds
{
		FLAG_ID_EQUALPOWER_CLAMPINPUT, // ClampInput
};

struct EqualPower : BaseCurve
{
	static const rage::u32 TYPE_ID = 5;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	EqualPower()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	struct tFlip
	{
		union
		{
			rage::u8 Value;
			struct 
			{
#if __BE
				bool p7:1; // padding
				bool p6:1; // padding
				bool p5:1; // padding
				bool p4:1; // padding
				bool p3:1; // padding
				bool p2:1; // padding
				bool y:1;
				bool x:1;
#else // !__BE
				bool x:1;
				bool y:1;
				bool p2:1; // padding
				bool p3:1; // padding
				bool p4:1; // padding
				bool p5:1; // padding
				bool p6:1; // padding
				bool p7:1; // padding
#endif // !__BE
			}BitFields;
		};
	}Flip;

};

enum ValueTableFlagIds
{
		FLAG_ID_VALUETABLE_CLAMPINPUT, // ClampInput
};

struct ValueTable : BaseCurve
{
	static const rage::u32 TYPE_ID = 6;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	ValueTable()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	static const rage::u16 MAX_VALUES = 65535;
	rage::u16 numValues;
	struct tValue
	{
		rage::f32 y;
	}Value[MAX_VALUES];

};

enum ExponentialFlagIds
{
		FLAG_ID_EXPONENTIAL_CLAMPINPUT, // ClampInput
};

struct Exponential : BaseCurve
{
	static const rage::u32 TYPE_ID = 7;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	Exponential()
	:Exponent(2.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	struct tFlip
	{
		union
		{
			rage::u8 Value;
			struct 
			{
#if __BE
				bool p7:1; // padding
				bool p6:1; // padding
				bool p5:1; // padding
				bool p4:1; // padding
				bool p3:1; // padding
				bool p2:1; // padding
				bool y:1;
				bool x:1;
#else // !__BE
				bool x:1;
				bool y:1;
				bool p2:1; // padding
				bool p3:1; // padding
				bool p4:1; // padding
				bool p5:1; // padding
				bool p6:1; // padding
				bool p7:1; // padding
#endif // !__BE
			}BitFields;
		};
	}Flip;

	rage::f32 Exponent;
};

enum DecayingExponentialFlagIds
{
		FLAG_ID_DECAYINGEXPONENTIAL_CLAMPINPUT, // ClampInput
		FLAG_ID_DECAYINGEXPONENTIAL_CLAMPTOZEROATONE, // ClampToZeroAtOne
};

struct DecayingExponential : BaseCurve
{
	static const rage::u32 TYPE_ID = 8;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	DecayingExponential()
	:HorizontalScaling(1.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::f32 HorizontalScaling;
};

enum DecayingSquaredExponentialFlagIds
{
		FLAG_ID_DECAYINGSQUAREDEXPONENTIAL_CLAMPINPUT, // ClampInput
		FLAG_ID_DECAYINGSQUAREDEXPONENTIAL_CLAMPTOZEROATONE, // ClampToZeroAtOne
};

struct DecayingSquaredExponential : BaseCurve
{
	static const rage::u32 TYPE_ID = 9;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	DecayingSquaredExponential()
	:HorizontalScaling(1.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::f32 HorizontalScaling;
};

enum SineCurveFlagIds
{
		FLAG_ID_SINECURVE_CLAMPINPUT, // ClampInput
};

struct SineCurve : BaseCurve
{
	static const rage::u32 TYPE_ID = 10;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	SineCurve()
	:StartPhase(0.0f)
	,EndPhase(-1.0f)
	,Frequency(-1.0f)
	,VerticalScaling(1.0f)
	,VerticalOffset(0.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::f32 StartPhase;
	rage::f32 EndPhase;
	rage::f32 Frequency;
	rage::f32 VerticalScaling;
	rage::f32 VerticalOffset;
};

enum OneOverXFlagIds
{
		FLAG_ID_ONEOVERX_CLAMPINPUT, // ClampInput
		FLAG_ID_ONEOVERX_CLAMPTOZEROATONE, // ClampToZeroAtOne
};

struct OneOverX : BaseCurve
{
	static const rage::u32 TYPE_ID = 11;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	OneOverX()
	:HorizontalScaling(1.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::f32 HorizontalScaling;
};

enum OneOverXSquaredFlagIds
{
		FLAG_ID_ONEOVERXSQUARED_CLAMPINPUT, // ClampInput
		FLAG_ID_ONEOVERXSQUARED_CLAMPTOZEROATONE, // ClampToZeroAtOne
};

struct OneOverXSquared : BaseCurve
{
	static const rage::u32 TYPE_ID = 12;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	OneOverXSquared()
	:HorizontalScaling(1.0f)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::f32 HorizontalScaling;
};

enum DefaultDistanceAttenuationFlagIds
{
		FLAG_ID_DEFAULTDISTANCEATTENUATION_CLAMPINPUT, // ClampInput
};

struct DefaultDistanceAttenuation : BaseCurve
{
	static const rage::u32 TYPE_ID = 13;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	DefaultDistanceAttenuation()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

};

enum DefaultDistanceAttenuationClampedFlagIds
{
		FLAG_ID_DEFAULTDISTANCEATTENUATIONCLAMPED_CLAMPINPUT, // ClampInput
		FLAG_ID_DEFAULTDISTANCEATTENUATIONCLAMPED_RESCALE, // Rescale
};

struct DefaultDistanceAttenuationClamped : DefaultDistanceAttenuation
{
	static const rage::u32 TYPE_ID = 14;
	static const rage::u32 BASE_TYPE_ID = DefaultDistanceAttenuation::TYPE_ID;

	DefaultDistanceAttenuationClamped()
	:MaxGain(0)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	rage::s16 MaxGain;
};

enum DistanceAttenuationValueTableFlagIds
{
		FLAG_ID_DISTANCEATTENUATIONVALUETABLE_CLAMPINPUT, // ClampInput
};

struct DistanceAttenuationValueTable : BaseCurve
{
	static const rage::u32 TYPE_ID = 15;
	static const rage::u32 BASE_TYPE_ID = BaseCurve::TYPE_ID;

	DistanceAttenuationValueTable()
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const rage::u32 fieldNameHash);

	static const rage::u16 MAX_VALUES = 65535;
	rage::u16 numValues;
	struct tValue
	{
		rage::f32 y;
	}Value[MAX_VALUES];

};


// PURPOSE
//	Gets the parent class type for the passed in class type
rage::u32 gCurvesGetBaseTypeId(const rage::u32 classId);


} // namespace rage
#endif // AUD_CURVEDEFS_H
