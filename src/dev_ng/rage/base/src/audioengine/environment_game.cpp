#include "audiohardware/driverutil.h"
#include "audiohardware/submix.h"
#include "audioeffecttypes/biquadfiltereffect.h"
#include "audioeffecttypes/delayeffect.h"
#include "audioeffecttypes/ereffect.h"
#include "audioeffecttypes/variabledelayeffect.h"
#include "audioeffecttypes/reverbeffect.h"
#include "audioeffecttypes/reverbeffect4.h"
#include "audioeffecttypes/reverbeffect_prog.h"
#include "audioeffecttypes/underwatereffect.h"
#include "audioeffecttypes/waveshapereffect.h"
#include "audioengine/ambisonics.h"
#include "audioengine/engine.h"
#include "audioengine/dynamicmixmgr.h"
#include "audiosynth/synthcore.h"
#include "bank/bank.h"
#include "category.h"
#include "curve.h"
#include "environment.h"
#include "audiohardware/debug.h"
#include "audiohardware/device.h" 
#include "audiohardware/driver.h"
#include "audiohardware/driverdefs.h"
#include "audiosoundtypes/sounddefs.h"
#include "audiohardware/submix.h"
#include "profile/profiler.h"
#include "system/param.h"

#include "vectormath/vectormath.h"

#if RSG_ORBIS
#include "audiohardware/device_orbis.h"
#endif // RSG_ORBIS

#if __SPU
#include "ambisonics.cpp"
#endif

#if RSG_PC
#define SEPERATE_MUSIC_ROUTE 0
#else
#define SEPERATE_MUSIC_ROUTE 1
#endif

namespace rage
{
	using namespace Vec;

atRangeArray<audGameEffectSettings, kNumSpecialEffectModes> g_EffectSettings;

extern audEngine g_AudioEngine;

XPARAM(cheapaudioeffects);

PF_PAGE(EnvironmentGamePage, "GTA environment_game");
PF_GROUP(EnvironmentGameGroup);
PF_LINK(EnvironmentGamePage, EnvironmentGameGroup);
PF_TIMER(CalcEnvMetricsForSingleListener, EnvironmentGameGroup);
PF_TIMER(CalcEnvFXMetricsForSingleListener, EnvironmentGameGroup);
PF_TIMER(Compute4ChannelVolsFromPos, EnvironmentGameGroup);
PF_TIMER(CollapseFourChannelsToTwo, EnvironmentGameGroup);
PF_TIMER(CalculateValueFor3, EnvironmentGameGroup);

bool g_AssertOnVolumeClamp = false;

#if __BANK
bool g_MaxOutReverb = false;
bool g_EnableSmallReverb = true;
bool g_UpdateSmallSettings = false;

bool g_EverythingToSmall = false;
#endif

float g_SFXHPFCutoff = 30.f;
bool g_SFXHPFBypass = true;
float g_SFXHPFReso = 7.f;

f32 g_PadSpeakerGain = 5.5f;

void audEnvironment::InitRoutes()
{
 	audDriver::GetMixer()->EnterBML();

	const u32 numOutputChannels = audDriver::GetNumOutputChannels();
	const u32 numSurroundChannels = Min<u32>(4, numOutputChannels);

	m_VehicleEngineSubmixesInUse.Reset();
	m_VehicleExhaustSubmixesInUse.Reset();
	m_ConversationSpeechSubmixesInUse.Reset();
	m_ReflectionsOutputSubmixesInUse.Reset();
	
	u32 masterSubmixIndex = audDriver::GetMixer()->GetSubmixIndex(audDriver::GetMixer()->GetMasterSubmix());
	audDriver::GetMixer()->GetMasterSubmix()->SetFlag(audMixerSubmix::ComputeRMS, true);

	audMixerSubmix *SFXSubmix = audDriver::GetMixer()->CreateSubmix("SFX", numOutputChannels, true);
	Assign(m_SFXSubmixId, audDriver::GetMixer()->GetSubmixIndex(SFXSubmix));
	SFXSubmix->AddOutput(masterSubmixIndex, true);
	SFXSubmix->SetFlag(audMixerSubmix::ComputeRMS, true);

	audMixerSubmix *musicSubmix = audDriver::GetMixer()->CreateSubmix("Music", numSurroundChannels, true);
	Assign(m_MusicSubmixId, audDriver::GetMixer()->GetSubmixIndex(musicSubmix));
	musicSubmix->AddOutput(masterSubmixIndex, true);
	musicSubmix->SetFlag(audMixerSubmix::InterleavedProcessing, true);
	// ProcessAsSourceEffect was only necessary to ensure we had the required effects code loaded on the SPU
	//musicSubmix->SetFlag(audMixerSubmix::ProcessAsSourceEffect, true);

#if SEPERATE_MUSIC_ROUTE
	audMixerSubmix *musicReverbSubmixes[3];
	musicReverbSubmixes[0] = audDriver::GetMixer()->CreateSubmix("Mus-Sml", numSurroundChannels, true);
	musicReverbSubmixes[1] = audDriver::GetMixer()->CreateSubmix("Mus-Med", numSurroundChannels, true);
	musicReverbSubmixes[2] = audDriver::GetMixer()->CreateSubmix("Mus-Lrg", numSurroundChannels, true);	

	for(s32 i = 0; i < 3; i++)
	{
		Assign(m_MusicReverbSubmixIds[i], audDriver::GetMixer()->GetSubmixIndex(musicReverbSubmixes[i]));
		musicReverbSubmixes[i]->AddOutput(m_MusicSubmixId, false);
	}
#endif

	audMixerSubmix *reverbSubmixes[3];
	reverbSubmixes[0] = audDriver::GetMixer()->CreateSubmix("Small", numSurroundChannels, true);
	reverbSubmixes[1] = audDriver::GetMixer()->CreateSubmix("Medium", numSurroundChannels, true);
	reverbSubmixes[2] = audDriver::GetMixer()->CreateSubmix("Large", numSurroundChannels, true);	

	for(s32 i = 0; i < 3; i++)
	{
		Assign(m_ReverbSubmixIds[i], audDriver::GetMixer()->GetSubmixIndex(reverbSubmixes[i]));
		reverbSubmixes[i]->AddOutput(m_SFXSubmixId, false);
	}

	for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; i++)
	{
		audMixerSubmix *engineSubmix = audDriver::GetMixer()->CreateSubmix("V-Eng", 1, true);
		engineSubmix->SetFlag(audMixerSubmix::ProcessAsSourceEffect, true);
		Assign(m_VehicleEngineSubmixIds[i], audDriver::GetMixer()->GetSubmixIndex(engineSubmix));
	}

	for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; i++)
	{
		audMixerSubmix *exhaustSubmix = audDriver::GetMixer()->CreateSubmix("V-Ex", 1, true);
		exhaustSubmix->SetFlag(audMixerSubmix::ProcessAsSourceEffect, true);
		Assign(m_VehicleExhaustSubmixIds[i], audDriver::GetMixer()->GetSubmixIndex(exhaustSubmix));
	}

	audMixerSubmix *localVoiceSubmix = audDriver::GetMixer()->CreateSubmix("HdSt", 1, true);
	localVoiceSubmix->SetFlag(audMixerSubmix::ProcessAsSourceEffect, true);
	Assign(m_LocalVoiceSubmixId, audDriver::GetMixer()->GetSubmixIndex(localVoiceSubmix));

	for(u32 i = 0; i <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; i++)
	{
		audMixerSubmix *conversationSubmix = audDriver::GetMixer()->CreateSubmix("Conv", 1, true);
		conversationSubmix->SetFlag(audMixerSubmix::ProcessAsSourceEffect, true);
		Assign(m_ConversationSpeechSubmixIds[i], audDriver::GetMixer()->GetSubmixIndex(conversationSubmix));
	}

	// Create the reflections output submixes
	for(u32 reflectionLoop = 0; reflectionLoop < AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES; reflectionLoop++)
	{
		audMixerSubmix *reflectionsOutputSubmix = audDriver::GetMixer()->CreateSubmix("RefOut", 1, false);
		Assign(m_ReflectionsOutputSubmixIds[reflectionLoop], audDriver::GetMixer()->GetSubmixIndex(reflectionsOutputSubmix));
	}

	// Create the reflections source submix
	audMixerSubmix *reflectionsSourceSubmix = audDriver::GetMixer()->CreateSubmix("RefSrc", AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES, false);
	Assign(m_ReflectionsSourceSubmixId, audDriver::GetMixer()->GetSubmixIndex(reflectionsSourceSubmix));

	// Route the reflections source submix through each of the output submixes, and mute the relevant channels
	for(u32 reflectionLoop = 0; reflectionLoop < AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES; reflectionLoop++)
	{
		reflectionsSourceSubmix->AddOutput(m_ReflectionsOutputSubmixIds[reflectionLoop], true);
		ALIGNAS(16) f32 reflectionsOutputVolumes[g_MaxOutputChannels] ;
		sysMemSet(&reflectionsOutputVolumes[0], 0, sizeof(reflectionsOutputVolumes));
		reflectionsOutputVolumes[reflectionLoop] = 1.0f;
		reflectionsSourceSubmix->SetOutputVolumes(reflectionLoop, reflectionsOutputVolumes);
	}
	
	for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; i++)
	{
		audMixerSubmix* engineSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleEngineSubmixIds[i]);
		engineSubmix->AddOutput(m_SFXSubmixId, true);
	}

	for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; i++)
	{
		audMixerSubmix* exhaustSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleExhaustSubmixIds[i]);
		exhaustSubmix->AddOutput(m_SFXSubmixId, true);
	}

	for(u32 i = 0; i <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; i++)
	{
		audMixerSubmix* conversationSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ConversationSpeechSubmixIds[i]);
		conversationSubmix->AddOutput(m_SFXSubmixId, true);
	}

	for(u32 reflectionLoop = 0; reflectionLoop < AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES; reflectionLoop++)
	{
		audMixerSubmix* reflectionsOutputSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ReflectionsOutputSubmixIds[reflectionLoop]);
		reflectionsOutputSubmix->AddOutput(m_SFXSubmixId, true);
	}

	localVoiceSubmix->AddOutput(m_SFXSubmixId, true);

	for(s32 i = 0; i < 3; i++)
	{
		for(u32 loop = 0; loop <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; loop++)
		{
			audMixerSubmix* engineSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleEngineSubmixIds[loop]);
			engineSubmix->AddOutput(m_ReverbSubmixIds[i], true);
		}

		for(u32 loop = 0; loop <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; loop++)
		{
			audMixerSubmix* exhaustSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleExhaustSubmixIds[loop]);
			exhaustSubmix->AddOutput(m_ReverbSubmixIds[i], true);
		}

		/*for(u32 loop = 0; loop <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; loop++)
		{
			audMixerSubmix* conversationSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ConversationSpeechSubmixIds[loop]);
			conversationSubmix->AddOutput(m_ReverbSubmixIds[i], true);
		}*/

		localVoiceSubmix->AddOutput(m_ReverbSubmixIds[i], true);

		for(u32 reflectionLoop = 0; reflectionLoop < AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES; reflectionLoop++)
		{
			audMixerSubmix* reflectionsOutputSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ReflectionsOutputSubmixIds[reflectionLoop]);
			reflectionsOutputSubmix->AddOutput(m_ReverbSubmixIds[i], true);
		}
	}

	for(u32 loop = 0; loop <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; loop++)
	{
		audMixerSubmix* engineSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleEngineSubmixIds[loop]);
		engineSubmix->AddOutput(m_ReflectionsSourceSubmixId, true);
	}

	for(u32 loop = 0; loop <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; loop++)
	{
		audMixerSubmix* exhaustSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleExhaustSubmixIds[loop]);
		exhaustSubmix->AddOutput(m_ReflectionsSourceSubmixId, true);
	}

	for(s32 loop = 0; loop < kMaxSubmixes; loop++)
	{
		m_SubmixVirtualisationScores[loop] = ~0U;
	}

	/*for(u32 loop = 0; loop <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; loop++)
	{
		audMixerSubmix* conversationSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ConversationSpeechSubmixIds[loop]);
		conversationSubmix->AddOutput(m_ReflectionsSourceSubmixId, true);
	}*/

#if RSG_ORBIS
	audMixerSubmix *padSpeaker1 = audDriver::GetMixer()->CreateSubmix("Pad", 1, true);
	m_PadSpeakerIds[0] = audDriver::GetMixer()->GetSubmixIndex(padSpeaker1);

	((audMixerDeviceOrbis*)audDriver::GetMixer())->SetPadSpeakerSubmix(padSpeaker1);
	
	audDriver::GetMixer()->AddEndPoint(padSpeaker1);
#endif // RSG_ORBIS

#if RSG_DURANGO || RSG_ORBIS
	audDriver::GetMixer()->SetMainOutputSubmix(SFXSubmix);
	audDriver::GetMixer()->SetRestrictedOutputSubmix(musicSubmix);
#endif

	audDriver::GetMixer()->ComputeProcessingGraph();
	audDriver::GetMixer()->ExitBML();	

	// Mute vehicle submixes on startup
	ALIGNAS(16) f32 silence[g_MaxOutputChannels] ;
	sysMemSet(&silence[0], 0, sizeof(silence));

	// Listener, reverbs x 3
	for(s32 i = 0; i < 4; i++)
	{
		localVoiceSubmix->SetOutputVolumes(i, silence);

		for(u32 reflectionLoop = 0; reflectionLoop < AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES; reflectionLoop++)
		{
			audMixerSubmix* reflectionsOutputSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ReflectionsOutputSubmixIds[reflectionLoop]);
			reflectionsOutputSubmix->SetOutputVolumes(i, silence);
		}
	}

	// Listener, reverbs x 3, reflections
	for(s32 i = 0; i < 5; i++)
	{
		for(u32 loop = 0; loop <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; loop++)
		{
			audMixerSubmix* engineSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleEngineSubmixIds[loop]);
			engineSubmix->SetOutputVolumes(i, silence);
		}

		for(u32 loop = 0; loop <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; loop++)
		{
			audMixerSubmix* exhaustSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleExhaustSubmixIds[loop]);
			exhaustSubmix->SetOutputVolumes(i, silence);
		}

		/*for(u32 loop = 0; loop <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; loop++)
		{
			audMixerSubmix* conversationSubmix = audDriver::GetMixer()->GetSubmixFromIndex(m_ConversationSpeechSubmixIds[loop]);
			conversationSubmix->SetOutputVolumes(i, silence);
		}*/
	}

	audDriver::GetMixer()->FlagThreadCommandBufferReadyToProcess();
}

void audEnvironment::ComputeVoiceRoutes(audEnvironmentMetricsInternal *metrics) const
{
	//Apply 2dB offset to compensate for headroom adjustment in EnvSound.
	const f32 linearVolume = Clamp(metrics->volume.GetLinear() * g_HeadroomOffsetCompensationLin, 0.f, 1.f);

	const f32 wetMix = linearVolume * metrics->environmentSoundMetrics->routingMetric.sourceEffectWetMix;
	const f32 dryMix = linearVolume * metrics->environmentSoundMetrics->routingMetric.sourceEffectDryMix;

	const f32 postSubmixAttenLin = metrics->postSubmixVolumeAttenuation.GetLinear();

	atRangeArray<f32, g_MaxOutputChannels> dryChannelVolumes;
	atMultiRangeArray<f32, g_NumSourceReverbChannels, g_NumSourceReverbs> wetChannelVolumes;

	// Calculate our new channel volumes
	for(u32 channelIndex = 0; channelIndex < g_MaxOutputChannels; channelIndex++)
	{
		// Initialise the new ones to our desired volumes
		dryChannelVolumes[channelIndex] = postSubmixAttenLin * metrics->relativeDryChannelVolumes[channelIndex].GetLinear();
		
		// Calculate a wet volume if this is a reverb channel
		if(channelIndex < g_NumSourceReverbChannels)
		{
			for(u32 reverbIndex = 0 ; reverbIndex < g_NumSourceReverbs; reverbIndex++)
			{
				wetChannelVolumes[channelIndex][reverbIndex] = postSubmixAttenLin * metrics->relativeWetChannelVolumes[channelIndex][reverbIndex].GetLinear();
			}
		}		
	}

	u32 numDirectDestinations = 0;
	s32 directDestinationSubmixIds[g_MaxVoiceRouteDestinations];
	sysMemSet(directDestinationSubmixIds, 0xff, sizeof(directDestinationSubmixIds));
	f32 directChannelVolumes[g_MaxVoiceRouteDestinations][g_MaxOutputChannels];
	sysMemSet(directChannelVolumes, 0, g_MaxVoiceRouteDestinations * g_MaxOutputChannels * sizeof(f32));

	s32 destinationRouteId = -1;

	u32 effectRoute = metrics->environmentSoundMetrics->routingMetric.effectRoute;

	// If we're not using the control pad speaker, reroute to FE
	if(!m_PadSpeakerEnabled && effectRoute == EFFECT_ROUTE_PADSPEAKER_1)
	{
		effectRoute = EFFECT_ROUTE_FRONT_END;
	}

	metrics->virtualisationScore = ~0U;

	switch(effectRoute)
	{
	case EFFECT_ROUTE_MUSIC:
		destinationRouteId = m_MusicSubmixId;
		if(destinationRouteId != -1)
		{
			numDirectDestinations = 1;
			directDestinationSubmixIds[0] = m_MusicSubmixId;
			
			for(u32 i=0; i < g_MaxOutputChannels; i++)
			{
				// if we are NOT routing to a custom submix then apply postsubmix to direct
				directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
			}
		}
		break;

	case EFFECT_ROUTE_FRONT_END:
		destinationRouteId = m_SFXSubmixId;
		if(destinationRouteId != -1)
		{
			numDirectDestinations = 1;

			// if we are NOT routing to a custom submix then apply postsubmix to direct
			directDestinationSubmixIds[0] = destinationRouteId;
			
			for(u32 i=0; i < g_MaxOutputChannels; i++)
			{
				directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
			}
		}
		break;

	case EFFECT_ROUTE_PADSPEAKER_1:
		{
			s32 routeIndex = 0;

			destinationRouteId = m_PadSpeakerIds[0];
			if(destinationRouteId != -1)
			{
				// if we are NOT routing to a custom submix then apply postsubmix to direct
				directDestinationSubmixIds[routeIndex] = destinationRouteId;
				
				for(u32 i=0; i < g_MaxOutputChannels; i++)
				{
					directChannelVolumes[routeIndex][i] = dryMix * dryChannelVolumes[i] * g_PadSpeakerGain * m_PadSpeakerVolume;
				}

				routeIndex++;
			}

			directDestinationSubmixIds[routeIndex] = m_SFXSubmixId;
			directChannelVolumes[routeIndex][RAGE_SPEAKER_ID_FRONT_CENTER] = dryMix * dryChannelVolumes[0] * m_PadSpeakerSFXVolume;
	
			routeIndex++;

			numDirectDestinations = routeIndex;
		}
		break;

	case EFFECT_ROUTE_SML_REVERB_FULL_WET:
		destinationRouteId = m_ReverbSubmixIds[0];
		goto FullWetRouting;
	case EFFECT_ROUTE_MED_REVERB_FULL_WET:
		destinationRouteId = m_ReverbSubmixIds[1];
		goto FullWetRouting;
	case EFFECT_ROUTE_LRG_REVERB_FULL_WET:
		destinationRouteId = m_ReverbSubmixIds[2];
		goto FullWetRouting;
	case EFFECT_ROUTE_SFX:
		destinationRouteId = m_SFXSubmixId;
		goto FullWetRouting;
	case EFFECT_ROUTE_MASTER:
		destinationRouteId = 0;
		goto FullWetRouting;
FullWetRouting:

		numDirectDestinations = 1;
		directDestinationSubmixIds[0] = destinationRouteId;

		for(u32 i=0; i < g_MaxOutputChannels; i++)
		{
			directChannelVolumes[0][i] = 1.0f;
		}

		break;

	case EFFECT_ROUTE_POSITIONED_MUSIC: //Intentional fall-through.
	case EFFECT_ROUTE_POSITIONED: //Intentional fall-through.
	case EFFECT_ROUTE_LOCAL_VOICE:
	default:
		if(IsDirectMonoEffectRoute(effectRoute))
		{
			if(effectRoute >= EFFECT_ROUTE_VEHICLE_ENGINE_MIN && effectRoute <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX)
			{
				destinationRouteId = m_VehicleEngineSubmixIds[effectRoute - EFFECT_ROUTE_VEHICLE_ENGINE_MIN];
				u32 submixVirtScoreInverted = ~0U - m_SubmixVirtualisationScores[destinationRouteId];
				u64 newVirtScore = u64(submixVirtScoreInverted) * (u64)(Min(1.f, dryMix));

				u32 scaledVirtScore = ~0U - u32(Min<u64>(0xFFFFFFFF, newVirtScore));
				metrics->virtualisationScore = scaledVirtScore;
			}
			else if(effectRoute >= EFFECT_ROUTE_VEHICLE_EXHAUST_MIN && effectRoute <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX)
			{
				destinationRouteId = m_VehicleExhaustSubmixIds[effectRoute - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN];
				u32 submixVirtScoreInverted = ~0U - m_SubmixVirtualisationScores[destinationRouteId];
				u64 newVirtScore = u64(submixVirtScoreInverted) * (u64)(Min(1.f, dryMix));

				u32 scaledVirtScore = ~0U - u32(Min<u64>(0xFFFFFFFF, newVirtScore));
				metrics->virtualisationScore = scaledVirtScore;
			}
			else if(effectRoute >= EFFECT_ROUTE_CONVERSATION_SPEECH_MIN && effectRoute <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX)
			{
				destinationRouteId = m_ConversationSpeechSubmixIds[effectRoute - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN];
			}
			else
			{
				audAssertf(false, "Unexpected effect direct mono effect route");
			}

			if(destinationRouteId != -1)
			{
				numDirectDestinations = 1;

				// if we are NOT routing to a custom submix then apply postsubmix to direct
				directDestinationSubmixIds[0] = destinationRouteId;

				for(u32 i=0; i < g_MaxOutputChannels; i++)
				{
					directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
				}
			}
		}
		else
		{
			s32 destSubmixIds[g_NumSourceReverbs + 1] = 
			{
#if SEPERATE_MUSIC_ROUTE 
				effectRoute == EFFECT_ROUTE_POSITIONED_MUSIC ? m_MusicSubmixId : m_SFXSubmixId,
				effectRoute == EFFECT_ROUTE_POSITIONED_MUSIC ? m_MusicReverbSubmixIds[0] : m_ReverbSubmixIds[0],
				effectRoute == EFFECT_ROUTE_POSITIONED_MUSIC ? m_MusicReverbSubmixIds[1] : m_ReverbSubmixIds[1],
				effectRoute == EFFECT_ROUTE_POSITIONED_MUSIC ? m_MusicReverbSubmixIds[2] : m_ReverbSubmixIds[2]

#else
				m_SFXSubmixId,
				m_ReverbSubmixIds[0],
				m_ReverbSubmixIds[1],
				m_ReverbSubmixIds[2]
#endif
			};

			for(u32 i=0; i < g_NumSourceReverbs + 1; i++)
			{
				destinationRouteId = destSubmixIds[i];
				if(destinationRouteId != -1)
				{
					directDestinationSubmixIds[i] =	destinationRouteId;
				}
			}

			numDirectDestinations = g_NumSourceReverbs + 1;

			// the LISTENER route:
			for(u32 i=0; i < g_MaxOutputChannels; i++)
			{
				// if we are NOT routing to a custom submix then apply postSubmixAtten on the direct route
				directChannelVolumes[0][i] = dryMix * dryChannelVolumes[i];
			}

			// the three SOURCE_REVERB routes
			// if we are NOT routing to a custom submix then apply postSubmixAtten on the direct route
			u32 routeIndex = 1;
			
			for(u32 reverbChannelIndex = 0; reverbChannelIndex < g_NumSourceReverbChannels; reverbChannelIndex++)
			{
				directChannelVolumes[routeIndex][reverbChannelIndex] = dryMix * wetChannelVolumes[reverbChannelIndex][routeIndex - 1];
			}
			routeIndex++;
			// medium
			for(u32 reverbChannelIndex = 0; reverbChannelIndex < g_NumSourceReverbChannels; reverbChannelIndex++)
			{
				directChannelVolumes[routeIndex][reverbChannelIndex] = dryMix * wetChannelVolumes[reverbChannelIndex][routeIndex - 1];
			}
			routeIndex++;
			// large
			for(u32 reverbChannelIndex = 0; reverbChannelIndex < g_NumSourceReverbChannels; reverbChannelIndex++)
			{
				directChannelVolumes[routeIndex][reverbChannelIndex] = dryMix * wetChannelVolumes[reverbChannelIndex][routeIndex - 1];
			}

			if(effectRoute == EFFECT_ROUTE_LOCAL_VOICE)
			{
				directDestinationSubmixIds[numDirectDestinations] = m_LocalVoiceSubmixId;

				for(u32 j=0; j < g_MaxOutputChannels; j++)
				{
					directChannelVolumes[numDirectDestinations][j] = wetMix;
				}

				numDirectDestinations++;
			}
		}
		break;
	}

	Assert(numDirectDestinations <= audVoiceSettings::kMaxVoiceRouteDestinations);
	for(u32 routeIndex=0; routeIndex < audVoiceSettings::kMaxVoiceRouteDestinations; routeIndex++)
	{
		if(routeIndex < numDirectDestinations)
		{
			metrics->environmentSoundMetrics->voiceSettings->Routes[routeIndex].SetSubmixId(directDestinationSubmixIds[routeIndex]);
			metrics->environmentSoundMetrics->voiceSettings->Routes[routeIndex].invertPhase = metrics->environmentSoundMetrics->shouldInvertPhase;
			ALIGNAS(16) f32 gainMatrix[8]  = {0.f};
			for(u32 channelIndex = 0; channelIndex < g_MaxOutputChannels; channelIndex++)
			{
				gainMatrix[channelIndex] = directChannelVolumes[routeIndex][channelIndex];			
			}

			metrics->environmentSoundMetrics->voiceSettings->RouteChannelVolumes[routeIndex] = 
				audMixerConnection::PackVolumeMatrix(gainMatrix);

		}
		else
		{
			metrics->environmentSoundMetrics->voiceSettings->Routes[routeIndex].SetSubmixId(-1);
		}
	}

	// also compute the virtualisation score
	const f32 totalVolume =  Min(1.f, (wetMix + dryMix) * postSubmixAttenLin);
	// NOTE: currentPeakLevel is 16bit; upscale to close to full 32bit (2^31 - 65535)
	// float(2^31)*1.f is rounded up to (2^31)+1, which leads to an overflow, which is avoided by using (2^31 - 65535)
	const u32 thisVoiceVirtualisationScore = ~0U - (u32)(totalVolume * f32(metrics->environmentSoundMetrics->currentPeakLevel * 65536U)) ;

	if(metrics->virtualisationScore == ~0U)
	{	
		metrics->virtualisationScore = thisVoiceVirtualisationScore;
	}
}

audMixerSubmix *audEnvironment::GetMasterSubmix() const
{
	return audDriver::GetMixer()->GetMasterSubmix();
}

audMixerSubmix *audEnvironment::GetSfxSubmix() const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_SFXSubmixId);
}

audMixerSubmix *audEnvironment::GetMusicSubmix() const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_MusicSubmixId);
}

audMixerSubmix* audEnvironment::GetReflectionsSourceSubmix() const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_ReflectionsSourceSubmixId);
}

audMixerSubmix* audEnvironment::GetReflectionsOutputSubmix(const u32 index) const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_ReflectionsOutputSubmixIds[index]);
}

audMixerSubmix *audEnvironment::GetSourceReverbSubmix(const u32 index) const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_ReverbSubmixIds[index]);
}

audMixerSubmix *audEnvironment::GetMusicReverbSubmix(const u32 index) const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_MusicReverbSubmixIds[index]);
}

audMixerSubmix *audEnvironment::GetVehicleEngineSubmix(const u32 index) const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleEngineSubmixIds[index]);
}

audMixerSubmix *audEnvironment::GetVehicleExhaustSubmix(const u32 index) const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_VehicleExhaustSubmixIds[index]);
}

s8 audEnvironment::GetVehicleEngineSubmixId(const u32 index) const
{
	return m_VehicleEngineSubmixIds[index];
}

s8 audEnvironment::GetVehicleExhaustSubmixId(const u32 index) const
{
	return m_VehicleExhaustSubmixIds[index];
}

audMixerSubmix *audEnvironment::GetConversationSpeechSubmix(const u32 index) const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_ConversationSpeechSubmixIds[index]);
}

s8 audEnvironment::GetConversationSpeechSubmixId(const u32 index) const
{
	return m_ConversationSpeechSubmixIds[index];
}

bool audEnvironment::IsDirectMonoEffectRoute(const u32 route)
{
	if((route >= EFFECT_ROUTE_VEHICLE_ENGINE_MIN && route <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX) ||
	   (route >= EFFECT_ROUTE_VEHICLE_EXHAUST_MIN && route <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX) ||
	   (route >= EFFECT_ROUTE_CONVERSATION_SPEECH_MIN && route <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX))
	{
		return true;
	}

	return false;
}

s32 audEnvironment::AssignVehicleEngineSubmix()
{
	for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; i++)
	{
		if(!m_VehicleEngineSubmixesInUse.IsSet(i) && audDriver::GetMixer()->GetActiveVoiceCount(m_VehicleEngineSubmixIds[i]) == 0)
		{
			m_VehicleEngineSubmixesInUse.Set(i);
			return i;
		}
	}

	return -1;
}

void audEnvironment::FreeVehicleEngineSubmix(const u32 index)
{
	m_VehicleEngineSubmixesInUse.Clear(index);
}

s32 audEnvironment::AssignVehicleExhaustSubmix()
{
	for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; i++)
	{
		if(!m_VehicleExhaustSubmixesInUse.IsSet(i) && audDriver::GetMixer()->GetActiveVoiceCount(m_VehicleExhaustSubmixIds[i]) == 0)
		{
			m_VehicleExhaustSubmixesInUse.Set(i);
			return i;
		}
	}

	return -1;
}

void audEnvironment::FreeVehicleExhaustSubmix(const u32 index)
{
	m_VehicleExhaustSubmixesInUse.Clear(index);
}

s32 audEnvironment::AssignConversationSpeechSubmix()
{
	for(u32 i = 0; i <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; i++)
	{
		if(!m_ConversationSpeechSubmixesInUse.IsSet(i) && audDriver::GetMixer()->GetActiveVoiceCount(m_ConversationSpeechSubmixIds[i]) == 0)
		{
			m_ConversationSpeechSubmixesInUse.Set(i);
			return i;
		}
	}

	return -1;
}

void audEnvironment::FreeConversationSpeechSubmix(const u32 index)
{
	m_ConversationSpeechSubmixesInUse.Clear(index);
}

s32 audEnvironment::AssignReflectionsOutputSubmix()
{
	for(u32 i = 0; i < AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES; i++)
	{
		if(!m_ReflectionsOutputSubmixesInUse.IsSet(i) && audDriver::GetMixer()->GetActiveVoiceCount(m_ReflectionsOutputSubmixIds[i]) == 0)
		{
			m_ReflectionsOutputSubmixesInUse.Set(i);
			return i;
		}
	}

	return -1;
}

void audEnvironment::FreeReflectionsOutputSubmix(const u32 index)
{
	m_ReflectionsOutputSubmixesInUse.Clear(index);
}

bool audEnvironment::IsEngineSubmixFree(const u32 index)
{
	if(!m_VehicleEngineSubmixesInUse.IsSet(index) && audDriver::GetMixer()->GetActiveVoiceCount(m_VehicleEngineSubmixIds[index]) == 0)
	{
		return true;
	}

	return false;
}

bool audEnvironment::IsExhaustSubmixFree(const u32 index)
{
	if(!m_VehicleExhaustSubmixesInUse.IsSet(index) && audDriver::GetMixer()->GetActiveVoiceCount(m_VehicleExhaustSubmixIds[index]) == 0)
	{
		return true;
	}

	return false;
}

bool audEnvironment::IsConversationSpeechSubmixFree(const u32 index)
{
	if(!m_ConversationSpeechSubmixesInUse.IsSet(index) && audDriver::GetMixer()->GetActiveVoiceCount(m_ConversationSpeechSubmixIds[index]) == 0)
	{
		return true;
	}

	return false;
}

bool audEnvironment::IsReflectionsOutputSubmixFree(const u32 index)
{
	if(!m_ReflectionsOutputSubmixesInUse.IsSet(index) && audDriver::GetMixer()->GetActiveVoiceCount(m_ReflectionsOutputSubmixIds[index]) == 0)
	{
		return true;
	}

	return false;
}

audMixerSubmix *audEnvironment::GetLocalVoiceSubmix() const
{
	return audDriver::GetMixer()->GetSubmixFromIndex(m_LocalVoiceSubmixId);
}

void audEnvironment::GameInit()
{
	m_DeafeningReverbWetness = 0.0f;
	m_DeafeningFilterCutoff = kVoiceFilterLPFMaxCutoff;
	m_PlateauScale = 0.1f;
	m_PlateauInner = 1.0f;
	m_PlateauOuter = 5.0f;
	m_OcclusionLeakage = 0.8f;
	m_HeadphoneLeakage = 0.15f;
	m_GlobalPositionedLeakage = 0.0f;
	m_BuiltUpFactor = 0.5f;
	m_EarAttenuation[0] = m_EarAttenuation[1] = 1.0f;
	m_DisableDistanceReverb = false;
	m_ShouldScalePlateaUp = true;
	m_ShouldAverageVolumes = true;
	m_ShouldDoPanningStep2 = true;
	m_ShouldDoPanningStep3 = true;
	m_ShouldDoPanningStep4 = true;
	m_ShouldDoPanningStep5 = true;
	m_ShouldLeakReverb	   = false;
	m_DontUseEnvironmentalStuff = false;
	m_EnableSourceEnvironmentMetrics = true;
	m_DeafeningAffectsEverything = false;
	m_UsingHeadphones = false;
	m_UsingAmbisonics = true;
	m_ElevationFactor = 1.f;
	for (u32 i=0; i<4; i++)
	{
		for (u32 j=0; j<3; j++)
		{
			m_SpeakerReverbWets[i][j] = 0.0f;
		}
	}
	for (u32 i=0; i<4; i++)
	{
		m_BuiltUpSpeakerFactors[i] = 0.0f;
		m_BuiltUpDirectionFactors[i] = 0.0f;
		m_BlockedSpeakerFactors[i] = 0.0f;
		m_BlockedSpeakerLinearVolumes[i] = 0.0f;
	}
	m_ListenerReverbScaling = 1.0f;
	m_ExtToIntMaxReverbDamping = 0.25f;
	m_OnlyReverb = false;
	m_UpCloseScaling = 1.f;
	m_SourceReverbScaling = 0.8f;
	m_GlobalRolloffPlateauScale = 1.0f;
	m_RolloffDampingFactor = 1.0f;

	m_StartSpeechCategoryIndex = m_EndSpeechCategoryIndex = ~0U;

	m_SpecialEffectMode = kSpecialEffectModeNormal;

	m_MaxDistanceReverbWet = 1.0f;
	m_MaxDistanceForReverb = 100.0f;
	m_DistanceLoudnessScalarMin = 0.5f;
	m_DistanceLoudnessScalarMax = 1.0f;
	m_ReverbDistanceDamping = 0.65f;
	m_ReverbDampingDistanceMin = 5.0f;
	m_ReverbDampingDistanceMax = 20.0f;
	m_ReverbSizeBuiltUp = 0.75f;
	m_ReverbSizeOpen = 0.0f;
	m_MaxBuiltUpReverbWetForLoudSounds = 0.3f;
	m_MaxBuiltUpReverbWet = 0.05f;
	m_PadSpeakerVolume = 1.f;
	m_PadSpeakerSFXVolume = 0.f;
	m_PadSpeakerEnabled = RSG_ORBIS;
}



void audEnvironment::InitCurves()
{
}

#if __BANK && !__SPU

void UpdateAmbisonicMode()
{
	g_AudioEngine.GetAmbisonics().SetEnvironmentDecoder(audAmbisonics::sm_DecoderMode);
}

void audEnvironment::AddGameWidgets(bkBank &bank)
{
	ORBIS_ONLY(bank.AddSlider("PadSpeakerGain", &g_PadSpeakerGain, 0.f, 10.f, 0.1f));

	bank.AddToggle("BypassSFX_HPF", &g_SFXHPFBypass);
	bank.AddSlider("SFX_HPFCutoff", &g_SFXHPFCutoff, 0.f, 24000.f, 0.1f);
	bank.AddSlider("SFX_HPFReso", &g_SFXHPFReso, -100.f, 24.f, 0.1f);

	const char *specialEffectModes[kNumSpecialEffectModes] = { "Normal", "Underwater", "Stoned", "PauseMenu", "SlowMotion"};
	bank.AddCombo("SpecialEffectMode", (s32*)&m_SpecialEffectMode, kNumSpecialEffectModes, (const char**)specialEffectModes);
	
	bank.PushGroup("Effects");
	
	for(s32 i = 0; i < kNumSpecialEffectModes; i++)
	{
		bank.PushGroup(specialEffectModes[i]);
		audGameEffectSettings &settings = g_EffectSettings[i];

		bank.AddToggle("SmallBypass", &settings.SmallBypass);

		bank.AddToggle("MediumBypassDelay", &settings.MediumBypassDelay);
		bank.AddSlider("MediumPredelayS" , &settings.MediumPredelay, 0.f, 1.f, 0.001f);
		bank.AddSlider("MediumFeedback" , &settings.MediumFeedback, 0.f, 1.f, 0.001f);
		bank.AddToggle("MediumBypassReverb", &settings.MediumBypassReverb);
		bank.AddSlider("MediumRoomSize" , &settings.MediumRoomSize, 0.f, 1.f, 0.001f);
		bank.AddSlider("MediumDamping" , &settings.MediumDamping, 0.f, 1.f, 0.001f);

		bank.AddToggle("LargeBypassLPF", &settings.LargeBypassLPF);
		bank.AddSlider("LargeReverbLPF" , &settings.LargeReverbLPF, 0.f, 24000.f, 0.001f);
		bank.AddToggle("LargeBypassHPF", &settings.LargeBypassHPF);
		bank.AddSlider("LargeHPF", &settings.LargeReverbHPF, 0.f, 20000.f, 0.1f);
		bank.AddToggle("LargeBypassDelay", &settings.LargeBypassDelay);
		bank.AddSlider("LargePredelayS" , &settings.LargePredelay, 0.f, 1.f, 0.001f);
		bank.AddSlider("LargeFeedback" , &settings.LargeFeedback, 0.f, 1.f, 0.001f);
		bank.AddToggle("LargeBypassReverb", &settings.LargeBypassReverb);
		bank.AddSlider("LargeRoomSize" , &settings.LargeRoomSize, 0.f, 1.f, 0.001f);
		bank.AddSlider("LargeDamping" , &settings.LargeDamping, 0.f, 1.f, 0.001f);
		
		bank.AddSlider("MinSmallSend" , &settings.MinSmallSend, 0.f, 1.f, 0.001f);
		bank.AddSlider("MinMediumSend" , &settings.MinMediumSend, 0.f, 1.f, 0.001f);
		bank.AddSlider("MinLargeSend" , &settings.MinLargeSend, 0.f, 1.f, 0.001f);

		bank.AddSlider("UnderwaterEffectLevel", &settings.UnderwaterEffectLevel, 0.f, 5.f, 0.001f);

		bank.PopGroup();
	}
	
	bank.PushGroup("Active Settings");
	{
		audGameEffectSettings &settings = m_ActiveEffectSettings;

		bank.AddText("SmallBypass", &settings.SmallBypass, true);

		bank.AddText("MediumBypassDelay", &settings.MediumBypassDelay, true);
		bank.AddText("MediumPredelayS" , &settings.MediumPredelay, true);
		bank.AddText("MediumFeedback" , &settings.MediumFeedback, true);
		bank.AddText("MediumBypassReverb", &settings.MediumBypassReverb, true);
		bank.AddText("MediumRoomSize" , &settings.MediumRoomSize, true);
		bank.AddText("MediumDamping" , &settings.MediumDamping, true);

		bank.AddText("LargeBypassLPF", &settings.LargeBypassLPF, true);
		bank.AddText("LargeReverbLPF" , &settings.LargeReverbLPF, true);
		bank.AddText("LargeBypassHPF", &settings.LargeBypassHPF, true);
		bank.AddText("LargeHPF", &settings.LargeReverbHPF, true);
		bank.AddText("LargeBypassDelay", &settings.LargeBypassDelay, true);
		bank.AddText("LargePredelayS" , &settings.LargePredelay, true);
		bank.AddText("LargeFeedback" , &settings.LargeFeedback, true);
		bank.AddText("LargeBypassReverb", &settings.LargeBypassReverb, true);
		bank.AddText("LargeRoomSize" , &settings.LargeRoomSize, true);
		bank.AddText("LargeDamping" , &settings.LargeDamping, true);
		
		bank.AddText("MinSmallSend" , &settings.MinSmallSend, true);
		bank.AddText("MinMediumSend" , &settings.MinMediumSend, true);
		bank.AddText("MinLargeSend" , &settings.MinLargeSend, true);

		bank.AddText("UnderwaterEffectLevel", &settings.UnderwaterEffectLevel, true);
	}
	bank.PopGroup();



	bank.PopGroup(); // Effects

	bank.AddToggle("EverythingToSmall", &g_EverythingToSmall);
	
	bank.AddToggle("AssertOnVolumeClamp", &g_AssertOnVolumeClamp);
	bank.PushGroup("SourceEnvironmentMetrics");
		bank.AddToggle("EnableSourceEnvironmentMetrics", &m_EnableSourceEnvironmentMetrics);
		bank.AddToggle("DisableDistanceReverb", &m_DisableDistanceReverb);
		bank.AddToggle("Don't Use Environmental Stuff", &m_DontUseEnvironmentalStuff);
		bank.AddSlider("UpCloseReverbScaling", &m_UpCloseScaling, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("SourceReverbScaling", &m_SourceReverbScaling, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("ListenerReverbScaling", &m_ListenerReverbScaling, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("Ext->Int Max Damping", &m_ExtToIntMaxReverbDamping, 0.0f, 1.0f, 0.01f);
		bank.AddToggle("Only Reverb", &m_OnlyReverb);
		bank.AddToggle("Max out Reverb", &g_MaxOutReverb);
	bank.PopGroup();

	bank.PushGroup("Panning Algorithm");
		bank.AddToggle("Mix for headphones", &m_UsingHeadphones);
		bank.AddToggle("Use ambisonic pan", &m_UsingAmbisonics);

		bank.PushGroup("Ambisonic settings");
			bank.AddSlider("Elevation factor", &m_ElevationFactor, 0.f, 1.f, 0.01f);
			bank.AddToggle("Ambisonic circle opto", &m_AmbisonicUnitCircleOpto);
			const char * nameList[kNumAmbisonicsModes] = 
			{
				"Square",
				"Itu", 
				"ItuSide",
				"Headphones",
				"Stereo",
				"ItuFront15",
				"FwRr",
				"FwRm",
				"FwRs",
				"FnRr",
				"FnRm",
				"FnRs",
				"FmRr",
				"FmRm",
				"FmRs"
			};
			bank.AddCombo("Select ambisonic decoder", (s32*)&audAmbisonics::sm_DecoderMode, kNumAmbisonicsModes, nameList, 0);
			bank.AddButton("Update ambisonic mode", CFA(UpdateAmbisonicMode));
		bank.PopGroup();

		bank.AddSlider("headphone leak", &m_HeadphoneLeakage, 0.0f, 1.0f, 0.01f);
		bank.AddToggle("Step 2 - height", &m_ShouldDoPanningStep2);
		bank.AddToggle("Step 3 - plateau", &m_ShouldDoPanningStep3);
		bank.AddSlider("PlateauScale", &m_PlateauScale, 0.0f, 10.0f, 0.01f);
		bank.AddSlider("PlateauInner", &m_PlateauInner, 0.0f, 5.0f, 0.01f);
		bank.AddSlider("PlateauOuter", &m_PlateauOuter, 0.0f, 10.0f, 0.01f);
		bank.AddToggle("ShouldScalePlateaUp", &m_ShouldScalePlateaUp);
		bank.AddToggle("AverageVolume", &m_ShouldAverageVolumes);
		bank.AddToggle("Step 4 - distance leak", &m_ShouldDoPanningStep4);
		bank.AddToggle("Step 5 - occlusion", &m_ShouldDoPanningStep5);
		bank.AddSlider("OcclusionLeakage", &m_OcclusionLeakage, 0.0f, 1.0f, 0.01f);
		bank.AddToggle("Leak reverb", &m_ShouldLeakReverb);
	bank.PopGroup();

	bank.PushGroup("Environment Game Reverb");
		bank.AddSlider("MaxDistanceReverbWet", &m_MaxDistanceReverbWet, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("MaxDistanceForReverb", &m_MaxDistanceForReverb, 16.0f, 200.0f, 1.0f);
		bank.AddSlider("DistanceLoudnessScalarMin", &m_DistanceLoudnessScalarMin, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("DistanceLoudnessScalarMax", &m_DistanceLoudnessScalarMax, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("ReverbDistanceDamping", &m_ReverbDistanceDamping, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("ReverbDampingDistanceMin", &m_ReverbDampingDistanceMin, 0.0f, 100.0f, 1.0f);
		bank.AddSlider("ReverbDampingDistanceMax", &m_ReverbDampingDistanceMax, 0.0f, 100.0f, 1.0f);
		bank.AddSlider("ReverbSizeOpen", &m_ReverbSizeOpen, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("ReverbSizeBuiltUp", &m_ReverbSizeBuiltUp, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("MaxBuiltUpReverbStrength", &m_MaxBuiltUpReverbWetForLoudSounds, 0.0f, 1.0f, 0.01f);
		bank.AddSlider("ReverbWetForLargeBuiltUp", &m_MaxBuiltUpReverbWet, 0.0f, 1.0f, 0.01f);
	bank.PopGroup();
}
#endif

void audEnvironment::EnableSourceMetrics(const bool enable)
{
	m_EnableSourceEnvironmentMetrics = enable;
}

bool audEnvironment::AreSourceMetricsEnabled() const
{
	return m_EnableSourceEnvironmentMetrics;
}

void audEnvironment::SetBuiltUpFactor(const f32 builtUpFactor)
{
	m_BuiltUpFactor = builtUpFactor;
}

f32 audEnvironment::GetBuiltUpFactor() const
{
	return m_BuiltUpFactor;
}

void audEnvironment::SetRolloffDampingFactor(const f32 rolloffDampingFactor)
{
	m_RolloffDampingFactor = rolloffDampingFactor;
}

void audEnvironment::SetBuiltUpSpeakerFactors(const f32* builtUpSpeakerFactors)
{
	Assert(builtUpSpeakerFactors);
	for (u32 i=0; i<4; i++)
	{
		m_BuiltUpSpeakerFactors[i] = builtUpSpeakerFactors[i];
	}
}

void audEnvironment::SetBuiltUpDirectionFactors(const f32* builtUpDirectionFactors)
{
	Assert(builtUpDirectionFactors);
	for (u32 i=0; i<4; i++)
	{
		m_BuiltUpDirectionFactors[i] = builtUpDirectionFactors[i];
	}
}

void audEnvironment::SetBlockedSpeakerLinearVolumes(const f32* blockedSpeakerLinearVolumes)
{
	Assert(blockedSpeakerLinearVolumes);
	for (u32 i=0; i<4; i++)
	{
		m_BlockedSpeakerLinearVolumes[i] = Clamp(blockedSpeakerLinearVolumes[i], 0.f, 1.f);
	}
}

void audEnvironment::SetBlockedSpeakerFactors(const f32* blockedSpeakerFactors)
{
	Assert(blockedSpeakerFactors);
	for (u32 i=0; i<4; i++)
	{
		m_BlockedSpeakerFactors[i] = blockedSpeakerFactors[i];
	}
}

void audEnvironment::SetGlobalRolloffPlateauScale(const f32 globalRolloffPlateauScale)
{
	m_GlobalRolloffPlateauScale = globalRolloffPlateauScale;
}

void audEnvironment::SetEarAttenuation(const F32_VerifyFinite leftAtten, const F32_VerifyFinite rightAtten)
{
	m_EarAttenuation[0] = leftAtten;
	m_EarAttenuation[1] = rightAtten;
}

void audEnvironment::SetUsingHeadphones(const bool usingHeadphones)
{
	m_UsingHeadphones = usingHeadphones;
}

void audEnvironment::SetDeafeningFilterCutoff(const f32 cutoff)
{
	m_DeafeningFilterCutoff = cutoff;
}

void audEnvironment::SetDeafeningReverbWetness(const f32 wet)
{
	m_DeafeningReverbWetness = wet;
}

void audEnvironment::SetDeafeningAffectsEverything(const bool doesIt)
{
	m_DeafeningAffectsEverything = doesIt;
}

void audEnvironment::SetSpeakerReverbWets(const f32 speakerReverbWets[4][3])
{
	for (u32 i=0; i<4; i++)
	{
		for (u32 j=0; j<3; j++)
		{
			m_SpeakerReverbWets[i][j] = speakerReverbWets[i][j];
		}
	}
}

void audEnvironment::SetGlobalPositionedLeakage(f32 globalPositionedLeakage)
{
	m_GlobalPositionedLeakage = globalPositionedLeakage;
}

void audEnvironment::SetSpeechCategoryRange(const u32 start, const u32 end)
{
	m_StartSpeechCategoryIndex = start;
	m_EndSpeechCategoryIndex = end;
}

void audEnvironment::SetDownmixMode(const audOutputMode downmixOutput)
{
	m_DownmixOutputMode = downmixOutput;
	audDriver::SetDownmixOutputMode(downmixOutput);
}

audOutputMode audEnvironment::GetDownmixMode() const
{
	return audDriver::GetDownmixOutputMode();
}

void audEnvironment::InitRouteEffects()
{
	if(!PARAM_cheapaudioeffects.Get())
	{
		// small reverb (which also hosts custom underwater effect)
		audMixerSubmix *submix = GetSourceReverbSubmix(0);

		submix->SetFlag(audMixerSubmix::InterleavedProcessing, true);

		submix->SetEffect(kSmallReverb_Reverb, rage_aligned_new(16) audEarlyReflectionEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kSmallReverb_Reverb, audEarlyReflectionEffect::Settings, ATSTRINGHASH("default_small_er", 0xFD1C7F6A));

		submix->SetEffect(kSmallReverb_UnderwaterEffect, rage_aligned_new(16) audUnderwaterEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);

		// medium reverb
		submix = GetSourceReverbSubmix(1);
		submix->SetFlag(audMixerSubmix::InterleavedProcessing, true);

		submix->SetEffect(kMediumReverb_Delay, rage_aligned_new(16) audDelayEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);

		submix->SetEffect(kMediumReverb_Reverb, rage_aligned_new(16) audReverbEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::Bypass, false);
		submix->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::RoomSize, 0.666f);	
		submix->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::Damping, 0.5f);

		// large reverb
		submix = GetSourceReverbSubmix(2);
		submix->SetFlag(audMixerSubmix::InterleavedProcessing, true);
		
		submix->SetEffect(kLargeReverb_HPF, rage_aligned_new(16) audBiquadFilterEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Bypass, true);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Mode, BIQUAD_MODE_HIGH_PASS_2POLE);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::CutoffFrequency, 100.f);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::ProcessFormat, Interleaved);

		submix->SetEffect(kLargeReverb_LPF, rage_aligned_new(16) audBiquadFilterEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::Bypass, true);
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::Mode, BIQUAD_MODE_LOW_PASS_4POLE);
#if RSG_CPU_X64
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::CutoffFrequency, 20000.f);
#else
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::CutoffFrequency, 23900.f);
#endif
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::ProcessFormat, Interleaved);

		submix->SetEffect(kLargeReverb_Delay, rage_aligned_new(16) audDelayEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffect(kLargeReverb_Reverb, rage_aligned_new(16) audReverbEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Bypass, false);
		submix->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::RoomSize, 0.66f);
		submix->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Damping, 0.05f);


#if SEPERATE_MUSIC_ROUTE
		// Music reverbs
		submix = GetMusicReverbSubmix(0);
		submix->SetFlag(audMixerSubmix::InterleavedProcessing, true);
		submix->SetEffect(kSmallReverb_Reverb, rage_aligned_new(16) audEarlyReflectionEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kSmallReverb_Reverb, audEarlyReflectionEffect::Settings, ATSTRINGHASH("default_small_er", 0xFD1C7F6A));

		submix = GetMusicReverbSubmix(1);
		submix->SetFlag(audMixerSubmix::InterleavedProcessing, true);
		submix->SetEffect(kMediumReverb_Delay, rage_aligned_new(16) audDelayEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffect(kMediumReverb_Reverb, rage_aligned_new(16) audReverbEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::Bypass, false);
		submix->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::RoomSize, 0.666f);	
		submix->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::Damping, 0.5f);

		submix = GetMusicReverbSubmix(2);		
		submix->SetFlag(audMixerSubmix::InterleavedProcessing, true);
		
		submix->SetEffect(kLargeReverb_HPF, rage_aligned_new(16) audBiquadFilterEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Bypass, true);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Mode, BIQUAD_MODE_HIGH_PASS_2POLE);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::CutoffFrequency, 100.f);
		submix->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::ProcessFormat, Interleaved);

		submix->SetEffect(kLargeReverb_LPF, rage_aligned_new(16) audBiquadFilterEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::Bypass, true);
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::Mode, BIQUAD_MODE_LOW_PASS_4POLE);
#if RSG_CPU_X64
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::CutoffFrequency, 20000.f);
#else
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::CutoffFrequency, 23900.f);
#endif
		submix->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::ProcessFormat, Interleaved);

		submix->SetEffect(kLargeReverb_Delay, rage_aligned_new(16) audDelayEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffect(kLargeReverb_Reverb, rage_aligned_new(16) audReverbEffect4(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Bypass, false);
		submix->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::RoomSize, 0.66f);
		submix->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Damping, 0.05f);
#endif	

		for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN; i++)
		{
			submix = GetVehicleEngineSubmix(i);
			submix->SetEffect(0, rage_aligned_new(16) synthCoreDspEffect(), RAGE_SPEAKER_MASK_MONO);
		}

		for(u32 i = 0; i <= EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN; i++)
		{
			submix = GetVehicleExhaustSubmix(i);
			submix->SetEffect(0, rage_aligned_new(16) synthCoreDspEffect(), RAGE_SPEAKER_MASK_MONO);
		}

		for(u32 i = 0; i <= EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN; i++)
		{
			submix = GetConversationSpeechSubmix(i);
			submix->SetEffect(0, rage_aligned_new(16) synthCoreDspEffect(), RAGE_SPEAKER_MASK_MONO);
		}

		submix = GetReflectionsSourceSubmix();
		submix->SetEffect(0, rage_aligned_new(16) audVariableDelayEffect(), RAGE_SPEAKER_MASK_MONO);

		for(u32 i = 0; i < g_MaxOutputChannels; i++)
		{
			submix->SetEffectParam(0, audVariableDelayEffect::DelayInSamples + i, kMixBufNumSamples);
		}

		submix = GetLocalVoiceSubmix();
		s32 index = 0;

#if __WIN32 && 0
		submix->SetEffect(index, rage_aligned_new(16) synthCoreDspEffect(), RAGE_SPEAKER_MASK_MONO);
		submix->SetEffectParam(index, synthCorePcmSource::Params::CompiledSynthNameHash, ATSTRINGHASH("LOUDHAILER_FX_MONO", 0x7EAA2B0));
		index++;
#endif

		submix->SetEffect(index, rage_aligned_new(16) audBiquadFilterEffect(), RAGE_SPEAKER_MASK_MONO);
		submix->SetEffectParam(index, audBiquadFilterEffect::CutoffFrequency, 1000.f);
		submix->SetEffectParam(index, audBiquadFilterEffect::Bandwidth, 1500.f);
		submix->SetEffectParam(index, audBiquadFilterEffect::Gain, 15.f);
		submix->SetEffectParam(index, audBiquadFilterEffect::Mode, BIQUAD_MODE_BAND_PASS_4POLE);

		submix->SetEffect(index, rage_aligned_new(16) audDelayEffect(), RAGE_SPEAKER_MASK_MONO);
		submix->SetEffectParam(index, audDelayEffect::FrontLeftDelayTime, 150U);
		submix->SetEffectParam(index, audDelayEffect::FrontLeftGain, -9.f);
		submix->SetEffectParam(index, audDelayEffect::FrontLeftDryMix, 0.f);
		submix->SetEffectParam(index, audDelayEffect::FrontLeftFeedback, -100.f);

		// Music
		submix = GetMusicSubmix();
		submix->SetEffect(0, rage_aligned_new(16) synthCoreDspEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(0, synthCorePcmSource::Params::CompiledSynthNameHash, ATSTRINGHASH("RADIO_DSP", 0xC0C9A6F4));
		submix->SetEffectParam(0, ATSTRINGHASH("signalLevel", 0xFAF72E9), 1.f);
		submix->SetEffectParam(0, ATSTRINGHASH("Bypass", 0x10F3D95C), true);

		// SFX
		submix = GetSfxSubmix();
		submix->SetEffect(0, rage_aligned_new(16) audBiquadFilterEffect(), RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT);
		submix->SetEffectParam(0, audBiquadFilterEffect::Bypass, true);
	}

	// Setup special effect settings
	// stoned
	g_EffectSettings[kSpecialEffectModeStoned].LargeBypassDelay = true;
	g_EffectSettings[kSpecialEffectModeStoned].LargeBypassLPF = false;
	g_EffectSettings[kSpecialEffectModeStoned].LargeReverbLPF = 4000.f;
	g_EffectSettings[kSpecialEffectModeStoned].LargeReverbHPF = 50.f;
	g_EffectSettings[kSpecialEffectModeStoned].LargeRoomSize = 1.f;
	g_EffectSettings[kSpecialEffectModeStoned].LargeDamping = 0.f;
	g_EffectSettings[kSpecialEffectModeStoned].MinLargeSend = 1.f;
	g_EffectSettings[kSpecialEffectModeStoned].MinSmallSend = 1.f;

	// underwater
	g_EffectSettings[kSpecialEffectModeUnderwater].SmallBypass = true;
	g_EffectSettings[kSpecialEffectModeUnderwater].MediumBypassDelay = true;
	g_EffectSettings[kSpecialEffectModeUnderwater].MediumBypassReverb = true;
	g_EffectSettings[kSpecialEffectModeUnderwater].LargeBypassHPF = true;
	g_EffectSettings[kSpecialEffectModeUnderwater].LargeBypassDelay = true;
	g_EffectSettings[kSpecialEffectModeUnderwater].LargeBypassReverb = true;
	g_EffectSettings[kSpecialEffectModeUnderwater].UnderwaterEffectLevel = 3.f;
	g_EffectSettings[kSpecialEffectModeUnderwater].MinSmallSend = 0.5f;	

	// Pause menu (menu music)
	g_EffectSettings[kSpecialEffectModePauseMenu].SmallBypass = true;
	g_EffectSettings[kSpecialEffectModePauseMenu].LargeBypassHPF = true;
	g_EffectSettings[kSpecialEffectModePauseMenu].MediumBypassDelay = true;
	g_EffectSettings[kSpecialEffectModePauseMenu].LargeDamping = 0.15f;
	g_EffectSettings[kSpecialEffectModePauseMenu].LargeRoomSize = 0.9f;
	g_EffectSettings[kSpecialEffectModePauseMenu].MediumDamping = 0.f;
	g_EffectSettings[kSpecialEffectModePauseMenu].MediumRoomSize = 0.75f;
}

void audEnvironment::CalculateEnvironmentSoundMetricsForSingleListener(audEnvironmentMetricsInternal *metrics, u32 listenerId)
{
	// Do all the regular stuff - volume attenuation, doppler calculation, etc. Then call another overridable function for the fun occlusion/reverb stuff.
	// We create a new inputMetric and outputMetric, which we pass into that other fn, then combine.
	// I don't particularly like that. (MdS)
	PF_FUNC(CalcEnvMetricsForSingleListener);

	f32 distanceToPanningListener;
	f32 distanceRelativeToVolumeListener;
	Vec3V positionRelativeToPanningListener;
	Vec3V positionRelativeToVolumeListener;
	if(metrics->environmentSoundMetrics->environmentGameMetric.GetUseOcclusionPath())
	{
		distanceRelativeToVolumeListener = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionPathDistance();
		// @OCC TODO - Determine how positionRelativePan should fit into the path based repositioning
		//positionRelativeToPanningListener = ComputePositionRelativeToPanningListener(metrics->environmentSoundMetrics->GetOcclusionPathPosition(), listenerId);
		positionRelativeToPanningListener = ComputePositionRelativeToPanningListener(metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionPathPosition(), listenerId, 
																					metrics->environmentSoundMetrics->positionRelativePan);
	}
	else
	{
		//Convert world-relative position to listener-relative.
		distanceRelativeToVolumeListener = ComputeDistanceRelativeToVolumeListener(metrics->environmentSoundMetrics->position, listenerId);
		positionRelativeToPanningListener = ComputePositionRelativeToPanningListener(metrics->environmentSoundMetrics->position, listenerId, metrics->environmentSoundMetrics->positionRelativePan);
	}
	distanceToPanningListener = ComputeDistanceToPanningListener(metrics->environmentSoundMetrics->position,listenerId);

	Assert(FPIsFinite(distanceRelativeToVolumeListener));
	if (!FPIsFinite(distanceRelativeToVolumeListener))
	{
		// Everything's set up to very dull, silent, defaults
		return;
	}

#if RSG_ORBIS
	// PadSpeaker is mono, so ensure anything Centre speaker masked is routed correct
	if(m_PadSpeakerEnabled
		&& metrics->environmentSoundMetrics->routingMetric.effectRoute == EFFECT_ROUTE_PADSPEAKER_1 
		&& metrics->environmentSoundMetrics->speakerMask == 0x4)
	{
		const_cast<audEnvironmentSoundMetrics*>(metrics->environmentSoundMetrics)->speakerMask = RAGE_SPEAKER_MASK_MONO;
	}
#endif

	f32 distanceAttenuation = 0.0f;
	f32 directionalMicAttenuation = 0.0f;

	metrics->dopplerFrequencyScalingFactor = 1.f;
	metrics->LPFCutoff = metrics->environmentSoundMetrics->LPFCutoff;

	if(metrics->environmentSoundMetrics->shouldAttenuateOverDistance)
	{
		f32 scale = metrics->environmentSoundMetrics->volumeCurveScale;
		scale *= g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].DistanceRollOffScale.GetFloat32_FromFloat16();

		scale *= metrics->environmentSoundMetrics->environmentGameMetric.GetRolloffFactor();	
		scale *= GetListener(listenerId).RollOff;

		f32 additionalPlateau = (f32)metrics->environmentSoundMetrics->volumeCurvePlateau;

		// Work out our built up rolloff, based on where we are. 
		// Work out our position relative to the primary mic.
		// By position relative, I mean without any orientation change, just a positional offset, since our built-ups are NSEW based
		Vec3V listenerPosition = GetPanningListenerPosition();
		Vec3V relative2dPosition = metrics->environmentSoundMetrics->position - listenerPosition;
		relative2dPosition.SetZ(ScalarV(V_ZERO));
		Vec3V positionOnUnitCircle = NormalizeSafe(relative2dPosition, Vec3V(V_ZERO));
		Vec3V directions[4];
		directions[0] = Vec3V(V_Y_AXIS_WZERO); //N
		directions[1] = Vec3V(V_X_AXIS_WZERO); //E
		directions[2] = -Vec3V(V_Y_AXIS_WZERO); //S
		directions[3] = -Vec3V(V_X_AXIS_WZERO); //W
		f32 directionalBuiltUpFactor = 0.0f;
		for (int i=0; i<4; i++)
		{
			f32 dotProd = Dot(positionOnUnitCircle, directions[i]).Getf();
			// Don't care about ones more than 90degs away
			dotProd = Max(0.0f, dotProd);
			directionalBuiltUpFactor += (dotProd*dotProd * m_BuiltUpDirectionFactors[i]);
		}

		// Now morph between that directional one and the centre one.
		f32 distanceAwayOnUnitCircle = Mag(relative2dPosition).Getf();
		f32 distanceBuiltUpRatio = Min(distanceAwayOnUnitCircle/125.0f, 1.0f);
		f32 builtUpFactor = (distanceBuiltUpRatio * directionalBuiltUpFactor) + (1.0f - distanceBuiltUpRatio) * m_BuiltUpFactor;
//		f32 builtUpRolloff = m_BuiltUpToRolloff.CalculateValue(builtUpFactor);
		
		static const audThreePointPiecewiseLinearCurve builtUpRolloffCurve(0.0f, 1.0f, 0.5f, 1.0f, 1.0f, 1.0f);
		f32 builtUpRolloff = builtUpRolloffCurve.CalculateValue(builtUpFactor);

//		f32 scaledBuiltUpRolloff = m_RolloffDampingFactor * g_GlobalRolloff * builtUpRolloff + (1.0f-m_RolloffDampingFactor);
		f32 scaledBuiltUpRolloff = m_RolloffDampingFactor * builtUpRolloff + (1.0f-m_RolloffDampingFactor);

		distanceAttenuation = GetDistanceAttenuationWithBuiltUpRolloff(scale,distanceRelativeToVolumeListener, scaledBuiltUpRolloff, additionalPlateau);

		
		// Also work out directional mic attenuation here, as the two sensibly go together
		f32 frontRearRatio = 0.f;
		directionalMicAttenuation = GetDirectionalMicAttenuation(listenerId,positionRelativeToPanningListener,distanceToPanningListener, &frontRearRatio);
		// Now scale down the effect of that by distance, so things in focus get affected, but the overall ambience doesn't.
		static const audThreePointPiecewiseLinearCurve directionalMicCurve(0.0f, 0.0f, 1.1f, 0.0f, 2.0f, 1.0f);
		f32 directionalMicAttenuationScaling = directionalMicCurve.CalculateValue(distanceRelativeToVolumeListener);
		directionalMicAttenuation *= directionalMicAttenuationScaling;
		frontRearRatio = 1.0f - ((1.0f-frontRearRatio) * directionalMicAttenuationScaling);
		// scale down any distance/dir-mic attenuation by any occlusion metric factor. Try just scaling the db value :-/
	
		f32 distanceDamping = metrics->environmentSoundMetrics->environmentGameMetric.GetDistanceAttenuationDamping();
		distanceAttenuation *= distanceDamping;
		directionalMicAttenuation *= distanceDamping;
		frontRearRatio = 1.0f - ((1.0f-frontRearRatio) * distanceDamping);
	

		// disable rear filtering if outputting surround or if the category has disabled it
		if(m_DownmixOutputMode < AUD_OUTPUT_5_1 && !metrics->environmentSoundMetrics->rearAttenuationDisabled && !metrics->environmentSoundMetrics->rearFilteringDisabled)
		{
			f32 rearFilterCutoff = 18000.0f * (1.0f - frontRearRatio) + kVoiceFilterLPFMaxCutoff * frontRearRatio;
			metrics->LPFCutoff = Min(metrics->LPFCutoff, (u16)rearFilterCutoff);
		}

	}//should attenuate over distance
	// if we're going down the positioned route filter for deafening stuff
	if (m_DeafeningAffectsEverything || metrics->environmentSoundMetrics->routingMetric.effectRoute == EFFECT_ROUTE_POSITIONED || metrics->environmentSoundMetrics->routingMetric.effectRoute == EFFECT_ROUTE_POSITIONED_MUSIC)
	{
		metrics->LPFCutoff = Min(metrics->LPFCutoff, (u16)m_DeafeningFilterCutoff);
	}

	if(metrics->environmentSoundMetrics->rearAttenuationDisabled && GetListener(listenerId).RearAttenuationType != 1)//REAR_ATTENUATION_ALWAYS
	{
		// disable rear attenuation for Categories flagged to ignore it
		directionalMicAttenuation = 0.f;
	}
	// Update our metrics with the calculated volume changes
	const f32 attenuationLinear = audDriverUtil::ComputeLinearVolumeFromDb(distanceAttenuation + directionalMicAttenuation);
	metrics->postSubmixVolumeAttenuation.ScaleLinear(attenuationLinear);

	// Now we do the fancy environmental stuff
	if (metrics->environmentSoundMetrics->shouldApplyEnvironmentalEffects || (GetSpecialEffectMode() != kSpecialEffectModeNormal && (metrics->environmentSoundMetrics->routingMetric.effectRoute == EFFECT_ROUTE_POSITIONED || metrics->environmentSoundMetrics->routingMetric.effectRoute == EFFECT_ROUTE_POSITIONED_MUSIC)))
	{
		CalculateEnvironmentEffectsMetricsForSingleListener(metrics, listenerId);
	}
	else
	{
		// Still use the sound set on the sound
		metrics->reverbWets[0] = metrics->environmentSoundMetrics->minSmallSend;
		metrics->reverbWets[1] = metrics->environmentSoundMetrics->minMediumSend;
		metrics->reverbWets[2] = metrics->environmentSoundMetrics->minLargeSend;

		metrics->reverbDry = 1.f;
		
		for (u32 i=0; i<4; i++)
		{

			metrics->relativeWetChannelVolumes[i][0] = 0.f;
			metrics->relativeWetChannelVolumes[i][1] = 0.f;
			metrics->relativeWetChannelVolumes[i][2] = 0.f;
		}
	}

	// Calculate the channel volumes last, because it might depend on source reverb levels, final volume, etc
	if(metrics->environmentSoundMetrics->hasQuadSpeakerLevels)
	{
		ComputeSpeakerVolumesFromQuadLevels(metrics, audDriver::GetNumOutputChannels());
		metrics->dopplerFrequencyScalingFactor = 1.f;
	}
	else if(metrics->environmentSoundMetrics->speakerMask != 0)
	{
		ComputeSpeakerVolumesFromSpeakerMask(metrics, audDriver::GetNumOutputChannels());
		metrics->dopplerFrequencyScalingFactor = 1.f;
		for (int i = RAGE_SPEAKER_ID_FRONT_LEFT; i <= RAGE_SPEAKER_ID_BACK_RIGHT; i++)
		{
			if (metrics->environmentSoundMetrics->routingMetric.effectRoute != EFFECT_ROUTE_MUSIC && 
				metrics->environmentSoundMetrics->routingMetric.effectRoute != EFFECT_ROUTE_FRONT_END &&
				!IsDirectMonoEffectRoute(metrics->environmentSoundMetrics->routingMetric.effectRoute))
			{
				if (i == RAGE_SPEAKER_ID_FRONT_LEFT || i == RAGE_SPEAKER_ID_BACK_LEFT)
				{
					metrics->relativeChannelVolumes[i].ScaleLinear(m_EarAttenuation[0]);
				}
				else if (i == RAGE_SPEAKER_ID_FRONT_RIGHT || i == RAGE_SPEAKER_ID_BACK_RIGHT)
				{
					metrics->relativeChannelVolumes[i].ScaleLinear(m_EarAttenuation[1]);
				}
			}
		}
	}
	else if(metrics->environmentSoundMetrics->pan >= 0)
	{
		ComputeSpeakerVolumesFromPan(metrics, audDriver::GetNumOutputChannels());

		if(!IsDirectMonoEffectRoute(metrics->environmentSoundMetrics->routingMetric.effectRoute))
		{
			metrics->dopplerFrequencyScalingFactor = 1.f;
		}
	}
	else
	{
		ComputeSpeakerVolumesFromPosition(positionRelativeToPanningListener, metrics, audDriver::GetNumOutputChannels());
	}

	// We need to back up our wet channel volumes which contain the "up-close" directional reverb, as the copy fn overwrites them
	f32 wetChannelVolumes[4][g_NumSourceReverbs];
	for (u32 i=0; i<4; i++)
	{
		for (u32 j=0; j<g_NumSourceReverbs; j++)
		{
			wetChannelVolumes[i][j] = metrics->relativeWetChannelVolumes[i][j].GetLinear();
		}
	}

	
	CopyDryVolumesToReverbs(metrics);
	
	// Add in the local speaker reverb levels, if we're applying environmental effects - and the backed-up ones
#if __BANK
	if(g_EverythingToSmall)
	{
		for(u32 i=0; i<g_NumSourceReverbs; i++)
		{
			if(i == 0)
			{
				for (u32 j=0; j<4; j++)
				{
					metrics->relativeWetChannelVolumes[j][i] = 
					Max(1.f*wetChannelVolumes[j][i]*m_UpCloseScaling, 
					Max(1.f*metrics->relativeWetChannelVolumes[j][i].GetLinear(), 
					g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].SourceReverbDamping.GetFloat32_FromFloat16() * m_ListenerReverbScaling*m_SpeakerReverbWets[j][i]));
				}
			}
			else
			{
				for (u32 j=0; j<4; j++)
				{
					metrics->relativeWetChannelVolumes[j][i] = 0.f;
				}
			}
		}
	}
	else
#endif // __BANK
	{
		if (metrics->environmentSoundMetrics->shouldApplyEnvironmentalEffects  || GetSpecialEffectMode() != kSpecialEffectModeNormal)
		{
			f32 sourceReverbDamping = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].SourceReverbDamping.GetFloat32_FromFloat16();

			if(audEnvironment::GetIsListenerInInterior() && GetSpecialEffectMode() == kSpecialEffectModeNormal)
			{
				// Dampen smaller interior reverbs less than large ones, so speech is wet in small rooms and drier in large rooms
				f32 categoryInteriorReverbDamping = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].InteriorReverbDamping.GetFloat32_FromFloat16();
				f32 interiorReverbDamping[g_NumSourceReverbs];
				interiorReverbDamping[0] = 1.0f;
				interiorReverbDamping[1] = Lerp(categoryInteriorReverbDamping, 0.5f, 1.0f);
				interiorReverbDamping[2] = categoryInteriorReverbDamping;

				// Dampen reverb based on distance, so when a sound is closer it has less reverb.  This will simulate the reverb volume
				// changing less due to distance than the source volume.
				f32 reverbDistanceDamping[g_NumSourceReverbs]; 
				reverbDistanceDamping[0] = RampValueSafe(distanceRelativeToVolumeListener, 5.0f, 10.0f, 1.0f, 1.0f);
				reverbDistanceDamping[1] = RampValueSafe(distanceRelativeToVolumeListener, 5.0f, 20.0f, 0.55f, 1.0f);
				reverbDistanceDamping[2] = RampValueSafe(distanceRelativeToVolumeListener, 5.0f, 30.0f, 0.55f, 1.0f);

				// Dampen listener reverb for outside sounds based on how loud they are
				f32 extToIntListReverbDamping = 1.0f;
				if(!metrics->environmentSoundMetrics->environmentGameMetric.GetIsInInterior())
				{
					const f32 howLoud = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].EnvironmentalLoudness.GetFloat32_FromFloat16() * metrics->environmentSoundMetrics->environmentalLoudness;
					extToIntListReverbDamping = Lerp(howLoud, m_ExtToIntMaxReverbDamping, 1.0f);
					extToIntListReverbDamping = Clamp(extToIntListReverbDamping, 0.0f, 1.0f);
				}

				for(u32 i = 0; i < g_NumSourceReverbs; i++)
				{
					for(u32 j = 0; j < 4; j++)
					{
						metrics->relativeWetChannelVolumes[j][i] = 
							Max(m_SourceReverbScaling * wetChannelVolumes[j][i] * m_UpCloseScaling * reverbDistanceDamping[i] * interiorReverbDamping[i], 
								m_SourceReverbScaling * metrics->relativeWetChannelVolumes[j][i].GetLinear() * reverbDistanceDamping[i] * interiorReverbDamping[i], 
								sourceReverbDamping * m_ListenerReverbScaling * extToIntListReverbDamping * m_SpeakerReverbWets[j][i] * reverbDistanceDamping[i] * interiorReverbDamping[i]);
					}
				}
			}
			else
			{
				// Dampen the reverb if we're up close to simulate the fact that reverb will not attenuate over distance the same as the source
				// However, dampen that by the environmental loudness, so something that's really loud will still get full verb even when close
				const f32 howLoud = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].EnvironmentalLoudness.GetFloat32_FromFloat16() * metrics->environmentSoundMetrics->environmentalLoudness;
				const f32 closeProximityReverbDampingScalar = Lerp(howLoud, m_ReverbDistanceDamping, 1.0f);
				const f32 reverbDistanceDamping = RampValueSafe(distanceRelativeToVolumeListener, m_ReverbDampingDistanceMin, m_ReverbDampingDistanceMax, closeProximityReverbDampingScalar, 1.0f);

				switch(GetSpecialEffectMode())
				{
				case kSpecialEffectModeNormal:
				case kSpecialEffectModePauseMenu:
				case kSpecialEffectModeSlowMotion:
					for(u32 i=0; i<g_NumSourceReverbs; i++)
					{
						for (u32 j=0; j<4; j++)
						{
							metrics->relativeWetChannelVolumes[j][i] = 
								Max(m_SourceReverbScaling * wetChannelVolumes[j][i] * m_UpCloseScaling * reverbDistanceDamping, 
								m_SourceReverbScaling * metrics->relativeWetChannelVolumes[j][i].GetLinear() * reverbDistanceDamping, 
								sourceReverbDamping*m_ListenerReverbScaling*m_SpeakerReverbWets[j][i] * reverbDistanceDamping);
						}
					}
					break;
				case kSpecialEffectModeStoned:				
					{
						const float stonedWet = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].StonedWetLevel.GetFloat32_FromFloat16();
						for(u32 i=0; i<g_NumSourceReverbs; i++)
						{
							for (u32 j=0; j<4; j++)
							{
								metrics->relativeWetChannelVolumes[j][i] = 
									stonedWet * Max(m_SourceReverbScaling * wetChannelVolumes[j][i] * m_UpCloseScaling * reverbDistanceDamping, 
										m_SourceReverbScaling * metrics->relativeWetChannelVolumes[j][i].GetLinear() * reverbDistanceDamping, 
										sourceReverbDamping*m_ListenerReverbScaling*m_SpeakerReverbWets[j][i] * reverbDistanceDamping);
							}
						}
					}
					break;
				case kSpecialEffectModeUnderwater:
					{
						const float underwaterWet = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].UnderwaterWetLevel.GetFloat32_FromFloat16();
						// Position with dry, scaled by UnderwaterWetLevel in category
						for(u32 j = 0; j < 4; j++)
						{
	
							metrics->relativeWetChannelVolumes[j][0] = underwaterWet * metrics->relativeDryChannelVolumes[j].GetLinear();
						}

						// scale dry by (1 - wet)
						const float dryVol = 1.f - Min(1.f, underwaterWet);
						for(u32 j = 0; j < g_MaxOutputChannels; j++)
						{
							metrics->relativeDryChannelVolumes[j].SetLinear(metrics->relativeDryChannelVolumes[j].GetLinear() * dryVol);
						}

						// silence medium/large sends
						for(u32 i=1; i<g_NumSourceReverbs; i++)
						{
							for (u32 j=0; j<4; j++)
							{
								metrics->relativeWetChannelVolumes[j][i] = 0.f;
							}										
						}
					}
					break;
				default:
					Assert(0);
					break;
				}
			}
		}
	}

	//Compute doppler frequency scaling.		
	if(metrics->environmentSoundMetrics->positionHasUpdated && (metrics->environmentSoundMetrics->shouldAttenuateOverDistance || IsDirectMonoEffectRoute(metrics->environmentSoundMetrics->routingMetric.effectRoute)))
	{
		// Work out our estimated position last frame 30ms ago
		Vec3V position30msAgo = metrics->environmentSoundMetrics->position - metrics->environmentSoundMetrics->velocity;

		Vec3V cameraVelocity = GetListener(listenerId).Velocity;
		Vec3V cameraPosition = GetListener(listenerId).VolumeMatrix.GetCol3();
		Vec3V cameraPosition30msAgo = cameraPosition - cameraVelocity;

		f32 distanceForDoppler = Mag(metrics->environmentSoundMetrics->position - cameraPosition).Getf();
		f32 distanceForDopplerLastFrame = Mag(position30msAgo - cameraPosition30msAgo).Getf();

		f32 relativeDistanceDelta = distanceForDoppler - distanceForDopplerLastFrame;
		metrics->dopplerFrequencyScalingFactor = ComputeDopplerFrequencyScalingFactor(relativeDistanceDelta, 30, metrics->environmentSoundMetrics->dopplerFactor);
	}
		
	const float dryAtten = m_OnlyReverb ? 0.f : metrics->environmentSoundMetrics->environmentGameMetric.GetDryLevel();
	for (int i=0; i< metrics->relativeDryChannelVolumes.GetMaxCount(); i++)
	{
		metrics->relativeDryChannelVolumes[i].ScaleLinear(dryAtten);
	}
}

void audEnvironment::CalculateEnvironmentEffectsMetricsForSingleListener(audEnvironmentMetricsInternal *metrics, u32 listenerId)
{
	PF_FUNC(CalcEnvFXMetricsForSingleListener);

	f32 distanceToVolumeListener;
	Vec3V positionRelativeToPanningListener;
	if(metrics->environmentSoundMetrics->environmentGameMetric.GetUseOcclusionPath())
	{
		distanceToVolumeListener = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionPathDistance();
		positionRelativeToPanningListener = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionPathPosition();
	}
	else
	{
		distanceToVolumeListener = ComputeDistanceRelativeToVolumeListener(metrics->environmentSoundMetrics->position, listenerId);
		positionRelativeToPanningListener = ComputePositionRelativeToPanningListener(metrics->environmentSoundMetrics->position, listenerId);
	}

	// Work out our speaker direction contributions 
	Vec3V positionOnUnitCircle = positionRelativeToPanningListener;
	positionOnUnitCircle.SetZ(ScalarV(V_ZERO));
	positionOnUnitCircle = NormalizeSafe(positionOnUnitCircle, Vec3V(V_ZERO));

	const Vec3V speakerPositions[4] = {
		Vec3V(V4VConstant<0xBF350481, 0x3F350481, 0, 0>()), // FL
		Vec3V(V4VConstant<0x3F350481, 0x3F350481, 0, 0>()), // FR
		Vec3V(V4VConstant<0xBF350481, 0xBF350481, 0, 0>()), // BL
		Vec3V(V4VConstant<0x3F350481, 0xBF350481, 0, 0>()), // BL
	};

	ScalarV channelProportionV[4];
	for (int i=0; i<4; i++)
	{
		ScalarV dotProd = Dot(positionOnUnitCircle, speakerPositions[i]);

		// Don't care about ones more than 90degs away
		dotProd = Max(ScalarV(V_ZERO), dotProd);
		channelProportionV[i] = dotProd*dotProd;
	}

	Vec3V normalisedPositionRelativeToPanningListener = NormalizeSafe(positionRelativeToPanningListener, Vec3V(V_ZERO));
	ScalarV elevation = Abs(Dot(normalisedPositionRelativeToPanningListener, Vec3V(V_Z_AXIS_WZERO))); // 0 for not at all elevated, 1 for fully

	float channelProportion[4];
	for (int i=0; i<4; i++)
	{
		// Linearly interp between primary volume and 0.5 as elevation goes from 0 (flat) to 1 (90 degrees)
		// 0.5, 0.25? Who can say.
		channelProportion[i] = Lerp(elevation, channelProportionV[i], ScalarV(V_QUARTER)).Getf();
	}

	// We'll artificially pump up reverb for sounds/categories that are marked as being loud to simulate a louder sound.
	const f32 howLoud = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].EnvironmentalLoudness.GetFloat32_FromFloat16() * metrics->environmentSoundMetrics->environmentalLoudness;
	//naAssert(howLoud <= 0.1f);


	// REVERB
	// Source Reverb -		The reverb on a sound due to its local environment � navmesh or interior. It has the same directionality as the source. 
	//						So the listener out in the open and the source in an alleyway will have the sound and a small reverb both come from the direction of the source. 
	// Distance Reverb -	This is the amount of reverb a sound gets purely due to its distance from the listener, wetter the further away
	//						This ignores local environment (interiors, alleyways, underpasses, etc) and is JUST distance-based. 
	//						Wetness is determined by distance itself (and damping) and size from the BuiltUpFactor,
	//						in the open but a sound comes from a long way away in a built-up area, it gets the city-like reverb. 
	//						Distance reverb is panned to come from the same direction as the dry source.
	// Built-Up Reverb -	Uses EnvironmentalLoudess metric (�howLoud�) and lets a very loud sound that�s close to the listener (no DistanceReverb) 
	//						and in a local environment (navmesh) with no reverb still sound wet � eg sirens.
	//						The idea is that these sounds travel away from the listener, interact with the �city�, and come back.
	//						The wetness is determined by the environmentalLoudness, and the size is determined by the built-up factor, so in the city a loud sound has a lot of large verb
	//						Example:  Turning a siren on at the edge of Central Park. You hear a big swirling reverb from the city side, and nothing from the park.

	// Combine our distance and source reverb params:
	// First, work out the three reverbs' settings for each, and then combine.
	// Source Reverb:
	f32 newSourceReverbWets[g_NumSourceReverbs];
	f32 sourceReverbDamping = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].SourceReverbDamping.GetFloat32_FromFloat16();
	newSourceReverbWets[0] = metrics->environmentSoundMetrics->environmentGameMetric.GetReverbSmall() * sourceReverbDamping;
	newSourceReverbWets[1] = metrics->environmentSoundMetrics->environmentGameMetric.GetReverbMedium() * sourceReverbDamping;
	newSourceReverbWets[2] = metrics->environmentSoundMetrics->environmentGameMetric.GetReverbLarge() * sourceReverbDamping;

	// sound and/or special effects can specify minimum send levels
	newSourceReverbWets[0] = Max(metrics->environmentSoundMetrics->minSmallSend, newSourceReverbWets[0]);
	newSourceReverbWets[1] = Max(metrics->environmentSoundMetrics->minMediumSend, newSourceReverbWets[1]);
	newSourceReverbWets[2] = Max(metrics->environmentSoundMetrics->minLargeSend, newSourceReverbWets[2]);

	if(GetSpecialEffectMode() != kSpecialEffectModeNormal)
	{
		newSourceReverbWets[0] = Max(m_ActiveEffectSettings.MinSmallSend, newSourceReverbWets[0]);
		newSourceReverbWets[1] = Max(m_ActiveEffectSettings.MinMediumSend, newSourceReverbWets[1]);
		newSourceReverbWets[2] = Max(m_ActiveEffectSettings.MinLargeSend, newSourceReverbWets[2]);
	}

	f32 sourceReverbDry = 1.0f;// - sourceReverbWet;

	//  Distance Reverb:
	f32 distanceReverbDry = 1.0f;
	f32 distanceReverbWets[g_NumSourceReverbs] = {0.0f};
	f32 distanceReverbDamping = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].DistanceReverbDamping.GetFloat32_FromFloat16();

	// We don't want to do any distance metrics based on built-up factor etc. if both the source and the listener are inside
	if(!audEnvironment::GetIsListenerInInterior() || !metrics->environmentSoundMetrics->environmentGameMetric.GetIsInInterior())
	{
		const f32 distanceFactor = RampValueSafe(distanceToVolumeListener, 15.0f, m_MaxDistanceForReverb, 0.0f, 1.0f);
		
		f32 distanceReverbWet = distanceFactor * m_MaxDistanceReverbWet;

		// Scale our wet by the environmental loudness, so loud sounds have more reverb due to distance
		const f32 distanceLoudnessScalar = Lerp(howLoud, m_DistanceLoudnessScalarMin, m_DistanceLoudnessScalarMax);

		distanceReverbWet *= distanceLoudnessScalar;
		distanceReverbWet *= distanceReverbDamping;
		distanceReverbWet = Max(distanceReverbWet, m_DeafeningReverbWetness);
		distanceReverbDry = 1.0f - distanceReverbWet;

		// Determine the distant built up factor based on interpolating between centre one and proportion of speaker one
		// The further away we are from the listener, the more we want to use the directional ones
		f32 distanceBuiltUpFactor = 0.0f;
		for (u32 i=0; i<4; i++)
		{
			f32 distanceBuiltUpFactorForSpeaker = Lerp(distanceFactor, m_BuiltUpFactor, m_BuiltUpSpeakerFactors[i]);
			distanceBuiltUpFactor += (channelProportion[i] * distanceBuiltUpFactorForSpeaker);
		}
		
		// Interpolate between the open and built up sizes based on how built up we are, so the more built up the greater the size.
		const f32 distanceReverbSize = Lerp(distanceBuiltUpFactor, m_ReverbSizeOpen, m_ReverbSizeBuiltUp);

		static const audThreePointPiecewiseLinearCurve dist0(0.0f, 1.0f, 0.25f, 0.71f, 0.5f, 0.0f);

		distanceReverbWets[0] = distanceReverbWet * dist0.CalculateValue(distanceReverbSize);
		if (distanceReverbSize<=0.5f)
		{
			static const audThreePointPiecewiseLinearCurve dist1sm(0.0f, 0.0f, 0.25f, 0.71f, 0.5f, 1.0f);
			distanceReverbWets[1] = distanceReverbWet * dist1sm.CalculateValue(distanceReverbSize);
		}
		else
		{
			static const audThreePointPiecewiseLinearCurve dist1big(0.5f, 1.0f, 0.75f, 0.71f, 1.0f, 0.0f);
			distanceReverbWets[1] = distanceReverbWet * dist1big.CalculateValue(distanceReverbSize);
		}
		static const audThreePointPiecewiseLinearCurve dist2(0.5f, 0.0f, 0.75f, 0.71f, 1.0f, 1.0f);
		distanceReverbWets[2] = distanceReverbWet * dist2.CalculateValue(distanceReverbSize);
	}

	// Now combine:
	// Our combined dry is with sourceEnv applied, and then distance
	f32 combinedDry = sourceReverbDry*distanceReverbDry;
#if __BANK
	if(g_MaxOutReverb)
	{
		combinedDry = 0.f;
	}
#endif
	// now scale down our source wets by the dry-due-to-distance factor, and ADD our distance wets. Not 100% convinced by this yet.
	f32 combinedReverbWets[g_NumSourceReverbs];
	for(u32 i=0; i<g_NumSourceReverbs; i++)
	{
		combinedReverbWets[i] = Min(1.0f, (newSourceReverbWets[i] * distanceReverbDry) + distanceReverbWets[i]);

#if __BANK
		if(g_MaxOutReverb)
		{
			combinedReverbWets[i] = 1.f;
		}
#endif
	}

	// Now set all these params in the metric
	for(u32 i=0; i<g_NumSourceReverbs; i++)
	{
		metrics->reverbWets[i] = combinedReverbWets[i];
	}
	metrics->reverbDry = combinedDry;


	// Built-Up Reverb:
	// If both source and the listener are inside then don't do any outside/built-up metrics
	if(!audEnvironment::GetIsListenerInInterior() || !metrics->environmentSoundMetrics->environmentGameMetric.GetIsInInterior())
	{
		// Work out the built-up reverb size in each of the directions, based on a slightly different curve, and scaled by distance
		f32 builtUpReverbWet[4];
		for (u32 i=0; i<4; i++)
		{
			// We want loud sounds to use more of the built-up reverb, so scale between the max verb for a regular sound and a loud sound
			builtUpReverbWet[i] = Lerp(howLoud, m_MaxBuiltUpReverbWet, m_MaxBuiltUpReverbWetForLoudSounds);

			// Factor in the damping on the category
			builtUpReverbWet[i] *= sourceReverbDamping;

			// The m_BlockedSpeakerLinearVolumes values are occlusion-related, and are zero if there�s an obstruction very close in the direction of a speaker. 
			// Hence beeping a horn next to a wall won�t give you a distant city reverb from that direction.
			builtUpReverbWet[i] *= m_BlockedSpeakerLinearVolumes[i];
		}

		// We also need to write back our built-up reverb, per speaker reverb settings. 
		// Let's write these directly, and then do the max after usual positioning.
		// First, need to calculate the three levels per reverb, per speaker
		// new distance
		static const audThreePointPiecewiseLinearCurve builtUp0(0.0f, 1.0f, 0.25f, 0.71f, 0.5f, 0.0f);
		static const audThreePointPiecewiseLinearCurve builtUp1sm(0.0f, 0.0f, 0.25f, 0.71f, 0.5f, 1.0f);
		static const audThreePointPiecewiseLinearCurve builtUp1big(0.5f, 1.0f, 0.75f, 0.71f, 1.0f, 0.0f);
		static const audThreePointPiecewiseLinearCurve builtUp2(0.5f, 0.0f, 0.75f, 0.71f, 1.0f, 1.0f);
		for (u32 i=0; i<4; i++)
		{
			const f32 reverbSize = Lerp(m_BuiltUpSpeakerFactors[i], m_ReverbSizeOpen, m_ReverbSizeBuiltUp);

			metrics->relativeWetChannelVolumes[i][0] = builtUpReverbWet[i] * builtUp0.CalculateValue(reverbSize);

			const f32 relWetSmall = builtUpReverbWet[i] * builtUp1sm.CalculateValue(reverbSize);
			const f32 relWetBig = builtUpReverbWet[i] * builtUp1big.CalculateValue(reverbSize);

			metrics->relativeWetChannelVolumes[i][1] = Selectf(reverbSize - 0.5f, relWetBig, relWetSmall);

			metrics->relativeWetChannelVolumes[i][2] = builtUpReverbWet[i] * builtUp2.CalculateValue(reverbSize);
		}
	}
	// REVERB


	// OCCLUSION
	// envGroup:
	f32 occlusionDamping = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].OcclusionDamping.GetFloat32_FromFloat16();

	f32 occlusionAttenuationLin = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionAttenuationLin();
	occlusionAttenuationLin = Lerp(1.0f - occlusionDamping, occlusionAttenuationLin, 1.0f);

	f32 occlusionCutoffFrequencyLin = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionFilterCutoffLin();
	occlusionCutoffFrequencyLin = Lerp(1.0f - occlusionDamping, occlusionCutoffFrequencyLin, 1.0f);
	f32 occlusionCutoffFrequency = audDriverUtil::ComputeHzFrequencyFromLinear(occlusionCutoffFrequencyLin);

	// distance:
	f32 environmentalFilterDamping = g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].EnvironmentalFilterDamping.GetFloat32_FromFloat16();

	f32 distanceFilterCutoffLin = 1.f;
	if(metrics->environmentSoundMetrics->lpfCurveId == 0)
	{
		// Based on best fit of a 1-pole filter to match an atmospheric absorption curve at 35C with 85% humidity
		f32 distanceRatio = RampValueSafe(distanceToVolumeListener, 5.0f, 100.0f, 0.0f, 1.0f);
		distanceFilterCutoffLin = Lerp(1.0f - distanceRatio, 0.78f, 1.0f);	// pitch linear filter value of 0.78 ~= 4000 hZ		
		distanceFilterCutoffLin = Lerp(1.0f - environmentalFilterDamping, distanceFilterCutoffLin, 1.0f);
	}
	else
	{
		// Don't apply environmental filter damping if we have a custom curve, use the value from the curve directly
		distanceFilterCutoffLin = m_LPFCurves[metrics->environmentSoundMetrics->lpfCurveId - 1].CalculateValue(distanceToVolumeListener);		
	}
	
	f32 distanceCutoffFrequency = audDriverUtil::ComputeHzFrequencyFromLinear(distanceFilterCutoffLin);

	// sound:
	f32 requestedCutoffFrequency = (f32)metrics->LPFCutoff;

	// combine:
	// Use the lowest LPF value we have based on occlusion, distance, requested (variable-set), and deafening
	f32 sourceFilterCutoffFrequency = Min(occlusionCutoffFrequency, distanceCutoffFrequency, requestedCutoffFrequency);

	// Set the values in the metric:
	metrics->postSubmixVolumeAttenuation.ScaleLinear(occlusionAttenuationLin);
	metrics->LPFCutoff = (u16)sourceFilterCutoffFrequency;

	u32 distanceHPFCutoffFrequency = 0;
	if(metrics->environmentSoundMetrics->hpfCurveId != 0)
	{
		// Don't apply environmental filter damping if we have a custom curve, use the value from the curve directly
		f32 hpfCutoffLin = m_HPFCurves[metrics->environmentSoundMetrics->hpfCurveId - 1].CalculateValue(distanceToVolumeListener);		
		distanceHPFCutoffFrequency = (u32)audDriverUtil::ComputeHzFrequencyFromLinear(hpfCutoffLin);
	}
	metrics->HPFCutoff = Max(metrics->HPFCutoff, (u16)distanceHPFCutoffFrequency);

	// OCCLUSION
}

f32 audEnvironment::GetDistanceAttenuationWithBuiltUpRolloff(const f32 initialCurveScale, const f32 distance, const f32 builtUpRolloff, const f32 additionalPlateau) const
{
	// This used to treat the built-up rolloff differently, and do a non-linear scaling of distance. We no longer do this,
	// look at v25 in dev_north_gta of this file for the old (quite nice!) algorithm.
	Assert(FPIsFinite(initialCurveScale));
	Assert(FPIsFinite(distance));
	Assert(FPIsFinite(builtUpRolloff));
	Assert(FPIsFinite(additionalPlateau));
	Assert(initialCurveScale != 0.0f);
	Assert(builtUpRolloff != 0.0f);

	const float curveScale = initialCurveScale * builtUpRolloff;

	const f32 plateau = 5.0f + additionalPlateau;

	const f32 oneOverCurveScale = 1.f/curveScale;
	const f32 normalizedDistance = distance;

	// If we have a >1 roll-off, we leave the 5m plateau, and only scale after that. 
	// For <1, we scale back the plateau too.
	// Lets you use long roll-offs without everything close being all mushy.
	// Use fsels for speed, and work out all three options.
	const f32 tightNormalisedDistance = normalizedDistance * oneOverCurveScale - additionalPlateau;
	const f32 wideFarExtraDistance = normalizedDistance - plateau;
	const f32 wideFarNormalisedDistance = 5.0f + (wideFarExtraDistance * oneOverCurveScale);
	const f32 wideCloseNormalisedDistance = normalizedDistance - additionalPlateau;

	const f32 wideNormalisedDistance = Selectf(normalizedDistance-plateau, wideFarNormalisedDistance, wideCloseNormalisedDistance);

	const f32 finalDistance = Selectf(curveScale - 1.0f, wideNormalisedDistance, tightNormalisedDistance);

	// Now actually get the attenuation
	const f32 attenuation = audCurve::DefaultDistanceAttenuation_CalculateValue(finalDistance);

	// catch any assets using the wrong rolloff curve
	//Assert(curve->CalculateValue(finalDistance) == attenuation);

	return attenuation;
}

void audEnvironment::ComputeSpeakerVolumesFromPosition(Vec3V_In relativePosition, audEnvironmentMetricsInternal *metrics, const u32 numChannels)
{
	// Assume 6-channel, rustle up our standard four, and then collapse if we're running with 2.
	ComputeFourChannelSpeakerVolumesFromPosition(relativePosition, metrics);

	// Now modify the volumes by the deafening strength
	for (int i=0; i<(int)g_MaxOutputChannels; i++)
	{
		if ((i==RAGE_SPEAKER_ID_FRONT_LEFT) || (i==RAGE_SPEAKER_ID_BACK_LEFT))
		{
			metrics->relativeChannelVolumes[i].ScaleLinear(m_EarAttenuation[0]);
		}
		else if ((i==RAGE_SPEAKER_ID_FRONT_RIGHT) || (i==RAGE_SPEAKER_ID_BACK_RIGHT))
		{
			metrics->relativeChannelVolumes[i].ScaleLinear(m_EarAttenuation[1]);
		}
		else
		{
			// LFE/centre
			metrics->relativeChannelVolumes[i] = 0.f;
		}
	}

	if (numChannels == 2)
	{
		CollapseFourChannelsToTwo(metrics);
	}	
}

void audEnvironment::ComputeSpeakerVolumesFromPan(audEnvironmentMetricsInternal *metrics, const u32 numChannels)
{
	const Vec3V positionOnUnitCircle = ComputePositionFromPan(metrics->environmentSoundMetrics->pan);

	/*static const audGtaSpeakerPosition g_GtaSpeakerPositions[] =
	{
		{ -0.7071f,  0.7071f, 0.0f }, // FL
		{  0.7071f,  0.7071f, 0.0f }, // FR

		{ -0.7071f, -0.7071f, 0.0f }, // BL
		{  0.7071f, -0.7071f, 0.0f },  // BR
	};*/
	const Vec3V speakerPositions[4] = {
		Vec3V(V4VConstant<0xBF350481, 0x3F350481, 0, 0>()), // FL
		Vec3V(V4VConstant<0x3F350481, 0x3F350481, 0, 0>()), // FR
		Vec3V(V4VConstant<0xBF350481, 0xBF350481, 0, 0>()), // BL
		Vec3V(V4VConstant<0x3F350481, 0xBF350481, 0, 0>()), // BL
	};

	const ScalarV earAtten[2] =
	{
		ScalarV(m_EarAttenuation[0]),
		ScalarV(m_EarAttenuation[1])
	};

	f32 channelVolume[4];
	for (int i = 0; i < 4; i++)
	{
		ScalarV dotProd = Dot(positionOnUnitCircle, speakerPositions[i]);
		// Don't care about ones more than 90degs away
		dotProd = Max(ScalarV(V_ZERO), dotProd);
		
		// if we're not music, do the deafening stuff
		if (m_DeafeningAffectsEverything || (metrics->environmentSoundMetrics->routingMetric.effectRoute != EFFECT_ROUTE_MUSIC &&
			metrics->environmentSoundMetrics->routingMetric.effectRoute != EFFECT_ROUTE_FRONT_END &&
			!IsDirectMonoEffectRoute(metrics->environmentSoundMetrics->routingMetric.effectRoute)))
		{
			dotProd *= earAtten[i & 1];		
		}

		channelVolume[i] = dotProd.Getf();
	}


	//////////////////
	// Headphone leakage - don't leak music

	if (m_UsingHeadphones && metrics->environmentSoundMetrics->routingMetric.effectRoute != EFFECT_ROUTE_MUSIC)
	{
		f32 channelVolumeBackup[4];
		for (int i=0; i<4; i++)
		{
			channelVolumeBackup[i] = channelVolume[i];
		}

		f32 nonLeak = 1.0f - m_HeadphoneLeakage;
		channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_LEFT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_RIGHT];
		channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_RIGHT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_LEFT];
		channelVolume[RAGE_SPEAKER_ID_BACK_LEFT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_LEFT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_RIGHT];
		channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_RIGHT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_LEFT];
	}

	/////////////////////////////
	// Store the result back in our metrics
	metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_LOW_FREQUENCY] = 0.0f;
	metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_CENTER] = 0.0f;
	metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_LEFT] = channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT];
	metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_RIGHT] = channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT];
	metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_LEFT] = channelVolume[RAGE_SPEAKER_ID_BACK_LEFT];
	metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_RIGHT] = channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT];

	if (numChannels == 2)
	{
		CollapseFourChannelsToTwo(metrics);
	}	
}

//****************************************************************************************************************************************
// Private helper fns called by above overriden audio-thread functions - don't themselves override audEnvironment fns
//****************************************************************************************************************************************

void audEnvironment::ComputeFourChannelSpeakerVolumesFromPosition(Vec3V_In relativePosition, audEnvironmentMetricsInternal *metrics)
{
	PF_FUNC(Compute4ChannelVolsFromPos);
	// Multi-step process:
	// 1:
	// Work out where on the flat 2d plane the sound is - that's our primary direction.
	// 2:
	// See if we're above the 2d plane, and scale the non-primary directions up, and the primary one down accordingly.
	// 3:
	// Then see if it's inside our 'plateau' - (for now) the distance we don't do volume roll-off over - so this shrinks for 'tight' sounds but doesn't grow.
	// Outside of that, we don't leak to other channels because of 2d position
	// Inside that, we leave the primary direction as is, and fade up all the other channels linearly to reach the same volume at the centre
	//   - note this will actually make it louder inside the plateau, which may not be what we want - but it's AWFUL when it gets quieter on the correct side.
	// OR - do scale up the plateau, and leak more, but try making the overall volume level, so we don't get crazy loud too wide
	// 4:
	// Take into account reverb wetness (currently not size) - and leak more (scaling down primary) with wetness
	// 5:
	// Occlusion - leak more if we're occluded - the vague hope being it hides that we're not repositioning. Maybe tone this down with distance?
	// We could also up the wetness of occluded things, because a higher component is reflections.
	// 6:
	// Take into account the local environment - leak more in reverberant spaces, probably depending on the actual closeness to walls

	// Now I need to stop writing comments, and actually do that stuff...

	/////////////////////////////
	// 1:
	Vec3V positionOnUnitCircle = relativePosition;
	positionOnUnitCircle.SetZ(ScalarV(V_ZERO));
	positionOnUnitCircle = NormalizeSafe(positionOnUnitCircle, Vec3V(V_ZERO));


	f32 channelVolume[5];
	channelVolume[RAGE_SPEAKER_ID_FRONT_CENTER] = 0.f;

	if(!m_UsingAmbisonics)
	{
		const Vec3V speakerPositions[4] = {
			Vec3V(V4VConstant<0xBF350481, 0x3F350481, 0, 0>()), // FL
			Vec3V(V4VConstant<0x3F350481, 0x3F350481, 0, 0>()), // FR
			Vec3V(V4VConstant<0xBF350481, 0xBF350481, 0, 0>()), // BL
			Vec3V(V4VConstant<0x3F350481, 0xBF350481, 0, 0>()), // BL
		};

		for (int i=0; i<4; i++)
		{
			
			f32 dotProd = Dot(positionOnUnitCircle, speakerPositions[i]).Getf();
			// Don't care about ones more than 90degs away
			dotProd = Max(0.0f, dotProd);
			channelVolume[i] = dotProd;
		}

		// if we're doing side positioning, in a multichannel out, then do a different mapping
		if (/*m_SurroundSpeakerPositioning == AUD_SURROUND_SPEAKERS_SIDE*/ false && m_DownmixOutputMode >= AUD_OUTPUT_5_1)
		{
			
			f32 dotFwds  = Dot(positionOnUnitCircle, Vec3V(V_Y_AXIS_WZERO)).Getf();
			f32 dotRight = Dot(positionOnUnitCircle, Vec3V(V_X_AXIS_WZERO)).Getf();

			// 305-45 ie dotFwds>0.707f
			// as per the regular calc

			if (dotFwds>0.0f && dotFwds<0.707f)
			{
				const f32 frTonedDown = Max(0.0f, (channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT]-0.707f)*3.412f);
				const f32 rrCrankedUp = Min(1.0f, channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT]*1.414f);
				const f32 flTonedDown = Max(0.0f, (channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT]-0.707f)*3.412f);
				const f32 rlCrankedUp = Min(1.0f, channelVolume[RAGE_SPEAKER_ID_BACK_LEFT]*1.414f);

				const f32 fr = channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT];
				const f32 rr = channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT];
				const f32 fl = channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT];
				const f32 rl = channelVolume[RAGE_SPEAKER_ID_BACK_LEFT];

				// FR gets toned down, RR gets cranked up if (dotRight>0)
				channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT] = Selectf(dotRight, frTonedDown, fr);
				channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT]  = Selectf(dotRight, rrCrankedUp, rr);

				// FL gets toned down, RL gets cranked up if (dotRight<0)
				channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT] = Selectf(dotRight, fl, flTonedDown);
				channelVolume[RAGE_SPEAKER_ID_BACK_LEFT] = Selectf(dotRight, rl, rlCrankedUp);
			}
			else if (dotFwds <= 0.707f)
			{
				channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT] = 0.0f;
				channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT] = 0.0f;

				const f32 dotLeft = 0.0f-dotRight;
				const f32 rrWhenInRR = (dotRight*0.293f)+0.707f;
				const f32 rlWhenInRR = (1.0f-dotRight)*0.707f;
				const f32 rlWhenInRL = (dotLeft*0.293f)+0.707f;
				const f32 rrWhenInRL = (1.0f-dotLeft)*0.707f;

				// it's in the rear right if (dotRight>0)
				channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT] = Selectf(dotRight, rrWhenInRR, rrWhenInRL);
				channelVolume[RAGE_SPEAKER_ID_BACK_LEFT] = Selectf(dotRight, rlWhenInRR, rlWhenInRL);	
			}
		}

		/////////////////////////////
		// 2:
		Vec3V positionOnUnitSphere = NormalizeSafe(relativePosition, Vec3V(V_ZERO));
		// Get the angle between the pos on unit sphere and unit circle - and try linearly interping between our primary volume and 0.5 (4 channel equal power)
		f32 elevation = Min(1.f,Dot(positionOnUnitCircle, positionOnUnitSphere).Getf());
		if (elevation < -0.01f)
		{
			audWarningf("elevation: %f", elevation);
			elevation = 0.0f; // it's almost certainly directly above or below, which is elevation=0.0f
			//		Assert(0);
		}

		if (m_ShouldDoPanningStep2)
		{
			for (int i=0; i<4; i++)
			{
				// Linerly interp between 0.5 and primary volume as elevation goes from 0 (90degs) to 1 (flat)
				// 0.5, 0.25? Who can say.
				channelVolume[i] = ((1.0f - elevation) * 0.5f) + (elevation * channelVolume[i]);
			}
		}

		//////////////////////////////
		// 3:
		f32 distanceRelativeToListener;
		if(metrics->environmentSoundMetrics->environmentGameMetric.GetUseOcclusionPath())
		{
			distanceRelativeToListener = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionPathDistance();
		}
		else
		{
			distanceRelativeToListener = Mag(relativePosition).Getf();
		}
		f32 scale = metrics->environmentSoundMetrics->volumeCurveScale;
		scale *= g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].DistanceRollOffScale.GetFloat32_FromFloat16();
		scale *= g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].PlateauRollOffScale.GetFloat32_FromFloat16();
		scale *= m_PlateauScale;
		scale *= m_GlobalRolloffPlateauScale;		

		scale *= metrics->environmentSoundMetrics->environmentGameMetric.GetRolloffFactor();		

		const f32 plateauInner = m_PlateauInner * scale;
		const f32 plateauOuter = m_PlateauOuter * scale;	
		
		// clamp to 1 if scale is 0
		f32 proportionOut = Selectf(scale - SMALL_FLOAT, (distanceRelativeToListener - plateauInner) / (plateauOuter - plateauInner), 1.f);
		proportionOut = Clamp(proportionOut, 0.0f, 1.0f);

		f32 maxVolume = 0.0f;
		for (int i=0; i<4; i++)
		{
			maxVolume = Max(maxVolume, channelVolume[i]);
		}

		if (1) //m_ShouldDoPanningStep3)
		{
			if (m_ShouldAverageVolumes)
			{
				for (int i=0; i<4; i++)
				{
					channelVolume[i] = (0.5f * (1.0f - proportionOut)) + (proportionOut * channelVolume[i]);
				}
			}
			else
			{
				for (int i=0; i<4; i++)
				{
					channelVolume[i] = (maxVolume * (1.0f - proportionOut)) + (proportionOut * channelVolume[i]);
				}
			}
		}

		//////////////////////////////////////
		// 4:
		// Combine our occlusion and distance leakage curves - we want to leak loads for up-close occluded things, to smudge corners, 
		// but when we're miles away, we want it to not make any difference.

		// Work out occlusion leakage first:
		f32 leakage = 0.0f;
		
		leakage = metrics->environmentSoundMetrics->environmentGameMetric.GetLeakageFactor();
		AssertMsg(leakage<1.1f, "metrics->environmentGameMetric.GetLeakageFactor()>1.1f");
		
		leakage = Max(leakage, m_GlobalPositionedLeakage);
		leakage = Min(leakage, 1.0f);
		// Now see how much to tone that down by distance
	//	static const audThreePointPiecewiseLinearCurve leakageCurve(10.0f, 1.0f, 28.0f, 0.0f, 51.0f, 0.0f);
	//	f32 distanceOcclusionLeakageDamping = leakageCurve.CalculateValue(distanceRelativeToListener);
	//	f32 distanceOcclusionLeakageDamping = m_DistanceOcclusionLeakageDamping.CalculateValue(distanceRelativeToListener);

		if (m_ShouldDoPanningStep4)
		{
			for (int i=0; i<4; i++)
			{
				channelVolume[i] = (0.5f * leakage) + ((1.0f - leakage) * channelVolume[i]);
			}
		}

		//////////////////
		// Headphone leakage

		if (m_UsingHeadphones)
		{
			f32 channelVolumeBackup[4];
			for (int i=0; i<4; i++)
			{
				channelVolumeBackup[i] = channelVolume[i];
			}

			f32 nonLeak = 1.0f - m_HeadphoneLeakage;
			channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_LEFT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_RIGHT];
			channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_RIGHT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_FRONT_LEFT];
			channelVolume[RAGE_SPEAKER_ID_BACK_LEFT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_LEFT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_RIGHT];
			channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT] = nonLeak * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_RIGHT] + m_HeadphoneLeakage * channelVolumeBackup[RAGE_SPEAKER_ID_BACK_LEFT];
		}

		/////////////////////////////
		// Store the result back in our metrics

		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_LOW_FREQUENCY] = 0.0f;
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_CENTER] = channelVolume[RAGE_SPEAKER_ID_FRONT_CENTER]; //0.f;
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_LEFT] = channelVolume[RAGE_SPEAKER_ID_FRONT_LEFT];
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_RIGHT] = channelVolume[RAGE_SPEAKER_ID_FRONT_RIGHT];
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_LEFT] = channelVolume[RAGE_SPEAKER_ID_BACK_LEFT];
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_RIGHT] = channelVolume[RAGE_SPEAKER_ID_BACK_RIGHT];
		
	}
	else //m_UsingAmbisonics
	{

		Vec3V positionOnUnitSphere = NormalizeSafe(relativePosition, Vec3V(V_ZERO));
		//If we have the need for speed here it might be ok to replace the sq/sqr with an optimized fabs
		m_DecodeData.elevationComponent = (Sqrtf(1.f - Min(1.f, positionOnUnitSphere.GetZf()*positionOnUnitSphere.GetZf())));		


		f32 distanceRelativeToListener;
		if(metrics->environmentSoundMetrics->environmentGameMetric.GetUseOcclusionPath())
		{
			distanceRelativeToListener = metrics->environmentSoundMetrics->environmentGameMetric.GetOcclusionPathDistance();
		}
		else
		{
			distanceRelativeToListener = Mag(relativePosition).Getf();
		}

		f32 scale = metrics->environmentSoundMetrics->volumeCurveScale;
		scale *= g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].DistanceRollOffScale.GetFloat32_FromFloat16();
		scale *= g_ResolvedCategorySettings[metrics->environmentSoundMetrics->categoryIndex].PlateauRollOffScale.GetFloat32_FromFloat16();
		scale *= m_PlateauScale;
		scale *= m_GlobalRolloffPlateauScale;
		
		scale *= metrics->environmentSoundMetrics->environmentGameMetric.GetRolloffFactor();
	
		const f32 plateauInner = m_PlateauInner * scale;
		const f32 plateauOuter = m_PlateauOuter * scale;

		// clamp to 1 if scale is 0
		m_DecodeData.proportionOut = Selectf(scale - SMALL_FLOAT, (distanceRelativeToListener - plateauInner) / (plateauOuter - plateauInner), 1.f);
		m_DecodeData.proportionOut = Clamp(m_DecodeData.proportionOut, 0.0f, 1.0f);
		
		// Work out occlusion leakage first:
		f32 leakage = 0.0f;
	

		leakage = metrics->environmentSoundMetrics->environmentGameMetric.GetLeakageFactor();
		AssertMsg(leakage<1.1f, "metrics->environmentGameMetric.GetLeakageFactor()>1.1f");

		leakage = Max(leakage, m_GlobalPositionedLeakage);
		leakage = Min(leakage, 1.0f);


		m_DecodeData.occlusionLeak = 1 - leakage;

		audAmbisonics::CalculateBFormatEncode(m_DecodeData, positionOnUnitCircle.GetYf(), -positionOnUnitCircle.GetXf());
		audAmbisonics::CalculateSpeakerGains(m_DecodeData);

		/////////////////////////////
		// Store the result back in our metrics

		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_LOW_FREQUENCY] = 0.0f;
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_CENTER] = m_DecodeData.speakerGains[AMBISONICS_SPEAKER_CENTER]; //0.f;
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_LEFT] = m_DecodeData.speakerGains[AMBISONICS_SPEAKER_L_FRONT];
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_RIGHT] = m_DecodeData.speakerGains[AMBISONICS_SPEAKER_R_FRONT];
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_LEFT] = m_DecodeData.speakerGains[AMBISONICS_SPEAKER_L_BACK];
		metrics->relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_RIGHT] = m_DecodeData.speakerGains[AMBISONICS_SPEAKER_R_BACK];

	}

}

void audEnvironment::CollapseFourChannelsToTwo(audEnvironmentMetricsInternal *metrics)
{
	PF_FUNC(CollapseFourChannelsToTwo);
	atRangeArray<f32,6> relativeChannelVolumes;
	for(u32 i = 0; i < 6; i++)
	{
		relativeChannelVolumes[i] = metrics->relativeChannelVolumes[i].GetLinear();
	}

	

	// For left pair and right pair, sum the squares, then sqrt and half (we half to guarantee we're in 0-1 range still)
	f32 leftVolSq  = ((relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_LEFT])  * (relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_LEFT])) +
				 	 ((relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_LEFT])   * (relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_LEFT]));
	f32 rightVolSq = ((relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_RIGHT]) * (relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_RIGHT])) +
					 ((relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_RIGHT])  * (relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_RIGHT]));

	relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_LEFT]  = 0.5f * sqrt(leftVolSq);
	relativeChannelVolumes[RAGE_SPEAKER_ID_FRONT_RIGHT] = 0.5f * sqrt(rightVolSq);
	relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_LEFT]  = 0.0f;
	relativeChannelVolumes[RAGE_SPEAKER_ID_BACK_RIGHT] = 0.0f;

	for(u32 i = 0; i < 6; i++)
	{
		metrics->relativeChannelVolumes[i].SetLinear(relativeChannelVolumes[i]);
	}

}

void audEnvironmentGameMetric::SetMixGroup(s32 mixGroupIndex) 
{ 
		Assign(m_MixGroupIndex, mixGroupIndex); 
}
void audEnvironmentGameMetric::SetPrevMixGroup(s32 mixGroupIndex) 
{ 
	Assign(m_PrevMixGroupIndex, mixGroupIndex); 
}
void audEnvironmentGameMetric::SetRolloffFactor(const f32 rolloff)
{
	Assert(rolloff != 0);
	m_RolloffFactor = rolloff; 
}

void audEnvironment::UpdateEffectSettings()
{
	// TODO: vectorize
	const float deltaS = g_AudioEngine.GetTimeDeltaInSeconds();
	const float lerpRate = 0.35f;
	
	const audSpecialEffectMode activeMode = GetSpecialEffectMode();
	Approach(m_ActiveEffectSettings.LargeFeedback, g_EffectSettings[activeMode].LargeFeedback, lerpRate, deltaS);
	Approach(m_ActiveEffectSettings.LargeRoomSize, g_EffectSettings[activeMode].LargeRoomSize, lerpRate, deltaS);
	Approach(m_ActiveEffectSettings.LargeDamping, g_EffectSettings[activeMode].LargeDamping, lerpRate, deltaS);
	
	Approach(m_ActiveEffectSettings.LargeReverbHPF, g_EffectSettings[activeMode].LargeReverbHPF, lerpRate * kVoiceFilterHPFMaxCutoff, deltaS);	
	Approach(m_ActiveEffectSettings.LargeReverbLPF, g_EffectSettings[activeMode].LargeReverbLPF, lerpRate * kVoiceFilterLPFMaxCutoff, deltaS);

	Approach(m_ActiveEffectSettings.MediumRoomSize, g_EffectSettings[activeMode].MediumRoomSize, lerpRate, deltaS);
	Approach(m_ActiveEffectSettings.MediumDamping, g_EffectSettings[activeMode].MediumDamping, lerpRate, deltaS);	
	Approach(m_ActiveEffectSettings.MediumFeedback, g_EffectSettings[activeMode].MediumFeedback, lerpRate, deltaS);

	m_ActiveEffectSettings.LargeBypassLPF = g_EffectSettings[activeMode].LargeBypassLPF;
	m_ActiveEffectSettings.LargeBypassHPF = g_EffectSettings[activeMode].LargeBypassHPF;
	m_ActiveEffectSettings.LargeBypassDelay = g_EffectSettings[activeMode].LargeBypassDelay;
	m_ActiveEffectSettings.LargeBypassReverb = g_EffectSettings[activeMode].LargeBypassReverb;
	m_ActiveEffectSettings.LargePredelay = g_EffectSettings[activeMode].LargePredelay;
	m_ActiveEffectSettings.MediumPredelay = g_EffectSettings[activeMode].MediumPredelay;
	m_ActiveEffectSettings.MediumBypassReverb = g_EffectSettings[activeMode].MediumBypassReverb;
	m_ActiveEffectSettings.MediumBypassDelay = g_EffectSettings[activeMode].MediumBypassDelay;
	m_ActiveEffectSettings.MediumBypassHPF = g_EffectSettings[activeMode].MediumBypassHPF;
	
	m_ActiveEffectSettings.SmallBypass = g_EffectSettings[activeMode].SmallBypass;
	m_ActiveEffectSettings.SmallReverbSettings = g_EffectSettings[activeMode].SmallReverbSettings;

	Approach(m_ActiveEffectSettings.MinSmallSend, g_EffectSettings[activeMode].MinSmallSend, lerpRate * 0.65f, deltaS);
	Approach(m_ActiveEffectSettings.MinMediumSend, g_EffectSettings[activeMode].MinMediumSend, lerpRate * 0.65f, deltaS);
	Approach(m_ActiveEffectSettings.MinLargeSend, g_EffectSettings[activeMode].MinLargeSend, lerpRate * 0.65f, deltaS);

	Approach(m_ActiveEffectSettings.UnderwaterEffectLevel, g_EffectSettings[activeMode].UnderwaterEffectLevel, lerpRate * 2.f, deltaS);
}

void audEnvironment::GameUpdate(const u32 UNUSED_PARAM(timeInMs))
{	
	UpdateEffectSettings();

	const audGameEffectSettings &settings = m_ActiveEffectSettings;

	audMixerSubmix *smallSubmix = GetSourceReverbSubmix(0);
	audMixerSubmix *smallMusicSubmix = GetMusicReverbSubmix(0);
	audMixerSubmix *medium = GetSourceReverbSubmix(1);
	audMixerSubmix *mediumMusic = GetMusicReverbSubmix(1);
	audMixerSubmix *large = GetSourceReverbSubmix(2);
	audMixerSubmix *largeMusic = GetMusicReverbSubmix(2);

	smallSubmix->SetEffectParam(kSmallReverb_Reverb, audEarlyReflectionEffect::Bypass, settings.SmallBypass);
	smallSubmix->SetEffectParam(kSmallReverb_Reverb, audEarlyReflectionEffect::Settings, settings.SmallReverbSettings);
	smallMusicSubmix->SetEffectParam(kSmallReverb_Reverb, audEarlyReflectionEffect::Bypass, settings.SmallBypass);
	smallMusicSubmix->SetEffectParam(kSmallReverb_Reverb, audEarlyReflectionEffect::Settings, settings.SmallReverbSettings);

	smallSubmix->SetEffectParam(kSmallReverb_UnderwaterEffect, audUnderwaterEffect::Gain, settings.UnderwaterEffectLevel);

	medium->SetEffectParam(kMediumReverb_Delay, audDelayEffect4::Feedback, settings.MediumFeedback);
	medium->SetEffectParam(kMediumReverb_Delay, audDelayEffect4::DelayTime, settings.MediumPredelay);

	medium->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::RoomSize, settings.MediumRoomSize);
	medium->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::Damping, settings.MediumDamping);

	mediumMusic->SetEffectParam(kMediumReverb_Delay, audDelayEffect4::Feedback, settings.MediumFeedback);
	mediumMusic->SetEffectParam(kMediumReverb_Delay, audDelayEffect4::DelayTime, settings.MediumPredelay);

	mediumMusic->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::RoomSize, settings.MediumRoomSize);
	mediumMusic->SetEffectParam(kMediumReverb_Reverb, audReverbEffect::Damping, settings.MediumDamping);

	large->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Mode, BIQUAD_MODE_HIGH_PASS_4POLE);
	large->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Bypass, settings.LargeBypassHPF);
	large->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::CutoffFrequency, settings.LargeReverbHPF);

	large->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::Bypass, settings.LargeBypassLPF);
	large->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::CutoffFrequency, settings.LargeReverbLPF);

	large->SetEffectParam(kLargeReverb_Delay, audDelayEffect4::DelayTime, settings.LargePredelay);
	large->SetEffectParam(kLargeReverb_Delay, audDelayEffect4::Feedback, settings.LargeFeedback);
	large->SetEffectParam(kLargeReverb_Delay, audDelayEffect4::Bypass, settings.LargeBypassDelay);

	large->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::RoomSize, settings.LargeRoomSize);
	large->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Damping, settings.LargeDamping);
	large->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Bypass, settings.LargeBypassReverb);

	largeMusic->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Mode, BIQUAD_MODE_HIGH_PASS_4POLE);
	largeMusic->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::Bypass, settings.LargeBypassHPF);
	largeMusic->SetEffectParam(kLargeReverb_HPF, audBiquadFilterEffect::CutoffFrequency, settings.LargeReverbHPF);

	largeMusic->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::Bypass, settings.LargeBypassLPF);
	largeMusic->SetEffectParam(kLargeReverb_LPF, audBiquadFilterEffect::CutoffFrequency, settings.LargeReverbLPF);

	largeMusic->SetEffectParam(kLargeReverb_Delay, audDelayEffect4::DelayTime, settings.LargePredelay);
	largeMusic->SetEffectParam(kLargeReverb_Delay, audDelayEffect4::Feedback, settings.LargeFeedback);
	largeMusic->SetEffectParam(kLargeReverb_Delay, audDelayEffect4::Bypass, settings.LargeBypassDelay);

	largeMusic->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::RoomSize, settings.LargeRoomSize);
	largeMusic->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Damping, settings.LargeDamping);
	largeMusic->SetEffectParam(kLargeReverb_Reverb, audReverbEffect::Bypass, settings.LargeBypassReverb);

	audMixerSubmix *sfx = GetSfxSubmix();
	sfx->SetEffectParam(0, audBiquadFilterEffect::Mode, BIQUAD_MODE_HIGH_PASS_4POLE);
	sfx->SetEffectParam(0, audBiquadFilterEffect::CutoffFrequency, g_SFXHPFCutoff);
	sfx->SetEffectParam(0, audBiquadFilterEffect::ResonanceLevel, g_SFXHPFReso);
	sfx->SetEffectParam(0, audBiquadFilterEffect::Bypass, g_SFXHPFBypass);
}

void audEnvironment::SetSpecialEffectMode(const audSpecialEffectMode mode)
{
	m_SpecialEffectMode = mode;
}

SPU_ONLY(extern audEnvironment *g_audEnvironmentEA);

void audEnvironment::SetSubmixVirtualisationScore(const s32 submixId, const u32 virtualisationScore)
{
	m_SubmixVirtualisationScores[submixId] = virtualisationScore;
#if __SPU
	sysDmaPutUInt32(virtualisationScore, (uint64_t)&(g_audEnvironmentEA->m_SubmixVirtualisationScores[submixId]), 5);
#endif
}

}//namespace rage
