   
#include "dynamicmixmgr.h"
#include "categorymanager.h"
#include "engine.h"
#include "system/param.h"
#include "audiohardware/channel.h"
#include "audiohardware/config.h"
#include "audiohardware/debug.h"
#include "bank/bkmgr.h"
#include "system/interlocked.h"

RAGE_DEFINE_SUBCHANNEL(Audio,DynamicMixer)


namespace rage
{


#if !__SPU

audMixGroup * g_MixGroupPoolMem = NULL;
u32 * g_MixGroupReferenceCounts = NULL;
u32 * g_MixGroupInstanceIds = NULL;


#if DEBUG_MIXGROUPS
u32 * g_MixGroupTypedRefCounts[audMixGroup::NUM_REF_TYPES] = {NULL};

const char* audMixGroup::k_RefTypeNames[NUM_REF_TYPES] = 
{
	"SCENE",
	"SOUND",
	"ENTITY",
	"PATCH"
};
#endif


XPARAM(noaudio);
XPARAM(noaudiothread);
XPARAM(audiowidgets);

atPool<audMixGroup> *g_MixGroupPool = NULL;

//Temporary code for setting up the mixgroup maps
static  atArray< atArray<const audCategory *> > s_MixGroupMaps;
static u32 * s_MixGroupMapHashes = NULL;
static u32 s_NumMixGroupMapCategories = 0;
static u32 s_NumMixGroups = 0;
static atArray<u32> s_MixGroupLoadedChunks;

#if __BANK
bool g_PrintMixgroupCatMaps = false;
bool g_PrintMixgroupHeirarchies = false;
bool g_PrintMixGroupPool = false;
#endif
//////////////////////////////////////////////////////////////////////////////////
//audDynamicMixMgr
//////////////////////////////////////////////////////////////////////////////////

audDynamicMixMgr::audDynamicMixMgr()
	 //:m_NumMixGroups(0)
{
}


audDynamicMixMgr::~audDynamicMixMgr()
{

}

void audDynamicMixMgr::Shutdown()
{
	if(g_MixGroupPool)
	{
		delete g_MixGroupPool;
		g_MixGroupPool = NULL;
	}
	
	if(g_MixGroupPoolMem)
	{
		delete [] g_MixGroupPoolMem;
		g_MixGroupPoolMem = NULL;
	}

	m_MetadataMgr.Shutdown();

	if(s_MixGroupMapHashes)
	{
		delete [] s_MixGroupMapHashes;
		s_MixGroupMapHashes = NULL;
	}

	if(g_MixGroupInstanceIds)
	{
		delete [] g_MixGroupInstanceIds;
		g_MixGroupInstanceIds = NULL;
	}

	s_MixGroupMaps.Reset();

	if(g_MixGroupReferenceCounts)
	{
		delete [] g_MixGroupReferenceCounts;
		g_MixGroupReferenceCounts = NULL;
	} 

#if DEBUG_MIXGROUPS
	for(int i=0; i < audMixGroup::NUM_REF_TYPES; i++)
	{
		if(g_MixGroupTypedRefCounts[i])
		{
			delete [] g_MixGroupTypedRefCounts[i];
			g_MixGroupTypedRefCounts[i] = NULL;
		}
	}
#endif
}

void audDynamicMixMgr::SetupMixGroupMap(atArray<const audCategory*> & map, audCategory * cat, u32 & mapIndex, MixGroupCategoryMap * hierarchy, u32 nodeHash)
{
	audAssert(cat);
	audAssertf(mapIndex< s_NumMixGroupMapCategories, "There are %d categories and only %d mix map categories; increase s_NumMixGroupMapCategories", mapIndex+1, s_NumMixGroupMapCategories);

	const Category * catSettings = cat->GetMetadata();

	bool found = false;
	
	for(int i=0; i< hierarchy->numEntrys; i++)
	{
		if(g_AudioEngine.GetCategoryManager().GetMetadataManager().GetObject<Category>(hierarchy->Entry[i].CategoryHash) == catSettings)
		{
			map[mapIndex] =  cat;
			nodeHash = hierarchy->Entry[i].CategoryHash;
		}
	}
	if(!found)
	{
		if(nodeHash != NULL_HASH) //if we have already found a dynamic hierarchy root/node map this category to that
		{
			map[mapIndex] = g_AudioEngine.GetCategoryManager().GetCategoryPtr(nodeHash);
		}
		else //this category is outside the dynamic hierarchy so map it to itself
		{	
			map[mapIndex] = cat;
		}
	}
	mapIndex++;

	for(u32 i=0; i< cat->GetNumChildren(); i++)
	{
		SetupMixGroupMap(map, cat->GetChildByIndex(i), mapIndex, hierarchy, nodeHash);	
	}
}

bool audDynamicMixMgr::Init()
{
	Assert(!g_MixGroupPool); 

	const char *metadataFileName = audConfig::GetValue("metadataPaths_DynamicMixer", "audio:/config/dynamix.dat");
	
	if (!m_MetadataMgr.Init("DynamicMixer", metadataFileName, audMetadataManager::External_NameTable_BankOnly, DYNAMIXDEFS_SCHEMA_VERSION, true))
	{
		return false;
	}
	s_NumMixGroupMapCategories = g_AudioEngine.GetCategoryManager().GetNumberOfStaticCategories();

	g_MixGroupPoolMem = rage_aligned_new(16) audMixGroup[NUM_AUD_MIXGROUPS];
	Assert(g_MixGroupPoolMem);

	g_MixGroupPool = rage_new atPool<audMixGroup>((u8*)g_MixGroupPoolMem, NUM_AUD_MIXGROUPS*sizeof(audMixGroup));
	Assert(g_MixGroupPool);
	
	InitMixgroupsForChunk(0);
		
	return true;
}

void audDynamicMixMgr::StartUnloadingChunk(const char *chunkName)
{
	audMetadataManager &metadataMgr = GetMetadataManager();
	s32 chunkId = metadataMgr.FindChunkId(chunkName);
	if(chunkId != -1)
	{
		metadataMgr.SetChunkEnabled(chunkId, false);		
	}
}

void audDynamicMixMgr::CancelUnloadingChunk(const char *chunkName)
{
	audMetadataManager &metadataMgr = GetMetadataManager();
	s32 chunkId = metadataMgr.FindChunkId(chunkName);
	if(audVerifyf(chunkId != -1, "Failed to find loaded chunk %s", chunkName))
	{
		metadataMgr.SetChunkEnabled(chunkId, true);
	}
}

void audDynamicMixMgr::InitMixgroupsForChunk(int chunkId)
{
	audMetadataObjectInfo mixGroupListInfo;

	//Check that we've not already loaded in this chunk.
	if(chunkId < 0 || s_MixGroupLoadedChunks.Find(chunkId) >= 0)
	{
		return;
	}

	if(m_MetadataMgr.GetObjectInfoFromSpecificChunk(ATSTRINGHASH("MixGroupList", 0x1AC6A07B), chunkId, mixGroupListInfo))
	{
		const MixGroupList * mixGroupList =  mixGroupListInfo.GetObject<MixGroupList>();
		
		dmDisplayf("[NorthAudio] Num mix groups is %d in chunk %d", mixGroupList->numMixGroups, chunkId);

		ASSERT_ONLY(static u32 mixgroupArraySize = 0;)
		static const u32 maxDlcMixGroups = 64;
		
		//This is always the main game chunk so we allocate our arrays and add a little space to accommodate 
		//mixgroups in future chunks e.g. online
		if(chunkId == 0)
		{
			s_MixGroupMaps.Resize(mixGroupList->numMixGroups + maxDlcMixGroups);
			for(u32 i=0; i < (mixGroupList->numMixGroups + maxDlcMixGroups); i++)
			{
				s_MixGroupMaps[i].Resize(s_NumMixGroupMapCategories);
			}


			s_MixGroupMapHashes = rage_new u32[mixGroupList->numMixGroups + maxDlcMixGroups];
			g_MixGroupInstanceIds = rage_aligned_new(16) u32[mixGroupList->numMixGroups + maxDlcMixGroups];
			g_MixGroupReferenceCounts = rage_aligned_new(16) u32[mixGroupList->numMixGroups + maxDlcMixGroups];
			sysMemSet(g_MixGroupReferenceCounts, 0, sizeof(u32)*(mixGroupList->numMixGroups + maxDlcMixGroups));
			sysMemSet(g_MixGroupInstanceIds, ~0U, sizeof(u32)*(mixGroupList->numMixGroups + maxDlcMixGroups));
			ASSERT_ONLY(mixgroupArraySize = mixGroupList->numMixGroups + maxDlcMixGroups;)

#if DEBUG_MIXGROUPS 
			for(int i=0; i < audMixGroup::NUM_REF_TYPES; i++)
			{
				g_MixGroupTypedRefCounts[i] = rage_aligned_new(16) u32[mixGroupList->numMixGroups + 16];
				sysMemSet(g_MixGroupTypedRefCounts[i], 0, sizeof(u32)*(mixGroupList->numMixGroups + 16));
			}
#endif
		}

		audAssertf(s_NumMixGroupMapCategories >= g_AudioEngine.GetCategoryManager().GetNumberOfStaticCategories(), "Increase s_NumMixGroupMapCategorys to at least %u", g_AudioEngine.GetCategoryManager().GetNumberOfStaticCategories());

		//Get the 'BASE' category
		audCategory * baseCat = g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(0);

		audAssertf(s_NumMixGroups+mixGroupList->numMixGroups <= mixgroupArraySize, "Adding more mixgroups (s_NumMixGroups %d, mixGroupList->numMixGroups %d) in chunk %d than we have space to handle in the mixgroup arrays (%d)", s_NumMixGroups, mixGroupList->numMixGroups, chunkId, mixgroupArraySize);
		
		for(int i=0; i < (mixGroupList->numMixGroups); i++)
		{
			s_MixGroupMapHashes[i + s_NumMixGroups] = mixGroupList->MixGroup[i].MixGroupHash;  
			MixGroup * mixGroup = m_MetadataMgr.GetObject<MixGroup>(s_MixGroupMapHashes[i+s_NumMixGroups]);

			u32 mapIndex = 0; 

			MixGroupCategoryMap * hierarchy = GetMetadataManager().GetObject<MixGroupCategoryMap>(mixGroup->Map);

			if(hierarchy)
			{
				SetupMixGroupMap(s_MixGroupMaps[i+s_NumMixGroups], baseCat, mapIndex, hierarchy, NULL_HASH);
			}
		}

		s_NumMixGroups += mixGroupList->numMixGroups;
		s_MixGroupLoadedChunks.PushAndGrow(chunkId);
	}
}

audMixGroup* audDynamicMixMgr::GetMixGroup(u32 mixGroupHash)
{
	for(int i=0; i < NUM_AUD_MIXGROUPS; i++)
	{
		if(g_MixGroupPoolMem[i].m_MixGroupIndex != -1 && g_MixGroupPoolMem[i].m_MixGroupHash == mixGroupHash)
		{
			return &g_MixGroupPoolMem[i];
		}
	}
	
	return NULL;
}

s32 audDynamicMixMgr::GetMixGroupIndex(u32 mixGroupHash)
{
	for(s16 i=0; i < NUM_AUD_MIXGROUPS ; i++)
	{
		if(g_MixGroupPoolMem[i].m_MixGroupIndex != -1 && g_MixGroupPoolMem[i].m_MixGroupHash == mixGroupHash)
		{
			return g_MixGroupPoolMem[i].m_MixGroupIndex;
		}
	}

	return -1;
}

u32 audDynamicMixMgr::GetMixGroupHash(s32 index) const
{
	if(index != -1)
	{
		for(s16 i=0; i < NUM_AUD_MIXGROUPS ; i++)
		{
			if(g_MixGroupPoolMem[i].m_MixGroupIndex == index)
			{
				return g_MixGroupPoolMem[i].m_MixGroupHash;
			}
		}
	}
	return 0;
}



audMixGroup * audDynamicMixMgr::CreateMixGroup(u32 mixGroupHash)
{
	//First see if we already have this mix group
	audMixGroup * mixGroup = GetMixGroup(mixGroupHash); 
	if(mixGroup)
	{
		return mixGroup;
	}

#if !__FINAL
	if(g_MixGroupPool->GetFreeCount() < 6)
	{
		static bool s_HavePrintedMixGroupPool = false; 
		static bool s_PoolIsFull = false;
		bool poolIsFull = g_MixGroupPool->IsFull();

		if((!s_HavePrintedMixGroupPool || poolIsFull) && !s_PoolIsFull) 
		{ 
			s_HavePrintedMixGroupPool = true; 
			dmErrorf("----------");
#if __BANK
			dmErrorf("Creating mix group %s (%u). Pool count %" SIZETFMT "d  Existing mixgroups:", GetMetadataManager().GetObjectName(mixGroupHash), mixGroupHash, g_MixGroupPool->GetCount());
#else
			dmErrorf("Creating mix group %u. Pool count %" SIZETFMT "d  Existing mixgroups:", mixGroupHash, g_MixGroupPool->GetCount());
#endif

			for(int i =0; i < NUM_AUD_MIXGROUPS; i++)
			{
				if(g_MixGroupPoolMem[i].m_MixGroupIndex >= 0)
				{
#if __BANK
					dmErrorf("%s (%u)", GetMetadataManager().GetObjectName(g_MixGroupPoolMem[i].m_MixGroupHash), g_MixGroupPoolMem[i].m_MixGroupHash);
#else
					dmErrorf("%u", g_MixGroupPoolMem[i].m_MixGroupHash);
#endif
				}
			}
		}
		s_PoolIsFull = poolIsFull;
	}
#endif
	
	audAssertf(g_MixGroupPool, "Creating a mix group but the mix group mgr has not been initialized");
	
	if(!g_MixGroupPool->IsFull())
	{
		mixGroup = static_cast<audMixGroup *>(g_MixGroupPool->New());
	}
	else
	{
		audAssertf(0, "Mixgroup pool is full!");
	}

	Assert(mixGroup);
	if(!mixGroup)
	{
		return NULL;
	}
	
	MixGroup * settings = m_MetadataMgr.GetObject<MixGroup>(mixGroupHash);

	audAssertf(settings, "Couldn't find mix group settings for hash %d", mixGroupHash);

	if(!settings)
	{
		g_MixGroupPool->Delete(mixGroup);
		return NULL;
	}

	mixGroup->m_MixGroupHash = mixGroupHash;

	mixGroup->m_FadeTime = settings->FadeTime;


	if(mixGroup->Init(settings))
	{
		BANK_ONLY(dmDisplayf("Created mix group %s (%u), refs: %u", GetMetadataManager().GetObjectName(mixGroupHash), mixGroupHash, g_MixGroupReferenceCounts[mixGroup->m_MixGroupIndex]);) 
		return mixGroup;
	}


	g_MixGroupPool->Delete(mixGroup);
	return NULL;
}

void audDynamicMixMgr::GetMixGroupReference(u32 mixGroupHash, audMixGroup::MixGroupRefType type)
{
	//First see if we already have this mix group
	audMixGroup * mixGroup = GetMixGroup(mixGroupHash); 
	if(mixGroup)
	{
		mixGroup->AddReference(type);
		return;
	}

	mixGroup = CreateMixGroup(mixGroupHash);

	if(mixGroup)
	{
		mixGroup->AddReference(type);
	}

	return;
}

#if __BANK
void PrintCategoryInfo(audCategory *cat)
{
	if(cat)
	{
		audCategory * parent = cat->GetParent();
		dmErrorf("  %s: %d, parent: %s, parent index: %i", cat->GetNameString(), g_AudioEngine.GetCategoryManager().GetCategoryIndex(cat), parent ? parent->GetNameString() : "no parent", parent ? g_AudioEngine.GetCategoryManager().GetCategoryIndex(parent) : -1 );
		for(u32 i=0; i< cat->GetNumChildren(); i++)
		{
			PrintCategoryInfo(cat->GetChildByIndex(i));
		}
	}
}
#endif

void audDynamicMixMgr::ReleaseMixGroupReference(u32 mixGroupHash, audMixGroup::MixGroupRefType type)
{
	audMixGroup * mixGroup = GetMixGroup(mixGroupHash);
	
	//If there are multiple references decrement the count, otherwise desroy the mix group
	Assert(mixGroup->GetReferenceCount());
	mixGroup->RemoveReference(type);
}

bool g_PrintMixgroupHeirarchyCats = false;

void audDynamicMixMgr::PostUpdateProcess()
{
	//Garbage collect the mix groups to see if any have become zero referenced
	//during the update
	for(int i = 0; i < NUM_AUD_MIXGROUPS; i++)
	{
		audMixGroup * mixGroup = &g_MixGroupPoolMem[i];
		if(mixGroup->m_MixGroupIndex != -1)
		{
			if(g_MixGroupReferenceCounts[mixGroup->m_MixGroupIndex] ==0)
			{
				BANK_ONLY(dmDisplayf("Destroying mix group %s (%u)", GetMetadataManager().GetObjectName(mixGroup->GetObjectHash()), mixGroup->GetObjectHash());)
					mixGroup->Shutdown();
				g_MixGroupPool->Delete(mixGroup);
				// in non final builds, the memory gets trashed...but we need to use
				// the pool memory independently, so we'll just quietly poke it back the way we want it...
	#if !__FINAL
				mixGroup->m_MixGroupIndex = -1;
	#endif
			}
		}
	}

	PrintDebug();
}

void audDynamicMixMgr::ActivateDebugPrinting()
{
#if __BANK
	g_PrintMixgroupCatMaps = g_PrintMixgroupHeirarchies = g_PrintMixGroupPool = g_PrintMixgroupHeirarchyCats = true;
#endif
}

void audDynamicMixMgr::PrintDebug()
{
#if __BANK
	if(g_PrintMixGroupPool)
	{
		g_PrintMixGroupPool = false;
		dmErrorf("----------");
		for(int i =0; i < NUM_AUD_MIXGROUPS; i++)
		{
			if(g_MixGroupPoolMem[i].m_MixGroupIndex >= 0)
			{
#if DEBUG_MIXGROUPS
				audMixGroup * mixGroup = &g_MixGroupPoolMem[i];

				char refCountString[256] = {0};
				audMixGroup::FormatRefCountStr(refCountString, 256, mixGroup->m_MixGroupIndex);

				dmErrorf("%s (%u, %d), %s", GetMetadataManager().GetObjectName(mixGroup->m_MixGroupHash), mixGroup->m_MixGroupHash, mixGroup->m_MixGroupIndex, refCountString);
#endif
			}
		}
	}

	if(g_PrintMixgroupCatMaps)
	{
		dmErrorf("----------");
		for(int i = 0; i < NUM_AUD_MIXGROUPS; i++)
		{
			audMixGroup * mixGroup = &g_MixGroupPoolMem[i];
			if(mixGroup->m_MixGroupIndex != -1)
			{
				dmErrorf("Mixgroup %s category map, Index %d, Cat root index %d", GetMetadataManager().GetObjectName(mixGroup->GetObjectHash()), mixGroup->GetIndex(), mixGroup->GetCategoryIndex());
				for(u32 j=0; j < s_NumMixGroupMapCategories; j++)
				{
					dmErrorf("	%d %s: %d", j, g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(j)->GetNameString(), mixGroup->GetMap()[j]);
				}
			}
		}
	}

	if(g_PrintMixgroupHeirarchies)
	{
		dmErrorf("----------");
		for(int i = 0; i < NUM_AUD_MIXGROUPS; i++)
		{
			audMixGroup * mixGroup = &g_MixGroupPoolMem[i];
			if(mixGroup->m_MixGroupIndex != -1)
			{
				dmErrorf("Mixgroup %s heirarchy, Index %d, Cat root index %d", GetMetadataManager().GetObjectName(mixGroup->GetObjectHash()), mixGroup->GetIndex(), mixGroup->GetCategoryIndex());

				MixGroupCategoryMap * heirarchy = GetObject<MixGroupCategoryMap>(GetObject<MixGroup>(mixGroup->GetObjectHash())->Map);
				for(u32 j=0; j < heirarchy->numEntrys; j++)
				{
					audCategory *parent = g_AudioEngine.GetCategoryManager().GetCategoryPtr(heirarchy->Entry[j].ParentHash);
					dmErrorf("	%d %s: %d, parent: %s, parent index: %i", j, g_AudioEngine.GetCategoryManager().GetCategoryPtr(heirarchy->Entry[j].CategoryHash)->GetNameString(), mixGroup->GetCategoryIndex(), parent ? parent->GetNameString() : "no parent", parent ? g_AudioEngine.GetCategoryManager().GetCategoryIndex(parent) : -1 );
				}
			}
		}
	}

	if(g_PrintMixgroupHeirarchyCats)
	{
		dmErrorf("----------");
		for(int i = 0; i < NUM_AUD_MIXGROUPS; i++)
		{
			audMixGroup * mixGroup = &g_MixGroupPoolMem[i];
			if(mixGroup->m_MixGroupIndex != -1)
			{
				dmErrorf("Mixgroup %s heirarchy, Index %d, Cat root index %d", GetMetadataManager().GetObjectName(mixGroup->GetObjectHash()), mixGroup->GetIndex(), mixGroup->GetCategoryIndex());
				PrintCategoryInfo(g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(mixGroup->GetCategoryIndex()));
			}
		}
	}
		
	g_PrintMixgroupCatMaps = false;
	g_PrintMixgroupHeirarchies = false;
	g_PrintMixgroupHeirarchyCats = false;
#endif
}

u32 audDynamicMixMgr::GetMixGroupInstIndex(const audMixGroup * mixGroup)
{
	if(mixGroup)
	{
		return (s32)(mixGroup - g_MixGroupPoolMem); 
	}
	return ~0U;
}

//////////////////////////////////////////////////////////////////////////////////
//audMixGroup
//////////////////////////////////////////////////////////////////////////////////

audMixGroup::audMixGroup()
	:m_MixGroupHash(0)
	,m_MixGroupIndex(-1)
	,m_CategoryIndex(-1)
	,m_Map(NULL)
	  
{}

bool audMixGroup::Init(MixGroup * mixGroupSettings)
{
	Assertf(mixGroupSettings, "Null mixgroup settings object passed into audMixGroup::Init");
	if(!mixGroupSettings)
	{
		return false;
	}

	m_Map = rage_new s16[s_NumMixGroupMapCategories];
	bool foundMixGroup = false;

	for(s32 i = 0; i < s_MixGroupMaps.GetCount(); i++)
	{
		if(s_MixGroupMapHashes[i] == m_MixGroupHash)
		{
			foundMixGroup = true;
			MixGroupCategoryMap * map = DYNAMICMIXMGR.GetMetadataManager().GetObject<MixGroupCategoryMap>(mixGroupSettings->Map);
			Assertf(map, "Couldn't find category hierarchy map for mixgroup %s, this should be auto-generated", DYNAMICMIXMGR.GetMetadataManager().GetObjectNameFromNameTableOffset(mixGroupSettings->NameTableOffset));
			if(!map)	
			{
				return false;
			}
			Assign(m_MixGroupIndex, i);
			Assign(m_CategoryIndex, g_AudioEngine.GetCategoryManager().InitDynamicHeirarchy(map->Entry[0].CategoryHash, map));
			for(s32 j=0; j < static_cast<s32>(s_NumMixGroupMapCategories); j++)
			{
				Assign(m_Map[j], j);
				if(s_MixGroupMaps[i][j])
				{
					Assign(m_Map[j], g_AudioEngine.GetCategoryManager().GetDynamicCategoryIndex(m_CategoryIndex, s_MixGroupMaps[i][j]->GetMetadata()));
					if(AUD_GET_TRISTATE_VALUE(mixGroupSettings->Flags, FLAG_ID_MIXGROUP_OVERRIDEPARENT) == AUD_TRISTATE_TRUE)
					{
						audCategory * cat = g_AudioEngine.GetCategoryManager().GetDynamicCategoryPtr(m_Map[j], s_MixGroupMaps[i][j]->GetMetadata());
						if(cat)
						{
							cat->SetParentOverrides(true);
						}
					}
				}
			}
			break;
		}
	}

	// Verify that we actually found this mix group - if not, possible that static array sizes need increasing or we haven't set up a particlar DLC pack to autogenerate the mix group list
	audAssertf(foundMixGroup, "Failed to find mixgroup %s in static mix group name list", DYNAMICMIXMGR.GetMetadataManager().GetObjectNameFromNameTableOffset(mixGroupSettings->NameTableOffset));
	g_MixGroupInstanceIds[m_MixGroupIndex] = audDynamicMixMgr::GetMixGroupInstIndex(this);

	return true;
}
	
audMixGroup::~audMixGroup()
{
	Shutdown();
}

void audMixGroup::Shutdown()
{
	if(m_CategoryIndex >= 0)
	{
		g_AudioEngine.GetCategoryManager().ShutdownDynamicHeirachy(m_CategoryIndex);
	}

	//If we're not active, we don't want to shut down
	if(m_MixGroupIndex == -1)
	{
		return;
	}

	 g_MixGroupInstanceIds[m_MixGroupIndex] = ~0U;

	//This tells us that the slot previously occupied by this mix group is now inactive
	m_MixGroupIndex = -1;

	if(m_Map)
	{
		delete [] m_Map;
		m_Map = NULL;
	}
}

#endif //#if !__SPU

#if __SPU
extern u32* g_MixGroupReferenceCountsEa;
#endif

void audMixGroup::AddReference(MixGroupRefType type)
{
	audMixGroup::AddMixGroupReference(m_MixGroupIndex, type);
}

void audMixGroup::RemoveReference(MixGroupRefType type)
{
	audMixGroup::RemoveMixGroupReference(m_MixGroupIndex, type);
}

u32 audMixGroup::GetReferenceCount() const
{
#if __SPU
	return sysInterlockedRead(&g_MixGroupReferenceCountsEa[m_MixGroupIndex]);
#else
	return sysInterlockedRead(&g_MixGroupReferenceCounts[m_MixGroupIndex]);
#endif
}

u32 audMixGroup::GetReferenceCount(s32 mixGroupIndex) 
{
#if __SPU
	return sysInterlockedRead(&g_MixGroupReferenceCountsEa[mixGroupIndex]);
#else
	return sysInterlockedRead(&g_MixGroupReferenceCounts[mixGroupIndex]);
#endif
}

#if DEBUG_MIXGROUPS
void audMixGroup::FormatRefCountStr(char* strBuf, int bufLen, int mixGroupIndex)
{
	if(dmVerifyf(bufLen <= 265, "buffers passed into FormatRefCountStr must be 256 or less"))
	{
		char buf1[256] = {0};
		formatf(buf1, bufLen, "%sTotal refs %u ", strBuf, g_MixGroupReferenceCounts[mixGroupIndex]);
		formatf(strBuf, bufLen, "%s", buf1); 
		for(int i=0; i< NUM_REF_TYPES; i++)
		{
			char buf[256] = {0};
			formatf(buf, bufLen, "%s, %s %u", strBuf, k_RefTypeNames[i], g_MixGroupTypedRefCounts[i][mixGroupIndex]);
			formatf(strBuf, bufLen, "%s", buf);
		}
	}
}
#endif

u32 audMixGroup::AddMixGroupReference(s32 mixGroupIndex, MixGroupRefType DEBUG_MIXGROUPS_ONLY(refType))
{
	TrapEQ(mixGroupIndex, -1);

	u32 ret = sysInterlockedIncrement(&g_MixGroupReferenceCounts[mixGroupIndex]);

#if __DEV
#if DEBUG_MIXGROUPS
	dmAssertf(refType != NULL_REF, "Adding a mixgroup reference with a NULL type: please specify in code what audMixGroup::MixGroupRefType is being added");
	Assert(g_MixGroupTypedRefCounts[refType] != 0); //DO NOT SUBTMIT
	sysInterlockedIncrement(&(g_MixGroupTypedRefCounts[refType][mixGroupIndex]));

	char refCountString[256] = {0};

	FormatRefCountStr(refCountString, 256, mixGroupIndex);
	
	dmDebugf1("Adding %s ref to mixgroup %s (hash %u, index %d) at %s", k_RefTypeNames[refType], DYNAMICMIXMGR.GetMixGroupNameFromIndex(mixGroupIndex), DYNAMICMIXMGR.GetMixGroupHash(mixGroupIndex), mixGroupIndex, refCountString); 
#else
	//dmDebugf3("Adding ref to mixgroup %s (hash %u, index %d) at %u references", DYNAMICMIXMGR.GetMixGroupNameFromIndex(mixGroupIndex), DYNAMICMIXMGR.GetMixGroupHash(mixGroupIndex), mixGroupIndex, g_MixGroupReferenceCounts[mixGroupIndex]); 
#endif
#endif
	return ret;
}

u32 audMixGroup::RemoveMixGroupReference(s32 mixGroupIndex, MixGroupRefType DEBUG_MIXGROUPS_ONLY(refType))
{
	u32 ret = sysInterlockedDecrement(&g_MixGroupReferenceCounts[mixGroupIndex]);

#if __DEV
#if DEBUG_MIXGROUPS 
	dmAssertf(refType != NULL_REF, "Removing a mixgroup reference with a NULL type: please specify in code what audMixGroup::MixGroupRefType is being removed");
	sysInterlockedDecrement(&(g_MixGroupTypedRefCounts[refType][mixGroupIndex]));
	char refCountString[256] = {0};

	FormatRefCountStr(refCountString, 256, mixGroupIndex);

	dmDebugf1("Removing %s ref to mixgroup %s (hash %u, index %d) at %s", k_RefTypeNames[refType], DYNAMICMIXMGR.GetMixGroupNameFromIndex(mixGroupIndex), DYNAMICMIXMGR.GetMixGroupHash(mixGroupIndex), mixGroupIndex, refCountString); 
#else
	//dmDebugf1("Removing ref to mixgroup %s (hash %u, index %d) at %u references", DYNAMICMIXMGR.GetMixGroupNameFromIndex(mixGroupIndex), DYNAMICMIXMGR.GetMixGroupHash(mixGroupIndex), mixGroupIndex, g_MixGroupReferenceCounts[mixGroupIndex]); 
#endif
#endif

	TrapEQ(ret, ~0U);
	return ret;
}

#if __DEV

const char *audDynamicMixMgr::GetMixGroupName(const u32 mixGroupHash) const
{
	return m_MetadataMgr.GetObjectName(mixGroupHash);
}

const char *audDynamicMixMgr::GetMixModuleName(const u32 moduleHash) const 
{
	return m_MetadataMgr.GetObjectNameFromNameTableOffset(m_MetadataMgr.GetObject<DynamicMixModuleSettings>(moduleHash)->NameTableOffset);
}
#endif // __DEV

} //namespace rage
