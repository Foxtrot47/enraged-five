//
// audioengine/engine.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// main include for audio code
#ifndef AUD_ENGINE_H
#define AUD_ENGINE_H

#include "audioengine/ambisonics.h"
#include "categorymanager.h"
#include "curverepository.h"
#include "dynamicmixmgr.h"
#include "engineconfig.h"
#include "environment.h"
#include "remotecontrol.h"
#include "remotedebug.h"
#include "soundmanager.h"

#include "system/ipc.h"

namespace rage
{
	//	Function pointer to update any game specific tasks in the audio thread
	typedef void (*pfUpdateAudioThread)();

	class audWaveSlot;
	class audController;
	class audMetadataManager;
	class sysMemAllocator;

	// PURPOSE
	//	Initialize the audio engine.
	// PARAMS
	//	controller			- The controller to setup for the calling thread
	//	physicalAllocator	- the memory allocator to use for physical (wave data etc) allocations
	//	virtualAllocator	- the memory allocator to use for virtual allocations
	//	configPath			- The path of the directory that holds the audio engine and driver configuration files.
	//							This must be relative to the root folder of the game. If unspecified, the audio
	//							configuration files are assumed to reside in the '/config' subfolder of the root folder of
	//							the game.
	//	relativeAudioPath	- Optionally configPath can be a path relative to this path passed in. All audio paths
	//							will subsequently be appended relative to this path including paths specified in 
	//							config xml files.
	bool InitializeAudio(audController &controller, sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator,
		const char *configPath=NULL, const char* relativeAudioPath=NULL, void *externallyAllocatedBankHeap=NULL, u32 externallyAllocatedBankHeapSize=0);


// PURPOSE
//	Handles audio engine initialization/shutdown and manages the audio thread.  All other audio subsystems
//	are initialized/shutdown and Updated from here
class audEngine 
{
public:

	audEngine();
	~audEngine();

	// PURPOSE
	//	Initialises the entire audio engine including all managers and hardware
	// PARAMS
	//	physicalAllocator	- The memory allocator to use for physical (wave data etc) allocations
	//	virtualAllocator	- The memory allocator to use for virtual allocations
	//	configPath			- The path of the directory that holds the audio engine and driver configuration files.
	//							This must be relative to the root folder of the game. If unspecified, the audio
	//							configuration files are assumed to reside in the '/config' subfolder of the root folder of
	//							the game.
	//	relativeAudioPath	- Optionally configPath can be a path relative to this path passed in. All audio paths
	//							will subsequently be appended relative to this path including paths specified in 
	//							config xml files.
	// RETURNS
	//	True on success, false otherwise
	bool Init(sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator, const char *configPath, const char* relativeAudioPath, void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize);

	// PURPOSE
	//	Initialises the majority of the audio engine, but nothing static, as we'll call this on Reset() as well.
	// RETURNS
	//	True on success, false otherwise
	bool PartialInit();

	// PURPOSE
	//	Shuts down the entire audio engine including all managers and hardware
	void Shutdown();

	// PURPOSE
	//	Shuts down the majority of the audio engine, but nothing static, as we'll call this on Reset() as well.
	void PartialShutdown();

	// PURPOSE
	//	Resets the entire audio engine by shutting it down then re-initializing it
	// RETURNS
	//	True on success, false indicates Init() failed
	bool Reset();


#if __BANK
	// PURPOSE
	//	populates the supplied bank - calls audio subsystems and allows them
	//	to add their parameters
	// PARAMS
	//	bank - the bank to which widgets should be added
	void AddWidgets(bkBank &bank);

	// PURPOSE
	//	Returns the audio engine's audRemoteControl instance
	audRemoteControl &GetRemoteControl()
	{
		return m_RemoteControl;
	}
	audRemoteDebug &GetRemoteDebug()
	{
		return m_RemoteDebug;
	}
#endif // __BANK

	// PURPOSE
	//	Called by game thread whenever the listener/camera postion/orientation changes
	// PARAMS
	//	listener - matrix representing the new listener position/orientation
	// SEE ALSO
	//	audEnvironment
	bool SetVolumeListenerMatrix(Mat34V_In listener, u32 listenerId = 0 );
	bool SetPanningListenerMatrix(Mat34V_In listener, u32 listenerId = 0);

	bool SetVolumeListenerMatrix(const Matrix34 &listener, u32 listenerId = 0)
	{
		return SetVolumeListenerMatrix(MATRIX34_TO_MAT34V(listener), listenerId);
	}
	bool SetPanningListenerMatrix(const Matrix34 &listener, u32 listenerId = 0)
	{
		return SetPanningListenerMatrix(MATRIX34_TO_MAT34V(listener), listenerId);
	}
	// PURPOSE
	//  Commits this frame's listener settings. Must be called, or listener position will never change.
	//  Similarly to requested settings, this is triple buffered, with an atomic index change, to achieve
	//  thread-safe atomic settings without any actual locking. 
	void CommitGameSettings(u32 timeInMs);

	// PURPOSE
	//	Returns the current time in milliseconds, updated once per audio frame.  This is the
	//	time that is used by all audio code.
	// RETURNS
	//	Current time in whole milliseconds
	u32 GetTimeInMilliseconds() const;

	// PURPOSE
	//	Returns the time difference in seconds between this update and the last update, updated once per audio frame.
	// RETURNS
	//	Time delta in seconds
	f32 GetTimeDeltaInSeconds() const
	{
		return m_TimeDeltaInSeconds;
	}

	// PURPOSE
	// Returns the audio thread id
	sysIpcThreadId GetAudioThreadId() const;
	bool IsAudioThread() const { return sysIpcGetCurrentThreadId() == m_AudioThreadIdForComparison; }

	// PURPOSE
	//	Ensures nothing is played physically
	void StartVirtualisingEverything();

	// PURPOSE
	//	Resumes normal physical playback 
	void StopVirtualisingEverything();

	// PURPOSE
	//	Returns the number of active physical voices
	u32 GetNumberOfActivePhysicalVoices() const;

	// PURPOSE
	//	Returns true if the game is in control of music, false otherwise.
	// NOTES
	//	If false is returned from this function then all sounds/categories that
	//	mute on user playback will be currently muted.  e.g. when the user is playing
	//	music via the HUD on Xbox360
	bool IsGameInControlOfMusicPlayback() const;
	
	// PURPOSE
	//	Returns the audEngineConfig class containing static engine configuration settings
	const audEngineConfig &GetConfig() const
	{
		return m_Config;
	}

	// PURPOSE
	//  Returns true if audControllers should process new sound requests, false if not.
	bool IsAudioEnabled() const
	{
		return m_IsAudioEnabled;
	}

	// PURPOSE
	//	Returns true if the engine should be using the cheap audio effects.
	// RETURNS
	//	True if the engine should be using the cheap audio effects.
	//
	bool ShouldUseCheapAudioEffects() const 
	{
		return m_ShouldUseCheapAudioEffects;
	}

	void* AllocateVirtual(const size_t size, const size_t align = 16);
	void FreeVirtual(const void *ptr);
	void* AllocatePhysical(const size_t size, const size_t align = 16);
	void FreePhysical(const void *ptr);

	bool IsShuttingDown()
	{
		return !m_ShouldThreadBeRunning;
	}

	void RequestThreadPause(bool paused)
	{
		m_ShouldThreadBePaused = paused;
	}

	// PURPOSE
	//	Lock the engine update
	static void EnterUpdateLock()
	{
		sm_EngineUpdateLock.Lock();
	}

	// PURPOSE
	//	Unlock the engine update
	static void ExitUpdateLock()
	{
		sm_EngineUpdateLock.Unlock();
	}

	// PURPOSE
	//  Sets whether the audio engine is active - meaning whether audControllers accept new sound requests.
	//  This is overridden by -audio or -noaudio command line flags.
	// PARAM
	//  enable - true to allow new sound requests, false to ignore them.
	void EnableAudio(const bool enable);

	// PURPOSE
	//	Returns true if the user is currently playing his/her own music, currently only supported on XENON and PS3
	// RETURNS
	//	true if the user is currently playing his/her own music, currently only supported on XENON and PS3
	bool IsUserMusicPlaying() const;
	
	// PURPOSE
	//	Forces the user's current background music to temporarily halt until RestoreBackgroundMusic() is called.
	//	Currently implemented on XENON and PS3.  Only intended to be used when absolutely necessary, eg. cutscene with music
	void OverrideUserMusic();

	// PURPOSE
	//	Restores the user's background music after a call to OverrideBackgroundMusic()
	//	Currently implemented on XENON and PS3.
	void RestoreUserMusic();

	audEnvironment &GetEnvironment()
	{
		return m_Environment;
	}

#if AUD_ENABLE_AMBISONICS
	audAmbisonics &GetAmbisonics()
	{
		return m_Ambisonics;
	}
#endif

	audCategoryManager &GetCategoryManager()
	{
		return m_CategoryManager;
	}

	audCurveRepository &GetCurveRepository()
	{
		return m_CurveRepository;
	}


	audDynamicMixMgr &GetDynamicMixManager()
	{
		return m_DynamicMixMgr;
	}

	audSoundManager &GetSoundManager()
	{
		return m_SoundManager;
	}

	const char* GetRelativeAudioPath() const
	{
		return m_RelativeAudioPath;
	}
	const char* GetConfigPath() const
	{
		return m_ConfigPath;
	}

	// PURPOSE
	//  Takes in a void function pointer that will be called once per audio frame
	// PARAM
	//  void (*func)(void) functor, Passing NULL returns it back to default DoNothing()
	void SetUserGameUpdateFunction(pfUpdateAudioThread fn)
	{
		if (fn == NULL)
			m_pfUpdateAudioThread = &audEngine::DoNothing;
		else
			m_pfUpdateAudioThread = fn;
	}

	// PURPOSE
	//  Returns the user set update function pointer that is called on the audio thread
	pfUpdateAudioThread GetUserGameUpdateFunction()
	{
		return m_pfUpdateAudioThread;
	}

	u32 GetEngineTimeFrames() const
	{
		return m_EngineTimeFrames;
	}

	void SetTriggeredUpdates(bool triggeredUpdates)	{ m_TriggeredUpdates = triggeredUpdates; }

	void DrawDebug();

	// This is to force a specific number of audio updates to happen per frame.
	// It is up to the higher level game code (usually the game's Update() function) to call TriggerUpdate
	void TriggerUpdate();
	void WaitForAudioFrame();
	void PollForAudioFrame();

	// PURPOSE
	//	Executes an audio frame - services all audio systems
	void AudioFrame();

	// Used to tell the audio engine if the game is paused
	void SetGamePaused(bool isPaused)	{ m_IsGamePaused = isPaused; }

private:
	// PURPOSE
	//	Kicks off our audio servicing thread
	// RETURNS
	//	True on success, false if the thread initialization failed
	bool InitAudioThread();

	// PURPOSE
	//	Main audio loop function
	static DECLARE_THREAD_FUNC(ThreadEntryProc);


	// PURPOSE
	//	This is the default for m_pfUpdateAudioThread 
	static void DoNothing() {}

	audCategoryManager m_CategoryManager;
	audCurveRepository m_CurveRepository;
	audDynamicMixMgr m_DynamicMixMgr;
	audSoundManager m_SoundManager;

	volatile bool	m_IsThreadRunning, m_ShouldThreadBeRunning, m_ShouldThreadBePaused;
	sysIpcThreadId	m_ThreadId;
	sysIpcCurrentThreadId m_AudioThreadIdForComparison;
	static sysCriticalSectionToken sm_EngineUpdateLock;
	u32				m_PrevTimeInMs;
	u32				m_TimeInMs;
	f32				m_TimeDeltaInSeconds;
	u32				m_MixerTimeFrames;
	u32				m_EngineTimeFrames;

	u32				m_NumPhysicalVoices;

	bool m_ShouldBeVirtualisingEverything;
	bool m_CurrentlyVirtualisingEverything;
	bool m_IsAudioEnabled;
	bool m_ShouldUseCheapAudioEffects;

#if __BANK
	audRemoteControl	m_RemoteControl;
	audRemoteDebug		m_RemoteDebug;
	static u32			sm_NumberOfPhysicalChannels;
	static bool			sm_OverrideChannels;
#endif

	audEngineConfig m_Config;

	sysMemAllocator	*m_PhysicalAllocator, *m_VirtualAllocator;

	f32				m_MusicVolume;
	f32				m_SfxVolume;
	f32				m_MasterVolume;

	audEnvironment	m_Environment;

#if AUD_ENABLE_AMBISONICS
	audAmbisonics	m_Ambisonics;
#endif

	//	Function pointer to update any game specific tasks in the audio thread
	pfUpdateAudioThread m_pfUpdateAudioThread;

	char m_RelativeAudioPath[256]; // Loading of all audio assets will be relative to this path
	char m_ConfigPath[256];

	void* m_ExternallyAllocatedBankHeap;
	u32 m_ExternallyAllocatedBankHeapSize;

	volatile bool m_TriggeredUpdates; // updates when TriggerUpdate() is called, rather than every N milliseconds
	volatile bool m_EnableTriggeredWait;

	sysIpcSema m_UpdateSema;
	sysIpcSema m_UpdateFinishSema;

	bool m_IsGamePaused;

};

inline sysIpcThreadId audEngine::GetAudioThreadId() const
{
	return m_ThreadId;
}

#if !__SPU
inline bool audEngine::SetVolumeListenerMatrix(Mat34V_In listener, u32 listenerId)
{
	return m_Environment.SetVolumeListenerMatrix(listener, listenerId);
}

inline bool audEngine::SetPanningListenerMatrix(Mat34V_In listener, u32 listenerId)
{
	return m_Environment.SetPanningListenerMatrix(listener, listenerId);
}
#endif
inline u32 audEngine::GetTimeInMilliseconds() const
{
	return m_TimeInMs;
}

inline void audEngine::StartVirtualisingEverything()
{
	m_ShouldBeVirtualisingEverything = true;
}
inline void audEngine::StopVirtualisingEverything()
{
	m_ShouldBeVirtualisingEverything = false;
}


extern audEngine g_AudioEngine;

} // namespace rage

#endif // AUD_AUDIOENGINE_H
