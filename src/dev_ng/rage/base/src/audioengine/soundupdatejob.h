// 
// audioengine/soundupdatejob.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_SOUNDUPDATEJOB_H
#define AUD_SOUNDUPDATEJOB_H

#if __PS3

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"
#include "enginedefs.h"
DECLARE_TASK_INTERFACE(SoundUpdateJob);


namespace rage
{

const u32 g_MaxWaveSlotRequestsPerJob = 32;

struct tWaveSlotState
{
	u32 waveNameHash;	
	u16 bankId;
	u8 loadPriority;
	u8 waveSlotId;
	u8 streamState;
};
struct audEnvironmentListenerData;
struct audEnvironmentSoundMetrics;
class audMixerSyncManager;
class audMixGroup;
struct ALIGNAS(16) tSoundUpdateJobData
{
	u32 numSoundSlotsPerBucket;
	u32 numReqSetSlotsPerBucketAligned;
	u32 soundSlotSize;
	u32 reqSetSlotSize;
	const audEnvironmentListenerData *listenerData;

	void *waveSlots;
	void *waveSlotReferenceCounts;
	u32 numWaveSlots;

	audMixGroup *mixGroupPoolMem; //this is a direct pointer to the global mix groups and can only be used for reference management
	u32 * mixGroupReferenceCounts; //static data used for reference counfting; not dma'ds
	size_t numMixGroupReferenceCounts;

	u32 audSoundPoolBucketSize;
	u32 numOutputChannels;
	u32 numBucketsToProcess;
	void *bucketLocks;
	variableNameValuePair *globalVariables;
	float *entityVariables;
	u32 * entityVariableRefs;
	void *pcmSourcePool;
	void *pcmSourceState;
	u32 numPcmSourcePoolSlots;
	audTimerState timers[g_NumAudioTimers];
	u32 requestedSettingsReadIndex;

	audMixerSyncManager *syncManagerEA;
	void *soundPoolBufferEA;

	const u8 *disabledMetadataPtr;
	u32 disabledMetadataSize;
};

struct ALIGNAS(16) tSoundUpdateJobResult
{
	u32 numSoundsDormant;
	u32 numSoundsPreparing;
	u32 numSoundsPlaying;
	u32 numSoundsWaitingToBeDeleted;
	u32 numWaveSlotRequests;
	u32 scratchSpaceLeft;
	u32 commandBufferUsed;
	tWaveSlotState waveSlotRequests[g_MaxWaveSlotRequestsPerJob];
#if __DEV
	u32 entityVariableCacheHits;
	u32 entityVariableCacheMisses;
#endif
};

}
#endif //__PS3
#endif //AUD_SOUNDUPDATEJOB_H
