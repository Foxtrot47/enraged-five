//
// audioengine/curverepository.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_CURVEREPOSITORY_H
#define AUD_CURVEREPOSITORY_H

#include "curve.h"
#include "metadatamanager.h"

#include "string/stringhash.h"

#define DEBUG_CURVE 1

namespace rage {

// PURPOSE
//  This class manages access to curves. It uses a block of curve (essentially the same as sound) metadata
//  to call the appropriate static curve function, pointing at the requested metadata.
class audCurveRepository : audObjectModifiedInterface
{
	friend class audCurve;
public:

	audCurveRepository();
	~audCurveRepository();

	// PURPOSE
	//	Initializes the metadata manager by loading curves.dat into memory and building lookup structures
	// RETURNS
	//	True if successful, false otherwise
	bool Init();

	// PURPOSE
	//	Frees all curve metadata memory
	void Shutdown();

	// PURPOSE
	//  Generates a straight line between two points, (minInput, minOutput) and (maxInput, maxOutput) and
	//  returns the value of the line at the x co-ord given by input. input is forced into the range 
	//  min-maxInput.
	// PARAMS
	//  minOutput - the output associated with an input of minInput
	//  maxOutput - the output associated with an input of maxInput - minOutput>maxOutput is valid
	//  minInput - the lowest valid input value
	//  maxInput - the highest valid input value
	//  input - the input value to be interpolated.
	// RETURNS
	//  The interpolated value.
	static inline float GetLinearInterpolatedValue(float minOutput, float maxOutput, 
					float minInput, float maxInput, float input)
	{
		float ratio = ClampRange(input, minInput, maxInput); // this puts it in range 0-1
		return Lerp(ratio, minOutput, maxOutput);
	}

#if __BANK
	// PURPOSE
	//	Returns the name of the curve from the specified name table offset
	const char *GetCurveNameFromNameTableOffset(u32 nameTableOffset)
	{
		return m_MetadataMgr.GetObjectNameFromNameTableOffset(nameTableOffset);
	}
#endif

	bool LoadMetadataChunk(const char *chunkName, const char *metadata)
	{
		return m_MetadataMgr.LoadMetadataChunk(chunkName, metadata);
	}

	bool UnloadMetadataChunk(const char *chunkName)
	{
		return m_MetadataMgr.UnloadMetadataChunk(chunkName);
	}

	const audMetadataManager &GetMetadataManager() const
	{
		return m_MetadataMgr;
	}

	audMetadataManager &GetMetadataManager()
	{
		return m_MetadataMgr;
	}


#if RSG_BANK
	void OnObjectModified(const u32 nameHash);
	void OnObjectOverridden(const u32 nameHash);
#endif // RSG_BANK

private:

	
	// PURPOSE
	//  Searches for the curve given by curveNameHash, and populates the metadata ptr with 
	//  the appropriate values.
	//  This is only used by curves themselves, on initialisation. (Should probably make protected and have
	//  them as friend classes.)
	// PARAMS
	//  curveNameHash - the desired curve
	//  metadata - ptr to a variable in which to store a ptr to the curve's metadata
	// RETURNS
	//  true if the curve was found
	bool GetCurveMetadataPtr(u32 curveNameHash, void** metadata);

	audMetadataManager	m_MetadataMgr;
	
};

} // namespace rage

#endif //AUD_CURVEREPOSITORY_H
