#include "tempomap.h"

namespace rage
{

void audTempoMap::ComputeTimelinePosition(const float timeS, TimelinePosition &pos) const
{
	pos.Bar = 0;
	pos.Beat = 0;

	float accumulatedTimeS = 0.f;

	for(s32 i = 0; i < m_TempoMapEntries.GetCount(); i++)
	{
		// Is the requested time in this section (or beyond the end of the tempo map)
		if(i == m_TempoMapEntries.GetCount() - 1 || m_TempoMapEntries[i].LengthSeconds + accumulatedTimeS > timeS)
		{
			const float timeInSection = timeS - accumulatedTimeS;
			const u32 numBeats = static_cast<u32>(ComputeNumBeats(timeInSection, m_TempoMapEntries[i].Tempo));
			const u32 numBars = numBeats / m_TempoMapEntries[i].Meter;
			pos.Bar += numBars;
			pos.Beat = numBeats - numBars * m_TempoMapEntries[i].Meter;
			pos.BeatOffsetS = timeInSection - numBeats * ComputeBeatLength(m_TempoMapEntries[i].Tempo);

			// Start from bar/beat 1, rather than 0
			pos.Beat++;
			pos.Bar++;

			pos.Tempo = m_TempoMapEntries[i].Tempo;
			pos.Meter = m_TempoMapEntries[i].Meter;
			pos.BeatLength = ComputeBeatLength(m_TempoMapEntries[i].Tempo);
			return;
		}

		// accumulate bars in this section
		// Note: we don't support tempo markers that aren't bar aligned
		pos.Bar += m_TempoMapEntries[i].LengthBars;
		accumulatedTimeS += m_TempoMapEntries[i].LengthSeconds;
	}
}

float audTempoMap::ComputeTimeForPosition(const TimelinePosition &inputPos) const
{
	TimelinePosition pos = inputPos;

	// The musical timeline starts at bar 1, beat 1.
	pos.Bar -= 1;
	pos.Beat -= 1;

	u32 accumulatedBars = 0;
	float accumulatedTimeS = 0.f;
	const s32 numEntries = m_TempoMapEntries.GetCount();
	for(s32 i = 0; i < numEntries; i++)
	{
		// Is the requested bar in this section?
		if(i == numEntries - 1 ||
			pos.Bar < accumulatedBars + m_TempoMapEntries[i].LengthBars)
		{
			u32 numBeats = pos.Beat + (pos.Bar - accumulatedBars) * m_TempoMapEntries[i].Meter;
			return pos.BeatOffsetS + accumulatedTimeS + numBeats * ComputeBeatLength(m_TempoMapEntries[i].Tempo);
		}
		else
		{
			accumulatedBars += m_TempoMapEntries[i].LengthBars;
			accumulatedTimeS += m_TempoMapEntries[i].LengthSeconds;
		}
	}

	// We can only reach here if our tempo map is empty
	return 0.f;
}

f32 audTempoMap::ComputeNextBeatTime(const f32 timeMs) const
{
	TimelinePosition pos;
	ComputeTimelinePosition(timeMs * 0.001f, pos);

	// The supplied time is in milliseconds so our timeline position calculation loses some accuracy. 
	// We need to round up/down to the nearest beat as appropriate if we're within the error range.
	if(Abs((s32)Floorf((pos.BeatOffsetS * 1000.f) + 0.5f) - (s32)Floorf((pos.BeatLength * 1000.f) + 0.5f)) <= 1)
	{
		if(pos.BeatOffsetS > (pos.BeatLength * 0.5f))
		{
			pos.IncrementBeat();
		}
		else
		{
			pos.DecrementBeat();
		}
	}

	// We're selecting the next beat so move on by one
	pos.IncrementBeat();
	pos.BeatOffsetS = 0.0f;	

	// Finally, convert our new beat into a time value
	f32 beatTime = ComputeTimeForPosition(pos);
	return beatTime;
}

f32 audTempoMap::ComputePrevBeatTime(const f32 timeMs) const
{
	TimelinePosition pos;
	ComputeTimelinePosition(timeMs * 0.001f, pos);

	// The supplied time is in milliseconds so our timeline position calculation loses some accuracy. 
	// We need to round up/down to the nearest beat as appropriate if we're within the error range.
	if(Abs((s32)Floorf((pos.BeatOffsetS * 1000.f) + 0.5f) - (s32)Floorf((pos.BeatLength * 1000.f) + 0.5f)) <= 1)
	{
		if(pos.BeatOffsetS > (pos.BeatLength * 0.5f))
		{
			pos.IncrementBeat();
		}
		else
		{
			pos.DecrementBeat();
		}
	}

	// We're selecting the previous beat so move back by one
	pos.DecrementBeat();
	pos.BeatOffsetS = 0.0f;	

	// Finally, convert our new beat into a time value
	f32 beatTime = ComputeTimeForPosition(pos);
	return beatTime;
}

} // namespace rage
