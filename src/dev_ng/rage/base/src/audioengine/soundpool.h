//
// audioengine/soundpool.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SOUNDPOOL_H
#define AUD_SOUNDPOOL_H

#include "audiohardware/voicesettings.h"
#include "audiosoundtypes/soundcontrol.h"
#include "enginedefs.h"
#include "environment.h"
#include "metadataref.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "system/cellsyncmutex.h"
#include "system/criticalsection.h"
#include "system/ipc.h"

#define AUD_SOUNDPOOL_LOCK_ON_SPU (__PS3)

namespace rage
{
#ifndef AUD_VOICE_SPU

struct audEngineContext;
class audSound;
class audRequestedSettings;

typedef void (*audSoundPoolCallback)(audEngineContext *context, audSound *sound);

struct audSoundPoolStats
{
	u32 fullestBucket;
	u32 emptiestBucket;
	u32 mostSoundSlotsFree;
	u32 leastSoundSlotsFree;
	u32 mostReqSetsSlotsFree;
	u32 leastReqSetsSlotsFree;
	u32 soundSlotsAllocated;
	u32 soundSlotsFree;
	u32 leastVoicesFree;
	u32 mostVoicesUsed;
	u32 skippedChildProcessing;
};

#endif // !AUD_VOICE_SPU

struct audChildSoundRequest
{
	audSoundInternalInitParams initParams;
	audSoundScratchInitParams scratchInitParams;
	audMetadataRef metadataOffset;
	u8 parentSoundIdx;
	u8 childIndex;	
};

// Disabled on PS3 for now to help fit soundupdatejob on SPU
#if __PS3
#define AUD_POOL_CHECK_ALLOCATION 0
#else
#define AUD_POOL_CHECK_ALLOCATION (__ASSERT || __BANK)
#endif

#if AUD_SUPPORT_RAVE_EDITING
struct audSoundVarianceCache
{
	f32 VolumeOffset;	
	s32 PredelayOffset, StartOffsetOffset;
	s16	LPFOffset, HPFOffset;
	s16 PitchOffset, PanOffset;
	
	u32 TimeInitialised;
	
};
#endif // AUD_SUPPORT_RAVE_EDITING

struct audSoundPoolBucket
{
	// NOTE:
	//	Once we can remove top level sounds, the most voice hungry a single bucket would consist of hierarchies
	//	containing just an environmentsound and requested settings so we'd need one voice for every two slots
	atFixedBitSet<g_audMaxVoicesPerBucket> voiceAllocationState;
	atRangeArray<audVoiceSettings, g_audMaxVoicesPerBucket> voiceSettings;
	
	// store all child requests
	atRangeArray<audChildSoundRequest, g_audMaxChildSoundRequestsPerBucket> childSoundRequests;

#if __PS3
	atRangeArray<audSoundCombineBuffer, g_audMaxVoicesPerBucket> environmentSoundRequests;
	atRangeArray<u8, g_audMaxVoicesPerBucket> environmentSoundRequestSlotIds;
#endif 

	// Linked list of unallocated sound slot indices
	atRangeArray<u8,g_audMaxSoundSlotsPerBucket> soundFreeList;
	// 1 bit of parent flags per slot
	atRangeArray<u32, ((g_audMaxSoundSlotsPerBucket+1) >> 5)> soundSlotIsParent;
	// linked list of unallocated requested settings slot indices
	atRangeArray<u8,g_audMaxRequestedSettingsSlotsPerBucket> requestedSettingsFreeList;

#if AUD_POOL_CHECK_ALLOCATION
	// 1 bit of allocation state per slot
	atFixedBitSet<g_audMaxSoundSlotsPerBucket> soundSlotAllocationState;
	// 1 bit of allocation state per slot
	atFixedBitSet<g_audMaxRequestedSettingsSlotsPerBucket> requestedSettingsSlotAllocationState;
#endif // AUD_POOL_CHECK_ALLOCATION

	// 1 bit of allocation type per slot (storage/sound)
	atFixedBitSet<g_audMaxSoundSlotsPerBucket> soundSlotAllocationType;

	// how many child requests are active in this bucket
	u8 numChildRequests;
	// how many slots are free in this bucket
	u8 numSoundSlotsFree;
	u8 numRequestedSettingsSlotsFree;
	u8 numVoicesFree;
#if __PS3
	// number of populated environment sound metrics
	u8 numEnvironmentSoundRequests;
#endif // __PS3
	// ptr to first sound slot in this bucket
	void *baseSoundPtr;
	void *baseRequestedSettingsPtr;

	u8 firstFreeSoundSlotIndex;
	u8 firstFreeRequestedSettingsSlotIndex;
	u8 currentUniqueId;
	// this is set to true if this bucket is ProcessingChildSoundRequests
	bool isProcessingChildSoundRequests;

#if __DEV
	const char **reqSetOwners;
#endif
} ;

#ifndef AUD_VOICE_SPU
#if __SPU
extern audSoundPoolBucket *g_Bucket;
extern u32 g_BucketSoundSlotSize;
extern u32 g_NumSoundSlotsPerBucket;
extern u32 g_BucketRequestedSettingsSlotSize;
extern u32 g_NumRequestedSettingsSlotsPerBucketAligned;
#endif
#else
extern audSoundPoolBucket *g_Bucket;
#endif
// PURPOSE
//	Manages allocation of sounds across a set of buckets, so that every sound hierarchy is contained within
//	exactly one bucket.
class audSoundPool
{
	friend class audSoundManager;
public:
	
#ifndef AUD_VOICE_SPU
	enum audSoundAllocationType
	{
		AUD_SOUND_ALLOCATION,
		AUD_REQUESTEDSETTINGS_ALLOCATION,
	};

	audSoundPool()
		: m_Buckets(NULL/*, 0*/) // HACK_GTA4
	{
	}

	~audSoundPool();

	// PURPOSE
	//	Allocates a slot from the specified bucket
	// NOTE
	//	If isStorageOnly is true then this slot will never be updated as a sound
	void *AllocateSoundSlot(const size_t size, const u32 bucketId, const bool isStorageOnly = false);
#if __DEV
	void *AllocateRequestedSettingsSlot(const size_t size, const u32 bucketId, const char *owner);
#else
	void *AllocateRequestedSettingsSlot(const size_t size, const u32 bucketId);
#endif
	// PURPOSE
	//	Frees the specified slot
	void DeleteSound(void *p, u32 bucketId);
	void DeleteRequestedSettings(void *p, u32 bucketId);
	void DeleteRequestedSettings(u32 bucketId, u32 slotIndex);

	// PURPOSE
	//	Returns an slot index for the supplied ptr
	u8 GetSoundSlotIndex(const u32 bucketId, const void *ptr) const
	{
#if __SPU
		return (u8)(ptr ? (((size_t)ptr - (size_t)g_Bucket->baseSoundPtr) / g_BucketSoundSlotSize) : 0xff);
#else
		return (u8)(ptr ? (((size_t)ptr - (size_t)m_Buckets[bucketId].baseSoundPtr) / m_SoundSlotSize) : 0xff);
#endif
	}

	// PURPOSE
	//	Returns an slot index for the supplied ptr
	u8 GetRequestedSettingsSlotIndex(const u32 bucketId, const void *ptr)
	{
#if __SPU
		return (u8)(ptr ? (((size_t)ptr - (size_t)g_Bucket->baseRequestedSettingsPtr) / g_BucketRequestedSettingsSlotSize) : 0xff);
#else
		return (u8)(ptr ? (((size_t)ptr - (size_t)m_Buckets[bucketId].baseRequestedSettingsPtr) / m_RequestedSettingsSlotSize) : 0xff);
#endif
	}

	// PURPOSE
	//	Returns a ptr to the slot for the specified index
	void *GetSoundSlot(const u32 bucketId, const u32 index)
	{
#if __SPU
		return (char*)g_Bucket->baseSoundPtr + (index * g_BucketSoundSlotSize);
#else
		return (char*)m_Buckets[bucketId].baseSoundPtr + (index * m_SoundSlotSize);
#endif
	}

	// PURPOSE
	//	Returns a ptr to the slot for the specified index
	void *GetRequestedSettingsSlot(const u32 bucketId, const u32 index)
	{
#if __SPU
		return (char*)g_Bucket->baseRequestedSettingsPtr + (index * g_BucketRequestedSettingsSlotSize);
#else
		return (char*)m_Buckets[bucketId].baseRequestedSettingsPtr + (index * m_RequestedSettingsSlotSize);
#endif
	}

 	u32 GetNumBuckets() const
	{
		return m_NumBuckets;
	}

	u32 GetSoundSlotSize() const
	{
#if __SPU
		return g_BucketSoundSlotSize;
#else
		return m_SoundSlotSize;
#endif
	}

	u32 GetRequestedSettingsSlotSize() const
	{
#if __SPU
		return g_BucketRequestedSettingsSlotSize;
#else
		return m_RequestedSettingsSlotSize;
#endif
	}

	audSoundPoolBucket *GetBucket(const u32 bucketId) const
	{
		TrapGT(bucketId, m_NumBuckets);
#if __SPU
		return g_Bucket;
#else
		return &m_Buckets[bucketId];
#endif
	}

	void MarkSoundSlotAsParent(const u32 bucketId, const u32 slotId);

	// PURPOSE
	//	Allocates a virtual voice from the specified bucket
	// RETURNS
	//	per-bucket voice id to use, 0xff denotes failure
	u8 AllocateVoice(const u32 bucketId);

	// PURPOSE
	//	Frees the specified voice
	void FreeVoice(const u32 bucketId, const u32 voiceId);

	u32 GetUniqueId(const u32 bucketId);

#if !__SPU

	void LockBucket(const u32 bucketId);
	void UnlockBucket(const u32 bucketId);

	// PURPOSE
	//  Reserves a bucket, and won't allocate sounds from it, unless they're explicitly requested
	//  Returns the bucket id
	u32  GetNextReservedBucketId()
	{
		const u32 ret =	m_CurrentReservedBucketId;
		TrapGT(ret, m_NumBuckets);
		m_CurrentReservedBucketId++;
		return ret;
	}

	bool IsReservedBucket(const u32 bucketId) const
	{
		return bucketId >= (m_NumBuckets - m_NumBucketsReserved);
	}

	u32 GetNumReservedBuckets() const
	{
		return m_NumBucketsReserved;
	}

	u32 GetNumSoundSlotsPerBucket() const
	{
		return m_NumSoundSlotsPerBucketAligned;
	}

	// PURPOSE
	//	Returns the bucket id with the most space
	u32 GetEmptiestBucketId();
	
	// PURPOSE
	//	Allocates memory for the pool
	// PARAMS
	//	slotSize - the size of the slot, must be large enough to contain the largest object to be allocated
	//	numSlots - the number of slots in each bucket
	//	numBuckets - number of buckets that the slots will be split between
	void Init(u32 soundSlotSize, u32 numSoundSlotsPerBucket, u32 requestedSettingsSlotSize, u32 numRequestedSettingsSlotsPerBucket, u32 numBuckets, u32 numReservedBuckets);

	// PURPOSE
	//	Frees pool memory
	void Shutdown();

	// PURPOSE
	//	Processes all requested child sound allocations for the specified bucket
	void ProcessChildSoundRequests(const audEngineContext *context, const u32 bucketId);

	void *GetFirstBucketPtr() const
	{
		return (void *) &m_Buckets[0];
	}

	void *GetPoolBufferPtr() const
	{
		return m_PoolBuffer;
	}

#if AUD_SOUNDPOOL_LOCK_ON_SPU
	RageCellSyncMutex *GetBucketLocks()
	{
		return m_BucketLocks;
	}
#endif // AUD_SOUNDPOOL_USE_SEMA

#if AUD_SUPPORT_RAVE_EDITING
	audSoundVarianceCache *GetVarianceCache(const u32 bucketId, const void* sound)
	{
		const u32 index = GetSoundSlotIndex(bucketId, sound);
		return &m_VarianceCache[bucketId][index];
	}
#endif // AUD_SUPPORT_RAVE_EDITING

#else 
	bool IsReservedBucket(const u32) const
	{
		return false;
	}

#endif // !__SPU

	class ParentSoundIterator
	{
	public:
		ParentSoundIterator(const u32 numSoundSlotsAligned, const u32 soundSlotSize, audSoundPoolBucket *bucket)
		{
			m_SoundSlotSize = soundSlotSize;
			m_Bucket = bucket;
			m_NumBlocks = numSoundSlotsAligned >> 5;

			// set up for first block
			m_CurrentIndex = 0;
			m_Block = 0;
			m_BitMask = 1;
			m_CurrentParentBlock = m_Bucket->soundSlotIsParent[0];			
		}

		audSound *Next();

	private:

		audSoundPoolBucket *m_Bucket;

		u32 m_SoundSlotSize;
		u32 m_NumBlocks;
		u32 m_BitMask;
		u32 m_Block;
		u32 m_CurrentParentBlock;
		u32 m_CurrentIndex;
	};


	// PURPOSE
	//	Adds a child sound request to the list for the bucket
	void RequestChildSound(const u32 bucketId, const u32 parentIdx, const audMetadataRef offset, const audSoundInternalInitParams &initParams, const u32 childIndex, const audSoundScratchInitParams *scratchInitParams);

	bool IsBucketProcessingChildSoundRequests(u32 bucketId)
	{
#if __SPU
		bucketId = bucketId;
		return g_Bucket->isProcessingChildSoundRequests;
#else
		return m_Buckets[bucketId].isProcessingChildSoundRequests;
#endif
	}
#endif // !AUD_VOICE_SPU

	__inline audVoiceSettings *GetVoiceSettings(const u32 bucketId, const u32 voiceId)
	{
		TrapGT(voiceId, g_audMaxVoicesPerBucket);

#if __SPU
		(void)bucketId;
		audSoundPoolBucket *bucket = g_Bucket;
#else
		audSoundPoolBucket *bucket = &m_Buckets[bucketId];
#endif
		
		Assert(bucket->voiceAllocationState.IsSet(voiceId));
		return &bucket->voiceSettings[voiceId];
	}

	bool IsParentSoundSlot(const u32 bucketId, const u32 slotId);

#ifndef AUD_VOICE_SPU
	u32 GetVoiceId(const u32 bucketId, const audVoiceSettings *voiceSettings);

#if !__SPU

#if !__FINAL
	// PURPOSE
	//	Returns the id of the fullest bucket and the number of slots free in that bucket, and the id of the emptiest
	//	bucket and the number of slots free in that bucket
	void GetPoolUtilisation(audSoundPoolStats &stats) const;
	void PointerRangeTrapCheck(void* p, u32 size);
#else
	static void PointerRangeTrapCheck(void*, u32) { }
#endif//!__FINAL

#if __BANK
	// PURPOSE
	//	Provides index based access to the pool for debugging
	static audSound **sm_SoundPointers;
	static audRequestedSettings **sm_RequestedSettingsPointers;
	// PURPOSE
	//	Prints out the name and state of every allocated sound in the pool
	void DebugDrawBuckets(f32 yScroll, u32 numSoundsPerBucket) const;
	void DebugPrintSoundPool() const;
	void DebugPrintSoundsUsingEnvironmentGroup(const audEnvironmentGroupInterface* group, const u32 slotIndex) const;
	void DebugPrintSoundPoolOldSounds() const;
	void DebugPrintDormantSounds() const;
	void DebugPrintWaitingToBeDeletedSounds() const;
	void DebugDrawSounds(bool drawPositionInfo, 
                         bool drawEntityInfo,
						 bool drawSoundHierarchy, 
						 bool drawSoundPCMCosts, 
						 const char* nameFilter,
                         const char* ignoreNameFilter,
                         const char* tagFilter,
                         const char* ignoreTagFilter,
						 bool parentSoundsOnly, 
						 Vec3V currentPlayerPosition, 
						 f32 viewScroll,
                         f32 yPositionSpread);
	static bool MatchRAGFilter(const char* soundName, const char* soundTag, const char* nameFilter, const char* ignoreNameFilter, const char* tagFilter, const char* ignoreTagFilter);
#endif // __DEV
#endif//__SPU


#if __SPU
	static u8 RegisterEnvironmentSound(const audSoundCombineBuffer &combineBuffer, const u32 soundSlotIndex)
	{
		if(g_Bucket->numEnvironmentSoundRequests < g_audMaxVoicesPerBucket)
		{
			const u32 slot = g_Bucket->numEnvironmentSoundRequests++;
			sysMemCpy(&g_Bucket->environmentSoundRequests[slot], &combineBuffer, sizeof(audSoundCombineBuffer));
			Assign(g_Bucket->environmentSoundRequestSlotIds[slot], soundSlotIndex);
			return slot;
		}
		else
		{
			audErrorf("Failed to register environment sound for post update; already have %u", g_audMaxVoicesPerBucket);
			return 0xff;
		}
	}
#endif // __SPU

	void InvalidateEnvironmentSound(const u32 PS3_ONLY(bucketId), const u32 PS3_ONLY(environmentSlot))
	{
#if __PS3
#if __SPU
		(void)bucketId;
		TrapGE(environmentSlot, (u32)g_Bucket->numEnvironmentSoundRequests);
		g_Bucket->environmentSoundRequestSlotIds[environmentSlot] = 0xff;
#else
		TrapGE(environmentSlot, (u32)m_Buckets[bucketId].numEnvironmentSoundRequests);
		m_Buckets[bucketId].environmentSoundRequestSlotIds[environmentSlot] = 0xff;
#endif
#endif
	}


#if __BANK

	void ComputeSoundSlotAllocationState(const u32 bucketId, atFixedBitSet<g_audMaxSoundSlotsPerBucket> &allocationState) const;
	
#endif


#if AUD_SOUNDPOOL_LOCK_ON_SPU
	RageCellSyncMutex *m_BucketLocks;
#else
	sysCriticalSectionToken *m_BucketLocks;
	sysIpcCurrentThreadId* m_ThreadIds;
#endif

	u32 m_SoundSlotSize;
	u32 m_RequestedSettingsSlotSize;
	u32 m_NumBuckets;
	u32 m_NumSoundSlotsPerBucket;
	u32 m_NumSoundSlotsPerBucketAligned;
	u32 m_NumRequestedSettingsSlotsPerBucket;
	u32 m_NumRequestedSettingsSlotsPerBucketAligned;

	u32 m_NumBucketsReserved;
	u32 m_CurrentReservedBucketId;

	u32 m_SoundToReqSetRatio;

	//HACK_GTA4: for some reason this isn't compiling on SPU at the moment
	//atUserArray<audSoundPoolBucket> m_Buckets;
	audSoundPoolBucket *m_Buckets;
	
#if AUD_SUPPORT_RAVE_EDITING
	atArray< atArray<audSoundVarianceCache> > m_VarianceCache;
#endif
	
	u32 m_PoolBufferSize;
	void *m_PoolBuffer;

	BANK_ONLY(static sysCriticalSectionToken sm_DebugPrintToken);
	NOTFINAL_ONLY(static u32 sm_SkippedChildProcessing);
#endif // AUD_VOICE_SPU
};

}//namespace rage

#endif // AUD_SOUNDPOOL_H
