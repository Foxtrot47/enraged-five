//
// audioengine/soundmanager.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "engine.h"
#include "soundmanager.h"
#include "audiohardware/syncsource.h"
#include "audiosoundtypes/sound.h"

namespace rage
{
#if __SPU
	u32 g_NumSoundsDormant;
	u32 g_NumSoundsPreparing;
	u32 g_NumSoundsPlaying;
	u32 g_NumSoundsWaitingToBeDeleted;
#endif

void audSoundManager::ProcessHierarchy(audEngineContext *context, audSound *sound)
{
	Assert(sound && !sound->_GetParent());

	audSound::SetCurrentSyncMasterId(audMixerSyncManager::InvalidId);
	audSound::SetCurrentSyncSlaveId(audMixerSyncManager::InvalidId);

	if (sound->IsSetToBeDestroyed())
	{
		sound->_ManagedAudioDestroy();
	}

	const u32 timerId = sound->GetTimerId();

	u32 updateTimeInMs = context->timers[timerId].timeInMs;

	// If we've just been asked to pause, pause this sound
	if (context->timers[timerId].isPaused && !context->timers[timerId].wasPausedLastFrame)
	{
		sound->InvokePause(updateTimeInMs);
	}
	
	// Don't do ANYTHING if it's paused. 
	if (!context->timers[timerId].isPaused)
	{
		// See what state it's in
		s32 state = sound->GetPlayState();

		// Quick check to see if the new requested settings has IsSetToPlay set, and set our sound flag if so
		Assert(sound->m_RequestedSettingsIndex!=0xff);
		const tRequestedSettings &settings = sound->GetRequestedSettings()->GetSettings_AudioThread();
		if(settings.IsSetToPlay)
		{
			sound->SetToPlay();
		}		
		if(settings.IsSetToStop)
		{
			sound->_ActionStopRequest();
		}

		switch(state)
		{
		case AUD_SOUND_DORMANT:
#if !__SPU
			g_AudioEngine.GetSoundManager().m_NumSoundsDormant++;
#else
			g_NumSoundsDormant++;
#endif

			if(sound->GetRequestedSettings()->IsPrepareRequested())
			{
				audPrepareState prepareState = sound->_ManagedAudioPrepareFromSoundManager();
				if(prepareState == AUD_PREPARED && sound->IsSetToPlay())
				{
					audSoundCombineBuffer combineBuffer;
					sound->_ManagedAudioPlay(updateTimeInMs, combineBuffer);
				}
			}
			else if (sound->IsSetToPlay())
			{
				// start it off
				audSoundCombineBuffer combineBuffer;
				sound->_ManagedAudioPlay(updateTimeInMs, combineBuffer);
			}
			break;

		case AUD_SOUND_PREPARING:
#if !__SPU
			g_AudioEngine.GetSoundManager().m_NumSoundsPreparing++;
#else
			g_NumSoundsPreparing++;
#endif

			// We're currently preparing
			switch(sound->_ManagedAudioPrepareFromSoundManager())
			{
				case AUD_PREPARED:
				
					if (sound->IsSetToPlay())
					{
						// start it off
						audSoundCombineBuffer combineBuffer;
						//@@: location AUDSOUNDMANAGER_PROCESSHEIRARCHY_AUDIO_PREPARED
						sound->_ManagedAudioPlay(updateTimeInMs, combineBuffer);
					}
					break;

				case AUD_PREPARING:
				
					// See if we've passed our time limit
					// Note that all prepare times are treated as real times, not adjusted for pausing.
					if(context->systemTimeMs > sound->GetPrepareTimeLimit())
					{
						// Taken too long to Prepare(), so kill the sound
#if __DEV
						audWarningf("Sound %s didn't prepare in the time limit", sound->GetName());
#endif
						sound->_ManagedAudioDestroy();
					}
					break;

				case AUD_PREPARE_FAILED:
					sound->_ManagedAudioDestroy();
					break;

				}// switch(prepare state)
			break;
		
		case AUD_SOUND_PLAYING:
#if !__SPU
			g_AudioEngine.GetSoundManager().m_NumSoundsPlaying++;
#else
			g_NumSoundsPlaying++;
#endif

			if (sound->IsSetToBeKilled())
			{
				sound->_ManagedAudioKill();
			}
			else
			{
				audSoundCombineBuffer combineBuffer;
				sound->_ManagedAudioUpdate(updateTimeInMs, combineBuffer);
			}
			break;

		case AUD_SOUND_WAITING_TO_BE_DELETED:
#if !__SPU
			g_AudioEngine.GetSoundManager().m_NumSoundsWaitingToBeDeleted++;
#else
			g_NumSoundsWaitingToBeDeleted++;
#endif

			// having this case inside the switch does mean that we don't take into account playstate changes
			// during this audio frame.  That's not a big problem however, it just means we'll leave a sound waiting
			// to be deleted until the next audio frame.
				if(!sound->IsReferencedByGame())
				{
					// We're free to delete it, as nothing cares about it any more.
					delete sound;
				}
			break;

		} // switch(Playstate)
	}
	else
	{
		// Allow paused sounds in a preparing state to continue preparing
		switch(sound->GetPlayState())
		{

		case AUD_SOUND_DORMANT:
			if(sound->GetRequestedSettings()->IsPrepareRequested())
			{
				sound->_ManagedAudioPrepareFromSoundManager();
			}		
		break;

		case AUD_SOUND_PREPARING:
		{
			sound->_ManagedAudioPrepareFromSoundManager();
			// Don't change the sound's play state while paused, but this will allow Prepare to return something other than Preparing
		}
		break;

		default:
			break;
		}
	}

}

f32* audSoundManager::GetVariableAddress(const u32 nameHash)
{
	// Loop through our own variable block, to see if it's one of ours
	u32 i;
	for (i=0; i<g_MaxSoundManagerVariables; i++)
	{
		if (m_Variables[i].hash == nameHash || m_VariableAliases[i] == nameHash)
		{
			// We own the variable it's asking about, so return its address
			return &(m_Variables[i].value);
		}
		else if (m_Variables[i].hash == 0)
		{
			return NULL;
		}
	}
	return NULL;
}

f32* audSoundManager::GetVariableAddress(const char *name)
{
	return GetVariableAddress(atStringHash(name));
}


}
