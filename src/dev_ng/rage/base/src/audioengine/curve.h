//
// audioengine/curve.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_CURVE_H
#define AUD_CURVE_H

#include "curvedefs.h"
#include "math/amath.h"
#include "audiohardware/driverdefs.h"
#include "audioengine/widget_verify.h"

namespace rage {

typedef f32 (*curveValueFunction)( f32, void* );


// PURPOSE
//  Curves take a single input value, and transform it into an output value, according to an algorithm defined
//  in code, and a set of metadata defined using RAVE.
//  On initialisation, curves look up their metadata and function pointer, and cache these for speedy access. To
//  allow live auditioning from RAVE, these are looked up on every CalculateValue() call in debug builds.
//  There is a single audCurve class, and the various algorithms are defined statically in this class. Because of this,
//  a curve member variable may be included in code without knowing the RAVE defined type of the curve, as this does not
//  effect the instantiated class, just the metadata. A downside of this is that curve types may not define additional 
//  member variables, and hence are stateless.
//  All curves transform an f32 into another f32.
class audCurve
{
public:
	audCurve();
	~audCurve(){};
#if !__SPU
	// PURPOSE
	//  Initialises the curve, caching (in non-debug builds) ptrs to its metadata.
	// PARAMS
	//  Overloaded versions, using one of:
	//  curveName - string containing the name of the curve
	//  curveNameHash - a hash of the curve name
	//  curveNameHashPtr - a ptr to a hash of the curve name - I remember adding this to allow for more reliable
	//                   - auditioning, but I forget why it's necessary!
	// RETURNS
	//  True on successful initialisation, false otherwise. Initialisation is successful if the CurveRepository
	//  can find the named curve.
	bool Init(const char* curveName);
	bool Init(u32 curveNameHash);
	bool Init(u32* curveNameHashPtr);
#endif
	// PURPOSE
	//  Calculates the output value of the curve, based on its metadata, curve-type, and input value
	// PARAMS
	//  input - the input value, to be transformed
	// RETURNS
	//  The transformed value.
	F32_VerifyFinite CalculateValue(F32_VerifyFinite input) const;
	// PURPOSE
	//  Rescales the input value, transforms it according to the curve metadata and type, and then rescales the output.
	//  Often, it is convenient to define curves over an input and output range of 0-1. Should the usage need another 
	//  range, this function allows the same 0-1 curve to be used.
	// PARAMS
	//  minOutput - the value returned for a curve output of 0
	//  maxOutput - the value returned for a curve output of 1
	//  minInput  - the input value that will be rescaled to 0 before being passed into the curve
	//  maxInput  - the input value that will be rescaled to 1 before being passed into the curve
	//  input     - the value to be transformed. It is first rescaled, and then passed into the curve
	// RETURNS
	//  The output of the curve, rescaled according to min/maxOutput
	F32_VerifyFinite CalculateRescaledValue(F32_VerifyFinite minOutput, F32_VerifyFinite maxOutput, F32_VerifyFinite minInput, F32_VerifyFinite maxInput, F32_VerifyFinite input) const;
	// PURPOSE
	//  The following static functions implement the available curve types, and will be called by CalculateValue().
	//  A straight-line curve.
	F32_VerifyFinite Linear_CalculateValue(F32_VerifyFinite input) const;
	bool Linear_Init();
	//  A straight-line curve, whose output is then converted into dBs.
	F32_VerifyFinite LinearDb_CalculateValue(F32_VerifyFinite input) const;
	bool LinearDb_Init();
	//  A constant value.
	F32_VerifyFinite Constant_CalculateValue(F32_VerifyFinite input) const;
	bool Constant_Init();
	//  A set of multiple straight lines, that do not overlap, and touch the next line at their end point.
	//  By having more and more, smaller, linear sections, any curve shape may be approximated.
	F32_VerifyFinite PiecewiseLinear_CalculateValue(F32_VerifyFinite input) const;
	bool PiecewiseLinear_Init();
	//  Essentially a sqrt curve over the range 0-1, with metadata determining x and y axis flipping. Useful
	//  for maintaining equal power when applied to the linear volume of two crossfading sounds.
	F32_VerifyFinite EqualPower_CalculateValue(F32_VerifyFinite input) const;
	bool EqualPower_Init();
	//  A set of output values for integer inputs from 0 upwards. No interpolation is performed.
	F32_VerifyFinite ValueTable_CalculateValue(F32_VerifyFinite input) const;
	bool ValueTable_Init();
	//  y = x^n where n < 1 = inverse exponential (convex), n = 1 = linear, n > 1 = exponential (concave)
	F32_VerifyFinite Exponential_CalculateValue(F32_VerifyFinite input) const;
	bool Exponential_Init();
	//  e^(-Ax) - with a vertical shear, to make it zero at a given (1, doubtless!) horizontal value
	F32_VerifyFinite DecayingExponential_CalculateValue(F32_VerifyFinite input) const;
	bool DecayingExponential_Init();
	//  e^(-A*x^2) - with a vertical shear, to make it zero at a given (1, doubtless!) horizontal value
	F32_VerifyFinite DecayingSquaredExponential_CalculateValue(F32_VerifyFinite input) const;
	bool DecayingSquaredExponential_Init();
	//  A*Sin(B+C*x)+D   - haven't done a Cosine, just offset this one
	F32_VerifyFinite SineCurve_CalculateValue(F32_VerifyFinite input) const;
	bool SineCurve_Init();
	//  1/(A*x+B) with a vertical shear to to make it zero at a given horizontal value
	F32_VerifyFinite OneOverX_CalculateValue(F32_VerifyFinite input) const;
	bool OneOverX_Init();
	//  1/(A*x^2+B) with a vertical shear to to make it zero at a given horizontal value
	F32_VerifyFinite OneOverXSquared_CalculateValue(F32_VerifyFinite input) const;
	bool OneOverXSquared_Init();
	//  A totally hardcoded poly approximation of the default distance attenuation curve.
	static F32_VerifyFinite DefaultDistanceAttenuation_CalculateValue(F32_VerifyFinite const input);

	bool DefaultDistanceAttenuation_Init();
	
	//	Will clamp the output volume of the default distance attenuation result to a specified value and optionally rescale the output
	F32_VerifyFinite DefaultDistanceAttenuationClamped_CalculateValue(F32_VerifyFinite input) const;
	bool DefaultDistanceAttenuationClamped_Init();

	F32_VerifyFinite GetMetadataMinInput() const;
	F32_VerifyFinite GetMetadataMaxInput() const;

	bool ShouldClampInput() const
	{
		return m_ShouldClampInput;
	}

	bool IsValidInSoundTask() const
	{
		return m_IsValidInSoundTask;
	}

	bool IsValid() const 
	{
		return m_Metadata != NULL;
	}

#if __BANK
	const char *GetName() const;
#endif

	u32 GetType() const
	{
		return m_CurveType;
	}

private:

	inline static void ComputeLine(const float x1, const float y1, const float x2, const float y2,
								float &c0, float &c1, float &c2, float &c3)
	{
		// find gradient and intercept: m = (y' - y) / (x' - x)
		float m = (y2 - y1) / (x2- x1);
		// when x1 == x2 set m to 0
		m = Selectf((x2-x1) - SMALL_FLOAT, m, 0.f);
		// b = y - mx
		const float b = y2 - m*x2;
		c0 = m;
		c1 = b;
		c2 = x1;
		c3 = x2;
	}

	BaseCurve *m_Metadata;

	f32 m_Coefficients[6];
	f32 m_MinInput;
	f32 m_MaxInput;
	u8	m_CurveType;
	bool m_ShouldClampInput;
	bool m_IsValidInSoundTask;

#if __BANK
	u32 m_NameHash;
#endif
};


class audThreePointPiecewiseLinearCurve
{
public:
	audThreePointPiecewiseLinearCurve(const f32 x0, const f32 y0, const f32 x1, const f32 y1, const f32 x2, const f32 y2)
	{
		Init(x0, y0, x1, y1, x2, y2);
	}

	audThreePointPiecewiseLinearCurve()
		: in0(0.f)
		, out0(0.f)
		, in1(0.f)
		, out1(0.f)
		, in2(0.f)
		, out2(0.f)
		, ratio1(0.f)
		, ratio2(0.f)
	{

	}

	void Init(const f32 x0, const f32 y0, const f32 x1, const f32 y1, const f32 x2, const f32 y2)
	{
		in0 = x0;
		out0 = y0;
		in1 = x1;
		out1 = y1;
		in2 = x2;
		out2 = y2;
		ratio1 = 1.f / (in1 - in0);
		ratio2 = 1.f / (in2 - in1);
	}

	F32_VerifyFinite CalculateValue(const F32_VerifyFinite input) const
	{
		// Calculate our output for each of the three sections, and then fsel all the options.
		const f32 region0Ratio = (input-in0) * ratio1;
		const f32 region0Output = out0 + (out1-out0)*region0Ratio;
		const f32 region1Ratio = (input-in1) * ratio2;
		const f32 region1Output = out1 + (out2-out1)*region1Ratio;
		f32 output = Selectf(input-in0, region0Output, out0);
		output     = Selectf(input-in1, region1Output, output);
		output     = Selectf(input-in2, out2, output);
		return output;
	}
private:
	f32 in0;
	f32 out0;
	f32 ratio1;
	f32 in1; 
	f32 out1;
	f32 ratio2;
	f32 in2; 
	f32 out2;
};

} // namespace rage

#endif //AUD_CURVE_H
