//
// audioengine/metadataref.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_METADATAREF_H
#define AUD_METADATAREF_H

namespace rage
{

class audMetadataRef
{
	friend class audMetadataManager;
public:

#if !__SPU
	audMetadataRef()
		: m_MetadataRef(~0U)
	{

	}
#endif

	bool operator==(const audMetadataRef rhs) const
	{
		return rhs.m_MetadataRef == m_MetadataRef;
	}

	bool operator!=(const audMetadataRef rhs) const
	{
		return rhs.m_MetadataRef != m_MetadataRef;
	}

	void SetInvalid() { m_MetadataRef = ~0U; }
	
	bool IsValid() const 
	{ 
		// When running with RAVE metadata refs are hashes; 0 is the null hash value
		// Otherwise hashes are resolved to metadata offsets, and ~0U signifies an invalid offset.
		return (m_MetadataRef != ~0U && m_MetadataRef != 0);
	}

	static audMetadataRef Create(const u32 refVal)
	{
		audMetadataRef metadataRef;
		metadataRef.Set(refVal);
		return metadataRef;
	}

	u32 Get() const { return m_MetadataRef; }

#if !__SPU
private:
#endif

	void Set(const u32 refVal) { m_MetadataRef = refVal; }
	u32 m_MetadataRef;
};

} // namespace rage
#endif // AUD_METADATAREF_H
