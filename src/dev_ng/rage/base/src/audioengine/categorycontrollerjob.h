// 
// audioengine/categorycontrollerjob.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_CATEGORYCONTROLLERJOB_H
#define AUD_CATEGORYCONTROLLERJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(CategoryControllerJob);

#endif // AUD_CATEGORYCONTROLLERJOB_H
