//
// audioengine/soundfactory.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SOUNDFACTORY_H
#define AUD_SOUNDFACTORY_H

#include "metadatamanager.h"
#include "audiosoundtypes/sound.h"

namespace rage {

class audEnvironmentSound;

AUD_DECLARE_SOUNDDEFS_FUNCTION(AudioUpdate);
AUD_DECLARE_SOUNDDEFS_FUNCTION(AudioPlay);
AUD_DECLARE_SOUNDDEFS_FUNCTION(AudioKill);
AUD_DECLARE_SOUNDDEFS_FUNCTION(AudioPrepare);
AUD_DECLARE_SOUNDDEFS_FUNCTION(Pause);
AUD_DECLARE_SOUNDDEFS_FUNCTION(ComputeDurationMsExcludingStartOffsetAndPredelay);
AUD_DECLARE_SOUNDDEFS_FUNCTION(Destruct);
AUD_DECLARE_SOUNDDEFS_FUNCTION(ActionReleaseRequest);

struct Sound;

// PURPOSE
//	This class implements a sound creation factory.  It uses the sound metadata to determine what sound type to instantiate.
class audSoundFactory : audObjectModifiedInterface
{
public:
	audSoundFactory();
	~audSoundFactory();

#if !__SPU
	
	// PURPOSE
	//	Returns an instantiated sound object from the supplied soundName.  
	// PARAMS
	//	soundNameHash - hashed name of the sound
	//  initParams - a structure containing runtime initialisation parameters. See soundcontrol.h for details.
	// RETURNS
	//	Pointer to the newly instantiated sound instance, NULL if not found
	audSound *GetInstance(const u32 soundNameHash, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams, audSoundParentInitParams *parentInitParams);

	// PURPOSE
	//	Returns an instantiated sound object from the supplied metadata reference
	// PARAMS
	//	metadataRef - metadata reference
	//  initParams - a structure containing runtime initialisation parameters. See soundcontrol.h for details.
	// RETURNS
	//	Pointer to the newly instantiated sound instance, NULL if not found
	audSound *GetInstance(const audMetadataRef metadataRef, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams, audSoundParentInitParams *parentInitParams);

	// PURPOSE
	//	Returns an instantiated sound object
	// PARAMS
	//	metadataOffset - the offset into metadata that contains the requested
	//					 sound's data.
	//	parent	-	the parent that is creating this sound
	//  initParams - a structure containing runtime initialisation parameters. See soundcontrol.h for details.
	// NOTES
	//	Sound references are stored in metadata as offsets, therefore sound types
	//	use this function to instantiate their child sounds.
	// SEE ALSO
	//	GetInstanceFromMetadata
	audSound *GetChildInstance(const audMetadataRef metadataOffset, audSound *parent, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams, bool lockBucket);

	// PURPOSE
	//	Returns an instantiated sound object from the supplied soundNameHash, with parent.  
	// PARAMS
	//	soundName - hashed string containing the unique name of the sound
	//	parent	-	the parent that is creating this sound
	//  initParams - a structure containing runtime initialisation parameters. See soundcontrol.h for details.
	// RETURNS
	//	Pointer to the newly instantiated sound instance, NULL if not found
	// SEE ALSO
	//	GetInstanceFromMetadata
	audSound *GetChildInstance(u32 soundNameHash, audSound *parent, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams, bool lockBucket);

	// PURPOSE
	//	Returns an audEnvironmentSound
	audEnvironmentSound *GetEnvironmentSound(audSound *parent, const audSoundInternalInitParams *initParams, audSoundScratchInitParams *scratchInitParams) const;

	// PURPOSE
	//  Helper class for calling a function on all sounds encountered by ProcessHierarchy
	class audSoundProcessHierarchyFn 
	{
	public:
		virtual ~audSoundProcessHierarchyFn() {};

		// u32 is the sound's ClassID, void* is the actual decompressed sound metadata
		virtual void operator()(u32, const void*) {}
	};

	// PURPOSE
	//  For a given sound, recursively call the provided function on all child sounds
	void ProcessHierarchy(audMetadataRef metadataRef, audSoundProcessHierarchyFn& fn) const;
	void ProcessHierarchy(const u32 soundNameHash, audSoundProcessHierarchyFn& fn) const;

	// PURPOSE
	//	Initializes the metadata manager by loading sounds.dat into memory and building lookup structures
	// RETURNS
	//	True if successful, false otherwise
	bool Init();

	// PURPOSE
	//	Frees all sound metadata memory
	void Shutdown();

	// PURPOSE
	//	Loads an additional metadata file
	bool LoadMetadataChunk(const char *chunkName, const char *path)
	{
		return m_MetadataMgr.LoadMetadataChunk(chunkName, path);
	}

	bool UnloadMetadataChunk(const char *chunkName)
	{
		return m_MetadataMgr.UnloadMetadataChunk(chunkName);
	}

	bool LoadExtraBankNames(const char *sourceName, fiStream *stream, const s32 size)
	{
		return m_MetadataMgr.LoadExtraStringTable(sourceName, stream, size);
	}

	void AddBankRemapping(u32 originalBankNameHash, u32 newBankNameHash);
	void RemoveBankRemapping(u32 originalBankNameHash);

	// PURPOSE
	//	Looks up a bank name from the supplied index
	// PARAMS
	//	bankNameIndex - index within the bank name table for the required bank
	// RETURNS
	//	pointer to NULL terminated string containing the name of the bank
	// NOTES
	//	Wave references are stored in metadata as a bankNameIndex and a WaveNameHash,
	//	this function allows sounds that are loading banks to turn their bankNameIndex
	//	into a string bank name
	const char *GetBankNameFromIndex(const u32 bankNameIndex) const;

	// PURPOSE
	//	Looks up a bank index from the specified name
	u32 GetBankIndexFromName(const char *bankName) const;
	u32 GetBankIndexFromName(const u32 bankNameHash) const;

	// PURPOSE
	//	Returns a bank index from the specified reference id
	// NOTES
	//	When running -rave, the parameter is interpreted as a hash, otherwise its treated as an index
	u32 GetBankIndexFromMetadataRef(const u32 ref) const;

	// PURPOSE
	//	Returns a bank index from the specified reference hash name
	// NOTES
	//	Use this for when you know its a hash value
	u32 GetBankNameIndexFromHash(const u32 hash) const
	{
		return m_MetadataMgr.GetStringIdFromHash(hash);
	}

	const audMetadataManager &GetMetadataManager() const { return m_MetadataMgr; }
	audMetadataManager &GetMetadataManager() { return m_MetadataMgr; }

	const Sound *GetMetadataPtr(const u32 nameHash) const;
	const Sound *GetMetadataPtr(const audMetadataRef metadataRef) const;

	template<class _T> const _T *DecompressMetadata(const u32 nameHash) const
	{
		Sound uncompressedSound;
		return audSound::DecompressMetadata<_T>(GetMetadataPtr(nameHash), uncompressedSound);
	}

	template<class _T> const _T *DecompressMetadata(const audMetadataRef metadataRef) const
	{
		Sound uncompressedSound;
		return DecompressMetadata<_T>(metadataRef, uncompressedSound);
	}

	template<class _T> const _T *DecompressMetadata(const u32 nameHash, Sound &uncompressedSound) const
	{
		return audSound::DecompressMetadata<_T>(GetMetadataPtr(nameHash), uncompressedSound);
	}
	template<class _T> const _T *DecompressMetadata(const audMetadataRef metadataRef, Sound &uncompressedSound) const
	{
		return audSound::DecompressMetadata<_T>(GetMetadataPtr(metadataRef), uncompressedSound);
	}

	const char *GetTypeName(const u32 typeId) const;
#if __BANK
	void DrawOverriddenSounds() const
	{
		m_MetadataMgr.DrawOverriddenObjects();
	}
#endif // __BANK
	

#endif

	static u32 GetMaxSoundTypeSize();

#if !__SPU && __BANK
	// audObjectModifiedInterface
	void OnObjectAuditionStart(const u32 soundNameHash)
	{
		m_AuditionStartSoundHash = soundNameHash;
		sys_lwsync();
		m_HadAuditionStart = true;
	}

	void OnObjectAuditionStop(const u32 soundNameHash)
	{
		m_AuditionStopSoundHash = soundNameHash;
		sys_lwsync();
		m_HadAuditionStop = true;
	}

	void OnObjectModified(const u32 nameHash)
	{
		if(m_EditedSoundList.GetCount() < m_EditedSoundList.GetMaxCount())
		{
			m_EditedSoundList.Append() = nameHash;
		}
	}
	void OnObjectOverridden(const u32 nameHash)
	{
		if(m_EditedSoundList.GetCount() < m_EditedSoundList.GetMaxCount())
		{
			m_EditedSoundList.Append() = nameHash;
		}
	}

	u32 GetNumEditedSounds() const { return m_EditedSoundList.GetCount(); }
	u32 GetEditedSoundHash(const u32 index) { return m_EditedSoundList[index]; }
	void ResetEditedSounds() { m_EditedSoundList.Reset(); }
	bool IsSoundNameInEditedList(const u32 nameHash) { return (m_EditedSoundList.Find(nameHash) != -1); }

	bool GetAuditionStartSound(u32 &nameHash)
	{
		if(m_HadAuditionStart)
		{
			nameHash = m_AuditionStartSoundHash;
			m_HadAuditionStart = false;
			sys_lwsync();
			return true;
		}
		return false;
	}

	bool GetAuditionStopSound(u32 &nameHash)
	{
		if(m_HadAuditionStop)
		{
			nameHash = m_AuditionStopSoundHash;
			m_HadAuditionStop = false;
			sys_lwsync();
			return true;
		}
		return false;
	}
#endif
#if __BANK
	void CountSoundsCreated()
	{
		sm_NumSoundsCreated = 0;
		sm_NumParentSoundsCreated = 0;
	}
	void LogSoundsCreated()
	{
		Displayf("----------------------Sounds created ------------------");
		Displayf("Total parent sounds (game created) : %u",sm_NumParentSoundsCreated);
		Displayf("Total sounds : %u",sm_NumSoundsCreated);
	}
#endif

private:

	// PURPOSE
	//	Creates a new sound instance from the supplied metadata
	//	PARAMS
	//	metadata - pointer to the metadata containing this sounds properties
	//	parent - the owning sound if present
	// RETURNS
	//	pointer to the newly instantiated sound, NULL if the sound couldn't be instantiated
	// NOTES
	//	This function can fail for a variety of reasons including invalid metadata sound type and
	//	the sound pool being full
	audSound *GetInstanceFromMetadata(const void *metadata, audSound *parent, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams, audSoundParentInitParams *parentInitParams, bool lockBucket);
	
	audMetadataManager	m_MetadataMgr;
	atMap<u32, u32> m_BankRemappings;

#if __BANK
	enum {kMaxSoundEdits = 128};
	atFixedArray<u32, kMaxSoundEdits> m_EditedSoundList;
	u32 m_AuditionStartSoundHash;
	u32 m_AuditionStopSoundHash;
	bool m_HadAuditionStart;
	bool m_HadAuditionStop;

	static u32 sm_NumParentSoundsCreated;
	static u32 sm_NumSoundsCreated;

	struct BucketSoundCount
	{
		BucketSoundCount() 
			: NumSoundsCreatedInHierarchy(0)
			, NumSoundSlotsFreeForHierarchy(0)
			, PeakSoundSlotsForHierarchy(0)
		{

		}
		u32 NumSoundsCreatedInHierarchy;
		u32 NumSoundSlotsFreeForHierarchy;
		u32 PeakSoundSlotsForHierarchy;
	};
	static atRangeArray<BucketSoundCount, 32> sm_BucketSoundCount;



#endif
};
#if !__SPU
#define SOUNDFACTORY g_AudioEngine.GetSoundManager().GetFactory()

#endif // !__SPU

} // namespace rage


#endif //AUD_SOUNDFACTORY_H
