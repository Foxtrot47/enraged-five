// 
// audioengine/categorycontrollermgr.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_CATEGORYCONTROLLERMGR_H
#define AUD_CATEGORYCONTROLLERMGR_H

//////////////////////////////////////////////////////////////////////////
//	Includes
#include "categorycontroller.h"
#if __PS3
#include "categorycontrollerjob.h"
#endif
#include "atl/array.h"
#include "diag/trap.h"
#include "system/criticalsection.h"
//////////////////////////////////////////////////////////////////////////


namespace rage {



//////////////////////////////////////////////////////////////////////////
//	Forward Declarations
class audCategory;
class bkBank;

//////////////////////////////////////////////////////////////////////////

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to __declspec(align)
#endif // __WIN32



class audCategoryControllerPackage
{
	friend class audCategoryControllerManager;
public:
	audCategoryControllerPackage();
	~audCategoryControllerPackage();
	bool QueueCreateControllers(audCategoryController** pControllerArray, const u32* hashCategoryNames, u8 numControllers, u8 strideBytes);
	bool QueueDestroyControllers();
	u32 GetNumControllers() const { return m_uNumControllers; }
	audCategoryController* GetController(u32 index) { TrapZ((u32)m_bControllersCreated); TrapGE(index, GetNumControllers()); return m_pControllerArray[index]; }
	bool GetControllersCreated() const { return m_bControllersCreated; }
	bool GetIsSafeToDestruct() const { return m_uNumControllers==0; }
	bool GetErrorCreatingControllers() const { return m_bErrorCreatingControllers; }

private:
	audCategoryController** m_pControllerArray;
	const u32* m_hashCategoryNames;
	u8 m_uNumControllers;
	u8 m_uCatNameStrideBytes;
	bool m_bIsCreatingControllers:1;
	bool m_bControllersCreated:1;
	bool m_bIsDeletingControllers:1;
	bool m_bErrorCreatingControllers:1;
};



/*************************************************************************************
Class:		audCategoryControllerManager
Purpose:	Manages our category controllers. Call CreateController() to create one.
*************************************************************************************/
class audCategoryControllerManager
{
	audCategoryControllerManager();
	audCategoryControllerManager(const audCategoryControllerManager&);
	audCategoryControllerManager& operator=(const audCategoryControllerManager&);
	~audCategoryControllerManager();

public:
	static audCategoryControllerManager* Instantiate();
	static void Destroy();

	static audCategoryControllerManager* InstancePtr()	{ FastAssert(sm_pInstance);	return  sm_pInstance; }
	static audCategoryControllerManager& InstanceRef()	{ FastAssert(sm_pInstance);	return *sm_pInstance; }

	/*************************************************************************************
	Function:	CreateController
	Purpose:	Creates a rage_new category controller
	In:			szCategoryName:	NULL terminated string for the category name
	Return:		A pointer to our rage_new controller, NULL if not successful
	*************************************************************************************/
	audCategoryController* CreateController(const char* szCategoryName);

	/*************************************************************************************
	Function:	CreateDynamicController
	Purpose:	Creates a rage_new category controller
In:				heirachyIndex:		indext to the root category of the heirachy
				hashCategoryName:	Hashed name of the category
				isSceneController:	Debugging for whether a dynamic mix scene uses this controller
	Return:		A pointer to our rage_new controller, NULL if not successful
	*************************************************************************************/
	audCategoryController* CreateDynamicController(s32 heirachyIndex, u32 hashCategoryName, bool isSceneController = false);

	/*************************************************************************************
	Function:	CreateController
	Purpose:	Creates a rage_new category controller for a dynamic category
	In:			hashCategoryName:	Hashed name of the category
				isSceneController:	Debugging for whether a dynamic mix scene uses this controller
				deferSort: If true then the controller list won't be sorted, used to speed up multiple controller creation
	Return:		A pointer to our rage_new controller, NULL if not successful
	*************************************************************************************/
	audCategoryController* CreateController(const u32 hashCategoryName, bool isSceneController = false);

	/*************************************************************************************
	Function:	CreateMultipleControllers
	Purpose:	Creates a bunch category controllers inside an arbitrarily sized structure
				This can be much  more efficient especially on PS3 because it only creates 
				one lock for the whole pass instead of one for each controller
	In:			pOutControllers:	Array of controller pointers to fill
				hashCategoryNames:	Hashed names of the categories
				numControllers:		Size of the array to fill in
				strideBytes:		If hashCategoryNames resides in an array of
									arbitrarily sized structures this should be the size
									of one structure in bytes.
				isSceneController:	Debugging for whether a dynamic mix scene uses this controller
	Return:		true for success, false for failure. Failure could be undefined
	*************************************************************************************/
	bool CreateMultipleControllers(audCategoryController** pOutControllers, const u32* hashCategoryNames, u32 numControllers, u32 strideBytes, bool isSceneController = false);

	/*************************************************************************************
	Function:	CreateMultipleDynamicControllers
	Purpose:	Creates a bunch dynamic-category controllers inside an arbitrarily sized structure
				This can bemuch  more efficient especially on PS3 because it only creates 
				one lock for the whole pass instread of one for each controller
In:				heirachyIndex:		index of the root category for the heirachy
				pOutControllers:	Array of controller pointers to fill
				hashCategoryNames:	Hashed names of the categories
				numControllers:		Size of the array to fill in
				strideBytes:		If hashCategoryNames resides in an array of
									arbitrarily sized structures this should be the size
									of one structure in bytes.
				isSceneController:	Debugging for whether a dynamic mix scene uses this controller
	Return:		true for success, false for failure. Failure could be undefined
	*************************************************************************************/
	bool CreateMultipleDynamicControllers(s32 heirachyIndex, audCategoryController** pOutControllers, const u32* hashCategoryName, u32 numControllers, u32 strideBytes, bool isSceneController = false);

	/*************************************************************************************
	Function:	DestroyController
	Purpose:	Deletes a registered category controller
	In:			pController:	pointer to the controller
	*************************************************************************************/
	void DestroyController(audCategoryController* pController);

	/*************************************************************************************
	Function:	DestroyMultipleControllers
	Purpose:	Deletes multiple controllers in one pass. Use this where possible for speed.
	In:			pControllerArray:	pointer to the controller array
				numControllers:		The number of controllers to destroy
	*************************************************************************************/
	void DestroyMultipleControllers(audCategoryController** pControllerArray, const u32 numControllers);

	bool AddPackageToCreateList(audCategoryControllerPackage* pPackage);
	bool AddPackageToDeleteList(audCategoryControllerPackage* pPackage);

	/*************************************************************************************
	Function:	Update
	Purpose:	This is be called once per game frame
	*************************************************************************************/
	void Update();

#if __BANK
	void AddWidgets(bkBank& bank);
	void SetWidgetActiveControls(bkGroup* pGroup) { m_pWidgetActiveControls = pGroup; }
	void RecurseAddCategoryWidget(bkBank& bank, audCategory* pCategory);
	//void MuteAllExcept(const audCategory* pCurrCategory, const audCategory* pMuteExcept, bool& bUnmuteParent);
	//void UnmuteAll();
	static void PrintControllers() { sm_PrintControllers = true; }
#endif

	audCategoryController* GetControllerPool()
	{
		return reinterpret_cast<audCategoryController*>(m_Pools.m_ControllerPool);
	}
	const audCategoryController* GetControllerPool() const
	{
		return reinterpret_cast<const audCategoryController*>(m_Pools.m_ControllerPool);
	}
	audCategoryControllerSettings* GetControllerSettingsPool()
	{
		return reinterpret_cast<audCategoryControllerSettings*>(m_Pools.m_ControllerSettingsPool);
	}
	const audCategoryControllerSettings* GetControllerSettingsPool() const
	{
		return reinterpret_cast<const audCategoryControllerSettings*>(m_Pools.m_ControllerSettingsPool);
	}
	
	//	Controllers
	audCategoryController &GetControllerRefFromId(const s32 id)
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return GetControllerPool()[id];
	}
	const audCategoryController &GetControllerRefFromId(const s32 id) const
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return GetControllerPool()[id];
	}
	audCategoryController *GetControllerPtrFromId(const s32 id)
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return &GetControllerPool()[id];
	}
	const audCategoryController *GetControllerPtrFromId(const s32 id) const
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return &GetControllerPool()[id];
	}

	//	Settings
	audCategoryControllerSettings &GetControllerSettingsRefFromId(const s32 id)
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return GetControllerSettingsPool()[id];
	}
	const audCategoryControllerSettings &GetControllerSettingsRefFromId(const s32 id) const
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return GetControllerSettingsPool()[id];
	}
	audCategoryControllerSettings *GetControllerSettingsPtrFromId(const s32 id)
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return &GetControllerSettingsPool()[id];
	}
	const audCategoryControllerSettings *GetControllerSettingsPtrFromId(const s32 id) const
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return &GetControllerSettingsPool()[id];
	}
	
	audCategoryController* GetListHeadFromCategoryIndex(const s32 id)
	{
		TrapGE((u32)id, g_MaxCategoryControllers);
		TrapLT(id, 0);
		return m_ControllerListIndices[id] != -1 ? GetControllerPtrFromId(m_ControllerListIndices[id]) : NULL;
	}

private:
	void ProcessPackageCreationAndDeletion();

private:
	audCategoryControllerPools		m_Pools;
	atRangeArray<u32,g_MaxCategoryControllers> m_FreeIndexStack;
	u32								m_NumControllers;	
	
	atRangeArray<audCategoryControllerPackage*,128> m_PendingCreatePackages;
	atRangeArray<audCategoryControllerPackage*,128> m_PendingDeletePackages;
	u32								m_NumPendingCreatePackages;	
	u32								m_NumPendingDeletePackages;	

	sysCriticalSectionToken			m_ControllerPoolLock;
	sysCriticalSectionToken			m_ControllerPackageLock;

#if __PS3
	audCategoryControllerOutput*	m_pControllerOutputArray;
	sysTaskHandle					m_categoryUpdateHandle;
#endif

	//	The size of this will match the number of categories
	s16*							m_ControllerListIndices;
	u32								m_uControllerListSizeBytesAligned;
	u32								m_NumCategories;

#if __BANK
	bkGroup*						m_pWidgetActiveControls;
	static	bool sm_PrintControllers;
#endif
	
	//	Statics
	static audCategoryControllerManager*	sm_pInstance;
};
}//namespace rage


#define	AUDCATEGORYCONTROLLERMANAGER	    (rage::audCategoryControllerManager::InstanceRef())

#if __WIN32
#pragma warning(pop)
#endif // __WIN32



#endif	//	AUD_CATEGORYCONTROLLERMGR_H

