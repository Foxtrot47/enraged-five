//
// audioengine/soundset.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SOUNDSET_H
#define AUD_SOUNDSET_H

#include "audioengine/entity.h"

namespace rage
{

struct SoundSet;

class audSoundSet
{
public:
	audSoundSet()
		: m_Data(NULL)
		, m_NameHash(0)
	{

	}

	bool IsInitialised() const { return m_Data != NULL; }
	void Reset() { m_Data = NULL; m_NameHash = 0; }
	
	// PURPOSE
	//	Initialises this sound set with the specified metadata
	bool Init(const SoundSet *metadata);

	// PURPOSE
	//	Initialises this sound set with the specific object name
	bool Init(const u32 nameHash);
	bool Init(const char *name);

	// PURPOSE
	//	Returns a reference to the sound, referenced by its set name
	audMetadataRef Find(const char *name) const;
	audMetadataRef Find(const u32 nameHash) const;

	s32 FindIndex(const u32 nameHash) const;
	bool Contains(const u32 nameHash) const { return FindIndex(nameHash) != -1; }

	const SoundSet *GetMetadata() const { return m_Data; }
	u32 GetNameHash() const { return m_NameHash; }

private:

	const SoundSet *m_Data;
	u32 m_NameHash;
};

class audSoundSetFieldRef
{
public:
	audSoundSetFieldRef() { SetInvalid(); }
	audSoundSetFieldRef(const u32 objectHash) { Set(objectHash); }
	audSoundSetFieldRef(audSoundSet& soundSet, const u32 fieldHash) { Set(soundSet, fieldHash); }
	audSoundSetFieldRef(const u32 objectSetHash, const u32 fieldHash) { Set(objectSetHash, fieldHash); }

	// Get a packed u64 version - handy for storing in network packets etc.
	u64 Get() const { return ((u64)m_SoundSetHash << 32) | m_FieldHash; }

	// Generate a audSoundSetFieldRef from a u64 packed value
	static audSoundSetFieldRef Create(const u64 packedValue)
	{
		return audSoundSetFieldRef(packedValue >> 32, packedValue & 0xffffffff);
	}

	// Assignment operator - allows ref to be set to a simple hash (ie. a raw object name, not from an object set)
	audSoundSetFieldRef &operator=(const u32 &hash)
	{
		Set(hash);
		return *this;
	}

	bool operator!=(const audSoundSetFieldRef rhs) const
	{
		return rhs.Get() != Get();
	}

	bool operator!=(const audMetadataRef rhs) const
	{
		return rhs != m_MetadataRef;
	}

	bool operator!=(const bool rhs) const
	{
		return IsValid() != rhs;
	}

	bool operator==(const audSoundSetFieldRef rhs) const
	{
		return rhs.Get() == Get();
	}

	bool operator==(const audMetadataRef rhs) const
	{
		return rhs == m_MetadataRef;
	}

	bool operator==(const bool rhs) const
	{
		return IsValid() == rhs;
	}

	void SetInvalid() { m_SoundSetHash = 0u; m_FieldHash = 0u; m_MetadataRef.SetInvalid(); }
	bool IsValid() const { return m_MetadataRef.IsValid(); }
	bool IsSoundSetValid() const { return m_SoundSetHash != 0u; }
	bool IsMetadataRefValid() const { return GetMetadataRef().IsValid(); }		

	void Set(const u32 objectSetHash, const u32 fieldHash)
	{
		m_SoundSetHash = objectSetHash;
		m_FieldHash = fieldHash;
		ResolveMetadataRef();
	}

	void Set(audSoundSet& objectSet, const u32 fieldHash)
	{
		m_SoundSetHash = objectSet.GetNameHash();
		m_FieldHash = fieldHash;
		ResolveMetadataRef();
	}

	void Set(const u32 soundHash)
	{
		m_SoundSetHash = 0u;
		m_FieldHash = soundHash;
		ResolveMetadataRef();
	}

	audMetadataRef GetMetadataRef() const
	{			
		return m_MetadataRef;
	}

	bool SetIfValid(const u32 soundSetHash, const u32 fieldHash)
	{
		audSoundSet soundSet;

		if(soundSet.Init(soundSetHash) && soundSet.Contains(fieldHash))
		{			
			Set(soundSetHash, fieldHash);
			return true;
		}

		return false;
	}

	u32 GetSoundSetHash() const { return m_SoundSetHash; }
	u32 GetFieldHash() const { return m_FieldHash; }

	// Compare a raw hash value (ie. test that object set is null and hash == field)
	bool EqualsHash(const u32 hashValue) const { return m_FieldHash == hashValue && m_SoundSetHash == 0u; }


private:
	void ResolveMetadataRef();

private:
	u32 m_SoundSetHash;
	u32 m_FieldHash;
	audMetadataRef m_MetadataRef;
};

} // namespace rage

#endif // AUD_SOUNDSET_H
