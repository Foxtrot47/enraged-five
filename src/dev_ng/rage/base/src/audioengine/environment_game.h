// 
// audioengine/environment_game.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#ifndef AUD_ENVIRONMENT_GAME_H
#define AUD_ENVIRONMENT_GAME_H

#define AUD_GAME_OVERRIDDEN_ENVIRONMENT				1
#define AUD_GAME_OVERRIDDEN_ROUTING					1
#define AUD_GAME_OVERRIDDEN_ROUTE_EFFECTS			1
#define AUD_GAME_OVERRIDDEN_VOLUME_IMPLEMENTATION	1
#define AUD_CLAMP_VOLUMES_AT_1						1
#define AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES			2

#include "audiohardware/driverdefs.h"
#include "audiohardware/mixer.h"
#include "audiosoundtypes/sounddefs.h"
#include "atl/bitset.h"
#include "string/stringhash.h"

namespace rage
{

	struct audGameEffectSettings
	{
		audGameEffectSettings()
		{
			MediumRoomSize = 0.55f;
			MediumDamping = 0.2f;
			MediumPredelay = 0.035f;
			MediumFeedback = 0.f;

			LargePredelay = 0.1f;
			LargeFeedback = 0.1f;

			LargeRoomSize = 0.78f;
			LargeDamping = 0.7f;
			LargeReverbHPF = 200.f;
			LargeReverbLPF = static_cast<float>(kVoiceFilterLPFMaxCutoff);

			MinSmallSend = 0.f;
			MinMediumSend = 0.f;
			MinLargeSend = 0.f;
			UnderwaterEffectLevel = 0.f;
			
			SmallReverbSettings = ATSTRINGHASH("default_small_er", 0xFD1C7F6A);

			LargeBypassLPF = true;
			LargeBypassHPF = false;
			LargeBypassDelay = false;
			LargeBypassReverb = false;
			MediumBypassHPF = false;
			MediumBypassDelay = false;
			MediumBypassReverb = false;
			SmallBypass = false;

			
		}
	
		float MediumRoomSize;
		float MediumDamping;
		float MediumFeedback;

		float LargeReverbLPF;
		float LargeReverbHPF;
		float LargeRoomSize;
		float LargeDamping;
		float LargeFeedback;

		float MinSmallSend;
		float MinMediumSend;
		float MinLargeSend;

		float MediumPredelay;
		float LargePredelay;

		float UnderwaterEffectLevel;

		u32 SmallReverbSettings;

		bool LargeBypassLPF;
		bool LargeBypassHPF;
		bool LargeBypassDelay;
		bool LargeBypassReverb;
		bool MediumBypassHPF;
		bool MediumBypassDelay;
		bool MediumBypassReverb;
		bool SmallBypass;
	};

	enum naEffectRoutes
	{
		EFFECT_ROUTE_VEHICLE_ENGINE_MIN = NUM_EFFECTROUTES,
		EFFECT_ROUTE_VEHICLE_ENGINE_0 = EFFECT_ROUTE_VEHICLE_ENGINE_MIN,
		EFFECT_ROUTE_VEHICLE_ENGINE_1,
		EFFECT_ROUTE_VEHICLE_ENGINE_2,
		EFFECT_ROUTE_VEHICLE_ENGINE_3,
		EFFECT_ROUTE_VEHICLE_ENGINE_4,
		EFFECT_ROUTE_VEHICLE_ENGINE_5,
		EFFECT_ROUTE_VEHICLE_ENGINE_6,
		EFFECT_ROUTE_VEHICLE_ENGINE_7,
		EFFECT_ROUTE_VEHICLE_ENGINE_MAX = EFFECT_ROUTE_VEHICLE_ENGINE_7,
		EFFECT_ROUTE_VEHICLE_EXHAUST_MIN,
		EFFECT_ROUTE_VEHICLE_EXHAUST_0 = EFFECT_ROUTE_VEHICLE_EXHAUST_MIN,
		EFFECT_ROUTE_VEHICLE_EXHAUST_1,
		EFFECT_ROUTE_VEHICLE_EXHAUST_2,
		EFFECT_ROUTE_VEHICLE_EXHAUST_3,
		EFFECT_ROUTE_VEHICLE_EXHAUST_4,
		EFFECT_ROUTE_VEHICLE_EXHAUST_5,
		EFFECT_ROUTE_VEHICLE_EXHAUST_6,
		EFFECT_ROUTE_VEHICLE_EXHAUST_7,
		EFFECT_ROUTE_VEHICLE_EXHAUST_MAX = EFFECT_ROUTE_VEHICLE_EXHAUST_7,
		EFFECT_ROUTE_LOCAL_VOICE,
		EFFECT_ROUTE_CONVERSATION_SPEECH_MIN,
		EFFECT_ROUTE_CONVERSATION_SPEECH_0 = EFFECT_ROUTE_CONVERSATION_SPEECH_MIN,
		EFFECT_ROUTE_CONVERSATION_SPEECH_1,
		EFFECT_ROUTE_CONVERSATION_SPEECH_MAX = EFFECT_ROUTE_CONVERSATION_SPEECH_1,
	};

	struct audRoutingMetric
	{
		f32 sourceEffectDryMix;
		f32 sourceEffectWetMix;
		s8 sourceEffectSubmixId;
		s8 effectRoute;
	};
		
struct audEnvironmentSoundMetrics;
class audMixerSubmix;

enum audSpecialEffectMode
{
	kSpecialEffectModeNormal = 0,
	kSpecialEffectModeUnderwater,
	kSpecialEffectModeStoned,
	kSpecialEffectModePauseMenu,
	kSpecialEffectModeSlowMotion,

	kNumSpecialEffectModes
};

#define AUD_GAME_ENVIRONMENT_PUBLIC_DECLARATIONS \
		void SetDeafeningReverbWetness(const f32 reverbWetness); \
		void SetDeafeningFilterCutoff(const f32 filterCutoff); \
		void SetUsingHeadphones(const bool usingHeadphones); \
		void SetEarAttenuation(const F32_VerifyFinite leftAtten, const F32_VerifyFinite rightAtten); \
		void SetBuiltUpFactor(const f32 builtUpFactor); \
		f32 GetBuiltUpFactor() const; \
		void SetRolloffDampingFactor(const f32 rolloffDampingFactor); \
		void SetBuiltUpSpeakerFactors(const f32* builtUpSpeakerFactors); \
		void SetBuiltUpDirectionFactors(const f32* builtUpDirectionFactors); \
		void SetBlockedSpeakerFactors(const f32* blockedSpeakerFactors); \
		void SetBlockedSpeakerLinearVolumes(const f32* blockedSpeakerLinearVolumes); \
		void EnableSourceMetrics(const bool enabled); \
		bool AreSourceMetricsEnabled() const; \
		void SetSpeakerReverbWets(const f32 speakerReverbWets[4][3]); \
		void SetGlobalRolloffPlateauScale(const f32 globalRolloffPlateauScale); \
		void InitCurves(); \
		void SetGlobalPositionedLeakage(f32 globalPositionedLeakage); \
		void SetDeafeningAffectsEverything(const bool doesIt); \
		void SetSpeechCategoryRange(const u32 firstCategory, const u32 lastCategory); \
		void SetSpeakerAngles(); \
		void SetDownmixMode(const audOutputMode downmixOutput); \
		audOutputMode GetDownmixMode() const; \
		bool DoesDeafeningAffectEverything() const { return m_DeafeningAffectsEverything; } \
		bool IsUsingHeadphones() const { return m_UsingHeadphones; } \
		f32 GetHeadphoneLeakage() const { return m_HeadphoneLeakage; } \
		f32 GetEarAttenuation(const u32 index) const { return m_EarAttenuation[index]; } \
		audMixerSubmix *GetMasterSubmix() const; \
		audMixerSubmix *GetSfxSubmix() const; \
		audMixerSubmix *GetMusicSubmix() const; \
		audMixerSubmix *GetSourceReverbSubmix(const u32 index) const; \
		audMixerSubmix *GetMusicReverbSubmix(const u32 index) const; \
		audMixerSubmix *GetVehicleEngineSubmix(const u32 index) const; \
		audMixerSubmix *GetVehicleExhaustSubmix(const u32 index) const; \
		audMixerSubmix *GetConversationSpeechSubmix(const u32 index) const; \
		audMixerSubmix* GetReflectionsSourceSubmix() const; \
		audMixerSubmix* GetReflectionsOutputSubmix(const u32 index) const; \
		s8 GetVehicleEngineSubmixId(const u32 index) const; \
		s8 GetVehicleExhaustSubmixId(const u32 index) const; \
		s8 GetConversationSpeechSubmixId(const u32 index) const; \
		audMixerSubmix *GetLocalVoiceSubmix() const; \
		s32 AssignVehicleEngineSubmix(); \
		void FreeVehicleEngineSubmix(const u32 index); \
		s32 AssignVehicleExhaustSubmix(); \
		void FreeVehicleExhaustSubmix(const u32 index); \
		s32 AssignConversationSpeechSubmix(); \
		void FreeConversationSpeechSubmix(const u32 index); \
		s32 AssignReflectionsOutputSubmix(); \
		void FreeReflectionsOutputSubmix(const u32 index); \
		bool IsEngineSubmixFree(const u32 index); \
		bool IsExhaustSubmixFree(const u32 index); \
		bool IsConversationSpeechSubmixFree(const u32 index); \
		bool IsReflectionsOutputSubmixFree(const u32 index); \
		static bool IsDirectMonoEffectRoute(const u32 route); \
		void SetSpecialEffectMode(const audSpecialEffectMode mode); \
		audSpecialEffectMode GetSpecialEffectMode() const { return m_SpecialEffectMode; } \
		void SetPadSpeakerEnabled(const bool enabled) { m_PadSpeakerEnabled = enabled; } \
		bool GetPadSpeakerEnabled() const { return m_PadSpeakerEnabled; } \
		void SetPadSpeakerVolume(const float linearVolume) { m_PadSpeakerVolume = linearVolume; } \
		void SetPadSpeakerSFXVolume(const float linearVolume) { m_PadSpeakerSFXVolume = linearVolume; } \
		void SetSubmixVirtualisationScore(const s32 submixId, const u32 virtualisationScore);
		
#define AUD_GAME_ENVIRONMENT_PRIVATE_DECLARATIONS \
	void ComputeFourChannelSpeakerVolumesFromPosition(Vec3V_In relativePosition, audEnvironmentMetricsInternal *metrics); \
	void CollapseFourChannelsToTwo(audEnvironmentMetricsInternal *metrics); \
	f32 GetDistanceAttenuationWithBuiltUpRolloff(const f32 curveScale, const f32 distance, const f32 builtUpRolloff, const f32 additionalPlateau) const; \
	void UpdateEffectSettings(); \
	audGameEffectSettings m_ActiveEffectSettings; \
	f32 m_EarAttenuation[2]; \
	f32 m_DeafeningReverbWetness; \
	f32 m_DeafeningFilterCutoff; \
	f32 m_PlateauScale; \
	f32 m_PlateauInner; \
	f32 m_PlateauOuter; \
	f32 m_OcclusionLeakage; \
	f32 m_HeadphoneLeakage; \
	f32 m_GlobalPositionedLeakage; \
	f32 m_BuiltUpFactor; \
	f32 m_RolloffDampingFactor; \
	f32 m_BuiltUpSpeakerFactors[4]; \
	f32 m_BuiltUpDirectionFactors[4]; \
	f32 m_BlockedSpeakerFactors[4]; \
	f32 m_BlockedSpeakerLinearVolumes[4]; \
	f32 m_AmbDirectivity, m_ElevationFactor; \
	f32  m_SpeakerReverbWets[4][3]; \
	f32 m_UpCloseScaling; \
	f32 m_SourceReverbScaling; \
	f32 m_ListenerReverbScaling; \
	f32 m_ExtToIntMaxReverbDamping; \
	f32 m_DistanceFilteringRolloffScale; \
	f32 m_GlobalRolloffPlateauScale; \
	f32 m_MaxDistanceReverbWet; \
	f32 m_MaxDistanceForReverb; \
	f32 m_DistanceLoudnessScalarMin; \
	f32 m_DistanceLoudnessScalarMax; \
	f32 m_ReverbDistanceDamping; \
	f32 m_ReverbDampingDistanceMin; \
	f32 m_ReverbDampingDistanceMax; \
	f32 m_ReverbSizeBuiltUp; \
	f32 m_ReverbSizeOpen; \
	f32 m_MaxBuiltUpReverbWetForLoudSounds; \
	f32 m_MaxBuiltUpReverbWet; \
	f32 m_PadSpeakerVolume; \
	f32 m_PadSpeakerSFXVolume; \
	u32 m_StartSpeechCategoryIndex; \
	u32 m_EndSpeechCategoryIndex; \
	audSpecialEffectMode m_SpecialEffectMode; \
	audOutputMode m_DownmixOutputMode; \
	atRangeArray<s8,3> m_ReverbSubmixIds; \
	atRangeArray<s8,3> m_MusicReverbSubmixIds; \
	s8 m_MusicSubmixId; \
	s8 m_SFXSubmixId; \
	atRangeArray<u32, kMaxSubmixes> m_SubmixVirtualisationScores; \
	atRangeArray<s8, EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN + 1> m_VehicleEngineSubmixIds; \
	atRangeArray<s8, EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN + 1> m_VehicleExhaustSubmixIds; \
	atFixedBitSet<EFFECT_ROUTE_VEHICLE_ENGINE_MAX - EFFECT_ROUTE_VEHICLE_ENGINE_MIN + 1> m_VehicleEngineSubmixesInUse; \
	atFixedBitSet<EFFECT_ROUTE_VEHICLE_EXHAUST_MAX - EFFECT_ROUTE_VEHICLE_EXHAUST_MIN + 1> m_VehicleExhaustSubmixesInUse; \
	s8 m_LocalVoiceSubmixId; \
	atRangeArray<s8, EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN + 1> m_ConversationSpeechSubmixIds; \
	atFixedBitSet<EFFECT_ROUTE_CONVERSATION_SPEECH_MAX - EFFECT_ROUTE_CONVERSATION_SPEECH_MIN + 1> m_ConversationSpeechSubmixesInUse; \
	s8 m_ReflectionsSourceSubmixId; \
	atRangeArray<s8, AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES> m_ReflectionsOutputSubmixIds; \
	atFixedBitSet<AUD_NUM_REFLECTIONS_OUTPUT_SUBMIXES> m_ReflectionsOutputSubmixesInUse; \
	atRangeArray<s8, 1> m_PadSpeakerIds; \
	bool m_DisableDistanceReverb; \
	bool m_ShouldDoPanningStep2; \
	bool m_ShouldDoPanningStep3; \
	bool m_ShouldScalePlateaUp; \
	bool m_ShouldAverageVolumes; \
	bool m_ShouldDoPanningStep4; \
	bool m_ShouldDoPanningStep5; \
	bool m_DontUseEnvironmentalStuff; \
	bool m_UsingHeadphones; \
	bool m_UsingAmbisonics; \
	bool m_AmbisonicUnitCircleOpto; \
	bool m_EnableSourceEnvironmentMetrics; \
	bool m_ShouldLeakReverb; \
	bool m_OnlyReverb; \
	bool m_DeafeningAffectsEverything; \
	bool m_PadSpeakerEnabled;

class audEnvironmentGameMetric
{
public:
	audEnvironmentGameMetric()
	{
		Reset();
	}
	
	~audEnvironmentGameMetric() 
	{
	}




	f32 GetLeakageFactor() const {return m_LeakageFactor;};
	void SetLeakageFactor(const f32 leakageFactor){m_LeakageFactor = leakageFactor;};
	f32 GetDistanceAttenuationDamping() const {return m_DistanceAttenuationDamping;};
	void SetDistanceAttenuationDamping(const f32 distanceAttenuationDamping){m_DistanceAttenuationDamping = distanceAttenuationDamping;};
	f32 GetReverbSmall() const {return m_ReverbSmall;};
	void SetReverbSmall(const f32 reverbSmall){m_ReverbSmall = reverbSmall;};
	f32 GetReverbMedium() const {return m_ReverbMedium;};
	void SetReverbMedium(const f32 reverbMedium){m_ReverbMedium = reverbMedium;};
	f32 GetReverbLarge() const {return m_ReverbLarge;};
	void SetReverbLarge(const f32 reverbLarge){m_ReverbLarge = reverbLarge;};
	bool GetIsInInterior() const { return m_IsInInterior; }
	void SetIsInInterior(const bool isInInterior) { m_IsInInterior = isInInterior; }
	f32 GetRolloffFactor() const { return m_RolloffFactor; }
	void SetRolloffFactor(const f32 rolloff);
	f32 GetOcclusionAttenuationLin() const { return m_OcclusionAttenLin; }
	void SetOcclusionAttenuationLin(const f32 occlusionAttenuationLin) { m_OcclusionAttenLin = occlusionAttenuationLin; }
	f32 GetOcclusionFilterCutoffLin() const { return m_OcclusionFilterCutoffLin; }
	void SetOcclusionFilterCutoffLin(const f32 occlusionFilterCutoffLin) { m_OcclusionFilterCutoffLin = occlusionFilterCutoffLin; }
	f32 GetOcclusionPathDistance() const { return m_OcclusionPathDistance; }
	void SetOcclusionPathDistance(const f32 occlusionPathDistance) { m_OcclusionPathDistance = occlusionPathDistance; }
	const Vec3V& GetOcclusionPathPosition() const { return m_OcclusionPathPosition; }
	void SetOcclusionPathPosition(const Vec3V& occlusionPathPosition) { m_OcclusionPathPosition = occlusionPathPosition; }
	bool GetUseOcclusionPath() const { return m_UseOcclusionPath; }
	void SetUseOcclusionPath(const bool useOcclusionPath) { m_UseOcclusionPath = useOcclusionPath; }
	bool GetJustReverb() const { return m_JustReverb; }
	void SetJustReverb(const bool justReverb) { m_JustReverb = justReverb; }
	void SetDryLevel(const float dryLevel) { m_DryLevel = dryLevel; }
	float GetDryLevel() const { return m_JustReverb ? 0.0f : m_DryLevel; }
	void SetMixGroup(s32 mixGroupIndex);
	void SetPrevMixGroup(s32 mixGroupIndex);
	void SetMixGroupApplyValue(u8 applyValue){m_MixGroupApplyValue = applyValue;};
	void InitMixGroup() { m_MixGroupIndex = -1; m_PrevMixGroupIndex = -1; }
	s32	 GetMixGroup() const { return m_MixGroupIndex; }
	s32	 GetPrevMixGroup() const { return m_PrevMixGroupIndex; }
	u8	 GetMixGroupApplyValue() const { return m_MixGroupApplyValue; }
	void SetOcclusion(const audEnvironmentGameMetric & occlusionMetrics)
	{ 
		m_OcclusionPathPosition = occlusionMetrics.GetOcclusionPathPosition(); 
		m_OcclusionPathDistance = occlusionMetrics.GetOcclusionPathDistance();
		m_OcclusionAttenLin = occlusionMetrics.GetOcclusionAttenuationLin();
		m_OcclusionFilterCutoffLin = occlusionMetrics.GetOcclusionFilterCutoffLin();
		m_LeakageFactor = occlusionMetrics.GetLeakageFactor();
		m_DistanceAttenuationDamping = occlusionMetrics.GetDistanceAttenuationDamping();
		m_UseOcclusionPath = occlusionMetrics.GetUseOcclusionPath();
	}
	void SetReverb(const audEnvironmentGameMetric & reverbMetrics)
	{ 
		m_ReverbSmall = reverbMetrics.GetReverbSmall();
		m_ReverbMedium = reverbMetrics.GetReverbMedium();
		m_ReverbLarge = reverbMetrics.GetReverbLarge();
		m_DryLevel = reverbMetrics.GetDryLevel();
		m_JustReverb = reverbMetrics.GetJustReverb();
		m_IsInInterior = reverbMetrics.GetIsInInterior();
	}

	void Reset()
	{
		m_LeakageFactor = 0.0f;
		m_DistanceAttenuationDamping = 1.0f;
		m_OcclusionAttenLin = 1.0f;
		m_OcclusionFilterCutoffLin = 1.0f;
		m_OcclusionPathDistance = 0.0f;
		m_OcclusionPathPosition.ZeroComponents();
		m_UseOcclusionPath = false;

		m_ReverbSmall = 0.0f;
		m_ReverbMedium = 0.0f;
		m_ReverbLarge = 0.0f;
		m_DryLevel = 1.f;
		m_JustReverb = false;
		m_IsInInterior = false;

		m_RolloffFactor = 1.f;
		m_MixGroupIndex = -1;
		m_PrevMixGroupIndex = -1;
		m_MixGroupApplyValue = 0;
	}

private:

	Vec3V m_OcclusionPathPosition;

	f32 m_OcclusionPathDistance;
	f32 m_OcclusionAttenLin;
	f32 m_OcclusionFilterCutoffLin;
	f32 m_LeakageFactor;

	f32 m_DistanceAttenuationDamping;
	f32 m_ReverbSmall;
	f32 m_ReverbMedium;
	f32 m_ReverbLarge;

	f32 m_DryLevel;
	f32 m_RolloffFactor;
	s16 m_MixGroupIndex;
	s16 m_PrevMixGroupIndex;
	//Purpose: Use for both, adding/removing an entity form a mix group and moving that entity between mixgroups.
	u8  m_MixGroupApplyValue;
	bool m_UseOcclusionPath : 1;
	bool m_JustReverb : 1;
	bool m_IsInInterior : 1;
};

#if AUD_GAME_OVERRIDDEN_VOLUME_IMPLEMENTATION

#if __PS3
// for PS3 its worth using 8bit volumes due to SPU local memory size, for xenon the packing/unpacking is too expensive
// to be worth the memory savings
class audCompressedVolume8;
typedef audCompressedVolume8 audCompressedVolume;
#else
class audCompressedVolume32;
typedef audCompressedVolume32 audCompressedVolume;
#endif

#endif // AUD_GAME_OVERRIDDEN_VOLUME_IMPLEMENTATION

enum audSmallReverbEffectIds
{
	kSmallReverb_Reverb = 0,
	kSmallReverb_UnderwaterEffect,
};

enum audMediumReverbEffectIds
{	
	kMediumReverb_Delay = 0,
	kMediumReverb_Reverb,
};
enum audLargeReverbEffectIds
{	
	kLargeReverb_HPF = 0,
	kLargeReverb_LPF,
	kLargeReverb_Delay,
	kLargeReverb_Reverb,
};

}
#endif // AUD_ENVIRONMENT_GAME_H


