// 
// audioengine/requestedsettings.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "engine.h"
#include "enginedefs.h"
#include "requestedsettings.h"
#include "audiosoundtypes/sound.h"
#include "system/memory.h"
#include "audioengine/controller.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundpool.h"
#include "audiohardware/debug.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/syncsource.h"
#include "fwsys/timer.h"

namespace rage
{

AUD_RECORDER_ONLY(audSoundRecorderInterface g_dummyRecorderInterface);
AUD_RECORDER_ONLY(audSoundRecorderInterface* audRequestedSettings::sm_pRecorder = &g_dummyRecorderInterface);

bool audRequestedSettings::sm_IgnoreBlockedAudioThread;
u32 audRequestedSettings::sm_RequestedSettingsReadIndex;
u32 audRequestedSettings::sm_RequestedSettingsWriteIndex;

void audRequestedSettings::operator delete(void *)
{
	Assert(0);
}
audRequestedSettings::audRequestedSettings(audSound* pSnd)
{
	// the sound pool memsets all slots to 0 when allocating; this constructor must deal with anything that should
	// be initialised to a non-zero value

	m_ParentSound = pSnd;
#if RSG_PC || RSG_ORBIS || RSG_DURANGO
	m_FrequencySmoothingRate = -1.f;


	for (u32 n=0; n<NUM_REQUEST_BUFFERS;++n)
	{
		// note that 0 is a valid pan, so we need to manually set it to -1
		m_Settings[n].Pan = tRequestedSettings::InvalidPanValue;

		// default to all listeners
		m_Settings[n].ListenerMask = 0xff;

		// default source effect wet/dry to full wet
		m_Settings[n].SourceEffectDryMix = 1.f;

		// We always use this value, and take the lowest of this and the occlusion freq, so need to default to full
		m_Settings[n].LPFCutoff = kVoiceFilterLPFMaxCutoff;
		// HPF is initialised to 0

		m_Settings[n].DopplerFactor = 1.0f;

		m_Settings[n].VolumeCurveScale = 1.0f;

		m_Settings[n].EnvironmentalLoudness = 255;
	}
#else
	// note that 0 is a valid pan, so we need to manually set it to -1
	m_Settings[0].Pan = tRequestedSettings::InvalidPanValue;
	m_Settings[1].Pan = tRequestedSettings::InvalidPanValue;
	m_Settings[2].Pan = tRequestedSettings::InvalidPanValue;

	// default to all listeners
	m_Settings[0].ListenerMask = m_Settings[1].ListenerMask = m_Settings[2].ListenerMask = 0xff;

	// default source effect wet/dry to full wet
	m_Settings[0].SourceEffectDryMix = m_Settings[1].SourceEffectDryMix = m_Settings[2].SourceEffectDryMix = 1.f;

	// We always use this value, and take the lowest of this and the occlusion freq, so need to default to full
	m_Settings[0].LPFCutoff = m_Settings[1].LPFCutoff = m_Settings[2].LPFCutoff = kVoiceFilterLPFMaxCutoff;
	// HPF is initialised to 0

	m_FrequencySmoothingRate = -1.f;

	m_Settings[0].DopplerFactor = m_Settings[1].DopplerFactor = m_Settings[2].DopplerFactor = 1.0f;

	m_Settings[0].VolumeCurveScale = m_Settings[1].VolumeCurveScale = m_Settings[2].VolumeCurveScale = 1.0f;

	m_Settings[0].EnvironmentalLoudness = m_Settings[1].EnvironmentalLoudness = m_Settings[2].EnvironmentalLoudness = 255;

	// if number of buffers changes then the above hardcoded initialisation lines need updating
	CompileTimeAssert(g_audRequestedSettingsBuffers == 3);
#endif

	m_EntityVariableBlockIndex = -1;

	m_PrepareTimeLimit = ~0U;
	m_ReleaseTime = -1;

	m_WaveSlotIndex = 0xff;

	m_PlayTimeMixerFrames = ~0U;
	m_PlayTimeMixerSubFrameOffset = 0xffff;

	m_SyncSlaveId = m_SyncMasterId = audMixerSyncManager::InvalidId;

	m_ShouldAttenuateOverDistance = AUD_TRISTATE_UNSPECIFIED;
	m_ShouldDisableRearAttenuation = AUD_TRISTATE_UNSPECIFIED;
	m_ShouldDisableRearFiltering = AUD_TRISTATE_UNSPECIFIED;

	// Vector3 constructor helpfully NaNs stuff out, so the fact that we've already initialised the memory isnt enough
	m_SettingsPosition->ZeroComponents();

	//Init our compressed quat
	m_Orientation.Init();

	m_PositionRelativePanDamping = 1.0f;
}

#if !__SPU
void audRequestedSettings::InitClass()
{
	sm_IgnoreBlockedAudioThread = false;

	// We start off by pointing the write head at 1 and the read head at 0.
	// ************************
	// Atomic changes work like this: The audController on the game thread updates the write index [0] at the
	// end of its Update(). At the start of the SoundManager's Update() on the audio thread, it updates the
	// read-index [1] to be one behind the current write index. This ensures all game-thread changes on the same frame
	// are picked up on the same audio-frame.
	// Note: the write index mirrors the audSoundManager::GameUpdateIndex, and must start in sync with that
	SetWriteIndex(1);
	SetReadIndex(0);

}

void audRequestedSettings::ShutdownClass()
{

}

void audRequestedSettings::Commit(const u32 timeInMs, const Indices &indices)
{
	// 05/09/06 MdS
	// This has changed a little, as we now have a single read and write index per controller.
	// We don't change indexes any more, but we do still need to copy settings across.
	// Also, re-write the assert-case where our audio thread is lagging. This will no longer crash on pausing,
	// so is safe to leave in. This will still crash on single-stepping the audio thread, but in that circumstance
	// the assert is ignorable and the game will carry on.

	u32 writeIndex = indices.writeIndex;
	u32 nextWriteIndex = indices.nextWriteIndex;
	u32 previousWriteIndex = indices.previousWriteIndex;
	u32 currentReadIndex = indices.readIndex;

	// Check the audio thread is close behind the game thread
	if ((nextWriteIndex == currentReadIndex) && !sm_IgnoreBlockedAudioThread)
	{
		// We'd be about to overwrite something that's about to be read, so we block.
		const u32 timeStartBlocking =  sysTimer::GetSystemMsTime();
		u32 timeNow =  sysTimer::GetSystemMsTime();
		// block for up to a second

		TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"Block Delay");
		static u32 uMaxWait = 1000;
		while ((nextWriteIndex == currentReadIndex) && (timeNow<(timeStartBlocking+uMaxWait)))
		{
			sysIpcSleep(1);
			currentReadIndex = GetReadIndex();
			timeNow =  sysTimer::GetSystemMsTime();
		}
		TELEMETRY_END_ZONE(__FILE__,__LINE__);

		// See if we've broken out because we've blocked for unacceptably long
		if (nextWriteIndex == currentReadIndex)
		{
#if RSG_PC
			Warningf("There seems to be something wrong with your sound card. The game will likely be silent. Please reboot to fix");
#endif
			audWarningf("Audio Thread is running too slowly, or is blocked. Ignoring this will result in non-atomic audio changes");
			sm_IgnoreBlockedAudioThread = true;
		}
	}

	// Work out our velocity
	if (m_LastTimeUpdated == 0)
	{
		// This is the first commit, so we don't know our velocity
		Vector3 zero;
		zero.Zero();
		m_Velocity.Set(zero);
		m_LastTimeUpdated = timeInMs;
	}
	else
	{
		Vec3V velocity = m_SettingsPosition[writeIndex] - m_SettingsPosition[previousWriteIndex];
		u32 timeStep = timeInMs - m_LastTimeUpdated;
		m_LastTimeUpdated = timeInMs;
		m_Settings[writeIndex].UpdateFrame++;
		m_TimeStep = (u32)(((f32)timeStep)/g_AudioEngine.GetSoundManager().GetTimeScale(0));

		if(timeStep > 0)
		{
			m_Velocity.Set(InvScale(velocity,ScalarV(((f32)timeStep) * 0.03333f)));
		}
		else
		{
			m_Velocity.Set(Vec3V(V_ZERO));
		}
	}

	m_Settings[nextWriteIndex] = m_Settings[writeIndex];
	m_SettingsPosition[nextWriteIndex] = m_SettingsPosition[writeIndex];
	

	return;
}
#endif // !__SPU

audRequestedSettings::~audRequestedSettings()
{
	// ****** NOTE
	// Note that occasionally (eg MathOperationSound) we use a sound/reqSet pool slot for extra storage, and cast to a ReqSet* for deletion.
	// When doing this, the ReqSet must be in a safe state for this destructor. If changing this code, also change ShutdownWhenNotReallyARequestedSetting()

	// We now do environment group reference from the SetEnvironmentGroup fn - and nowhere else - so check we've no environment group by now.
	Assert(!m_EnvironmentGroup);

	audMixerSyncManager::Get()->Release(m_SyncMasterId);
	audMixerSyncManager::Get()->Release(m_SyncSlaveId);

	audEntity::RemoveVariableBlockRef(m_EntityVariableBlockIndex);

	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnDelete(m_ParentSound); })
}

void audRequestedSettings::UpdateEnvironmentGameMetric()
{
	if (m_EnvironmentGroup)
	{
#if RSG_ASSERT
		const u32 numSoundRefs = m_EnvironmentGroup->GetNumberOfSoundReference();
		audAssertf(numSoundRefs != 0 && numSoundRefs != 0xDDDDDDDD,
			"UpdateEnvironmentGameMetric called with invalid environment group reference count; %u", numSoundRefs);
#endif
		m_EnvironmentGroup->PopulateEnvironmentMetric(&m_EnvironmentGameMetric);
	}
}

bool audRequestedSettings::HasMixerPlayTime(u32& playTime, u16& subFrameOffset)
{
	if (m_PlayTimeMixerFrames != ~0u)
	{
		playTime = m_PlayTimeMixerFrames;
		Assign(subFrameOffset, m_PlayTimeMixerSubFrameOffset);
		return true;
	}

	return false;
}

void audRequestedSettings::SetPan(const s32 pan)
{
	u32 packedPan = (u16)pan;
	if(pan == -1)
	{
		packedPan = tRequestedSettings::InvalidPanValue;
	}
	else
	{
		Assertf(packedPan < tRequestedSettings::InvalidPanValue, "Invalid pan value: %d", pan);
	}
	m_Settings[GetWriteIndex()].Pan = packedPan;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedPan(m_ParentSound, pan); })
}

void audRequestedSettings::SetPitch(const s32 pitch)
{
	m_Settings[GetWriteIndex()].Pitch = (s16)pitch;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedPitch(m_ParentSound, pitch); })
}

void audRequestedSettings::SetPosition(Vec3V_In position)
{
	m_SettingsPosition[GetWriteIndex()] = position;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedPosition(m_ParentSound, position); })
}
void audRequestedSettings::SetPosition(const Vector3 &position)
{
	SetPosition(VECTOR3_TO_VEC3V(position));
}
void audRequestedSettings::SetVolume(const f32 volume)
{
	// Assert if the specified volume is improbably loud or quiet, as it typically indicates a high level error.
	Assertf(volume >= g_SilenceVolume*3.f && volume <= 24.f, "Invalid volume: %f", volume);
	m_Settings[GetWriteIndex()].Volume = volume;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedVolume(m_ParentSound, volume); })
}

void audRequestedSettings::SetPostSubmixVolumeAttenuation(const f32 volumeAttenuation)
{
	m_Settings[GetWriteIndex()].PostSubmixVolumeAttenuation = volumeAttenuation;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedPostSubmixVolumeAttenuation(m_ParentSound, volumeAttenuation); })
}

void audRequestedSettings::SetSourceEffectMix(const f32 wet, const f32 dry)
{
	m_Settings[GetWriteIndex()].SourceEffectWetMix = wet;
	m_Settings[GetWriteIndex()].SourceEffectDryMix = dry;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedSourceEffectMix(m_ParentSound, wet, dry); })
}

void audRequestedSettings::SetShouldAttenuateOverDistance(const TristateValue state)
{
	m_ShouldAttenuateOverDistance = (u8)state;
	Assert(m_ShouldAttenuateOverDistance == state);
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedShouldAttenuateOverDistance(m_ParentSound, state); })
}

void audRequestedSettings::SetShouldDisableRearAttenuation(const TristateValue state)
{
	m_ShouldDisableRearAttenuation = (u8)state;
	Assert(m_ShouldDisableRearAttenuation == state);
	// NOTE - Not currently debug recorded!
}

void audRequestedSettings::SetShouldDisableRearFiltering(const TristateValue state)
{
	m_ShouldDisableRearFiltering = (u8)state;
	Assert(m_ShouldDisableRearFiltering == state);
	// NOTE - Not currently debug recorded!
}

void audRequestedSettings::SetLowPassFilterCutoff(const u32 cutoff)
{
	Assign(m_Settings[GetWriteIndex()].LPFCutoff, Clamp<u32>(cutoff, kVoiceFilterLPFMinCutoff, kVoiceFilterLPFMaxCutoff));
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedLPFCutoff(m_ParentSound, cutoff); })
}

void audRequestedSettings::SetHighPassFilterCutoff(const u32 cutoff)
{
	Assign(m_Settings[GetWriteIndex()].HPFCutoff, cutoff);
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedHPFCutoff(m_ParentSound, cutoff); })
}

void audRequestedSettings::SetFrequencySmoothingRate(const f32 rate)
{
	m_FrequencySmoothingRate = rate;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedFrequencySmoothingRate(m_ParentSound, rate); })
}

void audRequestedSettings::SetIsSetToPlay(const bool setToPlay)
{
	m_Settings[GetWriteIndex()].IsSetToPlay = setToPlay;
}

void audRequestedSettings::SetIsSetToStop(const bool setToStop)
{
	m_Settings[GetWriteIndex()].IsSetToStop = setToStop;

#if AUD_ENABLE_RECORDER_INTERFACE
	if(sm_pRecorder != NULL)
	{
		if(m_IsEntityRefPtrInvalid == true && m_IsEntityPtrInvalid == true && m_AllowOrphaned == true)
		{
			sm_pRecorder->OnStopAndForget(m_ParentSound);
		}
		else
		{
			sm_pRecorder->OnStop(m_ParentSound);
		}
	}
#endif // AUD_ENABLE_RECORDER_INTERFACE
}

void audRequestedSettings::SetDopplerFactor(const f32 dopplerFactor)
{
	audAssert(FPIsFinite(dopplerFactor));
	m_Settings[GetWriteIndex()].DopplerFactor = dopplerFactor;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedDopplerFactor(m_ParentSound, dopplerFactor); })
}

void audRequestedSettings::SetVolumeCurveScale(const f32 volumeCurveScale)
{
	m_Settings[GetWriteIndex()].VolumeCurveScale = volumeCurveScale;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedVolumeCurveScale(m_ParentSound, volumeCurveScale); })
}

void audRequestedSettings::SetSpeakerMask(const u8 speakerMask)
{
	m_Settings[GetWriteIndex()].SpeakerMask = speakerMask;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedSpeakerMask(m_ParentSound, speakerMask); })
}

void audRequestedSettings::SetQuadSpeakerLevels(const f32* speakerLevels)
{
	// Pack all the volume level data into the position vector, since it won't be required
	Vec3V speakerLevelsVec3;
	speakerLevelsVec3.SetXf(speakerLevels[0]);
	speakerLevelsVec3.SetYf(speakerLevels[1]);
	speakerLevelsVec3.SetZf(speakerLevels[2]);
	speakerLevelsVec3.SetWf(speakerLevels[3]);
	SetPosition(speakerLevelsVec3);

	// Disable doppler and distance attenuation - make no sense if we're requesting explicit volumes
	SetDopplerFactor(0.0f);
	SetShouldAttenuateOverDistance(AUD_TRISTATE_FALSE);

	// Set the flag to let the environment sound know that position data is bogus
	m_Settings[GetWriteIndex()].HasQuadSpeakerLevels = true;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedQuadSpeakerLevels(m_ParentSound, speakerLevels); })
}

void audRequestedSettings::SetListenerMask(const u8 listenerMask)
{
	m_Settings[GetWriteIndex()].ListenerMask = listenerMask;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedListenerMask(m_ParentSound, listenerMask); })
}

void audRequestedSettings::SetEnvironmentalLoudness(const u8 environmentalLoudness)
{
	m_Settings[GetWriteIndex()].EnvironmentalLoudness = environmentalLoudness;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedEnvironmentalLoudness(m_ParentSound, environmentalLoudness); })
}

void audRequestedSettings::SetEnvironmentalLoudnessFloat(const f32 environmentalLoudness)
{
	u8 environmentalLoudnessU8 = (u8)(environmentalLoudness*255.0f);
	m_Settings[GetWriteIndex()].EnvironmentalLoudness = environmentalLoudnessU8;
	AUD_RECORDER_ONLY(if(sm_pRecorder != NULL) { sm_pRecorder->OnSetRequestedEnvironmentalLoudnessFloat(m_ParentSound, environmentalLoudness); })
}

s32 audRequestedSettings::GetPan() const
{
	const u32 packedPan = m_Settings[GetWriteIndex()].Pan;
	if(packedPan == tRequestedSettings::InvalidPanValue)
	{
		return -1;
	}
	return static_cast<s32>(packedPan);
}

s32 audRequestedSettings::GetPitch() const
{
	return m_Settings[GetWriteIndex()].Pitch;
}

Vec3V_Out audRequestedSettings::GetPosition() const
{
	return m_SettingsPosition[GetWriteIndex()];
}

f32 audRequestedSettings::GetVolume() const
{
	return m_Settings[GetWriteIndex()].Volume;
}

f32 audRequestedSettings::GetPostSubmixVolumeAttenuation() const
{
	return m_Settings[GetWriteIndex()].PostSubmixVolumeAttenuation;
}

f32 audRequestedSettings::GetSourceEffectWetMix() const
{
	return m_Settings[GetWriteIndex()].SourceEffectWetMix;
}

f32 audRequestedSettings::GetSourceEffectDryMix() const
{
	return m_Settings[GetWriteIndex()].SourceEffectDryMix;
}

bool audRequestedSettings::GetHasQuadSpeakerLevels() const
{
	return m_Settings[GetWriteIndex()].HasQuadSpeakerLevels;
}

u32 audRequestedSettings::GetLowPassFilterCutoff() const
{
	return m_Settings[GetWriteIndex()].LPFCutoff;
}

u32 audRequestedSettings::GetHighPassFilterCutoff() const
{
	return m_Settings[GetWriteIndex()].HPFCutoff;
}

bool audRequestedSettings::GetIsSetToPlay() const
{
	return m_Settings[GetWriteIndex()].IsSetToPlay;
}

bool audRequestedSettings::GetIsSetToStop() const
{
	return m_Settings[GetWriteIndex()].IsSetToStop;
}

void audRequestedSettings::ShutdownWhenNotReallyARequestedSetting()
{
	// Use this fn when you're using a slot for some nefarious purpose, and need to leave the ReqSet in a state that's safe to have it's destructor run.
	// eg the MathOperationSound uses a ReqSet/Sound pool slot for extra storage, and casts that slot to a ReqSet for deletion. As it's full of
	// non-ReqSet data, this can look like it has an environment group when it doesn't, and either screw up the ref count, or assert.
	m_EnvironmentGroup = NULL;
}

f32 audRequestedSettings::GetDopplerFactor() const
{
	return m_Settings[GetWriteIndex()].DopplerFactor;
}

f32 audRequestedSettings::GetVolumeCurveScale() const
{
	return m_Settings[GetWriteIndex()].VolumeCurveScale;
}

u8 audRequestedSettings::GetSpeakerMask() const
{
	return m_Settings[GetWriteIndex()].SpeakerMask;
}

u8 audRequestedSettings::GetListenerMask() const
{
	return m_Settings[GetWriteIndex()].ListenerMask;
}

u8 audRequestedSettings::GetEnvironmentalLoudness() const
{
	return m_Settings[GetWriteIndex()].EnvironmentalLoudness;
}

f32 audRequestedSettings::GetEnvironmentalLoudnessFloat() const
{
	return (((f32)m_Settings[GetWriteIndex()].EnvironmentalLoudness)/255.0f);
}

void audRequestedSettings::SetEnvironmentGroup(audEnvironmentGroupInterface* environmentGroup)
{
	// Environment groups are referenced counted, so this fn needs to be super safe, and game-code shouldn't be fudging ref counts itself.
	// We only ever do this on initial sound creation, so don't ever allow it to be changed once it's initially set.
	// 17/7/7 MdS actually, we now clear this from the controller, rather than the destructor, so that it can run on spus, so 
	// lets just allow it to be changed, and inc/dec as appropriate
	// NOTE: sounds underneath this parent point directly to the environment metric in the group (primarily to allow for the SequentialStreamingSound)
//	if (m_EnvironmentGroup)
//	{
//		AssertMsg(0, "Audio: setting an environment group on a ReqSet when we already have one");
//		return;
//	}
	if (m_EnvironmentGroup)
	{
		m_EnvironmentGroup->RemoveSoundReference();
		m_EnvironmentGroup = NULL;
	}
	if (environmentGroup)
	{
		environmentGroup->AddSoundReference();
	}
	m_EnvironmentGroup = environmentGroup;
}

audEnvironmentGroupInterface* audRequestedSettings::GetEnvironmentGroup() const
{
	return m_EnvironmentGroup;
}

void audRequestedSettings::SetSyncSlaveId(const u32 syncId)
{
	audMixerSyncManager::Get()->Release(m_SyncSlaveId);
	if(syncId >= audMixerSyncManager::InvalidId)
	{		
		m_SyncSlaveId = audMixerSyncManager::InvalidId;
	}
	else
	{
		audMixerSyncManager::Get()->AddOwnerRef(syncId);
		Assign(m_SyncSlaveId, syncId);
	}
}

void audRequestedSettings::SetSyncMasterId(const u32 syncId)
{
	audMixerSyncManager::Get()->Release(m_SyncMasterId);
	if(syncId >= audMixerSyncManager::InvalidId)
	{
		m_SyncMasterId = audMixerSyncManager::InvalidId;
	}
	else
	{
		audMixerSyncManager::Get()->AddOwnerRef(syncId);
		Assign(m_SyncMasterId, syncId);
	}
}


}//namespace rage
