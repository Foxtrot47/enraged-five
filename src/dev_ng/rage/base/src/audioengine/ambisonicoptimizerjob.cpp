// 
// audioengine/ambisonicaptimizerjob.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "vector/vector3_consts_spu.cpp"

#include "ambisonicoptimizerjob.h"

using namespace rage;


#if __SPU
#include "string/stringhash.cpp"
#include "ambisonics.cpp"
#else
#include "ambisonics.h"
#endif

#if AUD_ENABLE_AMBISONICS

#if __SPU
static audAmbisonics *inAmb;
#endif

static TabuSpuOutput *outData;
                                       
void AmbisonicOptimizerJob(sysTaskParameters &p)
{
	Assert(p.UserData[2].asInt == sizeof(audAmbisonics));
#if __DEV
	if(p.UserData[2].asInt == sizeof(audAmbisonics))
#endif
	{
#if __SPU
		outData = (TabuSpuOutput*)p.Output.Data;
		inAmb = (audAmbisonics*)p.Input.Data;
		inAmb->TabuCore(p.UserData[0].asInt, p.UserData[1].asInt);
		inAmb->SetJobOutput(outData);
#else
		outData = (TabuSpuOutput*)p.Output.Data;
		outData->Init();
		audAmbisonics temp((audAmbisonics*)p.Input.Data);
		temp.TabuCore(p.UserData[0].asInt, p.UserData[1].asInt);
		temp.SetJobOutput(outData, &temp);
#endif
	}
}

#else
void AmbisonicOptimizerJob(sysTaskParameters &){}
#endif
