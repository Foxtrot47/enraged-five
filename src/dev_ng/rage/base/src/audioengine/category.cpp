//
// audioengine/category.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "category.h"
#include "categorydefs.h"
#include "categorymanager.h"
#include "engine.h"
#include "environment.h"
#include "metadatamanager.h"

#include "string/stringhash.h"

XPARAM(audiodesigner);

namespace rage {
extern atPool<audCategory> *g_CategoryStore;

#if !__SPU

audCategory::audCategory()
{
	m_Metadata = NULL;
#if __BANK
	m_IsMuted = false;
	m_RecursionDebugIndex = -1;
#endif
}

void audCategory::Shutdown()
{
	m_Metadata = NULL;
}

bool audCategory::InitCore(u32 categoryNameHash, audCategory* parent, audMetadataManager* metadataManager)
{

	audAssertf(!m_Metadata, "Trying to initialise a category that already has metadata, g_CategoryStore and g_CategoryStoreMem may be out of sync");

	// Clear out our settings array
#if AUD_ATOMIC_CATEGORIES
	for (int i=0; i<3; i++)
	{
		m_CategorySettings[i].Volume.SetFloat16_Zero();
		m_CategorySettings[i].Pitch = 0;
		m_CategorySettings[i].DistanceRollOffScale.SetFloat16_One();
		m_CategorySettings[i].PlateauRollOffScale.SetFloat16_One();
		m_CategorySettings[i].OcclusionDamping.SetFloat16_One();
		m_CategorySettings[i].EnvironmentalFilterDamping.SetFloat16_One();
		m_CategorySettings[i].SourceReverbDamping.SetFloat16_One();
		m_CategorySettings[i].DistanceReverbDamping.SetFloat16_One();
		m_CategorySettings[i].InteriorReverbDamping.SetFloat16_One();
		m_CategorySettings[i].EnvironmentalLoudness.SetFloat16_One();
		m_CategorySettings[i].LPFCutoff = kVoiceFilterLPFMaxCutoff;
		m_CategorySettings[i].HPFCutoff = 0;
		m_CategorySettings[i].UnderwaterWetLevel.SetFloat16_One();
		m_CategorySettings[i].StonedWetLevel.SetFloat16_One();

	}
#else
	m_CategorySettings.Volume.SetFloat16_Zero();
	m_CategorySettings.Pitch = 0;
	m_CategorySettings.DistanceRollOffScale.SetFloat16_One();
	m_CategorySettings.PlateauRollOffScale.SetFloat16_One();
	m_CategorySettings.OcclusionDamping.SetFloat16_One();
	m_CategorySettings.EnvironmentalFilterDamping.SetFloat16_One();
	m_CategorySettings.SourceReverbDamping.SetFloat16_One();
	m_CategorySettings.DistanceReverbDamping.SetFloat16_One();
	m_CategorySettings.InteriorReverbDamping.SetFloat16_One();
	m_CategorySettings.EnvironmentalLoudness.SetFloat16_One();
	m_CategorySettings.LPFCutoff = kVoiceFilterLPFMaxCutoff;
	m_CategorySettings.HPFCutoff = 0;
	m_CategorySettings.UnderwaterWetLevel.SetFloat16_One();
	m_CategorySettings.StonedWetLevel.SetFloat16_One();
#endif

	if (!metadataManager)
	{
		Assert(0);
		return false;
	}

	// Set up our metadata ptr
	Category *metadata = metadataManager->GetObject<Category>(categoryNameHash);
	if (!metadata)
	{
		Assert(0);
		return false;
	}

	m_Metadata = metadata;


	Assign(m_Index, audCategoryManager::GetCategoryIndex(this));

	g_ResolvedCategorySettings[m_Index].Volume.SetFloat16_Zero();
	g_ResolvedCategorySettings[m_Index].Pitch = 0;
	g_ResolvedCategorySettings[m_Index].DistanceRollOffScale.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].PlateauRollOffScale.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].OcclusionDamping.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].EnvironmentalFilterDamping.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].SourceReverbDamping.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].DistanceReverbDamping.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].InteriorReverbDamping.SetFloat16_One();
	g_ResolvedCategorySettings[m_Index].EnvironmentalLoudness.SetFloat16_Zero();
	g_ResolvedCategorySettings[m_Index].LPFCutoff = kVoiceFilterLPFMaxCutoff;
	g_ResolvedCategorySettings[m_Index].HPFCutoff = 0;
	g_ResolvedCategorySettings[m_Index].UnderwaterWetLevel.SetFloat16_Zero();
	g_ResolvedCategorySettings[m_Index].StonedWetLevel.SetFloat16_One();

	Assign(m_ParentIndex, audCategoryManager::GetCategoryIndex(parent));
	// Clear our list of child catagories
	for (s32 i=0; i < m_ChildCategoryIndices.GetMaxCount(); i++)
	{
		m_ChildCategoryIndices[i] = -1;
	}

	if (categoryNameHash==0)
	{
		Assert(0);
		return false;
	}
	
	return true;
}

bool audCategory::InitDynamic(u32 categoryNameHash, audMetadataManager* metadataManager, audCategory* parent, const MixGroupCategoryMap * map)
{
#if __BANK
	m_IsMuted = false;
#endif

	if(!InitCore(categoryNameHash, parent, metadataManager)) 
	{
		return false;
	}

	InitMetadataDynamic();
	
	// Now see how many children we have, and instantiate all of them
	u32 childCatIndex = 0;
	for (int i=0; i<map->numEntrys; i++)
	{
		int childHierarchyIndex = i;
		if(childHierarchyIndex != -1 && categoryNameHash == map->Entry[childHierarchyIndex].ParentHash)
		{
			m_ChildCategoryIndices[childCatIndex++] = (s16)g_AudioEngine.GetCategoryManager().InitDynamicCategory(map->Entry[i].CategoryHash, this, map);
		}
	}

	return true;
}

bool audCategory::Init(u32 categoryNameHash, audMetadataManager* metadataManager, audCategory* parent)
{
#if __BANK
	m_IsMuted = false;
#endif

	if(!InitCore(categoryNameHash, parent, metadataManager))
	{
		return false;
	}

	PullMetadataValues(m_Metadata);

	// Now see how many children we have, and instantiate all of them
	audAssertf(m_Metadata->numCategoryRefs < m_ChildCategoryIndices.GetMaxCount(), "Category %s has %u children; max is %u", metadataManager->GetObjectNameFromNameTableOffset(m_Metadata->NameTableOffset), m_Metadata->numCategoryRefs, m_ChildCategoryIndices.GetMaxCount());
	for (u32 i=0; i < m_Metadata->numCategoryRefs; i++)
	{
		const Category *childMetadata = GetChildCategory(metadataManager, metadataManager->GetObjectMetadataRefFromHash(m_Metadata->CategoryRef[i].CategoryId));
		if (!childMetadata)
		{
			return false;
		}
		Assign(m_ChildCategoryIndices[i], audCategoryManager::GetCategoryIndex(g_AudioEngine.GetCategoryManager().InitCategory(m_Metadata->CategoryRef[i].CategoryId, this)));
	}

	return true;
}

void audCategory::SetParentOverrides(bool override)
{
	m_ParentOverrides.BitFields.VolumeOverridesParent = override;
	m_ParentOverrides.BitFields.PitchOverridesParent = override;
	m_ParentOverrides.BitFields.LPFCutoffOverridesParent = override;
	m_ParentOverrides.BitFields.HPFCutoffOverridesParent = override;
	m_ParentOverrides.BitFields.DistanceRollOffScaleOverridesParent = override;
	m_ParentOverrides.BitFields.PlateauRollOffScaleOverridesParent = override;
	m_ParentOverrides.BitFields.OcclusionDampingOverridesParent = override;
	m_ParentOverrides.BitFields.EnvironmentalFilterDampingOverridesParent = override;
	m_ParentOverrides.BitFields.SourceReverbDampingOverridesParent = override;
	m_ParentOverrides.BitFields.DistanceReverbDampingOverridesParent = override;
	m_ParentOverrides.BitFields.InteriorReverbDampingOverridesParent = override;
	m_ParentOverrides.BitFields.EnvironmentalLoudnessOverridesParent = override;
	m_ParentOverrides.BitFields.UnderwaterWetLevelOverridesParent = override;
	m_ParentOverrides.BitFields.StonedWetLevelOverridesParent = override;
}

const Category *audCategory::GetChildCategory(const audMetadataManager *metadataManager, const audMetadataRef childRef)
{
	return metadataManager->GetObject<Category>(childRef);
}

#if AUD_ATOMIC_CATEGORIES

void audCategory::CommitCategorySettings(u16 currentIndex, u16 nextIndex)
{
	// Copy the current settings over to the next one.

	// If Categories held anything clever, like references, we'd need a smarter copy, but for now can copy the struct.
	m_CategorySettings[nextIndex] = m_CategorySettings[currentIndex];

	// Now do the same on all our children
	for (int i=0; i<AUD_MAX_CHILD_CATEGORIES; i++)
	{
		if (m_ChildCategoryIndices[i] != -1)
		{
			audCategoryManager::GetCategoryFromIndex(m_ChildCategoryIndices[i])->CommitCategorySettings(currentIndex, nextIndex);
		}
	}
}
#endif // AUD_ATOMIC_CATEGORIES


void audCategory::PullMetadataValues(Category *metadata)
{
	//We don't pull metadata on dynamic categories
	if(m_Index >= (int)g_AudioEngine.GetCategoryManager().GetNumberOfStaticCategories())
	{
		return;
	}

	m_MetadataSettings.Volume.SetFloat16_FromFloat32((float)metadata->Volume * 0.01f);
	m_MetadataSettings.DistanceRollOffScale.SetFloat16_FromFloat32((float)metadata->DistanceRollOffScale * 0.01f);
	m_MetadataSettings.PlateauRollOffScale.SetFloat16_FromFloat32((float)metadata->PlateauRollOffScale * 0.01f);
	m_MetadataSettings.EnvironmentalFilterDamping.SetFloat16_FromFloat32((f32)metadata->EnvironmentalFilterDamping * 0.01f);
	m_MetadataSettings.SourceReverbDamping.SetFloat16_FromFloat32((f32)metadata->SourceReverbDamping * 0.01f);
	m_MetadataSettings.DistanceReverbDamping.SetFloat16_FromFloat32((f32)metadata->DistanceReverbDamping * 0.01f);
	m_MetadataSettings.InteriorReverbDamping.SetFloat16_FromFloat32((f32)metadata->InteriorReverbDamping * 0.01f);
	m_MetadataSettings.EnvironmentalLoudness.SetFloat16_FromFloat32((f32)metadata->EnvironmentalLoudness * 0.01f);
	m_MetadataSettings.OcclusionDamping.SetFloat16_FromFloat32((f32)metadata->OcclusionDamping * 0.01f);
	m_MetadataSettings.Pitch = metadata->Pitch;
	m_MetadataSettings.UnderwaterWetLevel.SetFloat16_FromFloat32((f32)metadata->UnderwaterWetLevel * 0.01f);
	m_MetadataSettings.StonedWetLevel.SetFloat16_FromFloat32((f32)metadata->StonedWetLevel * 0.01f);
	m_MetadataSettings.LPFCutoff = metadata->LPFCutoff;
	m_MetadataSettings.HPFCutoff = metadata->HPFCutoff;
	m_ParentOverrides = metadata->ParentOverrides;
	m_Flags = metadata->Flags;

	m_TimerId = (SoundTimer)metadata->Timer;

	m_HPFDistanceCurve = metadata->HPFDistanceCurve;
	m_LPFDistanceCurve = metadata->LPFDistanceCurve;

#if __BANK

	const u32 nameHash = atStringHash(GetNameString());
	const audMetadataManager &metadataManager = g_AudioEngine.GetCategoryManager().GetMetadataManager();

	const bool isSolod = (AUD_GET_TRISTATE_VALUE(m_Flags, FLAG_ID_CATEGORY_SOLO)==AUD_TRISTATE_TRUE) || metadataManager.IsObjectSoloed(nameHash);
	const bool isMuted = (AUD_GET_TRISTATE_VALUE(m_Flags, FLAG_ID_CATEGORY_MUTE)==AUD_TRISTATE_TRUE) || metadataManager.IsObjectMuted(nameHash);
	
	bool isParentSolod = false;
	bool isParentMuted = false;
	s16 parentIndex = m_ParentIndex;
	// search up until we find a parent that is muted
	while(parentIndex != -1 && (!isParentMuted))
	{
		const audCategory *parent = g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(parentIndex);
		const u32 parentNameHash = atStringHash(parent->GetNameString());
		if(AUD_GET_TRISTATE_VALUE(parent->m_Flags, FLAG_ID_CATEGORY_SOLO)==AUD_TRISTATE_TRUE || metadataManager.IsObjectSoloed(parentNameHash))
		{
			isParentSolod = true;
		}
		else if(AUD_GET_TRISTATE_VALUE(parent->m_Flags, FLAG_ID_CATEGORY_MUTE)==AUD_TRISTATE_TRUE || metadataManager.IsObjectMuted(parentNameHash))
		{
			isParentMuted = true;
		}
		parentIndex = parent->m_ParentIndex;
	}

	const bool soloActive = g_AudioEngine.GetCategoryManager().IsSoloActive() || metadataManager.AreAnyObjectsSoloed();
	if(soloActive && (!isSolod && !isParentSolod))
	{
		m_IsMuted = true;
	}
	else if(isMuted || (isParentMuted && !isSolod && !isParentSolod))
	{
		m_IsMuted = true;
	}
	else
	{
		m_IsMuted = false;
	}
#endif

}

void audCategory::InitMetadataDynamic()
{

	m_MetadataSettings.LPFCutoff = 0;
	m_MetadataSettings.HPFCutoff = 0;
	SetParentOverrides(false);
	//m_Flags = 0;

	m_MetadataSettings.Volume.SetFloat16_Zero();
	m_MetadataSettings.Pitch = 0;
	m_MetadataSettings.DistanceRollOffScale.SetFloat16_Zero();
	m_MetadataSettings.OcclusionDamping.SetFloat16_Zero();
	m_MetadataSettings.EnvironmentalFilterDamping.SetFloat16_Zero();
	m_MetadataSettings.SourceReverbDamping.SetFloat16_Zero();
	m_MetadataSettings.DistanceReverbDamping.SetFloat16_Zero();
	m_MetadataSettings.InteriorReverbDamping.SetFloat16_Zero();
	m_MetadataSettings.EnvironmentalLoudness.SetFloat16_Zero();
	m_MetadataSettings.LPFCutoff = 0;
	m_MetadataSettings.HPFCutoff = 0;
	m_MetadataSettings.UnderwaterWetLevel.SetFloat16_Zero();
	m_MetadataSettings.StonedWetLevel.SetFloat16_Zero();

}


void audCategory::SetVolume(float volume)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].Volume.SetFloat16_FromFloat32(volume);
#else
	m_CategorySettings.Volume.SetFloat16_FromFloat32(volume);
#endif
}

float audCategory::GetRequestedVolume() const
{
#if AUD_ATOMIC_CATEGORIES
	return m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].Volume.GetFloat32_FromFloat16();
#else
	return m_CategorySettings.Volume.GetFloat32_FromFloat16();
#endif
}

void audCategory::SetPitch(s32 pitch)
{
#if AUD_ATOMIC_CATEGORIES
	Assign(m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].Pitch, pitch);
#else
	Assign(m_CategorySettings.Pitch, pitch);
#endif
}

s32 audCategory::GetRequestedPitch() const
{
#if AUD_ATOMIC_CATEGORIES
	return m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].Pitch;
#else
	return m_CategorySettings.Pitch;
#endif
}

void audCategory::SetLPFCutoff(u32 cutoff)
{
#if AUD_ATOMIC_CATEGORIES
	Assign(m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].LPFCutoff, cutoff);
#else
	Assign(m_CategorySettings.LPFCutoff, cutoff);
#endif
}

u32 audCategory::GetRequestedLPFCutoff() const
{
#if AUD_ATOMIC_CATEGORIES
	return m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].LPFCutoff;
#else
	return m_CategorySettings.LPFCutoff;
#endif
}

void audCategory::SetHPFCutoff(u32 cutoff)
{
#if AUD_ATOMIC_CATEGORIES
	Assign(m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].HPFCutoff, cutoff);
#else
	Assign(m_CategorySettings.HPFCutoff, cutoff);
#endif
}

u32 audCategory::GetRequestedHPFCutoff() const
{
#if AUD_ATOMIC_CATEGORIES
	return m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].HPFCutoff;
#else
	return m_CategorySettings.HPFCutoff;
#endif
}

void audCategory::SetFilterCutoff(u16 filterCutoff)
{
	SetLPFCutoff(filterCutoff);
}

void audCategory::SetDistanceRollOffScale(float distanceRollOffScale)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].DistanceRollOffScale.SetFloat16_FromFloat32(distanceRollOffScale);
#else
	m_CategorySettings.DistanceRollOffScale.SetFloat16_FromFloat32(distanceRollOffScale);
#endif
}

void audCategory::SetOcclusionDamping(float occlusionDamping)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].OcclusionDamping.SetFloat16_FromFloat32(occlusionDamping);
#else
	m_CategorySettings.OcclusionDamping.SetFloat16_FromFloat32(occlusionDamping);
#endif
}

void audCategory::SetEnvironmentalFilterDamping(float environmentalFilterDamping)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].EnvironmentalFilterDamping.SetFloat16_FromFloat32(environmentalFilterDamping);
#else
	m_CategorySettings.EnvironmentalFilterDamping.SetFloat16_FromFloat32(environmentalFilterDamping);
#endif
}

void audCategory::SetDistanceReverbDamping(float environmentalReverbDamping)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].DistanceReverbDamping.SetFloat16_FromFloat32(environmentalReverbDamping);
#else
	m_CategorySettings.DistanceReverbDamping.SetFloat16_FromFloat32(environmentalReverbDamping);	
#endif
}

void audCategory::SetSourceReverbDamping(float environmentalReverbDamping)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].SourceReverbDamping.SetFloat16_FromFloat32(environmentalReverbDamping);
#else
	m_CategorySettings.SourceReverbDamping.SetFloat16_FromFloat32(environmentalReverbDamping);
#endif
}

void audCategory::SetInteriorReverbDamping(float interiorReverbDamping)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].InteriorReverbDamping.SetFloat16_FromFloat32(interiorReverbDamping);
#else
	m_CategorySettings.InteriorReverbDamping.SetFloat16_FromFloat32(interiorReverbDamping);	
#endif
}


void audCategory::SetEnvironmentalLoudness(float environmentalLoudness)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].EnvironmentalLoudness.SetFloat16_FromFloat32(environmentalLoudness);
#else
	m_CategorySettings.EnvironmentalLoudness.SetFloat16_FromFloat32(environmentalLoudness);
#endif
}

void audCategory::SetUnderwaterWetLevel(float wetLevel)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].UnderwaterWetLevel.SetFloat16_FromFloat32(wetLevel);
#else
	m_CategorySettings.UnderwaterWetLevel.SetFloat16_FromFloat32(wetLevel);
#endif
}

void audCategory::SetStonedWetLevel(float wetLevel)
{
#if AUD_ATOMIC_CATEGORIES
	m_CategorySettings[g_AudioEngine.GetCategoryManager().GetGameThreadCategoryIndex()].StonedWetLevel.SetFloat16_FromFloat32(wetLevel);
#else
	m_CategorySettings.StonedWetLevel.SetFloat16_FromFloat32(wetLevel);
#endif
}

bool audCategory::GetMuteOnUserMusic()
{
	TristateValue val = AUD_GET_TRISTATE_VALUE(m_Flags, FLAG_ID_CATEGORY_MUTEONUSERMUSIC);

	if(val == AUD_TRISTATE_UNSPECIFIED)
	{
		if(m_ParentIndex != -1 && audCategoryManager::GetCategoryFromIndex(m_ParentIndex)->GetMuteOnUserMusic())
		{
			return true;
		}

		return false;
	}
	else return (val == AUD_TRISTATE_TRUE);
}

u32 audCategory::GetTimerId() const
{
	u32 val = m_TimerId;

	if(val == kSoundTimerUnspecified)
	{
		if(m_ParentIndex != -1)
		{
			return audCategoryManager::GetCategoryFromIndex(m_ParentIndex)->GetTimerId();
		}

		return kSoundTimerNormal;
	}
	else if(val > kSoundTimerUnspecified)
	{
		// For timers added after, subtract 1 to avoid having to bump schema
		return val - 1;
	}
	else return val;
}

bool audCategory::GetRearAttenuationDisabled()
{
	TristateValue val = AUD_GET_TRISTATE_VALUE(m_Flags, FLAG_ID_CATEGORY_DISABLEREARATTEN);

	if(val == AUD_TRISTATE_UNSPECIFIED)
	{
		if(m_ParentIndex != -1 && audCategoryManager::GetCategoryFromIndex(m_ParentIndex)->GetRearAttenuationDisabled())
		{
			return true;
		}

		return false;
	}
	else return (val == AUD_TRISTATE_TRUE);
}

u32 audCategory::GetNumChildren() const
{
	return m_Metadata->numCategoryRefs;
}

audCategory* audCategory::GetChildByIndex(const u32 catIndex) const
{
	Assert(catIndex < GetNumChildren());
	return g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(m_ChildCategoryIndices[catIndex]);
}

#if __BANK

const char* audCategory::GetNameString() const
{
	return g_AudioEngine.GetCategoryManager().GetMetadataManager().GetObjectNameFromNameTableOffset(m_Metadata->NameTableOffset);
}

void audCategory::PullMetadataValues()
{
	// Our metadata may have been overridden; deal with this by updating our pointer
	m_Metadata = g_AudioEngine.GetCategoryManager().GetMetadataManager().GetObject<Category>(GetNameString());

	PullMetadataValues(m_Metadata);
	for (int i=0; i < m_ChildCategoryIndices.GetMaxCount(); i++)
	{
		if (m_ChildCategoryIndices[i] != -1)
		{
			audCategoryManager::GetCategoryFromIndex(m_ChildCategoryIndices[i])->PullMetadataValues();
		}
	}	
}

bool audCategory::IsSoloActive() const
{
	Assert(m_Metadata);

	if (!PARAM_audiodesigner.Get())
		return false;

	if(AUD_GET_TRISTATE_VALUE(m_Metadata->Flags, FLAG_ID_CATEGORY_SOLO) == AUD_TRISTATE_TRUE)
	{
		return true;
	}
	const u32 nameHash = atStringHash(GetNameString());
	const audMetadataManager &metadataManager = g_AudioEngine.GetCategoryManager().GetMetadataManager();

	if(metadataManager.IsObjectSoloed(nameHash))
	{
		return true;
	}
	for (int i=0; i < m_ChildCategoryIndices.GetMaxCount(); i++)
	{
		if (m_ChildCategoryIndices[i] != -1 && audCategoryManager::GetCategoryFromIndex(m_ChildCategoryIndices[i])->IsSoloActive())
		{
			return true;
		}
	}
	return false;
}

bool audCategory::IsMuteActive() const
{
	if(m_IsMuted)
	{
		return true;
	}
	for (int i=0; i < m_ChildCategoryIndices.GetMaxCount(); i++)
	{
		if (m_ChildCategoryIndices[i] != -1 && audCategoryManager::GetCategoryFromIndex(m_ChildCategoryIndices[i])->IsMuteActive())
		{
			return true;
		}
	}
	return false;
}
#endif

#endif // !__SPU

audCategory* audCategory::GetParent() const
{
	return g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(m_ParentIndex);
}

#if __BANK
int g_DebugCategoryIndex = -1;
#endif

void audCategory::AudioThreadUpdate(u16 audioThreadIndex, bool useMetaDeta)
{
	// Work out this frame's combined settings for each parameter, and then call any children to do the same.
	bool parent = (m_ParentIndex != -1);

#if AUD_ATOMIC_CATEGORIES
	tCategorySettings &settings = m_CategorySettings[audioThreadIndex];
#else
	tCategorySettings &settings = m_CategorySettings;
#endif

#if __BANK
	if(m_Index == g_DebugCategoryIndex)
	{
		Assert(0);
	}
#endif

	// Volume
	float volume = settings.Volume.GetFloat32_FromFloat16() + (useMetaDeta ? m_MetadataSettings.Volume.GetFloat32_FromFloat16(): 0.f);
	if(parent && !m_ParentOverrides.BitFields.VolumeOverridesParent)
	{
		volume += g_ResolvedCategorySettings[m_ParentIndex].Volume.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].Volume.SetFloat16_FromFloat32(volume);

	// Pitch
	s16 pitch = settings.Pitch + (useMetaDeta ? m_MetadataSettings.Pitch : 0);
	if(parent && !m_ParentOverrides.BitFields.PitchOverridesParent)
	{
		pitch = pitch + g_ResolvedCategorySettings[m_ParentIndex].Pitch;
	}
	g_ResolvedCategorySettings[m_Index].Pitch = pitch;

	// FilterCutoff - LPF
	// use the most restrictive filter setting (lowest Fc)
	u32 filterCutoff = settings.LPFCutoff;
	filterCutoff = Min(filterCutoff, (useMetaDeta ? (u32)m_MetadataSettings.LPFCutoff: 23900));

	if(parent && !m_ParentOverrides.BitFields.LPFCutoffOverridesParent)
	{
		filterCutoff = Min(filterCutoff, (u32)g_ResolvedCategorySettings[m_ParentIndex].LPFCutoff);
	}
	Assign(g_ResolvedCategorySettings[m_Index].LPFCutoff, filterCutoff);
	// HPF
	// use the most restrictive filter setting (highest Fc)
	filterCutoff = settings.HPFCutoff;
	filterCutoff = Max(filterCutoff, (useMetaDeta ? (u32)m_MetadataSettings.HPFCutoff: 0));

	if(parent && !m_ParentOverrides.BitFields.HPFCutoffOverridesParent)
	{
		filterCutoff = Max(filterCutoff, (u32)g_ResolvedCategorySettings[m_ParentIndex].HPFCutoff);
	}
	Assign(g_ResolvedCategorySettings[m_Index].HPFCutoff, filterCutoff);

	// DistanceRollOff
	float distanceRollOffScale = settings.DistanceRollOffScale.GetFloat32_FromFloat16();
	distanceRollOffScale *=(useMetaDeta ?  m_MetadataSettings.DistanceRollOffScale.GetFloat32_FromFloat16(): 1.f);

	if(parent && !m_ParentOverrides.BitFields.DistanceRollOffScaleOverridesParent)
	{
		distanceRollOffScale *= g_ResolvedCategorySettings[m_ParentIndex].DistanceRollOffScale.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].DistanceRollOffScale.SetFloat16_FromFloat32(distanceRollOffScale);

	// PlateauRollOff
	float plateauRollOffScale = settings.PlateauRollOffScale.GetFloat32_FromFloat16();
	plateauRollOffScale *= (useMetaDeta ?  m_MetadataSettings.PlateauRollOffScale.GetFloat32_FromFloat16(): 1.f);

	if(parent && !m_ParentOverrides.BitFields.PlateauRollOffScaleOverridesParent)
	{
		plateauRollOffScale *= g_ResolvedCategorySettings[m_ParentIndex].PlateauRollOffScale.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].PlateauRollOffScale.SetFloat16_FromFloat32(plateauRollOffScale);

	// OcclusionDamping
	float occlusionDamping = settings.OcclusionDamping.GetFloat32_FromFloat16();
	occlusionDamping *= (useMetaDeta ?  m_MetadataSettings.OcclusionDamping.GetFloat32_FromFloat16(): 1.f);

	if(parent && !m_ParentOverrides.BitFields.OcclusionDampingOverridesParent)
	{
		occlusionDamping *= g_ResolvedCategorySettings[m_ParentIndex].OcclusionDamping.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].OcclusionDamping.SetFloat16_FromFloat32(occlusionDamping);

	// EnvironmentFilterDamping
	float environmentalFilterDamping = settings.EnvironmentalFilterDamping.GetFloat32_FromFloat16();
	environmentalFilterDamping *=(useMetaDeta ?  m_MetadataSettings.EnvironmentalFilterDamping.GetFloat32_FromFloat16(): 1.f); 

	if(parent && !m_ParentOverrides.BitFields.EnvironmentalFilterDampingOverridesParent)
	{
		environmentalFilterDamping *= g_ResolvedCategorySettings[m_ParentIndex].EnvironmentalFilterDamping.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].EnvironmentalFilterDamping.SetFloat16_FromFloat32(environmentalFilterDamping);

	// SourceReverbDamping
	f32 environmentalReverbDamping = settings.SourceReverbDamping.GetFloat32_FromFloat16();
	environmentalReverbDamping *= (useMetaDeta ?  m_MetadataSettings.SourceReverbDamping.GetFloat32_FromFloat16(): 1.f); 

	if(parent && !m_ParentOverrides.BitFields.SourceReverbDampingOverridesParent)
	{
		environmentalReverbDamping *= g_ResolvedCategorySettings[m_ParentIndex].SourceReverbDamping.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].SourceReverbDamping.SetFloat16_FromFloat32(environmentalReverbDamping);

	// DistanceReverbDamping
	environmentalReverbDamping = settings.DistanceReverbDamping.GetFloat32_FromFloat16();
	environmentalReverbDamping *= (useMetaDeta ?  m_MetadataSettings.DistanceReverbDamping.GetFloat32_FromFloat16(): 1.f); 

	if(parent && !m_ParentOverrides.BitFields.DistanceReverbDampingOverridesParent)
	{
		environmentalReverbDamping *= g_ResolvedCategorySettings[m_ParentIndex].DistanceReverbDamping.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].DistanceReverbDamping.SetFloat16_FromFloat32(environmentalReverbDamping);

	// InteriorReverbDamping
	f32 interiorReverbDamping = settings.InteriorReverbDamping.GetFloat32_FromFloat16();
	interiorReverbDamping *= (useMetaDeta ?  m_MetadataSettings.InteriorReverbDamping.GetFloat32_FromFloat16(): 1.f); 

	if(parent && !m_ParentOverrides.BitFields.InteriorReverbDampingOverridesParent)
	{
		interiorReverbDamping *= g_ResolvedCategorySettings[m_ParentIndex].InteriorReverbDamping.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].InteriorReverbDamping.SetFloat16_FromFloat32(interiorReverbDamping);

	// EnvironmentLoudness
	float environmentalLoudness = settings.EnvironmentalLoudness.GetFloat32_FromFloat16();
	environmentalLoudness *= (useMetaDeta ?  m_MetadataSettings.EnvironmentalLoudness.GetFloat32_FromFloat16(): 1.f); 

	if(parent && !m_ParentOverrides.BitFields.EnvironmentalLoudnessOverridesParent)
	{
		environmentalLoudness *= g_ResolvedCategorySettings[m_ParentIndex].EnvironmentalLoudness.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].EnvironmentalLoudness.SetFloat16_FromFloat32(environmentalLoudness);

	// UnderwaterWetLevel
	float underwaterWetLevel = settings.UnderwaterWetLevel.GetFloat32_FromFloat16();
	underwaterWetLevel *= (useMetaDeta ?  m_MetadataSettings.UnderwaterWetLevel.GetFloat32_FromFloat16() : 1.f); 

	if(parent && !m_ParentOverrides.BitFields.UnderwaterWetLevelOverridesParent)
	{
		underwaterWetLevel *= g_ResolvedCategorySettings[m_ParentIndex].UnderwaterWetLevel.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].UnderwaterWetLevel.SetFloat16_FromFloat32(underwaterWetLevel);

	// StonedWetLevel
	float stonedWetLevel = settings.StonedWetLevel.GetFloat32_FromFloat16();
	stonedWetLevel *= (useMetaDeta ?  m_MetadataSettings.StonedWetLevel.GetFloat32_FromFloat16() : 1.f); 

	if(parent && !m_ParentOverrides.BitFields.StonedWetLevelOverridesParent)
	{
		stonedWetLevel *= g_ResolvedCategorySettings[m_ParentIndex].StonedWetLevel.GetFloat32_FromFloat16();
	}
	g_ResolvedCategorySettings[m_Index].StonedWetLevel.SetFloat16_FromFloat32(stonedWetLevel);

	// Now call any children to get calculate their combined settings
	for (int i=0; i < m_ChildCategoryIndices.GetMaxCount(); i++)
	{
		if (m_ChildCategoryIndices[i] != -1)
		{
			if(audVerifyf(audCategoryManager::GetCategoryFromIndex(m_ChildCategoryIndices[i]), "Couldn't get child category at index %d, for  category %s in AudioThreadUpdate", i, GetNameString()))
			{
#if __BANK
				m_RecursionDebugIndex = i;
#endif
				audCategoryManager::GetCategoryFromIndex(m_ChildCategoryIndices[i])->AudioThreadUpdate(audioThreadIndex, useMetaDeta);
			}
			else
			{
				Errorf("Couldn't get child category at index %d, for  category index %d in AudioThreadUpdate", i, m_Index);
			}
		}
	}

#if __BANK
	// finally override all that with mute settings
	// NOTE: do this last as we don't want it combined with children volumes
	if(m_IsMuted)
	{
		g_ResolvedCategorySettings[m_Index].Volume.SetFloat16_FromFloat32(-100.f);
	}
#endif
}

} // namespace rage
