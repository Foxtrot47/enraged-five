//
// audioengine/curve.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "curve.h"
#include "curvedefs.h"

#if !__SPU
#include "curverepository.h"
#include "engine.h"
#include "metadatamanager.h"
#endif
 
#include "audiohardware/channel.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/driverutil.h"
#include "math/amath.h"
#include "phcore/conversion.h"

#ifndef AUD_LIMITED_CURVES
#define AUD_OPT_CURVE_SUPPORT 1
#else
#define AUD_OPT_CURVE_SUPPORT 0
#endif

#ifndef AUD_CONSTANT_CURVE
#define AUD_CONSTANT_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_LINEAR_CURVE
#define AUD_LINEAR_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_LINEARDB_CURVE
#define AUD_LINEARDB_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_PIECEWISE_LINEAR_CURVE
#define AUD_PIECEWISE_LINEAR_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_PIECEWISE_LINEAR_CURVE
#define AUD_PIECEWISE_LINEAR_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_EQUAL_POWER_CURVE
#define AUD_EQUAL_POWER_CURVE AUD_OPT_CURVE_SUPPORT
#endif


#ifndef AUD_VALUETABLE_CURVE
#define AUD_VALUETABLE_CURVE AUD_OPT_CURVE_SUPPORT
#endif


#ifndef AUD_EXPONENTIAL_CURVE
#define AUD_EXPONENTIAL_CURVE AUD_OPT_CURVE_SUPPORT
#endif


#ifndef AUD_DECAYING_EXPONENTIAL_CURVE
#define AUD_DECAYING_EXPONENTIAL_CURVE AUD_OPT_CURVE_SUPPORT
#endif


#ifndef AUD_DECAYING_SQUARED_EXPONENTIAL_CURVE
#define AUD_DECAYING_SQUARED_EXPONENTIAL_CURVE AUD_OPT_CURVE_SUPPORT
#endif


#ifndef AUD_SINE_CURVE
#define AUD_SINE_CURVE AUD_OPT_CURVE_SUPPORT
#endif


#ifndef AUD_ONE_OVER_X_CURVE
#define AUD_ONE_OVER_X_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_ONE_OVER_X_SQUARED_CURVE
#define AUD_ONE_OVER_X_SQUARED_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_DEFAULT_DISTANCE_ATTENUATION_CURVE
#define AUD_DEFAULT_DISTANCE_ATTENUATION_CURVE AUD_OPT_CURVE_SUPPORT
#endif

#ifndef AUD_DEFAULT_DISTANCE_ATTENUATION_CLAMPED_CURVE
#define AUD_DEFAULT_DISTANCE_ATTENUATION_CLAMPED_CURVE AUD_OPT_CURVE_SUPPORT
#endif

namespace rage {

BANK_ONLY(bool g_WarnOnMissingCurves = false);
BANK_ONLY(bool g_FullCurveTweakAbility = false);

audCurve::audCurve()
{
#if __DEV
	m_NameHash = 0;
#endif

	m_Metadata = NULL;
	m_IsValidInSoundTask = false;
}
#if !__SPU

bool audCurve::Init(const char* curveName)
{
	const bool success = Init(atStringHash(curveName));
#if __BANK
	if(g_WarnOnMissingCurves && !success)
	{
		audWarningf("Missing curve %s", curveName);
	}
#endif
	return success;
}

bool audCurve::Init(u32 curveNameHash)
{
#if __BANK
	m_NameHash = curveNameHash;
#endif
	if (curveNameHash==0)
	{
		m_Metadata = NULL;
		return false;
	}

	bool success = g_AudioEngine.GetCurveRepository().GetCurveMetadataPtr(curveNameHash, (void**)&m_Metadata);
	// all curve metadata should be 4byte aligned
	//audAssert(((size_t)m_Metadata) % 4 == 0);

	if (success)
	{
		m_IsValidInSoundTask = false;

		m_MinInput = m_Metadata->MinInput;
		m_MaxInput = m_Metadata->MaxInput;
		m_ShouldClampInput = (AUD_GET_TRISTATE_VALUE(m_Metadata->Flags, FLAG_ID_BASECURVE_CLAMPINPUT) == AUD_TRISTATE_TRUE);
		m_CurveType = m_Metadata->ClassID;

		switch(m_CurveType)
		{
#if AUD_CONSTANT_CURVE
		case Constant::TYPE_ID:
				return Constant_Init();
#endif
#if AUD_LINEAR_CURVE
		case Linear::TYPE_ID:
				return Linear_Init();
#endif
#if AUD_LINEARDB_CURVE
		case LinearDb::TYPE_ID:
				return LinearDb_Init();
#endif
#if AUD_PIECEWISE_LINEAR_CURVE
		case PiecewiseLinear::TYPE_ID:
				return PiecewiseLinear_Init();
#endif
#if AUD_EQUAL_POWER_CURVE
		case EqualPower::TYPE_ID:
				return EqualPower_Init();
#endif
#if AUD_VALUETABLE_CURVE
		case ValueTable::TYPE_ID:
				return ValueTable_Init();
#endif
#if AUD_EXPONENTIAL_CURVE
		case Exponential::TYPE_ID:
				return Exponential_Init();
#endif
#if AUD_DECAYING_EXPONENTIAL_CURVE
		case DecayingExponential::TYPE_ID:
				return DecayingExponential_Init();
#endif
#if AUD_DECAYING_SQUARED_EXPONENTIAL_CURVE
		case DecayingSquaredExponential::TYPE_ID:
				return DecayingSquaredExponential_Init();
#endif
#if AUD_SINE_CURVE
		case SineCurve::TYPE_ID:
				return SineCurve_Init();
#endif
#if AUD_ONE_OVER_X_CURVE
		case OneOverX::TYPE_ID:
				return OneOverX_Init();
#endif
#if AUD_ONE_OVER_X_SQUARED_CURVE
		case OneOverXSquared::TYPE_ID:
				return OneOverXSquared_Init();
#endif
#if AUD_DEFAULT_DISTANCE_ATTENUATION_CURVE
		case DefaultDistanceAttenuation::TYPE_ID:
				return DefaultDistanceAttenuation_Init();
#endif
#if AUD_DEFAULT_DISTANCE_ATTENUATION_CLAMPED_CURVE
		case DefaultDistanceAttenuationClamped::TYPE_ID:
				return DefaultDistanceAttenuationClamped_Init();
#endif
			default:
				return false;
		}
	}
	else
	{
		m_Metadata = NULL;
		return false;
	}
}

#endif

F32_VerifyFinite audCurve::GetMetadataMaxInput() const
{	
	return m_MaxInput;	
}

F32_VerifyFinite audCurve::GetMetadataMinInput() const
{
	return m_MinInput;
}

F32_VerifyFinite audCurve::CalculateValue(F32_VerifyFinite input) const
{
	// If we're in a debug build, regenerate the metadata and function pointers, as they might have changed.
#if !__SPU
#if __BANK
	if(g_FullCurveTweakAbility)
	{
		if (m_NameHash)
		{
			const_cast<audCurve*>(this)->Init(m_NameHash);
		}
	}
#endif
#else
	// On SPU we can only evaluate if m_IsValidInSoundTask is true
	audAssertf(m_IsValidInSoundTask, "CurveType: %u metadata: %p nameHash: %u", m_CurveType, m_Metadata, m_NameHash);
#endif

	// See if we have valid function and metadata pointers, and call the appropriate thing
	if (m_Metadata)
	{
		// piecewise linear curves are by far the most common so its worth an explicit check before the switch
		if(m_CurveType == PiecewiseLinear::TYPE_ID)
		{
			if(m_IsValidInSoundTask)
			{
				// clamp input to whole curve range
				const float x = Clamp((float)input, m_MinInput, m_MaxInput);

				// compute both line segments
				const float y1 = m_Coefficients[0]*x + m_Coefficients[1];
				const float y2 = m_Coefficients[3]*x + m_Coefficients[4];

				// If x is greater than x2, return the second segment otherwise the first
				return Selectf(x - m_Coefficients[2], y2, y1);
			}
			return PiecewiseLinear_CalculateValue(input);
		}
		else
		{
			// no point clamping for piecewise linear so lets save a branch in the common case
			if(ShouldClampInput())
			{
				input = Clamp(input, GetMetadataMinInput(), GetMetadataMaxInput());
			}

			switch(m_CurveType)
			{
#if AUD_CONSTANT_CURVE
			case Constant::TYPE_ID:
				return Constant_CalculateValue(input);
#endif

#if AUD_LINEAR_CURVE
			case Linear::TYPE_ID:
				return Linear_CalculateValue(input);
#endif

#if AUD_LINEARDB_CURVE
			case LinearDb::TYPE_ID:
				return LinearDb_CalculateValue(input);
#endif

#if AUD_EQUAL_POWER_CURVE
			case EqualPower::TYPE_ID:
				return EqualPower_CalculateValue(input);
#endif

#if AUD_VALUETABLE_CURVE
			case ValueTable::TYPE_ID:
				return ValueTable_CalculateValue(input);
#endif


#if AUD_EXPONENTIAL_CURVE
			case Exponential::TYPE_ID:
				return Exponential_CalculateValue(input);
#endif

#if AUD_DECAYING_EXPONENTIAL_CURVE
			case DecayingExponential::TYPE_ID:
				return DecayingExponential_CalculateValue(input);
#endif

#if AUD_DECAYING_SQUARED_EXPONENTIAL_CURVE
			case DecayingSquaredExponential::TYPE_ID:
				return DecayingSquaredExponential_CalculateValue(input);
#endif

#if AUD_SINE_CURVE
			case SineCurve::TYPE_ID:
				return SineCurve_CalculateValue(input);
#endif


#if AUD_ONE_OVER_X_CURVE
			case OneOverX::TYPE_ID:
				return OneOverX_CalculateValue(input);
#endif

#if AUD_ONE_OVER_X_SQUARED_CURVE
			case OneOverXSquared::TYPE_ID:
				return OneOverXSquared_CalculateValue(input);
#endif


#if AUD_DEFAULT_DISTANCE_ATTENUATION_CURVE
			case DefaultDistanceAttenuation::TYPE_ID:
				return DefaultDistanceAttenuation_CalculateValue(input);
#endif

#if AUD_DEFAULT_DISTANCE_ATTENUATION_CLAMPED_CURVE
			case DefaultDistanceAttenuationClamped::TYPE_ID:
				return DefaultDistanceAttenuationClamped_CalculateValue(input);
#endif
			default: return 0.f;
			}
		}
	}
	else
	{
		return 0.0f;
	}
}

F32_VerifyFinite audCurve::CalculateRescaledValue(F32_VerifyFinite minOutput, F32_VerifyFinite maxOutput, F32_VerifyFinite minInput, F32_VerifyFinite maxInput, F32_VerifyFinite input) const
{
	f32 ratio = ClampRange (input, minInput, maxInput); // this puts it in range 0-1
	f32 output = CalculateValue(ratio);
	ClampRange(output, 0.0f, 1.0f);
	return (minOutput + ((output) * (maxOutput - minOutput)));
}

// ***************************************************************
// Curve Types
bool audCurve::Linear_Init()
{
	Linear *metadata = (Linear*)m_Metadata;

	m_IsValidInSoundTask = true;
	ComputeLine(metadata->LeftHandPair.x, metadata->LeftHandPair.y, metadata->RightHandPair.x, metadata->RightHandPair.y,
					m_Coefficients[0],m_Coefficients[1],m_Coefficients[2],m_Coefficients[3]);

	return true;
}

F32_VerifyFinite audCurve::Linear_CalculateValue(F32_VerifyFinite input) const
{
	const float x = Clamp((float)input, m_Coefficients[2], m_Coefficients[3]);
	return m_Coefficients[0]*x + m_Coefficients[1];
}

//Perform a standard linear interpolation between two points (see Linear_CalculateValue) and return the result in dBs.
//Enables a linear curve to be used as a roll-off curve.
bool audCurve::LinearDb_Init()
{
	return Linear_Init();
}

F32_VerifyFinite audCurve::LinearDb_CalculateValue(F32_VerifyFinite input) const
{
	f32 linearResult = Linear_CalculateValue(input);
	return audDriverUtil::ComputeDbVolumeFromLinear(linearResult);
}

bool audCurve::Constant_Init()
{
	m_Coefficients[0] = ((Constant*)m_Metadata)->Value;
	m_IsValidInSoundTask = true;
	return true;
}

F32_VerifyFinite audCurve::Constant_CalculateValue(F32_VerifyFinite UNUSED_PARAM(input)) const
{
	return m_Coefficients[0];

}

struct audPiecewisePoint
{
	f32 x;
	f32 y;
};

bool audCurve::PiecewiseLinear_Init()
{
	PiecewiseLinear* piecewiseLinearCurve = (PiecewiseLinear*)m_Metadata;
	if(piecewiseLinearCurve->numPoints <= 3)
	{
		m_IsValidInSoundTask = true;
		if(piecewiseLinearCurve->numPoints == 1)
		{
			m_Coefficients[0] = piecewiseLinearCurve->Point[0].y;
			m_CurveType = Constant::TYPE_ID;			
		}
		else if(piecewiseLinearCurve->numPoints == 2)
		{			
			ComputeLine(piecewiseLinearCurve->Point[0].x, piecewiseLinearCurve->Point[0].y,
						piecewiseLinearCurve->Point[1].x, piecewiseLinearCurve->Point[1].y,
						m_Coefficients[0],m_Coefficients[1],m_Coefficients[2],m_Coefficients[3]);			
			m_CurveType = Linear::TYPE_ID;
		}
		else
		{
			// Compute both line segments
			// p0 - p1
			ComputeLine(piecewiseLinearCurve->Point[0].x, piecewiseLinearCurve->Point[0].y, piecewiseLinearCurve->Point[1].x, piecewiseLinearCurve->Point[1].y,
											m_Coefficients[0], m_Coefficients[1], m_MinInput, m_Coefficients[2]);
			// p1 - p2
			float unused;
			ComputeLine(piecewiseLinearCurve->Point[1].x, piecewiseLinearCurve->Point[1].y, piecewiseLinearCurve->Point[2].x, piecewiseLinearCurve->Point[2].y,
											m_Coefficients[3], m_Coefficients[4], unused, m_MaxInput);
		}
	}
	else
	{
		m_IsValidInSoundTask = false;
	}
	return true;
}

F32_VerifyFinite audCurve::PiecewiseLinear_CalculateValue(F32_VerifyFinite input) const
{
	f32 fLinearRatio;
	f32 fOutput;
	u32 numPoints; 
	
	u32 i;
	
	PiecewiseLinear* piecewiseLinearCurve = (PiecewiseLinear*)m_Metadata;	
	numPoints = piecewiseLinearCurve->numPoints;
	PiecewiseLinear::tPoint* points = piecewiseLinearCurve->Point;
	input = Selectf(input - points[numPoints-1].x, points[numPoints-1].x, input);
	input = Selectf(points[0].x - input, points[0].x, input);

	// see which straight-line section we're in
	for (i=1; i<numPoints; i++)
	{
		if (points[i].x >= input)
			break;
	}

	fLinearRatio = ( (input-points[i-1].x) / (points[i].x-points[i-1].x) );

	fOutput = points[i-1].y + fLinearRatio * ( points[i].y - points[i-1].y );

	return (fOutput);
	
}

bool audCurve::EqualPower_Init()
{
	EqualPower *metadata = (EqualPower*)m_Metadata;

	m_Coefficients[0] = (metadata->Flip.BitFields.x?-1.0f:0.0f);
	m_Coefficients[1] = (metadata->Flip.BitFields.y?-1.0f:0.0f);

	m_IsValidInSoundTask = true;
	return true;
}

F32_VerifyFinite audCurve::EqualPower_CalculateValue(F32_VerifyFinite input) const
{
	// By default we have 0->1 maps to 0->1 and 0.5->0.707
	f32 flippedInput = 1.0f - input;
	// conditionally flip the input base on flags cached in Init()
	input = Selectf(m_Coefficients[0], input, flippedInput);

	// Get into the range 0-1
	input = Clamp<f32>(input, 0.0f, 1.0f);

	const float PI_over_2 = (PI / 2.0f);
	f32 output = sinf(input * PI_over_2);

	f32 flippedOutput = 1.0f - output;
	// conditionally flip output based on flip y flag
	return Selectf(m_Coefficients[1], output, flippedOutput);
}

bool audCurve::ValueTable_Init()
{
	m_IsValidInSoundTask = false;
	return true;
}

F32_VerifyFinite audCurve::ValueTable_CalculateValue(F32_VerifyFinite input) const
{
	ValueTable *valueTable = (ValueTable*)m_Metadata;
	s32 index = (s32)floor(input);
	index = Clamp(index, 0, valueTable->numValues-1);
	return valueTable->Value[index].y;
}

bool audCurve::Exponential_Init()
{
	Exponential *exponential = (Exponential*)m_Metadata;
	m_Coefficients[0] = (exponential->Flip.BitFields.x?-1.0f:0.0f);
	m_Coefficients[1] = (exponential->Flip.BitFields.y?-1.0f:0.0f);
	m_Coefficients[2] = exponential->Exponent;
	m_IsValidInSoundTask = true;
	return true;
}

//  y = x^n where n < 1 = inverse exponential (convex), n = 1 = linear, n > 1 = exponential (concave)
F32_VerifyFinite audCurve::Exponential_CalculateValue(F32_VerifyFinite input) const
{
	f32 flippedInput = 1.0f - input;
	input = Selectf(m_Coefficients[0], input, flippedInput);
	f32 output = Powf(input, m_Coefficients[2]);
	f32 flippedOutput = 1.0f - output;
	return Selectf(m_Coefficients[1], output, flippedOutput);
}

bool audCurve::DecayingExponential_Init()
{
	DecayingExponential* decayingExponential = (DecayingExponential*)m_Metadata;
	m_Coefficients[0] = decayingExponential->HorizontalScaling;
	m_Coefficients[1] = (AUD_GET_TRISTATE_VALUE(decayingExponential->Flags, FLAG_ID_DECAYINGEXPONENTIAL_CLAMPTOZEROATONE) != AUD_TRISTATE_FALSE?-1.0f:0.0f);
	m_IsValidInSoundTask = true;
	return true;
}

//  e^(-Ax) - with a vertical shear, to make it zero at a given (1, doubtless!) horizontal value
F32_VerifyFinite audCurve::DecayingExponential_CalculateValue(F32_VerifyFinite input) const
{
	const f32 horizontalScaling = m_Coefficients[0];
	f32 rescaledX = input * horizontalScaling;
	f32 yAtOne = Powf(M_E, -1.0f * horizontalScaling);
	f32 clampedOutput = (Powf(M_E, (-1.0f * rescaledX)) - yAtOne) / (1.0f - yAtOne);
	f32 output = Powf(M_E, (-1.0f * rescaledX));
	return Selectf(m_Coefficients[1], output, clampedOutput);	
}

bool audCurve::DecayingSquaredExponential_Init()
{
	DecayingSquaredExponential* decayingExponential = (DecayingSquaredExponential*)m_Metadata;
	m_Coefficients[0] = decayingExponential->HorizontalScaling;
	m_Coefficients[1] = (AUD_GET_TRISTATE_VALUE(decayingExponential->Flags, FLAG_ID_DECAYINGEXPONENTIAL_CLAMPTOZEROATONE) != AUD_TRISTATE_FALSE?-1.0f:0.0f);
	m_IsValidInSoundTask = true;
	return true;
}

//  e^(-A*x^2) - with a vertical shear, to make it zero at a given (1, doubtless!) horizontal value
F32_VerifyFinite audCurve::DecayingSquaredExponential_CalculateValue(F32_VerifyFinite input) const
{
	const f32 horizontalScaling = m_Coefficients[0];
	f32 rescaledX = input * horizontalScaling;
	f32 yAtOne = Powf(M_E, -1.0f * horizontalScaling * horizontalScaling);
	f32 clampedOutput = (Powf(M_E, (-1.0f * rescaledX * rescaledX)) - yAtOne) / (1.0f - yAtOne);
	f32 output = Powf(M_E, (-1.0f * rescaledX * rescaledX));
	return Selectf(m_Coefficients[1], output, clampedOutput);
}

bool audCurve::SineCurve_Init()
{
	SineCurve *sineCurve = (SineCurve*)m_Metadata;

	m_Coefficients[0] = sineCurve->VerticalScaling;
	m_Coefficients[1] = sineCurve->StartPhase;
	m_Coefficients[2] = sineCurve->EndPhase;
	m_Coefficients[3] = sineCurve->VerticalOffset;
	m_Coefficients[4] = sineCurve->Frequency;
	m_IsValidInSoundTask = true;
	return true;
}
//  A*Sin(B+C*x)+D   - haven't done a Cosine, just offset this one
F32_VerifyFinite audCurve::SineCurve_CalculateValue(F32_VerifyFinite input) const
{
	f32 A, B, D;
	A = m_Coefficients[0]; // vertical scaling
	B = m_Coefficients[1]; // start phase
	D = m_Coefficients[3]; // vertical offset

	// endPhase - startPhase
	f32 frequency = m_Coefficients[2] - m_Coefficients[1];

	// choose between (endPhase - startPhase) and sineCurve->Frequency based on endPhase being > 0
	f32 finalFrequency = Selectf(m_Coefficients[2], frequency, m_Coefficients[4]);
	f32 output = A * rage::Sinf(PH_DEG2RAD(B+finalFrequency*input)) + D;
	return output;
}

bool audCurve::OneOverX_Init()
{
	OneOverX* oneOverX = (OneOverX*)m_Metadata;
	m_Coefficients[0] = oneOverX->HorizontalScaling;
	m_Coefficients[1] = (AUD_GET_TRISTATE_VALUE(oneOverX->Flags, FLAG_ID_DECAYINGEXPONENTIAL_CLAMPTOZEROATONE) != AUD_TRISTATE_FALSE?-1.0f:0.0f);
	m_IsValidInSoundTask = true;
	return true;
}

//  1/(A*x+B) with a vertical shear to make it zero at a given horizontal value
F32_VerifyFinite audCurve::OneOverX_CalculateValue(F32_VerifyFinite input) const
{
	const f32 horizontalScaling = m_Coefficients[0];
	f32 rescaledX = input * horizontalScaling;
#if !__OPTIMIZED
	if ((1.0f + rescaledX) < 0.5f)
	{
		audAssert(0);
		return 0.0f;
	}
#endif
	
	f32 yAtOne = 1.0f / (1.0f + horizontalScaling);
	f32 clampedOutput = ((1.0f / (1.0f + rescaledX)) - yAtOne) / (1.0f - yAtOne);
	f32 output = 1.0f / (1.0f + rescaledX);
	return Selectf(m_Coefficients[1], output, clampedOutput);
}

bool audCurve::OneOverXSquared_Init()
{
	OneOverXSquared* oneOverX = (OneOverXSquared*)m_Metadata;
	m_Coefficients[0] = oneOverX->HorizontalScaling;
	m_Coefficients[1] = (AUD_GET_TRISTATE_VALUE(oneOverX->Flags, FLAG_ID_DECAYINGEXPONENTIAL_CLAMPTOZEROATONE) != AUD_TRISTATE_FALSE?-1.0f:0.0f);
	m_IsValidInSoundTask = true;
	return true;
}

//  1/(A*x^2+B) with a vertical shear to make it zero at a given horizontal value
F32_VerifyFinite audCurve::OneOverXSquared_CalculateValue(F32_VerifyFinite input) const
{
	const f32 horizontalScaling = m_Coefficients[0];

	f32 rescaledX = input * horizontalScaling;
#if !__OPTIMIZED
	if ((1.0f + rescaledX) < 0.5f)
	{
		audAssert(0);
		return 0.0f;
	}
#endif

	f32 yAtOne = (1.0f / (1.0f + horizontalScaling)) * (1.0f / (1.0f + horizontalScaling));
	f32 clampedOutput = ((1.0f / ((1.0f + rescaledX)*(1.0f + rescaledX))) - yAtOne) / (1.0f - yAtOne);
	f32 output = 1.0f / ((1.0f + rescaledX)*(1.0f + rescaledX));
	return Selectf(m_Coefficients[1], output, clampedOutput);
}

bool audCurve::DefaultDistanceAttenuation_Init()
{
	m_IsValidInSoundTask = true;
	return true;
}

bool audCurve::DefaultDistanceAttenuationClamped_Init()
{
	DefaultDistanceAttenuationClamped* defaultClamped = (DefaultDistanceAttenuationClamped*)m_Metadata;
	m_Coefficients[0] = defaultClamped->MaxGain / 100.0f;
	m_Coefficients[1] = (AUD_GET_TRISTATE_VALUE(defaultClamped->Flags, FLAG_ID_DEFAULTDISTANCEATTENUATIONCLAMPED_RESCALE) == AUD_TRISTATE_TRUE) ? 1.0f : 0.0f;

	return DefaultDistanceAttenuation_Init();
}

F32_VerifyFinite audCurve::DefaultDistanceAttenuationClamped_CalculateValue(F32_VerifyFinite input) const
{
	f32 maxGain = m_Coefficients[0];
	f32 output = Min<f32>(DefaultDistanceAttenuation_CalculateValue(input), maxGain);
	if (m_Coefficients[1] != 0.0f)
		output -= maxGain;
	return output;
}

F32_VerifyFinite audCurve::DefaultDistanceAttenuation_CalculateValue(F32_VerifyFinite const input)
{
	// Don't look at our metadata - we need this to be super-speedy, so everything's hardcoded.

	// This is doing an 8th order poly approximation of the value table curve we used to use.
	// The coeffs were generated using matlab's polyfit() fn, including MU scaling values, 
	// to reduce the chance of numerical inaccuracy.
	// If you want a different default curve, you can use any curve type. But as they're used twice
	// every audio frame in every environment sound, it pays to produce a hard-coded one like this,
	// that's computationally efficient, and doesn't hit metadata.

/*	f32 coeff[8]={
		0.2201f,
		-0.5388f,
		-0.2507f,
		0.9942f,
		0.7150f,
		-2.5018f,
		4.3724f,
		-17.0421f
		//				-62.6395f
	};
*/
	static const f32 coeff[8]={
		-17.0421f,
		4.3724f,
		-2.5018f,
		0.7150f,
		0.9942f,
		-0.2507f,		
		-0.5388f,
		0.2201f
		//				-62.6395f
	};

	// scale the input, to as per matlab's polyval
	const f32 x = (input-63.95f)/36.9648f;
	f32 attenuationCurve = -62.4395f;
	f32 x_power = 1.0f;
	// we don't do the first one, as we initialise attenuationCurve to the y offset, hence i<8
	for (int i=0; i<8; i++) 
	{
		x_power *= x;
		attenuationCurve += (x_power * coeff[i]);
	}

	const f32 attenuation = Selectf(input-4.99f, attenuationCurve, 0.0f);
	const f32 output = Selectf(input-128.0f, g_SilenceVolume, attenuation);

	return output;
}

#if __BANK && !__SPU
const char *audCurve::GetName() const
{
	return g_AudioEngine.GetCurveRepository().GetMetadataManager().GetObjectName(m_NameHash);
}
#endif

} // namespace rage
