// 
// audioengine/environmentgroup.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "environmentgroup.h"
#include "controller.h"
#include "engineconfig.h"
#include "dynamicmixmgr.h"
#include "system/interlocked.h"
#include "system/param.h"

namespace rage {

XPARAM(noaudio);

#if !__SPU
void audEnvironmentGroupInterface::BaseInit(audEntity* entity)
{
	audController* controller = audController::FindControllerForThread();
	Assert(controller);
	controller->RegisterEnvironmentGroup(this);
	m_Entity = entity;
}

#endif//!__SPU

audEnvironmentGroupInterface::audEnvironmentGroupInterface()
{
	m_ReferencingSounds = 0;
	m_Entity = NULL;
	m_NextIndex = 0xffff;
	m_PrevIndex = 0xffff;
	m_MixGroupIndex = -1;
	m_PrevMixGroupIndex = -1;
	BANK_ONLY(m_DebugIdentifier = NULL);
}

audEnvironmentGroupInterface::~audEnvironmentGroupInterface()
{
	if(m_PrevMixGroupIndex != -1)
	{
		audMixGroup::RemoveMixGroupReference(m_PrevMixGroupIndex, audMixGroup::ENTITY_REF);
	}
	//If we're in a mix group, remove the reference;
	if(m_MixGroupIndex != -1)
	{
		audMixGroup::RemoveMixGroupReference(m_MixGroupIndex, audMixGroup::ENTITY_REF);
	}
}

//  NEVER call this willy-nilly from gamecode - it should only be done from ReqSets, or if you really REALLY know what you're doing.
void audEnvironmentGroupInterface::AddSoundReference()
{
#if !__SPU
	sysInterlockedIncrement(&m_ReferencingSounds);
#endif
}

//  NEVER call this willy-nilly from gamecode - it should only be done from ReqSets, or if you really REALLY know what you're doing.
void audEnvironmentGroupInterface::RemoveSoundReference()
{
#if !__SPU
	Assert(m_ReferencingSounds>0);
	(void)AssertVerify(sysInterlockedDecrement(&m_ReferencingSounds) != ~0U);
#endif
}

u32 audEnvironmentGroupInterface::GetNumberOfSoundReference() const
{
	return m_ReferencingSounds;
}

void audEnvironmentGroupInterface::PopulateEnvironmentMetric(audEnvironmentGameMetric* environmentMetric)
{
	if (environmentMetric)
	{
		*environmentMetric = m_EnvironmentGameMetric;
	}
}

void audEnvironmentGroupInterface::UnsetMixGroup() 
{ 
	if(m_MixGroupIndex != -1)
	{
		audMixGroup::RemoveMixGroupReference(m_MixGroupIndex, audMixGroup::ENTITY_REF);
	}

	m_MixGroupIndex = -1; 
}

void audEnvironmentGroupInterface::SetMixGroup(s32 mixgroupIndex) 
{ 
	//If we're already in a mix group we need to remove our reference to it
	if(m_MixGroupIndex != -1)
	{
		audMixGroup::RemoveMixGroupReference(m_MixGroupIndex, audMixGroup::ENTITY_REF);
	}

	Assign(m_MixGroupIndex, mixgroupIndex);

	if(m_MixGroupIndex != -1)
	{
		audMixGroup::AddMixGroupReference(m_MixGroupIndex, audMixGroup::ENTITY_REF);
	}
}

void audEnvironmentGroupInterface::SetPrevMixGroup(s32 mixgroupIndex) 
{ 
	//If we're already in a mix group we need to remove our reference to it
	if(m_PrevMixGroupIndex != -1)
	{
		audMixGroup::RemoveMixGroupReference(m_PrevMixGroupIndex, audMixGroup::ENTITY_REF);
	}

	Assign(m_PrevMixGroupIndex, mixgroupIndex);

	if(m_PrevMixGroupIndex != -1)
	{
		audMixGroup::AddMixGroupReference(m_PrevMixGroupIndex, audMixGroup::ENTITY_REF);
	}
}

void audEnvironmentGroupManager::RegisterDeletedEnvironmentGroup(audEnvironmentGroupInterface* environmentGroup)
{
	if(!PARAM_noaudio.Get())
	{
		if(Verifyf(!m_DeletedEnvironmentGroups.IsFull(), "Need to increase size of deleted environment group tracking array"))
		{
			m_DeletedEnvironmentGroups.Push(environmentGroup);
		}
	}
}

bool audEnvironmentGroupManager::IsEnvironmentGroupInDeletedList(audEnvironmentGroupInterface* environmentGroup) const
{
	for(s32 i = 0; i < m_DeletedEnvironmentGroups.GetCount(); i++)
	{
		if(environmentGroup == m_DeletedEnvironmentGroups[i])
		{
			return true;
		}
	}
	return false;
}

#if __BANK
void audEnvironmentGroupInterface::DrawEntityBoundingBox(const Color32 colour)
{
	if(m_Entity)
	{
		m_Entity->DrawBoundingBox(colour);
	}
}
#endif


} // namespace rage