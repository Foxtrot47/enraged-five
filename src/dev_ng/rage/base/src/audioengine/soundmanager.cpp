//
// audioengine/soundmanager.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "categorymanager.h"
#include "engine.h"
#include "engineutil.h"
#include "engineconfig.h"
#include "environment.h"
#include "dynamicmixmgr.h"
#include "environment.h"
#include "requestedsettings.h"
#include "soundfactory.h"
#include "soundmanager.h"

#include "atl/pool.h"
#include "audiodata/simpletypes.h"
#include "audiohardware/config.h"
#include "audiohardware/debug.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/streamingwaveslot.h"
#include "bank/bank.h"
#include "grcore/im.h"
#include "grcore/debugdraw.h"
#include "string/stringhash.h"
#include "system/param.h"
#include "system/performancetimer.h"
#include "system/task.h"
#include "system/timer.h"
#include "system/xtl.h"
#include "profile/profiler.h"
#include "vector/colorvector3.h"

#include "audiosoundtypes/environmentsound.h"

#if __XENON
#include "tracerecording.h"
#endif

#if __WIN32PC
#include <xmmintrin.h>
#endif

#include "audiosoundtypes/sound.h"
#include "environment.h"
#include "audiohardware/voicesettings.h"

#include "audiosoundtypes/speechsound.h"

#include "categoryupdatejob.h"
#include "environmentupdatejob.h"

#ifndef FINAL_ONLY
#if __FINAL
#define FINAL_ONLY(x) x
#else
#define FINAL_ONLY(x)
#endif
#endif //#ifndef FINAL_ONLY

#if __PS3
FINAL_ONLY(const) bool g_RunCategoryUpdateOnSPU = __PS3;
				  bool g_RunSoundUpdateOnSPUs = false;
FINAL_ONLY(const) bool g_SoundUpateOnSPUsEnabled = (__PS3 && __OPTIMIZED);
FINAL_ONLY(const) bool g_WaitForCategoryResults = false;
#endif

#include "soundupdatejob.h"

RELOAD_TASK_DECLARE(SoundUpdateJob);
RELOAD_TASK_DECLARE(environmentupdatejob);



namespace rage 
{
	BANK_ONLY(extern float g_AmbisonicsDecoderScaling;)

	extern const char *gSoundClassFactoryGetTypeName(const u32 classId);

#if __PS3
	u32 g_SoundUpdateSpus = 1;
	u32 g_MaxBucketsPerSoundJob = 0;
	volatile u32 g_CurrentSoundBucket;
	
	atRangeArray<s32,4> g_SoundUpdateCommandBuffers;
	atRangeArray<s32,4> g_EnvironmentUpdateCommandBuffers;	

	BANK_ONLY(u32 g_SoundUpdateScratchFree = 0);
	BANK_ONLY(u32 g_SoundUpdateCodeSize = 0);
	BANK_ONLY(u32 g_EnvironmentUpdateCodeSize = 0);
	BANK_ONLY(u32 g_EnvironmentUpdateScratchFree = 0);
	BANK_ONLY(bool g_DebugSoundUpdateJob = false);

	
#endif

	EXT_PF_GROUP(AudioSoundsAndVoices);
	
	PF_VALUE_INT(ActiveParentSounds, AudioSoundsAndVoices);
	PF_VALUE_INT(AllocatedSounds, AudioSoundsAndVoices);
	PF_VALUE_INT(SoundUpateCommandBufferUsed, AudioSoundsAndVoices);
	PF_VALUE_INT(EnvUpateCommandBufferUsed, AudioSoundsAndVoices);

	PF_PAGE(SoundMgrTimingPage,"SoundManager Timing");
	PF_GROUP(SoundMgrTiming);
	PF_LINK(SoundMgrTimingPage, SoundMgrTiming);
	PF_TIMER(CategoryUpdate, SoundMgrTiming);
	PF_TIMER(SoundUpdate, SoundMgrTiming);
#if __PPU
	PF_TIMER(SoundUpdateSpuWait, SoundMgrTiming);
	PF_TIMER(EnvironmentUpdateSpuWait, SoundMgrTiming);
	PF_TIMER(CategoryUpdateSpuWait, SoundMgrTiming);
	PF_TIMER(SoundUpdateCommandBufferWait, SoundMgrTiming);
	PF_TIMER(EnvironmentUpdateCommandBufferWait, SoundMgrTiming);
#endif

	PF_PAGE(SoundPoolPage,"audSoundPool stats");
	PF_GROUP(SoundPool);
	PF_LINK(SoundPoolPage, SoundPool);
	PF_VALUE_INT(SoundSlotsAllocated, SoundPool);
	PF_VALUE_INT(AvailableSoundSlots, SoundPool);
	PF_VALUE_INT(FullestBucket, SoundPool);
	PF_VALUE_INT(EmptiestBucket, SoundPool);
	PF_VALUE_INT(LeastSlotsFree, SoundPool);
	PF_VALUE_INT(MostSlotsFree, SoundPool);
	PF_VALUE_INT(MostReqSetsSlotsFree,SoundPool);
	PF_VALUE_INT(LeastReqSetsSlotsFree,SoundPool);
	PF_VALUE_INT(NumSoundsDormant, SoundPool);
	PF_VALUE_INT(NumSoundsPlaying, SoundPool);
	PF_VALUE_INT(NumSoundsPreparing, SoundPool);
	PF_VALUE_INT(NumSoundsWaitingToBeDeleted, SoundPool);
	PF_VALUE_INT(MaxVoicesUsed, SoundPool);
	PF_VALUE_INT(LeastVoicesFree, SoundPool);
	PF_VALUE_INT(SkippedChildProcessing, SoundPool);

	PF_PAGE(SoundUpdatePage, "sound update stats");
	PF_GROUP(SoundUpdate);
	PF_LINK(SoundUpdatePage, SoundUpdate);
	PF_VALUE_INT(EntityVariableCacheHits, SoundUpdate);
	PF_VALUE_INT(EntityVariableCacheMisses, SoundUpdate);
	

	XPARAM(nospuaudio);

#if __BANK
	
	bool g_ShowAudioTime = 0;

	bool g_DebugDrawPlayingSoundPositions = false;
	bool g_DebugDrawEntityInfo = false;
	bool g_DebugDrawSoundHierarchy = false;
	bool g_DebugDrawSoundPCMCost = false;
	bool g_DisplayAudioVariables = false;	
	bool g_DebugDrawParentsOnly = true;    
	bool g_MuteMatchedSounds = false;
	bool g_SoloMatchedSounds = false;
	bool g_BreakOnMatchedSoundCreate = false;
	bool g_BreakOnMatchedSoundStop = false;
	bool g_DisplayPositionModulationValuesAtListenerPosition = false;
	f32 g_DebugDrawSoundVerticalScroll = 0.0f;
	f32 g_SoundRenderingDebugOffset = 0.0f;
	char g_DrawPlayingSoundFilter[128]={0};	
	char g_IgnorePlayingSoundFilter[128]={0};
	char g_DrawPlayingSoundEntityTagFilter[128]={0};
	char g_IgnorePlayingSoundEntityTagFilter[128]={0};
	extern bool g_DrawBucketInfo;
	extern f32 g_DrawBucketYScroll;
	extern u32 g_DrawBucketMaxSounds;
#endif

audSoundManager::audSoundManager()
{
}

audSoundManager::~audSoundManager()
{
}

bool audSoundManager::InitClass()
{
	u32 numBuckets = audConfig::GetValue("engineSettings_NumBuckets", 24U);

	// allocate sound pool memory
	const u32 maxSoundSize = audSoundFactory::GetMaxSoundTypeSize();
	u32 numSoundsPerBucket = audConfig::GetValue("engineSettings_NumSoundsPerBucket", 128U);
	u32 numRequestedSettingsPerBucket = audConfig::GetValue("engineSettings_NumRequestedSettingsPerBucket", 64U);

	PS3_ONLY(g_MaxBucketsPerSoundJob = audConfig::GetValue("engineSettings_MaxBucketsPerSoundJob", 12U));

#if __PS3 && __DEV
	// Reduce bucket size in PS3 beta builds to fit on SPU
	numSoundsPerBucket = Min(numSoundsPerBucket, 160U);
	numRequestedSettingsPerBucket = Min(numRequestedSettingsPerBucket, 48U);
	//numBuckets += 4; // compensate for reducing the size of each bucket by allocating additional buckets

	g_MaxBucketsPerSoundJob = numBuckets;
#endif
	
#if __PS3	
	g_SoundUpdateSpus = numBuckets / g_MaxBucketsPerSoundJob;
	if (numBuckets % g_MaxBucketsPerSoundJob != 0)
	{
		Quitf("[audSoundManager::InitClass] NumBuckets(%u) must be a multiple of MaxBucketsPerSoundJob(%u)", numBuckets, g_MaxBucketsPerSoundJob);
	}
	
#if !__FINAL
	if(PARAM_nospuaudio.Get())
	{
		g_RunCategoryUpdateOnSPU = false;
		g_SoundUpateOnSPUsEnabled = false;
	}
#endif
#endif

	audRequestedSettings::InitClass();

	audSound::InitClass(maxSoundSize, 
		numSoundsPerBucket, 
		sizeof(audRequestedSettings), 
		numRequestedSettingsPerBucket, 
		numBuckets, 
		2 /*audConfig::GetValue("engineSettings_NumReservedBuckets", 1U)*/);

	return true;
}

void audSoundManager::ShutdownClass()
{
	audSound::ShutdownClass();
	audRequestedSettings::ShutdownClass();
}


void audSoundManager::InitCommandBuffers()
{
	// On other platforms we only need one command buffer for the entire audio thread.
	const u32 commandBufferSize = audConfig::GetValue("engineSettings_AudioThreadCommandBuffer", 64U*1024U) WIN32PC_ONLY( *COMMAND_BUFFER_SIZE_MUTIPLIER );
	audDriver::GetMixer()->InitClientThread("AudioThread", commandBufferSize);
}

bool audSoundManager::Init()
{
	if(!m_SoundFactory.Init())
	{
		return false;
	}

	if(!audSpeechSound::InitClass())
	{
		return false;
	}

	sysMemZeroBytes<sizeof(m_Variables)>(&m_Variables);


	// Set up our global variables
	ravesimpletypes::VariableList *variableMetadata = audConfig::GetMetadataObject<ravesimpletypes::VariableList>(ATSTRINGHASH("GlobalVariableList", 0x68444408));
	if(variableMetadata)
	{
		audAssert(g_MaxSoundManagerVariables >= variableMetadata->numVariables);
		for(u32 i = 0; i < variableMetadata->numVariables; i++)
		{
			m_Variables[i].hash = variableMetadata->Variable[i].Name;
			m_Variables[i].value = variableMetadata->Variable[i].InitialValue;
			m_VariableAliases[i] = m_Variables[i].hash;
		}
	}

	m_MasterRMSVariable = GetVariableAddress(ATSTRINGHASH("Game.Audio.RMSLevels.Master", 0xE122F32D));
	m_SFXRMSVariable = GetVariableAddress(ATSTRINGHASH("Game.Audio.RMSLevels.SFX", 0xA37BC1CC));
	m_MasterRMSSmoothedVariable = GetVariableAddress(ATSTRINGHASH("Game.Audio.RMSLevels.MasterSmoothed", 0x34596512));
	m_SFXRMSSmoothedVariable = GetVariableAddress(ATSTRINGHASH("Game.Audio.RMSLevels.SFXSmoothed", 0xEEE20C63));

	m_MasterRMSSmoother.Init(2.5e-5f, 1.25e-5f, true);
	m_SfxRMSSmoother.Init(2.5e-5f, 1.25e-5f, true);

	m_LastUpdateTime = audEngineUtil::GetCurrentTimeInMilliseconds();
	m_DisabledMetadataSize = 0;
	m_DisabledMetadataPtr = NULL;
	m_GameUpdateIndex = 1;
	return true;
}

void audSoundManager::Shutdown()
{
	audSpeechSound::ShutdownClass();
	m_SoundFactory.Shutdown();
}

void audSoundManager::CommitGameSettings()
{ 	
	sysInterlockedIncrement(&m_GameUpdateIndex);
	audRequestedSettings::IncrementWriteIndex();
}

void audSoundManager::SetVariableAlias(const u32 nameHash, const u32 aliasNameHash)
{
	for(u32 i = 0; i < g_MaxSoundManagerVariables; i++)
	{
		if(m_Variables[i].hash == nameHash)
		{
			m_VariableAliases[i] = aliasNameHash;
		}
	}
}

f32* audSoundManager::SetVariableValue(const u32 nameHash, const f32 value)
{
	// Loop through our own variable block, to see if it already exists
	u32 i;
	for (i=0; i<g_MaxSoundManagerVariables; i++)
	{
		if (m_Variables[i].hash == nameHash || m_VariableAliases[i] == nameHash)
		{
			// The variable exists, so set it.
			m_Variables[i].value = value;
			return &m_Variables[i].value;
		}
		else if (m_Variables[i].hash == 0)
		{
			// we've got to the first empty one, so set this one to be the new variable and bail out
			m_Variables[i].hash = nameHash;
			m_Variables[i].value = value;
			return &m_Variables[i].value;
		}
	}
	Assert(i<g_MaxSoundManagerVariables);
	return NULL;
}

#if __BANK
void audSoundManager::DebugDrawSounds()
{
	if(g_DebugDrawPlayingSoundPositions || g_DebugDrawSoundHierarchy || g_DebugDrawSoundPCMCost)
	{
		Vec3V playerPosition = Vec3V(V_ZERO);
		f32* playerPosX = GetVariableAddress("Game.Player.Position.x");
		f32* playerPosY = GetVariableAddress("Game.Player.Position.y");
		f32* playerPosZ = GetVariableAddress("Game.Player.Position.z");

		if(playerPosX && playerPosY && playerPosZ)
		{
			playerPosition = Vec3V(ScalarV(*playerPosX), ScalarV(*playerPosY), ScalarV(*playerPosZ));
		}

		audSound::GetStaticPool().DebugDrawSounds(g_DebugDrawPlayingSoundPositions, 
                                                  g_DebugDrawEntityInfo,
												  g_DebugDrawSoundHierarchy, 
												  g_DebugDrawSoundPCMCost, 
												  g_DrawPlayingSoundFilter, 
                                                  g_IgnorePlayingSoundFilter,
                                                  g_DrawPlayingSoundEntityTagFilter, 
                                                  g_IgnorePlayingSoundEntityTagFilter,
												  g_DebugDrawParentsOnly, 
												  playerPosition, 												  
												  g_DebugDrawSoundVerticalScroll,
                                                  g_SoundRenderingDebugOffset);
	}
}

void audSoundManager::DebugDrawVariableValues()
{
	if(g_DisplayAudioVariables)
	{
		char tempString[256];
		f32 screenX = 0.15f;
		f32 screenY = 0.2f;

		for (int i=0; i<g_MaxSoundManagerVariables; i++)
		{
			if(m_Variables[i].hash != 0)
			{
				formatf(tempString, "%u: %f", m_Variables[i].hash, m_Variables[i].value);
				grcDebugDraw::Text(Vector2(screenX, screenY), Color32(255,255,255), tempString);
				screenY += 0.02f;

				if(screenY > 0.8f)
				{
					screenY = 0.2f;
					screenX += 0.2f;
				}
			}		
		}
	}	
}

bool audSoundManager::ShouldMuteSound(audSound* sound)
{
	if(sound && (g_MuteMatchedSounds || g_SoloMatchedSounds) && (strlen(g_DrawPlayingSoundFilter) > 0 || strlen(g_IgnorePlayingSoundFilter) > 0))
	{
		const char* soundName = sound->GetName();
		audSound* parentSound = sound->GetTopLevelParent();
		audEntity* parentEntity = parentSound? parentSound->GetEntity() : NULL;
		bool nameMatches = audSoundPool::MatchRAGFilter(soundName, parentEntity? parentEntity->GetEntityName() : NULL, g_DrawPlayingSoundFilter, g_IgnorePlayingSoundFilter, g_DrawPlayingSoundEntityTagFilter, g_IgnorePlayingSoundEntityTagFilter);

		if(!g_DebugDrawParentsOnly || parentSound == NULL || sound == parentSound)
		{
			if(g_MuteMatchedSounds && nameMatches)
			{				
				return true;
			}

			if(g_SoloMatchedSounds && !nameMatches)
			{
				return true;
			}
		}		
	}

	return false;
}

bool audSoundManager::MatchesNameFilter(audSound* sound)
{
	if(sound)
	{
		const char* soundName = sound->GetName();
		audSound* parentSound = sound->GetTopLevelParent();
		audEntity* parentEntity = parentSound? parentSound->GetEntity() : NULL;
		return MatchesNameFilter(soundName, parentEntity);
	}

	return false;
}

bool audSoundManager::MatchesNameFilter(const char* soundName, audEntity* parentEntity)
{
	if((strlen(g_DrawPlayingSoundFilter) > 0 || strlen(g_IgnorePlayingSoundFilter) > 0))
	{
		return audSoundPool::MatchRAGFilter(soundName, parentEntity? parentEntity->GetEntityName() : NULL, g_DrawPlayingSoundFilter, g_IgnorePlayingSoundFilter, g_DrawPlayingSoundEntityTagFilter, g_IgnorePlayingSoundEntityTagFilter);
	}

	return false;
}

bool audSoundManager::ShouldBreakOnStop(audSound* sound)
{
	return g_BreakOnMatchedSoundStop && MatchesNameFilter(sound);
}

bool audSoundManager::ShouldBreakOnCreate(audSound* sound)
{
	return g_BreakOnMatchedSoundCreate && MatchesNameFilter(sound);
}

bool audSoundManager::ShouldBreakOnCreate(audMetadataRef soundRef, audEntity* entity)
{
	if(g_BreakOnMatchedSoundCreate)
	{
		audMetadataObjectInfo objInfo;
		if(g_AudioEngine.GetSoundManager().GetFactory().GetMetadataManager().GetObjectInfo(soundRef, objInfo))
		{
			return MatchesNameFilter(objInfo.GetName_Debug(), entity);
		}		
	}

	return false;
}
#endif

f32* audSoundManager::SetVariableValue(const char *name, const f32 value)
{
	return SetVariableValue(atStringHash(name), value);
}

void audSoundManager::Update(u32 timeInMs)
{
	audEngineContext context;

	// Update RMS globals
	if(m_SFXRMSVariable && m_MasterRMSVariable && m_MasterRMSSmoothedVariable && m_SFXRMSSmoothedVariable && audDriver::GetMixer())
	{
		*m_MasterRMSVariable = g_audEnvironment->GetMasterSubmix()->GetRMS();
		*m_MasterRMSSmoothedVariable = m_MasterRMSSmoother.CalculateValue(*m_MasterRMSVariable, timeInMs);
		*m_SFXRMSVariable = g_audEnvironment->GetSfxSubmix()->GetRMS();
		*m_SFXRMSSmoothedVariable = m_SfxRMSSmoother.CalculateValue(*m_SFXRMSVariable, timeInMs);
	}

#if __PS3
	const s32 schedulerId = sysTaskManager::SCHEDULER_AUDIO_ENGINE;
#endif

#if AUD_ATOMIC_CATEGORIES
	// Flick over the Category audio-thread read index, so we're pointing at the most current thread-safe one.
	g_AudioEngine.GetCategoryManager().UpdateAudioThreadCategoryIndex();
#endif

#if __PS3
	sysTaskParameters p;
	if(g_RunCategoryUpdateOnSPU)
	{
		PF_FUNC(CategoryUpdate);

		g_AudioEngine.GetCategoryManager().LockUpdate();
		
		p.Input.Data = g_CategoryStoreMem;
		p.Input.Size = g_AudioEngine.GetCategoryManager().GetNumberOfCategories() * sizeof(audCategory);

		p.Output.Data = g_ResolvedCategorySettings;
		p.Output.Size = g_AudioEngine.GetCategoryManager().GetNumberOfCategories() * sizeof(tCategorySettings);

		p.SpuStackSize = 12*1024U;

		p.ReadOnly[0].Data = g_DynamicHeirachies;
		p.ReadOnly[0].Size = NUM_DYNAMIC_HEIRACHIES*sizeof(s16);
		p.ReadOnlyCount = 1;

#if AUD_ATOMIC_CATEGORIES
		p.UserData[0].asInt = g_AudioEngine.GetCategoryManager().GetCategorySettingsAudioThreadIndex();
#endif
		// safety check - supply the size of PPU side audCategory so we can assert on SPU if its not
		// consistent
		p.UserData[1].asInt = sizeof(audCategory);
		p.UserData[2].asInt = sizeof(tCategorySettings);
		p.UserData[3].asInt = NUM_DYNAMIC_HEIRACHIES;
		p.UserDataCount = 4;

		// run the category update job
		{			
			PF_FUNC(CategoryUpdateSpuWait);			
			sysTaskManager::Wait(sysTaskManager::Create(TASK_INTERFACE(CategoryUpdateJob),p, schedulerId));
		}

		g_AudioEngine.GetCategoryManager().UnlockUpdate();
	}
	else
#endif //__PS3
	{
		PF_FUNC(CategoryUpdate);
		g_AudioEngine.GetCategoryManager().Update();
	}

	BANK_ONLY(g_AudioEngine.GetCategoryManager().UpdateTelemetry());

	const u32 gameWriteIndex = m_GameUpdateIndex;
	// Flick over to most up-to-date listener settings
	g_AudioEngine.GetEnvironment().UpdateListenerAudioThreadIndex(gameWriteIndex);

	// Flick over the RequestedSetting read indexes
	audRequestedSettings::UpdateReadIndex(gameWriteIndex);
	
	// Update timers
#if __WIN32PC
	// SORR : Here's the story. On the PC the high resolution timer, is shit. It doesn't run at real time 
	// and in fact is completely different on different machines. Some are fast and some are slow. 
	// So trying to sync anything that uses these timers to audio never works well, it eventually
	// drifts. In a 2 minute time frame you could be out by a couple of seconds by the end.
	// To help with this in the past we have used the bios clock, which is way more accurate to 
	// correct the systimer. Problem is sometimes this correction is backwards, doh.
	// When this happens the soundmanager shits itself and bad things happen.
	// This code keeps everything running fine.
	if( m_LastUpdateTime > timeInMs )
	{
		m_LastUpdateTime = timeInMs;
	}
#endif

	f32 timeElapsed = static_cast<f32>(timeInMs - m_LastUpdateTime);
	context.systemTimeMs = timeInMs;
	for(u32 timerId = 0; timerId < g_NumAudioTimers; timerId++)
	{
		context.timers[timerId] = m_Timers[timerId];
		if(!context.timers[timerId].isPaused)
		{
			context.timers[timerId].timeInMs += static_cast<u32>(0.5f + timeElapsed * context.timers[timerId].timeScale);
		}
	
		m_Timers[timerId].timeInMs = context.timers[timerId].timeInMs;
	}
	
	m_LastUpdateTime = timeInMs;

	m_NumSoundsDormant = 0;
	m_NumSoundsPreparing = 0;
	m_NumSoundsWaitingToBeDeleted = 0;
	m_NumSoundsPlaying = 0;

#if __PPU

#if __BANK
	if(m_SoundFactory.GetNumEditedSounds() > 0)
	{
		// Run on PPU for a frame to pick up metadata changes
		g_RunSoundUpdateOnSPUs = false;
	}
#endif

	if(g_RunSoundUpdateOnSPUs)
	{
		PF_FUNC(SoundUpdate);

		static ALIGNAS(16) tSoundUpdateJobData s_SoundUpdateData ;
		static ALIGNAS(16) atRangeArray<tSoundUpdateJobResult,4> s_SoundUpdateResults ;

		sysTaskHandle soundUpdateHandles[g_SoundUpdateSpus];

		s_SoundUpdateData.numSoundSlotsPerBucket = audSound::sm_Pool.m_NumSoundSlotsPerBucket;
		s_SoundUpdateData.soundSlotSize = audSound::sm_Pool.m_SoundSlotSize;
		s_SoundUpdateData.numReqSetSlotsPerBucketAligned = audSound::sm_Pool.m_NumRequestedSettingsSlotsPerBucketAligned;
		s_SoundUpdateData.reqSetSlotSize = audSound::sm_Pool.m_RequestedSettingsSlotSize;
		s_SoundUpdateData.audSoundPoolBucketSize = sizeof(audSoundPoolBucket);
		s_SoundUpdateData.waveSlots = audWaveSlot::GetWaveSlotFromIndex(0);
		s_SoundUpdateData.waveSlotReferenceCounts = audWaveSlot::sm_ReferenceCounts;
		s_SoundUpdateData.numWaveSlots = audWaveSlot::GetNumWaveSlots();
		s_SoundUpdateData.listenerData = &g_audEnvironment->GetListener(0);
		s_SoundUpdateData.numOutputChannels = audDriver::GetNumOutputChannels();
		s_SoundUpdateData.globalVariables = &m_Variables[0];
		s_SoundUpdateData.entityVariables = audEntity::GetEntityVariableValuePoolAddress();
		s_SoundUpdateData.entityVariableRefs = audEntity::GetEntityVariableRefPoolAddress();
		s_SoundUpdateData.bucketLocks = audSound::sm_Pool.m_BucketLocks;
		sysMemCpy(&s_SoundUpdateData.timers, &m_Timers, sizeof(m_Timers));
		s_SoundUpdateData.numBucketsToProcess = g_MaxBucketsPerSoundJob;
		s_SoundUpdateData.pcmSourcePool = g_PcmSourcePool;
		s_SoundUpdateData.pcmSourceState = audDriver::GetVoiceManager().GetPcmSourceState(0);
		s_SoundUpdateData.numPcmSourcePoolSlots = g_PcmSourcePool->GetNumSlots();
		s_SoundUpdateData.mixGroupReferenceCounts = g_MixGroupReferenceCounts;
		s_SoundUpdateData.syncManagerEA = audMixerSyncManager::Get();
		s_SoundUpdateData.soundPoolBufferEA = audSound::sm_Pool.GetPoolBufferPtr();
		s_SoundUpdateData.requestedSettingsReadIndex = audRequestedSettings::GetReadIndex();
		s_SoundUpdateData.disabledMetadataPtr = m_DisabledMetadataPtr;
		s_SoundUpdateData.disabledMetadataSize = m_DisabledMetadataSize;
			
		sysMemSet(&p, 0, sizeof(p));

		p.Input.Data =  &s_SoundUpdateData;
		p.Input.Size = sizeof(s_SoundUpdateData);
		
		p.Output.Size = sizeof(s_SoundUpdateResults[0]);
		p.SpuStackSize = 10*1024;

		// allocate as much memory as possible for scratch space
		size_t codeSize = (size_t)RELOAD_TASK_INTERFACE_SIZE(SoundUpdateJob);
		
		p.UserData[0].asPtr = (void*)&g_CurrentSoundBucket;
		p.UserData[1].asInt = audSound::sm_Pool.GetNumBuckets() - audSound::sm_Pool.GetNumReservedBuckets();
		p.UserData[3].asPtr = &audSound::sm_Pool.m_Buckets[0];
		p.UserData[6].asUInt = audDriver::GetFrameCount();
		BANK_ONLY(p.UserData[7].asBool = g_DebugSoundUpdateJob);
		BANK_ONLY(g_DebugSoundUpdateJob = false;)
		p.UserData[8].asInt = NUM_DYNAMIC_HEIRACHIES;
#if RSG_BANK
		if(m_SoundFactory.GetMetadataManager().AreObjectNamesAvailable())
		{
			p.UserData[9].asPtr = (void*)m_SoundFactory.GetMetadataManager().GetObjectNameFromNameTableOffset(0U);
		}
		else
#endif // RSG_BANK
		{
			p.UserData[9].asPtr = NULL;
		}
		p.UserDataCount = 10;

		// SoundUpdateJob no longer has PcmSourceState data passed through - instead we DMA it in as and when its required
		p.ReadOnlyCount = 0;

		BANK_ONLY(g_SoundUpdateCodeSize = codeSize);
		p.Scratch.Size = kMaxSPUJobSize - (p.SpuStackSize + codeSize + p.Input.Size + p.Output.Size + p.ReadOnly[0].Size);

#if !AUD_SOUNDPOOL_LOCK_ON_SPU
		// lock all the buckets
		for(u32 i = 0; i < audSound::GetStaticPool().GetNumBuckets() - audSound::GetStaticPool().GetNumReservedBuckets(); i++)
		{
			audSound::GetStaticPool().LockBucket(i);
		}
#endif

		// kick off all of the update jobs
		g_CurrentSoundBucket = 0;
		for(u32 i = 0; i < g_SoundUpdateSpus; i++)
		{
			p.UserData[2].asInt = i;
			p.Output.Data =  &s_SoundUpdateResults[i];

			audCommandBuffer *commandBuffer = audDriver::GetMixer()->GetCommandBufferForThread(g_SoundUpdateCommandBuffers[i]);
			p.UserData[4].asPtr = commandBuffer->GetBuffer();
			p.UserData[5].asUInt = commandBuffer->GetSize();

			soundUpdateHandles[i] = sysTaskManager::Create(TASK_INTERFACE_RELOADABLE(SoundUpdateJob),p,schedulerId);
		}

		// process all reserved buckets on the PPU
		for(u32 i = audSound::GetStaticPool().GetNumBuckets() - audSound::GetStaticPool().GetNumReservedBuckets(); i < audSound::GetStaticPool().GetNumBuckets(); i++)
		{
			audSound::GetStaticPool().LockBucket(i);

			audSoundPool::ParentSoundIterator iter(audSound::GetStaticPool().GetNumSoundSlotsPerBucket(), 
				audSound::GetStaticPool().GetSoundSlotSize(), 
				audSound::GetStaticPool().GetBucket(i));
			while(true)
			{
				audSound *sound = iter.Next();
				if(sound == NULL)
				{
					// NULL indicates the end of this bucket
					break;
				}
				ProcessHierarchy(&context, sound);
			}
			audSound::GetStaticPool().UnlockBucket(i);
			audSound::GetStaticPool().ProcessChildSoundRequests(&context, i);
		}

#define AUD_ENVIRONMENT_UPDATE_ON_SPU 1

		{
			// wait for SPU results
			PF_FUNC(SoundUpdateSpuWait);
			sysTaskManager::WaitMultiple(g_SoundUpdateSpus, &soundUpdateHandles[0]); // TODO - was sleepyTime=300
		}

#if !__NO_OUTPUT
		static bool s_PrintedSoundUpdateScratchFree = false;
		if(!s_PrintedSoundUpdateScratchFree)
		{
			s_PrintedSoundUpdateScratchFree = true;
			audDisplayf("SoundUpdateJob scratch free: %u", s_SoundUpdateResults[0].scratchSpaceLeft);
		}
#endif // !__NO_OUTPUT

		audAssertf(g_CurrentSoundBucket >= audSound::sm_Pool.GetNumBuckets() - audSound::sm_Pool.GetNumReservedBuckets(), "After soundupdatejob g_CurrentSoundBucket: %u, expected >= %u", g_CurrentSoundBucket, audSound::sm_Pool.GetNumBuckets() - audSound::sm_Pool.GetNumReservedBuckets());

		// Process child requests from SPU buckets
		for(u32 i = 0; i < audSound::GetStaticPool().GetNumBuckets() - audSound::GetStaticPool().GetNumReservedBuckets(); i++)
		{
			audSound::sm_Pool.ProcessChildSoundRequests(&context, i);
		}

#if AUD_ENVIRONMENT_UPDATE_ON_SPU
		// Post update environment sounds
		g_CurrentSoundBucket = 0;

		static ALIGNAS(16) audEnvironmentUpdateJobInputData s_EnvironmentUpdateJobInputData ;
		static ALIGNAS(16) atRangeArray<u128,4> s_EnvironmentUpdateResults ;

		s_EnvironmentUpdateJobInputData.numSoundSlotsPerBucket = audSound::sm_Pool.m_NumSoundSlotsPerBucket;
		s_EnvironmentUpdateJobInputData.soundSlotSize = audSound::sm_Pool.m_SoundSlotSize;
		s_EnvironmentUpdateJobInputData.audSoundPoolBucketSize = sizeof(audSoundPoolBucket);
		s_EnvironmentUpdateJobInputData.numOutputChannels = audDriver::GetNumOutputChannels();
		s_EnvironmentUpdateJobInputData.bucketLocks = audSound::sm_Pool.m_BucketLocks;
		s_EnvironmentUpdateJobInputData.numBucketsToProcess = g_MaxBucketsPerSoundJob;
		s_EnvironmentUpdateJobInputData.numReqSetSlotsPerBucketAligned = audSound::sm_Pool.m_NumRequestedSettingsSlotsPerBucketAligned;
		s_EnvironmentUpdateJobInputData.reqSetSlotSize = audSound::sm_Pool.m_RequestedSettingsSlotSize;

		s_EnvironmentUpdateJobInputData.environmentListenerIndex = g_audEnvironment->GetListenerAudioThreadIndex();
		s_EnvironmentUpdateJobInputData.environmentManager = g_audEnvironment;
		s_EnvironmentUpdateJobInputData.sizeOfEnvironmentManager = sizeof(audEnvironment);
		s_EnvironmentUpdateJobInputData.resolvedCategorySettings = g_ResolvedCategorySettings;
		s_EnvironmentUpdateJobInputData.sizeOfResolvedCategorySettings = g_AudioEngine.GetCategoryManager().GetNumberOfCategories() * sizeof(tCategorySettings);
		s_EnvironmentUpdateJobInputData.mixGroups = g_MixGroupPoolMem;
		s_EnvironmentUpdateJobInputData.sizeOfMixGroups = NUM_AUD_MIXGROUPS * sizeof(audMixGroup);
		s_EnvironmentUpdateJobInputData.mixGroupReferenceCounts = g_MixGroupReferenceCounts;
		s_EnvironmentUpdateJobInputData.mixGroupInstanceIds = g_MixGroupInstanceIds;

		s_EnvironmentUpdateJobInputData.requestedSettingsReadIndex = audRequestedSettings::GetReadIndex();
		s_EnvironmentUpdateJobInputData.soundPoolBufferEA = audSound::sm_Pool.GetPoolBufferPtr();
		s_EnvironmentUpdateJobInputData.syncManagerEA = audMixerSyncManager::Get();

		BANK_ONLY(s_EnvironmentUpdateJobInputData.ambisonicDecoderScaling = g_AmbisonicsDecoderScaling;)

		sysMemCpy(&s_EnvironmentUpdateJobInputData.timers, &m_Timers, sizeof(m_Timers));
		
		p.Input.Data = &s_EnvironmentUpdateJobInputData;
		p.Input.Size = sizeof(s_EnvironmentUpdateJobInputData);

		p.Output.Size = sizeof(s_EnvironmentUpdateResults[0]);

		p.ReadOnlyCount = 1;
		p.ReadOnly[0].Data = audDriver::GetVoiceManager().GetPcmSourceState(0);
		p.ReadOnly[0].Size = sizeof(PcmSourceState) * kMaxPcmSources;
		
		// allocate as much memory as possible for scratch space
		codeSize = (size_t)RELOAD_TASK_INTERFACE_SIZE(environmentupdatejob);

		BANK_ONLY(g_EnvironmentUpdateCodeSize = codeSize);
		p.Scratch.Size = kMaxSPUJobSize - (p.SpuStackSize + codeSize + p.Input.Size + p.Output.Size + p.ReadOnly[0].Size);

		for(u32 i = 0; i < g_SoundUpdateSpus; i++)
		{
			p.UserData[2].asInt = i;
			audCommandBuffer *commandBuffer = audDriver::GetMixer()->GetCommandBufferForThread(g_EnvironmentUpdateCommandBuffers[i]);
			p.UserData[4].asPtr = commandBuffer->GetBuffer();
			p.UserData[5].asUInt = commandBuffer->GetSize();

			p.Output.Data = &s_EnvironmentUpdateResults[i];

			soundUpdateHandles[i] = sysTaskManager::Create(TASK_INTERFACE_RELOADABLE(environmentupdatejob), p, schedulerId);
		}

		// While environment processing is happening, flag sound update thread command buffers ready for processing 
		// (They were written to from sound update SPU job)
		BANK_ONLY(u32 maxOffset = 0);
		for(u32 i = 0; i < g_SoundUpdateSpus; i++)
		{
			BANK_ONLY(maxOffset = Max(s_SoundUpdateResults[i].commandBufferUsed, maxOffset));
			audDriver::GetMixer()->GetCommandBufferForThread(g_SoundUpdateCommandBuffers[i])->SetCurrentOffset(s_SoundUpdateResults[i].commandBufferUsed);
		}

		BANK_ONLY(PF_SET(SoundUpateCommandBufferUsed, maxOffset));
		{
			// wait for SPU results
			PF_FUNC(EnvironmentUpdateSpuWait);
			sysTaskManager::WaitMultiple(g_SoundUpdateSpus, &soundUpdateHandles[0]); // TODO - was sleepyTime=300
		}

		audAssertf(g_CurrentSoundBucket >= audSound::sm_Pool.GetNumBuckets() - audSound::sm_Pool.GetNumReservedBuckets(), "After environmentupdatejob g_CurrentSoundBucket: %u, expected >= %u", g_CurrentSoundBucket, audSound::sm_Pool.GetNumBuckets() - audSound::sm_Pool.GetNumReservedBuckets());
#endif

		for(u32 bucketId = 0; bucketId < audSound::sm_Pool.GetNumBuckets() - audSound::sm_Pool.GetNumReservedBuckets(); bucketId++)
		{
#if !AUD_ENVIRONMENT_UPDATE_ON_SPU
			for(u32 j = 0; j < audSound::sm_Pool.GetBucket(bucketId)->numEnvironmentSoundRequests; j++)
			{
				audEnvironmentSound *sound = (audEnvironmentSound*)audSound::sm_Pool.GetSoundSlot(bucketId, audSound::sm_Pool.GetBucket(bucketId)->environmentSoundRequestSlotIds[j]);
				TrapNE(sound->GetSoundTypeID(), EnvironmentSound::TYPE_ID);
				TrapZ((size_t)sound);
				sound->AudioUpdateVoice(audSound::sm_Pool.GetBucket(bucketId)->environmentSoundRequests[j]);
			}
#endif

#if !AUD_SOUNDPOOL_LOCK_ON_SPU
			audSound::sm_Pool.UnlockBucket(bucketId);
#endif
		} // foreach bucket

		
#if AUD_ENVIRONMENT_UPDATE_ON_SPU
		BANK_ONLY(maxOffset = 0);
		// Flag sound update thread command buffers ready for processing (They were written to from SPU)
		for(u32 i = 0; i < g_SoundUpdateSpus; i++)
		{
			const u32 commandBufferUsed =  s_EnvironmentUpdateResults[i][0];
			BANK_ONLY(maxOffset = Max(maxOffset, commandBufferUsed));
			audDriver::GetMixer()->GetCommandBufferForThread(g_EnvironmentUpdateCommandBuffers[i])->SetCurrentOffset(commandBufferUsed);
		}
		BANK_ONLY(PF_SET(EnvUpateCommandBufferUsed, maxOffset));
#endif

		BANK_ONLY(g_SoundUpdateScratchFree = s_SoundUpdateResults[0].scratchSpaceLeft);

#if __DEV
		u32 entityVariableCacheHits = 0;
		u32 entityVariableCacheMisses = 0;
#endif

		// synchronise wave slot state, update stats
		m_NumSoundsDormant = m_NumSoundsPreparing = m_NumSoundsWaitingToBeDeleted = m_NumSoundsPlaying = 0;
		for(u32 i = 0; i < g_SoundUpdateSpus; i++)
		{
			m_NumSoundsDormant += s_SoundUpdateResults[i].numSoundsDormant;
			m_NumSoundsPreparing += s_SoundUpdateResults[i].numSoundsPreparing;
			m_NumSoundsWaitingToBeDeleted += s_SoundUpdateResults[i].numSoundsWaitingToBeDeleted;
			m_NumSoundsPlaying += s_SoundUpdateResults[i].numSoundsPlaying;

			for(u32 j = 0; j < s_SoundUpdateResults[i].numWaveSlotRequests; j++)
			{
				audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndex(s_SoundUpdateResults[i].waveSlotRequests[j].waveSlotId);
				if(slot->IsStreaming())
				{
					const u32 startOffset = s_SoundUpdateResults[i].waveSlotRequests[j].waveNameHash;
					const bool  isLooping = ((s_SoundUpdateResults[i].waveSlotRequests[j].waveNameHash&2)==2);
					const bool isPreload = (s_SoundUpdateResults[i].waveSlotRequests[j].streamState==1);
					//audDisplayf("streaming slot %u has changed: so: %d, bank: %u, preload: %s looping %s", j, startOffset, g_SoundUpdateResults[i].waveSlotRequests[j].bankId, (isPreload?"yes":"no"), (isLooping?"yes":"no"));
					((audStreamingWaveSlot*)slot)->_SyncStreamingWaveSlotRequest(startOffset, s_SoundUpdateResults[i].waveSlotRequests[j].bankId, isPreload, isLooping);
				}
				else
				{
					slot->_SyncWaveSlotRequest(s_SoundUpdateResults[i].waveSlotRequests[j].bankId,
						s_SoundUpdateResults[i].waveSlotRequests[j].waveNameHash, s_SoundUpdateResults[i].waveSlotRequests[j].loadPriority);
				}
			}

#if __DEV //Profile entity variable cache hits/misses
			entityVariableCacheHits += s_SoundUpdateResults[i].entityVariableCacheHits;
			entityVariableCacheMisses += s_SoundUpdateResults[i].entityVariableCacheMisses;
#endif
		}
#if __DEV
		PF_SET(EntityVariableCacheHits, entityVariableCacheHits);
		PF_SET(EntityVariableCacheMisses, entityVariableCacheMisses);
#endif
	}
	else
#endif // __PPU
	{
		// run sound update on PPU
		PF_FUNC(SoundUpdate);
		u32 serviceableSounds = 0;
		for(u32 i = 0; i < audSound::sm_Pool.GetNumBuckets(); i++)
		{
			audSound::GetStaticPool().LockBucket(i);
			audSoundPool::ParentSoundIterator iter(audSound::GetStaticPool().GetNumSoundSlotsPerBucket(), 
													audSound::GetStaticPool().GetSoundSlotSize(), 
													audSound::GetStaticPool().GetBucket(i));
			while(true)
			{
				audSound *sound = iter.Next();
				if(sound == NULL)
				{
					// NULL indicates the end of this bucket
					break;
				}
				serviceableSounds++;
				ProcessHierarchy(&context, sound);
			}

			audSound::GetStaticPool().UnlockBucket(i);
			audSound::GetStaticPool().ProcessChildSoundRequests(&context, i);
		}
		
		PF_SET(ActiveParentSounds, serviceableSounds);
	}

	PF_SET(NumSoundsDormant, m_NumSoundsDormant);
	PF_SET(NumSoundsPreparing, m_NumSoundsPreparing);
	PF_SET(NumSoundsWaitingToBeDeleted, m_NumSoundsWaitingToBeDeleted);
	PF_SET(NumSoundsPlaying, m_NumSoundsPlaying);

	for(u32 timerId = 0; timerId < g_NumAudioTimers; timerId++)
	{
		m_Timers[timerId].wasPausedLastFrame = context.timers[timerId].isPaused;
	}

#if !__FINAL
	audSoundPoolStats stats;
	audSound::sm_Pool.GetPoolUtilisation(stats);
	PF_SET(AllocatedSounds, stats.soundSlotsAllocated);
	PF_SET(SoundSlotsAllocated, stats.soundSlotsAllocated);
	PF_SET(AvailableSoundSlots, stats.soundSlotsFree);
	PF_SET(FullestBucket, stats.fullestBucket);
	PF_SET(EmptiestBucket, stats.emptiestBucket);
	PF_SET(LeastSlotsFree, stats.leastSoundSlotsFree);
	PF_SET(MostSlotsFree, stats.mostSoundSlotsFree);
	PF_SET(LeastReqSetsSlotsFree, stats.leastReqSetsSlotsFree);
	PF_SET(MostReqSetsSlotsFree, stats.mostReqSetsSlotsFree);
	PF_SET(LeastVoicesFree, stats.leastVoicesFree);
	PF_SET(MaxVoicesUsed, stats.mostVoicesUsed);
	PF_SET(SkippedChildProcessing, stats.skippedChildProcessing);
#endif

	// enable SPU update after the first audio frame
	PS3_ONLY(g_RunSoundUpdateOnSPUs = g_SoundUpateOnSPUsEnabled);

	BANK_ONLY(m_SoundFactory.ResetEditedSounds());
}

void audSoundManager::StartUnloadingChunk(const char *chunkName)
{
	audMetadataManager &metadataMgr = GetFactory().GetMetadataManager();
	s32 chunkId = metadataMgr.FindChunkId(chunkName);
	if(chunkId != -1)
	{
		const audMetadataChunk &chunk = metadataMgr.GetChunk(chunkId);
		m_DisabledMetadataPtr = chunk.data;
		m_DisabledMetadataSize = chunk.dataSize;

		metadataMgr.SetChunkEnabled(chunkId, false);		
	}
}

void audSoundManager::CancelUnloadingChunk(const char *chunkName)
{
	audMetadataManager &metadataMgr = GetFactory().GetMetadataManager();
	s32 chunkId = metadataMgr.FindChunkId(chunkName);
	if(audVerifyf(chunkId != -1, "Failed to find loaded chunk %s", chunkName))
	{
		metadataMgr.SetChunkEnabled(chunkId, true);
	}
}

#if __BANK
void ReloadSoundUpdateJob()
{
	RELOAD_TASK_BINARY(SoundUpdateJob);
}
void CountCreatedSoundsCB()
{
	g_AudioEngine.GetSoundManager().GetFactory().CountSoundsCreated();
}
void LogSCreatedSoundsCB()
{
	g_AudioEngine.GetSoundManager().GetFactory().LogSoundsCreated();
}

void audSoundManager::AddSoundDebuggingWidgets(bkBank &bank)
{
	bank.PushGroup("Sound Debugging");	
	bank.AddToggle("Draw Sound Positions", &g_DebugDrawPlayingSoundPositions);
	bank.AddToggle("Draw Additional Details", &g_DebugDrawEntityInfo);
	bank.AddToggle("Draw Sound Hierarchy", &g_DebugDrawSoundHierarchy);
	bank.AddToggle("Draw Sound PCM Costs", &g_DebugDrawSoundPCMCost);
	bank.AddSlider("Vertical View Scroll", &g_DebugDrawSoundVerticalScroll, 0.0f, 10.0f, 0.1f);
	bank.AddSlider("Label Spacing", &g_SoundRenderingDebugOffset, 0, 100.0f, 0.1f);
	bank.AddToggle("Mute Matched Sounds", &g_MuteMatchedSounds);
	bank.AddToggle("Solo Matched Sounds", &g_SoloMatchedSounds);
	bank.AddToggle("Match Parent Sounds Only", &g_DebugDrawParentsOnly);
	bank.AddText("Filter by Name", g_DrawPlayingSoundFilter, sizeof(g_DrawPlayingSoundFilter));
	bank.AddText("Ignore Name", g_IgnorePlayingSoundFilter, sizeof(g_IgnorePlayingSoundFilter));
	bank.AddText("Filter by Audio Entity", g_DrawPlayingSoundEntityTagFilter, sizeof(g_DrawPlayingSoundEntityTagFilter));
	bank.AddText("Ignore Audio Entity", g_IgnorePlayingSoundEntityTagFilter, sizeof(g_IgnorePlayingSoundEntityTagFilter));
	bank.AddToggle("Break on Matched Sound Init", &g_BreakOnMatchedSoundCreate);
	bank.AddToggle("Break on Matched Sound Stop", &g_BreakOnMatchedSoundStop);    
	bank.PushGroup("Buckets");
			bank.AddToggle("Draw Bucket Info", &g_DrawBucketInfo);
			bank.AddSlider("Draw Bucket Y Scroll", &g_DrawBucketYScroll, 0.0f, 5.0f, 0.02f);
			bank.AddSlider("Draw Bucket Max Sounds", &g_DrawBucketMaxSounds, 0, 500, 1);
	bank.PopGroup();
	bank.PopGroup();
}

void audSoundManager::AddWidgets(bkBank &bank)
{
	bank.PushGroup("audSoundManager");

	// add sound manager widgets ...
#if __PS3
	bank.AddToggle("SPUCategoryUpdate", &g_RunCategoryUpdateOnSPU);
	bank.AddToggle("SPUSoundUpdate", &g_SoundUpateOnSPUsEnabled);
	bank.AddToggle("WaitForCategoryResults", &g_WaitForCategoryResults);
	bank.AddButton("ReloadSoundUpdateJob", CFA(ReloadSoundUpdateJob));
	bank.AddSlider("g_SoundUpdateScratchFree", &g_SoundUpdateScratchFree, 0, 256*1024, 1);
	bank.AddSlider("g_SoundUpdateCodeSize", &g_SoundUpdateCodeSize, 0, 256*1024, 1);
	bank.AddSlider("g_EnvironmentUpdateCodeSize", &g_EnvironmentUpdateCodeSize, 0, 256*1024, 1);
	bank.AddSlider("g_EnvironmentUpdateScratchFree", &g_EnvironmentUpdateScratchFree, 0, 256*1024, 1);
	bank.AddToggle("g_DebugSoundUpdateJob",&g_DebugSoundUpdateJob);
#endif	

	bank.AddSlider("TimeScale", &m_Timers[0].timeScale, 0.0f, 10.f, 0.1f);
	bank.AddToggle("DrawTime",&g_ShowAudioTime);
	bank.AddButton("Start counting created sounds", CFA(CountCreatedSoundsCB));
	bank.AddButton("Stop and log created sounds", CFA(LogSCreatedSoundsCB));
	audSound::AddWidgets(bank);

	bank.PopGroup();
}
#endif

} // namespace rage
