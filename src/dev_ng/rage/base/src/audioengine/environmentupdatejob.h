// 
// audioengine/environmentupdatejob.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_ENVIRONMENTUPDATEJOB_H
#define AUD_ENVIRONMENTUPDATEJOB_H

#if __PS3

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(environmentupdatejob);
#include "enginedefs.h"
namespace rage
{
class audMixerSyncManager;
ALIGNAS(16)
struct audEnvironmentUpdateJobInputData
{
	u32 numSoundSlotsPerBucket;
	u32 soundSlotSize;
	u32 audSoundPoolBucketSize;
	u32 numReqSetSlotsPerBucketAligned;
	u32 reqSetSlotSize;
	void *bucketLocks;
	u32 numBucketsToProcess;

	void *resolvedCategorySettings;
	size_t sizeOfResolvedCategorySettings;
	void *environmentManager;
	size_t sizeOfEnvironmentManager;
	void *mixGroups;
	size_t sizeOfMixGroups;
	u32 *mixGroupReferenceCounts;
	u32 *mixGroupInstanceIds;
	u32 environmentListenerIndex;
	u32 numOutputChannels;
	audMixerSyncManager *syncManagerEA;
	audTimerState timers[g_NumAudioTimers];

	void *soundPoolBufferEA;
	u32 requestedSettingsReadIndex;

	BANK_ONLY(f32 ambisonicDecoderScaling;)
	
};

};
#endif // __PS3
#endif // AUD_ENVIRONMENTUPDATEJOB_H
