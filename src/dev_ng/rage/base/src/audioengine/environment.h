//
// audioengine/environment.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_ENVIRONMENT_H
#define AUD_ENVIRONMENT_H

#if __SPU
#include "vector/vector3_consts_spu.cpp"
#endif

#include "atl/array.h"

#include "audiohardware/driverdefs.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/voicesettings.h"
#include "math/float16.h"


#include "ambisonics.h"
#include "compressedvolume.h"
#include "curve.h"
#include "environment_game.h"

#include "vectormath/vec3v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

namespace rage 
{
class audEnvironmentSound;
class audCategory;
class audCurve;
class audMixerSubmix;
class bkBank;

const u32 g_MaxCustomVolumeCurves = 32;

const u32 g_maxListeners = 2;

// Have 4 buffers, so we can look at the previous one to work out camera movement, for doppler, etc.
const u32 g_audListenerBuffers = 4;

extern const char *g_SourceReverbCurveNames[g_NumSourceReverbs];

extern __THREAD u32 g_ThreadSpecificListenerIndex;

#if !AUD_GAME_OVERRIDDEN_VOLUME_IMPLEMENTATION

#if __PS3
typedef audCompressedVolume8 audCompressedVolume;
#else
typedef audCompressedVolume32 audCompressedVolume;
#endif

#endif // AUD_GAME_OVERRIDDEN_VOLUME_IMPLEMENTATION

#if !AUD_GAME_OVERRIDDEN_ENVIRONMENT
// PURPOSE
//  audEnvironmentGameMetric includes all the various parameters that determine how occluded a sound is. This will
//  include a measure of the amount and type of occlusion.
//  This is now just an interface, and along with game-specific occlusion groups, you also create a game-specific occlusion metric.
//  This may also include environmental factors, since occl groups makes sense for calculating more than just occlusion. May rename to reflect this.
class audEnvironmentGameMetric
{
public:
	audEnvironmentGameMetric(){};
	~audEnvironmentGameMetric(){};

private:
};
#endif // AUD_GAME_OVERRIDDEN_ENVIRONMENT

#if !AUD_GAME_OVERRIDDEN_ROUTING

struct audRoutingMetric
{
	f32 sourceEffectDryMix;
	f32 sourceEffectWetMix;
	s8 sourceEffectSubmixId;
	s8 effectRoute;
};
#endif // AUD_GAME_OVERRIDDEN_ROUTING

// PURPOSE
//	Contains the things that the environment sound needs to pass in and get back from the audGameEnvironment::CalculateEnvironmentSoundMetrics() fn
struct audEnvironmentSoundMetrics
{
public:

	audEnvironmentSoundMetrics() :
		
			minSmallSend(0.f),
			minMediumSend(0.f),
			minLargeSend(0.f),
			requestedVolume(0.0f),
			postSubmixVolumeAttenuation(0.f),
			volume(0.0f),
			
			LPFCutoff(kVoiceFilterLPFMaxCutoff),
			HPFCutoff(0),
			dopplerFactor(1.0f),
			dopplerFrequencyScalingFactor(1.0f),
			pan(-1),
			positionRelativePan(0.0f),
			usePositionRelativePan(false),
			volumeCurveScale(1.0f),
			volumeCurveId(0xff),
			volumeCurvePlateau(0),
			hpfCurveId(0xff),
			lpfCurveId(0xff),
			environmentalLoudness(0.0f),
			currentPeakLevel(0),
			categoryIndex(-1),
			speakerMask(0),
			listenerMask(0xff),	
			hasQuadSpeakerLevels(false),
			shouldAttenuateOverDistance(true),
			shouldApplyEnvironmentalEffects(true),
			shouldInvertPhase(false),
			positionHasUpdated(false),
			voiceSettings(NULL)
	  {

	  };

	Vec3V position;
	Vec3V velocity;

	audEnvironmentGameMetric environmentGameMetric;
	audRoutingMetric routingMetric;
	
	float minSmallSend;
	float minMediumSend;
	float minLargeSend;

	audCompressedVolume32 requestedVolume;
	audCompressedVolume32 postSubmixVolumeAttenuation;
	audCompressedVolume32 volume;

	audVoiceSettings *voiceSettings;

	float dopplerFactor;
	float dopplerFrequencyScalingFactor;
	float volumeCurveScale;
	float environmentalLoudness;

	float positionRelativePan;

	u16 currentPeakLevel;

	s16 categoryIndex;
	s16 pan;
	u16 LPFCutoff;
	u16 HPFCutoff;

	u8 volumeCurveId;
	u8 volumeCurvePlateau;
	u8 hpfCurveId;
	u8 lpfCurveId;
	u8 speakerMask;
	u8 listenerMask;

#if __PS3
	u8 soundSlotId;
#endif

	bool hasQuadSpeakerLevels : 1;
	bool shouldAttenuateOverDistance : 1;
	bool shouldApplyEnvironmentalEffects : 1;
	bool shouldInvertPhase : 1;
	bool positionHasUpdated : 1;
	bool rearAttenuationDisabled : 1;
	bool rearFilteringDisabled : 1;
	bool usePositionRelativePan : 1;


private:
	audEnvironmentSoundMetrics(const audEnvironmentSoundMetrics&);     // do not give these a body
	audEnvironmentSoundMetrics& operator = (const audEnvironmentSoundMetrics&);
};


struct audEnvironmentMetricsInternal
{
	audEnvironmentMetricsInternal() : 
	reverbDry(1.f),
	postSubmixVolumeAttenuation(0.0f),
	volume(0.f),
	requestedVolume(0.f),
	virtualisationScore(0)
	{
	
	}

	void Init(const audEnvironmentSoundMetrics *metrics)
	{
		environmentSoundMetrics = metrics;

		// we need to copy the values that we're going to calculate per listener
		dopplerFrequencyScalingFactor = metrics->dopplerFrequencyScalingFactor;
		LPFCutoff = metrics->LPFCutoff;
		HPFCutoff = metrics->HPFCutoff;

		postSubmixVolumeAttenuation = metrics->postSubmixVolumeAttenuation.GetLinear();
		virtualisationScore = 0;
		
		volume = metrics->volume.GetLinear();
		requestedVolume = metrics->requestedVolume.GetLinear();
	}

	const audEnvironmentSoundMetrics *environmentSoundMetrics;

	atRangeArray<audCompressedVolume32, g_MaxOutputChannels> relativeChannelVolumes;
	atRangeArray<audCompressedVolume32, g_MaxOutputChannels> relativeDryChannelVolumes;
	atMultiRangeArray<audCompressedVolume32, g_NumSourceReverbChannels, g_NumSourceReverbs> relativeWetChannelVolumes;
	atRangeArray<audCompressedVolume32, g_NumSourceReverbs> reverbWets;

	audCompressedVolume32 reverbDry;
	audCompressedVolume32 postSubmixVolumeAttenuation;
	audCompressedVolume32 volume;
	audCompressedVolume32 requestedVolume;

	float dopplerFrequencyScalingFactor;
	u16 LPFCutoff;
	u16 HPFCutoff;

	u32 virtualisationScore;	
};

struct ALIGNAS(16) audEnvironmentListenerData
{
	audEnvironmentListenerData() : 
		Contribution(0.f),
		UpdatedTime(0),
		ActualSpeed(0.f),
		JumpedThisFrame(false),
		IsInInterior(false)
		{
			PanningMatrix.SetIdentity3x3();
			VolumeMatrix.SetIdentity3x3();
			PanningTransform.SetIdentity3x3();
			Velocity.ZeroComponents();
		}

	Mat34V VolumeMatrix;
	Mat34V PanningMatrix;
	Mat34V PanningTransform;
	

	Vec3V Velocity;
	f32 Contribution;
	f32 CosRearConeAngle;
	f32 CosConeAngle;
	f32 CloseDirectionalMicAttenuation;
	f32 FarDirectionalMicAttenuation;
	f32 RollOff;
	f32 ActualSpeed;
	u32 UpdatedTime;
	u32 RearAttenuationType;

	bool JumpedThisFrame;
	bool IsInInterior;
};


#if __SPU
extern const audEnvironmentListenerData *g_audListenerData;
#endif

// PURPOSE
//	This class contains all audio environment functionality, including listener position/orientation, 
//	distance attenuation, doppler, etc
//  EnvironmentSounds call out to the singleton AUDENVIRONMENT for centralised environmental metrics.
//	Games can override the behaviour by replacing environment_game.cpp
class audEnvironment
{

public:

	audEnvironment();
	~audEnvironment();

#if !__SPU

	// PURPOSE
	//	Initialises the class - must be called after audEngineConfig is initialised.
	void Init();

	// PURPOSE
	//	Shuts down the environment.
	void Shutdown(){};

	// PURPOSE
	//	Sets the virtual listener position/orientation. At present, no generic method of moving the microphone
	//  relative to the camera exists. If you wish to have a microphone positioned differently from the camera,
	//  do this in game-code, and pass the new matrix into this function.
	//  A flexible way of doing this generically may be added, as implementing a directional microphone would 
	//  need to be a core part of the engine.
	//  Different games may use different coordinate systems (orientation and scaling). A game->audio transformation
	//  matrix is calculated on audEnvironment::Init(), and applied to this listener matrix. The coord system is 
	//  specified in engineSettings.xml in BuildData\Config. 
	// NOTE: Currently (30/03/06) this is fudged, with the engine checking for either 'normal' coords or 'gta' coords,
	//  determined by the specified forward and up vectors. This will be changed to simply specify the transformation
	//  matrix itself in engineSettings.xml.
	// PARAMS
	//	listener - matrix containing listener position/orientation
//	void SetListenerMatrix(const Matrix34 &listener);
	// NOTE: We now (19/10/06) support multiple listeners, passed in all at once via a big array - even unused ones must be specified.
	//       See alternate method of just passing one, below. - actually, we don't yet support that :-)
//	void SetListenerMatrices(const Matrix34 *listeners);

	// This functions return false if the listener matrix is wrong (and disables the listener ), true otherwise.
	bool SetVolumeListenerMatrix(Mat34V_In listener, const u32 listenerId = 0);
	bool SetPanningListenerMatrix(Mat34V_In listener, const u32 listenerId = 0);

	bool SetVolumeListenerMatrix(const Matrix34 &listener, const u32 listenerId = 0)
	{
		return SetVolumeListenerMatrix(MATRIX34_TO_MAT34V(listener), listenerId); 
	}
	bool SetPanningListenerMatrix(const Matrix34 &listener, const u32 listenerId = 0)
	{
		return SetPanningListenerMatrix(MATRIX34_TO_MAT34V(listener), listenerId);
	}

	// PURPOSE
	//  Commits this frame's listener settings. Must be called, or listener position will never change.
	//  Similarly to requested settings, this is triple buffered, with an atomic index change, to achieve
	//  thread-safe atomic settings without any actual locking. Don't call this directly - call the one in g_AudioEngine
	//  which in turn calls this.
	void CommitListenerSettings(u32 timeInMs);

	void SetListenerContribution(f32 contribution, u32 listenerId)
	{
		GetListener(listenerId).Contribution = contribution;
	}

	void SetIsListenerInInterior(bool isInInterior, u32 listenerId = 0)
	{
		GetListener(listenerId).IsInInterior = isInInterior;
	}

	void InitReverbCurves();

#endif// !__SPU


	const audEnvironmentListenerData &GetListener(const u32 listenerId) const
	{
		FastAssert(listenerId < g_maxListeners);
#if __SPU && !defined(__AUD_ENVIRONMENT_UPDATE)
		return g_audListenerData[listenerId];
#else
		return m_Listeners[g_ThreadSpecificListenerIndex][listenerId];
#endif
	}

	// only the const version is available in the sound task
#if !__SPU || defined(__AUD_ENVIRONMENT_UPDATE)
	audEnvironmentListenerData &GetListener(const u32 listenerId)
	{
		FastAssert(listenerId < g_maxListeners);
		return m_Listeners[g_ThreadSpecificListenerIndex][listenerId];
	}
#endif

	bool IsListenerInUse(u32 listenerId) const
	{
		const s32 *p = (const s32*)&GetListener(listenerId).Contribution;
		return (*p != 0);
	}

	f32 GetListenerContribution(u32 listenerId) const
	{
		return GetListener(listenerId).Contribution;
	}

	// PURPOSE
	//	Returns the virtual listener position
	Vec3V_Out GetVolumeListenerPosition(u32 listenerId = 0) const
	{
		return GetListener(listenerId).VolumeMatrix.GetCol3();
	}
	Vec3V_Out GetPanningListenerPosition(u32 listenerId = 0) const
	{
		return GetListener(listenerId).PanningMatrix.GetCol3();
	}

	// PURPOSE
	//	Returns the virtual listener position last frame
	Vec3V_Out GetVolumeListenerPositionLastFrame(u32 listenerId = 0) const
	{
		Assert(listenerId<g_maxListeners);
		return m_VolumeListenerPositionLastFrame[listenerId];
	}
	// PURPOSE
	//	Returns the virtual listener matrix
	Mat34V_Out GetVolumeListenerMatrix(u32 listenerId = 0) const
	{
		return GetListener(listenerId).VolumeMatrix;
	}
	Mat34V_Out GetPanningListenerMatrix(u32 listenerId = 0) const
	{
		return GetListener(listenerId).PanningMatrix;
	}
	Vec3V_Out GetListenerVelocity(u32 listenerId = 0) const
	{
		return GetListener(listenerId).Velocity;
	}

	f32 GetActualListenerSpeed(u32 listenerId = 0) const
	{
		return GetListener(listenerId).ActualSpeed;
	}

	bool GetIsListenerInInterior(u32 listenerId = 0) const
	{
		return GetListener(listenerId).IsInInterior;
	}

	// PURPOSE
	//	Calculates position relative to listener. The supplied worldPosition is first transformed according to the
	//  listener transformation matrix specified in engineSettings.xml. All internal position-related calculations
	//  may then be done in a game-coord independent way.
	// PARAMS
	//	worldPosition - vector containing the position in world terms.
	//  listenerId - id of the listener to use
	// RETURNS
	//	Position relative to listener position/orientation
	// 
	Vec3V_Out ComputePositionRelativeToPanningListener(Vec3V_In worldPosition, const u32 listenerId = 0, const f32 positionRelativePan = 0.0f) const;
	

	// PURPOSE
	//  Calculates the distance to the nearest listener. Typically in game-code, we get the listener position, and then see how close it is,
	//  for culling. Instead of having to iterate through, use this function.
	f32 ComputeDistanceToClosestVolumeListener(Vec3V_In worldPosition) const
	{
		// Loop through all active listeners and work out the distance, returning the smallest one.
		ScalarV smallestDistanceSqdV(FLT_MAX);
		ASSERT_ONLY(u32 listenersInUse = 0;)
		for (u32 i=0; i<g_maxListeners; i++)
		{
			if (IsListenerInUse(i))
			{
				ASSERT_ONLY(listenersInUse ++;)
				const ScalarV distanceSqdV = ComputeSqdDistanceRelativeToVolumeListenerV(worldPosition,i);
				smallestDistanceSqdV = Min(distanceSqdV, smallestDistanceSqdV);	 // takes the min
			}
		}
		Assertf(listenersInUse != 0, "Current number of listener in use: %d, not all of them are in use.",listenersInUse);
		// We must have at least one listener (must we?)
		Assertf(smallestDistanceSqdV.Getf() >= 0.0f && smallestDistanceSqdV.Getf() < FLT_MAX, "smallestDistanceSqd: %f", smallestDistanceSqdV.Getf());
		return Sqrt(smallestDistanceSqdV).Getf();
	}

	f32 ComputeDistanceToClosestPanningListener(Vec3V_In worldPosition) const;
	f32 ComputeDistanceToPanningListener(Vec3V_In worldPosition, const u32 listenerId = 0) const;
	
	f32 ComputeSqdDistanceToPanningListener( Vec3V_In worldPosition,const u32 listenerId = 0) const;
	f32 ComputeSqdDistanceToPanningListener(const Vector3 &worldPosition,const u32 listenerId = 0) const
	{
		return ComputeSqdDistanceToPanningListener(VECTOR3_TO_VEC3V(worldPosition), listenerId);
	}
	f32 ComputeDistanceRelativeToVolumeListener(const Vector3 &worldPosition, u32 listenerId = 0) const
	{
		return ComputeDistanceRelativeToVolumeListener(VECTOR3_TO_VEC3V(worldPosition), listenerId);
	}
	f32 ComputeDistanceRelativeToVolumeListener(Vec3V_In worldPosition, u32 listenerId = 0) const
	{
		Assert(listenerId<g_maxListeners);
		Vec3V relativePosition;
		relativePosition = worldPosition - GetVolumeListenerMatrix(listenerId).GetCol3();
		return Mag(relativePosition).Getf();
	}
	ScalarV_Out ComputeSqdDistanceRelativeToVolumeListenerV(Vec3V_In worldPosition, u32 listenerId = 0) const
	{
		Assert(listenerId<g_maxListeners);
		return DistSquared(worldPosition, GetVolumeListenerMatrix(listenerId).GetCol3());
	}

	// PURPOSE
	//	Gets the audio attenuation (in dB) for a specified distance from a lookup table.
	// TODO: this should be renamed to a generic curve function once we support multiple curves
	//	PARAMS
	//	curve - the roll off curve to use
	//	curveScale - the scale of the curve
	//	distance - the distance in metres
	f32 GetDistanceAttenuation(const u32 curveId, f32 curveScale, f32 distance) const;

	// PURPOSE
	//  Set the directional mic variables, so these may be altered during play, for special effects.
	// PARAMS
	//  frontCosConeAngle - the cosine of the angle of the front cone - hence 1.0 for no cone.
	//  rearCosConeAngle - the cosine of the angle of the rear cone - measured out the front of the camera,
	//       hence -1.0 for no rear cone.
	//  attenuation - the attenuation applied to sounds falling fully within the rear cone - this value is ADDED to the
	//       volume, hence should be negative for quietening sounds in the rear.
	void SetDirectionalMicParameters(f32 frontCosConeAngle, f32 rearCosConeAngle, f32 closeAttenuation, f32 farAttenuation,const u32 rearAttenuationType,u32 listenerId);

	void SetRollOff(f32 rollOff,u32 listenerId);

	f32 GetRollOff(u32 listenerId = 0) const;

	// PURPOSE
	//  Gets the attenuation (in dB) due to the relative position of the emitter to the mic. Two cones are set up,
	//  pointing out the front and back along the axis of the camera - anything fully in the rear cone is attenuated by
	//  m_DirectionalMicAttenuation, anything fully in the front cone isn't, and anything inbetween is attenuated by an
	//  interpolated amount.
	f32 GetDirectionalMicAttenuation(u32 listenerId,Vec3V_In relativePosition,f32 distanceToPanningListener, f32 *frontBackRatio = NULL) const;

		// PURPOSE
	//	Calculates the (scaled) doppler frequency scaling factor for a given relative distance and timestep.
	// PARAMS
	//	relativeDistanceDelta - the change in distance
	//	timeStepMs	-	the time in milliseconds over which this distance change has occurred
	//	dopplerFactor - scaling factor
	f32 ComputeDopplerFrequencyScalingFactor(f32 relativeDistanceDelta, u32 timeStepMs, f32 dopplerFactor) const;

	void ComputeSpeakerVolumesFromPosition(Vec3V_In relativePosition, audEnvironmentMetricsInternal *metrics, const u32 numChannels);

	void ComputeSpeakerVolumesFromPan(audEnvironmentMetricsInternal *metrics, const u32 numChannels);
	
	void ComputeSpeakerVolumesFromSpeakerMask(audEnvironmentMetricsInternal *metrics, const u32 numChannels);

	void ComputeSpeakerVolumesFromQuadLevels(audEnvironmentMetricsInternal *metrics, const u32 numChannels);
	
	Vec3V_Out ComputePositionFromPan(const s32 pan);
	Vec3V_Out ComputePositionFromPan(const f32 pan);

	void CalculateEnvironmentSoundMetrics(audEnvironmentSoundMetrics *metrics, u32 &virtualisationScore);

	void CalculateEnvironmentSoundMetricsForSingleListener(audEnvironmentMetricsInternal *metrics, u32 listenerId);

	void CombineEnvironmentalSoundMetrics(audEnvironmentMetricsInternal *metrics, 
		f32 *listenerContribution, u32 numListenersInUse,const audEnvironmentMetricsInternal *individualMetrics);

	void CalculateEnvironmentEffectsMetricsForSingleListener(audEnvironmentMetricsInternal *metrics, u32 listenerId);

	void CalculateReverbSends(f32 *sends, f32 reverbWet, f32 reverbSize);

#if __BANK
	// PURPOSE
	void AddWidgets(bkBank &bank);
	void AddGameWidgets(bkBank &bank);
#endif // __BANK


#if __SPU
	static void SetListenerAudioThreadIndex(const u32 index)
	{
		g_ThreadSpecificListenerIndex = index;
	}
#endif

	void UpdateListenerAudioThreadIndex(const u32 unwrappedWriteIndex)
	{
		// Only call this on the audio thread
		m_ListenerAudioThreadIndex = (unwrappedWriteIndex - 1 + g_audListenerBuffers) % g_audListenerBuffers;
		g_ThreadSpecificListenerIndex = m_ListenerAudioThreadIndex;

		audAssertf(m_ListenerAudioThreadIndex != m_ListenerGameThreadIndex, "%u,%u,%u", unwrappedWriteIndex, m_ListenerAudioThreadIndex, m_ListenerGameThreadIndex);
	}

	u32 FindVolumeCurveId(const u32 curveHash);
	u32 FindHPFCurveId(const u32 curveHash);
	u32 FindLPFCurveId(const u32 curveHash);
	void InitVolumeCurve(const char *curveName, const u32 curveId);

#if AUD_ENABLE_AMBISONICS
	void SetDecoder(const ambisonicDecodeData &data); 
#endif

	AUD_GAME_ENVIRONMENT_PUBLIC_DECLARATIONS;

#if !AUD_GAME_OVERRIDDEN_ROUTING
	audMixerSubmix *GetMasterSubmix() const;
	audMixerSubmix *GetSfxSubmix() const;
	audMixerSubmix *GetMusicSubmix() const;
	audMixerSubmix *GetSourceReverbSubmix(const u32 index) const;
#endif

	u32 GetListenerAudioThreadIndex() const 
	{
		return m_ListenerAudioThreadIndex;
	}

#if RSG_BANK
	void OnCurveModified(const u32 nameHash);
#endif
private:

	static void SetupCustomCurve(audThreePointPiecewiseLinearCurve &curve, const u32 curveNameHash);

	// PURPOSE
	//	Called to set up required mixer routes
	// NOTES
	//	Define AUD_GAME_OVERRIDEN_ROUTING 1 to provide a game-specific implementation in environment_game
	void InitRoutes();

	// PURPOSE
	//	Called after routes are created
	// NOTES
	//	Define AUD_GAME_OVERRIDDEN_ROUTE_EFFECTS 1 to provide a game-specific implementation in environment_game
	void InitRouteEffects();

	// PURPOSE
	//	Called to set up routing for a given environment sound
	// NOTES
	//	Define AUD_GAME_OVERRIDEN_ROUTING 1 to provide a game-specific implementation in environment_game
	void ComputeVoiceRoutes(audEnvironmentMetricsInternal *metrics) const;

	void GameInit();
	void GameUpdate(const u32 timeInMs);
	
	void CopyDryVolumesToReverbs(audEnvironmentMetricsInternal *metrics);


#if AUD_ENABLE_AMBISONICS
	//Data used to perform ambisonic encoding/decoding in conjunction with audAmbisonics
	ambisonicDecodeData m_DecodeData;
#endif

	// We have arrays of Listener Matrices and Transforms, to allow for multi-listener support. 
	atMultiRangeArray<audEnvironmentListenerData,g_audListenerBuffers,g_maxListeners> m_Listeners;

	// Only ever use this from the game frame
	atRangeArray<Vec3V, g_maxListeners> m_VolumeListenerPositionLastFrame;

	atRangeArray<audCurve, g_NumSourceReverbs> m_SourceReverbCrossfadeCurves;
		
	// Only ever update this from the game frame
	u32 m_ListenerGameThreadIndex;
	// Only ever update this from the audio frame
	u32 m_ListenerAudioThreadIndex;
	
	f32 m_SpeedOfSoundMps,m_MaxDopplerRelativeVelMps;

	atRangeArray<u32, g_MaxCustomVolumeCurves> m_VolumeCurveNameHashes;
	atRangeArray<audCurve, g_MaxCustomVolumeCurves> m_VolumeCurves;

	enum 
	{
		kMaxLPFCurves = 4,
		kMaxHPFCurves = 4
	};
	atFixedArray<audThreePointPiecewiseLinearCurve, kMaxLPFCurves> m_LPFCurves;
	atFixedArray<u32, kMaxLPFCurves> m_LPFCurveNames;
	atFixedArray<audThreePointPiecewiseLinearCurve, kMaxHPFCurves> m_HPFCurves;
	atFixedArray<u32, kMaxHPFCurves> m_HPFCurveNames;

	static f32 sm_CloseAttenuationDistance;
	static f32 sm_FarAttenuationDistance;
#if !AUD_GAME_OVERRIDDEN_ROUTING
	s8 m_ReverbSubmixIds[3];
	s8 m_ListenerSubmixId;
	s8 m_MusicSubmixId;
	s8 m_FrontendSubmixId;
	s8 m_SFXSubmixId;
#endif

	AUD_GAME_ENVIRONMENT_PRIVATE_DECLARATIONS;
};

extern audEnvironment *g_audEnvironment;

} // namespace rage

#endif // AUD_ENVIRONMENT_H
