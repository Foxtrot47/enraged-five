// 
// audioengine/compressedvolume.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "compressedvolume.h"

namespace rage {
	audCompressedVolume32& audCompressedVolume32::operator=(const audCompressedVolume16 &rhs)
	{
		m_Data = rhs.GetLinear();
		return *this;
	}

	audCompressedVolume32& audCompressedVolume32::operator=(const audCompressedVolume8 &rhs)
	{
		m_Data = rhs.GetLinear();
		return *this;
	}

	audCompressedVolume16& audCompressedVolume16::operator=(const audCompressedVolume32 &rhs)
	{
		SetLinear(rhs.GetLinear());
		return *this;
	}

	audCompressedVolume16& audCompressedVolume16::operator=(const audCompressedVolume8 &rhs)
	{
		SetLinear(rhs.GetLinear());
		return *this;
	}

	audCompressedVolume8& audCompressedVolume8::operator=(const audCompressedVolume32 &rhs)
	{
		SetLinear(rhs.GetLinear());
		return *this;
	}

	audCompressedVolume8& audCompressedVolume8::operator=(const audCompressedVolume16 &rhs)
	{
		SetLinear(rhs.GetLinear());
		return *this;
	}
} // namespace rage
