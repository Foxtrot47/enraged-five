// 
// audioengine/environmentupdatejob.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "vector/vector3_consts_spu.cpp"

#include "environmentupdatejob.h"

using namespace rage;

#if __SPU

#define AUD_IMPL

#define __AUD_ENVIRONMENT_UPDATE 1

#include "dynamicmixmgr.h"
#include "environment.h"
#include "environment.cpp"
#include "environment_game.cpp"
#include "requestedsettings.cpp"

#include "soundpool.cpp"
#include "widgets.cpp"
#include "curve.cpp"
#include "audiosoundtypes/sound.cpp"
#include "audiosoundtypes/environmentsound.cpp"
#include "audiohardware/pcmsource_interface.cpp"
#include "audiohardware/syncsource.cpp"
#include "string/stringhash.cpp"
#include "system/cellsyncmutex.cpp"
#include "spuutil.h"


namespace rage
{
	tCategorySettings *g_ResolvedCategorySettings = NULL;
	audSoundPoolBucket *g_Bucket = NULL;
	audMixGroup *g_MixGroupPoolMem = NULL;
	u32 *g_MixGroupReferenceCountsEa = NULL;
	u32 *g_MixGroupInstanceIdsEa = NULL;

	u32 g_BucketSoundSlotSize;
	u32 g_NumSoundSlotsPerBucket;
	u32 g_NumRequestedSettingsSlotsPerBucketAligned;
	u32 g_BucketRequestedSettingsSlotSize;

	bool g_DebugSoundUpdateJob_SPU = false;

	BANK_ONLY(extern float g_AmbisonicsDecoderScaling;)

	audCommandBuffer *g_MixerCommandBuffer = NULL;
	
	u32 audDriver::m_NumOutputChannels;

	const PcmSourceState *g_PcmSourceState = NULL;
	audEnvironment *g_audEnvironmentEA = NULL;
	
	void EnvironmentUpdateJob(sysTaskParameters &p)
	{
		InitScratchBuffer(p.Scratch.Data, p.Scratch.Size);

		TrapNE(p.Input.Size, sizeof(audEnvironmentUpdateJobInputData));
		audEnvironmentUpdateJobInputData *data = (audEnvironmentUpdateJobInputData*)p.Input.Data;
		
		const u32 numBuckets = p.UserData[1].asUInt;
		const u32 commandBufferSize = p.UserData[5].asUInt;
		void *commandBufferEA = p.UserData[4].asPtr;

		BANK_ONLY(g_DisplayAllocations = g_DebugSoundUpdateJob_SPU = p.UserData[7].asBool);

		audDriver::SetNumOutputChannels(data->numOutputChannels);

		g_NumSoundSlotsPerBucket = data->numSoundSlotsPerBucket;
		g_BucketSoundSlotSize = data->soundSlotSize;
		g_NumRequestedSettingsSlotsPerBucketAligned = data->numReqSetSlotsPerBucketAligned;
		g_BucketRequestedSettingsSlotSize = data->reqSetSlotSize;

		audRequestedSettings::SetReadIndex(data->requestedSettingsReadIndex);

		BANK_ONLY(g_AmbisonicsDecoderScaling = data->ambisonicDecoderScaling;)

		audMixerSyncManager::sm_InstanceEA = data->syncManagerEA;

		Assert(p.ReadOnlyCount == 1);
		g_PcmSourceState = (PcmSourceState*)p.ReadOnly[0].Data;
		Assert(p.ReadOnly[0].Size == sizeof(PcmSourceState) * kMaxPcmSources);
		
		// check PPU and SPU sizes match
		TrapNE(data->sizeOfEnvironmentManager, sizeof(audEnvironment));
		g_ResolvedCategorySettings = (tCategorySettings*)grabData(data->resolvedCategorySettings, data->sizeOfResolvedCategorySettings, "ResolvedCategorySettings");
		g_audEnvironment = (audEnvironment*)grabData(data->environmentManager, data->sizeOfEnvironmentManager, "EnvironmentManager");
		g_audEnvironmentEA = (audEnvironment*)data->environmentManager;
		g_MixGroupPoolMem = (audMixGroup *)grabData(data->mixGroups, data->sizeOfMixGroups, "MixGroups");
		g_MixGroupReferenceCountsEa = data->mixGroupReferenceCounts;
		g_MixGroupInstanceIdsEa = data->mixGroupInstanceIds;

		audCommandBuffer mixerCommandBuffer;
		const u32 localCommandBufferSize = Min(commandBufferSize, 4096U);
		mixerCommandBuffer.Init(commandBufferEA, commandBufferSize, 
			(u8*)AllocateFromScratch(localCommandBufferSize, "Mixer Command Buffer"), localCommandBufferSize, 
			"EnvironmentUpdate");

		g_MixerCommandBuffer = &mixerCommandBuffer;

		SetScratchBookmark();

		const u32 soundBucketSize = g_BucketSoundSlotSize * g_NumSoundSlotsPerBucket;
		const u32 requestedSettingsBucketSize = g_BucketRequestedSettingsSlotSize * g_NumRequestedSettingsSlotsPerBucketAligned;

		u32 numBucketsProcessed = 0;
		bool finished = false;

		while(!finished && numBucketsProcessed++ < data->numBucketsToProcess)
		{
			const u32 bucketId = sysInterlockedIncrement((u32*)p.UserData[0].asPtr) - 1;
			if(bucketId >= numBuckets)
			{
				finished = true;
				break;
			}
			else
			{				
				// reset the scratch ptr - overwriting the previous buckets data
				ResetScratchToBookmark();

#if AUD_SOUNDPOOL_LOCK_ON_SPU
				// lock the bucket before grabbing the header
				rageCellSyncMutexLock((u32)(data->bucketLocks)+ 4 * bucketId);
#endif
				// process this bucket
				void *bucketEa = (char*)p.UserData[3].asPtr + (data->audSoundPoolBucketSize*bucketId);
				g_Bucket = (audSoundPoolBucket*)grabData(bucketEa, sizeof(audSoundPoolBucket), "BucketHeader");
			
				// need to cache the EAs since I'm going to overwrite them with LS
				void *baseSoundPtr = (u8 *)data->soundPoolBufferEA + (bucketId * (soundBucketSize + requestedSettingsBucketSize));
				void *baseRequestedSettingsPtr = (u8*)baseSoundPtr + soundBucketSize;

				void *localBaseSoundPtr = grabData(baseSoundPtr, soundBucketSize, "BucketSounds");
				void *localBaseReqSetPtr = grabData(baseRequestedSettingsPtr, requestedSettingsBucketSize, "BucketRequestedSettings");

				dmaWait();

				Assert(g_Bucket->baseSoundPtr == baseSoundPtr);
				Assert(g_Bucket->baseRequestedSettingsPtr == baseRequestedSettingsPtr);
								
				g_Bucket->baseSoundPtr = localBaseSoundPtr;
				g_Bucket->baseRequestedSettingsPtr = localBaseReqSetPtr;

				// only need to do this once, but we need to do it after the DMA has completed, so this is
				// a decent place do set this up (after the first dmaWait())
				g_audEnvironment->SetListenerAudioThreadIndex(data->environmentListenerIndex);

				for(u32 i = 0 ; i < g_Bucket->numEnvironmentSoundRequests; i++)
				{
					if(g_Bucket->environmentSoundRequestSlotIds[i] != 0xff)
					{					
						audEnvironmentSound *sound = (audEnvironmentSound *)((char*)g_Bucket->baseSoundPtr + data->soundSlotSize*g_Bucket->environmentSoundRequestSlotIds[i]);
						TrapZ((size_t)sound);
						TrapNE(sound->GetSoundTypeID(), EnvironmentSound::TYPE_ID);
						sound->AudioUpdateVoice(g_Bucket->environmentSoundRequests[i]);
					}
				}

				// dma results back to PPU

				// bucket data
				dmaPut(g_Bucket->baseSoundPtr, baseSoundPtr, g_BucketSoundSlotSize * g_NumSoundSlotsPerBucket, false);

				// fix up bucket header
				g_Bucket->baseSoundPtr = baseSoundPtr;
				g_Bucket->baseRequestedSettingsPtr = baseRequestedSettingsPtr;
				
				dmaPut(g_Bucket, bucketEa, sizeof(audSoundPoolBucket), false);

				// wait for transfers to complete
				dmaWait();


				// unlock the bucket
#if AUD_SOUNDPOOL_LOCK_ON_SPU
				rageCellSyncMutexUnlock((u32)(data->bucketLocks)+ 4 * bucketId);
#endif
			}
		}

		// DMA mixer commands back to main memory
		u128 *commandBufferUsed = (u128*)p.Output.Data;
		mixerCommandBuffer.Flush();
		(*commandBufferUsed)[0] = mixerCommandBuffer.GetMainOffset();
	}
}

void EnvironmentUpdateJob(sysTaskParameters &p)
{
	rage::EnvironmentUpdateJob(p);
}

#endif // __SPU
