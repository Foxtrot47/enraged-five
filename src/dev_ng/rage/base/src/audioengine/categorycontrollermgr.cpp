// 
// audioengine/categorycontrollermgr.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


//////////////////////////////////////////////////////////////////////////
//	Includes

#include "category.h"
#include "categorycontrollermgr.h"
#include "categorymanager.h"
#include "engine.h"
#include "environment.h"
#include "audiohardware/DriverUtil.h"

#include "math/simplemath.h"
#include "profile/profiler.h"
#include "system/task.h"

#if __BANK
#include "bank/bank.h"
#include "system/param.h"
#endif
//////////////////////////////////////////////////////////////////////////



#if __PS3
RELOAD_TASK_DECLARE(CategoryControllerJob);
#endif

namespace rage {

#if __BANK
PARAM(categorycontrolwidgets, "Add widget controls for every category");
#if __PS3
bool g_CategoryControllerUpdateJob = !__DEV;
#endif

bool audCategoryControllerManager::sm_PrintControllers = false;
#endif


PF_PAGE(AudioCatControlPage, "RAGE Audio Category Controllers");
PF_GROUP(AudioCatControl);
PF_LINK(AudioCatControlPage, AudioCatControl);

PF_TIMER(UpdateTime, AudioCatControl);
PF_TIMER(PackageCreationAndDeletion, AudioCatControl);
PF_TIMER(DestroyMultiple, AudioCatControl);
PF_TIMER(CreateMultiple, AudioCatControl);
PF_TIMER(LockWaitTime, AudioCatControl);
#if __PS3
PF_TIMER(SPUTaskWaitTime, AudioCatControl);
#endif


audCategoryControllerPackage::audCategoryControllerPackage() :
m_pControllerArray(NULL),
m_hashCategoryNames(NULL),
m_uNumControllers(0),
m_uCatNameStrideBytes(4),
m_bIsCreatingControllers(false),
m_bControllersCreated(false),
m_bIsDeletingControllers(false),
m_bErrorCreatingControllers(false)
{
}
audCategoryControllerPackage::~audCategoryControllerPackage()
{
	QueueDestroyControllers();
	while (!GetIsSafeToDestruct())
		sysIpcSleep(0);
}

bool audCategoryControllerPackage::QueueCreateControllers(audCategoryController** pControllerArray, const u32* hashCategoryNames, u8 numControllers, u8 strideBytes)
{
	if (m_uNumControllers!=0 || m_bIsDeletingControllers || m_bIsCreatingControllers)
		return false;

	m_pControllerArray = pControllerArray;
	m_hashCategoryNames = hashCategoryNames;
	m_uNumControllers = numControllers;
	m_uCatNameStrideBytes = strideBytes;
	m_bIsCreatingControllers = true;
	m_bErrorCreatingControllers = false;
	return m_bIsCreatingControllers = AUDCATEGORYCONTROLLERMANAGER.AddPackageToCreateList(this);
}
bool audCategoryControllerPackage::QueueDestroyControllers()
{
	if (m_uNumControllers==0 || m_bIsDeletingControllers || m_bIsCreatingControllers)
		return false;
	return m_bIsDeletingControllers = AUDCATEGORYCONTROLLERMANAGER.AddPackageToDeleteList(this);
}

audCategoryControllerManager* audCategoryControllerManager::sm_pInstance = NULL;


//	Constructor
audCategoryControllerManager::audCategoryControllerManager()
{
	audCategoryController::SetPools(GetControllerPool(), GetControllerSettingsPool());

	m_NumCategories = g_AudioEngine.GetCategoryManager().GetNumberOfCategories();

	const u32 numCategories = m_NumCategories;
	
	sysMemSet((void*)GetControllerPool(), 0, sizeof(audCategoryController) * g_MaxCategoryControllers);
	sysMemSet((void*)GetControllerSettingsPool(), 0, sizeof(audCategoryControllerSettings) * g_MaxCategoryControllers);
#if __PS3
	m_pControllerOutputArray = NULL;
	const u32 uNumCategoriesAligned = (m_NumCategories+15) & ~15;
	m_pControllerOutputArray = (audCategoryControllerOutput*)g_AudioEngine.AllocateVirtual(sizeof(audCategoryControllerOutput) * uNumCategoriesAligned, 16);
	for (u32 i = 0; i < uNumCategoriesAligned; ++i)
		m_pControllerOutputArray[i].SetDefaults();
	m_categoryUpdateHandle = NULL;
#endif
	
	m_NumControllers = 0;
	for (u32 i = 0; i < g_MaxCategoryControllers; ++i)
		m_FreeIndexStack[i] = i;

	for (int i = 0; i < m_PendingCreatePackages.GetMaxCount(); ++i)
	{
		m_PendingCreatePackages[i] = NULL;
		m_PendingDeletePackages[i] = NULL;
	}
	m_NumPendingCreatePackages = 0;
	m_NumPendingDeletePackages = 0;

	m_ControllerListIndices = NULL;
	const u32 uBytes = numCategories * sizeof(s16);
	m_uControllerListSizeBytesAligned = (uBytes+15) & ~15;
	m_ControllerListIndices = (s16*)g_AudioEngine.AllocateVirtual(m_uControllerListSizeBytesAligned, 16);
	for (u32 i = 0; i < numCategories; ++i)
		m_ControllerListIndices[i] = -1;

#if __BANK
	m_pWidgetActiveControls = NULL;
#endif
}

//	Destructor
audCategoryControllerManager::~audCategoryControllerManager()
{
	if (m_ControllerListIndices)
		g_AudioEngine.FreeVirtual(m_ControllerListIndices);
#if __PS3
	if (m_pControllerOutputArray)
		g_AudioEngine.FreeVirtual(m_pControllerOutputArray);
#endif
}

/*************************************************************************************
Function:	Instantiate
*************************************************************************************/
audCategoryControllerManager* audCategoryControllerManager::Instantiate()
{
	Assert(!sm_pInstance);
	sm_pInstance = rage_new audCategoryControllerManager;
	return sm_pInstance;
}

/*************************************************************************************
Function:	Destroy
*************************************************************************************/
void audCategoryControllerManager::Destroy()
{
	Assert(sm_pInstance);
	delete sm_pInstance;
	sm_pInstance = NULL;
}

/*************************************************************************************
Function:	CreateDynamicController
*************************************************************************************/

audCategoryController* audCategoryControllerManager::CreateDynamicController(const s32 heirachyIndex, const u32 hashCategoryName, bool BANK_ONLY(isSceneController))
{
	RAGE_TRACK(AudioController);

	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPoolLock);
	PF_STOP(LockWaitTime);

	audAssertf(m_NumControllers < (s32)g_MaxCategoryControllers, "[audCategoryControllerManager] Out of space in category controller list");
	if(m_NumControllers == (s32)g_MaxCategoryControllers)
	{
		audErrorf("[audCategoryControllerManager] Out of space in category controller pool. Reach max num of category controllers :%d",(s32)g_MaxCategoryControllers);
		return NULL;
	}

	audCategory* pCategory = g_AudioEngine.GetCategoryManager().GetDynamicCategoryPtr(heirachyIndex, hashCategoryName);
	audAssertf(pCategory, "[audCategoryControllerManager] Dynamic Category hash %u not found in mixgroup heirachy", hashCategoryName);
	if(pCategory == NULL)
	{
		audErrorf("[audCategoryControllerManager] Dynamic Category hash %u not found in mixgroup heirarchy", hashCategoryName);
		g_AudioEngine.GetDynamicMixManager().ActivateDebugPrinting();
		g_AudioEngine.GetDynamicMixManager().PrintDebug();
		return NULL;
	}

	audCategoryController* pNewController = NULL;
	Assert(m_FreeIndexStack[m_NumControllers] != ~1U);
	const u32 uPoolIndex = m_FreeIndexStack[m_NumControllers];
	pNewController = GetControllerPtrFromId(m_FreeIndexStack[m_NumControllers]);
	audCategoryControllerSettings* pNewSettings = GetControllerSettingsPtrFromId((s32)uPoolIndex);
	m_FreeIndexStack[m_NumControllers++] = ~1U; // Mark this one as used for debug purposes

	//	Explicit constructor call on memory space we know we own
	::new(pNewController) audCategoryController((u16)uPoolIndex);
	::new(pNewSettings) audCategoryControllerSettings();

	Assign(pNewController->m_iCategoryIndex, g_AudioEngine.GetCategoryManager().GetCategoryIndex(pCategory));

	{
		audCategoryController* pFirstController = GetListHeadFromCategoryIndex(pNewController->m_iCategoryIndex);
		if (pFirstController)
		{
			pNewController->SetNext(pFirstController);
			pFirstController->SetPrev(pNewController);
		}
		m_ControllerListIndices[pNewController->m_iCategoryIndex] = (s16)uPoolIndex;
	}

#if __BANK
	pNewController->SetIsSceneController(isSceneController);
	pNewController->SetIsDynamic(true);
#endif

	return pNewController;
}

/*************************************************************************************
Function:	CreateController
*************************************************************************************/
audCategoryController* audCategoryControllerManager::CreateController(const char* szCategoryName)
{
	return CreateController( atStringHash(szCategoryName) );
}

/*************************************************************************************
Function:	CreateController
*************************************************************************************/
audCategoryController* audCategoryControllerManager::CreateController(const u32 hashCategoryName, bool BANK_ONLY(isSceneController))
{
	RAGE_TRACK(AudioController);

	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPoolLock);
	PF_STOP(LockWaitTime);

	AssertMsg(m_NumControllers < g_MaxCategoryControllers , "[audCategoryControllerManager] Out of space in category controller list");
	if (m_NumControllers == g_MaxCategoryControllers)
	{
		audErrorf("[audCategoryControllerManager] Out of space in category controller pool. Reach max num category controllers: %d",g_MaxCategoryControllers);
		return NULL;
	}

	audCategory* pCategory = g_AudioEngine.GetCategoryManager().GetCategoryPtr(hashCategoryName);
	if (pCategory == NULL)
	{
		audAssertf(pCategory, "[audCategoryControllerManager] Category hash %i not found", hashCategoryName);
		audErrorf("[audCategoryControllerManager] Category hash %i not found", hashCategoryName);
		return NULL;
	}

	Assert(m_FreeIndexStack[m_NumControllers] != ~1U);
	const u32 uPoolIndex = m_FreeIndexStack[m_NumControllers];
	
	audCategoryController* pNewController = GetControllerPtrFromId((s32)uPoolIndex);
	audCategoryControllerSettings* pNewSettings = GetControllerSettingsPtrFromId((s32)uPoolIndex);

	m_FreeIndexStack[m_NumControllers++] = ~1U; // Mark this one as used for debug purposes

	//	Explicit constructor calls on memory spaces we know we own
	::new(pNewController) audCategoryController((u16)uPoolIndex);
	::new(pNewSettings) audCategoryControllerSettings();

	Assign(pNewController->m_iCategoryIndex, g_AudioEngine.GetCategoryManager().GetCategoryIndex(pCategory));

	{
		audCategoryController* pFirstController = GetListHeadFromCategoryIndex(pNewController->m_iCategoryIndex);
		if (pFirstController)
		{
			pNewController->SetNext(pFirstController);
			pFirstController->SetPrev(pNewController);
		}
		m_ControllerListIndices[pNewController->m_iCategoryIndex] = (s16)uPoolIndex;
	}

#if __BANK
	pNewController->m_IsSceneController = isSceneController;
#endif

	return pNewController;
}

/*************************************************************************************
Function:	CreateMultipleDynamicControllers
*************************************************************************************/
bool audCategoryControllerManager::CreateMultipleDynamicControllers(
        s32 heirachyIndex, audCategoryController** pOutControllers, const u32* hashCategoryNames, u32 numControllers, u32 strideBytes, bool isSceneController)
{
	// CreateDynamicController() also has a lock, but we need to make sure we lock it here so it will only lock the
	//	thread once rather than many times
	sysCriticalSection lock(m_ControllerPoolLock);
	
	const u8* bytePtr = (const u8*)hashCategoryNames;

	bool bFailed = false;
	u32 iCatIndex = 0;
	for (; iCatIndex < numControllers; ++iCatIndex, bytePtr += strideBytes)
	{
		hashCategoryNames = (const u32*)bytePtr;
		pOutControllers[iCatIndex] = CreateDynamicController(heirachyIndex, *hashCategoryNames, isSceneController);
		if (pOutControllers[iCatIndex] == NULL)	
		{
			audAssertf(0, "[audCategoryControllerManager] There was a problem creating category %u. Destroying the rest.", iCatIndex);
			audErrorf("[audCategoryControllerManager] There was a problem creating category %u. Destroying the rest.", iCatIndex);
			bFailed = true;
			break;
		}
	}

	if (bFailed)
	{
		for (u32 i = 0; i < iCatIndex; ++i)
			DestroyController(pOutControllers[i]);
		return false;
	}

	return true;
}


/*************************************************************************************
Function:	CreateMultipleControllers
*************************************************************************************/
bool audCategoryControllerManager::CreateMultipleControllers(
	audCategoryController** pOutControllers, const u32* hashCategoryNames, u32 numControllers, u32 strideBytes, bool isSceneController)
{
	PF_FUNC(CreateMultiple);

	// CreateController() also has a lock, but we need to make sure we lock it here so it will only lock the
	//	thread once rather than many times
	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPoolLock);
	PF_STOP(LockWaitTime);
	
	const u8* bytePtr = (const u8*)hashCategoryNames;

	bool bFailed = false;
	u32 iCatIndex = 0;
	for (; iCatIndex < numControllers; ++iCatIndex, bytePtr += strideBytes)
	{
		hashCategoryNames = (const u32*)bytePtr;
		pOutControllers[iCatIndex] = CreateController(*hashCategoryNames, isSceneController);
		if (pOutControllers[iCatIndex] == NULL)	
		{
			audAssertf(0, "[audCategoryControllerManager] There was a problem creating category %u. Destroying the rest.", iCatIndex);
			audErrorf("[audCategoryControllerManager] There was a problem creating category %u. Destroying the rest.", iCatIndex);
			bFailed = true;
			break;
		}
	}

	if (bFailed)
	{
		for (u32 i = 0; i < iCatIndex; ++i)
			DestroyController(pOutControllers[i]);
		return false;
	}

	return true;
}

/*************************************************************************************
Function:	DestroyController
*************************************************************************************/
void audCategoryControllerManager::DestroyController(audCategoryController* pController)
{
	Assert(pController);

	if (pController)
	{
		//	Make sure the audio thread doesn't get in the way
		PF_START(LockWaitTime);
		sysCriticalSection lock(m_ControllerPoolLock);
		PF_STOP(LockWaitTime);
		{
			audCategoryController* pNext = pController->GetNext();
			audCategoryController* pPrev = pController->GetPrev();

			if (pNext)
				pNext->SetPrev(pPrev);
			if (pPrev)
				pPrev->SetNext(pNext);
			else
			{
				if (pNext)
					m_ControllerListIndices[pController->m_iCategoryIndex] = (s16)pNext->GetPoolIndex();
				else
				{
					m_ControllerListIndices[pController->m_iCategoryIndex] = -1;

					//	If the controller is going away, we should make sure to reset these to their defaults
					audCategory *pCategory = pController->GetCategory();
					pCategory->SetVolume(0.f);
					pCategory->SetPitch(0);
					pCategory->SetLPFCutoff(kVoiceFilterLPFMaxCutoff);
					pCategory->SetHPFCutoff(0);
				}
			};

			Assert(m_NumControllers != 0);
			const s32 iPoolIndex = (s32)pController->GetPoolIndex();
			audCategoryControllerSettings* pSettings = AUDCATEGORYCONTROLLERMANAGER.GetControllerSettingsPtrFromId(iPoolIndex);

			//	Explicit destructor calls
			pController->~audCategoryController();
			pSettings->~audCategoryControllerSettings();

			m_FreeIndexStack[--m_NumControllers] = iPoolIndex;
		}
	}
}

/*************************************************************************************
Function:	DestroyMultipleControllers
*************************************************************************************/
void audCategoryControllerManager::DestroyMultipleControllers(audCategoryController** pControllerArray, const u32 numControllers)
{
	PF_FUNC(DestroyMultiple);

	// DestroyController() also has a lock, but we need to make sure we lock it here so it will only lock the
	//	thread once rather than many times
	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPoolLock);
	PF_STOP(LockWaitTime);

	for (u32 i = 0; i < numControllers; ++i)
	{
		DestroyController(pControllerArray[i]);
		pControllerArray[i] = NULL;
	}
}

bool audCategoryControllerManager::AddPackageToCreateList(audCategoryControllerPackage* pPackage)
{
	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPackageLock);
	PF_STOP(LockWaitTime);

	if (m_NumPendingCreatePackages != (u32)m_PendingCreatePackages.GetMaxCount())
	{
		m_PendingCreatePackages[m_NumPendingCreatePackages++] = pPackage;
		return true;
	}
	else
	{
		audWarningf("[audCategoryControllerManager::AddPackageToCreateList] Create list is full");
		return false;
	}
}
bool audCategoryControllerManager::AddPackageToDeleteList(audCategoryControllerPackage* pPackage)
{
	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPackageLock);
	PF_STOP(LockWaitTime);
	if (m_NumPendingDeletePackages != (u32)m_PendingDeletePackages.GetMaxCount())
	{
		m_PendingDeletePackages[m_NumPendingDeletePackages++] = pPackage;
		return true;
	}
	else
	{
		audWarningf("[audCategoryControllerManager::AddPackageToDeleteList] Delete list is full");
		return false;
	}
}

/*************************************************************************************
Function:	Update
*************************************************************************************/
void audCategoryControllerManager::Update()
{
	PF_FUNC(UpdateTime);

	const f32 timeStep = g_AudioEngine.GetTimeDeltaInSeconds();
	const u32 numCategories = g_AudioEngine.GetCategoryManager().GetNumberOfCategories();

#if __PS3	

	BANK_ONLY(if(g_CategoryControllerUpdateJob))
	{
		sysCriticalSection lock(m_ControllerPoolLock);
		sysTaskParameters p;
		sysMemSet(&p, 0, sizeof(p));

		// input/output data is the controller pool
		p.Input.Data = &m_Pools;
		p.Input.Size = sizeof(m_Pools);
		
		p.UserData[0].asFloat = timeStep;
		p.UserData[1].asPtr = m_pControllerOutputArray;
		p.UserData[2].asUInt = numCategories;
		p.UserData[3].asPtr = &m_Pools.m_ControllerSettingsPool;
		p.UserData[4].asUInt = (unsigned)sizeof(m_Pools.m_ControllerSettingsPool);
		p.UserDataCount = 5;

		p.ReadOnly[0].Data = m_ControllerListIndices;
		p.ReadOnly[0].Size = m_uControllerListSizeBytesAligned;
		p.ReadOnlyCount = 1;

		const u32 uNumCategoriesAligned = (m_NumCategories+15) & ~15;

		static ALIGNAS(16) u32 s_DummyOut[4] ;
		p.Output.Data = s_DummyOut;
		p.Output.Size = sizeof(s_DummyOut);

		// The scratch size must be a multiple of 16
		p.Scratch.Size = uNumCategoriesAligned * sizeof(audCategoryControllerOutput);
		
		m_categoryUpdateHandle = sysTaskManager::Create(TASK_INTERFACE(CategoryControllerJob), p);

		if (m_categoryUpdateHandle)
		{
			{
				PF_FUNC(SPUTaskWaitTime);
				sysTaskManager::Wait(m_categoryUpdateHandle);
			}

			m_categoryUpdateHandle = NULL;

			// Apply computed and combined values to categories
			for(u32 i = 0; i < numCategories; i++)
			{
				const audCategoryControllerOutput& results = m_pControllerOutputArray[i];
				audCategory* pCategory = g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(i);
				if(pCategory) //dynamic categories return null if inactive
				{
					pCategory->SetVolume(results.VolumeDb);
					pCategory->SetPitch(results.PitchCents);
					pCategory->SetLPFCutoff(results.LPFCutoff);
					pCategory->SetDistanceRollOffScale(results.DistRolloffScale);				
					pCategory->SetHPFCutoff(results.HPFCutoff);
				}
			}
		}

		//	Create and delete controllers while we're sure the SPU task is not running
		ProcessPackageCreationAndDeletion();
	}

	BANK_ONLY(else)
#endif // __PS3
#if !__PS3 || __BANK
	{
		PF_START(LockWaitTime);
		// lock against controller creation/deletion
		sysCriticalSection lock(m_ControllerPoolLock);
		PF_STOP(LockWaitTime);

#if __BANK
		bool printControllers = sm_PrintControllers;
		sm_PrintControllers = false;
#endif

		for (u32 i = 0; i < numCategories; ++i)
		{
			audCategory* pCategory = g_AudioEngine.GetCategoryManager().GetCategoryFromIndex((s16)i);
			if(!pCategory) //Dynamic categories will return NULL if inactive
			{
				continue;
			}
			audCategoryController* pHead = GetListHeadFromCategoryIndex(i);

			if (pHead)
			{
#if __BANK
				if(printControllers)
				{
					pHead->Print();
				}
#endif
				audCategoryControllerOutput results;
				audCategoryController::ComputeOutput(results, s32(pHead - GetControllerPool()), timeStep);

				pCategory->SetVolume(results.VolumeDb);
				pCategory->SetDistanceRollOffScale(results.DistRolloffScale);
				pCategory->SetPitch(results.PitchCents);
				pCategory->SetLPFCutoff(results.LPFCutoff);
				pCategory->SetHPFCutoff(results.HPFCutoff);

			}
			else
			{
				pCategory->SetVolume(0.f);
				pCategory->SetDistanceRollOffScale(1.f);
				pCategory->SetPitch(0);
				pCategory->SetLPFCutoff(kVoiceFilterLPFMaxCutoff);
				pCategory->SetHPFCutoff(0);
			}
		}

		//	Create and delete controllers after the full update
		ProcessPackageCreationAndDeletion();

	}
#endif // !__PS3 || __BANK
}

void audCategoryControllerManager::ProcessPackageCreationAndDeletion()
{
	PF_FUNC(PackageCreationAndDeletion);

	PF_START(LockWaitTime);
	sysCriticalSection lock(m_ControllerPackageLock);
	PF_STOP(LockWaitTime);
	
	//	Run through the list of controllers we want to make
	const u32 numPendingCreates = m_NumPendingCreatePackages;
	for (u32 i = 0; i < numPendingCreates; ++i)
	{
		audCategoryControllerPackage* pPackage = m_PendingCreatePackages[i];
		Assert(pPackage->m_bIsCreatingControllers);
		Assert(!pPackage->m_bControllersCreated);
		const bool bSuccess = CreateMultipleControllers(pPackage->m_pControllerArray, pPackage->m_hashCategoryNames, pPackage->GetNumControllers(), pPackage->m_uCatNameStrideBytes);
		if (bSuccess)
		{
			pPackage->m_bControllersCreated = true;
			pPackage->m_bIsCreatingControllers = false;
		}
		else
			pPackage->m_bErrorCreatingControllers = true;
	}
	m_NumPendingCreatePackages = 0;

	//	Run through the list of controllers we want to delete
	const u32 numPendingDeletes = m_NumPendingDeletePackages;
	for (u32 i = 0; i < numPendingDeletes; ++i)
	{
		audCategoryControllerPackage* pPackage = m_PendingDeletePackages[i];
		Assert(!pPackage->m_bIsCreatingControllers);
		Assert(pPackage->m_bControllersCreated);
		DestroyMultipleControllers(pPackage->m_pControllerArray, pPackage->GetNumControllers());
		pPackage->m_uNumControllers = 0;
		pPackage->m_bControllersCreated = false;
	}
	m_NumPendingDeletePackages = 0;
}

#if __BANK

#if __PS3
void ReloadCategoryControllerJobCB()
{
	RELOAD_TASK_BINARY(CategoryControllerJob);
}
#endif

void audCategoryControllerManager::AddWidgets(bkBank& bank)
{
	bank.PushGroup("Category Control");
	{
#if __PS3
		bank.AddToggle("UpdateOnSPU", &g_CategoryControllerUpdateJob);
		bank.AddButton("ReloadTask", CFA(ReloadCategoryControllerJobCB));
#endif
		bank.AddText("Num Controllers", (int*)&m_NumControllers, true);
		if (PARAM_categorycontrolwidgets.Get())
		{
			audCategory* pBase = g_AudioEngine.GetCategoryManager().GetCategoryPtr(ATSTRINGHASH("BASE", 0x44E21C90));
			RecurseAddCategoryWidget(bank, pBase);
		}
	}bank.PopGroup();
}
void audCategoryControllerManager::RecurseAddCategoryWidget(bkBank& bank, audCategory* pCategory)
{
	const char* pName = pCategory->GetNameString();

	audCategoryController* pController = AUDCATEGORYCONTROLLERMANAGER.CreateController(pName);
	if (pController == NULL)
	{
		audAssertf(pController, "%s category not found", pName);
		return;
	}

	//	Passing in false means that the group doesn't pop when the function returns. We pop it ourselves a few lines down instead.
	pController->AddWidgets(bank, false);
	{
		const u32 numChildren = pCategory->GetNumChildren();
		for (u32 i = 0; i < numChildren; ++i)
		{
			audCategory* pChild = pCategory->GetChildByIndex(i);
			RecurseAddCategoryWidget(bank, pChild);
		}
	}
	bank.PopGroup(); // Because we passed in false to pController->AddWidgets()
}
//void audCategoryControllerManager::MuteAllExcept(const audCategory* pCurrCategory, const audCategory* pMuteExcept, bool& bUnmuteParent)
//{
//	Assert(pCurrCategory);
//	Assert(pMuteExcept);
//
//	bool bShouldUnmute = false;
//
//	bUnmuteParent = (pCurrCategory == pMuteExcept);
//
//	const u32 numChildren = pCurrCategory->GetNumChildren();
//	for (u32 i = 0; i < numChildren; ++i)
//	{
//		const audCategory* pChild = const_cast<audCategory*>(pCurrCategory)->GetChildByIndex(i);
//		MuteAllExcept(pChild, pMuteExcept == pCurrCategory ? pChild : pMuteExcept, bShouldUnmute);
//	}
//
//	bUnmuteParent |= bShouldUnmute;
//
//	audCategoryController** ppController = m_ControllerMap.SafeGet(atStringHash(pCurrCategory->GetNameString()));
//	if (ppController)
//	{
//		audCategoryController* pController = *ppController;
//		while (pController)
//		{
//			pController->m_bMute = !bShouldUnmute && pMuteExcept != pCurrCategory;
//			pController->m_bSolo = !pController->m_bMute;
//			pController = pController->GetNext();
//		}
//	}
//}
//void audCategoryControllerManager::UnmuteAll()
//{
//	for (atBinaryMap<audCategoryController*, u32>::Iterator iter = m_ControllerMap.Begin(); iter != m_ControllerMap.End(); ++iter)
//	{
//		audCategoryController* pController = *iter;
//		while (pController)
//		{
//			pController->m_bMute = false;
//			pController->m_bSolo = false;
//			pController = pController->GetNext();
//		}
//	}
//}

#endif

} //namespace rage

