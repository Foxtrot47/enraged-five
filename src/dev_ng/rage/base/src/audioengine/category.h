//
// audioengine/category.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_CATEGORY_H
#define AUD_CATEGORY_H

#include "dynamicmixmgr.h"
#include "categorydefs.h"
#include "atl/array.h"
#include "audioengine/metadataref.h"
#include "math/float16.h"


namespace rage {


class audMetadataManager;

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif

#define AUD_ATOMIC_CATEGORIES 0

#if __PS3
class audCategoryFloat : public Float16
{
};
#else
class audCategoryFloat
{
public:
	__forceinline void SetFloat16_FromFloat32(const f32 val) // renamed to match Float16 class ..
	{
		m_Val = val;
	}

	__forceinline f32 GetFloat32_FromFloat16() const // renamed to match Float16 class ..
	{
		return m_Val;
	}

	__forceinline void SetFloat16_Zero()
	{
		m_Val = 0.0f;
	}

	__forceinline void SetFloat16_One()
	{
		m_Val = 1.0f;
	}

	__forceinline audCategoryFloat operator=(const f32 rhs)
	{
		m_Val = rhs;
		return *this;
	}

private:
	f32 m_Val;
};
#endif
// only align on PS3
struct tCategorySettings
{
	audCategoryFloat	Volume;
	audCategoryFloat	DistanceRollOffScale;
	audCategoryFloat	PlateauRollOffScale;
	audCategoryFloat	OcclusionDamping;
	audCategoryFloat	EnvironmentalFilterDamping;
	audCategoryFloat	SourceReverbDamping;
	audCategoryFloat	DistanceReverbDamping;
	audCategoryFloat	InteriorReverbDamping;
	audCategoryFloat	EnvironmentalLoudness;
	audCategoryFloat	UnderwaterWetLevel;
	audCategoryFloat	StonedWetLevel;
	s16				Pitch;
	u16				LPFCutoff;
	u16				HPFCutoff;
};

extern tCategorySettings *g_ResolvedCategorySettings;

struct mixGroupSettings;

// PURPOSE
//  Categories are defined using RAVE, and are used to control various properties (volume, etc) of groups of sounds. Every
//  EnvironmentSound looks up the sound hierarchy to determine its category, and then combines that category's settings 
//  with its own. Categories also combine their settings with their parents, all the way up the category hierarchy.
//  From game-code, use the Category Manager to access categories. Settings can either be altered via the category manager,
//  or it can return a ptr to the requested category, which can then be accessed directly. Unless performing multiple
//  updates per frame, it is probably best to go via the category manager, although this is less efficient.
class audCategory
{
	friend class audCategoryManager;
public:
	audCategory();
	~audCategory(){}; //NB: any cleanup needs to be done in Shutdown() as dynamic categories won't call this

	//Purpose
	// Resets the category so it's slot can be used again - primarily for dynamic categories
	void Shutdown();


	//Purpose
	//Returns whether the category has been initialised and hence if this slot is taken
	bool IsActive() const { return m_Metadata!=NULL; } 

	const Category * GetMetadata() const { return m_Metadata; }



	// PURPOSE
	//  Initialises the category. This will in turn initialise all its child categories. (By going via the Category
	//  Manager, not directly calling its children's Init).
	//  On startup, the Category Manager initialises the BASE category, and hence all other categories are 
	//  subsequently initialised.
	// PARAMS
	//  categoryNameHash - the hashed name of the category.
	//  metadataManager - a ptr to the category metadata manager.
	//  parent - the parent category, so this category can navigate up the hierarchy.
	bool Init(u32 categoryNameHash, audMetadataManager* metadataManager, audCategory* parent);

	//Same as Init but used for dynamic categories
	bool InitDynamic(u32 categoryNameHash, audMetadataManager* metadatamanager, audCategory* parent, const MixGroupCategoryMap * map);

	// PURPOSE
	//  Setters set the requested settings, getters return the requested combined with the metadata value.
	void		SetVolume(float volume);
	float		GetRequestedVolume() const;
	void		SetPitch(s32 pitch);
	s32			GetRequestedPitch() const;
	void		SetLPFCutoff(u32 cutoff);
	u32			GetRequestedLPFCutoff() const;
	void		SetHPFCutoff(u32 cutoff);
	u32			GetRequestedHPFCutoff() const;
	// NOTE
	//	This function is deprecated - use SetLPFCutoff()
	void	SetFilterCutoff(u16 filterCutoff);

	void	SetDistanceRollOffScale(float distanceRollOffScale);
	void	SetOcclusionDamping(f32 occlusionDamping);
	void	SetEnvironmentalFilterDamping(f32 environmentalFilterDamping);
	void	SetSourceReverbDamping(f32 environmentalReverbDamping);
	void	SetDistanceReverbDamping(f32 environmentalReverbDamping);
	void	SetInteriorReverbDamping(f32 interiorReverbDamping);
	void	SetEnvironmentalLoudness(f32 environmentalLoudness);
	void	SetUnderwaterWetLevel(const float underwaterWetness);
	void	SetStonedWetLevel(const float stonedWetness);

	__forceinline f32 GetVolume() const
	{
		return g_ResolvedCategorySettings[m_Index].Volume.GetFloat32_FromFloat16();
	}

	__forceinline s32 GetPitch() const 
	{
		return g_ResolvedCategorySettings[m_Index].Pitch;
	}

	__forceinline u16 GetLPFCutoff() const 
	{
		return g_ResolvedCategorySettings[m_Index].LPFCutoff;
	}

	__forceinline u16 GetHPFCutoff() const 
	{
		return g_ResolvedCategorySettings[m_Index].HPFCutoff;
	}

	__forceinline f32 GetDistanceRollOffScale() const 
	{
		return g_ResolvedCategorySettings[m_Index].DistanceRollOffScale.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetOcclusionDamping() const 
	{
		return g_ResolvedCategorySettings[m_Index].OcclusionDamping.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetEnvironmentalFilterDamping() const 
	{
		return g_ResolvedCategorySettings[m_Index].EnvironmentalFilterDamping.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetSourceReverbDamping() const 
	{
		return g_ResolvedCategorySettings[m_Index].SourceReverbDamping.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetDistanceReverbDamping() const 
	{
		return g_ResolvedCategorySettings[m_Index].DistanceReverbDamping.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetInteriorReverbDamping() const 
	{
		return g_ResolvedCategorySettings[m_Index].InteriorReverbDamping.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetEnvironmentalLoudness() const 
	{
		return g_ResolvedCategorySettings[m_Index].EnvironmentalLoudness.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetUnderwaterWet() const
	{
		return g_ResolvedCategorySettings[m_Index].UnderwaterWetLevel.GetFloat32_FromFloat16();
	}

	__forceinline f32 GetStonedWet() const
	{
		return g_ResolvedCategorySettings[m_Index].StonedWetLevel.GetFloat32_FromFloat16();
	}

	u32 GetHPFDistanceCurve() const { return m_HPFDistanceCurve; }
	u32 GetLPFDistanceCurve() const { return m_LPFDistanceCurve; }

	// PURPOSE
	// Sets all parent override values to 'override'
	void SetParentOverrides(bool override);


	// PURPOSE
	//	Returns true if sounds within this category should mute when the user plays their own music via the HUD.
	//  (This is currently a Xenon specific requirement)
	bool	GetMuteOnUserMusic();

	// PURPOSE
	//	Returns the timer id to use for sounds in this category, traversing up the hierarchy
	u32	GetTimerId() const;

	// PURPOSE
	//	Returns true if rear filtering and attenuation should be disabled for sounds within this category
	bool GetRearAttenuationDisabled();

	// PURPOSE
	//  Copies the current category settings over to the next index, in preparation for that becoming the current one.
	//  Also passes on this request to all children. Hence by calling this on BASE, all categories will be updated.
	// PARAMS
	//  currentIndex - the current game-thread index into the requested settings.
	//  nextIndex    - the index to copy the current settings into, and then use as the new current index.
#if AUD_ATOMIC_CATEGORIES
	void	CommitCategorySettings(u16 currentIndex, u16 nextIndex);
#endif // AUD_ATOMIC_CATEGORIES

	// PURPOSE
	//  Work down to each category, calculating the new volume, pitch, etc. This can be done just once,
	//  instead of calculating it for every category every time a sound requests it. By working down the hierarchy,
	//  a category can work out and store its parameters, and then respond when its children ask.
	// PARAMS
	//  audioThreadIndex - the current index into the requested settings that the audio thread should use. This is
	//  common across all categories.
	void	AudioThreadUpdate(u16 audioThreadIndex,bool useMetaDeta);

	// PURPOSE
	//	Returns the number of children
	u32 GetNumChildren() const;

	// PURPOSE
	//	Returns The corresponding child audCategory pointer
	// PARAMS
	//  catIndex - The child number index
	audCategory* GetChildByIndex(const u32 catIndex) const;

	//PURPOSE
	// Returns a pointer to the parent category or NULL if none
	audCategory* GetParent() const;

	//PURPOSE
	// Returns corresponding category index 
	// PARAMS
	// catIndex - the child number index
	s32 GetChildCategoryIndex(const u32 childIndex) const 
	{ 
		return m_ChildCategoryIndices[childIndex];
	}

#if __BANK

	// PURPOSE
	//	Returns The name as a null terminated string
	const char* GetNameString() const;

	// PURPOSE
	//	Traverses hierarchy pulling out all metadata values down the tree
	void PullMetadataValues();

	// PURPOSE
	//	Returns true if this category or any of its children are solo'd
	bool IsSoloActive() const;

	bool IsMuteActive() const;

#if !__SPU
	u32 GetNameTableOffset() const
	{
		return m_Metadata->NameTableOffset;
	}
#endif
#endif

private:

	//Does the core initialisation work shared by static and dynamic categories
	bool InitCore(u32 categoryNameHash, audCategory * parent, audMetadataManager* metadataManager);

	const Category *GetChildCategory(const audMetadataManager *, const audMetadataRef childRef);

	// PURPOSE
	//	Populates m_MetadataSettings from the supplied metadata ptr
	void PullMetadataValues(Category *metadata);
	
	// PURPOSE
	// Inits metadata for dynamic categories (everything set to zero)
	void InitMetadataDynamic();

	// cache the metadata set values so we don't have to keep hitting metadata memory
	tCategorySettings m_MetadataSettings;
	
#if AUD_ATOMIC_CATEGORIES
	// PURPOSE
	//  Caches the requested category settings, so allow lock-free access from sounds.
	// NOTES
	//	Watch this size of this as it is stored three times per category instance
	tCategorySettings m_CategorySettings[3];
#else
	tCategorySettings m_CategorySettings;
#endif

	u32 m_Flags;
	u32 m_HPFDistanceCurve;
	u32 m_LPFDistanceCurve;

#if __BANK
	s32 m_RecursionDebugIndex;
#endif

	// keep a pointer to metadata so we can check if category is active plus keep refreshing it to catch rave tweaks for bank builds
	Category* m_Metadata;

	s16 m_ParentIndex, m_Index;
	atRangeArray<s16, Category::MAX_CATEGORYREFS> m_ChildCategoryIndices;
	Category::tParentOverrides m_ParentOverrides;
	SoundTimer m_TimerId;
#if __BANK
	bool m_IsMuted;
#endif
};

#if __WIN32
#pragma warning(pop)
#endif // __WIN32

} // namespace rage

#endif //AUD_CATEGORY_H
