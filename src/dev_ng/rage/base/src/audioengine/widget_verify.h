//
// audioengine/widget_verify.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef WIDGET_VERIFY_H
#define WIDGET_VERIFY_H

namespace rage
{

#if RSG_ASSERT
class F32_VerifyFinite
{
public:
	F32_VerifyFinite() {}
	F32_VerifyFinite(const f32 val) : m_Value(val) {Assert(FPIsFinite(val));}

	operator f32() const {return m_Value;}


	F32_VerifyFinite& operator=(const f32 rhs)
	{
		Assert(FPIsFinite(rhs));
		m_Value = rhs;
		return *this;
	}

	F32_VerifyFinite& operator=(const F32_VerifyFinite rhs)
	{
		Assert(FPIsFinite(rhs.m_Value));
		m_Value = rhs.m_Value;
		return *this;
	}

	F32_VerifyFinite& operator*=(const f32 rhs)
	{
		Assert(FPIsFinite(rhs));
		m_Value *= rhs;
		return *this;
	}

	F32_VerifyFinite& operator*=(const F32_VerifyFinite rhs)
	{
		Assert(FPIsFinite(rhs.m_Value));
		m_Value *= rhs.m_Value;
		return *this;
	}

	F32_VerifyFinite& operator+=(const f32 rhs)
	{
		Assert(FPIsFinite(rhs));
		m_Value += rhs;
		return *this;
	}

	F32_VerifyFinite& operator+=(const F32_VerifyFinite rhs)
	{
		Assert(FPIsFinite(rhs.m_Value));
		m_Value += rhs.m_Value;
		return *this;
	}

private:
	f32 m_Value;
};
#else
	typedef f32 F32_VerifyFinite;
#endif // RSG_ASSERT
}

#endif // WIDGET_VERIFY_H
