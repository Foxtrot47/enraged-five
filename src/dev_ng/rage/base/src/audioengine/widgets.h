//
// audioengine/widgets.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_WIDGETS_H
#define AUD_WIDGETS_H

#include "enginedefs.h"
#include "curve.h"
#include "atl/array.h"
#include "string/stringhash.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"
#include "widget_verify.h"
#include "audiodata/simpletypes.h"
#include "audiohardware/channel.h"

namespace rage 
{

// PURPOSE
//  audSmoother takes an input, and returns a smoothed output. The smoothing rate is either per-call, or per
//  millisecond, depending on which CalculateValue() is used. Min and Max bounds can also be applied.
class audSmoother
{
public:
	audSmoother();
	// PURPOSE
	//  Initialise the smoother
	// PARAMS
	//  increaseRate - the maximum increase possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	//  decreaseRate - the maximum decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	void Init(float increaseRate, float decreaseRate);
	// PARAMS
	//  minimum - the lowest value that will ever be returned
	//  maximum - the highest value that will ever be returned
	void Init(float increaseRate, float decreaseRate, float minimum, float maximum);

	// PURPOSE
	//  Sets the smoothing rates. This can be done after initialisation, while the smoother is being used.
	// PARAMS
	//  increaseRate - the maximum increase possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	//  decreaseRate - the maximum decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	void SetRates(float increaseRate, float decreaseRate);
	// PURPOSE
	//  Sets the extreme bounds. This can be done after initialisation, while the smoother is being used.
	// PARAMS
	//  minimum - the lowest value that will ever be returned
	//  maximum - the highest value that will ever be returned
	void SetBounds(float minimum, float maximum);
	// PURPOSE
	//  Sets the last time. This can be done after initialisation, while the smoother is being used.
	// The idea is to don't need to update the smoother every frame and be able to use it at certain points. 
	// PARAMS
	//  lastTime - value to set as the smoother last time
	void SetLastTime(u32 lastTime);

	// PURPOSE
	//  Calculates the smoothed value for the given input.
	float CalculateValue(float input);
	// PURPOSE
	//  Calculates the smoothed value for the given input and time - using this implies the smoothing rates are
	//  in milliseconds.
	float CalculateValue(float input, u32 time);
	// PURPOSE
	//  Calculates the smoothed value for the given input and timeStep - using this implies the smoothing rates are
	//  in milliseconds.
	float CalculateValue(const float input, const f32 timeStepS);
	// PURPOSE
	//	Resets state to first request so that the next call to CalculateValue will jump with no smoothing
	void Reset()
	{
		m_FirstRequest = true;
	}

	f32 GetLastValue() const
	{
		return m_LastValue;
	}

	f32 GetIncreaseRate() const { return m_IncreaseRate; }
	f32 GetDecreaseRate() const { return m_DecreaseRate; }

	bool IsInitialized() const {return m_Initialised;}

private:

	f32 m_IncreaseRate;
	f32 m_DecreaseRate;
	f32 m_Maximum;
	f32 m_Minimum;

	f32 m_LastValue;
	u32 m_LastTime;

	bool m_Initialised : 1;
	bool m_FirstRequest : 1;
	bool m_BoundsSet : 1;
};

class audSimpleSmoother
{
public:
	audSimpleSmoother();

	// PURPOSE
	//  Initialise the smoother
	// PARAMS
	//  rate - the maximum increase/decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	// clamp01 - should the output be clamped to [0,1]
	void Init(const float changeRate, const bool clamp01 = false);
	
	// PURPOSE
	//  Sets the smoothing rate. This can be done after initialisation, while the smoother is being used.
	// PARAMS
	//  changeRate - the maximum increase/decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	void SetRate(const float changeRate);
	
	// PURPOSE
	//  Calculates the smoothed value for the given input.
	float CalculateValue(const float input);
	// PURPOSE
	//  Calculates the smoothed value for the given input and time - using this implies the smoothing rates are
	//  in milliseconds.
	float CalculateValue(const float input, const u32 timeMs);
	// PURPOSE
	//  Calculates the smoothed value for the given input and timeStep - using this implies the smoothing rates are
	//  in milliseconds.
	float CalculateValue(const float input, const f32 timeStepS);

	// PURPOSE
	//	Resets state to first request so that the next call to CalculateValue will jump with no smoothing
	void Reset()
	{
		m_FirstRequest = true;
	}

	f32 GetLastValue() const
	{
		return m_LastValue;
	}

	f32 GetChangeRate() const { return m_ChangeRate; }
	bool IsInitialized() const {return m_Initialised;}
	
private:

	f32 m_ChangeRate;
	f32 m_LastValue;
	u32 m_LastTime;

	bool m_Initialised : 1;
	bool m_FirstRequest : 1;
	bool m_Clamp01 : 1;
};


// PURPOSE
//  Same as the simple smoother, but with discrete increase/decrease rates
class audSimpleSmootherDiscrete
{
public:
	audSimpleSmootherDiscrete();

	// PURPOSE
	//  Initialise the smoother
	// PARAMS
	//  rate - the maximum increase/decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	// clamp01 - should the output be clamped to [0,1]
	void Init(const float increaseRate, const float decreaseRate, const bool clamp01 = false);

	// PURPOSE
	//  Sets the smoothing rate. This can be done after initialisation, while the smoother is being used.
	// PARAMS
	//  changeRate - the maximum increase/decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	void SetRates(float increaseRate, float decreaseRate);

	// PURPOSE
	//  Calculates the smoothed value for the given input.
	float CalculateValue(const float input);
	// PURPOSE
	//  Calculates the smoothed value for the given input and time - using this implies the smoothing rates are
	//  in milliseconds.
	float CalculateValue(const float input, const u32 timeMs);
	// PURPOSE
	//  Calculates the smoothed value for the given input and timeStep - using this implies the smoothing rates are
	//  in milliseconds.
	float CalculateValue(const float input, const f32 timeStepS);

	// PURPOSE
	//	Resets state to first request so that the next call to CalculateValue will jump with no smoothing
	void Reset()
	{
		m_FirstRequest = true;
	}

	f32 GetLastValue() const
	{
		return m_LastValue;
	}

	f32 GetIncreaseRate() const { return m_IncreaseRate; }
	f32 GetDecreaseRate() const { return m_DecreaseRate; }
	bool IsInitialized() const {return m_Initialised;}

private:

	f32 m_IncreaseRate;
	f32 m_DecreaseRate;
	f32 m_LastValue;
	u32 m_LastTime;

	bool m_Initialised : 1;
	bool m_FirstRequest : 1;
	bool m_Clamp01 : 1;
};

// PURPOSE
//	audFluctuatorProbBased returns a value which over time fluctuates between two bands (and is smoothed). On each request,
//  it flips with a certain probability between the two bands, and flips with a different probability between
//  the top and bottom of whichever band it's in. The output is the smoothed version of this.
//  Sounds weird, but has proved quite a useful natural-sounding thing in the past, for effects such as weather.
// TODO
//  Add a time-based equivalent.

class audFluctuatorProbBased
{
public:
	//audFluctuatorProbBased();
	//~audFluctuatorProbBased(){};

	// PURPOSE
	//  Initialise the fluctuator
	// PARAMS
	//  increaseRate - the maximum increase possible per call.  
	//  decreaseRate - the maximum decrease possible per call.  
	//  bandOneMinimum - the lowest value of the first band 
	//  bandOneMaximum - the highest value of the first band 
	//  bandTwoMinimum - the lowest value of the second band 
	//  bandTwoMaximum - the highest value of the second band 
	//  intrabandFlipProbability - the probability per call that it will flip to the other extreme of the current band.
	//  interbandFlipProbability - the probability per call that it will flip to the other band.
	//  initialValue - the starting value, that will be smoothed from on first call
	void Init(float increaseRate, float decreaseRate,
			  float bandOneMinimum, float bandOneMaximum,
			  float bandTwoMinimum, float bandTwoMaximum,
			  float intrabandFlipProbability, float interbandFlipProbability,
			  float initialValue);

	// PURPOSE
	//  Sets the smoothing rates, band limits and flip-probabilities. This can be done after initialisation, while being used.
	// PARAMS
	//  increaseRate - the maximum increase possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	//  decreaseRate - the maximum decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	void SetRates(float increaseRate, float decreaseRate);
	// PARAMS
	//  bandOneMinimum - the lowest value of the first band 
	//  bandOneMaximum - the highest value of the first band 
	//  bandTwoMinimum - the lowest value of the second band 
	//  bandTwoMaximum - the highest value of the second band 
	void SetBands(float bandOneMinimum, float bandOneMaximum,
				  float bandTwoMinimum, float bandTwoMaximum);
	// PARAMS
	//  intrabandFlipProbability - the probability per call that it will flip to the other extreme of the current band.
	//  interbandFlipProbability - the probability per call that it will flip to the other band.
	void SetFlipProbabilities(float intrabandFlipProbability, float interbandFlipProbability);

	// PURPOSE
	//  Calculates the value of the fluctuator, after flipping band as appropriate, and smoothing the output.
	// RETURNS
	//  The calculated value.
	float CalculateValue();

	// PURPOSE
	//  Returns the last value calculated by the smoother
	inline f32 GetLastValue() const { return m_SmootherLastValue; }

protected:
private:

	f32 m_BandOneMinimum;
	f32 m_BandOneMaximum;
	f32 m_BandTwoMinimum;
	f32 m_BandTwoMaximum;
	f32 m_IntrabandFlipProbability, m_InterbandFlipProbability;
	f32 m_InitialValue;

	f32 m_SmootherIncreaseRate;
	f32 m_SmootherDecreaseRate;
	f32 m_SmootherLastValue;

	bool m_FirstRequest;
	bool m_IntrabandState;
	bool m_InterbandState;
};


// PURPOSE
// audFluctuatorTimeBased returns a value which over time fluctuates between two values (min and max) also smoothed. It flips when we reach a delta 
// time picked randomly between a min and a max value. The output is the smoothed version of this. 


class audFluctuatorTimeBased
{
public:
	//audFluctuatorTimeBased();
	//~audFluctuatorTimeBased(){};

	// PURPOSE
	//  Initialise the fluctuator
	// PARAMS
	//  increaseRate - the maximum increase possible per call.  
	//  decreaseRate - the maximum decrease possible per call.  
	//  minimumValue - lowest value of the fluctuator
	//  maximumValue - highest value of the fluctuator
	//  minSwitchTime - minimum switch time
	//  maxSwitchTime - maximum switch time
	//  initialValue - the starting value, that will be smoothed from on first call
	void Init(float increaseRate, float decreaseRate,
			  float minimumValue, float maximumValue,
			  u32 minSwitchTime, u32 maxSwitchTime,
			  float initialValue);

	// PURPOSE
	//  Sets the smoothing rates, band limits and flip-probabilities. This can be done after initialisation, while being used.
	// PARAMS
	//  increaseRate - the maximum increase possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	//  decreaseRate - the maximum decrease possible. Either per-call, or per millisecond, depending on which CalculateValue() is used
	void SetRates(float increaseRate, float decreaseRate);
	// PARAMS
	//  minimumValue - Time-based: lowest value of the fluctuator
	//  maximumValue - Time-based: highest value of the fluctuator
	void SetValues(float minimumValue, float maximumValue);

	// PARAMS
	//  minSwitchTime - minimum switch time
	//  maxSwitchTime - maximum switch time
	void SetSwitchingTimes(u32 minSwitchTime, u32 maxSwitchTime);


	// PURPOSE
	//  Calculates the value of the fluctuator, after flipping band as appropriate, and smoothing the output.
	// RETURNS
	//  The calculated value.
	float CalculateValue(u32 timeInMs);

protected:
private:

	f32 m_MinimumValue;
	f32 m_MaximumValue;
	f32 m_Value;

	u32 m_MinimumSwitchTime;
	u32 m_MaximumSwitchTime;
	u32 m_NextSwitchTime; 

	f32 m_SmootherIncreaseRate;
	f32 m_SmootherDecreaseRate;

	f32 m_SmootherLastValue;
	u32 m_SmootherLastTime;

	bool m_FirstRequest;
};

// PURPOSE
//	Computes attenuation in dBs based on the listener's position and a cone, specified by a direction vector and a rotation 
//	matrix.  Returned attenuation is the specified rear attenuation outside the outer angle and rises to 0dB inside the 
//	inner angle using the specified curve.
class audVolumeCone
{
public:
	audVolumeCone();
	~audVolumeCone();

	// PURPOSE
	//	Initialise the cone
	// PARAMS
	//	curveName - the name of the curve to use to interpolate volume attenuation, e.g. LINEAR_RISE
	//	coneDirection - vector specifying the direction of the cone, relative to the supplied matrix
	//	rearAttenuation - the attenuation outside of the outer angle
	//	innerAngle - attenuation is 0dB within this angle
	//	outerAngle - attenuation is at max outside this angle
	void Init(const u32 curveNameHash, Vec3V_In coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle);

	void Init(const u32 curveNameHash, const Vector3 &coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle)
	{
		Init(curveNameHash, VECTOR3_TO_VEC3V(coneDirection), rearAttenuation, innerAngle, outerAngle);
	}

	void Init(const char *curveName, Vec3V_In coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle)
	{
		Init(atStringHash(curveName), coneDirection, rearAttenuation, innerAngle, outerAngle);
	}

	void Init(const char *curveName, const Vector3 &coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle)
	{
		Init(atStringHash(curveName), VECTOR3_TO_VEC3V(coneDirection), rearAttenuation, innerAngle, outerAngle);
	}

	// PURPOSE
	//	Returns the computed attenuation based on the listeners position and the configured cone parameters.
	f32 ComputeAttenuation(Mat34V_In matrix, u32 listenerId = 0) const;
	f32 ComputeAttenuation(const Matrix34 &matrix, u32 listenerId = 0) const
	{
		return ComputeAttenuation(MATRIX34_TO_MAT34V(matrix), listenerId);
	}

private:

	Vec3V m_ConeDirection;
	audCurve m_Curve;
	f32 m_RearAttenuation, m_InnerConeAngle, m_OuterConeAngle;
	
};


// PURPOSE
//	Lite version - Computes linear attenuation in dB's based on the listener's position and a cone, specified by a direction vector and a rotation 
//	matrix.  Returned attenuation is the specified rear attenuation outside the outer angle and rises to 0dB inside the 
//	inner angle using a linear mapping.
class audVolumeConeLite
{
public:
	audVolumeConeLite();
	~audVolumeConeLite();

	// PURPOSE
	//	Initialise the cone
	// PARAMS
	//	coneDirection - vector specifying the direction of the cone, relative to the supplied matrix
	//	rearAttenuation - the attenuation in dB outside of the outer angle
	//	innerAngle - attenuation is 0dB within this angle
	//	outerAngle - attenuation is at max outside this angle
	void Init(Vec3V_In coneDirection, const f32 rearAttenuation, const f32 innerAngle, const f32 outerAngle);

	// PURPOSE
	//	Returns the computed attenuation based on the listeners position and the configured cone parameters.
	f32 ComputeAttenuation(const Mat34V &matrix, u32 listenerId = 0) const;

private:

	Vec3V m_ConeDirection;
	f32 m_RearAttenuation, m_InnerConeAngle, m_OuterConeAngle;

};

// PURPOSE
//	Serves as a wrapper round atStringHash to make it easier to compute hashes during static initialise
//	rather than every frame
class audStringHash
{
public:
	audStringHash(const char *string)
	{
		m_HashValue = atStringHash(string);
	}

	operator u32() const {return m_HashValue;}
private:
	u32 m_HashValue;

};

// PURPOSE
//	Serves as a wrapper round atPartialStringHash to make it easier to compute hashes during static initialise
//	rather than every frame
class audPartialStringHash
{
public:
	audPartialStringHash(const char *string)
	{
		m_HashValue = atPartialStringHash(string);
	}

	operator u32() const {return m_HashValue;}
private:
	u32 m_HashValue;

};

//PURPOSE
// A compressed normalized quaternion storage class converting to and from QuatV
// currently stores in 8 bytes
class audCompressedQuat
{
public:
	void Init()
	{
		m_X = m_Y = m_Z = 0;
		m_W = 1;
	}
	void Init(QuatV_In quatIn)
	{
		Set(quatIn);
	}
	void Set(QuatV_In quatIn)
	{
		QuatV quat = Scale(NormalizeFast(quatIn), ScalarV(32767.f));

		m_X = (s16)(
			quat.GetXf());
		m_Y = (s16)(quat.GetYf());
		m_Z = (s16)(quat.GetZf());
		m_W = (s16)(quat.GetWf());
	}
	void Get(QuatV_InOut quatOut)const
	{
		QuatV quat = QuatV((f32)m_X, (f32)m_Y, (f32)m_Z, (f32)m_W);
		quatOut = Scale(quat, ScalarV(1/32767.f));
	}
private:
	s16 m_X, m_Y, m_Z, m_W;
};

class audCompressedVelocity
{
public:
	void Init()
	{
		X = Y = Z = 0.f;
	}
	void Init(Vector3 &vecIn)
	{
		Set(vecIn);
	}
	void Set(const Vector3 &vecIn)
	{
		X = vecIn.GetX();
		Y = vecIn.GetY();
		Z = vecIn.GetZ();
	}
	void Get(Vector3 &vecOut)const
	{
		vecOut = Vector3(X, Y, Z);
	}

	void Init(Vec3V &vecIn)
	{
		Set(vecIn);
	}
	void Set(const Vec3V &vecIn)
	{
		X = vecIn.GetXf();
		Y = vecIn.GetYf();
		Z = vecIn.GetZf();
	}
	void Get(Vec3V &vecOut)const
	{
		vecOut = Vec3V(X, Y, Z);
	}
private:
	f32 X, Y, Z;
};

//Purpose stores a block of sound variables
class audVariableBlock
{
public:
	// PURPOSE
	//	Initialises the variable block with the specified list of variables
	// RETURNS
	//	True on success, false if the specified variable list could not be found
	bool Init(const char *variableListName);
	bool Init(const u32 variableListNameHash);

	// PURPOSE
	//	Finds the variable with the specified name
	// RETURNS
	//	Pointer to the variable value
	float *FindVariableAddress(const char *name);
	float *FindVariableAddress(const u32 hash);

	// PURPOSE
	//	Finds the variable with the specified name and updates its value
	void SetVariable(const u32 hash, const f32 value);

	enum { kNumVariablesPerBlock = 10 };

	const ravesimpletypes::VariableList *GetMetadata() const { return m_Metadata; }
	float GetValueByIndex(const u32 index) const
	{
		if(audVerifyf(index < kNumVariablesPerBlock, "Invalid variable index"))
		{
			return m_Values[index];
		}
		return 0.f;
	}

private:

	const ravesimpletypes::VariableList *m_Metadata;
	float *m_Values;

};


} // namespace rage

#endif //AUD_WIDGETS_H
