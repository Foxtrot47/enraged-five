//  
// audioengine/soundupdatejob.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if __SPU

#ifndef AUD_IMPL
#define AUD_IMPL
#endif

#define AUD_DMA_PCM_SOURCE_STATE_DATA 
#define AUD_DISABLE_ASSERTS __SPU

#if AUD_DISABLE_ASSERTS
#undef __ASSERT
#define __ASSERT 0
#undef ASSERT_ONLY
#define ASSERT_ONLY(x)


#undef Assert
#define Assert(x)
#undef Assertf
#define Assertf(x,...)
#undef FastAssert
#define FastAssert(x)
#undef AssertMsg
#define AssertMsg(x,msg)
#undef AssertVerify
#define AssertVerify(x) (x)
#undef Verifyf
#define Verifyf(x,fmt,...) (x)
#undef DebugAssert
#define DebugAssert(x)
#endif

#include "vector/vector3_consts_spu.cpp"

#include "soundupdatejob.h"

#include "audiohardware/voicesettings.h"

#include <cell/sync/mutex.h>

#include "environment.h"
#include "soundfactory.h"

#include "audiosoundtypes/automationsound.cpp"
#include "audiosoundtypes/collapsingstereosound.cpp"
#include "audiosoundtypes/crossfadesound.cpp"
#include "audiosoundtypes/dynamicentitysound.cpp"
#include "audiosoundtypes/directionalsound.cpp"
#include "audiosoundtypes/envelopesound.cpp"
#include "audiosoundtypes/environmentsound.cpp"
#include "audiosoundtypes/externalstreamsound.cpp"
#include "audiosoundtypes/granularsound.cpp"
#include "audiosoundtypes/kineticsound.cpp"
#include "audiosoundtypes/loopingsound.cpp"
#include "audiosoundtypes/modularsynthsound.cpp"
#include "audiosoundtypes/multitracksound.cpp"
#include "audiosoundtypes/onstopsound.cpp"
#include "audiosoundtypes/randomizedsound.cpp"
#include "audiosoundtypes/retriggeredoverlappedsound.cpp"
#include "audiosoundtypes/sequentialsound.cpp"
#include "audiosoundtypes/simplesound.cpp"
#include "audiosoundtypes/sound.cpp"
#include "audiosoundtypes/sequentialoverlapsound.cpp"
#include "audiosoundtypes/speechsound.cpp"
#include "audiosoundtypes/streamingsound.cpp"
#include "audiosoundtypes/twinloopsound.cpp"
#include "audiosoundtypes/wrappersound.cpp"
#include "audiosoundtypes/ifsound.cpp"
#include "audiosoundtypes/mathoperationsound.cpp"
#include "audiosoundtypes/parametertransformsound.cpp"
#include "audiosoundtypes/fluctuatorsound.cpp"
#include "audiosoundtypes/switchsound.cpp"
#include "audiosoundtypes/variableblocksound.cpp"
#include "audiosoundtypes/variablecurvesound.cpp"
#include "audiosoundtypes/variableprintvaluesound.cpp"

#include "curve.cpp"
#include "engineutil.cpp"
#include "entityvariablecache.cpp"
#include "requestedsettings.cpp"
#include "soundmanager_update.cpp"
#include "soundfactory.cpp"
#include "soundpool.cpp"
#include "spuutil.h"
#include "widgets.cpp"

#include "audiodata/container.cpp"
#include "audiohardware/driverutil.cpp"
#include "audiohardware/pcmsource.cpp"
#include "audiohardware/pcmsource_interface.cpp"
#include "audiohardware/streamingwaveslot.cpp"
#include "audiohardware/syncsource.cpp"
#include "audiohardware/waveplayer.cpp"
#include "audiohardware/waveref.cpp"
#include "audiohardware/waveslot.cpp"

#include "math/random.cpp"

#include "system/magicnumber.h"

namespace rage
{
	struct audVoiceSettings;
	struct PcmSourceState;

	u32 audDriver::m_NumOutputChannels;

	variableNameValuePair *g_GlobalSoundVariables;
	variableNameValuePair *g_GlobalVariableEAStart, *g_GlobalVariableEAEnd;

	float *g_EntityVariableEAStart, *g_EntityVariableEAEnd;
	u32 * audEntity::sm_EntityVariableBlockRefs;
	audEngineContext g_Context;

	bool g_AssertOnVolumeClamp = false;

	audSoundPoolBucket *g_Bucket;
	u32 g_BucketSoundSlotSize;
	u32 g_NumSoundSlotsPerBucket;
	u32 g_BucketRequestedSettingsSlotSize;
	u32 g_NumRequestedSettingsSlotsPerBucketAligned;

	s32 g_TaskBucketMemOffset;

	const audEnvironmentListenerData *g_audListenerData;

	audEntityVariableCache g_EntityVariableCache;

	const u8 *g_DisabledMetadataPtr = NULL;
	u32 g_DisabledMetadataSize = 0;
	
#if __DEV
	u32 g_EntityVariableCacheHits;
	u32 g_EntityVariableCacheMisses;
#endif

#if SPU_WAVESLOT_MAP
	//More hack....
	// PURPOSE: Given a particular prime number, return the next one in the sequence.
	// PARAMS: s - A prime number (our sequence starts at eleven)
	// RETURNS: A prime number larger than s.
	unsigned short atHashNextSize(unsigned short s) {
		// these are all primes.
		// these are all primes.
		if (s < 11) return 11;
		else if (s < 29) return 29;
		else if (s < 59) return 59;
		else if (s < 107) return 107;
		else if (s < 191) return 191;
		else if (s < 331) return 331;
		else if (s < 563) return 563;
		else if (s < 953) return 953;
		else if (s < 1609) return 1609;
		else if (s < 2729) return 2729;
		else if (s < 4621) return 4621;
		else if (s < 7841) return 7841;
		else if (s < 13297) return 13297;
		else if (s < 22571) return 22571;
		else if (s < 38351) return 38351;
		else if (s < 65167) return 65167;
		AssertMsg(0,"invalid size in atHashNextSize");
		return 0;
	}
#endif

	extern 	u32 g_NumSoundsDormant;
	extern u32 g_NumSoundsPreparing;
	extern u32 g_NumSoundsPlaying;
	extern u32 g_NumSoundsWaitingToBeDeleted;

	audCommandBuffer *g_MixerCommandBuffer = NULL;
	const PcmSourceState *g_PcmSourceState = NULL;

	audPcmSourcePool *g_PcmSourcePool = NULL;
	u32 g_NumPcmSourcePoolSlots = 0;
	u32 g_PcmSourceStateEA = 0;

	u32 audDriver::sm_FrameCounter = 0;
	u32 audWaveSlot::sm_NumWaveSlots = 0;

#if RSG_BANK
	const char *g_SoundNameTable = NULL;
#endif

#if SPU_WAVESLOT_MAP
	atMap<u16, audWaveSlot*> audWaveSlot::sm_LoadedBankTable;
	atMap<u32, audWaveSlot*> audWaveSlot::sm_LoadedWaveTable;
#endif

}// namespace rage

namespace rage
{
u32 * g_MixGroupReferenceCountsEa = NULL;
extern void InitFunctionPointers();
}

using namespace rage;

u32 *audWaveSlot::sm_ReferenceCounts = NULL;

void SoundUpdateJob(sysTaskParameters &p)
{
	TrapNE(sizeof(tSoundUpdateJobData), p.Input.Size);
	TrapNE(sizeof(tSoundUpdateJobResult), p.Output.Size);
	CompileTimeAssert((sizeof(audVoiceSettings) & 15) == 0);
	CompileTimeAssert((sizeof(audSoundPoolBucket) & 15) == 0);
	CompileTimeAssert((sizeof(audRequestedSettings) & 15) == 0);

	tSoundUpdateJobData *data = (tSoundUpdateJobData*)p.Input.Data;
	tSoundUpdateJobResult *result = (tSoundUpdateJobResult*)p.Output.Data;

#if __BANK
	const bool debug = p.UserData[7].asBool;
	g_DisplayAllocations = debug;
	g_SoundNameTable = (const char*)p.UserData[9].asPtr;
#endif

	g_EntityVariableCache.Init();

#if __DEV
	g_EntityVariableCacheHits = 0;
	g_EntityVariableCacheMisses = 0;
#endif

	const u32 numBuckets = (u32)p.UserData[1].asInt;
	const u32 timeInMs = data->timers[1].timeInMs;

	void *commandBufferEA = p.UserData[4].asPtr;
	const u32 commandBufferSize = p.UserData[5].asUInt;

	g_DisabledMetadataSize = data->disabledMetadataSize;
	g_DisabledMetadataPtr = data->disabledMetadataPtr;

	audDriver::SetFrameCount(p.UserData[6].asUInt);

	audEngineUtil::SetRandomSeed((s32)timeInMs);
	
	InitScratchBuffer(p.Scratch.Data, p.Scratch.Size);
	
	audDriver::SetNumOutputChannels(data->numOutputChannels);

	audCommandBuffer mixerCommandBuffer;
	const u32 localCommandBufferSize = 1024U;
	mixerCommandBuffer.Init(commandBufferEA, commandBufferSize, (u8*)AllocateFromScratch(localCommandBufferSize, "Mixer Command Buffer"), localCommandBufferSize, "SoundUpdateJob");
	g_MixerCommandBuffer = &mixerCommandBuffer;

	// grab resolved category settings
	TrapNZ(((size_t)data->listenerData&15));
	
	// grab global variables
	g_GlobalVariableEAStart = data->globalVariables;
	g_EntityVariableEAStart = data->entityVariables;
	g_GlobalVariableEAEnd = g_GlobalVariableEAStart + g_MaxSoundManagerVariables;
	g_EntityVariableEAEnd = g_EntityVariableEAStart + (g_MaxAudEntityVariableBlocks * audVariableBlock::kNumVariablesPerBlock);
	g_PcmSourcePool = (audPcmSourcePool*)data->pcmSourcePool;

	audMixerSyncManager::sm_InstanceEA = data->syncManagerEA;

	g_MixGroupReferenceCountsEa = data->mixGroupReferenceCounts;

	g_GlobalSoundVariables = (variableNameValuePair*)grabData(g_GlobalVariableEAStart, sizeof(variableNameValuePair) * g_MaxSoundManagerVariables, "GlobalVariables");

	audEntity::sm_EntityVariableBlockRefs = data->entityVariableRefs;

	g_audListenerData = (const audEnvironmentListenerData *)grabData(data->listenerData, sizeof(audEnvironmentListenerData) * g_maxListeners, "ListenerData");

	audWaveSlot::sm_ReferenceCounts = (u32*)data->waveSlotReferenceCounts;

	// grab wave slots
	const s32 waveSlotBufSizeUnaligned = audWaveSlot::AUD_WAVE_SLOT_SIZE * data->numWaveSlots;
	const s32 waveSlotBufSize = (waveSlotBufSizeUnaligned+15) & ~15;
	TrapNZ((waveSlotBufSize&15));
	audWaveSlot::SetWaveSlotData((audWaveSlot*)grabData(data->waveSlots, waveSlotBufSize, "WaveSlots"));

#ifdef AUD_DMA_PCM_SOURCE_STATE_DATA
	Assert(p.ReadOnlyCount == 0);
#else
	Assert(p.ReadOnlyCount == 1);
	g_PcmSourceState = (PcmSourceState*)p.ReadOnly[0].Data;
	Assert(p.ReadOnly[0].Size == sizeof(PcmSourceState) * kMaxPcmSources);
#endif
	
	g_NumSoundSlotsPerBucket = data->numSoundSlotsPerBucket;
	
	g_BucketSoundSlotSize = data->soundSlotSize;
	g_NumRequestedSettingsSlotsPerBucketAligned = data->numReqSetSlotsPerBucketAligned;
	g_BucketRequestedSettingsSlotSize = data->reqSetSlotSize;
	g_NumPcmSourcePoolSlots = data->numPcmSourcePoolSlots;

	g_PcmSourceStateEA = (u32)data->pcmSourceState;

	// copy over the requested settings indices
	audRequestedSettings::SetReadIndex(data->requestedSettingsReadIndex);

#if !AUD_SOUND_USE_VIRTUALS
	InitFunctionPointers();
#endif
	
	g_Context.systemTimeMs = timeInMs;
	
	sysMemCpy(g_Context.timers, data->timers, sizeof(g_Context.timers));

	u32 numProcessed = 0;

	bool finished = false;
	
	// mark scratch memory state so that we can reset to here after each bucket
	SetScratchBookmark();

	g_NumSoundsDormant = 0;
	g_NumSoundsPreparing = 0;
	g_NumSoundsPlaying = 0;
	g_NumSoundsWaitingToBeDeleted = 0;

	dmaWait();

	audWaveSlot::sm_NumWaveSlots = (u32)data->numWaveSlots;
#if SPU_WAVESLOT_MAP 
	audWaveSlot::sm_LoadedBankTable.Reset();
	audWaveSlot::sm_LoadedWaveTable.Reset();
#endif
	for(s32 i = 0; i < (s32)data->numWaveSlots; i++)
	{
		audWaveSlot * waveSlot = audWaveSlot::GetWaveSlotFromIndex(i);
		waveSlot->ClearHasChanged();
#if SPU_WAVESLOT_MAP 
		if(waveSlot->GetLoadedBankId() < AUD_INVALID_BANK_ID)
		{
			if(waveSlot->GetLoadedWaveNameHash() > 0)
			{
				waveSlot->RegisterLoadedWave();
			}
			else
			{
				waveSlot->RegisterLoadedBank();
			}
		}
#endif
	}

	const u32 numSoundSlotsPerBucketAligned  = 	(g_NumSoundSlotsPerBucket + 31) & ~31;
	const u32 soundBucketSize = g_BucketSoundSlotSize * g_NumSoundSlotsPerBucket;
	const u32 requestedSettingsBucketSize = g_BucketRequestedSettingsSlotSize * g_NumRequestedSettingsSlotsPerBucketAligned;

	u32 numBucketsProcessed = 0;
	while(!finished && numBucketsProcessed++ < data->numBucketsToProcess)
	{
		// sysInterlockedIncrement returns the incremented value; subtract one to get the bucket to process (ie first
		// call will return 1, in which case we process bucket 0)
		const u32 bucketId = sysInterlockedIncrement((u32*)p.UserData[0].asPtr) - 1;
		if(bucketId >= numBuckets)
		{
			finished = true;
			break;
		}
		else
		{
			// reset the scratch ptr - overwriting the previous buckets data
			ResetScratchToBookmark();

#if AUD_SOUNDPOOL_LOCK_ON_SPU
			// lock the bucket before grabbing the header
			rageCellSyncMutexLock((u32)(data->bucketLocks)+ 4 * bucketId);
#endif

			// process this bucket
			void *bucketEa = (char*)p.UserData[3].asPtr + (data->audSoundPoolBucketSize*bucketId);
			g_Bucket = (audSoundPoolBucket*)grabData(bucketEa, sizeof(audSoundPoolBucket), "BucketHeader");
			
			// need to cache the EAs since I'm going to overwrite them with LS
			void *baseSoundPtr = (u8 *)data->soundPoolBufferEA + (bucketId * (soundBucketSize + requestedSettingsBucketSize));
			void *baseRequestedSettingsPtr = (u8*)baseSoundPtr + soundBucketSize;

			void *localBaseSoundPtr = grabData(baseSoundPtr, soundBucketSize, "BucketSounds");
			void *localBaseReqSetPtr = grabData(baseRequestedSettingsPtr, requestedSettingsBucketSize, "BucketRequestedSettings");

			dmaWait();

			g_Bucket->baseSoundPtr = localBaseSoundPtr;
			g_Bucket->baseRequestedSettingsPtr = localBaseReqSetPtr;
			
			// Reset environment sound list each frame
			g_Bucket->numEnvironmentSoundRequests = 0;

			// store an offset to convert from PPU ptrs to SPU ptrs within the bucket (ie sound variables)
			Assign(g_TaskBucketMemOffset, (char*)g_Bucket->baseSoundPtr-(char*)baseSoundPtr);
			TrapNE((size_t)(RESOLVE_SOUND_VARIABLE_PTR((f32*)baseSoundPtr)), (size_t)(f32*)g_Bucket->baseSoundPtr);

			//audDisplayf("SPU %d bucket: %u (%u free)",p.UserData[2].asInt, bucketId, g_Bucket->numSoundSlotsFree);

			audSoundPool::ParentSoundIterator iter(numSoundSlotsPerBucketAligned, 
													g_BucketSoundSlotSize,
													g_Bucket);

			while(true)
			{
				audSound *sound = iter.Next();
				if(!sound)
					break;

				audSoundManager::ProcessHierarchy(&g_Context,sound);
				numProcessed++;
			}
			
			// dma results back to PPU
	
			// bucket data
			dmaPut(g_Bucket->baseSoundPtr, baseSoundPtr, g_BucketSoundSlotSize * g_NumSoundSlotsPerBucket, false);

			// fix up bucket header
			g_Bucket->baseSoundPtr = baseSoundPtr;
			g_Bucket->baseRequestedSettingsPtr = baseRequestedSettingsPtr;
			
			dmaPut(g_Bucket, bucketEa, sizeof(audSoundPoolBucket), false);
			
			// wait for transfers to complete
			dmaWait();			

			// unlock the bucket
#if AUD_SOUNDPOOL_LOCK_ON_SPU
			rageCellSyncMutexUnlock((u32)(data->bucketLocks)+ 4 * bucketId);
#endif
		}
	}

	result->numSoundsDormant = g_NumSoundsDormant;
	result->numSoundsPlaying = g_NumSoundsPlaying;
	result->numSoundsPreparing = g_NumSoundsPreparing;
	result->numSoundsWaitingToBeDeleted = g_NumSoundsWaitingToBeDeleted;
	result->scratchSpaceLeft = g_ScratchLeft;

#if __DEV
	result->entityVariableCacheHits = g_EntityVariableCacheHits;
	result->entityVariableCacheMisses = g_EntityVariableCacheMisses;
#endif

	// Write final mixer commands back to main memory
	mixerCommandBuffer.Flush();
	result->commandBufferUsed = mixerCommandBuffer.GetMainOffset();
	
	// sync changed wave slot data
	result->numWaveSlotRequests = 0;
	for(s32 i = 0; i < (s32)data->numWaveSlots; i++)
	{
		audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndex(i);
		if(slot->HasChanged())
		{
			if(Verifyf(!slot->IsStreaming(), "Modified streaming wave slot on SPU"))
			{
				Assign(result->waveSlotRequests[result->numWaveSlotRequests].waveSlotId, i);			
				//audDisplayf("slot %u has changed", i);
				slot->_GetSyncState(result->waveSlotRequests[result->numWaveSlotRequests].bankId,result->waveSlotRequests[result->numWaveSlotRequests].waveNameHash,result->waveSlotRequests[result->numWaveSlotRequests].loadPriority);
			
				result->numWaveSlotRequests++;
			}
			else
			{
				audErrorf("attempting to request streaming load from spu");
			}
			TrapGE(result->numWaveSlotRequests, g_MaxWaveSlotRequestsPerJob);
		}
	}

}

#else // __SPU

#endif // __SPU
