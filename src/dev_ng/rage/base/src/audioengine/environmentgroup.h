// 
// audioengine/environmentgroup.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_ENVIRONMENTGROUP_H
#define AUD_ENVIRONMENTGROUP_H

#include "enginedefs.h"
#include "environment.h"

namespace rage {

class audEntity;
struct audMetadataChunk;

class audEnvironmentGroupInterface
{
	friend class audController;
public:
	audEnvironmentGroupInterface();
	virtual ~audEnvironmentGroupInterface();

#if !__SPU

	virtual void BaseInit(audEntity* entity);
	virtual void Update(u32 timeInMs) = 0;
	virtual void FullyUpdate(u32 timeInMs) = 0;

	audEnvironmentGameMetric &GetEnvironmentGameMetric() { return m_EnvironmentGameMetric; }

#endif
	// PURPOSE
	//  Populates a passed in metric, copying over our own one.
	void PopulateEnvironmentMetric(audEnvironmentGameMetric* environmentMetric);
	// PURPOSE
	//  Environment groups may well want to outlive their creating entities, and only delete themselves
	//  when they no longer have any sounds that are referencing them. This fn is called whenever
	//  a requested settings structure gets pointed at the environment group, and hence can be ref counted.
	//  NEVER call this willy-nilly from gamecode - it should only be done from ReqSets, or if you really REALLY know what you're doing.
	void AddSoundReference();
	// PURPOSE
	//  Called when a requested settings structure is pointed away from an environment group, for ref counting purposes.
	//  NEVER call this willy-nilly from gamecode - it should only be done from ReqSets, or if you really REALLY know what you're doing.
	void RemoveSoundReference();
	// PURPOSE
	//  Returns the number of registered sound - used for automatic deletion
	u32 GetNumberOfSoundReference() const;
	// PURPOSE
	//  Sets the owning audio entity - used when entities report they're being deleted - actually set from BaseInit()
	void SetEntity(audEntity* entity) { m_Entity = entity; }
	// PURPOSE
	//  Returns the owning audio entity - used for automatic deletion
	audEntity* GetEntity() { return m_Entity; }

	// PURPOSE
	//  Returns the index of the next environment group in the controller list
	u32 GetNextIndex() const { return m_NextIndex; }

	// PURPOSE
	//  Returns the index of the prev environment group in the controller list
	u32 GetPrevIndex() const { return m_PrevIndex; }

	//Purpose
	//Makes this environment group a member of the specified mix group so that it may be dynamically mixed
	void SetMixGroup(s32 mixgroupIndex); 

	//Purpose
	//Makes this environment group a member of the specified mix group so that it may be dynamically mixed
	void SetPrevMixGroup(s32 mixgroupIndex); 

	//Purpose
	//Removes the environment group from its current mix group
	void UnsetMixGroup(); 

	//Purpose
	//Returns the current mix group that this environment group is a member of if any
	s32 GetMixGroupIndex() { return m_MixGroupIndex; }

	//Purpose
	//Returns the prev mix group that this environment group is a member of if any
	s32 GetPrevMixGroupIndex() { return m_PrevMixGroupIndex; }

	virtual void UpdateMixGroup() = 0;

#if __BANK
	void DrawEntityBoundingBox(const Color32 colour);
	void SetDebugIdentifier(const char* identifier)		{ m_DebugIdentifier = identifier; }
	const char* GetDebugIdentifier() const				{ return m_DebugIdentifier; }
#endif

protected:


	audEnvironmentGameMetric m_EnvironmentGameMetric;

private:
	u32 m_ReferencingSounds;
	audEntity* m_Entity;
	u16 m_NextIndex;
	u16 m_PrevIndex;
	s16 m_MixGroupIndex;
	s16 m_PrevMixGroupIndex;

#if __BANK
	const char* m_DebugIdentifier;
#endif

#if !__SPU
//	static sysIpcSema sm_NewOcclusionGroupListLock;
#endif
};

class audEnvironmentGroupManager
{
public:
	audEnvironmentGroupManager(){};
	virtual ~audEnvironmentGroupManager(){};

	virtual void UpdateEnvironmentGroups(audEnvironmentGroupInterface** environmentGroupList, u32 maxGroups, u32 timeInMs) = 0;

	virtual void RegisterGameObjectMetadataUnloading(audEnvironmentGroupInterface** environmentGroupList, const audMetadataChunk& chunk) = 0;

	// Linked environmentGroups can be set during the game update (ped in car sets vehicle as it's linked group)
	// We need to track which groups were deleted this frame so we don't try and use a deleted group 
	void RegisterDeletedEnvironmentGroup(audEnvironmentGroupInterface* environmentGroup);

protected:

	bool IsEnvironmentGroupInDeletedList(audEnvironmentGroupInterface* environmentGroup) const;

protected:

	atFixedArray<audEnvironmentGroupInterface*, 512> m_DeletedEnvironmentGroups;

};

} //namespace rage

#endif // AUD_ENVIRONMENTGROUP_H

