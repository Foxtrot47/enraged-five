// 
// audioengine/ambisonics.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "ambisonics.h"
#include "audiohardware/channel.h"

#if AUD_ENABLE_AMBISONICS

#include "engine.h"

#if !__SPU
#include "audiohardware/driverutil.h"
#include "audioengine/engineutil.h"
#include "bank/bank.h"
#include "system/task.h"
#include "profile/profiler.h"

#include "ambisonicoptimizerjob.h"

#endif


namespace rage
{
	float g_AmbisonicsDecoderScaling = 1.f;
	float g_AmbisonicsLeakScaling = 0.293f;
	float g_AmbisonicDebugLeak = 1.f;
#if !__SPU
	//////////////////////////////////////////////
	/////////// Globals and statics //////////////
	//////////////////////////////////////////////

PF_PAGE(AmbisonicsTimingPage,"Ambisonics Timing");
PF_GROUP(AmbisonicsTiming);
PF_LINK(AmbisonicsTimingPage, AmbisonicsTiming);
PF_TIMER(CalculateFitness, AmbisonicsTiming);
PF_TIMER(OptimizeDecoder, AmbisonicsTiming);


//Fitness weightings which steer the decoder optimization
#if __BANK
	static const f32 fitMaxMeMv1[FITNESS_WEIGHT_COUNT] = {0.f, 0.25f, 0.15f, 0.1f, 0.5f, 0.6f};
	static const f32 fitMaxMeMv2[FITNESS_WEIGHT_COUNT] = {0.f, 0.15f, 0.15f, 0.1f, 0.9f, 0.6f};

	bool g_OverrideEnvironmentDecoder = false;
#endif

static const f32 fitMaxAeAv[FITNESS_WEIGHT_COUNT] = {0.f, 0.15f, 0.25f, 0.1f, 0.5f, 0.7f};

static TabuSpuOutput spuOutput1, spuOutput2, spuOutput3, spuOutput4, spuOutput5, spuOutput6;

AmbisonicDecoderMode audAmbisonics::sm_DecoderMode = kAmbisonicsItu;


static void ResetSpuOutputs()
{
	spuOutput1.Init();
	spuOutput2.Init();
	spuOutput3.Init();
	spuOutput4.Init();
	spuOutput5.Init();
	spuOutput6.Init();
}

speakerSetup::speakerSetup():
	angle(0.f),
	angleCos(1.f),
	angleSin(0.f),
	isActive(false)
{
}

TabuSpuOutput::TabuSpuOutput():
bestLocalFit(99999999999999.f),
fitSpeaker(0),
fitOrder(0),
fitDir(0.f)
{
	audAmbisonics::ZeroSpeakerWeights(localBest);
	audAmbisonics::ZeroSpeakerWeights(direction);
	audAmbisonics::ZeroSpeakerWeights(localUp);
	audAmbisonics::ZeroSpeakerWeights(localDown);
}

void TabuSpuOutput::Init()
{
	bestLocalFit=99999999999999.f;
	fitSpeaker=0;
	fitOrder=0;
	fitDir=0.f;
	audAmbisonics::ZeroSpeakerWeights(localBest);
	audAmbisonics::ZeroSpeakerWeights(direction);
	audAmbisonics::ZeroSpeakerWeights(localUp);
	audAmbisonics::ZeroSpeakerWeights(localDown);
}

//Default positions for standard speaker setups
static const f32 squareSpeakers[AMBISONICS_SPEAKER_MAX] = {45.f, 315.f, 0.f, 135.f, 215.f, 0.f, 0.f};
static const f32 ituSpeakers[AMBISONICS_SPEAKER_MAX] = {30.f, 330.f, 0.f, 110.f, 250.f, 0.f, 0.f};
static const f32 ituSideSpeakers[AMBISONICS_SPEAKER_MAX] = {30.f, 330.f, 0.f, 90.f, 270.f, 0.f, 0.f};
static const f32 ituNarrowSpeakers[AMBISONICS_SPEAKER_MAX] = {15.f, 345.f, 0.f, 110.f, 250.f, 0.f, 0.f};
static const f32 fnrrSpeakers[AMBISONICS_SPEAKER_MAX] = {15.f, 345.f, 0.f, 135.f, 225.f, 0.f, 0.f};
static const f32 fnrsSpeakers[AMBISONICS_SPEAKER_MAX] = {15.f, 345.f, 0.f, 90.f, 270.f, 0.f, 0.f};
static const f32 fwrsSpeakers[AMBISONICS_SPEAKER_MAX] = {60.f, 300.f, 0.f, 90.f, 270.f, 0.f, 0.f};
static const f32 fwrmSpeakers[AMBISONICS_SPEAKER_MAX] = {60.f, 300.f, 0.f, 110.f, 250.f, 0.f, 0.f};
static const f32 fwrrSpeakers[AMBISONICS_SPEAKER_MAX] = {60.f, 300.f, 0.f, 135.f, 225.f, 0.f, 0.f};


//NB Whole-decoder scaling is currently stored in decoder[6][10]

//Pre-optimized decoders for standard itu setup
//x	    y       u       v       p       q       c4      s4      c5    s5    w
static f32 maxMeMv2[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = {{0.330f,  0.280f, 0.030f, 0.195f,-0.020f, 0.060f, 0.030f,-0.005f, 0.0f, 0.0f, 0.330f}, //FL
{0.330f, -0.280f, 0.030f,-0.195f,-0.020f,-0.060f, 0.030f, 0.005f, 0.0f, 0.0f, 0.330f}, //FR
{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f}, //C
{-0.35f,   0.405f,-0.040f,-0.010f,-0.020f,-0.060f,-0.025f, 0.030f, 0.0f, 0.0f, 0.565f}, //SL
{-0.35f,  -0.405f,-0.040f, 0.010f,-0.020f, 0.060f,-0.025f,-0.030f, 0.0f, 0.0f, 0.565f}, //SR
{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f},  //Dummy line for 7.1 BL (not used)
{1.f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 1.f}}; //Dummy line for 7.1 BR (not used)

//Itu decoder with no center speaker
static f32 ituNC1[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = {{0.356f, 0.291f, 0.048f, 0.097f, 0.030f, 0.011f, -0.035f, -0.025f, 0.021f, 0.030f, 0.334f}, //FL
{0.356f, -0.291f, 0.048f, -0.097f, 0.030f, -0.011f, -0.035f, 0.025f, 0.021f, -0.030f, 0.330f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.307f, 0.312f, -0.038f, 0.072f, 0.020f, -0.049f, -0.027f, -0.018f, -0.024f, 0.010f, 0.565f}, //BL
{-0.307f, -0.312f, -0.038f, -0.072f, 0.020f, 0.049f, -0.027f, 0.018f, -0.024f, -0.010f, 0.565f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{1.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 1.000f}}; //XR

//Square decdoder optimized with 5th order tabu
static f32 tabuSquare[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = {{0.317f, 0.354f, 0.007f, 0.039f, 0.019f, -0.013f, -0.002f, 0.003f, -0.009f, -0.017f, 0.326f}, //FL
{0.317f, -0.354f, 0.007f, -0.039f, 0.019f, 0.013f, -0.002f, -0.003f, -0.009f, 0.017f, 0.354f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.329f, 0.330f, 0.006f, -0.056f, -0.017f, -0.003f, 0.007f, -0.009f, 0.008f, -0.007f, 0.313f}, //BL
{-0.329f, -0.330f, 0.006f, 0.056f, -0.017f, 0.003f, 0.007f, 0.009f, 0.008f, 0.007f, 0.354f}, //BR
{0.220f, 0.000f, 0.185f, 0.000f, 0.060f, 0.000f, -0.150f, 0.000f, 0.000f, 0.000f, 0.095f}, //XL
{1.f, 0.000f, 0.185f, 0.000f, 0.060f, 0.000f, -0.150f, 0.000f, 0.000f, 0.000f, 1.f}}; //XR

////x        y       u       v       p        q       c4      s4       c5    s5    w
//static f32 maxMeMv[SPEAKER_SETUP_COUNT][AMB_ORDER*2+1] =     {{0.405f,  0.31f,  0.025f, 0.175f, -0.01f,  0.045f, 0.03f,  0.005f,  0.0f, 0.0f, 0.275f}, //FL
//{0.405f, -0.31f,  0.025f,-0.175f, -0.01f, -0.045f, 0.03f, -0.005f,  0.0f, 0.0f, 0.275f}, //FR
//{0.225f,  0.000f, 0.120f, 0.000f,  0.055f, 0.000f,-0.005f, 0.000f,  0.0f, 0.0f, 0.000f}, //C
//{-0.37f,   0.405f,-0.055f, 0.045f,  0.00f, -0.050f,-0.015f, 0.030f,  0.0f, 0.0f, 0.535f}, //SL
//{-0.37f,  -0.405f,-0.055f,-0.045f,  0.00f,  0.050f,-0.015f -0.030f,  0.0f, 0.0f, 0.535f}, //SR
//{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f},  //Dummy line for 7.1 BL (not used)
//{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f}}; //Dummy line for 7.1 BR (not used)
//
////x	   y       u       v       p       q       c4      s4      c5    s5    w
//static f32 maxAeAv[SPEAKER_SETUP_COUNT][AMB_ORDER*2+1] =	 {{0.295f, 0.275f,-0.010f, 0.140f,-0.010f, 0.055f, 0.020f,-0.015f, 0.0f, 0.0f, 0.350f}, //FL
//{0.295f,-0.275f,-0.010f,-0.140f,-0.010f,-0.055f, 0.020f, 0.015f, 0.0f, 0.0f, 0.350f}, //FR
//{0.250f, 0.000f, 0.210f, 0.000f, 0.100f, 0.000f,-0.050f, 0.000f, 0.0f, 0.0f, 0.145f}, //C
//{-0.315f, 0.330f,-0.030f, 0.080f, 0.040f, 0.030f, 0.000f,-0.010f, 0.0f, 0.0f, 0.635f}, //SL
//{-0.315f,-0.330f,-0.030f,-0.080f, 0.040f, 0.030f, 0.000f, 0.010f, 0.0f, 0.0f, 0.635f}, //SR
//{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f},  //Dummy line for 7.1 BL (not used)
//{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f}}; //Dummy line for 7.1 BR (not used)
//


////Pre-optimized decoders for standard square setup
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 energyDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] =   {{0.25f,  0.25f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.354f}, //FL
{0.25f, -0.25f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.354f}, //FR
{0.f,    0.f,   0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f},   //C
{-0.25f,  0.25f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.354f}, //BL
{-0.25f, -0.25f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.354f}, //BR
{0.220f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 0.095f},  //Dummy line for 7.1 BL (not used)
{1.f,  0.000f, 0.185f, 0.000f, 0.060f, 0.000f,-0.150f, 0.000f, 0.0f, 0.0f, 1.f}}; //Dummy line for 7.1 BR (not used)

////Pre-optimized decoder surround with side-speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 sideDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = {
{0.670f, 0.538f, 0.114f, 0.221f, 0.029f, 0.022f, 0.001f, -0.037f, 0.001f, 0.037f, 0.363f}, //FL
{0.670f, -0.538f, 0.114f, -0.221f, 0.029f, -0.022f, 0.001f, 0.037f, 0.001f, -0.037f, 0.344f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.552f, 0.508f, -0.113f, 0.093f, -0.008f, -0.088f, -0.065f, -0.015f, -0.025f, 0.021f, 0.893f}, //BL
{-0.552f, -0.508f, -0.113f, -0.093f, -0.008f, 0.088f, -0.065f, 0.015f, -0.025f, -0.021f, 0.891f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{1.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.750f}}; //XR

////Pre-optimized decoder for itu wih narrow front speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 narrowItuDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = {
{0.349f, 0.252f, 0.194f, 0.117f, 0.114f, 0.099f, -0.050f, -0.001f, 0.017f, 0.015f, 0.200f}, //FL
{0.349f, -0.252f, 0.194f, -0.117f, 0.114f, -0.099f, -0.050f, 0.001f, 0.017f, -0.015f, 0.195f}, //FR
{0.350f, -0.000f, -0.158f, -0.000f, -0.070f, -0.000f, 0.006f, -0.000f, 0.006f, -0.000f, 0.522f}, //CENTER
{-0.391f, 0.423f, -0.071f, 0.119f, -0.000f, -0.040f, -0.049f, -0.012f, -0.024f, 0.012f, 0.738f}, //BL
{-0.391f, -0.423f, -0.071f, -0.119f, -0.000f, 0.040f, -0.049f, 0.012f, -0.024f, -0.012f, 0.738f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.900f}}; //XR

////Pre-optimized decoder for narrow front, rear rear speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 fnrrDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = {
{0.319f, 0.356f, 0.008f, 0.038f, 0.018f, -0.013f, -0.002f, 0.003f, -0.009f, -0.016f, 0.328f}, //FL
{0.319f, -0.356f, 0.008f, -0.038f, 0.018f, 0.013f, -0.002f, -0.003f, -0.009f, 0.016f, 0.354f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.332f, 0.333f, 0.006f, -0.055f, -0.017f, -0.002f, 0.006f, -0.008f, 0.008f, -0.007f, 0.314f}, //BL
{-0.332f, -0.333f, 0.006f, 0.055f, -0.017f, 0.002f, 0.006f, 0.008f, 0.008f, 0.007f, 0.354f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 1.000f}}; //XR

////Pre-optimized decoder for narrow front, rear side speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 fnrsDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = 
{{0.319f, 0.356f, 0.008f, 0.038f, 0.018f, -0.013f, -0.002f, 0.003f, -0.009f, -0.016f, 0.328f}, //FL
{0.319f, -0.356f, 0.008f, -0.038f, 0.018f, 0.013f, -0.002f, -0.003f, -0.009f, 0.016f, 0.354f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.332f, 0.333f, 0.006f, -0.055f, -0.017f, -0.002f, 0.006f, -0.008f, 0.008f, -0.007f, 0.314f}, //BL
{-0.332f, -0.333f, 0.006f, 0.055f, -0.017f, 0.002f, 0.006f, 0.008f, 0.008f, 0.007f, 0.354f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 1.000f}}; //XR

////Pre-optimized decoder for wide front, rear side speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 fwrsDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = 
{{0.317f, 0.354f, 0.007f, 0.037f, 0.018f, -0.012f, -0.002f, 0.002f, -0.008f, -0.016f, 0.324f}, //FL
{0.317f, -0.354f, 0.007f, -0.037f, 0.018f, 0.012f, -0.002f, -0.002f, -0.008f, 0.016f, 0.354f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.329f, 0.329f, 0.006f, -0.054f, -0.017f, -0.003f, 0.007f, -0.009f, 0.008f, -0.007f, 0.312f}, //BL
{-0.329f, -0.328f, 0.006f, 0.054f, -0.017f, 0.003f, 0.007f, 0.009f, 0.008f, 0.007f, 0.354f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 1.00f}}; //XR

/*
////Pre-optimized decoder for wide front, rear back speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 fwrrDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = 
{{0.295f, 0.226f, 0.044f, 0.151f, -0.074f, 0.015f, -0.090f, -0.031f, -0.007f, 0.011f, 0.344f}, //FL
{0.295f, -0.225f, 0.044f, -0.151f, -0.074f, -0.015f, -0.090f, 0.031f, -0.007f, -0.011f, 0.354f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.165f, 0.290f, -0.062f, 0.124f, -0.059f, -0.033f, -0.065f, -0.044f, -0.012f, -0.011f, 0.356f}, //BL
{-0.165f, -0.290f, -0.062f, -0.124f, -0.059f, 0.033f, -0.065f, 0.044f, -0.012f, 0.011f, 0.354f}, //BR 
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 1.000f}}; //XR
*/

////Pre-optimized decoder for wide front rear back speakers NEW
static f32 fwrrNewDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = 
{{0.329f, 0.276f, -0.042f, -0.025f, 0.013f, -0.040f, -0.016f, 0.008f, 0.008f, 0.006f, 0.354f}, //FL
{0.329f, -0.275f, -0.042f, 0.025f, 0.013f, 0.040f, -0.016f, -0.008f, 0.008f, -0.006f, 0.354f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.318f, 0.262f, 0.034f, -0.063f, 0.003f, -0.005f, 0.010f, 0.012f, -0.007f, 0.008f, 0.352f}, //BL
{-0.318f, -0.262f, 0.034f, 0.063f, 0.003f, 0.005f, 0.010f, -0.012f, -0.007f, -0.008f, 0.354f}, //BR
{0.220f, 0.000f, 0.185f, 0.000f, 0.060f, 0.000f, -0.150f, 0.000f, 0.000f, 0.000f, 0.095f}, //XL
{1.000f, 0.000f, 0.185f, 0.000f, 0.060f, 0.000f, -0.150f, 0.000f, 0.000f, 0.000f, 1.000f}}; //XR

/*
////Pre-optimized decoder for narrow front, rear medium speakers
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 fwrmDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = 
{{0.525f, 0.419f, 0.084f, 0.164f, 0.028f, 0.017f, -0.022f, -0.032f, 0.009f, 0.041f, 0.363f}, //FL
{0.525f, -0.419f, 0.084f, -0.164f, 0.028f, -0.017f, -0.022f, 0.032f, 0.009f, -0.041f, 0.350f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.434f, 0.417f, -0.086f, 0.070f, -0.005f, -0.072f, -0.056f, -0.005f, -0.029f, 0.022f, 0.740f}, //BL
{-0.434f, -0.416f, -0.086f, -0.070f, -0.005f, 0.072f, -0.056f, 0.005f, -0.029f, -0.022f, 0.740f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.900f}}; //XR
*/

////Pre-optimized decoder for narrow front, rear medium speakers NEW
////x       y     u    v    p    q    c4   s4   c5   s5   w
static f32 fwrmNewDecoder[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1] = 
{{0.839f, 0.518f, -0.041f, 0.056f, -0.100f, -0.056f, 0.004f, -0.028f, 0.022f, 0.012f, 0.622f}, //FL
{0.839f, -0.518f, -0.041f, -0.056f, -0.100f, 0.056f, 0.004f, 0.028f, 0.022f, -0.012f, 0.622f}, //FR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //CENTER
{-0.800f, 0.450f, -0.161f, 0.035f, 0.091f, -0.061f, 0.054f, -0.011f, 0.006f, 0.011f, 0.768f}, //BL
{-0.800f, -0.450f, -0.161f, -0.035f, 0.091f, 0.061f, 0.054f, 0.011f, 0.006f, -0.011f, 0.768f}, //BR
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f}, //XL
{0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.000f, 0.500f}}; //XR


drawFuncPtr audAmbisonics::drawFunc;

void audAmbisonics::MeMv2Decode()
{
	DefaultItu();
	SetupBFormatCoefficients(decodeData.decoder, maxMeMv2);
}

void audAmbisonics::ituNc1Decode()
{
	ItuNC();
	SetupBFormatCoefficients(decodeData.decoder, ituNC1);
}

void audAmbisonics::itu15Decode()
{
	SetupDecode(ituNarrowSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, narrowItuDecoder);
}

void audAmbisonics::fnrrDecode()
{
	SetupDecode(fnrrSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, fnrrDecoder);
}

void audAmbisonics::fnrsDecode()
{
	SetupDecode(fnrsSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, fnrsDecoder);
}

void audAmbisonics::fwrsDecode()
{
	SetupDecode(fwrsSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, fwrsDecoder);
}

void audAmbisonics::fwrmDecode()
{
	SetupDecode(fwrmSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, fwrmNewDecoder);
}

void audAmbisonics::fwrrDecode()
{
	SetupDecode(fwrrSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, fwrrNewDecoder);
}

void audAmbisonics::tabuSquareDecode()
{
	DefaultSquare();
	SetupBFormatCoefficients(decodeData.decoder, tabuSquare);
}

void audAmbisonics::HeadphoneDecode()
{
	DefaultSquare();
	SetupBFormatCoefficients(decodeData.decoder, energyDecoder);
}

void audAmbisonics::EnergyDecode()
{
	DefaultSquare();
	SetupBFormatCoefficients(decodeData.decoder, energyDecoder);
}

void audAmbisonics::SideDecode()
{
	SetupDecode(ituSideSpeakers);
	SetupBFormatCoefficients(decodeData.decoder, sideDecoder);
}




void audAmbisonics::Temp1Decode()
{
	switch(tempDecoder1Type)
	{
	case SQUARE_DECODER:
		{
			DefaultSquare();
			LocalCopySpeakerWeights(decodeData.decoder, tempDecoder1);
			decodeData.decoderType = SQUARE_DECODER;
		}
		break;
	case ITU_DECODER:
		{
			DefaultItu();
			LocalCopySpeakerWeights(decodeData.decoder, tempDecoder1);
			decodeData.decoderType = ITU_DECODER;
		}
		break;
	case NO_DECODER:
		audDisplayf("Can't load temp1 decoder as it has not been set");
		break;
	}
}

void audAmbisonics::Temp2Decode()
{
	switch(tempDecoder2Type)
	{
	case SQUARE_DECODER:
		{
			DefaultSquare();
			LocalCopySpeakerWeights(decodeData.decoder, tempDecoder2);
			decodeData.decoderType = SQUARE_DECODER;
		}
		break;
	case ITU_DECODER:
		{
			DefaultItu();
			LocalCopySpeakerWeights(decodeData.decoder, tempDecoder2);
			decodeData.decoderType = ITU_DECODER;
		}
		break;
	case NO_DECODER:
		audDisplayf("Can't load temp2 decoder as it has not been set");
		break;
	}
}

void audAmbisonics::Temp3Decode()
{
	switch(tempDecoder3Type)
	{
	case SQUARE_DECODER:
		{
			DefaultSquare();
			LocalCopySpeakerWeights(decodeData.decoder, tempDecoder3);
			decodeData.decoderType = SQUARE_DECODER;
		}
		break;
	case ITU_DECODER:
		{
			DefaultItu();
			LocalCopySpeakerWeights(decodeData.decoder, tempDecoder3);
			decodeData.decoderType = ITU_DECODER;
		}
		break;
	case NO_DECODER:
		audDisplayf("Can't load temp3 decoder as it has not been set");
		break;
	}
}

void audAmbisonics::SetTemp1Decoder()
{
	LocalCopySpeakerWeights(tempDecoder1, decodeData.decoder);
	tempDecoder1Type = decodeData.decoderType;
}

void audAmbisonics::SetTemp2Decoder()
{
	LocalCopySpeakerWeights(tempDecoder2, decodeData.decoder);
	tempDecoder2Type = decodeData.decoderType;
}

void audAmbisonics::SetTemp3Decoder()
{
	LocalCopySpeakerWeights(tempDecoder2, decodeData.decoder);
	tempDecoder3Type = decodeData.decoderType;
}

void audAmbisonics::DefaultSquare()
{
	SetupSpeakers(squareSpeakers);
	for(int i=0; i<5; i++) //Activate 5.0 speaker setup...
	{
		speakers[i].isActive = true;
	}
	speakers[AMBISONICS_SPEAKER_CENTER].isActive = false; //...then turn off the center
	speakers[AMBISONICS_SPEAKER_L_EX].isActive = false; //turn off back (7.1) speakers
	speakers[AMBISONICS_SPEAKER_R_EX].isActive = false; 

	for(int i=0; i<AMB_ORDER; i++)
	{
		decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_L_EX].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_L_EX].Si[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_R_EX].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_R_EX].Si[i] = 0.f;
	}
	decodeData.decoder[AMBISONICS_SPEAKER_CENTER].w = 0.f;
	decodeData.decoderType = SQUARE_DECODER;
	m_TargetOrder = 5; //Squre decoders can work even with just first order but we'll use 5th since we already have a computed first order decoder
}

void audAmbisonics::DefaultItu()
{
	SetupSpeakers(ituSpeakers);
	for(int i=0; i<5; i++) //Activate 5.0 speaker setup
	{
		speakers[i].isActive = true;
	}
	speakers[AMBISONICS_SPEAKER_L_EX].isActive = false; //turn off back (7.1) speakers
	speakers[AMBISONICS_SPEAKER_R_EX].isActive = false; 

	m_TargetOrder = 5; //use max ambisonic order for irregular arrays

	decodeData.decoderType = ITU_DECODER;
}

void audAmbisonics::ItuNC()
{
	SetupSpeakers(ituSpeakers);
	for(int i=0; i<5; i++) //Activate 5.0 speaker setup
	{
		speakers[i].isActive = true;
	}

	for(int i=0; i<AMB_ORDER; i++)
	{
		decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_L_EX].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_L_EX].Si[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_R_EX].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_R_EX].Si[i] = 0.f;
	}
	decodeData.decoder[AMBISONICS_SPEAKER_CENTER].w = 0.f;
	speakers[AMBISONICS_SPEAKER_L_EX].isActive = false; //turn off back (7.1) speakers
	speakers[AMBISONICS_SPEAKER_R_EX].isActive = false; 
	speakers[AMBISONICS_SPEAKER_CENTER].isActive = false;

	m_TargetOrder = 5; //use max ambisonic order for irregular arrays

	decodeData.decoderType = ITU_DECODER;
}

void audAmbisonics::FwrrSpeakerSetup()
{
	SetupSpeakers(fwrrSpeakers);
}

void audAmbisonics::FwrmSpeakerSetup()
{
	SetupSpeakers(fwrmSpeakers);
}

void audAmbisonics::SetupDecode(const f32 speakerAngles[AMBISONICS_SPEAKER_MAX])
{
	SetupSpeakers(speakerAngles);
	for(int i=0; i<5; i++) //Activate 5.0 speaker setup
	{
		speakers[i].isActive = true;
	}

	for(int i=0; i<AMB_ORDER; i++)
	{
		decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_L_EX].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_L_EX].Si[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_R_EX].Co[i] = 0.f;
		decodeData.decoder[AMBISONICS_SPEAKER_R_EX].Si[i] = 0.f;
	}
	decodeData.decoder[AMBISONICS_SPEAKER_CENTER].w = 0.f;
	speakers[AMBISONICS_SPEAKER_L_EX].isActive = false; //turn off back (7.1) speakers
	speakers[AMBISONICS_SPEAKER_R_EX].isActive = false; 
	speakers[AMBISONICS_SPEAKER_CENTER].isActive = false;

	m_TargetOrder = 5; //use max ambisonic order for irregular arrays

	decodeData.decoderType = ITU_DECODER;
}

void audAmbisonics::ApplySpeakerSettingsToDecoder ()
{
	for(u32 i = 0; i<AMBISONICS_SPEAKER_MAX; i++)
	{
		if(!speakers[i].isActive)
		{
			for(u32 j = 0; j < AMB_ORDER; j++)
			{
				decodeData.decoder[i].Co[j] = 0.f;
				decodeData.decoder[i].Si[j] = 0.f;
				if(j==0)
				{
					switch(i)
					{
					case AMBISONICS_SPEAKER_L_FRONT:
						decodeData.decoder[i].Co[j] = 0.f;
						decodeData.decoder[i].Si[j] = 0.f;
						break;
					case AMBISONICS_SPEAKER_R_FRONT:
						decodeData.decoder[i].Co[j] = 0.f;
						decodeData.decoder[i].Si[j] = 0.f;
						break;
					case AMBISONICS_SPEAKER_L_BACK:
						decodeData.decoder[i].Co[j] = 0.f;
						decodeData.decoder[i].Si[j] = 0.f;
						break;
					case AMBISONICS_SPEAKER_R_BACK:
						decodeData.decoder[i].Co[j] = 0.f;
						decodeData.decoder[i].Si[j] = 0.f;
						break;
					}
				}
			}
			decodeData.decoder[i].w = 0.f;
		}
	}
}

#if __BANK
void audAmbisonics::DefaultSquareCallback(audAmbisonics *_this)
{
	_this->DefaultSquare();
}

void audAmbisonics::DefaultItuCallback(audAmbisonics *_this)
{
	_this->SetupSpeakers(ituSpeakers);
}

void audAmbisonics::NCItuCallback(audAmbisonics *_this)
{
	_this->ItuNC();
}

void audAmbisonics::FwrrCallback(audAmbisonics *_this)
{
	_this->FwrrSpeakerSetup();
}

void audAmbisonics::FwrmCallback(audAmbisonics *_this)
{
	_this->FwrmSpeakerSetup();
}

void audAmbisonics::MaxMeMv1(audAmbisonics *_this)
{
	_this->SetupFitnessWeights(fitMaxMeMv1);
}

void audAmbisonics::MaxMeMv2(audAmbisonics *_this)
{
	_this->SetupFitnessWeights(fitMaxMeMv2);
}

void audAmbisonics::MaxAeAv(audAmbisonics *_this)
{
	_this->SetupFitnessWeights(fitMaxAeAv);
}
#endif //__BANK

#endif //!__SPU

ambisonicDecodeData::ambisonicDecodeData()
{
	audAmbisonics::ZeroSpeakerWeights(decoder);
	sysMemSet(&bFormat, 0, sizeof(bFormatParms));
	sysMemSet(&speakerGains, 0, sizeof(speakerGains));
	proportionOut = 1.f;
	elevationComponent = 1.f;
	occlusionLeak = 1.f;
	decoderType = NO_DECODER;
}


#if !__SPU

audAmbisonics::audAmbisonics()
{
	Init();
}

audAmbisonics::audAmbisonics(audAmbisonics *cpy)
{
	sysMemCpy(this, cpy, sizeof(audAmbisonics));
}

void audAmbisonics::Init()
{
	m_TabuSteps = 50;
	m_AmbOrder = 1;
	m_TargetOrder = 5;
	m_DoFineTuning = true;
	m_UseSpuOptimize = true;
	m_StartTime = 0;
	m_DoDraw = false;
	m_DrawBestDecoder = false;

	//m_StereoDecode = NO_STEREO_DECODE;

	m_UpperBand = 0.35f;
	m_LowBand = 0.35f;
	bestFitness = 99999999999999.f;

	DefaultItu();

	ituNc1Decode();

	SetupFitnessWeights(fitMaxAeAv);
	ZeroSpeakerWeights(tabuWeights);
	
	ZeroSpeakerWeights(tempDecoder1);
	ZeroSpeakerWeights(tempDecoder2);
	ZeroSpeakerWeights(tempDecoder3);
	tempDecoder1Type = NO_DECODER;
	tempDecoder2Type = NO_DECODER;
	tempDecoder3Type = NO_DECODER;

	for(int i = 0; i<AMBISONICS_SPEAKER_MAX; i++)
	{
		decodeData.speakerGains[i] = 0.f;
	}

	InitSearchParams();

	SetEnvironmentDecoder(decodeData);

}

void audAmbisonics::SetEnvironmentDecoder(ambisonicDecodeData &data)
{
	g_AudioEngine.GetEnvironment().SetDecoder(data);
}

void audAmbisonics::SetEnvironmentDecoder()
{
	g_AudioEngine.GetEnvironment().SetDecoder(decodeData);
}

void audAmbisonics::SetEnvironmentDecoder(AmbisonicDecoderMode mode) 
{
#if __BANK
	if(g_OverrideEnvironmentDecoder)
	{
		return;
	}
#endif

	switch(mode)
	{
	case kAmbisonicsSquare:
		EnergyDecode();
		break;
	case kAmbisonicsItu:
		ituNc1Decode();
		break;
	case kAmbisonicsItuSide:
		SideDecode();
		break;
	case kAmbisonicsStereo:
		EnergyDecode();
		break;
	case kAmbisonicsHeadphones:
		EnergyDecode();
		break;
	case kAmbisonicsItuFront15:
		itu15Decode();
		break;
	case kAmbisonicsFwRr:
		fwrrDecode();
		break;
	case kAmbisonicsFwRm:
		fwrmDecode();
		break;
	case kAmbisonicsFwRs:
		fwrsDecode();
		break;
	case kAmbisonicsFnRr:
		fnrrDecode();
		break;
	case kAmbisonicsFnRm:
		itu15Decode();
		break;
	case kAmbisonicsFnRs:
		fnrsDecode();
		break;
	case kAmbisonicsFmRr: //Square
		EnergyDecode();
		break;
	case kAmbisonicsFmRm: //itu
		ituNc1Decode();
		break;
	case kAmbisonicsFmRs: //ituSide
		SideDecode();
		break;
	default:
		EnergyDecode();
	}

	SetEnvironmentDecoder();
}

void audAmbisonics::GetDecoder(ambisonicDecodeData &data)
{
	LocalCopySpeakerWeights(data.decoder, decodeData.decoder);
}

void audAmbisonics::InitSearchParams()
{
	m_Delta = 0.01f;
	m_IterationCount = 0;
	m_StuckCount = 0;
	m_SearchLevel = 0;
	fitSpeaker = 0;
	fitOrder = 0;
	m_CosMax = 1.f;
	m_CosMin = -1.f;
	m_SinMax = 1.f;
	m_SinMin = -1.f;


	ZeroSpeakerWeights(direction);
	ZeroSpeakerWeights(localDown);
	ZeroSpeakerWeights(localUp);

	CalculateFitnessCoefficients();
	bestFitness = GetDebugFitness();

	calculatenonstop = false;

}

void audAmbisonics::GenerateOptimizedDecoder()
{
	InitSearchParams();
	m_IterationCount = 1;
	calculatenonstop = true;
}

#endif
void audAmbisonics::SetDeltaLimits(int speaker, int order)
{
	switch(order)
	{
	case 0:
		{
			switch(speaker)
			{
			case AMBISONICS_SPEAKER_L_FRONT :
				m_CosMax = 1.f; m_CosMin = 0.f; m_SinMax = 1.f; m_SinMin = 0.f;
				break;
			case AMBISONICS_SPEAKER_R_FRONT :
				break;
				//CMax = 1.f, CMin = 0.f, SMax = 0.f, SMin = -1.f;
				//break;
			case AMBISONICS_SPEAKER_CENTER :
				m_CosMax = 1.f; m_CosMin = 0.f; m_SinMax = 0.f; m_SinMin = 0.f;
				break;
			case AMBISONICS_SPEAKER_L_BACK :
				m_CosMax = 0.f; m_CosMin = -1.f; m_SinMax = 1.f; m_SinMin = 0.f;
				break;
			case AMBISONICS_SPEAKER_R_BACK :
				break;
				//CMax = 0.f, CMin = -1.f, SMax = 0.f, SMin = -1.f;
				//break;
			}
		}
		break;
	case 4:
	case 3:
	case 2:
	case 1:
		{
			switch(speaker)
			{
			case AMBISONICS_SPEAKER_L_FRONT:
				m_CosMax = 0.5f; m_CosMin = -0.5f; m_SinMax = 0.5f; m_SinMin = -0.5f;
				break;
			case AMBISONICS_SPEAKER_R_FRONT:
				break;
			case AMBISONICS_SPEAKER_CENTER:
				m_CosMax = 0.5f; m_CosMin = -0.5f; m_SinMax = 0.f; m_SinMin = 0.f;
				break;
			case AMBISONICS_SPEAKER_L_BACK:
				m_CosMax = 0.5f; m_CosMin = -0.5f; m_SinMax = 0.5f; m_SinMin = -0.5f;
				break;
			case AMBISONICS_SPEAKER_R_BACK:
				break;
			}
		}
		break;
	}
}

#if !__SPU
void audAmbisonics::Tabu()
{
	PF_FUNC(OptimizeDecoder);
	bestLocalFit = 9999999999999.f;
	fitDir = 0.f;

	ZeroSpeakerWeights(localBest);

	for(int i =0; i<AMBISONICS_SPEAKER_MAX; i++)
	{
		for(int j=0; j<2; j++) //Test up, test down
		{
			TabuCore(i, j);
		}
	}

	if(fitDir==1.f) localDown[fitSpeaker].Co[fitOrder]=m_TabuSteps;
	if(fitDir==-1.f) localUp[fitSpeaker].Co[fitOrder]=m_TabuSteps;
	if(fitDir==2.f) localDown[fitSpeaker].Si[fitOrder]=m_TabuSteps;
	if(fitDir==-2.f) localUp[fitSpeaker].Si[fitOrder]=m_TabuSteps;
	if(fitDir==4.f) localDown[fitSpeaker].w=m_TabuSteps;
	if(fitDir==-4.f) localUp[fitSpeaker].w=m_TabuSteps;

	LocalCopySpeakerWeights(decodeData.decoder, localBest);
	if(bestLocalFit<bestFitness)
	{
		bestFitness=bestLocalFit;
		LocalCopySpeakerWeights(tabuWeights, localBest);
		m_StuckCount = 0;
	}
}

void audAmbisonics::TabuSpu()
{
	PF_FUNC(OptimizeDecoder);
	bestLocalFit = 9999999999999.f;
	fitDir = 0.f;

	ZeroSpeakerWeights(localBest);

	sysTaskParameters /*p1,*/ p2, p3, p4, p5, p6;
	sysTaskHandle /*tabuSpuHandle1 = NULL,*/ tabuSpuHandle2 = NULL, tabuSpuHandle3 = NULL, tabuSpuHandle4 = NULL, tabuSpuHandle5 = NULL, tabuSpuHandle6 = NULL;

	//Currently this element is calculated on the main thread
	//ResetSpuOutputs();
	//p1.Input.Data = this;
	//p1.Input.Size = sizeof(audAmbisonics);
	//p1.Output.Data = &spuOutput1;
	//p1.Output.Size = sizeof(TabuSpuOutput);
	//p1.UserData[0].asInt = SPEAKER_L_FRONT;
	//p1.UserData[1].asInt = 0;
	//p1.UserData[2].asInt = sizeof(audAmbisonics);
	//p1.UserDataCount = 3;

	p4.Input.Data = this;
	p4.Input.Size = sizeof(audAmbisonics);
	p4.Output.Data = &spuOutput4;
	p4.Output.Size = sizeof(TabuSpuOutput);
	p4.UserData[0].asInt = AMBISONICS_SPEAKER_L_FRONT;
	p4.UserData[1].asInt = 1;
	p4.UserData[2].asInt = sizeof(audAmbisonics);
	p4.UserDataCount = 3;

	p2.Input.Data = this;
	p2.Input.Size = sizeof(audAmbisonics);
	p2.Output.Data = &spuOutput2;
	p2.Output.Size = sizeof(TabuSpuOutput);
	p2.UserData[0].asInt = AMBISONICS_SPEAKER_CENTER;
	p2.UserData[1].asInt = 0;
	p2.UserData[2].asInt = sizeof(audAmbisonics);
	p2.UserDataCount = 3;

	p5.Input.Data = this;
	p5.Input.Size = sizeof(audAmbisonics);
	p5.Output.Data = &spuOutput5;
	p5.Output.Size = sizeof(TabuSpuOutput);
	p5.UserData[0].asInt = AMBISONICS_SPEAKER_CENTER;
	p5.UserData[1].asInt = 1;
	p5.UserData[2].asInt = sizeof(audAmbisonics);
	p5.UserDataCount = 3;

	p3.Input.Data = this;
	p3.Input.Size = sizeof(audAmbisonics);
	p3.Output.Data = &spuOutput3;
	p3.Output.Size = sizeof(TabuSpuOutput);
	p3.UserData[0].asInt = AMBISONICS_SPEAKER_L_BACK;
	p3.UserData[1].asInt = 0;
	p3.UserData[2].asInt = sizeof(audAmbisonics);
	p3.UserDataCount = 3;

	p6.Input.Data = this;
	p6.Input.Size = sizeof(audAmbisonics);
	p6.Output.Data = &spuOutput6;
	p6.Output.Size = sizeof(TabuSpuOutput);
	p6.UserData[0].asInt = AMBISONICS_SPEAKER_L_BACK;
	p6.UserData[1].asInt = 1;
	p6.UserData[2].asInt = sizeof(audAmbisonics);
	p6.UserDataCount = 3;

	//tabuSpuHandle1 = sysTaskManager::Create(TASK_INTERFACE(AmbisonicOptimizerJob),p1);
	tabuSpuHandle2 = sysTaskManager::Create(TASK_INTERFACE(AmbisonicOptimizerJob),p2);
	tabuSpuHandle3 = sysTaskManager::Create(TASK_INTERFACE(AmbisonicOptimizerJob),p3);
	tabuSpuHandle4 = sysTaskManager::Create(TASK_INTERFACE(AmbisonicOptimizerJob),p4);
	tabuSpuHandle5 = sysTaskManager::Create(TASK_INTERFACE(AmbisonicOptimizerJob),p5);
	tabuSpuHandle6 = sysTaskManager::Create(TASK_INTERFACE(AmbisonicOptimizerJob),p6);

	TabuCore(AMBISONICS_SPEAKER_L_FRONT, 0);
	SetJobOutput(&spuOutput1, this);

	//sysTaskManager::Wait(tabuSpuHandle1);
	sysTaskManager::Wait(tabuSpuHandle2);
	sysTaskManager::Wait(tabuSpuHandle3);
	sysTaskManager::Wait(tabuSpuHandle4);
	sysTaskManager::Wait(tabuSpuHandle5);
	sysTaskManager::Wait(tabuSpuHandle6);


	TabuSpuOutput *bestOutput = &spuOutput1;

	bestOutput = (bestOutput->bestLocalFit < spuOutput2.bestLocalFit) ? bestOutput : &spuOutput2;
	bestOutput = (bestOutput->bestLocalFit < spuOutput3.bestLocalFit) ? bestOutput : &spuOutput3;
	bestOutput = (bestOutput->bestLocalFit < spuOutput4.bestLocalFit) ? bestOutput : &spuOutput4;
	bestOutput = (bestOutput->bestLocalFit < spuOutput5.bestLocalFit) ? bestOutput : &spuOutput5;
	bestOutput = (bestOutput->bestLocalFit < spuOutput6.bestLocalFit) ? bestOutput : &spuOutput6;

	ApplyJobOutput(bestOutput);


	if(fitDir==1.f) localDown[fitSpeaker].Co[fitOrder]=m_TabuSteps;
	if(fitDir==-1.f) localUp[fitSpeaker].Co[fitOrder]=m_TabuSteps;
	if(fitDir==2.f) localDown[fitSpeaker].Si[fitOrder]=m_TabuSteps;
	if(fitDir==-2.f) localUp[fitSpeaker].Si[fitOrder]=m_TabuSteps;
	if(fitDir==4.f) localDown[fitSpeaker].w=m_TabuSteps;
	if(fitDir==-4.f) localUp[fitSpeaker].w=m_TabuSteps;

	LocalCopySpeakerWeights(decodeData.decoder, localBest);
	if(bestLocalFit<bestFitness)
	{
		bestFitness=bestLocalFit;
		LocalCopySpeakerWeights(tabuWeights, localBest);
		m_StuckCount = 0;
	}
}

void audAmbisonics::ApplyJobOutput(TabuSpuOutput *output)
{
	LocalCopySpeakerWeights(localBest, output->localBest);
	LocalCopySpeakerWeights(localUp, output->localUp);
	LocalCopySpeakerWeights(localDown, output->localDown);
	LocalCopySpeakerWeights(direction, output->direction);
	fitDir = output->fitDir;
	fitOrder = output->fitOrder;
	fitSpeaker = output->fitSpeaker;
	bestLocalFit = output->bestLocalFit;
}

#endif //!__SPU

void audAmbisonics::SetJobOutput(TabuSpuOutput *output)
{
	LocalCopySpeakerWeights(output->localBest, localBest);
	LocalCopySpeakerWeights(output->localUp, localUp);
	LocalCopySpeakerWeights(output->localDown, localDown);
	LocalCopySpeakerWeights(output->direction, direction);
	output->fitDir = fitDir;
	output->fitOrder = fitOrder;
	output->fitSpeaker = fitSpeaker;
	output->bestLocalFit = bestLocalFit;
}

void audAmbisonics::SetJobOutput(TabuSpuOutput *output, audAmbisonics *inAmb)
{
	LocalCopySpeakerWeights(output->localBest, inAmb->localBest);
	LocalCopySpeakerWeights(output->localUp, inAmb->localUp);
	LocalCopySpeakerWeights(output->localDown, inAmb->localDown);
	LocalCopySpeakerWeights(output->direction, inAmb->direction);
	output->fitDir = inAmb->fitDir;
	output->fitOrder = inAmb->fitOrder;
	output->fitSpeaker = inAmb->fitSpeaker;
	output->bestLocalFit = inAmb->bestLocalFit;
}

void audAmbisonics::TabuCore(int speaker, int upDown) //j controls up/down
{
	f32 currFit = 9999999999999.f;
	if(speakers[speaker].isActive)
	{
		switch(speaker) //This is an optimisation for systems which are left/right symmetrical; if we want to handle fully irregular systems this will need a tweak
		{
		case AMBISONICS_SPEAKER_R_FRONT:
		case AMBISONICS_SPEAKER_R_BACK:
			return;
		}

		for(u32 a=0; a<m_AmbOrder; a++)
		{

			SetDeltaLimits(speaker, a);

			//Do Cos
			{
				if(!localUp[speaker].Co[a] && upDown==0)
				{
					if(decodeData.decoder[speaker].Co[a]>=m_CosMax)
					{
						decodeData.decoder[speaker].Co[a]=m_CosMax;
						localUp[speaker].Co[a]=m_TabuSteps;
						localDown[speaker].Co[a]=0.f;
						direction[speaker].Co[a]=0.f;
					}
					else
					{
						decodeData.decoder[speaker].Co[a]+=m_Delta;
						direction[speaker].Co[a]=1.f;
					}
				}
				else if(upDown==0)
				{
					decodeData.decoder[speaker].Co[a]+=m_Delta;
					MirrorSpeakerWeights();
					CalculateFitnessCoefficients();
					if(GetFitness() < bestFitness)
					{
						direction[speaker].Co[a]=1.f;
					}
					else
					{
						direction[speaker].Co[a]=0.f;
						decodeData.decoder[speaker].Co[a]-=m_Delta;
					}
				}

				if(!localDown[speaker].Co[a] && upDown==1)
				{
					if(decodeData.decoder[speaker].Co[a]<=m_CosMin)
					{
						decodeData.decoder[speaker].Co[a]=m_CosMin;
						localDown[speaker].Co[a]=m_TabuSteps;
						localUp[speaker].Co[a]=0.f;
						direction[speaker].Co[a]=0.f;
					}
					else
					{
						decodeData.decoder[speaker].Co[a]-=m_Delta;
						direction[speaker].Co[a]=-1.f;
					}
				}
				else if(upDown==1)
				{
					decodeData.decoder[speaker].Co[a]-=m_Delta;
					MirrorSpeakerWeights();
					CalculateFitnessCoefficients();
					if(GetFitness() < bestFitness)
					{
						direction[speaker].Co[a]=-1.f;
					}
					else
					{
						direction[speaker].Co[a]=0.f;
						decodeData.decoder[speaker].Co[a]+=m_Delta;
					}
				}

				if(localUp[speaker].Co[a] && localDown[speaker].Co[a])
				{
					direction[speaker].Co[a]=0.f;
				}

				if(direction[speaker].Co[a])
				{
					MirrorSpeakerWeights();
					CalculateFitnessCoefficients();
					currFit = GetFitness();
				}
				else
				{
					currFit = 999999999999.f;
				}

				if(currFit<bestLocalFit)
				{
					fitDir = direction[speaker].Co[a];
					fitSpeaker=speaker;
					fitOrder=a;
					LocalCopySpeakerWeights(localBest, decodeData.decoder);
					bestLocalFit = currFit;
				}
				decodeData.decoder[speaker].Co[a]-=m_Delta*(direction[speaker].Co[a]);
			}

			if(localDown[speaker].Co[a]) localDown[speaker].Co[a]--;
			if(localUp[speaker].Co[a]) localUp[speaker].Co[a]--;

			//Do Sin
			for(int sinUpDown=0; sinUpDown<2; sinUpDown++) //Test up, test down
			{
				if(!localUp[speaker].Si[a] && sinUpDown==0)
				{
					if(decodeData.decoder[speaker].Si[a]>=m_SinMax)
					{
						decodeData.decoder[speaker].Si[a]=m_SinMax;
						localUp[speaker].Si[a]=m_TabuSteps;
						localDown[speaker].Si[a]=0.f;
						direction[speaker].Si[a]=0.f;
					}
					else
					{
						decodeData.decoder[speaker].Si[a]+=m_Delta;
						direction[speaker].Si[a]=2.f;
					}
				}
				else if(sinUpDown==0)
				{
					decodeData.decoder[speaker].Si[a]+=m_Delta;
					MirrorSpeakerWeights();
					CalculateFitnessCoefficients();
					if(GetFitness() < bestFitness)
					{
						direction[speaker].Si[a]=2.f;
					}
					else
					{
						direction[speaker].Si[a]=0.f;
						decodeData.decoder[speaker].Si[a]-=m_Delta;
					}
				}

				if(!localDown[speaker].Si[a] && sinUpDown==1)
				{
					if(decodeData.decoder[speaker].Si[a]<=m_SinMin)
					{
						decodeData.decoder[speaker].Si[a]=m_SinMin;
						localDown[speaker].Si[a]=m_TabuSteps;
						localUp[speaker].Si[a]=0.f;
						direction[speaker].Si[a]=0.f;
					}
					else
					{
						decodeData.decoder[speaker].Si[a]-=m_Delta;
						direction[speaker].Si[a]=-2.f;
					}
				}
				else if(sinUpDown==1)
				{
					decodeData.decoder[speaker].Si[a]-=m_Delta;
					MirrorSpeakerWeights();
					CalculateFitnessCoefficients();
					if(GetFitness() < bestFitness)
					{
						direction[speaker].Si[a]=-2.f;
					}
					else
					{
						direction[speaker].Si[a]=0.f;
						decodeData.decoder[speaker].Si[a]+=m_Delta;
					}
				}

				if(localUp[speaker].Si[a]&&localDown[speaker].Si[a])
				{
					direction[speaker].Si[a]=0.f;
				}

				if(direction[speaker].Si[a])
				{
					MirrorSpeakerWeights();
					CalculateFitnessCoefficients();
					currFit = GetFitness();
				}
				else
				{
					currFit = 999999999999.f;
				}

				if(currFit<bestLocalFit)
				{
					fitDir = direction[speaker].Si[a];
					fitSpeaker=speaker;
					fitOrder=a;
					LocalCopySpeakerWeights(localBest, decodeData.decoder);
					bestLocalFit = currFit;
				}
				decodeData.decoder[speaker].Si[a]-=m_Delta*(direction[speaker].Si[a]*0.5f);
			}
			

			if(localDown[speaker].Si[a]) localDown[speaker].Si[a]--;
			if(localUp[speaker].Si[a]) localUp[speaker].Si[a]--;
		} 

		//Do W
		for(int wUpDown=0; wUpDown<2; wUpDown++) //Test up, test down
		{
			if(!localUp[speaker].w && wUpDown==0)
			{
				if(decodeData.decoder[speaker].w>=1.f)
				{
					decodeData.decoder[speaker].w=1.f;
					localUp[speaker].w=m_TabuSteps;
					localDown[speaker].w=0.f;
					direction[speaker].w=0.f;
				}
				else
				{
					decodeData.decoder[speaker].w+=m_Delta;
					direction[speaker].w=4.f;
				}
			}
			else if(wUpDown==0)
			{
				decodeData.decoder[speaker].w+=m_Delta;
				MirrorSpeakerWeights();
				CalculateFitnessCoefficients();
				if(GetFitness() < bestFitness)
				{
					direction[speaker].w=4.f;
				}
				else
				{
					direction[speaker].w=0.f;
					decodeData.decoder[speaker].w-=m_Delta;
				}
			}

			if(!localDown[speaker].w && wUpDown==1)
			{
				if(decodeData.decoder[speaker].w<=0.f)
				{
					decodeData.decoder[speaker].w=0.f;
					localDown[speaker].w=m_TabuSteps;
					localUp[speaker].w=0.f;
					direction[speaker].w=0.f;
				}
				else
				{
					decodeData.decoder[speaker].w-=m_Delta;
					direction[speaker].w=-4.f;
				}
			}
			else if(wUpDown==1)
			{
				decodeData.decoder[speaker].w-=m_Delta;
				MirrorSpeakerWeights();
				CalculateFitnessCoefficients();
				if(GetFitness() < bestFitness)
				{
					direction[speaker].w=-4.f;
				}
				else
				{
					direction[speaker].w=0.f;
					decodeData.decoder[speaker].w+=m_Delta;
				}
			}

			if(localUp[speaker].w&&localDown[speaker].w)
			{
				direction[speaker].w=0.f;
			}

			if(direction[speaker].w)
			{
				CalculateFitnessCoefficients();
				currFit = GetFitness();
			}
			else
			{
				currFit = 999999999999.f;
			}

			if(currFit<bestLocalFit)
			{
				fitDir = direction[speaker].w;
				fitSpeaker=speaker;
				LocalCopySpeakerWeights(localBest, decodeData.decoder);
				bestLocalFit = currFit;
			}
			decodeData.decoder[speaker].w-=m_Delta*(direction[speaker].w*0.25f);
		}
		
		if(localDown[speaker].w) localDown[speaker].w--;
		if(localUp[speaker].w) localUp[speaker].w--;
	}
}

#if !__SPU
void audAmbisonics::OptimizeDecoder()
{
	++m_StuckCount;
	++m_TotalCount;
	if(m_UseSpuOptimize)
	{
		TabuSpu();
	}
	else
	{
		Tabu();
	}
	if(!(m_TotalCount%512))
	{
		audDisplayf("Fitness is %f, stuck count is %d, total count is %d", bestFitness, m_StuckCount, m_TotalCount);
	}
	UpdateSearchParameters();
}

void audAmbisonics::UpdateSearchParameters()
{
	switch(m_SearchLevel)
	{
	case 0 :
		{
			if(bestFitness < m_LowBand)
			{
				if(m_StuckCount > 3000)
				{
					m_Delta = 0.001f; //In the final stage of the search, start to fine tune
					m_StuckCount = 0;
					m_SearchLevel = 1;
					LocalCopySpeakerWeights(decodeData.decoder, tabuWeights);
				}
			}
			else if(bestFitness < m_UpperBand && m_StuckCount > 2000)
			{
				RandomizeSpeakerWeights();
				CalculateFitnessCoefficients();
				bestFitness = GetFitness();
				GenerateOptimizedDecoder();
			}
			else if(bestFitness >= m_UpperBand && m_StuckCount > 1000)
			{
				RandomizeSpeakerWeights();
				CalculateFitnessCoefficients();
				bestFitness = GetFitness();
				GenerateOptimizedDecoder();
			}
			break;
		}
	case 1 :
		{
			if(m_DoFineTuning && m_StuckCount < 2000)
			{
				break;
			}
			calculatenonstop = false;
			m_StuckCount = 0;
			if(m_AmbOrder < m_TargetOrder)
			{
				++m_AmbOrder;
				audDisplayf("Am order is now %d", m_AmbOrder);
				LocalCopySpeakerWeights(decodeData.decoder, tabuWeights);
				GenerateOptimizedDecoder();
			}
			else
			{
				m_AmbOrder = 1;
				LocalCopySpeakerWeights(decodeData.decoder, tabuWeights);
				ResetSpuOutputs();
				u32 seconds = (g_AudioEngine.GetTimeInMilliseconds() - m_StartTime)/1000;
				u32 mins = seconds/60;
				seconds -= mins*60;
				audDisplayf("********* Final fitness is %f **********", bestFitness);
				audDisplayf("********* Search time is %d mins %d seconds", mins, seconds);
			}
		}
		break;
	}
}

void audAmbisonics::SetSearchMetrics()
{
	m_StartTime = g_AudioEngine.GetTimeInMilliseconds();
	m_TotalCount = 0;
	bestFitness = 99999999999999.f;

}

#endif //!__SPU

void audAmbisonics::MirrorSpeakerWeights()
{
	for(u32 i=0; i<AMB_ORDER; i++)
	{
		decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[i] = decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[i];
		decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[i] = -decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[i];


		decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Co[i] = decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Co[i];
		decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Si[i] = -decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Si[i];

		//Add in the extended speakers if we want to support 7.1
	}
}

#if !__SPU
void audAmbisonics::SetupBFormatCoefficients(bFormatCoefficients outBF[AMBISONICS_SPEAKER_MAX], f32 inCo[AMBISONICS_SPEAKER_MAX][AMB_ORDER*2+1])
{
	for(int i = 0; i<AMBISONICS_SPEAKER_MAX;i++)
	{
		int num = 0;
		for(int j=0; j<(AMB_ORDER*2); j+=2)
		{
			outBF[i].Co[num] = inCo[i][j];
			outBF[i].Si[num] = inCo[i][j+1];
			++num;
		}
		outBF[i].w = inCo[i][AMB_ORDER*2];
	}
}

void audAmbisonics::ZeroSpeakerWeights(bFormatCoefficients inCo[AMBISONICS_SPEAKER_MAX])
{
	sysMemSet(inCo, 0, sizeof(bFormatCoefficients)*AMBISONICS_SPEAKER_MAX);
}

void audAmbisonics::RandomizeSpeakerWeights(bFormatCoefficients inCo[AMBISONICS_SPEAKER_MAX])
{
	ZeroSpeakerWeights(inCo);
	for(u32 i = 0; i<AMBISONICS_SPEAKER_MAX; i++)
	{
		if(speakers[i].isActive)
		{
			//The search now begins at first order and adds on additional orders as it progresses
			//so we only want random first order values and zero the other orders; since the high orders
			//are being used to pollute the first order values we can expect them to not stray too far
			//either side from zero
			//for(u32 j = 0; j < 1 /*m_AmbOrder*/; j++)
			u32 j = 0;
			{
				inCo[i].Co[j] = audEngineUtil::GetRandomNumberInRange(-1.f, 1.f);
				inCo[i].Si[j] = audEngineUtil::GetRandomNumberInRange(-1.f, 1.f);
				if(j==0)
				{
					switch(i)
					{
					case AMBISONICS_SPEAKER_L_FRONT:
						inCo[i].Co[j] = audEngineUtil::GetRandomNumberInRange(0.f, 1.f);
						inCo[i].Si[j] = audEngineUtil::GetRandomNumberInRange(0.f, 1.f);
						break;
					case AMBISONICS_SPEAKER_R_FRONT:
						inCo[i].Co[j] = audEngineUtil::GetRandomNumberInRange(0.f, 1.f);
						inCo[i].Si[j] = audEngineUtil::GetRandomNumberInRange(-1.f, 0.f);
						break;
					case AMBISONICS_SPEAKER_L_BACK:
						inCo[i].Co[j] = audEngineUtil::GetRandomNumberInRange(-1.f, 0.f);
						inCo[i].Si[j] = audEngineUtil::GetRandomNumberInRange(0.f, 1.f);
						break;
					case AMBISONICS_SPEAKER_R_BACK:
						inCo[i].Co[j] = audEngineUtil::GetRandomNumberInRange(-1.f, 0.f);
						inCo[i].Si[j] = audEngineUtil::GetRandomNumberInRange(-1.f, 0.f);
						break;
					}
					
					inCo[AMBISONICS_SPEAKER_CENTER].Si[j] = 0.f;
				}
			}
			inCo[i].w = audEngineUtil::GetRandomNumberInRange(0.f, 1.f);
		}
		/*else
		{
			for(u32 j = 0; j < AMB_ORDER; j++)
			{
				inCo[i].Co[j] = 0.f;
				inCo[i].Si[j] = 0.f;
				if(j==0)
				{
					switch(i)
					{
					case SPEAKER_L_FRONT:
						inCo[i].Co[j] = 0.f;
						inCo[i].Si[j] = 0.f;
						break;
					case SPEAKER_R_FRONT:
						inCo[i].Co[j] = 0.f;
						inCo[i].Si[j] = 0.f;
						break;
					case SPEAKER_L_BACK:
						inCo[i].Co[j] = 0.f;
						inCo[i].Si[j] = 0.f;
						break;
					case SPEAKER_R_BACK:
						inCo[i].Co[j] = 0.f;
						inCo[i].Si[j] = 0.f;
						break;
					}
				}
			}
			inCo[i].w = 0.f;
		}*/
	}
}

void audAmbisonics::RandomizeSpeakerWeights()
{
	RandomizeSpeakerWeights(decodeData.decoder);
}

#endif //!__SPU

//static
void audAmbisonics::CopySpeakerWeights(bFormatCoefficients toCoefficients[AMBISONICS_SPEAKER_MAX], const bFormatCoefficients fromCoefficients[AMBISONICS_SPEAKER_MAX])
{
	sysMemCpy(toCoefficients, fromCoefficients, sizeof(bFormatCoefficients)*AMBISONICS_SPEAKER_MAX);
}

//not static
void audAmbisonics::LocalCopySpeakerWeights(bFormatCoefficients toCoefficients[AMBISONICS_SPEAKER_MAX], const bFormatCoefficients fromCoefficients[AMBISONICS_SPEAKER_MAX])
{
	sysMemCpy(toCoefficients, fromCoefficients, sizeof(bFormatCoefficients)*AMBISONICS_SPEAKER_MAX);
}

f32 audAmbisonics::GetFitness()
{
	return m_PFit*fitnessWeights[FITNESS_WEIGHT_V] + m_EFit*fitnessWeights[FITNESS_WEIGHT_E] + m_MvFit*fitnessWeights[FITNESS_WEIGHT_MV] +
		m_MeFit*fitnessWeights[FITNESS_WEIGHT_ME] + m_AvFit*fitnessWeights[FITNESS_WEIGHT_AV] + m_AeFit*fitnessWeights[FITNESS_WEIGHT_AE];
}

#if !__SPU
f32 audAmbisonics::GetDebugFitness()
{
	/*audDisplayf("PFit: %f, weight: %f", m_PFit, fitnessWeights[FITNESS_WEIGHT_V]);
	audDisplayf("Efit: %f, weight: %f", m_EFit, fitnessWeights[FITNESS_WEIGHT_E]);
	audDisplayf("MvFit: %f, weight: %f", m_MvFit,fitnessWeights[FITNESS_WEIGHT_MV]);
	audDisplayf("MeFit: %f, weight: %f", m_MeFit, fitnessWeights[FITNESS_WEIGHT_ME]);
	audDisplayf("AvFit: %f, weight: %f", m_AvFit, fitnessWeights[FITNESS_WEIGHT_AV]);
	audDisplayf("AeFit: %f, weight: %f", m_AeFit, fitnessWeights[FITNESS_WEIGHT_AE]);*/
	return m_PFit*fitnessWeights[FITNESS_WEIGHT_V] + m_EFit*fitnessWeights[FITNESS_WEIGHT_E] + m_MvFit*fitnessWeights[FITNESS_WEIGHT_MV] +
		m_MeFit*fitnessWeights[FITNESS_WEIGHT_ME] + m_AvFit*fitnessWeights[FITNESS_WEIGHT_AV] + m_AeFit*fitnessWeights[FITNESS_WEIGHT_AE];
}
#endif //!__SPU

void audAmbisonics::CalculateFitnessCoefficients(bool draw)
{
#if __SPU
	draw=false;
#endif
	
	///Initialize angle at 0 degrees
	f32 angleCos = 1.f;
	f32 angleSin = 0.f;
	f32 angle = 0.f;

	f32 numAnglesRep = 0.011111111111f; // 1/(360/4)

	m_PFit = m_EFit = m_MvFit = m_MeFit = m_AvFit = m_AeFit = 0.f;

	f32 pressure0 = 0.f, energy0 = 0.f; //Pressure and Energy due to speaker output at 0 degrees

	for(int i = 0; i<360; i+=4)
	{
		LocalCalculateBFormatEncode(decodeData, angleCos, angleSin);
		LocalCalculateSpeakerGains(decodeData);

		f32 pressure=0.f, energy=0.f; //Pressure and Energy due to speaker output
		f32 Vx=0.f, Vy=0.f; //Velocity vector
		f32 Ex=0.f, Ey=0.f; //Energy vector
		f32 Me=0.f, Mv=0.f; //Magnitude of energy and velocity vectors 
		f32 Ae=0.f, Av=0.f; //Reproduced angle

		f32 temp_x = 0.f, temp_y = 0.f;

		//Calculate energy, pressure and the associated vectors
		for(int j=0; j<AMBISONICS_SPEAKER_MAX; j++)
		{
			if(speakers[j].isActive)
			{
				energy+=decodeData.speakerGains[j]*decodeData.speakerGains[j];
				pressure+=decodeData.speakerGains[j];
				temp_x = decodeData.speakerGains[j]*speakers[j].angleCos;
				temp_y = decodeData.speakerGains[j]*speakers[j].angleSin;
				Vx += temp_x;
				Vy += temp_y;
				Ex += temp_x*decodeData.speakerGains[j];
				Ey += temp_y*decodeData.speakerGains[j];
			}
		}

		f32 repE =  energy != 0.f ? 1/energy : 0.f, repP = pressure != 0.f ? 1/pressure : 0.f;

		Vx = Vx*repP;
		Vy = Vy*repP;
		Ex = Ex*repE;
		Ey = Ey*repE;

		if(i==0)
		{
			pressure0 = pressure;
			energy0 = energy;
		}

		//Calculate energy and velocity magnitude and reproduced angle
		Me = Sqrtf(Ex*Ex + Ey*Ey);
		Mv = Sqrtf(Vx*Vx + Vy*Vy);
		Ae = angle - Atan2f(Ey,Ex);
		Av = angle - Atan2f(Vy,Vx);

		//Ensure angles are within limits
		if(Ae < -PI)
			Ae+=2*PI;
		if(Ae > PI)
			Ae-=2*PI;
		if(Av < -PI)
			Av+=2*PI;
		if(Av > PI)
			Av-=2*PI;

		//Update fitness variables
		m_PFit += (1 - pressure0/pressure)*(1 - pressure0/pressure);
		//m_PFit += (1 - pressure)*(1 - pressure);
		m_EFit += (1 - energy0/energy)*(1 - energy0/energy);
		//m_EFit += (1 - energy)*(1 - energy);
		m_MvFit += (1 - Mv)*(1 - Mv);
		m_MeFit += (1 - Me)*(1 - Me);
		m_AvFit += Av*Av;
		m_AeFit += Ae*Ae;

		//Increment the angle cos & sin
		angle = DtoR*(f32)i;
		angleSin = Sinf(angle); 
		angleCos = Cosf(angle); 

#if !__SPU
		if(draw)
		{
			int index = i/4;
			drawData[index].incidentAng = angle;
			drawData[index].reproducedAngE = Atan2f(Ey,Ex);
			drawData[index].reproducedAngV = Atan2f(Vy,Vx);
			drawData[index].pressure = pressure;
			drawData[index].energy = energy;
			drawData[index].vecLengthE = Me;
			drawData[index].vecLengthV = Mv;
		}
#endif
	}
	m_PFit = sqrtf(m_PFit*numAnglesRep);
	m_EFit = sqrtf(m_EFit*numAnglesRep);
	m_MvFit = sqrtf(m_MvFit*numAnglesRep)*0.8f;
	m_MeFit = sqrtf(m_MeFit*numAnglesRep)*0.8f;
	m_AvFit = sqrtf(m_AvFit*numAnglesRep);
	m_AeFit = sqrtf(m_AeFit*numAnglesRep);
}


void audAmbisonics::CalculateSpeakerGains(ambisonicDecodeData &data)
{
	const float PI_over_2 = (PI / 2.0f);
	float fade1 = Sinf(data.proportionOut*data.elevationComponent*data.occlusionLeak * g_AmbisonicDebugLeak * PI_over_2);
	float fade2 = Sinf((1.f - data.proportionOut*data.elevationComponent*data.occlusionLeak*g_AmbisonicDebugLeak) * PI_over_2)*g_AmbisonicsLeakScaling;

	for( int i = 0; i<5; i++)
	{
		data.speakerGains[i] = ((1 + fade2)*data.decoder[i].w*data.bFormat.w + /*0th order*/
			fade1*( 
			data.decoder[i].Co[0]*data.bFormat.x+ data.decoder[i].Si[0]*data.bFormat.y +  /*1st order*/
			data.decoder[i].Co[1]*data.bFormat.u + data.decoder[i].Si[1]*data.bFormat.v + /*2nd order*/
			data.decoder[i].Co[2]*data.bFormat.p + data.decoder[i].Si[2]*data.bFormat.q + /*3rd order*/
			data.decoder[i].Co[3]*data.bFormat.c4 + data.decoder[i].Si[3]*data.bFormat.s4 + /*4th order*/
			data.decoder[i].Co[4]*data.bFormat.c5 + data.decoder[i].Si[4]*data.bFormat.s5)); /*5th order*/

		data.speakerGains[i]*= data.decoder[6].w*g_AmbisonicsDecoderScaling;
	}
}

void audAmbisonics::CalculateBFormatEncode(ambisonicDecodeData &data, f32 angleCos, f32 angleSin)
{
	const f32 cos_A = angleCos;
	const f32 sin_A = angleSin;
	const f32 cos_2A = cos_A*cos_A - sin_A*sin_A; //cos(A+B) = cosACosB - sanASinB
	const f32 sin_2A = cos_A*sin_A + sin_A*cos_A; //sin(A+B) = cosAsinB + sinAcosB
	const f32 cos_3A = cos_2A*cos_A - sin_2A*sin_A;
	const f32 sin_3A = cos_2A*sin_A + sin_2A*cos_A;
	const f32 cos_4A = cos_3A*cos_A - sin_3A*sin_A;
	const f32 sin_4A = cos_3A*sin_A + sin_3A*cos_A;
	const f32 cos_5A = cos_4A*cos_A - sin_4A*sin_A;
	const f32 sin_5A = cos_4A*sin_A + sin_4A*cos_A;

	data.bFormat.w = 0.707f;

	data.bFormat.x = cos_A;
	data.bFormat.y = sin_A;
	data.bFormat.u = cos_2A;
	data.bFormat.v = sin_2A;
	data.bFormat.p = cos_3A;
	data.bFormat.q = sin_3A;
	data.bFormat.c4 = cos_4A;
	data.bFormat.s4 = sin_4A;
	data.bFormat.c5 = cos_5A;
	data.bFormat.s5 = sin_5A;
}

void audAmbisonics::LocalCalculateSpeakerGains(ambisonicDecodeData &data)
{
	for( int i = 0; i<5; i++)
	{

		data.speakerGains[i] = /*0.5f**/((2-data.proportionOut*data.elevationComponent*data.occlusionLeak)*data.decoder[i].w*data.bFormat.w + /*0th order*/
			data.proportionOut*data.elevationComponent*data.occlusionLeak*( 
			data.decoder[i].Co[0]*data.bFormat.x+ data.decoder[i].Si[0]*data.bFormat.y +  /*1st order*/
			data.decoder[i].Co[1]*data.bFormat.u + data.decoder[i].Si[1]*data.bFormat.v + /*2nd order*/
			data.decoder[i].Co[2]*data.bFormat.p + data.decoder[i].Si[2]*data.bFormat.q + /*3rd order*/
			data.decoder[i].Co[3]*data.bFormat.c4 + data.decoder[i].Si[3]*data.bFormat.s4 + /*4th order*/
			data.decoder[i].Co[4]*data.bFormat.c5 + data.decoder[i].Si[4]*data.bFormat.s5)); /*5th order*/
	}
}

void audAmbisonics::LocalCalculateBFormatEncode(ambisonicDecodeData &data, f32 angleCos, f32 angleSin)
{
	f32 cos_A = angleCos;
	f32 sin_A = angleSin;
	f32 cos_2A = cos_A*cos_A - sin_A*sin_A; //cos(A+B) = cosACosB - sanASinB
	f32 sin_2A = cos_A*sin_A + sin_A*cos_A; //sin(A+B) = cosAsinB + sinAcosB
	f32 cos_3A = cos_2A*cos_A - sin_2A*sin_A;
	f32 sin_3A = cos_2A*sin_A + sin_2A*cos_A;
	f32 cos_4A = cos_3A*cos_A - sin_3A*sin_A;
	f32 sin_4A = cos_3A*sin_A + sin_3A*cos_A;
	f32 cos_5A = cos_4A*cos_A - sin_4A*sin_A;
	f32 sin_5A = cos_4A*sin_A + sin_4A*cos_A;

	data.bFormat.x = cos_A;
	data.bFormat.y = sin_A;
	//bFormat.z = positionOnUnitSphere.y*m_ElevationFactor; //Not currently using periphonics
	data.bFormat.u = cos_2A;
	data.bFormat.v = sin_2A;
	data.bFormat.p = cos_3A;
	data.bFormat.q = sin_3A;
	data.bFormat.c4 = cos_4A;
	data.bFormat.s4 = sin_4A;
	data.bFormat.c5 = cos_5A;
	data.bFormat.s5 = sin_5A;

	data.bFormat.w = 0.707f;
}


#if !__SPU
void audAmbisonics::SetupSpeakers(const f32 angles[AMBISONICS_SPEAKER_MAX])
{
	for(int i=0; i<AMBISONICS_SPEAKER_MAX; i++)
	{
		speakers[i].angle = angles[i];
		speakers[i].angleCos = Cosf(DtoR*speakers[i].angle);
		speakers[i].angleSin = Sinf(DtoR*speakers[i].angle);
	}
}

void audAmbisonics::SetupFitnessWeights(const f32 weights[FITNESS_WEIGHT_COUNT])
{
	for(int i=0;i<FITNESS_WEIGHT_COUNT; i++)
	{
		fitnessWeights[i] = weights[i];
	}
}

void audAmbisonics::Update()
{
	for(int i=0; i<32; i++)
	{
		if(m_IterationCount > 0)
		{
			if(!calculatenonstop)
			{
				m_IterationCount-=1;
			}
			OptimizeDecoder();
		}
	}
	if(m_DoDraw && drawFunc)
	{
		CalculateFitnessCoefficientsDrawHack(true);
		drawFunc(drawData);
	}
}

void audAmbisonics::CalculateFitnessCoefficientsDrawHack(bool draw)
{

	///Initialize angle at 0 degrees
	f32 angleCos = 1.f;
	f32 angleSin = 0.f;
	f32 angle = 0.f;

	f32 numAnglesRep = 0.011111111111f; // 1/(360/4)

	m_PFit = m_EFit = m_MvFit = m_MeFit = m_AvFit = m_AeFit = 0.f;

	f32 pressure0 = 0.f, energy0 = 0.f; //Pressure and Energy due to speaker output at 0 degrees

	for(int i = 0; i<360; i+=4)
	{
		ambisonicDecodeData drawDecode(decodeData);
		if(m_DoDraw && m_DrawBestDecoder)
		{
			CopySpeakerWeights(drawDecode.decoder, tabuWeights);
			LocalCalculateBFormatEncode(drawDecode, angleCos, angleSin);
			LocalCalculateSpeakerGains(drawDecode);
		}
		else
		{
			LocalCalculateBFormatEncode(decodeData, angleCos, angleSin);
			LocalCalculateSpeakerGains(decodeData);
		}

		f32 pressure=0.f, energy=0.f; //Pressure and Energy due to speaker output
		f32 Vx=0.f, Vy=0.f; //Velocity vector
		f32 Ex=0.f, Ey=0.f; //Energy vector
		f32 Me=0.f, Mv=0.f; //Magnitude of energy and velocity vectors 
		f32 Ae=0.f, Av=0.f; //Reproduced angle

		f32 temp_x = 0.f, temp_y = 0.f;

		//Calculate energy, pressure and the associated vectors
		for(int j=0; j<AMBISONICS_SPEAKER_MAX; j++)
		{
			if(m_DoDraw && m_DrawBestDecoder)
			{
				if(speakers[j].isActive)
				{
					energy+=drawDecode.speakerGains[j]*drawDecode.speakerGains[j];
					pressure+=drawDecode.speakerGains[j];
					temp_x = drawDecode.speakerGains[j]*speakers[j].angleCos;
					temp_y = drawDecode.speakerGains[j]*speakers[j].angleSin;
					Vx += temp_x;
					Vy += temp_y;
					Ex += temp_x*drawDecode.speakerGains[j];
					Ey += temp_y*drawDecode.speakerGains[j];
				}
			}else
			{
				if(speakers[j].isActive)
				{
					energy+=decodeData.speakerGains[j]*decodeData.speakerGains[j];
					pressure+=decodeData.speakerGains[j];
					temp_x = decodeData.speakerGains[j]*speakers[j].angleCos;
					temp_y = decodeData.speakerGains[j]*speakers[j].angleSin;
					Vx += temp_x;
					Vy += temp_y;
					Ex += temp_x*decodeData.speakerGains[j];
					Ey += temp_y*decodeData.speakerGains[j];
				}
			}
		}

		f32 repE = 1/energy, repP = 1/pressure;

		Vx = Vx*repP;
		Vy = Vy*repP;
		Ex = Ex*repE;
		Ey = Ey*repE;

		if(i==0)
		{
			pressure0 = pressure;
			energy0 = energy;
		}

		//Calculate energy and velocity magnitude and reproduced angle
		Me = Sqrtf(Ex*Ex + Ey*Ey);
		Mv = Sqrtf(Vx*Vx + Vy*Vy);
		Ae = angle - Atan2f(Ey,Ex);
		Av = angle - Atan2f(Vy,Vx);

		//Ensure angles are within limits
		if(Ae < -PI)
			Ae+=2*PI;
		if(Ae > PI)
			Ae-=2*PI;
		if(Av < -PI)
			Av+=2*PI;
		if(Av > PI)
			Av-=2*PI;

		//Update fitness variables
		m_PFit += (1 - pressure0/pressure)*(1 - pressure0/pressure);
		//m_PFit += (1 - pressure)*(1 - pressure);
		m_EFit += (1 - energy0/energy)*(1 - energy0/energy);
		//m_EFit += (1 - energy)*(1 - energy);
		m_MvFit += (1 - Mv)*(1 - Mv);
		m_MeFit += (1 - Me)*(1 - Me);
		m_AvFit += Av*Av;
		m_AeFit += Ae*Ae;

		angle = DtoR*(f32)i;
		angleSin = Sinf(angle); 
		angleCos = Cosf(angle); 

#if !__SPU
		if(draw)
		{
			int index = i/4;
			drawData[index].incidentAng = angle;
			drawData[index].reproducedAngE = Atan2f(Ey,Ex);
			drawData[index].reproducedAngV = Atan2f(Vy,Vx);
			drawData[index].pressure = pressure;
			drawData[index].energy = energy;
			drawData[index].vecLengthE = Me;
			drawData[index].vecLengthV = Mv;
		}
#endif
	}
	m_PFit = sqrtf(m_PFit*numAnglesRep);
	m_EFit = sqrtf(m_EFit*numAnglesRep);
	m_MvFit = sqrtf(m_MvFit*numAnglesRep)*0.8f;
	m_MeFit = sqrtf(m_MeFit*numAnglesRep)*0.8f;
	m_AvFit = sqrtf(m_AvFit*numAnglesRep);
	m_AeFit = sqrtf(m_AeFit*numAnglesRep);
}
//The Save and load functions are temporarily backwards pending the move of the profile save functionality to the engine.
//In the mean time actual saving will be done in the game and these functions will manage the data handling
void audAmbisonics::LoadDecoder(ambisonicDecodeData &data)
{
	LocalCopySpeakerWeights(decodeData.decoder, data.decoder);
	
	switch(decodeData.decoderType)
	{
	case SQUARE_DECODER:
			  DefaultSquare();
		break;
	case ITU_DECODER:
			  DefaultItu();
		break;
	case NO_DECODER:
		break;
	}
	SetEnvironmentDecoder();
}

void audAmbisonics::SaveDecoder(ambisonicDecodeData &data)
{
	LocalCopySpeakerWeights(data.decoder, decodeData.decoder);
}

void audAmbisonics::PrintDecoder()
{
	ambisonicDecodeData &data = decodeData;
	char buff[256] = "";
	char buff2[256] = "";

	strcat(buff, "{{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}, //FL", data.decoder[AMBISONICS_SPEAKER_L_FRONT].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';

	strcat(buff, "{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}, //FR", data.decoder[AMBISONICS_SPEAKER_R_FRONT].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';

	strcat(buff, "{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_CENTER].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_CENTER].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}, //CENTER", data.decoder[AMBISONICS_SPEAKER_CENTER].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';

	strcat(buff, "{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_L_BACK].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_L_BACK].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}, //BL", data.decoder[AMBISONICS_SPEAKER_L_BACK].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';

	strcat(buff, "{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_R_BACK].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_R_BACK].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}, //BR", data.decoder[AMBISONICS_SPEAKER_R_BACK].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';

	strcat(buff, "{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_L_EX].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_L_EX].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}, //XL", data.decoder[AMBISONICS_SPEAKER_L_EX].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';

	strcat(buff, "{");
	for(int j=0; j<AMB_ORDER; j++)
	{
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_R_EX].Co[j]);
		strcat(buff, buff2);
		sprintf(buff2, "%.3ff, ", data.decoder[AMBISONICS_SPEAKER_R_EX].Si[j]);
		strcat(buff, buff2);

	}
	sprintf(buff2, "%.3ff}}; //XR", data.decoder[AMBISONICS_SPEAKER_R_EX].w);
	strcat(buff, buff2);
	audDisplayf("%s", buff);
	buff[0] = '\0';
}


#if __BANK


void audAmbisonics::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Ambisonics");
	bank.AddToggle("Override Environment Decoder", &g_OverrideEnvironmentDecoder);
	bank.PushGroup("Speaker Setup");
	bank.AddToggle("Use Left Front", &speakers[AMBISONICS_SPEAKER_L_FRONT].isActive);
	bank.AddSlider("Left Front Angle", &speakers[AMBISONICS_SPEAKER_L_FRONT].angle, 0.f, 360.f, 2.f);

	bank.AddToggle("Use Right Front", &speakers[AMBISONICS_SPEAKER_R_FRONT].isActive);
	bank.AddSlider("Right Front Angle", &speakers[AMBISONICS_SPEAKER_R_FRONT].angle, 0.f, 360.f, 2.f);

	bank.AddToggle("Use Center", &speakers[AMBISONICS_SPEAKER_CENTER].isActive);
	bank.AddSlider("Center Angle", &speakers[AMBISONICS_SPEAKER_CENTER].angle, 0.f, 360.f, 2.f);

	bank.AddToggle("Use Left Surround", &speakers[AMBISONICS_SPEAKER_L_BACK].isActive);
	bank.AddSlider("Left Surround Angle", &speakers[AMBISONICS_SPEAKER_L_BACK].angle, 0.f, 360.f, 2.f);

	bank.AddToggle("Use Right Surround", &speakers[AMBISONICS_SPEAKER_R_BACK].isActive);
	bank.AddSlider("Right Surround Angle", &speakers[AMBISONICS_SPEAKER_R_BACK].angle, 0.f, 360.f, 2.f);

	bank.AddToggle("Use Left Back", &speakers[AMBISONICS_SPEAKER_L_EX].isActive);
	bank.AddSlider("Left Back Angle", &speakers[AMBISONICS_SPEAKER_L_EX].angle, 0.f, 360.f, 2.f);

	bank.AddToggle("Use Right Back", &speakers[AMBISONICS_SPEAKER_R_EX].isActive);
	bank.AddSlider("Right Back Angle", &speakers[AMBISONICS_SPEAKER_R_EX].angle, 0.f, 360.f, 2.f);

	bank.AddButton("Set to default square", datCallback(CFA1(DefaultSquareCallback), this));
	bank.AddButton("Set to default itu", datCallback(CFA1(DefaultItuCallback), this));
	bank.AddButton("Set to itu nc", datCallback(CFA1(NCItuCallback), this));
	bank.AddButton("Set to fwrr", datCallback(CFA1(FwrrCallback), this));
	bank.AddButton("Set to fwrm", datCallback(CFA1(FwrmCallback), this));

	bank.AddButton("Set Speakers", datCallback(CFA1(ApplySpeakerCallback), this));

	bank.PopGroup();

	bank.PushGroup("Fitness Weightings");
	bank.AddSlider("Velocity Volume", &fitnessWeights[FITNESS_WEIGHT_V], 0.f, 1.f, 0.01f);
	bank.AddSlider("Velocity Magnitude", &fitnessWeights[FITNESS_WEIGHT_MV], 0.f, 1.f, 0.01f);
	bank.AddSlider("Velocity Angle", &fitnessWeights[FITNESS_WEIGHT_AV], 0.f, 1.f, 0.01f);
	bank.AddSlider("Energy Volume", &fitnessWeights[FITNESS_WEIGHT_E], 0.f, 1.f, 0.01f);
	bank.AddSlider("Energy Magnitude", &fitnessWeights[FITNESS_WEIGHT_ME], 0.f, 1.f, 0.01f);
	bank.AddSlider("Energy Angle", &fitnessWeights[FITNESS_WEIGHT_AE], 0.f, 1.f, 0.01f);

	bank.AddButton("Max Me Mv 1", datCallback(CFA1(MaxMeMv1), this));
	bank.AddButton("Max Me Mv 2", datCallback(CFA1(MaxMeMv2), this));
	bank.AddButton("Max Ae Av", datCallback(CFA1(MaxAeAv), this));

	bank.PopGroup();

	bank.PushGroup("Decoders");
	bank.AddButton("Itu Decoder", datCallback(CFA1(MeMv2DecodeCallback), this));
	bank.AddButton("Itu NC1 Decoder", datCallback(CFA1(ituNc1DecodeCallback), this));
	bank.AddButton("Tabu Square", datCallback(CFA1(tabuSquareDecodeCallback), this));
	bank.AddButton("Energy Decoder", datCallback(CFA1(EnergyDecodeCallback), this));
	bank.AddButton("Headphone Decoder", datCallback(CFA1(HeadphoneDecodeCallback), this));
	bank.AddButton("Side-itu Decoder", datCallback(CFA1(SideDecodeCallback), this));
	bank.AddButton("Itu-15 front Decoder", datCallback(CFA1(itu15DecodeCallback), this));
	bank.AddButton("Temp1 Decoder", datCallback(CFA1(Temp1DecodeCallback), this));
	bank.AddButton("Temp2 Decoder", datCallback(CFA1(Temp2DecodeCallback), this));
	bank.AddButton("Temp3 Decoder", datCallback(CFA1(Temp3DecodeCallback), this));
	bank.AddButton("Set Temp1 Decoder", datCallback(CFA1(SetTemp1DecoderCallback), this));
	bank.AddButton("Set Temp2 Decoder", datCallback(CFA1(SetTemp2DecoderCallback), this));
	bank.AddButton("Set Temp3 Decoder", datCallback(CFA1(SetTemp3DecoderCallback), this));
	bank.AddButton("Set Environment Decoder", datCallback(CFA1(SetEnvironmentDecoderCallback), this));
	bank.PopGroup();

	bank.PushGroup("Core Functionality");
	bank.AddSlider("Decoder scaling", &g_AmbisonicsDecoderScaling, 0.f, 10.f, 0.1f);
	bank.AddSlider("Leakage scaling", &g_AmbisonicsLeakScaling, 0.f, 10.f, 0.1f);
	bank.AddSlider("Debug Leak factor", &g_AmbisonicDebugLeak, 0.f, 1.f, 0.1f);
	bank.AddButton("Randomize speaker weights", datCallback(CFA1(RandomizeSpeakerWeightsCallback), this));
	bank.AddButton("Generate Decoder", datCallback(CFA1(GenerateOptimizedDecoderCallback), this));
	bank.AddButton("Decoder Step", datCallback(CFA1(DecoderStepCallback), this));
	bank.AddSlider("Tabu steps", &m_TabuSteps, 1.f, 150.f, 1.f);
	bank.AddSlider("Ambisonic Order", &m_TargetOrder, 0, AMB_ORDER, 1);
	bank.AddSlider("Delta", &m_Delta, 0.f, 0.1f, 0.001f);
	bank.AddSlider("Upper band", &m_UpperBand, 0.f, 2.f, 0.01f);
	bank.AddSlider("Lower band", &m_LowBand, 0.f, 2.f, 0.01f);
	bank.AddToggle("Nonstop", &calculatenonstop);
	bank.AddToggle("Fine tuning", &m_DoFineTuning);
	bank.AddToggle("Use spu", &m_UseSpuOptimize);
	bank.AddToggle("Draw", &m_DoDraw);
	bank.AddToggle("Draw Best Decoder", &m_DrawBestDecoder);
	bank.AddButton("Print Decoder", datCallback(CFA1(PrintDecoderCallback), this));

	bank.PopGroup();

	bank.PushGroup("HOA coefficients");
	bank.AddSlider("Front Left x", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left y", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left u", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left v", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left p", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left q", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left c4", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left s4", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left c5", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left s5", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left w", &(decodeData.decoder[AMBISONICS_SPEAKER_L_FRONT].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Front Right x", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right y", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right u", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right v", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right p", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right q", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right c4", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right s4", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right c5", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right s5", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right w", &(decodeData.decoder[AMBISONICS_SPEAKER_R_FRONT].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Center x", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center y", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center u", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center v", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center p", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center q", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center c4", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center s4", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center c5", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center s5", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center w", &(decodeData.decoder[AMBISONICS_SPEAKER_CENTER].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Back Left x", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left y", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left u", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left v", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left p", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left q", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left c4", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left s4", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left c5", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left c5", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left w", &(decodeData.decoder[AMBISONICS_SPEAKER_L_BACK].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Back Right x", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right y", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right u", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right v", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right p", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right q", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right c4", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right s4", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right c5", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right c5", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right w", &(decodeData.decoder[AMBISONICS_SPEAKER_R_BACK].w), -1.f, 1.f, 0.01f);
	bank.PopGroup();

	bank.PushGroup("Optimized coefficients");
	bank.AddSlider("Front Left x", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left y", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left u", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left v", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left p", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left q", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left c4", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left s4", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left c5", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left s5", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Left w", &(tabuWeights[AMBISONICS_SPEAKER_L_FRONT].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Front Right x", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right y", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right u", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right v", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right p", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right q", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right c4", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right s4", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right c5", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right s5", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Front Right w", &(tabuWeights[AMBISONICS_SPEAKER_R_FRONT].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Center x", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center y", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center u", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center v", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center p", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center q", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center c4", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center s4", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center c5", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center s5", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Center w", &(tabuWeights[AMBISONICS_SPEAKER_CENTER].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Back Left x", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left y", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left u", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left v", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left p", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left q", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left c4", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left s4", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left c5", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left c5", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Left w", &(tabuWeights[AMBISONICS_SPEAKER_L_BACK].w), -1.f, 1.f, 0.01f);

	bank.AddSlider("Back Right x", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Co[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right y", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Si[0]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right u", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Co[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right v", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Si[1]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right p", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Co[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right q", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Si[2]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right c4", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Co[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right s4", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Si[3]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right c5", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Co[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right c5", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].Si[4]), -1.f, 1.f, 0.01f);
	bank.AddSlider("Back Right w", &(tabuWeights[AMBISONICS_SPEAKER_R_BACK].w), -1.f, 1.f, 0.01f);
	bank.PopGroup();
	bank.PopGroup();
}
# endif	

#endif //!__SPU
}// namespace rage

#endif // AUD_ENABLE_AMBISONICS
