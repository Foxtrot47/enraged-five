// 
// xmldata/xmltoken.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef XMLDATA_XMLTOKEN_H
#define XMLDATA_XMLTOKEN_H


#include "file/token.h"


#if __WIN32
#pragma warning (disable : 4514)	// necessary to go to warning level 4 (inlined functions removed)
#endif

namespace rage {

///////////////////////////////////////////////////////////////////////////
// the xmlAsciiTokenizerXml class is basically a tokenizer with a couple of extra
// methods to help out when parsing Ascii XML files.
//<FLAG Component>
class xmlAsciiTokenizerXml : public fiAsciiTokenizer
{
public:
	enum {MAX_CHECK_TOKEN_SIZE=128,MAX_ATTRIBUTE_NAME_LENGTH=128};

public:

	// PURPOSE: Initialize a new tokenizer. 
	// PARAMS:
	//		name - The name of the tokenizer
	//		s - The stream to read from or write to
	//		preload - If true, on reading the whole file will be read before tokenizing
	void Init( const char * name, fiStream *s, bool preload=true );

	// PURPOSE: Reads in an XML begin tag (<tag>)
	void GetTag(const char *tag, bool empty=false);

	// PURPOSE: Reads in an XML end tag (</tag>)
	void GetEndTag(const char *tag);

	// PURPOSE: Checks that <tag> is present. Returns true if it is.
	bool CheckTag(const char *tag, bool empty=false, bool consume=true);

	// PURPOSE: Checks that </tag> is present. Returns true if it is.
	bool CheckEndTag(const char *tag, bool consume=true);		

	// PURPOSE: Returns true if the next tag is <tag>
	bool CheckTagName(const char *tag);
	
	// PURPOSE: Gets a tag and reads in any attributes.
	// PARAMS:
	//		tag - The tag to read in
	//		attributeNames - a const array of attribute names terminated by an empty string
	//		maxlen - The maximum length for any one attribute value string
	//		attributeValues - A preallocated array of strings that will be filled out with attribute values
	//		empty - If true, will read empty tags (i.e. <tag/>)
	void GetTag(const char *tag, const char *attributeNames[], int maxlen, char *attributeValues[], bool empty=false,bool consume=true);

	// PURPOSE: Checks a tag and reads in any attributes if the tag matches
	// RETURNS: true if the next tag is <tag>
	// PARAMS:
	//		tag - The tag to read in
	//		attributeNames - a const array of attribute names terminated by an empty string
	//		maxlen - The maximum length for any one attribute value string
	//		attributeValues - A preallocated array of strings that will be filled out with attribute values
	//		empty - If true, will read empty tags (i.e. <tag/>)
	bool CheckTag(const char *tag, const char *attributeNames[], int maxlen, char *attributeValues[],bool empty=false);

	// PURPOSE: Checks a tag and reads in all attributes
	// PARAMS:
	//		tag - The tag to read in
	//		attributeNames - a const array of attribute names terminated by an empty string
	//		maxlen - The maximum length for any one attribute value string
	//		attributeValues - A preallocated array of strings that will be filled out with attribute values
	//		maxAttributes - Size of the attribute arrays
	void GetTagAttributes(const char *tag, char *attributeNames[], int maxlen, char *attributeValues[] , int maxAttributes,bool empty=false);

	// PURPOSE: Gets the next token in the stream
	int GetToken(char *dest,int maxlen);

	// PURPOSE: Gets the name of the next tag in the stream
	// RETURNS: length of tag name if valid tag found, 0 otherwise
	int GetTagName(char *dest, int maxlen);

	// PURPOSE: Reads the next token in the stream, asserts if it's not the same as match
	void MatchToken(const char *match);

	// PURPOSE: Reads the next token in the stream, asserts if it's not the same as match. Uses a case-insensitive comparision.
	void MatchIToken(const char *match);

	// PURPOSE: Returns true if check is the next token in the stream
	bool CheckToken(const char *check);

	// PURPOSE: Returns true if check is the next token in the stream. Uses a case-insensitive comparision.
	bool CheckIToken(const char *check);

	// PURPOSE: Same as MatchToken(de)
	void GetDelimiter(const char* de);

	// PURPOSE: Scans ahead to the point where this tag is closed.
	void PopTag();

	// PURPOSE: Scans past the XML declaration at the beginning of the file
	void PopXMLDeclaration();

	// PURPOSE: Scans past the DOCTYPE declaration at the beginning of the file.
	void PopDOCTYPE(char *firsttag, int maxlen, const char *dtdname = NULL);

	////////////////////////////////////////////////////////////////////////////////////////////
	// the following routines write out to the file

	// PURPOSE: Writes the XML declaration at the beginning of the file (e.g. "<?xml version=1.0 encoding="UTF-8"?>")
	// NOTES:
	//		This should be the first thing written in a XML file
	void PutXMLDeclaration();

	// PURPOSE: Writes the comment to the file, adding comment delimiters.
	void PutComment(const char *comment);

	// PURPOSE: Writes the doctype declaration.
	// PARAMS:
	//		firsttag - Name of the top-level tag
	//		dtdname - Name of the DTD that defines the schema
	// NOTES:
	//		This should be the second thing written in an XML file.
	void PutDOCTYPE(const char *firsttag, const char *dtdname=NULL);

	// PURPOSE: Starts a new tag (e.g. "<tag")
	// PARAMS:
	//		tag - The new tag to start
	//		tabs - How much to indent this tag
	//		closeImmediatly - Should the tag be closed right away (i.e. does it have no attributes)
	//		empty - Will this tag have children
	void PutTagStart(const char *tag, int tabs, bool closeImmediately=false, bool empty=false);

	// PURPOSE: Writes an attribute name-value pair.
	// PARAMS:
	//		attname - Name of the attribute
	//		attvalue - Value to write out
	//		asString - (bool only) If true, output "TRUE" or "FALSE". If false, output 1 or 0
	void PutAttribute(const char *attname, const char *attvalue);
	void PutAttribute(const char *attname, int attvalue);				// <ALIAS xmlAsciiTokenizerXml::PutAttribute@const char *@const char *>
	void PutAttribute(const char *attname, char attvalue);				// <ALIAS xmlAsciiTokenizerXml::PutAttribute@const char *@const char *>
	void PutAttribute(const char *attname, float attvalue);				// <ALIAS xmlAsciiTokenizerXml::PutAttribute@const char *@const char *>
	void PutAttribute(const char *attname, const Vector3 &attvalue);	// <ALIAS xmlAsciiTokenizerXml::PutAttribute@const char *@const char *>
	void PutAttribute(const char *attname, bool attvalue, bool asString=false); // <ALIAS xmlAsciiTokenizerXml::PutAttribute@const char *@const char *>

	// PURPOSE: Writes the end of the tag (i.e. the > or /> part of <tag> or <tag/> 
	// PARAMS:
	//		empty - Will this tag have no children? If true, closes the tag with /> and a call to PutEndTag is not allowed.
	void PutTagEnd(bool empty=false);

	// PURPOSE: Closes the current tag. (i.e. writes </tag>)
	void PutEndTag(const char *tag, int tabs);

	// ParseAssert can be used to report script errors to the script writer.
	// Use as ParseAssert(ASST_EXPR(expr), format, ...). If expr == FALSE it will report
	// the script name and line number, the source code name and line number,
	// the expression and a custom error message (specified by format and the
	// following arguments).
	void ParseAssert(int result, const char *expr,
			const char *file, int line, char *fmt, ...);

	// PURPOSE: detect <tag> ... </tag> versus <tag/>
	// NOTES: this function acts on the current tag and may assert if not called on a valid tag
	// should probably only call after CheckTagName
	bool	IsTagSingleLine();

protected:
	// PURPOSE: utility function for IsTagSingleLine
	int		GetTokenToCharFixed(char *dest,int maxLen,char terminator,bool stripWhiteSpace=true, bool ignoreCommentChar=false, bool consume=false);
};


}	// namespace rage

#endif
