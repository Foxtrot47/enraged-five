//
// xmldata/datatypes.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "xmldata/datatypes.h"
#include "xmldata/data.h"
#include "atl/map.h"
#include "bank/bank.h"

#include "diag/output.h"		// for Assertf
#include "file/stream.h" // for sprintf
#include "system/memory.h"
#include "string/string.h"
#include "system/timemgr.h"
#include "xmldata/xmltoken.h"

using namespace rage;

// Static members for aDataType and subclasses.
#if __BANK
u32 aDataType::smFrameLastChanged;
#endif

const char aString::smNullChar = 0;

// Load from an XML file. name is the name of the attribute, which is
// not stored in every object.

#if 0

void aDataType::LoadXML(const char *BANK_ONLY(name), xmlAsciiTokenizerXml &tok,
		const aAttribute &/*attrib*/)
{
	// This default function just skips the XML tag.
	Assert(tok.CheckTagName(name));
	tok.PopTag();
}

#endif

#if __BANK
void aDataType::AddWidget(const char* , bkBank &, const aDataType* )
{
}


void aDataType::UpdateFromDefault(const aDataType *parent)
{
	if(parent && (Flags & FLAG_DEFAULT) && !HasSameValue(*parent))
	{	SetValue(parent->Value);
		smFrameLastChanged = TIME.GetFrameCount();
	}
}

void aDataType::InitDefaultFlag(const aDataType *parent)
{
	smFrameLastChanged = UpdateFrame = TIME.GetFrameCount();
	if(!parent)
		return;
	Flags &= ~FLAG_DEFAULT;
	if(HasSameValue(*parent))
		Flags |= FLAG_DEFAULT;
}

void aDataType::UpdateDefaultFlag(const aDataType *parent)
{
	unsigned long frame = TIME.GetFrameCount();
	UpdateFrame = frame;
	smFrameLastChanged = frame;
	if(!parent || (frame - parent->UpdateFrame) < 4)
		return;
	Flags &= ~FLAG_DEFAULT;
	if(HasSameValue(*parent))
		Flags |= FLAG_DEFAULT;
}

#if A_USE_COMMENT

bool aDataType::ShouldSaveComment(const aDataType *parent) const
{
	const char *txt = Comment.Get();
	return txt[0] && (!parent || (strcmp(parent->Comment.Get(), txt) != 0));
}

#endif

#endif


aDataType *aInt::CreateInstance()
{
	return rage_new aInt;
}


void aInt::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &/*attrib*/, const aDataType *parent, bool BANK_ONLY(ignoreConstraints))
{
	if(parent)
		*this = *parent;

	static const char *attributeNames[] =
	{	"type", "value", "min", "max", "step", "comment", ""	};

	static char type[MAX_ATTR_LEN];
	static char value[MAX_ATTR_LEN];
	static char minValue[MAX_ATTR_LEN];
	static char maxValue[MAX_ATTR_LEN];
	static char step[MAX_ATTR_LEN];
	static char comment[MAX_ATTR_LEN];

	static char *attributeValues[] =
	{	type, value, minValue, maxValue, step, comment	};

	SetHasValue();

	type[0] = value[0] = minValue[0] = maxValue[0] = step[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN, attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "int"),
			"Invalid type for attribute '%s': 'int' expected, '%s' found.",
			name, type);

	if(value[0])
		ValueRef() = atoi(value);
#if __BANK
	if(minValue[0])
		MinValue = atoi(minValue);
	if(maxValue[0])
		MaxValue = atoi(maxValue);
	if(step[0])
		Step = atoi(step);
	else if(Step < 0)
	{	Step = int((MaxValue-MinValue)*0.01f);
		if(!Step)
			Step = 1;
	}
	if(!ignoreConstraints) { Assert(Step >= 0); }
#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
#endif
}


#if __BANK

void aInt::SaveXML(const char *name, xmlAsciiTokenizerXml &tok, int indent,
		const aDataType *parent)
{
	aInt *p = (aInt*)parent;
	if(p && p->ValueRef() == ValueRef() && p->MinValue == MinValue &&
			p->MaxValue == MaxValue && p->Step == Step)
		return;
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "int");
	if(!p || p->ValueRef() != ValueRef())
		tok.PutAttribute("value", ValueRef());
	if(!p || p->MinValue != MinValue)
		tok.PutAttribute("min", MinValue);
	if(!p || p->MaxValue != MaxValue)
		tok.PutAttribute("max", MaxValue);
	if(!p || p->Step != Step)
		tok.PutAttribute("step", Step);
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}

#endif

#if __BANK

void aInt::AddWidget(const char *name, bkBank &bank, const aDataType *parent)
{
	
	bank.AddSlider(name, &ValueRef(), MinValue, MaxValue, (float)Step,
			datCallback(MFA1(aDataType::UpdateDefaultFlag), this,
			(void*)parent)
#if A_USE_COMMENT
			,Comment.Get()
#endif
			);
	UpdateDefaultFlag(parent);
}


#endif

bool aInt::IsValid(void) const
{
#if __BANK
	if(ValueRef() > MaxValue || ValueRef() < MinValue)
		return false;
#endif
	return true;
}


aDataType *aFloat::CreateInstance(void)
{
	return rage_new aFloat;
}


void aFloat::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &/*attrib*/, const aDataType *parent, bool BANK_ONLY(ignoreConstraints))
{
	if(parent)
		*this = *parent;

	static const char *attributeNames[] =
	{	"type", "value", "min", "max", "step", "comment", ""	};

	static char type[MAX_ATTR_LEN];
	static char value[MAX_ATTR_LEN];
	static char minValue[MAX_ATTR_LEN];
	static char maxValue[MAX_ATTR_LEN];
	static char step[MAX_ATTR_LEN];
	static char comment[MAX_ATTR_LEN];

	static char *attributeValues[] =
	{	type, value, minValue, maxValue, step, comment	};

	SetHasValue();

	type[0] = value[0] = minValue[0] = maxValue[0] = step[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN, attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "double"),
			"Invalid type for attribute '%s': 'double' expected, '%s' found.",
			name, type);

	if(value[0])
		ValueRef() = (float)atof(value);

#if __BANK
	if(minValue[0])
		MinValue = (float)atof(minValue);
	if(maxValue[0])
		MaxValue = (float)atof(maxValue);
	if(step[0])
		Step = (float)atof(step);
	else if(Step < 0)
		Step = (MaxValue-MinValue)*0.01f;
	if(!ignoreConstraints) { Assert(Step >= 0); }

#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
#endif
}


#if __BANK

void aFloat::SaveXML(const char *name, xmlAsciiTokenizerXml &tok, int indent,
		const aDataType *parent)
{
	aFloat *p = (aFloat*)parent;
	if(p && AreEqual(p->ValueRef(), ValueRef()) && AreEqual(p->MinValue, MinValue) &&
			AreEqual(p->MaxValue, MaxValue) && AreEqual(p->Step, Step))
		return;
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "double");
	if(!p || !AreEqual(p->ValueRef(), ValueRef()))
		tok.PutAttribute("value", ValueRef());
	if(!p || !AreEqual(p->MinValue, MinValue))
		tok.PutAttribute("min", MinValue);
	if(!p || !AreEqual(p->MaxValue, MaxValue))
		tok.PutAttribute("max", MaxValue);
	if(!p || !AreEqual(p->Step, Step))
		tok.PutAttribute("step", Step);
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}

#endif


#if __BANK

void aFloat::AddWidget(const char *name, bkBank &bank,
		const aDataType *parent)
{
	
	bank.AddSlider(name, &ValueRef(), MinValue, MaxValue, Step,
			datCallback(MFA1(aDataType::UpdateDefaultFlag), this,
			(void*)parent)
#if A_USE_COMMENT
			,Comment.Get()
#endif
			);
	UpdateDefaultFlag(parent);
}

#endif

bool aFloat::IsValid(void) const
{
#if __BANK
	if(ValueRef() > MaxValue || ValueRef() < MinValue)
		return false;
#endif
	return true;
}


aDataType *aVector3::CreateInstance(void)
{
	return rage_new aVector3;
}


bool aVector3::HasSameValue(const aDataType &v) const
{
	const Vector3 &a = ValueRef();
	const Vector3 &b = ((aVector3&)v).ValueRef();

	return AreEqual(a.x, b.x) && AreEqual(a.y, b.y) && AreEqual(a.z, b.z);
}

	
void aVector3::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &/*attrib*/, const aDataType *parent, bool BANK_ONLY(ignoreConstraints))
{
	if(parent)
		*this = *parent;

	static const char *attributeNames[] =
	{	"type", "value", "min", "max", "step", "comment", ""	};

	static char type[MAX_ATTR_LEN];
	static char value[MAX_ATTR_LEN];
	static char minValue[MAX_ATTR_LEN];
	static char maxValue[MAX_ATTR_LEN];
	static char step[MAX_ATTR_LEN];
	static char comment[MAX_ATTR_LEN];

	static char *attributeValues[] =
	{	type, value, minValue, maxValue, step, comment	};

	SetHasValue();

	type[0] = value[0] = minValue[0] = maxValue[0] = step[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN, attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "double3"),
			"Invalid type for attribute '%s': 'double3' expected, '%s' found.",
			name, type);

	int start;
	if(value[0])
	{
		int i, j = 0;
		for(i = 0; i < 3; i++)
		{
			while(value[j] == ' ' || value[j] == '\t')
				j++;
			start = j;
			while(value[j] != ' ' && value[j] != '\t' && value[j] != 0)
				j++;
			Assert(value[j] || i == 2);
			value[j++] = 0;
			ValueRef()[i] = (float)atof(value + start);
		}
	}

#if __BANK
	if(minValue[0])
		MinValue = (float)atof(minValue);
	if(maxValue[0])
		MaxValue = (float)atof(maxValue);
	if(step[0])
		Step = (float)atof(step);
	else if(Step < 0)
		Step = (MaxValue-MinValue)*0.01f;
	if(!ignoreConstraints) { Assert(Step >= 0); }

#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
#endif
}


#if __BANK

void aVector3::SaveXML(const char *name, xmlAsciiTokenizerXml &tok, int indent,
		const aDataType *parent)
{
	aVector3 *p = (aVector3*)parent;

	if(p && AreEqual(p->ValueRef(), ValueRef()) && AreEqual(p->MinValue, MinValue) &&
			AreEqual(p->MaxValue, MaxValue) && AreEqual(p->Step, Step))
		return;
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "double3");
	if(!p || !AreEqual(p->ValueRef(), ValueRef()))
		tok.PutAttribute("value", ValueRef());
	if(!p || !AreEqual(p->MinValue, MinValue))
		tok.PutAttribute("min", MinValue);
	if(!p || !AreEqual(p->MaxValue, MaxValue))
		tok.PutAttribute("max", MaxValue);
	if(!p || !AreEqual(p->Step, Step))
		tok.PutAttribute("step", Step);
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}

#endif


#if __BANK

void aVector3::AddWidget(const char *name, bkBank &bank,
		const aDataType *parent)
{
	
	bank.AddSlider(name, &ValueRef(), MinValue, MaxValue, Step,
			datCallback(MFA1(aDataType::UpdateDefaultFlag), this,
			(void*)parent)
#if A_USE_COMMENT
			,Comment.Get()
#endif
			);
	UpdateDefaultFlag(parent);
}

#endif

bool aVector3::IsValid(void) const
{
#if __BANK
	const Vector3 &v = ValueRef();
	if(MaxValue < MinValue ||
			v.x < MinValue || v.y < MinValue || v.z < MinValue ||
			v.x > MaxValue || v.y > MaxValue || v.z > MaxValue)
		return false;
#endif
	return true;
}


#if __BANK

aEnumData::~aEnumData()
{
//	if(Choices)
//		DestroyChoiceArray(Choices, NumChoices);
}

#endif


aDataType *aEnumData::CreateInstance(void)
{
	return rage_new aEnumData;
}


bool aEnumData::IsValid(void) const
{
#if __BANK
	int v = ValueRef().Get();
	if(v < 0 || v >= NumChoices)
		return false;
#endif
	return true;
}


void aEnumData::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &attrib, const aDataType *parent, bool)
{
	if(parent)
		*this = *parent;

	static const char *attributeNames[] =
	{	"type", "value", "choices", "comment", ""	};

	static char type[MAX_ATTR_LEN_ENUM];
	static char value[MAX_ATTR_LEN_ENUM];
	static char choices[MAX_ATTR_LEN_ENUM];
	static char comment[MAX_ATTR_LEN_ENUM];

	static char *attributeValues[] =
	{	type, value, choices, comment	};

	SetHasValue();

	type[0] = value[0] = choices[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN_ENUM,
			attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "enum"),
			"Invalid type for attribute '%s': 'enum' expected, '%s' found.",
			name, type);

	if(choices[0])
	{
#if __BANK
		const char **choiceArray;
		int numChoices;
		sysMemStartTemp();
		choiceArray = CreateChoiceArray(choices, numChoices);
		sysMemEndTemp();

		Assert(numChoices > 0);
		if(attrib.NumChoices > 0)
		{
			Assertf(SameChoices(choiceArray, numChoices,
					attrib.ChoiceArray, attrib.NumChoices),
					"Enum choices for attribute %s don't match.",
					name);
			sysMemStartTemp();
			DestroyChoiceArray(choiceArray, numChoices);
			sysMemEndTemp();
		}
		else
		{
			attrib.NumChoices = numChoices;
			attrib.ChoiceArray = choiceArray;
		}
#else
		sysMemStartTemp();	// Not sure.
		if(attrib.NumChoices == 0)
			attrib.ChoiceArray = CreateChoiceArray(choices, attrib.NumChoices);
		sysMemEndTemp();
#endif
	}
#if __BANK
#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
	Choices = attrib.ChoiceArray;
	NumChoices = attrib.NumChoices;
#endif

	if(value[0])
		ValueRef().Set(StringToInt(value, attrib.ChoiceArray,
				attrib.NumChoices, choices));
}


#if __BANK

void aEnumData::SaveXML(const char *name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType *parent)
{
	const aEnumData *p = (aEnumData*)parent;

	bool sameValue = false;
	if(p)
	{
		sameValue = HasSameValue(*p);
		//if(&parent != &smNull && sameValue)
		if(sameValue)
			return;
	}

	Assert(IsValid());
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "enum");
	if(!sameValue)
		tok.PutAttribute("value", Choices[ValueRef().Get()]);
	//if(&parent == &smNull)
	if(!p)
	{
		char *str = CreateChoiceString();
		tok.PutAttribute("choices", str);
		DestroyChoiceString(str);
	}
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}

#endif


#if __BANK

void aEnumData::AddWidget(const char *name, bkBank &bank,
		const aDataType *parent)
{
	
	bank.AddCombo(name, &ValueRef().Value, NumChoices, Choices,
			datCallback(MFA1(aDataType::UpdateDefaultFlag),
			(aDataType*)this, (void*)parent));
	UpdateDefaultFlag(parent);
}

#endif


const char **aEnumData::CreateChoiceArray(const char *commaSep,
		int &numChoicesOut)
{
	int n = (int)strlen(commaSep), i, j, k;
	bool skip;

	numChoicesOut = 1;
	for(i = 0; i < n; i++)
		if(commaSep[i] == ',')
			numChoicesOut++;

	char **choices = rage_new char*[numChoicesOut];

	k = j = 0;
	skip = true;
	int len = 0;
	int lastNonWS = 0;
	for(i = 0; i <= n; i++)
	{
		char c = commaSep[i];
		switch(c)
		{
			case ' ':
			case '\t':
				if(!skip)
					len++;
				break;
			case 0:
			case ',':
				Assertf(!skip, "Empty enum choice in '%s'.",
						commaSep);
				skip = true;
				len -= (i - lastNonWS - 1);
				Assert(len >= 0);
				choices[k++] = rage_new char[len + 1];
				Assert(len <= 127);
				choices[k - 1][0] = (char)len;
				len = 0;
				break;
			default:
				skip = false;
				len++;
				lastNonWS = i;
				break;
		}
	}

	k = j = 0;
	skip = true;
	char *out = choices[0];
	lastNonWS = 0;
	len = (int)out[0];
	for(i = 0; i <= n; i++)
	{
		char c = commaSep[i];
		switch(c)
		{
			case ' ':
			case '\t':
				if(!skip)
				{	if(j < len)
						out[j] = c;
					j++;
				}
				break;
			case 0:
			case ',':
				Assert(!skip);
				skip = true;
				j -= (i - lastNonWS - 1);
				Assert(j >= 0);
				out[j] = 0;
				j = 0;
				k++;
				if(c)
				{
					out = choices[k];
					len = (int)out[0];
				}
				else
					out = NULL;
				break;
			default:
				skip = false;
				out[j++] = c;
				lastNonWS = i;
				break;
		}
	}
	Assert(k == numChoicesOut);

	return (const char**)choices;
}


void aEnumData::DestroyChoiceArray(const char **choices, int numChoices)
{
	for(int i = 0; i < numChoices; i++)
		delete [](char*)choices[i];
	delete []choices;
}


int aEnumData::StringToInt(const char *str, const char **choiceArray,
		int numChoices, const char *ASSERT_ONLY(commaSep))
{
	for(int i = 0; i < numChoices; i++)
		if(strcmp(choiceArray[i], str) == 0)
			return i;
	Assertf(0, "Invalid choice '%s': allowed choices are '%s'",
			str, commaSep);
	return 0;
}


#if __BANK
/*
const char **aEnumData::CloneChoiceArray(const char **choice, int numChoices)
{
	char **r = rage_new char*[numChoices];
	for(int i = 0; i < numChoices; i++)
	{
		Assert(choice[i]);
		r[i] = rage_new char[strlen(choice[i]) + 1];
		strcpy(r[i], choice[i]);
	}
	return (const char**)r;
}

	
bool aEnumData::SameChoices(const aEnumData &v)
{
	if(NumChoices != v.NumChoices)
		return false;
	for(int i = 0; i < NumChoices; i++)
		if(strcmp(Choices[i], v.Choices[i]) != 0)
			return false;
	return true;
}
*/

bool aEnumData::SameChoices(const char **choices1, int numChoices1,
		const char **choices2, int numChoices2)
{
	if(numChoices1 != numChoices2)
		return false;
	for(int i = 0; i < numChoices1; i++)
		if(strcmp(choices1[i], choices2[i]) != 0)
			return false;
	return true;
}


char *aEnumData::CreateChoiceString(void)
{
	int len = 0, i;
	for(i = 0; i < NumChoices; i++)
		len += (int)strlen(Choices[i]) + 1;
	char *str = rage_new char[len];
	int j = 0;
	for(i = 0; i < NumChoices; i++)
	{
		strcpy(str + j, Choices[i]);
		j += (int)strlen(Choices[i]);
		str[j++] = ',';
	}
	str[j - 1] = 0;
	Assert(j == len);

	return str;
}

#endif


#if __BANK
aBitfieldData::~aBitfieldData()
{
//	if(Choices)
//		DestroyChoiceArray(Choices, (u32*)ValueMap, NumChoices);
}
#endif


aDataType *aBitfieldData::CreateInstance(void)
{
	return rage_new aBitfieldData;
}


const char ** aBitfieldData::CreateChoiceArray( const char * commaSep, u32 ** valueMap, int &numChoicesOut )
{
	int n = (int)strlen(commaSep), i, k;
	bool skip;
	bool getValue;

	numChoicesOut = 1;
	for(i = 0; i < n; i++)
		if(commaSep[i] == ',')
			numChoicesOut++;

	char **choices = rage_new char*[numChoicesOut];
	*valueMap = rage_new u32[numChoicesOut];

	k = 0;
	skip = true;
	getValue = false;
	ASSERT_ONLY(bool gotValueLast = true);
	int len = 0;
	int lastNonWS = 0;
	for(i = 0; i <= n; i++)
	{
		char c = commaSep[i];
		switch(c)
		{
			case ' ':
			case '\t':
				if(!skip)
					len++;
				break;
			case '=':
				getValue = false;
			case 0:
			case ',':
				Assertf(!skip, "Empty bitfield choice in '%s'.",
						commaSep);
				skip = true;
				len -= (i - lastNonWS - 1);
				if( getValue )
				{
					Assert( !gotValueLast );
					char buf[8];
					strncpy( buf, &commaSep[i-len], len );
					buf[len] = '\0';
					(*valueMap)[k++] = atoi( buf );
					ASSERT_ONLY(gotValueLast = true);
				}
				else
				{
#if __ASSERT
					if(!gotValueLast)	// Not really necessary - Boundschecker complained about this.
						Assertf(gotValueLast, "expected a value to follow '%s'.", choices[k]);
#endif
					Assert(len >= 0);
					choices[k] = rage_new char[len + 1];
					Assert(len <= 127);
					strncpy( choices[k], &commaSep[i-len], len);
					choices[k][len] = '\0';
					ASSERT_ONLY(gotValueLast = false);
				}

				len = 0;
				getValue = true;
				
				break;
			default:
				skip = false;
				len++;
				lastNonWS = i;
				break;
		}
	}
	Assert(k == numChoicesOut);

	return (const char**)choices;	
}


void aBitfieldData::DestroyChoiceArray(const char **choices, u32 * valueMap, int numChoices)
{
	for(int i = 0; i < numChoices; i++)
		delete [](char*)choices[i];
	delete []choices;
	delete []valueMap;
}



void aBitfieldData::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &attrib, const aDataType *parent, bool)
{
	if(parent)
		*this = *parent;

	static const char *attributeNames[] =
	{	"type", "value", "choices", "comment", ""	};

	static char type[MAX_ATTR_LEN_ENUM];
	static char value[MAX_ATTR_LEN_ENUM];
	static char choices[MAX_ATTR_LEN_ENUM];
	static char comment[MAX_ATTR_LEN_ENUM];

	static char *attributeValues[] =
	{	type, value, choices, comment	};

	SetHasValue();

	type[0] = value[0] = choices[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN_ENUM,
			attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "bitfield"),
			"Invalid type for attribute '%s': 'bitfield' expected, '%s' found.",
			name, type);

	if(choices[0])
	{
#if __BANK
		const char **choiceArray;
		u32 * valueMap;
		int numChoices;
		sysMemStartTemp();
		choiceArray = CreateChoiceArray(choices, &valueMap, numChoices);
		sysMemEndTemp();
		Assert(numChoices > 0);
		if(attrib.NumChoices > 0)
		{
			Assertf(SameChoices(choiceArray, valueMap, numChoices,
					attrib.ChoiceArray, attrib.ValueMap, attrib.NumChoices),
					"Enum choices for attribute %s don't match.",
					name);
			sysMemStartTemp();
			DestroyChoiceArray(choiceArray, valueMap, numChoices);
			sysMemEndTemp();
		}
		else
		{
			attrib.NumChoices = numChoices;
			attrib.ChoiceArray = choiceArray;
			attrib.ValueMap = valueMap;
		}
#else
		if(attrib.NumChoices == 0)
			attrib.ChoiceArray = CreateChoiceArray(choices, (u32 **)&attrib.ValueMap, attrib.NumChoices);
#endif
	}
#if __BANK
#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
	Choices = attrib.ChoiceArray;
	NumChoices = attrib.NumChoices;
	ValueMap = attrib.ValueMap;
#endif

#if !__BANK
	// Declare ActiveChoices locally in non dev builds
	u32 ActiveChoices;
#endif

	if(value[0])
		ValueRef().Set(StringToBitfield(value, attrib.ChoiceArray,
				attrib.ValueMap, ActiveChoices, attrib.NumChoices, choices));
}


u32 aBitfieldData::StringToBitfield( const char * str, const char ** choiceArray, const u32 * valueMap, u32 &activeChoices, int numChoices, const char * ASSERT_ONLY(commaSep) )
{
	u32 rVal = 0;

	int n = (int)strlen(str);
	bool skip = true;
	int len = 0;
	int lastNonWS = 0;
	activeChoices = 0;

	for( int i = 0; i <= n; ++i )
	{
		const char c = str[i];
		switch(c)
		{
			case ' ':
			case '\t':
				if(!skip)
					len++;
				break;
			case 0:
			case ',':
				{
					ASSERT_ONLY(bool foundchoice = false);
					Assertf(!skip, "Empty enum choice in '%s'.",
							commaSep);
					skip = true;
					len -= (i - lastNonWS - 1);
					for( int j = 0; j < numChoices; ++j )
					{
						if( strncmp( choiceArray[j], &str[i-len], len ) == 0 )
						{
							rVal |= valueMap[j];
							activeChoices |= 1 << j;
							ASSERT_ONLY(foundchoice = true);
							break;
						}
					}
					Assertf(foundchoice, "Invalid bitfield choice '%s': allowed choices are '%s'",
						str, commaSep);
					len = 0;
				}
				break;
			default:
				skip = false;
				len++;
				lastNonWS = i;
				break;
		}

	}

	return rVal;
}


#if __BANK

char * aBitfieldData::CreateChoiceString() const
{
	const int bufSize = 16;
	char buf[bufSize];
	int len = 0, i;
	for(i = 0; i < NumChoices; i++)
	{
		sprintf(buf, "%d", ValueMap[i] );
		len += (int)strlen(Choices[i]) + (int)strlen(buf) + 2; // +1 for comma or '\0', +1 for equal sign
	}
	char *str = rage_new char[len];
	int j = 0;
	for(i = 0; i < NumChoices; i++)
	{
		sprintf( buf, "%d", ValueMap[i] );
		strcpy(str + j, Choices[i]);
		j += (int)strlen(Choices[i]);
		str[j++] = '=';
		strcpy(str + j, buf);
		j += (int)strlen(buf);
		str[j++] = ',';
	}
	str[j - 1] = 0;
	Assert(j == len);

	return str;
}

// assumes that a bitfield is a u32
char * aBitfieldData::CreateValueString() const
{
	u32 len = 0;

	for ( int i = 0; i < NumChoices; i++ )
	{
		if( ActiveChoices & (1 << i) )
			len += (int)strlen( Choices[i] ) + 1; // +1 for ',' or '\0'
	}

	// Fix when no alternatives are checked.
	if(len == 0)
		len++;

	char * str = rage_new char[len];
	u32 j = 0;

	for ( int i = 0; i < NumChoices; i++ )
	{
		if( ActiveChoices & (1 << i) )
		{
			strcpy( str + j, Choices[i] );
			j += (int)strlen(Choices[i]);
			str[j++] = ',';
		}
	}

	// Fix when no alternatives are checked.
	if(j == 0)
		j++;

	Assert( j==len );
	str[j-1] = '\0';

	return str;
}

void aBitfieldData::SaveXML(const char *name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType *parent)
{
	const aEnumData *p = (aEnumData*)parent;

	bool sameValue = false;
	if(p)
	{	sameValue = HasSameValue(*p);
		//if(&parent != &smNull && sameValue)
		if(sameValue)
			return;
	}

	Assert(IsValid());
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "bitfield");
	if(!sameValue)
	{
		char * str = CreateValueString();
		tok.PutAttribute("value", str);
		DestroyValueString( str );
	}
	//if(&parent == &smNull)
	if(!p)
	{
		char *str = CreateChoiceString();
		tok.PutAttribute("choices", str);
		DestroyChoiceString(str);
	}
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}


bool aBitfieldData::SameChoices(const char **choices1, const u32 *valueMap1, int numChoices1, const char **choices2, const u32 * valueMap2, int numChoices2)
{
	if(numChoices1 != numChoices2)
		return false;
	for(int i = 0; i < numChoices1; i++)
		if((strcmp(choices1[i], choices2[i]) != 0) || (valueMap1[i] != valueMap2[i]))
			return false;
	return true;
}


#endif


#if __BANK

void aBitfieldData::CalculateValueFromActiveChoices()
{
	u32 newValue = 0;
	for( int i = 0; i < NumChoices; i++ )
	{
		if( ActiveChoices & ( 1 << i) )
			newValue |= ValueMap[i];
	}

	ValueRef().Set( newValue );
}

void aBitfieldData::AddWidget(const char *name, bkBank &bank, const aDataType * /*parent*/)
{
	
	bank.PushGroup( name, false );
	for ( int i = 0; i < NumChoices; ++i )
	{
		bank.AddToggle( Choices[i], &ActiveChoices, (1 << i), datCallback( MFA(aBitfieldData::CalculateValueFromActiveChoices), this ) );
	}
	bank.PopGroup();
}

#endif


const aString &aString::operator=(const aString &v)
{	if(&v == this)
		return *this;
	Set(v.Get());
	return *this;
}


bool aString::HasSameValue(const aString &v) const
{
	return strcmp(Get(), v.Get()) == 0;
}

int aString::Length() const
{	
	return String ? (int)strlen( String ) : 0;
}
	
void aString::Set(const char *newString, int maxLength)
{
	if(String)	// Prevents OpDelete from being called.
	{
		if (String == newString) {
			return;
		}
		delete []String;
		String = NULL;
	}

	if(newString && newString[0])
	{
		char *buff;
		buff = rage_new char[( maxLength < 0 ) ? strlen(newString) + 1 : maxLength ];
		Assert(maxLength < 0 || (int)(strlen(newString) + 1) <= (int)maxLength);
		strcpy(buff, newString);
		String = buff;
	}
	else
	{
		String = ( maxLength < 0 ) ? NULL : rage_new char[maxLength];
		if( String ) String[0]=0;
	}
}


void aString::Insert( const char* s, int maxLength, int pos ) // negative will append after last char
{
	if(s && s[0])
	{
		int l = String ? (int)strlen( String ) : 0;
		if( pos < 0 || pos > l ) pos = l;
		Assert(maxLength < 0 || (int)( l + strlen(s) + 1 ) <= (int)maxLength);

		char *buff;
		buff = rage_new char[( maxLength < 0 ) ? l + strlen(s) + 1 : maxLength ];
		if( l ) 
		{
			if( pos > 0 )
			{
				strncpy( buff, String, pos );
				strcat( buff, s );
				strcat( buff, String + pos );
			}
			else
			{
				strcpy( buff, s );
				strcat( buff, String );
			}
		}
		else
		{
			strcpy( buff, s );
		}
		delete []String;
		String = buff;
	}
}

void aString::Insert( char c, int pos ) // negative will append after last char
{
	if( c )
	{
		int l = String ? (int)strlen( String ) : 0;
		if( pos < 0 || pos > l ) pos = l;
		char* buff = rage_new char[ l + 2 ];
		if( l )
		{
			if( pos > 0 )
			{
				strncpy( buff, String, pos );
				buff[ pos ] = c;
				buff[ pos + 1 ] = 0;
				strcat( buff, String + pos );
			}
			else
			{
				buff[ 0 ] = c;
				buff[ 1 ] = 0;
				strcat( buff, String );
			}
		}
		else
		{
			buff[ 0 ] = c;
			buff[ 1 ] = 0;		
		}
		delete []String;
		String = buff;
	}
}

void aString::Remove( int pos ) // negative will delete last char
{
	int l = String ? (int)strlen( String ) : 0;
	if( l >= 1 )
	{
		if( pos < 0 || pos >= l ) pos = l - 1;
		char* buff = rage_new char[ l ];
		strncpy( buff, String, pos );
		buff[ pos ] = 0;
		strcat( buff, String + pos + 1 );
		delete []String;
		String = buff;
	}
}

unsigned atHash( const aString& s )
{
	return atHash_const_char( s.Get() );
}


aDataType *aStringData::CreateInstance(void)
{
	return rage_new aStringData;
}


void aStringData::SetValue(void *ptr)
{
	ValueRef() = *(aString*)ptr;
#if __BANK
//	delete []StringBuff;
//	StringBuff = rage_new char[MaxLength];

	if(!StringBuff)
		StringBuff = rage_new char[MaxLength];
	Assert(StringBuff && (int)strlen(ValueRef().Get()) < (int)MaxLength);
	strcpy(StringBuff, ValueRef().Get());
#endif
}


void aStringData::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &/*attrib*/, const aDataType *parent, bool BANK_ONLY(ignoreConstraints))
{
	// We don't copy the value from the parent here. It will be overwritten
	// anyway, which would cause memory to be freed and screw up if buildin
	// resources.
	if(parent)
	{
#if __BANK
		Flags &= FLAG_HAS_VALUE;
		if(parent->GetHasValue())
			Flags |= FLAG_HAS_VALUE;
#if A_USE_COMMENT
		Comment.Set(parent->Comment.Get());
#endif
#endif
	}

	static const char *attributeNames[] =
	{	"type", "value", "maxlength", "comment", ""	};

	static char type[MAX_ATTR_LEN];
	static char value[MAX_ATTR_LEN];
	static char maxLength[MAX_ATTR_LEN];
	static char comment[MAX_ATTR_LEN];

	static char *attributeValues[] =
	{	type, value, maxLength, comment	};

	SetHasValue();

	type[0] = value[0] = maxLength[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN, attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "string"),
			"Invalid type for attribute '%s': 'string' expected, '%s' found.",
			name, type);

#if !__BANK
	// Note: this used to only happen if value[0] was non-zero -
	// could cause weird Release build crashes if the string already
	// had a value.
	ValueRef().Set(value, (int)strlen(value) + 1);	//atoi(maxLength));
#else
#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
	if(maxLength[0])
		MaxLength = atoi(maxLength);
	else
		MaxLength = (int)strlen(value) + 1;

	if(ignoreConstraints && !MaxLength)
		MaxLength = -1; // will set string length to strlen(value)+1

	ValueRef().Set(value, MaxLength);
#endif
}


#if __BANK

void aStringData::SaveXML(const char *name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType *parent)
{
	const aStringData *p = (const aStringData*)parent;
	if(p && HasSameValue(*p) && MaxLength == p->MaxLength)
		return;
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "string");
	if(!p || !HasSameValue(*p))
		tok.PutAttribute("value", ValueRef().Get());
	if(!p || p->MaxLength != MaxLength)
		tok.PutAttribute("maxlength", MaxLength);
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}

#endif


#if __BANK

void aStringData::StringChangedCB(const aDataType *parent)
{
	Assert(StringBuff);
	ValueRef().Set(StringBuff);
	UpdateDefaultFlag(parent);
}

void aStringData::AddWidget(const char *name, bkBank &bank,
		const aDataType *parent)
{
	
	if(!StringBuff)
		StringBuff = rage_new char[MaxLength];
	Assert((int)strlen(ValueRef().Get()) < (int)MaxLength);
	strcpy(StringBuff, ValueRef().Get());
	bank.AddText(name, StringBuff, MaxLength,
			datCallback(MFA1(aStringData::StringChangedCB),
			this, (void*)parent));
	UpdateDefaultFlag(parent);
}

#endif

bool aStringData::IsValid(void) const
{
#if __BANK
	const char *str = ValueRef().Get();
	if(!str || (int)strlen(str) > (int)MaxLength)
		return false;
#endif
	return true;
}
/////////////////////////////////////////////////////////////////////

aDataType *aBool::CreateInstance(void)
{
	return rage_new aBool;
}


void aBool::LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
		const aAttribute &/*attrib*/, const aDataType *parent, bool)
{
	if(parent)
		*this = *parent;

	static const char *attributeNames[] =
	{	"type", "value", "comment", ""	};

	static char type[MAX_ATTR_LEN];
	static char value[MAX_ATTR_LEN];
	static char comment[MAX_ATTR_LEN];

	static char *attributeValues[] =
	{	type, value, comment };

	SetHasValue();

	type[0] = value[0] = comment[0] = 0;

	tok.GetTag(name, attributeNames, MAX_ATTR_LEN, attributeValues, true);
	Assertf(!type[0] || !stricmp(type, "bool"),
			"Invalid type for attribute '%s': 'bool' expected, '%s' found.",
			name, type);

	if(value[0])
		ValueRef() = ( atoi(value) ) != 0;
#if __BANK
#if A_USE_COMMENT
	if(comment[0])
		Comment.Set(comment);
#endif
#endif
}


#if __BANK

void aBool::SaveXML(const char *name, xmlAsciiTokenizerXml &tok, int indent,
		const aDataType *parent)
{
	aBool *p = (aBool*)parent;
	if(p && p->ValueRef() == ValueRef())
		return;
	tok.PutTagStart(name, indent, false, true);
	tok.PutAttribute("type", "bool");
	if(!p || p->ValueRef() != ValueRef())
		tok.PutAttribute("value", ValueRef());
#if A_USE_COMMENT
	if(ShouldSaveComment(parent))
		tok.PutAttribute("comment", Comment.Get());
#endif
	tok.PutTagEnd(true);
}

#endif


#if __BANK

void aBool::AddWidget(const char *name, bkBank &bank, const aDataType *parent)
{
	
	bank.AddToggle(name, &ValueRef(), datCallback(MFA1(aDataType::UpdateDefaultFlag), this, (void*)parent) );
	UpdateDefaultFlag(parent);
}

#endif 

///////////////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------

#include "data/resourcehelpers.h"

aString::aString(datResource &rsc)
{
	rsc.PointerFixup(String);
}


void aFloat::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &/*attrib*/)
{
	::new(obj) aFloat(rsc);
}


void aInt::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &/*attrib*/)
{
	::new(obj) aInt(rsc);
}


void aBool::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &/*attrib*/)
{
	::new(obj) aBool(rsc);
}


void aStringData::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &/*attrib*/)
{
	::new(obj) aStringData(rsc);
}


void aVector3::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &/*attrib*/)
{
	::new(obj) aVector3(rsc);
}


void aEnumData::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &attrib)
{
	::new(obj) aEnumData(rsc, attrib);
}


void aBitfieldData::ResourceFixup(datResource &rsc, aDataType *obj,
		const aAttribute &attrib)
{
	::new(obj) aBitfieldData(rsc, attrib);
}


aDataType::aDataType(datResource &rsc)
{
	rsc.PointerFixup(Value);
#if __BANK
#if A_USE_COMMENT
	::new(&Comment) aString(rsc);
#endif
#endif
}


aFloat::aFloat(datResource &rsc) : aDataType(rsc)
{
}

aInt::aInt(datResource &rsc) : aDataType(rsc)
{
}

aBool::aBool(datResource &rsc) : aDataType(rsc)
{
}

aStringData::aStringData(datResource &rsc) : aDataType(rsc)
{
//	::new(Value) aString(rsc);

	// TODO: Check if this fails - it probably does.
//	Assert(!StringBuff);

}

aVector3::aVector3(datResource &rsc) : aDataType(rsc)
{
}

aEnumData::aEnumData(datResource &rsc, const aAttribute &BANK_ONLY(attrib)) :
		aDataType(rsc)
{
#if __BANK
	Choices = attrib.ChoiceArray;
	Assert(NumChoices == attrib.NumChoices);
#endif
}

aBitfieldData::aBitfieldData(datResource &rsc, const aAttribute &BANK_ONLY(attrib)) :
		aDataType(rsc)
{
#if __BANK
	Assert(NumChoices == attrib.NumChoices);
	Choices = attrib.ChoiceArray;
	ValueMap = attrib.ValueMap;
#endif
}

/* End of file xmldata/datatypes.cpp */
