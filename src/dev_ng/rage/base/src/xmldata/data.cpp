//
// xmldata/data.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#define ENABLE_LEAK_TRACKING (!__FINAL)

#include "xmldata/data.h"
#include "xmldata/datatypes.h"
#include "bank/bank.h"

#include "diag/output.h"
#include "file/stream.h"
#include "system/memory.h"
#include "data/resource.h"
#include "string/string.h"
#include "xmldata/xmltoken.h"

using namespace rage;
using namespace rage::aDataLimits;


// Shutdown the object.

void aAttributes::Shutdown(void)
{
	atSNode<aAttribute> *node;
	while((node = Attributes.PopHead()) != NULL)
		delete node;

#if A_IGNORE_ATTRIBUTES
	atSNode<const char*> *inode;
	while((inode = IgnoredAttributes.PopHead()) != NULL)
		delete inode;
#endif
}

	
aAttribute::aAttribute(void) : NumChoices(0), ChoiceArray(NULL),
		ValueMap(NULL)
{

}


aAttribute::~aAttribute()
{
	for(int i = 0; i < NumChoices; i++)
		delete [](char*)ChoiceArray[i];
	delete []ChoiceArray;
	delete [](u32*)ValueMap;
}


// Add an attribute. offset is the difference between the address of the
// attribute and the address of the struct. name is the name of the
// attribute, and attributeClassID identifies the data type of the
// attribute. Called by aDataStruct::AddAttribute().

int aAttributes::AddAttribute(int offset, const char *name,
		const aID &attributeClassID)
{
	atSNode<aAttribute> *node = rage_new atSNode<aAttribute>;
	aAttribute &attrib = node->Data;
	Assert(offset >= 0 && offset < (1 << (8*sizeof(attrib.Offset))));
	Assertf(XMLDATAMGR.IsValidAttributeClassID(attributeClassID),
			"Unknown attribute class for attribute '%s'.", name);
	attrib.Offset = (u16)offset;
	attrib.Name = name;
	attrib.Type = (u16)(int)attributeClassID;
	Attributes.Append(*node);
	return Attributes.GetNumItems() - 1;
}


#if A_IGNORE_ATTRIBUTES

void aAttributes::IgnoreAttribute(const char *name)
{
	atSNode<const char*> *node = rage_new atSNode<const char*>;
	node->Data = name;
	IgnoredAttributes.Append(*node);
}

#endif


// Copy (inherit) the attributes belonging to another struct (which
// should be a base class of this struct). Called by
// aDataStruct::InheritAttributes().

void aAttributes::CopyAttributes(const aAttributes &source)
{
	const atSNode<aAttribute> *srcNode = source.Attributes.GetHead();
	for(; srcNode; srcNode = srcNode->GetNext())
	{
		atSNode<aAttribute> *node = rage_new atSNode<aAttribute>;
		node->Data = srcNode->Data;
		Attributes.Append(*node);
	}

#if A_IGNORE_ATTRIBUTES
	const atSNode<const char*> *isrcNode = source.IgnoredAttributes.GetHead();
	for(; isrcNode; isrcNode = isrcNode->GetNext())
	{
		atSNode<const char*> *inode = rage_new atSNode<const char*>;
		inode->Data = isrcNode->Data;
		IgnoredAttributes.Append(*inode);
	}
#endif
}

	
// This function initializes an array of aDataType objects. arraySize
// is the size of the array, which must be at least NumAttributes.
// It sets up the pointers to the attribute values in base.

void aAttributes::CreateAttributeValues(aDataType **valueArray,
		int ASSERT_ONLY(arraySize), const aDataStruct *base) const
{
	const atSNode<aAttribute> *node = Attributes.GetHead();
	int numAttrib = Attributes.GetNumItems();
	Assert(arraySize >= numAttrib);
	for(int i = 0; i < numAttrib; i++, node = node->GetNext())
	{
		Assert(node);
		valueArray[i] = node->Data.CreateValue();
		valueArray[i]->SetValuePtr((void*)((char*)base + node->Data.Offset));
	}
}


void aAttributes::ResourceFixup(datResource &rsc,
		aDataType **valueArray, int ASSERT_ONLY(arraySize)) const
{
	const atSNode<aAttribute> *node = Attributes.GetHead();
	int numAttrib = Attributes.GetNumItems();
	Assert(arraySize >= numAttrib);
	for(int i = 0; i < numAttrib; i++, node = node->GetNext())
	{
		Assert(node);
		rsc.PointerFixup(valueArray[i]);
		node->Data.ResourceFixup(rsc, valueArray[i]);
	}
}


// Destroy resources allocated through CreateAttributeValues(). Note
// that this does not destroy the array itself.

void aAttributes::DestroyAttributeValues(aDataType **valueArray,
		int ASSERT_ONLY(arraySize)) const
{
	const atSNode<aAttribute> *node = Attributes.GetHead();
	int numAttrib = Attributes.GetNumItems();
	Assert(arraySize >= numAttrib);
	for(int i = 0; i < numAttrib; i++, node = node->GetNext())
	{	Assert(node);
		node->Data.DestroyValue(valueArray[i]);
	}
	Assert(!node);
}


void aAttributes::Validate(aDataType **valueArray,
		const char *structName, const char *fileName) const
{
	static char buff[256];
	const atSNode<aAttribute> *node = Attributes.GetHead();

	if(!structName)
		structName = "<unknown>";
	if(!fileName)
		fileName = "<unknown>";

	int numAttrib = Attributes.GetNumItems();
	for(int i = 0; i < numAttrib; i++, node = node->GetNext())
	{
		Assert(node);
		Assert(valueArray);
		if(!valueArray[i]->GetHasValue())
		{
			formatf(buff, sizeof(buff), "Attribute '%s' missing in "
					"struct '%s', file '%s'.",
					node->Data.Name, structName, fileName);
			ReportError(buff);
		}
		if(!valueArray[i]->IsValid())
		{
			formatf(buff, sizeof(buff),
					"Attribute '%s' has invalid value in struct '%s',"
					"file '%s'.", node->Data.Name, structName, fileName);
			ReportError(buff);
		}
	}
	Assert(!node);
}

	
void aAttributes::ReportError(const char *message) const
{
#if __ASSERT
	Assertf(0, "%s", message);
#else
	Quitf(message);
#endif
}


// Create a new aDataType object of the subclass corresponding to Type.

aDataType *aAttribute::CreateValue(void) const
{
	return XMLDATAMGR.GetCreateAttributeFunc(Type)();
}


void aAttribute::ResourceFixup(datResource &rsc, aDataType *obj) const
{
	aResourceFixupFunc func = XMLDATAMGR.GetResourceFixupFunc(Type);
	if(func)
		func(rsc, obj, *this);
}


// Destroy an aDataType object created by CreateValue().

void aAttribute::DestroyValue(aDataType *val) const
{
	Assert(val);
	delete val;
}


CURRENT_IMPLEMENTATION(aDataStructManager);

aDataStructManager::aDataStructManager() :
	m_AttributeNameChecksum(0),
	m_Flags(0),
	m_NumStructs(0),
	m_IgnoreInitializationOrder(false),
	m_NumAttributeClasses(0)
{

}

#if ENABLE_LEAK_TRACKING

static bool sTrackingEnabled = false;
static int sTrackingCounter[MAX_NUM_STRUCTS];
static ConstString *sTrackingStructNames = NULL;


void aDataStructManager::StartTracking(void)
{
	Assert(!sTrackingEnabled);

	int i;
	for(i = 0; i < MAX_NUM_STRUCTS; i++)
		sTrackingCounter[i] = 0;

	sTrackingEnabled = true;
}


void aDataStructManager::EndTracking(void)
{
	bool gotLeaks = false;

	Assert(sTrackingEnabled);
	int i;
	for(i = 0; i < MAX_NUM_STRUCTS; i++)
	{
		if(!sTrackingCounter[i])
			continue;
		if(!gotLeaks)
		{
			Displayf(TRed "***\nLeaks detected in aDataStruct sub-classes:");
			gotLeaks = true;
		}
		Assert(sTrackingStructNames);
		Displayf(TRed "Class: %-40s  ID: %-3d:  Leaks: %-4d",
				(const char*)sTrackingStructNames[i],
				i, sTrackingCounter[i]);
	}
	if(gotLeaks)
	{	Displayf(TRed "To avoid these leaks, make sure that aDataStruct::"
				"Shutdown() is called");
		Displayf(TRed "for all classes.\n***");
	}
	sTrackingEnabled = false;
}

#else	// !ENABLE_LEAK_TRACKING

void aDataStructManager::StartTracking(void)
{	}

void aDataStructManager::EndTracking(void)
{	}

#endif

void aDataStruct::Init(gdfTypeID gdfID)
{
	Init(gdfID.GetIDInNamespace());
}

void aDataStruct::Init(int structID)
{
#if ENABLE_LEAK_TRACKING
	if(sTrackingEnabled)
	{
		Assert(structID >= 0 && structID < MAX_NUM_STRUCTS);
		sTrackingCounter[structID]++;
	}
#endif

//	const aID &structID = GetStructID();
	StructID = (u16)structID;
#if __BANK
	Assertf(XMLDATAMGR.IsValidStructID(structID),
			"Class not initialized.");
	const aAttributes &attrib = XMLDATAMGR.GetAttributes(StructID);
	Assert(!Attributes);
	NumAttributes = attrib.GetNumAttributes();
	Attributes = rage_new aDataType*[NumAttributes];
	attrib.CreateAttributeValues(Attributes, NumAttributes, this);
#endif

//	Reset();
}


void aDataStruct::Shutdown(void)
{
#if ENABLE_LEAK_TRACKING
	if(sTrackingEnabled)
	{
		Assert(StructID < 512);
		sTrackingCounter[StructID]--;
		if(sTrackingCounter[StructID] == -1)
		{
			Assert(sTrackingStructNames);
			Displayf(TRed "aDataStruct::Shutdown() called without "
					"aDataStruct::Init() for class '%s' (ID %d).",
					(const char*)sTrackingStructNames[StructID], StructID);
		}
	}
#endif

#if __BANK
	Assert(Attributes || NumAttributes == 0);
	if(NumAttributes > 0)
	{
		const aAttributes &attrib = XMLDATAMGR.GetAttributes(StructID);
		attrib.DestroyAttributeValues(Attributes, NumAttributes);
	}
	delete []Attributes;
	Attributes = 0;
	NumAttributes = 0;
#endif
}


// Copy attribute values from a parent object, which should be of the
// same class as this object.

void aDataStruct::CopyFromParent(const aDataStruct &parent)
{
	Assert(StructID < XMLDATAMGR.GetNumStructs());
	Assertf(parent.StructID == StructID,
			"Trying to inherit from component with different class.");

#if __BANK
	aDataType **attributes = Attributes;
	aDataType **parentAttributes = parent.Attributes;
	int numAttributes = NumAttributes;
#else
	sysMemStartTemp();
	const aAttributes &attrib = XMLDATAMGR.GetAttributes(StructID);
	int numAttributes = attrib.GetNumAttributes();
	aDataType **attributes = rage_new aDataType*[numAttributes];
	aDataType **parentAttributes = rage_new aDataType*[numAttributes];
	attrib.CreateAttributeValues(attributes, numAttributes, this);
	attrib.CreateAttributeValues(parentAttributes, numAttributes, &parent);
	sysMemEndTemp();
#endif

	for(int i = 0; i < numAttributes; i++)
		*attributes[i] = *parentAttributes[i];

#if !__BANK
	sysMemStartTemp();
	attrib.DestroyAttributeValues(attributes, numAttributes);
	attrib.DestroyAttributeValues(parentAttributes, numAttributes);
	delete []attributes;
	delete []parentAttributes;
	sysMemEndTemp();
#endif
}


#if __BANK

void aDataStruct::UpdateDefaultFlags(const aDataStruct &parent, bool init)
{
	for(int i = 0; i < NumAttributes; i++)
	{	aDataType *p = parent.Attributes[i];
		if(init)
			Attributes[i]->InitDefaultFlag(p);
		else
			Attributes[i]->UpdateDefaultFlag(p);
	}
}

void aDataStruct::UpdateFromParent(const aDataStruct &parent)
{
	for(int i = 0; i < NumAttributes; i++)
		Attributes[i]->UpdateFromDefault(parent.Attributes[i]);
}

#endif


#if __BANK

int numSameCharacters(const char *a, const char *b)
{
	int i=0;
	while(a[i] && b[i] && a[i]==b[i]) i++;
	return i;
}

bool isUpper(char c)
{
	return (c>='A' && c<='Z');
}

void aDataStruct::AddWidgetsCB(bkBank &bank) 
{
	// Just call the virtual version with default parameters so RegisterBank still works properly.
	AddWidgets(bank);
}


void aDataStruct::AddWidgets(bkBank &bank, const aDataStruct *parent,
		bool useGroups)
{
	
	const aAttributes &attrib = XMLDATAMGR.GetAttributes(StructID);
	const atSNode<aAttribute> *node = attrib.GetList().GetHead();
	const aDataType *pType = NULL;
	int nameOffset=0;
	char buf[32];
	for(int i = 0; i < NumAttributes; i++, node = node->GetNext())
	{
		// here be code for grouping similarly named widgets
		if(nameOffset && strncmp(buf,node->Data.Name,nameOffset))
		{
			nameOffset=0;
			bank.PopGroup();
		}
		if(useGroups && !nameOffset && i+2<NumAttributes)
		{
			const atSNode<aAttribute> *tmpnode=node;
			int j=numSameCharacters(tmpnode->Data.Name,tmpnode->GetNext()->Data.Name);
			if(j>3)
			{
				tmpnode=tmpnode->GetNext();
				int k=numSameCharacters(tmpnode->Data.Name,tmpnode->GetNext()->Data.Name);
				j=Min(j,k);
			}
			if(j>3 && isUpper(node->Data.Name[j]))
			{
				strncpy(buf,node->Data.Name,j);
				buf[j]=0;
				nameOffset=j;
				bool under=false;
				if(buf[j-1]=='_') under=true,buf[j-1]=0; // if it ends in underscore, don't put that in the group title
				bank.PushGroup(buf,false);
				if(under) buf[j-1]='_';
			}
		}

		if(parent)
			pType = parent->Attributes[i];
		Attributes[i]->AddWidget(node->Data.Name+nameOffset, bank, pType);
	}
	if(nameOffset) bank.PopGroup();	
}

#endif


/*
void aDataStruct::Reset(void)
{
#if __BANK
	aDataType **attributes = Attributes;
	int numAttributes = NumAttributes;
#else
	const aAttributes &attrib = m_StructAttributes[StructID];
	int numAttributes = attrib.GetNumAttributes();
	aDataType **attributes = rage_new aDataType*[numAttributes];
	attrib.CreateAttributeValues(attributes, numAttributes, this);
#endif

	for(int i = 0; i < numAttributes; i++)
		attributes[i]->SetValueToDefault();

#if !__BANK
	attrib.DestroyAttributeValues(attributes, numAttributes);
	delete []attributes;
#endif
}
*/


void aDataStruct::LoadXML(xmlAsciiTokenizerXml &tok, const char *name,
		const aDataStruct *parent)
{
	Assert(StructID < XMLDATAMGR.GetNumStructs());
	const aAttributes &attrib = XMLDATAMGR.GetAttributes(StructID);
	int i;

	Assert(!parent || parent->StructID == StructID);
	Assert(parent != this);

	sysMemStartTemp();

#if __BANK
	aDataType **attributes = Attributes;
	int numAttributes = NumAttributes;

	const aDataType* const* parentAttributes = NULL;
	if(parent)
	{	parentAttributes = parent->Attributes;
		Assert(parentAttributes || !numAttributes);
	}
#else
	int numAttributes = attrib.GetNumAttributes();
	aDataType **attributes = rage_new aDataType*[numAttributes];
	attrib.CreateAttributeValues(attributes, numAttributes, this);

	aDataType **parentAttributes = NULL;
	if(parent)
	{	parentAttributes = rage_new aDataType*[numAttributes];
		attrib.CreateAttributeValues(parentAttributes, numAttributes, parent);
	}
#endif
	bool *readAttributes = rage_new bool[numAttributes];
	for(i = 0; i < numAttributes; i++)
		readAttributes[i] = false;

	sysMemEndTemp();

	if(!tok.CheckTag("attributes", true) && tok.CheckTagName("attributes"))
	{
		tok.PopTag();

		const atSNode<aAttribute> *node;

		while(!tok.CheckEndTag("attributes"))
		{
			char tagName[256];
			tok.GetTagName(tagName, sizeof(tagName));

			node = attrib.GetList().GetHead();
			for(i = 0; i < numAttributes; i++, node = node->GetNext())
			{
				Assert(node);
				if(!stricmp(tagName, node->Data.Name))
				{
					const aDataType *parentAttr = NULL;
					if(parent)
					{
						Assert(parentAttributes || !numAttributes);
						parentAttr = parentAttributes[i];
					}
#if __BANK
					attributes[i]->LoadXML(node->Data.Name, tok, node->Data, parentAttr, IgnoreValueConstraints);
					Assertf(!readAttributes[i],
							"Attribute '%s' found more than once.", tagName);
#else
					attributes[i]->LoadXML(node->Data.Name, tok, node->Data, parentAttr, false);
#endif
					readAttributes[i] = true;
					break;
				}
			}
			if(i == numAttributes)
			{
#if A_IGNORE_ATTRIBUTES
				const atSNode<const char*> *inode;
				for(inode = attrib.GetIgnoredList().GetHead(); inode; inode = inode->GetNext())
					if(stricmp(tagName, inode->Data) == 0)
						break;
				if(!inode)
#endif
				{	Warningf("Unknown attribute '%s' found (%s).", tagName, tok.filename);	}
				tok.PopTag();
			}
			Assert(i != numAttributes || !node);
		}
	}

	if(parent)
	{	Assert(parentAttributes || !numAttributes);
		for(i = 0; i < numAttributes; i++)
		{
			if(!readAttributes[i])
				*attributes[i] = *parentAttributes[i];
		}
	}

	if(!tok.CheckTag("contents", true))
	{
		if(tok.CheckTag("contents", false))
		{
			LoadContents(tok);
			// TODO: Worry about the contents.
			tok.GetEndTag("contents");
		}
	}

#if __BANK
	if(!IgnoreValueConstraints)
#endif
		attrib.Validate(attributes, name, tok.GetStream()->GetName());

	sysMemStartTemp();
	delete []readAttributes;
#if __BANK
//	node = attrib.GetList().GetHead();
//	for(i = 0; i < numAttributes; i++, node = node->GetNext())
//		if(!readAttributes[i])
//			Warningf("Attribute '%s' not found.", node->Data.Name);
#else
	attrib.DestroyAttributeValues(attributes, numAttributes);
	delete []attributes;

	if(parent)
	{	attrib.DestroyAttributeValues(parentAttributes, numAttributes);
		delete []parentAttributes;
	}
#endif
	sysMemEndTemp();

	AfterLoad(); // intended to be overridden by child classes
}


#if __BANK

void aDataStruct::SaveXML(xmlAsciiTokenizerXml &tok, int indent, const aDataStruct *parent) const
{
	BeforeSave(); // intended to be overridden by child classes

	Assert(StructID < XMLDATAMGR.GetNumStructs());
	tok.PutTagStart("attributes", indent, true);
	const atSNode<aAttribute> *node = XMLDATAMGR.GetAttributes(StructID).GetList().GetHead();
	for(int i = 0; i < NumAttributes; i++, node = node->GetNext())
	{	Assert(node);
		const aDataType *parentAttrib;
		if(parent)
			parentAttrib = parent->Attributes[i];
		else
			parentAttrib = NULL;
			// parentAttrib = &Attributes[i]->GetNull();

		Attributes[i]->SaveXML(node->Data.Name, tok, indent + 1, parentAttrib);
	}
	Assert(!node);
	tok.PutEndTag("attributes", indent);

	tok.PutTagStart("contents", indent, true);
	SaveContents(tok, indent + 1, parent);
	tok.PutEndTag("contents", indent);
}

#endif


// Call this to start initializing the class. Call AddAttributeClass(),
// AddStruct(), AddAttribute() and InheritAttributes() as needed,
// and then call EndInitClass() when finished.

void aDataStructManager::BeginInitClass(void)
{	Assert((m_Flags & SMFLAG_INITPHASE) == 0);
	Assert((m_Flags & SMFLAG_INITIALIZED) == 0);
	m_Flags |= SMFLAG_INITPHASE;

	m_AttributeNameChecksum = 0;

#if ENABLE_LEAK_TRACKING
	Assert(!sTrackingStructNames);
	sTrackingStructNames = rage_new ConstString[MAX_NUM_STRUCTS];
#endif
}


// Call when done adding initializing the class, before creating
// aDataStruct objects.

void aDataStructManager::EndInitClass(void)
{	Assert((m_Flags & SMFLAG_INITPHASE) != 0);
	Assert((m_Flags & SMFLAG_INITIALIZED) == 0);
	m_Flags = (m_Flags & ~SMFLAG_INITPHASE) | SMFLAG_INITIALIZED;
}


// ShutdownClass() undoes the effects of BeginInitClass()/
// AddAttributeClass()/EndInitClass().

void aDataStructManager::ShutdownClass(void)
{
#if ENABLE_LEAK_TRACKING
	Assert(sTrackingStructNames);
	delete []sTrackingStructNames;
	sTrackingStructNames = NULL;
#endif

	Assert((m_Flags & SMFLAG_INITPHASE) == 0);
	Assert((m_Flags & SMFLAG_INITIALIZED) != 0);
	m_Flags = 0;

	int i;
	for(i = 0; i < m_NumStructs; i++)
		m_StructAttributes[i].Shutdown();

	m_NumAttributeClasses = m_NumStructs = 0;
}


// Add an attribute class (or data type). Use like
//	aDataStruct::AddAttributeClass(ID(float), aFloat::CreateInstance);

void aDataStructManager::AddAttributeClass(const aID &attributeClassID,
		aCreateDataTypeFunc createFunc, aResourceFixupFunc fixupFunc,
		const char * /*className*/)
{
	Assert(m_NumAttributeClasses < MAX_NUM_ATTRIBUTE_CLASSES);
	Assert(createFunc);
	Assert(InInitPhase());
	int id;
	id = attributeClassID;
	Assert(attributeClassID == m_NumAttributeClasses);
	m_NumAttributeClasses++;

	m_CreateAttributeFuncs[id] = createFunc;
	m_ResourceFixupFuncs[id] = fixupFunc;
}

	
void aDataStructManager::PrintAttributes(const aID &structID)
{
	const aAttributes &attrib = m_StructAttributes[(int)structID];
	const atSNode<aAttribute> *node;
	for(node = attrib.GetList().GetHead(); node; node = node->GetNext())
		Displayf("%s %d %d", node->Data.Name, node->Data.Offset, node->Data.Type);
}


// Returns true if id is a valid attribute class ID.

bool aDataStructManager::IsValidAttributeClassID(const aID &id)
{	return (int)id >= 0 && (int)id < m_NumAttributeClasses;	}


// Returns true if id is a valid ID for a aDataStruct subclass.

bool aDataStructManager::IsValidStructID(int id)
{	return id >= 0 && id < m_NumStructs;	}


// Set up the ID for a new struct. Protected because it's intended to be
// called from a subclass through the BEGIN_ATTRIBUTES() macro.

void aDataStructManager::AddStruct(const aID &id,
#if ENABLE_LEAK_TRACKING
		const char *className)
#else
		const char* ASSERT_ONLY(className))
#endif
{
	Assert(m_NumStructs < MAX_NUM_STRUCTS);
	Assert(InInitPhase());

#if ENABLE_LEAK_TRACKING
	if( !m_IgnoreInitializationOrder )
	{
		Assertf(id == m_NumStructs, "aDataStruct class '%s' (ID %d)"
				" not initialized in the correct order, make sure the order in "
				"'sagactor/componentsystem.h' matches the order in the code."
				" Also, a clean build sometimes helps in cases like this.",
				className, (int)id);
	}
#endif

	m_NumStructs++;
	m_StructAttributes[id].Init(id);

#if ENABLE_LEAK_TRACKING
	Assert(className);
	Assert(sTrackingStructNames);
	sTrackingStructNames[id] = className;
#endif
}


// Add an attribute for the struct with the specified ID. attribute should
// point to an attribute value in the struct pointed to by base. name is
// the name of the attribute and attributeClassID determines the data type
// of the attribute. Normally called through the ATTRIBUTE() macro.

int aDataStructManager::AddAttribute(const aID &structID, const void *attribute,
		const char *name, aDataStruct *base, const aID &attributeClassID)
{
	// This checksum calculation is not very sophisticated
	// but will probably work just fine.
	const char *ptr;
	for(ptr = name; *ptr; ptr++)
		m_AttributeNameChecksum += *ptr;

	ptrdiff_t offset = (char*)attribute - (char*)base;
	Assert(offset >= 0);
	Assert(IsValidStructID(structID));
	Assert(InInitPhase());
	return m_StructAttributes[(int)structID].AddAttribute((int)offset, name, attributeClassID);
}


#if A_IGNORE_ATTRIBUTES

void aDataStructManager::IgnoreAttribute(const aID &structID, const char *name)
{
	Assert(IsValidStructID(structID));
	Assert(InInitPhase());
	m_StructAttributes[(int)structID].IgnoreAttribute(name);
}

#endif


// Inherit all attributes from a base class. structID is the ID for the
// struct to which the attributes should be added, and parentID is the
// ID for the base class.

void aDataStructManager::InheritAttributes(const aID &structID, const aID &parentID)
{
	Assert(IsValidStructID(structID));
	Assertf(IsValidStructID(parentID),
			"Inheriting attributes from uninitialized class.");
	Assert(InInitPhase());
	m_StructAttributes[(int)structID].CopyAttributes(m_StructAttributes[(int)parentID]);
}

//-----------------------------------------------------------------------------

#include "data/resourcehelpers.h"

aDataStruct::aDataStruct(datResource &BANK_ONLY(rsc))
{
#if __BANK
	rsc.PointerFixup(Attributes);
	Assert(StructID < XMLDATAMGR.GetNumStructs());
	const aAttributes &attrib = XMLDATAMGR.GetAttributes(StructID);
	attrib.ResourceFixup(rsc, Attributes, NumAttributes);
#endif
}

/* End of file xmldata/data.cpp */
