// 
// xmldata/data.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef XMLDATA_DATA_H
#define XMLDATA_DATA_H

#include "atl/slist.h"
#include "atl/binmap.h"
#include "data/base.h"
#include "obsolete/singleton.h"
#include "string/string.h"
#include "system/bit.h"
#include "xmldata/id.h"

namespace rage {

// Forward declarations.
class aAttribute;
class aDataStruct;
class aDataType;
class xmlAsciiTokenizerXml;
class bkBank;

// This makes the IGNORE_ATTRIBUTE() macro suppress warning about attributes
// found in XML files but not in the code. We may want to disable this
// (and possibly all such warnings) in Release builds.
#define A_IGNORE_ATTRIBUTES 1

// PURPOSE: aAttributes is basically a set of aAttribute objects, associated with a
// struct of data.
// <FLAG Component>
class aAttributes
{
public:
	// PURPOSE: Initialize the object. 
	// PARAMS:
	//	id - the ID for the data struct.
	void Init(const aID &)
	{	}

	// PURPOSE: Shutdown the object.
	void Shutdown(void);

	// PURPOSE: Adds an attribute to the attribute list.
	// PARAMS:
	//		offset - The difference between the address of the attribute and the address of the struct
	//		name - The name of the attribute
	//		attributeClassID - the data type of the attribute
	// NOTES:
	//		Called by aDataStruct::AddAttribute()
	int AddAttribute(int offset, const char *name,
			const aID &attributeClassID);

#if A_IGNORE_ATTRIBUTES
	// PURPOSE: Ignore the named attribute, if it occurs.
	void IgnoreAttribute(const char *name);
#endif

	// PURPOSE: Copy (inherit) the attributes belonging to another struct (which
	// should be a base class of this struct). Called by
	// aDataStruct::InheritAttributes().
	void CopyAttributes(const aAttributes &source);

	// PURPOSE: This function initializes an array of aDataType objects. 
	// PARAMS:
	//		valueArray - The array of data type pointers to copy.
	//		arraySize - The size of the array, which must be at least NumAttributes in length.
	//		base - ?
	void CreateAttributeValues(aDataType **valueArray, int arraySize,
			const aDataStruct *base) const;

	void ResourceFixup(datResource &rsc, aDataType **valueArray,
			int arraySize) const;

	// PURPOSE: Destroy resources allocated through CreateAttributeValues(). Note
	// that this does not destroy the array itself.
	void DestroyAttributeValues(aDataType **valueArray, int arraySize) const;

	void Validate(aDataType **valueArray, const char *structName,
			const char *fileName) const;

	// PURPOSE: Get a read-only list of the attributes.
	const atSList<aAttribute> &GetList(void) const
	{	return Attributes;	}

	// PURPOSE: Get the number of attributes.
	int GetNumAttributes(void) const
	{	return Attributes.GetNumItems();	}

#if A_IGNORE_ATTRIBUTES
	// PURPOSE: Return a list of ignored attributes
	const atSList<const char*> &GetIgnoredList(void) const
	{	return IgnoredAttributes;	}
#endif

protected:
	void ReportError(const char *message) const;
	
	// Linked list of attributes.
	atSList<aAttribute> Attributes;

#if A_IGNORE_ATTRIBUTES
	atSList<const char*> IgnoredAttributes;
#endif
};


// PURPOSE: aAttribute contains information about an attribute of a struct which is
// the same for all instances of that struct. This includes the name, the type
// and the relative address of its value.
// <FLAG Component>
class aAttribute
{
public:
	aAttribute(void);
	~aAttribute();

	// PURPOSE: Create a new aDataType object of the subclass corresponding to Type.
	aDataType *CreateValue(void) const;

	void ResourceFixup(datResource &rsc, aDataType *obj) const;

	// PURPOSE: Destroy an aDataType object created by CreateValue().
	void DestroyValue(aDataType *val) const;

	// This is the difference between the address of the attribute and the
	// address of the struct that contains it.
	u16 Offset;

	// The data type of the attribute. This is really a value taken from
	// an aID, but squeezed down to a u16.
	u16 Type;

	// Pointer to the name of the attribute.
	const char *Name;

	mutable int NumChoices;
	mutable const char **ChoiceArray;
	mutable const u32 *ValueMap;
};


// The macros below are intended to be used to declare the attributes of
// a struct, like in this example:
//
//	class aFoo : public aFooSuper
//	{
//	public:
//		static void InitClass(void)
//		{
//			BEGIN_ATTRIBUTES(aFoo);
//			INHERIT_ATTRIBUTES(aFooSuper);
//			ATTRIBUTE(Foo);
//			ATTRIBUTE(Zorp);
//			END_ATTRIBUTES(aFoo);
//		}
//	protected:
//		int Foo;
//		float Zorp;
//	};
//
// aFoo::InitClass() should then only be called once.

#define BEGIN_ATTRIBUTES(structName)					\
		structName *base = NULL;						\
		const ::rage::aID &id = ID(structName);	\
		XMLDATAMGR.AddStruct(id, #structName);\
		Assert(id >= 0)

#define INHERIT_ATTRIBUTES(superClass)				\
		Assert(id != ID(superClass));				\
		XMLDATAMGR.InheritAttributes(id, ID(superClass))

#define END_ATTRIBUTES(structName) base = base; Assert(id == ID(structName))

#define ATTRIBUTE(name) \
		XMLDATAMGR.AddAttribute(id, &base->name, #name, base, \
		XMLDATAMGR.GetAttributeClassID(base->name))

// This is supposed to be used when you have an attribute named
// m_'name but want an attribute called 'name for XML loading
// and widget purposes.
#define M_ATTRIBUTE(name) \
		XMLDATAMGR.AddAttribute(id, &base->m_##name, #name, base, \
		XMLDATAMGR.GetAttributeClassID(base->m_##name))

#if A_IGNORE_ATTRIBUTES
#define IGNORE_ATTRIBUTE(name) XMLDATAMGR.IgnoreAttribute(id, #name)
#else
#define IGNORE_ATTRIBUTE(name)
#endif

#define SUB_ATTRIBUTE(subcomp, name) \
		XMLDATAMGR.AddAttribute(id, &base->subcomp.name, \
		#subcomp"_"#name, base, XMLDATAMGR.GetAttributeClassID(base->subcomp.name))

// Use this when you have an attribute named m_'subcomp, with a member named
// m_'name' but want an attribute called 'subcomp'_'name' for XML loading and
// widget purposes.
#define M_SUB_ATTRIBUTE(subcomp, name) \
		XMLDATAMGR.AddAttribute(id, &base->m_##subcomp.m_##name, \
		#subcomp"_"#name, base, XMLDATAMGR.GetAttributeClassID(base->m_##subcomp.m_##name))

#define SUB_ATTRIBUTE2(subcomp, xmlprefix, name) \
		XMLDATAMGR.AddAttribute(id, &base->subcomp.name, \
		#xmlprefix"_"#name, base, XMLDATAMGR.GetAttributeClassID(base->subcomp.name))

// Pointer to a function creating a subclass of aDataType.
typedef aDataType *(*aCreateDataTypeFunc)(void);

typedef void (*aResourceFixupFunc)(datResource &rsc, aDataType *obj, const aAttribute &attrib);

struct aDataTypeInfo
{
	int ClassID;
	aCreateDataTypeFunc CreationFunc;
	aResourceFixupFunc FixupFunc;
	/* const char* name */
};


namespace aDataLimits
{
	enum
	{
		// Maximum number of subclasses of aDataStruct.
		MAX_NUM_STRUCTS = 256,

		// Maximum number of data types for attributes.
		MAX_NUM_ATTRIBUTE_CLASSES = 20
	};
};

// PURPOSE: aDataStruct is a struct of data containing a number of attributes that
// have been registered so that they can be loaded, saved and edited
// automatically. 
// NOTES:
// This is the base class for aComponentType, but it can
// also be used for other structs if needed, like for subcomponents
// (objects used by an actor component).
// <FLAG Component>
class aDataStruct : public datBase
{
public:
	aDataStruct(void) : StructID(0xffff)
	{
#if __BANK
		Attributes = NULL;
		NumAttributes = 0;
		IgnoreValueConstraints = false;
#endif
	}

	aDataStruct(datResource &rsc);

	void Init(gdfTypeID gdfID);
	void Init(int structID);
	void Shutdown(void);

	// PURPOSE: Copy attribute values from a parent object, which should be of the
	// same class as this object.
	void CopyFromParent(const aDataStruct &parent);

#if __BANK
	void UpdateDefaultFlags(const aDataStruct &parent, bool init);
	virtual void UpdateFromParent(const aDataStruct &parent);
#endif

#if __BANK
	virtual void AddWidgets(bkBank &bank, const aDataStruct *parent = NULL,
			bool useGroups = true);
	void AddWidgetsCB(bkBank &bank);
	virtual void RemoveWidgets() {}
#endif

	// virtual void Reset(void);

	void LoadXML(xmlAsciiTokenizerXml &tok, const char *name = NULL,
			const aDataStruct *parent = NULL);
	virtual void LoadContents(xmlAsciiTokenizerXml&) {}

	// This gets called after attributes (and contents if applicable) are 
	// read in. Overriding this (eg, for attribute-dependent initialization 
	// is better than overriding LoadXML() to keep parsing separate.
	virtual void AfterLoad(void)
	{	}

#if __BANK
	// This gets called before attributes are saved back. Overriding this
	// instead of SaveXML() for example will make it easier to share code
	// between multiple data formats (binary, XML etc).
	virtual void BeforeSave(void) const
	{	}

	void SaveXML(xmlAsciiTokenizerXml &tok, int indent,
			const aDataStruct *parent = 0) const;

	virtual void SaveContents(xmlAsciiTokenizerXml& /*tok*/, int /*indent*/, 
		const aDataStruct* /*parent*/ = 0) const {}
#endif

	int GetStructID(void) const
	{	return (int)StructID;	}

protected:
#if __BANK
	// Array of attribute meta-data (min values, max values, ...). This is
	// only needed in __BANK builds.
	aDataType **Attributes;

	// Number of elements in Attributes[].
	int NumAttributes;

	// Set to true to bypass Assert()'s on Min/Max/Step/MaxLength/etc when loading
	// (Useful for aDataStruct's that don't load in a defaults file before being used.)
	bool IgnoreValueConstraints;
	void SetIgnoreValueConstraints(bool b) { IgnoreValueConstraints = b; }
#endif

	// ID for this struct. An aID squeezed down to 16 bits.
	u16 StructID;

};

// PURPOSE: aDataStructManager holds a number of aDataStruct structure definitions.
// NOTES:
// Objects that inherit from aDataStruct will register their attribute lists with an
// aDataStruct manager, and then when they need to be loaded in, will look up their
// class ID in the data struct manager to get the list of attributes.
// Normally this class acts as a singleton (accessable with the XMLDATAMGR macro)
// but could be instantiated multiple times, once per subsystem, so for example the
// vehicle subsystem would be prevented from loading AI data.
class aDataStructManager {
	CURRENT_INTERFACE(aDataStructManager);
public:
	aDataStructManager();

	// PURPOSE: Call this to start initializing the class. Call AddAttributeClass(),
	// AddStruct(), AddAttribute() and InheritAttributes() as needed,
	// and then call EndInitClass() when finished.
	void BeginInitClass(void);

	// PURPOSE: Call when done adding initializing the class, before creating
	// aDataStruct objects.
	void EndInitClass(void);

	// PURPOSE: ShutdownClass() undoes the effects of BeginInitClass()/
	// AddAttributeClass()/EndInitClass().
	void ShutdownClass(void);

	// Add an attribute class (or data type). Use like
	//	aDataStruct::AddAttributeClass(ID(float), aFloat::CreateInstance);
	void AddAttributeClass(const aID &attributeClassID,
			aCreateDataTypeFunc createFunc,
			aResourceFixupFunc fixupFunc, const char *className);

	void PrintAttributes(const aID &structID);

	// Returns true if id is a valid attribute class ID.
	bool IsValidAttributeClassID(const aID &id);

	// Returns true if id is a valid ID for a aDataStruct subclass.
	bool IsValidStructID(int id);

	void SetIgnoreInitializationOrder(bool b) { m_IgnoreInitializationOrder = b; }

	// This function returns a pointer to a function that instantiates
	// an aDataType object of the type determined by type.
	aCreateDataTypeFunc GetCreateAttributeFunc(int type)
	{	FastAssert(type >= 0 && type < m_NumAttributeClasses);
		return m_CreateAttributeFuncs[type];
	}

	aResourceFixupFunc GetResourceFixupFunc(int type)
	{
		FastAssert(type >= 0 && type < m_NumAttributeClasses);
		return m_ResourceFixupFuncs[type];
	}

	u32 GetAttributeNameChecksum()
	{	return m_AttributeNameChecksum;	}

	int GetNumStructs(void)
	{	return (int)m_NumStructs;	}

	void StartTracking(void);
	void EndTracking(void);

	// Set up the ID for a new struct. Protected because it's intended to be
	// called from a subclass through the BEGIN_ATTRIBUTES() macro.
	// (Unprotected so that testers that do not initialize all of the subclasses
	// enumerated can still get their structID's synchronized by looping through
	// unused numbers and calling AddStruct(). - WillH 10/27/03)
	void AddStruct(const aID &structID, const char *className);

	// Add an attribute for the struct with the specified ID. attribute should
	// point to an attribute value in the struct pointed to by base. name is
	// the name of the attribute and attributeClassID determines the data type
	// of the attribute. Normally called through the ATTRIBUTE() macro.
	int AddAttribute(const aID &structID, const void *attribute,
			const char *name, aDataStruct *base,
			const aID &attributeClassID);

#if A_IGNORE_ATTRIBUTES
	void IgnoreAttribute(const aID &structID, const char *name);
#endif

	// Inherit all attributes from a base class. structID is the ID for the
	// struct to which the attributes should be added, and parentID is the
	// ID for the base class.
	void InheritAttributes(const aID &structID, const aID &parentID);

	// Returns true if between BeginInitClass() and EndInitClass() calls.
	bool InInitPhase(void)
	{	return (m_Flags & SMFLAG_INITPHASE) != 0;	}

	// Used by the ATTRIBUTE() macro to get the ID of the attribute class
	// from its C++ type.
	template<class T> inline const aID &GetAttributeClassID(T&)
	{	return aClassID<T>::sm_ClassID;	}

	enum
	{
		// Set between calls to BeginInitClass() and EndInitClass().
		SMFLAG_INITPHASE = BIT0,

		// Set between calls to EndInitClass() and ShutdownClass().
		SMFLAG_INITIALIZED = BIT1
	};

	aAttributes& GetAttributes(aID classID) {
		return m_StructAttributes[(int)classID];
	}

protected:

	// This is used for detecting code changes in component (or other XML data)
	// attributes, so it can be determined whether existing resource/paging
	// files can be used or need to be rebuilt.
	u32 m_AttributeNameChecksum;

	// Static flags to keep track of initialization status.
	u32 m_Flags;

	// Current number of registered subclasses of aDataStruct.
	int m_NumStructs;

	// Allows you to call AddStruct on subclasses in any order
	// (which allows using only aDataStruct's that a particular tester needs)
	bool m_IgnoreInitializationOrder;

	// Array of attributes for each aDataStruct subclass.
	aAttributes m_StructAttributes[aDataLimits::MAX_NUM_STRUCTS];

	// Number of attribute classes.
	int m_NumAttributeClasses;

	// Pointers to functions that instantiate the attribute classes.
	aCreateDataTypeFunc m_CreateAttributeFuncs[aDataLimits::MAX_NUM_ATTRIBUTE_CLASSES];

	aResourceFixupFunc m_ResourceFixupFuncs[aDataLimits::MAX_NUM_ATTRIBUTE_CLASSES];
};

#define XMLDATAMGR (::rage::aDataStructManager::GetCurrent())

#define ATTRIBUTE_CLASS(datatype, qualifiedtype, createfunc, fixupfunc)					\
		::rage::aClassID<qualifiedtype>::sm_ClassID = ATTR_ID(datatype);					\
		::rage::aDataStructManager::GetCurrent().AddAttributeClass(ATTR_ID(datatype), createfunc, fixupfunc, #datatype)

}	// namespace rage

#endif

/* End of file xmldata/data.h */
