// 
// xmldata/datatypes.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef XMLDATA_DATATYPES_H
#define XMLDATA_DATATYPES_H

#ifndef CORE_BIT_H
#include "system/bit.h"
#endif

#ifndef DATA_BASE_H
#include "data/base.h"
#endif

#ifndef VECTOR_VECTOR3_H
#include "vector/vector3.h"
#endif

namespace rage {

class aAttribute;
class bkBank;
class xmlAsciiTokenizerXml;

#define A_USE_COMMENT 0


// PURPOSE: aString is a helper class that holds a pointer to a char* and provides some
// useful operations on it.
class aString
{
public:
	const char *Get(void) const
	{	if(String)
			return String;
		else
			return &smNullChar;
	}

	operator const char*() const
	{	return Get();	}

	aString(void) : String(NULL)
	{	}
	aString(const aString &v) : String(NULL)
	{	Set(v.Get());	}
	aString(const char *newString) : String(NULL)
	{	Set(newString);	}
	aString(datResource &rsc);

	void Free() 
	{
		// This check is here only to prevent calling of the delete
		// operator, that would cause warnings when building resource chunks.
		if(String)
			delete []String;
		String = NULL;
	}
	~aString()
	{	Free();	}

	void Insert( const char* s, int maxLength = -1, int pos = -1 ); // negative will append after last char
	void Insert( char c, int pos = -1 ); // negative will append after last char
	void Remove( int index = -1 );  // negative will delete last char

	int Length() const;
	
	const aString &operator=(const aString &v);

	bool HasSameValue(const aString &v) const;

	void Set(const char *newString, int maxLength = -1 );

	// This sets the 'String' attribute directly. Hope
	// you know what you are doing, cause this may screw up things
	// if you're not careful.
	void SetDirect(char *stringToPointTo)
	{	String = stringToPointTo;	}

protected:
	char *String;

	static const char smNullChar;
};

unsigned atHash( const aString& s ); // for use in atMap


// PURPOSE:
// Each attribute of a component belongs to a certain data type (integer,
// float, string, ...). In addition to the actual value of the attribute,
// it is also of interest to store information about how the value can
// be changed, for example its minimum and maximum values. aDataType is
// the base class for the classes storing this information.
// NOTES:
// aDataType does not contain the actual value of the attribute, but
// stores a pointer to it. In order to be useful, each subclass of
// aDataType should have a static function for instantiating an object
// of that class.
//
// In general, the contents of aDataType is only relevant in __BANK builds.
//<FLAG Component>
class aDataType : public datBase
{
public:
	aDataType(void) :
#if __BANK
		UpdateFrame(0),
#endif
		Value(NULL)
#if __BANK
		, Flags(0)
#endif
	{	}
	aDataType(datResource &rsc);

	// Subclasses should define an assignment operator. This is needed
	// when inheriting attributes from a parent object. Note that the
	// argument (v) must be of the same subclass of aDataType as *this.
	virtual const aDataType &operator=(const aDataType &BANK_ONLY(v))
	{
#if __BANK
		Flags = u16((Flags & (~FLAG_HAS_VALUE)) | (v.Flags & FLAG_HAS_VALUE));
#if A_USE_COMMENT
		Comment.Set(v.Comment.Get());
#endif
#endif
		return *this;
	}

	// PURPOSE: Returns true if this and v are of the same type and have the same value.
	virtual bool HasSameValue(const aDataType &v) const = 0;

	// PURPOSE: Sets the value to some arbitrary data. Subclasses determine how to interpret ptr.
	virtual void SetValue(void *ptr) = 0;

	// PURPOSE: Load from an XML file. name is the name of the attribute, which is
	// not stored in every object.
	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false)) = 0;

#if __BANK
	// PURPOSE: Save to an XML file. name is the name of the attribute, which is
	// not stored in every object. parent must be of the same type
	// as *this and is used to determine how much information needs to
	// be stored, i.e. is different from its parent attribute. indent
	// is the number of tabs to indent the attribute in the XML file.
	virtual void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
			int indent, const aDataType* parent) = 0;
#endif

#if __BANK
	// PURPOSE: Add a widget (or several in the case of vectors, for example) to
	// the specified bank.
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

	// PURPOSE: Set the pointer to the value of the attribute.
	void SetValuePtr(void *value)
	{	Value = value;	}

	// PURPOSE: Returns true if the value in this data struct is valid and in the proper range.
	virtual bool IsValid(void) const = 0;

#if __BANK
	bool GetHasValue(void) const
	{	return (Flags & FLAG_HAS_VALUE) != 0;	}

	void UpdateFromDefault(const aDataType *parent);
	void InitDefaultFlag(const aDataType *parent);
	void UpdateDefaultFlag(const aDataType *parent);

	u32 UpdateFrame;
	static u32 smFrameLastChanged;

#if A_USE_COMMENT
	bool ShouldSaveComment(const aDataType *parent) const;

	aString Comment;
#endif
#else
	bool GetHasValue(void) const
	{	return true;	}
#endif

protected:
	// Pointer to the value of this attribute. For example, for an aInt
	// this would point to an int and for aFloat to a float.
	void *Value;

	void SetHasValue(void)
	{
#if __BANK
		Flags |= FLAG_HAS_VALUE;
#endif
	}

#if __BANK
	u16 Flags;
	enum
	{	FLAG_HAS_VALUE = BIT0,
		FLAG_DEFAULT = BIT1
	};
#endif

	// Maximum number of characters in the XML tag attribute values.
	enum
	{	MAX_ATTR_LEN = 1024,
		MAX_ATTR_LEN_ENUM = 1024	// Replaces MAX_ATTR_LEN for the aEnum type.
	};
};

// PURPOSE: aInt contains information about an integer attribute.
// <FLAG Component>
class aInt : public aDataType
{
protected:
	// These are just for convenience. The integer that Value points to can
	// be internally accessed (read/write) through ValueRef().
	// these should inline nicely up here
	const int &ValueRef(void) const
	{	return *(int*)Value;	}
	int &ValueRef(void)
	{	return *(int*)Value;	}

public:
#if __BANK
	aInt(void) : MinValue(0), MaxValue(0), Step(-1)
	{	}
#else
	aInt(void)
	{	}
#endif
	aInt(datResource &rsc);

	// Instantiate an aInt object.
	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	// Assignment operator.
	const aDataType &operator=(const aDataType &v)
	{
		aDataType::operator=(v);
		const aInt &w = (aInt&)v;
		ValueRef() = w.ValueRef();
#if __BANK
		MinValue = w.MinValue;
		MaxValue = w.MaxValue;
		Step = w.Step;
#endif
		return *this;
	}

	bool HasSameValue(const aDataType &v) const
	{	return ValueRef() == ((aInt&)v).ValueRef();	}

	void SetValue(void *ptr)
	{	ValueRef() = *(int*)ptr;	}

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));
#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

	bool IsValid(void) const;

protected:

#if __BANK
	// Minimum value of the attribute.
	int MinValue;

	// Maximum value of the attribute.
	int MaxValue;

	// Step between possible attribute values.
	int Step;
#endif
};


// PURPOSE: aFloat contains information about a float attribute.
// <FLAG Component>
class aFloat : public aDataType
{
protected:
	// These are just for convenience. The float that Value points to can
	// be internally accessed (read/write) through ValueRef().
	// these should inline nicely up here
	const float &ValueRef(void) const
	{	return *(float*)Value;	}
	float &ValueRef(void)
	{	return *(float*)Value;	}

	// This is used for comparing two floats with some tolerance. Not ideal,
	// but better than a == b.
	static bool AreEqual(float a, float b)
	{	return fabsf(a - b) < 0.0001f;	}

public:
#if __BANK
	aFloat(void) : MinValue(0), MaxValue(0), Step(-1)
	{	}
#else
	aFloat(void)
	{	}
#endif

	aFloat(datResource &rsc);

	// Instantiate a new object.
	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	bool HasSameValue(const aDataType &v) const
	{	return AreEqual(ValueRef(), ((aFloat&)v).ValueRef());	}

	void SetValue(void *ptr)
	{	ValueRef() = *(float*)ptr;	}

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));
#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

	bool IsValid(void) const;

protected:

	// Assignment operator.
	const aDataType &operator=(const aDataType &v)
	{
		aDataType::operator=(v);
		const aFloat &w = (aFloat&)v;
		ValueRef() = w.ValueRef();
#if __BANK
		MinValue = w.MinValue;
		MaxValue = w.MaxValue;
		Step = w.Step;
#endif
		return *this;
	}

protected:
#if __BANK
	// Minimum value of this attribute.
	float MinValue;

	// Maximum value of this attribute.
	float MaxValue;

	// Step between attribute values.
	float Step;
#endif
};

// PURPOSE: aVector3 contains information about a Vector3 attribute.
// <FLAG Component>
class aVector3 : public aDataType
{
protected:
	// These should inline nicely up here
	// These are just for convenience. The Vector3 that Value points to can
	// be internally accessed (read/write) through ValueRef().
	const Vector3 &ValueRef(void) const
	{	return *(Vector3*)Value;	}
	Vector3 &ValueRef(void)
	{	return *(Vector3*)Value;	}

	// This is used for comparing two floats with some tolerance. Not ideal,
	// but better than a == b.
	static bool AreEqual(float a, float b)
	{	return fabsf(a - b) < 0.0001f;	}

	static bool AreEqual(const Vector3 &a, const Vector3 &b)
	{
		return AreEqual(a.x, b.x) && AreEqual(a.y, b.y) && AreEqual(a.z, b.z);
	}

public:
#if __BANK
	aVector3(void) : MinValue(0), MaxValue(0), Step(-1)
	{	}
#else
	aVector3(void)
	{	}
#endif

	aVector3(datResource &rsc);

	// Instantiate a new object.
	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	bool HasSameValue(const aDataType &v) const;

	void SetValue(void *ptr)
	{	ValueRef().Set(*(Vector3*)ptr);	}

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));
#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

	bool IsValid(void) const;

protected:

	// Assignment operator.
	const aDataType &operator=(const aDataType &v)
	{
		aDataType::operator=(v);
		const aVector3 &w = (aVector3&)v;
		ValueRef() = w.ValueRef();
#if __BANK
		MinValue = w.MinValue;
		MaxValue = w.MaxValue;
		Step = w.Step;
#endif
		return *this;
	}

protected:
#if __BANK
	// Minimum value of this attribute.
	float MinValue;

	// Maximum value of this attribute.
	float MaxValue;

	// Step between attribute values.
	float Step;
#endif
};

// PURPOSE: aEnum represents an individual enumerated value. It is the value used in an aEnumData attribute
class aEnum
{
public:
	aEnum(void) : Value(0)
	{	}
	aEnum(datResource &/*rsc*/)
	{	}
	aEnum(int value) : Value(value)
	{	}

	void Set(int value)
	{	Value = value;	}
	int Get(void) const
	{	return Value;	}

	bool operator==(const aEnum &e) const
	{
		return Value == e.Value;
	}

protected:
	friend class aEnumData;

	int Value;
};

// PURPOSE: aEnumData contains information about an enum attribute.
// <FLAG Component>
class aEnumData : public aDataType
{
protected:
	const aEnum &ValueRef(void) const
	{	return *(aEnum*)Value;	}
	aEnum &ValueRef(void)
	{	return *(aEnum*)Value;	}

public:
#if __BANK
	aEnumData(void) : NumChoices(0), Choices(NULL)
	{
	}

	~aEnumData();
#else
	aEnumData(void)
	{	}
#endif

	aEnumData(datResource &rsc, const aAttribute &attrib);

	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	bool HasSameValue(const aDataType &v) const
	{	return ValueRef().Get() == ((const aEnumData&)v).ValueRef().Get();	}

	void SetValue(void *ptr)
	{	ValueRef() = *(aEnum*)ptr;	}

	bool IsValid(void) const;

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));
#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

protected:
	// Assignment operator.
	const aDataType &operator=(const aDataType &v)
	{
		aDataType::operator=(v);
		const aEnumData &w = (const aEnumData&)v;
		ValueRef() = w.ValueRef();
#if __BANK
		Assert(!Choices);
		NumChoices = w.NumChoices;
//		Choices = CloneChoiceArray(w.Choices, w.NumChoices);
		Choices = w.Choices;
#endif
		return *this;
	}

	static const char **CreateChoiceArray(const char *commaSep, int &numChoicesOut);
	static void DestroyChoiceArray(const char **choices, int numChoices);
	static int StringToInt(const char *str, const char **choiceArray,
			int numChoices, const char *commaSep);
#if __BANK
//	static const char **CloneChoiceArray(const char **choice, int numChoices);
//	bool SameChoices(const aEnumData &v);
	static bool SameChoices(const char **choices1, int numChoices1,
			const char **choices2, int numChoices2);
	char *CreateChoiceString(void);
	void DestroyChoiceString(char *str)
	{	delete []str;	}
#endif

#if __BANK
	int NumChoices;
	const char **Choices;
#endif
};

// PURPOSE: aBitfield represents an individual bitfield. It is the value used in an aBitfieldData attribute
class aBitfield
{
	friend class aBitfieldData;
public:
	aBitfield(void) : Value(0)
	{	}
	aBitfield(datResource &/*rsc*/)
	{	}
	aBitfield(int value) : Value(value)
	{	}

	void Set(u32 value)
	{	Value = value;	}
	u32 Get(void) const
	{	return Value;	}

	operator u32()
	{
		return Value;
	}

protected:
	u32 Value;
};

// PURPOSE: aBitfieldData contains information about a bitfield attribute.
// NOTES:
// Assumes a max of 32 choices
// Assumes that the bitfield is 32 bits
// <FLAG Component>
class aBitfieldData : public aDataType
{

protected:
	const aBitfield & ValueRef() const
	{
		return *(const aBitfield *) Value;
	}
	aBitfield & ValueRef()
	{
		return *(aBitfield *) Value;
	}

public:
#if __BANK
	aBitfieldData(void) : NumChoices(0), ActiveChoices(0),
			Choices(0), ValueMap(0)
	{	}

	~aBitfieldData();
#else
	aBitfieldData(void)
	{	}
#endif

	aBitfieldData(datResource &rsc, const aAttribute &attrib);

	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	bool HasSameValue( const aDataType &that ) const
	{	return ValueRef().Get() == ((const aBitfieldData *) &that)->ValueRef().Get();	}

	void SetValue(void *ptr)
	{	ValueRef().Set(*(u32 *)ptr);	}

	bool IsValid(void) const
	{	return true;	}

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));

#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

protected:

	const aDataType &operator=( const aDataType &that)
	{	aDataType::operator=(that);
		const aBitfieldData &bitfield = (const aBitfieldData &)that;
		ValueRef() = bitfield.ValueRef();
#if __BANK
		Assert(!Choices);
		NumChoices = bitfield.NumChoices;
		ActiveChoices = bitfield.ActiveChoices;	// This wasn't here originally.
		Choices = bitfield.Choices;
		ValueMap = bitfield.ValueMap;
#endif
		return *this;
	}

	static const char **CreateChoiceArray(const char *commaSep, u32 ** valueMap, int &numChoicesOut);
	static void DestroyChoiceArray( const char ** choices, u32 * valueMap , int numChoices );
	static u32 StringToBitfield( const char * str, const char ** choiceArray,
		const u32 * valueMap, u32 &activeChoices, int numChoices, const char * commaSep );

#if __BANK
	char *CreateChoiceString() const;
	void DestroyChoiceString(char *str) const
	{	delete []str;	}
	char *CreateValueString(void) const;
	void DestroyValueString(char *str) const
	{	delete []str;	}

	void CalculateValueFromActiveChoices();

	static bool SameChoices(const char **choices1, const u32 * valueMap1, int numChoices1,
			const char **choices2, const u32 * valueMap2, int numChoices2);

	int					NumChoices;
	u32					ActiveChoices;
	const char **		Choices;
	const u32 *			ValueMap;
#endif
};


// PURPOSE: aStringData contains information about an string attribute.
// <FLAG Component>
class aStringData : public aDataType
{
protected:
	const aString &ValueRef(void) const
	{	return *(aString*)Value;	}
	aString &ValueRef(void)
	{	return *(aString*)Value;	}

public:
#if __BANK
	aStringData(void) : MaxLength(0)
	{
#if __BANK
		StringBuff = NULL;
#endif
	}
#else
	aStringData(void)
	{	}
#endif

#if __BANK
	~aStringData()
	{
		delete []StringBuff;
	}
#endif

	aStringData(datResource &rsc);

	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	bool HasSameValue(const aDataType &v) const
	{	return ValueRef().HasSameValue(((const aStringData&)v).ValueRef());	}

	void SetValue(void *ptr);

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));
#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	void StringChangedCB(const aDataType *parent);
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

	bool IsValid(void) const;

protected:
	// Assignment operator.
	const aDataType &operator=(const aDataType &v)
	{
		aDataType::operator=(v);
		const aStringData &w = (aStringData&)v;
#if __BANK
		ValueRef().Set( w.ValueRef(), w.MaxLength );
		MaxLength = w.MaxLength;
#else
		ValueRef() = w.ValueRef();
#endif
		return *this;
	}

#if __BANK
	int MaxLength;
#endif

#if __BANK
	char *StringBuff;
#endif
};

// PURPOSE: aBool contains information about an boolean attribute.
// <FLAG Component>
class aBool : public aDataType
{
protected:
	// these should inline nicely
	// These are just for convenience. The integer that Value points to can
	// be internally accessed (read/write) through ValueRef().
	const bool &ValueRef(void) const
	{	return *(bool*)Value;	}
	bool &ValueRef(void)
	{	return *(bool*)Value;	}
public:
	aBool(void)	{	}

	aBool(datResource &rsc);

	// Instantiate an aBool object.
	static aDataType *CreateInstance(void);

	static void ResourceFixup(datResource &rsc, aDataType *obj, const aAttribute &attrib);

	// Assignment operator.
	const aDataType &operator=(const aDataType &v)
	{
		aDataType::operator=(v);
		const aBool &w = (aBool&)v;
		ValueRef() = w.ValueRef();
		return *this;
	}

	bool HasSameValue(const aDataType &v) const
	{	return ValueRef() == ((aBool&)v).ValueRef();	}

	void SetValue(void *ptr)
	{	ValueRef() = *(bool*)ptr;	}

	virtual void LoadXML(const char *name, xmlAsciiTokenizerXml &tok,
			const aAttribute &attrib, const aDataType *parent, bool BANK_ONLY(ignoreConstraints=false));
#if __BANK
	void SaveXML(const char* name, xmlAsciiTokenizerXml &tok,
		int indent, const aDataType* parent);
#endif

#if __BANK
	virtual void AddWidget(const char* name, bkBank &bank, const aDataType* parent);
#endif

	bool IsValid(void) const { return true; }

protected:
};

///////////////////////////////////////////////////////////////////////////////

#define ATTRIBUTE_CLASS_LIST										\
	ITEM(float,		float,					::rage::aFloat::CreateInstance,			::rage::aFloat::ResourceFixup)			\
	ITEM(int,		int,					::rage::aInt::CreateInstance,			::rage::aInt::ResourceFixup)			\
	ITEM(bool,		bool,					::rage::aBool::CreateInstance,			::rage::aBool::ResourceFixup)			\
	ITEM(aString,	::rage::aString,		::rage::aStringData::CreateInstance,	::rage::aStringData::ResourceFixup)		\
	ITEM(Vector3,	::rage::Vector3,		::rage::aVector3::CreateInstance,		::rage::aVector3::ResourceFixup)		\
	ITEM(aEnum,		::rage::aEnum,			::rage::aEnumData::CreateInstance,		::rage::aEnumData::ResourceFixup)		\
	ITEM(aBitfield,	::rage::aBitfield,		::rage::aBitfieldData::CreateInstance,	::rage::aBitfieldData::ResourceFixup)

// Generate static ID numbers for the attributes. Expands to
// enum
// {
//     ID_float,
//     ID_int,
//     ...
// };

enum
{
#define ITEM(a, b, c, d) ID_##a,
	ATTRIBUTE_CLASS_LIST
#undef ITEM

	ATTRIBUTE_CLASSES_NUM
};

}	// namespace rage

#endif

/* End of file xmldata/datatypes.h */
