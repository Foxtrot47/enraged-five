// 
// xmldata/id.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef XMLDATA_ID_H
#define XMLDATA_ID_H

#include "idmgr/idmgr.h"

namespace rage {

typedef int aID;


struct aClassIDCounter
{
	static aID sm_ClassIDCounter;
};

// PURPOSE:
// aClassID is an all static template. Its purpose is to store an index
// of a message class or component class. This is very powerful, because
// it allows the index to be instantiated only when needed, so a tester
// can use only the classes it needs and doesn't need to be linked with
// anything else. There is not any need for a global list of enums or
// any strings at all.
//<FLAG Component>
template<class T> class aClassID
{
public:
	// PURPOSE:
	// This ID number uniquely identifies a class of messages or components.
	// NOTES:
	// In the current implementation it is used as an index into internal
	// arrays in aActor.
	// To be able to use the ID() macro for both reading and writing, it
	// is not protected here, but aID objects cannot be changed except
	// for by the appropriate functions in aActor.
	static aID sm_ClassID;

private:
	aClassID<T>(void)
	{	}
};

template<class T> aID aClassID<T>::sm_ClassID = ++aClassIDCounter::sm_ClassIDCounter;

// PURPOSE: This macro returns the aID identifier associated with the className
#define CLASSID(className) ::rage::aClassID<className>::sm_ClassID

// PURPOSE: For backward compatability
#define ATTR_ID(className) ID_##className

// PURPOSE: This can be used to get the class identifier for a message or
// component class. Using the ID() macro, aClassID is abstracted
// away from the user.
#define ID(className) (::rage::s32)IDMGR_GET_CLASSID(className).GetIDInNamespace()

// PURPOSE: Returns the GUID associated with the named class
#define CLASSGUID(className) (::rage::s32)IDMGR_GET_CLASSID(className).GetGUID()

}	// namespace rage
#endif

/* End of file xmldata/id.h */
