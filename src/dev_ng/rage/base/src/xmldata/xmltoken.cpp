//
// xmldata/xmltoken.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __FINAL
#ifdef __GNUC__
#define ERR(args...)
#else
#define ERR
#endif
#else
#define ERR printf
#endif


#include <stdarg.h>

#include "file/stream.h"

#include "string/string.h"
#include "xmldata/xmltoken.h"
#include "vector/vector3.h"


using namespace rage;

void xmlAsciiTokenizerXml::Init( const char * name, fiStream *s, bool preload)
{
	// Preload the stream so that the seeks don't kill our loading times 
	// Initial test - dropped the load time from 4.838 seconds to 42ms
	if(preload) s = fiStream::PreLoad( s );
	fiAsciiTokenizer::Init( name, s );
}


int xmlAsciiTokenizerXml::GetToken(char *dest,int maxlen)
{
	// skip whitespace
	while (iswhitespace(nextch)) {
		if (nextch == 10)
			++line;
		nextch = S->FastGetCh();
	}

	int stored = 0;
	// store characters until next whitespace
	switch (nextch)
	{
		case '"':
			nextch = S->FastGetCh();
			while (nextch != -1 && nextch != '"') {
				if (stored < maxlen-1)
					dest[stored++] = (char) nextch;
				nextch = S->FastGetCh();
			}
			nextch = S->FastGetCh();	// consume closing quote.
			break;

		case '<':
			{
				dest[stored++] = (char) nextch;
				int loc = S->Tell();
				nextch = S->FastGetCh();	// let's see what the next character is
				switch (nextch)
				{
					case '!':
						dest[stored++] = (char) nextch;
						loc = S->Tell();
						if (S->FastGetCh() == '-' && S->FastGetCh() == '-')
						{
							// the token is "<!--" (xml comment)
							// we now want to skip the rest of the comment
							int ch[3];
							ch[0] = S->FastGetCh();
							ch[1] = S->FastGetCh();
							ch[2] = S->FastGetCh();
							while (ch[0] != '-' || ch[1] != '-' || ch[2] != '>')
							{
								ch[0] = ch[1];
								ch[1] = ch[2];
								ch[2] = S->FastGetCh();
							}
							nextch = S->FastGetCh();
							GetToken(dest,maxlen);
						}
						else
						{
							// the token is "<!" (probably followed by "DOCTYPE")
							S->Seek(loc);
							nextch = S->FastGetCh();
						}
						break;
					case '?':	// token = "<?" (xml declaration)
					case '/':	// token = "</" (end tag)
						dest[stored++] = (char) nextch;
						nextch = S->FastGetCh();
						break;
				}
			}
			break;

		case '?':
		case '/':
			dest[stored++] = (char) nextch;
			nextch = S->FastGetCh();
			if (nextch == '>')
			{
				dest[stored++] = (char ) nextch;	// token = "/>" (empty tag) or "?>" (end of xml declaration)
				nextch = S->FastGetCh();
			}
			break;
		
		case '-':
			{
				dest[stored++] = (char) nextch;
				int loc = S->Tell();
				if (S->FastGetCh() == '-' && S->FastGetCh() == '>')
				{
					// the token is "-->" (end of xml comment)
					dest[stored++] = '-';
					dest[stored++] = '>';
				}
				else
				{
					// the token is "-" (random token)
					S->Seek(loc);
				}
				nextch = S->FastGetCh();
			}
			break;

		case '>':
		case '=':
			dest[stored++] = (char) nextch;
			nextch = S->FastGetCh();	// go to the next character
			break;

		default:
			while (nextch!=-1 && !iswhitespace(nextch) && nextch != '=' && nextch != '/' && nextch != '?' && nextch != '>') {
					// see if we found a comment, read until the end of line:
					while (nextch==CommentChar)
					{
						SkipComment();
						// if we started the token, then we return what we've found...
						if (stored!=0)
							break;
						// ...otherwise, we skip over all whitespace until we get 
						// a valid token char:
						else
							while (iswhitespace(nextch))
								nextch = S->FastGetCh();
					}

					// handle case where comment is line line of file
					if (nextch != -1 && stored < maxlen-1)
						dest[stored++] = (char) nextch;
					nextch = S->FastGetCh();
			}
			break;
	}

	dest[stored] = 0;
	return stored;
}


void xmlAsciiTokenizerXml::MatchToken(const char *match) {
	char buf[64];
	buf[0] = 0;
	if (!GetToken(buf,sizeof(buf)) || strcmp(match, buf))
	{
#if !__FINAL
		Quitf("%s(%d): Expected '%s', got '%s'.",filename,line,match,buf);
#endif
	}
}


void xmlAsciiTokenizerXml::MatchIToken(const char *match) {
	char buf[64];
	buf[0] = 0;
	if (!GetToken(buf,sizeof(buf)) || stricmp(match, buf))
	{
#if !__FINAL
		Quitf("%s(%d): Expected '%s', got '%s'.",filename,line,match,buf);
#endif
	}
}


void xmlAsciiTokenizerXml::GetDelimiter(const char* de)
{
	MatchToken(de);
}


bool xmlAsciiTokenizerXml::CheckToken(const char *check)
{
	char buf[MAX_CHECK_TOKEN_SIZE];
	int loc = S->Tell();
	int oldnextch = nextch;

	buf[0] = 0;
	if (GetToken(buf,sizeof(buf)) == 0 || strcmp(check, buf))
	{
		S->Seek(loc);
		nextch = oldnextch;
		return false;
	}

	return true;
}


bool xmlAsciiTokenizerXml::CheckIToken(const char *check)
{
	char buf[MAX_CHECK_TOKEN_SIZE];
	int loc = S->Tell();
	int oldnextch = nextch;

	buf[0] = 0;
	if (GetToken(buf,sizeof(buf)) == 0 || stricmp(check, buf))
	{
		S->Seek(loc);
		nextch = oldnextch;
		return false;
	}

	return true;
}


void xmlAsciiTokenizerXml::GetTag(const char *tag, bool empty)
{
	GetDelimiter("<");
	GetDelimiter(tag);
	if (empty)
		GetDelimiter("/>");
	else
		GetDelimiter(">");
}


void xmlAsciiTokenizerXml::GetEndTag(const char *tag)
{
	GetDelimiter("</");
	GetDelimiter(tag);
	GetDelimiter(">");
}

bool xmlAsciiTokenizerXml::CheckTagName(const char *tag)
{
	int loc = S->Tell();
	int oldnextch = nextch;
	bool result = CheckToken("<") && CheckToken(tag);
	S->Seek(loc);
	nextch = oldnextch;
	return result;
}

int xmlAsciiTokenizerXml::GetTagName(char *dest, int maxlen)
{
	int loc = S->Tell();
	int oldnextch = nextch;
	int len = CheckToken("<") ? GetToken(dest,maxlen) : 0;
	S->Seek(loc);
	nextch = oldnextch;
	return len;
}

bool xmlAsciiTokenizerXml::CheckTag(const char *tag, bool empty, bool consume)
{
	int loc = S->Tell();
	int oldnextch = nextch;
	bool result = CheckToken("<") && CheckToken(tag) && ((empty == false && CheckToken(">")) || (empty && CheckToken("/>")));
	if (result == false || !consume)
	{
		S->Seek(loc);
		nextch = oldnextch;
	}
	return result;
}


bool xmlAsciiTokenizerXml::CheckEndTag(const char *tag, bool consume)
{
	int loc = S->Tell();
	int oldnextch = nextch;
	bool result = CheckToken("</") && CheckToken(tag) && CheckToken(">");
	if (result == false || !consume)
	{
		S->Seek(loc);
		nextch = oldnextch;
	}
	return result;
}


void xmlAsciiTokenizerXml::GetTag(const char *tag, const char *attributeNames[], int maxlen, char *attributeValues[], bool empty,bool consume)
{
	int loc = S->Tell();
	int oldnextch = nextch;
	char skip[MAX_ATTRIBUTE_NAME_LENGTH];

	GetDelimiter("<");
	GetDelimiter(tag);

	char buf[MAX_ATTRIBUTE_NAME_LENGTH];

	const char *emptytag = "/>";
	const char *nonemptytag = ">";

	const char *endtag = empty ? emptytag : nonemptytag;

	while (CheckToken(endtag) == false)
	{
		int index;

		GetToken(buf,MAX_ATTRIBUTE_NAME_LENGTH);

		for (index = 0; attributeNames[index][0] != '\0' && strcmp(buf,attributeNames[index]) != 0; index++);

		if (attributeNames[index][0] == '\0')
		{
			//skip the attribute. we are not concerned with it now
			GetDelimiter("=");
			GetToken(skip,MAX_ATTRIBUTE_NAME_LENGTH);
		}
		else
		{
			//i am assuming that you are getting a subset of attributes and will be doing by calling multiple GetTags
			//1 time for each subset.  The union of these will be all the available attributes. So, I don't want to 
			//assert since i am meaning to only get a subset.
			if (consume)
			{
				AssertMsg(attributeNames[index][0] != '\0' , "xmlAsciiTokenizerXml::GetTag(): unrecognized attribute");
			}
			GetDelimiter("=");
			GetToken(attributeValues[index],maxlen);
		}
	}

	if (!consume)
	{
		S->Seek(loc);
		nextch = oldnextch;
	}
}

void xmlAsciiTokenizerXml::GetTagAttributes(const char *tag, char *attributeNames[], int maxlen, char *attributeValues[] , int maxAttributes, bool empty)
{
	int loc = S->Tell();
	int oldnextch = nextch;

	GetDelimiter("<");
	GetDelimiter(tag);

	const char *emptytag = "/>";
	const char *nonemptytag = ">";

	const char *endtag = empty ? emptytag : nonemptytag;
	int index = 0;

	while (CheckToken(endtag) == false && index < maxAttributes)
	{		
		GetToken(attributeNames[index],maxlen);
		GetDelimiter("=");
		GetToken(attributeValues[index],maxlen);
		index++;
	}

	//reset the fp to before this func was called
	S->Seek(loc);
	nextch = oldnextch;
}
bool xmlAsciiTokenizerXml::CheckTag(const char *tag, const char *attributeNames[], int maxlen, char *attributeValues[], bool empty)
{
	int loc = S->Tell();
	int oldnextch = nextch;
	if (CheckToken("<") == false || CheckToken(tag) == false)
	{
		S->Seek(loc);
		nextch = oldnextch;
		return false;
	}

	char buf[MAX_ATTRIBUTE_NAME_LENGTH];

	const char *emptytag = "/>";
	const char *nonemptytag = ">";

	const char *endtag = empty ? emptytag : nonemptytag;
	const char *notendtag = empty ? nonemptytag : emptytag;

	while (CheckToken(endtag) == false)
	{
		if (CheckToken(notendtag))
		{
			S->Seek(loc);
			nextch = oldnextch;
			return false;
		}

		int index;

		GetToken(buf,MAX_ATTRIBUTE_NAME_LENGTH);

		for (index = 0; attributeNames[index][0] != '\0' && strcmp(buf,attributeNames[index]) != 0; index++);

		AssertMsg(attributeNames[index][0] != '\0' , "xmlAsciiTokenizerXml::GetTag(): unrecognized attribute");
		GetToken(buf,1);
		AssertMsg(buf[0] == '=' , "xmlAsciiTokenizerXml::GetTag(): bad attribute format");
		GetToken(attributeValues[index],maxlen);
	}

	return true;
}

void xmlAsciiTokenizerXml::PopTag()
{
	AssertVerify(CheckToken("<") || CheckToken("</"));
	while(!(CheckToken(">") || CheckToken("/>")))
	{
		char buf[40];
		GetToken(buf,sizeof(buf));
	}
}

void xmlAsciiTokenizerXml::PopXMLDeclaration()
{
	if(!CheckToken("<?")) return;
	GetDelimiter("xml");
	GetDelimiter("version");
	GetDelimiter("=");
	GetDelimiter("1.0");
	GetDelimiter("encoding");
	GetDelimiter("=");
	char buf[40];
	GetToken(buf,sizeof(buf));
	GetDelimiter("?>");
}


void xmlAsciiTokenizerXml::PopDOCTYPE(char *firsttag, int maxlen, const char *dtdname)
{
	if(!CheckToken("<!"))
	{
		*firsttag=0;
		return;
	}
	GetDelimiter("DOCTYPE");
	GetToken(firsttag,maxlen);
	if(CheckToken("SYSTEM"))
	{
		char path[300];
		GetToken(path,sizeof(path));

		if(dtdname)
		{
			char *actual;
			int len;
			int dtdlen = (int)strlen(dtdname);

			len = (int)strlen(path);
			actual = &path[len-(dtdlen+4)];
			if (strncmp(actual,dtdname,dtdlen) != 0)
				Quitf("xmlAsciiTokenizerXml::PopDOCTYPE(): unknown DTD type - expected '%s.dtd', found '%s'.",dtdname,actual);
		}
	}
	else if (dtdname)
	{
		Quitf("xmlAsciiTokenizerXml::PopDOCTYPE(): unknown DTD type - expected '%s.dtd', none found.",dtdname);
	}


	GetDelimiter(">");
}

void xmlAsciiTokenizerXml::PutComment(const char *comment)
{
	Put("<!--"); Put(comment); Put("-->\r\n");
}

void xmlAsciiTokenizerXml::PutXMLDeclaration()
{
	Put("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
}


void xmlAsciiTokenizerXml::PutDOCTYPE(const char *firsttag, const char *dtdname)
{
	PutStr("<!DOCTYPE %s",firsttag);
	if(dtdname)
		PutStr(" SYSTEM \"%s\"",dtdname);
	PutStr(">\r\n");
}


void xmlAsciiTokenizerXml::PutTagStart(const char *tag, int tabs, bool closeImmediately, bool empty)
{
	if (tabs)
		for(int i = 0; i < tabs; i++)
			PutStr("\t");
	PutStr("<%s",tag);
	if (closeImmediately)
		PutTagEnd(empty);
}


void xmlAsciiTokenizerXml::PutTagEnd(bool empty)
{
	if (empty == false)
		Put(">\r\n");
	else
		Put("/>\r\n");
}


void xmlAsciiTokenizerXml::PutAttribute(const char *attname, const char *attvalue)
{
	PutStr(" %s=\"%s\"",attname,attvalue);
}


void xmlAsciiTokenizerXml::PutAttribute(const char *attname, int attvalue)
{
	PutStr(" %s=\"%d\"",attname,attvalue);
}


void xmlAsciiTokenizerXml::PutAttribute(const char *attname, char attvalue)
{
	PutStr(" %s=\"%c\"",attname,attvalue);
}


void xmlAsciiTokenizerXml::PutAttribute(const char *attname, float attvalue)
{
	PutStr(" %s=\"%3.3f\"",attname,attvalue);
}


void xmlAsciiTokenizerXml::PutAttribute(const char *attname, bool attvalue, bool asString)
{
	if (asString)
		PutStr(" %s=\"%s\"",attname,attvalue?"TRUE":"FALSE");
	else
		PutStr(" %s=\"%d\"",attname,(int)attvalue);
}


void xmlAsciiTokenizerXml::PutAttribute(const char *attname, const Vector3 &attvalue)
{
	PutStr(" %s=\"%3.3f %3.3f %3.3f\"",attname,attvalue.x,attvalue.y,attvalue.z);
}


void xmlAsciiTokenizerXml::PutEndTag(const char *tag, int tabs)
{
	if (tabs)
		for(int i = 0; i < tabs; i++)
			PutStr("\t");
	PutStr("</%s>\r\n",tag);
}



// ParseAssert can be used to report script errors to the script writer.
// Use as ParseAssert(ASST_EXPR(expr), format, ...). If expr == FALSE it will report
// the script name and line number, the source code name and line number,
// the expression and a custom error message (specified by format and the
// following arguments).

void xmlAsciiTokenizerXml::ParseAssert(int result, const char *expr,
		const char *file, int line, char *fmt, ...)
{
	static char buff[256];
	static char buff2[256];

	if (result)
		return;

	va_list args;
	va_start(args, fmt);

	// This is not very nice, but I couldn't find a line number counter in
	// the parser or the stream. If GetLine() in the base class was virtual
	// it could have been overridden to increase a line counter.
	fiStream *s = GetStream();
	int errPos = s->Tell();
	s->Seek(0);
	int currentLine = 1;
	for(int pos = 0; pos < errPos; pos++)
		if(s->GetCh() == '\n')
			currentLine++;
	s->Seek(errPos);

	formatf(buff, sizeof(buff), "%s.xml, line %d: %s\r\nExpression '%s' in line %d of '%s'",
			GetName(), currentLine, fmt, expr, line, file);
	vsprintf(buff2, buff, args);
	va_end(args);

	Quitf(buff2);
}

/*
PURPOSE
Return the tokens.
PARAMS
dest - where the tokens saved.
maxLen - maximum tokens to get.
terminator - get tokens until reach this token.
RETURNS
the number of tokens returned.
NOTES
*/
int xmlAsciiTokenizerXml::GetTokenToCharFixed(char *dest,int maxLen,char terminator,bool stripWhiteSpace, bool ignoreCommentChar, bool consume)
{
	int loc = this->S->Tell();
	int oldnextch = this->nextch;

	bool start = true;
	int stored = 0, i;
	nextch = 0;
	while (stored < (maxLen-1))
	{
		nextch = GetTokenCh();
		if ((nextch==CommentChar) && !ignoreCommentChar)
			SkipComment();
		if (nextch == -1 && stored == 0) stored = -1;
		if (nextch == -1 || nextch == terminator)	break;
		if (!stripWhiteSpace || !start || !iswhitespace(nextch))
		{
			start = false;
			dest[stored++] = (char) nextch;
		}
	}

	// Trim trailing whitespace
	for (i = stored - 1; i >= 0; i--)
	{
		if (iswhitespace(dest[i]))
			stored--;
		else
			break;
	}

	if (stored>=0)
		dest[stored] = 0;


	if(!consume)
	{
		this->S->Seek(loc);
		this->nextch = oldnextch;
	}

	return stored;
}

bool	xmlAsciiTokenizerXml::IsTagSingleLine()
{
	char szBuffer[128];
	int iLen = this->GetTokenToCharFixed(szBuffer,sizeof(szBuffer),'>', true, false, false);
	AssertMsg(iLen > 0 , "This doesn't look like a valid XML tag");
	return szBuffer[iLen - 1] == '/';
}
