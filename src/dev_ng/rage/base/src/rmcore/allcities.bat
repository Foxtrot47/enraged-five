@ECHO OFF
FOR %%I IN (sd detroit atlanta) DO (
	FOR %%J IN (midnight dawn dusk) DO (
			FOR %%K IN (clear cloudy rainy) DO (
				ECHO -city %%I -condition %%J_%%K
				win32_rscrelease\afrc -city %%I -condition %%J_%%K
			)
	)
)
