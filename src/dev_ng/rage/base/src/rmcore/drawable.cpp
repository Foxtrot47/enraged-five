//
// rmcore/drawable.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "drawable.h"
#include "grmodel/model.h"
#include "grcore/texture.h"
#include "grcore/texturedefault.h"
#include "grcore/instancebuffer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "file/stream.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/jointdata.h"
#include "crmetadata/properties.h"
#include "file/token.h"
#include "typefileparser.h"
#include "paging/dictionary.h"
#include "paging/rscbuilder.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"
#include "system/alloca.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "grblendshapes/manager.h"
#include "grblendshapes/blendshapes.h"
#include "grblendshapes/morphable.h"
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "diag/art_channel.h"

#include "grcore/effect_config.h"
#include "grcore/vertexBuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/model.h"
#include "grmodel/shaderfx.h"
#include "system/param.h"

#if __PPU
#	include "system/task.h"
#	include "drawablespu.h"
#elif RSG_ORBIS
#	include "grcore/gfxcontext_gnm.h"
#endif

using namespace rage;

// LODs to load.
int rmcDrawableBase::sm_LodLoadFilter = -1;

// Remap the loaded LOD to this value. (-1 = don't change).
int rmcDrawableBase::sm_LodForceRemap = -1;

// Only modify the number part here, not the addition.
const int rmcDrawable::RORC_VERSION = 84 + grcTexture::RORC_VERSION + crSkeletonData::RORC_VERSION + grmGeometry::RORC_VERSION 
	+ grmShader::RORC_VERSION + __64BIT;

rmcDrawableBase::rmcDrawableBase() : m_ShaderGroup(0) {
}


rmcDrawableBase::~rmcDrawableBase() {
}


void rmcDrawableBase::Delete() {
	delete this;
}

// PURPOSE: Only load certain LODs and skip the rest. This functionality is not thread-safe.
// All subsequent rmcDrawableBase-derived loads will only load the LODs specified here (IF
// the derived version of rmcDrawableBase supports this functionality).
//
// PARAMS:
//   lodBitMask - Bit mask of the LODs to load. All other LODs will be skipped.
//   -1 will re-enable all LODs.
void rmcDrawableBase::SetLodLoadFilter(int lodBitMask)
{
	sm_LodLoadFilter = lodBitMask;
}

#if __DEV
__THREAD const char* rmcDrawable::sm_DebugDrawableName = NULL;
#endif // __DEV

rmcDrawable::rmcDrawable() 
	: m_SkeletonData(0)
	, m_JointData(0)
	, m_HandleIndex(0)
	, m_ContainerSizeQW(0)
	, m_ContainerPtr(NULL)
	, m_DebugName(NULL)
{
}

void rmcDrawable::CloneWithNewVertexData( const rmcDrawable &cloneme, int lod, const atArray<int>& modelIdx, const atArray<int>& geomIdx, bool createVB, void* preAllocatedMemory, int& memoryAvailable )
{
	Assert( !m_ShaderGroup );
	m_ShaderGroup = rage_aligned_new(16) grmShaderGroup( cloneme.m_ShaderGroup );
	m_LodGroup.CloneWithNewVertexData( cloneme.GetLodGroup(), lod, modelIdx, geomIdx, createVB, preAllocatedMemory, memoryAvailable ); 
}

void rmcDrawable::Delete() {
	delete this;
}

rmcDrawable::~rmcDrawable() {
	delete[] m_DebugName;

#if ENABLE_DEFRAGMENTATION
	pgHandleBase2::Unregister(this);
#endif

	if (m_JointData)
	{
		m_JointData->Release();
		m_JointData = NULL;
	}

	if( m_SkeletonData )
	{
		m_SkeletonData->Release();
		m_SkeletonData = NULL;
	}

	if (m_ShaderGroup) {
#if !__TOOL
		sysMemContainerData temp;
		temp.m_Base =  m_ShaderGroup;
		temp.m_Size = m_ShaderGroup->GetContainerSize();
		sysMemContainer shaderContainer(temp);
		shaderContainer.BeginShutdown();
#endif
		delete m_ShaderGroup;
#if !__TOOL
		shaderContainer.EndShutdown();
#endif
	}

#if !__TOOL
	sysMemContainerData tmp; 
	tmp.m_Base = m_ContainerPtr;
	tmp.m_Size = m_ContainerSizeQW<<4;
	sysMemContainer container(tmp);
	container.BeginShutdown();
#endif

	// Don't call Shutdown() if we're inside a resource and being destroyed.
#if __XENON || __PS3
	if (sysMemAllowResourceAlloc == 0)
#endif // __XENON || __PS3
	{
		m_LodGroup.Shutdown();
	}

#if !__TOOL
	container.EndShutdown();
#endif
}

#if MESH_LIBRARY

bool rmcDrawable::Load(const char *basename,rmcTypeFileParser *parser,bool configParser) {
//	Stream *S = Stream::Open(basename);
	fiStream *S = ASSET.Open(basename, "type");
	if (S) {
		fiTokenizer T;
		T.Init(basename,S);
		bool result = Load(T, parser, configParser);
		S->Close();
		return result;
	}
	else
		return false;
}


bool rmcDrawable::Load(fiTokenizer &T,rmcTypeFileParser *parser,bool configParser) {
	char buf[128];

	// New formats may have a 'Version: XXX' at the top of the file, so far there is
	// only one version (102) so we're just going to make sure it is 102, then load
	// the new format.
	if (T.CheckToken("Version:",false))
	{
		T.GetToken(buf, sizeof(buf));
		int drawableVersion = T.GetInt();
		if ( 103 == drawableVersion )
			return LoadFromParser(T, parser, configParser);
		Errorf("Shader (%s) has unsupported version (%i)", T.GetName(), drawableVersion);
		return false;
	}
	else
	{
		Errorf("Couldn't find version info in file '%s' - incorrect format?", T.GetName());
	}
	return false;
}

namespace rage
{
	PARAM(nodrawablecontainer,"[rmcore] Disable containers for drawables (so large objects will load, but won't render on SPU)");
	PARAM(drawablecontainersize,"[rmcore] Set maximum container size (in kilobytes, default is 64, 0 is same as -nodrawablecontainer)");
}

bool rmcDrawable::LoadFromParser(fiTokenizer &T,rmcTypeFileParser *parser,bool configParser) {
	// Register types
	rmcTypeFileParser defaultParser;
	if ( !parser )
		parser = &defaultParser;

	// Register the defaults
	if ( configParser ) {
		parser->RegisterLoader( "shadinggroup", "shadinggroup", datCallback(MFA1(rmcDrawable::LoadShader), this, 0, true) );
		parser->RegisterLoader( "lodgroup", "mesh", datCallback(MFA1(rmcDrawable::LoadMesh), this, 0, true) );
		parser->RegisterLoader( "skel", "skel", datCallback(MFA1(rmcDrawable::LoadSkel), this, 0, true) );
		parser->RegisterLoader( "skel", "jlimits", datCallback(MFA1(rmcDrawable::LoadJointLimits), this, 0, true) );
		parser->RegisterLoader( "skel", "properties", datCallback(MFA1(rmcDrawable::LoadSkelProperties), this, 0, true) );
		parser->RegisterLoader( "edge", "edge", datCallback(MFA1(rmcDrawable::LoadEdgeModel), this, 0, true) );
		// parser->RegisterLoader( "blendshapes", "blendshape", datCallback(MFA1(rmcDrawable::LoadBlendShapes), this, 0, true));
	}

	return parser->ProcessTypeFile( T );
}

void rmcDrawable::LoadShader(rmcTypeFileCbData *data) {
	
	fiTokenizer *T = data->m_T;
	T->CheckToken("{");

#if !__TOOL
	sysMemContainerData temp;
	sysMemContainer container(temp);
	container.Init(32768);
#endif
	m_ShaderGroup = grmShaderFactory::GetInstance().LoadGroup(*T);
#if !__TOOL
	Assert(m_ShaderGroup == temp.m_Base);
	container.Finalize();
	Assert(temp.m_Size);
	m_ShaderGroup->SetContainerSize(temp.m_Size);
#endif
	
	T->MatchToken("}");
}	

void rmcDrawable::LoadMesh( rmcTypeFileCbData *data )
{
	// Taken from Load() code, need to copy it since we handle shader groups differently
	fiTokenizer *T = data->m_T;
	T->MatchToken("{");

	// If we don't abort now, we'll just end up crashing later in pointer relocation.
	if (m_LodGroup.GetCullRadius())
		Quitf("Multiple mesh sections in a drawable %s, please fix the exporter!",T->GetName());

	int containersize = 64;
	if (PARAM_drawablecontainersize.Get())
		PARAM_drawablecontainersize.Get(containersize);
	if (PARAM_nodrawablecontainer.Get() || !containersize)
	{
		m_LodGroup.Load(*T, m_ShaderGroup, sm_LodLoadFilter, sm_LodForceRemap);
	}
	else 
	{
#if !__TOOL
		sysMemContainerData tmp;
		sysMemContainer container(tmp);
		container.Init(containersize << 10);
#endif
		m_LodGroup.Load(*T, m_ShaderGroup, sm_LodLoadFilter, sm_LodForceRemap);

#if !__TOOL
		container.Finalize();
		m_ContainerPtr = tmp.m_Base;
		Assign(m_ContainerSizeQW,tmp.m_Size >> 4);
		Assert(m_ContainerSizeQW);
#endif
	}

	m_LodGroup.ComputeBucketMask(*m_ShaderGroup);
	T->MatchToken("}");
}

void rmcDrawable::LoadSkel( rmcTypeFileCbData *data )
{
	fiTokenizer *T = data->m_T;
	char buf[128];
	m_SkeletonData = 0;
	T->GetToken(buf,sizeof(buf)); 
	if (stricmp(buf,"none")) {
		int version;
		m_SkeletonData = crSkeletonData::AllocateAndLoad(buf, &version);
		Assert(m_SkeletonData);
		// Assert(datIsBuildingPageFile() || m_SkeletonData->GetNumBones() >= m_LodGroup.GetModel(LOD_HIGH).GetMatrixCount());
		if (version<110)
		{
			bool hasLimits = false;
			const crBoneData* bones = m_SkeletonData->GetBones();
			for (int i=0, numBones = m_SkeletonData->GetNumBones(); i<numBones; ++i)
			{
				if (bones[i].HasDofs(crBoneData::HAS_ROTATE_LIMITS | crBoneData::HAS_TRANSLATE_LIMITS | crBoneData::HAS_SCALE_LIMITS))
				{
					hasLimits = true;
				}
			}

			if (hasLimits)
			{
				m_JointData = rage_new crJointData;
				AssertVerify(m_JointData->InitFromSkeletonData(buf, crJointData::kInitAsEulers));
			}
		}
	}
}

void rmcDrawable::LoadSkelProperties( rmcTypeFileCbData *data)
{
	AssertMsg(m_SkeletonData, "Skeleton properties must be defined after the skeleton in the .type file");

	fiTokenizer *T = data->m_T;
	char buf[128];
	T->GetToken(buf,sizeof(buf)); 
	if (stricmp(buf,"none")) {
		m_SkeletonData->SetProperties(crProperties::AllocateAndLoad(buf));
	}
}

void rmcDrawable::LoadJointLimits( rmcTypeFileCbData *data )
{
	AssertMsg(!m_JointData, "Joint data has already been loaded. Probably because the drawable references a skel file pre-version 110 and a jlimits file!");
	fiTokenizer *T = data->m_T;
	char buf[128];
	m_JointData = 0;
	T->GetToken(buf,sizeof(buf));
	if (stricmp(buf,"none")) {
		m_JointData = crJointData::AllocateAndLoad(buf);
		Assert(m_JointData);
	}
}

void rmcDrawable::LoadEdgeModel( rmcTypeFileCbData *data )
{
	fiTokenizer *T = data->m_T;
	char buf[128];
	bool foundBracket = T->CheckToken("{");
	T->GetToken(buf,sizeof(buf)); 
	// m_EdgeModel = 0;

#if 0
	if (stricmp(buf,"none")) {
		fiStream *S = ASSET.Open(buf, "em" );
		if (S) {
			m_EdgeModel = grmEdgeModel::Create(S, buf);
			S->Close();
		}
	}
#endif

	if ( foundBracket )
		T->MatchToken("}");
}
#endif		// MESH_LIBRARY

#if 0
grmEdgeModel* rmcDrawable::SetEdgeModel(grmEdgeModel *pNewEdgeModel)
{
	if(m_EdgeModel)
	{
		delete m_EdgeModel; 
		m_EdgeModel = NULL;
	}

	m_EdgeModel = pNewEdgeModel;

	return(m_EdgeModel);
}
#endif

grcCullStatus rmcDrawable::IsVisible(const Matrix34 &mtx,const grcViewport &vp,u8 &lod, float * retZDist) const {
	float zDist = 0;
	float radius = GetLodGroup().GetCullRadius();
	Vector3 center;
	center.Dot(GetLodGroup().GetCullSphere(),mtx);
	grcCullStatus status = vp.IsSphereVisible(center.x,center.y,center.z,radius,&zDist);
	lod = (u8) (status? GetLodGroup().ComputeLod(zDist - GetLodGroup().GetCullRadius()) : LOD_VLOW);
	if (retZDist)
		*retZDist = Max(0.0f,zDist-radius);
	return status;
}

grcCullStatus rmcDrawable::IsVisible(const Matrix44 &mtx,const grcViewport &vp,u8 &lod, float * retZDist) const {
	float zDist = 0;
	float radius = GetLodGroup().GetCullRadius();
	Vector4 sphere;
	Vector4 center;
	sphere.SetVector3(GetLodGroup().GetCullSphere());
	sphere.w = 1.0f;
	center.Dot(sphere, mtx);
	grcCullStatus status = vp.IsSphereVisible(center.x,center.y,center.z,radius,&zDist);
	lod = (u8) (status? GetLodGroup().ComputeLod(zDist - GetLodGroup().GetCullRadius()) : LOD_VLOW);
	if (retZDist)
		*retZDist = Max(0.0f,zDist-radius);
	return status;
}

#if ENABLE_BLENDSHAPES
void rmcDrawable::ApplyBlendShapeOffsets(grbTargetManager &blendTargetManager)
{
	AssertMsg(GRCDEVICE.CheckThreadOwnership(),"Attempted to call rmcDrawable::ApplyBlendShapeOffsets outside of render thread.");
	if (!GRCDEVICE.CheckThreadOwnership())
		return;

	MakePpuOnly();

	for (int k = 0; k < blendTargetManager.GetBlendShapeCount(); k++)
	{
		grbBlendShape* pBlendShape = blendTargetManager.GetBlendShape(k);
		Assert(pBlendShape);
		for (int i = 0; i < pBlendShape->GetMorphableCount(); i++)
		{
			grbMorphable* pMorphable = pBlendShape->GetMorphable(i);
			Assert(pMorphable);
			int materialIdx = pMorphable->GetMaterialIndex();
			int modelIdx = pBlendShape->GetModelIndex();
			int lodIdx = pBlendShape->GetLodGroup();
			rmcLodGroup& lodGroup = GetLodGroup();
			Assert(lodGroup.ContainsLod(lodIdx));
			rmcLod& lod = lodGroup.GetLod(lodIdx);
			grmModel* model = lod.GetModel(modelIdx);
			Assert(model);
			grmGeometry& geom = model->GetGeometry(materialIdx);

			if (pMorphable->IsUsingEdge())
			{
				geom.SetBlendHeaders(pMorphable->GetDrawBufferIdx(), (grmGeometryEdgeBlendHeader**)&pMorphable->GetBlendHeaders()[0].ptr);
			}
			else
			{
				grcVertexBuffer* pOffsets = pMorphable->GetDrawBuffer();
				if (pOffsets) 
				{
					geom.SetOffsets(pOffsets);
				}
			}
		}
	}
}

void rmcDrawable::RemoveBlendShapeOffsets( grbTargetManager &blendTargetManager )
{
	u32 numMorphableBuffers = blendTargetManager.GetNumMorphableBuffers();
	for (int k = 0; k < blendTargetManager.GetBlendShapeCount(); k++)
	{
		grbBlendShape* pBlendShape = blendTargetManager.GetBlendShape(k);
		Assert(pBlendShape);
		for (int i = 0; i < pBlendShape->GetMorphableCount(); i++)
		{
			grbMorphable* pMorphable = pBlendShape->GetMorphable(i);
			Assert(pMorphable);
			int materialIdx = pMorphable->GetMaterialIndex();
			int modelIdx = pBlendShape->GetModelIndex();
			int lodIdx = pBlendShape->GetLodGroup();
			rmcLodGroup& lodGroup = GetLodGroup();
			Assert(lodGroup.ContainsLod(lodIdx));
			rmcLod& lod = lodGroup.GetLod(lodIdx);
			grmModel* model = lod.GetModel(modelIdx);
			Assert(model);
			grmGeometry& geom = model->GetGeometry(materialIdx);

			// as the renderthread has been flushed at this point we can set the offsets explicitly
			if (pMorphable->IsUsingEdge())
			{
				for (u32 j = 0; j < numMorphableBuffers; ++j)
					geom.SetBlendHeaders(j, NULL);
			}
			else
			{
				geom.SetOffsets(NULL);
			}
		}
	}
}
#endif // ENABLE_BLENDSHAPES

void rmcDrawable::Optimize() {
	for (int i=0; i<LOD_COUNT; i++)
		if (&GetLodGroup().GetLod(i))
			GetLodGroup().GetLod(i).Optimize(GetShaderGroup());
}

#if HACK_GTA4_MODELINFOIDX_ON_SPU
	namespace rage { CGta4DbgSpuInfoStruct	gGta4DbgInfoStruct; }
#endif

#if !__FINAL || !__NO_OUTPUT
#	if RSG_ORBIS
		void rmcDrawable::SaveDebugInfoInCommandBuffer() const {
			u32 *const dbg = (u32*)gfxc.embedDataDcb(4);
			dbg[0] = 'EGAR';
			dbg[1] = 'LBRD';
			dbg[2] = (u32)(u64)this;
			dbg[3] = (u32)((u64)this>>32);

			DEV_ONLY(sm_DebugDrawableName = m_DebugName);
		}
	#endif
#endif

void rmcDrawable::Draw(const Matrix34 &mtx,u32 bucketMask,int lod, u16 DRAWABLE_STATS_ONLY(drawableStat)) const {
#if __PPU && DRAWABLE_ON_SPU
	if (!GetPpuOnly() BANK_ONLY(&& !grmModel::HasCustomShaderParamsFunc())) {
		AssertMsg(&GetLodGroup().GetLod(lod), "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

		// This can be changed back to __ASSERT only once we fix all the broken
		// assets, but at the moment, there is so many things that are broken,
		// lets turn this on in more builds so that i don't keep having to trawl
		// through coredumps to get the asset name.
#		if !__FINAL && !__PROFILE // __ASSERT
			// Make sure that none of the models are skinned, else we will crash
			// Edge (well actually crash SPURS trying to load Edge), due to the
			// lack of skeleton matrices.
			const rmcLod *const pLod = &GetLodGroup().GetLod(lod);
			const unsigned numModels = pLod->GetCount();
			for (unsigned i=0; i<numModels; ++i)
			{
				const grmModel *const pModel = pLod->GetModel(i);
// 				FatalAssert(!pModel->GetSkinFlag());
				if (Unlikely(pModel->GetSkinFlag()))
				{
					Quitf("Bad asset \"%s\", geometry is skinned, but entity is not.  Unable to continue, as it will crash the game.",
						m_DebugName);
				}
			}
#		endif

		grcStateBlock::FlushAndMakeDirty();
		SPU_COMMAND(rmcDrawable__Draw,0);
		cmd->drawable = const_cast<rmcDrawable*>(this);
		Assign(cmd->bucketMask,(bucketMask & 0xFFFF));
		Assign(cmd->lod,lod);
		Assign(cmd->stats,drawableStat);
		Assign(cmd->containerSizeQW,m_ContainerSizeQW);
		cmd->container = m_ContainerPtr;
		cmd->shaderGroup = m_ShaderGroup;
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		cmd->gta4DebugInfo = gGta4DbgInfoStruct;
	#endif
		cmd->matrix = mtx;
	}
	else
#endif
	{
		SaveDebugInfoInCommandBuffer();
		GetLodGroup().DrawSingle(GetShaderGroup(),mtx,bucketMask,lod DRAWABLE_STATS_ONLY(,drawableStat));
#		if DRAWABLE_STATS
			if(g_pCurrentStatsBucket)
			{
				g_pCurrentStatsBucket->DrawableDrawCalls++;
			}
#		endif
	}
}



void rmcDrawable::DrawSkinned(const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lod, u16 DRAWABLE_STATS_ONLY(drawableStat)) const {
#if __PPU && DRAWABLE_ON_SPU
	if (!GetPpuOnly() BANK_ONLY(&& !grmModel::HasCustomShaderParamsFunc())) {
		AssertMsg(&GetLodGroup().GetLod(lod), "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

		grcStateBlock::FlushAndMakeDirty();
		SPU_COMMAND(rmcDrawable__DrawSkinned,0,sizeof(Matrix34));
		cmd->drawable = const_cast<rmcDrawable*>(this);
		cmd->matrixSet = const_cast<grmMatrixSet*>(&ms);
		Assign(cmd->bucketMask,(bucketMask & 0xFFFF));
		Assign(cmd->lod,lod);
		Assign(cmd->stats,drawableStat);
		Assign(cmd->containerSizeQW,m_ContainerSizeQW);
		cmd->container = m_ContainerPtr;
		cmd->shaderGroup = m_ShaderGroup;
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		cmd->gta4DebugInfo = gGta4DbgInfoStruct;
	#endif
		cmd->rootMatrix = mtx;
	}
	else
#endif
	{
		SaveDebugInfoInCommandBuffer();
		GetLodGroup().DrawMulti(GetShaderGroup(),mtx,ms,bucketMask,lod DRAWABLE_STATS_ONLY(,drawableStat) ASSERT_ONLY(, m_DebugName));
#		if DRAWABLE_STATS
			if(g_pCurrentStatsBucket)
			{
				g_pCurrentStatsBucket->DrawableDrawSkinnedCalls++;
			}
#		endif
	}
}

void rmcDrawable::DrawInstanced(grcInstanceBufferList &list, u32 bucketMask, int lod) const
{
	DrawInstanced(list.GetFirst(), bucketMask, lod);
}

void rmcDrawable::DrawInstanced(grcInstanceBuffer *ib, u32 bucketMask, int lod) const
{
	SaveDebugInfoInCommandBuffer();
	GetLodGroup().DrawSingle(GetShaderGroup(),ib,bucketMask,lod);
}

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES

void rmcDrawable::DrawNoShaders(const Matrix34& mtx, u32 bucketMask,int lodIndex) const
{
	bool updatedMatrix = false;

	const rmcLod& lod=GetLodGroup().GetLod(lodIndex);
	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH(GetLodGroup().GetBucketMask(lodIndex), bucketMask))
	{
		SaveDebugInfoInCommandBuffer();

		for (int i=lod.GetCount()-1;i>=0;i--)
		{
			{
				const grmModel& model=*lod.GetModel(i);

				if( BUCKETMASK_MODELMATCH(model.GetMask(),bucketMask) )
					continue;
				
				for (int j=model.GetGeometryCount()-1;j>=0;j--)
				{
					if (bucketMask != 0xFFFFFFFF)
					{
						u32 shaderBucketMask = GetShaderGroup()[model.GetShaderIndex(j)].GetDrawBucketMask();

						if (!(BUCKETMASK_MATCH(shaderBucketMask,bucketMask)))
							continue;
					}

					if (!updatedMatrix)
					{
						grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));
						updatedMatrix = true;
					}

					model.Draw(j);
				}
			}
		}
	}
}

void rmcDrawable::DrawSkinnedNoShaders(const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lodIndex,EnumNoShadersSkinned renderOnly) const
{
	int lastMtx = -2;
	const rmcLod& lod=GetLodGroup().GetLod(lodIndex);
	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH(GetLodGroup().GetBucketMask(lodIndex), bucketMask))
	{
		SaveDebugInfoInCommandBuffer();

		for (int i=lod.GetCount()-1;i>=0;i--)
		{
			const grmModel& model=*lod.GetModel(i);

			if( BUCKETMASK_MODELMATCH(model.GetMask(),bucketMask) )
				continue;
			
			int idx=model.GetMatrixIndex();
			for (int j=model.GetGeometryCount()-1;j>=0;j--)
			{
				if (bucketMask != 0xFFFFFFFF)
				{
					u32 shaderBucketMask = GetShaderGroup()[model.GetShaderIndex(j)].GetDrawBucketMask();

					if (!(BUCKETMASK_MATCH(shaderBucketMask,bucketMask)))
						continue;
				}

				if (model.GetSkinFlag())
				{
					if (renderOnly&RENDER_SKINNED)
					{
						if (lastMtx != -1)
						{
							grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));
							lastMtx = -1;
						}

						model.DrawSkinned(j,ms);
					}
				}
				else
				{
					if (renderOnly&RENDER_NONSKINNED)
					{
						if (idx != lastMtx) {
							Matrix34 tmp;
							ms.ComputeMatrix(tmp,lastMtx = idx);
							tmp.Dot(mtx);
							grcViewport::SetCurrentWorldMtx(RCC_MAT34V(tmp));
						}

						model.Draw(j);
					}
				}
			}
		}
	}
}

#else //RAGE_SUPPORT_TESSELLATION_TECHNIQUES

void rmcDrawable::DrawNoShaders(const Matrix34& mtx, u32 bucketMask,int lodIndex) const
{
	const rmcLod& lod=GetLodGroup().GetLod(lodIndex);
	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH_LOD(GetLodGroup().GetBucketMask(lodIndex), bucketMask))
	{
		SaveDebugInfoInCommandBuffer();

		for (int i=lod.GetCount()-1;i>=0;i--)
		{
			{
				const grmModel& model=*lod.GetModel(i);

				if( BUCKETMASK_MODELMATCH(model.GetMask(),bucketMask) )
					continue;

				DrawModelGeometries(model, 0, model.GetGeometryCount(), mtx, bucketMask);
			}
		}
	}
}


void rmcDrawable::DrawNoShadersTessellationControlled(const Matrix34& mtx, u32 bucketMask,int lodIndex) const
{
	const rmcLod& lod=GetLodGroup().GetLod(lodIndex);
	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH_LOD(GetLodGroup().GetBucketMask(lodIndex), bucketMask))
	{
		SaveDebugInfoInCommandBuffer();

		for (int i=lod.GetCount()-1;i>=0;i--)
		{
			const grmModel& model=*lod.GetModel(i);

			if( BUCKETMASK_MODELMATCH(model.GetMask(),bucketMask) )
				continue;

			int loopEnd = 0;
			int startIdx = 0;
			ComputeGeometriesToDraw(model, startIdx, loopEnd);
			DrawModelGeometries(model, startIdx, loopEnd, mtx, bucketMask);
		}
	}
}


void rmcDrawable::DrawModelGeometries(const grmModel& model, int startIdx, int loopEnd, const Matrix34& mtx, u32 bucketMask) const
{
	SaveDebugInfoInCommandBuffer();

	bool updatedMatrix = false;

	for (int j=startIdx; j<loopEnd; j++)
	{
		if (bucketMask != 0xFFFFFFFF)
		{
			u32 shaderBucketMask = GetShaderGroup()[model.GetShaderIndex(j)].GetDrawBucketMask();

			if (!(BUCKETMASK_MATCH_SHADER(shaderBucketMask,bucketMask)))
				continue;
		}

		if (!updatedMatrix)
		{
			grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));
			updatedMatrix = true;
		}
		model.Draw(j);
	}
}


void rmcDrawable::DrawSkinnedNoShaders(const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lodIndex,EnumNoShadersSkinned renderOnly) const
{
	int lastMtx = -2;
	const rmcLod& lod=GetLodGroup().GetLod(lodIndex);
	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH_LOD(GetLodGroup().GetBucketMask(lodIndex), bucketMask))
	{
		SaveDebugInfoInCommandBuffer();

		for (int i=lod.GetCount()-1;i>=0;i--)
		{
			const grmModel& model=*lod.GetModel(i);

			if( BUCKETMASK_MODELMATCH(model.GetMask(),bucketMask) )
				continue;

			DrawModelGeometriesSkinned(model, 0, model.GetGeometryCount(), mtx, ms, bucketMask, renderOnly, lastMtx);
		}
	}
}


void rmcDrawable::DrawSkinnedNoShadersTessellationControlled(const Matrix34& mtx,const grmMatrixSet &ms,u32 bucketMask,int lodIndex,EnumNoShadersSkinned renderOnly) const
{
	int lastMtx = -2;
	const rmcLod& lod=GetLodGroup().GetLod(lodIndex);
	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH_LOD(GetLodGroup().GetBucketMask(lodIndex), bucketMask))
	{
		SaveDebugInfoInCommandBuffer();

		for (int i=lod.GetCount()-1;i>=0;i--)
		{
			const grmModel& model=*lod.GetModel(i);

			if( BUCKETMASK_MODELMATCH(model.GetMask(),bucketMask) )
				continue;

			int loopEnd = 0;
			int startIdx = 0;
			ComputeGeometriesToDraw(model, startIdx, loopEnd);
			DrawModelGeometriesSkinned(model, startIdx, loopEnd, mtx, ms, bucketMask, renderOnly, lastMtx);
		}
	}
}


void rmcDrawable::DrawModelGeometriesSkinned(const grmModel& model, int startIdx, int loopEnd, const Matrix34& mtx,const grmMatrixSet &ms,u32 bucketMask,EnumNoShadersSkinned renderOnly, int &lastMtx) const
{
	SaveDebugInfoInCommandBuffer();

	int idx=model.GetMatrixIndex();

	for (int j=startIdx; j<loopEnd; j++)
	{
		if (bucketMask != 0xFFFFFFFF)
		{
			u32 shaderBucketMask = GetShaderGroup()[model.GetShaderIndex(j)].GetDrawBucketMask();

			if (!(BUCKETMASK_MATCH_SHADER(shaderBucketMask,bucketMask)))
				continue;
		}

		if (model.GetSkinFlag())
		{
			if (renderOnly&RENDER_SKINNED)
			{
				if (lastMtx != -1)
				{
					grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));
					lastMtx = -1;
				}

				model.DrawSkinned(j,ms);
			}
		}
		else
		{
			if (renderOnly&RENDER_NONSKINNED)
			{
				if (idx != lastMtx) {
					Matrix34 tmp;
					ms.ComputeMatrix(tmp,lastMtx = idx);
					tmp.Dot(mtx);
					grcViewport::SetCurrentWorldMtx(RCC_MAT34V(tmp));
				}

				model.Draw(j);
			}
		}
	}
}

void rmcDrawable::ComputeGeometriesToDraw(const grmModel& model, int &startIdx, int &loopEnd)
{
	switch(rmcLodGroup::GetTessellatedDrawMode())
	{
	case rmcLodGroup::UNTESSELLATED_ONLY:
		{
			startIdx = 0;
			loopEnd = model.GetUntessellatedGeometryCount();
			break;
		}
	case rmcLodGroup::TESSELLATED_ONLY:
	case rmcLodGroup::TESSELLATED_ONLY_AS_UNTESSELLATED:
		{
			startIdx = model.GetGeometryCount() - model.GetTessellatedGeometryCount();
			loopEnd = model.GetGeometryCount();
			break;
		}
	case rmcLodGroup::UNTESSELLATED_AND_TESSELLATED:
	case rmcLodGroup::UNTESSELLATED_AND_TESSELLATED_AS_UNTESSELLATED:
		{
			startIdx = 0;
			loopEnd = model.GetGeometryCount();
			break;

		}
	}
}


#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

/* void rmcDrawable::SwapDrawBuffers()
{
	for( int l = 0; l < LOD_COUNT; l++ )
	{
		const rmcLod& lod = m_LodGroup.GetLod(l);
		for( int i = 0; i < lod.GetCount(); i++ )
		{
			lod.GetModel(i)->SwapBuffers();
		}
	}
} */

#if __PFDRAW

PFD_DECLARE_GROUP(Drawable);

PFD_DECLARE_ITEM(BoundingBox,Color32(0,255,255), Drawable); 
PFD_DECLARE_ITEM(VertexNormals,Color32(255, 0, 255),Drawable);
PFD_DECLARE_ITEM_COLOR(NormalColor,Drawable, Color32(255,0,0));

PFD_DECLARE_ITEM(BiNormals,Color32(255,0,255),Drawable);
PFD_DECLARE_ITEM_COLOR(BiNormalColor,Drawable, Color32(0,255,0));

PFD_DECLARE_ITEM(Tangents,Color32(255,0,255),Drawable);
PFD_DECLARE_ITEM_COLOR(TangentColor,Drawable, Color32(0,0,255));

PFD_DECLARE_ITEM_SLIDER(NormalLength, Color32(255,0,255), Drawable, 0.02f, 10.0f, 0.01f);

void rmcDrawable::ProfileDrawBoundingBox(const Matrix34* transformMat, const Color32& color, bool solid) const
{
	Vector3 bbMin;
	Vector3 bbMax;
	m_LodGroup.GetBoundingBox(bbMin, bbMax);

	Vector3 size;
	size.Subtract(bbMax, bbMin);


	Matrix34 bbMat;
	bbMat.Identity();

	//bbMat.d.y = size.y/2.0f; wat?
	//~R
	Vector3 center;
	center.Add(bbMax,bbMin);
	center=center/2.0f;

	bbMat.d.x=center.x;
	bbMat.d.y=center.y;
	bbMat.d.z=center.z;
	//

	bbMat.Dot(*transformMat);

	if(solid)
	{
		grcDrawSolidBox	(size, bbMat, color);
	}
	else
	{
		grcDrawBox(size, bbMat, color);	
	}
	grcWorldIdentity();
}

void rmcDrawable::ProfileDraw(const crSkeleton *WIN32_ONLY(pSkel), const Matrix34* WIN32_ONLY(transformMat), int WIN32_ONLY(lodLevel)) const
{
#if __WIN32
	if (PFDGROUP_Drawable.Begin())
	{
		bool oldLighting = grcLighting(false);
		
		if(PFD_BoundingBox.WillDraw())
		{
			ProfileDrawBoundingBox(transformMat, Color32(0.0f,1.0f,1.0f), false);
		}
		
		bool bDrawVNorm = PFD_VertexNormals.WillDraw();
		bool bDrawBNorm = PFD_BiNormals.WillDraw();
		bool bDrawTan = PFD_Tangents.WillDraw();

		if (bDrawVNorm || bDrawBNorm || bDrawTan)
		{
			grcWorldMtx(*transformMat);

			const rmcLod& lod = m_LodGroup.GetLod(lodLevel);
			if (&lod)
			{
				//Determine how many grcVertex3f calls will be made for
				//each vertex in the model.  This is used to batch together 
				//the grcVertex3f calls between the grcBegin and grcEnd calls
				int valPerVertex = 0;
				if(bDrawVNorm)
					valPerVertex += 2;
				if(bDrawBNorm)
					valPerVertex += 2;
				if(bDrawTan)
					valPerVertex += 2;

				Matrix43 *const mtx =  Alloca(Matrix43,m_SkeletonData->GetNumBones());

				int modelCount = lod.GetCount();
				for(int mdlIdx = 0; mdlIdx < modelCount; mdlIdx++)
				{
					grmModel* pModel = lod.GetModel(mdlIdx);
					Assert(pModel);

					pSkel->Attach(pModel->IsModelRelative(), mtx);;

					int geomCount = pModel->GetGeometryCount();
					for(int geomIdx = 0; geomIdx < geomCount; geomIdx++)
					{
	#if __BANK && __D3D && 0
						//Determine if this geometry is being rendered
						int shdIdx = pModel->GetShaderIndex(geomIdx);
						grmShaderFx *pShader = static_cast<grmShaderFx*>(m_ShaderGroup->GetShaderPtr(shdIdx));
						if(grmShaderPreset::IsDisabled( pShader->GetPresetName() ) )
							continue;
	#endif//__BANK

						grmGeometry	&geom = pModel->GetGeometry(geomIdx);
						const grcFvf* fvf = geom.GetFvf();
					
						bool bHasNrms(false), bHasBiNrms(false), bHasTan(false);
					
						bHasNrms = fvf->GetNormalChannel();
						bHasBiNrms = fvf->GetBinormalChannel(0);
						bHasTan = fvf->GetTangentChannel(0);

						//Are there normals in this geometry?
						if(fvf->GetNormalChannel())
						{
							grcVertexBuffer* pVtxBuf = geom.GetVertexBuffer();
							Assert(pVtxBuf);
							grcVertexBufferEditor vtxBufEditor(pVtxBuf);
							int vtxCount = pVtxBuf->GetVertexCount();

							//Render the normals for each vertex, batching the line
							//rendering into the largest groups possible..
							int start = 0;
							while(start < vtxCount)
							{	
								int drawCount = Min( (vtxCount-start), (grcBeginMax/valPerVertex));
								grcBegin(drawLines, drawCount*valPerVertex);
								{
									for( int vtxIdx = start; vtxIdx < (start+drawCount); vtxIdx++ )
									{
										Vector3 vPos = vtxBufEditor.GetPosition(vtxIdx);
										Vector3 vNrm = vtxBufEditor.GetNormal(vtxIdx);

										Vector3 vTan3(0.0f,0.0f,0.0f);
										Vector4 vTan4(0.0f,0.0f,0.0f,0.0f);
										if(bHasTan)
										{
											vTan4 = vtxBufEditor.GetTangent(vtxIdx, 0);
											vTan3 = Convert(vTan3,vTan4);
										}
										Vector3 bNrm(0.0f,0.0f,0.0f);

										if(fvf->GetBindingsChannel() && fvf->GetBlendWeightChannel() && mtx)
										{
											//Skin the vertex...
											const u16* matrixPalette = geom.GetMatrixPalette();
											Assert(matrixPalette);

											const Color32& blendIndices = vtxBufEditor.GetBlendIndices(vtxIdx);
											const Vector4& blendWeights = vtxBufEditor.GetBlendWeights(vtxIdx);

											int index = blendIndices.GetRed();
											Matrix34 skinMtx;
											mtx[matrixPalette[index]].ToMatrix34(RC_MAT34V(skinMtx));
											skinMtx.ScaleFull(blendWeights.x);
											index = blendIndices.GetGreen();
											Matrix34 temp;
											mtx[matrixPalette[index]].ToMatrix34(RC_MAT34V(temp));
											temp.ScaleFull(blendWeights.y);
											skinMtx.Add(temp);
											index = blendIndices.GetBlue();
											mtx[matrixPalette[index]].ToMatrix34(RC_MAT34V(temp));;
											temp.ScaleFull(blendWeights.z);
											skinMtx.Add(temp);
											index = blendIndices.GetAlpha();
											mtx[matrixPalette[index]].ToMatrix34(RC_MAT34V(temp));
											temp.ScaleFull(blendWeights.w);
											skinMtx.Add(temp);

											skinMtx.Transform(vPos);

											skinMtx.Transform(vNrm);
											vNrm.Normalize();
					
											if(bHasTan)
											{
												skinMtx.Transform(vTan3);
												vTan3.Normalize();

												bNrm.Cross(vNrm,vTan3);
												if(vTan4.w != 0)
													bNrm.Scale(vTan4.w);
												bNrm.Normalize();
											}
										}
										else
										{
											if(transformMat)
											{
												transformMat->Transform(vPos);
												transformMat->Transform3x3(vNrm);

												if(bHasTan)
												{
													transformMat->Transform3x3(vTan3);

													bNrm.Cross(vNrm,vTan3);
													if(vTan4.w != 0)
														bNrm.Scale(vTan4.w);
													bNrm.Normalize();
												}
											}
										}

							
										if(bDrawVNorm)
										{
											Vector3 nrmOffset;
											nrmOffset.Scale(vNrm, PFD_NormalLength.GetValue());
											Vector3 nrmPos = vPos + nrmOffset;

											grcColor(PFD_NormalColor.GetValue());
											grcVertex3f(vPos);
											grcVertex3f(nrmPos);
										}

										if(bDrawBNorm)
										{
											Vector3 bnrmOffset;
											bnrmOffset.Scale(bNrm, PFD_NormalLength.GetValue());
											Vector3 bnrmPos = vPos + bnrmOffset;

											grcColor(PFD_BiNormalColor.GetValue());
											grcVertex3f(vPos);
											grcVertex3f(bnrmPos);
										}

										if(bDrawTan)
										{
											Vector3 tanOffset;
											tanOffset.Scale(vTan3, PFD_NormalLength.GetValue());
											Vector3 tanPos = vPos + tanOffset;

											grcColor(PFD_TangentColor.GetValue());
											grcVertex3f(vPos);
											grcVertex3f(tanPos);
										}
										
									}//End for( int vtxIdx = 0...
								}

								grcEnd();
								start += drawCount;
							}//End while(start < vtxCount)
						}//End if(fvf->GetNormalChannel())
					}//End for(int geomIDx..
				}//End for(int mdlIdx..
			}//End if(&lod)

		}//End if(bDrawVNorm || bDrawBNorm || bDrawTan)

		grcLighting(oldLighting);

		PFDGROUP_Drawable.End();
	}//End if (PFDGROUP_Drawable.Begin())
#endif
}
#endif//__PFDRAW

IMPLEMENT_PLACE(rmcDrawable);

rmcDrawableBase::rmcDrawableBase(datResource &) {
	// DrawableBase does NOT own its shadergroups, or even the pointer to it.
	// rsc.PointerFixup(m_ShaderGroup);
}

rmcDrawableBase::rmcDrawableBase(datResource &, grmShaderGroup *group): m_ShaderGroup(group) {
	
	// DrawableBase does NOT own its shadergroup, 
	// this time it's possibly not even in the same resource
	
	// 2005.03.21.SS 
	// Because the shader group is not owned, assign the group
	// via the m_ShaderGroup(group) as opposed to m_ShaderGroup = group,
	// the latter method creates a resource for the group which
	// is redundant and causes a crash during the loading of resources,
	// because of pointer fix up occuring on the same group pointer.
}



void rmcDrawable::CheckSizeForSpu(datResource & PS3_ONLY(ASSERT_ONLY(rsc))) {
	// Some drawables are overly complicated and their container doesn't fit in the limited space available on SPU,
	// so we execute them on PPU instead. Not ideal, but prevents some crashing due to scratch overruns. 

#if __PS3

#if 0
	for (int i=0; i<LOD_COUNT; i++) {
		if (m_LodGroup.ContainsLod(i)) {
			for (int j=0; j<m_LodGroup.GetLod(i).GetCount(); j++) {
				grmModel &m = *m_LodGroup.GetLod(i).GetModel(j);
				for (int k=0; k<m.GetGeometryCount(); k++) {
					if (m.GetGeometry(k).GetType() == grmGeometry::GEOMETRYEDGE) {
						grmGeometryEdge &e = (grmGeometryEdge&)m.GetGeometry(k);
						const EdgeGeomPpuConfigInfo *p = e.GetEdgeGeomPpuConfigInfos();
						for (int l=0; l<e.GetEdgeGeomPpuConfigInfoCount(); l++) {
							// Change these numbers to whatever you're looking for.
							if (p[l].spuConfigInfo.numVertexes == 513 && p[l].spuConfigInfo.numIndexes == 7092) {
								printf("Bad model %s\n",rsc.GetDebugName());
								__debugbreak();
							}
						}
					}
				}
			}
		}
	}
#endif

	int geomCount = 0;
	if(m_LodGroup.ContainsLod(LOD_HIGH))
	{
		const u32 mCount = m_LodGroup.GetLod(LOD_HIGH).GetCount();
		for(u32 m=0; m<mCount; m++)
		{
			grmModel *pModel = m_LodGroup.GetLod(LOD_HIGH).GetModel(m);
			Assert(pModel);
			geomCount += pModel->GetGeometryCount();
		}
	}


	int modelCount = m_LodGroup.ContainsLod(LOD_HIGH)? m_LodGroup.GetLod(LOD_HIGH).GetCount() : 0;
	int shaderCount = m_ShaderGroup? m_ShaderGroup->GetCount() : 0;

	// If we exceed any limits, whine about it and set the flag that forces it to render from the PPU.
	if (!GetPpuOnly() && 
		 (!artVerifyf(GetContainerSize() <= DRAWABLE_MAX_CONTAINER_SIZE,"Object '%s' container too large (%dk > %dk)", rsc.GetDebugName(), (GetContainerSize()+1023)>>10, DRAWABLE_MAX_CONTAINER_SIZE>>10)
		||!artVerifyf(modelCount <= DRAWABLE_MAX_MODELS,"Object '%s' model count too large (%d > %d)", rsc.GetDebugName(), modelCount, DRAWABLE_MAX_MODELS)
		||!artVerifyf(geomCount <= DRAWABLE_MAX_GEOMS,"Object '%s' geometry count too large (%d > %d)", rsc.GetDebugName(), geomCount, DRAWABLE_MAX_GEOMS)
		||!artVerifyf(shaderCount <= DRAWABLE_MAX_SHADERS,"Object '%s' shader count too large (%d > %d)", rsc.GetDebugName(), shaderCount, DRAWABLE_MAX_SHADERS)))
		MakePpuOnly();

	// Catch bad shaders; this will let us remove the checks we added down in drawablespu when instancedata doesn't fit in the container any longer.
	char *start = (char*)m_ShaderGroup.ptr;
	if (start) {
		char *stop = start + m_ShaderGroup->GetContainerSize();
		for (int i=0; i<shaderCount; i++) {
			grmShader *s = &m_ShaderGroup->GetShader(i);
			grcInstanceData &d = s->GetInstanceData();
			if (!artVerifyf((char*)d.Entries >= start && (char*)d.Entries < stop,"Drawable %p (%s) %p<=%p<=%p will break spu!  (%d/%d, shader named %s, need to regenerate resources)",this,rsc.GetDebugName(),start,d.Entries,stop,i,shaderCount,s->GetName())) {
				MakePpuOnly();
				break;
			}
		}
	}
	// I think we don't want this check... it will force broken fragments (which borrow the shader group of their parent) to always render from the PPU.
	// else if (!artVerifyf(GetPpuOnly(),"Drawable with no container doesn't have ppuonly set! %p (%s)",this,rsc.GetDebugName()))
		// MakePpuOnly();
#endif // __PS3
}

rmcDrawable::rmcDrawable(datResource &rsc) : rmcDrawableBase(rsc), m_LodGroup(rsc) {
	rsc.PointerFixup(m_ContainerPtr);
	rsc.PointerFixup(m_DebugName);

	// Drawable DOES own its shadergroup
	// grmShaderFactory::GetInstance().ResourcePageIn(rsc,m_ShaderGroup.ptr);

	// Re-compute the draw buckets. The offline resourcing can't do that because shaders
	// are not loaded during that process (besides, shaders may be changed at any time)
	if (m_ShaderGroup)
	{
		GetLodGroup().ComputeBucketMask(GetShaderGroup());
		Optimize();

	#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
		m_LodGroup.SortForTessellation(GetShaderGroup());
	#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	}


	CheckSizeForSpu(rsc);
}

rmcDrawable::rmcDrawable(datResource &rsc, grmShaderGroup *group) : rmcDrawableBase(rsc, group), m_LodGroup(rsc) {
	rsc.PointerFixup(m_ContainerPtr);
	rsc.PointerFixup(m_DebugName);

	// Re-compute the draw buckets. The offline resourcing can't do that because shaders
	// are not loaded during that process (besides, shaders may be changed at any time)
	if (m_ShaderGroup)
	{
		GetLodGroup().ComputeBucketMask(GetShaderGroup());
		Optimize();
	#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
		m_LodGroup.SortForTessellation(GetShaderGroup());
	#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	}

	CheckSizeForSpu(rsc);
}

rmcDrawable* rmcDrawable::LoadResource(const char *drawableName,const char *texdictName) {
	pgDictionary<grcTexture> *td = NULL, *prev = NULL;
	rmcDrawable *drw = NULL;

	if (texdictName) {
		pgRscBuilder::Load(td,texdictName,"#td",grcTextureDefault::RORC_VERSION);
		prev = pgDictionary<grcTexture>::SetCurrent(td);
	}

	pgRscBuilder::Load(drw,drawableName,"#dr",rmcDrawable::RORC_VERSION);

	if (texdictName) {
		pgDictionary<grcTexture>::SetCurrent(prev);
	}

	return drw;
}

#if RSG_RSC
void rmcDrawable::SetDebugName(const char *name) {
	Assert(!m_DebugName);		// setting twice would leak...
	m_DebugName = StringDuplicate(name);
}
#endif


#if __PPU
#include "grcore/indexbuffer.h"
// #include "system/findsize.h"
CompileTimeAssert(sizeof(spuMatrix44) == sizeof(Matrix34));
CompileTimeAssert(sizeof(spuMatrix43) == sizeof(Matrix43));
CompileTimeAssert(sizeof(spuMatrixSet) == sizeof(grmMatrixSet));
CompileTimeAssert(sizeof(grcVertexBufferGCM) == sizeof(spuVertexBuffer));
CompileTimeAssert(sizeof(grcIndexBufferGCM) == sizeof(spuIndexBuffer));
CompileTimeAssert(sizeof(grcVertexDeclaration) == sizeof(spuVertexDeclaration));
CompileTimeAssert(sizeof(grmGeometryQB) == sizeof(spuGeometryQB));
CompileTimeAssert(sizeof(spuGeometryQB) == 80);
CompileTimeAssert(sizeof(grmModel) == sizeof(spuModel));
CompileTimeAssert(sizeof(grcTexture) == sizeof(spuTexture));
CompileTimeAssert(sizeof(spuTexture)==52);
CompileTimeAssert(sizeof(grmShader) == sizeof(spuShader));
CompileTimeAssert(sizeof(grmShader) == sizeof(grcInstanceData));
CompileTimeAssert(sizeof(grmShaderGroup) == sizeof(spuShaderGroup));
CompileTimeAssert(sizeof(rmcLod) == sizeof(spuLod));
CompileTimeAssert(sizeof(rmcLodGroup) == sizeof(spuLodGroup));
CompileTimeAssert(sizeof(rmcDrawable) == sizeof(spuDrawable));
CompileTimeAssertSize(rmcDrawable,128,128);
#endif

#if __DECLARESTRUCT
void rmcDrawableBase::DeclareStruct(datTypeStruct &s) {
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(rmcDrawableBase);
	STRUCT_FIELD(m_ShaderGroup);
	STRUCT_END();
}

void rmcDrawable::DeclareStruct(datTypeStruct &s) {
	rmcDrawableBase::DeclareStruct(s);
	STRUCT_BEGIN(rmcDrawable);
	STRUCT_FIELD(m_SkeletonData);
	STRUCT_FIELD(m_LodGroup);
	STRUCT_FIELD(m_JointData);
	STRUCT_FIELD(m_HandleIndex);
	STRUCT_FIELD(m_ContainerSizeQW);
	STRUCT_FIELD(m_ContainerPtr);
	STRUCT_FIELD(m_DebugName);
	STRUCT_END();
}
#endif
