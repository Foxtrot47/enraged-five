//
// rmcore/lodgroup.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "lodgroup.h"

#include "atl/array_struct.h"
#include "atl/bitset.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"
#include "file/stream.h"
#include "file/token.h"
#include "grcore/viewport.h"
#include "grmodel/matrixset.h"
#include "grmodel/model.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shadergroup.h"	// sigh -- for GetDeferredLoad, need a better solution
#include "rmcore/drawable.h"
#include "string/string.h"
#include "system/param.h"
#include "vector/matrix34.h"

#if __PPU
#include "grcore/wrapper_gcm.h"
#endif

using namespace rage;

bool rage::rmcEnableModelStreaming = true;
bool rage::rmcEnableModelPaginging = false;

#if __DEV
__THREAD const char* rmcLodGroup::debugRenderModel = NULL;
#endif // __DEV

rmcLod::rmcLod(int count) : m_Models(count,count) {
	memset(&m_Models[0],0,sizeof(grmModel*)*count);
}

//#pragma warning( disable:4100 )

void rmcLod::CloneWithNewVertexData( const rmcLod &cloneme, const atArray<int>& modelIdx, const atArray<int>& geomIdx, bool createVB, void* preAllocatedMemory, int& memoryAvailable )
{
	Assert( modelIdx.GetCount() == geomIdx.GetCount() );

	//// m_Models.Reserve( cloneme.m_Models.GetCount() );

	int iUniqueGeom = 0;
	int iStartGeom = 0;
	int iUniqueModelVal = modelIdx[0];
	m_Models[iUniqueModelVal] = rage_contained_new grmModelGeom();
	for( ; iUniqueGeom < geomIdx.GetCount(); iUniqueGeom++ )
	{
		int iModelVal = modelIdx[iUniqueGeom];

		if( iModelVal != iUniqueModelVal )
		{
			m_Models[iUniqueModelVal]->CloneWithNewVertexData( *(cloneme.m_Models[iUniqueModelVal]), &(geomIdx[iStartGeom]), iUniqueGeom-iStartGeom, createVB, preAllocatedMemory, memoryAvailable );
			iUniqueModelVal = iModelVal;
			iStartGeom = iUniqueGeom;
			m_Models[iUniqueModelVal] = rage_contained_new grmModelGeom();
		}
	}

	m_Models[iUniqueModelVal]->CloneWithNewVertexData( *(cloneme.m_Models[iUniqueModelVal]), &(geomIdx[iStartGeom]), iUniqueGeom-iStartGeom, createVB, preAllocatedMemory, memoryAvailable );

	const int modelCount = cloneme.m_Models.GetCount();
	for(int i=0; i<modelCount; ++i)
	{
		if(!m_Models[i])
			m_Models[i] = cloneme.m_Models[i];
	}
}

u32 rmcLod::ComputeBucketMask(const grmShaderGroup &group) const {
	u32 mask = 0;
	for (int i=0; i<m_Models.GetCount(); i++)
		if (GetModel(i))
			mask |= GetModel(i)->ComputeBucketMask(group);
	return mask;
}

void rmcLod::Optimize(grmShaderGroup &group) {
	for (int i=0; i<m_Models.GetCount(); i++)
		if (GetModel(i))
			GetModel(i)->Optimize(group);
}



void rmcLod::Shutdown() {
	for (int i=0; i<m_Models.GetCount(); i++)
		delete m_Models[i].ptr;
	m_Models.Reset();
}

rmcLod::~rmcLod() { 
	Shutdown();
}


#if MESH_LIBRARY
void rmcLod::SetModel(fiTokenizer &t,int i,const char *name,int mtxIndex, int lod, const grmShaderGroup *pShaderGroup,bool optimize,int extraVerts, u8 mask) {
	m_Models[i] = strcmp(name,"none")? grmModelFactory::GetInstance().Create(t,name,mtxIndex,NULL,pShaderGroup,optimize,extraVerts, mask, lod) : 0;
}
#endif

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
void rmcLod::SortForTessellation(const grmShaderGroup &group)
{
	int i;

	for(i=0; i<m_Models.GetCount(); i++)
	{
		if(m_Models[i])
		{
			m_Models[i]->SortForTessellation(group);
		}
	}
}
#endif //RAGE_SUPPORT_TESSELLATION_TECHNIQUES


#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
rmcLodGroup::TessellatedDrawMode DECLARE_MTR_THREAD rmcLodGroup::sm_TessellatedDrawMode=UNTESSELLATED_AND_TESSELLATED;
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

rmcLodGroup::rmcLodGroup()
{
	for (int i=0; i<LOD_COUNT; i++) {
		m_Lod[i] = 0;
		m_LodThresh[i] = 0;
// TODO: old way of settings the mask - clean up when everything confirmed to work properly
//		m_BucketMask[i] = ~0U;

		m_BucketMask[i] = 0;
	}
}


void rmcLodGroup::CloneWithNewVertexData( const rmcLodGroup &cloneme, int lod, const atArray<int>& modelIdx, const atArray<int>& geomIdx, bool createVB, void* preAllocatedMemory, int& memoryAvailable )
{
	m_Sphere = cloneme.m_Sphere;
	m_AABB = cloneme.m_AABB;

	m_Lod[lod] = rage_contained_new rmcLod(cloneme.m_Lod[lod]->GetCount());
	m_Lod[lod]->CloneWithNewVertexData( *(cloneme.m_Lod[lod]), modelIdx, geomIdx, createVB, preAllocatedMemory, memoryAvailable );

	m_LodThresh[lod] = cloneme.m_LodThresh[lod];
	m_BucketMask[lod] = cloneme.m_BucketMask[lod];
}

u32 rmcLodGroup::GetBucketMask(int lod) const	
{ 
	for (int i=lod ; i<LOD_COUNT ; i++)
	{
		if ( m_BucketMask[i] )
		{
			return m_BucketMask[i] & ~LODGROUP_MASK; 
		}
	}

	return 0;
}


void rmcLodGroup::Shutdown() {
	for (int i=0; i<LOD_COUNT; i++) {
		delete m_Lod[i];
		m_Lod[i] = NULL;
	}
}


rmcLodGroup::~rmcLodGroup() {
	// Don't call Shutdown() if we're inside a resource and being destroyed.
#if __XENON || __PS3
	if (sysMemAllowResourceAlloc == 0)
#endif // __XENON || __PS3
	{
		Shutdown();
	}
}

#if MESH_LIBRARY
void rmcLodGroup::LoadLod(fiTokenizer &t,const char *match,int lod, const grmShaderGroup *pShaderGroup, int lodMask, bool optimize, int extraVerts )
{
	// Are we supposed to load this LOD?
	if (lodMask & (1 << lod))
	{
		// Yeah, load it here.
		char buf[128];
		t.MatchToken(match); 

		if (!t.CheckToken("none")) {
			t.GetToken(buf,sizeof(buf));
			int count = 1;
			bool staleOldFormat = true;
			if (atoi(buf)) {
				count = atoi(buf);
				staleOldFormat = false;
			}

			m_Lod[lod] = rage_contained_new rmcLod(count);

			for (int i=0; i<count; i++) {
				int boneIndex = 0;
				if (!staleOldFormat) {
					t.GetToken(buf,sizeof(buf));
					boneIndex = t.GetInt();
				}

				u8 mask = 0xFF;
				if (t.CheckToken("mask"))
				{
					char maskBuf[128];
					t.GetToken(maskBuf,sizeof(maskBuf));
					u32 out32 = (strtoul(maskBuf, NULL, 16));

					Assign(mask, out32);
				}

				m_Lod[lod]->SetModel(t,i,buf,boneIndex,lod,pShaderGroup,optimize,extraVerts,mask);
			}
		}
		m_LodThresh[lod] = t.GetFloat();
	}
	else
	{
		// No - skip it.
		sysMemStartTemp();
		{
			rmcLodGroup dummy;

			Assert(lod < 32 && lod >= 0);		// Sanity check to avoid infinite loops.

			dummy.LoadLod(t, match, lod, pShaderGroup, -1);
		}
		sysMemEndTemp();
	}
}



void rmcLodGroup::Load(fiTokenizer &t, const grmShaderGroup *pShaderGroup, int lodMask, int lodRemap) {
	bool optimize = true;
	if( t.CheckToken("optimize") )
	{
		optimize = t.GetInt() ? true: false;
	}

	int extraverts = 0;
	if( t.CheckToken("extraverts") )
	{
		extraverts = t.GetInt();
		const int MAX_EXTRA_VERTS = 255;
		Assert( extraverts <= MAX_EXTRA_VERTS );
		if( extraverts > MAX_EXTRA_VERTS )
			extraverts = MAX_EXTRA_VERTS;
	}

	int startPosition = t.GetStreamPosition();
	LoadLod(t,"high",(lodRemap == -1) ? LOD_HIGH : lodRemap, pShaderGroup, lodMask, optimize, extraverts);
	LoadLod(t,"med",(lodRemap == -1) ? LOD_MED : lodRemap, pShaderGroup, lodMask, optimize, extraverts);
	LoadLod(t,"low",(lodRemap == -1) ? LOD_LOW : lodRemap,pShaderGroup, lodMask, optimize, extraverts);
	LoadLod(t,"vlow",(lodRemap == -1) ? LOD_VLOW : lodRemap, pShaderGroup, lodMask, optimize, extraverts);

	if (lodRemap != -1 && m_Lod[lodRemap] == NULL && ((1 << lodRemap) != lodMask))
	{
		// Go back to the beginning and parse again.
		t.SetStreamPosition(startPosition);
		Load(t, pShaderGroup, 1 << lodRemap, lodRemap);
		return;
	}

	Vector3 cullSphere(0,0,0), boxMin(0,0,0), boxMax(0,0,0);
	float cullRadius;

	if (t.CheckToken("center"))
		t.GetVector(cullSphere);
	else
		cullSphere.Zero();
	cullRadius = t.MatchFloat("radius");
	if (t.CheckToken("box")) {
		t.GetVector(boxMin);
		t.GetVector(boxMax);
	}
	m_Sphere = spdSphere(RCC_VEC3V(cullSphere),ScalarVFromF32(cullRadius));
	m_AABB = spdAABB(RCC_VEC3V(boxMin),RCC_VEC3V(boxMax));

#if __ASSERT
	bool lodFound = false;
	for (int i=0; i<LOD_COUNT && !lodFound; i++)
		lodFound = (m_Lod[i] != NULL) || lodFound;
	Assertf(lodFound || (lodMask != -1), "LOD groups must contain at least one mesh, none found in file:\n\n%s\n\nIf this is intentional, change your export rule so it doesn't include a Mesh section.\n", t.GetStream()->GetName());
#endif

//
//#if !__FINAL && __PAGING
//	SetRemapInfo(group);
//#endif
}
#endif		// MESH_LIBRARY

bool rmcLodGroup::IsEmpty(){
	for (int i=0; i<LOD_COUNT; i++)
	{
		if (m_Lod[i])
			return false;
	}
	
	return true;
}

void rmcLodGroup::ComputeBucketMask(const grmShaderGroup &group) {
	for (int i=0; i<LOD_COUNT; i++)
	{
		if (m_Lod[i])
			m_BucketMask[i] = m_Lod[i]->ComputeBucketMask(group);
		}
	}


int rmcLodGroup::GetCurrentLOD() const
{
	return GetLodIndex();
}

//Ideally, each LOD should have a bit to specify whether it's instanced or not. But it's fairly tightly packed at the moment,
//so for now, we do it the slower way. As usage of instanced geometry increases, we may need to re-visit.
bool rmcLodGroup::IsLodInstanced(int lodIdx, const grmShaderGroup &shaderGroup, bool &hasNonInstancedShader) const
{
	bool isInstanced = false;
	hasNonInstancedShader = false;

	//Check current lod if it has instanced geometry.
	if(ContainsLod(lodIdx))
	{
		const rmcLod &lod = GetLod(lodIdx);
		for(int i = 0; i < lod.GetCount(); ++i)
		{
			if(grmModel *model = lod.GetModel(i))
			{
				int count = model->GetGeometryCount();
				for(int index = 0; index < count; ++index)
				{
					const int shaderIndex = model->GetShaderIndex(index);
					if(shaderGroup.GetShader(shaderIndex).IsInstanced())
						isInstanced = true;
					else
						hasNonInstancedShader = true;
				}
			}
		}
	}

	return isInstanced;
}


void rmcLodGroup::DrawSingle(const grmShaderGroup &group,const Matrix34 &mtx,u32 bucketMask,int idx DRAWABLE_STATS_ONLY(, u16 drawableStat)) const {
	rmcLod &lod = *m_Lod[idx];

	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH_LOD(m_BucketMask[idx], bucketMask))
	{
		bool matrixSet=false;
		for (int i=0; i<lod.GetCount(); i++) {
			if (lod.GetModel(i)) {
				grmModel &model = *lod.GetModel(i);

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
				DRAWABLE_STATS_ONLY((void)drawableStat);
				if (!matrixSet)
				{
					grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));				
					matrixSet = true;
				}
				model.Draw(group,bucketMask,idx);
#else // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

				if ((sm_TessellatedDrawMode == TESSELLATED_ONLY && model.GetTessellatedGeometryCount() == 0)
				||  (sm_TessellatedDrawMode == UNTESSELLATED_ONLY && model.GetUntessellatedGeometryCount() == 0))
					continue;

				if (!matrixSet)
				{
					grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));				
					matrixSet = true;
				}

				switch(sm_TessellatedDrawMode)
				{
				case UNTESSELLATED_ONLY:
					{
						model.DrawUntessellatedPortion(group,bucketMask,idx DRAWABLE_STATS_ONLY(, drawableStat));
						break;
					}
				case TESSELLATED_ONLY:
					{
						model.DrawTessellatedPortion(group,bucketMask,idx,true DRAWABLE_STATS_ONLY(, drawableStat));
						break;
					}
				case UNTESSELLATED_AND_TESSELLATED:
					{
						model.DrawUntessellatedPortion(group,bucketMask,idx DRAWABLE_STATS_ONLY(, drawableStat));
						model.DrawTessellatedPortion(group,bucketMask,idx,true DRAWABLE_STATS_ONLY(, drawableStat));
						break;
					}
				case UNTESSELLATED_AND_TESSELLATED_AS_UNTESSELLATED:
					{
						model.DrawUntessellatedPortion(group,bucketMask,idx DRAWABLE_STATS_ONLY(, drawableStat));
						model.DrawTessellatedPortion(group,bucketMask,idx,false DRAWABLE_STATS_ONLY(, drawableStat));
						break;
					}
				case TESSELLATED_ONLY_AS_UNTESSELLATED:
					{
						model.DrawTessellatedPortion(group,bucketMask,idx,false DRAWABLE_STATS_ONLY(, drawableStat));
						break;
					}
				}				
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES
			}
		}
	}
}


void rmcLodGroup::DrawSingle(const grmShaderGroup &group,grcInstanceBuffer *ib,u32 bucketMask,int idx) const
{
	rmcLod &lod = *m_Lod[idx];

	AssertMsg(&lod, "Cannot draw a NULL LOD!\nAt least one mesh needs to be present for at least one LOD level.\n");

	if (BUCKETMASK_MATCH(m_BucketMask[idx], bucketMask))
	{
		for (int i=0; i<lod.GetCount(); i++) {
			if (lod.GetModel(i)) {
				grmModel &model = *lod.GetModel(i);
				model.Draw(group,ib,bucketMask);
			}
		}
	}
}


void rmcLodGroup::DrawMulti(const grmShaderGroup &group,const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lodIdx DRAWABLE_STATS_ONLY(, u16 drawableStat) ASSERT_ONLY(, const char* debugName)) const {
	rmcLod &lod = *m_Lod[lodIdx];

	// This seems to occur if a V mesh has a 0 distance set. (sample_expressions)
	if(m_Lod[lodIdx] == NULL)
	{
		Errorf("Trying to draw lod which does not exist - lod '%d':",lodIdx);
		return;
	}

	if (BUCKETMASK_MATCH_LOD(m_BucketMask[lodIdx], bucketMask))
	{
		int lastMtx = -2;
		for (int i=0; i<lod.GetCount(); i++) {
			if (lod.GetModel(i)) {
				grmModel &model = *lod.GetModel(i);

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
				if ((sm_TessellatedDrawMode == TESSELLATED_ONLY && model.GetTessellatedGeometryCount() == 0)
				||  (sm_TessellatedDrawMode == UNTESSELLATED_ONLY && model.GetUntessellatedGeometryCount() == 0))
					continue;
#endif// RAGE_SUPPORT_TESSELLATION_TECHNIQUES

				int idx = model.GetMatrixIndex();
				Assertf(idx>=0&&idx<ms.GetMatrixCount(), "Not enough matrices in model (idx: %d, Total: %d). You probably tried to draw an articulated drawable without a skeleton on model %s (%s).", idx, ms.GetMatrixCount(), debugName, debugRenderModel);
				if (model.GetSkinFlag()) {
					if (lastMtx != -1)
					{
						grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtx));
						lastMtx = -1;
					}
				#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
					DRAWABLE_STATS_ONLY((void)drawableStat);
					model.DrawSkinned(group,ms,bucketMask,lodIdx);
				#else
					switch(sm_TessellatedDrawMode)
					{
					case UNTESSELLATED_ONLY:
						{
							model.DrawUntessellatedPortionSkinned(group,ms,bucketMask,lodIdx DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case TESSELLATED_ONLY:
						{
							model.DrawTessellatedPortionSkinned(group,ms,bucketMask,lodIdx,true DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case UNTESSELLATED_AND_TESSELLATED:
						{
							model.DrawUntessellatedPortionSkinned(group,ms,bucketMask,lodIdx DRAWABLE_STATS_ONLY(, drawableStat));
							model.DrawTessellatedPortionSkinned(group,ms,bucketMask,lodIdx,true DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case UNTESSELLATED_AND_TESSELLATED_AS_UNTESSELLATED:
						{
							model.DrawUntessellatedPortionSkinned(group,ms,bucketMask,lodIdx DRAWABLE_STATS_ONLY(, drawableStat));
							model.DrawTessellatedPortionSkinned(group,ms,bucketMask,lodIdx,false DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case TESSELLATED_ONLY_AS_UNTESSELLATED:
						{
							model.DrawTessellatedPortionSkinned(group,ms,bucketMask,lodIdx,false DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					}				
				#endif
				}
				else {
					if (idx != lastMtx) {
						// This is a bit slower than I'd like because it has to untranspose the matrix and re-add the offset back in again.
						Matrix34 tmp;
						Assertf((idx >= 0) && (idx < ms.GetMatrixCount()), "Invalid matrix index set %d/%d in model %s, drawable %s", idx, ms.GetMatrixCount(), rmcLodGroup::debugRenderModel, rmcDrawable::sm_DebugDrawableName);
						ms.ComputeMatrix(tmp,lastMtx = idx);
						tmp.Dot(mtx);
						grcViewport::SetCurrentWorldMtx(RCC_MAT34V(tmp));
					}
				#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
					model.Draw(group,bucketMask,lodIdx);
				#else
					switch(sm_TessellatedDrawMode)
					{
					case UNTESSELLATED_ONLY:
						{
							model.DrawUntessellatedPortion(group,bucketMask,lodIdx DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case TESSELLATED_ONLY:
						{
							model.DrawTessellatedPortion(group,bucketMask,lodIdx,true DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case UNTESSELLATED_AND_TESSELLATED:
						{
							model.DrawUntessellatedPortion(group,bucketMask,lodIdx DRAWABLE_STATS_ONLY(, drawableStat));
							model.DrawTessellatedPortion(group,bucketMask,lodIdx,true DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case UNTESSELLATED_AND_TESSELLATED_AS_UNTESSELLATED:
						{
							model.DrawUntessellatedPortion(group,bucketMask,lodIdx DRAWABLE_STATS_ONLY(, drawableStat));
							model.DrawTessellatedPortion(group,bucketMask,lodIdx,false DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					case TESSELLATED_ONLY_AS_UNTESSELLATED:
						{
							model.DrawTessellatedPortion(group,bucketMask,lodIdx,false DRAWABLE_STATS_ONLY(, drawableStat));
							break;
						}
					}
				#endif
				}
			}
		}
	}
}


#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
void rmcLodGroup::SetTessellatedDrawMode(TessellatedDrawMode eMode)
{
	sm_TessellatedDrawMode = eMode;
}


rmcLodGroup::TessellatedDrawMode rmcLodGroup::GetTessellatedDrawMode()
{
	return sm_TessellatedDrawMode;
}


void rmcLodGroup::SortForTessellation(const grmShaderGroup &group)
{
	int i;

	for(i=0; i<LOD_COUNT; i++)
	{
		if(m_Lod[i])
		{
			m_Lod[i]->SortForTessellation(group);
		}
	}
}

#endif // !RAGE_SUPPORT_TESSELLATION_TECHNIQUES

rmcLodGroup::rmcLodGroup(datResource &rsc): m_Sphere(rsc), m_AABB(rsc)
{
	for( int i = 0; i < LOD_COUNT; ++i)
	{
		if( m_BucketMask[i] == ~0U )
			m_BucketMask[i] = 0;
	}

}

void rmcLod::ResourcePageIn(datResource &rsc) {
	::new (this) rmcLod(rsc);
}


rmcLod::rmcLod(datResource &rsc) : m_Models(GRCARRAY_PLACEELEMENTS,rsc) {
}

IMPLEMENT_PLACE(rmcLod)



#if __DECLARESTRUCT
void rmcLod::DeclareStruct(datTypeStruct &s) {
	STRUCT_BEGIN(rmcLod);
	STRUCT_FIELD(m_Models);
	STRUCT_END();
}


void rmcLodGroup::DeclareStruct(datTypeStruct &s) {
	STRUCT_BEGIN(rmcLodGroup);
	STRUCT_FIELD(m_Sphere);
	STRUCT_FIELD(m_AABB);
	STRUCT_FIELD(m_Lod);
	STRUCT_FIELD(m_LodThresh);
	STRUCT_FIELD(m_BucketMask);
	STRUCT_END();
}
#endif

#if __WIN32PC && !__FINAL && 0
void rmcLodGroup::TransformCullInfo( const Matrix34& worldTransform )
{
	worldTransform.Transform(m_Sphere.m_centerAndRadius);
	worldTransform.Transform(m_AABB.m_min);
	worldTransform.Transform(m_AABB.m_max);
}

#endif
