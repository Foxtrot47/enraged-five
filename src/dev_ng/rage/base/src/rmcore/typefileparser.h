//
// rmcore/typefileparser.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef RMCORE_TYPEFILEPARSER_H
#define RMCORE_TYPEFILEPARSER_H

#include "data/base.h"
#include "data/callback.h"
#include "atl/array.h"

namespace rage {

class fiTokenizer;

struct rmcTypeFileCbData {
	rmcTypeFileCbData() : m_EntityName(0), m_T(0) {}
	
	const char *m_EntityName;
	fiTokenizer *m_T;
};

// PURPOSE
//	parses a type file and performs callbacks when something needs to be loaded in.
//	Meant to be instanced on the stack
//
//	Once an entityname is found that matches, it is up to the callback to process all information
//	that is needed for that entity before returning (including the {} brackets if used).
// EXAMPLE
//	In order for this to work, the .type file format should be something like this:
//
//	Version: 103
//	SectionName {
//		EntityName *****ENTITY DATA HERE******
//	}
//
// <FLAG Component>
class rmcTypeFileParser : public datBase {
public:
	rmcTypeFileParser();
	~rmcTypeFileParser();

	// For sectionName, use one of the enums below (or one from a derived class)
	//	Set entityName to "all" to process all entities within a section
	void RegisterLoader( const char *sectionName, const char *entityName, datCallback loadCb, bool allowDuplicateSections=false );

	// Process the type file
	bool ProcessTypeFile( fiTokenizer &T, bool stopAtBracket = false );

	// Reset the type file entries
	void Reset();

	// Query to see if parser is empty (don't care about anything in the file)
	bool IsEmpty() { return m_SectionData.GetCount() == 0; }

	// Set a flag to indicate whether or not you want a bunch of warning messages
	void SetWarningSpew( bool warn ) { m_WarningSpew = warn; }

	// WIN32 builds require this to be public instead of protected for use in structures
	enum { TFP_MAX_SECTIONS=14, TFP_MAX_ENTITIES=25, TFP_MAX_NAMELEN=128 };

protected:
	struct rmEntityLoaderData {
		char		m_Name[TFP_MAX_NAMELEN];
		datCallback	m_LoadCb;
	};

	struct rmSectionLoaderData {
		char												m_Name[TFP_MAX_NAMELEN];
		u8													m_AllowDuplicates;
		u8													m_Activated;
		atFixedArray<rmEntityLoaderData, TFP_MAX_ENTITIES>	m_EntityData;
	};

	// Set to true if you want a lot of warning messages about missing sections
	bool													m_WarningSpew;

	atFixedArray<rmSectionLoaderData, TFP_MAX_SECTIONS>		m_SectionData;
};

}	// namespace rage

#endif	// RMCORE_TYPEFILEPARSER_H
