//
// rmcore/drawable.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef RMCORE_DRAWABLE_H
#define RMCORE_DRAWABLE_H

#include "lodgroup.h"

#include "paging/base.h"
#include "data/resource.h"
#include "data/struct.h"
#include "grblendshapes/blendshapes_config.h"
#include "grcore/viewport.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "mesh/mesh_config.h"
#include "grprofile/drawmanager.h"
#if HACK_GTA4
#include "atl/array.h"
#endif 

#define DRAWABLE_MAX_SHADERS		(48)
#define DRAWABLE_MAX_CONTAINER_SIZE	(48 * 1024)
#define DRAWABLE_MAX_MODELS			(24)	// do not allow drawables with far too many models (e.g. rayfire-style objects)
#define DRAWABLE_MAX_GEOMS			(64)	// do not allow drawables with far too many geometries (e.g. rayfire-style objects)

namespace rage {


class fiTokenizer;
class crJointData;
class crSkeleton;
class crSkeletonData;
class rmcModel;
class atBitSet;
class grmEdgeModel;
class grbTargetManager;
class grcInstanceBuffer;
class grcInstanceBufferList;

class rmcTypeFileParser;
struct rmcTypeFileCbData;

class rmcDrawableBase: public pgBase {
public:
	rmcDrawableBase();

	// Use Delete, not delete, to destroy rmcDrawables properly when streaming.
	// Unfortunately we can't make the destructor private because some legacy
	// code contains rmcDrawables.
	virtual ~rmcDrawableBase();

	rmcDrawableBase(class datResource&);
	rmcDrawableBase(class datResource &rsc, class grmShaderGroup *group);
	// Use Delete, not delete, to destroy rmcDrawables properly when streaming.
	// Unfortunately we can't make the destructor private because some legacy
	// code contains rmcDrawables.
	virtual void Delete();
	
#if MESH_LIBRARY
	virtual bool Load(const char *basename,rmcTypeFileParser *parser=0,bool configParser=true) = 0;
#endif

protected:
#if MESH_LIBRARY
	// Internal helper function, should not be called directly by new code.
	virtual bool Load(fiTokenizer & T,rmcTypeFileParser *parser=0,bool configParser=true) = 0;
	// Internal helper function, should not be called directly by new code.
	virtual bool LoadFromParser(fiTokenizer & T,rmcTypeFileParser *parser=0,bool configParser=true) = 0;
#endif

public:
	virtual void Draw(const Matrix34 &mtx,u32 bucketMask,int lod, u16 stats=0) const = 0;

	virtual void DrawSkinned(const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lod, u16 stats=0) const = 0;

	virtual void DrawNoShaders(const Matrix34& mtx, u32 bucketMask,int lod) const = 0;

	enum EnumNoShadersSkinned 
	{
		RENDER_SKINNED		=0x1,								// render only skinned objects
		RENDER_NONSKINNED	=0x2,								// render non skinned objects
		RENDER_ALL			=RENDER_SKINNED|RENDER_NONSKINNED	// render both skinned and nonskinned objects
	};
	virtual void DrawSkinnedNoShaders(const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lod,EnumNoShadersSkinned renderOnly) const = 0;

	virtual grcCullStatus IsVisible(const Matrix34 &mtx, const grcViewport &vp, u8 &lod, float * zdist=NULL) const = 0;
	virtual grcCullStatus IsVisible(const Matrix44 &mtx, const grcViewport &vp, u8 &lod, float * zdist=NULL) const = 0;

	// RETURNS: Shader group (an array of all shaders referenced by all models in this drawable)
	grmShaderGroup& GetShaderGroup() { return *m_ShaderGroup; }
	const grmShaderGroup& GetShaderGroup() const { Assert(GetShaderGroupPtr()); return *m_ShaderGroup; }

	grmShaderGroup* GetShaderGroupPtr() const { return m_ShaderGroup; }

	//Instancing Support
	bool HasInstancedGeometry() const { return (m_ShaderGroup ? m_ShaderGroup->HasInstancedShaders() : false); }

#if __BANK
	const pgDictionary<grcTexture>* GetTexDictSafe() const { return m_ShaderGroup ? m_ShaderGroup->GetTexDict() : NULL; }
#endif // __BANK

	// RETURNS: Bitmask of all draw buckets referenced by all shaders in this drawable
	virtual u32 GetBucketMask(int lod) const = 0;

	// RETURNS: Pointer to per-type shader data, if any.  If you want all instances of a particular
	// type to use the same shader data, put it here and pass this value to the Draw... functions.
	// grmShaderData *GetShaderData() const;

	// PURPOSE: Assign the shader group used by the drawable.  Used internally by loaders.
	inline void SetShaderGroup( grmShaderGroup *groups );

	// PURPOSE: Only load certain LODs and skip the rest.
	static void SetLodLoadFilter(int lodBitMask = -1);

	static int GetLodLoadFilter()					{ return sm_LodLoadFilter; }

	static void SetLodForceRemap(int remapLod)		{ sm_LodForceRemap = remapLod; }

	static int GetLodForceRemap()					{ return sm_LodForceRemap; }

protected:
#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

	mutable datOwner<grmShaderGroup> m_ShaderGroup;

	// Bitmask of LODs that should be loaded (everything not in this mask will be skipped).
	static int sm_LodLoadFilter;

	// If set, force the loaded LOD to always be this level.
	// Should preferably always be -1 (=don't change) unless sm_LodLoadFilter only has one bit set.
	static int sm_LodForceRemap;
};



/*
PURPOSE:
	An rmcDrawable contains up to four levels of detail; each level of detail
	consists of zero or more models.  Each model within the LOD can be bound to
	a different bone, allowing complex objects like cars to render with a single
	draw call.  The rmcDrawable class also contains a shader group, which is an
	array of all shaders used by all models within the drawable.

	The only reason we derive from Base is so that the vptr is at a known location
	for resources.

	-- NOTE ABOUT VERSIONS OF .TYPE FILES:
	--	Version: 103 is shader templates + expandable format via rmcTypeFileParser	
	--  Version numbers lower than 103 are no longer supported
	--  Files with no version number are no longer supported (original version)

	<FLAG Component>
*/
class rmcDrawable: public rmcDrawableBase {
public:
	rmcDrawable();
	virtual ~rmcDrawable();
	rmcDrawable(class datResource&);
	rmcDrawable(class datResource&, grmShaderGroup*);
	DECLARE_PLACE(rmcDrawable);
	void Delete();
	int Release() const { delete this; return 0; }	// Not true reference counting yet!

	static const int RORC_VERSION;

	/* PURPOSE: Load a drawable from a file.  This is the version most people will want to use.
		PARAMS: basename - Filename of the type file (default extension is .type).
			parser - Parser object to use; if null (the default), use a default parser.
			configParser - If true (the default), add the standard sections to the passed-in parser
		RETURNS: True if successful, false on failure (file not found) */
	bool Load(const char *basename,rmcTypeFileParser *parser=0,bool configParser=true);

#if MESH_LIBRARY
	// Internal helper function, should not be called directly by new code.
	virtual bool Load(fiTokenizer & T,rmcTypeFileParser *parser=0,bool configParser=true);

	// Internal helper function, should not be called directly by new code.
	virtual bool LoadFromParser(fiTokenizer & T,rmcTypeFileParser *parser=0,bool configParser=true);

	// Internal helper function, should not be called directly by new code.
	virtual void LoadMesh( rmcTypeFileCbData *data );

	// Internal helper function, should not be called directly by new code.
	virtual void LoadSkel( rmcTypeFileCbData *data );

	// Internal helper function, should not be called directly by new code.
	virtual void LoadSkelProperties( rmcTypeFileCbData *data);

	// Internal helper function, should not be called directly by new code.
	virtual void LoadJointLimits( rmcTypeFileCbData *data );

	// Internal helper function, should not be called directly by new code.
	virtual void LoadShader( rmcTypeFileCbData *data );

	// Internal helper function, should not be called directly by new code.
	virtual void LoadEdgeModel( rmcTypeFileCbData *data );
#endif

	// Internal helper function, should not be called directly by new code.
	// virtual void LoadBlendShapes( rmcTypeFileCbData *data );

#if ENABLE_BLENDSHAPES
	// PURPOSE: Apply blend shape offsets to the drawable model
	void ApplyBlendShapeOffsets(grbTargetManager &blendTargetManager);

	// PURPOSE: Remove the blend shape offsets from the drawable model
	void RemoveBlendShapeOffsets(grbTargetManager &blendTargetManager);
#endif // ENABLE_BLENDSHAPES

	/* PURPOSE: Draw a drawable (cannot be skinned or have per-instance cpv data)
		PARAMS: 
			data - per-instance shader local variable array (can be null if drawable's shaders don't access any locals)
			mtx - Root matrix of object.  If object has a skeleton, this is actually the address of the first
				matrix in an array of matrices.
			bucket - Draw bucket to render with (0-31).  Only shaders with a matching draw bucket will actually render;
				this is one way to control draworder within a model without having to break it into smaller pieces.
			lod - Level of detail, from zero (LOD_HIGH) to 3 (LOD_VLOW)
			enables - If zero (the default), draw all models at that lod within the drawable.  If nonzero, only
				draw the models which are specifically enabled.
	*/
	void Draw(const Matrix34 &mtx,u32 bucketMask,int lod, u16 drawableStat=0) const;

	/* PURPOSE: Draw a drawable (must be skinned)
		PARAMS: 
			data - per-instance shader local variable array (can be null if drawable's shaders don't access any locals)
			skel - Skeleton to render skinned object with
			bucket - Draw bucket to render with (0-31).  Only shaders with a matching draw bucket will actually render;
				this is one way to control draworder within a model without having to break it into smaller pieces.
			lod - Level of detail, from zero (LOD_HIGH) to 3 (LOD_VLOW)
			enables - If zero (the default), draw all models at that lod within the drawable.  If nonzero, only
				draw the models which are specifically enabled.
	*/
	void DrawSkinned(const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lod, u16 drawableStat=0) const;

	void DrawInstanced(grcInstanceBufferList &list, u32 bucketMask, int lod) const;
	void DrawInstanced(grcInstanceBuffer *ib, u32 bucketMask, int lod) const;

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	void DrawNoShaders(const Matrix34& mtx, u32 bucketMask,int lod) const;
	void DrawSkinnedNoShaders(const Matrix34& mtx,const grmMatrixSet &ms,u32 bucketMask,int lod,EnumNoShadersSkinned renderOnly) const;
#else
	void DrawNoShaders(const Matrix34& mtx, u32 bucketMask,int lod) const;
	void DrawNoShadersTessellationControlled(const Matrix34& mtx, u32 bucketMask,int lodIndex) const;
	void DrawModelGeometries(const grmModel& model, int startIdx, int loopEnd, const Matrix34& mtx, u32 bucketMask) const;

	void DrawSkinnedNoShaders(const Matrix34& mtx,const grmMatrixSet &ms,u32 bucketMask,int lod,EnumNoShadersSkinned renderOnly) const;
	void DrawSkinnedNoShadersTessellationControlled(const Matrix34& mtx,const grmMatrixSet &ms,u32 bucketMask,int lod,EnumNoShadersSkinned renderOnly) const;
	void DrawModelGeometriesSkinned(const grmModel& model, int startIdx, int loopEnd, const Matrix34& mtx,const grmMatrixSet &ms, u32 bucketMask,EnumNoShadersSkinned renderOnly, int &lastMtx) const;

	static void ComputeGeometriesToDraw(const grmModel& model, int &startIndex, int &loopEnd);
#endif

	/* PURPOSE: Determine if the drawable is visible based on its cull sphere.
		PARAMS: mtx - World matrix for model
			vp - Viewport to test against
			lod - Returns suitable level-of-detail index
			zdist - If null (the default), ignored.  Otherwise receives perpendicular z distance from near clip plane
		RETURNS: See gfxCullStatus enumerant.  cullOutside means it's completely off-screen.
		NOTES: Every drawable parses a sphere radius and center from the .type file. */
	grcCullStatus IsVisible(const Matrix34 &mtx,const grcViewport &vp,u8 &lod, float *zDist = NULL) const;
	grcCullStatus IsVisible(const Matrix44 &mtx,const grcViewport &vp,u8 &lod, float *zDist = NULL) const;

	// RETURNS: The lod group object used by the drawable.
	const rmcLodGroup& GetLodGroup() const { return m_LodGroup; }

	// RETURNS: The lod group object used by the drawable.
	rmcLodGroup& GetLodGroup() { return m_LodGroup; }

	// RETURNS: The skeleton data (for bony models like cars, or skinned models like characters)
	crSkeletonData *GetSkeletonData() const { return m_SkeletonData; }

	// RETURNS: The joint data (limits on the bony and skinned models above)
	crJointData *GetJointData() const { return m_JointData; }

	/* PURPOSE: Get the index of the blend shape object specified by lodGroup and modelIndex
		PARAMS: lodGroup - an integer containing the index of the desired lod group
				modelIndex - an integer containing the index of the desired model
		RETURNS: An integer containing the index of the desired blend shape	*/
	int GetBlendShapeIndex(int lodGroup, int modelIndex) const;

	// RETURNS: The edge model information
	// grmEdgeModel *GetEdgeModel() { return m_EdgeModel; }
	// const grmEdgeModel *GetEdgeModel() const { return m_EdgeModel; }
	// grmEdgeModel* SetEdgeModel(grmEdgeModel *pNewEdgeModel);

	// RETURNS: Bitmask of all draw buckets referenced by all shaders in this drawable
	virtual u32 GetBucketMask(int lod) const { return m_LodGroup.GetBucketMask(lod); }

	// PURPOSE:	Loads a drawable saved out as a resource, optionally with texture
	//			dictionary as well.  These are both created by rage/base/tools/rorc.
	// PARAMS:	drawableName - Name of the drawable; must not have an extension, and "_drw"
	//				is automatically appended to it.
	//			texdictName - Name of the texture dictionary to load first, or NULL if we
	//				don't want to make a dictionary resident.  "_td" is automatically
	//				appended to the name, which also must not have an extension.
	// RETURNS:	Pointer to drawable object, if load was successful.
	static rmcDrawable* LoadResource(const char *drawableName,const char *texdictName = NULL);

	// PURPOSE: Swap drawing vertex buffers
	void SwapDrawBuffers();

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

#if __PFDRAW
	// PURPOSE : Performs profile drawing for the drawable.
	//
	// PARAMS : pSkel - Skeleton to use for skinned drawables (can be NULL)
	//			transformMat - Transformation matrix to use for drawables that have no skeleton available
	//			lodLevel - lod level of the drawable to perform the profile drawing for.
	virtual void ProfileDraw(const crSkeleton *pSkel, const Matrix34* transformMat = NULL, int lodLevel = 0) const;

	// PURPOSE: Draw the bounding box for this drawable.
	//
	// PARAMS:
	//			transformMat - Transformation matrix to use for drawables that have no skeleton available
	//			color - The Color32 to use for drawing.
	//			solid - Whether to draw it as a solid box.
	void ProfileDrawBoundingBox(const Matrix34* transformMat, const Color32& color, bool solid) const;
#endif

#if BASE_DEBUG_NAME
	virtual const char *GetDebugName(char * /*buffer*/, size_t /*bufferSize*/) const		{ return m_DebugName; }
#endif // BASE_DEBUG_NAME

#if RSG_RSC
	void SetDebugName(const char*);
#endif

	// PURPOSE:	Runs grmShader::Optimize on everything.
	void Optimize();

	void CloneWithNewVertexData( const rmcDrawable &cloneme, int lod, const atArray<int>& modelIdx, const atArray<int>& geomIdx, bool createVB, void* preAllocatedMemory, int& memoryAvailable );

	bool IsSkinned() const 
	{
		if(!m_LodGroup.ContainsLod(LOD_HIGH)) return false;
		return m_LodGroup.GetLodModel0(LOD_HIGH).IsModelRelative(); 
	}

	u32 GetHandleIndex() const { return m_HandleIndex & 0x7FFF; }
	void SetHandleIndex(u32 h) { Assign(m_HandleIndex,(m_HandleIndex & 0x8000) | (h & 0x7FFF)); }

	// sysMemContainerData& GetContainer() { return m_Container; }
	// const sysMemContainerData& GetContainer() const { return m_Container; }
	u32 GetContainerSize() const { return m_ContainerSizeQW << 4; }

	void MakePpuOnly() { m_HandleIndex |= 0x8000; }
	bool GetPpuOnly() const { return (m_HandleIndex & 0x8000) != 0 || !m_ContainerPtr; }

#if __DEV
	__THREAD static const char* sm_DebugDrawableName;
#endif // __DEV

protected:
	void CheckSizeForSpu(datResource&);
	datOwner<crSkeletonData>			m_SkeletonData;
	
	rmcLodGroup							m_LodGroup;

	datOwner<crJointData>				m_JointData;

	u16 m_HandleIndex, m_ContainerSizeQW;
	void *m_ContainerPtr;
	const char *m_DebugName;

#if RSG_ORBIS && (!__FINAL || !__NO_OUTPUT) // Using these defines rather than GRCCONTEXTGNM_BASIC_ERROR_HANDLING to avoid including headers
	void SaveDebugInfoInCommandBuffer() const;
#else
	__forceinline void SaveDebugInfoInCommandBuffer() const { DEV_ONLY(sm_DebugDrawableName = m_DebugName); }
#endif
};

inline void rmcDrawableBase::SetShaderGroup( grmShaderGroup *group ) {
	m_ShaderGroup = group;
}

/* inline grmShaderData *rmcDrawableBase::GetShaderData() const {
	return m_ShaderGroup?m_ShaderGroup->GetLocals():NULL;
}*/

}	// namespace rage

#endif
