//
// rmcore/lodgroup.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef RMCORE_LODGROUP_H
#define RMCORE_LODGROUP_H

#include "vector/vector3.h"
#include "grmodel/model.h"
#include "atl/array.h"
#include "data/struct.h"
#include "system/container.h"
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"

namespace rage {


// Use bit 16 to set drawable lod index outside of render context: only 16 bits are used by the game (and passed onto spu). See Renderer.h for full list.
#define		LODGROUP_MASK			(1 << 16)


class grmShaderGroup;
class Matrix34;
class atBitSet;

// If you ever re-arrange the LOD enums, change LodCmp function to compensate
enum { LOD_HIGH, LOD_MED, LOD_LOW, LOD_VLOW, LOD_COUNT };

//	Compare LOD's -- returns +1 if A is higher lod than B, 0 if lods are same, -1 if A is lower lod than B
inline int rmcLodCmp(int lodA, int lodB) { return (lodA < lodB) ? 1 : (lodA == lodB) ? 0 : -1; }

extern bool rmcEnableModelStreaming;
extern bool rmcEnableModelPaginging;

class rmcLod {
public:
	rmcLod(int count);
	~rmcLod();
	DECLARE_PLACE(rmcLod);
	rmcLod(datResource&);
#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	void SortForTessellation(const grmShaderGroup &group);
#endif //RAGE_SUPPORT_TESSELLATION_TECHNIQUES

	void ResourcePageIn(class datResource&);
	grmModel* GetModel(int idx) const;
	const datOwner<grmModel>* GetModelPtrArray() const {return &m_Models[0];}
	void SetModel(fiTokenizer &t, int idx,const char *name,int mtxIndex,int lod,const grmShaderGroup *pShaderGroup = 0,bool optimize = true, int extraVerts = 0, u8 mask = 0xFF);
	int GetCount() const { return m_Models.GetCount(); }
	u32 ComputeBucketMask(const grmShaderGroup &group) const;
	void Optimize(grmShaderGroup&);
	void Shutdown();

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

	void CloneWithNewVertexData( const rmcLod &cloneme, const atArray<int>& modelIdx, const atArray<int>& geomIdx, bool createVB, void* preAllocatedMemory, int& memoryAvailable );

protected:
	grcArray< datOwner<grmModel> > m_Models;
};


class rmcLodGroup {
public:
#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	enum TessellatedDrawMode
	{
		UNTESSELLATED_ONLY = 0x1,
		TESSELLATED_ONLY = 0x2,
		// The default mode.
		UNTESSELLATED_AND_TESSELLATED = 0x3,
		// Special modes where tessellated meshes are rendered as normal ones (we might want to optimise for reflection maps etc).
		UNTESSELLATED_AND_TESSELLATED_AS_UNTESSELLATED = 0x4,
		TESSELLATED_ONLY_AS_UNTESSELLATED = 0x5,
	};
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

#if __DEV
	static __THREAD const char* debugRenderModel;
#endif // __DEV

public:
	rmcLodGroup();
	rmcLodGroup(class datResource&);
	DECLARE_PLACE(rmcLodGroup);
	~rmcLodGroup();

	void Load(class fiTokenizer &t, const grmShaderGroup *pShaderGroup = 0, int lodMask = -1, int lodRemap = -1);
	void Shutdown();

	void DrawSingle(const grmShaderGroup &group,const Matrix34 &mtx,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
	void DrawSingle(const grmShaderGroup &group,grcInstanceBuffer *ib,u32 bucketMask,int lod) const;
	void DrawMulti(const grmShaderGroup &group,const Matrix34 &mtx,const grmMatrixSet &ms,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat) ASSERT_ONLY(, const char* debugName)) const;

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	static void SetTessellatedDrawMode(TessellatedDrawMode eMode);
	static TessellatedDrawMode GetTessellatedDrawMode();
	void SortForTessellation(const grmShaderGroup &group);
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

	int ComputeLod(float zDist) const {
		int lod = LOD_HIGH;
		while (lod < LOD_VLOW && (zDist > m_LodThresh[lod] || m_Lod[lod] == NULL))
			++lod;
		return lod;
	}

	bool ContainsLod(int lod) const { return (lod >= 0 && lod < m_Lod.size() && m_Lod[lod] != 0); }

	const rmcLod& GetLod(int lod) const { return *m_Lod[lod]; }
	rmcLod& GetLod(int lod) { return *m_Lod[lod]; }
	atRangeArray<datOwner<rmcLod>,LOD_COUNT>& GetLodArray() { return m_Lod; }
	
	int GetCurrentLOD() const;

	const grmModel& GetLodModel0(int lod) const 
	{
		Assertf(m_Lod[lod], "Trying to access lod level %d, but there isn't one", lod);
		return *GetLod(lod).GetModel(0); 
	}

	grmModel& GetLodModel0(int lod)
	{
		Assertf(m_Lod[lod], "Trying to access lod level %d, but there isn't one", lod);
		return *GetLod(lod).GetModel(0); 
	}

	const spdSphere& GetCullSphereV() const { return m_Sphere; }

	const Vector3& GetCullSphere() const { return (Vector3&)m_Sphere; }

	void SetCullSphere( const Vector3 &sph ) { m_Sphere = spdSphere(VECTOR3_TO_VEC3V(sph),GetCullRadiusV()); }

	float GetCullRadius() const { return m_Sphere.GetRadiusf(); }

	ScalarV_Out GetCullRadiusV() const { return m_Sphere.GetRadius(); }

	void SetCullRadius( float rad ) { m_Sphere.SetRadiusf(rad); }

	float GetLodThresh(int i) const { return m_LodThresh[i]; }

	void SetLodThresh(int i,float f) { m_LodThresh[i] = f; }

	void GetBoundingBox( Vector3 &boxMin, Vector3 &boxMax ) const { boxMin.Set( m_AABB.GetMinVector3() ); boxMax.Set( m_AABB.GetMaxVector3() ); }

#if __PAGING && !__FINAL
	void SetRemapInfo(grmShaderGroup &group);
#endif
	bool IsEmpty();
	
	void ComputeBucketMask(const grmShaderGroup &group);

	u32 GetBucketMask(int lod) const;

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

#if __WIN32PC && !__FINAL && 0
	//transform the cull information from model space to world space.  This is meant to be a one time operation done on objects that are not shared.
	void TransformCullInfo( const Matrix34& worldTransform );
#endif

	void CloneWithNewVertexData( const rmcLodGroup &cloneme, int lod, const atArray<int>& modelIdx, const atArray<int>& geomIdx, bool createVB, void* preAllocatedMemory, int& memoryAvailable );

	bool IsLodInstanced(int lodIdx, const grmShaderGroup &shaderGroup, bool &hasNonInstancedShader) const;
	inline bool IsLodInstanced(int lodIdx, const grmShaderGroup &shaderGroup) const	{ bool ignored; return IsLodInstanced(lodIdx, shaderGroup, ignored); }

	void SetLodIndex( int lodIndex ) 
	{ 
		m_BucketMask[LOD_HIGH]	&= ~LODGROUP_MASK;
		m_BucketMask[LOD_MED]	&= ~LODGROUP_MASK;
		m_BucketMask[LOD_LOW]	&= ~LODGROUP_MASK;
		m_BucketMask[LOD_VLOW]	&= ~LODGROUP_MASK;

		m_BucketMask[lodIndex] |= LODGROUP_MASK;
	}

	int GetLodIndex() const
	{
		return	(m_BucketMask[LOD_VLOW] & LODGROUP_MASK) ? LOD_VLOW :
			(m_BucketMask[LOD_MED]  & LODGROUP_MASK) ? LOD_MED :
			(m_BucketMask[LOD_LOW]  & LODGROUP_MASK) ? LOD_LOW :
			LOD_HIGH;
	}

private:
	void LoadLod(class fiTokenizer &t,const char *match,int lod, const grmShaderGroup *pShaderGroup = 0, int lodMask = -1, bool optimize = true, int extraVerts = 0);

	spdSphere m_Sphere;
	spdAABB m_AABB;
	atRangeArray<datOwner<rmcLod>,LOD_COUNT> m_Lod;
	atRangeArray<float,LOD_COUNT> m_LodThresh;
	atRangeArray<u32,LOD_COUNT> m_BucketMask;

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	static DECLARE_MTR_THREAD TessellatedDrawMode	sm_TessellatedDrawMode;
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES
};


inline grmModel* rmcLod::GetModel(int idx) const
{
	return m_Models[idx];
}

}	// namespace rage

#endif
