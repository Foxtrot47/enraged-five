//
// rmcore/typefileparser.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "typefileparser.h"

#include "file/token.h"
#include "file/stream.h"
#include "string/string.h"

using namespace rage;

rmcTypeFileParser::rmcTypeFileParser()
{
	//HY Mike H. and me have discussed the warning issue
	//HY Please turn this on if you need it
	m_WarningSpew = false;
}

rmcTypeFileParser::~rmcTypeFileParser()
{
}

void rmcTypeFileParser::RegisterLoader( const char *sectionName, const char *entityName, datCallback loadCb, bool allowDuplicateSections )
{
	AssertMsg(strlen(sectionName) < TFP_MAX_NAMELEN, "section name too long");
	AssertMsg(strlen(entityName) < TFP_MAX_NAMELEN, "entity name too long");

	// Find section name to see if it's already been configured
	int i=0;
	char testName[TFP_MAX_NAMELEN];
	// Make it lower case
	StringNormalize(testName, sectionName, sizeof(testName));

	rmSectionLoaderData *section = 0;

	for ( i = 0; i < m_SectionData.GetCount(); ++i ) {
		if ( strcmp(m_SectionData[i].m_Name, testName) == 0 ) {
			section = &m_SectionData[i];
			break;
		}
	}
	
#if __ASSERT
	// To process "all" entities, there must not be any other entities present
	if ( section && strcmp( entityName, "all" ) == 0 )
		AssertMsg(0, "Can't request all entities when entities already registered for section");
	else if ( section && section->m_EntityData.GetCount() == 1 && strcmp(section->m_EntityData[0].m_Name, "all") == 0 )
		AssertMsg(0, "Something has already requested 'all' entities for this section");
#endif

	if ( section == 0 )
	{
		section = &m_SectionData.Append();
		strcpy(section->m_Name, testName);
		section->m_AllowDuplicates = allowDuplicateSections;
		section->m_Activated = false;
	}

	// Assert(section->m_AllowDuplicates==allowDuplicateSections);

	// Get a new entity data
	rmEntityLoaderData &entity = section->m_EntityData.Append();
	// Make it lower case
	StringNormalize( entity.m_Name, entityName, sizeof(entity.m_Name) );
	entity.m_LoadCb = loadCb;
}


bool rmcTypeFileParser::ProcessTypeFile( fiTokenizer &T, bool stopAtBracket )
{
	// If nothing to parse, back out
	if ( IsEmpty() )
		return true;

	char buff[TFP_MAX_NAMELEN];
	
	rmcTypeFileCbData cbData;

	// set all sections as never having been activated
	for (int i = 0; i < m_SectionData.GetCount(); ++i) {		
		m_SectionData[i].m_Activated=false;
	}

	while ( true ) {
		// Terminate when hitting a bracket matching our starting one
		if ( stopAtBracket && T.CheckToken("}", true) )
		{
			break;
		}

		if ( T.GetToken( buff, TFP_MAX_NAMELEN ) == 0 )
		{
			break;
		}

		StringNormalize( buff, buff, sizeof(buff) );
		rmSectionLoaderData *section = 0;
		// Find the section that matches
		for (int i = 0; i < m_SectionData.GetCount(); ++i) {
			if ( strcmp(m_SectionData[i].m_Name, buff) == 0 ) {
				section = &m_SectionData[i];
				break;
			}
		}
		int bracketCount = T.CheckToken("{", true) ? 1 : 0;
		if ( section ) {
			// if this can't be used more than once, then duplicated sections indicates an error in the file
			// perhaps just print a warning and throw this data away? (error probably would be lost in the spew)
#if __DEV
			if (section->m_Activated)
				Quitf("Invalid duplicate section '%s' in file '%s'",buff, T.GetStream()->GetName());
#endif
			Assert(!section->m_Activated);
			section->m_Activated = !section->m_AllowDuplicates;	

			// If the bracket count is zero, then just read one entity and terminate this section
			bool oneRead = false;
			bool onlyReadOne = false;
			if (bracketCount == 0)
			{
				onlyReadOne = true;
			}

			// Process all of the types
			while ( T.CheckToken("}", false) == false &&
				    (!onlyReadOne || !oneRead) &&
					T.GetToken( buff, TFP_MAX_NAMELEN ) ) {
				oneRead = true;
				StringNormalize( buff, buff, sizeof(buff) );
				// See if nothing exists
				if ( strcmp(buff, "none") == 0 )
					break;
				int i;
				for (i = 0; i < section->m_EntityData.GetCount(); ++i) {
					if ( strcmp(section->m_EntityData[i].m_Name, buff) == 0 || strcmp(section->m_EntityData[i].m_Name, "all") == 0) {
						cbData.m_EntityName = buff;
						cbData.m_T = &T;
						section->m_EntityData[i].m_LoadCb.Call( (CallbackData) &cbData );
						break;
					}
				}
				if ( i == section->m_EntityData.GetCount() )
				{
					if ( m_WarningSpew )
						Warningf("No entities to handle %s in '%s.type' file", buff, T.filename);
					// Skip block (DANGER - ENDLESS LOOP IF TOOLS DON'T EXPORT PROPERLY
					if ( T.CheckToken("{") )
						while( T.CheckToken("}") == false )
							T.GetToken(buff, TFP_MAX_NAMELEN);
				}
			}
			if (bracketCount > 0)
			{
				T.CheckToken("}");
			}
		}
		else {
			// Another risky case for when tools don't spit out data properly
			if ( T.CheckToken("none") == false && m_WarningSpew )
				Warningf("No sections registered to handle %s in file %s", buff, T.GetName());
			while( bracketCount )
			{
				if ( T.CheckToken("{") )
					bracketCount++;
				else if ( T.CheckToken("}") )
					bracketCount--;
				else if ( T.GetToken(buff, TFP_MAX_NAMELEN) == 0 )
					break;
			}
		}
	}
	return true;
}

void rmcTypeFileParser::Reset()
{
	for ( int i = 0; i < m_SectionData.GetCount(); ++i )
		m_SectionData[i].m_EntityData.Reset();
	m_SectionData.Reset();
}
