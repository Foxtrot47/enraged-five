//
// rmcore/spuevents.h
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef RMCORE_SPUEVENTS_H
#define RMCORE_SPUEVENTS_H

#if __PS3

#include "system/task.h"

namespace rage
{

enum
{
	EVENTCMD_RSX_STALL = NUM_SYSTEM_EVENTCMDS,
	NUM_RMCORE_EVENTCMDS
};

}
// namespace rage

#endif // __PS3

#endif // RMCORE_SPUEVENTS_H
