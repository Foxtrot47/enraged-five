// 
// rmcore/instance.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////
// Usage:
//
// rmcInstance derrives from rmcDrawable.  Create an rmcInstance the same way
// you would create an rmcDrawable object.  You must then allocate instance data
// for each instance of the drawable you want to render.  You can use the default
// rmcInstanceData class or you can subclass it to provide your own per instance
// data.
//
// Per instance data gets uploaded to the register specified by 
// rmcInstance::m_nConstantStartRegister.  Data is uploaded from the pointer returned
// by the rmcInstanceData::GetRegisterPointer function for each instance.  The number
// of vector registers that are uploaded is the number returned by the
// rmcInstanceData::GetRegisterCount function.

#ifndef RMCORE_INSTANCE_H
#define RMCORE_INSTANCE_H

#include "rmcore/drawable.h"
#include "vector/matrix44.h"
#include "grcore/effect_config.h"

namespace rage 
{

class rmcInstanceDataBase
{
public:
	rmcInstanceDataBase()							{}
	virtual ~rmcInstanceDataBase()					{}
	rmcInstanceDataBase(datResource&)				{}

	virtual int GetRegisterCount() const = 0;
	virtual /*const*/ float* GetRegisterPointer() const = 0;

	virtual const Matrix44& GetWorldMatrix() const = 0;
	virtual void SetWorldMatrix(const Matrix44& m) = 0;
};

class rmcInstanceData : public rmcInstanceDataBase
{
public:
	rmcInstanceData()								{}
	virtual ~rmcInstanceData()						{}
	rmcInstanceData(datResource&)					{}

	IMPLEMENT_PLACE_INLINE(rmcInstanceData);

	virtual int GetRegisterCount() const			{ return 4; }
	virtual /*const*/ float* GetRegisterPointer() const	{ return (/*const*/ float*)&m_WorldMatrix; }

	virtual const Matrix44& GetWorldMatrix() const	{ return m_WorldMatrix; }
	virtual void SetWorldMatrix(const Matrix44& m)			
	{ 
		m_WorldMatrix = m; 
	}

protected:
	Matrix44				m_WorldMatrix;
};

#if !(__D3D11 || RSG_ORBIS)

// On shader model 3 instance data is just placed into a set of contiguous vertex constant registers. 
typedef int rmcInstanceData_UsageInfo;

#else // (__D3D11 || RSG_ORBIS)

#define RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS 4

// On shader model 4 we support data being distributed over many constant buffers.
class rmcInstanceData_ConstBufferUpdateDesc
{
public:
	rmcInstanceData_ConstBufferUpdateDesc() {};
	virtual ~rmcInstanceData_ConstBufferUpdateDesc() {};

	virtual u32 GetNoOfConstantBuffers() const = 0;
	virtual grmShaderGroupVar GetStartVariable(int index) const = 0;
	virtual u32 GetRegisterCount(int index) const = 0;
};

typedef rmcInstanceData_ConstBufferUpdateDesc* rmcInstanceData_UsageInfo;

#endif // !(__D3D11 || RSG_ORBIS)


class rmcInstance: public rmcDrawable
{
public:
	rmcInstance();
	virtual ~rmcInstance();
	rmcInstance(class datResource&);
	rmcInstance(class datResource&, grmShaderGroup*);
	DECLARE_PLACE(rmcInstance);

	// PURPOSE: Preallocate the number of instances to be drawn
	// PARAMS:
	//	nReserveCount - The number of instances to reserve space for.
	void ReserveInstances(int nReserveCount);

	// RETURNS: The number of instances to be rendered.
	int GetInstanceCount() const;

	// PURPOSE: Add an instance to be rendered
	// PARAMS:
	//	pInstance - A pointer to the instance data for the added instance.
	// RETURNS: An integer containing the index of the newly added instance
	int AddInstance(const rmcInstanceDataBase* pInstance);

	// PURPOSE: Sets the instance specific data at the specified index
	// PARAMS:
	//	nInstance - The index of the instance to set
	//	pInstance - The new instance data for the specified index
	void SetInstanceData(int nInstance, const rmcInstanceDataBase* pInstance);

	// PURPOSE: Sets an array of instance specific data
	// PARAMS:
	//	pInstances - An array of instance data
	//	nInstanceCount - The number of elements in the pInstances array
	void SetInstanceData(const rmcInstanceDataBase** pInstances, int nInstanceCount);

	// PURPOSE: Gets the instance specific data at the specified index
	// PARAMS:
	//	nInstance - The index of the instance data to retrieve.
	// RETURNS:	A pointer to the instance data
	const rmcInstanceDataBase* GetInstanceData(int nInstance) const;

	// PURPOSE: Set the register number to upload instance variables to.
	// PARAMS:
	//	nRegister - The index of the vertex constant register.
	// NOTES:
	//	This register is the same for all shaders used by this drawable.  Care needs to be
	//	taken to ensure that this register is in a good spot and wont overrite data in all
	//	of the shaders.
	void SetConstantStartRegister(int nRegister
#if (__D3D11 || RSG_ORBIS)
		, grcCBuffer* poConstantBuffer = NULL
#endif // __WIN32PC
		);

	// RETURNS: The base register that instanced data is uploaded to
	int GetConstantStartRegister() const;
#if (__D3D11 || RSG_ORBIS)
	grcCBuffer* GetConstantBuffer() const;
#endif // __WIN32PC

	// PURPOSE: Set the lod and build a list of visible instances to render
	// PRAMS:
	//	viewport - The viewport to use for frustum culling
	// RETURNS: The highest lod level needed by any instance
	int Update(const grcViewport& viewport);

	/* PURPOSE: Draw a drawable (cannot be skinned or have per-instance cpv data)
		PARAMS: 
			mtx - Root matrix of object.  If object has a skeleton, this is actually the address of the first
				matrix in an array of matrices.
			bucket - Draw bucket to render with (0-31).  Only shaders with a matching draw bucket will actually render;
				this is one way to control draworder within a model without having to break it into smaller pieces.
			lod - Level of detail, from zero (LOD_HIGH) to 3 (LOD_VLOW)
			enables - If zero (the default), draw all models at that lod within the drawable.  If nonzero, only
				draw the models which are specifically enabled.
	*/
	void Draw(const Matrix34 &mtx,u32 bucket,int lod, u16 stats=0) const;

	/* PURPOSE: Draw a drawable (must be skinned)
		PARAMS: 
			data - per-instance shader local variable array (can be null if drawable's shaders don't access any locals)
			skel - Skeleton to render skinned object with
			bucket - Draw bucket to render with (0-31).  Only shaders with a matching draw bucket will actually render;
				this is one way to control draworder within a model without having to break it into smaller pieces.
			lod - Level of detail, from zero (LOD_HIGH) to 3 (LOD_VLOW)
			enables - If zero (the default), draw all models at that lod within the drawable.  If nonzero, only
				draw the models which are specifically enabled.
	*/
	void DrawSkinned(const Matrix34 &rootMatrix,const grmMatrixSet &ms,u32 bucket,int lod, u16 stats=0) const;

	// PURPOSE: Draw instances of a drawable
	// PARAMS:
	//		pDrawable - The drawable to draw instances of
	//		instanceArray - array of instance data objects representing the instances to be rendered
	//		bucket - Draw bucket to render with (0-31).  Only shaders with a matching draw bucket will actually render;
	//				this is one way to control draworder within a model without having to break it into smaller pieces.
	//		lod - Level of detail, from zero (LOD_HIGH) to 3 (LOD_VLOW)
	//		startRegister - the first register to upload shader constants to.
	static void DrawInstances(rmcDrawable* pDrawable, const atArray<datRef<const rmcInstanceDataBase> >* instanceArray, int bucket, int lod, rmcInstanceData_UsageInfo startRegister);

	// PURPOSE: Draw instances of a skinned drawable
	// PARAMS:
	//		skel - Skeleton to render skinned object with
	//		pDrawable - The drawable to draw instances of
	//		instanceArray - array of instance data objects representing the instances to be rendered
	//		bucket - Draw bucket to render with (0-31).  Only shaders with a matching draw bucket will actually render;
	//				this is one way to control draworder within a model without having to break it into smaller pieces.
	//		lod - Level of detail, from zero (LOD_HIGH) to 3 (LOD_VLOW)
	//		startRegister - the first register to upload shader constants to.
	static void DrawSkinnedInstances(const crSkeleton &skel, rmcDrawable* pDrawable, const atArray<datRef<const rmcInstanceDataBase> >* instanceArray, int bucket, int lod, rmcInstanceData_UsageInfo startRegister);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	static void InstDraw(const atArray<datRef<const rmcInstanceDataBase> >* pInstances, const grmShaderGroup &group, const rmcLodGroup& lodGroup, const Matrix34 *mtx, int bucket, int lodIdx, rmcInstanceData_UsageInfo nConstantStartRegister);
	static void InstDrawSkinned(const atArray<datRef<const rmcInstanceDataBase> >* pInstances, const grmModel* pModel, const grmShaderGroup &group,const Matrix34 *mtxs,int mtxCount,int bucket, rmcInstanceData_UsageInfo nConstantStartRegister);
	static void InstDrawUnskinned(const atArray<datRef<const rmcInstanceDataBase> >* pInstances, const grmModel* pModel, const grmShaderGroup &group,int bucket, rmcInstanceData_UsageInfo nConstantStartRegister);

protected:
	static const grmShader* GetShader(const grmShaderGroup& group, int nIndex, int bucket);

	int												m_nConstantStartRegister;
#if (__D3D11 || RSG_ORBIS)
	grcCBuffer*										m_poConstantBuffer;
#else
	void*											m_poConstantBuffer;
#endif
	atArray<datRef<const rmcInstanceDataBase> >		m_Instances;
	atArray<datRef<const rmcInstanceDataBase> >		m_VisibleInstances;
	mutable bool									m_bVisibilityComputed;
};

} // namespace rage

#endif // RMCORE_INSTANCE_H
