//
// rmcore/rsxstallhandler.cpp
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//

#if __PS3 && !__FINAL

#include "spuevents.h"
#include "drawablespu.h"
#include "grcore/grcorespu.h"
#include "grcore/wrapper_gcm.h"
#include "system/task.h"
#include <sys/event.h>
#include <sys/spu_thread.h>


namespace rage
{

static void rsxStallCallback(const sys_event_t *event)
{
	const sys_spu_thread_t spuThread = event->data1;
	const u32 argsLs = (u32)event->data3;

	const u32 rsxGet = gcm::g_ControlRegister->get;
	const u32 rsxPut = gcm::g_ControlRegister->put;
	Errorf("Ring buffer stalled out? get=%x, put=%x",rsxGet,rsxPut);

	u32 lsSpuGcmState;
	AssertVerify(SpuThreadReadLs(&lsSpuGcmState, spuThread, argsLs+0, sizeof(lsSpuGcmState)) == CELL_OK);
	spuGcmState cpSpuGcmState;
	AssertVerify(SpuThreadReadLs(&cpSpuGcmState, spuThread, lsSpuGcmState, sizeof(cpSpuGcmState)) == CELL_OK);

	void *get_addr = cpSpuGcmState.CommandFifo.GetAddressUnsafe(rsxGet);
	if (get_addr) {
		u32 contents = *(u32*)get_addr;
		Errorf("*get = %x (from ppu addr %x)",contents,(u32)get_addr);
	}
	else
		Errorf("(outside our ringbuffer, maybe a replay capture?)");

	if (cpSpuGcmState.CurrentDrawable)
	{
		const spuDrawable *pDrawable = cpSpuGcmState.CurrentDrawable;
#if HACK_GTA4
		char cpDrawable[sizeof(spuDrawable)] ALIGNED(__alignof(spuDrawable));
		if ((u32)cpSpuGcmState.CurrentDrawable < 256*1024)
		{
			AssertVerify(SpuThreadReadLs(&cpDrawable, spuThread, (u32)cpSpuGcmState.CurrentDrawable, sizeof(cpDrawable)) == CELL_OK);
			pDrawable = (spuDrawable*)&cpDrawable;
		}
#endif
		Printf(
			"current drawable=0x%p memdump=\n{\n"
			"\tvptr=0x%x\n"
			"\tm_PageMap=0x%p\n"
			"\tm_ShaderGroup=0x%p\n"
			"\tm_SkeletonData=0x%p\n"
			"\tm_LodGroup.m_CullSphere={%.2f,%.2f,%.2f}\n"
			"\tm_LodGroup.m_BoxMin={%.2f,%.2f,%.2f}\n"
			"\tm_LodGroup.m_BoxMax={%.2f,%.2f,%.2f}\n"
			"\tm_LodGroup.m_CullRadius=%.2f\n"
			"\tm_LodGroup.m_Container.m_Base=0x%p"
			"\tm_LodGroup.m_Container.m_Size=%d\n}\n",
			cpSpuGcmState.CurrentDrawable,
			pDrawable->vptr,pDrawable->m_PageMap,pDrawable->m_ShaderGroup,pDrawable->m_SkeletonData,
			((float*)&pDrawable->m_LodGroup.m_CullSphere)[0],((float*)&pDrawable->m_LodGroup.m_CullSphere)[1],((float*)&pDrawable->m_LodGroup.m_CullSphere)[2],
			((float*)&pDrawable->m_LodGroup.m_BoxMin)[0],((float*)&pDrawable->m_LodGroup.m_BoxMin)[1],((float*)&pDrawable->m_LodGroup.m_BoxMin)[2],
			((float*)&pDrawable->m_LodGroup.m_BoxMax)[0],((float*)&pDrawable->m_LodGroup.m_BoxMax)[1],((float*)&pDrawable->m_LodGroup.m_BoxMax)[2],
			((float*)&pDrawable->m_LodGroup.m_CullSphere)[3],pDrawable->m_ContainerPtr,pDrawable->m_ContainerSizeQW
		);
	}

	if (cpSpuGcmState.CurrentGeometry)
	{
		const spuGeometry *pGeometry = cpSpuGcmState.CurrentGeometry;
#if HACK_GTA4
		spuGeometry cpGeometry;
		if ((u32)cpSpuGcmState.CurrentGeometry < 256*1024)
		{
			AssertVerify(SpuThreadReadLs(&cpGeometry, spuThread, (u32)cpSpuGcmState.CurrentGeometry, sizeof(cpGeometry)) == CELL_OK);
			pGeometry = &cpGeometry;
		}
#endif
		Printf(
			"current geometry=0x%p memdump=\n{\n"
			"\tvptr=0x%x\n"
			"\tm_VtxDecl=0x%p\n"
			"\tm_Type=%s\n}\n",
			cpSpuGcmState.CurrentGeometry,
			pGeometry->vptr,pGeometry->m_VtxDecl,pGeometry->m_Type?"GEOMETRYEDGE":"GEOMETRYQB"
		);
	}

#if HACK_GTA4_MODELINFOIDX_ON_SPU
	Printf(
		"\n\tgcmDisplayGetStatus: modelIdx=%d, type=%d, renderPhase=%d.",
		cpSpuGcmState.gSpuGta4DebugInfo.gta4ModelInfoIdx,
		cpSpuGcmState.gSpuGta4DebugInfo.gta4ModelInfoType,
		cpSpuGcmState.gSpuGta4DebugInfo.gta4RenderPhaseID
	);
#endif

#if HACK_RDR2
	Printf("Transform type=%d\n", cpSpuGcmState.TransformType);
#endif // HACK_RDR2

	// Release the SPU
	sys_spu_thread_write_spu_mb(spuThread, 0);
}

REGISTER_SPU_EVENT_HANDLER(EVENTCMD_RSX_STALL, rsxStallCallback);

}
// namespace rage

#endif // __PS3 && !__FINAL
