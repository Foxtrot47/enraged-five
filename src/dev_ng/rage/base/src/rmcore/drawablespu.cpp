// 
// rmcore/drawablespu.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
//

#if __SPU
#define WANT_POSTSTALLHOLE_CALLBACK

#if !__FINAL
static void gcmDisplayGetStatus();
#define RINGBUFFER_STALL_CALLBACK	gcmDisplayGetStatus
#endif
#include <spu_printf.h>
#define grcTexture spuTexture
#define g_VertexShaderInputs (pSpuGcmState->VertexShaderInputs)
#include "drawablespu.h"

#include "system/magicnumber.h"
#include "system/taskheader.h"
#include "data/datcompress.cpp"

#include "grcore/effect_config.h"
#include "grcore/effectcache.cpp"

#if DRAWABLESPU_STATS
spuDrawableStats g_Stats;
#endif

static rage::grcEffectCache *s_effectCache;

#include "grcore/grcorespu_gcmcallback.h"
#include "profile/element.h"
#include "grcore/effect_render.cpp"
#include "grcore/effect_resource.cpp"
#include "grcore/effect_gcm.cpp"
#include "grcore/stateblock.cpp"
#include "shaderlib/instancebuffer.fxh"
#if SPU_GCM_FIFO
#include "grcore/edge_jobs.cpp"
#include "grcore/grcorespu.cpp"
#include "grmodel/grmodelspu.cpp"
#endif

const int TAG = 0;
#define dprintf(x)

using namespace rage;

namespace rage {

atBitSet* spuGet_(atBitSet *&ptr)
{
	if (ptr) {
		ptr = (atBitSet*) spuGetData(ptr, sizeof(atBitSet));
		ptr->SetBits((unsigned*) spuGetData(ptr->GetBitsPtr(), ptr->GetSizeInWords() * sizeof(unsigned)));
	}
	return ptr;
}

}	// namespace rage

void spuLodGroup::DrawSingle(spuShaderGroup &group,spuMatrix44 &mtx,u32 bucketMask,int idx, u16 stats)
{
	spuLod *lod = m_Lod[idx];
	ValidateSpuPtr(lod);
	ValidateSpuPtr(lod->m_Models.m_Elements);
	ValidateSpuPtr(&group);

	if (BUCKETMASK_MATCH(m_BucketMask[idx], bucketMask))
	{
#if SPU_GCM_FIFO		// Lame
		grcState__SetWorldFast(mtx);
#endif

		for (int i=0; i<lod->m_Models.GetCount(); i++) {
			if (lod->m_Models[i]) {
				spuModel *model = (lod->m_Models[i]);
				ValidateSpuPtr(model);
				model->Draw(group,bucketMask,idx,stats);
			}
		}
	}
}

vector float lod_XAXIS = { 1.0f, 0, 0, 0 };
vector float lod_YAXIS = { 0, 1.0f, 0, 0 };
vector float lod_ZAXIS = { 0, 0, 1.0f, 0 };

void spuLodGroup::DrawMulti(spuShaderGroup &group,spuMatrix44 *rootMatrix,spuMatrixSet &ms,u32 bucketMask,int lodIdx, u16 stats) 
{
	spuLod *lod = m_Lod[lodIdx];
	ValidateSpuPtr(lod);
	ValidateSpuPtr(lod->m_Models.m_Elements);
	ValidateSpuPtr(&group);

	// Displayf("lod = %p, model count is %d",lod,lod->m_Models.m_Count);

	spuMatrix44 wm;
	if (rootMatrix)
		wm = *rootMatrix;
	else {
		wm.a = lod_XAXIS;
		wm.b = lod_YAXIS;
		wm.c = lod_ZAXIS;
	}

	if (BUCKETMASK_MATCH(m_BucketMask[lodIdx], bucketMask))
	{	
		int lastMtx = -2;
		for (int i=0; i<lod->m_Models.GetCount(); i++) {
			if (lod->m_Models[i]) {
				spuModel *model = lod->m_Models[i];
				ValidateSpuPtr(model);
				int idx = model->m_MatrixIndex;
				{
					if (model->m_SkinFlag) {
						if (lastMtx != -1)
						{
	#if SPU_GCM_FIFO	// Lame
							grcState__SetWorldFast(wm);
	#endif
							lastMtx = -1;
						}

						model->DrawSkinned(group,ms,bucketMask,lodIdx,stats);
					}
					else {
						if (idx != lastMtx) {
							// This is a bit slower than I'd like because it has to untranspose the matrix and re-add the offset back in again.
							spuMatrix44 tmp;
							ms.ComputeMatrix(tmp, lastMtx = idx);
							
							if (rootMatrix)
							{
								spuMatrix44 tmp2; 
	#if HACK_GTA4
								spuMatrix44 rootMat;
								// Clean up the W column.
								rootMat.a = spu_insert(0.0f, rootMatrix->a, 3);
								rootMat.b = spu_insert(0.0f, rootMatrix->b, 3);
								rootMat.c = spu_insert(0.0f, rootMatrix->c, 3);
								rootMat.d = spu_insert(1.0f, rootMatrix->d, 3);
								tmp2.Transform(rootMat, tmp);		//tmp.Dot(*rootMatrix);
	#else
								tmp2.Transform(*rootMatrix, tmp);	//tmp.Dot(*rootMatrix);
	#endif
								tmp = tmp2;
							}
	#if SPU_GCM_FIFO
							grcState__SetWorldFast(tmp);
	#endif
						}

						model->Draw(group,bucketMask,lodIdx,stats);
					}
				}
			}
		}
	}
}


void spuDrawable::Draw(spuMatrix44& worldMatrix,u32 bucketMask,int lod, u16 stats, bool captureStats)
{
	ValidateSpuPtr(m_ShaderGroup.ptr);
	
	DRAWABLESPU_STATS_INC(DrawableDrawCalls);
	if (captureStats)
	{
		DRAWABLESPU_STATS_INC(EntityDrawCalls);
	}
	m_LodGroup.DrawSingle(*m_ShaderGroup,worldMatrix,bucketMask,lod, stats);
}

void spuDrawable::DrawSkinned(spuMatrix44* rootMatrix,spuMatrixSet& matrixSet,u32 bucketMask,int lod, u16 stats, bool captureStats)
{
	ValidateSpuPtr(m_ShaderGroup.ptr);

	// Retrieve the counts at least, don't know the matrices yet.
	spuMatrixSet *ms = (spuMatrixSet*) spuGetData(&matrixSet, sizeof(matrixSet));
	// Remember the original address in main memory (for EDGE)
	ms->PPUAddress = matrixSet.m_Matrices;
	// Now get all the matrices (a bit of a waste for edge)
	spuGetData(matrixSet.m_Matrices, ms->m_MatrixCount * sizeof(spuMatrix43));

	DRAWABLESPU_STATS_INC(DrawableDrawSkinnedCalls);
	if (captureStats)
	{
		DRAWABLESPU_STATS_INC(EntityDrawCalls);
	}
	m_LodGroup.DrawMulti(*m_ShaderGroup,rootMatrix,*ms,bucketMask,lod, stats);
}

/*static*/ bool gpuMemCpy::Pop(u32 thisEa, Job *job, u32 *doneSrcOffset, u32 mfcTag) {
	u8 buf[128] ALIGNED(128);
	gpuMemCpy *const ls = (gpuMemCpy*)buf;
	for (;;) {
		spu_writech(MFC_LSA, (u32)buf);
		spu_writech(MFC_EAL, thisEa);
		spu_writech(MFC_Cmd, MFC_GETLLAR_CMD);
		spu_readch(MFC_RdAtomicStat);

		const u16 putIdx = ls->m_putIdx;
		u16 getIdx = ls->m_getIdx;

		// Empty ?
		if (putIdx == getIdx) {
			const u16 pending = ls->m_pending;
			Assert(pending);
			ls->m_pending = pending-1;

			spu_writech(MFC_LSA, (u32)buf);
			spu_writech(MFC_EAL, thisEa);
			spu_writech(MFC_Cmd, MFC_PUTLLC_CMD);
			if (Likely(!spu_readch(MFC_RdAtomicStat))) {
				return false;
			}
		}

		// Job to be grabbed ?
		else {
			sysDmaGetAndWait(job, (u32)(((gpuMemCpy*)thisEa)->m_buffer+getIdx), sizeof(*job), mfcTag);
			getIdx = (getIdx+1)%MaxCopy;

			// Loop until we can successfully update the get index.  Notice that
			// we don't need to refetch the job while looping, since it will not
			// have changed.
			for (;;) {
				ls->m_getIdx = getIdx;
				spu_writech(MFC_LSA, (u32)buf);
				spu_writech(MFC_EAL, thisEa);
				spu_writech(MFC_Cmd, MFC_PUTLLC_CMD);
				if (Likely(!spu_readch(MFC_RdAtomicStat))) {
					*doneSrcOffset = ls->m_doneSrcOffset;
					return true;
				}

				spu_writech(MFC_LSA, (u32)buf);
				spu_writech(MFC_EAL, thisEa);
				spu_writech(MFC_Cmd, MFC_GETLLAR_CMD);
				spu_readch(MFC_RdAtomicStat);
			}
		}
	}
}

#if SPU_GCM_FIFO > 2

namespace rage {

void spuGetDrawable(spuDrawable *&dr, u32 drawableContainerSizeQW, u32 drawableContainerEa, u32 shaderGroupEa, unsigned drawTypeMask,unsigned lod) {

	u32 drEa = (u32) dr;
	if (Unlikely(drEa < 256*1024))
		return;

	// Leave space for the drawable and its container.  We know the size for
	// this, but not for the shader group container, so this needs to come
	// first.
	char *temp = spuScratch;
	spuDrawable *dr_ = (spuDrawable*) temp;
	const u32 drawableContainerSizeBytes = drawableContainerSizeQW << 4;
	void *container = dr_ + 1;
	temp += sizeof(spuDrawable) + drawableContainerSizeBytes;

	// Fetch the first part of the shader group container so we know its size.
	// While there could still be puts in flight from
	// grcEffect__SetVar_grcTexture, tthey are to the instance data in the
	// spuShaderGroup's container, not to the spuShaderGroup struct itself, so
	// no need to use spuBindTag here.
	spuShaderGroup *sg = (spuShaderGroup*) temp;
	spuShader *const forceShader = sm_ForceShader;
	if (Likely(!forceShader))
		sysDmaGet(sg, shaderGroupEa, sizeof(spuShaderGroup), spuGetTag);

	// Pull over the drawable and its container
	sysDmaGet(dr_, drEa, sizeof(spuDrawable), spuGetTag2);
	sysDmaLargeGet(container, drawableContainerEa, drawableContainerSizeBytes, spuGetTag2);
	dr = dr_;

	// And the shader's container (the container length is stored in the first part of the container itself)
	unsigned shaderContainerSize = 0;
	if (Likely(!forceShader))
	{
		sysDmaWaitTagStatusAll(1<<spuGetTag);
		shaderContainerSize = sg->m_ContainerSizeQW<<4;
		// Now we are fetching the spuShaderGroup's container which does contain
		// grcInstanceData.  So use a fenced get on spuBindTag, since there
		// could still be puts in flight from grcEffect__SetVar_grcTexture.
		sysDmaLargeGetf(sg+1, shaderGroupEa+sizeof(spuShaderGroup), shaderContainerSize-sizeof(spuShaderGroup), spuBindTag);
		temp += shaderContainerSize;
	}

	spuScratch = temp;

	if (Unlikely(spuScratch > spuScratchTop))
		Quitf("spuGetDrawable() - scratch overflow by %zu bytes (drawable is %u, shadergroup is %u)",spuScratch - spuScratchTop,drawableContainerSizeBytes,shaderContainerSize);

	sysDmaWaitTagStatusAll(1<<spuGetTag2);
	{ datResource rsc(container, dr_->m_ContainerPtr, drawableContainerSizeBytes);
	rage_placement_new(dr_) spuDrawable(rsc,lod); }

	if (Likely(!forceShader))
	{
		dr_->m_ShaderGroup = sg;

		int techniqueGroupId = pSpuGcmState->ForcedTechniqueGroupId;
		techniqueGroupId = (techniqueGroupId!=-1) ? techniqueGroupId : pSpuGcmState->LightingMode;

		sysDmaWaitTagStatusAll(1<<spuBindTag);
		{ datResource rsc(sg,(void*)shaderGroupEa,shaderContainerSize);
		rage_placement_new(sg) spuShaderGroup(rsc,techniqueGroupId,drawTypeMask); }
	}
}

}

#endif	// SPU_GCM_FIFO

namespace rage
{
	extern bool g_PatcherModifiedData;
}

void drawablespu(sysTaskParameters& taskParams)
{
#include "grcore/grcorespu_header.h"

#include "grcore/patcherspu_case.h"

#include "grcore/grcorespu_case.h"

#include "grcore/grcorespu_gamestate_case.h"

#include "grmodel/grmodelspu_case.h"

#if DRAWABLE_ON_SPU

BEGIN_SPU_COMMAND(rmcDrawable__Draw)
	SPU_BOOKMARK(0x1480);
#if !__FINAL
	pSpuGcmState->CurrentGeometry = NULL;
	pSpuGcmState->CurrentDrawable = cmd->drawable;
#endif // !__FINAL
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo = cmd->gta4DebugInfo;
		cellGcmSetWriteTextureLabel(GCM_CONTEXT, 255, cmd->gta4DebugInfo.gta4ModelInfoIdx);
	#endif
	spuGetDrawable(cmd->drawable,cmd->containerSizeQW,(u32)cmd->container,(u32)cmd->shaderGroup,1<<RMC_DRAW,cmd->lod);

	bool captureStats = false;
	NOTFINAL_ONLY(captureStats = pSpuGcmState->isCapturingStats;)
	cmd->drawable->Draw(cmd->matrix,cmd->bucketMask,cmd->lod, cmd->stats, captureStats);

	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo.Invalidate();
		cellGcmSetWriteTextureLabel(GCM_CONTEXT, 255, 0);
	#endif
END_SPU_COMMAND

BEGIN_SPU_COMMAND(rmcDrawable__DrawSkinned)
	SPU_BOOKMARK(0x1490);
#if !__FINAL
	pSpuGcmState->CurrentGeometry = NULL;
	pSpuGcmState->CurrentDrawable = cmd->drawable;
#endif // !__FINAL
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo = cmd->gta4DebugInfo;
		cellGcmSetWriteTextureLabel(GCM_CONTEXT, 255, cmd->gta4DebugInfo.gta4ModelInfoIdx);
	#endif
	
	// uint32_t drawStart = ~spu_readch(SPU_RdDec);

	// Need both RMC_DRAW and RMC_DRAWSKINNED here, since the draw type is based on the spuGeometry type.
	// spuGeometryEdge uses RMC_DRAW, and spuGeometryQB uses RMC_DRAWSKINNED.
	spuGetDrawable(cmd->drawable,cmd->containerSizeQW,(u32)cmd->container,(u32)cmd->shaderGroup,(1<<RMC_DRAW)|(1<<RMC_DRAWSKINNED),cmd->lod);

	bool captureStats = false;
	NOTFINAL_ONLY(captureStats = pSpuGcmState->isCapturingStats;)
	/// HACK -- m_Offset won't be correct. spuGet(cmd->matrixSet);
	cmd->drawable->DrawSkinned(&cmd->rootMatrix, *cmd->matrixSet, cmd->bucketMask, cmd->lod, cmd->stats, captureStats);

	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo.Invalidate();
		cellGcmSetWriteTextureLabel(GCM_CONTEXT, 255, 0);
	#endif
	/// Displayf("**** END DRAWSKINNED %d tix",	~spu_readch(SPU_RdDec) - drawStart);
END_SPU_COMMAND

BEGIN_SPU_SIMPLE_COMMAND(startStatRecord)
	NOTFINAL_ONLY(pSpuGcmState->isCapturingStats = true;)
END_SPU_SIMPLE_COMMAND

BEGIN_SPU_SIMPLE_COMMAND(stopStatRecord)
	NOTFINAL_ONLY(pSpuGcmState->isCapturingStats = false;)
END_SPU_SIMPLE_COMMAND

#endif

#include "grcore/grcorespu_footer.h"
}
#endif
