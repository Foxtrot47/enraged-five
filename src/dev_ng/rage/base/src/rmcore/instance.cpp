// 
// rmcore/instance.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rmcore/instance.h"
#include "data/resource.h"
#include "diag/art_channel.h"
#include "grmodel/geometry.h"
#include "grcore/vertexbuffer.h"
#include "grcore/indexbuffer.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "atl/array_struct.h"
#include "profile/element.h"
#include "shaderlib/rage_constants.h"

namespace rage
{

rmcInstance::rmcInstance()
{
	// Default this to register 64.  If this changes, it needs to be a global change to all shaders that might come through here
	m_nConstantStartRegister = 0;
	m_bVisibilityComputed = false;
}

rmcInstance::~rmcInstance()
{
}

rmcInstance::rmcInstance(datResource&)
{
}

rmcInstance::rmcInstance(datResource&, grmShaderGroup*)
{
}

IMPLEMENT_PLACE(rmcInstance);

#if __DECLARESTRUCT
void rmcInstance::DeclareStruct(datTypeStruct& s)
{
	rmcDrawable::DeclareStruct(s);
	STRUCT_BEGIN(rmcInstance);
	STRUCT_FIELD(m_nConstantStartRegister);
	STRUCT_FIELD(m_Instances);
	STRUCT_END();
}
#endif

void rmcInstance::ReserveInstances(int nReserveCount)
{
	m_Instances.Reserve(nReserveCount);
	m_VisibleInstances.Reserve(nReserveCount);
}

int rmcInstance::GetInstanceCount() const
{
	return m_Instances.GetCount();
}

int rmcInstance::AddInstance(const rmcInstanceDataBase* pInstance)
{
	int nIndex = m_Instances.GetCount();
	m_Instances.Grow() = pInstance;
	return nIndex;
}

void rmcInstance::SetInstanceData(int nInstance, const rmcInstanceDataBase* pInstance)
{
	m_Instances[nInstance] = pInstance;
}

void rmcInstance::SetInstanceData(const rmcInstanceDataBase** pInstances, int nInstanceCount)
{
	m_Instances.Resize(0);
	for( int i = 0; i < nInstanceCount; i++ )
	{
		AddInstance(pInstances[i]);
	}
}

const rmcInstanceDataBase* rmcInstance::GetInstanceData(int nInstance) const
{
	return m_Instances[nInstance];
}

void rmcInstance::SetConstantStartRegister(int nRegister
#if (__D3D11 || RSG_ORBIS)
 										   , grcCBuffer* poConstantBuffer
#endif
 										   )
{
	m_nConstantStartRegister = nRegister;
#if (__D3D11 || RSG_ORBIS)
	m_poConstantBuffer = poConstantBuffer;
#endif
}

int rmcInstance::GetConstantStartRegister() const
{
	return m_nConstantStartRegister;
}

int rmcInstance::Update(const grcViewport& viewport)
{
	int nHighestLod = 0;
	if( m_VisibleInstances.GetCapacity() < m_Instances.GetCount() )
	{
		m_VisibleInstances.Reserve(m_Instances.GetCapacity());
	}
	m_VisibleInstances.Resize(0);
	for( int i = 0; i < m_Instances.GetCount(); i++ )
	{
		u8 nLod;
		if( IsVisible(m_Instances[i]->GetWorldMatrix(), viewport, nLod) )
		{
			m_VisibleInstances.Append() = m_Instances[i];
			if( nHighestLod > nLod )
				nHighestLod = nLod;
		}
	}
	m_bVisibilityComputed = true;
	return nHighestLod;
}

const grmShader* rmcInstance::GetShader(const grmShaderGroup& group, int nIndex, int bucket)
{
	const grmShader *shader;
	u32 shaderBucketMask=0;
	if(grmModel::GetForceShader()) 
	{
		shader = grmModel::GetForceShader();
		if ( shader->InheritsBucketId() == false ) 
		{
			shaderBucketMask = shader->GetDrawBucketMask();
		}
	}
	else 
	{
		shaderBucketMask = group[nIndex].GetDrawBucketMask();
		shader = &group[nIndex];
	}
	if( shaderBucketMask & (1<<bucket) )
	{
		return shader;
	}

	return 0;
}

namespace GraphicsStats {
	EXT_PF_COUNTER(grmGeometry_Draw_Calls);
	EXT_PF_COUNTER(grmGeometry_Draw_Vertices);
	EXT_PF_COUNTER(grmGeometry_Draw_Indices);
}
using namespace GraphicsStats;


void rmcInstance::InstDrawSkinned(const atArray<datRef<const rmcInstanceDataBase> >* pInstances, const grmModel* pModel, const grmShaderGroup &group, const Matrix34 * /*mtxs*/, int /*mtxCount*/, int bucket, rmcInstanceData_UsageInfo UsageInfo)
{
	AssertMsg(false,"rmcInstance::InstDrawSkinned is broken");

#if __D3D11 || RSG_ORBIS
	// DX11 TODO:- Make sure all calls pass in a valid pointer!
	if(!UsageInfo)
		return;

	u32 nBuff;
	u32 FloatCounts[RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS];

	// Collect the number of constant buffers that will need to be updated...
	u32 NoOfCBuffers = UsageInfo->GetNoOfConstantBuffers();
	Assert(NoOfCBuffers <= RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS);
	ASSERT_ONLY(u32 AmountOfData = 0);

	// ...And how much data will be placed in each one.
	for(nBuff=0; nBuff<NoOfCBuffers; nBuff++)
	{
		ASSERT_ONLY(AmountOfData += UsageInfo->GetRegisterCount(nBuff));
		FloatCounts[nBuff] = UsageInfo->GetRegisterCount(nBuff) << 0x2;
	}
	Assert(AmountOfData == (u32)(*pInstances)[0]->GetRegisterCount());
#endif //__D3D11 || RSG_ORBIS

	for( int i = 0; i < pModel->GetGeometryCount(); i++ )
	{
		// Get the shader
		const grmShader* pShader = GetShader(group, pModel->GetShaderIndex(i), bucket);
		if( pShader )
		{
			// Bind the shader
			int numPasses = pShader->BeginDraw(grmShader::RMC_DRAWSKINNED, true);
			for ( int pass=0; pass < numPasses; pass++)
			{
				// pModel->SetMatrices(i, mtxs, mtxCount);
				pShader->Bind(pass);

				// Setup the vertex and index buffers
				grmGeometry& geom = pModel->GetGeometry(i);

				artAssertf(geom.GetType() == grmGeometry::GEOMETRYQB, "Cannot use rmcInstance with EDGE geometry!  If this is a car wheel, make sure the model uses vehicle_tire.sps for all geometry");
				if (geom.GetType() != grmGeometry::GEOMETRYQB) {
					continue; 
				}

				grcIndexBuffer* pIndexBuffer = geom.GetIndexBuffer(true);
				grcVertexBuffer* pVertexBuffer = geom.GetVertexBuffer(true);
				u16 nPrimType = geom.GetPrimitiveType();
				u32 nIdxCount = pIndexBuffer->GetIndexCount();
				// int nVerts = pVertexBuffer->GetVertexCount();
				grcVertexDeclaration *vertexDeclaration = geom.GetDecl();

				GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
				GRCDEVICE.SetIndices(*pIndexBuffer);
				GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());

				PF_INCREMENTBY(grmGeometry_Draw_Calls, pInstances->GetCount());
				PF_INCREMENTBY(grmGeometry_Draw_Vertices,pVertexBuffer->GetVertexCount() * pInstances->GetCount());
				PF_INCREMENTBY(grmGeometry_Draw_Indices,nIdxCount * pInstances->GetCount());

			#if !(__D3D11 || RSG_ORBIS)
				// Render the instances
				for( int j = 0; j < pInstances->GetCount(); j++ )
				{
					// Set the per instance vertex shader constants
					const rmcInstanceDataBase* pInstance = (*pInstances)[j];
 					GRCDEVICE.SetVertexShaderConstant(UsageInfo, pInstance->GetRegisterPointer(), pInstance->GetRegisterCount());
					// Render the geometry
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
				}
			#else //!(__D3D11 || RSG_ORBIS)

				int BufferOffsets[RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS];
				grcCBuffer *pConstantBuffers[RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS];

				// Collect which shader the model will be using.
				int ShaderIndex = pModel->GetShaderIndex(i);

				// Collect each constant buffer and offset for the shader being used.
				for(nBuff=0; nBuff<NoOfCBuffers; nBuff++)
				{
					pConstantBuffers[nBuff] = group.GetVarCBufferAndOffset(UsageInfo->GetStartVariable(nBuff), ShaderIndex, BufferOffsets[nBuff]);
				}

				// Now render the instances.
				for( int j = 0; j < pInstances->GetCount(); j++ )
				{
					const rmcInstanceDataBase* pInstance = (*pInstances)[j];
					float *pInstanceData = pInstance->GetRegisterPointer();

#if RAGE_INSTANCED_TECH
					if (grcViewport::GetInstancing())
					{
						grcViewport::RegenerateInstVPMatrices(RCC_MAT44V(pInstance->GetWorldMatrix()));
					}
#endif

					// Update the specified constant buffers.
					for(nBuff=0; nBuff<NoOfCBuffers; nBuff++)
					{
						// A shader might not use all variables specified in the usage.
						if(pConstantBuffers[nBuff])
						{
							char *pDataPtr = (char *)pConstantBuffers[nBuff]->GetDataPtr();
							memcpy((void *)(pDataPtr + BufferOffsets[nBuff]), (void *)pInstanceData, FloatCounts[nBuff]*sizeof(float));
							pConstantBuffers[nBuff]->Unlock();
						}

						// Advance in the source instance data.
						pInstanceData += FloatCounts[nBuff];
					}
					// Render the geometry
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
				}

			#endif //!(__D3D11 || RSG_ORBIS)

				GRCDEVICE.ClearStreamSource(0);

				// Unbind the shader
				pShader->UnBind();
			}
			pShader->EndDraw();
		}
	}
}

void rmcInstance::InstDrawUnskinned(const atArray<datRef<const rmcInstanceDataBase> >* pInstances, const grmModel* pModel, const grmShaderGroup &group, int bucket, rmcInstanceData_UsageInfo UsageInfo)
{
	// Bail immediately if there are no instances (shouldn't ever happen)
	if (!AssertVerify(pInstances->GetCount()))
		return;

#if (__D3D11 || RSG_ORBIS)
	// DX11 TODO:- Make sure all calls pass in a valid pointer!
	if(!UsageInfo)
		return;

	u32 nBuff;
	u32 FloatCounts[RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS];

	// Collect the number of constant buffers that will need to be updated...
	u32 NoOfCBuffers = UsageInfo->GetNoOfConstantBuffers();
	Assert(NoOfCBuffers <= RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS);
	ASSERT_ONLY(u32 AmountOfData = 0);

	// ...And how much data will be placed in each one.
	for(nBuff=0; nBuff<NoOfCBuffers; nBuff++)
	{
		ASSERT_ONLY(AmountOfData += UsageInfo->GetRegisterCount(nBuff));
		FloatCounts[nBuff] = UsageInfo->GetRegisterCount(nBuff) << 0x2;
	}
	Assert(AmountOfData == (u32)(*pInstances)[0]->GetRegisterCount());
#endif //__D3D11 || RSG_ORBIS

	for( int i = 0; i < pModel->GetGeometryCount(); i++ )
	{
		// Get the shader
		const grmShader* pShader = GetShader(group, pModel->GetShaderIndex(i), bucket);
		if( pShader )
		{
			// Bind the shader
			int numPasses = pShader->BeginDraw(grmShader::RMC_DRAW, true);
			for(int pass=0; pass<numPasses; pass++)
			{
				// Setup the vertex and index buffers
				grmGeometry& geom = pModel->GetGeometry(i);

				AssertMsg(geom.GetType() == grmGeometry::GEOMETRYQB, "Cannot use rmcInstance with EDGE geometry!");
				if (geom.GetType() != grmGeometry::GEOMETRYQB) {
					continue;
				}

				pShader->Bind(pass);

				// Setup the vertex and index buffers
				grcIndexBuffer* pIndexBuffer = geom.GetIndexBuffer(true);
				grcVertexBuffer* pVertexBuffer = geom.GetVertexBuffer(true);
				u16 nPrimType = geom.GetPrimitiveType();
				u32 nIdxCount = pIndexBuffer->GetIndexCount();
				// int nVerts = pVertexBuffer->GetVertexCount();
				grcVertexDeclaration *vertexDeclaration = geom.GetDecl();

			#if !__PS3
				GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
				GRCDEVICE.SetIndices(*pIndexBuffer);
				GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());
			#endif

				PF_INCREMENTBY(grmGeometry_Draw_Calls, pInstances->GetCount());
				PF_INCREMENTBY(grmGeometry_Draw_Vertices,pVertexBuffer->GetVertexCount() * pInstances->GetCount());
				PF_INCREMENTBY(grmGeometry_Draw_Indices,nIdxCount * pInstances->GetCount());

			#if !(__D3D11 || RSG_ORBIS)
				// Render the instances
				// Turning this into an SPU macro didn't help very much (maybe 0.2%) because the bulk of the expense is copying the shader constant values.
				int nRegisterOffset = (int)((*pInstances)[0]->GetRegisterPointer() - (float*) (*pInstances)[0].ptr);
				int nRegisterCount = (*pInstances)[0]->GetRegisterCount();
				for( int j = 0; j < pInstances->GetCount(); j++ )
				{
					// Set the per instance vertex shader constants
					const rmcInstanceDataBase* pInstance = (*pInstances)[j];
					Assert((float*) pInstance + nRegisterOffset == (*pInstances)[j]->GetRegisterPointer() && nRegisterCount == (*pInstances)[j]->GetRegisterCount());
 					GRCDEVICE.SetVertexShaderConstant(UsageInfo, (float*) pInstance + nRegisterOffset, nRegisterCount);

				#if !__PS3
					// Render the geometry
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
				#else
					// Render the geometry
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, vertexDeclaration, *pVertexBuffer, *pIndexBuffer, nIdxCount);
				#endif
				}

			#else // (__D3D11 || RSG_ORBIS)

				int BufferOffsets[RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS];
				grcCBuffer *pConstantBuffers[RMC_INSTANCEDATA_MAX_CONSTANT_BUFFERS];
				
				// Collect which shader the model will be using.
				int ShaderIndex = pModel->GetShaderIndex(i);

				// Collect each constant buffer and offset for the shader being used.
				for(nBuff=0; nBuff<NoOfCBuffers; nBuff++)
				{
					pConstantBuffers[nBuff] = group.GetVarCBufferAndOffset(UsageInfo->GetStartVariable(nBuff), ShaderIndex, BufferOffsets[nBuff]);
				}

				// Now render the instances.
				for( int j = 0; j < pInstances->GetCount(); j++ )
				{
					const rmcInstanceDataBase* pInstance = (*pInstances)[j];
					float *pInstanceData = pInstance->GetRegisterPointer();

					// Update the specified constant buffers.
					for(nBuff=0; nBuff<NoOfCBuffers; nBuff++)
					{
						// A shader might not use all variables specified in the usage.
						if(pConstantBuffers[nBuff])
						{
							char *pDataPtr = (char *)pConstantBuffers[nBuff]->GetDataPtr();
							memcpy((void *)(pDataPtr + BufferOffsets[nBuff]), (void *)pInstanceData, FloatCounts[nBuff]*sizeof(float));
							pConstantBuffers[nBuff]->Unlock();
						}

						// Advance in the source instance data.
						pInstanceData += FloatCounts[nBuff];
					}
					// Render the geometry
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
				}

			#endif // __D3D11 || RSG_ORBIS)

			#if !__PS3
				GRCDEVICE.ClearStreamSource(0);
			#endif

				// Unbind the shader
				pShader->UnBind();
			}
			pShader->EndDraw();
		}
	}
}

void rmcInstance::InstDraw(const atArray<datRef<const rmcInstanceDataBase> >* pInstances, const grmShaderGroup &group, const rmcLodGroup& lodGroup, const Matrix34 *mtx, int bucket, int lodIdx, rmcInstanceData_UsageInfo nConstantStartRegister)
{
	const rmcLod& lod = lodGroup.GetLod(lodIdx);

	for( int i = 0; i < lod.GetCount(); i++ )
	{
		grmModel* model = lod.GetModel(i);
		if( model )
		{
			{
				if( model->GetSkinFlag() )
				{
					// Draw Skinned
					InstDrawSkinned(pInstances, model, group, mtx, model->GetMatrixCount(), bucket, nConstantStartRegister);
				}
				else
				{
					// Draw Unskinned
					InstDrawUnskinned(pInstances, model, group, bucket, nConstantStartRegister);
				}
			}
		}
	}
}

void rmcInstance::Draw(const Matrix34 &mtx, u32 bucket, int lod, u16 UNUSED_PARAM(stats)) const 
{
	const atArray<datRef<const rmcInstanceDataBase> >* pInstances;
	if( m_bVisibilityComputed )
		pInstances = &m_VisibleInstances;
	else
		pInstances = &m_Instances;

#if !(__D3D11 || RSG_ORBIS)
	Assert(m_nConstantStartRegister >= 0 );
	InstDraw(pInstances, GetShaderGroup(), GetLodGroup(), &mtx,bucket,lod, m_nConstantStartRegister);
#else //!(__D3D11 || RSG_ORBIS)
	// DX11 TODO:-
	(void)lod;
	(void)bucket;
	(void)mtx;
#endif //!(__D3D11 || RSG_ORBIS)

	m_bVisibilityComputed = false;
}

void rmcInstance::DrawSkinned(const Matrix34 & rootMatrix, const grmMatrixSet & /*ms*/, u32 bucket, int lod, u16 UNUSED_PARAM(stats)) const 
{
	const atArray<datRef<const rmcInstanceDataBase> >* pInstances;
	if( m_bVisibilityComputed )
		pInstances = &m_VisibleInstances;
	else
		pInstances = &m_Instances;

#if !(__D3D11 || RSG_ORBIS)
	Assert(m_nConstantStartRegister >= 0 );
	InstDraw(pInstances, GetShaderGroup(), GetLodGroup(), &rootMatrix, bucket,lod, m_nConstantStartRegister);
#else //!(__D3D11 || RSG_ORBIS)
	// DX11 TODO:-
	(void)lod;
	(void)bucket;
	(void)rootMatrix;
#endif //!(__D3D11 || RSG_ORBIS)

	m_bVisibilityComputed = false;
}

void rmcInstance::DrawInstances(rmcDrawable* pDrawable, const atArray<datRef<const rmcInstanceDataBase> >* instanceArray, int bucket, int lod, rmcInstanceData_UsageInfo startRegister)
{
	Assert(startRegister >= 0 );
	Matrix34 mtx;
	InstDraw(instanceArray, pDrawable->GetShaderGroup(), pDrawable->GetLodGroup(), &mtx,bucket,lod, startRegister);
}

void rmcInstance::DrawSkinnedInstances(const crSkeleton&, rmcDrawable* pDrawable, const atArray<datRef<const rmcInstanceDataBase> >* instanceArray, int bucket, int lod, rmcInstanceData_UsageInfo startRegister)
{
	Assert(startRegister >= 0 );
	InstDraw(instanceArray, pDrawable->GetShaderGroup(), pDrawable->GetLodGroup(),NULL,bucket,lod, startRegister);
}


} // namespace rage


