// 
// rmcore/drawablespu.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMCORE_DRAWABLESPU_H 
#define RMCORE_DRAWABLESPU_H 

#define DRAWABLE_ON_SPU		(SPU_GCM_FIFO > 2)

#include "grmodel/grmodelspu.h"

#if __SPU
#include "atl/bitset.h"
#include "math/intrinsics.h"
#endif

#define LOD_COUNT	4

namespace rage {

class grmShaderGroup;
class rmcDrawable;

struct spuLod
{
	explicit spuLod(datResource &rsc) : m_Models(GRCARRAY_PLACEELEMENTS,rsc) {}

	IMPLEMENT_PLACE_INLINE(spuLod)
	grcArray< datOwner<spuModel> > m_Models;
};

struct spuLodGroup
{
	explicit spuLodGroup(datResource &rsc,unsigned lod) { rsc.PointerFixup(m_Lod[lod]); spuLod::Place(m_Lod[lod],rsc); }
	__vector4 m_CullSphere;
	__vector4 m_BoxMin;
	__vector4 m_BoxMax;
	spuLod *m_Lod[LOD_COUNT];
	float m_LodThresh[LOD_COUNT];
	u32 m_BucketMask[LOD_COUNT];

	void DrawSingle(spuShaderGroup &group,spuMatrix44 &mtx,u32 bucketMask,int idx, u16 stats);
	void DrawMulti(spuShaderGroup &group,spuMatrix44 *rootMatrix,spuMatrixSet &ms,u32 bucketMask,int lodIdx, u16 stats);
};

struct spuDrawable
{
	explicit spuDrawable(datResource &rsc,unsigned lod) : m_LodGroup(rsc,lod) { }

	// rmcDrawableBase
	u32 vptr;
	u32 *m_PageMap;
	spuShaderGroup* m_ShaderGroup;	// intentionally not declared as datOwner because we don't want it fixed up as part of drawable

	// rmcDrawable
	void *m_SkeletonData;		// We don't need this

	spuLodGroup m_LodGroup;

	void *m_JointData;			// We don't need this

	u16 m_HandleIndex, m_ContainerSizeQW;

	void *m_ContainerPtr;
	const char *m_DebugName;

	void Draw(spuMatrix44& worldMtx,u32 bucketMask,int lod, u16 stats, bool captureStats);
	void DrawSkinned(spuMatrix44* rootMatrix,spuMatrixSet& matrixSet,u32 bucketMask,int lod, u16 stats, bool captureStats);
};

enum spuCommands_rmcore { 
	rmcDrawable__Draw = GRMODEL_COMMAND_COUNT,
	rmcDrawable__DrawSkinned,
	startStatRecord,
	stopStatRecord,
	RMCORE_COMMAND_COUNT
};

struct spuCmd_startStatRecord: public spuCmd_Any {
	// no operation, only used to change state.
};

struct spuCmd_stopStatRecord: public spuCmd_Any {
	// no operation, only used to change state.
};

struct spuCmd_rmcDrawable__Draw: public spuCmd_Any
{
#if __PPU
	rmcDrawable *drawable;			            // +4
	u16 bucketMask;                             // +8
	u16 lod;                                    // +10
	u16 stats;                                  // +12
	u16 containerSizeQW;                        // +14
	void *container;                            // +16
	grmShaderGroup *shaderGroup;                // +20
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		CGta4DbgSpuInfoStruct	gta4DebugInfo;	// +24
		u32                     alignPad[1];    // +28
	#else
		u32                     alignPad[2];    // +24
	#endif
	Matrix34 matrix;				            // +32
#else
	spuDrawable *drawable;			            // +4
	u16 bucketMask;                             // +8
	u16 lod;                                    // +10
	u16 stats;                                  // +12
	u16 containerSizeQW;                        // +14
	void *container;                            // +16
	spuShaderGroup *shaderGroup;                // +20
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		CGta4DbgSpuInfoStruct	gta4DebugInfo;	// +24
		u32                     alignPad[1];    // +28
	#else
		u32                     alignPad[2];    // +24
	#endif
	spuMatrix44 matrix;				            // +32
#endif
} ;

struct spuCmd_rmcDrawable__DrawSkinned: public spuCmd_Any
{
#if __PPU
	rmcDrawable *drawable;			            // +4
	grmMatrixSet *matrixSet;		            // +8
	u16 bucketMask;                             // +12
	u16 lod;                                    // +14
	u16 stats;                                  // +16
	u16 containerSizeQW;                        // +18
	void *container;                            // +20
	grmShaderGroup *shaderGroup;                // +24
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		CGta4DbgSpuInfoStruct	gta4DebugInfo;  // +28
	#else
		u32 alignPad[1];                        // +28
	#endif
	Matrix34 rootMatrix;			            // +32
#else
	spuDrawable *drawable;			            // +4
	spuMatrixSet *matrixSet;		            // +8
	u16 bucketMask;                             // +12
	u16 lod;                                    // +14
	u16 stats;                                  // +16
	u16 containerSizeQW;                        // +18
	void *container;                            // +20
	spuShaderGroup *shaderGroup;                // +24
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		CGta4DbgSpuInfoStruct	gta4DebugInfo;  // +28
	#else
		u32 alignPad[1];                        // +28
	#endif
	spuMatrix44 rootMatrix;			            // +32
#endif
} ;

} // namespace rage

#endif // RMCORE_DRAWABLESPU_H 
