// 
// audioeffecttypes/underwatereffect.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_UNDERWATEREFFECT_H
#define AUD_UNDERWATEREFFECT_H

#include "filterdefs.h"

#include "audiohardware/dspeffect.h"
#include "audiosynth/blocks.h"
#include "vectormath/vectormath.h"
#include "audiohardware/mixer.h"


namespace rage 
{

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to alignment
#endif

	class audUnderwaterResonator
	{
	public:

		audUnderwaterResonator()
		{
			m_PhaseStep = m_Phase = Vec::V4VConstant(V_ZERO);
		}

		void SetFilter(const float fc, const float bw)
		{
			m_BPF.Set4PoleBPF(Vec::V4LoadScalar32IntoSplatted(fc), Vec::V4LoadScalar32IntoSplatted(bw));
		}

		void SetOscillator(const float freq)
		{
			SetOscillator(Vec::V4LoadScalar32IntoSplatted(freq));	
		}

		void SetOscillator(Vec::Vector_4V_In freq)
		{
			CompileTimeAssert(kMixerNativeSampleRate == 48000);
			// 1/48kHz
			const Vec::Vector_4V oneOverFs = Vec::V4VConstantSplat<0x37AEC33E>();

			m_PhaseStep = Vec::V4Scale(freq, oneOverFs);
		}

		void SetEnvelope(const float attack, const float release)
		{
			SetEnvelope(Vec::V4LoadScalar32IntoSplatted(attack),Vec::V4LoadScalar32IntoSplatted(release));
		}

		void SetEnvelope(Vec::Vector_4V_In attack, Vec::Vector_4V_In release)
		{
			m_Envelope.SetAttackRelease(attack, release);
		}

		void Validate() const
		{
			m_Envelope.Validate();
			m_BPF.Validate();
		}

		Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
		{	
			Vec::Vector_4V e = m_Envelope.Process(m_BPF.Process(input));

			Vec::Vector_4V o = Vec::V4Sin(Vec::V4Scale(m_Phase, Vec::V4VConstant(V_TWO_PI)));
			m_Phase = Vec::V4Add(m_Phase, m_PhaseStep);
			// wrap phase at 1
			m_Phase = Vec::V4Subtract(m_Phase, Vec::V4RoundToNearestIntZero(m_Phase));
						
			return Vec::V4Scale(o, e);
		}

	private:
		Vec::Vector_4V m_Phase;
		Vec::Vector_4V m_PhaseStep;	
		revEnvFollower_V4 m_Envelope;
		revBiquad_V4 m_BPF;
		
	};

	class audUnderwaterEffect : public audDspEffect
	{
	public:
		audUnderwaterEffect();
		virtual ~audUnderwaterEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audEarlyReflectionEffectParams
		{
			Bypass = audEffectParamIds::UnderwaterEffect,
			Gain,
		};

	private:

		void ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples);

		enum {kNumResonators = 6};
		atRangeArray<audUnderwaterResonator, kNumResonators> m_Resonators;
		revBiquad_V4 m_LPF;
		
		Vec::Vector_4V m_Gain;
		bool m_Bypass;

	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //AUD_UNDERWATEREFFECT_H
