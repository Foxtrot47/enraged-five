// 
// audioeffecttypes/leveldetecteffect_xenon.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __XENON
#define AUDIO_BENCHMARK_EFFECTS __BANK

#include "leveldetecteffect_xenon.h"

#include "audiohardware/driver_xenon.h"
#include "vectormath/legacyconvert.h"

#if AUDIO_BENCHMARK_EFFECTS
#include "profile/profiler.h"
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_EFFECTS


namespace rage {

#if AUDIO_BENCHMARK_EFFECTS
PF_PAGE(LevelDetectionPage, "RAGE Audio Level Detection Effect");
PF_GROUP(LevelDetectionEffect);
PF_LINK(LevelDetectionPage, LevelDetectionEffect);
PF_TIMER(LevelDetectionTiming, LevelDetectionEffect);

//u32 g_LevelDetectBenchmarkFrameIndex = 0;
sysPerformanceTimer g_LevelDetectTimer("LevelDetect");
#endif // AUDIO_BENCHMARK_EFFECTS

const __vector4 v4Zero = {0,0,0,0};
const __vector4 v4InvAbsMask = Vec::V4VConstant(V_80000000);

audLevelDetectEffectXenon::audLevelDetectEffectXenon()
{
	
}

bool audLevelDetectEffectXenon::Init(const Effect *effectMetadata)
{
	const LevelDetectEffect *metadata = (const LevelDetectEffect *)effectMetadata;
	m_ChannelMask = metadata->ChannelMask.Value;

	m_NumChannelsToProcess = 0;
	for(u32 i=0; i<g_MaxOutputChannels; i++)
	{
		if(m_ChannelMask & (1 << i))
		{
			Assign(m_ChannelProcessOrder[m_NumChannelsToProcess],i);
			m_NumChannelsToProcess++;
		}
	}

	sysMemSet(m_MaxLevels, 0, sizeof(m_MaxLevels));
	sysMemSet(m_RmsLevels, 0, sizeof(m_RmsLevels));
	sysMemSet(m_RmsSquaredSumHistory, 0, sizeof(m_RmsSquaredSumHistory));

	m_RmsHistoryIndex = 0;

	return true;
}

void audLevelDetectEffectXenon::DoProcess(const audLevelDetectEffectXenonParams& UNUSED_PARAM(params), f32* __restrict pData, u32 numFrames, u32 ASSERT_ONLY(numChannels))
{
	__vector4 vInputSamples;
	__vector4 vMax;
	__vector4 vSquaredSums;

	
	Assert(pData);
	
#if AUDIO_BENCHMARK_EFFECTS
	PF_FUNC(LevelDetectionTiming);
#endif

	Assert(numChannels >= m_NumChannelsToProcess);

	f32 *samples;

#if USE_VECTORIZED_LEVELDETECTION // This block takes 30 microseconds to complete
	for(u32 i=0; i<m_NumChannelsToProcess; i++)
	{
		const u32 inputChannelIndex = m_ChannelProcessOrder[i];

		vMax = v4Zero;
		vSquaredSums = v4Zero;

		//We need to process this input channel.
		samples = &(pData[inputChannelIndex * numFrames]);

		for(u32 j = 0; j < numFrames; j+=16)
		{
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((__vector4*)&samples[j+0]);
			vMax = __vmaxfp( __vandc(vInputSamples, v4InvAbsMask), vMax);	// Computes 4 maxes
			vSquaredSums += __vdot4fp(vInputSamples, vInputSamples);	// Adds sum of the squares of the 4 elements
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((__vector4*)&samples[j+4]);
			vMax = __vmaxfp( __vandc(vInputSamples, v4InvAbsMask), vMax);	// Computes 4 maxes
			vSquaredSums += __vdot4fp(vInputSamples, vInputSamples);	// Adds sum of the squares of the 4 elements
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((__vector4*)&samples[j+8]);
			vMax = __vmaxfp( __vandc(vInputSamples, v4InvAbsMask), vMax);	// Computes 4 maxes
			vSquaredSums += __vdot4fp(vInputSamples, vInputSamples);	// Adds sum of the squares of the 4 elements
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((__vector4*)&samples[j+12]);
			vMax = __vmaxfp( __vandc(vInputSamples, v4InvAbsMask), vMax);	// Computes 4 maxes
			vSquaredSums += __vdot4fp(vInputSamples, vInputSamples);	// Adds sum of the squares of the 4 elements
			///////////////////////////////////////////////////////////////////////////
		}

		//	Store the highest 4 values in memory and avoid converting them to floats until we need them later
		m_MaxLevels[inputChannelIndex] = __vmaxfp(m_MaxLevels[inputChannelIndex], vMax);

		//	Sum up the squares of each input value
		m_RmsSquaredSumHistory[inputChannelIndex] += vSquaredSums;
	} //inputChannelIndex

#else // USE_VECTORIZED_DETECTION  This block takes 400 microseconds to complete
	f32 inputSample;
	f32 absInputSample;
	for(u32 i=0; i<m_NumChannelsToProcess; i++)
	{
		const u32 inputChannelIndex = m_ChannelProcessOrder[i];

		//We need to process this input channel.
		samples = &(pData[inputChannelIndex * numFrames]);

		for(u32 j = 0; j < numFrames; j++)
		{
			inputSample = *(samples++);

			//	Store the max value
			absInputSample = Abs(inputSample);
			if (absInputSample > m_MaxLevels[inputChannelIndex])
				m_MaxLevels[inputChannelIndex] = absInputSample;

			//	Sum up the squares of each input value
			m_RmsSquaredSumHistory[inputChannelIndex] += square(inputSample);
		}
	} //inputChannelIndex
#endif // USE_VECTORIZED_LEVELDETECTION  Vectorized processing is your friend :)


	if (++m_RmsHistoryIndex == LEVELDETECT_RMS_WINDOW_FRAMES)
	{
		m_RmsHistoryIndex = 0;

		//	RMS is the square root of the average of the input samples over the number of window frames we specified
		for(u32 inputChannelIndex=0; inputChannelIndex<NUM_PLATFORM_SPEAKER_IDS; inputChannelIndex++)
		{
#if USE_VECTORIZED_LEVELDETECTION
			m_RmsLevels[inputChannelIndex] = Sqrtf(m_RmsSquaredSumHistory[inputChannelIndex].x / (numFrames*LEVELDETECT_RMS_WINDOW_FRAMES));
			m_RmsSquaredSumHistory[inputChannelIndex] = v4Zero; // Zero this accumulator out for the next run
#else
			m_RmsLevels[inputChannelIndex] = Sqrtf(m_RmsSquaredSumHistory[inputChannelIndex] / (numFrames*LEVELDETECT_RMS_WINDOW_FRAMES));
			m_RmsSquaredSumHistory[inputChannelIndex] = 0.0f; // Zero this accumulator out for the next run
#endif
		}
	}
}

void audLevelDetectEffectXenon::GetParameters(void *pParameters, UINT32 ASSERT_ONLY(parameterSizeBytes))
{
	Assert(pParameters);
	Assert(parameterSizeBytes == sizeof(tLevelDetectValues));
	tLevelDetectValues& rValues = *(tLevelDetectValues*)pParameters;
	
#if USE_VECTORIZED_LEVELDETECTION
	for (u32 i = 0; i < NUM_PLATFORM_SPEAKER_IDS; ++i)
	{
		float* pFloat = (float*)&m_MaxLevels[i];
		rValues.m_MaxLevels[i] = Max(pFloat[0], pFloat[1], pFloat[2], pFloat[3]);
		m_MaxLevels[i] = v4Zero;
	}
#else
	sysMemCpy(&rValues.m_MaxLevels, m_MaxLevels, sizeof(m_MaxLevels));
	sysMemSet(m_MaxLevels, 0, sizeof(m_MaxLevels));
#endif
	sysMemCpy(&rValues.m_RmsLevels, m_RmsLevels, sizeof(m_RmsLevels));
}


} // namespace rage

#endif // __XENON
