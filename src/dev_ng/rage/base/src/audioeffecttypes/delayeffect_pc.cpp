// 
// audioeffecttypes/delayeffect.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "delayeffect.h"
#include "system/memops.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"

#include "math/amath.h"

#ifdef AUDIO_BENCHMARK_EFFECTS
#include "profile/profiler.h"
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_EFFECTS

namespace rage {

#ifdef AUDIO_BENCHMARK_EFFECTS
PF_PAGE(AudioEffectsPage, "RAGE Audio Effects");
PF_GROUP(AudioEffects);
PF_LINK(AudioEffectsPage, AudioEffects);
PF_VALUE_FLOAT(DelayThreadPerc, AudioEffects);

u32 g_DelayFrameIndex = 0;
sysPerformanceTimer g_DelayTimer("Delay");
#endif // AUDIO_BENCHMARK_EFFECTS

#define SAMPLES_PER_MS (kMixerNativeSampleRate/1000)

audDelayEffectPc::audDelayEffectPc()
{
	m_DelayBufferWriteOffset = 0;

	for(u32 i=0; i<g_MaxOutputChannels; i++)
	{
		const u32 delaySamplesUnaligned	= DELAY_TIME_DEFAULT * SAMPLES_PER_MS;
		//Align the sample length of the delay to an integer multiple of the software mixer frame size.
		m_DelaySamples[i]		= DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)delaySamplesUnaligned / (f32)DELAY_ALIGNMENT_SAMPLES);
		m_GainLin[i]			= audDriverUtil::ComputeLinearVolumeFromDb(DELAY_GAIN_DEFAULT);
		m_FeedbackGainLin[i]	= audDriverUtil::ComputeLinearVolumeFromDb(DELAY_FEEDBACK_GAIN_DEFAULT);
	}

	//Align the length of the delay buffer to an integer multiple of the XAudio frame size.
	m_MaxDelaySamples = DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)DELAY_MAX_SAMPLES_UNALIGNED / (f32)DELAY_ALIGNMENT_SAMPLES);

	m_DelayBuffer = NULL;
}

bool audDelayEffectPc::Init(const u32 numChannels, const u32 channelMask)
{
	m_NumInputChannels = (u8)numChannels;
	m_ChannelMask = (u8)channelMask;

	m_NumChannelsToProcess = 0;
	u32 i;
	for(i=0; i<numChannels; i++)
	{
		if(m_ChannelMask & (1 << i))
		{
			m_NumChannelsToProcess++;
		}
	}

	m_DelayBuffer = rage_new f32[m_NumChannelsToProcess * m_MaxDelaySamples];
	sysMemSet(m_DelayBuffer, 0, m_NumChannelsToProcess * m_MaxDelaySamples * sizeof(f32));

    return true;
}

void audDelayEffectPc::Process(f32 *buf, u32 numSamples)
{
    Assert(buf);

	if(m_Bypass)
		return;

#ifdef AUDIO_BENCHMARK_EFFECTS
	g_DelayTimer.Start();
#endif // AUDIO_BENCHMARK_EFFECTS

	u32 channelIndex = 0;
    for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
    {
		if(m_ChannelMask & (1 << inputChannelIndex))
		{
			//We need to process this input channel.

			if(m_DelaySamples[inputChannelIndex] >= numSamples) //Ensure that the delay time is long enough to be valid (otherwise bypass.)
			{
				f32 *samples = &buf[inputChannelIndex * numSamples];
				f32 *delayBuffer = &(m_DelayBuffer[channelIndex * m_MaxDelaySamples]);
				u32 delayBufferReadOffset = (m_MaxDelaySamples + m_DelayBufferWriteOffset - m_DelaySamples[inputChannelIndex]) % m_MaxDelaySamples;
				f32 *delayReadBuffer = delayBuffer + delayBufferReadOffset;
				f32 *delayWriteBuffer = delayBuffer + m_DelayBufferWriteOffset;

				f32 gainLin = m_GainLin[inputChannelIndex];
				f32 feedbackGainLin = m_FeedbackGainLin[inputChannelIndex];

				for(u32 sampleIndex=0; sampleIndex<numSamples; sampleIndex++)
				{
					f32 sample = samples[sampleIndex];
					samples[sampleIndex] = /*sample + */(delayReadBuffer[sampleIndex] * gainLin); //Uncomment to include direct/dry signal in output.
					delayWriteBuffer[sampleIndex] = sample + (samples[sampleIndex] * feedbackGainLin);
				}
			}

			channelIndex++;
		}

    } //inputChannelIndex

	//Update ring buffer write offset.
    m_DelayBufferWriteOffset = (m_DelayBufferWriteOffset + numSamples) % m_MaxDelaySamples;

#ifdef AUDIO_BENCHMARK_EFFECTS
	g_DelayTimer.Stop();

	if(++g_DelayFrameIndex == 1)
	{
		f32 threadPerc = g_DelayTimer.GetTimeMS() * 18.75f;
		PF_SET(DelayThreadPerc, threadPerc);
		g_DelayTimer.Reset();
		g_DelayFrameIndex = 0;
	}
#endif // AUDIO_BENCHMARK_EFFECTS
}

void audDelayEffectPc::SetParam(const u32 paramId, const u32 value)
{
	if(paramId >= FrontLeftDelayTime && paramId <= RearRightDelayTime)
	{
		const u32 channelIndex = paramId - FrontLeftDelayTime;
		const u32 delaySamplesUnaligned	= value * (kMixBufNumSamples/1000);
		//Align the sample length of the delay to an integer multiple of the software mixer frame size.
		m_DelaySamples[channelIndex] = DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)delaySamplesUnaligned / (f32)DELAY_ALIGNMENT_SAMPLES);
	}
	else if(paramId == Bypass)
	{
		m_Bypass = (value != 0);
	}
	else
	{
		Assertf(false, "paramId %u is not a valid audDelayEffect u32 param id (val: %u; as f32: %f)", paramId, value, value);
	}
}

void audDelayEffectPc::SetParam(const u32 paramId, const f32 value)
{
	if(paramId >= FrontLeftGain	&& paramId <= RearRightGain)
	{
		const u32 channelIndex = paramId - FrontLeftGain;
		m_GainLin[channelIndex] = audDriverUtil::ComputeLinearVolumeFromDb(value);
	}
	else if(paramId >= FrontLeftFeedback && paramId <= RearRightFeedback)
	{
		const u32 channelIndex = paramId - FrontLeftFeedback;
		m_FeedbackGainLin[channelIndex] = audDriverUtil::ComputeLinearVolumeFromDb(value);
	}
	else
	{
		Assertf(false, "paramId %u is not a valid audDelayEffect f32 param id (val: %f; as u32: %u)", paramId, value, value);::SetParam(const u32 paramId, const f32 value);
	}
}

void audDelayEffectPc::Shutdown(void)
{
	if(m_DelayBuffer)
	{
		delete[] m_DelayBuffer;
		m_DelayBuffer = NULL;
	}
}

} // namespace rage


