/* SCE CONFIDENTIAL
* PLAYSTATION(R)3 Programmer Tool Runtime Library 110.006 
* Copyright (C) 2006 Sony Computer Entertainment Inc.
* All Rights Reserved.
*/

/* dsp.h - MultiStream DSP effect header */
#ifndef _DSP_HEADER_H_
#define _DSP_HEADER_H_

/**********************************************************************************************************
D E F I N E S
**********************************************************************************************************/

#define MS_DSP_FREQUENCYBASE  (0)	// Used to define a DSP effect as being in Frequency Domain
#define MS_DSP_TIMEBASE       (1)	// Used to define a DSP effect as being in Time Domain
#define NATIVE_MULTISTREAM_SAMPLE_RATE	(48000.0f)

/**********************************************************************************************************
T Y P E S
**********************************************************************************************************/

//**** The following is the structure passed to both the DSPInit and DSPApply functions by MultiStream

typedef struct MS_DSP_INFO
{
	int callNumber;		// DSP slot (0-7) being processed by DSP effect, or -1 for a mono stream
	char inMask; 		// If not a mono stream, what the incoming channel mask is
	char outMask;		// If not a mono stream, what the outgoing channel mask is
	int state;			// 0 = Stream is currently playing. !0 = Number of calls to DSP since stream is complete
	void *Reserved1;	// Reserved for system use
	void *Reserved2;	// Reserved for system use
	float* Reserved3;	// Reserved for system use
	unsigned char Reserved4; // Reserved for system use
	void* dbgOut;		// Used for TTY output
	int bandwidthIn;	// For analysis. For each DMA from main RAM to SPU, user should increase this by the size transferred
	int bandwidthOut;	// For analysis. For each DMA from SPU to main RAM, user should increase this by the size transferred
} MS_DSP_INFO;

/*
The following is a single FFT complex number (real / imaginary)
DSP input / output data in Frequency domain require 512 of these
*/

typedef struct MS_COMPLEX_NUM
{
	float r;
	float i;
} MS_COMPLEX_NUM;

/*
Structure required to be completed within a MultiStream DSP effects SPU entry point
See "DSP Effect Programming" within the MultiStream Overview documentation for more information
*/

typedef struct DSPPlugin
{
	int (*DSPInit) (void *self);	// DSP initialisation function (or NULL if no init function required)
	int (*DSPApply)	(void *self);	// DSP Apply function (actually processes the DSP effect)
	int Domain;						// 0 = Freq, 1 = Time
} DSPPlugin;

/**********************************************************************************************************
P R O T O T Y P E S
**********************************************************************************************************/

extern "C" void DSPPlugInSetup(DSPPlugin *plugInStruct);
extern "C" void DSPInit(char *paramList, void *workArea, MS_DSP_INFO *dspInfo);
extern "C" int DSPApply(void* input, void* output, char *paramList, void *workArea, MS_DSP_INFO *dspInfo);

// helper for function pointer cast
typedef int (*ms_fnct)(void*);

#endif

