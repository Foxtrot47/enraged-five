#if __XENON

#include "deinterleaver_xenon.h"
#include "system/memops.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	ALIGNAS(16) f32 g_InterleavedWorkBuffer[6 * 256];

	void audDeinterleaverEffect::DoProcess(const tDeinterleaverEffectSettings& UNUSED_PARAM(params), f32* __restrict pData, u32 numFrames, u32 ASSERT_ONLY(numChannels))
	{
		Assert(numFrames == 256);
		Assert(numChannels == 6);

		Vector_4V *input = (Vector_4V*)pData;
		Vector_4V *output[6] = {
			(Vector_4V*)&g_InterleavedWorkBuffer[numFrames*0],
			(Vector_4V*)&g_InterleavedWorkBuffer[numFrames*1],
			(Vector_4V*)&g_InterleavedWorkBuffer[numFrames*2],
			(Vector_4V*)&g_InterleavedWorkBuffer[numFrames*3],
			(Vector_4V*)&g_InterleavedWorkBuffer[numFrames*4],
			(Vector_4V*)&g_InterleavedWorkBuffer[numFrames*5],
		};

		for(u32 i = 0; i < numFrames; i += 4)
		{
			const Vector_4V samples_0123_x = *input++;
			const Vector_4V samples_4501_xy = *input++;
			const Vector_4V samples_2345_y = *input++;
			const Vector_4V samples_0123_z = *input++;
			const Vector_4V samples_4501_zw = *input++;
			const Vector_4V samples_2345_w = *input++;

			const Vector_4V samples_0011_xy = V4PermuteTwo<X1,Z2,Y1,W2>(samples_0123_x,samples_4501_xy);
			const Vector_4V samples_0011_zw = V4PermuteTwo<X1,Z2,Y1,W2>(samples_0123_z,samples_4501_zw);

			const Vector_4V samples_2233_xy = V4PermuteTwo<Z1,X2,W1,Y2>(samples_0123_x, samples_2345_y);
			const Vector_4V samples_2233_zw = V4PermuteTwo<Z1,X2,W1,Y2>(samples_0123_z, samples_2345_w);

			const Vector_4V samples_4455_xy = V4PermuteTwo<X1,Z2,Y1,W2>(samples_4501_xy, samples_2345_y);
			const Vector_4V samples_4455_zw = V4PermuteTwo<X1,Z2,Y1,W2>(samples_4501_zw, samples_2345_w);

			const Vector_4V samples_0000_xyzw = V4PermuteTwo<X1,Y1,X2,Y2>(samples_0011_xy,samples_0011_zw);
			const Vector_4V samples_1111_xyzw = V4PermuteTwo<Z1,W1,Z2,W2>(samples_0011_xy,samples_0011_zw);
			const Vector_4V samples_2222_xyzw = V4PermuteTwo<X1,Y1,X2,Y2>(samples_2233_xy,samples_2233_zw);
			const Vector_4V samples_3333_xyzw = V4PermuteTwo<Z1,W1,Z2,W2>(samples_2233_xy,samples_2233_xy);
			const Vector_4V samples_4444_xyzw = V4PermuteTwo<X1,Y1,X2,Y2>(samples_4455_xy,samples_4455_zw);
			const Vector_4V samples_5555_xyzw = V4PermuteTwo<Z1,W1,Z2,W2>(samples_4455_xy,samples_4455_zw);

			*output[0]++ = samples_0000_xyzw;
			*output[1]++ = samples_1111_xyzw;
			*output[2]++ = samples_2222_xyzw;
			*output[3]++ = samples_3333_xyzw;
			*output[4]++ = samples_4444_xyzw;
			*output[5]++ = samples_5555_xyzw;
		}

		{
			Vector_4V *out = (Vector_4V*)pData;
			const Vector_4V *input = (Vector_4V*)&g_InterleavedWorkBuffer[0];
			for(u32 frameIdx = 0; frameIdx < 256/4; frameIdx ++)
			{
				*out++ = *input++;
				*out++ = *input++;
				*out++ = *input++;
				*out++ = *input++;
				*out++ = *input++;
				*out++ = *input++;
			}
		}
	}
}
#endif // __XENON
