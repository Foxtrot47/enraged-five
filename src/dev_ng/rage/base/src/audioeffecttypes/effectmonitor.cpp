// 
// audioeffecttypes/effectmonitor.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "effectmonitor.h"
#include "audiohardware/channel.h"
#include "audiohardware/driver.h"

#if __SPU
#include <cell/dma.h>
#else
#include "grcore/im.h"
#include "grcore/viewport.h"
#endif

namespace rage {

audEffectMonitor::audEffectMonitor() : 
m_NumChannels(0), 
m_NumStages(0),
m_SamplesEA(NULL),
m_SamplesLS(NULL),
m_MaxLevels(NULL),
m_IsActive(false)
{

}

audEffectMonitor::~audEffectMonitor()
{
}

#if !__SPU
bool audEffectMonitor::Init(const u32 numChannels, const u32 numStages)
{
	Assert(numChannels > 0 && numStages > 0);
	m_NumChannels = numChannels;
	m_NumStages = numStages;
	m_SampleBufSize = (((sizeof(tEffectMonitorSample) * numChannels * numStages) + 15) & ~15);
	m_SamplesEA = static_cast<tEffectMonitorSample*>(audDriver::AllocateVirtual(m_SampleBufSize));
	m_MaxLevels = static_cast<f32*>(audDriver::AllocateVirtual(sizeof(f32) * numChannels * numStages));
	if(!m_SamplesEA || !m_MaxLevels)
	{
		return false;
	}
	for(u32 i = 0 ; i < m_NumStages; i++)
	{
		ResetStage(i);
	}
	return true;
}

void audEffectMonitor::Shutdown()
{
	if(m_SamplesEA)
	{
		audDriver::FreeVirtual(m_SamplesEA);
		m_SamplesEA = NULL;
	}
	if(m_MaxLevels)
	{
		audDriver::FreeVirtual(m_MaxLevels);
		m_MaxLevels = NULL;
	}
	m_NumChannels = 0;
	m_NumStages = 0;
}

void audEffectMonitor::ResetStage(const u32 stage)
{
	Assert(stage < m_NumStages);
	for(u32 i = 0; i < m_NumChannels; i++)
	{
		m_MaxLevels[stage*m_NumChannels + i] = 0.f;
	}
}

void audEffectMonitor::PostProcess()
{
	if(m_IsActive)
	{
		for(u32 stage = 0; stage < m_NumStages; stage++)
		{
			for(u32 channel = 0; channel < m_NumChannels; channel ++)
			{
				m_MaxLevels[stage*m_NumChannels + channel] *= 0.999f;
				m_MaxLevels[stage*m_NumChannels + channel] = Max(m_MaxLevels[stage*m_NumChannels + channel], m_SamplesEA[stage*m_NumChannels + channel].PeakLevel);
			}
		}
	}
}
#define DRAW_BAR(c) grcBegin(drawTriStrip, 4); \
grcColor(c); \
grcVertex2f(channelXOffset + barXOffset, y + height - (height * linVal));	 \
grcVertex2f(channelXOffset + barXOffset, y + height); \
grcVertex2f(channelXOffset + barXOffset + barWidth*0.9f, y + height - (height * linVal));	\
grcVertex2f(channelXOffset + barXOffset + barWidth*0.9f, y + height); \
grcEnd();

void audEffectMonitor::DrawMark(const f32 linVal, const char *label,const f32 x, const f32 y, const f32 height)
{
	const f32 lineX = x-10.f;
	const f32 lineY = y + height - (height * linVal);
	grcDrawLine(Vec3V(lineX, lineY, 0.f), Vec3V(x, lineY, 0.f), Color32(1.f,1.f,1.f,1.f));
	grcDraw2dText(lineX-15.f, lineY-7.5f, label);
}

void audEffectMonitor::Draw(const f32 x, const f32 y, const f32 width, const f32 height)
{
	const f32 stageWidth = width / (f32)m_NumStages;
	const f32 channelWidth = (stageWidth / (f32)m_NumChannels);
	const f32 barWidth = (channelWidth*0.95f)/3.f;

	PUSH_DEFAULT_SCREEN();

	grcBegin(drawTriStrip, 4);
		grcColor(Color32(0.1f,0.1f,0.1f,0.5f));
		grcVertex2f(x-10,y);	
		grcVertex2f(x-10,y+height);
		grcVertex2f(x+width,y);
		grcVertex2f(x+width,y+height);
	grcEnd();

	
	char buf[16];
	for(s32 i = 0; i > -24; i -= 3)
	{
		formatf(buf, 15, "%d", i);
		DrawMark(audDriverUtil::ComputeLinearVolumeFromDb((f32)i), buf, x, y, height);
	}

	for(u32 stage = 0; stage < m_NumStages; stage++)
	{
		const f32 stageXOffset = x + stage * stageWidth;
		for(u32 channel = 0; channel < m_NumChannels; channel++)
		{
			const f32 channelXOffset = stageXOffset + channel * channelWidth;
			f32 barXOffset = 0.f;
			f32 linVal = 0.f;
			
			linVal = m_SamplesEA[stage*m_NumChannels + channel].PeakLevel;
			DRAW_BAR(Color32(1.0f, 0.6f, 0.5f, 0.75f));
			grcDrawLine(Vec3V(channelXOffset + barXOffset, y + height - (height * m_MaxLevels[stage*m_NumChannels + channel]), 0.f),
							Vec3V(channelXOffset + barXOffset + barWidth, y + height - (height * m_MaxLevels[stage*m_NumChannels + channel]), 0.f),Color32(255,0,0));
			barXOffset += barWidth;
			linVal = m_SamplesEA[stage*m_NumChannels + channel].RmsLevel;
			DRAW_BAR(Color32(0.4f, 1.0f, 0.5f, 0.75f));
			barXOffset += barWidth;
			linVal = m_SamplesEA[stage*m_NumChannels + channel].FloorLevel;
			DRAW_BAR(Color32(0.5f, 0.3f, 1.0f, 0.75f));
		}
	}

	POP_DEFAULT_SCREEN();
}

#endif // !__SPU

void audEffectMonitor::InitFrame()
{
	u32 bytes = 0;
	InitFrame(NULL, bytes);
}

void audEffectMonitor::InitFrame(u8 **SPU_ONLY(workArea), u32 &SPU_ONLY(workAreaBytes))
{
	if(m_IsActive)
	{
	#if __SPU
		const u32 numBytesRequired = (sizeof(tEffectMonitorSample) * m_NumChannels * m_NumStages);
		Assert(workAreaBytes > numBytesRequired);

		// allocate space out of the work area
		m_SamplesLS = (tEffectMonitorSample*)*workArea;
		workAreaBytes -= numBytesRequired;
		(*workArea) += numBytesRequired;
	#else
		m_SamplesLS = m_SamplesEA;
	#endif
	}
}

void audEffectMonitor::ProcessStage(const u32 stage, const u32 channel, const f32 *samples, const u32 numSamples)
{
	Assert(stage < m_NumStages);
	Assert(channel < m_NumChannels);
	////// TODO - why is this firing? Assert(m_SamplesLS);

	if(!m_IsActive || stage >= m_NumStages || channel >= m_NumChannels || !m_SamplesLS)
	{
		return;
	}

	f32 peakValue = 0.f;
	f32 sqSum = 0.f;
	f32 floorValue = 99999.f;
	for(u32 i = 0; i < numSamples; i++)
	{
		peakValue = Max(peakValue, Abs(samples[i]));
		floorValue = Min(floorValue, Abs(samples[i]));
		sqSum += samples[i]*samples[i];
	}

	const f32 rmsValue = Sqrtf((sqSum / (f32)numSamples));

	m_SamplesLS[stage*m_NumChannels + channel].PeakLevel = peakValue;
	m_SamplesLS[stage*m_NumChannels + channel].FloorLevel = floorValue;	
	m_SamplesLS[stage*m_NumChannels + channel].RmsLevel = rmsValue;
}
	
void audEffectMonitor::FinalizeFrame()
{
	if(m_IsActive)
	{
	#if __SPU
		const u32 TAG = 8;
		cellDmaPut(m_SamplesLS, (uint64_t)m_SamplesEA, m_SampleBufSize, TAG, 0, 0);
	#endif

		m_SamplesLS = NULL;
	}
}

void audEffectMonitor::Print()
{
	for(u32 stage = 0 ; stage < m_NumStages; stage++)
	{
		audDisplayf("Stage %u:", stage);
		for(u32 chan = 0; chan < m_NumChannels; chan++)
		{
			audDisplayf("Channel %u: %f %f %f", chan, m_SamplesLS[stage*m_NumChannels + chan].PeakLevel, m_SamplesLS[stage*m_NumChannels + chan].FloorLevel, m_SamplesLS[stage*m_NumChannels + chan].RmsLevel);
		}
	}
}

} // namespace rage
