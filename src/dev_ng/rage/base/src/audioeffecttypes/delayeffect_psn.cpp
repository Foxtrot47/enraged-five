#if __SPU

#include "filterdefs.h"
#include "psn_dsp.h"
#include "vector/vector3_consts_spu.cpp"
#include "audiohardware/driverutil.h"
#include "math/amath.h"

#include <cell/dma.h>

#define TRUE				(1)
#define FALSE				(0)
#define EPISILON			(0.5f)
#define TAG					(7)

using namespace rage;

//
u32 GetChannelIndex(const u8 msMask)
{
	u8 flag = 1;
	for (u32 i=0; i<g_MaxOutputChannels; ++i)
	{
		if (msMask & flag)
		{
			return i;
		}
		flag <<= 1;
	}

	return 0;
}


int IsDelayParamsChanged(const tDelayFilterSPUParamData *pNext, const tDelayFilterSPUParamData *pLast)
{
	for (u32 i=0; i<g_MaxOutputChannels; ++i)
	{
		if (pNext->m_Settings.TimeMs[i] != pLast->m_Settings.TimeMs[i])
		{
			return TRUE;
		}

		if (pNext->m_Settings.FeedbackGainDb[i] > pLast->m_Settings.FeedbackGainDb[i] + EPISILON ||
			pNext->m_Settings.FeedbackGainDb[i] < pLast->m_Settings.FeedbackGainDb[i] - EPISILON)
		{
			return TRUE;
		}

		if (pNext->m_Settings.GainDb[i] > pLast->m_Settings.GainDb[i] + EPISILON ||
			pNext->m_Settings.GainDb[i] < pLast->m_Settings.GainDb[i] - EPISILON)
		{
			return TRUE;
		}
	}

	return FALSE;
}

void ComputeParameters(const tDelayFilterSPUParamData *pParams, tDelayFilterSPUData *pWorkData)
{
	for (u32 i=0; i<g_MaxOutputChannels; ++i)
	{
		const u32 timeMs				= Clamp(pParams->m_Settings.TimeMs[i], (u32)DELAY_TIME_MIN, (u32)DELAY_TIME_MAX);
		const f32 gainDb				= Clamp(pParams->m_Settings.GainDb[i], DELAY_GAIN_MIN, DELAY_GAIN_MAX);
		const f32 feedbackGainDb		= Clamp(pParams->m_Settings.FeedbackGainDb[i], DELAY_FEEDBACK_GAIN_MIN, DELAY_FEEDBACK_GAIN_MAX);

		const u32 delaySamplesUnaligned	= timeMs * SAMPLES_PER_MS;
		//Align the sample length of the delay to an integer multiple of the DSP block size.
		pWorkData->m_DelaySamples[i]	= DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)delaySamplesUnaligned / (f32)DELAY_ALIGNMENT_SAMPLES);
		pWorkData->m_GainLin[i]			= audDriverUtil::ComputeLinearVolumeFromDb(gainDb);
		pWorkData->m_FeedbackGainLin[i] = audDriverUtil::ComputeLinearVolumeFromDb(feedbackGainDb);

		/*Displayf("Delay: ch(%d) - time:%d - gaindb:%f feedback:%f\nsamples:%d - gain:%f - feedback:%f", i, 
			pParams->m_Settings.TimeMs[i], pParams->m_Settings.GainDb[i], pParams->m_Settings.FeedbackGainDb[i],
			pWorkData->m_DelaySamples[i], pWorkData->m_GainLin[i], pWorkData->m_FeedbackGainLin[i]);*/
	}
}

/**********************************************************************************
DSPInit
Initialisation routine
This routine is called by MultiStream when the DSP effect is first called.

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
paramList		Address of static parameter data (248 bytes)
	0-127		= Read Only (example: users input parameters)
	128-247		= Read / Write (example: DSP effects internal parameters)
workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area
dspInfo			Contains information as to the slot number to be processed, input / output bitmasks

Notes:
Each DSP effect has 248 bytes of RAM available.
If more RAM is required, this area can be used to store a pointer to the actual data.
The data can then be transferred from main RAM as required.
Splitting the paramList into a read and a read/write section allows us to make sure that if
the user is modifying parameters via PPU commands (example: distortion level), there is no conflicts
between the SPU updating parameter memory at the same time.

**********************************************************************************/
void DSPInit(char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{
}

/**********************************************************************************
DSPApply
Apply DSP effect routine
This routine is called by MultiStream to apply a DSP effect to an input signal

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
input			Address of input data (Time Domain float32 data OR Frequency Domain FFT data)
				For PCM, 1024 samples are passed in.
				For FFT, 512 frequency bands are passed in.

output			Address to write output data to

paramList		Address of static parameter data (248 bytes)
				0-127 = Read Only
				128-247 = Read / Write

workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area

dspInfo			Contains information as to the slot number to be processed, input / output bitmasks..

Returns:
0				DSP will complete when stream finishes
!0				DSP will stay active after stream finishes (for delays / reverbs)

Notes:
For time domain input data, the user must process all 1024 samples.
But note that any parameters used for modifying data must be preserved after 512 samples.
This is due to windowing within MultiStream.

**********************************************************************************/
int DSPApply(void* input, void* output, char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{
	tDelayFilterSPUParamData *pNextParams = (tDelayFilterSPUParamData *)paramList;
	tDelayFilterSPUParamData *pLastParams = (tDelayFilterSPUParamData *)(paramList+128);

	//Ensure that all previous effect DMAs have completed before we start.
	cellDmaWaitTagStatusAll(1 << TAG);

	//////////////////////////////////////////////////////////////////////////
	// dma across the work data; we'll also need to dma the delay buffer
	uint64_t ea = (uint64_t)pNextParams->EA;
	if(ea == 0)
	{
		//This effect has not yet been initialized on the PPU, so early-out.
		return 0;
	}

	cellDmaGet(workArea, ea, sizeof(tDelayFilterSPUData), TAG, 0, 0);
	cellDmaWaitTagStatusAll(1 << TAG);
	dspInfo->bandwidthIn += sizeof(tDelayFilterSPUData);

	tDelayFilterSPUData *pWorkData = (tDelayFilterSPUData *)workArea;

	//////////////////////////////////////////////////////////////////////////
	// only perform DSP if our metadata masks match
	if ( !(dspInfo->callNumber == -1) && !(dspInfo->inMask & pWorkData->m_ChannelMask) )
	{
		//Copy input buffer into output buffer - bypass mode.
		for(u32 cnt = 0; cnt<FRAME_SAMPLES; cnt++)
		{
			((f32 *)output)[cnt] = ((f32 *)input)[cnt];
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	// have our parameters changed such that we need to recalc coefficients?
	if (IsDelayParamsChanged(pNextParams, pLastParams) == TRUE)
	{
		*pLastParams = *pNextParams;

		ComputeParameters(pLastParams, pWorkData);
	}

	//////////////////////////////////////////////////////////////////////////
	// get channel number
	const int channelNum = GetChannelIndex(dspInfo->inMask);

	if(pWorkData->m_DelaySamples[channelNum] < FRAME_SAMPLES)
	{
		//The delay time for this channel is too short, so bypass.
		// - Copy input buffer into output buffer.
		for(u32 cnt = 0; cnt<FRAME_SAMPLES; cnt++)
		{
			((f32 *)output)[cnt] = ((f32 *)input)[cnt];
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	// append the delay buffer sections for this channel after the settings info
	f32 *delayReadBuffer1 = (f32 *)(pWorkData + 1);
	f32 *delayReadBuffer2 = delayReadBuffer1 + FRAME_SAMPLES;
	f32 *delayWriteBuffer = delayReadBuffer2 + FRAME_SAMPLES;

	const u32 delaySamples = pWorkData->m_DelaySamples[channelNum];
	const f32 gainLin = pWorkData->m_GainLin[channelNum];
	const f32 feedbackGainLin = pWorkData->m_FeedbackGainLin[channelNum];
	const u32 delayBufferWriteOffset = pWorkData->m_DelayBufferWriteOffset[channelNum];

	//Align the length of the delay buffer to an integer multiple of the DSP block size.
	const u32 maxDelaySamples = DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)DELAY_MAX_SAMPLES_UNALIGNED / (f32)DELAY_ALIGNMENT_SAMPLES);

	u32 delayBufferReadOffset1 = (maxDelaySamples + delayBufferWriteOffset - delaySamples) % maxDelaySamples;
	u32 delayBufferReadOffset2 = (delayBufferReadOffset1 + FRAME_SAMPLES) % maxDelaySamples;
	f32 *delayBuffer = pWorkData->m_DelayBuffer[channelNum];

	//The 2 read sections of the delay buffer are not guaranteed to be contiguous, so we will always fetch them separately.
	uint64_t delayBufferAddr = (uint64_t)(delayBuffer + delayBufferReadOffset1);
	cellDmaGet(delayReadBuffer1, delayBufferAddr, FRAME_SAMPLES * sizeof(f32), TAG, 0, 0);
	delayBufferAddr = (uint64_t)(delayBuffer + delayBufferReadOffset2);
	cellDmaGet(delayReadBuffer2, delayBufferAddr, FRAME_SAMPLES * sizeof(f32), TAG, 0, 0);
	dspInfo->bandwidthIn += FRAME_SAMPLES * sizeof(f32);
	cellDmaWaitTagStatusAll(1 << TAG);

	f32 *inputSamples = (f32 *)input;
	f32 *outputSamples = (f32 *)output;
	for (u32 i=0; i<FRAME_SAMPLES; i++)
	{
		outputSamples[i] = delayReadBuffer1[i] * gainLin;
		delayWriteBuffer[i] = inputSamples[i] + (outputSamples[i] * feedbackGainLin);
	}

	//Displayf("gainLin=%f, feedbackGainLin=%f, delayBufferWriteOffset=%u, delaySamples=%u", gainLin, feedbackGainLin, delayBufferWriteOffset, delaySamples);

	pWorkData->m_DelayBufferWriteOffset[channelNum] = (delayBufferWriteOffset + FRAME_SAMPLES) % maxDelaySamples;

	//Copy back the effect settings.
	cellDmaPut(workArea, ea, sizeof(tDelayFilterSPUData), TAG, 0, 0);
	//cellDmaWaitTagStatusAll(1 << TAG);
	dspInfo->bandwidthOut += sizeof(tDelayFilterSPUData);

	//Copy back the delay write buffer.
	delayBufferAddr = (uint64_t)(delayBuffer + delayBufferWriteOffset);
	cellDmaPut(delayWriteBuffer, delayBufferAddr, FRAME_SAMPLES * sizeof(f32), TAG, 0, 0);
	//cellDmaWaitTagStatusAll(1 << TAG);
	dspInfo->bandwidthOut += FRAME_SAMPLES * sizeof(f32);

	// If the stream ends and (and if this DSP effect is attached to a stream), end the DSP effect too..
	return 0;
}

/**********************************************************************************
DSPPlugInSetup
Setup as the entry point function within the DSP makefile
Initialises the DSP effect functions.
Called by MultiStream.

Requires:
plugInStruct		Address of a DSPPlugin structure

Notes:
This function sets the address of both the Init and the Apply functions.
It also specifies if the input / output data is required as time domain or FFT data
**********************************************************************************/
void DSPPlugInSetup(DSPPlugin* plugInStruct)
{
	plugInStruct->DSPInit = (ms_fnct)DSPInit;			// Init function (or NULL if no init function required)
	plugInStruct->DSPApply = (ms_fnct)DSPApply;			// DSP "Apply" function (processes DSP effect)
	plugInStruct->Domain = MS_DSP_TIMEBASE;				// DSP Effect is Frequency based (complex numbers)
}


#endif	// __SPU
