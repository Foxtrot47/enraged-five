// 
// audioeffecttypes/underwatereffect.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "underwatereffect.h"
#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/framebufferpool.h"

#include "system/performancetimer.h"

namespace rage 
{
	using namespace Vec;
	audUnderwaterEffect::audUnderwaterEffect() 
		: audDspEffect(AUD_DSPEFFECT_UNDERWATER)
		, m_Bypass(true)
	{

	}

	audUnderwaterEffect::~audUnderwaterEffect()
	{

	}


	bool audUnderwaterEffect::Init(const u32 ASSERT_ONLY(numChannels), const u32 ASSERT_ONLY(channelMask))
	{
		Assert(numChannels == 4);
		Assert(channelMask == (RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT));

		SetProcessFormat(Interleaved);

		m_Bypass = true;

		m_Gain = V4VConstant(V_ZERO);

		m_LPF.Set4PoleLPF(1500.f, 0.4f);

		const float freqMult = 0.42f;

		float freq = 120.f;
		const float width = 18.f;
		for(s32 i = 0; i < kNumResonators; i++)
		{
			m_Resonators[i].SetFilter(freq, width);
			m_Resonators[i].SetEnvelope(0.f, 350.f / freq);
			m_Resonators[i].SetOscillator(freq * freqMult);

			freq *= 1.138f;
		}

		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audUnderwaterEffect)
	{
		if(m_Bypass)
		{
			return;
		}

#if AUD_TIME_DSP
		sysPerformanceTimer timer("");
		timer.Start();
#endif

		Assert(buffer.Format == Interleaved);

		const u32 numSamplesPerPass = kMixBufNumSamples / 4;

		if (buffer.ChannelBufferIds[0] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[0], buffer.ChannelBuffers[0], numSamplesPerPass);
		if (buffer.ChannelBufferIds[1] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[1], buffer.ChannelBuffers[1], numSamplesPerPass);
		if (buffer.ChannelBufferIds[2] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[2], buffer.ChannelBuffers[2], numSamplesPerPass);
		if (buffer.ChannelBufferIds[3] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[3], buffer.ChannelBuffers[3], numSamplesPerPass);

#if RSG_ASSERT
		m_LPF.Validate();
		for(s32 i = 0; i < kNumResonators; i++)
		{
			m_Resonators[i].Validate();
		}
#endif

#if AUD_TIME_DSP
		timer.Stop();
		sm_ProcessTime[AUD_DSPEFFECT_UNDERWATER] = timer.GetTimeMS() * 1000.f;
#endif		
	}

	void audUnderwaterEffect::ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples)
	{
		const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outBuffer);
		const Vector_4V oneAndAHalf = V4VConstant<0x3FC00000,0x3FC00000,0x3FC00000,0x3FC00000>();
		u32 count = numSamples;

		while(count--)
		{
			const Vector_4V input = *inputPtr++;

			Vector_4V output = V4VConstant(V_ZERO);
			// sum resonator output
			for(s32 i = 0; i < kNumResonators; i++)
			{
				output = V4Add(output, m_Resonators[i].Process(input));
			}

			// filter 'dry' route
			output = V4Add(output, /*m_HPF.Process*/(m_LPF.Process(input)));

			output = V4Scale(output, m_Gain);

			// soft clip
			Vector_4V scaledInput = V4Abs(output);			

			Vector_4V scaledInputCubed = V4Scale(scaledInput, V4Scale(scaledInput, scaledInput));

			Vector_4V unscaledOutput = V4Subtract(V4Scale(oneAndAHalf, scaledInput), V4Scale(V4VConstant(V_HALF), scaledInputCubed));

			Vector_4V sign = V4SelectFT(
				V4IsGreaterThanOrEqualV(output, V4VConstant(V_ZERO)), 
				V4VConstant(V_NEGONE), 
				V4VConstant(V_ONE));

			*outputPtr++ = V4Scale(sign, unscaledOutput);
		}
	}

#if !__SPU
	void audUnderwaterEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case Bypass:
			m_Bypass = (value != 0);
			break;		
		default:
			audAssertf(false, "paramId %u is not a valid audUnderwaterEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audUnderwaterEffect::SetParam(const u32 paramId, const f32 value)
	{
		switch(paramId)
		{
		case Gain:
			m_Gain = V4LoadScalar32IntoSplatted(2.f * value);
			m_Bypass = (value <= g_SilenceVolumeLin);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audUnderwaterEffect float param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}

	}

	void audUnderwaterEffect::Shutdown(void)
	{

	}
#endif // !__SPU

} // namespace rage


