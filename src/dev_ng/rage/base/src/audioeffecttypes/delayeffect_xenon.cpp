//
// audioeffects/delayeffect_xenon.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON

#include "delayeffect_xenon.h"

#include "delayeffect.h"

#include "audiohardware/driverutil.h"
#ifdef AUDIO_BENCHMARK_EFFECTS
#include "profile/profiler.h"
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_EFFECTS

namespace rage {

#ifdef AUDIO_BENCHMARK_EFFECTS
PF_PAGE(AudioDelayPage, "RAGE Audio Delay Effect");
PF_GROUP(DelayEffect);
PF_LINK(AudioDelayPage, DelayEffect);
PF_VALUE_FLOAT(DelayThreadPerc, DelayEffect);

u32 g_DelayFrameIndex = 0;
sysPerformanceTimer g_DelayTimer("Delay");
#endif // AUDIO_BENCHMARK_EFFECTS

audDelayEffectXenon::audDelayEffectXenon()
{

	m_DelayBufferWriteOffset = 0;

	for(u8 i=0; i<g_MaxOutputChannels; i++)
	{
		const u32 delaySamplesUnaligned	= m_Settings.TimeMs[i] * SAMPLES_PER_MS;
		//Align the sample length of the delay to an integer multiple of the XAudio frame size.
		m_DelaySamples[i]		= DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)delaySamplesUnaligned / (f32)DELAY_ALIGNMENT_SAMPLES);
		m_GainLin[i]			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.GainDb[i]);
		m_FeedbackGainLin[i]	= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.FeedbackGainDb[i]);
	}

	m_DelayBuffer = NULL;

	//Align the length of the delay buffer to an integer multiple of the XAudio frame size.
	m_MaxDelaySamples = DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)DELAY_MAX_SAMPLES_UNALIGNED / (f32)DELAY_ALIGNMENT_SAMPLES);
}

bool audDelayEffectXenon::Init(const Effect *effectMetadata)
{
	m_ChannelMask = effectMetadata->ChannelMask.Value;

	DelayEffect *metadata = (DelayEffect*)effectMetadata;

	m_NumChannelsToProcess = 0;
	u32 i;
	for(i=0; i<g_MaxOutputChannels; i++)
	{
		if(m_ChannelMask & (1 << i))
		{
			m_NumChannelsToProcess++;
		}
	}

	m_DelayBuffer = rage_aligned_new(16) f32[m_NumChannelsToProcess * m_MaxDelaySamples];
	sysMemSet(m_DelayBuffer, 0, m_NumChannelsToProcess * m_MaxDelaySamples * sizeof(f32));

	//Populate initial settings with values from metadata.
	for(i=0; i<g_MaxOutputChannels; i++)
	{
		m_Settings.TimeMs[i]			= metadata->Time;
		m_Settings.GainDb[i]			= (f32)(metadata->Gain) / 100.0f;
		m_Settings.FeedbackGainDb[i]	= (f32)(metadata->FeedbackGain) / 100.0f;

		const u32 delaySamplesUnaligned	= m_Settings.TimeMs[i] * SAMPLES_PER_MS;
		//Align the sample length of the delay to an integer multiple of the XAudio frame size.
		m_DelaySamples[i]				= DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)delaySamplesUnaligned / (f32)DELAY_ALIGNMENT_SAMPLES);
		m_GainLin[i]					= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.GainDb[i]);
		m_FeedbackGainLin[i]			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.FeedbackGainDb[i]);
	}

    return true;
}

void audDelayEffectXenon::DoProcess(const tDelayEffectSettings& UNUSED_PARAM(params), f32* __restrict pData, u32 numFrames, u32 numChannels)
{   
#ifdef AUDIO_BENCHMARK_EFFECTS
	g_DelayTimer.Start();
#endif // AUDIO_BENCHMARK_EFFECTS

   	u32 channelIndex = 0;
    for(u32 inputChannelIndex=0; inputChannelIndex<numChannels; inputChannelIndex++)
    {
		if(m_ChannelMask & (1 << inputChannelIndex))
		{
			//We need to process this input channel.

			if(m_DelaySamples[inputChannelIndex] >= numFrames) //Ensure that the delay time is long enough to be valid (otherwise bypass.)
			{
				f32 *samples = &(pData[inputChannelIndex * numFrames]);
				f32 *delayBuffer = &(m_DelayBuffer[channelIndex * m_MaxDelaySamples]);
				u32 delayBufferReadOffset = (m_MaxDelaySamples + m_DelayBufferWriteOffset - m_DelaySamples[inputChannelIndex]) % m_MaxDelaySamples;
				f32 *delayReadBuffer = delayBuffer + delayBufferReadOffset;
				f32 *delayWriteBuffer = delayBuffer + m_DelayBufferWriteOffset;

				f32 gainLin = m_GainLin[inputChannelIndex];
				f32 feedbackGainLin = m_FeedbackGainLin[inputChannelIndex];

				for(u32 sampleIndex=0; sampleIndex<numFrames; sampleIndex++)
				{
					f32 sample = samples[sampleIndex];
					samples[sampleIndex] = /*sample + */(delayReadBuffer[sampleIndex] * gainLin); //Uncomment to include direct/dry signal in output.
					delayWriteBuffer[sampleIndex] = sample + (samples[sampleIndex] * feedbackGainLin);
				}
			}

			channelIndex++;
		}

    } //inputChannelIndex

	//Update ring buffer write offset.
    m_DelayBufferWriteOffset = (m_DelayBufferWriteOffset + numFrames) % m_MaxDelaySamples;

#ifdef AUDIO_BENCHMARK_EFFECTS
	g_DelayTimer.Stop();

	if(++g_DelayFrameIndex == 25)
	{
		f32 threadPerc = g_DelayTimer.GetTimeMS() * 18.75f;
		PF_SET(DelayThreadPerc, threadPerc);
		g_DelayTimer.Reset();
		g_DelayFrameIndex = 0;
	}
#endif // AUDIO_BENCHMARK_EFFECTS
}

void audDelayEffectXenon::OnSetParameters( const tDelayEffectSettings& params)
{
    m_Settings = params;
	for(u32 i=0; i<g_MaxOutputChannels; i++)
	{
		const u32 delaySamplesUnaligned	= m_Settings.TimeMs[i] * SAMPLES_PER_MS;
		//Align the sample length of the delay to an integer multiple of the XAudio frame size.
		m_DelaySamples[i]		= DELAY_ALIGNMENT_SAMPLES * (u32)ceilf((f32)delaySamplesUnaligned / (f32)DELAY_ALIGNMENT_SAMPLES);
		m_GainLin[i]			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.GainDb[i]);
		m_FeedbackGainLin[i]	= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.FeedbackGainDb[i]);
	}
}

} // namespace rage

#endif //__XENON
