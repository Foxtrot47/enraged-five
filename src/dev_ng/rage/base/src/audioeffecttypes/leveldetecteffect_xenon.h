// 
// audioeffecttypes/leveldetecteffect_xenon.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __XENON

#ifndef AUDIOEFFECTTYPES_LEVELDETECTEFFECT_XENON_H 
#define AUDIOEFFECTTYPES_LEVELDETECTEFFECT_XENON_H 

#include "audioeffecttypes/leveldetecteffect.h"

#include "filterdefs.h"
#include "system/xtl.h"

#include "rage_xapo.h"

#define USE_VECTORIZED_LEVELDETECTION 1

namespace rage {


	struct audLevelDetectEffectXenonParams
	{
		u32 dummy0;
	};

//
// PURPOSE
//  Xenon implementation of the channel meter
//
class __declspec(uuid("{122D3769-952B-499b-A0C3-4FBB44C34A24}"))
audLevelDetectEffectXenon : public audXAPOBase<audLevelDetectEffectXenon,audLevelDetectEffectXenonParams>
{
public:
	audLevelDetectEffectXenon();
	virtual bool Init(const Effect *metadata);

	void GetParameters(void *pParameters, UINT32 parameterSizeBytes);

private:

	void DoProcess(const audLevelDetectEffectXenonParams& params, f32* __restrict pData, u32 numFrames, u32 numChannels);

private:
	//	This begins a 16 byte boundary alignment
#if USE_VECTORIZED_LEVELDETECTION
	__vector4 m_MaxLevels[NUM_PLATFORM_SPEAKER_IDS];
	__vector4 m_RmsSquaredSumHistory[NUM_PLATFORM_SPEAKER_IDS];
#else
	f32 m_MaxLevels[NUM_PLATFORM_SPEAKER_IDS];
	f32 m_RmsSquaredSumHistory[NUM_PLATFORM_SPEAKER_IDS];
#endif

	u32 m_RefCount;
	void *m_Context;
	
	f32 m_RmsLevels[NUM_PLATFORM_SPEAKER_IDS];
	
	u32 m_NumChannelsToProcess;
	u8 m_RmsHistoryIndex;	
	u8 m_ChannelMask;

	u8 m_ChannelProcessOrder[NUM_PLATFORM_SPEAKER_IDS];
};


} // namespace rage

#endif // AUDIOEFFECTTYPES_LEVELDETECTEFFECT_XENON_H

#endif // __XENON
