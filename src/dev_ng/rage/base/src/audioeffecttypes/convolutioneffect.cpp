// 
// audioeffecttypes/convolutioneffect.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "convolutioneffect.h"

#include "convolutioneffect_xenon.h"
#include "convolutioneffect_pc.h"
//Add other platform-specific headers here.

#include "audiohardware/driver.h"
#include "audiohardware/driver_xenon.h"
#include "audiohardware/driver_psn.h"

#if __PPU
#include <cell/mstream.h>
extern char _binary_convolutioneffect_psn_dspfx_elf_start[];
#endif

namespace rage {

#if __XENON || __PPU
	int audConvolutionEffect::sm_PhysicalEffectId = (__XENON) ? 0 : -1;
#endif // __XENON || __PPU

audConvolutionEffect::audConvolutionEffect()
{
#if __WIN32PC
	// TODO: sort out the allocation for this if it truly has to be dynamic
	//m_ConvolutionEffectPc = rage_new audConvolutionEffectPC();
#endif

}

audConvolutionEffect::~audConvolutionEffect()
{
#if __WIN32PC
//	delete m_ConvolutionEffectPc;
#endif
}

bool audConvolutionEffect::InitClass(void)
{
#if __PS3
	//Assert(sm_PhysicalEffectId == CELL_MS_DSPOFF);
	CellMSDSP dspInfo;
	if (!audDriverPsn::InitDspEffect(_binary_convolutioneffect_psn_dspfx_elf_start, dspInfo))
	{
		return false;
	}
	sm_PhysicalEffectId = dspInfo.handle;
	Displayf("[Audio] Convolution filter init: mem=0x%x, avail=0x%x", dspInfo.memoryUsed, dspInfo.memoryAvail);
	return true;
#else
	//Add other platform-specific driver init calls here.
	return true;
#endif
}

// PURPOSE
//	Initialises an instance of the specific effect type.
bool audConvolutionEffect::Init(const void *pMetadata, u32 effectChainIndex)
{
	if (!audEffect::Init(pMetadata, effectChainIndex))
	{
		return false;
	}

	return true;
}

void* audConvolutionEffect::GetEffectSettings()
{
	return NULL;
}

void audConvolutionEffect::CommitSettings()
{
	audEffect::CommitSettings();

	// If we have a child, call CommitSettings on that effect too
	if (m_NextEffect)
	{
		m_NextEffect->CommitSettings();
	}
}

} // namespace rage
