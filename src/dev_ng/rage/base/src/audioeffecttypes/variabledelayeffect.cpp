// 
// audioeffecttypes/variabledelayeffect.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "variabledelayeffect.h"
#include "system/memops.h"

#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/src_linear.h"
#include "audiosynth/synthutil.h"
#include "biquadfiltereffect.h"

#include "math/amath.h"
#include "system/cache.h"
#include "vectormath/vectormath.h"

#define ALIGN(x, a)	(((x) + ((a) - 1)) & ~((a) - 1))

namespace rage 
{

using namespace Vec;

audVariableDelayEffect::audVariableDelayEffect() : audDspEffect(AUD_DSPEFFECT_VARIABLEDELAY)
{
	m_DelayBufferWriteOffset = 0;
	m_Bypass = false;
	m_StepRateSmoother = 1.0f;
	m_TimeStep = 0.0f;

	const s32 delaySamples = 0;

	for(u32 loop = 0; loop < g_MaxOutputChannels; loop++)
	{
		// Default delay settings
		m_DelaySettings[loop].bypass = false;
		m_DelaySettings[loop].invertPhase = false;
		m_DelaySettings[loop].readOffset = 0.0f;
		m_DelaySettings[loop].delaySamples = delaySamples;
		m_DelaySettings[loop].prevDelaySamples = delaySamples;
		m_DelaySettings[loop].stepRateLastFrame = 1.0f;

		// Default filter settings
		m_FilterSettings[loop].enabled = false;
		m_FilterSettings[loop].mode = synthBiquadFilter::LowPass2Pole;
		m_FilterSettings[loop].frequency = BIQUAD_FREQ_DEFAULT;
		m_FilterSettings[loop].resonance = audDriverUtil::ComputeLinearVolumeFromDb(BIQUAD_RESONANCE_DEFAULT - 3.01f);
		m_FilterSettings[loop].m_a_0 = 1.0f;
		m_FilterSettings[loop].m_a_1 = 0.0f;
		m_FilterSettings[loop].m_a_2 = 0.0f;
		m_FilterSettings[loop].m_b_1 = 0.0f;
		m_FilterSettings[loop].m_b_2 = 0.0f;
		m_FilterSettings[loop].m_State = V4VConstant(V_ZERO);
	}
	
	m_DelayBufferWriteOffset = delaySamples;
	m_MaxDelaySamples = kMixBufNumSamples * (u32)ceilf((f32)DELAY_MAX_SAMPLES_UNALIGNED / (f32)kMixBufNumSamples);
	Assert((m_MaxDelaySamples&15) == 0);
	m_DelayBuffer = NULL;
	m_PrintedSpareScratch = false;
}

bool audVariableDelayEffect::Init(const u32 numChannels, const u32 channelMask)
{
	m_NumInputChannels = (u8)numChannels;
	m_ChannelMask = (u8)channelMask;
	m_DelayBuffer = rage_aligned_new(16) delayBufferSample[m_MaxDelaySamples];
	sysMemSet(m_DelayBuffer, 0, m_MaxDelaySamples * sizeof(delayBufferSample));
	return true;
}

AUD_DEFINE_DSP_PROCESS_FUNCTION(audVariableDelayEffect)
{
	if(m_Bypass)
		return;

	static const u32 sampleGathererBufferLength = ALIGN(kMixBufNumSamples * kMaxSampleRateRatio + 2, 128);

#if __SPU
	delayBufferSample* sampleGathererBuffer = (delayBufferSample*)scratchSpace;
	scratchSpace = scratchSpace + (sampleGathererBufferLength * sizeof(delayBufferSample));

	delayBufferSample *delayBufferPtr = (delayBufferSample*)scratchSpace;
	const u32 delayBufferSize = m_MaxDelaySamples * sizeof(delayBufferSample);
	if(scratchSpaceSize - sampleGathererBufferLength < delayBufferSize)
	{
		if(!m_PrintedSpareScratch)
		{
			m_PrintedSpareScratch = true;
			audDisplayf("Variable delay effect disabled, not enough scratch space (%u available, %u required)", scratchSpaceSize - sampleGathererBufferLength, delayBufferSize);
		}
		return;
	}

	if(!m_PrintedSpareScratch)
	{
		m_PrintedSpareScratch = true;
		audDisplayf("Variable delay effect scratch space: %u available, %u required", scratchSpaceSize - sampleGathererBufferLength, delayBufferSize);
	}

	// TODO: For now grab the whole delay buffer; will change this to grab only the required data for this frame
	sysDmaLargeGetAndWait(delayBufferPtr, (uint64_t)m_DelayBuffer, delayBufferSize, 8);

#else
	delayBufferSample* sampleGathererBuffer = AllocaAligned(delayBufferSample, sampleGathererBufferLength, 128);
	delayBufferSample *delayBufferPtr = m_DelayBuffer;
#endif

	f32 stepRate[g_MaxOutputChannels];
	
	for(u32 channel = 0; channel < buffer.NumChannels; channel++)
	{
		stepRate[channel] = 1.0f;
	}

	// Work out our step rate for the frame. This is just based on the rate of change of delay
	if(m_TimeStep > 0.0f)
	{
		static const f32 buffersPerSec = (f32)kMixerNativeSampleRate / (f32)kMixBufNumSamples;

		for(u32 channel = 0; channel < buffer.NumChannels; channel++)
		{
			// Round up the buffers per frame to smooth out the pitch change slightly. Helps deal with slight framerate variances
			// and ensures that we don't reach our target pitch too soon (thereby having a frame or two at a static pitch)
			f32 buffersPerFrame = ceilf(buffersPerSec * m_TimeStep);
			f32 delayChangePerFrame = m_DelaySettings[channel].delaySamples - m_DelaySettings[channel].prevDelaySamples;
			f32 delayChangePerBuffer = Clamp(delayChangePerFrame/buffersPerFrame, (f32)-kMixBufNumSamples, (f32)kMixBufNumSamples);

			// Make sure we don't overshoot the target delay
			f32 delayChangeRemaining = m_DelaySettings[channel].delaySamples - GetCurrentDelaySamples(channel);
			delayChangePerBuffer = FPIfGtZeroThenElse(delayChangePerBuffer, Min(delayChangeRemaining, delayChangePerBuffer), Max(delayChangeRemaining, delayChangePerBuffer));
			f32 samplesRequired = kMixBufNumSamples + delayChangePerBuffer;

			// Clamp the step rate to a sensible value and apply any smoothing required
			stepRate[channel] = Clamp((f32)(kMixBufNumSamples /samplesRequired), 0.0f, (f32)kMaxSampleRateRatio);
			stepRate[channel] = (m_StepRateSmoother * stepRate[channel]) + ((1.0f - m_StepRateSmoother) * m_DelaySettings[channel].stepRateLastFrame);
		}
	}

	if (buffer.ChannelBufferIds[0] < audFrameBufferPool::InvalidId)
	{
		// Write incoming data to delay line
		CopyBufferToDelayLine(buffer.ChannelBuffers[0], delayBufferPtr);
	}
	else
	{
		if (delayBufferPtr)
		{
			sysMemSet(delayBufferPtr + m_DelayBufferWriteOffset, 0, kMixBufNumSamples); // Is this correct?????
		}
	}
	m_DelayBufferWriteOffset = (m_DelayBufferWriteOffset + kMixBufNumSamples) % m_MaxDelaySamples;

	// Grab the delayed data from the ringbuffer that we're going to be playing back
	for(u32 channel = 0; channel < buffer.NumChannels; channel++)
	{
		if(!m_DelaySettings[channel].bypass  && buffer.ChannelBufferIds[channel] < audFrameBufferPool::InvalidId)
		{
			u32 currentReadPoint = (u32)m_DelaySettings[channel].readOffset;
			f32 trailingFraction = (f32)(m_DelaySettings[channel].readOffset - (u32)(m_DelaySettings[channel].readOffset));
			const u32 inputSamplesRequired = 2 + (u32)((kMixBufNumSamples - 1) * stepRate[channel] + trailingFraction);

			u32 size1 = Min((u32)inputSamplesRequired, m_MaxDelaySamples - currentReadPoint) * sizeof(delayBufferSample);
			void* ptr1 = (void*) &delayBufferPtr[currentReadPoint];

			u32 size2 = 0;
			void* ptr2 = NULL;

			// If we didn't grab everything, wrap around and start reading from the front of the buffer
			if(size1 < inputSamplesRequired * sizeof(delayBufferSample))
			{
				ptr2 = delayBufferPtr;
				size2 = (sizeof(delayBufferSample) * inputSamplesRequired) - size1;
			}

			sysMemCpy(sampleGathererBuffer, ptr1, size1);

			if(ptr2 > 0)
			{
				sysMemCpy(((char*)sampleGathererBuffer + size1), ptr2, size2);
			}

			if(stepRate[channel] > 0.0f)
			{
				// Pitch the delayed data to the correct speed
				u32 samplesGenerated = audRateConvertorLinear::Process(stepRate[channel], buffer.ChannelBuffers[channel], sampleGathererBuffer, 1024u, kMixBufNumSamples, trailingFraction);
				Assert(samplesGenerated == kMixBufNumSamples);
				m_DelaySettings[channel].readOffset += stepRate[channel] * samplesGenerated;
			}

			// Advance delay buffer read point
			if(m_DelaySettings[channel].readOffset >= m_MaxDelaySamples)
			{
				m_DelaySettings[channel].readOffset -= m_MaxDelaySamples;
			}

			m_DelaySettings[channel].stepRateLastFrame = stepRate[channel];

			// Process the filter, if enabled
			if(m_FilterSettings[channel].enabled)
			{
				bool is4Pole = m_FilterSettings[channel].mode >= synthBiquadFilter::kFirst4PoleMode && 
					m_FilterSettings[channel].mode != synthBiquadFilter::LowShelf2Pole && 
					m_FilterSettings[channel].mode != synthBiquadFilter::HighShelf2Pole && 
					m_FilterSettings[channel].mode != synthBiquadFilter::AllPass;

				switch(m_FilterSettings[channel].mode)
				{
				case synthBiquadFilter::LowPass2Pole:
				case synthBiquadFilter::LowPass4Pole:
					synthBiquadFilter::ComputeCoefficients_LowPass(is4Pole, m_FilterSettings[channel].frequency, m_FilterSettings[channel].resonance, m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::HighPass2Pole:
				case synthBiquadFilter::HighPass4Pole:
					synthBiquadFilter::ComputeCoefficients_HighPass(is4Pole, m_FilterSettings[channel].frequency, m_FilterSettings[channel].resonance, m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::BandPass2Pole:
				case synthBiquadFilter::BandPass4Pole:
					synthBiquadFilter::ComputeCoefficients_BandPass(m_FilterSettings[channel].frequency, m_FilterSettings[channel].bandwidth, m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::BandStop2Pole:
				case synthBiquadFilter::BandStop4Pole:
					synthBiquadFilter::ComputeCoefficients_BandStop(m_FilterSettings[channel].frequency, m_FilterSettings[channel].bandwidth,  m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::PeakingEQ:
					synthBiquadFilter::ComputeCoefficients_PeakingEQ(m_FilterSettings[channel].frequency, m_FilterSettings[channel].bandwidth, m_FilterSettings[channel].resonance, m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::LowShelf4Pole:
				case synthBiquadFilter::LowShelf2Pole:
					synthBiquadFilter::ComputeCoefficients_LowShelf(is4Pole, m_FilterSettings[channel].frequency, m_FilterSettings[channel].bandwidth, m_FilterSettings[channel].resonance, m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::HighShelf2Pole:
				case synthBiquadFilter::HighShelf4Pole:
					synthBiquadFilter::ComputeCoefficients_HighShelf(is4Pole, m_FilterSettings[channel].frequency, m_FilterSettings[channel].bandwidth, m_FilterSettings[channel].resonance, m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				case synthBiquadFilter::AllPass:
					synthBiquadFilter::ComputeCoefficients_AllPass(m_FilterSettings[channel].frequency, m_FilterSettings[channel].resonance,  m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2);
					break;

				default:
					Assert(0);
				}

				if(is4Pole)
				{
					synthBiquadFilter::Process_4Pole(buffer.ChannelBuffers[channel], m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2, m_FilterSettings[channel].m_State, kMixBufNumSamples);
				}
				else //2-pole.
				{
					synthBiquadFilter::Process_2Pole(buffer.ChannelBuffers[channel], m_FilterSettings[channel].m_a_0, m_FilterSettings[channel].m_a_1, m_FilterSettings[channel].m_a_2, m_FilterSettings[channel].m_b_1, m_FilterSettings[channel].m_b_2, m_FilterSettings[channel].m_State, kMixBufNumSamples);
				}
			}

			if(m_DelaySettings[channel].invertPhase)
			{
				synthUtil::ScaleBuffer(buffer.ChannelBuffers[channel], V4VConstant(V_NEGONE), kMixBufNumSamples);
			}
		}
		else
		{
			if (buffer.ChannelBufferIds[channel] < audFrameBufferPool::InvalidId)
			{
				sysMemSet(buffer.ChannelBuffers[channel], 0, sizeof(f32) * kMixBufNumSamples);
			}
		}
	}

#if __SPU
	// Update main memory delay buffer
	// TODO: For now grab the whole delay buffer; will change this to grab only the required data for this frame
	sysDmaLargePutAndWait(delayBufferPtr, (uint64_t)m_DelayBuffer, delayBufferSize, 8);
#endif
}

f32 audVariableDelayEffect::GetCurrentDelaySamples(const u32 channel) const
{
	f32 delaySamples = (f32)((m_DelayBufferWriteOffset % m_MaxDelaySamples) - m_DelaySettings[channel].readOffset);

	// Write pointer may have wrapped around and be behind the read pointer
	if(delaySamples < 0)
	{
		delaySamples += m_MaxDelaySamples;
	}

	return delaySamples;
}

void audVariableDelayEffect::CopyBufferToDelayLine(const f32* channelBuffer, const delayBufferSample* delayBuffer)
{
	// Must be aligned as we're always copying 256 samples and not checking going past the end of the buffer
	Assert((m_DelayBufferWriteOffset&255) == 0);
	Assert((m_MaxDelaySamples&255) == 0);
	Assert(m_DelayBufferWriteOffset < (s32)m_MaxDelaySamples);

#if RSG_CPU_INTEL && S16_DELAY_BUFFER
	const Vector_4V scalar = V4VConstant<0x47000000,0x47000000,0x47000000,0x47000000>(); // 32768
#endif

	Vector_4V *samples = (Vector_4V*)channelBuffer;
	Vector_4V *delayWriteBuffer = (Vector_4V*)(delayBuffer + m_DelayBufferWriteOffset);

#if __XENON
	PrefetchDC2(delayWriteBuffer, 0);
#endif

#if S16_DELAY_BUFFER
	// process one 128 byte cache line at a time (64 samples at 2bytes/sample)
	enum {kNumSamplesPerBlock = 64};
#else
	// process one 128 byte cache line at a time (32 samples at 4bytes/sample)
	enum {kNumSamplesPerBlock = 32};
#endif
	for(u32 blockIndex = 0; blockIndex < kMixBufNumSamples / kNumSamplesPerBlock; blockIndex++)
	{
#if __XENON
		// Prefetch the next block / cache-line
		PrefetchDC2(delayWriteBuffer, 128);
#endif

		for(u32 sampleCounter = 0; sampleCounter < kNumSamplesPerBlock; sampleCounter += 8)
		{
			// perform delay processing: output is delay line*gain + input * dry
			//input + output*feedback is written to delay line
			const Vector_4V inputSamples0 = *samples;
			samples++;

			const Vector_4V inputSamples1 = *samples;
			samples++;

			// pack into 16bit values for storage
#if S16_DELAY_BUFFER
#if RSG_CPU_INTEL
			// again, V4FloatToIntRaw is slow when exponent != 0 on PC, so scale manually
			*delayWriteBuffer++ = V4PackSignedIntToSignedShort(
				V4FloatToIntRaw<0>(V4Scale(scalar,inputSamples0)),
				V4FloatToIntRaw<0>(V4Scale(scalar, inputSamples1)));
#else
			*delayWriteBuffer++ = V4PackSignedIntToSignedShort(V4FloatToIntRaw<15>(inputSamples0),V4FloatToIntRaw<15>(inputSamples1));
#endif
#else
			// f32 delay buffer
			*delayWriteBuffer++ = inputSamples0;
			*delayWriteBuffer++ = inputSamples1;
#endif
		} // sampleIndex
	}// block index 
}

void audVariableDelayEffect::SetParam(const u32 paramId, const u32 value)
{
	if(paramId >= BypassChannel && paramId < BypassChannel + g_MaxOutputChannels)
	{
		u32 channel = paramId - BypassChannel;
		m_DelaySettings[channel].bypass = (value != 0u);
	}
	else if(paramId >= InvertPhase && paramId < InvertPhase + g_MaxOutputChannels)
	{
		u32 channel = paramId - InvertPhase;
		m_DelaySettings[channel].invertPhase = (value != 0u);
	}
	else if(paramId >= DelayInSamples && paramId < DelayInSamples + g_MaxOutputChannels)
	{
		u32 channel = paramId - DelayInSamples;
		m_DelaySettings[channel].prevDelaySamples = GetCurrentDelaySamples(channel);
		m_DelaySettings[channel].delaySamples = Clamp(value, 0u, m_MaxDelaySamples - kMixBufNumSamples);
	}
	else if(paramId >= FilterEnabled && paramId < FilterEnabled + g_MaxOutputChannels)
	{
		u32 channel = paramId - FilterEnabled;
		bool filterEnabled = value != 0u;

		if(m_FilterSettings[channel].enabled != filterEnabled)
		{
			m_FilterSettings[channel].m_State = V4VConstant(V_ZERO);
			m_FilterSettings[channel].enabled = filterEnabled;
		}
	}
	else if(paramId >= FilterMode && paramId < FilterMode + g_MaxOutputChannels)
	{
		u32 channel = paramId - FilterMode;
		synthBiquadFilter::synthBiquadFilterModes newMode = (synthBiquadFilter::synthBiquadFilterModes)value;

		if(newMode != m_FilterSettings[channel].mode)
		{
			m_FilterSettings[channel].m_State = V4VConstant(V_ZERO);
			m_FilterSettings[channel].mode = newMode;
		}
	}
	else if(paramId >= ResetDelay && paramId <= ResetDelay + g_MaxOutputChannels)
	{
		u32 channel = paramId - ResetDelay;
		m_DelaySettings[channel].prevDelaySamples = (f32) m_DelaySettings[channel].delaySamples;
		m_DelaySettings[channel].readOffset = (s32)m_DelayBufferWriteOffset - m_DelaySettings[channel].delaySamples;
		m_DelaySettings[channel].stepRateLastFrame = 1.0f;
		m_FilterSettings[channel].m_State = V4VConstant(V_ZERO);

		if(m_DelaySettings[channel].readOffset < 0)
		{
			m_DelaySettings[channel].readOffset += m_MaxDelaySamples;
		}
	}
	else if(paramId == Bypass)
	{
		m_Bypass = (value != 0);
	}
	else
	{
		audAssertf(false, "paramId %u is not a valid audVariableDelayEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
	}
}

void audVariableDelayEffect::SetParam(const u32 paramId, const f32 value)
{
	if(paramId >= FilterFrequency && paramId < FilterFrequency + g_MaxOutputChannels)
	{
		u32 channel = paramId - FilterFrequency;
		m_FilterSettings[channel].frequency = value;
	}
	else if(paramId >= FilterResonance && paramId < FilterResonance + g_MaxOutputChannels)
	{
		u32 channel = paramId - FilterResonance;
		m_FilterSettings[channel].resonance = value;
	}
	else if(paramId >= FilterBandwidth && paramId < FilterBandwidth + g_MaxOutputChannels)
	{
		u32 channel = paramId - FilterBandwidth;
		m_FilterSettings[channel].bandwidth = value;
	}
	else if(paramId == TimeStep)
	{
		m_TimeStep = value;
	}
	else if(paramId == StepRateSmoothing)
	{
		m_StepRateSmoother = value;
	}
	else
	{
		audAssertf(false, "paramId %u is not a valid audVariableDelayEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
	}
}

void audVariableDelayEffect::Shutdown(void)
{
	if(m_DelayBuffer)
	{
		delete[] m_DelayBuffer;
		m_DelayBuffer = NULL;
	}
}

} // namespace rage

