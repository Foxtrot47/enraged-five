// 
// audioeffecttypes/reverbeffect_prog.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 


#ifndef __REVERBEFFECTPROG_H
#define __REVERBEFFECTPROG_H

#include "audiosynth/synthdefs.h"

#if __SYNTH_EDITOR

#include "filterdefs.h"
#include "revmodelprog.h"
#include "audiohardware/dspeffect.h"
#include "vectormath/vectormath.h"

namespace rage 
{


#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to alignment
#endif


	class ALIGNAS(16) audReverbEffectProgenitor : public audDspEffect
	{
	public:
		audReverbEffectProgenitor();
		virtual ~audReverbEffectProgenitor();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();

		AUD_DECLARE_DSP_PROCESS_FUNCTION;


		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audReverbProgEffectParams
		{
			Diffusion1 = audEffectParamIds::ReverbEffect_Prog,

			Diffusion2,
			DecayDiffusion,
			Decay1,
			Decay2,
			Decay3,
			HF_Bandwidth,
			Definition,

			ChorusDry,
			ChorusWet,

			Damping,

			Bypass,
		};

	private:
		
		revModelProgenitor_V4 m_Model;

		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;
		
		u8 m_Bypass : 1;
		
	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage
#endif // __SYNTH_EDITOR
#endif //__REVERBEFFECT_H

