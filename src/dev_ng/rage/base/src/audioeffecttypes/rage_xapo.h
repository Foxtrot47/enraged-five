// based heavily on Microsoft's ATG CSampleXAPOBase (ATGAPOBase.h)

#if __XENON

#ifndef __RAGE_XAPO_H
#define __RAGE_XAPO_H

#include "system/xtl.h"
#define XAPODEBUG 0
#include <xapobase.h>

namespace rage
{
	struct Effect;

	//--------------------------------------------------------------------------------------
	// audXAPOBase
	//
	// Template class that provides a default class factory implementation and typesafe
	// parameter passing for our sample xAPO classes
	//--------------------------------------------------------------------------------------
	template<typename APOClass, typename ParameterClass>
	class audXAPOBase
		: public CXAPOParametersBase
	{
	public:
		static HRESULT CreateInstance(APOClass** ppAPO );

		//
		// IXAPO
		//
		STDMETHOD(LockForProcess) (
			UINT32 InputLockedParameterCount,
			const XAPO_LOCKFORPROCESS_BUFFER_PARAMETERS *pInputLockedParameters,
			UINT32 OutputLockedParameterCount,
			const XAPO_LOCKFORPROCESS_BUFFER_PARAMETERS *pOutputLockedParameters );

		STDMETHOD_(void, Process) (
			UINT32 InputProcessParameterCount,
			const XAPO_PROCESS_BUFFER_PARAMETERS *pInputProcessParameters,
			UINT32 OutputProcessParameterCount,
			XAPO_PROCESS_BUFFER_PARAMETERS *pOutputProcessParameters,
			BOOL IsEnabled);

	
			virtual bool Init() = 0;

	protected:
		audXAPOBase();
		~audXAPOBase(void);

		//
		// Accessors
		//
		const WAVEFORMATEX& WaveFormat(){ return m_Wfx; }

		//
		// Overrides
		//
		void OnSetParameters (const void* pParams, UINT32 cbParams)
		{
			Assert( cbParams == sizeof( ParameterClass ) );
			cbParams;
			OnSetParameters( *(ParameterClass*)pParams );
		}

		//
		// Overridables
		//

		// Process a frame of audio. Marked pure virtual because without
		// this function there's not much point in having an xAPO.
		virtual void DoProcess(const ParameterClass& params, f32* __restrict pData,	u32 numFrames, u32 numChannels) = 0;

		// Do any necessary calculations in response to parameter changes.
		// NOT marked pure virtual because there may not be a reason to
		// do additional calculations when parameters are set.
		virtual void OnSetParameters( const ParameterClass& ){}

	private:
		// Ring buffer needed by CXAPOParametersBase
		ParameterClass  m_Parameters[3];

		// Format of the audio we're processing
		WAVEFORMATEX    m_Wfx;

		// Registration properties defining this xAPO class.
		static XAPO_REGISTRATION_PROPERTIES sm_RegProps;

	};

	//--------------------------------------------------------------------------------------
	// audXAPOBase::sm_RegProps
	//
	// Registration properties for the sample xAPO classes.
	//--------------------------------------------------------------------------------------
	template<typename APOClass, typename ParameterClass>
	__declspec(selectany) XAPO_REGISTRATION_PROPERTIES audXAPOBase<APOClass, ParameterClass>::sm_RegProps = {
		__uuidof(APOClass),
		L"RAGE Audio Effect",
		L"Copyright (C)2012 Rockstar Games",
		1,
		0,
		XAPO_FLAG_INPLACE_REQUIRED
		| XAPO_FLAG_CHANNELS_MUST_MATCH
		| XAPO_FLAG_FRAMERATE_MUST_MATCH
		| XAPO_FLAG_BITSPERSAMPLE_MUST_MATCH
		| XAPO_FLAG_BUFFERCOUNT_MUST_MATCH
		| XAPO_FLAG_INPLACE_SUPPORTED,
		1, 1, 1, 1 };

		//--------------------------------------------------------------------------------------
		// Name: audXAPOBase::CreateInstance
		// Desc: Class factory for sample xAPO objects
		//--------------------------------------------------------------------------------------
		template<typename APOClass, typename ParameterClass>
		HRESULT audXAPOBase<APOClass, ParameterClass>::CreateInstance( APOClass** ppAPO )
		{
			Assert( ppAPO );
			HRESULT hr = S_OK;

			*ppAPO = rage_new APOClass;

			if(!(*ppAPO)->Init())
			{
				hr = E_INVALIDARG;
			}
			return hr;
		}

		//--------------------------------------------------------------------------------------
		// Name: audXAPOBase::CSampleXAPOBase
		// Desc: Constructor
		//--------------------------------------------------------------------------------------
		template<typename APOClass, typename ParameterClass>
		audXAPOBase<APOClass, ParameterClass>::audXAPOBase( )
			: CXAPOParametersBase( &sm_RegProps, (BYTE*)m_Parameters, sizeof( ParameterClass ), FALSE )
		{
#ifdef _XBOX_VER
			XMemSet( (BYTE*)m_Parameters, 0, sizeof( m_Parameters ) );
#else
			ZeroMemory( m_Parameters, sizeof( m_Parameters ) );
#endif
		}


		//--------------------------------------------------------------------------------------
		// Name: audXAPOBase::~CSampleXAPOBase
		// Desc: Destructor
		//--------------------------------------------------------------------------------------
		template<typename APOClass, typename ParameterClass>
		audXAPOBase<APOClass, ParameterClass>::~audXAPOBase()
		{

		}

		//--------------------------------------------------------------------------------------
		// Name: audXAPOBase::LockForProcess
		// Desc: Overridden so that we can remember the wave format of the signal
		//       we're supposed to be processing
		//--------------------------------------------------------------------------------------
		template<typename APOClass, typename ParameterClass>
		HRESULT audXAPOBase<APOClass, ParameterClass>::LockForProcess(
			UINT32 InputLockedParameterCount,
			const XAPO_LOCKFORPROCESS_BUFFER_PARAMETERS *pInputLockedParameters,
			UINT32 OutputLockedParameterCount,
			const XAPO_LOCKFORPROCESS_BUFFER_PARAMETERS *pOutputLockedParameters )
		{
			HRESULT hr = CXAPOParametersBase::LockForProcess(
				InputLockedParameterCount,
				pInputLockedParameters,
				OutputLockedParameterCount,
				pOutputLockedParameters );

			if( SUCCEEDED( hr ) )
			{
				// Copy the wave format. Note that we're using memcpy rather than XMemCpy
				// because XMemCpy is really only beneficial for larger blocks of memory.
				memcpy( &m_Wfx, pInputLockedParameters[0].pFormat, sizeof( WAVEFORMATEX ) );
			}
			return hr;
		}

		BANK_ONLY(extern bool g_DisableXAPOProcessing);

		//--------------------------------------------------------------------------------------
		// Name: audXAPOBase::Process
		// Desc: Overridden to call this class's typesafe version
		//--------------------------------------------------------------------------------------
		template<typename APOClass, typename ParameterClass>
		void audXAPOBase<APOClass, ParameterClass>::Process(
			UINT32 ASSERT_ONLY(InputProcessParameterCount),
			const XAPO_PROCESS_BUFFER_PARAMETERS *pInputProcessParameters,
			UINT32 ASSERT_ONLY(OutputProcessParameterCount),
			XAPO_PROCESS_BUFFER_PARAMETERS *ASSERT_ONLY(pOutputProcessParameters),
			BOOL UNUSED_PARAM(IsEnabled))
		{
			Assert( IsLocked() );
			Assert( InputProcessParameterCount == 1 );
			Assert( OutputProcessParameterCount == 1 );
			Assert( pInputProcessParameters[0].pBuffer == pOutputProcessParameters[0].pBuffer );

			ParameterClass* pParams;
			pParams = (ParameterClass*)BeginProcess();

			BANK_ONLY(if(!g_DisableXAPOProcessing))
			{
				if ( pInputProcessParameters[0].BufferFlags == XAPO_BUFFER_SILENT )
				{
					memset( pInputProcessParameters[0].pBuffer, 0,
						pInputProcessParameters[0].ValidFrameCount * m_Wfx.nChannels * sizeof(FLOAT32) );

					DoProcess(
						*pParams,
						(FLOAT32* __restrict)pInputProcessParameters[0].pBuffer,
						pInputProcessParameters[0].ValidFrameCount,
						m_Wfx.nChannels );
				}
				else if( pInputProcessParameters[0].BufferFlags == XAPO_BUFFER_VALID )
				{
					DoProcess(
						*pParams,
						(FLOAT32* __restrict)pInputProcessParameters[0].pBuffer,
						pInputProcessParameters[0].ValidFrameCount,
						m_Wfx.nChannels );
				}
			}

			EndProcess();

		}

} // namespace rage

#endif // __RAGE_XAPO_H
#endif // __XENON
