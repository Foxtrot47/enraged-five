#if __SPU
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "psn_dsp.h"
#include "filterdefs.h"
#include "system/memops.h"
#include "vector/vector3_consts_spu.cpp"
#include "audiohardware/driverutil.h"

#include "system/spu_library.cpp"

#include <cell/dma.h>

#define TRUE		(1)
#define FALSE		(0)
#define EPISILON	(0.05f)
#define TAG			(7)

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// PURPOSE
//   This function checks to see if any of the core parameters in the
//   compressor settings have changed.  If so, we'll need to recalc some
//   constants prior to applying the filter to the input signal.
int IsCompressorParamsChanged(const tCompressorFilterSPUParamData *next, const tCompressorFilterSPUParamData *previous)
{
	float high, low;

	const tCompressorEffectSettings &nSettings = next->m_Settings;
	const tCompressorEffectSettings &pSettings = previous->m_Settings;

	// ratio
	high = pSettings.Ratio + EPISILON; low = pSettings.Ratio - EPISILON;
	if (nSettings.Ratio > high || nSettings.Ratio < low)
	{
		return TRUE;
	}

	// attack
	high = pSettings.Attack + EPISILON; low = pSettings.Attack - EPISILON;
	if (nSettings.Attack > high || nSettings.Attack < low)
	{
		return TRUE;
	}

	// average
	high = pSettings.Average + EPISILON; low = pSettings.Average - EPISILON;
	if (nSettings.Average > high || nSettings.Average < low)
	{
		return TRUE;
	}

	// gainDB
	high = pSettings.GainDb + EPISILON; low = pSettings.GainDb - EPISILON;
	if (nSettings.GainDb > high || nSettings.GainDb < low)
	{
		return TRUE;
	}

	// release
	high = pSettings.Release + EPISILON; low = pSettings.Release - EPISILON;
	if (nSettings.Release > high || nSettings.Release < low)
	{
		return TRUE;
	}

	// threshold
	high = pSettings.ThresholdDb + EPISILON; low = pSettings.ThresholdDb - EPISILON;
	if (nSettings.ThresholdDb > high || nSettings.ThresholdDb < low)
	{
		return TRUE;
	}

	if(pSettings.PeakLimiterMode != nSettings.PeakLimiterMode)
	{
		return TRUE;
	}

	return FALSE;
}

//
u32 GetChannelIndex(const u8 msMask)
{
	u8 flag = 1;
	for (u32 i=0; i<g_MaxOutputChannels; ++i)
	{
		if (msMask & flag)
		{
			return i;
		}
		flag <<= 1;
	}

	return ~0U;
}

void RecalculateFilterSettings(tCompressorEffectSettings *settings, tCompressorFilterSPUData *data)
{
	//Apply min and max limits to compressor parameters.
	settings->ThresholdDb	= Clamp(settings->ThresholdDb, COMPRESSOR_THRESH_MIN, COMPRESSOR_THRESH_MAX);
	settings->Attack		= Clamp(settings->Attack, COMPRESSOR_ATTACK_MIN, COMPRESSOR_ATTACK_MAX);
	settings->Average		= Clamp(settings->Average, COMPRESSOR_AVG_MIN, COMPRESSOR_AVG_MAX);
	settings->GainDb		= Clamp(settings->GainDb, COMPRESSOR_GAIN_MIN,	COMPRESSOR_GAIN_MAX);
	settings->Ratio			= Clamp(settings->Ratio, COMPRESSOR_RATIO_MIN, COMPRESSOR_RATIO_MAX);
	settings->Release		= Clamp(settings->Release, COMPRESSOR_RELEASE_MIN, COMPRESSOR_RELEASE_MAX);

	//Do some dB-->linear conversions
	data->m_ThresholdLin = audDriverUtil::ComputeLinearVolumeFromDb(settings->ThresholdDb);
	data->m_GainLin		 = audDriverUtil::ComputeLinearVolumeFromDb(settings->GainDb);

	data->m_IsPeakLimiter = settings->PeakLimiterMode;
	if(settings->PeakLimiterMode)
	{
		data->m_AttackTime = settings->PeakLimiterAttack;
		data->m_ReleaseTime = settings->PeakLimiterRelease;
	}
	else
	{
		// Attack and release time calculations
		data->m_AttackTime  = 1.0f - exp( -2.2f * (1.0f / NATIVE_MULTISTREAM_SAMPLE_RATE) / settings->Attack );
		data->m_ReleaseTime = 1.0f - exp( -2.2f * (1.0f / NATIVE_MULTISTREAM_SAMPLE_RATE) / settings->Release );

		// Slope calculation
		data->m_Slope = 1.0f - ( 1.0f / settings->Ratio );

		// RMS Window calculation
		f32 RMSWindow = settings->Average * NATIVE_MULTISTREAM_SAMPLE_RATE;
		data->m_RMSWindow1			= RMSWindow - 1.0f;
		data->m_OneOverRMSWindow	= 1.0f / RMSWindow;

		data->m_InverseSlopedThresholdLin = 1.0f / audDriverUtil::powf(data->m_ThresholdLin, data->m_Slope);
	}
}

/**********************************************************************************
DSPInit
Initialisation routine
This routine is called by MultiStream when the DSP effect is first called.

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
paramList		Address of static parameter data (248 bytes)
	0-127		= Read Only (example: users input parameters)
	128-247		= Read / Write (example: DSP effects internal parameters)
workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area
dspInfo			Contains information as to the slot number to be processed, input / output bitmasks

Notes:
	Each DSP effect has 248 bytes of RAM available.
	If more RAM is required, this area can be used to store a pointer to the actual data.
	The data can then be transferred from main RAM as required.
	Splitting the paramList into a read and a read/write section allows us to make sure that if
	the user is modifying parameters via PPU commands (example: distortion level), there is no conflicts
	between the SPU updating parameter memory at the same time.

**********************************************************************************/
void DSPInit(char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{
}

/**********************************************************************************
DSPApply
Apply DSP effect routine
This routine is called by MultiStream to apply a DSP effect to an input signal

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
input			Address of input data (Time Domain float32 data OR Frequency Domain FFT data)
				For PCM, 1024 samples are passed in.
				For FFT, 512 frequency bands are passed in.

output			Address to write output data to

paramList		Address of static parameter data (248 bytes)
				0-127 = Read Only
				128-247 = Read / Write

workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area

dspInfo			Contains information as to the slot number to be processed, input / output bitmasks..

Returns:
0				DSP will complete when stream finishes
!0				DSP will stay active after stream finishes (for delays / reverbs)

Notes:
	For time domain input data, the user must process all 1024 samples.
	But note that any parameters used for modifying data must be preserved after 512 samples.
	This is due to windowing within MultiStream.

**********************************************************************************/
int DSPApply(void *input, void *output, char *paramList, void *workArea, MS_DSP_INFO *dspInfo)
{
	tCompressorFilterSPUParamData *pNextParams = (tCompressorFilterSPUParamData*)paramList;
	tCompressorFilterSPUParamData *pLastParams = (tCompressorFilterSPUParamData*)(paramList+128);

	//Ensure that all previous effect DMAs have completed before we start.
	cellDmaWaitTagStatusAll(1 << TAG);

	//////////////////////////////////////////////////////////////////////////
	// dma across the work data
	uint64_t ea = (uint64_t)pNextParams->EA;
	if(ea == 0)
	{
		//This effect has not yet been initialized on the PPU, so early-out.
		return 0;
	}

	cellDmaGet(workArea, ea, sizeof(tCompressorFilterSPUData), TAG, 0, 0);
	cellDmaWaitTagStatusAll(1<<TAG);
	dspInfo->bandwidthIn += sizeof(tCompressorFilterSPUData);
	tCompressorFilterSPUData *pWorkData = (tCompressorFilterSPUData*)workArea;

	//////////////////////////////////////////////////////////////////////////
	// only perform DSP if our metadata masks match

	// - Actually set signalIndex to the input channel index directly, as channel linkage is currently broken.
	const u32 signalIndex = GetChannelIndex(dspInfo->inMask);

	if ( (dspInfo->callNumber == -1) ||  !(dspInfo->inMask & pWorkData->m_ChannelMask) || signalIndex >= g_MaxOutputChannels)
	{
		//Copy input buffer into output buffer - bypass mode.
		for(u32 cnt = 0; cnt<FRAME_SAMPLES; cnt++)
		{
			((f32 *)output)[cnt] = ((f32 *)input)[cnt];
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	// have our parameters changed?
	if (IsCompressorParamsChanged(pNextParams, pLastParams) == TRUE)
	{
		*pLastParams = *pNextParams;

		RecalculateFilterSettings(&pLastParams->m_Settings, pWorkData);
	}

	//Displayf("signalIndex: %u 0x%x [0x%x] %u", signalIndex, dspInfo->inMask, pWorkData->m_ChannelMask, dspInfo->callNumber);

	if(pWorkData->m_IsPeakLimiter)
	{
		const f32 attack = pWorkData->m_AttackTime;
		const f32 release = pWorkData->m_ReleaseTime;
		const f32 thresh = pWorkData->m_ThresholdLin;
		const f32 gainLin = pWorkData->m_GainLin;

		f32 lastEnv = pWorkData->m_LastEnvelope[signalIndex];
		f32 *inputData = (f32*)input;
		f32 *outputData = (f32*)output;
				
		for(u32 curSampleIndex = 0; curSampleIndex < FRAME_SAMPLES; curSampleIndex++)
		{
			const f32 delayedSample = (curSampleIndex < g_PeakLimiterDelayLength?pWorkData->m_DelayBuffer[signalIndex][curSampleIndex]:inputData[curSampleIndex-g_PeakLimiterDelayLength]);
			const f32 currentSample = inputData[curSampleIndex];

			const f32 x = Abs(currentSample);
			const f32 a = Selectf(x - lastEnv, attack, release);
			//const f32 a = (x>m_LastEnvelope[inputChannelIndex]?attack:release);

			// max filter
			f32 maxX = x;
			// get the max sample from [curSampleIndex - g_PeakLimiterDelayLength] to [curSampleIndex]
			u32 numSamplesCounted = 0;
			for(u32 i = curSampleIndex; i < g_PeakLimiterDelayLength; i++)
			{
				maxX = Max(maxX, Abs(pWorkData->m_DelayBuffer[signalIndex][i]));
				numSamplesCounted++;
			}
			for(; numSamplesCounted < g_PeakLimiterDelayLength; numSamplesCounted++)
			{
				maxX = Max(maxX, Abs(inputData[numSamplesCounted + curSampleIndex - g_PeakLimiterDelayLength]));
			}

			const f32 env = (a * maxX) + ((1.f-a)*lastEnv);
			// prevent divide by zero: clamp env to 1.f if env is super small, since it will be ignored in the next fsel anyway (being smaller
			// than gain)
			const f32 safeEnv = Selectf(env - 0.0001f, env, 1.f);
			const f32 gain = Selectf(thresh - env, 1.0f, Min(1.0f, thresh/safeEnv));
			lastEnv = env;
			outputData[curSampleIndex] = gain * delayedSample * gainLin;
		}
		pWorkData->m_LastEnvelope[signalIndex] = lastEnv;

		// populate delay line with last N samples from this buffer
		sysMemCpy(&pWorkData->m_DelayBuffer[signalIndex], &inputData[FRAME_SAMPLES-g_PeakLimiterDelayLength], g_PeakLimiterDelayLength*sizeof(f32));
	}
	else
	{
		f32 xdn;
		f32 a;
		f32 fn;
		f32 gn;
		f32 *samples = (f32*)input;

		// Create output data if DSP effect is set as Time Domain
		for(u32 cnt = 0; cnt < FRAME_SAMPLES; cnt++)
		{
			//----------------------------------------------------------------------
			// Level detection
			//----------------------------------------------------------------------

			// Running Average: (((n-1) * average) + newSample) / n
			xdn  = ((pWorkData->m_RMSWindow1 * pWorkData->m_xdn1[signalIndex]) + Abs(*samples)) * pWorkData->m_OneOverRMSWindow;
			pWorkData->m_xdn1[signalIndex] = xdn;

			xdn = Max(xdn, 1.0E-14f);
			fn = Max(pWorkData->m_InverseSlopedThresholdLin * audDriverUtil::powf(xdn, pWorkData->m_Slope), 1.0f); //Don't expand.

			//----------------------------------------------------------------------
			// Attack / release time adjustment
			//----------------------------------------------------------------------

			a = audDriverUtil::fsel(fn - pWorkData->m_fn1[signalIndex], pWorkData->m_AttackTime, pWorkData->m_ReleaseTime);
			gn = ((fn - pWorkData->m_gn1[signalIndex]) * a) + pWorkData->m_gn1[signalIndex];

			//----------------------------------------------------------------------
			// Adjust the signal
			//----------------------------------------------------------------------

			// Output
			*samples *= (pWorkData->m_GainLin / gn);
			((float*)output)[cnt] = *samples++;

			// Save samples
			pWorkData->m_fn1[signalIndex] = fn;
			pWorkData->m_gn1[signalIndex] = gn;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// restore the work data for the next pass
	cellDmaPut(pWorkData, ea, sizeof(tCompressorFilterSPUData), TAG, 0, 0);
	//cellDmaWaitTagStatusAll(1 << TAG);
	dspInfo->bandwidthOut += sizeof(tCompressorFilterSPUData);

	// If the stream ends and (and if this DSP effect is attached to a stream), end the DSP effect too..
	return 0;
}

/**********************************************************************************
DSPPlugInSetup
Setup as the entry point function within the DSP makefile
Initialises the DSP effect functions.
Called by MultiStream.

Requires:
plugInStruct		Address of a DSPPlugin structure

Notes:
This function sets the address of both the Init and the Apply functions.
It also specifies if the input / output data is required as time domain or FFT data
**********************************************************************************/
void DSPPlugInSetup(DSPPlugin* plugInStruct)
{
	plugInStruct->DSPInit = (ms_fnct)DSPInit;			// Init function (or NULL if no init function required)
	plugInStruct->DSPApply = (ms_fnct)DSPApply;			// DSP "Apply" function (processes DSP effect)
	plugInStruct->Domain = MS_DSP_TIMEBASE;				// DSP Effect is Frequency based (complex numbers) 
}


#endif	// __SPU
