// 
// audioeffecttypes/reverbeffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef __REVERBEFFECT_H
#define __REVERBEFFECT_H

#include "filterdefs.h"
#include "audiohardware/dspeffect.h"

#include "vectormath/vectormath.h"

namespace rage 
{

#define S16_DELAY_LINES 0

#if S16_DELAY_LINES
	typedef s16 delayLineSample;
#else
	typedef f32 delayLineSample;
#endif

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to alignment
#endif

	//
	// PURPOSE
	//  A Freeverb-based multichannel (mono) reverb.
	//
	class ALIGNAS(16) audReverbEffect : public audDspEffect
	{
	public:
		audReverbEffect();
		virtual ~audReverbEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		
		AUD_DECLARE_DSP_PROCESS_FUNCTION;

#if 0
		void Process_SSE(f32 *buffer, u32 numSamples);
#endif

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audReverbEffectParams
		{
			RoomSize = audEffectParamIds::ReverbEffect,
			WetMix,
			DryMix,
			Damping,
			ProcessAsMono,
			Bypass,
		};

	private:
		Vec::Vector_4V m_CombFilterDamp1FeedbackArray;
		Vec::Vector_4V m_DampVector1;
		Vec::Vector_4V m_DampVector2;
		Vec::Vector_4V m_DampVector3;
		Vec::Vector_4V m_DampVector4;

		Vec::Vector_4V m_WetDryCombFB;

		//Buffers for the combs.
		delayLineSample *m_CombBuffers[g_MaxOutputChannels];
		f32 m_CombStores[g_MaxOutputChannels][g_NumCombs];
		
		//Buffers for the allpasses.
		delayLineSample *m_AllPassBuffers[g_MaxOutputChannels];
		u16 m_AllPassBufferIndices[g_MaxOutputChannels][g_NumAllPasses];
		u16 m_CombBufferIndices[g_MaxOutputChannels][g_NumCombs];

		f32	m_Damping;
		
		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;
		u8 m_ProcessAsMono : 1;
		u8 m_Bypass : 1;
		u8 m_PrintedSpareScratch : 1;
	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //__REVERBEFFECT_H

