//
// audioeffecttypes/convolutioneffect_xenon.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC

#ifndef AUD_CONVOLUTION_EFFECT_PC_H
#define AUD_CONVOLUTION_EFFECT_PC_H

#include "convolutioneffect.h"

#define AUDIO_BENCHMARK_CONVOLUTION 0
#define AUDIOFRAMESIZE				1024

#if AUDIO_BENCHMARK_CONVOLUTION
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_CONVOLUTION

namespace rage {


class audConvolutionEffectPC: public audDspEffect
{
	public:
    audConvolutionEffectPC();

	virtual bool Init(u32 numChannels, Effect *metadata);
	virtual void Shutdown();
	virtual void Process(f32 *buffer, u32 numSamples);

	void SetParams();


private:

};

} // namespace rage

#endif // AUD_CONVOLUTION_EFFECT_PC_H

#endif // __WIN32PC
