//
// audioeffects/compressoreffect_xenon.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
// Adapted from Microsoft sample code so as to actually work.

#if __XENON
#define AUDIO_BENCHMARK_EFFECTS

#include "compressoreffect_xenon.h"
#include "compressoreffect.h"

#include "audiohardware/driver.h"
#include "audiohardware/driverutil.h"

#ifdef AUDIO_BENCHMARK_EFFECTS
#include "profile/profiler.h"
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_EFFECTS

namespace rage 
{

#ifdef AUDIO_BENCHMARK_EFFECTS
PF_PAGE(AudioCompressionPage, "RAGE Audio Compressor Effect");
PF_GROUP(CompressionEffect);
PF_LINK(AudioCompressionPage, CompressionEffect);
PF_VALUE_FLOAT(CompressorThreadPerc, CompressionEffect);

u32 g_CompressorFrameIndex = 0;
sysPerformanceTimer g_CompressorTimer("Compressor");
#endif // AUDIO_BENCHMARK_EFFECTS

#define USE_VECTORIZED_COMPRESSOR 1

ALIGNAS(16) f32 g_PeakLimiterOutputBuffer[256];

audCompressorEffectXenon::audCompressorEffectXenon()
{
	for (int i = 0; i < g_MaxOutputChannels; i++)
	{
		m_xdn1[i] = 0.0f;
		m_fn1[i]  = 0.0f;
		m_gn1[i]  = 0.0f;
	}

	if(audDriver::GetNumOutputChannels() >= 6)
	{
		m_IsDiscrete = true;
	}
	else
	{
		m_IsDiscrete = false;
	}
}

bool audCompressorEffectXenon::Init(const Effect *metadata)
{
	m_ChannelMask = metadata->ChannelMask.Value;
	m_NumChannelsToProcess = 0;
	for(u32 i=0; i<g_MaxOutputChannels; i++)
	{
		if(m_ChannelMask & (1 << i))
		{
			m_NumChannelsToProcess++;
		}
	}

	// Linear versions of gain and threshold
	m_GainLin	   = audDriverUtil::ComputeLinearVolumeFromDb(g_CompressorDefaultSettings.GainDbDefault);
	m_ThresholdLin = audDriverUtil::ComputeLinearVolumeFromDb(g_CompressorDefaultSettings.ThresholdDbDefault);

	// Attack and release time calculations
	m_AttackTime  = 1.0f - exp( -2.2f * (1.0f / 48000.f) / g_CompressorDefaultSettings.AttackDefault );
	m_ReleaseTime = 1.0f - exp( -2.2f * (1.0f / 48000.f) / g_CompressorDefaultSettings.ReleaseDefault );

	// Slope calculation
	m_Slope = 1.0f - ( 1.0f / g_CompressorDefaultSettings.RatioDefault );

	// RMS Window calculation
	f32 RMSWindow = g_CompressorDefaultSettings.AverageDefault * 48000.f;

	m_RMSWindow1	   = RMSWindow - 1.0f;
	m_OneOverRMSWindow = 1.0f / RMSWindow;

	m_InverseSlopedThresholdLin = 1.0f / audDriverUtil::powf(m_ThresholdLin, m_Slope);

	sysMemSet(&m_DelayBuffer[0][0],0,sizeof(m_DelayBuffer));
	sysMemSet(&m_LastEnvelope[0],0,sizeof(m_LastEnvelope));

    return true;
}

void audCompressorEffectXenon::DoProcess(const tCompressorEffectSettings &params, f32* __restrict pData, u32 numFrames, u32 numChannels)
{
	if(params.Bypass)
		return;

#ifdef AUDIO_BENCHMARK_EFFECTS
	g_CompressorTimer.Start();
#endif // AUDIO_BENCHMARK_EFFECTS

	if(params.PeakLimiterMode)
	{
		const f32 attack = params.PeakLimiterAttack;
		const f32 release = params.PeakLimiterRelease;
		//Apply min and max limits to compressor parameters.
		const f32 thresh = audDriverUtil::ComputeLinearVolumeFromDb(Clamp(params.ThresholdDb, g_CompressorDefaultSettings.ThresholdDbMin, g_CompressorDefaultSettings.ThresholdDbMax));
		const f32 finalGainLin = audDriverUtil::ComputeLinearVolumeFromDb(Clamp(params.GainDb, g_CompressorDefaultSettings.GainDbMin, g_CompressorDefaultSettings.GainDbMax));
		for(u32 inputChannelIndex=0; inputChannelIndex < numChannels; inputChannelIndex++)
		{
			if(m_ChannelMask & (1 << inputChannelIndex))
			{
				f32 *inputData = &(pData[inputChannelIndex * numFrames]);
				f32 lastEnv = m_LastEnvelope[inputChannelIndex];
				for(u32 curSample = 0; curSample < numFrames; curSample++)
				{
					float gain;

					const f32 delayedSample = (curSample < g_PeakLimiterDelayLength?m_DelayBuffer[inputChannelIndex][curSample]:inputData[curSample-g_PeakLimiterDelayLength]);
					const f32 currentSample = inputData[curSample];

					const f32 x = Abs(currentSample);
					const f32 a = audDriverUtil::fsel(x - lastEnv, attack, release);
					//const f32 a = (x>m_LastEnvelope[inputChannelIndex]?attack:release);
		
					// max filter
					f32 maxX = x;
					// get the max sample from [curSample - g_PeakLimiterDelayLength] to [curSample]
					u32 numSamplesCounted = 0;
					for(u32 i = curSample; i < g_PeakLimiterDelayLength; i++)
					{
						maxX = Max(maxX, Abs(m_DelayBuffer[inputChannelIndex][i]));
						numSamplesCounted++;
					}
					for(; numSamplesCounted < g_PeakLimiterDelayLength; numSamplesCounted++)
					{
						maxX = Max(maxX, Abs(inputData[numSamplesCounted + curSample - g_PeakLimiterDelayLength]));
					}

					const f32 env = (a * maxX) + ((1.f-a)*lastEnv);
					// prevent divide by zero: clamp env to 1.f if env is super small, since it will be ignored in the next fsel anyway (being smaller
					// than gain)
					const f32 safeEnv = audDriverUtil::fsel(env - 0.0001f, env, 1.f);
					gain = audDriverUtil::fsel(thresh - env, 1.0f, Min(1.0f, thresh/safeEnv));
					lastEnv = env;
					g_PeakLimiterOutputBuffer[curSample] = gain * delayedSample * finalGainLin;
				}
				m_LastEnvelope[inputChannelIndex] = lastEnv;

				// populate delay line with last N samples from this buffer
				XMemCpy(&m_DelayBuffer[inputChannelIndex], &inputData[numFrames-g_PeakLimiterDelayLength], g_PeakLimiterDelayLength*sizeof(f32));
				// overwrite the output
				XMemCpy(inputData, &g_PeakLimiterOutputBuffer, numFrames*sizeof(f32));
			}
		}

	}
	else
	{
	#if USE_VECTORIZED_COMPRESSOR
		__vector4 slopeVector;
		__vector4 inverseSlopedThresholdLinVector;
		__vector4 xdnsVector;
		__vector4 minXdnsVector = {{1.0E-14f, 1.0E-14f, 1.0E-14f, 1.0E-14f}};
		__vector4 onesVector = {{1.0f, 1.0f, 1.0f, 1.0f}};

		//Note: xdns must be the first variable after the __vector4s in order to guarantee 16-byte alignment.
		f32 xdns[4] = {0.0f, 0.0f, 0.0f, 0.0f};

		slopeVector = __lvlx(&m_Slope, 0);
		slopeVector = __vspltw(slopeVector, 0);
		inverseSlopedThresholdLinVector = __lvlx(&m_InverseSlopedThresholdLin, 0);
		inverseSlopedThresholdLinVector = __vspltw(inverseSlopedThresholdLinVector, 0);

	#endif // USE_VECTORIZED_COMPRESSOR

		f32 xdn;
		f32 a;
		f32 fn;
		f32 gn;

		u32 signalIndex = 0;

		for(u32 inputChannelIndex=0; inputChannelIndex < numChannels; inputChannelIndex++)
		{
			if(m_ChannelMask & (1 << inputChannelIndex))
			{
				//We need to process this input channel.
				f32* pfSample = &(pData[inputChannelIndex * numFrames]);

				if (inputChannelIndex == PLATFORM_SPEAKER_ID_LOW_FREQUENCY)
				{
					continue; //Don't compress the LFE
				}

				signalIndex = inputChannelIndex;

				/*else if (m_IsDiscrete)
				{
					//Link the fronts and link the surrounds if user is listening in a discrete mode.  If not, all 5 channels will be linked.
					if (inputChannelIndex == XAUDIOSPEAKER_FRONTLEFT || inputChannelIndex == XAUDIOSPEAKER_FRONTCENTER || inputChannelIndex == XAUDIOSPEAKER_FRONTRIGHT)
						signalIndex = 0;
					else 
					{
						Assert(inputChannelIndex == XAUDIOSPEAKER_BACKLEFT || inputChannelIndex == XAUDIOSPEAKER_BACKRIGHT);
						signalIndex = 1;
					}
				}*/

				// Loop through all the samples
	#if USE_VECTORIZED_COMPRESSOR

				for(u32 j=0; j<numFrames; j+=4)
				{
					//----------------------------------------------------------------------
					// Level detection
					//----------------------------------------------------------------------
					u32 offset;
					for(offset=0; offset<4; offset++)
					{
						// Running Average: (((n-1) * average) + newSample) / n
						xdn  = ((m_RMSWindow1 * m_xdn1[signalIndex]) + Abs(*(pfSample + offset))) * m_OneOverRMSWindow;
						m_xdn1[signalIndex] = xdn;
						xdns[offset] = xdn;
					}

					xdnsVector = __lvx(xdns, 0);
					xdnsVector = __vmaxfp(xdnsVector, minXdnsVector);
					xdnsVector = XMVectorPowEst(xdnsVector, slopeVector);
					xdnsVector = __vmulfp(xdnsVector, inverseSlopedThresholdLinVector);
					xdnsVector = __vmaxfp(xdnsVector, onesVector); //Don't expand.

					for(offset=0; offset<4; offset++)
					{
						fn = xdnsVector.v[offset];

						//----------------------------------------------------------------------
						// Attack / release time adjustment
						//----------------------------------------------------------------------

						a = audDriverUtil::fsel(fn - m_fn1[signalIndex], m_AttackTime, m_ReleaseTime);
						gn = ((fn - m_gn1[signalIndex]) * a) + m_gn1[signalIndex];

						//----------------------------------------------------------------------
						// Adjust the signal
						//----------------------------------------------------------------------

						// Output
						*pfSample++ *= (m_GainLin / gn);

						// Save samples
						m_fn1[signalIndex] = fn;
						m_gn1[signalIndex] = gn;
					}
				}

	#else // !USE_VECTORIZED_COMPRESSOR

				for(u32 j=0; j<numFrames; j++)
				{
					//----------------------------------------------------------------------
					// Level detection
					//----------------------------------------------------------------------

					// Running Average: (((n-1) * average) + newSample) / n
					xdn  = ((m_RMSWindow1 * m_xdn1[signalIndex]) + Abs(*pfSample)) * m_OneOverRMSWindow;
					m_xdn1[signalIndex] = xdn;

					xdn = Max(xdn, 1.0E-14f);
					fn = Max(m_InverseSlopedThresholdLin * audDriverUtil::powf(xdn, m_Slope), 1.0f); //Don't expand.

					//----------------------------------------------------------------------
					// Attack / release time adjustment
					//----------------------------------------------------------------------

					a = audDriverUtil::fsel(fn - m_fn1[signalIndex], m_AttackTime, m_ReleaseTime);
					gn = ((fn - m_gn1[signalIndex]) * a) + m_gn1[signalIndex];

					//----------------------------------------------------------------------
					// Adjust the signal
					//----------------------------------------------------------------------

					// Output
					*pfSample++ *= (m_GainLin / gn);

					// Save samples
					m_fn1[signalIndex] = fn;
					m_gn1[signalIndex] = gn;
				}

	#endif // USE_VECTORIZED_COMPRESSOR

			}
		} //inputChannelIndex
	}

#ifdef AUDIO_BENCHMARK_EFFECTS
	g_CompressorTimer.Stop();

	if(++g_CompressorFrameIndex == 1)
	{
		PF_SET(CompressorThreadPerc, g_CompressorTimer.GetTimeMS() * 18.75f);
		g_CompressorTimer.Reset();
		g_CompressorFrameIndex = 0;
	}
#endif // AUDIO_BENCHMARK_EFFECTS
}

void audCompressorEffectXenon::OnSetParameters(const tCompressorEffectSettings &params)
{
	if(!params.PeakLimiterMode)
	{
		
		//Apply min and max limits to compressor parameters.
		//Do some dB-->linear conversions
		m_ThresholdLin = audDriverUtil::ComputeLinearVolumeFromDb(Clamp(params.ThresholdDb, g_CompressorDefaultSettings.ThresholdDbMin, g_CompressorDefaultSettings.ThresholdDbMax));
		m_GainLin	   = audDriverUtil::ComputeLinearVolumeFromDb(Clamp(params.GainDb,		g_CompressorDefaultSettings.GainDbMin,		g_CompressorDefaultSettings.GainDbMax));

		const f32 sampleRate = 48000.f;

		// Attack and release time calculations
		m_AttackTime  = 1.0f - exp( -2.2f * (1.0f / sampleRate) / params.Attack );
		m_ReleaseTime = 1.0f - exp( -2.2f * (1.0f / sampleRate) / params.Release );

		// Slope calculation
		m_Slope = 1.0f - ( 1.0f / params.Ratio );

		// RMS Window calculation
		f32 RMSWindow = params.Average * sampleRate;

		m_RMSWindow1	   = RMSWindow - 1.0f;
		m_OneOverRMSWindow = 1.0f / RMSWindow;

		m_InverseSlopedThresholdLin = 1.0f / audDriverUtil::powf(m_ThresholdLin, m_Slope);
	}
}


} // namespace rage

#endif //__XENON
