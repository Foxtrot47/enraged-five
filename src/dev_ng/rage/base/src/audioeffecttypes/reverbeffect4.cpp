// 
// audioeffecttypes/reverbeffect4.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "reverbeffect.h"
#include "reverbeffect4.h"
#include "reverbtuning4.h"

#include "audioengine/engineutil.h"
#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/framebufferpool.h"

#include "system/cache.h"
#include "system/dma.h"
#include "system/memops.h"
#include "system/performancetimer.h"


namespace rage 
{
	using namespace Vec;
	audReverbEffect4::audReverbEffect4() : audDspEffect(AUD_DSPEFFECT_REVERB4)
		, m_Bypass(false)
		, m_PrintedSpareScratch(false)
	{

	}

	audReverbEffect4::~audReverbEffect4()
	{

	}

	bool audReverbEffect4::Init(const u32 ASSERT_ONLY(numChannels), const u32 ASSERT_ONLY(channelMask))
	{
		// For now we only support 4 channel interleaved
		Assert(numChannels == 4);
		SetProcessFormat(Interleaved);
		Assert(channelMask == (RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT));

		m_PrintedSpareScratch = 0;

		// Allocate delay lines
		for(s32 i = 0; i < m_CombFilters.GetMaxCount(); i++)
		{
			m_CombFilters[i].SetLength(audReverbEffect4Tuning::CombFilterLengths[i]);
		}

		for(s32 i = 0; i < m_AllpassFilters.GetMaxCount(); i++)
		{
			m_AllpassFilters[i].SetLength(audReverbEffect4Tuning::AllPassFilterLengths[i]);
		}

		// Initialise with default parameters
		SetParam(audReverbEffect4::Damping, audReverbEffect4Tuning::InitialDamp);
		SetParam(audReverbEffect4::RoomSize, audReverbEffect4Tuning::InitialRoom);

		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audReverbEffect4)
	{
		if(m_Bypass)
		{
			return;
		}

		Assert(buffer.Format == Interleaved);

#if AUD_TIME_DSP
		sysPerformanceTimer timer("");
		timer.Start();
#endif

#if __SPU
		const s32 localBufferSamples = kMixBufNumSamples;
		const s32 tag = 4;
		for(s32 i = 0; i < audReverbEffect4Tuning::NumCombs; i++)
		{
			m_CombFilters[i].Prefetch(localBufferSamples, tag);
		}
		for(s32 i = 0; i < audReverbEffect4Tuning::NumAllPasses; i++)
		{
			m_AllpassFilters[i].Prefetch(localBufferSamples, tag);
		}
		sysDmaWait(1<<tag);
#endif // __SPU

		const u32 numSamplesPerPass = kMixBufNumSamples / 4;

		if (buffer.ChannelBufferIds[0] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[0], buffer.ChannelBuffers[0], numSamplesPerPass);
		if (buffer.ChannelBufferIds[1] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[1], buffer.ChannelBuffers[1], numSamplesPerPass);
		if (buffer.ChannelBufferIds[2] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[2], buffer.ChannelBuffers[2], numSamplesPerPass);
		if (buffer.ChannelBufferIds[3] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[3], buffer.ChannelBuffers[3], numSamplesPerPass);

#if __SPU
		for(s32 i = 0; i < audReverbEffect4Tuning::NumCombs; i++)
		{
			m_CombFilters[i].Writeback(tag);
		}
		for(s32 i = 0; i < audReverbEffect4Tuning::NumAllPasses; i++)
		{
			m_AllpassFilters[i].Writeback(tag);
		}
		sysDmaWait(1<<tag);
#endif // __SPU

#if AUD_TIME_DSP
		timer.Stop();
		sm_ProcessTime[AUD_DSPEFFECT_REVERB4] = timer.GetTimeMS() * 1000.f;
#endif
	}

	void audReverbEffect4::ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples)
	{
		const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outBuffer);

		const Vector_4V damping = m_Damping;
		const Vector_4V roomSize = m_RoomSize;
		const Vector_4V fixedGain = V4VConstantSplat<0x3C75C28F>(); // 0.015

		u32 count = numSamples;

		while(count--)
		{
			Vector_4V input = V4Scale(*inputPtr++, fixedGain);
			Vector_4V output = V4VConstant(V_ZERO);

			// Accumulate comb filters in parallel
			CompileTimeAssert(audReverbEffect4Tuning::NumCombs == 8);
			output = V4Add(output, m_CombFilters[0].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[1].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[2].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[3].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[4].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[5].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[6].Process(input, damping, roomSize));
			output = V4Add(output, m_CombFilters[7].Process(input, damping, roomSize));

			// Feed through allpasses in series
			CompileTimeAssert(audReverbEffect4Tuning::NumAllPasses == 4);			
			output = m_AllpassFilters[0].Process(output);
			output = m_AllpassFilters[1].Process(output);
			output = m_AllpassFilters[2].Process(output);
			output = m_AllpassFilters[3].Process(output);

			// original applies a scaling of 3 to wet
			*outputPtr++ = V4Scale(output, V4VConstant(V_THREE));
		}
	}

	void audReverbEffect4::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case audReverbEffect::Bypass:
			m_Bypass = (value != 0);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audReverbEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audReverbEffect4::SetParam(const u32 paramId, const f32 value)
	{
		switch(paramId)
		{
		case audReverbEffect::Damping:
			{
				const float damping = Clamp(value, 0.f, 1.f) * audReverbEffect4Tuning::ScaleDamp;
				m_Damping = V4LoadScalar32IntoSplatted(damping);				
			}
			break;
		case audReverbEffect::RoomSize:
			{
				const float roomSize = (Clamp(value, 0.f, 1.f) * audReverbEffect4Tuning::ScaleRoom) + audReverbEffect4Tuning::OffsetRoom;
				m_RoomSize = V4LoadScalar32IntoSplatted(roomSize);				
			}
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audReverbEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}
	}

	void audReverbEffect4::Shutdown(void)
	{

	}

} // namespace rage


