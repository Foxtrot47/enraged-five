// 
// audioeffecttypes/ereffect.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_EREFFECT_H
#define AUD_EREFFECT_H

#include "filterdefs.h"

#include "audiohardware/dspeffect.h"
#include "audiosynth/blocks.h"
#include "vectormath/vectormath.h"
#include "audiohardware/mixer.h"

namespace ravesimpletypes
{
	struct ERSettings;
}

namespace rage 
{

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to alignment
#endif

#define AUD_ER_24kHz 1

	class ALIGNAS(16) audEarlyReflectionEffect : public audDspEffect
	{
	public:
		audEarlyReflectionEffect();
		virtual ~audEarlyReflectionEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audEarlyReflectionEffectParams
		{
			Settings = audEffectParamIds::EarlyReflectionEffect,
			
			Bypass,
		};

	private:

		void Upsample(const Vec::Vector_4V *inPtr, Vec::Vector_4V *outPtr, const u32 numSamples);
		void ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples);
		void SetupRoom(const ravesimpletypes::ERSettings *settings);

		enum { kNumNodes = 6};
	
		Vec::Vector_4V m_2ndOrderGain;
		Vec::Vector_4V m_3rdOrderGain;

#if AUD_ER_24kHz
		enum { kERSampleRate = 24000 };
#else
		enum { kERSampleRate = kMixerNativeSampleRate };
#endif
		
		atRangeArray<revStateVariable_V4<kERSampleRate>, kNumNodes> m_2ndOrderLPFs;
		atRangeArray<revStateVariable_V4<kERSampleRate>, kNumNodes> m_3rdOrderLPFs;
		atRangeArray<revSimpleDelay_V8, kNumNodes> m_1stOrderDelays;
		atRangeArray<revSimpleDelay_V8, kNumNodes> m_2ndOrderDelays;
		atRangeArray<revSimpleDelay_V8, kNumNodes> m_3rdOrderDelays;

		enum { kNumAllpasses = 4 };
		atRangeArray<revAllPass_V8, kNumAllpasses> m_Allpasses;
		
		revBiquad_V4 m_AntiImaging;

		//revHalfBandFilter m_AntiAlias;
		//rev8thOrderHalfBandFilter m_AntiAlias;
		//rev8thOrderHalfBandFilter m_AntiImaging;

		const ravesimpletypes::ERSettings *m_Settings;

		u32 m_CurrentSettingsHash;
		float m_RoomSize;
		// bool m_RecomputeCoefficients;
		bool m_Bypass;

	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //AUD_EREFFECT_H

