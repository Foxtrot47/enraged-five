#ifndef AUD_REVMODELPROG_H
#define AUD_REVMODELPROG_H

#include "audiosynth/blocks.h"
#include "vectormath/vectormath.h"

namespace rage
{

class revModelProgenitor_V4
{
public:

	Vec::Vector_4V Diffusion1;
	Vec::Vector_4V Diffusion2;
	Vec::Vector_4V DecayDiffusion;
	Vec::Vector_4V Decay1;
	Vec::Vector_4V Decay2;
	Vec::Vector_4V Decay3;
	Vec::Vector_4V HF_Bandwidth;
	Vec::Vector_4V Definition;

	Vec::Vector_4V ChorusDry;
	Vec::Vector_4V ChorusWet;

	Vec::Vector_4V Damping;

	revModelProgenitor_V4()
	{
		HF_Bandwidth = Vec::V4LoadScalar32IntoSplatted(0.188f);
		Decay1 = Vec::V4LoadScalar32IntoSplatted(0.938f);
		Decay2 = Vec::V4LoadScalar32IntoSplatted(0.844f);
		Decay3 = Vec::V4LoadScalar32IntoSplatted(0.906f);
		Diffusion1 = Vec::V4LoadScalar32IntoSplatted(0.312f);
		Diffusion2 = Vec::V4LoadScalar32IntoSplatted(0.375f);
		Definition = Vec::V4LoadScalar32IntoSplatted(0.25f);
		DecayDiffusion = Vec::V4LoadScalar32IntoSplatted(0.406f);

		ChorusDry = Vec::V4LoadScalar32IntoSplatted(0.781f);
		ChorusWet = Vec::V4LoadScalar32IntoSplatted(0.219f);

		Damping = Vec::V4LoadScalar32IntoSplatted(0.312f);
	}

	bool Init()
	{
		REV_CHECK_RET(n_60_61.SetLength(1));

		REV_CHECK_RET(n_15_16.SetLength(239));
		REV_CHECK_RET(n_16_17.SetLength(2));
		REV_CHECK_RET(n_17_18.SetLength(392));

		REV_CHECK_RET(n_23_24.SetLength(1055));
		REV_CHECK_RET(n_24_27.SetLength(1944, 612));
		REV_CHECK_RET(n_27_31.SetLength(344));


		REV_CHECK_RET(n_31_37.SetLength(1212, 121, 816, 1264));
		REV_CHECK_RET(n_37_39a.SetLength(1572));

		REV_CHECK_RET(n_65_66.SetLength(1));

		REV_CHECK_RET(n_19_20.SetLength(205));
		REV_CHECK_RET(n_20_21.SetLength(1));
		REV_CHECK_RET(n_21_22.SetLength(329));

		REV_CHECK_RET(n_40_41.SetLength(625));
		REV_CHECK_RET(n_41_42.SetLength(835));

		REV_CHECK_RET(n_42_45.SetLength(368, 2032));

		REV_CHECK_RET(n_45_49.SetLength(500));
		REV_CHECK_RET(n_49_55.SetLength(1452, 5, 688, 1340));
		REV_CHECK_RET(n_55_58.SetLength(16));

		return true;
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In lInput, Vec::Vector_4V_In rInput);

private:

	revDCBlocker_V4 dcL,dcR;
	// Left channel
	//revSimpleDelay<1> n_59_60;
	revSimpleDelay_V4 n_60_61;
	//revSimpleDelay<1> n_9_10;
	//revSimpleDelay<1> n_11_12;

	revOnePole_V4 n_7_8; // biquad?
	revOnePole_V4 n_9_10; // biquad?
	revOnePole_V4 n_59_60;
	revOnePole_V4 n_64_65;
	revOnePole_V4 n_11_12;
	revOnePole_V4 n_13_14;

	revAllPassFig8_V4 n_15_16;
	revSimpleDelay_V4 n_16_17;
	revAllPassFig8_V4 n_17_18;

	revSimpleDelay_V4 n_23_24;

	revAllPassDualFig8_V4 n_24_27;
	revSimpleDelay_V4 n_27_31;
	revAllPassTrioChorus_V4 n_31_37;
	revSimpleDelay_V4 n_37_39a;

	// right channel
	//revSimpleDelay<1> n_64_65;
	revSimpleDelay_V4 n_65_66;
	//revSimpleDelay<1> n_7_8;
	//revSimpleDelay<1> n_13_14;

	revAllPassFig8_V4 n_19_20;
	revSimpleDelay_V4 n_20_21;
	revAllPassFig8_V4 n_21_22;

	revSimpleDelay_V4 n_40_41;
	revSimpleDelay_V4 n_41_42;
	revAllPassDualFig8_V4 n_42_45;
	revSimpleDelay_V4 n_45_49;
	revAllPassTrioChorus_V4 n_49_55;
	revSimpleDelay_V4 n_55_58;

};

} // namespace rage

#endif // AUD_REVMODELPROG_H
