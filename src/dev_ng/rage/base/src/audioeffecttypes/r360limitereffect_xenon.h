// 
// audioeffecttypes/r360limitereffect_xenon.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __XENON && 0

#ifndef AUDIOEFFECTTYPES_R360LIMITEREFFECT_XENON_H 
#define AUDIOEFFECTTYPES_R360LIMITEREFFECT_XENON_H 

#include "audioeffecttypes/r360limitereffect.h"

#include "atl/queue.h"

#include "system/xtl.h"
#include <xaudio.h>
#include <xaudfx.h>

#if _XDK_VER >= 7978
#pragma warning(disable:4996)
#endif

namespace rage {

#ifndef ASSERT
#define ASSERT Assert //For XAUDIO_USE_BATCHALLOC_NEWDELETE macro.
#endif

#define assert Assert //So we work with RAGE.

//
// PURPOSE
//  Defines the parameters that can be used to control the compressor.
//
const XAUDIOFXPARAMID R360LIMITERFX_SETTINGS	= 0;

const u32 g_R360LimiterEffectMaxChannels	= XAUDIOSPEAKER_COUNT;

// Size function.
HRESULT QuerySizeR360LimiterEffectXenon(LPCXAUDIOFXINIT init, LPDWORD effectSize);
// Effect creation function.
HRESULT CreateR360LimiterEffectXenon(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator, IXAudioEffect **effect);

//
// PURPOSE
//  A general dynamic range compressor.  
//
class audR360LimiterEffectXenon : public IXAudioEffect
{
#pragma warning(push)
#pragma warning(disable:4100)
	XAUDIO_USE_BATCHALLOC_NEWDELETE();
#pragma warning(pop)

public:
	//IXAudioRefCount
	virtual ULONG AddRef(void);
	virtual ULONG Release(void);

	//IXAudioEffect
	virtual HRESULT GetInfo(XAUDIOFXINFO *info);
	virtual HRESULT GetParam(XAUDIOFXPARAMID paramId, XAUDIOFXPARAMTYPE paramType, XAUDIOFXPARAM *param);
	virtual HRESULT SetParam(XAUDIOFXPARAMID paramId, XAUDIOFXPARAMTYPE paramType, const XAUDIOFXPARAM *param);
	virtual HRESULT GetContext(LPVOID *context);
	virtual HRESULT Process(IXAudioFrameBuffer *inputBuffer, IXAudioFrameBuffer *outputBuffer);

	//UserEffect
	audR360LimiterEffectXenon(LPVOID context);
	virtual ~audR360LimiterEffectXenon() {}
	virtual HRESULT Init(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator);

private:
	u32   m_RefCount;
	void* m_Context;

	f32 m_DelayBuffer[g_MaxOutputChannels][g_R360DelayLength];

	u32 m_uCurrentSample;

	f32 m_fReleaseRate;			//	Release time in linear gain units per sample
	f32 m_fRmsThresholdMin;
	f32 m_fRmsThresholdMax;
	f32 m_fOneOverRmsThresholdDelta;

	f32 m_fInputClipLin;		//	Linear representation of dB clip point passed in
	f32 m_fThresholdLin;		//	Linear representation of dB threshold passed in
	f32 m_fOutputCeilingLin;	//	Linear representation of dB makeup gain passed in
	f32 m_fCurrentGain;			//	Linear gain current value
	f32 m_fTargetGain;			//	Linear gain target value
	f32 m_fGainRate;			//	Linear rate of gain change per sample

	atQueue<audR360LimiterEffect::tTargetGain, g_R360DelayLength> m_RunningTargets;	//	Stores 64 peaks ahead

	tR360LimiterEffectSettings m_Settings;

	u8 m_NumInputChannels;
	u8 m_NumChannelsToProcess;
	u8 m_ChannelMask;

	class RmsCalculator
	{
	public:
		RmsCalculator() : m_fValue(0.0) { for (int i = 0; i < sm_iSize; ++i) m_Queue.Push(0.0f); }
		__forceinline f32 AddValue(f32 fValue)
		{
			fValue *= (1.0f/sm_iSize);
			m_fValue += fValue;
			m_Queue.Push(fValue);
			m_fValue -= m_Queue.Pop();
			return m_fValue;
		}

	private:
		static const int sm_iSize = 2400;
		f32 m_fValue;
		atQueue<f32, sm_iSize> m_Queue;
	} m_RmsCalculator;
};

} // namespace rage

#endif // AUDIOEFFECTTYPES_R360LIMITEREFFECT_XENON_H

#endif // __XENON
