// 
// audioeffecttypes/r360limitereffect_pc.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __WIN32PC

#ifndef AUDIOEFFECTTYPES_R360LIMITEREFFECT_PC_H 
#define AUDIOEFFECTTYPES_R360LIMITEREFFECT_PC_H 

#include "audioeffecttypes/r360limitereffect.h"

#include "atl/queue.h"

#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"

namespace rage {

	//
	// PURPOSE
	//  A general dynamic range compressor.
	//
	class audR360LimiterEffectPc : public audDspEffect
	{
	public:
		audR360LimiterEffectPc();
		virtual ~audR360LimiterEffectPc() {}

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		virtual void Process(f32 *buffer, u32 numSamples);

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audR360LimiterEffectParams
		{
			OutCeilingDb = audEffectParamIds::R360LimiterEffect,
			ThresholdDb,
			Release,
			AutoReleaseMin,
			AutoReleaseMax,
			AutoReleaseRmsThresMin,
			AutoReleaseRmsThresMax,
			InputClipDb,
			AutoReleaseMode,
			Bypass,
		};

	private:
		f32 m_DelayBuffer[g_MaxOutputChannels][g_R360DelayLength];

		u32 m_uCurrentSample;

		f32 m_fReleaseRate;			//	Release time in linear gain units per sample
		f32 m_fRmsThresholdMin;
		f32 m_fRmsThresholdMax;
		f32 m_fOneOverRmsThresholdDelta;

		f32 m_fInputClipLin;		//	Linear representation of dB clip point passed in
		f32 m_fThresholdLin;		//	Linear representation of dB threshold passed in
		f32 m_fOutputCeilingLin;	//	Linear representation of dB makeup gain passed in
		f32 m_fCurrentGain;			//	Linear gain current value
		f32 m_fTargetGain;			//	Linear gain target value
		f32 m_fGainRate;			//	Linear rate of gain change per sample

		atQueue<audR360LimiterEffect::tTargetGain, g_R360DelayLength> m_RunningTargets;	//	Stores 64 peaks ahead

		tR360LimiterEffectSettings m_Settings;

		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;

		class RmsCalculator
		{
		public:
			RmsCalculator() : m_fValue(0.0) { for (int i = 0; i < sm_iSize; ++i) m_Queue.Push(0.0f); }
			__forceinline f32 AddValue(f32 fValue)
			{
				fValue *= (1.0f/sm_iSize);
				m_fValue += fValue;
				m_Queue.Push(fValue);
				m_fValue -= m_Queue.Pop();
				return m_fValue;
			}

		private:
			static const int sm_iSize = 2400;
			f32 m_fValue;
			atQueue<f32, sm_iSize> m_Queue;
		} m_RmsCalculator;
	};

} // namespace rage

#endif // AUDIOEFFECTTYPES_R360LIMITEREFFECT_PC_H 

#endif // __WIN32PC
