// 
// audioeffecttypes/compressoreffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef __COMPRESSOREFFECT_H
#define __COMPRESSOREFFECT_H


#include "filterdefs.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"

namespace rage {

//
// PURPOSE
//  A general dynamic range compressor.
//
#if __WIN32
#pragma warning(disable: 4324) // structure padded due to alignment
#endif
class audCompressorEffectPc : public audDspEffect
{
    public:
		audCompressorEffectPc();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		virtual void Process(f32 *buffer, u32 numSamples);
	   
		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audCompressorEffectParams
		{
			GainDb = audEffectParamIds::CompressorEffect,
			ThresholdDb,
			Ratio,
			Attack,
			Release,
			Average,
			PeakLimiterAttack,
			PeakLimiterRelease,
			PeakLimiterMode,
			Bypass,
		};

    private:

		ALIGNAS(16) f32 m_DelayBuffer[g_MaxOutputChannels][g_PeakLimiterDelayLength];
		ALIGNAS(16) f32 m_LastEnvelope[g_MaxOutputChannels];


		f32 m_xdn1[g_MaxOutputChannels];	// Running RMS average window				
		f32 m_fn1[g_MaxOutputChannels];	// Output factor of static Peak/RMS function (compressor control parameter)
		f32 m_gn1[g_MaxOutputChannels];	// Time-variant gain factor

		f32 m_AttackTime;			// internal representation of attack time
		f32 m_ReleaseTime;			// internal representation of release time
		f32 m_Slope;				// Slope factor
		f32 m_OneOverRMSWindow;
		f32 m_RMSWindow1;

		f32 m_ThresholdLin;			//linear representation of dB threshold passed in
		f32 m_GainLin;				//linear representation of dB makeup gain passed in

		f32 m_InverseSlopedTresholdLin;

		f32 m_PeakLimiterAttack;
		f32 m_PeakLimiterRelease;
		
		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;

		bool m_Bypass;
		bool m_PeakLimiterMode;
};
#if __WIN32
#pragma warning(error: 4324) // structure padded due to alignment
#endif
} // namespace rage

#endif //__COMPRESSOREFFECT_H

