//
// audioeffects/reverbtuning.h
// 
// Freeverb model tuning values. Simplified to mono tuning.
//
// Originally written by Jezar at Dreampoint, June 2000
// http://www.dreampoint.co.uk
// This code is public domain

#ifndef AUD_REVERB_TUNING_H
#define AUD_REVERB_TUNING_H

namespace rage {

const f32 g_FixedGain		= 0.015f;
const f32 g_ScaleWet		= 3.0f;
const f32 g_ScaleDry		= 2.0f;
const f32 g_ScaleDamp		= 0.4f;
const f32 g_ScaleRoom		= 0.28f;
const f32 g_OffsetRoom		= 0.7f;
const f32 g_InitialRoom		= 0.5f;
const f32 g_InitialDamp		= 0.5f;
const f32 g_InitialWet		= 0.0f;
const f32 g_InitialDry		= 1.0f;
const f32 g_AllPassFeedback = 0.5f;

#if !(__PS3 && __DEV)
// These values assume 44.1KHz sample rate
// they will probably be OK for 48KHz sample rate
// but would need scaling for 96KHz (or other) sample rates.
// The values were obtained by listening tests.
// Note: The values in comments are the lengths originally selected by Jezar. It was necessary to modify these values in
// order to achieve lengths that are divisible by 4, which is necessary for vectorization. It is hoped that the new lengths
// don't undermine the original tuning.
const u32 g_CombFilterLengths[] =
{
	1116,
	1188,
	1276,//1277,
	1356,
	1424,//1422,
	1492,//1491
	1556,//1557,
	1616//1617
};
const u32 g_NumCombs = (sizeof(g_CombFilterLengths)/sizeof(g_CombFilterLengths[0]));
const u32 g_TotalNumCombSamples = 1116+1188+1276+1356+1424+1492+1556+1616; // for some reason the compiler can't evaluate this itself
CompileTimeAssert(g_NumCombs == 8);
#else

// One less comb filter on PS3 for now to save local store.  This is temporary.
const u32 g_CombFilterLengths[] =
{
	1116,
	1188,
	1276,//1277,
	1356,
	1424,//1422,
	1492,//1491
	1556,//1557,
	//1616//1617
};
const u32 g_NumCombs = (sizeof(g_CombFilterLengths)/sizeof(g_CombFilterLengths[0]));
const u32 g_TotalNumCombSamples = 1116+1188+1276+1356+1424+1492+1556; // for some reason the compiler can't evaluate this itself
CompileTimeAssert(g_NumCombs == 7);
#endif

//Note: The values in comments are the lengths originally selected by Jezar. It was necessary to modify these values in
//order to achieve lengths that are divisible by 4, which is necessary for vectorization. It is hoped that the new lengths
//are similarly orthogonal in perturbing the phase of the input samples.
const u32 g_AllPassFilterLengths[] =
{
	556,
	440,	//441,
	340,	//341,
	224		//225
};
const u32 g_NumAllPasses = (sizeof(g_AllPassFilterLengths)/sizeof(g_AllPassFilterLengths[0]));
const u32 g_TotalNumAllPassSamples = 556+440+340+224;
CompileTimeAssert(g_NumAllPasses==4);

} // namespace rage

#endif // AUD_REVERB_TUNING_H
