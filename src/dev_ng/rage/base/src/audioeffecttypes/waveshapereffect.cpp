// 
// audioeffecttypes/waveshapereffect.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "waveshapereffect.h"

#include "system/memops.h"
#include "audioengine/curve.h"
#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/framebufferpool.h"
#include "math/amath.h"
#include "math/random.h"

#if __SPU
#include "audioengine/curve.cpp"
#endif

namespace rage {

	audWaveshaperEffect::audWaveshaperEffect() : audDspEffect(AUD_DSPEFFECT_WAVESHAPER)
	{
		m_InputGainLin  = 1.f;
		m_OutputGainLin = 1.f;
		m_ApplyFactor	= 1.f;
		m_Bypass		= false;
	}

	bool audWaveshaperEffect::Init(u32 numChannels, u32 channelMask)
	{
		m_NumInputChannels = (u8)numChannels;
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i=0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audWaveshaperEffect)
	{
		if (m_Bypass)
		{
			return;
		}

		if(!m_TransferFunction.IsValidInSoundTask())
		{
			return;
		}

		f32 *samples;
		f32 inputSample;
		f32 outputSample;
		f32 sign;

		for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
		{
			if(m_ChannelMask & (1 << inputChannelIndex))
			{
				//We need to process this input channel.
				samples = buffer.ChannelBuffers[inputChannelIndex];

				if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId)
				{
					for (u32 j = 0; j < kMixBufNumSamples; j++)
					{
						//apply input gain and clamp to known range
						inputSample = (*samples * m_InputGainLin);

						sign = Selectf(inputSample, 1.0f, -1.0f);
						outputSample = m_TransferFunction.CalculateValue(Abs(inputSample)) * sign;

						//now apply the morph factor
						outputSample = Lerp(m_ApplyFactor, inputSample, outputSample);
						//apply output gain
						*samples++ = (m_OutputGainLin * outputSample);
					}
				}
			}
		} //inputChannelIndex
	}

#if !__SPU
	void audWaveshaperEffect::SetParam(const u32 paramId, const f32 value)
	{
		switch(paramId)
		{
		case audWaveshaperEffect::InputGainDb:
			m_InputGainLin = audDriverUtil::ComputeLinearVolumeFromDb(value);
			break;
		case audWaveshaperEffect::OutputGainDb:
			m_OutputGainLin = audDriverUtil::ComputeLinearVolumeFromDb(value);
			break;
		case audWaveshaperEffect::ApplyFactor:
			m_ApplyFactor = value;
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audWaveshaperEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}
	}

	void audWaveshaperEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case audWaveshaperEffect::Bypass:
			m_Bypass = (value != 0);
			break;
		case audWaveshaperEffect::TransferFunction:
			AssertVerify(m_TransferFunction.Init(value));
			Assert(m_TransferFunction.IsValidInSoundTask());
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audWaveshaperEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audWaveshaperEffect::Shutdown(void)
	{
	}
#endif // !__SPU
} // namespace rage

