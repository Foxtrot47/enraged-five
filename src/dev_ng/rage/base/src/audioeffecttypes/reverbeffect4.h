// 
// audioeffecttypes/reverbeffect4.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REVERBEFFECT4_H
#define __REVERBEFFECT4_H

#include "filterdefs.h"
#include "reverbtuning4.h"
#include "audiohardware/dspeffect.h"
#include "audiosynth/blocks.h"
#include "vectormath/vectormath.h"

namespace rage 
{
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to alignment
#endif

	// PURPOSE
	//	Vectorised (interleaved channels) Freeverb allpass implementation
	class fvAllpass
	{
	public:
		fvAllpass()
		{

		}

		void SetLength(const u32 lengthSamples)
		{
			m_Delay.SetLength(lengthSamples);
		}

		void ZeroBuffer()
		{
			m_Delay.ZeroBuffer();
		}
	
		__forceinline Vec::Vector_4V_Out Process(const Vec::Vector_4V_In input)
		{
			const Vec::Vector_4V xn = input;	
			const Vec::Vector_4V az = m_Delay.Read();
			const Vec::Vector_4V yn = Vec::V4AddScaled(az, xn, Vec::V4VConstant(V_NEGONE));
			m_Delay.Write(Vec::V4AddScaled(xn, az, Vec::V4VConstant(V_HALF)));
			return yn;
		}

#if __SPU
		void Prefetch(const s32 localBufferSize, const s32 tag)
		{
			m_Delay.Prefetch(localBufferSize, tag);
		}

		void Writeback(const s32 tag)
		{
			m_Delay.Writeback(tag);
		}
#endif // __SPU

	private:
		revSimpleDelay_V4 m_Delay;
	};

	// PURPOSE
	//	Vectorised (interleaved channels) Freeverb comb filter
	class fvCombFilter
	{
	public:
		fvCombFilter() 			
			: m_FilterStore(Vec::V4VConstant(V_ZERO))			
		{

		}

		void SetLength(const u32 length)
		{
			m_Delay.SetLength(length);
		}

		void ZeroBuffer()
		{
			m_Delay.ZeroBuffer();
		}

		__forceinline Vec::Vector_4V_Out Process(Vec::Vector_4V_In input, Vec::Vector_4V_In damping, Vec::Vector_4V_In feedback)
		{
			const Vec::Vector_4V y = m_Delay.Read();
			const Vec::Vector_4V damp2 = Vec::V4Subtract(Vec::V4VConstant(V_ONE), damping);
			m_FilterStore = Vec::V4AddScaled(Vec::V4Scale(y, damp2), m_FilterStore, damping);
			m_Delay.Write(Vec::V4AddScaled(input, m_FilterStore, feedback));
			return y;
		}

#if __SPU
		void Prefetch(const s32 localBufferSize, const s32 tag)
		{
			m_Delay.Prefetch(localBufferSize, tag);
		}

		void Writeback(const s32 tag)
		{
			m_Delay.Writeback(tag);
		}
#endif // __SPU

	private:	

		revSimpleDelay_V4 m_Delay;
		// Vec::Vector_4V m_Feedback;
		Vec::Vector_4V m_FilterStore;
		// Vec::Vector_4V m_Damping;
	};

	//
	// PURPOSE
	//  A Freeverb-based multichannel (independent mono) reverb.
	//
	class ALIGNAS(16) audReverbEffect4 : public audDspEffect
	{
	public:
		audReverbEffect4();
		virtual ~audReverbEffect4();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audReverbEffectParams
		{
			RoomSize = audEffectParamIds::ReverbEffect,
			WetMix,
			DryMix,
			Damping,
			ProcessAsMono,
			Bypass,
		};

	private:
		void ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples);

		Vec::Vector_4V m_Damping;
		Vec::Vector_4V m_RoomSize;

		atRangeArray<fvCombFilter, audReverbEffect4Tuning::NumCombs> m_CombFilters;
		atRangeArray<fvAllpass, audReverbEffect4Tuning::NumAllPasses> m_AllpassFilters;
		
		u8 m_Bypass : 1;
		u8 m_PrintedSpareScratch : 1;
	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //__REVERBEFFECT_H
