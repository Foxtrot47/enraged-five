//
// audioeffects/delayeffect_xenon.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON

#ifndef __DELAYEFFECT_XENON_H
#define __DELAYEFFECT_XENON_H

#include "audioeffecttypes/delayeffect.h"
#include "math/amath.h"

#include "rage_xapo.h"

namespace rage {

//
// PURPOSE
//  A simple delay effect.
//
class __declspec(uuid("{E91B2C44-2A37-468d-8C58-362FB0D20E05}"))
audDelayEffectXenon : public audXAPOBase<audDelayEffectXenon,tDelayEffectSettings>
{
    public:
      
        audDelayEffectXenon();
        virtual bool Init(const Effect *metadata);

    private:

		void DoProcess(const tDelayEffectSettings& params, f32* __restrict pData, u32 cFrames, u32 cChannels);
		void OnSetParameters( const tDelayEffectSettings& params);

		tDelayEffectSettings m_Settings; //Consists of:
		//u32 TimeMs[g_MaxOutputChannels];
		//f32 GainDb[g_MaxOutputChannels];
		//f32 FeedbackGainDb[g_MaxOutputChannels];

		u32 m_DelayBufferWriteOffset;
		u32 m_DelaySamples[g_MaxOutputChannels];
		f32 m_GainLin[g_MaxOutputChannels];
		f32 m_FeedbackGainLin[g_MaxOutputChannels];
		f32 *m_DelayBuffer;
		u32 m_MaxDelaySamples;
		u32 m_NumChannelsToProcess;
		u8 m_ChannelMask;
};

} // namespace rage

#endif //__DELAYEFFECT_XENON_H

#endif // __XENON
