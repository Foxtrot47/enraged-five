// 
// audioeffecttypes/delayeffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __DELAYEFFECT_H
#define __DELAYEFFECT_H

#include "filterdefs.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"
#include "audiosynth/blocks.h"

#define AUD_DELAYEFFECT4_V8 0

namespace rage 
{

#define S16_DELAY_BUFFER 1
#if S16_DELAY_BUFFER
	typedef s16 delayBufferSample;
#else
	typedef f32 delayBufferSample;
#endif


#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif
	//
	// PURPOSE
	//  A simple delay effect.
	//
	class audDelayEffect : public audDspEffect
	{
	public:
		audDelayEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
	
		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		enum audDelayEffectParams
		{
			FrontLeftGain = audEffectParamIds::DelayEffect,
			FrontRightGain,
			FrontCentreGain,			
			RearLeftGain,
			RearRightGain,

			FrontLeftDelayTime,
			FrontRightDelayTime,
			FrontCentreDelayTime,
			RearLeftDelayTime,
			RearRightDelayTime,

			FrontLeftFeedback,
			FrontRightFeedback,
			FrontCentreFeedback,
			RearLeftFeedback,
			RearRightFeedback,

			FrontLeftDryMix,
			FrontRightDryMix,
			FrontCentreDryMix,
			RearLeftDryMix,
			RearRightDryMix,

			Bypass,
		};

	private:
		// gain and feedback in linear terms
		Vec::Vector_4V m_ChannelScalars[g_MaxOutputChannels];

		u32 m_DelayBufferWriteOffset;
		u32 m_DelaySamples[g_MaxOutputChannels];

		delayBufferSample *m_DelayBuffer;

		u32 m_MaxDelaySamples;
		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;
		bool m_Bypass;
		PS3_ONLY(bool m_PrintedSpareScratch);
	};

	class audDelayEffect4 : public audDspEffect
	{
	public:
		audDelayEffect4();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		enum audDelayEffect4Params
		{
			DelayTime = audEffectParamIds::DelayEffect,
			Feedback,

			FrontLeftFeedback,
			FrontRightFeedback,
			RearLeftFeedback,
			RearRightFeedback,
						
			Bypass,
		};

	private:

		void ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples);
		
		Vec::Vector_4V m_Feedback;
#if AUD_DELAYEFFECT4_V8
		revSimpleDelay_V8 m_DelayLine;
#else
		revSimpleDelay_V4 m_DelayLine;
#endif
		bool m_Bypass;
		PS3_ONLY(bool m_PrintedSpareScratch);
	};

#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //__DELAYEFFECT_H

