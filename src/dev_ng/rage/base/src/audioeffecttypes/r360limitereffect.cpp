// 
// audioeffecttypes/r360limitereffect.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "r360limitereffect.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/device.h"
#include "audiohardware/mixer.h"


namespace rage {

	audR360LimiterEffect::audR360LimiterEffect() : audDspEffect(AUD_DSPEFFECT_R360LIMITER)
	{

	}

	bool audR360LimiterEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		m_NumInputChannels = (u8)numChannels;
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i=0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		m_uCurrentSample = 0;

		// Linear versions of gain and threshold
		m_fInputClipLin				= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.InputClipDb);
		m_fOutputCeilingLin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.OutCeilingDb);
		m_fThresholdLin				= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.ThresholdDb);
		m_fCurrentGain				= 1.0f;
		m_fTargetGain				= 1.0f;
		m_fGainRate					= 0.0f;
		m_fRmsThresholdMin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMin) * m_fThresholdLin;
		m_fRmsThresholdMax			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMax) * m_fThresholdLin;
		m_fOneOverRmsThresholdDelta	= 1.0f / (m_fRmsThresholdMax-m_fRmsThresholdMin);

		// Release time calculations
		m_fReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / kMixerNativeSampleRate) / m_Settings.Release);	//	Release is in ms

		sysMemSet(&m_DelayBuffer[0][0],0,sizeof(m_DelayBuffer));

		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audR360LimiterEffect)
	{
		typedef f32 R360OutputBufferType[g_MaxOutputChannels][kMixBufNumSamples];

#if __SPU
		audAutoScratchBookmark autoScratch;
		R360OutputBufferType& r360OutputBuffer = *( (R360OutputBufferType*) AllocateFromScratch( sizeof(f32) * g_MaxOutputChannels * kMixBufNumSamples, 128, "R360OutputBuffer" ) );
#else
		R360OutputBufferType& r360OutputBuffer = *( (R360OutputBufferType*) AllocaAligned( f32, g_MaxOutputChannels * kMixBufNumSamples, 128 ) );
#endif

		if (!m_Settings.Bypass)
		{
			const u32 numChannels = m_NumChannelsToProcess;
			u8 channelIndices[g_MaxOutputChannels];
			for (u32 inputChannelIndex = 0; inputChannelIndex < numChannels; ++inputChannelIndex)
			{
				if(m_ChannelMask & (1 << inputChannelIndex))
				{
					Assign(channelIndices[inputChannelIndex], inputChannelIndex);
				}
			}

			for(u32 curSample = 0; curSample < kMixBufNumSamples; curSample++)
			{
				if (!m_RunningTargets.IsEmpty() && m_uCurrentSample == m_RunningTargets.Top().m_uSampleTarget)
				{
					Assert(m_uCurrentSample <= m_RunningTargets.Top().m_uSampleTarget);
					Assert(m_RunningTargets.Top().m_fGainTarget == m_fTargetGain);
					//m_fGainRate = 0.0f;
					m_fCurrentGain = m_fTargetGain;
					m_RunningTargets.Drop();
				}

				//	Compute the gain for the next delayed sample
				else if (m_fGainRate > 0.0f)
				{
					m_fCurrentGain += m_fGainRate;
					if (m_fCurrentGain >= m_fTargetGain)
						m_fCurrentGain = m_fTargetGain;
				}
				else if (m_fGainRate < 0.0f)
				{
					m_fCurrentGain += m_fGainRate;
					if (m_fCurrentGain <= m_fTargetGain)
						m_fCurrentGain = m_fTargetGain;
				}


				f32 fHiPeak = 0.0f;
				for(u32 iIndex=0; iIndex < numChannels; iIndex++)
				{
					const u32 inputChannelIndex = channelIndices[iIndex];

					if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId)
					{
						f32* inputData = buffer.ChannelBuffers[inputChannelIndex];
						const f32 fCurrentSample = inputData[curSample];

						//	Clip the input sample to our specified range
						inputData[curSample] = Clamp(fCurrentSample, -m_fInputClipLin, m_fInputClipLin);

						const f32 fDelayedSample = (curSample < kR360DelayLength ? m_DelayBuffer[inputChannelIndex][curSample] : inputData[curSample - kR360DelayLength]);

						r360OutputBuffer[inputChannelIndex][curSample] = (m_fCurrentGain * fDelayedSample * m_fOutputCeilingLin) / m_fThresholdLin;
						Assert(r360OutputBuffer[inputChannelIndex][curSample] <= 1.0f);

						//	Check the highest peak for each channel 64 samples in the future
						fHiPeak = Max(fHiPeak, Abs(fCurrentSample));
					}
				}

				const f32 fRMS = m_RmsCalculator.AddValue(fHiPeak);
				f32 fHiReleaseRate;
				if (m_Settings.AutoReleaseMode)
				{
					const f32 fAutoReleaseScalar = Clamp((fRMS-m_fRmsThresholdMin) * m_fOneOverRmsThresholdDelta, 0.0f, 1.0f);
					m_Settings.Release = Lerp(fAutoReleaseScalar, m_Settings.AutoReleaseMin, m_Settings.AutoReleaseMax);
					m_fReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / kMixerNativeSampleRate) / m_Settings.Release);	//	Release is in ms
					fHiReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / kMixerNativeSampleRate) / m_Settings.AutoReleaseMin);	//	Release is in ms
				}
				else
					fHiReleaseRate = m_fReleaseRate;

				//	Check every sample in the queue ahead of this one, and find the steepest descending gain rate
				if (fHiPeak > m_fThresholdLin)
				{
					const audR360LimiterEffect::tTargetGain newTarget = { m_fThresholdLin/fHiPeak, m_uCurrentSample+kR360DelayLength };
					m_RunningTargets.Push(newTarget);
				}

				int iSteepestSlopeIndex = 0;
				m_fGainRate = fHiReleaseRate;
				for (int i = 0; i < m_RunningTargets.GetCount(); ++i)
				{
					const audR360LimiterEffect::tTargetGain& rThisTarget = m_RunningTargets[i];

					const f32 fThisGainRate = rThisTarget.CalculateGainSlope(m_fCurrentGain, m_uCurrentSample);
					if (fThisGainRate <= m_fGainRate)
					{
						m_fGainRate = fThisGainRate;
						m_fTargetGain = rThisTarget.m_fGainTarget;
						iSteepestSlopeIndex = i;
					}
				}

				if (m_fGainRate == fHiReleaseRate)
				{
					m_RunningTargets.Reset();
					m_fGainRate = m_fReleaseRate;
					m_fTargetGain = 1.0f;
				}
				else
				{
					for (int i = 0; i < iSteepestSlopeIndex; ++i)
						m_RunningTargets.Drop();
				}

				++m_uCurrentSample;
			}

			for(u32 iIndex=0; iIndex < numChannels; iIndex++)
			{
				const u32 inputChannelIndex = channelIndices[iIndex];

				if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId)
				{
					f32* inputData = buffer.ChannelBuffers[inputChannelIndex];

					// populate delay line with last N samples from this buffer
					sysMemCpy(&m_DelayBuffer[inputChannelIndex], &inputData[kMixBufNumSamples - kR360DelayLength], kR360DelayLength * sizeof(f32));
					// overwrite the output
					sysMemCpy(inputData, &r360OutputBuffer[inputChannelIndex], kMixBufNumSamples * sizeof(f32));
				}
			}
		}
	}

	void audR360LimiterEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case audR360LimiterEffect::Bypass:
			m_Settings.Bypass = (value != 0);
			break;
		case audR360LimiterEffect::AutoReleaseMode:
			m_Settings.AutoReleaseMode = (value != 0);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audR360LimiterEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audR360LimiterEffect::SetParam(const u32 paramId, const f32 value)
	{
		switch(paramId)
		{
		case audR360LimiterEffect::InputClipDb:
			m_Settings.InputClipDb = Clamp(value, R360_INPUTCLIP_MIN, R360_INPUTCLIP_MAX);
			m_fInputClipLin	= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.InputClipDb);
			break;
		case audR360LimiterEffect::ThresholdDb:
			m_Settings.ThresholdDb = Clamp(value, R360_THRESH_MIN, R360_THRESH_MAX);
			m_fThresholdLin = audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.ThresholdDb);
			break;
		case audR360LimiterEffect::OutCeilingDb:
			m_Settings.OutCeilingDb				= Clamp(value, R360_OUTCEILING_MIN, R360_OUTCEILING_MAX);
			m_fOutputCeilingLin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.OutCeilingDb);
			break;
		case audR360LimiterEffect::AutoReleaseMin:
			m_Settings.AutoReleaseMin			= Clamp(value, R360_RELEASE_MIN, R360_RELEASE_MAX);
			break;
		case audR360LimiterEffect::AutoReleaseMax:
			m_Settings.AutoReleaseMax			= Clamp(value, m_Settings.AutoReleaseMin, R360_RELEASE_MAX);
			break;
		case audR360LimiterEffect::AutoReleaseRmsThresMin:
			m_Settings.AutoReleaseRmsThresMin	= Clamp(value, R360_AUTORELEASE_RMS_THRES_MIN_MIN, R360_AUTORELEASE_RMS_THRES_MIN_MAX);
			break;
		case audR360LimiterEffect::AutoReleaseRmsThresMax:
			m_Settings.AutoReleaseRmsThresMax	= Clamp(value, m_Settings.AutoReleaseRmsThresMin, R360_AUTORELEASE_RMS_THRES_MAX_MAX);
			break;
		case audR360LimiterEffect::Release:
			if (!m_Settings.AutoReleaseMode)
			{
				m_Settings.Release = Clamp(value,	R360_RELEASE_MIN, R360_RELEASE_MAX);
				m_fReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / kMixerNativeSampleRate) / m_Settings.Release);	//	Release is in ms
			}
		default:
			audAssertf(false, "paramId %u is not a valid audR360LimiterEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}

		m_fRmsThresholdMin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMin) * m_fThresholdLin;
		m_fRmsThresholdMax			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMax) * m_fThresholdLin;	
		m_fOneOverRmsThresholdDelta	= 1.0f / (m_fRmsThresholdMax-m_fRmsThresholdMin);	
	}

	void audR360LimiterEffect::Shutdown(void)
	{
	}

} // namespace rage

