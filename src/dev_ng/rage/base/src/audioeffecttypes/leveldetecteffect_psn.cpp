#if __SPU
// 
// audioeffecttypes/leveldetecteffect_psn.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "psn_dsp.h"
#include "filterdefs.h"

#include "system/bit.h"

//	For vectorized code
#include "vectormath/classes.h"

#include <cell/dma.h>

#include "system/spu_library.cpp"

#define TAG			(6)

#define USE_VECTORIZED_LEVELDETECTION 1

using namespace rage;

/**********************************************************************************
DSPInit
Initialisation routine
This routine is called by MultiStream when the DSP effect is first called.

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
paramList		Address of static parameter data (248 bytes)
0-127		= Read Only (example: users input parameters)
128-247		= Read / Write (example: DSP effects internal parameters)
workArea		Work area available for DSP effects (40 K)
Any DMA from main RAM must fit into this area
dspInfo			Contains information as to the slot number to be processed, input / output bitmasks

Notes:
Each DSP effect has 248 bytes of RAM available.
If more RAM is required, this area can be used to store a pointer to the actual data.
The data can then be transferred from main RAM as required.
Splitting the paramList into a read and a read/write section allows us to make sure that if
the user is modifying parameters via PPU commands (example: distortion level), there is no conflicts
between the SPU updating parameter memory at the same time.

**********************************************************************************/
void DSPInit(char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{
}

//
u32 GetChannelIndex(const u8 msMask)
{
	u8 flag = 1;
	for (u32 i=0; i<8; ++i)
	{
		if (msMask & flag)
		{
			return i;
		}
		flag <<= 1;
	}

	return ~0U;
}

/**********************************************************************************
DSPApply
Apply DSP effect routine
This routine is called by MultiStream to apply a DSP effect to an input signal

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
input			Address of input data (Time Domain float32 data OR Frequency Domain FFT data)
For PCM, 1024 samples are passed in.
For FFT, 512 frequency bands are passed in.

output			Address to write output data to

paramList		Address of static parameter data (248 bytes)
0-127 = Read Only
128-247 = Read / Write

workArea		Work area available for DSP effects (40 K)
Any DMA from main RAM must fit into this area

dspInfo			Contains information as to the slot number to be processed, input / output bitmasks..

Returns:
0				DSP will complete when stream finishes
!0				DSP will stay active after stream finishes (for delays / reverbs)

Notes:
For time domain input data, the user must process all 1024 samples.
But note that any parameters used for modifying data must be preserved after 512 samples.
This is due to windowing within MultiStream.

**********************************************************************************/
int DSPApply(void* input, void* output, char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{	
	tLevelDetectSPUParamData *pReadParams = (tLevelDetectSPUParamData*)paramList;
	tLevelDetectSPUParamData *pReadWriteParams = (tLevelDetectSPUParamData*)(paramList+128);

	//////////////////////////////////////////////////////////////////////////
	// dma across the work data
	uint64_t ea = (uint64_t)pReadParams->EA;

	if(ea == 0)
	{
		//This effect has not yet been initialized on the PPU, so early-out.
		return 0;
	}

	//Ensure that all previous effect DMAs have completed before we start.
	cellDmaWaitTagStatusAll(1 << TAG);

	cellDmaGet(workArea, ea, sizeof(tLevelDetectSPUData), TAG, 0, 0);
	cellDmaWaitTagStatusAll(1<<TAG);
	dspInfo->bandwidthIn += sizeof(tLevelDetectSPUData);
	tLevelDetectSPUData* pWorkData = (tLevelDetectSPUData*)workArea;

	const int inputChannelIndex = (int)(dspInfo->callNumber == -1 ? 0 : GetChannelIndex(dspInfo->inMask));


	f32 *samples = (f32*)input;
	f32 *outSamples = (f32*)output;

	//////////////////////////////////////////////////////////////////////////
	// only perform DSP if our metadata masks match
	if ( (dspInfo->callNumber != -1) && (!(dspInfo->inMask & pWorkData->m_ChannelMask) || inputChannelIndex > (int)g_MaxOutputChannels) )
	{
		//Copy input buffer into output buffer - bypass mode.
		for(int cnt = 0; cnt<FRAME_SAMPLES; cnt+=16)
		{
			*((Vec4V*)&outSamples[cnt+0]) = *((Vec4V*)&samples[cnt+0]);
			*((Vec4V*)&outSamples[cnt+4]) = *((Vec4V*)&samples[cnt+4]);
			*((Vec4V*)&outSamples[cnt+8]) = *((Vec4V*)&samples[cnt+8]);
			*((Vec4V*)&outSamples[cnt+12]) = *((Vec4V*)&samples[cnt+12]);
		}
	}
	else
	{
		if (BIT_ISSET(inputChannelIndex, pWorkData->m_ClearMaxLevelsMask))
			pWorkData->m_Values.m_MaxLevels[inputChannelIndex] = 0.0f;

		f32 maxInputSample = 0.0f;
		f32 rmsSquaredSum = 0.0f;

#if USE_VECTORIZED_LEVELDETECTION // This block takes 0.58 microseconds to complete
		Vec4V vInputSamples;
		Vec4V vMax(V_ZERO);
		ScalarV vSquaredSums(V_ZERO);
		for(u32 cnt = 0; cnt < FRAME_SAMPLES; cnt+=16)
		{
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((Vec4V*)&samples[cnt+0]);
			vMax = VMath::AoS::Max( VMath::AoS::Abs(vInputSamples), vMax);	// Computes 4 maxes
			vSquaredSums += VMath::AoS::MagSquared(vInputSamples);			// Adds sum of the squares of the 4 elements
			*((Vec4V*)&outSamples[cnt+0]) = vInputSamples;					// Apply output gain
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((Vec4V*)&samples[cnt+4]);
			vMax = VMath::AoS::Max( VMath::AoS::Abs(vInputSamples), vMax);	// Computes 4 maxes
			vSquaredSums += VMath::AoS::MagSquared(vInputSamples);			// Adds sum of the squares of the 4 elements
			*((Vec4V*)&outSamples[cnt+4]) = vInputSamples;					// Apply output gain
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((Vec4V*)&samples[cnt+8]);
			vMax = VMath::AoS::Max( VMath::AoS::Abs(vInputSamples), vMax);	// Computes 4 maxes
			vSquaredSums += VMath::AoS::MagSquared(vInputSamples);			// Adds sum of the squares of the 4 elements
			*((Vec4V*)&outSamples[cnt+8]) = vInputSamples;					// Apply output gain
			///////////////////////////////////////////////////////////////////////////
			vInputSamples = *((Vec4V*)&samples[cnt+12]);
			vMax = VMath::AoS::Max( VMath::AoS::Abs(vInputSamples), vMax);	// Computes 4 maxes
			vSquaredSums += VMath::AoS::MagSquared(vInputSamples);			// Adds sum of the squares of the 4 elements
			*((Vec4V*)&outSamples[cnt+12]) = vInputSamples;					// Apply output gain
			///////////////////////////////////////////////////////////////////////////
		}

		//	Store the highest 4 values in memory and avoid converting them to floats until we need them later
		//m_MaxLevels[inputChannelIndex] = VMath::AoS::Max(m_MaxLevels[inputChannelIndex], vMax);
		maxInputSample = Max(vMax.GetXf(), vMax.GetYf(), vMax.GetZf(), vMax.GetWf());

		//	Sum up the squares of each input value
		//m_RmsSquaredSumHistory[inputChannelIndex] += vSquaredSums;
		rmsSquaredSum = vSquaredSums.Getf();
#else // USE_VECTORIZED_LEVELDETECTION This block takes 3.2 microseconds to complete
		for (u32 cnt=0; cnt<FRAME_SAMPLES; cnt++)
		{
			const f32 inputSample = (*samples);

			//	Store the max value
			maxInputSample = Max(Abs(inputSample), maxInputSample);

			//	Sum up the squares of each input value
			rmsSquaredSum += square(inputSample);

			// apply output gain
			outSamples[cnt] = inputSample;
			*samples++;
		}
#endif // USE_VECTORIZED_LEVELDETECTION

		pWorkData->m_Values.m_MaxLevels[inputChannelIndex] = Max(maxInputSample, pWorkData->m_Values.m_MaxLevels[inputChannelIndex]);
		pWorkData->m_ClearMaxLevelsMask = BIT_CLEAR(inputChannelIndex, pWorkData->m_ClearMaxLevelsMask);
		pReadWriteParams->m_RmsSquaredSumHistory[inputChannelIndex] += rmsSquaredSum;

		if (inputChannelIndex<=pReadWriteParams->m_LastChannelProcessedIndex)
		{
			if (++pReadWriteParams->m_RmsHistoryIndex == LEVELDETECT_RMS_WINDOW_FRAMES)
			{
				pReadWriteParams->m_RmsHistoryIndex = 0;

				//	RMS is the square root of the average of the input samples over the number of window frames we specified
				for(u32 i=0; i<NUM_PLATFORM_SPEAKER_IDS; i++)
				{
					pWorkData->m_Values.m_RmsLevels[i] = Sqrtf(pReadWriteParams->m_RmsSquaredSumHistory[i] / (FRAME_SAMPLES*LEVELDETECT_RMS_WINDOW_FRAMES));
					pReadWriteParams->m_RmsSquaredSumHistory[i] = 0.0f; // Zero this accumulator out for the next run
				}
			}
		}

		//	Store the index of the last channel we processed
		pReadWriteParams->m_LastChannelProcessedIndex = inputChannelIndex;


		//////////////////////////////////////////////////////////////////////////
		// DMA the new peak levels back to main memory
		cellDmaPut(workArea, ea, sizeof(tLevelDetectSPUData), TAG, 0, 0);
		dspInfo->bandwidthOut += sizeof(tLevelDetectSPUData);
	}

	// If the stream ends and (and if this DSP effect is attached to a stream), end the DSP effect too..
	return 0;
}

/**********************************************************************************
DSPPlugInSetup
Setup as the entry point function within the DSP makefile
Initialises the DSP effect functions.
Called by MultiStream.

Requires:
plugInStruct		Address of a DSPPlugin structure

Notes:
This function sets the address of both the Init and the Apply functions.
It also specifies if the input / output data is required as time domain or FFT data
**********************************************************************************/
void DSPPlugInSetup(DSPPlugin* plugInStruct)
{
	plugInStruct->DSPInit = (ms_fnct)DSPInit;			// Init function (or NULL if no init function required)
	plugInStruct->DSPApply = (ms_fnct)DSPApply;			// DSP "Apply" function (processes DSP effect)
	plugInStruct->Domain = MS_DSP_TIMEBASE;				// DSP Effect is Frequency based (complex numbers)
}


#endif	// __SPU