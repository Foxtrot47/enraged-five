// 
// audioeffecttypes/ereffect.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "ereffect.h"
#include "audiodata/simpletypes.h"
#include "audiohardware/channel.h"
#include "audiohardware/config.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/framebufferpool.h"
#include "system/cache.h"
#include "system/dma.h"
#include "system/memops.h"
#include "system/performancetimer.h"

using namespace ravesimpletypes;

namespace rage 
{
	using namespace Vec;
	audEarlyReflectionEffect::audEarlyReflectionEffect() 
		: audDspEffect(AUD_DSPEFFECT_EARLY_REFLECTIONS)
		, m_Settings(NULL)
		, m_CurrentSettingsHash(0)
		, m_RoomSize(0.f)		
	{

	}

	audEarlyReflectionEffect::~audEarlyReflectionEffect()
	{

	}


	bool audEarlyReflectionEffect::Init(const u32 ASSERT_ONLY(numChannels), const u32 ASSERT_ONLY(channelMask))
	{
		Assert(numChannels == 4);
		Assert(channelMask == (RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT));

		SetProcessFormat(Interleaved);

		// 11kHz
		//0x462BE000
		// 11.5kHz
		//0x4633B000
		// 9kHz
		//0x460CA000
		// 8kHz
		//0x45FA0000
		m_AntiImaging.Set4PoleLPF(V4VConstant<0x45FA0000,0x45FA0000,0x45FA0000,0x45FA0000>(), V4VConstant(V_ONE));

		m_Bypass = false;
		
		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audEarlyReflectionEffect)
	{
		if(m_Bypass || !m_Settings)
		{
			return;
		}

		if (buffer.ChannelBufferIds[0] >= audFrameBufferPool::InvalidId || buffer.ChannelBufferIds[1] >= audFrameBufferPool::InvalidId ||
			buffer.ChannelBufferIds[2] >= audFrameBufferPool::InvalidId || buffer.ChannelBufferIds[3] >= audFrameBufferPool::InvalidId)
		{
			return;
		}

#if AUD_TIME_DSP
	sysPerformanceTimer timer("");
	timer.Start();
#endif

		Assert(buffer.Format == Interleaved);

#if __SPU
		const s32 tag = 4;
		const u32 localBufferSizeSamples = AUD_ER_24kHz && 0 ? kMixBufNumSamples >> 1 : kMixBufNumSamples;
		for(s32 d = 0; d < kNumNodes; d++)
		{
			m_1stOrderDelays[d].Prefetch(localBufferSizeSamples, tag);
			m_2ndOrderDelays[d].Prefetch(localBufferSizeSamples, tag);
			m_3rdOrderDelays[d].Prefetch(localBufferSizeSamples, tag);
		}
		for(s32 d = 0; d < kNumAllpasses; d++)
		{
			m_Allpasses[d].Prefetch(localBufferSizeSamples, tag);
		}

		sysDmaWait(1<<tag);
#endif

#if AUD_ER_24kHz
		// Decimate by a factor of two (average the two samples)
		const u32 numSamplesPerPass = kMixBufNumSamples / 8;
		
		Vector_4V *const inOut0 = reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[0]);
		Vector_4V *const inOut1 = reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[1]);
		Vector_4V *const inOut2 = reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[2]);
		Vector_4V *const inOut3 = reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[3]);

#define AUD_ER_ANTIALIAS 0
#if AUD_ER_ANTIALIAS
		for(u32 i = 0; i < numSamplesPerPass; i++)
		{
			inOut0[i] = m_AntiAlias.Process(V4Scale(V4VConstant(V_HALF), V4Add(inOut0[i*2], inOut0[i*2+1])));// m_AntiAlias.Process(inOut0[i*2+1]);
		}
		for(u32 i = 0; i < numSamplesPerPass; i++)
		{
			//inOut1[i] = m_AntiAlias.Process(inOut1[i*2]); m_AntiAlias.Process(inOut1[i*2+1]);
			inOut1[i] = m_AntiAlias.Process(V4Scale(V4VConstant(V_HALF), V4Add(inOut1[i*2], inOut1[i*2+1])));
		}
		for(u32 i = 0; i < numSamplesPerPass; i++)
		{
			//inOut2[i] = m_AntiAlias.Process(inOut2[i*2]); m_AntiAlias.Process(inOut2[i*2+1]);
			inOut2[i] = m_AntiAlias.Process(V4Scale(V4VConstant(V_HALF), V4Add(inOut2[i*2], inOut2[i*2+1])));
		}
		for(u32 i = 0; i < numSamplesPerPass; i++)
		{
			//inOut3[i] = m_AntiAlias.Process(inOut3[i*2]); m_AntiAlias.Process(inOut3[i*2+1]);
			inOut3[i] = m_AntiAlias.Process(V4Scale(V4VConstant(V_HALF), V4Add(inOut3[i*2], inOut3[i*2+1])));
		}
#else
	for(u32 i = 0; i < numSamplesPerPass; i++)
	{
		inOut0[i] = V4Scale(V4VConstant(V_HALF), V4Add(inOut0[i*2], inOut0[i*2+1]));
		inOut1[i] = V4Scale(V4VConstant(V_HALF), V4Add(inOut1[i*2], inOut1[i*2+1]));
		inOut2[i] = V4Scale(V4VConstant(V_HALF), V4Add(inOut2[i*2], inOut2[i*2+1]));
		inOut3[i] = V4Scale(V4VConstant(V_HALF), V4Add(inOut3[i*2], inOut3[i*2+1]));
	}
#endif
		Vector_4V downsampledBuffer[numSamplesPerPass];

		ProcessInterleaved(buffer.ChannelBuffers[0], (float*)&downsampledBuffer, numSamplesPerPass);
		Upsample(downsampledBuffer, reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[0]), numSamplesPerPass);
		ProcessInterleaved(buffer.ChannelBuffers[1], (float*)&downsampledBuffer, numSamplesPerPass);
		Upsample(downsampledBuffer, reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[1]), numSamplesPerPass);
		ProcessInterleaved(buffer.ChannelBuffers[2], (float*)&downsampledBuffer, numSamplesPerPass);
		Upsample(downsampledBuffer, reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[2]), numSamplesPerPass);
		ProcessInterleaved(buffer.ChannelBuffers[3], (float*)&downsampledBuffer, numSamplesPerPass);
		Upsample(downsampledBuffer, reinterpret_cast<Vector_4V*>(buffer.ChannelBuffers[3]), numSamplesPerPass);
#else
		const u32 numSamplesPerPass = kMixBufNumSamples / 4;

		ProcessInterleaved(buffer.ChannelBuffers[0], buffer.ChannelBuffers[0], numSamplesPerPass);
		ProcessInterleaved(buffer.ChannelBuffers[1], buffer.ChannelBuffers[1], numSamplesPerPass);
		ProcessInterleaved(buffer.ChannelBuffers[2], buffer.ChannelBuffers[2], numSamplesPerPass);
		ProcessInterleaved(buffer.ChannelBuffers[3], buffer.ChannelBuffers[3], numSamplesPerPass);
#endif

#if __SPU
		for(s32 d = 0; d < kNumNodes; d++)
		{
			m_1stOrderDelays[d].Writeback(tag);
			m_2ndOrderDelays[d].Writeback(tag);
			m_3rdOrderDelays[d].Writeback(tag);
		}
		for(s32 d = 0; d < kNumAllpasses; d++)
		{
			m_Allpasses[d].Writeback(tag);
		}

		sysDmaWait(1<<tag);
#endif

#if AUD_TIME_DSP
		timer.Stop();
		sm_ProcessTime[AUD_DSPEFFECT_EARLY_REFLECTIONS] = timer.GetTimeMS() * 1000.f;
#endif		
	}

	void audEarlyReflectionEffect::Upsample(const Vec::Vector_4V *input, Vec::Vector_4V *output, const u32 numSamples)
	{
		const Vector_4V *inPtr = input;
		Vector_4V *outPtr = output;

		u32 count = numSamples;
		while(count--)
		{
			// Multiply by upsample-ratio, zero-pad and filter
			Vector_4V inputSample = V4Scale(V4VConstant(V_TWO), *inPtr++);
			*outPtr++ = m_AntiImaging.Process(inputSample);
			*outPtr++ = m_AntiImaging.Process(V4VConstant(V_ZERO));
		}
	}

	void audEarlyReflectionEffect::ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples)
	{
		const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outBuffer);

		// Processing two samples at a time
		u32 count = numSamples >> 1;

		while(count--)
		{
			Vector_4V input0 = *inputPtr++;
			Vector_4V input1 = *inputPtr++;

			for(int d = 0; d < kNumAllpasses; d++)
			{
				m_Allpasses[d].Process(input0, input1);
			}

			Vector_4V er0 = V4VConstant(V_ZERO);
			Vector_4V er1 = V4VConstant(V_ZERO);
			
			Vector_4V d1_0 = V4VConstant(V_ZERO);
			Vector_4V d2_0 = V4VConstant(V_ZERO);
			Vector_4V d3_0 = V4VConstant(V_ZERO);

			Vector_4V d1_1 = V4VConstant(V_ZERO);
			Vector_4V d2_1 = V4VConstant(V_ZERO);
			Vector_4V d3_1 = V4VConstant(V_ZERO);

			// sum each order delays
			for(s32 d = 0; d < kNumNodes; d++)
			{
				Vector_4V x0, x1;

				m_1stOrderDelays[d].Read(x0, x1);
				d1_0 = V4Add(d1_0, x0);
				d1_1 = V4Add(d1_1, x1);

				m_2ndOrderDelays[d].Read(x0, x1);
				d2_0 = V4Add(d2_0, x0);
				d2_1 = V4Add(d2_1, x1);

				m_3rdOrderDelays[d].Read(x0, x1);
				d3_0 = V4Add(d3_0, x0);
				d3_1 = V4Add(d3_1, x1);
			}

			Vector_4V input0PlusD2D3 = V4Add(V4Add(input0, d2_0), d3_0);
			Vector_4V input0PlusD1D3 = V4Add(V4Add(input0, d1_0), d3_0);
			Vector_4V input0PlusD1D2 = V4Add(V4Add(input0, d1_0), d2_0);

			Vector_4V input1PlusD2D3 = V4Add(V4Add(input1, d2_1), d3_1);
			Vector_4V input1PlusD1D3 = V4Add(V4Add(input1, d1_1), d3_1);
			Vector_4V input1PlusD1D2 = V4Add(V4Add(input1, d1_1), d2_1);
						

			for(s32 d = 0; d < kNumNodes; d++)
			{
				Vector_4V x0 = V4VConstant(V_ZERO), x1 = V4VConstant(V_ZERO);

				x0 = input0PlusD2D3;
				x1 = input1PlusD2D3;

				m_1stOrderDelays[d].Process(x0, x1);
				
				er0 = V4Add(er0, x0);
				er1 = V4Add(er1, x1);

				x0 = input0PlusD1D3;
				x1 = input1PlusD1D3;

				m_2ndOrderDelays[d].Process(x0, x1);				
				x0 = m_2ndOrderLPFs[d](x0);
				x1 = m_2ndOrderLPFs[d](x1);

				er0 = V4AddScaled(er0, x0, m_2ndOrderGain);
				er1 = V4AddScaled(er1, x1, m_2ndOrderGain);

				x0 = input0PlusD1D2;
				x1 = input1PlusD1D2;

				m_3rdOrderDelays[d].Process(x0, x1);
				x0 = m_3rdOrderLPFs[d](x0);
				x1 = m_3rdOrderLPFs[d](x1);

				er0 = V4AddScaled(er0, x0, m_3rdOrderGain);
				er1 = V4AddScaled(er1, x1, m_3rdOrderGain);
			}

			*outputPtr++ = er0;
			*outputPtr++ = er1;
		}
	}

#if !__SPU

	void audEarlyReflectionEffect::SetupRoom(const ERSettings *settings)
	{
		if(settings == NULL)
		{
			m_Settings = NULL;
			return;
		}

		bool resetDelays = false;

		if(m_Settings == NULL || m_RoomSize != settings->RoomSize)
		{
			resetDelays = true;		
		}

		const float sampleRate = float(kERSampleRate);

		audAssertf(kNumAllpasses == settings->numAllpasses, "Only %u allpasses in settings, should be %u", settings->numAllpasses, kNumAllpasses);
				
		for(s32 i = 0; i < kNumAllpasses; i++)
		{
			if(resetDelays)
			{
				// Temporarily store/edit allpass length as 2x to allow easy switching
#if AUD_ER_24kHz
				m_Allpasses[i].SetLength(settings->Allpasses[i].DelayLength >> 1);
#else
				m_Allpasses[i].SetLength(settings->Allpasses[i].DelayLength);
#endif
			}
			m_Allpasses[i].SetDiffusion(V4LoadScalar32IntoSplatted(settings->Allpasses[i].Diffusion));
		}
				
		m_2ndOrderGain = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->Gain_2ndOrder.FL));
		m_3rdOrderGain = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->Gain_3rdOrder.FL));

		audAssertf(kNumNodes == settings->numNodeLPF_1stOrders, "Only %u 1st order LPF in settings, should be %u", settings->numNodeLPF_1stOrders, kNumNodes);
		audAssertf(kNumNodes == settings->numNodeLPF_2ndOrders, "Only %u 2nd order LPF in settings, should be %u", settings->numNodeLPF_2ndOrders, kNumNodes);
		audAssertf(kNumNodes == settings->numNodeLPF_3rdOrders, "Only %u 3rd order LPF in settings, should be %u", settings->numNodeLPF_3rdOrders, kNumNodes);

		for(s32 i = 0; i < kNumNodes; i++)
		{
			m_2ndOrderLPFs[i].SetFc(V4LoadUnaligned(&settings->NodeLPF_2ndOrder[i].FL_Cutoff));
			m_3rdOrderLPFs[i].SetFc(V4LoadUnaligned(&settings->NodeLPF_3rdOrder[i].FL_Cutoff));
		}

		const float speedOfSound = 340.29f; // m/s

		/* Louden
		const float wR = 1.36f;
		const float hR = 0.71f;
		

		// golden ratio
		const float wR = 1.62f;
		const float hR = 0.62f;
		*/

		const float wR = settings->RoomDimensions.widthRatio;
		const float hR = settings->RoomDimensions.heightRatio;
		const float lR = settings->RoomDimensions.lengthRatio;

		const float roomWidth = wR * settings->RoomSize;
		const float roomHeight = hR * settings->RoomSize;
		const float roomLength = lR * settings->RoomSize;

		/*float listX = 0.13f;
		float listY = 0.59f;
		float listZ = 0.73f;

		float listX = 0.23f;
		float listY = 0.59f;
		float listZ = 0.73f;*/

		const float listX = settings->ListenerPos.x;
		const float listY = settings->ListenerPos.y;
		const float listZ = settings->ListenerPos.z;

		const float distX_1 = 2.f * listX * roomWidth;
		const float distX_2 = 2.f * (1.f - listX) * roomWidth;
		const float distY_1 = 2.f * listY * roomHeight;
		const float distY_2 = 2.f * (1.f - listY) * roomHeight;
		const float distZ_1 = 2.f * listZ * roomLength;
		const float distZ_2 = 2.f * (1.f - listZ) * roomLength;

		const Vector_4V gainX1 = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->NodeGainMatrix.X1.FL));
		const Vector_4V gainX2 = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->NodeGainMatrix.X1.FL));
		const Vector_4V gainY1 = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->NodeGainMatrix.Y1.FL));
		const Vector_4V gainY2 = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->NodeGainMatrix.Y2.FL));
		const Vector_4V gainZ1 = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->NodeGainMatrix.Z1.FL));
		const Vector_4V gainZ2 = audDriverUtil::ComputeLinearVolumeFromDb(V4LoadUnaligned(&settings->NodeGainMatrix.Z2.FL));

		// First order room/listener
		if(resetDelays)
		{
			m_1stOrderDelays[0].SetLengthS(distX_1 / speedOfSound, sampleRate);		
			m_1stOrderDelays[1].SetLengthS(distX_2 / speedOfSound, sampleRate);
			m_1stOrderDelays[2].SetLengthS(distY_1 / speedOfSound, sampleRate);
			m_1stOrderDelays[3].SetLengthS(distY_2 / speedOfSound, sampleRate);
			m_1stOrderDelays[4].SetLengthS(distZ_1 / speedOfSound, sampleRate);
			m_1stOrderDelays[5].SetLengthS(distZ_2 / speedOfSound, sampleRate);
		}

#if 0
		float modSpeeds[kNumNodes] = {
			10.f,
			10.4f,
			15.32f,
			7.f,
			9.f,
			9.1f,
			//4.79f,
		};

		float modDepths[kNumNodes] = {
			0.1f,
			0.2f,
			0.5f,
			0.05f,
			0.3f,
			0.28f,		
		};

		for(int i = 0; i < kNumNodes; i++)
		{
			m_1stOrderDelays[i].SetModulationSpeed(modSpeeds[i]);
			m_1stOrderDelays[i].SetModulationDepth(Min(1.f,modDepths[i] * 3.f));
		}
#endif

		m_1stOrderDelays[0].SetGain(V4Negate(V4InvScale(gainX1, V4LoadScalar32IntoSplatted(distX_1))));
		m_1stOrderDelays[1].SetGain(V4InvScale(gainX2, V4LoadScalar32IntoSplatted(distX_2)));
		m_1stOrderDelays[2].SetGain(V4InvScale(gainY1, V4LoadScalar32IntoSplatted(distY_1)));
		m_1stOrderDelays[3].SetGain(V4Negate(V4InvScale(gainY2, V4LoadScalar32IntoSplatted(distY_2))));
		m_1stOrderDelays[4].SetGain(V4InvScale(gainZ1, V4LoadScalar32IntoSplatted(distZ_1)));
		m_1stOrderDelays[5].SetGain(V4Negate(V4InvScale(gainZ2, V4LoadScalar32IntoSplatted(distZ_2))));

		// arbitrary extra nodes

		// 70% of largest
		/*const float arbDelay = 0.7f * distX_2;
		m_1stOrderDelays[6].SetLengthS(arbDelay / speedOfSound);
		m_1stOrderDelays[6].SetGain(1.f / arbDelay);*/

		const float distX2nd_1 = distX_1 + distX_2;
		const float distX2nd_2 = distX_2 + distX_1;
		const float distY2nd_1 = distY_1 + distY_2;
		const float distY2nd_2 = distY_2 + distY_1;
		const float distZ2nd_1 = distZ_1 + distZ_2;
		const float distZ2nd_2 = distZ_2 + distZ_1;

		if(resetDelays)
		{
			m_2ndOrderDelays[0].SetLengthS(distX2nd_1 / speedOfSound, sampleRate);
			m_2ndOrderDelays[1].SetLengthS(distX2nd_2 / speedOfSound, sampleRate);
			m_2ndOrderDelays[2].SetLengthS(distY2nd_1 / speedOfSound, sampleRate);
			m_2ndOrderDelays[3].SetLengthS(distY2nd_2 / speedOfSound, sampleRate);
			m_2ndOrderDelays[4].SetLengthS(distZ2nd_1 / speedOfSound, sampleRate);
			m_2ndOrderDelays[5].SetLengthS(distZ2nd_2 / speedOfSound, sampleRate);
		}

		m_2ndOrderDelays[0].SetGain(V4Negate(V4InvScale(gainX1, V4LoadScalar32IntoSplatted(distX2nd_1))));
		m_2ndOrderDelays[1].SetGain(V4Negate(V4InvScale(gainX2, V4LoadScalar32IntoSplatted(distX2nd_2))));
		m_2ndOrderDelays[2].SetGain(V4InvScale(gainY1, V4LoadScalar32IntoSplatted(distY2nd_1)));	
		m_2ndOrderDelays[3].SetGain(V4InvScale(gainY2, V4LoadScalar32IntoSplatted(distY2nd_2)));
		m_2ndOrderDelays[4].SetGain(V4Negate(V4InvScale(gainZ1, V4LoadScalar32IntoSplatted(distZ2nd_1))));
		m_2ndOrderDelays[5].SetGain(V4InvScale(gainZ2, V4LoadScalar32IntoSplatted(distZ2nd_2)));

		// arbitrary extra nodes
		/*const float arb2Delay = 0.87f * distZ2nd_2;
		m_2ndOrderDelays[6].SetLengthS(arb2Delay / speedOfSound);
		m_2ndOrderDelays[6].SetGain(1.f / arb2Delay);*/


		const float distX3rd_1 = distX_1 * 2.f + distX_2;
		const float distX3rd_2 = distX_2 * 2.f + distX_1;
		const float distY3rd_1 = distY_1 * 2.f + distY_2;
		const float distY3rd_2 = distY_2 * 2.f + distY_1;
		const float distZ3rd_1 = distZ_1 * 2.f + distZ_2;
		const float distZ3rd_2 = distZ_2 * 2.f + distZ_1;

		if(resetDelays)
		{
			m_3rdOrderDelays[0].SetLengthS(distX3rd_1 / speedOfSound, sampleRate);
			m_3rdOrderDelays[1].SetLengthS(distX3rd_2 / speedOfSound, sampleRate);
			m_3rdOrderDelays[2].SetLengthS(distY3rd_1 / speedOfSound, sampleRate);
			m_3rdOrderDelays[3].SetLengthS(distY3rd_2 / speedOfSound, sampleRate);
			m_3rdOrderDelays[4].SetLengthS(distZ3rd_1 / speedOfSound, sampleRate);
			m_3rdOrderDelays[5].SetLengthS(distZ3rd_2 / speedOfSound, sampleRate);
		}

		m_3rdOrderDelays[0].SetGain(V4InvScale(gainX1, V4LoadScalar32IntoSplatted(distX3rd_1)));
		m_3rdOrderDelays[1].SetGain(V4Negate(V4InvScale(gainX2, V4LoadScalar32IntoSplatted(distX3rd_2))));
		m_3rdOrderDelays[2].SetGain(V4InvScale(gainY1, V4LoadScalar32IntoSplatted(distY3rd_1)));
		m_3rdOrderDelays[3].SetGain(V4Negate(V4InvScale(gainY2, V4LoadScalar32IntoSplatted(distY3rd_2))));
		m_3rdOrderDelays[4].SetGain(V4Negate(V4InvScale(gainZ1, V4LoadScalar32IntoSplatted(distZ3rd_1))));
		m_3rdOrderDelays[5].SetGain(V4InvScale(gainZ2, V4LoadScalar32IntoSplatted(distZ3rd_2)));

		// arbitrary extra nodes
		/*const float arb3Delay = 0.97f * distZ3rd_2;
		m_3rdOrderDelays[6].SetLengthS(arb3Delay / speedOfSound);
		m_3rdOrderDelays[6].SetGain(1.f / arb3Delay);*/

#if !__NO_OUTPUT
		if(resetDelays)
		{
			size_t sizeBytes = 0;
			for(s32 i = 0; i < kNumNodes; i++)
			{
				sizeBytes += m_1stOrderDelays[i].GetSizeBytes();
				sizeBytes += m_2ndOrderDelays[i].GetSizeBytes();
				sizeBytes += m_3rdOrderDelays[i].GetSizeBytes();		
			}

			for(s32 i = 0; i < kNumAllpasses; i++)
			{
				sizeBytes += m_Allpasses[i].GetSizeBytes();
			}

			audDisplayf("Using %u bytes for room size of %f (%f x %f x %f)", (u32)sizeBytes, settings->RoomSize, roomWidth, roomLength, roomHeight);
		}		
#endif

		m_Settings = settings;

		// Cache the current room size value so we can detect if it changes due to RAVE edits
		m_RoomSize = settings->RoomSize;
	}

	void audEarlyReflectionEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case Bypass:
			m_Bypass = (value != 0);
			break;
		case Settings:
			if(m_CurrentSettingsHash != value)
			{
				SetupRoom(audConfig::GetMetadataManager().GetObject<ERSettings>(value));
				m_CurrentSettingsHash = value;
			}
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audEarlyReflectionEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audEarlyReflectionEffect::SetParam(const u32 ASSERT_ONLY(paramId), const f32 ASSERT_ONLY(value))
	{
		audAssertf(false, "paramId %u is not a valid audEarlyReflectionEffect float param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
	}

	void audEarlyReflectionEffect::Shutdown(void)
	{
		// TODO: implement
	}
#endif // !__SPU

} // namespace rage


