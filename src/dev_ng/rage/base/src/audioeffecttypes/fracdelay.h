#ifndef AUD_FRACDELAY_H
#define AUD_FRACDELAY_H

#include "audiohardware/mixer.h"
#include "math/amath.h"

namespace rage
{

class audFractionalDelayLine
{
public:
	audFractionalDelayLine(const s32 bufferSize)
		: m_BufferSize(bufferSize)
		, m_Counter(0)
	{
		m_Buffer = new float[bufferSize];
		sysMemSet(m_Buffer, 0, sizeof(float) * bufferSize);
	}

	~audFractionalDelayLine()
	{
		delete[] m_Buffer;
	}

	float Process(const float in, const float feedback, const float delay)
	{
		const float output = Read(delay);

		// add to delay buffer with feedback
		m_Buffer[m_Counter] = in + output * feedback;

		m_Counter = (m_Counter+1)% m_BufferSize;

		return output;
	}

	void Feed(const float in)
	{
		m_Buffer[m_Counter] = in;
		m_Counter = (m_Counter+1)% m_BufferSize;
	}

	float Read(const float delay)
	{
		float fracOffset = (float)m_Counter - (delay*kMixerNativeSampleRate);

		// wrap lookback buffer-bound
		if(fracOffset < 0.0)
		{
			fracOffset = m_BufferSize + fracOffset;
		}

		// Interpolation mid sample
		const float floorOffset = Floorf(fracOffset);
		const s32 index0 = (s32)floorOffset;

		// compute interpolation right-floor
		s32  index_1 = index0 - 1;
		s32 index1 = index0 + 1;
		s32 index2 = index0 + 2;

		// Wrap interpolation
		if(index_1 < 0)
		{
			index_1 = m_BufferSize - 1;
		}
		index1 %= m_BufferSize;
		index2 %= m_BufferSize;

		const float y_1= m_Buffer [index_1];
		const float y0 = m_Buffer [index0];
		const float y1 = m_Buffer [index1];
		const float y2 = m_Buffer [index2];

		// compute interpolation x
		const float x = fracOffset - floorOffset;

		// calculate
		const float c0 = y0;
		const float c1 = 0.5f*(y1-y_1);
		const float c2 = y_1 - 2.5f*y0 + 2.0f*y1 - 0.5f*y2;
		const float c3 = 0.5f*(y2-y_1) + 1.5f*(y0-y1);

		return ((c3*x+c2)*x+c1)*x+c0;
	}


	s32		m_BufferSize;
	float   *m_Buffer;
	s32     m_Counter;
};

} // namespace rage

#endif // AUD_FRACDELAY_H
