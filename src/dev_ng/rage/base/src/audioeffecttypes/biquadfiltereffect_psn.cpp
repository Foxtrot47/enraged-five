#if __SPU
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "psn_dsp.h"
#include "filterdefs.h"
#include "math/amath.h"
#include <cell/dma.h>

#define TRUE		(1)

#define FALSE		(0)
#define EPISILON	(0.5f)
#define TAG			(5)

#ifndef PI
#define PI			(3.14159265358979323846264338327950288f)
#endif

#define Undenormalize(sample)			(sample) // ((((*(u32*)&sample)&0x7f800000)==0) ? 0.0f : sample)

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// PURPOSE: this function checks to see if any of the core parameters
//    in the biquad filter have changed.  If so, we'll need to recalc
//    the coefficients for the filter.
int IsBiquadParamsChanged(const tBiquadFilterSPUParamData *next, const tBiquadFilterSPUParamData *previous)
{
	float high, low;

	// check mode
	if (next->Settings.Mode != previous->Settings.Mode)
	{
		//Displayf("[spu] biquad: mode change");
		return TRUE;
	}

	// check cutoff
	high = previous->Settings.CutoffFrequency + EPISILON; low = previous->Settings.CutoffFrequency - EPISILON;
	if (next->Settings.CutoffFrequency > high || next->Settings.CutoffFrequency < low)
	{
		//Displayf("[spu] biquad: cutoff change");
		return TRUE;
	}

	// check resonance
	high = previous->Settings.ResonanceLevel + EPISILON; low = previous->Settings.ResonanceLevel - EPISILON;
	if (next->Settings.ResonanceLevel > high || next->Settings.ResonanceLevel < low)
	{
		//Displayf("[spu] biquad: resonance change");
		return TRUE;
	}

	// check bandwidth
	high = previous->Settings.Bandwidth + EPISILON; low = previous->Settings.Bandwidth - EPISILON;
	if (next->Settings.Bandwidth > high || next->Settings.Bandwidth < low)
	{
		//Displayf("[spu] biquad: bandwidth change");
		return TRUE;
	}

	// check gain
	high = previous->Settings.Gain + EPISILON; low = previous->Settings.Gain - EPISILON;
	if (next->Settings.Gain > high || next->Settings.Gain < low)
	{
		//Displayf("[spu] biquad: gain change");
		return TRUE;
	}

	// check mode
	if (next->Settings.Bypass != previous->Settings.Bypass)
	{
		//Displayf("[spu] biquad: bypass change");
		return TRUE;
	}

	return FALSE;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE
//   Compute filter coefficients
// PARAMS
//   paramData		- data passed into this routine by the PPU (the new settings)
//	 workData		- workspace buffer that is our pseudo-member variable storage 
void ComputeCoefficients(const tBiquadFilterSPUParamData *paramData, tBiquadFilterSPUData *workData)
{
	//////////////////////////////////////////////////////////////////////////
	// copy in the new settings to our workspace
	tBiquadFilterSettings *pSettings = &workData->m_Settings;

	pSettings->CutoffFrequency	= Clamp(paramData->Settings.CutoffFrequency, BIQUAD_FREQ_MIN, BIQUAD_FREQ_MAX);
	pSettings->ResonanceLevel	= Clamp(paramData->Settings.ResonanceLevel, BIQUAD_RESONANCE_MIN, BIQUAD_RESONANCE_MAX);
	//Clamp the bandwidth to ensure we don't go below DC - and into a world of filter stability pain.
	pSettings->Bandwidth		= Clamp(paramData->Settings.Bandwidth, BIQUAD_BANDWIDTH_MIN, Min(BIQUAD_BANDWIDTH_MAX, pSettings->CutoffFrequency*2.0f));
	pSettings->Gain				= Clamp(paramData->Settings.Gain, BIQUAD_GAIN_MIN, BIQUAD_GAIN_MAX);
	pSettings->Mode				= Clamp(paramData->Settings.Mode, (u32)BIQUAD_MODE_MIN, (u32)BIQUAD_MODE_MAX);
	pSettings->Bypass			= Clamp(paramData->Settings.Bypass, (u8)FALSE, (u8)TRUE);

	const int paramMode = paramData->Settings.Mode;
	workData->m_Is4Pole =((paramMode == BIQUAD_MODE_LOW_PASS_4POLE)  || (paramMode == BIQUAD_MODE_HIGH_PASS_4POLE) ||
						  (paramMode == BIQUAD_MODE_BAND_PASS_4POLE) || (paramMode == BIQUAD_MODE_BAND_STOP_4POLE) ||
						  (paramMode == BIQUAD_MODE_PEAKING_EQ_4POLE)|| (paramMode == BIQUAD_MODE_ULTIMATE_PEAKING_EQ)) ? TRUE : FALSE;

	pSettings->ResonanceLevel = powf(10.0f, (pSettings->ResonanceLevel-3.01f) * 0.05f);
	pSettings->Gain			  = powf(10.0f, pSettings->Gain * 0.05f);

	//////////////////////////////////////////////////////////////////////////

	if (pSettings->Bypass)
	{
		//Clear sample history so we don't click when we come out of bypass
		for (u32 i=0; i<g_MaxOutputChannels; ++i)
		{
			workData->m_xy_n1_1[i] = 0.0f;
			workData->m_xy_n1_1[i] = 0.0f;
			workData->m_xy_n2_1[i] = 0.0f;
			workData->m_xy_n1_2[i] = 0.0f;
			workData->m_xy_n2_2[i] = 0.0f;
		}
		return;
	}

	const f32 fs = NATIVE_MULTISTREAM_SAMPLE_RATE;

	f32 a0, a1, a2, b0, b1, b2;
	f32 omega = 2.0f * PI * workData->m_Settings.CutoffFrequency / fs;

	switch (workData->m_Settings.Mode)
	{
	case BIQUAD_MODE_LOW_PASS_2POLE:
	case BIQUAD_MODE_LOW_PASS_4POLE:
		{
			//Compensate for cascaded sections.
			const f32 resFactor = workData->m_Is4Pole ? 0.5f : 1.0f;
			const f32 cs = cosf(omega);
			const f32 sn = sinf(omega);
			const f32 alpha = sn * sinh(0.5f / (pSettings->ResonanceLevel * resFactor));

			a0 = (1.0f - cs) / 2.0f;
			a1 =  1.0f - cs;
			a2 = a0;
			b0 = 1.0f + alpha;
			b1 = -2.0f * cs;
			b2 = 1.0f - alpha;

			//Normalize so b0 = 1.0.
			workData->m_a_0 = a0 / b0;
			workData->m_a_1 = a1 / b0;
			workData->m_a_2 = a2 / b0;
			workData->m_b_1 = b1 / b0;
			workData->m_b_2 = b2 / b0;
		}
		break;

	case BIQUAD_MODE_HIGH_PASS_2POLE:
	case BIQUAD_MODE_HIGH_PASS_4POLE:
		{
			//Compensate for cascaded sections.
			const f32 resFactor = workData->m_Is4Pole ? 0.5f : 1.0f;
			const f32 cs = cosf(omega);
			const f32 sn = sinf(omega);
			const f32 alpha = sn * sinh(0.5f / (workData->m_Settings.ResonanceLevel * resFactor));

			a0 = (1.0f + cs) / 2.0f;
			a1 = -1.0f - cs;
			a2 = a0;
			b0 = 1.0f + alpha;
			b1 = -2.0f * cs;
			b2 = 1.0f - alpha;

			//Normalize so b0 = 1.0.
			workData->m_a_0 = a0 / b0;
			workData->m_a_1 = a1 / b0;
			workData->m_a_2 = a2 / b0;
			workData->m_b_1 = b1 / b0;
			workData->m_b_2 = b2 / b0;
		}
		break;

	case BIQUAD_MODE_BAND_PASS_2POLE:
	case BIQUAD_MODE_BAND_PASS_4POLE:
		{
			const f32 c = 1.0f / tanf(PI * (workData->m_Settings.Bandwidth / fs));
			const f32 d = 2.0f * cosf(omega);

			workData->m_a_0 = 1.0f / (1.0f + c);
			workData->m_a_1 = 0.0f;
			workData->m_a_2 = -workData->m_a_0;
			workData->m_b_1 = -workData->m_a_0 * c * d;
			workData->m_b_2 =  workData->m_a_0 * (c - 1.0f);
		}
		break;

	case BIQUAD_MODE_BAND_STOP_2POLE:
	case BIQUAD_MODE_BAND_STOP_4POLE:
		{
			const f32 c = tanf(PI * (workData->m_Settings.Bandwidth / fs));
			const f32 d = 2.0f * cosf(omega);

			workData->m_a_0 = 1.0f / (1.0f + c);
			workData->m_a_1 = -workData->m_a_0 * d;
			workData->m_a_2 =  workData->m_a_0;
			workData->m_b_1 =  workData->m_a_1;
			workData->m_b_2 =  workData->m_a_0 * (1.0f - c);
		}
		break;

	case BIQUAD_MODE_PEAKING_EQ_2POLE:
	case BIQUAD_MODE_PEAKING_EQ_4POLE:
		{
			const f32 Q = workData->m_Settings.CutoffFrequency / workData->m_Settings.Bandwidth;
			const f32 alpha = sinf(omega) / (2.f * Q);
			const f32 cs = cosf(omega);
			const f32 A = workData->m_Settings.ResonanceLevel;

			a0 = 1.0f + alpha * A;
			a1 = -2.0f * cs;
			a2 = 1.0f - alpha * A;
			b0 = 1.0f + alpha / A;
			b1 = -2.0f * cs;
			b2 = 1.0f - alpha / A;

			//Normalize so that b0 = 1.0.
			workData->m_a_0 = a0 / b0;
			workData->m_a_1 = a1 / b0;
			workData->m_a_2 = a2 / b0;
			workData->m_b_1 = b1 / b0;
			workData->m_b_2 = b2 / b0;
		}
		break;
	case BIQUAD_MODE_ULTIMATE_PEAKING_EQ:
		{
			const f32 Q = workData->m_Settings.CutoffFrequency / workData->m_Settings.Bandwidth;
			const f32 alpha = sinf(omega) / (2.f * Q);
			const f32 cs = cosf(omega);
			//Since we're using resonance as the gain/cut for this peak, and 0dB of resonance for the other filter
			//modes equates to -3.01dB at Fc, our desired gain/cut for this filter is actually 3.01 dB higher than 
			//the linear ResonanceLevel value stored in our settings structure.  That's why we multiply by 1.4142.
			f32 A = workData->m_Settings.ResonanceLevel * 1.4142f;

			a0 = 1.f + alpha * A;
			a1 = -2.f * cs;
			a2 = 1.f - alpha * A;
			b0 = 1.f + alpha / A;
			b1 = -2.f * cs;
			b2 = 1.f - alpha / A;

			//Normalize so that b0 = 1.0.
			workData->m_a_0 = a0 / b0;
			workData->m_a_1 = a1 / b0;
			workData->m_a_2 = a2 / b0;
			workData->m_b_1 = b1 / b0;
			workData->m_b_2 = b2 / b0;
		}
		break;
	default:
		//Errorf("[spu] biquad compute coefficients - unknown mode!");
		//Assert(0);
		break;
	}

// 	Displayf("compute coefficients results");
//	Displayf("   (a0)%4.3f  (a1)%4.3f  (a2)%4.3f", workData->m_a_0, workData->m_a_1, workData->m_a_2);
// 	Displayf("   (b1)%4.3f  (b1)%4.3f", workData->m_b_1, workData->m_b_2);
// 	Displayf("   (%s) (%s)", workData->m_Is4Pole ? "4-pole" : "2-pole", workData->m_Settings.Bypass == 0 ? "not bypassed" : "BYPASSED");
// 	Displayf("   (bw)%f (cf)%f (g)%f (m)%d", workData->m_Settings.Bandwidth, workData->m_Settings.CutoffFrequency, workData->m_Settings.Gain, workData->m_Settings.Mode);
}

//
u32 GetChannelIndex(const u8 msMask)
{
	u8 flag = 1;
	for (u32 i=0; i<8; ++i)
	{
		if (msMask & flag)
		{
			return i;
		}
		flag <<= 1;
	}

	return ~0U;
}


/**********************************************************************************
DSPInit
Initialisation routine
This routine is called by MultiStream when the DSP effect is first called.

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
paramList		Address of static parameter data (248 bytes)
	0-127		= Read Only (example: users input parameters)
	128-247		= Read / Write (example: DSP effects internal parameters)
workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area
dspInfo			Contains information as to the slot number to be processed, input / output bitmasks

Notes:
	Each DSP effect has 248 bytes of RAM available.
	If more RAM is required, this area can be used to store a pointer to the actual data.
	The data can then be transferred from main RAM as required.
	Splitting the paramList into a read and a read/write section allows us to make sure that if
	the user is modifying parameters via PPU commands (example: distortion level), there is no conflicts
	between the SPU updating parameter memory at the same time.

**********************************************************************************/
void DSPInit(char *paramList, void *workArea, MS_DSP_INFO *dspInfo)
{
}

/**********************************************************************************
DSPApply
Apply DSP effect routine
This routine is called by MultiStream to apply a DSP effect to an input signal

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
input			Address of input data (Time Domain float32 data OR Frequency Domain FFT data)
				For PCM, 1024 samples are passed in.
				For FFT, 512 frequency bands are passed in.

output			Address to write output data to

paramList		Address of static parameter data (248 bytes)
				0-127 = Read Only
				128-247 = Read / Write

workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area

dspInfo			Contains information as to the slot number to be processed, input / output bitmasks..

Returns:
0				DSP will complete when stream finishes
!0				DSP will stay active after stream finishes (for delays / reverbs)

Notes:
For time domain input data, the user must process all 1024 samples.
But note that any parameters used for modifying data must be preserved after 512 samples.
This is due to windowing within MultiStream.

**********************************************************************************/
int DSPApply(void *input, void *output, char *paramList, void *workArea, MS_DSP_INFO *dspInfo)
{
	tBiquadFilterSPUParamData *pNextParams = (tBiquadFilterSPUParamData*)paramList;
	tBiquadFilterSPUParamData *pLastParams = (tBiquadFilterSPUParamData*)(paramList+128);

	if (pNextParams->Settings.Bypass)
	{
		//Copy input buffer into output buffer - bypass mode.
		for(int cnt = 0; cnt<FRAME_SAMPLES; cnt++)
		{
			((f32 *)output)[cnt] = ((f32 *)input)[cnt];
		}

		return 0;
	}

	//Ensure that all previous effect DMAs have completed before we start.
	cellDmaWaitTagStatusAll(1 << TAG);

	//////////////////////////////////////////////////////////////////////////
	// dma across the work data, set it, and restore it back to the PPU
	uint64_t ea = (uint64_t)pNextParams->EA;
	if(ea == 0)
	{
		//This effect has not yet been initialized on the PPU, so early-out.
		return 0;
	}

	cellDmaGet(workArea, ea, sizeof(tBiquadFilterSPUData), TAG, 0, 0);
	cellDmaWaitTagStatusAll(1 << TAG);
	dspInfo->bandwidthIn += sizeof(tBiquadFilterSPUData);

	tBiquadFilterSPUData *pWorkData = (tBiquadFilterSPUData*)workArea;

	const u32 channelIndex = (dspInfo->callNumber == -1 ? 0 : GetChannelIndex(dspInfo->inMask));

	//////////////////////////////////////////////////////////////////////////
	// only perform DSP if our metadata masks match
	if ( (dspInfo->callNumber != -1) && (!(dspInfo->inMask & pWorkData->m_ChannelMask) || channelIndex > g_MaxOutputChannels) )
	{
		//Copy input buffer into output buffer - bypass mode.
		for(int cnt = 0; cnt<FRAME_SAMPLES; cnt++)
		{
			((f32 *)output)[cnt] = ((f32 *)input)[cnt];
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	// have our parameters changed such that we need to recalc coefficients?
	if (IsBiquadParamsChanged(pNextParams, pLastParams) == TRUE)
	{
		*pLastParams = *pNextParams;

		//Displayf("[spu] biquad - computing coefficients");
		ComputeCoefficients(pLastParams, pWorkData);
	}

	//////////////////////////////////////////////////////////////////////////
	// Create output data if DSP effect is set as Time Domain
	const f32 a_0 = pWorkData->m_a_0;
	const f32 a_1 = pWorkData->m_a_1;
	const f32 a_2 = pWorkData->m_a_2;
	const f32 b_1 = pWorkData->m_b_1;
	const f32 b_2 = pWorkData->m_b_2;

 	f32 xy_n1_1 = pWorkData->m_xy_n1_1[channelIndex];
 	f32 xy_n2_1 = pWorkData->m_xy_n2_1[channelIndex];
 	f32 xy_n1_2 = pWorkData->m_xy_n1_2[channelIndex];  
 	f32 xy_n2_2 = pWorkData->m_xy_n2_2[channelIndex];

	f32 currentSample1, currentSample2, newSample1, newSample2;

	f32 *samples = (f32*)input;
 	if (pWorkData->m_Is4Pole)
 	{
 		int cnt;
 		for (cnt=0; cnt<FRAME_SAMPLES; cnt++)
 		{
 			currentSample1 = *samples;
 			newSample1 = (a_0 * currentSample1) + xy_n1_1;
 			currentSample2 = newSample1;						//Current sample is now output of last stage.
 			newSample2 = (a_0 * currentSample2) + xy_n1_2;

 			xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
 			xy_n1_2 = (a_1 * currentSample2) - (b_1 * newSample2) + xy_n2_2;
 			xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);
 			xy_n2_2 = (a_2 * currentSample2) - (b_2 * newSample2);
 
 			((f32 *)output)[cnt] = newSample2 * pWorkData->m_Settings.Gain;
			samples++;
 		}
 	}
 	else
	{
		int cnt;
		for (cnt=0; cnt<FRAME_SAMPLES; cnt++)
 		{
 			currentSample1 = *samples;
			newSample1 = (a_0 * currentSample1) + xy_n1_1;
   
 			xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
 			xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);
   
 			((f32 *)output)[cnt] = newSample1 * pWorkData->m_Settings.Gain;
			samples++;
		}
	}

	//Store the state of our effect.
	pWorkData->m_xy_n1_1[channelIndex] = Undenormalize(xy_n1_1);
	pWorkData->m_xy_n2_1[channelIndex] = Undenormalize(xy_n2_1);
	pWorkData->m_xy_n1_2[channelIndex] = Undenormalize(xy_n1_2);
	pWorkData->m_xy_n2_2[channelIndex] = Undenormalize(xy_n2_2);

	//////////////////////////////////////////////////////////////////////////
	// restore the work data for the next pass
	cellDmaPut(workArea, ea, sizeof(tBiquadFilterSPUData), TAG, 0, 0);
	dspInfo->bandwidthOut += sizeof(tBiquadFilterSPUData);
	//cellDmaWaitTagStatusAll(1 << TAG);

	// If the stream ends and (and if this DSP effect is attached to a stream), end the DSP effect too..
	return 0;
}

/**********************************************************************************
DSPPlugInSetup
Setup as the entry point function within the DSP makefile
Initialises the DSP effect functions.
Called by MultiStream.

Requires:
plugInStruct		Address of a DSPPlugin structure

Notes:
This function sets the address of both the Init and the Apply functions.
It also specifies if the input / output data is required as time domain or FFT data
**********************************************************************************/
void DSPPlugInSetup(DSPPlugin* plugInStruct)
{
	plugInStruct->DSPInit = (ms_fnct)DSPInit;			// Init function (or NULL if no init function required)
	plugInStruct->DSPApply = (ms_fnct)DSPApply;			// DSP "Apply" function (processes DSP effect)
	plugInStruct->Domain = MS_DSP_TIMEBASE;				// DSP Effect is Time based (float32 PCM)
}


#endif	// __SPU
