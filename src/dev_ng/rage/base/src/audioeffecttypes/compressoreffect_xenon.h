//
// audioeffects/compressoreffect_xenon.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
// Adapted from Microsoft sample code so as to actually work.

#if __XENON

#ifndef __COMPRESSOREFFECT_XENON_H
#define __COMPRESSOREFFECT_XENON_H

#include "audioeffecttypes/compressoreffect.h"
#include "math/amath.h"

#include "rage_xapo.h"

namespace rage {

//
// PURPOSE
//  A general dynamic range compressor.  
//
class __declspec(uuid("{89D2FC67-FFEF-4e5c-BCF9-279D3F85C0DB}"))
audCompressorEffectXenon : public audXAPOBase<audCompressorEffectXenon, tCompressorEffectSettings>
{
    public:

        audCompressorEffectXenon();
        virtual bool Init(const Effect *metadata);

    private:

		void DoProcess( const tCompressorEffectSettings&, f32* __restrict pData, u32 cFrames, u32 cChannels );
		void OnSetParameters(const tCompressorEffectSettings &params);

		f32 m_DelayBuffer[g_MaxOutputChannels][g_PeakLimiterDelayLength];
		f32 m_LastEnvelope[g_MaxOutputChannels];

		f32 m_xdn1[g_MaxOutputChannels];	// Running RMS average window				
		f32 m_fn1[g_MaxOutputChannels];		// Output factor of static Peak/RMS function (compressor control parameter)
		f32 m_gn1[g_MaxOutputChannels];		// Time-variant gain factor

		f32 m_AttackTime;			// internal representation of attack time
		f32 m_ReleaseTime;			// internal representation of release time
		f32 m_Slope;				// Slope factor
		f32 m_OneOverRMSWindow;
		f32 m_RMSWindow1;

		f32 m_ThresholdLin;			//linear representation of dB threshold passed in
		f32 m_GainLin;				//linear representation of dB makeup gain passed in

		f32 m_InverseSlopedThresholdLin;

		u32 m_NumChannelsToProcess;
		u8 m_ChannelMask;

		bool	m_IsDiscrete;		// true if speaker config is XAUDIOSPEAKERCONFIG_DIGITAL_DOLBYDIGITAL
};

} // namespace rage

#endif //__COMPRESSOREFFECT_XENON_H

#endif // __XENON
