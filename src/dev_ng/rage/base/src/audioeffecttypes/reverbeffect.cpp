// 
// audioeffecttypes/reverbeffect.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "reverbeffect.h"
#include "reverbtuning.h"

#include "audioengine/engineutil.h"
#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/framebufferpool.h"

#include "system/cache.h"
#include "system/dma.h"
#include "system/memops.h"
#include "system/performancetimer.h"

#ifndef OPTIMIZED_ONLY
#if __OPTIMIZED
#define OPTIMIZED_ONLY(x)	x
#else
#define OPTIMIZED_ONLY(x)
#endif
#endif

#define undenormalise(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)

namespace rage 
{
	using namespace Vec;
	audReverbEffect::audReverbEffect() : audDspEffect(AUD_DSPEFFECT_REVERB)
	{

	}

	audReverbEffect::~audReverbEffect()
	{

	}

	f32 *g_ReverbTempBuffer = NULL;
	bool audReverbEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		m_PrintedSpareScratch = 0;
		m_NumInputChannels = (u8)numChannels;
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i = 0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		sysMemSet(m_CombStores, 0, sizeof(m_CombStores));

		// allocate one contiguous buffer for all delay lines
		u32 totalDelayLineSamples = 0;
#if __ASSERT
		for(u32 i = 0; i < g_NumCombs; i++)
		{
			totalDelayLineSamples += g_CombFilterLengths[i];
		}
		Assert(totalDelayLineSamples == g_TotalNumCombSamples);
		totalDelayLineSamples = 0;
		for(u32 i = 0; i < g_NumAllPasses; i++)
		{
			totalDelayLineSamples += g_AllPassFilterLengths[i];
		}
		Assert(totalDelayLineSamples == g_TotalNumAllPassSamples);
#endif
		
		totalDelayLineSamples = m_NumChannelsToProcess * (g_TotalNumAllPassSamples + g_TotalNumCombSamples);
		delayLineSample *bufPtr = rage_aligned_new(16) delayLineSample[totalDelayLineSamples];
		Assert(bufPtr);

		sysMemSet(bufPtr, 0, sizeof(delayLineSample) * totalDelayLineSamples);

		for(u32 channelIndex = 0; channelIndex < m_NumChannelsToProcess; channelIndex++)
		{
			// allocate comb filter delay lines for this channel
			for(u32 combIndex = 0; combIndex < g_NumCombs; combIndex++)
			{
				m_CombBufferIndices[channelIndex][combIndex] = 0;
			}
			m_CombBuffers[channelIndex] = bufPtr;
			bufPtr += g_TotalNumCombSamples;
		
			for(u32 allpassIndex = 0; allpassIndex < g_NumAllPasses; allpassIndex++)
			{
				m_AllPassBufferIndices[channelIndex][allpassIndex] = 0;				
			}
			m_AllPassBuffers[channelIndex] = bufPtr;
			bufPtr += g_TotalNumAllPassSamples;
		}

		// Set default values
		//m_RoomSize = g_InitialRoom;
		((f32*)&m_WetDryCombFB)[0] = g_InitialWet;
		((f32*)&m_WetDryCombFB)[1] = g_InitialDry;
		((f32*)&m_WetDryCombFB)[2] = ((f32*)&m_WetDryCombFB)[3] = 0.f;

		m_Damping = g_InitialDamp;
		m_Bypass = false;
		m_ProcessAsMono = false;

#if !__PS3
		if(!g_ReverbTempBuffer)
		{
			g_ReverbTempBuffer = rage_aligned_new(128) f32[kMixBufNumSamples];
		}
#endif
		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audReverbEffect)
	{
		if(m_Bypass)
		{
			return;
		}

#if AUD_TIME_DSP
		sysPerformanceTimer timer("");
		timer.Start();
#endif
		audAssertf(m_CombBuffers[0] != (delayLineSample*)0xcdcdcdcd,"invalid combbuffer ptr");

#if __SPU
		// set up double buffered DMA LS locations
		const s32 combTag = 7;
		const s32 allpassTag = 9;


		delayLineSample *combDmaDest0 = (delayLineSample*)scratchSpace;
		delayLineSample *allpassDmaDest0 = combDmaDest0 + g_TotalNumCombSamples;
		delayLineSample *combDmaDest1 = allpassDmaDest0 + g_TotalNumAllPassSamples;
		delayLineSample *allpassDmaDest1 = combDmaDest1 + g_TotalNumCombSamples;

		s32 currentDmaIndex = 0;
		delayLineSample *const combDmaDest[2] = {combDmaDest0,combDmaDest1};
		delayLineSample *const allpassDmaDest[2] = {allpassDmaDest0,allpassDmaDest1};

		// ensure we have enough scratch space
		const size_t scratchSpaceRequired = (g_TotalNumCombSamples + g_TotalNumAllPassSamples) * sizeof(delayLineSample) * 2;
		if(scratchSpaceSize < scratchSpaceRequired)
		{
			if(!m_PrintedSpareScratch)
			{
				m_PrintedSpareScratch = 1;
				audDisplayf("Reverb disabled, not enough scratch space (%u available, %" SIZETFMT "u required)", scratchSpaceSize, scratchSpaceRequired);
			}
			return;
		}

		if(!m_PrintedSpareScratch)
		{
			m_PrintedSpareScratch = 1;
			audDisplayf("reverb scratch space left: %" SIZETFMT "u", scratchSpaceSize - scratchSpaceRequired);
		}

		// DMA first two channels of comb + allpass

		//audDisplayf("grabbing first channel comb + allpass to %p (index %d)", combDmaDest[currentDmaIndex], currentDmaIndex);
		Assert(combDmaDest[currentDmaIndex]);
		sysDmaLargeGet(combDmaDest[currentDmaIndex], (uint64_t)m_CombBuffers[0], (g_TotalNumCombSamples + g_TotalNumAllPassSamples) * sizeof(delayLineSample), combTag + currentDmaIndex);

		ALIGNAS(16) f32 reverbTempBuffer[kMixBufNumSamples] ;
		g_ReverbTempBuffer = reverbTempBuffer;
#endif // SPU

		Vector_4V combBufferVector;
		Vector_4V damp1FeedbackVector, dampVector1, dampVector2, dampVector3, dampVector4;
		Vector_4V prevCombStoreVectors[g_NumCombs];
		Vector_4V damp1FeedbackPrevCombStoreVector;
		
#if __XENON || 1
		Vector_4V combStoreVector1, combStoreVector2, combStoreVector3;
		Vector_4V combStoreMergeVectorZW, combStoreMergeVectorXY;
#endif
		
		Vector_4V combStoreVector4;
		
		Vector_4V combStoreVector, combStoreFeedbackVector;
		Vector_4V allPassBufferOutVector, inVector, outVector, allPassOutVector;

		//(g_AllPassFeedback == 0.5f);
		//(g_FixedGain == 0.015f);
		// 1e-20
		const Vector_4V packedConstants = V4VConstant<U32_HALF,0x3C75C28F,0x1E3CE508,0U>();
	
		f32 *out = g_ReverbTempBuffer;
		
		// invalidate these cachelines (we're only overwriting so don't care about current contents)
		for(u32 i = 0; i < sizeof(f32) * kMixBufNumSamples; i += 128)
		{
			ZeroDC(g_ReverbTempBuffer, i);
		}

		delayLineSample *combBuffer, *allPassBuffer;

		const Vector_4V wetDryCombFB = m_WetDryCombFB;
		/*
		dryVector = V4LoadScalar32IntoSplatted(m_DryMix);
		wetVector = V4LoadScalar32IntoSplatted(m_WetMix);
		combFeedbackVector = V4LoadScalar32IntoSplatted(m_CombFilterFeedback);*/

		damp1FeedbackVector = m_CombFilterDamp1FeedbackArray;

		//Generate comb filter damping matrix.
		//(3,-,-,-)
		//(2,3,-,-)
		//(1,2,3,-)
		//(0,1,2,3)
		dampVector4 = m_DampVector4;
		dampVector3 = m_DampVector3;
		dampVector2 = m_DampVector2;
		dampVector1 = m_DampVector1;

		u32 inputChannelMask = 1;
		u32 channelIndex = 0;
		for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
		{
			if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId && (m_ChannelMask & inputChannelMask))
			{
				//We need to process this input channel.

				SPU_ONLY(const s32 nextDmaIndex = (currentDmaIndex + 1)%2);
				
				if(!m_ProcessAsMono || inputChannelIndex == 0)
				{
#if __XENON
					//Prefetch first 128-byte block of comb data for all 8 filters - utilizing all 8 cache lines
					//NOTE: This does not perform well around filter index wraps as the filters are not multiples of 32 taps in
					//length.
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][0] + 32) % g_CombFilterLengths[0]), &m_CombBuffers[channelIndex][0]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][1] + 32) % g_CombFilterLengths[1]), &m_CombBuffers[channelIndex][1]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][2] + 32) % g_CombFilterLengths[2]), &m_CombBuffers[channelIndex][2]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][3] + 32) % g_CombFilterLengths[3]), &m_CombBuffers[channelIndex][3]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][4] + 32) % g_CombFilterLengths[4]), &m_CombBuffers[channelIndex][4]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][5] + 32) % g_CombFilterLengths[5]), &m_CombBuffers[channelIndex][5]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][6] + 32) % g_CombFilterLengths[6]), &m_CombBuffers[channelIndex][6]);
					__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][7] + 32) % g_CombFilterLengths[7]), &m_CombBuffers[channelIndex][7]);
#endif

					//Restore previous comb store vectors for this channel.
					for(u32 i=0; i < g_NumCombs; i++)
					{
						prevCombStoreVectors[i] = V4LoadScalar32IntoSplatted(m_CombStores[channelIndex][i]);
					}

#if __SPU
					// wait on comb dma
					sysDmaWaitTagStatusAll(1<<(combTag + currentDmaIndex));
					delayLineSample *channelCombBuffers = combDmaDest[currentDmaIndex];
#else
					delayLineSample *channelCombBuffers = m_CombBuffers[channelIndex];
#endif

					f32 *input = buffer.ChannelBuffers[inputChannelIndex];

					for(u32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex += 32)
					{
#if __XENON
						//Prefetch next 128-byte block of comb data for all 8 filters - utilizing all 8 cache lines.
						//NOTE: This does not perform well around filter index wraps as the filters are not multiples of 32 taps in
						//length.
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][0] + 32) % g_CombFilterLengths[0]), &m_CombBuffers[channelIndex][0]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][1] + 32) % g_CombFilterLengths[1]), &m_CombBuffers[channelIndex][1]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][2] + 32) % g_CombFilterLengths[2]), &m_CombBuffers[channelIndex][2]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][3] + 32) % g_CombFilterLengths[3]), &m_CombBuffers[channelIndex][3]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][4] + 32) % g_CombFilterLengths[4]), &m_CombBuffers[channelIndex][4]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][5] + 32) % g_CombFilterLengths[5]), &m_CombBuffers[channelIndex][5]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][6] + 32) % g_CombFilterLengths[6]), &m_CombBuffers[channelIndex][6]);
						__dcbt(sizeof(delayLineSample) * ((m_CombBufferIndices[channelIndex][7] + 32) % g_CombFilterLengths[7]), &m_CombBuffers[channelIndex][7]);
#endif				

						for(u32 sampleOffset = 0; sampleOffset < 32; sampleOffset+=4)
						{
							const u32 sampleBufferIndex = sampleIndex + sampleOffset;
							
							inVector = *((Vector_4V*)&(input[sampleBufferIndex]));
							outVector = V4VConstant(V_ZERO);

							delayLineSample *combBufferPtr = channelCombBuffers;

							//Accumulate comb filters in parallel.
							for(u32 i = 0; i < g_NumCombs; i++)
							{
								combBuffer = combBufferPtr + m_CombBufferIndices[channelIndex][i];
								// move combBufferPtr onto next comb filter
								combBufferPtr += g_CombFilterLengths[i];

								combBufferVector = *((Vector_4V*)combBuffer);

								damp1FeedbackPrevCombStoreVector = V4Scale(damp1FeedbackVector, prevCombStoreVectors[i]);

								//Accumulate filter output.
								outVector = V4Add(outVector, combBufferVector);

	#if 0 && (__PS3 || __WIN32PC)
								//	This should be friendlier for SPU code..
								// dampVector1-4 are 4x4 matrix..
								//				x y z w
								//				y z w 0
								//				z w 0 0
								//				w 0 0 0
								//
								//	For this transpose is same :)
								combStoreVector = V4Scale(dampVector1,V4SplatX(combBufferVector));
								combStoreVector = V4AddScaled(combStoreVector, V4SplatY(combBufferVector), dampVector2);
								combStoreVector = V4AddScaled(combStoreVector, V4SplatZ(combBufferVector), dampVector3);
								combStoreVector = V4AddScaled(combStoreVector, V4SplatW(combBufferVector), dampVector4);
								combStoreVector4 = V4SplatW(combStoreVector);
	#else
								// Xbox360 has dot product instruction
								combStoreVector1 = V4DotV(dampVector1, combBufferVector);
								combStoreVector2 = V4DotV(dampVector2, combBufferVector);
								combStoreVector3 = V4DotV(dampVector3, combBufferVector);
								combStoreVector4 = V4DotV(dampVector4, combBufferVector);

								// unswizzle the splatted combStoreVectors into a single vector
								
								combStoreMergeVectorXY = V4MergeXY(combStoreVector1,combStoreVector2);
								combStoreMergeVectorZW = V4MergeXY(combStoreVector3,combStoreVector4);
								combStoreVector = V4PermuteTwo<X1,Y1,X2,Y2>(combStoreMergeVectorXY, combStoreMergeVectorZW);
	#endif

								combStoreFeedbackVector = V4Add(V4Scale(combStoreVector, V4SplatZ(wetDryCombFB)), damp1FeedbackPrevCombStoreVector);

								combBufferVector = V4Add(V4SplatZ(packedConstants), V4Add(V4Scale(inVector, V4SplatY(packedConstants)), combStoreFeedbackVector));

								*((Vector_4V*)combBuffer) = combBufferVector;

								// V4DotV sets this pre-splatted
								prevCombStoreVectors[i] = combStoreVector4;

								//Assign(m_CombBufferIndices[channelIndex][i], (m_CombBufferIndices[channelIndex][i] + 4) % g_CombFilterLengths[i]);
								m_CombBufferIndices[channelIndex][i] = (m_CombBufferIndices[channelIndex][i] + 4) % g_CombFilterLengths[i];
							}//foreach comb

							*((Vector_4V*)&out[sampleBufferIndex]) = V4Add(V4SplatZ(packedConstants), outVector);	

						} //sampleIndex
					}

					//Backup previous comb store vectors for this channel.
					for(u32 i = 0; i < g_NumCombs; i++)
					{
						V4StoreScalar32FromSplatted(m_CombStores[channelIndex][i], prevCombStoreVectors[i]);	 
					}

#if __SPU
					// dma comb buffer back to PPU
					//audDisplayf("putting channel %u comb from %p (index %d)", channelIndex, combDmaDest[currentDmaIndex], currentDmaIndex);
					Assert(combDmaDest[currentDmaIndex]);
					sysDmaLargePut(combDmaDest[currentDmaIndex], (uint64_t)m_CombBuffers[channelIndex], sizeof(f32) * g_TotalNumCombSamples, combTag+currentDmaIndex);
					
					// dma next comb buffer to LS
					if(channelIndex < (u32)(m_NumChannelsToProcess-1))
					{
						// ensure any pending write from this buffer has been completed before overwriting
						sysDmaWaitTagStatusAll(1<<(combTag+nextDmaIndex));
						//audDisplayf("grabbing next channel %u comb to %p (index %d)", channelIndex+1, combDmaDest[nextDmaIndex], nextDmaIndex);
						Assert(combDmaDest[nextDmaIndex]);
						sysDmaLargeGet(combDmaDest[nextDmaIndex], (uint64_t)m_CombBuffers[channelIndex+1], sizeof(f32) * g_TotalNumCombSamples, combTag+nextDmaIndex);						
					}
					// wait on allpass DMA
					sysDmaWaitTagStatusAll(1<<(allpassTag+currentDmaIndex));
					f32 *channelAllPassBuffers = allpassDmaDest[currentDmaIndex];
#else
					f32 *channelAllPassBuffers = m_AllPassBuffers[channelIndex];
#endif

					for(u32 sampleIndex = 0; sampleIndex<kMixBufNumSamples; sampleIndex+=32)
					{						
						for(u32 sampleOffset=0; sampleOffset<32; sampleOffset+=4)
						{
							const u32 sampleBufferIndex = sampleIndex + sampleOffset;

							inVector = *((Vector_4V*)&input[sampleBufferIndex]);
							outVector = *((Vector_4V*)&out[sampleIndex + sampleOffset]);

							//Feed through all-passes in series.
							f32 *allpassBufferPtr = channelAllPassBuffers;
							for(u32 i = 0; i < g_NumAllPasses; i++)
							{
								allPassBuffer = allpassBufferPtr + m_AllPassBufferIndices[channelIndex][i];
								allpassBufferPtr += g_AllPassFilterLengths[i];

								allPassBufferOutVector = *((Vector_4V*)allPassBuffer);

								allPassOutVector = V4Subtract(allPassBufferOutVector, outVector);
								allPassBufferOutVector = V4Add(V4Scale(allPassBufferOutVector, V4SplatX(packedConstants)), outVector);

								m_AllPassBufferIndices[channelIndex][i] = (m_AllPassBufferIndices[channelIndex][i] + 4) %	g_AllPassFilterLengths[i];

								*((Vector_4V*)allPassBuffer) = V4Add(V4SplatZ(packedConstants),allPassBufferOutVector);

								outVector = allPassOutVector;
							}// foreach allpass

							inVector = V4Scale(inVector, V4SplatY(wetDryCombFB));
							inVector = V4Add(V4Scale(outVector, V4SplatX(wetDryCombFB)), inVector);

							*((Vector_4V*)&input[sampleBufferIndex]) = V4Add(inVector, V4SplatZ(packedConstants));

						} //sampleOffset
					} //sampleIndex

#if __SPU
					// dma back allpass to PPU
					//audDisplayf("putting current channel %u allpass to %p (index %d)", channelIndex, allpassDmaDest[currentDmaIndex], currentDmaIndex);
					Assert(allpassDmaDest[currentDmaIndex]);
					sysDmaLargePut(allpassDmaDest[currentDmaIndex], (uint64_t)m_AllPassBuffers[channelIndex], g_TotalNumAllPassSamples * sizeof(f32), allpassTag+currentDmaIndex);
					
					// prefetch next allpass
					if(channelIndex < (u32)(m_NumChannelsToProcess-1))
					{
						// wait until any pending write from the next buffer is completed
						sysDmaWaitTagStatusAll(1<<(allpassTag+nextDmaIndex));
						//audDisplayf("getting next channel %u allpass to %p (index %d)", channelIndex+1, allpassDmaDest[nextDmaIndex], nextDmaIndex);
						Assert(allpassDmaDest[nextDmaIndex]);
						sysDmaLargeGet(allpassDmaDest[nextDmaIndex], (uint64_t)m_AllPassBuffers[channelIndex+1], sizeof(f32) * g_TotalNumAllPassSamples, allpassTag+nextDmaIndex);
					}

					currentDmaIndex = (currentDmaIndex + 1) % 2;
#endif

				}// !process as mono
				else if(m_ProcessAsMono)
				{
					// copy front left into this output
					for(u32 sampleIndex=0; sampleIndex<kMixBufNumSamples; sampleIndex+=32)
					{
						for(u32 sampleOffset=0; sampleOffset<32; sampleOffset+=4)
						{
							const u32 sampleBufferIndex = sampleIndex + sampleOffset;
							
							f32 *input = buffer.ChannelBuffers[0];
							f32 *output = buffer.ChannelBuffers[inputChannelIndex];
							if (buffer.ChannelBufferIds[0] < audFrameBufferPool::InvalidId && buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId)
							{
								inVector = *((Vector_4V*)&input[sampleBufferIndex]);
								// pass straight through
								*((Vector_4V*)&output[sampleBufferIndex]) = inVector;
							}
						}
					}
				}
				// step through channel index even if we're processing as mono
				channelIndex++;
			}// channelmask

			inputChannelMask <<= 1;
		} //inputChannelIndex

#if AUD_TIME_DSP
		timer.Stop();
		sm_ProcessTime[AUD_DSPEFFECT_REVERB] = timer.GetTimeMS() * 1000.f;
#endif
	}

#if 0
	void audReverbEffect::Process_FPU(f32 *OPTIMIZED_ONLY(buf))
	{
#if __OPTIMIZED

		Assert(buf);
		Assert(numSamples <= kMixBufNumSamples);

		f32 out[kMixBufNumSamples];
		f32 *input = buf, *combOutput, tempOutput, *combBuffer, *allPassBuffer;
		u32 channelIndex = 0, sampleIndex, sampleOffset, i;

#ifdef AUDIO_BENCHMARK_EFFECTS
		g_ReverbTimer.Start();
#endif // AUDIO_BENCHMARK_EFFECTS

		for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
		{
			if(m_ChannelMask & (1 << inputChannelIndex))
			{
				//We need to process this input channel.

				sysMemSet(out, 0, numSamples * sizeof(f32));


				for(sampleIndex=0; sampleIndex<numSamples; sampleIndex+=32)
				{
					for(sampleOffset=0; sampleOffset<32; sampleOffset++)
					{
						input = buf + (inputChannelIndex * numSamples) + sampleIndex + sampleOffset;
						combOutput = out + sampleIndex + sampleOffset;
						*combOutput = 0.0f;

						//Accumulate comb filters in parallel.
						for(i=0; i<g_NumCombs; i++)
						{
							combBuffer = m_CombBuffers[channelIndex][i] + m_CombBufferIndices[channelIndex][i];
							tempOutput = *combBuffer;
							tempOutput = undenormalise(tempOutput);

							m_CombStores[channelIndex][i] = (tempOutput * (1.0f - m_Damping)) +
								(m_CombStores[channelIndex][i] * m_Damping);
							m_CombStores[channelIndex][i] = undenormalise(m_CombStores[channelIndex][i]);

							*combBuffer = (*input * g_FixedGain) + (m_CombStores[channelIndex][i] * m_RoomSize);

							m_CombBufferIndices[channelIndex][i] = (m_CombBufferIndices[channelIndex][i] + 1) %
								g_CombFilterLengths[i];

							//Accumulate filter output.
							*combOutput += tempOutput;
						}
					} //sampleOffset

				} //sampleIndex

				for(sampleIndex=0; sampleIndex<numSamples; sampleIndex+=32)
				{
					for(sampleOffset=0; sampleOffset<32; sampleOffset++)
					{
						input = buf + (inputChannelIndex * numSamples) + sampleIndex + sampleOffset;
						combOutput = out + sampleIndex + sampleOffset;
						//Feed through all-passes in series.
						for(i=0; i<g_NumAllPasses; i++)
						{
							allPassBuffer = m_AllPassBuffers[channelIndex][i] + m_AllPassBufferIndices[channelIndex][i];
							*allPassBuffer = undenormalise(*allPassBuffer);

							tempOutput = *allPassBuffer - *combOutput;

							*allPassBuffer = *combOutput + (*allPassBuffer * g_AllPassFeedback);

							m_AllPassBufferIndices[channelIndex][i] = (m_AllPassBufferIndices[channelIndex][i] + 1) %
								g_AllPassFilterLengths[i];

							//Write-back output of all-pass to comb filter output buffer.
							*combOutput = tempOutput;
						}

						//Write-back wet/dry mixed result to input buffer (inplace effect).
						*input = (*input * ((f32*)&m_WetDryCombFB)[1]) + (*combOutput * ((f32*)&m_WetDryCombFB)[0]);

					} //sampleOffset

				} //sampleIndex


				channelIndex++;
			}

		} //inputChannelIndex


#endif // __OPTIMIZED
	}
#endif

	void audReverbEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case audReverbEffect::ProcessAsMono:
			m_ProcessAsMono = (value != 0);
			break;
		case audReverbEffect::Bypass:
			m_Bypass = (value != 0);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audReverbEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audReverbEffect::SetParam(const u32 paramId, const f32 value)
	{
		bool recomputeDampingMatrices = false;
		switch(paramId)
		{
		case audReverbEffect::Damping:
			m_Damping = Clamp(value, 0.f, 1.f) * g_ScaleDamp;
			recomputeDampingMatrices = true;
			break;
		case audReverbEffect::RoomSize:
			((f32*)&m_WetDryCombFB)[2] = (Clamp(value, 0.f, 1.f) * g_ScaleRoom) + g_OffsetRoom;
			recomputeDampingMatrices = true;
			break;
		case audReverbEffect::WetMix:
			((f32*)&m_WetDryCombFB)[0] = Clamp(value, 0.f, 1.f) * g_ScaleWet;
			break;
		case audReverbEffect::DryMix:
			((f32*)&m_WetDryCombFB)[1] = Clamp(value, 0.f, 1.f) * g_ScaleDry;
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audReverbEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}

		if(recomputeDampingMatrices)
		{
			f32 d1 = m_Damping;
			f32 d2 = 1.0f - d1;

			// set up damping matrix
			ALIGNAS(16) f32 fbarray[4] ;
			fbarray[0] = d1 * ((f32*)&m_WetDryCombFB)[2];
			fbarray[1] = fbarray[0] * d1;
			fbarray[2] = fbarray[1] * d1;
			fbarray[3] = fbarray[2] * d1;
			m_CombFilterDamp1FeedbackArray = *((Vector_4V*)&fbarray[0]);

			// NOTE: reversed order
			fbarray[3] = d2;
			fbarray[2] = fbarray[3] * d1;
			fbarray[1] = fbarray[2] * d1;
			fbarray[0] = fbarray[1] * d1;
			
			Vector_4V combFilterDamp12Array = *((Vector_4V*)&fbarray[0]);

			static const Vector_4V andMask = V4VConstant<~0U,~0U,~0U,0U>();

			m_DampVector4 = combFilterDamp12Array;	
			m_DampVector3 = V4Permute<Y,Z,W,X>(m_DampVector4);
			m_DampVector3 = V4And(m_DampVector3, andMask);
			m_DampVector2 = V4Permute<Y,Z,W,W>(m_DampVector3);
			m_DampVector1 = V4Permute<Y,W,W,W>(m_DampVector2);
		}
	}

	void audReverbEffect::Shutdown(void)
	{
		//Free the contiguous data block we're holding.
		delete[] m_CombBuffers[0];
	}

} // namespace rage


