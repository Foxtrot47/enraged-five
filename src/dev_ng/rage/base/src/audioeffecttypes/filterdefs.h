#ifndef _AUD_EFFECTTYPES_FILTERDEFS_H_
#define _AUD_EFFECTTYPES_FILTERDEFS_H_

// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "effectmonitor.h"
#include "reverbtuning.h"

#include "audiohardware/driverdefs.h"
#include "audioengine/curve.h"


//////////////////////////////////////////////////////////////////////////
// waveshaper settings
#define WAVESHAPER_GAIN_MIN			(-96.0f)
#define WAVESHAPER_GAIN_MAX			(96.0f)
#define WAVESHAPER_GAIN_DEFAULT		(0.0f)
#define WAVESHAPER_APPLY_MIN		(0.0f)
#define WAVESHAPER_APPLY_MAX		(1.0f)
#define WAVESHAPER_APPLY_DEFAULT	(1.0f)
#define WAVESHAPER_BYPASS_MIN		(0)
#define WAVESHAPER_BYPASS_MAX		(1)
#define WAVESHAPER_BYPASS_DEFAULT	(0)

//////////////////////////////////////////////////////////////////////////
// compressor settings
#define COMPRESSOR_GAIN_MIN			(-96.0f)
#define COMPRESSOR_GAIN_MAX			(40.0f)
#define COMPRESSOR_GAIN_DEFAULT		(0.0f)
#define COMPRESSOR_THRESH_MIN		(-96.0f)
#define COMPRESSOR_THRESH_MAX		(0.0f)
#define COMPRESSOR_THRESH_DEFAULT	(-6.0f)
#define COMPRESSOR_RATIO_MIN		(0.001f)
#define COMPRESSOR_RATIO_MAX		(1000.0f)
#define COMPRESSOR_RATIO_DEFAULT	(2.0f)
#define COMPRESSOR_ATTACK_MIN		(0.001f)
#define COMPRESSOR_ATTACK_MAX		(1.0f)
#define COMPRESSOR_ATTACK_DEFAULT	(0.005f)
#define COMPRESSOR_RELEASE_MIN		(0.001f)
#define COMPRESSOR_RELEASE_MAX		(2.0f)
#define COMPRESSOR_RELEASE_DEFAULT	(0.12f)
#define COMPRESSOR_AVG_MIN			(0.00001f)
#define COMPRESSOR_AVG_MAX			(1.0f)
#define COMPRESSOR_AVG_DEFAULT		(0.06f)
#define COMPRESSOR_FACTOR_LIN		(1.0f/63095.7f)

//////////////////////////////////////////////////////////////////////////
// delay effect settings
#define DELAY_TIME_MIN				(0)
#define DELAY_TIME_MAX				(200)
#define DELAY_TIME_DEFAULT			(100)
#define DELAY_GAIN_MIN				(-100.0f)
#define DELAY_GAIN_MAX				(0.0f)
#define DELAY_GAIN_DEFAULT			(-6.0f)
#define DELAY_FEEDBACK_GAIN_MIN		(-100.0f)
#define DELAY_FEEDBACK_GAIN_MAX		(0.0f)
#define DELAY_FEEDBACK_GAIN_DEFAULT	(-100.0f)

#define DELAY_MAX_SAMPLES_UNALIGNED	(DELAY_TIME_MAX * (kMixerNativeSampleRate/1000))

//////////////////////////////////////////////////////////////////////////
// reverb effect settings
#define REVERB_DEFAULT_ROOM_SIZE	(1.0f)
#define REVERB_DEFAULT_WET_MIX		(0.5f)
#define REVERB_DEFAULT_DRY_MIX		(0.5f)
#define REVERB_DEFAULT_DAMPING		(0.0f)

//////////////////////////////////////////////////////////////////////////
// R360 settings
#define R360_OUTCEILING_MIN						(-96.0f)
#define R360_OUTCEILING_MAX						(0.0f)
#define R360_OUTCEILING_DEFAULT					(0.0f)
#define R360_THRESH_MIN							(-96.0f)
#define R360_THRESH_MAX							(0.0f)
#define R360_THRESH_DEFAULT						(0.0f)
#define R360_RELEASE_MIN						(0.1f)
#define R360_RELEASE_MAX						(2000.0f)
#define R360_RELEASE_DEFAULT					(2.0f)
#define R360_INPUTCLIP_MIN						(-96.0f)
#define R360_INPUTCLIP_MAX						(96.0f)
#define R360_INPUTCLIP_DEFAULT					(96.0f)
#define R360_AUTORELEASE_MIN_DEFAULT			(2.0f)
#define R360_AUTORELEASE_MAX_DEFAULT			(750.0f)
#define R360_AUTORELEASE_RMS_THRES_MIN_MIN		(-10.0f)
#define R360_AUTORELEASE_RMS_THRES_MIN_MAX		(0.0f)
#define R360_AUTORELEASE_RMS_THRES_MIN_DEFAULT	(-3.0f)	// -3 dB
#define R360_AUTORELEASE_RMS_THRES_MAX_MIN		(-96.0f)
#define R360_AUTORELEASE_RMS_THRES_MAX_MAX		(96.0f)	// +96 dB
#define R360_AUTORELEASE_RMS_THRES_MAX_DEFAULT	(3.0f)	// +3 dB

namespace rage
{

	// PURPOSE
	//	Defines starting param Id values
	// NOTES
	//	When adding a new effect type add a corresponding entry to this enum, and use that
	//	as the starting value for the effect specific param id enum.  This will ensure that
	//	param ids are unique across different effect types and therefore catch errors in high
	//	level code.
	class audEffectParamIds
	{
	public:
		enum audEffectParamIdList
		{
			BiquadFilterEffect		= 100,
			ChannelMeterEffect		= 200,
			CompressorEffect		= 300,
			DelayEffect				= 400,
			R360LimiterEffect		= 500,
			ReverbEffect			= 600,
			WaveShaperEffect		= 700,
			ReverbEffect_Prog		= 800,
			VariableDelayEffect		= 900,
			EarlyReflectionEffect	= 1000,
			DelayEffect4			= 1100,
			UnderwaterEffect		= 1200,
		};
	};

	enum BiquadFilterEffectModes
	{
		BIQUAD_MODE_LOW_PASS_2POLE = 0,
		BIQUAD_MODE_LOW_PASS_4POLE,
		BIQUAD_MODE_HIGH_PASS_2POLE,
		BIQUAD_MODE_HIGH_PASS_4POLE,
		BIQUAD_MODE_BAND_PASS_2POLE,
		BIQUAD_MODE_BAND_PASS_4POLE,
		BIQUAD_MODE_BAND_STOP_2POLE,
		BIQUAD_MODE_BAND_STOP_4POLE,
		BIQUAD_MODE_PEAKING_EQ_2POLE,
		BIQUAD_MODE_PEAKING_EQ_4POLE,
		BIQUAD_MODE_ULTIMATE_PEAKING_EQ,
		NUM_BIQUADFILTEREFFECTMODES,
		BIQUADFILTEREFFECTMODES_MAX = NUM_BIQUADFILTEREFFECTMODES,
	};

};

#endif
