//
// audioeffecttypes/convolutioneffect_xenon.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON && 0

#ifndef AUD_CONVOLUTION_EFFECT_XENON_H
#define AUD_CONVOLUTION_EFFECT_XENON_H

#include "system/xtl.h"
#include <xaudio.h>
#include <xaudfx.h>

#ifndef ASSERT
#define ASSERT(x) FastAssert(x)
#endif //ASSERT

#define AUDIO_BENCHMARK_CONVOLUTION 0

#if AUDIO_BENCHMARK_CONVOLUTION
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_CONVOLUTION

#if _XDK_VER >= 7978
#pragma warning(disable:4996)
#endif

namespace rage {

//The only parameter available for this effect. It obtains the spectral data associated with this effect.
static const XAUDIOFXPARAMID g_ConvolutionEffectGetDataParamId = 0x0;

//Number of samples per processing block - FFT length.
static const u32 g_NumConvBlockSamples = XAUDIOFRAMESIZE_NATIVE * 2;

//The CONVOLUTIONEFFECTDATA structure is returned when you query this effect using its g_GetDataParamID parameter.
//You'll get a 2D array of floats containing the frequency taps for each channel.
//NOTE: A frame is 256 samples (5 1/3 ms), and the internal processing rate is 48kHz.
typedef struct CONVOLUTIONEFFECTDATA
{
    bool	isPrimed;	//Has the effect run long enough to have valid data?
    u32		frameIndex;	//A constantly-incremented number. Rolls over near 0xFFFFFFFF.
    f32		*frequencyTaps; //The frequency taps (for each channel.)
} CONVOLUTIONEFFECTDATA, *LPCONVOLUTIONEFFECTDATA;
typedef const CONVOLUTIONEFFECTDATA *LPCCONVOLUTIONEFFECTDATA;

//Initialization structure. You must pass an LPVOID to an instance of this when you create this effect.
typedef struct CONVOLUTIONEFFECT_INIT
{
    XAUDIOFXINIT effectHeader; //The required effects init data.
    u32 numChannels; //Number of channels we expect to be analyzing. Used to allocate appropriate amount of storage.
} CONVOLUTIONEFFECT_INIT, *LPCONVOLUTIONEFFECT_INIT;

//Size function.
HRESULT QuerySizeConvolutionEffectXenon(LPCXAUDIOFXINIT init, LPDWORD effectSize);
HRESULT QuerySizeConvolutionEffectBuffersXenon(LPCXAUDIOFXINIT init, LPDWORD effectSize);

//Effect creation function. Part of the data needed by XAudioInitialize().
HRESULT CreateConvolutionEffectXenon(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator, IXAudioEffect **effect);

class audConvolutionEffectXenon: public IXAudioEffect
{
#pragma warning(push)
#pragma warning(disable:4100)
	XAUDIO_USE_BATCHALLOC_NEWDELETE();
#pragma warning(pop)

public:
    //IXAudioRefCount
    virtual ULONG AddRef(void);
    virtual ULONG Release(void);

    //IXAudioEffect    
    virtual HRESULT GetInfo(XAUDIOFXINFO* pInfo);
    virtual HRESULT GetParam(XAUDIOFXPARAMID paramId, XAUDIOFXPARAMTYPE paramType, XAUDIOFXPARAM *param);
    virtual HRESULT SetParam(XAUDIOFXPARAMID paramId, XAUDIOFXPARAMTYPE paramType, const XAUDIOFXPARAM *param);
    virtual HRESULT GetContext(LPVOID *context);
    virtual HRESULT Process(IXAudioFrameBuffer *inputBuffer, IXAudioFrameBuffer *outputBuffer);

    //UserEffect
    audConvolutionEffectXenon(LPVOID context);
    virtual HRESULT Init(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator);

private:
    u32	m_RefCount;
	void *m_Context;
	f32	*m_ReverbFrequencyTaps;
	f32	*m_ReverbFrequencyTapsUnaligned;
    f32	*m_FrequencyTaps;
	f32	*m_FrequencyTapsUnaligned;
	f32	***m_FrequencyTapBlocksForChannel;
	f32	*m_IfftBuffer;
	f32	*m_IfftBufferUnaligned;
	f32	*m_PrevInputSamples;
	f32	*m_PrevInputSamplesUnaligned;
	f32	**m_PrevInputSamplesForChannel;
	f32	*m_FftW;
	s32	*m_FftIp;

	u16 m_NumConvBlocks;
	u16 m_FftBufferIndex;
	u8 m_NumChannels;

    //The user must have supplied our effect ID via SetConvolutionEffectID();
    u8 m_EffectID;
    
    //Has enough data passed through the pipe to allow valid convolution?
    bool m_IsPrimed;

#if AUDIO_BENCHMARK_CONVOLUTION
	u32 m_FrameIndex;
	sysPerformanceTimer *m_ConvTimer = NULL;
	sysPerformanceTimer *m_FftTimer = NULL;
	sysPerformanceTimer *m_PermTimer1 = NULL;
	sysPerformanceTimer *m_PermTimer2 = NULL;
	sysPerformanceTimer *m_MuxTimer = NULL;
#endif // AUDIO_BENCHMARK_CONVOLUTION
};

} // namespace rage

#endif // AUD_CONVOLUTION_EFFECT_XENON_H

#endif // __XENON
