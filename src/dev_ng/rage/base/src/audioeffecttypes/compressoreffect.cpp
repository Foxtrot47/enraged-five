// 
// audioeffecttypes/compressoreffect.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "compressoreffect.h"
#include "system/memops.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/device.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"

namespace rage {

	audCompressorEffect::audCompressorEffect() : audDspEffect(AUD_DSPEFFECT_COMPRESSOR)
	{
		for (u32 i = 0; i < g_MaxOutputChannels; i++)
		{
			m_xdn1[i] = 0.0f;
			m_fn1[i]  = 0.0f;
			m_gn1[i]  = 0.0f;
		}
	}

	bool audCompressorEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		m_NumInputChannels = (u8)numChannels;
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i=0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		// Linear versions of gain and threshold
		m_GainLin	   = audDriverUtil::ComputeLinearVolumeFromDb(COMPRESSOR_GAIN_DEFAULT);
		m_ThresholdLin = audDriverUtil::ComputeLinearVolumeFromDb(COMPRESSOR_THRESH_DEFAULT);

		// Attack and release time calculations
		m_AttackTime  = 1.0f - exp( -2.2f * g_OneOverNativeSampleRate / COMPRESSOR_ATTACK_DEFAULT );
		m_ReleaseTime = 1.0f - exp( -2.2f * g_OneOverNativeSampleRate / COMPRESSOR_RELEASE_DEFAULT );

		// Slope calculation
		m_Slope = 1.0f - ( 1.0f / COMPRESSOR_RATIO_DEFAULT );

		// RMS Window calculation
		f32 RMSWindow = COMPRESSOR_AVG_DEFAULT * (f32)kMixerNativeSampleRate;

		m_RMSWindow1	   = RMSWindow - 1.0f;
		m_OneOverRMSWindow = 1.0f / RMSWindow;

		m_InverseSlopedTresholdLin = 1.0f / Powf(m_ThresholdLin, m_Slope);

		sysMemSet(&m_DelayBuffer[0][0],0,sizeof(m_DelayBuffer));
		sysMemSet(&m_LastEnvelope[0],0,sizeof(m_LastEnvelope));

		return true;
	}

	bool g_OptimizedCompressor = true;

	ALIGNAS(16) f32 g_PeakLimiterOutputBuffer[kMixBufNumSamples];
	ALIGNAS(16) f32 g_PeakLimiterMaxTable[(kMixBufNumSamples+audCompressorEffect::kPeakLimiterDelayLength)/4];

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audCompressorEffect)
	{
		if(m_Bypass)
			return;

		if(m_PeakLimiterMode)
		{
			const f32 attack = m_PeakLimiterAttack;
			const f32 release = m_PeakLimiterRelease;
			const f32 thresh = m_ThresholdLin;
			if(g_OptimizedCompressor)
			{
				for(u32 inputChannelIndex=0; inputChannelIndex < m_NumInputChannels; inputChannelIndex++)
				{
					if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId && m_ChannelMask & (1 << inputChannelIndex))
					{
						f32 *inputData = buffer.ChannelBuffers[inputChannelIndex];

						// build up max table
						u32 maxTableIndex = 0;
						for(u32 curSample = 0; curSample < kPeakLimiterDelayLength-4; curSample+=4,maxTableIndex++)
						{
							g_PeakLimiterMaxTable[maxTableIndex] = Max(Abs(m_DelayBuffer[inputChannelIndex][curSample]),Abs(m_DelayBuffer[inputChannelIndex][curSample+1]),
								Abs(m_DelayBuffer[inputChannelIndex][curSample+2]),Abs(m_DelayBuffer[inputChannelIndex][curSample+3]));
						}
						for(u32 curSample = 0; curSample < kMixBufNumSamples-4; curSample+=4,maxTableIndex++)
						{
							g_PeakLimiterMaxTable[maxTableIndex] = Max(Abs(inputData[curSample]),Abs(inputData[curSample+1]),
								Abs(inputData[curSample+2]),Abs(inputData[curSample+3]));
						}

						f32 lastEnv = m_LastEnvelope[inputChannelIndex];
						for(u32 curSample = 0; curSample < kMixBufNumSamples; curSample++)
						{
							float gain;

							const f32 delayedSample = (curSample < kPeakLimiterDelayLength?m_DelayBuffer[inputChannelIndex][curSample]:inputData[curSample-kPeakLimiterDelayLength]);
							const f32 currentSample = inputData[curSample];

							const f32 x = Abs(currentSample);
							const f32 a = Selectf(x - lastEnv, attack, release);
							//const f32 a = (x>m_LastEnvelope[inputChannelIndex]?attack:release);

							// max filter
							f32 maxX = x;
							// get the max sample from [curSample - kPeakLimiterDelayLength] to [curSample]
							u32 numSamplesCounted = 0;
							for(u32 i = curSample; i < kPeakLimiterDelayLength; i+=4)
							{
								maxX = Max(maxX, g_PeakLimiterMaxTable[i>>2]);
								numSamplesCounted += 4;
							}
							for(; numSamplesCounted < kPeakLimiterDelayLength; numSamplesCounted += 4)
							{
								maxX = Max(maxX, g_PeakLimiterMaxTable[(numSamplesCounted + curSample - kPeakLimiterDelayLength) >> 2]);
							}

							const f32 env = (a * maxX) + ((1.f-a)*lastEnv);
							// prevent divide by zero: clamp env to 1.f if env is super small, since it will be ignored in the next fsel anyway (being smaller
							// than gain)
							const f32 safeEnv = Selectf(env - 0.0001f, env, 1.f);
							gain = Selectf(thresh - env, 1.0f, Min(1.0f, thresh/safeEnv));
							lastEnv = env;
							g_PeakLimiterOutputBuffer[curSample] = gain * delayedSample * m_GainLin;
						}
						m_LastEnvelope[inputChannelIndex] = lastEnv;

						// populate delay line with last N samples from this buffer
						sysMemCpy(&m_DelayBuffer[inputChannelIndex], &inputData[kMixBufNumSamples-kPeakLimiterDelayLength], kPeakLimiterDelayLength*sizeof(f32));
						// overwrite the output
						sysMemCpy(inputData, &g_PeakLimiterOutputBuffer, kMixBufNumSamples*sizeof(f32));
					}
				}
			}
			else//g_OptimizedCompressor
			{
				for(u32 inputChannelIndex=0; inputChannelIndex < m_NumInputChannels; inputChannelIndex++)
				{
					if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId && m_ChannelMask & (1 << inputChannelIndex))
					{
						f32 *inputData = buffer.ChannelBuffers[inputChannelIndex];
						f32 lastEnv = m_LastEnvelope[inputChannelIndex];
						for(u32 curSample = 0; curSample < kMixBufNumSamples; curSample++)
						{
							float gain;

							const f32 delayedSample = (curSample < kPeakLimiterDelayLength?m_DelayBuffer[inputChannelIndex][curSample]:inputData[curSample-kPeakLimiterDelayLength]);
							const f32 currentSample = inputData[curSample];

							const f32 x = Abs(currentSample);
							const f32 a = Selectf(x - lastEnv, attack, release);
							//const f32 a = (x>m_LastEnvelope[inputChannelIndex]?attack:release);

							// max filter
							f32 maxX = x;
							// get the max sample from [curSample - kPeakLimiterDelayLength] to [curSample]
							u32 numSamplesCounted = 0;
							for(u32 i = curSample; i < kPeakLimiterDelayLength; i++)
							{
								maxX = Max(maxX, Abs(m_DelayBuffer[inputChannelIndex][i]));
								numSamplesCounted++;
							}
							for(; numSamplesCounted < kPeakLimiterDelayLength; numSamplesCounted++)
							{
								maxX = Max(maxX, Abs(inputData[numSamplesCounted + curSample - kPeakLimiterDelayLength]));
							}

							const f32 env = (a * maxX) + ((1.f-a)*lastEnv);
							// prevent divide by zero: clamp env to 1.f if env is super small, since it will be ignored in the next fsel anyway (being smaller
							// than gain)
							const f32 safeEnv = Selectf(env - 0.0001f, env, 1.f);
							gain = Selectf(thresh - env, 1.0f, Min(1.0f, thresh/safeEnv));
							lastEnv = env;
							g_PeakLimiterOutputBuffer[curSample] = gain * delayedSample * m_GainLin;
						}
						m_LastEnvelope[inputChannelIndex] = lastEnv;

						// populate delay line with last N samples from this buffer
						sysMemCpy(&m_DelayBuffer[inputChannelIndex], &inputData[kMixBufNumSamples-kPeakLimiterDelayLength], kPeakLimiterDelayLength*sizeof(f32));
						// overwrite the output
						sysMemCpy(inputData, &g_PeakLimiterOutputBuffer, kMixBufNumSamples*sizeof(f32));
					}
				}
			}

		}
		else
		{
			f32 xdn;
			f32 a;
			f32 fn;
			f32 gn;

			u32 signalIndex = 0;

			for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
			{
				if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId && m_ChannelMask & (1 << inputChannelIndex))
				{
					//We need to process this input channel.
					f32* pfSample = buffer.ChannelBuffers[inputChannelIndex];

					// Loop through all the samples
					for(u32 j=0; j<kMixBufNumSamples; j++)
					{
						//----------------------------------------------------------------------
						// Level detection
						//----------------------------------------------------------------------

						// Running Average: (((n-1) * average) + newSample) / n
						xdn  = ((m_RMSWindow1 * m_xdn1[signalIndex]) + Abs(*pfSample)) * m_OneOverRMSWindow;
						m_xdn1[signalIndex] = xdn;

						xdn = Max(xdn, 1.0E-14f);
						fn = Max(m_InverseSlopedTresholdLin * Powf(xdn, m_Slope), 1.0f); //Don't expand.

						//----------------------------------------------------------------------
						// Attack / release time adjustment
						//----------------------------------------------------------------------

						a = Selectf(fn - m_fn1[signalIndex], m_AttackTime, m_ReleaseTime);
						gn = ((fn - m_gn1[signalIndex]) * a) + m_gn1[signalIndex];

						//----------------------------------------------------------------------
						// Adjust the signal
						//----------------------------------------------------------------------

						// Output
						*pfSample++ *= (m_GainLin / gn);

						// Save samples
						m_fn1[signalIndex] = fn;
						m_gn1[signalIndex] = gn;
					}

					signalIndex++;
				}
			} //inputChannelIndex
		}
	}

	void audCompressorEffect::SetParam(const u32 paramId, const f32 value)
	{
		bool recomputeInverseThreshSlope = false;
		switch(paramId)
		{
		case GainDb:
			m_GainLin = audDriverUtil::ComputeLinearVolumeFromDb(value);
			break;
		case ThresholdDb:
			m_ThresholdLin = audDriverUtil::ComputeDbVolumeFromLinear(value);
			recomputeInverseThreshSlope = true;
			break;
		case Ratio:
			// Slope calculation
			m_Slope = 1.0f - (1.0f / value);
			recomputeInverseThreshSlope = true;
			break;
		case Attack:
			m_AttackTime  = 1.0f - exp(-2.2f * g_OneOverNativeSampleRate / value);
			break;
		case Release:
			m_ReleaseTime = 1.0f - exp(-2.2f * g_OneOverNativeSampleRate / value);
			break;
		case Average:
			{
				f32 RMSWindow = value * (f32)kMixerNativeSampleRate;
				m_RMSWindow1	   = RMSWindow - 1.0f;
				m_OneOverRMSWindow = 1.0f / RMSWindow;
			}
			break;
		case PeakLimiterAttack:
			m_PeakLimiterAttack = value;
			break;
		case PeakLimiterRelease:
			m_PeakLimiterRelease = value;
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audCompressorEffect u32 param id (val: %u; as f32: %f)", paramId, *(u32*)&value, value);
			break;
		}

		if(recomputeInverseThreshSlope)
		{
			m_InverseSlopedTresholdLin = 1.0f / Powf(m_ThresholdLin, m_Slope);
		}
	}

	void audCompressorEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case PeakLimiterMode:
			m_PeakLimiterMode = (value != 0);
			break;
		case Bypass:
			m_Bypass = (value != 0);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audCompressorEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audCompressorEffect::Shutdown(void)
	{
	}

} // namespace rage

