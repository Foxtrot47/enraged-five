//
// audioeffecttypes/convolutioneffect_xenon.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON && 0

#include "convolutioneffect_xenon.h"

#include "effect_xenon.h"
#include "effectdefs.h"
#include "fft.h"

#include "file/asset.h"
#include "system/new.h"

namespace rage {

audConvolutionEffectXenon::audConvolutionEffectXenon(LPVOID context)
{
	m_Context = context;
	m_RefCount = 1;
	m_IsPrimed = false;
	m_FftBufferIndex = 0;
	m_NumConvBlocks = 0;

	m_ReverbFrequencyTaps = NULL;
	m_ReverbFrequencyTapsUnaligned = NULL;
    m_FrequencyTaps = NULL;
	m_FrequencyTapsUnaligned = NULL;
	m_FrequencyTapBlocksForChannel = NULL;
	m_IfftBuffer = NULL;
	m_IfftBufferUnaligned = NULL;
	m_PrevInputSamples = NULL;
	m_PrevInputSamplesUnaligned = NULL;
	m_PrevInputSamplesForChannel = NULL;
	m_FftW = NULL;
	m_FftIp = NULL;

#if AUDIO_BENCHMARK_CONVOLUTION
	m_FrameIndex = 0;
	m_ConvTimer = NULL;
	m_FftTimer = NULL;
	m_PermTimer1 = NULL;
	m_PermTimer2 = NULL;
	m_MuxTimer = NULL;
#endif // AUDIO_BENCHMARK_CONVOLUTION
}

HRESULT audConvolutionEffectXenon::Init(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator)
{
	u32 i, i2;

	Assert(!m_FrequencyTaps);

	m_NumChannels = ((tEffectInit *)init)->numChannels;
	ConvolutionEffect *effectMetadata = (ConvolutionEffect *)(((tEffectInit *)init)->metadata);
	if(effectMetadata->DataFilePathLen == 0)
	{
		AssertMsg(0, "Error: No impulse response path specified for convolution effect\n");
		return E_FAIL;
	}

	char irPath[256];
	//Extract Pascal-formatted path string.
	strncpy(irPath, effectMetadata->DataFilePath, effectMetadata->DataFilePathLen);
	irPath[effectMetadata->DataFilePathLen] = '\0';

	//Load impulse response data.
	fiStream *stream = ASSET.Open(irPath, "");
	if(!stream)
	{
        AssertMsg(0, "Error: Failed to load reverb data\n");
		return E_FAIL;
	}

	s32 irSamples = stream->Size() / sizeof(f32);
	if(irSamples <= 0)
	{
		AssertMsg(0, "Error: No impulse response found for convolution effect\n");
		return E_FAIL;
	}

	m_NumConvBlocks = (u16)((u32)irSamples / g_NumConvBlockSamples);
	if((m_NumConvBlocks * g_NumConvBlockSamples) != (u32)irSamples)
	{
		AssertMsg(0, "Error: Invalid impulse response for convolution effect\n");
		return E_FAIL;
	}

	m_ReverbFrequencyTapsUnaligned = XAUDIO_BATCHALLOC_ALLOC(f32, irSamples + (128 / sizeof(f32)), allocator);
	m_ReverbFrequencyTaps = (f32 *)(((u32)m_ReverbFrequencyTapsUnaligned + 0x80) & 0xFFFFFF80);
	XMemSet128(m_ReverbFrequencyTaps, 0, irSamples * sizeof(f32));
	stream->Read(m_ReverbFrequencyTaps, irSamples * sizeof(f32));
	stream->Close();

	//Allocate memory for working buffers.
	m_FrequencyTapsUnaligned = XAUDIO_BATCHALLOC_ALLOC(f32, (m_NumChannels * irSamples) + (128 / sizeof(f32)), allocator);
	m_FrequencyTaps = (f32 *)(((u32)m_FrequencyTapsUnaligned + 0x80) & 0xFFFFFF80);
	XMemSet128(m_FrequencyTaps, 0, m_NumChannels * irSamples * sizeof(f32));

	m_IfftBufferUnaligned = XAUDIO_BATCHALLOC_ALLOC(f32, g_NumConvBlockSamples + (128 / sizeof(f32)),
		allocator);
	m_IfftBuffer = (f32 *)(((u32)m_IfftBufferUnaligned + 0x80) & 0xFFFFFF80);
	XMemSet128(m_IfftBuffer, 0, g_NumConvBlockSamples * sizeof(f32));

	m_PrevInputSamplesUnaligned = XAUDIO_BATCHALLOC_ALLOC(f32, (m_NumChannels * XAUDIOFRAMESIZE_NATIVE) +
		(128 / sizeof(f32)), allocator);
	m_PrevInputSamples = (f32 *)(((u32)m_PrevInputSamplesUnaligned + 0x80) & 0xFFFFFF80);
	XMemSet128(m_PrevInputSamples, 0, m_NumChannels * XAUDIOFRAMESIZE_NATIVE * sizeof(f32));

	m_FftIp = XAUDIO_BATCHALLOC_ALLOC(s32, 2 + (1 << (int)(logf(((f32)g_NumConvBlockSamples / 2.0f) + 0.5f) / logf(2.0f)) /
		2), allocator);
	m_FftIp[0] = 0;
	m_FftW = XAUDIO_BATCHALLOC_ALLOC(f32, g_NumConvBlockSamples / 2, allocator);

	//Create friendly lookups.
	m_FrequencyTapBlocksForChannel = XAUDIO_BATCHALLOC_ALLOC(f32 **, m_NumChannels, allocator);
	m_PrevInputSamplesForChannel = XAUDIO_BATCHALLOC_ALLOC(f32 *, m_NumChannels, allocator);
	for(i=0; i<m_NumChannels; i++)
	{
		m_FrequencyTapBlocksForChannel[i] = XAUDIO_BATCHALLOC_ALLOC(f32 *, m_NumConvBlocks, allocator);
		for(i2=0; i2<m_NumConvBlocks; i2++)
		{
			m_FrequencyTapBlocksForChannel[i][i2] = m_FrequencyTaps + (i * irSamples) + (i2 * g_NumConvBlockSamples);
		}

		m_PrevInputSamplesForChannel[i] = m_PrevInputSamples + (i * XAUDIOFRAMESIZE_NATIVE);
	}

#if AUDIO_BENCHMARK_CONVOLUTION
	m_ConvTimer = rage_new sysPerformanceTimer("ConvTimer");
	m_FftTimer = rage_new sysPerformanceTimer("FftTimer");
	m_PermTimer1 = rage_new sysPerformanceTimer("PermTimer1");
	m_PermTimer2 = rage_new sysPerformanceTimer("PermTimer2");
	m_MuxTimer = rage_new sysPerformanceTimer("MuxTimer");
#endif // AUDIO_BENCHMARK_CONVOLUTION

	return S_OK;
}

HRESULT audConvolutionEffectXenon::Process(IXAudioFrameBuffer *inputBuffer, IXAudioFrameBuffer * /*outputBuffer*/)
{
	__vector4 inputReal1, inputImag1, inputReal2, inputImag2, inputReal3, inputImag3, inputReal4, inputImag4;
	__vector4 inputComplexA, inputComplexB;
	__vector4 reverbReal1, reverbImag1, reverbReal2, reverbImag2, reverbReal3, reverbImag3, reverbReal4, reverbImag4;
	__vector4 accumulatorReal1, accumulatorReal2, accumulatorReal3, accumulatorReal4;
	__vector4 accumulatorImag1, accumulatorImag2, accumulatorImag3, accumulatorImag4;
	__vector4 accumulatorsComplex[16];
	__vector4 permuteReal, permuteImag, permuteComplexA, permuteComplexB;
	__vector4 normalizationFactor = {{
		2.0f / (f32)g_NumConvBlockSamples,
		2.0f / (f32)g_NumConvBlockSamples,
        2.0f / (f32)g_NumConvBlockSamples,
		2.0f / (f32)g_NumConvBlockSamples }};

	f32 * __restrict currentFrequencyTapsBuffer;
	f32 * __restrict currentFrequencyTapsBufferPlusAcc;
	f32 * __restrict currentReverbTapsBufferPlusAcc;

	u32 channelIndex, accumulatorIndex, fftBufferIndex, fftBlockIndex;

	permuteReal.u[0] = 0x00010203;
	permuteReal.u[1] = 0x08090a0b;
	permuteReal.u[2] = 0x10111213;
	permuteReal.u[3] = 0x18191a1b;

	permuteImag.u[0] = 0x04050607;
	permuteImag.u[1] = 0x0c0d0e0f;
	permuteImag.u[2] = 0x14151617;
	permuteImag.u[3] = 0x1c1d1e1f;

	permuteComplexA.u[0] = 0x00010203;
	permuteComplexA.u[1] = 0x10111213;
	permuteComplexA.u[2] = 0x04050607;
	permuteComplexA.u[3] = 0x14151617;

	permuteComplexB.u[0] = 0x08090a0b;
	permuteComplexB.u[1] = 0x18191a1b;
	permuteComplexB.u[2] = 0x0c0d0e0f;
	permuteComplexB.u[3] = 0x1c1d1e1f;

	u32 accumulatorAlignmentOffset = (128 - (((u32)accumulatorsComplex) % 128)) / (sizeof(__vector4));

	Assert(inputBuffer);

#if AUDIO_BENCHMARK_CONVOLUTION
	m_ConvTimer->Start();
#endif // AUDIO_BENCHMARK_CONVOLUTION

	XAUDIOFRAMEBUFDATA frameBufferData;
	inputBuffer->GetProcessingData(&frameBufferData);
	//Verify the format is "internal" (float.)
	Assert(XAUDIOSAMPLETYPE_NATIVE == frameBufferData.Format.SampleType);
	Assert(frameBufferData.Format.ChannelCount <= m_NumChannels);
	if(frameBufferData.Format.ChannelCount > m_NumChannels)
	{
		return E_FAIL;
	}

	for(channelIndex=0; channelIndex<frameBufferData.Format.ChannelCount; channelIndex++)
	{
		currentFrequencyTapsBuffer = m_FrequencyTapBlocksForChannel[channelIndex][m_FftBufferIndex];

		//Copy last frame's input samples into first half of (output) frequency tap buffer for inplace conversion.
		XMemCpy128(currentFrequencyTapsBuffer, m_PrevInputSamplesForChannel[channelIndex], XAUDIOFRAMESIZE_NATIVE *
			sizeof(f32));

		//Copy input samples into second half of (output) frequency tap buffer for inplace conversion.
		XMemCpy128(currentFrequencyTapsBuffer + XAUDIOFRAMESIZE_NATIVE, frameBufferData.pSampleBuffer +
			(channelIndex * XAUDIOFRAMESIZE_NATIVE), XAUDIOFRAMESIZE_NATIVE * sizeof(f32));

		//Backup this frame's input samples for use next frame.
		XMemCpy128(m_PrevInputSamplesForChannel[channelIndex], frameBufferData.pSampleBuffer +
			(channelIndex * XAUDIOFRAMESIZE_NATIVE), XAUDIOFRAMESIZE_NATIVE * sizeof(f32));

#if AUDIO_BENCHMARK_CONVOLUTION
		m_FftTimer->Start();
#endif // AUDIO_BENCHMARK_CONVOLUTION

		//Perform a single FFT of the new sample data.
		audFft::Rdft(g_NumConvBlockSamples, 1, currentFrequencyTapsBuffer, m_FftIp, m_FftW);

#if AUDIO_BENCHMARK_CONVOLUTION
		m_FftTimer->Stop();

		m_PermTimer1->Start();
#endif // AUDIO_BENCHMARK_CONVOLUTION

		//
		//Permute FFT bins from interleaved real/imaginary to interleaved block of 4 real/4 imaginary.
		//
		//Prefetch first half of FFT data - utilizing all 8 cache lines.
		__dcbt(0, currentFrequencyTapsBuffer);
		__dcbt(128, currentFrequencyTapsBuffer);
		__dcbt(256, currentFrequencyTapsBuffer);
		__dcbt(384, currentFrequencyTapsBuffer);
		__dcbt(512, currentFrequencyTapsBuffer);
		__dcbt(640, currentFrequencyTapsBuffer);
		__dcbt(768, currentFrequencyTapsBuffer);
		__dcbt(896, currentFrequencyTapsBuffer);

		//Prefetch second half of FFT data - utilizing all 8 cache lines (will stall waiting for previous prefetches.)
		__dcbt(1024, currentFrequencyTapsBuffer);
		__dcbt(1152, currentFrequencyTapsBuffer);
		__dcbt(1280, currentFrequencyTapsBuffer);
		__dcbt(1408, currentFrequencyTapsBuffer);
		__dcbt(1536, currentFrequencyTapsBuffer);
		__dcbt(1664, currentFrequencyTapsBuffer);
		__dcbt(1792, currentFrequencyTapsBuffer);
		__dcbt(1920, currentFrequencyTapsBuffer);

		for(fftBufferIndex=0; fftBufferIndex<g_NumConvBlockSamples; fftBufferIndex+=32)
		{
			//Load and split input data into 4 real and 4 imaginary floats.
			inputComplexA = __lvx(currentFrequencyTapsBuffer, fftBufferIndex * sizeof(f32));
			inputComplexB = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 4) * sizeof(f32));
			accumulatorsComplex[accumulatorAlignmentOffset] = __vperm(inputComplexA, inputComplexB, permuteReal);
			accumulatorsComplex[accumulatorAlignmentOffset + 1] = __vperm(inputComplexA, inputComplexB, permuteImag);

			//Load and split input data into 4 real and 4 imaginary floats.
			inputComplexA = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 8) * sizeof(f32));
			inputComplexB = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 12) * sizeof(f32));
			accumulatorsComplex[accumulatorAlignmentOffset + 2] = __vperm(inputComplexA, inputComplexB, permuteReal);
			accumulatorsComplex[accumulatorAlignmentOffset + 3] = __vperm(inputComplexA, inputComplexB, permuteImag);

			//Load and split input data into 4 real and 4 imaginary floats.
			inputComplexA = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 16) * sizeof(f32));
			inputComplexB = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 20) * sizeof(f32));
			accumulatorsComplex[accumulatorAlignmentOffset + 4] = __vperm(inputComplexA, inputComplexB, permuteReal);
			accumulatorsComplex[accumulatorAlignmentOffset + 5] = __vperm(inputComplexA, inputComplexB, permuteImag);

			//Load and split input data into 4 real and 4 imaginary floats.
			inputComplexA = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 24) * sizeof(f32));
			inputComplexB = __lvx(currentFrequencyTapsBuffer, (fftBufferIndex + 28) * sizeof(f32));
			accumulatorsComplex[accumulatorAlignmentOffset + 6] = __vperm(inputComplexA, inputComplexB, permuteReal);
			accumulatorsComplex[accumulatorAlignmentOffset + 7] = __vperm(inputComplexA, inputComplexB, permuteImag);

			XMemCpy128(currentFrequencyTapsBuffer + fftBufferIndex, &accumulatorsComplex[accumulatorAlignmentOffset],
				128);

		} //fftBufferIndex

#if AUDIO_BENCHMARK_CONVOLUTION
		m_PermTimer1->Stop();
#endif // AUDIO_BENCHMARK_CONVOLUTION

		//Prefetch first 4 blocks of input and reverb data - utilizing all 8 cache lines.
		__dcbt(0, m_FrequencyTapBlocksForChannel[channelIndex][m_FftBufferIndex]);
		__dcbt(0, m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex + 1) % m_NumConvBlocks]);
		__dcbt(0, m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex + 2) % m_NumConvBlocks]);
		__dcbt(0, m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex + 3) % m_NumConvBlocks]);
		__dcbt(0, &m_ReverbFrequencyTaps[0]);
		__dcbt(0, &m_ReverbFrequencyTaps[1]);
		__dcbt(0, &m_ReverbFrequencyTaps[2]);
		__dcbt(0, &m_ReverbFrequencyTaps[3]);

		for(accumulatorIndex=0; accumulatorIndex<g_NumConvBlockSamples; accumulatorIndex+=32)
		{
#if AUDIO_BENCHMARK_CONVOLUTION
			m_MuxTimer->Start();
#endif // AUDIO_BENCHMARK_CONVOLUTION

			//Zero accumulators
			accumulatorReal1 = __vzero();
			accumulatorReal2 = __vzero();
			accumulatorReal3 = __vzero();
			accumulatorReal4 = __vzero();
			accumulatorImag1 = __vzero();
			accumulatorImag2 = __vzero();
			accumulatorImag3 = __vzero();
			accumulatorImag4 = __vzero();

			for(fftBlockIndex=0; fftBlockIndex<(u32)m_NumConvBlocks-4; fftBlockIndex+=4)
			{
				//Prefetch next 4 blocks of input and reverb data - utilizing all 8 cache lines.
				__dcbt(accumulatorIndex * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					(fftBlockIndex + 4)) % m_NumConvBlocks]);
				__dcbt(accumulatorIndex * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					(fftBlockIndex + 5)) % m_NumConvBlocks]);
				__dcbt(accumulatorIndex * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					(fftBlockIndex + 6)) % m_NumConvBlocks]);
				__dcbt(accumulatorIndex * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					(fftBlockIndex + 7)) % m_NumConvBlocks]);

				__dcbt(accumulatorIndex * sizeof(f32), &m_ReverbFrequencyTaps[fftBlockIndex + 4]);
				__dcbt(accumulatorIndex * sizeof(f32), &m_ReverbFrequencyTaps[fftBlockIndex + 5]);
				__dcbt(accumulatorIndex * sizeof(f32), &m_ReverbFrequencyTaps[fftBlockIndex + 6]);
				__dcbt(accumulatorIndex * sizeof(f32), &m_ReverbFrequencyTaps[fftBlockIndex + 7]);

				//
				//Block Offset 0.
				//
				currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					fftBlockIndex) % m_NumConvBlocks] + accumulatorIndex;
				currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + (fftBlockIndex *
					g_NumConvBlockSamples) + accumulatorIndex;

				inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
				reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
				inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
				reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
				inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
				reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
				inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
				reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

				inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
				inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
				inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
				inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

				reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
				reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
				reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
				reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

				accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
				accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
				accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
				accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

				accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
				accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
				accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
				accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

				accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
				accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
				accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
				accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

				accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
				accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
				accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
				accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

				//
				//Block Offset 1.
				//
				currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					fftBlockIndex + 1) % m_NumConvBlocks] + accumulatorIndex;
				currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + ((fftBlockIndex + 1) *
					g_NumConvBlockSamples) + accumulatorIndex;

				inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
				reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
				inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
				reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
				inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
				reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
				inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
				reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

				inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
				inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
				inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
				inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

				reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
				reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
				reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
				reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

				accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
				accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
				accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
				accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

				accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
				accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
				accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
				accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

				accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
				accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
				accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
				accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

				accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
				accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
				accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
				accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

				//
				//Block Offset 2.
				//
				currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					fftBlockIndex + 2) % m_NumConvBlocks] + accumulatorIndex;
				currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + ((fftBlockIndex + 2) *
					g_NumConvBlockSamples) + accumulatorIndex;

				inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
				reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
				inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
				reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
				inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
				reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
				inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
				reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

				inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
				inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
				inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
				inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

				reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
				reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
				reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
				reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

				accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
				accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
				accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
				accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

				accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
				accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
				accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
				accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

				accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
				accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
				accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
				accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

				accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
				accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
				accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
				accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

				//
				//Block Offset 3.
				//
				currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
					fftBlockIndex + 3) % m_NumConvBlocks] + accumulatorIndex;
				currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + ((fftBlockIndex + 3) *
					g_NumConvBlockSamples) + accumulatorIndex;

				inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
				reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
				inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
				reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
				inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
				reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
				inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
				reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

				inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
				inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
				inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
				inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

				reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
				reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
				reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
				reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

				accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
				accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
				accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
				accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

				accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
				accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
				accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
				accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

				accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
				accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
				accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
				accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

				accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
				accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
				accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
				accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

			} //fftBlockIndex

			//
			//Special-case final iteration on FFT blocks.
			//
			//Prefetch first 4 blocks of input and reverb data for next set of accumulator indices - utilizing all 8 cache
			//lines.
			//Note: This will prefetch beyond the end of the input and reverb arrays, but this should be safe so don't
			//waste an 'if'.
			__dcbt((accumulatorIndex + 32) * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][
				m_FftBufferIndex]);
			__dcbt((accumulatorIndex + 32) * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][
				(m_FftBufferIndex + 1) % m_NumConvBlocks]);
			__dcbt((accumulatorIndex + 32) * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][
				(m_FftBufferIndex + 2) % m_NumConvBlocks]);
			__dcbt((accumulatorIndex + 32) * sizeof(f32), m_FrequencyTapBlocksForChannel[channelIndex][
				(m_FftBufferIndex + 3) % m_NumConvBlocks]);

			__dcbt((accumulatorIndex + 32) * sizeof(f32), &m_ReverbFrequencyTaps[0]);
			__dcbt((accumulatorIndex + 32) * sizeof(f32), &m_ReverbFrequencyTaps[1]);
			__dcbt((accumulatorIndex + 32) * sizeof(f32), &m_ReverbFrequencyTaps[2]);
			__dcbt((accumulatorIndex + 32) * sizeof(f32), &m_ReverbFrequencyTaps[3]);

			//
			//Block Offset 0.
			//
			currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
				fftBlockIndex) % m_NumConvBlocks] + accumulatorIndex;
			currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + (fftBlockIndex *
				g_NumConvBlockSamples) + accumulatorIndex;

			inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
			reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
			inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
			reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
			inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
			reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
			inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
			reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

			inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
			inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
			inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
			inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

			reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
			reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
			reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
			reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

			accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
			accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
			accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
			accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

			accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
			accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
			accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
			accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

			accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
			accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
			accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
			accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

			accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
			accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
			accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
			accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

			//
			//Block Offset 1.
			//
			currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
				fftBlockIndex + 1) % m_NumConvBlocks] + accumulatorIndex;
			currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + ((fftBlockIndex + 1) *
				g_NumConvBlockSamples) + accumulatorIndex;

			inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
			reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
			inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
			reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
			inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
			reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
			inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
			reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

			inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
			inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
			inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
			inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

			reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
			reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
			reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
			reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

			accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
			accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
			accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
			accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

			accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
			accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
			accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
			accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

			accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
			accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
			accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
			accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

			accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
			accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
			accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
			accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

			//
			//Block Offset 2.
			//
			currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
				fftBlockIndex + 2) % m_NumConvBlocks] + accumulatorIndex;
			currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + ((fftBlockIndex + 2) *
				g_NumConvBlockSamples) + accumulatorIndex;

			inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
			reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
			inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
			reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
			inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
			reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
			inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
			reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

			inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
			inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
			inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
			inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

			reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
			reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
			reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
			reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

			accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
			accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
			accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
			accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

			accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
			accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
			accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
			accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

			accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
			accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
			accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
			accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

			accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
			accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
			accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
			accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

			//
			//Block Offset 3.
			//
			currentFrequencyTapsBufferPlusAcc = m_FrequencyTapBlocksForChannel[channelIndex][(m_FftBufferIndex +
				fftBlockIndex + 3) % m_NumConvBlocks] + accumulatorIndex;
			currentReverbTapsBufferPlusAcc = m_ReverbFrequencyTaps + ((fftBlockIndex + 3) *
				g_NumConvBlockSamples) + accumulatorIndex;

			inputReal1 = __lvx(currentFrequencyTapsBufferPlusAcc, 0);
			reverbReal1 = __lvx(currentReverbTapsBufferPlusAcc, 0);
			inputReal2 = __lvx(currentFrequencyTapsBufferPlusAcc, 8 * sizeof(f32));
			reverbReal2 = __lvx(currentReverbTapsBufferPlusAcc, 8 * sizeof(f32));
			inputReal3 = __lvx(currentFrequencyTapsBufferPlusAcc, 16 * sizeof(f32));
			reverbReal3 = __lvx(currentReverbTapsBufferPlusAcc, 16 * sizeof(f32));
			inputReal4 = __lvx(currentFrequencyTapsBufferPlusAcc, 24 * sizeof(f32));
			reverbReal4 = __lvx(currentReverbTapsBufferPlusAcc, 24 * sizeof(f32));

			inputImag1 = __lvx(currentFrequencyTapsBufferPlusAcc, 4 * sizeof(f32));
			inputImag2 = __lvx(currentFrequencyTapsBufferPlusAcc, 12 * sizeof(f32));
			inputImag3 = __lvx(currentFrequencyTapsBufferPlusAcc, 20 * sizeof(f32));
			inputImag4 = __lvx(currentFrequencyTapsBufferPlusAcc, 28 * sizeof(f32));

			reverbImag1 = __lvx(currentReverbTapsBufferPlusAcc, 4 * sizeof(f32));
			reverbImag2 = __lvx(currentReverbTapsBufferPlusAcc, 12 * sizeof(f32));
			reverbImag3 = __lvx(currentReverbTapsBufferPlusAcc, 20 * sizeof(f32));
			reverbImag4 = __lvx(currentReverbTapsBufferPlusAcc, 28 * sizeof(f32));

			accumulatorReal1 = __vmaddfp(inputReal1, reverbReal1, accumulatorReal1);	//Compute Real1*Real2.
			accumulatorReal2 = __vmaddfp(inputReal2, reverbReal2, accumulatorReal2);	//Compute Real1*Real2.
			accumulatorReal3 = __vmaddfp(inputReal3, reverbReal3, accumulatorReal3);	//Compute Real1*Real2.
			accumulatorReal4 = __vmaddfp(inputReal4, reverbReal4, accumulatorReal4);	//Compute Real1*Real2.

			accumulatorImag1 = __vmaddfp(inputImag1, reverbReal1, accumulatorImag1);	//Compute Imag1*Real2.
			accumulatorImag2 = __vmaddfp(inputImag2, reverbReal2, accumulatorImag2);	//Compute Imag1*Real2.
			accumulatorImag3 = __vmaddfp(inputImag3, reverbReal3, accumulatorImag3);	//Compute Imag1*Real2.
			accumulatorImag4 = __vmaddfp(inputImag4, reverbReal4, accumulatorImag4);	//Compute Imag1*Real2.

			accumulatorReal1 = __vnmsubfp(inputImag1, reverbImag1, accumulatorReal1);	//Compute -Imag1*Imag2.
			accumulatorReal2 = __vnmsubfp(inputImag2, reverbImag2, accumulatorReal2);	//Compute -Imag1*Imag2.
			accumulatorReal3 = __vnmsubfp(inputImag3, reverbImag3, accumulatorReal3);	//Compute -Imag1*Imag2.
			accumulatorReal4 = __vnmsubfp(inputImag4, reverbImag4, accumulatorReal4);	//Compute -Imag1*Imag2.

			accumulatorImag1 = __vmaddfp(inputReal1, reverbImag1, accumulatorImag1);	//Compute Real1*Imag2.
			accumulatorImag2 = __vmaddfp(inputReal2, reverbImag2, accumulatorImag2);	//Compute Real1*Imag2.
			accumulatorImag3 = __vmaddfp(inputReal3, reverbImag3, accumulatorImag3);	//Compute Real1*Imag2.
			accumulatorImag4 = __vmaddfp(inputReal4, reverbImag4, accumulatorImag4);	//Compute Real1*Imag2.

#if AUDIO_BENCHMARK_CONVOLUTION
			m_MuxTimer->Stop();

			m_PermTimer2->Start();
#endif // AUDIO_BENCHMARK_CONVOLUTION

			//
			//Reorder real and imaginary accumulators back into interleaved blocks.
			//
			//
			__dcbz128(accumulatorIndex * sizeof(f32), m_IfftBuffer);

			accumulatorsComplex[accumulatorAlignmentOffset] = __vperm(accumulatorReal1, accumulatorImag1,
				permuteComplexA);
			accumulatorsComplex[accumulatorAlignmentOffset + 1] = __vperm(accumulatorReal1, accumulatorImag1,
				permuteComplexB);
			//Normalise FFT results.
			accumulatorsComplex[accumulatorAlignmentOffset] = __vmulfp(accumulatorsComplex[accumulatorAlignmentOffset],
				normalizationFactor);
			accumulatorsComplex[accumulatorAlignmentOffset + 1] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 1], normalizationFactor);
			
			accumulatorsComplex[accumulatorAlignmentOffset + 2] = __vperm(accumulatorReal2, accumulatorImag2,
				permuteComplexA);
			accumulatorsComplex[accumulatorAlignmentOffset + 3] = __vperm(accumulatorReal2, accumulatorImag2,
				permuteComplexB);
			//Normalise FFT results.
			accumulatorsComplex[accumulatorAlignmentOffset + 2] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 2], normalizationFactor);
			accumulatorsComplex[accumulatorAlignmentOffset + 3] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 3], normalizationFactor);

			accumulatorsComplex[accumulatorAlignmentOffset + 4] = __vperm(accumulatorReal3, accumulatorImag3,
				permuteComplexA);
			accumulatorsComplex[accumulatorAlignmentOffset + 5] = __vperm(accumulatorReal3, accumulatorImag3,
				permuteComplexB);
			//Normalise FFT results.
			accumulatorsComplex[accumulatorAlignmentOffset + 4] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 4], normalizationFactor);
			accumulatorsComplex[accumulatorAlignmentOffset + 5] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 5], normalizationFactor);

			accumulatorsComplex[accumulatorAlignmentOffset + 6] = __vperm(accumulatorReal4, accumulatorImag4,
				permuteComplexA);
			accumulatorsComplex[accumulatorAlignmentOffset + 7] = __vperm(accumulatorReal4, accumulatorImag4,
				permuteComplexB);
			//Normalise FFT results.
			accumulatorsComplex[accumulatorAlignmentOffset + 6] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 6], normalizationFactor);
			accumulatorsComplex[accumulatorAlignmentOffset + 7] = __vmulfp(accumulatorsComplex[
				accumulatorAlignmentOffset + 7], normalizationFactor);

			//Write accumulators back to inverse FFT buffer.
			XMemCpy128(m_IfftBuffer + accumulatorIndex, &accumulatorsComplex[accumulatorAlignmentOffset], 128);

#if AUDIO_BENCHMARK_CONVOLUTION
			m_PermTimer2->Stop();
#endif // AUDIO_BENCHMARK_CONVOLUTION

		} //accumulatorIndex

#if AUDIO_BENCHMARK_CONVOLUTION
		m_FftTimer->Start();
#endif // AUDIO_BENCHMARK_CONVOLUTION

		//Perform inverse FFT on this effected block.
		audFft::Rdft(g_NumConvBlockSamples, -1, m_IfftBuffer, m_FftIp, m_FftW);

#if AUDIO_BENCHMARK_CONVOLUTION
		m_FftTimer->Stop();
#endif // AUDIO_BENCHMARK_CONVOLUTION

		//Copy the second half of the processed samples back into input buffer (this is an inplace effect.)
		sysMemCpy(frameBufferData.pSampleBuffer + (channelIndex * XAUDIOFRAMESIZE_NATIVE), m_IfftBuffer +
			XAUDIOFRAMESIZE_NATIVE, XAUDIOFRAMESIZE_NATIVE * sizeof(f32));

	} //channelIndex

	m_FftBufferIndex = (m_FftBufferIndex + m_NumConvBlocks - 1) % m_NumConvBlocks;

	m_IsPrimed = true;

#if AUDIO_BENCHMARK_CONVOLUTION
	m_ConvTimer->Stop();

	if(++m_FrameIndex == 375)
	{
		printf("Total: %7.3lfms (%5.2lf%%)\n", m_ConvTimer->GetTimeMS(), m_ConvTimer->GetTimeMS() / 20.0);
		printf("FFT/IFFT: %7.3lfms (%5.2lf%%)\n", m_FftTimer->GetTimeMS(), m_FftTimer->GetTimeMS() / 20.0);
		printf("Perm1: %7.3lfms (%5.2lf%%)\n", m_PermTimer1->GetTimeMS(), m_PermTimer1->GetTimeMS() / 20.0);
		printf("Perm2: %7.3lfms (%5.2lf%%)\n", m_PermTimer2->GetTimeMS(), m_PermTimer2->GetTimeMS() / 20.0);
		printf("Mux: %7.3lfms (%5.2lf%%)\n", m_MuxTimer->GetTimeMS(), m_MuxTimer->GetTimeMS() / 20.0);
		m_ConvTimer->Reset();
		m_FftTimer->Reset();
		m_PermTimer1->Reset();
		m_PermTimer2->Reset();
		m_MuxTimer->Reset();
		m_FrameIndex = 0;
	}
#endif // AUDIO_BENCHMARK_CONVOLUTION

	return S_OK;
}

HRESULT audConvolutionEffectXenon::GetInfo(XAUDIOFXINFO *info)
{
	Assert(info);
	info->DataFlow = XAUDIODATAFLOW_INPLACE;
	info->TrailFrames = 1;

	return S_OK;
}

HRESULT audConvolutionEffectXenon::GetParam(XAUDIOFXPARAMID UNUSED_PARAM(paramId),
	XAUDIOFXPARAMTYPE UNUSED_PARAM(paramType), XAUDIOFXPARAM *UNUSED_PARAM(param))
{
	AssertMsg(0, "Error: audConvolutionEffectXenon::GetParam is not valid for this effect\n");
	return E_FAIL;
}

HRESULT audConvolutionEffectXenon::SetParam(XAUDIOFXPARAMID UNUSED_PARAM(paramId),
	XAUDIOFXPARAMTYPE UNUSED_PARAM(paramType), const XAUDIOFXPARAM *UNUSED_PARAM(param))
{
	AssertMsg(0, "Error: audConvolutionEffectXenon::SetParam is not valid for this effect\n");
	return E_FAIL;
}

HRESULT audConvolutionEffectXenon::GetContext(LPVOID *context)
{
	Assert(context);
	*context = m_Context;
	return S_OK;
}

HRESULT QuerySizeConvolutionEffectBuffersXenon(LPCXAUDIOFXINIT init, LPDWORD effectSize)
{
	Assert(init);

	u8 numChannels = ((tEffectInit *)init)->numChannels;
	ConvolutionEffect *effectMetadata = (ConvolutionEffect *)(((tEffectInit *)init)->metadata);
	if(effectMetadata->DataFilePathLen == 0)
	{
		AssertMsg(0, "Error: No impulse response path specified for convolution effect\n");
		return E_FAIL;
	}

	char irPath[256];
	//Extract Pascal-formatted path string.
	strncpy(irPath, effectMetadata->DataFilePath, effectMetadata->DataFilePathLen);
	irPath[effectMetadata->DataFilePathLen] = '\0';

	//Find length of impulse response.
	fiStream *stream = ASSET.Open(irPath, "");
	if(!stream)
	{
		AssertMsg(0, "Error: Failed to open impulse response for convolution effect\n");
		return E_FAIL;
	}

	s32 irSamples = stream->Size() / sizeof(f32);
	stream->Close();

	if(irSamples <= 0)
	{
		AssertMsg(0, "Error: No impulse response found for convolution effect\n");
		return E_FAIL;
	}

	u32 numConvBlocks = (u32)irSamples / g_NumConvBlockSamples;
	if((numConvBlocks * g_NumConvBlockSamples) != (u32)irSamples)
	{
		AssertMsg(0, "Error: Invalid impulse response for convolution effect\n");
		return E_FAIL;
	}

	//Add FFT buffers.
	*effectSize = (sizeof(f32) * numChannels * irSamples) + 128;
	//Add inverse FFT buffer.
	*effectSize += (sizeof(f32) * g_NumConvBlockSamples) + 128;
	//Add input sample backup buffers.
	*effectSize += (sizeof(f32) * numChannels * XAUDIOFRAMESIZE_NATIVE) + 128;
	//Add impulse response tap buffer.
	*effectSize += (sizeof(f32) * irSamples) + 128;
	//Add FFT working buffers.
	*effectSize += sizeof(s32) * (2 + (1 << (int)(logf(((f32)g_NumConvBlockSamples / 2.0f) + 0.5f) / logf(2.0f)) / 2));
	*effectSize += sizeof(f32) * g_NumConvBlockSamples / 2;
	//Add lookup tables.
	*effectSize += sizeof(f32 **) * numChannels;
	*effectSize += sizeof(f32 *) * numChannels;
	*effectSize += sizeof(f32 *) * numChannels * numConvBlocks;

	return S_OK;
}

HRESULT QuerySizeConvolutionEffectXenon(LPCXAUDIOFXINIT init, LPDWORD effectSize)
{
	Assert(init);
	u32 bufferSize = 0;
	u32 objectSize = 0;
	QuerySizeConvolutionEffectBuffersXenon(init, (LPDWORD)&bufferSize);
	objectSize = sizeof(audConvolutionEffectXenon);
	*effectSize = bufferSize + objectSize;
	return S_OK;
}

HRESULT CreateConvolutionEffectXenon(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator, IXAudioEffect **effect)
{
	Assert(init);
	Assert(effect);
	Assert(allocator);

	audConvolutionEffectXenon *convolutionEffect = NULL;
	convolutionEffect = XAUDIO_BATCHALLOC_NEW(audConvolutionEffectXenon(((tEffectInit *)init)->effectHeader.pContext),
		allocator);
	convolutionEffect->Init(init, allocator);
	*effect = convolutionEffect;

	return S_OK;
}

ULONG audConvolutionEffectXenon::AddRef(void)
{
	return ++m_RefCount;
}

ULONG audConvolutionEffectXenon::Release(void)
{
	if(0 == --m_RefCount)
	{
		//Free the data we're holding.
		XAUDIO_BATCHALLOC_FREE(m_ReverbFrequencyTapsUnaligned);
		XAUDIO_BATCHALLOC_FREE(m_FrequencyTapsUnaligned);
		XAUDIO_BATCHALLOC_FREE(m_IfftBufferUnaligned);
		XAUDIO_BATCHALLOC_FREE(m_PrevInputSamplesUnaligned);

		XAUDIO_BATCHALLOC_FREE(m_FftIp);
		XAUDIO_BATCHALLOC_FREE(m_FftW);

		for(int i=0; i<m_NumChannels; i++)
		{
			XAUDIO_BATCHALLOC_FREE(m_FrequencyTapBlocksForChannel[i]);
		}

		XAUDIO_BATCHALLOC_FREE(m_FrequencyTapBlocksForChannel);
		XAUDIO_BATCHALLOC_FREE(m_PrevInputSamplesForChannel);

#if AUDIO_BENCHMARK_CONVOLUTION
		delete m_ConvTimer;
		delete m_FftTimer;
		delete m_PermTimer1;
		delete m_PermTimer2;
		delete m_MuxTimer;
#endif // AUDIO_BENCHMARK_CONVOLUTION

		delete this;
		return 0;
	}
	else
	{
		return m_RefCount;
	}
}

} // namespace rage

#endif // __XENON
