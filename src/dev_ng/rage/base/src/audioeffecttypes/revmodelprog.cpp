#include "revmodelprog.h"

namespace rage
{
	using namespace Vec;

	Vector_4V_Out revModelProgenitor_V4::Process(Vector_4V_In lInput, Vector_4V_In rInput)
	{
		n_15_16.SetDecay(Decay2);
		n_15_16.SetDiffusion(Diffusion2);
		n_17_18.SetDecay(Decay3);
		n_17_18.SetDiffusion(Diffusion1);

		n_24_27.SetDecay1(Decay1);
		n_24_27.SetDefinition(Definition);
		n_24_27.SetDecay2(Decay2);
		n_24_27.SetDecayDiffusion(DecayDiffusion);

		n_31_37.SetDecay1(Decay1);
		n_31_37.SetDecay2(Decay2);
		n_31_37.SetDecayDiffusion(DecayDiffusion);
		n_31_37.SetDefinition(Definition);
		n_31_37.SetChorusDry(ChorusDry);
		n_31_37.SetChorusWet(ChorusWet);
		

		n_19_20.SetDecay(Decay2);
		n_19_20.SetDiffusion(Diffusion2);
		n_21_22.SetDecay(Decay3);
		n_21_22.SetDiffusion(Diffusion1);

		n_42_45.SetDecay1(Decay1);
		n_42_45.SetDecay2(Decay2);
		n_42_45.SetDecayDiffusion(DecayDiffusion);
		n_42_45.SetDefinition(Definition);

		n_49_55.SetDecay1(Decay1);
		n_49_55.SetDecay2(Decay2);
		n_49_55.SetDefinition(Definition);
		n_49_55.SetDecayDiffusion(DecayDiffusion);
		n_49_55.SetChorusDry(ChorusDry);
		n_49_55.SetChorusWet(ChorusWet);

		dcL.SetFc(V4VConstant(V_FIVE));
		dcR.SetFc(V4VConstant(V_FIVE));


		n_59_60.SetDamping(HF_Bandwidth); // InputDamping
		n_64_65.SetDamping(HF_Bandwidth);

		n_11_12.SetDamping(Damping);//lfo?
		n_13_14.SetDamping(Damping);//-lfo?

		n_7_8.SetDamping(V4VConstantSplat<0x3F600000>()); //0.875f;
		n_9_10.SetDamping(V4VConstantSplat<0x3F600000>()); //0.875f;
		
		Vector_4V outL = V4VConstant(V_ZERO), outR = V4VConstant(V_ZERO);

	/*		const float n60 = n_59_60.Read();
		const float n59 = lInput * 0.812f + n60 * HF_Bandwidth;
		n_59_60.Write(n59);
		outL = n_60_61.Process(n60);

		const float n65 = n_64_65.Read();
		const float n64 = rInput * 0.812f + n65 * HF_Bandwidth;
		n_64_65.Write(n64);
		outR = n_65_66.Process(n65);
	*/

		
		outL = n_60_61.Process(n_59_60.Process(dcL.Process(lInput)));
		outR = n_65_66.Process(n_64_65.Process(dcR.Process(rInput)));
		
		
		Vector_4V crossL = n_37_39a.Read();
		Vector_4V crossR = n_55_58.Read();

		Vector_4V bassBoost = V4VConstant(V_ZERO);// V4VConstantSplat<0x3D75C28F>();// 0.06f;

		outL = V4AddScaled(outL, Decay1, (V4Add(V4Add(crossR, bassBoost), n_9_10.Process(crossR))));
		outR = V4AddScaled(outR, Decay1, (V4Add(V4Add(crossL, bassBoost), n_7_8.Process(crossL))));

		
		outL = n_17_18.Process(n_16_17.Process(n_15_16.Process(n_11_12.Process(outL))));
		outR = n_21_22.Process(n_19_20.Process(n_13_14.Process(outR)));

		n_37_39a.Process(n_31_37.Process(n_27_31.Process(n_24_27.Process(outL))));
		n_49_55.Process(n_45_49.Process(n_42_45.Process(n_41_42.Process(outR))));


		Vector_4V c938 = V4VConstantSplat<0x3F7020C5>(); // 0.938
		Vector_4V c469 = V4VConstantSplat<0x3EF020C5>(); // 0.469
		Vector_4V c438 = V4VConstantSplat<0x3EE04189>(); // 0.438
		Vector_4V c125 = V4VConstantSplat<0x3E000000>(); // 0.125

		const Vector_4V Aout = V4Scale(n_27_31.ReadTap(276), c938);
		Vector_4V b1 = V4AddScaled(V4Scale(n_45_49.ReadTap(468), c438), n_41_42.ReadTap(625), c938);
		Vector_4V b2 = V4SubtractScaled(b1, n_27_31.ReadTap(312), c438);
		const Vector_4V Bout = V4AddScaled(b2, n_55_58.ReadTap(8), c125);
		const Vector_4V Cout = V4AddScaled(V4Scale(n_45_49.ReadTap(24), c938), n_37_39a.ReadTap(36), c469);
		Vector_4V d1 = V4AddScaled(V4Scale(n_27_31.ReadTap(40),c438), n_23_24.Read(), c938);
		const Vector_4V Dout = V4AddScaled(V4SubtractScaled(d1, n_45_49.ReadTap(192), c438), n_37_39a.ReadTap(1572), c125);

		return V4Add(V4Add(Aout,Bout),V4Add(Cout,Dout));//?
	}

} // namespace rage
