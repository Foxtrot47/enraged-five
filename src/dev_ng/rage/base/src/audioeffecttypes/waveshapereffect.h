// 
// audioeffecttypes/waveshapereffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_WAVESHAPEREFFECT_H
#define AUD_WAVESHAPEREFFECT_H


#include "filterdefs.h"
#include "audiohardware/dspeffect.h"

namespace rage {

	//
	// PURPOSE
	//  A basic waveshaper.
	//
	class audCurve;
	class audWaveshaperEffect : public audDspEffect
	{
	public:
		audWaveshaperEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		
		//void SetParams(tWaveshaperFilterSettings *params);

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		enum audWaveShaperEffectParams
		{
			InputGainDb = audEffectParamIds::WaveShaperEffect,
			OutputGainDb,
			ApplyFactor,
			TransferFunction,
			Bypass,    
		};

	private:

		void InitializeLookupTable(void);

		f32			m_InputGainLin;		//Linear input gain, passed in using dB
		f32			m_OutputGainLin;	//Linear output gain, passed in using dB
		f32			m_ApplyFactor;		//0.0 - 1.0 morph value for applying transfer function
		audCurve	m_TransferFunction;

		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;
		bool m_Bypass;
	};

} // namespace rage

#endif //AUD_WAVESHAPEREFFECT_H

