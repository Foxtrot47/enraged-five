#if __XENON

#include "interleaver_xenon.h"
#include "system/memops.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	extern ALIGNAS(16) f32 g_InterleavedWorkBuffer[6 * 256];

	void audInterleaverEffect::DoProcess(const tInterleaverEffectSettings& UNUSED_PARAM(params), f32* __restrict pData, u32 numFrames, u32 ASSERT_ONLY(numChannels))
	{
		Assert(numFrames == 256);
		Assert(numChannels == 6);
	
		Vector_4V *out = (Vector_4V*)&g_InterleavedWorkBuffer[0];

		for(u32 frameIdx = 0; frameIdx < 256; frameIdx += 4)
		{
			const Vector_4V inputSamples0 = *((Vector_4V*)&pData[0*numFrames + frameIdx]);
			const Vector_4V inputSamples1 = *((Vector_4V*)&pData[1*numFrames + frameIdx]);
			const Vector_4V inputSamples2 = *((Vector_4V*)&pData[2*numFrames + frameIdx]);
			const Vector_4V inputSamples3 = *((Vector_4V*)&pData[3*numFrames + frameIdx]);
			const Vector_4V inputSamples4 = *((Vector_4V*)&pData[4*numFrames + frameIdx]);
			const Vector_4V inputSamples5 = *((Vector_4V*)&pData[5*numFrames + frameIdx]);

			const Vector_4V inputSamples_01_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inputSamples0, inputSamples1);
			const Vector_4V inputSamples_01_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inputSamples0, inputSamples1);
			
			const Vector_4V inputSamples_23_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inputSamples2, inputSamples3);
			const Vector_4V inputSamples_23_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inputSamples2, inputSamples3);

			const Vector_4V inputSamples_45_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inputSamples4, inputSamples5);
			const Vector_4V inputSamples_45_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inputSamples4, inputSamples5);
			
			const Vector_4V samples_0123_x = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_xy,inputSamples_23_xy);
			const Vector_4V samples_4501_xy = V4PermuteTwo<X1,Y1,Z2,W2>(inputSamples_45_xy, inputSamples_01_xy);
			const Vector_4V samples_2345_y = V4PermuteTwo<Z1,W1,Z2,W2>(inputSamples_23_xy, inputSamples_45_xy);
			const Vector_4V samples_0123_z = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_zw,inputSamples_23_zw);
			const Vector_4V samples_4501_zw = V4PermuteTwo<X1,Y1,Z2,W2>(inputSamples_45_zw, inputSamples_01_zw);
			const Vector_4V samples_2345_w = V4PermuteTwo<Z1,W1,Z2,W2>(inputSamples_23_zw, inputSamples_45_zw);

			*out++ = samples_0123_x;
			*out++ = samples_4501_xy;
			*out++ = samples_2345_y;
			*out++ = samples_0123_z;
			*out++ = samples_4501_zw;
			*out++ = samples_2345_w;		
		}

		out = (Vector_4V*)pData;
		const Vector_4V *input = (Vector_4V*)&g_InterleavedWorkBuffer[0];
		for(u32 frameIdx = 0; frameIdx < 256/4; frameIdx ++)
		{
			*out++ = *input++;
			*out++ = *input++;
			*out++ = *input++;
			*out++ = *input++;
			*out++ = *input++;
			*out++ = *input++;
		}
	}
}

#endif // __XENON
