#ifndef AUD_ALLPASSFILTER_H
#define AUD_ALLPASSFILTER_H

#include "system/memops.h"

#include "audioeffecttypes/fracdelay.h"
#include "audiohardware/mixer.h"

#define USE_FRAC_DELAY 0
#define DT_FEEDBACK 1
namespace rage
{

class audAllpassFilter
{
public:
	audAllpassFilter()
	{
		m_Diffusion = 0.375f;
		m_Decay = 0.844f;
		m_Gain = 1.f;
#if USE_FRAC_DELAY
		m_Delay = new audFractionalDelayLine(kMixerNativeSampleRate / 4);
#else
		m_BufIdx = 0;
#endif
	}

#if USE_FRAC_DELAY
	void SetLength(const float len)
	{
		m_DelayLength = len;
	}
#else
	void SetBuffer(float *buf, s32 size)
	{
		m_Buffer = buf;
		m_BufSize = size;
		sysMemSet(buf, 0, size * sizeof(float));
	}
#endif

#if DT_FEEDBACK
	
	float Process(float input)
	{
		const float delayOut = m_Buffer[m_BufIdx];

		const float x = input + (delayOut * m_Diffusion);
		m_Buffer[m_BufIdx] = x;

		if(++m_BufIdx>=m_BufSize) m_BufIdx = 0;

		return (delayOut * m_Decay) - (x * m_Diffusion);
	}
#else

	float Process(float input)
	{
#if USE_FRAC_DELAY


		/*float delayOut = m_Delay->Read(m_DelayLength);
		undenormalise(delayOut);
		float output = -m_Feedback * input + delayOut;
		m_Delay->Feed(input + delayOut * m_Feedback);*/

		float delayOut = m_Delay->Process(input, m_Feedback, m_DelayLength);
		float output = -m_Feedback * input + delayOut;

#else
		float bufout = m_Buffer[m_BufIdx];
		undenormalise(bufout);
#if 1
		float output = -m_Feedback * input + bufout;
		m_Buffer[m_BufIdx] = input + (bufout*m_Feedback);
#else
		float output = -input + bufout;
		m_Buffer[m_BufIdx] = input + (bufout*m_Feedback);
#endif
		if(++m_BufIdx>=m_BufSize) m_BufIdx = 0;
#endif
		return output * m_Gain;
	}
#endif
	void SetDiffusion(float val)
	{
		m_Diffusion = val;
	}
	
	void SetGain(float val)
	{
		m_Gain = val;
	}

	void SetDecay(float val)
	{
		m_Decay = val;
	}

private:
	
	static void undenormalise(float &sample)
	{
		if(((*(unsigned int*)&sample)&0x7f800000)==0) 
		{
			sample=0.0f;
		}
	}

	float	m_Diffusion;
	float	m_Gain;
	float	m_Decay;
#if !USE_FRAC_DELAY
	float	*m_Buffer;
	s32 m_BufSize;
	s32 m_BufIdx;
#else
	float m_DelayLength;
	audFractionalDelayLine *m_Delay;
#endif
};

} // namespace rage

#endif// AUD_ALLPASSFILTER_H
