#if __SPU
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "psn_dsp.h"

/**********************************************************************************
DSPInit
Initialisation routine
This routine is called by MultiStream when the DSP effect is first called.

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
paramList		Address of static parameter data (248 bytes)
	0-127		= Read Only (example: users input parameters)
	128-247		= Read / Write (example: DSP effects internal parameters)
workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area
dspInfo			Contains information as to the slot number to be processed, input / output bitmasks

Notes:
Each DSP effect has 248 bytes of RAM available.
If more RAM is required, this area can be used to store a pointer to the actual data.
The data can then be transferred from main RAM as required.
Splitting the paramList into a read and a read/write section allows us to make sure that if
the user is modifying parameters via PPU commands (example: distortion level), there is no conflicts
between the SPU updating parameter memory at the same time.

**********************************************************************************/
void DSPInit(char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{
}

/**********************************************************************************
DSPApply
Apply DSP effect routine
This routine is called by MultiStream to apply a DSP effect to an input signal

The function needs to be declared in the DSPPlugin structure.
See routine: DSPPlugInSetup for more information

Requires:
input			Address of input data (Time Domain float32 data OR Frequency Domain FFT data)
				For PCM, 1024 samples are passed in.
				For FFT, 512 frequency bands are passed in.

output			Address to write output data to

paramList		Address of static parameter data (248 bytes)
				0-127 = Read Only
				128-247 = Read / Write

workArea		Work area available for DSP effects (40 K)
				Any DMA from main RAM must fit into this area

dspInfo			Contains information as to the slot number to be processed, input / output bitmasks..

Returns:
0				DSP will complete when stream finishes
!0				DSP will stay active after stream finishes (for delays / reverbs)

Notes:
For time domain input data, the user must process all 1024 samples.
But note that any parameters used for modifying data must be preserved after 512 samples.
This is due to windowing within MultiStream.

**********************************************************************************/
int DSPApply(void* input, void* output, char * paramList, void * workArea, MS_DSP_INFO * dspInfo)
{
	int cnt;

	// Create output data if DSP effect is set as Time Domain
	for (cnt = 0; cnt < 256; cnt++)
	{
		((float *)output)[cnt] = ((float *)input)[cnt];
	}

	// If the stream ends and (and if this DSP effect is attached to a stream), end the DSP effect too..
	return 0;
}

/**********************************************************************************
DSPPlugInSetup
Setup as the entry point function within the DSP makefile
Initialises the DSP effect functions.
Called by MultiStream.

Requires:
plugInStruct		Address of a DSPPlugin structure

Notes:
This function sets the address of both the Init and the Apply functions.
It also specifies if the input / output data is required as time domain or FFT data
**********************************************************************************/
void DSPPlugInSetup(DSPPlugin* plugInStruct)
{
	plugInStruct->DSPInit = (ms_fnct)DSPInit;			// Init function (or NULL if no init function required)
	plugInStruct->DSPApply = (ms_fnct)DSPApply;			// DSP "Apply" function (processes DSP effect)
	plugInStruct->Domain = MS_DSP_TIMEBASE;				// DSP Effect is Frequency based (complex numbers)
}


#endif	// __SPU
