//
// audioeffects/interleaver_xenon.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON

#ifndef __INTERLEAVER_XENON_H
#define __INTERLEAVER_XENON_H

#include "rage_xapo.h"

namespace rage {

	struct tInterleaverEffectSettings
	{
		u32 dummy0;
		u32 dummy1;
	};

	//
	// PURPOSE
	//  A simple interleaver
	//
	class __declspec(uuid("{6CA14DC1-2AE8-475e-A836-5A25D08CD0CB}"))
audInterleaverEffect : public audXAPOBase<audInterleaverEffect,tInterleaverEffectSettings>
	{
	public:

		virtual bool Init(const Effect *){return true;}

	private:

		void DoProcess(const tInterleaverEffectSettings& params, f32* __restrict pData, u32 cFrames, u32 cChannels);
	};

} // namespace rage

#endif //__INTERLEAVER_XENON_H

#endif // __XENON
