// 
// audioeffecttypes/reverbeffect_prog.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "audiosynth/synthdefs.h"

#if __SYNTH_EDITOR

#if __SPU
#include "revmodelprog.cpp"
#endif

#include "reverbeffect_prog.h"
#include "audiohardware/channel.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"

#include "system/cache.h"
#include "system/dma.h"
#include "system/memops.h"

namespace rage 
{
	using namespace Vec;
	audReverbEffectProgenitor::audReverbEffectProgenitor() : audDspEffect(AUD_DSPEFFECT_REVERB_PROG)
	{

	}

	audReverbEffectProgenitor::~audReverbEffectProgenitor()
	{

	}


	bool audReverbEffectProgenitor::Init(const u32 numChannels, const u32 channelMask)
	{

		m_NumInputChannels = (u8)numChannels;
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i = 0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		if(!m_Model.Init())
			return false;

		return true;
	}

	ALIGNAS(16) float g_ReverbInterleavedBuffer[kMixBufNumSamples * 4] ;
	AUD_DEFINE_DSP_PROCESS_FUNCTION(audReverbEffectProgenitor)
	{
		if(m_Bypass)
		{
			return;
		}

		u32 interleavedIndex = 0;
		for(s32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex++)
		{
			u32 inputChannelMask = 1;
			for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
			{
				if ((buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId) && m_ChannelMask & inputChannelMask)
				{				
					g_ReverbInterleavedBuffer[interleavedIndex++] = buffer.ChannelBuffers[inputChannelIndex][sampleIndex];
				}// channelmask

				inputChannelMask <<= 1;
			} //inputChannelIndex
		}

		Vector_4V *inOutPtr = (Vector_4V*)&g_ReverbInterleavedBuffer;
		for(s32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex++)
		{
			Vector_4V input = *inOutPtr;
			*inOutPtr++ = m_Model.Process(input, input);			
		}

		interleavedIndex = 0;
		for(s32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex++)
		{
			u32 inputChannelMask = 1;
			for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
			{
				if ((buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId) && m_ChannelMask & inputChannelMask)
				{				
					buffer.ChannelBuffers[inputChannelIndex][sampleIndex] = g_ReverbInterleavedBuffer[interleavedIndex++];
				}// channelmask

				inputChannelMask <<= 1;
			} //inputChannelIndex
		}

	}


	void audReverbEffectProgenitor::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case audReverbEffectProgenitor::Bypass:
			m_Bypass = (value != 0);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audReverbEffectProgenitor u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}
	}

	void audReverbEffectProgenitor::SetParam(const u32 paramId, const f32 value)
	{
		switch(paramId)
		{
		case audReverbEffectProgenitor::Diffusion1:
			m_Model.Diffusion1 = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::Diffusion2:
			m_Model.Diffusion2 = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::DecayDiffusion:
			m_Model.DecayDiffusion = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::Decay1:
			m_Model.Decay1 = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::Decay2:
			m_Model.Decay2 = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::Decay3:
			m_Model.Decay3 = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::HF_Bandwidth:
			m_Model.HF_Bandwidth = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::Definition:
			m_Model.Definition = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::ChorusDry:
			m_Model.ChorusDry = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::ChorusWet:
			m_Model.ChorusWet = V4LoadScalar32IntoSplatted(value);
			break;
		case audReverbEffectProgenitor::Damping:
			m_Model.Damping = V4LoadScalar32IntoSplatted(value);
			break;

		default:
			audAssertf(false, "paramId %u is not a valid audReverbEffectProgenitor f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}
	}

	void audReverbEffectProgenitor::Shutdown(void)
	{
		// TODO: work out what to do here
	}

} // namespace rage

#endif // __SYNTH_EDITOR
