// 
// audioeffecttypes/effectmonitor.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOEFFECTTYPES_EFFECTMONITOR_H 
#define AUDIOEFFECTTYPES_EFFECTMONITOR_H 

namespace rage {


struct tEffectMonitorSample
{
	f32 PeakLevel;
	f32 FloorLevel;
	f32 RmsLevel;
};

// PURPOSE
//	Provides a platform independent interface to add monitoring to DSP effects
class audEffectMonitor
{
public: 
	audEffectMonitor();
	~audEffectMonitor();

	void InitFrame(u8 **workArea, u32 &workAreaBytes);
	void InitFrame();
	void ProcessStage(const u32 stage, const u32 channel, const f32 *samples, const u32 numSamples);
	void FinalizeFrame();

	void Print();

#if !__SPU
	bool Init(const u32 numChannels, const u32 numStages);
	void Shutdown();

	void ResetStage(const u32 stage);
	void PostProcess();

	void Draw(const f32 x, const f32 y, const f32 width, const f32 height);

	void SetActive(const bool active)
	{
		m_IsActive = active;
	}
#endif // !__SPU

private:

	void DrawMark(const f32 linVal, const char *label, const f32 x, const f32 y, const f32 height);

	u32 m_NumChannels;
	u32 m_NumStages;

	tEffectMonitorSample *m_SamplesEA;
	tEffectMonitorSample *m_SamplesLS;
	f32 *m_MaxLevels;

	u32 m_SampleBufSize;

	bool m_IsActive;
};

} // namespace rage

#endif // AUDIOEFFECTTYPES_EFFECTMONITOR_H 
