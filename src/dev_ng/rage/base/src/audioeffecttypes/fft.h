//
// audioeffects/fft.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_FFT_H
#define AUD_FFT_H

namespace rage {

class audFft
{
public:
	static void Rdft(int n, int isgn, float *a, int *ip, float *w);

private:
	static void MakeWt(int nw, int *ip, float *w);
	static void MakeCt(int nc, int *ip, float *c);
	static void BitRv2(int n, int *ip, float *a);
	static void CftfSub(int n, float *a, float *w);
	static void CftbSub(int n, float *a, float *w);
	static void Cft1st(int n, float *a, float *w);
	static void CftMdl(int n, int l, float *a, float *w);
	static void RftfSub(int n, float *a, int nc, float *c);
	static void RftbSub(int n, float *a, int nc, float *c);
};

} // namespace rage

#endif // AUD_FFT_H
