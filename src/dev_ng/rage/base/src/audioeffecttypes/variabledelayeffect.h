// 
// audioeffecttypes/variabledelayeffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __VARIABLEDELAYEFFECT_H
#define __VARIABLEDELAYEFFECT_H

#include "filterdefs.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"
#include "audiosynth/biquadfilter.h"

namespace rage 
{
#define S16_DELAY_BUFFER 1
#if S16_DELAY_BUFFER
	typedef s16 delayBufferSample;
#else
	typedef f32 delayBufferSample;
#endif


#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif
	//
	// PURPOSE
	//  A variable delay effect.
	//
	class audVariableDelayEffect : public audDspEffect
	{
	public:
		audVariableDelayEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
	
		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		enum audDelayEffectParams
		{
			DelayInSamples = audEffectParamIds::VariableDelayEffect,
			BypassChannel = DelayInSamples + g_MaxOutputChannels,
			ResetDelay = BypassChannel + g_MaxOutputChannels,
			InvertPhase = ResetDelay + g_MaxOutputChannels,
			FilterFrequency = InvertPhase + g_MaxOutputChannels,
			FilterResonance = FilterFrequency + g_MaxOutputChannels,
			FilterBandwidth = FilterResonance + g_MaxOutputChannels,
			FilterEnabled = FilterBandwidth + g_MaxOutputChannels,
			FilterMode = FilterEnabled + g_MaxOutputChannels,
			StepRateSmoothing = FilterMode + g_MaxOutputChannels,
			TimeStep,
			Bypass,
		};

	private:
		void CopyBufferToDelayLine(const f32* channelBuffer, const delayBufferSample* delayBuffer);
		f32 GetCurrentDelaySamples(const u32 channel) const;

		struct DelaySettings
		{
			bool bypass;
			bool invertPhase;
			double readOffset;
			s32 delaySamples;
			f32 prevDelaySamples;
			f32 stepRateLastFrame;
		};

		struct FilterSettings
		{
			bool enabled;
			synthBiquadFilter::synthBiquadFilterModes mode;
			f32 frequency;
			f32 bandwidth;
			f32 resonance;
			f32 m_a_0;
			f32 m_a_1;
			f32 m_a_2;
			f32 m_b_1;
			f32 m_b_2;
			Vec::Vector_4V m_State;
		};

		atRangeArray<DelaySettings, g_MaxOutputChannels> m_DelaySettings;
		atRangeArray<FilterSettings, g_MaxOutputChannels> m_FilterSettings;

		s32 m_DelayBufferWriteOffset;
		f32 m_TimeStep;
		f32 m_StepRateSmoother;

		delayBufferSample *m_DelayBuffer;

		u32 m_MaxDelaySamples;
		u8 m_NumInputChannels;
		u8 m_ChannelMask;
		bool m_Bypass;
		bool m_PrintedSpareScratch;
	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //__VARIABLEDELAYEFFECT_H

