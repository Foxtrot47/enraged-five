// 
// audioeffecttypes/channelmetereffect.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK && !__WIN32PC

#include "ChannelMeterEffect.h"

#include "filterdefs.h"

#include "audiohardware/driver.h"
#include "audioengine/engine.h"
#include "audiohardware/driverutil.h"

#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/viewport.h"

#include "system/timemgr.h"

#if __XENON
#include "audiohardware/driver_xenon.h"
#include "audioeffecttypes/channelmetereffect_xenon.h"
#endif

#if __PPU
#include "audiohardware/driver_psn.h"
#include <cell/mstream.h>
extern char _binary_channelmetereffect_psn_dspfx_elf_start[];
#endif

namespace rage {
#if __XENON || __PPU
int audChannelMeterEffect::sm_PhysicalEffectId = (__XENON) ? 0 : -1;
#endif // __XENON || __PPU

audChannelMeterEffect::audChannelMeterEffect() :
audEffect()
{
	for (int i = 0; i < NUM_SPEAKER_IDS; ++i)
		m_fMaxLevels[i] = m_fPeakLevels[i] = 0.0f;
	m_bDrawEnabled = false;
#if __WIN32PC
	// TODO: sort out the allocation for this if it truly has to be dynamic
	//m_WaveshaperEffectPc = rage_new audWaveshaperEffectPc();
#endif

#if __PPU
	m_pParamAddr = 0;
	m_pSPUData = (tChannelMeterSPUData*)audDriver::AllocateVirtual(sizeof(tChannelMeterSPUData));
	::new (m_pSPUData) tChannelMeterSPUData();
	sysMemSet(m_pSPUData, 0, sizeof(m_pSPUData));
	/*for (int i = 0; i < NUM_SPEAKER_IDS; ++i)
	{
		m_pSPUData->m_fMaxLevels[i] = 0.01f;
	}*/

	Displayf("audChannelMeterEffect::audChannelMeterEffect()");
#endif
}

audChannelMeterEffect::~audChannelMeterEffect()
{
#if __WIN32PC
	//delete m_WaveshaperEffectPc;
#endif

#if __PPU
	m_pParamAddr = 0;
	audDriver::FreeVirtual(m_pSPUData);
#endif
}

bool audChannelMeterEffect::InitClass(void)
{
#if __XENON
	return audDriverXenon::InitEffectType(audChannelMeterEffectXenon::Create, audChannelMeterEffectXenon::QuerySize, sm_PhysicalEffectId);
#elif __PPU
	Assert(sm_PhysicalEffectId == CELL_MS_DSPOFF);
	CellMSDSP dspInfo;
	if (!audDriverPsn::InitDspEffect(_binary_channelmetereffect_psn_dspfx_elf_start, dspInfo))
	{
		return false;
	}
	sm_PhysicalEffectId = dspInfo.handle;
	Displayf("[Audio] ChannelMeter init: %s, mem=0x%x, avail=0x%x", _binary_channelmetereffect_psn_dspfx_elf_start, dspInfo.memoryUsed, dspInfo.memoryAvail);
	return true;
#else
	//Add other platform-specific driver init calls here.
	return true;
#endif
}

// PURPOSE
//  Creates bank widgets
void audChannelMeterEffect::AddWidgets(bkBank& bank)
{
	bank.PushGroup("ChannelMeter");//AUDIOENGINE.GetEffectManager().GetFactory().GetEffectNameFromNameTableOffset(GetMetadata()->NameTableOffset));
	{
		bank.AddToggle("Draw       ", &m_bDrawEnabled);
		bank.AddButton("Clear Peaks", datCallback(MFA(audChannelMeterEffect::ClearPeaks),this));
	}bank.PopGroup();
}

// PURPOSE
//  Displays the meters on the screen
void audChannelMeterEffect::DebugDraw()
{
	if (!m_bDrawEnabled)
		return;
	
	const ChannelMeterEffect* pMetadata = (const ChannelMeterEffect*)m_Metadata;

	const f32 fFloor = pMetadata->Default.FloorLevel;
	const f32 fPeak = pMetadata->Default.PeakLevel;
	const f32 fDynRange = fPeak - fFloor;
	const f32 fInterval = pMetadata->Default.IntervalLevel;

	if (fDynRange <= 0)
		return;


	// Draw the levels here
	const grcViewport* pView = grcViewport::GetDefaultScreen();
	grcViewport::SetCurrent(pView);
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);
	grcState::SetCullMode(grccmNone);
	grcState::SetState(grcsDepthFunc,grcdfAlways);
	grcState::SetState(grcsDepthWrite,false);

	const f32 width = pView->GetWidth() / 4.0f;
	const f32 height = pView->GetHeight() / 4.0f;
	const f32 intervalHeight = height * (fInterval/fDynRange);
	const f32 left = pView->GetWidth()/2.0f - width/2.0f;
	const f32 right = pView->GetWidth()/2.0f + width/2.0f;
	const f32 bottom = pView->GetHeight()*0.95f;
	const f32 top = bottom - height;
	const f32 zeroLevel = top - (fPeak*height);
	const f32 channelWidth = width/NUM_SPEAKER_IDS;
	const f32 meterWidth = channelWidth*0.9f;
	const f32 meterGap = 0.5f *(channelWidth-meterWidth);

	// draw alpha'd background
	grcBegin(drawTriStrip, 4);
	grcColor(Color32(0.1f,0.1f,0.1f,0.5f));
	grcVertex2f(left-20.0f,bottom+10.0f);	
	grcVertex2f(left-20.0f,top-10.0f);
	grcVertex2f(right+20.0f,bottom+10.0f);
	grcVertex2f(right+20.0f,top-10.0f);
	grcEnd();


	static const char* szSpeakerNames[NUM_SPEAKER_IDS] = 
	{
		"L",
		"R",
		"C",
		"LFE",
		"LS",
		"RS",
		"LC",
		"RC"
	};

	char szGain[6];

	//	Render levels
	m_csToken.Lock();
	for (int i = 0; i < NUM_SPEAKER_IDS; ++i)
	{
		const f32 fDb = audDriverUtil::ComputeDbVolumeFromLinear_Precise(m_fMaxLevels[i]);
		const f32 fLevel = Clamp((fDb-fFloor) / fDynRange, 0.0f, 1.0f);

		const f32 meterLeft = left + (i*channelWidth) + meterGap;
		const f32 meterRight = meterLeft + (0.9f*channelWidth) - meterGap;
		const f32 meterHeight = height*fLevel;
		const f32 meterTop = bottom - meterHeight;

		grcBegin(drawTriStrip, 4);
		grcColor(fDb <= 0.0f ? Color32(fLevel,1.0f,0.0f,0.5f) : Color32(1.0f,0.0f,0.0f,0.5f));
		grcVertex2f(meterLeft, bottom);	
		grcVertex2f(meterLeft, meterTop);
		grcVertex2f(meterRight, bottom);	
		grcVertex2f(meterRight, meterTop);
		grcEnd();

		grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
		grcDraw2dText(meterLeft, bottom, szSpeakerNames[i]);

		//	Begin falling off
		f32 fFallDb = fDb;
		Approach(fFallDb, -100.0f, fDynRange, TIME.GetUnwarpedRealtimeSeconds());
		m_fMaxLevels[i] = fFallDb > -100.0f ? audDriverUtil::ComputeLinearVolumeFromDb(fFallDb) : 0.0f;

		if (pMetadata->Default.Flags.BitFields.DisplayPeaks)
		{
			if (m_fPeakLevels[i] > 0.0f)
			{
				const f32 fPeakDb = audDriverUtil::ComputeDbVolumeFromLinear_Precise(m_fPeakLevels[i]);
				formatf(szGain, sizeof(szGain), "%.1f", fPeakDb);
				grcColor(fPeakDb <= 0.0f ? Color32(0.0f,1.0f,0.0f,0.9f) : Color32(1.0f,0.0f,0.0f,0.9f));
				grcDraw2dText(meterLeft, top-10.0f, szGain);
			}
		}
		else
			m_fPeakLevels[i] = m_fMaxLevels[i];
	}
	m_csToken.Unlock();

	const u32 uIntervalUp = (u32)Max(fPeak / fInterval, 0.0f);
	const u32 uIntervalDn = (u32)Max(-fFloor / fInterval, 0.0f);

	//	Start from 0
	if (zeroLevel <= top)
	{
		//	Horizontal line
		grcColor(Color32(0.0f,0.0f,0.0f,1.0f));
		grcBegin(drawLines, 2);
		{
			grcVertex2f(left, zeroLevel);
			grcVertex2f(right, zeroLevel);
		}grcEnd();

		formatf(szGain, sizeof(szGain), "%.1f", 0.0f);
		grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
		grcDraw2dText(left-20.0f, zeroLevel, szGain);
	}

	//	Print gain levels above 0
	for (u32 i = 1; i < uIntervalUp; ++i)
	{
		f32 fValue = i*fInterval;
		f32 fY = zeroLevel - (i*intervalHeight);

		//	Horizontal line
		grcColor(Color32(0.0f,0.0f,0.0f,1.0f));
		grcBegin(drawLines, 2);
		{
			grcVertex2f(left, fY);
			grcVertex2f(right, fY);
		}grcEnd();

		formatf(szGain, sizeof(szGain), "%.1f", fValue);
		grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
		grcDraw2dText(left-20.0f, fY, szGain);			
	}

	//	Print gain levels below 0
	for (u32 i = 1; i < uIntervalDn; ++i)
	{
		f32 fValue = i*fInterval;
		f32 fY = zeroLevel + (i*intervalHeight);

		//	Horizontal line
		grcColor(Color32(0.0f,0.0f,0.0f,1.0f));
		grcBegin(drawLines, 2);
		{
			grcVertex2f(left, fY);
			grcVertex2f(right, fY);
		}grcEnd();

		formatf(szGain, sizeof(szGain), "%.1f", -fValue);
		grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
		grcDraw2dText(left-20.0f, fY, szGain);			
	}
}

// PURPOSE
//  Resets the displayed peak values
void audChannelMeterEffect::ClearPeaks()
{
	m_csToken.Lock();
	for (int i = 0; i < NUM_SPEAKER_IDS; ++i)
		m_fPeakLevels[i] = m_fMaxLevels[i];
	m_csToken.Unlock();
}

// PURPOSE
//	Initializes an instance of the specific effect type.
bool audChannelMeterEffect::Init(const void *pMetadata, u32 effectChainIndex)
{
	if (!audEffect::Init(pMetadata, effectChainIndex))
	{
		return false;
	}

	//	Do stuff here

#if __PPU
	m_pSPUData->m_ChannelMask = m_Metadata->ChannelMask.Value;
#endif

	return true;
}

void audChannelMeterEffect::Update()
{
	UpdateReadIndex();

	//Perform a platform-specific update of effect parameters.
#if __XENON
	IXAudioSubmixVoice *submixVoice = (IXAudioSubmixVoice *)m_PhysicalSubmix;
	if(submixVoice)
	{
		f32 fCurLevels[NUM_SPEAKER_IDS];

		XAUDIOVOICEFXINDEX effectIndex = (XAUDIOVOICEFXINDEX)m_EffectChainIndex;
		XAUDIOFXPARAM param;
		param.Data.pBuffer = fCurLevels;
		param.Data.BufferSize = sizeof(fCurLevels);
		HRESULT result = submixVoice->GetEffectParam(effectIndex, CHANNELMETEREFFECT_SETTINGS, XAUDIOFXPARAMTYPE_DATA, &param);
		Assert(SUCCEEDED(result));
		if(FAILED(result))
		{
			Warningf("Failed to set channelmeter effect parameters for effect 0x%p", this);
			return;
		}

		m_csToken.Lock();
		for (int i = 0; i < NUM_SPEAKER_IDS; ++i)
		{
			if (fCurLevels[i] > m_fMaxLevels[i])
				m_fMaxLevels[i] = fCurLevels[i];
			if (fCurLevels[i] > m_fPeakLevels[i])
				m_fPeakLevels[i] = fCurLevels[i];
		}
		m_csToken.Unlock();
	}
#elif __WIN32PC
	//	PC implementation here
#elif __PPU
	// copy settings data and address of our workspace to SPU
	tChannelMeterSPUParamData *pParamData	= (tChannelMeterSPUParamData*)m_pParamAddr;
	if (pParamData)
	{
		// setup paramData, which Multistream DMAs for us
		pParamData->EA								= (void*)m_pSPUData;

		m_csToken.Lock();
		for (int i = 0; i < NUM_SPEAKER_IDS; ++i)
		{
			if (!BIT_ISSET(i, m_pSPUData->m_ClearMaxLevelsMask))
			{
				if (m_pSPUData->m_fMaxLevels[i] > m_fMaxLevels[i])
					m_fMaxLevels[i] = m_pSPUData->m_fMaxLevels[i];
				if (m_pSPUData->m_fMaxLevels[i] > m_fPeakLevels[i])
					m_fPeakLevels[i] = m_pSPUData->m_fMaxLevels[i];
				m_pSPUData->m_ClearMaxLevelsMask = BIT_SET(i, m_pSPUData->m_ClearMaxLevelsMask);
			}
		}
		m_csToken.Unlock();
	}
#endif

	audEffect::Update();
}



void* audChannelMeterEffect::GetEffectSettings()
{
	return NULL;
}

void audChannelMeterEffect::CommitSettings()
{
	audEffect::CommitSettings();

	// If we have a child, call CommitSettings on that effect too
	if (m_NextEffect)
	{
		m_NextEffect->CommitSettings();
	}
}


}//	namespace rage


#endif //__BANK
