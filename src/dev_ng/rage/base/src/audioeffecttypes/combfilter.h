
#ifndef AUD_COMBFILTER_H
#define AUD_COMBFILTER_H

namespace rage
{

class audCombFilter
{
public:
	audCombFilter()
	{
		m_FilterStore = 0.0f;
		m_Feedback = 0.0f;
		m_Damping = 1.f;
		m_Gain = 1.f;
		m_BufIdx = 0;
	}
	
	void SetBuffer(float *buf, s32 size)
	{
		m_Buffer = buf;
		m_BufSize = size;
		sysMemSet(buf, 0, size * sizeof(float));
		m_FilterStore = 0.f;
	}

	float Process(float input)
	{
		float output = m_Buffer[m_BufIdx];
		undenormalise(output);

		const float damp2 = (1.f - m_Damping);
		m_FilterStore = (output*damp2) + (m_FilterStore*m_Damping);
		undenormalise(m_FilterStore);

		m_Buffer[m_BufIdx] = input + (m_FilterStore*m_Feedback);

		if(++m_BufIdx>=m_BufSize) m_BufIdx = 0;

		return output * m_Gain;
	}
	
	void SetDamping(float val)
	{
		m_Damping = val;
	}
	
	void SetFeedback(float val)
	{
		m_Feedback = val;
	}
	
	void SetGain(float val)
	{
		m_Gain = val;
	}
private:

	static void undenormalise(float &sample)
	{
		if(((*(unsigned int*)&sample)&0x7f800000)==0) 
		{
			sample=0.0f;
		}
	}

	float	m_Feedback;
	float	m_FilterStore;
	float	m_Damping;
	float	*m_Buffer;
	s32 	m_BufSize;
	s32		m_BufIdx;
	float m_Gain;
};

} // namespace rage

#endif //AUD_COMBFILTER
