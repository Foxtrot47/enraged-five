// 
// audioeffecttypes/convolutioneffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_CONVOLUTION_EFFECT_H
#define AUD_CONVOLUTION_EFFECT_H

#include "effect.h"

namespace rage {

// PURPOSE
//
class audConvolutionEffect : public audEffect
{
public:
	audConvolutionEffect();
	virtual ~audConvolutionEffect();

	// PURPOSE
	//	Initialises an instance of the convolution effect. Sets up all effect settings to sensible defaults.
	virtual bool Init(const void *pMetadata, u32 effectChainIndex);

	// PURPOSE
	//  Returns a ptr to the current game thread effect settings structure, so that the effect may be controlled by
	//  game code.
	virtual void* GetEffectSettings();

	// PURPOSE
	// Commits the current requested setting, so that they may be used by the audio thread.
	// In gamecode, this MUST be called one per frame after updating any effect setting parameters.
	virtual void CommitSettings();

	// PURPOSE
	//  Peforms platform-specific initialization for this effect type.
	// RETURNS
	//  True if initialisation was successful.
	static bool InitClass(void);

#if __XENON || __PPU
	//
	// PURPOSE
	//	Accessor function for the specific effect ID.
	// RETURNS
	//	The effect ID.
	virtual int GetPhysicalEffectId(void) const
	{
		return sm_PhysicalEffectId;
	}
#endif // __XENON

#if __WIN32PC
	virtual audDspEffect *GetDspEffect()
	{
		return NULL;
	}
#endif

private:

#if __XENON || __PPU
	static int sm_PhysicalEffectId;
#endif // __XENON || __PPU

};

} // namespace rage

#endif // AUD_CONVOLUTION_EFFECT_H
