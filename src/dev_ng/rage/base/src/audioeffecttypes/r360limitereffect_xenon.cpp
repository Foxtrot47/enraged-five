// 
// audioeffecttypes/r360limitereffect_xenon.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if __XENON && 0

#define AUDIO_BENCHMARK_EFFECTS __BANK

#include "r360limitereffect_xenon.h"

#include "r360limitereffect.h"
#include "effect_xenon.h"

#include "audiohardware/driverutil.h"

#if AUDIO_BENCHMARK_EFFECTS
#include "profile/profiler.h"
#include "system/performancetimer.h"
#endif // AUDIO_BENCHMARK_EFFECTS

namespace rage {

#if AUDIO_BENCHMARK_EFFECTS
PF_PAGE(R360LimitingPage, "RAGE Audio R360Limiter Effect");
PF_GROUP(R360LimitingEffect);
PF_LINK(R360LimitingPage, R360LimitingEffect);
PF_VALUE_FLOAT(R360LimiterThreadPerc, R360LimitingEffect);

u32 g_R360LimiterFrameIndex = 0;
sysPerformanceTimer g_R360LimiterTimer("R360Limiter");
#endif // AUDIO_BENCHMARK_EFFECTS

BEGIN_ALIGNED(128) f32 g_R360OutputBuffer[g_MaxOutputChannels][XAUDIOFRAMESIZE_NATIVE];


audR360LimiterEffectXenon::audR360LimiterEffectXenon(LPVOID context)
{
	m_Context  = context;
	m_RefCount = 1;
}

HRESULT audR360LimiterEffectXenon::Init(const XAUDIOFXINIT *init, IXAudioBatchAllocator * /*allocator*/)
{
	m_NumInputChannels = ((tEffectInit *)init)->numChannels;
	R360LimiterEffect *metadata = (R360LimiterEffect *)(((tEffectInit *)init)->metadata);
	m_ChannelMask = metadata->ChannelMask.Value;

	m_NumChannelsToProcess = 0;
	for(u32 i=0; i<m_NumInputChannels; i++)
	{
		if(m_ChannelMask & (1 << i))
		{
			m_NumChannelsToProcess++;
		}
	}

	m_uCurrentSample = 0;

	// Linear versions of gain and threshold
	m_fInputClipLin				= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.InputClipDb);
	m_fOutputCeilingLin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.OutCeilingDb);
	m_fThresholdLin				= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.ThresholdDb);
	m_fCurrentGain				= 1.0f;
	m_fTargetGain				= 1.0f;
	m_fGainRate					= 0.0f;
	m_fRmsThresholdMin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMin) * m_fThresholdLin;
	m_fRmsThresholdMax			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMax) * m_fThresholdLin;
	m_fOneOverRmsThresholdDelta	= 1.0f / (m_fRmsThresholdMax-m_fRmsThresholdMin);

	// Release time calculations
	m_fReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / XAUDIOSAMPLERATE_NATIVE) / m_Settings.Release);	//	Release is in ms

	memset(&m_DelayBuffer[0][0],0,sizeof(m_DelayBuffer));

	return S_OK;
}

HRESULT audR360LimiterEffectXenon::Process(IXAudioFrameBuffer* inputBuffer, IXAudioFrameBuffer *ASSERT_ONLY(outputBuffer))
{
	assert(inputBuffer);
	assert(!outputBuffer);

	XAUDIOFRAMEBUFDATA frameBufferData;
	inputBuffer->GetProcessingData(&frameBufferData);  

	//Verify the format is "internal". It should always be internal (float).
	assert(XAUDIOSAMPLETYPE_NATIVE == frameBufferData.Format.SampleType);
	assert(frameBufferData.Format.ChannelCount <= m_NumInputChannels);
	assert(frameBufferData.Format.ChannelCount >= m_NumChannelsToProcess);

#if AUDIO_BENCHMARK_EFFECTS
	g_R360LimiterTimer.Start();
#endif // AUDIO_BENCHMARK_EFFECTS

	if (!m_Settings.Bypass)
	{
		const u32 numChannels = frameBufferData.Format.ChannelCount;
		u8 channelIndices[g_MaxOutputChannels];
		for (u32 inputChannelIndex = 0; inputChannelIndex < numChannels; ++inputChannelIndex)
		{
			if(m_ChannelMask & (1 << inputChannelIndex))
				channelIndices[inputChannelIndex] = (u8)inputChannelIndex;
		}

		for(u32 curSample = 0; curSample < XAUDIOFRAMESIZE_NATIVE; curSample++)
		{
			if (!m_RunningTargets.IsEmpty() && m_uCurrentSample == m_RunningTargets.Top().m_uSampleTarget)
			{
				assert(m_uCurrentSample <= m_RunningTargets.Top().m_uSampleTarget);
				assert(m_RunningTargets.Top().m_fGainTarget == m_fTargetGain);
				//m_fGainRate = 0.0f;
				m_fCurrentGain = m_fTargetGain;
				m_RunningTargets.Drop();
			}

			//	Compute the gain for the next delayed sample
			else if (m_fGainRate > 0.0f)
			{
				m_fCurrentGain += m_fGainRate;
				if (m_fCurrentGain >= m_fTargetGain)
					m_fCurrentGain = m_fTargetGain;
			}
			else if (m_fGainRate < 0.0f)
			{
				m_fCurrentGain += m_fGainRate;
				if (m_fCurrentGain <= m_fTargetGain)
					m_fCurrentGain = m_fTargetGain;
			}


			f32 fHiPeak = 0.0f;
			for(u32 iIndex=0; iIndex < numChannels; iIndex++)
			{
				const u32 inputChannelIndex = channelIndices[iIndex];

				f32 *inputData = &(frameBufferData.pSampleBuffer[inputChannelIndex * XAUDIOFRAMESIZE_NATIVE]);
				const f32 fCurrentSample = inputData[curSample];
				
				//	Clip the input sample to our specified range
				inputData[curSample] = Clamp(fCurrentSample, -m_fInputClipLin, m_fInputClipLin);
				
				const f32 fDelayedSample = (curSample < g_R360DelayLength?m_DelayBuffer[inputChannelIndex][curSample]:inputData[curSample-g_R360DelayLength]);

				g_R360OutputBuffer[inputChannelIndex][curSample] = (m_fCurrentGain * fDelayedSample * m_fOutputCeilingLin) / m_fThresholdLin;
				assert(g_R360OutputBuffer[inputChannelIndex][curSample] <= 1.0f);

				//	Check the highest peak for each channel 64 samples in the future
				fHiPeak = Max(fHiPeak, Abs(fCurrentSample));
			}

			const f32 fRMS = m_RmsCalculator.AddValue(fHiPeak);
			f32 fHiReleaseRate;
			if (m_Settings.AutoReleaseMode)
			{
				const f32 fAutoReleaseScalar = Clamp((fRMS-m_fRmsThresholdMin) * m_fOneOverRmsThresholdDelta, 0.0f, 1.0f);
				m_Settings.Release = Lerp(fAutoReleaseScalar, m_Settings.AutoReleaseMin, m_Settings.AutoReleaseMax);
				m_fReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / XAUDIOSAMPLERATE_NATIVE) / m_Settings.Release);	//	Release is in ms
				fHiReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / XAUDIOSAMPLERATE_NATIVE) / m_Settings.AutoReleaseMin);	//	Release is in ms
			}
			else
				fHiReleaseRate = m_fReleaseRate;

			//	Check every sample in the queue ahead of this one, and find the steepest descending gain rate
			if (fHiPeak > m_fThresholdLin)
			{
				const audR360LimiterEffect::tTargetGain newTarget = { m_fThresholdLin/fHiPeak, m_uCurrentSample+g_R360DelayLength };
				m_RunningTargets.Push(newTarget);
			}

			int iSteepestSlopeIndex = 0;
			m_fGainRate = fHiReleaseRate;
			for (int i = 0; i < m_RunningTargets.GetCount(); ++i)
			{
				const audR360LimiterEffect::tTargetGain& rThisTarget = m_RunningTargets[i];
				
				const f32 fThisGainRate = rThisTarget.CalculateGainSlope(m_fCurrentGain, m_uCurrentSample);
				if (fThisGainRate <= m_fGainRate)
				{
					m_fGainRate = fThisGainRate;
					m_fTargetGain = rThisTarget.m_fGainTarget;
					iSteepestSlopeIndex = i;
				}
			}

			if (m_fGainRate == fHiReleaseRate)
			{
				m_RunningTargets.Reset();
				m_fGainRate = m_fReleaseRate;
				m_fTargetGain = 1.0f;
			}
			else
			{
				for (int i = 0; i < iSteepestSlopeIndex; ++i)
					m_RunningTargets.Drop();
			}

			++m_uCurrentSample;
		}

		for(u32 iIndex=0; iIndex < numChannels; iIndex++)
		{
			const u32 inputChannelIndex = channelIndices[iIndex];
			f32 *inputData = &(frameBufferData.pSampleBuffer[inputChannelIndex * XAUDIOFRAMESIZE_NATIVE]);

			// populate delay line with last N samples from this buffer
			XMemCpy(&m_DelayBuffer[inputChannelIndex], &inputData[XAUDIOFRAMESIZE_NATIVE-g_R360DelayLength], g_R360DelayLength*sizeof(f32));
			// overwrite the output
			XMemCpy(inputData, &g_R360OutputBuffer[inputChannelIndex], XAUDIOFRAMESIZE_NATIVE*sizeof(f32));
		}
	}

#if AUDIO_BENCHMARK_EFFECTS
	g_R360LimiterTimer.Stop();

	if(++g_R360LimiterFrameIndex == 1)
	{
		PF_SET(R360LimiterThreadPerc, g_R360LimiterTimer.GetTimeMS() * 18.75f);
		g_R360LimiterTimer.Reset();
		g_R360LimiterFrameIndex = 0;
	}
#endif // AUDIO_BENCHMARK_EFFECTS

	return S_OK;
}

HRESULT audR360LimiterEffectXenon::GetInfo(XAUDIOFXINFO *info)
{
	assert(info);

	info->DataFlow = XAUDIODATAFLOW_INPLACE;
	info->TrailFrames = 0;

	return S_OK;
}

HRESULT audR360LimiterEffectXenon::GetParam(XAUDIOFXPARAMID paramId, XAUDIOFXPARAMTYPE ASSERT_ONLY(paramType), XAUDIOFXPARAM *param)
{
	assert(param);
	HRESULT hr = S_OK;

	XAudioLock(XAUDIOLOCKTYPE_LOCK | XAUDIOLOCKTYPE_SPINLOCK);

	if(paramId == R360LIMITERFX_SETTINGS)
	{
		assert(paramType == XAUDIOFXPARAMTYPE_DATA);
		assert(param->Data.BufferSize == sizeof(tR360LimiterEffectSettings));
		XMemCpy(param->Data.pBuffer, &m_Settings, sizeof(tR360LimiterEffectSettings));
	}
	else
	{
		hr = E_FAIL;
	}

	XAudioLock(XAUDIOLOCKTYPE_UNLOCK | XAUDIOLOCKTYPE_SPINLOCK);

	return hr;
}

HRESULT audR360LimiterEffectXenon::SetParam(XAUDIOFXPARAMID paramId, XAUDIOFXPARAMTYPE ASSERT_ONLY(paramType),
	const XAUDIOFXPARAM *param)
{
	assert(param);
	HRESULT hr = S_OK;

	AssertVerify(SUCCEEDED(XAudioLock(XAUDIOLOCKTYPE_LOCK | XAUDIOLOCKTYPE_SPINLOCK)));

	if(paramId == R360LIMITERFX_SETTINGS)
	{
		assert(paramType == XAUDIOFXPARAMTYPE_DATA);
		assert(param->Data.BufferSize == sizeof(tR360LimiterEffectSettings));
		const tR360LimiterEffectSettings* pSettings = (const tR360LimiterEffectSettings*)param->Data.pBuffer;
		//XMemCpy(&m_Settings, param->Data.pBuffer, sizeof(tR360LimiterEffectSettings));

		//	Apply min and max limits to limiter parameters.
		m_Settings.InputClipDb				= Clamp(pSettings->InputClipDb, R360_INPUTCLIP_MIN, R360_INPUTCLIP_MAX);
		m_Settings.ThresholdDb				= Clamp(pSettings->ThresholdDb, R360_THRESH_MIN, R360_THRESH_MAX);
		m_Settings.OutCeilingDb				= Clamp(pSettings->OutCeilingDb, R360_OUTCEILING_MIN, R360_OUTCEILING_MAX);
		m_Settings.AutoReleaseMin			= Clamp(pSettings->AutoReleaseMin, R360_RELEASE_MIN, R360_RELEASE_MAX);
		m_Settings.AutoReleaseMax			= Clamp(pSettings->AutoReleaseMax, m_Settings.AutoReleaseMin, R360_RELEASE_MAX);
		m_Settings.AutoReleaseRmsThresMin	= Clamp(pSettings->AutoReleaseRmsThresMin, R360_AUTORELEASE_RMS_THRES_MIN_MIN, R360_AUTORELEASE_RMS_THRES_MIN_MAX);
		m_Settings.AutoReleaseRmsThresMax	= Clamp(pSettings->AutoReleaseRmsThresMax, m_Settings.AutoReleaseRmsThresMin, R360_AUTORELEASE_RMS_THRES_MAX_MAX);
		m_Settings.AutoReleaseMode			= pSettings->AutoReleaseMode;
		m_Settings.Bypass					= pSettings->Bypass;
		
		//Do some dB-->linear conversions
		m_fInputClipLin				= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.InputClipDb);
		m_fThresholdLin				= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.ThresholdDb);
		m_fOutputCeilingLin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.OutCeilingDb);
		m_fRmsThresholdMin			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMin) * m_fThresholdLin;
		m_fRmsThresholdMax			= audDriverUtil::ComputeLinearVolumeFromDb(m_Settings.AutoReleaseRmsThresMax) * m_fThresholdLin;
		m_fOneOverRmsThresholdDelta	= 1.0f / (m_fRmsThresholdMax-m_fRmsThresholdMin);

		if (!m_Settings.AutoReleaseMode)
		{
			m_Settings.Release = Clamp(pSettings->Release,	R360_RELEASE_MIN, R360_RELEASE_MAX);
			m_fReleaseRate = (1.0f-m_fThresholdLin) * ((1000.0f / XAUDIOSAMPLERATE_NATIVE) / m_Settings.Release);	//	Release is in ms
		}
	}
	else
	{
		hr = E_FAIL;
	}

	AssertVerify(SUCCEEDED(XAudioLock(XAUDIOLOCKTYPE_UNLOCK | XAUDIOLOCKTYPE_SPINLOCK)));

	return hr;
}

HRESULT audR360LimiterEffectXenon::GetContext(LPVOID *context)
{
	assert(context);
	*context = m_Context;
	return S_OK;
}

HRESULT QuerySizeR360LimiterEffectXenon(LPCXAUDIOFXINIT ASSERT_ONLY(init), LPDWORD effectSize)
{
	assert(init);
	assert(effectSize);

	*effectSize = sizeof(audR360LimiterEffectXenon);

	return S_OK;
}

HRESULT CreateR360LimiterEffectXenon(const XAUDIOFXINIT *init, IXAudioBatchAllocator *allocator, IXAudioEffect **effect)
{
	assert(init);
	assert(effect);
	assert(allocator);

	audR360LimiterEffectXenon *userEffect = NULL;

	tEffectInit *effectInit = (tEffectInit *)init;
	userEffect = XAUDIO_BATCHALLOC_NEW(audR360LimiterEffectXenon(effectInit->effectHeader.pContext), allocator);
	userEffect->Init(init, allocator);

	*effect = userEffect;

	//	Zero out the global output buffer
	for(u32 inputChannelIndex=0; inputChannelIndex < g_MaxOutputChannels; inputChannelIndex++)
	{
		for(u32 curSample = 0; curSample < XAUDIOFRAMESIZE_NATIVE; curSample++)
		{
			g_R360OutputBuffer[inputChannelIndex][curSample] = 0.0f;
		}
	}

	return S_OK;
}

ULONG audR360LimiterEffectXenon::AddRef(void)
{
	return ++m_RefCount;
}

ULONG audR360LimiterEffectXenon::Release(void)
{
	if(0 == --m_RefCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return m_RefCount;
	}
}

} // namespace rage

#endif //__XENON
