// 
// audioeffecttypes/leveldetecteffect.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOEFFECTTYPES_LEVELDETECTEFFECT_H 
#define AUDIOEFFECTTYPES_LEVELDETECTEFFECT_H 

#if !__WIN32PC

#include "effect.h"
#include "filterdefs.h"
#include "data/base.h"

#include "audiohardware/driverdefs.h"

#include "math/amath.h"
#include "system/xtl.h"



namespace rage {

class bkBank;
class audLevelDetectEffectPc;
struct tLevelDetectSPUData;

// PURPOSE
//  The audLevelDetectEffect passes the audio through it unaltered, but is able to display peak levels for each
//	input channel if you call DebugDraw() on it from your debug rendering thread
class audLevelDetectEffect : public datBase, public audEffect
{
public:
	audLevelDetectEffect();
	virtual ~audLevelDetectEffect();

	// PURPOSE
	//	Initializes an instance of the effect. Sets up all effect settings to sensible defaults.
	virtual bool Init(const void *pMetadata, u32 effectChainIndex);

	// PURPOSE
	//  Sets appropriate values on the per-platform implementation of the filter, and updates the next effect.
	virtual void Update();

	// PURPOSE
	//  Returns NULL
	virtual void* GetEffectSettings();

	// PURPOSE
	// Commits the current requested setting, so that they may be used by the audio thread.
	// In gamecode, this MUST be called one per frame after updating any effect setting parameters.
	virtual void CommitSettings();

	// PURPOSE
	//  Performs platform-specific initialization for this effect type.
	// RETURNS
	//  true if initialization was successful.
	static bool InitClass(void);

#if __PPU
	// PURPOSE
	//  Access function to set the Multistream DSP effect address
	virtual void SetParameterAddress(const char * pParamAddr);
#endif

	// PURPOSE
	//  Copies the recent peak data to a passed in array, and zeroes out the history
	// IN: A float array of size NUM_PLATFORM_SPEAKER_IDS
	void GetAndClearPeaks(f32* outPeakData);

	// PURPOSE
	//  Gets the current RMS level over 4096 samples for the speaker channel id specified
	f32 GetRmsLevel(audPlatformSpeakerIds id) const
	{
		FastAssert(id < NUM_PLATFORM_SPEAKER_IDS);
		return m_RmsLevels[id];
	}

#if __BANK
	// PURPOSE
	//  Creates bank widgets
	void AddWidgets(bkBank& bank);
	
	// PURPOSE
	//  Displays the meters on the screen
	void DebugDraw();

	// PURPOSE
	//  Resets the displayed peak values
	void ClearDrawPeaks();
#endif // __BANK

#if __XENON || __PPU
	//
	// PURPOSE
	//	Accessor function for the Xenon-specific effect ID.
	// RETURNS
	//	The effect ID.
	//
	virtual int GetPhysicalEffectId(void) const
	{
		return sm_PhysicalEffectId;
	}
#endif // __XENON || __PPU

#if __WIN32PC
	virtual audDspEffect *GetDspEffect()
	{
		return (audDspEffect*)m_WaveshaperEffectPc;
	}
#endif


private:

#if __XENON || __PPU
	static int sm_PhysicalEffectId;
#endif // __XENON || __PPU

#if __WIN32PC
	audWaveshaperEffectPc *m_WaveshaperEffectPc;
#endif

#if __PPU
	tLevelDetectSPUData* m_pSPUData;
#endif

	sysCriticalSectionToken	m_csToken;	//	Thread safe
	
	f32 m_MaxLevels[NUM_PLATFORM_SPEAKER_IDS];
	f32 m_PeakLevels[NUM_PLATFORM_SPEAKER_IDS];
	f32 m_RmsLevels[NUM_PLATFORM_SPEAKER_IDS];
	
#if __BANK
	f32 m_DrawPeakLevels[NUM_PLATFORM_SPEAKER_IDS];
	f32 m_DrawMaxLevels[NUM_PLATFORM_SPEAKER_IDS];
	bool m_bDrawEnabled;
#endif
};




} // namespace rage

#else // __WIN32PC
#include "nulleffect.h"
namespace rage
{
	class audLevelDetectEffect : public audNullEffect { };
}

#endif // __WIN32PC
#endif // AUDIOEFFECTTYPES_LEVELDETECTEFFECT_H 
