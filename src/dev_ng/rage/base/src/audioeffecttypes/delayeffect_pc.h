// 
// audioeffecttypes/delayeffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __DELAYEFFECT_H
#define __DELAYEFFECT_H

#include "filterdefs.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"

namespace rage {

//
// PURPOSE
//  A simple delay effect.
//
class audDelayEffectPc : public audDspEffect
{
    public:
		audDelayEffectPc();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		virtual void Process(f32 *buffer, u32 numSamples);

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audDelayEffectParams
		{
			FrontLeftGain = audEffectParamIds::DelayEffect,
			FrontRightGain,
			FrontCentreGain,			
			RearLeftGain,
			RearRightGain,

			FrontLeftDelayTime,
			FrontRightDelayTime,
			FrontCentreDelayTime,
			RearLeftDelayTime,
			RearRightDelayTime,

			FrontLeftFeedback,
			FrontRightFeedback,
			FrontCentreFeedback,
			RearLeftFeedback,
			RearRightFeedback,

			Bypass,
		};
	
    private:

		u32 m_DelayBufferWriteOffset;
		u32 m_DelaySamples[g_MaxOutputChannels];
		f32 m_GainLin[g_MaxOutputChannels];
		f32 m_FeedbackGainLin[g_MaxOutputChannels];
		f32 *m_DelayBuffer;
		u32 m_MaxDelaySamples;
		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;
		bool m_Bypass;
};

} // namespace rage

#endif //__DELAYEFFECT_PC_H

#endif // __WIN32PC
