// 
// audioeffecttypes/biquadfiltereffect.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


#ifndef __BIQUADFILTEREFFECT_H
#define __BIQUADFILTEREFFECT_H

#include "filterdefs.h"
#include "audiohardware/dspeffect.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////
// biquad parameter ranges
#define BIQUAD_MODE_MIN				BIQUAD_MODE_LOW_PASS_2POLE
#define BIQUAD_MODE_MAX				BIQUAD_MODE_ULTIMATE_PEAKING_EQ
#define BIQUAD_MODE_DEFAULT			BIQUAD_MODE_LOW_PASS_4POLE
#define BIQUAD_FREQ_MIN				(0.0f)
#if RSG_CPU_X64
#define BIQUAD_FREQ_MAX				(20000.0f)
#define BIQUAD_FREQ_DEFAULT			(20000.0f)
#else
#define BIQUAD_FREQ_MAX				(23900.0f)
#define BIQUAD_FREQ_DEFAULT			(23900.0f)
#endif
#define BIQUAD_RESONANCE_MIN		(-48.0f)
#define BIQUAD_RESONANCE_MAX		(24.0f)
#define BIQUAD_RESONANCE_DEFAULT	(0.0f)
#define BIQUAD_BANDWIDTH_MIN		(1.0f)
#define BIQUAD_BANDWIDTH_MAX		(10000.0f)
#define BIQUAD_BANDWIDTH_DEFAULT	(500.0f)
#define BIQUAD_GAIN_MIN				(-40.0f)
#define BIQUAD_GAIN_MAX				(24.0f)
#define BIQUAD_GAIN_DEFAULT			(0.0f)

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif

	//
	// PURPOSE
	//  A general biquad filter implementation that supports low-pass, high-pass, band-pass and band-stop modes, with either 2
	//	or 4 poles.
	//
	class audBiquadFilterEffect : public audDspEffect
	{
	public:
		audBiquadFilterEffect();

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		enum audBiquadFilterParams
		{
			CutoffFrequency = audEffectParamIds::BiquadFilterEffect,
			ResonanceLevel,
			Bandwidth,
			Gain,
			Mode,
			ProcessFormat,
			Bypass	
		};

	private:
		void ComputeCoefficients(void);
		void ProcessInterleaved_4Pole(Vec::Vector_4V_In a, Vec::Vector_4V_In b,
			Vec::Vector_4V_InOut xy11, Vec::Vector_4V_InOut xy12, Vec::Vector_4V_InOut xy21, Vec::Vector_4V_InOut xy22,
			const float *inBuffer, float *outBuffer, const u32 numSamples);
		void ProcessInterleaved_2Pole(Vec::Vector_4V_In a, Vec::Vector_4V_In b,
			Vec::Vector_4V_InOut xy11, Vec::Vector_4V_InOut xy21,
			const float *inBuffer, float *outBuffer, const u32 numSamples);

		// coefficients
		ALIGNAS(16) atRangeArray<f32, 3> m_a ;
		ALIGNAS(16) atRangeArray<f32, 2> m_b ;

		ALIGNAS(16) f32 m_xy_n1_1[g_MaxOutputChannels] ;
		ALIGNAS(16) f32 m_xy_n2_1[g_MaxOutputChannels] ;
		ALIGNAS(16) f32 m_xy_n1_2[g_MaxOutputChannels] ;
		ALIGNAS(16) f32 m_xy_n2_2[g_MaxOutputChannels] ;

		f32 m_Frequency;	//Fc for high or low pass filters, center frequency for band pass/stop filters.
		f32 m_Resonance;	//Resonance amount for high and low pass filters (passed in using dB, but stored linearly.)
		f32 m_Bandwidth;	//BW for band pass/stop filters.
		f32 m_Gain;			//Linear gain value (passed in using dB.)

		s32 m_Mode;			//Filter type

		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;

		bool m_Is4Pole;
		bool m_Bypass;
		bool m_RecomputeCoefficients;
	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif //__BIQUADFILTEREFFECT_H
