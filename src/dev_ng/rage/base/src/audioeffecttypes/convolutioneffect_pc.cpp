//
// audioeffecttypes/convolutioneffect_xenon.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC

#include "convolutioneffect_pc.h"

#include "memory.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"

namespace rage {

audConvolutionEffectPC::audConvolutionEffectPC()
{
}

bool audConvolutionEffectPC::Init(u32 UNUSED_PARAM(numChannels), Effect *UNUSED_PARAM(metadata))
{
	return true;
}

void audConvolutionEffectPC::Process(f32 *UNUSED_PARAM(buf), u32 UNUSED_PARAM(numSamples))
{
	Displayf("convolution effect");
}

void audConvolutionEffectPC::SetParams(void)
{
}

void audConvolutionEffectPC::Shutdown(void)
{
}

} // namespace rage

#endif // __WIN32PC
