//
// audioeffects/deinterleaver_xenon.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON

#ifndef __DEINTERLEAVER_XENON_H
#define __DEINTERLEAVER_XENON_H

#include "rage_xapo.h"

namespace rage {

	struct tDeinterleaverEffectSettings
	{
		u32 dummy0;
		u32 dummy1;
	};

	//
	// PURPOSE
	//  A simple de-interleaver, converts from interleaved samples to one block per channel.
	//
class __declspec(uuid("{40815BA2-30E1-4bd3-9C93-DCBEFCA11EC3}"))
audDeinterleaverEffect : public audXAPOBase<audDeinterleaverEffect,tDeinterleaverEffectSettings>
{
public:

	virtual bool Init(const Effect *){return true;}

private:

	void DoProcess(const tDeinterleaverEffectSettings& params, f32* __restrict pData, u32 cFrames, u32 cChannels);
};

} // namespace rage

#endif //__DEINTERLEAVER_XENON_H

#endif // __XENON
