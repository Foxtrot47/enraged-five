// 
// audioeffecttypes/delayeffect.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "delayeffect.h"
#include "system/memops.h"
#include "system/performancetimer.h"

#include "audiohardware/channel.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"

#include "math/amath.h"
#include "system/cache.h"
#include "vectormath/vectormath.h"

namespace rage {
	using namespace Vec;

	audDelayEffect::audDelayEffect() : audDspEffect(AUD_DSPEFFECT_DELAY)
	{
		m_DelayBufferWriteOffset = 0;
		m_Bypass = false;

		// default gain 0.5f, default feedback 0.f, dry 0.f
		const Vector_4V defaultChannelScalars = V4VConstant<U32_HALF,U32_ZERO,U32_ZERO,U32_ZERO>();

		for(u32 i=0; i<g_MaxOutputChannels; i++)
		{
			const u32 delaySamplesUnaligned	= DELAY_TIME_DEFAULT * (kMixerNativeSampleRate/1000);
			//Align the sample length of the delay to an integer multiple of the software mixer frame size.
			m_DelaySamples[i]		= kMixBufNumSamples * (u32)ceilf((f32)delaySamplesUnaligned / (f32)kMixBufNumSamples);

			m_ChannelScalars[i] = defaultChannelScalars;
		}

		//Align the length of the delay buffer to an integer multiple of the XAudio frame size.
		m_MaxDelaySamples = kMixBufNumSamples * (u32)ceilf((f32)DELAY_MAX_SAMPLES_UNALIGNED / (f32)kMixBufNumSamples);
		Assert((m_MaxDelaySamples&15) == 0);
		m_DelayBuffer = NULL;
		PS3_ONLY(m_PrintedSpareScratch = false);
	}

	bool audDelayEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		m_NumInputChannels = (u8)numChannels;
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		u32 i;
		for(i=0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		m_DelayBuffer = rage_aligned_new(16) delayBufferSample[m_NumChannelsToProcess * m_MaxDelaySamples];
		sysMemSet(m_DelayBuffer, 0, m_NumChannelsToProcess * m_MaxDelaySamples * sizeof(delayBufferSample));

		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audDelayEffect)
	{
		if(m_Bypass)
			return;

#if __SPU
		delayBufferSample *delayBufferPtr = (delayBufferSample*)scratchSpace;

		const u32 delayBufferSize = m_MaxDelaySamples * m_NumChannelsToProcess * sizeof(delayBufferSample);
		if(scratchSpaceSize < delayBufferSize)
		{
			if(!m_PrintedSpareScratch)
			{
				m_PrintedSpareScratch = true;
				audDisplayf("Delay disabled, not enough scratch space (%u available, %u required)", scratchSpaceSize, delayBufferSize);
			}
			return;
		}

		if(!m_PrintedSpareScratch)
		{
			m_PrintedSpareScratch = true;
			audDisplayf("Delay effect scratch space: %u available, %u required", scratchSpaceSize, delayBufferSize);
		}
		// TODO: For now grab the whole delay buffer; will change this to grab only the required data for this frame
		sysDmaLargeGetAndWait(delayBufferPtr, (uint64_t)m_DelayBuffer, delayBufferSize, 8);

#else
		delayBufferSample *delayBufferPtr = m_DelayBuffer;
#endif

#if RSG_CPU_INTEL && S16_DELAY_BUFFER
		const Vector_4V oneOverDivisor = V4VConstant<0x38000100,0x38000100,0x38000100,0x38000100>();// 1.f/32768
		const Vector_4V scalar = V4VConstant<0x47000000,0x47000000,0x47000000,0x47000000>(); // 32768
#endif

		u32 processingChannelIndex = 0;
		for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
		{
			if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId && (m_ChannelMask & (1 << inputChannelIndex)))
			{
				//We need to process this input channel.

				if(m_DelaySamples[inputChannelIndex] >= kMixBufNumSamples) //Ensure that the delay time is long enough to be valid (otherwise bypass.)
				{
					Vector_4V *samples = (Vector_4V*)buffer.ChannelBuffers[inputChannelIndex];

					delayBufferSample *delayBuffer = &(delayBufferPtr[processingChannelIndex * m_MaxDelaySamples]);

					u32 delayBufferReadOffset = (m_MaxDelaySamples + m_DelayBufferWriteOffset - m_DelaySamples[inputChannelIndex]) % m_MaxDelaySamples;
					Assert((delayBufferReadOffset&15) == 0);


					Vector_4V *delayReadBuffer = (Vector_4V*)(delayBuffer + delayBufferReadOffset);
					Vector_4V *delayWriteBuffer = (Vector_4V*)(delayBuffer + m_DelayBufferWriteOffset);

#if __XENON
					PrefetchDC2(delayReadBuffer, 0);
					PrefetchDC2(delayWriteBuffer, 0);
#endif

					const Vector_4V channelVals = m_ChannelScalars[inputChannelIndex];

					const Vector_4V gainLin = V4SplatX(channelVals);
					const Vector_4V feedbackGainLin = V4SplatY(channelVals);
					const Vector_4V dryLin = V4SplatZ(channelVals);

#if S16_DELAY_BUFFER
					// process one 128 byte cache line at a time (64 samples at 2bytes/sample)
					enum {kNumSamplesPerBlock = 64};
#else
					// process one 128 byte cache line at a time (32 samples at 4bytes/sample)
					enum {kNumSamplesPerBlock = 32};
#endif
					for(u32 blockIndex = 0; blockIndex < kMixBufNumSamples / kNumSamplesPerBlock; blockIndex++)
					{
#if __XENON
						// Prefetch the next block / cache-line
						PrefetchDC2(delayReadBuffer, 128);
						PrefetchDC2(delayWriteBuffer, 128);
#endif

						for(u32 sampleCounter = 0; sampleCounter < kNumSamplesPerBlock; sampleCounter += 8)
						{
							//const u32 sampleIndex = blockIndex*kNumSamplesPerBlock + sampleCounter;

#if S16_DELAY_BUFFER
							// read 8 samples from the delay line (16 bytes)
							const Vector_4V delayBufferIntSamples = *delayReadBuffer++;

							// unpack into 8 32bit ints (splitting into two vectors)
							const Vector_4V delayBufferInts0 = V4UnpackLowSignedShort(delayBufferIntSamples);
							const Vector_4V delayBufferInts1 = V4UnpackHighSignedShort(delayBufferIntSamples);

							// convert to float
#if RSG_CPU_INTEL
							// On PC V4IntoToFloatRaw is only efficient when exponent = 0, so scale manually
							const Vector_4V delayBuffer0 = V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(delayBufferInts0));
							const Vector_4V delayBuffer1 = V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(delayBufferInts1));
#else
							const Vector_4V delayBuffer0 = V4IntToFloatRaw<15>(delayBufferInts0);
							const Vector_4V delayBuffer1 = V4IntToFloatRaw<15>(delayBufferInts1);
#endif
#else
							// f32 delay buffer
							const Vector_4V delayBuffer0 = *delayReadBuffer++;
							const Vector_4V delayBuffer1 = *delayReadBuffer++;
#endif

							// perform delay processing: output is delay line*gain + input * dry
							//input + output*feedback is written to delay line
							const Vector_4V inputSamples0 = *samples;
							const Vector_4V scaledDelay0 = V4Scale(delayBuffer0, gainLin);
							*samples = V4AddScaled(scaledDelay0, inputSamples0, dryLin);

							const Vector_4V delayInput0 = V4AddScaled(inputSamples0, scaledDelay0, feedbackGainLin);
							samples++;

							const Vector_4V inputSamples1 = *samples;
							const Vector_4V scaledDelay1 = V4Scale(delayBuffer1, gainLin);
							*samples = V4AddScaled(scaledDelay1, inputSamples1, dryLin);
							const Vector_4V delayInput1 = V4AddScaled(inputSamples1, scaledDelay1, feedbackGainLin);
							samples++;

							// pack into 16bit values for storage
#if S16_DELAY_BUFFER
#if RSG_CPU_INTEL
							// again, V4FloatToIntRaw is slow when exponent != 0 on PC, so scale manually
							*delayWriteBuffer++ = V4PackSignedIntToSignedShort(
								V4FloatToIntRaw<0>(V4Scale(scalar,delayInput0)),
								V4FloatToIntRaw<0>(V4Scale(scalar, delayInput1)));
#else
							*delayWriteBuffer++ = V4PackSignedIntToSignedShort(V4FloatToIntRaw<15>(delayInput0),V4FloatToIntRaw<15>(delayInput1));
#endif
#else
							// f32 delay buffer
							*delayWriteBuffer++ = delayInput0;
							*delayWriteBuffer++ = delayInput1;
#endif

						} // sampleIndex
					}// block index 
				}

				processingChannelIndex++;
			}

		} //inputChannelIndex

		//Update ring buffer write offset.
		m_DelayBufferWriteOffset = (m_DelayBufferWriteOffset + kMixBufNumSamples) % m_MaxDelaySamples;

#if __SPU
		// Update main memory delay buffer

		// TODO: For now grab the whole delay buffer; will change this to grab only the required data for this frame
		sysDmaLargePutAndWait(delayBufferPtr, (uint64_t)m_DelayBuffer, delayBufferSize, 8);
#endif
	}

	void audDelayEffect::SetParam(const u32 paramId, const u32 value)
	{
		if(paramId >= FrontLeftDelayTime && paramId <= RearRightDelayTime)
		{
			const u32 channelIndex = paramId - FrontLeftDelayTime;
			const u32 delaySamplesUnaligned	= value * (kMixerNativeSampleRate/1000);
			//Align the sample length of the delay to an integer multiple of the software mixer frame size.
			m_DelaySamples[channelIndex] = kMixBufNumSamples * (u32)ceilf((f32)delaySamplesUnaligned / (f32)kMixBufNumSamples);
		}
		else if(paramId == Bypass)
		{
			m_Bypass = (value != 0);
		}
		else
		{
			audAssertf(false, "paramId %u is not a valid audDelayEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
		}
	}

	void audDelayEffect::SetParam(const u32 paramId, const f32 value)
	{
		if(paramId >= FrontLeftGain	&& paramId <= RearRightGain)
		{
			const u32 channelIndex = paramId - FrontLeftGain;
			Vector_4V inputVal = V4LoadScalar32IntoSplatted(value);
			Vector_4V gainIndBs = audDriverUtil::ComputeLinearVolumeFromDb(inputVal);
			m_ChannelScalars[channelIndex] = V4PermuteTwo<X2,Y1,Z1,W1>(m_ChannelScalars[channelIndex], gainIndBs);
		}
		else if(paramId >= FrontLeftFeedback && paramId <= RearRightFeedback)
		{
			const u32 channelIndex = paramId - FrontLeftFeedback;
			Vector_4V inputVal = V4LoadScalar32IntoSplatted(value);
			Vector_4V feedbackIndBs = audDriverUtil::ComputeLinearVolumeFromDb(inputVal);
			m_ChannelScalars[channelIndex] = V4PermuteTwo<X1,Y2,Z1,W1>(m_ChannelScalars[channelIndex], feedbackIndBs);
		}
		else if(paramId >= FrontLeftDryMix && paramId <= RearRightDryMix)
		{
			const u32 channelIndex = paramId - FrontLeftDryMix;
			Vector_4V inputVal = V4LoadScalar32IntoSplatted(value);			
			Vector_4V dryMix = audDriverUtil::ComputeLinearVolumeFromDb(inputVal);
			m_ChannelScalars[channelIndex] = V4PermuteTwo<X1,Y1,Z2, W1>(m_ChannelScalars[channelIndex], dryMix);
		}
		else
		{
			audAssertf(false, "paramId %u is not a valid audDelayEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
		}
	}

	void audDelayEffect::Shutdown(void)
	{
		if(m_DelayBuffer)
		{
			delete[] m_DelayBuffer;
			m_DelayBuffer = NULL;
		}
	}

#if !__SPU

	audDelayEffect4::audDelayEffect4() : audDspEffect(AUD_DSPEFFECT_DELAY4)
		, m_Bypass(false)
		PS3_ONLY(, m_PrintedSpareScratch(false))
	{		

	}

	void audDelayEffect4::SetParam(const u32 paramId, const u32 value)
	{
		if(paramId == Bypass)
		{
			m_Bypass = (value != 0);
		}
		else
		{
			audAssertf(false, "paramId %u is not a valid audDelayEffect4 u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
		}
	}

	void audDelayEffect4::SetParam(const u32 paramId, const f32 value)
	{
		switch(paramId)
		{
		case DelayTime:
			if(value == 0.f)
			{
				m_DelayLine.SetLength(0);
			}
			else
			{
				m_DelayLine.SetLengthS(value, static_cast<float>(kMixerNativeSampleRate));			
			}
			break;
		case FrontLeftFeedback:
			SetX(m_Feedback, value);
			break;
		case FrontRightFeedback:
			SetY(m_Feedback, value);
			break;
		case RearLeftFeedback:
			SetZ(m_Feedback, value);
			break;
		case RearRightFeedback:
			SetW(m_Feedback, value);
			break;
		case Feedback:
			m_Feedback = V4LoadScalar32IntoSplatted(value);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audDelayEffect4 f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}	
	}

#endif // !__SPU

	bool audDelayEffect4::Init(const u32 ASSERT_ONLY(numChannels), const u32 ASSERT_ONLY(channelMask))
	{
		// For now we only support 4 channel interleaved
		Assert(numChannels == 4);
		SetProcessFormat(Interleaved);
		Assert(channelMask == (RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT));

		m_Feedback = V4VConstant(V_ZERO);

		return true;
	}

	AUD_DEFINE_DSP_PROCESS_FUNCTION(audDelayEffect4)
	{
		if(m_Bypass || !m_DelayLine.HasBuffer())
		{
			return;
		}

		Assert(buffer.Format == Interleaved);

#if AUD_TIME_DSP
		sysPerformanceTimer timer("");
		timer.Start();
#endif // AUD_TIME_DSP

#if __SPU
		const u32 tag = 4;
		m_DelayLine.Prefetch(kMixBufNumSamples, tag);
		sysDmaWait(1<<tag);
#endif // __SPU

		const u32 numSamplesPerPass = kMixBufNumSamples / 4;

		if (buffer.ChannelBufferIds[0] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[0], buffer.ChannelBuffers[0], numSamplesPerPass);
		if (buffer.ChannelBufferIds[1] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[1], buffer.ChannelBuffers[1], numSamplesPerPass);
		if (buffer.ChannelBufferIds[2] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[2], buffer.ChannelBuffers[2], numSamplesPerPass);
		if (buffer.ChannelBufferIds[3] < audFrameBufferPool::InvalidId)
			ProcessInterleaved(buffer.ChannelBuffers[3], buffer.ChannelBuffers[3], numSamplesPerPass);

#if __SPU
		m_DelayLine.Writeback(tag);
		sysDmaWait(1<<tag);
#endif // __SPU

#if AUD_TIME_DSP
		timer.Stop();
		sm_ProcessTime[AUD_DSPEFFECT_DELAY4] = timer.GetTimeMS() * 1000.f;
#endif // AUD_TIME_DSP
	}

	void audDelayEffect4::ProcessInterleaved(const float *inBuffer, float *outBuffer, const u32 numSamples)
	{
		const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outBuffer);

		const Vector_4V feedback = m_Feedback;

#if !AUD_DELAYEFFECT4_V8
		u32 count = numSamples;
		Vector_4V finiteCheck = V4VConstant(V_ZERO);
		while(count--)
		{
			Vector_4V output0 = m_DelayLine.Read();
			Vector_4V input0 = *inputPtr++;
			Vector_4V d0 = V4AddScaled(input0, output0, feedback);
			m_DelayLine.Write(d0);

			finiteCheck = V4Scale(finiteCheck, output0);

			// The 32bit delay line doesn't clamp internally - replicate that behaviour here.
			*outputPtr++ =  V4Clamp(output0, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
		}

		if(!V4IsFiniteAll(finiteCheck))
		{
			audErrorf("%u: non-finite number in delay line %p", audDriver::GetMixer()->GetMixerTimeFrames(), this);
			sysMemSet128(outBuffer, 0, sizeof(Vector_4V) * numSamples);
			m_DelayLine.ZeroBuffer();
		}
#else
		u32 count = numSamples >> 1; // two samples at a time

		while(count--)
		{
			Vector_4V output0, output1;

			Vector_4V input0 = *inputPtr++;
			Vector_4V input1 = *inputPtr++;

			m_DelayLine.Read(output0, output1);

			Vector_4V d0 = V4AddScaled(input0, output0, feedback);
			Vector_4V d1 = V4AddScaled(input1, output1, feedback);

			m_DelayLine.Write(d0, d1);

			*outputPtr++ = output0;
			*outputPtr++ = output1;
		}
#endif
	}

	void audDelayEffect4::Shutdown(void)
	{

	}

} // namespace rage

