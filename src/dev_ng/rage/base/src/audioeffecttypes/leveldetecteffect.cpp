// 
// audioeffecttypes/leveldetecteffect.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if !__WIN32PC

#include "leveldetecteffect.h"

#include "filterdefs.h"

#include "audiohardware/driver.h"
#include "audioengine/engine.h"
#include "audiohardware/driverutil.h"

#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/viewport.h"

#include "system/timemgr.h"

#if __XENON
#include "audiohardware/driver_xenon.h"
#include "audioeffecttypes/leveldetecteffect_xenon.h"
#endif

#if __PPU
#include "audiohardware/driver_psn.h"
#include <cell/mstream.h>
extern char _binary_leveldetecteffect_psn_dspfx_elf_start[];
#endif

namespace rage {
#if __XENON || __PPU
int audLevelDetectEffect::sm_PhysicalEffectId = (__XENON) ? 0 : -1;
#endif // __XENON || __PPU

audLevelDetectEffect::audLevelDetectEffect() :
audEffect()
{
	for (int i = 0; i < NUM_PLATFORM_SPEAKER_IDS; ++i)
		m_MaxLevels[i] = BANK_ONLY(m_DrawMaxLevels[i] =) m_PeakLevels[i] = m_RmsLevels[i] = 0.0f;
	BANK_ONLY(m_bDrawEnabled = false;)
#if __WIN32PC
	// TODO
#endif

#if __PPU
	m_pParamAddr = 0;
	m_pSPUData = (tLevelDetectSPUData*)audDriver::AllocateVirtual(sizeof(tLevelDetectSPUData));
	::new (m_pSPUData) tLevelDetectSPUData();
	sysMemSet(m_pSPUData, 0, sizeof(m_pSPUData));

	Displayf("audLevelDetectEffect::audLevelDetectEffect()");
#endif
}

audLevelDetectEffect::~audLevelDetectEffect()
{
#if __WIN32PC
	//TODO
#endif

#if __PPU
	m_pParamAddr = 0;
	audDriver::FreeVirtual(m_pSPUData);
#endif
}

bool audLevelDetectEffect::InitClass(void)
{
#if __PPU
	//Assert(sm_PhysicalEffectId == CELL_MS_DSPOFF);
	CellMSDSP dspInfo;
	if (!audDriverPsn::InitDspEffect(_binary_leveldetecteffect_psn_dspfx_elf_start, dspInfo))
	{
		return false;
	}
	sm_PhysicalEffectId = dspInfo.handle;
	Displayf("[Audio] LevelDetect init: %x, mem=0x%x, avail=0x%x", _binary_leveldetecteffect_psn_dspfx_elf_start, dspInfo.memoryUsed, dspInfo.memoryAvail);
	return true;
#else
	//Add other platform-specific driver init calls here.
	return true;
#endif
}

#if __PPU
// PURPOSE
//  Access function to set the Multistream DSP effect address
void audLevelDetectEffect::SetParameterAddress(const char * pParamAddr)
{
	audEffect::SetParameterAddress(pParamAddr);
	tLevelDetectSPUParamData *pParamData	= (tLevelDetectSPUParamData*)m_pParamAddr;
	if (pParamData)
	{
		sysMemSet(pParamData, 0, sizeof(*pParamData));
		
		// setup paramData, which Multistream DMAs for us
		pParamData->EA = (void*)m_pSPUData;
		pParamData->m_LastChannelProcessedIndex = -1;
	}
}
#endif

// PURPOSE
//  Copies the recent peak data to a passed in array, and zeroes out the history
// IN: A float array of size NUM_PLATFORM_SPEAKER_IDS
void audLevelDetectEffect::GetAndClearPeaks(f32* outPeakData)
{
	sysCriticalSection lock(m_csToken);
	for (int i = 0; i < NUM_PLATFORM_SPEAKER_IDS; ++i)
	{
		outPeakData[i] = m_PeakLevels[i];
		m_PeakLevels[i] = 0.0f;
	}
}


#if __BANK
// PURPOSE
//  Creates bank widgets
void audLevelDetectEffect::AddWidgets(bkBank& bank)
{
	bank.PushGroup(AUDIOENGINE.GetEffectManager().GetFactory().GetEffectNameFromNameTableOffset(GetMetadata()->NameTableOffset));
	{
		bank.AddToggle("Draw       ", &m_bDrawEnabled);
		bank.AddButton("Clear Peaks", datCallback(MFA(audLevelDetectEffect::ClearDrawPeaks),this));
	}bank.PopGroup();
}

// PURPOSE
//  Displays the meters on the screen
void audLevelDetectEffect::DebugDraw()
{
	if (!m_bDrawEnabled)
		return;
	
	const LevelDetectEffect* pMetadata = (const LevelDetectEffect*)m_Metadata;

	const f32 fFloor = pMetadata->FloorLevel;
	const f32 fPeak = pMetadata->PeakLevel;
	const f32 fDynRange = fPeak - fFloor;
	const f32 fInterval = pMetadata->IntervalLevel;

	if (fDynRange <= 0)
		return;


	// Draw the levels here
	PUSH_DEFAULT_SCREEN();
	{
		grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcState::SetCullMode(grccmNone);
		grcState::SetState(grcsDepthFunc,grcdfAlways);
		grcState::SetState(grcsDepthWrite,false);

		const f32 width = curVp->GetWidth() / 4.0f;
		const f32 height = curVp->GetHeight() / 4.0f;
		const f32 intervalHeight = height * (fInterval/fDynRange);
		const f32 left = curVp->GetWidth()/2.0f - width/2.0f;
		const f32 right = curVp->GetWidth()/2.0f + width/2.0f;
		const f32 bottom = curVp->GetHeight()*0.95f;
		const f32 top = bottom - height;
		const f32 zeroLevel = top - (fPeak*height);
		const f32 channelWidth = width/NUM_PLATFORM_SPEAKER_IDS;
		const f32 meterWidth = channelWidth*0.9f;
		const f32 meterGap = 0.5f *(channelWidth-meterWidth);

		// draw alpha'd background
		grcBegin(drawTriStrip, 4);
		grcColor(Color32(0.1f,0.1f,0.1f,0.5f));
		grcVertex2f(left-20.0f,bottom+10.0f);	
		grcVertex2f(left-20.0f,top-10.0f);
		grcVertex2f(right+20.0f,bottom+10.0f);
		grcVertex2f(right+20.0f,top-10.0f);
		grcEnd();


		static const char* szSpeakerNames[NUM_PLATFORM_SPEAKER_IDS] = 
		{
			"L",
			"R",
			"C",
			"LFE",
			"LS",
			"RS",
			"LC",
			"RC"
		};

		char szGain[6];

		//	Render levels
		m_csToken.Lock();
		for (int i = 0; i < NUM_PLATFORM_SPEAKER_IDS; ++i)
		{
			const f32 fPeakDb = audDriverUtil::ComputeDbVolumeFromLinear_Precise(Max(m_MaxLevels[i], m_DrawMaxLevels[i]));
			const f32 fPeakLevel = Clamp((fPeakDb-fFloor) / fDynRange, 0.0f, 1.0f);
			const f32 fRmsDb = audDriverUtil::ComputeDbVolumeFromLinear_Precise(m_RmsLevels[i]);
			const f32 fRmsLevel = Clamp((fRmsDb-fFloor) / fDynRange, 0.0f, 1.0f);

			const f32 meterLeft = left + (i*channelWidth) + meterGap;
			const f32 meterRight = meterLeft + (0.9f*channelWidth) - meterGap;
			const f32 peakMeterHeight = height*fPeakLevel;
			const f32 peakMeterTop = bottom - peakMeterHeight;
			const f32 rmsMeterHeight = height*fRmsLevel;
			const f32 rmsMeterTop = bottom - rmsMeterHeight;

			grcBegin(drawTriStrip, 4);
			grcColor(fPeakDb <= 0.0f ? Color32(fPeakLevel,1.0f,0.0f,0.5f) : Color32(1.0f,0.0f,0.0f,0.5f));
			{
				grcVertex2f(meterLeft, bottom);	
				grcVertex2f(meterLeft, peakMeterTop);
				grcVertex2f(meterRight, bottom);	
				grcVertex2f(meterRight, peakMeterTop);
			}grcEnd();

			grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
			grcDraw2dText(meterLeft, bottom, szSpeakerNames[i]);

			//	Draw a blue bar to represent RMS levels
			grcBegin(drawTriStrip, 4);
			grcColor(Color32(0.0f,0.0f,1.0f,0.9f));
			{
				grcVertex2f(meterLeft, bottom);	
				grcVertex2f(meterLeft, rmsMeterTop);
				grcVertex2f(meterRight, bottom);	
				grcVertex2f(meterRight, rmsMeterTop);
			}grcEnd();


			//	Begin falling off
			f32 fFallDb = fPeakDb;
			Approach(fFallDb, -100.0f, fDynRange, TIME.GetUnwarpedRealtimeSeconds());
			m_DrawMaxLevels[i] = fFallDb > -100.0f ? audDriverUtil::ComputeLinearVolumeFromDb(fFallDb) : 0.0f;

			if (AUD_GET_TRISTATE_VALUE(pMetadata->Flags, FLAG_ID_LEVELDETECTEFFECT_DISPLAYPEAKS)==AUD_TRISTATE_TRUE)
			{
				if (m_DrawPeakLevels[i] > 0.0f)
				{
					const f32 fPeakDb = audDriverUtil::ComputeDbVolumeFromLinear_Precise(m_DrawPeakLevels[i]);
					formatf(szGain, sizeof(szGain), "%.1f", fPeakDb);
					grcColor(fPeakDb <= 0.0f ? Color32(0.0f,1.0f,0.0f,0.9f) : Color32(1.0f,0.0f,0.0f,0.9f));
					grcDraw2dText(meterLeft, top-10.0f, szGain);
				}
			}
			else
				m_DrawPeakLevels[i] = m_MaxLevels[i];
		}
		m_csToken.Unlock();

		const u32 uIntervalUp = (u32)Max(fPeak / fInterval, 0.0f);
		const u32 uIntervalDn = (u32)Max(-fFloor / fInterval, 0.0f);

		//	Start from 0
		if (zeroLevel <= top)
		{
			//	Horizontal line
			grcColor(Color32(0.0f,0.0f,0.0f,1.0f));
			grcBegin(drawLines, 2);
			{
				grcVertex2f(left, zeroLevel);
				grcVertex2f(right, zeroLevel);
			}grcEnd();

			formatf(szGain, sizeof(szGain), "%.1f", 0.0f);
			grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
			grcDraw2dText(left-20.0f, zeroLevel, szGain);
		}

		//	Print gain levels above 0
		for (u32 i = 1; i < uIntervalUp; ++i)
		{
			f32 fValue = i*fInterval;
			f32 fY = zeroLevel - (i*intervalHeight);

			//	Horizontal line
			grcColor(Color32(0.0f,0.0f,0.0f,1.0f));
			grcBegin(drawLines, 2);
			{
				grcVertex2f(left, fY);
				grcVertex2f(right, fY);
			}grcEnd();

			formatf(szGain, sizeof(szGain), "%.1f", fValue);
			grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
			grcDraw2dText(left-20.0f, fY, szGain);			
		}

		//	Print gain levels below 0
		for (u32 i = 1; i < uIntervalDn; ++i)
		{
			f32 fValue = i*fInterval;
			f32 fY = zeroLevel + (i*intervalHeight);

			//	Horizontal line
			grcColor(Color32(0.0f,0.0f,0.0f,1.0f));
			grcBegin(drawLines, 2);
			{
				grcVertex2f(left, fY);
				grcVertex2f(right, fY);
			}grcEnd();

			formatf(szGain, sizeof(szGain), "%.1f", -fValue);
			grcColor(Color32(1.0f,1.0f,1.0f,0.9f));
			grcDraw2dText(left-20.0f, fY, szGain);			
		}
	}POP_DEFAULT_SCREEN();
}

// PURPOSE
//  Resets the displayed peak values
void audLevelDetectEffect::ClearDrawPeaks()
{
	sysCriticalSection lock(m_csToken);
	sysMemCpy(m_DrawPeakLevels, m_MaxLevels, sizeof(m_DrawPeakLevels));
}
#endif // __BANK

// PURPOSE
//	Initializes an instance of the specific effect type.
bool audLevelDetectEffect::Init(const void *pMetadata, u32 effectChainIndex)
{
	if (!audEffect::Init(pMetadata, effectChainIndex))
	{
		return false;
	}

	//	Do stuff here

#if __PPU
	m_pSPUData->m_ChannelMask = m_Metadata->ChannelMask.Value;
#endif

	return true;
}

void audLevelDetectEffect::Update()
{
	UpdateReadIndex();

	//Perform a platform-specific update of effect parameters.
#if __XENON
	IXAudio2Voice *submixVoice = (IXAudio2Voice *)m_PhysicalSubmix;
	if(submixVoice)
	{
		tLevelDetectValues curValues;
		submixVoice->GetEffectParameters(GetEffectChainSubmixIndex(), &curValues, sizeof(curValues));

		{
			sysCriticalSection lock(m_csToken); // Lock so the debug drawing and committing settings doesn't step on us
			for (int i = 0; i < NUM_PLATFORM_SPEAKER_IDS; ++i)
			{
				m_PeakLevels[i] = Max(curValues.m_MaxLevels[i], m_PeakLevels[i]);	
#if __BANK
				m_DrawPeakLevels[i] = Max(curValues.m_MaxLevels[i], m_DrawPeakLevels[i]);
#endif
			}
		}
		sysMemCpy(m_MaxLevels, &curValues.m_MaxLevels, sizeof(m_MaxLevels));
		sysMemCpy(m_RmsLevels, &curValues.m_RmsLevels, sizeof(m_RmsLevels));
	}
#elif __WIN32PC
	//	TODO
#elif __PPU
	
	{
		sysCriticalSection lock(m_csToken); // Lock so the debug drawing and committing settings doesn't step on us
		for (int i = 0; i < NUM_PLATFORM_SPEAKER_IDS; ++i)
		{
			if (!BIT_ISSET(i, m_pSPUData->m_ClearMaxLevelsMask))
			{
				m_PeakLevels[i] = Max(m_pSPUData->m_Values.m_MaxLevels[i], m_PeakLevels[i]);
#if __BANK
				m_DrawPeakLevels[i] = Max(m_pSPUData->m_Values.m_MaxLevels[i], m_DrawPeakLevels[i]);
#endif
				m_pSPUData->m_ClearMaxLevelsMask = BIT_SET(i, m_pSPUData->m_ClearMaxLevelsMask);
			}
		}
	}
	sysMemCpy(m_MaxLevels, &m_pSPUData->m_Values.m_MaxLevels, sizeof(m_MaxLevels));
	sysMemCpy(m_RmsLevels, &m_pSPUData->m_Values.m_RmsLevels, sizeof(m_RmsLevels));
#endif

	audEffect::Update();
}



void* audLevelDetectEffect::GetEffectSettings()
{
	return NULL;
}

void audLevelDetectEffect::CommitSettings()
{
	audEffect::CommitSettings();

	// If we have a child, call CommitSettings on that effect too
	if (m_NextEffect)
	{
		m_NextEffect->CommitSettings();
	}
}


}//	namespace rage


#endif //!__WIN32PC
