// 
// audioeffecttypes/channelmetereffect.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOEFFECTTYPES_CHANNELMETEREFFECT_H 
#define AUDIOEFFECTTYPES_CHANNELMETEREFFECT_H 

#if __BANK && !__WIN32PC

#include "effect.h"
#include "data/base.h"

#include "audiohardware/driverdefs.h"

#include "math/amath.h"
#include "system/xtl.h"



namespace rage {

class bkBank;
class audChannelMeterEffectPc;
struct tChannelMeterSPUData;

// PURPOSE
//  The audChannelMeterEffect passes the audio through it unaltered, but is able to display peak levels for each
//	input channel if you call DebugDraw() on it from your debug rendering thread
class audChannelMeterEffect : public datBase, public audEffect
{
public:
	audChannelMeterEffect();
	virtual ~audChannelMeterEffect();

	// PURPOSE
	//	Initializes an instance of the effect. Sets up all effect settings to sensible defaults.
	virtual bool Init(const void *pMetadata, u32 effectChainIndex);

	// PURPOSE
	//  Sets appropriate values on the per-platform implementation of the filter, and updates the next effect.
	virtual void Update();

	// PURPOSE
	//  Returns NULL
	virtual void* GetEffectSettings();

	// PURPOSE
	// Commits the current requested setting, so that they may be used by the audio thread.
	// In gamecode, this MUST be called one per frame after updating any effect setting parameters.
	virtual void CommitSettings();

	// PURPOSE
	//  Performs platform-specific initialization for this effect type.
	// RETURNS
	//  true if initialization was successful.
	static bool InitClass(void);

	// PURPOSE
	//  Creates bank widgets
	void AddWidgets(bkBank& bank);
	
	// PURPOSE
	//  Displays the meters on the screen
	void DebugDraw();

	// PURPOSE
	//  Resets the displayed peak values
	void ClearPeaks();

#if __XENON || __PPU
	//
	// PURPOSE
	//	Accessor function for the Xenon-specific effect ID.
	// RETURNS
	//	The effect ID.
	//
	virtual int GetPhysicalEffectId(void) const
	{
		return sm_PhysicalEffectId;
	}
#endif // __XENON || __PPU

#if __WIN32PC
	virtual audDspEffect *GetDspEffect()
	{
		return (audDspEffect*)m_WaveshaperEffectPc;
	}
#endif


private:

#if __XENON || __PPU
	static int sm_PhysicalEffectId;
#endif // __XENON || __PPU

#if __WIN32PC
	audWaveshaperEffect *m_WaveshaperEffectPc;
#endif

#if __PPU
	tChannelMeterSPUData* m_pSPUData;
#endif

	f32 m_fMaxLevels[NUM_SPEAKER_IDS];
	f32 m_fPeakLevels[NUM_SPEAKER_IDS];
	sysCriticalSectionToken	m_csToken;	//	Thread safe
	bool m_bDrawEnabled;
};




} // namespace rage

#else // __BANK
#include "nulleffect.h"
namespace rage
{
	class audChannelMeterEffect : public audNullEffect { };
}
#endif // __BANK

#endif // AUDIOEFFECTTYPES_CHANNELMETEREFFECT_H 
