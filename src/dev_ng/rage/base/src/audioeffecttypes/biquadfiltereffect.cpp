// 
// audioeffecttypes/biquadfiltereffect.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


#include "biquadfiltereffect.h"
#include "system/memops.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/driver.h"
#include "audiohardware/device.h"
#include "audiohardware/mixer.h"
#include "vectormath/vectormath.h"
#include "math/amath.h"

#define Undenormalize(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)

namespace rage {

	using namespace Vec;

	audBiquadFilterEffect::audBiquadFilterEffect() : audDspEffect(AUD_DSPEFFECT_BIQUAD_FILTER)
	{
		m_Frequency = BIQUAD_FREQ_DEFAULT;
		m_Resonance = audDriverUtil::ComputeLinearVolumeFromDb(BIQUAD_RESONANCE_DEFAULT - 3.01f);
		m_Gain		= audDriverUtil::ComputeLinearVolumeFromDb(BIQUAD_GAIN_DEFAULT);
		m_Mode		= BIQUAD_MODE_DEFAULT;
		m_Bypass	= false;
		m_Is4Pole = ((m_Mode == BIQUAD_MODE_LOW_PASS_4POLE) || (m_Mode == BIQUAD_MODE_HIGH_PASS_4POLE) ||
			(m_Mode == BIQUAD_MODE_BAND_PASS_4POLE) || (m_Mode == BIQUAD_MODE_BAND_STOP_4POLE) ||
			(m_Mode == BIQUAD_MODE_ULTIMATE_PEAKING_EQ));

		// set up identity coefficients
		m_a[0] = 1.0f;
		m_a[1] = 0.0f;
		m_a[2] = 0.0f;
		m_b[0] = 0.0f;
		m_b[1] = 0.0f;

		m_RecomputeCoefficients = false;
	}

	bool audBiquadFilterEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		Assign(m_NumInputChannels, numChannels);
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i=0; i<numChannels; i++)
		{
			if(m_ChannelMask & (1 << i))
			{
				m_NumChannelsToProcess++;
			}
		}

		sysMemSet(m_xy_n1_1, 0, m_NumChannelsToProcess * sizeof(f32));
		sysMemSet(m_xy_n2_1, 0, m_NumChannelsToProcess * sizeof(f32));
		sysMemSet(m_xy_n1_2, 0, m_NumChannelsToProcess * sizeof(f32));
		sysMemSet(m_xy_n2_2, 0, m_NumChannelsToProcess * sizeof(f32));

		// set up identity coefficients
		m_a[0] = 1.0f;
		m_a[1] = 0.0f;
		m_a[2] = 0.0f;
		m_b[0] = 0.0f;
		m_b[1] = 0.0f;

		return true;
	}

	void audBiquadFilterEffect::SetParam(const u32 paramId, const u32 value)
	{
		bool haveSettingsChanged = false;
		switch(paramId)
		{
		case CutoffFrequency:
			m_Frequency = static_cast<f32>(Clamp(value, (u32)BIQUAD_FREQ_MIN, (u32)BIQUAD_FREQ_MAX));
			break;
		case Bandwidth:
			m_Bandwidth = Min(static_cast<f32>(Clamp(value, (u32)BIQUAD_BANDWIDTH_MIN, (u32)BIQUAD_BANDWIDTH_MAX)), m_Frequency * 2.0f);
			break;
		case Mode:
			{
				const s32 newMode = Clamp((s32)value, (s32)BIQUAD_MODE_MIN, (s32)BIQUAD_MODE_MAX);
				haveSettingsChanged = (m_Mode!=newMode);
				m_Mode = newMode;
				m_Is4Pole = ((m_Mode == BIQUAD_MODE_LOW_PASS_4POLE) || (m_Mode == BIQUAD_MODE_HIGH_PASS_4POLE) ||
					(m_Mode == BIQUAD_MODE_BAND_PASS_4POLE) || (m_Mode == BIQUAD_MODE_BAND_STOP_4POLE) ||
					(m_Mode == BIQUAD_MODE_PEAKING_EQ_4POLE) || (m_Mode == BIQUAD_MODE_ULTIMATE_PEAKING_EQ));
			}
			break;
		case Bypass:
			{
				const bool newBypass = (value != 0);
				haveSettingsChanged = (newBypass != m_Bypass);
				m_Bypass = newBypass;
				if (m_Bypass && haveSettingsChanged)
				{
					//Clear sample history so we don't click when coming out of bypass.
					sysMemSet(m_xy_n1_1, 0, m_NumChannelsToProcess * sizeof(f32));
					sysMemSet(m_xy_n2_1, 0, m_NumChannelsToProcess * sizeof(f32));
					sysMemSet(m_xy_n1_2, 0, m_NumChannelsToProcess * sizeof(f32));
					sysMemSet(m_xy_n2_2, 0, m_NumChannelsToProcess * sizeof(f32));
				}
			}
			break;
		case ProcessFormat:
			SetProcessFormat((audMixBufferFormat)value);
			break;
		default:
			audAssertf(false, "paramId %u is not a valid audBiquadFilterEffect u32 param id (val: %u; as f32: %f)", paramId, value, *(f32*)&value);
			break;
		}	

		if(haveSettingsChanged)
		{
			m_RecomputeCoefficients = true;
		}
	}

	void audBiquadFilterEffect::SetParam(const u32 paramId, const f32 value)
	{
		bool haveSettingsChanged = true;

		switch(paramId)
		{
		case CutoffFrequency:
			m_Frequency = Clamp(value, BIQUAD_FREQ_MIN, BIQUAD_FREQ_MAX);
			break;
		case ResonanceLevel:
			//0dB of resonance should give us -3 dB (0.7071) as our gain at Fc.
			m_Resonance = audDriverUtil::ComputeLinearVolumeFromDb(Clamp(value, BIQUAD_RESONANCE_MIN, BIQUAD_RESONANCE_MAX) - 3.01f);
			break;
		case Bandwidth:
			//Clamp the bandwidth to ensure we don't go below DC - and into a world of filter
			//stability pain.
			m_Bandwidth = Min(Clamp(value, BIQUAD_BANDWIDTH_MIN, BIQUAD_BANDWIDTH_MAX), m_Frequency * 2.0f);
			break;
		case Gain:
			m_Gain = audDriverUtil::ComputeLinearVolumeFromDb(Clamp(value, BIQUAD_GAIN_MIN, BIQUAD_GAIN_MAX));
			break;
		default:
			haveSettingsChanged = false;
			audAssertf(false, "paramId %u is not a valid audBiquadFilterEffect f32 param id (val: %f; as u32: %u)", paramId, value, *(u32*)&value);
			break;
		}

		if(haveSettingsChanged)
		{
			m_RecomputeCoefficients = true;
		}
	}

	void audBiquadFilterEffect::Shutdown(void)
	{
	}


#ifdef DISABLE_PC_DSP_EFFECTS
	AUD_DEFINE_DSP_PROCESS_FUNCTION(audBiquadFilterEffect)
	{

	}
#else
	AUD_DEFINE_DSP_PROCESS_FUNCTION(audBiquadFilterEffect)
	{
		if (m_Bypass)
			return;

		if(m_RecomputeCoefficients)
		{
			ComputeCoefficients();
		}

		if(buffer.Format == Interleaved)
		{
			if (buffer.ChannelBufferIds[0] < audFrameBufferPool::InvalidId && buffer.ChannelBufferIds[1] < audFrameBufferPool::InvalidId &&
				buffer.ChannelBufferIds[2] < audFrameBufferPool::InvalidId && buffer.ChannelBufferIds[3] < audFrameBufferPool::InvalidId)
			{
				Assert(m_NumInputChannels == 4);
				Assert(m_NumChannelsToProcess == 4);
				Assert(m_ChannelMask == (RAGE_SPEAKER_MASK_FRONT_LEFT | RAGE_SPEAKER_MASK_FRONT_RIGHT | RAGE_SPEAKER_MASK_BACK_LEFT | RAGE_SPEAKER_MASK_BACK_RIGHT));

				Vector_4V a = *((Vector_4V*)&m_a[0]);
				Vector_4V b = *((Vector_4V*)&m_b[0]);

				Vector_4V xy_n11 = *((Vector_4V*)&m_xy_n1_1[0]);
				Vector_4V xy_n21 = *((Vector_4V*)&m_xy_n1_2[0]);

				const u32 numSamplesPerPass = kMixBufNumSamples / 4;

				if (m_Is4Pole)
				{
					Vector_4V xy_n12 = *((Vector_4V*)&m_xy_n2_1[0]);
					Vector_4V xy_n22 = *((Vector_4V*)&m_xy_n2_2[0]);

					ProcessInterleaved_4Pole(a, b, xy_n11, xy_n12, xy_n21, xy_n22, buffer.ChannelBuffers[0], buffer.ChannelBuffers[0], numSamplesPerPass);
					ProcessInterleaved_4Pole(a, b, xy_n11, xy_n12, xy_n21, xy_n22, buffer.ChannelBuffers[1], buffer.ChannelBuffers[1], numSamplesPerPass);
					ProcessInterleaved_4Pole(a, b, xy_n11, xy_n12, xy_n21, xy_n22, buffer.ChannelBuffers[2], buffer.ChannelBuffers[2], numSamplesPerPass);
					ProcessInterleaved_4Pole(a, b, xy_n11, xy_n12, xy_n21, xy_n22, buffer.ChannelBuffers[3], buffer.ChannelBuffers[3], numSamplesPerPass);

					*((Vector_4V*)&m_xy_n2_1[0]) = xy_n12;
					*((Vector_4V*)&m_xy_n2_2[0]) = xy_n22;
				}
				else
				{
					ProcessInterleaved_2Pole(a, b, xy_n11, xy_n21, buffer.ChannelBuffers[0], buffer.ChannelBuffers[0], numSamplesPerPass);
					ProcessInterleaved_2Pole(a, b, xy_n11, xy_n21, buffer.ChannelBuffers[1], buffer.ChannelBuffers[1], numSamplesPerPass);
					ProcessInterleaved_2Pole(a, b, xy_n11, xy_n21, buffer.ChannelBuffers[2], buffer.ChannelBuffers[2], numSamplesPerPass);
					ProcessInterleaved_2Pole(a, b, xy_n11, xy_n21, buffer.ChannelBuffers[3], buffer.ChannelBuffers[3], numSamplesPerPass);
				}


				// non-finite result check
				const Vector_4V finiteCheck = V4Scale(xy_n11, xy_n21);
				if (!V4IsFiniteAll(finiteCheck))
				{
					audErrorf("%u: Interleaved %s biquad filter non-finite state: resetting: %p", audDriver::GetMixer()->GetMixerTimeFrames(), m_Is4Pole ? "4-Pole" : "2-Pole", this);

					*((Vector_4V*)&m_xy_n2_1[0]) = *((Vector_4V*)&m_xy_n2_2[0]) = xy_n11 = xy_n21 = V4VConstant(V_ZERO);

					sysMemSet(buffer.ChannelBuffers[0], 0, numSamplesPerPass * sizeof(Vector_4V));
					sysMemSet(buffer.ChannelBuffers[1], 0, numSamplesPerPass * sizeof(Vector_4V));
					sysMemSet(buffer.ChannelBuffers[2], 0, numSamplesPerPass * sizeof(Vector_4V));
					sysMemSet(buffer.ChannelBuffers[3], 0, numSamplesPerPass * sizeof(Vector_4V));
				}

				*((Vector_4V*)&m_xy_n1_1[0]) = xy_n11;
				*((Vector_4V*)&m_xy_n1_2[0]) = xy_n21;
			}
		}
		else
		{
			const f32 a_0 = m_a[0], a_1 = m_a[1], a_2 = m_a[2], b_1 = m_b[0], b_2 = m_b[1];

			f32 xy_n1_1 = 0.f, xy_n2_1 = 0.f, xy_n1_2 = 0.f, xy_n2_2 = 0.f;
			f32 *samples;
			//f32 currentSample, newSample;
			f32 currentSample1, currentSample2, newSample1, newSample2;
			u32 channelIndex = 0;

			for(u32 inputChannelIndex=0; inputChannelIndex<m_NumInputChannels; inputChannelIndex++)
			{
				if(m_ChannelMask & (1 << inputChannelIndex))
				{
					//We need to process this input channel.

					if (buffer.ChannelBufferIds[inputChannelIndex] < audFrameBufferPool::InvalidId)
					{
						samples = buffer.ChannelBuffers[inputChannelIndex];

						xy_n1_1 = m_xy_n1_1[channelIndex];
						xy_n2_1 = m_xy_n2_1[channelIndex];
						xy_n1_2 = m_xy_n1_2[channelIndex];
						xy_n2_2 = m_xy_n2_2[channelIndex];

						if (m_Is4Pole)
						{
							for (u32 j = 0; j < kMixBufNumSamples; ++j)
							{
								currentSample1 = *samples;
								newSample1 = (a_0 * currentSample1) + xy_n1_1;
								currentSample2 = newSample1; //Current sample is now output of last stage.
								newSample2 = (a_0 * currentSample2) + xy_n1_2;

								xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
								xy_n1_2 = (a_1 * currentSample2) - (b_1 * newSample2) + xy_n2_2;
								xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);
								xy_n2_2 = (a_2 * currentSample2) - (b_2 * newSample2);

								*samples++ = (newSample2 * m_Gain);
							}
						}
						else //2-pole.
						{
							for (u32 j = 0; j < kMixBufNumSamples; ++j)
							{
								currentSample1 = *samples;
								newSample1 = (a_0 * currentSample1) + xy_n1_1;

								xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
								xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);

								*samples++ = (newSample1 * m_Gain);
							}
						}

						if (!FPIsFinite(xy_n1_1 * xy_n2_1))
						{
							audErrorf("%u: Non-interleaved %s biquad filter non-finite, channel %u.  Resetting: %p", audDriver::GetMixer()->GetMixerTimeFrames(), m_Is4Pole ? "4-Pole" : "2-Pole", inputChannelIndex, this);
							xy_n1_1 = xy_n2_1 = xy_n1_2 = xy_n2_2 = 0.f;
							sysMemSet(buffer.ChannelBuffers[inputChannelIndex], 0, sizeof(float) * kMixBufNumSamples);
						}
					}

					m_xy_n1_1[channelIndex] = Undenormalize(xy_n1_1);
					m_xy_n2_1[channelIndex] = Undenormalize(xy_n2_1);
					m_xy_n1_2[channelIndex] = Undenormalize(xy_n1_2);
					m_xy_n2_2[channelIndex] = Undenormalize(xy_n2_2);

					channelIndex++;
				}
			}

		} //inputChannelIndex
	}
#endif // DISABLE DSP

	void audBiquadFilterEffect::ProcessInterleaved_4Pole(Vec::Vector_4V_In a, Vec::Vector_4V_In b,
		Vec::Vector_4V_InOut xy11, Vec::Vector_4V_InOut xy12, Vec::Vector_4V_InOut xy21, Vec::Vector_4V_InOut xy22,
		const float *inBuffer, float *outBuffer, const u32 numSamples)
	{
		const Vector_4V a0 = V4SplatX(a);
		const Vector_4V a1 = V4SplatY(a);
		const Vector_4V a2 = V4SplatZ(a);
		const Vector_4V b1 = V4SplatX(b);
		const Vector_4V b2 = V4SplatY(b);

		const Vector_4V gain = V4LoadScalar32IntoSplatted(m_Gain);

		const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outBuffer);	

		u32 count = numSamples;

		while(count--)
		{
			const Vec::Vector_4V currentSample1 = *inputPtr++;
			const Vec::Vector_4V newSample1 = Vec::V4AddScaled(xy11, a0, currentSample1);
			const Vec::Vector_4V currentSample2 = newSample1; //Current sample is now output of last stage.
			const Vec::Vector_4V newSample2 = Vec::V4AddScaled(xy12,  a0, currentSample2);

			xy11 = Vec::V4Add(Vec::V4Subtract(Vec::V4Scale(a1, currentSample1), Vec::V4Scale(b1, newSample1)), xy21);
			xy12 = Vec::V4Add(Vec::V4Subtract(Vec::V4Scale(a1, currentSample2), Vec::V4Scale(b1, newSample2)), xy22);
			xy21 = Vec::V4Subtract(Vec::V4Scale(a2, currentSample1), Vec::V4Scale(b2, newSample1));
			xy22 = Vec::V4Subtract(Vec::V4Scale(a2, currentSample2), Vec::V4Scale(b2, newSample2));

			*outputPtr++ = V4Scale(newSample2, gain);
		}
	}

	void audBiquadFilterEffect::ProcessInterleaved_2Pole(Vec::Vector_4V_In a, Vec::Vector_4V_In b,
		Vec::Vector_4V_InOut xy11, Vec::Vector_4V_InOut xy21,
		const float *inBuffer, float *outBuffer, const u32 numSamples)
	{
		const Vector_4V a0 = V4SplatX(a);
		const Vector_4V a1 = V4SplatY(a);
		const Vector_4V a2 = V4SplatZ(a);
		const Vector_4V b1 = V4SplatX(b);
		const Vector_4V b2 = V4SplatY(b);

		const Vector_4V gain = V4LoadScalar32IntoSplatted(m_Gain);

		const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outBuffer);	

		u32 count = numSamples;

		while(count--)
		{
			const Vec::Vector_4V currentSample1 = *inputPtr++;
			const Vec::Vector_4V newSample1 = Vec::V4AddScaled(xy11, a0, currentSample1);

			xy11 = Vec::V4Add(Vec::V4Subtract(Vec::V4Scale(a1, currentSample1), Vec::V4Scale(b1, newSample1)), xy21);			
			xy21 = Vec::V4Subtract(Vec::V4Scale(a2, currentSample1), Vec::V4Scale(b2, newSample1));			

			*outputPtr++ = V4Scale(newSample1, gain);
		}
	}

	void audBiquadFilterEffect::ComputeCoefficients(void)
	{
		if (m_Bypass)
			return;

		const f32 fs = (f32)kMixerNativeSampleRate;

		//Clamp the bandwidth to ensure we don't go below DC - and into a world of filter
		//stability pain.
		f32 cutoffFrequency = m_Frequency;

		if(m_Mode == BIQUAD_MODE_HIGH_PASS_4POLE)
		{
			// 0Hz is unstable on x64
			cutoffFrequency = Max(1.f, cutoffFrequency);
		}
		f32 bandwidth = Min(m_Bandwidth, cutoffFrequency * 2.0f);

		f32 a0 = 0.0f, a1 = 0.0f, a2 = 0.0f, b0 = 0.0f, b1 = 0.0f, b2 = 0.0f;
		f32 omega = 2.0f * PI * cutoffFrequency / fs;

		switch(m_Mode)
		{
		case BIQUAD_MODE_LOW_PASS_2POLE:
		case BIQUAD_MODE_LOW_PASS_4POLE:
			{
				//Compensate for cascaded sections.
				f32 resFactor = m_Is4Pole ? 0.5f : 1.0f;
				f32 cs = cosf(omega);
				f32 sn = sinf(omega);

				f32 alpha = sn * Sinhf(0.5f / (m_Resonance * resFactor));

				a0 = (1.0f - cs) / 2.0f;
				a1 =  1.0f - cs;
				a2 = a0;
				b0 = 1.0f + alpha;
				b1 = -2.0f * cs;
				b2 = 1.0f - alpha;

				//Normalize so b0 = 1.0.
				m_a[0] = a0 / b0;
				m_a[1] = a1 / b0;
				m_a[2] = a2 / b0;
				m_b[0] = b1 / b0;
				m_b[1] = b2 / b0;
			}
			break;

		case BIQUAD_MODE_HIGH_PASS_2POLE:
		case BIQUAD_MODE_HIGH_PASS_4POLE:
			{
				//Compensate for cascaded sections.
				f32 resFactor = m_Is4Pole ? 0.5f : 1.0f;
				f32 cs = cosf(omega);
				f32 sn = sinf(omega);

				f32 alpha = sn * Sinhf(0.5f / (m_Resonance * resFactor));

				a0 = (1.0f + cs) / 2.0f;
				a1 = -1.0f - cs;
				a2 = a0;
				b0 = 1.0f + alpha;
				b1 = -2.0f * cs;
				b2 = 1.0f - alpha;

				//Normalize so b0 = 1.0.
				m_a[0] = a0 / b0;
				m_a[1] = a1 / b0;
				m_a[2] = a2 / b0;
				m_b[0] = b1 / b0;
				m_b[1] = b2 / b0;
			}
			break;

		case BIQUAD_MODE_BAND_PASS_2POLE:
		case BIQUAD_MODE_BAND_PASS_4POLE:
			{
				f32 c = (1.0f / tanf(PI * (bandwidth / fs)));
				f32 d = (2.0f * cosf(omega));

				m_a[0] = (1.0f / (1.0f + c));
				m_a[1] = 0.0f;
				m_a[2] = -m_a[0];
				m_b[0] = -m_a[0] * c * d;
				m_b[1] =  m_a[0] * (c - 1.0f);
			}
			break;

		case BIQUAD_MODE_BAND_STOP_2POLE:
		case BIQUAD_MODE_BAND_STOP_4POLE:
			{
				f32 c = tanf(PI * (bandwidth / fs));
				f32 d = 2.0f * cosf(omega);

				m_a[0] = (1.0f / (1.0f + c));
				m_a[1] = -m_a[0] * d;
				m_a[2] =  m_a[0];
				m_b[0] =  m_a[1];
				m_b[1] =  m_a[0] * (1.0f - c);
			}
			break;

		case BIQUAD_MODE_ULTIMATE_PEAKING_EQ:
			{
				f32 Q = cutoffFrequency / bandwidth;
				f32 alpha = sinf(omega) / (2.f * Q);
				f32 cs = cosf(omega);
				//Since we're using resonance as the gain/cut for this peak, and 0dB of resonance for the other filter
				//modes equates to -3.01dB at Fc, our desired gain/cut for this filter is actually 3.01 dB higher than 
				//the linear m_Resonance value stored in our settings structure.  That's why we multiply by 1.4142.
				f32 A = m_Resonance * 1.4142f;

				a0 = 1.f + alpha * A;
				a1 = -2.f * cs;
				a2 = 1.f - alpha * A;
				b0 = 1.f + alpha / A;
				b1 = -2.f * cs;
				b2 = 1.f - alpha / A;

				// Normalize so that b0 = 1.0
				m_a[0] = a0 / b0;
				m_a[1] = a1 / b0;
				m_a[2] = a2 / b0;
				m_b[0] = b1 / b0;
				m_b[1] = b2 / b0;
			}
			break;
		default:
			Assert(0);
		}

		m_RecomputeCoefficients = false;
	}

} // namespace rage


