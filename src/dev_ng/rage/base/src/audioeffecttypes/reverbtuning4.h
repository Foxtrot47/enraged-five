//
// audioeffects/reverbtuning.h
// 
// Freeverb model tuning values. Simplified to mono tuning.
//
// Originally written by Jezar at Dreampoint, June 2000
// http://www.dreampoint.co.uk
// This code is public domain

#ifndef AUD_REVERBTUNING4_H
#define AUD_REVERBTUNING4_H

namespace rage
{
	namespace audReverbEffect4Tuning
	{
		const f32 ScaleDamp			= 0.4f;
		const f32 ScaleRoom			= 0.28f;
		const f32 OffsetRoom		= 0.7f;
		const f32 InitialRoom		= 0.5f;
		const f32 InitialDamp		= 0.5f;
		

		// NOTE: original stereo effect achieved width by adding 23 samples to all delay lines for right channel

		// These values assume 44.1KHz sample rate
		// they will probably be OK for 48KHz sample rate
		// but would need scaling for 96KHz (or other) sample rates.
		// The values were obtained by listening tests.
		static const u32 CombFilterLengths[] =
		{
			1116,
			1188,
			1277,
			1356,
			1422,
			1491,
			1557,
			1617
		};
		enum { NumCombs = (sizeof(CombFilterLengths)/sizeof(CombFilterLengths[0])) };
		CompileTimeAssert(NumCombs == 8);

		static const u32 AllPassFilterLengths[] =
		{
			556,
			441,
			341,
			257//225
		};
		enum { NumAllPasses = (sizeof(AllPassFilterLengths)/sizeof(AllPassFilterLengths[0])) };
		CompileTimeAssert(NumAllPasses == 4);
	};
}

#endif // AUD_REVERBTUNING4_H
