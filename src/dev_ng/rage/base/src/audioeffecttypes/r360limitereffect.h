// 
// audioeffecttypes/r360limitereffect.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOEFFECTTYPES_R360LIMITEREFFECT_H 
#define AUDIOEFFECTTYPES_R360LIMITEREFFECT_H 

#include "atl/queue.h"

#include "filterdefs.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"

namespace rage {

	struct tR360LimiterEffectSettings
	{
		tR360LimiterEffectSettings() :
	OutCeilingDb(R360_OUTCEILING_DEFAULT),
		ThresholdDb(R360_THRESH_DEFAULT),
		Release(R360_RELEASE_DEFAULT),
		AutoReleaseMin(R360_AUTORELEASE_MIN_DEFAULT),
		AutoReleaseMax(R360_AUTORELEASE_MAX_DEFAULT),
		AutoReleaseRmsThresMin(R360_AUTORELEASE_RMS_THRES_MIN_DEFAULT),
		AutoReleaseRmsThresMax(R360_AUTORELEASE_RMS_THRES_MAX_DEFAULT),
		InputClipDb(R360_INPUTCLIP_DEFAULT),
		AutoReleaseMode(true),
		Bypass(false)
	{
	}

	f32	OutCeilingDb;
	f32 ThresholdDb;
	f32 Release;
	f32 AutoReleaseMin;
	f32 AutoReleaseMax;
	f32 AutoReleaseRmsThresMin;
	f32 AutoReleaseRmsThresMax;
	f32 InputClipDb;
	bool AutoReleaseMode;
	bool Bypass;
	};

	//
	// PURPOSE
	//  A general dynamic range compressor.
	//
	class audR360LimiterEffect : public audDspEffect
	{
	public:
		enum {kR360DelayLength = 64};

		audR360LimiterEffect();
		virtual ~audR360LimiterEffect() {}

		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();
		
		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		enum audR360LimiterEffectParams
		{
			OutCeilingDb = audEffectParamIds::R360LimiterEffect,
			ThresholdDb,
			Release,
			AutoReleaseMin,
			AutoReleaseMax,
			AutoReleaseRmsThresMin,
			AutoReleaseRmsThresMax,
			InputClipDb,
			AutoReleaseMode,
			Bypass,
		};

		struct tTargetGain
		{
			f32 m_fGainTarget;
			u32 m_uSampleTarget;
			__forceinline float CalculateGainSlope(const f32 fCurrentGain, const u32 uCurrentSample) const
			{
				Assert(m_uSampleTarget>uCurrentSample);
				return (m_fGainTarget-fCurrentGain) / (m_uSampleTarget-uCurrentSample);
			}
		};

	private:
		f32 m_DelayBuffer[g_MaxOutputChannels][kR360DelayLength];

		u32 m_uCurrentSample;

		f32 m_fReleaseRate;			//	Release time in linear gain units per sample
		f32 m_fRmsThresholdMin;
		f32 m_fRmsThresholdMax;
		f32 m_fOneOverRmsThresholdDelta;

		f32 m_fInputClipLin;		//	Linear representation of dB clip point passed in
		f32 m_fThresholdLin;		//	Linear representation of dB threshold passed in
		f32 m_fOutputCeilingLin;	//	Linear representation of dB makeup gain passed in
		f32 m_fCurrentGain;			//	Linear gain current value
		f32 m_fTargetGain;			//	Linear gain target value
		f32 m_fGainRate;			//	Linear rate of gain change per sample

		atQueue<tTargetGain, kR360DelayLength> m_RunningTargets;	//	Stores 64 peaks ahead

		tR360LimiterEffectSettings m_Settings;

		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;

		class RmsCalculator
		{
		public:
			RmsCalculator() : m_fValue(0.0) { for (int i = 0; i < sm_iSize; ++i) m_Queue.Push(0.0f); }
			__forceinline f32 AddValue(f32 fValue)
			{
				fValue *= (1.0f/sm_iSize);
				m_fValue += fValue;
				m_Queue.Push(fValue);
				m_fValue -= m_Queue.Pop();
				return m_fValue;
			}

		private:
			static const int sm_iSize = 2400;
			f32 m_fValue;
			atQueue<f32, sm_iSize> m_Queue;
		} m_RmsCalculator;
	};

} // namespace rage

#endif // AUDIOEFFECTTYPES_R360LIMITEREFFECT_H 

