
#ifndef _RAGE_PNTRIANGLES_H
#define _RAGE_PNTRIANGLES_H

#include "grcore/viewport.h"

namespace rage
{

class PNTriangleTessellationGlobals
{
public:
	PNTriangleTessellationGlobals();
	virtual ~PNTriangleTessellationGlobals();

	// Computes and sets the tessellation globals...
	void Set(const grcViewport *pViewport, float nearDepth, float farDepth, u32 maxTessFactor, float ScreenSpaceErrorInPixels, float Aspect, float FrustumEpsilon, float BackFaceCullEpsilon, bool bCull);
	// ...Using this function to set global shader variables.
	virtual void SetGlobalVarV4(grcEffectGlobalVar var, const Vec4V &value) = 0;
	virtual void SetGlobalVarV4Array(grcEffectGlobalVar var, const Vec4V *paValue, int iCount) = 0;

private:
	// Camera position.
	grcEffectGlobalVar m_Rage_Tessellation_CameraPosition_Var;
	// Vector the camera is looking along.
	grcEffectGlobalVar m_Rage_Tessellation_CameraZAxis_Var;
	// Pixel error params: projection.x, projection.y, AllowedScreenSpaceErrorInX, AllowedScreenSpaceErrorInY.
	grcEffectGlobalVar m_Rage_Tessellation_ScreenSpaceErrorParams_Var;
	// DX11 TODO:- This may have to be local as it could potentially differ from mesh to mesh.
	// Linear scale params: Near, Far, MaxTessFactor.
	grcEffectGlobalVar m_Rage_Tessellation_LinearScale_Var;
	// View * Projection frustum planes for left, right, top, bottom for culling tessellated triangles on the GPU to avoid tessellating them
	grcEffectGlobalVar m_Rage_Tessellation_Frustum_Var;
	// Epsilon values for controling amount to consider still a facing/on screen triangle
	grcEffectGlobalVar m_Rage_Tessellation_Epsilons_Var;
};

};

#endif //_RAGE_PNTRIANGLES_H
