//
// rage utility functions
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

#ifndef __UTILITY_FXH
#define __UTILITY_FXH

	//
	// Generate normal from height map via gradient instructions
	// Note: gradient instructions work in screenspace only
	//       so this you need to create the normals in screenspace
	//     
/*
	map.a *= 1.0f;
    
    // Vdu = (1, 0, Hu); Vdv = (0, 1, Hv);
    // N = normalize(cross(Vdu, Vdv));
    float3 x = float3(1, 0, ddx(map.a));
    float3 y = float3(0, 1, ddy(map.a));
       
    float3 Normi = cross(x, y);
    Normi.xy = Normi.xy * 2.0f;
*/

// PURPOSE:
//		How does it work:
//			� Use all four color channels of DXT5 format
//			� One channel of the normal map (R, G or B) is copied into alpha channel A and then it is
//			  cleared (filled with zero values) (f.e. with Adobe Photoshop)
//			� The alpha channel is copied back into the color channel in the pixel shader
//		Why does it work:
//			- Alpha channel is quantized separately from the RGB channels
//			� Alpha channel is quantized with an increased accuracy
//			� Clearing one color channel leads to a higher precision in quantization of the color channels
float3 UncompressNormalMapShaderX2(sampler2D Sampler, float2 TexCoord)
{   	
	float3 Normal;
	Normal.xy = 2 * tex2D(Sampler, TexCoord.xy).wy - 1.0;  // compress normal maps have data in y & w only...
	// reconstruct normal.z
	Normal.z = sqrt(1 - Normal.x * Normal.x - Normal.y * Normal.y);
	return Normal;
}
	 
//
//	PURPOSE
// Generate normal from height map
//     
// ( di/ds, di/dt, scale ) = (1, 0, -di/ds) x (0, 1, -di/dt)
//  di/ds = change in height when moving along s axis
//  di/dt = change in height when moving along t axis
//
// +-+-+
// |/|\|
// +-+-+ (calculate the normal of the center +)
// |\|/|
// +-+-+
//           (h[x - 1, y] - h[x + 1, y])
// n[x, y] = (            2            )
//           (h[x, y - 1] - h[x, y + 1])
// Take all neighbor samples
float3 GenerateNormalFromHeightMap(sampler2D HeightSampler, float2 TexCoord, float2 texelSize, float Bumpiness)
{   	
   	float Left = tex2D(HeightSampler, TexCoord.xy + float2(-texelSize.x, 0)).x;
   	Left = Left * Left * Bumpiness;
   	float Right = tex2D(HeightSampler, TexCoord.xy + float2( texelSize.x,   0)).x;
   	Right = Right * Right * Bumpiness;
   	float Top = tex2D(HeightSampler, TexCoord.xy + float2(0, -texelSize.y)).x;
   	Top = Top * Top * Bumpiness;
   	float Bottom = tex2D(HeightSampler, TexCoord.xy + float2(0, texelSize.y)).x;
   	Bottom = Bottom * Bottom * Bumpiness;
   	
   	float sx = (Left - Right) * 1.0f;
   	float sy = (Top - Bottom) * 1.0f;
  
    float3 x = float3(1, 0, -sx);
    float3 y = float3(0, 1, -sy);
       
    // normal in tangent space   
    float3 N = cross(x, y);  
    
    // return this normal -> we might have to transform it into world space
    return N;
}

//
//	PURPOSE
//		Generates a Normal map from a height field using an approximation from Game Programming Gems 3
//		Fast HeightField Normal Calculation
//
float3 GenerateNormalFromHeightMapFast(sampler2D HeightSampler, float2 TexCoord, float2 texelSize, float Bumpiness)
{   	
	float4 pos;
   	pos.x = tex2D(HeightSampler, TexCoord.xy + float2( texelSize.x,   0)).x;
   	pos.y = tex2D(HeightSampler, TexCoord.xy + float2(0, texelSize.y)).x;
   	pos.z = tex2D(HeightSampler, TexCoord.xy + float2(-texelSize.x, 0)).x;
   	pos.w = tex2D(HeightSampler, TexCoord.xy + float2(0, -texelSize.y)).x;
   	pos = pos * pos * Bumpiness;
   	
   	// return this normal -> we might have to transform it into world space
   	return float3( pos.zw - pos.xy, 1.0f );
}
float utilLuminance( float3 col )
{
	float3 lightness = float3(0.30, 0.59, 0.11);
	return dot( col, lightness );
}
//
//	PURPOSE
// Generate normal from color map map uses the lumiance as a height value
//     
// ( di/ds, di/dt, scale ) = (1, 0, -di/ds) x (0, 1, -di/dt)
//  di/ds = change in height when moving along s axis
//  di/dt = change in height when moving along t axis
//
// +-+-+
// |/|\|
// +-+-+ (calculate the normal of the center +)
// |\|/|
// +-+-+
//           (h[x - 1, y] - h[x + 1, y])
// n[x, y] = (            2            )
//           (h[x, y - 1] - h[x, y + 1])
// Take all neighbor samples
float3 GenerateNormalFromColorMap(sampler2D HeightSampler, float2 TexCoord, float2 texelSize, float Bumpiness)
{   	
 	
   	float4 Left = tex2D(HeightSampler, TexCoord.xy + float2(-texelSize.x, 0));
   	Left = Left * Left * Bumpiness;
   	float4 Right = tex2D(HeightSampler, TexCoord.xy + float2( texelSize.x,   0));
   	Right = Right * Right * Bumpiness;
   	float4 Top = tex2D(HeightSampler, TexCoord.xy + float2(0, -texelSize.y));
   	Top = Top * Top * Bumpiness;
   	float4 Bottom = tex2D(HeightSampler, TexCoord.xy + float2(0, texelSize.y));
   	Bottom = Bottom * Bottom * Bumpiness;
   	
   	float4 sx = (Left - Right) * 1.0f;
   	float4 sy = (Top - Bottom) * 1.0f;
  
    float3 x = float3(1, 0, -utilLuminance( sx.xyz ));
    float3 y = float3(0, 1, -utilLuminance( sy.xyz ));
       
    // normal in tangent space   
    float3 N = cross(x, y);  
    
    // return this normal -> we might have to transform it into world space
    return N;
}
    
#endif // __UTILITY_FXH
