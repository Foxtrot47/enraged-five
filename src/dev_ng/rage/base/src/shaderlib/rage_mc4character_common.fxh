#include "rage_common.fxh"
#include "rage_skin.fxh"

#ifndef USE_DETAIL_MAP
#define USE_DETAIL_MAP (0)
#endif // USE_DETAIL_MAP

#ifndef USE_SUBSURFACE_SCATTERING
#define USE_SUBSURFACE_SCATTERING (0)
#endif // USE_SUBSURFACE_SCATTERING

#if USE_ANIMATED_NORMAL_MAP && !USE_NORMAL_MAP
#error You must have a normal map to use animated normal maps
#endif // USE_ANIMATED_NORMAL_MAP && !USE_DETAIL_MAP

#if USE_ANIMATED_NORMAL_MAP
#define USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS (1)
#include "rage_animatednormalmap.fxh"
#endif // USE_ANIMATED_NORMAL_MAP

#ifndef USE_ALPHA_BLENDING
#define USE_ALPHA_BLENDING (0)
#endif // USE_ALPHA_BLENDING

#ifndef DISABLE_NORMAL_OFFSETS
#define DISABLE_NORMAL_OFFSETS (1)
#endif // DISABLE_NORMAL_OFFSETS

// Diffuse texture sampler

BeginSampler(sampler2D, DiffuseTexture, DiffuseSampler, DiffuseTexture)
	string	UIName = "Diffuse Texture";
	string	UvSetName = "map1";
	int		UvSetIndex = 0;
ContinueSampler(sampler2D, DiffuseTexture, DiffuseSampler, DiffuseTexture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

#if USE_ALPHA_BLENDING
BeginSampler(sampler2D,specMapTexture,SpecMapSampler,specMapTexture)
	string	UIName = "Spec Map Texture";
ContinueSampler(sampler2D,specMapTexture,SpecMapSampler,specMapTexture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
#endif // USE_ALPHA_BLENDING

#if USE_NORMAL_MAP
BeginSampler(sampler2D,normalMapTexture,NormalMapSampler,normalMapTexture)
	string UIName = "Normal Map Texture";
	string TextureOutputFormats = "PC=COMP_NRM_PD_DXT5";
ContinueSampler(sampler2D,normalMapTexture,NormalMapSampler,normalMapTexture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

#if USE_DETAIL_MAP
BeginSampler(sampler2D,detailMapTexture,DetailMapSampler,detailMapTexture)
	string	UIName = "Detail Map Texture";
ContinueSampler(sampler2D,detailMapTexture,DetailMapSampler,detailMapTexture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
    AddressU  = WRAP;
    AddressV  = WRAP;
    AddressW  = WRAP;
EndSampler;
#endif // USE_DETAIL_MAP

float normalMapMod
<
	string UIName = "normal map intensity";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;

float detailMapUScale
<
	string UIName = "detail map U scale";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;

float detailMapVScale
<
	string UIName = "detail map V scale";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;

float detailMapOffset
<
	string UIName = "detail map offset";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.5f;

#endif // USE_NORMAL_MAP

float diffuseMod
<
	string UIName = "diffuse intensity";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 1.0f;

float ambientMod
<
	string UIName = "ambient intensity";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;

float SpecMapExp
<
	string UIName = "specular map exponent";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;

float SpecExp
<
	string UIName = "specular exponent";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 500.0;
	float UIStep = 0.1;
> = 1.0f;

float SpecIntensity
<
	string UIName = "specular intensity";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 1.0f;

float SpecMapIntensity
<
	string UIName = "specular map intensity";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.0f;

float specFresnelExp
<
	string UIName = "Fresnel Falloff";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 5.0;
    float UIStep = 0.05;
> = 3.5f;

float specFresnelMin
<
	string UIName = "Fresnel Min";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 4.0;
    float UIStep = 0.005;
> = 1.0f;

float specFresnelMax
<
	string UIName = "Fresnel Max";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 4.0;
    float UIStep = 0.005;
> = 2.0f;

float wrapAmount
<
	string UIName = "Wrap amount";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.005;
> = 0.5f;

float rimPower
<
	string UIName = "Rim power";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 128.0;
    float UIStep = 0.1;
> = 4.0f;

float rimAmount
<
	string UIName = "Rim amount";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
> = 0.0f;

float ambientOcclusionContrast
<
	string UIName = "Ambient occlusion contrast";
	int UIHidden = 1;
    float UIMin = 0.0;
    float UIMax = 30.0;
    float UIStep = 0.01;
> = 1.0f;

#if USE_SUBSURFACE_SCATTERING
float3 SubsurfaceColor
<
    string UIName = "Sub-Surface Color";
	int UIHidden = 1;
    float UIMin = 0.;
    float UIMax = 1.;
    float UIStep = 0.001;
> = {0.86667f, 0.6235f, 0.53333f};

float SubsurfaceRollOff
<
    string UIName = "Sub-Surface Rolloff Range";
	int UIHidden = 1;
    float UIMin = 0.0f;
    float UIMax = 1.0f;
    float UIStep = 0.01f;
> = 0.15;

float SubsurfaceIntensity
<
    string UIName = "Sub-Surface Intensity";
	int UIHidden = 1;
    float UIMin = 0.0f;
    float UIMax = 1.0f;
    float UIStep = 0.01f;
> = 0.0;
#endif // USE_SUBSURFACE_SCATTERING

struct VertexSkinnedBlendShapeData {
    float3 pos            : POSITION1;
    float3 normal		  : NORMAL1;
	float4 tangent		  : TANGENT1;
};

struct	VS_INPUT 
{
	float4 Position		: POSITION;
	float3 Normal		: NORMAL;
	float4 Diffuse		: COLOR0; // x = wind, yzw = unused
	float2 TexCoord0	: TEXCOORD0;
	float4 Tangent		: TANGENT;
};

struct	VS_INPUT_SKINNED
{
	float4 Position		: POSITION;
	float4 Weights		: BLENDWEIGHT;
	index4 Indices		: BLENDINDICES;
	float3 Normal		: NORMAL;
	float4 Diffuse		: COLOR0;  // x = wind, yzw = unused
	float2 TexCoord0	: TEXCOORD0;
	float4 Tangent		: TANGENT;
};

struct	VS_OUTPUT
{
	DECLARE_POSITION(Position)
	float4 Normal_TexCoordU		: TEXCOORD0;
	float4 ScreenPos			: TEXCOORD1;
	float4 WorldPos_TexCoordV	: TEXCOORD2;
	float4 Ambient_AmbOcc		: TEXCOORD3;
#if USE_NORMAL_MAP
	float4 Tangent				: TEXCOORD4;
	float3 Binormal				: TEXCOORD5;
#endif // USE_NORMAL_MAP
	float4 Diffuse				: COLOR0;  // x = wind, yzw = unused
};

struct mcLightParams
{
	float3 L;
	float LDist;
	float lFalloff;
	float lIntensity;
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
mcLightParams CalcLightParamsFull(int lightNum, float3 worldPos)
{
	mcLightParams lParams;

	lParams.L = gLightPosDir[lightNum].xyz - worldPos;
	lParams.LDist = length(lParams.L);
	lParams.L /= lParams.LDist;

	lParams.lFalloff = saturate(1.0 - min((lParams.LDist / gLightPosDir[lightNum].w), 1.0));
	lParams.lIntensity = -gLightColor[lightNum].w;
	
	return lParams;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
float3 ShiftNormal(float3 normal, float3 dir, float amt)
{
	float3 OUT;
	OUT = normalize(normal + (dir * amt));
	return OUT;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
float3 CalcAmbient(float3 N)
{
	return 0.5f.xxx;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
VS_OUTPUT VS_Common(float4 Position, float3 WorldPosition, float3 N, float2 TexCoord0, float4 Tangent, float3 Binormal, float4 Diffuse)
{
	VS_OUTPUT	OUT = (VS_OUTPUT)0;
	OUT.Position = Position;
	OUT.Normal_TexCoordU.xyz = N;
	OUT.Normal_TexCoordU.w = TexCoord0.x;
	OUT.ScreenPos = rageCalcScreenSpacePosition(OUT.Position);
	OUT.WorldPos_TexCoordV.xyz = WorldPosition;
	OUT.WorldPos_TexCoordV.w = TexCoord0.y;

	OUT.Ambient_AmbOcc.rgb = 0.0f;
	OUT.Ambient_AmbOcc.w = 1.0f;
	
#if USE_NORMAL_MAP
	OUT.Tangent = Tangent;
	OUT.Binormal = Binormal;
#endif // USE_NORMAL_MAP

	OUT.Diffuse = Diffuse;

	return(OUT);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
VS_OUTPUT VS(VS_INPUT IN) 
{
	float4 Position = mul(IN.Position, gWorldViewProj);
	float3 WorldPosition = mul(IN.Position, (float4x3)gWorld);
	float3 WorldNormal = mul(IN.Normal.xyz, (float3x3)gWorld);
	float4 WorldTangent = float4(mul(IN.Tangent.xyz, (float3x3)gWorld), IN.Tangent.w);
	float3 WorldBinormal = rageComputeBinormal(IN.Normal, IN.Tangent);
	WorldBinormal = mul(WorldBinormal, (float3x3)gWorld);
	
	return VS_Common(Position, WorldPosition, WorldNormal, IN.TexCoord0, WorldTangent, WorldBinormal, IN.Diffuse);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
VS_OUTPUT VS_BlendShape(VS_INPUT IN, VertexSkinnedBlendShapeData INOFFSET)
{
	float4 blendShapePos = IN.Position;
	blendShapePos.xyz += INOFFSET.pos;
	float3 blendShapeNorm = IN.Normal;
	float4 blendShapeTangent = IN.Tangent;

#if !DISABLE_NORMAL_OFFSETS
	blendShapeNorm += INOFFSET.normal;
	blendShapeTangent += INOFFSET.tangent;
#endif // !DISABLE_NORMAL_OFFSETS

	float4 Position = mul(blendShapePos, gWorldViewProj);
	float3 WorldPosition = mul(blendShapePos, (float4x3)gWorld);
	float3 WorldNormal = mul(blendShapeNorm, (float3x3)gWorld);
	float4 WorldTangent = float4(mul(blendShapeTangent.xyz, (float3x3)gWorld), blendShapeTangent.w);
	float3 WorldBinormal = rageComputeBinormal(blendShapeNorm, blendShapeTangent);
	WorldBinormal = mul(WorldBinormal, (float3x3)gWorld);

	return VS_Common(Position, WorldPosition, WorldNormal, IN.TexCoord0, WorldTangent, WorldBinormal, IN.Diffuse);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
VS_OUTPUT VSSkinned_BlendShape(VS_INPUT_SKINNED IN, VertexSkinnedBlendShapeData INOFFSET)
{
	float3 blendShapePos = IN.Position.xyz;
	blendShapePos.xyz += INOFFSET.pos;
	float3 blendShapeNorm = IN.Normal;
	float4 blendShapeTangent = IN.Tangent;

#if !DISABLE_NORMAL_OFFSETS
	blendShapeNorm += INOFFSET.normal;
	blendShapeTangent += INOFFSET.tangent;
#endif // !DISABLE_NORMAL_OFFSETS

	rageSkinMtx	BoneMtx = ComputeSkinMtx(IN.Indices, IN.Weights);
	float3 Pos = rageSkinTransform(blendShapePos, BoneMtx);
	
	float3 skinnedNorm = normalize(rageSkinRotate(blendShapeNorm, BoneMtx));
	float3 skinnedTangent = normalize(rageSkinRotate(blendShapeTangent.xyz, BoneMtx));

	float4 Position = mul(float4(Pos, 1.0), gWorldViewProj);
	float3 WorldPosition = mul(float4(Pos, 1.0), gWorld).xyz;
	float3 WorldNormal = normalize(mul(skinnedNorm, (float3x3)gWorld));
	float4 WorldTangent = float4(mul(skinnedTangent, (float3x3)gWorld), IN.Tangent.w);
	float3 WorldBinormal = rageComputeBinormal(blendShapeNorm, blendShapeTangent);
	WorldBinormal = normalize(rageSkinRotate(WorldBinormal, BoneMtx));
	WorldBinormal = mul(WorldBinormal, (float3x3)gWorld);

	return VS_Common(Position, WorldPosition, WorldNormal, IN.TexCoord0, WorldTangent, WorldBinormal, IN.Diffuse);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
VS_OUTPUT	VSSkinned	(VS_INPUT_SKINNED IN)
{
	rageSkinMtx	BoneMtx = ComputeSkinMtx(IN.Indices, IN.Weights);
	float3 Pos = rageSkinTransform(IN.Position.xyz, BoneMtx);
	float4 Position = mul(float4(Pos, 1.0), gWorldViewProj);
	float3 WorldPosition = mul(float4(Pos, 1.0), gWorld).xyz;

	float3 skinnedNorm = rageSkinRotate(IN.Normal, BoneMtx);
	float3 WorldNormal = normalize(mul(skinnedNorm, (float3x3)gWorld));
	float3 skinnedTangent = rageSkinRotate(IN.Tangent.xyz, BoneMtx);
	float4 WorldTangent = float4(normalize(mul(skinnedTangent, (float3x3)gWorld)), IN.Tangent.w);
	float3 WorldBinormal = rageComputeBinormal(IN.Normal, IN.Tangent);
	WorldBinormal = rageSkinRotate(WorldBinormal, BoneMtx);
	WorldBinormal = normalize(mul(WorldBinormal, (float3x3)gWorld));

	return VS_Common(Position, WorldPosition, WorldNormal, IN.TexCoord0, WorldTangent, WorldBinormal, IN.Diffuse);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
float4 PSInternal(VS_OUTPUT IN, bool enableAlphaBlending)
{
	float2 TexCoord0 = { IN.Normal_TexCoordU.w, IN.WorldPos_TexCoordV.w };
	float4 texColor = tex2D(DiffuseSampler, TexCoord0);
	
	float specMask;
#if USE_ALPHA_BLENDING
	if (enableAlphaBlending)
	{
		specMask = tex2D(SpecMapSampler, TexCoord0).r;
	}
	else
#endif // USE_ALPHA_BLENDING
	{
		specMask = texColor.a;
	}
	float ambientOcclusion = IN.Ambient_AmbOcc.w * ragePow(IN.Diffuse.w, ambientOcclusionContrast);

	float3 N = normalize(IN.Normal_TexCoordU.xyz);

	float localDiffuseMod = diffuseMod;
#if USE_DETAIL_MAP
	float4 detailColor = tex2D(DetailMapSampler, TexCoord0 * float2(detailMapUScale, detailMapVScale));
	float rescaledDetail = lerp(detailMapOffset, 1.0f, detailColor.r);
	specMask *= detailColor.r;
	localDiffuseMod *= rescaledDetail;
	IN.Ambient_AmbOcc.rgb *= rescaledDetail;
#endif // USE_DETAIL_MAP
#if USE_NORMAL_MAP
	float3 T = normalize(IN.Tangent.xyz);
	float3 B = normalize(IN.Binormal);
	float2 normalMapPd0 = tex2D(NormalMapSampler, TexCoord0).ga;
	float3 Nbump = rageReconstructPartialDerivativeNormal(normalMapPd0, T, B, N, normalMapMod);
#if USE_ANIMATED_NORMAL_MAP
	N = rageAnimateNormal(Nbump, TexCoord0, T, B, N);
#else
	N = Nbump;
#endif //
#endif // USE_NORMAL_MAP

	float3 V = normalize(gViewInverse[3].xyz - IN.WorldPos_TexCoordV.xyz);
	float3 R = normalize(reflect(V, N));

	float specFresnel = rageCalcFresnelTerm(N,V, specFresnelExp, specFresnelMin, specFresnelMax);

	float NdotV = dot(N,V);
	float3 spec = 0.0f;
	float3 diffuse = 0.0f;
	
	mcLightParams lParam;
	lParam = CalcLightParamsFull(0, IN.WorldPos_TexCoordV.xyz);

	float wrapScale = 1.0 - wrapAmount;
	float wrapBias = wrapAmount;

	float NdotL = dot(N, lParam.L.xyz);
    float3 halfVector = normalize(V + lParam.L.xyz);
	NdotL = NdotL * wrapScale + wrapBias;
	if (NdotL > 0)
		spec += ragePow(saturate(dot(halfVector, N)), SpecExp) * gLightColor[0].rgb * saturate(NdotL * 16.0f);
#if USE_SUBSURFACE_SCATTERING
	// add color tint at transition from light to dark
	diffuse += SubsurfaceIntensity * SubsurfaceColor * smoothstep(0.0, SubsurfaceRollOff, NdotL) * smoothstep(SubsurfaceRollOff * 2.0, SubsurfaceRollOff, NdotL);
#endif // USE_SUBSURFACE_SCATTERING
	NdotL = max(NdotL, 0.0f);
	diffuse += NdotL * gLightColor[0].rgb * lParam.lFalloff * lParam.lIntensity;

	lParam = CalcLightParamsFull(1, IN.WorldPos_TexCoordV.xyz);
	NdotL = dot(N, lParam.L.xyz);
    halfVector = normalize(V + lParam.L.xyz);
	NdotL = NdotL * wrapScale + wrapBias;
	if (NdotL > 0)
		spec += ragePow(saturate(dot(halfVector, N)), SpecExp) * gLightColor[1].rgb * saturate(NdotL * 16.0f);
#if USE_SUBSURFACE_SCATTERING
	// add color tint at transition from light to dark
	diffuse += SubsurfaceIntensity * SubsurfaceColor * smoothstep(0.0, SubsurfaceRollOff, NdotL) * smoothstep(SubsurfaceRollOff * 2.0, SubsurfaceRollOff, NdotL);
#endif // USE_SUBSURFACE_SCATTERING
	NdotL = max(NdotL, 0.0f);
	diffuse = rageAddSmooth(NdotL * gLightColor[1].rgb * lParam.lFalloff * lParam.lIntensity, diffuse);

	localDiffuseMod *= 1.8f;
	diffuse = rageAddSmooth(IN.Ambient_AmbOcc.rgb, diffuse * ambientOcclusion);
	diffuse *= localDiffuseMod;
	
	texColor.rgb *= diffuse;
	spec *= specFresnel * specMask * ambientOcclusion * SpecIntensity;
	texColor.rgb = rageAddSmooth(texColor.rgb, spec).rgb;

	float rim = ragePow(1.0f - max(NdotV, 0), rimPower);
	float3 rimLight = rim * rimAmount * ambientOcclusion * specFresnel;
	texColor.rgb = rageAddSmooth(texColor.rgb, rimLight);

	return float4(texColor.rgb, enableAlphaBlending ? texColor.a : 1.0f);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
float4 PS(VS_OUTPUT IN) : COLOR
{
	return PSInternal(IN, false);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
float4 PSAlpha(VS_OUTPUT IN) : COLOR
{
	return PSInternal(IN, USE_ALPHA_BLENDING);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
float4 PSAlphaDoubleSided(VS_OUTPUT IN, float face : VFACE) : COLOR
{
	IN.Normal_TexCoordU.xyz *= sign(face);
	return PSInternal(IN, USE_ALPHA_BLENDING);
}
