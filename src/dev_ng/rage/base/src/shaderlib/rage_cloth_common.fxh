
#if RSG_PC || RSG_DURANGO 
    #if __SHADERMODEL < 20
        // Currently, all we can go down to is 1.4
        //    If we need lower, we can allow it
        #define PIXELSHADER		ps_1_4
        #define VERTEXSHADER	vs_1_1
    #elif __SHADERMODEL >= 30
		#if __SHADERMODEL >= 40
			#if __SHADERMODEL >= 41
				#if __SHADERMODEL >= 50
					#define PIXELSHADER		ps_5_0
					#define VERTEXSHADER	vs_5_0
				#else
					#define PIXELSHADER		ps_4_1
					#define VERTEXSHADER	vs_4_1
				#endif
			#else
				#define PIXELSHADER		ps_4_0
				#define VERTEXSHADER	vs_4_0
			#endif
		#else
			#define PIXELSHADER		ps_3_0
			#define VERTEXSHADER	vs_3_0
		#endif
    #else
        #define PIXELSHADER ps_2_0
        #define VERTEXSHADER vs_2_0
    #endif    // __SHADERMODEL
#elif RSG_ORBIS
    #define PIXELSHADER		ps_5_0
    #define VERTEXSHADER	vs_5_0
#elif __XENON || __PS3
    #define PIXELSHADER ps_3_0
    #define VERTEXSHADER vs_3_0
#endif

#include "rage_diffuse_sampler.fxh"
#include "rage_bump_sampler.fxh"
#include "rage_spec_sampler.fxh"
#include "rage_diffuse_detail_sampler.fxh"


BeginSampler(sampler2D,SweatMap,SweatMapSampler,SweatMapTex)
    string UIName = "SweatMap";
ContinueSampler(sampler2D,SweatMap,SweatMapSampler,SweatMapTex)
	#if !__FXL 
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(samplerCUBE,specCube,specCubeSampler,SpecCubeTexture)
    string ResourceName = "__charcubemap";
ContinueSampler(samplerCUBE,specCube,specCubeSampler,SpecCubeTexture)

	AddressU = CLAMP;
    AddressV = CLAMP;
    AddressW = CLAMP;

	#if !__FXL 
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;




//float LeftStrainVal = 0.0f;
//float RightStrainVal = 0.0f;
//float ClothSweatiness = 0.0f;
float sweatiness = 0.0f;
float ClothStrainVal = 0.0f;

// color that is interpolated with the light color reflected by the floor/table
float3 RoofColor = float3(0.0, 0.0, 0.0);

/*
float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.5;
    float UIStep = 0.05;
> = 0.15;
*/

float cubeMapSpecExp
<
	string UIName = "Cube Map Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 7.0;
    float UIStep = 0.1;
> = 4.5f;


float cubeMapSpecIntensity
<
	string UIName = "Cube Map Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 20.0;
    float UIStep = 0.1;
> = 0.333f;

/*
float specExponent : SpecularExp
<
    string UIName = "Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 300.0;
    float UIStep = 1.0;
> = 10.0;
*/

float detailNormalScale
<
	string UIName = "Detail Normal Scale";
    float UIMin = 0.0;
    float UIMax = 10.0;
    float UIStep = 0.1;
> = 1.0f;

// the Minnaert Exponent is like a roughness value
float MinnaertExponent : MinnaertExp
<
    string UIName = "Minnaert Spec. Exp ~ Roughness";
    float UIMin = 1.0;
    float UIMax = 200.0;
    float UIStep = 0.1;
> = 3.0;

/*
float SpecularIntensity : SpecularIntensity
<
    string UIName = "Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
> = 0.5;


float AmbientIntensity : AmbientIntensity
<
    string UIName = "Ambient Intensity";
    float UIMin = 0.0;
    float UIMax = 2.0;
    float UIStep = 0.05;
> = 1.0;
*/

#if 1
float reflectMapDesatScale
<
	string UIName = "Reflect Map Desaturation Scale";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.75f;

float SpecToTexColorInterp
<
	string UIName = "Specular To Diffuse Color Interp";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.5f;
#else
float reflectMapDesatScale = 0.75f;
float SpecToTexColorInterp = 0.5f;
#endif

float ShinyDiffuseIntensity : ShinyDiffuseIntensity
<
    string UIName = "Shiny Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.5;
    float UIStep = 0.05;
> = 0.3;

float ShinySpecularIntensity : ShinySpecularIntensity
<
    string UIName = "Shiny Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
> = 0.33;

float ShinySpecExponent : ShinySpecExponent
<
    string UIName = "Shiny Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 300.0;
    float UIStep = 1.0;
> = 2.0;


float ShinyAmbientIntensity : ShinyAmbientIntensity
<
    string UIName = "Shiny Ambient Intensity";
    float UIMin = 0.0;
    float UIMax = 2.0;
    float UIStep = 0.05;
> = 0.55;

float reflectFresnelExp
<
	string UIName = "Reflection Fresnel Exponent";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 2.0f;

float reflectFresnelMin
<
	string UIName = "Reflection Fresnel Min";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 0.5f;


float reflectFresnelMax
<
	string UIName = "Reflection Fresnel Max";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 1.0f;




float rimFresnelExp
<
	string UIName = "Rim Lighting Fresnel Exponent";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 2.0f;

float rimFresnelMin
<
	string UIName = "Rim Lighting FresnelMin";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 0.0f;


float rimFresnelMax
<
	string UIName = "Rim Lighting Fresnel Max";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 0.25f;







#include "rage_ambient.fxh"
/*
float4 ambXPos = {0.05f, 0.07f, 0.07f, 1.0f};
float4 ambXNeg = {0.157f, 0.157f, 0.157f, 1.0f};
float4 ambYPos = {0.2f, 0.2f, 0.2f, 1.0f};
float4 ambYNeg = {0.03f, 0.03f, 0.03f, 1.0f};
float4 ambZPos = {0.157f, 0.157f, 0.157f, 1.0f};
float4 ambZNeg = {0.157f, 0.157f, 0.157f, 1.0f};


float4 CalcAmbient(float3 normal)
{
	float3 nInterp = normal * 0.5f + 0.5f;

	float4 amb = {0.0f, 0.0f, 0.0f, 0.0f};

	amb += lerp(ambXNeg, ambXPos, nInterp.x);
	amb += lerp(ambYNeg, ambYPos, nInterp.y);
	amb += lerp(ambZNeg, ambZPos, nInterp.z);

	//amb += lerp(ambXPos, ambXPos, nInterp.x);
	//amb += lerp(ambYNeg, ambYPos, nInterp.y);
	//amb += lerp(ambXPos, ambXPos, nInterp.z);

	return float4(amb.xyz, 1.0f);
}
*/

float CalcFresnelTerm(float3 N, float3 E, float exp, float low, float hi)
{
	float fresnel = 1.0f - clamp(dot(E, N), 0.0f, 1.0f);
	fresnel = ragePow(fresnel, exp);
	fresnel = lerp(low, hi, fresnel);
	return fresnel;
}



struct vertexOutputComposite 
{
    DECLARE_POSITION(hPosition)
    float2 texCoordDiffuse  : TEXCOORD0;
    float4 shadowmap_pixelPos   : TEXCOORD5;
    float3 worldPos         : TEXCOORD4;
    float3 worldEyePos      : TEXCOORD1;
    float3 worldNormal      : TEXCOORD6;
    float3 worldTangent     : TEXCOORD3;
    float3 worldBinormal    : TEXCOORD7;
};

struct pixelInComposite 
{
	DECLARE_POSITION_PSIN(hPosition)
    float2 texCoordDiffuse  : TEXCOORD0;
    float4 shadowmap_pixelPos   : TEXCOORD5;
    float3 worldPos         : TEXCOORD4;
    float3 worldEyePos      : TEXCOORD1;
    float3 worldNormal      : TEXCOORD6;
    float3 worldTangent     : TEXCOORD3;
    float3 worldBinormal    : TEXCOORD7;
};

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
#if USE_OFFSETS
	vertexOutputComposite VS_CompositeUnskinned(rageSkinVertexInputBump IN, rageSkinVertexInputBumpOffset IN2, uniform float bUseOffsets) 
#else
	vertexOutputComposite VS_CompositeUnskinned(rageSkinVertexInputBump IN) 
#endif
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT;
    
    Position = IN.pos;
    Normal   = IN.normal;
    Tangent  = IN.tangent.xyz;
    
#if USE_OFFSETS
    if(bUseOffsets != 0.0f)
    {
        Position += IN2.pos;
        Normal   += IN2.normal;
        Tangent  += IN2.tangent.xyz;
    }
#endif
    
    float4 Plocal = float4(Position, 1.0);

    OUT.worldPos = mul(float4(Position,1), gWorld).xyz;

    OUT.hPosition = mul(float4(Position,1), gWorldViewProj);
    OUT.texCoordDiffuse = IN.texCoord0;

    // tangent space matrix
    OUT.worldNormal = normalize(mul(Normal, (float3x3)gWorld));
    OUT.worldTangent = normalize(mul(Tangent, (float3x3)gWorld));
    OUT.worldBinormal = normalize(cross(OUT.worldTangent, OUT.worldNormal));

    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.worldPos); 
                                
    OUT.shadowmap_pixelPos = OUT.hPosition;

    return OUT;
}

#if USE_OFFSETS
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN, rageSkinVertexInputBumpOffset IN2, uniform float bUseOffsets) 
#else
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
#endif
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT;
    
    Position = IN.pos;
    Normal   = IN.normal;
    Tangent  = IN.tangent.xyz;
    
#if USE_OFFSETS
    if(bUseOffsets != 0.0f)
    {
        Position += IN2.pos;
        Normal   += IN2.normal;
        Tangent  += IN2.tangent.xyz;
    }
#endif
   
    float4 Plocal = float4(Position, 1.0);
    
#if USE_SKINNING
    rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
    float3 pos = rageSkinTransform(Position, boneMtx);
#else
	float3 pos = Position;
#endif

    OUT.worldPos = mul(float4(pos,1), gWorld).xyz;

    OUT.hPosition = mul(float4(pos,1), gWorldViewProj);
    OUT.texCoordDiffuse = IN.texCoord0;

    // tangent space matrix
#if USE_SKINNING
    Normal = normalize(rageSkinRotate(Normal, boneMtx));
    Tangent = normalize(rageSkinRotate(Tangent, boneMtx));
#endif

	OUT.worldNormal = normalize(mul(float4(Normal, 1), gWorld).xyz);
    OUT.worldTangent = normalize(mul(float4(Tangent, 1), gWorld).xyz);
    OUT.worldBinormal = normalize(cross(OUT.worldTangent, OUT.worldNormal));

    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - pos); 

    OUT.shadowmap_pixelPos = OUT.hPosition;

    return OUT;
}

float4 PS_CompositeLightingSilhouette( pixelInComposite IN ) : COLOR
{ 
	return float4(gLightAmbient.rgb,1.0);
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( pixelInComposite IN, DECLARE_FACING_INPUT(fFacing) ) : COLOR
{
	DECLARE_FACING_FLOAT(fFacing);
	float4 Color = tex2D( DiffuseSampler, IN.texCoordDiffuse );
	float4 bumpN = tex2D(BumpSampler, IN.texCoordDiffuse.xy) * 2 - 1;

	// compute specular color
	float4 SpecMapTexColor = tex2D(SpecSampler, IN.texCoordDiffuse.xy);
	float SpecularMap = SpecMapTexColor.x;

	//float4 ColorDetail = tex2D(DiffuseDetailMapSampler, IN.texCoordDiffuse.xy * DiffuseDetailMapTiling);
	float4 DetailNormTex = tex2D(DiffuseDetailMapSampler, IN.texCoordDiffuse.xy * DiffuseDetailMapTiling) * 2 - 1;
	float3 detailNormal;
	detailNormal.xy = DetailNormTex.wy;
	detailNormal.z = sqrt(1 - detailNormal.x * detailNormal.x - detailNormal.y * detailNormal.y);
	
	
	float4 SweatColor = float4(1.0f - tex2D(SweatMapSampler, IN.texCoordDiffuse.xy).xyz, 1.0f);

	float3 Normal;
	Normal.xy = bumpN.wy;
	Normal.z = sqrt(1 - Normal.x * Normal.x - Normal.y * Normal.y);
	bumpN.xyz = Normal;
	
	bumpN += float4(detailNormal * detailNormalScale, 0.0f);

	float backfacingSign = fFacing > 0 ? 1.0f : -1.0f;
	
	float3 worldNormal = IN.worldNormal * backfacingSign;
	float3 worldTangent = IN.worldTangent * backfacingSign;
	float3 worldBinormal = IN.worldBinormal * backfacingSign;

	// transforming N to worldspace ?
	float3 N = normalize((worldNormal * bumpN.z) + (bumpN.x * worldTangent) + (bumpN.y * worldBinormal));


	float4 amb = CalcAmbient(N);

	// Compute the light direction info, store the falloff in w component
	float4 LightDir = rageComputeLightData(IN.worldPos, 0).lightPosDir;
	float4 LightDir1 = rageComputeLightData(IN.worldPos, 1).lightPosDir;
	//float4 LightDir2 = rageComputeLightData(IN.worldPos, 2);

	if( gLightType[0] == LT_DIR )
		LightDir.w = 1.0f;

	if( gLightType[1] == LT_DIR )
		LightDir1.w = 1.0f;


	float3 L0 = normalize(LightDir.xyz);
	float3 L1 = normalize(LightDir1.xyz);
	//float3 L2 = normalize(LightDir2.xyz);

	// viewer vector in world space
	float3 E = normalize(IN.worldEyePos);


	// light vectors
	// should really multiply in the in the full shadow color somewhere, but our lights are just white right now anyway...
	float NL = saturate(dot(N, L0));    
	float NL1 = saturate(dot(N, L1));
	//float NL2 = saturate(dot(N, L2));

	float Specular0, Specular1;//, Specular2;

	//float4 Col = float4(lerp(Color, Color * ColorDetail, Color.a), 1);
	float4 Col = Color;

	float3 R = 2 * NL * N - L0;
	float3 R2 = 2 * NL1 * N - L1;
	//float3 R3 = 2 * NL2 * N - L2;

	float specExp = ShinySpecExponent;
	Specular0 = (ragePow(dot(R,E), specExp));
	Specular1 = (ragePow(dot(R2,E), specExp));
	//Specular2 = (ragePow(dot(R3,E), specExp));

	float3 specColor0 = lerp(gLightColor[0].rgb, Col.rgb * gLightColor[0].rgb, SpecToTexColorInterp);
	float3 specColor1 = lerp(gLightColor[1].rgb, Col.rgb * gLightColor[1].rgb, SpecToTexColorInterp);

	float4 Spec = float4((LightDir.w * Specular0 * specColor0) + (LightDir1.w * Specular1 * specColor1), 1);
	//float4 Spec = float4( 1,1,1,1 );

	float3 cubeReflect = reflect(-E, N);
	float4 specCoordBias;
	specCoordBias.xyz = cubeReflect;
	specCoordBias.w = cubeMapSpecExp;
	float4 specMap = texCUBElod(specCubeSampler, specCoordBias);
	float4 avgSpec = dot(specMap.rgb,float3(0.3f,0.59f,0.11f)); //greyscale cube map
	float colorMapLum = dot(Col.rgb, float3(0.3f, 0.59f, 0.11f));
	float4 cubeMapColor = lerp(specMap, avgSpec, colorMapLum * reflectMapDesatScale);

	Spec += lerp(cubeMapColor, cubeMapColor * Col, SpecToTexColorInterp) * cubeMapSpecIntensity;// * specMap.w;
	Spec *= CalcFresnelTerm(N, E, reflectFresnelExp, reflectFresnelMin, reflectFresnelMax);



	// add cloth sweatiness
	Col = Col * (1.0f - (sweatiness * SweatColor));

	//
	// Minnaert Lighting Model  
	//
	// MinnaertExponent == 1 -> Lambert
	// Color * (cos(A)^k * cos(B)^1-k)
	// Color * L.N^k * V.N^1-k
	float EN = saturate(ragePow(saturate(dot(E,N)), 1-MinnaertExponent));    
	
	float3 lightContrib, lightContrib1;
	if( gLightType[0] == LT_DIR )
		lightContrib = gLightColor[0].rgb;
	else
		lightContrib = LightDir.w * saturate(ragePow(NL, MinnaertExponent)) * EN * gLightColor[0].rgb;
	
	
	if( gLightType[1] == LT_DIR )
		lightContrib1 = gLightColor[1].rgb;
	else
		lightContrib1 = LightDir1.w * saturate(ragePow(NL1, MinnaertExponent)) * EN * gLightColor[1].rgb;
		
	
	float3 Minnaert =  lightContrib + lightContrib1;
					//LightDir1.w * saturate(ragePow(NL1, MinnaertExponent)) * EN * gLightColor[1].rgb;// +
					//LightDir2.w * saturate(ragePow(NL2, MinnaertExponent)) * EN * gLightColor[2].rgb;



	float4 diffuse = (ShinyAmbientIntensity * amb) + (ShinyDiffuseIntensity * float4(Minnaert, 1));
	float4 spec = ShinySpecularIntensity * SpecularMap * Spec;// * Col;
	float4 color = Col;
	float4 result = color * diffuse + spec;

	float fres = CalcFresnelTerm(N, E, rimFresnelExp, rimFresnelMin, rimFresnelMax);
	result = lerp(result, avgSpec, fres);
	return float4(result.xyz, 0.5f);


	//float4 color = AmbientIntensity * Col +  Bounce + (DiffuseIntensity * Col * float4(Minnaert, 1) + SpecularIntensity * SpecularMap * Spec);
	//color.w = 1.0f;
	//return color;
}

float4 PS_TestColor( pixelInComposite IN, uniform float4 color ) : COLOR
{
	return color;
}

#if USE_OFFSETS
#define OFFSET_ENABLE gbOffsetEnable
#else
#define OFFSET_ENABLE 
#endif

technique draw
{
    pass p0
    {
//#if	CULL_MODE != CW
        CullMode = CULL_MODE;
//#endif

		SrcBlend = ONE;
		DestBlend = ZERO;
 
        VertexShader = compile VERTEXSHADER VS_CompositeUnskinned(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}


technique drawskinned
{
    pass p0 
    {
//#if	CULL_MODE != CW
        CullMode = CULL_MODE;
//#endif

		SrcBlend = ONE;
		DestBlend = ZERO;
   
        VertexShader = compile VERTEXSHADER VS_CompositeSkin(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();

    }
}

technique silhouette_draw
{
    pass p0 
    {
        CullMode = NONE;
   
        VertexShader = compile VERTEXSHADER VS_CompositeUnskinned(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLightingSilhouette();

    }
}

technique silhouette_drawskinned
{
    pass p0 
    {

        CullMode = NONE;
   
        VertexShader = compile VERTEXSHADER VS_CompositeSkin(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLightingSilhouette();

    }
}
