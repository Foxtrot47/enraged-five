#ifndef __RAGE_MACROS_FXH_
#define __RAGE_MACROS_FXH_

#if RSG_DURANGO
//	#include "C:\Program Files (x86)\Microsoft Durango XDK\pc\include\xdk.h"  // not a valid for everyone include path.
	#define SHADER_STENCIL_ACCESS_AS_UINT2 (0 /*(_XDK_VER == 10951)*/)		  // Stencil access changed in May XDK. Set this to 1 for May only, June XDK (at least the Preview build) seems to be back to the old way
#else
	#define SHADER_STENCIL_ACCESS_AS_UINT2 (RSG_PC)
#endif

// =============================================================================================== //
// MACROS
// =============================================================================================== //

#define JOIN(x,y) _JOIN(x,y)
#define _JOIN(x,y) x##y

// ----------------------------------------------------------------------------------------------- //

#define JOIN3(x,y,z) _JOIN3(x,y,z)
#define _JOIN3(x,y,z) x##y##z

// ----------------------------------------------------------------------------------------------- //

#define JOIN4(x,y,z,w) _JOIN4(x,y,z,w)
#define _JOIN4(x,y,z,w) x##y##z##w

// ----------------------------------------------------------------------------------------------- //

#define JOIN5(v,x,y,z,w) _JOIN5(v,x,y,z,w)
#define _JOIN5(v,x,y,z,w) v##x##y##z##w

// ----------------------------------------------------------------------------------------------- //

#define JOIN6(u,v,w,x,y,z) _JOIN6(u,v,w,x,y,z)
#define _JOIN6(u,v,w,x,y,z) u##v##w##x##y##z

// ----------------------------------------------------------------------------------------------- //

#define JOIN8(s,t,u,v,w,x,y,z) _JOIN8(s,t,u,v,w,x,y,z)
#define _JOIN8(s,t,u,v,w,x,y,z) s##t##u##v##w##x##y##z

// ----------------------------------------------------------------------------------------------- //

#define FOREACH(DEF) FOREACH_##DEF(DEF)
#define DEF_TERMINATOR // terminator for multi-line FOREACH defines
#define EMPTY
#define COMMA ,
#define __COMMENT(x)
#define INDEX_NONE (-1)

#if __XENON
#define XENON_ONLY(x) x
#define XENON_SWITCH(_if_XENON_,_else_) _if_XENON_
#else
#define XENON_ONLY(x)
#define XENON_SWITCH(_if_XENON_,_else_) _else_
#endif

#if __PS3
#define PS3_ONLY(x) x
#define PS3_SWITCH(_if_PS3_,_else_) _if_PS3_
#else
#define PS3_ONLY(x)
#define PS3_SWITCH(_if_PS3_,_else_) _else_
#endif

#if __WIN32PC
#define WIN32PC_ONLY(x) x
#define WIN32PC_SWITCH(_if_WIN32PC_,_else_) _if_WIN32PC_
#else
#define WIN32PC_ONLY(x)
#define WIN32PC_SWITCH(_if_WIN32PC_,_else_) _else_
#endif

#if RSG_ORBIS
#define ORBIS_ONLY(x) x
#define ORBIS_SWITCH(_if_ORBIS_,_else_) _if_ORBIS_
#else
#define ORBIS_ONLY(x)
#define ORBIS_SWITCH(_if_ORBIS_,_else_) _else_
#endif

#if RSG_DURANGO
#define DURANGO_ONLY(x) x
#define DURANGO_SWITCH(_if_DURANGO_,_else_) _if_DURANGO_
#else
#define DURANGO_ONLY(x)
#define DURANGO_SWITCH(_if_DURANGO_,_else_) _else_
#endif

#if __XENON
#define XENON_PS3_SWITCH(_if_XENON_,_if_PS3_,_else_) _if_XENON_
#elif __PS3
#define XENON_PS3_SWITCH(_if_XENON_,_if_PS3_,_else_) _if_PS3_
#else
#define XENON_PS3_SWITCH(_if_XENON_,_if_PS3_,_else_) _else_
#endif

#define MAKE_float4x4(v,i) float4x4(v[i*4 + 0], v[i*4 + 1], v[i*4 + 2], v[i*4 + 3])

#define DECLARE_SAMPLER(samplerType,texName,samplerName,states) \
	BeginSampler       (samplerType,texName,samplerName,texName) \
	ContinueSampler    (samplerType,texName,samplerName,texName) \
	states \
	EndSampler

#define DECLARE_SHARED_SAMPLER(samplerType,texName,samplerName,samp,states) \
	BeginSharedSampler        (samplerType,texName,samplerName,texName,samp) \
	ContinueSharedSampler     (samplerType,texName,samplerName,texName,samp) \
	states \
	EndSampler

#define DECLARE_DX10_SAMPLER_INTERNAL(samplerType,textureType,texName,samplerName,states) \
	BeginDX10Sampler                 (samplerType,textureType,texName,samplerName,texName) \
	ContinueSampler                  (samplerType,            texName,samplerName,texName) \
	states \
	EndSampler

#if MULTISAMPLE_TECHNIQUES
#define TEXTURE2D_TYPE	Texture2DMS
#else
#define TEXTURE2D_TYPE	Texture2D
#endif	//MULTISAMPLE_TECHNIQUES

#define TEXTURE_DEPTH_COMPONENT_TYPE	float
#if SHADER_STENCIL_ACCESS_AS_UINT2
#define TEXTURE_STENCIL_COMPONENT_TYPE	uint2
#else
#define TEXTURE_STENCIL_COMPONENT_TYPE	uint
#endif

#define TEXTURE_DEPTH_TYPE			TEXTURE2D_TYPE<TEXTURE_DEPTH_COMPONENT_TYPE>
#define TEXTURE_STENCIL_TYPE		TEXTURE2D_TYPE<TEXTURE_STENCIL_COMPONENT_TYPE>

#define DECLARE_DX10_SAMPLER(pixelType,texName,samplerName,states)       DECLARE_DX10_SAMPLER_INTERNAL( sampler2D, Texture2D<pixelType>, texName, samplerName, states )
#define DECLARE_DX10_SAMPLER_MS(pixelType,texName,samplerName,states)    DECLARE_DX10_SAMPLER_INTERNAL( sampler2D, TEXTURE2D_TYPE<pixelType>, texName, samplerName, states )
#define DECLARE_DX10_SAMPLER_ARRAY(pixelType,texName,samplerName,states) DECLARE_DX10_SAMPLER_INTERNAL( sampler, Texture2DArray<pixelType>, texName, samplerName, states )

#define REP1(DEF,separator)                               DEF##0
#define REP2(DEF,separator) REP1(DEF,separator) separator DEF##1
#define REP3(DEF,separator) REP2(DEF,separator) separator DEF##2
#define REP4(DEF,separator) REP3(DEF,separator) separator DEF##3
#define REP5(DEF,separator) REP4(DEF,separator) separator DEF##4
#define REP6(DEF,separator) REP5(DEF,separator) separator DEF##5
#define REP7(DEF,separator) REP6(DEF,separator) separator DEF##6
#define REP8(DEF,separator) REP7(DEF,separator) separator DEF##7
#define REP9(DEF,separator) REP8(DEF,separator) separator DEF##8

#define REP1_COMMA(DEF)                  DEF##0
#define REP2_COMMA(DEF) REP1_COMMA(DEF), DEF##1
#define REP3_COMMA(DEF) REP2_COMMA(DEF), DEF##2
#define REP4_COMMA(DEF) REP3_COMMA(DEF), DEF##3
#define REP5_COMMA(DEF) REP4_COMMA(DEF), DEF##4
#define REP6_COMMA(DEF) REP5_COMMA(DEF), DEF##5
#define REP7_COMMA(DEF) REP6_COMMA(DEF), DEF##6
#define REP8_COMMA(DEF) REP7_COMMA(DEF), DEF##7
#define REP9_COMMA(DEF) REP8_COMMA(DEF), DEF##8

#endif // __RAGE_MACROS_FXH_
