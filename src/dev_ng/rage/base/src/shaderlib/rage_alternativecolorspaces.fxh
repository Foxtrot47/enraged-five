#ifndef __RAGE_ALTERNATIVECOLORSPACES_FXH
#define __RAGE_ALTERNATIVECOLORSPACES_FXH

#include "rage_constants.h"

// RGB12 format
// requires two 8-bit per channel render targets
// stores bits 1 - 8 of each color channel in render target 0 and
// bits 4 - 12 of each color channel in render target 1
// the 4 bit overlap is to account for alpha blending

#define VALUE_RANGE 8.0f
#define EIGHT_BIT_RANGE 255.0f
#define FOUR_BIT_RANGE 15.0f // four bits shared between hi and lo byte
#define FOUR_BIT_SHIFT 16.0f

// You need in each shader a similar output structure for encoding
struct Rgba12PixelOutput
{
	float4 Color0	: COLOR0;
	float4 Color1	: COLOR1;
};

Rgba12PixelOutput rageRgbaToRgba12(float4 rgba)
{
	Rgba12PixelOutput OUT=(Rgba12PixelOutput)0;

	float3 fhi;
	modf(rgba.rgb / VALUE_RANGE * EIGHT_BIT_RANGE / FOUR_BIT_SHIFT, fhi);
	fhi = fhi / (EIGHT_BIT_RANGE / FOUR_BIT_SHIFT);
	float3 flow = (rgba.rgb / VALUE_RANGE - fhi) * FOUR_BIT_RANGE;

	// alpha channel is stored in both render targets
	OUT.Color0= float4(flow, rgba.a);
	OUT.Color1= float4(fhi, rgba.a);
	
	return OUT;
}

float4 rageRgba12ToRgba(sampler2D OneToEight, sampler2D FourToTwelve, float2 Texcoord)
{
	float4 Low = tex2D(OneToEight, Texcoord);
	float4 High = tex2D(FourToTwelve, Texcoord);

	// store 
	return float4(High.xyz * VALUE_RANGE + Low.xyz * (VALUE_RANGE / FOUR_BIT_RANGE), Low.w);
}

float3 rageL16UvToRgb(float4 Luv)
{
	float2 xy = float2(9.0f, 4.0f) * Luv.yz / (dot(Luv.yz, float2(6.0f, -16.0f)) + 12.0f);

	float Ld = dot(Luv.wx, float2(8.0f, 8.0f / 255.0f));

	// Yxy -> XYZ conversion
	float3 XYZ = float3(xy.x, Ld, 1.0f - xy.x - xy.y);
	XYZ.xz = XYZ.xz * Ld / xy.y;

	// XYZ -> RGB conversion
	const float3x3 XYZ2RGB =
	{
		2.5651f,	-1.1665f,	-0.3986f,
		-1.0217f,	1.9777f,	0.0439f,
		0.0753f,	-0.2543f,	1.1892f
	};
	
	return mul(XYZ2RGB, XYZ);
}

float4 rageRgbToL16Uv(float3 rgb)
{
	// RGB -> XYZ conversion
	const float3x3 RGB2XYZ =
	{
		0.5141364f,	0.3238786f,		0.16036376f,
		0.265068f,	0.67023428f,	0.06409157f,
		0.0241188f,	0.1228178f,		0.84442666f
	};
	float3 XYZ = mul(RGB2XYZ, rgb); 

	// u and v convert to Luv
	float2 uv = float2(4.0f, 9.0f) * XYZ.xy / dot(XYZ, float3(1.0f, 15.0f, 3.0f));

	float fhi;
	modf(XYZ.y / 8.0f * 255.0f, fhi);
	fhi /= 255.0f;
	float flow = (XYZ.y / 8.0f - fhi) * 255.0f;

	// L lower range | u | v | L higher range
	return float4(flow, uv.x, uv.y, fhi);
}

int rageDeGamma(int C)
{
    int Clin;

    if (C < 64)
    {
         Clin = C;
    }
    else if (C < 96)
    {
	    Clin = 64 + (C-64)*2;
    }
    else if (C < 192)
    {
         Clin = 128 + (C-96)*4;
    }
    else
    {
        Clin = 513 + (C-192)*8;
    }
	
    return Clin;
}

int rageGamma(int Clin)
{
    int Cgam;

    if (Clin < 64)
    {
        Cgam = Clin;
    }
    else if (Clin < 128)
    {
        Cgam = 64 + (Clin-64)/2;
    }
    else if (Clin < 513)
    {
        Cgam = 96 + (Clin-128)/4;
    }
    else
    {
        Cgam = 192 + (Clin-512)/8;
    }
	
    return Cgam;
}

float rageDeGammaFloat(float f)
{
	int iRes = rageDeGamma(int(f*255.0f));
	return float(iRes / 1023.0f);
}

float rageGammaFloat(float f)
{
	int iRes = rageGamma(int(f*1023.0f));
	return float(iRes / 255.0f);
}

float4 rageRgbaToHsv(float4 rgba)
{
	float r, g, b, delta;
	float colorMax, colorMin;
	float h=0, s=0, v=0;
	float4 hsv=0;
	r = rgba[0];
	g = rgba[1];
	b = rgba[2];
	colorMax = max (r,g);
	colorMax = max (colorMax,b);
	colorMin = min (r,g);
	colorMin = min (colorMin,b);
	v = colorMax; // this is value
	if (colorMax != 0)
	{
		s = (colorMax - colorMin) / colorMax;
	}
	if (s != 0) // if not achromatic
	{
		delta = colorMax - colorMin;

		if (r == colorMax)
		{
			h = (g-b)/delta;
		}
		else if (g == colorMax)
		{
			h = 2.0 + (b-r) / delta;
		}
		else // b is max
		{
			h = 4.0 + (r-g)/delta;
		}

		h *= 60;
		if( h < 0)
		{
			h +=360;
		}
		
		hsv[0] = h / 360.0; // moving h to be between 0 and 1.
		hsv[1] = s;
		hsv[2] = v;
	}
	return hsv;
}

float4 rageHsvToRgb(float4 hsv)
{
	float4 color=0;
	float f,p,q,t;
	float h,s,v;
	float r=0,g=0,b=0;
	float i;
	if (hsv[1] == 0)
	{
		if (hsv[2] != 0)
		{
			color = hsv[2];
		}
	}
	else
	{
		h = hsv.x * 360.0;
		s = hsv.y;
		v = hsv.z;
		if (h == 360.0)
		{
			h=0;
		}
		
		h /=60;
		i = floor (h);
		f = h-i;
		p = v * (1.0 - s);
		q = v * (1.0 - (s * f));
		t = v * (1.0 - (s * (1.0 -f)));
		if (i == 0)
		{
			r = v;
			g = t;
			b = p;
		}
		else if (i == 1)
		{
			r = q;
			g = v;
			b = p;
		}
		else if (i == 2)
		{
			r = p;
			g = v;
			b = t;
		}
		else if (i == 3)
		{
			r = p;
			g = q;
			b = v;
		}
		else if (i == 4)
		{
			r = t;
			g = p;
			b = v;
		}
		else if (i == 5)
		{
			r = v;
			g = p;
			b = q;
		}
		color.r = r;
		color.g = g;
		color.b = b;
	}
	return color;
}

float3 rageRgbToCieYxy(float3 rgb)
{
   // RGB -> XYZ conversion
   const float3x3 RGB2XYZ = {0.5141364, 0.3238786,  0.16036376,
                      0.265068,  0.67023428, 0.06409157,
                      0.0241188, 0.1228178,  0.84442666};                                
   float3 XYZ = mul(RGB2XYZ, rgb);   
   
   // XYZ -> Yxy conversion
   float3 Yxy;
   Yxy.r = XYZ.g; // copy luminance Y

   // prevent division by zero
   float temp = dot(1.0f.xxx, XYZ.rgb);
   Yxy.gb = temp == 0.0f ? FLT_MAX : XYZ.rg / temp.xx;
   
   return Yxy;
}

float3 rageCieYxyToRgb(float3 Yxy)
{
   float3 XYZ;
   
   // Yxy -> XYZ conversion
   XYZ.r = Yxy.r * Yxy.g / Yxy. b;               // X = Y * x / y
   XYZ.g = Yxy.r;                                // copy luminance Y
   XYZ.b = Yxy.r * (1 - Yxy.g - Yxy.b) / Yxy.b;  // Z = Y * (1-x-y) / y

   // XYZ -> RGB conversion
   const float3x3 XYZ2RGB  = { 2.5651,-1.1665,-0.3986,
                       -1.0217, 1.9777, 0.0439, 
                        0.0753, -0.2543, 1.1892};
                        
  return mul(XYZ2RGB, XYZ);
}

float4 rageRgbToRgbe(float3 rgb)
{
   float4 rgbe;
   
   // Determine the largest color component
   float maxComponent = max(max(rgb.r, rgb.g), rgb.b);

   // round to the nearest ingeter exponent
   float exponent = ceil(rageLog2(maxComponent));
   
   // divde the components by the shared exponent
   rgbe.rgb = rgb / exp2(exponent);
   
   // store the shared exponent in the alpha channel
   rgbe.a = (exponent + 128) / 255.0;
   
   return rgbe;
}

float3 rageRgbeToRgb(float4 rgbe)
{
   float3 rgb;
   
   // retrieve the shared exponent
   float exponent = rgbe.a * 255.0 - 128.0;
   
   // multiply through the color components   
   rgb = rgbe.rgb * exp2(exponent);
   
   return rgb;
}

float4 rageRgbaToSrgb(float4 rgba)
{
#if __PS3 && 0
	return float4(unpack_4ubyte(pack_4ubytegamma(rgba)).rgb, rgba.a);
#else
	return float4(((rgba.rgb <= 0.0031308f) ? 12.92f * rgba.rgb : 1.055f * ragePow(rgba.rgb, 1.0f / 2.4f) - 0.055f), rgba.a);
#endif // __PS3
}

float4 rageSrgbToRgba(float4 srgb)
{
#if __PS3
 	return float4(unpack_4ubytegamma(pack_4ubyte(srgb)).rgb, srgb.a);
#else
	return float4(((srgb.rgb <= 0.04045f) ? srgb.rgb / 12.92f : ragePow((srgb.rgb + 0.055f) / 1.055f, 2.4f)), srgb.a);
#endif // __PS3
}

#if RAGE_ENCODEOPAQUECOLOR
float4 rageEncodeOpaqueColor(float3 rgb)
{
	return rageRgbToL16Uv(rgb);
}

float3 rageDecodeOpaqueColor(float4 encoded)
{
	return rageL16UvToRgb(encoded);
}
#else
float4 rageEncodeOpaqueColor(float3 rgb)
{
	return float4(rgb, 1);
}

float3 rageDecodeOpaqueColor(float4 encoded)
{
	return encoded.rgb;
}
#endif // RAGE_ENCODEOPAQUECOLOR

#endif // __RAGE_ALTERNATIVECOLORSPACES_FXH
