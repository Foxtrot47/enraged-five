#if __FXL
#define RAGE_DRAWBUCKET(n) property int __rage_drawbucket = n;
#define RAGE_DRAWBUCKETMASK(n) property int __rage_drawbucketmask = n;
#define RAGE_NAMED_DRAWBUCKET(c) property string drawBucketNamed = c;
#else
#define RAGE_DRAWBUCKET(n)
#define RAGE_DRAWBUCKETMASK(n)
#define RAGE_NAMED_DRAWBUCKET(c)
#endif
