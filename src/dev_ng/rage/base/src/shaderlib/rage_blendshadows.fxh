#ifndef __RAGE_BLENDSHADOWS_FXH
#define __RAGE_BLENDSHADOWS_FXH


#if !USE_GLOBAL_SHADOWLIGHTMTX
	float2 fadeOutLastSMap : FadeOutLastSMap;  // fade range
// #if __XENON	
// 	float4 fDepthBias;
 	float4 fBounds;
// #endif	
#if ENABLE_RAGE_SHADOWS
	float4 TopPlanes[3];
	float4 RightPlanes[3];
	float4 LeftPlanes[3];
#endif	
// 	float4 ShadowSpheres[4];
//    float4x4 LightMatrixArr[4];
#else
	#define fadeOutLastSMap gFadeOutLastSMap 
	#define LightMatrixArr	gLightMatrixArr
// #if __XENON	
// 	#define fDepthBias		gDepthBias
 	#define fBounds			gBounds
// #endif	
	#define TopPlanes		gTopPlanes
	#define RightPlanes		gRightPlanes
	#define LeftPlanes		gLeftPlanes
// 	#define ShadowSpheres	gShadowSpheres;
#endif

 	#define ShadowSpheres	gShadowSpheres;


//	float4 ShadowSpheres[4];


// #if !USE_GLOBAL_SHADOWLIGHTMTX
// 	#if __PSM	
// 		float4 fBounds;
// 	#endif	
// #else
// 	#if __PS3
// 		#define fBounds			gBounds
// 	#endif	
// #endif
/*
// #if !USE_GLOBAL_SHADOWLIGHTMTX
// BeginSampler(sampler2D, CharacterShadowTexture, CharacterShadowSampler, CharacterShadowTexture)
// string UIName="Character Shadow Texture";
// ContinueSampler(sampler2D, CharacterShadowTexture, CharacterShadowSampler, CharacterShadowTexture)
// 	AddressU  = WRAP;        
// 	AddressV  = WRAP;
// 	AddressW  = WRAP;
// #if __PS3	
// 	MAGFILTER = POINT;
// 	MINFILTER = POINT;
// #elif __XENON
// 	MAGFILTER = LINEAR;
// 	MINFILTER = LINEAR;
// #endif	
// 	MIPFILTER = POINT;
// 	MipMapLodBias = 0;
// EndSampler;
// #endif
*/
// follows the code in plane.h
float DistanceToPlane(float3 WorldSpace, float4 PlaneCoefficient)
{
	return dot(PlaneCoefficient.xyz, WorldSpace) - PlaneCoefficient.a;
};

#define USE_FPZ 0

#if __PS3
#define SHADOW_BACKBUFFER_WIDTH		960.0f
#define SHADOW_BACKBUFFER_HEIGHT	704.0f
#else
// #define SHADOW_BACKBUFFER_WIDTH		1280.0f
// #define SHADOW_BACKBUFFER_HEIGHT	720.0f
// RDR2
#define SHADOW_BACKBUFFER_WIDTH		1024.0f
#define SHADOW_BACKBUFFER_HEIGHT	600.0f

#endif


#if __XENON || __WIN32PC || __PSSL
#if __XENON
[reduceTempRegUsage(2)]
[maxtempreg(2)]
#endif

float4 BlendShadows(float4 WorldSpace,
					float3 ScreenPos,
					float fade
					)
#elif __PS3
float4 BlendShadows(float4 Pos0,
					float4 Pos1,
					float4 Pos2,
					float4 Pos3,
					float4 WorldSpace,
					float3 ScreenPos,
					float fade)
#endif				    
{
#if __XENON

#if ENABLE_RAGE_SHADOWS

	//
	// What we do here is 
	//
	// 1. calculate distance between two points
	// 2. compare to the radius. If smaller it is in the sphere
	//
	// // Original equation
	// inside = (sqrt((WorldSpace.x - CenterOfSphere.x)^2 + (WorldSpace.y - CenterOfSphere.y)^2 + (WorldSpace.z - CenterOfSphere.z)^2) < RadiusOfSphere
	//
	// The equation is simplified like this
	// inside = ((CenterOfSphere.x - WorldSpace.x)^2 + (CenterOfSphere.y - WorldSpace.y)^2 + (CenterOfSphere.z - WorldSpace.z)^2) < RadiusOfSphere^2	
	//
	float4 Dist;
	Dist.x = dot((WorldSpace.xyz - gShadowSpheres[0].xyz), (WorldSpace.xyz - gShadowSpheres[0].xyz));
	Dist.y = dot((WorldSpace.xyz - gShadowSpheres[1].xyz), (WorldSpace.xyz - gShadowSpheres[1].xyz));
	Dist.z = dot((WorldSpace.xyz - gShadowSpheres[2].xyz), (WorldSpace.xyz - gShadowSpheres[2].xyz));
	Dist.w = dot((WorldSpace.xyz - gShadowSpheres[3].xyz), (WorldSpace.xyz - gShadowSpheres[3].xyz));

	
	// whatever comes first, pick that
	// this is distance < radius
	// to make this work with the simplified equation, radius comes in as radius ^2 in the w channel
	float mapToUse = (Dist.x < gShadowSpheres[0].w) ? 0 :
				   (Dist.y < gShadowSpheres[1].w) ? 1 :
				   (Dist.z < gShadowSpheres[2].w) ? 2 :
				   (Dist.w < gShadowSpheres[3].w) ? 3 : 4;
    
    if(mapToUse == 4)
		return 1.0f;

	// Get pixel depth from the point of view from the light camera
	float4 pos = mul(float4(WorldSpace.xyz, 1.0f), LightMatrixArr[mapToUse]);
/*	
	float4 depth = 0.0f;
	float4 weights = 0.25;
	float  moments = 0.0f;
	
    asm 
    { 
        tfetch2D depth.x___, pos.xy, DepthTextureSampler0, OffsetX = -0.5, OffsetY = -0.5 
        tfetch2D depth._x__, pos.xy, DepthTextureSampler0, OffsetX =  0.5, OffsetY = -0.5 
        tfetch2D depth.__x_, pos.xy, DepthTextureSampler0, OffsetX = -0.5, OffsetY =  0.5 
        tfetch2D depth.___x, pos.xy, DepthTextureSampler0, OffsetX =  0.5, OffsetY =  0.5 
    };
	
	
// 		asm 
// 		{  
// 			// does not work with linear or anisotropic filtering here ... filtering seems to create a sampling artefact ... don't know why
// 			// NOTE: figure out why filtering is not working here, and save 3 texture lookups...
// 			tfetch2D depth.x___, pos.xy, DepthTextureSampler0, MagFilter = point, MinFilter = point, MipFilter = point, OffsetX = -0.5, OffsetY = -0.5
// 			tfetch2D depth._x__, pos.xy, DepthTextureSampler0, MagFilter = point, MinFilter = point, MipFilter = point, OffsetX =  0.5, OffsetY = -0.5
// 			tfetch2D depth.__x_, pos.xy, DepthTextureSampler0, MagFilter = point, MinFilter = point, MipFilter = point, OffsetX = -0.5, OffsetY =  0.5
// 			tfetch2D depth.___x, pos.xy, DepthTextureSampler0, MagFilter = point, MinFilter = point, MipFilter = point, OffsetX =  0.5, OffsetY =  0.5
// 			getWeights2D weights, pos.xy, DepthTextureSampler0, MagFilter=linear, MinFilter=linear
// 		}; 
// 		
// 	weights = float4( (1-weights.x)*(1-weights.y), weights.x*(1-weights.y), (1-weights.x)*weights.y, weights.x*weights.y );
// 
// 	moments = dot(depth, weights);
 	moments = dot(depth, 0.25f);

	
*/
	
	// now prepare for the 4-tap dither filter kernel
	// angle is in image space
    float angle = dot((ScreenPos.xy/ScreenPos.z), float2(3.0f * SHADOW_BACKBUFFER_WIDTH, 7.138f * SHADOW_BACKBUFFER_HEIGHT));

    float2 scx;
	sincos(angle, scx.x, scx.y);
	
	// scale the filter based on the map we use
    float scale =((4.0f - mapToUse) / SHADOW_BACKBUFFER_WIDTH); 
    
    #define KERNELSIZE 0.5f

	float4 Weight = float4(1.0f  * scale, 0.75f * scale, 0.5f  * scale, 0.25f * scale);
	Weight = Weight * KERNELSIZE;
    
    // you can feel the mad instruction in here :-)
    float4 toff0 = float4( scx.y, -scx.x,  scx.x,  scx.y) * Weight.xxyy + pos.xyxy; 
    float4 toff1 = float4(-scx.y,  scx.x, -scx.x, -scx.y) * Weight.zzww + pos.xyxy; 

    float4 moments;     
    asm 
    { 
        tfetch2D moments.x___, toff0.xy, DepthTextureSampler0, OffsetX= 0.0, OffsetY=0.0 
        tfetch2D moments._x__, toff0.zw, DepthTextureSampler0, OffsetX= 0.0, OffsetY=0.0 
        tfetch2D moments.__x_, toff1.xy, DepthTextureSampler0, OffsetX= 0.0, OffsetY=0.0 
        tfetch2D moments.___x, toff1.zw, DepthTextureSampler0, OffsetX= 0.0, OffsetY=0.0 
    };
    
   #if 1
    // the pos.z * pos.w is to compare a linear depth buffer with a linear depth buffer :-)
    float shade = dot(step(pos.z, moments), 0.25f);

    return lerp(shade, 1.0f, fade);
   #else   
	// get the pixels depth value & z bias value
	// we have an orthographic projection here, so there is no need to divide through w
  	float epsilon =  dot(max(0, moments - (pos.z)), 0.25f * fBounds[mapToUse]); 

	// probability function
	float eps = exp(epsilon); // * fBounds[mapToUse]);
	
	// the divide is a magic number to make the shadow dark
	return (lerp(saturate(eps / 4.0f - 0.25f), 1.0f, fade));   
   #endif	
   
#else
	return float4(0.0, 1.0, 0.0, 1.0);
#endif // ENABLE_RAGE_SHADOWS
	
   
#elif __PS3 

#if ENABLE_RAGE_SHADOWS
	// compare the pixel position to all three planes
	// then pick the shadow map based on this comparison
	float3 Result;
	Result.x = dot(TopPlanes[0].xyz, WorldSpace.xyz) - TopPlanes[0].a;
	Result.y = dot(TopPlanes[1].xyz, WorldSpace.xyz) - TopPlanes[1].a;
	Result.z = dot(TopPlanes[2].xyz, WorldSpace.xyz) - TopPlanes[2].a;
	float mapToUse = dot(Result.xyz > -0.1f, float3(1.0f,1.0f,1.0f));	


	// we can assume now that the point lies before the infinite top plane in mapToUse
	// if the point is before the top plane it can still end up in one of the corners
	// so check if it is behind the right or left plane
	// if it is -> grab the following map
	//
	// exclude the fourth map
	
 	float4 ws = float4(WorldSpace.xyz,-1);

	float2 Resulti = (mapToUse == 0) ? float2(dot(RightPlanes[0], ws), dot(LeftPlanes[0], ws)) :
					 (mapToUse == 1) ? float2(dot(RightPlanes[1], ws), dot(LeftPlanes[1], ws)) :
					 (mapToUse == 2) ? float2(dot(RightPlanes[2], ws), dot(LeftPlanes[2], ws)) :
					                   float2(-1.0f, -1.0f);	
							
	mapToUse += dot(Resulti> float2(-0.1f, -0.1f), float2(1.0f, 1.0f));

	float4 Posit;
	
	// now pick the texture coordinate to fetch the shadow map
	// squeeze in the depth bias value into the fourth channel
	Posit = (mapToUse == 0)? Pos0 : 
			(mapToUse == 1)? Pos1 :
			(mapToUse == 2)? Pos2 : Pos3;

	// now prepare for the 4-tap dither filter kernel
	// angle is in image space
    float angle = dot((ScreenPos.xy / ScreenPos.z), float2(1.0f * SHADOW_BACKBUFFER_WIDTH, 2.76f * SHADOW_BACKBUFFER_HEIGHT));

    float2 scx;
	sincos(angle, scx.x, scx.y);
	
	// scale the filter based on the map we use
    float scale =((4.0f - mapToUse) / SHADOW_BACKBUFFER_WIDTH); 

	float4 Weight = float4(1.0f  * scale, 0.75f * scale, 0.5f  * scale, 0.25f * scale);
    
    // you can feel the mad instruction in here :-)
    float4 toff0 = float4( scx.y, -scx.x,  scx.x,  scx.y) * Weight.xxyy + Posit.xyxy; 
    float4 toff1 = float4(-scx.y,  scx.x, -scx.x, -scx.y) * Weight.zzww + Posit.xyxy; 
    
	float4 moments;
	
	moments.x = tex2D(DepthTextureSampler0, float3(toff0.xy, Posit.z)).x;
	moments.y = tex2D(DepthTextureSampler0, float3(toff0.zw, Posit.z)).x;
	moments.z = tex2D(DepthTextureSampler0, float3(toff1.xy, Posit.z)).x;
	moments.w = tex2D(DepthTextureSampler0, float3(toff1.zw, Posit.z)).x;
	

	// probability function
	// Posit.w holds the bounds
//	float eps = exp(dot(moments, 0.25f*Posit.w));
	float eps = dot(moments, 0.25f);

	// the divide is a magic number to make the shadow dark
	return (lerp(eps, 1.0f, fade));   
	
#else
	return float4(0.0, 1.0, 0.0, 1.0);
#endif // ENABLE_RAGE_SHADOWS
	
#elif __WIN32PC || __PSSL
	return float4(0.0, 1.0, 0.0, 1.0);
#endif	
}

struct vertexOutputShadow 
{
	DECLARE_POSITION(hPosition)
    float4 worldPos				: TEXCOORD0; // w is used by MC to pass down object alpha/fade info
#if __PS3
	float4 shadowPosOne			: TEXCOORD1;
	float4 shadowPosTwo			: TEXCOORD2;
	float4 shadowPosThree		: TEXCOORD3;
	float4 shadowPosFour		: TEXCOORD4;
#endif 
#if __XENON || __PS3
	float3 ScreenPos			: TEXCOORD5;   
	float2 ExtraData			: TEXCOORD6; // x holds the dot product with the light source, y hold the shadow fade value
#endif	
};

//
// helper functions so you can "roll your own" shadow techniques
//
vertexOutputShadow VS_ShadowCompositeCommon(float4 hPosition, float4 worldPos , float3 normal) 
{
#if __XENON
	vertexOutputShadow OUT = (vertexOutputShadow)0;
#elif __PS3 || __WIN32PC || __PSSL
	vertexOutputShadow OUT = (vertexOutputShadow)0;
#endif    

	OUT.hPosition = hPosition;
	
//#if __XENON
	OUT.worldPos = worldPos;
#if USE_GLOBAL_SHADOWLIGHTMTX && __PS3
//	float3 viewDir = worldPos.xyz - gCameraMatrix[3].xyz;
//	OUT.camDistanceFadeOut = dot(viewDir, -gCameraMatrix[2].xyz);

	OUT.shadowPosOne = float4(mul(worldPos, gLightMatrixArr[0]).xyz, 1.0f);
	OUT.shadowPosTwo = float4(mul(worldPos, gLightMatrixArr[1]).xyz, 1.0f);
	OUT.shadowPosThree = float4(mul(worldPos, gLightMatrixArr[2]).xyz, 1.0f);
	OUT.shadowPosFour = float4(mul(worldPos, gLightMatrixArr[3]).xyz, 1.0f);
#elif __WIN32PC || __PSSL
	// nothing more needed pf PC?
#endif

#if USE_GLOBAL_SHADOWLIGHTMTX
    OUT.ScreenPos = ShadowCollectorGetScreenPos(hPosition);
    //OUT.ExtraData.x = saturate(dot(normal,-gLightPosDir[0].xyz));  // dot product of normal and light source
    OUT.ExtraData.x = 1.0f ; // fix this saturate(dot(normal,-gPrimaryDirectionalDir.xyz));  // dot product of normal and light source
    OUT.ExtraData.y = saturate((length(worldPos.xyz - gViewInverse[3].xyz) - fadeOutLastSMap.y) / fadeOutLastSMap.x);  // fade value...
#endif

	return OUT;
}

float4 PS_ShadowCompositeCommon(vertexOutputShadow IN)
{
#if __XENON
// 	// we subtract the position of the camera from the position of the vertex to get a ray
// 	float3 Ray = IN.worldPos.xyz - gCameraMatrix[3].xyz;
// 	
// 	// this code actually scales the length of the camera Z direction
// 	// so that it covers the camera distance
// 	// this results in the distance of the camera to a plane
// 	//
// 	//  vertex (2.0f, 0.0f, 3.0f)
// 	//  \
// 	//   \
// 	//    \ | gCameraMatrix[2](0, 0, 1.0f)
// 	//     \|
// 	//      \
// 	// the result of the dot product is (0.0, 0.0, 3.0) which is the distance
// 	float camDistance = dot(Ray, -gCameraMatrix[2].xyz);
// 
	return BlendShadows(IN.worldPos, IN.ScreenPos.xyz, IN.ExtraData.y); //, camDistance);
#elif __PS3

	return BlendShadows(IN.shadowPosOne, IN.shadowPosTwo, IN.shadowPosThree, IN.shadowPosFour, IN.worldPos, IN.ScreenPos.xyz, IN.ExtraData.y);
#elif __WIN32PC || __PSSL
	return float4(0.0, 0.0, 0.0, 0.0); // PC version does nothing for now...
#endif
}

#endif // __RAGE_BLENDSHADOWS_FXH		
