//
// shaderlib/memorycs.fxh
//
// Copyright (C) 2014-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef SHADERLIB_MEMORYCS_FXH
#define SHADERLIB_MEMORYCS_FXH

#ifndef MEMORYCS_COMPILING_SHADER
#	error "MEMORYCS_COMPILING_SHADER must be defined to either 0 or 1"
#endif

#if MEMORYCS_COMPILING_SHADER
#	include "rage_common.fxh"
#	include "rage_constantbuffers.fxh"
#endif


// Slot assignments
#define MEMORYCS_DST_BUF_SLOT                   0
#define MEMORYCS_SRC_BUF_SLOT                   0
#define MEMORYCS_WAIT_MEM32_SRC_BUF_SLOT        0
#define MEMORYCS_CBUF_ARGS_SLOT                 0


// Macros to make the shader globals cross platform
#if MEMORYCS_COMPILING_SHADER
#	if RSG_DURANGO || (RSG_PC && __SHADERMODEL >= 40)
#		define ConstantBuffer                   cbuffer
#		define ByteBuffer                       ByteAddressBuffer
#		define RegularBuffer                    StructuredBuffer
#		define RW_ByteBuffer                    RWByteAddressBuffer
#		define RW_RegularBuffer                 RWStructuredBuffer
#	elif RSG_ORBIS
#	endif
#	define GLOBAL_PREFIX(PREFIX, NAME)      MacroJoin(PREFIX, NAME)
#	define SHADER_GLOBALS                   passthrough
#	define MEMORYCS_REGISTER(TYPE, SLOT)    : register(MacroJoin(TYPE, SLOT))
#else
#	define ConstantBuffer                   struct ALIGNAS(16)
#	define uint                             u32
#	define GLOBAL_PREFIX(PREFIX, NAME)      NAME
#	define SHADER_GLOBALS                   namespace rage
#	define MEMORYCS_REGISTER(TYPE, SLOT)
#endif


// Global definitions
SHADER_GLOBALS {

#	if MEMORYCS_COMPILING_SHADER
		RW_ByteBuffer           g_Dst           MEMORYCS_REGISTER(u, MEMORYCS_DST_BUF_SLOT);
		ByteBuffer              g_Src           MEMORYCS_REGISTER(t, MEMORYCS_SRC_BUF_SLOT);
		RW_RegularBuffer<uint>  g_WaitMem32Poll MEMORYCS_REGISTER(u, MEMORYCS_WAIT_MEM32_SRC_BUF_SLOT);
#	endif

	ConstantBuffer GpuFill512_cbuf MEMORYCS_REGISTER(b, MEMORYCS_CBUF_ARGS_SLOT) {
		uint GLOBAL_PREFIX(g_Fill512, Size);        // number of bytes to write (must be multiple of 64)
		uint GLOBAL_PREFIX(g_Fill512, Stride);      // destination address increment for thread in loop
		uint GLOBAL_PREFIX(g_Fill512, Value);       // 32-bit value to write
	};

	ConstantBuffer GpuCopy512_cbuf MEMORYCS_REGISTER(b, MEMORYCS_CBUF_ARGS_SLOT) {
		uint GLOBAL_PREFIX(g_Copy512, Size);        // number of bytes to write (must be multiple of 64)
		uint GLOBAL_PREFIX(g_Copy512, Stride);      // destination address increment for thread in loop
	};

#	if RSG_DURANGO
		ConstantBuffer GpuWaitMem32_cbuf MEMORYCS_REGISTER(b, MEMORYCS_CBUF_ARGS_SLOT) {
			uint GLOBAL_PREFIX(g_WaitMem32, Value);      // polled memory location
		};
#	endif

} // SHADER_GLOBALS


// Shader code
#if MEMORYCS_COMPILING_SHADER

	[numthreads(64,1,1)]
	void CS_GpuFill512(uint tid:SV_DispatchThreadID) {
		uint  stride    = g_Fill512Stride;
		uint4 value4    = uint4(g_Fill512Value, g_Fill512Value, g_Fill512Value, g_Fill512Value);
		uint  offset    = tid*64;
		uint  size      = g_Fill512Size;
		[loop] while (offset < size) {
			g_Dst.Store4(offset,    value4);
			g_Dst.Store4(offset+16, value4);
			g_Dst.Store4(offset+32, value4);
			g_Dst.Store4(offset+48, value4);
			offset += stride;
		}
	}

	[numthreads(64,1,1)]
	void CS_GpuCopy512(uint tid:SV_DispatchThreadID) {
		uint stride     = g_Copy512Stride;
		uint offset     = tid*64;
		uint size       = g_Copy512Size;
		[loop] while (offset < size) {
			uint4 v0 = g_Src.Load4(offset);
			uint4 v1 = g_Src.Load4(offset+16);
			uint4 v2 = g_Src.Load4(offset+32);
			uint4 v3 = g_Src.Load4(offset+48);
			g_Dst.Store4(offset,    v0);
			g_Dst.Store4(offset+16, v1);
			g_Dst.Store4(offset+32, v2);
			g_Dst.Store4(offset+48, v3);
			offset += stride;
		}
	}

#	if RSG_DURANGO
		[numthreads(1,1,1)]
		void CS_GpuWaitMem32() {
			uint mem, test = g_WaitMem32Value;
			[allow_uav_condition] do {
				InterlockedCompareExchange(g_WaitMem32Poll[0], 0, 0, mem);
			} while (mem != test);
		}
#	endif

#endif // MEMORYCS_COMPILING_SHADER


// Techniques
#define TECHNIQUE_LIST(FUNC)                                                   \
	FUNC(CS_GpuFill512)                                                        \
	FUNC(CS_GpuCopy512)                                                        \
	DURANGO_ONLY_FUNC(FUNC, CS_GpuWaitMem32)

#if RSG_DURANGO
#	define DURANGO_ONLY_FUNC(FUNC, NAME)    FUNC(NAME)
#else
#	define DURANGO_ONLY_FUNC(FUNC, NAME)
#endif

#if MEMORYCS_COMPILING_SHADER
#	define TECHNIQUE_FUNC(NAME)     technique NAME { pass pass0 { SetComputeShader(compileshader(cs_5_0, NAME())); } }
	TECHNIQUE_LIST(TECHNIQUE_FUNC)
#else
#	define TECHNIQUE_FUNC(NAME)     NAME,
	namespace rage {
		enum MemoryCSTechniques {
			_unused_technique_0     = 0,
			TECHNIQUE_LIST(TECHNIQUE_FUNC)
		};
	}
#endif


// Cleanup
#if MEMORYCS_COMPILING_SHADER
#	if RSG_DURANGO
#		undef ByteBuffer
#		undef ConstantBuffer
#		undef RW_ByteBuffer
#		undef RW_RegularBuffer
#	elif RSG_ORBIS
#	endif
#	undef TECHNIQUE
#else
#	undef ConstantBuffer
#	undef uint
#	undef NEXT_TECHNIQUE_NUMBER
#endif
#undef GLOBAL_PREFIX
#undef SHADER_GLOBALS
#undef MEMORYCS_REGISTER
#undef DURANGO_ONLY_FUNC
#undef TECHNIQUE_LIST
#undef TECHNIQUE_FUNC

#endif // SHADERLIB_MEMORYCS_FXH
