
#ifndef __RAGE_FAST_LIGHT_FXH
#define __RAGE_FAST_LIGHT_FXH
//-----------------------------------------------------
// Calculates lighting with 4 lights at once using optimized shaders
//

#include "rage_sphericallight.fxh"

struct FastLight4
{
	float4 OneOverRangeSq;
	float4 posX;
	float4 posY;
	float4 posZ;

	float4 colR;
	float4 colG;
	float4 colB;
};
#define MAX_LIGHT_CONSTS	4

float4	Light4PosX[ MAX_LIGHT_CONSTS] : Light4PosX;
float4	Light4PosY[ MAX_LIGHT_CONSTS] : Light4PosY;
float4	Light4PosZ[ MAX_LIGHT_CONSTS] : Light4PosZ;
float4  Light4ColR[ MAX_LIGHT_CONSTS] : Light4ColR;
float4  Light4ColG[ MAX_LIGHT_CONSTS] : Light4ColG;
float4  Light4ColB[ MAX_LIGHT_CONSTS] : Light4ColB;
float4 Light4OneOverRange[ MAX_LIGHT_CONSTS] : Light4OneOverRange;


//------------------------ Light Sorting -------------------------------------------
//

float4 computeCost( float3 pos, float4 X, float4 Z )
{
	float4 dX = X - pos.xxxx;
	float4 dZ = Z - pos.zzzz;
	float4 dist = dX * dX;
	dist = dZ * dZ + dist;			// 4
	return dist;
}
float4 computeCostManhatten( float3 pos,float4 X, float4 Y )
{
	float4 dX = abs( X - pos.xxxx);
	float4 dY = abs( Y - pos.yyyy);
	return dY + dX;
}
struct sortEl
{
	float4 dist;
	float4 index;
};

void Swap( sortEl e1, sortEl e2 , out sortEl r1, out sortEl r2 )
{
	float4 selector =  e1.dist > e2.dist;

	r1.dist = lerp( e1.dist, e2.dist , selector );
	r1.index = lerp( e1.index, e2.index , selector );
	r2.dist = lerp( e2.dist, e1.dist , selector );
	r2.index = lerp( e2.index,e1.index , selector );
}


//---------------------------------------------------------------------------
//
// 4 at a time lighting calculations
//
float4 PhongIntensity( float4 lightDirX, float4 lightDirY, float4 lightDirZ, float3 ReflectedEye , float4 power, float4 occlusion, float4 recip )  
{
	// now get the dot for all the lights
	float4 res =  ReflectedEye.xxxx * lightDirX;								// 3
	res =  ReflectedEye.yyyy * lightDirY  + res;
	res =  ReflectedEye.zzzz * lightDirZ  + res;
	res *= recip;
	return ragePow( res, power ) * occlusion;													// 5  so with pairing shoulw be 7 instructions
}

// 19 instructions  15 instructions when paired
float3 CalculateDiffuseFastLight4( float3 pos, float3 normal, FastLight4 lgt )
{   
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	
	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	light *= atten; // 4 instructions
	light = saturate( light * recip );	

	// apply to colour
	float3	col = float3( dot( lgt.colR, light  ),
						   dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
	return col;
}

float3 CalculateDiffuseFastLightShadows4( float3 pos, float3 normal, float4 shadows, FastLight4 lgt )
{   
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	
	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	light *= atten; // 4 instructions
	light = saturate( light * recip );	
	light *=shadows;
	// apply to colour
	float3	col = float3( dot( lgt.colR, light  ),
						   dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
	return col;
}


// float4 depth
// float4 OneOverdepth
// float4 xxxx;
// float4 yyyy;
//
//	ang = x * x + y  * y
//  atten = depth * range + 1;
//  lit = dot( normal ? LightDir );
//  spot = ang * scale + bias;
//  lit *= ang;
// return lit;
float3 CalculateDiffuseSpotFastLight4( float3 pos, float3 normal, FastLight4 lgt, 
											float4 spotDirX, float4 spotDirY, float4 spotDirZ, 
											float4 SpotFadeScale, float4  SpotFadeOffset, float4 shadows )
{   
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	
	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	light *= atten; // 4 instructions
	light = saturate( light * recip );	
	
	float4 fallOff = dX * -spotDirX;
	fallOff = dY * -spotDirY + fallOff;
	fallOff = dZ * -spotDirZ + fallOff;
	fallOff *=recip;
	float4 spotInten = saturate( fallOff * SpotFadeScale + SpotFadeOffset );
	light = saturate( spotInten * light );
	light *=shadows;
	
	// apply to colour
	float3	col = float3( dot( lgt.colR, light  ),
						   dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
	return col;
}
float4 CalculateIntensitySpotFastLight4( float3 pos, FastLight4 lgt, 
											float4 spotDirX, float4 spotDirY, float4 spotDirZ, 
											float4 SpotFadeScale, float4  SpotFadeOffset )
{   
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	
	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct
	float4 light = saturate( atten * recip );	
	
	float4 fallOff = dX * -spotDirX;
	fallOff = dY * -spotDirY + fallOff;
	fallOff = dZ * -spotDirZ + fallOff;
	fallOff *=recip;
	float4 spotInten = saturate( fallOff * SpotFadeScale + SpotFadeOffset );
	light = saturate( spotInten * light );
	return light;
}
float CalculateVolumeSpotLight( float3 pos, float3 view, float3 spotPos,
											float3 spotDir, float spotOneOverRange, 
											float SpotFadeScale, float  SpotFadeOffset, float volIntensity)
{   

	FastLight4 lgt;
	float4 steps = float4( 0.5, 1.5, 2.5, 3.5 );
 
	lgt.posX = spotPos.xxxx + steps * view.xxxx;
	lgt.posY = spotPos.yyyy + steps * view.yyyy;
	lgt.posZ = spotPos.zzzz + steps * view.zzzz;
	lgt.OneOverRangeSq = spotOneOverRange;
	
	float4 inten = CalculateIntensitySpotFastLight4( pos, lgt, 
											spotDir.x, spotDir.y, spotDir.z, 
											SpotFadeScale, SpotFadeOffset );
				
	// now do blending from front to back
	// simple dot for now
	return dot( inten, volIntensity );
}
void PreCalculateDirections( FastLight4 lgt, float3 pos, out float4 dX, out float4 dY, out float4 dZ )
{
	dX = lgt.posX - pos.xxxx;
	dY = lgt.posY - pos.yyyy;
	dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions

	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360
	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	atten *= recip;
	dX *= atten;
	dY *= atten;
	dZ *= atten;
}

float3 CalculateDiffuseDirectional(  float3 normal, float4 dX, float4 dY, float4 dZ, FastLight4 lgt  )
{
	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	float3	col;
	col.x  = dot( lgt.colR, light);
	col.y  = dot( lgt.colG, light);
	col.z  =  dot( lgt.colB, light );
	
	return col;
}
// 19 instructions  15 instructions when paired
float3 CalculateDiffuseSpecularFastLight4( float3 pos, float3 normal, float3 ReflectedEye, float power, FastLight4 lgt, out float3 spec  )
{
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions

	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	light *= atten;	// 4
							
	float4 occ = (light > 0 );

	//recip *= atten;
	float4 phInten = PhongIntensity( dX, dY, dZ, ReflectedEye, power, occ, recip );
	
	light = saturate( light * recip );	// 4 instructions
	
	// apply to colour
	float3	col = float3(	dot( lgt.colR, light  ),
							dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
	
	 spec = float3(	dot( lgt.colR, phInten  ),
					dot( lgt.colG, phInten  ),
					dot( lgt.colB, phInten  ) );	// 3						
	return col;
}
float3 CalculateDiffuseSpecularFastSpotLight4( float3 pos, float3 normal, float3 Eye, float power, FastLight4 lgt, out float3 spec  )
{
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions

	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	light = saturate( light * recip );	// 4 instructions
	light *= atten;	// 1

	//-----------------------		// ~ 25 instructions
	// extra for spot lights
/*	float4 fallOff = dX * SpotDirX.xxxx;
	fallOff = dY * SpotDirY.yyyy + fallOff;
	fallOff = dZ * SpotDirZ.zzzz + fallOff;
	fallOff *=recip;
	spotInten = saturate( fallOff * SpotFadeScale + SpotFadeOffset );
	light = saturate( spotInten * light ) */

	// apply to colour
	float3	col = float3(	dot( lgt.colR, light  ),
							dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
							
	float4 occ = (light > 0 );
	spec = 1.0f;//PhongIntensity( dX, dY, dZ, normal,Eye, power, occ, recip );
	
	return col;
}


FastLight4 FastLight4Constants()
{
	// I am hoping the compiler with optimize out these moves completely
	FastLight4	lgt;
	lgt.posX = Light4PosX[0];
	lgt.posY = Light4PosY[0];
	lgt.posZ = Light4PosZ[0];

	lgt.colR = Light4ColR[0];
	lgt.colG = Light4ColG[0];
	lgt.colB = Light4ColB[0];
	
	lgt.OneOverRangeSq = Light4OneOverRange[0];
	return lgt;
}

FastLight4 FastLight4GetConstants( int index )
{
	FastLight4	lgt;
	lgt.posX = Light4PosX[ index];
	lgt.posY = Light4PosY[ index];
	lgt.posZ = Light4PosZ[ index];

	lgt.colR = Light4ColR[ index];
	lgt.colG = Light4ColG[ index];
	lgt.colB = Light4ColB[ index];
	
	lgt.OneOverRangeSq = Light4OneOverRange[ index];
	return lgt;
}
float3 CalculateSpecDiffuseFastLight4Constants( float3 pos, float3 normal )
{

	FastLight4	lgt;
	lgt.posX = Light4PosX[0];
	lgt.posY = Light4PosY[0];
	lgt.posZ = Light4PosZ[0];

	lgt.colR = Light4ColR[0];
	lgt.colG = Light4ColG[0];
	lgt.colB = Light4ColB[0];
	
	lgt.OneOverRangeSq = Light4OneOverRange[0];
	return CalculateDiffuseFastLight4( pos, normal , lgt );
}

float2 FastDirectional( float3 normal, float3 dir, float3 H , float power )  // H should be precomputed in vertex shader / offline
{
	float2 res;
	res.x = saturate( dot( normal, dir  ));
	float occ = saturate( res.x * 16.0f);
	res.y = ragePow( dot( normal, H ), power ) * occ;
	return res;
}
float3 FastDirectionalLight4( float3 normal, float4 dirX, float4 dirY, float4 dirZ, float4 colR, float4 colG, float4 colB )
{
	float4 dif =  normal.x * dirX;							
	dif =  normal.y * dirY  + dif;
	dif =  saturate( normal.z * dirZ  + dif );
	float3 res = float3( dot( colR, dif ),
						 dot( colG, dif ),
						 dot( colB, dif ) );
	return res;
}
float3 FastDirectionalLightShadows4( float3 normal, float4 shadows, float4 dirX, float4 dirY, float4 dirZ, float4 colR, float4 colG, float4 colB )
{
	float4 dif =  normal.x * dirX;							
	dif =  normal.y * dirY  + dif;
	dif =  saturate( normal.z * dirZ  + dif );
	dif *= shadows;
	float3 res = float3( dot( colR, dif ),
						 dot( colG, dif ),
						 dot( colB, dif ) );
	return res;
}
float3 FastDirectionalWrapLightShadows4( float3 normal, float4 shadows, float4 wrapScale, float4 wrapBias, float4 dirX, float4 dirY, float4 dirZ, float4 colR, float4 colG, float4 colB )
{
	float4 dif =  normal.x * dirX + wrapBias;							
	dif =  normal.y * dirY  + dif;
	dif =  saturate( (normal.z * dirZ  + dif) * wrapScale  );
	dif *= shadows;
	float3 res = float3( dot( colR, dif ),
						 dot( colG, dif ),
						 dot( colB, dif ) );
	return res;
}
float3 FastDirectionalWrapLightShadowsRim4( float3 normal, float3 view, float4 rimBias, float4 rimScale, float4 shadows, 
							float4 wrapScale, float4 wrapBias, float4 dirX, float4 dirY, float4 dirZ, float4 colR, float4 colG, float4 colB )
{
	float4 dif =  normal.x * dirX + wrapBias;							
	dif =  normal.y * dirY  + dif;
	dif =  saturate( (normal.z * dirZ  + dif) * wrapScale  );
	
	float4 rim = view.x * dirX + rimBias;
	rim =  view.y * dirY  + rim;
	rim =  saturate( (view.z * dirZ  + rim) * rimScale  );
	dif = rim * rim + dif;
	
	dif *= shadows;
	float3 res = float3( dot( colR, dif ),
						 dot( colG, dif ),
						 dot( colB, dif ) );
	return res;
}
float3 FastDirectionalWrapLightShadowsCamSpaceRim4( float3 normal, float4 rimBias, float4 rimScale, float4 shadows, 
							float4 wrapScale, float4 wrapBias, float4 dirX, float4 dirY, float4 dirZ, float4 colR, float4 colG, float4 colB )
{
	float4 dif =  normal.x * dirX + wrapBias;							
	dif =  normal.y * dirY  + dif;
	dif =  saturate( (normal.z * dirZ  + dif) * wrapScale  );
	
	float4 rim = saturate( normal.z * rimScale + rimBias );
	dif = rim * rim + dif;
	dif *= shadows;
	float3 res = float3( dot( colR, dif ),
						 dot( colG, dif ),
						 dot( colB, dif ) );
	return res;
}
/*
float4 FastDirectional4( float3 normal, float4 dir, float4 H[3], float power, out float4 dif  )  // H should be precomputed in vertex shader / offline
{
	float4 spec =  normal.x * H[0];								// 3
	spec =  normal.y * H[1]  + spec;
	spec =  normal.z * H[2]  + spec;
	
	spec = ragePow( spec, power );
	
	dif =  normal.x * dir[0];								// 3
	dif =  normal.y * dir[1]  + dif;
	dif =  saturate( normal.z * dir[2]  + dif );
	
	spec *=  ( dif > 0.0);
	return spec;													// 5  so with pairing shoulw be 7 instructions
}	*/

float3 FastDirectionLight( float3 normal, float4 dirX, float4 dirY, float4 dirZ, float4 colR, float4 colG, float4 colB )
{
	float4 dif =  normal.x * dirX;							
	dif =  normal.y * dirY  + dif;
	dif =  saturate( normal.z * dirZ  + dif );
	float3 res = float3( dot( colR, dif ),
						 dot( colG, dif ),
						 dot( colB, dif ) );
	return res;
}

SH2Color CalculateSH2DiffuseFastLight4( float3 pos, FastLight4 lgt, SH2Color shCol )
{
	float4 dX = lgt.posX - pos.xxxx;
	float4 dY = lgt.posY - pos.yyyy;
	float4 dZ = lgt.posZ - pos.zzzz;
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions

	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4	shConstants = GetSH2Constants();  
	dX *= recip;		// 3.0f instuctions
	dY *= recip ;
	dZ *= recip;
	
	lgt.colR *= atten;
	lgt.colG *= atten;
	lgt.colB *= atten; 		// 3 

	float4 red = float4( dot( lgt.colR , dY ) ,		//3
					dot( lgt.colR, dZ ) ,
					dot( lgt.colR, dX ),
					dot( lgt.colR, 1.0f) );
					
	shCol.red = red* shConstants * 0.25f;		//1

	float4 green = float4( dot( lgt.colG , dY ) ,		//3
					dot( lgt.colG, dZ ) ,
					dot( lgt.colG, dX ),
					dot( lgt.colG, 1.0f) );
	shCol.green  = green  * shConstants  * 0.25f;	//1

	float4 blue = float4( dot( lgt.colB , dY ) ,
					dot( lgt.colB, dZ ) ,
					dot( lgt.colB, dX ),
					dot( lgt.colG, 1.0f) );				//3
	shCol.blue  = blue  * shConstants * 0.25f;	//1

										// 25 instructions
	return shCol;
}
// --------------------------------------
// extra to do bounce lights ( they are light spot lights over the hemisphere )
// bounce lights
//float4 recip = 1.0f/dist;		// 4 instructions should be paired on 360

//float4 BounceDirX;

//float4 fallOff = dX * BounceDirX.xxxx;
//fallOff = dY * BounceDirY.yyyy + fallOff;
//fallOff = dZ * BounceDirZ.zzzz + fallOff;
//light *= fallOff;
//light = saturate( light * recip );	// ~ 24 instructions instructions

#endif
