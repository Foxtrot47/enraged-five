//---
//	Calculates hemisphere lighting with bounce contribution and mid range color

#ifndef RAGE_ANTIAS_FXH
#define RAGE_ANTIAS_FXH

//
// PURPOSE: Calculates alpha fade on distance to allow for subpixel objects such as wires or rods.
//
// REMARKS: Basically a simple ass linear fog mapped to alpha which is quick and dirty but works
//
// NOTES: pixel shader / vertex shader
// 
float antialias_CalculateSubPixelFadeApprox(  float distance,  float fov, float thickness )
{
	return saturate( thickness /( distance * 0.0001) + 0.01);
}

//
// PURPOSE: Calculates alpha fade on distance to allow for subpixel objects such as wires or rods using projection matrix.
//
// REMARKS: Uses the screen space projection of the object with a given thickness to calculate it's coverage over a pixel.
//	    so fade is better than the faster function above.
//
// NOTES: pixel shader / vertex shader
// 
float antialias_CalculateSubPixelFade( float3 inPos, float distance, float thickness )
{
	//float3 dir  = gViewInverse[2].xyz * distance + gViewInverse[0].xyz * thickness + gViewInverse[3].xyz;

	float3x3 WorldTranspose = transpose( (float3x3)gWorld );
	float3 modelSpaceOffset = mul( ( gViewInverse[0].xyz  * thickness ), (float3x3)WorldTranspose );
	
	float4 ispos = mul(float4( inPos, 1.0), gWorldViewProj);

	float4 scrpos = mul(float4( inPos + modelSpaceOffset, 1.0), gWorldViewProj);
	ispos.x /= ispos.w;
	scrpos.x /= scrpos.w;

	scrpos.x -= ispos.x;

	float2 screenScale = float2( 640.0f * 0.5f, 640.0f  * 0.5 );
	scrpos.x = scrpos.x * screenScale.x;
	return saturate( scrpos.x );
}
//
// PURPOSE: Calculates difference between screen space postions and hardware vpos register
//
// REMARKS: The calcuates the difference in two screen space positions in num texels. 
//
// NOTES: To be used in pixel shader
// 
float antialias_LineAntialias(	float4 projPos,float2 vpos)															
{
	float2 tpos=projPos.xy / projPos.w;
	tpos*=float4(640.0f,576.0f,0,0); 
	float len=smoothstep( 1.0f, 0.0f, length(tpos.xy-vpos.xy));
	return saturate( len );
}

//
// PURPOSE: Calculates difference between two screen space postions to approach antialiasing for lines
//
// REMARKS: The calcuates the difference in two screen space positions in num texels. It is slighty overestimated
//		as I found it gave a better look
//
// NOTES: To be used in pixel shader
// 
float antialias_LineAntialiasDiff(	float4 projPos, float4 projPos2)															
{
	float2 tpos=projPos.xy / projPos.w;
	float2 tpos2=projPos2.xy / projPos2.w;
	tpos*=float2(640.0f,576.0f); 
	tpos2*=float2(640.0f,576.0f); 
	float len=1.0f-  length(tpos.xy-tpos2.xy) * 0.7;
	return saturate( len );
}

//
// PURPOSE: Calculates fade antialaising position to do cheap antiasling on convex objects
//
// REMARKS: This stretches a convex about along it's normal in screen space. And stores both the orignal screenspace position
// and the new stretched one which allows for the difference to be calculated in the pixel shader to get fake antialiasing
//
// NOTES: To be used in vertex shader
// 
void antialias_CalculateFakeOffsets( float thickness, float3 wpos, float3 wnorm, float3 pos, float3 norm, float fov, out float4 newpos, out float4 scrpos )
{
	scrpos = mul(float4( pos, 1.0), gWorldViewProj);
	float d = scrpos.z;

	float3 invNorm = mul( float4( pos + norm.xyz, 1.0), gWorldViewProj).xyz;
	invNorm.xy = -( scrpos.xy - invNorm.xy );
	fov = 0.002f;
	newpos.z = scrpos.z;
	newpos.xy=  scrpos.xy + invNorm.xy * d * fov;
	scrpos.z = antialias_CalculateSubPixelFade(  pos, d,  thickness);
}

#endif

