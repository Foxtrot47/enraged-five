// RAGE_TOOLSET
//	This is designed to be a common header file to be used to handle some of the more
//	elaborate but commonly used shader effects.  Teams can feel free to use this.
//	Below, are a list of #defines that may or may not be set in order to control the
//	functionality and API of these functions.  IF YOU ADD MORE TO THIS FILE, MAKE SURE
//	YOU DESCRIBE THEM HERE
//
//		USE_NORMAL_MAP
//			Process appropriate tangent space vectors in vertex shader, and pull normal info from
//			a texture.
//		USE_SPECULAR
//			Factor in light specularity
//			USE_SPECULAR_UV1
//				Specular UV coordinates come from texcoord1
//			IGNORE_SPECULAR_MAP
//				Do not use a specular texture when computing specular strength
//		USE_REFLECT
//			Enable reflections
//			USE_SPHERICAL_REFLECT
//				Use a spherical reflection model instead of a cubic reflection model
//		USE_EMISSIVE
//			Enable CPV's to be treated as "light sources" instead of material colors
//		USE_DIFFUSE2
//			Enable a second texture stage channel
//			USE_DIFFUSE2_SPECULAR_MASK
//				Scale down specularity component in relation to inv. alpha channel
//
//		USE_DEFAULT_TECHNIQUES
//			Use the techniques in this shader versus rolling your own.
//
//		NO_SKINNING
//			Do not use skinning
//
//		INTERVAL_SHADOW_TERRAIN
//			Applies interval shadows to the terrain by using a storing interval map texture
//
//		USE_AMBIENT_OCCLUSION
//			Applies ambient occlusion to the by using a storing ambient occlusion map
//
//		USE_FORCECOLOR
//			Applies final multiply of float4 gForcedColor 
//


// There are a lot of #if tests in this code, but it greatly reduces code duplication
//	for a number of shaders.


#ifndef __RAGE_TOOLSET
#define __RAGE_TOOLSET

#include "rage_samplers.fxh"

#ifndef __RAGE_COMMON_FXH
	#include "rage_common.fxh"
#endif	// __RAGE_COMMON_FXH

#ifndef __RAGE_SKIN_FXH
  #ifndef NO_SKINNING
	#include "rage_skin.fxh"
  #endif
#endif

#ifdef USE_SHADOW_MAP
#include "rage_shadowmap_common.fxh"
#endif

// -----------------------------------------
// Process externally set #define's for configuring functions, variables, structures, etc.

#ifdef USE_SHADOW_MAP
	#define SHADOW_MAP 1
#else
	#define SHADOW_MAP 0
#endif

#ifdef USE_NORMAL_MAP
	#define NORMAL_MAP 1
#else
	#define NORMAL_MAP 0
#endif	// USE_NORMAL_MAP

#ifdef USE_SPECULAR
	#define SPECULAR 1
	
	#ifdef IGNORE_SPECULAR_MAP
		#define SPEC_MAP 0
	#else
		#define SPEC_MAP 1
	#endif	// IGNORE_SPECULAR_MAP
	#ifdef USE_SPECULAR_UV1
		#define SPECULAR_PS_INPUT	IN.texCoord.zw
		#define SPECULAR_VS_INPUT	IN.texCoord1.xy
		#define SPECULAR_VS_OUTPUT	OUT.texCoord.zw
		#define SPECULAR_UV1 1
	#else
		#define SPECULAR_PS_INPUT	IN.texCoord.xy
		#define SPECULAR_VS_INPUT	IN.texCoord0.xy
		#define SPECULAR_VS_OUTPUT	OUT.texCoord.xy
		#define SPECULAR_UV1 0
	#endif	// USE_SPECULAR_UV1
#else
	#define SPECULAR 0
	#define SPEC_MAP 0
#endif		// USE_SPECULAR

#ifdef USE_REFLECT
	#ifdef USE_SPHERICAL_REFLECT
		#define REFLECT_SAMPLER sampler2D
		#define REFLECT_TEXOP tex2D
		#define REFLECT_SPHERICAL 1
	#else
		#define REFLECT_SAMPLER samplerCUBE
		#define REFLECT_TEXOP texCUBE
		#define REFLECT_SPHERICAL 0
	#endif	// REFLECT_SPHERICAL
	#define REFLECT 1
#else
	#define REFLECT 0
#endif	// USE_REFLECT

#ifdef USE_EMISSIVE
	#define EMISSIVE 1
#else
	#define EMISSIVE 0
#endif		// USE_EMISSIVE

#ifdef USE_DIFFUSE2
	#define DIFFUSE2 1
	#ifdef USE_DIFFUSE2_SPECULAR_MASK
		#define DIFFUSE2_SPEC_MASK 1
	#else
		#define DIFFUSE2_SPEC_MASK 0
	#endif	// USE_DIFFUSE2_SPECULAR_MASK
#else
	#define DIFFUSE2 0
	#define DIFFUSE2_SPEC_MASK 0
#endif	// USE_DIFFUSE2

#define DIFFUSE_PS_INPUT	IN.texCoord.xy
#define DIFFUSE_VS_INPUT	IN.texCoord0.xy
#define DIFFUSE_VS_OUTPUT	OUT.texCoord.xy

#ifdef USE_DEFAULT_TECHNIQUES
	#define TECHNIQUES 1
#else
	#define TECHNIQUES 0
#endif	// USE_DEFAULT_TECHNIQUES

#ifdef NO_SKINNING
	#define SKINNING 0
#else
	#define SKINNING 1
#endif

// -----------------------------------------

BeginConstantBuffer(rage_material,c64)

// VARIABLES THAT ARE SET BASED ON SWITCHES ENABLED
#if SPECULAR
	float specularFactor : Specular
	<
		string UIName = "Specular Falloff";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 100.0;

	float specularColorFactor : SpecularColor
	<
		string UIName = "Specular Intensity";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 1.0;

	#if SPEC_MAP
	#endif	// SPEC_MAP

#endif		// SPECULAR

#if NORMAL_MAP
	float bumpiness : Bumpiness
	<
		string UIWidget = "slider";
		float UIMin = 0.0;
		float UIMax = 200.0;
		float UIStep = .01;
		string UIName = "Bumpiness";
	> = 1.0;

#endif		// NORMAL

#if REFLECT

	float reflectivePower : Reflectivity
	<
		string UIName = "Reflectivity";
		float UIMin = -10.0;
		float UIMax = 10.0;
		float UIStep = 0.1;
	> = 0.45;
#endif	// REFLECT

#if DIFFUSE2
	#if SPECULAR
		#if DIFFUSE2_SPEC_MASK
			float diffuse2SpecClamp : Diffuse2MinSpec
			<
				string UIName="Texture2 Min Specular";
				string UIHelp="Minimum amount of specular allowed for secondary texture";
				float UIMin = 0.0;
				float UIMax = 10.0;
				float UIStep = 0.01;
			> = 0.2;
		#else
			float diffuse2SpecMod : Diffuse2ModSpec
			<
				string UIName="Texture2 Specular Modifier";
				string UIHelp="Amount of specular power added by alpha of secondary texture";
				float UIMin = 0.0;
				float UIMax = 1000.0;
				float UIStep = 0.1;
			> = 0.8;
		#endif // DIFFUSE_SPEC_MASK
	#endif // SPECULAR
#endif	// DIFFUSE2

EndConstantBuffer(rage_material)

// This value is used for depth render code
#define Depth_DivideWTerm 1.0



// DISABLE FEATURES BASED ON HARDWARE LIMITATIONS
#if RSG_PC || RSG_DURANGO 
	#if __SHADERMODEL < 20
		// Really old shaders lose lots of stuff
		#undef NORMAL_MAP
		#define NORMAL_MAP 0
		#undef REFLECT
		#define REFLECT 0
		#undef SPECULAR
		#define SPECULAR 0
		#undef DIFFUSE2
		#define DIFFUSE2 0
		#undef SHADOW_MAP
		#define SHADOW_MAP 0


		#undef 	DIFFUSE_TEX_COORDS
		#define DIFFUSE_TEX_COORDS			float2
		
		// Currently, all we can go down to is 1.4
		//	If we need lower, we can allow it
		#define PIXELSHADER	ps_1_4
		#define VERTEXSHADER vs_1_1
	#elif __SHADERMODEL >= 30
		#if __SHADERMODEL >= 40
			#if __SHADERMODEL >= 41
				#if __SHADERMODEL >= 50
					#define PIXELSHADER		ps_5_0
					#define VERTEXSHADER	vs_5_0
				#else
					#define PIXELSHADER		ps_4_1
					#define VERTEXSHADER	vs_4_1
				#endif
			#else
				#define PIXELSHADER		ps_4_0
				#define VERTEXSHADER	vs_4_0
			#endif
		#else
			#define PIXELSHADER		ps_3_0
			#define VERTEXSHADER	vs_3_0
		#endif
	#else
		// If shadow maps are desired, turn off everything else for 2.0
		#if SHADOW_MAP
			#undef NORMAL_MAP
			#define NORMAL_MAP 0
			#undef REFLECT
			#define REFLECT 0
			#undef SPECULAR
			#define SPECULAR 0
		#endif	// SHADOW_MAP
		#define PIXELSHADER ps_2_0
		#define VERTEXSHADER vs_2_0
	#endif	// __SHADERMODEL
#elif RSG_ORBIS
	#define PIXELSHADER		ps_5_0
	#define VERTEXSHADER	vs_5_0
#elif __XENON || __PS3
	#define PIXELSHADER ps_3_0
	#define VERTEXSHADER vs_3_0
#endif

// Determine whether to pack in two uv's into one register
#if DIFFUSE2 || SPECULAR_UV1
	#define DIFFUSE_TEX_COORDS			float4
#else
	#define DIFFUSE_TEX_COORDS			float2
#endif	// DIFFUSE2 || SPECULAR_UV1

// Default vertexoutput/pixelshader input structure -- feel free to use if needed

#define INTERPOLATE_VIEWINVPOS		__PS3

struct vertexOutput {
    DECLARE_POSITION(pos)
    DIFFUSE_TEX_COORDS texCoord     : TEXCOORD0;
	float3 worldNormal				: TEXCOORD1;
#if SHADOW_MAP	
	float4 shadowmap_pixelPos		: TEXCOORD2;
#endif //SHADOW_MAP
	float3 worldEyePos				: TEXCOORD3;
#if NORMAL_MAP
	float3 worldTangent				: TEXCOORD4;
	float3 worldBinormal			: TEXCOORD5;
#endif // NORMAL_MAP
	float4 color0					: COLOR0;
#if INTERVAL_SHADOW_TERRAIN
	float2 intervalCoords			: TEXCOORD6;
#endif
#if INTERPOLATE_VIEWINVPOS
	float3 viewInvPos				: TEXCOORD7;
#endif
};


struct vertexOutputUnlit {
    DECLARE_POSITION(pos)
    DIFFUSE_TEX_COORDS texCoord		: TEXCOORD0;
	float4 color0					: COLOR0;
#if SHADOW_MAP
	float4 screenPos				: TEXCOORD1;
#endif
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct vertexOutputDepth {
	DECLARE_POSITION(pos)
    float2 texCoord         : TEXCOORD0;
	float3 depthColor		: COLOR0;
	float4 color0			: COLOR1;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rtsProcessVertLightingUnskinned(
					float3 localPos, 
					float3 localNormal, 
	#if NORMAL_MAP
					float4 localTangent, 
	#endif // NORMAL_MAP
					float4 vertColor,
					float4x4 worldMtx, 
					out float3 worldEyePos,
					out float3 worldNormal,
	#if NORMAL_MAP
					out float3 worldBinormal,
					out float3 worldTangent,
	#endif	// NORMAL_MAP
					out float4 color0
	#if INTERVAL_SHADOW_TERRAIN
					, out float4	intervalCoords
	#endif 
	)
{
	float3 pos = mul(float4(localPos,1), worldMtx).xyz;

	// Store position in eyespace
	worldEyePos = gViewInverse[3].xyz - pos;

	// Transform normal by "transpose" matrix
	worldNormal = normalize(mul(localNormal, (float3x3)worldMtx));;

#if NORMAL_MAP
	// Do the tangent+binormal stuff
	float3 binorm = rageComputeBinormal(localNormal, localTangent);
	worldTangent = normalize(mul(localTangent.xyz, (float3x3)worldMtx));
	worldBinormal = normalize(mul(binorm, (float3x3)worldMtx));
#endif		// NORMAL_MAP

#if INTERVAL_SHADOW_TERRAIN
	intervalCoords =  shadowIntervalGetTexCoords( pos );
#endif
#if EMISSIVE
	color0 = float4(vertColor.xyz, 0.0);
#else
    color0 = vertColor;
#endif
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rtsProcessVertLightingSkinned(
					float3 localPos, 
					float3 localNormal, 
	#if NORMAL_MAP
					float4 localTangent, 
	#endif // NORMAL_MAP
					float4 vertColor,
					rageSkinMtx boneMtx, 
					out float3 worldEyePos,
					out float3 worldNormal,
	#if NORMAL_MAP
					out float3 worldBinormal,
					out float3 worldTangent,
	#endif	// NORMAL_MAP
					out float4 color0
	#if INTERVAL_SHADOW_TERRAIN
					, out float4	intervalCoords
	#endif 
	)
{
	float3 pos = rageSkinTransform(localPos,boneMtx);

	// Store position in eyespace
	worldEyePos = gViewInverse[3].xyz - pos;

	// Transform normal by "transpose" matrix
	worldNormal = normalize(rageSkinRotate(localNormal,boneMtx));

#if NORMAL_MAP
	// Do the tangent+binormal stuff
	float3 binorm = rageComputeBinormal(localNormal, localTangent);
	worldTangent = normalize(rageSkinRotate(localTangent.xyz, boneMtx));
	worldBinormal = normalize(rageSkinRotate(binorm, boneMtx));
#endif		// NORMAL_MAP

#if INTERVAL_SHADOW_TERRAIN
	intervalCoords =  shadowIntervalGetTexCoords( pos );
#endif
#if EMISSIVE
	color0 = float4(vertColor.xyz, 0.0);
#else
    color0 = vertColor;
#endif
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 rtsComputePixelColor( float4 baseColor,
							float3 worldNormal,
							float2 diffuseTexCoord,
							sampler2D diffuseSampler,
	#if DIFFUSE2
							float2 diffuseTexCoord2,
							sampler2D diffuseSampler2,
	#endif
	#if SPEC_MAP
							float2 specularTexCoord,
							sampler2D specularSampler,
	#endif	// SPEC_MAP
	#if REFLECT	
							REFLECT_SAMPLER environmentSampler,
	#endif	// REFLECT
	#if NORMAL_MAP
							float2 bumpTexCoord,
							sampler2D bumpSampler,
							float3 worldTangent,
							float3 worldBinormal,
	#endif	// NORMAL_MAP
							float3 worldEyePos
	#if SHADOW_MAP
							,float4 shadowmap_pixelpos
	#endif
	
	#if INTERVAL_SHADOW_TERRAIN
							, float4	intervalCoords
	#endif
	#if INTERPOLATE_VIEWINVPOS
							, float3    viewInvPos
	#endif
						)
{


	float4 diffuseColor = tex2D(diffuseSampler, diffuseTexCoord);
    // Compute the shared stuff first
#if NORMAL_MAP
    // Normally, bump would be = (sampledColor - 0.5) * 2.0
    //	However, since we're pulling in a bumpiness term, we can factor in the scale there & save a mult
    float3 bumpN = (tex2D(bumpSampler, bumpTexCoord.xy).xyz - 0.5) * bumpiness;
    float3 N = normalize(worldNormal + (bumpN.x * worldTangent) + (bumpN.y * worldBinormal) );
#else
	float3 N = worldNormal;
#endif	// NORMAL_MAP

#if SPECULAR || REFLECT || NORMAL_MAP
    float3 E = normalize(worldEyePos);
#endif	// SPECULAR || REFLECT

#if SPECULAR
	#if SPEC_MAP
		float4 specularColor = tex2D(specularSampler, specularTexCoord.xy) * specularColorFactor;
	#else
		float4 specularColor = specularColorFactor;
	#endif	// SPEC_MAP
	
	float specStrength = specularColor.w * specularFactor;
#endif	// SPECULAR

#if REFLECT
	// Compute reflection vector & sample color
	float3 R = normalize(reflect(-E, N));
#if REFLECT_SPHERICAL
	R = (R + 1.0) * -0.5;
#endif	// REFLECT_SPHERICAL

#if SPECULAR
	#if REFLECT_SPHERICAL
		float3 reflectColor = REFLECT_TEXOP(environmentSampler, R.xy ).xyz * specularColor.xyz * reflectivePower;
	#else
		float3 reflectColor = REFLECT_TEXOP(environmentSampler, R ).xyz * specularColor.xyz * reflectivePower;
	#endif	// SPEC_MAP
#else
    float3 reflectColor = REFLECT_TEXOP(environmentSampler, R ).xyz * reflectivePower;
#endif // SPECULAR
#endif    
       

#if SHADOW_MAP
	// NOTE: HOW SHOULD THIS HOOK BACK IN WITH NEW SHADOWS?
	float4 screenpos;
	float scale = 1.f / (shadowmap_pixelpos.w * 2.f);
	screenpos.x = 0.5+(shadowmap_pixelpos.x * scale);
	screenpos.y = 1-(0.5+(shadowmap_pixelpos.y * scale));
	
	float4 shadowFactor = ShadowCollectorGetShadowTexCoord(screenpos.xy).xxxx;
#if INTERVAL_SHADOW_TERRAIN
	rageLightOutput lightData = rageComputeLightData(viewDir, i);
	shadowFactor = Min( shadowIntervalVisiblityTerrain(  lightData.lightPosDir.xyz, intervalCoords ), shadowFactor );
#endif
#endif
    
    // Now, compute light contribution(s)
	// Compute the light info, store the distance to light in w component
	float4 light;
	float3 color = float3(0,0,0);
#if SPECULAR
	// Compute specular color
	float3 specular = float3(0,0,0);
#endif	// SPECULAR

#ifndef DISABLE_RAGE_LIGHTING
// HACK -- work around NVIDIA DRIVER BUG
#if __XENON
	#define LOOP_COUNT	gLightCount
#elif __SHADERMODEL < 30
	#define LOOP_COUNT 1
#else
	#define LOOP_COUNT 4
#endif
// END HACK
	float3 viewDir;
#if INTERPOLATE_VIEWINVPOS
	viewDir = viewInvPos - worldEyePos;
#else
	viewDir = gViewInverse[3].xyz - worldEyePos;
#endif
	for (int i = 0; i < LOOP_COUNT; ++i) {
		rageLightOutput lightData = rageComputeLightData(viewDir, i);
	    float3 L = lightData.lightPosDir.xyz;

		// Compute falloff term
#if __SHADERMODEL < 30
		float falloff = 1.0;
#else
		float falloff = 1.0 - min((lightData.lightPosDir.w / gLightPosDir[i].w), 1.0);
		float lightIntensity = gLightColor[i].w;

#if 1	// SCE OPTIMIZED
	    float bra = ( lightIntensity < 0.f );
	    float bra_true = falloff * -lightIntensity;
	    float bra_false = min(falloff * lightIntensity, 1.0);
	    falloff = bra ? bra_true:bra_false;
#else
		if ( lightIntensity < 0.f ) {
			falloff *= -lightIntensity;		// Allow oversaturation
		}
		else {
			falloff = min(falloff * lightIntensity, 1.0);
		}
#endif
#endif

#if SPECULAR
	    float3 H = normalize(L + E);
	    light.y = saturate(dot(N,L));
#if 1	// SCE_OPTIMIZED
	{
		//SCE code: step2: delete 2nd branch
	    //condition
        float bra2 = (light.y <=0);
		//for taken
        float2 lyz_bra2_true = float2(light.y, 0.0);
		//for not taken
        //condition
        float bra3 = (lightData.lightDir.w > 0.0f);

        //for taken
        float spotEffect = saturate(dot(lightData.lightDir.xyz, lightData.lightPosDir.xyz));
        float lz0 = ragePow((saturate(dot(N,H))),specStrength);
        float2 lyz0 = float2(light.y, lz0) * 
            ( (spotEffect > lightData.lightDir.w) ? spotEffect - lightData.lightDir.w : 0 );

        //for not taken
        float lz1 = ragePow((saturate(dot(N,H))),specStrength);
        float2 lyz1 = float2(light.y, lz1);

        //select
        float2 lyz_bra2_false = bra3 ? lyz0 : lyz1;

		//final select
        light.yz = bra2 ? lyz_bra2_true : lyz_bra2_false;
    }
#else	   
	    if (light.y <=0)
	    {
			light.z = 0.0;
		}
		else
		{
			if (lightData.lightDir.w > 0.0f)
			{
				float spotEffect = saturate(dot(lightData.lightDir.xyz, lightData.lightPosDir.xyz));
				light.z = ragePow((saturate(dot(N,H))),specStrength);
				light.yz *= (spotEffect > lightData.lightDir.w) ? spotEffect - lightData.lightDir.w : 0;
			}
			else
			{		
				light.z = ragePow((saturate(dot(N,H))),specStrength);
			}
		}	 
#endif
		light.xw = 1.0;
	       
	    //light = rageGetLitFactors(dot(N,L), dot(N,H), specStrength);
#else
		light = saturate(dot(N,L));
		
		if (lightData.lightDir.w > 0.0f)
		{
			float spotEffect = dot(lightData.lightDir.xyz, lightData.lightPosDir.xyz);
			light *= (spotEffect > lightData.lightDir.w) ? spotEffect : 0;
		}		
#endif	// SPECULAR

#if SPECULAR
		specular += gLightColor[i].rgb * light.z * falloff;
#endif	// SPECULAR		

		// Sum up color contribution of light sources
		color += gLightColor[i].rgb * light.y * falloff;
	}
#endif // DISABLE_RAGE_LIGHTING

#if EMISSIVE
	color += baseColor.rgb;
#endif	// EMISSIVE

#if SHADOW_MAP
	color *= shadowFactor.xyz;
#endif

#ifndef DISABLE_RAGE_LIGHTING
    color += gLightAmbient.xyz;
#endif // DISABLE_RAGE_LIGHTING

#if SPECULAR
    specular *= specularColor.xyz;
#endif	// SPECULAR
	
	// NOTE: I'm intentially not saturating the color here so that I can solicit feedback
	//	about conditions where a lot of bright light is added to the scene (yields a pseudo
	//	bloom effect without blurring)
	
    // Sum it all up
    float4 outColor =diffuseColor;
#if DIFFUSE2
	float4 overlayColor = tex2D(diffuseSampler2, diffuseTexCoord2);
	float invAlpha = 1.0 - overlayColor.w;
	outColor.rgb = (outColor.rgb * invAlpha) + (overlayColor.rgb * overlayColor.a);
	// Scale specular term if needed
	#if SPECULAR
		#if DIFFUSE2_SPEC_MASK
			specular *= max(invAlpha, diffuse2SpecClamp);
		#else
			specular *= overlayColor.w * diffuse2SpecMod;
		#endif
	#endif	// SPECULAR
#endif	// DIFFUSE2

#if REFLECT
	#if SHADOW_MAP
		reflectColor *= shadowFactor.xyz;
	#endif	// SHADOW_MAP
	// Add in reflection component
	outColor.rgb += reflectColor;
#endif	// REFLECT
	
	// Factor in lighting
	outColor *= float4(color,1.0);

#if !EMISSIVE
	// Factor in material color
	outColor *= baseColor;
#endif	// !EMISSIVE
#if SPECULAR
    #if SHADOW_MAP
		specular *= shadowFactor.xyz;
	#endif
    // Add in specular
    outColor += float4(specular,0.);
#endif	// SPECULAR

#ifndef DISABLE_RAGE_LIGHTING
	outColor.a *= gLightAmbient.w;
#endif // DISABLE_RAGE_LIGHTING

#ifdef USE_FORCECOLOR
	// outColor *= gForcedColor;
#endif //USE_FORCECOLOR

	return outColor;
}

float4 rtsComputeNormalColor( float4 baseColor,
							float3 worldNormal,
							float2 diffuseTexCoord,
							sampler2D diffuseSampler
	#if NORMAL_MAP
							,float2 bumpTexCoord,
							sampler2D bumpSampler,
							float3 worldTangent,
							float3 worldBinormal
	#endif	// NORMAL_MAP
						)
{
	float alpha = (tex2D(diffuseSampler, diffuseTexCoord).a) * baseColor.a;
    // Compute the shared stuff first
#if NORMAL_MAP
    // Normally, bump would be = (sampledColor - 0.5) * 2.0
    //	However, since we're pulling in a bumpiness term, we can factor in the scale there & save a mult
    float3 bumpN = (tex2D(bumpSampler, bumpTexCoord.xy).xyz - 0.5) * bumpiness;
    float3 N = normalize(worldNormal + (bumpN.x * worldTangent) + (bumpN.y * worldBinormal) );
#else
	float3 N = worldNormal;
#endif	// NORMAL_MAP
	
	N = (N * 0.5) + 0.5;
	
	//float alpha = (tex2D(diffuseSampler, diffuseTexCoord).a) * baseColor.a;

	return float4(N, alpha);
}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* ====== VERTEX SHADERS =========== */
// ******************************
//	DRAW METHODS
// ******************************
vertexOutput VS_Transform(rageVertexInputBump IN
#ifdef RAGE_ENABLE_OFFSET
	, rageVertexOffsetBump INOFFSET
#endif
)
{
    vertexOutput OUT;
    
    float3 inPos = IN.pos;
    float3 inNrm = IN.normal;
    float4 inCpv = IN.diffuse;

#if NORMAL_MAP
    float4 inTan = IN.tangent;
#endif

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
	inNrm += INOFFSET.normal;
	inCpv += INOFFSET.diffuse;
# if NORMAL_MAP
	inTan.xyz += INOFFSET.tangent.xyz;
# endif
#endif

	rtsProcessVertLightingUnskinned( inPos, 
							inNrm, 
#if NORMAL_MAP
							inTan,
#endif
							inCpv, 
							gWorld, 
							OUT.worldEyePos,
							OUT.worldNormal,
#if NORMAL_MAP
							OUT.worldBinormal,
							OUT.worldTangent,
#endif // NORMAL_MAP
							OUT.color0 
						);

    // Write out final position & texture coords
    OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
#if INTERPOLATE_VIEWINVPOS
    OUT.viewInvPos = gViewInverse[3].xyz;
#endif
    
    // NOTE: These 3 statements may resolve to the exact same code -- rely on HLSL to strip worthless code
    DIFFUSE_VS_OUTPUT = DIFFUSE_VS_INPUT;
#if SPEC_MAP
    SPECULAR_VS_OUTPUT = SPECULAR_VS_INPUT;
#endif	// SPEC_MAP
#if NORMAL_MAP
	OUT.texCoord.xy = IN.texCoord0.xy;
#endif	// NORMAL_MAP

#if DIFFUSE2
	// Pack in 2nd set of texture coords if needed
	OUT.texCoord.zw = IN.texCoord1.xy;
#endif	// DIFFUSE2

#if SHADOW_MAP    
	OUT.shadowmap_pixelPos = OUT.pos;
#endif

	return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputUnlit VS_TransformUnlit(rageVertexInput IN
#ifdef RAGE_ENABLE_OFFSET
	, rageVertexOffsetBump INOFFSET
#endif
	)
{
    vertexOutputUnlit OUT;
   
    float3 inPos = IN.pos;
    float4 inCpv = IN.diffuse;

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
	inCpv += INOFFSET.diffuse;
#endif
	
    // Write out final position & texture coords
    OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
    // NOTE: These 3 statements may resolve to the exact same code -- rely on HLSL to strip worthless code
    DIFFUSE_VS_OUTPUT = DIFFUSE_VS_INPUT;
#if SPEC_MAP
    SPECULAR_VS_OUTPUT = SPECULAR_VS_INPUT;
#endif	// SPEC_MAP
#if NORMAL_MAP
	OUT.texCoord.xy = IN.texCoord0.xy;
#endif	// NORMAL_MAP

#if DIFFUSE2
	// Pack in 2nd set of texture coords if needed
	OUT.texCoord.zw = IN.texCoord1.xy;
#endif	// DIFFUSE2

#if SHADOW_MAP
    OUT.screenPos = OUT.pos;
#endif

    OUT.color0 = inCpv;
    return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputDepth VS_TransformDepth(rageVertexInput IN
#ifdef RAGE_ENABLE_OFFSET
	, rageVertexOffset INOFFSET
#endif
	)
{
   vertexOutputDepth OUT;
   
	float3 inPos = IN.pos;
    float4 inCpv = IN.diffuse;

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
	inCpv += INOFFSET.diffuse;
#endif
	
   // Write out final position & texture coords
   OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
   OUT.texCoord = IN.texCoord0;
   float  depth=(OUT.pos.z/Depth_DivideWTerm);
   OUT.depthColor = float3(depth,0.0,0.0);
   OUT.color0 = inCpv;
        
    return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputDepth smd_VS_TransformDepth(rageVertexInput IN
#ifdef RAGE_ENABLE_OFFSET
	, rageVertexOffset INOFFSET
#endif
)
{
   vertexOutputDepth OUT;

	float3 inPos = IN.pos;
    float4 inCpv = IN.diffuse;

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
	inCpv += INOFFSET.diffuse;
#endif

   //Write out final position & texture coords
   //OUT.worldNormal = mul(float4(IN.normal,1), gWorldViewProj);
   OUT.texCoord = IN.texCoord0;
   OUT.color0 = IN.diffuse;
	
	OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
   float4 A = OUT.pos;
   
   float depth = A.z/A.w;
   OUT.depthColor.x =  A.z;
   OUT.depthColor.y =  A.w;
   OUT.depthColor.z =  depth;
   OUT.color0	= inCpv;
   
   return OUT;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ******************************
//	DRAWSKINNED METHODS
// ******************************
#ifndef NO_SKINNING
vertexOutput VS_TransformSkin(rageSkinVertexInputBump IN
#ifdef RAGE_ENABLE_OFFSET
	, rageSkinVertexOffsetBump INOFFSET
#endif
	) 
{
    vertexOutput OUT;
    
    float3 inPos = IN.pos;
    float3 inNrm = IN.normal;

#if NORMAL_MAP
    float4 inTan = IN.tangent;
#endif

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
	inNrm += INOFFSET.normal;
# if NORMAL_MAP
	inTan.xyz += INOFFSET.tangent.xyz;
# endif
#endif
	
#if SKINNING
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );

	rtsProcessVertLightingSkinned( inPos,
							inNrm,
#if NORMAL_MAP
							inTan,
#endif	// NORMAL_MAP
#if EMISSIVE
							/*IN.diffuse*/ float4(0,0,0,1),													
#else
							/*IN.diffuse*/ float4(1,1,1,1),
#endif
							boneMtx, 
							OUT.worldEyePos,
							OUT.worldNormal,
#if NORMAL_MAP
							OUT.worldBinormal,
							OUT.worldTangent,
#endif	// NORMAL_MAP
							OUT.color0
						);
#else
	rtsProcessVertLightingUnskinned( inPos,
							inNrm,
#if NORMAL_MAP
							inTan,
#endif	// NORMAL_MAP
#if EMISSIVE
							/*IN.diffuse*/ float4(0,0,0,1),													
#else
							/*IN.diffuse*/ float4(1,1,1,1),
#endif
							gWorldMtx, 
							OUT.worldEyePos,
							OUT.worldNormal,
#if NORMAL_MAP
							OUT.worldBinormal,
							OUT.worldTangent,
#endif	// NORMAL_MAP
							OUT.color0
						);
#endif
			

    // Write out final position & texture coords
	// Transform position by bone matrix
#if SKINNING
	float3 pos = rageSkinTransform(inPos, boneMtx);
#else
	float3 pos = inPos;
#endif

    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    // NOTE: These 3 statements may resolve to the exact same code -- rely on HLSL to strip worthless code
    DIFFUSE_VS_OUTPUT = DIFFUSE_VS_INPUT;
#if SPEC_MAP
    SPECULAR_VS_OUTPUT = SPECULAR_VS_INPUT;
#endif	// SPEC_MAP
#if NORMAL_MAP
	OUT.texCoord.xy = IN.texCoord0.xy;
#endif	// NORMAL_MAP

#if DIFFUSE2
	// Pack in 2nd set of texture coords if needed
	OUT.texCoord.zw = IN.texCoord1.xy;
#endif	// DIFFUSE2

#if SHADOW_MAP    
	OUT.shadowmap_pixelPos = OUT.pos;
#endif
#if INTERPOLATE_VIEWINVPOS
	OUT.viewInvPos = gViewInverse[3].xyz;
#endif

	return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputUnlit VS_TransformSkinUnlit(rageSkinVertexInput IN
#ifdef RAGE_ENABLE_OFFSET
	, rageSkinVertexOffset INOFFSET
#endif
	) 
{
    vertexOutputUnlit OUT;
    
    float3 inPos = IN.pos;

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
#endif
	
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
	// Transform position by bone matrix
	float3 pos = rageSkinTransform(inPos, boneMtx);

    // Write out final position & texture coords
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    // NOTE: These 3 statements may resolve to the exact same code -- rely on HLSL to strip worthless code
    DIFFUSE_VS_OUTPUT = DIFFUSE_VS_INPUT;
#if SPEC_MAP
    SPECULAR_VS_OUTPUT = SPECULAR_VS_INPUT;
#endif	// SPEC_MAP
#if NORMAL_MAP
	OUT.texCoord.xy = IN.texCoord0.xy;
#endif	// NORMAL_MAP

#if DIFFUSE2
	// Pack in 2nd set of texture coords if needed
	OUT.texCoord.zw = IN.texCoord1.xy;
#endif	// DIFFUSE2

#if SHADOW_MAP
    OUT.screenPos = OUT.pos;
#endif

    OUT.color0 = /*IN.diffuse*/ float4(1,1,1,1);
    return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputDepth VS_TransformSkinDepth(rageSkinVertexInput IN
#ifdef RAGE_ENABLE_OFFSET
	, rageSkinVertexOffset INOFFSET
#endif
	) 
{
    vertexOutputDepth OUT;
    
    float3 inPos = IN.pos;

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
#endif
	
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
	// Transform position by bone matrix
	float3 pos = rageSkinTransform(inPos, boneMtx);

    // Write out final position & texture coords
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    OUT.texCoord = IN.texCoord0;
    
    float  depth=(OUT.pos.z/Depth_DivideWTerm);
    
    OUT.depthColor = float3(depth,0.0,0.0);
    OUT.color0 =  float4(1,1,1,1);
        
    return OUT;    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vertexOutputDepth smd_VS_TransformSkinDepth(rageSkinVertexInput IN
#ifdef RAGE_ENABLE_OFFSET
	, rageSkinVertexOffset INOFFSET
#endif
	) 
{
    vertexOutputDepth OUT;
    float3 inPos = IN.pos;

#ifdef RAGE_ENABLE_OFFSET
	inPos += INOFFSET.pos;
#endif
    
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
	// Transform position by bone matrix
	float3 pos = rageSkinTransform(inPos, boneMtx);

    // Write out final position & texture coords
    OUT.texCoord = IN.texCoord0;
    OUT.color0 =  float4(1,1,1,1);
    
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
	float4 A = OUT.pos;
    float depth = A.z/A.w;
    
    OUT.depthColor.x =  A.z;
    OUT.depthColor.y =  A.w;
    OUT.depthColor.z =  depth;
    
    OUT.color0 = IN.diffuse;
        
    return OUT;    
}
#endif	// NO_SKINNING

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//------------------------------------
/* ====== Texture Samplers =========== */
// NOTE: Do we want to configure the filter & wrap states via the texture itself??

BeginSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName="Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

#if DIFFUSE2
	BeginSampler(sampler2D,diffuseTexture2,TextureSampler2,DiffuseTex2)
		string UIName="Secondary Texture";
		string UIHint="uv1";
	ContinueSampler(sampler2D,diffuseTexture2,TextureSampler2,DiffuseTex2)
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	EndSampler;
#endif	// DIFFUSE2

#if NORMAL_MAP
	BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
		string UIName="Bump Texture";
		string UIHint="normalmap";
	ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	EndSampler;
#endif	// NORMAL_MAP

#if SPEC_MAP
	BeginSampler(sampler2D,specTexture,SpecSampler,SpecularTex)
		string UIName="Specular Texture";
		#if SPECULAR_UV1
			string UIHint="specularmap,uv1";
		#else
			string UIHint="specularmap";
		#endif
	ContinueSampler(sampler2D,specTexture,SpecSampler,SpecularTex)
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	EndSampler;
#endif	// SPEC_MAP

#if REFLECT
	BeginSampler(REFLECT_SAMPLER,envTexture,EnvironmentSampler,EnvironmentTex)
		string UIName="Environment Texture";
		string ResourceType = "Cube";
	ContinueSampler(REFLECT_SAMPLER,envTexture,EnvironmentSampler,EnvironmentTex)
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	EndSampler;
#endif // REFLECT

/* ====== PIXEL SHADERS =========== */
//-----------------------------------
float4 PS_Textured( vertexOutput IN): COLOR
{
	IN.worldNormal = normalize( IN.worldNormal );
	return rtsComputePixelColor( IN.color0, 
								IN.worldNormal,
								DIFFUSE_PS_INPUT,
								TextureSampler, 
#if DIFFUSE2
								IN.texCoord.zw,
								TextureSampler2,
#endif	// DIFFUSE2
#if SPEC_MAP
								SPECULAR_PS_INPUT,
								SpecSampler,
#endif	// SPEC_MAP
#if REFLECT
								EnvironmentSampler,
#endif	// REFLECT
#if NORMAL_MAP
								IN.texCoord.xy,
								BumpSampler, 
								IN.worldTangent,
								IN.worldBinormal,
#endif	// NORMAL_MAP
								IN.worldEyePos				
#if SHADOW_MAP
								,IN.shadowmap_pixelPos
#endif						
#if INTERPOLATE_VIEWINVPOS
								,IN.viewInvPos		
#endif
							);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_TexturedUnlit( vertexOutputUnlit IN ) : COLOR
{
	float4 color = IN.color0;
    
    // Sum it all up
    float4 outColor =  tex2D(TextureSampler, DIFFUSE_PS_INPUT);
#if SHADOW_MAP	
	float4 screenpos;
	float scale = 1.f / (IN.screenPos.w * 2.f);
	screenpos.x = 0.5+(IN.screenPos.x * scale);
	screenpos.y = 1-(0.5+(IN.screenPos.y * scale));
	
	float4 shadowColor = ShadowCollectorGetShadowTexCoord(screenpos.xy).xxxx;
#endif	

#if DIFFUSE2
	float4 overlayColor = tex2D(TextureSampler2, IN.texCoord.zw);
	outColor.rgb = outColor.rgb * (1.0 - overlayColor.w) + overlayColor.rgb * overlayColor.w;
#endif	// DIFFUSE2

#if SHADOW_MAP
	outColor.rgb *= shadowColor.rgb; 
#endif
	// outColor.a *= gLightAmbient.w;
	return outColor * color;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if (__WIN32PC && !__PSSL) || __XENON
#pragma warning (disable: 3596)
#endif
float4 PS_PaintNormals( vertexOutput IN ) : COLOR
{
	return rtsComputeNormalColor( IN.color0, 
								IN.worldNormal,
								DIFFUSE_PS_INPUT,
								TextureSampler 
#if NORMAL_MAP
								,IN.texCoord.xy,
								BumpSampler, 
								IN.worldTangent,
								IN.worldBinormal
#endif	// NORMAL_MAP
							);
}
#if (__WIN32PC && !__PSSL) || __XENON
#pragma warning (enable: 3596)
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_DrawDepth( vertexOutputDepth IN ) : COLOR
{
	//if((IN.color0.w*tex2D(TextureSampler, IN.texCoord.xy).w)<1);
#if __SHADERMODEL >= 20
	if((tex2D(TextureSampler, IN.texCoord).w*IN.color0.w)<1)
		clip(-1);	
#endif	// __SHADERMODEL
	//Assumes floating point texture
	return float4(IN.depthColor.x,0.0,0.0,1.0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 smd_PS_DrawDepth( vertexOutputDepth IN ) : COLOR
{
#if __SHADERMODEL >= 20
	if((tex2D(TextureSampler, IN.texCoord).w*IN.color0.w)<0.6f)
		clip(-1);	
#endif	// __SHADERMODEL
	return float4((IN.depthColor.x/IN.depthColor.y),0.f, 0.f, 0.0);
	//return float4(0.0,1.0,0.0,1.0);
}

float4 PS_Simple() : COLOR
{
	return float4(0,1,0,1);
}

//-----------------------------------

#if TECHNIQUES
	// ===============================
	// Lit (default) techniques
	// ===============================
	technique draw
	{
		pass p0 
		{        
			AlphaBlendEnable = true;
			AlphaTestEnable = true;
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Textured() CGC_FLAGS("-fastprecision");
		}
	}
	technique drawskinned
	{
		pass p0 
		{        
			AlphaRef = 100;
			AlphaBlendEnable = true;
			AlphaTestEnable = true;
			VertexShader = compile VERTEXSHADER VS_TransformSkin();
			PixelShader  = compile PIXELSHADER PS_Textured() CGC_FLAGS("-fastprecision");
		}
	}

#if !__PS3
	// ===============================
	// depth techniques
	// ===============================
	technique depth_draw
	{
		pass p0 
		{        
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile VERTEXSHADER VS_TransformDepth();
			PixelShader  = compile PIXELSHADER PS_DrawDepth();
			//AlphaBlendEnable = true;
			//AlphaTestEnable = true;
		}
	}
	technique depth_drawskinned
	{
		pass p0 
		{        
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile VERTEXSHADER VS_TransformSkinDepth();
			PixelShader  = compile PIXELSHADER PS_DrawDepth();
			//AlphaBlendEnable = true;
			//AlphaTestEnable = true;

		}
	}
	technique smd_draw
	{
		pass p0 
		{        
			//ZEnable = true;
			//ZWriteEnable = true;
			//ZFunc = LESS;
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile VERTEXSHADER smd_VS_TransformDepth();
			PixelShader  = compile PIXELSHADER smd_PS_DrawDepth();
		}
	}
	technique smd_drawskinned
	{
		pass p0 
		{        
			//ZEnable = true;
			//ZWriteEnable = true;
			//ZFunc = LESS;
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile VERTEXSHADER smd_VS_TransformSkinDepth();
			PixelShader  = compile PIXELSHADER smd_PS_DrawDepth();
		}
	}
#endif	// !__PS3
	
	// ===============================
	// Unlit techniques
	// ===============================
	technique unlit_draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_TransformUnlit();
			PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
		}
	}
	technique unlit_drawskinned
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
			PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
		}
	}
	
	// ===============================
	// Tool techniques
	// ===============================
	technique tool_draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_TransformUnlit();
			PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
		}
	}
	technique tool_drawskinned
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
			PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
		}
	}
	
#if !__PS3
	// ===============================
	// Normals Only techniques
	// ===============================
	technique normals_draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_PaintNormals();
		}
	}
	technique normals_drawskinned
	{
		pass p0 
		{        
			AlphaRef = 100;
			VertexShader = compile VERTEXSHADER VS_TransformSkin();
			PixelShader  = compile PIXELSHADER PS_PaintNormals();
		}
	}
#endif	// !__PS3

#endif	// TECHNIQUES

#endif	// __RAGE_TOOLSET
