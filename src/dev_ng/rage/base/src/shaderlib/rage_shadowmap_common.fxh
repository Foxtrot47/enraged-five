// INCLUDE GUARDS
#ifndef __RAGE_SHADOWMAP_COMMON_FXH
#define __RAGE_SHADOWMAP_COMMON_FXH

#include "rage_common.fxh"

#include "rage_shadow_config.h"

// the following variables are used for cascaded shadow maps
//row_major shared float4x4 gLightViewProjMatrixArr[4] : LightViewProjMatrixArr REGISTER(c48);
CBSHARED BeginConstantBufferPagedDX10(rage_CamMtxBuffer, b5)
SHARED ROW_MAJOR float4x4 gCameraMatrix : CameraMatrix REGISTER(c32);
SHARED float4 gShadowmap_ShadowColor0 : Shadowmap_ShadowColor0 REGISTER(c36) = float4(1,1,1,0.723);
SHARED float4 gShadowmap_PixelSize0 : Shadowmap_PixelSize0 REGISTER(c37);
EndConstantBufferDX10(rage_CamMtxBuffer)

#if USE_GLOBAL_SHADOWLIGHTMTX
CBSHARED BeginConstantBufferDX10(rage_ShadowConstBuffer)
	SHARED float2 gFadeOutLastSMap	 : FadeOutLastSMap REGISTER(c38);  // .x = fade range, .y = start distance for fade
	SHARED ROW_MAJOR float4x4 gLightMatrixArr[4]: LightMatrixArr REGISTER2(ps,c64);
	SHARED float4 gDepthBias		: fDepthBias REGISTER2(ps,c80);
EndConstantBufferDX10(rage_ShadowConstBuffer)
#if __PS3	
//	shared float4 gBounds			: fBounds REGISTER2(ps,c81);
	shared float4 gTopPlanes[3]		: TopPlanes REGISTER2(ps,c82);
	shared float4 gRightPlanes[3]	: RightPlanes REGISTER2(ps,c85);
	shared float4 gLeftPlanes[3]	: LeftPlanes REGISTER2(ps,c88);
#endif	
#endif

CBSHARED BeginConstantBufferPagedDX10(rage_ShadowSphereBuffer, b6)
	SHARED float4   gShadowSpheres[4] : ShadowSpheres   REGISTER2(ps,c81);
EndConstantBufferDX10(rage_ShadowSphereBuffer)

// the following variables are used for cube shadow maps
CBSHARED BeginConstantBufferPagedDX10(rage_ShadowConstBuffer1, b7)
SHARED float gGeometryScale : GeometryScale REGISTER(c42);
SHARED float2 gShadowCollectorTexelSize : ShadowCollectorTexelSize REGISTER(c43);
SHARED float3 gPointLightPosition : PointLightPosition REGISTER(c44);
SHARED float gPointLightAttenuation : PointLightAttenuation REGISTER(c45);
SHARED float gLightAttenuationEnd : LightAttenuationEnd REGISTER(c46);
EndConstantBufferDX10(rage_ShadowConstBuffer1)

#if USE_GLOBAL_SHADOWLIGHTMTX
BeginSharedSampler(sampler2D, BlurTexture, BlurSampler, BlurTexture, s14)
string UIName="Blur Texture";
ContinueSharedSampler(sampler2D, BlurTexture, BlurSampler, BlurTexture, s14)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
#if __PS3	
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
	MIPFILTER = POINT;
#elif __XENON
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
	MIPFILTER = POINT;
#endif	
EndSharedSampler;
#endif


// sampler for cascaded shadow maps 
BeginSharedSampler(sampler2D,DepthTexture0,DepthTextureSampler0,DepthTexture0, s15)
ContinueSharedSampler(sampler2D,DepthTexture0,DepthTextureSampler0,DepthTexture0, s15)
#if __PS3
	AddressU  = BORDER;
	AddressV  = BORDER;
	AddressW  = BORDER;
	BorderColor = 0xffffffff;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = POINT;
	TEXTUREZFUNC= TEXTUREZFUNC_ALWAYS;
#elif __XENON || RSG_PC || RSG_DURANGO || RSG_ORBIS
// clamp mode is necessary for bigger filter kernels running over a texture atlas
// in the pixel shader each texture in the texture atlas is addressed with 0..1 
// so clamping the value range actually works for each texture in the texture atlas
	AddressU  = BORDER;
	AddressV  = BORDER;
	AddressW  = BORDER;
	BorderColor = 0xffffffff;
#if __SHADERMODEL >= 40
	MaxLOD = 0;
#endif
	MIPFILTER = NONE;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
#endif
EndSharedSampler;

// sampler for cube shadow maps
BeginSharedSampler(samplerCUBE, cubeMap, CubeShadowMapSampler, CubeSphadowMap, s14)
    //string UIName = "Cube Map";
ContinueSharedSampler(samplerCUBE, cubeMap, CubeShadowMapSampler, CubeShadowMap, s9)
    AddressU  = CLAMP;        
    AddressV  = CLAMP;
    AddressW  = CLAMP;
#if __SHADERMODEL >= 40
	MaxLOD = 0;
#endif
	MIPFILTER = NONE;
#if __XENON	
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC; 
	MAXANISOTROPY = 16.0f;
#elif __PS3	
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC; 
	MAXANISOTROPY = 16;
#else
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;


BeginSampler(sampler2D, AmbientOccTexture,AmbientOccSampler,AmbientOccTex)
ContinueSampler(sampler2D,AmbientOccTexture,AmbientOccSampler,AmbientOccTex)
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D, LightOcclusionTex,LightOcclusionSampler,LightOcclusionTex)
ContinueSampler(sampler2D,LightOcclusionTex,LightOcclusionSampler,LightOcclusionTex)
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D, PRTOccTexture,PRTOccSampler,PRTOccTex)
ContinueSampler(sampler2D,PRTOccTexture,PRTOccSampler,PRTOccTex)
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		LOD_BIAS = 0;
EndSampler;

// shadow collector sampler
BeginSharedSampler(sampler, ShadowCollectorTexture, ShadowCollectorSampler, ShadowCollectorTex, s13)
ContinueSharedSampler(sampler,ShadowCollectorTexture, ShadowCollectorSampler, ShadowCollectorTex, s13)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIPFILTER = NONE;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
EndSharedSampler;

float3 ShadowCollectorGetScreenPos( float4 pos )
{
	float3 ScreenPos;
	ScreenPos.x = (pos.x * 0.5f + pos.w * 0.5f);
	ScreenPos.y = (pos.w * 0.5f - pos.y * 0.5f);
	ScreenPos.z = pos.w;
	return ScreenPos;
}

float ShadowCollectorGetShadowTexCoord( float2 TexCoord )
{
#if __PS3
	// On PS3 we can't use a L8 texture for the shadow collector
	// because it doesn't support depth testing. The next best option,
	// memory wise is a R5G6B5 texture, and we copy the shadow term
	// into green channel which has the highest precision
	return tex2D( ShadowCollectorSampler, TexCoord).g;
#else
	return tex2D( ShadowCollectorSampler, TexCoord).r;
#endif	
}

float ShadowCollectorGetShadowScreenPos( float3 ScreenPos )
{
	return ShadowCollectorGetShadowTexCoord(ScreenPos.xy /  ScreenPos.z);
}

float ShadowCollectorGetShadowVPos( float2 VPos )
{
	return ShadowCollectorGetShadowTexCoord(VPos * gShadowCollectorTexelSize);
}

half4 CubeShadowMapPackMoments(half2 inMoments)
{
#if __PS3
	return (unpack_4ubyte(pack_2half(inMoments)));
#else
	return inMoments.xyxy;
#endif
}

#endif //__RAGE_SHADOWMAP_COMMON_FXH

