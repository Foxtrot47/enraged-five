
//-----------------------------------------------------
//
//	TITLE
//		Spherical harmonic lighting rountines
//

#ifndef SPHERICAL_LIGHT_H
#define SPHERICAL_LIGHT_H

#include "rage_common.fxh"

float3 SphericalLightCoefficients[9]: SphericalLightCoefficients;

float		SphericalAmbientOn: SphericalAmbientOn;

//
//	PURPOSE 
//		To apply 3rd order spherical harmonic lighting  to a point object 
//
float3 sphGetSphericalLightValue3rdOrder( float3 dirn )
{
	float3 d2 = dirn * dirn;
	float f1 = 3.0f * d2.z - 1.0f;		// on xbox these instructions should pair nicely
	float f2 = d2.x - d2.y;
	float3 dirnSecond = dirn.xzy * dirn.zyx;

	float3 result = SphericalLightCoefficients[1] * dirn.x + SphericalLightCoefficients[0];
	result = SphericalLightCoefficients[2] * dirn.y + result;
	result = SphericalLightCoefficients[3] * dirn.z + result;
	result = SphericalLightCoefficients[4] * dirnSecond.x + result;
	result = SphericalLightCoefficients[5] * dirnSecond.y + result;
	result = SphericalLightCoefficients[6] * dirnSecond.z + result;
	result = SphericalLightCoefficients[7] * f1 + result;
	result = SphericalLightCoefficients[8] * f2 + result;
	return result;
}

float3 sphGetSphericalLightValue3rdOrderWithVis2ndOrder( float3 dirn, float4 vis  )
{
	float3 d2 = dirn * dirn;
	float f1 = 3.0f * d2.z - 1.0f;		// on xbox these instructions should pair nicely
	float f2 = d2.x - d2.y;
	float3 dirnSecond = dirn.xzy * dirn.zyx;

	float3 result = SphericalLightCoefficients[1] * dirn.x * vis.x + SphericalLightCoefficients[0] * vis.w;
	result = SphericalLightCoefficients[2] * dirn.y * vis.y + result;
	result = SphericalLightCoefficients[3] * dirn.z * vis.z + result;
	result = SphericalLightCoefficients[4] * dirnSecond.x + result;
	result = SphericalLightCoefficients[5] * dirnSecond.y + result;
	result = SphericalLightCoefficients[6] * dirnSecond.z + result;
	result = SphericalLightCoefficients[7] * f1 + result;
	result = SphericalLightCoefficients[8] * f2 + result;
	return result;
}

//
//	PURPOSE 
//		To apply 3rd order spherical harmonic lighting  to a point object  on a sphere
//		so that it applys it over the complete object better.
//
// -- TODO
float3 SphericalBounceLightCentre : SphericalBounceLightCentre;
float  SphericalBounceOneOverLightRadiusByRatio : SphericalBounceOneOverLightRadiusByRatio;

float3 sphGetSphericalBounceLightValue3rdOrder( float3 dirn , float3 wpos )
{
	float3 relPos =(  wpos - SphericalBounceLightCentre ) * SphericalBounceOneOverLightRadiusByRatio;
	float3 normal = normalize ( dirn + relPos );
	return sphGetSphericalLightValue3rdOrder( normal );
}



//----------- trying to force the compiler to calculate this
#define  NORMALIZATION  4.0f* PI/3.0f
//----------------------------------------------------------------------------------------

float4 ConvertDirToSH2( float3 dir )
{
	const float dcConst = 1.0f/(2.0f*sqrt( PI));
	const float linearConst = sqrt(3.0f)/(2.0f*sqrt( PI));
	float4	SH2constants = float4( -linearConst, linearConst, -linearConst, dcConst   ) * NORMALIZATION;

	float4 convolve = float4( dir.yzx, 1.0f ) * SH2constants;
	return convolve;
}

float4 GetSH2Constants()
{
	const float dcConst = 1.0f/(2.0f*sqrt( PI));
	const float linearConst = sqrt(3.0f)/(2.0f*sqrt( PI));
	float4	SH2constants = float4( -linearConst, linearConst, -linearConst, dcConst   ) * NORMALIZATION;

	return SH2constants;
}
	
float GetSphericalLightVis2ndOrder( float3 dirn, float4 vis )
{	
	return dot( ConvertDirToSH2( dirn ) , vis );
}

#define	DECLARE_SH2_COLOR		float4 red: TEXCOORD5; float4 green: TEXCOORD6; float4 blue: TEXCOORD7;

#define GET_SH2_COLOR(sh)			OUT.red = sh.red; OUT.green = sh.green; OUT.blue = sh.blue;
#define SET_SH2_COLOR(sh)			sh.red = IN.red; sh.green = IN.green; sh.blue = IN.blue;

struct SH2Color
{
	float4	red;
	float4	green;
	float4	blue;
};



SH2Color AddSphericalLight2ndOrder( float3 col, float3 dir, SH2Color sh )
{	
	SH2Color res;
	float4 convolve = ConvertDirToSH2( dir);
	res.red = convolve * col.x + sh.red;
	res.green = convolve * col.y + sh.green;
	res.blue = convolve * col.z + sh.blue;
	return res;
}

float3 CalculateSphericalLight2ndOrder( float4 visSH, SH2Color sh )
{
	float3 res = float3( dot( visSH, sh.red ), dot( visSH, sh.green ), dot( visSH, sh.blue ) );
	return saturate( res );
}

float GetSphericalLightVis3rdOrder(   float3 dirn, float3 shCoeff1, float3 shCoeff2, float3 shCoeff3 )
{
	float3 d2 = dirn * dirn;
	float3 f =float3(  3.0f * d2.z - 1.0f,   d2.x - d2.y, 1.0f );
	
	return dot( dirn.xyz, shCoeff1 ) + dot( dirn.xzy * dirn.zyx, shCoeff2 ) + dot( f, shCoeff3 );
}
#endif
