#ifndef RAGE_DUALPARABOLOID_FXH
#define RAGE_DUALPARABOLOID_FXH

#include "rage_common.fxh"

row_major shared float4x4 gDpBasisMtx : gDpBasisMtx	REGISTER(c48);
float gDpMapFarClip = 2000.0f;

float4 DualParaboloidPosTransform(float3 localPos)
{
	float4 pos = mul(float4(localPos, 1.0f), gWorldViewProj);
	float preDepth = pos.z;
	pos.xyz /= abs(pos.w);

	float posMag = length(pos.xyz);
	pos.xyz /= posMag;

	pos.z += 1.0f;
	pos.xy /= pos.z;

	pos.z = preDepth / gDpMapFarClip;
	pos.w = 1.0f;

	pos.x *= 0.5f;

	if (gDpBasisMtx[0][0] >= 0.75f)
		pos.x += 0.5f;
	else
		pos.x -= 0.5f;

	return pos;
}

float2 DualParaboloidTexCoord(float3 reflection, float scale)
{
	reflection = mul(reflection, (float3x3)gDpBasisMtx);

	static const float4 invertChannels = { 1.0f, 1.0f, -1.0f, -1.0f };
	float4 fbCoord = reflection.xyxy / (2.0f * (1 + reflection.zzzz * invertChannels)) * scale + 0.5f;
	fbCoord.xz = 0.5f - fbCoord.xz * 0.5f;
	fbCoord.z += 0.5f;

	float2 useCoord;
	if (reflection.z > 0.0f)
		useCoord = fbCoord.xy;
	else
		useCoord = fbCoord.zw;

	return useCoord;
}

float3 DualParaboloidTexLookup(sampler2D s, float3 reflection, float scale)
{
	return tex2D(s, DualParaboloidTexCoord(reflection, scale)).rgb;
}

#endif // RAGE_DUALPARABOLOID_FXH
