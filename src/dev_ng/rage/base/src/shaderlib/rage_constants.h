#ifndef RAGE_CONSTANTS_H
#define RAGE_CONSTANTS_H

// This code is shared between the runtime and shader source code
// All _SIZE values are in number of float4's consumed.

#define RAGE_MOTIONBLUR_MRT	1
#define RAGE_ENCODEOPAQUECOLOR (0 && __PS3)

#define RAGE_MAX_CLIPPLANES		1

#define MATRIX_BASE				0
#define MATRIX_COUNT			4
#define MATRIX_SIZE				16

#define GLOBALS_BASE			0
#define GLOBALS_SIZE			64

#define SKINNING_BASE			64

#define SKINNING_COUNT_PC_DX9	32
#define SKINNING_COUNT_DX11		255
#define SKINNING_COUNT_XENON	48
#define SKINNING_COUNT_PS3		48
#define SKINNING_COUNT_PSP2		48

#if __WIN32PC || RSG_DURANGO || RSG_ORBIS
#define MAX_NUM_CBUFFER_INSTANCING		6	// matching cascade viewport num for now. it can be increased to whatever value we want.
#define CBINSTANCING_BASE				160
#define CBINST_WORLDVIEWPROJ_BASE		160
#define CBINST_VIEWINV_BASE				184	//CBINST_WORLDVIEWPROJ_BASE + 4 * MAX_NUM_CBUFFER_INSTANCING

#define CBINST_WORLD_BASE				208	//CBINST_VIEWINV_BASE + 4 * MAX_NUM_CBUFFER_INSTANCING
#define CBINST_OPT_INDEX				212

#if defined(__SHADERMODEL) && __SHADERMODEL >= 40
#define SKINNING_COUNT	SKINNING_COUNT_DX11
#elif __WIN32PC && __64BIT
#define SKINNING_COUNT	SKINNING_COUNT_DX11
#else
#define SKINNING_COUNT	SKINNING_COUNT_PC_DX9
#endif

#elif __XENON
#define SKINNING_COUNT	SKINNING_COUNT_XENON
#elif __PS3
#define SKINNING_COUNT	SKINNING_COUNT_PS3
#elif __PSP2
#define SKINNING_COUNT	SKINNING_COUNT_PSP2
#endif

#if __RESOURCECOMPILER && !__64BIT
#define SKINNING_COUNT_FOR_PLATFORM ((g_sysPlatform==platform::WIN32PC || g_sysPlatform==platform::WIN64PC || g_sysPlatform==platform::DURANGO || g_sysPlatform==platform::ORBIS)? SKINNING_COUNT_PC_DX9 : SKINNING_COUNT_XENON)
#else
#define SKINNING_COUNT_FOR_PLATFORM SKINNING_COUNT
#endif

#define CASCADE_SHADOW_TEXARRAY			 0//1
#define CASCADE_SHADOWS_COUNT            4
#endif // RAGE_CONSTANTS_H
