
#ifndef RAGE_SPECULAR_FXH
#define RAGE_SPECULAR_FXH

float3 specGetReflectedView( float3 pos, float3 normal )
{
	//float3 view = -gViewInverse[2].xyz;// 
	float3 view = normalize( pos  - gViewInverse[3].xyz);
	return reflect( view, normal );
}

float4 specSlickApproxPhong( float4 t )
{
	const float  n = 32.0f;
	const float n1 = ( 1.0f - n );
	return saturate( t / ( n1  * t + n ) );
}

float4 specSpecularPow( float t, float3 M, float3 ldir1, float3 ldir2, float3 ldir3, float3 ldir4 )
{
	float4 spec;
	spec.x =  saturate( dot( M, ldir1 ) );
	spec.y =  saturate(dot( M, ldir2 ) );
	spec.z =  saturate(dot( M, ldir3 ) );
	spec.w =  saturate(dot( M, ldir4 ) );

	return saturate( ragePow( spec, t ) );
}
float4 specSpecularSlick( float t,  float3 M, float3 ldir1, float3 ldir2, float3 ldir3, float3 ldir4 )
{
	float4 spec;
	spec.x =  saturate( dot( M, ldir1 ) );
	spec.y =  saturate(dot( M, ldir2 ) );
	spec.z =  saturate(dot( M, ldir3 ) );
	spec.w =  saturate(dot( M, ldir4 ) );

	return specSlickApproxPhong( spec );	
}
float4 specSpecular3( float t, float3 M, float4 ldirX, float4 ldirY, float4 ldirZ )
{
	float4 spec =  ldirX * M.x;
	spec =  ldirY * M.y + spec;
	spec =  ldirY * M.z + spec;
	//return specSlickApproxPhong( spec );	
	return saturate( ragePow( spec, t ) );
}

float4 specSpecularBlin4( float t, float3 N, float3 V, float4 ldirX, float4 ldirY, float4 ldirZ )
{
	float4 HX = V.xxxx + ldirX;
	float4 HY = V.yyyy + ldirY;
	float4 HZ = V.zzzz + ldirZ;
	
	float4 D = HX * HX;
	D = HY * HY + D;
	D = HZ * HZ + D;
	D = rsqrt( D );
	
	float4 spec =  HX * N.x;
	spec =  HY * N.y + spec;
	spec =  HZ * N.z + spec;
	return saturate( ragePow( spec * D, t ) );
}
float4 specSpecularBlinSlick4( float t, float3 N, float3 V, float4 ldirX, float4 ldirY, float4 ldirZ )
{
	float4 HX = V.xxxx + ldirX;
	float4 HY = V.yyyy + ldirY;
	float4 HZ = V.zzzz + ldirZ;
	
	float4 D = HX * HX;
	D = HY * HY + D;
	D = HZ * HZ + D;
	D = rsqrt( D );
	
	float4 spec =  HX * N.x;
	spec =  HY * N.y + spec;
	spec =  HZ * N.z + spec;
	
	float  n =  t;
	float n1 = ( 1.0f - t );///( 1.0f - n );
	
	float4 recipocal = n1 * spec * D + n ;
	
	return saturate( spec / recipocal );
	
}
float fresnelSlick( float VdotN, float rollOff )
{
	return ( 1. - rollOff ) + ( rollOff ) * ragePow( 1.0 - VdotN, 5 );
}
float4 specSpecularBlinn( float t, float3 N, float3 V, float3 ldir1, float3 ldir2, float3 ldir3, float3 ldir4 )
{
	float3 H1 = normalize( V + ldir1 );
	float3 H2 = normalize( V + ldir2 );
	float3 H3 = normalize( V + ldir3 );
	float3 H4 = normalize( V + ldir4 );

	float4 spec;
	spec.x = saturate( dot( H1, N ) );	
	spec.y = saturate( dot( H2, N ) );	
	spec.z = saturate( dot( H3, N ) );	
	spec.w = saturate( dot( H4, N ) );	
	return saturate( ragePow( spec, t ) );
}

#endif

