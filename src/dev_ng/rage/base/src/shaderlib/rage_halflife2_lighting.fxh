
#ifndef _RAGE_HALF_LIFE2_LIGHTING
#define _RAGE_HALF_LIFE2_LIGHTING

// PURPOSE : Half1
// From the source half life lighting paper  http://www.aut.bme.hu/PORTAL/Default/DocDownload.aspx?DocId=32c4b791-4d5d-4c99-9aad-9011cf865e62&CultureId=16df90ec-fcf2-466d-8f3e-8c4057561621.

const float3 HalfLifeRed = float3( 0.81649658092772603273242802490196f, 0.0f, 0.57735026918962576450914878050196f ); //  float3( 0.0f, 0.0f, 1.0f );
const float3 HalfLifeGreen = float3( -0.40824829046386301636621401245098f, 0.70710678118654752440084436210485f, 0.57735026918962576450914878050196f );
const float3 HalfLifeBlue = float3( -0.40824829046386301636621401245098f, -0.70710678118654752440084436210485f, 0.57735026918962576450914878050196f );
 
 struct hVals
 {
	 float3 v1;
	 float3 v2;
	 float3 v3;
 };

hVals halflife_GetTriNormals()
{
	hVals h;
	h.v1 = HalfLifeRed;
	h.v2 = HalfLifeGreen;
	h.v3 = HalfLifeBlue;
	return h;
}
hVals halflife_Modulate( hVals h, float3 col )
{
	h.v1 *= col;
	h.v2 *= col;
	h.v3 *= col;
	return h;
}
void halflife_Get( hVals h, out float3 c1, out float3 c2, out float3 c3 )
{
	c1 = h.v1;
	c2 = h.v2;
	c3 = h.v3;
}
float3 GetHL2Color( float3 normal, float3 c1, float3 c2, float3 c3 )
{
	return saturate(dot( HalfLifeRed, normal )) * c1 + saturate(dot( HalfLifeGreen, normal )) * c2 +  saturate(dot( HalfLifeBlue, normal )) * c3;
}
float3 GetHL2Color( float3 normal, hVals cols  )
{
	return dot( HalfLifeRed, normal ) * cols.v1 + dot( HalfLifeGreen, normal ) * cols.v2 +  dot( HalfLifeBlue, normal ) * cols.v3;
}

float2 rotateVecZ( float2 v , float4 angles )
{
	return float2( dot( angles.xy , v.xy ), dot( angles.zw , v.xy )  );
}

hVals halflife_RotateZ( hVals h , float angle )
{
	float sinTheta, cosTheta;
	sincos( angle, sinTheta, cosTheta);	
	
	float4 ang = float4( cosTheta, -sinTheta, sinTheta, cosTheta );
	h.v1.xy = rotateVecZ( h.v1.xy, ang );
	h.v2.xy = rotateVecZ( h.v2.xy, ang );
	h.v3.xy = rotateVecZ( h.v3.xy, ang );

	return h;
}

//------------------- bounce lighting using halfLife2 format

float3 CalculateSpotHL2( float3 n, float3 pos, float3 normal1, float3 normal2, float3 normal3, float4 lpos1, float4 ldir, float4 col1 )
{
	float3 d1 = lpos1.xyz - pos.xyz;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 dir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w * recipicol  * recipicol);	// 1 instruct now quadratic
	
	float3 light = float3( saturate( dot( dir.xyz, normal1.xyz ) ),
							saturate( dot( dir.xyz, normal2.xyz ) ),
							saturate( dot( dir.xyz, normal3.xyz ) ) );
	light *= atten;
	light *= saturate( dot( dir.xyz, n.xyz ) * 32.0f );
	
	float spot = dot( ldir.xyz, -dir.xyz );
	light *= saturate(  spot * ldir.w  + lpos1.w  );
	return light;
}

#endif
