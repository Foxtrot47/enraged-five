

float4 ambXPos = {0.05f, 0.07f, 0.07f, 1.0f};
float4 ambXNeg = {0.157f, 0.157f, 0.157f, 1.0f};
float4 ambYPos = {0.2f, 0.2f, 0.2f, 1.0f};
float4 ambYNeg = {0.03f, 0.03f, 0.03f, 1.0f};
float4 ambZPos = {0.157f, 0.157f, 0.157f, 1.0f};
float4 ambZNeg = {0.157f, 0.157f, 0.157f, 1.0f};


float4 CalcAmbient(float3 normal)
{
	float3 nInterp = normal * 0.5f + 0.5f;

	float4 amb = {0.0f, 0.0f, 0.0f, 0.0f};

#if 1
	amb += lerp(ambXNeg, ambXPos, nInterp.x);
	amb += lerp(ambYNeg, ambYPos, nInterp.y);
	amb += lerp(ambZNeg, ambZPos, nInterp.z);
#else
	float3 nSq = normal * normal;

	amb += normal.x > 0 ? ambXPos * nSq.x : ambXNeg * nSq.x;
	amb += normal.y > 0 ? ambYPos * nSq.y : ambYNeg * nSq.y;
	amb += normal.z > 0 ? ambZPos * nSq.z : ambZNeg * nSq.z;
#endif

	return float4(amb.xyz, 1.0f);
}
