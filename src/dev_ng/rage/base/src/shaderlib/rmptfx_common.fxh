//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// rmptfx_common.fxh
// 
// All rmptfx shaders should include this header.
// 
// POSTPROCESS_VS and POSTPROCESS_PS should be called
// at the end of the vertex and pixel shaders, respectively.
//  
// USABLE ANNOTATIONS
//		string UIName = "variable_name"		Create UI controls for this variable
//		string UIAssetName = "file_name"	Specifies a default texture for a sampler
//		string UIHint = "Keyframeable"		Specifies a variable keyframeable over life.  
//											The variable will be passed in per vertex in custom1, custom2, custom3, etc., 
//											in the vertex shader input structure (rmptfxVS).
//											They will be packed in starting from custom2.y.		
//		UIHint = "Bool"						Uses a toggleable checkbox for rag UI						
//		float UIMin = 0.0;					UI - min value
//		float UIMax = 1.0;					UI - maximum value
//		float UIStep = 0.01;				UI - slider step amount
//
// RECOGNIZED GLOBAL VARIABLES
// The program will feed in the values for the following global variables if they're defined in the shader.
//
//	float		CurrentTime:CurrentTime;							// Time elapsed in seconds
//	float2		NearFarPlane:NearFarPlane;							// near plane, far plane
//	float3		EffectPos:EffectPos;								// Position of the instance effect
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
#ifndef RAGE_RMPTFX_COMMON_FXH
#define RAGE_RMPTFX_COMMON_FXH

#ifndef __RAGE_COMMON_FXH
	#define NO_SKINNING
	#include "rage_common.fxh"
#endif	// __RAGE_COMMON_FXH

#define POSTPROCESS_PS(out,in) \
	rage_rmptfx_PostProcessPS(out,in)

// To avoid compiler warnings of uninitialized variables
#define POSTPROCESS_VS(out,in) \
	out.normalTexCoord = 0;\
	out.internal = 0;\
	out.custom1=0;\
	out.custom2=0;\
	rage_rmptfx_PostProcessVS(out,in)
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Programmed Variables	///////////////////////////////////////////////////////////////////
// Depth map
#if !HACK_GTA4
BeginSharedSampler(sampler,DepthMap,DepthMapTexSampler,DepthMap,s15)	// Depth map
ContinueSharedSampler(sampler,DepthMap,DepthMapTexSampler,DepthMap,s15) 
   MinFilter = POINT;
   MagFilter = POINT;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSharedSampler;
#endif // !HACK_GTA4

#pragma constant 64

BeginConstantBufferDX10( rmptfx_common_locals )

float	HybridAdd;								// Hybrid Add selected

float gBiasToCamera : BiasToCamera
	<
		string UIName = "Move particle closer to camera";
		float UIMin = -1000.0;
		float UIMax = 1000.0;
		float UIStep = 0.01;
	> = 0.0f;

float gHybridAddRatio : HybridAddRatio
	<
		string UIName = "Ratio between additive and normal";
		string UIHint = "Keyframeable";
		string nostrip = "nostrip";
		float UIMin = 0.0;
		float UIMax = 1.0;
		float UIStep = 0.01;
	> = 0.0f;
	
EndConstantBufferDX10( rmptfx_common_locals )

// Shader-defined Variables	/////////////////////////////////////////////////////////////////////////////////////
// TEXTURE			///////////////////////////////////////////////////////////////////////////////////////
#ifdef HDR_TEXTURE_PARTICLES
	BeginSamplerHDR(sampler2D,DiffuseTex2,DiffuseTexSampler,DiffuseTex2)
#else	
	BeginSampler(sampler2D,DiffuseTex2,DiffuseTexSampler,DiffuseTex2)
#endif	
	string UIName = "Texture Map";
	ContinueSampler(sampler2D,DiffuseTex2,DiffuseTexSampler,DiffuseTex2)
	    MinFilter = LINEAR;
	    MagFilter = LINEAR;
	    MipFilter = NONE;
	    AddressU  = CLAMP;        
		AddressV  = CLAMP;
	    
	EndSampler;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Input/Output Structure
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct rmptfxVS {
    float4 center 		    : POSITION0;					// center position, life
    float3 billboard		: NORMAL0;						// scale X, scale Y, theta
    float4 color			: COLOR0;						// color
    float4 texCoord         : TEXCOORD0;					// u, v, u2,v2
    float4 normalTexCoord   : TEXCOORD1;					// u, v, empty, empty
	float4 velLife			: TEXCOORD2;					// velocity, life, blendrate, empty

    float4 custom1			: TEXCOORD3;					// shader-defined keyframeable vars
    float4 custom2			: TEXCOORD4;					// shader-defined keyframeable vars

 /* 
  * If more custom variables are added, ptcore/draw.h must be modified accordingly.
  *
    float4 custom3			: TEXCOORD4;					// shader-defined keyframeable vars
    float4 custom4			: TEXCOORD5;					// shader-defined keyframeable vars
    float4 custom5			: TEXCOORD6;					// shader-defined keyframeable vars
    float4 custom6			: TEXCOORD7;					// shader-defined keyframeable vars
  */
};

struct rmptfxPSBase {    
    DECLARE_POSITION(pos)									// position
    float4 color			: COLOR0;						// color
    float4 texCoord         : TEXCOORD0;					// u, v, u2,v2
    float4 normalTexCoord   : TEXCOORD1;					// u, v, empty,empty

	// Variables below are set by POSTPROCESS_VS
	float4 internal			: TEXCOORD2;					// x=rate to blend texture frames
    float4 custom1			: TEXCOORD3;					// life, colortune.x, colortune.y, colortune.z
    float4 custom2			: TEXCOORD4;					// colortune.w
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rage_rmptfx_ComputeDiffusePixelColor(out float4 color,rmptfxPSBase BASE)
{
	//Determine color from diffuse texture (takes smooth blending between animation frames into account)
	float4 texColor  = tex2D(DiffuseTexSampler,BASE.texCoord.xy);
	float4 texColor2 = tex2D(DiffuseTexSampler,float2(BASE.texCoord.z,BASE.texCoord.w));
	float4 compTexColor;
	float blend = saturate(BASE.internal.x);
	compTexColor.x = lerp(texColor.x,texColor2.x,blend);
	compTexColor.y = lerp(texColor.y,texColor2.y,blend);
	compTexColor.z = lerp(texColor.z,texColor2.z,blend);
	compTexColor.w = lerp(texColor.w,texColor2.w,blend);
	color = compTexColor;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rage_rmptfx_BiasToCamera(inout float3 pos)
{
	//Bias Towards the camera
	float3 cameraBias = gViewInverse[3].xyz - pos;
	normalize(cameraBias);
	pos += cameraBias*(gBiasToCamera*0.1f);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rage_rmptfx_PostProcessPS(inout float4 color, rmptfxPSBase BASE)
{	
	if(HybridAdd)
	{
		color.xyz*=color.w;
		color.w*=(1-BASE.custom1.x);
	}
}

/////////////////////////////////////////////////////////////////////////////////////
void rage_rmptfx_PostProcessVS(inout rmptfxPSBase BASE, rmptfxVS IN)
{	
	BASE.normalTexCoord = IN.normalTexCoord;
	BASE.internal = float4(IN.velLife.z,0.0f,0.0f,0.0f);
	
	// Custom shader-defined vars
	BASE.custom1 = IN.custom1;
	BASE.custom2 = IN.custom2;
}

#endif
