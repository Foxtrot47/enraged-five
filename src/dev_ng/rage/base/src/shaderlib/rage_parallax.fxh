


float fBumpHeightScale
<
	string UIName = "bump height scale";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.0025;
> = 0.02f;

float fPerspectiveBias
<
	string UIName = "perspective bias";
	float UIMin = -10.0;
	float UIMax = 10.0;
	float UIStep = 0.01;
> = 1.0f;

float ShadowBumpHeightScale
<
	string UIName = "shadow bump height scale";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.0025;
> = 0.02f;

float shadowScale
<
    string UIName = "Shadow Scale";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01f;
> = 0.4;

float fShadowSoftening : fShadowSoftening
<
    string UIName = "Shadow Softening";
    float UIMin = -1.0;
    float UIMax = 1.0;
    float UIStep = 0.01f;
> = 0.4;




#define DEFINE_ParallaxInfo			float4  LightDirectionTS : TEXCOORD6;  float3 ViewTS : TEXCOORD7;
#define WRITE_ParallaxInfo( info)	OUT.LightDirectionTS.xy = info.LightDirectionTS.xy; OUT.ViewTS = info.ViewTS; OUT.LightDirectionTS.zw = info.POMParallaxVec.xy;
#define READ_ParallaxInfo( info)	info.LightDirectionTS.xy = IN.LightDirectionTS.xy; info.ViewTS = IN.ViewTS; info.POMParallaxVec.xy = IN.LightDirectionTS.zw;



struct parallaxVSOutput
{
	float2 LightDirectionTS; //first light direction in tangent space, used for self shadowing
	float3 ViewTS; //view vector in tangent space, used for low res parallax mapping
	float2 POMParallaxVec; //parallax vector for POM parallax mapping
};


float3 CalcViewTS(float3 InTangent, float3 InBinormal, float3 InNormal, float3 WorldPos)
{
	//float3 tanView = normalize(mul(InTangent, (float3x3)gWorldInverseTranspose));
	//float3 binormView = normalize(mul(InBinormal, (float3x3)gWorldInverseTranspose));
	//float3 normView = normalize(mul(InNormal, (float3x3)gWorldInverseTranspose));

	float3 tanView = InTangent;
	float3 binormView = InBinormal;
	float3 normView = InNormal;
	
	float3 parallaxViewVec = (gViewInverse[3].xyz - WorldPos);

	// move viewer vector to tangent space
	float3x3 worldToTangentSpace;
	worldToTangentSpace[0] = tanView;
	worldToTangentSpace[1] = binormView;
	worldToTangentSpace[2] = normView;
	return (mul(worldToTangentSpace, normalize(parallaxViewVec)));
}

parallaxVSOutput CalcPOMVSInfo(float3 InTangent, float3 InBinormal, float3 InNormal, float3 WorldPos, int ShadowLightIdx)
{
	parallaxVSOutput OUT;
	
	//float3 tanView = normalize(mul(InTangent, (float3x3)gWorldInverseTranspose));
	//float3 binormView = normalize(mul(InBinormal, (float3x3)gWorldInverseTranspose));
	//float3 normView = normalize(mul(InNormal, (float3x3)gWorldInverseTranspose));
	
	float3 tanView = normalize(mul(InTangent, (float3x3)gWorld));
	float3 binormView = normalize(mul(InBinormal, (float3x3)gWorld));
	float3 normView = normalize(mul(InNormal, (float3x3)gWorld));


	float3 parallaxViewVec = -(gViewInverse[3].xyz - WorldPos);
	float3 lightVec = (gViewInverse[3].xyz - gLightPosDir[ShadowLightIdx].xyz);


    // move viewer vector to tangent space
	float3x3 worldToTangentSpace;
    worldToTangentSpace[0] = -tanView;
    worldToTangentSpace[1] = -binormView;
    worldToTangentSpace[2] = -normView;
    float3 ViewTS = mul(worldToTangentSpace, normalize(parallaxViewVec));
    OUT.ViewTS = ViewTS;
    
    float3 ViewLightVecTS = mul(worldToTangentSpace, normalize(lightVec));
    OUT.LightDirectionTS = (ViewLightVecTS.xy * ShadowBumpHeightScale);

	//============================================================//
	// Parallax occlusion direction vector and length computation //
	//============================================================//
	// Compute initial parallax displacement direction:
	float2 vParallaxDirection = normalize( ViewTS.xy );
	ViewTS.z = saturate(ViewTS.z);

	// Compute the length of parallax displacement vector
	// i.e. the amount of parallax displacement.
	// Can also do: / vViewTS.z for more offset limiting.
	float fParallaxLength = -sqrt( 1 - (ViewTS.z * ViewTS.z) );// / (ViewTS.z); 

	// Compute parallax bias parameter which allows
	// the artists control over the parallax perspective
	// bias via an artist editable parameter 'fPerspectiveBias'
	// Use view vector length in tangent space as an offset 
	// limiting technique as well:
	float fParallaxBias = fPerspectiveBias + (1 - fPerspectiveBias) * ( 2 * ViewTS.z - 1 );

	// Compute the actual reverse parallax displacement vector:
	OUT.POMParallaxVec.xy = -(vParallaxDirection * fParallaxLength * fBumpHeightScale) * fParallaxBias;
	return OUT;
}


struct parallaxPSOutput
{
	float2 OffsetTexCoord;
	float ShadowVal;
};

float2 CalcPOMTexOffset(float2 InTexCoord, float2 Parallax, sampler2D NormalSampler)
{
	float2 texCoord = InTexCoord;

	// Compute height map profile for 8 samples:
	// Note that the height map is contained in the alpha channel
	// of the input normal map:
	float h0 = tex2D( NormalSampler, texCoord - Parallax.xy * 1.000 ).a;
	float h1 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.875 ).a;
	float h2 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.750 ).a;
	float h3 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.625 ).a;
	float h4 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.500 ).a;
	float h5 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.375 ).a;
	float h6 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.250 ).a;
	float h7 = tex2D( NormalSampler, texCoord - Parallax.xy * 0.125 ).a;
	
	// Determine the section in the height profile that contains
	// a sample that is higher than the view vector. This determines
	// the correct displacement point:    
	float x;
	float y;
	float xh;
	float yh;

	if      ( h7 > 0.875 ) { x = 0.937; y = 0.938; xh = h7; yh = h7; }
	else if ( h6 > 0.750 ) { x = 0.750; y = 0.875; xh = h6; yh = h7; }
	else if ( h5 > 0.625 ) { x = 0.625; y = 0.750; xh = h5; yh = h6; }
	else if ( h4 > 0.500 ) { x = 0.500; y = 0.625; xh = h4; yh = h5; }
	else if ( h3 > 0.375 ) { x = 0.375; y = 0.500; xh = h3; yh = h4; }
	else if ( h2 > 0.250 ) { x = 0.250; y = 0.375; xh = h2; yh = h3; }
	else if ( h1 > 0.125 ) { x = 0.125; y = 0.250; xh = h1; yh = h2; }
	else                   { x = 0.000; y = 0.125; xh = h0; yh = h1; }

	
	// Compute the intersection between the view vector and the highest
	// sample point in the height profile:
	float fParallaxAmount = (x * (y - yh) - y * (x - xh))/((y - yh) - (x - xh));

	float2 vParallaxOffset = Parallax.xy * (1 - fParallaxAmount );

	// Adjust the texture coordinate using the parallax offset vector:
	return (texCoord - vParallaxOffset);
}

float2 CalcPOMTexOffsetOptimized(float2 InTexCoord, float2 Parallax, sampler2D NormalSampler)
{
	float2 texCoord = InTexCoord;

	// Compute height map profile for 8 samples:
	// Note that the height map is contained in the alpha channel
	// of the input normal map:
	float4 h1;
	float4 h2;

	const float4 compare1 = float4( 0.0f, 0.125f, 0.250f, 0.375f );
	const float4 compare2 = float4( 0.500f, 0.625f, 0.750f, 0.875f );
	
	float4 delta = Parallax.xyxy * compare1.yyyy;
	float4 shift = texCoord.xyxy - Parallax.xyxy * float4( 1.0f, 1.0f, 0.875, 0.875 );
	
	h1.x = tex2D( NormalSampler, shift.xy ).a;
	h1.y = tex2D( NormalSampler, shift.zw ).a;
	shift += delta;
	h1.z = tex2D( NormalSampler, shift.xy ).a;
	h1.w = tex2D( NormalSampler,  shift.zw ).a;
	shift += delta;
	h2.x = tex2D( NormalSampler,  shift.xy ).a;
	h2.y = tex2D( NormalSampler, shift.zw ).a;
	shift += delta;
	h2.z = tex2D( NormalSampler, shift.xy ).a;
	h2.w = tex2D( NormalSampler, shift.zw).a;
	
	// Determine the section in the height profile that contains
	// a sample that is higher than the view vector. This determines
	// the correct displacement point:    
	float4 mask1 = h1 > compare1;
	float4 mask2 = h2 > compare2;
	
	
	//float2 xy;
	//xy.y = ( dot( mask1, 1.0f ) + dot( mask2, 1.0f ) ) * 0.125f;
	//xy.x = xy.y - 0.125f;
	//xy = min( xy, float2( 0.937, 0.938f ) );
	float2 xy = mask1.yy  > 0.0f ?  compare1.yz : compare1.xy;
	xy =  mask1.zz > 0.0f  ? compare1.zw : xy;
	xy =  mask1.ww  > 0.0f  ?  float2( 0.375, 0.500f ) : xy;
	xy =  mask2.xx  > 0.0f ?  compare2.xy : xy;
	xy =  mask2.yy  > 0.0f  ?  compare2.yz : xy;
	xy =  mask2.zz  > 0.0f ?  compare2.zw  : xy;
	xy =  mask2.ww  > 0.0f  ?  float2( 0.937, 0.938f ) : xy;
	
	float2 xyh =  mask1.yy  > 0.0f  ?  h1.yz : h1.xy;
	xyh =  mask1.zz  > 0.0f  ?  h1.zw : xyh;
	xyh =  mask1.ww  > 0.0f  ?  float2( h1.w, h2.x) : xyh;
	xyh =  mask2.xx  > 0.0f  ?  h2.xy : xyh;
	xyh =  mask2.yy  > 0.0f ?  h2.yz : xyh;
	xyh =  mask2.zz  > 0.0f ?  h2.zw : xyh;
	xyh =  mask2.ww  > 0.0f ?  h2.ww : xyh;
	
	xyh = xy - xyh;
	xyh.x = -xyh.x;
	
	float fParallaxAmount = dot( xy.xy, xyh.yx ) / dot( xyh.yx, 1.0f);
	
	// Compute the intersection between the view vector and the highest
	// sample point in the height profile:
	//float fParallaxAmount = (x * (y - yh) - y * (x - xh))/((y - yh) - (x - xh));

	float2 vParallaxOffset = Parallax.xy * (1 - fParallaxAmount );

	// Adjust the texture coordinate using the parallax offset vector:
	return (texCoord - vParallaxOffset);
}
float2 CalcPOMTexOffsetOptimized4(float2 InTexCoord, float2 Parallax, sampler2D NormalSampler)
{
	float2 texCoord = InTexCoord;

	// Compute height map profile for 8 samples:
	// Note that the height map is contained in the alpha channel
	// of the input normal map:
	float4 h1;
	float4 h2;

	const float4 compare = float4( 0.0f, 0.25f, 0.5f, 0.75f );
	
	float4 delta = Parallax.xyxy * compare.xxxx;
	float4 shift = texCoord.xyxy - Parallax.xyxy * float4( 1.0f, 1.0f, 0.75, 0.75 );
	
	h1.x = tex2D( NormalSampler, shift.xy ).a;
	h1.y = tex2D( NormalSampler, shift.zw ).a;
	shift += delta;
	h1.z = tex2D( NormalSampler, shift.xy ).a;
	h1.w = tex2D( NormalSampler,  shift.zw ).a;
	
	// Determine the section in the height profile that contains
	// a sample that is higher than the view vector. This determines
	// the correct displacement point:    
	float4 mask = h1 > compare;
	
	float2 xy = mask.yy  > 0.0f ?  compare.yz : compare.xy;
	xy =  mask.zz > 0.0f  ? compare.zw : xy;
	xy =  mask.ww  > 0.0f  ?  float2( 0.75, 1.0f ) : xy;
	
	float2 xyh =  mask.yy  > 0.0f  ?  h1.yz : h1.xy;
	xyh =  mask.zz  > 0.0f  ?  h1.zw : xyh;
	xyh =  mask.ww  > 0.0f  ?  float2( h1.w, 1.0f) : xyh;
	
	xyh = xy - xyh;
	xyh.x = -xyh.x;
	
	float fParallaxAmount = dot( xy.xy, xyh.yx ) / dot( xyh.yx, 1.0f);
	
	// Compute the intersection between the view vector and the highest
	// sample point in the height profile:
	float2 vParallaxOffset = Parallax.xy * (1 - fParallaxAmount );

	// Adjust the texture coordinate using the parallax offset vector:
	return (texCoord - vParallaxOffset);
}
float CalcPOMShadowVal(float2 texOffset, float2 InLightDirection, sampler2D NormalSampler)
{
	float2 inXY = InLightDirection;

	//============================================//
	// Soft shadow and self-occlusion computation //
	//============================================//

	//float3 TexCorrected = texSample.x * IN.Tangent + texSample.y * IN.Binormal;

	// Compute the blurry shadows (note that this computation takes into 
	// account self-occlusion for shadow computation):
	float sh0 =  tex2D( NormalSampler, texOffset).a;
	float shA = (tex2D( NormalSampler, texOffset + inXY * 0.88 ).a - sh0 - 0.88 ) *  1 * fShadowSoftening;
	float sh9 = (tex2D( NormalSampler, texOffset + inXY * 0.77 ).a - sh0 - 0.77 ) *  2 * fShadowSoftening;
	float sh8 = (tex2D( NormalSampler, texOffset + inXY * 0.66 ).a - sh0 - 0.66 ) *  4 * fShadowSoftening;
	float sh7 = (tex2D( NormalSampler, texOffset + inXY * 0.55 ).a - sh0 - 0.55 ) *  6 * fShadowSoftening;
	float sh6 = (tex2D( NormalSampler, texOffset + inXY * 0.44 ).a - sh0 - 0.44 ) *  8 * fShadowSoftening;
	float sh5 = (tex2D( NormalSampler, texOffset + inXY * 0.33 ).a - sh0 - 0.33 ) * 10 * fShadowSoftening;
	float sh4 = (tex2D( NormalSampler, texOffset + inXY * 0.22 ).a - sh0 - 0.22 ) * 12 * fShadowSoftening;

	// Compute the actual shadow strength:
	float fShadow = 1 - (saturate(max( max( max( max( max( max( shA, sh9 ), sh8 ), sh7 ), sh6 ), sh5 ), sh4 )) * shadowScale);
	return fShadow;
}

parallaxPSOutput CalcParallaxPSInfo(float2 InTexCoord, float2 ViewTS, sampler2D NormalSampler)
{
 	parallaxPSOutput OUT;

	//float Height = fBumpHeightScale * tex2D( HeightSampler, IN.Tex).x - fBumpHeightScale;
	float Height = fBumpHeightScale * tex2D(NormalSampler, InTexCoord).a - fBumpHeightScale;

	float2 TexCorrected = (Height * -ViewTS + InTexCoord);
	
	OUT.OffsetTexCoord = TexCorrected;
	OUT.ShadowVal = 1.0f;
	
	return OUT;
}
float2 ModifyTexParallax(float2 InTexCoord, float2 ViewTS, sampler2D NormalSampler)
{
	//float Height = fBumpHeightScale * tex2D( HeightSampler, IN.Tex).x - fBumpHeightScale * 0.5;
	float Height = fBumpHeightScale * tex2D(NormalSampler, InTexCoord).x - fBumpHeightScale * 0.5;

	float2 TexCorrected = (Height * ViewTS + InTexCoord);
	return TexCorrected;
}
float2 ModifyHeightParallax(float2 InTexCoord, float2 ViewTS, float h )
{
	float Scale = 0.00035f;
	float Height = Scale * h  - Scale * 0.5;
	float2 TexCorrected = (Height * ViewTS + InTexCoord);
	return TexCorrected;
}
parallaxPSOutput CalcPOMShadowPSInfo(float2 InTexCoord, float2 Parallax, float2 InLightDirection, sampler2D NormalSampler)
{
#if 1
	//===============================================//
	// Parallax occlusion mapping offset computation //
	//===============================================//

	//float3 L = normalize(gLightPosDir[0] - IN.WorldPos);
 	//float3 LightTS = mul(worldToTangentSpace, L);

 	parallaxPSOutput OUT;

 	OUT.OffsetTexCoord = CalcPOMTexOffset(InTexCoord, Parallax, NormalSampler);   //CalcPOMTexOffset(InTexCoord, Parallax, NormalSampler);
 	OUT.ShadowVal = CalcPOMShadowVal(OUT.OffsetTexCoord, InLightDirection, NormalSampler);
 	return OUT;

#else


	//float Height = fBumpHeightScale * tex2D( HeightSampler, IN.Tex).x - fBumpHeightScale;
	float Height = fBumpHeightScale * tex2D(NormalSampler, InTexCoord).a - fBumpHeightScale;

	float2 TexCorrected = (Height * -IN.ViewTS.xy + InTexCoord);
	
	OUT.OffsetTexCoord = TexCorrected;
	OUT.ShadowVal = 0.0f;
#endif
}
parallaxPSOutput CalcPOMPSInfo(float2 InTexCoord, float2 Parallax, float2 InLightDirection, sampler2D NormalSampler)
{
	//===============================================//
	// Parallax occlusion mapping offset computation //
	//===============================================//

	//float3 L = normalize(gLightPosDir[0] - IN.WorldPos);
 	//float3 LightTS = mul(worldToTangentSpace, L);

 	parallaxPSOutput OUT;

 	OUT.OffsetTexCoord = CalcPOMTexOffset(InTexCoord, Parallax, NormalSampler); // CalcPOMTexOffset// CalcPOMTexOffsetOptimized
 	OUT.ShadowVal = 1.0f;
 	return OUT;
}
