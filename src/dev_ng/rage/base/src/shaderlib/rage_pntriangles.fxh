// =============================================================================================== //
// Barycentric helpers.																			   //
// =============================================================================================== //

// U goes along edge A->B, V goes along edge A->C. W = 1 - U - V,
float4 rage_ComputeBarycentric(float3 WUV, float4 A, float4 B, float4 C)
{
	return WUV.x*A + WUV.y*B + WUV.z*C;
}

float3 rage_ComputeBarycentric(float3 WUV, float3 A, float3 B, float3 C)
{
	return WUV.x*A + WUV.y*B + WUV.z*C;
}

float2 rage_ComputeBarycentric(float3 WUV, float2 A, float2 B, float2 C)
{
	return WUV.x*A + WUV.y*B + WUV.z*C;
}

float rage_ComputeBarycentric(float3 WUV, float A, float B, float C)
{
	return WUV.x*A + WUV.y*B + WUV.z*C;
}


#define RAGE_COMPUTE_BARYCENTRIC(_coord, _patch, _component) rage_ComputeBarycentric(_coord, _patch[0]._component, _patch[1]._component, _patch[2]._component)

#if RAGE_USE_PN_TRIANGLES

struct rage_PNTri_ControlPoints
{
	float3 b300 : PN_300B; // (0, 0) = PatchPoints[0].
	float3 b030 : PN_030B; // (1, 0) = PatchPoints[1].
	float3 b003 : PN_003B; // (0, 1) = PatchPoints[2].

	// Edge PatchPoints[0] -> PatchPoints[1].
	float3 b210 : PN_210B; // (1/3, 0).
	float3 b120 : PN_120B; // (2/3, 0).

	// Edge PatchPoints[0] -> PatchPoints[2].
	float3 b201 : PN_201B; // (0, 1/3).
	float3 b102 : PN_102B; // (0, 2/3).

	// Edge PatchPoints[1] -> PatchPoints[2].
	float3 b021 : PN_021B; // (2/3, 1/3).
	float3 b012 : PN_012B; // (1/3, 2/3).
	
	// Mid point.
	float3 b111 : PN_111B; // (1/3, 1/3).
	
	float3 n200 : PN_200N; // (0, 0)
	float3 n020 : PN_020N; // (1, 0)
	float3 n002 : PN_002N; // (0, 1)
	
	float3 n110 : PN_110N; // (1/2, 0);
	float3 n101 : PN_101N; // (0,   1/2);
	float3 n011 : PN_011N; // (1/2, 1/2);
	
	float ControlPointBlendFactor : PN_CPBF;
};

struct rage_PNTri_PatchInfo
{
	float Edges[3] : SV_TessFactor;
	float Inside[1] : SV_InsideTessFactor;
	rage_PNTri_ControlPoints ControlPoints;
};


struct rage_PNTri_Vertex
{
	float3 Position;
	float3 Normal;
};


// =============================================================================================== //
// Evaluation functions.																		   //
// =============================================================================================== //


float3 rage_EvaluatePatchAt(rage_PNTri_PatchInfo Patch, float3 WUV)
{
	float W = WUV.x;
	float U = WUV.y;
	float V = WUV.z;
	
	float3 Ret = Patch.ControlPoints.b300*(W*W*W) + Patch.ControlPoints.b030*(U*U*U) + Patch.ControlPoints.b003*(V*V*V)
	+ 3*(Patch.ControlPoints.b210*(W*W*U) + Patch.ControlPoints.b120*(W*U*U) 
	+ Patch.ControlPoints.b201*(W*W*V) + Patch.ControlPoints.b102*(W*V*V) 
	+ Patch.ControlPoints.b021*(U*U*V) + Patch.ControlPoints.b012*(U*V*V))
	+ 6*Patch.ControlPoints.b111*W*U*V;
	
	return Ret;
}


float3 rage_EvaluatePatchNormalAt(rage_PNTri_PatchInfo Patch, float3 WUV)
{
	float W = WUV.x;
	float U = WUV.y;
	float V = WUV.z;
	
	float3 Ret = Patch.ControlPoints.n200*(W*W) + Patch.ControlPoints.n020*(U*U) + Patch.ControlPoints.n002*(V*V) +
	Patch.ControlPoints.n110*(W*U) + Patch.ControlPoints.n101*(W*V) + Patch.ControlPoints.n011*(U*V);
	Ret = normalize(Ret);
	return Ret; 
}


// ================================================================================================ //
// Patch/control point functions.																	//
// ================================================================================================ //


// Computes edge control points.
void rage_ComputeEdgeControlPoints(float ControlPointBlendFactor, float3 A, float3 B, float3 nA, float3 nB, out float3 B1Over3, out float3 B2Over3)
{
	B1Over3 = (2*A + B - ControlPointBlendFactor*dot(nA, B - A)*nA)/3;
	B2Over3 = (2*B + A - ControlPointBlendFactor*dot(nB, A - B)*nB)/3;
}


// Computes edge normal control points.
float3 rage_ComputeEdgeNormalControlPoints(float ControlPointBlendFactor, float3 A, float3 B, float3 nA, float3 nB)
{
	float3 E = B - A;
	float3 Sum = nA + nB;
	float EdotE = dot(E, E);
	
	float k = 2.0f*ControlPointBlendFactor*dot(E, Sum)/EdotE;
	float3 Ret = Sum - k*E;
	return normalize(Ret);
}


// Computes PN triangle control points.
void rage_ComputePNTriangleContolPoints(float ControlPointBlendFactor, rage_PNTri_Vertex PatchPoint0, rage_PNTri_Vertex PatchPoint1, rage_PNTri_Vertex PatchPoint2, out rage_PNTri_ControlPoints ControlPoints)
{
	ControlPoints.b300 = PatchPoint0.Position;
	ControlPoints.b030 = PatchPoint1.Position;
	ControlPoints.b003 = PatchPoint2.Position;

	rage_ComputeEdgeControlPoints(ControlPointBlendFactor, PatchPoint0.Position, PatchPoint1.Position, PatchPoint0.Normal, PatchPoint1.Normal, ControlPoints.b210, ControlPoints.b120);  
	rage_ComputeEdgeControlPoints(ControlPointBlendFactor, PatchPoint0.Position, PatchPoint2.Position, PatchPoint0.Normal, PatchPoint2.Normal, ControlPoints.b201, ControlPoints.b102);  
	rage_ComputeEdgeControlPoints(ControlPointBlendFactor, PatchPoint1.Position, PatchPoint2.Position, PatchPoint1.Normal, PatchPoint2.Normal, ControlPoints.b021, ControlPoints.b012);  

	float3 E = (ControlPoints.b210 + ControlPoints.b120 + ControlPoints.b201 + ControlPoints.b102 + ControlPoints.b021 + ControlPoints.b012)/6;
	float3 V = (ControlPoints.b300 + ControlPoints.b030 + ControlPoints.b003)/3;
	ControlPoints.b111 = E + (E-V)/2;
	
	ControlPoints.n200 = PatchPoint0.Normal;
	ControlPoints.n020 = PatchPoint1.Normal;
	ControlPoints.n002 = PatchPoint2.Normal;
	
	ControlPoints.n110 = rage_ComputeEdgeNormalControlPoints(ControlPointBlendFactor, PatchPoint0.Position, PatchPoint1.Position, PatchPoint0.Normal, PatchPoint1.Normal);
	ControlPoints.n101 = rage_ComputeEdgeNormalControlPoints(ControlPointBlendFactor, PatchPoint0.Position, PatchPoint2.Position, PatchPoint0.Normal, PatchPoint2.Normal);
	ControlPoints.n011 = rage_ComputeEdgeNormalControlPoints(ControlPointBlendFactor, PatchPoint1.Position, PatchPoint2.Position, PatchPoint1.Normal, PatchPoint2.Normal);
	
	ControlPoints.ControlPointBlendFactor = ControlPointBlendFactor;
}


#define PN_TRIANGLE_NEAR g_Rage_Tessellation_LinearScale.x
#define PN_TRIANGLE_FAR g_Rage_Tessellation_LinearScale.y
#define PN_TRIANGLE_MAX g_Rage_Tessellation_LinearScale.z

#define PN_TRIANGLE_PROJECTION_X g_Rage_Tessellation_ScreenSpaceErrorParams.x
#define PN_TRIANGLE_PROJECTION_Y g_Rage_Tessellation_ScreenSpaceErrorParams.y
#define PN_TRIANGLE_ALLOWED_ERROR_IN_X g_Rage_Tessellation_ScreenSpaceErrorParams.z
#define PN_TRIANGLE_ALLOWED_ERROR_IN_Y g_Rage_Tessellation_ScreenSpaceErrorParams.w


// Returns depth relative to the camera (P is in model space).
float rage_ComputeDepth(float3 P) 
{ 
#if PN_TRIANGLES_USE_WORLD_SPACE
	float3 Pw = P;
#else
	float3 Pw = mul(float4(P,1), gWorld).xyz;
#endif
#if PN_TRIANGLES_USE_DEPTH_LENGTH
	return length(Pw - g_Rage_Tessellation_CameraPosition.xyz);
#else
	return dot(Pw - g_Rage_Tessellation_CameraPosition.xyz, g_Rage_Tessellation_CameraZAxis.xyz);
#endif
} 


// Computes tessellation factor based upon depth.
float rage_ComputePNTriangleTessellationFactor_DepthBased(float3 P)
{
	float Depth = rage_ComputeDepth(P);
	
	// Apply the linear calculation.
	float k = (PN_TRIANGLE_MAX - 1.0f)*((Depth - PN_TRIANGLE_FAR)/(PN_TRIANGLE_NEAR - PN_TRIANGLE_FAR));
	
	// Clamp to allowed ranges.
	k = min(k, PN_TRIANGLE_MAX - 1.0f);
	k = max(k, 0.0f);
	
	// Compute tessellation factor.
	float Ret = k + 1.0f; // The +1.0f is the + c of the linear calculation (y = mx + c).
	
	return Ret;
}


// Computes tessellation facro based in pixel error.
float rage_ComputePNTriangleTessellationFactor_ScreenSpaceError(float3 P, rage_PNTri_PatchInfo Patch, float factorMultiplier)
{
	// Compute position on the triangle plane.
	float3 POnTriangle = rage_ComputeBarycentric(P, Patch.ControlPoints.b300, Patch.ControlPoints.b030, Patch.ControlPoints.b003); 
	// And also on the patch.
	float3 POnPatch = rage_EvaluatePatchAt(Patch, P);
	// Form the difference.
	float Difference = length(POnTriangle - POnPatch);
	
	// Compute the depth relative the camera...
	float Depth = abs(rage_ComputeDepth(POnTriangle));
	// (avoid division by zero).
	Depth = max(Depth, 0.001f);
	
	// DX11 TODO:- Optimise by storing max(PN_TRIANGLE_PROJECTION_X/PN_TRIANGLE_ALLOWED_ERROR_IN_X, PN_TRIANGLE_PROJECTION_Y/PN_TRIANGLE_ALLOWED_ERROR_IN_Y).
	
	//...And project onto the screen.
	float2 PixelError = (Difference/Depth) * float2(PN_TRIANGLE_PROJECTION_X, PN_TRIANGLE_PROJECTION_Y);
	
	// Tess factor = 2^(log2(PixelError/AllowedError) + 1).
	float Ret = 2*max(PixelError.x / PN_TRIANGLE_ALLOWED_ERROR_IN_X, PixelError.y / PN_TRIANGLE_ALLOWED_ERROR_IN_Y);
	Ret = min(max(1.0f, Ret*factorMultiplier), PN_TRIANGLE_MAX);
		
	return Ret; 
}


// Computes PN triangle patch info.
void rage_ComputePNTrianglePatchInfo(rage_PNTri_Vertex PatchPoint0, rage_PNTri_Vertex PatchPoint1, rage_PNTri_Vertex PatchPoint2, out rage_PNTri_PatchInfo PatchInfo, float TessMultiplier)
{
	rage_ComputePNTriangleContolPoints(1.0f, PatchPoint0, PatchPoint1, PatchPoint2, PatchInfo.ControlPoints);

#if PN_TRIANGLE_DEPTH_BASED_TESSELLATION_FACTOR

#if PN_TRIANGLE_USE_MODEL_POSITION

	// Compute passing model space origin.
	float TessFactor = rage_ComputePNTriangleTessellationFactor_DepthBased(float3(0.0f, 0.0f, 0.0f)); 
	
	PatchInfo.Edges[0] = TessFactor;	
	PatchInfo.Edges[1] = TessFactor;	
	PatchInfo.Edges[2] = TessFactor;	
	PatchInfo.Inside[0] = TessFactor;	
	
#elif PN_TRIANGLE_USE_TRIANGLE_MID_POINT

	float3 P = (PatchPoint0.Position + PatchPoint1.Position + PatchPoint2.Position)/3;
	
	// Compute passing model space origin.
	float TessFactor = rage_ComputePNTriangleTessellationFactor_DepthBased(P); 
	
	PatchInfo.Edges[0] = TessFactor;	
	PatchInfo.Edges[1] = TessFactor;	
	PatchInfo.Edges[2] = TessFactor;	
	PatchInfo.Inside[0] = TessFactor;	

#elif PN_TRIANGLES_USE_EDGES_AND_MIDPOINT

	float3 P01 = (PatchPoint0.Position + PatchPoint1.Position)/2;
	float3 P12 = (PatchPoint1.Position + PatchPoint2.Position)/2;
	float3 P20 = (PatchPoint2.Position + PatchPoint0.Position)/2;
	
	PatchInfo.Edges[0] = rage_ComputePNTriangleTessellationFactor_DepthBased(P12); 
	PatchInfo.Edges[1] = rage_ComputePNTriangleTessellationFactor_DepthBased(P20); 
	PatchInfo.Edges[2] = rage_ComputePNTriangleTessellationFactor_DepthBased(P01); 
	PatchInfo.Inside[0] = max(max(PatchInfo.Edges[0], PatchInfo.Edges[1]), PatchInfo.Edges[2]);
	
#endif

#elif PN_TRIANGLE_SCREEN_SPACE_ERROR_TESSELLATION_FACTOR

#if PN_TRIANGLE_USE_TRIANGLE_MID_POINT

	float3 P = float3(1.0f/3.0f, 1.0f/3.0f, 1.0f/3.0f);
	float TessFactor = rage_ComputePNTriangleTessellationFactor_ScreenSpaceError(P, PatchInfo, TessMultiplier); 
	
	PatchInfo.Edges[0] = TessFactor;	
	PatchInfo.Edges[1] = TessFactor;	
	PatchInfo.Edges[2] = TessFactor;	
	PatchInfo.Inside[0] = TessFactor;	
	
#elif PN_TRIANGLES_USE_EDGES_AND_MIDPOINT
	
	float3 P01 = float3(1.0f/2.0f, 1.0f/2.0f, 0.0f);
	float3 P12 = float3(0.0f, 1.0f/2.0f, 1.0f/2.0f);
	float3 P20 = float3(1.0f/2.0f, 0.0f, 1.0f/2.0f);
	
	PatchInfo.Edges[0] = rage_ComputePNTriangleTessellationFactor_ScreenSpaceError(P12, PatchInfo, TessMultiplier); 
	PatchInfo.Edges[1] = rage_ComputePNTriangleTessellationFactor_ScreenSpaceError(P20, PatchInfo, TessMultiplier); 
	PatchInfo.Edges[2] = rage_ComputePNTriangleTessellationFactor_ScreenSpaceError(P01, PatchInfo, TessMultiplier); 
	PatchInfo.Inside[0] = max(max(PatchInfo.Edges[0], PatchInfo.Edges[1]), PatchInfo.Edges[2]);

#endif

#elif PN_TRIANGLE_FIXED_TESSELLATION_FACTOR //this is mainly for testing

	float3 P01 = float3(1.0f/2.0f, 1.0f/2.0f, 0.0f);
	float3 P12 = float3(0.0f, 1.0f/2.0f, 1.0f/2.0f);
	float3 P20 = float3(1.0f/2.0f, 0.0f, 1.0f/2.0f);
	
	PatchInfo.Edges[0] = PN_TRIANGLE_MAX;
	PatchInfo.Edges[1] = PN_TRIANGLE_MAX;
	PatchInfo.Edges[2] = PN_TRIANGLE_MAX;
	PatchInfo.Inside[0] = max(max(PatchInfo.Edges[0], PatchInfo.Edges[1]), PatchInfo.Edges[2]);

#endif
}

//--------------------------------------------------------------------------------------
// Returns the distance of a given point from a given plane
//--------------------------------------------------------------------------------------
float rage_DistanceFromPlane ( 
                        float3 f3Position,      // World space position of the patch control point
                        float4 f4PlaneEquation  // Plane equation of a frustum plane
                        )
{
    float fDistance = dot( float4( f3Position, 1.0f ), f4PlaneEquation );
    
    return fDistance;
}

//--------------------------------------------------------------------------------------
// Returns the dot product between the viewing vector and the patch edge
//--------------------------------------------------------------------------------------
float rage_GetEdgeDotProduct ( 
							  float3 f3EdgeNormal0,   // Normalized normal of the first control point of the given patch edge 
							  float3 f3EdgeNormal1,   // Normalized normal of the second control point of the given patch edge 
							  float3 f3ViewVector     // Normalized viewing vector
	                         )
{
    float3 f3EdgeNormal = normalize(f3EdgeNormal0 + f3EdgeNormal1);
    float fEdgeDotProduct = dot( f3EdgeNormal, f3ViewVector );
	return fEdgeDotProduct;
}

//--------------------------------------------------------------------------------------
// Returns back face culling test result (true / false)
//--------------------------------------------------------------------------------------
bool rage_BackFaceCull( float3 fEdgeDotProduct, // Dot product of edge 0,1,2 normal with view vector
						float fBackFaceEpsilon  // Epsilon to determine cut off value for what is considered back facing
						)
{  
    return all(fEdgeDotProduct < -fBackFaceEpsilon);
}

//--------------------------------------------------------------------------------------
// Returns back face culling test result (true / false)
// Wrapper so you just provide the normals
//--------------------------------------------------------------------------------------
bool rage_BackFaceCulling (float3 f3Normal0, float f3Normal1, float f3Normal2,
							float3 f3ViewVector, float fEpsilon )
{
    float3 fEdgeDot;

    // Perform back face culling test
        
    // Aquire patch edge dot product between patch edge normal and view vector 
    fEdgeDot.x = rage_GetEdgeDotProduct( f3Normal2, f3Normal0, f3ViewVector );
    fEdgeDot.y = rage_GetEdgeDotProduct( f3Normal0, f3Normal1, f3ViewVector );
    fEdgeDot.z = rage_GetEdgeDotProduct( f3Normal1, f3Normal2, f3ViewVector );

    // If all 3 fail the test then back face cull
    return rage_BackFaceCull( fEdgeDot, fEpsilon );
}

//--------------------------------------------------------------------------------------
// Returns view frustum Culling test result (true / false)
//--------------------------------------------------------------------------------------
bool rage_IsTriangleInFrustum(
							float3 f3VertexPosition0,         // World space position of patch control point 0
							float3 f3VertexPosition1,         // World space position of patch control point 1
							float3 f3VertexPosition2,         // World space position of patch control point 2
							float fCullEpsilon              // Epsilon to determine the distance outside the view frustum is still considered inside
							)
{    
    float4 f4PlaneTest;
    
    // Left clip plane
    f4PlaneTest.x = ( ( rage_DistanceFromPlane( f3VertexPosition0, g_Rage_Tessellation_Frustum[0]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition1, g_Rage_Tessellation_Frustum[0]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition2, g_Rage_Tessellation_Frustum[0]) > -fCullEpsilon ) ? 1.0f : 0.0f );
    // Right clip plane
    f4PlaneTest.y = ( ( rage_DistanceFromPlane( f3VertexPosition0, g_Rage_Tessellation_Frustum[1]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition1, g_Rage_Tessellation_Frustum[1]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition2, g_Rage_Tessellation_Frustum[1]) > -fCullEpsilon ) ? 1.0f : 0.0f );
    // Top clip plane
    f4PlaneTest.z = ( ( rage_DistanceFromPlane( f3VertexPosition0, g_Rage_Tessellation_Frustum[2]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition1, g_Rage_Tessellation_Frustum[2]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition2, g_Rage_Tessellation_Frustum[2]) > -fCullEpsilon ) ? 1.0f : 0.0f );
    // Bottom clip plane
    f4PlaneTest.w = ( ( rage_DistanceFromPlane( f3VertexPosition0, g_Rage_Tessellation_Frustum[3]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition1, g_Rage_Tessellation_Frustum[3]) > -fCullEpsilon ) ? 1.0f : 0.0f ) +
                    ( ( rage_DistanceFromPlane( f3VertexPosition2, g_Rage_Tessellation_Frustum[3]) > -fCullEpsilon ) ? 1.0f : 0.0f );
        
    // Triangle has to pass all 4 plane tests to be visible
    return all( f4PlaneTest );
}

#endif // defined(RAGE_USE_PN_TRIANGLES)

