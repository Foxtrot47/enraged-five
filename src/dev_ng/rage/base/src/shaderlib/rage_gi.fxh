#ifndef RAGE_GI_FXH
#define RAGE_GI_FXH

float giFactor : GIFactor
	<
		string UIName = "GI lighting factor";
		float UIMin = 0.0;
		float UIMax = 3.0;
		float UIStep = 0.1;
	> = 1.0;

float emissiveFactor : emisiveFactor
	<
		string UIName = "Emissive Light";
		float UIMin = 0.0;
		float UIMax = 100.0;
		float UIStep = 0.1;
	> = 0.1;
float emissiveMult : emissiveMult = 0.0;

// Things we want to set programatically
float samplesWidth : samplesWidth = 2;
float samplesHeight : samplesHeight = 2;
float samplesDepth : samplesDepth = 2;
float cubeSize : cubeSize = 64;
float cubesWide : cubesWide = 3;
float cubesHigh : cubesHigh = 4;
float numCubeFaces : numCubeFaces = 6;
float texWidth : texWidth = 1152;
float texHeight : texHeight = 256;
float3 bbMin : bbMin = {-100.000000, -50.500000, 0.000000};
float3 bbMax : bbMax = {0.500000, 50.500000, 101.000000};
float3 bbCenter : bbCenter = {-50.000000, 0.000000, 50.251236};

float shX : shX = 0;
float shY : shY = 0;
float shZ : shZ = 0;

float shShowCube : shShowCube = 0;
float shShowIndirect : shShowIndirect = 0;
float shCameraAtCorner : shCameraAtCorner = 0;

float3 lightPos = {-50.0, 0.0, 50.0};
float  lightAtten = 50;
float4 lightColor = {0.75, 0.75, 0.75, 1.0f};

float  headLightAtten = 50;
float4 headLightColor = {0.75, 0.75, 0.75, 1.0f};

//------------------------------------------------------------------------------
// Compute simple lighting
//------------------------------------------------------------------------------
float3 ComputeLight(float3 position, float3 normal)
{
	// Figure out light vector/distance
	float3 L = lightPos - position;
	float lDistSq = dot(L, L);
	float lDist = sqrt(lDistSq);
	L = L/lDist;

	// Now lambert lighting
	float NdotL = saturate(dot(normalize(normal), L));
	float atten = lightAtten/lDistSq;
	return lightColor.rgb*NdotL*atten;
}//ComputeLight

//------------------------------------------------------------------------------
// Convert from a 3D world space position to sample indices
//------------------------------------------------------------------------------
float3 giConvertFromPositionToSampleNorm(float3 position)
{
	// Compute our normalized position in the bounding box 0-1 for each face
	float3 nPos = saturate((position - bbMin) / (bbMax - bbMin));

	// Vector of our sample sizes
	float3 samples = {samplesWidth, samplesHeight, samplesDepth};

	// Need to renormalize chopping off the ends since the cameras/samples
	// aren't at the edges of the box.
	if (shCameraAtCorner < 0.9)
	{
		nPos = nPos - (1.0f / (samples+1));
		nPos *= ((samples+1)/((samples+1)-2));
		saturate(nPos);
	}

	// Convert normalized position into clampped sample indices
	float3 indices = nPos * (samples-1);
	indices = round(indices);
	indices = clamp(indices, float3(0.0f, 0.0f, 0.0f), (samples - 1));
	indices = floor(indices);
	return indices;
}//giConvertFromPosToSampleNorm

//------------------------------------------------------------------------------
// Convert from a 3D world space position to sample indices
//------------------------------------------------------------------------------
float3 giConvertFromPositionToSample(float3 position)
{
	// Compute our normalized position in the bounding box 0-1 for each face
	float3 nPos = saturate((position - bbMin) / (bbMax - bbMin));

	// Vector of our sample sizes
	float3 samples = {samplesWidth, samplesHeight, samplesDepth};

	// Need to renormalize chopping off the ends since the cameras/samples
	// aren't at the edges of the box.
	if (shCameraAtCorner < 0.9)
	{
		nPos = nPos - (1.0f / (samples+1));
		nPos *= ((samples+1)/((samples+1)-2));
		saturate(nPos);
	}

	// Convert normalized position into clampped sample indices
	float3 indices = nPos * (samples-1);
	indices = clamp(indices, float3(0.0f, 0.0f, 0.0f), (samples - 1));
	return indices;
}//giConvertFromPositionToSample

//------------------------------------------------------------------------------
// Convert from a sample indices to 3D world space position
//------------------------------------------------------------------------------
float3 giConvertFromSampleToPostion(float3 in_sample)
{
	// Convert into normalized coordinates
	float3 samples = {samplesWidth, samplesHeight, samplesDepth};
	float3 nPos = saturate(in_sample/(samples-1));

	// Now we have 0-1 clamped coordinates to the sample points, if the cameras
	// are not at the corners we need to further compress the range because we
	// aren't going to the bounding box edges.
	if (shCameraAtCorner < 0.9)
	{
		nPos *= (((samples+1)-2)/(samples+1));
		nPos += (1.0f / (samples+1));
	}

	// Now convert the 0-1 bounding box coordinates into real positions
	float3 pos = (nPos*(bbMax - bbMin)) + bbMin;
	return pos;
}//giConvertFromSampleToPostion

//------------------------------------------------------------------------------
// Convert from sample indices into cube map indices
//------------------------------------------------------------------------------
float2 giConvertFromSampleToCube(float3 indices)
{
	float index = indices.x + indices.y*samplesWidth + indices.z*samplesWidth*samplesHeight;
	float2 cubeIdx;
	cubeIdx.x = (uint)index % (uint)cubesWide;
	cubeIdx.y = (uint)(floor((float)(index) / (uint)(cubesWide))); 
	return cubeIdx;
}//giConvertFromSampleToCube

//------------------------------------------------------------------------------
// Gets the 2D texture coordinate offset to a particular cube face given 
// the 3D index for a particular probe.
//------------------------------------------------------------------------------
float2 giGetCubeFaceOffset(float3 pos, float face)
{
	// Figure out cube coordinates in the paged texture
	float3 sample_ = giConvertFromPositionToSampleNorm(pos);
	float2 cube = giConvertFromSampleToCube(sample_);
	
	// Figure out texture offset to the cube face we care about
	float wX = cube.x * cubeSize;//((cubeX*numCubeFaces) + face) * cubeSize;
	float wY = ((cube.y * numCubeFaces) + face) * cubeSize;//cubeY * cubeSize;
	float2 offset;
	offset.x = wX/texWidth;
	offset.y = wY/texHeight;
	return offset;
}//giGetCubeFaceOffset

//------------------------------------------------------------------------------
// Convert normal into 2D texture coordinates (xy -1 to +1) and a face index (z)
//------------------------------------------------------------------------------
float3 giGetCubeMapTexFace(float3 norm)
{
	// Figure out which face and it's texture coordinates
	float2 tx = norm.xy;
	float face = 0.0f;
	if ( (abs(norm.x) > abs(norm.y)) && (abs(norm.x) > abs(norm.z)) )
	{
		if (sign(norm.x) > 0)
		{
			tx = norm.zy;
			tx.y = -tx.y;
			face = 1.0f;
		}
		else
		{
			tx = -norm.zy;
			face = 0.0f;
		}
	}
	else if ( (abs(norm.y) > abs(norm.x)) && (abs(norm.y) > abs(norm.z)) )
	{
		if (sign(norm.y) > 0)
		{
			tx = norm.xz;
			tx.y = -tx.y;
			face = 3.0f;
		}
		else
		{
			tx = norm.xz;
			face = 2.0f;
		}
	}
	else
	{
		if (sign(norm.z) > 0)
		{
			tx = -norm.xy;
			face = 5.0f;
		}
		else
		{
			tx = norm.xy;
			tx.y = -tx.y;
			face = 4.0f;
		}
	}
	float3 vOut;
	vOut.xy = tx*0.5 + 0.5;
	vOut.z = face;
	return vOut;
}//giGetCubeMapTexFace

//------------------------------------------------------------------------------
// Convert from position to texture coordinates in the SH Coeff texture
//------------------------------------------------------------------------------
float2 giConvertFromPosToSHTex(float3 pos)
{
	float3 sPos = giConvertFromPositionToSample(pos);
	sPos.z = floor(round(sPos.z));
	float3 nPos = saturate(sPos/float3(samplesWidth-1, samplesHeight-1, samplesDepth-1));
	float2 shCoord;
	shCoord.x = nPos.x;
	shCoord.y = nPos.y/(samplesDepth);
	shCoord.y += (sPos.z/(samplesDepth));
	return shCoord;
}//giConvertFromPosToSHTex

//------------------------------------------------------------------------------
// Convert from 0-N texture coordinates into 0-1 flattened cubemap coordinates
//------------------------------------------------------------------------------
float2 giConvertSHCubeCoord(float2 tx, float face)
{
	float2 tex = tx.xy;
	tex.x = tex.x/8;
	tex.y = (tex.y + (face*cubeSize))/(6*8);
	return tex;
}//giConvertSHCubeCoord

#endif // RAGE_GI_FXH

