//
//	PURPOSE
//		A library of rountines and samplers for creating grass 
//

#ifndef RAGE_GRASS_FXH
#define RAGE_GRASS_FXH

#define NO_SKINNING
#include "../../../../rage/base/src/shaderlib/rage_common.fxh"


BeginSampler(sampler2D,colorMapTexture,ColorMapSampler,ColorMapTex)
    string UIName = "Color Map Texture";
ContinueSampler(sampler2D,colorMapTexture,ColorMapSampler,ColorMapTex)
		AddressU = clamp; 
		AddressV = clamp;
		AddressW = clamp;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		MipMapLodBias = 0;
EndSampler;

BeginSampler(sampler2D,normalMapTexture,NormalMapSampler,NormalMapTexGrass)
    string UIName = "Normal Map Texture";
ContinueSampler(sampler2D,normalMapTexture,NormalMapSampler,NormalMapTexGrass)
	#if !__FXL  // these are the defaults for FXL
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = MIN_FILTER;
		MAGFILTER = LINEAR; 
		MipMapLodBias = 0;
	#else
		#if DIFFUSE_CLAMP != Wrap   // wrap is default...
			AddressU = DIFFUSE_CLAMP;
			AddressV = DIFFUSE_CLAMP;
			AddressW = DIFFUSE_CLAMP;
		#endif

		#if MIN_FILER != Linear		//don't reset if default
			MINFILTER = MIN_FILTER;
		#endif
	
		#ifdef MIPMAP_LOD_BIAS
			MipMapLodBias = MIPMAP_LOD_BIAS;
		#endif
	#endif
EndSampler;

BeginSampler(sampler2D,windTexture,WindSampler,WindTexture)
    string UIName = "Wind Texture";
ContinueSampler(sampler2D,windTexture,WindSampler,WindTexture)
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSampler;

float	Grass_GetMipLevel	(sampler2D ss, float2 uv)
{
	float	Result;
#if	__XENON
	asm
	{
		getCompTexLOD2D	Result.x, uv, ss
	};
#else
	Result = 0.0;
#endif
	return(Result);
}
float	Grass_ComputeScaledAlpha	(float Alpha, sampler2D ss, float2 uv)
{
	float	MipLevel = Grass_GetMipLevel(ss, uv);

	float	AlphaOut = saturate(Alpha * lerp(1.5, 3.0, saturate((MipLevel - 1) / 6)));
	// push alpha that is "close" to full opaque, this helps with alpha to mask dithering
	return(AlphaOut);
}


float3 g_vUpVector : UpVector = {0.0f, 1.0f, 0.0f};
float g_fBillboardAgainstVP : BillboardAgainstViewPlane = 1.0f;
float g_fUseWindTexture : UseWindTexture = 0.0f;
float g_fUseNormalMap : UseNormalMap = 0.0f;
float4 g_vFieldExtents : FieldExtents;

float2 g_FadeScale : Fade;

struct grassVertexIn
{
	float4 center			: POSITION;
	float2 uv				: TEXCOORD0;
	float4 color			: COLOR0;
};

// int			g_GrassThinAmount : GrassThinAmount;
// int			g_GrassThinAmountNext : GrassThinAmountNext ;
float2		g_GrassThinFade: GrassThinFade;
float3		g_GrassQuantScale: GrassQuantScale;
float3		g_GrassQuantOffset: GrassQuantOffset;
float4		g_GrassTexSize : GrassTexSize;


float2		g_DoubleFadeDist : GrassDoubleFadeDist = float2(20.0f,1.0/5.0f);
// Fading
float2 FadeOffsetSlope = float2(10.0f, 1.0f);


float GetFadeOutAmount(float distance, float2 offsetSlope)
{
	float result = saturate((distance - offsetSlope.x) * offsetSlope.y);
	return (1.0f - result);
}

// w is used to randomize grass direction
CBSHARED BeginConstantBufferDX10(rage_global)
SHARED float4 GlobalScale : GlobalScale;// REGISTER2(vs, c78);
// w is used to fade out new grasses
SHARED float4 GlobalBias : GlobalBias;// REGISTER2(vs, c79);
EndConstantBufferDX10(rage_global)

#if __XENON
#define RANDOM_TABLE_SIZE  2
float4		g_ExtraVertData[ RANDOM_TABLE_SIZE ] : GrassExtraVertData;



// uniform bool		g_CameraFacing : GrassCameraFacing;

#define _USE_CAMERA_SPACE	0

#if _USE_CAMERA_SPACE
float3 GetNormalDirection( int BladeId, float2 axiasScale )
{
	float2 angleDir = ((float)(BladeId % 30) - 15.0f) / 30.0f;
	angleDir = float2(cos(angleDir.x), sin(angleDir.x));
	return angleDir.x * axiasScale.x * gViewInverse[0].xyz -
			 axiasScale.y * gViewInverse[1].xyz 
			 + angleDir.y * axiasScale.x * gViewInverse[2].xyz;
}

#else
float3 GetNormalDirection( int BladeId, float2 axiasScale, float angleOffset )
{
	float2 angleDir =(((float) (BladeId % 30 ) - 15.0f ) /15.0f ) * 3.142f;
	angleDir+= angleOffset;
	angleDir = float2( cos( angleDir.x), sin( angleDir.x) );
	
	return  float3( angleDir.x * axiasScale.x,-axiasScale.y , angleDir.y * axiasScale.x);
}
#endif
#define ENLARGE_AMT		0.7	

//------------------------------------------------------------------------------
//	PURPOSE
//		Similar to GrassDecodeDetailsQuad but does a last-minute redimension and
//	UV shift to reduce empty pixels, improving performance.
//
float3 GrassDecodeClippedQuad(int i, out float2 uvs, out float thinFade, out float4 col, out float4 center)
{
	int index = i % 4;
	int vertex = (i / 4);
	
	float4 centre;
	float4 color;
	asm {
		vfetch	centre, vertex, position0
		vfetch color, vertex, color0
	};
	// lookup into random table for vertex and uvs
	color.xyz = color.zyx / 255.0f;
	centre.w = color.w/255.0f;
	//scale = centre.w;

	

	uvs = float2(saturate(((i + 1) / 2) % 2), saturate((i / 2) % 2));

	
	
	// decompress position
	centre.xyz = centre.xyz * GlobalScale.xyz + GlobalBias.xyz;
	center = centre;
	
	float distance = length(gViewInverse[3].xyz - centre.xyz );	
	thinFade = GetFadeOutAmount( distance, FadeOffsetSlope );
	
	// calculate edge
	float2 axiasScale = (uvs - float2(0.5f, 1.0f)) * centre.w * 2.0f ;
	axiasScale.x *= 1. + ENLARGE_AMT; // scale up to fill area.
	
	float3 Normal =GetNormalDirection( vertex, axiasScale, 0.0f );
	
	// rescale uv's to focus on the clip area
	uvs.x *= 2.0;

#if COLORIZE_TECHNIQUES
color.rgb = float3(1.0f, 0.5f, 0.5f);
#endif

	col = color;
	return centre + Normal;
}

#define NUM_HIGH_GRASS_VERTS 6

float3 BentVerts2[8] =  { float3( 0.0f,0.0f, 0.0f),
						 float3( 0.0f, 0.0f, 1.0f),
						 float3( 0.0f, 1.0f, 1.0f ),
						 float3( 0.0f, 1.0f, 0.0f ),
						 float3( 0.0f, 0.0f, 1.0f ),
						 float3( 1.0f, 0.0f, 0.0f ),
						 float3( 1.0f, 1.0f, 0.0f ),
						 float3( 0.0f, 1.0f, 1.0f ) } ;

float3 BentVerts[NUM_HIGH_GRASS_VERTS] =  { 
			float3( 0.0f,0.0f, 0.0f),		
			float3( 0.0f, 0.0f, 1.0f),
			float3( 0.0f, 1.0f, 1.0f ),			
			float3( 0.0f, 1.0f, 0.0f ),			
			float3( 1.0f, 1.0f, 0.0f ),
			float3( 1.0f, 0.0f, 0.0f ) } ;

float3 CalculateBentVerts( int index )
{
	float y =(float)( index > 1  && index != 5);
	float x =(float)( index > 3 );
	float z = (float)( index==1 || index ==2 );
	//float3 bv = BentVerts[index];
	
	return float3( x, y, z );

}

//------------------------------------------------------------------------------
//	PURPOSE
//		Similar to GrassDecodeDetailsQuad but does a last-minute redimension and
//	UV shift to reduce empty pixels, improving performance.
//
float3 GrassDecodeClippedCylinder(int i, out float2 uvs, out float thinFade, out float4 col, out float4 centerPoint)
{
	int vertex = (i + 0.5) / NUM_HIGH_GRASS_VERTS;
	int index = i - vertex * NUM_HIGH_GRASS_VERTS;
	
	float4 centre;
	float4 color;
	asm {
		vfetch	centre, vertex, position0
		vfetch color, vertex, color0
	};
	// lookup into random table for vertex and uvs
	color.xyz = color.zyx / 255.0f;
	centre.w = color.w/255.0f;
	
	
	float3 vert = CalculateBentVerts( index);
	uvs = vert.xy;


	// decompress position
	centre.xyz = centre.xyz * GlobalScale.xyz + GlobalBias.xyz;

	float distance = length(gViewInverse[3].xyz - centre.xyz );	
	float distFade = saturate(1.0f - ( (distance - g_DoubleFadeDist.x) * g_DoubleFadeDist.y));
	
	float fadeOut =saturate((1.0f-saturate(GlobalScale.w))+distFade );
	thinFade = fadeOut * GetFadeOutAmount( distance, FadeOffsetSlope );
	
	// calculate edge
	float angleFadeIn = 1.0 - saturate( GlobalBias.w);
	uvs.x +=0.5f * vert.z;
	
	
	
	float2 axiasScale = (uvs - float2(0.5f, 1.0f)) * centre.w * 2.0f ;
	axiasScale.x += vert.z* centre.w * 0.75f * angleFadeIn;
	
	// scale up in distance
	axiasScale.x *= (1. + ENLARGE_AMT) - ENLARGE_AMT * angleFadeIn;
	float3 Normal =GetNormalDirection( vertex + (int)GlobalScale.w, axiasScale, vert.z * 3.142/2.0 * angleFadeIn);
	
	uvs.x *= (2.0 - angleFadeIn );

	centerPoint = centre;
	centerPoint.xz += Normal.xz;
#if COLORIZE_TECHNIQUES
color.rgb = float3(1.0f, 0.5f, 0.5f);
#endif

	col = color;
	return centre + Normal;
}

float3 GrassDecodeClippedVert( uniform bool useCylinder, int i, out float2 uvs, out float thinFade, out float4 col, out float4 center)
{
	float3 pos;
	if ( useCylinder )
	{
		pos = GrassDecodeClippedCylinder(i, uvs, thinFade, col, center);
	}
	else
	{
		pos = GrassDecodeClippedQuad(i, uvs, thinFade, col, center);
	}
	return pos;
}


float3 GrassDecodeDetailsSimpleQuad(int i , out float2 uvs, out float thinFade, out float4 col, out float scale   )
{
	int index = i % 4;
	int vertex = (i / 4);
	
	float4 centre;
	float4 color;
	asm {
		vfetch	centre, vertex, position0
		vfetch color, vertex, color0
	};
	// lookup into random table for vertex and uvs
	color.xyz = color.zyx / 255.0f;
	
	uvs =float2( saturate( (( i + 1 )/2) %2 ), saturate( (i /2)% 2 ) ); 

	
	thinFade = 1.0f - ( ((float) vertex - g_GrassThinFade.y ) * g_GrassThinFade.x );	
	thinFade = saturate( thinFade );
	
	// decompress position
	centre.xyz = centre.xyz * g_GrassQuantScale + g_GrassQuantOffset;

	float2 axiasScale = (uvs - float2( 0.5f, 1.0f) ) * centre.w * 2.0f;
	scale = centre.w;	
	float3 Normal=  1.0f*axiasScale.x * gViewInverse[0].xyz 
				- axiasScale.y * gViewInverse[ 1 ].xyz
				+ 0.0f *axiasScale.x * gViewInverse[2].xyz ;//float3( 0.0f,1.0f, 0.0f);//vVertex;

	col = color;
	return centre + Normal;
}

float3 GrassDecodeDetailsQuadNormalvDataTDataCol( int i , out float2 uvs, out float thinFade , out float3 Normal, 
										out float4 vdata,
										out float4 tdata ,
										out float4 col,
										out float scale  )
{
	int index = i % 4;
	int vertex = i / 4;
	
	float4 centre;
	asm {
		vfetch	centre, vertex, position0
		vfetch  vdata, vertex, normal0
		vfetch  col, vertex, color0
	};
	// lookup into random tab le for vertex and uvs
	uvs =float2( saturate( (( i + 1 )/2) %2 ), saturate( (i /2)% 2 ) ); 
	tdata = lerp( g_ExtraVertData[ 0], g_ExtraVertData[ 1] , uvs.y);  // just use two 
	
	thinFade = saturate( 1.0f - ( ((float) vertex - g_GrassThinFade.y ) * g_GrassThinFade.x ) );	
	
	// decompress position
	centre.xyz = centre.xyz * g_GrassQuantScale + g_GrassQuantOffset;

	scale = centre.w;
	float2 axiasScale = (uvs - float2( 0.5f, 1.0f) ) * centre.w * 2.0f;
	
	float2 angleDir =((float) (i % 30 ) - 15.0f ) /30.0f;
	angleDir = float2( cos( angleDir.x), sin( angleDir.x) );
	
	Normal=  angleDir.x *axiasScale.x * gViewInverse[0].xyz 
				- axiasScale.y * gViewInverse[ 1 ].xyz
				+ angleDir.y *axiasScale.x * gViewInverse[2].xyz ;//float3( 0.0f,1.0f, 0.0f);//vVertex;
	
	// apply camera scaling

	return centre.xyz + Normal;
//return centre.xyz + vVertex;	
}
float3 GrassDecodeDetailsQuadSimple( int i , out float2 uvs,out float scale  )
{
	int index = i % 4;
	int vertex = i / 4;
	
	float4 centre;
	asm {
		vfetch	centre, vertex, position0
	};
	// lookup into random tab le for vertex and uvs
	uvs =float2( saturate( (( i + 1 )/2) %2 ), saturate( (i /2)% 2 ) ); 
	// decompress position
	centre.xyz = centre.xyz * g_GrassQuantScale + g_GrassQuantOffset;

	// apply camera scaling
	scale = centre.w;
	float2 axiasScale = (uvs - float2( 0.5f, 1.0f) ) * centre.w * 2.0f;
	return centre.xyz + axiasScale.x * gViewInverse[0].xyz - axiasScale.y * gViewInverse[ 1 ].xyz;
		
//return centre.xyz + vVertex;	
}
#endif



float3 GrassSetPositionVS(grassVertexIn IN)
{	
	float distance = length( IN.center.xyz - gViewInverse[3].xyz );
	
	float distScale = 1.0f + distance / 900.0f;
	
	// Rotate to face the camera
	float4 vWorldPos =float4(IN.center.xyz, 0.0f);
	return vWorldPos.xyz;
}
float2 GrassSetTexLookupVS( float3 vWorldPos )
{
	float2 uv = float2((vWorldPos.x - g_vFieldExtents.x) / (g_vFieldExtents.y - g_vFieldExtents.x), 
							(vWorldPos.z - g_vFieldExtents.z) / (g_vFieldExtents.w - g_vFieldExtents.z));

	return uv;
}

float3 GrassGetNormalVS(  grassVertexIn IN)
{
	return float3( IN.center.x, IN.center.y, IN.center.z );	
}

float4	GrassGetMaterialColorPS( float2 uv  )
{
	return  tex2D( ColorMapSampler, uv) * float4( 1.0f, 1.0f, 1.0f, 2.0f);
}

float GrassCalculateFade( float3 worldPos )
{
	float3 cameraDistance =  gViewInverse[3].xyz - worldPos.xyz;
	return ( 1.0f - saturate (  length(cameraDistance) * g_FadeScale.x - g_FadeScale.y ) );
}
float GrassSunVisPS( float2 uv )
{
	return ( 1.0f - saturate(  uv.y * 2 - 1 )  );
}
float GrassGetAmbientOcclusionPS( float2 uv )
{
	return 0.5f + saturate( ( 1.0f - ( uv.y * uv.y ) ) ) * 0.5f;
}

#define USE_GRASS_RENDERSTATES				AlphaTestEnable = true;		AlphaRef = 50;		AlphaFunc = Greater;		CullMode = None;		AlphaBlendEnable = true;
	



#endif
