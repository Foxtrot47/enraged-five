#ifndef __RAGE_MEMEXPORT_FXH
#define __RAGE_MEMEXPORT_FXH

#if __XENON

// This constant gets updated by the CPU.  It contains several things the gpu
// needs in order to export to the proper location
float4 exportAddress : ExportAddress : register(vs, c255);

// This constant is a multiplier to help with indexing into the output vertex buffer
float4 addressConstValue = float4(0.0f, 1.0f, 0.0f, 0.0f);

// Macro helper for the pre pass function
#define MEM_EXPORT_PREPASS(input)		void rageMemExportPrePass(input, int vertIndex : INDEX)
		
// Function to export only one vector register
void rageMemExport1(int vertIndex, float4 ov0)
{
	float fVertIndex = (float)vertIndex;
	asm
	{
		alloc export = 1
		mad eA, fVertIndex, addressConstValue, exportAddress
		mov eM0, ov0	
	};
}

// Function to export two vector registers
void rageMemExport2(int vertIndex, float4 ov0, float4 ov1)
{
	float fVertIndex = (float)vertIndex * 2.0f;
	asm
	{
		alloc export = 2
		mad eA, fVertIndex, addressConstValue, exportAddress
		mov eM0, ov0
		mov eM1, ov1	
	};
}
	
// Funciton to export three vector registers
void rageMemExport3(int vertIndex, float4 ov0, float4 ov1, float4 ov2)
{
	float fVertIndex = (float)vertIndex * 3.0f;
	asm
	{
		alloc export = 3
		mad eA, fVertIndex, addressConstValue, exportAddress
		mov eM0, ov0
		mov eM1, ov1
		mov eM2, ov2	
	};
}

// Funciton to export four vector registers
void rageMemExport4(int vertIndex, float4 ov0, float4 ov1, float4 ov2, float4 ov3)
{
	float fVertIndex = (float)vertIndex * 4.0f;
	asm
	{
		alloc export = 4
		mad eA, fVertIndex, addressConstValue, exportAddress
		mov eM0, ov0
		mov eM1, ov1
		mov eM2, ov2
		mov eM3, ov3	
	};
}
	
// Function to export 5 vector registers
void rageMemExport5(int vertIndex, float4 ov0, float4 ov1, float4 ov2, float4 ov3, float4 ov4)
{
	float fVertIndex = (float)vertIndex * 5.0f;
	asm
	{
		alloc export = 5
		mad eA, fVertIndex, addressConstValue, exportAddress
		mov eM0, ov0
		mov eM1, ov1
		mov eM2, ov2
		mov eM3, ov3
		mov eM4, ov4	
	};
}
	
// Macro for a fast pass basic passthrough function
#define MEM_EXPORT_FASTPASS(outStruct)								\
outStruct rageMemExportVSFastPass(outStruct IN)						\
{																	\
	return IN;														\
}

// Pixel shader used durring the export pass, this never gets executed but 
// D3D requires its presence
float4 rageMemExportPSPrePass() : COLOR0
{
	return float4(0.0f, 0.0f, 0.0f, 0.0f);
}

// A macro for the basic memory export technique. This uses the standard
// Pass through fast pass and the defined pre pass with the specified
// pixel shader.
#define MEM_EXPORT_TECHNIQUE(pixelShaderFunc)							\
technique drawmemexport													\
{																		\
	pass p0																\
	{																	\
		VertexShader = compile VERTEXSHADER rageMemExportPrePass();		\
		PixelShader = compile PIXELSHADER rageMemExportPSPrePass();		\
	}																	\
	pass p1																\
	{																	\
		VertexShader = compile VERTEXSHADER rageMemExportVSFastPass();	\
		PixelShader = compile PIXELSHADER pixelShaderFunc();			\
	}																	\
}	

// A macro for the memory export technique. This will use the defined
// pre pass function with the specified fast pass function and the specified
// pixel shader function.
#define MEM_EXPORT_TECHNIQUE_VS(fastVSFunc, pixelShaderFunc)			\
technique drawmemexport													\
{																		\
	pass p0																\
	{																	\
		VertexShader = compile VERTEXSHADER rageMemExportPrePass();		\
		PixelShader = compile PIXELSHADER rageMemExportPSPrePass();		\
	}																	\
	pass p1																\
	{																	\
		VertexShader = compile VERTEXSHADER fastVSFunc();				\
		PixelShader = compile PIXELSHADER pixelShaderFunc();			\
	}																	\
}	

#endif // __XENON

#endif // __RAGE_MEMEXPORT_FXH
