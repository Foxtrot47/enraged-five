
#ifndef RAGE_SHADOW_VARIANCE
#define RAGE_SHADOW_VARIANCE

 //--------------------------------------------------------------------------------------
//
// Variance Shadow maps based on the papers at
// http://www.punkuser.net/vsm/
//
//--------------------------------------------------------------------------------------
float shadowVariance_CompareDepth( sampler inShadowMap, float2 inTex, float fDistToLight, float attenuation, float depthBias, float eplision )
{
    float2 moments;
    // cube map look-up

    moments = tex2D(inShadowMap, inTex).xy ;
    
    // Rescale light distance and check if we're in shadow
	//float rescaled_dist_to_light = dist_to_light / light_atten_end;
    float RescaledDistToLight =  fDistToLight * attenuation - depthBias;
    
#if 1    
    float LightFactor = (RescaledDistToLight <= moments.x);
    
    // Variance shadow mapping
    float E_x2 = moments.y;
    float Ex_2 = moments.x * moments.x;
    float variance = min(max(E_x2 - Ex_2, 0.0) + eplision, 1.0);
    float m_d = (moments.x - RescaledDistToLight);
   	float p = smoothstep(0.15f, 1.0f, (variance / (variance + m_d * m_d)));
//    float p = variance / (variance + m_d * m_d);
    
    //return LightFactor;
	return max(LightFactor, p);
#else

	float epsilon = max(0, RescaledDistToLight - moments.x);
	
	// size of filter kernel (if you multiply it by 2 it looks cooler)
#define PIPPO (8)
	
	return exp(-epsilon * epsilon * (PIPPO + 1)*(PIPPO + 1) * 8);	
#endif	
}


float4 shadowVariance_CompareDepthBy4( float4 pos1, float4 pos2, float4 fDistToLight,
									sampler s1, sampler s2, sampler s3, sampler s4,
									float4 attenuation, float4 bias, float4 eplision )
{
	float4 RescaledDistToLight = fDistToLight *attenuation + bias;// 1 instrution   // 
    
    float4 moments;
    float4 moments2;
    
    moments.xz = tex2D( s1, pos1.xy ).xy;
    moments.yw = tex2D( s2, pos1.zw ).xy;
    float4 temp;
    temp.xz = tex2D( s3, pos2.xy ).xy;
    temp.yw = tex2D( s4, pos2.zw ).xy;
    moments2.xy = moments.zw;
    moments2.zw = temp.zw;
    moments.zw = temp.xy;
#if 1     
    float4 Ex_2 = -moments * moments + moments2;
    
    // Variance shadow mapping
    float4 variance =saturate( saturate( Ex_2 )  + eplision); 
    float4 m_d = (moments - RescaledDistToLight);	
    float4 val = m_d * m_d + variance;
    float4 valRecip = 1.0f/ val ;
   // float4 p = variance  * valRecip;
   	float4 p = smoothstep( (float4)0.15f, (float4)1.0f, variance  * valRecip);
   
    
    return (  m_d > 0.0f ? float4(1.0f, 1.0f, 1.0f, 1.0f) : p );
#else

	float4 epsilon = max(0, RescaledDistToLight - moments);
	
	// size of filter kernel (if you multiply it by 2 it looks cooler)
#define PIPPO (8)
	
	return exp(-epsilon * epsilon * (PIPPO + 1)*(PIPPO + 1) * 8);	
#endif    
}

float4 shadowVariance_CompareDepthBy4TA( float4 pos1, float4 pos2, float4 fDistToLight, sampler s1,
									float4 attenuation, float4 bias, float4 eplision )
{
	return shadowVariance_CompareDepthBy4( pos1, pos2, fDistToLight,
									s1, s1, s1, s1,
									attenuation, bias, eplision );
}
float4 shadowVariance_CompareDepthBy4Precscaled( float4 pos1, float4 pos2, float4 RescaledDistToLight,
									sampler s1, float4 eplision )
{
	float4 moments;
    float4 moments2;
    
    moments.xz = tex2D( s1, pos1.xy ).xy;
    moments.yw = tex2D( s1, pos1.zw ).xy;
    float4 temp;
    temp.xz = tex2D( s1, pos2.xy ).xy;
    temp.yw = tex2D( s1, pos2.zw ).xy;
     
    moments2.xy = moments.zw;
    moments2.zw = temp.zw;
    moments.zw = temp.xy;
#if 1
    float4 Ex_2 = -moments * moments + moments2;
    
    // Variance shadow mapping
    float4 variance =saturate( saturate( Ex_2 )  + eplision); 
    float4 m_d = (moments - RescaledDistToLight);	
    float4 val = m_d * m_d + variance;
    float4 valRecip = 1.0f/ val ;
    float4 p = variance  * valRecip;
  	//float4 p = smoothstep( (float4)0.15f, (float4)1.0f, variance  * valRecip);
  	//float4 p = ragePow( variance  * valRecip, 10.0f);//smoothstep( (float4)0.4f, (float4)1.0f, variance  * valRecip);
    
    
    return (  m_d > 0.0f ? float4(1.0f, 1.0f, 1.0f, 1.0f) : p );
#else
	float4 epsilon = max(0, RescaledDistToLight - moments);
	
	// size of filter kernel (if you multiply it by 2 it looks cooler)
#define PIPPO (8)
	
	return exp(-epsilon * epsilon * (PIPPO + 1)*(PIPPO + 1) * 8);	
#endif    
    
}
#endif

