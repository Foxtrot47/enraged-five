#ifndef HDRSAMPLERS_FXH
#define HDRSAMPLERS_FXH

//
// on the PS3 we can use a MRT with two 8-bit per channel render targets
//
#define PS3HDRRENDERTARGETS (0 && __PS3)

#define decompressHDR( value, Semantic ) (( value * Semantic##Exp) + Semantic##Ofs)

#if !__PS3
#define	tex2DHDR(SamplerName, TexCoords, Semantic)	((tex2D(SamplerName, TexCoords) * Semantic##Exp) + Semantic##Ofs)
#define	tex2DHDR2(SamplerName, TexCoords, DUVDX, DUVDY, Semantic)	((tex2D(SamplerName, TexCoords, DUVDX, DUVDY) * Semantic##Exp) + Semantic##Ofs)
#else
#define	tex2DHDR(SamplerName, TexCoords, Semantic)	(tex2D(SamplerName, TexCoords))
#define	tex2DHDR2(SamplerName, TexCoords, DUVDX, DUVDY, Semantic)	((tex2D(SamplerName, TexCoords) * Semantic##Exp) + Semantic##Ofs)
#endif

#if __FXL

#define	BeginSamplerHDR(samplerType, texName, samplerName, semantic) \
	float4	semantic##Exp	:	semantic##Exp \
	< \
		string	UIName = "same"; \
		int		UIHidden = 1; \
		float	UIMin = 0.0; \
		float	UIMax = 8.0; \
		float	UIStep = 0.05; \
	> = { 1.0, 1.0, 1.0, 1.0 }; \
	float4	semantic##Ofs	:	semantic##Ofs \
	< \
		string	UIName = "same"; \
		int		UIHidden = 1; \
		float	UIMin = 0.0; \
		float	UIMax = 8.0; \
		float	UIStep = 0.05; \
	> = { 0.0, 0.0, 0.0, 0.0 }; \
	samplerType samplerName : semantic <

#else

#define BeginSamplerHDR(samplerType, texName, samplerName, semantic) \
	float4	semantic##Exp	:	semantic##Exp \
	< \
		string	UIName = "same"; \
		int		UIHidden = 1; \
		float	UIMin = 0.0; \
		float	UIMax = 8.0; \
		float	UIStep = 0.05; \
	> = { 1.0, 1.0, 1.0, 1.0 }; \
	float4	semantic##Ofs	:	semantic##Ofs \
	< \
		string	UIName = "same"; \
		int		UIHidden = 1; \
		float	UIMin = 0.0; \
		float	UIMax = 8.0; \
		float	UIStep = 0.05; \
	> = { 0.0, 0.0, 0.0, 0.0 }; \
	texture texName : semantic <
#endif


#endif	// HDRSAMPLERS_FXH
