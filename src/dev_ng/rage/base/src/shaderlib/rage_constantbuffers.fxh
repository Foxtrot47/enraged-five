
#if __SHADERMODEL >= 40
// nvstereo 3D define
//#define NVSTEREO

#define CBSHARED	shared
#define SHARED
#define SCR_VPOS	SV_Position
#else
#define CBSHARED
#define SHARED		shared
#define SCR_VPOS	VPOS
#endif

#if __FXL || __SHADERMODEL >= 40
#if __SHADERMODEL >= 40
#define BeginConstantBufferDX10(name)			cbuffer name {
#define BeginConstantBufferPagedDX10(name,page)	cbuffer name : register(page) {
#define BeginConstantBuffer(name,regbase)		cbuffer name {
#define BeginConstantBufferPaged(name,regbase,page)	cbuffer name : register(page) {
#define EndConstantBufferDX10(name)				}
#else
#define BeginConstantBufferDX10(name)
#define BeginConstantBufferPagedDX10(name,page)
#define EndConstantBufferDX10(name)
#define BeginConstantBuffer(name,regbase)	cbuffer name : register(regbase) {
#define BeginConstantBufferPaged(name,regbase,page)	cbuffer name : register(regbase) {
#endif
#define EndConstantBuffer(name)				}
#else
#define BeginConstantBuffer(name,regbase)
#define BeginConstantBufferPaged(name,regbase,page)
#define BeginConstantBufferDX10(name)
#define BeginConstantBufferPagedDX10(name,page)
#define EndConstantBuffer(name)
#define EndConstantBufferDX10(name)
#endif

#ifdef NVSTEREO
#define BEGIN_RAGE_CONSTANT_BUFFER(name,slot)	BeginConstantBufferPagedDX10(name,slot)
#else
#define BEGIN_RAGE_CONSTANT_BUFFER(name,slot)	BeginConstantBufferDX10(name)
#endif
