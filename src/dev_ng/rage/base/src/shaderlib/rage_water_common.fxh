#ifndef RAGE_COMMON_WATER_FXH
#define RAGE_COMMON_WATER_FXH

#include "rage_common.fxh"
#include "rage_samplers.fxh"

const float	CurrentTime : Scene_CurrentTime;

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
    string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	#if 1
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;


BeginSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
    //string UIName = "Refraction Texture";
    string ResourceName= "__reflectionplanert__.dds";
ContinueSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
	#if 1
		AddressU  = CLAMP;        
		AddressV  = CLAMP;
		AddressW  = CLAMP;
		MIPFILTER = NONE;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;


float SpecularIntensity : SpecularIntensity
<
	string UIName = "Specular Intensity";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.05;
> = 1.0; // was 0.5

float SpecExponent : SpecularExp
<
	string UIName = "Specular Exponent";
	float UIMin = 4.0;
	float UIMax = 10000.0;
	float UIStep = 5.0;
> = 250.0;

float ReflectionDistortion : ReflectionDistortion
<
	string UIName = "Reflection Distortion";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.1;

float RippleAmplitude : RippleAmplitude
<
	string UIName = "Ripple Amplitude";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 10.00;
	float UIStep = 0.5;
> = 2.5;//2.00;

float RippleScale : RippleScale
<
	string UIName = "Ripple Scale";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 100.00;
	float UIStep = 0.5;
> = 3.00;

const float RipplePhase : RipplePhase;

const float R0 = 0.02037f; // going by shaderX 2 advanced water effects

float tesselationDepthClampMin
<
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.1;
    string UIName = "tesselation depth clamp min";	
> = 5.0;

float tesselationDepthClampMax
<
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 1.0;
    string UIName = "tesselation depth clamp max";	
> = 50.0;


float4 Scene_ScrTesselationCorners[4];
//float2 Scene_ScrTesselationCornerUVs[4];

float4 getTesselatedPosition( float2 uv, out float2 newuv )
{
   // interpolate reciprocol depth value using screen space interpolant ( in .w )
    // also interpolate vertex attribute/depth ( in .xyz )
    float4 A01 = lerp( Scene_ScrTesselationCorners[0], Scene_ScrTesselationCorners[1], uv.x );
    // A01.xyz /= A01.w;  // the worldspace point  
    // A01.xyz *= A01.w;  // precompute A/depth
    float4 A32 = lerp( Scene_ScrTesselationCorners[3], Scene_ScrTesselationCorners[2], uv.x );
    float4 At = lerp( A01, A32, uv.y );


// could use Scene_ScrTesselationCornerUVs, but it's not really needed for the water... since the mesh should be simple 0-1 spans
// note the y has been flipped to undo the flip I did in the uv coord to reverse the directX flip
	float2 uvnew =	lerp( 
						lerp( float2(0,1)*Scene_ScrTesselationCorners[0].w, float2(1,1)*Scene_ScrTesselationCorners[1].w, uv.x ), 
						lerp( float2(0,0)*Scene_ScrTesselationCorners[3].w, float2(1,0)*Scene_ScrTesselationCorners[2].w, uv.x ), 
					uv.y );  //lerp( A01, A32, IN.UV.y );
 	newuv = uvnew / At.w;

	// convert to worldspace
	At /= At.w;
	
	return At;
}

float4  waterShaderSimpleDontuse( float3 view, float3 normal, float3 t, float3 bn, float displace, float2 Tex, float4 ScrPos ,float3 sunDir, float3 sunColor )
{
	float3 verletpert = displace * 0.1;
	view = normalize( view );
//	normal = normalize( normal );
	
	// move ripples in multiple directions
	float3 np = float3(Tex * RippleScale, Tex.y);
	float3 normal0 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.02,0.05) * RipplePhase+CurrentTime.x*0.1) * 2 - 1);// * rphase0;
	float3 normal1 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.1,-0.03) * RipplePhase+CurrentTime.x*0.1) * 2 - 1);// * rphase1;
	float3 normal2 = (float3) (tex2D(BumpSampler, (float2)np + float2(-0.1,0.05) * RipplePhase+CurrentTime.x*0.02) * 2 - 1);
	float3 normal3 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.03,0.07) * RipplePhase+CurrentTime.x*0.01) * 2 - 1);

	float2 refr = ScrPos.xy / ScrPos.w;
	
	refr = refr * 0.5 + 0.5;
	refr.y = 1-refr.y;
	
	
	float3 newNormal = normalize( normal0  + normal1  + normal2  + normal3 );
    newNormal = t * newNormal.y + bn * newNormal.x + normal * newNormal.z;
	
	refr.xy += -newNormal.xz * ReflectionDistortion * 0.1f; //0.8f * saturate(1.0/ScrPos.z);
	newNormal.y += RippleAmplitude;
	newNormal = normalize( newNormal );
	
	// fresnel term
	float cosine = dot( view,newNormal);
	float fresnel = max(cosine, -cosine);
	fresnel = R0 + (1.0 - R0) * ragePow(1.0 - fresnel, 5);
	
	// calculate specular and diffuse for sunlight
	float3 sunDiffuse =  (ragePow( saturate( dot( newNormal, sunDir ) ) , 4)) * sunColor;
	float3 sunReflectPhong = ragePow( saturate( dot( reflect( -sunDir, newNormal ), view )), SpecExponent * 2.0f ) * sunColor * SpecularIntensity;
	
	float3 reflection =  tex2D(RefractionSampler,refr).xyz + sunReflectPhong;
	float3 diffuseColor =  sunDiffuse * 1.0f;
	float3 lumiance = float3( 0.3f, 0.5f, 0.2f );
	float alphaDepth = saturate( dot(lumiance, diffuseColor) * 1.0f + fresnel );		// transparencey
	diffuseColor *= alphaDepth;
	float3 reflectionAmt = lerp( diffuseColor, reflection, fresnel );
	return float4( reflectionAmt, alphaDepth );
}
#endif  // RAGE_COMMON_WATER_FXH
