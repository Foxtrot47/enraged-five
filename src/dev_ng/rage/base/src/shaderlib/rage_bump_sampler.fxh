#include "rage_samplers.fxh"

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)

#if !__FXL 
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
#if __SHADERMODEL >= 40
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
#else
	MIPFILTER = LINEAR;
	MINFILTER = MIN_FILTER;
	MAGFILTER = LINEAR;
#endif
	LOD_BIAS = 0;
#else
	#if MIN_FILER != Linear		//don't reset if default
#if __SHADERMODEL >= 40
		MIN_LINEAR_MAG_POINT_MIP_POINT;
#else
		MINFILTER = MIN_FILTER;
#endif
	#endif
#endif
EndSampler;
