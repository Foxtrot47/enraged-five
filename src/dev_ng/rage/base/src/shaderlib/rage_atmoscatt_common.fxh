//**************************************************************//
//  RAGE Atmospheric Scattering common definitions
//
//  David Serafim 05 Dec 2005
//**************************************************************//

#ifndef RAGE_ATMOSCATT_COMMON_FXH
#define RAGE_ATMOSCATT_COMMON_FXH


#include "rage_samplers.fxh"
#include "rage_common.fxh"

#ifndef TERRAIN_SUN_BLEEDING
#define TERRAIN_SUN_BLEEDING			1
#endif
#ifndef TERRAIN_CLOUD_SHADOWS
#define TERRAIN_CLOUD_SHADOWS			0
#endif
#ifndef SKY_CLOUD_SHADOW_DETAIL
#define SKY_CLOUD_SHADOW_DETAIL			1		// number of texture fetches per cloud shadow (1=cheap to 5=expensive)	
#endif

#ifndef ATTENUATION_MAP_DECLARED
BeginSampler(sampler1D,attenuationMap,AttenuationSampler,AttenuationMap)
    string UIName = "Attenuation Map";
ContinueSampler(sampler1D,attenuationMap,AttenuationSampler,AttenuationMap)
EndSampler;
#endif

//float SunElevation : SunElevation
//<
//	string UIName = "Sun Elevation";
//	string UIWidget = "Numeric";
//	float UIMin = -10.00;
//	float UIMax = 10.00;
//	float UIStep = 0.5;
//> = -1.0;

float3 SunCentre : SunCentre;


/*	    PURPOSE
			Calculates the size of the sun
		PARAMS
			- NONE
*/
float2 CalcSunSize()
{
	// just provide a min and max value
	// min seems to be bigger than max
	const float min_size = 0.965;
	const float max_size = 0.8;
	
	// delta between those valuescl
	const float delta_size = min_size - max_size; // 0.165

	// this is a combination of magic numbers ... cool
	float sz = min_size - ((0.2/10) * delta_size);
	
	// ok these numbers also look magic ...
	// the result is 104
	float sz2 = (1-0.2) * 130;
	
	// a genius combination of magic numbers
	// this result lies between 8 .. 104.0 * 1.0 + 8.0
	sz2 = sz2 * delta_size + 8;

	// 
	return float2(sz,sz2);
}

float quicksmoothstep( float negativeEdge0byOneOverWidth, float oneOverWidth, float x )
{
	x = saturate( x * oneOverWidth + negativeEdge0byOneOverWidth );
	return x * x *( - 2*x + 3  );
}

/*	    PURPOSE
			Calculates color gradient that surrounds the sun
		PARAMS
			- Sun size 
			- sun direction vector
			- object space position of vertex 
				
	// zenith is where the y coordinate of the sun direction vector is
*/
float4 CalcSunColor( float zenith, float3 sundir, float3 view , float outerSunSize )
{
	float2 sunsz = CalcSunSize();



	// just a starting value
	
	// angle between the current vertex and the sun direction vector
	// we add 1 and divide by 2 to move the range from -1..1 to 0..1
	float3 angle =  saturate( dot(view,sundir) * float3( 0.5f * sunsz.y,-0.5f / 8.0f,0.5f) + float3( (0.5f - sunsz.x)* sunsz.y, 1 - 0.5f / 8.0f,0.5f) );
	
	// give me a nice curve 
	// this one increases first very slow but then very fast on the y axis ... so it is pretty low.
	// 
	float atten = pow(  angle.x , 32 );//pow(saturate(angle.x - sunsz.x) * sunsz.y, 32);

	// subtract another curve ...
	// 
	// ok let's check the math: x is the angle and y is the zenith
	// z=(1-(1*(x/8)))^3*(y+0.6)	
	// 
	// this can be simplified to z=(y+0.6)*(-x/8+1)^3 ...
	// there is a very low rate of change going on here
	// 
	atten -= pow( angle.y , 3) * saturate(zenith + 0.6);

	// so 
	float sunattn = atten.x + angle.z  * outerSunSize;
	float suncolor = saturate( 0.5f *sunattn);
	suncolor *= 4;
	
	float centre = smoothstep( SunCentre.x, SunCentre.y, angle.z ) * SunCentre.z;
	suncolor += centre;

	return float4( suncolor.xxx, saturate( 1 - suncolor.x ));
}

#if TERRAIN_SUN_BLEEDING
float3 CalcTerrainSunBleedingPS(float3 ScatTerm, float3 SunDirection, float SunBleeding)
{
	float2 sunsize = CalcSunSize();

	float zenith = abs(SunDirection.y);

	float angle = (ScatTerm.x + 1) / 2;
	float3 atten = pow(saturate(angle-sunsize.x)*sunsize.y,4);

	float3 bleed = (float3) tex1D(AttenuationSampler,saturate(zenith+0.3)-0.1);

	const float3 lumratio = { 0.3,0.6,0.1 };
	float lum = 1 - dot(bleed,lumratio);
	atten -= pow(1 - 0*(1-lum),2);

	float sunattn = dot(atten,lumratio) + angle;
	bleed = saturate(bleed*sunattn) * (lum*6);

	return bleed * (ScatTerm.z*(SunBleeding*0.003));
}
#endif // TERRAIN_SUN_BLEEDING

#if TERRAIN_CLOUD_SHADOWS
float2 CalcTerrainCloudShadowsVS(float3 inPos)
{
	return (inPos.xz / 1000) - 0.5;
}

float CalcTerrainCloudShadowsPS(sampler2D PerlinNoiseSampler, float2 CloudTexCoord, float CloudThreshold,
													float CloudBias, float CloudShadowStrength)
{
	float cloud = tex2D(PerlinNoiseSampler, CloudTexCoord).x;
	float amount = 1 - saturate(CloudThreshold * cloud - CloudBias);
	return cloud * amount*CloudShadowStrength;
}
#endif // TERRAIN_CLOUD_SHADOWS

struct SCATTERING_VS_OUT
{
	float3 ScatTerm;
	float Extinction;
	float InScattering;
};

SCATTERING_VS_OUT CalcScatteringVS( float4 inPos, float3 SunDirection, float HazeDistance )
{
	SCATTERING_VS_OUT OUT;

	// Terms used in the scattering equation.
	// [cos(theta), 1+cos^2(theta), s] 
	float3 view = (float3) normalize(mul(inPos, gWorldView)).xyz;

	float3 scatterm;
	// angle (theta) between sun direction (L) and view direction (V).
	// scatterm.x = saturate( dot(view,SunDirection) );
	scatterm.x = dot(view,SunDirection);
	scatterm.y = pow(scatterm.x,2) + 1;
	// distance (s)
	scatterm.z = -mul(inPos, gWorldView).z;

	OUT.ScatTerm = scatterm;

	// Extinction term E: e^(-(beta_1 + beta_2) * s) = E1
	const float2 beta = { 0.0001, 0.0004 };
	float hazedist = (1-HazeDistance) * 10;
	float extinction = beta.x*hazedist + beta.y*hazedist;
	extinction *= scatterm.z;
	extinction *= 1.4427;   // log_2 E
	extinction = exp(extinction);
	extinction = clamp(0,2,extinction);

	// apply Reflectance to E to get total net effective E
//	extinction *= soil_reflectance_spectrum;

	// Phase2(theta) = (1-g^2)/(1+g-2g*cos(theta))^(3/2)
	// theta is 180 actual theta (this corrects for sign)
	// theta = [(1-g)^2, 1+g^2, 2g]
	const float g = 0.01;
	const float3 phase = { pow(1-g,2), pow(g,2)+1, g*2 };

	float phase2 = pow(rsqrt(phase.z * scatterm.x + phase.y),3) * phase.x;

	// Inscattering (I) = (Beta'_1 * Phase_1(theta) + Beta'_2 * Phase_2(theta)) * 
	//        [1-exp(-Beta_1*s).exp(-Beta_2*s)] / (Beta_1 + Beta_2)
	float inscattering = (beta.x * scatterm.y + beta.y * phase2) * (1 - extinction);
	inscattering *= 1 / (beta.x + beta.y);
	inscattering *= 1.4;

	OUT.Extinction = 2-extinction;
	OUT.InScattering = -inscattering;

	return OUT;
}

struct SCATTERINGCOLOR_VS_OUT
{
	float4 AttnExtinction;
	float4 AttnInScattering;
};

SCATTERINGCOLOR_VS_OUT CalcScatteringColorVS( float3 ScatTerm, float Extinction, float InScattering,
																	float3 SunDirection, float4 SkyColor, float4 SkyColorStatic )
{
	SCATTERINGCOLOR_VS_OUT OUT;

	float zenith = abs(SunDirection.y);
#if __XENON
	float4 idx = float4(  saturate(zenith+0.3)-0.1, 0.0f, 0.0f, 0.0f );
	float3 attn = (float3) tex1Dlod(AttenuationSampler, idx).xyz;
#else
	float3 attn = lerp(float3(.39,.21,.01),float3(.92,.87,.74),saturate(zenith+0.1));	//FIXTHIS: approximation
#endif

	float3 hazecol = lerp((float3)SkyColorStatic.xyz,((float3)SkyColor.xyz+attn)/2,saturate(zenith*3));
	float3 sunattnd = Extinction * attn;
	float3 sunattns = lerp(InScattering*hazecol,InScattering*attn,ScatTerm.x);

	OUT.AttnExtinction = float4(sunattnd,1);
	OUT.AttnInScattering = float4(sunattns * 0.7,1);

	return OUT;
}

float4 CalcScatteringColorPS_2( float4 Diffuse, float4 AttnExtinction, float4 AttnInScattering, float HazeIntensity )
{
	return lerp(Diffuse,Diffuse*AttnExtinction + AttnInScattering,HazeIntensity);
}


float3 CalcScatteringColorPS( float3 ScatTerm, float Extinction, float InScattering, float3 SunDirection,
													float4 SkyColor, float4 SkyColorStatic, float HazeIntensity, float4 Diffuse )
{
	float zenith = abs(SunDirection.y);

	float3 attn = (float3) tex1D(AttenuationSampler,saturate(zenith+0.3)-0.1);
	float3 hazecol = lerp((float3)SkyColorStatic,((float3)SkyColor+attn)/2,saturate(zenith*3));
	float3 sunattnd = Extinction * attn;
	float3 sunattns = lerp(InScattering*hazecol,InScattering*attn,ScatTerm.x);

	float3 diffuse = (float3) Diffuse;

	return lerp(diffuse,diffuse*sunattnd + sunattns*0.7,HazeIntensity);
}

#endif
