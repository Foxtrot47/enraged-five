#define BS_Add				AlphaBlendEnable = true; SrcBlend = ONE; DestBlend = ONE; BlendOp = ADD;
#define BS_Subtract			AlphaBlendEnable = true; SrcBlend = ONE; DestBlend = ONE; BlendOp = SUBTRACT;
#define BS_Min				AlphaBlendEnable = true; SrcBlend = ONE; DestBlend = ONE; BlendOp = MIN;
#define BS_Max				AlphaBlendEnable = true; SrcBlend = ONE; DestBlend = ONE; BlendOp = MAX;
#define BS_Normal			AlphaBlendEnable = true; SrcBlend = SRCALPHA; DestBlend = INVSRCALPHA; BlendOp = ADD;
#define BS_CompositeAlpha	AlphaBlendEnable = true; SrcBlend = ONE; DestBlend = INVSRCALPHA; BlendOp = ADD;
#define BS_AlphaAdd			AlphaBlendEnable = true; SrcBlend = SRCALPHA; DestBlend = ONE; BlendOp = ADD;
#define BS_AlphaSubtract	AlphaBlendEnable = true; SrcBlend = SRCALPHA; DestBlend = ONE; BlendOp = REVSUBTRACT;
