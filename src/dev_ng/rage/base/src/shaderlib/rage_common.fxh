// INCLUDE GUARDS
#ifndef __RAGE_COMMON_FXH
#define __RAGE_COMMON_FXH

#include "rage_constants.h"
#include "rage_defines.fxh"
#include "rage_macros.fxh"
#include "rage_samplers.fxh"
#include "rage_instancing.fxh"

#if __SHADERMODEL >= 40
//shared cbuffer globalBuffer
//{
//	float4 gAllGlobals[GLOBALS_SIZE] : AllGlobals REGISTER(c0);
//}
#else
SHARED float4 gAllGlobals[GLOBALS_SIZE] : AllGlobals REGISTER(c0);
#endif

#if INSTANCED && __XENON
//Instanced parameters for instancing shaders overlap skinning constants.
//Xbox instancing requires an additional constant which defines how many instances are drawn so the proper offsets can be computed.
SHARED float4 gInstancingParams		REGISTER2(vs, PASTE2(c, INSTANCE_SHADER_CONTROL_SLOT));
#endif

#ifndef NO_SKINNING
CBSHARED BeginConstantBufferPagedDX10(rage_bonemtx,b4)
SHARED const ROW_MAJOR float3x4 gBoneMtx[SKINNING_COUNT] : WorldMatrixArray REGISTER2(vs,PASTE2(c,SKINNING_BASE));
EndConstantBufferDX10(rage_bonemtx)
#endif

#define rageSkinMtx			float3x4
float3 rageSkinTransform(float3 pos,rageSkinMtx mtx) { return mul(mtx,float4(pos,1)); }
float3 rageSkinRotate(float3 pos,rageSkinMtx mtx) { return mul((float3x3)mtx,pos); }

CBSHARED BeginConstantBufferPaged(rage_matrices,c0,b1)
	row_major SHARED float4x4 gWorld : World REGISTER(c0);
	row_major SHARED float4x4 gWorldView : WorldView REGISTER(c4);
	row_major SHARED float4x4 gWorldViewProj : WorldViewProjection REGISTER(c8);
	row_major SHARED float4x4 gViewInverse : ViewInverse REGISTER(c12);
EndConstantBuffer(rage_matrices)

#if RAGE_INSTANCED_TECH
// This is for instancing a model per viewport.
// used for cascade shadow rendering, reflection map rendering ,... etc
// anything that needs to be rendered into multiple viewport-

// these get updated at the start of instancing
CBSHARED BeginConstantBufferPagedDX10(rage_cbinst_matrices,b5)
SHARED ROW_MAJOR float4x4 gInstWorldViewProj[MAX_NUM_CBUFFER_INSTANCING] : InstWorldViewProj REGISTER(PASTE2(c,CBINST_WORLDVIEWPROJ_BASE));
SHARED ROW_MAJOR float4x4 gInstViewInverse[MAX_NUM_CBUFFER_INSTANCING] : InstInverse REGISTER(PASTE2(c,CBINST_VIEWINV_BASE));
EndConstantBufferDX10(rage_cbinst_matrics)

// these get updated per instanced object if needed
CBSHARED BeginConstantBufferPagedDX10(rage_cbinst_update,b6)
SHARED ROW_MAJOR float4x4 gInstWorld : InstWorld REGISTER(PASTE2(c,CBINST_WORLD_BASE));
SHARED uint4 gInstParam[2] : gInstParam REGISTER(PASTE2(c,CBINST_OPT_INDEX));
EndConstantBufferDX10(rage_cbinst_update)

// use zw component of gInstParam since MAX_NUM_CBUFFER_INSTANCING is 6
#define INST_NUM				gInstParam[1].z

#define INSTOPT_INDEX(x)	((gInstParam[x/4]))[x%4]
#endif

#ifdef NVSTEREO
shared Texture2D<float4> StereoParmsTexture REGISTER(t20);

#if SCALEFORM_TEXTURE
shared Texture2D<float> StereoReticuleDistTexture REGISTER(t21);
#endif

float StereoToMonoScalar(float EyeZ)
{
	float2 stereoParms = StereoParmsTexture.Load(int3(0,0,0)).xy;
	return (stereoParms.x * (EyeZ - stereoParms.y));
}

float3 StereoWorldCamOffSet()
{
	float3 stereoParams = StereoParmsTexture.Load(int3(0,1,0)).xyz;
	return stereoParams;
}

float4 StereoToMonoClipSpace(float4 StereoClipPos)
{
	float4 monoClipPos = StereoClipPos;
	float2 stereoParms = StereoParmsTexture.Load(int3(0,0,0)).xy;
	monoClipPos.x -= stereoParms.x * (monoClipPos.w - stereoParms.y);
	return monoClipPos;
}

float4 MonoToStereoClipSpace(float4 MonoClipPos)
{
	float4 stereoClipPos = MonoClipPos;
	float2 stereoParms = StereoParmsTexture.Load(int3(0,0,0)).xy;
	stereoClipPos.x += stereoParms.x * (stereoClipPos.w - stereoParms.y);
	return stereoClipPos;
}

bool IsStereoActive()
{
	float4 stereoParms = StereoParmsTexture.Load(int3(0,0,0)).rgba;
	return any(stereoParms.xy);
}
#else
float StereoToMonoScalar(float EyeZ)
{
	EyeZ;
	return 0.0f;
}

float3 StereoWorldCamOffSet()
{
	return float3(0.0f,0.0f,0.0f);
}

float4 StereoToMonoClipSpace(float4 StereoClipPos)
{
	return StereoClipPos;
}

float4 MonoToStereoClipSpace(float4 MonoClipPos)
{
	return MonoClipPos;
}

bool IsStereoActive()
{
	return false;
}
#endif

#if __SHADERMODEL >= 40

CBSHARED BeginConstantBufferPaged(rage_clipplanes,c0,b0)
	float4 ClipPlanes[RAGE_MAX_CLIPPLANES];
EndConstantBuffer(rage_clipplanes)

#define RAGE_COMPUTE_CLIP_DISTANCES( x ) RAGE_ComputeClipDistances( x , x##_ClipDistance0 )
#define RAGE_NEAR_CLIP_DISTANCE( pos ) pos##_ClipDistance0.x

void RAGE_ComputeClipDistances( float4 Pos, out float4 SV_Plane0 )
{
	SV_Plane0.x = dot(Pos, ClipPlanes[0]);
#if RAGE_MAX_CLIPPLANES >= 2
	SV_Plane0.y = dot(Pos, ClipPlanes[1]);
#else
	SV_Plane0.y = 0.0f;
#endif
#if RAGE_MAX_CLIPPLANES >= 3
	SV_Plane0.z = dot(Pos, ClipPlanes[2]);
#else
	SV_Plane0.z = 0.0f;
#endif
#if RAGE_MAX_CLIPPLANES >= 4
	SV_Plane0.w = dot(Pos, ClipPlanes[3]);
#else
	SV_Plane0.w = 0.0f;
#endif
}

#else

#define RAGE_COMPUTE_CLIP_DISTANCES( x )
#define RAGE_NEAR_CLIP_DISTANCE( pos ) pos.x

#endif

#define MAX_LIGHTS 4

// Need to match grcore/light.h
#define LT_DIR 0
#define LT_POINT 1
#define LT_SPOT 2
#define LT_COUNT 3

#ifndef DISABLE_RAGE_LIGHTING

// Rage lighting uses constants c18 through c31.  If you don't use rage
// lighting you are free to recycle these as you see fit.
CBSHARED BeginConstantBufferPaged(rage_lighting,c20,b2)

SHARED float4 gLightType : LightType REGISTER(c18)
<
	string UIName = "The type of each light source";
> = float4( (float) LT_DIR, (float) LT_DIR, (float) LT_DIR, (float) LT_DIR);

SHARED float4 gLightAmbient : Ambient REGISTER(c19)
<
    string UIWidget = "Ambient Light Color";
    string Space = "material";
> = {0.0f, 0.0f, 0.0f, 1.0f};

SHARED float4 gLightDir[MAX_LIGHTS] : Direction  REGISTER(c20)
<
	string Object = "Light Direction";
	string Space = "World";
> = { {0.0f, 0.0f, -1.0f, 0.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

SHARED float4 gLightPosDir[MAX_LIGHTS] : Position  REGISTER(c24)
<
	string Object = "PointDirLight";
	string Space = "World";
> = { {1403.0f, 1441.0f, 1690.0f, 0.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

SHARED float4 gLightColor[MAX_LIGHTS] : Diffuse REGISTER(c28)
<
    string UIName = "Diffuse Light Color";
    string Object = "LightPos";
> = { {1.0f, 1.0f, 1.0f, 1.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

#define gLightCount 4

EndConstantBuffer(rage_lighting)

#endif // DISABLE_RAGE_LIGHTING

//#if !HACK_GTA4
//# if	__XENON
//shared float gInvColorExpBias : ColorExpBias REGISTER(c27);
//# else
//# define	gInvColorExpBias	1.0
//# endif
//#endif // HACK_GTA4

// common rage structs:
struct rageVertexInput {
	// This covers the default rage vertex format (non-skinned)
    float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float3 normal		: NORMAL;
};

#if !HACK_GTA4
CBSHARED BeginConstantBufferPaged(rage_ptx,c16,b3)
	SHARED float4 gForcedColor : ForcedColor REGISTER(c16) = {1,1,1,1};
	SHARED float4 gAspectRatio: AspectRatio REGISTER(c17) = {1,1,1,1};
EndConstantBuffer(rage_ptx)


// xyt.xy contains the world-space width and height of the item to billboard
// xyt.z contains the theta value.
// Returns the rotated offset in projective space (including the aspect ratio)
float4 rageComputeBillboard(float3 xyt)
{
	float sinTheta, cosTheta;
	sincos(xyt.z, sinTheta, cosTheta);
	// x1 = x cos theta - y sin theta
	// y1 = x sin theta + y cos theta
	
	return float4(xyt.x * cosTheta - xyt.y * sinTheta, xyt.x * sinTheta + xyt.y * cosTheta, 0, 0) * gAspectRatio;
}
#else
#define rageComputeBillboard rageComputeBillboardNoAspect
#endif // !HACK_GTA4

// xyt.xy contains the world-space width and height of the item to billboard
// xyt.z contains the theta value.
// Returns the rotated offset in projective space
float4 rageComputeBillboardNoAspect(float3 xyt)
{
	float sinTheta, cosTheta;
	sincos(xyt.z, sinTheta, cosTheta);
	// x1 = x cos theta - y sin theta
	// y1 = x sin theta + y cos theta
	
	return float4(xyt.x * cosTheta - xyt.y * sinTheta, xyt.x * sinTheta + xyt.y * cosTheta, 0, 0);
}

struct rageVertexInputBump {
	// This covers the default rage vertex format (non-skinned)
    float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float3 normal		: NORMAL;
	float4 tangent		: TANGENT0;
};

#ifdef RAGE_ENABLE_OFFSET
struct rageVertexOffset {
	float3 pos			: POSITION1;
	float4 diffuse		: COLOR1;
};

struct rageVertexOffsetBump {
    float3 pos			: POSITION1;
	float4 diffuse		: COLOR1;
    float3 normal		: NORMAL1;
	float4 tangent		: TANGENT1;
};
#endif

struct rageVertexOutputPassThrough {
	// This is used for the simple vertex and pixel shader routines
    DECLARE_POSITION(pos)
    float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
	float2 texCoord1	: TEXCOORD1;
	float3 normal		: TEXCOORD2;
};

// special struct for screen space blits
struct rageVertexOutputPassThroughTexOnly {
	// This is used for the simple vertex and pixel shader routines
    DECLARE_POSITION(pos)
    float4 texCoord0	: TEXCOORD0;
};

// special struct for screen space blits
struct rageVertexOutputPassThrough4TexOnly {
	// This is used for the simple vertex and pixel shader routines
    DECLARE_POSITION(pos)
    float4 texCoord0	: TEXCOORD0;
    float4 texCoord1	: TEXCOORD1;
};

struct rageLightOutput {
	float4 lightPosDir;				// Light position/direction
	float4 lightDir;				// Spot Light Direction
};


// common rage functions:
// Simple passthrough vertex shader - useful for operations that are primarily per-pixel
rageVertexOutputPassThrough VS_ragePassThrough(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
	OUT.pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.normal = mul(float4(IN.normal, 1.0), gWorld).xyz;
    return OUT;
}

// Even simpler passthrough vertex shader - this one assumes positions are pretransformed
//	Good for screen space blits.
rageVertexOutputPassThrough VS_ragePassThroughNoXform(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
//	OUT.pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
	OUT.pos = float4(IN.pos, 1.0);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.normal = float3(0.0, 0.0, -1.0);	// Force normal to face camera
    return OUT;
}

// even simpler 
// Even simpler passthrough vertex shader - this one assumes positions are pretransformed
//	Good for screen space blits.
rageVertexOutputPassThroughTexOnly VS_ragePassThroughNoXformTexOnly(rageVertexInput IN)
{
 	rageVertexOutputPassThroughTexOnly OUT;
//	OUT.pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
	OUT.pos = float4(IN.pos, 1.0);
    OUT.texCoord0.xy = IN.texCoord0;
    OUT.texCoord0.zw = 0;

    return OUT;
}

#ifndef DISABLE_RAGE_LIGHTING

#ifdef SIMPLIFIED_RAGE_LIGHTING

rageLightOutput rageComputeLightData(float3 inWorldPos, int idx)
{
	rageLightOutput OUT;

	float4 pointLight;
	pointLight.xyz = (gLightPosDir[idx].xyz - inWorldPos.xyz);
	pointLight.w = length(pointLight.xyz);
	if (pointLight.w)
		pointLight.xyz /= pointLight.w;
		
	OUT.lightPosDir = pointLight;
	OUT.lightDir = float4(0,0,0,0);
	return OUT;
}

float3 rageComputeLightColor(float3 inWorldPos, float3 inWorldNormal, int index)
{
	rageLightOutput lightData = rageComputeLightData(inWorldPos, index);
	float3 L = lightData.lightPosDir.xyz;

	// Compute falloff term
	float falloff = 1.0 - min((lightData.lightPosDir.w / gLightPosDir[index].w), 1.0);
	falloff = min(falloff, 1.0);

	float3 light = max(dot(inWorldNormal,L), 0.0);
	
	// Sum up color contribution of light sources
	float3 lightColor = gLightColor[index].rgb * light.y * falloff;
	
	return lightColor;
}


#else	// SIMPLIFIED_RAGE_LIGHTING

// Lighting function helpers
rageLightOutput rageComputeLightData(float3 inWorldPos, int idx)
{
	rageLightOutput OUT;

	float4 pointLight;
	pointLight.xyz = (gLightPosDir[idx].xyz - inWorldPos.xyz);
	pointLight.w = length(pointLight.xyz);
	if (pointLight.w)
		pointLight.xyz /= pointLight.w;
		
	float4 dirLight;
	dirLight.xyz = -gLightPosDir[idx].xyz; // Should do this on the CPU side
	dirLight.w = 0.0;  // Should do this on the CPU side

	// Count on the compiler to inline the idx check!
	float fType = idx==0? gLightType.x : idx==1? gLightType.y : idx==2? gLightType.z : gLightType.w;
	OUT.lightPosDir = lerp(dirLight, pointLight, min(fType,1.f));
	OUT.lightDir = gLightDir[idx];
	return OUT;
}

float3 rageComputeLightColor(float3 inWorldPos, float3 inWorldNormal, int index)
{
	rageLightOutput lightData = rageComputeLightData(inWorldPos, index);
	float3 L = lightData.lightPosDir.xyz;

	// Compute falloff term
	float falloff = 1.0 - min((lightData.lightPosDir.w / gLightPosDir[index].w), 1.0);
	float lightIntensity = gLightColor[index].w;
	if ( lightIntensity < 0.f ) 
	{
		falloff *= -lightIntensity;		// Allow oversaturation
	}
	else 
	{
		falloff = min(falloff * lightIntensity, 1.0);
	}

	float3 light = max(dot(inWorldNormal,L), 0.0);
	
	if (lightData.lightDir.w > 0.0f)
	{
		float spotEffect = max(dot(lightData.lightDir.xyz, lightData.lightPosDir.xyz), 0.0f);
		light *= (spotEffect > lightData.lightDir.w) ? spotEffect - lightData.lightDir.w : 0;
	}

	// Sum up color contribution of light sources
	float3 lightColor = gLightColor[index].rgb * light.y * falloff;
	
	return lightColor;
}

#endif // !SIMPLIFIED_RAGE_LIGHTING

#endif // DISABLE_RAGE_LIGHTING


// This mimics the intrinsic "lit" function, but uses less instructions (faster also??)
float4 rageGetLitFactors( float NdotL, float NdotH, float power )
{
	float4 OUT;
//	OUT = lit(NdotL, NdotH, power);
	OUT.y = max(NdotL, 0.0);
	if ( OUT.y <= 0 )
		OUT.z = 0.0;
	else
		OUT.z = ragePow((max(NdotH, 0.0)),power);
	OUT.xw = 1.0;
// NOTE: Had to rework to reduce instruction count (just the lit
//		command alone puts us over the top)
//    float4 OUT = lit( dot(N, L), dot(N, H), specularColor.w );
//    OUT.y *= IN.lightDistDir0.w;
//	OUT.y = min(light0.y, 1.0);

	return OUT;
}

// Computes the binormal given the normal & tangent (accounts for mirroring UV's)
float3 rageComputeBinormal( float3 normal, float4 tangent )
{
	float3 OUT;
	OUT = cross((float3)tangent.xyz, normal);
	OUT *= tangent.w;	// w component contains the sign of the binormal

	return OUT;
}

// Compute the Depth value of a given point
// (all platforms use 0 to 1 now, no PS3-specific code here any longer)
float rageComputeDepth(float4 p)
{
	float depth = p.z / p.w;
	
	return depth;
}

float3x3 invert_3x3( float3x3 M )
{
	float D = determinant( M );		
	float3x3 T = transpose( M );	

	return float3x3(
		cross( T[1], T[2] ),
		cross( T[2], T[0] ),
		cross( T[0], T[1] ) ) / D;	
}


float3x3 invert_3x3_nodet( float3x3 M )
{
	float3x3 T = transpose( M );	

	return float3x3(
		cross( T[1], T[2] ),
		cross( T[2], T[0] ),
		cross( T[0], T[1] ) );	
}

float3x3 rageComputeHighQualityTangentFrame(float3 normal, float3 position, float2 texCoords)
{
	// Implemenataion with full matrix inverse
	// straight from theory.

    // get edge vectors of the pixel triangle
    float3 positionDdx = ddx(position);
    float3 positionDdy = ddy(position);
    float2 texCoordsDdx = ddx(texCoords);
    float2 texCoordsDdy = ddy(texCoords);

    // solve the linear system
    float3x3 M = float3x3(positionDdx, positionDdy, cross(positionDdx, positionDdy));
    float3x3 inverseM = invert_3x3(M);
    float3 T = mul(inverseM, float3(texCoordsDdx.x, texCoordsDdy.x, 0));
    float3 B = mul(inverseM, float3(texCoordsDdx.y, texCoordsDdy.y, 0));

    // construct tangent frame 
    float maxRecipLength = rsqrt(max(dot(T, T), dot(B, B)));
    return float3x3(T * maxRecipLength, B * maxRecipLength, normal);
}

float3x3 rageComputeTangentFrame(float3 normal, float3 position, float2 texCoords)
{
	float3 positionDdx = ddx(position);
	float3 positionDdy = ddy(position);
	float2 texCoordsDdx = ddx(texCoords);
	float2 texCoordsDdy = ddy(texCoords);
	
	float3x3 tangentFrame;
	tangentFrame[0] = normalize(positionDdx * texCoordsDdx.x + positionDdy * texCoordsDdy.x);
	tangentFrame[1] = normalize(positionDdx * texCoordsDdx.y + positionDdy * texCoordsDdy.y);
	tangentFrame[2] = normal;
	
	return tangentFrame;
}

#if RAGE_PREVWORLDVIEWPROJ
CBSHARED BeginConstantBufferDX10(rage_PWVPBuffer)
	row_major SHARED float4x4 gPrevWorldViewProj : PrevWorldViewProjection REGISTER(MacroJoin(c, RAGE_PREVWORLDVIEWPROJ_REGISTER));
EndConstantBufferDX10(rage_PWVPBuffer);
#endif // RAGE_PREVWORLDVIEWPROJ

// RAGE_MOTIONBLUR is defined in rage_constants.h

#if RAGE_MOTIONBLUR
#	define RAGE_MOTIONBLUR_ONLY(x) x
	float gMotionBlurScalar = 1.0f;
#else
#	define RAGE_MOTIONBLUR_ONLY(x)
#endif // RAGE_MOTIONBLUR

#if RAGE_VELOCITYBUFFER
#	define RAGE_VELOCITYBUFFER_ONLY(x) x
#	define RAGE_VELOCITYBUFFER_PSOUTPUT(x) out float4 x : MacroJoin(COLOR, RAGE_MOTIONBLUR_MRT),
#	define RAGE_VELOCITYBUFFER_PSOUTPUT_MEMBER(x) float4 x : MacroJoin(COLOR, RAGE_MOTIONBLUR_MRT);

	void rageComputeVelocityBufferVs(out float4 prevPosProj, float4 localPos)
	{
		prevPosProj = mul(localPos, gPrevWorldViewProj);
	}

	void rageComputeVelocityBufferPs(out float4 packedVelocity, float4 currPosProj, float4 prevPosProj, half blurScalar)
	{
		float2 velocity = (currPosProj.xy / currPosProj.w - prevPosProj.xy / prevPosProj.w) * 0.5f * blurScalar;
#		if __PS3
			packedVelocity = unpack_4ubyte(pack_2half(velocity));
#		else
			packedVelocity = float4(velocity, 0.0f, 1.0f);
#		endif
	}
#else
#	define RAGE_VELOCITYBUFFER_ONLY(x)
#	define RAGE_VELOCITYBUFFER_PSOUTPUT(x)
#	define RAGE_VELOCITYBUFFER_PSOUTPUT_MEMBER(x)
#	define rageComputeVelocityBufferVs(prevPosProj, localPos)
#	define rageComputeVelocityBufferPs(packedVelocity, currPosProj, prevPosProj, blurScalar)
#endif // RAGE_VELOCITYBUFFER

#if !HACK_GTA4 // no shader clip planes (c49+ is used for cascade shadows anyway)
#	if __PS3
#		define gClipPlaneCount 6
		shared float4 gClipPlanes[gClipPlaneCount] : ClipPlanes REGISTER(c49);
		shared bool gEnableClipPlane0 : EnableClipPlane0 REGISTER(b26);
		shared bool gEnableClipPlane1 : EnableClipPlane1 REGISTER(b27);
		shared bool gEnableClipPlane2 : EnableClipPlane2 REGISTER(b28);
		shared bool gEnableClipPlane3 : EnableClipPlane3 REGISTER(b29);
		shared bool gEnableClipPlane4 : EnableClipPlane4 REGISTER(b30);
		shared bool gEnableClipPlane5 : EnableClipPlane5 REGISTER(b31);
#		pragma constant 55
#	else
#		pragma constant 50
#	endif // __PS3 
#endif // !HACK_GTA4

// Clip plane support
#if __PS3 && !HACK_GTA4 // no shader clip planes (c49+ is used for cascade shadows anyway)
struct rageVertexOutputClipPlanes
{
	float clip0 : CLP0;
	float clip1 : CLP1;
	float clip2 : CLP2;
	float clip3 : CLP3;
	float clip4 : CLP4;
	float clip5 : CLP5;
};
#define RAGE_CLIPPLANE_VSOUTPUT(name) out rageVertexOutputClipPlanes name,
#define RAGE_CLIPPLANE_VSOUTPUT_MEMBER(name) rageVertexOutputClipPlanes name;
#else
#define RAGE_CLIPPLANE_VSOUTPUT(name)
#define RAGE_CLIPPLANE_VSOUTPUT_MEMBER(name)
#endif

#if __PS3 && !HACK_GTA4 // no shader clip planes
#define rageComputeClipPlanes(pos, clipPlaneOutput)				\
do																\
{																\
	float4 clipPos = pos;										\
	if (gEnableClipPlane0)										\
	{															\
		clipPlaneOutput.clip0 = dot(clipPos, gClipPlanes[0]);	\
	}															\
	else { clipPlaneOutput.clip0 = 1.0f; }						\
	if (gEnableClipPlane1)										\
	{															\
		clipPlaneOutput.clip1 = dot(clipPos, gClipPlanes[1]);	\
	}															\
	else { clipPlaneOutput.clip1 = 1.0f; }						\
	if (gEnableClipPlane2)										\
	{															\
		clipPlaneOutput.clip2 = dot(clipPos, gClipPlanes[2]);	\
	}															\
	else { clipPlaneOutput.clip2 = 1.0f; }						\
	if (gEnableClipPlane3)										\
	{															\
		clipPlaneOutput.clip3 = dot(clipPos, gClipPlanes[3]);	\
	}															\
	else { clipPlaneOutput.clip3 = 1.0f; }						\
	if (gEnableClipPlane4)										\
	{															\
		clipPlaneOutput.clip4 = dot(clipPos, gClipPlanes[4]);	\
	}															\
	else { clipPlaneOutput.clip4 = 1.0f; }						\
	if (gEnableClipPlane5)										\
	{															\
		clipPlaneOutput.clip5 = dot(clipPos, gClipPlanes[5]);	\
	}															\
	else { clipPlaneOutput.clip5 = 1.0f; }						\
} while (false)
#else
#define rageComputeClipPlanes(pos, clipPlaneOutput)
#endif

// compute 4 weights using the cubic polynomial:
// w = (A+2.0)*x^3 - (A+3.0)*x^2         + 1.0    for 0<x<1
// w = A*x^3 -       5.0*A*x^2 + 8.0*A*x - 4.0*A  for 1<x<2
// A controls sharpness
half4 rageComputeCubicWeights(half x)
{
	// calculate weights in parallel
	half4 x1 = x*half4(1,1,-1,-1) + half4(1,0,1,2); // (x+1, x, 1-x, 2-x)
	half4 x2 = x1*x1;
	half4 x3 = x2*x1;
	
//	const half A = -1;
	const half A = -0.75;
	half4 w;	
	w =  x3 * half2(  A,      A+2.0).xyyx;
	w += x2 * half2( -5.0*A, -(A+3.0)).xyyx;
	w += x1 * half2(  8.0*A,  0).xyyx;
	w +=      half2( -4.0*A,  1.0).xyyx;
	return w;
}

// filter 4 values
half4 rageCubicFilter(half4 w, half4 c0, half4 c1, half4 c2, half4 c3)
{
	return c0*w[0] + c1*w[1] + c2*w[2] + c3*w[3];
}

half4 rageTex2DBicubic(sampler2D tex, half2 texCoords, half4 texelAndTextureSize)
{
	half2 f = (half2)frac(texCoords * texelAndTextureSize.zw); // fractional position within texel
	
	// filter in x
	half4 w = rageComputeCubicWeights(f.x);
	half4 t0 = rageCubicFilter(w,	(half4)h4tex2D(tex, texCoords+half2(-1, -1)*texelAndTextureSize.xy),
							  	(half4)h4tex2D(tex, texCoords+half2(0, -1)*texelAndTextureSize.xy),
							  	(half4)h4tex2D(tex, texCoords+half2(1, -1)*texelAndTextureSize.xy),
							  	(half4)h4tex2D(tex, texCoords+half2(2, -1)*texelAndTextureSize.xy) );
	half4 t1 = rageCubicFilter(w,	(half4)h4tex2D(tex, texCoords+half2(-1, 0)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(0, 0)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(1, 0)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(2, 0)*texelAndTextureSize.xy) );
	half4 t2 = rageCubicFilter(w,	(half4)h4tex2D(tex, texCoords+half2(-1, 1)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(0, 1)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(1, 1)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(2, 1)*texelAndTextureSize.xy) );
	half4 t3 = rageCubicFilter(w,	(half4)h4tex2D(tex, texCoords+half2(-1, 2)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(0, 2)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(1, 2)*texelAndTextureSize.xy),
								(half4)h4tex2D(tex, texCoords+half2(2, 2)*texelAndTextureSize.xy) );
	// filter in y								
	w = rageComputeCubicWeights(f.y);
	return rageCubicFilter(w, t0, t1, t2, t3);
}

#if __PS3
float4 rageUnpack4UByte(float packedValue)
{
	unsigned int packedValueUInt = (unsigned int)packedValue;
	unsigned int4 temp;
	temp.x = packedValueUInt * 16777216;
	temp.y = packedValueUInt * 65536;
	temp.z = packedValueUInt * 256;
	temp.w = packedValueUInt;
	temp /= 16777216;

	float4 unpackedValue = (float4)temp;
	unpackedValue /= 255.0f;

	return unpackedValue;
}

float rageDepthUnpack(float4 depthSample)
{
	float3 depthFactor = float3( 65536.0/16777215.0, 256.0/16777215.0, 1.0/16777215.0 );
	float depth = dot( round( depthSample.rgb * 255.0 ), depthFactor );
	return depth;
}
#endif // __PS3

// usage: rageReconstructNormal(tex2D(normalMap, texCoords).ga ...
float3 rageReconstructNormal(float2 bumpXy, float3 tangent, float3 binormal, float3 normal, float intensity)
{
	float3 bump;
	bump.xy = bumpXy;
	bump.xy = bump.xy * 2.0f - 1.0f;
	bump.xy *= intensity;
	bump.z = sqrt(1.0f - dot(bump.xy, bump.xy));
	return normalize(bump.x * tangent + bump.y * binormal + bump.z * normal);
}

// usage: rageReconstructPartialDerivativeNormal(tex2D(normalMap, texCoords).gab ...
float3 rageReconstructPartialDerivativeNormal(float2 pd, float3 tangent, float3 binormal, float3 normal, float intensity)
{
	float2 bump = pd * -2.0f + 1.0f;
	bump *= intensity;
	return normalize(bump.x * tangent + bump.y * binormal + normal);
}

float4 rageGenerateNormalFromHeightMap(sampler2D HeightSampler, float2 TexCoord, float2 TexelSize, float Bumpiness)
{
	float4 LRTB;  // left right bottom top.

#define OFFSET 0.5f    // using .5, though 1.0 is probably more correct, it results in more texture cache misses...
#if __XENON				 // Xenon has fast texel offsets, so use them
	asm {
        tfetch2D LRTB.x___,  TexCoord.xy, HeightSampler, OffsetX=-OFFSET, OffsetY=0    
        tfetch2D LRTB._x__,  TexCoord.xy, HeightSampler, OffsetX=OFFSET, OffsetY=0
        tfetch2D LRTB.__x_,  TexCoord.xy, HeightSampler, OffsetX=0, OffsetY=-OFFSET
        tfetch2D LRTB.___x,  TexCoord.xy, HeightSampler, OffsetX=0, OffsetY=OFFSET
    };
#else
	TexelSize.xy *= OFFSET;
	LRTB.x = tex2D(HeightSampler, TexCoord.xy + float2(-TexelSize.x, 0)).r;
	LRTB.y = tex2D(HeightSampler, TexCoord.xy + float2( TexelSize.x, 0)).r;
	LRTB.z = tex2D(HeightSampler, TexCoord.xy + float2(0, -TexelSize.y)).r;
	LRTB.w = tex2D(HeightSampler, TexCoord.xy + float2(0,  TexelSize.y)).r;
#endif // __XENON
#undef OFFSET

	LRTB *= Bumpiness;
	float4 N = float4(LRTB.xx - LRTB.yz, 1, LRTB.w);
	return(N);
}

float3 rageReflectBetter(float3 worldPos, float3 envMapCenter, float3 N, float envMapRadiusSquared)
{
	float3 I = -normalize(worldPos - gViewInverse[3].xyz);
	float3 R = 2 * dot(N, I) * N - I;
	float radius = envMapRadiusSquared; 
	
	float3 pos = worldPos - envMapCenter;
		
	//compute the projection of the reflected vetor onto a "room sized sphere"
	// NOTE: the sphere needs to big enough to cover the room, to insure the quadratic solution is good
	float b = 2*dot(R,pos.xyz);
	float c = dot(pos.xyz,pos.xyz) - radius; // EnvPosAdjust = radius of room sphere 
	float sqrt_b2_4c = sqrt(b*b-4*c);
	float t = (-b+sqrt_b2_4c)*0.5f; // first quadratic solution.
	//if (t<0) t = (-b-sqrt_b2_4c)*0.5; // the other solution... (this would only be needed if the world pos is farther from the origin than the radius, which is probably bad tuning...)
	
	R = (pos+t*R);
	return R;
}

float rageGetLuminance(float3 color)
{
	float desat = dot(color.rgb,float3(0.3f,0.59f,0.11f));
	return desat;
}

float3 rageDesaturateColor(float3 color, float amount)
{
	float3 desat = dot(color.rgb,float3(0.3f,0.59f,0.11f));
	return lerp(color, desat, amount);
}

float rageCalcFresnelTerm(float3 N, float3 E, float exponent, float low, float hi)
{
	float fresnel = 1.0f - saturate(abs(dot(E, N)));
	fresnel = ragePow(fresnel, exponent);
	fresnel = lerp(low, hi, fresnel);
	return fresnel;
}


float rageCalcFresnelTermVdotN(float VdotN, float exponent, float low, float hi)
{
	float fresnel = saturate(1.0f - VdotN);
	fresnel = ragePow(fresnel, exponent);
	fresnel = lerp(low, hi, fresnel);
	return fresnel;
}

float3 rageShiftNormal(float3 normal, float3 view, float3 shift)
{
	float3 shiftedNormal = normal + shift * view;
	return normalize(shiftedNormal);
}

float3 rageAddSmooth(float3 c1, float3 c2)
{
	return c1 + c2 - saturate(c1 * c2);
}

float rageAddSmoothSingle(float c1, float c2)
{
	return c1 + c2 - saturate(c1 * c2);
}

float4 rageCalcScreenSpacePosition(float4 Position)
{
	float4	ScreenPosition;

	ScreenPosition.x = Position.x * 0.5 + Position.w * 0.5;
	ScreenPosition.y = Position.w * 0.5 - Position.y * 0.5;
	ScreenPosition.z = Position.w;
	ScreenPosition.w = Position.w;

	return(ScreenPosition);
}

// bufferWidth is the texture width, so a 1280x720 MSAA 2x surface would have a width of 1280x2=2560
float2 rageGetPs3Msaa2xSampleA(float2 texCoord, float bufferWidth)
{
	float2 sampleTexCoord;
	float denormX = texCoord.x * bufferWidth;
	float roundedX = 2.0f * round((denormX + 1.0f) / 2.0f);
	sampleTexCoord.x = (roundedX - 1.5f) / bufferWidth;
	sampleTexCoord.y = texCoord.y;
	
	return sampleTexCoord;
}

float2 rageGetPs3Msaa2xSampleB(float2 texCoord, float bufferWidth)
{
	float2 sampleTexCoord = rageGetPs3Msaa2xSampleA(texCoord, bufferWidth);
	sampleTexCoord.x += 1.0f / bufferWidth;
	
	return sampleTexCoord;
}

// http://number-none.com/product/Understanding%20Slerp,%20Then%20Not%20Using%20It/index.html
// "There's a cosmetic difference between this slerp and the Shoemake code. Where I use a
// Normalize function, Shoemake divides by sin(angt).  Indeed this has the same effect as a
// Normalize; some trig will tell you that the length of 'c' prior to normalization is
// sin(angt), so the divide turns it back into a unit vector.  I like to use the explicit
// Normalize, though, because it emphasizes the vector nature of the computation."
float3 rageSlerp(float3 a, float3 b, float t)
{
    float adb = dot(a, b);
    float angt = acos(adb) * t;
    float3 c = normalize(b - adb * a);
    float2 angtSinCos;
    sincos(angt, angtSinCos.x, angtSinCos.y);
    return a * angtSinCos.y + c * angtSinCos.x;
}

float3 rageNlerp(float3 a, float3 b, float t)
{
	return normalize(lerp(a, b, t));
}

int rageGetMipMapCount1D(int width)
{
	return ceil(rageiLog2(width));
}

int rageGetMipMapCount2D(int width, int height)
{
	return max(ceil(rageiLog2(width)), ceil(rageiLog2(height)));
}

int rageGetMipMapCount3D(int width, int height, int depth)
{
	return max(max(ceil(rageiLog2(width)), ceil(rageiLog2(height))), ceil(rageiLog2(depth)));
}

static const float FLT_MAX = 340282346600000000000000000000000000000.0f;
// This number is too small to parse properly anyway.
// static const float FLT_MIN = 0.00000000000000000000000000000000000001175494351f;
static const float PI = 3.14159265358979323846264338327950288f;
#define INV_PI (1.0 / PI)
static const float M_E = 2.71828182846f;
#define TWO_PI (2.0 * PI)

#include "rage_alternativecolorspaces.fxh"

#endif // __RAGE_COMMON_FXH
