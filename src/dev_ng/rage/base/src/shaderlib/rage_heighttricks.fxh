#ifndef RAGE_HEIGHTTRICKS_H
#define RAGE_HEIGHTTRICKS_H

float Bumpiness
<
	string UIName = "Bumpiness";
	string UIWidget = "Numeric";
	float UIMin = 0;
	float UIMax = 10.0f;
	float UIStep = 0.01;
> = 1.0;

float4 HeightGetNeighbours(sampler2D HSampler, float2 TexCoord, float2 texelSize)
{   	
	float4 pos;
	pos.x = tex2D(HSampler, TexCoord.xy + float2( texelSize.x,   0)).x;
   	pos.y = tex2D(HSampler, TexCoord.xy + float2(0, texelSize.y)).x;
   	pos.z = tex2D(HSampler, TexCoord.xy + float2(-texelSize.x, 0)).x;
   	pos.w = tex2D(HSampler, TexCoord.xy + float2(0, -texelSize.y)).x;
    return pos;
}
float3 HeightGenerateNormalFromHeights( float4 pos, float Bump )
{
	pos = pos * pos * Bump;
   	return float3( pos.zw - pos.xy, 1.0f );
}
float4 HeightCalculateEffects( float h )
{
	float4 val;
	float4 h2 = h.xxxx;
	h2.xzw = h.x * h.x;
	
	// do with scale bias stuff
	float4 scale = float4( 0.5, 1.0f, -1.5, 0.25f );
	float4 bias = float4( 0.5, 0.0f, 1.0f, 0.75f );
	
	return saturate( h2 * scale + bias );
}

float HeightGetInfo( float2 tex, sampler2D s, out float3 norm, out float4 val )
{
	// get normal
	float4 myHeights = HeightGetNeighbours( s, tex, 1.0/512.0 );
	norm =  HeightGenerateNormalFromHeights( myHeights, Bumpiness );
	float h = tex2D(s, tex ).x;
	val = HeightCalculateEffects( h);
	return h;
}
float HeightCalculateApproxSelfShadowing( float VNdotL, float3 surfVal )
{
	return saturate(saturate(  VNdotL * 1.5   - surfVal.z )  * 4.0) * saturate( surfVal.y * 2.0f)  ;
	//return saturate(saturate(  VNdotL * 1.5   - surfVal.z )  * 4.0) ;
}
float4 HeightCalculateApproxSelfShadowingx4( float4 VNdotL, float3 surfVal )
{
	return saturate(saturate(  VNdotL * 1.5   - surfVal.zzzz )  * 4.0) ;
}

#endif
