#ifndef SAMPLERS_FXH
#define SAMPLERS_FXH

#if __FXL
#define DEFINE_SAMPLER_FULL(texName,semantic,samplerName,s1,s2,s3,s4,addr,mipf,minf,magf) \
sampler2D samplerName : semantic < string s1=s2; string s3=s4; > = sampler_state { \
	AddressU = addr; AddressV = addr; AddressW = addr; \
	MipFilter = mipf; MinFilter = minf; MagFilter = magf; }
#else
#define DEFINE_SAMPLER_FULL(texName,semantic,samplerName,s1,s2,s3,s4,addr,mipf,minf,magf) \
texture texName : semantic < string s1=s2; string s3=s4; >; \
sampler2D samplerName = sampler_state { texture = <texName>; \
	AddressU = addr; AddressV = addr; AddressW = addr; \
	MipFilter = mipf; MinFilter = minf; MagFilter = magf; }
#endif

#define DEFINE_SAMPLER(texName,semantic,samplerName,uiName,uiHint) \
DEFINE_SAMPLER_FULL(texName,semantic,samplerName,UIName,uiName,UIHint,uiHint,WRAP,LINEAR,LINEAR,LINEAR)

#if __SHADERMODEL >= 40
#define BeginDX10Sampler(samplerType, texType, texName, samplerName, semantic) \
	DX10TEXTURESAMPLERTOKEN texType texName samplerType samplerName : semantic  <
#define BeginDX10SamplerShared(samplerType, texType, texName, samplerName, semantic, reg) \
	DX10TEXTURESAMPLERTOKEN texType texName shared samplerType samplerName : semantic REGISTER(reg)  <
#else
#if __MAX
	#define BeginDX10Sampler(samplerType, texType, texName, samplerName, semantic) \
		texType texName : texName <
	#define BeginDX10SamplerShared(samplerType, texType, texName, samplerName, semantic, reg) \
		texType texName : texName <
#else
	#define BeginDX10Sampler(samplerType, texType, texName, samplerName, semantic) \
		samplerType samplerName : semantic <
	#define BeginDX10SamplerShared(samplerType, texType, texName, samplerName, semantic, reg) \
		shared samplerType samplerName : semantic REGISTER(reg)  <
#endif // __MAX
#endif // __SHADERMODEL

#if __FXL
#define BeginSampler(samplerType,texName,samplerName,semantic) \
	samplerType samplerName : semantic <
#define BeginSharedSampler(samplerType,texName,samplerName,semantic,reg) \
	shared samplerType samplerName : semantic REGISTER(reg) <
#define ContinueSampler(samplerType,texName,samplerName,semantic) \
	> = sampler_state {
#define ContinueSharedSampler(samplerType,texName,samplerName,semantic,reg) \
	> = sampler_state {
#else
#define BeginSampler(samplerType,texName,samplerName,semantic) \
	texture texName : semantic <
#define BeginSharedSampler(samplerType,texName,samplerName,semantic,reg) \
	shared texture texName  <
#define ContinueSampler(samplerType,texName,samplerName,semantic) \
	>; samplerType samplerName = sampler_state { texture = < texName >;
#define ContinueSharedSampler(samplerType,texName,samplerName,semantic,reg) \
	>; samplerType samplerName : semantic REGISTER(reg) = sampler_state { texture = < texName >;
#endif

#define EndSampler }
#define EndSharedSampler }

/* example:

	BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
		string UIName = "Diffuse Texture";
	ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
		AddressU = CLAMP;
		AddressV = CLAMP;
	EndSampler

  note that if the texture/sampler is a global, you can put the
  keyword "shared" before the BeginSampler.

  You MUST pass the same values into BeginSampler and ContinueSampler.
  The alternative would have been to #define and #undef the inputs ahead
  of time, which seems much more verbose to me.

  Token pasting doesn't seem to work correctly in the June 2005 DX9 FXC
  application, which is why I have to explicitly pass in the texture name
  to the BeginSampler and ContinueSampler macros.
*/

#ifndef MIN_FILTER
#define MIN_FILTER Linear
#endif

#ifndef DIFFUSE_CLAMP
#define DIFFUSE_CLAMP Wrap
#endif
#ifndef DIFFUSE2_CLAMP
#define DIFFUSE2_CLAMP Wrap
#endif
#ifndef DIFFUSE3_CLAMP
#define DIFFUSE3_CLAMP Wrap
#endif
#ifndef DIFFUSE4_CLAMP
#define DIFFUSE4_CLAMP Wrap
#endif

#ifndef LOD_BIAS
#define LOD_BIAS	MipMapLodBias
#define	MIN_POINT_MAG_POINT_MIP_POINT		MINFILTER=POINT;MAGFILTER=POINT;MIPFILTER=POINT
#define MIN_LINEAR_MAG_LINEAR_MIP_LINEAR	MINFILTER=LINEAR;MAGFILTER=LINEAR;MIPFILTER=LINEAR
#define MIN_LINEAR_MAG_LINEAR_MIP_NONE		MINFILTER=LINEAR;MAGFILTER=LINEAR;MIPFILTER=NONE
#define MIN_POINT_MAG_POINT_MIP_NONE		MINFILTER=POINT;MAGFILTER=POINT;MIPFILTER=NONE
#define MIN_ANISO_MAG_ANISO_MIP_NONE		MINFILTER=ANISOTROPIC;MAGFILTER=LINEAR;MIPFILTER=NONE
#define MIN_ANISO_MAG_ANISO_MIP_LINEAR		MINFILTER=ANISOTROPIC;MAGFILTER=LINEAR;MIPFILTER=LINEAR
#define MIN_LINEAR_MAG_POINT_MIP_POINT		MINFILTER=LINEAR
#endif

#endif	// SAMPLERS_FXH
