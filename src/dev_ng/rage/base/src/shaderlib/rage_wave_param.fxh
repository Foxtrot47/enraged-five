#ifndef RAGE_WATER_PARAM_FXH
#define RAGE_WATER_PARAM_FXH

//!me deprecated as soon as it was born!

float3 SWaveDirection : SWaveDirection
<
	string UIName = "Wave Direction";
	string UIWidget = "Numeric";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;	
> = { -1.0, 0, 0 };

float SWavePeriod : SWavePeriod
<
	string UIName = "Wave Period";
	string UIWidget = "Numeric";
	float UIMin = 0.1;
	float UIMax = 100.0;
	float UIStep = 0.1;	
> = 3.5;

float SWaveHeight : SWaveHeight
<
	string UIName = "Wave Height";
	string UIWidget = "Numeric";
	float UIMin = 0.1;
	float UIMax = 100.0;
	float UIStep = 0.1;	
> = 0.6;

float SWaveWindKludge : SWaveWindKludge
<
	string UIName = "Wave Wind Kludge";
	string UIWidget = "Numeric";
	float UIMin = -10.0;
	float UIMax = 10.0;
	float UIStep = 0.01;	
> = 0.0;

//!me remove this if you don't use it, just wastes cycles.
float SWaveInvScale : SWaveInvScale
<
	string UIName = "Wave Inv Scale";
	string UIWidget = "Numeric";
	float UIMin = 0.01;
	float UIMax = 100.0;
	float UIStep = 0.01;	
> = 1.0;

float SWaveBottomSlope : SWaveBottomSlope
<
	string UIName = "Wave Bottom Slope";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 100.0;
	float UIStep = 0.01;	
> = 0.5;

float SWaveBreakerStartOffset : SWaveBreakerStartOffset
<
	string UIName = "Wave Breaker Start Point";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 13.5;

float SWaveTimeScale : SWaveTimeScale
<
	string UIName = "Wave Time Scale";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;	
> = 1.0;

float3 SWaveShorelineOrigin : SWaveShorelineOrigin
<
	string UIName = "Wave Shoreline Origin";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;
> = { 0,0,0 };

float3 SWaveShorelineAxis : SWaveShorelineAxis
<
	string UIName = "Wave Shoreline Axis";
	string UIWidget = "Numeric";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = { 0,0,1 };

float SWaveShorelineWidth : SWaveShorelineWidth 
<
	string UIName = "Wave Shoreline Width";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 1.0;
> = 50.0f;

float SWaveBeachContourControl0 : SWaveBeachContourControl0
<
	string UIName = "Wave Beach Contour Control Point 0";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 30.0;

float SWaveBeachContourControl1 : SWaveBeachContourControl1
<
	string UIName = "Wave Beach Contour Control Point 1";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 40.0;

float SWaveBeachContourControl2 : SWaveBeachContourControl2
<
	string UIName = "Wave Beach Contour Control Point 2";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 0.0;

float SWaveBeachContourControl3 : SWaveBeachContourControl3
<
	string UIName = "Wave Beach Contour Control Point 3";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 0.0;

//--------------

float DWavePeriod1
<
	string UIName = "Period for deep wave 1";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 2.0;

float DWaveAmp1
<
	string UIName = "Amplitude for deep wave 1";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.15;

float2 DWaveDirection1
<
	string UIName = "Direction for deep wave 1";
	string UIWidget = "Numeric";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;	
> = { -1.0, 0 };

//----

float DWavePeriod2
<
	string UIName = "Period for deep wave 2";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 2.1;

float DWaveAmp2
<
	string UIName = "Amplitude for deep wave 2";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.15;

float2 DWaveDirection2
<
	string UIName = "Direction for deep wave 2";
	string UIWidget = "Numeric";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;	
> = { -1.0, 1.1 };

//----

float DWavePeriod3
<
	string UIName = "Period for deep wave 3";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 1.9;

float DWaveAmp3
<
	string UIName = "Amplitude for deep wave 3";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.15;

float2 DWaveDirection3
<
	string UIName = "Direction for deep wave 3";
	string UIWidget = "Numeric";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;	
> = { -1.0, 0.85 };

//----

float DWavePeriod4
<
	string UIName = "Period for deep wave 4";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.1;	
> = 1.95;

float DWaveAmp4
<
	string UIName = "Amplitude for deep wave 4";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.10;

float2 DWaveDirection4
<
	string UIName = "Direction for deep wave 4";
	string UIWidget = "Numeric";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;	
> = { 1.0, 0.75 };

//----

float DWaveNoiseScale
<
	string UIName = "Noise Scale";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.025;

float DWaveNoiseDt
<
	string UIName = "Noise Rate of Change";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.033;

float DWaveNoiseMagnitude
<
	string UIName = "Noise Magnitude";
	string UIWidget = "Numeric";
	float UIMin = -1000.0;
	float UIMax = 1000.0;
	float UIStep = 0.01;	
> = 0.1;

#endif
