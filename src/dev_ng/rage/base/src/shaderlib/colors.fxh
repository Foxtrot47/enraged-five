#ifndef _COLORS_FXH_
#define _COLORS_FXH_

#include "noise.fxh"

float mix( float color0, float color1, float value)
{
	return ((1.0f - value) * color0) + (value * color1);
}

float4 mix4( float4 color0, float4 color1, float value)
{
	return ((1.0f - value) * color0) + (value * color1);
}

float4 rgbToHsl(float4 rgb)
{
	float red, green, blue;
	float hue, saturation, light;
	float maxcolor, mincolor;
		
	red = rgb.x;
	green = rgb.y;
	blue = rgb.z;
	
	maxcolor = max(red, max(green, blue));
	mincolor = min(red, min(green, blue));

	if(red == green && red == blue) 
	{
		hue = saturation = 0.0;
		light = red;
	} 
	else 
	{
		light = (mincolor + maxcolor) / 2;

		if(light < 0.5)
			saturation = (maxcolor - mincolor) / (maxcolor + mincolor);
		else
			saturation = (maxcolor - mincolor) / (2.0 - maxcolor - mincolor);

		if(red == maxcolor)
			hue = (green - blue) / (maxcolor - mincolor);
		else if(green == maxcolor)
			hue = 2.0 + (blue - red) / (maxcolor - mincolor);
		else
			hue = 4.0 + (red - green) / (maxcolor - mincolor);

		/* note. not degrees.. */
		if(hue > 1)
			hue /= 6;
		if(hue < 0)
			hue++;
	}

	return float4(hue, saturation, light, rgb.w);
}

float4 hslToRgb(float4 HSL) 
{
	float red, green, blue;
	float hue, saturation, light;
	float tmpred, tmpgreen, tmpblue;
	float tmp1, tmp2;

	hue = HSL.x;
	saturation = HSL.y;
	light = HSL.z;

	if(saturation == 0)
		red = green = blue = light;
	else 
	{
		if(light < 0.5)
			tmp2 = light * (1 + saturation);
		else
			tmp2 = (light + saturation) - (light * saturation);

		tmp1 = 2 * light - tmp2;
		tmpred = hue + 1.0 / 3.0;

		if(tmpred > 1)
			tmpred--;

		tmpgreen = hue;
		tmpblue = hue - 1.0 / 3.0;

		if(tmpblue < 0)
			tmpblue++;

		/* red */
		if(tmpred < 1.0 / 6.0)
			red = tmp1 + (tmp2 - tmp1) * 6.0 * tmpred;
		else if(tmpred < 0.5)
			red = tmp2;
		else if(tmpred < 2.0 / 3.0)
			red = tmp1 + (tmp2 - tmp1) * ((2.0 / 3.0) - tmpred) * 6.0;
		else
			red = tmp1;

		/* green */
		if(tmpgreen < 1.0 / 6.0)
			green = tmp1 + (tmp2 - tmp1) * 6.0 * tmpgreen;
		else if(tmpgreen < 0.5)
			green = tmp2;
		else if(tmpgreen < 2.0 / 3.0)
			green = tmp1 + (tmp2 - tmp1) * ((2.0 / 3.0) - tmpgreen) * 6.0;
		else
			green = tmp1;

		/* blue */
		if(tmpblue < 1.0 / 6.0)
			blue = tmp1 + (tmp2 - tmp1) * 6.0 * tmpblue;
		else if(tmpblue < 0.5)
			blue = tmp2;
		else if(tmpblue < 2.0 / 3.0)
			blue = tmp1 + (tmp2 - tmp1) * ((2.0 / 3.0) - tmpblue) * 6.0;
		else
			blue = tmp1;
	}

	return float4(red, green, blue, HSL.w);
}


/* varyEach takes a computed color, then tweaks each indexed item
 * separately to add some variation.  Hue, saturation, and lightness
 * are all independently controlled.  Hue adds, but saturation and
 * lightness multiply.
 */
float4 varyEach (float4 Cin, float index, float varyhue, float varysat, float varylum)
{
    /* Convert to "hsl" space, it's more convenient */
    float4 Chsl = rgbToHsl(Cin);
    float h = Chsl.x;
    float s = Chsl.y;
    float l = Chsl.z;
    
    /* Modify Chsl by adding Cvary scaled by our separate h,s,l controls */
    h += varyhue * (noise(index+3)-0.5).x;
    s *= 1 - varysat * (noise(index-14)-0.5).x;
    l *= 1 - varylum * (noise(index+37)-0.5).x;
    Chsl = float4(fmod(h,1.0f), clamp(s,0,1), clamp(l,0,1), Cin.w);
    /* Clamp hsl and transform back to rgb space */
	Chsl = clamp(Chsl,float4(0.0f, 0.0f, 0.0f, 0.0f), float4(1.0f, 1.0f, 1.0f, 1.0f));
	return hslToRgb(Chsl);
}


#endif // _COLORS_FXH_
