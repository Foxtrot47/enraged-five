#ifndef RAGE_MEGATEXTURE_FXH


#include "rage_hdr.fxh"


float megaTexture_LODSCALE : megaTexture_LODSCALE  = 1.0f;

float MegaTileScale
<
   string UIName = "Mega Tile Scale";
   string UIWidget = "Numeric";
   float UIMin =0.0;
   float UIMax = 1.00;
	float UIStep = 0.001;
> = 0.1;


float MegaTexSpecularIntensityMax
<
   string UIName = "Mega Spec Inten Max";
   string UIWidget = "Numeric";
   float UIMin =0.0;
   float UIMax = 1.00;
	float UIStep = 0.001;
> = 0.6f;

float MegaTexSpecularIntensityMin
<
   string UIName = "Mega Spec Inten Min";
   string UIWidget = "Numeric";
   float UIMin =0.0;
   float UIMax = 1.00;
	float UIStep = 0.001;
> = 0.1f;

float MegaTexSpecularExponentMin
<
   string UIName = "Mega Spec Exp Min";
   string UIWidget = "Numeric";
   float UIMin =0.0;
   float UIMax = 100.00;
	float UIStep = 0.001;
> = 1.0f;

float MegaTexSpecularExponentMax 
<
   string UIName = "Mega Spec Exp Max";
   string UIWidget = "Numeric";
   float UIMin =0.0;
   float UIMax = 128.00;
	float UIStep = 0.001;
> = 10.0f;

float MegaTexBumpMin
<
   string UIName = "Mega Bump Self Shadow Min";
   string UIWidget = "Numeric";
   float UIMin =-1.0;
   float UIMax = 1.00;
	float UIStep = 0.001;
> = 0.2f;

float MegaTexBumpMax 
<
   string UIName = "Mega Bump Self Shadow Max";
   string UIWidget = "Numeric";
   float UIMin =-1.0;
   float UIMax = 1.00;
	float UIStep = 0.001;
> = -0.2f;

float MegaBlendWidth
<
	string UIName = "Blend Width";
	string UIWidget = "Numeric";
	float UIMin = 0;
	float UIMax = 64.0f;
	float UIStep = 0.01;
> = 4.0;

float MegaCompactEdges
<
	string UIName = "Compact Edges";
	string UIWidget = "Numeric";
	float UIMin = 0;
	float UIMax = 512.0f;
	float UIStep = 0.01;
> = 128.0;

BeginSampler(sampler2D, ChannelTexture, ChannelSampler,ChannelTex)
    string UIName = "Channel Texture";
    string TextureOutputFormats = "PC=MEGA_CHANNEL";
ContinueSampler(sampler2D,ChannelTexture, ChannelSampler, ChannelTex)
		AddressU = WRAP;
		AddressV = WRAP;
		AddressW = WRAP; 
		MIN_POINT_MAG_POINT_MIP_POINT;
		LOD_BIAS = 0;
EndSampler;


//#define USE_NORMAL_DISP_MAPS

BeginSamplerHDR(sampler2D,ColorAtlasTexture,ColorAtlasSampler,ColorAtlasTex)
    string UIName = "Color Texture Atlas";
#ifdef USE_NORMAL_DISP_MAPS
    string TextureOutputFormats = "PC=DXT1";
#endif
    
ContinueSampler(sampler2D,ColorAtlasTexture,ColorAtlasSampler,ColorAtlasTex)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler2D, NormalMapAtlasTexture, NormalMapAtlasSampler, NormalMapAtlasTex)
    string UIName = "Normal Map Atlas";
    //
 
#ifdef USE_NORMAL_DISP_MAPS
    string TextureOutputFormats = "PC=COMP_NRM_DISP_DXT5";
#else
	string TextureOutputFormats = "PC=COMP_NRM_DXT5";
#endif
    
ContinueSampler(sampler2D, NormalMapAtlasTexture,  NormalMapAtlasSampler, NormalMapAtlasTex)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;



float2 MegaTexGetTileData( float2 Tex, out float4 deltas)
{	
	float2 ChannelMapSize = float2( 2048.0f, 2048.0f);
	float2 ScaleUVs = float2( 1.0f / 4.0f, 1.0f / 4.0f );
	float2 ddUV = Tex * 48.0f;
	float2 uv = frac( Tex.xy * ChannelMapSize  * MegaTileScale.xx / megaTexture_LODSCALE.xx );  // was 0.1f
	
	float2 ddUV2 = Tex * MegaCompactEdges;
	float2 scale = max( abs(ddx( ddUV2)),  abs(ddy( ddUV2)) );
	scale.x = min(abs(max( scale.x, scale.y )) , 1.0) + 0.5/512.; 
	
	uv = uv * ( 1.0 - scale.xx ) + scale.xx * 0.5 ; 
		
	deltas = float4( ddx( ddUV), ddy( ddUV) );
	return uv * ScaleUVs;
}


float2 MegaTexGetTileDataScaled( float2 Tex, out float4 deltas)
{	
	float2 ChannelMapSize = float2( 2048.0f, 2048.0f);
	float2 ScaleUVs = float2( 1.0f / 4.0f, 1.0f / 4.0f );
	float2 uv = frac( Tex.xy * ChannelMapSize  * MegaTileScale.xx / megaTexture_LODSCALE.xx  );  // was 0.1f
	
	float2 ddUV2 = Tex * MegaCompactEdges;
	float2 scale = max( abs(ddx( ddUV2)),  abs(ddy( ddUV2)) );
	scale.x = min(abs(max( scale.x, scale.y )) , 1.0) + 0.5/512.; 
	
	uv = uv * ( 1.0 - scale.xx ) + scale.xx * 0.5 ;
		
	deltas = 0.0f;//float4( ddx( ddUV), ddy( ddUV) );
	return uv * ScaleUVs;
}

float4 GetMegaTexture( float2 cTex , float2 Tex, out float3 norm )
{
	float index  = tex2D( ChannelSampler, cTex ).x * 255.0f;
	
	float2 texPosition;
	texPosition.x = frac( index /4.0f );
	texPosition.y = float( (int)index/(int)4 ) * 0.25f;
	
	float4 deltas;
	float2 tileUV = MegaTexGetTileData(  Tex, deltas);
	
	
	float4 atlasUvs = float4( texPosition +  tileUV, 0.0f, 0.0f);
	float4 col = tex2D( ColorAtlasSampler, atlasUvs.xy, deltas.xy, deltas.zw );
	norm = tex2D( NormalMapAtlasSampler, atlasUvs.xy, deltas.xy, deltas.zw  ).wyw * 2. - 1.;
	norm.z = 1.0f - dot( norm.xy, norm.xy );
	
	return col;	
}
float4 GetMegaTextureSample( float2 cTex , out float3 norm, float2 tileUV, float4 deltas )
{
	float index  = tex2D( ChannelSampler, cTex ).x * 255.0f + 0.5f/255.0f;
	
	float2 texPosition;
	texPosition.x = frac( index /4.0f );
	texPosition.y = float( floor( index/ 4.0f) ) * 0.25f;
	
	
	float4 atlasUvs = float4( texPosition +  tileUV, 0.0f, 0.0f);
	float4 col = tex2DHDR2( ColorAtlasSampler, atlasUvs.xy, deltas.xy, deltas.zw , ColorAtlasTex );
	norm = tex2D( NormalMapAtlasSampler, atlasUvs.xy /*, deltas.xy, deltas.zw*/  ).wyw ;
	
	return col;	
}
void MegaTex(float2 InTex,  out float3 diffuseColor, out float3 normal )
{
#ifdef USE_NOISE_OFFSET
	float2 offset =  ( tex2D( NoiseSampler, InTex * NoiseScale * 64 ).xx * 2.0 - 1 ) * NoiseOffset;
	float2 tex = InTex + offset * 1.0f/2048;
#endif
	diffuseColor = GetMegaTexture( InTex , InTex, normal );
}
void MegaTex2(float2 InTex1, float2 InTex2,   out float4 diffuseColor, out float3 normal )
{
	diffuseColor = GetMegaTexture( InTex2 , InTex1, normal );
}

#ifdef USE_PARALLAX
float2 MegaTexGetParallaxTileData( float2 Tex, out  float4 deltas )
{
	float2 ChannelMapSize = float2( 2048.0f, 2048.0f);
	float2 ScaleUVs = float2( 1.0f / 4.0f, 1.0f / 4.0f );
	float2 ddUV = Tex * 32.0f;//18.0f;//32.0f;
	float2 uv = frac( Tex.xy * ChannelMapSize  * MegaTileScale / megaTexture_LODSCALE.xx );  // was 0.1f
	
	float2 ddUV2 = Tex * 24.0f;
	float2 scale = max( abs(ddx( ddUV2)),  abs(ddy( ddUV2)) );
	scale.x = min(abs(max( scale.x, scale.y )) , 1.0) + 0.5/512.; 
	
	uv = uv * ( 1.0 - scale.xx ) + scale.xx * 0.5 ;
		
	deltas = float4( ddx( ddUV), ddy( ddUV) );
	return uv * ScaleUVs;
}
float MegaTexGetParrallexHeight( float2 InTex  )
{
	float tiles = tex2D( ChannelSampler, InTex ).x;
	float hOffset = 0.1f + 1.5f * tiles * 16.0f;
	tiles  = tiles* 255.0f+ 0.5f/255.0f;
		
	float2 texPosition;
	texPosition.x = frac( tiles /4.0f );
	texPosition.y = float( floor( tiles/ 4.0f) ) * 0.25f;
	
	float4 deltas;
	float2 tileUV = MegaTexGetParallaxTileData(  InTex, deltas);
	
	
	float2 atlasUvs = float2( texPosition +  tileUV );
	float4 col = tex2D( ColorAtlasSampler, atlasUvs.xy, deltas.xy, deltas.zw ).xyzw;
	return col.w * hOffset;
}

#endif

#if __XENON


float4 ChannelMapBilinearPS( float2 InTex, out float4 weights  )
{
    float4 Weights =0.25f;
    float4 SampledChannel;
  
    asm {
         tfetch2D SampledChannel.x___, InTex.xy, ChannelSampler,
                OffsetX = -0.5, OffsetY = -0.5
              //  OffsetX =0 , OffsetY = 0

        tfetch2D SampledChannel._x__, InTex.xy, ChannelSampler,
                OffsetX =  0.5, OffsetY = -0.5
                // OffsetX =0.5 , OffsetY = 0
               
        tfetch2D SampledChannel.__x_, InTex.xy, ChannelSampler,
                 OffsetX = -0.5, OffsetY =  0.5
                 //  OffsetX =0.0 , OffsetY = 0.5
               
        tfetch2D SampledChannel.___x, InTex.xy, ChannelSampler,
				 OffsetX =  0.5, OffsetY =  0.5
		       
        getWeights2D Weights, InTex.xy, ChannelSampler,
                     MagFilter=linear, MinFilter=linear,
                     UseComputedLOD=true
    };
    Weights = float4( (1-Weights.x)*(1-Weights.y), 
                       Weights.x*(1-Weights.y), 
                      (1-Weights.x)*Weights.y, 
                       Weights.x*Weights.y );
	weights  = Weights;
	return SampledChannel;
}
float4 GetColor( float2 InTex, float4 p1, float4 p2, float4 weights, out float4 norm, float4 deltas )
{
	

	float4 col1;
	float4 col2;
	float4 col3;
	float4 col4;
	
	float4 norm1;
	float4 norm2;
	float4 norm3;
	float4 norm4;
	
	float2 t1 = p1.xy;
	float2 t2 = p1.zw;
	float2 t3 = p2.xy;
	float2 t4 = p2.zw;
	
	InTex.xy *= 40.0f / megaTexture_LODSCALE.xx ;
	
	norm = float4( 0.0f, 0.0f, 1.0f, 1.0f );
	
	float LOD;
     asm {
        getCompTexLOD2D LOD.x, InTex.xy, ColorAtlasSampler
									//	,AnisoFilter=max2to1

        setTexLOD LOD.x

        tfetch2D col1, t1.xy, ColorAtlasSampler,
				  UseComputedLOD=false, UseRegisterLOD=true

        tfetch2D col2, t2.xy, ColorAtlasSampler,
				 UseComputedLOD=false, UseRegisterLOD=true

        tfetch2D col3, t3.xy, ColorAtlasSampler,
				 UseComputedLOD=false, UseRegisterLOD=true

        tfetch2D col4, t4.xy , ColorAtlasSampler,
				UseComputedLOD=false, UseRegisterLOD=true
				
	
     }; 
     //if ( LOD.x < 6.5f)
	{    
	t1.xy = p1.xy + float2( 1.0f/2048.0f, 0.0f);
	t2.xy = p1.zw + float2( 1.0f/2048.0f, 0.0f);
	t3.xy = p2.xy + float2( 1.0f/2048.0f, 0.0f);
	t4.xy = p2.zw + float2( 1.0f/2048.0f, 0.0f);
	
	
   asm {
	    setTexLOD LOD.x

		tfetch2D norm1.w___, t1.xy, ColorAtlasSampler,  OffsetX = 0.0, OffsetY = 0.0, UseComputedLOD=false, UseRegisterLOD=true
        tfetch2D norm2.w___, t2.xy, ColorAtlasSampler,  OffsetX = 0.0, OffsetY = 0.0, UseComputedLOD=false, UseRegisterLOD=true
        tfetch2D norm3.w___, t3.xy, ColorAtlasSampler,  OffsetX = 0.0, OffsetY = 0.0, UseComputedLOD=false, UseRegisterLOD=true
        tfetch2D norm4.w___, t4.xy , ColorAtlasSampler,	OffsetX = 0.0, OffsetY = 0.0, UseComputedLOD=false, UseRegisterLOD=true				
	};
	

	t1.xy = p1.xy + float2( 0.0f, 1.0f/2048.0f);
	t2.xy = p1.zw + float2( 0.0f, 1.0f/2048.0f);
	t3.xy = p2.xy + float2( 0.0f, 1.0f/2048.0f);
	t4.xy = p2.zw + float2( 0.0f, 1.0f/2048.0f);
	
	asm {	
		  setTexLOD LOD.x
		tfetch2D norm1._w__, t1.xy, ColorAtlasSampler,	OffsetX = 0.0, OffsetY = 0.0,  UseComputedLOD=false, UseRegisterLOD=true
        tfetch2D norm2._w__, t2.xy, ColorAtlasSampler,	OffsetX = 0.0, OffsetY = 0.0,  UseComputedLOD=false, UseRegisterLOD=true
        tfetch2D norm3._w__, t3.xy, ColorAtlasSampler,	OffsetX = 0.0, OffsetY = 0.0,  UseComputedLOD=false, UseRegisterLOD=true
        tfetch2D norm4._w__, t4.xy , ColorAtlasSampler,	OffsetX = 0.0, OffsetY = 0.0,  UseComputedLOD=false, UseRegisterLOD=true
	}; 

		norm1.xy =  norm1.xy - col1.ww;
		norm2.xy =  norm2.xy - col2.ww;
		norm3.xy =  norm3.xy - col3.ww;
		norm4.xy =  norm4.xy - col4.ww;
	
		
		norm.xy = norm1.xy * weights.x + norm2.xy * weights.y + norm3.xy * weights.z + norm4.xy * weights.w;
		//norm.xy = norm.wy * 2. - 1.;
		norm.z = 0.2f;//1.0f - dot( norm.xy, norm.xy );
	
    }
    float4 col = col1 * weights.x + col2 * weights.y + col3 * weights.z + col4 * weights.w;
    // rescale col
    col = decompressHDR( col, ColorAtlasTex );
	return col;
}

float4 GetColorNormal( float2 InTex, float4 p1, float4 p2, float4 weights, out float4 norm, float4 deltas )
{
	float4 col1;
	float4 col2;
	float4 col3;
	float4 col4;
	
	float4 norm1;
	float4 norm2;
	float4 norm3;
	float4 norm4;
	
	float2 t1 = p1.xy;
	float2 t2 = p1.zw;
	float2 t3 = p2.xy;
	float2 t4 = p2.zw;
	
	InTex.xy *= MegaBlendWidth * 2.0f  / megaTexture_LODSCALE.xx;//84.0f;
	
	norm = float4( 0.0f, 0.0f, 1.0f, 1.0f );
	
	float LOD;
     asm {
        getCompTexLOD2D LOD.x, InTex.xy, ColorAtlasSampler
					,AnisoFilter=max4to1

        setTexLOD LOD.x

        tfetch2D col1, t1.xy, ColorAtlasSampler,
				  UseComputedLOD=false, UseRegisterLOD=true

        tfetch2D col2, t2.xy, ColorAtlasSampler,
				 UseComputedLOD=false, UseRegisterLOD=true

        tfetch2D col3, t3.xy, ColorAtlasSampler,
				 UseComputedLOD=false, UseRegisterLOD=true

        tfetch2D col4, t4.xy , ColorAtlasSampler,
				UseComputedLOD=false, UseRegisterLOD=true
				
	
     }; 
#ifdef USE_NORMAL_DISP_MAPS
	float h;
#endif
     //if ( LOD.x < 6.5f)
	{          
   asm {
	    setTexLOD LOD.x

				
   			tfetch2D norm1, t1.xy, NormalMapAtlasSampler,
					 UseComputedLOD=false, UseRegisterLOD=true
	                 
			tfetch2D norm2, t2.xy, NormalMapAtlasSampler,
					 UseComputedLOD=false, UseRegisterLOD=true

			tfetch2D norm3, t3.xy, NormalMapAtlasSampler,
					 UseComputedLOD=false, UseRegisterLOD=true

			tfetch2D norm4, t4.xy , NormalMapAtlasSampler,
					UseComputedLOD=false, UseRegisterLOD=true
	}; 

		
		norm = norm1 * weights.x + norm2 * weights.y + norm3 * weights.z + norm4 * weights.w;
	
#ifdef USE_NORMAL_DISP_MAPS
		h = norm.x;
#endif
		norm.xy = norm.wy * 2. - 1.;
		norm.z = 1.0f - dot( norm.xy, norm.xy );
	
    }
    float4 col = col1 * weights.x + col2 * weights.y + col3 * weights.z + col4 * weights.w;
     col = decompressHDR( col, ColorAtlasTex );
#ifdef USE_NORMAL_DISP_MAPS
    col.w = h;
  #endif
	return col;
}
float MegaTexPCFXenon(float2 cTex, float2 InTex, out float4 diffuseColor, out float3 normal, out float inten )
{

	float4 weights = float4( 1.0f, 0.0f, 0.0f, 0.0f );
	diffuseColor = float4( 0.0f, 1.0f, 0.0f, 0.5f );
	normal = float4( 0.0f, 1.0f, 0.0f, 1.0f );
	float4 tiles =  ChannelMapBilinearPS( cTex, weights );
	{
		tiles  = tiles* 255.0f+ 0.5f/255.0f;
		
		float4 posX;
		float4 posY;
		posX = frac( tiles /4.0f );
		posY = float4( floor( tiles/ 4.0f) ) * 0.25f;
		
		float4 deltas;
		float2 tileUV = MegaTexGetTileDataScaled(  InTex, deltas);
		
		float4 p1;
		float4 p2;
		p1.xz = tileUV.xx + posX.xy;
		p1.yw = tileUV.yy + posY.xy;
		p2.xz = tileUV.xx + posX.zw;
		p2.yw = tileUV.yy + posY.zw;

		//diffuseColor = GetColor( InTex, p1, p2, weights , normal, deltas);
		diffuseColor = GetColorNormal( InTex, p1, p2, weights , normal, deltas);
	}	
	float minSpec = MegaTexSpecularIntensityMin;
	inten = dot( tiles * 16.0f/ 255.0f, weights );
	
	float diffSpec = ( MegaTexSpecularIntensityMax - MegaTexSpecularIntensityMin );
	
	float spec = minSpec + diffSpec * inten;
	return  spec;
}	

#endif

float MegaTexPCF( float2 InTex, out float4 diffuseColor, out float3 normal, out float inten  )
{
#if __XENON
	float spec= MegaTexPCFXenon( InTex, InTex, diffuseColor, normal , inten);
	//diffuseColor = HDR( diffuseColor );
	return spec;
	
#endif
	float2 newUVs;

	float2 noiseUVs =InTex * 128.0f;// InTex * 4096.0f * 6.0;
	float2 offset = 0.0;//( tex2D( NoiseSampler, noiseUVs ).xx * 2.0 - 1  ) * NoiseOffset;
	
	inten = 0.5f;
	
	offset *= 1.0f/2048;
	newUVs = InTex + offset;
	
	float2 blend;
	
	
	float2 scale = max( abs(ddx( InTex * 8.0)),  abs(ddy( InTex * 8.0)) );
		
	float b = lerp( 2.0, 1.0f,  saturate(max( scale.x, scale.y ) ));
	
	float3 delta = float3( 1.0f/(2048 * b), 1.0f/(2048 * b), 0.0f );
	
	
	//also add have a texel
	blend = frac( newUVs / delta.xy )  ;
	newUVs = newUVs - blend * delta.xy;
	
	float4 diffTemp1;
	float4 diffTemp2;
	float3 normTemp1;
	float3 normTemp2;
	
	float4 deltas;
	float2 tileUV = MegaTexGetTileData(  InTex, deltas);
	
	
	diffuseColor = GetMegaTextureSample(  newUVs, normal , tileUV, deltas );
	diffTemp1 = GetMegaTextureSample(  newUVs + delta.xz, normTemp1, tileUV , deltas );
	diffuseColor = lerp( diffuseColor, diffTemp1, blend.x );
	normal = lerp( normal, normTemp1, blend.x );
	
	diffTemp1 = GetMegaTextureSample(  newUVs + delta.zy, normTemp1, tileUV , deltas );
	diffTemp2 = GetMegaTextureSample(  newUVs + delta.xy,  normTemp2, tileUV , deltas );
	diffTemp1 = lerp( diffTemp1, diffTemp2, blend.x );
	normTemp1 = lerp( normTemp1, normTemp2, blend.x );
	
	diffuseColor = lerp( diffuseColor, diffTemp1, blend.y );
	normal = lerp( normal, normTemp1, blend.y );
	
	// build normal once
	normal.xy = normal.xy * 2. - 1.;
	normal.z = 1.0f - dot( normal.xy, normal.xy );
	
	return 0.5f;
//	diffuseColor.w = saturate( diffuseColor.w * 2.0 - 1.0f);
}

float MegaTexGetExponent( float specExp )
{
	return MegaTexSpecularExponentMin + ( MegaTexSpecularExponentMax -MegaTexSpecularExponentMin )  * specExp;
}
float MegaTexGetBumpSelfShadow( float specExp )
{
	return MegaTexBumpMin + ( MegaTexBumpMax -MegaTexBumpMin )  * specExp;
}



#endif
