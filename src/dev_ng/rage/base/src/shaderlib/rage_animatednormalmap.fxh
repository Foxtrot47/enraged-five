#ifndef RAGE_ANIMATEDNORMALMAP_FXH
#define RAGE_ANIMATEDNORMALMAP_FXH

#include "rage_common.fxh"

#ifndef USE_ANIMATED_NORMAL_MAP
#define USE_ANIMATED_NORMAL_MAP (0)
#elif USE_ANIMATED_NORMAL_MAP
#define USE_ANIMATED_NORMAL_MAP_VARS (1)
#undef USE_ANIMATED_NORMAL_MAP
#define USE_ANIMATED_NORMAL_MAP (1)
#endif // USE_ANIMATED_NORMAL_MAP

#ifndef USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS
#define USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS (0)
#endif // USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS

#define BLEND_WEIGHTED	(0)
#define BLEND_LERP		(1)
#define BLEND_NLERP		(2)
#define BLEND_SLERP		(3)

#ifndef ANIMATED_NORMAL_MAP_BLEND_TYPE
#define ANIMATED_NORMAL_MAP_BLEND_TYPE (BLEND_WEIGHTED)
#endif // USE_SLERP

#if USE_ANIMATED_NORMAL_MAP_VARS
#define ANIMATED_NORMAL_MAP_PARAM4_COUNT		((ANIMATED_NORMAL_MAP_PARAM_COUNT) / 4)
#define ANIMATED_NORMAL_MAP_PARAM4_COUNT2		((ANIMATED_NORMAL_MAP_PARAM4_COUNT) / 2)
#define ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT	((ANIMATED_NORMAL_MAP_PARAM4_COUNT2) / (ANIMATED_NORMAL_MAP_WRINKLE_MAP_COUNT))
float4 gAnimatedNormalMapParams[ANIMATED_NORMAL_MAP_PARAM4_COUNT];

#if ANIMATED_NORMAL_MAP_WRINKLE_MAP_COUNT > 2
#error Need to add more normal map samplers
#endif // ANIMATED_NORMAL_MAP_WRINKLE_MAP_COUNT > 2

BeginSampler(sampler2D,normalMapTexture1,NormalMapSampler1,normalMapTexture1)
	string UIName = "Wrinkle Map Texture 0";
#if USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS
	string TextureOutputFormats = "PC=COMP_NRM_PD_DXT5";
#else
	string TextureOutputFormats = "PC=COMP_NRM_DXT5";
#endif // USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS
ContinueSampler(sampler2D,normalMapTexture1,NormalMapSampler1,normalMapTexture1)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
BeginSampler(sampler2D,normalMapTexture2,NormalMapSampler2,normalMapTexture2)
	string UIName = "Wrinkle Map Texture 1";
#if USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS
	string TextureOutputFormats = "PC=COMP_NRM_PD_DXT5";
#else
	string TextureOutputFormats = "PC=COMP_NRM_DXT5";
#endif // USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS
ContinueSampler(sampler2D,normalMapTexture2,NormalMapSampler2,normalMapTexture2)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
BeginSampler(sampler2D,animatedNormalMapMask0Texture,AnimatedNormalMapMask0Sampler,animatedNormalMapMask0Texture)
	string	UIName = "Animated Normal Map Mask Texture 0";
	string TextureOutputFormats = "PC=A4R4G4B4";
ContinueSampler(sampler2D,animatedNormalMapMask0Texture,AnimatedNormalMapMask0Sampler,animatedNormalMapMask0Texture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
BeginSampler(sampler2D,animatedNormalMapMask1Texture,AnimatedNormalMapMask1Sampler,animatedNormalMapMask1Texture)
	string	UIName = "Animated Normal Map Mask Texture 1";
	string TextureOutputFormats = "PC=A4R4G4B4";
ContinueSampler(sampler2D,animatedNormalMapMask1Texture,AnimatedNormalMapMask1Sampler,animatedNormalMapMask1Texture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
BeginSampler(sampler2D,animatedNormalMapMask2Texture,AnimatedNormalMapMask2Sampler,animatedNormalMapMask2Texture)
	string	UIName = "Animated Normal Map Mask Texture 2";
	string TextureOutputFormats = "PC=A4R4G4B4";
ContinueSampler(sampler2D,animatedNormalMapMask2Texture,AnimatedNormalMapMask2Sampler,animatedNormalMapMask2Texture)
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

float animatedNormalMapMod1
<
	string UIName = "animated normal map intensity 1";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;
float animatedNormalMapMod2
<
	string UIName = "animated normal map intensity 2";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 10.0;
	float UIStep = 0.1;
> = 1.0f;
/*
float animatedNormalMapWeightBias
<
	string UIName = "Weighting of blend region between two face sides";
	int UIHidden = 1;
	float UIMin = 0.0;
	float UIMax = 100.0;
	float UIStep = 1.0;
> = 1.0f;
*/
#endif // USE_ANIMATED_NORMAL_MAP_VARS

#if USE_ANIMATED_NORMAL_MAP
float3 rageAnimateNormal(float3 baseNormal, float2 texCoords, float3 tangent, float3 binormal, float3 normal)
{
	float2 wrinkleMapXy[] =
	{
		tex2D(NormalMapSampler1, texCoords).ga,
		tex2D(NormalMapSampler2, texCoords).ga
	};
	float wrinkleMapMods[] = { animatedNormalMapMod1, animatedNormalMapMod2 };

	float4 params[ANIMATED_NORMAL_MAP_PARAM4_COUNT2];

	float lerpFactor = texCoords.x;
/*
	// Might be nice to try and weight the blending of the two maps so that the outter
	// edges are not blended as much as the center. It seems unnecessary for the moment
	// as there is no visual difference.
	lerpFactor = 1.0f - lerpFactor; // Invert because we want to weight the outter more
	lerpFactor = lerpFactor * 2.0f - 1.0f; // Get between -1 to 1
	lerpFactor = ragePow(lerpFactor, animatedNormalMapWeightBias);
	lerpFactor = lerpFactor * 0.5f + 0.5f;
	lerpFactor = 1.0f - lerpFactor;
*/
	{
		for (int i = 0; i < ANIMATED_NORMAL_MAP_PARAM4_COUNT2; ++i)
		{
			params[i] = lerp(gAnimatedNormalMapParams[ANIMATED_NORMAL_MAP_PARAM4_COUNT2 + i], gAnimatedNormalMapParams[i], lerpFactor);
		}
	}
	
	float4 masks[ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT];
	masks[0] = tex2D(AnimatedNormalMapMask0Sampler, texCoords);
#if ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT > 1
	masks[1] = tex2D(AnimatedNormalMapMask1Sampler, texCoords);
#if ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT == 3
	masks[2] = tex2D(AnimatedNormalMapMask2Sampler, texCoords);
#elif ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT > 3
#error Need to add more mask samplers
#endif
#endif // ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT > 1

	// Figure out how many of the masks have a value greater than zero
	float maskCount = 0.0f;
	{
		for (int i = 0; i < ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT; ++i)
		{
			maskCount += dot(masks[i] > 0.0f.xxxx, 1.0f.xxxx);
		}
	}
	
	// Avoid divide by zero
#if __WIN32PC
	float3 _baseNormal = baseNormal;
#else
	if (maskCount <= 0.0f)
	{
		return baseNormal;
	}
	else
#endif // __WIN32PC
	{
		for (int i = 0; i < ANIMATED_NORMAL_MAP_WRINKLE_MAP_COUNT; ++i)
		{		
			// Calculate the weighted blend amount
			float blendAmt = 0.0f;
			for (int j = 0; j < ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT; ++j)
			{
				blendAmt += dot(masks[j], params[i * ANIMATED_NORMAL_MAP_PACKED_MASK_COUNT + j]);
			}
			blendAmt /= maskCount;
			
			// Construct the wrinkle map normal
#if USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS
			float3 wrinkleNormal = rageReconstructPartialDerivativeNormal(wrinkleMapXy[i], tangent, binormal, normal, wrinkleMapMods[i]);
#else
			float3 wrinkleNormal = rageReconstructNormal(wrinkleMapXy[i], tangent, binormal, normal, wrinkleMapMods[i]);
#endif // USE_PARTIAL_DERIVATIVE_WRINKLE_MAPS

#if ANIMATED_NORMAL_MAP_BLEND_TYPE == BLEND_WEIGHTED
			baseNormal += wrinkleNormal * blendAmt;
#elif ANIMATED_NORMAL_MAP_BLEND_TYPE == BLEND_LERP
			baseNormal = lerp(baseNormal, wrinkleNormal, blendAmt);
#elif ANIMATED_NORMAL_MAP_BLEND_TYPE == BLEND_NLERP
			baseNormal = rageNlerp(baseNormal, wrinkleNormal, blendAmt);
#elif ANIMATED_NORMAL_MAP_BLEND_TYPE == BLEND_SLERP
			baseNormal = rageSlerp(baseNormal, wrinkleNormal, blendAmt);
#endif
		}

#if __WIN32PC
		return lerp(normalize(baseNormal), _baseNormal, maskCount <= 0.0f);
#else
		return normalize(baseNormal);
#endif // __WIN32PC
	}
}
#endif // USE_ANIMATED_NORMAL_MAP

#endif // RAGE_ANIMATEDNORMALMAP_FXH
