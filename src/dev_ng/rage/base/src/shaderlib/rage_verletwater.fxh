//**************************************************************//
//  RAGE Water shader
//
//  David Serafim 12 Oct 2005
//**************************************************************//

#ifndef RAGE_VERLET_WATER_FXH
#define RAGE_VERLET_WATER_FXH

#include "rage_samplers.fxh"
//	#include "rage_common.fxh"
#include "rage_common.fxh"

#include "rage_drawbucket.fxh"


#define FLOATDEPTHBUFFER 1

#include "rage_diffuse_sampler.fxh"


BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
    string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	#if 1
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;


BeginSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
    string UIName = "Refraction Texture";
ContinueSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
	#if 1
		AddressU  = CLAMP;        
		AddressV  = CLAMP;
		AddressW  = CLAMP;
		MIPFILTER = NONE;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

#ifdef USE_SIMULATION


BeginSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
    string UIName = "Environment Texture";
ContinueSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
	#if !__FXL
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(sampler2D,verletsimTexture,VerletSimSampler,VerletSimTex)
    string UIName = "Verlet Simulation Texture";
ContinueSampler(sampler2D,verletsimTexture,VerletSimSampler,VerletSimTex)
EndSampler;

#endif

 
float HeightScale : HeightScale
<
	string UIName = "HeightScale";
	float UIMin = -200.0;
	float UIMax = 200.0;
	float UIStep = 0.5;
> = 0.5;

float HeightMin : HeightMin
<
	string UIName = "HeightMin";
	float UIMin = -200.0;
	float UIMax = 200.0;
	float UIStep = 0.5;
> = 0.5;

float ReflectiveIntensity : ReflectiveIntensity
<
	string UIName = "Reflection Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.5;


#ifdef USE_SIMULATION
float RefractionIndex : RefractionIndex
<
	string UIName = "Refraction Index";
	string UIWidget = "Numeric";
	float UIMin = -0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 1.00;

#endif

float RefractionIntensity : RefractionIntensity
<
	string UIName = "Refraction Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.5;

float DiffuseIntensity : DiffuseIntensity
<
	string UIName = "Diffuse Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.15;

float SpecularIntensity : SpecularIntensity
<
	string UIName = "Specular Intensity";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.05;
> = 1.0; // was 0.5

float SpecExponent : SpecularExp
<
	string UIName = "Specular Exponent";
	float UIMin = 4.0;
	float UIMax = 10000.0;
	float UIStep = 5.0;
> = 250.0;

float ReflectionDistortion : ReflectionDistortion
<
	string UIName = "Reflection Distortion";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.1;


float FresnelIntensity : FresnelIntensity
<
	string UIName = "Fresnel Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.50;

/*
const float3 WaterColor
<
	string UIName = "Water Color";
	string UIWidget = "Color";
#if !__PS3
	float4 UIMin = {0.0,0.0,0.0,1.0};
	float4 UIMax = {1.0,1.0,1.0,1.0};
#endif
> = { 0.2, 0.4, 0.64 };
*/

float RippleAmplitude : RippleAmplitude
<
	string UIName = "Ripple Amplitude";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 10.00;
	float UIStep = 0.5;
> = 2.5;//2.00;

float RippleScale : RippleScale
<
	string UIName = "Ripple Scale";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 100.00;
	float UIStep = 0.5;
> = 3.00;


const float RipplePhase : RipplePhase;


const float R0 = 0.02037f; // going by shaderX 2 advanced water effects

struct WATER_VS_OUTPUT
{
	DECLARE_POSITION(ScrPos)
	float3	WorldPos;
	float3	View;
	float	displace;
};



const float CurrentTime : Scene_CurrentTime;

#ifdef USE_GERSTNER

const float2 WaveVector1
<
	string UIName = "Wave Vector 1";
	string UIWidget = "Numeric";
	float UIMin = -10.00;
	float UIMax = 10.00;
	float UIStep = 0.05;
> ={ 1, 1 };

const float2 WaveVector2
<
	string UIName = "Wave Vector 2";
	string UIWidget = "Numeric";
	float UIMin = -10.00;
	float UIMax = 10.00;
	float UIStep = 0.05;
> ={ -2, 1.5 };

const float2 WaveVector3
<
	string UIName = "Wave Vector3";
	string UIWidget = "Numeric";
	float UIMin = -10.00;
	float UIMax = 10.00;
	float UIStep = 0.05;
> ={ 2, 2 };

const float3 WaveFrequency
<
	string UIName = "Wave Vector3";
	string UIWidget = "Numeric";
	float UIMin = -10.00;
	float UIMax = 10.00;
	float UIStep = 0.05;
> ={ 1, 1, .1  };

const float3 WaveLength
<
	string UIName = "Wave Length";
	string UIWidget = "Numeric";
	float UIMin = -10.00;
	float UIMax = 30.00;
	float UIStep = 0.05;
> ={ 4, 15, 3 };

const float3 Amplitude
<
	string UIName = "Amplitude";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 30.00;
	float UIStep = 0.05;
> ={ .25, .1, .4 };


float3 GerstnerWaves(float3 pos, out float displace ) 
{ 
  float3 SinDataO;
  float3 CosDataO;
  float3 SinDataI;

  float2 tpos = pos.xz * 0.05f;
  SinDataI.x = dot( WaveVector1,tpos.xy);
  SinDataI.y = dot( WaveVector2,tpos.xy);
  SinDataI.z = dot( WaveVector3,tpos.xy);
  
  SinDataI= SinDataI-( WaveFrequency * CurrentTime );
  
  sincos(SinDataI,SinDataO, CosDataO);
  SinDataO=SinDataO* Amplitude;
 
  float3 k=2*3.14/WaveLength;
  float2 newWaveVector1 = ( WaveVector1/k.xx )  * SinDataO.xx;
  float2 newWaveVector2 = ( WaveVector2/k.yy )  * SinDataO.yy;
  float2 newWaveVector3 = ( WaveVector3/k.zz )  * SinDataO.zz;
  
  float3 xvec;
  xvec.xy= pos.xz - (newWaveVector1 + newWaveVector2 + newWaveVector3);
  
  displace= dot( CosDataO, Amplitude );;
  xvec.z= pos.y + displace * RippleAmplitude * 1.0f;
  
   return xvec.xzy;
} // main 

#endif

#ifdef USE_SIMULATION
float3	waterApplyDisplacementVS( float3 pos, float2 inTxr, out float displace )
{
	float4 displ = tex2Dlod(VerletSimSampler,float4(inTxr,0,0));
	displace = saturate( displ.r );
	pos.y += displace * RippleAmplitude;
	
	return pos;
}
#endif


WATER_VS_OUTPUT waterSetupVS( float3 pos, float2 inTxr, float displace )
{
	WATER_VS_OUTPUT		res;
	float3				wPos = (float3)mul( float4( pos, 1.0f), gWorld).xyz;
	
	res.View = normalize(gViewInverse[3].xyz - wPos );
	res.ScrPos = mul( float4( pos, 1.0f), gWorldViewProj);
	res.WorldPos = (float3)  wPos ;
	res.displace = displace;
	return res;
}
/*
float3  planarReflection( float3 screenPosition )
{
	float2 refr = screenPosition.xy / screenPosition.w;
	refr = refr * 0.5 + 0.5;
	refr.y = 1.0f-refr.y;
	return tex2D(RefractionSampler,refr).xyz
}	
*/

float4  waterShaderSimpleDontuse( float3 view, float3 normal, float3 t, float3 bn, float displace, float2 Tex, float4 ScrPos ,float3 sunDir, float3 sunColor )
{
	float3 verletpert = displace * 0.1;
	view = normalize( view );
//	normal = normalize( normal );
	
	// move ripples in multiple directions
	float3 np = float3(Tex * RippleScale, Tex.y);
	float3 normal0 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.02,0.05) * RipplePhase+CurrentTime.x*0.1) * 2 - 1);// * rphase0;
	float3 normal1 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.1,-0.03) * RipplePhase+CurrentTime.x*0.1) * 2 - 1);// * rphase1;
	float3 normal2 = (float3) (tex2D(BumpSampler, (float2)np + float2(-0.1,0.05) * RipplePhase+CurrentTime.x*0.02) * 2 - 1);
	float3 normal3 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.03,0.07) * RipplePhase+CurrentTime.x*0.01) * 2 - 1);

	float2 refr = ScrPos.xy / ScrPos.w;
	
	refr = refr * 0.5 + 0.5;
	refr.y = 1-refr.y;
	
	
	float3 newNormal = normalize( normal0  + normal1  + normal2  + normal3 ).xzy;
    half3x3 m; // tangent to world matrix
    m[0] = bn;
    m[1] = t;
    m[2] = normal;
    newNormal = mul(m, newNormal.xyz);	
	
	refr.xy += -newNormal.xz * ReflectionDistortion * 0.1f; //0.8f * saturate(1.0/ScrPos.z);
	newNormal.y += RippleAmplitude;
	newNormal = normalize( newNormal );
	 
	// fresnel term
	float cosine = dot(view,newNormal);
	float fresnel = max(cosine, -cosine);
	fresnel = R0 + (1.0 - R0) * ragePow(1.0 - fresnel, 5);
	
	// calculate specular and diffuse for sunlight
	float3 sunDiffuse =  (ragePow( saturate( dot( newNormal, sunDir ) ) , 4)) * sunColor;
	float3 sunReflectPhong = ragePow( saturate( dot( reflect( -sunDir, newNormal ), view )), SpecExponent * 2.0f ) * sunColor * SpecularIntensity;
	
	float3 reflection =  tex2D(RefractionSampler,refr).xyz + sunReflectPhong;
	float3 diffuseColor =  sunDiffuse * 1.0f;
	float3 lumiance = float3( 0.3f, 0.5f, 0.2f );
	float alphaDepth = saturate( dot(lumiance, diffuseColor) * 1.0f + fresnel );		// transparencey
	diffuseColor *= alphaDepth;
	float3 reflectionAmt = lerp( diffuseColor, reflection, fresnel );
	return float4( reflectionAmt, alphaDepth );
}

float4  waterShader( float3 view, float3 WorldPos, float3 normal, float displace, float2 Tex, float4 ScrPos ,float3 sunDir, float3 sunColor )
{
	float3 verletpert = displace * 0.1;
	view = normalize( view );
	normal = normalize( normal );
	WorldPos = normalize(float3(0,0,1000) - WorldPos );
	
	// move ripples in multiple directions
	float3 np = float3(Tex * RippleScale, Tex.y);
	float3 normal0 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.02,0.05) * RipplePhase) * 2 - 1);// * rphase0;
	float3 normal1 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.1,-0.03) * RipplePhase) * 2 - 1);// * rphase1;
	float3 normal2 = (float3) (tex2D(BumpSampler, (float2)np + float2(-0.1,0.05) * RipplePhase) * 2 - 1);
	float3 normal3 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.03,0.07) * RipplePhase) * 2 - 1);
	float3 ldir = (float3) normalize( WorldPos );
//	float3 halfvec = normalize(ldir+view);
//	view.z=-view.z;
//	float3 halfvec2 = normalize(ldir+view);
	float3x3 TangentSpace;
	TangentSpace[0] = float3(1,0,0);
	TangentSpace[1] = cross(TangentSpace[0],normal);
	TangentSpace[2] = normal;
//	halfvec = mul(halfvec,TangentSpace);
//	halfvec2 = mul(halfvec2,TangentSpace);
//	ldir = mul(ldir,TangentSpace);
	
//	float3 diffuse = (dot(ldir,normal0) + dot(ldir,normal1)) / 2;
//	float3 specular = dot(halfvec,normal2) * dot(halfvec,normal3) * dot(halfvec,normal1);
//	specular *= (ragePow(dot(halfvec2,normal), SpecExponent) * SpecularIntensity) * gLightColor[2].rgb;
//	float3 bump = diffuse + specular;
//	bump *= 1-displace;

	float4 pos = ScrPos;
	float2 refr = pos.xy / pos.w;
	
	
//	float3 zcorrectedbump = bump * saturate(1.0/ScrPos.z) + 0.1;
//	refr = refr + (float2)zcorrectedbump * RefractionIndex;
	refr = refr * 0.5 + 0.5;
//	refr.y += 0.05;
//	refr.x -= 0.05;
	refr.y = 1-refr.y;
	
	// depth fog
	float alphaDepth = 0.2f;//WaterDepthFog( pos.z, tex2D( DepthSampler, refr ).x );
	//return alphaDepth;
	
	//return float4( tex2D(RefractionSampler,refr).xyz, 1.0f);
	
	
	float3 newNormal = normalize( normal0  + normal1 * 0.5f + normal2 * 0.24 + normal3 *0.1 ).xzy;
	refr.xy += -newNormal.xz * ReflectionDistortion * 0.1f; //0.8f * saturate(1.0/ScrPos.z);
	newNormal.y += RippleAmplitude;

	 newNormal = normalize( newNormal );
	
	 
	float cosine = dot(view,newNormal);
	// fresnel term
	float fresnel = max(cosine, -cosine);
	fresnel = R0 + (1.0 - R0) * ragePow(1.0 - fresnel, 5);
	
	// calculate specular and diffuse for sunlight
	float3 sunDiffuse = 0.5f * (saturate( dot( newNormal, sunDir ) ) + 1.0f) * sunColor;
	float3 sunReflectPhong = ragePow( saturate( dot( reflect( -sunDir, newNormal ), view )), SpecExponent ) * sunColor * SpecularIntensity;
	
	float3 reflection =  tex2D(RefractionSampler,refr).xyz + sunReflectPhong;
	float3 diffuseColor =  sunDiffuse * 0.9;
	
	alphaDepth = saturate( alphaDepth + fresnel );
	// add bump map offset
	float3 reflectionAmt = lerp( diffuseColor, reflection, fresnel );
	return float4( reflectionAmt, alphaDepth );

	refr += (float2) verletpert;

	
	//float2 difftcoord = (Tex*6+ RipplePhase*0.1) - (float2)bump;
	//float2 difftcoord2 = ( Tex*3 - RipplePhase*0.15) + (float2)bump;
	//float4 diff = tex2D( DiffuseSampler,difftcoord)*tex2D( DiffuseSampler,difftcoord2)*WaterColor*DiffuseIntensity + WaterColor;

//	float3 refl = -view + 2*cosine * normal - bump * ReflectionDistortion;
	//refl += verletpert;

	
	//fresnel = saturate((fresnel*4+verletpert.x)*FresnelIntensity);


	//float ratio = (ReflectiveIntensity / (ReflectiveIntensity+RefractionIntensity));
	//ratio = saturate(lerp(ratio,ratio*fresnel,FresnelIntensity));
	//float4 col = lerp(tex2D(RefractionSampler,refr)*RefractionIntensity, texCUBE(EnvironmentSampler,refl)*ReflectiveIntensity, ratio);
	//col = lerp(col,col*diff,DiffuseIntensity);
	//float4 diffcolor = diff*DiffuseIntensity * (float4(diffuse,1)*0.3+0.7);
	//col += diffcolor + ragePow(diff,8);
	//col += float4(specular,1);

	//col.a = WaterColor.a;

	//return col;
}
#endif

