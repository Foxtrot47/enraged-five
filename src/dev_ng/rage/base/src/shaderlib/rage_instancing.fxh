// INCLUDE GUARDS
#ifndef __RAGE_INSTANCING_FXH
#define __RAGE_INSTANCING_FXH

#if defined(USE_INSTANCED) && !__MAX
	#define INSTANCED (1)
	#include "instancebuffer.fxh"

	//This property tells the game that the current shader is instanced.
	property int __rage_instanced = 1;
#else
	#define INSTANCED (0)
#endif

#if defined(USE_BATCH_INSTANCING) && !__MAX
#	if INSTANCED
#		error Can't have dynamic instancing and batch instancing enabled at the same time.
#	endif
#	define BATCH_INSTANCING (1)
#	include "instancebuffer.fxh"
#else
#	define BATCH_INSTANCING (0)
#endif

#if INSTANCED
	#define INSTANCED_ONLY(x) x
	#define NOT_INSTANCED_ONLY(x)
#else //INSTANCED
	#define INSTANCED_ONLY(x)
	#define NOT_INSTANCED_ONLY(x) x
#endif //INSTANCED

#if BATCH_INSTANCING
#	define BATCH_INSTANCING_ONLY(x) x
#else //BATCH_INSTANCING
#	define BATCH_INSTANCING_ONLY(x)
#endif //BATCH_INSTANCING

#if INSTANCED || BATCH_INSTANCING
#	define INSTANCING_ONLY_ARG(x) x,
#else //INSTANCED || BATCH_INSTANCING
#	define INSTANCING_ONLY_ARG(x)
#endif //INSTANCED || BATCH_INSTANCING

//Macro to help declare an instanced version of the vertex shader input structure, as there are a few variations needed.
#if (INSTANCED || BATCH_INSTANCING) && __XENON
	#define DeclareInstancedStuct(InstStructName, BaseStructName)	\
		struct InstStructName {										\
			int nIndex : INDEX;										\
		};
#elif (INSTANCED || BATCH_INSTANCING) && (__SHADERMODEL >= 40)
	#define DeclareInstancedStuct(InstStructName, BaseStructName)	\
		struct InstStructName {										\
			BaseStructName baseVertIn;								\
			uint nInstIndex : SV_InstanceID;						\
		};
#else
	#define DeclareInstancedStuct(InstStructName, BaseStructName)	\
		struct InstStructName {										\
			BaseStructName baseVertIn;								\
		};
#endif

float4 ApplyWorldTransform(float4 pos, float4x4 worldMtx)
{
	return mul(pos, worldMtx);
}

float4 ApplyWorldTransform(float4 pos, float3x4 worldMtx)
{
	return float4(mul(worldMtx, pos), 1.0f);
}


float4 ApplyCompositeWorldTransform(float4 pos, INSTANCING_ONLY_ARG(float4x4 worldMtx) float4x4 compositeWorldMtx)
{
#if INSTANCED || BATCH_INSTANCING
	float4 worldPos = ApplyWorldTransform(pos, worldMtx);
	return mul(worldPos, compositeWorldMtx);
#else
	return mul(pos, compositeWorldMtx);
#endif
}

#if INSTANCED || BATCH_INSTANCING
//row_major world transform
float4 ApplyCompositeWorldTransform(float4 pos, INSTANCING_ONLY_ARG(float3x4 worldMtx) float4x4 compositeWorldMtx)
{
#if INSTANCED || BATCH_INSTANCING
	float3 worldPos = ApplyWorldTransform(pos, worldMtx).xyz;
	return mul(float4(worldPos, 1.0f), compositeWorldMtx);
#else
	return mul(pos, compositeWorldMtx);
#endif
}
#endif

#endif //__RAGE_INSTANCING_FXH
