#ifndef __RAGE_XPLATFORMTEXTUREFETCHMACROS_FXH
#define __RAGE_XPLATFORMTEXTUREFETCHMACROS_FXH

#ifndef RAGE_DEPENDENCY_MODE
float4 TexelSize = { 0, 0, 0, 0 };
#endif

// this macro makes texture fetches cross platform
// it works on the XBOX 360 assuming that the HALFPIXELOFFSET render state is set,
// so that the DX texture offset does not need to be considered
// the windows version would need the half pixel offset somewhere: in the blit function or by offsetting uvs
//
// although some game teams will want to set the half pixel offset by offsetting the rendering position ...
//
// Please note that we need to fill up the TexelSize variable
float4 _Tex2DOffset(sampler2D ss, float2 uv, float2 offset)
{
    float4 result;
    
#if __XENON
    float offsetX = offset.x;
    float offsetY = offset.y;
    asm {
        tfetch2D result, uv, ss, OffsetX=offsetX, OffsetY=offsetY
    };
#endif
    
#if __PS3
	result = tex2D( ss, uv + TexelSize.xy * offset );
#endif	
	
#if __WIN32PC || __PSSL || __MAX
	// please note that PC fetches from the edge of the first pixel to fetch from the center 
	// of the pixel we have to add 0.5 * TexelSize
	// you can do this here to the uv set or you can adjust blitting
	//result = tex2D( ss, uv + TexelSize.xy * offset + TexelSize.xy * 0.5);
	result = tex2D( ss, uv + TexelSize.xy * offset);
#endif

    return result;
}

half4 _H4Tex2DOffset(sampler2D ss, float2 uv, float2 offset)
{
    half4 result;
    
#if __XENON
    float offsetX = offset.x;
    float offsetY = offset.y;
    asm {
        tfetch2D result, uv, ss, OffsetX=offsetX, OffsetY=offsetY
    };
#endif
    
#if __PS3
	result = h4tex2D( ss, uv + TexelSize.xy * offset );
#endif	
	
#if __WIN32PC || __PSSL
	// please note that PC fetches from the edge of the first pixel to fetch from the center 
	// of the pixel we have to add 0.5 * TexelSize
	// you can do this here to the uv set or you can adjust blitting
	//result = tex2D( ss, uv + TexelSize.xy * offset + TexelSize.xy * 0.5);
	result = (half4)h4tex2D( ss, uv + TexelSize.xy * offset);
#endif

    return result;
}

#endif
