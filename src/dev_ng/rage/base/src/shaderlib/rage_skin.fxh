// rage commonly used functions and defines related to skin rendering

// Include guards
#ifndef __RAGE_SKIN_FXH
#define __RAGE_SKIN_FXH

#if !defined(NO_SKINNING)

// #if __XENON
// #include <xdk.h>
// #endif

#include "skinning_method_config.h"

//------------------------------------
// Will work on PS3 eventually as well.
// no it won't :-) ... NVIDIA does not have UBYTE4 support
#define USE_UBYTE4	(__PSP2 || __PSSL)

#if USE_UBYTE4
#define index4 int4
#else
#define index4 float4
#endif

//----------------------
// common rage structs:
//		** Put skinning related structs here that can be shared
struct rageSkinVertexInput {
	// This covers the default rage skinned vertex
    float3 pos            : POSITION0;
	float4 weight		  : BLENDWEIGHT;
	index4 blendindices	  : BLENDINDICES;
    float2 texCoord0      : TEXCOORD0;
    float2 texCoord1      : TEXCOORD1;
    float3 normal		  : NORMAL;
	float4 diffuse		  : COLOR0;
};

struct rageSkinVertexOffset {
    float3 pos            : POSITION1;
};

struct rageSkinVertexInputBump {
	// This covers the default rage skinned vertex
    float3 pos            : POSITION0;
	float4 weight		  : BLENDWEIGHT;
	index4 blendindices	  : BLENDINDICES;
    float2 texCoord0      : TEXCOORD0;
    float2 texCoord1      : TEXCOORD1;
    float3 normal		  : NORMAL0;
	float4 tangent		  : TANGENT0;
	float4 diffuse		  : COLOR0;
};

struct rageSkinVertexOffsetBump {
	// This covers the default rage skinned vertex
    float3 pos            : POSITION1;
    float3 normal		  : NORMAL1;
	float4 tangent		  : TANGENT1;
};


#if MTX_IN_VB && __XENON
rageSkinMtx FetchBoneMatrix( int BoneIndex )
{
	float4 xaxis, yaxis, zaxis;
	asm { 
		vfetch xaxis, BoneIndex, position5, UseTextureCache=true
		vfetch yaxis, BoneIndex, position6, UseTextureCache=true
		vfetch zaxis, BoneIndex, position7, UseTextureCache=true
	};
	return rageSkinMtx(xaxis,yaxis,zaxis);
}
#elif MTX_IN_TEX && __XENON
sampler1D bone_palette_texture : register(s3) = 
sampler_state {
	MipFilter = NONE; 
	MinFilter = POINT; 
	MagFilter = POINT; 
	AddressU = CLAMP; 
	AddressV = CLAMP; 
};

rageSkinMtx FetchBoneMatrix( int BoneIndex )
{
	float4 xaxis, yaxis, zaxis;
	float u = (BoneIndex * 3 + (MTX_IN_TEX_HALF? 10 : 5)) / 768.0;
    asm
    {
        tfetch1D xaxis, u, bone_palette_texture, UseComputedLOD = false, OffsetX = 0.5
        tfetch1D yaxis, u, bone_palette_texture, UseComputedLOD = false, OffsetX = 1.5
        tfetch1D zaxis, u, bone_palette_texture, UseComputedLOD = false, OffsetX = 2.5
    };
	return rageSkinMtx(xaxis,yaxis,zaxis);
}
#else
rageSkinMtx FetchBoneMatrix( int BoneIndex )
{
	return gBoneMtx[BoneIndex];
}
#endif

#if __XENON && ((MTX_IN_VB && !MTX_IN_VB_HALF) || (MTX_IN_TEX && !MTX_IN_TEX_HALF))
#define FETCH_PREDICATE(x) if (x)
#else
#define FETCH_PREDICATE(x)
#endif


//----------------------
// common rage functions:
//		** Put skinning related functions here that can be shared

// Compute a composite matrix using 4 weights for a given bone
rageSkinMtx ComputeSkinMtx(index4 indices, float4 weights)
{
#if __PS3
	rageSkinMtx skinMtx = gBoneMtx[indices.x] * weights.x;
	skinMtx += gBoneMtx[indices.y] * weights.y;
	skinMtx += gBoneMtx[indices.z] * weights.z;
	skinMtx += gBoneMtx[indices.w] * weights.w;
	return skinMtx;
#else
#if USE_UBYTE4
	int4 i = D3DCOLORtoUBYTE4(indices);
#else
	int4 i = D3DCOLORtoUBYTE4(indices);
#endif
	int bone0 = i.z, bone1 = i.y, bone2 = i.x, bone3 = i.w;
	rageSkinMtx skinMtx = FetchBoneMatrix(bone0) * weights.x;
	FETCH_PREDICATE(weights.y) skinMtx += FetchBoneMatrix(bone1) * weights.y;
	FETCH_PREDICATE(weights.z) skinMtx += FetchBoneMatrix(bone2) * weights.z;
	FETCH_PREDICATE(weights.w) skinMtx += FetchBoneMatrix(bone3) * weights.w;
	return skinMtx;
#endif
}

rageSkinMtx ComputeSkinMtx8(index4 indices,index4 indices2,float4 weights,float4 weights2)
{
#if __PS3
	rageSkinMtx skinMtx = gBoneMtx[indices.x] * weights.x;
	skinMtx += gBoneMtx[indices.y] * weights.y;
	skinMtx += gBoneMtx[indices.z] * weights.z;
	skinMtx += gBoneMtx[indices.w] * weights.w;
	
	skinMtx += gBoneMtx[indices2.x] * weights2.x;
	skinMtx += gBoneMtx[indices2.y] * weights2.y;
	skinMtx += gBoneMtx[indices2.z] * weights2.z;
	skinMtx += gBoneMtx[indices2.w] * weights2.w;	
	return skinMtx;
#else
#if USE_UBYTE4
	int4 i = D3DCOLORtoUBYTE4(indices);
	int4 i2 = D3DCOLORtoUBYTE4(indices2);
#else
	int4 i = D3DCOLORtoUBYTE4(indices);
	int4 i2 = D3DCOLORtoUBYTE4(indices2);
#endif
	int bone0 = i.z, bone1 = i.y, bone2 = i.x, bone3 = i.w;
	int bone4 = i2.z, bone5 = i2.y, bone6 = i2.x, bone7 = i2.w;
	rageSkinMtx skinMtx = FetchBoneMatrix(bone0) * weights.x;
	FETCH_PREDICATE(weights.y) skinMtx += FetchBoneMatrix(bone1) * weights.y;
	FETCH_PREDICATE(weights.z) skinMtx += FetchBoneMatrix(bone2) * weights.z;
	FETCH_PREDICATE(weights.w) skinMtx += FetchBoneMatrix(bone3) * weights.w;
	FETCH_PREDICATE(weights2.x) skinMtx += FetchBoneMatrix(bone4) * weights2.x;
	FETCH_PREDICATE(weights2.y) skinMtx += FetchBoneMatrix(bone5) * weights2.y;
	FETCH_PREDICATE(weights2.z) skinMtx += FetchBoneMatrix(bone6) * weights2.z;
	FETCH_PREDICATE(weights2.w) skinMtx += FetchBoneMatrix(bone7) * weights2.w;
	return skinMtx;
#endif
}

float3 rageSkinTransformRigid(float4 pos) { return mul(FetchBoneMatrix(pos.w),float4(pos.xyz,1)); }

#if __PS3

#define  rageSkinGetBone0(value) (value).x
#define  rageSkinGetBone1(value) (value).y
#define  rageSkinGetBone2(value) (value).z
#define  rageSkinGetBone3(value) (value).w

#elif __WIN32PC || __PSSL

#define  rageSkinGetBone0(value) (value).x
#define  rageSkinGetBone1(value) (value).w
#define  rageSkinGetBone2(value) (value).z
#define  rageSkinGetBone3(value) (value).y

#else

#define  rageSkinGetBone0(value) (value).z
#define  rageSkinGetBone1(value) (value).y
#define  rageSkinGetBone2(value) (value).x
#define  rageSkinGetBone3(value) (value).w

#endif

#endif	// !defined(NO_SKINNING)
#endif // __RAGE_SKIN_FXH
