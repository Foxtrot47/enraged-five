// PURPOSE : Helper functions for creating fast soft particles
//
#ifndef RAGE_SOFTPARTICLES_FXH
#define RAGE_SOFTPARTICLES_FXH

#if RAGE_USE_SOFT_CONSTANTS
CBSHARED BeginConstantBufferPagedDX10(rage_SoftParticleBuffer, b5)
SHARED float4 NearFarPlane : NearFarPlane REGISTER(c128);   
SHARED float4 gInvScreenSize : InvScreenSize REGISTER2( ps,c129); 
EndConstantBufferDX10(rage_SoftParticleBuffer)

BeginConstantBufferDX10( rage_SoftParticles_locals )
#if HACK_GTA4

float Softness : Softness
<
	string UIName	= "Softness"; 
	float UIMin = 0.0;
	float UIMax = 20.0;
	float UIStep = 0.01;
	string UIHint	= "Keyframeable";
	string nostrip	= "nostrip";
> = 2.0f; 
#define SOFTNESS										custom1.x

#else

float gSoftness : gSoftness
<
string UIName = "Softness Fade : Controls how much soft particles fade out as objects become close to them ;";
		float UIMin = 0.0;
		float UIMax = 1000.0;
		float UIStep = 0.01;
> = 2.0f;

#endif // HACK_GTA4
EndConstantBufferDX10( rage_SoftParticles_locals )

#endif

#if RAGE_USE_SOFT_SAMPLER

#ifndef RAGE_SOFT_PARTICLE_DEPTH_SHARED_SAMPLER_REGISTER
	#define RAGE_SOFT_PARTICLE_DEPTH_SHARED_SAMPLER_REGISTER s15 //maybe s14 would be a better default?
#endif

// Depth map
BeginSharedSampler(sampler,DepthMap,DepthMapTexSampler,DepthMap,RAGE_SOFT_PARTICLE_DEPTH_SHARED_SAMPLER_REGISTER)	// Depth map
ContinueSharedSampler(sampler,DepthMap,DepthMapTexSampler,DepthMap,RAGE_SOFT_PARTICLE_DEPTH_SHARED_SAMPLER_REGISTER) 
   MinFilter = POINT;
   MagFilter = POINT;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSharedSampler;
#endif

// ---------------------------------------------------
// Soft Particles API
// ---------------------------------------------------

// PURPOSE : returns an alpha value to fade out the particle as it reachs the near plane
// USAGE: Vertex shader is best.
float softpart_GetNearPlaneFade( float posz, float softness )
{
	float nearPlaneDiff = (posz - NearFarPlane[0]);
	return saturate( nearPlaneDiff * softness);
}

// PURPOSE : stores values in vertex shader to allow for calculation in pixel shader for soft particle
// NOTES : Could have extra values added to improve performance
// USAGE : Vertex shader
float softpart_SetupSoftParticlesVS(  float4 pos )
{
	return pos.z;
}

// PURPOSE : Gets the fade amount of a point by using a depht comparision 
// NOTES : could be used for cheap depth fog where the value represent color rather than alpha
// USAGE : Pixel shaders
float softpart_GetSoftParticleFadePS( sampler2D depthSampler, float2 scrPos, float cz , float softness )
{
#if __PS3
	float Zdepth = 1.0f - texDepth2D(depthSampler, scrPos.xy);
#else
	float Zdepth = tex2D(depthSampler, scrPos.xy).x;	// Depth value at pixel position
#endif // __PS3

#if __XENON || __PS3
	float currDepth = Zdepth * NearFarPlane[2] + NearFarPlane[3]; 
	currDepth = 1.0f/currDepth;
#else
	float currDepth = Zdepth;
#endif // __XENON || __PS3

	// Fade particle as it intersects other objects and as it approaches near plane
	float depthDiff = (currDepth - cz);
	depthDiff *= depthDiff;

	return saturate( depthDiff * softness );
}	

#if RAGE_USE_SOFT_CONSTANTS
// PURPOSE : Gets the fade amount of a point by using a depht comparision 
// NOTES : Calculates screen pos given vpos
// USAGE : Pixel shaders
#if HACK_GTA4
float softpart_GetSoftParticleFadeVPosPS( sampler2D depthSampler, float2 vpos, float cz, float kfSoftness )
#else
float softpart_GetSoftParticleFadeVPosPS( sampler2D depthSampler, float2 vpos, float cz )
#endif
{
#if __PS3
	float2 screenPos = vpos * gInvScreenSize.xy;
	screenPos.y = 1.0f - screenPos.y;
#else
	float2 screenPos = vpos * gInvScreenSize.xy + gInvScreenSize.zw;
#endif // __PS3

#if HACK_GTA4
	return softpart_GetSoftParticleFadePS( depthSampler, screenPos, cz, kfSoftness);
#else
	return softpart_GetSoftParticleFadePS( depthSampler, screenPos, cz, gSoftness);
#endif // HACK_GTA4
}
#endif

#endif

