#ifndef RAGE_PTXGPURENDER_H
#define RAGE_PTXGPURENDER_H


struct ptxgpuVertexIn
{
#if __XENON
	int index				: INDEX;
#else
	float4 position			: TEXCOORD0;
	float4 velocity			: TEXCOORD1;
	float2 index			: TEXCOORD2;
#endif
};

struct ptxgpuVertexOut
{
	DECLARE_POSITION(pos)
#if __PS3 && HACK_GTA4
	float4 color			: TEXCOORD4;
#else
	float4 color			: COLOR0;
#endif
	float2 texCoord			: TEXCOORD0;
	float3 worldPos 		: TEXCOORD1;
#if __XENON || __PS3
	float3 ScreenPos		: TEXCOORD2;
#endif	
	float2 ExtraData		: TEXCOORD3;
};

struct ptxgpuParticleInfo
{
	DECLARE_POSITION(pos)
	float2 texCoord;
	float3 worldPos;
	float3 normal;
	float  life;
};


// PURPOSE : Calculate wrapping texture frame animation
float ptxgpuGetTextureFrameWrap( float life, float uCoord, float4 TexAnimation )
{
	return frac( ( int)(life * TexAnimation.x - TexAnimation.y )	 * TexAnimation.z )+ TexAnimation.z * uCoord;
}

// PURPOSE : Calculate clampping texture frame animation
float ptxgpuGetTextureFrameClamp( float life, float uCoord, float4 TexAnimation )
{
	return saturate( ( int)(life * TexAnimation.x - TexAnimation.y )  * TexAnimation.z ) + TexAnimation.z * uCoord;
}

#if __XENON
// PURPOSE : Get the State which is a 4 element position and velocity values
// REMARKS: This is the only cross platform function
void ptxgpuGetParticleState( ptxgpuVertexIn IN, out float2 uvs, out float4 Position4, out float4 Velocity4 )
{
	int index = IN.index;
	float4 PositionXY;
	float4 PositionZW;
	float4 VelocityXY;
	float4 VelocityZW;
	int idx = index / 4;
    asm {
        vfetch PositionXY.xy, idx, texcoord0
        vfetch PositionZW.xy, idx, texcoord1

		vfetch VelocityXY.xy, idx, texcoord2
        vfetch VelocityZW.xy, idx, texcoord3
    };
	Position4 = float4( PositionXY.xy, PositionZW.xy );
	Velocity4 = float4( VelocityXY.xy, VelocityZW.xy );

	uvs = float2( saturate( (( index + 1 )/2) %2 ), saturate( (index /2)% 2 ) ); 
}

#else

void ptxgpuGetParticleState( ptxgpuVertexIn IN, out float2 uvs,   out float4 Position4, out float4 Velocity4)
{
	Position4 =  IN.position;//IN.position.wzyx;
	Velocity4 = IN.velocity;//IN.velocity.wzyx;

	uvs = IN.index;
}
#endif


// PURPOSE : Convert from a ParticleInfo to a vertex out
// REMARKS : The color is not set as this is a game specific thing
ptxgpuVertexOut ptxgpuInfoToOut( float4 pos, float2 texCoord, float3 worldPos )
{
	ptxgpuVertexOut OUT=(ptxgpuVertexOut)0;
	OUT.texCoord = texCoord;
	OUT.worldPos = worldPos;
	OUT.pos = pos;
	OUT.color = float4( 0.0f, 0.0f, 0.0f, 0.0f);
	return OUT;
}


// PURPOSE : Uses the state and stanard setting creates the Particle information
// REMARKS: This is following from rmptfx
//
ptxgpuParticleInfo ptxgpuParticleCreate( float2 uvs , float4 Position4, float4 Velocity4, bool isDirectional, float radius, float motionBlur )
{
	ptxgpuParticleInfo OUT=(ptxgpuParticleInfo)0;

	OUT.life = Position4.w;

	OUT.texCoord = uvs;
	
	uvs.xy = uvs.xy - 0.5f;

	float3 velocity = float3( Velocity4.xyz );
	
	float3 up;
	float3 across;
	float3  offset;
	if ( isDirectional )
	{

		across = normalize(velocity );
		float3 viewDir = gViewInverse[3].xyz - Position4.xyz ;
	
		up = normalize( cross( across, viewDir ) );
		across = (radius * across) + (motionBlur.xxx * velocity );
		up *=radius;
		offset = uvs.x * up + (0.7 - ( uvs.y + 0.5f)) * -across;
	}	
	else
	{
		up = gViewInverse[0].xyz * radius ; 
		across = -gViewInverse[1].xyz * radius;
		offset = uvs.x * up + uvs.y * across;
	}
	
	OUT.normal = normalize( offset );
	
	// Transform position to the clipping space 
	float4 pos = float4( Position4.xyz, 1.0f );
	pos.xyz += offset.xyz;

	OUT.worldPos = pos.xyz;
	OUT.pos = (float4)mul(pos, gWorldViewProj);
	
	return OUT;
}



// packs two floats in the range [0.0...99.99) into a single float 
float PackPosW(float currLife, float maxLife)
{
	return floor(currLife*100.0f) + (maxLife/100.0f);
}

// unpacks a packed float into two floats in the range [0.0...99.99)
void UnPackPosW(float packedFloat, out float currLife, out float maxLife)
{
	float temp = frac(packedFloat);
	currLife = (packedFloat-temp) / 100.0f;
	maxLife = temp * 100.0f;
}

// packs two floats in the range [0.0...99.99) into a single float 
float PackVelW(float colnState, float seed)
{
	return floor(colnState*100.0f) + (seed/100.0f);
}

// unpacks a packed float into two floats in the range [0.0...99.99)
void UnPackVelW(float packedFloat, out float colnState, out float seed)
{
	float temp = frac(packedFloat);
	colnState = (packedFloat-temp) / 100.0f;
	seed = temp * 100.0f;
}



#endif
