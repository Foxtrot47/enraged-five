#include "rage_samplers.fxh"

BeginSampler(sampler2D,specTexture,SpecSampler,SpecularTex)
	string UIName="Specular Texture";
ContinueSampler(sampler2D,specTexture,SpecSampler,SpecularTex)
#if !__FXL 
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIPFILTER = LINEAR;
	MINFILTER = MIN_FILTER;
	MAGFILTER = LINEAR;
	LOD_BIAS = 0;
#else
	#if MIN_FILER != Linear		//don't reset if default
		MINFILTER = MIN_FILTER;
	#endif
#endif
EndSampler;
