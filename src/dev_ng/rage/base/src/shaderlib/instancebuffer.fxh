#include "rage_constants.h"

#if (defined(__D3D11) && __D3D11) || (defined(__SHADERMODEL) && __SHADERMODEL >= 40)
#define MAX_INSTANCES_PER_DRAW	32
#else
#define MAX_INSTANCES_PER_DRAW	8
#endif

#define MAX_INSTANCED_OVERLAPPING_CONSTANTS (3 * SKINNING_COUNT) //Instanced constants overlap skinning constants.

#define INSTANCE_CONSTANT_BUFFER_SLOT	7
#define INSTANCE_SHADER_CONSTANT_SLOT	64
#define INSTANCE_SHADER_CONTROL_SLOT	63 //Instance Control Var. (Needed by Xbox currently)
