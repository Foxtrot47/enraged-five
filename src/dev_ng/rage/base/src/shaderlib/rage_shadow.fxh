//-----------------------------------------------------
// Rage Cascaded Shadow Maps shader
//
#ifndef __RAGE_SHADOW_FXH
#define __RAGE_SHADOW_FXH

#include "rage_common.fxh"
#include "rage_xplatformtexturefetchmacros.fxh" // TexelSize
/*
// PC only
#if !__XENON && !__PS3
	float3 ClipPlanes;
#endif

float DepthBias;

#if !USE_GLOBAL_SHADOWLIGHTMTX
row_major float4x4 LightMatrixArr[4];
#if __XENON
	// all four starting points or also called split distances of the shadows
	#if USE_GLOBAL_SHADOWLIGHTMTX
		m_StartShadowsID = grcEffect::LookupGlobalVar("StartShadows");
	#else
		m_StartShadowsID = m_RenderIntoCollectorShader->LookupVar("StartShadows");
	#endif
#elif __PS3
	m_StartShadowsID = grcEffect::LookupGlobalVar("StartShadows");
#endif
 
float ShadowGradientScaleBias 
<
    string UIWidget = "slider";
    float UIMin = 0;
    float UIMax = 8.0;
    float UIStep = 0.0001;
    string UIName = "Shadow Gradient Scale Bias";
> = 0.1;

float ShadowFuzzyWidth 
<
    string UIWidget = "slider";
    float UIMin = 0;
    float UIMax = 8.0;
    float UIStep = 0.01;
    string UIName = "Shadow Fuzzy Width";
> = 2.0;


float GradientClamp 
<
	string UIWidget = "slider";
	float UIMin = 0;
	float UIMax = 1.0;
	float UIStep = 0.0001;
    string UIName = "Gradient Clamp";
> = 0.001;


float ZBias 
<
	string UIWidget = "slider";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.001;
    string UIName = "ZBias";
> 
#if __XENON
= 0.0;
#else
= 0.0008f;
#endif

// lots of love for RDR2 here ...
#ifndef TWOSHADOWMAPS
   #define TWOSHADOWMAPS 0
#endif

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float PS_ShadowMap( float4 ShadowPixelPos, sampler2D DepthSampler, sampler2D _RandomSampler, float FadeOut, float _Shadowmap_PixelSize0)
{
	// Get pixel depth from the point of view from the light camera
	float4 pos = ShadowPixelPos;	
	
	// fetch pixel depth from the shadow map
	// this is also from the point of view from the light camera
	pos.x = (pos.x + 1.0f) * 0.5;
	pos.y = 1.0f - ((pos.y + 1.0f) * 0.5);
	pos /= pos.w; // perspective divide for projection

	// get the pixel z bias value
	float pixDepth = (pos.z) - ZBias;
	pixDepth = min(pixDepth, 1.f);

#if __PS3
	pos.z = rageComputeDepth(pos);
#endif

	float4 depth;
	float2 pixeloffset = _Shadowmap_PixelSize0;   
	
	//
	// trapezoidal filter
	//
	float2 fFrac = frac(pos.xy * 1600.0f); // Fractional component of projected coordinates

	float4 bilinWeights  = float4(1.0-fFrac.x,     fFrac.x, 1.0-fFrac.x, fFrac.x); // Bilinear interpolation weights
	bilinWeights *= float4(1.0-fFrac.y, 1.0-fFrac.y,     fFrac.y, fFrac.y);
				   
	// just rotate the four taps 30 degrees to the right
	depth = float4(tex2D( DepthSampler, pos.xy + float2( -0.87 * pixeloffset.x,  0.5 * pixeloffset.y) ).x,
    			   tex2D( DepthSampler, pos.xy + float2( 0.5 * pixeloffset.x,  0.87 * pixeloffset.y) ).x,
    			   tex2D( DepthSampler, pos.xy + float2( 0.87 * pixeloffset.x, -0.5 * pixeloffset.y) ).x,
    			   tex2D( DepthSampler, pos.xy + float2(-0.5 * pixeloffset.x,  -0.87 * pixeloffset.y) ).x );
    			   

	
	
	//return PCF(DepthSampler, pos.xy, pixDepth);


#if FLOATDEPTHBUFFER
	depth = (1 - pixDepth < depth)? float4(0.0 + FadeOut, 0.0 + FadeOut, 0.0 + FadeOut, 0.0 + FadeOut) : float4(1.0f, 1.0f, 1.0f, 1.0f);
#else
	depth = (pixDepth < depth)? float4(0.0 + FadeOut, 0.0 + FadeOut, 0.0 + FadeOut, 0.0 + FadeOut) : float4(1.0f, 1.0f, 1.0f, 1.0f);
#endif		
	return dot(depth, bilinWeights);

}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float PS_ShadowMap_8tap( float4 ShadowPixelPos, sampler2D _DepthSampler, sampler2D _RandomSampler, float FadeOut, float _Shadowmap_PixelSize0)
{
	// Get pixel depth from the point of view from the light camera
	float4 pos = ShadowPixelPos;	
	
	// fetch pixel depth from the shadow map
	// this is also from the point of view from the light camera
	pos.x = (pos.x + 1.0f) * 0.5;
	pos.y = 1.0f - ((pos.y + 1.0f) * 0.5);
	pos /= pos.w; // perspective divide for projection

	// get the pixel z bias value
	float pixDepth = (pos.z) - ZBias;
	pixDepth = min(pixDepth, 1.f);

#if __PS3
	pos.z = rageComputeDepth(pos);
#endif

	float2 poisson[8] =
	{
		float2(0.527837, -0.085868),
		float2(-0.040088, 0.536087),
		float2(-0.670445, -0.179949),
		float2(-0.419418, -0.616039),
		float2(0.440453, -0.639399),
		float2(-0.757088, 0.349334),
		float2(0.574619, 0.685879),
		float2(0.03851, -0.939059)
	};

	// build up rotation matrix with the help of a look-up texture
	float2x2 RMat;
	RMat[0] = tex2Dlod(_RandomSampler, float4(pos.xy * 10.0f, 0.0, 0.0));
	RMat[0] = 2.0 * RMat[0] - 1.0;
	RMat[0] = normalize (RMat[0]);
	RMat[1] = float2(-1.0, 1.0) * RMat[0].yx;
	
	// default to zero
	float Shadow = 0.0f;
	float4 sPos = 0.0f;
	float texDepth;
	
	int numSamples = 8;
	
	// fetch a huge number of pixels and then run a PCF on them
	for (int i = 0; i < numSamples; i++)
	{
		sPos.xy = pos.xy;
		sPos.xy += mul(RMat, poisson[i]) * 2.0f * TexelSize.xy;

		texDepth = tex2Dlod (_DepthSampler, sPos).x;
#if FLOATDEPTHBUFFER
		Shadow += (1.0f - pixDepth < texDepth ) ?  (0.0 + FadeOut) : 1.0f;
#else
		Shadow += (pixDepth > texDepth ) ?  (0.0 + FadeOut) : 1.0f;
#endif		
	}

	return Shadow /= numSamples;	
}

// fade out function
float fadeOut(float CameraDistance, float ShadowFadeStart, float LengthOfFade)
{
	return 1.0f - saturate( (max(CameraDistance - ShadowFadeStart, 0) ) * 1.0 / LengthOfFade );
}
*/

#endif // __RAGE_SHADOW_FXH

