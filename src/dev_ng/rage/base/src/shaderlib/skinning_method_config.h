#define MTX_IN_VB				(0)
#define MTX_IN_TEX				(0)
#define MTX_IN_VB_HALF			(MTX_IN_VB && 0)
#define MTX_IN_TEX_HALF			(MTX_IN_TEX && 1)

#define MTX_IN_VB_STREAM		(3)

#define MTX_IN_CB				(RSG_PC && __D3D11)
