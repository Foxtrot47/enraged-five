#include "rage_samplers.fxh"

float DiffuseDetailMapTiling : DiffuseDetailMapTiling
<
	string UIName = "Diffuse Detail Tiling";
	float UIMin = 0.0;
	float UIMax = 200.0;
	float UIStep = 1.0;
> = 10.0;

BeginSampler(sampler2D,DiffuseDetailMap,DiffuseDetailMapSampler,DiffuseDetailMap)
	string UIName="Diffuse Detail Map";
ContinueSampler(sampler2D,DiffuseDetailMap,DiffuseDetailMapSampler,DiffuseDetailMap)
#if !__FXL 
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
#if __SHADERMODEL >= 40
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
#else
	MIPFILTER = LINEAR;
	MINFILTER = MIN_FILTER;
	MAGFILTER = LINEAR;
#endif
	LOD_BIAS = 0;
#else
	#if MIN_FILER != Linear		//don't reset if default
#if __SHADERMODEL >= 40
		MIN_LINEAR_MAG_POINT_MIP_POINT;
#else
		MINFILTER = MIN_FILTER;
#endif
	#endif
#endif
EndSampler;
