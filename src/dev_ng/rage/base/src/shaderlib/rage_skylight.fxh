//---
//	Calculates hemisphere lighting with bounce contribution and mid range color

#ifndef RAGE_SKYLIGHT_FXH
#define RAGE_SKYLIGHT_FXH

#ifndef DECLARE_SKY_VARS
float4 skyLight_LightRed: skyLight_LightRed;
float4 skyLight_LightGreen: skyLight_LightGreen;
float4 skyLight_LightBlue: skyLight_LightBlue;
#endif

float3 SkyLightCalculateFillLights( float3 normal , float AO, float bounceIntensity )
{
	// calculate weights
	float4 normal4 = normal.xyzx;
	normal4.w = abs( normal.y );
	
	const float4 scaleVal = float4( 1.0f, -1.0f, -1.0f, -1.0f/1.75f );
	const float4 baisVal = float4( 0.0f, 1.0f, 0.0f, 0.75f );
	
	float4 scale = float4( AO.xxx, bounceIntensity );
	float4 weights = saturate( normal4.ywyy * scaleVal + baisVal ) * scale;

	return float3( dot( weights, skyLight_LightRed ) ,
					dot( weights, skyLight_LightGreen ),
					dot( weights, skyLight_LightBlue ) );
}
#endif

