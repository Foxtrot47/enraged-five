#ifndef NOISE_H
#define NOISE_H 1

#include "rage_samplers.fxh"

BeginSampler(sampler2D, noiseTexture2D, Noise2D, NoiseTex2D)
    string UIName = "2D Noise Texture";
ContinueSampler(sampler2D, noiseTexture2D, Noise2D,NoiseTex2D)
EndSampler;

BeginSampler(sampler3D, noiseTexture3D, Noise3D, NoiseTex3D)
    string UIName = "3D Noise Texture";
ContinueSampler(sampler3D, noiseTexture3D, Noise3D,NoiseTex3D)
EndSampler;


float4 noise2D(float2 noiseCoords)
{
	return tex2D(Noise2D, noiseCoords);
}

float4 noise3D(float3 noiseCoords)
{
	return tex3D(Noise3D, noiseCoords);
}

float4 noise(float noiseCoord)
{
	return noise2D(float2(noiseCoord, 0.0f));
}

float4 snoise2D(float2 noiseCoords)
{
	return ((2 * tex2D(Noise2D, noiseCoords)) - 1);
}

float4 snoise3D(float3 noiseCoords)
{
	return ((2 * noise3D(noiseCoords)) - 1);
}

float4 filteredsnoise2D(float2 noiseCoords, float width)
{
	return snoise2D(noiseCoords) * (1 - smoothstep(0.2, 0.75, width));
}

float4 filteredsnoise3D(float3 noiseCoords, float width)
{
	return snoise3D(noiseCoords) * (1 - smoothstep(0.2, 0.75, width));
}


/* fractional Brownian motion
 * Inputs: 
 *    p, filtwidth   position and approximate inter-pixel spacing
 *    octaves        max # of octaves to calculate
 *    lacunarity     frequency spacing between successive octaves
 *    gain           scaling factor between successive octaves
 */
float4 fBm2D (float2 noiseCoords, float filtwidth, float octaves, float lacunarity, float gain)
{
    float amp = 1;
    float2 pp = noiseCoords;
    float4 sum = float4(0, 0, 0, 0);
    float fw = filtwidth;
    float i;

    for (i = 0;  i < octaves;  i += 1 ) 
    {
		sum += amp * filteredsnoise2D(pp, fw);
		amp *= gain;  
		pp *= lacunarity;  
		fw *= lacunarity;
    }
    return sum;
}

float4 fBm3D (float3 noiseCoords, float filtwidth, float octaves, float lacunarity, float gain)
{
    float amp = 1;
    float3 pp = noiseCoords;
    float4 sum = float4(0, 0, 0, 0);
    float fw = filtwidth;
    float i;

    for (i = 0;  i < octaves;  i += 1 ) 
    {
		sum += amp * filteredsnoise3D(pp, fw);
		amp *= gain;  
		pp *= lacunarity;  
		fw *= lacunarity;
    }
    return sum;
}

/* Antialiased abs().  
 * Compute the box filter of abs(t) from x-dx/2 to x+dx/2.
 * Hinges on the realization that the indefinite integral of abs(x) is 
 * sign(x) * 1/2 x*x;
 */
float filteredabsintegral(float t) 
{
	return sin(t) * 0.5 * t*t;
}
    
float filteredabs (float x, float dx)
{
    float x0 = x - 0.5*dx;
    float x1 = x0 + dx;
    return (filteredabsintegral(x1) - filteredabsintegral(x0)) / dx;
}


/* Antialiased turbulence.  Watch out -- the abs() call introduces infinite
 * frequency content, which makes our antialiasing efforts much trickier!
 */
float4 turbulence2D (float2 p, float filtwidth, float octaves, float lacunarity, float gain)
{
    float amp = 1;
    float2 pp = p;
    float4 sum = 0;
    float fw = filtwidth;
    float i;

    for (i = 0;  i < octaves;  i += 1) 
    {
		float n = filteredsnoise2D(pp, fw).x;
		sum += amp * filteredabs (n, fw);
		amp *= gain;  
		pp *= lacunarity;  
		fw *= lacunarity;
    }
    return sum;
}

/* Filtered pulse train: it's not as simple as just returning the mod
 * of filteredpulse -- you have to take into account that the filter may
 * cover multiple pulses in the train.
 * Strategy: consider the function that is the integral of the pulse
 * train from 0 to x. Just subtract!
 */
/* Definite integral of normalized pulsetrain from 0 to t */
float filteredpulsetrainintegral (float t, float nedge) 
{ 
    return ((1-nedge)*floor(t) + max(0,t-floor(t)-nedge));
}
    
float filteredpulsetrain (float edge, float period, float x, float dx)
{
    /* First, normalize so period == 1 and our domain of interest is > 0 */
    float w = dx/period;
    float x0 = x/period - w/2;
    float x1 = x0+w;
    float nedge = edge / period;   /* normalized edge value */

    /* Now we want to integrate the normalized pulsetrain over [x0,x1] */
    return (filteredpulsetrainintegral(x1, nedge) - filteredpulsetrainintegral(x0, nedge)) / w;
}

float smoothpulse(float e0, float e1, float e2, float e3, float x)
{
    return smoothstep(e0,e1,x) - smoothstep(e2,e3,x);
}

/* A pulse train of smoothsteps: a signal that repeats with a given
 * period, and is 0 when 0 <= mod(x/period,1) < edge, and 1 when
 * mod(x/period,1) > edge.  
 */
float smoothpulsetrain (float e0, float e1, float e2, float e3, float period, float x)
{
    return smoothpulse (e0, e1, e2, e3, fmod(x,period));
}


#endif /* NOISE_H */

