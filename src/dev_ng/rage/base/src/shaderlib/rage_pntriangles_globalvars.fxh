
// This should be included inside the braces for a global constant buffer.

#if __SHADERMODEL >= 40

#define MAX_FRUSTUM_PLANES 4
#define ENABLE_FRUSTUM_CULL (1)
#define ENABLE_BACKFACE_CULL (0)

shared float4 g_Rage_Tessellation_CameraPosition : g_Rage_Tessellation_CameraPosition	< int nostrip = 1; > = 0;
shared float4 g_Rage_Tessellation_CameraZAxis : g_Rage_Tessellation_CameraZAxis	< int nostrip = 1; > = 0;
shared float4 g_Rage_Tessellation_ScreenSpaceErrorParams : g_Rage_Tessellation_ScreenSpaceErrorParams < int nostrip = 1; > = 0;
shared float4 g_Rage_Tessellation_LinearScale : g_Rage_Tessellation_LinearScale	< int nostrip = 1; > = 0;
shared float4 g_Rage_Tessellation_Frustum[MAX_FRUSTUM_PLANES];

shared float4 g_Rage_Tessellation_Epsilons : g_Rage_Tessellation_Epsilons < int nostrip = 1; > = 0;
#define BackFaceCullEpsilon g_Rage_Tessellation_Epsilons.x
#define ViewFrustumEpsilon  g_Rage_Tessellation_Epsilons.y

#if defined(SHADER_FINAL)
	#define CullEnable			(ENABLE_FRUSTUM_CULL || ENABLE_BACKFACE_CULL)
#else
	#define CullEnable			((ENABLE_FRUSTUM_CULL || ENABLE_BACKFACE_CULL) && (g_Rage_Tessellation_Epsilons.z > 0.0f))
#endif

#endif

