#include "rage_samplers.fxh"

#ifndef RAGE_DIFFUSE_SAMPLER_H
#define RAGE_DIFFUSE_SAMPLER_H

#if HACK_GTA4
	#ifndef UINAME_SAMPLER_DIFFUSETEXTURE
		#define UINAME_SAMPLER_DIFFUSETEXTURE		"Diffuse Texture"
	#endif
#endif

BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
	#if HACK_GTA4
		string UIName = UINAME_SAMPLER_DIFFUSETEXTURE;
	#else
		string UIName = "Diffuse Texture";
	#endif
	string	TCPTemplate = "Diffuse";
	string	TextureType="Diffuse";
#ifdef USE_DIFFUSE_SAMPLER_NOSTRIP
	int nostrip=1;
#endif
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
	#if __FXL
		// these are the defaults for FXL
		#if DIFFUSE_CLAMP != Wrap   // wrap is default...
			AddressU = DIFFUSE_CLAMP;
			AddressV = DIFFUSE_CLAMP;
			AddressW = DIFFUSE_CLAMP;
		#endif
		#if HACK_GTA4 && defined(USE_ANISOTROPIC) && !__LOW_QUALITY
			MINFILTER = ANISOTROPIC;
			MAGFILTER = ANISOTROPIC; 
			MaxAnisotropy = 4;
		#else
			#if MIN_FILER != Linear		//don't reset if default
				MINFILTER = MIN_FILTER;
			#else
				MIPFILTER = LINEAR;
				MINFILTER = MIN_FILTER;
				MAGFILTER = LINEAR; 
			#endif
		#endif	
	#else //__FXL...
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = LINEAR;
		#if HACK_GTA4 && defined(USE_ANISOTROPIC) && !__LOW_QUALITY
			MINFILTER = ANISOTROPIC;
			MAGFILTER = ANISOTROPIC; 
			MaxAnisotropy = 4;
		#else
			MINFILTER = MIN_FILTER;
			MAGFILTER = LINEAR; 
		#endif
	#endif //!__FXL...
	
	#ifdef MIPMAP_LOD_BIAS
		MipMapLodBias = MIPMAP_LOD_BIAS;
	#endif
	#if defined(USE_HIGH_QUALITY_TRILINEARTHRESHOLD) && !__MAX
		TRILINEARTHRESHOLD=0;
	#endif
	#ifdef MIN_MIP_LEVEL
		MINMIPLEVEL = MIN_MIP_LEVEL;
	#endif
	#ifdef MAX_MIP_LEVEL
		MAXMIPLEVEL = MAX_MIP_LEVEL;
	#endif
EndSampler;

#if HACK_GTA4 && __MAX
	// 3DSMAX only: an extra DiffuseAlpha sampler (only for reading alpha field);
	//				must be defined right after DiffuseSampler (as samplers are set via indices in Max)
	BeginSampler(sampler2D,diffuseTextureAlpha,DiffuseSamplerAlpha,DiffuseTexAlpha)
		string UIName = "Diffuse Texture (Alpha only)";
		string	TCPTemplate = "Diffuse";
		string	TextureType="Diffuse";
#ifdef USE_DIFFUSE_SAMPLER_NOSTRIP
		int nostrip=1;
#endif
	ContinueSampler(sampler2D,diffuseTextureAlpha,DiffuseSamplerAlpha,DiffuseTexAlpha)
		#if __FXL  // these are the defaults for FXL
			#if DIFFUSE_CLAMP != Wrap   // wrap is default...
				AddressU = DIFFUSE_CLAMP;
				AddressV = DIFFUSE_CLAMP;
				AddressW = DIFFUSE_CLAMP;
			#endif

			#if MIN_FILER != Linear		//don't reset if default
				MINFILTER = MIN_FILTER;
			#else
				MIPFILTER = LINEAR;
				MINFILTER = MIN_FILTER;
				MAGFILTER = LINEAR; 
			#endif
		#else //__FXL
			AddressU = DIFFUSE_CLAMP;
			AddressV = DIFFUSE_CLAMP;
			AddressW = DIFFUSE_CLAMP;
			MIPFILTER = LINEAR;
			MINFILTER = MIN_FILTER;
			MAGFILTER = LINEAR; 
		#endif //!__FXL...

		#ifdef MIPMAP_LOD_BIAS
			MipMapLodBias = MIPMAP_LOD_BIAS;
		#endif
	EndSampler;
#endif //__MAX...


#endif
