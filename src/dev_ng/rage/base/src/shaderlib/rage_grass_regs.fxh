//
// All constant registers used by grass shader
//
//	24/11/2008	- Andrzej:	- initial;
//
//
//
#ifndef __GRASSREGS_FXH__
#define __GRASSREGS_FXH__


//
// VS registers used by grass shader:
//
#define GRASS_REG_TRANSFORM					(64)
#define GRASS_CREG_TRANSFORM				c64
#define GRASS_REG_PLANTCOLOR				(68)
#define GRASS_CREG_PLANTCOLOR				c68
#define GRASS_REG_GROUNDCOLOR				(69)
#define GRASS_CREG_GROUNDCOLOR				c69

#define GRASS_REG_CAMERAPOS					(80)
#define GRASS_CREG_CAMERAPOS				c80
#define GRASS_REG_PLAYERPOS					(81)
#define GRASS_CREG_PLAYERPOS				c81

#define GRASS_REG_DIMENSIONLOD2				(82)
#define GRASS_CREG_DIMENSIONLOD2			c82

#define GRASS_REG_PLAYERCOLLPARAMS			(83)
#define GRASS_CREG_PLAYERCOLLPARAMS			c83

#define GRASS_REG_VEHCOLLENABLED			(10)
#if __WIN32PC || __PSSL
#define GRASS_BREG_VEHCOLLENABLED			c92
#else
#define GRASS_BREG_VEHCOLLENABLED			b10
#endif // __WIN32PC || __PSSL

#define GRASS_REG_VEHCOLLB					(84)
#define GRASS_CREG_VEHCOLLB					c84
#define GRASS_REG_VEHCOLLM					(85)
#define GRASS_CREG_VEHCOLLM					c85
#define GRASS_REG_VEHCOLLR					(86)
#define GRASS_CREG_VEHCOLLR					c86

#define GRASS_REG_ALPHALOD0DISTUMTIMER		(87)
#define GRASS_CREG_ALPHALOD0DISTUMTIMER		c87
#define GRASS_REG_ALPHALOD1DIST				(88)
#define GRASS_CREG_ALPHALOD1DIST			c88
#define GRASS_REG_ALPHALOD2DIST				(89)
#define GRASS_CREG_ALPHALOD2DIST			c89
#define GRASS_REG_UMPARAMS					(90)
#define GRASS_CREG_UMPARAMS					c90

#define GRASS_REG_FAKEDNORMAL				(91)
#define GRASS_CREG_FAKEDNORMAL				c91

#endif //__GRASSREGS_FXH__...
