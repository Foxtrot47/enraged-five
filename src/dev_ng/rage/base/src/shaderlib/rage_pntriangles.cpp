
#include "rage_pntriangles.h"

#include "grcore/effect.h"

namespace rage
{


PNTriangleTessellationGlobals::PNTriangleTessellationGlobals()
{

}


PNTriangleTessellationGlobals::~PNTriangleTessellationGlobals()
{

}

// Computes and sets the tessellation globals...
void PNTriangleTessellationGlobals::Set(const grcViewport *pViewport, float nearDepth, float farDepth, u32 maxTessFactor, float screenSpaceErrorInPixels, float aspect, float FrustumEpsilon, float BackFaceCullEpsilon, bool bCull)
{
	(void)aspect;

	// TODO - Not good for Min spec as this will always be None
	if(m_Rage_Tessellation_CameraPosition_Var == rage::grcegvNONE)
	{
		m_Rage_Tessellation_CameraPosition_Var = grcEffect::LookupGlobalVar("g_Rage_Tessellation_CameraPosition");
		m_Rage_Tessellation_CameraZAxis_Var = grcEffect::LookupGlobalVar("g_Rage_Tessellation_CameraZAxis");
		m_Rage_Tessellation_ScreenSpaceErrorParams_Var = grcEffect::LookupGlobalVar("g_Rage_Tessellation_ScreenSpaceErrorParams");
		m_Rage_Tessellation_LinearScale_Var = grcEffect::LookupGlobalVar("g_Rage_Tessellation_LinearScale");
		m_Rage_Tessellation_Epsilons_Var = grcEffect::LookupGlobalVar("g_Rage_Tessellation_Epsilons");
		m_Rage_Tessellation_Frustum_Var = grcEffect::LookupGlobalVar("g_Rage_Tessellation_Frustum");
	}

	Mat44V_ConstRef cameraMatrix = pViewport->GetCameraMtx44();
	Vec4V cameraPosition = cameraMatrix.GetCol3();
	Vec4V cameraZAxis = -cameraMatrix.GetCol2();

	float nearPlane = nearDepth;
	float farPlane = farDepth;

	if(farPlane < nearPlane)
	{
		farPlane = nearPlane + 0.001f;
	}

	float width = (float)pViewport->GetWidth();
	float height = (float)pViewport->GetHeight();

	Vec4V pixelErrorParams = Vec4V((0.5f*width)/pViewport->GetTanHFOV(), 
		(0.5f*height)/pViewport->GetTanVFOV(), 
		screenSpaceErrorInPixels, 
		screenSpaceErrorInPixels);
	Vec4V linearParams = Vec4V(nearPlane, farPlane, (float)maxTessFactor, 0.0f);
	Vec4V vEpsilons = Vec4V(BackFaceCullEpsilon, FrustumEpsilon, (bCull ? 1.0f : 0.0f), 0.0f);

	// Frustum Planes
	const int MaxFrustumPlanes = 4;
	Vec4V avFrustumPlanes[MaxFrustumPlanes];
#if NV_SUPPORT
	avFrustumPlanes[0] = pViewport->GetCullFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT);
	avFrustumPlanes[1] = pViewport->GetCullFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT);
#else
	avFrustumPlanes[0] = pViewport->GetFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT);
	avFrustumPlanes[1] = pViewport->GetFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT);
#endif
	avFrustumPlanes[2] = pViewport->GetFrustumClipPlane(grcViewport::CLIP_PLANE_TOP);
	avFrustumPlanes[3] = pViewport->GetFrustumClipPlane(grcViewport::CLIP_PLANE_BOTTOM);

	SetGlobalVarV4(m_Rage_Tessellation_CameraPosition_Var, cameraPosition);
	SetGlobalVarV4(m_Rage_Tessellation_CameraZAxis_Var, cameraZAxis);
	SetGlobalVarV4(m_Rage_Tessellation_ScreenSpaceErrorParams_Var, pixelErrorParams);
	SetGlobalVarV4(m_Rage_Tessellation_LinearScale_Var, linearParams);
	SetGlobalVarV4(m_Rage_Tessellation_Epsilons_Var, vEpsilons);

	SetGlobalVarV4Array(m_Rage_Tessellation_Frustum_Var, avFrustumPlanes, MaxFrustumPlanes);
}

};
