#ifndef RAGE_DEFINES_H
#define RAGE_DEFINES_H

// rage commonly used functions and defines
#include "..\..\..\base\src\forceinclude\hacks.h"
#include "..\..\..\base\src\grcore\config_switches.h"

#if !__PS3 && (!HACK_GTA4)	// TEMPORARY HACK TO ELIMINATE WARNING SPEW!
#pragma warning (disable: 3595)
#pragma warning (disable: 3596)
#endif
#if __WIN32PC && HACK_GTA4
#pragma warning (disable: 3571)
#pragma warning (disable: 3205)
#pragma warning (disable: 3206)
#endif // __WIN32PC
#if __PSSL
// #pragma warning (disable: 4200)	// 'xxx' shadows previous declaration (too many false positives) (except that the previous decl spew cannot be turned off!)
#pragma warning (disable: 5202)	// implicit conversion to lower precision
#pragma warning (disable: 5203)	// parameter is unreferenced (would be nice to clean this up)
#pragma warning (disable: 5204)	// assignment causes truncation
#pragma warning (disable: 5205)	// implicit downcast (float3 to float, etc)
#pragma warning (disable: 5206)	// local variable is unreferenced (would be nice to clean this up)
#pragma warning (disable: 5557)	// vector or matrix accessed with dyanmic index
#pragma warning (disable: 5581)	// target treats 'half' type as full precision
#pragma warning (disable: 5609)	// half4 treated as float4 on this target
#endif

#ifndef __LOW_QUALITY 
	#define __LOW_QUALITY 0
#endif

#if RSG_PC || RSG_DURANGO
    #if __SHADERMODEL < 20
        // Currently, all we can go down to is 1.4
        //    If we need lower, we can allow it
        #define PIXELSHADER		ps_1_4
        #define VERTEXSHADER	vs_1_1
        #error 1
    #elif __SHADERMODEL >= 30
		#if __SHADERMODEL >= 40
			#pragma warning(error: 4121)
			#if __SHADERMODEL >= 41
				#if __SHADERMODEL >= 50
					#define PIXELSHADER		ps_5_0
					#define VERTEXSHADER	vs_5_0
				#else
					#define PIXELSHADER		ps_4_1
					#define VERTEXSHADER	vs_4_1
				#endif
			#else
				#define PIXELSHADER		ps_4_0
				#define VERTEXSHADER	vs_4_0
			#endif
		#else
			#define PIXELSHADER		ps_3_0
			#define VERTEXSHADER	vs_3_0
		#endif
    #else
		#error 1
        #define PIXELSHADER ps_2_0
        #define VERTEXSHADER vs_2_0
    #endif    // __SHADERMODEL

	#define VSDS_SHADER		VERTEXSHADER
	#define VSGS_SHADER		VERTEXSHADER
	#define VSGS_SHADER_50	vs_5_0
	#define DSGS_SHADER		ds_5_0
#elif RSG_ORBIS
	#define PIXELSHADER		ps_5_0
	#define VERTEXSHADER	vs_5_0
	#define VSDS_SHADER		vs_ls_5_0
	#define VSGS_SHADER		vs_es_5_0
	#define VSGS_SHADER_50	vs_es_5_0
	#define DSGS_SHADER		ds_es_5_0
#elif __XENON || __PS3 || __PSP2 || __MAX
    #define PIXELSHADER ps_3_0
    #define VERTEXSHADER vs_3_0
#endif

// ----------------------------------------------------------------------------------------------- //
// Shader compiler flag helpers
// ----------------------------------------------------------------------------------------------- //
#define SUPPORT_SHADER_COMPILER_FLAGS (RSG_DURANGO || RSG_ORBIS || RSG_PS3)
#if SUPPORT_SHADER_COMPILER_FLAGS
#define COMPILER_FLAGS_STRINGIFY(A) #A					// Converts A to "A"
#define COMPILER_FLAGS_STRING(x) x						// Compiler flags passed in as a string
#define COMPILER_FLAGS(x) COMPILER_FLAGS_STRINGIFY(x)   // Compiler flags are passed in raw, not yet a string
#else
#define COMPILER_FLAGS_STRING(x)
#define COMPILER_FLAGS(x)
#endif

#define COMPILER_FLAG_DEFINE(x) COMPILER_FLAGS(ORBIS_SWITCH(-D##x,/D##x))                          // Use the -Dx syntax for Orbis and /Dx for everything else
#define COMPILER_FLAG_DEFINE_VALUE(x,y) COMPILER_FLAGS(ORBIS_SWITCH(-D##x=y, /D##x=y))

#define DURANGO_COMPILER_FLAGS_STRING(x) DURANGO_ONLY(COMPILER_FLAGS_STRING(x))
#define DURANGO_COMPILER_FLAGS(x) DURANGO_ONLY(COMPILER_FLAGS(x))
#define DURANGO_COMPILER_OPT_AND_FLAGS(OPT_LEVEL, FLAGS)	DURANGO_ONLY(COMPILER_FLAGS(/O##OPT_LEVEL FLAGS))

#define ORBIS_COMPILER_FLAGS_STRING(x) ORBIS_ONLY(COMPILER_FLAGS_STRING(x))
#define ORBIS_COMPILER_FLAGS(x) ORBIS_ONLY(COMPILER_FLAGS(x))
#define ORBIS_COMPILER_OPT_AND_FLAGS(OPT_LEVEL, FLAGS)	ORBIS_ONLY(COMPILER_FLAGS(-O##OPT_LEVEL FLAGS))

#define ORBIS_DURANGO_COMPILER_FLAGS(ORBIS_FLAGS,DURANGO_FLAGS) ORBIS_ONLY(COMPILER_FLAGS(ORBIS_FLAGS)) DURANGO_ONLY(COMPILER_FLAGS(DURANGO_FLAGS))

// TODO: PS3_COMPILER_FLAGS and CGC_FLAGS can go away once we get rid of the linger PS3 compiler flags in the code base
#if RSG_PS3
#define PS3_COMPILER_FLAGS(x) PS3_ONLY(COMPILER_FLAGS_STRING(x)) 
#define CGC_FLAGS(x) PS3_COMPILER_FLAGS(x) 
#else
#define PS3_COMPILER_FLAGS(x) // Not sure why this is necessary since PS3_COMPILER_FLAGS is wrapped in a PS3_ONLY but I can't seem to get Orbis shaders to compile without it
#define CGC_FLAGS(x)
#endif
// ----------------------------------------------------------------------------------------------- //

// ----------------------------------------------------------------------------------------------- //
#if __PSSL
//output target format
#pragma PSSL_target_output_format(default PS4_OUTPUT_FORMAT)
#endif //__PSSL

#define PS4_TARGET(target)	ORBIS_COMPILER_FLAGS(COMPILER_FLAG_DEFINE_VALUE(PS4_OUTPUT_FORMAT,target))
// ----------------------------------------------------------------------------------------------- //

#if __PSSL
//semantics
#define SV_Target			S_TARGET_OUTPUT0
#define SV_Target0			S_TARGET_OUTPUT0
#define SV_Target1			S_TARGET_OUTPUT1
#define SV_Target2			S_TARGET_OUTPUT2
#define SV_Target3			S_TARGET_OUTPUT3
#define SV_Depth			S_DEPTH_OUTPUT
#define SV_Position			S_POSITION
#define COLOR				S_TARGET_OUTPUT0
#define DEPTH				S_DEPTH_OUTPUT
#define SV_IsFrontFace		S_FRONT_FACE
#define SV_Coverage			S_COVERAGE
#define SV_InstanceID		S_INSTANCE_ID
#define SV_RenderTargetArrayIndex	S_RENDER_TARGET_INDEX
#define SV_VertexID			S_VERTEX_ID
#define SV_SampleIndex		S_SAMPLE_INDEX
#define SV_PrimitiveID		S_PRIMITIVE_ID
#define SV_GroupID			S_GROUP_ID
#define SV_GroupThreadID	S_GROUP_THREAD_ID
#define SV_GroupIndex		S_GROUP_INDEX
#define SV_DispatchThreadID	S_DISPATCH_THREAD_ID
#define SV_ViewportArrayIndex	S_VIEWPORT_INDEX
#define SV_RenderTargetArrayIndex	S_RENDER_TARGET_INDEX
#define SV_TessFactor				S_EDGE_TESS_FACTOR
#define SV_InsideTessFactor			S_INSIDE_TESS_FACTOR
#define SV_OutputControlPointID		S_OUTPUT_CONTROL_POINT_ID
#define SV_DomainLocation			S_DOMAIN_LOCATION
//specials
#define point					Point
#define line					Line
#define lineadj					AdjacentLine
#define triangle				Triangle
#define triangleadj				AdjacentTriangle
#define ROW_MAJOR				row_major
//attributes
#define instance(x)				INSTANCE(x)
#define maxvertexcount(x)		MAX_VERTEX_COUNT(x)
#define numthreads(x,y,z)		NUM_THREADS(x,y,z)
#define domain(x)				DOMAIN_PATCH_TYPE(x)
#define partitioning(x)			PARTITIONING_TYPE(x)
#define outputtopology(x)		OUTPUT_TOPOLOGY_TYPE(x)
#define outputcontrolpoints(x)	OUTPUT_CONTROL_POINTS(x)
#define patchconstantfunc(x)	PATCH_CONSTANT_FUNC(x)
#define maxtessfactor(x)		MAX_TESS_FACTOR(x)
#define earlydepthstencil		FORCE_EARLY_DEPTH_STENCIL
//structures
#define Texture2DArray			Texture2D_Array
#define TextureCubeArray		TextureCube_Array
#define RWTexture2D				RW_Texture2D
#define RWBuffer				RW_DataBuffer
#define RWByteAddressBuffer		RW_ByteBuffer
#define RWStructuredBuffer		RW_RegularBuffer
#define Buffer					DataBuffer
#define ByteAddressBuffer		ByteBuffer
#define StructuredBuffer		RegularBuffer
#define AppendStructuredBuffer	AppendRegularBuffer
#define ConsumeStructuredBuffer	ConsumeRegularBuffer
#define PointStream				PointBuffer
#define LineStream				LineBuffer
#define TriangleStream			TriangleBuffer
//functions
#define	GroupMemoryBarrierWithGroupSync()	ThreadGroupMemoryBarrierSync()
#define SampleLevel				SampleLOD
#define SampleCmpLevelZero		SampleCmpLOD0
#define __tex1D(x,u)			x##__map.Sample(x,(u))
#define tex1D(x,u)				__tex1D(x,u)
#define __tex2D(x,uv)			x##__map.Sample(x,(uv))
#define tex2D(x,uv)				__tex2D(x,uv)
#define __tex2Dgrad(x,uv,dx,dy)	x##__map.SampleGradient(x,(uv),(dx),(dy))
#define tex2Dgrad(x,uv,dx,dy)	__tex2Dgrad(x,uv,dx,dy)
#define __tex2Dlod(x,uv_w)		x##__map.SampleLOD(x,(uv_w).xy,(uv_w).w)
#define tex2Dlod(x,uv_w)		__tex2Dlod(x,uv_w)
#define __tex3D(x,uvw)			x##__map.Sample(x,(uvw))
#define tex3D(x,uvw)			__tex3D(x,uvw)
#define __tex3Dlod(x,uv_w)		x##__map.SampleLOD(x,(uv_w).xyz,(uv_w).w)
#define tex3Dlod(x,uv_w)		__tex3Dlod(x,uv_w)
#define __texCUBE(x,xyz)		x##__map.Sample(x,(xyz))
#define texCUBE(x,xyz)			__texCUBE(x,xyz)
#define __texCUBElod(x,xyz_w)	x##__map.SampleLOD(x,(xyz_w).xyz,(xyz_w).w)
#define texCUBElod(x,xyz_w)		__texCUBElod(x,xyz_w)
int4 D3DCOLORtoUBYTE4(float4 bgra) { return int4(bgra.bgra * 255.0f); }
int4 D3DCOLORtoUBYTE4(int4 bgra) { return bgra.bgra; }
//atomic operations
#define InterlockedAdd			AtomicAdd
#define InterlockedMin			AtomicMin
#define InterlockedMax			AtomicMax
#define InterlockedOr			AtomicOr
#define InterlockedAnd			AtomicAnd
#define InterlockedXor			AtomicXor
#define InterlockedCompareStore		AtomicCmpStore
#define InterlockedCompareExchange	AtomicCmpExchange
#define InterlockedExchange		AtomicExchange

#elif __SHADERMODEL < 40
#define SV_Target	COLOR
#define SV_Target0	COLOR0
#define SV_Target1	COLOR1
#define SV_Target2	COLOR2
#define SV_Target3	COLOR3
#define SV_Depth	DEPTH
#define SV_Position	POSITION
#define uint		int
#define uint4		int4
#define ROW_MAJOR
#else	//__PSSL,__SHADERMODEL < 40
#define ROW_MAJOR
#endif	//__PSSL,__SHADERMODEL

#if __PS3
#define tex2Dgrad(x,uv,dx,dy)	tex2D(x,(uv),(dx),(dy))
#endif

#if __MAX
	#define DECLARE_POSITION(pos)					float4 pos:POSITION;
	#define DECLARE_POSITION_PSIN(pos)				float4 pos:VPOS;
	#define DECLARE_POSITION_CLIPPLANES(pos)		float4 pos:POSITION;
	#define DECLARE_POSITION_CLIPPLANES_PSIN(pos)	float4 pos:VPOS;
#else
	#define DECLARE_POSITION_PSIN(pos)				DECLARE_POSITION(pos)
	#define DECLARE_POSITION_CLIPPLANES_PSIN(pos)	DECLARE_POSITION_CLIPPLANES(pos)
#endif	//__MAX

#if __SHADERMODEL >= 40
	#define DECLARE_FACING_NAME(x)	x##flag
	#define DECLARE_FACING_INPUT(x)	bool x##flag : SV_IsFrontFace
	#define DECLARE_FACING_FLOAT(x) float x = x##flag? 1 : -1;
#else
	#define DECLARE_FACING_NAME(x)	x
	#define DECLARE_FACING_INPUT(x)	float x : VFACE
	#define DECLARE_FACING_FLOAT(x)
#endif	//__SHADERMODEL

#include "rage_constantbuffers.fxh"

// Define some useful constants so that shaders don't specify CW or CCW directly.
#define CULL_BACK	CW
#define CULL_FRONT	CCW

#define WRITE_RGB	7
#define WRITE_RGBA	15

#define MacroJoin( X, Y ) MacroDoJoin(X, Y)
#define MacroDoJoin( X, Y ) MacroDoJoin2(X, Y)
#define MacroDoJoin2( X, Y ) X##Y

//------------------------------------
#if !__PS3 && !__PSP2 && !__PSSL
#define REGISTER(x)	: register(x)
#define REGISTER2(x,y)	: register(x,y)
#else
#define REGISTER(x)	: register(x)
#define REGISTER2(x,y)	: register(y)
#endif

// http://www.parashift.com/c++-faq-lite/misc-technical-issues.html
#define PASTE2_HIDDEN(x,y)	x##y
#define PASTE2(x,y)			PASTE2_HIDDEN(x,y)

#if !__PS3
#define f1tex1D(X, Y) tex1D(X, Y).x
#define f2tex1D(X, Y) tex1D(X, Y).xy
#define f3tex1D(X, Y) tex1D(X, Y).xyz
#define f4tex1D(X, Y) tex1D(X, Y)
#define f1tex1Dproj(X, Y) tex1Dproj(X, Y).x
#define f2tex1Dproj(X, Y) tex1Dproj(X, Y).xy
#define f3tex1Dproj(X, Y) tex1Dproj(X, Y).xyz
#define f4tex1Dproj(X, Y) tex1Dproj(X, Y)
#define f1tex2D(X, Y) tex2D(X, Y).x
#define f2tex2D(X, Y) tex2D(X, Y).xy
#define f3tex2D(X, Y) tex2D(X, Y).xyz
#define f4tex2D(X, Y) tex2D(X, Y)
#define f1tex2Dproj(X, Y) tex2Dproj(X, Y).x
#define f2tex2Dproj(X, Y) tex2Dproj(X, Y).xy
#define f3tex2Dproj(X, Y) tex2Dproj(X, Y).xyz
#define f4tex2Dproj(X, Y) tex2Dproj(X, Y)
#define f1tex3D(X, Y) tex3D(X, Y).x
#define f2tex3D(X, Y) tex3D(X, Y).xy
#define f3tex3D(X, Y) tex3D(X, Y).xyz
#define f4tex3D(X, Y) tex3D(X, Y)
#define f1tex3Dproj(X, Y) tex3Dproj(X, Y).x
#define f2tex3Dproj(X, Y) tex3Dproj(X, Y).xy
#define f3tex3Dproj(X, Y) tex3Dproj(X, Y).xyz
#define f4tex3Dproj(X, Y) tex3Dproj(X, Y)
#define f1texCUBE(X, Y) texCUBE(X, Y).x
#define f2texCUBE(X, Y) texCUBE(X, Y).xy
#define f3texCUBE(X, Y) texCUBE(X, Y).xyz
#define f4texCUBE(X, Y) texCUBE(X, Y)
#define f1texCUBEproj(X, Y) texCUBEproj(X, Y).x
#define f2texCUBEproj(X, Y) texCUBEproj(X, Y).xy
#define f3texCUBEproj(X, Y) texCUBEproj(X, Y).xyz
#define f4texCUBEproj(X, Y) texCUBEproj(X, Y)
#define f1texRECT(X, Y) texRECT(X, Y).x
#define f2texRECT(X, Y) texRECT(X, Y).xy
#define f3texRECT(X, Y) texRECT(X, Y).xyz
#define f4texRECT(X, Y) texRECT(X, Y)
#define f1texRECTproj(X, Y) texRECTproj(X, Y).x
#define f2texRECTproj(X, Y) texRECTproj(X, Y).xy
#define f3texRECTproj(X, Y) texRECTproj(X, Y).xyz
#define f4texRECTproj(X, Y) texRECTproj(X, Y)
#define h1tex1D(X, Y) tex1D(X, Y).x
#define h2tex1D(X, Y) tex1D(X, Y).xy
#define h3tex1D(X, Y) tex1D(X, Y).xyz
#define h4tex1D(X, Y) tex1D(X, Y)
#define h1tex1Dproj(X, Y) tex1Dproj(X, Y).x
#define h2tex1Dproj(X, Y) tex1Dproj(X, Y).xy
#define h3tex1Dproj(X, Y) tex1Dproj(X, Y).xyz
#define h4tex1Dproj(X, Y) tex1Dproj(X, Y)
#define h1tex2D(X, Y) tex2D(X, Y).x
#define h2tex2D(X, Y) tex2D(X, Y).xy
#define h3tex2D(X, Y) tex2D(X, Y).xyz
#define h4tex2D(X, Y) tex2D(X, Y)
#define h1tex2Dproj(X, Y) tex2Dproj(X, Y).x
#define h2tex2Dproj(X, Y) tex2Dproj(X, Y).xy
#define h3tex2Dproj(X, Y) tex2Dproj(X, Y).xyz
#define h4tex2Dproj(X, Y) tex2Dproj(X, Y)
#define h1tex3D(X, Y) tex3D(X, Y).x
#define h2tex3D(X, Y) tex3D(X, Y).xy
#define h3tex3D(X, Y) tex3D(X, Y).xyz
#define h4tex3D(X, Y) tex3D(X, Y)
#define h1tex3Dproj(X, Y) tex3Dproj(X, Y).x
#define h2tex3Dproj(X, Y) tex3Dproj(X, Y).xy
#define h3tex3Dproj(X, Y) tex3Dproj(X, Y).xyz
#define h4tex3Dproj(X, Y) tex3Dproj(X, Y)
#define h1texCUBE(X, Y) texCUBE(X, Y).x
#define h2texCUBE(X, Y) texCUBE(X, Y).xy
#define h3texCUBE(X, Y) texCUBE(X, Y).xyz
#define h4texCUBE(X, Y) texCUBE(X, Y)
#define h1texCUBEproj(X, Y) texCUBEproj(X, Y).x
#define h2texCUBEproj(X, Y) texCUBEproj(X, Y).xy
#define h3texCUBEproj(X, Y) texCUBEproj(X, Y).xyz
#define h4texCUBEproj(X, Y) texCUBEproj(X, Y)
#define h1texRECT(X, Y) texRECT(X, Y).x
#define h2texRECT(X, Y) texRECT(X, Y).xy
#define h3texRECT(X, Y) texRECT(X, Y).xyz
#define h4texRECT(X, Y) texRECT(X, Y)
#define h1texRECTproj(X, Y) texRECTproj(X, Y).x
#define h2texRECTproj(X, Y) texRECTproj(X, Y).xy
#define h3texRECTproj(X, Y) texRECTproj(X, Y).xyz
#define h4texRECTproj(X, Y) texRECTproj(X, Y)
#define x1tex1D(X, Y) tex1D(X, Y).x
#define x2tex1D(X, Y) tex1D(X, Y).xy
#define x3tex1D(X, Y) tex1D(X, Y).xyz
#define x4tex1D(X, Y) tex1D(X, Y)
#define x1tex1Dproj(X, Y) tex1Dproj(X, Y).x
#define x2tex1Dproj(X, Y) tex1Dproj(X, Y).xy
#define x3tex1Dproj(X, Y) tex1Dproj(X, Y).xyz
#define x4tex1Dproj(X, Y) tex1Dproj(X, Y)
#define x1tex2D(X, Y) tex2D(X, Y).x
#define x2tex2D(X, Y) tex2D(X, Y).xy
#define x3tex2D(X, Y) tex2D(X, Y).xyz
#define x4tex2D(X, Y) tex2D(X, Y)
#define x1tex2Dproj(X, Y) tex2Dproj(X, Y).x
#define x2tex2Dproj(X, Y) tex2Dproj(X, Y).xy
#define x3tex2Dproj(X, Y) tex2Dproj(X, Y).xyz
#define x4tex2Dproj(X, Y) tex2Dproj(X, Y)
#define x1tex3D(X, Y) tex3D(X, Y).x
#define x2tex3D(X, Y) tex3D(X, Y).xy
#define x3tex3D(X, Y) tex3D(X, Y).xyz
#define x4tex3D(X, Y) tex3D(X, Y)
#define x1tex3Dproj(X, Y) tex3Dproj(X, Y).x
#define x2tex3Dproj(X, Y) tex3Dproj(X, Y).xy
#define x3tex3Dproj(X, Y) tex3Dproj(X, Y).xyz
#define x4tex3Dproj(X, Y) tex3Dproj(X, Y)
#define x1texCUBE(X, Y) texCUBE(X, Y).x
#define x2texCUBE(X, Y) texCUBE(X, Y).xy
#define x3texCUBE(X, Y) texCUBE(X, Y).xyz
#define x4texCUBE(X, Y) texCUBE(X, Y)
#define x1texCUBEproj(X, Y) texCUBEproj(X, Y).x
#define x2texCUBEproj(X, Y) texCUBEproj(X, Y).xy
#define x3texCUBEproj(X, Y) texCUBEproj(X, Y).xyz
#define x4texCUBEproj(X, Y) texCUBEproj(X, Y)
#define x1texRECT(X, Y) texRECT(X, Y).x
#define x2texRECT(X, Y) texRECT(X, Y).xy
#define x3texRECT(X, Y) texRECT(X, Y).xyz
#define x4texRECT(X, Y) texRECT(X, Y)
#define x1texRECTproj(X, Y) texRECTproj(X, Y).x
#define x2texRECTproj(X, Y) texRECTproj(X, Y).xy
#define x3texRECTproj(X, Y) texRECTproj(X, Y).xyz
#define x4texRECTproj(X, Y) texRECTproj(X, Y)
#endif // !__PS3

//
// if the value goes outside the range of a half-float you might run into bugs with pow() and log()
//
// Cg: pow() and log() intrinsics return undefined black blobs when input is not in valid range:
#define rageLog(a)			log(abs((a)))
#define rageLog2(a)			log2(abs((a)))
#define rageiLog2(a)		log2(abs(((float)a)))
#define ragePow(a, b)		pow(abs(a), (b))

#endif //RAGE_DEFINES_H
