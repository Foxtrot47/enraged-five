#ifndef LIGHTING_FXH
#define LIGHTING_FXH

#include "rage_common.fxh"

//
// rage lighting functions
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

//
// computes a Oren-Nayar reflection model as described in
// Michael Oren, Shree K. Nayar, "Generalization of Lambert's Reflectance Model", SIGGRAPH 1994, pp. 239 - 246
//
float ComputeOrenNayar(sampler2D SinTanSamp, float3 LightDir, float3 Normal, float3 Eye, float A, float B)
{
     
     float3 l = normalize(LightDir);
     float3 n = normalize(Normal);
     float3 e = normalize(Eye);
     
     // fetch texture with sin(alpha) * tan(alpha)
     float2 tcoord = {dot(l,n), max(0,dot(e,n))};
     float sintan = tex2D( SinTanSamp, 0.5f * tcoord + 0.5f ).x;
     
     // convert from spherical to a cartesian coord. system
     float3 al = normalize(l-dot(l,n)*n);
     float3 ae = normalize(e-dot(e,n)*n);
     float C = max(0, dot(al,ae));
 
     return dot(l,n) * (A + B * C * sintan);
}


//
// computes an anisotropic Ashikhmin-Shirley model as described in
// "An Anisotropic Phong BRDF Model" in Journal of Graphics Tools: JGT, Vol. 5, No.2, 2000: pp 25 - 32.
//
// This is a Pong like model with a diffuse and a specular component.
//
float3 ComputeAshikhminShirley(float3 f3Normal, float3 f3Tangent, float3 f3Binormal,
                                    float3 f3LightDir, float3 f3ViewDir, float fMX, float fMY)
{
    float3 N = normalize(f3Normal);
    float3 L = normalize(f3LightDir);
    float3 V = normalize(f3ViewDir);    
    float3 H = normalize(L + V);
    float3 T = normalize(f3Tangent);
    float3 B = normalize(f3Binormal); 

    float NL = saturate(dot(L, N));
    float NV = saturate(dot(V, N));    
    float HN = saturate(dot(H, N));
        
    float HT1 = dot(H, T);
    float HT2 = dot(H, B);

    float E = exp (-2 * (((HT1 / fMX) * (HT1 / fMX) + (HT2 / fMY) * (HT2 / fMY))/ (1 + HN)));
        
    // note - there are edge problems when using 1/sqrt(NLxNV)
    //return ((Di * f3Colour * NL)/ PI + (Si * ((1 /  sqrt(NL * NV) * (1/ (4 * PI * fMX * fMY))) * E))) * NL; 
    //return ((Di * f3Colour * NL)/ PI + (Si * (((1/ (4 * PI * fMX * fMY))) * E))) * NL; 

    return (((1/ (4 * PI * fMX * fMY))) * E);
}


float3 ComputeHemisphericLighting(sampler2D GroundSampler, sampler2D SkySampler, float2 texCoord, float2 SizeXY,
                                    float NL, float TweakHemisphere, float3 LightColor)
{
	// just divide texture coordinates through the size of the 
	// area to get the point were we need to sample.
	texCoord /= SizeXY.x;

    float3 GroundColor = h3tex2D(GroundSampler, texCoord);
    float3 SkyColor = h3tex2D(SkySampler, texCoord);    
	
	// hemispheric lighting
	return float4(LightColor * lerp(SkyColor, GroundColor, saturate(NL * 0.5 + 0.5) + TweakHemisphere), 1.0);  	
}

#endif // LIGHTING_FXH
