// 
// scaleform/input.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_INPUT_H 
#define SCALEFORM_INPUT_H 

#include "atl/array.h"
#include "input/mapper.h"

namespace rage {

class sfScaleformMovieView;

enum eBlockedMouseInputs
{
	BLOCKED_MOUSE_NONE = 0,
	BLOCKED_MOUSE_LEFT = BIT(0), // LMB
	BLOCKED_MOUSE_RIGHT = BIT(1), // RMB
	BLOCKED_MOUSE_MIDDLE = BIT(2), // MMB
	BLOCKED_MOUSE_SCROLL = BIT(3), // Mouse Wheel
	BLOCKED_MOUSE_MOVE = BIT(4), // X and Y movement
	BLOCKED_MOUSE_ALL = BIT(5)-1,
};



inline eBlockedMouseInputs operator | (eBlockedMouseInputs lhs, eBlockedMouseInputs rhs)
{
	return static_cast<eBlockedMouseInputs>( static_cast<int>(lhs) | static_cast<int>(rhs) );
}

inline eBlockedMouseInputs operator ^ (eBlockedMouseInputs lhs, eBlockedMouseInputs rhs)
{
	return static_cast<eBlockedMouseInputs>( static_cast<int>(lhs) ^ static_cast<int>(rhs) );
}

inline eBlockedMouseInputs& operator |= (eBlockedMouseInputs& lhs, const eBlockedMouseInputs rhs)
{
	lhs = lhs | rhs;
	return lhs;
}


class sfInput
{
public:


	void Init();
	void Update(float dt, eBlockedMouseInputs eBMI); // Does some per-frame stuff that's too expensive to do per-movie

	bool HaveButtonsChanged();
	unsigned GetReleasedButtons();
	unsigned GetPressedButtons();

	void SendMouseEvents(sfScaleformMovieView* movie);
	void SendKeyboardEvents(sfScaleformMovieView* movie);
	void SendPadEvents(sfScaleformMovieView* movie);

	void SetIgnoreBlocksForNextSend(bool bShouldIgnore) { m_bIgnoreBlocksForNextSend = bShouldIgnore; }
	void SetForceReleaseForNextSend() { m_bForceReleaseForNextSend = true; }

	// The number of analog stick actions that correspond to button presses
	static const int NUM_MAPPED_BUTTONS = 8;

	// Number of pads we support
	static const int MAX_MAPPED_PADS = 4;
protected:
	bool		m_KeysChanged;
	bool		m_bIgnoreBlocksForNextSend;
	bool		m_bForceReleaseForNextSend;
#if RSG_EXTRA_MOUSE_SUPPORT
	bool		m_bForceReleaseForNextUpdate;
	bool		m_bLastHadFocus;
#endif
	ioMapper	m_Mapper;
	atRangeArray<atRangeArray<ioValue, NUM_MAPPED_BUTTONS>, MAX_MAPPED_PADS>	m_Values;

	// We don't get mouse dx & dy data as often as we like, so compute it here.
	int			m_LastMouseX, m_LastMouseY, m_DX, m_DY;
	eBlockedMouseInputs m_BlockedInputs;
#if __DEV
	u32			m_LastFrameUpdated;
#endif
};

} // namespace rage

#endif // SCALEFORM_INPUT_H 
