// 
// scaleform/renderer.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SCALEFORM_RENDERER_H
#define SCALEFORM_RENDERER_H

#include "scaleformheaders.h"

#include "channel.h"

#include "atl/array.h"
#include "atl/delegate.h"
#include "grcore/effect_typedefs.h"
#include "grcore/stateblock.h"
#include "system/xtl.h"

#define SF_RECORD_COMMAND_BUFFER 0 && (__WIN32PC && __DEV)

namespace rage
{
	class grcEffect;
	class grcFvf;
	class grcTexture;
	class grcRenderTarget;
	class grcViewport;
	struct grcVertexDeclaration;
	class sfCommandBuffer;

	enum sfTechniques
	{
		sfTechSolidFill,
		sfTechTextureFill,
		sfTechAlphaSprite,
		sfTechColorSprite,
		sfTechGFill_ColorPremult,
		sfTechGFill_Color,
		sfTechGFill_1Texture,
		sfTechGFill_1TextureColor,
		sfTechGFill_2Texture,
		sfNumTechniques,
	};

	enum sfEffectVars {
		sfVarPosMtx,
		sfVarTex0Mtx,
		sfVarTex1Mtx,
		sfVarColor,
		sfVarColorOffset,
		sfVarColorScale,
		sfVarPremultiplyAlpha,
#if RSG_PC
		sfVarStereoFlag,
#endif
		sfVarTexture0,
		sfVarTexture1,
		//	sfVarTexture2,
		sfNumVars,
	};

	enum sfGlobalVars {
		sfVarGWorldViewProj,
		sfNumGlobals,
	};

	enum sfBlendStateBlocks
	{
		SF_BS_NORMAL,
		SF_BS_MULTIPLY,
		SF_BS_SCREEN,
		SF_BS_LIGHTEN,
		SF_BS_DARKEN,
		SF_BS_ADD,
		SF_BS_SUBTRACT,
		SF_BS_INVERT,
		SF_BS_ALPHA,
		SF_BS_STENCIL,
		NUM_SF_BS_HANDLES,
	};

	namespace sfGlobals
	{
		extern grcEffectTechnique	g_Techniques[sfNumTechniques];
		extern grcEffectVar			g_EffectVars[sfNumVars];
		extern grcEffectGlobalVar	g_GlobalVars[sfNumGlobals];
		extern atRangeArray<grcVertexDeclaration*, 9> g_ScaleformVertexDecls;
		extern atRangeArray<grcFvf*, 9> g_ScaleformFvfs;
		extern grcEffect*			g_Effect;
		extern grcBlendStateHandle g_BlendStateHandles[NUM_SF_BS_HANDLES];
	}

#if RSG_WIN32
#pragma warning(push)
#pragma warning(disable: 4263)
#endif

class sfTextureBase : public GTexture
{
public:
	sfTextureBase() {}
	virtual ~sfTextureBase() {}

	virtual int		GetWidth() const;
	virtual int		GetHeight() const;

	virtual bool	InitTexture(GImageBase* pim, UInt OUTPUT_ONLY(usage) /* = Usage_Wrap */) = 0;
	virtual bool	InitTexture(grcTexture* tex, bool unused = false) = 0;

	virtual grcTexture*		GetNativeTexture() const = 0;

	typedef rage::atDelegate<int (grcTexture*)> AddRefCallback;
	typedef rage::atDelegate<void (grcTexture*, int)> RemoveRefCallback;

	static AddRefCallback addRefCallback;
	static RemoveRefCallback removeRefCallback;

	static volatile u32	sm_TotalRuntimeImageSizes; // Total size of all images _not_ loaded from a file (and not counting the font cache texture)
};

#if RSG_WIN32
#pragma warning(pop)
#endif

#if USE_SCALEFORM_AMP
	#define SCALEFORM_AMP_DUMMY_IMPLEMENTATION(...)         __VA_ARGS__
#else
	#define SCALEFORM_AMP_DUMMY_IMPLEMENTATION(...)         = 0;
#endif

class sfRendererBase : public GRenderer
{
public:
	typedef sfTextureBase BaseTexture;
	typedef sfTextureBase TextureType;
	typedef grcTexture*	NativeTextureType;
	typedef grcRenderTarget* NativeRenderTargetType;

	enum
	{
		Vertex_XYUV32fC32 = GRenderer::Vertex_XY16iCF32 + 1 // Used for sprite drawing
	};

	virtual sfTextureBase* CreateTexture()                                  SCALEFORM_AMP_DUMMY_IMPLEMENTATION({return NULL;})
	virtual sfTextureBase* CreateTexture(NativeTextureType /*nativeTex*/)   SCALEFORM_AMP_DUMMY_IMPLEMENTATION({return NULL;})

	// AMP integration expects this enum to exist
	enum DisplayStatus
	{
		DisplayStatus_Ok			= 0,
		DisplayStatus_NoModeSet		= 1,
		DisplayStatus_Unavailable	= 2,
		DisplayStatus_NeedsReset	= 3,
	};
	DisplayStatus CheckDisplayStatus() const { return DisplayStatus_Ok; }

	// Used when you don't want to queue texture deletion for the next frame
	// (i.e. you _know_ that the textures aren't still in use by the GPU)
	// Generally only called by the manager
	virtual void SetUseDeletionQueue(bool)      SCALEFORM_AMP_DUMMY_IMPLEMENTATION({sfErrorf("Don't call this function except with the real rage renderer");})
	virtual bool GetUseDeletionQueue()          SCALEFORM_AMP_DUMMY_IMPLEMENTATION({sfErrorf("Don't call this function except with the real rage renderer"); return false;})

	// Call this right before Display() every time you want to render a movie in world space.
	virtual void RenderNextMovieInWorldSpace()  SCALEFORM_AMP_DUMMY_IMPLEMENTATION({})

#if __BANK
	virtual grcTexture* GetFontCacheTexture()   SCALEFORM_AMP_DUMMY_IMPLEMENTATION({return NULL;})
#endif

#if USE_SCALEFORM_AMP
	virtual void SetWireframeMode(bool)         SCALEFORM_AMP_DUMMY_IMPLEMENTATION({})
	virtual bool GetWireframeMode()             SCALEFORM_AMP_DUMMY_IMPLEMENTATION({return false;})
#endif

	virtual void ForceNormalBlendState(bool /*forceNormalBlendState*/)			SCALEFORM_AMP_DUMMY_IMPLEMENTATION({})
	virtual void OverrideBlendState(bool /*overrideBlendState*/)                SCALEFORM_AMP_DUMMY_IMPLEMENTATION({})
	virtual void OverrideRasterizerState(bool /*overrideRasterizerState*/)      SCALEFORM_AMP_DUMMY_IMPLEMENTATION({})
	virtual void OverrideDepthStencilState(bool /*overrideDepthStencilState*/)  SCALEFORM_AMP_DUMMY_IMPLEMENTATION({})

	static sfRendererBase* CreateRenderer();

#if USE_SCALEFORM_AMP
	static GAmpRendererImpl<sfRendererBase> *CreateAmpRendererWrapper(sfRendererBase* renderer);
#endif

#if SF_RECORD_COMMAND_BUFFER
	// PURPOSE: The next time a movie is rendered, we will record a command buffer.
	// PARAMS:
	//		triggerName - if NULL, record everything, otherwise only record movieclips whose rendererString property match the triggerName
	virtual void RecordCommandBuffer(const char* /*triggerName*/) = 0;
	virtual sfCommandBuffer* GetRecordedCommandBuffer() = 0;
	virtual void ClearRecordedCommandBuffer() = 0;
#endif

	static bool sm_AlwaysSmooth; // Set this BEFORE THE RENDERER IS CREATED to get smooth interpolation for all textures, regardless of flash settings
};

}

#endif // SCALEFORM_RENDERER_H
