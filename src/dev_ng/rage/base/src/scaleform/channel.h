// 
// scaleform/channel.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_CHANNEL_H 
#define SCALEFORM_CHANNEL_H 

#include "diag/channel.h"

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Scaleform)

#define sfFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(Scaleform,cond,fmt,##__VA_ARGS__)
#define sfAssertf(cond,fmt,...)				RAGE_ASSERTF(Scaleform,cond,fmt,##__VA_ARGS__)
#define sfVerifyf(cond,fmt,...)				RAGE_VERIFYF(Scaleform,cond,fmt,##__VA_ARGS__)
#define sfErrorf(fmt,...)					RAGE_ERRORF(Scaleform,fmt,##__VA_ARGS__)
#define sfWarningf(fmt,...)					RAGE_WARNINGF(Scaleform,fmt,##__VA_ARGS__)
#define sfDisplayf(fmt,...)					RAGE_DISPLAYF(Scaleform,fmt,##__VA_ARGS__)
#define sfDebugf1(fmt,...)					RAGE_DEBUGF1(Scaleform,fmt,##__VA_ARGS__)
#define sfDebugf2(fmt,...)					RAGE_DEBUGF2(Scaleform,fmt,##__VA_ARGS__)
#define sfDebugf3(fmt,...)					RAGE_DEBUGF3(Scaleform,fmt,##__VA_ARGS__)
#define sfLogf(severity,fmt,...)			RAGE_LOGF(Scaleform,severity,fmt,##__VA_ARGS__)
#define sfCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Scaleform,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END

#endif // SCALEFORM_CHANNEL_H 
