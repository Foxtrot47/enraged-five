Project scaleform
Files {
    renderer.h
    renderer.cpp
    input.h
    input.cpp
    scaleform.h
    scaleform.cpp
    scaleformheaders.h
    channel.h
    translator.h
    translator.cpp
	tweenstar.h
	tweenstar.cpp
	commandbuffer.h
	commandbuffer.cpp
#    Folder EmbeddedShaders {
#        embedded_scaleform_shaders_fxl_final.h
#        embedded_scaleform_shaders_psn.h
#        embedded_scaleform_shaders_win32_30.h
#    }
}
