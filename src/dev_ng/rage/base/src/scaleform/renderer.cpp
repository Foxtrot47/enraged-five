// 
// scaleform/renderer.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "renderer.h"

#include "channel.h"
#include "commandbuffer.h"

#include "atl/array.h"
#include "atl/vector.h"
#include "file/asset.h"
#include "grcore/effect.h"
#include "grcore/effect_typedefs.h"
#include "grcore/effect_values.h"
#include "grcore/image.h"
#include "grcore/indexbuffer.h"
#include "grcore/quads.h"
#include "grcore/effect_values.h"
#include "grcore/texture.h"
#include "grcore/vertexbuffer.h"
#include "grcore/viewport.h"
#include "paging/ref.h"
#include "profile/element.h"
#include "profile/timebars.h"
#include "system/cache.h"
#include "system/interlocked.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timemgr.h"

#include "system/xtl.h"

#if __WIN32
#include "system/d3d9.h"
#include "grcore/d3dwrapper.h"
#endif

#if __PS3
#include "grcore/wrapper_gcm.h"
#endif

EXT_PF_TIMER(MovieDraw_Stall);
PARAM(scaleform_alwayssmooth, "Always use smooth (linear) sampling for scaleform textures");

using namespace rage;
using namespace rage::sfGlobals;

bool sfRendererBase::sm_AlwaysSmooth = false;

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// sfTexture and sfRenderer implementation class declarations
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

class sfRenderer;

class sfTexture : public sfTextureBase
{
public:
	sfTexture(sfRenderer* renderer);

	virtual ~sfTexture();

	// Init functions initialize texture data, essentially allocating the
	// texture. Allocating texture may fail if there is not enough memory,
	// or the renderer is not ready for allocation.
	// Calling Init a second time will recreate the texture with a new size.

	// Target width/height specify the size image should have if it wasn't rounded up 
	// to the next power of two. That is the 'logical' texture size used during rendering.
	// Target width/height of 0 indicates the same size as the image.    
	virtual bool InitTexture(GImageBase* pim, UInt OUTPUT_ONLY(usage) /* = Usage_Wrap */);

	virtual bool InitTexture(grcTexture* tex, bool unused = false);

	virtual bool InitDynamicTexture(int /*width*/, int /*height*/, GImage::ImageFormat /*format*/, int /*mipmaps*/, UInt /*usage*/);

	// Update a region of a texture. Only guaranteed to work with textures created as updateable.
	// Supplied image must be the same format texture was created with.
	// Only updates specified mipmap level (0 is full size)
	virtual void Update(int ASSERT_ONLY(level), int n, const GTexture::UpdateRect* rects, const GImageBase* pim);

	// Map texture data to memory. Format of texture data may vary by platform. Not
	// always supported, especially if the texture was not created as mappable or updatable. A YUV texture
	// uses 3 (4 for YUV+A) maps.
	virtual int Map(int /*level*/, int /*n*/, GTexture::MapRect* /*maps*/, int /*flags*/ /* =0 */);

	virtual bool Unmap(int /*level*/, int /*n*/, GTexture::MapRect* /*maps*/, int /*flags*/ /* =0 */);

	// Obtains the renderer that create TextureInfo
	// GetRenderer can return 0 iff the renderer has been released, in that
	// case surviving GetTexture object should be just a stub
	virtual GRenderer* GetRenderer() const;

	virtual bool IsDataValid() const;

	// Set and get user data handle; can be useful to pass extra info
	// about the texture to a custom renderer implementation. Note that renderer
	// should still support the default handle value of null.
	virtual GTexture::Handle GetUserData() const;

	virtual void SetUserData(GTexture::Handle hdata);

	// Add/Remove notification
	virtual void AddChangeHandler(GTexture::ChangeHandler *phandler);

	virtual void RemoveChangeHandler(GTexture::ChangeHandler *ASSERT_ONLY(phandler));

	grcTexture*		GetGrcTexture() const { return m_Texture;}

	atFixedBitSet8	GetChannels() const {return m_Channels;}

	virtual int		GetWidth() const;

	virtual int		GetHeight() const;

	virtual grcTexture*		GetNativeTexture() const {return m_Texture;}

protected:
	sfRenderer*						m_Renderer;
	GTexture::Handle				m_UserData;
	pgRef<grcTexture>				m_Texture;
	GTexture::ChangeHandler*		m_Handler;
	grcImage*						m_Image;
	atFixedBitSet8					m_Channels; // we need this to know what tech. to bind
	int								m_CallbackData;
}; // sfTexture


class sfRendererSingleThreaded
{
public:

	sfRendererSingleThreaded();

	void RenderNextMovieInWorldSpace();

	// Set up to render a full frame from a movie and fills the
	// background.  Sets up necessary transforms, to scale the
	// movie to fit within the given dimensions.  Call
	// EndDisplay() when you're done.
	//
	// The Rectangle (viewportX0, viewportY0,
	// viewportX0 + viewportWidth, viewportY0 + viewportHeight)
	// defines the window coordinates taken up by the movie.
	//
	// The Rectangle (x0, y0, x1, y1) defines the pixel
	// coordinates of the movie that correspond to the viewport bounds.
	void BeginDisplay(GColor backgroundColor, const GViewport& viewport, float x0, float x1, float y0, float y1);

	// Called when we're done rendering
	void EndDisplay();

#if RSG_PC
	void SetStereoFlag(float flag);
#endif

	// Set the current transform for mesh & line-strip rendering.
	void SetMatrix(const GRenderer::Matrix& m);

	// Sets an additional matrix the user applies in addition to the SetMatrix
	// Not changed during Display().
	void SetUserMatrix(const GRenderer::Matrix& m);

	// Sets the current color transform for mesh and line-strip rendering
	void SetCxform(const GRenderer::Cxform& cx);

	// Pushes a Blend mode onto renderer.
	// Note: Blend modes are supposed to be cumulative, so if a clip with 
	// 'multiply' had a subclip with 'add' the subclip should get both blends.
	// If we really need this we'll deal with it by rendering off-screen and blending
	// back in to the current clip but for now we'll skip it.
	void PushBlendMode(GRenderer::BlendType mode);

	// Pops the last blend mode
	void PopBlendMode();

	// User data comes from the flash shapes, as a string and a float, and can be used
	// to push/pop shader variable settings or other renderer controls
	bool PushUserData(GRenderer::UserData* data);

	// Pops the last user data
	void PopUserData();

	// Sets the current vertex data. The contents of the vertex buffer will remain valid for any subsequent
	// draw calls until a new SetVertexData comes along (or EndDisplay)
	void SetVertexData(const void* vertices, int numVertices, GRenderer::VertexFormat vf, GRenderer::CacheProvider* cache);

	// Sets the current index data. The contents of the index buffer will remain valid for any subsequent
	// draw calls until a new SetIndexData comes along (or EndDisplay)
	void SetIndexData(const void* indices, int numIndices, GRenderer::IndexFormat idxf, GRenderer::CacheProvider* cache);

	// Draws the trilist from the previously set vertex and index buffer
	void DrawIndexedTriList(int baseVertexIndex, int minVertexIndex, int numVertices, int startIndex, int triangleCount);

	// Draws a line strip from the previously set vertex buffer
	void DrawLineStrip(int baseVertexIndex, int lineCount);

	// Turn off line drawing (?)
	void LineStyleDisable();

	// Sets the line color for the next line drawing commands
	void LineStyleColor(GColor color);

	// Turn off filling
	void FillStyleDisable();

	// Sets the fill color for the next filling commands
	void FillStyleColor(GColor color);

	// Sets the bitmap to fill with for the next filling commands
	void FillStyleBitmap(const GRenderer::FillTexture* fill);

	// Use the gouraud shaders for the next set of filling commands, using up to 3 textures.
	// Blending will be controlled by the vert data 
	void FillStyleGouraud(GRenderer::GouraudFillType fillType, 
		const GRenderer::FillTexture* texture0,
		const GRenderer::FillTexture* texture1,
		const GRenderer::FillTexture* texture2);

	// Draw a set of rectangles textured with the given bitmap, with the given color.
	// Apply given transform; ignore any currently set transforms.
	//
	// Intended for textured glyph rendering.
	void DrawBitmaps(GRenderer::BitmapDesc* bitmapList, int listSize, int startIndex, int count,
		const GTexture* texture, const GRenderer::Matrix& m, GRenderer::CacheProvider* cache);

	// Start rendering into the mask layer. 
	// maskMode can control which mask layer we're drawing into _and_ using. 
	// If its increment we draw into the next higher layer and start using it
	// If its decrement we draw using the prev. value, but decrement the counter.
	void BeginSubmitMask(GRenderer::SubmitMaskMode maskMode);

	// Stops mask layer rendering
	void EndSubmitMask();

	// Disables masking
	void DisableMask();

	// Returns a set of renderer statistics
	void GetRenderStats(GRenderer::Stats* stats, bool resetStats);

	// Another way to return renderer statistics
	void GetStats(GStatBag* bag, bool reset);

	// Called at the end of the game - to release any remaining resources
	void ReleaseResources();

#if USE_SCALEFORM_AMP
	void SetWireframeMode(bool useWires) { m_UseWireframe = useWires; }
	bool GetWireframeMode() { return m_UseWireframe; }
#endif

	////////////////
	// TODO: Implement these!
	void SetDisplayRenderTarget(GRenderTarget* /*prt*/, bool /*setstate*/) {}
	void PushRenderTarget(const GRectF& /*frameRect*/, GRenderTarget* /*prt*/) {}
	void PopRenderTarget() {}
	sfTextureBase* PushTempRenderTarget(const GRectF& /*frameRect*/, UInt /*targetW*/, UInt /*targetH*/, bool /*wantsStencil*/) { return NULL; }

	void SetPerspective3D(const GMatrix3D &/*projMatIn*/);
	void SetView3D(const GMatrix3D &/*viewMatIn*/);
	void SetWorld3D(const GMatrix3D * /*pWorldMatIn*/);

	////////////////
	// TODO: Implement these?
	void DrawBlurRect(GTexture* /*psrcin*/, const GRectF& /*insrcrect*/, const GRectF& /*indestrect*/, const GRenderer::BlurFilterParams& /*params*/, bool /*islast*/) {}
	void DrawColorMatrixRect(GTexture* /*psrcin*/, const GRectF& /*insrcrect*/, const GRectF& /*destrect*/, const Float * /*matrix*/, bool /*islast*/) {}

#if SF_RECORD_COMMAND_BUFFER
	void				RecordCommandBuffer(const char* triggerName) { m_CommandBuffer = NULL; m_CommandBufferState = CB_WAITING; m_CommandBufferTrigger = triggerName; }
	sfCommandBuffer*	GetRecordedCommandBuffer() { return m_CommandBufferState == CB_DONE ? m_CommandBuffer : NULL; }
	void				ClearRecordedCommandBuffer() { m_CommandBuffer = NULL; m_CommandBufferState = CB_NONE; m_CommandBufferTrigger = NULL; }
	bool				IsRecording() { return m_CommandBufferState == CB_RECORDING; }
	void				BeginRecording();
	void				EndRecording() { if (IsRecording()) { m_CommandBufferState = CB_DONE; } }
#endif

	void ForceNormalBlendState(bool forceNormalBlendState) { m_ForceNormalBlendState = forceNormalBlendState; }
	void OverrideBlendState(bool overrideBlendState) { m_BlendStateOverridden = overrideBlendState; m_DisableClear = overrideBlendState; }
	void OverrideRasterizerState(bool overrideRasterizerState) { m_RasterizerStateOverriden = overrideRasterizerState; m_DisableClear = overrideRasterizerState; }
	void OverrideDepthStencilState(bool overrideDepthStencilState) { m_DepthStencilStateOverriden = overrideDepthStencilState; m_DisableClear = overrideDepthStencilState; }

protected:
	void UpdateBlendMode(GRenderer::BlendType mode, bool force);
	void UpdateColorXform();

	sfTexture* GetSfTexture(const GRenderer::FillTexture* fill);

	void SetTexture(u32 texIndex, const GRenderer::FillTexture* fill);

	grcViewport				m_Viewport;
#if USE_SCALEFORM_AMP
	static const int maxBlendModes = 128;
#else
	static const int maxBlendModes = 16;
#endif
	atFixedArray<GRenderer::BlendType, maxBlendModes> m_BlendModeStack;
	GRenderer::BlendType	m_CurrentBlendMode;
	GRenderer::Cxform		m_RealXform;
	grcViewport*			m_OldViewport;
	u32						m_StencilValue;
	u32						m_Technique;
	bool					m_UsingScissor;
	bool					m_ForceXformToWhite;
	bool					m_JustClearedStencil;

	Mat44V					m_ViewportMatrix;
	Mat44V					m_UserMatrix;
	Mat44V					m_ProjectionMatrix;
	Mat44V					m_ViewMatrix;
	Mat44V					m_UVPMatrix;
	const GMatrix3D*		m_WorldMatrix;
	bool					m_UVPMatrixDirty;
	bool					m_HadWorldMatrix;
	bool					m_RenderInWorld;
#if RSG_PC
	float					m_StereoFlag;
#endif
#if USE_SCALEFORM_AMP
	bool					m_UseWireframe;
#endif

	const void*				m_CurrentVertices;
	u32						m_CurrentNumVertices;
	GRenderer::VertexFormat	m_CurrentVertexFormat;

	const void*				m_CurrentIndices;
	u32						m_CurrentNumIndices;
	GRenderer::IndexFormat	m_CurrentIndexFormat;

	GRenderer::Stats		m_RenderStats;

	atFixedArray<GRenderer::UserData, 16> m_UserData;
#if SF_RECORD_COMMAND_BUFFER
	sfCommandBuffer*		m_CommandBuffer;
	int						m_CurrVBIndex, m_CurrIBIndex;
	const char*				m_CommandBufferTrigger;
	enum CommandBufferState
	{
		CB_NONE,
		CB_WAITING,			// waiting for the record trigger
		CB_DONE,			// recording has finished
		CB_RECORDING,
	};
	CommandBufferState		m_CommandBufferState;
#endif

	bool m_ForceNormalBlendState;
	bool m_BlendStateOverridden;
	bool m_RasterizerStateOverriden;
	bool m_DepthStencilStateOverriden;
	bool m_DisableClear;
};

class sfRenderer : public sfRendererBase
{
public:

	sfRenderer();
	~sfRenderer();

	// GRenderer interfaces ////////////////////////////////////////////////////

    virtual bool GetRenderCaps(RenderCaps *pcaps);
    virtual sfTextureBase* CreateTexture();
    virtual sfTextureBase* CreateTextureYUV();
    virtual void BeginFrame();
    virtual void EndFrame();
    virtual GRenderTarget* CreateRenderTarget();
    virtual void SetDisplayRenderTarget(GRenderTarget* prt, bool setstate);
    virtual void PushRenderTarget(const GRectF& frameRect, GRenderTarget* prt);
    virtual void PopRenderTarget();
    virtual GTexture* PushTempRenderTarget(const GRectF& frameRect, UInt targetW, UInt targetH, bool wantStencil);
    virtual void ReleaseTempRenderTargets(UInt keepArea);
    virtual void BeginDisplay(GColor backgroundColor, const GViewport& viewport, Float x0, Float x1, Float y0, Float y1);
    virtual void EndDisplay();
#if RSG_PC
	virtual void SetStereoFlag(float flag);
#endif
    virtual void SetMatrix(const Matrix& m);
    virtual void SetUserMatrix(const Matrix& m);
    virtual void SetCxform(const Cxform& cx);
    virtual void PushBlendMode(BlendType mode);
    virtual void PopBlendMode();
    virtual bool PushUserData(UserData* pdata);
    virtual void PopUserData();

#ifndef GFC_NO_3D
    virtual void SetPerspective3D(const GMatrix3D &projMatIn);
    virtual void SetView3D(const GMatrix3D &viewMatIn);
    virtual void SetWorld3D(const GMatrix3D *pWorldMatIn);
#endif

    virtual void SetVertexData(const void* pvertices, int numVertices, VertexFormat vf, CacheProvider *pcache);
    virtual void SetIndexData(const void* pindices, int numIndices, IndexFormat idxf, CacheProvider *pcache);
    virtual void ReleaseCachedData(CachedData *pdata, CachedDataType type);
    virtual void DrawIndexedTriList(int baseVertexIndex, int minVertexIndex, int numVertices, int startIndex, int triangleCount);
    virtual void DrawLineStrip(int baseVertexIndex, int lineCount);
    virtual void LineStyleDisable();
    virtual void LineStyleColor(GColor color);
    virtual void FillStyleDisable();
    virtual void FillStyleColor(GColor color);
    virtual void FillStyleBitmap(const FillTexture* pfill);
    virtual void FillStyleGouraud(GouraudFillType fillType, const FillTexture *ptexture0, const FillTexture *ptexture1, const FillTexture *ptexture2);
    virtual void DrawBitmaps(GRenderer::BitmapDesc* pbitmapList, int listSize, int startIndex, int count, const GTexture* pti, const GRenderer::Matrix& m, GRenderer::CacheProvider *pcache);
    virtual void BeginSubmitMask(SubmitMaskMode maskMode);
    virtual void EndSubmitMask();
    virtual void DisableMask();
    virtual UInt CheckFilterSupport(const BlurFilterParams& params);
    virtual void DrawBlurRect(GTexture* psrcin, const GRectF& insrcrect, const GRectF& indestrect, const BlurFilterParams& params, bool islast);
    virtual void DrawColorMatrixRect(GTexture* psrcin, const GRectF& insrcrect, const GRectF& destrect, const Float *matrix, bool islast);
    virtual void GetRenderStats(Stats *pstats, bool resetStats);
    virtual void GetStats(GStatBag* pbag, bool reset);
    virtual void ReleaseResources();


	// sfRendererBase interfaces ///////////////////////////////////////////////

	virtual sfTextureBase* CreateTexture(NativeTextureType nativeTex);
	virtual void RenderNextMovieInWorldSpace();

#if __BANK
	virtual grcTexture* GetFontCacheTexture() { return m_FontCacheTexture; }
	virtual void SetFontCacheTexture(grcTexture* tex) { m_FontCacheTexture = tex; }
#endif

#if USE_SCALEFORM_AMP
	virtual void SetWireframeMode(bool useWires);
	virtual bool GetWireframeMode();
#endif

	virtual void ForceNormalBlendState(bool forceNormalBlendState);
	virtual void OverrideBlendState(bool overrideBlendState);
	virtual void OverrideRasterizerState(bool overrideRasterizerState);
	virtual void OverrideDepthStencilState(bool overrideDepthStencilState);


	struct TextureDeletionInfo {
		pgRef<grcTexture> m_Texture;
		class grcImage* m_Image;
		int m_CallbackData;
	};

	void ScheduleTextureDeletion(TextureDeletionInfo& tex);

	void SetUseDeletionQueue(bool b) { m_UseDeletionQueue = b; }
	bool GetUseDeletionQueue() {return m_UseDeletionQueue;}

	// Used when you may have queued texture deletions, but you know you don't need to wait anymore.
	// You typically don't have to call this yourself - it's done in sfRenderer::BeginFrame or sfRenderer's destructor
	// NOTE: Only call this when you're sure the GPU is done with any referenced textures too.
	void FlushDeletionQueue();


private:

	sfRendererSingleThreaded    m_st[NUMBER_OF_RENDER_THREADS];

	bool					m_UseDeletionQueue;

	atFixedVector<TextureDeletionInfo, 16>	m_TextureDeletionStack;


#if __ASSERT
	bool					m_BeginFrameCalled;
#endif
#if __BANK
	grcTexture*				m_FontCacheTexture;
#endif



	void DeleteTexture(TextureDeletionInfo& tex);
};


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// Global variables and initialization functions, shared across all renderer instances
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

volatile u32 sfTextureBase::sm_TotalRuntimeImageSizes = 0;
sfTextureBase::AddRefCallback sfTextureBase::addRefCallback;
sfTextureBase::RemoveRefCallback sfTextureBase::removeRefCallback;


namespace rage
{
	namespace sfGlobals
	{
		atRangeArray<grcVertexDeclaration*, 9> g_ScaleformVertexDecls;
		atRangeArray<grcFvf*, 9> g_ScaleformFvfs;
	}
}

#define START_VTX_DECL(vtxDecl)					\
{												\
	grcVertexDeclaration** declPtr = &vtxDecl;	\
	grcVertexElement elts[32];					\
	int elementIndex = 0;						\
//END

#define VTX_DECL_ATTR(theType, theChannel, theSize, theFormat)	\
	elts[elementIndex].type = theType;				\
	elts[elementIndex].channel = theChannel;		\
	elts[elementIndex].size = theSize;				\
	elts[elementIndex].format = theFormat;			\
	elementIndex++;
//END

#define END_VTX_DECL()													\
	*declPtr = GRCDEVICE.CreateVertexDeclaration(elts, elementIndex);	\
}																		\
//END

static void CreateVertexDeclarations()
{
	grcFvf* fvf = NULL;

	// Vertex_XY16i
	START_VTX_DECL(g_ScaleformVertexDecls[GRenderer::Vertex_XY16i]);
	VTX_DECL_ATTR(grcVertexElement::grcvetPosition, 0, 4, grcFvf::grcdsShort2);
	END_VTX_DECL();

	fvf = rage_new grcFvf();
	fvf->SetPosChannel(true, grcFvf::grcdsShort2, false);
	sfAssertf(!g_ScaleformFvfs[GRenderer::Vertex_XY16i], "FVF already exists!");
	g_ScaleformFvfs[GRenderer::Vertex_XY16i] = fvf;


	// Vertex_XY32f
	START_VTX_DECL(g_ScaleformVertexDecls[GRenderer::Vertex_XY32f]);
	VTX_DECL_ATTR(grcVertexElement::grcvetPosition, 0, 8, grcFvf::grcdsFloat2);
	END_VTX_DECL();

	fvf = rage_new grcFvf();
	fvf->SetPosChannel(true, grcFvf::grcdsFloat2, false);
	sfAssertf(!g_ScaleformFvfs[GRenderer::Vertex_XY32f], "FVF already exists!");
	g_ScaleformFvfs[GRenderer::Vertex_XY32f] = fvf;


	// Vertex_XY16iC32
	START_VTX_DECL(g_ScaleformVertexDecls[GRenderer::Vertex_XY16iC32]);
	VTX_DECL_ATTR(grcVertexElement::grcvetPosition, 0, 4, grcFvf::grcdsShort2);
	VTX_DECL_ATTR(grcVertexElement::grcvetColor,	0, 4, grcFvf::grcdsColor);
	END_VTX_DECL();

	fvf = rage_new grcFvf();
	fvf->SetPosChannel(true, grcFvf::grcdsShort2, false);
	fvf->SetDiffuseChannel(true, grcFvf::grcdsColor);
	sfAssertf(!g_ScaleformFvfs[GRenderer::Vertex_XY16iC32], "FVF already exists!");
	g_ScaleformFvfs[GRenderer::Vertex_XY16iC32] = fvf;


	// Vertex_XY16iCF32
	START_VTX_DECL(g_ScaleformVertexDecls[GRenderer::Vertex_XY16iCF32]);
	VTX_DECL_ATTR(grcVertexElement::grcvetPosition, 0, 4, grcFvf::grcdsShort2);
	VTX_DECL_ATTR(grcVertexElement::grcvetColor,	0, 4, grcFvf::grcdsColor);
	VTX_DECL_ATTR(grcVertexElement::grcvetColor,	1, 4, grcFvf::grcdsColor);
	END_VTX_DECL();

	fvf = rage_new grcFvf();
	fvf->SetPosChannel(true, grcFvf::grcdsShort2, false);
	fvf->SetDiffuseChannel(true, grcFvf::grcdsColor);
	fvf->SetSpecularChannel(true, grcFvf::grcdsColor);
	sfAssertf(!g_ScaleformFvfs[GRenderer::Vertex_XY16iCF32], "FVF already exists!");
	g_ScaleformFvfs[GRenderer::Vertex_XY16iCF32] = fvf;


	// Vertex_XYUV32fC32
	START_VTX_DECL(g_ScaleformVertexDecls[sfRendererBase::Vertex_XYUV32fC32]);
	VTX_DECL_ATTR(grcVertexElement::grcvetPosition, 0, 8, grcFvf::grcdsFloat2);
	VTX_DECL_ATTR(grcVertexElement::grcvetColor,	0, 4, grcFvf::grcdsColor);
	VTX_DECL_ATTR(grcVertexElement::grcvetTexture,  0, 8, grcFvf::grcdsFloat2);
	END_VTX_DECL();

	fvf = rage_new grcFvf();
	fvf->SetPosChannel(true, grcFvf::grcdsFloat2, false);
	fvf->SetDiffuseChannel(true, grcFvf::grcdsColor);
	fvf->SetTextureChannel(0, true, grcFvf::grcdsFloat2);
	sfAssertf(!g_ScaleformFvfs[sfRendererBase::Vertex_XYUV32fC32], "FVF already exists!");
	g_ScaleformFvfs[sfRendererBase::Vertex_XYUV32fC32] = fvf;
}

static void DestroyVertexDeclarations()
{
	for(int i = 0; i < g_ScaleformFvfs.GetMaxCount(); i++)
	{
		delete g_ScaleformFvfs[i];
		g_ScaleformFvfs[i] = NULL;
	}

	for(int i = 0; i < g_ScaleformVertexDecls.GetMaxCount(); i++)
	{
		if (g_ScaleformVertexDecls[i])
		{
			GRCDEVICE.DestroyVertexDeclaration(g_ScaleformVertexDecls[i]);
			g_ScaleformVertexDecls[i] = NULL;
		}
	}
}

struct XYUV32fC32
{
	volatile float m_X, m_Y;
	volatile u32 m_Color;
	volatile float m_U, m_V;
	inline void Set(float x, float y, float u, float v, Color32 color)
	{
		m_X = x;
		m_Y = y;
		m_Color = color.GetColor();
		m_U = u;
		m_V = v;
	}
};

namespace rage
{
namespace sfGlobals
{

grcEffectTechnique			g_Techniques[sfNumTechniques];
static const char* const	g_TechniqueNames[sfNumTechniques] = {
	"sfTechSolidFill",
	"sfTechTextureFill",
	"sfTechAlphaSprite",
	"sfTechColorSprite",
	"sfTechGFill_ColorPremult",
	"sfTechGFill_Color",
	"sfTechGFill_1Texture",
	"sfTechGFill_1TextureColor",
	"sfTechGFill_2Texture",
};

grcEffectVar				g_EffectVars[sfNumVars];
static const char* const	g_EffectVarNames[sfNumVars] = {
	"UIPosMtx",
	"UITex0Mtx",
	"UITex1Mtx",
	"UIColor",
	"UIColorXformOffset",
	"UIColorXformScale",
	"UIPremultiplyAlpha",
#if RSG_PC
	"UIStereoFlag",
#endif
	"UITexture0",
	"UITexture1",
};

grcEffectGlobalVar			g_GlobalVars[sfNumGlobals];
static const char* const	g_GlobalVarNames[sfNumGlobals] = {
	"WorldViewProjection"
};

static const char* const	g_ShaderName = "scaleform_shaders";
grcEffect*					g_Effect = NULL;

}
}

static void CreateEffect()
{
	sysMemStartTemp(); // Effects need to be created in the main heap
	g_Effect = grcEffect::Create(g_ShaderName);
	sysMemEndTemp();
	if (!g_Effect)
	{
		sfErrorf("Couldn't create effect %s for scaleform renderer", g_ShaderName);
	}

	for (int i = 0; i < sfNumTechniques; i++)
	{
		g_Techniques[i] = g_Effect->LookupTechnique(g_TechniqueNames[i]);
	}

	for (int i = 0; i < sfNumVars; i++) 
	{
#if RSG_PC
		if (i == sfVarStereoFlag)
			g_EffectVars[i] = g_Effect->LookupVar(g_EffectVarNames[i],false);
		else
			g_EffectVars[i] = g_Effect->LookupVar(g_EffectVarNames[i]);
#else
		g_EffectVars[i] = g_Effect->LookupVar(g_EffectVarNames[i]);
#endif
	}

	for (int i = 0; i < sfNumGlobals; i++)
	{
		g_GlobalVars[i] = g_Effect->LookupGlobalVar(g_GlobalVarNames[i]);
	}
}

static void DestroyEffect()
{
	sysMemStartTemp();
	delete g_Effect;
	g_Effect = NULL;
	sysMemEndTemp();
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// Code that can eventually be moved into grcore/device
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

static const u32 BEGIN_INDEXED_VERTICES_MAX_INDEX = 65536;

struct BufferPair
{
	void* m_Vertices;
	u16* m_Indices;
};

#if __XENON
static BufferPair BeginIndexedVertices(grcDrawMode dm, u32 vertexCount,u32 vertexSize, int baseIndex, u32 indexCount) {
	GRCDEVICE.ClearStreamSource(0);
	Assert(vertexCount * vertexSize <= grcDevice::BEGIN_VERTICES_MAX_SIZE);

	BufferPair result;
	result.m_Indices = NULL;
	result.m_Vertices = NULL;

	static D3DPRIMITIVETYPE translate[] = {
		D3DPT_POINTLIST,
		D3DPT_LINELIST,
		D3DPT_LINESTRIP,
		D3DPT_TRIANGLELIST,
		D3DPT_TRIANGLESTRIP,
		D3DPT_TRIANGLEFAN,
		D3DPT_QUADLIST,
		D3DPT_RECTLIST
	};
	grcStateBlock::Flush();
	HRESULT res = GRCDEVICE.GetCurrent()->BeginIndexedVertices(
		translate[dm], baseIndex, vertexCount, indexCount, D3DFMT_INDEX16, vertexSize, 
		reinterpret_cast<void**>(&result.m_Indices), &result.m_Vertices);
	if (res != S_OK)
	{
		result.m_Indices = NULL;
		result.m_Vertices = NULL;
	}

	return result;
}

static void EndIndexedVertices() {
	GRCDEVICE.GetCurrent()->EndIndexedVertices();
}


#endif // __XENON


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// sfTexture implementation
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT
static const char* const imageFormatNames[] = 
{
	"None",
	"ARGB_8888",
	"RGB_888",
	"<invalid>",
	"<invalid>",
	"<invalid>",
	"<invalid>",
	"<invalid>",
	"L_8",
	"A_8",
	"DXT1",
	"DXT3",
	"DXT5",
};
#endif


	sfTexture::sfTexture( sfRenderer* renderer ) : m_Renderer(renderer)
		, m_Texture(NULL)
		, m_Handler(NULL)
		, m_Image(NULL)
		, m_Channels(false)
		, m_CallbackData(-1)
	{
	}

	sfTexture::~sfTexture()
	{
		sfAssertf(m_Renderer, "Every texture must have a renderer");

		sfRenderer::TextureDeletionInfo texDel;
		texDel.m_Texture = m_Texture.ptr;
		texDel.m_Image = m_Image;
		texDel.m_CallbackData = m_CallbackData;

		m_Texture = pgRef<grcTexture>(NULL);

		m_Renderer->ScheduleTextureDeletion(texDel);
	}

	bool sfTexture::InitTexture( GImageBase* pim, UInt ASSERT_ONLY(usage) /* = Usage_Wrap */ )
	{
		m_Renderer = smart_cast<sfRenderer*>(GetRenderer()); // AMP wrappers keep their own renderer pointer. But I need one locally so I don't have to call virtual functions in the d'tor.
		sfAssertf(!m_Texture, "We don't support reinitting sfTextures (yet)"); 
		sfAssertf(!m_Image, "We don't support reinitting sfTextures (yet)"); 

		// Upping this to an assert, so we can track these down when we need to
		sfAssertf(0, "  Creating image from %s data, Usage flags: 0x%x", imageFormatNames[pim->Format], usage);

		bool dxtFormat = false;

		switch(pim->Format)
		{
		case GImageBase::Image_ARGB_8888:
			{
				m_Image = grcImage::Create(pim->Width, pim->Height, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
				sysMemCpy(m_Image->GetBits(), pim->pData, pim->DataSize);

				// Byte swap all the data
				u8* bits = m_Image->GetBits();
				u8* endBits = bits + m_Image->GetSize();
				for(; bits != endBits; bits += 4)
				{
					u8 r = bits[0];
					u8 g = bits[1];
					u8 b = bits[2];
					u8 a = bits[3];
#if __XENON || __PS3
					bits[0] = a;
					bits[1] = r;
					bits[2] = g;
					bits[3] = b;
#elif __WIN32PC || RSG_DURANGO	|| RSG_ORBIS // should be !RSG_BE?
					bits[0] = b;
					bits[1] = g;
					bits[2] = r;
					bits[3] = a;
#endif
				}
			}
			break;
		case GImageBase::Image_RGB_888:
			{
				m_Image = grcImage::Create(pim->Width, pim->Height, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);

				for(int i = 0; i < m_Image->GetHeight(); i++)
				{
					u8* destBits = m_Image->GetBits() + i * m_Image->GetStride();
					u8* destEnd = destBits + m_Image->GetWidth() * 4;

					u8* srcBits = pim->GetScanline(i);

					while(destBits != destEnd)
					{
						u8 r = srcBits[0];
						u8 g = srcBits[1];
						u8 b = srcBits[2];
#if __XENON || __PS3
						destBits[0] = 255;
						destBits[1] = r;
						destBits[2] = g;
						destBits[3] = b;
#elif __WIN32PC || RSG_DURANGO || RSG_ORBIS
						destBits[0] = b;
						destBits[1] = g;
						destBits[2] = r;
						destBits[3] = 255;
#endif
						srcBits += 3;
						destBits += 4;
					}
				}
			}
			break;
		case GImageBase::Image_DXT1:
		case GImageBase::Image_DXT3:
		case GImageBase::Image_DXT5:
			{
				dxtFormat = true;
				sfAssertf(_IsPowerOfTwo(pim->Height), "Height must be a power of 2");
				sfAssertf(_IsPowerOfTwo(pim->Width), "Width must be a power of 2");

				u32 format = grcImage::DXT1;
				if		(pim->Format == GImageBase::Image_DXT3) { format = grcImage::DXT3; }
				else if (pim->Format == GImageBase::Image_DXT5) { format = grcImage::DXT5; }

				// count the number of useful mips.
				u32 mipCount = 0;
				for(u32 i = 0; i < pim->MipMapCount; i++)
				{
					u32 width, height;
					pim->GetMipMapLevelData(i, &width, &height);
					if (width >= 4 && height >= 4)
					{
						mipCount++;
					}
				}

				m_Image = grcImage::Create(pim->Width, pim->Height, 1, (grcImage::Format)format, grcImage::STANDARD, mipCount, 0);
				for(u32 i = 0; i < mipCount; i++)
				{
					u32 width, height;
					u8* bytes = pim->GetMipMapLevelData(i, &width, &height);
					u32 size = GImageBase::GetMipMapLevelSize(pim->Format, width, height);
					sysMemCpy(m_Image->GetBits(0, i), bytes, size);
				}
				break;
			}
		case GImageBase::Image_A_8:
			{
				// count the number of useful mips.
				u32 mipCount = 0;
				for(u32 i = 0; i < pim->MipMapCount; i++)
				{
					u32 width, height;
					pim->GetMipMapLevelData(i, &width, &height);
					if (width >= 4 && height >= 4)
					{
						mipCount++;
					}
				}

				m_Image = grcImage::Create(pim->Width, pim->Height, 1, grcImage::A8, grcImage::STANDARD, mipCount, 0);
				for(u32 i = 0; i < mipCount; i++)
				{
					u32 width, height;
					u8* bytes = pim->GetMipMapLevelData(i, &width, &height);
					u32 size = GImageBase::GetMipMapLevelSize(pim->Format, width, height);
					sysMemCpy(m_Image->GetBits(0, i), bytes, size);
				}

				break;
			}
		default:
			sfErrorf("Unsupported image format %s", imageFormatNames[pim->Format]);
			return false;
		}

#if __PS3
		if (!dxtFormat)
		{
			if (!_IsPowerOfTwo(pim->Height) || !_IsPowerOfTwo(pim->Width))
			{
				// need to resize to a power of 2
				u32 newWidth = 1;
				u32 newHeight = 1;
				while(newWidth < pim->Width)
				{
					newWidth *= 2;
				}
				while(newHeight < pim->Height)
				{
					newHeight *= 2;
				}
				m_Image->Scale(newWidth, newHeight);
			}
		}
#endif

		if (dxtFormat || __WIN32PC) // Win32PC textures don't go in system memory or need swizzling
		{
			m_Texture = grcTextureFactory::GetInstance().Create(m_Image);
		}
		else
		{
			// For non-DXT Create the textures in system memory, so we can swizzle them 
			// (since this should be dev-time only anyway)
			grcTextureFactory::TextureCreateParams params(
				grcTextureFactory::TextureCreateParams::SYSTEM,
				grcTextureFactory::TextureCreateParams::TILED
				);

			m_Texture = grcTextureFactory::GetInstance().Create(m_Image, &params);
#if !RSG_PC
			grcTextureLock lock;
			m_Texture->LockRect(0, 0, lock);
			m_Texture->SwizzleTexture2D(lock);
			m_Texture->UnlockRect(lock);
#endif // !RSG_PC
		}

		m_Channels = m_Texture->FindUsedChannels();

		if (m_Handler)
		{
			m_Handler->OnChange(GetRenderer(), GTexture::ChangeHandler::Event_DataChange);
		}

		sysInterlockedAdd(&sm_TotalRuntimeImageSizes, m_Image->GetSize());

		return true;
	}

	bool sfTexture::InitDynamicTexture( int width, int height, GImage::ImageFormat format, int /*mipmaps*/, UInt usage )
	{
		m_Renderer = smart_cast<sfRenderer*>(GetRenderer()); // AMP wrappers keep their own renderer pointer. But I need one locally so I don't have to call virtual functions in the d'tor.
		sfAssertf(!m_Texture, "We don't support reinitting sfTextures (yet)"); 
		sfAssertf(!m_Image, "We don't support reinitting sfTextures (yet)"); 

		if ((usage & GTexture::Usage_Update) == 0)
		{
			sfErrorf("Only Usage_Update works for now, found 0x%x", usage);
			return false;
		}

		sfDebugf3("  Creating empty %dx%d image with format %s.", width, height, imageFormatNames[format]);

		if (format != GImageBase::Image_A_8)
		{
			sfErrorf("We only support A8 images right now");
			return false;
		}

		m_Image = grcImage::Create(width, height, 1, grcImage::A8, grcImage::STANDARD, 0, 0);
		sysMemSet(m_Image->GetBits(), 0, m_Image->GetSize());

		BANK_ONLY(grcTexture::SetCustomLoadName("sfTexture::InitDynamicTexture");)
#if __WIN32PC || __PS3
		//Create the texture as a Dynamic texture
		grcTextureFactory::TextureCreateParams params(
			grcTextureFactory::TextureCreateParams::VIDEO,	// Video indicates Dynamic CPU Write Access
			grcTextureFactory::TextureCreateParams::LINEAR);
#if __D3D11
		params.LockFlags = /*grcsRead |*/ grcsWrite; // So we get default so subresource can update.
#endif
		m_Texture = grcTextureFactory::GetInstance().Create(m_Image,&params);
#else
		grcTextureFactory::TextureCreateParams params(
			grcTextureFactory::TextureCreateParams::SYSTEM,
			grcTextureFactory::TextureCreateParams::LINEAR);

		m_Texture = grcTextureFactory::GetInstance().Create(m_Image, &params);
#endif
		BANK_ONLY(grcTexture::SetCustomLoadName(NULL);)

		m_Image->Release();
		m_Image = NULL; // No reason to keep the image around after creation.
		// rsTODO: We shouldn't even need to create the image in the first place - but PS3 can't create a texture w/o an image yet.

		// Don't swizzle this one - we need to do frequent updates.
		m_Channels = m_Texture->FindUsedChannels();

#if __BANK
		m_Renderer->SetFontCacheTexture(m_Texture); // I think that's a safe assumption for now.
#endif

		return true;
	}

	bool sfTexture::InitTexture( grcTexture* tex, bool /*unused*/ /*= false*/ )
	{
		sfAssertf(!m_Texture, "We don't support reinitting sfTextures (yet)"); 
		sfAssertf(!m_Image, "We don't support reinitting sfTextures (yet)"); 
		m_Renderer = smart_cast<sfRenderer*>(GetRenderer()); // AMP wrappers keep their own renderer pointer. But I need one locally so I don't have to call virtual functions in the d'tor.
		m_Texture = tex;
		m_Image = NULL;
		m_Channels = m_Texture->FindUsedChannels();

		if (addRefCallback.IsBound())
		{
			m_CallbackData = addRefCallback(m_Texture);
		}
		else
		{
			tex->AddRef();
		}

		if (m_Handler)
		{
			m_Handler->OnChange(GetRenderer(), GTexture::ChangeHandler::Event_DataChange);
		}

		return true;
	}

	int sfTextureBase::GetWidth() const
	{
		return GetNativeTexture()->GetWidth();
	}

	int sfTextureBase::GetHeight() const
	{
		return GetNativeTexture()->GetHeight();
	}

	void sfTexture::Update( int ASSERT_ONLY(level), int n, const GTexture::UpdateRect* rects, const GImageBase* pim )
	{
		PF_AUTO_PUSH_TIMEBAR_BUDGETED("sfTexture::Update", 0.4f);
		// Clear out the textures (we might need to overwrite them while they're active)
		g_Effect->SetVar(g_EffectVars[sfVarTexture0], grcTexture::None);
		g_Effect->SetVar(g_EffectVars[sfVarTexture1], grcTexture::None);

		sfAssertf(level == 0, "Only works with mipmap level 0, not %d", level);

#if __D3D9
		// The textures don't actually get set until the next bind, so force the texture samplers to be cleared now.
		GRCDEVICE.GetCurrent()->SetTexture(g_Effect->GetVarRegister(g_EffectVars[sfVarTexture0]), NULL);
		GRCDEVICE.GetCurrent()->SetTexture(g_Effect->GetVarRegister(g_EffectVars[sfVarTexture1]), NULL);
#endif // __WIN32

		u32 uBitPerPixel = m_Texture->GetBitsPerPixel();

		grcTextureLock lock;
#if !RSG_PC
		AssertVerify(m_Texture->LockRect(0, 0, lock, grcsWrite | grcsAllowVRAMLock | grcsDiscard));
#endif // !RSG_PC

		// Use UpdateSubresource to update texture to avoid locking it
		for (int i = 0; i < n; i++)
		{
			const GTexture::UpdateRect& update = rects[i];
			sfDebugf3("Updating FontCache Texture x=%d y=%d, l=%d t=%d r=%d b=%d", update.dest.x, update.dest.y, update.src.Left, update.src.Top, update.src.Right, update.src.Bottom );
#if USE_TELEMETRY
			static char szTexName[256];
			static const char szNull[] = "NULL";
			formatf(szTexName, sizeof(szTexName) - 1, "Updating %s FontCache Texture x=%d y=%d, l=%d t=%d r=%d b=%d", (m_Texture->GetName() ? m_Texture->GetName() : szNull),
				update.dest.x, update.dest.y, update.src.Left, update.src.Top, update.src.Right, update.src.Bottom);
			PF_AUTO_PUSH_TIMEBAR_BUDGETED(szTexName, 0.4f);
#endif // USE_TELEMETRY

			u32 width = update.src.Right - update.src.Left;
			u32 height = update.src.Bottom - update.src.Top;

			u8* srcPixel = pim->pData + (pim->GetPitch() * update.src.Top) + ((update.src.Left * uBitPerPixel) >> 3); // first pixel to read from
			grcPoint srcDim;
			srcDim.x = pim->GetPitch();
			srcDim.y = height;

			grcRect oDest = { update.dest.x, update.dest.y, update.dest.x + width, update.dest.y + height };
			m_Texture->Copy2D(srcPixel, srcDim, oDest, lock, 0);
		}
#if !RSG_PC
		m_Texture->UnlockRect(lock);
#endif // !RSG_PC
	}

	int sfTexture::Map( int /*level*/, int /*n*/, GTexture::MapRect* /*maps*/, int /*flags*/ /* =0 */ )
	{
		sfErrorf("Map unimplemented");
		return 0;
	}

	bool sfTexture::Unmap( int /*level*/, int /*n*/, GTexture::MapRect* /*maps*/, int /*flags*/ /* =0 */ )
	{
		sfErrorf("Unmap unimplemented");
		return false;
	}

	GRenderer* sfTexture::GetRenderer() const
	{
		return m_Renderer;
	}

	bool sfTexture::IsDataValid() const
	{
		return m_Texture != NULL;
	}

	GTexture::Handle sfTexture::GetUserData() const
	{
		return m_UserData;
	}

	void sfTexture::SetUserData( GTexture::Handle hdata )
	{
		m_UserData = hdata;
	}

	void sfTexture::AddChangeHandler( GTexture::ChangeHandler *phandler )
	{
		Assert(!m_Handler);
		m_Handler = phandler;
	}

	void sfTexture::RemoveChangeHandler( GTexture::ChangeHandler *ASSERT_ONLY(phandler) )
	{
		Assert(m_Handler == phandler);
		m_Handler = NULL;
	}

	int sfTexture::GetWidth() const
	{
		return m_Texture ? m_Texture->GetWidth() : 0;
	}

	int sfTexture::GetHeight() const
	{
		return m_Texture ? m_Texture->GetHeight() : 0;
	}


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// sfRendererSingleThreaded implementation
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

#define SCALEFORM_REPEAT	2
#define SCALEFORM_CLAMP		0
#define SCALEFORM_POINT		1
#define SCALEFORM_LINEAR	0

static grcSamplerStateHandle g_SamplerStateHandles[4];

namespace rage
{
	namespace sfGlobals
	{
		grcBlendStateHandle g_BlendStateHandles[NUM_SF_BS_HANDLES];
	}
}

enum sfRasterStateBlocks
{
	SF_RS_2D_DRAWING,
	SF_RS_3D_DRAWING,
	NUM_SF_RS_HANDLES,
};

static grcRasterizerStateHandle g_RasterizerStateHandles[NUM_SF_RS_HANDLES];

enum sfDepthStencilStateBlocks
{
	SF_DSS_2D_DRAWING,
	SF_DSS_3D_DRAWING,
	SF_DSS_2D_DRAWING_WITH_STENCIL,
	SF_DSS_3D_DRAWING_WITH_STENCIL,
	SF_DSS_BACKGROUND_CLEAR,
	SF_DSS_STENCIL_INCREMENT,
	SF_DSS_STENCIL_DECREMENT,
	NUM_SF_DSS_HANDLES,
};

static grcDepthStencilStateHandle g_DepthStencilStateHandles[NUM_SF_DSS_HANDLES];


static __forceinline void CreateBlendStateDesc(int BLEND_src, int BLENDOP_op, int BLEND_dest, int ALPHA_src, int ALPHA_op, int ALPHA_dest, grcBlendStateDesc& desc)
{
	desc.BlendRTDesc[0].BlendEnable = true;
	desc.BlendRTDesc[0].SrcBlend = BLEND_src;
	desc.BlendRTDesc[0].BlendOp = BLENDOP_op;
	desc.BlendRTDesc[0].DestBlend = BLEND_dest;
	
	desc.BlendRTDesc[0].SrcBlendAlpha = ALPHA_src;
	desc.BlendRTDesc[0].BlendOpAlpha = ALPHA_op;
	desc.BlendRTDesc[0].DestBlendAlpha= ALPHA_dest;
	
}

sfRendererSingleThreaded::sfRendererSingleThreaded()
: m_OldViewport(NULL)
, m_CurrentVertices(NULL)
, m_CurrentNumVertices(0)
, m_CurrentVertexFormat(GRenderer::Vertex_None)
, m_CurrentIndices(NULL)
, m_CurrentNumIndices(0)
, m_CurrentIndexFormat(GRenderer::Index_None)
, m_StencilValue(0)
, m_JustClearedStencil(false)
, m_ForceXformToWhite(false)
, m_Technique(sfTechSolidFill)
, m_ForceNormalBlendState(false)
, m_BlendStateOverridden(false)
, m_RasterizerStateOverriden(false)
, m_DepthStencilStateOverriden(false)
, m_DisableClear(false)
{
	Mat44V iden(V_IDENTITY);

	m_ViewportMatrix = iden;
	m_UserMatrix = iden;
	m_ProjectionMatrix = iden;
	m_ViewMatrix = iden;
	m_UVPMatrix = iden;
	m_WorldMatrix = NULL;
	m_UVPMatrixDirty = false;
	m_HadWorldMatrix = false;
	m_RenderInWorld = false;
#if RSG_PC
	m_StereoFlag = 0.0f;
#endif
#if USE_SCALEFORM_AMP
	m_UseWireframe = false;
#endif

#if SF_RECORD_COMMAND_BUFFER
	m_CommandBuffer = NULL;
	m_CommandBufferState = CB_NONE;
	m_CurrVBIndex = -1;
	m_CurrIBIndex = -1;
	m_CommandBufferTrigger = NULL;
#endif

	m_Viewport.ClearOwner();
}

#if SF_RECORD_COMMAND_BUFFER
void sfRendererSingleThreaded::BeginRecording()
{
	sfAssertf(m_CommandBufferState == CB_WAITING, "Invalid buffer state %d", m_CommandBufferState);
	sfAssertf(m_CommandBuffer == NULL, "Expected a NULL command buffer");
	m_CommandBuffer = rage_new sfCommandBuffer;
	m_CommandBufferState = CB_RECORDING;
}
#endif

void sfRendererSingleThreaded::RenderNextMovieInWorldSpace()
{
	m_RenderInWorld = true;
}

void sfRendererSingleThreaded::BeginDisplay( GColor backgroundColor, const GViewport& viewport, float x0, float x1, float y0, float y1 )
{
	PIXBegin(0, "Scaleform Rendering");

	if (!m_RenderInWorld)
	{
		int left = viewport.Left,
			top = viewport.Top,
			width = viewport.Width,
			height = viewport.Height;

#if SUPPORT_MULTI_MONITOR && 0
		// instead of doing the multi-monitor adjustment at render time, we do that at the creation time instead
		if (GRCDEVICE.GetMonitorConfig().isMultihead()) {
			const GridMonitor &monActive = GRCDEVICE.GetMonitorConfig().getLandscapeMonitor();
			const GridMonitor &monGlobal = GRCDEVICE.GetMonitorConfig().m_WholeScreen;
			// proceed only if rendering to the main screen
			if (GRCDEVICE.GetWidth() == (int)monGlobal.getWidth() && GRCDEVICE.GetHeight() == (int)monGlobal.getHeight())
			{
				left = (left * monActive.getWidth()) / monGlobal.getWidth() + monActive.uLeft;
				width = (width * monActive.getWidth()) / monGlobal.getWidth(); 
				top = (top * monActive.getHeight()) / monGlobal.getHeight() + monActive.uTop;
				height = (height * monActive.getHeight()) / monGlobal.getHeight();
			}
		}
#endif	//SUPPORT_MULTI_MONITOR

		// Handle partly offscreen buffers differently than rage normally does.
		// Find a new x0 and y0 for the ortho coords so that the image slides off screen
		if (viewport.Left < 0)
		{
			x0 = (x1 - x0) * (-(float)viewport.Left / (float)viewport.Width);
			width += left;
			left = 0;
		}
		if (viewport.Top < 0)
		{
			y0 = (y1 - y0) * (-(float)viewport.Top / (float)viewport.Height);
			height += top;
			top = 0;
		}
	
		m_Viewport.ResetWindow();
		m_Viewport.SetWindow(left, top, width, height);
		m_Viewport.Ortho(x0, x1, y1, y0, 0.0f, 1.0f);

		Mat44V proj = m_Viewport.GetProjection();
		Multiply(m_ViewportMatrix, proj, m_UserMatrix);

		m_OldViewport = grcViewport::SetCurrent(&m_Viewport);

#if USE_SCALEFORM_AMP
		if (m_UseWireframe)
		{  
			// No scissoring, lets see everything
			GRCDEVICE.DisableScissor();
		}
		else
#endif
		if (viewport.Flags & GViewport::View_UseScissorRect)
		{
			int vpleft = Clamp(viewport.ScissorLeft, 0, GRCDEVICE.GetWidth());
			int vpright = Clamp(viewport.ScissorLeft + viewport.ScissorWidth, vpleft, GRCDEVICE.GetWidth());
			int vpwidth = vpright - vpleft;

			int vptop = Clamp(viewport.ScissorTop, 0, GRCDEVICE.GetHeight());
			int vpbottom = Clamp(viewport.ScissorTop + viewport.ScissorHeight, vptop, GRCDEVICE.GetHeight());
			int vpheight = vpbottom - vptop;

			GRCDEVICE.SetScissor(vpleft, vptop, vpwidth, vpheight);
			m_UsingScissor = true;
		}
		else
		{
			int right = Min(left + width, viewport.BufferWidth);
			int bottom = Min(top + height, viewport.BufferHeight);

			int vpleft = Clamp(left, 0, GRCDEVICE.GetWidth());
			int vpright = Clamp(right, vpleft, GRCDEVICE.GetWidth());
			int vpwidth = vpright - vpleft;

			int vptop = Clamp(top, 0, GRCDEVICE.GetHeight());
			int vpbottom = Clamp(bottom, vptop, GRCDEVICE.GetHeight());
			int vpheight = vpbottom - vptop;

			GRCDEVICE.SetScissor(vpleft, vptop, vpwidth, vpheight);
			m_UsingScissor = true;
		}

	}
	else
	{
		// Assume the viewport has already been set up for us, so we don't need a special one.
		m_UsingScissor = false;
		GRCDEVICE.DisableScissor();


		m_OldViewport = NULL;
	}

	m_CurrentBlendMode = GRenderer::Blend_None;
	UpdateBlendMode(GRenderer::Blend_Normal, true); // Force one update
	SetVertexData(NULL, 0, GRenderer::Vertex_None, NULL);
	SetIndexData(NULL, 0, GRenderer::Index_None, NULL);

	if( !m_RasterizerStateOverriden )
		grcStateBlock::SetRasterizerState(g_RasterizerStateHandles[m_RenderInWorld ? SF_RS_3D_DRAWING : SF_RS_2D_DRAWING]);
	
	if( !m_DepthStencilStateOverriden )
		grcStateBlock::SetDepthStencilState(g_DepthStencilStateHandles[m_RenderInWorld ? SF_DSS_3D_DRAWING : SF_DSS_2D_DRAWING]);

	if (backgroundColor.GetAlpha() > 0 && m_DisableClear == false)
	{
		Color32 rageColor(backgroundColor.GetRed(), backgroundColor.GetGreen(), backgroundColor.GetBlue(), backgroundColor.GetAlpha());
		if (backgroundColor.GetAlpha() == 0xFF && !m_RenderInWorld)
		{
			// Solid color - quick clear
			GRCDEVICE.Clear(true, rageColor, false, 1.0f, 0); 
			m_JustClearedStencil = true;
		}
		else
		{
			// Semi-transparent or 3d - need to blend. Clear the stencil while we're here
			grcStateBlock::SetDepthStencilState(g_DepthStencilStateHandles[SF_DSS_BACKGROUND_CLEAR]);

			grcDrawSingleQuadf(x0, y0, x1, y1, 
				0.0f,
				0.0f, 0.0f, 1.0f, 1.0f, rageColor);

			grcStateBlock::SetDepthStencilState(g_DepthStencilStateHandles[m_RenderInWorld ? SF_DSS_3D_DRAWING : SF_DSS_2D_DRAWING]);
			m_JustClearedStencil = true;
		}
	}

	
	g_Effect->PushSamplerState(g_EffectVars[sfVarTexture0], g_SamplerStateHandles[SCALEFORM_CLAMP | SCALEFORM_LINEAR]);
	g_Effect->PushSamplerState(g_EffectVars[sfVarTexture1], g_SamplerStateHandles[SCALEFORM_CLAMP | SCALEFORM_LINEAR]);

#if SF_RECORD_COMMAND_BUFFER
	if (m_CommandBufferState == CB_WAITING && m_CommandBufferTrigger == NULL)
	{
		BeginRecording();
	}
#endif
}

void sfRendererSingleThreaded::EndDisplay()
{
#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		EndRecording();
	}
#endif

	// Clear out the textures (we might need to overwrite them)
	g_Effect->SetVar(g_EffectVars[sfVarTexture0], grcTexture::None);
	g_Effect->SetVar(g_EffectVars[sfVarTexture1], grcTexture::None);

	g_Effect->PopSamplerState(g_EffectVars[sfVarTexture0]);
	g_Effect->PopSamplerState(g_EffectVars[sfVarTexture1]);

	if (m_OldViewport)
	{
		grcViewport::SetCurrent(m_OldViewport);
	}
	m_OldViewport = NULL;

	if (m_UsingScissor)
	{
		GRCDEVICE.DisableScissor();
	}

	m_RenderInWorld = false;

	Assertf(m_BlendModeStack.GetCount() == 0, "%d blend modes were left in the stack", m_BlendModeStack.GetCount());
	PIXEnd(); // "Scaleform Rendering"
}

// PURPOSE: Sets 2 shader global vars to a 2x3 matrix (using x,y and w components)
static void SetShaderMatrix(sfEffectVars which, const GRenderer::Matrix& mtx
#if SF_RECORD_COMMAND_BUFFER
					 , sfCommandBuffer* commandBuffer
#endif
					 )
{
	Vector4 matrixVar[2];
	matrixVar[0].x = mtx.M_[0][0];
	matrixVar[0].y = mtx.M_[0][1];
	matrixVar[0].z = 0.0f;
	matrixVar[0].w = mtx.M_[0][2];

	matrixVar[1].x = mtx.M_[1][0];
	matrixVar[1].y = mtx.M_[1][1];
	matrixVar[1].z = 0.0f;
	matrixVar[1].w = mtx.M_[1][2];

	g_Effect->SetVar(g_EffectVars[which], matrixVar, 2);
#if SF_RECORD_COMMAND_BUFFER
	if (commandBuffer)
	{
		commandBuffer->Add_SetMatrix(which, matrixVar[0], matrixVar[1]);
	}
#endif
}

#if RSG_PC
void sfRendererSingleThreaded::SetStereoFlag(float flag)
{
	m_StereoFlag = flag;
}
#endif

void sfRendererSingleThreaded::SetMatrix( const GRenderer::Matrix& m )
{
	bool needsUpdate = false;
	Mat44V final;
	if (m_WorldMatrix)
	{
		if (m_UVPMatrixDirty)
		{
			m_UVPMatrixDirty = false;

			Mat44V vu;
			Multiply(vu, m_ViewMatrix, m_UserMatrix);
			Multiply(m_UVPMatrix, m_ProjectionMatrix, vu);

		}

		Mat44V world;
		sysMemCpy(&world, m_WorldMatrix, sizeof(m_WorldMatrix->M_)); // maybe reinterpret_cast? GMatrix may be unaligned tho

		// Final transform is (proj * view * user * world * m * v)
		Multiply(final, m_UVPMatrix, world);

		needsUpdate = true; // Not sure if worldMatrix changed - better assume it did 

		m_HadWorldMatrix = true;
	}
	else
	{
		needsUpdate = m_HadWorldMatrix; // 3d -> 2d: needs update
		m_HadWorldMatrix = false;
		// Final transform for vert v is (proj * user * m * v)
		final = m_ViewportMatrix;
	}

	if (needsUpdate)
	{
#if RSG_SM_50
		Mat44V *mtx = (Mat44V*) g_MatrixBase->BeginUpdate(4 * sizeof(Mat44V));
		mtx[0] = Mat44V(V_IDENTITY);	// not strictly necessary
		mtx[1] = Mat44V(V_IDENTITY);	// not strictly necessary
		mtx[2] = final;
		mtx[3] = Mat44V(V_IDENTITY);	// not strictly necessary
		g_MatrixBase->EndUpdate();
#else
		g_Effect->SetGlobalVar(g_GlobalVars[sfVarGWorldViewProj], final);
#endif //RSG_SM_50
	}

#if SF_RECORD_COMMAND_BUFFER
	SetShaderMatrix(sfVarPosMtx, m, IsRecording() ? m_CommandBuffer : NULL);
#else
	SetShaderMatrix(sfVarPosMtx, m);
#endif

#if RSG_PC
	// setting stereo params everytime matrix is set up
	g_Effect->SetVar(g_EffectVars[sfVarStereoFlag], &m_StereoFlag, 1);
#endif
}

void sfRendererSingleThreaded::SetUserMatrix( const GRenderer::Matrix& m )
{
	// Expand m to a 3d matrix
	// need to transform:
	// a b c       a b 0 c
	// e f g   ->  e f 0 g
	//             0 0 1 0
	//             0 0 0 1
	Mat44V iden(V_IDENTITY);
	m_UserMatrix = iden;

	m_UserMatrix.GetCol0Ref().SetXf(m.M_[0][0]); // a
	m_UserMatrix.GetCol0Ref().SetYf(m.M_[1][0]); // e
	m_UserMatrix.GetCol1Ref().SetXf(m.M_[0][1]); // b
	m_UserMatrix.GetCol1Ref().SetYf(m.M_[1][1]); // f
	m_UserMatrix.GetCol3Ref().SetXf(m.M_[0][2]); // c
	m_UserMatrix.GetCol3Ref().SetYf(m.M_[1][2]); // g

	m_UVPMatrixDirty = true;
}

void sfRendererSingleThreaded::SetPerspective3D(const GMatrix3D & m)
{
	CompileTimeAssert(sizeof(Mat44V) == sizeof(m.M_));
	sysMemCpy(&m_ProjectionMatrix, &m.M_, sizeof(m.M_));
	m_UVPMatrixDirty = true;
}

void sfRendererSingleThreaded::SetView3D(const GMatrix3D & m)
{
	CompileTimeAssert(sizeof(Mat44V) == sizeof(m.M_));
	sysMemCpy(&m_ViewMatrix, &m.M_, sizeof(m.M_));
	m_UVPMatrixDirty = true;
}

void sfRendererSingleThreaded::SetWorld3D(const GMatrix3D * m)
{
	m_WorldMatrix = m;
}

void sfRendererSingleThreaded::SetCxform( const GRenderer::Cxform& cx )
{
	m_RealXform = cx;

	const float toFloat = (1.0f / 255.0f);
	Vector4 offsetAndScale[2];

	if (Likely(!m_ForceXformToWhite))
	{
		offsetAndScale[0].x = m_RealXform.M_[0][1] * toFloat;
		offsetAndScale[0].y = m_RealXform.M_[1][1] * toFloat;
		offsetAndScale[0].z = m_RealXform.M_[2][1] * toFloat;
		offsetAndScale[0].w = m_RealXform.M_[3][1] * toFloat;

		offsetAndScale[1].x = m_RealXform.M_[0][0];
		offsetAndScale[1].y = m_RealXform.M_[1][0];
		offsetAndScale[1].z = m_RealXform.M_[2][0];
		offsetAndScale[1].w = m_RealXform.M_[3][0];
	}
	else
	{
		offsetAndScale[0].Set(1.0f, 1.0f, 1.0f, m_RealXform.M_[3][1] * toFloat);
		offsetAndScale[1].Set(0.0f, 0.0f, 0.0f, m_RealXform.M_[3][0]);
	}

	g_Effect->SetVar(g_EffectVars[sfVarColorOffset], &offsetAndScale[0], 1);
	g_Effect->SetVar(g_EffectVars[sfVarColorScale], &offsetAndScale[1], 1);

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		m_CommandBuffer->Add_Comment("ColorTransform");
		m_CommandBuffer->Add_SetVar(sfVarColorOffset, offsetAndScale[0]);
		m_CommandBuffer->Add_SetVar(sfVarColorScale, offsetAndScale[1]);
	}
#endif
}

#if !__NO_OUTPUT
	static const char* const blendModes[] = {
		"Blend_None",
		"Blend_Normal",    
		"Blend_Layer",  
		"Blend_Multiply",  
		"Blend_Screen",
		"Blend_Lighten",  
		"Blend_Darken", 
		"Blend_Difference",
		"Blend_Add",
		"Blend_Subtract",
		"Blend_Invert",
		"Blend_Alpha",
		"Blend_Erase",   
		"Blend_Overlay",
		"Blend_HardLight",
	};
#endif

void sfRendererSingleThreaded::PushBlendMode( GRenderer::BlendType mode )
{
	// We use the same hack the SF renderer does where we only apply the last blend mode
	// To do it "right" we'd have to compose all of the movie clips in their own render targets and
	// blend them back in to the main RT.
	m_BlendModeStack.Push(mode);
	if (mode != GRenderer::Blend_None)
	{
		UpdateBlendMode(mode, false);
	}
}

void sfRendererSingleThreaded::PopBlendMode()
{
	m_BlendModeStack.Pop();
	// search down the stack for the next interesting blend mode
	bool found = false;
	for(int i = m_BlendModeStack.GetCount()-1; i >= 0; i--)
	{
		if (m_BlendModeStack[i] != GRenderer::Blend_None)
		{
			UpdateBlendMode(m_BlendModeStack[i], false);
			found = true;
			break;
		}
	}
	if (!found)
	{
		UpdateBlendMode(GRenderer::Blend_Normal, false);
	}
}

void sfRendererSingleThreaded::UpdateBlendMode(GRenderer::BlendType mode, bool force)
{
	// see if we're already on this blend mode
	if (!force && m_CurrentBlendMode == mode)
	{
		return;
	}

	// return if blend state is externally overridden
	if (m_BlendStateOverridden)
	{		
		return;
	}

	m_CurrentBlendMode = mode;

	float premult = 0.0f;
	bool forceToWhite = false;

#if USE_SCALEFORM_AMP
	if (m_UseWireframe)
	{
		mode = GRenderer::Blend_None;
	}
#endif

	sfBlendStateBlocks blendState = SF_BS_NORMAL;

	switch(mode)
	{
	case GRenderer::Blend_None:
	case GRenderer::Blend_Normal:
		blendState = SF_BS_NORMAL;
		break;

	case GRenderer::Blend_Multiply:
		// Multiply:      (Sc * Dc)
		// Alpha multiply: Dc * (Sa * Sc) + (1 - Sa) * Dc
		// (Sa * Sc) is done in the shader (premult)
		premult = 1.0f;
		blendState = SF_BS_MULTIPLY;
		break;

	case GRenderer::Blend_Screen:
		// Screen:			(1 - (1 - Sc) * (1 - Dc))
		//				 =  Sc + (1 - Sc) * Dc
		// Alpha screen:	Sa * (Sc + (1 - Sc) * Dc) + (1 - Sa) * Dc
		//				 =  Sa * Sc + Sa * Dc - Sa * Sc * Dc + Dc - Sa * Dc
		//				 =  (1 - Dc) * Sa * Sc + Dc
		premult = 1.0f;
		blendState = SF_BS_SCREEN;
		break;

	case GRenderer::Blend_Lighten:
		blendState = SF_BS_LIGHTEN;
		break;

	case GRenderer::Blend_Darken:
		blendState = SF_BS_DARKEN;
		break;

	case GRenderer::Blend_Add:
		blendState = SF_BS_ADD;
		break;

	case GRenderer::Blend_Subtract:
		blendState = SF_BS_SUBTRACT;
		break;

	case GRenderer::Blend_Invert:
		// Regular invert: 1 - Dc
		// Alpha blended invert:			Sa * (1 - Dc) + (1 - Sa) * Dc
		// Force all rgb to white to get:	Sa * Sc * (1 - Dc) + (1 - Sa) * Dc
		// So now its a doable blend
		premult = 1.0f;
		forceToWhite = true;
		blendState = SF_BS_INVERT;
		break;

	case GRenderer::Blend_Alpha:
		blendState = SF_BS_ALPHA;
		break;

	case GRenderer::Blend_Layer:		// TODO: Fix me?
	case GRenderer::Blend_Difference:	// TODO: Fix me?
	case GRenderer::Blend_Erase:		// TODO: Fix me?
	case GRenderer::Blend_Overlay:		// TODO: Fix me?
	case GRenderer::Blend_HardLight:	// TODO: Fix me?
		sfDebugf1("Blend mode %s is not implemented", blendModes[mode]);
		blendState = SF_BS_NORMAL;
		break;
	}
	
	if( m_ForceNormalBlendState )
		grcStateBlock::SetBlendState(grcStateBlock::BS_Normal);
	else
		grcStateBlock::SetBlendState(g_BlendStateHandles[blendState]);

	g_Effect->SetVar(g_EffectVars[sfVarPremultiplyAlpha], &premult, 1);

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		m_CommandBuffer->Add_SetBlendState(blendState);
		m_CommandBuffer->Add_SetVar(sfVarPremultiplyAlpha, Vector4(premult));
	}
#endif

	if (forceToWhite != m_ForceXformToWhite)
	{
		m_ForceXformToWhite = forceToWhite;
		SetCxform(m_RealXform);
	}
}

bool sfRendererSingleThreaded::PushUserData( GRenderer::UserData* data)
{
	m_UserData.Push(data ? *data : GRenderer::UserData());
#if SF_RECORD_COMMAND_BUFFER
	// If there is data...
	if (data && data->pString)
	{
		// If we're waiting to start recording, check to see if this is the string that should trigger recording
		if (m_CommandBufferTrigger != NULL && m_CommandBufferState == CB_WAITING)
		{
			if (!strcmp(m_CommandBufferTrigger, data->pString))
			{
				BeginRecording();
			}
		}
	}
#endif
	return true;
}

void sfRendererSingleThreaded::PopUserData()
{
	GRenderer::UserData data = m_UserData.Pop();
	if (data.pString)
	{
#if SF_RECORD_COMMAND_BUFFER
		if (m_CommandBuffer && !strcmp(data.pString, "ReplayCB"))
		{
			m_CommandBuffer->Execute();
		}
		if (IsRecording() && m_CommandBufferTrigger && !strcmp(m_CommandBufferTrigger, data.pString))
		{
			EndRecording();
		}
#endif
	}
}

void sfRendererSingleThreaded::SetVertexData( const void* vertices, int numVertices, GRenderer::VertexFormat vf, GRenderer::CacheProvider* /*cache*/ /*= NULL*/ )
{
	m_CurrentVertices = vertices;
	m_CurrentNumVertices = numVertices;
	m_CurrentVertexFormat = vf;

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		if (vf == GRenderer::Vertex_None)
		{
			m_CurrVBIndex = -1;
		}
		else
		{
			m_CurrVBIndex = m_CommandBuffer->Add_VertexBuffer(vf, vertices, numVertices);
		}
	}
#endif
}

void sfRendererSingleThreaded::SetIndexData( const void* indices, int numIndices, GRenderer::IndexFormat idxf, GRenderer::CacheProvider* /*cache*/ /*= NULL*/ )
{
	m_CurrentIndices = indices;
	m_CurrentNumIndices = numIndices;
	m_CurrentIndexFormat = idxf;

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		if (idxf == GRenderer::Index_None)
		{
			m_CurrIBIndex = -1;
		}
		else
		{
			m_CurrIBIndex = m_CommandBuffer->Add_IndexBuffer(idxf, indices, numIndices );
		}
	}
#endif

}

#if __PPU
namespace rage 
{
	extern u16  g_VertexShaderInputs;
}
#endif

void sfRendererSingleThreaded::DrawIndexedTriList( int baseVertexIndex, int /*minVertexIndex*/, int XENON_ONLY(numVertices), int startIndex, int triangleCount )
{
	sfAssertf(m_CurrentIndexFormat == GRenderer::Index_16, "Index format must be u16");

	// Last minute change to the technique, now that we know both what vertex format we're getting and what fill mode to use.
	u32 technique = m_Technique;
	if (m_CurrentVertexFormat == GRenderer::Vertex_XY16iC32 && m_Technique == sfTechGFill_Color)
	{
		technique = sfTechGFill_ColorPremult;
	}

	// Figure out which effect to use.
	g_Effect->Bind(g_Techniques[technique]);

	int stride = g_ScaleformVertexDecls[m_CurrentVertexFormat]->Stream0Size;

#ifndef GFC_NO_STAT
	m_RenderStats.Triangles += triangleCount;
#endif

#if __PPU && 0 // RS: Turning this off for now. PS3 drawing will be slower, but scaleform sometimes re-uses the vertex and index buffers
			   // before the GPU is finished with them. Need to fix this either by implementing our own cache or hopefully SF 4.0 fixes 
			   // this for us.
	if (gcm::IsMainPtr(m_CurrentVertices) && gcm::IsMainPtr(m_CurrentIndices))
	{
		CellGcmEnum primType = CELL_GCM_PRIMITIVE_TRIANGLES;
#if USE_SCALEFORM_AMP
		if (m_UseWireframe)
		{
			primType = CELL_GCM_PRIMITIVE_LINE_STRIP;
		}
#endif
		SPU_COMMAND(grcDevice__DrawIndexedPrimitive,primType);
		cmd->decl = (spuVertexDeclaration*) g_ScaleformVertexDecls[m_CurrentVertexFormat];
		cmd->vertexData[0] = gcm::EncodeOffset(m_CurrentVertices) + baseVertexIndex * stride;
		cmd->vertexData[1] = cmd->vertexData[2] = cmd->vertexData[3] = 0;
		CompileTimeAssert(grcVertexDeclaration::c_MaxStreams == 4);
		cmd->indexData = gcm::EncodeOffset(m_CurrentIndices) + sizeof(u16) * startIndex;
		cmd->indexCount = triangleCount * 3;
		cmd->vertexInputs = g_VertexShaderInputs;
#ifndef GFC_NO_STAT
		m_RenderStats.Primitives++;
#endif
	}
	else
#endif
	{
		GRCDEVICE.SetVertexDeclaration(g_ScaleformVertexDecls[m_CurrentVertexFormat]);

		grcDrawMode primType = drawTris;

#if USE_SCALEFORM_AMP
		if (m_UseWireframe)
		{
			primType = drawLineStrip;
		}
#endif

#if SF_RECORD_COMMAND_BUFFER
		if (IsRecording())
		{
			sfAssertf((triangleCount *3) < USHRT_MAX, "Too many verts for one draw call: %d", triangleCount * 3);
			sfAssertf(startIndex == 0, "Can't do startIndex in a cross platform way yet");
			sfAssertf(baseVertexIndex < USHRT_MAX, "Base vertex index too high: %d", baseVertexIndex);
			m_CommandBuffer->Add_DrawIndexedTriangles((sfTechniques)m_Technique, m_CurrVBIndex, m_CurrIBIndex, (u16)(triangleCount * 3), (u16)baseVertexIndex);
		}
#endif

#if __XENON

		int batchSize = (BEGIN_INDEXED_VERTICES_MAX_INDEX / 3) * 3;
		int batchStartIndex = 0;
		int indicesRemaining = triangleCount * 3;
		int indicesInBatch = Min(batchSize, indicesRemaining);
		do 
		{
			// Xenon has a quick way to put both vertex and index buffers in the command list.
			PF_START(MovieDraw_Stall);
			BufferPair buffers = BeginIndexedVertices(primType, numVertices, stride, 0, indicesInBatch);
			PF_STOP(MovieDraw_Stall);
			if (buffers.m_Indices && buffers.m_Vertices)
			{
				sysMemCpyStreaming(buffers.m_Vertices, (char*)m_CurrentVertices + stride * baseVertexIndex, stride * numVertices);
				sysMemCpyStreaming(buffers.m_Indices, (u16*)m_CurrentIndices + batchStartIndex + startIndex, sizeof(u16) * indicesInBatch);
				EndIndexedVertices();
#ifndef GFC_NO_STAT
				m_RenderStats.Primitives++;
#endif
			}

			batchStartIndex += indicesInBatch;
			indicesRemaining -= indicesInBatch;
			indicesInBatch = Min(batchSize, indicesRemaining);
		} while (indicesRemaining > 0);

#else
		// Flatten out the indices

		int batchSize = (GPU_VERTICES_MAX_SIZE / stride); // Max # of bytes
		batchSize = Min(batchSize, 65536); // Max # of verts
		batchSize = batchSize / 3 * 3; // round down to mult. of 3.
		int batchStartIndex = 0;
		int indicesRemaining = triangleCount * 3;
		int indicesInBatch = Min(batchSize, indicesRemaining);

		do 
		{
			PF_START(MovieDraw_Stall);
			void* destVertexBuffer = GRCDEVICE.BeginVertices(primType, indicesInBatch, stride);
			PF_STOP(MovieDraw_Stall);
			if (destVertexBuffer)
			{
				char* currVert = reinterpret_cast<char*>(destVertexBuffer);
				const char* sourceVerts = reinterpret_cast<const char*>(m_CurrentVertices);
				const u16* indices = reinterpret_cast<const u16*>(m_CurrentIndices) + batchStartIndex;

#define INNER_LOOP(size) \
				{																	\
					struct Vert														\
					{																\
						u32 m_Data[size/4];											\
					};																\
					for(int i = startIndex; i < indicesInBatch + startIndex; i++)	\
					{																\
						int index = indices[i] + baseVertexIndex;					\
						*(Vert*)currVert = *((Vert*)(sourceVerts) + index);			\
						currVert += size;											\
					}																\
				}																	\
				//END

				switch(stride)
				{
				case 4:		INNER_LOOP(4); break;
				case 8:		INNER_LOOP(8); break;
				case 12:	INNER_LOOP(12); break;
				case 16:	INNER_LOOP(16); break;
				case 20:	INNER_LOOP(20); break;
				case 24:	INNER_LOOP(24); break;
				case 32:	INNER_LOOP(32); break;
				default:	
					for(int i = startIndex; i < indicesInBatch + startIndex; i++)
					{												
						int index = indices[i] + baseVertexIndex;		
						memcpy(currVert, sourceVerts + index * stride, stride);
						currVert += stride;									
					}													
				}

#undef INNER_LOOP

				GRCDEVICE.EndVertices(currVert);
#ifndef GFC_NO_STAT
				m_RenderStats.Primitives++;
#endif
			}

			batchStartIndex += indicesInBatch;
			indicesRemaining -= indicesInBatch;
			indicesInBatch = Min(batchSize, indicesRemaining);
		} while (indicesRemaining > 0);
#endif
	}

	g_Effect->UnBind();
}

void sfRendererSingleThreaded::DrawLineStrip( int baseVertexIndex, int lineCount )
{
	g_Effect->Bind(g_Techniques[m_Technique]);
	int stride = g_ScaleformVertexDecls[m_CurrentVertexFormat]->Stream0Size;

#ifndef GFC_NO_STAT
	m_RenderStats.Primitives++;
	m_RenderStats.Lines += lineCount;
#endif

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		sfAssertf((lineCount + 1) < USHRT_MAX, "Too many verts for one draw call: %d", lineCount + 1);
		sfAssertf(baseVertexIndex < USHRT_MAX, "Starting index was too high %d", baseVertexIndex);
		m_CommandBuffer->Add_DrawLineStrip((sfTechniques)m_Technique, m_CurrVBIndex, (u16)(lineCount + 1), (u16)baseVertexIndex);
	}
#endif

	GRCDEVICE.SetVertexDeclaration(g_ScaleformVertexDecls[m_CurrentVertexFormat]);
	const char* sourceVerts = reinterpret_cast<const char*>(m_CurrentVertices);
	PF_START(MovieDraw_Stall);
	void* destVertexBuffer = GRCDEVICE.BeginVertices(drawLineStrip, lineCount + 1, stride);
	PF_STOP(MovieDraw_Stall);
	if (destVertexBuffer)
	{
		size_t size = (lineCount+1) * stride;
		sysMemCpyStreaming(destVertexBuffer, sourceVerts + (baseVertexIndex * stride), size);
		GRCDEVICE.EndVertices(((char *) destVertexBuffer) + size);
	}

	g_Effect->UnBind();
}

void sfRendererSingleThreaded::LineStyleDisable()
{
	LineStyleColor(GColor(0, 0, 0, 0));
}

void sfRendererSingleThreaded::LineStyleColor( GColor color )
{
	m_Technique = sfTechSolidFill;
	Vector4 colorVec;
	color.GetRGBAFloat(&colorVec.x, &colorVec.y, &colorVec.z, &colorVec.w);
	g_Effect->SetVar(g_EffectVars[sfVarColor], &colorVec, 1);
#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		m_CommandBuffer->Add_Comment("LineStyleColor");
		m_CommandBuffer->Add_SetVar(sfVarColor, colorVec);
	}
#endif
}

void sfRendererSingleThreaded::FillStyleDisable()
{
	FillStyleColor(GColor(0, 0, 0, 0));
}

void sfRendererSingleThreaded::FillStyleColor( GColor color )
{
	m_Technique = sfTechSolidFill;
	Vector4 colorVec;
	color.GetRGBAFloat(&colorVec.x, &colorVec.y, &colorVec.z, &colorVec.w);
	g_Effect->SetVar(g_EffectVars[sfVarColor], &colorVec, 1);
#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		m_CommandBuffer->Add_Comment("FillStyleColor");
		m_CommandBuffer->Add_SetVar(sfVarColor, colorVec);
	}
#endif
}

sfTexture* sfRendererSingleThreaded::GetSfTexture(const GRenderer::FillTexture* fill)
{
	if (!fill)
	{
		return NULL;
	}
	sfTexture* tex = smart_cast<sfTexture*>(fill->pTexture);
	return tex;
}

void sfRendererSingleThreaded::FillStyleBitmap( const GRenderer::FillTexture* fill )
{
	if (!fill || !fill->pTexture)
	{
		FillStyleDisable();
		return;
	}

	SetTexture(0, fill);
	m_Technique = sfTechTextureFill;
}

void sfRendererSingleThreaded::SetTexture(u32 texIndex, const GRenderer::FillTexture* fill)
{
	sfAssertf(texIndex <= 2, "texAddr must be 0, 1 or 2");
	if (!fill)
	{
		sfErrorf("Filling with empty texture");
		g_Effect->SetVar(g_EffectVars[sfVarTexture0 + texIndex], grcTexture::None);
		return;
	}

	GRenderer::Matrix texMtx = fill->TextureMatrix;

	sfTexture* tex = GetSfTexture(fill);

#if SF_RECORD_COMMAND_BUFFER
	SetShaderMatrix((sfEffectVars)(sfVarTex0Mtx + texIndex), texMtx, IsRecording() ? m_CommandBuffer : NULL);
#else
	SetShaderMatrix((sfEffectVars)(sfVarTex0Mtx + texIndex), texMtx);
#endif
#if RSG_PC
	tex->GetGrcTexture()->UpdateGPUCopy();
#endif // RSG_PC

	g_Effect->SetVar(g_EffectVars[sfVarTexture0 + texIndex], tex->GetGrcTexture());

	// There are actually only four combinations of sampler states above.
	g_Effect->SetSamplerState(g_EffectVars[sfVarTexture0 + texIndex], g_SamplerStateHandles[(fill->WrapMode == GRenderer::Wrap_Repeat? SCALEFORM_REPEAT : SCALEFORM_CLAMP) | (fill->SampleMode == GRenderer::Sample_Point? SCALEFORM_POINT : SCALEFORM_LINEAR)]);

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		sfWarningf("Can't record textures yet");
	}
#endif
}

void sfRendererSingleThreaded::FillStyleGouraud( GRenderer::GouraudFillType fillType, const GRenderer::FillTexture* texture0 /*= NULL*/, const GRenderer::FillTexture* texture1 /*= NULL*/, const GRenderer::FillTexture* /*texture2*/ /*= NULL*/ )
{
	switch(fillType)
	{
	case GRenderer::GFill_Color:
		m_Technique = sfTechGFill_Color;
		break;
	case GRenderer::GFill_1Texture:
		m_Technique = sfTechGFill_1Texture;
		SetTexture(0, texture0);
		break;
	case GRenderer::GFill_1TextureColor:
		m_Technique = sfTechGFill_1TextureColor;
		SetTexture(0, texture0);
		break;
	case GRenderer::GFill_2Texture:
	case GRenderer::GFill_2TextureColor:
	case GRenderer::GFill_3Texture:
		m_Technique = sfTechGFill_2Texture;
		SetTexture(0, texture0);
		SetTexture(1, texture1);
		break;
	}
}

void sfRendererSingleThreaded::DrawBitmaps( GRenderer::BitmapDesc* bitmapList, int /*listSize*/, int startIndex, int count, const GTexture* texture, const GRenderer::Matrix& m, GRenderer::CacheProvider* /*cache*/ )
{
#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		sfWarningf("Can't record bitmap drawing yet");
	}
#endif

	SetMatrix(m);
	if (texture)
	{
		sfTexture* sftex = static_cast<sfTexture*>(const_cast<GTexture*>(texture));
		g_Effect->SetVar(g_EffectVars[sfVarTexture0], sftex->GetGrcTexture());
#if RSG_PC
		sftex->GetGrcTexture()->UpdateGPUCopy();
#endif // RSG_PC

		// SetTexture may have messed with the filter state, so reset it here.
		// Really would rather not have to set this much state every time we draw bitmaps - maybe cache the texture ptr (or sampler state) and
		// don't reset state unless the texture ptr changes or someone called SetTexture
		g_Effect->SetSamplerState(g_EffectVars[sfVarTexture0], g_SamplerStateHandles[SCALEFORM_CLAMP | SCALEFORM_LINEAR]);

		grcTexture::ChannelBits channels = sftex->GetGrcTexture()->FindUsedChannels();
		if (channels.IsSet(grcTexture::CHANNEL_RED) ||
			channels.IsSet(grcTexture::CHANNEL_GREEN) ||
			channels.IsSet(grcTexture::CHANNEL_BLUE))
		{
			g_Effect->Bind(g_Techniques[sfTechColorSprite]);
		}
		else
		{
			sfAssertf(channels.IsSet(grcTexture::CHANNEL_ALPHA), "Texture must have RGB or A channels");
			g_Effect->Bind(g_Techniques[sfTechAlphaSprite]);
		}

	}

	GRCDEVICE.SetVertexDeclaration(g_ScaleformVertexDecls[sfRendererBase::Vertex_XYUV32fC32]);

#if __XENON || __PS3 || RSG_ORBIS
	const int vertsPerPrim = 4;
	const grcDrawMode drawMode = drawQuads;
#else
	const int vertsPerPrim = 6;
	const grcDrawMode drawMode = drawTris;
#endif

	int stride = g_ScaleformVertexDecls[sfRendererBase::Vertex_XYUV32fC32]->Stream0Size;
	int batchSize = (GPU_VERTICES_MAX_SIZE / stride); // Max # of bytes

	batchSize = (batchSize / vertsPerPrim); // round down to mult. of prim size.
	int batchStartIndex = 0;
	int primsRemaining = count;
	int primsInBatch = Min(batchSize, primsRemaining);

	do 
	{
		PF_START(MovieDraw_Stall);
		XYUV32fC32* __restrict vbuf = reinterpret_cast<XYUV32fC32*>(GRCDEVICE.BeginVertices(drawMode, primsInBatch * vertsPerPrim, sizeof(XYUV32fC32)));
		PF_STOP(MovieDraw_Stall);
		if (vbuf)
		{
			for(int i = 0; i < primsInBatch; i++)
			{
				GRenderer::BitmapDesc& quad = bitmapList[batchStartIndex + startIndex + i];
				Color32 color;
				color.SetBytes(quad.Color.GetRed(), quad.Color.GetGreen(), quad.Color.GetBlue(), quad.Color.GetAlpha());

				vbuf[0].Set(quad.Coords.X1(), quad.Coords.Y1(), quad.TextureCoords.X1(), quad.TextureCoords.Y1(), color);
				vbuf[1].Set(quad.Coords.X1(), quad.Coords.Y2(), quad.TextureCoords.X1(), quad.TextureCoords.Y2(), color);
				vbuf[2].Set(quad.Coords.X2(), quad.Coords.Y2(), quad.TextureCoords.X2(), quad.TextureCoords.Y2(), color);
#if __XENON || __PS3 || RSG_ORBIS
				vbuf[3].Set(quad.Coords.X2(), quad.Coords.Y1(), quad.TextureCoords.X2(), quad.TextureCoords.Y1(), color);
				vbuf += 4;
#else
				vbuf[3].Set(quad.Coords.X2(), quad.Coords.Y2(), quad.TextureCoords.X2(), quad.TextureCoords.Y2(), color);
				vbuf[4].Set(quad.Coords.X2(), quad.Coords.Y1(), quad.TextureCoords.X2(), quad.TextureCoords.Y1(), color);
				vbuf[5].Set(quad.Coords.X1(), quad.Coords.Y1(), quad.TextureCoords.X1(), quad.TextureCoords.Y1(), color);
				vbuf += 6;
#endif
			}
			GRCDEVICE.EndVertices(vbuf);
#ifndef GFC_NO_STAT
			m_RenderStats.Triangles += primsInBatch * (drawMode == drawQuads ? 2 : 1);
			m_RenderStats.Primitives++;
#endif
		}
		batchStartIndex += primsInBatch;
		primsRemaining -= primsInBatch;
		primsInBatch = Min(batchSize, primsRemaining);
	} while(primsRemaining > 0);

	g_Effect->UnBind();
}

void sfRendererSingleThreaded::BeginSubmitMask( GRenderer::SubmitMaskMode maskMode /*= Mask_Clear*/ )
{  

	// ignore masks when rendering in the world, otherwise the stencil buffer will get trashed
	if (m_RenderInWorld)
	{
		return;
	}

#ifndef GFC_NO_STAT
	m_RenderStats.Masks++;
#endif

#if USE_SCALEFORM_AMP
	if (m_UseWireframe)
	{		  
		m_JustClearedStencil = false;
		return;
	}
#endif

	sfDepthStencilStateBlocks depthStencilState = SF_DSS_STENCIL_INCREMENT;
	// TODO:
	// Dr. Pix says: 
	// 1	If SetPixelShader(NULL) were called everywhere possible, rendering of entire scene would be 5.48% faster.
	// But as far as I know we can't just unbind the pixel shader, so would need a new set of techniques or something
	switch(maskMode)
	{
	case GRenderer::Mask_Clear:
		if (!m_JustClearedStencil && m_DisableClear == false)
		{
#if __PS3
			// Yes, it's that dumb.
			if( true == GRCDEVICE.HasADepthTarget() )
#endif					
			{
				GRCDEVICE.Clear(false, Color32(0xFFFFFFFF), false, 0.0f, true, 0);
			}
		}
		depthStencilState = SF_DSS_STENCIL_INCREMENT;
		m_StencilValue = 1;
		break;
	case GRenderer::Mask_Increment:
		depthStencilState = SF_DSS_STENCIL_INCREMENT;
		m_StencilValue++;
		break;
	case GRenderer::Mask_Decrement:
		depthStencilState = SF_DSS_STENCIL_DECREMENT;
		m_StencilValue--;
		break;
	}

	if( !m_DepthStencilStateOverriden )
		grcStateBlock::SetDepthStencilState(g_DepthStencilStateHandles[depthStencilState], m_StencilValue);
	
	if( !m_BlendStateOverridden )
		grcStateBlock::SetBlendState(g_BlendStateHandles[SF_BS_STENCIL]);

	m_JustClearedStencil = false;

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		sfWarningf("Can't use stencils in prerecorded command buffers");
	}
#endif
}

void sfRendererSingleThreaded::EndSubmitMask()
{
	// ignore masks when rendering in the world, otherwise the stencil buffer will get trashed
	if (m_RenderInWorld)
	{
		return;
	}

#if USE_SCALEFORM_AMP
	if (m_UseWireframe)
	{
		return;
	}
#endif

	UpdateBlendMode(m_CurrentBlendMode, true); // reapply whatever blend mode we were in before stenciling

	if( !m_DepthStencilStateOverriden )
		grcStateBlock::SetDepthStencilState(g_DepthStencilStateHandles[ m_RenderInWorld ? SF_DSS_3D_DRAWING_WITH_STENCIL : SF_DSS_2D_DRAWING_WITH_STENCIL], m_StencilValue);

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		sfWarningf("Can't use stencils in prerecorded command buffers");
	}
#endif
}

void sfRendererSingleThreaded::DisableMask()
{
#if USE_SCALEFORM_AMP
	if (m_UseWireframe)
	{
		return;
	}
#endif

	if( !m_DepthStencilStateOverriden )
		grcStateBlock::SetDepthStencilState(g_DepthStencilStateHandles[ m_RenderInWorld ? SF_DSS_3D_DRAWING : SF_DSS_2D_DRAWING], m_StencilValue);

#if SF_RECORD_COMMAND_BUFFER
	if (IsRecording())
	{
		sfWarningf("Can't use stencils in prerecorded command buffers");
	}
#endif
}

void sfRendererSingleThreaded::GetRenderStats( GRenderer::Stats* stats, bool resetStats /*= false*/ )
{
	if (stats)
	{
		sysMemCpy(stats, &m_RenderStats, sizeof(*stats));
	}

	if (resetStats)
	{
		m_RenderStats.Clear();
	}
}

void sfRendererSingleThreaded::GetStats( GStatBag* /*bag*/, bool /*reset*/ /*= true*/ )
{
	sfErrorf("GetStats unimplemented");
}

void sfRendererSingleThreaded::ReleaseResources()
{
	sfErrorf("ReleaseResources unimplemented");
}



//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// sfRenderer implementation
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////



sfRenderer::sfRenderer()
: m_UseDeletionQueue(true)
#if __ASSERT
, m_BeginFrameCalled(false)
#endif
#if __BANK
, m_FontCacheTexture(NULL)
#endif
{
	CreateVertexDeclarations();

	sfAssertf(!g_Effect, "Effects have already been initialized - are you making a 2nd instance of this renderer?");
	if (!g_Effect)
	{
		CreateEffect();
	}

	// Initialize all of our state blocks

	using namespace grcRSV;

	if (PARAM_scaleform_alwayssmooth.Get())
	{
		sfRendererBase::sm_AlwaysSmooth = true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Sampler States
	grcSamplerStateDesc desc;	// wrap / linear is default.
	desc.Filter = grcSSV::FILTER_MIN_MAG_MIP_LINEAR;
	desc.AddressU = desc.AddressV = grcSSV::TADDRESS_WRAP;
	g_SamplerStateHandles[SCALEFORM_REPEAT | SCALEFORM_LINEAR] = grcStateBlock::CreateSamplerState(desc);

	desc.Filter = sfRendererBase::sm_AlwaysSmooth ? grcSSV::FILTER_MIN_MAG_MIP_LINEAR : grcSSV::FILTER_MIN_MAG_MIP_POINT;
	desc.AddressU = desc.AddressV = grcSSV::TADDRESS_WRAP;
	g_SamplerStateHandles[SCALEFORM_REPEAT | SCALEFORM_POINT] = grcStateBlock::CreateSamplerState(desc);

	desc.Filter = sfRendererBase::sm_AlwaysSmooth ? grcSSV::FILTER_MIN_MAG_MIP_LINEAR : grcSSV::FILTER_MIN_MAG_MIP_POINT;
	desc.AddressU = desc.AddressV = grcSSV::TADDRESS_CLAMP;
	g_SamplerStateHandles[SCALEFORM_CLAMP | SCALEFORM_POINT] = grcStateBlock::CreateSamplerState(desc);

	desc.Filter = grcSSV::FILTER_MIN_MAG_MIP_LINEAR;
	desc.AddressU = desc.AddressV = grcSSV::TADDRESS_CLAMP;
	g_SamplerStateHandles[SCALEFORM_CLAMP | SCALEFORM_LINEAR] = grcStateBlock::CreateSamplerState(desc);

	//////////////////////////////////////////////////////////////////////////
	// Blend States
	grcBlendStateDesc blendDesc;
	CreateBlendStateDesc(BLEND_SRCALPHA, BLENDOP_ADD, BLEND_INVSRCALPHA, BLEND_SRCALPHA, BLENDOP_ADD, BLEND_INVSRCALPHA, blendDesc);
	g_BlendStateHandles[SF_BS_NORMAL] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_DESTCOLOR, BLENDOP_ADD, BLEND_INVSRCALPHA, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_MULTIPLY] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_INVDESTCOLOR, BLENDOP_ADD, BLEND_ONE, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_SCREEN] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_ONE, BLENDOP_MAX, BLEND_ONE, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_LIGHTEN] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_ONE, BLENDOP_MIN, BLEND_ONE, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_DARKEN] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_SRCALPHA, BLENDOP_ADD, BLEND_ONE, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_ADD] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_SRCALPHA, BLENDOP_REVSUBTRACT, BLEND_ONE, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_SUBTRACT] = grcStateBlock::CreateBlendState(blendDesc);
	
	CreateBlendStateDesc(BLEND_INVDESTCOLOR, BLENDOP_ADD, BLEND_INVSRCCOLOR, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_INVERT] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_ZERO, BLENDOP_ADD, BLEND_ONE, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	g_BlendStateHandles[SF_BS_ALPHA] = grcStateBlock::CreateBlendState(blendDesc);

	CreateBlendStateDesc(BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, BLEND_ONE, BLENDOP_ADD, BLEND_ZERO, blendDesc);
	blendDesc.BlendRTDesc[0].RenderTargetWriteMask = COLORWRITEENABLE_NONE;
	g_BlendStateHandles[SF_BS_STENCIL] = grcStateBlock::CreateBlendState(blendDesc);

	//////////////////////////////////////////////////////////////////////////
	// Rasterizer States
	grcRasterizerStateDesc rasterDesc2d;
	rasterDesc2d.ScissorEnable = true;
	rasterDesc2d.CullMode = CULL_NONE;
	rasterDesc2d.HalfPixelOffset = true;
	g_RasterizerStateHandles[SF_RS_2D_DRAWING] = grcStateBlock::CreateRasterizerState(rasterDesc2d);

	grcRasterizerStateDesc rasterDesc3d;
	// rasterDesc3d.ScissorEnable = false;
	rasterDesc3d.CullMode = CULL_NONE;
	rasterDesc3d.HalfPixelOffset = true;
	g_RasterizerStateHandles[SF_RS_3D_DRAWING] = grcStateBlock::CreateRasterizerState(rasterDesc3d);


	//////////////////////////////////////////////////////////////////////////
	// DepthStencil States
	grcDepthStencilStateDesc dssDesc2d;
	dssDesc2d.DepthEnable = false;
	dssDesc2d.DepthWriteMask = 0x0;
	dssDesc2d.DepthFunc = grcRSV::CMP_LESS;
	g_DepthStencilStateHandles[SF_DSS_2D_DRAWING] = grcStateBlock::CreateDepthStencilState(dssDesc2d);

	grcDepthStencilStateDesc dssDesc3d;
	dssDesc3d.DepthEnable = true;
	dssDesc3d.DepthWriteMask = 0x0;
	dssDesc3d.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	g_DepthStencilStateHandles[SF_DSS_3D_DRAWING] = grcStateBlock::CreateDepthStencilState(dssDesc3d);

	grcDepthStencilStateDesc dssDesc2dSten;
	dssDesc2dSten.DepthEnable = false;
	dssDesc2dSten.DepthWriteMask = 0x0;
	dssDesc2dSten.DepthFunc = grcRSV::CMP_LESS;
	dssDesc2dSten.StencilEnable = true;
	dssDesc2dSten.FrontFace.StencilFunc = CMP_EQUAL;	
	dssDesc2dSten.FrontFace.StencilPassOp = STENCILOP_KEEP;
	dssDesc2dSten.BackFace.StencilFunc = CMP_EQUAL;	
	dssDesc2dSten.BackFace.StencilPassOp = STENCILOP_KEEP;
	g_DepthStencilStateHandles[SF_DSS_2D_DRAWING_WITH_STENCIL] = grcStateBlock::CreateDepthStencilState(dssDesc2dSten);

	grcDepthStencilStateDesc dssDesc3dSten;
	dssDesc3dSten.DepthEnable = true;
	dssDesc3dSten.DepthWriteMask = 0x0;
	dssDesc3dSten.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESS);
	dssDesc3dSten.StencilEnable = true;
	dssDesc3dSten.FrontFace.StencilFunc = CMP_EQUAL;	
	dssDesc3dSten.FrontFace.StencilPassOp = STENCILOP_KEEP;
	dssDesc3dSten.BackFace.StencilFunc = CMP_EQUAL;	
	dssDesc3dSten.BackFace.StencilPassOp = STENCILOP_KEEP;
	g_DepthStencilStateHandles[SF_DSS_3D_DRAWING_WITH_STENCIL] = grcStateBlock::CreateDepthStencilState(dssDesc3dSten);

	grcDepthStencilStateDesc dssDescClear;
	dssDescClear.DepthEnable = false;
	dssDescClear.DepthWriteMask = 0x0;
	dssDescClear.DepthFunc = grcRSV::CMP_LESS;
	dssDescClear.FrontFace.StencilFunc = CMP_ALWAYS;		
	dssDescClear.FrontFace.StencilPassOp = STENCILOP_ZERO;
	dssDescClear.BackFace.StencilFunc = CMP_ALWAYS;		
	dssDescClear.BackFace.StencilPassOp = STENCILOP_ZERO;
	g_DepthStencilStateHandles[SF_DSS_BACKGROUND_CLEAR] = grcStateBlock::CreateDepthStencilState(dssDescClear);

	grcDepthStencilStateDesc dssDescStencilIncr;
	dssDescStencilIncr.DepthEnable = false;
	dssDescStencilIncr.DepthWriteMask = 0x0;
	dssDescStencilIncr.DepthFunc = grcRSV::CMP_LESS;
	dssDescStencilIncr.StencilEnable = true;
	dssDescStencilIncr.FrontFace.StencilFunc = CMP_ALWAYS;		
	dssDescStencilIncr.FrontFace.StencilPassOp = STENCILOP_INCRSAT;
	dssDescStencilIncr.BackFace.StencilFunc = CMP_ALWAYS;		
	dssDescStencilIncr.BackFace.StencilPassOp = STENCILOP_INCRSAT;
	g_DepthStencilStateHandles[SF_DSS_STENCIL_INCREMENT] = grcStateBlock::CreateDepthStencilState(dssDescStencilIncr);

	grcDepthStencilStateDesc dssDescStencilDecr;
	dssDescStencilDecr.DepthEnable = false;
	dssDescStencilDecr.DepthWriteMask = 0x0;
	dssDescStencilDecr.DepthFunc = grcRSV::CMP_LESS;
	dssDescStencilDecr.StencilEnable = true;
	dssDescStencilDecr.FrontFace.StencilFunc = CMP_ALWAYS;		
	dssDescStencilDecr.FrontFace.StencilPassOp = STENCILOP_DECRSAT;
	dssDescStencilDecr.BackFace.StencilFunc = CMP_ALWAYS;		
	dssDescStencilDecr.BackFace.StencilPassOp = STENCILOP_DECRSAT;
	g_DepthStencilStateHandles[SF_DSS_STENCIL_DECREMENT] = grcStateBlock::CreateDepthStencilState(dssDescStencilDecr);
}

sfRenderer::~sfRenderer()
{
	FlushDeletionQueue();

	if (sfGlobals::g_Effect)
	{
		DestroyEffect();
	}

	DestroyVertexDeclarations();
}

bool sfRenderer::GetRenderCaps(RenderCaps *pcaps)
{
	sfAssertf(pcaps, "No caps structure to fill out");
	pcaps->CapBits =
		//GRenderer::Cap_CacheDataUse | // No caching (yet)
		GRenderer::Cap_Index16 |
		//GRenderer::Cap_Index32 | // Our grcIndexBuffers assume 16 bit
		GRenderer::Cap_RenderStats |
		GRenderer::Cap_FillGouraud |
		GRenderer::Cap_FillGouraudTex |
		GRenderer::Cap_CxformAdd |
		GRenderer::Cap_NestedMasks |

		// TODO: Decide which of this to turn back on?
		//GRenderer::Cap_TexNonPower2 |
		//GRenderer::Cap_TexNonPower2Mip |
		//GRenderer::Cap_TexNonPower2Wrap |

#if __WIN32PC
		GRenderer::Cap_CanLoseData | // Consoles don't lose data - need to fix this for PC though
#endif
		//GRenderer::Cap_KeepVertexData | // Do what you want with vertex data, we copy it up to the command buffer right away
		GRenderer::Cap_NoTexOverwrite | // Probably should leave this on until we figure out how textures get updated and if they need resolve
		//GRenderer::Cap_ThreadedTextureCreation | // Again leaving turned off just in case.

		// TODO: Support these
		//GRenderer::Cap_RenderTargets |
		//GRenderer::Cap_RenderTargetPrePass |
		//GRenderer::Cap_RenderTargetNonPow2 |
		//GRenderer::Cap_RenderTargetMip |

		// TODO: Support these
		//GRenderer::Cap_Filter_Blurs |
		//GRenderer::Cap_Filter_ColorMatrix |
		0;

	// Lets say these all work for now
	pcaps->VertexFormats =
		(1 << GRenderer::Vertex_XY16i) |
		(1 << GRenderer::Vertex_XY32f) |
		(1 << GRenderer::Vertex_XY16iC32) |
		(1 << GRenderer::Vertex_XY16iCF32) |
		0;

	// These are the ones that have been tested and work for the most part.
	// Missing ones would require either offscreen rendering or a more flexible combiner
	pcaps->BlendModes =
		(1 << GRenderer::Blend_None) |
		(1 << GRenderer::Blend_Normal ) |
		//(1 << GRenderer::Blend_Layer ) |
		(1 << GRenderer::Blend_Multiply ) |
		(1 << GRenderer::Blend_Screen ) |
		(1 << GRenderer::Blend_Lighten ) |
		(1 << GRenderer::Blend_Darken ) |
		//(1 << GRenderer::Blend_Difference ) |
		(1 << GRenderer::Blend_Add ) |
		(1 << GRenderer::Blend_Subtract ) |
		(1 << GRenderer::Blend_Invert ) |
		//(1 << GRenderer::Blend_Alpha ) |
		//(1 << GRenderer::Blend_Erase ) |
		//(1 << GRenderer::Blend_Overlay  ) |
		//(1 << GRenderer::Blend_HardLight  ) |
		0;

	pcaps->MaxTextureSize = 4096; // why not.

	return true;
}

sfTextureBase* sfRenderer::CreateTexture()
{
	sfTexture* tex = rage_new sfTexture(this);
	return tex;
}

sfTextureBase* sfRenderer::CreateTextureYUV()
{
	sfErrorf("CreateTextureYUV unimplemented");
	return NULL;
}

void sfRenderer::BeginFrame()
{
#if __ASSERT
	sfAssertf(m_BeginFrameCalled == false, "BeginFrame() called twice in a row");
	m_BeginFrameCalled = true;
#endif

	FlushDeletionQueue();

	GRenderer::BeginFrame();
}

void sfRenderer::EndFrame()
{
	GRenderer::EndFrame();

#if __ASSERT
	sfAssertf(m_BeginFrameCalled == true, "sfRenderer::EndFrame() called without matching EndFrame"); 
	m_BeginFrameCalled = false;
#endif
}

void sfRenderer::FlushDeletionQueue()
{
	// Delete any textures we had scheduled for deletion last frame.
	for(int i = 0; i < m_TextureDeletionStack.GetCount(); i++)
	{
		DeleteTexture(m_TextureDeletionStack[i]);
	}
	m_TextureDeletionStack.SetCount(0);

}

GRenderTarget* sfRenderer::CreateRenderTarget()
{
	return NULL;
}

void sfRenderer::SetDisplayRenderTarget(GRenderTarget* prt, bool setstate)
{
	m_st[g_RenderThreadIndex].SetDisplayRenderTarget(prt, setstate);
}

void sfRenderer::PushRenderTarget(const GRectF& frameRect, GRenderTarget* prt)
{
	m_st[g_RenderThreadIndex].PushRenderTarget(frameRect, prt);
}

void sfRenderer::PopRenderTarget()
{
	m_st[g_RenderThreadIndex].PopRenderTarget();
}

GTexture* sfRenderer::PushTempRenderTarget(const GRectF& frameRect, UInt targetW, UInt targetH, bool wantStencil)
{
	return m_st[g_RenderThreadIndex].PushTempRenderTarget(frameRect, targetW, targetH, wantStencil);
}

void sfRenderer::ReleaseTempRenderTargets(UInt keepArea)
{
	GUNUSED(keepArea);
}

void sfRenderer::BeginDisplay(GColor backgroundColor, const GViewport& viewport, Float x0, Float x1, Float y0, Float y1)
{
	sfAssertf(m_BeginFrameCalled == true, "sfRenderer::BeginFrame() must be called before any rendering is done this frame");
	m_st[g_RenderThreadIndex].BeginDisplay(backgroundColor, viewport, x0, x1, y0, y1);
}

void sfRenderer::EndDisplay()
{
	m_st[g_RenderThreadIndex].EndDisplay();
}

#if RSG_PC
void sfRenderer::SetStereoFlag(float flag)
{
	m_st[g_RenderThreadIndex].SetStereoFlag(flag);
}
#endif

void sfRenderer::SetMatrix(const Matrix& m)
{
	m_st[g_RenderThreadIndex].SetMatrix(m);
}

void sfRenderer::SetUserMatrix(const Matrix& m)
{
	m_st[g_RenderThreadIndex].SetUserMatrix(m);
}

void sfRenderer::SetCxform(const Cxform& cx)
{
	m_st[g_RenderThreadIndex].SetCxform(cx);
}

void sfRenderer::PushBlendMode(BlendType mode)
{
	m_st[g_RenderThreadIndex].PushBlendMode(mode);
}

void sfRenderer::PopBlendMode()
{
	m_st[g_RenderThreadIndex].PopBlendMode();
}

bool sfRenderer::PushUserData(UserData* pdata)
{
	return m_st[g_RenderThreadIndex].PushUserData(pdata);
}

void sfRenderer::PopUserData()
{
	m_st[g_RenderThreadIndex].PopUserData();
}

#ifndef GFC_NO_3D
    void sfRenderer::SetPerspective3D(const GMatrix3D &projMatIn)
	{
		m_st[g_RenderThreadIndex].SetPerspective3D(projMatIn);
	}

    void sfRenderer::SetView3D(const GMatrix3D &viewMatIn)
	{
		m_st[g_RenderThreadIndex].SetView3D(viewMatIn);
	}

    void sfRenderer::SetWorld3D(const GMatrix3D *pWorldMatIn)
	{
		m_st[g_RenderThreadIndex].SetWorld3D(pWorldMatIn);
	}
#endif

void sfRenderer::SetVertexData(const void* pvertices, int numVertices, VertexFormat vf, CacheProvider *pcache)
{
	m_st[g_RenderThreadIndex].SetVertexData(pvertices, numVertices, vf, pcache);
}

void sfRenderer::SetIndexData(const void* pindices, int numIndices, IndexFormat idxf, CacheProvider *pcache)
{
	m_st[g_RenderThreadIndex].SetIndexData(pindices, numIndices, idxf, pcache);
}

void sfRenderer::ReleaseCachedData(CachedData* /*pdata*/, CachedDataType /*type*/)
{
	sfErrorf("ReleaseCachedData unimplemented");
}

void sfRenderer::DrawIndexedTriList(int baseVertexIndex, int minVertexIndex, int numVertices, int startIndex, int triangleCount)
{
	m_st[g_RenderThreadIndex].DrawIndexedTriList(baseVertexIndex, minVertexIndex, numVertices, startIndex, triangleCount);
}

void sfRenderer::DrawLineStrip(int baseVertexIndex, int lineCount)
{
	m_st[g_RenderThreadIndex].DrawLineStrip(baseVertexIndex, lineCount);
}

void sfRenderer::LineStyleDisable()
{
	m_st[g_RenderThreadIndex].LineStyleDisable();
}

void sfRenderer::LineStyleColor(GColor color)
{
	m_st[g_RenderThreadIndex].LineStyleColor(color);
}

void sfRenderer::FillStyleDisable()
{
	m_st[g_RenderThreadIndex].FillStyleDisable();
}

void sfRenderer::FillStyleColor(GColor color)
{
	m_st[g_RenderThreadIndex].FillStyleColor(color);
}

void sfRenderer::FillStyleBitmap(const FillTexture* pfill)
{
	m_st[g_RenderThreadIndex].FillStyleBitmap(pfill);
}

void sfRenderer::FillStyleGouraud(GouraudFillType fillType, const FillTexture *ptexture0, const FillTexture *ptexture1, const FillTexture *ptexture2)
{
	m_st[g_RenderThreadIndex].FillStyleGouraud(fillType, ptexture0, ptexture1, ptexture2);
}

void sfRenderer::DrawBitmaps(BitmapDesc* pbitmapList, int listSize, int startIndex, int count, const GTexture* pti, const Matrix& m, CacheProvider *pcache )
{
	m_st[g_RenderThreadIndex].DrawBitmaps(pbitmapList, listSize, startIndex, count, pti, m, pcache);
}

void sfRenderer::BeginSubmitMask(SubmitMaskMode maskMode)
{
	m_st[g_RenderThreadIndex].BeginSubmitMask(maskMode);
}

void sfRenderer::EndSubmitMask()
{
	m_st[g_RenderThreadIndex].EndSubmitMask();
}

void sfRenderer::DisableMask()
{
	m_st[g_RenderThreadIndex].DisableMask();
}

UInt sfRenderer::CheckFilterSupport(const BlurFilterParams& /*params*/)
{
	return 0;
}

void sfRenderer::DrawBlurRect(GTexture* psrcin, const GRectF& insrcrect, const GRectF& indestrect, const BlurFilterParams& params, bool islast)
{
	m_st[g_RenderThreadIndex].DrawBlurRect(psrcin, insrcrect, indestrect, params, islast);
}

void sfRenderer::DrawColorMatrixRect(GTexture* psrcin, const GRectF& insrcrect, const GRectF& destrect, const Float *matrix, bool islast)
{
	m_st[g_RenderThreadIndex].DrawColorMatrixRect(psrcin, insrcrect, destrect, matrix, islast);
}

void sfRenderer::GetRenderStats(Stats *pstats, bool resetStats)
{
# if MULTIPLE_RENDER_THREADS
	if (!g_IsSubRenderThread)
	{
		if (pstats)
		{
			pstats->Clear();
		}
		for (unsigned i=0; i<NELEM(m_st); ++i)
		{
			Stats tmp;
			m_st[i].GetRenderStats(&tmp, resetStats);
			if (pstats)
			{
				pstats->Triangles  += tmp.Triangles;
				pstats->Lines      += tmp.Lines;
				pstats->Primitives += tmp.Primitives;
				pstats->Masks      += tmp.Masks;
				pstats->Filters    += tmp.Filters;
			}
		}
	}
	else
# endif
	{
		m_st[g_RenderThreadIndex].GetRenderStats(pstats, resetStats);
	}
}

void sfRenderer::GetStats(GStatBag* pbag, bool reset)
{
	m_st[g_RenderThreadIndex].GetStats(pbag, reset);
}

void sfRenderer::ReleaseResources()
{
	for (unsigned i=0; i<NELEM(m_st); ++i)
	{
		m_st[i].ReleaseResources();
	}
}

void sfRenderer::RenderNextMovieInWorldSpace()
{
	m_st[g_RenderThreadIndex].RenderNextMovieInWorldSpace();
}

#if USE_SCALEFORM_AMP

void sfRenderer::SetWireframeMode(bool useWires)
{
	m_st[g_RenderThreadIndex].SetWireframeMode(useWires);
}

bool sfRenderer::GetWireframeMode()
{
	return m_st[g_RenderThreadIndex].GetWireframeMode();
}

#endif // USE_SCALEFORM_AMP

void sfRenderer::ForceNormalBlendState(bool forceNormalBlendState)
{
	m_st[g_RenderThreadIndex].ForceNormalBlendState(forceNormalBlendState);
}

void sfRenderer::OverrideBlendState(bool overrideBlendState)
{
	m_st[g_RenderThreadIndex].OverrideBlendState(overrideBlendState);
}

void sfRenderer::OverrideRasterizerState(bool overrideRasterizerState)
{
	m_st[g_RenderThreadIndex].OverrideRasterizerState(overrideRasterizerState);
}

void sfRenderer::OverrideDepthStencilState(bool overrideDepthStencilState)
{
	m_st[g_RenderThreadIndex].OverrideDepthStencilState(overrideDepthStencilState);
}

void sfRenderer::ScheduleTextureDeletion(TextureDeletionInfo& tex)
{
	sfAssertf(tex.m_Texture || tex.m_Image, "Invalid TextureDeletionInfo object");
	if (!m_UseDeletionQueue)
	{
		// Delete immediately, don't bother queueing
		DeleteTexture(tex);
	}
	else if (m_TextureDeletionStack.IsFull())
	{
		sfWarningf("Deleting texture while it might still be in use, because there's no more room on the stack. Texture is %s", tex.m_Texture->GetName());
		DeleteTexture(tex);
	}
	else
	{
		m_TextureDeletionStack.Push(tex);
	}
}

void sfRenderer::DeleteTexture(TextureDeletionInfo& tex)
{
	sfAssertf(tex.m_Texture || tex.m_Image, "Invalid TextureDeletionInfo object");
	bool isDynamic = (tex.m_Image != NULL);
	if (tex.m_Image) {
		sfTextureBase::sm_TotalRuntimeImageSizes -= tex.m_Image->GetSize();
		tex.m_Image->Release();
	}
	if (tex.m_Texture)
	{
		if (sfTextureBase::removeRefCallback.IsBound() && !isDynamic) // Call the callback, unless this is a dynamic images in which case we never called addRefCallback
		{
			grcTexture* localTex = tex.m_Texture;
			tex.m_Texture = pgRef<grcTexture>(NULL);
			sfTextureBase::removeRefCallback(localTex, tex.m_CallbackData);
		}
		else
		{
			tex.m_Texture.Release();
		}
	}
}

sfTextureBase* sfRenderer::CreateTexture(NativeTextureType nativeTex)
{
	sfTextureBase* tex = static_cast<sfTextureBase*>(CreateTexture());
	if (!tex->InitTexture(nativeTex, false))
	{
		tex->Release();
		return NULL;
	}
	return tex;
}

sfRendererBase* sfRendererBase::CreateRenderer()
{
	return new sfRenderer;
}

#if USE_SCALEFORM_AMP

GAmpRendererImpl<sfRendererBase> *sfRendererBase::CreateAmpRendererWrapper(sfRendererBase* renderer)
{
	return rage_new GAmpRendererImpl<sfRendererBase>(renderer);
}

#endif // USE_SCALEFORM_AMP
