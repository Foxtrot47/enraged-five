//
// scaleform/scaleform.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef SAMPLE_FLASH_SCALEFORM_H
#define SAMPLE_FLASH_SCALEFORM_H

#include "channel.h"
#include "input.h"
#include "renderer.h"
#include "translator.h"
#include "scaleformheaders.h"

#include "atl/bitset.h"
#include "atl/delegate.h"
#include "atl/inlist.h"
#include "diag/stats.h"
#include "file/asset.h"
#include "file/stream.h"
#include "grcore/effect.h"
#include "input/pad.h"
#include "paging/dictionary.h"
#include "system/memory.h"
#include "system/timemgr.h"


// allowing folks to selectively force these back on
#define SCALEFORM_MAX_PATH (96)
#define SCALEFORM_DICT_MAX_PATH (48)

#define __SF_STATS __STATS || __BANK

#if __SF_STATS
#define SF_STATS_ONLY(x)  x
#else
#define SF_STATS_ONLY(x)
#endif

#define __SF_STATS_WITHOUT_AMP (__STATS && !USE_SCALEFORM_AMP)

namespace rage
{
class grcViewport;
class sfScaleformMovie;
class sfScaleformManager;



class sfFile : public GFile
{
public:
	sfFile() : m_Stream(NULL) {}

	~sfFile() {
		if (m_Stream)
		{
			m_Stream->Close();
		}
	}

	void Open(const char* filename) {
		const char* extn = ASSET.FindExtensionInPath(filename);
		m_Stream = ASSET.Open(filename, extn ? extn + 1 : "");
	}

	virtual const char* GetFilePath();

	virtual bool IsValid();
	virtual bool IsWritable();

	virtual SInt Tell();
	virtual SInt64 LTell();

	virtual SInt GetLength();
	virtual SInt64 LGetLength();

	virtual SInt GetErrorCode();

	virtual SInt Write(const UByte* pbuffer, SInt numBytes);
	virtual SInt Read(UByte* pbuffer, SInt numBytes);
	virtual SInt SkipBytes(SInt numBytes);

	virtual SInt BytesAvailable();

	virtual bool Flush();

	virtual SInt Seek(SInt offset, SInt origin/* =Seek_Set */);
	virtual SInt64 LSeek(SInt64 offset, SInt origin/* =Seek_Set */);

	virtual bool ChangeSize(SInt /*newSize*/);

	virtual SInt CopyFromStream(GFile *pstream, SInt byteSize);

	virtual bool Close();

protected:
	fiStream* m_Stream;
};

class sfPreallocatedMemoryWrapper : public GSysAllocPaged
{
public:
	sfPreallocatedMemoryWrapper(datResourceMap preallocatedChunks, size_t granularity, size_t SF_STATS_ONLY(peak), sysMemAllocator* extraAllocator, atDelegate<void (void*)> freePreallocationDelegate, const char* arenaName);
	virtual ~sfPreallocatedMemoryWrapper();

	virtual void GetInfo(GSysAllocPaged::Info* i) const;

	virtual void* Alloc(UPInt size, UPInt align);

	virtual bool Free(void* ptr, UPInt size, UPInt);

	struct ScaleformAllocStat
	{
		size_t m_Size;
		size_t m_Count;
	};
	typedef atFixedArray<ScaleformAllocStat, 32> AllocsRequiredArray;

	static bool ComputePreallocation(AllocsRequiredArray& allocsRequired, size_t lowestGranularity, datResourceInfo& outRsc, size_t& outGranularity);

	bool ComputeCurrentPreallocationNeeds(datResourceInfo& outRsc, size_t& outGranularity);

#if __SF_STATS
	void PrintDebugInfo();
#endif

	size_t MaxChunkSize() { return 32 * m_Granularity; }

	// We return "units" of multiples of m_Granularity to scaleform. A chunk could contain enough space for multiple units.
	struct ChunkInfo
	{
		char*	m_Storage;
		size_t	m_Size;
		u32		m_UsedBitmap; // bits represent 'granularity'-sized units of allocation. In other words on a 64k chunk with 16k granularity, there are 4 useful bits. A '1' means that unit within the chunk is used

		int		GetNumUnits(sfPreallocatedMemoryWrapper& owner) { return ptrdiff_t_to_int(m_Size / owner.m_Granularity); }
		void*	GetUnitAddr(int i, sfPreallocatedMemoryWrapper& owner) { return m_Storage + owner.m_Granularity * i; }
		int		GetUnitIndex(void* addr, sfPreallocatedMemoryWrapper& owner) { return ptrdiff_t_to_int((char*)addr - m_Storage) / (int)owner.m_Granularity; }
		bool	IsAddrOnChunk(void* addr) { return (char*)addr >= m_Storage && (char*)addr < (m_Storage + m_Size); }

		static bool SizeCompare(const ChunkInfo& a, const ChunkInfo& b)
		{
			return a.m_Size < b.m_Size;
		}
	};
	size_t					m_Granularity;
	const char*				m_ArenaName;
	sysMemAllocator*		m_Allocator;
	atArray<ChunkInfo>		m_PreallocatedChunks;
	atArray<ChunkInfo>		m_ExtraChunks;
	atDelegate<void (void*)> m_FreePreallocationDelegate;

	static atDelegate<void (void)> sm_AllocationFailedDelegate;

#if __SF_STATS

	size_t FindPreallocatedSize();
	size_t FindExtraAllocationSize();

	struct ChunkStat
	{
		ChunkStat() : m_Size(0), m_HighCount(0), m_Count(0), m_Reserved(0) {}
		u16 m_Size;
		u16 m_HighCount;
		u16 m_Count;
		u16 m_Reserved;
	};

	typedef atRangeArray<ChunkStat, 8> StatTable;

	void LogAlloc(StatTable& table, size_t size);
	void LogFree(StatTable& table, size_t size);

	StatTable	m_PreallocatedStats;
	StatTable	m_ExtraStats;
	size_t		m_PeakAllowed;
#endif
};

class sfScaleformFunctionHandler : public GFxFunctionHandler
{
public:
	inlist_node<sfScaleformFunctionHandler> m_HandlerListNode;

	enum
	{
		CONSTRUCTOR_ID = 0,	// The constructor function must have this ID
	};

	virtual void OnUpdate(sfScaleformMovieView& /*view*/, float /*dt*/) {};
	virtual void OnDelete(sfScaleformMovieView& /*view*/) {};

	// PURPOSE: Creates and returns a new class object.
	// NOTES: This function calls AddStaticMethods and AddInstanceMethods with the appropriate data
	GFxValue BuildActionscriptObject(GFxMovieView& movie, GFxValue& originalClass);

	// PURPOSE: This function should call AddMethodToAsObject for each static method in the new class
	// NOTES: Subclasses should override this to add their functions
	virtual void AddStaticMethods(GFxMovieView& /*movie*/, GFxValue& /*classObject*/, GFxValue& /*originalClass*/) {}

	// PURPOSE: This function should call AddMethodToAsObject for each instance method in the new class
	// NOTES: Subclasses should override this to add their functions
	virtual void AddInstanceMethods(GFxMovieView& /*movie*/, GFxValue& /*classPrototype*/, GFxValue& /*originalClass*/) {}

	// Used when building a class, to add methods either to the class object itself (for static methods) or to the 
	// prototype object (for instance methods)
	void AddMethodToAsObject(GFxMovieView& movie, GFxValue& asClassPrototype, const char* name, int methodId);

protected:
};

#define CHECK_NUM_SF_ARGS(name, count) \
	if (!sfVerifyf(params.ArgCount == count,		\
	"Wrong number of arguments to \"%s\", expected %d and got %d", name, count, params.ArgCount)) \
{												\
	return;										\
}												\
	//END

#define CHECK_SF_VAR(var, requiredType) \
	if (!sfVerifyf(var.GetType() == requiredType || (requiredType == GFxValue::VT_Object && var.IsObject()), \
	"Wrong type for argument \"%s\", expected %s and got %s", #var, sfScaleformManager::GetTypeName(requiredType), sfScaleformManager::GetTypeName(var)))	\
{								\
	return;						\
}								\
	//END

#define CHECK_OPT_SF_VAR(var, requiredType) \
	if (!sfVerifyf(!var.IsDefined() || var.GetType() == requiredType, \
	"Wrong type for argument \"%s\", expected %s or undefined and got %s", #var, sfScaleformManager::GetTypeName(requiredType), sfScaleformManager::GetTypeName(var)))	\
{								\
	return;						\
}								\
	//END



// PURPOSE: A rage wrapper around a scaleform GFxMovieView object.
class sfScaleformMovieView
{
public:

	// PURPOSE: Returns the raw scaleform movieview object.
	GFxMovieView&				GetMovieView() {return *m_MovieView;}

	// PURPOSE: Returns the sfScaleformMovie that this movieview was created from
	sfScaleformMovie*			GetMovie() {return m_Movie;}

	// PURPOSE: Gets and Sets the ACTIVE flag
	// NOTES: This is independent of GFxMovie::GetPlayState and only controls whether
	// or not the movie is updated through sfScaleformManager::UpdateAll
	bool						GetIsActive() {return m_Flags.IsSet(FLAG_ACTIVE);}
	void						SetIsActive(bool b) { m_Flags.Set(FLAG_ACTIVE, b);}

	// PURPOSE: Gets and Sets the VISIBLE flag
	// NOTES: This is independent of GFxMovie::GetVisible and only controls whether
	// or not the movie is drawn through sfScaleformManager::DrawAll
	bool						GetIsVisible() {return m_Flags.IsSet(FLAG_VISIBLE);}
	void						SetIsVisible(bool b) { m_Flags.Set(FLAG_VISIBLE, b);}

	// PURPOSE: Gets and Sets any of the flag values (including ACTIVE and VISIBLE)
	bool						GetFlag(int flag) {return m_Flags.IsSet(flag);}
	void						SetFlag(int flag, bool b) {m_Flags.Set(flag, b);}

	typedef atFixedBitSet<ioPad::MAX_PADS, u8> PadMask;
	// PURPOSE: Pads whose bits are set here get their input passed on to the 
	// movie - if FLAG_PAD_INPUT is also set.
	PadMask						GetInputPadMask() {return m_PadMask;}
	void						SetInputPadMask(PadMask p) {m_PadMask = p;}

	// PURPOSE: Update this movie. Does not check the active flag or apply any input values
	void						Update(float time = TIME.GetSeconds());

	// PURPOSE: Draws this movie. Does not check the visible flag but does do a bounding
	// box check to skip drawing for offscreen movies
	void						Draw();

	// PURPOSE: Draws the movie in world space. Pass in the current viewport and the world space position for the movie
	// NOTES: The movie's native output size is in TWIPS (twentyths of a pixel). Translated into worldspace coordinates 
	// this makes for a very large movie. To make working with the movies more convenient we resize the movies by a factor
	// of (1 / (20 * MovieSize)) - such that movies come out as 1x1 units. Note that you should also set the viewport
	// size to be the 'typical' size of the movie because I believe that's what's used for purposes of tesselation and 
	// font rendering.
	void						DrawInWorldSpace(sfScaleformManager& mgr);

	// PURPOSE: Sets the new scale to unit length vector based on given width, height 
	// NOTES: This function is required for the client code to override
	// the scale in case original movies were made with incorrect scale
	void						RecomputeScaleToUnitLengthVector(float width, float height);

	// PURPOSE: Applies any input data from the last frame to this movie.
	// NOTES: Update does not call this, but sfScaleformManager::UpdateAll does
	// Only input sources whose FLAG_*_INPUT flags are set get applied,
	// When a movie is created all of the input flags are false.
	void						ApplyInput(sfInput& input);

	// PURPOSE: Installs a new function handler (which could also mean installs a new class) in actionscript.
	// This may replace an existing function / class, or provide a new one
	// PARAMS:
	//		actionscriptPath - The path to the new object, e.g. "_global.rockstar.ui.Colour"
	//		onlyReplaceExistingClass - If true, only replace a function or class that already exists in actionscript - don't add a function or class if its not already there
	//		needsMovieEvents - If true, we call the OnUpdate() and OnDelete() methods for the object we're registering
	template<typename _Type>
	bool						InstallFunctionHandler(const char* actionscriptPath, bool onlyReplaceExistingClass, bool needsMovieEvents);

#if __SF_STATS_WITHOUT_AMP
	// PURPOSE: Every time we get stats for a scaleform movie, scaleform will allocate space. Normally
	// the AMP server will reset these stats and free the space for us, but when running without an AMP server
	// we need to periodically reset the stats. PerformSafeModeOperations will do this, but you can also call this on
	// individual movies if there's no opportunity to call PerformSafeModeOperations
	void ResetStats(); 
#endif

	enum
	{
		FLAG_ACTIVE,
		FLAG_VISIBLE,
		FLAG_PAD_INPUT,
		FLAG_MOUSE_INPUT,
		FLAG_KEYBOARD_INPUT,
		NUM_FLAGS,
	};


#if __SF_STATS
	// These are temporary while I find a way to get the data that scaleform already collects.
	struct Stat
	{
		Stat() { last = 0.0f; avg = 0.0f; }
		float last, avg;
		void Set(float f) { last = f; avg = Lerp(0.99f, f, avg); }
	};
	Stat m_StatUpdate, m_StatUpdateAs, m_StatDraw, m_StatGame, m_StatTess, m_StatPrims, m_StatTris;
#endif

protected:
	// PURPOSE: Adds a function handler to the list of function handlers for this movie view
	// NOTES: Technically you only need to do this if you need to know about the Update or Delete events.
	// So a function handler that's just replacing some expensive math function wouldn't need to do this
	void						RegisterFunctionHandler(sfScaleformFunctionHandler* fn);


	friend class sfScaleformMovie;
	friend class sfScaleformManager;

	sfScaleformMovieView();
	~sfScaleformMovieView();

	// For worldspace drawing - compute a scale vector so we can rescale for movie to 1x1 units
	Vec3V								m_ScaleToUnitLengthVector;
	GPtr<GFxMovieView>					m_MovieView;
	sfScaleformMovie*					m_Movie;
	inlist_node<sfScaleformMovieView>	m_MovieListNode;
	atFixedBitSet<NUM_FLAGS, u8>		m_Flags;
	PadMask								m_PadMask; // which pads do we take input from
	atArray<sfScaleformFunctionHandler*> m_FunctionHandlers;
};


// PURPOSE: A rage wrapper around a scaleform GFxMovieDef object.
class sfScaleformMovie
{
public:

	// PURPOSE: Returns the raw scaleform moviedef object
	GFxMovieDef&				GetMovie() {return *m_Movie;}

protected:
	friend class sfScaleformManager;

	sfScaleformMovie();
	~sfScaleformMovie();

	GPtr<GFxMovieDef>				m_Movie;
	pgDictionary<grcTexture>*		m_Textures;
	inlist_node<sfScaleformMovie>	m_MovieListNode;
	bool							m_OwnsTextures;

};

// PURPOSE: A manager for the rage-wrapped scaleform system. 
// NOTES:
//		This class is responsible for initting and shutting down the
//	scaleform libraries, as well as adding in the hooks to the rage
//	renderer, memory system, file system, and others.
class sfScaleformManager : public datBase
{
public:

	static const char* GetTypeName(GFxValue::ValueType val);
	static const char* GetTypeName(const GFxValue& val) { return GetTypeName(val.GetType()); }

	static int sm_MemoryBucket;
	static int sm_ChildThreadProcessor; // The processor to put ALL child threads on (TODO: maybe we don't want all of them on their own)

	// PURPOSE: Initialize the scaleform manager. Call this just once
	void Init(sysMemAllocator* allocator = NULL);

	// PURPOSE: Shutdown the scaleform manager. Call this just once.
	void Shutdown();

	// PURPOSE: Create a special arena for loading resources into.
	void CreateResourceArena(class sysMemAllocator* allocator);

	// PURPOSE: Destroy the special resource arena.
	void DestroyResourceArena();

	// PURPOSE: Call this every frame in the render thread
	void BeginFrame();

	// PURPOSE: Call this every frame in the render thread
	void EndFrame();

	// PURPOSE: Call this every frame when no other scaleform operations are being done (no update, no render, no invokes, etc.)
	void PerformSafeModeOperations();

	// PURPOSE: Loads a movie given a filename. The movie can be a .swf or .gfx file.
	// NOTES:
	//		This method also loads in a .#td texture dictionary with the same name
	//	as the movie file if one exists. The client is responsible for deleting the 
	//  movie when they are finished with it.    The txd is only loaded if one is not
	//  passed into the function
	//  can choose whether to try and load in a txd or not using bHasTxd
	// RETURNS:
	//		A new movie instance or NULL if one couldn't be loaded.
	sfScaleformMovie* LoadMovie(const char* filename, bool bHasTxd = true, pgDictionary<grcTexture>* pTxd = NULL, u32 arenaId = 0 );

	// PURPOSE: Destroys the movie. 
	// NOTES: All the memory allocated by the movie may not
	// be immediately freed, if other movies or movie instances are holding references
	// to this movie. The texture dictionary _is_ released though. 
	void DeleteMovie(sfScaleformMovie* movie);

	// PURPOSE: Creates a movie view (instance) of a scaleform movie
	// RETURNS: A new movie view, or NULL if one couldn't be created
	// NOTES: The movie is active and visible when created.
	sfScaleformMovieView* CreateMovieView(sfScaleformMovie& movie, u32 arenaId = 0);

	// PURPOSE: Deletes an existing movie view
	void DeleteMovieView(sfScaleformMovieView* movieView);

	// PURPOSE: Sets the callback class to use when ExternalInterface calls
	// are made in actionscript. A sample interface class in rage/script/src/scr_scaleform
	// which will call any function that has been registered with ragescript
	void InitExternalInterface(GFxExternalInterface& inter);

	// PURPOSE: Call when you're not going to do any more drawing with this manager. 
	// It means we can free resources a little more efficiently
	void FinishedDrawing();

	// PURPOSE: Call this if you want to create a GFxDrawTextManager for drawing text w/o 
	// playing a flash movie.
	// PARAMS: fontMovie - a movie containing the fonts to draw with
	void InitDrawTextManager(sfScaleformMovie& fontMovie);

	// PURPOSE: Destroys the GFxDrawTextManager.
	// NOTES: The user is still responsible for destroying
	// the movie that contained the fonts the manager used.
	void ShutdownDrawTextManager();

	// PURPOSE: Access the GFxDrawTextManager.
	GFxDrawTextManager* GetDrawTextManager();

	// PURPOSE: Get the sfInput object
	sfInput& GetInput() {return m_Input;}

	// PURPOSE: get the loader object
	GFxLoader* GetLoader() {return m_Loader;}

	// PURPOSE: Gets the renderer
	sfRendererBase& GetRenderer();

	// PURPOSE: Gets the rage renderer (not the potential AMP wrapper)
	sfRendererBase& GetRageRenderer();

	// PURPOSE: Gets the global translator.
	// NOTES: Each movie that gets loaded will use this translator by default.
	// But if you want a different one you can install one on each movie definition using
	// movie->GetMovie().SetTranslator()
//	GFxTranslator& GetTranslator() {sfAssertf(m_Translator.GetPtr(), "Translator isn't initted!"); return *m_Translator.GetPtr();}

	// PURPOSE: Resizes the mesh caches back to their originally specified size, and flushes them
	void ResetMeshCache();
	void ResetFontCache(); // resets the font texture cache

	void SetEdgeAAEnable(bool value);

	// PURPOSE: Creates a new memory arena that will hand out preallocated memory to scaleform, along with an
	// extra allocator in case we run out
	// PARAMS:
	//		preallocatedChunks - A resource map that lists all of the preallocated chunks of memory for scaleform to use
	//		extraAllocator - A memory allocator to allocate from if we used up all the chunks or can't find enough free space
	//		freePreallocationDelegate - A delegate to call on each of the preallocated chunks when the arena is destroyed
	// RETURNS:
	//		The ID of the new allocator - pass this in to LoadMovie or CreateMovieView to use it
	u32 CreatePreallocatedMemoryArena(const datResourceMap& preallocatedChunks, size_t granularity, size_t peakAllowed, sysMemAllocator* extraAllocator, atDelegate<void (void*)> freePreallocationDelegate, const char* arenaName);

	// PURPOSE: Shuts down a memory arena. Make sure the movies and movieviews using this arena have been shut down first.
	void DestroyPreallocatedMemoryArena(u32 i);

	sfPreallocatedMemoryWrapper* GetAllocatorForArena(u32 i);

#if __BANK
	void AddWidgets(class bkGroup&);

	float						m_CurveTolerance;
	void UpdateCurveTolerance();
#endif

#if __SF_STATS
	void GetGlobalHeapSizes(size_t& curr, size_t& peak, size_t& debugCurr, size_t& debugPeak, const char*& lastMovieToOverAllocate);
#endif

#if !__NO_OUTPUT
	struct AutoSetCurrMovieName
	{
		AutoSetCurrMovieName(const char* movie);
		~AutoSetCurrMovieName() { sm_DebugOutputCurrMovieName = NULL;}
	};

	static __THREAD const char*			sm_DebugOutputCurrMovieName;
#else // __NO_OUTPUT
	struct AutoSetCurrMovieName
	{
		AutoSetCurrMovieName(const char* ) {}
		~AutoSetCurrMovieName() { }
	};
#endif
	
	static bool IsPerformingMemoryOperation() { return sm_IsPerformingMemoryOperation; }

	static void SetIsPerformingMemoryOperation(bool b) { sm_IsPerformingMemoryOperation = b; }

	static u32 sm_FramesBetweenGarbageCollection;
	static u32 sm_MaxRootsForGarbageCollection;

protected:
	friend class sfScaleformMovieView;
	friend class sfScaleformMovie;
	friend class sfAppControlCallback;

	void ForceGC();
#if __SF_STATS
	void PrintStats(u32 memReportType);
	void PrintArenaStats();
#endif

	int FindArenaIndexFromId(u32 id);

	typedef inlist<sfScaleformMovieView, &sfScaleformMovieView::m_MovieListNode> MovieViewList;
	typedef inlist<sfScaleformMovie, &sfScaleformMovie::m_MovieListNode> MovieList;	

	struct ArenaInfo
	{
		sfPreallocatedMemoryWrapper* m_Allocator;
		u32							 m_Id;
	};

	class GSysAllocPaged*		m_MemoryWrapper;
	class sfRageMemoryWrapper*	m_ResourceMemoryWrapper;
	GFxSystem*					m_System;
	GFxLoader*					m_Loader;
	GPtr<GFxExternalInterface>  m_ExternalInterface;
	GPtr<GFxFileOpener>			m_FileOpener;
	GPtr<GFxFSCommandHandler>	m_FSCommandHandler;
	GPtr<GFxImageCreator>		m_ImageCreator;
	GPtr<sfRendererBase>		m_Renderer;
//	GPtr<sfTranslator>			m_Translator;
	GPtr<GFxRenderConfig>		m_RenderConfig;
	GPtr<GFxLog>				m_Log;
	GPtr<GFxDrawTextManager>	m_DrawTextManager;
	GPtr<GFxFontCompactorParams> m_FontCompactorParams;
	MovieViewList				m_MovieViewList;
	MovieList					m_MovieList;
	sfInput						m_Input;
	u32							m_MeshCacheResetRequested;
	atArray<ArenaInfo>			m_MemoryArenas;
	u32							m_NextArenaId;
	sysCriticalSectionToken		m_MovieListCS;
#if USE_SCALEFORM_AMP
	GAmpRendererImpl<sfRendererBase>*	m_RendererWrapper;
	GFxAmpAppControlInterface*	m_AppControlCallback;

	// Not sure why AMP doesn't maintain these state variables itself
	bool						m_Overdraw;
#endif

	// True if we're in the middle of a memory operation
	static volatile bool		sm_IsPerformingMemoryOperation;

};

template<typename _Type>
bool sfScaleformMovieView::InstallFunctionHandler(const char* actionscriptPath, bool onlyReplaceExistingClass, bool needsMovieEvents)
{
	GFxMovieView& movie = GetMovieView();

	bool hasOriginalClass = movie.IsAvailable(actionscriptPath);

	if (onlyReplaceExistingClass && !hasOriginalClass)
	{
		return false;
	}

	GFxValue originalClass;
	if (hasOriginalClass)
	{
		movie.GetVariable(&originalClass, actionscriptPath);
	}

	// Create the new function handler object
	_Type* classDef = rage_new _Type;

	GFxValue asObject = classDef->BuildActionscriptObject(movie, originalClass);

	// Finally add the class to the scaleform movie
	movie.SetVariable(actionscriptPath, asObject, GFxMovie::SV_Normal);

	if (needsMovieEvents)
	{
		RegisterFunctionHandler(classDef);
	}
	return true;
}

} // namespace rage

#endif
