// 
// scaleform/scaleform.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "scaleform.h"

#include "channel.h"

#include "atl/binmap.h"
#include "atl/dlist.h"
#include "diag/output.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "paging/dictionary.h"
#include "paging/rscbuilder.h"
#include "profile/profiler.h"
#include "system/memory.h"
#include "system/memvisualize.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/stack.h"
#include "profile/timebars.h"

RAGE_DEFINE_CHANNEL(Scaleform);

RAGE_DEFINE_SUBCHANNEL(Scaleform, Player);

PF_PAGE(ScaleformPage, "rage - Scaleform");
PF_GROUP(Scaleform);
PF_LINK(ScaleformPage, Scaleform);
PF_TIMER(ManagerUpdate, Scaleform);
PF_TIMER(ApplyInput, Scaleform);
PF_TIMER(MovieUpdate, Scaleform);
PF_TIMER(MovieDraw, Scaleform);
PF_TIMER(BeginFrame, Scaleform);
PF_TIMER(MovieDraw_Stall, Scaleform);

PARAM(noscaleformamp, "Disable scaleform AMP server");
PARAM(scaleform_maxslotheight, "Set the font cache max slot height to X");
PARAM(scaleform_compactfonts, "Compacts the font library on load.");

#define USE_PAGE_ALLOCATOR 1
#define USE_SCALEFORM_DEBUG_FILL !__FINAL

namespace rage
{

int sfScaleformManager::sm_MemoryBucket = 11;
int sfScaleformManager::sm_ChildThreadProcessor = 2;
u32 sfScaleformManager::sm_FramesBetweenGarbageCollection = ~0u;
u32 sfScaleformManager::sm_MaxRootsForGarbageCollection = ~0u;
OUTPUT_ONLY(__THREAD const char* sfScaleformManager::sm_DebugOutputCurrMovieName = NULL);

atDelegate<void(void)> sfPreallocatedMemoryWrapper::sm_AllocationFailedDelegate;

volatile bool sfScaleformManager::sm_IsPerformingMemoryOperation = false;


const int MESH_CACHE_MEM_LIMIT = 384 * 1024;
const int RENDERGEN_MEM_LIMIT = 64 * 1024;

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfRageMemoryWrapper
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

class sfRageMemoryWrapper : public GSysAllocPaged
{
public:
	sfRageMemoryWrapper(sysMemAllocator* allocator, int allocationType)
	{
		m_Allocator = allocator;
		m_AllocationType = allocationType;
#if __SF_STATS
		m_HighWaterMark = 0;	  
		m_DebugHighWaterMark = 0;
		m_CurrentAllocations = 0;
		m_CurrentDebugAllocations = 0;
		m_LastMovieToOverAllocate[0] = '\0';
#endif
	}

	virtual void GetInfo(GSysAllocPaged::Info* i) const;

	virtual void* Alloc(UPInt size, UPInt align);

	virtual bool Free(void* ptr, UPInt, UPInt);

	sysMemAllocator* m_Allocator;
	int m_AllocationType;
#if __SF_STATS
	size_t m_HighWaterMark;
	size_t m_DebugHighWaterMark;
	size_t m_CurrentAllocations;
	size_t m_CurrentDebugAllocations;
	char m_LastMovieToOverAllocate[SCALEFORM_MAX_PATH];
#endif
};

void sfRageMemoryWrapper::GetInfo( GSysAllocPaged::Info* i ) const
{
	i->MinAlign = 4;
	i->MaxAlign = 0;
	i->Granularity = 16 * 1024;
}

void* sfRageMemoryWrapper::Alloc( UPInt size, UPInt align )
{
	sfAssertf(!sfScaleformManager::IsPerformingMemoryOperation(), "No recursive memory ops!");
	sfScaleformManager::SetIsPerformingMemoryOperation(true);

	sysMemUseMemoryBucket SCALEFORM(sfScaleformManager::sm_MemoryBucket);
	void* newMem = NULL;
	if (GMemory::sm_DoingDebugAlloc)
	{
		MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
		sysMemStartDebug();
		newMem = sysMemAllocator::GetCurrent().Allocate(size, align, MEMTYPE_DEBUG_VIRTUAL);
		sysMemEndDebug();
	}
	else
	{
		MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
		newMem = m_Allocator->Allocate(size, align, m_AllocationType);
	}
	if (!newMem)
	{
		sfErrorf("Movie %s couldn't get any resource memory! This is really bad.", sfScaleformManager::sm_DebugOutputCurrMovieName);
		sysMemStartTemp();
		newMem = sysMemAllocator::GetCurrent().TryAllocate(size, align);
		sysMemEndTemp();
		if (!newMem)
		{
#if __NO_OUTPUT
			Quitf(ERR_GUI_SF_MEM_1,"Movie: couldn't get any game heap memory either. This is basically fatal b/c scaleform doesn't expect allocs to fail, so I'll just crash now.");
#else
			Quitf(ERR_GUI_SF_MEM_1,"Movie %s: couldn't get any game heap memory either. This is basically fatal b/c scaleform doesn't expect allocs to fail, so I'll just crash now.", sfScaleformManager::sm_DebugOutputCurrMovieName);
#endif //__NO_OUTPUT
		}
		else
		{
			sfErrorf("Movie %s: I got some emergency memory from the game heap.", sfScaleformManager::sm_DebugOutputCurrMovieName);
		}
	}

#if __SF_STATS
	if (GMemory::sm_DoingDebugAlloc)
	{
		m_CurrentDebugAllocations += size;
		if (m_CurrentDebugAllocations > m_DebugHighWaterMark)
		{
			m_DebugHighWaterMark = m_CurrentDebugAllocations;
		}
	}
	else
	{
		m_CurrentAllocations += size;
		if (m_CurrentAllocations > m_HighWaterMark)
		{
			m_HighWaterMark = m_CurrentAllocations;
			sfDebugf1("Movie %s: new high water mark for scaleform allocations: %" SIZETFMT "dk", sfScaleformManager::sm_DebugOutputCurrMovieName, m_CurrentAllocations / 1024);
		}
#if !__NO_OUTPUT
		safecpy(m_LastMovieToOverAllocate, sfScaleformManager::sm_DebugOutputCurrMovieName);
#endif
	}
#endif

//	sfDebugf1("ResourceMem allocated %dk (align=%d) at 0x%p", size/1024, align, newMem);

#if USE_SCALEFORM_DEBUG_FILL
	sysMemSet(newMem, 0x5f, size);
#endif

	sfScaleformManager::SetIsPerformingMemoryOperation(false);

	return newMem;
}

bool sfRageMemoryWrapper::Free( void* ptr, UPInt SF_STATS_ONLY(size), UPInt )
{
	sfAssertf(!sfScaleformManager::IsPerformingMemoryOperation(), "No recursive memory ops!");
	sfScaleformManager::SetIsPerformingMemoryOperation(true);

	sysMemStartTemp();
	if (sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->IsValidPointer(ptr))
	{
		MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
		// maybe this was our emergency allocation
		SF_STATS_ONLY(m_CurrentAllocations -= size);
		sysMemAllocator::GetCurrent().Free(ptr);
	}
	else if (sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_DEBUG_VIRTUAL)->IsValidPointer(ptr))
	{
		MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
		// maybe this was a debug heap allocation
		SF_STATS_ONLY(m_CurrentDebugAllocations -= size);
		sysMemAllocator::GetCurrent().Free(ptr);
	}
	else
	{
		MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
		SF_STATS_ONLY(m_CurrentAllocations -= size);
		m_Allocator->Free(ptr);
	}
	sysMemEndTemp();

	sfScaleformManager::SetIsPerformingMemoryOperation(false);

	return true;
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfPreallocatedMemoryWrapper
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


sfPreallocatedMemoryWrapper::sfPreallocatedMemoryWrapper( datResourceMap preallocatedChunks, size_t granularity, size_t SF_STATS_ONLY(peak), sysMemAllocator* extraAllocator, atDelegate<void (void*)> freePreallocationDelegate, const char* SF_STATS_ONLY(arenaName) )
: m_Granularity(granularity)
, m_Allocator(extraAllocator)
, m_FreePreallocationDelegate(freePreallocationDelegate)
, m_ArenaName(NULL)
{
#if __SF_STATS
	m_ArenaName = arenaName;
	m_PeakAllowed = peak;
#endif
	sysMemStartTemp();
	m_PreallocatedChunks.Resize(preallocatedChunks.VirtualCount);
	sysMemEndTemp();

	Assertf(granularity == AlignPow2(granularity, 4 * 1024), "Granularity needs to be a multiple of GHeap_PageSize (%dk) for scaleform, got %" SIZETFMT "d", 4, granularity);

	for(int i = 0; i < preallocatedChunks.VirtualCount; i++)
	{
		ChunkInfo& chunk = m_PreallocatedChunks[i];
		chunk.m_Storage = (char*)preallocatedChunks.Chunks[i].DestAddr;
		chunk.m_Size = preallocatedChunks.Chunks[i].Size;

		// How many allocation units (m_Granularity multiples) can fit within this chunk?
		u32 units = ptrdiff_t_to_int(chunk.m_Size / m_Granularity);
		sfAssertf(units * m_Granularity == chunk.m_Size, "Preallocated chunk size %" SIZETFMT "d is not a multiple of granularity %" SIZETFMT "d", chunk.m_Size, m_Granularity);
		sfAssertf(units > 0, "Preallocated chunk size %" SIZETFMT "d is too small", chunk.m_Size);

		// make a bitmask that has 'units' 0s, and everything else 1s
		u32 unitOnes = (1 << units) - 1;	/// 0b000000000111
		chunk.m_UsedBitmap = ~unitOnes;		/// 0b111111111000
	}

	std::sort(m_PreallocatedChunks.begin(), m_PreallocatedChunks.end(), ChunkInfo::SizeCompare);
}

sfPreallocatedMemoryWrapper::~sfPreallocatedMemoryWrapper()
{
	sfAssertf(m_ExtraChunks.GetCount() == 0, "There were still %d extra chunks remaining - not cleaned up properly?", m_ExtraChunks.GetCount());

	for(int i = m_ExtraChunks.GetCount()-1; i >= 0; --i)
	{
		sfVerifyf(Free(m_ExtraChunks[i].m_Storage, m_ExtraChunks[i].m_Size, 0), "Couldn't free one of the extra allocations %p size %" SIZETFMT "d. This will leak!", m_ExtraChunks[i].m_Storage, m_ExtraChunks[i].m_Size);
	}

	for(int i = 0; i < m_PreallocatedChunks.GetCount(); i++)
	{
		m_FreePreallocationDelegate(m_PreallocatedChunks[i].m_Storage);
	}
}

void sfPreallocatedMemoryWrapper::GetInfo( GSysAllocPaged::Info* i ) const
{
	i->MinAlign = 4;
	i->MaxAlign = 0;
	i->Granularity = m_Granularity;
}

void* sfPreallocatedMemoryWrapper::Alloc( UPInt size, UPInt align )
{
	sysMemUseMemoryBucket b(MEMBUCKET_SCALEFORM);

	// Arguably we only need this around the actual rage allocator calls below. But for consistencies sake put it here for now.
	sfAssertf(!sfScaleformManager::IsPerformingMemoryOperation(), "No recursive memory ops!");
	sfScaleformManager::SetIsPerformingMemoryOperation(true);

#if !__FINAL
	if (size > 64 * 1024 && m_ArenaName &&!strstr(m_ArenaName, "font_lib")) // we know the font lib has huge objects in it.
	{
		sysStack::PrintStackTrace();
		sfWarningf("In arena %s, large scaleform allocation of %" SIZETFMT "d kb coming from here", m_ArenaName, size / 1024);
	}
#endif

	// loop over all the chunks, finding one that will hold the requested allocation.
	int numUnitsNeeded = ptrdiff_t_to_int(size / m_Granularity);

	for(int chunkIdx = 0; chunkIdx < m_PreallocatedChunks.GetCount(); chunkIdx++)
	{
		ChunkInfo& chunk = m_PreallocatedChunks[chunkIdx];

		if (chunk.m_UsedBitmap != 0xFFFFFFFF)
		{
			// This block has free space, see if it has enough contiguous space

			// turn numUnitsNeeded into a bit mask that looks like 0b0000000111 (if we needed 3 units)
			u32 bitMask = (1 << numUnitsNeeded) - 1;

			int unitsInChunk = ptrdiff_t_to_int(chunk.m_Size / m_Granularity);

			for(int unitIdx = 0; unitIdx < unitsInChunk - numUnitsNeeded + 1; unitIdx++)
			{
				if ((bitMask & chunk.m_UsedBitmap) == 0)
				{
					// found some free space
					void* startAddr = chunk.GetUnitAddr(unitIdx, *this);
					chunk.m_UsedBitmap |= bitMask; // Mark all those bits used.

#if __SF_STATS
					LogAlloc(m_PreallocatedStats, numUnitsNeeded);
#endif

//					sfDebugf1("PreAlloc allocated %dk (align=%d) at 0x%p (from %dk chunk at 0x%p)", size/1024, align, startAddr, chunk.m_Size / 1024, chunk.m_Storage);

#if USE_SCALEFORM_DEBUG_FILL
					sysMemSet(startAddr, 0x5f, size);
#endif

					sfScaleformManager::SetIsPerformingMemoryOperation(false);

					return startAddr;
				}
				bitMask <<= 1;
			}
		}
	}


	// No preallocated memory found eh? Allocate a new chunk
	ChunkInfo newChunk;

	{
		MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
		newChunk.m_Storage = (char*)m_Allocator->Allocate(size, align);
		newChunk.m_Size = m_Allocator->GetSize(newChunk.m_Storage);
	}

	if (!newChunk.m_Storage)
	{
		if (sm_AllocationFailedDelegate.IsBound())
		{
			sm_AllocationFailedDelegate();
		}
#if __SF_STATS
		PrintDebugInfo();
#endif
		sfAssertf(false,"Movie %s: couldn't get any resource memory! This is really bad.", m_ArenaName);
		sysMemStartTemp();
		newChunk.m_Storage = (char*)sysMemAllocator::GetCurrent().TryAllocate(size, align);
		newChunk.m_Size = size;
		sysMemEndTemp();
		if (!newChunk.m_Storage)
		{
			Quitf(ERR_GUI_SF_MEM_2,"Movie %s: Couldn't get any game heap memory either. This is basically fatal b/c scaleform doesn't expect allocs to fail, so I'll just crash now.", m_ArenaName);
		}
		else
		{
			sfErrorf("Movie %s: I got some emergency memory from the game heap.", m_ArenaName);
		}
	}


	newChunk.m_UsedBitmap = ~0u;
	sysMemStartTemp();
	m_ExtraChunks.PushAndGrow(newChunk);
	sysMemEndTemp();
#if __SF_STATS
	LogAlloc(m_ExtraStats, numUnitsNeeded);

	u32 preallocHighAllocs = 0;
	for(int i = 0; i < m_PreallocatedStats.GetMaxCount(); i++)
	{
		if (m_PreallocatedStats[i].m_Size > 0)
		{
			preallocHighAllocs += m_PreallocatedStats[i].m_Size * m_PreallocatedStats[i].m_HighCount;
		}
	}

	u32 totalExtraAllocs = 0;
	for(int i = 0; i < m_ExtraStats.GetMaxCount(); i++)
	{
		if (m_ExtraStats[i].m_Size > 0)
		{
			totalExtraAllocs += m_ExtraStats[i].m_Size * m_ExtraStats[i].m_HighCount;
		}
	}

	u32 newPeak = (totalExtraAllocs + preallocHighAllocs) * (u32)m_Granularity / 1024;

	sfDebugf2("Movie %s: Needed extra storage in movie arena, max extra now %" SIZETFMT "dk. Current peak is %dk", m_ArenaName, totalExtraAllocs * m_Granularity / 1024, newPeak);


	if (FindPreallocatedSize() + FindExtraAllocationSize() > m_PeakAllowed)
	{
		PrintDebugInfo();
#if __64BIT || !__ASSERT
		ASSERT_ONLY(u32 sizeThatTriggeredAssert = ((u32)FindPreallocatedSize() + (u32)FindExtraAllocationSize()) / 1024);
		sfAssertf(false, "Movie %s: exceeded allowed peak of %" SIZETFMT "dk with an allocation of %" SIZETFMT "d align %" SIZETFMT "d! Current peak is %dk (%dk). This is bad. - Add bug for '*Code (UI)*'", m_ArenaName, m_PeakAllowed / 1024, size, align, newPeak, sizeThatTriggeredAssert);
		sysStack::PrintStackTrace();
#else
		sfAssertf(false, "Movie %s: exceeded allowed peak of %dk an allocation of %d align %d! Current peak is %dk. This is bad. - Add bug for '*Code (UI)*'", m_ArenaName, m_PeakAllowed / 1024, size, align, newPeak);
#endif
	}
#endif // __SF_STATS

#if USE_SCALEFORM_DEBUG_FILL
	sysMemSet(newChunk.m_Storage, 0x5f, size);
#endif

	sfScaleformManager::SetIsPerformingMemoryOperation(false);

	return newChunk.m_Storage;
}

bool sfPreallocatedMemoryWrapper::Free( void* ptr, UPInt size, UPInt )
{
	// Arguably we only need this around the actual rage allocator calls below. But for consistencies sake put it here for now.
	sfAssertf(!sfScaleformManager::IsPerformingMemoryOperation(), "No recursive memory ops!");
	sfScaleformManager::SetIsPerformingMemoryOperation(true);

	int numUnits = ptrdiff_t_to_int(size / m_Granularity);

	for(int i = 0; i < m_PreallocatedChunks.GetCount(); i++)
	{
		ChunkInfo& chunk = m_PreallocatedChunks[i];
		if (chunk.IsAddrOnChunk(ptr))
		{
			int startIndex = chunk.GetUnitIndex(ptr, *this);

			u32 unitBits = (1 << numUnits)-1; // 0x000000111
			u32 bitMask = ~(unitBits << startIndex); // 0x11111000111111

			chunk.m_UsedBitmap &= bitMask;

#if __SF_STATS
			LogFree(m_PreallocatedStats, numUnits);
#endif

//			sfDebugf1("PreAlloc freed %dk at 0x%p (from %dk chunk at 0x%p)", size/1024, ptr, chunk.m_Size / 1024, chunk.m_Storage);

			sfScaleformManager::SetIsPerformingMemoryOperation(false);

			return true;
		}
	}

	for(int i = 0; i < m_ExtraChunks.GetCount(); i++)
	{
		ChunkInfo& chunk = m_ExtraChunks[i];
		if (chunk.IsAddrOnChunk(ptr))
		{
			sysMemStartTemp();
			if (sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->IsValidPointer(ptr))
			{
				MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
				// maybe this was our emergency allocation
				sysMemAllocator::GetCurrent().Free(ptr);
			}
			else
			{
				MEM_USE_USERDATA(MEMUSERDATA_SCALEFORM);
				m_Allocator->Free(ptr);
			}
			sysMemEndTemp();
			m_ExtraChunks.DeleteFast(i);
#if __SF_STATS
			LogFree(m_ExtraStats, numUnits);
#endif

//			sfDebugf1("PreAlloc overfreed %dk at 0x%p", chunk.m_Size / 1024, chunk.m_Storage);

			sfScaleformManager::SetIsPerformingMemoryOperation(false);

			return true;
		}
	}

	sfErrorf("Couldn't find chunk containing address 0x%p (size %" SIZETFMT "d) for delete", ptr, size);

	sfScaleformManager::SetIsPerformingMemoryOperation(false);

	return false;
}

struct sfResourceInfo
{	
	enum Pages
	{
		TAIL16,
		TAIL8,
		TAIL4,
		TAIL2,
		BASE,
		HEAD2,
		HEAD4,
		HEAD8,
		HEAD16,
		NUM_PAGES,
	};

	static const u8 MaxPageCount[NUM_PAGES];

	static const u32 MAX_PAGES = datResourceChunk::MAX_CHUNKS - 2; // -1 for the file data, -1 just to be safe

	size_t m_BasePageSize;
	atRangeArray<u8, NUM_PAGES> m_Pages;
	u32 m_TotalAllocs;
	bool m_Error;

	void Reset()
	{
		m_BasePageSize = 0;
		m_TotalAllocs = 0;
		m_Error = false;
		sysMemSet(m_Pages.GetElements(), 0, sizeof(u8) * m_Pages.GetMaxCount());
	}

	void AdjustToPageCountLimit()
	{
		for(int i = 0; i < NUM_PAGES-1; i++)
		{
			while (m_Pages[i] > MaxPageCount[i])
			{
				m_Pages[i] -= 2;
				m_Pages[i+1] += 1;
				m_TotalAllocs--;
			}
		}
	}

	void AdjustToTotalPageLimit()
	{
		// Now make sure we don't go over the total number of allocations
		while(m_TotalAllocs >= MAX_PAGES)
		{
			bool found = false;
			for(int i = 0; i < NUM_PAGES-1 && !found; i++)
			{
				if (m_Pages[i] >= 2)
				{
					m_Pages[i] -= 2;
					m_Pages[i+1] += 1;
					m_TotalAllocs--;
					if (m_Pages[i+1] > MaxPageCount[i+1])
					{
						AdjustToPageCountLimit();
					}
					found = true;
				}
			}
			if (!found)
			{
				m_Error = true;
				return;
			}
		}
	}

	void InsertAlloc(size_t allocSize)
	{
		Assertf(_IsPowerOfTwo((int)allocSize), "Alloc size %" SIZETFMT "d needs to be a power of 2", allocSize);

		int pageIdx = _CeilLog2((int)((allocSize * 16) / m_BasePageSize)); /* "*16" because idx 0 = HasTail16 */

		if (pageIdx >= NUM_PAGES)
		{
			m_Error = true;
			return;
		}

		m_Pages[pageIdx]++;
		m_TotalAllocs++;

		if (m_Pages[pageIdx] > MaxPageCount[pageIdx])
		{
			AdjustToPageCountLimit();
		}

		AdjustToTotalPageLimit();

	}

	datResourceInfo MakeDatResourceInfo()
	{
		datResourceInfo ret;
		sysMemSet(&ret, 0, sizeof(ret));

		// Bail out if we added too much memory
		if (m_Error || m_Pages[HEAD16] > MaxPageCount[HEAD16])
		{
			return ret;
		}

		size_t leafSize = g_rscVirtualLeafSize;
		while(leafSize < m_BasePageSize)
		{
			ret.Virtual.LeafShift++;
			leafSize <<= 1;
		}

		ret.Virtual.Head16Count = m_Pages[HEAD16];
		ret.Virtual.Head8Count = m_Pages[HEAD8];
		ret.Virtual.Head4Count = m_Pages[HEAD4];
		ret.Virtual.Head2Count = m_Pages[HEAD2];
		ret.Virtual.BaseCount = m_Pages[BASE];
		ret.Virtual.HasTail2 = (m_Pages[TAIL2] != 0);
		ret.Virtual.HasTail4 = (m_Pages[TAIL4] != 0);
		ret.Virtual.HasTail8 = (m_Pages[TAIL8] != 0);
		ret.Virtual.HasTail16 = (m_Pages[TAIL16] != 0);

		return ret;
	}
};

const u8 sfResourceInfo::MaxPageCount[NUM_PAGES] =
{
	1, // TAIL16
	1, // TAIL8
	1, // TAIL4,
	1, // TAIL2,
	(1 << RESOURCE_N_BITS) - 1, // BASE
	(1 << RESOURCE_2N_BITS) - 1, // HEAD2
	(1 << RESOURCE_4N_BITS) - 1, // HEAD4
	(1 << RESOURCE_8N_BITS) - 1, // HEAD8
	(1 << RESOURCE_16N_BITS) - 1, // HEAD16
};

// C++11 lambdas will be nice...
struct sfSortBySize { 
	bool operator()(const sfPreallocatedMemoryWrapper::ScaleformAllocStat& a, const sfPreallocatedMemoryWrapper::ScaleformAllocStat& b) {
		return a.m_Size < b.m_Size; 
	} 
};
struct sfOpenAlloc { size_t m_Size, m_SpaceRemaining; };

bool sfPreallocatedMemoryWrapper::ComputePreallocation(AllocsRequiredArray& allocsRequired, size_t lowestGranularity, datResourceInfo& outRsc, size_t& outGranularity)
{
	// Constraints:
	//		Max allocation must be at most 32x the granularity
	//		Chunk units: 1 1/16x, 1 1/8x, 1 1/4x, 1 1/2x, 127 1x, 63 2x, 15 4x, 3 8x, 1 16x
	//		Total chunks: 126
	//		Smallest allocation: 16k ?

	// Optimize:
	//		Minimize Total size of preallocation
	//		Maximize use of 16k or smaller pages
	//		Minimize granularity

	sfAssertf(allocsRequired.GetCount() < 32, "No more than 32 unique sizes allowed");

	if (allocsRequired.GetCount() == 0)
	{
		sfResourceInfo res;
		res.Reset();
		res.m_BasePageSize = lowestGranularity;
		outRsc = res.MakeDatResourceInfo();
		outGranularity = lowestGranularity;
		return true;
	}

	size_t maxGranularity = 128 * 1024; // arbitrary stopping point
	for(size_t currGranularity = lowestGranularity; currGranularity <= maxGranularity; currGranularity *= 2)
	{
		// In theory we could try different BasePageSizes here. But this should be good enough for now
		sfResourceInfo res;
		res.Reset();
		res.m_BasePageSize = currGranularity;

		// Overall plan:
		// Pull the biggest allocation off the list, look to see if we have a partial allocation that can be filled
		// If so: 
		//		fill the partial allocation, insert it into the chunk set as a complete chunk
		// Else:
		//		Create a new partial allocation, add this alloc
		//		Is this now a complete chunk?
		//		If so:
		//			Add to the chunk set


		// Make a list of all the allocations we need, sorted by size
		atFixedArray<ScaleformAllocStat, 32> sortedAllocs;
		sortedAllocs.insert(sortedAllocs.begin(), allocsRequired.begin(), allocsRequired.end());
		std::sort(sortedAllocs.begin(), sortedAllocs.end(), sfSortBySize());

		atFixedArray<sfOpenAlloc, 32> openAllocs;

		// This loop just takes the list of allocations that come from scaleform and tries to pack them into power-of-two sized
		// pages, by sorting them, maintaining a list of open (non-full) bins, and trying to fill open bins with the next largest alloc.
		while(sortedAllocs.GetCount())
		{
			// Try to find room for the largest remaining allocation
			ScaleformAllocStat& nextAlloc = sortedAllocs.Top();
			if (nextAlloc.m_Count == 0)
			{
				sortedAllocs.Pop();
				continue;
			}

			size_t nextAllocSize = nextAlloc.m_Size;
			nextAlloc.m_Count--;

			// Check the open list first
			bool foundSpaceOnOpenList = false;
			for(int i = 0; i < openAllocs.GetCount(); i++)
			{
				sfOpenAlloc& open = openAllocs[i];
				if (open.m_SpaceRemaining >= nextAllocSize)
				{
					open.m_SpaceRemaining -= nextAllocSize;
					foundSpaceOnOpenList = true;
					if (open.m_SpaceRemaining == 0)
					{
						res.InsertAlloc(open.m_Size);
						openAllocs.DeleteFast(i);
					}
					break;
				}
			}

			if (!foundSpaceOnOpenList)
			{
				if (_IsPowerOfTwo((int)nextAllocSize))
				{
					res.InsertAlloc(nextAllocSize);
				}
				else
				{
					sfOpenAlloc newOpen;
					newOpen.m_Size = (size_t)(1u << (u32)_CeilLog2((int)nextAllocSize));
					newOpen.m_SpaceRemaining = newOpen.m_Size - nextAllocSize;
					openAllocs.Push(newOpen);
				}
			}
		}

		// Insert all remaining open allocations
		for(int i = 0; i < openAllocs.GetCount(); i++)
		{
			res.InsertAlloc(openAllocs[i].m_Size);
		}

		datResourceInfo rsc = res.MakeDatResourceInfo();
		if (rsc.GetVirtualSize() > 0)
		{
			outRsc = rsc;
			outGranularity = currGranularity;
			return true;
		}
		// else there was an error, so try again with a larger granularity
	}

	// Couldn't find a solution
	sysMemSet(&outRsc, 0, sizeof(datResourceInfo));
	outGranularity = 0;
	return false;
}

#if __SF_STATS
void sfPreallocatedMemoryWrapper::LogAlloc(StatTable& table, size_t size)
{
	for(int i = 0; i < table.GetMaxCount(); i++)
	{
		if (table[i].m_Size == size)
		{
			table[i].m_Count++;
			table[i].m_HighCount = Max(table[i].m_HighCount, table[i].m_Count);
			return;
		}
		else if (table[i].m_Size == 0) {
			table[i].m_Size = (u16)size;
			table[i].m_Count = 1;
			table[i].m_HighCount = 1;
			return;
		}
	}
	sfWarningf("No room to track an allocation of size %" SIZETFMT "d", size);
}

void sfPreallocatedMemoryWrapper::LogFree(StatTable& table, size_t size)
{
	for(int i = 0; i < table.GetMaxCount(); i++)
	{
		if (table[i].m_Size == size)
		{
			table[i].m_Count--;
			return;
		}
	}
	sfWarningf("Couldn't find an alloc of size %" SIZETFMT "d to free", size);
}

bool sfPreallocatedMemoryWrapper::ComputeCurrentPreallocationNeeds(datResourceInfo& outRsc, size_t& outGranularity)
{
	AllocsRequiredArray allocsReq;
	for(int i =0; i < m_PreallocatedStats.GetMaxCount(); i++)
	{
		ChunkStat& stat = m_PreallocatedStats[i];
		if (stat.m_HighCount == 0)
		{
			continue;
		}

		bool found = false;
		for(int j = 0; j < allocsReq.GetCount(); j++)
		{
			if (allocsReq[j].m_Size == stat.m_Size * m_Granularity)
			{
				allocsReq[j].m_Count += stat.m_HighCount;
				found = true;
			}
		}
		if (!found)
		{
			ScaleformAllocStat& alloc = allocsReq.Append();
			alloc.m_Size = stat.m_Size * m_Granularity;
			alloc.m_Count = stat.m_HighCount;
		}
	}

	for(int i =0; i < m_ExtraStats.GetMaxCount(); i++)
	{
		ChunkStat& stat = m_ExtraStats[i];
		if (stat.m_HighCount == 0)
		{
			continue;
		}

		bool found = false;
		for(int j = 0; j < allocsReq.GetCount(); j++)
		{
			if (allocsReq[j].m_Size == stat.m_Size * m_Granularity)
			{
				allocsReq[j].m_Count += stat.m_HighCount;
				found = true;
			}
		}
		if (!found)
		{
			ScaleformAllocStat& alloc = allocsReq.Append();
			alloc.m_Size = stat.m_Size * m_Granularity;
			alloc.m_Count = stat.m_HighCount;
		}
	}

	return ComputePreallocation(allocsReq, m_Granularity, outRsc, outGranularity);
}

void sfPreallocatedMemoryWrapper::PrintDebugInfo()
{
	OUTPUT_ONLY(int unitsToKb = (int)m_Granularity / 1024);

	sfDisplayf("NAME: %s", m_ArenaName);

	sfDisplayf("\t----- PREALLOCATION CHUNKS ----");
	if (m_PreallocatedChunks.GetCount() > 0)
	{
		u32 chunkSize = (u32)m_PreallocatedChunks[0].m_Size;

		u32 totalUnits = 0;
		u32 unitsAllocated = 0;
		u32 unitsFree = 0;
		u32 numChunks = 0;

		for(int i = 0; i < m_PreallocatedChunks.GetCount() + 1; i++) // Intentionally run off the end of the array, so we can do one extra printout
		{
			if (i == m_PreallocatedChunks.GetCount() || m_PreallocatedChunks[i].m_Size != chunkSize)
			{
				sfDisplayf("\t%d %dk chunks. Total %d units (%" SIZETFMT "dk); Allocated %d units (%" SIZETFMT "dk); Free %d units (%" SIZETFMT "dk)",
					numChunks, chunkSize / 1024,
					totalUnits, totalUnits * m_Granularity / 1024,
					unitsAllocated, unitsAllocated * m_Granularity / 1024,
					unitsFree, unitsFree * m_Granularity / 1024);
				if (i == m_PreallocatedChunks.GetCount())
				{
					break;
				}
				chunkSize = (u32)m_PreallocatedChunks[i].m_Size;
				totalUnits = 0;
				unitsAllocated = 0;
				unitsFree = 0;
				numChunks = 0;
			}

			u32 unitsInChunk = m_PreallocatedChunks[i].GetNumUnits(*this);

			atFixedBitSet32 bits;
			bits.SetBits(m_PreallocatedChunks[i].m_UsedBitmap);
			u32 unitsFreeInChunk = bits.CountBits(false); // counts the number of off bits (i.e. free bits)

			numChunks++;
			totalUnits += unitsInChunk;
			unitsFree += unitsFreeInChunk;
			unitsAllocated += (unitsInChunk - unitsFreeInChunk);
		}
	}

	sfDisplayf("\t----- USED PREALLOCATED MEMORY -----");
	for(int i = 0; i < m_PreallocatedStats.GetMaxCount(); i++)
	{
		if (m_PreallocatedStats[i].m_Size > 0)
		{
			sfDisplayf("\t%dk chunks: Peak %d (%dk), Current %d (%dk)",
				m_PreallocatedStats[i].m_Size * unitsToKb,
				m_PreallocatedStats[i].m_HighCount, m_PreallocatedStats[i].m_HighCount * m_PreallocatedStats[i].m_Size * unitsToKb,
				m_PreallocatedStats[i].m_Count, m_PreallocatedStats[i].m_Count * m_PreallocatedStats[i].m_Size * unitsToKb);
		}
	}

	if (m_ExtraStats[0].m_Size > 0)
	{
		// if there are any extra chunks

		sfDisplayf("\t----- EXTRA CHUNKS -----");
		for(int i = 0; i < m_ExtraStats.GetMaxCount(); i++)
		{
			if (m_ExtraStats[i].m_Size > 0)
			{
				sfDisplayf("\t%dk chunks: Peak %d (%dk), Current %d (%dk)",
					m_ExtraStats[i].m_Size * unitsToKb,
					m_ExtraStats[i].m_HighCount, m_ExtraStats[i].m_HighCount * m_ExtraStats[i].m_Size * unitsToKb,
					m_ExtraStats[i].m_Count, m_ExtraStats[i].m_Count * m_ExtraStats[i].m_Size * unitsToKb);
			}
		}
	}

	sfDisplayf("\t----- COMBINED ALLOCATIONS -----");

	datResourceInfo rsc;
	size_t granularity = m_Granularity;
	size_t peakMemory = 0;

	AllocsRequiredArray allocsReq;
	for(int i =0; i < m_PreallocatedStats.GetMaxCount(); i++)
	{
		ChunkStat& stat = m_PreallocatedStats[i];
		if (stat.m_HighCount == 0)
		{
			continue;
		}
		peakMemory += stat.m_HighCount * stat.m_Size;

		bool found = false;
		for(int j = 0; j < allocsReq.GetCount(); j++)
		{
			if (allocsReq[j].m_Size == stat.m_Size * m_Granularity)
			{
				allocsReq[j].m_Count += stat.m_HighCount;
				found = true;
			}
		}
		if (!found)
		{
			ScaleformAllocStat& alloc = allocsReq.Append();
			alloc.m_Size = stat.m_Size * m_Granularity;
			alloc.m_Count = stat.m_HighCount;
		}
	}

	for(int i =0; i < m_ExtraStats.GetMaxCount(); i++)
	{
		ChunkStat& stat = m_ExtraStats[i];
		if (stat.m_HighCount == 0)
		{
			continue;
		}
		peakMemory += stat.m_HighCount * stat.m_Size;
		bool found = false;
		for(int j = 0; j < allocsReq.GetCount(); j++)
		{
			if (allocsReq[j].m_Size == stat.m_Size * m_Granularity)
			{
				allocsReq[j].m_Count += stat.m_HighCount;
				found = true;
			}
		}
		if (!found)
		{
			ScaleformAllocStat& alloc = allocsReq.Append();
			alloc.m_Size = stat.m_Size * m_Granularity;
			alloc.m_Count = stat.m_HighCount;
		}
	}

	sfDisplayf("\t\t<movie name=\"%s\" peakSize=\"%" SIZETFMT "d\" granularity=\"%" SIZETFMT "d\" sfalloc=\"true\">", 
		m_ArenaName, (peakMemory * unitsToKb), m_Granularity / 1024);
	for(int i = 0; i < allocsReq.GetCount(); i++)
	{
		sfDisplayf("\t\t\t<SFAlloc><Size value=\"%" SIZETFMT "d\"/><Count value=\"%" SIZETFMT "d\"/></SFAlloc>", allocsReq[i].m_Size / 1024, allocsReq[i].m_Count);
	}
	sfDisplayf("\t\t</movie>");


	sfDisplayf("\t----- ALLOCATION STRATEGY -----");

	if (ComputeCurrentPreallocationNeeds(rsc, granularity))
	{
		size_t baseK = rsc.GetVirtualChunkSize() / 1024;
		sfDisplayf("\t\tBaseSize: %" SIZETFMT "dk", granularity / 1024);
		if (rsc.Virtual.HasTail16) { sfDisplayf("\t\tHasTail16 (%" SIZETFMT "dk): true", baseK / 16); }
		if (rsc.Virtual.HasTail8) { sfDisplayf("\t\tHasTail8 (%" SIZETFMT "dk): true", baseK / 8); }
		if (rsc.Virtual.HasTail4) { sfDisplayf("\t\tHasTail4 (%" SIZETFMT "dk): true", baseK / 4); }
		if (rsc.Virtual.HasTail2) { sfDisplayf("\t\tHasTail2 (%" SIZETFMT "dk): true", baseK / 2); }
		if (rsc.Virtual.BaseCount > 0) { sfDisplayf("\t\tBase (%" SIZETFMT "dk): %d", baseK, rsc.Virtual.BaseCount); }
		if (rsc.Virtual.Head2Count > 0) { sfDisplayf("\t\tHead2Count (%" SIZETFMT "dk): %d", baseK * 2, rsc.Virtual.Head2Count); }
		if (rsc.Virtual.Head4Count > 0) { sfDisplayf("\t\tHead4Count (%" SIZETFMT "dk): %d", baseK * 4, rsc.Virtual.Head4Count); }
		if (rsc.Virtual.Head8Count > 0) { sfDisplayf("\t\tHead8Count (%" SIZETFMT "dk): %d", baseK * 8, rsc.Virtual.Head8Count); }
		if (rsc.Virtual.Head16Count > 0) { sfDisplayf("\t\tHead16Count (%" SIZETFMT "dk): %d", baseK * 16, rsc.Virtual.Head16Count); }
	}

	sfDisplayf("");
}

size_t sfPreallocatedMemoryWrapper::FindPreallocatedSize()
{
	size_t totalUnits = 0;
	for(int i = 0; i < m_PreallocatedChunks.GetCount(); i++)
	{
		totalUnits += m_PreallocatedChunks[i].GetNumUnits(*this);
	}
	return totalUnits * m_Granularity;
}

size_t sfPreallocatedMemoryWrapper::FindExtraAllocationSize()
{
	size_t totalUnits = 0;
	for(int i = 0; i < m_ExtraChunks.GetCount(); i++)
	{
		totalUnits += m_ExtraChunks[i].GetNumUnits(*this);
	}
	return totalUnits * m_Granularity;
}
#endif // __SF_STATS
 

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfFile
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

// This is a wrapper around a fiStream for scaleform.
// These functions don't generally do validity tests. 


const char* sfFile::GetFilePath()
{
	return m_Stream->GetName();
}

bool sfFile::IsValid()
{
	return m_Stream != NULL;
}

bool sfFile::IsWritable()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return true; // can't tell for an fiStream?
}

SInt sfFile::Tell()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Tell();
}

SInt64 sfFile::LTell()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Tell64();
}

SInt sfFile::GetLength()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Size();
}

SInt64 sfFile::LGetLength()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Size64();
}

SInt sfFile::GetErrorCode()
{
	return 0;
}

SInt sfFile::Write( const UByte* pbuffer, SInt numBytes )
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Write(pbuffer, numBytes);
}

SInt sfFile::Read( UByte* pbuffer, SInt numBytes )
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Read(pbuffer, numBytes);
}

SInt sfFile::SkipBytes( SInt numBytes )
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	int oldPos = m_Stream->Tell();
	int newPos = m_Stream->Seek(numBytes);
	if (newPos < 0) {
		return newPos;
	}
	return newPos - oldPos;
}

SInt sfFile::BytesAvailable()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Size() - m_Stream->Tell();
}

bool sfFile::Flush()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	m_Stream->Flush();
	return true;
}

SInt sfFile::Seek( SInt offset, SInt origin/* =Seek_Set */ )
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	switch (origin)
	{
	case Seek_Set: return m_Stream->Seek(offset);
	case Seek_End: return m_Stream->Seek(m_Stream->Size() - offset);
	case Seek_Cur: return m_Stream->Seek(m_Stream->Tell() + offset);
	}
	return -1;
}

SInt64 sfFile::LSeek( SInt64 offset, SInt origin/* =Seek_Set */ )
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	switch(origin)
	{
	case Seek_Set: return m_Stream->Seek64(offset);
	case Seek_End: return m_Stream->Seek64(m_Stream->Size64() - offset);
	case Seek_Cur: return m_Stream->Seek64(m_Stream->Tell64() + offset);
	}
	return -1;
}

bool sfFile::ChangeSize( SInt /*newSize*/ )
{
	return false;
}

SInt sfFile::CopyFromStream( GFile *pstream, SInt byteSize )
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	sfAssertf(pstream, "fiStream doesn't exist - did you open the file first?");
	// we might need to be smarter about this
	char* bytes = Alloca(char, byteSize);
	int bytesRead = pstream->Read((UByte*)bytes, byteSize);
	if (bytesRead >= 0)
	{
		return m_Stream->Write(bytes, bytesRead);
	}
	return -1;
}

bool sfFile::Close()
{
	sfAssertf(m_Stream, "fiStream doesn't exist - did you open the file first?");
	return m_Stream->Close() == 0;
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfFileOpener
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


class sfFileOpener : public GFxFileOpener
{
public:
	virtual GFile* OpenFile(const char* pUrl, SInt /*flags*/ = GFileConstants::Open_Read|GFileConstants::Open_Buffered, SInt /*mode*/=GFileConstants::Mode_ReadWrite)
	{
		sysMemUseMemoryBucket b(sfScaleformManager::sm_MemoryBucket);

		if (!pUrl || pUrl[0] == '\0')
		{
			sfErrorf("Empty or missing file name!");
			return NULL;
		}
		if (!strncmp("http:/", pUrl, 6))
		{
			sfWarningf("Flash movie contains reference to remote file %s", pUrl);
			return NULL;
		}
		sfDebugf1("opening %s", pUrl);
		sfFile* file = rage_new sfFile;
		file->Open(pUrl);
		if (file->IsValid())
		{
			return file;
		}
		else
		{
			file->Release();
			return NULL;
		}
	}
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfImageCreator
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT
const char* g_CreateInfoTypes[] = 
{
	"None",
	"Image",
	"File"
};

const char* g_ImageResourceUse[] = 
{
	"None",
	"Bitmap",
	"Gradient",
	"FontTexture",
	"SoundSample",
};
#endif

class sfImageCreator : public GFxImageCreator
{
public:
	sfImageCreator(bool keepImageBindData)
		: GFxImageCreator(keepImageBindData)
	{
	}

	virtual GImageInfoBase* CreateImage(const GFxImageCreateInfo &info)
	{
		sysMemUseMemoryBucket b(sfScaleformManager::sm_MemoryBucket);

		// Intercept any requests to open a bitmap. See if its in the texture dictionary already
		// or if we want to point to a render target.
		// TODO: Maybe we should handle ALL external file opening ourselves?
		if (info.Type == GFxImageCreateInfo::Input_File &&
			(info.Use == GFxResource::Use_Bitmap ||
			info.Use == GFxResource::Use_FontTexture ||
			info.Use == GFxResource::Use_Gradient))
		{
			const char* shortName = ASSET.FileName(info.pFileInfo->FileName.ToCStr());
			char baseName[SCALEFORM_MAX_PATH];
			ASSET.RemoveExtensionFromPath(baseName, SCALEFORM_MAX_PATH, shortName);

			// Check the current texture dictionaries for the texture.
			// This code is only ever called from the main thread, so pgDictionary<grcTexture>::GetCurrent() is safe here.
			pgDictionary<grcTexture>* texDict = pgDictionary<grcTexture>::GetCurrent();
			if (texDict)
			{
				grcTexture* nativeTex = texDict->Lookup(baseName);
				if (nativeTex)
				{
					// Make sure our renderer is initted - we need it to create sfTextures
					sfAssertf(info.pRenderConfig, "No renderer has been set up - do this before loading movies so we can load textures from resources");
					sfRendererBase* renderer = smart_cast<sfRendererBase*>(info.pRenderConfig->GetRenderer());
					sfAssertf(renderer, "No renderer has been set up - do this before loading movies so we can load textures from resources");
					sfTextureBase* tex = renderer->CreateTexture(nativeTex);
					if (tex)
					{
						// OK we found the grcTexture, we've created the sfTexture, so
						// build a new GImageInfo for it - no need to use the default image creator.
						GImageInfoBase* newInfo = rage_new GImageInfo(tex, info.pFileInfo->TargetWidth, info.pFileInfo->TargetHeight);
						tex->Release();
						return newInfo;
					}
				}
			}
		}
		if(Verifyf(info.Type != GFxImageCreateInfo::Input_File, "Scaleform movie failed to find texture %s", info.pFileInfo->FileName.ToCStr()))
		{
			return GFxImageCreator::CreateImage(info);
		}
		return NULL;
	}

}; // class sfImageCreator

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfLog
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

extern sysCriticalSectionToken diagLogfToken;

class sfLog : public GFxLog
{
protected:
	static const int CHANNEL_MESSAGE_SIZE = 1024;

	static void Prepend(const char* tag, char*& buffer, int& size)
	{
		safecpy(buffer, tag, size);
		int len = (int)strlen(tag);
		size -= len;
		buffer += len;
	}

public:
#if __NO_OUTPUT
	virtual void LogMessageVarg(GFxLogConstants::LogMessageType , const char* , va_list )
	{
	}
#else
	virtual void LogMessageVarg(GFxLogConstants::LogMessageType messageType, const char* fmt, va_list args)
	{
		u32 channel = messageType & GFxLogConstants::Log_Channel_Mask;
		u32 severity = messageType & ~GFxLogConstants::Log_Channel_Mask;

		diagSeverity rageSeverity = 
			(severity == GFxLogConstants::Log_MessageType_Error) ? DIAG_SEVERITY_ERROR :
			(severity == GFxLogConstants::Log_MessageType_Warning) ? DIAG_SEVERITY_WARNING :
			DIAG_SEVERITY_DISPLAY;

		SYS_CS_SYNC(diagLogfToken);
		char buffer[CHANNEL_MESSAGE_SIZE];
		char* bufferRemaining = buffer;
		int size=CHANNEL_MESSAGE_SIZE;
		switch(channel)
		{
		case GFxLogConstants::Log_Channel_General:	Prepend("[General] ",	bufferRemaining, size); break;
		case GFxLogConstants::Log_Channel_Script:	Prepend("[Script] ",	bufferRemaining, size); break;
		case GFxLogConstants::Log_Channel_Parse:	Prepend("[Parse] ",		bufferRemaining, size); break;
		case GFxLogConstants::Log_Channel_Action:	Prepend("[Action] ",	bufferRemaining, size); break;
		case GFxLogConstants::Log_Channel_Debug:	Prepend("[Debug] ",		bufferRemaining, size); break;
		}
		vformatf(bufferRemaining,size,fmt,args);
		diagLogf(Channel_Scaleform_Player,rageSeverity,"Scaleform Message Handler",atStringHash(buffer),"%s", buffer);
	}

	// Doesn't have anything to do with the log class per se, but put here because the code is 
	// basically the same.
	static void GCDECL DebugMessageVarg(GDebug::MessageType msgType, const char *fmt, va_list args)
	{
		diagSeverity rageSeverity = DIAG_SEVERITY_DISPLAY;
		switch(msgType)
		{
		case GDebug::Message_Note:		rageSeverity = DIAG_SEVERITY_DISPLAY;	break;
		case GDebug::Message_Warning:	rageSeverity = DIAG_SEVERITY_WARNING;	break;
		case GDebug::Message_Error:		rageSeverity = DIAG_SEVERITY_ERROR;		break;
		case GDebug::Message_Assert:	rageSeverity = DIAG_SEVERITY_ASSERT;	break;
		}

		SYS_CS_SYNC(diagLogfToken);
		char buffer[CHANNEL_MESSAGE_SIZE];
		int size=CHANNEL_MESSAGE_SIZE;
		vformatf(buffer,size,fmt,args);
		diagLogf(Channel_Scaleform_Player,rageSeverity,"Scaleform Debug Message Handler",atStringHash(buffer),"%s", buffer);
	}
#endif
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfMemoryTracker
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
#if RAGE_TRACK_SCALEFORM_ALLOCS != RAGE_TRACK_SCALEFORM_NONE
struct sfHeapTracer : public GMemoryHeap::HeapTracer
{
	virtual ~sfHeapTracer() {}
	
	virtual void OnCreateHeap(const GMemoryHeap* heap) 
	{
		GMemoryHeap::HeapInfo info;
		heap->GetHeapInfo(&info);

		sfDebugf1("Created scaleform heap %p %s (parent = %p, flags = %08x, gran = %dk, res = %dk, limit = %dk, arena = %d, id = %d", 
			heap, info.pName, info.pParent, info.Desc.Flags, info.Desc.Granularity/1024, info.Desc.Reserve/1024, info.Desc.Limit/1024, info.Desc.Arena, info.Desc.HeapId );
	}
	
	virtual void OnDestroyHeap(const GMemoryHeap* heap) 
	{
		sfDebugf1("Destroyed scaleform heap %p", heap);	
	}

#if RAGE_TRACK_SCALEFORM_ALLOCS == RAGE_TRACK_SCALEFORM_ALL
	u32 m_FakeHeapOffset;
	
	virtual void OnAlloc(const GMemoryHeap* /*heap*/, UPInt size, UPInt /*align*/, unsigned /*sid*/, const void* ptr) 
	{
		if (::rage::diagTracker::GetCurrent()) {
			::rage::diagTracker::GetCurrent()->Tally((char*)ptr + m_FakeHeapOffset, size, 0);
		}
	}

	virtual void OnRealloc(const GMemoryHeap* /*heap*/, const void* oldPtr, UPInt newSize, const void* newPtr) 
	{
		if (::rage::diagTracker::GetCurrent()) {
			::rage::diagTracker::GetCurrent()->UnTally((char*)oldPtr + m_FakeHeapOffset, 0);
			::rage::diagTracker::GetCurrent()->Tally((char*)newPtr + m_FakeHeapOffset, newSize, 0);
		}
	}

	virtual void OnFree(const GMemoryHeap* /*heap*/, const void* ptr) 
	{
		if (::rage::diagTracker::GetCurrent()) {
			::rage::diagTracker::GetCurrent()->UnTally((char*)ptr + m_FakeHeapOffset, 0);
		}
	}

	static void InitClass(u32 heapOffset = 0x80000000)
	{
		sm_TracerInstance.m_FakeHeapOffset = heapOffset;
		GMemory::GetGlobalHeap()->SetTracer(&sfHeapTracer::sm_TracerInstance);
		if (::rage::diagTracker::GetCurrent()) {
			sysMemAllocator* rscVirtual = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
			if (rscVirtual && sysMemVisualize::GetInstance().HasUI())
			{
				::rage::diagTracker::GetCurrent()->InitHeap("Scaleform Internal", (char*)rscVirtual->GetHeapBase() + sm_TracerInstance.m_FakeHeapOffset, rscVirtual->GetHeapSize());
			}
		}
	}
#else
	virtual void OnAlloc(const GMemoryHeap* /*heap*/, UPInt /*size*/, UPInt /*align*/, unsigned /*sid*/, const void* /*ptr*/) {}
	virtual void OnRealloc(const GMemoryHeap* /*heap*/, const void* /*oldPtr*/, UPInt /*newSize*/, const void* /*newPtr*/) {}
	virtual void OnFree(const GMemoryHeap* /*heap*/, const void* /*ptr*/) {}
	static void InitClass()
	{
		GMemory::GetGlobalHeap()->SetTracer(&sfHeapTracer::sm_TracerInstance);
	}
#endif

	static sfHeapTracer sm_TracerInstance;
};

sfHeapTracer sfHeapTracer::sm_TracerInstance;

#endif

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfFSCommandHandler
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


class sfFSCommandHandler : public GFxFSCommandHandler
{
public:
	virtual void Callback(GFxMovieView* , const char* OUTPUT_ONLY(pcommand), const char* OUTPUT_ONLY(parg))
	{
		sfErrorf("fscommand(\"%s\", \"%s\") no longer supported. Use ExternalInterface instead", pcommand, parg);
	}
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfScaleformFunctionHandler
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


void sfScaleformFunctionHandler::AddMethodToAsObject(GFxMovieView& movie, GFxValue& asClassPrototype, const char* name, int methodId)
{
	GFxValue functionObject;
	movie.CreateFunction(&functionObject, this, (void*)(ptrdiff_t)methodId);
	asClassPrototype.SetMember(name, functionObject);
}

GFxValue sfScaleformFunctionHandler::BuildActionscriptObject(GFxMovieView& movie, GFxValue& originalClass)
{
	// An AS2.0 'class' looks kind of odd, imo.
	// A class is really just a function object that wraps the constructor function. And since function objects
	// are like any other objects, they can have properties. In the case of a class, there is a .prototype property
	// that contains another object. This object contains all of the instance methods. All of the static methods
	// get attached as properties of the constructor function object.
	//
	// Side note not important here but it took me a little while to figure out... 
	//   Then when any instance of the class is created, AS creates a new object with a .__proto__ property. The
	//	 __proto__ points to the same thing that the constructor's .prototype points to.
	//   So I /think/ when it's time to make an instance method call, it looks like this:
	//	   foo.method()
	//		1) does foo have a .method property
	//			if so, call foo.method()
	//		2) does foo have a .__proto__?
	//			2a) does foo.__proto__ have a .method property?
	//				if so, call foo.__proto__.method()
	//			2b) does foo.__proto__ have a .prototype property (i.e. a base class)
	//				if so, call foo.__proto__.prototype.method()
	//			... 

	// Create the function object that calls the constructor function
	GFxValue constructorFnObj;
	movie.CreateFunction(&constructorFnObj, this, (void*)CONSTRUCTOR_ID);

	AddStaticMethods(movie, constructorFnObj, originalClass);

	// Add a .prototype property to hold the instance methods
	GFxValue classProto;
	movie.CreateObject(&classProto);
	constructorFnObj.SetMember("prototype", classProto);

	// Add all the instance methods to the class prototype
	AddInstanceMethods(movie, classProto, originalClass);

	return constructorFnObj;
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfScaleformMovieView
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

sfScaleformMovieView::sfScaleformMovieView()
: m_Movie(NULL)
{
	SetIsActive(true);
	SetIsVisible(true);
	m_PadMask.SetAll();
}

sfScaleformMovieView::~sfScaleformMovieView()
{
	sfAssertf(!m_MovieListNode.m_next && !m_MovieListNode.m_prev && !m_MovieListNode.m_owner,
		"Destroying a movie view that's still in the master list!");

	for(int i = 0; i < m_FunctionHandlers.GetCount(); i++)
	{
		m_FunctionHandlers[i]->OnDelete(*this);
		m_FunctionHandlers[i]->Release();
	}

	m_MovieView->Release(); // RefCount was 1 on creation, 2 when we assigned it to a GPtr.
	sfAssertf(m_MovieView->GetRefCount() == 1, "MovieView had %d outstanding references, and will leak!", m_MovieView->GetRefCount()-1);
	m_MovieView.Clear();
}

void sfScaleformMovieView::Update(float time /* = TIME.GetSeconds() */)
{
#if __SF_STATS
	sysTimer t;
#endif

	PF_FUNC(MovieUpdate);
	//DIAG_CONTEXT_MESSAGE("Updating Flash Movie %s", GetMovie()->GetMovie().GetFileURL());

	for(int i = 0; i < m_FunctionHandlers.GetCount(); i++)
	{
		m_FunctionHandlers[i]->OnUpdate(*this, time);
	}

	GetMovieView().Advance(time);

#if __SF_STATS
	m_StatUpdate.Set(t.GetMsTime());

	GStatBag stats;
	m_MovieView->GetStats(&stats, false);
	stats.UpdateGroups();

	GStatInfo si;
	if (stats.GetStat(&si, GFxStatMV_ScriptCommunication_Tks)) 
	{
		m_StatGame.Set( (float)si.ToTimerStat()->GetTicks() / 1000.0f );
	}

	if (stats.GetStat(&si, GFxStatMV_Action_Tks))
	{
		m_StatUpdateAs.Set( (float)si.ToTimerStat()->GetTicks() / 1000.0f );
	}

#endif
}

// NOTE: required for streaming iterators
#if __DEV
PARAM(noScaleformRender,"disable sfScaleformMovieView::Draw");
#endif // __DEV

void sfScaleformMovieView::Draw()
{
	PF_AUTO_PUSH_TIMEBAR("sfScaleformMovieView - Draw()");

#if __SF_STATS
	sysTimer t;
	GRenderer::Stats startStats;
	m_MovieView->GetRenderer()->GetRenderStats(&startStats);
#endif

	PF_FUNC(MovieDraw);
	//DIAG_CONTEXT_MESSAGE("Drawing Flash Movie %s", GetMovie()->GetMovie().GetFileURL());
	// do a quick bounding box check
	GViewport vp;
	GetMovieView().GetViewport(&vp);
	if (vp.Left >= vp.BufferWidth || vp.Left + vp.Width <= 0 || vp.Width <= 0)
	{
		return;
	}
	if (vp.Top >= vp.BufferHeight || vp.Top + vp.Height <= 0 || vp.Height <= 0)
	{
		return;
	}

#if __DEV
	if (PARAM_noScaleformRender.Get())
	{
		return;
	}
#endif // __DEV

	GetMovieView().Display();
#if __SF_STATS
	m_StatDraw.Set(t.GetMsTime());

	GStatBag stats;
	m_MovieView->GetStats(&stats, false); 
	stats.UpdateGroups();

	GStatInfo si;
	if (stats.GetStat(&si, GFxStatMV_Tessellate_Tks)) 
	{
		m_StatTess.Set( (float)si.ToTimerStat()->GetTicks() / 1000.0f );
	}

	GRenderer::Stats endStats;
	m_MovieView->GetRenderer()->GetRenderStats(&endStats);

	m_StatPrims.Set((float)(endStats.Primitives - startStats.Primitives));
	m_StatTris.Set((float)(endStats.Triangles - startStats.Triangles));

#endif
}

void sfScaleformMovieView::DrawInWorldSpace(sfScaleformManager& mgr)
{
	PF_PUSH_TIMEBAR("Scaleform - DrawInWorldSpace");

#if __SF_STATS
	sysTimer t;
	GRenderer::Stats startStats;
	m_MovieView->GetRenderer()->GetRenderStats(&startStats);
#endif

	PF_FUNC(MovieDraw);
	//DIAG_CONTEXT_MESSAGE("Drawing Flash Movie %s", GetMovie()->GetMovie().GetFileURL());
	// The 'view' matrix really need to be the concatenation of 3 matrices;
	// This is the original matrix stack:
	// "Final transform is (proj * view * user * world * m) * v"
	// What we want is:
	//   (proj * (view * rageWorld * normalizedSize) * user * world * m) * v
	//  proj = normal rage projection matrix
	//	view = normal rage view matrix
	//	rageWorld = world space matrix for the small (1x1 sized) matrix
	//	normalizedSize = sf outputs in TWIPS - convert to more reasonable size (1x1)
	//  user * world * m = the 2d user matrix, 3d internal-to-scaleform world matrix, and the per-object matrix

	grcViewport& vp = *grcViewport::GetCurrent();

	const Mat44V& projection = vp.GetProjection();

	// Make a copy of view, so we can modify it
	Mat44V view44 = vp.GetViewMtx();

	Mat34V normalizedSize;
	Mat34VFromScale(normalizedSize, m_ScaleToUnitLengthVector);

	Mat34V rageWorld = vp.GetWorldMtx();


	Mat34V worldTimesSize;
	Transform(worldTimesSize, rageWorld, normalizedSize);

	Mat34V finalView;
	Transform(finalView, reinterpret_cast<Mat34V&>(view44), worldTimesSize);

	view44.GetCol0Ref().SetXYZ(finalView.GetCol0());
	view44.GetCol1Ref().SetXYZ(finalView.GetCol1());
	view44.GetCol2Ref().SetXYZ(finalView.GetCol2());
	view44.GetCol3Ref().SetXYZ(finalView.GetCol3());

	// Fortunately a rage matrix and a scaleform matrix have the same memory layout!
	GetMovieView().SetPerspective3D(reinterpret_cast<const GMatrix3D&>(projection));
	GetMovieView().SetView3D(reinterpret_cast<const GMatrix3D&>(view44));

	mgr.GetRageRenderer().RenderNextMovieInWorldSpace();

	// No bounding box check here.
	GetMovieView().Display();
#if __SF_STATS
	m_StatDraw.Set(t.GetMsTime());

	GStatBag stats;
	m_MovieView->GetStats(&stats, false);
	stats.UpdateGroups();

	GStatInfo si;
	if (stats.GetStat(&si, GFxStatMV_Tessellate_Tks)) 
	{
		m_StatTess.Set( (float)si.ToTimerStat()->GetTicks() / 1000.0f );
	}

	GRenderer::Stats endStats;
	m_MovieView->GetRenderer()->GetRenderStats(&endStats);

	m_StatPrims.Set((float)(endStats.Primitives - startStats.Primitives));
	m_StatTris.Set((float)(endStats.Triangles - startStats.Triangles));
#endif

	PF_POP_TIMEBAR();
}

void sfScaleformMovieView::RecomputeScaleToUnitLengthVector(float width, float height)
{
	m_ScaleToUnitLengthVector = Vec3V(1.0f / PixelsToTwips(width), 1.0f / PixelsToTwips(height), 1.0f);
}

void sfScaleformMovieView::ApplyInput( sfInput& input )
{
	PF_FUNC(ApplyInput);
	if (GetFlag(FLAG_MOUSE_INPUT))
	{
		input.SendMouseEvents(this);
	}
	if (GetFlag(FLAG_KEYBOARD_INPUT))
	{
		input.SendKeyboardEvents(this);
	}
	if (GetFlag(FLAG_PAD_INPUT))
	{
		input.SendPadEvents(this);
	}
}

void sfScaleformMovieView::RegisterFunctionHandler(sfScaleformFunctionHandler* fn)
{
	m_FunctionHandlers.PushAndGrow(fn, 2);
	fn->AddRef();
}

#if __SF_STATS_WITHOUT_AMP
void sfScaleformMovieView::ResetStats()
{
	GStatBag stats;
	GetMovieView().GetStats(&stats, true);
}
#endif

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfScaleformMovie
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

rage::sfScaleformMovie::sfScaleformMovie()
: m_Textures(NULL)
, m_OwnsTextures(false)
{
}

rage::sfScaleformMovie::~sfScaleformMovie()
{
	sfAssertf(!m_MovieListNode.m_next && !m_MovieListNode.m_prev && !m_MovieListNode.m_owner,
		"Destroying a movie that's still in the master list!");

	if (m_Movie)
	{
		sfAssertf(m_Movie->GetRefCount() == 1, "Movie had %d outstanding references and will leak", m_Movie->GetRefCount() - 1);
		m_Movie.Clear();
	}

	if (m_Textures && m_OwnsTextures)
	{
		m_Textures->Release();
	}
}



#if USE_SCALEFORM_AMP
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//   sfAppControlCallback
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
class sfAppControlCallback : public GFxAmpAppControlInterface
{
public:
	sfAppControlCallback(sfScaleformManager* manager)
		: m_Manager(manager)
	{
	}

	// TODO: What thread do these calls come in on? Do they need to be queued and processed before update?
	// TODO: Also break these into seperate functions so we could also hook them to widgets or script commands or whatnot?
	bool HandleAmpRequest(const GFxAmpMessageAppControl* message)
	{
		if (message->IsCurveToleranceDown())
		{
			m_Manager->m_CurveTolerance -= 0.5f;
			m_Manager->UpdateCurveTolerance();
		}
		if (message->IsCurveToleranceUp())
		{
			m_Manager->m_CurveTolerance += 0.5f;
			m_Manager->UpdateCurveTolerance();
		}
		if (message->IsToggleAaMode())
		{
			u32 renderFlags = m_Manager->m_RenderConfig->GetRenderFlags();
			renderFlags ^= GFxRenderConfig::RF_EdgeAA;
			m_Manager->m_RenderConfig->SetRenderFlags(renderFlags);
		}
		if (message->IsToggleStrokeType())
		{
			u32 renderFlags = m_Manager->m_RenderConfig->GetRenderFlags();
			u32 stroke = renderFlags & GFxRenderConfig::RF_StrokeMask;

			// 0 out the stroke bits
			renderFlags &= ~GFxRenderConfig::RF_StrokeMask;

			switch(stroke)
			{
			case GFxRenderConfig::RF_StrokeHairline:
				stroke = GFxRenderConfig::RF_StrokeNormal; break;
			case GFxRenderConfig::RF_StrokeNormal:
				stroke = GFxRenderConfig::RF_StrokeCorrect; break;
			case GFxRenderConfig::RF_StrokeCorrect:
				stroke = GFxRenderConfig::RF_StrokeHairline; break;
			}
			renderFlags |= stroke;

			m_Manager->m_RenderConfig->SetRenderFlags(renderFlags);
		}

		if (message->IsToggleOverdraw())
		{
			if (m_Manager->m_Overdraw)
			{
				m_Manager->m_RendererWrapper->DisableProfile();
			}
			else
			{
				m_Manager->m_RendererWrapper->ProfileCounters(
					(UInt64(GAmpRenderer::Profile_Fill   | 16) << GAmpRenderer::Channel_Green) |
					(UInt64(GAmpRenderer::Profile_Mask   | 32) << GAmpRenderer::Channel_Red) |
					(UInt64(GAmpRenderer::Profile_Filter | 32) << GAmpRenderer::Channel_Blue)
					);
			}
			m_Manager->m_Overdraw = !m_Manager->m_Overdraw;
		}

		if (message->IsToggleWireframe())
		{
			m_Manager->GetRageRenderer().SetWireframeMode(!m_Manager->GetRageRenderer().GetWireframeMode());
		}

		return true;
	}

	sfScaleformManager* m_Manager;
};
#endif // #if USE_SCALEFORM_AMP

//GPtr<GFxPreprocessParams> g_preprocessParams;

void InitScaleformThread(void* cbData)
{
	sysMemAllocator* mainAlloc = reinterpret_cast<sysMemAllocator*>(cbData);
	sysMemAllocator::SetCurrent(*mainAlloc);
	sysMemAllocator::SetMaster(*mainAlloc);
	sysMemAllocator::SetContainer(*mainAlloc);

	// Get off the render thread!
#if __XENON
	XSetThreadProcessor(GetCurrentThread(), sfScaleformManager::sm_ChildThreadProcessor);
#endif
}

const char* rage::sfScaleformManager::GetTypeName(GFxValue::ValueType val)
{
	switch(val)
	{
	case GFxValue::VT_Undefined: return "Undefined";
	case GFxValue::VT_Null: return "Null";
	case GFxValue::VT_Boolean: return "Boolean";
	case GFxValue::VT_Number: return "Number";
	case GFxValue::VT_String: return "String";
	case GFxValue::VT_StringW: return "StringW";
	case GFxValue::VT_Object: return "Object";
	case GFxValue::VT_Array: return "Array";
	case GFxValue::VT_DisplayObject: return "DisplayObject";
	default:
		return "<unknown type>";
	}
}

void rage::sfScaleformManager::Init(sysMemAllocator* allocator)
{
	sysMemUseMemoryBucket b(sfScaleformManager::sm_MemoryBucket);

	m_Input.Init();

#if !__NO_OUTPUT
	GDebug::SetMessageHandler(sfLog::DebugMessageVarg);
#endif

	// try a fixed size buffer
//	void* mem = rage_new char[6*1024*1024];
//	m_MemoryWrapper = rage_new GSysAllocStatic(mem, 6*1024*1024);
							
	m_MeshCacheResetRequested = 0;

	m_MemoryWrapper = m_ResourceMemoryWrapper = rage_new sfRageMemoryWrapper(allocator ? allocator : &sysMemAllocator::GetCurrent(), MEMTYPE_RESOURCE_VIRTUAL);

	m_NextArenaId = 1; // let 0 mean 'no arena'

	GThread::EngineInitCallbackData = &sysMemAllocator::GetMaster();
	GThread::EngineInitCallbackFn = &InitScaleformThread;

	m_System = rage_new GFxSystem(m_MemoryWrapper);

#if RAGE_TRACK_SCALEFORM_ALLOCS
	// Init order for this is kind of weird. Init this after the root heap is created, but then we'll miss the first
	// OnCreateHeap call (which I actually had to fix scaleform to handle)
	sfHeapTracer::InitClass();
#endif

	m_FileOpener = *new sfFileOpener;

	m_Loader = rage_new GFxLoader(m_FileOpener);
	m_Loader->GetMeshCacheManager()->SetMeshCacheMemLimit(MESH_CACHE_MEM_LIMIT);
	m_Loader->GetMeshCacheManager()->SetRenderGenMemLimit(RENDERGEN_MEM_LIMIT);

	m_FSCommandHandler = *new sfFSCommandHandler;
	m_Loader->SetFSCommandHandler(m_FSCommandHandler);

	m_ImageCreator = *new sfImageCreator(false); // false = don't keep image creation info in memory after loading
	m_Loader->SetImageCreator(m_ImageCreator);

	m_Log = *new sfLog;
	m_Loader->SetLog(m_Log);

//	g_preprocessParams = *new GFxPreprocessParams(1.0f);
//	m_Loader->SetPreprocessParams(g_preprocessParams);

	// Create renderer.
	if (!(m_Renderer = *sfRendererBase::CreateRenderer()))
	{
		Errorf("Couldn't create renderer");
		return;
	}
	// No SetDependentVideoMode a.t.m.

	// Set renderer on loader so that it is also applied to all children.
	m_RenderConfig = *new GFxRenderConfig(m_Renderer, GFxRenderConfig::RF_EdgeAA);

#if USE_SCALEFORM_AMP
	if (PARAM_noscaleformamp.Get())
	{
		GFxAmpServer::GetInstance().SetState(Amp_Disabled, true);
	}
	else
	{
		m_RendererWrapper = sfRendererBase::CreateAmpRendererWrapper(m_Renderer);
		m_RenderConfig->SetRenderer(m_RendererWrapper);
		GFxAmpServer::GetInstance().AddAmpRenderer(m_RendererWrapper);
		u32 ampHeapLimit = 256;
		GFxAmpServer::GetInstance().SetHeapLimit(ampHeapLimit * 1024); // originally 1mb
		sfDebugf2("AMP heap set to %udk", ampHeapLimit);

		GFxAmpMessageAppControl caps;
		caps.SetCurveToleranceDown(true);
		caps.SetCurveToleranceUp(true);
		caps.SetToggleAaMode(true);
		caps.SetToggleAmpRecording(true);
		caps.SetToggleInstructionProfile(true);
		caps.SetToggleOverdraw(true);
		caps.SetToggleStrokeType(true);
		caps.SetForceInstructionProfile(true);
		caps.SetToggleWireframe(true);

		m_AppControlCallback = rage_new sfAppControlCallback(this);

		GFxAmpServer::GetInstance().SetAppControlCaps(&caps);
		GFxAmpServer::GetInstance().SetAppControlCallback(m_AppControlCallback);

		m_Overdraw = false;
	}
#endif

	m_RenderConfig->SetMaxCurvePixelError(3.0f);

#if __BANK
	m_CurveTolerance = m_RenderConfig->GetMaxCurvePixelError();
#endif

	m_Loader->SetRenderConfig(m_RenderConfig);

	if (PARAM_scaleform_compactfonts.Get())
	{
		int fontSettings[2];
		fontSettings[0] = 256;
		fontSettings[1] = 1;
		PARAM_scaleform_compactfonts.GetArray(&fontSettings[0], 2);
		m_FontCompactorParams = *new GFxFontCompactorParams(fontSettings[0], fontSettings[1] != 0); 
		m_Loader->SetFontCompactorParams(m_FontCompactorParams);
	}


	// Now that the renderer is all set up, init the font cache textures (do this early so we know up front how
	// much memory we need)
	GFxFontCacheManager::TextureConfig textureConfig;
	m_Loader->GetFontCacheManager()->GetTextureConfig(&textureConfig);
	int maxSlotHeight = 128;

	PARAM_scaleform_maxslotheight.Get(maxSlotHeight);

	textureConfig.MaxSlotHeight = maxSlotHeight;
	m_Loader->GetFontCacheManager()->SetTextureConfig(textureConfig);


	m_Loader->GetFontCacheManager()->InitTextures(m_RenderConfig->GetRenderer());

	// Create translator
//	m_Translator = *new sfTranslator;
//	m_Loader->SetTranslator(m_Translator);
}

void rage::sfScaleformManager::CreateResourceArena(class sysMemAllocator* allocator)
{
	FastAssert(!m_ResourceMemoryWrapper);
	m_ResourceMemoryWrapper = rage_new sfRageMemoryWrapper(allocator, MEMTYPE_RESOURCE_VIRTUAL);
	GMemory::CreateArena(1, m_ResourceMemoryWrapper);
}

void rage::sfScaleformManager::DestroyResourceArena()
{
	if (m_ResourceMemoryWrapper)
	{
		GMemory::DestroyArena(1);

		delete m_ResourceMemoryWrapper;
		m_ResourceMemoryWrapper = NULL;
	}
}

#if __SF_STATS
void sfScaleformManager::PrintStats(u32 memReportType)
{
	sfDebugf1("Mesh cache - current size %" SIZETFMT "dk, limit %" SIZETFMT "dk", m_Loader->GetMeshCacheManager()->GetNumBytes() / 1024, m_Loader->GetMeshCacheManager()->GetMeshCacheMemLimit() / 1024);
	sfDebugf1("Font vector cache - max glyphs %d", m_Loader->GetFontCacheManager()->GetMaxVectorCacheSize());

	GMemory::GetGlobalHeap()->MemReport(m_Log, (GMemoryHeap::MemReportType)memReportType);
}

void sfScaleformManager::PrintArenaStats()
{
	for(int i = 0; i < m_MemoryArenas.GetCount(); i++)
	{
		sfDisplayf("Arena index = %d, id = %d:", i, m_MemoryArenas[i].m_Id);
		if (m_MemoryArenas[i].m_Allocator)
		{
			m_MemoryArenas[i].m_Allocator->PrintDebugInfo();
		}
		else
		{
			sfDisplayf("\t(null)");
		}
	}
}
#endif // __SF_STATS


#if !__NO_OUTPUT
rage::sfScaleformManager::AutoSetCurrMovieName::AutoSetCurrMovieName( const char* OUTPUT_ONLY(movie) )
{
	const char* shortName = strrchr(movie, ':');
	shortName = shortName ? shortName + 1 : movie;
	sm_DebugOutputCurrMovieName = shortName;
}
#endif

#if __BANK
namespace
{
	/* void MemReport(GFxLog* log, GMemoryHeap::MemReportType type)
	{
		GMemory::GetGlobalHeap()->MemReport(log, type);
	} */

#if USE_SCALEFORM_AMP
	void ToggleWireframe(sfScaleformManager* mgr)
	{
		mgr->GetRageRenderer().SetWireframeMode(!mgr->GetRageRenderer().GetWireframeMode());
	}
#endif
}


void rage::sfScaleformManager::AddWidgets(bkGroup& group)
{
	bkGroup* debugWidgets = group.AddGroup("Debug", false);

	debugWidgets->AddButton("Force GC", datCallback(MFA(rage::sfScaleformManager::ForceGC), this));
	debugWidgets->AddButton("Reset Mesh Cache", datCallback(MFA(rage::sfScaleformManager::ResetMeshCache), this));
	debugWidgets->AddButton("Reset Font Cache", datCallback(MFA(rage::sfScaleformManager::ResetFontCache), this));
#if USE_SCALEFORM_AMP
	debugWidgets->AddButton("Toggle Wireframe", datCallback(CFA1(ToggleWireframe), this));
#endif

	debugWidgets->AddSlider("Curve Tolerance", &m_CurveTolerance, 0.1f, 10.0f, 0.1f, datCallback(MFA(rage::sfScaleformManager::UpdateCurveTolerance), this));

#if __SF_STATS
	debugWidgets->AddButton("Print Stats", datCallback(MFA(rage::sfScaleformManager::PrintArenaStats), this));

	bkGroup* memReports = group.AddGroup("Memory Reports", false);
	memReports->AddButton("Brief", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportBrief));
	memReports->AddButton("Summary", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportSummary));
	memReports->AddButton("Medium", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportMedium));
	memReports->AddButton("Full", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportFull));
	memReports->AddButton("Simple", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportSimple));
	memReports->AddButton("Simple Brief", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportSimpleBrief));
	memReports->AddButton("File Summary", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportFileSummary));
	memReports->AddButton("Heaps Only", datCallback(MFA1(rage::sfScaleformManager::PrintStats), this, (void*)GMemoryHeap::MemReportHeapsOnly));
#endif
}

void rage::sfScaleformManager::UpdateCurveTolerance()
{
	m_CurveTolerance = Clamp(m_CurveTolerance, 0.1f, 10.0f);
	m_RenderConfig->SetMaxCurvePixelError(m_CurveTolerance);
}

// Force all movie views to collect garbage
void rage::sfScaleformManager::ForceGC()
{
	sysCriticalSection movieList(m_MovieListCS);
	for(MovieViewList::iterator i = m_MovieViewList.begin(); i != m_MovieViewList.end(); ++i)
	{
		(*i)->GetMovieView().ForceCollectGarbage();	
	}
}
#endif

void rage::sfScaleformManager::InitExternalInterface(GFxExternalInterface& inter)
{
	m_ExternalInterface = inter;
	m_Loader->SetExternalInterface(m_ExternalInterface);
}

void rage::sfScaleformManager::Shutdown()
{
	m_Renderer->SetUseDeletionQueue(false);

	// Delete any remaining movies and movieviews
	sysCriticalSection movieList(m_MovieListCS);
	if (!m_MovieViewList.empty())
	{
		sfWarningf("There are undeleted movie views, deleting now...");
		while(!m_MovieViewList.empty())
		{
			DeleteMovieView(m_MovieViewList.back());
		}
	}

	if (!m_MovieList.empty())
	{
		sfWarningf("There are undeleted movies, deleting now...");
		while(!m_MovieList.empty())
		{
			sfWarningf("    Deleting %s", m_MovieList.back()->GetMovie().GetFileURL());
			DeleteMovie(m_MovieList.back());
		}
	}

	for(int i = 0; i < m_MemoryArenas.GetCount(); i++)
	{
		if (m_MemoryArenas[i].m_Allocator)
		{
			sfWarningf("Allocator index = %d, id = %d was not shut down!", i, m_MemoryArenas[i].m_Id);
			DestroyPreallocatedMemoryArena(m_MemoryArenas[i].m_Id);
		}
	}

	// Normally GPtr destructor would do this, but normally the GPtrs wouldn't be owned by the same object
	// that owns the GFxSystem
	m_RenderConfig.Clear();
	m_Log.Clear();
	m_FSCommandHandler.Clear();
	m_ImageCreator.Clear();
	m_FileOpener.Clear();
	m_ExternalInterface.Clear();
//	m_Translator.Clear();

	ShutdownDrawTextManager();

	// delete all the bits of the loader that rage allocated
	m_Loader->SetLog(NULL);
	m_Loader->SetFSCommandHandler(NULL);
	m_Loader->SetImageCreator(NULL);
	m_Loader->SetFileOpener(NULL);
	m_Loader->SetExternalInterface(NULL);
	m_Loader->SetRenderConfig((GFxRenderConfig*)NULL);
	delete m_Loader;

	m_Renderer.Clear();

	delete m_System;

	delete m_MemoryWrapper;
}

void rage::sfScaleformManager::InitDrawTextManager(sfScaleformMovie& fontMovie)
{
	sfAssertf(!m_DrawTextManager, "You've already initted the drawtextmanager");
	m_DrawTextManager = *new GFxDrawTextManager(&fontMovie.GetMovie(), GMemory::GetGlobalHeap());
}

GFxDrawTextManager* rage::sfScaleformManager::GetDrawTextManager()
{
	return m_DrawTextManager.GetPtr();
}

void rage::sfScaleformManager::ShutdownDrawTextManager()
{
	m_DrawTextManager.Clear();
}

sfScaleformMovie* rage::sfScaleformManager::LoadMovie( const char* filename, bool bHasTxd, pgDictionary<grcTexture>* pTxd, u32 arenaId)
{
#if RAGE_TRACK_SCALEFORM_ALLOCS
	sysMemUseMemoryBucket b(arenaId + 1024);
#else
	sysMemUseMemoryBucket b(sfScaleformManager::sm_MemoryBucket);
#endif
	sfScaleformMovie* movie = rage_new sfScaleformMovie;

	pgDictionary<grcTexture>* texDict = pTxd;

	if (bHasTxd)
	{
		if (!texDict)
		{
			// Load a texture dictionary (if one exists)
			char texDictName[SCALEFORM_MAX_PATH];
			ASSET.RemoveExtensionFromPath(texDictName, SCALEFORM_MAX_PATH, filename);
			pgRscBuilder::ConstructName(texDictName, SCALEFORM_MAX_PATH, texDictName, "#td", grcTexture::RORC_VERSION);
			if (ASSET.Exists(texDictName, ""))
			{
				pgRscBuilder::Load(texDict, texDictName, "#td", grcTexture::RORC_VERSION);
			}

			if (texDict)
			{
				// if we actually loaded one, we're the owner
				movie->m_OwnsTextures = true;
			}
		}

		movie->m_Textures = texDict;
		if (texDict)
		{
			sfAssertf(pgDictionary<grcTexture>::GetCurrent() == NULL, "Why is there a texture dictionary on the stack already?");
			pgDictionary<grcTexture>::SetCurrent(texDict);
		}
	}

	// Load the movie file and create its instance.
	u32 loadFlags = 
		GFxLoader::LoadWaitCompletion	| // synchronous loading  // back to LoadWaitCompletion until i've sorted out minimap interior issues
#if __DEV
		GFxLoader::LoadImageFiles		| // only load images from the SWF in DEV builds, otherwise they need to come from a tex dict.
#else
		GFxLoader::LoadDisableSWF		| // only gfx files allowed in non-DEV builds
#endif
		GFxLoader::LoadOnThread			| // defer texture creation until we're back on the main thread (do I even need to push the texture dict then?)
		0;


	if (!(movie->m_Movie = *m_Loader->CreateMovie(filename, loadFlags, FindArenaIndexFromId(arenaId))))
	{
		sfAssertf(false, "Couldn't create moviedef for %s - see TTY for more error messages.", filename);

		sfAssertf(pgDictionary<grcTexture>::GetCurrent() == pTxd, "Someone changed the current texture dictionary out from under us!");
		pgDictionary<grcTexture>::SetCurrent(NULL);

		if (bHasTxd && texDict)
		{
			if (!pTxd)
			{
				delete texDict;
			}
		}

		delete movie;
		return NULL;
	}

	sfAssertf(pgDictionary<grcTexture>::GetCurrent() == pTxd, "Someone changed the current texture dictionary out from under us!");
	pgDictionary<grcTexture>::SetCurrent(NULL);

	sysCriticalSection movieList(m_MovieListCS);
	m_MovieList.push_back(movie);

	return movie;
}

void rage::sfScaleformManager::BeginFrame()
{
	if (sysInterlockedCompareExchange(&m_MeshCacheResetRequested, 0, 1))
	{
		m_Loader->GetMeshCacheManager()->SetMeshCacheMemLimit(MESH_CACHE_MEM_LIMIT);
		m_Loader->GetMeshCacheManager()->SetRenderGenMemLimit(RENDERGEN_MEM_LIMIT);
		m_Loader->GetMeshCacheManager()->ClearCache();
	}

	PF_FUNC(BeginFrame);
	if (m_Renderer)
	{
		m_Renderer->BeginFrame();
	}
}

sfScaleformMovieView* rage::sfScaleformManager::CreateMovieView(sfScaleformMovie& movie, u32 arenaId)
{
	sysMemUseMemoryBucket b(sfScaleformManager::sm_MemoryBucket);

	GFxMovieDef::MemoryParams memParams;
	memParams.Desc.Arena = FindArenaIndexFromId(arenaId);

	if (memParams.Desc.Arena >= 2) // 0 and 1 are reserved values, index 2 maps to array elt 0
	{
		memParams.Desc.Granularity = m_MemoryArenas[(u32)(memParams.Desc.Arena-2)].m_Allocator->m_Granularity;
	}
	else
	{
		memParams.Desc.Granularity = 16 * 1024;
	}

	memParams.Desc.Reserve = memParams.Desc.Granularity * 1; // how many units to keep in reserve.

	memParams.FramesBetweenCollections = sm_FramesBetweenGarbageCollection;
	memParams.MaxCollectionRoots = sm_MaxRootsForGarbageCollection;

	GFxMovieView* gfxMovieView = movie.GetMovie().CreateInstance(memParams, true);
	if (!gfxMovieView)
	{
		return NULL;
	}

	sfScaleformMovieView* movieView = rage_new sfScaleformMovieView;
	movieView->m_MovieView = gfxMovieView;
	movieView->m_Movie = &movie;

	GFxValue gvalTrue;
	gvalTrue.SetBoolean(true);
	gfxMovieView->SetVariable("_global.noInvisibleAdvance", gvalTrue, GFxMovie::SV_Sticky);
	gfxMovieView->SetVariable("_global.isRage", gvalTrue, GFxMovie::SV_Sticky);
//	gfxMovieView->SetVariable("_global.gfxExtensions", gvalTrue, GFxMovie::SV_Sticky);

#if SUPPORT_MULTI_MONITOR
	const GridMonitor &mon = GRCDEVICE.GetMonitorConfig().getLandscapeMonitor();
	gfxMovieView->SetViewport(
		GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight(), // render target size
		mon.uLeft, mon.uTop,
		mon.getWidth(), mon.getHeight()
		);
#else
	// Default to full screen viewport
	gfxMovieView->SetViewport(
		GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight(), // render target size
		0, 0,										 // top,left coords
		GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight()	 // width, height of movie
		);
#endif	//SUPPORT_MULTI_MONITOR

	// We want a scaling factor that converts the movie extents (in TWIPS) to 1x1
	const GFxMovieDef& gfxMovie = movie.GetMovie();
	movieView->RecomputeScaleToUnitLengthVector( gfxMovie.GetWidth(), gfxMovie.GetHeight() );

	sysCriticalSection movieList(m_MovieListCS);
	m_MovieViewList.push_back(movieView);

	return movieView;
}

int sfScaleformManager::FindArenaIndexFromId(u32 id)
{
	sysCriticalSection movieList(m_MovieListCS);
	if (id == 0)
	{
		return 0;
	}

	for(int i = 0; i < m_MemoryArenas.GetCount(); i++)
	{
		if (m_MemoryArenas[i].m_Id == id)
		{
			return i+2;
		}
	}
	sfErrorf("Couldn't find memory arena with id %d", id);
	return 0;
}

u32 sfScaleformManager::CreatePreallocatedMemoryArena( const datResourceMap& preallocatedChunks, size_t granularity, size_t peakAllowed, sysMemAllocator* extraAllocator, atDelegate<void (void*)> freePreallocationDelegate, const char* arenaName )
{
	sysCriticalSection movieList(m_MovieListCS);
	int newIndex = -1;

	ArenaInfo* newArena = NULL;

	// Look for an existing slot that's unused
	for(int i = 0; i < m_MemoryArenas.GetCount(); i++)
	{
		if (m_MemoryArenas[i].m_Allocator == NULL)
		{
			newArena = &m_MemoryArenas[i];
			newIndex = i;
			break;
		}
	}
	if(newIndex == -1)
	{
		m_MemoryArenas.Grow();
		newArena = &m_MemoryArenas.Top();
		newIndex = m_MemoryArenas.GetCount() - 1;
	}

	FastAssert(newArena);
	newArena->m_Allocator = rage_new sfPreallocatedMemoryWrapper(preallocatedChunks, granularity, peakAllowed, extraAllocator, freePreallocationDelegate, arenaName);
	newArena->m_Id = m_NextArenaId;
	m_NextArenaId++;

#if __SF_STATS
	sfDebugf2("Created new memory arena index = %d, id = %d with %" SIZETFMT "dk preallocated storage", 
		newIndex + 2, newArena->m_Id, newArena->m_Allocator->FindPreallocatedSize() / 1024);
#endif

	GMemory::CreateArena(newIndex + 2, newArena->m_Allocator);
	return newArena->m_Id;
}

void sfScaleformManager::DestroyPreallocatedMemoryArena(u32 arenaId)
{				
	sysCriticalSection movieList(m_MovieListCS);
	int arenaIndex = -1;
	for(int i = 0; i < m_MemoryArenas.GetCount(); i++)
	{
		if (m_MemoryArenas[i].m_Id == arenaId)
		{
			arenaIndex = i;
			break;
		}
	}
	if (arenaIndex == -1)
	{
		sfErrorf("Couldn't find a memory arena with ID %d!", arenaId);
		return;
	}

	ArenaInfo* arena = &m_MemoryArenas[arenaIndex];

	sfAssertf(arena->m_Allocator, "Allocator doesn't exist!");

#if __SF_STATS
	sfDebugf2("Destroying memory arena index = %d, id = %d with %" SIZETFMT "dk preallocated storage and %" SIZETFMT "dk extra allocations",
		arenaIndex, arenaId, arena->m_Allocator->FindPreallocatedSize() / 1024, arena->m_Allocator->FindExtraAllocationSize() / 1024);

	if (arena->m_Allocator->m_ExtraStats[0].m_Size != 0)
	{
		arena->m_Allocator->PrintDebugInfo();
		sfErrorf("A scaleform movie needed to allocate extra memory, see TTY for more info");
	}
#endif // __SF_STATS

	if (sfVerifyf(GMemory::ArenaIsEmpty(arenaIndex + 2), "Arena is not empty!"))
	{
		GMemory::DestroyArena(arenaIndex + 2);

		delete arena->m_Allocator;
		arena->m_Allocator = NULL;
		arena->m_Id = 0;
	}
}   

void sfScaleformManager::DeleteMovie( sfScaleformMovie* movie )
{
	if (!movie) 
	{
		return;
	}

	sysCriticalSection movieList(m_MovieListCS);
	sfAssertf(std::find(m_MovieList.begin(), m_MovieList.end(), movie) != m_MovieList.end(),
		"Couldn't find movie in master list");

	// See if there are any active instances of this movie, and error if there are
	for(MovieViewList::iterator i = m_MovieViewList.begin(); i != m_MovieViewList.end(); ++i )
	{
		if ((*i)->GetMovieView().GetMovieDef() == &movie->GetMovie())
		{
			sfErrorf("Can't delete movie %s, there's still a movieview using it", movie->GetMovie().GetFileURL());
			return;
		}
	}
	m_MovieList.erase(movie);
	delete movie;
}

void sfScaleformManager::DeleteMovieView( sfScaleformMovieView* movieView )
{
	if (!movieView)
	{
		return;
	}
	sysCriticalSection movieList(m_MovieListCS);
	sfAssertf(std::find(m_MovieViewList.begin(), m_MovieViewList.end(), movieView) != m_MovieViewList.end(), 
		"Couldn't find movieView in master list");
	m_MovieViewList.erase(movieView);
	delete movieView;
}

void sfScaleformManager::FinishedDrawing()
{
	m_Renderer->SetUseDeletionQueue(false);
}

sfRendererBase& sfScaleformManager::GetRenderer()
{
	sfAssertf(m_Renderer.GetPtr(), "Renderer isn't initted!"); 
#if USE_SCALEFORM_AMP
	return *m_RendererWrapper;
#else
	return *m_Renderer.GetPtr();
#endif
}

sfRendererBase& sfScaleformManager::GetRageRenderer()
{
	sfAssertf(m_Renderer.GetPtr(), "Renderer isn't initted!"); 
	return *m_Renderer.GetPtr();
}

void sfScaleformManager::EndFrame()
{
	if (m_Renderer) m_Renderer->EndFrame();
}

void sfScaleformManager::PerformSafeModeOperations()
{
#if !USE_SCALEFORM_AMP && !defined(GFC_NO_STAT) // normally the amp server would reset the stats, but in case we're not running with it we need to.
	sysCriticalSection movieList(m_MovieListCS);
	// See if there are any active instances of this movie, and error if there are
	for(MovieViewList::iterator i = m_MovieViewList.begin(); i != m_MovieViewList.end(); ++i )
	{
		i->ResetStats();
	}
#endif

}

void sfScaleformManager::ResetMeshCache()
{
	sysInterlockedExchange(&m_MeshCacheResetRequested, 1);
}

void sfScaleformManager::ResetFontCache()
{
	m_Loader->GetFontCacheManager()->ClearRasterCache();
}

void sfScaleformManager::SetEdgeAAEnable(bool value)
{
	u32 renderFlags = m_RenderConfig->GetRenderFlags();
	if( value )
	{
		renderFlags |= GFxRenderConfig::RF_EdgeAA;
	}
	else
	{
		renderFlags &= ~GFxRenderConfig::RF_EdgeAA;
	}
	m_RenderConfig->SetRenderFlags(renderFlags);
}

sfPreallocatedMemoryWrapper* sfScaleformManager::GetAllocatorForArena( u32 arenaId )
{
	sysCriticalSection movieList(m_MovieListCS);
	for(int i = 0; i < m_MemoryArenas.GetCount(); i++)
	{
		if (m_MemoryArenas[i].m_Id == arenaId)
		{
			return m_MemoryArenas[i].m_Allocator;
		}
	}
	return NULL;
}

#if __SF_STATS
void sfScaleformManager::GetGlobalHeapSizes( size_t& curr, size_t& peak, size_t& debugCurr, size_t& debugPeak, const char*& lastAlloc )
{
	curr = m_ResourceMemoryWrapper->m_CurrentAllocations;
	peak = m_ResourceMemoryWrapper->m_HighWaterMark;
	debugCurr = m_ResourceMemoryWrapper->m_CurrentDebugAllocations;
	debugPeak = m_ResourceMemoryWrapper->m_DebugHighWaterMark;
	lastAlloc = m_ResourceMemoryWrapper->m_LastMovieToOverAllocate;
}
#endif

} // namespace rage

