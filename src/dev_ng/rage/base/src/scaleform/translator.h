// 
// scaleform/translator.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_TRANSLATOR_H 
#define SCALEFORM_TRANSLATOR_H 

#if 0

#include "scaleformheaders.h"

#include "text/stringtable.h"

namespace rage {

class sfTranslator : public GFxTranslator
{
public:
	sfTranslator();

	virtual void Translate(TranslateInfo* translateInfo);

	virtual UInt GetCaps() const;

	void SetStringTable(txtStringTable* table) {m_StringTable = table;}
	void SetTranslateEverything(bool everything) {m_TranslateEverything = everything;}

protected:
	txtStringTable*			m_StringTable;
	bool					m_TranslateEverything;
};

} // namespace rage


#endif

#endif // SCALEFORM_TRANSLATOR_H 
