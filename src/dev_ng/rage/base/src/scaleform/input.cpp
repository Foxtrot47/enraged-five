// 
// scaleform/input.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "input.h"

#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "scaleform/scaleform.h"
#include "scaleform/scaleformheaders.h"
#include "system/codecheck.h"

namespace rage {

	const char g_RageToLowercaseAscii[256] = 
	{
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		' ',0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		'0','1','2','3',		'4','5','6','7',		'8','9',0,0,		0,0,0,0,
		0,'a','b','c',		'd','e','f','g',		'h','i','j','k',		'l','m','n','o',
		'p','q','r','s',		't','u','v','w',		'x','y','z',0,		0,0,0,0,
		'0','1','2','3',		'4','5','6','7',		'8','9','*','+',		0,'-','.','/',
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,';','=',		',','-','.','/',
		'`',0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,'[',		'\\',']','\'','`',
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
	};

	const char g_RageToUppercaseAscii[256] = 
	{
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		' ',0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		')','!','@','#',		'$','%','^','&',		'*','(',0,0,		0,0,0,0,
		0,'A','B','C',		'D','E','F','G',		'H','I','J','K',		'L','M','N','O',
		'P','Q','R','S',		'T','U','V','W',		'X','Y','Z',0,		0,0,0,0,
		'0','1','2','3',		'4','5','6','7',		'8','9','*','+',		0,'-','.','/',
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,':','+',		'<','_','>','?',
		'~',0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,'{',		'|','}','"','~',
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
		0,0,0,0,		0,0,0,0,		0,0,0,0,		0,0,0,0,
	};


	const u8	g_RageToSfKeymap[256] = 
	{
		0,					0,					0,					0,					// 0x00 - 0x03
		0,					0,					0,					0,					// 0x04 - 0x07
		GFxKey::Backspace,	GFxKey::Tab,		0,					0,					// 0x08 - 0x0B
		GFxKey::Clear,		GFxKey::Return,		0,					0,					// 0x0C - 0x0F

		0,					0,					0,					GFxKey::Pause,		// 0x10 - 0x13
		GFxKey::CapsLock,	/*Kana*/0,			0,					/*Junja*/0,			// 0x14 - 0x17
		/*Final*/0,			/*Kanji*/0,			0,					GFxKey::Escape,		// 0x18 - 0x1B
		/*Convert*/0,		/*NoConvert*/0,		/*Accept*/0,		/*ModeChange*/0,	// 0x1C - 0x1F

		GFxKey::Space,		GFxKey::PageUp,		GFxKey::PageDown,	GFxKey::End,		// 0x20 - 0x23
		GFxKey::Home,		GFxKey::Left,		GFxKey::Up,			GFxKey::Right,		// 0x24 - 0x27
		GFxKey::Down,		/*Select*/0,		/*Print*/0,			/*Execute*/0,		// 0x28 - 0x2B
		/*PrintScreen*/0,	GFxKey::Insert,		GFxKey::Delete,		GFxKey::Help,		// 0x2C - 0x2F

		GFxKey::Num0,		GFxKey::Num1,		GFxKey::Num2,		GFxKey::Num3,		// 0x30 - 0x33
		GFxKey::Num4,		GFxKey::Num5,		GFxKey::Num6,		GFxKey::Num7,		// 0x34 - 0x37
		GFxKey::Num8,		GFxKey::Num9,		0,					0,					// 0x38 - 0x3B
		0,					0,					0,					0,					// 0x3C - 0x3F

		0,					GFxKey::A,			GFxKey::B,			GFxKey::C,			// 0x40 - 0x43
		GFxKey::D,			GFxKey::E,			GFxKey::F,			GFxKey::G,			// 0x44 - 0x47
		GFxKey::H,			GFxKey::I,			GFxKey::J,			GFxKey::K,			// 0x48 - 0x4B
		GFxKey::L,			GFxKey::M,			GFxKey::N,			GFxKey::O,			// 0x4C - 0x4F

		GFxKey::P,			GFxKey::Q,			GFxKey::R,			GFxKey::S,			// 0x50 - 0x53
		GFxKey::T,			GFxKey::U,			GFxKey::V,			GFxKey::W,			// 0x54 - 0x57
		GFxKey::X,			GFxKey::Y,			GFxKey::Z,			/*LWin*/0,			// 0x58 - 0x5B
		/*RWin*/0,			/*Apps*/0,			0,					/*Sleep*/0,			// 0x5C - 0x5F

		GFxKey::KP_0,		GFxKey::KP_1,		GFxKey::KP_2,		GFxKey::KP_3,		// 0x60 - 0x63
		GFxKey::KP_4,		GFxKey::KP_5,		GFxKey::KP_6,		GFxKey::KP_7,		// 0x64 - 0x67
		GFxKey::KP_8,		GFxKey::KP_9,		GFxKey::KP_Multiply,GFxKey::KP_Add,		// 0x68 - 0x6B
		/*Separator*/0,		GFxKey::KP_Subtract,GFxKey::KP_Decimal,	GFxKey::KP_Divide,	// 0x6C - 0x6F

		GFxKey::F1,			GFxKey::F2,			GFxKey::F3,			GFxKey::F4,			// 0x70 - 0x73
		GFxKey::F5,			GFxKey::F6,			GFxKey::F7,			GFxKey::F8,			// 0x74 - 0x77
		GFxKey::F9,			GFxKey::F10,		GFxKey::F11,		GFxKey::F12,		// 0x78 - 0x7B
		GFxKey::F13,		GFxKey::F14,		GFxKey::F15,		/*F16*/0,			// 0x7C - 0x7F

		/*F17*/0,			/*F18*/0,			/*F19*/0,			/*F20*/0,			// 0x80 - 0x83
		/*F21*/0,			/*F22*/0,			/*F23*/0,			/*F24*/0,			// 0x84 - 0x87
		0,					0,					0,					0,					// 0x88 - 0x8B
		0,					0,					0,					0,					// 0x8C - 0x8F

		GFxKey::NumLock,	GFxKey::ScrollLock,	/*NumPadEquals*/0,	/*FJ_Masshou*/0,	// 0x90 - 0x93
		/*FJ_Touroku*/0,	/*FJ_LOya*/0,		/*FJ_ROya*/0,		0,					// 0x94 - 0x97
		0,					0,					0,					0,					// 0x98 - 0x9B
		0,					0,					0,					0,					// 0x9C - 0x9F

		/*L*/GFxKey::Shift,	/*R*/GFxKey::Shift,	/*L*/GFxKey::Control, /*R*/GFxKey::Control,	// 0xA0 - 0xA3
		/*LMenu*/0,			/*RMenu*/0,			/*WebBack*/0,		/*WebForward*/0,	// 0xA4 - 0xA7
		/*WebRefresh*/0,	/*WebStop*/0,		/*WebSearch*/0,		/*WebFavorites*/0,	// 0xA8 - 0xAB
		/*WebHome*/0,		/*Mute*/0,			/*VolDown*/0,		/*VolUp*/0,			// 0xAC - 0xAF

		/*NextTrack*/0,		/*PrevTrack*/0,		/*MediaStop*/0,		/*PlayPause*/0,		// 0xB0 - 0xB3
		/*Mail*/0,			/*MediaSelect*/0,	/*LaunchApp1*/0,	/*LaunchApp2*/0,	// 0xB4 - 0xB7
		0,					0,					GFxKey::Semicolon,	/*Plus*/GFxKey::Equal,// 0xB8 - 0xBB
		GFxKey::Comma,		GFxKey::Minus,		GFxKey::Period,		GFxKey::Slash,		// 0xBC - 0xBF

		/*Tilde*/GFxKey::Bar, 0,				0,					0,					// 0xC0 - 0xC3
		0,					0,					0,					0,					// 0xC4 - 0xC7
		0,					0,					0,					0,					// 0xC8 - 0xCB
		0,					0,					0,					0,					// 0xCC - 0xCF

		0,					0,					0,					0,					// 0xD0 - 0xD3
		0,					0,					0,					0,					// 0xD4 - 0xD7
		0,					0,					0,					GFxKey::BracketLeft,// 0xD8 - 0xDB
		GFxKey::Backslash, GFxKey::BracketRight,GFxKey::Quote,		/*Grave*/GFxKey::Bar,// 0xDC - 0xDF

		0,					GFxKey::OEM_AX,		GFxKey::OEM_102,	GFxKey::ICO_HELP,	// 0xE0 - 0xE3
		GFxKey::ICO_00,		/*ProcessKey*/0,	/*ICO_Clear*/0,		/*Packet*/0,		// 0xE4 - 0xE7
		0,					0,					0,					0,					// 0xE8 - 0xEB
		0,					0,					0,					0,					// 0xEC - 0xEF

		0,					0,					0,					0,					// 0xF0 - 0xF3
		0,					0,					0,					0,					// 0xF4 - 0xF7
		0,					0,					0,					0,					// 0xF8 - 0xFB
		0,					GFxKey::KP_Enter,	/*Chatpad_Greed_Shift*/0,	/*Chatpad_Orange_Shift*/0,	// 0xFC - 0xFF
	};



	// The order for this table is:
	/*
	L2_INDEX, R2_INDEX, L1_INDEX, R1_INDEX,
	RUP_INDEX, RRIGHT_INDEX, RDOWN_INDEX, RLEFT_INDEX,
	SELECT_INDEX, L3_INDEX, R3_INDEX, START_INDEX,
	LUP_INDEX, LRIGHT_INDEX, LDOWN_INDEX, LLEFT_INDEX,
	*/
	const u8 g_PadToKeyMap[ioPad::NUMBUTTONS] = 
	{
		GFxKey::KP_5,	GFxKey::KP_8,		GFxKey::KP_4,		GFxKey::KP_7,
		GFxKey::KP_3,	GFxKey::KP_1,		GFxKey::KP_0,		GFxKey::KP_2,
		GFxKey::KP_Add, GFxKey::KP_6,		GFxKey::KP_9,		GFxKey::KP_Multiply,
		GFxKey::Up,		GFxKey::Right,		GFxKey::Down,		GFxKey::Left,
	};

	struct MapperData
	{
		ioMapperSource	 m_MapperSource;
		u32				 m_MapperValue;
		GFxKey::Code	 m_Output;
	};

	// Analog stick mapping
	const MapperData g_PadMapper[sfInput::NUM_MAPPED_BUTTONS] = 
	{
		{IOMS_PAD_AXIS, IOM_AXIS_LUP,		GFxKey::Up},	
		{IOMS_PAD_AXIS, IOM_AXIS_LRIGHT,	GFxKey::Right},	
		{IOMS_PAD_AXIS, IOM_AXIS_LDOWN,		GFxKey::Down},	
		{IOMS_PAD_AXIS, IOM_AXIS_LLEFT,		GFxKey::Left},
		{IOMS_PAD_AXIS, IOM_AXIS_RUP,		GFxKey::Home},
		{IOMS_PAD_AXIS, IOM_AXIS_RRIGHT,	GFxKey::PageDown},
		{IOMS_PAD_AXIS, IOM_AXIS_RDOWN,		GFxKey::End},
		{IOMS_PAD_AXIS, IOM_AXIS_RLEFT,		GFxKey::Delete},
	};

	void sfInput::Init()
	{
		// Arguably I could use the mapper for everything - but just using it for analog
		// stick values keeps it a little smaller.
		for(int padNum = 0; padNum < MAX_MAPPED_PADS; padNum++)
		{
			for(int mapping = 0; mapping < NUM_MAPPED_BUTTONS; mapping++)
			{
				m_Mapper.Map(g_PadMapper[mapping].m_MapperSource, g_PadMapper[mapping].m_MapperValue, m_Values[padNum][mapping], padNum);
			}
		}
		m_LastMouseX = ioMouse::GetX();
		m_LastMouseY = ioMouse::GetY();
		m_DX = m_DY = 0;
		m_bIgnoreBlocksForNextSend = false;
		m_bForceReleaseForNextSend = false;
#if RSG_EXTRA_MOUSE_SUPPORT
		m_bForceReleaseForNextUpdate = false;
		m_bLastHadFocus = true;
#endif
	}

	bool isBitSet(int field, int flag) { return (field & flag) == flag; }

	void sfInput::Update(float dt, eBlockedMouseInputs eBMI)
	{
		m_Mapper.Update((int)(dt * 1000.0f));
		m_KeysChanged = ioKeyboard::AnyKeyChanged();

		int newX = ioMouse::GetX();
		int newY = ioMouse::GetY();
		m_DX = newX - m_LastMouseX;
		m_DY = newY - m_LastMouseY;
		m_LastMouseX = newX;
		m_LastMouseY = newY;

		m_BlockedInputs = eBMI;

#if RSG_EXTRA_MOUSE_SUPPORT
		if( ioMouse::ClientAreaHasFocus() )
		{
			m_bLastHadFocus = true;
			m_bForceReleaseForNextUpdate = false;
		}
		else // game has lost focus
		{
			// m_bLastHadFocus is true only the first time after we lose focus
			m_bForceReleaseForNextUpdate = m_bLastHadFocus;

			m_bLastHadFocus = false;
			m_BlockedInputs = BLOCKED_MOUSE_ALL;
		}
#endif

#if __DEV
		m_LastFrameUpdated = TIME.GetFrameCount();
#endif
	}

	bool sfInput::HaveButtonsChanged()
	{
		return ioMouse::GetChangedButtons() || m_bForceReleaseForNextSend;
	}

	unsigned sfInput::GetReleasedButtons()
	{
		unsigned released = ioMouse::GetReleasedButtons();

		if(m_bForceReleaseForNextSend)
		{
			released |= ioMouse::MOUSE_LEFT;
		}

		return released;
	}

	unsigned sfInput::GetPressedButtons()
	{
		if(m_bForceReleaseForNextSend)
		{
			return 0u;
		}

		return ioMouse::GetPressedButtons();
	}

	void sfInput::SendMouseEvents( sfScaleformMovieView* movieview )
	{
		GFxMovieView& movie = movieview->GetMovieView();

		GViewport vp;
		movieview->GetMovieView().GetViewport(&vp);

		bool bHasFocus = RSG_EXTRA_MOUSE_SUPPORT_SWITCH( ioMouse::ClientAreaHasFocus(), true);

		float x = bHasFocus ? ((float)ioMouse::GetX() - vp.Left) : -1.0f; // if we're lacking focus, send our positions as off screen
		float y = bHasFocus ? ((float)ioMouse::GetY() - vp.Top) : -1.0f;

		eBlockedMouseInputs eBMI = m_bIgnoreBlocksForNextSend ? BLOCKED_MOUSE_NONE : m_BlockedInputs;
		m_bIgnoreBlocksForNextSend = false;

		// for the rest of this Send, set this to true
#if RSG_EXTRA_MOUSE_SUPPORT
		if( m_bForceReleaseForNextUpdate )
			m_bForceReleaseForNextSend = true;
#endif

		if (HaveButtonsChanged())
		{
			// something changed - figure out what it was.
			unsigned released = GetReleasedButtons();
			unsigned pressed = GetPressedButtons();

			if (released)
			{
				// send MouseUp for any released buttons
				if ((isBitSet(released, ioMouse::MOUSE_LEFT) && !isBitSet(eBMI, BLOCKED_MOUSE_LEFT)) || m_bForceReleaseForNextSend)
				{
					GFxMouseEvent evt(GFxEvent::MouseUp, 0, x, y);
					movie.HandleEvent(evt);
				}
				if (isBitSet(released, ioMouse::MOUSE_RIGHT) && !isBitSet(eBMI, BLOCKED_MOUSE_RIGHT))
				{
					GFxMouseEvent evt(GFxEvent::MouseUp, 1, x, y);
					movie.HandleEvent(evt);
				}
				if (isBitSet(released, ioMouse::MOUSE_MIDDLE) && !isBitSet(eBMI, BLOCKED_MOUSE_MIDDLE))
				{
					GFxMouseEvent evt(GFxEvent::MouseUp, 2, x, y);
					movie.HandleEvent(evt);
				}
			}
			if (pressed)
			{
				// send MouseDown for any pressed buttons
				if( isBitSet(pressed, ioMouse::MOUSE_LEFT) && !isBitSet(eBMI, BLOCKED_MOUSE_LEFT))
				{
					GFxMouseEvent evt(GFxEvent::MouseDown, 0, x, y);
					movie.HandleEvent(evt);
				}
				if (isBitSet(pressed, ioMouse::MOUSE_RIGHT) && !isBitSet(eBMI, BLOCKED_MOUSE_RIGHT))
				{
					GFxMouseEvent evt(GFxEvent::MouseDown, 1, x, y);
					movie.HandleEvent(evt);
				}
				if (isBitSet(pressed, ioMouse::MOUSE_MIDDLE) && !isBitSet(eBMI, BLOCKED_MOUSE_MIDDLE))
				{
					GFxMouseEvent evt(GFxEvent::MouseDown, 2, x, y);
					movie.HandleEvent(evt);
				}
			}
		}
		if (!isBitSet(eBMI, BLOCKED_MOUSE_SCROLL) && ioMouse::GetDZ())
		{
			// send MouseWheel
			GFxMouseEvent evt(GFxEvent::MouseWheel, 0, x, y, (float)ioMouse::GetDZ());
			movie.HandleEvent(evt);
		}

		// send mouse moves
		if ( !isBitSet(eBMI, BLOCKED_MOUSE_MOVE) &&   
			(ioMouse::GetDX() != 0 || ioMouse::GetDY() != 0 ||
			m_DX != 0 || m_DY != 0))
		{
			// send MouseMove
			GFxMouseEvent evt(GFxEvent::MouseMove, 0, x, y);
			movie.HandleEvent(evt);
		}

		m_bForceReleaseForNextSend = false;
	}

	void sfInput::SendKeyboardEvents( sfScaleformMovieView* movieview )
	{
#if __DEV
		Assertf(m_LastFrameUpdated == TIME.GetFrameCount(), "sfInput::Update (or sfScaleformManager::UpdateManagerOnly) needs to be called every frame");
#endif

		GFxMovieView& movie = movieview->GetMovieView();

		if (m_KeysChanged)
		{
			bool isShifted = ioKeyboard::KeyDown(KEY_SHIFT) != 0;

			GFxSpecialKeysState modKeyState;
			modKeyState.Reset();
			modKeyState.SetAltPressed(ioKeyboard::KeyDown(KEY_ALT) != 0);
			modKeyState.SetCtrlPressed(ioKeyboard::KeyDown(KEY_CONTROL) != 0);
			modKeyState.SetShiftPressed(isShifted);

			// figure out which one changed, run it through the scaleform map.
			for(int i = 0; i < 256; i++)
			{
				if (ioKeyboard::KeyChanged(i))
				{
					char16 unicodeChar = ioKeyboard::GetPrintableChar((ioMapperParameter)i);
					char asciiChar = 0;
					if (unicodeChar <= 127)
					{
						asciiChar = (char)unicodeChar;
						if (asciiChar == 0) // Still couldn't get one, check the default US keyboard map
						{
							asciiChar = isShifted ? g_RageToUppercaseAscii[i] : g_RageToLowercaseAscii[i];
						}
					}

					GFxEvent::EventType evtType = ioKeyboard::KeyPressed(i) ? GFxEvent::KeyDown : GFxEvent::KeyUp;

					GFxKeyEvent evt(evtType, (GFxKey::Code)g_RageToSfKeymap[i], asciiChar, unicodeChar, modKeyState);
					movie.HandleEvent(evt);

					if (unicodeChar != 0 && ioKeyboard::KeyPressed(i))
					{
						GFxCharEvent charEvt(unicodeChar, 0);
						movie.HandleEvent(charEvt);
					}
				}
			}
		}
	}

	void sfInput::SendPadEvents( sfScaleformMovieView* movieview)
	{
#if __DEV
		Assertf(m_LastFrameUpdated == TIME.GetFrameCount(), "sfInput::Update (or sfScaleformManager::UpdateManagerOnly) needs to be called every frame");
#endif

		GFxMovieView& movie = movieview->GetMovieView();

		for(int padNum = 0; padNum < MAX_MAPPED_PADS; padNum++)
		{
			if (movieview->GetInputPadMask().IsClear(padNum))
			{
				continue;
			}
			ioPad& pad = ioPad::GetPad(padNum);
			u32 changed = pad.GetChangedButtons();
			u32 pressed = pad.GetPressedButtons();
			u32 released = pad.GetReleasedButtons();
			if (changed)
			{
				// Check the pad buttons that we have mappings for
				for(int button = 0; button < ioPad::NUMBUTTONS; button++)
				{
					GFxKey::Code keyCode = (GFxKey::Code)g_PadToKeyMap[button];
					if ((released & 1) && keyCode > 0)
					{
						GFxKeyEvent evt(GFxEvent::KeyUp, keyCode, 255, 0xFFFFFFFF, (u8)padNum);
						movie.HandleEvent(evt);
					}
					if ((pressed & 1) && keyCode > 0)
					{
						GFxKeyEvent evt(GFxEvent::KeyDown, keyCode, 255, 0xFFFFFFFF, (u8)padNum);
						movie.HandleEvent(evt);
					}
					pressed >>= 1;
					released >>= 1;
					changed >>= 1;
				}
			}

			// Now check the ioValues
			for(int val = 0; val < NUM_MAPPED_BUTTONS; val++)
			{
				if (m_Values[padNum][val].IsReleased())
				{
					GFxKeyEvent evt(GFxEvent::KeyUp, g_PadMapper[val].m_Output, 255, 0xFFFFFFFF, (u8)padNum);
					movie.HandleEvent(evt);
				}
				if (m_Values[padNum][val].IsPressed())
				{
					GFxKeyEvent evt(GFxEvent::KeyDown, g_PadMapper[val].m_Output, 255, 0xFFFFFFFF, (u8)padNum);
					movie.HandleEvent(evt);
				}
			}
		}
	}
} // namespace rage
