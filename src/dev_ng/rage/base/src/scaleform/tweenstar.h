// 
// scaleform/tweenstar.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_TWEENSTAR_H
#define SCALEFORM_TWEENSTAR_H

#include "scaleform.h"

namespace rage
{
	namespace TweenStar
	{
		enum EaseType {
			EASE_linear,
			EASE_quadratic_EI,
			EASE_quadratic_EO,
			EASE_quadratic_EIEO,
			EASE_cubic_EI,
			EASE_cubic_EO,
			EASE_cubic_EIEO,
			EASE_quartic_EI,
			EASE_quartic_EO,
			EASE_quartic_EIEO,
			EASE_sine_EI,
			EASE_sine_EO,
			EASE_sine_EIEO,
			EASE_back_EI,
			EASE_back_EO, 
			EASE_back_EIEO,
			EASE_circular_EI,
			EASE_circular_EO,
			EASE_circular_EIEO,
		};

		float ComputeEasedValue(float t, EaseType Ease);
	}


	void sfInstallTweenstar(sfScaleformMovieView& movie);
}

#endif