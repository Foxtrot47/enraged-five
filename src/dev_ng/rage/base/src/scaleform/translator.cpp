// 
// scaleform/translator.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "translator.h"

#include "channel.h"

#include "string/unicode.h"

#if 0

namespace rage {

sfTranslator::sfTranslator()
: m_StringTable(NULL)
, m_TranslateEverything(false)
{
}

UInt sfTranslator::GetCaps() const
{
	return GFxTranslator::Cap_StripTrailingNewLines;
}

void sfTranslator::Translate(TranslateInfo* translateInfo)
{
	sfAssertf(translateInfo, "Scaleform didn't send in any translation info");
	const wchar_t* key = translateInfo->GetKey();

	USES_CONVERSION;

	if (!m_StringTable)
	{
		if (m_TranslateEverything || key[0] == '$')
		{
			static bool alreadyWarned = false;
			if (!alreadyWarned)
			{
				alreadyWarned = true;
				sfWarningf("No string table has been set for translation");
			}
		}
		return;
	}

	if (!m_TranslateEverything && key[0] != '$')
	{
		return; // nothing to do in this case
	}

	if (key[0] == '$')
	{
		key++;
	}

	CompileTimeAssert(sizeof(wchar_t) == sizeof(char16));
	const char* utf8key = WIDE_TO_UTF8(reinterpret_cast<const char16*>(key));

	sfCondLogf(translateInfo->IsKeyHtml(), DIAG_SEVERITY_ERROR, "Stringtable key should not be HTML. Key is \"%s\", instance is %s", utf8key, translateInfo->GetInstanceName());

	txtStringData* data = m_StringTable->Get(utf8key);
	if (data)
	{
		const char16* newString = data->GetStringUnicode();
		if (newString)
		{
			translateInfo->SetResult(reinterpret_cast<const wchar_t*>(newString)); // when should I call SetResultHTML here?
		}
	}
}

} // namespace rage

#endif