// 
// scaleform/commandbuffer.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_COMMANDBUFFER_H
#define SCALEFORM_COMMANDBUFFER_H

#include "scaleformheaders.h"

#include "renderer.h"

#include "atl/array.h"
#include "vector/vector4.h"
#include "vectormath/vec4f.h"

namespace rage
{
	class grcVertexBuffer;
	class grcIndexBuffer;

	class sfCommandBuffer
	{
	public:
		void CreateRageBuffers();
		void Execute();


#if SF_RECORD_COMMAND_BUFFER
		void DumpAscii(const char* filename);

		int Add_VertexBuffer(GRenderer::VertexFormat, const void*, size_t vertexCount);
		int Add_IndexBuffer(GRenderer::IndexFormat, const void*, size_t indexCount);

		void Add_SetVar(sfEffectVars varIndex, const Vector4& data);
		void Add_SetMatrix(sfEffectVars varIndex, const Vector4& row0, const Vector4& row1);

		void Add_SetBlendState(sfBlendStateBlocks blendState);
		void Add_DrawIndexedTriangles(sfTechniques technique, int vertexBufferIdx, int indexBufferIdx, u16 numVerts, u16 baseVertexIndex);
		void Add_DrawLineStrip(sfTechniques technique, int vertexBufferIdx, u16 numVerts, u16 startIndex);

		void Add_Comment(const char* string);
#endif

	private:
		struct VertexBuffer
		{
			VertexBuffer() : m_Format(GRenderer::Vertex_None), m_RageVB(NULL) {}
			size_t GetVertexCount() { return m_VertexData.GetCount() / GetVertexStride(); }
			size_t GetVertexStride();
#if SF_RECORD_COMMAND_BUFFER
			void DumpAscii(fiStream* stream);
#endif
			GRenderer::VertexFormat m_Format;
			atArray<char, 16> m_VertexData;
			grcVertexBuffer* m_RageVB;
		};

		struct IndexBuffer
		{
			IndexBuffer() : m_Format(GRenderer::Index_None), m_RageIB(NULL), m_Offset(0) {}
			size_t GetIndexCount() { return m_IndexData.GetCount() / GetIndexStride(); }
			size_t GetIndexStride();
#if SF_RECORD_COMMAND_BUFFER
			void DumpAscii(fiStream* stream);
			void ApplyOffset(u16 off);
#endif

			GRenderer::IndexFormat m_Format;
			atArray<char, 16> m_IndexData;
			grcIndexBuffer* m_RageIB;
			u16 m_Offset;
		};

		enum CommandID {
			SET_SHADER_VAR,		
			SET_SHADER_MTX,
			SET_BLEND_STATE,
			DRAW_INDEXED_TRIS,
			DRAW_LINESTRIP,
			COMMENT,
		};

		struct Command
		{
			CommandID GetId() { return (CommandID)m_ID; }

			u32 DECLARE_BITFIELD_3(
				m_ID, 8,
				m_Payload1, 8,
				m_Pad,		16
			);
		};

		struct SetVarCommand : public Command
		{
			/* payload1 = var index */
			float m_X, m_Y, m_Z, m_W;
		};

		struct SetMtxCommand : public Command
		{
			/* payload1 = var index */
			float m_X0, m_Y0, m_Z0, m_W0;
			float m_X1, m_Y1, m_Z1, m_W1;
		};

		struct DrawIndexedCommand : public Command
		{
			/* payload1 = technique */
			u16 m_NumVerts;
			u16 m_IBIndex, m_VBIndex; // which index buffer and vertex buffer
			u16 m_VBStartIndex;
		};

		struct DrawVertsCommand : public Command
		{
			/* payload1 = technique */
			u16 m_NumVerts;
			u16 m_VBIndex, m_VBOffset;
		};

		atArray<VertexBuffer> m_VertexBuffers;
		atArray<IndexBuffer> m_IndexBuffers;
		atArray<u32> m_Commands;


		template<typename Type> Type& ReserveSpace(CommandID id);
	};

}

#endif // SCALEFORM_COMMANDBUFFER_H
