// 
// scaleform/commandbuffer.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "commandbuffer.h"

#include "renderer.h"

#include "file/asset.h"
#include "file/stream.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/wrapper_gcm.h"
#include "grprofile/pix.h"

using namespace rage;
using namespace rage::sfGlobals;

template<typename Type> Type& sfCommandBuffer::ReserveSpace(CommandID id)
{
	int oldCount = m_Commands.GetCount();
	for(int i = 0; i < sizeof(Type)/sizeof(u32); i++)
	{
		m_Commands.Grow();
	}

	Command& cmd = *(Command*)&m_Commands[oldCount];
	cmd.m_ID = id;
	cmd.m_Payload1 = 0;
	cmd.m_Pad = 0;

	return *(Type*)&m_Commands[oldCount];
}

size_t sfCommandBuffer::VertexBuffer::GetVertexStride()
{
	return g_ScaleformVertexDecls[m_Format]->Stream0Size;
}

size_t sfCommandBuffer::IndexBuffer::GetIndexStride()
{
	Assertf(m_Format == GRenderer::Index_16, "Only 16 bit indices are supported for rage index buffers");
	return sizeof(u16);
}

#if SF_RECORD_COMMAND_BUFFER
void sfCommandBuffer::IndexBuffer::ApplyOffset(u16 off)
{
	if (m_Offset != 0)
	{
		sfAssertf(0, "Offset has already been applied to this index buffer. We need to clone it and try again");
		return;
	}
	if (off == 0)
	{
		// nothing to do
		return;
	}

	switch(m_Format)
	{
	case GRenderer::Index_16:
		{
			u16* indexData = reinterpret_cast<u16*>(m_IndexData.GetElements());
			int numIndices = GetIndexCount();
			for(int i = 0; i < numIndices; i++)
			{
				u32 newIndex = indexData[i] + off;
				sfAssertf(newIndex < USHRT_MAX, "Offset index is too big. Original: %d, offset %d", indexData[i], off);
				indexData[i] = (u16)newIndex;
			}
			m_Offset = off;
		}
		break;
	default:
		sfErrorf("Can't offset this format");
		break;
	}
}

int sfCommandBuffer::Add_VertexBuffer(GRenderer::VertexFormat format, const void* data, size_t vertexCount)
{
	size_t size = g_ScaleformVertexDecls[format]->Stream0Size * vertexCount;
	VertexBuffer& vb = m_VertexBuffers.Grow(4);
	vb.m_Format = format;
	Assertf(size < USHRT_MAX, "Too many verts (%d) for an atArray!", size);
	vb.m_VertexData.CopyFrom(reinterpret_cast<const char*>(data), (u16)size);
	return m_VertexBuffers.GetCount()-1;
}

int sfCommandBuffer::Add_IndexBuffer(GRenderer::IndexFormat format, const void* data, size_t indexCount)
{
	if (!parVerifyf(format == GRenderer::Index_16, "Only 16 bit indices are supported for rage index buffers"))
	{
		return -1;
	}
	IndexBuffer& ib = m_IndexBuffers.Grow(4);
	ib.m_Format = format;
	size_t size = indexCount * sizeof(u16);
	Assertf(size < USHRT_MAX, "Too many indices (%d) for an atArray!", size);
	ib.m_IndexData.CopyFrom(reinterpret_cast<const char*>(data), (u16)size);
	return m_IndexBuffers.GetCount()-1;
}

void rage::sfCommandBuffer::Add_SetVar( sfEffectVars varIndex, const Vector4& data )
{
	SetVarCommand& cmd = ReserveSpace<SetVarCommand>(SET_SHADER_VAR);
	cmd.m_Payload1 = (u8)varIndex;
	cmd.m_X = data.x;
	cmd.m_Y = data.y;
	cmd.m_Z = data.z;
	cmd.m_W = data.w;
}

void sfCommandBuffer::Add_SetMatrix(sfEffectVars varIndex, const Vector4& row0, const Vector4& row1)
{
	SetMtxCommand& cmd = ReserveSpace<SetMtxCommand>(SET_SHADER_MTX);
	cmd.m_Payload1 = (u8)varIndex;
	cmd.m_X0 = row0.x;
	cmd.m_Y0 = row0.y;
	cmd.m_Z0 = row0.z;
	cmd.m_W0 = row0.w;
	cmd.m_X1 = row1.x;
	cmd.m_Y1 = row1.y;
	cmd.m_Z1 = row1.z;
	cmd.m_W1 = row1.w;
}

void sfCommandBuffer::Add_Comment(const char* string)
{
	Command& cmd = ReserveSpace<Command>(COMMENT);
	size_t len = strlen(string);
	if (!Verifyf(len < UCHAR_MAX * sizeof(u32), "String is too long"))
	{ 
		return; 
	}
	size_t blocks = (len+1 + (sizeof(u32)-1)) / sizeof(u32); // len+1 includes null byte
	cmd.m_Payload1 = (u8)blocks;

	int destIdx = m_Commands.GetCount();

	for(size_t i = 0; i < blocks; i++)
	{
		m_Commands.Grow() = 0;
	}

	safecpy((char*)&m_Commands[destIdx], string, (char*)m_Commands.end()-(char*)(&m_Commands[destIdx]));
}

void sfCommandBuffer::Add_SetBlendState(sfBlendStateBlocks blendState)
{
	Command& cmd = ReserveSpace<Command>(SET_BLEND_STATE);
	cmd.m_Payload1 = (u8)blendState;
}

void sfCommandBuffer::Add_DrawIndexedTriangles(sfTechniques technique, int vertexBufferIdx, int indexBufferIdx, u16 numVerts, u16 baseVertexIndex)
{
	if (!parVerifyf(vertexBufferIdx >= 0 && vertexBufferIdx < m_VertexBuffers.GetCount() &&
		indexBufferIdx >= 0 && indexBufferIdx < m_IndexBuffers.GetCount(), "Vertex buffer (%d) or index buffer (%d) index out of range!", vertexBufferIdx, indexBufferIdx))
	{
		return;
	}

	IndexBuffer& ib = m_IndexBuffers[indexBufferIdx];
	ib.ApplyOffset(baseVertexIndex);

	DrawIndexedCommand& cmd = ReserveSpace<DrawIndexedCommand>(DRAW_INDEXED_TRIS);
	cmd.m_Payload1 = (u8)technique;
	cmd.m_NumVerts = numVerts;
	cmd.m_VBIndex = (u16)vertexBufferIdx;
	cmd.m_IBIndex = (u16)indexBufferIdx;
	cmd.m_VBStartIndex = baseVertexIndex;
}

void sfCommandBuffer::Add_DrawLineStrip(sfTechniques technique, int vertexBufferIdx, u16 numVerts, u16 startIndex)
{
	if (!parVerifyf(vertexBufferIdx >= 0 && vertexBufferIdx < m_VertexBuffers.GetCount(), "Vertex buffer (%d) index out of range!", vertexBufferIdx))
	{
		return;
	}
	DrawVertsCommand& cmd = ReserveSpace<DrawVertsCommand>(DRAW_LINESTRIP);
	cmd.m_Payload1 = (u8)technique;
	cmd.m_NumVerts = numVerts;
	cmd.m_VBIndex = (u16)vertexBufferIdx;
	cmd.m_VBOffset = startIndex;
}
#endif

void sfCommandBuffer::CreateRageBuffers()
{
	for(int i = 0; i < m_IndexBuffers.GetCount(); i++)
	{
		IndexBuffer& ib = m_IndexBuffers[i];
		bool dynamic = __PS3;
		ib.m_RageIB = grcIndexBuffer::Create((int)ib.GetIndexCount(), dynamic, ib.m_IndexData.GetElements());
	}

	for(int i = 0; i < m_VertexBuffers.GetCount(); i++)
	{
		VertexBuffer& vb = m_VertexBuffers[i];
		bool dynamic = __PS3;
		bool readWrite = __PS3;
		vb.m_RageVB = grcVertexBuffer::Create((int)vb.GetVertexCount(), *g_ScaleformFvfs[vb.m_Format], readWrite, dynamic, vb.m_VertexData.GetElements());
	}
}

void sfCommandBuffer::Execute()
{
	using namespace sfGlobals;

	u32* pc = m_Commands.begin();
	u32* end = m_Commands.end();

	PIX_AUTO_TAG(1, "Replay SF Buffer");

	while(pc != end)
	{
		Command cmd = *(Command*)pc;
		switch(cmd.GetId())
		{
		case SET_SHADER_VAR:
			{
				SetVarCommand& svcmd = *(SetVarCommand*)pc;
				Vec4f vec(svcmd.m_X, svcmd.m_Y, svcmd.m_Z, svcmd.m_W);
				g_Effect->SetVar(g_EffectVars[svcmd.m_Payload1], vec);
				pc += sizeof(SetVarCommand)/sizeof(u32);
			}
			break;
		case SET_SHADER_MTX:
			{
				// We don't actually want to replay this because we want the proxy's matrix to have an effect.
				// (I think? Or do we want to try to record a matrix that includes all xforms except what the top-level
				// movie clip is applying?)
				//SetMtxCommand& svcmd = *(SetMtxCommand*)pc;
				//Vec4f mtx[2];
				//mtx[0] = Vec4f(svcmd.m_X0, svcmd.m_Y0, svcmd.m_Z0, svcmd.m_W0);
				//mtx[1] = Vec4f(svcmd.m_X1, svcmd.m_Y1, svcmd.m_Z1, svcmd.m_W1);
				//g_Effect->SetVar(g_EffectVars[svcmd.m_Payload1], mtx, 2);
				pc += sizeof(SetMtxCommand)/sizeof(u32);
			}
			break;
		case SET_BLEND_STATE:
			{
				grcStateBlock::SetBlendState(g_BlendStateHandles[cmd.m_Payload1]);
				pc += sizeof(Command)/sizeof(u32);
			}
			break;
		case DRAW_INDEXED_TRIS:
			{
				DrawIndexedCommand& drawcmd = *(DrawIndexedCommand*)pc;
				g_Effect->Bind(g_Techniques[drawcmd.m_Payload1]);

				VertexBuffer& vb = m_VertexBuffers[drawcmd.m_VBIndex];
				IndexBuffer& ib = m_IndexBuffers[drawcmd.m_IBIndex];

				GRCDEVICE.SetVertexDeclaration(g_ScaleformVertexDecls[vb.m_Format]);
				GRCDEVICE.SetStreamSource(0, *(vb.m_RageVB), 0, g_ScaleformVertexDecls[vb.m_Format]->Stream0Size);
				GRCDEVICE.SetIndices(*(ib.m_RageIB));
				GRCDEVICE.DrawIndexedPrimitive(drawTris, 0, drawcmd.m_NumVerts);
				GRCDEVICE.ClearStreamSource(0);

				g_Effect->UnBind();

				pc += sizeof(DrawIndexedCommand)/sizeof(u32);
			}
			break;
		case DRAW_LINESTRIP:
			{
				DrawVertsCommand& drawcmd = *(DrawVertsCommand*)pc;
				g_Effect->Bind(g_Techniques[drawcmd.m_Payload1]);

				VertexBuffer& vb = m_VertexBuffers[drawcmd.m_VBIndex];
				GRCDEVICE.SetStreamSource(0, *(vb.m_RageVB), 0, g_ScaleformVertexDecls[vb.m_Format]->Stream0Size);
				GRCDEVICE.DrawPrimitive(drawLineStrip, drawcmd.m_VBOffset, drawcmd.m_NumVerts);
				pc += sizeof(DrawVertsCommand)/sizeof(u32);

				g_Effect->UnBind();
			}
			break;
		case COMMENT:
			{
				pc += sizeof(Command)/sizeof(u32);
				pc += cmd.m_Payload1;
			}
			break;
		default:
			sfErrorf("Unknown command %d", cmd.GetId());
			return;
		}
	}
}

#if SF_RECORD_COMMAND_BUFFER
void sfCommandBuffer::VertexBuffer::DumpAscii(fiStream* stream)
{
	int numVertices = GetVertexCount();
	char* vertices = m_VertexData.GetElements();
	fprintf(stream, "NumVerts: %d   Format: %d\n", numVertices, m_Format);
	switch(m_Format)
	{
	case GRenderer::Vertex_XY16i:
		{
			fprintf(stream, "#\tP0x\tP0y\n");
			for(int i = 0; i < numVertices; i++)
			{
				struct XY16i { u16 x, y; };
				XY16i* item = ((XY16i*)vertices) + i;
				fprintf(stream, "\t%8d\t%8d\n", item->x, item->y);
			}
		}
		break;
	case GRenderer::Vertex_XY32f:
		{
			fprintf(stream, "#\tP0x\tP0y\n");
			for(int i = 0; i < numVertices; i++)
			{
				struct XY32f { float x, y; };
				XY32f* item = ((XY32f*)vertices) + i;
				fprintf(stream, "\t%8f\t%8f\n", item->x, item->y);
			}
		}
		break;
	case GRenderer::Vertex_XY16iC32:
		{
			fprintf(stream, "#\tP0x\tP0y\tC0\n");
			for(int i = 0; i < numVertices; i++)
			{
				struct XY16iC32 { u16 x,y; u32 c; };
				XY16iC32 * item = ((XY16iC32*)vertices) + i;
				fprintf(stream, "\t%8d\t%8d\t0x%08x\n", item->x, item->y, item->c);
			}
		}
		break;
	case GRenderer::Vertex_XY16iCF32:
		{
			fprintf(stream, "#\tP0x\tP0y\tC0\tC1\n");
			for(int i = 0; i < numVertices; i++)
			{
				struct XY16iCF32 { u16 x,y; u32 c, f; };
				XY16iCF32 * item = ((XY16iCF32*)vertices) + i;
				fprintf(stream, "\t%8d\t%8d\t0x%08x\t0x%08x", item->x, item->y, item->c, item->f);
			}
		}
		break;
	default:
		break;
	}
}

void sfCommandBuffer::IndexBuffer::DumpAscii(fiStream* stream)
{
	size_t numIndices = GetIndexCount();
	char* indices = m_IndexData.GetElements();
	fprintf(stream, "NumIndices: %d   Format: %d\n", numIndices, m_Format);
	switch(m_Format)
	{
	case GRenderer::Index_16:
		{
			size_t idx = 0;
			for(size_t i = 0; i < numIndices / 3; i++)
			{
				fprintf(stream, "\t%d\t%d\t%d\n", *((u16*)indices + 3*i), *((u16*)indices + 3*i + 1), *((u16*)indices + 3*i + 2));
				idx += 3;
			}
			for(; idx < numIndices; idx++)
			{
				fprintf(stream, "\t%d\n", *((u16*)indices + idx));
			}
		}
		break;
	case GRenderer::Index_32:
		for(size_t i = 0; i < numIndices; i++)
		{
			fprintf(stream, "\t%d\n", *((u32*)indices + i));
		}
		break;
	case GRenderer::Index_None:
		break;
	}
}

void sfCommandBuffer::DumpAscii(const char* filename)
{
	fiStream* stream = ASSET.Create(filename, "");
	if (!stream) 
	{
		return;
	}

	fprintf(stream, "VertexBuffers: %d\n", m_VertexBuffers.GetCount());
	for(int i = 0; i < m_VertexBuffers.GetCount(); i++) 
	{
		fprintf(stream, "VertexBuffer %d  ", i);
		m_VertexBuffers[i].DumpAscii(stream);
	}

	fprintf(stream, "\n\nIndexBuffers: %d\n", m_IndexBuffers.GetCount());
	for(int i = 0; i < m_IndexBuffers.GetCount(); i++)
	{
		fprintf(stream, "IndexBuffer %d  ", i);
		m_IndexBuffers[i].DumpAscii(stream);
	}

	fprintf(stream, "\n\nCommandBuffer:\n");

	u32* pc = m_Commands.begin();
	u32* end = m_Commands.end();

	while(pc != end)
	{
		Command cmd = *(Command*)pc;
		switch(cmd.GetId())
		{
		case SET_SHADER_VAR:
			{
				SetVarCommand& svcmd = *(SetVarCommand*)pc;
				fprintf(stream, "\tSET_SHADER_VAR\tvar:%d x:%f y:%f z:%f w:%f\n", svcmd.m_Payload1, svcmd.m_X, svcmd.m_Y, svcmd.m_Z, svcmd.m_W);
				pc += sizeof(SetVarCommand)/sizeof(u32);
			}
			break;
		case SET_SHADER_MTX:
			{
				SetMtxCommand& svcmd = *(SetMtxCommand*)pc;
				fprintf(stream, "\tSET_SHADER_MATRIX\tvar:%d x0:%f y0:%f z0:%f w0:%f   x1:%f y1:%f z1:%f w1:%f\n", svcmd.m_Payload1, svcmd.m_X0, svcmd.m_Y0, svcmd.m_Z0, svcmd.m_W0, svcmd.m_X1, svcmd.m_Y1, svcmd.m_Z1, svcmd.m_W1);
				pc += sizeof(SetMtxCommand)/sizeof(u32);
			}
			break;
		case SET_BLEND_STATE:
			{
				fprintf(stream, "\tSET_BLEND_STATE\tstate:%d\n", cmd.m_Payload1);
				pc += sizeof(Command)/sizeof(u32);
			}
			break;
		case DRAW_INDEXED_TRIS:
			{
				DrawIndexedCommand& drawcmd = *(DrawIndexedCommand*)pc;
				fprintf(stream, "\tDRAW_INDEXED_TRIS\ttech:%d vb:%d ib:%d verts:%d vbstart:%d\n", drawcmd.m_Payload1, drawcmd.m_VBIndex, drawcmd.m_IBIndex, drawcmd.m_NumVerts, drawcmd.m_VBStartIndex);
				pc += sizeof(DrawIndexedCommand)/sizeof(u32);
			}
			break;
		case DRAW_LINESTRIP:
			{
				DrawVertsCommand& drawcmd = *(DrawVertsCommand*)pc;
				fprintf(stream, "\tDRAW_LINESTRIP\ttech:%d vb:%d verts:%d vboff:%d\n", drawcmd.m_Payload1, drawcmd.m_VBIndex, drawcmd.m_NumVerts, drawcmd.m_VBOffset);
				pc += sizeof(DrawVertsCommand)/sizeof(u32);
			}
			break;
		case COMMENT:
			{
				fprintf(stream, "# %s\n", reinterpret_cast<char*>(pc+1));
				pc += sizeof(Command)/sizeof(u32);
				pc += cmd.m_Payload1;
			}
			break;
		default:
			sfErrorf("Unknown command %d", cmd.GetId());
			stream->Close();
			return;
		}
	}

	stream->Close();
}
#endif // SF_RECORD_COMMAND_BUFFER

