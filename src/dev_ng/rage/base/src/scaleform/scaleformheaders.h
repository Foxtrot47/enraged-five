// 
// scaleform/scaleformheaders.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_SCALEFORMHEADERS_H 
#define SCALEFORM_SCALEFORMHEADERS_H 

#include "system/xtl.h"

#define USE_SCALEFORM_AMP (0)

#if __WIN32
#pragma warning ( push )
#pragma warning ( disable : 4100 )
#endif

#ifdef __SNC__
#pragma diag_suppress 828   // parameter "dummy" was never referenced
#pragma diag_suppress 194   // zero used for undefined preprocessing identifier "BLAH"
#endif

#if __DEV
#define GFC_BUILD_DEBUG
#endif 

#include "../../../scaleform/Include/GAmpRenderer.h"
#include "../../../scaleform/Include/GFile.h"
#include "../../../scaleform/Include/GFxAmpServer.h"
#include "../../../scaleform/Include/GFxDrawText.h"
#include "../../../scaleform/Include/GFxFontLib.h"
#include "../../../scaleform/Include/GFxImageResource.h"
#include "../../../scaleform/Include/GFxPlayer.h"
#include "../../../scaleform/Include/GFxLoader.h"
#include "../../../scaleform/Include/GFxLog.h"
#include "../../../scaleform/Include/GStats.h"
#include "../../../scaleform/Include/GSysAlloc.h"

#if defined(__SNC__) && __ASSERT && !RAGE_MINIMAL_ASSERTS
#pragma diag_error 828
#pragma diag_error 194
#endif

#if defined(GFX_AMP_SERVER)
CompileTimeAssert(USE_SCALEFORM_AMP); // These two must match
#else
CompileTimeAssert(!USE_SCALEFORM_AMP); // These two must match
#endif

#if __WIN32
#pragma warning ( pop )

// This is #defined in winbase.h - undefine it here or else anyone that includes script
// code after scaleform code will get compiled errors. The only places where the MS 
// GetCurrentThread are used in rage are in .cpp files - system\stack.cpp and system\timer.cpp
//#ifdef GetCurrentThread
//#undef GetCurrentThread
//#endif
#undef GetObject
#undef DrawText
#undef PlaySound

#endif

#endif // SCALEFORM_SCALEFORMHEADERS_H 
