// 
// scaleform/tweenstar.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "tweenstar.h"
#include "string/stringhash.h"
#include "atl/hashstring.h"
using namespace rage;
using namespace TweenStar;

static const float s_EaseParam = 1.70158f;

float rage::TweenStar::ComputeEasedValue(float t, EaseType Ease)
{

	switch (Ease)
	{
	case EASE_linear:
		return t;

	case EASE_quadratic_EI:
		return t*t;
	case EASE_quadratic_EO:
		return (2.0f-t)*t; // 2t - t^2;
	case EASE_quadratic_EIEO:
		if (t < 0.5f) {
			return t * t * 2.0f;
		}
		else {
			return -2.0f * t * t + 4.0f * t - 1.0f; 
		}

	case EASE_cubic_EI:
		return t * t * t;
	case EASE_cubic_EO:
		t -= 1.0f;
		return t * t * t + 1.0f;
	case EASE_cubic_EIEO:
		if (t < 0.5f) {
			return 4.0f * t * t * t;
		}
		else {
			t = 2.0f * t - 2.0f;
			return 0.5f * t * t * t + 1.0f;
		}

	case EASE_quartic_EI:
		return t * t * t * t;
	case EASE_quartic_EO:
		t -= 1.0f;
		return 1.0f - t*t*t*t;
	case EASE_quartic_EIEO:
		if (t < 0.5f) {
			return 8.0f * t * t * t * t;
		}
		else
		{
			t = 2.0f * t - 2.0f;
			return -0.5f * t * t * t * t + 1.0f;
		}

	case EASE_sine_EI:
		return -Cosf(t * PI * 0.5f) + 1.0f;
	case EASE_sine_EO:
		return Sinf(t * PI * 0.5f);
	case EASE_sine_EIEO:
		return -0.5f * Cosf(t * PI) + 0.5f;

	case EASE_back_EI:
		return t * t * ((s_EaseParam+1.0f)*t - s_EaseParam);
	case EASE_back_EO:
		t -= 1.0f;
		return t * t * ((s_EaseParam+1.0f)*t + s_EaseParam) + 1.0f;
	case EASE_back_EIEO:
		{
			float easeParam = s_EaseParam * 1.525f;
			t *= 2.0f;
			if (t < 1.0f)
			{
				return 0.5f * ( t * t * ((easeParam+1.0f) * t - easeParam));
			}
			else
			{
				t -= 2.0f;
				return 0.5f * t * t * ((easeParam+1.0f) * t + easeParam) + 1.0f;
			}
		}

	case EASE_circular_EI:
		return 1.0f - Sqrtf( 1.0f - t*t);
	case EASE_circular_EO:
		t -= 1.0f;
		return Sqrtf(1.0f - t*t);
	case EASE_circular_EIEO:
		t *= 2.0f;
		if (t < 1.0f)
		{
			return -0.5f * Sqrtf(1.0f - t * t) + 0.5f;
		}
		else
		{
			t -= 2.0f;
			return 0.5f * Sqrtf(1.0f - t * t) + 0.5f;
		}
	}

	return t;
}

class sfTweenInstance
{
public:
	sfTweenInstance() 
		: m_PropName(NULL) {}

	enum TweenableProperty {
		TWEEN_x,
		TWEEN_y,
		TWEEN_z,
		TWEEN_alpha,
		TWEEN_xscale,
		TWEEN_yscale,
		TWEEN_zscale,
		TWEEN_rotation,
		TWEEN_xrotation,
		TWEEN_yrotation,
		TWEEN_member,
	};

	TweenableProperty m_Property;
	float m_Initial, m_Delta;
	const char* m_PropName;

	void ApplyTween(float easedT, GFxValue& movieClip, GFxValue::DisplayInfo& dispInfo)
	{
		float newValue = m_Initial + m_Delta * easedT;

		switch (m_Property)
		{
		case TWEEN_x: dispInfo.SetX(newValue); break;
		case TWEEN_y: dispInfo.SetY(newValue); break;
		case TWEEN_z: dispInfo.SetZ(newValue); break;
		case TWEEN_alpha: dispInfo.SetAlpha(newValue); break;
		case TWEEN_xscale: dispInfo.SetXScale(newValue); break;
		case TWEEN_yscale: dispInfo.SetYScale(newValue); break;
		case TWEEN_zscale: dispInfo.SetZScale(newValue); break;
		case TWEEN_rotation: dispInfo.SetRotation(newValue); break;
		case TWEEN_xrotation: dispInfo.SetXRotation(newValue); break;
		case TWEEN_yrotation: dispInfo.SetYRotation(newValue); break;
		case TWEEN_member: 
			{
				GFxValue val(newValue);
				movieClip.SetMember(m_PropName, val);
			}
			break;
		default:
			break;
		}
	}
};

// All the tweens we're applying to a single movieclip
class sfTweenGroup
{
public:
	sfTweenGroup() 
		: m_StartTime(FLT_MAX)
		, m_Duration(FLT_MAX)
		, m_Ease(EASE_linear)

	{
	}

	~sfTweenGroup()
	{
	}

	enum UpdateStatus
	{
		UPDATESTATUS_NotStarted,
		UPDATESTATUS_Tweening,
		UPDATESTATUS_Complete,
	};

	void AddTween(sfTweenInstance& inst)
	{
		if (inst.m_Property == sfTweenInstance::TWEEN_member)
		{
			m_MemberInstances.PushAndGrow(inst, 4);
		}
		else
		{
			m_DisplayInstances.PushAndGrow(inst, 4);
		}
	}

	void FinishTween(bool callOnComplete)
	{
		GFxValue::DisplayInfo dispInfo;
		if (m_MovieClip.IsDefined())
		{
			if (m_MovieClip.GetDisplayInfo(&dispInfo))
			{
				for(int i = 0; i < m_DisplayInstances.GetCount(); i++)
				{
					m_DisplayInstances[i].ApplyTween(1.0f, m_MovieClip, dispInfo);
				}

				m_MovieClip.SetDisplayInfo(dispInfo);
			}	

			for(int i = 0; i < m_MemberInstances.GetCount(); i++)
			{
				m_MemberInstances[i].ApplyTween(1.0f, m_MovieClip, dispInfo);
			}
		}

		if (callOnComplete && m_OnComplete.IsObject()) // No way to tell if its a function object. Lets assume it is.
		{
			GFxValue args[2];
			args[0] = m_OnCompleteScope;
			args[1] = m_OnCompleteArgs;
			m_OnComplete.Invoke("apply", NULL, args, 2);
		}
	}

	UpdateStatus Update(float newTime)
	{
		float t = (newTime - m_StartTime) / m_Duration;

		t = Min(t, 1.0f);

		if (t > 0.0f)
		{
			if (m_MovieClip.IsDefined())
			{
				float easedT = rage::TweenStar::ComputeEasedValue(t, m_Ease);
				GFxValue::DisplayInfo dispInfo;
				if (m_MovieClip.GetDisplayInfo(&dispInfo))
				{
					for(int i = 0; i < m_DisplayInstances.GetCount(); i++)
					{
						m_DisplayInstances[i].ApplyTween(easedT, m_MovieClip, dispInfo);
					}

					m_MovieClip.SetDisplayInfo(dispInfo);
				}	

				for(int i = 0; i < m_MemberInstances.GetCount(); i++)
				{
					m_MemberInstances[i].ApplyTween(easedT, m_MovieClip, dispInfo);
				}
			}
		}
		else
		{
			return UPDATESTATUS_NotStarted;
		}

		if (t == 1.0f)
		{
			return UPDATESTATUS_Complete;
		}

		return UPDATESTATUS_Tweening;
	}

	GFxValue m_MovieClip;
	GFxValue m_OnComplete;
	GFxValue m_OnCompleteScope;
	GFxValue m_OnCompleteArgs;
	float m_StartTime;
	float m_Duration;
	TweenStar::EaseType m_Ease;

	atArray<sfTweenInstance> m_DisplayInstances;
	atArray<sfTweenInstance> m_MemberInstances;
};



class sfTweenStarLiteClass : public sfScaleformFunctionHandler
{
public:

	enum Method
	{
		TWEENSTARMETHOD_Ctor = sfScaleformFunctionHandler::CONSTRUCTOR_ID,
		TWEENSTARMETHOD_to,
		TWEENSTARMETHOD_delayCall,
		TWEENSTARMETHOD_setDelay,
		TWEENSTARMETHOD_removeAllTweens,
		TWEENSTARMETHOD_removeTweenOf,
		TWEENSTARMETHOD_endTweenOf,
		TWEENSTARMETHOD_endAllTweens,
	};

	virtual ~sfTweenStarLiteClass() {}

	// One call method to handle a bunch of function calls means we don't have to create a bunch of GFxFunctionHandler subclasses
	virtual void Call(const Params& params)
	{
		size_t method = size_t(params.pUserData);
//		sfDebugf1("Calling method %d", (u32)method);
		switch(method)
		{
			case TWEENSTARMETHOD_Ctor:
				DoConstructor(params);
				break;
			case TWEENSTARMETHOD_to:
				DoTo(params);
				break;
			case TWEENSTARMETHOD_delayCall:
				DoDelayCall(params);
				break;
			case TWEENSTARMETHOD_setDelay:
				DoSetDelay(params);
				break;
			case TWEENSTARMETHOD_removeAllTweens:
				DoRemoveAllTweens(params);
				break;
			case TWEENSTARMETHOD_removeTweenOf:
				DoRemoveTweenOf(params);
				break;
			case TWEENSTARMETHOD_endTweenOf:
				DoEndTweenOf(params);
				break;
			case TWEENSTARMETHOD_endAllTweens:
				DoEndAllTweens(params);
				break;
			default:
				sfErrorf("Unknown method ID %" SIZETFMT "d", method);
				break;
		}
	}

	void AddTween(sfTweenGroup* group, sfTweenInstance::TweenableProperty property, float initial, float final, const char* propName = NULL)
	{
		sfTweenInstance inst;
		inst.m_Property = property;
		inst.m_Initial = initial;
		inst.m_Delta = final - initial;
		inst.m_PropName = propName;
		group->AddTween(inst);
	}

	void DoConstructor(const Params& params)
	{
		CHECK_NUM_SF_ARGS("TweenStarLite", 4);

		GFxValue& mytarget =	params.pArgs[0];
		GFxValue& duration =	params.pArgs[1];
		GFxValue& vars =		params.pArgs[2];
		GFxValue& isTween =		params.pArgs[3];

		CHECK_OPT_SF_VAR(mytarget, GFxValue::VT_DisplayObject);
		CHECK_SF_VAR(duration, GFxValue::VT_Number);
		CHECK_SF_VAR(vars, GFxValue::VT_Object);
		CHECK_SF_VAR(isTween, GFxValue::VT_Boolean);

		sfTweenGroup* tweenGroup = rage_new sfTweenGroup;

		tweenGroup->m_MovieClip = mytarget;
		vars.GetMember("onComplete", &tweenGroup->m_OnComplete);
		vars.GetMember("onCompleteArgs", &tweenGroup->m_OnCompleteArgs);
		vars.GetMember("onCompleteScope", &tweenGroup->m_OnCompleteScope);

		tweenGroup->m_StartTime = (float)params.pMovie->GetMovieTimer();

		tweenGroup->m_Duration = (float)duration.GetNumber() * 1000.0f;

		GFxValue delay(0.0f);
		vars.GetMember("delay", &delay);
		if (delay.IsNumber())
		{
			tweenGroup->m_StartTime += (float)delay.GetNumber() * 1000.0f; // convert seconds to ms
		}

		GFxValue ease(0.0f);
		vars.GetMember("ease", &ease);
		if (ease.IsNumber())
		{
			tweenGroup->m_Ease = (EaseType)(int)ease.GetNumber();
		}

		if (isTween.GetBool())
		{
			sfAssertf(mytarget.IsDefined(), "Need a valid movieclip if you're doing any tweening");
			GFxValue::DisplayInfo dispInfo;
			mytarget.GetDisplayInfo(&dispInfo);

			class AddTweenVisitor : public GFxValue::ObjectVisitor
			{
			public:
				AddTweenVisitor(sfTweenStarLiteClass& me, sfTweenGroup* tweenGroup, GFxValue::DisplayInfo& dispInfo) 
					: m_TweenStar(me)
					, m_TweenGroup(tweenGroup)
					, m_DispInfo(dispInfo)
				{
				}

				virtual void Visit(const char* name, const GFxValue& tweenValue)
				{
					atHashValue objHash(name);

					if ( objHash==ATSTRINGHASH("delay",0x58e0c1a4)  
						||  objHash==ATSTRINGHASH("ease",0x4fdb55b3)  
						||  objHash==ATSTRINGHASH("onComplete",0x49155149)  
						||  objHash==ATSTRINGHASH("onCompleteScope",0x1fe425ec)  
						||  objHash==ATSTRINGHASH("onCompleteArgs",0x43c999c) )
					{
						// Internal properties, ignore
						return;
					}

					// handle wrong type
					if (!sfVerifyf(tweenValue.GetType() == GFxValue::VT_Number, "Wrong type for argument \"%s\", expected Number and got %s", name, sfScaleformManager::GetTypeName(tweenValue)))
						return;

					if (objHash==ATSTRINGHASH("_x",0xce44b0af))
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_x, (float)m_DispInfo.GetX(), (float)tweenValue.GetNumber());
					}
					else if ( objHash==ATSTRINGHASH("_y",0xbd6d8f01) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_y, (float)m_DispInfo.GetY(), (float)tweenValue.GetNumber());
					}
					else if ( objHash==ATSTRINGHASH("_z",0x3281f928) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_z, (float)m_DispInfo.GetZ(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_alpha",0xb18445fe) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_alpha, (float)m_DispInfo.GetAlpha(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_xscale",0x379949df) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_xscale, (float)m_DispInfo.GetXScale(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_yscale",0x777f7a9d) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_yscale, (float)m_DispInfo.GetYScale(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_zscale",0x9eb9b3b9) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_zscale, (float)m_DispInfo.GetZScale(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_rotation",0x17447cc) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_rotation, (float)m_DispInfo.GetRotation(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_xrotation",0xf7262233) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_xrotation, (float)m_DispInfo.GetXRotation(), (float)tweenValue.GetNumber());
					}

					else if ( objHash==ATSTRINGHASH("_yrotation",0x30ed0b96) )
					{
						m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_yrotation, (float)m_DispInfo.GetYRotation(), (float)tweenValue.GetNumber());
					}
					else 
					{
						GFxValue initialVal;
						if (m_TweenGroup->m_MovieClip.GetMember(name, &initialVal) && initialVal.IsNumber())
						{
							m_TweenStar.AddTween(m_TweenGroup, sfTweenInstance::TWEEN_member, (float)initialVal.GetNumber(), (float)tweenValue.GetNumber(), name);
						}
					}
				}

				sfTweenStarLiteClass& m_TweenStar;
				sfTweenGroup* m_TweenGroup;
				GFxValue::DisplayInfo& m_DispInfo;
			};

			AddTweenVisitor visitor(*this, tweenGroup, dispInfo);
			vars.VisitMembers(&visitor);

		}

		m_TweenGroups.PushAndGrow(tweenGroup, 4);
	}

	void DoTo(const Params& params)
	{
		CHECK_NUM_SF_ARGS("to", 3);

		GFxValue& target = params.pArgs[0];
		GFxValue& duration = params.pArgs[1];
		GFxValue& vars = params.pArgs[2];

        if (!target.IsDefined())
		{
			sfDebugf2("TweenStarLite.to() target is undefined");
			return;
        }

        CHECK_SF_VAR(target, GFxValue::VT_DisplayObject);
        CHECK_SF_VAR(duration, GFxValue::VT_Number);
        CHECK_SF_VAR(vars, GFxValue::VT_Object);

		RemoveTweenOf(target);

		GFxValue ctorArgs[4];
		ctorArgs[0] = target;
		ctorArgs[1] = duration;
		ctorArgs[2] = vars;
		ctorArgs[3].SetBoolean(true);

		params.pMovie->CreateObject(params.pRetVal, "com.rockstargames.ui.tweenStar.TweenStarLite", ctorArgs, 4);
	}

	void DoDelayCall(const Params& params)
	{
		CHECK_NUM_SF_ARGS("delayCall", 3);

		GFxValue& target = params.pArgs[0];
		GFxValue& duration = params.pArgs[1];
		GFxValue& vars = params.pArgs[2];

        if (!target.IsDefined())
        {
            sfDebugf2("TweenStarLite.delayCall() target is undefined");
            return;
        }

		CHECK_SF_VAR(target, GFxValue::VT_DisplayObject);
		CHECK_SF_VAR(duration, GFxValue::VT_Number);
		CHECK_SF_VAR(vars, GFxValue::VT_Object);

		RemoveTweenOf(target);

		GFxValue ctorArgs[4];
		ctorArgs[0] = target;
		ctorArgs[1] = duration;
		ctorArgs[2] = vars;
		ctorArgs[3].SetBoolean(false);

		params.pMovie->CreateObject(params.pRetVal, "com.rockstargames.ui.tweenStar.TweenStarLite", ctorArgs, 4);
	}

	void DoSetDelay(const Params& params)
	{
		CHECK_NUM_SF_ARGS("setDelay", 2);

		GFxValue& target = params.pArgs[0];
		GFxValue& duration = params.pArgs[1];

		CHECK_SF_VAR(target, GFxValue::VT_DisplayObject);
		CHECK_SF_VAR(duration, GFxValue::VT_Number);
	}

	void DoRemoveAllTweens(const Params& params)
	{
		CHECK_NUM_SF_ARGS("removeAllTweens", 0);

		// No-op
	}

	void RemoveTweenOf(GFxValue& target)
	{
		if (!target.IsDefined())
		{
			return;
		}

		for(int i = m_TweenGroups.GetCount()-1; i >= 0; i--)
		{
			if (m_TweenGroups[i]->m_MovieClip == target)
			{
				delete m_TweenGroups[i];
				m_TweenGroups.DeleteFast(i);
			}
		}
	}

	void DoRemoveTweenOf(const Params& params)
	{
		CHECK_NUM_SF_ARGS("removeTweenOf", 1);

		GFxValue& target = params.pArgs[0];

		CHECK_OPT_SF_VAR(target, GFxValue::VT_DisplayObject);

		RemoveTweenOf(target);
	}

	void DoEndTweenOf(const Params& params)
	{
		CHECK_NUM_SF_ARGS("endTweenOf", 2);

		GFxValue& target = params.pArgs[0];
		GFxValue& forceComplete = params.pArgs[1];

		CHECK_SF_VAR(target, GFxValue::VT_DisplayObject);
		CHECK_SF_VAR(forceComplete, GFxValue::VT_Boolean);

		// We have to do this in a kind of strange order because onComplete can start new tweens or cancel old ones
		// so we don't want to mess up the m_TweenGroups list while iterating over it.
		sfTweenGroup** groupMem = Alloca(sfTweenGroup*, m_TweenGroups.GetCount());
		atUserArray<sfTweenGroup*> removedGroups(groupMem, (u16)m_TweenGroups.GetCount());

		for(int i = 0; i < m_TweenGroups.GetCount(); i++)
		{
			if (m_TweenGroups[i]->m_MovieClip == target) 
			{
				removedGroups.Push(m_TweenGroups[i]);
				m_TweenGroups.DeleteFast(i);
				i--;
			}
		}

		for(int i = 0; i < removedGroups.GetCount(); i++)
		{
			removedGroups[i]->FinishTween(forceComplete.GetBool());
			delete removedGroups[i];
		}
	}

	void DoEndAllTweens(const Params& params)
	{
		CHECK_NUM_SF_ARGS("endAllTweens", 1);

		GFxValue& forceComplete = params.pArgs[0];

		CHECK_SF_VAR(forceComplete, GFxValue::VT_Boolean);

		// No-op
	}

	void OnUpdate(sfScaleformMovieView& movie, float dt)
	{
		// Add dt here to the current time becuase this gets called _before_ the real scaleform update, so dt hasn't been applied yet
		float newTime = (float)movie.GetMovieView().GetMovieTimer() + dt;

		// We have to do this in a kind of strange order because onComplete can start new tweens or cancel old ones
		// so we don't want to mess up the m_TweenGroups list while iterating over it.
		sfTweenGroup** groupMem = Alloca(sfTweenGroup*, m_TweenGroups.GetCount());
		atUserArray<sfTweenGroup*> completeGroups(groupMem, (u16)m_TweenGroups.GetCount());

		for(int i = 0; i < m_TweenGroups.GetCount(); i++)
		{
			sfTweenGroup::UpdateStatus status = m_TweenGroups[i]->Update(newTime);

			if (status == sfTweenGroup::UPDATESTATUS_Complete)
			{
				sfTweenGroup* group = m_TweenGroups[i];
				completeGroups.Push(group);
				m_TweenGroups.DeleteFast(i);
				i--;
			}
		}

		for(int i = 0; i < completeGroups.GetCount(); i++)
		{
			completeGroups[i]->FinishTween(true);
			delete completeGroups[i];
		}
	}

	void OnDelete(sfScaleformMovieView& /*movie*/)
	{
		// end all the outstanding tweens
		for(int i = m_TweenGroups.GetCount()-1; i >= 0; i--)
		{
			delete m_TweenGroups[i];
		}
	}

	void AddStaticMethods(GFxMovieView& movie, GFxValue& classObject, GFxValue& /*originalClass*/)
	{
		AddMethodToAsObject(movie, classObject, "to",				TWEENSTARMETHOD_to);
		AddMethodToAsObject(movie, classObject, "delayCall",		TWEENSTARMETHOD_delayCall);
		AddMethodToAsObject(movie, classObject, "setDelay",			TWEENSTARMETHOD_setDelay);
		AddMethodToAsObject(movie, classObject, "removeAllTweens",	TWEENSTARMETHOD_removeAllTweens);
		AddMethodToAsObject(movie, classObject, "removeTweenOf",	TWEENSTARMETHOD_removeTweenOf);
		AddMethodToAsObject(movie, classObject, "endTweenOf",		TWEENSTARMETHOD_endTweenOf);
		AddMethodToAsObject(movie, classObject, "endAllTweens",		TWEENSTARMETHOD_endAllTweens);
	}

protected:
	atArray<sfTweenGroup*> m_TweenGroups;

};


namespace rage {

void sfInstallTweenstar(sfScaleformMovieView& sfmovie)
{
	sfmovie.InstallFunctionHandler<sfTweenStarLiteClass>("_global.com.rockstargames.ui.tweenStar.TweenStarLite", true, true);
}

}
