template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _SX_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _SX_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _SX_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v1__SW_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MXY_v1_v2_, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v1__SW_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _SX_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _SX_v1_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _SX_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _SX_v1_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v1__SW_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v1__SW_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _MXY_v1__SY_v2__, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v1__SW_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v1__SW_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v1__SW_v2__, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _SX_v1_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _SX_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _SX_v1_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SW_v1___SY_v1__ = V4MergeXY( _SW_v1_, _SY_v1_ );
	return V4MergeXY( v1, _MXY__SW_v1___SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SW_v1___SY_v2__ = V4MergeXY( _SW_v1_, _SY_v2_ );
	return V4MergeXY( v1, _MXY__SW_v1___SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _MXY_v1__SW_v1__, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _MXY_v1__SW_v1__, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _MXY_v1_v2_, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _MXY_v1_v2_, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v1__SW_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _SX_v1_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _SX_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _SX_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _SX_v1_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _SX_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _MXY_v1__SZ_v1__, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v1__SW_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v1__SW_v1__, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v1__SW_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _MXY_v1__SZ_v2__, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v1__SW_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v1__SW_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,X2,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _MXY_v1__SW_v2__, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _SX_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _SX_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _SX_v1_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _SX_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v1__SW_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _MXY_v1_v2_, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MXY_v1_v2_, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _MXY_v1_v2_, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v1__SW_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Y2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _SX_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _SX_v1_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _SX_v1_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _SX_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v1__SW_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v1__SW_v1__, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v1__SW_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _MXY_v1_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v1__SY_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _MXY_v1__SY_v2__, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v1__SW_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v1__SW_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,Z2,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _SX_v1_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _SX_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _SX_v1_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SW_v2___SY_v1__ = V4MergeXY( _SW_v2_, _SY_v1_ );
	return V4MergeXY( v1, _MXY__SW_v2___SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SW_v2___SY_v2__ = V4MergeXY( _SW_v2_, _SY_v2_ );
	return V4MergeXY( v1, _MXY__SW_v2___SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v1__SZ_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v1__SW_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v1__SW_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _MXY_v1_v2_, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _MXY_v1_v2_, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v1__SY_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v1__SY_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v1__SZ_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _MXY_v1__SW_v2__, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v1__SW_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X1,W2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _MXY_v1__SW_v2__, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _MXY_v1__SX_v1__, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _SY_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _SY_v1_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _SY_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _SY_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v1__SW_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _MXY_v1_v2_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v1_v2_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v1_v2_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v1__SX_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v1__SW_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v1__SW_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v1__SX_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v1__SW_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v1__SX_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _SY_v1_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _SY_v1_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _SY_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _SY_v1_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MXY_v1_v2_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MXY_v1_v2_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v1__SX_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v1__SX_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v1__SX_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _SY_v1_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _SY_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _SY_v1_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v1__SW_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v1__SX_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v1__SX_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v1__SX_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v1__SW_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _SY_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _SY_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _SY_v1_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _SY_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v1__SW_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _MXY_v1__SX_v2__, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v1_v2_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v1_v2_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _MXY_v1_v2_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,X2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v1__SX_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _SY_v1_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _SY_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _SY_v1_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _SY_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _DOI_v1_v1_4_, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v1__SW_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v1__SW_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v1__SX_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v1__SW_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Y2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _SY_v1_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _SY_v1_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _SY_v1_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _SY_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v1__SW_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v1__SX_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _MXY_v1_v2_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MXY_v1_v2_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MXY_v1_v2_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,Z2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v1__SW_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v1__SX_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v1__SX_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v1__SX_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v1__SX_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _SY_v1_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _SY_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _SY_v1_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _DOI_v1_v1_4_, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v1__SW_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v1__SW_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v1__SW_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v1__SX_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v1__SX_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v1__SX_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v1__SX_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _MXY_v1_v2_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _MXY_v1__SZ_v2__, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v1__SZ_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v1__SW_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y1,W2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v1__SW_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v1__SX_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v1__SY_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _SZ_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _SZ_v1_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _SZ_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _SZ_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SX_v1___SZ_v1__ = V4MergeXY( _SX_v1_, _SZ_v1_ );
	return V4MergeZW( v1, _MXY__SX_v1___SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SX_v1___SZ_v2__ = V4MergeXY( _SX_v1_, _SZ_v2_ );
	return V4MergeZW( v1, _MXY__SX_v1___SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v1__SX_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v1__SX_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v1__SY_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v1__SY_v2__, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v1__SW_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _MZW_v1__SW_v2__, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v1__SX_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v1__SX_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _SZ_v1_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _SZ_v1_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _SZ_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _SZ_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v1__SX_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v1__SX_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _MZW_v1_v2_, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v1_v2_, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v1_v2_, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v1__SW_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v1__SX_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v1__SX_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v1__SX_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v1__SX_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v1__SX_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v1__SX_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v1__SW_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v1__SW_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v1__SY_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _SZ_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _SZ_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v1__SX_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v1__SX_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v1__SY_v1__, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _SZ_v1_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _SZ_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _SZ_v1_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _SZ_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SX_v2___SZ_v1__ = V4MergeXY( _SX_v2_, _SZ_v1_ );
	return V4MergeZW( v1, _MXY__SX_v2___SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SX_v2___SZ_v2__ = V4MergeXY( _SX_v2_, _SZ_v2_ );
	return V4MergeZW( v1, _MXY__SX_v2___SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v1__SX_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,X2,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _MZW_v1__SW_v2__, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v1__SX_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v1__SX_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _SZ_v1_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _SZ_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _SZ_v1_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _SZ_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v1__SX_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v1__SX_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _MZW_v1__SY_v2__, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v1_v2_, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v1_v2_, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _MZW_v1_v2_, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Y2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v1__SW_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _MZW_v1__SX_v1__, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v1__SX_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v1__SX_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v1__SX_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v1__SY_v1__, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v1__SY_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _SZ_v1_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _SZ_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _SZ_v1_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _SZ_v1_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _SZ_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SZ_v2___SX_v1__ = V4MergeXY( _SZ_v2_, _SX_v1_ );
	return V4MergeZW( v1, _MXY__SZ_v2___SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SZ_v2___SX_v2__ = V4MergeXY( _SZ_v2_, _SX_v2_ );
	return V4MergeZW( v1, _MXY__SZ_v2___SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v1__SX_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _MZW_v1__SX_v2__, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v1__SX_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v1__SX_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v1__SY_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _MZW_v1__SY_v2__, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _MZW_v1_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v1__SW_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,Z2,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v1__SW_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v1__SX_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v1__SX_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v1__SX_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v1__SY_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v1__SY_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _SZ_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _SZ_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _SZ_v1_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v1__SX_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v1__SX_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v1__SX_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v1__SY_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v1__SY_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _MZW_v1_v2_, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _MZW_v1_v2_, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v1__SW_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _MZW_v1__SW_v2__, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v1__SW_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z1,W2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _MZW_v1__SW_v2__, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _SW_v1_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _SW_v1_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _SW_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _SW_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MZW_v1__SY_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _MZW_v1_v2_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _MZW_v1_v2_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v1__SY_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _SW_v1_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _SW_v1_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _SW_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _SW_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MZW_v1__SY_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v1__SY_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MZW_v1__SY_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _SW_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _SW_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _SW_v1_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MZW_v1__SY_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MZW_v1_v2_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v1_v2_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v1_v2_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MZW_v1__SY_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MZW_v1__SY_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MZW_v1__SY_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MZW_v1__SY_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MZW_v1__SY_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _SW_v1_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _SW_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _SW_v1_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _SW_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _MZW_v1_v2_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,X2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _MZW_v1_v2_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v1__SY_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MZW_v1__SY_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _MZW_v1__SZ_v1__, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _SW_v1_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _SW_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _SW_v1_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _SW_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v1__SY_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Y2,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _DOI_v1_v1_4_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MZW_v1__SY_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _SW_v1_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _SW_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _SW_v1_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _SW_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _DOI_v1_v2_4_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MZW_v1__SY_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v1_v2_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v1_v2_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,Z2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MZW_v1_v2_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v1_4_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MZW_v1__SY_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v1__SY_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MZW_v1__SY_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MZW_v1__SZ_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _SW_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _SW_v1_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _SW_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _SW_v1_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v2_4_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MZW_v1__SY_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v1__SY_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MZW_v1__SY_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v1__SZ_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W1,W2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _MZW_v1_v2_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _MXY_v2__SZ_v1__, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v2__SW_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _MXY_v2__SW_v1__, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v2__SW_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _SX_v2_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _SX_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _SX_v2_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _SX_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _SX_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _MXY_v2__SZ_v2__, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MXY_v2__SW_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MXY_v2__SW_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v2__SW_v2__, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _MXY_v2_v1_, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _MXY_v2_v1_, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MXY_v2_v1_, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v2__SW_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _SX_v2_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _SX_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _SX_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _SX_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MXY_v2__SW_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _MXY_v2__SY_v1__, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v2__SW_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v2__SW_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _SX_v2_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _SX_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _SX_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _SX_v2_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MXY_v2__SW_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MXY_v2__SW_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _MXY_v2__SW_v2__, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _MXY_v2_v1_, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _MXY_v2_v1_, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _MXY_v2__SW_v1__, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _MXY_v2__SW_v1__, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _SX_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _SX_v2_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _SX_v2_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SW_v1___SY_v1__ = V4MergeXY( _SW_v1_, _SY_v1_ );
	return V4MergeXY( v2, _MXY__SW_v1___SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SW_v1___SY_v2__ = V4MergeXY( _SW_v1_, _SY_v2_ );
	return V4MergeXY( v2, _MXY__SW_v1___SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MXY_v2__SW_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,X2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MXY_v2_v1_, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v2__SW_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _SX_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _SX_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _SX_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MXY_v2__SW_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Y2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _MXY_v2_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _MXY_v2__SY_v1__, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v2__SY_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v2__SW_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _MXY_v2__SW_v1__, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v2__SW_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _SX_v2_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _SX_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _SX_v2_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _SX_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MXY_v2__SW_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MXY_v2__SW_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,Z2,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _MXY_v2_v1_, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _MXY_v2_v1_, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v2__SY_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v2__SY_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v2__SZ_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v2__SW_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v2__SW_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _SX_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _SX_v2_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _SX_v2_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SW_v2___SY_v1__ = V4MergeXY( _SW_v2_, _SY_v1_ );
	return V4MergeXY( v2, _MXY__SW_v2___SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SW_v2___SY_v2__ = V4MergeXY( _SW_v2_, _SY_v2_ );
	return V4MergeXY( v2, _MXY__SW_v2___SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MXY_v2__SZ_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW__SW_v2__v1_ = V4MergeZW( _SW_v2_, v1 );
	return V4MergeXY( _MXY_v2__SW_v2__, _MZW__SW_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MXY_v2__SW_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<X2,W2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW__SW_v2__v2_ = V4MergeZW( _SW_v2_, v2 );
	return V4MergeXY( _MXY_v2__SW_v2__, _MZW__SW_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _MXY_v2__SX_v1__, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _MXY_v2_v1_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v2_v1_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _MXY_v2_v1_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _SY_v2_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _SY_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _SY_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _SY_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MXY_v2__SW_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v2__SX_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v2__SW_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v2__SX_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _SY_v2_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _SY_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _SY_v2_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _SY_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MXY_v2__SW_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MXY_v2__SW_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v2__SX_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MXY_v2_v1_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _MXY_v2_v1_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MXY_v2_v1_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _SY_v2_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _SY_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _SY_v2_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _SY_v2_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MXY_v2__SW_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v2__SX_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v2__SX_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v2__SX_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v2__SW_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v2__SX_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v2__SX_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v2__SX_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _SY_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _SY_v2_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _SY_v2_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MXY_v2__SW_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W1,W2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MXY_v2__SW_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v2_v1_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v2_v1_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _MXY_v2_v1_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _MXY_v2__SX_v2__, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _SY_v2_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _SY_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _SY_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _SY_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MXY_v2__SW_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,X2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v2__SX_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v2__SW_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v2__SX_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MXY_v2__SW_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MXY_v2__SW_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Y2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MXY_v2_v1_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MXY_v2_v1_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MXY_v2__SX_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _SY_v2_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _SY_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _SY_v2_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _SY_v2_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,Z2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _DOI_v1_v2_12_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v2__SX_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v2__SX_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v2__SX_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v2__SX_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _MXY_v2_v1_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v2__SZ_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v2__SW_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v2__SW_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v2__SW_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v2__SX_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v2__SX_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v2__SX_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MXY_v2__SX_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _SY_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _SY_v2_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _SY_v2_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _DOI_v1_v2_12_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MXY_v2__SW_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Y2,W2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MXY_v2__SW_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v2__SX_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _MZW_v2__SW_v1__, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v2__SX_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v2__SX_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v2__SY_v2__, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _SZ_v2_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _SZ_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _SZ_v2_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeXY( _SZ_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SX_v1___SZ_v1__ = V4MergeXY( _SX_v1_, _SZ_v1_ );
	return V4MergeZW( v2, _MXY__SX_v1___SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SX_v1___SZ_v2__ = V4MergeXY( _SX_v1_, _SZ_v2_ );
	return V4MergeZW( v2, _MXY__SX_v1___SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v2__SX_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v2__SX_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _MZW_v2__SY_v1__, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v1_ = V4MergeXY( _SY_v1_, v1 );
	return V4MergeXY( _MZW_v2_v1_, _MXY__SY_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY__SY_v1__v2_ = V4MergeXY( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v2_v1_, _MXY__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW__SY_v1__v2_ = V4MergeZW( _SY_v1_, v2 );
	return V4MergeXY( _MZW_v2_v1_, _MZW__SY_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v2__SW_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v2__SX_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v2__SX_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _SZ_v2_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _SZ_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _SZ_v2_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _SZ_v2_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _MZW_v2__SX_v1__, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v2__SX_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v2__SX_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v2__SX_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _MZW_v2__SY_v1__, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v2__SW_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v2__SX_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v2__SX_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _MZW_v2__SX_v2__, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v2__SX_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _MZW_v2__SY_v2__, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _SZ_v2_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _SZ_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _SZ_v2_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _SZ_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MZW_v1__SW_v2__ = V4MergeZW( v1, _SW_v2_ );
	return V4MergeXY( _SZ_v2_, _MZW_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SZ_v1___SX_v1__ = V4MergeXY( _SZ_v1_, _SX_v1_ );
	return V4MergeZW( v2, _MXY__SZ_v1___SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SZ_v1___SX_v2__ = V4MergeXY( _SZ_v1_, _SX_v2_ );
	return V4MergeZW( v2, _MXY__SZ_v1___SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v2__SX_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v2__SX_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v2__SX_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _MZW_v2_v1_, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _MZW_v2_v1_, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v2__SW_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MZW__SW_v1__v1_ = V4MergeZW( _SW_v1_, v1 );
	return V4MergeXY( _MZW_v2__SW_v1__, _MZW__SW_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v2__SW_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MZW__SW_v1__v2_ = V4MergeZW( _SW_v1_, v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _MZW__SW_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v2__SX_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v2__SX_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v2__SX_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _SZ_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _SZ_v2_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _SZ_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v2__SX_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v2__SX_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v2__SY_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v2__SY_v1__, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v2__SW_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _MZW_v2__SW_v1__, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v2__SX_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeXY( _MZW_v2__SY_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _SZ_v2_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _SZ_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _SZ_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,Z2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeXY( _SZ_v2_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SX_v2___SZ_v1__ = V4MergeXY( _SX_v2_, _SZ_v1_ );
	return V4MergeZW( v2, _MXY__SX_v2___SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,X2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SX_v2___SZ_v2__ = V4MergeXY( _SX_v2_, _SZ_v2_ );
	return V4MergeZW( v2, _MXY__SX_v2___SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v2__SX_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v2__SX_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v2_v1_, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v2_v1_, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _MZW_v2_v1_, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v2__SW_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeXY( _MZW_v2__SX_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v2__SX_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY__SY_v2__v1_ = V4MergeXY( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MXY__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MZW__SY_v2__v1_ = V4MergeZW( _SY_v2_, v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MZW__SY_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY__SY_v2__v2_ = V4MergeXY( _SY_v2_, v2 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MXY__SY_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _SZ_v2_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _SZ_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _SZ_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Y2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _SZ_v2_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v2__SX_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v2__SX_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v2__SX_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v2__SX_v1__ = V4MergeZW( v2, _SX_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _MZW_v2_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v2__SW_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v2__SW_v1__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v2__SX_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeXY( _MZW_v2__SX_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v2__SX_v2__ = V4MergeZW( v2, _SX_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v2__SX_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeXY( _MZW_v2__SY_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,Z2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v2_v1_8_ = vec_sld( v2, v1, 8 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v2_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v2__SY_v1__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v2__SY_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v2__SW_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v2_v1_12_ = vec_sld( v2, v1, 12 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v2_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v2_v2_12_ = vec_sld( v2, v2, 12 );
	return V4MergeXY( _MZW_v2__SY_v2__, _DOI_v2_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeXY( _MZW_v2__SY_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _SZ_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<Z2,W2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _SZ_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY__SX_v1__v2_ = V4MergeXY( _SX_v1_, v2 );
	return V4MergeZW( _MZW_v2_v1_, _MXY__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _MZW_v2_v1_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v1_ = V4MergeZW( _SX_v1_, v1 );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW__SX_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW__SX_v1__v2_ = V4MergeZW( _SX_v1_, v2 );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW__SX_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MZW_v2__SY_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _SX_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_8_ = vec_sld( v1, v1, 8 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v1_v1_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v1__SZ_v1__ = V4MergeXY( v1, _SZ_v1_ );
	return V4MergeXY( _SW_v2_, _MXY_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeXY( _SW_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MXY_v1__SY_v2__ = V4MergeXY( v1, _SY_v2_ );
	return V4MergeXY( _SW_v2_, _MXY_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeXY( _SW_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v2__SY_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v1__SW_v2__ = V4MergeXY( v1, _SW_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v1__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MZW_v2__SY_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Y2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v2__SY_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _SY_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_12_ = vec_sld( v1, v1, 12 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v1_v1_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MXY_v1_v2_ = V4MergeXY( v1, v2 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MXY_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v1__SX_v1__ = V4MergeXY( v1, _SX_v1_ );
	return V4MergeZW( _SW_v2_, _MXY_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v1__SW_v1__ = V4MergeXY( v1, _SW_v1_ );
	return V4MergeZW( _SW_v2_, _MXY_v1__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v1__SX_v2__ = V4MergeXY( v1, _SX_v2_ );
	return V4MergeZW( _SW_v2_, _MXY_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v1__SZ_v2__ = V4MergeXY( v1, _SZ_v2_ );
	return V4MergeZW( _SW_v2_, _MXY_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,Y1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MZW_v2__SY_v1__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _MZW_v2_v1_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v2_v1_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _MZW_v2_v1_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v1_ = V4MergeXY( _SZ_v1_, v1 );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY__SZ_v1__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY__SZ_v1__v2_ = V4MergeXY( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW__SZ_v1__v2_ = V4MergeZW( _SZ_v1_, v2 );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW__SZ_v1__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,Y2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MZW_v2__SY_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,Z2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _SZ_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MZW_v1__SX_v1__ = V4MergeZW( v1, _SX_v1_ );
	return V4MergeXY( _SW_v2_, _MZW_v1__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeXY( _SW_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW_v1__SX_v2__ = V4MergeZW( v1, _SX_v2_ );
	return V4MergeXY( _SW_v2_, _MZW_v1__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeXY( _SW_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MZW_v2__SY_v1__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MZW_v2__SY_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MZW_v2__SY_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MZW_v1_v2_ = V4MergeZW( v1, v2 );
	return V4MergeZW( _MZW_v2__SY_v2__, _MZW_v1_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v1_4_ = vec_sld( v1, v1, 4 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v1_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _SW_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_4_ = vec_sld( v1, v2, 4 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v1_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v1__SY_v1__ = V4MergeZW( v1, _SY_v1_ );
	return V4MergeZW( _SW_v2_, _MZW_v1__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v1__SZ_v1__ = V4MergeZW( v1, _SZ_v1_ );
	return V4MergeZW( _SW_v2_, _MZW_v1__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v1__SY_v2__ = V4MergeZW( v1, _SY_v2_ );
	return V4MergeZW( _SW_v2_, _MZW_v1__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W1,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v1__SZ_v2__ = V4MergeZW( v1, _SZ_v2_ );
	return V4MergeZW( _SW_v2_, _MZW_v1__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MZW_v2__SY_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _MZW_v2_v1_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _MZW_v2_v1_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY__SX_v2__v1_ = V4MergeXY( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v1_ = V4MergeZW( _SX_v2_, v1 );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW__SX_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MZW__SX_v2__v2_ = V4MergeZW( _SX_v2_, v2 );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW__SX_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _SX_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_8_ = vec_sld( v1, v2, 8 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v1_v2_8_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MXY_v2__SY_v1__ = V4MergeXY( v2, _SY_v1_ );
	return V4MergeXY( _SW_v2_, _MXY_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeXY( _SW_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeXY( _SW_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,X2,W2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY_v2__SZ_v2__ = V4MergeXY( v2, _SZ_v2_ );
	return V4MergeXY( _SW_v2_, _MXY_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Y1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v2__SY_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Y1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MZW_v2__SY_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _MZW_v2__SZ_v1__, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,X2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _MXY_v2__SW_v2__ = V4MergeXY( v2, _SW_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY_v2__SW_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Y2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v2__SY_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MXY_v2_v1_ = V4MergeXY( v2, v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MXY_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _SY_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,Z2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v1_v2_12_ = vec_sld( v1, v2, 12 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v1_v2_12_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SX_v1_ = V4SplatX( v1 );
	Vector_4V _MXY_v2__SX_v1__ = V4MergeXY( v2, _SX_v1_ );
	return V4MergeZW( _SW_v2_, _MXY_v2__SX_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MXY_v2__SZ_v1__ = V4MergeXY( v2, _SZ_v1_ );
	return V4MergeZW( _SW_v2_, _MXY_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MXY_v2__SW_v1__ = V4MergeXY( v2, _SW_v1_ );
	return V4MergeZW( _SW_v2_, _MXY_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Y2,W2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SX_v2_ = V4SplatX( v2 );
	Vector_4V _MXY_v2__SX_v2__ = V4MergeXY( v2, _SX_v2_ );
	return V4MergeZW( _SW_v2_, _MXY_v2__SX_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,X1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,X1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _DOI_v2_v1_4_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,Y1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MZW_v2__SY_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,Z1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v2_v1_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,W1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v2_v1_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MZW_v2_v1_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,X2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,X2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _DOI_v2_v2_4_, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,Y2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	return V4MergeZW( _MZW_v2__SY_v2__, _SZ_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,Z2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MXY__SZ_v2__v1_ = V4MergeXY( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MXY__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MZW__SZ_v2__v1_ = V4MergeZW( _SZ_v2_, v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MZW__SZ_v2__v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,Z2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MXY__SZ_v2__v2_ = V4MergeXY( _SZ_v2_, v2 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MXY__SZ_v2__v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,W2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeXY( _SW_v2_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,W2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SW_v1_ = V4SplatW( v1 );
	Vector_4V _MZW_v2__SW_v1__ = V4MergeZW( v2, _SW_v1_ );
	return V4MergeXY( _SW_v2_, _MZW_v2__SW_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,Z2,W2,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeXY( _SW_v2_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,X1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,X1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,X1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _DOI_v2_v1_4_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y1,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MZW_v2__SY_v1__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v2__SY_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MZW_v2__SY_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Z1,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Z1,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Z1,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MZW_v2__SZ_v1__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,W1,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,W1,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,W1,Y2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v2__SY_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,W1,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _MZW_v2_v1_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,X2,Y1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SY_v1_ = V4SplatY( v1 );
	Vector_4V _MZW_v2__SY_v1__ = V4MergeZW( v2, _SY_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW_v2__SY_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,X2,Z1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v1_ = V4SplatZ( v1 );
	Vector_4V _MZW_v2__SZ_v1__ = V4MergeZW( v2, _SZ_v1_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW_v2__SZ_v1__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,X2,Z2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	return V4MergeZW( _DOI_v2_v2_4_, _MZW_v2__SZ_v2__ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MZW_v2__SY_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v2__SY_v2__, _DOI_v2_v2_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Y2,W2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SY_v2_ = V4SplatY( v2 );
	Vector_4V _MZW_v2__SY_v2__ = V4MergeZW( v2, _SY_v2_ );
	Vector_4V _SW_v2_ = V4SplatW( v2 );
	return V4MergeZW( _MZW_v2__SY_v2__, _SW_v2_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Z2,X1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v2_v1_4_ = vec_sld( v2, v1, 4 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v2_v1_4_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Z2,W1>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _MZW_v2_v1_ = V4MergeZW( v2, v1 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _MZW_v2_v1_ );
}

template <>
__forceinline Vector_4V_Out V4PermuteTwo<W2,W2,Z2,X2>( Vector_4V_In v1, Vector_4V_In v2 )
{
	Vector_4V _SZ_v2_ = V4SplatZ( v2 );
	Vector_4V _MZW_v2__SZ_v2__ = V4MergeZW( v2, _SZ_v2_ );
	Vector_4V _DOI_v2_v2_4_ = vec_sld( v2, v2, 4 );
	return V4MergeZW( _MZW_v2__SZ_v2__, _DOI_v2_v2_4_ );
}

