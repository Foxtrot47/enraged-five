#if UNIQUE_VECTORIZED_TYPE

namespace rage
{
namespace Vec
{
namespace Util
{

//================================================
//	VECTOR(4) UTILITY FUNCS (Vector_4V params)
//================================================

Vector_4V_Out	V4SinHelper( Vector_4V_In inVector, Vector_4V_In N );

} // namespace Util
} // namespace Vec
} // namespace rage

#endif // UNIQUE_VECTORIZED_TYPE
