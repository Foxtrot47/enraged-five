
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __SPU

#include "boolv.cpp"
#include "quatf.cpp"
#include "quatv.cpp"
#include "quatv_soa.cpp"
#include "scalarv.cpp"
#include "scalarv_soa.cpp"
#include "vec2f.cpp"
#include "vec2v.cpp"
#include "vec2v_soa.cpp"
#include "vec3f.cpp"
#include "vec3v.cpp"
#include "vec3v_soa.cpp"
#include "vec4f.cpp"
#include "vec4v.cpp"
#include "vec4v_soa.cpp"
#include "mat33v.cpp"
#include "mat33v_soa.cpp"
#include "mat34v.cpp"
#include "mat34v_soa.cpp"
#include "mat44v.cpp"
#include "mat44v_soa.cpp"
#include "vecbool1v_soa.cpp"
#include "vecbool2v_soa.cpp"
#include "vecbool3v_soa.cpp"
#include "vecbool4v_soa.cpp"
#include "vecboolv.cpp"
#include "vectorutility.cpp"
#include "vectormath.cpp"
#include "mathops.cpp"

#endif
