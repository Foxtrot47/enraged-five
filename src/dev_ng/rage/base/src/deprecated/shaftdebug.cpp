// 
// spatialdata/shaftdebug.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


#include "spatialdata/shaftdebug.h"

#include "bank/bank.h"
#include "grcore/viewport.h"



using namespace rage;


spdShaftDebug *spdShaftDebug::sm_Instance = NULL;

#if __DEV


void spdShaftDebug::InitClass()
{
	Assert(!sm_Instance);
	sm_Instance = rage_new spdShaftDebug();
}

void spdShaftDebug::ShutdownClass()
{
	delete sm_Instance;
	sm_Instance = NULL;
}

spdShaftDebug::spdShaftDebug()
	: m_ShaftIndex(-1)
	, m_DrawShadowShafts(false)
	, m_LockShadowShafts(false)
	, m_UseSolidShafts(true)
	, m_DepthTest(true)
{
	m_Shafts.Resize(MaxDebugShafts);
	sysMemSet(m_ShaftValid, 0, sizeof(m_ShaftValid));
}

void spdShaftDebug::StoreDebugShaft(int index, const spdShaft &shaft)
{
	if (!m_LockShadowShafts)
	{
		m_Shafts[index] = shaft;
		m_ShaftValid[index] = true;
	}
}

void spdShaftDebug::StoreDebugShaft(int index, const grcViewport &viewport)
{
	if (!m_LockShadowShafts)
	{
		m_Shafts[index].Set(viewport, RCC_MATRIX34(viewport.GetCameraMtx()));
		m_ShaftValid[index] = true;
	}
}

void spdShaftDebug::DebugDraw() const
{
	if (m_DrawShadowShafts)
	{
		for (int i=0; i<MaxDebugShafts; i++)
		{
			if (m_ShaftIndex == -1 || m_ShaftIndex == i)
			{
				if (m_ShaftValid[i])
				{
					m_Shafts[i].DrawDebug(m_UseSolidShafts, m_DepthTest);
				}
			}
		}
	}
}




#if __BANK
void spdShaftDebug::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Shaft Debugging");
	bank.AddToggle("Lock Shafts", &m_LockShadowShafts);
	bank.AddToggle("Draw Shafts", &m_DrawShadowShafts);
	bank.AddToggle("Use Solid Shafts", &m_UseSolidShafts);
	bank.AddToggle("Use Depth Buffer", &m_DepthTest);
	bank.AddSlider("Shaft To Visualize", &m_ShaftIndex, -1, MaxDebugShafts-1, 1.0f);
	bank.PopGroup();
}
#endif // __BANK



#endif // __DEV
