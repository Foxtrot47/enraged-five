// 
// spatialdata/shaft.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaft.h"
#include "shaft_inline.h"

#include "polygon.h"

#include "grcore/im.h"
#include "grcore/state.h"
#include "rmcore/drawable.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"
#include "vectormath/layoutconvert.h"
#include "vectormath/legacyconvert.h"

#define SOA_OPTIMIZED_ISVISIBLE 0


using namespace rage;

float spdApical::GetDistanceToViewpoint(const Vector3 &pos) const
{
	return (GetPosition() - pos).Mag();
}

// for projective shafts only
void spdShaft::SetX1(float x1)
{
	spdApical::SetX1(x1);

	float near = GetViewport().GetNearClip();
	float width = 2.0f * near * GetViewport().GetTanHFOV();
#if FAST_SPDVIEWPORT
	float fx1 = x1 * GetViewport().GetInvWidth();
#else
	float fx1 = x1 / GetViewport().GetWidth();
#endif
	float localx1 = (fx1 * width) - (width * 0.5f);

	Vector3 p;
	Vector3 n;

	p.Set(localx1, 0.0f, -near);
	if (m_IsOrtho)
	{
#if FAST_SHAFT
		n = VEC3V_TO_VECTOR3( Vec3V(V_X_AXIS_WZERO) );
#else
		n.Set(1.0f,0.0f,0.0f);
#endif
	}
	else
	{
#if FAST_SHAFT
		n.Cross(p, VEC3V_TO_VECTOR3( Negate(Vec3V(V_Y_AXIS_WZERO)) ) );
#else
		n.Cross(p, Vector3(0.0f, -1.0f, 0.0f));
#endif
		n.Normalize();
	}

	spdPlane &local = LocalPlanes[PLANE_LEFT];
	spdPlane &world = WorldPlanes[PLANE_LEFT];

#if FAST_SHAFT
	local.SetPair( p, n, GetMatrix(), world );
#else
	local.Set(p, n);
	local.Transform(GetMatrix(), world);
#endif
	CornersUpToDate = false;

	// TODO: Update the cone?
}

// for projective shafts only
void spdShaft::SetX2(float x2)
{
	spdApical::SetX2(x2);

	float near_ = GetViewport().GetNearClip();
	float width = 2.0f * near_ * GetViewport().GetTanHFOV();
#if FAST_SPDVIEWPORT
	float fx2 = x2 * GetViewport().GetInvWidth();
#else
	float fx2 = x2 / GetViewport().GetWidth();
#endif
	float localx2 = (fx2 * width) - (width * 0.5f);

	Vector3 p(localx2, 0.0f, -near_);
	Vector3 n;
	if (m_IsOrtho)
	{
#if FAST_SHAFT
		n = VEC3V_TO_VECTOR3( Negate(Vec3V(V_X_AXIS_WZERO)) );
#else
		n.Set(-1.0f,0.0f,0.0f);
#endif
	}
	else
	{
#if FAST_SHAFT
		n.Cross(p, VEC3V_TO_VECTOR3(Vec3V(V_Y_AXIS_WZERO)) );
#else
		n.Cross(p, Vector3(0.0f, 1.0f, 0.0f));
#endif
		n.Normalize();
	}

	spdPlane &local = LocalPlanes[PLANE_RIGHT];
	spdPlane &world = WorldPlanes[PLANE_RIGHT];

#if FAST_SHAFT
	local.SetPair(p,n,GetMatrix(), world);
#else
	local.Set(p, n);
	local.Transform(GetMatrix(), world);
#endif
	CornersUpToDate = false;
}

// for projective shafts only
void spdShaft::SetY1(float y1)
{
	spdApical::SetY1(y1);

	float near_ = GetViewport().GetNearClip();
	float height = 2.0f * near_ * GetViewport().GetTanVFOV();
#if FAST_SPDVIEWPORT
	float fy1 = y1 * GetViewport().GetInvHeight();
#else
	float fy1 = y1 / GetViewport().GetHeight();
#endif
	float localy1 = (fy1 * height) - (height * 0.5f);

	Vector3 p(0.0f, -localy1, -near_);
	Vector3 n;

	if (m_IsOrtho)
	{
#if FAST_SHAFT
		n = VEC3V_TO_VECTOR3( Negate(Vec3V(V_Y_AXIS_WZERO)) );
#else
		n.Set(0.0f,-1.0f,0.0f);
#endif
	}
	else
	{
#if FAST_SHAFT
		n.Cross(p, VEC3V_TO_VECTOR3( Negate(Vec3V(V_X_AXIS_WZERO)) ));
#else
		n.Cross(p, Vector3(-1.0f, 0.0f, 0.0f));
#endif
		n.Normalize();
	}

	spdPlane &local = LocalPlanes[PLANE_TOP];
	spdPlane &world = WorldPlanes[PLANE_TOP];

#if FAST_SHAFT
	local.SetPair(p, n, GetMatrix(), world);
#else
	local.Set(p, n);
	local.Transform(GetMatrix(), world);
#endif
	CornersUpToDate = false;
}

// for projective shafts only
void spdShaft::SetY2(float y2)
{
	spdApical::SetY2(y2);

	float near_ = GetViewport().GetNearClip();
	float height = 2.0f * near_ * GetViewport().GetTanVFOV();
#if FAST_SPDVIEWPORT
	float fy2 = y2 * GetViewport().GetInvHeight();
#else
	float fy2 = y2 / GetViewport().GetHeight();
#endif
	float localy2 = (fy2 * height) - (height * 0.5f);

	Vector3 p(0.0f, -localy2, -near_);
	Vector3 n;

	if (m_IsOrtho)
	{
#if FAST_SHAFT
		n = VEC3V_TO_VECTOR3( Vec3V(V_Y_AXIS_WZERO) );
#else
		n.Set(0.0f,1.0f,0.0f);
#endif
	}
	else
	{
#if FAST_SHAFT
		n.Cross(p, VEC3V_TO_VECTOR3( Vec3V(V_X_AXIS_WZERO) ) );
#else
		n.Cross(p, Vector3(1.0f, 0.0f, 0.0f));
#endif
		n.Normalize();
	}

	spdPlane &local = LocalPlanes[PLANE_BOTTOM];
	spdPlane &world = WorldPlanes[PLANE_BOTTOM];

#if FAST_SHAFT
	local.SetPair(p, n, GetMatrix(), world);
#else
	local.Set(p, n);
	local.Transform(GetMatrix(), world);
#endif
	CornersUpToDate = false;
}

void spdShaft::SetFrontPlane(const spdPlane &plane)
{
	WorldPlanes[PLANE_NEAR] = plane;

	if (!IsFacing(plane))
		WorldPlanes[PLANE_NEAR].NegateNormal();

	WorldPlanes[PLANE_NEAR].Transform(GetInverse(), LocalPlanes[PLANE_NEAR]);

	CornersUpToDate = false;
}


/*
void spdShaft::SetFrustum(const grcViewport &viewport, const rmwPortalRegion &region, const spdPlane &localFrontPlane, const Matrix34 &mat)
{
PF_START(Set);

float x1 = region.X1;
float x2 = region.X2;
float y1 = region.Y1;
float y2 = region.Y2;

float minx = 0.0f;
float miny = 0.0f;
float maxx = GetViewport().GetWidth();
float maxy = GetViewport().GetHeight();

// Can we see the region?
Assert(x1 < maxx);
Assert(x2 > minx);
Assert(y1 < maxy);
Assert(y2 > miny);

// Clip. May not be necessary.
x1 = Max(x1, minx);
x2 = Min(x2, maxx);
y1 = Max(y1, miny);
y2 = Min(y2, maxy);

// Get world-space metrics of the viewport.
float d = viewport.GetNearClip();
float width = 2.0f * d * viewport.GetTanHFOV();
float height = 2.0f * d * viewport.GetTanVFOV();

// Get fractions across and down.
float fx1 = x1 / maxx;
float fx2 = x2 / maxx;
float fy1 = y1 / maxy;
float fy2 = y2 / maxy;

fx1 *= width;
fx1 -= width * 0.5f;
fx2 *= width;
fx2 -= width * 0.5f;

fy1 *= height;
fy1 -= height * 0.5f;
fy2 *= height;
fy2 -= height * 0.5f;

// Find points on planes.
LocalPoints[PLANE_NEAR  ].Set( localFrontPlane.Pos );
LocalPoints[PLANE_FAR   ].Set( 0.0f,        0.0f,        -viewport.GetFarClip());
LocalPoints[PLANE_LEFT  ].Set( fx1,         0.0f,        -d );
LocalPoints[PLANE_RIGHT ].Set( fx2,         0.0f,        -d );
LocalPoints[PLANE_TOP   ].Set( 0.0f,       -fy1,         -d );
LocalPoints[PLANE_BOTTOM].Set( 0.0f,       -fy2,         -d );

// Find plane normals. For the front plane, the normal must face along +z.
// If it does not, flip it.
LocalNormals[PLANE_NEAR  ].Set( localFrontPlane.Normal.z >= 0.0f ? localFrontPlane.Normal : -localFrontPlane.Normal );
LocalNormals[PLANE_FAR   ].Set( 0.0f,        0.0f,        -1.0f);
LocalNormals[PLANE_LEFT  ].Cross(LocalPoints[PLANE_LEFT],   Vector3( 0.0f, -1.0f,  0.0f));
LocalNormals[PLANE_RIGHT ].Cross(LocalPoints[PLANE_RIGHT],  Vector3( 0.0f,  1.0f,  0.0f));
LocalNormals[PLANE_TOP   ].Cross(LocalPoints[PLANE_TOP],    Vector3(-1.0f,  0.0f,  0.0f));
LocalNormals[PLANE_BOTTOM].Cross(LocalPoints[PLANE_BOTTOM], Vector3( 1.0f,  0.0f,  0.0f));

LocalNormals[PLANE_LEFT  ].Normalize();
LocalNormals[PLANE_RIGHT ].Normalize();
LocalNormals[PLANE_TOP   ].Normalize();
LocalNormals[PLANE_BOTTOM].Normalize();

SetMatrix(mat);

PF_STOP(Set);
}
*/

void spdShaft::Set(const grcViewport &viewport, const Matrix34 &mat)
{
	m_IsOrtho=!viewport.IsPerspective();

//rs	PF_START(Set);

	// store everything you want to know about the viewport 
	// like TanHFOV, TanVFOV, ZClipNear, ZClipFar, PixelLeft, PixelTop, 
	// PixelWidth, PixelHeight
	// this fills up a structure that contains information about the grcViewport
	// that is used to make a shaft.
	spdApical::Viewport.Set(viewport);

	// Sets the matrix and its inverse (note that this means the matrix must be invertable).
	// Also builds projection matrices.
	// this is actually setting what will be used later as the world matrix to transform 
	// the local planes from local to world space
	SetMatrix(viewport, mat);

	// Camera-space navigation:
	// -x = left
	// +y = up
	// -z = forward

	// Get FOV angles.
	/*
	float hfov = atanf(viewport.GetTanHFOV());
	float vfov = atanf(viewport.GetTanVFOV());

	// Compose the frustum-space planes.
	LocalPlanes[PLANE_NEAR  ].Pos.Set( 0.0f, 0.0f, -viewport.GetNearClip());
	LocalPlanes[PLANE_FAR   ].Pos.Set( 0.0f, 0.0f, -viewport.GetFarClip());
	LocalPlanes[PLANE_LEFT  ].Pos.Set( 0.0f, 0.0f, 0.0f);
	LocalPlanes[PLANE_RIGHT ].Pos.Set( 0.0f, 0.0f, 0.0f);
	LocalPlanes[PLANE_TOP   ].Pos.Set( 0.0f, 0.0f, 0.0f);
	LocalPlanes[PLANE_BOTTOM].Pos.Set( 0.0f, 0.0f, 0.0f);

	LocalPlanes[PLANE_NEAR  ].Normal.Set( 0.0f,         0.0f,        1.0f);
	LocalPlanes[PLANE_FAR   ].Normal.Set( 0.0f,         0.0f,       -1.0f);
	LocalPlanes[PLANE_LEFT  ].Normal.Set(-cosf(hfov),   0.0f,        sinf(hfov));
	LocalPlanes[PLANE_RIGHT ].Normal.Set( cosf(hfov),   0.0f,        sinf(hfov));
	LocalPlanes[PLANE_TOP   ].Normal.Set( 0.0f,         cosf(vfov),  sinf(vfov));
	LocalPlanes[PLANE_BOTTOM].Normal.Set( 0.0f,        -cosf(vfov),  sinf(vfov));
	*/


	/*
	There is some sense in setting the nearClip plane at z=0,
	rather than at the nearClip clip plane as defined by the viewport.
	This is because when detecting visibility of portals in our
	shaft/portal tests, we usually don't care about the front plane,
	because portals inside the apical (behind the front clip plane)
	need to be considered as visible.

	I resorted to ignoring the nearClip clip in the shaft/portal test itself,
	and keeping the shaft's nearClip plane consistent with the viewport's
	nearClip plane. As portals are encountered and rage_new shafts are made,
	the front plane of the shaft is moved to the portal's plane.
	*/

	//	LocalPlanes[PLANE_NEAR  ].Set( Vector3(0.0f, 0.0f, 0.0f),                     Vector3(0.0f,         0.0f,        1.0f) );


#if FAST_SHAFT
	const Vec3V _zAxis		= Vec3V(V_Z_AXIS_WZERO);
	const Vec3V _negZAxis	= Negate( Vec3V(V_Z_AXIS_WZERO) );

	LocalPlanes[PLANE_NEAR  ].SetPair( Vector3(0.0f, 0.0f, -viewport.GetNearClip()),	VEC3V_TO_VECTOR3(_zAxis),		GetMatrix(), WorldPlanes[PLANE_NEAR] );
	LocalPlanes[PLANE_FAR   ].SetPair( Vector3(0.0f, 0.0f, -viewport.GetFarClip()) ,	VEC3V_TO_VECTOR3(_negZAxis),	GetMatrix(), WorldPlanes[PLANE_FAR] );
#else
	// build local planes
	LocalPlanes[PLANE_NEAR  ].Set( Vector3(0.0f, 0.0f, -viewport.GetNearClip()), Vector3(0.0f,         0.0f,        1.0f) );
	LocalPlanes[PLANE_FAR   ].Set( Vector3(0.0f, 0.0f, -viewport.GetFarClip()) , Vector3(0.0f,         0.0f,       -1.0f) );

	// Transform LocalPlanes and store the result in WorldPlanes
	// GetMatrix() representing the position and orientation of this apical.
	// It is the matrix that is set above with SetMatrix()
	LocalPlanes[PLANE_NEAR  ].Transform(GetMatrix(), WorldPlanes[PLANE_NEAR]);
	LocalPlanes[PLANE_FAR   ].Transform(GetMatrix(), WorldPlanes[PLANE_FAR]);
#endif

	if (m_IsOrtho)
	{

		//  the camera matrix is the camera world matrix
		const Matrix34 &camMtx = GetMatrix();

#if FAST_SHAFT
		const Vec3V _xAxis		= Vec3V(V_X_AXIS_WZERO);
		const Vec3V _negXAxis	= Negate( Vec3V(V_X_AXIS_WZERO) );
		const Vec3V _yAxis		= Vec3V(V_Y_AXIS_WZERO);
		const Vec3V _negYAxis	= Negate( Vec3V(V_Y_AXIS_WZERO) );

		const Vec4V _lrtbV = viewport.GetLRTB();
		Vector3 left	= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.a), SplatX( _lrtbV ) ) );
		Vector3 right	= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.a), SplatY( _lrtbV ) ) );
		Vector3 top		= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.b), SplatZ( _lrtbV ) ) );
		Vector3 bottom	= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.b), SplatW( _lrtbV ) ) );
#else
		// GetLRTB() is for orthographic viewports only
		Vector3 left = camMtx.a * viewport.GetLRTB().GetXf();
		Vector3 right = camMtx.a * viewport.GetLRTB().GetYf();
		Vector3 top = camMtx.b * viewport.GetLRTB().GetZf();
		Vector3 bottom = camMtx.b * viewport.GetLRTB().GetWf();
#endif
		Vector3 front = camMtx.c * -viewport.GetNearClip();
		Vector3 back = camMtx.c * -viewport.GetFarClip();

		WorldCorners[0] = camMtx.d + front + top + left;
		WorldCorners[1] = camMtx.d + front + bottom + left;
		WorldCorners[2] = camMtx.d + front + top + right;
		WorldCorners[3] = camMtx.d + front + bottom + right;
		WorldCorners[4] = camMtx.d + back + top + left;
		WorldCorners[5] = camMtx.d + back + bottom + left;
		WorldCorners[6] = camMtx.d + back + top + right;
		WorldCorners[7] = camMtx.d + back + bottom + right;

		int j;

#if FAST_SHAFT

		Vector3 _lrtb_L, _lrtb_R, _lrtb_T, _lrtb_B;
		const Vector3 _zero = VEC3V_TO_VECTOR3( Vec3V(V_ZERO) );
		_lrtb_L = _zero; 
		_lrtb_R = _zero; 
		_lrtb_T = _zero;
		_lrtb_B = _zero;
		RC_VEC3V(_lrtb_L).SetX( _lrtbV.GetX() );
		RC_VEC3V(_lrtb_R).SetX( _lrtbV.GetY() );
		RC_VEC3V(_lrtb_T).SetY( _lrtbV.GetZ() );
		RC_VEC3V(_lrtb_B).SetY( _lrtbV.GetW() );

		LocalPlanes[PLANE_LEFT  ].SetPair( _lrtb_L, VEC3V_TO_VECTOR3(_negXAxis)	,GetMatrix(), WorldPlanes[PLANE_LEFT] );
		LocalPlanes[PLANE_RIGHT ].SetPair( _lrtb_R, VEC3V_TO_VECTOR3(_xAxis)	,GetMatrix(), WorldPlanes[PLANE_RIGHT] );
		LocalPlanes[PLANE_TOP   ].SetPair( _lrtb_T, VEC3V_TO_VECTOR3(_yAxis)	,GetMatrix(), WorldPlanes[PLANE_TOP] );
		LocalPlanes[PLANE_BOTTOM].SetPair( _lrtb_B, VEC3V_TO_VECTOR3(_negYAxis)	,GetMatrix(), WorldPlanes[PLANE_BOTTOM] );

		Vector3 bmin = VEC3V_TO_VECTOR3(Vec3V(V_FLT_MAX));
		Vector3 bmax;
		bmax.Negate(bmin);
		j = 0;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
		geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;

		Assert( j == 8 );

		BoundingBox.Set(bmin, bmax);

		j = 0;
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;

		Assert(j == NUMOF_PLANES);
#else

		LocalPlanes[PLANE_LEFT  ].Set( Vector3(viewport.GetLRTB().GetXf(), 0.0f, 0.0f), Vector3(-1.0f,         0.0f,        0.0f) );
		LocalPlanes[PLANE_RIGHT ].Set( Vector3(viewport.GetLRTB().GetYf(), 0.0f, 0.0f), Vector3(1.0f,          0.0f,        0.0f) );
		LocalPlanes[PLANE_TOP   ].Set( Vector3(0.0f, viewport.GetLRTB().GetZf(), 0.0f), Vector3(0.0f,          1.0f,        0.0f) );
		LocalPlanes[PLANE_BOTTOM].Set( Vector3(0.0f, viewport.GetLRTB().GetWf(), 0.0f), Vector3(0.0f,          -1.0f,       0.0f) );

		LocalPlanes[PLANE_LEFT  ].Transform(GetMatrix(), WorldPlanes[PLANE_LEFT]);
		LocalPlanes[PLANE_RIGHT ].Transform(GetMatrix(), WorldPlanes[PLANE_RIGHT]);
		LocalPlanes[PLANE_TOP   ].Transform(GetMatrix(), WorldPlanes[PLANE_TOP]);
		LocalPlanes[PLANE_BOTTOM].Transform(GetMatrix(), WorldPlanes[PLANE_BOTTOM]);


		Vector3 bmin(FLT_MAX, FLT_MAX, FLT_MAX);
		Vector3 bmax;
		bmax.Negate(bmin);
		for(j = 0; j < 8; j++)
		{
			const Vector3 &p = WorldCorners[j];
			geomVectors::Min(bmin,p);
			geomVectors::Max(bmax,p);
		}
		BoundingBox.Set(bmin, bmax);

		for (j=0; j<NUMOF_PLANES; j++)
		{
			// Create the absolute values of the plane normals.
			AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal());
		}
#endif
		CornersUpToDate = true;
	}
	else
	{
		// 
		float x = (float)viewport.GetWindow().GetX();
		float y = (float)viewport.GetWindow().GetY();
		float w = (float)viewport.GetWindow().GetWidth();
		float h = (float)viewport.GetWindow().GetHeight();

		// this calculates the local and world plane for a perspective shaft
		SetX1(x);
		SetX2(x + w);
		SetY1(y);
		SetY2(y + h);

		// this will be only used for perspective shafts ... corners of orthographic shafts are done above
		UpdateCorners();
	}

//rs	PF_STOP(Set);
}

void spdViewport::Set(const grcViewport &viewport)
{
	ZClipNear = viewport.GetNearClip();
	ZClipFar = viewport.GetFarClip();
	TanHFOV = viewport.GetTanHFOV();
	TanVFOV = viewport.GetTanVFOV();
	PixelLeft = static_cast<float>(viewport.GetWindow().GetX());
	PixelTop = static_cast<float>(viewport.GetWindow().GetY());
	PixelWidth = static_cast<float>(viewport.GetWindow().GetWidth());
	PixelHeight = static_cast<float>(viewport.GetWindow().GetHeight());
	invPixelWidth = 1.0f / PixelWidth;
	invPixelHeight = 1.0f / PixelHeight;
}

/* //rs
bool spdShaft::IsVisible(const rmwBoundInfo &bound) const
{
	if (!IsVisible(bound.Sphere))
		return false;

	return IsVisible(bound.WorldOOBB);
}
*/


bool spdShaft::IsVisible(const spdSphere &sphere) const
{
	//rs	PF_START(TestSphere);

	const Vector3 &center = sphere.GetCenterVector3();

	for (int i = 0; i < NUMOF_PLANES; i++)
	{
		// Old version:
		//Vector3 v(sphere.GetCenter() - WorldPlanes[i].GetPos());
		//float d = v.Dot(WorldPlanes[i].GetNormal());

		// New version, seems to be about 30% faster. /FF
		float d = WorldPlanes[i].DistanceToPlane(center);

		if (d > sphere.GetRadiusf())
		{
			//rs			PF_STOP(TestSphere);
			return false;
		}
	}

	//rs	PF_STOP(TestSphere);
	return true;
}

grcCullStatus spdShaft::IsSphereVisible(const spdSphere &sphere, float *zDist) const
{
	CompileTimeAssert(PLANE_NEAR == 0);	// We start there and move up

#if FAST_SHAFT
	grcCullStatus clipped = cullOutside;

	Vec3V center = sphere.GetCenter();
	ScalarV d = WorldPlanes[PLANE_NEAR].DistanceToPlaneV( center );
	ScalarV radius = sphere.GetRadius();
	if (zDist)
	{
		*zDist = (Add(d, radius)).Getf();
	}	
	
	ScalarV negRadius = Negate( sphere.GetRadius() );

	if( IsGreaterThanAll( d, radius ) )
		return cullOutside;
	if( IsGreaterThanAll( d, negRadius ) )
		clipped = cullClipped;

	for (int i = 1; i < NUMOF_PLANES; i++)
	{
		d = WorldPlanes[i].DistanceToPlaneV(center);
		if( IsGreaterThanAll( d, radius ) )
			return cullOutside;
		if( IsGreaterThanAll( d, negRadius ) )
			clipped = cullClipped;
	}

	return (grcCullStatus)(cullInside-clipped);

#else
	bool clipped = false;

	const Vector3 &center = sphere.GetCenterVector3();

	float d = WorldPlanes[PLANE_NEAR].DistanceToPlane(center);
	float radius = sphere.GetRadiusf();
	if (zDist)
	{
		*zDist = d + radius;
	}
	if (d > radius)
	{
		return cullOutside;
	}
	float negRadius = -radius;
	if (d > negRadius)
	{
		clipped = true;
	}

	for (int i = 1; i < NUMOF_PLANES; i++)
	{
		float d = WorldPlanes[i].DistanceToPlane(center);
		if (d > radius)
		{
			return cullOutside;
		}
		if (d > negRadius)
		{
			clipped = true;
		}
	}

	return (clipped) ? cullClipped : cullInside;
#endif
}


grcCullStatus spdShaft::IsVisible(const Matrix34 &mtx,const rmcDrawable &drawable,u8 &lod, float * retZDist) const {
	float zDist = 0;
	float radius = drawable.GetLodGroup().GetCullRadius();
	Vector3 center;
	center.Dot(drawable.GetLodGroup().GetCullSphere(),mtx);
	grcCullStatus status = IsSphereVisible(spdSphere(RCC_VEC3V(center), ScalarV(radius)),&zDist);
	lod = (u8) (status? drawable.GetLodGroup().ComputeLod(zDist - drawable.GetLodGroup().GetCullRadius()) : LOD_VLOW);
	if (retZDist)
		*retZDist = Max(0.0f,zDist-radius);
	return status;
}

grcCullStatus spdShaft::IsVisible(const Matrix44 &mtx,const rmcDrawable &drawable,u8 &lod, float * retZDist) const {
	float zDist = 0;
	float radius = drawable.GetLodGroup().GetCullRadius();
	Vector4 sphere;
	Vector4 centerAndRadius;
	sphere.SetVector3(drawable.GetLodGroup().GetCullSphere());
#if FAST_SHAFT
	RC_VEC4V(sphere).SetW( ScalarV(V_ONE) );
#else
	sphere.w = 1.0f;
#endif
	centerAndRadius.Dot(sphere, mtx);
#if FAST_SHAFT
	RC_VEC4V(centerAndRadius).SetW( ScalarVFromF32(radius));
#else
	centerAndRadius.w = radius;
#endif
	grcCullStatus status = IsSphereVisible(spdSphere(RCC_VEC4V(centerAndRadius)),&zDist);
	lod = (u8) (status? drawable.GetLodGroup().ComputeLod(zDist - drawable.GetLodGroup().GetCullRadius()) : LOD_VLOW);
	if (retZDist)
		*retZDist = Max(0.0f,zDist-radius);
	return status;
}


bool spdShaft::IsVisibleWithoutNearFar(const spdSphere &sphere) const
{
	//rs	PF_START(TestSphere);

#if FAST_SHAFT

	Vec3V center = sphere.GetCenter();
	ScalarV radius = sphere.GetRadius();

	for (int i = PLANE_FAR+1; i < NUMOF_PLANES; i++)
	{
		if( IsGreaterThanAll( WorldPlanes[i].DistanceToPlaneV(center), radius ) )
			return false;
	}

#else
	const Vector3 &center = sphere.GetCenterVector3();

	// skip nearClip, farClip planes
	for (int i = PLANE_FAR+1; i < NUMOF_PLANES; i++)
	{
		// Old version:
		//Vector3 v(sphere.GetCenter() - WorldPlanes[i].GetPos());
		//float d = v.Dot(WorldPlanes[i].GetNormal());

		// New version, seems to be about 30% faster. /FF
		float d = WorldPlanes[i].DistanceToPlane(center);

		if (d > sphere.GetRadiusf())
		{
			//rs			PF_STOP(TestSphere);
			return false;
		}
	}
#endif
	//rs	PF_STOP(TestSphere);
	return true;
}


#if SPD_VALIDATE_ISVISIBLE

grcCullStatus spdShaft::IsVisible_ShaftAABB(const spdAABB &aabb) const
{
	grcCullStatus rOld = IsVisibleOld(aabb);
	grcCullStatus rNew = IsVisibleNew(aabb);

	if(rOld != rNew)
	{
		if(rOld == cullClipped && rNew == cullOutside)
		{
		}
		else
		{
			Quitf("Incorrect results of spdShaft::IsVisible(const spdAABB&) function: %d %d", rOld, rNew);
		}
	}

	//	if((ioPad::GetPad(0).GetButtons() & ioPad::LDOWN) == 0)
	return rNew;
	//	else
	//		return rOld;
}

grcCullStatus spdShaft::IsVisibleNew(const spdAABB &aabb) const

#else

grcCullStatus spdShaft::IsVisible_ShaftAABB(const spdAABB &aabb) const

#endif
#if !SOA_OPTIMIZED_ISVISIBLE
{

//rs	PF_START(TestAABB);

	// Note: this is ugly, casting away from const. In my opinion,
	// UpdateCorners() should be const, and the stuff it touches
	// should be mutable. Personally I found 'mutable' to be
	// perfectly acceptable for cached variables.
	//
	// Quite possibly we could do this earlier, but doing it here
	// makes comparisons between old and new versions more fair,
	// since the old one didn't need the bounding box, so now this
	// call ends up in the timing of the new function.
	//
	// One more thing: don't be fooled into thinking that this isn't
	// needed if you put in an Assert(CornersUpToDate) that isn't failing,
	// because at least at the moment the corners are updated by some
	// __DEV-only code.
	if(!CornersUpToDate)
	{
		const_cast<spdShaft&>(*this).UpdateCorners();
	}

	if(!aabb.Intersects(BoundingBox))
	{
//rs	PF_STOP(TestAABB);
		return cullOutside;
	}

//	int i;
//	bool clippedOrOutside = false;
	Vec3V center = VECTOR3_TO_VEC3V( aabb.GetCenter() );
	Vec3V halfExtents = VECTOR3_TO_VEC3V(aabb.GetMax()) - center;

	Vec4V inscribedRadiusVInW = VECTOR4_TO_VEC4V( aabb.GetInscribedRadiusV() );

	ScalarV distancesV_0, distancesV_1, distancesV_2, distancesV_3, distancesV_4, distancesV_5;
	ScalarV halfExtent_0, halfExtent_1, halfExtent_2, halfExtent_3, halfExtent_4, halfExtent_5;
	BoolV outsideCheck_0, outsideCheck_1, outsideCheck_2, outsideCheck_3, outsideCheck_4, outsideCheck_5;
	BoolV clippedCheck_0, clippedCheck_1, clippedCheck_2, clippedCheck_3, clippedCheck_4, clippedCheck_5;

	ScalarV inscribedRadiusV = SplatW( inscribedRadiusVInW );
	Vec4V maskXYZ(V_MASKXYZ);

	halfExtents = And( halfExtents, maskXYZ.GetXYZ() );

	distancesV_0 = WorldPlanes[0].DistanceToPlaneV(center);
	distancesV_1 = WorldPlanes[1].DistanceToPlaneV(center);
	distancesV_2 = WorldPlanes[2].DistanceToPlaneV(center);
	distancesV_3 = WorldPlanes[3].DistanceToPlaneV(center);
	distancesV_4 = WorldPlanes[4].DistanceToPlaneV(center);
	distancesV_5 = WorldPlanes[5].DistanceToPlaneV(center);

	halfExtent_0 = Dot( RCC_VEC3V(AbsPlaneNormals[0]), halfExtents );
	halfExtent_1 = Dot( RCC_VEC3V(AbsPlaneNormals[1]), halfExtents );
	halfExtent_2 = Dot( RCC_VEC3V(AbsPlaneNormals[2]), halfExtents );
	halfExtent_3 = Dot( RCC_VEC3V(AbsPlaneNormals[3]), halfExtents );
	halfExtent_4 = Dot( RCC_VEC3V(AbsPlaneNormals[4]), halfExtents );
	halfExtent_5 = Dot( RCC_VEC3V(AbsPlaneNormals[5]), halfExtents );

	halfExtent_0 = Max( halfExtent_0, inscribedRadiusV );
	halfExtent_1 = Max( halfExtent_1, inscribedRadiusV );
	halfExtent_2 = Max( halfExtent_2, inscribedRadiusV );
	halfExtent_3 = Max( halfExtent_3, inscribedRadiusV );
	halfExtent_4 = Max( halfExtent_4, inscribedRadiusV );
	halfExtent_5 = Max( halfExtent_5, inscribedRadiusV );

	outsideCheck_0 = IsGreaterThan( distancesV_0, halfExtent_0 );
	outsideCheck_1 = IsGreaterThan( distancesV_1, halfExtent_1 );
	outsideCheck_2 = IsGreaterThan( distancesV_2, halfExtent_2 );
	outsideCheck_3 = IsGreaterThan( distancesV_3, halfExtent_3 );
	outsideCheck_4 = IsGreaterThan( distancesV_4, halfExtent_4 );
	outsideCheck_5 = IsGreaterThan( distancesV_5, halfExtent_5 );

	// Negation.
	ScalarV _80000000(V_80000000);
	halfExtent_0 ^= _80000000;
	halfExtent_1 ^= _80000000;
	halfExtent_2 ^= _80000000;
	halfExtent_3 ^= _80000000;
	halfExtent_4 ^= _80000000;
	halfExtent_5 ^= _80000000;

	outsideCheck_0 |= outsideCheck_1;
	outsideCheck_2 |= outsideCheck_3;
	outsideCheck_4 |= outsideCheck_5;
	outsideCheck_0 |= outsideCheck_2;
	outsideCheck_0 |= outsideCheck_4;

	clippedCheck_0 = IsLessThan( halfExtent_0, distancesV_0 );
	clippedCheck_1 = IsLessThan( halfExtent_1, distancesV_1 );
	clippedCheck_2 = IsLessThan( halfExtent_2, distancesV_2 );
	clippedCheck_3 = IsLessThan( halfExtent_3, distancesV_3 );
	clippedCheck_4 = IsLessThan( halfExtent_4, distancesV_4 );
	clippedCheck_5 = IsLessThan( halfExtent_5, distancesV_5 );

	clippedCheck_0 |= clippedCheck_1;
	clippedCheck_2 |= clippedCheck_3;
	clippedCheck_4 |= clippedCheck_5;
	clippedCheck_0 |= clippedCheck_2;
	clippedCheck_0 |= clippedCheck_4;

	if ( !IsEqualIntAll( outsideCheck_0, BoolV(V_TRUE)) )
	{
		return cullOutside;
	}
	else if ( !IsEqualIntAll( clippedCheck_0, BoolV(V_TRUE)) )
	{
		return cullClipped;
	}

	return cullInside;




/*
	for(i = 0; i < spdShaft::NUMOF_PLANES; i++)
	{
		const float d = WorldPlanes[i].DistanceToPlane(center);

		if(clippedOrOutside && d <= 0.0f)
		{
			// Some other plane has already intersected a plane,
			// so there is no way the box can be completely inside
			// of the shaft. Therefore, the only interesting case
			// would have been if we are outside of this plane, but
			// since d <= 0 that's not the case.
			continue;
		}
		float absD = fabsf(d);
		const Vector3 &normal = GetAbsPlaneNormal(i);
		if(
			// First, check if an inscribed sphere intersects the plane. If so,
			// the box must also intersect the plane.
			absD <= inscribedRadius ||

			// Do the box to plane check.
			absD <= normal.Dot(halfExtents))
		{
			clippedOrOutside = true;
		}
		else if(d >= 0.0f)
		{
			// The box is completely outside of this plane, so there is no
			// way the box can be visible in the shaft.
//rs			PF_STOP(TestAABB);
			return cullOutside;
		}

		// The box is inside of this plane. We need to keep checking
		// the other planes.
	}

	if(clippedOrOutside)
	{
		// If we get here, it has been determined that the box is not
		// completely inside the shaft, and that it the faces of the box
		// and the shaft are not separating planes.

		// It is still possible that a different separating plane exists.
		// There is an article in Graphs Gems IV that describes how
		// to solve that case. It's probably worth doing. /FF

//rs		PF_STOP(TestAABB);
		return cullClipped;
	}

//rs	PF_STOP(TestAABB);
	return cullInside;
	*/
}
#else
{

	if(!CornersUpToDate)
	{
		const_cast<spdShaft&>(*this).UpdateCorners();
	}

	if(!aabb.Intersects(BoundingBox))
	{
		//rs	PF_STOP(TestAABB);
		return cullOutside;
	}

	//	int i;
	//	bool clippedOrOutside = false;
	AoS::Vec3V center = VECTOR3_TO_VEC3V( aabb.GetCenter() );
	AoS::Vec3V halfExtents = VECTOR3_TO_VEC3V(aabb.GetMax()) - center;

	AoS::Vec4V inscribedRadiusVInW = VECTOR4_TO_VEC4V( aabb.GetInscribedRadiusV() );

	AoS::ScalarV inscribedRadiusV = AoS::SplatW( inscribedRadiusVInW );

	// Turn these Vector3's on their sides, duplicating since we'll need that.
	SoA::Vec3V center_soa;
	SoA::Vec3V halfExtents_soa;
	ToSoA( center_soa, center, center, center, center );
	ToSoA( halfExtents_soa, halfExtents, halfExtents, halfExtents, halfExtents );

	// Each WorldPlane[i] is just a Vector4. Let's turn them on their sides.
	SoA::Vec4V worldPlanes0to3_soa;
	SoA::Vec4V worldPlanes4to5_soa; // last two entries of each of x/y/z/w component vecs are ZERO here. This is important later.
	ToSoA( worldPlanes0to3_soa, RCC_VEC4V(WorldPlanes[0].GetCoefficientVector()), RCC_VEC4V(WorldPlanes[1].GetCoefficientVector()), RCC_VEC4V(WorldPlanes[2].GetCoefficientVector()), RCC_VEC4V(WorldPlanes[3].GetCoefficientVector()) );
	ToSoA( worldPlanes4to5_soa, RCC_VEC4V(WorldPlanes[4].GetCoefficientVector()), RCC_VEC4V(WorldPlanes[5].GetCoefficientVector()), AoS::Vec4V(AoS::V_ZERO), AoS::Vec4V(AoS::V_ZERO) );

	// SoA version of spdPlane::DistanceToPlaneV().
	SoA::ScalarV worldPlanes0to3_W_soa = worldPlanes0to3_soa.GetW();
	SoA::ScalarV worldPlanes4to5_W_soa = worldPlanes4to5_soa.GetW();
	SoA::ScalarV distancesV_0to3_soa = SoA::Dot( worldPlanes0to3_soa.GetXYZ(), center_soa ) - worldPlanes0to3_W_soa;
	SoA::ScalarV distancesV_4to5_soa = SoA::Dot( worldPlanes4to5_soa.GetXYZ(), center_soa ) - worldPlanes4to5_W_soa;

	// Each AbsPlaneNormals[i] is just a Vector3. Let's turn them on their sides.
	SoA::Vec3V absPlaneNormals0to3_soa;
	SoA::Vec3V absPlaneNormals4to5_soa; // last two entries of each of x/y/z component vecs are ZERO here. This is important later.
	ToSoA( absPlaneNormals0to3_soa, RCC_VEC3V(AbsPlaneNormals[0]), RCC_VEC3V(AbsPlaneNormals[1]), RCC_VEC3V(AbsPlaneNormals[2]), RCC_VEC3V(AbsPlaneNormals[3]) );
	ToSoA( absPlaneNormals4to5_soa, RCC_VEC3V(AbsPlaneNormals[4]), RCC_VEC3V(AbsPlaneNormals[5]), AoS::Vec3V(AoS::V_ZERO), AoS::Vec3V(AoS::V_ZERO) );

	// SoA Dot prods.
	SoA::ScalarV halfExtent_0to3_soa = SoA::Dot( absPlaneNormals0to3_soa, halfExtents_soa );
	SoA::ScalarV halfExtent_4to5_soa = SoA::Dot( absPlaneNormals4to5_soa, halfExtents_soa );

	// SoA Max's.
	SoA::ScalarV inscribedRadiusV_soa( inscribedRadiusV.GetIntrin128() );
	SoA::ScalarV inscribedRadiusV_ZWzero_soa( AoS::GetFromTwo<Vec::Z1,Vec::W1,Vec::X2,Vec::Y2>( AoS::Vec4V(inscribedRadiusV), AoS::Vec4V(AoS::V_ZERO) ).GetIntrin128() );
	halfExtent_0to3_soa = SoA::Max( halfExtent_0to3_soa, inscribedRadiusV_soa );
	halfExtent_4to5_soa = SoA::Max( halfExtent_4to5_soa, inscribedRadiusV_ZWzero_soa );

	// SoA comparisons.
	SoA::VecBool1V outsideCheck_0to3_soa = SoA::IsGreaterThan( distancesV_0to3_soa, halfExtent_0to3_soa );
	SoA::VecBool1V outsideCheck_4to5_soa = SoA::IsGreaterThan( distancesV_4to5_soa, halfExtent_4to5_soa );

	// Negation.
	AoS::ScalarV _80000000(AoS::ScalarV::INT_80000000);
	SoA::ScalarV _80000000_soa( _80000000.GetIntrin128() );
	halfExtent_0to3_soa ^= _80000000_soa;
	halfExtent_4to5_soa ^= _80000000_soa;

	// SoA comparisons.
	SoA::VecBool1V clippedCheck_0to3_soa = SoA::IsLessThan( halfExtent_0to3_soa, distancesV_0to3_soa );
	SoA::VecBool1V clippedCheck_4to5_soa = SoA::IsLessThan( halfExtent_4to5_soa, distancesV_4to5_soa );

	// Gather results.
	//
	// (Remember how the last two values of the components of the *_4to5_soa vectors were zero? This means that all of the
	// operations should amount to zero, and the IsLessThan()'s and IsGreaterThan()'s should always return 0x0 (false)
	// for their comparisons, so we don't have to mask them out before doing these bitwise OR's below.)
	ASSERT_ONLY( AoS::Vec4V o(outsideCheck_4to5_soa.GetIntrin128()) );
	ASSERT_ONLY( AoS::Vec4V c(clippedCheck_4to5_soa.GetIntrin128()) );
	Assertf( o.GetZi() == 0 && o.GetWi() == 0, "These should be zero, not %i,%i. The algorithm is wrong. Make SOA_OPTIMIZED_ISVISIBLE equal to 0 immediately!", o.GetZi(), o.GetWi() );
	Assertf( c.GetZi() == 0 && c.GetWi() == 0, "These should be zero, not %i,%i. The algorithm is wrong. Make SOA_OPTIMIZED_ISVISIBLE equal to 0 immediately!", c.GetZi(), c.GetWi() );
	outsideCheck_0to3_soa |= outsideCheck_4to5_soa;
	clippedCheck_0to3_soa |= clippedCheck_4to5_soa;

	// Back to AoS.
	AoS::VecBoolV outsideCheckResult( outsideCheck_0to3_soa.GetIntrin128() );
	AoS::VecBoolV clippedCheckResult( clippedCheck_0to3_soa.GetIntrin128() );

	if ( !AoS::IsEqualIntAll( outsideCheckResult, AoS::VecBoolV(AoS::VecBoolV::F_F_F_F)) )
	{
		return cullOutside;
	}
	else if ( !IsEqualIntAll( clippedCheckResult, AoS::VecBoolV(AoS::VecBoolV::F_F_F_F)) )
	{
		return cullClipped;
	}

	return cullInside;
}
#endif

#if SPD_VALIDATE_ISVISIBLE

// Old, non-optimized version.

grcCullStatus spdShaft::IsVisibleOld(const spdAABB &aabb) const
{
	PF_START(TestAABBOld);

	// Perform trivial reject test with a sphere.
	grcCullStatus res = cullInside;

	int i;
	float boundingRadius = aabb.GetBoundingRadius();
	const Vector3 &center = aabb.GetCenter();
	for (i = 0; i < spdShaft::NUMOF_PLANES; i++)
	{
		// Old version:
		//Vector3 v(aabb.GetCenter() - WorldPlanes[i].GetPos());
		//float d = v.Dot(WorldPlanes[i].GetNormal());

		// New version: /FF
		float d = WorldPlanes[i].DistanceToPlane(center);

		if (fabsf(d) < boundingRadius)
			res = cullClipped;
		else
			if (d > 0.0f)
			{
				PF_STOP(TestAABBOld);
				return cullOutside;
			}
	}

	if (res == cullInside)
	{
		PF_STOP(TestAABBOld);
		return res;  // The bounding sphere is inside the shaft.
	}

	Assert(res == cullClipped);

	// Perform tighter test against the box planes.

	Vector3 min, max;
	min = aabb.GetMin();
	max = aabb.GetMax();

	// TODO: It's probably faster to construct these as they're tested.
	// But see rmwOccluder::Occludes(const spdAABB &box).
	Vector3 verts[8] = {

		Vector3(min.x, min.y, min.z),
			Vector3(max.x, max.y, max.z),
			Vector3(min.x, max.y, max.z),
			Vector3(max.x, min.y, min.z),
			Vector3(min.x, min.y, max.z),
			Vector3(min.x, max.y, min.z),
			Vector3(max.x, min.y, max.z),
			Vector3(max.x, max.y, min.z),
	};

	int andTest = 0x3f;
	int orTest = 0;
	int test;

	for (i = 0; i < 8; i++)
	{
		test = 0;
		for (int j = 0; j < NUMOF_PLANES; j++)
		{
			if (WorldPlanes[j].IsPointInFront(verts[i]))
				test |= 1 << j;
		}

		andTest &= test;

		// TODO: Could return earlier here if we don't care about cullClipped vs cullInside.
		/*		if (andTest == 0)
		{
		PF_STOP(TestAABB);
		return cullClipped;  // We can see some part of the box.
		}
		*/
		orTest |= test;
	}

	PF_STOP(TestAABBOld);

	if (andTest)
		return cullOutside;  // Completely outside frustum.

	if (orTest)
		return cullClipped;  // Partially visible.

	return cullInside;
}

#endif

/*
bool spdShaft::IsVisible(const awCurve &curve) const
{
int andTest = 0x3f;
int test;

for (int i = 0; i < curve.GetNumCVs(); i++)
{
test = 0;
for (int j = 0; j < NUMOF_PLANES; j++)
{
if (WorldPlanes[j].IsPointInFront(curve.GetCV(i)))
test |= 1 << j;
}

andTest &= test;
if (andTest == 0)
return true;
}

// All points out of view due to any particular plane(s)?
if (andTest)
return false;  // Completely outside frustum.

return true;
}
*/

bool spdShaft::IsVisible(const Vector3 *verts, int count) const
{
	Assert(verts);
	Assert(count >= 0);

	int andTest = 0x3f;
	int test;

	for (int i = 0; i < count; i++)
	{
		test = 0;
		u32 bit_shfit = 1;
		for (int j = 0; j < NUMOF_PLANES; j++)
		{
			if (WorldPlanes[j].IsPointInFront(verts[i]))
				test |= bit_shfit;

			bit_shfit = bit_shfit<<1;
		}

		andTest &= test;
		if (andTest == 0)
			return true;
	}

	// All points out of view due to any particular plane(s)?
	if (andTest)
		return false;  // Completely outside frustum.

	return true;
}

bool spdShaft::GetVisibleEdges(spdPolygon &polygon) const
{
//rs	PF_START(TestPolygon);

	// Like IsVisible(spdPolygon&), except that edge visibility is also recorded.

	polygon.SetEdgeVisibleFrame(TIME.GetFrameCount());

	Assert(polygon.GetNumPoints() > 0 && polygon.GetNumPoints() < 16);  // Sanity check.

	if (polygon.IsSingleSided())
	{
		if (!IsFacing( polygon.GetPlane() ))
		{
			for (int i = 0; i <	polygon.GetNumPoints(); i++)
				polygon.SetEdgeVisible(i, false);
//rs			PF_STOP(TestPolygon);
			return false;
		}
	}

	int firstOutcode = 0;
	int thisOutcode = 0;
	int prevOutcode = 0;
	// We skip the front plane check.
	int andTest = 0x3e;  // 0x3f;

	int i, j, k;

	int nTheseHidingPlanes = 0;
	int nPrevHidingPlanes = 0;
	int nFirstHidingPlanes = 0;
	int hidingPlanes1[6];
	int hidingPlanes2[6];
	int firstHidingPlanes[6];

	int *theseHidingPlanes = hidingPlanes1;
	int *prevHidingPlanes = hidingPlanes2;

	/////////////////////////
	// Consider the first point.

	int bit_shift = 1;

	for (j = PLANE_FAR; j < NUMOF_PLANES; j++)
	{
		if (WorldPlanes[j].IsPointInFront(polygon.GetPoint(0)))
		{
			firstOutcode |= bit_shift;
			prevHidingPlanes[nPrevHidingPlanes++] = j;
			firstHidingPlanes[nFirstHidingPlanes++] = j;
		}
		bit_shift = bit_shift<<1;
	}

	andTest &= firstOutcode;

	prevOutcode = firstOutcode;


	/////////////////////////
	// Remaining points in polygon.

	Vector3 p1, p2, out;
	int InvisEdges = 0;

	for (i = 1; i < polygon.GetNumPoints(); i++)  // Starts at 1.
	{
		// Assume edge visibility.
		polygon.SetEdgeVisible(i - 1, true);

		p2 = polygon.GetPoint(i);

		thisOutcode = 0;
		nTheseHidingPlanes = 0;

		bit_shift = 1;
		for (j = PLANE_FAR; j < NUMOF_PLANES; j++)
		{
			if (WorldPlanes[j].IsPointInFront(p2))
			{
				thisOutcode |= bit_shift;
				theseHidingPlanes[nTheseHidingPlanes++] = j;
			}
			bit_shift = bit_shift << 1;
		}

		andTest &= thisOutcode;
		//		orTest |= thisOutcode;

		if (thisOutcode & prevOutcode)
		{
			// Both points are behind the same plane(s).
			// The edge is not visible.

			polygon.SetEdgeVisible(i - 1, false);
			//			Displayf("edge %d is invisible (%d)", i - 1, TIME.GetFrameCount());
			InvisEdges++;
		}
		else
			if (thisOutcode && prevOutcode)
			{
				// This point was behind one or more planes.
				// Intersect edge with the plane(s) that p1 is hidden by.

				Assert(nPrevHidingPlanes > 0);
				Assert(nTheseHidingPlanes > 0);

				p1 = polygon.GetPoint(i - 1);

				bool hidden = false;
				for (k = 0; k < nPrevHidingPlanes && !hidden; k++)
				{
					Assert(prevHidingPlanes[k] != theseHidingPlanes[0]);

					if (WorldPlanes[prevHidingPlanes[k]].IntersectLine(p1, p2, out))
					{
						// Test if intersection point is revealed by the plane that p2 is hidden by.
						// If the intersection point is invisible, the edge is hidden.
						// Else the edge may still be visible.

						if (WorldPlanes[theseHidingPlanes[0]].IsPointInFront(out))
						{
							// The edge is not visible.
							polygon.SetEdgeVisible(i - 1, false);
							InvisEdges++;
							hidden = true;
						}
					}
				}
			}

			// Swap buffers.
			int *tmp = theseHidingPlanes;
			theseHidingPlanes = prevHidingPlanes;
			prevHidingPlanes = tmp;
			nPrevHidingPlanes = nTheseHidingPlanes;
			prevOutcode = thisOutcode;
	}

	//////////////////////////////////
	// Final edge in polygon.

	p2 = polygon.GetPoint(0);

	thisOutcode = firstOutcode;
	theseHidingPlanes = firstHidingPlanes;
	nTheseHidingPlanes = nFirstHidingPlanes;

	// Assume edge visibility.
	polygon.SetEdgeVisible(i - 1, true);

	if (thisOutcode & prevOutcode)
	{
		// Both points are behind the same plane(s).
		// The edge is not visible.
		polygon.SetEdgeVisible(i - 1, false);
		//		Displayf("edge %d is invisible (%d)", i - 1, TIME.GetFrameCount());
		InvisEdges++;
	}
	else
		if (thisOutcode && prevOutcode)
		{
			// This point was behind one or more planes.
			// Intersect edge with the plane(s) that p1 is hidden by.

			Assert(nPrevHidingPlanes > 0);
			Assert(nTheseHidingPlanes > 0);

			p1 = polygon.GetPoint(i - 1);

			bool hidden = false;
			for (k = 0; k < nPrevHidingPlanes && !hidden; k++)
			{
				Assert(prevHidingPlanes[k] != theseHidingPlanes[0]);

				if (WorldPlanes[prevHidingPlanes[k]].IntersectLine(p1, p2, out))
				{
					// Test if intersection point is revealed by the plane that p2 is hidden by.
					// If the intersection point is invisible, the edge is hidden.
					// Else the edge may still be visible.

					if (WorldPlanes[theseHidingPlanes[0]].IsPointInFront(out))
					{
						// The edge is not visible.
						polygon.SetEdgeVisible(i - 1, false);
						InvisEdges++;
						hidden = true;
					}
				}
			}
		}

		if (andTest)
		{
//rs			PF_STOP(TestPolygon);
			return false;  // Completely outside frustum.
		}

		if (InvisEdges == polygon.GetNumPoints())
		{
			// Beware false-positives. We think that the polygon
			// may be filling the viewport, but perform a ray check
			// to make sure, as it may be completely off-screen.

			// TODO: Probably doesn't work for double-sided occluders.

			Vector3 p1 = GetPosition();
			Vector3 p2 = p1 + (GetCenterVector()) * 1000.0f;  // Magic, length of shaft.
			if (!polygon.LineIntersects(p1, p2))
			{
				//			Displayf("Poly is off-screen frame=%d", TIME.GetFrameCount());
//rs				PF_STOP(TestPolygon);
				return false;  // False-positive detected.
			}
		}

//rs		PF_STOP(TestPolygon);
		return true;
}

bool spdShaft::IsVisible(const spdPolygon &polygon) const
{
	Assert(polygon.GetNumPoints() > 0 && polygon.GetNumPoints() < 16);  // Sanity check.

	if (polygon.IsSingleSided())
	{
		if (!IsFacing( polygon.GetPlane() ))
			return false;
	}

//rs	PF_START(TestPolygon);

	//	int andTest = 0x3f;
	int andTest = 0x3e;  // We skip the front plane check.
	int test;

	for (int i = 0; i < polygon.GetNumPoints(); i++)
	{
		test = 0;
		u32 bit_shfit = 1<<PLANE_FAR;
		for (int j = PLANE_FAR; j < NUMOF_PLANES; j++)
		{
			if (WorldPlanes[j].IsPointInFront(polygon.GetPoint(i)))
				test |= bit_shfit;

			bit_shfit = bit_shfit<<1;
		}

		andTest &= test;
		if (andTest == 0)
		{
//rs			PF_STOP(TestPolygon);
			return true;  // We can see some part of the polygon.
		}
	}

//rs	PF_STOP(TestPolygon);

	if (andTest)
		return false;  // Completely outside frustum.

	return true;
}


grcCullStatus spdApical::IsProjectable(const spdPolygon &polygon) const
{
	Assert(polygon.GetNumPoints() > 0 && polygon.GetNumPoints() < 16);  // Sanity check.

	//	PF_START(TestPortal);  // Lumped in with IsVisible(rmwPortal&).

	grcCullStatus res;

	int andTest = 1;
	int orTest = 0;
	int test;

	for (int i = 0; i < polygon.GetNumPoints(); i++)
	{
		test = 0;

		if (ViewportPlane.IsPointInFront(polygon.GetPoint(i)))
			test = 1;

		orTest |= test;
		andTest &= test;
	}

	if (andTest)
		res = cullOutside;  // Behind camera.
	else
		if (orTest)
			res = cullClipped;  // Straddling near_ plane.
		else
			res = cullInside;

	//	PF_STOP(TestPortal);
	return res;
}

/*
bool spdShaft::IsVisibleInCone(const spdSphere &sphere) const
{
// TODO: Needs more testing.

PF_START(ConeToSphere);

Vector3 v1 = sphere.GetCenter() - GetPosition();  // v1 = sphere center - cone apex
Vector3 v2 = v1 + (sphere.GetRadius() * Cone.OOSin) * Cone.Dir;  // v2 = v1 + (sphere radius * 1/sin) * cone axis
float len2 = v2.Mag2();
float e = v2.Dot(Cone.Dir);

bool res;

if ((e > 0.0f) &&
(e * e >= len2 * Cone.Cos2))
{
float m2 = v1.Mag2();
e = -v1.Dot(Cone.Dir);

if ((e > 0.0f) &&
(e * e >= m2 * Cone.Sin2))
{
res = (m2 <= sphere.GetRadius2()) ? true : false;
}
else
res = true;
}
else
res = false;

PF_STOP(ConeToSphere);
return res;
}
*/

// TODO:
/*
void spdShaft::UpdateCone(void)
{
// TODO: Needs more testing.

// We make the assumption that the planes were constructed using points that
// are on the world-space viewport plane. There are asserts to check this
// in the Set??? (float) functions.

// Actually those asserts have been removed, at least for now,
// because they would fire when the cut-scene camera did a cut.
// It only occured on PS2 though, something to do with how viewports work.

int i;

Vector3 c;
c.Zero();
for (i = PLANE_LEFT; i <= PLANE_BOTTOM; i++)
c += GetPlane(i).Pos;
c /= 4.0f;

Cone.Dir = c - GetPosition();
Cone.Dir.Normalize();

float a = FLT_MIN;
for (i = PLANE_LEFT; i <= PLANE_BOTTOM; i++)
{
Vector3 v = GetPlane(i).Pos - GetPosition();
v.Normalize();  // Required?
float b = v.Dot(Cone.Dir);

b = Clamp(b, -1.0f, 1.0f);

if (acosf(b) > a)
a = acosf(b);
}

// Henry had this fire for some reason. 08/15/03
//	Assert(a > FLT_MIN);
if (a <= FLT_MIN)
{
Errorf("spdShaft::UpdateCone() - unidentified problem with camera (a=%f)", a);
return;
}

// See culling inside viewport.
//	a *= 0.5f;

Cone.Angle = a;
Cone.OOSin = 1.0f / sinf(a);
Cone.Sin2 = sinf(a) * sinf(a);
Cone.Cos2 = cosf(a) * cosf(a);
}
*/


#if __BANK
//#include "grcore/device.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"

int spdApical::NumProjectedPoints = 0;
int spdApical::SetMatrixCalls = 0;
int spdApical::PolygonsProjected = 0;
int spdApical::PolygonsClipped = 0;
bool spdApical::WidgetsAdded = false;

void spdApical::AddWidgets(void)
{
	Assert(!WidgetsAdded);
	WidgetsAdded = true;

	bkBank &bk = BANKMGR.CreateBank("Frusta");

	bk.PushGroup( "Stats", false );
	bk.AddSlider("Projected Points", &NumProjectedPoints, 0, 0, 0);
	bk.AddSlider("SetMatrix Calls", &SetMatrixCalls, 0, 0, 0);

	//	bk.AddSlider("Camera Position", &CameraPosition, -100000, 100000, 1.0f);

	bk.AddSlider("Polygons Projected", &PolygonsProjected, 0, 0, 0);
	bk.AddSlider("Polygons Clipped", &PolygonsClipped, 0, 0, 0);
	bk.PopGroup();

	//	bk.PushGroup( "Debug", false );
	//	bk.AddToggle("Use Box Frustum Tests", &UseBoxFrustumTests);
	//	bk.AddToggle("Draw Projected Spheres", &DrawProjectedSpheres);
	//	bk.PopGroup();

}
#endif


bool spdShaft::Intersect(const spdPolygon &polygon)
{
	bool res;

	float minx = FLT_MAX;
	float maxx = -FLT_MAX;
	float miny = FLT_MAX;
	float maxy = -FLT_MAX;

	atRangeArray<Vector2, MAXPROJECTED> clipped;

	int count = polygon.ProjectShaft(*this, &clipped);

	if (count > 0)
	{
//rs		PF_START(Intersect);

		for (int i = 0; i < count; i++)
		{
			// Find the projected bounds.
			Vector2 v = clipped[i];
			if (v.x < minx)
				minx = v.x;
			if (v.x > maxx)
				maxx = v.x;
			if (v.y < miny)
				miny = v.y;
			if (v.y > maxy)
				maxy = v.y;
		}

		// Crop the bounds to the current region.
		// Doesn't seem to matter much.
		if (minx < GetX1())
			minx = GetX1();
		if (maxx > GetX2())
			maxx = GetX2();
		if (miny < GetY1())
			miny = GetY1();
		if (maxy > GetY2())
			maxy = GetY2();

		// Trivial reject.
		if (minx > GetX2())
			return false;
		if (maxx < GetX1())
			return false;
		if (miny > GetY2())
			return false;
		if (maxy < GetY1())
			return false;

		// Shrink the region.
		if (minx > GetX1())
			SetX1(minx);

		if (maxx < GetX2())
			SetX2(maxx);

		if (miny > GetY1())
			SetY1(miny);

		if (maxy < GetY2())
			SetY2(maxy);

//				if (GetFrontPlane().IsSimilar(polygon.GetPlane()))
//		{
//		Displayf("Overlapping portals? frame=%d", TIME.GetFrameCount());
//		//			return false;
//		}
		
		SetFrontPlane(polygon.GetPlane());

#if __DEV
		// Only really required for the detach camera widgets,
		// so that the intersected shafts can be debug-drawn.
		UpdateCorners();
#else
		//for much for a dev only
		for (u32 j=0; j<NUMOF_PLANES; j++)
		{
			// Create the absolute values of the plane normals.
			AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal());
		}
#endif

		res = true;
//rs		PF_STOP(Intersect);
	}
	else
		res = false;  // The portal is out of view.

	return res;
}

void spdShaft::ComputeOrthoCorners()
{
	Matrix34 cameraMatrix(GetMatrix());
	//cameraMatrix.Inverse();

	Vector3 camPos(cameraMatrix.d);
	Vector3 camDir(-cameraMatrix.c); 
	Vector3 camA(cameraMatrix.a);
	Vector3 camB(cameraMatrix.b);

	// forget about the position ... we deal with a directional light
	camDir.Normalize();
	camA.Normalize();
	camB.Normalize();

	// rectangle that is seen by the light camera
	// camRange - light cameras nearClip clipping plane
	//  m_LightNearClip - ?
	Vector2 camViewWidth;  
	Vector2 camViewHeight; 

	camViewWidth.Set(Viewport.GetTanHFOV() * Viewport.GetFarClip(), Viewport.GetTanHFOV() * Viewport.GetFarClip());
	camViewHeight.Set(Viewport.GetTanVFOV() * Viewport.GetFarClip(), Viewport.GetTanVFOV() * Viewport.GetFarClip());

	Vector3 nearCenter(camPos);
	// add a vector that points in the camera direction and 
	// is scaled to get the nearClip center of the frustum
	nearCenter.AddScaled(camDir, Viewport.GetNearClip());
	Vector3 farCenter(camPos);
	// add a vector that points in the camera direction and 
	// is scaled to get the farClip center of the frustum
	farCenter.AddScaled(camDir, Viewport.GetFarClip());

	// left nearClip point from viewer camera frustum
	Vector3 leftNear(camA);
	leftNear.Scale(camViewWidth.x);

	// up nearClip point from viewer camera frustum
	Vector3 upNear(-camB);
	upNear.Scale(camViewHeight.x);

	// left farClip point from viewer camera frustum
	Vector3 leftFar(camA);
	leftFar.Scale(camViewWidth.y);

	// up farClip point from viewer camera frustum
	Vector3 upFar(-camB);
	upFar.Scale(camViewHeight.y);

	// Generate visiblity box
	int i;
	for(i = 0; i < 4; i++)
	{
		// fill up with the nearClip and farClip center points
		WorldCorners[i].Set(nearCenter);
		WorldCorners[i + 4].Set(farCenter);
	}

	// Create nearClip plane
	// creates based on the nearClip center point the four points that form 
	// the nearClip plane of a viewing frustum
	// go from nearClip center point to the left and then up
	WorldCorners[0].Add( leftNear);  WorldCorners[0].Add( upNear);
	// go from nearClip center point to the right and then up 
	WorldCorners[1].Add(-leftNear);  WorldCorners[1].Add( upNear);
	// go from the nearClip center point to the right and then down
	WorldCorners[2].Add(-leftNear);  WorldCorners[2].Add(-upNear);
	// go from the nearClip center point to the left and then down
	WorldCorners[3].Add( leftNear);  WorldCorners[3].Add(-upNear);

	// Create farClip plane
	// creates based on the farClip center point the four points that form
	// the farClip plane of a viewing frustum
	// go from the farClip center point to the left and then up
	WorldCorners[4].Add( leftFar);  WorldCorners[4].Add( upFar);
	// go from the farClip center point to the right and then up
	WorldCorners[5].Add(-leftFar);  WorldCorners[5].Add( upFar);
	// go from the farClip center point to the right and then down
	WorldCorners[6].Add(-leftFar);  WorldCorners[6].Add(-upFar);
	// go from the farClip center point to the left and then down
	WorldCorners[7].Add( leftFar);  WorldCorners[7].Add(-upFar);
}

void spdShaft::UpdateCorners(void)
{
	if (CornersUpToDate)
		return;

	if (m_IsOrtho)
	{
		ComputeOrthoCorners();
	}
	else
	{
		float x1 = GetX1();
		float x2 = GetX2();
		float y1 = GetY1();
		float y2 = GetY2();

		float nearClip = GetViewport().GetNearClip();
	#if __DEV
		float farClip = GetViewport().GetFarClip();
	#endif

		float width = 2.0f * nearClip * GetViewport().GetTanHFOV();
		float height = 2.0f * nearClip * GetViewport().GetTanVFOV();

#if FAST_SPDVIEWPORT
		float fx1 = x1 * GetViewport().GetInvWidth();
		float fx2 = x2 * GetViewport().GetInvWidth();
		float fy1 = y1 * GetViewport().GetInvHeight();
		float fy2 = y2 * GetViewport().GetInvHeight();
#else
		float fx1 = x1 / GetViewport().GetWidth();
		float fx2 = x2 / GetViewport().GetWidth();
		float fy1 = y1 / GetViewport().GetHeight();
		float fy2 = y2 / GetViewport().GetHeight();
#endif
		float localx1 = (fx1 * width) - (width * 0.5f);
		float localx2 = (fx2 * width) - (width * 0.5f);
		float localy1 = (fy1 * height) - (height * 0.5f);
		float localy2 = (fy2 * height) - (height * 0.5f);


		Vector3 localCorners[4] = {
			Vector3(localx1, -localy1, -nearClip),
				Vector3(localx1, -localy2, -nearClip),
				Vector3(localx2, -localy1, -nearClip),
				Vector3(localx2, -localy2, -nearClip),
		};

		static int cornerIndexes[8] = {
			// Front, back.
			0, 4,  // Top left.
				3, 7,  // Bottom left.
				1, 5,  // Top right.
				2, 6,  // Bottom right.
		};
#if FAST_SHAFT
		BoolV success;
#else
		bool success;
#endif
		Vector3 pos = GetPosition();  // Ray origin.
		Vector3 corner;  // Ray direction.
		

		// Fire rays along the edges of the shaft, into the nearClip and farClip planes,
		// to determine the corners.

		for (int i = 0; i < 4; i++)
		{
			corner.Set(localCorners[i]);
			corner.Normalize();
			GetMatrix().Transform3x3(corner);
	
			int nearCorner = cornerIndexes[i * 2];
			int farCorner = cornerIndexes[(i * 2)+1];

#if FAST_SHAFT
			success = BoolV(V_TRUE);
			success &= WorldPlanes[PLANE_NEAR].IntersectRayV( VECTOR3_TO_VEC3V(pos), VECTOR3_TO_VEC3V(corner), RC_VEC3V(WorldCorners[ nearCorner ]) );
			success &= WorldPlanes[PLANE_FAR].IntersectRayV( VECTOR3_TO_VEC3V(pos), VECTOR3_TO_VEC3V(corner), RC_VEC3V(WorldCorners[ farCorner ]) );
#else
			success = true;
			success &= WorldPlanes[PLANE_NEAR].IntersectRay(pos, corner, WorldCorners[ nearCorner ] );
			success &= WorldPlanes[PLANE_FAR].IntersectRay(pos, corner, WorldCorners[ farCorner ] );
#endif
	#if __DEV

		#if FAST_SHAFT
			if ( !IsTrue( success ) )
		#else
			if (!success)
		#endif
			{
				// Should be compiled in. Switched off to prevent spew for Dec '03 milestone.
				//			Errorf("spdShaft::UpdateCorners() Problem with finding corners %d, %d", nearCorner, farCorner);
				WorldCorners[nearCorner] = pos + (corner * nearClip);
				WorldCorners[farCorner] = pos + (corner * farClip);
			}
	#endif
		}
	}

#if FAST_SHAFT

	int j = 0;
	Vector3 bmin = VEC3V_TO_VECTOR3(Vec3V(V_FLT_MAX));
	Vector3 bmax;
	bmax.Negate(bmin);
	
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;
	geomVectors::Min(bmin, WorldCorners[j]); geomVectors::Max(bmax, WorldCorners[j]); ++j;

	Assert( j == 8 );

	BoundingBox.Set(bmin, bmax);

	j = 0;
	AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
	AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
	AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
	AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
	AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;
	AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal()); ++j;

	Assert(j == NUMOF_PLANES);

#else
	// compute bounding box:
	int j;
	Vector3 bmin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 bmax;
	bmax.Negate(bmin);
	for(j = 0; j < 8; j++)
	{
		const Vector3 &p = WorldCorners[j];
		geomVectors::Min(bmin,p);
		geomVectors::Max(bmax,p);
	}

	for (j=0; j<NUMOF_PLANES; j++)
	{
		// Create the absolute values of the plane normals.
		AbsPlaneNormals[j].Abs(WorldPlanes[j].GetNormal());
	}
	BoundingBox.Set(bmin, bmax);
#endif

	CornersUpToDate = true;
}



Vector3 spdShaft::GetCenterVector(void) const
{
#if FAST_SHAFT
	Vector3 c = GetCorners()[0];
			c += GetCorners()[1];
			c += GetCorners()[2];
			c += GetCorners()[3];

	c = VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(c), ScalarVFromF32(0.25f) ) );
	Vector3 v = c - GetPosition();
	v.Normalize();
	return v;

#else
	Vector3 c;
	c.Zero();

	for (int i = 0; i < 4; i++)
		c += GetCorners()[i];
	c *= (const float)(1.0f / 4.0f);

	Vector3 v = c - GetPosition();
	v.Normalize();
	return v;
#endif
}


void spdApical::PrepareProjections(const grcViewport &viewport)
{
	// All the once-a-frame stuff for projecting points.

	BANK_ONLY(NumProjectedPoints = 0);
	BANK_ONLY(PolygonsProjected = 0);
	BANK_ONLY(PolygonsClipped = 0);

	ProjN.FromMatrix34(GetMatrix());
	ProjM.FastInverse(ProjN);
	ProjN.Dot(RCC_MATRIX44(viewport.GetProjection()), ProjM);

	Assert(viewport.GetNearClip() == GetViewport().GetNearClip());

	// Set the viewport point-on-plane, normal.
	ViewportPlane.Set( Vector3(0.0f, 0.0f, -viewport.GetNearClip()), Vector3(0.0f, 0.0f, 1.0f) );
#if __ASSERT
	float mag;
	mag = ViewportPlane.GetNormal().Mag();
	Assert(mag > 0.95f && mag < 1.05f);
#endif

	ViewportPlane.Transform( GetMatrix() );
#if __ASSERT
	mag = ViewportPlane.GetNormal().Mag();
	Assert(mag > 0.95f && mag < 1.05f);
#endif
}


grcCullStatus spdApical::Project(Vector3::Vector3Param point, Vector2 &out) const
{
	return ProjectInline(point, out);
}


bool spdApical::IsRegionBigEnough(void) const
{
	Assert(X2 >= X1);
	Assert(Y2 >= Y1);
	float dx = X2 - X1;
	float dy = Y2 - Y1;

	if (dx < GetViewport().GetWidth() * 0.01f)
		return false;

	if (dy < GetViewport().GetHeight() * 0.01f)
		return false;

	return true;
}


void spdShaft::DrawDebug(bool DEV_ONLY(solid), bool DEV_ONLY(depthTest)) const
{
#if __DEV
	{
		grcBindTexture(NULL);
		grcState::SetAlphaBlend(false);
		grcState::SetDepthWrite(false);
		grcState::SetDepthTest(depthTest);

		u32 previousColor = grcCurrentColor;

		const Vector3 *corners = GetCorners();

		if (solid)
		{
			// Do the solid part
			Vector3 polygon[4];

			polygon[0] = corners[0];
			polygon[1] = corners[1];
			polygon[2] = corners[2];
			polygon[3] = corners[3];
			grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.0f, 0.0f, 0.8f));

			polygon[0] = corners[0];
			polygon[1] = corners[4];
			polygon[2] = corners[6];
			polygon[3] = corners[2];
			grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.8f, 0.0f, 0.8f));

			polygon[0] = corners[0];
			polygon[1] = corners[4];
			polygon[2] = corners[5];
			polygon[3] = corners[1];
			grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.0f, 0.0f, 0.8f));

			polygon[0] = corners[1];
			polygon[1] = corners[5];
			polygon[2] = corners[7];
			polygon[3] = corners[3];
			grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.0f, 0.8f, 0.8f));

			polygon[0] = corners[2];
			polygon[1] = corners[6];
			polygon[2] = corners[7];
			polygon[3] = corners[3];
			grcDrawPolygon(polygon, 4, NULL, true, Color32(0.0f, 0.8f, 0.8f, 0.8f));

			polygon[0] = corners[4];
			polygon[1] = corners[5];
			polygon[2] = corners[6];
			polygon[3] = corners[7];
			grcDrawPolygon(polygon, 4, NULL, true, Color32(0.0f, 0.0f, 0.8f, 0.8f));
		}

		// 0-3 = nearClip plane
		// 4-7 = farClip plane
		Vector3 color;
		color.Set(1.0f, 1.0f, 1.0f);

		if (m_IsOrtho)
		{
			// Draw shaft.
			grcDrawLine(corners[0], corners[4], color); 
			grcDrawLine(corners[1], corners[5], color); 
			grcDrawLine(corners[2], corners[6], color); 
			grcDrawLine(corners[3], corners[7], color); 
		}
		else
		{
			// Draw shaft.
			grcDrawLine(GetPosition(), corners[4], color); 
			grcDrawLine(GetPosition(), corners[5], color); 
			grcDrawLine(GetPosition(), corners[6], color); 
			grcDrawLine(GetPosition(), corners[7], color); 
		}

		// Draw front and back planes.
		color.Set(0.0f, 0.5f, 1.0f);
		grcDrawLine(corners[0], corners[1], color);
		grcDrawLine(corners[1], corners[2], color);
		grcDrawLine(corners[2], corners[3], color);
		grcDrawLine(corners[3], corners[0], color);
		grcDrawLine(corners[4], corners[5], color);
		grcDrawLine(corners[5], corners[6], color);
		grcDrawLine(corners[6], corners[7], color);
		grcDrawLine(corners[7], corners[4], color);
		grcDrawLine(corners[0], corners[1], color);
		grcDrawLine(corners[1], corners[2], color);
		grcDrawLine(corners[2], corners[3], color);

		//		for (int i = 0; i < ProjectedCircles.GetCount(); i++)
		//		{
		//			rmwVGLExt::DrawCircle(ProjectedCircles[i].Center, ProjectedCircles[i].Radius, ProjectedCircles[i].R, ProjectedCircles[i].G, ProjectedCircles[i].B);
		//		}

		// Restore the original color.
		grcColor(Color32(previousColor));
	}
	//	ProjectedCircles.Reset();
#endif
}

