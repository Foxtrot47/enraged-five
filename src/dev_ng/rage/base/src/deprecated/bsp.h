// 
// spatialdata/bsp.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_BSP_H
#define SPATIALDATA_BSP_H

#include "polygon.h"

#include "atl/string.h"
#include "vector/vector4.h"
#include "mesh/array.h"

namespace rage {
class spdShaft;
class bkBank;
class datResource;
class datTypeStruct;
class fiTokenizer;

#define RESOURCIFY_BUILDNODES 0  // Leave as zero.
#define STORE_DEBUG_POLYS 0      // This gets you debug drawing, and may spend quite a lot of memory.

// PURPOSE: This is similar to the spdBspNode below, but is used during construction 
// of a bsp tree from a set of polygons.
class spdBspBuildNode
{
public:
	spdBspBuildNode()
	{
		Pos = NULL;
		Neg = NULL;
		NodeNumber = 0;
	}
	~spdBspBuildNode();

	spdBspBuildNode(const spdBspBuildNode &n)
	{
		Polys = n.Polys;
		Neg = n.Neg;
		Pos = n.Pos;
		NodeNumber = n.NodeNumber;
		Plane = n.Plane;
	}

	spdBspBuildNode(datResource &);

	DECLARE_PLACE(spdBspBuildNode);

	// adds an array of polygons to this node
	void Init(const atArray<const spdPolygon*> *polys);

	// Builds a bsp tree, with this node as the root. 
	// put polygons into the node with Init, and call BuildRecursively to 
	// create a tree with those polys.
	spdBspBuildNode *BuildRecursively(bool coplanar, int &count, float tol = 0.1f) const;

	// Cut will put all the polys in the current node into neg, pos, or cop 
	// depending on which side of plane that poly is on (cop = coplanar). 
	// Polys that cross the plane will be cut into two polys.
	int Cut(const spdPlane &plane, spdBspBuildNode &neg, spdBspBuildNode &pos, spdBspBuildNode &cop, float tol = 0.1f) const;

	// Compares all polygons in the polys list with the plane. The polys will
	// be sorted into the same or opp nodes. Coplanar polys won't go into 
	// either list, and polys that cross the plane will trigger an assert.
	int Compare(const spdPlane &plane, spdBspBuildNode &same, spdBspBuildNode &opp, float tol = 0.1f) const;

	// Returns the cost for splitting the polys in this node with plane, and 
	// writes a poly count into neg, pos, cop.
	// The cost is | num_neg - num_pos | + 5 * num_splits - 2 * num_coplanar
	int CuttingCost(const spdPlane &plane, int &neg, int &pos, int &cop, float tol = 0.1f) const;

	// Finds the best plane at which to perform a cut (i.e. the plane with 
	// the smallest CuttingCost). If coplanar is true, the plane will be 
	// perpendicular to one of the polys and incorporate one poly's edge.
	// If false, the plane will be one of the polygons' planes.
	bool ComputeCuttingPlane(bool coplanar, spdPlane &resultantPlane, float tol = 0.1f) const;

#if __DEV
	void DrawRecursively(int highLightNode) const;
	void Draw(float r, float g, float b) const;
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	int CountPolysRecursively(void) const;
	int CountDataRecursively(void) const;
	int CountNodesRecursively(void) const;

	void SaveRecursively(fiTokenizer &tok) const;

	// The defining plane for this node.
	spdPlane Plane;

	// Polys that are coplanar with the Plane (after Building, 
	// just a bag of polys before then).
	atArray<spdPolygon> Polys;

	// After Building, UniqueSame has a list of all the unique Data for polys 
	// that are coplanar with Plane and face the same direction.
	// UniqueOpp has a list of all the Data that is coplanar with Plane and 
	// faces the opposite direction.
	atArray<int> UniqueSame;
	atArray<int> UniqueOpp;

	datOwner<spdBspBuildNode> Neg, Pos; // Trees containing polys on the negative, positive sides of the seperating plane

	int NodeNumber; // unique node number in the tree

protected:

private:
};

// PURPOSE: An spdBspNode is a single node in a Binary Seperating Plane tree. 
// It can be a leaf node or a branch node. A branch node will have a
// Neg or a Pos node attached to it, as well as a list of coplanar
// polys
class spdBspNode
{
public:

	// This will collect all _unique_ data elements in the tree, sorted by distance
	// from the plane to p
	void GetTreeData(Vector3::Vector3Param p, int *pData, int maxData, int &count, bool backToFront) const;

	// This will collect all _unique_ data elements in the tree but only for nodes where the cutting plane intersects the shaft
	// (sorted by distance from the plane to shaft.GetPosition()
	void GetTreeData(const spdShaft &shaft, int *pData, int maxData, int &count, bool backToFront) const;
	void GetTreeData(const spdShaft &shaft, const atArray<const spdPolygon*> *polys, int *pData, int maxData, int &count, bool backToFront) const;

	// Finds the leaf node in the tree containing p, returns either SameData[0] or OppData[0] depending on which side of the leaf node
	// p was on and returns true if data existed. If there was no leaf data, returns false, doesn't change the data argument
	bool GetLeafData(Vector3::Vector3Param p, int &data) const;
	// Traverses the tree, collecting all unique SameData[0] or OppData[0] for any leaf planes that are within radius of p
	void GetLeafData(Vector3::Vector3Param p, float radius, int *pData, int maxData, int &count) const;
	// Traverses the tree, collecting all unique SameData[0] or OppData[0] for any leaf planes that intersect the segment p1->p2
	void GetLeafData(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData, int &count) const;

	void Load(const class spdBspTree &tree, fiTokenizer &tok, int &datanum);

//private:
	// Adds all data items (SameData and OppData) to the pData list. data items will only be added if they weren't already in the list.
	void AddDataToList(int *pData, int maxData, int &count) const;
	// Adds a single data item to the pData array. data items will only be added if they weren't already in the list
	void AddDataToList(int data, int *pData, int maxData, int &count) const;
	// Adds all data items (SameData and OppData), but adds a data item "i" to the list only if polys[i] intersects shaft. 
	// as above, only unique data gets added.
	void AddDataToList(const spdShaft &shaft, const atArray<const spdPolygon*> *polys, int *pData, int maxData, int &count) const;

	// Used by GetLeafData(p1, p2, ...) when a segment is wholly on the negative side of this plane
	void GetLeafDataNeg(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData, int &count) const;
	// Used by GetLeafData(p1, p2, ...) when a segment is wholly on the positive side of this plane
	void GetLeafDataPos(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData, int &count) const;

	spdBspNode()
	{
		Plane.Invalidate();
		SameData = OppData = NULL;
		SameCount = OppCount = 0;
		Neg = Pos = NULL;
	}

	spdBspNode(datResource &);

	// TODO: Possibly store an interior branch flag,
	// for point-in-region tests that don't care about
	// exactly which region.

	spdPlane Plane;

	// Note: I tried packing all this data together in various ways
	// to save memory. But I couldn't live with the resultant code.
	// It will probably have to be revisited though. For a start,
	// closed-region trees probably don't need to store any data
	// in non-leaf nodes.

	// "Data", which is a copy of the "polygon refs"
	// from the polys that are coplanar with the Plane.
	// See comments about spdBspTree::PolyData.
	int *SameData;
	int SameCount;

	int *OppData;
	int OppCount;

	datOwner<spdBspNode> Neg;
	datOwner<spdBspNode> Pos;

	DECLARE_PLACE(spdBspNode);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

#if __DEV
	void DrawRecursively(int highLightNode) const;
	void Draw(float r, float g, float b) const;
#endif

#if STORE_DEBUG_POLYS
	// Polys that are coplanar with the Plane.
	atArray<spdPolygon> Polys;
#else
	char Pad[8];
#endif
};


// PURPOSE: An spdBspTree is the main access point for a Binary Seperating Plane tree.
// It actually contains two types of trees, a build-time tree and a run-time tree.
// It can construct a build-time tree from an array of polygons, and can save that tree
// to a file, but can't load in a build-time tree or perform querys on it.
// Conversely, a run-time tree can be loaded from a file but not saved, and
// queries can be performed on it (via GetLeafData and GetTreeData)
// <FLAG Component>
class spdBspTree
{
public:

	spdBspTree();
	spdBspTree(datResource &);
	~spdBspTree();

	// Point/region test. Which region (room) is the point in? Run-time tree only
	bool GetLeafData(Vector3::Vector3Param p, int &data) const;

	// Sphere/region test. TODO: This can return false-positives. Run-time tree only
	int GetLeafData(Vector3::Vector3Param p, float radius, int *pData, int maxData) const;

	// Line/region test. Run-time tree only
	int GetLeafData(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData) const;

	// Get a sorted list of polys from a point. Run-time tree only
	int GetTreeData(Vector3::Vector3Param p, int *pData, int maxData, bool backToFront) const;

	// Get a sorted list of polys, while plane-culling against a shaft. Run-time tree only
	int GetTreeData(const spdShaft &shaft, int *pData, int maxData, bool backToFront) const;

	// Get a sorted list of polys, while polygon-culling against a shaft.
	// The data at each tree node is considered to be an index into the polygons and only
	// indices for polys that are visible from the shaft are added to the pData list
	// Run-time tree only
	int GetTreeData(const spdShaft &shaft, const atArray<const spdPolygon*> *polys, int *pData, int maxData, bool backToFront) const;

	// TODO: Sphere/polys test.
	//	int GetTreeData(const Vector3 &p, float radius, const atArray<const spdPolygon*> *polys, int *pData, int maxData, bool backToFront) const;

	// TODO: Line/polys test.
	//	int GetTreeData(const Vector3 &p1, const Vector3 &p2, const atArray<const spdPolygon*> *polys, int *pData, int maxData, bool backToFront) const;

#if __DEV && !__TOOL
	void DrawDebug(void) const;
	void Draw(int highLightNode = -1) const;
#endif
#if __BANK
	void AddWidgets(bkBank & bank, const char *name);
#endif
	// creates a build-time tree from an array of polygons (leaves the run-time tree alone)
	void Build(const atArray<spdPolygon> *polys, bool closedRegions);
	void Build(const atArray<const spdPolygon*> *polys, bool closedRegions);

	// PURPOSE: Convert the build-time tree to a runtime tree, then delete the build time.
	void ConvertBuildToRuntime();

	DECLARE_PLACE(spdBspTree);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

//protected:

	// Loads a tree from a file. This function loads a run-time tree and leaves the build-time tree alone.
	void Load(const char *name, bool temporary);
	// Save saves the tree info (i.e. the Data array) and only saves the build-time tree.
	void Save(const char *name);


	// Build-time tree.
	datOwner<spdBspBuildNode> Built;

	// Run-time tree. Root points to an array of all spdBspNodes (Root[0] being the tree root)
	int NodeCount;
	datOwner<spdBspNode> Root;

	// Number of polys originally used to build the tree.
	int PolyCount;

	// Poly-data comes from the poly indexes.
	// spdBspNode::SameData and spdBspNode::OppData point into this list.
	// In the case of room polygons, these are room numbers.
	// In the case of occluder polygons, these are indexes into the occluder list.
	int *Data;
	int DataCount;
	float MemoryUsed;
	int HighlightNode;

	bool DrawTree;
	bool ClosedRegionTree;
	bool m_Pad[2];
};

}	// namespace rage

#endif
