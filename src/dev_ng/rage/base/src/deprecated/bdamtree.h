// 
// spatialdata/bdamtree.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_BDAMTREE_H
#define SPATIALDATA_BDAMTREE_H

#define SPLITLIST_TRAVERSAL 0

#include "aainfcylinder.h"

#include "atl/atfunctor.h"
#include "math/constants.h"
#include "vector/vector2.h"

namespace rage {

// PURPOSE: Represents a location on the BDAM grid.
// NOTES:
//		For even levels of detail (most importantly the base level) the grid is divided up into
//		squares where each side is 1, 2, 4, 8, etc times the base grid size.
//		The squares are then split along their diagonals into 4 triangles. These triangles are
//		identified with the row and column index for the grid square, and a direction (N, S, W, or E)
//		For odd levels of detail, we use the same grid as the level above (so LOD 1 uses LOD 2's grid)
//		but each square is divided along its diagonals as well as vertically and horizontally through the
//		center of the cell, such that
//		each grid cell contains 8 triangles. These triangles are identified with the row and column index
//		for the grid square, as well as a direction (NE, EN, ES, SE, SW, WS, WN, NW). The first letter
//		in the direction selects 2 possible triangles (so NE and NW are the two touching the northern
//		grid cell boundary, ES and EN are the two touching the eastern boundary). The second letter then
//		selects between the two.
struct spdBdamLocation
{
	enum Direction {
		DIR_N,
		DIR_E,
		DIR_S,
		DIR_W,
		DIR_NE,
		DIR_EN,
		DIR_ES,
		DIR_SE,
		DIR_SW,
		DIR_WS,
		DIR_WN,
		DIR_NW,
		DIR_INVALID
	};

	spdBdamLocation(int level, int row, int col, Direction dir)
		: m_Level(level)
		, m_Row(row)
		, m_Col(col)
		, m_Dir(dir)
	{
	}

	spdBdamLocation()
		: m_Level(-1)
		, m_Row(0)
		, m_Col(0)
		, m_Dir(DIR_N)
	{
	}

	void Set(int level, int row, int col, Direction dir)
	{
		m_Level = level;
		m_Row = row;
		m_Col = col;
		m_Dir = dir;
	}

	bool operator==(const spdBdamLocation& other) const {
		return m_Level == other.m_Level &&
			m_Row == other.m_Row &&
			m_Col == other.m_Col &&
			m_Dir == other.m_Dir;
	}
	bool operator!=(const spdBdamLocation& other) const {
		return !(*this == other);
	}

	bool operator<(const spdBdamLocation& other) const {
		if (m_Level != other.m_Level)
		{
			return m_Level < other.m_Level;
		}

		if (m_Row != other.m_Row)
		{
			return m_Row < other.m_Row;
		}

		if (m_Col != other.m_Col)
		{
			return m_Col < other.m_Col;
		}

		return m_Dir < other.m_Dir;
	}

	void GetChildLocations(spdBdamLocation& outLeft, spdBdamLocation& outRight) const;

	void GetParentLocation(spdBdamLocation& outParent) const;

	Vector2 GetLongEdgeCenter(float gridSize) const;

	static int Compare(const spdBdamLocation* a, const spdBdamLocation* b)
	{
		if (a == NULL) {
			if (b == NULL) {
				return 0;
			}
			return 1;
		}
		if (b == NULL) {
			return -1;
		}

		if (a->m_Level == b->m_Level)
		{
			if (a->m_Row == b->m_Row) 
			{
				if (a->m_Col == b->m_Col)
				{
					if (a->m_Dir == b->m_Dir) 
					{
						return 0;
					}
					else
					{
						return a->m_Dir < b->m_Dir ? -1 : 1;
					}
				}
				else 
				{
					return a->m_Col < b->m_Col ? -1 : 1;
				}
			}
			else 
			{
				return a->m_Row < b->m_Row ? -1 : 1;
			}
		}
		else
		{
			return a->m_Level < b->m_Level ? -1 : 1;
		}
	}

	// Returns a bounding cylinder that contains not just this node's children, but also contains the children
	// of this nodes long-edge-neighbor. Therefore, if we're in the cylinder for this node, we're also
	// in the cylinder for the long edge neighbor. So we can never draw the long edge neighbors kids without
	// also drawing this node (or its kids)
	// NOTES:
	//   The recursive way to get the bounding cylinder:
	//		Find the bounding cylinders for the two children
	//		Radius is the distance from this node's long edge center to a child's long edge center, plus the child's radius
	//   The analytic way to get the bounding cylinder:
	//		The method above produces the following sequence
	//		LOD 0: 0.5
	//		LOD 1: 1
	//		LOD 2: 1 + 1/sqrt(2)
	//		LOD 3: 2 + 1/sqrt(2)
	//		LOD 4: 2 + 3/sqrt(2)
	//		LOD 5: 4 + 3/sqrt(2)
	//		So going from an odd LOD l to an even LOD l', we add 2^((l'/2)-1)/sqrt(2), which means the total contribution
	//		from the even LODs is the sum from 0 to (l'/2), or (2^(l'/2))-1
	//      Going from an even LOD l to an odd LOD l', we add 2^(((l'-1)/2)-1), so the total contribution
	//      from the odd LODs is the sum from 0 to (l'/2), or (2^(((l'-1)/2)) - 1
	//		And there is an extra 0.5 from LOD 0.
	//		So the final bounding volume for any LOD L is:
	//				(2^(L/2) - 1)/sqrt(2) + 2 ^ ((L-1) / 2)
	//   We can get an even better bound by not bounding the child bounding cylinders, but rather bounding the
	//   optimal bounding volume (which looks like a jaggedy octagon). The furthest point from the center of
	//   octagon follows this sequence:
	//      LOD 0: 0.5, 0.5
	//		LOD 1: 1, 1
	//		LOD 2: 1, 2
	//		LOD 3: 2, 3
	//		LOD 4: 2, 5
	//		LOD 5: 4, 7
	//		And the radius at each LOD is then just sqrt(x^2 + y^2) / sqrt(2)
	//		The X coord grows in the sequence .5, 0, 1, 0, 2, 0, 4, ...
	//		The Y coord grows in the sequence .5, 1, 1, 2, 2, 4, 4, ...
	//		So for non-zero LODs
	//		The final X value is given by 2^((l-1)/2)
	//		And the final Y value is given by 2^(l/2) + 2^((l-1)/2) - 1
	//		This method creates cylinders with approx 85% of the cross sectional area as the method above.
	//   Unfortunately we can't use these tighter bounding cylinders directly because they lack the property that
	//   each cylinder encloses its child cylinders.
	template<typename _CylinderType>
	_CylinderType GetOverlappingBoundingCylinder(float gridSize) const;

	// Uses the first sequence in the comment above
	static float GetBoundRadiusMethod1(int level);
	// Uses the second sequence in the comment above
	static float GetBoundRadiusMethod2(int level);

	static spdBdamLocation FindLocationContainingPoint(Vector2 point, int lod, float gridSize);

	// The first corner will be opposite the triangle's long edge. The other two aren't in any particular order
	void GetCornerLocations(float gridSize, Vector2& pointA, Vector2& pointB, Vector2& pointC) const;

	// Returns a bounding cylinder whose center is in the middle of the triangle and just bounds the tri.
	template<typename _CylinderType>
	_CylinderType GetTightBoundingCylinder(float gridSize) const;

	// Returns the min, max corners of a 2d AABB
	void GetBoxCorners(float gridSize, Vector2& minCorner, Vector2& maxCorner) const;

	const char* GetDirName() const
	{
		return sm_DirectionNames[m_Dir];
	}

	static Direction GetDirFromString(const char* name)
	{
		for(int i = 0; i < (int)DIR_INVALID; i++) {
			if (!strcmp(name, sm_DirectionNames[i])) {
				return (Direction)i;
			}
		}
		return DIR_INVALID;
	}

	// Copies the formatString to outString, replacing %L with the level, %R with the row, %C with the column, and %D with the direction.
	// outString must be allocated with sufficient space before calling this function.
	void FormatLocationString(char* outString, const char* formatString) const;

	int m_Level;
	int m_Row;
	int m_Col;
	Direction			m_Dir;

	static Vector2 sm_Corners[][3];
	static const char* sm_DirectionNames[];
	static Vector2 sm_BoxCorners[][2];
};

// This class can be used with the spdBdamTreelessLocationIterator to provide an example split callback
class spdBdamSinglePointSplit
{
	static const int sm_NumFactors = 10;
public:
	spdBdamSinglePointSplit()
		: m_PointOfInterest(ORIGIN)
		, m_SplitSize(128.0f)
		, m_MaxLod(10000)
		, m_MinLod(0)
	{
		for(int i = 0; i < sm_NumFactors; i++) {
			m_RadiusFactors[i] = 1.0f;
		}
	}

	Vector3	m_PointOfInterest;
	float	m_SplitSize;
	int		m_MaxLod;
	int		m_MinLod;
	float   m_RadiusFactors[sm_NumFactors];
	bool SplitCallback(const spdBdamLocation& loc)
	{
		FastAssert(m_MaxLod >= m_MinLod);
		FastAssert(m_SplitSize > 0.0f);
		if (loc.m_Level > m_MaxLod) {
			return true;
		}
		if (loc.m_Level == 0 || loc.m_Level == m_MinLod) {
			return false;
		}
		spdAAInfCylinderY cyl = loc.GetOverlappingBoundingCylinder<spdAAInfCylinderY>(m_SplitSize);
		cyl.SetRadius(cyl.GetRadius() * m_RadiusFactors[Min(sm_NumFactors-1,(loc.m_Level-1))]);
		return cyl.Contains(m_PointOfInterest);
	}
};

// Given a starting location and a function that determines whether or not a region of interest
// intersects a spdBdamLocation, this class lets you make a list of all the nodes you'll need.
class spdBdamTreelessLocationIterator
{
public:
	void Init(spdBdamLocation root, atFunctor1<bool, const spdBdamLocation&> splitcb);

	spdBdamLocation GetCurrent() {
		return m_CurrLocation;
	}
	bool MoveNext();

protected:
	spdBdamLocation m_Root;
	atFunctor1<bool, const spdBdamLocation&> m_ShouldSplitCb;
	spdBdamLocation m_CurrLocation;
	int m_MaxLod;
};


template<typename _Data, typename _CylinderType = spdAAInfCylinderY>
class spdBdamNode
{

	typedef spdBdamNode<_Data, _CylinderType> Node;
	typedef spdBdamNode<_Data, _CylinderType>* NodePtr;

public:
	spdBdamNode()
		: m_Data(NULL)
		, m_Parent(NULL)
		, m_Left(NULL)
		, m_Right(NULL)
	{
	}

#if 0
	void Draw();
	void DrawLevel(int level);
#endif

	bool PointWithinBoundingCylinder(const Vector3& pointOfInterest);

	void ComputeOverlappingBoundingCylinder(float gridSize);

	void ComputeTightBoundingCylinder(float gridSize);

	void SelectRecursively(atFunctor1<bool, NodePtr>& splitChildrenCb);

	void AddToSelectedList();

//	static spdBdamNode<_Data, _CylinderType>* LoadFullTree(const char* baseName, const spdBdamLocation& loc);

//	static spdBdamNode<_Data, _CylinderType>* LoadNode(const char* baseName, const spdBdamLocation& loc);

	// Creates a full tree with all Data members set to NULL
	static spdBdamNode<_Data, _CylinderType>* CreateFullTree(const spdBdamLocation& rootLocation);

	// Creates a partial tree, calling a callback to know whether or not to create a particular node
	static spdBdamNode<_Data, _CylinderType>* CreateTree(const spdBdamLocation& rootLocation, atFunctor1<bool, const spdBdamLocation&>& createNodeCb);

	_Data*			m_Data;
	NodePtr			m_Parent;
	NodePtr			m_Left;
	NodePtr			m_Right;
	NodePtr			m_NextSelected;

	_CylinderType	m_Cylinder;

	spdBdamLocation	m_Location;

#if SPLITLIST_TRAVERSAL
	bool RemoveParent(dlList& drawList);
	void Split(dlList& drawList, dlList& splitList, const Vector3& point);
	void ForceSplit(const Vector2& splitVtx, dlList& drawList, dlList& splitList, const Vector3& point, TerrainTreeNode* exclude);
	Vector2				m_SplitVtx;
#endif
};

template<typename _Data, typename _CylinderType=spdAAInfCylinderY>
class spdBdamTree
{
public:
	typedef spdBdamNode<_Data, _CylinderType> Node;
	typedef spdBdamNode<_Data, _CylinderType>* NodePtr;

	struct Iterator {
		Iterator& operator++();
		bool operator==(const Iterator& other);
		bool operator!=(const Iterator& other);
		NodePtr operator->() {
			return m_CurrNode;
		}
		Node& operator*() {
			return *m_CurrNode;
		}

		NodePtr m_CurrNode;

	protected:
		Iterator(NodePtr node);
		friend class spdBdamTree;
	};

	static spdBdamTree<_Data, _CylinderType>* CreateFullTree(const spdBdamLocation& rootLocation)
	{
		spdBdamTree<_Data, _CylinderType>* tree = rage_new spdBdamTree<_Data, _CylinderType>;
		tree->m_Root = Node::CreateFullTree(rootLocation);
		return tree;
	}

	Iterator Begin();
	Iterator End();

	struct SelectedIterator {
		SelectedIterator& operator++() {
			m_CurrNode = m_CurrNode->m_NextSelected;
			return *this;
		}
		bool operator==(const SelectedIterator& other) {
			return other.m_CurrNode == m_CurrNode;
		}
		bool operator!=(const SelectedIterator& other) {
			return other.m_CurrNode != m_CurrNode;
		}
		NodePtr operator->() {
			return m_CurrNode;
		}
		Node& operator*() {
			return *m_CurrNode;
		}
	protected:
		friend class spdBdamTree<_Data, _CylinderType>;
		SelectedIterator(NodePtr node) : m_CurrNode(node) {}
		NodePtr m_CurrNode;
	};

	static SelectedIterator BeginSelected() {
		return SelectedIterator(sm_FirstSelected);
	}

	static SelectedIterator EndSelected() {
		return SelectedIterator(NULL);
	}

	static void ClearSelectedList() {
		sm_FirstSelected = NULL;
	}
	void SelectRecursively(atFunctor1<bool, NodePtr>& splitChildrenCb)
	{
		m_Root->SelectRecursively(splitChildrenCb);
	}

protected:
	NodePtr m_Root;

	friend class spdBdamNode<_Data, _CylinderType>;
	static NodePtr sm_FirstSelected;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Template implementations

inline float spdBdamLocation::GetBoundRadiusMethod1(int level) {
	// Sequence we want is: 0.5, 1.0, 1.0, 2.0, 2.0, 4.0, 4.0, 8.0, 8.0
	float wholePart = (1 << ((level+1) / 2)) * 0.5f;

	// Sequence we want is: 0.0, 0.0, 1.0, 1.0, 3.0, 3.0, 7.0, 7.0, 15.0
	float root2Part = (1 << ((level / 2))) - 1.0f;

	return wholePart + root2Part * SQRT2DIV2;
}

inline float spdBdamLocation::GetBoundRadiusMethod2(int level) {
	// This could give us a tighter bound (approx 85% the area of the above bound)
	float x = (float)(1 << (((level+1) / 2)-1));
	float y = (1 << (level / 2)) + (1 << ((level + 1) / 2 - 1)) - 1.0f;
	// head off precision problems with X^2 and Y^2 at the pass here, by doing
	// sqrt(x*x + y*y) == y * sqrt((x/y) * (x/y) + 1)
	float ratio = x / y;
	return y * sqrtf(ratio * ratio + 1.0f) * SQRT2DIV2;
}

template<typename _CylinderType>
inline _CylinderType spdBdamLocation::GetOverlappingBoundingCylinder(float gridSize) const
{
/* Old and recursive
	_CylinderType cyl;
	cyl.SetCenter(GetLongEdgeCenter(gridSize));

	if (m_Level == 0)
	{
		cyl.SetRadius(gridSize * 0.5f);
	}
	else {
		spdBdamLocation leftNode;
		spdBdamLocation rightNode;
		GetChildLocations(leftNode, rightNode);

		_CylinderType leftCyl;

		leftCyl = leftNode.GetOverlappingBoundingCylinder<_CylinderType>(gridSize);

		cyl.SetRadius(cyl.GetCenter().Dist(leftCyl.GetCenter()) + leftCyl.GetRadius());
	}
	return cyl;
	*/

	// new and analytical 

	_CylinderType cyl;
	cyl.SetCenter(GetLongEdgeCenter(gridSize));


	cyl.SetRadius(GetBoundRadiusMethod1(m_Level) * gridSize);

	return cyl;
}

template<typename _CylinderType>
inline _CylinderType spdBdamLocation::GetTightBoundingCylinder(float gridSize) const
{
	_CylinderType cyl;

	Vector2 a,b,c;
	GetCornerLocations(gridSize, a,b,c);
	Vector2 center;
	center.Average(b,c);
	center.Average(center, a);

	cyl.SetCenter(center);
	cyl.SetRadius(center.Dist(b));
	return cyl;
}

template<typename _Data, typename _CylinderType>
inline bool spdBdamNode<_Data, _CylinderType>::PointWithinBoundingCylinder(const Vector3& pointOfInterest)
{
	return m_Cylinder.Contains(pointOfInterest);

	// Could compute theoretical best bounding area here (looks like an octagon)
	// Below we just use a square that fits around the octagon
	//float gridSize = 64.0f;

	//gridSize *= 1 << m_Location.m_Level;

	//Vector2 distVec;
	//distVec.Set(pointOfInterest.x, pointOfInterest.z);
	//distVec.Subtract(m_Location.GetLongEdgeCenter(128.0f));

	//if (fabsf(distVec.x) < gridSize && fabsf(distVec.y) < gridSize) {
	//	return true;
	//}
	//return false;
}

template<typename _Data, typename _CylinderType>
inline void spdBdamNode<_Data, _CylinderType>::ComputeOverlappingBoundingCylinder(float gridSize)
{
	if (m_Location.m_Level == 0) {
		m_Cylinder = m_Location.GetOverlappingBoundingCylinder <_CylinderType>(gridSize);
	}
	else {
		spdAAInfCylinderY leftCyl;

		spdBdamLocation leftLoc, rightLoc;
		m_Location.GetChildLocations(leftLoc, rightLoc);

		if (m_Left) {
			m_Left->ComputeOverlappingBoundingCylinder(gridSize);
			leftCyl = m_Left->m_Cylinder;
		}
		else {
			leftCyl = leftLoc.GetOverlappingBoundingCylinder <_CylinderType>(gridSize);
		}

		if (m_Right) {
			m_Right->ComputeOverlappingBoundingCylinder(gridSize);
		}

		m_Cylinder.SetCenter(m_Location.GetLongEdgeCenter(gridSize));
		m_Cylinder.SetRadius(m_Cylinder.GetCenter().Dist(leftCyl.GetCenter()) + leftCyl.GetRadius());
	}
}

template<typename _Data, typename _CylinderType>
inline void spdBdamNode<_Data, _CylinderType>::ComputeTightBoundingCylinder(float gridSize)
{
	m_Cylinder = m_Location.GetTightBoundingCylinder<_CylinderType>(gridSize);

#if SPLITLIST_TRAVERSAL
	m_SplitVtx = m_Location.GetLongEdgeCenter(gridSize);
#endif

	if (m_Left) {
		m_Left->ComputeTightBoundingCylinder(gridSize);
	}
	if (m_Right) {
		m_Right->ComputeTightBoundingCylinder(gridSize);
	}
}

#if 0
template<typename _Data, typename _CylinderType>
spdBdamNode<_Data, _CylinderType>* spdBdamNode<_Data, _CylinderType>::LoadFullTree(const char* baseName, const spdBdamLocation& loc)
{
	spdBdamNode<_Data, _CylinderType>* node = LoadNode(baseName, loc);
	if (node && loc.m_Level > 0) {
		// load kids

		spdBdamLocation left, right;
		node->m_Location.GetChildLocations(left, right);
		node->m_Left = LoadFullTree(baseName, left);
		node->m_Right = LoadFullTree(baseName, right);

		if (node->m_Left) {
			node->m_Left->m_Parent = node;
		}
		if (node->m_Right) {
			node->m_Right->m_Parent = node;
		}
	}

	return node;
}

template<typename _Data, typename _CylinderType>
spdBdamNode<_Data, _CylinderType>* spdBdamNode<_Data, _CylinderType>::LoadNode(const char* baseName, const spdBdamLocation& loc)
{
	// generate file name

	char fileName[256];
	sprintf(fileName, "%s_L%d_c%d_r%d_%s.dmsh", baseName, loc.m_Level, loc.m_Col, loc.m_Row, spdBdamLocation::sm_DirectionNames[loc.m_Dir]);

	//	if (ASSET.Exists(fileName, NULL)) {
	spdBdamNode<_Data, _CylinderType>* node = rage_new spdBdamNode<_Data, _CylinderType>;
	//		node->m_Mesh = deMesh::CreateFromFile(fileName);
	node->m_Location = loc;
	return node;
	//   }
	//	else {
	//		return NULL;
	//	}
}
#endif

template<typename _Data, typename _CylinderType>
inline spdBdamNode<_Data, _CylinderType>* spdBdamNode<_Data, _CylinderType>::CreateFullTree(const spdBdamLocation& rootLocation)
{
	spdBdamNode<_Data, _CylinderType>* node = rage_new spdBdamNode<_Data, _CylinderType>;
	node->m_Location = rootLocation;
	if (rootLocation.m_Level > 0) {
		spdBdamLocation left, right;
		node->m_Location.GetChildLocations(left, right);
		node->m_Left = CreateFullTree(left);
		node->m_Left->m_Parent = node;
		node->m_Right = CreateFullTree(right);
		node->m_Right->m_Parent = node;
	}
	return node;
}

template<typename _Data, typename _CylinderType>
inline spdBdamNode<_Data, _CylinderType>* spdBdamNode<_Data, _CylinderType>::CreateTree(const spdBdamLocation& rootLocation, atFunctor1<bool, const spdBdamLocation&>& shouldCreate)
{
	if (!shouldCreate(rootLocation)) {
		return;
	}
	spdBdamNode<_Data, _CylinderType>* node = rage_new spdBdamNode<_Data, _CylinderType>;
	if (rootLocation.m_Level > 0) {
		spdBdamLocation left, right;
		node->m_Location.GetChildLocations(left, right);
		node->m_Left = CreateTree(left, shouldCreate);
		if (node->m_Left) {
			node->m_Left->m_Parent = node;
		}
		node->m_Right = CreateFullTree(right, shouldCreate);
		if (node->m_Right) {
			node->m_Right->m_Parent = node;
		}
	}
}

template<typename _Data, typename _CylinderType>
void spdBdamNode<_Data, _CylinderType>::SelectRecursively(atFunctor1<bool, NodePtr>& splitChildrenCb)
{
	if ((m_Left || m_Right) && splitChildrenCb(this)) {
		if (m_Left) {
			m_Left->SelectRecursively(splitChildrenCb);
		}
		if (m_Right) {
			m_Right->SelectRecursively(splitChildrenCb);
		}
	}
	else {
		AddToSelectedList();
	}
}

template<typename _Data, typename _CylinderType>
void spdBdamNode<_Data, _CylinderType>::AddToSelectedList()
{
	m_NextSelected = spdBdamTree<_Data, _CylinderType>::sm_FirstSelected;
	spdBdamTree<_Data, _CylinderType>::sm_FirstSelected = this;
}

template<typename _Data, typename _CylinderType>
inline typename spdBdamTree<_Data, _CylinderType>::Iterator& spdBdamTree<_Data, _CylinderType>::Iterator::operator ++()
{
	if (m_CurrNode) {
		// As long as you can go left, do so.
		if (m_CurrNode->m_Left) {
			m_CurrNode = m_CurrNode->m_Left;
		}
		else if (m_CurrNode->m_Right) {
			m_CurrNode = m_CurrNode->m_Right;
		}
		else { // We're on a leaf node, so walk up the tree until we've walked along a left edge. Then walk down the right edge
				// if it exists, or keep walking otherwise.

			// If you can't go left, go up until you can go right
			NodePtr prev = m_CurrNode;
			m_CurrNode = m_CurrNode->m_Parent;
			while(m_CurrNode) {
				if (m_CurrNode->m_Left == prev) {
					// walked along a left edge
					if (m_CurrNode->m_Right) {
						m_CurrNode = m_CurrNode->m_Right;
						break;
					}
				}
				prev = m_CurrNode;
				m_CurrNode = m_CurrNode->m_Parent;
			}
		}
	}
	return *this;
}

template<typename _Data, typename _CylinderType>
spdBdamNode<_Data, _CylinderType>* spdBdamTree<_Data, _CylinderType>::sm_FirstSelected = NULL;

template<typename _Data, typename _CylinderType>
inline bool spdBdamTree<_Data, _CylinderType>::Iterator::operator ==(const typename spdBdamTree<_Data, _CylinderType>::Iterator& other) {
	return other.m_CurrNode == m_CurrNode;
}

template<typename _Data, typename _CylinderType>
inline bool spdBdamTree<_Data, _CylinderType>::Iterator::operator !=(const typename spdBdamTree<_Data, _CylinderType>::Iterator& other) {
	return other.m_CurrNode != m_CurrNode;
}

template<typename _Data, typename _CylinderType>
inline spdBdamTree<_Data, _CylinderType>::Iterator::Iterator(NodePtr node) {
	m_CurrNode = node;
}

template<typename _Data, typename _CylinderType>
inline typename spdBdamTree<_Data, _CylinderType>::Iterator spdBdamTree<_Data, _CylinderType>::Begin() {
	return Iterator(m_Root);
}

template<typename _Data, typename _CylinderType>
inline typename spdBdamTree<_Data, _CylinderType>::Iterator spdBdamTree<_Data, _CylinderType>::End() {
	return Iterator(NULL);
}


}

#endif
