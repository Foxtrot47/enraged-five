// 
// spatialdata/polygon.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "polygon.h"

#include "shaft.h"
#include "shaft_inline.h"

#include "atl/array_struct.h"
#include "file/token.h"
#include "grcore/im.h"
#include "mesh/mesh.h"
#include "system/memory.h"


#define		FAST_POLYGON		FAST_PLANE

using namespace rage;

atArray<Vector3> *spdPolygon::sm_List1 = NULL;
atArray<Vector3> *spdPolygon::sm_List2 = NULL;


IMPLEMENT_PLACE(spdPolygon);



bool spdPolygon::AddPoint(const Vector3 &p)
{
	int i;
	for (i = 0; i < GetNumPoints(); i++)
	{
		if (p.IsClose(GetPoint(i), 0.01f))
		{
			// We already have the point.
			return false;
		}
	}

	m_Points.PushAndGrow(p);
	m_VisibleEdges.PushAndGrow(false);

	// Not exactly necessary after every point,
	// but it's convenient not to have to think about it.
	ComputeCenter();
	ComputePlane();

	return true;
}

void spdPolygon::ComputeCenter(void)
{
	m_Center.Zero();
	for (int i = 0; i < GetNumPoints(); i++)
		m_Center += GetPoint(i);

	Assert(GetNumPoints() > 0);
	m_Center /= (float)GetNumPoints();
}

bool spdPolygon::ComputePlane(void)
{
	Vector3 norm;
	norm.Zero();

	if (GetNumPoints() >= 3)
	{
		// Newell's method.
		for (int i = 0; i < GetNumPoints(); i++)
		{
			Vector3 p1 = GetPoint(i);
			Vector3 p2 = (i == GetNumPoints() - 1) ? GetPoint(0) : GetPoint(i + 1);

			norm.x += (p1.y - p2.y) * (p1.z + p2.z);
			norm.y += (p1.z - p2.z) * (p1.x + p2.x);
			norm.z += (p1.x - p2.x) * (p1.y + p2.y);
		}

		norm.Normalize();
		m_Plane.Set(GetPoint(0), norm);
		return true;
	}
	else
	{
		m_Plane.Set(GetPoint(0), norm);
		return false;
	}
}

// So we can get a tolerance value that matches any trees being made.
extern float BSP_TOLERANCE;
namespace rage {
	float BSP_TOLERANCE = 0.20f;
}

#if MESH_LIBRARY
typedef void (* AddExporterError)(const char *error);
bool spdPolygon::FindPerimeter(const mshMaterial *pMtl, const mshPrimitive *pPrim, const Matrix34 &mat)
{
	Assert(pMtl);
	Assert(pPrim);
	switch(pPrim->Type)
	{
	case mshPOLYGON:
		{
			m_Points.Reserve(pPrim->Idx.GetCount());

			for (int i = 0; i < pPrim->Idx.GetCount(); i++)
			{
				Assert(pPrim->Idx[i] != mshUndefined);
				Vector3 v = pMtl->GetPos(pPrim->Idx[i]);
				mat.Transform(v);
				AddPoint(v);
			}
		}
		break;

#if 0
	case mshTRIANGLES:  // TODO: Maybe remove sometime.
		{
			// Currently used by portals and occluders in older exports (pre-September '03).

			// Make the edge table. We only need half of it really,
			// but it makes indexing easier if it's square.
			int nv = static_cast<int>(pPrim->Idx());  // mshArray::operator()
			//			sysMemStartTemp();
			polygonEdge *edgeTable = rage_new polygonEdge[nv * nv];
			//			sysMemEndTemp();

			Assert(pPrim->GetSubPrimitiveCount() == (nv / 3));  // The number of triangles.

			int i, j;
			int outsideEdges = 0;

			for (i = 0; i < pPrim->GetSubPrimitiveCount(); i++)
			{
				int m = i * 3;
				Assert(pPrim->Idx[m] != mshUndefined);
				Assert(pPrim->Idx[m + 1] != mshUndefined);
				Assert(pPrim->Idx[m + 2] != mshUndefined);

				// Get the adjunct indexes for this triangle.

				int index[3];
				index[0] = static_cast<int>(pPrim->Idx[m]);
				index[1] = static_cast<int>(pPrim->Idx[m + 1]);
				index[2] = static_cast<int>(pPrim->Idx[m + 2]);

				Assert(index[0] >= 0 && index[0] < nv);
				Assert(index[1] >= 0 && index[1] < nv);
				Assert(index[2] >= 0 && index[2] < nv);

				// Sort.

				int temp;
				if (index[0] > index[2])
				{
					temp = index[0];
					index[0] = index[2];
					index[2] = temp;
				}
				if (index[0] > index[1])
				{
					temp = index[0];
					index[0] = index[1];
					index[1] = temp;
				}
				if (index[1] > index[2])
				{
					temp = index[1];
					index[1] = index[2];
					index[2] = temp;
				}
				Assert(index[0] <= index[1] && index[1] <= index[2]);

				// Poly i is made up from indexes [0,1] [1,2] [0,2]

				outsideEdges += edgeTable[(index[0] * nv) + index[1]].AddPoly(i, index[0], index[1]);
				outsideEdges += edgeTable[(index[1] * nv) + index[2]].AddPoly(i, index[1], index[2]);
				outsideEdges += edgeTable[(index[0] * nv) + index[2]].AddPoly(i, index[0], index[2]);
			}

			// Make a list of the perimeter edges, O(n2).

			Assert(outsideEdges > 0);
			//			sysMemStartTemp();
			polygonEdge **pEdges = rage_new polygonEdge*[outsideEdges];
			//			sysMemEndTemp();

			int k = 0;
			for (i = 0; i < nv; i++)
			{
				for (j = 0; j < nv; j++)
				{
					polygonEdge *pEdge = &edgeTable[(i * nv) + j];
					if (pEdge->Perimeter())
						pEdges[k++] = pEdge;
				}
			}
			Assert(k == outsideEdges);

			// Walk the perimeter edges.
			// Make an effort to preserve the winding order.

			int t = 0;
			int edge = 0;

			m_Points.Reserve(outsideEdges);
			for (i = 0; i < outsideEdges; i++)
			{
				// Add the edge's p1 or p2, depending on t.
				Vector3 v = pMesh->GetPos( pMesh->GetAdj( pEdges[edge]->P[t] ).P );
				mat.Transform(v);
				AddPoint(v);

				t ^= 1;

				// Find the connecting edge.
				// TODO: Could try starting j at current edge.
				for (j = 0; j < outsideEdges; j++)
				{
					if (j != edge)  // Ignore current edge.
					{
						if (pEdges[edge]->P[t] == pEdges[j]->P[0])
						{
							t = 0;
							edge = j;
							break;
						}
						else
							if (pEdges[edge]->P[t] == pEdges[j]->P[1])
							{
								t = 1;
								edge = j;
								break;
							}
					}
				}

				Assert(j < outsideEdges);  // We didn't find the connecting edge?
			}

			//			sysMemStartTemp();
			delete[] pEdges;
			delete[] edgeTable;
			//			sysMemEndTemp();
		}
		break;

		// TODO: Maybe remove these sometime.
	case mshTRISTRIP:
	case mshTRISTRIP2:  // Maybe should negate normals in this case.
		{
			// Make the edge table. We only need half of it really,
			// but it makes indexing easier if it's square.
			int nv = static_cast<int>(pPrim->Idx());  // mshArray::operator()
			//			sysMemStartTemp();
			polygonEdge *edgeTable = rage_new polygonEdge[nv * nv];
			//			sysMemEndTemp();

			Assert(pPrim->GetSubPrimitiveCount() == (nv - 2));

			int i, j;
			int outsideEdges = 0;

			for (i = 0; i < pPrim->GetSubPrimitiveCount(); i++)
			{
				Assert(pPrim->Idx[i] != mshUndefined);
				Assert(pPrim->Idx[i + 1] != mshUndefined);
				Assert(pPrim->Idx[i + 2] != mshUndefined);

				// Get the adjunct indexes for this triangle.

				int index[3];
				index[0] = static_cast<int>(pPrim->Idx[i]);
				index[1] = static_cast<int>(pPrim->Idx[i + 1]);
				index[2] = static_cast<int>(pPrim->Idx[i + 2]);

				Assert(index[0] >= 0 && index[0] < nv);
				Assert(index[1] >= 0 && index[1] < nv);
				Assert(index[2] >= 0 && index[2] < nv);

				// Sort.

				int temp;
				if (index[0] > index[2])
				{
					temp = index[0];
					index[0] = index[2];
					index[2] = temp;
				}
				if (index[0] > index[1])
				{
					temp = index[0];
					index[0] = index[1];
					index[1] = temp;
				}
				if (index[1] > index[2])
				{
					temp = index[1];
					index[1] = index[2];
					index[2] = temp;
				}
				Assert(index[0] <= index[1] && index[1] <= index[2]);

				// Poly i is made up from indexes [0,1] [1,2] [0,2]

				outsideEdges += edgeTable[(index[0] * nv) + index[1]].AddPoly(i, index[0], index[1]);
				outsideEdges += edgeTable[(index[1] * nv) + index[2]].AddPoly(i, index[1], index[2]);
				outsideEdges += edgeTable[(index[0] * nv) + index[2]].AddPoly(i, index[0], index[2]);
			}

			// Make a list of the perimeter edges, O(n2).

			Assert(outsideEdges > 0);
			//			sysMemStartTemp();
			polygonEdge **pEdges = rage_new polygonEdge*[outsideEdges];
			//			sysMemEndTemp();

			int k = 0;
			for (i = 0; i < nv; i++)
			{
				for (j = 0; j < nv; j++)
				{
					polygonEdge *pEdge = &edgeTable[(i * nv) + j];
					if (pEdge->Perimeter())
						pEdges[k++] = pEdge;
				}
			}
			Assert(k == outsideEdges);

			// Walk the perimeter edges.
			// Make an effort to preserve the winding order.

			int t = 0;
			int edge = 0;

			m_Points.Reserve(outsideEdges);
			for (i = 0; i < outsideEdges; i++)
			{
				// Add the edge's p1 or p2, depending on t.
				Vector3 v = pMesh->GetPos( pMesh->GetAdj( pEdges[edge]->P[t] ).P );
				mat.Transform(v);
				AddPoint(v);

				t ^= 1;

				// Find the connecting edge.
				// TODO: Could try starting j at current edge.
				for (j = 0; j < outsideEdges; j++)
				{
					if (j != edge)  // Ignore current edge.
					{
						if (pEdges[edge]->P[t] == pEdges[j]->P[0])
						{
							t = 0;
							edge = j;
							break;
						}
						else
							if (pEdges[edge]->P[t] == pEdges[j]->P[1])
							{
								t = 1;
								edge = j;
								break;
							}
					}
				}

				Assert(j < outsideEdges);  // We didn't find the connecting edge?
			}

			//			sysMemStartTemp();
			delete[] pEdges;
			delete[] edgeTable;
			//			sysMemEndTemp();
		}
		break;
#endif
	default:
		Quitf("Can't find perimeter of mesh type %d", pPrim->Type);
	}

	Assert(GetNumPoints() >= 3);

	if (!IsFlat(BSP_TOLERANCE))
	{
		// TODO: Make useful. - ok, I'll try. bd...
		// const char	*bogus;
		//		AddExporterError	addExporterError;// = (AddExporterError)PARAM_spdPolygon_exportHelper.Get((char *)addExporterError);
		/* 		if(PARAM_spdPolygon_exportHelper.Get(bogus))
		{
		char	errorString[255];

		addExporterError = (AddExporterError)bogus;
		sprintf(errorString, "Found a twisted polygon.\n");
		addExporterError(errorString);
		} */
		Errorf("Found a twisted polygon.");
		Flatten();
		return false;
	}

	return true;
}
#endif

#if 1//!__TOOL
#if 0
spdPolygon::spdPolygon(rmwNode &node)
{
	//	NodeMesh = &node;

	m_Data = -1;
	m_RefNumber = -1;

	rmwNodeMesh *pNodeMesh = node.NodeMesh();
	Assert(pNodeMesh);

	m_Node.m_Polygon = this;

	//	sysMemStartTemp();
	mshMesh *pMesh = rmwNodeMesh::LoadMesh(pNodeMesh->GetMeshName());
	//	sysMemEndTemp();
	Assert(pMesh);

	// I suppose this should break out after one primitive is found.
	for (int i = 0; i < 1 /*pMesh->GetMtlCount()*/; i++)
	{
		mshMaterial *pMtl = &pMesh->GetMtl(i);

		for (int j = 0; j < 1 /*pMtl->Prim.GetCount()*/; j++)  // The number of primitives (not sub-primitives).
		{
			mshPrimitive *pPrim = &pMtl->Prim[j];
			FindPerimeter(pMtl, pPrim, pNodeMesh->GetMatrix());
			//			break;
		}
		//		break;
	}
	Assert(GetNumPoints() > 0 && GetNumPoints() < 32);

	//	sysMemStartTemp();
	delete pMesh;
	//	sysMemEndTemp();

	/*	
	// Please leave this here.
	for (int k = 0; k < pPrim->GetSubPrimitiveCount(); k++)  // The number of triangles.
	{
	Vector3 t[3];
	for (int m = 0; m < 3; m++)
	{
	mshIndex idx = pPrim->Idx[(k * 3) + m];  // The adjunct index.

	t[m] = pMesh->GetPos( pMesh->GetAdj(idx).P );

	pNodeMesh->GetMatrix().Transform(t[m]);

	AddPoint(t[m]);
	}
	}
	*/

	SetSingleSided(false);
}
#endif //0

/*
PURPOSE
Project a polygon onto the front plane of a shaft.
The polygon is clipped to the shaft's world-plane, and so it is
"guaranteed" to project safely.
PARAMS
shaft	- the shaft to project with.
pOut	- will be the array of output vertices.
RETURNS
The number of valid points in the output array.
NOTES
This function guards against other allocations that
may be caused by the generic clipper.
*/
int spdPolygon::ProjectShaft(const spdShaft &shaft, atRangeArray<Vector2, MAXPROJECTED> *pOut) const
{
	//	PF_START(PolygonProject);

	BANK_ONLY(shaft.PolygonsProjected++);

	int count = GetNumPoints();

	bool onePointIn = false;			// True if at least one point is in FRONT of the near clipping plane
	spdPlane plane;
	plane = shaft.GetViewportPlane();
#if FAST_POLYGON
	plane.AddDistanceV( ScalarVFromF32(-0.05f));
#else
	plane.AddDistance(-0.05f);
#endif
	Assert(count >= 0);
	for (int i = 0; i < count; i++)
	{
		Vector3 point = m_Points[i];
#if FAST_POLYGON
		ScalarV distToPlane = plane.DistanceToPlaneV( VECTOR3_TO_VEC3V(point));
		if( IsGreaterThanAll(distToPlane, ScalarV(V_ZERO)) != 0 )
		{
			point = VEC3V_TO_VECTOR3( Subtract( VECTOR3_TO_VEC3V(point), Scale( VECTOR3_TO_VEC3V(plane.GetNormal()), distToPlane) ) );
		}
		if( distToPlane.Getf() < shaft.GetViewport().GetNearClip())
#else
		float distToPlane = plane.DistanceToPlane(point);
		if (distToPlane > 0.0f)
		{
			point -= plane.GetNormal() * distToPlane;
		}
		if (distToPlane < shaft.GetViewport().GetNearClip())
#endif
		{
			onePointIn = true;
		}



		// We're not going to check the return value here - it's perfectly valid
		// for a polygon to have one or more vertices outside the frustum. /MAK
		shaft.ProjectInline(point, (*pOut)[i]);
	}

	if (!onePointIn)
	{
		return 0;
	}

	//	PF_STOP(PolygonProject);
	return count;
}
#endif  // !__TOOL


// Returns: 0=not on polygon, 1=on polygon, 2=on edge or corner.
int spdPolygon::ClassifyPoint(const Vector3 &v, float tol) const
{
	if (!GetPlane().IsPointOnPlane(v, tol))
		return false;

	Assert(GetNumPoints() > 2);

	Vector3 dir = GetPoint(0) - GetCenter();
	Assert(dir.Mag2() < square(1000.0f));
	dir.Normalize();

	int crosses = 0;

	Vector3 p1, p2;
	for (int i = 0; i < GetNumPoints(); i++)
	{
		GetEdge(i, p1, p2);

		// Incoming point near enough to a corner?
		if ((p1 - v).Mag2() < square(tol))
			return 2;

		// Incoming point near enough to an edge?
		if (geomDistances::DistanceLineToPoint(p1, p2 - p1, v) < tol)
			return 2;

		float t1, t2;  // Don't really want these.
		if (geomTValues::FindTValuesLineToLine(v, dir * 1000.0f, p1, p2 - p1, &t1, &t2))
			crosses++;
	}

	return crosses & 1;
}

bool spdPolygon::LineIntersects(const Vector3 &p1, const Vector3 &p2) const
{
	// TODO: Double-sided case.

	Assert(GetNumPoints() > 2);
	Vector3 v0 = GetPoint(0);
	for (int i = 0; i < GetNumPoints() - 2; i++)
	{
		Vector3 v1 = GetPoint(i + 1);
		Vector3 v2 = GetPoint(i + 2);
		float out;

		int res = geomSegments::SegmentTriangleIntersectDirected(p1, p2 - p1, v0, v1, v2, out);

		if (res == 0)
		{
#if __DEV
			if (out < -0.05f || out > 1.05f)
				Warningf("Strange ray/triangle hit, t=%f", out);
#endif
			return true;  // Hit.
		}
		else
			if (res == 1)
				return false;  // Backfacing.
	}
	return false;
}


// Returns: 0=no intersection, 1=intersection inside, 2=intersection on edge or corner, 3=coplanar intersection.
int spdPolygon::ClassifyLine(const Vector3 &p1, const Vector3 &p2, float *t, float tol) const
{
	// If either end point is inside this polygon, it's a touching case.
	if (ClassifyPoint(p1, tol) || ClassifyPoint(p2, tol))
		return 2;

	// If the line is coplanar, check for any crossings.
	if (GetPlane().IsPointOnPlane(p1, tol) && GetPlane().IsPointOnPlane(p2, tol))
	{
		Vector3 e1, e2;

		for (int i = 0; i < GetNumPoints(); i++)
		{
			GetEdge(i, e1, e2);

			// TODO: Tolerance is ignored here. But perhaps it doesn't matter,
			// as endpoints that are close to an edge will have been detected earlier.
			float t1, t2;  // Don't really want these.
			if (geomTValues::FindTValuesLineToLine(p1, p2 - p1, e1, e2 - e1, &t1, &t2))
			{
				// TODO: May not be nearest crossing.
				if (t)
					*t = t1;
				return 3;
			}
		}

		return 0;
	}

	Vector3 p;
	if (GetPlane().IntersectLine(p1, p2, p, NULL))
		return ClassifyPoint(p, tol);
	else
		return 0;
}

void spdPolygon::GetEdge(int i, Vector3 &p1, Vector3 &p2) const
{
	Assert(i >= 0 && i < GetNumPoints());
	p1 = GetPoint(i);
	if ((i + 1) == GetNumPoints())
		p2 = GetPoint(0);
	else
		p2 = GetPoint(i + 1);
}

bool spdPolygon::EdgesIntersect(const spdPolygon &poly, bool filterTouches, float tol) const
{
	//	Displayf("EdgesIntersect()");
	for (int i = 0; i < poly.GetNumPoints(); i++)
	{
		Vector3 p1, p2;
		poly.GetEdge(i, p1, p2);
		float t;

		int res = ClassifyLine(p1, p2, &t, tol);

		bool hit = res > 0;
		bool touch = res >= 2;

		//		Displayf("\t%f,%f,%f -> %f,%f,%f", p1.x, p1.y, p1.z, p2.x, p2.y, p2.z);
		//		Displayf("\tt = %f, touch = %d, hit = %d", t, touch, hit);

		if (filterTouches && !touch && hit)
			return true;

		if (!filterTouches && (touch || hit))
			return true;
	}
	return false;
}

/*
PURPOSE
Test every point in the polygon against a plane.
PARAMS
plane	- the plane to test against.
neg		- output number of points on the negative side of the plane.
pos		- output number of points on the positive side of the plane.
cop		- output number of points that lie on or near the plane.
tol		- point-on-plane tolerance.
RETURNS
'true' if the plane cuts the polygon.
*/
bool spdPolygon::Classify(const spdPlane &plane, int &neg, int &pos, int &cop, float tol) const
{
	neg = pos = cop = 0;

	for (int i = 0; i < GetNumPoints(); i++)
	{
		float d = plane.DistanceToPlane(GetPoint(i));
		//		Displayf("i=%d, d=%f", i, d);

		if (fabsf(d) <= tol)
			++cop;
		else
			if (d < 0.0f)
				++neg;
			else
				++pos;
	}

	Assert(neg + pos + cop == GetNumPoints());
	return (neg > 0 && pos > 0);
}

/*
PURPOSE
Test if the polygon lies in the same plane as the test plane.
PARAMS
plane	- the plane to test against.
tol		- point-on-plane tolerance.
RETURNS
'true' if the polygon is coplanar with the test plane.
*/
bool spdPolygon::IsCoplanar(const spdPlane &plane, float tol) const
{
	for (int i = 0; i < GetNumPoints(); i++)
	{
		if (fabsf(plane.DistanceToPlane(GetPoint(i))) > tol)
			return false;
	}

	return true;
}

bool spdPolygon::IsLevel(void) const
{
	return fabsf(GetNormal().y) > 0.95f;
}

/*
PURPOSE
Test if the polygon lies in the same plane as the test plane,
and that its normal points into the same half-space.
PARAMS
plane	- the plane to test against.
tol		- point-on-plane tolerance.
RETURNS
'true' if the polygon is coplanar with the test plane.
*/
bool spdPolygon::IsCoplanarWithSimilarNormal(const spdPlane &plane, float tol) const
{
	if (GetNormal().Dot( plane.GetNormal() ) <= 0.0f)
		return false;

	return (IsCoplanar(plane, tol));
}

bool spdPolygon::IsCoincident(const spdPolygon &poly) const
{
	if (GetNumPoints() != poly.GetNumPoints())
		return false;

	if (!GetNormal().IsClose( poly.GetNormal(), 0.01f ))
		return false;

	if (!GetCenter().IsClose( poly.GetCenter(), 0.01f ))
		return false;

	// Detect vertex coincidence, however ordered.
	sysMemStartTemp();
	int *count = rage_new int[GetNumPoints()];
	int matches = 0;

	int i, j;
	for (i = 0; i < GetNumPoints(); i++)
	{
		count[i] = 0;
		for (j = 0; j < poly.GetNumPoints(); j++)
		{
			if (GetPoint(i).IsClose( poly.GetPoint(j), 0.01f))
			{
				count[i]++;
				matches++;
			}
		}
	}

	bool res = false;
	if (matches == GetNumPoints())
	{
		res = true;
		for (i = 0; i < GetNumPoints() && res; i++)
			if (count[i] != 1)
				res = false;
	}
	delete[] count;
	sysMemEndTemp();

	return res;
}

// Test polyhedral convexity between this poly and another.
bool spdPolygon::IsConvex(const spdPolygon &poly, float tol) const
{
	spdPlane plane = GetPlane();
	for (int i = 0; i < poly.GetNumPoints(); i++)
	{
		if (plane.DistanceToPlane(poly.GetPoint(i)) > tol)
			return false;
	}
	return true;
}


bool spdPolygon::IsFlat(float tol) const
{
	spdPlane plane = GetPlane();
	for (int i = 0; i < GetNumPoints(); i++)
	{
		if (fabsf(plane.DistanceToPlane(GetPoint(i))) > tol)
			return false;
	}
	return true;
}

void spdPolygon::Flatten(void)
{
	spdPlane plane = GetPlane();
	for (int i = 0; i < GetNumPoints(); i++)
	{
		float d = plane.DistanceToPlane(GetPoint(i));
		SetPoint(i, GetPoint(i) - (plane.GetNormal() * d));
	}
}

void spdPolygon::Project(const spdPlane &plane)
{
	for (int i = 0; i < GetNumPoints(); i++)
	{
		float d = plane.DistanceToPlane(GetPoint(i));
		SetPoint(i, GetPoint(i) - (plane.GetNormal() * d));
	}
#if __DEV
	//	if (!GetPlane().IsSimilar(plane))
	//		Errorf("spdPolygon::Project() didn't work as expected");
#endif
	m_Plane = plane;  // Shouldn't really be necessary.
}

/*
PURPOSE
Cut a polygon against a plane giving (up to) two other polygons.
PARAMS
plane	- the plane to cut with.
poly1	- output polygon on the negative side of the plane.
poly2	- output polygon on the positive side of the plane.
NOTES
This function allocates space for the output polygon vertices.
*/
void spdPolygon::Cut(const spdPlane &plane, spdPolygon &poly1, spdPolygon &poly2) const
{
	spdPlane p(plane);

	// TODO: Could perform clipping before operator=
	// (and therefore before allocating). For now we're only
	// using this function for BSP building so we don't care.
	poly1 = *this;
	poly2 = *this;

	// poly1 is the -ve poly.
	// poly2 is the +ve poly.

	poly1.Clip(p);
	p.NegateNormal();
	poly2.Clip(p);
}

/*
PURPOSE
Clip this polygon against a plane.
PARAMS
plane	- the plane to clip against.
RETURNS
The number of points remaining in the polygon.
NOTES
This function will assert out if reallocation occurs.
You can guard against that by preallocating m_Points in some way.
*/
int spdPolygon::Clip(const spdPlane &plane)
{
	int count = plane.Clip(&m_Points, sm_List1);
	Assert(sm_List1->GetCapacity() == MAX_POLYVERTS);

#if __DEV
	//	int alloc = m_Points.GetCapacity();
#endif
	m_Points.Reset();
	m_Points.Resize(count);

	// TODO: We want this Assert compiled in. It is commented out for now
	// because the bsp building may cause it to fire. The building will
	// be part of the export process soon. -KPM
	//	Assert(alloc == m_Points.GetCapacity());

	// Not cheap or pretty.
	m_Points = *sm_List1;

	//	for (int i = 0; i < GetNumPoints(); i++)
	//		Displayf("%08x spdPolygon::Clip(): %f, %f, %f", this, GetPoint(i).x, GetPoint(i).y, GetPoint(i).z);

	ComputeCenter();

	Assert(m_Points.GetCount() == count);

	return count;
}


// TODO: Could be prestored.
spdPlane spdPolygon::ComputeEdgePlane(int edge) const
{
	Assert(edge >= 0 && edge < GetNumPoints());

	Vector3 p1 = GetPoint(edge);
	Vector3 p2 = GetPoint( (edge + 1 == GetNumPoints()) ? 0 : edge + 1);

	Vector3 n = p2 - p1;
	n.Cross( GetNormal() );
	n.Normalize();
	Assert(n.Mag() > 0.95f && n.Mag() < 1.05f);

	spdPlane plane(GetPoint(edge), n);
	return plane;
}

spdPlane spdPolygon::ComputeEdgePlane(const Vector3 &pos, int edge) const
{
	Assert(edge >= 0 && edge < GetNumPoints());

	Vector3 p1 = GetPoint(edge);
	Vector3 p2 = GetPoint( (edge + 1 == GetNumPoints()) ? 0 : edge + 1);

	p1 -= pos;
	p2 -= pos;

	Vector3 n;
	n.Cross( p1, p2 );
	n.Normalize();
	Assert(n.Mag() > 0.95f && n.Mag() < 1.05f);

	spdPlane plane(GetPoint(edge), n);
	return plane;
}



void spdPolygon::SaveData(fiTokenizer &tok) const
{
	tok.PutStr("POLYGON\n");
	tok.PutStr("\tPointCount %d\n", GetNumPoints());
	int i;
	for (i = 0; i < GetNumPoints(); i++)
	{
		tok.Put("\t");
		tok.Put(GetPoint(i));
		tok.PutStr("\n");
	}
	tok.PutStr("\tPlane ");
	tok.Put(GetPlane().GetCoefficientVector());
	tok.PutStr("\n");

	tok.PutStr("\tCenter ");
	tok.Put(GetCenter());
	tok.PutStr("\n");

	tok.PutStr("\tSingleSided %d", GetSingleSided());
	tok.PutStr("\n");

	tok.PutStr("\tData %d", GetData());
	tok.PutStr("\n");
}

void spdPolygon::LoadData(fiTokenizer &tok)
{
	tok.MatchToken("POLYGON");
	int count = tok.MatchInt("PointCount");
	m_Points.Resize(count);
	m_VisibleEdges.Resize(count);

	int i;
	for (i = 0; i < count; i++)
		tok.GetVector(m_Points[i]);

	Vector4 planeVec;
	tok.MatchVector("Plane", planeVec);
	m_Plane.SetCoefficientVector(planeVec);
	tok.MatchVector("Center", m_Center);

	tok.MatchToken("SingleSided");
	m_SingleSided = tok.GetInt() > 0 ? true : false;

	m_Data = tok.MatchInt("Data");
	//	Assert(m_Data >= 0);  // Probably over-cautious.
}

#if __DEV && !__SPU
void spdPolygon::Print(const char *str) const
{
	if (str)
		Displayf(str);

	Displayf("ref=%d", GetRefNumber());
	Vector4 plane = GetPlane().GetCoefficientVector();
	Displayf("plane=%f, %f, %f, w=%f", plane.x, plane.y, plane.z, plane.w);

	int i;
	for (i = 0; i < GetNumPoints(); i++)
		Displayf("point %d=%f, %f, %f", i, GetPoint(i).x, GetPoint(i).y, GetPoint(i).z);
}
#endif

spdPolyNode::spdPolyNode(datResource &rsc)
	: m_PolyNode(rsc)
{
	m_PolyNode.Data = this;
	m_Dist2 = 0.0f;
	ResetNode();
}

#if __DECLARESTRUCT

	void spdPolyNode::DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(spdPolyNode);
		STRUCT_FIELD(m_PolyNode);
		STRUCT_FIELD_VP(m_Polygon);
		STRUCT_FIELD(m_Dist2);
		STRUCT_FIELD(m_Pad1);
		STRUCT_FIELD(m_Pad2);
		STRUCT_FIELD(m_Pad3);
		STRUCT_END();
	}

#endif // __DECLARESTRUCT


spdPolygon::spdPolygon(datResource &rsc)
	: m_Node(rsc)
	, m_Plane(rsc)
	, m_Center(rsc)
	, m_Points(rsc)
	, m_VisibleEdges(rsc)
{

	m_Node.m_Polygon = this;
}

#if __DEV && !__TOOL
void spdPolygon::Draw(bool solid, float r, float g, float b, float a) const
{
	grcDrawPolygon(m_Points, &GetNormal(), solid, Color32(r,g,b,a));

}
#endif

#if __DECLARESTRUCT

	void spdPolygon::DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(spdPolygon);
		STRUCT_FIELD(m_EdgeVisFrame);
		STRUCT_FIELD(m_Data);
		STRUCT_FIELD(m_RefNumber);
        STRUCT_FIELD(m_Node);
		STRUCT_FIELD(m_Plane);
		STRUCT_FIELD(m_Center);
		STRUCT_FIELD(m_Points);
		STRUCT_FIELD(m_VisibleEdges);
		STRUCT_FIELD(m_SingleSided);
		STRUCT_FIELD(m_Pad1);
		STRUCT_FIELD(m_Pad2);
		STRUCT_FIELD(m_Pad3);
		STRUCT_FIELD(m_Pad4);
		STRUCT_FIELD(m_Pad5);
		STRUCT_FIELD(m_Pad6);
		STRUCT_END();
	}

#endif // __DECLARESTRUCT


//rs do we need this?
#if 0
int polygonEdge::AddPoly(int i, int p1, int p2)
{
	Assert(i != PolyA && i != PolyB && "Already added this poly");

	if (PolyA == -1)
	{
		PolyA = i;
		P[0] = p1;
		P[1] = p2;
		return 1;
	}
	else
		if (PolyB == -1)
		{
			Assert(P[0] == p1 && P[1] == p2);  // Should be the same edge.
			PolyB = i;
			return -1;
		}

		Assert(!"Edge connects >2 triangles");
		return 0;
}


bool polygonEdge::Perimeter(void)
{
	// Not an edge?
	if (PolyA == -1 && PolyB == -1)
		return false;

	// One side connected?
	if (PolyA == -1 || PolyB == -1)
		return true;

	// Interior edge.
	return false;
}
#endif
