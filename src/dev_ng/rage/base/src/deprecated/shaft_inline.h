// 
// spatialdata/shaft_inline.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_SHAFT_INLINE_H 
#define SPATIALDATA_SHAFT_INLINE_H 

#ifndef SPATIALDATA_SHAFT_H
#error "Must be included after shaft.h"
#endif


namespace rage {


inline grcCullStatus spdApical::ProjectInline(Vector3::Vector3Param point, Vector2 &out) const
{
	//rs	PF_START(ProjectPoint);

	// Taken from vglProject
	Vector4		 inPt(point);
	inPt.And(VEC4_ANDW);
	inPt.Or(VEC4_ONEW);
	Vector4      outPt;

	ProjN.Transform(inPt, outPt);

	grcCullStatus res = cullOutside;	// TODO: Should be "behind"

	if (outPt.w > 0.0f)
	{
		const float invW = 1.0f / outPt.w;
		const spdViewport &vp = GetViewport();
		float hw = vp.GetWidth() * 0.5f;
		float hh = vp.GetHeight() * 0.5f;
		out.x  = vp.GetX1() + (hw + hw * (outPt.x * invW ));
		out.y  = vp.GetY1() + (hh - hh * (outPt.y * invW ));
		res = (	out.x >= vp.GetX1() &&
				out.x < vp.GetX2() &&
				out.y >= vp.GetY1() &&
				out.y < vp.GetY2()) ? cullInside : cullOutside;
	}

	//rs	PF_STOP(ProjectPoint);

	return res;
}

inline grcCullStatus spdShaft::IsVisibleInline_ShaftAABB(const spdAABB *RESTRICT aabb) const
{
	Vec3V centerV = VECTOR3_TO_VEC3V( aabb->GetCenter() );
	Vec3V halfExtents = Subtract( VECTOR3_TO_VEC3V(aabb->GetMax()), centerV );
	ScalarV inscribedRadiusV = SplatW( VECTOR4_TO_VEC4V( aabb->GetInscribedRadiusV() ) );
	return IsVisibleInline( centerV, halfExtents, inscribedRadiusV );
}

#if __SPU
inline 
#else
__forceinline
#endif 
grcCullStatus spdShaft::IsOutsideInline_ShaftAABB(const spdAABB *RESTRICT aabb) const
{
	const Vec3V centerV = VECTOR3_TO_VEC3V( aabb->GetCenter() );
	const Vec3V maxV = VECTOR3_TO_VEC3V(aabb->GetMax());
	//Vec3V halfExtents = Subtract( VECTOR3_TO_VEC3V(aabb->GetMax()), centerV );
	const Vec4V InscribedRadiusV = VECTOR4_TO_VEC4V( aabb->GetInscribedRadiusV()  );
	const Vec3V halfExtents = Subtract(maxV , centerV);
	const ScalarV inscribedRadiusW = SplatW( InscribedRadiusV );
	//ScalarV inscribedRadiusV = SplatW( VECTOR4_TO_VEC4V( aabb->GetInscribedRadiusV() ) );
	return IsOutside( centerV, halfExtents, inscribedRadiusW );
}

inline grcCullStatus spdShaft::IsVisibleInline( Vec3V_In centerV, Vec3V_In halfExtentsIn, ScalarV_In inscribedRadiusV ) const
{
	ScalarV distancesV_0, distancesV_1, distancesV_2, distancesV_3, distancesV_4, distancesV_5;
	ScalarV halfExtent_0, halfExtent_1, halfExtent_2, halfExtent_3, halfExtent_4, halfExtent_5;
	BoolV outsideCheck_0, outsideCheck_1, outsideCheck_2, outsideCheck_3, outsideCheck_4, outsideCheck_5;
	BoolV clippedCheck_0, clippedCheck_1, clippedCheck_2, clippedCheck_3, clippedCheck_4, clippedCheck_5;

	const Vec3V halfExtents( And( halfExtentsIn, Vec3V(V_MASKXYZ) ) );

	const spdPlane* RESTRICT wPlanes = WorldPlanes;

	distancesV_0 = wPlanes[0].DistanceToPlaneV(centerV);
	distancesV_1 = wPlanes[1].DistanceToPlaneV(centerV);
	distancesV_2 = wPlanes[2].DistanceToPlaneV(centerV);
	distancesV_3 = wPlanes[3].DistanceToPlaneV(centerV);
	distancesV_4 = wPlanes[4].DistanceToPlaneV(centerV);
	distancesV_5 = wPlanes[5].DistanceToPlaneV(centerV);

	const Vector3* RESTRICT apNormals = AbsPlaneNormals;

	halfExtent_0 = Dot( RCC_VEC3V(apNormals[0]), halfExtents );
	halfExtent_1 = Dot( RCC_VEC3V(apNormals[1]), halfExtents );
	halfExtent_2 = Dot( RCC_VEC3V(apNormals[2]), halfExtents );
	halfExtent_3 = Dot( RCC_VEC3V(apNormals[3]), halfExtents );
	halfExtent_4 = Dot( RCC_VEC3V(apNormals[4]), halfExtents );
	halfExtent_5 = Dot( RCC_VEC3V(apNormals[5]), halfExtents );

	halfExtent_0 = Max( halfExtent_0, inscribedRadiusV );
	halfExtent_1 = Max( halfExtent_1, inscribedRadiusV );
	halfExtent_2 = Max( halfExtent_2, inscribedRadiusV );
	halfExtent_3 = Max( halfExtent_3, inscribedRadiusV );
	halfExtent_4 = Max( halfExtent_4, inscribedRadiusV );
	halfExtent_5 = Max( halfExtent_5, inscribedRadiusV );

	outsideCheck_0 = IsGreaterThan( distancesV_0, halfExtent_0 );
	outsideCheck_1 = IsGreaterThan( distancesV_1, halfExtent_1 );
	outsideCheck_2 = IsGreaterThan( distancesV_2, halfExtent_2 );
	outsideCheck_3 = IsGreaterThan( distancesV_3, halfExtent_3 );
	outsideCheck_4 = IsGreaterThan( distancesV_4, halfExtent_4 );
	outsideCheck_5 = IsGreaterThan( distancesV_5, halfExtent_5 );

	// Negation.
	ScalarV _80000000(V_80000000);
	halfExtent_0 ^= _80000000;
	halfExtent_1 ^= _80000000;
	halfExtent_2 ^= _80000000;
	halfExtent_3 ^= _80000000;
	halfExtent_4 ^= _80000000;
	halfExtent_5 ^= _80000000;

	outsideCheck_0 |= outsideCheck_1;
	outsideCheck_2 |= outsideCheck_3;
	outsideCheck_4 |= outsideCheck_5;
	outsideCheck_0 |= outsideCheck_2;
	outsideCheck_0 |= outsideCheck_4;

	clippedCheck_0 = IsLessThan( halfExtent_0, distancesV_0 );
	clippedCheck_1 = IsLessThan( halfExtent_1, distancesV_1 );
	clippedCheck_2 = IsLessThan( halfExtent_2, distancesV_2 );
	clippedCheck_3 = IsLessThan( halfExtent_3, distancesV_3 );
	clippedCheck_4 = IsLessThan( halfExtent_4, distancesV_4 );
	clippedCheck_5 = IsLessThan( halfExtent_5, distancesV_5 );

	clippedCheck_0 |= clippedCheck_1;
	clippedCheck_2 |= clippedCheck_3;
	clippedCheck_4 |= clippedCheck_5;
	clippedCheck_0 |= clippedCheck_2;
	clippedCheck_0 |= clippedCheck_4;

#if 1

	u32 rOut = IsEqualIntAll( outsideCheck_0, BoolV(V_FALSE));		// 0 = pass - outside, 1 fail
	u32 rClip = IsEqualIntAll( clippedCheck_0, BoolV(V_FALSE));	// 0 = pass - clip, 1 fail

	return (grcCullStatus)(rOut + (rClip&&rOut));
#else
	if ( !IsEqualIntAll( outsideCheck_0, BoolV(V_FALSE)) )
	{
		return cullOutside;
	}
	else if ( !IsEqualIntAll( clippedCheck_0, BoolV(V_FALSE)) )
	{
		return cullClipped;
	}

	return cullInside;
#endif
}


#if __SPU
inline 
#else
__forceinline
#endif
grcCullStatus spdShaft::IsOutside( Vec3V_In centerV, Vec3V_In halfExtentsIn, ScalarV_In inscribedRadiusV ) const
{
	ScalarV distancesV_0, distancesV_1, distancesV_2, distancesV_3, distancesV_4, distancesV_5;
	ScalarV halfExtent_0, halfExtent_1, halfExtent_2, halfExtent_3, halfExtent_4, halfExtent_5;
	BoolV outsideCheck_0, outsideCheck_1, outsideCheck_2, outsideCheck_3, outsideCheck_4, outsideCheck_5;

	const Vec4V maskXYZ(V_MASKXYZ);

	const Vec3V halfExtents( And( halfExtentsIn, maskXYZ.GetXYZ() ) );

	const spdPlane* RESTRICT wPlanes = WorldPlanes;

	distancesV_0 = wPlanes[0].DistanceToPlaneV(centerV);
	distancesV_1 = wPlanes[1].DistanceToPlaneV(centerV);
	distancesV_2 = wPlanes[2].DistanceToPlaneV(centerV);
	distancesV_3 = wPlanes[3].DistanceToPlaneV(centerV);
	distancesV_4 = wPlanes[4].DistanceToPlaneV(centerV);
	distancesV_5 = wPlanes[5].DistanceToPlaneV(centerV);

	const Vector3* RESTRICT apNormals = AbsPlaneNormals;

	halfExtent_0 = Dot( RCC_VEC3V(apNormals[0]), halfExtents );
	halfExtent_1 = Dot( RCC_VEC3V(apNormals[1]), halfExtents );
	halfExtent_2 = Dot( RCC_VEC3V(apNormals[2]), halfExtents );
	halfExtent_3 = Dot( RCC_VEC3V(apNormals[3]), halfExtents );
	halfExtent_4 = Dot( RCC_VEC3V(apNormals[4]), halfExtents );
	halfExtent_5 = Dot( RCC_VEC3V(apNormals[5]), halfExtents );

	halfExtent_0 = Max( halfExtent_0, inscribedRadiusV );
	halfExtent_1 = Max( halfExtent_1, inscribedRadiusV );
	halfExtent_2 = Max( halfExtent_2, inscribedRadiusV );
	halfExtent_3 = Max( halfExtent_3, inscribedRadiusV );
	halfExtent_4 = Max( halfExtent_4, inscribedRadiusV );
	halfExtent_5 = Max( halfExtent_5, inscribedRadiusV );

	outsideCheck_0 = IsGreaterThan( distancesV_0, halfExtent_0 );
	outsideCheck_1 = IsGreaterThan( distancesV_1, halfExtent_1 );
	outsideCheck_2 = IsGreaterThan( distancesV_2, halfExtent_2 );
	outsideCheck_3 = IsGreaterThan( distancesV_3, halfExtent_3 );
	outsideCheck_4 = IsGreaterThan( distancesV_4, halfExtent_4 );
	outsideCheck_5 = IsGreaterThan( distancesV_5, halfExtent_5 );

	// Negation.
	ScalarV _80000000(V_80000000);
	halfExtent_0 ^= _80000000;
	halfExtent_1 ^= _80000000;
	halfExtent_2 ^= _80000000;
	halfExtent_3 ^= _80000000;
	halfExtent_4 ^= _80000000;
	halfExtent_5 ^= _80000000;

	outsideCheck_0 |= outsideCheck_1;
	outsideCheck_2 |= outsideCheck_3;
	outsideCheck_4 |= outsideCheck_5;
	outsideCheck_0 |= outsideCheck_2;
	outsideCheck_0 |= outsideCheck_4;

#if 1

	u32 rOut = IsEqualIntAll( outsideCheck_0, BoolV(V_FALSE));		// 0 = pass - outside, 1 fail
	return (grcCullStatus)rOut; // will return Outside or Clip/Inside

#else
	if ( !IsEqualIntAll( outsideCheck_0, BoolV(V_FALSE)) )
		return cullOutside;

	return cullClipped;  // it has to return 1
#endif
}




} // namespace rage

#endif // SPATIALDATA_SHAFT_INLINE_H 
