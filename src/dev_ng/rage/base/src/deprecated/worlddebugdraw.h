// 
// spatialdata/worlddebugdraw.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_WORLDDEBUGDRAW_H 
#define SPATIALDATA_WORLDDEBUGDRAW_H 

#include "aabb.h"

#include "vector/color32.h"


namespace rage {

class bkBank;
class spdPolygon;
class spdSphere;
class spdShaft;

/* PURPOSE:
 *  This class can be used to create a 2D visualization of the game world
 *  (or any subset of it).
 *
 *  To use it, instantiate an object, set the 3D region that is supposed to
 *  be visualized using SetWorldExtents(), then set the 2D area on the screen
 *  on which this area will be projected.
 *
 *  Also, it might be a good idea to call AddWidgets() to add allow
 *  users some customizations, including scrolling and zooming.
 *
 *  During every frame, call BeginDraw() at some point while you are rendering,
 *  then draw all the elements to be visualized using the
 *  Draw() functions. Finally, call EndDraw() when you are done.
 *
 *  Make sure that all those calls calls are embedded within
 *  PUSH_DEFAULT_SCREEN()/POP_DEFAULT_SCREEN() pairs - also don't forget
 *  to call grcWorldMtx(M34_IDENTITY) if you modify the matrix (although
 *  BeginDraw() does that for you).
 *
 *  Multiple spdWorldDebugDraw objects can co-exist without problems.
 */
class spdWorldDebugDraw
{
public:
	spdWorldDebugDraw();

	spdWorldDebugDraw(float left, float top, float right, float bottom);

	// PURPOSE: Clear the background of the object. Done automatically by BeginDraw.
	void Clear() const;

	// PURPOSE: Call this before doing any draw calls.
	void BeginDraw();

	// PURPOSE: Call this after doing all draw calls.
	void EndDraw();

	// PURPOSE: Set the dimensions of this object on the screen, in screen coordinates.
	void SetScreenDimensions(float left, float top, float right, float bottom);

	// PURPOSE: Get the dimensions of this object on the screen, in screen coordinates.
	void GetScreenDimensions(float &outLeft, float &outTop, float &outRight, float &outBottom);

	// PURPOSE: Set the area in 3D space that this object will visualize.
	// (Currently, Y is ignored). This will also call UpdateBoundingBox() to apply
	// any user-defined zooming or offset.
	void SetWorldExtents(const spdAABBFast &aabb);
	void SetWorldExtents(Vector3::Vector3Param minPos, Vector3::Vector3Param maxPos);

	// PURPOSE: Draw an AABB in the 2D visualization.
	void Draw(const spdAABBFast &aabb, const Color32 &color, bool highlightIfSelected = false, const char *objName = NULL) const;

	// PURPOSE: Draw a sphere in the 2D visualization.
	void Draw(const spdSphere &aabb, const Color32 &color, bool highlightIfSelected = false, const char *objName = NULL) const;

	// PURPOSE: Draw a shaft in the 2D visualization.
	void Draw(const spdShaft &aabb, const Color32 &color, bool highlightIfSelected = false, const char *objName = NULL) const;

	// PURPOSE: Draw a polygon in the 2D visualization.
	void Draw(const spdPolygon &polygon, const Color32 &color, bool highlightIfSelected = false, const char *objName = NULL) const;

	// RETURNS: TRUE if the cursor is currently above this object.
	bool IsSelected(const spdAABBFast &aabb) const;

	// RETURNS: TRUE if the cursor is currently above this object.
	bool IsSelected(const spdSphere &sphere) const;

	// PURPOSE: Recalculate the actual bounding box from the original one.
	// This function should be called every frame to make sure user-defined modifiers
	// like zoom or offset are applied.
	void UpdateBoundingBox();

	// RETURNS: TRUE if this object matches the user-defined filter.
	bool DoesMatchFilter(const char *name) const;

#if __BANK
	void AddWidgets(bkBank &bank);
#endif // __BANK

protected:
	// Convert a point in 3D space to a 2D coordinate on the screen.
	void ConvertTo2D(Vector3::Vector3Param position, Vector3 &outVector) const;

	// Convert a length in 3D space to a 2D distance on the screen.
	void ConvertTo2D(float distance, float &outDistance) const;

	// Convert a point in 2D space (x, y) to a 3D coordinate.
	void ConvertTo3D(Vector3::Vector3Param position, Vector3 &outVector) const;

	// Create a version of this color that's used for highlighting solid objects.
	static Color32 CreateHighlightColor(const Color32 &color);

	// Common initialization of this object.
	void Init();

private:
#if __BANK
	// The area in world space that this object covers - the final area, after all modifiers.
	spdAABBFast	m_BoundingBox;

	// The area in world space that this object covers - the original area, as defined by the user.
	spdAABBFast m_OriginalBoundingBox;

	// Background color of this debug draw object, including alpha.
	Color32 m_BackgroundColor;

	// The extents of the debug draw area in 2D screen space.
	float	m_Left, m_Top, m_Right, m_Bottom;

	// The zoom factor.
	float	m_Zoom;

	// X and Z offset for the bounding box.
	float	m_OffsetX, m_OffsetZ;

	// Border around the visualization area at which objects are no longer treated
	// as being selectable.
	float	m_SelectionBorder;

	// Prevent the application from updating the bounding box.
	bool	m_DisableBoundingBoxUpdates;

	// Show the top plane of shafts only.
	bool	m_ShaftTopPlaneOnly;

	// Show only the items whose name matches this filter
	char	m_SubstringFilter[64];
	
	// User-friendly string showing current coordinates in RAG
	char	m_CoordinateString[32];

#endif // __BANK
};


//
// Stubs for non-__BANK builds to make sure everything compiles out.
//
#if !__BANK

__forceinline spdWorldDebugDraw::spdWorldDebugDraw() {}
__forceinline spdWorldDebugDraw::spdWorldDebugDraw(float /*left*/, float /*top*/, float /*right*/, float /*bottom*/) {};
__forceinline void spdWorldDebugDraw::Clear() const {};
__forceinline void spdWorldDebugDraw::SetScreenDimensions(float /*left*/, float /*top*/, float /*right*/, float /*bottom*/) {};
__forceinline void spdWorldDebugDraw::SetWorldExtents(const spdAABBFast &/*aabb*/) {};
__forceinline void spdWorldDebugDraw::SetWorldExtents(Vector3::Vector3Param /*minPos*/, Vector3::Vector3Param /*maxPos*/) {};
__forceinline void spdWorldDebugDraw::Draw(const spdAABBFast &/*aabb*/, const Color32 &/*color*/, bool /*highlightIfSelected*/, const char * /*objName*/) const {};
__forceinline void spdWorldDebugDraw::Draw(const spdSphere &/*aabb*/, const Color32 &/*color*/, bool /*highlightIfSelected*/, const char * /*objName*/) const {};
__forceinline void spdWorldDebugDraw::Draw(const spdShaft &/*aabb*/, const Color32 &/*color*/, bool /*highlightIfSelected*/, const char * /*objName*/) const {};
__forceinline void spdWorldDebugDraw::Draw(const spdPolygon &/*polygon*/, const Color32 &/*color*/, bool /*highlightIfSelected*/, const char * /*objName*/) const {};
__forceinline void spdWorldDebugDraw::UpdateBoundingBox() {};
__forceinline void spdWorldDebugDraw::ConvertTo2D(Vector3::Vector3Param /*position*/, Vector3 &/*outVector*/) const {};
__forceinline void spdWorldDebugDraw::ConvertTo2D(float /*distance*/, float &/*outDistance*/) const {};
__forceinline void spdWorldDebugDraw::ConvertTo3D(Vector3::Vector3Param /*position*/, Vector3 &/*outVector*/) const {};
__forceinline void spdWorldDebugDraw::Init() {};
__forceinline void spdWorldDebugDraw::BeginDraw() {};
__forceinline void spdWorldDebugDraw::EndDraw() {};

#endif // !__BANK


} // namespace rage

#endif // SPATIALDATA_WORLDDEBUGDRAW_H 
