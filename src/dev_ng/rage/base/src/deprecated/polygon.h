// 
// spatialdata/polygon.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SPATIALDATA_POLYGON_H
#define SPATIALDATA_POLYGON_H

#include "plane.h"
#include "shaft.h"

#include "atl/array.h"
#include "atl/dlist.h"
#include "data/base.h"
#include "vector/matrix34.h"


namespace rage {

class datTypeStruct;
class fiTokenizer;
struct mshMaterial;
struct mshPrimitive;
class spdShaft;

// PURPOSE: Used to build a list of spdPolygons
struct spdPolyNode
{
	spdPolyNode(class datResource &);

	atDNode<spdPolyNode*> m_PolyNode;
	class spdPolygon *m_Polygon;
	float m_Dist2;

	int m_Pad1, m_Pad2, m_Pad3;

	void ResetNode(void)
	{
		m_PolyNode.SetNext(NULL);
		m_PolyNode.SetPrev(NULL);
	}

	spdPolyNode()
	{
		m_PolyNode.Data = this;
		m_Polygon = NULL;
		m_Dist2 = 0.0f;
		ResetNode();
	}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

};

// PURPOSE: a convex, planar polygon. Convexity and planarity are not strictly enforced, but are assumed.
class spdPolygon : public datBase
{
public:
	enum { MAX_POLYVERTS=64 };


	DECLARE_PLACE(spdPolygon);


	static void InitClass(void)
	{
		FastAssert(!sm_List1 && !sm_List2);
		sm_List1 = rage_new atArray<Vector3>(0,MAX_POLYVERTS);
		sm_List2 = rage_new atArray<Vector3>(0,MAX_POLYVERTS);
	}

	static void ShutdownClass(void)
	{
		FastAssert(sm_List1 && sm_List2);
		delete sm_List2;
		delete sm_List1;
		sm_List1 = sm_List2 = NULL;
	}

	spdPolygon(datResource &rsc);

	spdPolygon()
	{
		m_Plane.Invalidate();
		m_Center.Zero();
		m_Data = -1;
		m_RefNumber = -1;
		m_SingleSided = false;
		m_EdgeVisFrame = -1;
		m_Node.m_Polygon = this;
	}

	spdPolygon(const spdPolygon &p)
	{
		m_Node = p.m_Node;
		m_Plane = p.m_Plane;
		m_Center = p.m_Center;
		m_Points = p.m_Points;
		m_EdgeVisFrame = p.m_EdgeVisFrame;
		m_VisibleEdges = p.m_VisibleEdges;
		m_SingleSided = p.m_SingleSided;
		m_Data = p.m_Data;
		m_RefNumber = p.m_RefNumber;
	}

	const spdPolygon &operator=(const spdPolygon &p)
	{
		m_Node = p.m_Node;
		m_Plane = p.m_Plane;
		m_Center = p.m_Center;
		m_Points = p.m_Points;
		m_EdgeVisFrame = p.m_EdgeVisFrame;
		m_VisibleEdges = p.m_VisibleEdges;
		m_SingleSided = p.m_SingleSided;
		m_Data = p.m_Data;
		m_RefNumber = p.m_RefNumber;
		return *this;
	}

	virtual ~spdPolygon() {}

	// used for polygon subclasses
	virtual bool IsPortal(void) const { return false; }
	// used for polygon subclasses
	virtual bool IsOccluder(void) const { return false; }

#define MAXPROJECTED 32  // Really to avoid heap allocations, or bad static solutions.
	int ProjectShaft(const spdShaft &shaft, atRangeArray<Vector2, MAXPROJECTED> *pOut) const;


	__forceinline int GetNumPoints(void) const
	{
		return m_Points.GetCount();
	}

	const Vector3 &GetNormal(void) const
	{
		return m_Plane.GetNormal();
	}

	__forceinline const Vector3 &GetPoint(int i) const
	{
		FastAssert(i >= 0 && i < m_Points.GetCount());
		return m_Points[i];
	}

	void SetPoint(int i, const Vector3 &v, bool recompute=true)  // Not fast.
	{
		FastAssert(i >= 0 && i < m_Points.GetCount());
		m_Points[i] = v;
		if (recompute)
		{
			ComputePlane();
			ComputeCenter();
		}
	}

	void MoveAlongNormal(float d)  // Not fast.
	{
		for (int i = 0; i < GetNumPoints(); i++)
			m_Points[i] += (GetNormal() * d);

		ComputePlane();
		ComputeCenter();
	}

	const Vector3 &GetCenter(void) const
	{
		return m_Center;
	}

	const spdPlane& GetPlane(void) const { return m_Plane; }
	spdPlane& GetPlane(void) { return m_Plane; }
	spdPlane ComputeEdgePlane(int i) const;

	void GetEdge(int i, Vector3 &p1, Vector3 &p2) const;

	// Used by occluders.
	spdPlane ComputeEdgePlane(const Vector3 &pos, int edge) const;

	int GetEdgeVisibleFrame() const { return m_EdgeVisFrame; }
	void SetEdgeVisibleFrame(int frame) { m_EdgeVisFrame = frame; }

	void SetEdgeVisible(int i, bool visibility) { m_VisibleEdges[i] = visibility; }
	bool IsEdgeVisible(int i) const {return m_VisibleEdges[i];}

	// Test polyhedral convexity between this poly and another.
	bool IsConvex(const spdPolygon &poly, float tol = 0.01f) const;

	bool IsFlat(float tol = 0.01f) const;
	void Flatten(void);
	void Project(const spdPlane &plane);  // Project the poly onto this new plane.

	bool AddPoint(const Vector3 &p);

	bool IsSingleSided(void) const
	{
		return m_SingleSided;
	}
	bool GetSingleSided(void) const
	{
		return m_SingleSided;
	}
	void SetSingleSided(bool b)
	{
		m_SingleSided = b;
	}

	bool IsCoplanar(const spdPlane &plane, float tol = 0.1f) const;
	bool IsCoplanarWithSimilarNormal(const spdPlane &plane, float tol = 0.1f) const;
	bool IsCoincident(const spdPolygon &poly) const;
	bool IsLevel(void) const;

	bool Classify(const spdPlane &plane, int &neg, int &pos, int &cop, float tol = 0.1f) const;

	void Cut(const spdPlane &plane, spdPolygon &poly1, spdPolygon &poly2) const;

	int Clip(const spdPlane &plane);

	int GetData(void) const { return m_Data; }
	void SetData(int i) { m_Data = i; }

	// TOOLSTUFF:
	int GetRefNumber(void) const { FastAssert(m_RefNumber >= 0); return m_RefNumber; }
	void SetRefNumber(int i) { FastAssert(m_RefNumber == -1); m_RefNumber = i; }

#if __DEV && !__TOOL
	void Draw(bool solid, float r, float g, float b, float a = 1.0f) const;
#endif

	// Is the point inside the polygon, or lying on an edge or corner?
	// Returns: 0=not on polygon, 1=on polygon, 2=on edge or corner.
	int ClassifyPoint(const Vector3 &v, float tol = 0.1f) const;

	bool LineIntersects(const Vector3 &p1, const Vector3 &p2) const;

	// Returns: 0=no intersection, 1=intersection inside, 2=intersection on edge or corner, 3=coplanar intersection.
	int ClassifyLine(const Vector3 &p1, const Vector3 &p2, float *t = NULL, float tol = 0.1f) const;

	// Do any edges of the incoming poly intersect this one?
	bool EdgesIntersect(const spdPolygon &poly, bool filterTouches, float tol = 0.1f) const;

	int DetermineEdgeConnectivity(const atArray<spdPolygon*> &polys);
	int ConnectingEdge(const spdPolygon &poly);

	bool FindPerimeter(const mshMaterial *pMtl, const mshPrimitive *pPrim, const Matrix34 &mat);

	// Perhaps these should be virtual.
	void SaveData(fiTokenizer &tok) const;
	void LoadData(fiTokenizer &tok);
#if __DEV
	void Print(const char *str = NULL) const;  // Print the polygon details.
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	spdPolyNode& GetNode() {
		return m_Node;
	}
	const spdPolyNode& GetNode() const {
		return m_Node;
	}

private:
	void ComputeCenter(void);
	bool ComputePlane(void);

protected:
	//	Vector3 Normal;
	int m_EdgeVisFrame;

private:
	int m_Data;  // General purpose.
	int m_RefNumber;  // TOOLSTUFF:

protected:
	spdPolyNode m_Node;
	spdPlane m_Plane;
	Vector3 m_Center;
	atArray<Vector3> m_Points;

	// Currently used by spdShaft::GetVisibleEdges().
	atArray<bool> m_VisibleEdges;

	bool m_SingleSided;
	u8 m_Pad1, m_Pad2, m_Pad3;
	int m_Pad4, m_Pad5, m_Pad6;


private:

	// These lists are preallocated with MAX_POLYVERTS,
	// and are used primarily as clipping buffers.
	// You can use them for anything you like when dealing with
	// polygons, but they should not have a persistent scope
	// beyond the function you are writing. You should check that
	// they do not grow or shrink the heap whenever you use them.
	static atArray<Vector3> *sm_List1;
	static atArray<Vector3> *sm_List2;

	friend class rmwRoom;
};

//rs do we need this?
#if 0
struct polygonEdge
{
public:
	polygonEdge()
	{
		PolyA = PolyB = -1;
		P[0] = P[1] = -1;
	}

	polygonEdge(const polygonEdge &e)
	{
		PolyA = e.PolyA;
		PolyB = e.PolyB;
		P[0] = e.P[0];
		P[1] = e.P[1];
	}

	const polygonEdge& operator=(const polygonEdge &e)
	{
		PolyA = e.PolyA;
		PolyB = e.PolyB;
		P[0] = e.P[0];
		P[1] = e.P[1];
		return *this;
	}

	// Returns the perimeter-edge count adjustment;
	// 1 if we just created a perimeter edge,
	// -1 if we just became an interior edge.
	int AddPoly(int i, int p1, int p2);

	bool Perimeter(void);

	int PolyA, PolyB;
	int P[2];
};
#endif


} // namespace rage

#endif // SPATIALDATA_POLYGON_H
