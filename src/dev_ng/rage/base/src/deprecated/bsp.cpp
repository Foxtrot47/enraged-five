// 
// spatialdata/bsp.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/*
	TODO:
	- For closed-region trees, consider that the non-leaf nodes are probably holding
	  data that is never used.
	- As co-planar cutting mode comes to the last poly, we end up storing the very last
	  plane at the leaf, but it is also stored once higher up in the tree.
*/

#include "bsp.h"
#include "shaft.h"

#include "atl/array.h"
#include "atl/array_struct.h"
#include "bank/bank.h"
#include "data/resource.h"

#if __DEV
#include "grcore/im.h"
#include "grcore/state.h"
#endif

#include "file/stream.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/token.h"
#include "system/memory.h"
#include "vector/geometry.h"

#include "profile/profiler.h"
#include "grprofile/ekg.h"  // pfEKGMgr

#define NOISY_BUILD 0  // Various warnings and stuff.
#define IGNORE_OPPOSITE_REGION_DATA 0

using namespace rage;

IMPLEMENT_PLACE(spdBspTree);
IMPLEMENT_PLACE(spdBspBuildNode);
IMPLEMENT_PLACE(spdBspNode);


namespace spdBspEKG
{
	PF_PAGE(spdBSP, "spd BSP");

	PF_GROUP(UpdateLoop);
	PF_LINK(spdBSP, UpdateLoop);

//	PF_TIMER(spdPointBSP, UpdateLoop);
	PF_TIMER(spdSphereBSP, UpdateLoop);
	PF_TIMER(spdLineBSP, UpdateLoop);
	PF_TIMER(spdShaftBSP, UpdateLoop);
	PF_TIMER(spdShaftPolyBSP, UpdateLoop);
	PF_TIMER(spdAddData, UpdateLoop);
};

using namespace spdBspEKG;

// Works for dc_bk_26. Not sure about the other interiors,
// but block 26 has the most complicated interior for now.
static float SPD_BSP_TOLERANCE = 0.20f;  // 0.05f


#if !__TOOL
int spdBspTree::GetTreeData(const spdShaft &shaft, int *pData, int maxData, bool backToFront) const
{
	PF_START(spdShaftBSP);

	int count = 0;

	if (Root && pData && maxData > 0)
		Root->GetTreeData(shaft, pData, maxData, count, backToFront);

	PF_STOP(spdShaftBSP);

	return count;
}

int spdBspTree::GetTreeData(const spdShaft &shaft, const atArray<const spdPolygon*> *polys, int *pData, int maxData, bool backToFront) const
{
	PF_START(spdShaftPolyBSP);

	int count = 0;

	if (Root && pData && maxData > 0)
		Root->GetTreeData(shaft, polys, pData, maxData, count, backToFront);

	PF_STOP(spdShaftPolyBSP);
	return count;
}
#endif  // !__TOOL


int spdBspTree::GetTreeData(Vector3::Vector3Param p, int *pData, int maxData, bool backToFront) const
{
//	PF_START(spdPointBSP);

	int count = 0;

	if (Root && pData && maxData > 0)
		Root->GetTreeData(p, pData, maxData, count, backToFront);

	//PF_STOP(spdPointBSP);
	return count;
}

int spdBspTree::GetLeafData(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData) const
{
	PF_START(spdLineBSP);

#if __DEV
//	if (!ClosedRegionTree)
//		Warningf("Bsp: Line/LeafData query against open-region tree, result is probably meaningless");
#endif

	int count = 0;

	if (Root && pData && maxData > 0)
		Root->GetLeafData(p1, p2, pData, maxData, count);

	PF_STOP(spdLineBSP);
	return count;
}

int spdBspTree::GetLeafData(Vector3::Vector3Param p, float radius, int *pData, int maxData) const
{
	PF_START(spdSphereBSP);

#if __DEV
//	if (!ClosedRegionTree)
//		Warningf("Bsp: Sphere/LeafData query against open-region tree, result is probably meaningless");
#endif

	int count = 0;

	if (Root && pData && maxData > 0)
		Root->GetLeafData(p, radius, pData, maxData, count);

	PF_STOP(spdSphereBSP);
	return count;
}

bool spdBspTree::GetLeafData(Vector3::Vector3Param p, int &data) const
{
	//PF_START(spdPointBSP);

#if __DEV
//	if (!ClosedRegionTree)
//		Warningf("Bsp: Point/LeafData query against open-region tree, result is probably meaningless");
#endif

	bool res = false;

	if (Root)
		res = Root->GetLeafData(p, data);

	//PF_STOP(spdPointBSP);
	return res;
}

void spdBspTree::Load(const char *name, bool temporary)
{
	if (!temporary)
		sysMemStartTemp();

	if (!temporary)
		Printf("Loading '%s.bsp'... ", name);

	Assert(!Root);

	fiSafeStream s(ASSET.Open(name, "bsp"));
	Assert(s);

	fiTokenizer tok;
	tok.Init(name, s);

	tok.MatchToken("NodeCount");
	NodeCount = tok.GetInt();
	Assert(NodeCount >= 0 && NodeCount < 16384);

	tok.MatchToken("PolyCount");
	PolyCount = tok.GetInt();
	Assert(PolyCount >= 0 && PolyCount < 16384);

	tok.MatchToken("DataCount");
	DataCount = tok.GetInt();
	Assert(DataCount >= 0 && DataCount < 16384);
	Assert(DataCount <= PolyCount);

	tok.MatchToken("ClosedRegionTree");
	ClosedRegionTree = tok.GetInt() > 0 ? true : false;

	if (!temporary)
		sysMemEndTemp();

	if (NodeCount > 0)
	{
		Root = rage_new spdBspNode[NodeCount];
		Data = rage_new int[DataCount];

		int datanum = 0;

		for (int i = 0; i < NodeCount; i++)
			Root[i].Load(*this, tok, datanum);

		Assert(datanum == DataCount);
	}

	if (!temporary)
		Printf("has %d nodes; done.\n", NodeCount);
}



void spdBspNode::Load(const spdBspTree &tree, fiTokenizer &tok, int &datanum)
{
	int i;

	tok.MatchToken("Node");
	/*int nodenum =*/ tok.GetInt();

	Vector4 plane;
	tok.GetVector(plane);
	Plane.SetCoefficientVector(plane);

	tok.MatchToken("Neg");
	int neg = tok.GetInt();

	tok.MatchToken("Pos");
	int pos = tok.GetInt();

	tok.MatchToken("SameCount");
	SameCount = tok.GetInt();

	Assert(tree.Data);
	if (SameCount > 0)
		SameData = &tree.Data[datanum];
	else
		SameData = NULL;

	for (i = 0; i < SameCount; i++)
		tree.Data[datanum++] = tok.GetInt();

	tok.MatchToken("OppCount");
	OppCount = tok.GetInt();

	Assert(tree.Data);
	if (OppCount > 0)
		OppData = &tree.Data[datanum];
	else
		OppData = NULL;

	for (i = 0; i < OppCount; i++)
		tree.Data[datanum++] = tok.GetInt();

	if (neg >= 0)
	{
		Assert(neg < tree.NodeCount);
		Neg = &tree.Root[neg];
	}
	else
		Neg = NULL;

	if (pos >= 0)
	{
		Assert(pos < tree.NodeCount);
		Pos = &tree.Root[pos];
	}
	else
		Pos = NULL;

	int count = tok.MatchInt("PolyCount");
#if NOISY_BUILD
	if (count < 0)
		Errorf("Found a node with %d polys in it", count);
#endif

#if STORE_DEBUG_POLYS
	Polys.Reallocate(count);
	for (i = 0; i < count; i++)
		Polys.Append().LoadData(tok);
#else
	// Absorb the polys.
	sysMemStartTemp();
	atArray<spdPolygon> *polys = rage_new atArray<spdPolygon>;
	polys->Reserve(count);
	for (i = 0; i < count; i++)
		polys->Append().LoadData(tok);
	delete polys;
	sysMemEndTemp();
#endif
}

// Convert the build-time tree to a runtime tree, then delete the build time.
void spdBspTree::ConvertBuildToRuntime()
{
	const int textBufferSize = 10 * 1024 * 1024;

	// Create a memory buffer to write the temporary text file to.
	sysMemStartTemp();
	char *textFile = rage_new char[textBufferSize];
	sysMemEndTemp();
	char memFileName[256];

	AssertMsg(textFile, "Out of memory trying to allocate a temporary buffer for the BSP tree");

	fiDevice::MakeMemoryFileName(memFileName, sizeof(memFileName), textFile, textBufferSize, false, "BSP_Temp_Buffer");

	// Save and load to convert from build-time to run-time buffer.
	Save(memFileName);

	// Delete the build-time buffer.
	sysMemStartTemp();
	delete Built;
	Built = NULL;
	sysMemEndTemp();

	// Time to load to create the run-time buffer.
	Load(memFileName, true);

	// Delete the temporary buffer
	sysMemStartTemp();
	delete[] textFile;
	sysMemEndTemp();
}


void spdBspTree::Save(const char *name)
{
//	sysMemStartTemp();

//	Assert(Built);

	fiSafeStream s(ASSET.Create(name, "bsp", false));
	Assert(s);

	fiTokenizer tok;
	tok.Init(name, s);

	// PolyCount and DataCount will be the same if each poly has a unique index.
	// If polys share data (such as for room numbers), then any bsp node
	// that happens to contain more than one of those polys, can only contain
	// the data that maps to that single index.

	// In other words, the difference between DataCount and PolyCount
	// is the total number of same-data polys that ended up in any given nodes.

	tok.PutStr("NodeCount %d\n", Built ? Built->CountNodesRecursively() : 0);
	tok.PutStr("PolyCount %d\n", Built ? Built->CountPolysRecursively() : 0);
	tok.PutStr("DataCount %d\n", Built ? Built->CountDataRecursively() : 0);
	tok.PutStr("ClosedRegionTree %d\n", ClosedRegionTree);
	tok.PutStr("\n");

	if (Built)
		Built->SaveRecursively(tok);

//	sysMemEndTemp();
}

#if __DECLARESTRUCT

void spdBspBuildNode::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(spdBspBuildNode);
	STRUCT_FIELD(Plane);
	STRUCT_FIELD(Polys);
	STRUCT_FIELD(UniqueSame);
	STRUCT_FIELD(UniqueOpp);
	STRUCT_FIELD(Neg);
	STRUCT_FIELD(Pos);
	STRUCT_END();
}

#endif // __DECLARESTRUCT


void spdBspBuildNode::SaveRecursively(fiTokenizer &tok) const
{
	Vector4 plane( Plane.GetCoefficientVector() );

	tok.PutStr("Node %d\n", NodeNumber);
	tok.Put(plane);
	tok.Put("\n");

	if (Neg)
		tok.PutStr("Neg %d\n", Neg->NodeNumber);
	else
		tok.PutStr("Neg -1\n");

	if (Pos)
		tok.PutStr("Pos %d\n", Pos->NodeNumber);
	else
		tok.PutStr("Pos -1\n");

#if __ASSERT
	// We can't have a leaf with >1 poly in the node.
	if (!Neg)
		Assert(UniqueSame.GetCount() <= 1);
	if (!Pos)
		Assert(UniqueOpp.GetCount() <= 1);
#endif

	int i;
	tok.PutStr("SameCount %d\n", UniqueSame.GetCount());
	for (i = 0; i < UniqueSame.GetCount(); i++)
		tok.PutStr("%d ", UniqueSame[i]);
	if (UniqueSame.GetCount() > 0)
		tok.PutStr("\n");

	tok.PutStr("OppCount %d\n", UniqueOpp.GetCount());
	for (i = 0; i < UniqueOpp.GetCount(); i++)
		tok.PutStr("%d ", UniqueOpp[i]);
	if (UniqueOpp.GetCount() > 0)
		tok.PutStr("\n");

	tok.PutStr("\n");

	tok.PutStr("PolyCount %d\n", Polys.GetCount());
	for (i = 0; i < Polys.GetCount(); i++)
		Polys[i].SaveData(tok);

	tok.PutStr("\n");

	if (Neg)
		Neg->SaveRecursively(tok);
	if (Pos)
		Pos->SaveRecursively(tok);
}


spdBspBuildNode::~spdBspBuildNode()
{
	delete Neg;
	Neg = NULL;
	delete Pos;
	Pos = NULL;
}

spdBspBuildNode *spdBspBuildNode::BuildRecursively(bool coplanar, int &count, float tol) const
{
	spdPlane plane;
	if (!ComputeCuttingPlane(coplanar, plane, tol))
		return NULL;

	// Build a list of polys on the neg, pos side of the cutting plane,
	// along with any polys that are coplanar with it.

	spdBspBuildNode neg, pos, cop;
	Cut(plane, neg, pos, cop, tol);

#if RESOURCIFY_BUILDNODES
//	sysMemEndTemp();
#endif

	spdBspBuildNode *pNode = rage_new spdBspBuildNode;
	pNode->NodeNumber = count++;
	pNode->Polys = cop.Polys;
	pNode->Plane = plane;

	// Build a list of (unique) poly refs for the same-plane and
	// opposite-plane polys in this node.

	spdBspBuildNode same, opp;
	pNode->Compare(pNode->Plane, same, opp, tol);

	int i;
	for (i = 0; i < same.Polys.GetCount(); i++)
		if (pNode->UniqueSame.Find( same.Polys[i].GetData() ) == -1)
			pNode->UniqueSame.PushAndGrow( same.Polys[i].GetData() );

	for (i = 0; i < opp.Polys.GetCount(); i++)
		if (pNode->UniqueOpp.Find( opp.Polys[i].GetData() ) == -1)
			pNode->UniqueOpp.PushAndGrow( opp.Polys[i].GetData() );


	// Either must be true, but not both.
	// If both are true, it means that a bad separating plane was chosen.
	// If both are false, it means that the cutting plane doesn't match any polys.
	// We can't just warn because we'll probably get into an infinite loop.
	Assert((cop.Polys.GetCount() > 0) ^ coplanar);

#if RESOURCIFY_BUILDNODES
//	sysMemStartTemp();
#endif

	if (neg.Polys.GetCount() > 0)
	{
//		There are poly(s) on the -ve side of the cut plane.
//		If we are building a branch that consists of only-coplanar polys, detect if
//		we've arrived at the last one. Else continue building the tree as normal.
		if (coplanar && neg.Polys.GetCount() == 1)
			pNode->Neg = neg.BuildRecursively(false, count, tol);
		else
			pNode->Neg = neg.BuildRecursively(coplanar, count, tol);
	}
	else
	{
//		There are no polys on the -ve side of the cut plane.
//		Build a list of polys that are coplanar with the cut plane,
//		and distinguish which of those face the same way as the plane
//		and which face the opposite way.
//		If we still have more than one poly that matches the plane,
//		continue building the tree as normal, while noting that
//		we only have co-planar polys remaining.

		spdBspBuildNode same, opp;
		cop.Compare(plane, same, opp, tol);

#if NOISY_BUILD
		if (same.Polys.GetCount() + opp.Polys.GetCount() != cop.Polys.GetCount())
			Errorf("-ve leaf node lost a poly somewhere (%d+%d != %d)", same.Polys.GetCount(), opp.Polys.GetCount(), cop.Polys.GetCount());
#endif

		if (same.Polys.GetCount() > 1)
			pNode->Neg = same.BuildRecursively(true, count, tol);
	}

//	See comments above.
	if (pos.Polys.GetCount() > 0)
	{
		if (coplanar && pos.Polys.GetCount() == 1)
		{
//			pNode->PosGroup = pos.Polys[0].GetData();
			pNode->Pos = pos.BuildRecursively(false, count, tol);
		}
		else
			pNode->Pos = pos.BuildRecursively(coplanar, count, tol);
	}
	else
	{
		spdBspBuildNode same, opp;
		cop.Compare(plane, same, opp, tol);

#if NOISY_BUILD
		if (same.Polys.GetCount() + opp.Polys.GetCount() != cop.Polys.GetCount())
			Errorf("+ve leaf node lost a poly somewhere (%d+%d != %d)", same.Polys.GetCount(), opp.Polys.GetCount(), cop.Polys.GetCount());
#endif

		if (opp.Polys.GetCount() > 1)
			pNode->Pos = opp.BuildRecursively(true, count, tol);
	}

	// We can't have a leaf with >1 poly in the node.
	if (!pNode->Neg && pNode->UniqueSame.GetCount() > 1)
		Errorf("-ve leaf has %d same-data entries. Probably indicates overlapping rooms or occluders somewhere.", pNode->UniqueSame.GetCount());
	if (!pNode->Pos && pNode->UniqueOpp.GetCount() > 1)
		Errorf("+ve leaf has %d opp-data entries. Probably indicates overlapping rooms or occluders somewhere.", pNode->UniqueOpp.GetCount());

	return pNode;
}

/*
	for (int i = 0; i < n; i++)
	{
		const spdPolygon &poly = pNode->Polys[i];

		Displayf("\tPoly %d:", i);
		Vector3 norm = poly.GetNormal();
		Displayf("\tnorm=%f, %f, %f", norm.x, norm.y, norm.z);
		Displayf("\tindex=%d", poly.GetData());
		Displayf("\tverts(%d)=", poly.GetNumPoints());
		for (int j = 0; j < poly.GetNumPoints(); j++)
			Displayf("\t\t%f, %f, %f", poly.GetPoint(j).x, poly.GetPoint(j).y, poly.GetPoint(j).z);
	}
*/


int spdBspBuildNode::Compare(const spdPlane &plane, spdBspBuildNode &same, spdBspBuildNode &opp, float tol) const
{
	int nMatches = 0;

	for (int i = 0; i < Polys.GetCount(); i++)
	{
		const spdPolygon &poly = Polys[i];
		int nneg, npos, ncop;

		bool cut = poly.Classify(plane, nneg, npos, ncop, tol);

		if (cut)
		{
			Assert(!poly.IsCoplanar(plane, tol));
		}
		else
		{
			// Grow the lists.
			Assert(nneg == 0 || npos == 0);
			if (ncop == poly.GetNumPoints())
			{
				Assert(nneg + npos == 0);

				float d = poly.GetNormal().Dot( plane.GetNormal() );

				if (fabsf(d) < 0.9f)
				{
#if NOISY_BUILD
					Warningf("spdBspBuildNode::Compare() Poly not really coplanar:");
					poly.Classify(plane, nneg, npos, ncop, tol);  // So we can step into the classification again.
					Displayf("comparison plane=%f, %f, %f, %f", plane.x, plane.y, plane.z, plane.w);
					Displayf("   polygon plane=%f, %f, %f, %f", poly.GetPlane().x, poly.GetPlane().y, poly.GetPlane().z, poly.GetPlane().w);
					for (int j = 0; j < poly.GetNumPoints(); j++)
						Displayf("vert %d=%f, %f, %f", j, poly.GetPoint(j).x, poly.GetPoint(j).y, poly.GetPoint(j).z);
#endif
				}
				else
				if (d > 0.0f)
				{
					same.Polys.Grow() = poly;
					nMatches++;
				}
				else
				{
					opp.Polys.Grow() = poly;
					nMatches++;
				}
			}
		}
	}

	return nMatches;
}

int spdBspBuildNode::Cut(const spdPlane &plane, spdBspBuildNode &neg, spdBspBuildNode &pos, spdBspBuildNode &cop, float tol) const
{
	int nCuts = 0;

	for (int i = 0; i < Polys.GetCount(); i++)
	{
		const spdPolygon &poly = Polys[i];
		poly.GetPlane().CheckNormal();
		int nneg, npos, ncop;

		// Note: we already did this when deciding the cutting costs.
		// We probably don't care though because this is an off-line process.
		bool cut = poly.Classify(plane, nneg, npos, ncop, tol);

		if (cut)
		{
			Assert(!poly.IsCoplanar(plane, tol));

			spdPolygon poly1, poly2;
			poly.Cut(plane, poly1, poly2);
			Assert(poly1.IsFlat(tol));
			Assert(poly2.IsFlat(tol));

			// Add the two pieces to the neg and pos lists.
			if (poly1.GetNumPoints() > 0)
				neg.Polys.Grow() = poly1;
			if (poly2.GetNumPoints() > 0)
				pos.Polys.Grow() = poly2;

			nCuts++;
		}
		else
		{
			// Grow the lists.

			Assert(nneg == 0 || npos == 0);
			if (ncop == poly.GetNumPoints())
			{
				Assert(nneg + npos == 0);
				cop.Polys.Grow() = poly;

				// Flatten this coincident poly.
				if (poly.GetNormal().Dot( plane.GetNormal() ) > 0.0f)
					cop.Polys[cop.Polys.GetCount()-1].Project( plane );
				else
					cop.Polys[cop.Polys.GetCount()-1].Project( -plane );
			}
			else
			if (nneg > 0)
			{
				Assert(npos == 0);
				neg.Polys.Grow() = poly;
			}
			else
			if (npos > 0)
			{
				Assert(nneg == 0);
				pos.Polys.Grow() = poly;
			}
			else
				Quitf("Problem while cutting: nneg=%d, npos=%d, ncop=%d", nneg, npos, ncop);
		}
	}

	Assert(neg.Polys.GetCount() +
	       pos.Polys.GetCount() +
	       cop.Polys.GetCount() -
	       nCuts == Polys.GetCount());

	return nCuts;
}

bool spdBspBuildNode::ComputeCuttingPlane(bool coplanar, spdPlane &resultantPlane, float tol) const
{
	if (coplanar)
	{
		// All the polys in this node are coplanar.
		// Find a suitable separating plane using one of the poly edges.

#if NOISY_BUILD
		Displayf("Cutting coplanar polys");
#endif
		Assert(Polys.GetCount() > 1);  // Can't separate < 2 polys.

		int cheapest = 0x7fffffff;
		spdPlane plane;
		plane.Invalidate();

		for (int i = 0; i < Polys.GetCount(); i++)
		{
//			Polys[i].Print();
			for (int j = 0; j < Polys[i].GetNumPoints(); j++)
			{
				spdPlane test = Polys[i].ComputeEdgePlane(j);
				int neg, pos, cop;
				int cost = CuttingCost(test, neg, pos, cop, tol);
				if (cost < cheapest && neg > 0 && pos > 0)  // Plane actually separates something?
				{
					cheapest = cost;
					plane = test;
				}
			}
		}

		// If this failed to find a plane, we most likely have
		// two or more remaining polys that are now coincident.
		resultantPlane = plane;
		return plane.CheckNormal();
	}
	else
	{
		// Pick a plane from one of the existing polys in this node.

		Assert(Polys.GetCount() > 0);

		int cheapest = 0x7fffffff;
		int picked = -1;

		for (int i = 0; i < Polys.GetCount(); i++)
		{
			int neg, pos, cop;
			int cost = CuttingCost(Polys[i].GetPlane(), neg, pos, cop, tol);
//#if NOISY_BUILD
			if (cop == 0)
				Errorf("Bsp: poly %d (index=%d) seems to be twisted or is otherwise bad", i, Polys[i].GetData());
//#endif
			if (cost < cheapest)
			{
				cheapest = cost;
				picked = i;
			}
		}

		Assert(picked >= 0 && picked < Polys.GetCount());

// TEMP! A debugging thing. Sometimes I want a bad tree.
//		picked = 0;

		resultantPlane = Polys[picked].GetPlane();
		return true;
	}
}

int spdBspBuildNode::CuttingCost(const spdPlane &plane, int &neg, int &pos, int &cop, float tol) const
{
	neg = 0;
	pos = 0;
	cop = 0;
	int split = 0;

	for (int i = 0; i < Polys.GetCount(); i++)
	{
		int negp, posp, copp;
		bool cut = Polys[i].Classify(plane, negp, posp, copp, tol);

		if (copp == Polys[i].GetNumPoints())
		{
			Assert(negp == 0 && posp == 0);
			cop++;
		}
		else
		{
			if (negp > 0)
				neg++;
			if (posp > 0)
				pos++;
			if (cut)
				split++;
		}
	}

	// Straight from mshBspNode::Classify().
	return abs(neg - pos) + 5 * split - 2 * cop;
}


void spdBspBuildNode::Init(const atArray<const spdPolygon*> *polys)
{
	// Filter duplicate polys, artists love 'em!

	for (int i = 0; i < polys->GetCount(); i++)
	{
		bool duplicate = false;

		for (int j = 0; j < Polys.GetCount() && !duplicate; j++)
		{
			if ((*polys)[i]->IsCoincident( Polys[j] ))
			{
				Warningf("Bsp: Ignoring duplicate poly %d, index=%d", i, (*polys)[i]->GetData());
				duplicate = true;
			}
		}

		if (!duplicate)
			Polys.Grow() = *(*polys)[i];
	}
}


void spdBspTree::Build(const atArray<spdPolygon> *polys, bool closedRegions)
{
	if (polys->GetCount() < 1)
	{
#if NOISY_BUILD
		Warningf("Tried to build bsp tree using %d polys.", polys->GetCount());
#endif
		return;
	}

	sysMemStartTemp();

	atArray<const spdPolygon*> *ptrs = rage_new atArray<const spdPolygon*>(0,polys->GetCount());

	for (int i = 0; i < polys->GetCount(); i++)
	{
		const spdPolygon &poly = (*polys)[i];
//		if (!poly.IsLevel())  // TEMP! Fallback to "2D" bsp for debugging axis-aligned rooms.
			ptrs->Append() = &poly;
	}

	sysMemEndTemp();

	Build(ptrs, closedRegions);

	sysMemStartTemp();
	delete ptrs;
	sysMemEndTemp();
}


spdBspTree::~spdBspTree()
{
	delete Built;
	Built = NULL;
	delete[] Root;
	Root = NULL;
	delete[] Data;
	Data = NULL;
}


void spdBspTree::Build(const atArray<const spdPolygon*> *polys, bool closedRegions)
{
	if (polys->GetCount() < 1)
	{
#if NOISY_BUILD
		Warningf("Tried to build bsp tree using %d polys.", polys->GetCount());
#endif
		return;
	}

#if __BANK
	size_t mem = sysMemGetMemoryUsed(-1);
#endif

	sysMemStartTemp();

	ClosedRegionTree = closedRegions;

	spdBspBuildNode *node = rage_new spdBspBuildNode;
	node->Init(polys);

	Printf("   Building %s-region bsp tree from %d polys... ", closedRegions ? "closed" : "open", polys->GetCount());
	int count = 0;
	Built = node->BuildRecursively(false, count, SPD_BSP_TOLERANCE);  // Not all polys coplanar, nodecount, tolerance.
	Assert(Built);
	Assert(count == Built->CountNodesRecursively());

	delete node;
	node = NULL;

	NodeCount = Built->CountNodesRecursively();
	PolyCount = Built->CountPolysRecursively();
	DataCount = Built->CountDataRecursively();
	Assert(DataCount <= PolyCount);

	Printf("created %d nodes; done.\n", NodeCount);

	sysMemEndTemp();

#if __BANK
	MemoryUsed = static_cast<float>((sysMemGetMemoryUsed(-1) - mem) + sizeof(*this)) / 1024.0f;
#endif
}

#if __DEV && !__TOOL
void spdBspTree::DrawDebug(void) const
{
	if (DrawTree)
	{
		grcBindTexture(NULL);
		grcState::SetAlphaBlend(false);
		grcState::SetDepthWrite(false);
		grcState::SetDepthTest(false);

		Draw(HighlightNode);
	}
}

void spdBspTree::Draw(int highlightNode) const
{
//	if (Built)
//		Built->DrawRecursively(highlightNode);

	if (Root)
	{
		if (highlightNode < 0)
		{
			for (int i = 0; i < NodeCount; i++)
				Root[i].Draw(1.0f, 0.8f, 0.8f);
		}
		else
		if (highlightNode < NodeCount)
		{
			Root[highlightNode].Draw(0.0f, 1.0f, 0.0f);
			if (Root[highlightNode].Neg)
				Root[highlightNode].Neg->Draw(0.0f, 0.0f, 1.0f);
			if (Root[highlightNode].Pos)
				Root[highlightNode].Pos->Draw(1.0f, 0.0f, 0.0f);
		}
	}
}

void spdBspBuildNode::Draw(float r, float g, float b) const
{
	for (int i = 0; i < Polys.GetCount(); i++)
	{
		Polys[i].Draw(true, r, g, b, 0.25f);
		Polys[i].Draw(false, r * 0.5f, g * 0.5f, b * 0.5f, 1.0f);
	}
}

void spdBspBuildNode::DrawRecursively(int highlightNode) const
{
	if (NodeNumber == highlightNode)
	{
		Draw(0.0f, 1.0f, 0.0f);

		if (Neg)
			Neg->Draw(0.0f, 0.0f, 1.0f);
		if (Pos)
			Pos->Draw(1.0f, 0.0f, 0.0f);
	}
	else
	if (highlightNode == -1)
	{
		for (int i = 0; i < Polys.GetCount(); i++)
		{
			Polys[i].Draw(true, 1.0f, 0.8f, 0.8f, 0.25f);
			Polys[i].Draw(false, 0.0f, 0.0f, 0.0f, 1.0f);
		}
	}

	if (Neg)
		Neg->DrawRecursively(highlightNode);
	if (Pos)
		Pos->DrawRecursively(highlightNode);
}

#if STORE_DEBUG_POLYS
void spdBspNode::Draw(float r, float g, float b) const
{
	for (int i = 0; i < Polys.GetCount(); i++)
	{
		Polys[i].Draw(true, r, g, b, 0.25f);
		Polys[i].Draw(false, r * 0.5f, g * 0.5f, b * 0.5f, 1.0f);
	}
}
#else
void spdBspNode::Draw(float, float, float) const {}
#endif

#endif


int spdBspBuildNode::CountPolysRecursively(void) const
{
	int count = Polys.GetCount();
	if (Neg)
		count += Neg->CountPolysRecursively();
	if (Pos)
		count += Pos->CountPolysRecursively();
	return count;
}

int spdBspBuildNode::CountDataRecursively(void) const
{
	int count = UniqueSame.GetCount() + UniqueOpp.GetCount();
	if (Neg)
		count += Neg->CountDataRecursively();
	if (Pos)
		count += Pos->CountDataRecursively();
	return count;
}

int spdBspBuildNode::CountNodesRecursively(void) const
{
	int count = 1;
	if (Neg)
		count += Neg->CountNodesRecursively();
	if (Pos)
		count += Pos->CountNodesRecursively();
	return count;
}


spdBspTree::spdBspTree()
{
	Built = NULL;
	Root = NULL;
	Data = NULL;
	ClosedRegionTree = false;

	NodeCount = PolyCount = DataCount = 0;

	BANK_ONLY(DrawTree = false);
	BANK_ONLY(HighlightNode = -1);
	BANK_ONLY(MemoryUsed = 0.0f);
}




void spdBspNode::GetLeafDataNeg(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData, int &count) const
{
	if (Neg)
		Neg->GetLeafData(p1, p2, pData, maxData, count);
	else
	if (SameCount > 0)
	{
//		Assert(SameCount == 1);
		AddDataToList(SameData[0], pData, maxData, count);
	}
	else
	{
		// There is no -ve data.
	}
}

void spdBspNode::GetLeafDataPos(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData, int &count) const
{
	if (Pos)
		Pos->GetLeafData(p1, p2, pData, maxData, count);
	else
	if (OppCount > 0)
	{
#if !IGNORE_OPPOSITE_REGION_DATA
//		Assert(OppCount == 1);
		AddDataToList(OppData[0], pData, maxData, count);
#endif
	}
	else
	{
		// There is no +ve data.
	}
}


void spdBspNode::GetLeafData(Vector3::Vector3Param p1, Vector3::Vector3Param p2, int *pData, int maxData, int &count) const
{
	bool a = Plane.DistanceToPlane(p1) < 0.0f;
	bool b = Plane.DistanceToPlane(p2) < 0.0f;

	if (a && b)
		GetLeafDataNeg(p1, p2, pData, maxData, count);
	else
	if (!a && !b)
		GetLeafDataPos(p1, p2, pData, maxData, count);
	else
	{
		Vector3 x;
		float t;
		int res = Plane.IntersectLine(p1, p2, x, &t);
		if (res != 1)
		{
#if NOISY_BUILD
			Warningf("Bsp: (%f, %f, %f) (%f, %f, %f) does not cross plane (t=%f, res=%d)",
			p1.x, p1.y, p1.z, p2.x, p2.y, p2.z, t, res);
#endif
		}

		if (a && !b)
		{
			GetLeafDataNeg(p1, x, pData, maxData, count);
			GetLeafDataPos(x, p2, pData, maxData, count);
		}
		else
		{
			FastAssert(!a && b);
			GetLeafDataPos(p1, x, pData, maxData, count);
			GetLeafDataNeg(x, p2, pData, maxData, count);
		}
	}
}

bool spdBspNode::GetLeafData(Vector3::Vector3Param p, int &data) const
{
	if (Plane.DistanceToPlane(p) < 0.0f)
	{
		if (Neg)
			return Neg->GetLeafData(p, data);
		else
		if (SameCount > 0)
		{
//			Assert(SameCount == 1);
			data = SameData[0];
			return true;
		}
		else
			return false;
	}
	else
	{
		if (Pos)
			return Pos->GetLeafData(p, data);
#if !IGNORE_OPPOSITE_REGION_DATA
		else
		if (OppCount > 0)
		{
//			Assert(OppCount == 1);
			data = OppData[0];
			return true;
		}
		else
#endif
			return false;
	}
}

#if !__TOOL
void spdBspNode::GetTreeData(const spdShaft &shaft, int *pData, int maxData, int &count, bool backToFront) const
{
	Vector3 p = shaft.GetPosition();

	int neg, pos, cop;
	Plane.Classify(shaft.GetCorners(), 8, neg, pos, cop, 0.0f);

	if (backToFront ^ (Plane.DistanceToPlane(p) < 0.0f))
	{
		if (Neg && pos < 8)
			Neg->GetTreeData(shaft, pData, maxData, count, backToFront);

		if (neg < 8 && pos < 8)
			AddDataToList(pData, maxData, count);

		if (Pos && neg < 8)
			Pos->GetTreeData(shaft, pData, maxData, count, backToFront);
	}
	else
	{
		if (Pos && neg < 8)
			Pos->GetTreeData(shaft, pData, maxData, count, backToFront);

		if (neg < 8 && pos < 8)
			AddDataToList(pData, maxData, count);

		if (Neg && pos < 8)
			Neg->GetTreeData(shaft, pData, maxData, count, backToFront);
	}
}

void spdBspNode::GetTreeData(const spdShaft &shaft, const atArray<const spdPolygon*> *polys, int *pData, int maxData, int &count, bool backToFront) const
{

	Vector3 p = shaft.GetPosition();

	int neg, pos, cop;
	Plane.Classify(shaft.GetCorners(), 8, neg, pos, cop, 0.0f);

	if (backToFront ^ (Plane.DistanceToPlane(p) < 0.0f))
	{
		if (Neg && pos < 8)
			Neg->GetTreeData(shaft, polys, pData, maxData, count, backToFront);

		if (neg < 8 && pos < 8)
			AddDataToList(shaft, polys, pData, maxData, count);

		if (Pos && neg < 8)
			Pos->GetTreeData(shaft, polys, pData, maxData, count, backToFront);
	}
	else
	{
		if (Pos && neg < 8)
			Pos->GetTreeData(shaft, polys, pData, maxData, count, backToFront);

		if (neg < 8 && pos < 8)
			AddDataToList(shaft, polys, pData, maxData, count);

		if (Neg && pos < 8)
			Neg->GetTreeData(shaft, polys, pData, maxData, count, backToFront);
	}
}
#endif  // !__TOOL


void spdBspNode::GetTreeData(Vector3::Vector3Param p, int *pData, int maxData, int &count, bool backToFront) const
{
	if (backToFront ^ (Plane.DistanceToPlane(p) < 0.0f))
	{
		if (Neg)
			Neg->GetTreeData(p, pData, maxData, count, backToFront);

		AddDataToList(pData, maxData, count);

		if (Pos)
			Pos->GetTreeData(p, pData, maxData, count, backToFront);
	}
	else
	{
		if (Pos)
			Pos->GetTreeData(p, pData, maxData, count, backToFront);

		AddDataToList(pData, maxData, count);

		if (Neg)
			Neg->GetTreeData(p, pData, maxData, count, backToFront);
	}
}

void spdBspNode::GetLeafData(Vector3::Vector3Param p, float radius, int *pData, int maxData, int &count) const
{
	float d = Plane.DistanceToPlane(p);

	bool close = fabsf(d) < radius;

	if (close || d < 0.0f)
	{
		if (Neg)
			Neg->GetLeafData(p, radius, pData, maxData, count);
		else
		if (SameCount > 0)
		{
//			Assert(SameCount == 1);
			AddDataToList(SameData[0], pData, maxData, count);
		}
	}

	if (close || d >= 0.0f)
	{
		if (Pos)
			Pos->GetLeafData(p, radius, pData, maxData, count);
		else
		if (OppCount > 0)
		{
#if !IGNORE_OPPOSITE_REGION_DATA
//			Assert(OppCount == 1);
			AddDataToList(OppData[0], pData, maxData, count);
#endif
		}
	}
}

#if !__TOOL
void spdBspNode::AddDataToList(const spdShaft &shaft, const atArray<const spdPolygon*> *polys, int *pData, int maxData, int &count) const
{
	FastAssert(SameCount >= 0 && SameCount < 1024);  // Sanity.
	FastAssert(OppCount >= 0 && OppCount < 1024);    // Sanity.

	int i;
	for (i = 0; i < SameCount; i++)
		if (shaft.IsVisible( *((*polys)[SameData[i]]) ))
			AddDataToList(SameData[i], pData, maxData, count);
//		else
//			Displayf("Bsp: clipped same-poly %d, frame=%d", SameData[i], TIME.GetFrameCount());

	for (i = 0; i < OppCount; i++)
		if (shaft.IsVisible( *((*polys)[OppData[i]]) ))
			AddDataToList(OppData[i], pData, maxData, count);
//		else
//			Displayf("Bsp: clipped opp-poly %d, frame=%d", OppData[i], TIME.GetFrameCount());
}
#endif  // !__TOOL

void spdBspNode::AddDataToList(int *pData, int maxData, int &count) const
{
	FastAssert(SameCount >= 0 && SameCount < 1024);  // Sanity.
	FastAssert(OppCount >= 0 && OppCount < 1024);    // Sanity.

	int i;
	for (i = 0; i < SameCount; i++)
		AddDataToList(SameData[i], pData, maxData, count);

	for (i = 0; i < OppCount; i++)
		AddDataToList(OppData[i], pData, maxData, count);
}

void spdBspNode::AddDataToList(int data, int *pData, int maxData, int &count) const
{
	PF_START(spdAddData);

	for (int i = 0; i < count; i++)
		if (pData[i] == data)
		{
			PF_STOP(spdAddData);
			return;
		}

	if (count >= maxData)
	{
#if __DEV
		Warningf("Bsp: data array too short (%d entries, count=%d)", maxData, count);
#endif
		PF_STOP(spdAddData);
		return;
	}

	pData[count++] = data;
	PF_STOP(spdAddData);
}


#if __BANK
#include "bank/bkmgr.h"

void spdBspTree::AddWidgets(bkBank & bank, const char *name)
{
	char str[256];
	sprintf(str, "Bsp (%s)", name);

	// push BSP group
	bank.PushGroup(str,false);

	bank.PushGroup("Stats", false);
	bank.AddSlider("Num Nodes", &NodeCount, 0, 0, 0);
	bank.AddSlider("Num Polys", &PolyCount, 0, 0, 0);
	bank.AddSlider("MemoryUsed(K)", &MemoryUsed, 0, 0, 0);
	bank.PopGroup();

	bank.PushGroup("Draw Debug",false);
	bank.AddToggle("Draw Tree", &DrawTree);
	bank.AddSlider("Highlight Node", &HighlightNode, -1, 1024, 1);
	bank.PopGroup();

	// pop BSP group
	bank.PopGroup();
}
#endif

#if __DECLARESTRUCT

void spdBspTree::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(spdBspTree);
	STRUCT_FIELD(Built);
	STRUCT_FIELD(NodeCount);
	STRUCT_FIELD(Root);
	STRUCT_FIELD(PolyCount);
	STRUCT_DYNAMIC_ARRAY(Data, DataCount);
	STRUCT_FIELD(DataCount);
	STRUCT_FIELD(MemoryUsed);
	STRUCT_FIELD(HighlightNode);
	STRUCT_FIELD(DrawTree);
	STRUCT_FIELD(ClosedRegionTree);
	STRUCT_CONTAINED_ARRAY(m_Pad);
	STRUCT_END();
}

#endif // __DECLARESTRUCT



#include "data/resourcehelpers.h"

spdBspTree::spdBspTree(datResource &rsc)
	: Built(rsc)
	, Root(rsc)
{
	rsc.PointerFixup(Data);

#if RESOURCIFY_BUILDNODES
	// TODO: We don't want this stuff in non-__DEV builds.
	// The build nodes should be moved out of here altogether
	// but it would still be nice to be able to render the
	// polys at each node for debugging.
#else
	Built = NULL;
#endif
}


//Warning: Cannot delete from resource heap; use datMemoryStart/EndUseTemporary

spdBspNode::spdBspNode(datResource &rsc)
: Plane(rsc)
, Neg(rsc)
, Pos(rsc)
#if STORE_DEBUG_POLYS
, Polys(rsc)
#endif
{
	rsc.PointerFixup(SameData);
	rsc.PointerFixup(OppData);
}

#if __DECLARESTRUCT
void spdBspNode::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(spdBspNode);
	STRUCT_FIELD(Plane);
	//STRUCT_DYNAMIC_ARRAY(SameData, SameCount);
	STRUCT_FIELD_VP(SameData);		// Note: The contents will be fixed up by spdBspNode::DeclareStruct.
	STRUCT_FIELD(SameCount);
//	STRUCT_DYNAMIC_ARRAY(OppData, OppCount);
	STRUCT_FIELD_VP(OppData);		// Note: The contents will be fixed up by spdBspNode::DeclareStruct.
	STRUCT_FIELD(OppCount);
	STRUCT_FIELD(Neg);
	STRUCT_FIELD(Pos);
#if STORE_DEBUG_POLYS
	STRUCT_FIELD(Poys);
#else
	STRUCT_CONTAINED_ARRAY(Pad);
#endif
	STRUCT_END();
}

#endif // __DECLARESTRUCT


spdBspBuildNode::spdBspBuildNode(datResource &rsc)
	: Polys(rsc, true)
	, UniqueSame(rsc)
	, UniqueOpp(rsc)
	, Neg(rsc)
	, Pos(rsc)
{
}
