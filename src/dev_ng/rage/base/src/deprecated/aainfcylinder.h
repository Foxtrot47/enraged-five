// 
// spatialdata/aainfcylinder.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_AAINFCYLINDER_H
#define SPATIALDATA_AAINFCYLINDER_H

#include "vector/vector3.h"

namespace rage {

namespace spdAxes
{
	enum Axes {
		AXIS_X,
		AXIS_Y,
		AXIS_Z
	};
}

// PURPOSE:
//   This class represents an axis aligned infinitely long cylinder. The axis template parameter determines which axis of
//   the cylinder is aligned along. There are implementations for cylinders aligned along the X, Y, and Z axes.
// NOTES:
//   The class uses a Vector2 to represent the center of the cylinder. The Vector2's components are in the same order
//   as a Vector3's, but leave out the component along the axis of the cylinder.
template<int axis>
class spdAAInfCylinder
{
public:

	// RETURNS: true if this point is within the cylinder.
	bool Contains(const Vector3& point);

	// RETURNS: true if the two cylinders intersect
	bool Intersects(const spdAAInfCylinder<axis>& other);

	// RETURNS: The center of the cylinder. 
	// NOTES: The component that lies on the cylinder's axis will be 0.0. 
	Vector3 GetCenter() const;

	// RETURNS: The two components of the center point that don't lie along the cylinder's axis.
	Vector2 GetCenterVector2() const;

	// RETURNS: The radius of the cylinder
	float GetRadius(void) const;

	// PURPOSE: Sets the center and radius of the cylinder
	void Set(const Vector3& center, float radius);

	// PURPOSE: Sets the center and radius of the cylinder
	// NOTES: For an X-aligned cylinder, center should be Y,Z. For a Y-aligned cylinder, 
	// center should be X,Z. For a Z-aligned cylinder, center should be X, Y.
	void Set(const Vector2& center, float radius);

	// PURPOSE: Sets the center of the cylinder
	void SetCenter(const Vector3& center);

	// PURPOSE: Sets the center of the cylinder.
	void SetCenter(const Vector2& center);

	// PURPOSE: Sets the radius of the cylinder.
	void SetRadius(float f);

protected:
	// PURPOSE: Removes the component of the vector along the cylinder's axis
	static Vector2 GetVec2(const Vector3& point);

	// PURPOSE: Sets the component of the vector along the cylinder's axis to 0.0f
	static Vector3 GetVec3(const Vector2& point);

	Vector2 m_Center;
	float m_Radius;
};

typedef spdAAInfCylinder<spdAxes::AXIS_X> spdAAInfCylinderX;
typedef spdAAInfCylinder<spdAxes::AXIS_Y> spdAAInfCylinderY;
typedef spdAAInfCylinder<spdAxes::AXIS_Z> spdAAInfCylinderZ;

template<>
inline Vector2 spdAAInfCylinder<spdAxes::AXIS_X>::GetVec2(const Vector3& point)
{
	return Vector2(point.y, point.z);
}

template<>
inline Vector2 spdAAInfCylinder<spdAxes::AXIS_Y>::GetVec2(const Vector3& point)
{
	return Vector2(point.x, point.z);
}

template<>
inline Vector2 spdAAInfCylinder<spdAxes::AXIS_Z>::GetVec2(const Vector3& point)
{
	return Vector2(point.x, point.y);
}

template<>
inline Vector3 spdAAInfCylinder<spdAxes::AXIS_X>::GetVec3(const Vector2& point)
{
	return Vector3(0.0f, point.x, point.y);
}

template<>
inline Vector3 spdAAInfCylinder<spdAxes::AXIS_Y>::GetVec3(const Vector2& point)
{
	return Vector3(point.x, 0.0f, point.y);
}

template<>
inline Vector3 spdAAInfCylinder<spdAxes::AXIS_Z>::GetVec3(const Vector2& point)
{
	return Vector3(point.x, point.y, 0.0f);
}

template<int axis>
inline bool spdAAInfCylinder<axis>::Contains(const Vector3& point)
{
	return GetVec2(point).Dist2(m_Center) <= m_Radius * m_Radius;
}

template<int axis>
inline bool spdAAInfCylinder<axis>::Intersects(const spdAAInfCylinder<axis>& other)
{
	return m_Center.Dist2(other.m_Center) <= (m_Radius * m_Radius + other.m_Radius * other.m_Radius);
}

template<int axis>
inline Vector3 spdAAInfCylinder<axis>::GetCenter() const
{
	return GetVec3(m_Center);
}

template<int axis>
inline Vector2 spdAAInfCylinder<axis>::GetCenterVector2() const
{
	return m_Center;
}

template<int axis>
inline float spdAAInfCylinder<axis>::GetRadius(void) const
{
	return m_Radius;
}

template<int axis>
inline void spdAAInfCylinder<axis>::Set(const Vector3& center, float radius)
{
	m_Center.Set(GetVec2(center));
	m_Radius = radius;
}

template<int axis>
inline void spdAAInfCylinder<axis>::Set(const Vector2& center, float radius)
{
	m_Center.Set(center);
	m_Radius = radius;
}

template<int axis>
inline void spdAAInfCylinder<axis>::SetCenter(const Vector3& center)
{
	m_Center.Set(GetVec2(center));
}

template<int axis>
inline void spdAAInfCylinder<axis>::SetCenter(const Vector2& center)
{
	m_Center.Set(center);
}

template<int axis>
inline void spdAAInfCylinder<axis>::SetRadius(float f)
{
	m_Radius = f;
}

}

#endif
