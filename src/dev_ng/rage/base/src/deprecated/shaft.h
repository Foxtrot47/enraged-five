// 
// spatialdata/shaft.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SPATIALDATA_SHAFT_H
#define SPATIALDATA_SHAFT_H

#include "aabb.h"
#include "plane.h"
#include "sphere.h"

#include "grcore/viewport.h"
#include "vector/vector3.h"

#define SPD_VALIDATE_ISVISIBLE		0

#define FAST_SHAFT		((__XENON || __PS3) && VECTORIZED)
#define FAST_SPDVIEWPORT		1
#define FAST_MATRIX				1


namespace rage {
class rmcDrawable;
class spdPolygon;
class spdSphere;
/*
PURPOSE
A structure to contain information about the grcViewport
that was used to make a shaft.
NOTES
Functions such as spdShaft::Set(const grcViewport &, const Matrix34 &)
populate this structure.
*/
struct spdViewport
{
	spdViewport() : ZClipNear(0.0f), ZClipFar(0.0f),
		TanHFOV(0.0f), TanVFOV(0.0f),
		PixelLeft(0.0f), PixelTop(0.0f),
		PixelWidth(0.0f), PixelHeight(0.0f) {}

		float GetNearClip() const { return ZClipNear; }
		float GetFarClip() const { return ZClipFar; }
		float GetTanHFOV() const { return TanHFOV; }
		float GetTanVFOV() const { return TanVFOV; }
		float GetX1() const { return PixelLeft; }
		float GetY1() const { return PixelTop; }
		float GetWidth() const { return PixelWidth; }
		float GetHeight() const { return PixelHeight; }
#if FAST_SPDVIEWPORT
		float GetInvWidth() const { return invPixelWidth; }
		float GetInvHeight() const { return invPixelHeight; }
#endif
		float GetX2() const { return PixelLeft + PixelWidth - 1.0f; }
		float GetY2() const { return PixelTop + PixelHeight - 1.0f; }
		void Set(const grcViewport &viewport);

protected:
	float ZClipNear, ZClipFar;
	float TanHFOV, TanVFOV;
	float PixelLeft, PixelTop;
	float PixelWidth, PixelHeight;
#if FAST_SPDVIEWPORT
	float invPixelWidth, invPixelHeight;
#endif
};


/*
PURPOSE
A class to contain basically the camera position in world space,
a projection plane, and a 2D region on that plane, measured in pixels.
NOTES
Apical: adj. Of, relating to, located at, or constituting an apex.

In this case, think of it as the frustum apex (camera position)
in combination with the viewport, and a 2D viewing region on that viewport.
*/
class spdApical
{
public:
	spdApical() : X1(0.0f), X2(0.0f), Y1(0.0f), Y2(0.0f), Matrix(M34_IDENTITY) {}

	virtual ~spdApical() {}

	spdApical(const spdApical &a)
	{
		Matrix = a.Matrix;
		Inverse = a.Inverse;
		ProjM = a.ProjM;
		ProjN = a.ProjN;
		X1 = a.X1;
		X2 = a.X2;
		Y1 = a.Y1;
		Y2 = a.Y2;
		ViewportPlane = a.ViewportPlane;
		Viewport = a.Viewport;
	}

	const spdApical &operator=(const spdApical &a)
	{
		Matrix = a.Matrix;
		Inverse = a.Inverse;
		ProjM = a.ProjM;
		ProjN = a.ProjN;
		X1 = a.X1;
		X2 = a.X2;
		Y1 = a.Y1;
		Y2 = a.Y2;
		ViewportPlane = a.ViewportPlane;
		Viewport = a.Viewport;
		return *this;
	}

	float GetDistanceToViewpoint(const Vector3 &pos) const;

	// Position of apex (usually the viewpoint.)^
	const Vector3 &GetPosition() const { return Matrix.d; }

	// Matrix representing the position and orientation of this apical.
	const Matrix34 &GetMatrix() const { return Matrix; }

	const Matrix34 &GetInverse() const { return Inverse; }

	// Sets the matrix and its inverse (note that this means the matrix must be invertable).
	// Also builds projection matrices.
	virtual void SetMatrix(const grcViewport &viewport, const Matrix34 &mat)
	{
#if FAST_MATRIX

// 		ScalarV _zero(V_ZERO);
// 		ScalarV _one(V_ONE);

		Matrix = mat;

// 		RC_VEC4V(Matrix.a).SetW( _zero );
// 		RC_VEC4V(Matrix.b).SetW( _zero );
// 		RC_VEC4V(Matrix.c).SetW( _zero );
// 		RC_VEC4V(Matrix.d).SetW( _one );

		Mat44V invM44V;
		InvertFull( invM44V, MATRIX44_TO_MAT44V(Matrix) );
		Inverse = MAT34V_TO_MATRIX34(invM44V.GetMat34());

#else
		Matrix = mat;
		AssertVerify(Inverse.Inverse(Matrix));
#endif
		PrepareProjections(viewport);
	}

	const spdViewport &GetViewport() const { return Viewport; }

	// Get the viewport plane in world-space.
	const spdPlane &GetViewportPlane() const { return ViewportPlane; }

	// Project a 3D point onto the 2D viewport.
	grcCullStatus Project(Vector3::Vector3Param point, Vector2 &out) const;
	__forceinline grcCullStatus ProjectInline(Vector3::Vector3Param point, Vector2 &out) const;

	// Is the polygon in front of the viewport?
	grcCullStatus IsProjectable(const spdPolygon &polygon) const;

	// is this region's width and height at least 1/100th of the screen width and height?
	bool IsRegionBigEnough() const;

	bool IsFacing(const spdPlane &plane) const
	{
		//		Vector3 v = plane.Pos - GetPosition();
		//		return v.Dot(plane.Normal) < 0.0f ? true : false;
		Vector3 v = plane.GetPos() - GetPosition();
		return v.Dot(plane.GetNormal()) < 0.0f ? true : false;
	}

#if __BANK
	void AddWidgets();
	static bool WidgetsAdded;
	static int NumProjectedPoints, SetMatrixCalls;
	static int PolygonsProjected, PolygonsClipped;
	//	bool UseBoxFrustumTests, DrawProjectedSpheres;
#endif

	// Region accessors.
	float GetX1() const { return X1; }
	float GetX2() const { return X2; }
	float GetY1() const { return Y1; }
	float GetY2() const { return Y2; }

	virtual void SetX1(float x1) { X1 = x1; }
	virtual void SetX2(float x2) { X2 = x2; }
	virtual void SetY1(float y1) { Y1 = y1; }
	virtual void SetY2(float y2) { Y2 = y2; }

	// A way of recording which portal was responsible for
	// making this shaft. A bit nasty; remove if possible.
	//	const rmwPortal *Portal;

protected:

	// The current region that maps onto the viewport.
	float X1, X2, Y1, Y2;

	void PrepareProjections(const grcViewport &viewport);
	Matrix44 ProjM, ProjN;

	Matrix34 Inverse;

	// Use the accessors to get at this.
	Matrix34 Matrix;

	// The world-space plane of the viewport.
	spdPlane ViewportPlane;
	spdViewport Viewport;
};

/*
PURPOSE
A class that is very like a frustum. A distinction is made by name to
emphasize the fact that the six planes may not represent a symmetrical
pyramid, and that the front and back planes are not necessarily parallel.
NOTES
Shafts always have six planes, like a pyramid.

Culling services live in this class, and other things you'd expect from a
frustum.

Shafts are clipped against each other to perform portal culling; functions
like Intersect() modify the shaft, by shrinking it, to fit around a world-
space polygon.

<FLAG Component>
*/
class spdShaft : public spdApical
{
public:

	// Don't change the order of these.
	enum { PLANE_NEAR, PLANE_FAR, PLANE_LEFT, PLANE_RIGHT, PLANE_TOP, PLANE_BOTTOM, NUMOF_PLANES };

	spdShaft()
	{
		// If you allocate memory in here, think about the resource ctors for
		// rmwStreamedNodeList and rmwStreamedNodeArray, which at the time of writing
		// make a temporary shaft on the stack.

		// TODO: Possibly __DEV only. See Set() though.
		for (int i = 0; i < NUMOF_PLANES; i++)
		{
			LocalPlanes[i].Invalidate();
			WorldPlanes[i].Invalidate();
		}
		for (int i = 0; i < 8; i++)
			WorldCorners[i].Zero();
		CornersUpToDate = false;
		Inside = false;
		m_IsOccluder  =false;
	}

	virtual ~spdShaft() {}

	spdShaft(const spdShaft &f)
	{
		*this = f;
	}

	const spdShaft &operator=(const spdShaft &f)
	{
		spdApical::operator=(f);
		for (int i = 0; i < NUMOF_PLANES; i++)
		{
			LocalPlanes[i] = f.LocalPlanes[i];
			WorldPlanes[i] = f.WorldPlanes[i];
			AbsPlaneNormals[i] = f.AbsPlaneNormals[i];
		}
		for (int i = 0; i < 8; i++)
			WorldCorners[i] = f.WorldCorners[i];
		CornersUpToDate = f.CornersUpToDate;
		Inside = f.Inside;
		BoundingBox = f.BoundingBox;
		m_IsOrtho=f.m_IsOrtho;
		//		Cone = f.Cone;

		m_IsOccluder = f.m_IsOccluder;
		return *this;
	}

	// PURPOSE: Return a world-space plane.
	const spdPlane &GetPlane(int i) const
	{
		FastAssert(i >= 0 && i < NUMOF_PLANES);
		return WorldPlanes[i];
	}

	// PURPOSE: Return a local-space plane.
	const spdPlane &GetLocalPlane(int i) const
	{
		FastAssert(i >= 0 && i < NUMOF_PLANES);
		return LocalPlanes[i];
	}

	// Region accessors that also update the shaft.
	void SetX1(float x1);
	void SetX2(float x2);
	void SetY1(float y1);
	void SetY2(float y2);


//	// RETURNS: 'true' if at least one vertex in the bound's OOBB is visible.
//	bool IsVisible(const rmwBoundInfo &bound) const;

	// RETURNS: 'true' if at least one vertex in the polygon in visible.
	bool IsVisible(const spdPolygon &polygon) const;

	// PURPOSE: Sets the visibility bits on the polygon's edges.
	// RETURNS: 'true' if polygon is at all visible.
	bool GetVisibleEdges(spdPolygon &polygon) const;

	// RETURNS: 'true' if any part of the sphere is visible.
	
	bool IsVisible( Vec3V_In center, ScalarV_In radius) const;
	bool IsVisible(const spdSphere &sphere) const;


	// RETURNS: grcCullStatus depending on the visibility of the sphere in this shaft.
	grcCullStatus IsSphereVisible(const spdSphere &sphere, float *zDist = NULL) const;

	// RETURNS: 'true' if any part of the sphere is visible. Does NOT consider near or far planes
	bool IsVisibleWithoutNearFar(const spdSphere& sphere) const;

	grcCullStatus IsVisible(const Matrix34 &mtx,const rmcDrawable &drawable,u8 &lod, float *zDist = NULL) const;
	grcCullStatus IsVisible(const Matrix44 &mtx,const rmcDrawable &drawable,u8 &lod, float *zDist = NULL) const;

#if SPD_VALIDATE_ISVISIBLE
	grcCullStatus IsVisible_ShaftAABB(const spdAABB &aabb) const;
	grcCullStatus IsVisibleNew(const spdAABB &aabb) const;
	grcCullStatus IsVisibleOld(const spdAABB &aabb) const;
#else
	grcCullStatus IsVisible_ShaftAABB(const spdAABB &aabb) const;
#endif
	inline grcCullStatus IsVisibleInline_ShaftAABB(const spdAABB *RESTRICT aabb) const;
	inline grcCullStatus IsOutsideInline_ShaftAABB(const spdAABB *RESTRICT aabb) const;

	inline grcCullStatus IsVisibleInline( Vec3V_In centerV, Vec3V_In halfExtents, ScalarV_In inscribedRadiusV ) const;
	inline grcCullStatus IsOutside( Vec3V_In centerV, Vec3V_In halfExtents, ScalarV_In inscribedRadiusV ) const;

	// RETURNS: 'true' if any vertex in the list is in the frustum.
	bool IsVisible(const Vector3 *verts, int count) const;

	// PURPOSE: Shrink the frustum to fit around the polygon.
	// NOTES: The polygon is projected and possibly clipped to form a 2D region first.
	bool Intersect(const spdPolygon &polygon);

	// Returns a pointer to the eight corners of the shaft,
	// 0-3 = near plane
	// 4-7 = far plane
	// If the assert fires, try calling UpdateCorners(),
	// although really that should only get called once a frame.
	const Vector3 *GetCorners() const { FastAssert(CornersUpToDate); return WorldCorners; }

	const Vector3 &GetAbsPlaneNormal(int i) const	{ return AbsPlaneNormals[i]; }

	// Returns the bounding box of this shaft.
	const spdAABB &GetBoundingBox() const	{ return BoundingBox; }

	bool Intersects(const spdPlane &plane) const;

	void Set(const grcViewport &viewport, const Matrix34 &mat);

	void DrawDebug(bool solid = false, bool depthTest = false) const;

	bool IsInside() const { return Inside; }
	void SetInside(bool b) { Inside = b; }

	inline bool IsOccluder() const { return m_IsOccluder; }
	inline void SetOccluder(bool b) { m_IsOccluder = b; }
	// This gets you a normalized vector that runs from the apex
	// along the center of the shaft, sort of a forward vector.
	Vector3 GetCenterVector() const;

protected:
	virtual void SetMatrix(const grcViewport &viewport, const Matrix34 &mat)
	{
		spdApical::SetMatrix(viewport, mat);

		// TODO: Could detect that viewport is unchanged.
		for (int i = 0; i < NUMOF_PLANES; i++)
			LocalPlanes[i].Transform(GetMatrix(), WorldPlanes[i]);
		CornersUpToDate = false;

		// If things in here change, update SetX1() etc.
	}

private:
	void ComputeOrthoCorners();

	// This function expects a world-space plane.
	void SetFrontPlane(const spdPlane &plane);
	const spdPlane &GetFrontPlane() const { return WorldPlanes[PLANE_NEAR]; }

	//	rmwCone Cone;
	//	void UpdateCone();

	spdPlane LocalPlanes[NUMOF_PLANES];
	spdPlane WorldPlanes[NUMOF_PLANES];

	// These are essentially the same as WorldPlanes[].GetNormal(), but with
	// the absolute value of each component.
	Vector3 AbsPlaneNormals[NUMOF_PLANES];

	void UpdateCorners();

	// World-space corners of the shaft.
	Vector3 WorldCorners[8];
	spdAABB BoundingBox;
	bool CornersUpToDate;

	// Inside or outside.
	bool Inside;
	bool m_IsOrtho;
	bool m_IsOccluder;
};



inline bool spdShaft::IsVisible(Vec3V_In center, ScalarV_In radius) const
{
#if 1
	// this seems to be faster code ... the fn is too short TO NOT BE inlined- svetli
	for (int i = 0; i < NUMOF_PLANES; i++)
	{
		if( IsGreaterThanAll( WorldPlanes[i].DistanceToPlaneV(center), radius ) )
			return false;
	}
#else
	int i = 0;

	if( IsGreaterThanAll( WorldPlanes[i++].DistanceToPlaneV(center), radius ) )
		return false;
	if( IsGreaterThanAll( WorldPlanes[i++].DistanceToPlaneV(center), radius ) )
		return false;
	if( IsGreaterThanAll( WorldPlanes[i++].DistanceToPlaneV(center), radius ) )
		return false;
	if( IsGreaterThanAll( WorldPlanes[i++].DistanceToPlaneV(center), radius ) )
		return false;
	if( IsGreaterThanAll( WorldPlanes[i++].DistanceToPlaneV(center), radius ) )
		return false;
	if( IsGreaterThanAll( WorldPlanes[i++].DistanceToPlaneV(center), radius ) )
		return false;

	Assert( i == spdShaft::NUMOF_PLANES );
#endif

	return true;
}


}

#endif
