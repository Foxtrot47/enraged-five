// 
// spatialdata/worlddebugdraw.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "worlddebugdraw.h"

#if __BANK

#include "shaft.h"
#include "sphere.h"

#include "bank/bank.h"
#include "grcore/device.h"
#include "grcore/font.h"
#include "grcore/im.h"
#include "grcore/quads.h"
#include "input/mouse.h"
#include "spatialdata/polygon.h"


namespace rage {

spdWorldDebugDraw::spdWorldDebugDraw()
{
	m_Left = 100.0f;
	m_Top = 100.0f;
	m_Right = GRCDEVICE.GetWidth() - 100.0f;
	m_Bottom = GRCDEVICE.GetHeight() - 100.0f;
	m_CoordinateString[0] = 0;

	Init();
}

spdWorldDebugDraw::spdWorldDebugDraw(float left, float top, float right, float bottom)
	: m_Left(left)
	, m_Top(top)
	, m_Right(right)
	, m_Bottom(bottom)
{
	Init();
}

void spdWorldDebugDraw::Init()
{
	m_OriginalBoundingBox.Set(Vector3(-10000.0f, -10000.0f, -10000.0f), Vector3(10000.0f, 10000.0f, 10000.0f));
	m_BackgroundColor.Set(0, 0, 0, 0);
	m_Zoom = 1.0f;
	m_OffsetX = 0.0f;
	m_OffsetZ = 0.0f;
	m_DisableBoundingBoxUpdates = false;
	m_ShaftTopPlaneOnly = false;
	m_SelectionBorder = 10.0f;
	m_SubstringFilter[0] = 0;

	UpdateBoundingBox();
}

// PURPOSE: Set the dimensions of this object on the screen, in screen coordinates.
void spdWorldDebugDraw::SetScreenDimensions(float left, float top, float right, float bottom)
{
	m_Left = left;
	m_Top = top;
	m_Right = right;
	m_Bottom = bottom;
}

// PURPOSE: Get the dimensions of this object on the screen, in screen coordinates.
void spdWorldDebugDraw::GetScreenDimensions(float &outLeft, float &outTop, float &outRight, float &outBottom)
{
	outLeft = m_Left;
	outTop = m_Top;
	outRight = m_Right;
	outBottom = m_Bottom;
}

// Convert a point in 3D space to a 2D coordinate on the screen.
void spdWorldDebugDraw::ConvertTo2D(Vector3::Vector3Param position, Vector3 &outVector3) const
{
	Vector3 pos(position);

	outVector3.x = (pos.x - m_BoundingBox.GetMin().x) / m_BoundingBox.GetWidth() * (m_Right - m_Left) + m_Left;
	outVector3.y = (pos.z - m_BoundingBox.GetMin().z) / m_BoundingBox.GetDepth() * (m_Bottom - m_Top) + m_Top;
	outVector3.z = 0.0f;
}

// Convert a point in 3D space to a 2D coordinate on the screen.
void spdWorldDebugDraw::ConvertTo3D(Vector3::Vector3Param position, Vector3 &outVector) const
{
	Vector3 pos(position);

	outVector.x = (pos.x - m_Left) / (m_Right - m_Left) * m_BoundingBox.GetWidth() + m_BoundingBox.GetMin().x;
	outVector.y = 0.0f;
	outVector.z = (pos.y - m_Top) / (m_Bottom - m_Top) * m_BoundingBox.GetDepth() + m_BoundingBox.GetMin().z;
}

// Convert a length in 3D space to a 2D distance on the screen.
void spdWorldDebugDraw::ConvertTo2D(float distance, float &outDistance) const
{
	outDistance = distance / m_BoundingBox.GetWidth() * (m_Right - m_Left);
}

// Create a version of this color that's used for highlighting solid objects.
Color32 spdWorldDebugDraw::CreateHighlightColor(const Color32 &color)
{
	Color32 result = color;
	result.SetAlpha(color.GetAlpha() / 4);

	return result;
}

// PURPOSE: Clear the background of the object.
void spdWorldDebugDraw::Clear() const
{
	if (m_BackgroundColor.GetAlphaf() > 0.0f)
	{
		grcBeginQuads(1);
		grcDrawQuadf(m_Left, m_Top, m_Right, m_Bottom, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, m_BackgroundColor);
		grcEndQuads();
	}
}

// PURPOSE: Call this before doing any draw calls.
void spdWorldDebugDraw::BeginDraw()
{
	UpdateBoundingBox();
	Clear();
	grcWorldMtx(M34_IDENTITY);
}

// PURPOSE: Call this after doing all draw calls.
void spdWorldDebugDraw::EndDraw()
{
}


// RETURNS: TRUE if the cursor is currently above this object.
bool spdWorldDebugDraw::IsSelected(const spdAABBFast &aabb) const
{
	Vector3 minPos, maxPos;
	float mouseX = (float) MOUSE.GetX();
	float mouseY = (float) MOUSE.GetY();

	ConvertTo2D(aabb.GetMin(), minPos);
	ConvertTo2D(aabb.GetMax(), maxPos);

	// Ignore objects that are outside the visible area.
	if (minPos.x < m_Left + m_SelectionBorder || maxPos.x > m_Right - m_SelectionBorder ||
		minPos.y < m_Top + m_SelectionBorder || maxPos.y > m_Bottom - m_SelectionBorder)
	{
		return false;
	}

	return (mouseX >= minPos.x && mouseX <= maxPos.x && mouseY >= minPos.y && mouseY <= maxPos.y);
}

// RETURNS: TRUE if the cursor is currently above this object.
bool spdWorldDebugDraw::IsSelected(const spdSphere &sphere) const
{
	Vector3 center;

	float mouseX = (float) MOUSE.GetX();
	float mouseY = (float) MOUSE.GetY();
	float radius;

	ConvertTo2D(sphere.GetCenterVector3(), center);
	ConvertTo2D(sphere.GetRadiusf(), radius);

	// Ignore objects that are outside the visible area.
	if (center.x < m_Left + radius + m_SelectionBorder || center.x > m_Right - radius - m_SelectionBorder ||
		center.y < m_Top + radius + m_SelectionBorder || center.y > m_Bottom - radius - m_SelectionBorder)
	{
		return false;
	}

	Vector3 diff = Vector3(mouseX - center.x, mouseY - center.y, 0.0f);

	return (diff.Mag2() <= radius * radius);
}

void spdWorldDebugDraw::Draw(const spdAABBFast &aabb, const Color32 &color, bool highlightIfSelected, const char *objName) const
{
	Vector3 minPos, maxPos;
	bool selected = false;

	if (!DoesMatchFilter(objName))
	{
		return;
	}

	ConvertTo2D(aabb.GetMin(), minPos);
	ConvertTo2D(aabb.GetMax(), maxPos);

	// Do we want to display any selection info?
	if (highlightIfSelected || objName)
	{
		selected = IsSelected(aabb);
	}

	if (highlightIfSelected && selected)
	{
		Color32 highColor = CreateHighlightColor(color);
		grcBeginQuads(1);
		grcDrawQuadf(minPos.x, minPos.y, maxPos.x, maxPos.y, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, highColor);
		grcEndQuads();
	}

	Vector3 tempPos;
	grcBegin(drawLineStrip,5);
	grcColor(color);
	tempPos = Vector3(minPos.x, minPos.y, 0.0f);
	grcVertex3f(tempPos);
	tempPos = Vector3(maxPos.x, minPos.y, 0.0f);
	grcVertex3f(tempPos);
	tempPos = Vector3(maxPos.x, maxPos.y, 0.0f);
	grcVertex3f(tempPos);
	tempPos = Vector3(minPos.x, maxPos.y, 0.0f);
	grcVertex3f(tempPos);
	tempPos = Vector3(minPos.x, minPos.y, 0.0f);
	grcVertex3f(tempPos);
	grcEnd();

	if (selected && objName)
	{
		Vector3 center = aabb.GetCenter();
		Vector3 center2D;

		ConvertTo2D(center, center2D);
		grcFont::GetCurrent().SetInternalColor(Color32(1.0f, 1.0f, 1.0f, 1.0f));
		grcFont::GetCurrent().Drawf(center2D.x, center2D.y, objName);
	}
}

void spdWorldDebugDraw::Draw(const spdPolygon &polygon, const Color32 &color, bool highlightIfSelected, const char *objName) const
{
	Vector3 pt, center, vecMin, vecMax;
	bool selected = false;

	center = VEC3_ZERO;
	vecMin = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	vecMax = Vector3(FLT_MIN, FLT_MIN, FLT_MIN);


	if (!DoesMatchFilter(objName))
	{
		return;
	}

	int points = polygon.GetNumPoints();

	grcBegin(drawLineStrip, points + 1);
	grcColor(color);

	for (int x=0; x<points; x++)
	{
		Vector3 vertex = polygon.GetPoint(x);

		ConvertTo2D(vertex, pt);
		grcVertex3f(pt);
		center += pt;

		vecMin.Min(vecMin, vertex);
		vecMax.Max(vecMax, vertex);
	}

	ConvertTo2D(polygon.GetPoint(0), pt);
	grcVertex3f(pt);

	grcEnd();

	center /= (float) points;

	// Do we want to display any selection info?
	if (highlightIfSelected || objName)
	{
		spdAABBFast aabb;
		
		aabb.Set(vecMin, vecMax);
		selected = IsSelected(aabb);
	}


	if (selected && objName)
	{
		Vector3 center2D;

		ConvertTo2D(center, center2D);
		grcFont::GetCurrent().SetInternalColor(Color32(1.0f, 1.0f, 1.0f, 1.0f));
		grcFont::GetCurrent().Drawf(center2D.x, center2D.y, objName);
	}
}


void spdWorldDebugDraw::Draw(const spdSphere &sphere, const Color32 &color, bool highlightIfSelected, const char *objName) const
{
	Vector3 centerPos;
	float radius;
	bool selected = false;

	if (!DoesMatchFilter(objName))
	{
		return;
	}

	ConvertTo2D(sphere.GetCenterVector3(), centerPos);
	ConvertTo2D(sphere.GetRadiusf(), radius);

	// Do we want to display any selection info?
	if (highlightIfSelected || objName)
	{
		selected = IsSelected(sphere);
	}

	if (highlightIfSelected && selected)
	{
		Color32 highColor = CreateHighlightColor(color);
		grcColor(highColor);
		grcDrawSphere(radius, centerPos, 16, true, true);
	}

	grcColor(color);
	grcDrawSphere(radius, centerPos, 16, true);
	grcWorldMtx(M34_IDENTITY);

	if (selected && objName)
	{
		grcFont::GetCurrent().SetInternalColor(Color32(1.0f, 1.0f, 1.0f, 1.0f));
		grcFont::GetCurrent().Drawf(centerPos.x, centerPos.y, objName);
	}
}

// RETURNS: TRUE if this object matches the user-defined filter.
bool spdWorldDebugDraw::DoesMatchFilter(const char *name) const
{
	size_t stringLength = strlen(m_SubstringFilter);

	if (stringLength == 0)
	{
		return true;
	}

	if (!name)
	{
		return false;
	}

	// Do a case-insensitive substring search.
	while (*name)
	{
		// Found it.
		if (strnicmp(name, m_SubstringFilter, stringLength) == 0)
		{
			return true;
		}
		
		name++;
	}

	// Substring not found. Empty strings also fail.
	return false;
}


void spdWorldDebugDraw::Draw(const spdShaft &shaft, const Color32 &color, bool /*highlightIfSelected*/, const char * /*objName*/) const
{
//	if (!DoesMatchFilter(objName))
//	{
//		return;
//	}

	// TODO: Bleeh.
	if (m_ShaftTopPlaneOnly)
	{
		grcBegin(drawLineStrip, 5);
		grcColor(color);

		Vector3 tempPos;
		ConvertTo2D(shaft.GetCorners()[0], tempPos);
		grcVertex3f(tempPos);
		ConvertTo2D(shaft.GetCorners()[2], tempPos);
		grcVertex3f(tempPos);
		ConvertTo2D(shaft.GetCorners()[4], tempPos);
		grcVertex3f(tempPos);
		ConvertTo2D(shaft.GetCorners()[6], tempPos);
		grcVertex3f(tempPos);
		ConvertTo2D(shaft.GetCorners()[0], tempPos);
		grcVertex3f(tempPos);
		grcEnd();
	}
	else
	{
		grcBegin(drawLines, 8 * 7);	// 8 * 8 grid, only one side, excluding own elements. (8 * 7) / 2, 2 vertices each = 8 * 7
		grcColor(color);

		for (int x=1; x<8; x++)
		{
			Vector3 pt1 = shaft.GetCorners()[x];
			Vector3 pt1_2D;

			ConvertTo2D(pt1, pt1_2D);

			for (int y=0; y<x; y++)
			{
				Vector3 pt2 = shaft.GetCorners()[y];
				Vector3 pt2_2D;

				ConvertTo2D(pt2, pt2_2D);
				grcVertex3f(pt1_2D);
				grcVertex3f(pt2_2D);
			}
		}

		grcEnd();
	}
}

// PURPOSE: Set the area in 3D space that this object will visualize.
// (Currently, Y is ignored). This will also call UpdateBoundingBox() to apply
// any user-defined zooming or offset.
void spdWorldDebugDraw::SetWorldExtents(const spdAABBFast &aabb)
{
	if (!m_DisableBoundingBoxUpdates)
	{
		m_OriginalBoundingBox = aabb;
		UpdateBoundingBox();
	}

	Vector3 mouseCoords((float) MOUSE.GetX(), (float) MOUSE.GetY(), 0.0f);
	Vector3 worldCoords;

	ConvertTo3D(mouseCoords, worldCoords);

	formatf(m_CoordinateString, sizeof(m_CoordinateString), "%.2f %.2f", worldCoords.x, worldCoords.z);
}

// PURPOSE: Set the area in 3D space that this object will visualize.
// (Currently, Y is ignored). This will also call UpdateBoundingBox() to apply
// any user-defined zooming or offset.
void spdWorldDebugDraw::SetWorldExtents(Vector3::Vector3Param minPos, Vector3::Vector3Param maxPos)
{
	spdAABBFast aabb;
	
	aabb.Set(minPos, maxPos);
	SetWorldExtents(aabb);
}

void spdWorldDebugDraw::UpdateBoundingBox()
{
	Vector3 center = m_OriginalBoundingBox.GetCenter();

	// Apply the offset
	center -= Vector3(m_OffsetX, 0.0f, m_OffsetZ);

	// And grab the modified extents based on the zoom factor.
	Vector3 diagonal = Vector3(m_OriginalBoundingBox.GetWidth() / (2.0f * m_Zoom), 0.0f, m_OriginalBoundingBox.GetDepth() / (2.0f * m_Zoom));
	
	// Now set the final bounding box.
	m_BoundingBox.Set(center - diagonal, center + diagonal);
}

#if __BANK

void spdWorldDebugDraw::AddWidgets(bkBank &bank)
{
	bank.AddColor("Background Color", &m_BackgroundColor);
	bank.AddToggle("Freeze Bounding Box", &m_DisableBoundingBoxUpdates);
	bank.AddToggle("Show Shaft Top Plane Only", &m_ShaftTopPlaneOnly);
	bank.AddSlider("Zoom", &m_Zoom, 0.01f, 2000.0f, 0.01f);
	bank.AddSlider("X Offset", &m_OffsetX, -10000.0f, 10000.0f, 0.01f);
	bank.AddSlider("Z Offset", &m_OffsetZ, -10000.0f, 10000.0f, 0.01f);
	bank.AddSlider("Selection Edge Border", &m_SelectionBorder, 0.0f, 500.0f, 0.01f);
	bank.AddText("Substring Filter", m_SubstringFilter, sizeof(m_SubstringFilter));
	bank.AddText("Mouse Coordinates", m_CoordinateString, sizeof(m_CoordinateString), true);
}

#endif // __BANK



} // namespace rage

#endif // __BANK


