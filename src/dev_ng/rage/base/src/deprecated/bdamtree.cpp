// 
// spatialdata/bdamtree.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "bdamtree.h"

#include <stdio.h>

using namespace rage;

Vector2 spdBdamLocation::sm_Corners[][3] = {
	{Vector2(0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector2(1.0f, 0.0f)},	//DIR_N
	{Vector2(0.5f, 0.5f), Vector2(1.0f, 0.0f), Vector2(1.0f, 1.0f)},	//DIR_E,
	{Vector2(0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector2(0.0f, 1.0f)},	//DIR_S,
	{Vector2(0.5f, 0.5f), Vector2(0.0f, 1.0f), Vector2(0.0f, 0.0f)},	//DIR_W,
	{Vector2(0.5f, 0.0f), Vector2(1.0f, 0.0f), Vector2(0.5f, 0.5f)},	//DIR_NE,
	{Vector2(1.0f, 0.5f), Vector2(0.5f, 0.5f), Vector2(1.0f, 0.0f)},	//DIR_EN,
	{Vector2(1.0f, 0.5f), Vector2(1.0f, 1.0f), Vector2(0.5f, 0.5f)},	//DIR_ES,
	{Vector2(0.5f, 1.0f), Vector2(0.5f, 0.5f), Vector2(1.0f, 1.0f)},	//DIR_SE,
	{Vector2(0.5f, 1.0f), Vector2(0.0f, 1.0f), Vector2(0.5f, 0.5f)},	//DIR_SW,
	{Vector2(0.0f, 0.5f), Vector2(0.5f, 0.5f), Vector2(0.0f, 1.0f)},	//DIR_WS,
	{Vector2(0.0f, 0.5f), Vector2(0.0f, 0.0f), Vector2(0.5f, 0.5f)},	//DIR_WN,
	{Vector2(0.5f, 0.0f), Vector2(0.5f, 0.5f), Vector2(0.0f, 0.0f)}		//DIR_NW,
};

const char* spdBdamLocation::sm_DirectionNames[] = {
	"N", "E", "S", "W",
	"NE", "EN", "ES", "SE",
	"SW", "WS", "WN", "NW"
};

Vector2 spdBdamLocation::sm_BoxCorners[][2] = {
	{Vector2(0.0f, 0.0f), Vector2(1.0f, 0.5f)},	//DIR_N
	{Vector2(0.5f, 0.0f), Vector2(1.0f, 1.0f)},	//DIR_E,
	{Vector2(0.0f, 0.5f), Vector2(1.0f, 1.0f)},	//DIR_S,
	{Vector2(0.0f, 0.0f), Vector2(0.5f, 1.0f)},	//DIR_W,
	{Vector2(0.5f, 0.0f), Vector2(1.0f, 0.5f)},	//DIR_NE,
	{Vector2(0.5f, 0.0f), Vector2(1.0f, 0.5f)},	//DIR_EN,
	{Vector2(0.5f, 0.5f), Vector2(1.0f, 1.0f)},	//DIR_ES,
	{Vector2(0.5f, 0.5f), Vector2(1.0f, 1.0f)},	//DIR_SE,
	{Vector2(0.0f, 0.5f), Vector2(0.5f, 1.0f)},	//DIR_SW,
	{Vector2(0.0f, 0.5f), Vector2(0.5f, 1.0f)},	//DIR_WS,
	{Vector2(0.0f, 0.0f), Vector2(0.5f, 0.5f)},	//DIR_WN,
	{Vector2(0.0f, 0.0f), Vector2(0.5f, 0.5f)}	//DIR_NW,
};

void spdBdamLocation::GetParentLocation(spdBdamLocation& outParent) const
{
	if (m_Level % 2 == 0) {
		Direction dir;
		int row, col;
		if (m_Row % 2 == 0) {
			if (m_Col % 2 == 0) {
				if (m_Dir == DIR_N || m_Dir == DIR_E) {
					dir = DIR_NW;
				}
				else {
					dir = DIR_WN;
				}
				col = m_Col / 2;
			}
			else {
				if (m_Dir == DIR_N || m_Dir == DIR_W) {
					dir = DIR_NE;
				}
				else {
					dir = DIR_EN;
				}
				col = (m_Col - 1) / 2;
			}
			row = m_Row / 2;
		}
		else {
			if (m_Col % 2 == 0) {
				if (m_Dir == DIR_N || m_Dir == DIR_W) {
					dir = DIR_WS;
				}
				else {
					dir = DIR_SW;
				}
				col = m_Col / 2;
			}
			else {
				if (m_Dir == DIR_N || m_Dir == DIR_E) {
					dir = DIR_ES;
				}
				else {
					dir = DIR_SE;
				}
				col = (m_Col - 1) / 2;
			}
			row = (m_Row - 1) / 2;
		}
		outParent.Set(m_Level+1, row, col, dir);
	}
	else {
		switch(m_Dir) {
		case DIR_NE:
		case DIR_NW:
			outParent.Set(m_Level+1, m_Row, m_Col, DIR_N);
			break;
		case DIR_EN:
		case DIR_ES:
			outParent.Set(m_Level+1, m_Row, m_Col, DIR_E);
			break;
		case DIR_SW:
		case DIR_SE:
			outParent.Set(m_Level+1, m_Row, m_Col, DIR_S);
			break;
		case DIR_WN:
		case DIR_WS:
			outParent.Set(m_Level+1, m_Row, m_Col, DIR_W);
			break;
		default:
			Quitf("Invalid direction for this LOD");
			break;
		}
	}
}

void spdBdamLocation::GetChildLocations(spdBdamLocation& left, spdBdamLocation& right) const
{
	if (m_Level % 2 == 0) {
		switch(m_Dir) {
		case DIR_N:
			left.Set(m_Level-1, m_Row, m_Col, DIR_NE);
			right.Set(m_Level-1, m_Row, m_Col, DIR_NW);
			break;
		case DIR_E:
			left.Set(m_Level-1, m_Row, m_Col, DIR_EN);
			right.Set(m_Level-1, m_Row, m_Col, DIR_ES);
			break;
		case DIR_S:
			left.Set(m_Level-1, m_Row, m_Col, DIR_SW);
			right.Set(m_Level-1, m_Row, m_Col, DIR_SE);
			break;
		case DIR_W:
			left.Set(m_Level-1, m_Row, m_Col, DIR_WN);
			right.Set(m_Level-1, m_Row, m_Col, DIR_WS);
			break;
		default:
			Quitf("Invalid direction for this LOD");
			break;
		}
	}
	else {
		switch(m_Dir) {
		case DIR_NE:
			left.Set(m_Level-1, m_Row * 2, m_Col * 2 + 1, DIR_N);
			right.Set(m_Level-1, m_Row * 2, m_Col * 2 + 1, DIR_W);
			break;
		case DIR_EN:
			left.Set(m_Level-1, m_Row * 2, m_Col * 2 + 1, DIR_E);
			right.Set(m_Level-1, m_Row * 2, m_Col * 2 + 1, DIR_S);
			break;
		case DIR_ES:
			left.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2 + 1, DIR_N);
			right.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2 + 1, DIR_E);
			break;
		case DIR_SE:
			left.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2 + 1, DIR_S);
			right.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2 + 1, DIR_W);
			break;
		case DIR_SW:
			left.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2, DIR_E);
			right.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2, DIR_S);
			break;
		case DIR_WS:
			left.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2, DIR_N);
			right.Set(m_Level-1, m_Row * 2 + 1, m_Col * 2, DIR_W);
			break;
		case DIR_WN:
			left.Set(m_Level-1, m_Row * 2, m_Col * 2, DIR_S);
			right.Set(m_Level-1, m_Row * 2, m_Col * 2, DIR_W);
			break;
		case DIR_NW:
			left.Set(m_Level-1, m_Row * 2, m_Col * 2, DIR_N);
			right.Set(m_Level-1, m_Row * 2, m_Col * 2, DIR_E);
			break;
		default:
			Quitf("Invalid direction for this LOD");
			break;
		}
	}
}

void spdBdamLocation::GetCornerLocations(float gridSize, Vector2& pointA, Vector2& pointB, Vector2& pointC) const
{
	int gridMultiplier = 1 << ((m_Level + 1) / 2);
	float gridSizeThisLevel = gridSize * (float)gridMultiplier;

	pointA.Set(sm_Corners[m_Dir][0]);
	pointB.Set(sm_Corners[m_Dir][1]);
	pointC.Set(sm_Corners[m_Dir][2]);

	Vector2 offset((float)m_Col, (float)m_Row);
	pointA.Add(offset);
	pointB.Add(offset);
	pointC.Add(offset);

	pointA.Scale(gridSizeThisLevel);
	pointB.Scale(gridSizeThisLevel);
	pointC.Scale(gridSizeThisLevel);
}

void spdBdamLocation::GetBoxCorners(float gridSize, Vector2& minCorner, Vector2& maxCorner) const
{
	int gridMultiplier = 1 << ((m_Level + 1) / 2);
	float gridSizeThisLevel = gridSize * (float)gridMultiplier;

	minCorner.Set(sm_BoxCorners[m_Dir][0]);
	maxCorner.Set(sm_BoxCorners[m_Dir][1]);

	Vector2 offset((float)m_Col, (float)m_Row);
	minCorner.Add(offset);
	maxCorner.Add(offset);

	minCorner.Scale(gridSizeThisLevel);
	maxCorner.Scale(gridSizeThisLevel);
}

Vector2 spdBdamLocation::GetLongEdgeCenter(float gridSize) const
{
	Vector2 a, b, c;
	GetCornerLocations(gridSize, a, b, c);
	Vector2 center;
	center.Average(b,c);
	return center;
}

void spdBdamLocation::FormatLocationString(char* outString, const char* formatString) const
{
	int outPos = 0;
	for(int inPos = 0; formatString[inPos] != '\0'; ++inPos) {
		if (formatString[inPos] == '%') {
			int newChars = 0;
			switch(formatString[inPos+1]) {
				case 'L':
					newChars = sprintf(outString + outPos, "%d", m_Level);
					outPos += newChars;
					break;
				case 'R':
					newChars = sprintf(outString + outPos, "%d", m_Row);
					outPos += newChars;
					break;
				case 'C':
					newChars = sprintf(outString + outPos, "%d", m_Col);
					outPos += newChars;
					break;
				case 'D':
					newChars = sprintf(outString + outPos, sm_DirectionNames[m_Dir]);
					outPos += newChars;
					break;
				case '%':
					outString[outPos] = '%';
					outPos++;
					break;
				default:
					outString[outPos] = formatString[inPos];
					outPos++;
					inPos--; // back up so the following ++ has no effect
			}
			inPos++;
		}
		else {
			outString[outPos] = formatString[inPos];
			outPos++;
		}
	}
	outString[outPos] = '\0';
}

spdBdamLocation spdBdamLocation::FindLocationContainingPoint(Vector2 point, int lod, float gridSize)
{
	// step 1: find which row, column it's in

	int gridScale = 1 << ((lod + 1) / 2);
	float realGridSize = gridSize * (float)gridScale;

	float xCell = point.x / realGridSize;
	float yCell = point.y / realGridSize;

	float xWhichCell = floorf(xCell);
	float yWhichCell = floorf(yCell);

	float xCellPart = xCell - xWhichCell;
	float yCellPart = yCell - yWhichCell;

	spdBdamLocation loc;
	loc.m_Level = lod;
	loc.m_Col = (int)xWhichCell;
	loc.m_Row = (int)yWhichCell;

	// Find out which diagonal quadrant the point is in

	// Is it S or W?
	if (xCellPart < yCellPart)
	{
		if ((1.0f - xCellPart) < yCellPart)
		{
			loc.m_Dir = DIR_S;
		}
		else
		{
			loc.m_Dir = DIR_W;
		}
	}
	else // N or E
	{
		if ((1.0f - xCellPart) < yCellPart)
		{
			loc.m_Dir = DIR_E;
		}
		else
		{
			loc.m_Dir = DIR_N;
		}
	}

	// For odd LODs, find out which side of the quadrant the point is in
	if (lod % 2 == 1)
	{
		switch(loc.m_Dir)
		{
		case DIR_N:	loc.m_Dir = (xCellPart < 0.5f) ? DIR_NW : DIR_NE; break;
		case DIR_S: loc.m_Dir = (xCellPart < 0.5f) ? DIR_SW : DIR_SE; break;
		case DIR_E: loc.m_Dir = (yCellPart < 0.5f) ? DIR_EN : DIR_ES; break;
		case DIR_W: loc.m_Dir = (yCellPart < 0.5f) ? DIR_WN : DIR_WS; break;
		default:
			Errorf("Invalid direction: %s", spdBdamLocation::sm_DirectionNames[loc.m_Dir]);
		}
	}

	return loc;
}

bool spdBdamTreelessLocationIterator::MoveNext()
{

	// Move up until we've moved along a left link, then move right
	spdBdamLocation up, left, right;
	while(!(m_CurrLocation == m_Root)) {
		m_CurrLocation.GetParentLocation(up);
		up.GetChildLocations(left, right);
		if (m_CurrLocation == left) {
			m_CurrLocation = right;
			// now move left as far as possible
			while(m_CurrLocation.m_Level > m_MaxLod && m_ShouldSplitCb(m_CurrLocation)){
				m_CurrLocation.GetChildLocations(left, right);
				m_CurrLocation = left;
			}
			return true;
		}
		else {
			m_CurrLocation = up;
		}
	}
	return false;
}

void spdBdamTreelessLocationIterator::Init(spdBdamLocation root, atFunctor1<bool, const spdBdamLocation&> splitcb) {
	m_MaxLod = 0;
	m_Root = root;
	m_ShouldSplitCb = splitcb;
	
	// find starting point
	m_CurrLocation = root;
	while(m_ShouldSplitCb(m_CurrLocation)) {
		spdBdamLocation left, right;
		m_CurrLocation.GetChildLocations(left, right);
		m_CurrLocation = left;
	}
}


#if 0

void spdBdamNode::Draw() {
	grcState::Default();
	grcLightState::SetEnabled(false);
	grcState::Default();

	Color32 baseColor;
	//baseColor = LevelColors[m_Location.m_Level];
	//baseColor.SetAlpha(128);
	//grcColor(baseColor);

	/*
	m_Mesh->DrawFast();
	*/

	Vector2 a,b,c;
	m_Location.GetCornerLocations(128.0f, a,b,c);

	Vector3 edgeA, edgeB, edgeC;
	edgeA.Set(a.x, 0.0f, a.y);
	edgeB.Set(b.x, 0.0f, b.y);
	edgeC.Set(c.x, 0.0f, c.y);

	Color32 thisColor = LevelColors[m_Location.m_Level];
	thisColor.SetAlpha(128);
	Color32 nextColor = LevelColors[m_Location.m_Level+1];
	nextColor.SetAlpha(128);

	Color32 avgColor;//.) = thisColor; //thisColor * Color32(128, 128, 128) + nextColor * Color32(128, 128, 128);

	avgColor.Setf(
		0.5f * (thisColor.GetRedf() + nextColor.GetRedf()),
		0.5f * (thisColor.GetGreenf() + nextColor.GetGreenf()),
		0.5f * (thisColor.GetBluef() + nextColor.GetBluef()),
		thisColor.GetAlphaf());

	Vector3 edgeBC;
	edgeBC.Average(edgeB, edgeC);

	grcBegin(drawTriFan, 4);

	grcColor(nextColor);

	grcVertex3f(edgeBC);

	grcColor(avgColor);
	grcVertex3f(edgeB);

	grcColor(thisColor);
	grcVertex3f(edgeA);

	grcColor(avgColor);
	grcVertex3f(edgeC); 
	//		grcVertex3f(edgeA);
	grcEnd();

	Vector3 midA, midB, midC;
	midA.Average(edgeB, edgeC);
	midB.Average(edgeA, edgeC);
	midC.Average(edgeA, edgeB);

	Vector3 innerA, innerB, innerC;
	innerA.Average(midB, midC);
	innerB.Average(midA, midC);
	innerC.Average(midA, midB);

	midA.Lerp(0.2f, edgeA, innerA);
	midB.Lerp(0.2f, edgeB, innerB);
	midC.Lerp(0.2f, edgeC, innerC);

	grcColor(thisColor);

	midA.y = midB.y = midC.y = 0.2f;

	grcBegin(drawLineStrip, 4);
	grcVertex3f(midA);
	grcVertex3f(midB);
	grcVertex3f(midC);
	grcVertex3f(midA);
	grcEnd();


	Vector3 circle = m_Cylinder.GetCenter();
	grcColor3f(1.0f, 1.0f, 0.0);
	//	grcDrawCircle(m_Cylinder.GetRadius(), circle, Vector3(1.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f), 16);
}

void spdBdamNode::DrawLevel(int level)
{
	if (level == m_Location.m_Level) {
		Draw();
	}
	else if (level < m_Location.m_Level) {
		if (m_Left) {
			m_Left->DrawLevel(level);
		}
		if (m_Right) {
			m_Right->DrawLevel(level);
		}
	}
}
#endif

#if SPLITLIST_TRAVERSAL

bool spdBdamNode::RemoveParent(dlList& drawList)
{
	bool bRet = false;
	if( m_Parent )
	{
		if( m_Parent->RemoveParent(drawList) )
			bRet = true;

		dlList* parent = drawList.Find(m_Parent);
		if( parent )
		{
			delete parent;
			bRet = true;
		}
	}
	return bRet;
}

void spdBdamNode::Split(dlList& drawList, dlList& splitList, const Vector3& point)
{

	if( (m_Left && m_Right) && m_Cylinder.Contains(point) )
	{
		splitList.Add(this);
		m_Left->Split(drawList, splitList, point);
		m_Right->Split(drawList, splitList, point);
	}
	else
	{
		// No split, draw this tri
		if( !drawList.Find(this) )
			drawList.Add(this);
	}
}

void spdBdamNode::ForceSplit(const Vector2& split, dlList& drawList, dlList& splitList, const Vector3& point, spdBdamNode* exclude)
{
	if( split == m_SplitVtx && this != exclude )
	{
		// Remove this mesh from the draw list
		dlList* pIter = drawList.Find(this);
		if( pIter )
		{
			// If this triangle is at the desired level, split its children
			delete pIter;
		}
		else
		{
			if (m_Parent) {
				// Force a split of the parent
				m_Parent->ForceSplit(m_Parent->m_SplitVtx, drawList, splitList, point, NULL);
				splitList.Add(m_Parent);
			}
		}
		if( !drawList.Find(m_Left) )
			drawList.Add(m_Left);
		if( !drawList.Find(m_Right) )
			drawList.Add(m_Right);
	}
	else if( m_Left && m_Right )
	{
		m_Left->ForceSplit(split, drawList, splitList, point, exclude);
		m_Right->ForceSplit(split, drawList, splitList, point, exclude);
	}
}

#endif
