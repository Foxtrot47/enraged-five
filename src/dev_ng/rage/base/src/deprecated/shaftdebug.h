// 
// spatialdata/shaftdebug.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SPATIALDATA_SHAFTDEBUG_H
#define SPATIALDATA_SHAFTDEBUG_H

#include "spatialdata/shaft.h"


namespace rage
{

class bkBank;
class grcViewport;

#if __DEV

/* PURPOSE: A simple debugging environment to analyze the shafts of various
 * cameras, like the shadow map cameras and the primary camera.
 *
 * The application can have the debugging environment remember any shaft that
 * was used during a frame by calling StoreDebugShaft() with an index that
 * identifies the shaft (an example would be 0 through 3 for the four shadow
 * maps, 4 for the shadow collector, 5 for the main camera, although the
 * choice of an index is up to the user). spdShaftDebug::MaxDebugShafts
 * dictates the amount of shafts (and limit to the index).
 *
 * During the render phase, the application should call DebugDraw() to
 * trigger any debug drawing. Finally, AddWidgets() should be called during
 * initialization to enable all debugging widgets.
 *
 * spdShaftDebug is a singleton that call be initialized with the
 * static function spdShaftDebug::InitClass.
 *
 * If a non-__DEV environment, all functions (including InitClass()) are
 * stubbed out. It is perfectly legal to call any function, even members
 * like spdShaftDebug::StoreDebugShaft(), in non-__DEV builds. All stubs
 * are inlined and will not result in any actual code.
 */
class spdShaftDebug
{
	enum
	{
		MaxDebugShafts = 24
	};

public:
	spdShaftDebug();

	const spdShaft &GetShadowShaft(int x)			{ return m_Shafts[x]; }

	void StoreDebugShaft(int index, const spdShaft &shaft);
	void StoreDebugShaft(int index, const grcViewport &viewport);

	void SetLockShadowShafts(bool lockShadowShafts)	{ m_LockShadowShafts = lockShadowShafts; }

	// RETURNS: The shaft being debugged right now, or -1 if no shaft is debugged.
	int GetDebugShaftIndex() const					{ return m_ShaftIndex; }

	void DebugDraw() const;

	static void InitClass();

	static void ShutdownClass();

	static spdShaftDebug &GetInstance() { FastAssert(sm_Instance); return *sm_Instance; }

	static bool IsInstantiated()		{ return (sm_Instance != NULL); }





#if __BANK
	void AddWidgets(bkBank &bank);
#endif // __BANK


private:
	atFixedArray<spdShaft, MaxDebugShafts>	m_Shafts;	// All debug shafts
	int					m_ShaftIndex;				// The current shaft being drawn, -1 to draw all of them
	bool				m_DrawShadowShafts;			// If true, the active shadow shaft will be rendered
	bool				m_LockShadowShafts;			// If true, the shafts will be locked and no longer updated
	bool				m_UseSolidShafts;			// If true, all shafts will be rendered with solid polygons
	bool				m_DepthTest;				// If true, the shafts will be drawn with the depth test enabled
	bool				m_ShaftValid[MaxDebugShafts];	// If true, the shaft has been set at least once.

	static spdShaftDebug *sm_Instance;
};


#else // __DEV

class spdShaftDebug
{
public:
	inline spdShaftDebug()			{};
	inline void StoreDebugShaft(int /*index*/, const spdShaft & /*shaft*/) {};
	inline void SetLockShadowShafts(bool /*lockShadowShafts*/)	{};
	inline void DebugDraw() const {};
	inline static void InitClass() {};
	inline static void ShutdownClass() {};
	inline static spdShaftDebug &GetInstance()	{ return *sm_Instance; }
	inline static bool IsInstantiated()			{ return false; }
	inline int GetDebugShaftIndex() const		{ return -1; }

private:
	static spdShaftDebug *sm_Instance;
};

#endif // __DEV


} //	namespace rage

#endif // SPATIALDATA_SHAFTDEBUG_H

