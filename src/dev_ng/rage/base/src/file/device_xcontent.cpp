//
// filename:	device_xcontent.h
// description:	
//
#include "device_xcontent.h"
#include "system/memops.h"

#if __XENON

#include "system/xtl.h"

namespace rage {

CompileTimeAssert(sizeof(XCONTENT_DATA) == XCONTENT_DATA_SIZE);

bool fiDeviceXContent::Init(s32 userIndex, XCONTENT_DATA* pContentData)
{
	m_userIndex = userIndex;
	sysMemCpy(m_contentData, pContentData, sizeof(m_contentData));
	return true;
}

bool fiDeviceXContent::MountAs(const char* pName)
{
	if( fiDevice::Mount( pName, *this, false ) )
	{
		const char* pColon = strchr(pName, ':');
		s32 len;
		Assert(pColon);
		len = pColon - pName;
		Assert(len < sizeof(m_mountName));

		strncpy(m_mountName, pName, len);
		m_mountName[len] = '\0';

		if(XContentCreate(m_userIndex, m_mountName, (XCONTENT_DATA*) &m_contentData, XCONTENTFLAG_OPENEXISTING, NULL, NULL, NULL) == ERROR_SUCCESS)
		{
			return true;
		}
	}
	return false;
}

inline void fiDeviceXContent::Shutdown()
{
	XContentClose(m_mountName, NULL);
}


}	// namespace rage

#endif
