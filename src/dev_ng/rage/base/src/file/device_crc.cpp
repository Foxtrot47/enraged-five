// 
// file/device_crc.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "device_crc.h"

#include "file/asset.h"
#include "string/string.h"
#include "string/stringhash.h"


#define START_CRC_VALUE		(0x00feeb1e)

namespace rage {
	
	atBinaryMap<int,atFinalHashString> fiDeviceCrc::sm_filenameCrcMap;
	u32 fiDeviceCrc::ms_crcAccumulator = START_CRC_VALUE;
	bool fiDeviceCrc::ms_bCRCTableCheckFailed = false;

	fiHandle fiDeviceCrc::Open(const char *filename,bool readOnly) const
	{
		Assertf(readOnly, "fiDeviceCrc only support read only");
		Assertf(m_crc == 0, "fiDeviceCrc only supports one open file at a time");
		m_filenameHash = filename;
		return fiDeviceRelative::Open(filename, readOnly);
	}

	int fiDeviceCrc::Read(fiHandle handle,void *outBuffer,int bufferSize) const
	{
		int numBytes = fiDeviceRelative::Read(handle, outBuffer, bufferSize);
		m_crc = atPartialDataHash((char*)outBuffer, numBytes, m_crc);
		return numBytes;
	}

	int fiDeviceCrc::Close(fiHandle handle) const
	{
		if(!sm_filenameCrcMap.Has(m_filenameHash))
		{
			sm_filenameCrcMap.Insert(m_filenameHash,m_crc);
			ms_crcAccumulator ^= m_crc;
		}
		m_crc = 0;
		return fiDeviceRelative::Close(handle);
	}

	bool fiDeviceCrc::IsCRCTableConsistent(void)
	{
		u32 XorCRCs = START_CRC_VALUE;

		sm_filenameCrcMap.FinishInsertion();

		atBinaryMap<int,atFinalHashString>::Iterator it = sm_filenameCrcMap.Begin();
		atBinaryMap<int,atFinalHashString>::Iterator  stop = sm_filenameCrcMap.End();

		for(; stop != it; ++it)
		{
			const atFinalHashString key = it.GetKey();
			XorCRCs ^= (*sm_filenameCrcMap.SafeGet(key));
		}

		if (XorCRCs == ms_crcAccumulator)
		{
			return(true);
		}
		else
		{
			ms_bCRCTableCheckFailed = true;
			return(false);
		}
	}
}		// namespace rage
