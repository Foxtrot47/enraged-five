// 
// file/ftpserver.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_FTPSERVER_H
#define FILE_FTPSERVER_H

#if !__FINAL

namespace rage {

/*	PURPOSE:	Run the ftp server loop forever.
	PARAMS:		unused - Not currently used, ignored
	NOTES:		Designed to be in a separate thread, or a standalone server.
*/

void fiFtpServer(void* unused);

/*	PURPOSE:	Launch a thread that runs fiFtpServer */
void fiLaunchFtpServerThread();

}

#endif

#endif
