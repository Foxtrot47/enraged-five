// 
// file/savegame_durango.winrt.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_DURANGO
#include "savegame.h"

// local headers
#include "asset.h"
#include "limits.h"
#include "stream.h"
#include "winrtbuffer.winrt.h"

// rage headers
#include "atl/array.h"
#include "math/amath.h"
#include "string/unicode.h"
#include "string/stringhash.h"
#include "system/criticalsection.h"
#include "system/userlist_durango.winrt.h"


// This must be included *Before* WinRT headers but we *DO NOT* want it to
// Affect our normal headers for minimal impact.
#include "system/new_winrt.h"

// WinRT headers
// The following warnings cause issue with the WinRT headers.
#pragma warning(push)
#pragma warning(disable: 4668) // Undefined preprocessor used in #if.
#include <collection.h>
#include <xdk.h>

// Reset warnings that WinRT cause.
#pragma warning(pop)


namespace rage
{
RAGE_DEFINE_CHANNEL(savegamedurango);

#define sgdAssertf(cond,fmt,...)	RAGE_ASSERTF(savegamedurango,cond,fmt,##__VA_ARGS__)
#define sgdVerifyf(cond,fmt,...)	RAGE_VERIFYF(savegamedurango,cond,fmt,##__VA_ARGS__)
#define sgdDisplayf(fmt,...)		RAGE_DISPLAYF(savegamedurango, fmt, ##__VA_ARGS__)
#define sgdDebugf1(fmt, ...)		RAGE_DEBUGF1(savegamedurango, fmt, ##__VA_ARGS__)
#define sgdDebugf2(fmt, ...)		RAGE_DEBUGF2(savegamedurango, fmt, ##__VA_ARGS__)
#define sgdDebugf3(fmt, ...)		RAGE_DEBUGF3(savegamedurango, fmt, ##__VA_ARGS__)
#define sgdWarningf(fmt, ...)		RAGE_WARNINGF(savegamedurango, fmt, ##__VA_ARGS__)
#define sgdErrorf(fmt, ...)			RAGE_ERRORF(savegamedurango, fmt, ##__VA_ARGS__)

#if _XDK_VER >= 9586
// PURPOSE: Wraps the various WinRT types into a seperate Durango NameSpace (dns).
// NOTES:	This is because Microsoft have a habit of moving namespaces around, this way we can fix this in one place.
//			It also means we do not need to type out the excessively long names (especially when template are used such
//			as IVectorView.
namespace dns
{
	using Platform::Collections::Map;
	using Platform::Collections::Vector;
	using Windows::Foundation::AsyncStatus;
	using Windows::Foundation::AsyncOperationCompletedHandler;
	using Windows::Foundation::AsyncActionCompletedHandler;
	using Windows::Foundation::IAsyncAction;
	using Windows::Foundation::IAsyncOperation;
	using Windows::Foundation::Collections::IIterator;
	using Windows::Foundation::Collections::IMapView;
	using Windows::Foundation::Collections::IVectorView;
	using Windows::Storage::Streams::Buffer;
	using Windows::Storage::Streams::IBuffer;
	using Windows::Xbox::Storage::ContainerInfo;
	using Windows::Xbox::Storage::ConnectedStorageSpace;
	using Windows::Xbox::Storage::ContainerInfoQueryResult;
	using Windows::Xbox::Storage::ConnectedStorageContainer;
	using Windows::Xbox::System::User;
}
#endif // _XDK_VER >= 9586
	
using namespace fiSaveGameState;

// Uses the unicode converter
#define UTF8_TO_UTF16(x) reinterpret_cast<const wchar_t* const>(UTF8_TO_WIDE(x))
#define UTF16_TO_UTF8(x) WIDE_TO_UTF8(reinterpret_cast<const char16* const>(x))

//////////////////////////////////////////////////////////////////////////
// Data Save Type
//////////////////////////////////////////////////////////////////////////
//
// Used to identify which piece of information we are saving!
// 
//////////////////////////////////////////////////////////////////////////

#define SAVE_GAME_DATA		L"Save Game Data"
#define SAVE_GAME_HEADER	L"Save Game Header"

#if _XDK_VER >= 9586
//////////////////////////////////////////////////////////////////////////
// DurangoProfile
//////////////////////////////////////////////////////////////////////////
//
// Ties a profile to save files (containers and blobs on Durango).
//
//////////////////////////////////////////////////////////////////////////

// PURPOSE: Internal class to store user information for save/loading.
class DurangoProfile
{
public:
	DurangoProfile();
	~DurangoProfile();

	bool BeginEnumeration(u32 contentType, fiSaveGame::Content* outContent, int maxCount);
	bool CheckEnumeration(int &outCount);
	void EndEnumeration();

	bool BeginSave(u32 contentType, const char16 *contentName, const char *filename, const void* saveData, u32 saveSize, bool overwrite);
	bool CheckSave(bool& outIsValid, bool& fileExists);
	void EndSave();



	bool BeginGetFileSize(const char* filename, bool bCalculateSizeOnDisk);
	bool CheckGetFileSize( u64 &fileSize);
	void EndGetFileSize();

	bool BeginLoad(u32 contentType,u32 deviceId,const char* filename,void* loadData,u32 loadSize);
	bool CheckLoad(bool &outIsValid,u32 &loadSize);
	void EndLoad();

	bool BeginDelete(u32 contentType, const char *filename);
	bool CheckDelete();
	void EndDelete();

	void SetStateToIdle(){m_State = IDLE;}
	fiSaveGameState::State GetState() const;

	void SetUser(dns::User^ user);
	dns::User^ GetUser();

// 	bool BeginContentVerify(u32 contentType,u32 deviceId,const char *filename);
// 	bool CheckContentVerify(bool &verified);
// 	void EndContentVerify();
// 
// #if HACK_GTA4
// 	bool BeginFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType, const char *filename, u32 saveSize);
// 	bool CheckFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType, int &ExtraSpaceRequired, int &TotalHDDFreeSizeKB);
// 	void EndFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType);
// #else	//	HACK_GTA4
// 	bool BeginFreeSpaceCheck(const char *filename,u32 saveSize);
// 	bool CheckFreeSpaceCheck(int &ExtraSpaceRequired);
// 	void EndFreeSpaceCheck();
// #endif	//	HACK_GTA4
// 
// 	bool BeginGetCreator(u32 contentType,u32 deviceId,const char *filename);
// 	bool CheckGetCreator(bool &bIsCreator);
// 	void EndGetCreator();
// 
// 	u64 GetTotalAvailableSpace();
// 	bool GetDeviceData(u32 deviceId, fiSaveGame::Device *outDevice);
// 	bool GetCurrentDeviceData(fiSaveGame::Device *outDevice);
// 	u64 CalculateDataSizeOnDisk(u32 dataSize);
// 	bool IsDeviceValid(u32 deviceId);
// 	bool IsCurrentDeviceValid();
// 	bool IsStorageDeviceChanged();
// 	bool IsStorageDeviceRemoved();
// 	void FillDevice();
// 
// 	fiSaveGame::Errors GetError();
// 	bool HasFileBeenAccessed(const char* filename);
// 	fiSaveGameState::State GetState() const { return m_State; }
// 
// 	bool CompareTimestamps(HANDLE hFile);

private:
	// PURPOSE: Indicates that a task should be and is ready to start.
	bool ShouldStartTask() const;

	// PURPOSE:	Marks a task as started.
	void SetTaskAsStarted();

	// PURPOSE:	Sets-up a task to be started.
	// PARAMS:	taskState - the state to be in on success.
	// RETURNS:	true if the task was setup correctly.s
	bool SetupTask(fiSaveGameState::State taskState);

	// PURPOSE: Cleans-up a previous task.
	// PARAMS:	taskState - the state the task should currently be in.
	// NOTES:	taskState is passed in to ensure that a task does not
	//			clean up another task.
	void CleanupTask(fiSaveGameState::State taskState);

	// PURPOSE:	Starts the enumeration task.
	bool StartEnumeration();

	// PURPOSE:	Starts the save task.
	bool StartSave();

	// PURPOSE: Starts getting the file size task.
	bool StartGetFileSize();

	// PURPOSE: Starts the load task.
	bool StartLoad();

	// PURPOSE: Start the delete task.
	bool StartDelete();

	// WinRT callback event handlers.
	void OnGetContainerNamesComplete(dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ opertation, dns::AsyncStatus status);
	void OnIterateFileHeaderReadComplete(dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status);
	void IterateFileHeaders();

	void OnSaveGameComplete(dns::IAsyncAction^ action, dns::AsyncStatus status);


	void OnGetFileInfoComplete(dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ action, dns::AsyncStatus status);
	void OnReadHeaderForFileSize(dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status);

	void OnSaveGameDataReadComplete(dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status);


	void OnSaveGameDelete(dns::IAsyncAction^ action, dns::AsyncStatus status);

	void OnGetUserStorageSpaceComplete(dns::IAsyncOperation<dns::ConnectedStorageSpace^>^ operation, dns::AsyncStatus status);

	// PURPOSE: Retrieves the User's Connected storage space info.
	void RetrieveConnectedStorageSpaceInfo();

	// PURPOSE: The save/load state.
	fiSaveGameState::State m_State;

	// PURPOSE: The information regarding a User's connected storage space.
	dns::ConnectedStorageSpace^ m_StorageSpace;

	// PURPOSE:	The state the user's storage space is in.
	enum StorageSpaceState
	{
		// PURPOSE:	The storage space has not been requested to load.
		NOT_REQUESTED,

		// PURPOSE: The storage space is currently loading.
		LOADING,

		// PURPOSE:	The storage space load request has been completed.
		LOAD_SUCCESSFUL,

		// PURPOSE:	The storage space failed to load.
		LOAD_FAILED,
	};

	// PURPOSE: The state of the user's storage space load request.
	StorageSpaceState m_StorageSpaceState;

	// PURPOSE:	Starts a task in a check task callback if the user's storage space is not loaded at the beginning of the task.
	// NOTES:	It is possible that the users storage space (m_StorageSpace) has not been
	//			retrieved by the time we need it. If this happens, then this flag is set to
	//			indicate that the current task needs to be started once the user's storage
	//			space information has been retrieved.
	bool		m_StartTask;
	
	// PURPOSE: The user info.
	dns::User^ m_User;

	// PURPOSE:	The list of container info.
	// NOTES:	This will be filled and used asynchronously!
	dns::IIterator<dns::ContainerInfo>^ m_FileList;

	// PURPOSE:	A pointer to an array of content to be filled in by the file enumerator.
	fiSaveGame::Content*	m_Content;

	// PURPOSE:	The maximum number of content elements in the m_Content array!
	int						m_MaxContent;

	// PURPOSE:	The number of content elements in the m_Content array!
	int						m_ContentCount;

	// PURPOSE:	The name of the content.
	const char16*			m_ContentName;

	// PURPOSE: The filename to use for saving and loading.
	const char*				m_Filename;

	// PURPOSE:	The data to be saved.
	const void*				m_SaveData;

	// PURPOSE:	The load data buffer.
	void*					m_LoadData;

	// PURPOSE: The size of the save data.
	u32						m_DataSize;


	// PURPOSE:	Save game header.
	struct SaveDataHeader
	{
		// PURPOSE:	Constructor.
		SaveDataHeader();

		// PURPOSE:	Constants.
		enum
		{
			// PURPOSE:	The maximum number chars in the display name.
			MAX_DISPLAY_NAME = 128,

			// PURPOSE: Expected version number.
			EXPECTED_HEADER_VERSION = 1,
		};

		// PURPOSE: The version number.
		u32 m_Version;

		// PURPOSE: The size of the data.
		u32	m_DataSize;

		// PURPOSE: The hash of the data to detect tampering.
		u32 m_DataHash;

		// PURPOSE: The hash of the header to detect tampering.
		u32 m_HeaderHash;

		// PURPOSE:	The display name.
		// NOTES:	This should always be the last member variable!
		char16 m_DisplayName [MAX_DISPLAY_NAME];

		// PURPOSE: Clears the header.
		void Clear();

		// PURPOSE: Generates the hash of this header.
		// RETURNS: The hash of the header.
		u32 GenerateHeaderHash();
	};

	mutable sysCriticalSectionToken m_Cs;
};

DurangoProfile::DurangoProfile()
	: m_State(IDLE)
	, m_StorageSpace(nullptr)
	, m_StorageSpaceState(NOT_REQUESTED)
	, m_StartTask(false)
	, m_User(nullptr)
	, m_FileList(nullptr)
	, m_Content(nullptr)
	, m_MaxContent(0)
	, m_ContentCount(0)
	, m_ContentName(nullptr)
	, m_Filename(nullptr)
	, m_SaveData(nullptr)
	, m_LoadData(nullptr)
	, m_DataSize(0u)
	, m_Cs()
{}

DurangoProfile::~DurangoProfile()
{}

bool DurangoProfile::ShouldStartTask() const
{
	sysCriticalSection lock(m_Cs);
	const bool result = m_StartTask && m_StorageSpaceState != LOADING;

	sgdDebugf1("DurangoProfile::ShouldStartTask returned %s, m_StartTask %d, m_StorageSpaceState %d",
		(result) ? "True" : "False",
		m_StartTask,
		m_StorageSpaceState );

	return result;
}

void DurangoProfile::SetTaskAsStarted()
{
	sysCriticalSection lock(m_Cs);
	sgdDebugf1("DurangoProfile::SetTaskAsStarted m_StartTask %s", (m_StartTask) ? "True" : "False");
	m_StartTask = false;
}

bool DurangoProfile::SetupTask(fiSaveGameState::State taskState)
{
	sgdDebugf1("DurangoProfile::SetupTask %d", taskState);

	bool success;

	// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
	// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
	{
		sysCriticalSection lock(m_Cs);

		if (m_State != IDLE && m_State != HAD_ERROR)
		{
			sgdWarningf("Cannot begin savegame task, operation already in progress");
			success = false;
		}
		else
		{
			sgdAssertf(m_Content == nullptr, "A previous enumeration task is in progress or failed to clean up correctly!");
			sgdAssertf(m_MaxContent == 0u, "A previous enumeration task is in progress or failed to clean up correctly!");
			sgdAssertf(m_ContentCount == 0u, "A previous enumeration task is in progress or failed to clean up correctly!");
			sgdAssertf(m_ContentName == nullptr, "A previous save task is in progress or failed to clean up correctly!");
			sgdAssertf(m_Filename == nullptr, "A previous task is in progress or failed to clean up correctly!");
			sgdAssertf(m_LoadData == nullptr, "A previous load task is in progress or failed to clean up correctly!");
			sgdAssertf(m_SaveData == nullptr, "A previous save task is in progress or failed to clean up correctly!");
			sgdAssertf(m_DataSize == 0u, "A previous task is in progress or failed to clean up correctly!");

			if(m_StorageSpaceState == LOADING || m_StorageSpaceState == LOAD_SUCCESSFUL)
			{
				sgdDebugf1("DurangoProfile::SetupTask Successful");
				success		= true;
				m_State		= taskState;
				m_StartTask	= true;
			}
			else
			{
				sgdDebugf1("DurangoProfile::SetupTask Unuccessful");
				success = false;
			}
		}
	}

	return success;
}

void DurangoProfile::CleanupTask(fiSaveGameState::State taskState)
{
	sysCriticalSection lock(m_Cs);
	sgdDebugf1("DurangoProfile::CleanupTask %d, Old State %d", taskState, m_State);
	if(sgdVerifyf(m_State == HAD_ERROR || m_State == taskState || m_State == IDLE, "Cannot clean-up current task, expected %d, got %d!", taskState, m_State))
	{
		m_State		   = IDLE;
		m_Content	   = nullptr;
		m_MaxContent   = 0u;
		m_ContentCount = 0u;
		m_ContentName  = nullptr;
		m_Filename	   = nullptr;
		m_SaveData	   = nullptr;
		m_LoadData	   = nullptr;
		m_DataSize	   = 0u;
	}
}

bool DurangoProfile::BeginEnumeration( u32  UNUSED_PARAM(contentType), fiSaveGame::Content *outContent, int maxCount )
{
	sgdDebugf1("DurangoProfile::BeginEnumeration");
	bool success = SetupTask(ENUMERATING_CONTENT);

	if(success)
	{
		sysCriticalSection lock(m_Cs);
		m_Content    = outContent;
		m_MaxContent = maxCount;
	}

	if(success && ShouldStartTask())
	{
		success = StartEnumeration();
		sgdDebugf1("DurangoProfile::BeginEnumeration - starting now");
	}
	else
	{
		sgdDebugf1("DurangoProfile::BeginEnumeration - failed or postponed until update");
	}

	return success;
}

bool DurangoProfile::StartEnumeration()
{
	sgdDebugf1("DurangoProfile::StartEnumeration");
	SetTaskAsStarted();

	bool success = false;
	try
	{
		dns::ContainerInfoQueryResult^ result = nullptr;
		// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
		// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
		{
			sysCriticalSection lock(m_Cs);

			if (m_State != ENUMERATING_CONTENT)
			{
				sgdWarningf("Cannot begin enumeration, operation already in progress");
				return false;
			}

			if(m_StorageSpace != nullptr)
			{
				m_FileList = nullptr;
				m_ContentCount = 0;

				result = m_StorageSpace->CreateContainerInfoQuery(nullptr);
			}
			else
			{
				sgdDebugf1("DurangoProfile::StartEnumeration m_StorageSpace was NULL");
			}
		}

		if(sgdVerifyf(result != nullptr, "DurangoProfile::BeginEnumeration Failed!"))
		{
			dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ asyncOp = result->GetContainerInfoAsync();

			if(sgdVerifyf(asyncOp != nullptr, "DurangoProfile::BeginEnumeration Failed!"))
			{
				asyncOp->Completed = ref new dns::AsyncOperationCompletedHandler<dns::IVectorView<dns::ContainerInfo>^>(
					[this](dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ opertation, dns::AsyncStatus status)
				{
					this->OnGetContainerNamesComplete(opertation, status);
				}
				);

				success = true;
			}
		}

		if(success == false)
		{
			sysCriticalSection lock(m_Cs);
			sgdErrorf("DurangoProfile::StartEnumeration Failed");
			m_State = HAD_ERROR;
		}
	}
	catch (Platform::Exception^ e)
	{
		sysCriticalSection lock(m_Cs);
		sgdAssertf(false, "DurangoProfile::BeginEnumeration Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");
		m_State = HAD_ERROR;
	}

	return success;
}

bool DurangoProfile::CheckEnumeration( int &outCount )
{
	sgdDebugf1("DurangoProfile::CheckEnumeration");
	if(ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::CheckEnumeration Starting postponed enumeration");
		StartEnumeration();
	}

	bool success = false;
	sysCriticalSection lock(m_Cs);
	if(m_State == HAD_ERROR)
	{
		sgdErrorf("DurangoProfile::CheckEnumeration Failed");
		outCount = 0;
		success = true;
	}
	else if(m_State == ENUMERATED_CONTENT)
	{
		sgdDebugf1("DurangoProfile::StartEnumeration Finished");
		outCount = m_ContentCount;
		success = true;
	}

	return success;
}

void DurangoProfile::EndEnumeration()
{
	sgdDebugf1("DurangoProfile::EndEnumeration");
	CleanupTask(ENUMERATED_CONTENT);
}

void DurangoProfile::IterateFileHeaders()
{
	sgdDebugf1("DurangoProfile::IterateFileHeaders");

	// NOTE: As this function is called internally from code surrounded in a try/catch, we do not wrap it here!
	dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ asyncOp = nullptr;

	// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
	// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
	{
		sysCriticalSection lock(m_Cs);

		if(m_StorageSpace != nullptr)
		{
			USES_CONVERSION;

			safecpy(m_Content[m_ContentCount].Filename, UTF16_TO_UTF8(m_FileList->Current.Name->Data()));

			sgdDebugf1("DurangoProfile::IterateFileHeaders File - %s", m_Filename);
			dns::ConnectedStorageContainer^ saveGame = m_StorageSpace->CreateContainer(m_FileList->Current.Name);

			dns::Vector<Platform::String^>^ blobsToRead = ref new dns::Vector<Platform::String^>();
			blobsToRead->Append(SAVE_GAME_HEADER);

			asyncOp = saveGame->GetAsync(blobsToRead);
		}
		else
		{
			sgdErrorf("DurangoProfile::StartEnumeration m_StorageSpace is NULL");
		}
	}

	if(sgdVerifyf(asyncOp != nullptr, "Error iterating file headers!"))
	{
		asyncOp->Completed = ref new dns::AsyncOperationCompletedHandler<dns::IMapView<Platform::String^, dns::IBuffer^>^>(
			[this]( dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status )
		{
			this->OnIterateFileHeaderReadComplete(operation, status);
		}
		);
	}
	else
	{
		sysCriticalSection lock(m_Cs);
		m_State = HAD_ERROR;
	}
}

void DurangoProfile::OnGetContainerNamesComplete(dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ opertation, dns::AsyncStatus status)
{
	sgdDebugf1("DurangoProfile::OnGetContainerNamesComplete");
	try
	{
		if(sgdVerifyf(status == dns::AsyncStatus::Completed, "Error reading save game containers (%08X)!", status))
		{
			bool hasCurrent = false;

			// The asyncOp callback in IterateFileHeaders() can happen on this thread if it finishes before we 
			// set the callback function. We need to ensure that the lock is not set if this is the case or a subtle
			// rare deadlock will occur.
			{
				sysCriticalSection lock(m_Cs);
				m_FileList = opertation->GetResults()->First();
				hasCurrent = m_FileList->HasCurrent;

				if(!hasCurrent)
				{
					// If we reach here there are no files.
					m_State = ENUMERATED_CONTENT;
					sgdDebugf1("DurangoProfile::OnGetContainerNamesComplete - no more files");
				}
			}

			if(hasCurrent)
			{
				IterateFileHeaders();
				return;
			}
		}
		else
		{
			sysCriticalSection lock(m_Cs);
			sgdAssertf(false, "Error reading save game containers!");
			m_State = HAD_ERROR;
		}
	}
	catch (Platform::Exception^ e)
	{
		sysCriticalSection lock(m_Cs);
		m_State = HAD_ERROR;
		sgdAssertf(false, "Error reading save game containers! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");
	}
}

void DurangoProfile::OnIterateFileHeaderReadComplete( dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnIterateFileHeaderReadComplete");
	try
	{
		bool interateNextFile = false;

		// The asyncOp callback in IterateFileHeaders() can happen on this thread if it finishes before we 
		// set the callback function. We need to ensure that the lock is not set if this is the case or a subtle
		// rare deadlock will occur.
		{
			sysCriticalSection lock(m_Cs);

			// NOTE: There is no else so if it failed we move on to the next file.
			if(sgdVerifyf(status == dns::AsyncStatus::Completed, "Error reading save game header!"))
			{
				dns::IMapView<Platform::String^, dns::IBuffer^>^ blobs = operation->GetResults();

				if(sgdVerifyf(blobs != nullptr && blobs->HasKey(SAVE_GAME_HEADER), "Failed to read file header!"))
				{
					dns::IBuffer^ buffer = blobs->Lookup(SAVE_GAME_HEADER);

					SaveDataHeader header;
					if( sgdVerifyf(WinRTBuffer::ReadBuffer(buffer, &header), "Failed to read file header!") &&
						sgdVerifyf(header.m_HeaderHash == header.GenerateHeaderHash(), "Save header failed vailidation, possible tampering!") &&
						sgdVerifyf(header.m_Version == SaveDataHeader::EXPECTED_HEADER_VERSION, "Incorrect header version, cannot load save!") )
					{
						safecpy(m_Content[m_ContentCount].DisplayName, header.m_DisplayName);
						++m_ContentCount;
					}
				}
			}

			m_FileList->MoveNext();
			interateNextFile = (m_FileList->HasCurrent && m_ContentCount < m_MaxContent);

			if(!interateNextFile)
			{
				sgdDebugf1("DurangoProfile::OnIterateFileHeaderReadComplete - Finished");
				m_State = ENUMERATED_CONTENT;
			}
		}

		if(m_FileList->HasCurrent && m_ContentCount < m_MaxContent)
		{
			IterateFileHeaders();
		}
	}
	catch(Platform::Exception^ e)
	{
		sysCriticalSection lock(m_Cs);

		sgdAssertf(false, "Error reading save game header! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_State = HAD_ERROR;
	}
}

bool DurangoProfile::BeginSave( u32 UNUSED_PARAM(contentType), const char16* contentName, const char* filename,const void* saveData, u32 saveSize, bool UNUSED_PARAM(overwrite) )
{
	sgdDebugf1("DurangoProfile::BeginSave");
	bool success = SetupTask(SAVING_CONTENT);

	if(success)
	{
		sysCriticalSection lock(m_Cs);
		m_ContentName = contentName;
		m_Filename	  = filename;
		m_SaveData	  = saveData;
		m_DataSize	  = saveSize;
	}

	if(success && ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::BeginSave - starting save now");
		success = StartSave();
	}
	else
	{
		sgdDebugf1("DurangoProfile::BeginSave - failed or StartSave postponed until update");
	}

	return success;
}

bool DurangoProfile::StartSave()
{
	sgdDebugf1("DurangoProfile::StartSave");
	SetTaskAsStarted();

	bool success = false;

	try
	{
		dns::IAsyncAction^ asyncOp = nullptr;

		// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
		// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
		{
			sysCriticalSection lock (m_Cs);

			if (m_State != SAVING_CONTENT)
			{
				sgdWarningf("Cannot begin enumeration, operation (%d) already in progress", m_State);
				return false;
			}

			if(m_StorageSpace != nullptr)
			{
				// now add the date in the form "mm/dd/yy hh:mm:ss"
				char16	dateTime[22];

				// we are using the windows functions as the game ones are not in rage but are in framework.
				SYSTEMTIME time;
				GetLocalTime( &time );

				USES_CONVERSION;

				formatf( dateTime,
					22,
					UTF8_TO_WIDE(" - %02d/%02d/%02d %02d:%02d:%02d"),
					time.wMonth,
					time.wDay,
					((time.wYear - 2000) % 100),
					time.wHour,
					time.wMinute,
					time.wSecond );

				SaveDataHeader header;
				sysMemSet(&header, 0, sizeof(header));

				header.m_Version = SaveDataHeader::EXPECTED_HEADER_VERSION;
				header.m_DataSize = m_DataSize;
				header.m_DataHash = atDataHash((const char*)m_SaveData, m_DataSize);
				safecpy(header.m_DisplayName, m_ContentName);
				safecat(header.m_DisplayName, dateTime);
				header.m_HeaderHash = header.GenerateHeaderHash();

				dns::IBuffer^ headerBuffer = WinRTBuffer::WriteBuffer(header);
				dns::IBuffer^ saveBuffer   = WinRTBuffer::WriteBuffer(m_SaveData, m_DataSize);

				if(sgdVerifyf(headerBuffer != nullptr && saveBuffer != nullptr, "Failed to create save game data buffers!"))
				{
					dns::ConnectedStorageContainer^ saveGame = m_StorageSpace->CreateContainer(Platform::StringReference(UTF8_TO_UTF16(m_Filename)));

					dns::Map<Platform::String^, dns::IBuffer^>^ blobsToSave = ref new dns::Map<Platform::String^, dns::IBuffer^>();
					blobsToSave->Insert(SAVE_GAME_DATA, saveBuffer);
					blobsToSave->Insert(SAVE_GAME_HEADER, headerBuffer);

					asyncOp = saveGame->SubmitUpdatesAsync(blobsToSave->GetView(), nullptr);
				}
			}
			else
			{
				sgdErrorf("DurangoProfile::StartSave - m_StorageSpace is NULL");
			}
		}
		
		if(sgdVerifyf(asyncOp != nullptr, "Error Saving!"))
		{
			asyncOp->Completed = ref new dns::AsyncActionCompletedHandler(
				[this]( dns::IAsyncAction^ action, dns::AsyncStatus status )
			{
				this->OnSaveGameComplete(action, status);
			});

			success = true;
		}
		else
		{
			sysCriticalSection lock (m_Cs);
			m_State = HAD_ERROR;
			success = false;
		}
	}
	catch(Platform::Exception^ e)
	{
		sgdAssertf(false, "Error tring to save game! Exception 0x%08x, Msg: %ls", e->HResult, e->Message != nullptr ? e->Message->Data() : L"NULL");
		sysCriticalSection lock (m_Cs);
		m_State = HAD_ERROR;
		success = false;
	}

	return success;
}

void DurangoProfile::OnSaveGameComplete( dns::IAsyncAction^ action, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnSaveGameComplete");
	sysCriticalSection lock (m_Cs);

	try
	{
		action->GetResults();
		if(sgdVerifyf(status == dns::AsyncStatus::Completed, "Error writing to save game (%d)!", status))
		{
			m_State = SAVED_CONTENT;
		}
		else
		{
			m_State = HAD_ERROR;
		}
	}
	catch(Platform::Exception^ e)
	{
		m_State = HAD_ERROR;
		sgdAssertf(false, "Error writing to save game! Exception 0x%08x, Msg: %ls", e->HResult, e->Message != nullptr ? e->Message->Data() : L"NULL");
	}
}

bool DurangoProfile::CheckSave( bool& outIsValid, bool& /*fileExists*/ )
{
	sgdDebugf1("DurangoProfile::CheckSave");
	if(ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::CheckSave - starting postponed task");
		StartSave();
	}

	sysCriticalSection lock (m_Cs);

	if(m_State == SAVED_CONTENT || m_State == HAD_ERROR)
	{
		sgdDebugf1("DurangoProfile::CheckSave - finished, m_State %d", m_State);
		outIsValid = (m_State == SAVED_CONTENT);
		return true;
	}

	return false;
}

void DurangoProfile::EndSave()
{
	sgdDebugf1("DurangoProfile::EndSave");
	CleanupTask(SAVED_CONTENT);
}

bool DurangoProfile::BeginGetFileSize( const char* filename, bool UNUSED_PARAM(bCalculateSizeOnDisk) )
{
	sgdDebugf1("DurangoProfile::BeginGetFileSize - File: %s", (filename ? filename : "NULL"));
	bool success = SetupTask(GETTING_FILE_SIZE);

	if(success)
	{
		sysCriticalSection lock(m_Cs);
		m_Filename = filename;
	}

	if(success && ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::EndSave - starting task now");
		success = StartGetFileSize();
	}
	else
	{
		sgdDebugf1("DurangoProfile::EndSave - failed or postponing task until update");
	}

	return success;
}

bool DurangoProfile::StartGetFileSize()
{
	sgdDebugf1("DurangoProfile::StartGetFileSize");
	SetTaskAsStarted();

	try
	{
		dns::ContainerInfoQueryResult^ result = nullptr;

		// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
		// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
		{
			sysCriticalSection lock (m_Cs);

			if (m_State != GETTING_FILE_SIZE)
			{
				sgdWarningf("Cannot begin get file size, operation already in progress");
				return false;
			}

			if(m_StorageSpace != nullptr)
			{
				USES_CONVERSION;
				result = m_StorageSpace->CreateContainerInfoQuery(Platform::StringReference(UTF8_TO_UTF16(m_Filename)));
			}
			else
			{
				sgdErrorf("DurangoProfile::StartGetFileSize - m_StorageSpace was NULL");
			}
		}

		bool callbackSetup = false;
		if(sgdVerifyf(result != nullptr, "Error getting file size!"))
		{
			dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ asyncOp = result->GetContainerInfoAsync();

			if(sgdVerifyf(asyncOp != nullptr, "Error getting file size!"))
			{
				asyncOp->Completed = ref new dns::AsyncOperationCompletedHandler<dns::IVectorView<dns::ContainerInfo>^>(
					[this](dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ action, dns::AsyncStatus status)
				{
					this->OnGetFileInfoComplete(action, status);
				}
				);

				callbackSetup = true;
			}
		}
		
		if(callbackSetup == false)
		{
			sysCriticalSection lock (m_Cs);
			m_State = HAD_ERROR;
		}
	}
	catch(Platform::Exception^ e)
	{
		sysCriticalSection lock (m_Cs);
		sgdAssertf(false, "Error getting file size! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_FileList = nullptr;
		m_State = HAD_ERROR;

		return false;
	}

	return true;
}

bool DurangoProfile::CheckGetFileSize(  u64 &fileSize )
{
	sgdDebugf1("DurangoProfile::CheckGetFileSize");
	if(ShouldStartTask())
	{
		StartGetFileSize();
	}

	sysCriticalSection lock (m_Cs);

	if(m_State == HAD_ERROR)
	{
		sgdErrorf("DurangoProfile::CheckGetFileSize - had error");
		fileSize = 0;
		return true;
	}

	if(m_State == HAVE_GOT_FILE_SIZE)
	{
		sgdDebugf1("DurangoProfile::CheckGetFileSize - finished");
		fileSize = m_DataSize;
		m_DataSize = 0u;
		m_FileList = nullptr;
		return true;
	}

	return false;
}

void DurangoProfile::EndGetFileSize()
{
	sgdDebugf1("DurangoProfile::EndGetFileSize");
	CleanupTask(HAVE_GOT_FILE_SIZE);
}

void DurangoProfile::OnGetFileInfoComplete( dns::IAsyncOperation<dns::IVectorView<dns::ContainerInfo>^>^ action, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnGetFileInfoComplete");
	try
	{
		dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ asyncOp = nullptr;

		if(m_StorageSpace != nullptr && sgdVerifyf(status == dns::AsyncStatus::Completed, "Error reading save game header!") && action != nullptr)
		{
			sysCriticalSection lock (m_Cs);
			dns::IVectorView<dns::ContainerInfo>^ files = action->GetResults();

#if !__NO_OUTPUT
			if (files->Size > 1)
			{
				u32 fileCount = 0;
				dns::IIterator<dns::ContainerInfo>^ debugIterator = files->First();
				while (debugIterator->HasCurrent)
				{
					USES_CONVERSION;
					if (sgdVerifyf(debugIterator->Current.Name, "DurangoProfile::OnGetFileInfoComplete - file %u doesn't have a valid filename String", fileCount))
					{
						sgdDisplayf("DurangoProfile::OnGetFileInfoComplete - file %u is %s", fileCount, W2A( (char16 *) debugIterator->Current.Name->Data()));
					}
					fileCount++;
					debugIterator->MoveNext();
				}
			}
#endif	//	!__NO_OUTPUT
			sgdAssertf(files->Size == 1, "Wrong number of files returned using first!");

			if(files->Size > 0)
			{
				dns::ConnectedStorageContainer^ saveGame = m_StorageSpace->CreateContainer(files->First()->Current.Name);

				dns::Vector<Platform::String^>^ blobsToRead = ref new dns::Vector<Platform::String^>();
				blobsToRead->Append(SAVE_GAME_HEADER);

				asyncOp = saveGame->GetAsync(blobsToRead);
			}
		}

		if(sgdVerifyf(asyncOp != nullptr, "Error reading file size header!"))
		{
			asyncOp->Completed = ref new dns::AsyncOperationCompletedHandler<dns::IMapView<Platform::String^, dns::IBuffer^>^>(
				[this]( dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status )
			{
				this->OnReadHeaderForFileSize(operation, status);
			}
			);
		}
		else
		{
			sysCriticalSection lock (m_Cs);
			sgdErrorf("DurangoProfile::OnGetFileInfoComplete - had error");
			m_FileList = nullptr;
			m_State = HAD_ERROR;
		}
	}
	catch(Platform::Exception^ e)
	{
		sysCriticalSection lock (m_Cs);
		sgdAssertf(false, "Error reading save game header! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_FileList = nullptr;
		m_State = HAD_ERROR;
	}
}

void DurangoProfile::OnReadHeaderForFileSize( dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnReadHeaderForFileSize");
	sysCriticalSection lock(m_Cs);
	try
	{
		if(sgdVerifyf(status == dns::AsyncStatus::Completed, "Error reading save game header!"))
		{
			dns::IMapView<Platform::String^, dns::IBuffer^>^ blobs = operation->GetResults();

			if(sgdVerifyf(blobs != nullptr && blobs->HasKey(SAVE_GAME_HEADER), "Failed to read file header!"))
			{
				dns::IBuffer^ buffer = blobs->Lookup(SAVE_GAME_HEADER);

				SaveDataHeader header;
				if( sgdVerifyf(WinRTBuffer::ReadBuffer(buffer, &header), "Failed to read file header!") &&
					sgdVerifyf(header.m_HeaderHash == header.GenerateHeaderHash(), "Save game header failed vailidation, possible tampering!") &&
					sgdVerifyf(header.m_Version == SaveDataHeader::EXPECTED_HEADER_VERSION, "Incorrect header version, cannot load save!") )
				{
					m_DataSize = header.m_DataSize;
					m_State = HAVE_GOT_FILE_SIZE;
				}
			}
		}

		if(m_State != HAVE_GOT_FILE_SIZE)
		{
			sgdErrorf("DurangoProfile::OnReadHeaderForFileSize - had error");
			m_State = HAD_ERROR;
		}
	}
	catch(Platform::Exception^ e)
	{
		sgdAssertf(false, "Error reading save game header! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_State = HAD_ERROR;
	}
}

bool DurangoProfile::BeginLoad( u32 UNUSED_PARAM(contentType), u32 UNUSED_PARAM(deviceId), const char* filename, void* loadData, u32 loadSize )
{
	sgdDebugf1("DurangoProfile::BeginLoad");
	bool success = SetupTask(LOADING_CONTENT);

	if(success)
	{
		sysCriticalSection lock(m_Cs);
		m_Filename = filename;
		m_LoadData = loadData;
		m_DataSize = loadSize;
	}

	if(success && ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::BeginLoad - Starting Load");
		success = StartLoad();
	}
	else
	{
		sgdDebugf1("DurangoProfile::BeginLoad - Failed or postponing load until update");
	}

	return success;
}

bool DurangoProfile::StartLoad()
{
	sgdDebugf1("DurangoProfile::StartLoad");
	SetTaskAsStarted();

	bool success = false;

	try
	{
		dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ asyncOp = nullptr;

		// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
		// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
		if(m_StorageSpace != nullptr)
		{
			sysCriticalSection lock (m_Cs);

			if (m_State != LOADING_CONTENT)
			{
				sgdWarningf("Cannot begin start load, operation already in progress");
				return false;
			}

			USES_CONVERSION;

			dns::ConnectedStorageContainer^ saveGame = m_StorageSpace->CreateContainer(Platform::StringReference(UTF8_TO_UTF16(m_Filename)));

			dns::Vector<Platform::String^>^ blobsToRead = ref new dns::Vector<Platform::String^>();
			blobsToRead->Append(SAVE_GAME_DATA);
			blobsToRead->Append(SAVE_GAME_HEADER);

			asyncOp = saveGame->GetAsync(blobsToRead);
		}

		if(sgdVerifyf(asyncOp != nullptr, "Error loading saved game!"))
		{
			asyncOp->Completed = ref new dns::AsyncOperationCompletedHandler<dns::IMapView<Platform::String^, dns::IBuffer^>^>(
				[this]( dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status )
			{
				this->OnSaveGameDataReadComplete(operation, status);
			}
			);
			success = true;
		}
		else
		{
			sysCriticalSection lock (m_Cs);
			sgdErrorf("DurangoProfile::StartLoad - had error");
			m_State = HAD_ERROR;
			success = false;
		}
	}
	catch(Platform::Exception^ e)
	{
		sysCriticalSection lock (m_Cs);
		sgdAssertf(false, "Error loading save game data! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_State = HAD_ERROR;
		success = false;
	}

	return success;
}

bool DurangoProfile::CheckLoad( bool &outIsValid, u32 &loadSize )
{
	sgdDebugf1("DurangoProfile::CheckLoad");
	if(ShouldStartTask())
	{
		StartLoad();
	}

	sysCriticalSection lock (m_Cs);

	if(m_State == HAD_ERROR)
	{
		sgdErrorf("DurangoProfile::CheckLoad - Had Error");
		outIsValid = false;
		loadSize = 0;
		return true;
	}

	if(m_State == LOADED_CONTENT)
	{
		sgdDebugf1("DurangoProfile::CheckLoad - Finished");
		outIsValid = true;
		loadSize = m_DataSize;
		return true;
	}

	return false;
}

void DurangoProfile::EndLoad()
{
	sgdDebugf1("DurangoProfile::EndLoad");
	CleanupTask(LOADED_CONTENT);
}

void DurangoProfile::OnSaveGameDataReadComplete( dns::IAsyncOperation<dns::IMapView<Platform::String^, dns::IBuffer^>^>^ operation, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnSaveGameDataReadComplete");
	sysCriticalSection lock (m_Cs);

	try
	{
		if(status == dns::AsyncStatus::Completed)
		{
			dns::IMapView<Platform::String^, dns::IBuffer^>^ blobs = operation->GetResults();

			if(sgdVerifyf(blobs != nullptr && blobs->HasKey(SAVE_GAME_HEADER) && blobs->HasKey(SAVE_GAME_DATA), "Failed to read saved game, it could be corrupted!"))
			{
				dns::IBuffer^ headerBuffer = blobs->Lookup(SAVE_GAME_HEADER);
				dns::IBuffer^ saveBuffer   = blobs->Lookup(SAVE_GAME_DATA);

				SaveDataHeader header;
				if( sgdVerifyf(WinRTBuffer::ReadBuffer(headerBuffer, &header), "Failed to read file header!") &&
					sgdVerifyf(header.m_HeaderHash == header.GenerateHeaderHash(), "Save header failed validation, possible tampering!") &&
					sgdVerifyf(header.m_Version == SaveDataHeader::EXPECTED_HEADER_VERSION, "Incorrect header version, cannot load save!") &&
					sgdVerifyf(saveBuffer->Length == header.m_DataSize, "Datasizes don't match, savegame might be corrupted! saveBuffer size %d, datasize in header %d", saveBuffer->Length, header.m_DataSize) &&
					sgdVerifyf(WinRTBuffer::ReadBuffer(saveBuffer, m_LoadData, header.m_DataSize), "Failed to read save data!") &&
					sgdVerifyf(header.m_DataHash == atDataHash((const char*)m_LoadData, header.m_DataSize), "Datahashes don't match, file might be corrupted! header %d savegame %d", header.m_DataHash, atDataHash((const char*)m_LoadData, header.m_DataSize)))
				{
					m_DataSize = header.m_DataSize;
					m_State = LOADED_CONTENT;
				}
			}
		}
		else
		{
			sgdWarningf("Error reading save game header!");
		}

		if(m_State != LOADED_CONTENT)
		{
			sgdErrorf("Error reading save game headers - state was %d!", m_State);
			m_State = HAD_ERROR;
		}
	}
	catch(Platform::Exception^ e)
	{
		sgdAssertf(false, "Error reading save game header! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_State = HAD_ERROR;
	}

	m_SaveData = nullptr;
}

bool DurangoProfile::BeginDelete( u32 UNUSED_PARAM(contentType), const char *filename )
{
	sgdDebugf1("DurangoProfile::BeginDelete");
	bool success = SetupTask(DELETING_CONTENT);

	if(success)
	{
		sysCriticalSection lock(m_Cs);
		m_Filename = filename;
	}

	if(success && ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::BeginDelete - Started Delete");
		success = StartDelete();
	}
	else
	{
		sgdDebugf1("DurangoProfile::BeginDelete - Error or postponed Started Delete until update");
	}

	return success;
}

bool DurangoProfile::StartDelete()
{
	sgdDebugf1("DurangoProfile::StartDelete");
	SetTaskAsStarted();

	bool success = false;
	try
	{
		dns::IAsyncAction^ asyncOp = nullptr;

		// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
		// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
		{
			sysCriticalSection lock(m_Cs);

			if(m_StorageSpace != nullptr)
			{
				m_State = DELETING_CONTENT;

				USES_CONVERSION;
				asyncOp = m_StorageSpace->DeleteContainerAsync(Platform::StringReference(UTF8_TO_UTF16(m_Filename)));
			}
			else
			{
				sgdErrorf("DurangoProfile::StartDelete - m_StorageSpace was NULL");
			}
		}

		if(sgdVerifyf(asyncOp != nullptr, "Error deleting file!"))
		{
			asyncOp->Completed = ref new dns::AsyncActionCompletedHandler(
				[this]( dns::IAsyncAction^ action, dns::AsyncStatus status )
			{
				this->OnSaveGameDelete(action, status);
			}
			);

			success = true;
		}
		else
		{
			sysCriticalSection lock(m_Cs);
			m_State = HAD_ERROR;
			success = false;
		}
	}
	catch(Platform::Exception^ e)
	{
		sysCriticalSection lock(m_Cs);
		sgdAssertf(false, "Error deleting save game! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_State = HAD_ERROR;
		success = false;
	}

	return success;
}

bool DurangoProfile::CheckDelete()
{
	sgdDebugf1("DurangoProfile::CheckDelete");
	if(ShouldStartTask())
	{
		sgdDebugf1("DurangoProfile::CheckDelete - Started from postponed");
		StartDelete();
	}

	sysCriticalSection lock (m_Cs);
	return m_State == HAD_ERROR || m_State == DELETED_CONTENT;
}

void DurangoProfile::EndDelete()
{
	sgdDebugf1("DurangoProfile::EndDelete");
	CleanupTask(DELETED_CONTENT);
}

void DurangoProfile::OnSaveGameDelete( dns::IAsyncAction^ action, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnSaveGameDelete");
	sysCriticalSection lock (m_Cs);

	if(sgdVerifyf(status == dns::AsyncStatus::Completed, "Error deleting save game - status was %d!", status))
	{
		m_State = DELETED_CONTENT;
	}
	else
	{
		m_State = HAD_ERROR;
	}
}

void DurangoProfile::RetrieveConnectedStorageSpaceInfo()
{
	sgdDebugf1("DurangoProfile::RetrieveConnectedStorageSpaceInfo");
	try
	{
		dns::IAsyncOperation<dns::ConnectedStorageSpace^>^ asyncOp = nullptr;

		// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
		// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
		{
			sysCriticalSection lock(m_Cs);
			
			// Clear previous container information.
			m_StorageSpace = nullptr;
			m_StorageSpaceState = LOADING;
			asyncOp = dns::ConnectedStorageSpace::GetForUserAsync(m_User);
		}

		if(sgdVerifyf(asyncOp != nullptr, "Error getting user storage space!"))
		{
			asyncOp->Completed = ref new dns::AsyncOperationCompletedHandler<dns::ConnectedStorageSpace^>(
				[this](dns::IAsyncOperation<dns::ConnectedStorageSpace^>^ operation, dns::AsyncStatus status)
			{
				this->OnGetUserStorageSpaceComplete(operation, status);
			});
		}
		else
		{
			m_StorageSpaceState = LOAD_FAILED;
		}
	}
	catch(Platform::Exception^ e)
	{
		sgdAssertf(false, "Error retrieving User's connected storage space information! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_StorageSpaceState = LOAD_FAILED;
	}
}

void DurangoProfile::OnGetUserStorageSpaceComplete( dns::IAsyncOperation<dns::ConnectedStorageSpace^>^ operation, dns::AsyncStatus status )
{
	sgdDebugf1("DurangoProfile::OnGetUserStorageSpaceComplete");
	try
	{
		sysCriticalSection lock(m_Cs);
		m_StorageSpace = nullptr;

		if(status == dns::AsyncStatus::Completed)
		{
			m_StorageSpace = operation->GetResults();
		}

		if(m_StorageSpace != nullptr)
		{
			m_StorageSpaceState = LOAD_SUCCESSFUL;
		}
		else
		{
			sgdErrorf("DurangoProfile::OnGetUserStorageSpaceComplete - Failed to get storage space");
			m_StorageSpaceState = LOAD_FAILED;
		}
	}
	catch(Platform::Exception^ e)
	{
		sgdAssertf(false, "Error retrieving User's storage space information! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

		m_StorageSpaceState = LOAD_FAILED;
		m_StorageSpace = nullptr;
	}
}

void DurangoProfile::SetUser( dns::User^ user )
{
	sgdDebugf1("DurangoProfile::SetUser");
	// The asyncOp callback can happen on this thread if it finishes before we set the callback function.
	// We need to ensure that the lock is not set if this is the case or a subtle rare deadlock will occur.
	{
		sysCriticalSection lock(m_Cs);
		m_User = user;
		if(m_State != IDLE && m_State != HAD_ERROR)
		{
			sgdAssertf(false, "Changing user whilst a task in progress!");

			// Terminate current task with an error.
			m_State = HAD_ERROR;
		}
	}

	if(user != nullptr)
	{
		RetrieveConnectedStorageSpaceInfo();
	}
}

inline dns::User^ DurangoProfile::GetUser()
{
	sysCriticalSection lock(m_Cs);
	return m_User;
}

fiSaveGameState::State DurangoProfile::GetState() const
{
	sysCriticalSection lock(m_Cs);
	return m_State;
}

DurangoProfile::SaveDataHeader::SaveDataHeader()
{
	sgdDebugf1("DurangoProfile::SaveDataHeader");
	Clear();
}

void DurangoProfile::SaveDataHeader::Clear()
{
	sgdDebugf1("DurangoProfile::Clear");
	m_Version = 0;
	memset(m_DisplayName, 0, sizeof(m_DisplayName));
	m_DataHash = 0;
	m_HeaderHash = 0;
}

u32 DurangoProfile::SaveDataHeader::GenerateHeaderHash()
{
	u32 hash = atDataHash((const char*)&m_Version, sizeof(m_Version));
	hash = atDataHash((const char*)&m_DataSize, sizeof(m_DataSize), hash);
	hash = atDataHash((const char*)m_DisplayName, sizeof(m_DisplayName), hash);
	return atDataHash((const char*)&m_DataHash, sizeof(m_DataHash), hash);
}


#else // _XDK_VER >= 9586
class DurangoProfile
{
public:
	DurangoProfile();
	~DurangoProfile();

	bool BeginEnumeration(u32, fiSaveGame::Content*, int){return true;}
	bool CheckEnumeration(int&){return true;}
	void EndEnumeration(){;}

	bool BeginSave(u32, const char16*, const char*, const void*, u32, bool){return true;}
	bool CheckSave(bool&, bool&){return true;}
	void EndSave(){;}



	bool BeginGetFileSize(const char*, bool){return true;}
	bool CheckGetFileSize(u64&){return true;}
	void EndGetFileSize(){;}

	bool BeginLoad(u32, u32,const char*,void*,u32){return true;}
	bool CheckLoad(bool &,u32 &){return true;}
	void EndLoad(){;}

	bool BeginDelete(u32, const char *){return true;}
	bool CheckDelete(){return true;}
	void EndDelete(){;}

	void SetStateToIdle(){;}
};
DurangoProfile::DurangoProfile()
{}

DurangoProfile::~DurangoProfile()
{}

#endif	// _XDK_VER >= 9586
// This is intentionally the same Title ID as the XContent sample.
char *XEX_TITLE_ID = "FFFF011D";

fiSaveGame SAVEGAME;

static atRangeArray<DurangoProfile, fiSaveGame::SIGNIN_COUNT> s_Profiles;
static atRangeArray<int*,fiSaveGame::SIGNIN_COUNT> s_Counts;
static atRangeArray<bool*,fiSaveGame::SIGNIN_COUNT> s_Saves;
static atRangeArray<u32*,fiSaveGame::SIGNIN_COUNT> s_Loads;
static atRangeArray<bool,fiSaveGame::SIGNIN_COUNT> s_Selects;

static sysUserList::Delegate s_DurangoSaveDelegate;

static void OnUserListChange(const sysUserInfo& userInfo)
{
	sgdDebugf1("OnUserListChange");
	const sysDurangoUserInfo& durangoUser = static_cast<const sysDurangoUserInfo&>(userInfo);
	s32 index = durangoUser.GetUserIndex();
	dns::User^ user = durangoUser.GetPlatformUser();

	if( s_Profiles[index].GetUser() == nullptr || user == nullptr || user->Id != s_Profiles[index].GetUser()->Id)
	{
		s_Profiles[index].SetUser(user);
	}
}

void fiSaveGame::InitClass()
{
	sgdDebugf1("fiSaveGame::InitClass");
	s_DurangoSaveDelegate.Bind(&OnUserListChange);
	sysDurangoUserList::GetPlatformInstance().AddDelegate(&s_DurangoSaveDelegate);
	
	try
	{
		const u32 maxUsers = Min(sysDurangoUserList::GetPlatformInstance().GetMaxUsers(), SIGNIN_COUNT);
		for(u32 i = 0; i < maxUsers; ++i)
		{
			const sysDurangoUserInfo* userInfo = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(i);

			if(userInfo != nullptr)
			{
				s_Profiles[i].SetUser(userInfo->GetPlatformUser());
			}
			else
			{
				s_Profiles[i].SetUser(nullptr);
			}
		}
	}
	catch(Platform::Exception^ e)
	{
		sgdAssertf(false, "Error retrieving users! Exception 0x%08x, Msg: %ls",
			e->HResult,
			e->Message != nullptr ? e->Message->Data() : L"NULL");

	}
}

void fiSaveGame::ShutdownClass()
{
	sgdDebugf1("fiSaveGame::ShutdownClass");
	for(u32 i = 0; i < SIGNIN_COUNT; ++i)
	{
		s_Profiles[i].SetUser(nullptr);
	}

	sysDurangoUserList::GetPlatformInstance().RemoveDelegate(&s_DurangoSaveDelegate);
	s_DurangoSaveDelegate.Unbind();
}

bool fiSaveGame::BeginSelectDevice(int /*signInId*/,u32 /*contentType*/,u32 /*minSpaceAvailable*/,bool /*forceShow*/,bool /*manageStorage*/) {
	return true;
}


bool fiSaveGame::CheckSelectDevice(int /*signInId*/) {
	return true;
}


void fiSaveGame::EndSelectDevice(int) {
}


u32 fiSaveGame::GetSelectedDevice(int) {
	return 1;	// same as Xenon Hard Drive
}


void fiSaveGame::SetSelectedDevice(int, u32) {
}


bool fiSaveGame::BeginEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount)
{
	return s_Profiles[signInId].BeginEnumeration(contentType, outContent, maxCount);
}


bool fiSaveGame::CheckEnumeration(int signInId,int &outCount)
{
	return s_Profiles[signInId].CheckEnumeration(outCount);
}


void fiSaveGame::EndEnumeration(int signInId)
{
	s_Profiles[signInId].EndEnumeration();
}


bool fiSaveGame::BeginSave(int signInId,u32 contentType,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool overwrite)
{
	return s_Profiles[signInId].BeginSave(contentType, displayName, filename, saveData, saveSize, overwrite);
}


bool fiSaveGame::CheckSave(int signInId, bool &outIsValid, bool& fileExists)
{
	return s_Profiles[signInId].CheckSave(outIsValid, fileExists);
}


void fiSaveGame::EndSave(int signInId)
{
	s_Profiles[signInId].EndSave();
}

bool fiSaveGame::BeginFreeSpaceCheck(int /*signInId*/,const char* /*filename*/,u32 /*saveSize*/)
{
	return true;
}

bool fiSaveGame::CheckFreeSpaceCheck(int /*signInId*/, int& /*ExtraSpaceRequired*/)
{
	return true;
}

void fiSaveGame::EndFreeSpaceCheck(int /*signInId*/)
{
}

bool fiSaveGame::BeginGetFileSize(int signInId, const char* filename, bool bCalculateSizeOnDisk)
{
	return s_Profiles[signInId].BeginGetFileSize(filename, bCalculateSizeOnDisk);
}

bool fiSaveGame::CheckGetFileSize(int signInId, u64 &fileSize)
{
	return s_Profiles[signInId].CheckGetFileSize(fileSize);
}

void fiSaveGame::EndGetFileSize(int signInId)
{
	return s_Profiles[signInId].EndGetFileSize();
}


#if HACK_GTA4
bool fiSaveGame::BeginExtraSpaceCheck(int /*signInId*/, const char * /*filename*/, u32 /*saveSize*/)
{
	return true;
}

bool fiSaveGame::CheckExtraSpaceCheck(int /*signInId*/, int & /*ExtraSpaceRequired*/, int & /*TotalHDDFreeSize*/, int & /*SizeOfSystemFileKB*/)
{
	return true;
}

void fiSaveGame::EndExtraSpaceCheck(int /*signInId*/)
{
}
#endif	//	HACK_GTA4

#if HACK_GTA4
int fiSaveGame::CalculateSizeOfSaveGameFile(int /*DataSize*/, int /*SizeOfSystemFiles*/)
{
	sgdErrorf("CalculateSizeOfSaveGameFile only supported for PS3");
	return -1;
}
#endif	//	HACK_GTA4

#if HACK_GTA4
bool fiSaveGame::BeginReadNamesAndSizesOfAllSaveGames(int /*signInId*/, Content* /*outContent*/, int* /*outFileSizes*/, int /*maxCount*/)
{
	return true;
}

bool fiSaveGame::CheckReadNamesAndSizesOfAllSaveGames(int /*signInId*/, int& /*outCount*/)
{
	return true;
}

void fiSaveGame::EndReadNamesAndSizesOfAllSaveGames(int /*signInId*/)
{
}
#endif	//	HACK_GTA4

bool fiSaveGame::BeginLoad(int signInId,u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize)
{
	return s_Profiles[signInId].BeginLoad(contentType, deviceId, filename, loadData, loadSize);
}


bool fiSaveGame::CheckLoad(int signInId,bool& outIsValid,u32 &loadSize)
{
	return s_Profiles[signInId].CheckLoad(outIsValid, loadSize);
}


void fiSaveGame::EndLoad(int signInId)
{
	return s_Profiles[signInId].EndLoad();
}


bool fiSaveGame::BeginIconSave(int /*signInId*/,u32 /*contentType*/,const char* /*filename*/,const void* /*iconData*/,u32 /*iconSize*/)
{
	// Durango has no concepts of Icons as Durango will not feature a UI for editing/deleting saved games.
	return true;
}


bool fiSaveGame::CheckIconSave(int /*signInId*/)
{
	return true;
}


void fiSaveGame::EndIconSave(int /*signInId*/)
{
}


bool fiSaveGame::BeginContentVerify(int /*signInId*/,u32 /*contentType*/, u32 /*deviceId*/, const char* /*filename*/)
{
	return true;
}


bool fiSaveGame::CheckContentVerify(int /*signInId*/,bool& /*verified*/)
{
	return true;
}


void fiSaveGame::EndContentVerify(int /*signInId*/)
{
}


bool fiSaveGame::BeginDelete(int signInId, u32 contentType, const char* filename)
{
	return s_Profiles[signInId].BeginDelete(contentType, filename);
}


bool fiSaveGame::CheckDelete(int signInId)
{
	return s_Profiles[signInId].CheckDelete();
}


void fiSaveGame::EndDelete(int signInId)
{
	s_Profiles[signInId].EndDelete();
}

bool fiSaveGame::BeginGetCreator(int /*signInId*/, u32 /*contentType*/,u32 /*deviceId*/,const char* /*filename*/)
{
	return true;
}

bool fiSaveGame::CheckGetCreator(int /*signInId*/, bool& bIsCreator)
{
	// As the async containers are tied to a user, the save should belong to them.
	// TR: TODO: Check if we still need to perform this check!!
	bIsCreator = true;
	return true;
}

void fiSaveGame::EndGetCreator(int /*signInId*/)
{
}


u64 fiSaveGame::GetTotalAvailableSpace(int)
{
	return 0;
}

u64 fiSaveGame::CalculateDataSizeOnDisk(int , rage::u32)
{
	return 0;
}

fiSaveGameState::State fiSaveGame::GetState(int signInId) const
{
	return s_Profiles[signInId].GetState();
}

int fiSaveGame::GetMaxUsers()
{
	return SIGNIN_COUNT;
}

bool fiSaveGame::IsStorageDeviceChanged(int /*signInId*/)
{
	return false;
}

fiSaveGame::Errors fiSaveGame::GetError(int /*signInId*/)
{
	return fiSaveGame::SAVE_GAME_SUCCESS;
}

void fiSaveGame::SetStateToIdle(int signInId)
{   
	s_Profiles[signInId].SetStateToIdle();
}

void fiSaveGame::SetSelectedDeviceToAny(int /*signInId*/)
{
}

bool fiSaveGame::IsCurrentDeviceValid(int /*signInId*/)
{
	return true;
}

bool fiSaveGame::IsDeviceValid(int /*signInId*/, u32 /*deviceId*/)
{
	return true;
}

bool fiSaveGame::GetCurrentDeviceData(int /*signInId*/, fiSaveGame::Device* /*outDevice*/)
{
	return false;
	//return GetDeviceData(m_DeviceID, outDevice);
}

bool fiSaveGame::IsStorageDeviceRemoved(int /*signInId*/)
{
	return false;
}

bool fiSaveGame::GetDeviceData(int /*signInId*/, u32 /*deviceId*/, fiSaveGame::Device* /*outDevice*/)
{
	return false;
}

bool fiSaveGame::HasFileBeenAccessed(int , const char* )
{
	return false;
}

void fiSaveGame::SetIcon(char *, const u32)
{
}

void fiSaveGame::SetMaxNumberOfSaveGameFilesToEnumerate(int /*MaxNumSaveGames*/)
{
}

void fiSaveGame::SetSaveGameTitle(const char* /*pSaveGameTitle*/)
{
}

void fiSaveGame::SetCheckForUserBindErrors(const char* /*pBindErrorMessage*/)
{
}

void fiSaveGame::SetUserServiceId(int /* signInId */ , int /* userId */)
{
}

void fiSaveGame::SetMountIndex(int /* signInId */, int /* index */)
{
}

void fiSaveGame::FillDevice(int)
{
}

bool fiSaveGame::BeginGetFileModifiedTime(int /*signInId*/, const char* /*filename*/)
{
	return true;
}

bool fiSaveGame::CheckGetFileModifiedTime(int /*signInId*/, u32& /*modTimeHigh*/, u32& /*modTimeLow*/)
{
	return true;
}

void fiSaveGame::EndGetFileModifiedTime(int /*signInId*/)
{
}

bool fiSaveGame::BeginGetFileExists(int /*signInId*/, const char* /*filename*/)
{
	return true;
}

bool fiSaveGame::CheckGetFileExists(int /*signInId*/, bool& /*exists*/)
{
	return true;
}

void fiSaveGame::EndGetFileExists(int /*signInId*/)
{	
}


// so we don't interfere with other cpp files in unity builds
#undef UTF8_TO_UTF16
#undef UTF16_TO_UTF8

}		// namespace rage


#endif // RSG_DURANGO
