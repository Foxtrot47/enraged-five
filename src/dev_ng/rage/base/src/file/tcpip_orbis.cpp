// 
// file/tcpip_orbis.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS

#include "tcpip.h"

#include <net.h>
#include <sdk_version.h>

#pragma comment(lib,"SceNetCtl_stub_weak")
#pragma comment(lib,"SceNet_stub_weak")

namespace rage {

int fiDeviceTcpIp::PrintInterfaceState()
{
	return 0;
}

}	//rage

#endif	//RSG_ORBIS
