// 
// file/decompress.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "decompress.h"
#include "compress_internal.h"

#if LOG_EVERYTHING
#include "file/stream.h"
#endif

namespace rage {

inline u32 get_bits(int &accum,const u8 *&src,int count) {
	u32 result = 0;
	Assert(count);
	do {
		if (accum & 0x10000)
			accum = 0x100 | *src++;
		// Printf((accum & 0x80)?"1":"0");
		result = (result<<1) | ((accum & 0x80) >> 7);
		accum <<= 1;
	} while (--count);
	// Displayf("");
	return result;
}


/*
	NOTES
	The data format is defined as a series of blocks (calling code is assumed to know
	the uncompressed length of the data as that is our termination condition).

	Each block starts with a length encoding:
	1 -> 1
	000 -> 2
	001 -> 3
	010 -> 4
	0110xxxxxx -> 5-68 (field width is c_InitialWidth+c_WidthStep=6)
	01110xxxxxxxxxx -> 69...1092 (field width is c_InitialWidth+2*c_WidthStep=10)
	011110xxxxxxxxxx -> 1093...
	Each extra "1" after the initial 011 causes the field width (which starts at
	c_InitialWidth+c_WidthStep) to be extended by c_WidthStep more bits.  The list of 
	ones is terminated by a zero, followed by the field width.
	Note that each additional "1" also affects the bias of the field so that there
	is no redundancy (and therefore wasted space) in the representable length range.

	Next a single type bit identifies the block as either a literal run or a window.

	If the type bit is one, the block is a window, and the lookback value is encoded in a 
	similar fashion to above:
	0xx -> 1,2,3,4 (when c_InitialWidth=2)
	10xxxxxx = 5-68 (and rest is as above).
	We copy "length+1" bytes from "lookback" bytes before the current output position
	forward into the output buffer.  Note that this gives us trivial RLE (or pattern 
	replication) when the length is longer than the lookback value.

	If the type bit is zero, the block is a literal run, and "length" bytes of data
	are pulled from the source compressed stream and copied into the output.

	Note that encodings of small values is different for lengths and lookback values;
	this is because a length value of one is very common, so we special-case it at
	the expense of runs bits or longer.

	Some test data:
	476193 byte text file compressed to 48058 bytes (versus 69501 bytes for zip)
	56.25M resource file compressed to 8.63M bytes (versus 27.72M for zip)
	
	8323316 bytes with max lookback of 1M, with proper update (very slow).
	8898722 bytes with max loolback of 1M, no proper update (pretty quick -- 17.41 seconds)
	8277993 bytes with max lookback of 64k, with proper update.
	9035189 bytes with max lookback of 64k, no proper update (5 seconds)

	9,035,189 output.dev (64k lookback)
	8,608,704 output.rel.16M
	8,191,677 output.rel.16M.full_lookback_update
	8,898,723 output.rel.1M
	8,323,317 output.rel.1M.full_lookback_update

	I suspect we handily beat zip on very large files because we basically have unbounded
	lookup, so relatively long chains can be recycled cheaply.

	TODO:
	- Consider more permutations of c_InitialWidth and c_WidthStep, possibly using different
	  values between lengths and offsets.
	- Consider compressing a file with several different permutations and then making a few
	  of those parameters stored in the file itself and configurable during decompression setup.
*/
bool fiDecompress(u8 *dest,u32 destSize,const u8 *src,u32 /*srcSize*/) {
	int accum = 0x10000;
	u32 offset = 0;

	bool overlapped = (src > dest && src < dest + destSize);

#if LOG_EVERYTHING
	fiSafeStream logfile(fiStream::Create("t:\\decompress.txt"));
#endif

#define GET_BIT()	get_bits(accum,src,1)
#define GET_BITS(n)	get_bits(accum,src,(n))

	if (src[0] != c_Magic_1 || src[1] != c_Magic_2)
		return false;
	src += 2;

	// Displayf("[DECOMP]");
	while (offset < destSize) {
		if (overlapped && dest + offset > src)
			return false;

		// Decode length (either literal count or window length minus one)
		u32 length = 0;
		if (GET_BIT())
			length = 1;
		else if (GET_BIT()) {	// 4 or 5-20
			if (GET_BIT()) {	// 5-20+
				u32 width = c_InitialWidth + c_WidthStep;
				u32 bias = 1 + (1 << c_InitialWidth);
				while (GET_BIT()) {
					bias += (1<<width);
					width += c_WidthStep;
				}
				length = bias + GET_BITS(width);
			}
			else			// 4
				length = 4;
		}
		else if (GET_BIT())
			length = 3;
		else
			length = 2;

		// Frame type:
		if (GET_BIT()) {		// window
			++length;			// min window length is two
			u32 width = c_InitialWidth;
			u32 bias = 1;
			while (GET_BIT()) {
				bias += (1<<width);
				width += c_WidthStep;
			}
			u32 backup = bias + GET_BITS(width);
			LOG2("**W%u,%u\r\n",length,backup);
			if (backup > offset)
				return false;
			do {
				if (offset >= destSize)
					return false;
				dest[offset] = dest[offset-backup];
				++offset;
			} while (--length);
		}
		else {					// literals
			LOG1("**L%u\r\n",length);
			do {
				if (offset >= destSize)
					return false;
				LOG1(" **[%d]\r\n",*src);
				dest[offset++] = *src++;
			} while (--length);
		}
	}

	if (offset != destSize)
		return false;
	else
		return true;
}




}	// namespace rage
