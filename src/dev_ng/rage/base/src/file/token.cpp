//
// file/token.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "token.h"

#include "file/stream.h"

#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/vec.h"
#include "vectormath/classes.h"

#include <stdarg.h>	// required for PutStr()
#include <string.h>

using namespace rage;



/***************** fiBaseTokenizer *****************/

/*
	It could be argued that this really belongs in the parse module.
	But asset really belongs in fia, and asset includes this file for
	convenience.  I see parse as being higher-level code that people might
	not want to buy in to, while this is simple enough for people to
	accept.
*/

#if __FINAL
#ifdef __GNUC__
#define ERRORF(args...)
#else
#define ERRORF
#endif
#else
#define ERRORF Quitf
#endif

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4702)
#endif

int fiBaseTokenizer::CommentChar=';';

/*
PURPOSE
	Initialize the base tokenizer.
PARAMS
	name - used to set the file name.
	s - to set the stream.
RETURNS
	none.
NOTES
*/
void fiBaseTokenizer::Init(const char *name,fiStream *s) {
	line = 1;
	filename = name;
	S = s;
	nextch = 32;
	pushcount = 0;
	pushbuf[0] = 0;
	terminators = 0;
	m_WriteState = ENDLINE;
}


/*
PURPOSE
	Reset the tokenizer.
*/
void fiBaseTokenizer::Reset() {
	S->Seek(0);
	Init(filename,S);
}

/*
PURPOSE
	Push the string into the buffer.
PARAMS
	string - the string to push.
	length - the length of the string.
RETURNS
	none.
NOTES
*/
void fiBaseTokenizer::PushBack(const char *string,int length) {
	Assert(pushcount==0 || !length);	// make sure something's not already pushed back
	Assert(length < (int)sizeof(pushbuf)-1);
	const char quote = '\"';
	pushcount = length + 1;
	// Don't need quotes if it's already quoted or it does not contain a space or tab
	bool quoted = (*string == quote) || (!strchr(string,32) && !strchr(string,9));
	if (!quoted) {
		length += 2;
		pushcount += 2;
		pushbuf[length--] = quote;
	}
	while (*string)
		pushbuf[length--] = *string++;
	if (!quoted)
		pushbuf[1] = quote;
	pushbuf[0] = 32;	// so token gets terminated.
}

/*
PURPOSE
	Get one token. It returns the last one in the buffer.
PARAMS
	none.
RETURNS
	the token.
NOTES
*/
int fiBaseTokenizer::GetTokenCh() {
	int result;

	if (pushcount)
		result = pushbuf[--pushcount];
	else
		result = S->FastGetCh();
	if (result == 10)
		++line;
	return result;
}

/*
PURPOSE
	Skip the comment tokens.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiBaseTokenizer::SkipComment()
{
	while (nextch==CommentChar)
		while (nextch!=-1 && nextch!=10 && nextch!=13) {
			nextch = GetTokenCh();
		}
}

inline char isterminator(int c, const char* terminators, int numTerminators)
{	
	for (int i = 0; i != numTerminators; ++i)
		if (terminators[i] == c)
			return terminators[i];

	return -1;
}

//////// read functions ////////
/*
PURPOSE
	Get the tokens.
PARAMS
	dest - where the tokens saved.
	maxlen - maximum number of tkens to get.
RETURNS
	the number of token have been returned.
NOTES
*/
int fiBaseTokenizer::GetToken(char *dest,int maxlen) {
	// skip whitespace
	while (iswhitespace(nextch)) {
		nextch = GetTokenCh();
	}

	int stored = 0;
	// store characters until next whitespace
	if (nextch == '"') {
		nextch = GetTokenCh();
		while (nextch != -1 && nextch != '"') {
			if (stored < maxlen-1)
				dest[stored++] = (char) nextch;
			nextch = GetTokenCh();
		}
		nextch = GetTokenCh();	// consume closing quote.
	}
	else {
		while (nextch!=-1 && !iswhitespace(nextch) && (!terminators || !strchr(terminators, nextch))) {
				// see if we found a comment, read until the end of line:
				while (nextch==CommentChar)
				{
					SkipComment();
					// if we started the token, then we return what we've found...
					if (stored!=0)
						break;
					// ...otherwise, we skip over all whitespace until we get 
					// a valid token char:
					else
						while (iswhitespace(nextch))
							nextch = GetTokenCh();
				}

				// handle case where comment is line line of file
				if (nextch != -1 && stored < maxlen-1)
					dest[stored++] = (char) nextch;
				nextch = GetTokenCh();
		}
	}

	// if we stopped because of a terminator, consume it now
	// we can't treat terminators as just whitespace or else parsing 
	// an empty field in a csv file would not work.
	if (terminators && strchr(terminators, nextch))
		nextch = GetTokenCh();

	if ( dest ) {
		dest[stored] = 0;
	}
	return stored;
}

void fiBaseTokenizer::SetStreamPosition(int position)
{
	Assert(S);

	// Clear the pushback buffer.
	pushcount = 0;
	S->Seek(position);	
}

int fiBaseTokenizer::GetStreamPosition()
{
	Assert(S);
	return S->Tell();
}


/*
PURPOSE
	Get a "length" of block of characters.
PARAMS
	dest - where the tokens saved.
	length - the size of the block to get.
RETURNS
	the number of tokens returned.
NOTES
*/
int fiBaseTokenizer::GetBlock(char *dest,int length) {
	int i;

	for (i = 0; nextch != -1 && i < length; i++)
	{
		dest[i] = (char) nextch;
		nextch = S->GetCh();
	}

	return i;
}

/*
PURPOSE
	If the token in the buffer does not match the given one, display error.
PARAMS
	match - the tokens to be matched.
RETURNS
	none.
NOTES
	Case sensitive
*/
void fiBaseTokenizer::MatchToken(const char *match) {
	char buf[64];
	buf[0] = 0;
	if (!GetToken(buf,sizeof(buf)) || strcmp(match, buf))
		ERRORF("%s(%d): Expected '%s', got '%s'.",filename,line,match,buf);
}

/*
PURPOSE
	Similar to MatchToken but it's case insensitive.
PARAMS
	none.
RETURNS
	none.
NOTES
	Case insensitive
*/
void fiBaseTokenizer::MatchIToken(const char *match) {
	char buf[64];
	buf[0] = 0;
	if (!GetToken(buf,sizeof(buf)) || stricmp(match, buf))
		ERRORF("%s(%d): Expected '%s', got '%s'.",filename,line,match,buf);
}


bool fiBaseTokenizer::EndOfFile()
{
	if (pushcount)
		return false;
	else
	{
		int result = GetTokenCh();
		if(result == -1)
			return true; // probably going to want to get more sophisticated here and check for comments and white space
		pushbuf[pushcount++]=(char)result;
	}
	return false;
}

/*
PURPOSE
	Check the token in the buffer.
PARAMS
	check - used for checking.
	consume - Pull the token out of stream if it s a match with <b>check</b> param
RETURNS
	false if no token in the buffer or not same as "check".
NOTES
	Case sensitive
*/
bool fiBaseTokenizer::CheckToken(const char *check, bool consume) {
	char buf[PUSH_BUFFER_SIZE];
	int length;
	buf[0] = 0;
	int lastNextChr=nextch;
	if ((length=GetToken(buf,sizeof(buf))) == 0 || strcmp(check, buf))
	{
		PushBack(buf,length);
		nextch=lastNextChr;
		return false;
	}
	if (!consume)
	{
		PushBack(buf,length);
		nextch=lastNextChr;
	}

	return true;
}

/*
PURPOSE
	Same as above but case insensitive.
PARAMS
	check - used for checking.
	consume - Pull the token out of stream if it s a match with <b>check</b> param
RETURNS
	false if no token in the buffer or not same as "check".
NOTES
	Case insensitive
*/
bool fiBaseTokenizer::CheckIToken(const char *check, bool consume) {
	char buf[PUSH_BUFFER_SIZE];
	int length;
	buf[0] = 0;
	int lastNextChr=nextch;
	if ((length=GetToken(buf,sizeof(buf))) == 0 || stricmp(check, buf))
	{
		PushBack(buf,length);
		nextch=lastNextChr;
		return false;
	}
	if (!consume)
	{
		PushBack(buf,length);
		nextch=lastNextChr;
	}

	return true;
}

/*
PURPOSE
	Return the tokens.
PARAMS
	dest - where the tokens saved.
	maxLen - maximum tokens to get.
	terminator - get tokens until reach this token.
RETURNS
	the number of tokens returned.
NOTES
*/
int fiBaseTokenizer::GetTokenToChar(char *dest,int maxLen,char terminator,bool stripWhiteSpace)
{
	bool start = true;
	int stored = 0, nextch = 0, i;
	while (stored < (maxLen-1))
	{
		nextch = GetTokenCh();
		if (nextch==CommentChar)
			SkipComment();
		if (nextch == -1 && stored == 0) stored = -1;
		if (nextch == -1 || nextch == terminator)	break;
		if (!stripWhiteSpace || !start || !iswhitespace(nextch))
		{
			start = false;
			dest[stored++] = (char) nextch;
		}
	}

	// Trim trailing whitespace
	for (i = stored - 1; i >= 0; i--)
		if (iswhitespace(dest[i]))
			stored--;
		else
			break;

	if (stored>=0)
		dest[stored] = 0;
	return stored;
}

/*
PURPOSE
Return the tokens.
PARAMS
dest - where the tokens saved.
maxLen - maximum tokens to get.
terminator - get tokens until reach any of these token.
RETURNS
the number of tokens returned.
NOTES
*/


int fiBaseTokenizer::GetTokenToChars(char *dest,int maxLen,const char* terminators,int numTerminators, char& terminator, bool stripWhiteSpace)
{
	bool start = true;
	int stored = 0, nextch = 0, i;
	while (stored < (maxLen-1))
	{
		nextch = GetTokenCh();
		if (nextch==CommentChar)
			SkipComment();
		if (nextch == -1 && stored == 0) stored = -1;
		terminator = isterminator(nextch, terminators, numTerminators);
		if (nextch == -1 || terminator != -1) break;

		if (!stripWhiteSpace || !start || !iswhitespace(nextch))
		{
			start = false;
			dest[stored++] = (char) nextch;
		}
	}

	// Trim trailing whitespace
	for (i = stored - 1; i >= 0; i--)
		if (iswhitespace(dest[i]))
			stored--;
		else
			break;

	if (stored>=0)
		dest[stored] = 0;
	return stored;
}

/*
PURPOSE
	Skip to the end of the line.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiBaseTokenizer::SkipToEndOfLine() {
	do {
		nextch = GetTokenCh();
	} while(nextch != -1 && nextch != '\n');
	return;

}

/*
PURPOSE
	Ignore one token.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiBaseTokenizer::IgnoreToken() {
	char buf[1];
	GetToken(buf,1);
}

/*
PURPOSE
	Pop() is useful for file formats which are in flux.  it allows you to have
	a block (delimited with "{" and "}") with unspecified information at the
	end - so if you're using Pop(), and someone adds some info to the end of
	the block, then your version won't crash when you use their fia.  this way
	programmers can add things to files without forcing everyone to immediately
	upgrade.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiBaseTokenizer::Pop() {
#if 1
	int depth = 1;
	do
	{
		const int BUFF_SIZE = 100;
		char buff[BUFF_SIZE];
		GetToken(buff,BUFF_SIZE);
		if (buff[0] == '{')
			depth++;
		else if (buff[0] == '}')
			depth--;
		else if (nextch == -1)	// beats the hell out of just hanging up here indefinitely.
		{
			Quitf(ERR_SYS_INVALIDFILE,"Unexpected EOF in fiBaseTokenizer::Pop");
		}
	} while (depth > 0);
#else
	MatchToken("}");
#endif
}

//////// write functions ////////
/*
PURPOSE
	Add one token into buffer.
PARAMS
	c - token to be added.
RETURNS
	true if success.
NOTES
*/
bool fiBaseTokenizer::Put(char c) {
	m_WriteState = OTHER;
	return S->FastPutCh(c)==1;
}

/*
PURPOSE
	Put a formated string into buffer.
PARAMS
	fmt - foramt string.
RETURNS
	true if success.
NOTES
*/
bool fiBaseTokenizer::PutStr(const char* fmt, ...)
{
	m_WriteState = OTHER;
	char buffer[PUT_BUFFER_SIZE];
	va_list args;
	va_start(args,fmt);
	vsprintf(buffer,fmt,args);
	va_end(args);
	return Put(buffer);
}


/*
PURPOSE
	Same as PutStr, but preceded by StartLine and followed by EndLine.
PARAMS
	fmt - foramt string.
RETURNS
	true if success.
NOTES
*/
bool fiBaseTokenizer::PutStrLine (const char* fmt, ...)
{
	bool rv;
	StartLine();

	// PutStr
	{
		char buffer[PUT_BUFFER_SIZE];
		va_list args;
		va_start(args,fmt);
		vsprintf(buffer,fmt,args);
		va_end(args);
		rv = Put(buffer);
	}

	EndLine();
	return rv;
}


/***************** fiAsciiTokenizer *****************/
//////// read functions ////////
/*
PURPOSE
	Get a byte.
PARAMS
	none.
RETURNS
	a byte.
NOTES
*/
int fiAsciiTokenizer::GetByte(const bool error) {
	return GetInt(error);
}

/*
PURPOSE
	Get a short.
PARAMS
	none.
RETURNS
	a short.
NOTES
*/
int fiAsciiTokenizer::GetShort(const bool error) {
	return GetInt(error);
}

/*
PURPOSE
Get a u32 (stored as a hex string)
PARAMS
none.
RETURNS
an integer (0-9).
NOTES
*/
u32 fiAsciiTokenizer::GetHexU32(const bool error) {

	/*
	t.GetToken(buffer,sizeof(buffer));		
	if (hexString[1]=='x'||hexString[1]=='X')
	hexString+=2;
	
	*/
	char buf[32]="\0";
	char* hexString = &buf[0];
	if (GetToken(buf,sizeof(buf)) && ((buf[0]>='0'&&buf[0]<='9') || (buf[0]>='A' && buf[0]<='F') || (buf[0]>='a' && buf[0]<='f')))
	{
		if (hexString[1]=='x'||hexString[1]=='X')
			hexString+=2;
		return strtoul(hexString,NULL,16);
	}
	else if(error) {
		ERRORF("%s(%d): Expected integer.",filename,line);
		return 0;
	}
	else return (u32)-1;
}

/*
PURPOSE
	Get an integer.
PARAMS
	none.
RETURNS
	an integer (0-9).
NOTES
*/
int fiAsciiTokenizer::GetInt(const bool error) {
	char buf[32];
	if (GetToken(buf,sizeof(buf)) && (buf[0]=='-' || (buf[0]>='0'&&buf[0]<='9')))
		return atoi(buf);
	else if(error) {
		ERRORF("%s(%d): Expected integer.",filename,line);
		return 0;
	}
	else return -1;
}

/*
PURPOSE
	Get a float.
PARAMS
	none.
RETURNS
	a float number.
NOTES
*/
float fiAsciiTokenizer::GetFloat(const bool error) {
	char buf[32];
	if (GetToken(buf,sizeof(buf)) && (buf[0]=='-' || buf[0]=='.' ||
			(buf[0]>='0'&&buf[0]<='9')))
		return (float) atof(buf);
	else if(error){
		ERRORF("%s(%d): Expected float.",filename,line);
		return 0.0f;
	}
	else return -1.0f;
}

/*
PURPOSE
	Get a 2D vector.
PARAMS
	v - the output vector.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::GetVector(Vector2& v, const bool error) {
	v.x = GetFloat(error);
	v.y = GetFloat(error);
}


/*
PURPOSE
	Get a 3D vector.
PARAMS
	v - the output vector.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::GetVector(Vector3& v, const bool error) {
	v.x = GetFloat(error);
	v.y = GetFloat(error);
	v.z = GetFloat(error);
}


/*
PURPOSE
	Get a 4D vector.
PARAMS
	v - the output vector.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::GetVector(Vector4& v, const bool error) {
	v.x = GetFloat(error);
	v.y = GetFloat(error);
	v.z = GetFloat(error);
	v.w = GetFloat(error);
}

/*
PURPOSE
	Get a 4D vector. The v.w = 1.
PARAMS
	v - the output vector.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::GetVector(Vec_xyz& v, const bool error) {
	float x = GetFloat(error), y = GetFloat(error), z = GetFloat(error);
	v.Set(x,y,z);
}

/*
PURPOSE
	Get a 4D vector.
PARAMS
	v - the output vector.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::GetVector(Vec_xyzw& v, const bool error) {
	float x = GetFloat(error), y = GetFloat(error), z = GetFloat(error), w = GetFloat(error);
	v.Set(x,y,z,w);
}

/*
PURPOSE
	This method does MatchToken.
PARAMS
	de - delimiter to be matched.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::GetDelimiter(const char* de)
{ MatchToken(de); }

/*
PURPOSE
	This method does match token then return an integer.
PARAMS
	match - to be matched.
RETURNS
	integer.
NOTES
*/
int		fiAsciiTokenizer::MatchInt(const char *match)
{ MatchToken(match); return GetInt(); }

/*
PURPOSE
	This method does match token then return a float.
PARAMS
	match - to be matched.
RETURNS
	float number.
NOTES
*/
float	fiAsciiTokenizer::MatchFloat(const char *match)
{ MatchToken(match); return GetFloat(); }

/*
PURPOSE
	This method does match token then return a 2D vector.
PARAMS
	match - to be matched.
	v - 2D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchVector(const char *match,Vector2& v)
{ MatchToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 3D vector.
PARAMS
	match - to be matched.
	v - 3D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchVector(const char *match,Vector3& v)
{ MatchToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 3D vector.
PARAMS
	match - to be matched.
	v - 3D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchVector(const char *match,Vector4& v)
{ MatchToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 3D vector.
PARAMS
	match - to be matched.
	v - 3D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchVector(const char *match,Vec_xyz& v)
{ MatchToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 4D vector.
PARAMS
	match - to be matched.
	v - 4D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchVector(const char *match,Vec_xyzw& v)
{ MatchToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return an integer.
PARAMS
	match - to be matched.
RETURNS
	integer.
NOTES
*/
int		fiAsciiTokenizer::MatchIInt(const char *match)
{ MatchIToken(match); return GetInt(); }

/*
PURPOSE
	This method does match token then return a float.
PARAMS
	match - to be matched.
RETURNS
	float number.
NOTES
*/
float	fiAsciiTokenizer::MatchIFloat(const char *match)
{ MatchIToken(match); return GetFloat(); }

/*
PURPOSE
	This method does match token then return a 2D vector.
PARAMS
	match - to be matched.
	v - 2D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchIVector(const char *match,Vector2& v)
{ MatchIToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 3D vector.
PARAMS
	match - to be matched.
	v - 3D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchIVector(const char *match,Vector3& v)
{ MatchIToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 4D vector.
PARAMS
	match - to be matched.
	v - 4D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchIVector(const char *match,Vector4& v)
{ MatchIToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 3D vector.
PARAMS
	match - to be matched.
	v - 3D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchIVector(const char *match,Vec_xyz& v)
{ MatchIToken(match); GetVector(v); }

/*
PURPOSE
	This method does match token then return a 4D vector.
PARAMS
	match - to be matched.
	v - 4D vecto rto be returned.
RETURNS
	none.
NOTES
*/
void	fiAsciiTokenizer::MatchIVector(const char *match,Vec_xyzw& v)
{ MatchIToken(match); GetVector(v); }

/////// Formatting functions /////////
/*
PURPOSE
	Start a new block with "{".
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::StartBlock() 
{
	WriteTabs(m_nNumTabs);
	S->FastPutCh('{');
	S->FastPutCh('\r');
	S->FastPutCh('\n');
	m_nNumTabs++; 
}

/*
PURPOSE
	End a block with "}".
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::EndBlock() 
{ 
	m_nNumTabs--; 
	AssertMsg((m_nNumTabs >= 0) , "More End Blocks Than Start Blocks");
	WriteTabs(m_nNumTabs);
	S->FastPutCh('}');
	S->FastPutCh('\r');
	S->FastPutCh('\n');
}

/*
PURPOSE
	This method writes a number of tabs.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::StartLine() 
{
	if (m_WriteState != STARTLINE)
		WriteTabs(m_nNumTabs);
	m_WriteState = STARTLINE;
}

/*
PURPOSE
	Put a new line.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::EndLine() 
{ 
	if (m_WriteState != ENDLINE) {
		S->FastPutCh('\r');
		S->FastPutCh('\n');
	}
	m_WriteState = ENDLINE;
}

/*
PURPOSE
	Put a number of tabs into stream.
PARAMS
	nNumTabs - number of tabs to be put.
RETURNS
	none.
NOTES
*/
void fiAsciiTokenizer::WriteTabs(int nNumTabs)
{
	while(nNumTabs--)
	{
		S->FastPutCh('\t');
	}
}

//////// write functions ////////
/*
PURPOSE
	Put the string plus number of tabs.
PARAMS
	src - the string to be written
	tabs - number of tabs to be written.
RETURNS
	true if the written amount is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const char* src,unsigned int tabs) {
	m_WriteState = OTHER;
	if (!*src) src = "\"\"";	// so we know it's an empty string at read time
	int len=StringLength(src);
	int requested=len+(int)tabs;
	int count=S->Write(src,len);
	while (tabs--)
	{
		count++;
		S->FastPutCh('\t');
	}

	return count==requested;
}


/*
PURPOSE
	Put a byte (integer) as a string into stream.
PARAMS
	i - a byte to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::PutByte(int i) {
	m_WriteState = OTHER;
	char str[64];
	int num=sprintf(str,"%d ",i);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a short (integer) as a string into stream.
PARAMS
	i - a short to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::PutShort(int i) {
	m_WriteState = OTHER;
	char str[64];
	int num=sprintf(str,"%d ",i);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put an integer as a string into stream.
PARAMS
	i - an integer to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(int i) {
	m_WriteState = OTHER;
	char str[64];
	int num=sprintf(str,"%d ",i);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a float as a string into stream.
PARAMS
	f - a float to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(float f)
{
	m_WriteState = OTHER;
	char str[64];
	int num=sprintf(str,"%.8g ",f);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a 2D vector as a string into stream.
PARAMS
	v - a 2D vector to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Vector2& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g ",v.x,v.y);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a 3D vector as a string into stream.
PARAMS
	v - a 3D vector to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Vector3& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g\t%.8g ",v.x,v.y,v.z);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a 4D vector as a string into stream.
PARAMS
	v - a 4D vector to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Vector4& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%g\t%g\t%g\t%g ",v.x,v.y,v.z,v.w);
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a ScalarV as a string into stream.
PARAMS
	v - a ScalarV value to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const ScalarV& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g",v.Getf());
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a Vec2V as a string into stream.
PARAMS
	v - a Vec2V vector to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Vec2V& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g ",v.GetXf(),v.GetYf());
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a Vec3V as a string into stream.
PARAMS
	v - a Vec3V vector to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Vec3V& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g\t%.8g ",v.GetXf(),v.GetYf(),v.GetZf());
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a Vec4V as a string into stream.
PARAMS
	v - a Vec4V vector to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Vec4V& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g\t%.8g\t%.8g ",v.GetXf(),v.GetYf(),v.GetZf(),v.GetWf());
	return S->Write(str,num)==num;
}

/*
PURPOSE
	Put a Mat33V as a string into stream.
PARAMS
	v - a Mat33V matrix to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Mat33V& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol0().GetXf(),v.GetCol0().GetYf(),v.GetCol0().GetZf());
	bool result = (S->Write(str,num)==num);
	if (result)
	{
		num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol1().GetXf(),v.GetCol1().GetYf(),v.GetCol1().GetZf());
		result = (S->Write(str,num)==num);
	}
	if (result)
	{
		num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol2().GetXf(),v.GetCol2().GetYf(),v.GetCol2().GetZf());
		result = (S->Write(str,num)==num);
	}
	return result;
}

/*
PURPOSE
	Put a Mat34V as a string into stream.
PARAMS
	v - a Mat34V matrix to be put.
RETURNS
	true if the written size is correct.
NOTES
*/
bool fiAsciiTokenizer::Put(const Mat34V& v)
{
	m_WriteState = OTHER;
	char str[128];
	int num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol0().GetXf(),v.GetCol0().GetYf(),v.GetCol0().GetZf());
	bool result = (S->Write(str,num)==num);
	if (result)
	{
		num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol1().GetXf(),v.GetCol1().GetYf(),v.GetCol1().GetZf());
		result = (S->Write(str,num)==num);
	}
	if (result)
	{
		num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol2().GetXf(),v.GetCol2().GetYf(),v.GetCol2().GetZf());
		result = (S->Write(str,num)==num);
	}
	if (result)
	{
		num=sprintf(str,"%.8g\t%.8g\t%.8g\n",v.GetCol3().GetXf(),v.GetCol3().GetYf(),v.GetCol3().GetZf());
		result = (S->Write(str,num)==num);
	}
	return result;
}


/*
PURPOSE
	Put the string as delimiter.
PARAMS
	de - the delimiter.
RETURNS
	true if the size to be put correctly.
NOTES
*/
bool	fiAsciiTokenizer::PutDelimiter(const char* de)			{return Put(de);}

/***************** fiBinTokenizer *****************/
//////// read functions ////////
/*
PURPOSE
	Get a byte.
PARAMS
	none.
RETURNS
	a byte.
NOTES
*/
int fiBinTokenizer::GetByte(const bool error) {
	unsigned char result;
	if (S->Read(&result,1)<0 && error)
	{
		ERRORF("%s(%d): Expected byte.",filename,line);
		return 0;
	}

	return result; 
}

/*
PURPOSE
	Get a short.
PARAMS
	none.
RETURNS
	a short.
NOTES
*/
int fiBinTokenizer::GetShort(const bool error) {
	unsigned short result;
	if (S->ReadShort(&result,1)<0 && error)
	{
		ERRORF("%s(%d): Expected short.",filename,line);
		return 0;
	}

	return result; 
}

/*
PURPOSE
	Get an integer.
PARAMS
	none.
RETURNS
	an integer.
NOTES
*/
int fiBinTokenizer::GetInt(const bool error) {
	int result;
	if (S->ReadInt(&result,1)<0 && error)
	{
		ERRORF("%s(%d): Expected integer.",filename,line);
		return 0;
	}

	return result; 
}

/*
PURPOSE
	Get a float.
PARAMS
	none.
RETURNS
	a float number.
NOTES
*/
float fiBinTokenizer::GetFloat(const bool error) {
	float result;
	if (S->ReadFloat(&result,1)<0 && error)
	{
		ERRORF("%s(%d): Expected float.",filename,line);
		return 0;
	}

	return result; 
}


/*
PURPOSE
	Get a 2D vector.
PARAMS
	v - the returned 2D vector.
RETURNS
	none.
NOTES
*/
void fiBinTokenizer::GetVector(Vector2& v, const bool error) {
	if (S->ReadFloat(&v.x,2) != 2 && error)
		ERRORF("%s(%d): Expected Vector2.",filename,line);
}


/*
PURPOSE
	Get a 3D vector.
PARAMS
	v - the returned 3D vector.
RETURNS
	none.
NOTES
*/
void fiBinTokenizer::GetVector(Vector3& v, const bool error) {
	if (S->ReadFloat(&v.x,3) != 3 && error)
		ERRORF("%s(%d): Expected Vector3.",filename,line);
}


/*
PURPOSE
	Get a 4D vector.
PARAMS
	v - the returned 4D vector.
RETURNS
	none.
NOTES
*/
void fiBinTokenizer::GetVector(Vector4& v, const bool error) {
	if (S->ReadFloat(&v.x,4) != 4 && error)
		ERRORF("%s(%d): Expected Vector4.",filename,line);
}


/*
PURPOSE
	Get a 3D vector.
PARAMS
	v - the returned 3D vector.
RETURNS
	none.
NOTES
*/
void fiBinTokenizer::GetVector(Vec_xyz& v, const bool error) {
	if (S->ReadFloat((float*)&v,3) != 3 && error)
		ERRORF("%s(%d): Expected Vec.",filename,line);
	// v.w = 1;
}


/*
PURPOSE
	Get a 4D vector.
PARAMS
	v - the returned 4D vector.
RETURNS
	none.
NOTES
*/
void fiBinTokenizer::GetVector(Vec_xyzw& v, const bool error) {
	if (S->ReadFloat((float*)&v,4) != 4 && error)
		ERRORF("%s(%d): Expected Vec.",filename,line);
}


/*
PURPOSE
	Do nothing.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::GetDelimiter(const char* /*de*/)	{}

/*
PURPOSE
	Return an integer.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
int		fiBinTokenizer::MatchInt(const char * /*match*/)					{ return GetInt(); }

/*
PURPOSE
	Return a float.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
float	fiBinTokenizer::MatchFloat(const char * /*match*/)					{ return GetFloat(); }

/*
PURPOSE
	Return a 2D vector.
PARAMS
	v - 2D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchVector(const char * /*match*/,Vector2& v)		{ GetVector(v); }

/*
PURPOSE
	Return a 3D vector.
PARAMS
	v - 3D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchVector(const char * /*match*/,Vector3& v)		{ GetVector(v); }

/*
PURPOSE
	Return a 4D vector.
PARAMS
	v - 4D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchVector(const char * /*match*/,Vector4& v)		{ GetVector(v); }

/*
PURPOSE
	Return a 3D vector.
PARAMS
	v - 3D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchVector(const char * /*match*/,Vec_xyz& v)		{ GetVector(v); }

/*
PURPOSE
	Return a 4D vector.
PARAMS
	v - 4D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchVector(const char * /*match*/,Vec_xyzw& v)	{ GetVector(v); }

/*
PURPOSE
	Return an integer.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
int		fiBinTokenizer::MatchIInt(const char * /*match*/)					{ return GetInt(); }

/*
PURPOSE
	Return a float.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
float	fiBinTokenizer::MatchIFloat(const char * /*match*/)				{ return GetFloat(); }

/*
PURPOSE
	Return a 2D vector.
PARAMS
	v - 2D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchIVector(const char * /*match*/,Vector2& v)	{ GetVector(v); }

/*
PURPOSE
	Return a 3D vector.
PARAMS
	v - 3D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchIVector(const char * /*match*/,Vector3& v)	{ GetVector(v); }

/*
PURPOSE
	Return a 4D vector.
PARAMS
	v - 4D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchIVector(const char * /*match*/,Vector4& v)	{ GetVector(v); }

/*
PURPOSE
	Return a 3D vector.
PARAMS
	v - 3D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchIVector(const char * /*match*/,Vec_xyz& v)	{ GetVector(v); }

/*
PURPOSE
	Return a 4D vector.
PARAMS
	v - 4D vector to be returned.
RETURNS
	none.
NOTES
*/
void	fiBinTokenizer::MatchIVector(const char * /*match*/,Vec_xyzw& v)	{ GetVector(v); }

//////// write functions ////////
/*
PURPOSE
	Put the string into the stream followed by a NULL character.
PARAMS
	src - the string to put.
RETURNS
	true if amount of written is same as the length of the string.
NOTES
*/
bool fiBinTokenizer::Put(const char* src,unsigned int) {
	int len=StringLength(src);
	int requested=len;
	int count=S->Write(src,len);

	// write a NULL character:
	return count==requested && S->Write("",1);
}

/*
PURPOSE
	Put a byte into the stream.
PARAMS
	i - a byte to be put.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::PutByte(int i) {
	char b = (char) i;
	return S->Write(&b,sizeof(char))==sizeof(char);
}

/*
PURPOSE
	Put a short into the stream.
PARAMS
	i - the short to be written.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::PutShort(int i) {
	short s = (short) i;
	return S->Write(&s,sizeof(short))==sizeof(short);
}

/*
PURPOSE
	Put an integer into the stream.
PARAMS
	i - the integer to be written.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::Put(int i) {
	return S->Write(&i,sizeof(int))==sizeof(int);
}

/*
PURPOSE
	Put a float into the stream.
PARAMS
	f - the float number to be written.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::Put(float f)
{
	return S->Write(&f,sizeof(float))==sizeof(float);
}

/*
PURPOSE
	Put a 2D vector into the stream.
PARAMS
	v - a 2D vector to be written.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::Put(const Vector2& v)
{
	return S->Write(&v,sizeof(Vector2))==sizeof(Vector2);
}

/*
PURPOSE
	Put a 3D vector into the stream.
PARAMS
	v - a 3D vector to be written.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::Put(const Vector3& v)
{
	return S->Write(&v,sizeof(Vector3))==sizeof(Vector3);
}

/*
PURPOSE
	Put a 4D vector into the stream.
PARAMS
	v - a 4D vector to be written.
RETURNS
	true if amount of written is correct.
NOTES
*/
bool fiBinTokenizer::Put(const Vector4& v)
{
	return S->Write(&v,sizeof(Vector4))==sizeof(Vector4);
}

/*
PURPOSE
	Do nothing.
PARAMS
	none.
RETURNS
	Always true.
NOTES
*/
bool	fiBinTokenizer::PutDelimiter(const char*)			{return true;}


/***************** fiMultiTokenizer *****************/
/*
PURPOSE
	Constructor.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
fiMultiTokenizer::fiMultiTokenizer() { }

/*
PURPOSE
	Get a read tokenizer.
PARAMS
	name - used to initialize the tokenizer.
	s - stream used to initialize the tokenizer.
	asciiString - used to determine which tokenizer to return.
	binString - used to determine which tokenizer to return.
RETURNS
	reference to the returned tokenizer.
NOTES
	read in a string from the stream and compare with the two input strings to
	determine which tokenizer to initialize and return.

	If can not determine which, return the ascii tokenizer.
*/
fiBaseTokenizer& fiMultiTokenizer::GetReadTokenizer(const char* name,fiStream* s,const char* asciiString,const char* binString)
{
	int len=StringLength(asciiString);
	Assert(len==StringLength(binString));

	char buff[32];
	if (s->Read(buff,len)<0)
		Errorf("couldn't get header string");

	buff[len]=0;

	// is it an ascii file...
	if (!strcmp(buff,asciiString))
	{
		Ascii.Init(name,s);
		return Ascii;
	}
	// ...or is it a binary file..
	else if (!strcmp(buff,binString))
	{
		Bin.Init(name,s);
		return Bin;
	}

	// ... unknown:
	ERRORF("%s(%d): unknown version string: '%s'.",name,0,buff);
	Ascii.Init(name,s);
	return Ascii;
}

/*
PURPOSE
	Get a write tokenizer and put the input string into the stream.
PARAMS
	name - used to initialize the tokenizer.
	s - stream used to initialize the tokenizer.
	bin - used to determine which tokenizer to return.
	string - the string to put.
RETURNS
	reference to the returned tokenizer.
NOTES
*/
fiBaseTokenizer& fiMultiTokenizer::GetWriteTokenizer(const char* name,fiStream* s,bool bin,const char* string)
{ 
	fiBaseTokenizer* base;
	if (bin)
	{
		Bin.Init(name,s);
		base=&Bin;
	}
	else
	{
		Ascii.Init(name,s);
		base=&Ascii;
	}


	base->Put(string);
	base->PutDelimiter("\n");

	return *base;
}


/*
PURPOSE
	Constructor.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
fiTokenizer::fiTokenizer() { }

/*
PURPOSE
	Constructor.
PARAMS
	name - used to initialize the takenizer.
	S -
RETURNS
	none.
NOTES
*/
fiTokenizer::fiTokenizer(const char *name,fiStream *S) {
	Init(name,S);
}

#if __WIN32
#pragma warning(pop)
#endif
