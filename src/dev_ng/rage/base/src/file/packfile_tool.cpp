// 
// file/packfile_tool.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "packfile.h"

// For PC-based tools (leave out release builds for now)
#if (__WIN32PC && (__TOOL || __DEV || __RESOURCECOMPILER) && (!defined(__RGSC_DLL) || !__RGSC_DLL))

#include "data/aes.h"
#include "system/xtl.h"
#include "system/endian.h"
#include "paging/xcompress_7776.h"
#include "system/xcompress_settings.h"
#if RSG_CPU_X64
#pragma comment(lib,"xcompress_x64_2008.lib")
#elif RSG_CPU_X86
#pragma comment(lib,"xcompress_2008.lib")
#endif

#include "zlib/zlib.h"

namespace rage {

u8* fiPackfile::ExtractFileToMemory(u32 handle,u32 &size) const {
	const fiPackEntry &pe = m_Entries[handle & EntryMask];

	u32 offset = pe.GetFileOffset();
	size = pe.GetConsumedSize();
	u8 *buffer;
	int amtRead = 0;
	if (pe.IsFile() && pe.IsCompressed()) {
		buffer = rage_new u8[size];
		amtRead = ReadBulk(m_Handle,offset,buffer,size);

		u32 uncompSize = pe.GetUncompressedSize();
		u8 *uncompressed = rage_new u8[uncompSize];

		if (pe.u.file.m_Encrypted) {
#if RSG_CPU_X64 
			const char* name = m_NameHeap + (pe.m_NameOffset << m_NameShift);
			unsigned int transformitSelector = atStringHash(name);
			transformitSelector+= pe.GetUncompressedSize();
			transformitSelector = transformitSelector % TFIT_NUM_KEYS;
#endif

			AES aes(m_KeyId);
#if RSG_CPU_X64 
			aes.Decrypt(m_KeyId, transformitSelector, buffer, amtRead);
#else
			aes.Decrypt(buffer,amtRead);
#endif
		}

		if (m_IsXCompressed) {
			XMEMDECOMPRESSION_CONTEXT ctxt;
			XMEMCODEC_PARAMETERS_LZX codecParams = XCOMPRESS_SETTINGS;
			if (XMemCreateDecompressionContext(XMEMCODEC_LZX, (VOID*)&codecParams, XMEMCOMPRESS_STREAM, &ctxt))
				Quitf("Error in XMemCreateDecompressionContext");
			SIZE_T outputSize = uncompSize;
			if (XMemDecompress(ctxt,uncompressed,&outputSize,buffer,size))
				Quitf("Error in decompression");
			XMemDestroyDecompressionContext(ctxt);
		}
		else {
			z_stream c_stream;
			memset(&c_stream,0,sizeof(c_stream));
			if (inflateInit2(&c_stream,-MAX_WBITS) < 0)
				Quitf("Error in inflateInit");
			c_stream.next_in = buffer;
			c_stream.avail_in = (uInt) size;
			c_stream.next_out = uncompressed;
			c_stream.avail_out = uncompSize;
			if (inflate(&c_stream, Z_SYNC_FLUSH) < 0)
				Quitf("Error extracting zip data");
			inflateEnd(&c_stream);
		}
		delete[] buffer;
		buffer = uncompressed;
		size = uncompSize;
	}
	else if (pe.IsResource()) {
		// Special case - huge resources
		if (size == fiPackEntry::MaxConsumedSize) {
			u8 s[16];
			ReadBulk(m_Handle,offset,s,sizeof(s));
			size = (s[2] << 24) | (s[5] << 16) | (s[14] << 8) | s[7];
		}
		buffer = rage_new u8[size];
		amtRead = ReadBulk(m_Handle,offset,buffer,size);
		// We scramble the file headers in resources now for security, so replace the garbage with a correct
		// header derived from the packfile directory
		if (amtRead > sizeof(datResourceFileHeader)) {
			datResourceFileHeader &fh = *(datResourceFileHeader*)buffer;
			fh.Magic = datResourceFileHeader::c_MAGIC;
			fh.Version = (pe.u.resource.m_Info.Virtual.Version << 4) | (pe.u.resource.m_Info.Physical.Version);
			fh.Info = pe.u.resource.m_Info;
			if (m_IsByteSwapped) {
				u32 *tmp = (u32*)&fh;
				tmp[0] = sysEndian::Swap(tmp[0]);
				tmp[1] = sysEndian::Swap(tmp[1]);
				tmp[2] = sysEndian::Swap(tmp[2]);
				tmp[3] = sysEndian::Swap(tmp[3]);
			}
		}
	}
	else {
		// Uncompressed file
		size = pe.GetUncompressedSize();
		buffer = rage_new u8[size];
		amtRead = ReadBulk(m_Handle,offset,buffer,size);
	}
	return buffer;
}

}	// namespace rage

#endif
