// 
// file/device_relative.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "device_relative.h"

#include "file/asset.h"
#include "string/string.h"
	

namespace rage {


void fiDeviceRelative::Init(const char* pPath, bool readOnly, fiDevice* pDeviceOverride)
{
	int pathLength; 
	char fullname[RAGE_MAX_PATH];
	ASSET.FullPath(fullname,sizeof(fullname),pPath,""); // To allow other extensions than .rpf

	m_IsReadOnly = readOnly;
	m_pDevice = pDeviceOverride ? pDeviceOverride : GetDevice(fullname, readOnly);
	strncpy(m_path, fullname, RAGE_MAX_PATH);

	pathLength = StringLength(m_path);
	if(m_path[pathLength-1] == '/')
	{
//		Assert(pathLength < RAGE_MAX_PATH-2);
		//m_path[pathLength] = '/';
		m_path[pathLength-1] = '\0';
	}
}

bool fiDeviceRelative::MountAs(const char *path) {
	if( fiDevice::Mount( path, *this, m_IsReadOnly ) )
	{
		char deviceName[RAGE_MAX_PATH]={0};
		strncpy(deviceName,path,static_cast<int>(strcspn(path,":")));
		atHashString deviceHash(deviceName);
		m_filenameOffset = StringLength(path) - 1; 
		return true;
	}
	return false;
}

const char *fiDeviceRelative::FixName(char *dest,int destSize,const char *src) const
{
	if(Verifyf(fiDevice::CheckFileValidity(src),"Non-relative path is being passed in here, %s",src))
	{
		strncpy(dest, m_path, destSize);
		strncat(dest, src+m_filenameOffset, destSize);
		return dest;
	}
	else
		return "";
}

fiHandle fiDeviceRelative::Open(const char *filename,bool readOnly) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->Open(path, readOnly);
}

fiHandle fiDeviceRelative::OpenBulk(const char *filename,u64 &bias) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->OpenBulk(path,bias);
}

fiHandle fiDeviceRelative::OpenBulkDrm(const char *filename,u64 &outBias, const void * pDrmKey) const {
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->OpenBulkDrm(path, outBias, pDrmKey);
}

fiHandle fiDeviceRelative::Create(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->Create(path);
}

int fiDeviceRelative::Read(fiHandle handle,void *outBuffer,int bufferSize) const
{
	return m_pDevice->Read(handle, outBuffer, bufferSize);
}

int fiDeviceRelative::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const
{
	return m_pDevice->ReadBulk(handle, offset, outBuffer, bufferSize);
}

int fiDeviceRelative::Write(fiHandle handle,const void *buffer,int bufferSize) const
{
	return m_pDevice->Write(handle, buffer, bufferSize);
}

int fiDeviceRelative::Seek(fiHandle handle,int offset,fiSeekWhence whence) const
{
	return m_pDevice->Seek(handle, offset, whence);
}

u64 fiDeviceRelative::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const
{
	return m_pDevice->Seek64(handle, offset, whence);
}

int fiDeviceRelative::Close(fiHandle handle) const
{
	return m_pDevice->Close(handle);
}

int fiDeviceRelative::CloseBulk(fiHandle handle) const
{
	return m_pDevice->CloseBulk(handle);
}

int fiDeviceRelative::Size(fiHandle handle) const
{
	return m_pDevice->Size(handle);
}
u64 fiDeviceRelative::Size64(fiHandle handle) const
{
	return m_pDevice->Size64(handle);
}

u64 fiDeviceRelative::GetBulkOffset(const char* name) const {
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, name);
	return m_pDevice->GetBulkOffset(path);
}


int fiDeviceRelative::GetResourceInfo(const char* name,datResourceInfo &info) const {
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, name);
	return m_pDevice->GetResourceInfo(path, info);
}


bool fiDeviceRelative::Delete(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->Delete(path);
}

bool fiDeviceRelative::Rename(const char *from,const char *to) const
{
	char path[RAGE_MAX_PATH];
	char path2[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, from);
	FixName(path2, RAGE_MAX_PATH, to);
	return m_pDevice->Rename(path, path2);
}

bool fiDeviceRelative::MakeDirectory(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->MakeDirectory(path);
}

bool fiDeviceRelative::UnmakeDirectory(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->UnmakeDirectory(path);
}

u64 fiDeviceRelative::GetFileSize(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->GetFileSize(path);
}

u64 fiDeviceRelative::GetFileTime(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->GetFileTime(path);
}

bool fiDeviceRelative::SetFileTime(const char *filename,u64 timestamp) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->SetFileTime(path, timestamp);
}

fiHandle fiDeviceRelative::FindFileBegin(const char *filename,fiFindData &outData) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->FindFileBegin(path, outData);
}

bool fiDeviceRelative::FindFileNext(fiHandle handle,fiFindData &outData) const
{
	return m_pDevice->FindFileNext(handle, outData);
}

int fiDeviceRelative::FindFileEnd(fiHandle handle) const
{
	return m_pDevice->FindFileEnd(handle);
}

bool fiDeviceRelative::SetEndOfFile(fiHandle handle) const
{
	return m_pDevice->SetEndOfFile(handle);
}

u32 fiDeviceRelative::GetAttributes(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->GetAttributes(path);
}

bool fiDeviceRelative::PrefetchDir(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->PrefetchDir(path);
}


bool fiDeviceRelative::SetAttributes(const char *filename,u32 attributes) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->SetAttributes(path, attributes);
}

u32 fiDeviceRelative::GetPhysicalSortKey(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return m_pDevice->GetPhysicalSortKey(path);
}

const char *fiDeviceRelative::GetDebugName() const
{
	return m_path;
}

bool fiDeviceRelative::IsMaskingAnRpf() const
{
	return m_pDevice->IsRpf();
}

const fiDevice* fiDeviceRelative::GetRpfDevice() const
{
	return m_pDevice->GetRpfDevice();
}

}		// namespace rage

