// 
// file/device_psn.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __PPU

#include "device.h"
#include "system/ipc.h"
#include "system/param.h"
#include "system/bootmgr.h"

#include <cell/cell_fs.h>
#include <cell/rtc.h>
#include <sysutil/sysutil_gamecontent.h>
#include <cell/sysmodule.h>
#include <sys/memory.h>
#include <np/drm_inline.h>

#include "string/string.h"
#include "string/stringhash.h"

#include "system/memops.h"

#pragma comment(lib,"fs_stub")
#pragma comment(lib,"rtc_stub")
#pragma comment(lib,"sysutil_stub")
#pragma comment(lib,"sysutil_np_stub")
#pragma comment(lib,"sysutil_game_stub")

#if !__FINAL
#pragma comment(lib,"gcm_gpad_stub")
#endif

using namespace rage;

namespace rage
{
	bool g_IsExiting = false;
	extern char *XEX_TITLE_ID;
};

const int PREFIX_SIZE = 10;

#if !__NO_OUTPUT
static const char *GetErrorString(CellFsErrno err) {
	switch (err) {
		case CELL_FS_ERROR_EDOM         : return "EDOM";
		case CELL_FS_ERROR_EFAULT        : return "EFAULT";
		case CELL_FS_ERROR_EFBIG         : return "EFBIG";
		case CELL_FS_ERROR_EFPOS         : return "EFPOS";
		case CELL_FS_ERROR_EMLINK        : return "EMLINK";
		case CELL_FS_ERROR_ENFILE        : return "ENFILE";
		case CELL_FS_ERROR_ENOENT        : return "ENOENT";
		case CELL_FS_ERROR_ENOSPC        : return "ENOSPC";
		case CELL_FS_ERROR_ENOTTY        : return "ENOTTY";
		case CELL_FS_ERROR_EPIPE         : return "EPIPE";
		case CELL_FS_ERROR_ERANGE        : return "ERANGE";
		case CELL_FS_ERROR_EROFS         : return "EROFS";
		case CELL_FS_ERROR_ESPIPE        : return "ESPIPE";
		case CELL_FS_ERROR_E2BIG         : return "E2BIG";
		case CELL_FS_ERROR_EACCES        : return "EACCES";
		case CELL_FS_ERROR_EAGAIN        : return "EAGAIN";
		case CELL_FS_ERROR_EBADF         : return "EBADF";
		case CELL_FS_ERROR_EBUSY         : return "EBUSY";
		case CELL_FS_ERROR_EEXIST        : return "EEXIST";
		case CELL_FS_ERROR_EINTR         : return "EINTR";
		case CELL_FS_ERROR_EINVAL        : return "EINVAL";
		case CELL_FS_ERROR_EIO           : return "EIO";
		case CELL_FS_ERROR_EISDIR        : return "EISDIR";
		case CELL_FS_ERROR_EMFILE        : return "EMFILE";
		case CELL_FS_ERROR_ENODEV        : return "ENODEV";
		case CELL_FS_ERROR_ENOEXEC       : return "ENOEXEC";
		case CELL_FS_ERROR_ENOMEM        : return "ENOMEM";
		case CELL_FS_ERROR_ENOTDIR       : return "ENOTDIR";
		case CELL_FS_ERROR_ENXIO         : return "ENXIO";
		case CELL_FS_ERROR_EPERM         : return "EPERM";
		case CELL_FS_ERROR_ESRCH         : return "ESRCH";
		case CELL_FS_ERROR_EXDEV         : return "EXDEV";
		case CELL_FS_ERROR_EBADMSG       : return "EBADMSG";
		case CELL_FS_ERROR_ECANCELED     : return "ECANCELED";
		case CELL_FS_ERROR_EDEADLK       : return "EDEADLK";
		case CELL_FS_ERROR_EILSEQ        : return "EILSEQ";
		case CELL_FS_ERROR_EINPROGRESS   : return "EINPROGRESS";
		case CELL_FS_ERROR_EMSGSIZE      : return "EMSGSIZE";
		case CELL_FS_ERROR_ENAMETOOLONG  : return "ENAMETOOLONG";
		case CELL_FS_ERROR_ENOLCK        : return "ENOLCK";
		case CELL_FS_ERROR_ENOSYS        : return "ENOSYS";
		case CELL_FS_ERROR_ENOTEMPTY     : return "ENOTEMPTY";
		case CELL_FS_ERROR_ENOTSUP       : return "ENOTSUP";
		case CELL_FS_ERROR_ETIMEDOUT     : return "ETIMEDOUT";
		case CELL_FS_ERROR_EFSSPECIFIC   : return "EFSSPECIFIC";
		case CELL_FS_ERROR_EOVERFLOW     : return "EOVERFLOW";
		case CELL_FS_ERROR_ENOTMOUNTED   : return "ENOTMOUNTED";
		case CELL_FS_ERROR_ENOTMSELF     : return "ENOTMSELF";
		default							 : return "unknown";
	}
}
#endif

#if !__FINAL
namespace rage {
PARAM(enablefatalreaderror,"Enable fatal read errors (use -toggleN=enablefatalreaderror)");

	void SimulateFatalError()
	{
		if (PARAM_enablefatalreaderror.Get()) {
			// Intentional display, even in final builds.
			printf("Fatal disc error (DeviceRemote::Read) SIMULATED by command line toggle, simulating infinite retry.\n");
			static bool didDialog;
			if (!didDialog) {
				didDialog = true;
				if (cellSysmoduleLoadModule(CELL_SYSMODULE_SYSUTIL_GAME) != CELL_OK)
					printf("...failed to load fake dialog box\n");
				cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA, 0, NULL);
			}
			while (1)
				sysIpcSleep(1000);
		}
	}
}
#endif

#define CheckForFatalError(error) CheckForFatalError_(error,__LINE__)

static void CheckForFatalError_(CellFsErrno error,int line) {
#if !__FINAL
	SimulateFatalError();
#endif
	if (error == CELL_FS_ERROR_EBADF || error == CELL_FS_ERROR_EIO) {
		// Intentional display, even in final builds.

#if !__NO_OUTPUT
		const char *threadName = g_CurrentThreadName;
#else // !__NO_OUTPUT
		const char *threadName = "";
#endif // !__NO_OUTPUT

		printf("[%s] Fatal disc error encountered (%s) in line %d of file/device_psn.cpp, simulating infinite retry.\n",threadName,error==CELL_FS_ERROR_EBADF?"EBADF":"EIO",line);
		Assertf( false, "Fatal disc error encountered, check TTY for details." );
		while (1)
			sysIpcSleep(1000);
	}
}

static char cellGameDataPath[CELL_GAME_PATH_MAX];

static void GetGameDataPath()
{
	char contentInfoPath[CELL_GAME_PATH_MAX];
	int ret;
	CellGameContentSize contentSize;

	cellSysmoduleLoadModule(CELL_SYSMODULE_SYSUTIL_GAME);

	ret = cellGameDataCheck ( CELL_GAME_GAMETYPE_GAMEDATA, XEX_TITLE_ID, &contentSize );
	if (ret)
		Errorf("cellGameDataCheck can't find GAMEDATA (%d)",ret);
	else {
		ret = cellGameContentPermit ( contentInfoPath, cellGameDataPath );
		if (ret)
			Errorf("cellGameContentPermit can't retrieve the gamedata directory (%d)", ret);
	}

	cellSysmoduleUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
}

const char *fiDeviceLocal::FixName(char *outDest,int size,const char *src) {
	// Translate game: into wherever the executable was launched from.
	if (!strncmp(src,"game:",5)) {
		safecpy(outDest,"/dev_bdvd/PS3_GAME/USRDIR",size);

#if !__NO_OUTPUT
		if(sysBootManager::IsBootedFromAppHomeXMB())
			safecpy(outDest,"/app_home/PS3_GAME/USRDIR",size);
#endif

		safecat(outDest,src+5,size);
	}
	else if (!strncmp(src, "gamedata:", 9)) {
		if(!cellGameDataPath[0])
			GetGameDataPath();

		safecpy(outDest,cellGameDataPath,size);
		safecat(outDest,src+9,size);
	}
	else if (strncmp(src,"/app_home/",PREFIX_SIZE) && strncmp(src,"/dev_",5) && strncmp(src,"/host_root/",11)) {
		strcpy(outDest,"/app_home/");
		safecat(outDest,src,size);
	}
	else
		safecpy(outDest,src,size);
	while (strchr(outDest,'\\'))
		*strchr(outDest,'\\') = '/';
	return outDest;
}

void CheckForQuit()
{
	if (g_IsExiting) {
		printf("*** Blocking file request during shutdown.\n");
		while (1)
			sysIpcSleep(1000);
	}
}

#define SUPPORT_SDAT	0		// No longer necessary now that rpf's are more fully encrypted

fiHandle fiDeviceLocal::Open(const char *filename,bool readOnly) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	int fd = 0;
	FixName(namebuf,sizeof(namebuf),filename);
#if SUPPORT_SDAT
	const char *ext = strrchr(namebuf,'.');
	bool sdat = ext && !stricmp(ext,".sdat") && readOnly;
	CellFsErrno error = sdat
		? cellFsSdataOpen(namebuf, CELL_FS_O_RDONLY, &fd, NULL, 0) 
		: cellFsOpen(namebuf, readOnly? CELL_FS_O_RDONLY : CELL_FS_O_RDWR, &fd, NULL, 0);
#else
	CellFsErrno error = cellFsOpen(namebuf, readOnly? CELL_FS_O_RDONLY : CELL_FS_O_RDWR, &fd, NULL, 0);
#endif
	CheckForFatalError(error);

#if __DEVPROF
	if (error == CELL_FS_SUCCEEDED)
		sm_DeviceProfiler->OnOpen((fiHandle) fd, filename);
#endif // __DEVPROF

	return error != CELL_FS_SUCCEEDED ? fiHandleInvalid : (fiHandle)fd;
}

fiHandle fiDeviceLocal::OpenBulk(const char *filename,u64 &outBias) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	int fd = 0;
	FixName(namebuf,sizeof(namebuf),filename);
#if SUPPORT_SDAT
	const char *ext = strrchr(namebuf,'.');
	bool sdat = ext && !stricmp(ext,".sdat");
	CellFsErrno error = sdat
		? cellFsSdataOpen(namebuf, CELL_FS_O_RDONLY , &fd, NULL, 0) 
		: cellFsOpen(namebuf, CELL_FS_O_RDONLY , &fd, NULL, 0);
#else
	CellFsErrno error = cellFsOpen(namebuf, CELL_FS_O_RDONLY , &fd, NULL, 0);
#endif
	CheckForFatalError(error);

#if __DEVPROF
	if (error == CELL_FS_SUCCEEDED)
		sm_DeviceProfiler->OnOpen((fiHandle) fd, filename);
#endif // __DEVPROF

	outBias = 0;	
	return error != CELL_FS_SUCCEEDED ? fiHandleInvalid : (fiHandle)fd;
}

fiHandle fiDeviceLocalDrm::OpenBulkDrm(const char *filename,u64 &outBias, const void * pDrmKey) const {
	CheckForQuit();
	if (!pDrmKey)
		return OpenBulk(filename, outBias);

	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	int fd = 0;
	FixName(namebuf,sizeof(namebuf),filename);

	SceNpDrmKey licence;
	Assert(pDrmKey);
	sysMemCpy( licence.keydata, pDrmKey, sizeof(licence.keydata));

	CellFsErrno error = sceNpDrmOpen(&licence, namebuf, CELL_FS_O_RDONLY , &fd, NULL, 0);
	CheckForFatalError(error);
#if !__NO_OUTPUT
	if (error)
		Errorf("DRM OpenBulk(%s aka %s) returns %d(%s)",filename,namebuf,error,GetErrorString(error));
#endif
	outBias = 0;

#if __DEVPROF
	if (error == CELL_FS_SUCCEEDED)
		sm_DeviceProfiler->OnOpen((fiHandle) fd, filename);
#endif // __DEVPROF

	return error != CELL_FS_SUCCEEDED ? fiHandleInvalid : (fiHandle)fd;
}

fiHandle fiDeviceLocal::CreateBulk(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	int fd = 0;
	FixName(namebuf,sizeof(namebuf),filename);
	CellFsErrno error = cellFsOpen(namebuf, CELL_FS_O_CREAT | CELL_FS_O_RDWR | CELL_FS_O_TRUNC, &fd, NULL, 0);
	CheckForFatalError(error);

#if __DEVPROF
	if (error == CELL_FS_SUCCEEDED)
		sm_DeviceProfiler->OnOpen((fiHandle) fd, filename);
#endif // __DEVPROF

	return error != CELL_FS_SUCCEEDED ? fiHandleInvalid : (fiHandle)fd;
}

fiHandle fiDeviceLocal::Create(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	int fd = 0;
	FixName(namebuf,sizeof(namebuf),filename);
	CellFsErrno error = cellFsOpen(namebuf, CELL_FS_O_CREAT | CELL_FS_O_RDWR | CELL_FS_O_TRUNC, &fd, NULL, 0);
	if (error == CELL_FS_SUCCEEDED && strncmp(namebuf,"/app_home/",10))
		error = cellFsChmod(namebuf,CELL_FS_S_IWUSR | CELL_FS_S_IRUSR | CELL_FS_S_IRGRP | CELL_FS_S_IROTH);
	CheckForFatalError(error);

#if __DEVPROF
	if (error == CELL_FS_SUCCEEDED)
		sm_DeviceProfiler->OnOpen((fiHandle) fd, filename);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? static_cast<fiHandle>(fd) : fiHandleInvalid;
}

int fiDeviceLocal::Seek(fiHandle handle,int offset,fiSeekWhence whence) const {
	CheckForQuit();
	u64 pos;
	CellFsErrno error = cellFsLseek(static_cast<int>(handle), offset, whence, &pos);
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnSeek(handle, offset);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? pos : 0;
}

u64 fiDeviceLocal::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const {
	CheckForQuit();
	u64 pos;
	CellFsErrno error = cellFsLseek(static_cast<int>(handle), offset, whence, &pos);
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnSeek(handle, offset);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? pos : (u64)(s64)-1;
}

int fiDeviceLocal::Write(fiHandle handle,const void *buffer,int count) const {
	CheckForQuit();
	u64 bytesWritten;
	CellFsErrno error = cellFsWrite(static_cast<int>(handle), buffer, count, &bytesWritten);
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnWrite(handle, count);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? (int) bytesWritten : -1;
}

void (*fiDevice::sm_FatalReadError)(void);
#if !__FINAL
u32 fiDevice::sm_InjectReadError;
#endif

int fiDeviceLocal::Read(fiHandle handle,void *buffer,int count) const {
	CheckForQuit();
	u64 bytesRead;
#if __DEVPROF
	sm_DeviceProfiler->OnRead(handle, count, 0);
#endif // __DEVPROF

	CellFsErrno error = cellFsRead(static_cast<int>(handle), buffer, count, &bytesRead);
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(handle);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? (int) bytesRead : -1;
}

int fiDeviceLocal::ReadBulk(fiHandle handle,u64 offset,void *buffer,int count) const {
	CheckForQuit();

#if __DEVPROF
	sm_DeviceProfiler->OnRead(handle, count, offset);
#endif // __DEVPROF

	CellFsErrno error = CELL_FS_SUCCEEDED; 
	int amount = 0;
	while (count)
	{
		u64 bytesRead = 0;
		error = cellFsReadWithOffset(static_cast<int>(handle), offset, buffer, count, &bytesRead);
		CheckForFatalError(error);

		if (error != CELL_FS_SUCCEEDED)
			break;

		if (bytesRead)
		{
			offset += bytesRead;
			amount += bytesRead;
			count -= bytesRead;
			buffer = (void*)((char*)buffer + bytesRead);
		}
		else
		{
			break;
		}
	}
	Assert(count == 0);

#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(handle);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? amount : -1;
}

int fiDeviceLocal::WriteBulk(fiHandle handle,u64 offset,const void *buffer,int count) const {
	CheckForQuit();
	u64 bytesWritten;
	CellFsErrno error = cellFsWriteWithOffset(static_cast<int>(handle), offset, buffer, count, &bytesWritten);
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnWrite(handle, count/*, offset*/);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? (int) bytesWritten : -1;
}

int fiDeviceLocal::Close(fiHandle handle) const {
	CheckForQuit();
	CellFsErrno error = cellFsClose(static_cast<int>(handle));
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnClose(handle);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? 0 : -1;
}

int fiDeviceLocal::CloseBulk(fiHandle handle) const {
	CheckForQuit();
	CellFsErrno error = cellFsClose(static_cast<int>(handle));
	CheckForFatalError(error);

#if __DEVPROF
	sm_DeviceProfiler->OnClose(handle);
#endif // __DEVPROF

	return error == CELL_FS_SUCCEEDED ? 0 : -1;
}

u64 fiDeviceLocal::GetFileSize(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	u64 result = 0;
	CellFsStat stat;
	CellFsErrno error = cellFsStat(namebuf, &stat) ;
	CheckForFatalError(error);
	if (error == CELL_FS_SUCCEEDED)
		result = stat.st_size;
	return result;
}

// SYSTEMTIME _1970 = { 1970, 1, 0, 1, 0, 0, 0, 0 };
// SystemTimeToFileTime(&_1970, (FILETIME*)&ft);
// char buffer[100];
// _ui64toa(ft,buffer,10);
// printf("filetime of 1970 %s\n",buffer);

static const u64 FILETIME_BIAS = 116444736000000000ULL;
static const u64 CENTINANOSECONDS_PER_SECOND = 10000000ULL;

u64 fiDeviceLocal::GetFileTime(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	u64 result = 0;
	CellFsStat stat;
	CellFsErrno error = cellFsStat(namebuf, &stat);
	CheckForFatalError(error);
	if (error == CELL_FS_SUCCEEDED)
		result = (stat.st_mtime * CENTINANOSECONDS_PER_SECOND) + FILETIME_BIAS;
	return result;
}

bool fiDeviceLocal::SetFileTime(const char * filename,u64 timestamp) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);

	time_t converted = (timestamp - FILETIME_BIAS) / CENTINANOSECONDS_PER_SECOND;
	CellFsUtimbuf timebuf = { converted, converted };
	// If this doesn't compile, upgrade to 085.
	CellFsErrno error = cellFsUtime(namebuf, &timebuf);
	CheckForFatalError(error);
	return error == CELL_FS_SUCCEEDED;
}

bool fiDeviceLocal::Delete(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	CellFsErrno error = cellFsUnlink(namebuf);
	CheckForFatalError(error);
	return error == CELL_FS_SUCCEEDED;
}

bool fiDeviceLocal::Rename(const char *from,const char *to) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	char namebuf2[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),from);
	FixName(namebuf2,sizeof(namebuf2),to);
	CellFsErrno error = cellFsRename(namebuf,namebuf2);
	CheckForFatalError(error);
	return error == CELL_FS_SUCCEEDED;
}

u32 fiDeviceLocal::GetAttributes(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	u32 result = ~0U;
	CellFsStat stat;
	CellFsErrno error = cellFsStat(namebuf, &stat);
	CheckForFatalError(error);
	if (error == CELL_FS_SUCCEEDED) {
		result = 0;
		if (stat.st_mode & CELL_FS_S_IFDIR)
			result |= FILE_ATTRIBUTE_DIRECTORY;
		if (!(stat.st_mode & CELL_FS_S_IWUSR))
			result |= FILE_ATTRIBUTE_READONLY;
	}
	return result;
}


bool fiDeviceLocal::SetAttributes(const char * /*filename*/,u32 /*attributes*/) const {
	return false;
}


bool fiDeviceLocal::MakeDirectory(const char *filename) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	CellFsErrno error = cellFsMkdir(namebuf, CELL_FS_DEFAULT_CREATE_MODE_3 | CELL_FS_S_IWUSR | CELL_FS_S_IWGRP | CELL_FS_S_IWOTH);
	CheckForFatalError(error);
	return error == CELL_FS_SUCCEEDED;	
}


bool fiDeviceLocal::UnmakeDirectory(const char *pathname) const {
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),pathname);
	CellFsErrno error = cellFsRmdir(namebuf);
	CheckForFatalError(error);
	return error == CELL_FS_SUCCEEDED;	
}


fiHandle fiDeviceLocal::FindFileBegin(const char *filename,fiFindData &outData) const
{
	CheckForQuit();
	fiHandle fd;
	char namebuf[RAGE_MAX_PATH];
	FixName(namebuf,sizeof(namebuf),filename);

	CellFsErrno error = cellFsOpendir(namebuf, (int*) &fd);
	CheckForFatalError(error);
	if (error != CELL_FS_SUCCEEDED)
		return fiHandleInvalid;
	if (!FindFileNext(fd,outData)) {
		FindFileEnd(fd);
		return fiHandleInvalid;
	}
	else
		return fd;
}

bool fiDeviceLocal::FindFileNext(fiHandle handle,fiFindData &outData) const
{
	CheckForQuit();
	CellFsDirent dirEntry;
	u64 bytesRead;

	CellFsErrno error = cellFsReaddir(static_cast<int>(handle), &dirEntry, &bytesRead);
	CheckForFatalError(error);
	// return if no directory entry
	if (error != CELL_FS_SUCCEEDED || bytesRead == 0)
		return false;
	// copy filename
	strncpy(outData.m_Name, dirEntry.d_name, sizeof(outData.m_Name));
	outData.m_Size = 0;
	outData.m_LastWriteTime = 0;
	outData.m_Attributes = dirEntry.d_type == CELL_FS_TYPE_DIRECTORY? FILE_ATTRIBUTE_DIRECTORY : 0;
	return true;
}

int fiDeviceLocal::FindFileEnd(fiHandle handle) const
{
	CheckForQuit();
	CellFsErrno error = cellFsClosedir(static_cast<int>(handle));
	CheckForFatalError(error);
	return error != CELL_FS_SUCCEEDED ? -1 : 0;
}


bool fiDeviceLocal::SetEndOfFile(fiHandle handle) const {
	CheckForQuit();
	uint64_t pos;
	return cellFsLseek(handle, 0, CELL_FS_SEEK_CUR, &pos) == CELL_FS_SUCCEEDED &&
		cellFsFsync(handle) == CELL_FS_SUCCEEDED &&		// https://ps3.scedev.net/technotes/view/716
		cellFsFtruncate(handle, pos) == CELL_FS_SUCCEEDED;
}

fiDevice::RootDeviceId fiDeviceLocal::GetRootDeviceId(const char *name) const
{
	char namebuf[RAGE_MAX_PATH];
	FixName(namebuf,sizeof(namebuf),name);
	return strncmp(namebuf,"/dev_bdvd/",10) == 0 ? OPTICAL : HARDDRIVE;
}


u32 fiDeviceLocal::GetPhysicalSortKey(const char* name) const
{
	CheckForQuit();
	char namebuf[RAGE_MAX_PATH];
	FixName(namebuf,sizeof(namebuf),name);
	bool isOptical = !strncmp(namebuf,"/dev_bdvd/",10);

	u32 baseKey = (isOptical) ? 0 : HARDDRIVE_LSN;
	int device = 1 - (int) isOptical;		// "1 - optical" simply to make it match our convention of 0=ODD, 1=HDD

	const int maxPhysical = __FINAL ? 100 : 400;		// Allow for more in non-final where we might not have mega-RPFs
	static u32 hashes[maxPhysical], lsns[maxPhysical], hashCount;
	static u32 nextLsn[2] = { 80000, 2 };		// approximate value, important mostly because it's nonzero.
	u32 namehash = atStringHash(namebuf);
	for (u32 i=0; i<maxPhysical; i++)
		if (namehash == hashes[i])
			return lsns[i];
	if (hashCount == maxPhysical)	// This shouldn't ever happen.
		Quitf("Exceeded max capacity for cached hashes");
	u64 fs = GetFileSize(name);
	if (fs) {
		Displayf("Assigning emulated LSN %u to file '%s' on device %d",nextLsn[device],namebuf,device);
		hashes[hashCount] = namehash;
		lsns[hashCount] = nextLsn[device] | baseKey;
		nextLsn[device] += ((fs + 32767) & ~32767) >> 11;		// Some higher level code gets cranky if files don't start on a 32k boundary, so simulate that.
		return lsns[hashCount++];
	}
	return HARDDRIVE_LSN;
}

void fiDevice::ConvertFileTimeToSystemTime(u64 inTime,SystemTime &outTime) {
	CellRtcDateTime dt = {0};
	if (cellSysmoduleLoadModule(CELL_SYSMODULE_RTC) == CELL_OK) {
		cellRtcSetWin32FileTime(&dt, inTime);
		outTime.wYear = dt.year;
		outTime.wMonth = dt.month;
		outTime.wDay = dt.day;
		outTime.wDayOfWeek = 0;
		outTime.wHour = dt.hour;
		outTime.wMinute = dt.minute;
		outTime.wSecond = dt.second;
		outTime.wMilliseconds = (u16)(dt.microsecond / 1000);
		cellSysmoduleUnloadModule(CELL_SYSMODULE_RTC);
	}
	else
		printf("[RTC] unable to load module\n");
}

void fiDevice::GetLocalSystemTime(SystemTime & outTime)
{
	CellRtcDateTime dt = {0};
	if (cellSysmoduleLoadModule(CELL_SYSMODULE_RTC) == CELL_OK) {
		cellRtcGetCurrentClockLocalTime(&dt);
		outTime.wYear = dt.year;
		outTime.wMonth = dt.month;
		outTime.wDay = dt.day;
		outTime.wDayOfWeek = 0;
		outTime.wHour = dt.hour;
		outTime.wMinute = dt.minute;
		outTime.wSecond = dt.second;
		outTime.wMilliseconds = (u16)(dt.microsecond / 1000);
		cellSysmoduleUnloadModule(CELL_SYSMODULE_RTC);
	}
	else
		printf("[RTC] unable to load module\n");
}

void fiDevice::GetSystemTimeUtc(SystemTime & outTime)
{
	SceRtcDateTime oDateTime = {0};
	int iErrorCode = sceRtcGetCurrentClockUtc(&oDateTime);
	if (iErrorCode == SCE_OK) {
		outTime.wYear = oDateTime.year;
		outTime.wMonth = oDateTime.month;
		outTime.wDay = oDateTime.day;
		outTime.wDayOfWeek = 0;
		outTime.wHour = oDateTime.hour;
		outTime.wMinute = oDateTime.minute;
		outTime.wSecond = oDateTime.second;
		outTime.wMilliseconds = (u16)(oDateTime.microsecond / 1000);
	}
	else
	{
		Assertf(iErrorCode == SCE_OK, "[RTC] sceRtcGetCurrentClockLocalTime error code %x", iErrorCode);
	}
}

#endif	// __PPU
