// 
// file/compress_internal.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef COMPRESS_INTERNAL_H
#define COMPRESS_INTERNAL_H

// Initial width currently has to be two so that initial bias is correct when invoked from encode_length
const int c_InitialWidth = 2;
const int c_WidthStep = 2;

// Magic number (to allow for future configurability)
const rage::u8 c_Magic_1 = 0xDC;
const rage::u8 c_Magic_2 = 0xE0;

#define LOG_EVERYTHING	0

#if LOG_EVERYTHING
#define LOG1(a,b)	fprintf(logfile,a,b)
#define LOG2(a,b,c)	fprintf(logfile,a,b,c)
#else
#define LOG1(a,b)
#define LOG2(a,b,c)
#endif

#endif
