// 
// file/packfile.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_PACKFILE_H
#define FILE_PACKFILE_H

#include "atl/array.h"
#include "atl/string.h"
#include "data/resourceheader.h"
#include "file/device.h"
#include "string/string.h"

namespace rage {

class fiStream;

#if __XENON
#define DVD_DEVICE_NAME		"game:\\"
#elif __PPU
#define DVD_DEVICE_NAME		"/dev_bdvd/"
#else
#define DVD_DEVICE_NAME		""
#endif

struct fiPackHeader {		// Little-endian
	u32 m_Magic;			// 'RPF7'
	u32 m_EntryCount;		// number of fiPackEntry7 structures (immediately after this header)
	u32 m_NameHeapSize;		// Size allocated for name heap (always a multiple of 16, immediately after entries, can be up to 512k now)
	u32 m_Encrypted;		// AES encryption key id.  Key is project-specific.
};

/*
	Every entry is either a directory, a normal uncompressed file, a normal compressed file, or a resource.
	Uncompressed files (like audio streams) can be up to 4G in size.  Compressed files and all resources
	can be up to 16M in size.  File offsets are stored as an implicit multiple of 512 bytes and can reference
	up to four gigabytes in a single file (strictly speaking, the last file in the archive has to start just
	before the four gigabyte mark).
*/
struct fiPackEntry {		// Big-endian
	static const u64 FileOffset_IsDir = (1<<23)-1;
	static const int FileOffset_Shift = 9;
	static const u32 MaxConsumedSize = 0xFFFFFF;

	u64 DECLARE_BITFIELD_4(
		m_NameOffset,16,			// This is implicitly shifted left by m_NameShift in the containing archive.
		m_ConsumedSize,24,			// always zero for uncompressed files or directories.  Must be exact in order for Xcompress to work.
									// For resources that are at least MaxConsumedSize (currently only supported on PC) this entry is
									// set to MaxConsumedSize and the real length is hidden in the scrambled first 16 bytes of the resource
									// that are ordinarily unused.
		m_FileOffset,23,			// Contains FileOffset_IsDir if it's really a directory.  Otherwise lower FileOffset_Shift bits are zero.
		m_IsResource,1);			// nonzero if file is a resource

	union
	{
		struct 
		{
			u32 m_UncompressedSize;		// For files; file is compressed if m_ConsumedSize is nonzero.  Resources are always compressed.
			u32 m_Encrypted;			// Encryption key ID, or 0 if not encrypted. Only compressed files will be encrypted for now?
		} file;
		struct
		{
			u32 m_DirectoryIndex;		// For directories; first index in pack array of the directory
			u32 m_DirectoryCount;		// For directories; count of entries in the directory.
		} directory;
		struct  
		{
			datResourceInfo m_Info;
		} resource;
	} u;

	bool IsDir() const { return m_FileOffset == FileOffset_IsDir; }

	bool IsResource() const { return m_IsResource; }

	bool IsFile() const { return !IsDir() && !IsResource(); }

	bool IsCompressed() const { return m_ConsumedSize != 0; }

	u32 GetConsumedSize() const { FastAssert(IsFile() || IsResource()); return m_ConsumedSize? (u32)m_ConsumedSize : u.file.m_UncompressedSize; }

	u32 GetUncompressedSize() const { return IsResource()? u32(m_ConsumedSize) : u.file.m_UncompressedSize; }

// Disable inlining of GetFileOffset to fix Visual Studio 2010 (version 1600) compiler bug
#if __WIN32PC && defined(_MSC_VER) && _MSC_VER == 1600
	__declspec(noinline) u32 GetFileOffset() const { return u32(m_FileOffset) << FileOffset_Shift; }
#else
	u32 GetFileOffset() const { return u32(m_FileOffset) << FileOffset_Shift; }
#endif
	

	void SetFileOffset(u32 offset) { m_FileOffset = offset >> FileOffset_Shift; FastAssert(offset == GetFileOffset()); }

	int GetVersion() const { return IsResource()? (int) u.resource.m_Info.GetVersion() : 0; }
};

CompileTimeAssert(sizeof(fiPackEntry) == 16);



struct fiPackfileState;


class fiCollection: public fiDevice {
public:
	fiCollection() : m_isStreaming(true) {}

	static int GetCollectionIndex(u32 handle) { return (handle >> ArchiveShift) & ArchiveMask; }
	static fiCollection* GetCollection(u32 handle) { return sm_Collections[GetCollectionIndex(handle)]; }
	static u32 MakeHandle(u32 archiveIndex,u32 entryIndex) { 
		Assert(archiveIndex <= ArchiveMask); 
		Assert(entryIndex <= EntryMask); 
		return (archiveIndex << ArchiveShift) | entryIndex;
	}

	virtual void Shutdown() {}
	virtual fiHandle OpenBulkFromHandle(u32 handle,u64 &offset) const = 0;
	virtual const fiPackEntry& GetEntryInfo(u32 handle) const = 0;
	virtual u32 GetEntryPhysicalSortKey(u32 handle,bool uncached) const = 0;
	virtual const char *GetEntryName(u32 handle) const = 0;
	virtual const char *GetEntryFullName(u32 handle,char *dest,int destSize) const = 0;		// Slower
	virtual int GetEntryIndex(const char *name) const = 0;
	virtual u32 GetBasePhysicalSortKey() const = 0;
	virtual bool Prefetch(u32 handle) const = 0;
	virtual bool IsPackfile() const { return false; }
	virtual bool IsStreaming() const { return m_isStreaming; }
	virtual void SetStreaming(bool val) { m_isStreaming = val; }
protected:
	bool m_isStreaming;

	static fiCollection *sm_Collections[];
	static const u32 EntryMask = 0xFFFF;
	static const int ArchiveShift = 16;
	static const u32 ArchiveMask = 0xFFFF;
};

/*
	The fiPackfile device implements read-only zipfile access with compression support.
	Higher-level code is expected to call fiDevice::Mount with a new instance of fiPackfile
	that has succeeded in its Init call.  The zipfile will remember the device and handle
	of its original filename without consuming an fiStream permanently; in theory it is
	possible to even nest zipfiles within zipfiles -- which can be useful if you're trying
	to save memory, because the entire zipfile directory structure is kept resident in
	memory at all times.

	The file format is custom, and is created by the zip2rpf tool.

	Internally each element of a pathname is stored separately (like a real filesystem would)
	so deep directory structures are not any more expensive, particularly if they
	result in shorter element names.  For example, prop_sandiego_lamppost_01 would be
	stored more efficiently as prop/sandiego/lamppost_01 if there were many other props
	in sandiego.
	<FLAG COMPONENT>
*/
class fiPackfile: public fiCollection {
public:
	fiPackfile();
	virtual ~fiPackfile();

	static void SetReadAheadCacheCPU(int cpu);

	enum CacheMode { CACHE_NONE, CACHE_HARDDRIVE, CACHE_MEMORY, CACHE_INSTALLER_FILE };
	bool Init(const char *filename,bool keepNameTable,CacheMode cacheMode, char *packfileData = NULL);
	virtual void Shutdown();
	bool MountAs(const char *filename);

	// PURPOSE: Re-initialize a packfile after it has been UnInited().
	bool ReInit(const char *filename, bool readNameHeap, void *headerdata);

	// PURPOSE: Free up the memory of the pack file header data, but don't close the handle.
	void UnInit();

	// PURPOSE: Returns true if this packfile is initialized, false if UnInit() had been called on it.
	bool IsInitialized() const							{ return m_Entries != NULL; }

	bool IsUserHeader() {return m_IsUserHeader; }
	void SetDrmKey(const void * pDrmKey);

	// PURPOSE: Call this function after having called Shutdown() and Init()
    // on this packfile - it will look for all other packfiles depending on
	// it and update their file handles.
	//
	// PARAMS: The fiHandle of this fiPackfile before it had been shut down.
	void RemountDependentPackfiles(fiHandle oldHandle);

	virtual const fiPackEntry* FindEntry(const char *name) const;

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenDirect(const char * filename,bool readOnly) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Size(fiHandle handle) const;
	virtual u64 Size64(fiHandle handle) const;
	virtual int Close(fiHandle handle) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual u32 GetAttributes(const char *filename) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int CloseBulk(fiHandle handle) const;

	virtual fiHandle FindFileBegin(const char *directoryName,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;
	void SetRelativePath(const char *path) { m_RelativeOffset = (int)strlen(path); }
	void ClearRelativeOffset() { m_RelativeOffset = 0; }
	int GetRelativeOffset() const { return m_RelativeOffset; }

	virtual int GetResourceInfo(const char *name,datResourceInfo &outHeader) const;

	virtual u64 GetBulkOffset(const char *name) const;
	virtual u32 GetPhysicalSortKey(const char* pName) const;
	virtual u32 GetPackfileIndex() const { return m_SelfIndex; }

	const fiPackEntry*	GetEntries() const {return m_Entries;}
	u32	GetEntryCount() const {return m_EntryCount;}
	const char* GetNameHeap() const {return m_NameHeap;}
	int GetNameShift() const {return m_NameShift;}

	const atString& GetFullName() const {return m_Fullname;}

	virtual u32 GetEncryptionKey() const	{ return m_KeyId; }

	virtual const fiPackEntry& GetEntryInfo(u32 handle) const;
	virtual fiHandle OpenBulkFromHandle(u32 handle,u64 &offset) const;
	virtual u32 GetEntryPhysicalSortKey(u32 handle,bool) const;
	virtual const char *GetEntryName(u32 handle) const;
	virtual const char *GetEntryFullName(u32 handle,char *dest,int destSize) const;
	virtual u32 GetBasePhysicalSortKey() const;
	virtual int GetEntryIndex(const char *name) const;
	virtual bool IsRpf() const;
	virtual const char *GetDebugName() const;
	virtual bool Prefetch(u32) const;
	virtual bool IsPackfile() const { return true; }
	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const
	{
        if (m_ForceOnOdd)
			return OPTICAL;
		return m_Device->GetRootDeviceId(m_Fullname.c_str());
	}

    void SetForceOnOdd(bool val) { m_ForceOnOdd = val; }
	bool GetForceOnOdd() const	{ return m_ForceOnOdd; }

	u32 GetHeaderSize(bool withMetaData) const;

	u64 GetPackfileSize() const { return m_ArchiveSize; }
	u64 GetPackfileTime() const { return m_FileTime; }
	void SetPackfileTime(u64 newFileTime) { m_FileTime = newFileTime; }
	fiHandle GetPackfileHandle() const { return m_Handle; }
	void SetpackfileHandle(fiHandle handle)	{ m_Handle = handle; }

#if __WIN32PC
	// RETURNS: Returns NULL on error.  Otherwise free the buffer with delete[] when done.
	// Function only available on certain PC tool configurations, not for game-side use!
	u8* ExtractFileToMemory(u32 handle,u32 &outSize) const;
#endif

	// Serialize state to a stream (whatever is left after loading and unloading an rpf)
	void SaveState(fiStream*);
	// Restore serialized state from a stream.  Returns false if certain details (timestamps, etc) didn't actually match.
	bool RestoreState(fiStream*);

	bool GetKeepNameHeap() const { return m_KeepNameHeap; }
	void SetKeepNameHeap(bool keepNameHeap) { m_KeepNameHeap = keepNameHeap; }

protected:
	void		FillData(fiFindData &outData,u32 entryIndex) const;
	fiHandle	OpenInternal(const char *lookupName,bool readOnly) const;
	int			ReadInternal(fiPackfileState& state,void *outBuffer,int bufferSize) const;
	int			ReadInternalBulk(const fiPackfileState& state,u32 relativeOffset,void *outBuffer,int bufferSize) const;
	bool		InitFromImg(const char *filename);			// not in final builds.
	void		FillParentIndices(int dirIdx);
	typedef		u16 ParentIndexType;

	char *							m_NameHeap;				// nameheap for all filenames in the zipfile.
	ParentIndexType	*				m_ParentIndices;
	fiPackEntry *					m_Entries;				// master entry table; entry 0 is the root directory
	u32								m_EntryCount;
	fiHandle						m_Handle;				// (bulk) handle on the device of the zipfile itself
	u64								m_FileTime;				// file time of the zipfile itself
	u64								m_ArchiveBias;			// for nested zipfiles (sigh)
	u64								m_ArchiveSize;
	const fiDevice *				m_Device;				// device the zipfile itself lives on
	int								m_RelativeOffset;		// amount of incoming pathname to strip off
	char							m_Name[32];				// debug name
	atString						m_Fullname;				

	bool							m_IsXCompressed;
	bool							m_IsByteSwapped;
	u16								m_SelfIndex;
	u32								m_SortKey;				// sort key used to define position on a DVD
	const void *					m_pDrmKey;				// Drmkey used to decode drm content (ps3 only?)
	char *							m_MemoryBuffer;			// buffer to put packfile into if caching in memory
	bool							m_IsRpf;
	bool							m_IsCached;
	bool							m_IsInstalled;
	bool							m_IsUserHeader;			// If true, the user provided the memory for the header
															// data and is responsible for its clean-up.
	u32								m_CachedMetaDataSize;	// Cached meta data size, only valid after having
															// called Init/ReInit() at least once.
	u32								m_CachedDataSize;		// Cached size for the header (including both
															// the initial header as well as all entries).

	u32								m_KeyId;				// Encryption key ID
	unsigned char					m_NameShift;
    bool                            m_ForceOnOdd;
	bool							m_KeepNameHeap;			// Should we persist the name heap? i.e., don't delete on UnInit()/Shutdown(), etc.
#if !__PROFILE && !__FINAL
	bool							m_NameHeapInDebugHeap;	// If true, the name heap is in the debug heap. In this case,
															// In this case, the name heap remains resident until Shutdown() is
															// called. However, ReInit() with readNameHeap set to true will
															// reload the name heap.
#endif // !__PROFILE && !__FINAL
};

};	// namespace rage

#endif
