// 
// file/serialize.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_SERIALIZE_H
#define FILE_SERIALIZE_H

#include "token.h"
#include "asset.h"
#include "data/serialize.h"

namespace rage {

class fiStream;

class fiSerialize : public datSerialize {
protected:
public:
	fiSerialize(fiStream *stream, bool read, bool binary);
	virtual ~fiSerialize();

	virtual void Serialize(bool &val);
	virtual void Serialize(u8 &val);
	virtual void Serialize(s8 &val);
	virtual void Serialize(u16 &val);
	virtual void Serialize(s16 &val);
	virtual void Serialize(u32 &val);
	virtual void Serialize(s32 &val);
	virtual void Serialize(f32 &val);
	virtual void Serialize(u64 &val);

	// PURPOSE: Serialize a null-terminated string
	virtual void Put(const char *string);

	// PURPOSE: Unserialize a null-terminated string. Storage for the new string must be pre-allocated.
	//			UNLESS, size is 0, at which point the string is just discarded.
	virtual void Get(char *outString,int size);

protected:
	fiStream *			m_Stream;		// Used for binary files
	fiAsciiTokenizer	m_Token;		// Used for ascii files
	int					m_Version;		// Version of file
};

template<class T> bool fiSerializeTo( fiStream *stream, T &object, bool binary=true ) {
	// Create a serialize object
	fiSerialize ser(stream, false, binary);
	
	// Now, save it
	ser << object;
	
	return !ser.HasError();
}

template<class T> bool fiSerializeTo( const char *name, const char *ext, T &object, bool binary=true ) {
	fiStream *s = ASSET.Create(name, ext);
	if ( s ) {
		bool res = fiSerializeTo(s, object, binary);
		s->Close();
		return res;
	}
	return false;
}

template<class T> bool fiSerializeFrom( fiStream *stream, T &object ) {
	fiSerialize ser(stream, true, false);
	
	// Now, load it
	ser << object;
	
	return !ser.HasError();
}

template<class T> bool fiSerializeFrom( const char *name, const char *ext, T &object ) {
	fiStream *s = ASSET.Open(name, ext);
	if ( s ) {
		bool res = fiSerializeFrom( s, object );
		s->Close();
		return res;
	}
	return false;
}

}	// namespace rage

#endif	// FILE_SERIALIZE_H
