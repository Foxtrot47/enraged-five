#if RSG_DURANGO

#include "winrtbuffer.winrt.h"
#include "system/memops.h"

// WRL headers
// The following warnings cause issue with the WRL headers.
#pragma warning(push)
#pragma warning(disable: 4265) // Virtual functions without virtual destructor.
#pragma warning(disable: 4668) // Undefined preprocessor used in #if.

#include "grcore/config.h"

#include <wrl.h>
#include <robuffer.h>
#include <Windows.Storage.Streams.h>

// Reset warnings that WRL cause.
#pragma warning(pop)


namespace rage
{
// PURPUSE:	Wraps WinRT IBuffer functions inside their own namepspace.
namespace WinRTBuffer
{
// PURPUSE: Attempts to retrieves the raw buffer for an IBuffer.
// PARAMS:	buffer - the buffer to retrieve the raw data buffer from.
// NOTES:	This is instead of using the buffer wrapper example in the Durango samples and was
//			taken from https://forums.xboxlive.com/AnswerPage.aspx?qid=a1d37a74-97b3-4153-8251-2ea3e8e243a1&tgt=1
static void* GetRawBuffer(Windows::Storage::Streams::IBuffer^ buffer)
{
	using namespace Windows::Storage::Streams;
	IUnknown* unknown = reinterpret_cast<IUnknown*>(buffer);
	Microsoft::WRL::ComPtr<IBufferByteAccess> bufferByteAccess;
	HRESULT hr = unknown->QueryInterface(_uuidof(IBufferByteAccess), &bufferByteAccess);
	if (FAILED(hr))
	{
		return nullptr;
	}
	byte* bytes = nullptr;
	bufferByteAccess->Buffer(&bytes);
	return static_cast<void*>(bytes);
}

bool ReadBuffer(Windows::Storage::Streams::IBuffer^ buffer, void* dest, u32 size)
{
	bool result = false;
	if( Verifyf(dest != nullptr, "Invalid destination") &&
		Verifyf(buffer != nullptr, "Invalid buffer") &&
		Verifyf(buffer->Length >= size, "Data in buffer is not large enough!") )
	{
		void* rawBuff = GetRawBuffer(buffer);
		if(Verifyf(rawBuff != nullptr, "Failed to retrieve raw data buffer!"))
		{
			sysMemCpy(dest, rawBuff, size);
			result = true;
		}
	}

	return result;
}

Windows::Storage::Streams::IBuffer^ WriteBuffer(const void *src, u32 size)
{
	Windows::Storage::Streams::Buffer^ buffer = ref new Windows::Storage::Streams::Buffer(size);
	bool result = false;
	if( Verifyf(src != nullptr, "Invalid source!") &&
		Verifyf(buffer != nullptr, "Failed to create output buffer!") &&
		Verifyf(buffer->Capacity >= size, "Output buffer is too small for data!"))
	{
		void* rawBuff = GetRawBuffer(buffer);
		if(Verifyf(rawBuff != nullptr, "Failed to retrieve raw data buffer!"))
		{
			sysMemCpy(rawBuff, src, size);
			buffer->Length = size;
			result = true;
		}
	}
	
	// Clean up buffer on error and return nullptr.
	if(result == false)
	{
		buffer = nullptr;
	}

	return buffer;
}

}

}

#endif // RSG_DURANGO
