// 
// file/ftpserver.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if 0
#include "file/ftpserver.h"

#if !__FINAL

#include "file/tcpip.h"
#include "string/string.h"
#include "system/ipc.h"

#include <stdio.h>
#include <stdlib.h>

#if __WIN32
#include "file/winsock.h"
#include "system/xtl.h"
typedef int socklen_t;
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/process.h>
#endif


using namespace rage;

#define TCPIP fiDeviceTcpIp::GetInstance()
#define LOCAL fiDeviceLocal::GetInstance()

static void SendCommand(fiHandle socket,int id,const char *fmt,...) {
	va_list args;
	va_start(args,fmt);
	char buffer[256];
	sprintf(buffer,"%3d ",id);
	vsprintf(buffer+4,fmt,args);
#if !__OPTIMIZED
	Displayf("ftpserver: <<< [%s]",buffer);
#endif
	strcat(buffer,"\r\n");
	TCPIP.SafeWrite(socket,buffer,(int) strlen(buffer));
}

static u32 GetLocalAddress(fiHandle PPU_ONLY(control)) {
#if __PPU
	sockaddr_in sin;
	socklen_t namelen = sizeof(sin);
	if (getsockname((int) control /*listener -- doesn't work on PS3 */,(sockaddr*)&sin,&namelen) == 0)
		return sin.sin_addr.s_addr;
	else
		return 0;
#elif __XENON
	XNADDR xnaddr;
	DWORD status;
	while ((status = XNetGetTitleXnAddr( &xnaddr )) == XNET_GET_XNADDR_PENDING)
		Sleep(1);
	if( status != XNET_GET_XNADDR_TROUBLESHOOT )
		return xnaddr.ina.s_addr;
	else
		return 0;
#else
	return 0;
#endif
}


static void FixName(char *name,const char *cwd,const char *arg) {
	if (arg[0]=='/' || arg[0]=='\\' || strchr(arg,':'))
		strcpy(name,arg);
	else {
		strcpy(name,cwd);
		strcat(name,"/");
		strcat(name,arg);
	}
#if __XENON
	int sl = strlen(name);
	sysMemCpy(name,"devkit:",7);
	while (strchr(name,'/'))
		*strchr(name,'/') = '\\';
	if (sl && name[sl-1] == '\\')
		name[sl-1] = '\0';
#else
	// if (sl && name[sl-1] == '/')
	//	name[sl-1] = '\0';
#endif
}


static fiHandle ConnectData(fiHandle &listener,const char *otherside,int otherport) {
	fiHandle result = fiHandleInvalid;
	if (fiIsValidHandle(listener)) {
		result = TCPIP.Pickup(listener);
		TCPIP.Close(listener);
		listener = fiHandleInvalid;
	}
	else
		result = TCPIP.Connect(otherside,otherport);
	return result;
}


static void Server() {
	fiHandle listener = TCPIP.Listen(21,1);
	if (!fiIsValidHandle(listener))
		return;

	Displayf("ftpserver: Waiting for pickup...");
	fiHandle control = TCPIP.Pickup(listener);
	TCPIP.Close(listener);
	listener = fiHandleInvalid;
	if (!fiIsValidHandle(control))
		return;

	Displayf("ftpserver: Connected.");

	SendCommand(control,220,"Service ready for new user.");

	int size;
	char buffer[4096];
	char otherside[32] = "unknown";
	int otherport = 20;

#if __PPU
	char cwd[256] = "/dev_hdd0";
#else
	char cwd[256] = "/devkit";
#endif
	char origName[256] = "";

	while ((size = TCPIP.Read(control, buffer, sizeof(buffer))) > 0) {
		buffer[size] = '\0';
#if !__OPTIMIZED
		strtok(buffer,"\r\n");
		Displayf("ftpserver: >>>%s",buffer);
#endif
		char *cmd = strtok(buffer," \r\n");
		cmd[0] = (char) toupper(cmd[0]);
		if (cmd[1]) cmd[1] = (char) toupper(cmd[1]);
		if (cmd[2]) cmd[2] = (char) toupper(cmd[2]);
		if (cmd[3]) cmd[3] = (char) toupper(cmd[3]);
		if (!strcmp(cmd,"QUIT")) {
			SendCommand(control,221,"Later dude.");
			break;
		}
		else if (!strcmp(cmd,"USER")) {
			SendCommand(control,331,"Password required, but it can be anything.");
		}
		else if (!strcmp(cmd,"PASS")) {
			SendCommand(control,230,"User logged in.");
		}
		else if (!strcmp(cmd,"SYST")) {
			SendCommand(control,215,"UNIX Type: L8");
		}
		else if (!strcmp(cmd,"TYPE")) {
			char *arg = strtok(NULL," \r\n");
			SendCommand(control,200,"Type set to %s",arg);
		}
		else if (!strcmp(cmd,"CWD")) {
			char *arg = strtok(NULL,"\r\n");
			char tempCwd[256];
			FixName(tempCwd,cwd,arg);
			if (LOCAL.GetAttributes(tempCwd) != ~0U) {
				strcpy(cwd,tempCwd);
#if __XENON
				sysMemCpy(cwd,"/devkit",7);
				while (strchr(cwd,'\\'))
					*strchr(cwd,'\\') = '/';
#endif
				SendCommand(control,250,"CWD command successful. \"%s\" is current directory.",cwd);
			}
			else
				SendCommand(control,550,"CWD failed. \"%s\" : no such directory.",tempCwd);
		}
		else if (!strcmp(cmd,"PWD")) {
			SendCommand(control,257,"\"%s\" is current directory.",cwd);
		}
		else if (!strcmp(cmd,"PORT")) {
			char *h1 = strtok(NULL," ,\r\n");
			char *h2 = strtok(NULL," ,\r\n");
			char *h3 = strtok(NULL," ,\r\n");
			char *h4 = strtok(NULL," ,\r\n");
			char *p1 = strtok(NULL," ,\r\n");
			char *p2 = strtok(NULL," ,\r\n");
			sprintf(otherside,"%s.%s.%s.%s",h1,h2,h3,h4);
			otherport = (atoi(p1)<<8) + atoi(p2);
			SendCommand(control,200,"Port command successful (%s:%d)",otherside,otherport);
		}
		else if (!strcmp(cmd,"PASV")) {
			static u8 nextPort;
			++nextPort;
			if (!fiIsValidHandle(listener))
				listener = TCPIP.Listen(4096+nextPort,1);	// If you change the 4096, change the 16 in the SendCommand(..227...) below
			if (fiIsValidHandle(listener)) {
				u32 ia = GetLocalAddress(control);
				if (ia)
					SendCommand(control,227,"Entering Passive Mode (%d,%d,%d,%d,16,%d)",
						ia>>24,(ia>>16)&255,(ia>>8)&255,ia&255,nextPort);
				else
					SendCommand(control,502,"Couldn't get socket info.");
			}
			else
				SendCommand(control,502,"Couldn't find an open local port.");
		}
		else if (!strcmp(cmd,"NOOP")) {
			SendCommand(control,200,"Doing nothing.");
		}
		else if (!strcmp(cmd,"SIZE")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			SendCommand(control,213,"%" I64FMT "u",LOCAL.GetFileSize(name));
		}
		else if (!strcmp(cmd,"DELE")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			if (LOCAL.Delete(name))
				SendCommand(control,250,"File \"%s\" deleted.",name);
			else
				SendCommand(control,550,"\"%s\": no such file.",name);
		}
		else if (!strcmp(cmd,"MKD")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			if (LOCAL.MakeDirectory(name))
				SendCommand(control,257,"Directory \"%s\" created.",name);
			else
				SendCommand(control,550,"\"%s\": cannot create directory.",name);
		}
		else if (!strcmp(cmd,"RMD")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			if (LOCAL.UnmakeDirectory(name))
				SendCommand(control,250,"Directory \"%s\" removed.",name);
			else
				SendCommand(control,550,"\"%s\": cannot remove directory.",name);
		}
		else if (!strcmp(cmd,"RNFR")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			if (LOCAL.GetAttributes(name) != ~0U) {
				strcpy(origName,name);
				SendCommand(control,350,"File \"%s\" ready to rename.",name);
			}
			else
				SendCommand(control,550,"\"%s\": no such file.",name);
		}
		else if (!strcmp(cmd,"RNTO")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			if (!origName[0])
				SendCommand(control,550,"Need RNFR first!");
			else if (LOCAL.Rename(origName,name))
				SendCommand(control,250,"File renamed to \"%s\".",name);
			else
				SendCommand(control,550,"\"%s\": Unable to rename to this name.",name);
			origName[0] = '\0';
		}
		else if (!strcmp(cmd,"LIST")) {
			fiHandle data = ConnectData(listener,otherside,otherport);
			if (fiIsValidHandle(data)) {
				SendCommand(control,150,"Opening data connection for directory list.");
				fiFindData fd;
				char dir[256];
				FixName(dir,cwd,"");
				fiHandle h = LOCAL.FindFileBegin(dir,fd);
				if (fiIsValidHandle(h)) {
					do {
						// dummy up unix-style info, should make display in Explorer more accurate
						// we always leave off the year because unix normally displays either the
						// file time (to the nearest minute) or the file year, depending upon how
						// old the file is.
						if (fd.m_Name[0]!='.' || !(fd.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) {
							char localName[256];
							FixName(localName,cwd,fd.m_Name);
							char d = fd.m_Attributes & FILE_ATTRIBUTE_DIRECTORY? 'd' : '-';
							char w = fd.m_Attributes & FILE_ATTRIBUTE_READONLY? '-' : 'w';
							// drw-rw-rw-   1 user     group           0 Jul 13 11:35:04 foo
							u64 fileSize = LOCAL.GetFileSize(localName);
							u64 fileTime = LOCAL.GetFileTime(localName);
							fiDevice::SystemTime st;
							fiDevice::ConvertFileTimeToSystemTime(fileTime,st);
							static const char *months[] = { 0, "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec" };
							sprintf(buffer,"%cr%c-r%c-r%c-    1 user     group %11lld %s %2d %2d:%02d:%02d %s\r\n",
								d,w,w,w,fileSize,
								months[st.wMonth],st.wDay,st.wHour,st.wMinute,st.wSecond,
								fd.m_Name);
							TCPIP.SafeWrite(data,buffer,(int) strlen(buffer));
						}
					} while (LOCAL.FindFileNext(h,fd));
					LOCAL.FindFileEnd(h);
				}
				TCPIP.Close(data);
				SendCommand(control,226,"Transfer ok.");
			}
			else {
				SendCommand(control,425,"Cannot open data connection.");
			}
		}
		else if (!strcmp(cmd,"RETR")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			fiHandle file = LOCAL.Open(name,true);
			if (fiIsValidHandle(file)) {
				u64 size = LOCAL.GetFileSize(name);
				fiHandle data = ConnectData(listener,otherside,otherport);
				if (fiIsValidHandle(data)) {
					SendCommand(control,150,"Opening data connection for \"%s\" (%" I64FMT "u bytes)",name,size);
					int amount;
					while ((amount = LOCAL.Read(file,buffer,sizeof(buffer))) > 0) {
						TCPIP.SafeWrite(data,buffer,amount);
					}
					TCPIP.Close(data);
					SendCommand(control,226,"File sent ok.");
				}
				else {
					SendCommand(control,425,"Cannot open data connection.");
				}
				LOCAL.Close(file);
			}
			else
				SendCommand(control,550,"File not found.");
		}
		else if (!strcmp(cmd,"STOR")) {
			char *arg = strtok(NULL,"\r\n");
			char name[256];
			FixName(name,cwd,arg);
			fiHandle file = LOCAL.Create(name);
			if (fiIsValidHandle(file)) {
				fiHandle data = ConnectData(listener,otherside,otherport);
				if (fiIsValidHandle(data)) {
					SendCommand(control,150,"Opening data connection for \"%s\"",name);
					int amount;
					while ((amount = TCPIP.Read(data,buffer,sizeof(buffer))) > 0) {
						LOCAL.SafeWrite(file,buffer,amount);
					}
					TCPIP.Close(data);
					SendCommand(control,226,"File received ok.");
				}
				else {
					SendCommand(control,425,"Cannot open data connection.");
				}
				LOCAL.Close(file);
			}
			else
				SendCommand(control,550,"File not found, or could not be created: '%s'",name);
		}
		else if (!strcmp(cmd,"SITE")) {
			char *arg = strtok(NULL," \r\n");
			if (arg && !stricmp(arg,"EXEC")) {
#if __PPU
				char *exe = strtok(NULL," \r\n");
				if (exe) {
					const char *argv[2] = { exe, NULL };
					const char *envp[1] = { NULL };
					SendCommand(control,226,"Launching executable '%s'...",exe);
					// Close the sockets to avoid a crash in exitspawn.
					if (listener != fiHandleInvalid)
						TCPIP.Close(listener);
					TCPIP.Close(control);
					sys_game_process_exitspawn(exe, argv, envp, 0, 0, 2048, SYS_PROCESS_PRIMARY_STACK_SIZE_256K);
					Displayf("sys_game_process_exitspawn failed?");
					sys_process_exit(0);
				}
				else
					SendCommand(control,500,"Missing parameter after SITE EXEC.");
#endif
			}
			else
				SendCommand(control,500,"Unrecognized SITE command '%s'.",arg);
		}
		else
			SendCommand(control,500,"Sorry dude, I'm really stupid.  I don't understand '%s'",cmd);
	}

	if (listener != fiHandleInvalid)
		TCPIP.Close(listener);
	TCPIP.Close(control);
}


namespace rage {

void fiFtpServer(void*) {
	while (1)
		Server();
}

void fiLaunchFtpServerThread() {
	sysIpcCreateThread(fiFtpServer,NULL,sysIpcMinThreadStackSize,PRIO_HIGHEST,"FTP Server Thread");
}

}	// namespace rage

#endif
#endif	// 0
