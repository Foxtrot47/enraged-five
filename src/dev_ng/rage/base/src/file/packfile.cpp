// 
// file/packfile.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "packfile.h"

#include "diag/tracker.h"
#include "file/asset.h"
#include "file/diskcache.h"
#include "paging/streamer.h"
#include "paging/streamer_internal.h"
#include "system/bootmgr.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/messagequeue.h"
#include "string/stringhash.h"
#include "data/aes.h"
#include "atl/staticpool.h"

#define USE_XCOMPRESS	(__XENON)

// For PC-based tools (leave out release builds for now)
#define SUPPORT_OTHER_PLATFORM		(__WIN32PC && (__TOOL || __DEV || __RESOURCECOMPILER))

#if USE_XCOMPRESS
#include "system/xtl.h"
#include "system/xcompress_settings.h"
#else
#include "zlib/zlib.h"
#endif

#include <stdlib.h>

namespace rage {

/* 
	RPF7 format:
	HEADER:
		16-byte header (fiPackFileHeader).  Target endian, unencrypted.
		Contains magic number, entry count, name heap size (always a multiple of 16), and encryption flags.
		MSB of name heap size is set if the files are compressed with XCompress instead of zlib.
		The endian of the magic number determines what platform it was created for ('RPF7' in native order)
	MAIN DIRECTORY:
		hdr.m_EntryCount * sizeof(fiPackEntry) in size, target endian, encrypted.  This is followed
		by the name heap, encrypted.
	BODY:
		The body of the archive is next, starting on the next 512b boundary after the header and main directory
		and name heap.  All files start on a 512b boundary; this can be adjusted by changing fiPackEntry::FileOffset_Shift.
		The body of the archive will always end on a 2k boundary.  The last file in an archive must start *before*
		offset 0xFFFFFE00 so the effective maximum file size is 4 gigabytes.
*/		


PARAM(dumprpfinfo,"Dump RPF file info at mount time");

// MaxPackfiles line used to have this comment after it:	
	// If we raise this past 256 we'll need to decrement fiCollection::ArchiveShift again.
// but it seems totally out of date - we have 16 bits worth of size to fit into and the no. of pack files has been >256 for a looooong time
extern const int MaxPackfiles = 1024+718+650/*HD texture pack*/+256/*even more packs*/+256/*more DLC for 2018*/+256/*more DLC for 2019*/+256/*more DLC for 2020*/+256/*more DLC for 2021*/;	

fiCollection* fiCollection::sm_Collections[MaxPackfiles];		// Entry 0 is never used. (except by the pgRawStreamer)

fiPackfile::fiPackfile() 
	: m_NameHeap(NULL)
	, m_Entries(NULL) 
	, m_Handle(fiHandleInvalid)
	, m_FileTime(0)
	, m_Device(NULL)
	, m_pDrmKey(NULL)
	, m_MemoryBuffer(NULL)
    , m_ForceOnOdd(false)
	, m_KeepNameHeap(false)
	, m_IsUserHeader(false)
	, m_IsXCompressed(false)
	, m_SelfIndex(0)
	, m_CachedMetaDataSize(0)
	, m_CachedDataSize(0)
	, m_EntryCount(0)
#if !__PROFILE && !__FINAL
	, m_NameHeapInDebugHeap(false)
#endif // !__PROFILE && !__FINAL
{
	m_Name[0] = 0;
	for (u16 i=1; i<MaxPackfiles; i++)
		if (!fiCollection::sm_Collections[i]) {
			m_SelfIndex = i;
			fiCollection::sm_Collections[i] = this;
			break;
		}

	if (!m_SelfIndex)
	{
#if __BANK
		for (u16 i=1; i<MaxPackfiles; i++)
		{
			Displayf("%d - %s", i, fiCollection::sm_Collections[i]->GetDebugName());
		}
#endif // __BANK
		Quitf(ERR_FIL_PACK_1,"Too many archives mounted, raise array size of sm_Collections.");
	}
}


void fiPackfile::UnInit() {
	if(m_MemoryBuffer)
	{
		delete[] m_MemoryBuffer;
		m_MemoryBuffer = NULL;
	}

	// If we're using a user-provided buffer, it's not our responsibility to clean up.
	if (!m_IsUserHeader) {
		delete [] m_Entries;
	}

	m_Entries = NULL;

	if(!m_KeepNameHeap)
	{
	#if !__PROFILE && !__FINAL
		// If the name heap is in the debug heap, it may as well stay there.
		if (!m_NameHeapInDebugHeap)
	#endif // !__PROFILE && !__FINAL
		{
			delete[] m_NameHeap;
			m_NameHeap = NULL;
		}
	}
}

void fiPackfile::Shutdown() {
	UnInit();

	if(!m_KeepNameHeap)
	{
		// Delete name heap, even if it's in debug heap
		delete[] m_NameHeap;
		m_NameHeap = NULL;
	#if !__PROFILE && !__FINAL
		m_NameHeapInDebugHeap = false;
	#endif
	}

	if (m_Device && fiIsValidHandle(m_Handle))
		m_Device->CloseBulk(m_Handle);
	m_Device = NULL;
	m_Handle = fiHandleInvalid;
}


fiPackfile::~fiPackfile() {

	// Make sure to set to false before calling Shutdown(), 
	// so that it will appropriately clean up the name heap!
	m_KeepNameHeap = false;

	Shutdown();
	sm_Collections[m_SelfIndex] = NULL;
}

/*
	char *							m_NameHeap;				// nameheap for all filenames in the zipfile.
	ParentIndexType	*				m_ParentIndices;
	fiPackEntry *					m_Entries;				// master entry table; entry 0 is the root directory
	u32								m_EntryCount;
	fiHandle						m_Handle;				// (bulk) handle on the device of the zipfile itself
	u64								m_FileTime;				// file time of the zipfile itself
	u64								m_ArchiveBias;			// for nested zipfiles (sigh)
	u64								m_ArchiveSize;
	const fiDevice *				m_Device;				// device the zipfile itself lives on
	int								m_RelativeOffset;		// amount of incoming pathname to strip off
	char							m_Name[32];				// debug name
	atString						m_Fullname;				

	bool							m_IsXCompressed;
	bool							m_IsByteSwapped;
	u16								m_SelfIndex;
	u32								m_SortKey;				// sort key used to define position on a DVD
	const void *					m_pDrmKey;				// Drmkey used to decode drm content (ps3 only?)
	char *							m_MemoryBuffer;			// buffer to put packfile into if caching in memory
	bool							m_IsRpf;
	bool							m_IsCached;
	bool							m_IsInstalled;
	bool							m_IsUserHeader;			// If true, the user provided the memory for the header
															// data and is responsible for its clean-up.
	u32								m_CachedMetaDataSize;	// Cached meta data size, only valid after having
															// called Init/ReInit() at least once.
	u32								m_CachedDataSize;		// Cached size for the header (including both
															// the initial header as well as all entries).

	u32								m_KeyId;				// Encryption key ID
	unsigned char					m_NameShift;
	bool                            m_ForceOnOdd;
	bool							m_KeepNameHeap;			// Should we persist the name heap? i.e., don't delete on UnInit()/Shutdown(), etc.
#if !__PROFILE && !__FINAL
	bool							m_NameHeapInDebugHeap;	// If true, the name heap is in the debug heap. In this case,
	*/

void fiPackfile::SaveState(fiStream *S)
{
	S->Write(&m_EntryCount,sizeof(m_EntryCount));
	S->Write(&m_ArchiveSize,sizeof(m_ArchiveSize));
	S->Write(&m_FileTime,sizeof(m_FileTime));
	S->Write(&m_RelativeOffset,sizeof(m_RelativeOffset));
	S->Write(m_Name,sizeof(m_Name));
	int sl = istrlen(m_Fullname);
	S->PutCh((u8)sl);
	S->Write(m_Fullname.c_str(),sl);
	S->Write(&m_IsXCompressed,sizeof(m_IsXCompressed));
	S->Write(&m_IsRpf,sizeof(m_IsRpf));
	S->Write(&m_IsCached,sizeof(m_IsCached));
	S->Write(&m_IsInstalled,sizeof(m_IsInstalled));
	S->Write(&m_CachedMetaDataSize,sizeof(m_CachedMetaDataSize));
	S->Write(&m_CachedDataSize,sizeof(m_CachedDataSize));
	S->Write(&m_KeyId,sizeof(m_KeyId));
	S->Write(&m_NameShift,sizeof(m_NameShift));
}

bool fiPackfile::RestoreState(fiStream *S)
{
	S->Read(&m_EntryCount,sizeof(m_EntryCount));
	S->Read(&m_ArchiveSize,sizeof(m_ArchiveSize));
	S->Read(&m_FileTime,sizeof(m_FileTime));
	S->Read(&m_RelativeOffset,sizeof(m_RelativeOffset));
	S->Read(m_Name,sizeof(m_Name));
	int sl = S->GetCh();
	char *temp = Alloca(char,sl+1);
	S->Read(temp,sl);
	temp[sl] = 0;
	m_Fullname = temp;
	S->Read(&m_IsXCompressed,sizeof(m_IsXCompressed));
	S->Read(&m_IsRpf,sizeof(m_IsRpf));
	S->Read(&m_IsCached,sizeof(m_IsCached));
	S->Read(&m_IsInstalled,sizeof(m_IsInstalled));
	S->Read(&m_CachedMetaDataSize,sizeof(m_CachedMetaDataSize));
	S->Read(&m_CachedDataSize,sizeof(m_CachedDataSize));
	S->Read(&m_KeyId,sizeof(m_KeyId));
	S->Read(&m_NameShift,sizeof(m_NameShift));

	m_Device = fiDevice::GetDevice(m_Fullname,true);
	m_Handle = m_Device->OpenBulkDrm(m_Fullname,m_ArchiveBias,m_pDrmKey);
	m_SortKey = m_Device->GetPhysicalSortKey(m_Fullname);
#if __PS3
	u64 checkFileTime = m_Device->GetFileTime(m_Fullname);
#endif
	u64 checkArchiveSize = 0;

	if (fiIsValidHandle(m_Handle) && m_Device->IsRpf())
	{
		fiPackfile* rpfDevice = (fiPackfile*)m_Device->GetRpfDevice();
		const fiPackEntry* e = rpfDevice->FindEntry(m_Fullname + rpfDevice->GetRelativeOffset());
		checkArchiveSize = e->u.file.m_UncompressedSize;
	}
	else
		checkArchiveSize = m_Device->GetFileSize(m_Fullname);

	// printf("%s - %lx/%lx %lx/%lx\n",m_Fullname.c_str(),checkArchiveSize,m_ArchiveSize,checkFileTime,m_FileTime);
	return checkArchiveSize == m_ArchiveSize PS3_ONLY(&& checkFileTime == m_FileTime);
}


void fiPackfile::RemountDependentPackfiles(fiHandle oldHandle) {
	fiHandle newHandle = m_Handle;

	for (int x=0; x<MaxPackfiles; x++) {
		fiCollection *collection = sm_Collections[x];

		if (collection && collection->IsRpf()) {
			fiPackfile *packfile = static_cast<fiPackfile *>(collection);

			if (packfile->m_Handle == oldHandle) {
				// Remount.
				packfile->m_Handle = newHandle;
				Assertf(packfile->m_Device == this || packfile == this, "It's actually depending on %s, not %s", packfile->m_Device->GetDebugName(), GetDebugName());
				packfile->m_SortKey = packfile->m_Device->GetPhysicalSortKey(packfile->m_Fullname);
			}
		}
	}
}

bool fiPackfile::Init(const char *filename,bool readNameHeap,CacheMode cacheMode, char *packfileData) {
	char fullname[RAGE_MAX_PATH];
	ASSET.FullReadPath(fullname,sizeof(fullname),filename,""); // To allow other extensions than .rpf
	safecpy(m_Name,ASSET.FileName(fullname));
	m_Fullname = fullname;

	m_Device = fiDevice::GetDevice(fullname,true);
	if(!Verifyf(m_Device,"Cannot find device for filename '%s'",fullname))
	{
		return false;
	}


	if (cacheMode == CACHE_NONE && m_Device->GetRootDeviceId(fullname) == fiDevice::HARDDRIVE)
	{
		Displayf("Packfile %s is on HDD, promoting to CACHE_INSTALLER_FILE", fullname);
		cacheMode = CACHE_INSTALLER_FILE;
	}

	Assert(!fiIsValidHandle(m_Handle));	// Calling Init() twice without Shutdown() inbetween?

	m_Handle = m_Device->OpenBulkDrm(fullname,m_ArchiveBias, m_pDrmKey);
	if (!fiIsValidHandle(m_Handle))
		return false;
	m_FileTime = m_Device->GetFileTime(fullname);

	// if cache mode is cache in memory then load file into buffer and create memory device file
	if(cacheMode == CACHE_MEMORY)
	{
		u32 size = (u32)m_Device->GetFileSize(fullname);

		if (packfileData)
		{
			m_MemoryBuffer = packfileData;
		}
		else
		{
			m_MemoryBuffer = rage_new char[size];
			m_Device->ReadBulk(m_Handle, m_ArchiveBias, m_MemoryBuffer, size);
			m_Device->CloseBulk(m_Handle);
		}

		fiDeviceMemory::MakeMemoryFileName(fullname, 256, m_MemoryBuffer, size, false, filename);
		m_Device = fiDevice::GetDevice(fullname,true);
		m_Handle = m_Device->OpenBulk(fullname,m_ArchiveBias);

		if (packfileData)
			m_MemoryBuffer = NULL;		// We already set the memory file to free on close, so we shouldn't free the memory up!
	}

	if (m_Device->IsRpf())
	{
		fiPackfile* rpfDevice = (fiPackfile*)m_Device->GetRpfDevice();
		const fiPackEntry* e = rpfDevice->FindEntry(filename + rpfDevice->GetRelativeOffset());
		m_ArchiveSize = e->u.file.m_UncompressedSize;
	}
	else
	{
		m_ArchiveSize = m_Device->GetFileSize(fullname);
	}

	if (m_ArchiveSize < 16 /* 2048 - if you ENABLE_NO_BUFFERING in file/device_win32.cpp */) {
		Displayf("'%s' is not a valid rpf or zip file (way too small)",filename);
		m_Device->CloseBulk(m_Handle);
		m_Handle = fiHandleInvalid;
		return false;
	}

	m_SortKey = m_Device->GetPhysicalSortKey(fullname);


	//u32 firstSortKey = m_SortKey;
	//Displayf("Packfile %s: Sortkey %x", fullname, m_SortKey);

	if (!__FINAL XENON_ONLY(|| m_ForceOnOdd)) {
		// If we know nothing about the sort key, synthesize one.  Assumes archives aren't mounted more than once.
		if (m_SortKey == HARDDRIVE_LSN XENON_ONLY(|| m_ForceOnOdd)) {
			if (cacheMode == CACHE_INSTALLER_FILE XENON_ONLY(&& !m_ForceOnOdd)) {
				// If this is an installer file, just give it an LSN that indicates it's
				// on the hard disk, and preferably a value other than HARDDRIVE_LSN, since that
				// value is often used to indicate "unknown LSN".
				m_SortKey = HARDDRIVE_LSN + 1;
			} else {
				static u32 nextSortKey = HARDDRIVE_LSN | 1600;
				// If this file would cross the layer0/layer1 boundary, force it to the boundary instead.
				if (nextSortKey < LAYER1_LSN && nextSortKey + u32(m_ArchiveSize >> 11) > LAYER1_LSN)
					nextSortKey = LAYER1_LSN;
				// Displayf("[PACKFILE] Generating fake sort key %u for '%s'",nextSortKey,fullname);
				m_SortKey = nextSortKey;
				nextSortKey += u32(m_ArchiveSize >> 11);
				nextSortKey = (nextSortKey + 15) & ~15;
			}
		}
	}

#if __XENON && !__NO_OUTPUT
	char finalString[256];
	formatf(finalString, "*@ %s sortkey: %x (forceODD=%d)\n", fullname, m_SortKey, m_ForceOnOdd);
	OutputDebugString(finalString);

	//Displayf("**SK**\t%s\t%x\t%x\t%llx\t%d", fullname, firstSortKey, m_SortKey, m_Device->GetFileSize(fullname), m_SortKey);
#endif

	// TODO: Establish whether it's worth caching the archive headers or not.
	if (cacheMode == CACHE_HARDDRIVE && fiDiskCache::IsCacheEnabled()) {
		// m_ArchiveSize, m_FileTime, m_Handle, and m_SortKey need to be initialized by now.
		m_Device = fiDiskCache::GetCachedDevice(fullname,this);
		m_IsCached = true;
	}
	else
		m_IsCached = false;

	// If we didn't want it to be cached, remember that
	m_IsInstalled = (cacheMode == CACHE_NONE);

#if !__TOOL
	// const char *layer = m_SortKey>=fiDevice::HARDDRIVE_LSN? "hard drive" :  (m_SortKey >= fiDevice::LAYER1_LSN? "optical layer 1" : "optical layer 0");
	// Displayf("[PACKFILE] Loading archive '%s' from %s",fullname,layer);
#endif

	m_RelativeOffset = int(ASSET.FileName(fullname) - fullname);

	m_IsRpf = false;
	return ReInit(filename, readNameHeap, NULL);
}

void fiPackfile::FillParentIndices(int dirIdx)
{
	Assert(m_Entries[dirIdx].IsDir());
	int start = m_Entries[dirIdx].u.directory.m_DirectoryIndex;
	int stop = start + m_Entries[dirIdx].u.directory.m_DirectoryCount;
	for (int i=start; i<stop; i++) {
		Assign(m_ParentIndices[i],dirIdx);
		if (m_Entries[i].IsDir())
			FillParentIndices(i);
	}
}


bool fiPackfile::ReInit(const char *filename, bool readNameHeap, void *headerData)
{
	char *headerDataPtr = (char *) headerData;

	m_IsUserHeader = headerData != NULL;

	// Don't initialize twice without uninitializing first.
	Assertf(m_Entries == NULL, "Packfile '%s' intitialized twice without UnInit() inbetween", GetDebugName());

	// Only read the first five words, that's enough to identify the file type and avoids
	// lockups on very small (less than 2k) .zip (ie not rpf) files.
	fiPackHeader hdr;

	unsigned int transformitSelector = 0;
#if RSG_CPU_X64 
	//@@: location FIPACKFILE_REINIT_ON_SELECTOR
	transformitSelector = atStringHash(ASSET.FileName(filename));
	transformitSelector+=(int)m_ArchiveSize;
	transformitSelector = transformitSelector % TFIT_NUM_KEYS;
#else
	(void)filename;
#endif
	if (!headerData) {
		if (!m_Device->IsRpf() WIN32PC_ONLY(&& !m_Device->IsMaskingAnRpf()))
		{
			m_Device->Seek(m_Handle, 0, seekSet);
		}
		m_Device->ReadBulk(m_Handle,m_ArchiveBias,&hdr,16);
	} else {
		hdr = *((fiPackHeader *) headerDataPtr);
		headerDataPtr += sizeof(fiPackHeader);
	}

	m_IsByteSwapped = false;
	if (hdr.m_Magic != 'RPF7') {
#if SUPPORT_OTHER_PLATFORM
		if (hdr.m_Magic == '7FPR')
		{
			hdr.m_EntryCount = sysEndian::Swap(hdr.m_EntryCount);
			hdr.m_NameHeapSize = sysEndian::Swap(hdr.m_NameHeapSize);
			hdr.m_Encrypted = sysEndian::Swap(hdr.m_Encrypted);
			m_IsByteSwapped = true;
		}
		else
#endif
		{
			Displayf("'%s' is not a valid rpf for this platform (magic=%x, wanted %x)",filename,hdr.m_Magic,'RPF7');
			m_Device->CloseBulk(m_Handle);
			m_Handle = fiHandleInvalid;
			return false;
		}
	}
	// The MSB will be set if data is compressed with XCompress instead of Zlib.
	m_IsXCompressed = (hdr.m_NameHeapSize & 0x80000000) != 0;
	m_NameShift = (hdr.m_NameHeapSize >> 28) & 7;
	hdr.m_NameHeapSize &= 0xFFFFFFF;
	RAGE_TRACK(Packfile);

	m_EntryCount = hdr.m_EntryCount;
	m_IsRpf = true;

	{
		u32 entrySize = (hdr.m_EntryCount * sizeof(fiPackEntry) + 15) & ~15U;

		if (headerDataPtr) {
			m_Entries = (fiPackEntry*) headerDataPtr;
			headerDataPtr += entrySize;
		} else {
			m_Entries = (fiPackEntry*) rage_new char[entrySize];
			m_Device->ReadBulk(m_Handle,m_ArchiveBias + sizeof(hdr),m_Entries,entrySize);
			m_CachedDataSize = sizeof(hdr) + entrySize;
		}
#if !__FINAL
		if (!hdr.m_Encrypted)
			Quitf("%s - Unencrypted archives no longer supported.",filename);
# if !__WIN32PC 
		else if (hdr.m_Encrypted != AES_KEY_ID_DEFAULT)
			Warningf("%s - Outdated encryption key '%x'",filename,hdr.m_Encrypted);
# endif
#endif
		m_KeyId = hdr.m_Encrypted;
		{
			#if RSG_PC && (RSG_CPU_X86 || RSG_CPU_X64 || __RESOURCECOMPILER || RSG_TOOL) && !__RGSC_DLL
				bool isTfit = AES::isTransformITKeyId(m_KeyId);
				if(isTfit)
				{
					switch(m_KeyId)
					{
					case AES_MULTIKEY_ID_GTA5_PS4:
						Assertf(false, "%s - Not encrypted with a TransformIT key! RPF is encrypted for PS4.", filename);
						break;
					case AES_MULTIKEY_ID_GTA5_XBOXONE:
						Assertf(false, "%s - Not encrypted with a TransformIT key! RPF is encrypted for XB1.", filename);
						break;
					default:
						break;
					}
				}
				
				#if __RESOURCECOMPILER || RSG_TOOL
				if(AES::isTransformITKeyId(m_KeyId))
				{
					AES::TransformITDecrypt(transformitSelector, m_Entries, entrySize);
				}
				else
				{
					AES::Decrypt(m_KeyId, transformitSelector, m_Entries, entrySize);
				}
				#else
				AES::TransformITDecrypt(transformitSelector, m_Entries, entrySize);
				#endif
			#else
				AES::Decrypt(m_KeyId, transformitSelector, m_Entries, entrySize);
			#endif
		}

		// Set our AES key if this one uses the old-school boolean value.
		for (u32 x=0; x<m_EntryCount; x++) {
			fiPackEntry &e = m_Entries[x];
			if (e.IsFile()) {
				if (e.u.file.m_Encrypted == 1) {
					e.u.file.m_Encrypted = hdr.m_Encrypted;
				}
			}
		}

#if SUPPORT_OTHER_PLATFORM
		if (hdr.m_Magic == '7FPR') {
			for (u32 i=0; i<m_EntryCount; i++) {
				u64 &dw0 = *(u64*)&m_Entries[i];
				u32 &w2 = m_Entries[i].u.file.m_UncompressedSize;
				u32 &w3 = m_Entries[i].u.file.m_Encrypted;
				dw0 = sysEndian::Swap(dw0);
				w2 = sysEndian::Swap(w2);
				w3 = sysEndian::Swap(w3);
			}
		}
#endif

#if !__PROFILE && !__FINAL
		if (m_NameHeapInDebugHeap) {
			// We already have a name heap. That's convenient.
			Assert(m_NameHeap);

			// However, if the user wants to load the name heap again, then we should delete it now - after all,
			// it may have been the intention to reload it.
			if (readNameHeap) {
				delete[] m_NameHeap;
				m_NameHeap = NULL;
				m_NameHeapInDebugHeap = false;
			}
		}
#endif

		if (!m_NameHeap) {
			if (readNameHeap) {
				int nameHeapSize = hdr.m_NameHeapSize + m_EntryCount * sizeof(ParentIndexType);

#if !__PROFILE && !__FINAL && ENABLE_DEBUG_HEAP
				if (g_sysHasDebugHeap) {
					sysMemAllocator* debugAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL);
					m_NameHeapInDebugHeap = true;
					m_NameHeap = (char *) debugAllocator->Allocate(nameHeapSize, 16);
				}
				else
#endif // !__PROFILE && !__FINAL && ENABLE_DEBUG_HEAP
				{
					m_NameHeap = rage_new char[nameHeapSize];
				}

				m_Device->ReadBulk(m_Handle,m_ArchiveBias + sizeof(fiPackHeader) + entrySize,m_NameHeap,hdr.m_NameHeapSize);
				AES::Decrypt(m_KeyId, transformitSelector,  m_NameHeap,hdr.m_NameHeapSize);

				m_ParentIndices = (ParentIndexType*)(m_NameHeap + hdr.m_NameHeapSize);	// piggybacked on nameheap alloc so doesn't need to be deleted separately
				m_ParentIndices[0] = 0;
				FillParentIndices(0);

#if 0		// Useful debug code
				for (u32 i=0; i<m_EntryCount; i++) {
					fiPackEntry &pe = m_Entries[i];
					if (pe.IsDir())
						Displayf("%03d. %s (index %d, count %d) (parent %d)",i,m_NameHeap + (pe.m_NameOffset << m_NameShift),pe.u.directory.m_DirectoryIndex,pe.u.directory.m_DirectoryCount,m_ParentIndices[i]);
					else
						Displayf("%03d. %s (isRsc %d) (parent %d)",i,m_NameHeap + (pe.m_NameOffset << m_NameShift),(int)pe.m_IsResource,m_ParentIndices[i]);
				}
#endif
			}
			else {
				m_NameHeap = NULL;
				m_ParentIndices = NULL;
			}
		}
	}

	return true;
}

u32 fiPackfile::GetHeaderSize(bool /*withMetaData*/) const
{
	Assertf(m_EntryCount > 0, "Init() and/or ReInit() must have been called before GetHeaderSize() can be used");
	Assertf(m_CachedDataSize > 0, "GetHeaderSize() only works for RPF5 and RPF6");

	u32 entrySize = m_CachedDataSize;

/*	if (withMetaData) {
		entrySize += m_CachedMetaDataSize;
	}*/

	return entrySize;	
}


inline bool isLeap(int year) {
	// 2000 is a leap year; 2100 is not.
	// 2004 and 2008 are leap years.
	return ((year % 4) == 0 && (year % 100) != 0) || (year % 400 == 0);
}




bool fiPackfile::MountAs(const char *path) {
	if( fiDevice::Mount( path, *this, true ) )
	{
		m_RelativeOffset = StringLength(path); 
		return true;
	}
	
	return false;
}

void fiPackfile::SetDrmKey(const void * pDrmKey)
{
	m_pDrmKey = pDrmKey;
}


fiHandle fiPackfile::Create(const char * /*filename*/) const {
	// Never legal to create a file inside a zipfile
	return fiHandleInvalid;
}


fiHandle fiPackfile::Open(const char * filename,bool readOnly) const {
	return OpenInternal(filename + m_RelativeOffset,readOnly);
}


fiHandle fiPackfile::OpenDirect(const char * filename,bool readOnly) const {
	return OpenInternal(filename,readOnly);
}

static sysCriticalSectionToken s_ZipOpen;

#define STATIC_STATE_BUFFERS	(!__DEV && !__BANK && !__TOOL)

struct fiPackfileState {
	static const u32 BufferSize = 8192;
	bool IsCompressed() const;
#if STATIC_STATE_BUFFERS
	char						m_Buffer[BufferSize];
#else
	char						*m_Buffer;
#endif
	char						m_DebugName[32];
	const fiPackEntry *			m_Entry;			// Pointer to the file directory entry
	u32							m_Offset;			// Current offset within the (virtual, uncompressed) file
	u32							m_CompOffset;		// Current offset within the compressed file
#if USE_XCOMPRESS
	XMEMDECOMPRESSION_CONTEXT   m_xStream;
	void *						next_in;
	u32							avail_in;
#else
	z_stream					m_zStream;			// Stream object used by Zlib
#endif

	fiPackfileState() : m_Entry(NULL) {}

	void Reset() {
		m_Entry = 0;
		m_Offset = 0;
		m_CompOffset = 0;
#if USE_XCOMPRESS
		m_xStream = NULL;
		next_in = NULL;
		avail_in = 0;
#else
		memset(&m_zStream, 0, sizeof(z_stream)); // should we be nulling this?
#endif
	}
};

bool fiPackfileState::IsCompressed() const { 
	return m_Entry->IsCompressed();
}

atStaticPool<fiPackfileState,STATIC_STATE_BUFFERS? 4 : 16> s_States;

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4702)	// Unreachable code
#endif

fiHandle fiPackfile::OpenInternal(const char *lookupName,bool readOnly) const {
	// Never legal to modify a file inside a zipfile
	if (!readOnly)
		return fiHandleInvalid;

	const fiPackEntry *e = FindEntry(lookupName);
	if (e && e->IsFile()) {	// Cannot open a directory this way
		SYS_CS_SYNC(s_ZipOpen);

		if (s_States.IsFull()) {
			for (int i=0; i<s_States.GetSize(); i++)
				Displayf("%d. %s",i,s_States.GetElement(i).m_DebugName);
			Quitf(ERR_FIL_PACK_2,"Out of packfile descriptors opening %s, too many files in packs open at once (see tty for open files)",lookupName);
			return fiHandleInvalid;
		}

		fiPackfileState *s = s_States.New();

		s->Reset();
		if (e->IsCompressed())
		{
			RAGE_TRACK(PackfileState);
			sysMemStartTemp();
#if !STATIC_STATE_BUFFERS
			s->m_Buffer = rage_new char[fiPackfileState::BufferSize];
#endif
#if USE_XCOMPRESS
			XMEMCODEC_PARAMETERS_LZX codecParams = XCOMPRESS_SETTINGS;
			u32 contextSize = XMemGetDecompressionContextSize(XMEMCODEC_LZX, (VOID*)&codecParams, XMEMCOMPRESS_STREAM);
			s->m_xStream = (VOID*) rage_new char[contextSize];
			XMemInitializeDecompressionContext(XMEMCODEC_LZX, (VOID*)&codecParams, XMEMCOMPRESS_STREAM, s->m_xStream, contextSize);
#else
			if (inflateInit2(&s->m_zStream,-MAX_WBITS) != Z_OK)
			{
				// Clean up memory!
#if !STATIC_STATE_BUFFERS
				delete[] s->m_Buffer;
				s->m_Buffer = NULL;
#endif
				Errorf("fiPackfile::Open(%s) - inflateInit failed.",lookupName);
				sysMemEndTemp();
				return fiHandleInvalid;
			}
#endif
			sysMemEndTemp();
		}
		s->m_Entry = e;
		s->m_Offset = 0;
		safecpy(s->m_DebugName,ASSET.FileName(lookupName));

		return (fiHandle)s;
	}
	else {
		// Displayf("Open(%s) failed",filename + m_RelativeOffset);
		return fiHandleInvalid;
	}
}
#if __WIN32
#pragma warning(pop)
#endif


/*
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

void main()
{
	SYSTEMTIME _2000 = { 2000, 1, 0, 1, 0, 0, 0, 0 };
	unsigned __int64 ft;
	SystemTimeToFileTime(&_2000, (FILETIME*)&ft);
	char buffer[100];
	_ui64toa(ft,buffer,10);
	printf("filetime of 2000 %s\n",buffer);
}
*/
// Seconds since January 1, 2000 gives us a reach of 136 years.
static const u64 FILETIME_2000 = 125911584000000000ULL;
static const u64 CENTINANOSECONDS_PER_SECOND = 10000000ULL;

namespace Rpf {

u32 ConvertFileTimeToRpfTime(u64 filetime) {
	return u32((filetime - FILETIME_2000) / CENTINANOSECONDS_PER_SECOND);
}

u64 ConvertRpfTimeToFileTime(u32 filetime) {
	// system filetimes are in 100ns units
	return (u64) (filetime * CENTINANOSECONDS_PER_SECOND) + FILETIME_2000;
}

}	// Rpf


void fiPackfile::FillData(fiFindData &outData,u32 entryIndex) const {
	Assert(m_NameHeap);
	safecpy(outData.m_Name,m_NameHeap + (m_Entries[entryIndex].m_NameOffset << m_NameShift),sizeof(outData.m_Name));
	bool isDir = m_Entries[entryIndex].IsDir();
	outData.m_Size = isDir?0:m_Entries[entryIndex].GetUncompressedSize();
	outData.m_LastWriteTime		= m_FileTime;
	outData.m_Attributes		= FILE_ATTRIBUTE_READONLY | (isDir? FILE_ATTRIBUTE_DIRECTORY : 0);
}


fiHandle fiPackfile::FindFileBegin(const char *directoryName,fiFindData &outData) const {
	if (!m_NameHeap) {
		AssertMsg(false,"Archive doesn't have name heap, cannot search it");
		return fiHandleInvalid;
	}
	const fiPackEntry *e = FindEntry(directoryName + m_RelativeOffset);
	if (e && e->IsDir() && e->u.directory.m_DirectoryCount) {	// must be a non-empty directory
		SYS_CS_SYNC(s_ZipOpen);

		if (s_States.IsFull())
			return fiHandleInvalid;

		fiPackfileState *s = s_States.New();
		s->m_Entry = e;
		s->m_Offset = 0;
		FillData(outData,e->u.directory.m_DirectoryIndex);
		return (fiHandle)(size_t)(s);
	}
	else
		return fiHandleInvalid;
}


bool fiPackfile::FindFileNext(fiHandle _handle,fiFindData &outData) const {
	fiPackfileState &s = *(fiPackfileState*)_handle;
	if (++s.m_Offset < s.m_Entry->u.directory.m_DirectoryCount) {
		FillData(outData,s.m_Entry->u.directory.m_DirectoryIndex + s.m_Offset);
		return true;
	}
	else
		return false;
}


int fiPackfile::FindFileEnd(fiHandle _handle) const {
	SYS_CS_SYNC(s_ZipOpen);
	s_States.Delete((fiPackfileState*)_handle);
	return 0;
}

// can't do a simple subtraction any longer, the hash is a full 32 bits.
inline int hashcmp(u32 a,u32 b) {
	if (a > b)
		return 1;
	else if (b > a)
		return -1;
	else
		return 0;
}

const fiPackEntry* fiPackfile::FindEntry(const char *name) const
{
	Assertf(m_NameHeap, "fiPackfile::FindEntry - m_NameHeap is NULL! This could be a problem with the RPF data.");

	fiPackEntry *e = m_Entries;	// root directory
	if (!e || !m_NameHeap)
		return NULL;

	// remove any leading slash since the root dir is the starting point
	// but it may not be present in the name.
	if (*name == '/' || *name == '\\')
		++name;
	// return the root dir, really only for FindFirst and siblings
	if (!*name)
		return e;

	do {
		// Get min and max entries to search
		Assertf(e->IsDir(), "Found a file where a directory was assumed. Verify the path name: %s", name);
		u32 low = e->u.directory.m_DirectoryIndex;
		u32 high = low + e->u.directory.m_DirectoryCount - 1;
		e = NULL;
		char currentPart[RAGE_MAX_PATH], *cp = currentPart;
		while (*name && *name!='/' && *name!='\\') {
			*cp++ = (*name>='A'&&*name<='Z')?*name+('a'-'A'):*name;
			++name;
		}
		while (*name == '/' || *name == '\\')
			++name;
		*cp = 0;
		while (low <= high) {
			u32 mid = (low + high) >> 1;
			int result = strcmp(currentPart,m_NameHeap + (m_Entries[mid].m_NameOffset << m_NameShift));
			if (result < 0)
				high = mid-1;
			else if (result > 0)
				low = mid+1;
			else {
				e = &m_Entries[mid];
				break;
			}
		}
	} while (e && e->IsDir() && *name);
	return e;
}


int fiPackfile::Close(fiHandle _handle) const {
	fiPackfileState *s = (fiPackfileState*)_handle;
	SYS_CS_SYNC(s_ZipOpen);
	if (s->IsCompressed()) {
		sysMemStartTemp();
#if USE_XCOMPRESS
		XMemDestroyDecompressionContext(s->m_xStream);
		delete[] (char*) s->m_xStream;
#else
		inflateEnd(&s->m_zStream);
#endif
#if !STATIC_STATE_BUFFERS
		delete[] s->m_Buffer;
		s->m_Buffer = NULL;
#endif
		sysMemEndTemp();
	}
	s_States.Delete(s);
	return 0;
}


int fiPackfile::Read(fiHandle _handle,void *outBuffer,int bufferSize) const {
	fiPackfileState &s = *(fiPackfileState*)_handle;
	return ReadInternal(s,outBuffer,bufferSize);
}

int fiPackfile::ReadInternalBulk(const fiPackfileState& s,u32 relativeOffset,void *outBuffer,int toRead) const
{
	// Eliminate zero-byte requests before getting any further.
	if (toRead == 0)
		return 0;
	// If we got here, assume it's an optical read and therefore worth warning the cache thread about.
	pgLastActivity[pgStreamer::OPTICAL] = 0;
	int result = m_Device->ReadBulk(m_Handle, m_ArchiveBias + ((u64)s.m_Entry->GetFileOffset()) + relativeOffset,outBuffer,toRead);
	pgLastActivity[pgStreamer::OPTICAL] = sysTimer::GetTicks();
	return result;
}

int	fiPackfile::ReadInternal(fiPackfileState& s,void *outBuffer,int bufferSize) const
{
	if (s.IsCompressed()) {

#if USE_XCOMPRESS
		void *next_out = outBuffer;
		u32 avail_out = bufferSize;
		int total_written = 0;

		while (avail_out) {
			// queue up some source data if we need some
			if (s.avail_in == 0) {
				u32 toRead = fiPackfileState::BufferSize;
				if (toRead > u32(s.m_Entry->m_ConsumedSize - s.m_CompOffset))
					toRead = u32(s.m_Entry->m_ConsumedSize - s.m_CompOffset);
				int thisRead = ReadInternalBulk(s,s.m_CompOffset,s.m_Buffer,toRead);
				if (s.m_Entry->u.file.m_Encrypted)
					aes->Decrypt(s.m_Buffer,thisRead);
				s.m_CompOffset += (s.avail_in = thisRead);
				s.next_in = s.m_Buffer;
			}

			SIZE_T amount_written = avail_out, amount_consumed = s.avail_in;
			HRESULT hr = XMemDecompressStream(s.m_xStream, next_out, &amount_written,
				s.next_in, &amount_consumed);
			if (hr && hr != XMCDERR_MOREDATA && !amount_written)
				Quitf("Error in XMemDecompressStream");
			// If we have neither written any data nor consumed any data, we must be at EOF
			if (!amount_written && !amount_consumed)
				break;
			total_written += amount_written;
			next_out = (void*)((char*)next_out + amount_written);
			avail_out -= amount_written;
			s.next_in = (void*)((char*)s.next_in + amount_consumed);
			s.avail_in -= amount_consumed;
		}
		s.m_Offset += total_written;
		return total_written;
#else
		s.m_zStream.next_out = (Bytef*) outBuffer;
		s.m_zStream.avail_out = bufferSize;

		unsigned int transformitSelector = 0;
#if RSG_CPU_X64 
		if(s.m_Entry->u.file.m_Encrypted)
		{
			Assertf(m_NameHeap, "When loading encrypted files we need the name heap in memory");
			transformitSelector = atStringHash(m_NameHeap + (s.m_Entry->m_NameOffset << m_NameShift));
			transformitSelector+= s.m_Entry->GetUncompressedSize();
			transformitSelector = transformitSelector % TFIT_NUM_KEYS;
		}
#endif
		while (s.m_zStream.avail_out) {
			// queue up some source data if we need some
			if (s.m_zStream.avail_in == 0) {
				u32 toRead = fiPackfileState::BufferSize;
				if (toRead > u32(s.m_Entry->m_ConsumedSize - s.m_CompOffset))
					toRead = u32(s.m_Entry->m_ConsumedSize - s.m_CompOffset);
				int thisRead = ReadInternalBulk(s,s.m_CompOffset,s.m_Buffer,toRead);

				if (s.m_Entry->u.file.m_Encrypted)
					AES::Decrypt(m_KeyId, transformitSelector, s.m_Buffer,thisRead);

				s.m_CompOffset += (s.m_zStream.avail_in = thisRead);
				s.m_zStream.next_in = (Bytef*) s.m_Buffer;
			}

			// set destination for inflation.
			sysMemStartTemp();
			int err = inflate(&s.m_zStream,Z_SYNC_FLUSH);
			sysMemEndTemp();

			if (err == Z_STREAM_END) 
				break;
			else if (err < 0)
				Quitf(ERR_FIL_PACK_3,"Fatal disc error (code %d)",err);
		}

		// Figure out how much we actually read by subtracting out what was left unfilled.
		bufferSize -= s.m_zStream.avail_out;
		s.m_Offset += bufferSize;
		return bufferSize;
#endif
	}
	else {
		u32 amtToRead = bufferSize;
		if (amtToRead > s.m_Entry->GetUncompressedSize() - s.m_Offset)
			amtToRead = s.m_Entry->GetUncompressedSize() - s.m_Offset;
		amtToRead = ReadInternalBulk(s,s.m_Offset,outBuffer,amtToRead);
		s.m_Offset += amtToRead;
		return amtToRead;
	}
}

int fiPackfile::Size(fiHandle _handle) const {
	fiPackfileState &s = *(fiPackfileState*)_handle;
	return s.m_Entry->GetUncompressedSize();
}


u64 fiPackfile::Size64(fiHandle _handle) const {
	fiPackfileState &s = *(fiPackfileState*)_handle;
	return s.m_Entry->GetUncompressedSize();
}


int fiPackfile::Write(fiHandle /*handle*/,const void * /*buffer*/,int /*bufferSize*/) const {
	return -1;
}


int fiPackfile::Seek(fiHandle _handle,int offset,fiSeekWhence whence) const {
	return (int)Seek64(_handle,offset,whence);
}

u64 fiPackfile::Seek64(fiHandle _handle,s64 offset,fiSeekWhence whence) const {
	fiPackfileState &s = *(fiPackfileState*)_handle;

	if (s.IsCompressed()) {
		u32 destPos;
		switch (whence) {
		case seekSet: destPos = (u32) offset; break;
		case seekEnd: destPos = (u32)(s.m_Entry->GetUncompressedSize() + offset); break;
		case seekCur: destPos = (u32)(s.m_Offset + offset); break;
		default:
			return (u64)(s64)-1;
		}
		if (destPos > s.m_Entry->GetUncompressedSize())
			destPos  = s.m_Entry->GetUncompressedSize();

		if (destPos < s.m_Offset) {
			s.m_Offset = s.m_CompOffset = 0;
			sysMemStartTemp();
#if USE_XCOMPRESS
			XMemResetDecompressionContext(s.m_xStream);
			s.avail_in = 0;
#else
			inflateReset(&s.m_zStream);
			s.m_zStream.avail_in = 0;
#endif
			sysMemEndTemp();
		}

		while (destPos > s.m_Offset) {
			char buffer[4096];
			unsigned toRead = destPos - s.m_Offset;
			if (toRead > sizeof(buffer))
				toRead = sizeof(buffer);
			if (ReadInternal(s,buffer,toRead) == -1)
				return (u64)(s64)-1;
		}

		return destPos;
	}
	else {
		u32 newPos =
			(whence == seekEnd)? u32(s.m_Entry->GetUncompressedSize() + offset) :
			(whence == seekCur)? u32(s.m_Offset + offset) :
			u32(offset);
		if (newPos > s.m_Entry->GetUncompressedSize())
			newPos = s.m_Entry->GetUncompressedSize();
		s.m_Offset = (u32) newPos;		// TODO: Support >4G files within archives?
		return newPos;
	}
}


u64 fiPackfile::GetFileTime(const char * filename) const {
	const fiPackEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return m_FileTime;
	else
		return 0;
}

bool fiPackfile::SetFileTime(const char * /*filename*/,u64 /*timestamp*/) const {
	return false;
}


u32 fiPackfile::GetAttributes(const char * filename) const {
	const fiPackEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return e->IsDir()? (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_READONLY) : FILE_ATTRIBUTE_READONLY;
	else
		return FILE_ATTRIBUTE_INVALID;
}


u64 fiPackfile::GetFileSize(const char * filename) const {
	const fiPackEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return e->GetUncompressedSize();
	else
		return 0;
}


int fiPackfile::GetResourceInfo(const char *name,datResourceInfo &outHeader) const {
	const fiPackEntry *e = FindEntry(name + m_RelativeOffset);
	if (e) {
		if (e->IsResource()) {
			outHeader = e->u.resource.m_Info;
			return e->GetVersion();
		}
	}
	// not a resource, or not found
	return 0;
}


fiHandle fiPackfile::OpenBulk(const char *filename,u64 &outBias) const {
	const fiPackEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e) {
		outBias = (u64) e->GetFileOffset();
		return m_Handle;
	}
	return fiHandleInvalid;
}


const char *fiPackfile::GetEntryName(u32 handle) const {
	if (m_NameHeap)
		return m_NameHeap + (m_Entries[handle & EntryMask].m_NameOffset << m_NameShift);
	else {
		static char tmp[RAGE_MAX_PATH];
		formatf_n(tmp,"%s:hash_%08x",GetDebugName(),m_Entries[handle & EntryMask].m_NameOffset << m_NameShift);
		return tmp;
	}
}

const char *fiPackfile::GetEntryFullName(u32 handle,char *dest,int destSize) const {
	if (m_NameHeap) {
		const u32 dirStackMax = 16;
		ParentIndexType dirStack[dirStackMax];
		u32 dirCount = 0;
		handle &= EntryMask;
		u32 i = handle;
		while (m_ParentIndices[i] && dirCount < dirStackMax) {
			dirStack[dirCount++] = m_ParentIndices[i];
			i = m_ParentIndices[i];
		}
		dest[0] = 0;
		while (dirCount--) {
			safecat(dest,m_NameHeap + (m_Entries[dirStack[dirCount]].m_NameOffset << m_NameShift),destSize);
			safecat(dest,"/",destSize);
		}
		safecat(dest,m_NameHeap + (m_Entries[handle].m_NameOffset << m_NameShift),destSize);
		return dest;
	}
	else
		return GetEntryName(handle);
}

u64 fiPackfile::GetBulkOffset(const char *filename) const {
	const fiPackEntry *e = FindEntry(filename + m_RelativeOffset);
	return (e && !e->IsCompressed())? (u64)e->GetFileOffset() : 0;
}


int fiPackfile::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const {
	AssertMsg(handle == m_Handle,"You are probably trying to load a zip with STREAM_FROM_ARCHIVES_ONLY enabled.");
	offset += m_ArchiveBias;

	u64 totalSize = m_ArchiveSize;
	if (m_Device->IsRpf() WIN32PC_ONLY(|| m_Device->IsMaskingAnRpf() ))
	{
		fiPackfile* rpfDevice = (fiPackfile*)m_Device->GetRpfDevice();
		totalSize += rpfDevice->GetPackfileSize();
	}

	if (offset + bufferSize > totalSize)
		bufferSize = (int)(totalSize - offset);
	return m_Device->ReadBulk(handle,offset,outBuffer,bufferSize);
}

int fiPackfile::CloseBulk(fiHandle /*handle*/) const {
	// Nothing to do here, no real handle allocated.
	return 0;
}

u32 fiPackfile::GetPhysicalSortKey(const char* filename) const
{
	const fiPackEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e) {
		u64 offset = (u64)e->GetFileOffset();
		// sort key is a sector address (2048 bytes)
		return m_SortKey + (u32)(offset >> 11);
	}
	return 0;
}

fiHandle fiPackfile::OpenBulkFromHandle(u32 handle,u64 &offset) const { 
#if __ASSERT
	if (!m_Entries) {
		pgStreamer::CallErrorDumpCallback();
	}
#endif // __ASSERT
	AssertMsg(m_Entries,"Attempting to open a file in an archive that was streamed out");
	offset = (u64)m_Entries[handle & EntryMask].GetFileOffset(); 
	return m_Handle; 
}

const fiPackEntry& fiPackfile::GetEntryInfo(u32 handle) const {
	return m_Entries[handle & EntryMask];
}

u32 fiPackfile::GetEntryPhysicalSortKey(u32 handle,bool uncached) const { 
#if __ASSERT
	if (!m_Entries) {
		pgStreamer::CallErrorDumpCallback();
	}
#endif // __ASSERT
	AssertMsg(m_Entries,"Attempting to get sort key of file in an archive that was streamed out. Please look at the TTY for details.");
	/*if (m_IsInstalled)
		return HARDDRIVE_LSN;
	else*/ if (m_IsCached && !uncached)
		return reinterpret_cast<const fiCachedDevice*>(m_Device)->GetBlockPhysicalSortKey((u64)m_Entries[handle & EntryMask].GetFileOffset(),m_Entries[handle & EntryMask].GetConsumedSize());
	else
		return m_SortKey + (u32)((u64)m_Entries[handle & EntryMask].GetFileOffset() >> (11)); 
}

bool fiPackfile::Prefetch(u32 handle) const {
	if (m_IsCached)
		return reinterpret_cast<const fiCachedDevice*>(m_Device)->Prefetch((u64)m_Entries[handle & EntryMask].GetFileOffset(),m_Entries[handle & EntryMask].GetConsumedSize());
	else
		return false;
}

int fiPackfile::GetEntryIndex(const char *name) const { 
	const fiPackEntry *e = FindEntry(name + m_RelativeOffset); 
	return e? int(e - m_Entries) : -1; 
}

bool fiPackfile::IsRpf() const { 
	return m_IsRpf; 
}

const char *fiPackfile::GetDebugName() const {
	return m_Name;
}

u32 fiPackfile::GetBasePhysicalSortKey() const {
	return m_SortKey;
}

}	// namespace rage
