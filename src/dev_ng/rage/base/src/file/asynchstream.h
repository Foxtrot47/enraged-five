// 
// file/asynchstream.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_ASYNCHSTREAM_H
#define FILE_ASYNCHSTREAM_H

#include "paging/streamer.h"

namespace rage {

class fiStream;

/*
	This class implements a wrapper that allows code to asynchronously
	read a file into memory (via a polling interface) and then, once
	the operation is complete, present a normal fiStream* object that
	is a memory stream that will automatically free itself when the
	file is closed.
*/
class fiAsynchStream {
public:
	fiAsynchStream();
	~fiAsynchStream();

	/*	PURPOSE:	Uses ASSET.Open semantics to attempt to asynchronously
					schedule the file open.  Will not block if (and only if) 
					you're using archives.
		PARAMS:		filename - as per ASSET.Open
					ext - as per ASSET.Open
		RETURNS:	True if the file could be found (and scheduled), else false */
	bool Init(const char *filename,const char *ext);

	bool Init(const char *filename,const char *ext, int offset, int size);

	/*	PURPOSE:	If the file has finished streaming in, returns a suitable
					memory filename that can be passed to any function that expects
					a filename.
		PARMS:		dest - Destination buffer for name
					destSize - sizeof(dest);
		RETURNS:	True if the file loading is complete (and fills in the name),
					else false.
		NOTES:		The memory for the file will be automatically freed
					after the file is closed.  Note that if the file doesn't open
					properly for some reason, you will get a memory leak.  It may be
					better to use Poll instead, which calls this function internally
					and automatically frees the storage memory if the file couldn't
					be opened for some reason. */
	bool GetMemoryFileName(char *dest,int destSize);

	/*	PURPOSE:	Determine if the file operation has completed yet.
		RETURNS:	Valid fiStream if the operation completed, else NULL.
		NOTES:		You must close the resulting stream or you'll get a memory leak. */
	fiStream *Poll();

	//	RETURNS:	Size of file (only valid if Init returned true)
	u32 GetSize() const { return m_Size; }

private:
	static void Callback(void *userArg,void *dest,u32 offset,u32 amtRead);
	pgStreamer::Handle m_Handle;
	char *m_Storage;
	u32 m_Size;
	volatile bool m_Completed;
};

}	// namespace rage

#endif	// FILE_ASYNCHSTREAM_H
