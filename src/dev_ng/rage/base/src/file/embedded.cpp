// 
// file/embedded.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "embedded.h"

#if 0

#include <string.h>

namespace rage {


fiEmbeddedFile*	fiEmbeddedFile::sm_First;


fiEmbeddedFile::fiEmbeddedFile(const char *name,const unsigned char *data,int size) :
	m_Path(name),
	m_Data(data),
	m_DataSize(size),
	m_Next(sm_First) 
{
	sm_First = this;
}


bool fiEmbeddedFile::Lookup(const char *name,const unsigned char *&data,int &dataSize)
{
	fiEmbeddedFile *i = sm_First;
	while (i) {
		if (!strcmp(name,i->m_Path)) {
			data = i->m_Data;
			dataSize = i->m_DataSize;
			return true;
		}
		i = i->m_Next;
	}
	return false;
}


}	// namespace rage

#endif