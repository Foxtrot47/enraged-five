// 
// file/stream.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_STREAM_H
#define FILE_STREAM_H

#include "string/unicode.h"
#include "file/handle.h"

#include <stdio.h>

#if __WIN32PC
#include <tchar.h>
#endif

#if RSG_SPU
typedef int64_t off_t;
#endif

namespace rage {

extern int StringLength(const char*); //lint !e18

class fiDevice;
class fiStream;

#define STREAM_STORE_NAME	(__DEV || __TOOL || __RESOURCECOMPILER)

typedef bool (*StreamOpenCallback)(const char *filename,bool readonly, fiHandle fd);
typedef bool (*StreamCreateCallback)(const char *filename, fiHandle fd);
typedef void (*StreamReadCallback)(fiHandle fd, int count);
typedef void (*StreamCloseCallback)(const char *filename, fiStream &stream);
/*
PURPOSE
	This implements functions to do the file I/O work.  Implements buffered file I/O; should be used instead of 
	FILE in stdio.h if portability is desired.

	Where practical, stdio-style interfaces are supported; most use inlines to translate into the particular member function call.
NOTES
	It is not necessary to flush between reading and writing the same file.
<FLAG Component>
*/
class fiStream {
#if !__TOOL
	// prevent creation on the heap
	static void* operator new(size_t);
#endif

public:
	// PURPOSE: Fully loads a stream into memory. Will create a new stream and close the given one.
	// PARAMS
	// 	s - The stream to be read from and then closed.
	// RETURNS: The pointer to the stream.
	// NOTES
	// 	Read data from the stream S into a new buffer which will be the buffer of
	// 	the new stream.
	static fiStream* PreLoad(fiStream* s);

	// PURPOSE
	//	Attempts to open a file of the given name, returning NULL on failure. All filenames are treated verbatim at this level; 
	//	there is no notion of an asset path or anything like that. See data/asset.h for a higher-level way to open files. 
	//	Uses the current "default open file method" to access the file.
	// PARAMS
	//	file - name of the file to open.
	//	readOnly - read only if true.
	// RETURNS:	the pointer to the stream.
	static fiStream* Open(const char *file,bool readOnly = true);

	// PURPOSE
	//  Associates an already-existing device handle with a stream (which assumes ownership of the file
	//	and will close the handle when the stream is closed, unless you call CloseShared)
	// PARAMS
	//  file - Filename (for debugging only, ie leaked streams)
	//  handle - handle of the file, suitable for use by fiDeviceLocal
	// RETURNS: the pointer to the stream, or NULL on failure.
	static fiStream* fdopen(const char *file,fiHandle handle);

	// PURPOSE
	//	Attempts to create a file of the given name for read-write access, returning NULL on failure. All 
	//	filenames are treated verbatim at this level. Uses the current "default create file method" to 
	//	access the file.
	// PARAMS
	//	file - name of the file to open.
	// RETURNS: the pointer to the stream.
	static fiStream* Create(const char *file);

	// PURPOSE
	//  Attempts to open a file of the given name, returning <c>NULL</c> on failure. All filenames are treated verbatim at this level; 
	//  there is no notion of an asset path or anything like that. See data/asset.h for a higher-level way to open files. 
	//  Uses the supplied <c>coreFileMethods*</c> pointer to determine how to actually open the file.
	// PARAMS
	//  file - name of the file to open.
	//  device - methods to open a file.
	//  readOnly - read only if true.
	// RETURNS: the pointer to the stream.
	static fiStream* Open(const char *file, const fiDevice &device, bool readOnly = true);

	// PURPOSE
	//  Attempts to create a file of the given name for read-write access, returning NULL on failure. All 
	//  filenames are treated verbatim at this level. Uses the supplied <c>coreFileMethods*</c> pointer to 
	//  determine how to actually create the file.
	// PARAMS
	//  file - file to create.
	//  device - used to create a file.
	// RETURNS: The pointer to the stream.
	static fiStream* Create(const char *file,const fiDevice &device);

	// PURPOSE
	//  Turns on a bool that causes all file operations to print debug information
	static void OutputAllFileAccess();

	// PURPOSE
	//  Read the as many as "count" into the "dest".
	// PARAMS
	//  dest - where to store the read.
	//  count - how many to read.
	// RETURNS: Returns count of bytes read.
	// NOTES
	//  Large writes (more than 4K bypass buffering and are sent directly 
	//  to the core file routine for handling. 
	int Read(void* dest,int count);

	// PURPOSE
	//  Write as many as "count" from src into buffer.
	// PARAMS
	//  src - to be written.
	//  count - total to be written.
	// RETURNS: Returns count of bytes written.
	int Write(const void* src, int count);

	//	PURPOSE
	//	 Seeks directly to specified position in the file. No "whence" parameter is supported; use 
	//	 <c>fiStream::Tell</c> as necessary to implement relative file pointer movement.
	//	PARAMS
	//	 newPos - the specified position
	//  RETURNS: returns -1 if position is outside file boundary.
	int Seek(int newPos);

	//	PURPOSE
	//	 Seeks directly to specified position in the file. No "whence" parameter is supported; use 
	//	 <c>fiStream::Tell64</c> as necessary to implement relative file pointer movement.
	//	PARAMS
	//	 newPos - the specified position
	//  RETURNS: returns -1 if position is outside file boundary.
	u64 Seek64(u64 newPos);

	// PURPOSE:	Returns current file position (start + offset.)
	int Tell();

	int TellStart();

	// PURPOSE:	Returns current file position (start + offset.)
	u64 Tell64();

	// PURPOSE:	Returns the total size of the file. 
	int Size();

	// PURPOSE:	Returns the total size of the file. 
	u64 Size64();

	// PURPOSE: Flush any buffered data.
	// RETURNS: Number of bytes flushed.
	int Flush();

	// PURPOSE: Closes the stream. 
	// RETURNS: 0 if successful, non-zero on failure
	int Close();
    
    // PURPOSE: Sanitizes the internal buffer of the stream
    // RETURNS: N/A
    void Sanitize();


	// PURPOSE: Closes the stream without closing the underlying handle (assuming it's shared)
	//	This flushes the buffers and frees our fiStream object without invalidating the handle.
	// RETURNS: 0 if successful, non-zero on failure
	int CloseShared();

	// PURPOSE:	Read one or more data items.
	// PARAMS
	//  dest - where to store the read data.
	//  num - how many to read.
	// RETURNS: total items read.
	int ReadByte(unsigned char* dest,int num);
	int ReadByte(char *dest,int num);				// <ALIAS fiStream::ReadByte@unsigned char*@int>
	int ReadShort(unsigned short *dest,int num);	// <ALIAS fiStream::ReadByte@unsigned char*@int>
	int ReadShort(short *dest,int num);				// <ALIAS fiStream::ReadByte@unsigned char*@int>
#if __WIN32PC
	int ReadTChar(_TCHAR *dest,int num);			// <ALIAS fiStream::ReadByte@unsigned char*@int>
#endif
	int ReadInt(unsigned *dest,int num);			// <ALIAS fiStream::ReadByte@unsigned char*@int>
	int ReadInt(int *dest,int num);					// <ALIAS fiStream::ReadByte@unsigned char*@int>
	int ReadFloat(float *dest,int num);				// <ALIAS fiStream::ReadByte@unsigned char*@int>
	int ReadLong(u64 *dest,int num);
	int ReadLong(s64 *dest,int num);
	int ReadDouble(double* dest, int num);

	// PURPOSE: Write one or more data items.
	// PARAMS
	//  src - the data to be written.
	//  num - number of items to be written.
	// RETURNS: total items written.
	int WriteByte(const unsigned char *src,int num);
	int WriteByte(const char *src,int num);				// <ALIAS fiStream::WriteByte@const unsigned char *@int>
	int WriteShort(const unsigned short *src,int num);	// <ALIAS fiStream::WriteByte@const unsigned char *@int>
	int WriteShort(const short *src,int num);			// <ALIAS fiStream::WriteByte@const unsigned char *@int>
#if __WIN32PC
	int WriteTChar(const _TCHAR *src,int num);			// <ALIAS fiStream::WriteByte@const unsigned char *@int>
#endif
	int WriteInt(const unsigned *src,int num);			// <ALIAS fiStream::WriteByte@const unsigned char *@int>
	int WriteInt(const int *src,int num);				// <ALIAS fiStream::WriteByte@const unsigned char *@int>
	int WriteFloat(const float *src,int num);			// <ALIAS fiStream::WriteByte@const unsigned char *@int>
	int WriteLong(const u64 *src,int num);
	int WriteLong(const s64 *src,int num);
	int WriteDouble(const double* src, int num);

	// PURPOSE: Pulls next available character from the file stream. Returns -1 on <c>EOF</c>.
	inline int FastGetCh();

	// PURPOSE: Pushes supplied character into the file stream.
	// RETURNS: -1 on failure (disk full), ch otherwise.
	// PARAMS:
	//	ch - The character to put into the stream
	inline int FastPutCh(u8 ch);

	// PURPOSE: Gets a single char from the stream, returns it.
	// RETURNS: -1 on failure (EOF), a character from the stream otherwise.
	int GetCh();

	// PURPOSE: Puts a single char to the string. 
	// RETURNS: The character, or -1 on EOF
	// PARAMS:
	//	ch - The character to put into the stream
	int PutCh(unsigned char ch);

	// PURPOSE: Print information about open files
	// PARAMS:
	//	linePrefix - Text to prefix the Displayf() output with.
	static void DumpOpenFiles(const char *linePrefix = "");

	// PURPOSE: Close all open files
	static void CloseOpenFiles();

	// PURPOSE: Get the name of this stream (__DEV && __TOOL builds only)
	const char *GetName() const;

	// PURPOSE: Get the underlying device
	const fiDevice *GetDevice() const			{ return m_Device; }

	// PURPOSE: Sets a callback that gets called every time a stream is opened.
	// PARAMS:
	//	callback - The callback function to call
	static void SetOpenCallback(StreamOpenCallback callback);

	// PURPOSE: Returns the current stream-open callback
	static StreamOpenCallback GetOpenCallback();

	// PURPOSE: Sets a callback that gets called every time a stream is read from.
	// PARAMS:
	//	callback - The callback function to call
	static void SetReadCallback(StreamReadCallback callback);

	// PURPOSE: Returns the current stream-open callback
	static StreamReadCallback GetReadCallback();

	// PURPOSE: Sets a callback that gets called every time a stream is created.
	// PARAMS:
	//	callback - The callback function to call
	static void SetCreateCallback(StreamCreateCallback callback);

	// PURPOSE: Returns the current stream-create callback
	static StreamCreateCallback GetCreateCallback();

	// PURPOSE: Sets a callback that gets called every time a stream is closed.
	// PARAMS:
	//	callback - The callback function to call
	static void SetCloseCallback(StreamCloseCallback callback);

	// PURPOSE: Returns the current stream-close callback
	static StreamCloseCallback GetCloseCallback();

	fiHandle GetLocalHandle() const { return m_Handle; }


	// PURPOSE: also for testing error handling by making certain folders fail loading
	static void SetFaultInjectionPath( const char* path );

#if __ASSERT
	// PURPOSE: Turn on asserts if blocking loads occur.
	static void AllowBlockingLoads( bool allowBlockingLoads )		{ sm_AllowBlockingLoads = allowBlockingLoads; }

	// RETURNS: TRUE if it's OK to do blocking loads.
	static bool	AreBlockingLoadsAllowed()							{ return sm_AllowBlockingLoads; }
#else // __ASSERT
	static void AllowBlockingLoads( bool /*allowBlockingLoads*/ )	{ };
	static bool	AreBlockingLoadsAllowed()							{ return true; }
#endif // !__ASSERT



private:
	const fiDevice *m_Device;

	static fiStream* AllocStream(const char*,fiHandle fileDes,const fiDevice&);
	static int MaxStreams;
	enum { NameLength=160 };
	enum { BufferSize = 4096 };
	static fiStream sm_Streams[];
	static char sm_Buffers[][BufferSize];

	static bool HasAvailableStream();

	static StreamOpenCallback sm_OpenCallback;
	static StreamCreateCallback sm_CreateCallback;
	static StreamCloseCallback sm_CloseCallback;
	static StreamReadCallback sm_ReadCallback;
	static const char * const sm_UnavailableString;
	static const char * sm_FaultInjectionPath;
	fiHandle m_Handle;
#if STREAM_STORE_NAME
	char m_Name[NameLength];
#endif
	char *m_Buffer;

	u64 m_Start;	// location of last seek
	int m_Offset,	// offset into buffer of current position
		m_Length,	// amount of info in buffer during read
		m_Size;		// total size of the buffer

	// When we've buffered a read, length != 0.
	// When we've buffered a write, length == 0 and offset != 0.
	// When we've buffered nothing (ie right after a Flush),
	//	length = offset = 0

#if __WIN32PC && __DEV
	static bool m_bOutputAllFileAccess;
#endif

#if __ASSERT
	static bool sm_AllowBlockingLoads;
#endif // __ASSERT
};

int fiStream::FastGetCh() { return (m_Offset < m_Length)? ((unsigned char*)m_Buffer)[m_Offset++] : GetCh(); }

int fiStream::FastPutCh(unsigned char ch) { 
		return (m_Length==0 && (m_Offset < m_Size))
			? m_Buffer[m_Offset++]=ch 
			: PutCh(ch); 
}

/*
PURPOSE
Pass the result of a fiStream::Open or ASSET.Open to this class and it
will assume ownership of the stream for you; its destructor will
guarantee that the stream is properly closed.  Use it anywhere you
would use a fiStream*, it has the appropriate implicit conversion operators.
<FLAG Component>
*/
class fiSafeStream {
#if !__TOOL
	// prevent creation on the heap
	static void* operator new(size_t);
#endif
public:
	// PURPOSE: Creates a new safe stream that will close s when the safe stream object is destroyed.
	fiSafeStream(fiStream*s) : m_Stream(s) { }

	// PURPOSE: The safe stream destructor will close the stream its holding on to
	~fiSafeStream() { if (m_Stream) m_Stream->Close(); }

	// PURPOSE: Returns the stream object, so that fiSafeStream can be used just like an fiStream
	fiStream* operator->() { return m_Stream; }

	// PURPOSE: Implicit conversion to fiStream, so that fiSafeStream can be used just like an fiStream
	operator fiStream*() { return m_Stream; }
private:
	fiStream *m_Stream;
};

#undef getc
#undef putc

/* For compatibility with old stdio code */
/* these are inlined to allow better optimization since 'es' is often
	a power of two */
inline int fread(void*t,unsigned es,unsigned ct,fiStream *file) 
	{ return file->Read(t,es * ct) / es; }
inline int fwrite(const void*t,unsigned es,unsigned ct,fiStream *file)
	{ return file->Write(t,es * ct) / es; }
inline int fgetc(fiStream *file) { return file->GetCh(); }
inline int getc(fiStream *file) { return file->GetCh(); }
inline int fputs(const char *s,fiStream *file) { return file->Write(s,StringLength(s)); }
inline int fputc(int ch,fiStream *file) { return file->PutCh((unsigned char)ch); }
inline int putc(int ch,fiStream *file) { return file->PutCh((unsigned char)ch); }

extern void fprintf(fiStream*,const char*,...);
// extern fiStream* fopen(char *,char *);
extern int fseek(fiStream *,int,int);
extern int fseeko(fiStream *, off_t offset, int whence);
// inline void fclose(fiStream *s) { delete s; }
inline int ftell(fiStream *s) { return s->Tell(); }
inline int fflush(fiStream *s) { return s->Flush(); }
inline int rewind(fiStream *s) { return s->Seek(0); }
extern int fgets(char *,int,fiStream*);

// PURPOSE:	Like fgets, but strips off whitespace at end of line
// RETURNS:	Count of characters stored, or 0 on EOF.  Will silently consume empty lines.
extern int fgetline(char*,int,fiStream*);

#define DECLARE_STREAM_BUFFERS(count) \
	int fiStream::MaxStreams = (count); \
	fiStream fiStream::sm_Streams[count]; \
	char fiStream::sm_Buffers[count][BufferSize] ALIGNED(128);

//////////////////////////////////////////////////////////////////////
// fiStream inlines

inline int fiStream::ReadByte(unsigned char *dest,int num) {
	return Read(dest,num); 
}

inline int fiStream::WriteByte(const unsigned char *src,int num) { 
	return Write(src,num); 
}

#if !__BE
inline int fiStream::ReadShort(unsigned short *dest,int num) { 
	return Read(dest,num<<1)>>1; 
}

#if __WIN32PC
inline int fiStream::ReadTChar(_TCHAR *dest,int num) { 
	if (sizeof(_TCHAR) == sizeof(short))
	{
		return Read(dest, num << 1) >> 1;
	}
	else
	{
		return Read(dest, num);
	}
}
#endif

inline int fiStream::ReadInt(unsigned int *dest,int num) { 
	return Read(dest,num<<2)>>2; 
}

inline int fiStream::ReadLong(u64 *dest,int num) { 
	return Read(dest,num<<3)>>3; 
}

inline int fiStream::ReadLong(s64 *dest,int num) { 
	return Read(dest,num<<3)>>3; 
}

inline int fiStream::WriteShort(const unsigned short *src,int num) { 
	return Write(src,num<<1)>>1; 
}

#if __WIN32PC
inline int fiStream::WriteTChar(const _TCHAR *src,int num) { 
	if (sizeof(_TCHAR) == sizeof(short))
	{
		return Write(src, num<<1) >> 1;
	}
	else
	{
		return Write(src, num);
	}
}
#endif

inline int fiStream::WriteInt(const unsigned int *src,int num) { 
	return Write(src,num<<2)>>2; 
}

inline int fiStream::WriteLong(const u64 *src,int num) { 
	return Write(src,num<<3)>>3; 
}

inline int fiStream::WriteLong(const s64 *src,int num) { 
	return Write(src,num<<3)>>3; 
}
#endif // !__BE

inline int fiStream::ReadByte(char *dest,int num) { 
	return ReadByte((unsigned char*)dest,num); 
}

inline int fiStream::ReadShort(short *dest,int num) { 
	return ReadShort((unsigned short*)dest,num); 
}

inline int fiStream::ReadInt(int *dest,int num) { 
	return ReadInt((unsigned*)dest,num); 
}

inline int fiStream::ReadFloat(float *dest,int num) { 
	return ReadInt((unsigned*)dest,num); 
}

inline int fiStream::ReadDouble(double* dest, int num) {
	return ReadLong((u64*)dest, num);
}

inline int fiStream::WriteByte(const char *src,int num) { 
	return WriteByte((unsigned char*)src,num); 
}

inline int fiStream::WriteShort(const short *src,int num) { 
	return WriteShort((const unsigned short*)src,num); 
}

inline int fiStream::WriteInt(const int *src,int num) { 
	return WriteInt((const unsigned int*)src,num); 
}

inline int fiStream::WriteFloat(const float *src,int num) { 
	return WriteInt((unsigned*)src,num); 
}

inline int fiStream::WriteDouble(const double* src, int num) {
	return WriteLong((u64*)src, num);
}

inline const char *fiStream::GetName() const {
#if STREAM_STORE_NAME
	return m_Name;
#else
	return sm_UnavailableString;
#endif
}

inline StreamOpenCallback fiStream::GetOpenCallback() { 
	return sm_OpenCallback; 
}

inline StreamReadCallback fiStream::GetReadCallback() { 
	return sm_ReadCallback; 
}

inline StreamCreateCallback fiStream::GetCreateCallback() { 
	return sm_CreateCallback; 
}

inline StreamCloseCallback fiStream::GetCloseCallback() { 
	return sm_CloseCallback; 
}
} // namespace rage

#endif	// CORE_STREAM_H
