Project file
Files {
	file_channel.h
	file_config.h
	asset.cpp
	asset.h
	stream.cpp
	stream.h
	winsock.cpp
	winsock.h
	serialize.cpp
	serialize.h
	token.cpp
	token.h
	cachepartition.cpp
	cachepartition.h
	default_paths.h
	diskcache.cpp
	diskcache.h
	Folder Compression {
		ficompress.cpp
		compress.h
		decompress.cpp
		decompress.h
		ficompressiondata.cpp
		ficompressiondata.h
		compress_internal.h
	}
	embedded.cpp
	embedded.h
	ftpserver.cpp
	ftpserver.h
	asynchstream.cpp
	asynchstream.h
	winrtbuffer.winrt.cpp
	winrtbuffer.winrt.h
	Folder Devices {
		device_crc.cpp
		device_crc.h
		device_relative.cpp
		device_relative.h
		device_win32.cpp
		device_psn.cpp
		device_psp2.cpp
		device_common.cpp
		device.h
		device_xcontent.h
		device_xcontent.cpp
		device_installer.h
		device_installer.cpp
		packfile.cpp
		packfile_tool.cpp
		packfile_builder.cpp
		packfile_builder.h
		packfile.h
		zipfile.cpp
		zipfile.h
		remote.cpp
		remote.h
		tcpip_win32.cpp
		tcpip_psn.cpp
		tcpip_psp2.cpp
		tcpip_orbis.cpp
		tcpip_common.cpp
		tcpip.h
		Parse {
			device_installer
		}
	}
	streambuffers.cpp
	Folder Savegame {
		savegame_xenon.cpp
		savegame_dummy.cpp
		savegame_durango.winrt.cpp
		savegame_psn.cpp
		savegame_pc.cpp
		savegame_orbis.cpp
		savegame_steam.cpp
		savegame.h
		dummy_titleid.cpp
	}
	handle.h
	archive.h
	limits.h
}
Custom {
	file.dtx
}
