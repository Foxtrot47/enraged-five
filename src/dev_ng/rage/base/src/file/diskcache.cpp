//
// file/diskcache.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "diskcache.h"
#include "file/device.h"
#include "file/packfile.h"

#include "atl/array.h"
#include "atl/staticpool.h"
#include "string/stringhash.h"
#include "system/ipc.h"
#include "system/new.h"
#include "system/memory.h"
#include "system/messagequeue.h"
#include "system/param.h"
#include "system/timer.h"
#include "file/asset.h"
#include "file/cachepartition.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "diag/trap.h"
#include "paging/streamer.h"
#include "paging/streamer_internal.h"

#if __XENON
#include "system/xtl.h"
#elif __PPU
#include <cell/sysmodule.h>
#include <sysutil/sysutil_syscache.h>
#if ENABLE_GAMEDATA_FOR_DISKCACHE
#include <sysutil/sysutil_gamecontent.h>
#include <sysutil/sysutil_msgdialog.h>
#endif
#include <cell/cell_fs.h>
#include "file/savegame.h"		// for XEX_TITLE_ID
#endif

#if __ASSERT
#define FieldAssign(a,b)	Assert((a = (b)) == (b))
#else
#define FieldAssign(a,b)	a = (b)
#endif

#include "system/criticalsection.h"

namespace rage {

/*
	fiDiskCache implements a read cache between any source device (typically a packfile) and
	higher level code.  The current implementation maintains strict least-recently-used via
	a doubly-linked list that persists across sessions.  Everything is carefully sanity checked
	to avoid problems; in particular, for every file added to the cache, we compute a hash code
	based off of its name, size, and timestamp and record that in the persistent data.
*/

RAGE_DEFINE_CHANNEL(DiskCache)

#if __FINAL && __PS3
#define dcErrorf(fmt,...)					printf("[DiskCache] Error: " fmt "\n",##__VA_ARGS__)
#define dcWarningf(fmt,...)					printf("[DiskCache] Warning: " fmt "\n",##__VA_ARGS__)
#define dcDisplayf(fmt,...)					printf("[DiskCache] " fmt "\n",##__VA_ARGS__)
#define dcDebugf1(fmt,...)	//				printf("[DiskCache] Debug1: " fmt "\n",##__VA_ARGS__)
#define dcDebugf2(fmt,...)	//				printf("[DiskCache] Debug2: " fmt "\n",##__VA_ARGS__)
#define dcDebugf3(fmt,...)	//				printf("[DiskCache] Debug3: " fmt "\n",##__VA_ARGS__)
#else
#define dcErrorf(fmt,...)					RAGE_ERRORF(DiskCache,fmt,##__VA_ARGS__)
#define dcWarningf(fmt,...)					RAGE_WARNINGF(DiskCache,fmt,##__VA_ARGS__)
#define dcDisplayf(fmt,...)					RAGE_DISPLAYF(DiskCache,fmt,##__VA_ARGS__)
#define dcDebugf1(fmt,...)					RAGE_DEBUGF1(DiskCache,fmt,##__VA_ARGS__)
#define dcDebugf2(fmt,...)					RAGE_DEBUGF2(DiskCache,fmt,##__VA_ARGS__)
#define dcDebugf3(fmt,...)					RAGE_DEBUGF3(DiskCache,fmt,##__VA_ARGS__)
#endif

#if __WIN32
#pragma warning(disable: 4201)
#endif

// Entry zero is actually the file header.
struct Slot {
	union {
		struct {
			u32 srcBlock: 24, srcFile: 4, state: 4;
		};
		u32 magic;
	};
	u16 older, newer;
};

struct File {
	u32 namehash;
	u32 sizetimehash;
};

// Note that FlushCacheHeader creates a structure roughly 8*CACHE_BLOCK_MAX on the stack so we need to watch the size.
const u32 CACHE_BLOCK_SHIFT = __XENON? 23 : 21;
const u32 CACHE_BLOCK_MAX = __XENON? 256 : 1024;

const u32 CACHE_BLOCK_SIZE = (1<<CACHE_BLOCK_SHIFT);
const u32 CACHE_BLOCK_MASK = (CACHE_BLOCK_SIZE-1);
const u32 CACHE_FILE_COUNT = __XENON? 2 : 1;
const u64 CACHE_FILE_BLOCK_COUNT = 1 << ((__XENON? 32 : 33) - CACHE_BLOCK_SHIFT);
const u32 CACHE_PADDING = 128 - CACHE_FILE_COUNT;
const u32 CACHE_BLOCK_COUNT	= CACHE_BLOCK_MAX - CACHE_FILE_COUNT - CACHE_PADDING;	// Slot zero is used for magic and linked list.  Because of max size limits on PS3, this cannot be 4096.

// Data structure which maps a source block to the cache block tracking it (the cache block itself may be either resident or pending)
static atMultiRangeArray<u16,CACHE_FILE_COUNT,CACHE_FILE_BLOCK_COUNT> s_ResidentMap;
CompileTimeAssert(sizeof(s_ResidentMap) == __XENON? 2048 : 8192);

// STATE_PENDING_EITHER is a bitmask that encompasses the two pending states, and will not match any other states.
enum { STATE_RESIDENT = 0, STATE_PENDING_NORMAL = 1, STATE_PENDING_BOOT = 2, STATE_PENDING_EITHER = 3, STATE_INVALID = 4 };
static u32 s_PendingState = STATE_PENDING_BOOT;
static volatile int s_NumValidBlocks;
static volatile int s_NumInProgressBlocks;

void fiDiskCache::LeaveBootMode() { s_PendingState = STATE_PENDING_NORMAL; }

// Final builds always have monolithic rpfs.  Other builds may have more.
atRangeArray<fiCachedDevice,CACHE_FILE_COUNT> s_CachedDevices;

// CACHE_BLOCK_SIZE times CACHE_BLOCK_COUNT must be no larger than 2G.
// The cache itself is one block smaller than this amount, with a fixed-size
// header; this allows the cache file to fill a 2G cache drive without running
// afoul of directory overhead, etc.

struct CacheHeader {
	atRangeArray<Slot,CACHE_BLOCK_COUNT> Slots;
	atRangeArray<File,CACHE_FILE_COUNT> Files;
	atRangeArray<u32,CACHE_PADDING*2> Padding;
};
static CacheHeader *s_Header;

const u32 CACHE_FILE_HEADER_SIZE = sizeof(CacheHeader);

// Change this magic number (last digit) if you make any structural or semantic changes to the s_Header.
// In particular, changing either CACHE_BLOCK_COUNT, CACHE_BLOCK_SHIFT, CACHE_FILE_COUNT,
// or CACHE_FILE_SHIFT require a new magic number to avoid crashing on old cache files.
const u32 CACHE_FILE_MAGIC = 'R*$8';
u32 CACHE_FILE_VERSION;
const u32 CACHE_FILE_SIZE = CACHE_FILE_HEADER_SIZE + (CACHE_BLOCK_COUNT-1) * CACHE_BLOCK_SIZE;
// Make sure we're under the hard limits for the cache partition on each platform.
// 360 max size seems to be lower (possibly because of the larger number of files).
CompileTimeAssert(CACHE_FILE_SIZE <= (!__XENON? 0x7FF70000 : 0x7D800000));
static sysCriticalSectionToken s_Header_CritSec;

// The head and tail of the list simply live in slot zero, keeping the data simple.
#define s_Magic		(s_Header->Slots[0].magic)
#define s_Slots		(s_Header->Slots)
#define s_Oldest	(s_Header->Slots[0].older)
#define s_Newest	(s_Header->Slots[0].newer)
#define s_Files		(s_Header->Files)

NOSTRIP_XPARAM(clearhddcache);

#if __XENON
static char s_CachePath[32] = "cache:\\" ;
#elif __PPU
static char s_CachePath[CELL_SYSCACHE_PATH_MAX];			// typically /dev_hdd1 but we're not supposed to count on that.
#else
static char s_CachePath[32] = { "c:/cache_drive/" };
#endif

#define MULTIPLE_CACHE_FILES	(__XENON && 1)

static fiHandle s_CacheFile = fiHandleInvalid;
#if MULTIPLE_CACHE_FILES
const u32 CACHE_SUBFILE_SHIFT =  0;		// number of bits per cache file
const u32 CACHE_SUBFILE_COUNT = 256;		// number of cache files
const u32 CACHE_SUBFILE_MASK = (1 << CACHE_SUBFILE_SHIFT) - 1;
CompileTimeAssert((1 << CACHE_SUBFILE_SHIFT) * CACHE_SUBFILE_COUNT == CACHE_BLOCK_MAX);
static atRangeArray<fiHandle,CACHE_SUBFILE_COUNT> s_CacheBlockFiles;
#define CACHE_BLOCK_HANDLE(x)	(s_CacheBlockFiles[(x - 1) >> CACHE_SUBFILE_SHIFT])
#else
#define CACHE_BLOCK_HANDLE(x)	s_CacheFile
#endif

// Cache limit is 2G so no reason to use a u64 here.  Even on PS3 gamedata the limit is still 4G.
static inline u32 CACHE_BLOCK_ADDR(u32 slot)
{
	TrapZ(slot);
	TrapGE(slot,CACHE_BLOCK_COUNT);
#if MULTIPLE_CACHE_FILES
	return ((slot - 1) & CACHE_SUBFILE_MASK) << CACHE_BLOCK_SHIFT;
#else
	return CACHE_FILE_HEADER_SIZE + ((slot - 1) << CACHE_BLOCK_SHIFT);
#endif
}

#define CACHEDEV	(fiDeviceLocal::GetInstance())

static void ResetCache();
static void MakeOldest(u32);
static void MakeNewest(u32);
static void FlushCacheHeader();


static bool ValidateCacheHeader()
{
	if (PARAM_clearhddcache.Get()) {
		dcWarningf("-clearhddcache on command line");
		return false;
	}

#if __PS3 && !MULTIPLE_CACHE_FILES	// cheap on PS3 (and required for gamedata) so do it.
	u64 fs = CACHEDEV.GetFileSize(s_CachePath);
	if (fs != CACHE_FILE_SIZE) {
		dcErrorf("Cache file is wrong size?  (got %x, expected %x)",(u32)fs,CACHE_FILE_SIZE);
		return false;
	}
#endif

	fiHandle temp = CACHEDEV.Open(s_CachePath,false);
	if (!fiIsValidHandle(temp)) {
		dcWarningf("No cache file in cache partition.");
		return false;
	}

	int amtRead = CACHEDEV.ReadBulk(temp,0,s_Header,CACHE_FILE_HEADER_SIZE);
	CACHEDEV.Close(temp);

	if (amtRead != CACHE_FILE_HEADER_SIZE) {
		dcErrorf("ValidateCacheHeader - short read on file");
		return false;
	}

	if (s_Magic != CACHE_FILE_MAGIC) {
		dcWarningf("ValidateCacheHeader - old magic number (got %x, expected %x)",s_Magic,CACHE_FILE_MAGIC);
		return false;
	}

	if (s_Oldest < 1 || s_Oldest >= CACHE_BLOCK_COUNT || s_Newest < 1 || s_Newest >= CACHE_BLOCK_COUNT) {
		dcErrorf("ValidateCacheHeader - invalid linked list anchors");
		return false;
	}

	atRangeArray<bool,CACHE_BLOCK_COUNT> visited;
	memset(&visited,0,sizeof(visited));
	u32 visitCount = 0, prevI = 0;
	for (u32 i = s_Oldest; i; prevI=i,i=s_Slots[i].newer,visitCount++) {
		if (visited[i]) {
			dcErrorf("ValidateCacheHeader - loop in linked list");
			return false;
		}
		visited[i] = true;
		if (s_Slots[i].older != prevI) {
			dcErrorf("ValidateCacheHeader - previous link is incorrect");
			return false;
		}
		if (s_Slots[i].newer >= CACHE_BLOCK_COUNT) {
			dcErrorf("ValidateCacheHeader - next link is impossible");
			return false;
		}
	}
	if (prevI != s_Newest) {
		dcErrorf("ValidateCacheHeader - new head link is incorrect");
		return false;
	}
	if (visitCount != CACHE_BLOCK_COUNT-1) {
		dcErrorf("ValidateCacheHeader - linked list is incomplete");
		return false;
	}
	dcDisplayf("ValidateCacheHeader - existing cache is valid.");
	return true;
}

#if __PS3 && ENABLE_GAMEDATA_FOR_DISKCACHE
bool CopyContentInfoFile(const char *filename,const char *contentInfoPath,int copySize) {
	char srcFile[64], dstFile[CELL_GAME_PATH_MAX];
	formatf(srcFile,"/dev_bdvd/PS3_GAME/%s",filename);
	formatf(dstFile,"%s/%s",contentInfoPath,filename);
	dcDisplayf("Copying %s to %s (%d bytes)...",srcFile,dstFile,copySize);
	char *buffer = Alloca(char,copySize);
	fiHandle h = CACHEDEV.Open(srcFile, true);
#if !__FINAL
	if (!fiIsValidHandle(h)) {
		dcDisplayf("(no blu ray mounted, trying /app_home instead...)");
		formatf(srcFile,"/app_home/PS3_GAME/%s",filename);
		h = CACHEDEV.Open(srcFile, true);
	}
#endif
	if (!fiIsValidHandle(h))
		return false;
	if (CACHEDEV.Read(h,buffer,copySize) != copySize) {
		CACHEDEV.Close(h);
		return false;
	}
	CACHEDEV.Close(h);
	h = CACHEDEV.Create(dstFile);
	if (!fiIsValidHandle(h))
		return false;
	if (CACHEDEV.Write(h,buffer,copySize) != copySize) {
		CACHEDEV.Close(h);
		CACHEDEV.Delete(dstFile);
		return false;
	}
	CACHEDEV.Close(h);
	return true;
}
#endif

/* Performance tests: (on a 1400, so hard drive performance should be similar to retail?)
	#rdr -megarpfs -forcebootcd -clearhddcache -All_tty=fatal -XDiskCache_tty=debug2 -introshot=0 -introweather=0
	Via gamedata: (note about 18s is the cache file initialization)
	ASYNC_CACHE_COPIES=1, ASYNC_READ_DATA=0: 58.3s
	ASYNC_CACHE_COPIES=1, ASYNC_READ_DATA=1: 57.8s
	ASYNC_CACHE_COPIES=0, ASYNC_READ_DATA=0: 63.2s
	ASYNC_CACHE_COPIES=0, ASYNC_READ_DATA=1: 63.7s
	Via syscache:
	ASYNC_CACHE_COPIES=1, ASYNC_READ_DATA=0: 46.5s
	ASYNC_CACHE_COPIES=1, ASYNC_READ_DATA=1: 46.1s
	ASYNC_CACHE_COPIES=0, ASYNC_READ_DATA=0: 49.0s
	ASYNC_CACHE_COPIES=0, ASYNC_READ_DATA=1: 47.5s

	Now, using a primed cache: (ie removing -clearhddcache) (ASYNC_CACHE_COPIES doesn't matter, no cache misses)
	Via gamedata:
	ASYNC_READ_DATA=0: 19.3s
	ASYNC_READ_DATA=1: 26.0s
	Via syscache:
	ASYNC_READ_DATA=0: 19.5s
	ASYNC_READ_DATA=1: 19.0s

	Finally, with the gamedata support (and extra higher-level renderthread support) compiled out,
	but ASYNC_CACHE_COPIES=1, ASYNC_READ_DATA=1: 18.6s
*/

// Whether cache misses are serviced asynchronously or not based on holdoff time.
#define ASYNC_CACHE_COPIES		(__XENON || __PS3)

#if !__WIN32PC
#if ASYNC_CACHE_COPIES
static sysIpcThreadId s_CacheWorkerThread = sysIpcThreadIdInvalid;
volatile bool s_ShutdownCacheWorkerThread;
#endif
#ifdef CONST_FREQ
static volatile utimer_t s_HoldoffDelta;
#endif
static pgReadData s_Reader;
#endif

static int s_HoldoffTime;
volatile unsigned fiDiskCache::sm_LastCacheCopyCompletion;


void fiDiskCache::SetHoldoffTime(int ms) {
	s_HoldoffTime = ms;
#ifdef CONST_FREQ
	// (ticks/sec) * (1 sec / 1000ms) * (ms) = ticks
	s_HoldoffDelta = (utimer_t)(CONST_FREQ / 1000) * ms;
#endif
}


int fiDiskCache::GetHoldoffTime() {
	return s_HoldoffTime;
}

float fiDiskCache::GetCacheOccupancy() {
	// Clamp the result in case our computations go haywire somehow.
	if (s_NumValidBlocks <= 0)
		return 0.0f;
	else if (s_NumValidBlocks >= CACHE_BLOCK_COUNT)
		return 1.0f;
	else 
		return (float) s_NumValidBlocks * (1.0f / CACHE_BLOCK_COUNT);
}

float fiDiskCache::GetCacheInProgress() {
	return (float) s_NumInProgressBlocks * (1.0f / CACHE_BLOCK_COUNT);
}


// Assume the cache is primed if you never called fiDiskCache::InitClass, so that normal (non-megarpf)
// development will still get random start screens, etc.
static bool s_IsCachePrimed = true;

bool fiDiskCache::IsCachePrimed() {
	dcDisplayf("IsCachePrimed returns %d",s_IsCachePrimed);
	return s_IsCachePrimed;
}

bool fiDiskCache::InitClass(int 
#if !__WIN32PC
							cpu
#endif
							,const char *PS3_ONLY(XEX_TITLE)) {
	sysTimer initTimer;
#if MULTIPLE_CACHE_FILES
	for (u32 i=0; i<CACHE_SUBFILE_COUNT; i++)
		s_CacheBlockFiles[i] = fiHandleInvalid;
#endif
#if __WIN32PC
	return false;
#else
	RAGE_TRACK(DiskCache);
	Assert(!s_Header);
	s_Header = rage_new CacheHeader;
# if __XENON
	/*	Use XContentGetLicenseMask to detect if title code is being run from the hard drive. If the code is 
		being run from a disc image on the hard drive, then XContentGetLicenseMask returns ERROR_SUCCESS. 
		If the code is being run from a game disc in the optical drive, then XContentGetLicenseMask 
		fails, returning ERROR_FUNCTION_FAILED. */
	DWORD licenseMask;
	if (XContentGetLicenseMask(&licenseMask,NULL) == ERROR_SUCCESS) {
		dcDisplayf("Game installed to hard drive, caching is unnecessary.");
		// This is a special case -- we don't cache because there's no reason to, not because
		// we're unable to do it.  Therefore, pretend the cache is available so higher level
		// code doesn't scale back content for a Core system.
		s_IsCachePrimed = true;
		s_NumValidBlocks = CACHE_BLOCK_COUNT;
		return true;
	}
	else
		dcDisplayf("Game running from optical media, cache will be allowed.");

	// Assume the cache isn't primed at this point.
	s_IsCachePrimed = false;

	if (!fiCachePartition::Init())
		return false;
# elif __PPU
	if (!XEX_TITLE_ID || strlen(XEX_TITLE_ID) != 9)
		Quitf("Must have valid nine-character-long XEX_TITLE_ID to use disk cache on PS3.");

#  if ENABLE_GAMEDATA_FOR_DISKCACHE
	if (XEX_TITLE) {
		if (sysIpcLoadModule(CELL_SYSMODULE_SYSUTIL_GAME) < 0) {
			dcErrorf("Unable to load game sysutil.");
			return false;
		}

		// GetFileSize returns 0 if the file doesn't exist (which will happen when not running emulation builds)
		int icon0PngSize = CACHEDEV.GetFileSize("/dev_bdvd/PS3_GAME/ICON0.PNG");
#   if !__FINAL
		if (!icon0PngSize)	// try here so we don't get bogus bugs about missing icons.
			icon0PngSize = CACHEDEV.GetFileSize("/app_home/PS3_GAME/ICON0.PNG");
#   endif
		int needSize = (CACHE_FILE_SIZE + icon0PngSize + 1023) >> 10;
		static const char gameCacheVersion[] = "01.00";
		char gamedataDir[CELL_GAME_PATH_MAX];
		safecpy(gamedataDir, XEX_TITLE_ID);
		safecat(gamedataDir, "-CACHE");

		char checkCacheVersion[CELL_GAME_SYSP_VERSION_SIZE] = "";
		CellGameContentSize contentSize;
		int code = cellGameDataCheck ( CELL_GAME_GAMETYPE_GAMEDATA, gamedataDir, &contentSize );
		if (code < 0) {
			dcErrorf("cellGameDataCheck failed, code %x",code);
			cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA,needSize,gamedataDir);
			sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
			return false;
		}
		else if (code == CELL_GAME_RET_NONE) {
			CellGameSetInitParams initParam;
			char tmp_contentInfoPath [ CELL_GAME_PATH_MAX ];
			char tmp_usrdirPath [ CELL_GAME_PATH_MAX ];

			memset ( &initParam, 0, sizeof ( CellGameSetInitParams ) );
			safecpy( initParam.title, XEX_TITLE /* like "Red Dead Redemption Temporary Data" */);
			safecpy( initParam.titleId, gamedataDir /* like "TEST12345CACHE" */);
			safecpy( initParam.version,  gameCacheVersion);

			code = cellGameCreateGameData ( &initParam, tmp_contentInfoPath, tmp_usrdirPath );
			if (code < 0) {
				dcErrorf("cellGameCreateGameData failed, code %x", code);
				cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA,needSize,gamedataDir);
				sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
				return false;
			}
		}
		else if (cellGameGetParamString(CELL_GAME_PARAMID_VERSION,checkCacheVersion,sizeof(checkCacheVersion)) != CELL_OK ||
				strcmp(checkCacheVersion, gameCacheVersion)) {
			dcErrorf("gamedata version check failed, '%s' != '%s'", checkCacheVersion,gameCacheVersion);
			cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA,needSize,gamedataDir);
			sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
			return false;
		}

		char contentInfoPath [ CELL_GAME_PATH_MAX ];
		code = cellGameContentPermit ( contentInfoPath, s_CachePath );
		if ( code < 0 ) {
			dcErrorf("cellGameContentPermit failed, code %x", code);
			cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA,needSize,gamedataDir);
			sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
			return false;
		}
		else {
			safecat(s_CachePath,"/",sizeof(s_CachePath));
			dcDisplayf("Using '%s' for game content path.",s_CachePath);
		}

		if (code && code != (int)CELL_GAME_ERROR_NOSPACE) {
			cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA,needSize,gamedataDir);
			sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
			return false;
		}
		else if (code == (int)CELL_GAME_ERROR_NOSPACE || needSize > contentSize.hddFreeSizeKB) {
			cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_NOSPACE_EXIT,needSize,gamedataDir);
			sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);
			return false;
		}
		sysIpcUnloadModule(CELL_SYSMODULE_SYSUTIL_GAME);

		// Failing to copy files from blu-ray is only fatal on final builds.
		if (!CopyContentInfoFile("ICON0.PNG",contentInfoPath,icon0PngSize) && __FINAL) {
			cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA,needSize,gamedataDir);
			dcErrorf("copying content info files failed.");
			return false;
		}
	}
	else 
#  endif	// ENABLE_GAMEDATA_FOR_SYSCACHE
	{
		if (!fiCachePartition::Init())
			return false;
		safecpy(s_CachePath,fiCachePartition::GetCachePrefix(),sizeof(s_CachePath));
		safecat(s_CachePath,"/",sizeof(s_CachePath));
	}
# endif	// __XENON || __PPU

	safecat(s_CachePath,"cache.dat");

	if (!ValidateCacheHeader()) {
		// If the file is invalid, nuke the partition and try again
#if __XENON
		if (!fiCachePartition::Reset())
			return false;
		s_CacheFile = CACHEDEV.Create(s_CachePath);
		if (!fiIsValidHandle(s_CacheFile))
			return false;
		CACHEDEV.Close(s_CacheFile);
# if MULTIPLE_CACHE_FILES
		for (u32 i=0; i<CACHE_SUBFILE_COUNT; i++) {
			char buf[sizeof(s_CachePath) + 8];
			formatf(buf,"%s.%03u",s_CachePath,i);
			fiHandle tempFile = CACHEDEV.Create(buf);
			if (!fiIsValidHandle(tempFile)) {
				CACHEDEV.Close(s_CacheFile);
				s_CacheFile = fiHandleInvalid;
				return false;
			}
			CACHEDEV.Close(tempFile);
		}
# endif
#elif __PS3
		if (XEX_TITLE)
			CACHEDEV.Delete(s_CachePath);
		else if (!fiCachePartition::Reset())
			return false;

		s_CacheFile = CACHEDEV.Create(s_CachePath);
		if (!fiIsValidHandle(s_CacheFile))
			return false;
		CACHEDEV.Close(s_CacheFile);
# if ENABLE_GAMEDATA_FOR_DISKCACHE
		if (XEX_TITLE) {
			unsigned int type =   CELL_MSGDIALOG_TYPE_SE_MUTE_ON
				| CELL_MSGDIALOG_TYPE_BUTTON_TYPE_NONE
				| CELL_MSGDIALOG_TYPE_DISABLE_CANCEL_ON
				| CELL_MSGDIALOG_TYPE_DEFAULT_CURSOR_NONE
				| CELL_MSGDIALOG_TYPE_PROGRESSBAR_SINGLE;
			int result = cellMsgDialogOpen2(type, XEX_TITLE, NULL, NULL, NULL);
			if (result) {
				dcErrorf("cellMsgDialogOpen2 failed, code %x",result);
				return false;
			}
			sysIpcSleep(50);	// give dialog time to show before we start updating it.
			const int STEPS = 100;
			for (int i=1; i<=STEPS; i++) {
				result |= cellFsAllocateFileAreaWithoutZeroFill(s_CachePath,((u64)CACHE_FILE_SIZE * i) / STEPS);
				result |= cellMsgDialogProgressBarInc(CELL_MSGDIALOG_PROGRESSBAR_INDEX_SINGLE, 100 / STEPS);
			}
			cellMsgDialogClose(0.0f);
			sysIpcSleep(200);		// Let the dialog fade out nicely.
			if (result) {
				dcErrorf("something failed during size grow, code %x",result);
				return false;
			}
		}
		else 
# endif	// ENABLE_GAMEDATA_FOR_DISKCACHE
		{
			int result = cellFsAllocateFileAreaWithoutZeroFill(s_CachePath,CACHE_FILE_SIZE);
			if (result) {
				dcErrorf("Allocate file area failed, code %x",result);
				return false;
			}
		}
#endif	// __XENON || __PS3
		s_CacheFile = CACHEDEV.Open(s_CachePath,false);
		ResetCache();
		FlushCacheHeader();
		dcDisplayf("Rebuilt the cache file from scratch.");
	}
	else
		s_CacheFile = CACHEDEV.Open(s_CachePath,false);

	if (!fiIsValidHandle(s_CacheFile)) {
		dcErrorf("Unable to reopen the cache file?");
		return false;
	}
#if MULTIPLE_CACHE_FILES
	for (u32 i=0; i<CACHE_SUBFILE_COUNT; i++) {
		char buf[sizeof(s_CachePath) + 8];
		formatf(buf,"%s.%03u",s_CachePath,i);
		s_CacheBlockFiles[i] = CACHEDEV.Open(buf,false);
		if (!fiIsValidHandle(s_CacheBlockFiles[i])) {
			dcErrorf("Unable to reopen cache subfile %u",i);
			while (i--) {
				CACHEDEV.Close(s_CacheBlockFiles[i]);
				s_CacheBlockFiles[i] = fiHandleInvalid;
			}
			CACHEDEV.Close(s_CacheFile);
			s_CacheFile = fiHandleInvalid;
			return false;
		}
	}
#endif

	// Make any blocks that were in progress as old as possible
	int numValidBlocks = 0;
	for (u32 i=1; i<CACHE_BLOCK_COUNT; i++)
	{
		if (s_Slots[i].state & STATE_PENDING_EITHER) {
			MakeOldest(i);
			s_Slots[i].state = STATE_INVALID;
		}
		else if (s_Slots[i].state == STATE_RESIDENT) {
			// Update the residency map.
			Assign(s_ResidentMap[s_Slots[i].srcFile][s_Slots[i].srcBlock],i);
			++numValidBlocks;
		}
	}
	s_NumValidBlocks = numValidBlocks;

	// We consider the cache primed if at least one thirty second (about 3%) of the sectors are valid.
	// Boot mode sectors are always done first, and there are about 22 of them (out of 1024 on PS3); round that
	// up to 32 for good measure, and 1024/32 is 32.  Still some screens that apparently take a long time
	// with about 10% primed, so let's bump this up a bit, to 25%, B*211487.
	s_IsCachePrimed = (numValidBlocks >= (CACHE_BLOCK_MAX>>2));

	// This is probably overkill, but make sure that we don't cache during load until higher-level code lowers it.
	SetHoldoffTime(5000);

	s_Reader.Init(PRIO_ABOVE_NORMAL,cpu,"[RAGE] DiskCache Reader",0,pgReadData::READ_BUFFERS_HDD);
#if ASYNC_CACHE_COPIES
	s_CacheWorkerThread = sysIpcCreateThread(CacheWorker,0,sysIpcMinThreadStackSize,PRIO_ABOVE_NORMAL,"[RAGE] DiskCache Worker",cpu);
#endif
	dcDisplayf("Cache is ready to go in %.3fs (%u valid blocks, cache is %sprimed).",initTimer.GetTime(),numValidBlocks,s_IsCachePrimed?"":"NOT ");
	return true;
#endif		// !__WIN32PC
}

void fiDiskCache::ShutdownClass() {
#if !__WIN32PC
#if ASYNC_CACHE_COPIES
	if (s_CacheWorkerThread != sysIpcThreadIdInvalid) {
		s_ShutdownCacheWorkerThread = true;
		sysIpcWaitThreadExit(s_CacheWorkerThread);
		s_CacheWorkerThread = sysIpcThreadIdInvalid;
	}
#endif
	s_Reader.Shutdown();

	if (fiIsValidHandle(s_CacheFile)) {
		CACHEDEV.Close(s_CacheFile);
		s_CacheFile = fiHandleInvalid;
#if MULTIPLE_CACHE_FILES
		for (u32 i=0; i<CACHE_SUBFILE_COUNT; i++) {
			if (s_CacheBlockFiles[i] != fiHandleInvalid)
				CACHEDEV.Close(s_CacheBlockFiles[i]);
			s_CacheBlockFiles[i] = fiHandleInvalid;
		}
#endif
	}
	delete s_Header;
	s_Header = NULL;
#endif
}

static void InvalidateBlocksForFile(u32 file) {
	for (u32 i=1; i<CACHE_BLOCK_COUNT; i++) {
		if (s_Slots[i].srcFile == file && s_Slots[i].state == STATE_RESIDENT) {
			// Clear residency map entry.
			Assert(s_ResidentMap[s_Slots[i].srcFile][s_Slots[i].srcBlock] == i);
			s_ResidentMap[s_Slots[i].srcFile][s_Slots[i].srcBlock] = 0;
			// Mark the slot invalid.
			s_Slots[i].state = STATE_INVALID;
			--s_NumValidBlocks;
			MakeOldest(i);
		}
	}
}


bool fiDiskCache::IsCacheEnabled() {
	return fiIsValidHandle(s_CacheFile);
}


const fiCachedDevice* fiDiskCache::GetCachedDevice(const char *fullPathName,const fiPackfile *pf) {
	AssertMsg(fiIsValidHandle(s_CacheFile),"You shouldn't be asking for a cached device if you didn't init the cache.");

	if (!fiIsValidHandle(s_CacheFile))
		return NULL;

	const fiDevice *device = fiDevice::GetDevice(fullPathName, true);
	if (!device)
		return NULL;

	// sysTimer T;
	u64 fileSize = pf->GetPackfileSize();
	u64 fileTime = pf->GetPackfileTime();
	fiHandle handle = pf->GetPackfileHandle();

	u32 sizeTimeHash = (u32)((fileSize >> 32) ^ fileSize ^ (fileTime >> 32) ^ fileTime);
	dcDebugf1("GetCachedDevice(%s) - %" I64FMT "x %" I64FMT "x -> %x",fullPathName,fileSize,fileTime,sizeTimeHash);

	// Search for the file in the existing table
	u32 i, nameHash = atStringHash(fullPathName), anEmptySlot = CACHE_FILE_COUNT;

	for (i=0; i<CACHE_FILE_COUNT; i++) {
		if (s_Files[i].namehash == nameHash) {
			if (s_Files[i].sizetimehash == sizeTimeHash)
				dcDebugf1("Found file '%s' in fileslot %u, contents are unchanged.",fullPathName,i);
			else {
				dcDebugf1("Found file '%s' in fileslot %u, but contents have changed; invalidating any cached data for just this file.",fullPathName,i);
				s_Files[i].sizetimehash = sizeTimeHash;
				InvalidateBlocksForFile(i);
				FlushCacheHeader();
			}
			break;
		}
		else if (!s_Files[i].namehash && anEmptySlot == CACHE_FILE_COUNT)
			anEmptySlot = i;
	}

	// File we know nothing about?
	if (i == CACHE_FILE_COUNT) {
		SYS_CS_SYNC(s_Header_CritSec);
		// Have all slots been used already?
		if (anEmptySlot == CACHE_FILE_COUNT) {
			// Look for a slot that doesn't have a device associated with it yet this run.
			for (anEmptySlot=0; anEmptySlot<CACHE_FILE_COUNT; anEmptySlot++)
				if (!s_CachedDevices[anEmptySlot].m_InnerDevice)
					break;
			if (anEmptySlot == CACHE_FILE_COUNT) {
				Quitf(ERR_FIL_DISK,"Ran out of cache file slots?  Raise CACHE_FILE_COUNT if you need more archives.");
			}
			else {
				dcDebugf1("Recycling disconnected cache slot %u",anEmptySlot);
				InvalidateBlocksForFile(anEmptySlot);
			}
		}
		i = anEmptySlot;
		dcDebugf1("New file '%s' assigned to fileslot %u.",fullPathName,i);
		s_Files[i].namehash = nameHash;
		s_Files[i].sizetimehash = sizeTimeHash;
		FlushCacheHeader();
	}

	fiCachedDevice &that = s_CachedDevices[i];
	that.m_InnerDevice = device;
	that.m_InnerHandle = handle;
	that.m_SelfIndex = i;
	that.m_InnerSize = fileSize;
	that.m_SortKey = pf->GetBasePhysicalSortKey();
	safecpy(that.m_Name,ASSET.FileName(fullPathName));
	// dcDisplayf("GetCachedDevice(%s) took %f ms to read data, %f total to process",fullPathName,toReady,T.GetMsTime());
	return &that;
}


fiHandle fiCachedDevice::Open(const char * /*filename*/,bool /*readOnly*/) const {
	AssertMsg(false , "Cannot Open on cached device");
	return fiHandleInvalid;
}

fiHandle fiCachedDevice::OpenBulk(const char * /*filename*/,u64 & outBias) const {
	outBias = 0;
	return m_InnerHandle;
}

fiHandle fiCachedDevice::Create(const char * /*filename*/) const {
	AssertMsg(false , "Cannot Create on cached device");
	return fiHandleInvalid;
}

int fiCachedDevice::Read(fiHandle /*handle*/,void * /*outBuffer*/,int /*bufferSize*/) const {
	AssertMsg(false , "Cannot Read from cached device");
	return 0;
}

static void ResetCache()
{
	s_Magic = CACHE_FILE_MAGIC;
	s_Oldest = 1;
	s_Newest = CACHE_BLOCK_COUNT-1;
	// Build the cache block chain; first block (1) is oldest (and 0 means no link)
	for (u32 i=1; i<CACHE_BLOCK_COUNT; i++) {
		s_Slots[i].state = STATE_INVALID;
		Assign(s_Slots[i].older,i-1);
		Assign(s_Slots[i].newer,i+1);
	}
	// Fix the last entry since we got it wrong in the loop above.
	s_Slots[CACHE_BLOCK_COUNT-1].newer = 0;
	for (u32 i=0; i<CACHE_FILE_COUNT; i++)
		s_Files[i].namehash = s_Files[i].sizetimehash = 0;
	s_NumValidBlocks = 0;
}

static void MakeNewest(u32 i) {
	Slot &cur = s_Slots[i];
	// If this slot isn't already newest, make it newest.
	if (cur.newer) {
		// Remove self from current location in chain
		s_Slots[cur.newer].older = cur.older;
		if (cur.older)
			s_Slots[cur.older].newer = cur.newer;
		else
			s_Oldest = cur.newer;
		// Patch ourselves into the head as newest.
		Assign(s_Slots[s_Newest].newer,i);
		cur.older = s_Newest;
		cur.newer = 0;
		Assign(s_Newest,i);
	}
	/* Printf("NEWEST: ");
	int j = s_Newest;
	while (j) {
		Printf("%d -> ",j);
		j = s_Slots[j].older;
	}
	Printf(": OLDEST\n"); */
}

static void MakeOldest(u32 i) {
	Slot &cur = s_Slots[i];
	// If this slot isn't already oldest, make it oldest.
	if (cur.older) {
		// Remove self from current location in chain
		s_Slots[cur.older].newer = cur.newer;
		if (cur.newer)
			s_Slots[cur.newer].older = cur.older;
		else
			s_Newest = cur.older;
		// Patch ourselves into the tail as oldest
		Assign(s_Slots[s_Oldest].older,i);
		cur.newer = s_Oldest;
		cur.older = 0;
		Assign(s_Oldest,i);
	}
}

static void FlushCacheHeader()
{
	// Lock the critsec long enough to make a copy so we know it's consistent on disc.
	s_Header_CritSec.Lock();
	static char tempHeader[CACHE_FILE_HEADER_SIZE];
	sysMemCpy(tempHeader,s_Header,CACHE_FILE_HEADER_SIZE);
	s_Header_CritSec.Unlock();

	CACHEDEV.WriteBulk(s_CacheFile,0,tempHeader,CACHE_FILE_HEADER_SIZE);

#if __PS3
	cellFsFsync(s_CacheFile);
#elif __XENON
	// TODO: Figure out how often we should be calling this.
	XFlushUtilityDrive();
#endif
}

#if !__WIN32PC

extern fiStream *pgStreamingLog;
extern sysCriticalSectionToken pgStreamingLogToken;


// Block copy currently in progress (protected by s_Header_CritSec).
volatile u32 fiDiskCache::sm_Copying;

bool fiDiskCache::CopyCacheSector(u32 dstBlock)
{
#if !__NO_OUTPUT
	u32 now = sysTimer::GetSystemMsTime(), delayed = 0;
	static u32 accumTime, accumCount;
#endif

	// Flush the cache header now before we start copying because as soon as the
	// copy is started, the cache will be in an invalid state and we want to remember
	// that all the blocks marked as pending are now invalid.  Strictly speaking,
	// we should flush the cache header again once the copy is completed but that's
	// not as important and we want to avoid excessive flushes.  In the worst case,
	// the last cache miss we had will still be marked invalid.
	// Lock the hard drive mutex first but allow the optical mutex to be outstanding
	// for a little while longer, since the flush we do is pretty expensive.
	sysIpcLockMutex(pgIsActive[pgStreamer::HARDDRIVE]);

#if !__FINAL
	if (pgStreamer::sm_ProcessCallback)
		pgStreamer::sm_ProcessCallback("FLUSHING cache header",1U<<31,CACHE_BLOCK_SIZE,0,0,0,0,0,0,0);
#endif

	FlushCacheHeader();
	sysIpcLockMutex(pgIsActive[pgStreamer::OPTICAL]);

	// Determine the source and destination of the copy.
	u32 srcDeviceIndex = s_Slots[dstBlock].srcFile;
	u32 srcBlock = s_Slots[dstBlock].srcBlock;

#if !__FINAL
	if (pgStreamer::sm_ProcessCallback) {
		char tmp[64];
		formatf(tmp, "STARTING copy %u->%u %s",srcBlock,dstBlock,s_Slots[dstBlock].state == STATE_PENDING_BOOT ? "BOOT" : "norm");
		pgStreamer::sm_ProcessCallback(tmp,1U<<31,CACHE_BLOCK_SIZE,0,0,0,0,0,0,0);
	}
#endif

	const fiCachedDevice *cachedDevice = &s_CachedDevices[srcDeviceIndex];
	u64 srcOffset = (u64)srcBlock << CACHE_BLOCK_SHIFT;
	u64 dstOffset = CACHE_BLOCK_ADDR(dstBlock);
	fiHandle dstHandle = CACHE_BLOCK_HANDLE(dstBlock);
	dcDebugf2("dstBlock = %d, handle %" HANDLEFMT ", offset %" I64FMT "x",dstBlock,dstHandle,dstOffset);
	u32 blockRemain = CACHE_BLOCK_SIZE;
	if (srcOffset + blockRemain > cachedDevice->m_InnerSize)
		blockRemain = u32(cachedDevice->m_InnerSize - srcOffset);
	fiHandle srcHandle = cachedDevice->m_InnerHandle;
	const fiDevice *srcDevice = cachedDevice->m_InnerDevice; 
	int toRead = (blockRemain > pgReadData::MAX_READ)? pgReadData::MAX_READ : blockRemain;
	// Start copying the file
	bool completed = true;
	s_Reader.Request(srcDevice,srcHandle,srcOffset,toRead,cachedDevice->m_SortKey,false,0,0,NULL,false);
#if !__FINAL
	if (pgStreamingLog) {
		SYS_CS_SYNC(pgStreamingLogToken);
		fprintf(pgStreamingLog,"%9u,dskcache,%9u,%5u,%s,block%04u\r\n",sysTimer::GetSystemMsTime(),cachedDevice->m_SortKey + u32(srcOffset>>11),(toRead + 2047) >> 11,cachedDevice->m_Name,dstBlock);
	}
#endif

	while (blockRemain) {
		// Wait for the previous read to finish.
		char *writeBuffer = NULL;
		Assert(false);
		int amtRead = /*s_Reader.Wait(writeBuffer)*/ 0;
		if (amtRead != toRead) {
			dcErrorf("Short read (got %d, expected %d) during cache copy!",amtRead,toRead);
			completed = false;
			break;
		}
		blockRemain -= toRead;
		srcOffset += toRead;

		// Schedule the next read if we think we'll need one.
		if (blockRemain) {
			toRead = (blockRemain > pgReadData::MAX_READ)? pgReadData::MAX_READ : blockRemain;
			s_Reader.Request(srcDevice,srcHandle,srcOffset,toRead,cachedDevice->m_SortKey,false,0,0,NULL,false);
#if !__FINAL
			if (pgStreamingLog) {
				SYS_CS_SYNC(pgStreamingLogToken);
				fprintf(pgStreamingLog,"%9u,dskcache,%9u,%5u,%s,block%04u\r\n",sysTimer::GetSystemMsTime(),cachedDevice->m_SortKey + u32(srcOffset>>11),(toRead + 2047) >> 11,cachedDevice->m_Name,dstBlock);
			}
#endif
		}
		// Write the next buffer out.
		int amtWritten = CACHEDEV.WriteBulk(dstHandle, dstOffset, writeBuffer, amtRead);
		if (amtWritten != amtRead) {
			dcErrorf("Short write (got %d, expected %d) during cache copy!",amtWritten,amtRead);
			completed = false;
			break;
		}
		dstOffset += amtWritten;
		// If we attempted to initiate an activity on either queue, back off immediately.
		while (!pgLastActivity[pgStreamer::OPTICAL] || !pgLastActivity[pgStreamer::HARDDRIVE])
		{
			OUTPUT_ONLY(u32 thisDelay = sysTimer::GetSystemMsTime());
			sysIpcUnlockMutex(pgIsActive[pgStreamer::HARDDRIVE]);
			sysIpcUnlockMutex(pgIsActive[pgStreamer::OPTICAL]);
			sysIpcSleep(10);
			sysIpcLockMutex(pgIsActive[pgStreamer::OPTICAL]);
			sysIpcLockMutex(pgIsActive[pgStreamer::HARDDRIVE]);
			OUTPUT_ONLY(delayed += (sysTimer::GetSystemMsTime() - thisDelay));
		}
	}

#if !__FINAL
	if (pgStreamer::sm_ProcessCallback) {
		char tmp[64];
		formatf(tmp, "FINISHED copy %u->%u %s",srcBlock,dstBlock,s_Slots[dstBlock].state == STATE_PENDING_BOOT ? "BOOT" : "norm");
		pgStreamer::sm_ProcessCallback(tmp,1U<<31,CACHE_BLOCK_SIZE,0,0,0,0,0,0,0);
	}
#endif

	// Mark this block as now being valid.  We'll flush the cache header next time through to save
	// the extra traffic right now.
	u32 slot = dstBlock;
	Assertf(slot < CACHE_BLOCK_COUNT,"Bad DstBlock %u",slot);
	Assertf(s_Slots[slot].state & STATE_PENDING_EITHER,"Bad block state %x (something get scheduled twice?)",s_Slots[slot].state);
	s_Header_CritSec.Lock();
	if (completed) {
		s_Slots[slot].state = STATE_RESIDENT;
		++s_NumValidBlocks;
		u32 completion = sysTimer::GetSystemMsTime();
#if !__NO_OUTPUT
		accumTime += (completion - now - delayed);
		++accumCount;
		dcDebugf1("Cache block %d finished copying in %ums (%ums delay)  avg: %ums.",slot,completion - now - delayed,delayed,accumTime / accumCount);
#endif
		sm_LastCacheCopyCompletion = completion;
	}
	else {
		s_Slots[slot].state = STATE_INVALID;
		dcErrorf("Cache block %d FAILED copying.",slot);
	}
	--s_NumInProgressBlocks;
	// Let the world know we're not still copying (while the critsec is still locked)
	sm_Copying = 0;
	s_Header_CritSec.Unlock();

	// Release our semaphores that block the streamer.
	sysIpcUnlockMutex(pgIsActive[pgStreamer::OPTICAL]);
	sysIpcUnlockMutex(pgIsActive[pgStreamer::HARDDRIVE]);

#if !__FINAL
	if (pgStreamingLog) {
		SYS_CS_SYNC(pgStreamingLogToken);
		fprintf(pgStreamingLog,"%9u,complete,%9u,    0,%s,block%04u\r\n",sysTimer::GetSystemMsTime(),cachedDevice->m_SortKey + u32(srcOffset>>11),cachedDevice->m_Name,dstBlock);
	}
#endif

	return completed;
}

#if ASYNC_CACHE_COPIES
void fiDiskCache::CacheWorker(void*) {
	while (!s_ShutdownCacheWorkerThread) {
		// Figure out how long it's been since there's been real activity.
		utimer_t now = sysTimer::GetTicks();
		utimer_t thenOptical = pgLastActivity[pgStreamer::OPTICAL];
#if DUAL_QUEUE_SUPPORT
		utimer_t thenHardDrive = pgLastActivity[pgStreamer::HARDDRIVE];
#endif
		// A 'then' time of zero means something is in progress already.
		// We wait for a holdoff time to elapse on optical (and nothing in progress on either).
		if (!pgStreamer::IsLocked(pgStreamer::OPTICAL) && thenOptical && 
#if DUAL_QUEUE_SUPPORT
				!pgStreamer::IsLocked(pgStreamer::HARDDRIVE) && thenHardDrive && 
#endif
				now - thenOptical >= s_HoldoffDelta) {
			s_Header_CritSec.Lock();

			// First pass; look for any PENDING_BOOT sectors, favoring the oldest one.
			int slot = s_Newest;
			int bootSlot = 0;
			while (slot) {
				// Keep remembering the oldest PENDING_BOOT sector.
				if (s_Slots[slot].state == STATE_PENDING_BOOT)
					bootSlot = slot;
				slot = s_Slots[slot].older;
			}
			// If we got a boot sector, process it
			if (bootSlot)
				slot = bootSlot;
			else {		// Else start over looking for a normal one.
				slot = s_Newest;
				while (slot) {
					// Locate the most recently asked-for pending block.
					if (s_Slots[slot].state == STATE_PENDING_NORMAL)
						break;
					slot = s_Slots[slot].older;
				}
			}
			// Remember that we're about to start a copy, while we still have the critsec.
			sm_Copying = slot;
			s_Header_CritSec.Unlock();

			if (slot)
				CopyCacheSector(slot);

			// If we finished a cache copy, or there was nothing to do, wait a little while
			// regardless of whether s_HoldoffDelta is zero or not.
			sysIpcSleep(100);
		}
		else
			// Wait a bit before checking again, but not as long if holdoff is zero.
			sysIpcSleep(s_HoldoffDelta ? 100 : 10);
	}
}
#endif

#endif

#if !__WIN32PC
bool fiCachedDevice::PrefetchCommon(u32 firstBlock,u32 lastBlock) const {
	// Lock the cache header since we're going to manipulate it.
	s_Header_CritSec.Lock();

	// Scan the entire cache in LRU order, looking for cached blocks.  The streamer
	// will have already called GetLayer on this request and that already puts the
	// cached blocks at the top of the LRU list, so don't repeat the work here.
	u32 hits = 0, pending = 0;
	u32 totalBlocks = lastBlock-firstBlock+1;

	for (u32 curBlock=firstBlock; curBlock<=lastBlock; curBlock++) {
		int cacheSlot = s_ResidentMap[m_SelfIndex][curBlock];
		if (cacheSlot) {
			if (s_Slots[cacheSlot].state == STATE_RESIDENT)
				++hits;
			else {
				Assert(s_Slots[cacheSlot].state & STATE_PENDING_EITHER);
				++pending;
			}
		}
	}

	bool result = false;

	if (hits < totalBlocks) {
		dcDebugf3("One or more blocks not resident in cache.");
		if (hits + pending < totalBlocks) {
			u32 missed = 0;
			// We don't need to flush the cache header here because the cache is still in a consistent state.
			// If the machine is turned off right now, the previous contents are still valid.  Note that
			// the worker thread will flush the cache header when it starts the copy.
			for (u32 curBlock=firstBlock; curBlock<=lastBlock; curBlock++) {
				if (!s_ResidentMap[m_SelfIndex][curBlock]) {
					// Remember our slot index
					u32 newSlot = s_Oldest;
					// Make sure by some crazy bad luck it's not the block we're currently copying.
					// Note that the header critsec is still locked here, and the only other place we
					// set it to nonzero also has the critsec locked.  Also make sure that either we
					// are at boot priority, or we don't have more than 8% of the cache already pending.
					if (newSlot != fiDiskCache::sm_Copying && (s_PendingState == STATE_PENDING_BOOT || s_NumInProgressBlocks < (CACHE_BLOCK_MAX>>4))) {
						// We are no longer the oldest entry in the cache.
						s_Slots[s_Oldest = s_Slots[s_Oldest].newer].older = 0;
						// But we are the newest entry in the cache.
						MakeNewest(newSlot);
						Slot &cur = s_Slots[newSlot];
						// If the slot was actually in use, let it know that it's no longer resident.
						if (cur.state < STATE_PENDING_EITHER) {
							if (cur.state == STATE_RESIDENT)
								--s_NumValidBlocks;
							Assert(s_ResidentMap[cur.srcFile][cur.srcBlock] == newSlot);
							s_ResidentMap[cur.srcFile][cur.srcBlock] = 0;
						}
						++s_NumInProgressBlocks;
						// Update the slot with the new parameters (priority depends on whether we left boot mode or not)
						FieldAssign(cur.state,s_PendingState);
						FieldAssign(cur.srcFile,m_SelfIndex);
						FieldAssign(cur.srcBlock,curBlock);
						Assign(s_ResidentMap[m_SelfIndex][curBlock],newSlot);
						dcDebugf2("Assigning src block %x to slot %u (oldest is now %u)",curBlock,newSlot,s_Oldest);
					}
					++missed;
				}
			}

			Assert(hits + pending + missed == totalBlocks);
		}
	}
	else
		result = true;

	// Note that we don't access the cache header at any point from here onward and no longer need the header critsec.
	s_Header_CritSec.Unlock();
	return result;
}
#endif	// !__WIN32PC

bool fiCachedDevice::Prefetch(u64 offset,int bufferSize) const {
#if __WIN32PC
	return !(offset && bufferSize);		// bogus result that validates the parameters
#else
	u32 firstBlock = u32(offset >> CACHE_BLOCK_SHIFT);
	u32 lastBlock = u32((offset + bufferSize - 1) >> CACHE_BLOCK_SHIFT);

	Assert(m_InnerDevice);
	Assert(fiIsValidHandle(m_InnerHandle));

	return PrefetchCommon(firstBlock,lastBlock);
#endif
}


void fiCachedDevice::InvalidateCacheBlock(u32 errorBlock) const {
	int cacheSlot = s_ResidentMap[m_SelfIndex][errorBlock];
	s_ResidentMap[m_SelfIndex][errorBlock] = 0;
	dcErrorf("Corruption detected in cache slot %d, invalidating",cacheSlot);
	s_Header_CritSec.Lock();
	s_Magic = ~CACHE_FILE_MAGIC;			// invalidate the entire cache for next time
	s_Slots[cacheSlot].state = STATE_INVALID;
	--s_NumValidBlocks;
	s_Header_CritSec.Unlock();
	FlushCacheHeader();
}

extern __THREAD bool tls_Uncached;		// HACK HACK HACK

int fiCachedDevice::ReadBulk(fiHandle /*handle*/,u64 offset,void *outBuffer,int bufferSize) const {
#if __WIN32PC
	return m_InnerDevice->ReadBulk(m_InnerHandle,offset,outBuffer,bufferSize);
#else
	if (bufferSize <= 0) {
		dcErrorf("fiCachedDevice::ReadBulk got read request of %d bytes, ignoring.",bufferSize);
		return 0;
	}

	if (!fiIsValidHandle(s_CacheFile)) {
		dcWarningf("Cached file access after cache was shut down.");
		return m_InnerDevice->ReadBulk(m_InnerHandle,offset,outBuffer,bufferSize);
	}
	else if (tls_Uncached)
		return m_InnerDevice->ReadBulk(m_InnerHandle,offset,outBuffer,bufferSize);

	OUTPUT_ONLY(sysTimer T);

	u32 firstBlock = u32(offset >> CACHE_BLOCK_SHIFT);
	u32 lastBlock = u32((offset + bufferSize - 1) >> CACHE_BLOCK_SHIFT);

	dcDebugf3("device %p handle %" HANDLEFMT " firstBlock=%x lastBlock=%x to %p, %d bytes.",m_InnerDevice,m_InnerHandle,firstBlock,lastBlock,outBuffer,bufferSize);

	Assert(m_InnerDevice);
	Assert(fiIsValidHandle(m_InnerHandle));

	if (!PrefetchCommon(firstBlock,lastBlock)) {
		// Fall back to the inner device once we've scheduled the copies.
		return m_InnerDevice->ReadBulk(m_InnerHandle,offset,outBuffer,bufferSize);
	}

	int result;
	u64 origOffset = offset;
	void *origOutBuffer = outBuffer;
	int origBufferSize = bufferSize;

	// By now, everything is in the cache.  Service the requests.
	if (firstBlock == lastBlock) {
		dcDebugf2("Single block read from slot %u (offset %u)",s_ResidentMap[m_SelfIndex][firstBlock],u32(offset & CACHE_BLOCK_MASK));
		result = CACHEDEV.ReadBulk(CACHE_BLOCK_HANDLE(s_ResidentMap[m_SelfIndex][firstBlock]),CACHE_BLOCK_ADDR(s_ResidentMap[m_SelfIndex][firstBlock]) + (offset & CACHE_BLOCK_MASK),outBuffer,bufferSize);
		if (result < 0) {
			InvalidateCacheBlock(firstBlock);
			return m_InnerDevice->ReadBulk(m_InnerHandle,origOffset,origOutBuffer,origBufferSize);
		}
	}
	else {
		// do firstBlock
		dcDebugf2("Multi block read, first slot is %u",s_ResidentMap[m_SelfIndex][firstBlock]);
		int firstLen = CACHE_BLOCK_SIZE - ((u32)offset & CACHE_BLOCK_MASK);
		int totalRead = CACHEDEV.ReadBulk(CACHE_BLOCK_HANDLE(s_ResidentMap[m_SelfIndex][firstBlock]),CACHE_BLOCK_ADDR(s_ResidentMap[m_SelfIndex][firstBlock]) + (offset & CACHE_BLOCK_MASK),outBuffer,firstLen);
		offset += firstLen;
		outBuffer = (char*)outBuffer + firstLen;
		bufferSize -= firstLen;
		if (totalRead <= 0) {
			InvalidateCacheBlock(firstBlock);
			return m_InnerDevice->ReadBulk(m_InnerHandle,origOffset,origOutBuffer,origBufferSize);
		}
		// do middle, if any
		for (u32 middle=firstBlock+1; middle<lastBlock; middle++) {
			dcDebugf2("Multi block read, a middle slot is %u",s_ResidentMap[m_SelfIndex][middle]);
			int thisLen = CACHEDEV.ReadBulk(CACHE_BLOCK_HANDLE(s_ResidentMap[m_SelfIndex][middle]),CACHE_BLOCK_ADDR(s_ResidentMap[m_SelfIndex][middle]),outBuffer,CACHE_BLOCK_SIZE);
			if (thisLen < 0) {
				InvalidateCacheBlock(middle);
				return m_InnerDevice->ReadBulk(m_InnerHandle,origOffset,origOutBuffer,origBufferSize);
			}
			else if (thisLen != CACHE_BLOCK_SIZE) {
				dcErrorf("[short read, middle block]");
				return totalRead + thisLen;
			}
			offset += CACHE_BLOCK_SIZE;
			outBuffer = (char*)outBuffer + CACHE_BLOCK_SIZE;
			totalRead += CACHE_BLOCK_SIZE;
			bufferSize -= CACHE_BLOCK_SIZE;
		}
		// do lastBlock
		dcDebugf2("Multi block read, last slot is %u",s_ResidentMap[m_SelfIndex][lastBlock]);
		int finalRead = CACHEDEV.ReadBulk(CACHE_BLOCK_HANDLE(s_ResidentMap[m_SelfIndex][lastBlock]),CACHE_BLOCK_ADDR(s_ResidentMap[m_SelfIndex][lastBlock]),outBuffer,bufferSize);
		if (finalRead < 0) {
			InvalidateCacheBlock(lastBlock);
			return m_InnerDevice->ReadBulk(m_InnerHandle,origOffset,origOutBuffer,origBufferSize);
		}
		else
			result = totalRead + finalRead;
	}
	dcDebugf2("ReadBulk of %d bytes completed in %fms, %fK/sec",result,T.GetMsTime(),((result+1023)>>10)/T.GetTime());
	return result;
#endif
}


u32 fiCachedDevice::GetBlockPhysicalSortKey(u64 offset,int bufferSize) const
{
	u32 stateAccum = STATE_RESIDENT;
	CompileTimeAssert(STATE_RESIDENT == 0);

	if (fiIsValidHandle(s_CacheFile)) {
		SYS_CS_SYNC(s_Header_CritSec);

		u32 firstBlock = u32(offset >> CACHE_BLOCK_SHIFT);
		u32 lastBlock = u32((offset + bufferSize - 1) >> CACHE_BLOCK_SHIFT);

		Assert(m_InnerDevice);
		Assert(fiIsValidHandle(m_InnerHandle));

		// Scan all blocks in the request and determine if they're resident, pending, or unknown.
		for (u32 curBlock=firstBlock; curBlock<=lastBlock; curBlock++) {
			int curSlot = s_ResidentMap[m_SelfIndex][curBlock];
			if (curSlot) {
				// If we already know about it, mark it newest.
				if (s_Slots[curSlot].newer)
					MakeNewest(curSlot);
				// Track whether all blocks are resident or not.
				stateAccum |= s_Slots[curSlot].state;
			}
			else {		// One of the blocks is not resident, bail immediately.
				stateAccum = STATE_INVALID;
				break;
			}
		}
	}
	else
		stateAccum = STATE_INVALID;

	// If accumulated 
	return stateAccum == STATE_RESIDENT? HARDDRIVE_LSN : (m_SortKey + u32(offset >> 11));
}

int fiCachedDevice::Write(fiHandle /*handle*/,const void * /*buffer*/,int /*bufferSize*/) const {
	AssertMsg(false , "Cannot Write to cached device");
	return 0;
}

int fiCachedDevice::Seek(fiHandle /*handle*/,int /*offset*/,fiSeekWhence /*whence*/) const {
	AssertMsg(false , "Cannot Seek on cached device");
	return 0;
}

u64 fiCachedDevice::Seek64(fiHandle /*handle*/,s64 /*offset*/,fiSeekWhence /*whence*/) const {
	AssertMsg(false , "Cannot64 Seek on cached device");
	return 0;
}

int fiCachedDevice::Close(fiHandle /*handle*/) const {
	AssertMsg(false , "Cannot Close on cached device");
	return -1;
}

int fiCachedDevice::CloseBulk(fiHandle /*handle*/) const {
	if (fiIsValidHandle(m_InnerHandle)) {
		m_InnerDevice->CloseBulk(m_InnerHandle);
		m_InnerHandle = fiHandleInvalid;
		return 0;
	}
	else
		return -1;
}

u64 fiCachedDevice::GetFileSize(const char *filename) const {
	return m_InnerDevice->GetFileSize(filename);
}

u64 fiCachedDevice::GetFileTime(const char *filename) const {
	return m_InnerDevice->GetFileTime(filename);
}

bool fiCachedDevice::SetFileTime(const char * /*filename*/,u64 /*timestamp*/) const {
	AssertMsg(false , "Cannot SetFileTime on cached device");
	return false;
}

const char *fiCachedDevice::GetDebugName() const {
	return m_InnerDevice->GetDebugName();
}


}	// namespace rage
