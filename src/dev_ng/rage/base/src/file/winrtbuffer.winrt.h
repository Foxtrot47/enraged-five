// 
// file/winrtbuffer.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

//////////////////////////////////////////////////////////////////////////
// This header provides different IBuffer wrappers needed for
// much of the saving/loading on Durango.
//////////////////////////////////////////////////////////////////////////

#ifndef FILE_WINRTBUFFER_H
#define FILE_WINRTBUFFER_H

#if RSG_DURANGO

namespace rage
{
namespace WinRTBuffer
{
// 
// // PURPOSE: Retrieves the native buffer of an IBuffer.
// // PARAMS:	buffer - the IBuffer to retrieve the native buffer for.
// // NOTES:	The Buffer must not be resized whilst the native buffer is in use or it could be invalidated.
// //			nullptr is returned if the native buffer could not be retrieved.
// void* GetNativeBuffer(Windows::Storage::Streams::IBuffer^ buffer);

// PURPOSE:	Reads the contents of an IBuffer to a native buffer.
// PARAMS:	buffer - the buffer to be read.
//			dest - the destination to copy to.
//			size - the number of bytes to read.
// RETURNS:	true on success.
// NOTES:	size is limited to 32 bits, this is a WinRT restriction.
bool ReadBuffer(Windows::Storage::Streams::IBuffer^ buffer, void* dest, u32 size);

template<typename T>
bool ReadBuffer(Windows::Storage::Streams::IBuffer^ buffer, T* dest)
{
	return ReadBuffer(buffer, static_cast<void*>(dest), static_cast<u32>(sizeof(T)));
}

Windows::Storage::Streams::IBuffer^ WriteBuffer(const void* data, u32 size);

template<typename T>
Windows::Storage::Streams::IBuffer^ WriteBuffer(const T& data)
{
	return WriteBuffer(static_cast<const void*>(&data), static_cast<u32>(sizeof(T)));
}

}

} // namespace rage

#endif // RSG_DURANGO

#endif // FILE_WINRTBUFFER_H