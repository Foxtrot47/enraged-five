//
// file/winsock.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "file/file_config.h"
#include "diag/errorcodes.h"

#if __LIVE
#if __XENON
#include "system/xtl.h"
#include <winsockx.h>

#if !__OPTIMIZED
#pragma comment(lib, "xonlined.lib")
#else
#pragma comment(lib, "xonline.lib")
#endif //__OPTIMIZED
#elif __GFWL
#define _WINSOCKAPI_
#include "system/xtl.h"
#include <winsock2.h>
#include <winxnet.h>
#pragma comment(lib, "xlive.lib")
#endif // __XENON
#elif __WIN32PC

#pragma comment(lib, "Ws2_32.lib")

#include "diag\diagerrorcodes.h"

#ifndef __UNITYBUILD
#define STRICT
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(error: 4668)
#endif

#elif RSG_DURANGO

#define STRICT
#pragma warning(disable: 4668)
#include <winapifamily.h>
#include <Windows.h>
#include <WinSock2.h>
#pragma warning(error: 4668)
#pragma comment(lib, "Ws2_32.lib")

#elif __PPU

#include "netex/net.h"
#include "netex/libnetctl.h"
#include "data/marker.h"

#pragma comment(lib,"netctl_stub")
#pragma comment(lib,"net_stub")

#elif __PSP2

#include <net.h>
#include <../include/net/net.h>
#include <libnetctl.h>
#include <libsysmodule.h>
#pragma comment(lib,"Scesysmodule_stub")

#elif RSG_ORBIS

#include <net.h>
#include <libnetctl.h>
#include <libnetctl_error.h>

#endif  //__LIVE

#include "string/string.h"
#include "system/param.h"

using namespace rage;

static int s_RefCount;

#if __XENON
namespace rage
{
NOSTRIP_PARAM(xboxSecureNetworking, "If present use xbox secure networking ***RFS WON'T WORK***");
}
#endif

#if __PPU
namespace rage
{

//Helper function used to display cell network error strings.
const char* netGetCellNetErrorString(const int code)
{
    switch(code)
    {
        case CELL_NET_CTL_ERROR_NOT_INITIALIZED:    return "CELL_NET_CTL_ERROR_NOT_INITIALIZED";
        case CELL_NET_CTL_ERROR_NOT_TERMINATED:     return "CELL_NET_CTL_ERROR_NOT_TERMINATED";
        case CELL_NET_CTL_ERROR_HANDLER_MAX:        return "CELL_NET_CTL_ERROR_HANDLER_MAX";
        case CELL_NET_CTL_ERROR_ID_NOT_FOUND:       return "CELL_NET_CTL_ERROR_ID_NOT_FOUND";
        case CELL_NET_CTL_ERROR_INVALID_ID:         return "CELL_NET_CTL_ERROR_INVALID_ID";
        case CELL_NET_CTL_ERROR_INVALID_CODE:       return "CELL_NET_CTL_ERROR_INVALID_CODE";
        case CELL_NET_CTL_ERROR_INVALID_ADDR:       return "CELL_NET_CTL_ERROR_INVALID_ADDR";
        case CELL_NET_CTL_ERROR_NOT_CONNECTED:      return "CELL_NET_CTL_ERROR_NOT_CONNECTED";
        case CELL_NET_CTL_ERROR_NOT_AVAIL:          return "CELL_NET_CTL_ERROR_NOT_AVAIL";
        case CELL_NET_CTL_ERROR_INVALID_TYPE:       return "CELL_NET_CTL_ERROR_INVALID_TYPE";
        case CELL_NET_CTL_ERROR_INVALID_SIZE:       return "CELL_NET_CTL_ERROR_INVALID_SIZE";
//        case CELL_NET_CTL_ERROR_INVALID_MEMORY_CONTAINER:       return "CELL_NET_CTL_ERROR_INVALID_MEMORY_CONTAINER";
//        case CELL_NET_CTL_ERROR_INSUFFICIENT_MEMORY_CONTAINER:  return "CELL_NET_CTL_ERROR_INSUFFICIENT_MEMORY_CONTAINER";
        case CELL_NET_CTL_ERROR_NET_DISABLED:       return "CELL_NET_CTL_ERROR_NET_DISABLED";
        case CELL_NET_CTL_ERROR_NET_NOT_CONNECTED:  return "CELL_NET_CTL_ERROR_NET_NOT_CONNECTED";
        case CELL_NET_CTL_ERROR_NP_NO_ACCOUNT:      return "CELL_NET_CTL_ERROR_NP_NO_ACCOUNT";
        case CELL_NET_CTL_ERROR_NP_RESERVED1:       return "CELL_NET_CTL_ERROR_NP_RESERVED1";
        case CELL_NET_CTL_ERROR_NP_RESERVED2:       return "CELL_NET_CTL_ERROR_NP_RESERVED2";
        case CELL_NET_CTL_ERROR_DIALOG_CANCELED:    return "CELL_NET_CTL_ERROR_DIALOG_CANCELED";
        default:                                    return "CELL_NET_CTL_ERROR_UNKNOWN";
    }
}

}
#elif RSG_ORBIS
namespace rage
{

//Helper function used to display sce network error strings.
const char* netGetSceNetErrorString(const int code)
{
    switch(code)
    {
		case SCE_NET_CTL_ERROR_NOT_INITIALIZED:	 return "SCE_NET_CTL_ERROR_NOT_INITIALIZED";
		case SCE_NET_CTL_ERROR_CALLBACK_MAX:	 return "SCE_NET_CTL_ERROR_CALLBACK_MAX";
		case SCE_NET_CTL_ERROR_ID_NOT_FOUND:	 return "SCE_NET_CTL_ERROR_ID_NOT_FOUND";
		case SCE_NET_CTL_ERROR_INVALID_ID:		 return "SCE_NET_CTL_ERROR_INVALID_ID";
		case SCE_NET_CTL_ERROR_INVALID_CODE:	 return "SCE_NET_CTL_ERROR_INVALID_CODE";
		case SCE_NET_CTL_ERROR_INVALID_ADDR:	 return "SCE_NET_CTL_ERROR_INVALID_ADDR";
		case SCE_NET_CTL_ERROR_NOT_CONNECTED:	 return "SCE_NET_CTL_ERROR_NOT_CONNECTED";
		case SCE_NET_CTL_ERROR_NOT_AVAIL:		 return "SCE_NET_CTL_ERROR_NOT_AVAIL";
		case SCE_NET_CTL_ERROR_NETWORK_DISABLED: return "SCE_NET_CTL_ERROR_NETWORK_DISABLED";
		case SCE_NET_CTL_ERROR_DISCONNECT_REQ:	 return "SCE_NET_CTL_ERROR_DISCONNECT_REQ";
		case SCE_NET_CTL_ERROR_INVALID_TYPE:	 return "SCE_NET_CTL_ERROR_INVALID_TYPE";
		case SCE_NET_CTL_ERROR_INVALID_SIZE:	 return "SCE_NET_CTL_ERROR_INVALID_SIZE";
		case SCE_NET_CTL_ERROR_ETHERNET_PLUGOUT: return "SCE_NET_CTL_ERROR_ETHERNET_PLUGOUT";
		case SCE_NET_CTL_ERROR_WIFI_DEAUTHED:	 return "SCE_NET_CTL_ERROR_WIFI_DEAUTHED";
		case SCE_NET_CTL_ERROR_WIFI_BEACON_LOST: return "SCE_NET_CTL_ERROR_WIFI_BEACON_LOST";
        default:                                 return "SCE_NET_CTL_ERROR_UNKNOWN";
    }
}

}
#endif

#if __PPU
static char __libnet_memory[128 * 1024];
#endif

void InitWinSock(int XENON_ONLY(argc), char** XENON_ONLY(argv)) {
    if (s_RefCount++ == 0)
    {
#if __XENON
        for(int i = 1; i < argc; ++i)
        {
            if('-' == argv[i][0]
                && !strnicmp(&argv[i][1], PARAM_xboxSecureNetworking.GetName(), strlen(PARAM_xboxSecureNetworking.GetName())))
            {
                PARAM_xboxSecureNetworking.Set("");
                break;
            }
        }
#endif

#if __XENON || __WIN32PC

        //Clear out prior config possibly set by external libs like Deja.
#if __LIVE
        XWSACleanup();
        XOnlineCleanup();
        XNetCleanup();
#else
#if !defined(__RGSC_DLL) || !__RGSC_DLL
		// don't call WSACleanup() in the socialclub.dll prior to initializing winsock
		// since other tools such as rag will have already established connections and
		// calling this will shutdown all sockets in the process.
        WSACleanup();
#endif
#endif

#if __LIVE
	    XNetStartupParams xnsp;
	    memset(&xnsp,0,sizeof(xnsp));
	    xnsp.cfgSizeOfStruct=sizeof(xnsp);
#if __XENON
        if(!PARAM_xboxSecureNetworking.Get())
        {
	        xnsp.cfgFlags=XNET_STARTUP_BYPASS_SECURITY;
        }
#endif
#if !__FINAL && __XENON
	    // Workarounds for XDK socket lockup problem.
	    xnsp.cfgSockDefaultRecvBufsizeInK = 64;
	    xnsp.cfgSockDefaultSendBufsizeInK = 64;
	    xnsp.cfgFlags|=XNET_STARTUP_ALLOCATE_MAX_DGRAM_SOCKETS|XNET_STARTUP_ALLOCATE_MAX_STREAM_SOCKETS;
#endif
        xnsp.cfgKeyRegMax = 64;
        xnsp.cfgSecRegMax = 64;
        if(0 != XNetStartup(&xnsp))
        {
            Quitf("XNetStartup failed");
        }

        XNetStartupParams checkXnsp = {0};
        DWORD sizeofXnsp = sizeof(checkXnsp);
	    checkXnsp.cfgSizeOfStruct=sizeof(checkXnsp);
        if(0 != XNetGetOpt(XNET_OPTID_STARTUP_PARAMS,
                (BYTE*) &checkXnsp,
                &sizeofXnsp))
        {
            Quitf("Failed to check XNetStartupParams.");
        }
        else if(checkXnsp.cfgKeyRegMax != xnsp.cfgKeyRegMax)
        {
            Quitf("Failed to configure cfgKeyRegMax.");
        }
        else if(checkXnsp.cfgSecRegMax != xnsp.cfgSecRegMax)
        {
            Quitf("Failed to configure cfgSecRegMax.");
        }
#endif  //__LIVE

	    WORD wVersionRequested=MAKEWORD(2,0);
 	    WSADATA wsaData;

#if __LIVE
		int ret = XWSAStartup(wVersionRequested,&wsaData);
#else
	    int ret = WSAStartup(wVersionRequested,&wsaData);
#endif
		if (ret != 0)
		{
#if RSG_PC
			diagErrorCodes::SetExtraReturnCodeNumber(ret, false);
#endif // RSG_PC
			Quitf(ERR_NET_WINSOCK_1,"InitWinSock - Couldn't find a usable WinSock DLL");
		}

	    if (LOBYTE(wsaData.wVersion)!=2 || HIBYTE(wsaData.wVersion)!=0) {
#if __LIVE
			XWSACleanup();
#else
			WSACleanup();
#endif
			Quitf(ERR_NET_WINSOCK_2,"InitWinSock - Couldn't find a usable WinSock DLL (version=%x)",wsaData.wVersion);
	    }
#if __XENON && !__FINAL
	    // Leak one socket to avoid DMA hang problem.
	    socket(AF_INET,SOCK_STREAM,0);
#endif

#elif __PPU
        OUTPUT_ONLY(u32 errCode);

		sys_net_initialize_parameter_t __libnet_param;
		__libnet_param.memory = __libnet_memory;
		__libnet_param.memory_size = sizeof(__libnet_memory);
		__libnet_param.flags = 0;

		if(0 != sys_net_initialize_network_ex(&__libnet_param))
	    {
		    Quitf("Error calling sys_net_initialize_network()");
	    }
        else if (0 != (OUTPUT_ONLY(errCode =) cellNetCtlInit()))
        {
#if RAGE_USE_DEJA
    		if (errCode != CELL_NET_CTL_ERROR_NOT_TERMINATED)		// Deja might have already done this
#endif
			    Quitf("Error calling cellNetCtlInit(): %s", netGetCellNetErrorString(errCode));
        }

		sys_net_show_ifconfig();
#elif __PSP2
		if (sceSysmoduleLoadModule(SCE_SYSMODULE_NET))
			Quitf("Unable to load net module");

		static char netBuf[32768];
		SceNetInitParam p = {netBuf, sizeof(netBuf), 0 };
		if (sceNetInit(&p) || sceNetCtlInit())
			Quitf("Error calling sceNetInit");
#elif RSG_ORBIS
	if (sceNetInit() < 0 || sceNetCtlInit() < 0)
		Quitf("Error calling sceNetInit");
#elif RSG_DURANGO
	    WORD wVersionRequested=MAKEWORD(2,0);
 	    WSADATA wsaData;
	    if (WSAStartup(wVersionRequested,&wsaData) != 0)
			Quitf("Couldn't start networking");
#endif  //__XENON || __WIN32PC
    }
}

void ShutdownWinSock() {
    if (--s_RefCount == 0)
    {
#if __XENON || __WIN32PC

#if __LIVE

		XWSACleanup();
        XNetCleanup();

#elif __WIN32PC

    	WSACleanup();

#endif  //__LIVE

#elif __PPU
        if(0 > sys_net_free_thread_context(0, SYS_NET_THREAD_ALL))
        {
	        Errorf("Error calling sys_net_free_thread_context()");
        }

        cellNetCtlTerm();
        sys_net_finalize_network();

#elif __PSP2

		sceNetCtlTerm();
		sceNetTerm();

#elif RSG_ORBIS

		sceNetCtlTerm();
		sceNetTerm();

#endif  //__XENON || __WIN32PC
    }
	AssertMsg(s_RefCount >= 0,"Too many ShutdownWinSock calls");
}


#if __WIN32 || (defined(__UNIX) && __UNIX)

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4702)
#endif

int safe_send(int s,const char *buf,int len) {
	int done = 0;
	while (done < len) {
#if __GFWL
		int amt = XSocketSend(s,buf + done,len - done,0);
#else
		int amt = send(s,buf + done,len - done,0);
#endif
		if (amt < 0) {
#if __GFWL
			if (XWSAGetLastError() != WSAEWOULDBLOCK) {
				Quitf("safe_send - error on send, code %d",XWSAGetLastError());
#else
			int lastError = WSAGetLastError();
			if (lastError != WSAEWOULDBLOCK) {
#if RSG_PC
				diagErrorCodes::SetExtraReturnCodeNumber(lastError, false);
#endif // RSG_PC
				Quitf(ERR_NET_WINSOCK_3,"safe_send - error on send, code %d",WSAGetLastError());
#endif
				return -1;
			}
		}
		else
			done += amt;
	}
	return done;
}

int safe_recv(int s,char *buf,int len) {
	int done = 0;
	while (done < len) {
#if __GFWL
		int amt = XSocketRecv(s,buf + done,len - done,0);
#else
		int amt = recv(s,buf + done,len - done,0);
#endif
		if (amt < 0) {
#if __GFWL
			if (XWSAGetLastError() != WSAEWOULDBLOCK) {
				Quitf("safe_recv - error on recv, code %d",XWSAGetLastError());
#else
			int lastError = WSAGetLastError();
			if (lastError != WSAEWOULDBLOCK) {
#if RSG_PC
				diagErrorCodes::SetExtraReturnCodeNumber(lastError, false);
#endif // RSG_PC
				Quitf(ERR_NET_WINSOCK_4,"safe_recv - error on recv, code %d",WSAGetLastError());
#endif
				return -1;
			}
		}
		else
			done += amt;
	}
	return done;
}

#if __WIN32
#pragma warning(pop)
#endif


#endif
