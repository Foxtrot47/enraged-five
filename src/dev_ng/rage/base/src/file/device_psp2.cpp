// 
// file/device_psn.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_ORBIS

#include <rtc.h>
#include "device.h"
#include "system/ipc.h"
#include "system/param.h"
#include "system/bootmgr.h"
#include <sceerror.h>

#include "string/string.h"
#include "string/stringhash.h"

#include <kernel.h>

#pragma comment(lib, "libSceRtc_stub_weak.a")

using namespace rage;

#if RSG_ORBIS
#define SCE_STM_RWU		SCE_KERNEL_S_IRWU
#else
#define SCE_STM_RWU		0600
#endif

namespace rage
{
	bool g_IsExiting = false;
	extern char *XEX_TITLE_ID;
};

const int PREFIX_SIZE = 0;

const char *fiDeviceLocal::FixName(char *outDest,int size,const char *src) {
	if (!strncmp(src, "/savedata", 9)) {
		safecpy(outDest,src,size);
	}
	else if (!strncmp(src, "/addcont", 8)) {
		safecpy(outDest,src,size);
	}
	else if (!strncmp(src,"/app0/",6)) {
		safecpy(outDest,src,size);
	}
	else if (!strncmp(src,"/data/",6)) {
		safecpy(outDest,src,size);
	}
	else if (!strncmp(src,"/usb",4)) {
		safecpy(outDest,src,size);
	}
	else if (!strncmp(src, "/download", 9)) {
		safecpy(outDest,src,size);
	}
#if RSG_ORBIS
	else if (!strncmp(src,"/av_contents/",13)) {
		safecpy(outDest,src,size);
	}
	else if (!strncmp(src,"/temp0/",7)) {
		safecpy(outDest,src,size);
	}
#endif
	else if (strncmp(src,"/host/",6)) {
		strcpy(outDest,"/host/");
		safecat(outDest,src,size);
	}
	else
		safecpy(outDest,src,size);

	if (strncmp(src,"/host/",6))
	{
		while (strchr(outDest,'\\'))
			*strchr(outDest,'\\') = '/';
	}

	return outDest;
}

static fiHandle CheckError(int fd) {
	return fd>=0? (fiHandle)fd : fiHandleInvalid;
}

fiHandle fiDeviceLocal::Open(const char *filename,bool readOnly) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	fiHandle h = CheckError(sceKernelOpen(namebuf, readOnly? SCE_KERNEL_O_RDONLY : SCE_KERNEL_O_RDWR, 0));
#if __DEVPROF
	if (h != fiHandleInvalid)
		sm_DeviceProfiler->OnOpen(h, filename);
#endif // __DEVPROF
	return h;
}

fiHandle fiDeviceLocal::OpenBulk(const char *filename,u64 &outBias) const {
	outBias = 0;
	return Open(filename,true);
}

fiHandle fiDeviceLocalDrm::OpenBulkDrm(const char *filename,u64 &outBias, const void *) const {
	return OpenBulk(filename,outBias);
}

fiHandle fiDeviceLocal::CreateBulk(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	fiHandle h = CheckError(sceKernelOpen(namebuf, SCE_KERNEL_O_RDWR | SCE_KERNEL_O_CREAT | SCE_KERNEL_O_TRUNC, SCE_STM_RWU));
#if __DEVPROF
	if (h != fiHandleInvalid)
		sm_DeviceProfiler->OnOpen(h, filename);
#endif // __DEVPROF
	return h;
}

fiHandle fiDeviceLocal::Create(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	fiHandle h = CheckError(sceKernelOpen(namebuf, SCE_KERNEL_O_RDWR | SCE_KERNEL_O_CREAT | SCE_KERNEL_O_TRUNC, SCE_STM_RWU));
#if __DEVPROF
	if (h != fiHandleInvalid)
		sm_DeviceProfiler->OnOpen(h, filename);
#endif // __DEVPROF
	return h;
}

int fiDeviceLocal::Seek(fiHandle handle,int offset,fiSeekWhence whence) const {
#if __DEVPROF
	sm_DeviceProfiler->OnSeek(handle, offset);
#endif // __DEVPROF
	return sceKernelLseek((int)handle,offset,whence);
}

u64 fiDeviceLocal::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const {
#if __DEVPROF
	sm_DeviceProfiler->OnSeek(handle, offset);
#endif // __DEVPROF
	return sceKernelLseek((int)handle, offset, whence);
}

int fiDeviceLocal::Write(fiHandle handle,const void *buffer,int count) const {
#if __DEVPROF
	sm_DeviceProfiler->OnWrite(handle, count);
#endif // __DEVPROF
	return sceKernelWrite((int)handle,buffer,count);
}

void (*fiDevice::sm_FatalReadError)(void);
#if !__FINAL
u32 fiDevice::sm_InjectReadError;
#endif

int fiDeviceLocal::Read(fiHandle handle,void *buffer,int count) const {
#if __DEVPROF
	sm_DeviceProfiler->OnRead(handle, count, 0);
#endif // __DEVPROF
	int r = sceKernelRead((int)handle,buffer,count);
#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(handle);
#endif // __DEVPROF
	return r;
}

int fiDeviceLocal::ReadBulk(fiHandle handle,u64 offset,void *buffer,int count) const {
#if __DEVPROF
	sm_DeviceProfiler->OnRead(handle, count, offset);
#endif // __DEVPROF
	int r = sceKernelPread((int)handle,buffer,count,offset);
#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(handle);
#endif // __DEVPROF
	return r;
}

int fiDeviceLocal::WriteBulk(fiHandle handle,u64 offset,const void *buffer,int count) const {
#if __DEVPROF
	sm_DeviceProfiler->OnWrite(handle, count);
#endif // __DEVPROF
	return sceKernelPwrite((int)handle,buffer,count,offset);
}

int fiDeviceLocal::Close(fiHandle handle) const {
#if __DEVPROF
	sm_DeviceProfiler->OnClose(handle);
#endif // __DEVPROF
	return sceKernelClose((int)handle);
}

int fiDeviceLocal::CloseBulk(fiHandle handle) const {
#if __DEVPROF
	sm_DeviceProfiler->OnClose(handle);
#endif // __DEVPROF
	return sceKernelClose((int)handle);
}

int fiDeviceLocal::Size(fiHandle handle) const
{
	return (int)Size64(handle);
}

u64 fiDeviceLocal::Size64(fiHandle handle) const
{
	SceKernelStat stat;
	if( sceKernelFstat(handle, &stat) == 0 )
		return stat.st_size;

	return 0;
}

u64 fiDeviceLocal::GetFileSize(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	SceKernelStat stat;
	if (sceKernelStat(namebuf,&stat)==0)
		return stat.st_size;
	else
		return 0;
}

// static u64 ConvertTime(const SceDateTime & /*dt*/) {
// 	return 12345;
// }

// SYSTEMTIME _1970 = { 1970, 1, 0, 1, 0, 0, 0, 0 };
// SystemTimeToFileTime(&_1970, (FILETIME*)&ft);
// char buffer[100];
// _ui64toa(ft,buffer,10);
// printf("filetime of 1970 %s\n",buffer);

static const u64 FILETIME_BIAS = 116444736000000000ULL;
static const u64 CENTINANOSECONDS_PER_SECOND = 10000000ULL;

u64 fiDeviceLocal::GetFileTime(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	SceKernelStat stat;
	if (sceKernelStat(namebuf,&stat)==0)
		return (stat.st_mtime * CENTINANOSECONDS_PER_SECOND) + FILETIME_BIAS;
	else
		return 0;
}

bool fiDeviceLocal::SetFileTime(const char * filename,u64 /*timestamp*/) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	SceKernelStat stat;
	memset(&stat,0,sizeof(stat));
	// return sceKernelUtimes(namebuf,&stat,SCE_CST_MT)==0;
	return false;
}

bool fiDeviceLocal::Delete(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	return sceKernelUnlink(namebuf) == 0;
}

bool fiDeviceLocal::Rename(const char *from,const char *to) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	char namebuf2[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),from);
	FixName(namebuf2,sizeof(namebuf2),to);
	return sceKernelRename(namebuf,namebuf2) == 0;
}

u32 fiDeviceLocal::GetAttributes(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	SceKernelStat stat;
	u32 result = FILE_ATTRIBUTE_INVALID;
	if (sceKernelStat(namebuf,&stat)==0) {
		result = 0;
		if (stat.st_mode & SCE_KERNEL_S_IFDIR)
			result |= FILE_ATTRIBUTE_DIRECTORY;
		if (!(stat.st_mode & SCE_KERNEL_S_IWUSR))
			result |= FILE_ATTRIBUTE_READONLY;
	}
	return result;
}

bool fiDeviceLocal::SetAttributes(const char * /*filename*/,u32 /*attributes*/) const {
	return false;
}


bool fiDeviceLocal::MakeDirectory(const char *filename) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	return sceKernelMkdir(namebuf, SCE_STM_RWU) == 0;
}


bool fiDeviceLocal::UnmakeDirectory(const char *pathname) const {
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),pathname);
	return sceKernelRmdir(namebuf) == 0;
}

struct FFData {
	int fd, readOfs, readSiz;
	long bufSiz;
	char buf[0];
};

fiHandle fiDeviceLocal::FindFileBegin(const char *filename,fiFindData &outData) const
{
	char namebuf[RAGE_MAX_PATH+PREFIX_SIZE];
	FixName(namebuf,sizeof(namebuf),filename);
	int fd = sceKernelOpen(namebuf,SCE_KERNEL_O_RDONLY | SCE_KERNEL_O_DIRECTORY, SCE_STM_RWU);
	if (fd < 0)
		return fiHandleInvalid;
	SceKernelStat sb;
	if (sceKernelFstat(fd,&sb) < 0) {
		sceKernelClose(fd);
		return fiHandleInvalid;
	}
	FFData *ff = (FFData*) rage_new char[sizeof(FFData) + sb.st_blksize];
	ff->fd = fd;
	ff->bufSiz = sb.st_blksize;
	ff->readOfs = ff->readSiz = 0;
	if (!FindFileNext((fiHandle)ff,outData)) {
		FindFileEnd((fiHandle)fd);
		return fiHandleInvalid;
	}
	else
		return (fiHandle) ff;
}

bool fiDeviceLocal::FindFileNext(fiHandle handle,fiFindData &outData) const
{
	if (handle == fiHandleInvalid)
		return false;

	FFData *ff = (FFData*) handle;
	if (ff->readOfs >= ff->readSiz) {
		ff->readOfs = 0;
		ff->readSiz = sceKernelGetdents(ff->fd,ff->buf,ff->bufSiz);
		if (ff->readSiz == 0)
			return false;
	}
	SceKernelDirent *de = (SceKernelDirent*) (ff->buf + ff->readOfs);
	ff->readOfs += de->d_reclen;

	// copy filename
	safecpy(outData.m_Name, de->d_name);

	SceKernelStat stat;
	if(sceKernelFstat(ff->fd, &stat) == SCE_OK)
	{
		outData.m_Size = stat.st_size;		
		outData.m_LastWriteTime = stat.st_mtim.tv_sec; //time_t is 64-bit in PS4.
	}
	outData.m_Attributes = de->d_type == SCE_KERNEL_DT_DIR? FILE_ATTRIBUTE_DIRECTORY : 0;
	return true;
}

int fiDeviceLocal::FindFileEnd(fiHandle handle) const
{
	if (handle != fiHandleInvalid) {
		sceKernelClose(((FFData*)handle)->fd);
		delete[] (char*) handle;
	}
	return 0;
}


bool fiDeviceLocal::SetEndOfFile(fiHandle /*handle*/) const {
	AssertMsg(false,"SetEndOfFile not implemented.");
	return false;
}

fiDevice::RootDeviceId fiDeviceLocal::GetRootDeviceId(const char *name) const
{
	// Ideally we never get here - the child device should handle it.
	char namebuf[RAGE_MAX_PATH];
	FixName(namebuf,sizeof(namebuf),name);
	return strncmp(namebuf,"/app0/",6) == 0 ? OPTICAL : HARDDRIVE;
}


u32 fiDeviceLocal::GetPhysicalSortKey(const char* name) const
{
	char namebuf[RAGE_MAX_PATH];
	FixName(namebuf,sizeof(namebuf),name);
	if (!strncmp(namebuf,"/app0/",6)) {
		const int maxPhysical = 255;
		static u32 hashes[maxPhysical], lsns[maxPhysical], hashCount;
		static u32 nextLsn = 80000;		// approximate value, important mostly because it's nonzero.
		u32 namehash = atStringHash(namebuf);
		for (u32 i=0; i<maxPhysical; i++)
			if (namehash == hashes[i])
				return lsns[i];
		if (hashCount == maxPhysical) { // This shouldn't ever happen.
			Assertf(0, "Exceeded max capacity for cached hashes");
			Quitf("Exceeded max capacity for cached hashes");
		}
		u64 fs = GetFileSize(name);
		if (fs) {
			Displayf("Assigning emulated LSN %u to file '%s'",nextLsn,namebuf);
			hashes[hashCount] = namehash;
			lsns[hashCount] = nextLsn;
			nextLsn += ((fs + 32767) & ~32767) >> 11;		// Some higher level code gets cranky if files don't start on a 32k boundary, so simulate that.
			return lsns[hashCount++];
		}
	}
	return HARDDRIVE_LSN;
}

void fiDevice::ConvertFileTimeToSystemTime(u64 inTime,SystemTime &outTime) {
	SceRtcDateTime oDateTime = {0};
	int iErrorCode = sceRtcConvertTime_tToDateTime( (time_t)inTime, &oDateTime );
	// To convert to local time we need to convert from SceRtcDateTime to SceRtcTick, update the tick time, then convert back as
	// there doesn't appear to be a conversion in POSIX (inTime) format.
	if (Verifyf(iErrorCode == SCE_OK, "[RTC] sceRtcConvertTime_tToDateTime error code %x", iErrorCode)) {
		SceRtcTick oUtcTick = {0};
		iErrorCode = sceRtcGetTick(&oDateTime, &oUtcTick);
		if(Verifyf(iErrorCode == SCE_OK, "[RTC] sceRtcGetTick error code %x", iErrorCode)) {
			SceRtcTick oLocalTick = {0};
			iErrorCode = sceRtcConvertUtcToLocalTime(&oUtcTick, &oLocalTick);
			if(Verifyf(iErrorCode == SCE_OK, "[RTC] sceRtcConvertUtcToLocalTime error code %x", iErrorCode)) {
				iErrorCode = sceRtcSetTick(&oDateTime, &oLocalTick);
				if(Verifyf(iErrorCode == SCE_OK, "[RTC] sceRtcSetTick error code %x", iErrorCode)) {
					outTime.wYear = oDateTime.year;
					outTime.wMonth = oDateTime.month;
					outTime.wDay = oDateTime.day;
					outTime.wDayOfWeek = 0;
					outTime.wHour = oDateTime.hour;
					outTime.wMinute = oDateTime.minute;
					outTime.wSecond = oDateTime.second;
					outTime.wMilliseconds = (u16)(oDateTime.microsecond / 1000);
				}
			}
		}
	}
}

void fiDevice::GetLocalSystemTime(SystemTime & outTime)
{
	SceRtcDateTime oDateTime = {0};
	int iErrorCode = sceRtcGetCurrentClockLocalTime(&oDateTime);
	if (iErrorCode == SCE_OK) {
		outTime.wYear = oDateTime.year;
		outTime.wMonth = oDateTime.month;
		outTime.wDay = oDateTime.day;
		outTime.wDayOfWeek = 0;
		outTime.wHour = oDateTime.hour;
		outTime.wMinute = oDateTime.minute;
		outTime.wSecond = oDateTime.second;
		outTime.wMilliseconds = (u16)(oDateTime.microsecond / 1000);
	}
	else
	{
		Assertf(iErrorCode == SCE_OK, "[RTC] sceRtcGetCurrentClockLocalTime error code %x", iErrorCode);
	}
}

void fiDevice::GetSystemTimeUtc(SystemTime & outTime)
{
	SceRtcDateTime oDateTime = {0};
	int iErrorCode = sceRtcGetCurrentClockUtc(&oDateTime);
	if (iErrorCode == SCE_OK) {
		outTime.wYear = oDateTime.year;
		outTime.wMonth = oDateTime.month;
		outTime.wDay = oDateTime.day;
		outTime.wDayOfWeek = 0;
		outTime.wHour = oDateTime.hour;
		outTime.wMinute = oDateTime.minute;
		outTime.wSecond = oDateTime.second;
		outTime.wMilliseconds = (u16)(oDateTime.microsecond / 1000);
	}
	else
	{
		Assertf(iErrorCode == SCE_OK, "[RTC] sceRtcGetCurrentClockLocalTime error code %x", iErrorCode);
	}
}

#endif	// RSG_ORBIS
