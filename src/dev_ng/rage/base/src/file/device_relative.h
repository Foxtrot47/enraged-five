// 
// file/device_relative.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_DEVICE_RELATIVE_H
#define FILE_DEVICE_RELATIVE_H

#include "file/device.h"

namespace rage {

//
// name:		fiDeviceRelative
// description:	Specialised version of device that uses a relative path from another device
/* Notes from Adam Fowler:
	The "platform:" device name is just specific to GTA. We are in the process of changing 
	the structure of our build folder so that all the files common to all platforms are 
	in one directory tree and all the files that are platform dependent have another 
	directory tree per platform eg
		Gta/
			Common files/
			Pc files/
			Ps3 files/
			360 files/

	I used the class to define two devices common:/ and platform:/ to define where the 
	different sets of data go. 

	// setup relative common data device
	gCommonDevice.Init("<common files tree>", false);
	gCommonDevice.MountAs("common:/");
	// setup relative platform data device
	gPlatformDevice.Init("<platform dependent files tree>, false);
	gPlatformDevice.MountAs("platform:/");

	I did a quick test after setting this up of packing the common file tree into a packfile 
	and mounting that instead of the file tree and the game worked straight off the pack file.
*/
class fiDeviceRelative : public fiDevice
{
public:
	fiDeviceRelative() : m_pDevice(NULL) {}
	virtual ~fiDeviceRelative() { }

	void Init(const char* pPath, bool readOnly=true, fiDevice* pDeviceOverride = NULL);
	bool MountAs(const char *path);

	const char *FixName(char *dest,int destSize,const char *src) const;
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64&bias) const;
	virtual fiHandle OpenBulkDrm(const char *filename,u64 &outBias,const void * pDrmKey) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	int Size(fiHandle handle) const override;
	u64 Size64(fiHandle handle) const override;
	virtual bool Delete(const char *filename) const;
	virtual bool Rename(const char *from,const char *to) const;
	virtual bool MakeDirectory(const char *filename) const;
	virtual bool UnmakeDirectory(const char *filename) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;

	virtual fiHandle FindFileBegin(const char *filename,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;
	virtual bool SetEndOfFile(fiHandle handle) const;

	virtual u32 GetAttributes(const char *filename) const;
	virtual bool SetAttributes(const char *filename,u32 attributes) const;
	virtual bool PrefetchDir(const char *directory) const;
	virtual u32 GetPhysicalSortKey(const char* filename) const;
	virtual int GetResourceInfo(const char* name,datResourceInfo &info) const;
	virtual u64 GetBulkOffset(const char* name) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * name) const
	{
		return m_pDevice->GetRootDeviceId(name);
	}

	virtual const fiDevice *GetLowLevelDevice() const	{ return m_pDevice->GetLowLevelDevice(); }

	virtual char* FixRelativeName(char *dest,int destSize,const char *src) const {
		char basePath[RAGE_MAX_PATH];
		FixName(basePath, RAGE_MAX_PATH, src);
		m_pDevice->FixRelativeName(dest, destSize, basePath);
		return dest;
	}

	virtual bool IsMaskingAnRpf() const;
	virtual const fiDevice* GetRpfDevice() const;

protected:
	const fiDevice* m_pDevice;

private:
	char m_path[RAGE_MAX_PATH];
	s32 m_filenameOffset;
	bool m_IsReadOnly;
};

}	// namespace rage

#endif // !FILE_DEVICE_RELATIVE_H
