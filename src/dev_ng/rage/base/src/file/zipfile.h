// 
// file/packfile.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_ZIPFILE_H
#define FILE_ZIPFILE_H

#include "atl/array.h"
#include "data/resourceheader.h"
#include "file/device.h"
#include "string/string.h"

#include "zlib/zlib.h"

namespace rage {

class fiStream;

struct zipDirEntry;

struct fiZipfileState;

/*
	The fiZipfile device implements read-only zipfile access with compression support.
	Higher-level code is expected to call fiDevice::Mount with a new instance of fiZipfile
	that has succeeded in its Init call.  The zipfile will remember the device and handle
	of its original filename without consuming an fiStream permanently; in theory it is
	possible to even nest zipfiles within zipfiles -- which can be useful if you're trying
	to save memory, because the entire zipfile directory structure is kept resident in
	memory at all times.

	Internally each element of a pathname is stored separately (like a real filesystem would)
	so deep directory structures are not any more expensive, particularly if they
	result in shorter element names.  For example, prop_sandiego_lamppost_01 would be
	stored more efficiently as prop/sandiego/lamppost_01 if there were many other props
	in sandiego.
	<FLAG COMPONENT>
*/
class fiZipfile: public fiDevice {
public:
	fiZipfile();
	~fiZipfile();

	bool Init(const char *filename);

	bool MountAs(const char *filename);

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Size(fiHandle handle) const;
	virtual u64 Size64(fiHandle handle) const;
	virtual int Close(fiHandle handle) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual u32 GetAttributes(const char *filename) const;

	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int CloseBulk(fiHandle /*handle*/) const;

	virtual fiHandle FindFileBegin(const char *directoryName,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;

	virtual const char *GetDebugName() const;
	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return UNKNOWN; }

protected:
	void		FillData(fiFindData &outData,zipDirEntry *e) const;
	fiHandle	OpenInternal(const char *lookupName,bool readOnly) const;
	int			ReadInternal(fiZipfileState& state,void *outBuffer,int bufferSize) const;
	int			ReadInternalBulk(const fiZipfileState& state,u32 relativeOffset,void *outBuffer,int bufferSize) const;
	zipDirEntry* FindEntry(const char *name) const;

	zipDirEntry						*m_Root;
	fiHandle						m_Handle;				// (bulk) handle on the device of the zipfile itself
	u64								m_ArchiveBias;			// for nested zipfiles (sigh)
	u64								m_ArchiveSize;
	const fiDevice *				m_Device;				// device the zipfile itself lives on
	int								m_RelativeOffset;		// amount of incoming pathname to strip off
	char							m_Name[32];				// debug name
};

};	// namespace rage

#endif
