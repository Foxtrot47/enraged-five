<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="::rage::fiDeviceInstaller::InitData" >
    <string name="m_sourceFile" type="ConstString"/>
    <string name="m_partitionName" type="ConstString"/>
    <string name="m_partitionDesc" type="ConstString"/>
    <string name="m_mount" type="ConstString"/>
    <string name="m_platform" type="ConstString"/>
    <bool name="m_cached" init="false"/>
    <bool name="m_readOnly"/>
  </structdef>

  <structdef type="::rage::fiDeviceInstaller::InitDataList" >
    <array name="m_initDatas" type="atArray">
      <struct type="::rage::fiDeviceInstaller::InitData"/>
    </array>
  </structdef>

</ParserSchema>