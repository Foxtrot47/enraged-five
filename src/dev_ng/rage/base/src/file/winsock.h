// 
// file/winsock.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_WINSOCK_H
#define FILE_WINSOCK_H

// PURPOSE: Initialize windows sockets
extern void InitWinSock(int argc = 0, char** argv = NULL);

// PURPOSE: Terminate use of the WS2_32.dll
extern void ShutdownWinSock();

// PURPOSE: Sends data on a connected socket.
// PARAMS
//  s - socket.
//  buf - data to be sent.
//  len - the length of data to be sent.
// RETURNS: the amount of data to be sent.
// NOTES:
//	Keeps trying to send until len bytes have been sent or an error occurs
extern int safe_send(int s,const char *buf,int len);

// PURPOSE: Receives data from a connected socket.
// PARAMS
//  s - socket.
//  buf - data to be sent.
//  len - the length of data to be sent.
// RETURNS: the amount of data to be received.
// NOTES:
//  Keeps trying to recieve until len bytes have been read or an error occurs	
extern int safe_recv(int s,char *buf,int len);

#endif		// FILE_WINSOCK_H
