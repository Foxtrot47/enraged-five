// 
// file/savegame.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_SAVEGAME_H
#define FILE_SAVEGAME_H

#include "string/unicode.h"

#define CREATE_BACKUP_SAVES	(1 && RSG_PC)

#define USE_SAVE_DATA_MEMORY (1 && RSG_ORBIS) 

#define USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP	(1 && RSG_ORBIS)
#define USE_DOWNLOAD0_FOR_PROFILE_BACKUP	(1 && RSG_ORBIS)

#define USE_STEAM_CLOUD_SAVES (0 && __STEAM_BUILD)

#if USE_DOWNLOAD0_FOR_PROFILE_BACKUP
	#define DOWNLOAD0_PROFILE_ONLY(...)	__VA_ARGS__
#else
	#define DOWNLOAD0_PROFILE_ONLY(...)
#endif

#define USE_DOWNLOAD0_FOR_BACKUP		(USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP || USE_DOWNLOAD0_FOR_PROFILE_BACKUP)

namespace rage {

// Toplevel code must define this variable on non-Xenon builds.
// It is used as the directory name for all emulated content.
extern char *XEX_TITLE_ID;

namespace fiSaveGameState {

enum State {
	IDLE, HAD_ERROR,
	SELECTING_DEVICE, SELECTED_DEVICE,
	ENUMERATING_CONTENT, ENUMERATED_CONTENT,
	SAVING_CONTENT, SAVED_CONTENT,
	LOADING_CONTENT, LOADED_CONTENT,
	SAVING_ICON, SAVED_ICON,
	VERIFYING_CONTENT, VERIFIED_CONTENT,
	DELETING_CONTENT, DELETED_CONTENT,
	DELETING_CONTENT_FROM_LIST, DELETED_CONTENT_FROM_LIST,
	CHECKING_FREE_SPACE_NO_EXISTING_FILE, CHECKING_FREE_SPACE, HAVE_CHECKED_FREE_SPACE,
#if HACK_GTA4
	CHECKING_EXTRA_SPACE_NO_EXISTING_FILE, CHECKING_EXTRA_SPACE, HAVE_CHECKED_EXTRA_SPACE,
	READING_NAMES_AND_SIZES_OF_ALL_SAVE_GAMES, HAVE_READ_NAMES_AND_SIZES_OF_ALL_SAVE_GAMES,
#endif	//	HACK_GTA4
	GETTING_CREATOR, GOT_CREATOR,
	GETTING_FILE_SIZE, HAVE_GOT_FILE_SIZE,
	GETTING_FILE_MODIFIED_TIME, HAVE_GOT_FILE_MODIFIED_TIME,
	GETTING_FILE_EXISTS, HAVE_GOT_FILE_EXISTS
};

};

/*
	fiSaveGame manages independent save state for up to four different players.

	The class is designed to be polled every frame depending on what higher-level operation is in progress.

	To select the current device for load/save, call BeginSelectDevice.  If it returns true, keep calling
	CheckSelectDevice every frame until it returns true, at which point you should call EndSelectDevice.

	To enumerate all savegames on the current device, call BeginEnumeration with the largest number of content
	items you expect to handle.  If it returns true, keep calling CheckEnumeration every frame until it
	returns true, at which point you can call GetEnumerationCount and GetEnumerationData.  When you're done
	with the data, call EndEnumeration.

	To save a game, call BeginSave with the display name to associate with the content and a filename to
	use internally and the actual save data.  If it returns true, keep calling CheckSave every frame until it
	returns true, at which point it will also tell you if the save was successful.  Next call EndSave to
	return to the idle state.

	To load a game, call BeginLoad with the display name to associate with the content and a filename to
	use internally and the load data buffer.  If it returns true, keep calling CheckLoad every frame until it
	returns true, at which point it will also tell you the amount of data read.  Next call EndLoad to
	return to the idle state.

	Note that the display name is actually internally converted into a directory name, so you can actually
	do multiple savegames under the same display name if you use a different filename each time.  You cannot
	currently individually delete files under the content directory though.  Also note that the system
	browser only lets you manipulate top-level content objects, not files within the content directory.

	Any error detected causes the system to enter an error state which can only be left by doing BeginSelectDevice.
	The most common cause of errors is removing a memory card device.
*/
class fiSaveGame {
public:

   //list of possible errors while saving,loading,ect..
   enum Errors {
      SAVE_GAME_SUCCESS,
      NOT_ENOUGH_FREE_SPACE_ON_DEVICE,
      USER_CANCELLED_DEVICE_SELECTION,
      FILE_CORRUPT,
      DEVICE_CORRUPT,
      NO_FILES_EXIST,
      PATH_NOT_FOUND,
	  COULD_NOT_MAP_DEVICE,
	  THE_DEVICE_IS_NOT_READY,
	  PS3_ERROR_USER_CANCELLED,
	  PS3_ERROR_CALLBACK_ERROR,
	  PS3_ERROR_ACCESS,
	  PS3_ERROR_INTERNAL,
	  PS3_ERROR_PARAM,
	  PS3_ERROR_FAILURE,
	  PS3_ERROR_BUSY,
	  UNKNOWN_ERROR,
      NUM_ERROR_TYPES
   };

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4201)
#endif

	struct Content {	// Shadows XCONTENT_DATA structure
		union {
			struct {
				u32		DeviceId;			// Valid on XENON
				u32		ContentType;		// Valid on XENON
			};
			struct {
				u32		ModificationTimeHigh;			// Valid on PS3
				u32		ModificationTimeLow;			// Valid on PS3
			};
		};
		char16	DisplayName[128];
		char	Filename[42];

		void Clear() { DeviceId = 0; ContentType = 0; DisplayName[0] = '\0'; Filename[0] = '\0'; }
	};

#if __WIN32
#pragma warning(pop)
#endif

	struct Device {	//Mirrors XDEVICE_DATA structure
		u32		DeviceId;
		u32		DeviceType; //either hard drive or memory unit
		u64		DeviceBytes; //total capacity of the device
		u64		DeviceFreeBytes;	//number of free bytes
		char16  FriendlyName[27];	//27 == XCONTENTDEVICE_MAX_NAME_LENGTH
	};

	// Last SignIn Reserved for Global User
	static const int SIGNIN_COUNT = 5;

	// PURPOSE:	Setup
	static void InitClass();

	// PURPOSE:	Teardown
	static void ShutdownClass();

	// PURPOSE:	Call this once per frame to keep asynchronous operations moving.  Currently a NO-OP
	void UpdateClass();

	// PURPOSE:	Select the active content device
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			minSpaceAvailable - If nonzero, only devices with this much space available (in byte) are shown
	//			forceShow - force the device selector to show even if the system reports only 1 available device
	//			manageStorage - allows the user to select a Xenon storage device even if there is no free space - required for loading a saved game
	bool BeginSelectDevice(int signInId,u32 contentType,u32 minSpaceAvailable = 0,bool forceShow=false,bool manageStorage=false);

	// PURPOSE:	See if device selection has completed
	// PARAMS:	signInId - Which profile (0-3) to use
	bool CheckSelectDevice(int signInId);

	// PURPOSE:	Return currently selected device id
	// PARAMS:	signInId - Which profile (0-3) to use
	u32 GetSelectedDevice(int signInId);

	// PURPOSE:	Sets device id
	// PARAMS:	signInId - Which profile (0-3) to use
	//			deviceId - Which device to use
	void SetSelectedDevice(int signInId, u32 deviceId);

   void SetSelectedDeviceToAny(int signInId);

	// PURPOSE:	Close down a device selection
	void EndSelectDevice(int signInId);

	// PURPOSE:	Begin enumerating content for the associated signInId
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			outContent - Pointer to maxCount Content elements to be filled in (must be persistent until EndEnumeration)
	//			maxCount - Maximum number of items to return
	// RETURNS:	True if enumeration was successfully started
	bool BeginEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount);

#if RSG_ORBIS
	void SetLocalisedBackupTitleString(const char *pLocalisedBackupString);

	bool BeginPhotoEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount);
	bool BeginClipEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount,void *validateFunc);
	bool BeginMontageEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount,void *validateFunc);
#endif	//	RSG_ORBIS

	// PURPOSE:	Check if an enumeration is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	//			outCount - Number of items retrieve
	// RETURNS:	True if enumeration was completed (may not be valid), and outCount is set to number of items retrieved
	//			or -1 on an error
	bool CheckEnumeration(int signInId,int &outCount);

	// PURPOSE:	Close an enumeration successfully started via BeginEnumeration
	void EndEnumeration(int signInId);

	// PURPOSE:	Begins a content save
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			displayName - Human-readable name for content container
	//			filename - filename within the container to save under; must be DOS-style uppercase 8.3 name.
	//			saveData - Save game data (must persist until EndSave)
	//			saveSize - Size of game data (cannot be zero)
	//			overwrite - allows existing content to be overwritten when set to true
	// NOTES:   overwrite flag allows is for the actual file, not the content container -
	//          the content container will be opened if it exists, otherwise a new one will be created
	bool BeginSave(int signInId,u32 contentType,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool overwrite);

	// PURPOSE:	Checks if a content save is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	// RETURNS:	True if content save is complete, and outIsValid will be set to true if the operation
	//			was successful, or false if there was a failure.
	// NOTES:	outIsValid is only changed if CheckSave returned true (which will happen exactly once per Begin/End block)
	//          fileExists set to true if the file already exists
	bool CheckSave(int signInId,bool &outIsValid,bool &fileExists);

	// PURPOSE:	Ends a content save
	// PARAMS:	signInId - Which profile (0-3) to use
	void EndSave(int signInId);

#if CREATE_BACKUP_SAVES

	// PURPOSE:	Begins a content save
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			displayName - Human-readable name for content container
	//			filename - filename within the container to save under; must be DOS-style uppercase 8.3 name.
	//			saveData - Save game data (must persist until EndSave)
	//			saveSize - Size of game data (cannot be zero)
	//			overwrite - allows existing content to be overwritten when set to true
	// NOTES:   overwrite flag allows is for the actual file, not the content container -
	//          the content container will be opened if it exists, otherwise a new one will be created
	bool BeginBackupSave(int signInId,u32 contentType,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool overwrite);

	// PURPOSE:	Checks if a content save is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	// RETURNS:	True if content save is complete, and outIsValid will be set to true if the operation
	//			was successful, or false if there was a failure.
	// NOTES:	outIsValid is only changed if CheckSave returned true (which will happen exactly once per Begin/End block)
	//          fileExists set to true if the file already exists
	bool CheckBackupSave(int signInId,bool &outIsValid,bool &fileExists);

	// PURPOSE:	Ends a content save
	// PARAMS:	signInId - Which profile (0-3) to use
	void EndBackupSave(int signInId);

#endif	//CREATE_BACKUP_SAVES

	// PURPOSE:	Begins a check for sufficient free space to save a save game file
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	//			filename - filename within the container to save under; must be DOS-style uppercase 8.3 name.
	//			saveSize - Size of game data (cannot be zero)
	//						On 360, this is the size before calling CalculateDataSizeOnDisk
	// RETURNS:	True if free space check started successfully, false if there was a problem
	// NOTES:   On PS3, the calculation of free space differs depending on whether the file already exists or not
	//			On 360, assume that if there is any problem opening an existing file with the same name then 
	//				there is no existing file
	bool BeginFreeSpaceCheck(int signInId,const char *filename,u32 saveSize);

	// PURPOSE:	Checks if a free space check is complete
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// RETURNS:	True if free space check is complete. ExtraSpaceRequired will be >= 0 if there is enough space to save the file
	//			on the hard disk. A negative value gives the amount of space required.
	bool CheckFreeSpaceCheck(int signInId, int &ExtraSpaceRequired);

	// PURPOSE:	Ends a free space check
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	void EndFreeSpaceCheck(int signInId);

#if HACK_GTA4
	// PURPOSE:	Begins a check to determine how much extra space is required to save a save game file
	//			This gives slightly more information than BeginFreeSpaceCheck. The extra info is needed
	//			in certain circumstances such as checking if there is enough free space for several
	//			save games before allowing the game to start.
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	//			filename - filename within the container to save under; must be DOS-style uppercase 8.3 name.
	//			saveSize - Size of game data (cannot be zero)
	//						On 360, this is the size before calling CalculateDataSizeOnDisk
	// RETURNS:	True if extra space check started successfully, false if there was a problem
	// NOTES:	Implemented for PS3 and 360
	//			On PS3, the calculation of free space differs depending on whether the file already exists or not
	//			On 360, assume that if there is any problem opening an existing file with the same name then 
	//				there is no existing file
	bool BeginExtraSpaceCheck(int signInId, const char *filename, u32 saveSize);

	// PURPOSE:	Checks if an extra space check is complete
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// RETURNS:	True if extra space check is complete. 
	//			ExtraSpaceRequired gives the difference in size (in KB) between the new and existing file.
	//				It will be negative if the new file is smaller than the existing file.
	//				If a negative value is returned, you may want to treat it as if 0 had been returned
	//				so that you are not taking into account space that would only exist after the new
	//				file has overwritten the existing one.
	//			TotalHDDFreeSizeKB - This value is returned on both PS3 and 360
	//			SizeOfSystemFileKB - On PS3, this is required for any subsequent calls to CalculateSizeOfSaveGameFile
	//											I'm assuming that sysSizeKB is the same for all files
	//								 On 360, this parameter is not used
	// NOTES:	Implemented for PS3 and 360
	bool CheckExtraSpaceCheck(int signInId, int &ExtraSpaceRequired, int &TotalHDDFreeSizeKB, int &SizeOfSystemFileKB);

	// PURPOSE:	Ends an extra space check
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// NOTES:	Implemented for PS3 and 360
	void EndExtraSpaceCheck(int signInId);
#endif	//	HACK_GTA4

#if HACK_GTA4
	// PURPOSE:	Calculates the actual PS3 HDD space required to store a file of the given size
	// PARAMS:	DataSize - size of the buffer to be saved
	//			SizeOfSystemFiles - space required for PS3 system files that are stored along with each save game
	//								This value can be obtained by an earlier call of the ExtraSpaceCheck functions
	//								(I'm assuming that the system files take up the same amount of space for all files)
	// RETURNS:	The actual PS3 HDD space required
	// NOTES:	Only implemented for PS3 - displays an error on 360
	int CalculateSizeOfSaveGameFile(int DataSize, int SizeOfSystemFiles);
#endif	//	HACK_GTA4

#if HACK_GTA4
	// PURPOSE:	This is an attempt to speed up the free space checks at the start of the game on PS3.
	//	cellSaveDataAutoSave2 is just too slow. I still need to call ExtraSpaceCheck once before this to get
	//	the total free space on the hard disk and the size of the system files. So call ExtraSpaceCheck for 
	//	the profile and then call this for checking the save game slots.
	//	This is based on a suggestion of James Wang of SCEA Developer Support. We store the size of each file
	//	in the listparam string when a save is made and then read that in a Fixed Callback rather than sizeKB in a Stat Callback.
	//	This relies on the listparam being saved correctly with the file and the listparam not being needed for anything else.
	//	It also relies on sysSizeKB being the same for all files.
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	//			outContent		- Pointer to maxCount Content elements to be filled in (must be persistent until EndReadNamesAndSizesOfAllSaveGames)
	//			outFileSizes	- Pointer to maxCount int elements (to store the size of each file) (must be persistent until EndReadNamesAndSizesOfAllSaveGames)
	//			maxCount		- maximum size of the two arrays above
	// NOTES:	Only implemented for PS3 - does nothing on 360
	bool BeginReadNamesAndSizesOfAllSaveGames(int signInId, Content *outContent, int *outFileSizes, int maxCount);

	// PURPOSE:	Checks if the names and sizes of all save games have been successfully read
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// RETURNS:	True if the reading of all names and sizes is complete.
	//				outCount - contains the total number of save game files that exist
	// NOTES:	Only implemented for PS3 - does nothing on 360
	bool CheckReadNamesAndSizesOfAllSaveGames(int signInId, int &outCount);

	// PURPOSE:	Ends the reading of the names and sizes of all save games
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// NOTES:	Only implemented for PS3 - does nothing on 360
	void EndReadNamesAndSizesOfAllSaveGames(int signInId);
#endif	//	HACK_GTA4

	// PURPOSE:	Begins a content load
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			deviceId - Which device to use (get this from Content::DeviceId)
	//			filename - filename within the container to save under; must be DOS-style uppercase 8.3 name.
	//			loadData - Load game data buffer (must persist until EndLoad)
	//			loadSize - max size of game data (cannot be zero)
	bool BeginLoad(int signInId,u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize);

	// PURPOSE:	Checks if a content load is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	// RETURNS:	True if content load is complete, and loadSize will contain number of bytes read
	// NOTES:	loadSize is only changed if CheckLoad returned true (which will happen exactly once per Begin/End block)
	bool CheckLoad(int signInId,bool &outIsValid,u32 &loadSize);

	// PURPOSE:	Ends a content load
	// PARAMS:	signInId - Which profile (0-3) to use
	void EndLoad(int signInId);

	// PURPOSE:	Begins an icon save
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			filename - filename within the container to save under
	//			iconData - icon image buffer (must persist until EndSave)
	//			iconSize - size of icon image buffer (cannot be zero)
	bool BeginIconSave(int signInId,u32 contentType,const char *filename,const void *iconData,u32 iconSize);

	// PURPOSE:	Checks if an icon save is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	// RETURNS:	True if icon save is complete
	bool CheckIconSave(int signInId);

	// PURPOSE:	Ends an icon save
	// PARAMS:	signInId - Which profile (0-3) to use
	void EndIconSave(int signInId);

	// PURPOSE: Verify saved content (this will probably be platform specific)
	// PARAMS:  signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			deviceId - Which device to use (get this from Content::DeviceId)
	//			filename - filename within the container
	bool BeginContentVerify(int signInId,u32 contentType,u32 deviceId,const char *filename);

	// PURPOSE:	Checks if a content verify is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	// RETURNS:	True if content verify is complete, and verified will be set to true if the operation
	//			was successful, or false if there was a failure.
	bool CheckContentVerify(int signInId,bool &verified);

	// PURPOSE:	Ends a content verify
	// PARAMS:	signInId - Which profile (0-3) to use
	void EndContentVerify(int signInId);


	// PURPOSE: Get the size of a file so that we know how big a buffer we need to allocate to load the file
	//				On the 360, we use GetFileSize
	//				On the PS3, we read the size from listParam. This size will also include the size of the icon and system files
	// PARAMS:  signInId - Which profile (0-3) to use
	//			filename - filename within the container
	//			bCalculateSizeOnDisk - On 360, calling this with TRUE will use XContentCalculateSize to calculate the total size of the XContent package
	// RETURNS:	True if the get file size process began successfully, false if there was a problem
	bool BeginGetFileSize(int signInId, const char *filename, bool bCalculateSizeOnDisk);

	// PURPOSE:	Checks if a get file size process is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	//			fileSize - the size of the file in bytes
	//						on 360, this will be the size of the XContent package if BeginGetFileSize was called with bCalculateSizeOnDisk = true
	//						on PS3, the size includes the size of the save icon and any system files
	// RETURNS:	True if get file size is complete (successful or error)
	//				need to check GetState() for errors
	//				if successful then fileSize will contain the file size
	//			False if it hasn't finished yet
	bool CheckGetFileSize(int signInId, u64 &fileSize);

	// PURPOSE:	Ends a get file size process
	// PARAMS:	signInId - Which profile (0-3) to use
	void EndGetFileSize(int signInId);


	// PURPOSE: Get the modified time of a file.  Only implemented for PS3 so far. Called after a file is saved so that the game has an accurate record of the file's timestamp.
	// PARAMS:  signInId - Which profile (0-3) to use (ignored on PS3)
	//			filename - filename within the container
	// RETURNS:	True if the get file modified time process began successfully, false if there was a problem
	bool BeginGetFileModifiedTime(int signInId, const char *filename);

	// PURPOSE:	Checks if a get file modified time process is complete. Only implemented for PS3 so far
	// PARAMS:	signInId - Which profile (0-3) to use (ignored on PS3)
	//			modTimeHigh and modTimeLow - the two parts of the 8 byte time
	// RETURNS:	True if get file modified time is complete (successful or error)
	//				need to check GetState() for errors
	//				if successful then modTimeHigh and modTimeLow will contain the modified time
	//			False if it hasn't finished yet
	bool CheckGetFileModifiedTime(int signInId, u32 &modTimeHigh, u32 &modTimeLow);

	// PURPOSE:	Ends a get file modified time process. Only implemented for PS3 so far
	// PARAMS:	signInId - Which profile (0-3) to use (ignored on PS3)
	void EndGetFileModifiedTime(int signInId);

	// PURPOSE: Get Whether a given file exists, currently PS4 only
	// PARAMS:  signInId - Which profile (0-3) to use (ignored on PS3)
	//			filename - filename within the container
	// RETURNS:	True if the get file exists process began successfully, false if there was a problem
	bool BeginGetFileExists(int signInId, const char *filename);

	// PURPOSE:	Checks if a get file modified time process is complete. Only implemented for PS4 so far
	// PARAMS:	signInId - Which profile (0-3) to use (ignored on PS3)
	//			bool - whether it exists
	// RETURNS:	True if get file exists is complete (successful or error)
	//				need to check GetState() for errors
	//			False if it hasn't finished yet
	bool CheckGetFileExists(int signInId, bool &exists);

	// PURPOSE:	Ends a get file exists process. Only implemented for PS4 so far
	// PARAMS:	signInId - Which profile (0-3) to use (ignored on PS3)
	void EndGetFileExists(int signInId);


	// PURPOSE:	Begin deleting content for the associated signInId
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			filename - filename within the container to delete
	// RETURNS:	True if deletion was successfully started
	bool BeginDelete(int signInId,u32 contentType,const char *filename);

	// PURPOSE:	Check if a deletion is complete
	// PARAMS:	signInId - Which profile (0-3) to use
	// RETURNS:	True if content deletion is complete
	bool CheckDelete(int signInId);

	// PURPOSE:	Close a deletion successfully started via BeginDelete
	void EndDelete(int signInId);

	// PURPOSE:	Begin checking if the signed-in user is the creator of a save game file
	// PARAMS:	signInId - Which profile (0-3) to use
	//			contentType - the type of content
	//			deviceId - Which device to use (get this from Content::DeviceId)
	//			filename - filename of the save game to check
	// RETURNS:	True if started successfully
	// NOTES:	Xenon only
	//			Needed for TCRs
	bool BeginGetCreator(int signInId, u32 contentType,u32 deviceId,const char *filename);

	// PURPOSE:	Check if the GetCreator process is finished
	// PARAMS:	signInId - Which profile (0-3) to use
	//			bIsCreator - will be updated if the process completes successfully
	//			bHasCreator - will be updated if the process completes successfully
	// RETURNS:	True if GetCreator is complete (successful or error)
	//				need to check GetState() for errors
	//				if successful then bIsCreator will say whether the current signed-in user is the creator of the save game file
	//			False if it hasn't finished yet
	// NOTES:	Xenon only
	//			Needed for TCRs
#if __PPU && HACK_GTA4
	bool CheckGetCreator(int signInId, bool &bIsCreator, bool& bHasCreator);
#else
	bool CheckGetCreator(int signInId, bool &bIsCreator);
#endif

	// PURPOSE: Ends a GetCreator
	// PARAMS:	signInId - Which profile (0-3) to use
	// NOTES:	Xenon only
	//			Needed for TCRs
	void EndGetCreator(int signInId);

	// PURPOSE:	Begin the PS3's delete from list function
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// RETURNS:	True if deletion was successfully started
	// NOTES:	PS3 only
	bool BeginDeleteFromList(int signInId);

	// PURPOSE:	Check if the PS3's delete from list function has finished
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// RETURNS:	True if finished - can't tell if the player actually chose to delete any files
	// NOTES:	PS3 only
	bool CheckDeleteFromList(int signInId);

	// PURPOSE:	Tidies up after the PS3's delete from list function has finished
	// PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
	// NOTES:	PS3 only
	void EndDeleteFromList(int signInId);



   // PURPOSE: returns the total available space for the currently selected device
   // PARAMS:	signInId - Which profile (0-3) to use
   u64 GetTotalAvailableSpace(int signInId);

   // PURPOSE:	Retrieves device data for the specified device
   // PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
   //			deviceId - The device whose information we're looking up
   //			outDevice - Space to write out device data for our chosen device
   // RETURNS:	True if we got the data
   bool GetDeviceData(int signInId, u32 deviceId, Device *outDevice);

   // PURPOSE:	Retrieves device data for the current device
   // PARAMS:	signInId - Which profile (0-3) to use - ignored on PS3
   //			outDevice - Space to write out device data for our current device
   // RETURNS:	True if we got the data
   bool GetCurrentDeviceData(int signInId, Device *outDevice);

   // PURPOSE: returns the total space on file for data with the given size. Note that xenon and ps3 will increase the size
   //          of the file by adding headers, security, encryption, ect.  This file returns the size of the file on disk
   //          after all modifications and additions are applied by the system.
   // PARAMS:	signInId - Which profile (0-3) to use
   // PARAMS:	dataSize - The size of the data that will be saved/loaded
   u64 CalculateDataSizeOnDisk(int signInId,u32 dataSize);

   // PURPOSE: checks to see if the given device is valid
   // PARAMS:	signInId - Which profile (0-3) to use
   // RETURNS:	True if device is valid, false otherwise
   bool IsDeviceValid(int signInId, u32 deviceId);

   // PURPOSE: checks to see if the currently selected device is valid
   // PARAMS:	signInId - Which profile (0-3) to use
   // RETURNS:	True if device is valid, false otherwise
   bool IsCurrentDeviceValid(int signInId);

   // PURPOSE: gets the maximum number of users that can be signed into the system at one time (hardware specific)
   // PARAMS:	signInId - Which profile (0-3) to use
   // RETURNS:	the maximum number of users that can be signed into the system at one time
   int GetMaxUsers();

   // PURPOSE: checks to see if a storage device has been inserted or removed
   // PARAMS:	signInId - Which profile (0-3) to use
   // RETURNS:	True if a device has changed, false otherwise
   bool IsStorageDeviceChanged(int signInId);

   // PURPOSE: checks to see if a storage device has been removed
   // PARAMS:	signInId - Which profile (0-3) to use
   // RETURNS:	True if a device has been removed, false otherwise
   bool IsStorageDeviceRemoved(int signInId);

   // PURPOSE: returns the error associated with the latest action
   // PARAMS:	signInId - Which profile (0-3) to use
   fiSaveGame::Errors GetError(int signInId);

	// PURPOSE:	Returns current internal state.
	// PARAMS:	signInId - Which profile (0-3) to use
	// NOTES:	Whenever this returns the ERROR state, the only way to clear it again is to call BeginSelectDevice.  
	//			This should also be reflected in some change in user-visible state.
	fiSaveGameState::State GetState(int signInId) const;

   // PURPOSE: set the current state to IDLE
   void SetStateToIdle(int signInId);

   // PURPOSE:	Checks to see if a file has been accessed (verified, loaded, or saved)
   // PARAMS:	signInId - Which profile (0-3) to use
   // PARAMS:	filename - The filename to check against
   // NOTES:	Checks to see if the given file has been accessed.  This function is usefull when verifying
   //          that a file has not been removed
   bool HasFileBeenAccessed(int signInId, const char* filename);

   // PURPOSE:	Sets the PNG file to be used for the savegame icon on PS3
   // PARAMS:	pIcon - pointer to memory holding the 320 x 176 PNG file
   // PARAMS:	SizeOfIconFile
   // NOTES:	Doesn't do anything on Xenon
   void SetIcon(char *pIcon, const u32 SizeOfIconFile);


   // PURPOSE:	Sets the maximum number of save files to enumerate on PS3
   // PARAMS:	MaxNumSaveGames - maximum number of save games that can be enumerated
   //			Should be the same as the maxCount parameter of BeginEnumeration
   // NOTES:	Doesn't do anything on Xenon
   void SetMaxNumberOfSaveGameFilesToEnumerate(int MaxNumSaveGames);


   // PURPOSE:	On the PS3, this will set the Title of all subsequent save games.
   //			The display name of the individual save game will be the subtitle
   // PARAMS:	string to set as the save game title
   // NOTES:	Doesn't do anything on Xenon
   void SetSaveGameTitle(const char *pSaveGameTitle);

   // PURPOSE:	Enables checking for bind errors and sets the string to display if a bind error occurs
   // PARAMS:	bind error message
   // NOTES:	Doesn't do anything on Xenon
   //			CELL_SAVEDATA_BINDSTAT_ERR_NOUSER always seems to occur unless the user
   //			is registered with the PLAYSTATION Network (and has been given a unique Account ID).
   //			This function won't be any use unless the game requires all players to be registered
   //			with the PLAYSTATION Network
   void SetCheckForUserBindErrors(const char *pBindErrorMessage);

   // PURPOSE: On Orbis, this will set the User Service Id for the given Index.
   // PARAMS:	which profile (0-3) to use, and the user service id to set
   // NOTES:	Doesn't do anything on non-Orbis Platforms
   void SetUserServiceId(int signInId, int userId);

   // PURPOSE: On Orbis, each user is given up to 16 mount indices per Title
   // PARAMS: which profile (0-3) to use, and the mount index (0-15)
   // NOTES:	Doesn't do anything on non-Orbis Platforms
   void SetMountIndex(int signInId, int index);

   // PURPOSE On Orbis, set the directory name based on a dialog result
   // PARAMS:	Which profile (0-3) to use, and the directory naame
   // NOTES:	Doens't do anything on non-orbis platforms
   void SetSelectedDirectory(int signInId, const char* directory);

   // PURPOSE:	On Orbis, begin deleting an entire save mount for the associated signInId
   // PARAMS:	signInId - Which profile (0-3) to use
   //			pName - the directory or filename
   //			bIsFilename - if TRUE then interpret pName as a filename and delete the folder that contains that file
   //						- if FALSE then interpret pName as the name of the directory to be deleted
   // RETURNS:	True if deletion was successfully started
   // NOTES:	Doesn't do anything on non-Orbis Platforms
   bool BeginDeleteMount(int signInId, const char *pName, bool bIsFilename);

   // PURPOSE:	On Orbis, Check if a Orbis save mount deletion is complete
   // PARAMS:	signInId - Which profile (0-3) to use
   // RETURNS:	True if mount deletion is complete
   // NOTES:	Doesn't do anything on non-Orbis Platforms
   bool CheckDeleteMount(int signInId);

   // PURPOSE:	On Orbis, Close a deletion successfully started via BeginDelete
   // NOTES:	Doesn't do anything on non-Orbis Platforms
   void EndDeleteMount(int signInId);

   // PURPOSE: fills the current device for testing purposes
   void FillDevice(int signInId);

#if USE_SAVE_DATA_MEMORY
   // PURPOSE: On Orbis, this will setup the SaveDataMemory Library.
   void SetSaveDataMemory(int userId);
#endif

#if __PPU && HACK_GTA4
   //	For ELC, we're going to use the savegame folder that is used by DLC.
   //	So we need to allow the game to specify the folder rather than just always using XEX_TITLE_ID
   void SetSaveGameFolder(const char *pTitleIDForSaveGameFolder);
#endif
};

extern fiSaveGame SAVEGAME;

};

#endif
