//
// file/streambuffers.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stream.h"

using namespace rage;

/* You shouldn't need to modify these definitions from their defaults; if your
   project needs different values here just add your own DECLARE_STREAM_BUFFERS
   call at file scope of your toplevel tester or some other place where it's
   guaranteed to get linked in before this library version is found.
   Actually unity builds kinda hose us here, so just change it directly.
   */
#if IS_CONSOLE

#if RSG_DURANGO || RSG_ORBIS
DECLARE_STREAM_BUFFERS(32)
#elif __DEV
DECLARE_STREAM_BUFFERS(20)
#else
DECLARE_STREAM_BUFFERS(16)
#endif

#else
DECLARE_STREAM_BUFFERS(64)
#endif
