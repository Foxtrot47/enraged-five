// 
// file/dummy_titleid.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

namespace rage {

#if __PS3 && !defined(__UNITYBUILD)
char *XEX_TITLE_ID = "RAGE12345";
#endif

} // namespace rage
