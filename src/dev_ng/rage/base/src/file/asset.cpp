//
// file/asset.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "file/file_channel.h"
#include "asset.h"
#include "device.h"
#include "limits.h"

#include "string/string.h"
#include "system/param.h"
#include "system/criticalsection.h"
#include "system/memops.h"

#if !__FINAL
#include "system/exec.h"
#include "system/rageroot.h"
#endif

#include <string.h>

using namespace rage;

RAGE_DEFINE_CHANNEL(file);
RAGE_DEFINE_SUBCHANNEL(file, asset, rage::DIAG_SEVERITY_DEBUG3);

#undef __file_channel
#define __file_channel file_asset

fiAssetManager rage::ASSET;

static sysCriticalSectionToken s_AssetCritSec;

// Put this in its own namespace so that we don't get duplicate symbol errors.
namespace asset {
PARAM(path,"[RAGE] Set default asset path root");
}

#if __DEV
#define CHECK(tag,name,action) if (1) { \
	fiStream *rv = action; \
	if (!rv && !probeOnly) \
		fileWarningf("Cannot " #tag " file '%s'.",name); \
	return rv; \
	} else
#else
#define CHECK(tag,name,action)	return action
#endif

#define SEPARATOR			'/'
#define SEPARATOR_STRING	"/"
#define OTHER_SEPARATOR	'\\'


fiAssetManager::fiAssetManager() {
	m_Paths[0][0] = 0;
	m_SP = 0;
	m_PathCount = 1;
	m_WritePath = 0;
	m_WritePathIsWriteOnly = false;
}


fiAssetManager::~fiAssetManager() {
}



static void FixPath(char *dest,int destSize,const char *src, bool trailingSlash = true) {
	safecpy(dest,src,destSize);
	if (dest[0] == 0)
		return;

	int sl = StringLength(dest);
	char *path = dest;

	// Convert to expected slash type
	while (*path) {
		if (*path == OTHER_SEPARATOR)
			*path = SEPARATOR;
		path++;
	}

	// Tack on trailing appropriate slash if necessary
	if (trailingSlash && sl && dest[--sl] != SEPARATOR) {
		dest[++sl] = SEPARATOR;
		dest[++sl] = 0;
	}
}



void fiAssetManager::SetPath(const char *_path) {
#if !__FINAL
	int rarLen = (int)strlen(RAGE_ASSET_ROOT);
	char newAssetRoot[RAGE_MAX_PATH];
	if (_path && !asset::PARAM_path.Get() && !strncmp(_path,RAGE_ASSET_ROOT,rarLen) && sysGetEnv("RAGE_ASSET_ROOT",newAssetRoot,sizeof(newAssetRoot)-1)) {
		int newLen = StringLength(newAssetRoot);
		if (!newLen || (newAssetRoot[newLen-1] != '/' && newAssetRoot[newLen-1] != '\\')) {
			newAssetRoot[newLen++] = SEPARATOR;
			newAssetRoot[newLen] = '\0';
		}
		Displayf("Redirecting default rage sample path to '%s'...",newAssetRoot);
		safecat(newAssetRoot,_path+rarLen,sizeof(newAssetRoot));
		_path = newAssetRoot;
	}
#endif
	m_PathCount = 0;
	m_WritePathIsWriteOnly = false;
	char buffer[RAGE_MAX_PATH], *bp = buffer;
	asset::PARAM_path.Get(_path);

	// Make sure we have a valid path we can set
	if (!_path) {
		Errorf("Invalid path passed into fiAssetManager::SetPath()");
		return;
	}

	safecpy(buffer,_path,sizeof(buffer));
	m_WritePath = 0;
	while (m_PathCount < c_MaxPaths) {
		char *element = strtok(bp,";");
		bp = NULL;
		if (!element)
			break;
		if (element[0] == '*' || element[0] == '#') {
			m_WritePathIsWriteOnly = (element[0] == '#');
			++element;
			Displayf("fiAssetManager::SetPath - Setting write path to '%s'",element);
			m_WritePath = m_PathCount;
		}
		// Displayf("addelement(%s)",element);
		if (!strchr(element,':') 
#if __PPU || RSG_ORBIS
			&& element[0] != SEPARATOR
#endif // __PPU || RSG_ORBIS
			)
			fileWarningf("ASSET.SetPath works best with fully-qualified names (ie drive letter or device name, not '%s')",element);
		FixPath(m_Paths[m_PathCount],sizeof(m_Paths[m_PathCount]),element);
		++m_PathCount;
	}
	// Handle empty string properly
	if (!m_PathCount) {
		m_Paths[0][0] = 0;
		m_PathCount = 1;
	}
}


bool fiAssetManager::IsAbsolutePath(const char *base) {

	if ( !base )
		return false;

	// If the name starts with either slash, or contains a colon anywhere, it does not need a path.
	if (base[0] == '/' || base[0] == '\\' || strchr(base,':'))
		return true;
	else
		return false;
}

bool fiAssetManager::MakeRelative(char* dest, int destSize, const char* src)
{
	Assertf(dest != src, "Can't have the same src, dest strings");
	if (!IsAbsolutePath(src))
	{
		safecpy(dest, src, destSize);
		return true;
	}

	char normSrc[RAGE_MAX_PATH];
	FixPath(normSrc, RAGE_MAX_PATH, src, false);
    strlwr( normSrc );      // lowercase for strncmp (strncmpi doesn't exist on PSN)

	for (int i = 0; i < m_PathCount; i++)
	{
		// copy the root path so we can normalize it
		char root[RAGE_MAX_PATH];
		FixPath(root, RAGE_MAX_PATH, GetPath(i));
        strlwr( root );     // lowercase for strncmp (strncmpi doesn't exist on PSN)

		int rootLen = (int) strlen(root);
        if ( rootLen == 0 )
        {
            continue;
        }

		if ( (rootLen > 0) && !strncmp( root, normSrc, rootLen ) )
		{
			// OK, leading parts of the paths match. Make it a relative path
			dest[0] = '$';
			dest[1] = '/';
			safecpy(dest+2, normSrc + rootLen, RAGE_MAX_PATH-2);

			// Now we have a path name - make sure its not the name of a file earlier in the path list
			for(int j = 0; j < i; j++)
			{
				char buf[RAGE_MAX_PATH];
				FullPath(buf, RAGE_MAX_PATH, dest, "", j);
				if (ASSET.Exists(buf, ""))
				{
					Errorf("MakeRelative - \"%s\" names an existing file earlier in the path (orig file: \"%s\", found \"%s\")", dest, normSrc, buf);
					safecpy(dest, normSrc, destSize);
					return false;
				}
			}
			return true;
		}
	}
	safecpy(dest, normSrc, destSize);
	return false;
}


bool fiAssetManager::MakeFolderRelative(char* dest, int destSize, const char* src)
{
	// horrible temporary implementation + only works with one path
	Assert(ASSET.GetPathCount() == 1);

	// make source absolute
	char srcAbsolute[RAGE_MAX_PATH];
	ASSET.FullPath(srcAbsolute, RAGE_MAX_PATH, src, NULL);

	// store away old path
	char oldPath[RAGE_MAX_PATH];
	strcpy(oldPath, ASSET.GetPath());

	// temporarily replace path with one that includes pushed folders
	char pathIncludingPushes[RAGE_MAX_PATH];
	ASSET.GetStackedPath(pathIncludingPushes, RAGE_MAX_PATH);
	ASSET.SetPath(pathIncludingPushes);

	// try making relative to folder
	ASSET.MakeRelative(dest, destSize, srcAbsolute);

	// reset old path
	ASSET.SetPath(oldPath);

	// if $/ found on front of path, then making relative to folder succeeded ( strip off $/ )
	if(dest && dest[0]=='$' && (dest[1]=='/'||dest[1]=='\\'))
	{
		char* ch = dest+2;
		do 
		{
			*(ch-2) = *ch;
		} 
		while(*(ch++));

		return true;
	}
	else
	{
		ASSET.MakeRelative(dest, destSize, src);
		return false;
	}
}


void fiAssetManager::AddExtension(char *dest,int maxLen,const char *base,const char *ext) {
	safecat(dest,base,maxLen);
	// If we have a nonempty extension, and either no extension in the base name or the one that is
	// there is different, add the new extension.
	if (ext && ext[0] && (!strrchr(base,'.') || stricmp(strrchr(base,'.')+1,ext))) {
		safecat(dest,".",maxLen);
		safecat(dest,ext,maxLen);
	}
	char *d = dest;
	char *s = dest;
	while (*s) {
		// Normalize to forward slashes
		if (*s == OTHER_SEPARATOR)
			*d = SEPARATOR;
		else
			*d = *s;
		// Remove doubled-up slashes (except at start of a filename so UNC paths will work)
		if (d < dest+2 || d[0] != SEPARATOR || d[-1] != SEPARATOR)
			++d;
		++s;
	}
	*d = 0;

	if ((int)strlen(dest)+1==maxLen)
		fileWarningf("DANGER!  Likely truncated path: %s",dest);
}


static bool StripDotDots(char *buffer) {
	char *dot = strchr(buffer,'.');
	if (dot && dot > buffer && (dot[-1]==SEPARATOR || dot[-1]==OTHER_SEPARATOR) &&
		dot[1] == '.' && (dot[2]==SEPARATOR || dot[2]==OTHER_SEPARATOR)) {
		char *src = dot+2;
		while (dot > buffer+2 && (dot[-2]!=SEPARATOR && dot[-2]!=OTHER_SEPARATOR))
			--dot;
		memmove(dot-2,src,StringLength(src)+1);
		return true;
	}
	else
		return false;
}


bool fiAssetManager::FullReadPath(char *dest,int maxLen,const char *base,const char *ext) {
	const char *origBase = base;
	for (int pathIndex=0; pathIndex<m_PathCount; pathIndex++) {
		if (m_WritePathIsWriteOnly && pathIndex == m_WritePath)
			continue;
		if (!IsAbsolutePath(base)) {
			// Always add asset root path
			safecpy(dest, GetPath(pathIndex),maxLen);

			if (base[0] == '$')
				base += 2;
			else if (m_SP) {
				// If somebody PushFolder'd something with a drive letter or a starting slash
				// let that take precedence over the asset path.
				if (IsAbsolutePath(m_Entries[m_SP-1].folder))
					safecpy(dest, m_Entries[m_SP-1].folder,maxLen);
				else
					safecat(dest, m_Entries[m_SP-1].folder, maxLen);
			}
		}
		else
			dest[0] = 0;

		AddExtension(dest,maxLen,base,ext);

		while (StripDotDots(dest))
			;

		const fiDevice *device = fiDevice::GetDevice(dest);
		if (device && device->GetAttributes(dest) != FILE_ATTRIBUTE_INVALID)
			return true;

		base = origBase;
	}
	return false;
}


void fiAssetManager::FullPath(char *dest,int maxLen,const char *base,const char *ext,int pathIndex) {
	if (!IsAbsolutePath(base)) {
		// Always add asset root path
		safecpy(dest, GetPath(pathIndex),maxLen);

		if (base[0] == '$')
			base += 2;
		else if (m_SP) {
			// If somebody PushFolder'd something with a drive letter or a starting slash
			// let that take precedence over the asset path.
			if (IsAbsolutePath(m_Entries[m_SP-1].folder))
				safecpy(dest, m_Entries[m_SP-1].folder,maxLen);
			else
				safecat(dest, m_Entries[m_SP-1].folder, maxLen);
		}
	}
	else
		dest[0] = 0;

	AddExtension(dest,maxLen,base,ext);

	while (StripDotDots(dest))
		;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Given a filename, looks through all paths and returns the index of the 
//	first path that contains the file.  This is useful if the game has multiple
//	data sources, but only some are mounted.  Pass this index to FullPath to
//	expand the correct data path you need (instead of just "the first one").
//  NOTE that you usually should consider FullReadPath() or FullWritePath() instead.
//
//	RETURNS
//		-1 = error
//		0+ = the ASSET path index
//
int fiAssetManager::FindPath(const char* base, const char* ext, bool printFailures)
{
	char buf[RAGE_MAX_PATH];
	fiStream *rv = NULL;
	int result = -1;

	for (int i = 0; i < m_PathCount && !rv; i++)
	{
		FullPath(buf, sizeof(buf), base, ext, i);
		rv = fiStream::Open(buf, true);
		if (rv)
		{
			result = i;
			rv->Close();
		}
		else if(printFailures)
		{
			fileWarningf("ASSET.FindPath could not find '%s'",buf);
		}
	}

	if (result == -1)
	{
		fileWarningf("ASSET.FindPath could not find an archive that contains the file '%s.%s'.", base, ext);
	}
	return result;
}



void fiAssetManager::GetStackedPath(char *dest,int maxLen, int pathIndex) {
	Assert(pathIndex >= 0 && pathIndex < m_PathCount);

	// Always add asset root path
	safecpy(dest, GetPath(pathIndex),maxLen);

	if (m_SP) {
		// If somebody PushFolder'd something with a drive letter,
		// let that take precedence over the asset path.
		if (m_Entries[m_SP-1].folder[0] && IsAbsolutePath(m_Entries[m_SP-1].folder))
			safecpy(dest, m_Entries[m_SP-1].folder,maxLen);
		else
			safecat(dest, m_Entries[m_SP-1].folder, maxLen);
	}

	while (StripDotDots(dest))
		;
}

void fiAssetManager::PushFolder(const char *folder) {
	Assertf(!datResource_sm_Current,"Calling PushFolder from a resource ctor, this can cause deadlocks!");
	s_AssetCritSec.Lock();
	Assert(m_SP < MaxEntries);
	if (folder[0] == '$' || IsAbsolutePath(folder))
		FixPath(m_Entries[m_SP].folder,sizeof(m_Entries[0].folder),folder[0]=='$'?folder+2:folder);
	else {
		char cd[RAGE_MAX_PATH];
		safecpy(cd,m_SP?m_Entries[m_SP-1].folder:"",sizeof(cd));
		safecat(cd,folder,sizeof(cd));
		FixPath(m_Entries[m_SP].folder,sizeof(m_Entries[0].folder),cd);
	}
	++m_SP;
}


void fiAssetManager::PopFolder() {
	Assert(m_SP);
	--m_SP;
	s_AssetCritSec.Unlock();
}


fiStream *fiAssetManager::Open(const char * base,const char * ext, bool DEV_ONLY(probeOnly),bool readOnly) {
	char buf[RAGE_MAX_PATH];
	fiStream *rv = NULL;
#if __DEV
	bool fileFound = false;
#endif
	for (int i=0; i<m_PathCount && !rv; i++) {
		if (!readOnly && m_WritePathIsWriteOnly && i == m_WritePath)
			continue;

		FullPath(buf,sizeof(buf),base,ext,i);
		rv = fiStream::Open(buf,readOnly); 
		if (rv)
		{
#if __DEV
			fileFound = true;
			fileDebugf1("Opening '%s'...",buf);
#endif
		}
	}

#if __DEV
	if ((!fileFound) && (!probeOnly))
	{
		fileWarningf("Cannot open file '%s'.",buf);
	}
#endif
	return rv;
}

fiStream *fiAssetManager::Create(const char * base,const char * ext,bool probeOnly) {
	char buf[RAGE_MAX_PATH];
	FullPath(buf, sizeof(buf), base, ext, m_WritePath);
	fileDebugf1("Creating '%s'...", buf);
#if __BANK
	if(!ASSET.Exists(base,ext))
		CreateLeadingPath(buf);
#endif
	fiStream *rv = fiStream::Create(buf);
	if (!rv && !probeOnly)
		fileWarningf("Cannot create file '%s'.",buf);
	return rv;
}


u32 fiAssetManager::GetAttributes(const char * base,const char *ext) {
	char buf[RAGE_MAX_PATH];
	FullPath(buf,sizeof(buf),base,ext);
	const fiDevice *dev = fiDevice::GetDevice(buf);
	return dev? dev->GetAttributes(buf) : FILE_ATTRIBUTE_INVALID;
}


bool fiAssetManager::SetAttributes(const char * base,const char *ext,u32 attributes) {
	char buf[RAGE_MAX_PATH];
	FullPath(buf,sizeof(buf),base,ext);
	const fiDevice *dev = fiDevice::GetDevice(buf);
	return dev? dev->SetAttributes(buf,attributes) : false;
}


bool fiAssetManager::ClearAttributes(const char * base,const char * ext,u32 attrToClear) {
	u32 current = GetAttributes(base,ext);
	if (current != FILE_ATTRIBUTE_INVALID)
		return SetAttributes(base,ext,current & ~attrToClear);
	else
		return false;
}


bool fiAssetManager::Exists(const char * base,const char * ext) {
	char buffer[RAGE_MAX_PATH];
	return FullReadPath(buffer,sizeof(buffer),base,ext);
}


const char *fiAssetManager::FileName(const char * name) {

	if (!name)
		return NULL;

	// If this is a memory file name, we need to use a
	// completely different check.
	if (!strncmp(name,"memory:",7))	{
		// It's a memory filename - look for the second colon
		// and skip it.
		const char *result = strchr(name + 7, ':');

		return (result) ? result + 1 : NULL;
	}

	const char *e = name + StringLength(name);
	while (e > name) {
		if (e[-1] == '/' || e[-1] == '\\')
			break;
		else
			--e;
	}
	return e;
}


void fiAssetManager::BaseName(char *base,int bufSize,const char * name) {
	bufSize--;
	while(*name && (*name != '.') && (bufSize > 0)) {
		*base++ = *name++;
		bufSize--;
	}
	*base=0;	
}


bool fiAssetManager::CreateLeadingPath(const char *filename) {
	char buf[RAGE_MAX_PATH];
	FullPath(buf,sizeof(buf),filename,"");
	const fiDevice *d = fiDevice::GetDevice(buf,true);
	if (!d || !IsAbsolutePath(buf))
		return false;

	char *p = buf + 3;

	while (*p) {
		while (*p && *p != '/' && *p != '\\')
			++p;
		if (*p) {
			char ch = *p;
			*p = 0;
			if (d->GetAttributes(buf) == ~0U)
				d->MakeDirectory(buf);
			*p++ = ch;
		}
	}
	return true;
}

int fiAssetManager::EnumFiles(const char *directory,void (*callback)(const fiFindData &data,void *userArg),void *userArg, bool bCheckAllExtraMountDevices)
{
	char dest[RAGE_MAX_PATH];
	int matches = 0;

    char searched[c_MaxPaths][RAGE_MAX_PATH];
	for (int i=0; i<m_PathCount; i++) {
		if (m_WritePathIsWriteOnly && i == m_WritePath)
			continue;
		FullPath(dest,sizeof(dest),directory,"",i);

        // don't search the same directory more than once
        bool skip = false;
        for ( int j = 0; j < i; ++j )
        {
            if ( stricmp( dest, searched[j] ) == 0 )
            {
                skip = true;
                break;
            }
        }

        if ( skip )
        {
            continue;
        }

		matches = fiDevice::EnumFiles(dest,callback,userArg,bCheckAllExtraMountDevices);

        safecpy( searched[i], dest, sizeof(searched) );
	}
	return matches;
}


void fiAssetManager::RemoveExtensionFromPath(char *dest,int destSize,const char *src) {
	if (dest != src)
		safecpy(dest,src,destSize);
	int sl = StringLength(dest);
	while (sl && dest[sl-1] != SEPARATOR && dest[sl-1] != OTHER_SEPARATOR) {
		--sl;
		if (dest[sl] == '.') {
			dest[sl] = 0;
			break;
		}
	}
}

const char* fiAssetManager::FindExtensionInPath(const char *path) {
	if (path)
	{
		int sl = StringLength(path);
		while (sl && path[sl-1] != SEPARATOR && path[sl-1] != OTHER_SEPARATOR) {
			--sl;
			if (path[sl] == '.') {
				return &(path[sl]);
			}
		}
	}

	return NULL;
}

void fiAssetManager::RemoveNameFromPath(char *dest,int destSize,const char *src) {
	if (dest != src)
		safecpy(dest,src,destSize);
	int sl = StringLength(dest);
	while (sl && dest[sl-1] != SEPARATOR && dest[sl-1] != OTHER_SEPARATOR)
		--sl;
	if (sl)
		dest[sl-1] = 0;
}



bool fiAssetManager::InsertPathElement(char *inoutBuffer,int bufferSize,const char *insert,const char* insertAfter) {
	// Validate buffer size
	if (strlen(inoutBuffer) + strlen(insert) + 1 > (size_t)bufferSize)
		return false;

	// Make sure there's a reasonable insertion point (get the address of the first character to move)
	char *lastSlash;
	int offset = 1;

	if(insertAfter==NULL)
	{
		lastSlash = strrchr(inoutBuffer,'/');
	}
	else
	{
		// could be that we'll need to do something like an strrstr operation here
		lastSlash = strstr(inoutBuffer,insertAfter);
		offset = (int)strlen(insertAfter);
	}

	if (!lastSlash)
		lastSlash = inoutBuffer;
	else
		lastSlash+=offset;

	int insertLen = (int) strlen(insert);

	// Move last part of filename over rightward with overlapping copy (leave room for another slash,
	// and make sure we include the \0 in the copy)
	memmove(lastSlash + insertLen + 1,lastSlash,strlen(lastSlash)+1);

	// Copy in the filename part
	sysMemCpy(lastSlash,insert,insertLen);

	// Insert the additional slash
	lastSlash[insertLen] = '/';

	return true;
}

#undef SEPARATOR
#undef SEPARATOR_STRING
#undef OTHER_SEPARATOR
