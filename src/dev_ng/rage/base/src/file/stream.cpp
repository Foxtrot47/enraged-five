//
// file/stream.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stream.h"

#include "file/file_channel.h"
#include "file/device.h"
#include "system/ipc.h"
#include "system/memory.h"		// sigh, cross-dependency
#include "system/param.h"
#include "system/criticalsection.h"
#include "diag/channel.h"

#include <stdarg.h>
#include <string.h>
#include <algorithm>

using namespace rage;

RAGE_DEFINE_SUBCHANNEL(file, stream, rage::DIAG_SEVERITY_DEBUG3);

#undef __file_channel
#define __file_channel file_stream

#undef DEBUG

#if defined(__UNITYBUILD) && defined(_MSC_VER)
#pragma warning(disable: 4702)
#endif

#if __WIN32
#define SEPARATOR			'\\'
#define SEPARATOR_STRING	"\\"
#define OTHER_SEPARATOR		'/'
#else
#define SEPARATOR			'/'
#define SEPARATOR_STRING	"/"
#define OTHER_SEPARATOR		'\\'
#endif

StreamOpenCallback fiStream::sm_OpenCallback;
StreamCreateCallback fiStream::sm_CreateCallback;
StreamCloseCallback fiStream::sm_CloseCallback;
StreamReadCallback fiStream::sm_ReadCallback;

const char * const fiStream::sm_UnavailableString = "unavailable";

const char * fiStream::sm_FaultInjectionPath = 0;

#if __ASSERT
bool fiStream::sm_AllowBlockingLoads = true;
#endif // __ASSERT

#if __WIN32PC && __DEV
bool fiStream::m_bOutputAllFileAccess = false;
#endif

void fiStream::SetFaultInjectionPath( const char* path ) 
{
	sm_FaultInjectionPath = path;
}

void fiStream::SetOpenCallback(StreamOpenCallback callback) 
{
	sm_OpenCallback = callback;
}

void fiStream::SetReadCallback(StreamReadCallback callback) 
{
	sm_ReadCallback = callback;
}

void fiStream::SetCreateCallback(StreamCreateCallback callback) 
{
	sm_CreateCallback = callback;
}

void fiStream::SetCloseCallback(StreamCloseCallback callback) 
{
	sm_CloseCallback = callback;
}

static int sMaxIndex = - 1;

#if __WIN32PC
#pragma warning(disable: 4715)
#endif

/*
PURPOSE
	Allocate a stream.
PARAMS
	filename - file name for __DEV & __TOOL use only.
	fd -
	methods - the methods to be used.
RETURNS
	the pointer to the stream.
NOTES
*/
static sysCriticalSectionToken s_AllocStream;
fiStream* fiStream::AllocStream(const char *
#if STREAM_STORE_NAME
								filename
#endif
								,fiHandle fd,const fiDevice &device) {
	
	// Prevent thread conflicts via critical section
	sysCriticalSection cs(s_AllocStream);

	for (int i=0; i<MaxStreams; i++)
		if (sm_Streams[i].m_Device == 0) {
			sm_Streams[i].m_Handle = fd;
			sm_Streams[i].m_Device = &device;
			sm_Streams[i].m_Size = BufferSize;
			sm_Streams[i].m_Start = 0;
			sm_Streams[i].m_Offset = 0;
			sm_Streams[i].m_Length = 0;
			sm_Streams[i].m_Buffer = sm_Buffers[i];
#if STREAM_STORE_NAME
			strncpy(sm_Streams[i].m_Name,filename,NameLength-1);
#endif
			if (i > sMaxIndex) 
				sMaxIndex = i;
			return sm_Streams + i;
		}

		DumpOpenFiles();
		Quitf(ERR_SYS_NOFILEHANDLES_1,"Out of file handles.");
		return 0;
}

#if __WIN32PC
#pragma warning(error: 4715)
#endif

bool fiStream::HasAvailableStream()
{
	// Prevent thread conflicts via critical section
	sysCriticalSection cs(s_AllocStream);

	for(int i = 0; i < MaxStreams; i++)
	{
		if (sm_Streams[i].m_Device == 0)
		{
			return true;
		}
	}
	return false;
}

void fiStream::CloseOpenFiles() {
#if !__FINAL
	// Displayf("Max files open at once: %d",sMaxIndex + 1);

	for (int i=0; i<MaxStreams; i++) {
		if (sm_Streams[i].m_Device == NULL)
			continue;
		fiStream &S = sm_Streams[i];
		fileDisplayf("closing '%s'",S.GetName());
		S.Close();
	}
#endif
}

void fiStream::DumpOpenFiles(const char * NOTFINAL_ONLY(linePrefix)) {
#if !__FINAL
	// Displayf("Max files open at once: %d",sMaxIndex + 1);

	for (int i=0; i<MaxStreams; i++) {
		if (sm_Streams[i].m_Device == NULL)
			continue;
		OUTPUT_ONLY(fiStream &S = sm_Streams[i]);
#if STREAM_STORE_NAME
		fileDisplayf("%s%d. HANDLE=%" HANDLEFMT " NAME=%s",linePrefix, i,S.m_Handle,S.m_Name);
#else
		fileDisplayf("%s%d. HANDLE=%" HANDLEFMT,linePrefix, i,S.m_Handle);
#endif
	}
#endif
}

PARAM(missingfilesarefatal,"[system] If set, any file open failure is considered a fatal abort.");

#if RSG_PC || !__NO_OUTPUT
static fiStream* FailOpen(const char *filename) {
#else
static fiStream* FailOpen(const char *) {
#endif // RSG_PC || !__NO_OUTPUT
	if (PARAM_missingfilesarefatal.Get())
		Quitf(ERR_STR_MISSING,"Missing filename: %s",filename);
	return NULL;
}

fiStream* fiStream::Open(const char *filename,bool readOnly) {
	const fiDevice *device = fiDevice::GetDevice(filename,readOnly);
	if (!device)
		return FailOpen(filename);

	if(!Verifyf(AreBlockingLoadsAllowed() || device == &fiDeviceMemory::GetInstance(), "There's a blocking load going on (file '%s'). They are not allowed right now, probably because you're risking a TRC/TCR failure by doing so.", filename))
		fileWarningf("There's a blocking load going on (file '%s'). They are not allowed right now, probably because you're risking a TRC/TCR failure by doing so.", filename);

#if __ASSERT
	// SWIFI : Inject a fault so that certain folders cannot load any files
	if ( sm_FaultInjectionPath && 
		std::equal( sm_FaultInjectionPath, sm_FaultInjectionPath + strlen( sm_FaultInjectionPath ), filename ) )
	{
		return FailOpen(filename);
	}
#endif

	fiHandle fd=device->Open(filename,readOnly);

#if __WIN32PC && __DEV
	if(m_bOutputAllFileAccess)
	{
		fileDisplayf("%s%s: %s", (fiIsValidHandle(fd)?"Successfully opened ":"Failed to open "), (readOnly? "as readonly " : ""), filename);
	}
#endif

	if (fiIsValidHandle(fd) && sm_OpenCallback && !(*sm_OpenCallback)(filename, readOnly, fd))
	{
		fileDebugf3("fiStream::Open(%s,%d) %s - OpenCallback Failed", filename, readOnly, fiIsValidHandle(fd) ? "ok" : "failed");

		device->Close(fd);
		return FailOpen(filename);
	}
	if (fiIsValidHandle(fd))
	{
		fiStream* stream = AllocStream(filename, fd, *device);
		fileDebugf3("fiStream::Open(%s,%d) %s - AllocStream: %s", filename, readOnly, fiIsValidHandle(fd) ? "ok" : "failed", stream ? "ok" : "failed");	
		return stream;
	}
	else
	{
		fileDebugf3("fiStream::Open(%s,%d) %s - Invalid Handle", filename, readOnly, fiIsValidHandle(fd) ? "ok" : "failed");
		return FailOpen(filename);
	}
}

fiStream* fiStream::fdopen(const char *filename,fiHandle handle)
{
	return AllocStream(filename,handle,fiDeviceLocal::GetInstance());
}

fiStream* fiStream::Create(const char *filename) 
{
	const fiDevice *device = fiDevice::GetDevice(filename,false);
	if (!device)
	{
		return NULL;
	}

	if (!HasAvailableStream())
	{
		DumpOpenFiles();
		Quitf(ERR_SYS_NOFILEHANDLES_2,"Out of file handles.");
	}

	fiHandle fd = device->Create(filename);
#if __WIN32PC && __DEV
	if(m_bOutputAllFileAccess)
	{
		fileDisplayf("%s: %s", (fiIsValidHandle(fd)?"Successfully created ":"Failed to create "), filename);
	}
#endif
	fileDebugf3("fiStream::Create(%s) %s", filename, fiIsValidHandle(fd) ? "ok" : "failed");
	if (sm_CreateCallback)	(*sm_CreateCallback)(filename, fd);
	return fiIsValidHandle(fd)? AllocStream(filename,fd,*device) : 0;
}


fiStream* fiStream::Open(const char *filename,const fiDevice &device,bool readOnly) {
	fiHandle fd = device.Open(filename,readOnly);
#if __WIN32PC && __DEV
	if(m_bOutputAllFileAccess)
	{
		fileDisplayf("%s%s: %s", (fiIsValidHandle(fd)?"Successfully opened ":"Failed to open "), (readOnly? "as readonly " : ""), filename);
	}
#endif
	fileDebugf3("fiStream::Open(%s,%d) %s", filename, readOnly, fiIsValidHandle(fd) ? "ok" : "failed");
	if (fiIsValidHandle(fd) && sm_OpenCallback && !(*sm_OpenCallback)(filename, readOnly, fd))
	{
		device.Close(fd);
		return FailOpen(filename);
	}
	return fiIsValidHandle(fd)? AllocStream(filename,fd,device) : 0;
}

fiStream* fiStream::Create(const char *filename,const fiDevice &device) 
{
	if (!HasAvailableStream())
	{
		DumpOpenFiles();
		Quitf(ERR_SYS_NOFILEHANDLES_3,"Out of file handles.");
	}

	fiHandle fd = device.Create(filename);
#if __WIN32PC && __DEV
	if(m_bOutputAllFileAccess)
	{
		fileDisplayf("%s: %s", (fiIsValidHandle(fd)?"Successfully created ":"Failed to create "), filename);
	}
#endif
	fileDebugf3("fiStream::Create(%s, device) %s", filename, fiIsValidHandle(fd) ? "ok" : "failed");
	if(sm_CreateCallback)	(*sm_CreateCallback)(filename, fd);
	return fiIsValidHandle(fd)? AllocStream(filename,fd,device) : 0;
}

int fiStream::Read(void *dest,int count) {
	int copied = 0;

	// If last operation was a write, make sure it gets flushed
	// (note that an EOF read will look like this too, although
	// it's harmless)
	if (!m_Length && m_Offset)
		if (Flush() < 0)
			return -1;

	if (sm_ReadCallback)
		sm_ReadCallback(m_Handle, count);

	int amtread = 0;

	// Is everything we need already in the buffer?
	if (count > m_Length - m_Offset) {	// No...
		/* copy what we can */
		if (m_Length != m_Offset) {
			sysMemCpy(dest,m_Buffer + m_Offset,m_Length - m_Offset);
			dest = (void*) ((char*)dest + (m_Length - m_Offset));
			amtread += (m_Length - m_Offset);
			copied += (m_Length - m_Offset);
			count -= (m_Length - m_Offset);
			m_Offset = m_Length;
		}
		m_Start += m_Offset;

		/* if this is (still) a large read, don't bother going through
			the buffer, just copy it directly into caller's
			buffer */
		if (count >= m_Size) {
			int thisread = m_Device->Read(m_Handle,dest,count);
#ifdef DEBUG
			fileDisplayf("Read (big) wanted %d, got %d",count,thisread);
#endif
			if (thisread < 0) {
				m_Length = m_Offset = 0;
				return -1;
			}
			copied += thisread;
#ifdef DEBUG
			HexDump(dest,thisread);
#endif
			m_Start += thisread;
			amtread += thisread;
			m_Offset = m_Length = 0;
			return amtread;
		}

		/* otherwise try to fill the buffer */
		m_Length = m_Device->Read(m_Handle,m_Buffer,m_Size);
#ifdef DEBUG
		fileDisplayf("Read (refill) wanted %d, got %d",size,length);
		HexDump(buffer,m_Length);
#endif
		m_Offset = 0;
		if (m_Length < 0) {
			m_Length = m_Offset = 0;
			return -1;
		}
		else
			amtread += m_Length;

		/* and let common code handle the rest */
	}

	/* at this point, we've either already got enough stuff in the
		buffer or we hit eof */
	if (count > m_Length - m_Offset)
		count = m_Length - m_Offset;

	sysMemCpy(dest,m_Buffer + m_Offset,count);
	copied += count;
	m_Offset += count;
	amtread += count;

	return copied;
}


int fiStream::Write(const void *src,int count) {
	int copied = 0;
	int original_request=count;

	// Was our last operation a successful read?
	if (m_Length)
		if (Flush() < 0)
			return -1;

	/* if this is a large write, don't bother trying to buffer it */
	if (count >= m_Size) {
		if ((copied=Flush()) < 0)
			return -1;
#ifdef DEBUG
		fileDisplayf("Write (big), want %d",count);
		HexDump(src,count);
#endif
		int written = m_Device->Write(m_Handle,src,count);
		if (written < 0)
			return -1;
		else {
			m_Start += written;
			return written;
		}
	}

	/* otherwise, if it's split (or exactly fits), fill the buffer
		before issuing the write */
	if (count >= m_Size - m_Offset) {
		/* copy into buffer */
		sysMemCpy(m_Buffer + m_Offset,src,m_Size - m_Offset);
		src = (void*)((char*)src + (m_Size - m_Offset));
		copied += (m_Size - m_Offset);
		count -= (m_Size - m_Offset);
		m_Offset = m_Size;
		int flushed = Flush();
		if (flushed < 0)
			return -1;
		copied += flushed;
	}

	/* if we get this far, we're guaranteed to fit. */
	sysMemCpy(m_Buffer + m_Offset,src,count);
	copied += count;
	m_Offset += count;

	//return copied;
	return original_request;
}


int fiStream::GetCh() {
	unsigned char ch;
	return Read(&ch,1) == 1? ch : -1;
}

int fiStream::PutCh(unsigned char ch) {
	return Write(&ch,1) == 1? ch : -1;
}

int fiStream::Seek(int newPos) {
	// TODO if (!m_Methods->seek)
	//	return offset = newPos;

	if ((u64)newPos >= m_Start && (u64)newPos < m_Start + m_Length)
	{
		m_Offset = int(newPos - m_Start);
		return newPos;
	}

	if (Flush() < 0)
		return -1;

	m_Start = newPos;
	int rv = int(m_Device->Seek64(m_Handle,newPos,seekSet));
	return rv;
}


u64 fiStream::Seek64(u64 newPos) {
	// TODO if (!m_Methods->seek)
	//	return offset = newPos;

	if (newPos >= m_Start && newPos < m_Start + m_Length)
	{
		m_Offset = int(newPos - m_Start);
		return newPos;
	}

	if (Flush() < 0)
		return (u64)(s64)-1;

	u64 rv = m_Device->Seek64(m_Handle,m_Start = newPos,seekSet);
	return rv;
}


int fiStream::TellStart() {
	return int(m_Start);
}

int fiStream::Tell() {
	return int(m_Start + m_Offset);
}

u64 fiStream::Tell64() {
	return m_Start + m_Offset;
}

int fiStream::Close() {
	if (!m_Length && m_Offset) 
		Flush();	// if unwritten data, flush it
	if (sm_CloseCallback)
		sm_CloseCallback(GetName(), *this);
	m_Device->Close(m_Handle);
	m_Handle = fiHandleInvalid;
	m_Device = 0;	// mark entry as free
	return 0;
}

void fiStream::Sanitize()
{
	sysMemSet(m_Buffer, 0, m_Length);
	m_Device->Sanitize(m_Handle);
}


int fiStream::CloseShared() {
	if (!m_Length && m_Offset) 
		Flush();	// if unwritten data, flush it
	if (sm_CloseCallback)
		sm_CloseCallback(GetName(), *this);
	// m_Device->Close(m_Handle); 
	m_Handle = fiHandleInvalid;
	m_Device = 0;	// mark entry as free
	return 0;
}


int fiStream::Size() {
	return m_Device->Size(m_Handle);
}


u64 fiStream::Size64() {
	return m_Device->Size64(m_Handle);
}


int fiStream::Flush() {
	int rv = 0;

	// Anything still in the buffer that we haven't already consumed
	// needs to get thrown out.
	if (m_Length) {
		/* special case: we've read everything available; don't try the
			seek in case it's a pipe (or tcp stream) */
		if (m_Length == m_Offset /* || !m_Methods->seek TODO */)
			rv = 0;
		else 
		{
			rv = (int) m_Device->Seek64(m_Handle,m_Start + m_Offset,seekSet);
		}
#ifdef DEBUG
		fileDisplayf("Flush.RawSeek %d == %d", m_Start + m_Offset, rv);
#endif
	}
	else if (m_Offset) {	// Unwritten data in the buffer
		rv = m_Device->SafeWrite(m_Handle,m_Buffer,m_Offset);//rv = m_Device->Write(m_Handle,m_Buffer,m_Offset);
#ifdef DEBUG
		fileDisplayf("Flush.RawWrite %d",rv);
		HexDump(buffer,rv);
#endif
	}

	// Reflect new position in file.
	m_Start += m_Offset;

	// Indicate buffers are clear
	m_Offset = m_Length = 0;

	rv = m_Device->Flush(m_Handle);

	return rv;
}



#if __BE
inline unsigned short SWAPS(unsigned short s) {
	return (s << 8) | (s >> 8);
}
inline unsigned SWAPL(unsigned s) {
	return (s >> 24) | ((s >> 8) & 0xFF00) | ((s << 8) & 0xFF0000) | (s << 24);
}
inline u64 SWAP64(u64 s) {
	const u64 hi = SWAPL((u32)(s >> 32));
	const u64 lo = SWAPL((u32)s);
	return hi | (lo << 32);
}

int fiStream::ReadShort(unsigned short *dest,int num) {
	int ct = Read(dest, num * 2) >> 1;
	for (int i=0; i<ct; i++)
		dest[i] = SWAPS(dest[i]);
	return ct;
}

#if __WIN32PC
int fiStream::ReadTChar(_TCHAR *dest,int num) {
	if (sizeof(_TCHAR) == sizeof(short))
	{
		return ReadShort((u16*)dest,num);
	}
	else
	{
		return ReadByte(dest,num);
	}
}
#endif

int fiStream::ReadInt(unsigned *dest,int num) {
	int ct = Read(dest, num * 4) >> 2;
	for (int i=0; i<ct; i++)
		dest[i] = SWAPL(dest[i]);
	return ct;
}

int fiStream::ReadLong(u64 *dest,int num) {
	int ct = Read(dest, num * 8) >> 3;
	for (int i=0; i<ct; i++)
		dest[i] = SWAP64(dest[i]);
	return ct;
}

int fiStream::ReadLong(s64 *dest,int num) {
	return ReadLong((u64*)dest,num);
}

int fiStream::WriteShort(const unsigned short *src,int num) {
	int amt = 0;
	for (int i=0; i<num; i++) {
		unsigned short s = SWAPS(src[i]);
		amt += (Write(&s,2) >> 1);
	}
	return amt;
}

#if __WIN32PC
int fiStream::WriteTChar(const _TCHAR *src,int num) {
	if (sizeof(_TCHAR) == sizeof(short))
	{
		return WriteShort((u16*)src,num);
	}
	else
	{
		return WriteByte(src,num);
	}
}
#endif

int fiStream::WriteInt(const unsigned *src,int num) {
	int amt = 0;
	for (int i=0; i<num; i++) {
		unsigned l = SWAPL(src[i]);
		amt += (Write(&l,4) >> 2);
	}
	return amt;
}

int fiStream::WriteLong(const u64 *src,int num) {
	int amt = 0;
	for (int i=0; i<num; i++) {
		u64 l = SWAP64(src[i]);
		amt += (Write(&l,8) >> 3);
	}
	return amt;
}

int fiStream::WriteLong(const s64 *src,int num) {
	return WriteLong((const u64*)src,num);
}
#endif




void rage::fprintf(fiStream* file,const char *fmt,...) {
	static sysCriticalSectionToken s_bufCritSecTok;
	static char s_buf[2048];
	const sysCriticalSection lock(s_bufCritSecTok);
    va_list args;
    va_start(args,fmt);
	vformatf(s_buf,sizeof(s_buf),fmt,args);
	int sl = StringLength(s_buf);
	file->Write(s_buf,sl);
    va_end(args);
}


/* fiStream *fopen(const char *nm,const char *mode) {
    int ro;
    if (mode[0] == 'r') {
        ro = (mode[1]!='+');
        return fiStream::Open(nm);
    }
    else
        return fiStream::Create(nm);
} */


int rage::fseek(fiStream *b, int offset, int whence) {
    if (whence == 0 /* SEEK_SET */)
        return b->Seek(offset);
    else if (whence == 1 /* SEEK_CUR */)
        return b->Seek(offset + b->Tell());
    else if (whence == 2 /* SEEK_SET */)
        return b->Seek(offset + b->Size());
    else
        return -1;
}

int rage::fseeko(fiStream *b, off_t offset, int whence)
{
	if (whence == 0 /* SEEK_SET */)
		return (b->Seek64((u64)offset) == (u64)-1) ? -1 : 0;
	else if (whence == 1 /* SEEK_CUR */)
		return (b->Seek64((u64)offset + b->Tell()) == (u64)-1) ? -1 : 0;
	else if (whence == 2 /* SEEK_SET */)
		return (b->Seek64(offset + b->Size()) == (u64)-1) ? -1 : 0;
	else
		return -1;
}

int rage::fgets(char *dest,int maxSize,fiStream *S) {
    int ch, stored = 0;
    --maxSize;
    while (maxSize>0 && ((ch = S->FastGetCh()) != -1)) {
        *dest++ = (char) ch;
        maxSize--;
        stored++;
        if (ch == '\n')
            break;
    }
    *dest = 0;
    return stored;
}


int rage::fgetline(char *dest,int maxSize,fiStream *S) {
	int stored;
	do {
		stored = fgets(dest,maxSize,S);
		if (stored == 0)
			break;			// real EOF
		while (stored && dest[stored-1] <= 32)
			--stored;
		dest[stored] = 0;
		// consume empty lines:
	} while (!stored);
	return stored;
}


fiStream* fiStream::PreLoad(fiStream *S) {
	if (!S)
		return 0;

	unsigned size = S->Size();
	sysMemStartTemp();
	char *myBuffer = rage_new char[size];
	sysMemEndTemp();
	S->Read(myBuffer, size);

	char buf[RAGE_MAX_PATH];
	fiDevice::MakeMemoryFileName(buf,sizeof(buf),myBuffer,size,true,S->GetName());
	S->Close();
	S = fiStream::Open(buf,fiDeviceMemory::GetInstance(),true);
	if (!S)
	{
		delete[] myBuffer;
	}
	return S;
}

#if __WIN32PC && __DEV
void fiStream::OutputAllFileAccess()
{
	m_bOutputAllFileAccess = true;
}
#else
void fiStream::OutputAllFileAccess()
{
}
#endif


#undef SEPARATOR
#undef SEPARATOR_STRING
#undef OTHER_SEPARATOR
