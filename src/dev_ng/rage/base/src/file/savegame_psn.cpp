// 
// file/savegame_psn.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __PPU
#include <sdk_version.h>
#if CELL_SDK_VERSION >= 0x150004

#ifdef __SNC__
#pragma diag_suppress 828
#endif

#include "savegame.h"

#include "system/ipc.h"
#include "system/timer.h"
#if HACK_GTA4
#include "system/param.h"	//	required for -hddfreespace
#endif	//	HACK_GTA4
#include "string/string.h"

#include <sysutil/sysutil_savedata.h>
#include <cell/sysmodule.h>
#include "system/memops.h"

/*
From the docs (shown on debugger console when something goes wrong)

1 Arguments Invalid version of save data format 
2 Arguments Directory name is NULL 
3 Arguments Directory name format is invalid 
4 Arguments Directory name is too long or too short 
5 Arguments Output mode of error dialog is invalid 
6 CellSaveDataSetBuf Pointer is NULL even though buffer size is 1 or more 
7 CellSaveDataSetBuf Insufficient buffer size 
8 CellSaveDataSetBuf Maximum number of directory lists to obtain is too large 
9 CellSaveDataSetBuf Maximum number of file lists to obtain is too large 
10 CellSaveDataSetBuf Reserved area is not NULL 
11 Arguments CellSaveDataSetList structure is not specified with NULL 
12 CellSaveDataSetList Sort type is invalid 
13 CellSaveDataSetList Sort order is invalid 
14 CellSaveDataSetList Reserved area is not NULL 
15 CellSaveDataSetList Directory prefix is NULL 
16 CellSaveDataSetList Directory prefix format is invalid 
17 CellSaveDataSetList Directory prefix is too long or too short 
18 Arguments Data list callback function is specified to NULL 
19 Arguments Fixed data callback function is specified to NULL 
20 Arguments Data status callback function is specified to NULL 
21 Arguments File operation callback function is specified to NULL 
22 CellSaveDataCBResult Result status value is invalid 
23 CellSaveDataCBResult Reserved area is not NULL 
24 CellSaveDataCBResult Error message format is invalid 
25 CellSaveDataCBResult Error message is too long 
26 CellSaveDataFixedSet Directory name is NULL 
27 CellSaveDataFixedSet Directory name format is invalid 
28 CellSaveDataFixedSet Directory name is too long or too short 
29 CellSaveDataFixedSet Reserved area is not NULL 
30 CellSaveDataNewDataIcon Title name format is invalid 
31 CellSaveDataNewDataIcon Title name is too long 
32 Unassigned - 
33 CellSaveDataNewDataIcon Reserved area is not NULL 
34 CellSaveDataListSet Setting of initial focus is invalid 
35 CellSaveDataListSet Directory name of initial focus is NULL 
36 CellSaveDataListSet Directory name format of initial focus is invalid 
37 CellSaveDataListSet Directory name of initial focus is too long 
38 CellSaveDataListSet Number of list items is too large 
39 CellSaveDataListSet Array pointer of list is NULL 
40 CellSaveDataListSet Directory name format of an item in the list is invalid 
41 CellSaveDataListSet Directory name of an item in the list is too long 
42 CellSaveDataListSet Reserved area is not NULL 
43 CellSaveDataNewData Setting of position in list is invalid 
44 CellSaveDataNewData Directory name is NULL 
45 CellSaveDataNewData Directory name format is invalid 
46 CellSaveDataNewData Directory name is too long 
47 CellSaveDataNewData Reserved area is not NULL 
48 CellSaveDataStatSet Setting of delete files first is invalid 
49 CellSaveDataStatSet Reserved area is not NULL 
50 CellSaveDataStatSet PARAM.SFO content is undefined 
51 CellSaveDataSystemFileParam Title name format is invalid 
52 CellSaveDataSystemFileParam Title name is too long 
53 CellSaveDataSystemFileParam Subtitle name format is invalid 
54 CellSaveDataSystemFileParam Subtitle name is too long 
55 CellSaveDataSystemFileParam Detailed information format is invalid 
56 CellSaveDataSystemFileParam Detailed information is too long 
57 CellSaveDataSystemFileParam Value of usage conditions is invalid 
58 CellSaveDataSystemFileParam Value of restricted audience age level is invalid 
59 CellSaveDataSystemFileParam Reserved area is not filled with 0s 
60 CellSaveDataFileSet Instruction of file operation is invalid 
61 CellSaveDataFileSet File type is invalid 
62 CellSaveDataFileSet Value of encryption ID of protected data file is invalid 
63 Unassigned - 
64 CellSaveDataFileSet Content information file: Size of still-image icon is too large 
65 CellSaveDataFileSet Content information file: Size of animated icon is too large 
66 CellSaveDataFileSet Content information file: Size of background image is too large 
67 CellSaveDataFileSet Content information file: Size of BGM is too large 
68 CellSaveDataFileSet Reserved area is not NULL 
69 CellSaveDataFileSet Filename is NULL 
70 CellSaveDataFileSet Filename format is invalid 
71 CellSaveDataFileSet Filename is too long 
72 CellSaveDataFileSet Insufficient buffer size 
73 CellSaveDataFileSet Pointer is NULL even though buffer size is 1 or more 
74 Arguments CellSaveDataSetBuf structure is not specified with NULL 
75 CellSaveDataDirList Reserved area is not filled with 0 
76 CellSaveDataDirList List parameter format is invalid 
77 CellSaveDataDirList List parameter is too long 
78 Arguments Memory container ID is invalid 
79 Arguments Memory container is not empty (it is being used for another purpose) 
80 Arguments Memory container size is too small
*/


#if CELL_SDK_VERSION >= 0x420001
	#define RAGE_CELL_SAVEGAME_VERSION CELL_SAVEDATA_VERSION_420
#else // CELL_SDK_VERSION >= 0x420001
	#define RAGE_CELL_SAVEGAME_VERSION CELL_SAVEDATA_VERSION_CURRENT
#endif // CELL_SDK_VERSION >= 0x420001


#if HACK_GTA4
PARAM(hddfreespace, "Fake the amount of free space available on the hard disk");
#endif	//	HACK_GTA4

#if !__NO_OUTPUT
static char *SaveGameGetErrorString(int err)
{
	switch (err)
	{
	case CELL_SAVEDATA_ERROR_CBRESULT: return "Callback function returned an error";
	case CELL_SAVEDATA_ERROR_ACCESS_ERROR: return "HDD access error";
	case CELL_SAVEDATA_ERROR_INTERNAL: return "Fatal internal error";
	case CELL_SAVEDATA_ERROR_PARAM: return "Error in parameter to be set to utility (application bug)";
	case CELL_SAVEDATA_ERROR_NOSPACE: return "Insufficient free space (application bug: lack of free space must be judged and handled within the callback function)";
	case CELL_SAVEDATA_ERROR_BROKEN: return "Save data corrupted (modification detected, etc.)";
	case CELL_SAVEDATA_ERROR_FAILURE: return "Save/load of save data failed (file could not be found, etc.)";
	case CELL_SAVEDATA_ERROR_BUSY: return "Save data utility function was called simultaneously";
	}
	static char buf[32];
	rage::formatf(buf,sizeof(buf),"Error %x",err);
	return buf;
}
#endif


namespace rage {

const u32 s_Container = SYS_MEMORY_CONTAINER_ID_INVALID;

#if HACK_GTA4
#define MAX_CHARS_IN_TITLE_ID_FOR_SAVES_FOLDER	(16)	//	should never actually need more than 10 (9 + the null character)
static char s_TitleIdForSavesFolder[MAX_CHARS_IN_TITLE_ID_FOR_SAVES_FOLDER];
#else	//	HACK_GTA4
extern char *XEX_TITLE_ID;
#endif	//	HACK_GTA4

const char FileName[CELL_SAVEDATA_FILENAME_SIZE] = "RAGE.SAV";

#define SAVEGAME_STACK_SIZE	16384
#define SAVEGAME_STACK_SIZE_FOR_ENUMERATION	(20480)

char *pIconImage = NULL;
u32 SizeOfIconImage = 0;

u32 s_FileProgress = 0;

#if HACK_GTA4
//	Changelist 129860 by Adam
//	Implemented savegame state and also assert on calling the wrong functions based on what the savegame state is
fiSaveGameState::State g_SaveGameState=fiSaveGameState::IDLE;
#endif	//	HACK_GTA4

fiSaveGame SAVEGAME;

void fiSaveGame::InitClass()
{
}

void fiSaveGame::ShutdownClass()
{
}

static CellSaveDataSetList s_setList;
static CellSaveDataSetBuf s_setBuf;
static volatile bool s_Done, s_IsValid, s_FileExists;
static volatile int s_EnumCount;
static sysIpcThreadId s_ThreadId = sysIpcThreadIdInvalid;
#pragma diag_suppress 552
static const char *s_LastThreadOwner;
static fiSaveGame::Content *s_Content;
static int s_ContentMax;
static int s_LastError = CELL_SAVEDATA_RET_OK;
static int s_MaxNumberOfDirectories = 16;
static bool s_bCheckForUserBindErrors = false;
static char s_UserBindErrorMsg[CELL_SAVEDATA_INVALIDMSG_MAX];

static void InitSetBuf(bool bSetFileListMaxToZero = false)
{
#if HACK_GTA4	//	Changelist 136639 - I think this was so that we could use cellSaveDataListAutoLoad to get the most recent save file.
	s_setList.sortType = CELL_SAVEDATA_SORTTYPE_MODIFIEDTIME;
	s_setList.sortOrder = CELL_SAVEDATA_SORTORDER_DESCENT;
	s_setList.dirNamePrefix = s_TitleIdForSavesFolder;
#else	//	HACK_GTA4
	s_setList.dirNamePrefix = XEX_TITLE_ID;
#endif	//	HACK_GTA4
	s_setList.reserved = NULL;
	
	s_setBuf.dirListMax = s_MaxNumberOfDirectories;
	if (bSetFileListMaxToZero)
	{
		s_setBuf.fileListMax = 0;
	}
	else
	{
		s_setBuf.fileListMax = 16;
	}
	memset(s_setBuf.reserved, 0, sizeof(s_setBuf.reserved));
	s_setBuf.bufSize = (s_setBuf.dirListMax * sizeof(CellSaveDataDirList)) + (s_setBuf.fileListMax * sizeof(CellSaveDataFileStat));
	if (!s_setBuf.buf)
		s_setBuf.buf = rage_new char[s_setBuf.bufSize];
}

static void ShutdownSetBuf()
{
	if (s_setBuf.buf)
	{
		delete[] (char*) s_setBuf.buf;
		s_setBuf.buf = NULL;
	}
}

#if HACK_GTA4
void CheckState(fiSaveGameState::State ExpectedState)
{
	Assertf(g_SaveGameState==ExpectedState, "Expected Savegame state to be %d, but it's actually %d", ExpectedState, g_SaveGameState);
}
#endif	//	HACK_GTA4

bool fiSaveGame::BeginSelectDevice(int /*signInId*/,u32 /*contentType*/,u32 /*minSpaceAvailable*/,bool /*forceShow*/,bool /*manageStorage*/) 
{
#if HACK_GTA4
	//	Fix for Bug 201028 - Reset s_LastError to CELL_SAVEDATA_RET_OK in fiSaveGame::BeginGetCreator so that any previous error 
	//	caused by attempting to load a damaged save is forgotten when we start a new load.
	//	I also reset s_LastError in any other Begin... functions that do nothing.
	s_LastError = CELL_SAVEDATA_RET_OK;
#endif	//	HACK_GTA4

	return true;
}

bool fiSaveGame::CheckSelectDevice(int /*signInId*/) 
{
	return true;
}

void fiSaveGame::EndSelectDevice(int /*signInId*/) 
{
}

void fiSaveGame::SetSelectedDevice(int, u32) 
{
}

u32 fiSaveGame::GetSelectedDevice(int) 
{
	return 1;	// same as Xenon Hard Drive
}

#pragma comment(lib,"sysutil_savedata_stub")

static void EnumFixedCallback(CellSaveDataCBResult *cbResult,CellSaveDataListGet *get,CellSaveDataFixedSet *set)
{
	cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;

	if (get->dirNum > get->dirListNum)
	{
		Errorf("EnumFixedCallback - more save game directories than expected");
	}
	s_EnumCount = get->dirListNum;
	if (s_EnumCount > s_ContentMax)
		s_EnumCount = s_ContentMax;
	for (int i=0; i<s_EnumCount; i++) 
	{
		// Skip the product code
		// This isn't really orthogonal, but the filename is a munged form of the original display name.
		safecpy(s_Content[i].Filename, get->dirList[i].dirName + 9, sizeof(s_Content[i].Filename));

#if CELL_SDK_VERSION >= 0x300001
		USES_CONVERSION;
		CellSaveDataDirStat dirStat;
		CellSaveDataSystemFileParam fileParam;
		unsigned bind;
		int sizeKb;
		if (cellSaveDataGetListItem(get->dirList[i].dirName, &dirStat, &fileParam, &bind, &sizeKb) == CELL_OK) {
//			Displayf("Fixed - %llu %s '%s' '%s' '%s'",dirStat.st_mtime,dirStat.dirName,fileParam.title,fileParam.subTitle,fileParam.detail);
			safecpy(s_Content[i].DisplayName,UTF8_TO_WIDE(fileParam.subTitle));
			s_Content[i].ModificationTimeHigh = dirStat.st_mtime >> 32;
			s_Content[i].ModificationTimeLow = dirStat.st_mtime;
		}
		else
#endif
			Errorf("cellSaveDataGetListItem failed.");
	}
}

static void EnumStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	Assert(false);
}

static void EnumFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
	Assert(false);
}


#if HACK_GTA4
#define DIALOG_MODE		CELL_SAVEDATA_ERRDIALOG_ALWAYS
#else	//	HACK_GTA4
#define DIALOG_MODE		CELL_SAVEDATA_ERRDIALOG_NONE
#endif	//	HACK_GTA4

static void s_BeginEnumeration(void*)
{
	s_LastError = cellSaveDataListAutoLoad(RAGE_CELL_SAVEGAME_VERSION,
		DIALOG_MODE,
		&s_setList,
		&s_setBuf,
		EnumFixedCallback,
		EnumStatCallback,
		EnumFileCallback,
		s_Container,
		NULL);
	if (s_LastError)
		Errorf("BeginEnumeration - %s",SaveGameGetErrorString(s_LastError));
	else
		Displayf("BeginEnumeration - Success.");
	s_Done = true;
}


bool fiSaveGame::BeginEnumeration(int /*signInId*/,u32 /*contentType*/,Content * outContent,int maxCount)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::IDLE);
#endif	//	HACK_GTA4

	if (s_ThreadId != sysIpcThreadIdInvalid) {
		Errorf("BeginEnumeration - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}
#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::ENUMERATING_CONTENT;
#endif	//	HACK_GTA4
	if (cellSysmoduleLoadModule(CELL_SYSMODULE_SYSUTIL_SAVEDATA) != CELL_OK) {
		Errorf("BeginEnumeration - cannot load sysutil");
		return false;
	}
	
	InitSetBuf();

	s_EnumCount = 0;
	s_Done = false;
	s_Content = outContent;
	s_ContentMax = maxCount;

	s_ThreadId = sysIpcCreateThread(s_BeginEnumeration,NULL,SAVEGAME_STACK_SIZE_FOR_ENUMERATION,PRIO_NORMAL,"[RAGE] EnumThread");
	s_LastThreadOwner = "s_BeginEnumeration";

	return s_ThreadId != sysIpcThreadIdInvalid;
}

bool fiSaveGame::CheckEnumeration(int /*signInId*/,int &outCount)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::ENUMERATING_CONTENT);
#endif	//	HACK_GTA4

	if(s_Done)
	{
		outCount = s_EnumCount;
#if HACK_GTA4
		g_SaveGameState = fiSaveGameState::ENUMERATED_CONTENT;
#endif	//	HACK_GTA4
		return true;
	}
	return false;
}

void fiSaveGame::EndEnumeration(int /*signInId*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::ENUMERATED_CONTENT);
#endif	//	HACK_GTA4
	cellSysmoduleUnloadModule(CELL_SYSMODULE_SYSUTIL_SAVEDATA);

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::IDLE;
#endif	//	HACK_GTA4
}

#if HACK_GTA4
//	Changelist 130919 by Graeme
//	Added ExtraSpaceCheck. It's almost identical to FreeSpaceCheck but gives a bit more information that I require 
//	when checking if there is enough space for save games and a profile file at the start of a PS3 game.
enum eTypeOfSaveProcess
{
	CHECK_FOR_FREE_SPACE_FOR_THIS_FILE,		//	Returns (hddFreeSizeKB - space required to save this file)
	CHECK_FOR_EXTRA_SPACE_FOR_THIS_FILE,	//	Returns hddFreeSizeKB and space required to save this file
	SAVE_THIS_FILE
};
#endif	//	HACK_GTA4

static char s_DirName[CELL_SAVEDATA_DIRNAME_SIZE];
static char s_FilenameBuf[CELL_SAVEDATA_FILENAME_SIZE];
static char s_DisplayName[CELL_SAVEDATA_SYSP_SUBTITLE_SIZE];
static char s_TitleString[CELL_SAVEDATA_SYSP_TITLE_SIZE];
static void *s_Data;
static u32 s_DataSize;

static int s_SpaceRequired = 0;
#if HACK_GTA4
static eTypeOfSaveProcess s_eSaveProcessToPerform = SAVE_THIS_FILE;
static int s_SpaceRequiredToSaveThisFile = 0;
static int s_TotalHDDFreeSize = 0;
static int s_SizeOfSystemOverheads = 0;
#else	//	HACK_GTA4
static bool s_bCheckingForFreeSpace = false;
#endif	//	HACK_GTA4
static u64 s_hddFreeSizeKB = 0;

static void SaveStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	int DiffSizeKB = 0;
	int NeedSizeKB = 0;

	int NewSizeKB = (s_DataSize + 1023) >> 10;
	Displayf("SaveStatCallback - buffer size %d rounded up to %d", s_DataSize, NewSizeKB);

	Assert(SizeOfIconImage);
	int sizeOfIconFileKB = (SizeOfIconImage + 1023) >> 10;
	Displayf("SaveStatCallback - size of icon file is %dKB", sizeOfIconFileKB);
	Displayf("SaveStatCallback - sysSizeKB = %d", get->sysSizeKB);
	int sizeOfExtraFilesKB = sizeOfIconFileKB + get->sysSizeKB;

	NewSizeKB += sizeOfExtraFilesKB;
	Displayf("SaveStatCallback - Final Size = %d", NewSizeKB);

	/*Displayf("SAVEGAME Free space: %u kb",get->hddFreeSizeKB);
	Displayf("SAVEGAME is new data?: %s",get->isNewData?"yes":"no");
	Displayf("SAVEGAME dir: '%s'",get->dir.dirName);
	Displayf("SAVEGAME title: '%s'",get->getParam.title);
	Displayf("SAVEGAME subtitle: '%s'",get->getParam.subTitle);
	Displayf("SAVEGAME detail: '%s'",get->getParam.detail);
	Displayf("SAVEGAME current size: %u kb",get->sizeKB);
	Displayf("SAVEGAME new size: %u kb",kbNow);*/

	s_FileExists = (get->isNewData == CELL_SAVEDATA_ISNEWDATA_NO);

	if (s_FileExists)
	{	//	If file already exists
		if (NewSizeKB <= get->sizeKB)
		{	//	Since the new save data is smaller than the existing save data, 
			//	there is seen to be sufficient free space and this will be the end of the check.
			DiffSizeKB = 0;
		}
		else
		{	//	Calculate the difference in size between the new save data and the existing save data
			DiffSizeKB = NewSizeKB - get->sizeKB;
		}
	}
	else
	{	//	if file doesn�t already exist
		DiffSizeKB = NewSizeKB;
	}

#if HACK_GTA4
#if !__FINAL
	int freeSpace = 0;
	if(PARAM_hddfreespace.Get(freeSpace))
	{
		s_hddFreeSizeKB = freeSpace;
	}
	else
#endif
#endif	//	HACK_GTA4
	{
		s_hddFreeSizeKB = get->hddFreeSizeKB;//save off for future use
	}

	NeedSizeKB = s_hddFreeSizeKB - DiffSizeKB;


#if HACK_GTA4
	if (CHECK_FOR_FREE_SPACE_FOR_THIS_FILE == s_eSaveProcessToPerform)
#else	//	HACK_GTA4
	if (s_bCheckingForFreeSpace)
#endif	//	HACK_GTA4
	{
		/*Displayf("SAVEGAME hdd free space: %u kb",get->hddFreeSizeKB);
		Displayf("SAVEGAME NewSizeKB: %u kb",NewSizeKB);
		Displayf("SAVEGAME DiffSizeKB: %u kb",DiffSizeKB);*/

		s_SpaceRequired = NeedSizeKB;	//	If this is negative then the player will need to free space
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
	}
#if HACK_GTA4
	else if (CHECK_FOR_EXTRA_SPACE_FOR_THIS_FILE == s_eSaveProcessToPerform)
	{
		if (s_FileExists)
		{	//	If file already exists
			//	Calculate the difference in size between the new save data and the existing save data
			//	This will be negative if the new file is smaller than the existing file
			s_SpaceRequiredToSaveThisFile = NewSizeKB - get->sizeKB;
		}
		else
		{	//	if file doesn't already exist
			s_SpaceRequiredToSaveThisFile = NewSizeKB;
		}

#if !__FINAL
		int freeSpace = 0;
		if(PARAM_hddfreespace.Get(freeSpace))
			s_TotalHDDFreeSize = freeSpace;
		else
#endif
			s_TotalHDDFreeSize = get->hddFreeSizeKB;
		s_SizeOfSystemOverheads = get->sysSizeKB;
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
	}
	else if (SAVE_THIS_FILE == s_eSaveProcessToPerform)
#else	//	HACK_GTA4
	else
#endif	//	HACK_GTA4
	{
		// This will show a Sony UI.  Do we want this?
		if (NeedSizeKB < 0) {
			cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_NOSPACE;
			cbResult->errNeedSizeKB = NeedSizeKB;
		}
		else {
			strcpy(get->getParam.title, s_TitleString);
			strcpy(get->getParam.subTitle, s_DisplayName);
			strcpy(get->getParam.detail, "");
			
			set->setParam = &get->getParam;
#if 0//__FINAL
			set->setParam->attribute = CELL_SAVEDATA_ATTR_NODUPLICATE;
#else
			set->setParam->attribute = CELL_SAVEDATA_ATTR_NORMAL;
#endif
			
#if CELL_SDK_VERSION >= 0x210000
			memset(&set->setParam->reserved2,0,sizeof(set->setParam->reserved2));
#else
			set->setParam->parentalLevel = 1;
#endif

#if HACK_GTA4
//	Need to store the total size of the save game in listParam so that it can be read by ReadNamesAndSizesOfAllSaveGames
			rage::formatf(set->setParam->listParam,sizeof(set->setParam->listParam),"%d_%d", NewSizeKB, sizeOfExtraFilesKB);
#endif	//	HACK_GTA4


			memset(set->setParam->reserved, 0, sizeof(set->setParam->reserved));
#if HACK_GTA4
			set->reCreateMode = CELL_SAVEDATA_RECREATE_YES_RESET_OWNER;	//	Delete existing save data first
#else	//	HACK_GTA4
			set->reCreateMode = CELL_SAVEDATA_RECREATE_YES;	//	Delete existing save data first
#endif	//	HACK_GTA4
#if CELL_SDK_VERSION >= 0x210000
			set->indicator = NULL;
#else
			set->reserved = NULL;
#endif
			// Save is done all at once.
			cbResult->progressBarInc = 100;

			cbResult->result = CELL_SAVEDATA_CBRESULT_OK_NEXT;
		}
	}
}

static const char secureFileId[CELL_SAVEDATA_SECUREFILEID_SIZE] = {
#include "savegame_psn_id.h"
};

static void SaveFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
#if HACK_GTA4
	if (s_eSaveProcessToPerform != SAVE_THIS_FILE)
#else	//	HACK_GTA4
	if (s_bCheckingForFreeSpace)
#endif	//	HACK_GTA4
	{
		Assert(false);
	}
	else
	{
		switch (s_FileProgress)
		{
			case 0 :
			{
				set->fileOperation = CELL_SAVEDATA_FILEOP_WRITE;
				set->reserved = NULL;
				set->fileType = CELL_SAVEDATA_FILETYPE_SECUREFILE;
				sysMemCpy(set->secureFileId, secureFileId, sizeof(set->secureFileId));
				set->fileName = s_FilenameBuf;
				set->fileOffset = 0;
				set->fileSize = set->fileBufSize = s_DataSize;
				set->fileBuf = s_Data;
				cbResult->result = CELL_SAVEDATA_CBRESULT_OK_NEXT;

				s_FileProgress++;
			}
			break;

			case 1 :
			{
				set->fileOperation = CELL_SAVEDATA_FILEOP_WRITE;
				set->reserved = NULL;
				set->fileType = CELL_SAVEDATA_FILETYPE_CONTENT_ICON0;
				set->fileOffset = 0;
				//	Need to have called SetIcon() once before attempting to save a game
				Assert(SizeOfIconImage);
				Assert(pIconImage);
				set->fileSize = set->fileBufSize = SizeOfIconImage;
				set->fileBuf = pIconImage;
				cbResult->result = CELL_SAVEDATA_CBRESULT_OK_NEXT;

				s_FileProgress++;
			}

			break;

			case 2 :
				cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
				break;

			default :
				Assert(false);
				break;
		}
	}
}

static void s_BeginSave(void*)
{
	s_FileProgress = 0;
#if HACK_GTA4
	s_eSaveProcessToPerform = SAVE_THIS_FILE;
#else	//	HACK_GTA4
	s_bCheckingForFreeSpace = false;
#endif	//	HACK_GTA4
	s_LastError = cellSaveDataAutoSave2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
		DIALOG_MODE,	// experimental.  not sure when this will show up.
		&s_setBuf,
		SaveStatCallback,
		SaveFileCallback,
		s_Container,
		NULL);
	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("BeginSave - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("BeginSave - Success.");
	s_Done = true;
}

static void s_BeginFreeSpaceCheck(void*)
{
	s_FileProgress = 0;
#if HACK_GTA4
	s_eSaveProcessToPerform = CHECK_FOR_FREE_SPACE_FOR_THIS_FILE;
#else	//	HACK_GTA4
	s_bCheckingForFreeSpace = true;
#endif	//	HACK_GTA4
	s_SpaceRequired = 0;
	s_LastError = cellSaveDataAutoSave2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
		DIALOG_MODE,	// experimental.  not sure when this will show up.
		&s_setBuf,
		SaveStatCallback,
		SaveFileCallback,
		s_Container,
		NULL);
	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("BeginFreeSpaceCheck - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("BeginFreeSpaceCheck  Success.");
	s_Done = true;
}

#if HACK_GTA4
static void s_BeginExtraSpaceCheck(void*)
{
	s_FileProgress = 0;
	s_eSaveProcessToPerform = CHECK_FOR_EXTRA_SPACE_FOR_THIS_FILE;
	s_SpaceRequiredToSaveThisFile = 0;
	s_TotalHDDFreeSize = 0;
	s_SizeOfSystemOverheads = 0;
	s_LastError = cellSaveDataAutoSave2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
		DIALOG_MODE,	// experimental.  not sure when this will show up.
		&s_setBuf,
		SaveStatCallback,
		SaveFileCallback,
		s_Container,
		NULL);
	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("BeginExtraSpaceCheck - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("BeginExtraSpaceCheck  Success.");
	s_Done = true;
}
#endif	//	HACK_GTA4

static void s_AppendToDirectoryName(char *dest,size_t destSize,const char *displayName) {
	while (--destSize && *displayName) {
		int ch = *displayName++;
		if (ch >= 'a' && ch <= 'z')
			ch -= 32;
		else if ((ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '-')
			;
		else
			ch = '_';
		*dest++ = ch;
	}
	*dest = '\0';
}

#if HACK_GTA4
bool s_BeginSaveOrFreeSpaceCheck(eTypeOfSaveProcess eSaveProcess, int signInId,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize)
#else	//	HACK_GTA4
bool s_BeginSaveOrFreeSpaceCheck(bool bSave, int signInId,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize)
#endif	//	HACK_GTA4
{
	if (s_ThreadId != sysIpcThreadIdInvalid) {
		Errorf("BeginSaveOrFreeSpaceCheck - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}

	InitSetBuf();

	s_DataSize = saveSize;

#if HACK_GTA4
	strcpy(s_DirName, s_TitleIdForSavesFolder);
#else	//	HACK_GTA4
	strcpy(s_DirName, XEX_TITLE_ID);
#endif	//	HACK_GTA4

	Assertf(strlen(s_DirName) == 9, "s_BeginSaveOrFreeSpaceCheck - expected %s to have 9 characters", s_DirName);
	s_AppendToDirectoryName(s_DirName+9,sizeof(s_DirName)-9,filename);

	s_Done = false;
#if HACK_GTA4
	switch (eSaveProcess)
	{
		case CHECK_FOR_FREE_SPACE_FOR_THIS_FILE :
		{
			s_ThreadId = sysIpcCreateThread(s_BeginFreeSpaceCheck,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] FreeSpaceCheckThread");
			s_LastThreadOwner = "s_BeginFreeSpaceCheck";
		}
		break;

		case CHECK_FOR_EXTRA_SPACE_FOR_THIS_FILE :
		{
			s_ThreadId = sysIpcCreateThread(s_BeginExtraSpaceCheck,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] ExtraSpaceCheckThread");
			s_LastThreadOwner = "s_BeginExtraSpaceCheck";
		}
		break;

		case SAVE_THIS_FILE :
	    {
		    s_Data = (void*) saveData;
    
		    USES_CONVERSION;
		    safecpy(s_DisplayName,WIDE_TO_UTF8(displayName),sizeof(s_DisplayName));
		    safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));
    
			s_ThreadId = sysIpcCreateThread(s_BeginSave,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] SaveThread");
		    s_LastThreadOwner = "s_BeginSave";
	    }
		break;
	}
#else	//	HACK_GTA4
	if (bSave)
	{
		s_Data = (void*) saveData;

		USES_CONVERSION;
		safecpy(s_DisplayName,WIDE_TO_UTF8(displayName),sizeof(s_DisplayName));
		safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));

		s_ThreadId = sysIpcCreateThread(s_BeginSave,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] SaveThread");
		s_LastThreadOwner = "s_BeginSave";
	}
	else
	{
		s_ThreadId = sysIpcCreateThread(s_BeginFreeSpaceCheck,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] FreeSpaceCheckThread");
		s_LastThreadOwner = "s_BeginFreeSpaceCheck";
	}
#endif	//	HACK_GTA4

	return s_ThreadId != sysIpcThreadIdInvalid;
}

bool fiSaveGame::BeginSave(int signInId,u32 /*contentType*/,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool /*overwrite*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::IDLE);

	bool rt = s_BeginSaveOrFreeSpaceCheck(SAVE_THIS_FILE, signInId,displayName,filename,saveData,saveSize);
	if(rt)
		g_SaveGameState = fiSaveGameState::SAVING_CONTENT;
	return rt;
#else	//	HACK_GTA4
	return s_BeginSaveOrFreeSpaceCheck(true, signInId,displayName,filename,saveData,saveSize);
#endif	//	HACK_GTA4
}

bool fiSaveGame::CheckSave(int signInId,bool &outIsValid,bool &fileExists)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::SAVING_CONTENT);
#endif	//	HACK_GTA4

	if (s_Done)
	{
		outIsValid = s_IsValid;
		fileExists = s_FileExists;
#if HACK_GTA4
		g_SaveGameState = fiSaveGameState::SAVED_CONTENT;
#endif	//	HACK_GTA4
		return true;
	}
	return false;
}

void fiSaveGame::EndSave(int signInId)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::SAVED_CONTENT);
#endif	//	HACK_GTA4

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::IDLE;
#endif	//	HACK_GTA4
}

bool fiSaveGame::BeginFreeSpaceCheck(int signInId,const char *filename,u32 saveSize)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::IDLE);

	bool rt = s_BeginSaveOrFreeSpaceCheck(CHECK_FOR_FREE_SPACE_FOR_THIS_FILE, signInId,NULL,filename,NULL,saveSize);
	if(rt)
		g_SaveGameState = fiSaveGameState::CHECKING_FREE_SPACE;
	return rt;
#else	//	HACK_GTA4
	return s_BeginSaveOrFreeSpaceCheck(false, signInId,NULL,filename,NULL,saveSize);
#endif	//	HACK_GTA4
}

bool fiSaveGame::CheckFreeSpaceCheck(int signInId, int &ExtraSpaceRequired)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::CHECKING_FREE_SPACE);
#endif	//	HACK_GTA4

	if(s_Done)
	{
		ExtraSpaceRequired = s_SpaceRequired;	//	If this is negative then the player will need to free space
#if HACK_GTA4
		g_SaveGameState = fiSaveGameState::HAVE_CHECKED_FREE_SPACE;
#endif	//	HACK_GTA4
		return true;
	}
	return false;
}

void fiSaveGame::EndFreeSpaceCheck(int signInId)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::HAVE_CHECKED_FREE_SPACE);
#endif	//	HACK_GTA4

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}
#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::IDLE;
#endif	//	HACK_GTA4
}


#if HACK_GTA4
bool fiSaveGame::BeginExtraSpaceCheck(int signInId, const char *filename, u32 saveSize)
{
	CheckState(fiSaveGameState::IDLE);

	bool rt = s_BeginSaveOrFreeSpaceCheck(CHECK_FOR_EXTRA_SPACE_FOR_THIS_FILE, signInId,NULL,filename,NULL,saveSize);
	if(rt)
		g_SaveGameState = fiSaveGameState::CHECKING_EXTRA_SPACE;
	return rt;
}

bool fiSaveGame::CheckExtraSpaceCheck(int signInId, int &ExtraSpaceRequired, int &TotalHDDFreeSizeKB, int &SizeOfSystemFileKB)
{
	CheckState(fiSaveGameState::CHECKING_EXTRA_SPACE);

	if(s_Done)
	{
		ExtraSpaceRequired = s_SpaceRequiredToSaveThisFile;
		TotalHDDFreeSizeKB = s_TotalHDDFreeSize;
		SizeOfSystemFileKB = s_SizeOfSystemOverheads;
		g_SaveGameState = fiSaveGameState::HAVE_CHECKED_EXTRA_SPACE;
		return true;
	}
	return false;
}

void fiSaveGame::EndExtraSpaceCheck(int signInId)
{
	CheckState(fiSaveGameState::HAVE_CHECKED_EXTRA_SPACE);

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

	g_SaveGameState = fiSaveGameState::IDLE;
}
#endif	//	HACK_GTA4


static void LoadStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	Displayf("[LOAD] LoadStatCallback %d",sysTimer::GetSystemMsTime());
	/// Uncommment this to repro the SDK 220.000.03 hang.
	/// cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_BROKEN;
	/// return;

	// If no files found then return no data error
	if(get->fileNum == 0)
	{
		cbResult->progressBarInc = 100;
		cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_NODATA;

		Displayf("[LOAD] LoadStatCallback - no files have been found");
		return;
	}

	Assert(get->fileNum == 2);	//	data and icon
	Assert(get->fileListNum == 2);	//	data and icon	// should I change these to error messages?

	if (s_bCheckForUserBindErrors && (get->bind & (CELL_SAVEDATA_BINDSTAT_ERR_NOUSER|CELL_SAVEDATA_BINDSTAT_ERR_OTHERS) ) )
	{
		cbResult->invalidMsg = s_UserBindErrorMsg;
		cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_INVALID;

		Displayf("[LOAD] LoadStatCallback - user bind error");
	}
	else
	{
		//	Changelist 128447 by Adam
		//	Don't setup setParam in LoadStatCallback as it was saving the file status everytime a load was called.
		set->setParam = NULL;

#if HACK_GTA4
		// HACK to prevent causing a ban on trophy enablement being triggered. 
		// If an old save is loaded, the ban is invoked indefinitely. Starting a new game would not remove the ban, so
		// would prevent trophies being awarded. 
		// Was this only needed for GTA4? Should it be removed now?
		if ((get->bind & CELL_SAVEDATA_BINDSTAT_ERR_NOOWNER) || (get->bind & CELL_SAVEDATA_BINDSTAT_ERR_OWNER))
			set->reCreateMode = CELL_SAVEDATA_DISABLE_TROPHY_OWNERSHIP_CHECK;
#endif	//	HACK_GTA4

		// Load is done all at once.
		cbResult->progressBarInc = 100;
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_NEXT;
	}
}

static void LoadFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
	Displayf("[LOAD] LoadFileCallback %d",sysTimer::GetSystemMsTime());

	if (get->excSize)
	{
		s_DataSize = get->excSize;
		set->fileSize = 0;
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
	}
	else
	{
		set->fileOperation = CELL_SAVEDATA_FILEOP_READ;
		set->reserved = NULL;
		set->fileType = CELL_SAVEDATA_FILETYPE_SECUREFILE;
		sysMemCpy(set->secureFileId, secureFileId, sizeof(set->secureFileId));
		set->fileName = s_FilenameBuf;
		set->fileOffset = 0;
		set->fileSize = set->fileBufSize = s_DataSize;
		set->fileBuf = s_Data;
		// Only one file needs to be read
		// Don't need to bother about the icon file here
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_NEXT;
	}
}

static void s_BeginLoad(void*)
{
	Displayf("[LOAD] s_BeginLoad %d",sysTimer::GetSystemMsTime());
	s_LastError = cellSaveDataAutoLoad2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
#if HACK_GTA4
		CELL_SAVEDATA_ERRDIALOG_NONE,	// experimental.  not sure when this will show up.
#else	//	HACK_GTA4
		DIALOG_MODE,	// experimental.  not sure when this will show up.
#endif	//	HACK_GTA4
		&s_setBuf,
		LoadStatCallback,
		LoadFileCallback,
		s_Container,
		NULL);
	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("[LOAD] s_BeginLoad - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("[LOAD] s_BeginLoad - Success %d",sysTimer::GetSystemMsTime());
	s_Done = true;
}


bool fiSaveGame::BeginLoad(int signInId,u32 /*contentType*/,u32 /*deviceId*/,const char *filename,void *loadData,u32 loadSize)
{
	Displayf("[LOAD] BeginLoad %d",sysTimer::GetSystemMsTime());
	if (s_ThreadId != sysIpcThreadIdInvalid) {
		Errorf("BeginLoad - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}

#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::LOADING_CONTENT;
#endif	//	HACK_GTA4

	InitSetBuf();

	s_Data = (void*) loadData;
	s_DataSize = loadSize;

#if HACK_GTA4
	strcpy(s_DirName, s_TitleIdForSavesFolder);
#else	//	HACK_GTA4
	strcpy(s_DirName, XEX_TITLE_ID);
#endif	//	HACK_GTA4
	Assertf(strlen(s_DirName) == 9, "fiSaveGame::BeginLoad - expected %s to have 9 characters", s_DirName);
	s_AppendToDirectoryName(s_DirName+9,sizeof(s_DirName)-9,filename);
	safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));

	s_Done = false;
	s_ThreadId = sysIpcCreateThread(s_BeginLoad,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] LoadThread");
	s_LastThreadOwner = "s_BeginLoad";

	Displayf("[LOAD] BeginLoad %d",sysTimer::GetSystemMsTime());
	return s_ThreadId != sysIpcThreadIdInvalid;
}


bool fiSaveGame::CheckLoad(int signInId,bool &outIsValid,u32 &loadSize)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::LOADING_CONTENT);
#endif	//	HACK_GTA4

	if (s_Done)
	{
		outIsValid = s_IsValid;
		loadSize = s_DataSize;
#if HACK_GTA4
		g_SaveGameState = fiSaveGameState::LOADED_CONTENT;
#endif	//	HACK_GTA4
		return true;
	}
	return false;
}


void fiSaveGame::EndLoad(int signInId)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::LOADED_CONTENT);
#endif	//	HACK_GTA4

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}
#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::IDLE;
#endif	//	HACK_GTA4
}


static void s_BeginDeleteFromList(void*)
{
	s_LastError = cellSaveDataDelete2(s_Container);
//	s_IsValid = true;
	if (s_LastError != CELL_SAVEDATA_RET_CANCEL)
	{
		Errorf("BeginDeleteFromList - %s",SaveGameGetErrorString(s_LastError));
//		s_IsValid = false;
//	Maybe should deal with errors here - should only be CELL_SAVEDATA_ERROR_INTERNAL or CELL_SAVEDATA_ERROR_BUSY
	}
	else
		Displayf("DeleteFromList finished");
	s_Done = true;
}


bool fiSaveGame::BeginDeleteFromList(int /*signInId*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::IDLE);
#endif	//	HACK_GTA4
	if (s_ThreadId != sysIpcThreadIdInvalid) {
		Errorf("BeginDeleteFromList - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}
#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::DELETING_CONTENT_FROM_LIST;
#endif	//	HACK_GTA4

	InitSetBuf();

	s_Done = false;
	s_ThreadId = sysIpcCreateThread(s_BeginDeleteFromList,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] DeleteFromListThread");
	s_LastThreadOwner = "s_BeginDeleteFromList";

	return s_ThreadId != sysIpcThreadIdInvalid;
}


bool fiSaveGame::CheckDeleteFromList(int /*signInId*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::DELETING_CONTENT_FROM_LIST);
#endif	//	HACK_GTA4

	if(s_Done)
	{
#if HACK_GTA4
		g_SaveGameState = fiSaveGameState::DELETED_CONTENT_FROM_LIST;
#endif	//	HACK_GTA4
		return true;
	}
	return false;
}


void fiSaveGame::EndDeleteFromList(int /*signInId*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::DELETED_CONTENT_FROM_LIST);
#endif	//	HACK_GTA4

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::IDLE;
#endif	//	HACK_GTA4
}


// *********** Delete *************************

static void DeleteSavegameFixedCallback(CellSaveDataCBResult *cbResult, CellSaveDataListGet *get, CellSaveDataFixedSet *set)
{
	// Maybe I should check that s_DirName actually exists in the list inside the CellSaveDataListGet structure

	set->dirName = s_DirName;
	set->newIcon = NULL;
	set->option = CELL_SAVEDATA_OPTION_NOCONFIRM;	//	or CELL_SAVEDATA_OPTION_NONE
	
	cbResult->result = CELL_SAVEDATA_CBRESULT_OK_NEXT;
}


static void DeleteSavegameDoneCallback(CellSaveDataCBResult *cbResult, CellSaveDataDoneGet *get)
{
	cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST_NOCONFIRM;	//	Or CELL_SAVEDATA_CBRESULT_OK_LAST to display a confirmation message
}


static void s_BeginDelete(void*)
{
	s_LastError = cellSaveDataFixedDelete(
		&s_setList,
		&s_setBuf,
		DeleteSavegameFixedCallback,
		DeleteSavegameDoneCallback,
		s_Container,
		NULL);

	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("Savegame s_BeginDelete - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
	{
		Displayf("Savegame Delete finished");
	}
	s_Done = true;
}

bool fiSaveGame::BeginDelete(int /*signInId*/,u32 /*contentType*/, const char *filename)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::IDLE);
#endif	//	HACK_GTA4
	if (s_ThreadId != sysIpcThreadIdInvalid) {
		Errorf("BeginDelete - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}
#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::DELETING_CONTENT;
#endif	//	HACK_GTA4

	InitSetBuf(true);

#if HACK_GTA4
	strcpy(s_DirName, s_TitleIdForSavesFolder);
#else	//	HACK_GTA4
	strcpy(s_DirName, XEX_TITLE_ID);
#endif	//	HACK_GTA4
	Assertf(strlen(s_DirName) == 9, "fiSaveGame::BeginDelete - expected %s to have 9 characters", s_DirName);
	s_AppendToDirectoryName(s_DirName+9,sizeof(s_DirName)-9,filename);
//	safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));

	s_Done = false;
	s_ThreadId = sysIpcCreateThread(s_BeginDelete,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] DeleteSavegameThread");
	s_LastThreadOwner = "s_BeginDelete";

	return s_ThreadId != sysIpcThreadIdInvalid;
}

bool fiSaveGame::CheckDelete(int /*signInId*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::DELETING_CONTENT);
#endif	//	HACK_GTA4

	if(s_Done)
	{
#if HACK_GTA4
		g_SaveGameState = fiSaveGameState::DELETED_CONTENT;
#endif	//	HACK_GTA4
		return true;
	}
	return false;
}

void fiSaveGame::EndDelete(int /*signInId*/)
{
#if HACK_GTA4
	CheckState(fiSaveGameState::DELETED_CONTENT);
#endif	//	HACK_GTA4

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

#if HACK_GTA4
	g_SaveGameState = fiSaveGameState::IDLE;
#endif	//	HACK_GTA4
}


// *********** IconSave *************************

bool fiSaveGame::BeginIconSave(int /*signInId*/,u32 /*contentType*/,const char* /*filename*/,const void* /*iconData*/,u32 /*iconSize*/)
{
#if HACK_GTA4
//	Fix for Bug 201028 - Reset s_LastError to CELL_SAVEDATA_RET_OK in fiSaveGame::BeginGetCreator so that any previous error 
//	caused by attempting to load a damaged save is forgotten when we start a new load. 
//	I also reset s_LastError in any other Begin... functions that do nothing.
	s_LastError = CELL_SAVEDATA_RET_OK;
#endif	//	HACK_GTA4
	return true;
}

bool fiSaveGame::CheckIconSave(int /*signInId*/)
{
	return true;
}

void fiSaveGame::EndIconSave(int /*signInId*/)
{
}

void fiSaveGame::SetIcon(char *pIcon, const u32 SizeOfIconFile)
{
	Assert(pIconImage == NULL);		//	Should only need to call SetIcon() once at the start of the game.
	Assert(SizeOfIconImage == 0);	//	Just remove these two Asserts if that is not the case.
	pIconImage = pIcon;
	SizeOfIconImage = SizeOfIconFile;
}


fiSaveGameState::State fiSaveGame::GetState(int /*signInId*/) const
{
	return fiSaveGameState::IDLE;	// a lie!
}

int fiSaveGame::GetMaxUsers()
{
	return 0;
}

bool fiSaveGame::IsStorageDeviceChanged(int /*signInId*/)
{
	return false;
}

fiSaveGame::Errors fiSaveGame::GetError(int /*signInId*/)
{
	switch (s_LastError)
	{
		case CELL_SAVEDATA_RET_OK :	//	Normal termination
			return SAVE_GAME_SUCCESS;

		case CELL_SAVEDATA_RET_CANCEL :	//	User cancelled operation (normal termination)
			return PS3_ERROR_USER_CANCELLED;

		case CELL_SAVEDATA_ERROR_CBRESULT :	//	Callback function returned an error
			return PS3_ERROR_CALLBACK_ERROR;

		case CELL_SAVEDATA_ERROR_ACCESS_ERROR :	//	HDD access error
			return PS3_ERROR_ACCESS;

		case CELL_SAVEDATA_ERROR_INTERNAL :	//	Fatal internal error
			return PS3_ERROR_INTERNAL;

		case CELL_SAVEDATA_ERROR_PARAM :	//	Error in parameter to be set to utility (application bug)
			return PS3_ERROR_PARAM;

		case CELL_SAVEDATA_ERROR_NOSPACE :	//	Insufficient free HDD space (application bug: lack of free space must be judged and handled within the callback function)
			return NOT_ENOUGH_FREE_SPACE_ON_DEVICE;

		case CELL_SAVEDATA_ERROR_BROKEN :	//	Save data corrupted (modification detected, etc.)
			return FILE_CORRUPT;

		case CELL_SAVEDATA_ERROR_FAILURE :	//	Save/load of save data failed(File could not be found, etc.)
			return PS3_ERROR_FAILURE;

		case CELL_SAVEDATA_ERROR_BUSY :	//	Save data utility function was called simultaneously
			return PS3_ERROR_BUSY;
	}

	return UNKNOWN_ERROR;
}

void fiSaveGame::SetStateToIdle(int /*signInId*/)
{   
}

void fiSaveGame::SetSelectedDeviceToAny(int /*signInId*/)
{
}

bool fiSaveGame::IsCurrentDeviceValid(int /*signInId*/)
{
	return true;
}

bool fiSaveGame::IsStorageDeviceRemoved(int /*signInId*/)
{
	return false;
}

bool fiSaveGame::IsDeviceValid(int signInId, u32 deviceId)
{
	return true;
}

bool fiSaveGame::GetCurrentDeviceData(int /*signInId*/, fiSaveGame::Device */*outDevice*/)
{
	return true;
}

bool fiSaveGame::GetDeviceData(int /*signInId*/, u32 /*deviceId*/, fiSaveGame::Device* /*outDevice*/)
{
	return true;
}

bool fiSaveGame::HasFileBeenAccessed(int , const char* )
{
	return false;
}

u64 fiSaveGame::CalculateDataSizeOnDisk(int,u32 dataSize)
{
	return dataSize + 65536;	// HACK
}

u64 fiSaveGame::GetTotalAvailableSpace(int)
{
	return s_hddFreeSizeKB;		// HACK
}

void fiSaveGame::SetMaxNumberOfSaveGameFilesToEnumerate(int MaxNumSaveGames)
{
	s_MaxNumberOfDirectories = MaxNumSaveGames;
}

void fiSaveGame::SetSaveGameTitle(const char *pSaveGameTitle)
{
	safecpy(s_TitleString, pSaveGameTitle, sizeof(s_TitleString));
}

void fiSaveGame::SetCheckForUserBindErrors(const char *pBindErrorMessage)
{
	safecpy(s_UserBindErrorMsg, pBindErrorMessage, sizeof(s_UserBindErrorMsg));
	s_bCheckForUserBindErrors = true;
}

bool fiSaveGame::BeginContentVerify(int,u32,u32,const char*)
{
#if HACK_GTA4
//	Fix for Bug 201028 - Reset s_LastError to CELL_SAVEDATA_RET_OK in fiSaveGame::BeginGetCreator so that any previous error 
//	caused by attempting to load a damaged save is forgotten when we start a new load. 
//	I also reset s_LastError in any other Begin... functions that do nothing.
	s_LastError = CELL_SAVEDATA_RET_OK;
#endif	//	HACK_GTA4
	return true;
}

bool fiSaveGame::CheckContentVerify(int,bool &verified)
{
	verified = true;
	return true;
}

void fiSaveGame::EndContentVerify(int)
{
}

#if HACK_GTA4

//////////////////////////////////////////////////////////////////////////
static bool s_bHasCreator;
static bool s_bIsCreator;
static void GetCreatorStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	s_bHasCreator = !(get->bind & CELL_SAVEDATA_BINDSTAT_ERR_NOOWNER);
	s_bIsCreator = !(get->bind & CELL_SAVEDATA_BINDSTAT_ERR_NOOWNER) && !(get->bind & CELL_SAVEDATA_BINDSTAT_ERR_OWNER);
	cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
}

static void GetCreatorFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
	Assert(false);
}

static void s_BeginGetCreator(void*)
{
	s_LastError = cellSaveDataAutoLoad2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
		CELL_SAVEDATA_ERRDIALOG_ALWAYS,	// experimental.  not sure when this will show up.
		&s_setBuf,
		GetCreatorStatCallback,
		GetCreatorFileCallback,
		s_Container,
		NULL);
	if (s_LastError)
	{
		Errorf("BeginGetCreator - %s",SaveGameGetErrorString(s_LastError));
	}
	else
		Displayf("Success.");
	s_Done = true;
}


bool fiSaveGame::BeginGetCreator(int signInId, u32 /*contentType*/,u32 /*deviceId*/,const char *filename)
{
	CheckState(fiSaveGameState::IDLE);

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		Errorf("BeginGetCreator - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}

	g_SaveGameState = fiSaveGameState::GETTING_CREATOR;

	InitSetBuf();

	strcpy(s_DirName, s_TitleIdForSavesFolder);
	Assertf(strlen(s_DirName) == 9, "fiSaveGame::BeginGetCreator - expected %s to have 9 characters", s_DirName);
	s_AppendToDirectoryName(s_DirName+9,sizeof(s_DirName)-9,filename);
	safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));

	s_Done = false;
	s_bHasCreator = false;
	s_bIsCreator = false;
	s_ThreadId = sysIpcCreateThread(s_BeginGetCreator,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] GetCreatorThread");
	s_LastThreadOwner = "s_BeginGetCreator";

	return s_ThreadId != sysIpcThreadIdInvalid;
}


bool fiSaveGame::CheckGetCreator(int , bool &bIsCreator, bool &bHasCreator)
{
	CheckState(fiSaveGameState::GETTING_CREATOR);

	if (s_Done)
	{
		bIsCreator = s_bIsCreator;
		bHasCreator = s_bHasCreator;
		g_SaveGameState = fiSaveGameState::GOT_CREATOR;
		return true;
	}
	return false;
}


void fiSaveGame::EndGetCreator(int signInId)
{
	CheckState(fiSaveGameState::GOT_CREATOR);

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

	g_SaveGameState = fiSaveGameState::IDLE;
}

#else	//	HACK_GTA4

bool fiSaveGame::BeginGetCreator(int , u32 , u32 , const char *)
{
#if HACK_GTA4
	//	Fix for Bug 201028 - Reset s_LastError to CELL_SAVEDATA_RET_OK in fiSaveGame::BeginGetCreator so that any previous error 
	//	caused by attempting to load a damaged save is forgotten when we start a new load. 
	//	I also reset s_LastError in any other Begin... functions that do nothing.
	s_LastError = CELL_SAVEDATA_RET_OK;
#endif	//	HACK_GTA4
	return true;
}

bool fiSaveGame::CheckGetCreator(int , bool &bIsCreator)
{
	bIsCreator = true;
	return true;
}

void fiSaveGame::EndGetCreator(int )
{
}

#endif	//	HACK_GTA4

//////////////////////////////////////////////////////////////////////////


void fiSaveGame::FillDevice(int)
{

}



#if HACK_GTA4
//	The listParam string should contain a positive integer - the size of the complete save game file (including the icon and system files)
//	followed by an underscore
//	followed by the size of the icon + the size of the system files
//	If it contains anything else then return 0
static void GetSizeOfFileFromString(const char *pStringContainingInteger, int MaxLengthOfString, int &completeSizeOfFile, int &sizeOfIconAndSystemFiles)
{
	completeSizeOfFile = 0;
	sizeOfIconAndSystemFiles = 0;

	if (!pStringContainingInteger)
	{
		return;
	}

	int CharLoop = 0;
	bool bReadingFullSize = true;
	while ( (CharLoop < MaxLengthOfString) && (pStringContainingInteger[CharLoop] != '\0') )
	{
		if (bReadingFullSize && (pStringContainingInteger[CharLoop] == '_') )
		{
			bReadingFullSize = false;
		}
		else if ( (pStringContainingInteger[CharLoop] >= '0') && (pStringContainingInteger[CharLoop] <= '9') )
		{
			int NewDigit = pStringContainingInteger[CharLoop] - '0';

			if (bReadingFullSize)
			{
				completeSizeOfFile = (completeSizeOfFile * 10) + NewDigit;
			}
			else
			{
				sizeOfIconAndSystemFiles = (sizeOfIconAndSystemFiles * 10) + NewDigit;
			}
		}
		else
		{	//	string contains characters other than numbers so return 0
			Errorf("GetSizeOfFileFromString - listparam contains a non-numerical character");
			return;
		}
		
		CharLoop++;
	}
}

int fiSaveGame::CalculateSizeOfSaveGameFile(int DataSize, int SizeOfSystemFiles)
{
	int NewSizeKB = (DataSize + 1023) >> 10;
	Assert(SizeOfIconImage);
	NewSizeKB += (SizeOfIconImage + 1023) >> 10;

	NewSizeKB += SizeOfSystemFiles;	//	Relies on SizeOfSystemFiles being a standard size for all files

	return NewSizeKB;
}

static int *s_SaveGameSizes;

static void ReadNamesAndSizesOfAllSaveGamesFixedCallback(CellSaveDataCBResult *cbResult,CellSaveDataListGet *get, CellSaveDataFixedSet *set)
{
	if (get->dirNum > get->dirListNum)
	{
		Errorf("ReadNamesAndSizesOfAllSaveGamesFixedCallback - more save game directories than expected");
	}

	int iIgnore = 0;

	s_EnumCount = get->dirListNum;
	if (s_EnumCount > s_ContentMax)
		s_EnumCount = s_ContentMax;
	for (int i=0; i<s_EnumCount; i++) 
	{
		// Skip the product code
		// This isn't really orthogonal, but the filename is a munged form of the original display name.
		safecpy(s_Content[i].Filename, get->dirList[i].dirName + 9, sizeof(s_Content[i].Filename));

		GetSizeOfFileFromString(get->dirList[i].listParam, CELL_SAVEDATA_SYSP_LPARAM_SIZE, s_SaveGameSizes[i], iIgnore);
	}
	
	cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
}

static void ReadNamesAndSizesOfAllSaveGamesStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	Assert(false);
}

static void ReadNamesAndSizesOfAllSaveGamesFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
	Assert(false);
}

static void s_BeginReadNamesAndSizesOfAllSaveGames(void*)
{
	s_LastError = cellSaveDataListAutoLoad(RAGE_CELL_SAVEGAME_VERSION,
		DIALOG_MODE,	// experimental.  not sure when this will show up.
		&s_setList,
		&s_setBuf,
		ReadNamesAndSizesOfAllSaveGamesFixedCallback,
		ReadNamesAndSizesOfAllSaveGamesStatCallback,
		ReadNamesAndSizesOfAllSaveGamesFileCallback,
		s_Container,
		NULL);

	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("BeginReadNamesAndSizesOfAllSaveGames - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("Success.");
	s_Done = true;
}

bool fiSaveGame::BeginReadNamesAndSizesOfAllSaveGames(int signInId, Content *outContent, int *outFileSizes, int maxCount)
{
	CheckState(fiSaveGameState::IDLE);

	if (s_ThreadId != sysIpcThreadIdInvalid)
		return false;

	g_SaveGameState = fiSaveGameState::READING_NAMES_AND_SIZES_OF_ALL_SAVE_GAMES;

	InitSetBuf();

	s_EnumCount = 0;
	s_Done = false;
	s_Content = outContent;
	s_SaveGameSizes = outFileSizes;
	s_ContentMax = maxCount;
	s_ThreadId = sysIpcCreateThread(s_BeginReadNamesAndSizesOfAllSaveGames,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] SizesOfAllSavesThread");
	s_LastThreadOwner = "s_BeginReadNamesAndSizesOfAllSaveGames";
	
	return s_ThreadId != sysIpcThreadIdInvalid;
}

bool fiSaveGame::CheckReadNamesAndSizesOfAllSaveGames(int signInId, int &outCount)
{
	CheckState(fiSaveGameState::READING_NAMES_AND_SIZES_OF_ALL_SAVE_GAMES);

	if(s_Done)
	{
		outCount = s_EnumCount;
		g_SaveGameState = fiSaveGameState::HAVE_READ_NAMES_AND_SIZES_OF_ALL_SAVE_GAMES;
		return true;
	}
	return false;
}

void fiSaveGame::EndReadNamesAndSizesOfAllSaveGames(int /*signInId*/)
{
	CheckState(fiSaveGameState::HAVE_READ_NAMES_AND_SIZES_OF_ALL_SAVE_GAMES);

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

	g_SaveGameState = fiSaveGameState::IDLE;
}

void fiSaveGame::SetSaveGameFolder(const char *pTitleIDForSaveGameFolder)
{
	strncpy(s_TitleIdForSavesFolder, pTitleIDForSaveGameFolder, MAX_CHARS_IN_TITLE_ID_FOR_SAVES_FOLDER);
}


static int s_FileSize = 0;
static int s_SizeOfIconAndSystemFiles = 0;
static bool s_bCalculateSizeOnDisk = false;

static void GetFileSizeStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	// If no files found then return no data error
	if(get->fileNum == 0)
	{
//		cbResult->progressBarInc = 100;
		cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_NODATA;
		return;
	}

	Assert(get->fileNum == 2);	//	data and icon
	Assert(get->fileListNum == 2);	//	data and icon	// should I change these to error messages?

	Displayf("GetFileSizeStatCallback - get->sizeKB = %d", get->sizeKB);
	Displayf("GetFileSizeStatCallback - get->sysSizeKB = %d", get->sysSizeKB);

//	if (s_bCheckForUserBindErrors && (get->bind & (CELL_SAVEDATA_BINDSTAT_ERR_NOUSER|CELL_SAVEDATA_BINDSTAT_ERR_OTHERS) ) )
//	{
//		cbResult->invalidMsg = s_UserBindErrorMsg;
//		cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_INVALID;
//	}
//	else
	{
		GetSizeOfFileFromString(get->getParam.listParam, CELL_SAVEDATA_SYSP_LPARAM_SIZE, s_FileSize, s_SizeOfIconAndSystemFiles);

		//	Changelist 128447 by Adam
		//	Don't setup setParam in LoadStatCallback as it was saving the file status everytime a load was called.
		set->setParam = NULL;

//		cbResult->progressBarInc = 100;
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
	}
}

static void GetFileSizeFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
	Assert(false);
}

static void s_BeginGetFileSize(void*)
{
	s_LastError = cellSaveDataAutoLoad2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
		CELL_SAVEDATA_ERRDIALOG_NONE,	// experimental.  not sure when this will show up.
		&s_setBuf,
		GetFileSizeStatCallback,
		GetFileSizeFileCallback,
		s_Container,
		NULL);

	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("s_BeginGetFileSize - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("Success.");
	s_Done = true;
}

bool fiSaveGame::BeginGetFileSize(int /*signInId*/, const char *filename, bool bCalculateSizeOnDisk)
{
	CheckState(fiSaveGameState::IDLE);

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		Errorf("BeginGetFileSize - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}

	g_SaveGameState = fiSaveGameState::GETTING_FILE_SIZE;

	InitSetBuf();

#if HACK_GTA4
	strcpy(s_DirName, s_TitleIdForSavesFolder);
#else	//	HACK_GTA4
	strcpy(s_DirName, XEX_TITLE_ID);
#endif	//	HACK_GTA4
	Assertf(strlen(s_DirName) == 9, "fiSaveGame::BeginGetFileSize - expected %s to have 9 characters", s_DirName);
	s_AppendToDirectoryName(s_DirName+9,sizeof(s_DirName)-9,filename);

	safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));

	s_FileSize = 0;
	s_SizeOfIconAndSystemFiles = 0;
	s_bCalculateSizeOnDisk = bCalculateSizeOnDisk;
	s_Done = false;
	s_ThreadId = sysIpcCreateThread(s_BeginGetFileSize,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] GetSavegameFileSizeThread");
	s_LastThreadOwner = "s_BeginGetFileSize";
	
	return s_ThreadId != sysIpcThreadIdInvalid;
}

bool fiSaveGame::CheckGetFileSize(int /*signInId*/, u64 &fileSize)
{
	CheckState(fiSaveGameState::GETTING_FILE_SIZE);

	if(s_Done)
	{
		fileSize = s_FileSize;
		if (!s_bCalculateSizeOnDisk)
		{
			if (Verifyf(s_FileSize >= s_SizeOfIconAndSystemFiles, "fiSaveGame::CheckGetFileSize - the size of the icon and system files %d is greater than the total file size %d", s_SizeOfIconAndSystemFiles, s_FileSize))
			{
				fileSize -= s_SizeOfIconAndSystemFiles;
			}
		}

		fileSize *= 1024;	//	Convert from KBytes to Bytes
		g_SaveGameState = fiSaveGameState::HAVE_GOT_FILE_SIZE;
		return true;
	}
	return false;
}

void fiSaveGame::EndGetFileSize(int /*signInId*/)
{
	CheckState(fiSaveGameState::HAVE_GOT_FILE_SIZE);

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

	g_SaveGameState = fiSaveGameState::IDLE;
}

#endif	//	HACK_GTA4


// *********************** GetFileModifiedTime ***********************

static u32 s_ModifiedTimeHigh = 0;
static u32 s_ModifiedTimeLow = 0;

static void GetFileModifiedTimeStatCallback(CellSaveDataCBResult *cbResult,CellSaveDataStatGet *get,CellSaveDataStatSet *set)
{
	// If no files found then return no data error
	if(get->fileNum == 0)
	{
//		cbResult->progressBarInc = 100;
		cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_NODATA;
		return;
	}

	Assert(get->fileNum == 2);	//	data and icon
	Assert(get->fileListNum == 2);	//	data and icon	// should I change these to error messages?

//	if (s_bCheckForUserBindErrors && (get->bind & (CELL_SAVEDATA_BINDSTAT_ERR_NOUSER|CELL_SAVEDATA_BINDSTAT_ERR_OTHERS) ) )
//	{
//		cbResult->invalidMsg = s_UserBindErrorMsg;
//		cbResult->result = CELL_SAVEDATA_CBRESULT_ERR_INVALID;
//	}
//	else
	{
		s_ModifiedTimeHigh = get->fileList[0].st_mtime >> 32;
		s_ModifiedTimeLow = get->fileList[0].st_mtime;

		//	Changelist 128447 by Adam
		//	Don't setup setParam in LoadStatCallback as it was saving the file status everytime a load was called.
		set->setParam = NULL;

//		cbResult->progressBarInc = 100;
		cbResult->result = CELL_SAVEDATA_CBRESULT_OK_LAST;
	}
}


static void GetFileModifiedTimeFileCallback(CellSaveDataCBResult *cbResult,CellSaveDataFileGet *get,CellSaveDataFileSet *set)
{
	Assert(false);
}


static void s_BeginGetFileModifiedTime(void*)
{
	s_LastError = cellSaveDataAutoLoad2(RAGE_CELL_SAVEGAME_VERSION,
		s_DirName,
		CELL_SAVEDATA_ERRDIALOG_NONE,	// experimental.  not sure when this will show up.
		&s_setBuf,
		GetFileModifiedTimeStatCallback,
		GetFileModifiedTimeFileCallback,
		s_Container,
		NULL);

	s_IsValid = true;
	if (s_LastError)
	{
		Errorf("s_BeginGetFileModifiedTime - %s",SaveGameGetErrorString(s_LastError));
		s_IsValid = false;
	}
	else
		Displayf("s_BeginGetFileModifiedTime - Success.");
	s_Done = true;
}


bool fiSaveGame::BeginGetFileModifiedTime(int /*signInId*/, const char *filename)
{
	CheckState(fiSaveGameState::IDLE);

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		Errorf("BeginGetFileModifiedTime - %s didn't finish properly, can't start.",s_LastThreadOwner);
		return false;
	}

	g_SaveGameState = fiSaveGameState::GETTING_FILE_MODIFIED_TIME;

	InitSetBuf();

#if HACK_GTA4
	strcpy(s_DirName, s_TitleIdForSavesFolder);
#else	//	HACK_GTA4
	strcpy(s_DirName, XEX_TITLE_ID);
#endif	//	HACK_GTA4
	Assertf(strlen(s_DirName) == 9, "fiSaveGame::BeginGetFileModifiedTime - expected %s to have 9 characters", s_DirName);
	s_AppendToDirectoryName(s_DirName+9,sizeof(s_DirName)-9,filename);

	safecpy(s_FilenameBuf, FileName, sizeof(s_FilenameBuf));

	s_ModifiedTimeHigh = 0;
	s_ModifiedTimeLow = 0;

	s_Done = false;
	s_ThreadId = sysIpcCreateThread(s_BeginGetFileModifiedTime,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] GetSavegameFileModifedTimeThread");
	s_LastThreadOwner = "s_BeginGetFileModifiedTime";

	return s_ThreadId != sysIpcThreadIdInvalid;
}

bool fiSaveGame::CheckGetFileModifiedTime(int /*signInId*/, u32 &modTimeHigh, u32 &modTimeLow)
{
	CheckState(fiSaveGameState::GETTING_FILE_MODIFIED_TIME);

	if(s_Done)
	{
		modTimeHigh = s_ModifiedTimeHigh;
		modTimeLow = s_ModifiedTimeLow;
		g_SaveGameState = fiSaveGameState::HAVE_GOT_FILE_MODIFIED_TIME;
		return true;
	}
	return false;
}

void fiSaveGame::EndGetFileModifiedTime(int /*signInId*/)
{
	CheckState(fiSaveGameState::HAVE_GOT_FILE_MODIFIED_TIME);

	ShutdownSetBuf();

	if (s_ThreadId != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(s_ThreadId);
		s_ThreadId = sysIpcThreadIdInvalid;
	}

	g_SaveGameState = fiSaveGameState::IDLE;
}


} // namespace rage

#endif	// CELL_SDK_VERSION
#endif	// __PPU
