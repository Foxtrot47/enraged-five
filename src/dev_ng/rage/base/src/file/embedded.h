// 
// file/embedded.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_EMBEDDED_H
#define FILE_EMBEDDED_H

namespace rage {

/*
	These auto-register themselves but you still need to make sure
	they piggyback onto a .cpp file that is being used so that they
	don't get ignored by the linker.
*/
class fiEmbeddedFile {
public:
	fiEmbeddedFile(const char*,const unsigned char*,int);

	static bool Lookup(const char *name,const unsigned char *&data,int &dataSize);

private:
	const char *m_Path;
	const unsigned char *m_Data;
	int m_DataSize;

	static fiEmbeddedFile *sm_First;
	fiEmbeddedFile *m_Next;
};

}	// namespace rage

#endif
