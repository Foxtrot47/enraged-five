// 
// file/device_common.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#include "device.h"
#include "device_relative.h"
#include "device_installer.h"
#include "embedded.h"

#include "atl/array.h"
#include "atl/binmap.h"

#include "data/growbuffer.h"
#include "data/resourceheader.h"
#include "string/stringutil.h"
#include "system/endian.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/timer.h"
#include "tcpip.h"
#include "file/asset.h"

#include <string.h>
#include <stdlib.h>

PARAM(checkUnusedFiles, "Record all open files so that RAG can display unused files");

namespace rage {

PARAM(breakonfile, "Break when a file containing the specified substring is opened");

#define READ_PROGRESS_WATCHDOG                          (!__NO_OUTPUT)
#define READ_PROGRESS_WATCHDOG_ERROR_LIMIT_SECONDS      (10.f)

#define LOCAL_DEVICE_UPDATE (__XENON || __PPU)

#if __PPU
fiDeviceLocalDrm s_DeviceLocal;
#else
fiDeviceLocal s_DeviceLocal;
#endif

#if !__FINAL && HACK_GTA4 && !__PPU
PARAM(update, "Override development update folder");
PARAM(noupdate, "Disable use of development update folder");
#endif
fiDeviceRelative s_DeviceUpdateRelative;
fiDeviceRelative s_DeviceUpdateRelative2;
fiDevice* s_pDeviceUpdate = &s_DeviceLocal;

const fiDevice* s_DefaultDevice = &s_DeviceLocal;
static bool s_DefaultDeviceReadOnly = false;
int fiDevice::sm_MaxMounts = 16;
int fiDevice::sm_MaxExtraMounts = MAX_EXTRA_MOUNTS;

#if __DEVPROF
static fiDeviceProfiler s_DummyProfiler;
fiDeviceProfiler *fiDevice::sm_DeviceProfiler = &s_DummyProfiler;
#endif // __DEVPROF

bool sm_FallbackToDefaultDevice = false;
bool sm_MatchLongestPath = true;
bool sm_MatchNewest = false;

struct Mount_t {
	ConstString Path;
	u16 ReadOnly;
	u16 PathLen;
	atArray<const fiDevice *> Device;
	Mount_t()
	{
		Device.Reserve(1);
	}

	void Reset()
	{
		ReadOnly = 0;
		PathLen = 0;
		Path = NULL;
		Device.ResetCount();
	}
};

static atArray<Mount_t> s_Mounts;
static atBinaryMap<Mount_t, u32> s_ExtraMounts;
static sysCriticalSectionToken sm_Token;

#if __BANK
atBinaryMap<atString, u32> g_RequestedFiles;
atBinaryMap<atString, u32> g_RequestedDevices;
static sysCriticalSectionToken g_RequestedFilesAndDevicesLock;
#endif

static int mountcmp(const char *dst,const char *src,unsigned int n) {
	int f = 0, l = 0;
	if (n) do {
		if ( ((f = (unsigned char)(*(dst++))) >= 'A') && (f <= 'Z') )
			f -= ('A' - 'a');
		if ( ((l = (unsigned char)(*(src++))) >= 'A') && (l <= 'Z') )
			l -= ('A' - 'a');
		if (f == '\\') f = '/';
		if (l == '\\') l = '/';
	} while ( --n && f && (f == l));
	return (f - l);
}

void fiDevice::InitClass()
{
#if HACK_GTA4
#if !__FINAL
	const char* updateFolder;
	if(PARAM_update.Get(updateFolder) && !PARAM_noupdate.Get())
	{
		// Not read-only since we want to be able to find the correct device for writable subfolders 
		s_DeviceUpdateRelative.Init(updateFolder, false);
		s_DeviceUpdateRelative.MountAs("update:/");
		s_pDeviceUpdate = &s_DeviceUpdateRelative;

		s_DeviceUpdateRelative2.Init( updateFolder, false );
		s_DeviceUpdateRelative2.MountAs( "update2:/" );

		Displayf("Mounted UPDATE data at [%s] - override via -update option",updateFolder);
	}
#endif

#if LOCAL_DEVICE_UPDATE
	if(s_pDeviceUpdate == &s_DeviceLocal
		NOTFINAL_ONLY(&& !PARAM_noupdate.Get())
		)
	{
		// Not read-only since we want to be able to find the correct device for writable subfolders 
		s_DeviceUpdateRelative.Init("update/", false);
		s_DeviceUpdateRelative.MountAs("update:/");
		s_pDeviceUpdate = &s_DeviceUpdateRelative;
		OUTPUT_ONLY(char updateFolder[RAGE_MAX_PATH];)
		Displayf("Mounted UPDATE data at [%s] - using internal default",s_pDeviceUpdate->FixRelativeName(updateFolder,RAGE_MAX_PATH,"update/"));
	}

	if(s_pDeviceUpdate == &s_DeviceLocal)
	{
		Warningf("No UPDATE data mounted - check to see that this is the intent (%s)",NOTFINAL_ONLY(PARAM_noupdate.Get()?"mount was disabled with -noupdate":)"problem with update directory/packfile?");
		fiDevice::Mount("update:/", *s_pDeviceUpdate, true);
	}
#endif // LOCAL_DEVICE_UPDATE

#endif	// HACK_GTA4
}

void fiDevice::SetFindNewest(bool val)
{
	sm_MatchNewest = val;
}

bool fiDevice::IsExtraMount(const char* filename)
{
	return (strncmp(filename, "dlc", 3) == 0);
}

bool fiDevice::CheckFileValidity(const char* pFilename)
{
 	if(!ASSET.IsAbsolutePath(pFilename))
 		return true;
	char deviceName[RAGE_MAX_PATH]={0};
	strncpy(deviceName,pFilename,static_cast<int>(strcspn(pFilename,":")));
	if(!strnicmp(pFilename,"dlc",3)==0)
	{
		for(int i=0; i<s_Mounts.GetCount();i++)
		{	
			char registeredDevice[RAGE_MAX_PATH]={0};
			strncpy(registeredDevice,s_Mounts[i].Path,static_cast<int>(strcspn(pFilename,":")));
			if (!stricmp(registeredDevice,deviceName))
			{
				return true;
			}
		}
		return false;
	}
	return true;
}

void fiDevice::GetExtraDeviceName(const char* filename, char *deviceName, int maxNameLength)
{
	deviceName[0] = '\0';

	if(const char *p = strchr(filename, ':'))
	{
		int len = rage::Min((int)(p - filename), maxNameLength - 1);
		memcpy(deviceName, filename, len);
		deviceName[len] = '\0';
	}
}

Mount_t *fiDevice::FindExtraMount(const char* filename)
{
	char deviceName[RAGE_MAX_PATH] = { 0 };
	GetExtraDeviceName(filename, deviceName, RAGE_MAX_PATH);
	return s_ExtraMounts.SafeGet(atStringHash(deviceName));
}

const fiDevice *fiDevice::FindExtraDevice(const char* filename)
{
	if(Mount_t *m = FindExtraMount(filename))
	{
		for (int i = m->Device.GetCount()-1; i>=0; i--) {	// search in the last mounted device first!
			const fiDevice* device = m->Device[i];

			if(device->GetAttributes(filename) != FILE_ATTRIBUTE_INVALID)
			{
				return device;
			}
		}

		// Device exists, but we didn't find any files, just select the last device on the list
		if (m->Device.GetCount() > 0)
			return m->Device[m->Device.GetCount() - 1];
	}

	return NULL;
}

Mount_t *fiDevice::AddExtraMount(const char* filename)
{
	char deviceName[RAGE_MAX_PATH] = { 0 };
	GetExtraDeviceName(filename, deviceName, RAGE_MAX_PATH);
	u32 key = atStringHash(deviceName);
	Mount_t *m = s_ExtraMounts.SafeGet(key);
	if(m)
		return m;

	s_ExtraMounts.Insert(key); // it can be useful to have InsertSorted method
	s_ExtraMounts.FinishInsertion();
	m = s_ExtraMounts.SafeGet(key);
	m->Reset();

	return m;
}

bool fiDevice::UnmountExtra(const char* filename)
{
	char deviceName[RAGE_MAX_PATH] = { 0 };
	GetExtraDeviceName(filename, deviceName, RAGE_MAX_PATH);
	u32 key = atStringHash(deviceName);

	if(Mount_t *m = s_ExtraMounts.SafeGet(key))
	{
		SYS_CS_SYNC(sm_Token);
		m->Device.clear();
		u32 index = s_ExtraMounts.GetIndexFromDataPtr(m);
		s_ExtraMounts.Remove(index);
		return true;
	}	
	else
	{
		Warningf("UnmountExtra, device for %s not found!", filename);
	}

	return false;
}

bool fiDevice::GetIsReadOnly(const char* device)
{
	for(int i=0;i<s_Mounts.GetCount();i++)
	{
		if(strcmp(s_Mounts[i].Path.c_str(),device)==0)
		{
			return s_Mounts[i].ReadOnly!=0;
		}
	}
	return false;
}

#if __BANK 

/*
PURPOSE		Notes that a file was requested.
			In bank only, will store filename and device in map
PARAMS		filename - Name of file requested
*/
static void NoteRequestedFileAndDevice(const char *filename, const char *devicename) {
	SYS_CS_SYNC(g_RequestedFilesAndDevicesLock);
	
	// First store device
	if (devicename && strlen(devicename)) {
		atString strDevice(devicename);
		atHashString hashedDevice = atStringHash(strDevice);

		if (!g_RequestedDevices.Has(hashedDevice)) {
			g_RequestedDevices.Insert(hashedDevice, strDevice);
		}
	}
	
	// Now store file
	atString strFile(filename);
	atHashString hashed = atStringHash(strFile);

	if (!g_RequestedFiles.Has(hashed)) {
		g_RequestedFiles.Insert(hashed, strFile);
	}
}

#endif

const fiDevice* fiDevice::GetDevice(const char* filename,bool readOnly) {
	const fiDevice *dev = GetDeviceImpl(filename, readOnly);

#if __BANK
	if (dev && PARAM_checkUnusedFiles.Get()) {
		char fullPath[RAGE_MAX_PATH];
		dev->FixRelativeName(fullPath, RAGE_MAX_PATH, filename); 

		char deviceName[RAGE_MAX_PATH] = { 0 };

		// get device, if possible
		if (const char *colonPtr = strstr(filename, ":")) {
			size_t colonPos = colonPtr - filename;

			strncpy(deviceName, filename, colonPos+1); // +1 to include :
		}

		NoteRequestedFileAndDevice(fullPath, deviceName);
	}
#endif //__BANK

	return dev;
}

const fiDevice* fiDevice::GetDeviceImpl(const char* filename,bool readOnly) {
	const char* breakName = NULL;
	if (sysParam::IsInitialized() && PARAM_breakonfile.Get(breakName))
	{
		if (stristr(filename, breakName))
		{
			Warningf("Breaking for filename %s", filename);
			__debugbreak();
		}
	}

	if (!strncmp(filename,"memory:",7))
		return &fiDeviceMemory::GetInstance();
	//else if (!strncmp(filename,"embedded:/",10))
	//	return &fiDeviceMemory::GetInstance();
	else if (!strncmp(filename, "count:", 6))
		return &fiDeviceCount::GetInstance();
	else if (!strncmp(filename, "growbuffer:", 11))
		return &fiDeviceGrowBuffer::GetInstance();	
#if !__NO_OUTPUT
	else if (!strncmp(filename, "console:", 8))
		return &fiDeviceConsole::GetInstance();
#endif
	else if(IsExtraMount(filename))
		return FindExtraDevice(filename);

#if __WIN32 || __PPU
	if (!strncmp(filename,"tcpip:",6))
		return &fiDeviceTcpIp::GetInstance();
#endif
#if __XENON
	if (!strncmp(filename,"cache:",6))												// this is the title's cache partition
		return &fiDeviceLocal::GetInstance();
	if (!strncmp(filename,"cache1:",7))												// this is the title's cache partition
		return &fiDeviceLocal::GetInstance();
	if (!strncmp(filename,"game:",5))												// this is the title's DVD partition
		return &fiDeviceLocal::GetInstance();
	if (!strncmp(filename,"dvd:",4))												// this is the title's DVD partition
		return &fiDeviceLocal::GetInstance();

#if !__FINAL && HACK_GTA4
	if (!strncmp(filename,"update:",7) && (s_pDeviceUpdate == &s_DeviceUpdateDebug))												// this is the title update resource partition
		return s_pDeviceUpdate;
#endif // __FINAL && HACK_GTA4

#if !__FINAL
	if (!strncmp(filename,"devkit:",7))												// this is the title's devkit partition
		return &fiDeviceLocal::GetInstance();

	if (fiDeviceInstaller::IsPartitionPath(filename))								// if we have installed content it might be accessed with an arbitrary symbolic link name
		return &fiDeviceLocal::GetInstance();
#endif

#endif
#if __PPU
	if (!strncmp(filename,"game:",5) || !strncmp(filename,"/dev_",5) || !strncmp(filename,"/app_home/",10) || !strncmp(filename,"/host_root/",11))
		return &fiDeviceLocal::GetInstance();

	if (!strncmp(filename,"gamedata:",9))
		return &fiDeviceLocal::GetInstance();
#endif
#if __PSP2
	if (!strncmp(filename,"host0:",6) || !strncmp(filename,"disc0:",6) || !strncmp(filename, "/savedata", 9))
		return &fiDeviceLocal::GetInstance();
	// need to add savedata etc...
#endif
#if RSG_ORBIS
#if !__FINAL
	if (!strncmp(filename,"host0:",6) || !strncmp(filename,"disc0:",6) || !strncmp(filename,"/data/",6))
		return &fiDeviceLocal::GetInstance();
#endif
	if ( !strncmp(filename, "/savedata", 9) || !strncmp(filename,"/app0/",6) || !strncmp(filename,"/usb",4)  || !strncmp(filename,"/av_contents/",13) || !strncmp(filename,"/addcont",8) || !strncmp(filename,"/temp0/",7) || !strncmp(filename, "/download", 9))
		return &fiDeviceLocal::GetInstance();
#endif
#if RSG_DURANGO
// For Durango DLC
	if(stristr(filename,"globalroot"))
		return &fiDeviceLocal::GetInstance();
#if !__FINAL
	if (!strnicmp(filename,"xg:",3))											// this is the HD title partition when referred to in a command line arg
		return &fiDeviceLocal::GetInstance();
	if (!strnicmp(filename,"xd:",3))											// this is the HD title scratch partition when referred to in a command line arg
		return &fiDeviceLocal::GetInstance();
#endif
	if (!strnicmp(filename,"t:",2) )											// title temporary HDD partition
		return &fiDeviceLocal::GetInstance();
	//According to this link https://forums.xboxlive.com/AnswerPage.aspx?qid=a039367b-1c07-42c7-a1d6-a5b7a5ef9af5&tgt=1
	//there is a bug in VS reporting '??' instead of '?' so to be sure we're supporting both.
	if (!strnicmp(filename,"\\??\\T:",6) || !strnicmp(filename,"/??/T:",6) || !strnicmp(filename,"\\?\\T:",5) || !strnicmp(filename,"/?/T:",5) )
		return &fiDeviceLocal::GetInstance();
	if (!strnicmp(filename,"g:",2) )											// title main HDD partition
		return &fiDeviceLocal::GetInstance();
#endif	// RSG_DURANGO

	const fiDevice* retDevice = NULL;
	const Mount_t* mount = NULL;

	if (sm_MatchLongestPath)
	{
		SYS_CS_SYNC(sm_Token);
		// Find longest match in mount table (more specific mount points take
		// precedence over less specific ones)
		int longest = 0;
		int longestIndex = -1;
		NOTFINAL_ONLY(u64 newestTimestamp = 0;)
		for (int i=0; i<s_Mounts.GetCount(); i++) {
			if (!mountcmp(s_Mounts[i].Path,filename,s_Mounts[i].PathLen)) {
				if (s_Mounts[i].PathLen > longest) {
					longest = s_Mounts[i].PathLen;
					longestIndex = i;
				}
			}
		}
		if(longestIndex != -1) {
			if(s_Mounts[longestIndex].Device.GetCount() == 1)
			{
				retDevice = s_Mounts[longestIndex].Device[0];
			}
			else
			{
				for (int i = s_Mounts[longestIndex].Device.GetCount()-1; i>=0; i--) {	// search in the last mounted device first!
					const fiDevice* device = s_Mounts[longestIndex].Device[i];
					if (device->GetAttributes(filename) != FILE_ATTRIBUTE_INVALID || i == 0)
					{
#if !__FINAL
						if (sm_MatchNewest)
						{
							u64 timestamp = device->GetFileTime(filename);
							if (timestamp > newestTimestamp)
							{
								newestTimestamp = timestamp;
								retDevice = device;
							}
						}
						else
#endif
						{
							retDevice = device;
							break;
						}
					}
				}
			}

			mount = &s_Mounts[longestIndex];
		}
	}
	else
	{
		SYS_CS_SYNC(sm_Token);
		//Search for file in all matching mount paths, not just the longest one. In reverse order -- last mounted device gets priority.
		for (int mountIndex=s_Mounts.GetCount()-1; mountIndex >= 0 && !retDevice; mountIndex--) {
			if (!mountcmp(s_Mounts[mountIndex].Path,filename,s_Mounts[mountIndex].PathLen)) {
				mount = &s_Mounts[mountIndex];
				for (int i = s_Mounts[mountIndex].Device.GetCount()-1; i>=0; i--) {	// search in the last mounted device first!
					const fiDevice* device = s_Mounts[mountIndex].Device[i];
					if(device->GetAttributes(filename) != FILE_ATTRIBUTE_INVALID)
					{
						retDevice = device;
						break;
					}
				}
			}
		}
	}

	if (mount && (retDevice || !sm_FallbackToDefaultDevice)) {
		if (readOnly || !mount->ReadOnly)
		{
			return retDevice;
		}
	}

	if (!s_DefaultDevice)
		Errorf("fiDevice::GetDevice - Filename '%s' not in mount list, no default device",filename);

	// If the file is readonly or the default device is NOT marked readonly, return the default device
	return (readOnly || !s_DefaultDeviceReadOnly)? s_DefaultDevice : NULL;
}

int fiDevice::EnumFiles(const char *filename,void (*callback)(const fiFindData &data,void *userArg),void *userArg,bool bCheckAllExtraMountDevices)
{
	int matches = 0;

	if(bCheckAllExtraMountDevices && IsExtraMount(filename))
	{
		if(Mount_t *m = FindExtraMount(filename))
		{
			for (int i = 0; i < m->Device.GetCount(); i++)
			{
				const fiDevice* device = m->Device[i];
				if(device->GetAttributes(filename) != FILE_ATTRIBUTE_INVALID)
				{
					matches += device->EnumFilesForDevice(filename, callback, userArg);
				}
			}
		}
	}
	else
	{
		const fiDevice *device = GetDevice(filename,true);
		if (device)
		{
			matches = device->EnumFilesForDevice(filename, callback, userArg);
		}
	}

	return matches;
}

int fiDevice::EnumFilesForDevice(const char *filename,void (*callback)(const fiFindData &data,void *userArg),void *userArg) const
{
	fiFindData data;
	int matchesForThisDevice = 0;

	fiHandle handle = FindFileBegin(filename,data);
	if (fiIsValidHandle(handle)) {
		do {
			if (data.m_Name[0] != '.' || !(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) {
				++matchesForThisDevice;
				callback(data,userArg);
			}
		} while (FindFileNext(handle,data));
		FindFileEnd(handle);
	}

	return matchesForThisDevice;
}

bool fiDevice::Remount(const fiDevice &newdevice, const fiDevice &olddevice, bool UNUSED_PARAM(readonly)) {

	// lets check if the current mounting point already exists and use it to add the new device to.
	Mount_t *m = NULL;
	SYS_CS_SYNC(sm_Token);
	for (int i=0; i<s_Mounts.GetCount(); i++) {
		m = &s_Mounts[i];
		for (int j=0; j<m->Device.GetCount(); j++)
		{
			if(m->Device[j] == &olddevice)
			{
				// we don't need this to be an assertion right now
				//Assert(readonly == (m->ReadOnly!=0));
				m->Device[j] = &newdevice;
				return true;
			}
		}
	}

	for (int i=0; i<s_ExtraMounts.GetCount(); i++) {
		m = s_ExtraMounts.GetItem(i);
		for (int j=0; j<m->Device.GetCount(); j++)
		{
			if(m->Device[j] == &olddevice)
			{
				// we don't need this to be an assertion right now
				//Assert(readonly == (m->ReadOnly!=0));
				m->Device[j] = &newdevice;
				return true;
			}
		}
	}

	Warningf("Remount, device not found!");
	return false;
}

bool fiDevice::Mount(const char *sourcePath,const fiDevice &device,bool readonly) {
	if (!strcmp(sourcePath,"default")) {
		if (s_DefaultDevice)
			return false;
		else {
			s_DefaultDevice = &device;
			s_DefaultDeviceReadOnly = readonly;
			return true;
		}
	}

	Mount_t *m = NULL;
	int sourcePathLen = StringLength(sourcePath);

	if (!sourcePath[0] || !strchr("/\\",sourcePath[sourcePathLen-1])) {
		Errorf("fiDevice::Mount - mount name must end in a slash");
		return false;
	}

	if ( sourcePathLen >= RAGE_MAX_PATH-2 ) {
		Errorf("fiDevice::Mount - mount name too long");
		return false;
	}

	if(!IsExtraMount(sourcePath))
	{
		SYS_CS_SYNC(sm_Token);
		if(!s_Mounts.GetCapacity())
			s_Mounts.Reserve(sm_MaxMounts);

		// lets check if the current mounting point already exists and use it to add the new device to.
		for (int i=0; i<s_Mounts.GetCount(); i++) {
			if (!mountcmp(s_Mounts[i].Path,sourcePath,sourcePathLen))
			{
				m = &s_Mounts[i];
				break;
			}
		}

		if(!m)
		{
			// Check to see if we have any pre-allocated mounts available before appending a new one (to avoid memory allocation)
			if (s_Mounts.GetCount() == s_Mounts.GetCapacity())
			{
				Quitf(ERR_FIL_COMMON,"fiDevice::Mount - Ran out of device mounts! Quitting to prevent unspecified behaviour. %i/%i", s_Mounts.GetCount(), s_Mounts.GetCapacity());
#if !__FINAL && !RSG_PC
				return false;
#endif // !__FINAL && !RSG_PC
			}

			m = &s_Mounts.Append();
			// device array doesn't always get cleared
			m->Device.clear();	
		}
		else
		{
			// we don't need this to be an assertion right now
			//Assert(readonly == (m->ReadOnly!=0));
		}
	}
	else
	{
		SYS_CS_SYNC(sm_Token);
		if(!s_ExtraMounts.GetRawDataArray().GetCapacity())
			s_ExtraMounts.Reserve(sm_MaxExtraMounts);

		m = AddExtraMount(sourcePath);
	}

	m->Path = sourcePath;
	Assign(m->PathLen, sourcePathLen);
	m->ReadOnly = readonly;

	// we only reserved one entry for devices that use this path. Lets see if we used this slot up:
	if (m->Device.GetCount() == m->Device.GetCapacity())
		// Ok, we used the pre-allocated slot up, so we need to allocate a new slot
		m->Device.Grow(1) = &device;
	else
		// nope, first entry is still available, use it! (Avoids dynamic memory allocation)
		m->Device.Append() = &device;
	return true;
}


bool fiDevice::Unmount(const char *sourcePath) {
	if (!strcmp(sourcePath,"default")) {
		if (s_DefaultDevice) {
			s_DefaultDevice = NULL;
			return true;
		}
		else
			return false;
	}

	if(IsExtraMount(sourcePath))
		return UnmountExtra(sourcePath);

	SYS_CS_SYNC(sm_Token);
	int sl = StringLength(sourcePath);
	for (int i=0; i<s_Mounts.GetCount(); i++) {
		if (!mountcmp(s_Mounts[i].Path,sourcePath,sl)) {
			s_Mounts[i].Device.clear(); // KS
			s_Mounts.Delete(i);
			// If unmounting the last device free the array
			if (!s_Mounts.GetCount())
				s_Mounts.Reset();
			return true;
		}
	}
	return false;
}


bool fiDevice::Unmount(const fiDevice &olddevice) {
	// lets check if the mounting point exists and remove from the list if it does.
	Mount_t *m = NULL;
	SYS_CS_SYNC(sm_Token);
	for (int i=0; i<s_Mounts.GetCount(); i++) {
		m = &s_Mounts[i];
		for (int j=0; j<m->Device.GetCount(); j++)
		{
			if(m->Device[j] == &olddevice)
			{
				m->Device.Delete(j);	// reshuffle the device pointer array (does not delete the fiDevice itself, caller should do that)
				return true;
			}
		}
	}

	for (int i=0; i<s_ExtraMounts.GetCount(); i++) {
		m = s_ExtraMounts.GetItem(i);
		for (int j=0; j<m->Device.GetCount(); j++)
		{
			if(m->Device[j] == &olddevice)
			{
				m->Device.Delete(j);	// reshuffle the device pointer array (does not delete the fiDevice itself, caller should do that)
				return true;
			}
		}
	}

	Warningf("Unmount, device not found!");
	return false;
}


u64 fiDevice::GetFileSize(const char *filename) const {
	Assertf(false, "Please override this for your device.");
	fiHandle h = this->Open(filename, true);
	if(h == fiHandleInvalid) return 0;
	u64 retval = Size64(h);
	this->Close(h);
	return retval;
}

int fiDevice::Size(fiHandle handle) const {
	Assertf(false, "Please override this for your device.");
	int current = Seek(handle,0,seekCur);
	int rv = Seek(handle,0,seekEnd);
	Seek(handle,current,seekSet);
	return rv;
}


u64 fiDevice::Size64(fiHandle handle) const {
	Assertf(false, "Please override this for your device.");
	u64 current = Seek64(handle,0,seekCur);
	u64 rv = Seek64(handle,0,seekEnd);
	Seek64(handle,current,seekSet);
	return rv;
}


fiHandle fiDevice::OpenBulk(const char *,u64&) const {
	return fiHandleInvalid;
}


fiHandle fiDevice::OpenBulkDrm(const char *filename,u64 &outBias,const void * ) const {
	return OpenBulk(filename, outBias);
}


fiHandle fiDevice::CreateBulk(const char *) const {
	return fiHandleInvalid;
}


int fiDevice::ReadBulk(fiHandle /*handle*/,u64 /*offset*/,void * /*outBuffer*/,int /*bufferSize*/) const {
	return -1;
}


int fiDevice::WriteBulk(fiHandle /*handle*/,u64 /*offset*/,const void * /*inBuffer*/,int /*bufferSize*/) const {
	return -1;
}


int fiDevice::CloseBulk(fiHandle /*handle*/) const {
	return -1;
}


int fiDevice::Flush(fiHandle /*handle*/) const {
	return 0;
}


int fiDevice::GetResourceInfo(const char* name,datResourceInfo &info) const {
	// ReadBulk will hang indefinitely on short reads so check that the file isn't bad.
	if (GetFileSize(name) < sizeof(datResourceFileHeader))
		return 0;

	u64 bias;
	fiHandle h = OpenBulk(name,bias);
	int result = 0;
	if (fiIsValidHandle(h)) {
		datResourceFileHeader hdr;
		// For RSC7 non-tools builds, let's always have the endianness be appropriate for the target platform and skip the extra checks.
		bool hasHeader = (ReadBulk(h,bias,&hdr,sizeof(hdr))==(int)sizeof(hdr));
		if (hasHeader)
		{
#if RSG_PC && !RSG_FINAL // intention is this is for tools only
			if (hdr.Magic == sysEndian::Swap(datResourceFileHeader::c_MAGIC))
			{
				sysEndian::SwapMe(hdr.Magic);
				sysEndian::SwapMe(hdr.Version);
				sysEndian::SwapMe((u32&)hdr.Info.Virtual);
				sysEndian::SwapMe((u32&)hdr.Info.Physical);
			}
#endif
			if (hdr.Magic == datResourceFileHeader::c_MAGIC)
			{
				info = hdr.Info;
				result = (int) hdr.Version;
			}
		}
		CloseBulk(h);
	}
	return result;
}


bool fiDevice::Delete(const char * /*filename*/) const {
	return false;
}


bool fiDevice::Rename(const char * /*from*/,const char * /*to*/) const {
	return false;
}

void fiDevice::Sanitize(fiHandle /*handle*/) const{
	// No implementation here necessary
}

bool fiDevice::MakeDirectory(const char * /*filename*/) const {
	return false;
}

bool fiDevice::UnmakeDirectory(const char * /*filename*/) const {
	return false;
}

u32 fiDevice::GetAttributes(const char*) const {
	return FILE_ATTRIBUTE_INVALID;
}


bool fiDevice::SetAttributes(const char*,u32) const {
	return false;
}


fiHandle fiDevice::FindFileBegin(const char *,fiFindData &) const {
	return fiHandleInvalid;
}

bool fiDevice::FindFileNext(fiHandle,fiFindData &) const {
	return false;
}

int fiDevice::FindFileEnd(fiHandle) const {
	return -1;
}

bool fiDevice::SetEndOfFile(fiHandle) const {
	return false;
}

bool fiDevice::IsOutOfDate(const char *original,const char *dependent) {
	const fiDevice *oDev = GetDevice(original,true);
	const fiDevice *dDev = GetDevice(dependent,true);
	u64 origStamp = oDev? oDev->GetFileTime(original) : 0;
	u64 depStamp = dDev? dDev->GetFileTime(dependent) : 0;
	return  origStamp > depStamp;
}
								  



const fiDevice& fiDeviceLocal::GetInstance() {
	return s_DeviceLocal;
}


/*
PURPOSE
	Safe version of coreRawReadFile.
PARAMS
	handle - file handle.
	buffer - where data to save.
	size - the data size to read.
RETURNS
	0 if success.
NOTES
*/
bool fiDevice::SafeRead(fiHandle handle,void *buffer,int size) const {
#	if READ_PROGRESS_WATCHDOG
		sysTimer watchdog;
		const float WARN_TIME_STEP = 1.f;
		float warnTime = WARN_TIME_STEP;
#	endif
	int done = 0;
	while (done < size) {
		int amt = Read(handle,(char*)buffer + done,size - done);
		if (amt < 0)
			return false;
		else {
			done += amt;
#			if READ_PROGRESS_WATCHDOG
				if (amt > 0) {
					watchdog.Reset();
					if (warnTime > WARN_TIME_STEP) {
						Displayf("\"%s\" fiDevice::SafeRead progress resumed",GetDebugName());
						warnTime = WARN_TIME_STEP;
					}
				}
				else {
					const float elapsed = watchdog.GetTime();
					if (elapsed > warnTime) {
						if (elapsed > READ_PROGRESS_WATCHDOG_ERROR_LIMIT_SECONDS)
							Errorf("\"%s\" fiDevice::SafeRead stalled, %.1f sec, potential SysTrayRFS problem",GetDebugName(),warnTime);
						else
							Warningf("\"%s\" fiDevice::SafeRead stalled, %.1f sec",GetDebugName(),warnTime);
						warnTime += WARN_TIME_STEP;
					}
				}
#			endif
		}
	}
	return true;
}


/*
PURPOSE
	Safe version of coreRawWriteFile.
PARAMS
	handle - file handle.
	buffer - data to write to file.
	size - the data size to be written.
RETURNS
	true if success.
NOTES
*/
bool fiDevice::SafeWrite(fiHandle handle,const void *buffer,int size) const {
	int done = 0;
	while (done < size) {
		int amt = Write(handle,(const char*)buffer + done,size - done);
		if (amt < 0)
			return false;
		else
			done += amt;
	}
	return true;
}


sysIpcSema fiDevice::sm_FileSemaphore;

void fiDevice::Lock() {
	if (!sm_FileSemaphore)
		sm_FileSemaphore = sysIpcCreateSema(true);
	sysIpcWaitSema(sm_FileSemaphore);
}


void fiDevice::Unlock() {
	sysIpcSignalSema(sm_FileSemaphore);
}

char* fiDevice::FixRelativeName( char* dest, int destSize, const char* src ) const
{
	safecpy(dest, src, destSize); return dest;
}

bool fiDevice::DeleteDirectory(const char *pathname, bool deleteContents, bool ignoreReadOnly) {
	bool result = true;
	const fiDevice *dev = fiDevice::GetDevice(pathname);
	if ( deleteContents ) {
		// Iterate through all folders first, files second to make sure everything is deleted
		static fiFindData data;
		fiHandle handle = dev->FindFileBegin(pathname, data);
		if ( fiIsValidHandle(handle) ) {
			while (result) {
				char currentPath[RAGE_MAX_PATH];
				if ((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0)) {
					// Found a directory, so deal with it
					if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY) {
						// Build the new pathname to use
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);
						// Recurse!
						if (!DeleteDirectory(currentPath, true)) {
							dev->FindFileEnd(handle);
							return false;
						}
					}
					else {
						// Must be a file, kill it
						// Build the new pathname to use
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);
						if (!dev->Delete( currentPath )) {
							// Try again, clearing readonly bit.
							if(!ignoreReadOnly)
								return false;
							dev->SetAttributes( currentPath, dev->GetAttributes(currentPath) & ~FILE_ATTRIBUTE_READONLY );
							if (!dev->Delete( currentPath )) {
								dev->FindFileEnd(handle);
								return false;
							}
						}
					}
				}
				result = dev->FindFileNext(handle, data);
			}
			dev->FindFileEnd(handle);
		}
	}
	result = dev->UnmakeDirectory( pathname ) != 0;
	return result;
}

#if __TOOL
const char *fiDevice::FindFile(const char *path, const char *filename, u32 options) const
{
	const fiDevice *dev = fiDevice::GetDevice(path);
	// Iterate through all folders first, files second to make sure everything is deleted
	static fiFindData data;
	fiHandle handle = dev->FindFileBegin(path, data);
	bool validHandle = true;
	if ( fiIsValidHandle(handle) ) {
		while (validHandle) {
			char currentPath[RAGE_MAX_PATH];
			if (data.m_Name[0] != '.') {
				// Found a directory, so deal with it
				if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY) {
					// Build the new pathname to use
					formatf(currentPath, sizeof(currentPath), "%s\\%s", path, data.m_Name);
					// Recurse!
					const char *subDirResult = FindFile(currentPath, filename, options);
					if (subDirResult) 
					{
						return subDirResult;
					}
				}
				else {
					// Build the new pathname to use
					if(strstr(data.m_Name, filename))
					{
						char *returnAlloc = new char[512];
						formatf(returnAlloc, sizeof(char)*512, "%s\\%s", path, data.m_Name);
						return returnAlloc;
					}
				}
			}
			validHandle = dev->FindFileNext(handle, data);
		}
		dev->FindFileEnd(handle);
	}
	return NULL;
}
#endif

bool fiDevice::CopySingleFile(const char *origFile, const char *newFile) {
	const fiDevice *origDev = fiDevice::GetDevice(origFile,true);
	const fiDevice *newDev = fiDevice::GetDevice(newFile,false);
	if (!origDev || !newDev)
		return false;

	fiHandle origHandle = origDev->Open(origFile,true);
	if (!fiIsValidHandle(origHandle))
		return false;

	fiHandle newHandle = newDev->Create(newFile);
	if (!fiIsValidHandle(newHandle)) {
		origDev->Close(origHandle);
		return false;
	}

	bool result = true;
	static const int bufferSize = 32768;
	char buffer[bufferSize];
	int amtRead;
	while ((amtRead = origDev->Read(origHandle, buffer, bufferSize)) > 0) {
		int amtWritten = newDev->Write(newHandle, buffer, amtRead);
		if (amtWritten != amtRead) {
			result = false;
			break;
		}
	}
	
	// Fail the copy if any read failed
	if (amtRead < 0)
		result = false;

	// Clean up file handles
	newDev->Close(newHandle);
	origDev->Close(origHandle);
	return result;
}


bool fiDevice::CopyDirectory(const char *fromDirectory, const char *toDirectory) {
	bool result = true;

	// Create the output directory
	const fiDevice *fromDev = fiDevice::GetDevice(fromDirectory);
	const fiDevice *toDev = fiDevice::GetDevice(toDirectory);
	toDev->MakeDirectory(toDirectory);

	// Iterate through all folders first, files second to make sure everything is copied
	char currentPath[RAGE_MAX_PATH];
	char currentToPath[RAGE_MAX_PATH];
	fiFindData data;
	// We need to store the string length in case we start recursion
	int fromLen = StringLength(fromDirectory);
	int toLen = StringLength(toDirectory);
	fiHandle handle = fromDev->FindFileBegin(fromDirectory, data);
	if ( fiIsValidHandle(handle) ) {
		while (result) {
			if ((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0)) {
				// Found a directory, so deal with it
				if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY) {
					// Build the new pathname to use
					// truncate the currentPath to compensate for any modifications from recursive calls
					currentPath[fromLen] = '\0';
					currentToPath[toLen] = '\0';
					formatf(currentPath, sizeof(currentPath), "%s\\%s", fromDirectory, data.m_Name);
					formatf(currentToPath, sizeof(currentToPath), "%s\\%s", toDirectory, data.m_Name);
					// Recurse!
					if (!CopyDirectory(currentPath, currentToPath)) {
						fromDev->FindFileEnd(handle);
						return false;
					}
				}
				else {
					// Must be a file, copy it
					// Build the new pathname to use
					currentPath[fromLen] = '\0';
					currentToPath[toLen] = '\0';
					formatf(currentPath, sizeof(currentPath), "%s\\%s", fromDirectory, data.m_Name);
					formatf(currentToPath, sizeof(currentToPath), "%s\\%s", toDirectory, data.m_Name);
					if (!CopySingleFile( currentPath, currentToPath )) {
						fromDev->FindFileEnd(handle);
						return false;
					}
				}
			}

			result = fromDev->FindFileNext(handle, data);
		}
		fromDev->FindFileEnd(handle);
	}
	return true;
}

bool fiDevice::IsAnythingMountedAtMountPoint(const char* mountPath)
{
	int mountPathLen = StringLength(mountPath);

	if (!mountPath[0] || !strchr("/\\",mountPath[mountPathLen-1])) {
		Errorf("fiDevice::DoesMountPointExist - mount name must end in a slash");
		return false;
	}

	if ( mountPathLen >= RAGE_MAX_PATH-2 ) {
		Errorf("fiDevice::DoesMountPointExist - mount name too long");
		return false;
	}

	SYS_CS_SYNC(sm_Token);
	for (int i=0; i<s_Mounts.GetCount(); i++) {
		if (!mountcmp(s_Mounts[i].Path,mountPath,mountPathLen))
		{
			if(s_Mounts[i].Device.GetCount() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	return false;
}

static fiDeviceMemory s_DeviceMemory;
static sysCriticalSectionToken s_DeviceMemoryCS;

#if RSG_DURANGO || RSG_ORBIS || __WIN32PC
const int maxMem = 64;
#else
const int maxMem = 16;
#endif
struct DeviceMemoryState {
	const unsigned char *m_Storage;
	int m_Size;
	int m_Offset;
	bool m_FreeOnClose;
} s_MemoryState[maxMem];

const fiDevice& fiDeviceMemory::GetInstance() {
	return s_DeviceMemory;
}

static int hexval(int c) {
	if (c >= '0' && c <= '9')
		return c-'0';
	else if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	else
		return -1;
}

// Some addresses have their sign bit set, and atoi on Xenon at least will clamp!
unsigned char* atou(const char *s) {
	size_t value = 0;
	while (hexval(*s) != -1)
		value = (value * 16) + (hexval(*s++));
	return (unsigned char*) value;
}

fiHandle fiDeviceMemory::Open(const char *filename,bool readOnly) const {
	if (!readOnly)
		return fiHandleInvalid;	// for now...

	const unsigned char *data = NULL;
	int dataSize = 0;
	bool flag = false;
	/* if (!strncmp(filename,"embedded:/",10)) {
		if (!fiEmbeddedFile::Lookup(filename+10,data,dataSize))
			return fiHandleInvalid;	// unknown embedded file
	}
	else */ if (strncmp(filename,"memory:",7))
		return fiHandleInvalid;	// not our device

	if (!data) {
		AssertMsg(filename[7]=='$',"Please use fiDeviceMemory::MakeMemoryFileName now.");

		// Some cstd libraries put 0x in front of a %p, so skip it if it's there.
		data = atou(filename[9]=='x'?filename+10:filename+8);
		const char *comma = strchr(filename,',');
		if (!comma)
			return fiHandleInvalid;
		dataSize = atoi(comma+1);
		comma = strchr(comma+1,',');
		if (!comma)
			return fiHandleInvalid;
		flag = atoi(comma+1) != 0;
	}

	SYS_CS_SYNC(s_DeviceMemoryCS);
	for (int i=0; i<maxMem; i++) {
		if (s_MemoryState[i].m_Storage == NULL) {
			DeviceMemoryState &d = s_MemoryState[i];
			d.m_Storage = data;
			d.m_Size = dataSize;
			d.m_Offset = 0;
			d.m_FreeOnClose = flag;
			return (fiHandle)(size_t)i;
		}
	}

	Errorf("Ran out of file handles trying to open \"%s\"", filename);
	return fiHandleInvalid;	// out of handles
}


fiHandle fiDeviceMemory::OpenBulk(const char *filename,u64 &outBias) const {
	if (strncmp(filename,"memory:",7))
		return fiHandleInvalid;	// not our device

	AssertMsg(filename[7]=='$',"Please use fiDeviceMemory::MakeMemoryFileName now.");

	// Some cstd libraries put 0x in front of a %p, so skip it if it's there.
	unsigned char *data = atou(filename[9]=='x'?filename+10:filename+8);

	outBias = 0;
	return (fiHandle) data;
}


int fiDeviceMemory::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const {
	sysMemCpy(outBuffer,(char*)handle + offset,bufferSize);
	return bufferSize;
}


fiHandle fiDeviceMemory::Create(const char *filename) const {
	if (strncmp(filename,"memory:",7))
		return fiHandleInvalid;	// not our device

	AssertMsg(filename[7]=='$',"Please use fiDeviceMemory::MakeMemoryFileName now.");

	unsigned char* addr = atou(filename+8);
	const char *comma = strchr(filename,',');
	if (!comma)
		return fiHandleInvalid;
	int size = atoi(comma+1);
	comma = strchr(comma+1,',');
	if (!comma)
		return fiHandleInvalid;
	bool flag = atoi(comma+1) != 0;

	SYS_CS_SYNC(s_DeviceMemoryCS);
	for (int i=0; i<maxMem; i++) {
		if (s_MemoryState[i].m_Storage == NULL) {
			DeviceMemoryState &d = s_MemoryState[i];
			d.m_Storage = addr;
			d.m_Size = size;
			d.m_Offset = 0;
			d.m_FreeOnClose = flag;		// DO WE EVEN WANT THIS TO BE AN OPTION ON CREATE??
			return (fiHandle)(size_t)i;
		}
	}
	return fiHandleInvalid;	// out of handles
}

int fiDeviceMemory::Read(fiHandle _handle,void *outBuffer,int bufferSize) const {
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	if (d.m_Size - d.m_Offset < bufferSize)
		bufferSize = d.m_Size - d.m_Offset;
	sysMemCpy(outBuffer,d.m_Storage + d.m_Offset,bufferSize);
	d.m_Offset += bufferSize;
	return bufferSize;
}

int fiDeviceMemory::Write(fiHandle _handle,const void*buffer,int bufferSize) const {
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	if (d.m_Offset == d.m_Size && bufferSize > 0)
	{
		// buffer is full and it's not going to get any bigger. error out now.
		char name[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(name, RAGE_MAX_PATH, d.m_Storage, d.m_Size, false, NULL);
		Errorf("Overflowed memory device %s", name);
		return -1;
	}
	if (d.m_Size - d.m_Offset < bufferSize) {
		bufferSize = d.m_Size - d.m_Offset;
	}
	sysMemCpy((void*)(d.m_Storage + d.m_Offset),buffer,bufferSize);
	d.m_Offset += bufferSize;
	return bufferSize;
}

void fiDeviceMemory::Sanitize(fiHandle _handle) const 
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return;
	DeviceMemoryState &d = s_MemoryState[handle];
	sysMemSet((void*)d.m_Storage, 0, d.m_Size);
	return;
}

int fiDeviceMemory::Close(fiHandle _handle) const {
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	if (d.m_FreeOnClose) {
		sysMemStartTemp();
		delete[] d.m_Storage;
		sysMemEndTemp();
	}
	d.m_Storage = NULL;
	return 0;
}

int fiDeviceMemory::CloseBulk(fiHandle /*_handle*/) const {
	return 0;
}

int fiDeviceMemory::Size(fiHandle handle) const
{
	return (int)Size64(handle);
}

u64 fiDeviceMemory::Size64(fiHandle handle) const
{
	int h = (int)(size_t)handle;
	Assertf(h >= 0 && h < maxMem, "Bad index (%d) for fiDeviceMemory::Size64", h);
	DeviceMemoryState &d = s_MemoryState[h];
	return d.m_Size;
}

int fiDeviceMemory::Seek(fiHandle _handle,int offset,fiSeekWhence whence) const {
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	if (whence == seekEnd)
		d.m_Offset = d.m_Size + offset;
	else if (whence == seekSet)
		d.m_Offset = offset;
	else
		d.m_Offset += offset;
	if (d.m_Offset < 0)
		d.m_Offset = 0;
	else if (d.m_Offset > d.m_Size)
		d.m_Offset = d.m_Size;
	return d.m_Offset;
}

u64 fiDeviceMemory::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const {
	return Seek(handle,(int)offset,whence);
}

u64 fiDeviceMemory::GetFileTime(const char *) const {
	return 0;
}

bool fiDeviceMemory::SetFileTime(const char *,u64) const {
	return false;
}

u64 fiDeviceMemory::GetFileSize(const char *filename) const {
	if (strncmp(filename,"memory:",7))
		return 0;

	// Some cstd libraries put 0x in front of a %p, so skip it if it's there.
	const char *comma = strchr(filename,',');
	if (!comma)
		return 0;
	return atoi(comma+1);
}


bool fiDeviceMemory::SafeRead(fiHandle _handle,void *outBuffer,int size) const
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
	{
		return false;
	}
	DeviceMemoryState &d = s_MemoryState[handle];
	if (d.m_Size - d.m_Offset < size)
	{
		return false;
	}

	return fiDevice::SafeRead(_handle, outBuffer, size);
}

bool fiDeviceMemory::SafeWrite(fiHandle _handle,const void *buffer,int size) const
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
	{
		return false;
	}
	DeviceMemoryState &d = s_MemoryState[handle];
	if (d.m_Size - d.m_Offset < size)
	{
		return false;
	}

	return fiDevice::SafeWrite(_handle, buffer, size);
}

void fiDevice::MakeMemoryFileName(char *dest,int destSize,const void *data,size_t dataSize,bool freeOnClose,const char *comment) {
	formatf(dest,destSize,"memory:$%p,%d,%d:%s",data,(int)dataSize,freeOnClose,comment);
	Assertf(istrlen(dest) != destSize-1,"Supplied buffer may be too short! Destination Length %d Size %d Comment %s", istrlen(dest), destSize-1, comment);
}

u32 fiDeviceMemory::GetAttributes(const char*) const {
	return FILE_ATTRIBUTE_NORMAL;
}

fiHandle fiDeviceCount::Open(const char * /*filename*/,bool /*readOnly*/) const {
	Errorf("Can't open the count: device for reading");
	return fiHandleInvalid;
}

int fiDeviceCount::Read(fiHandle _handle,void * /*outBuffer*/,int bufferSize) const
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];

	// Is this right? Or should we read "off the end" and increase m_Size?
	// Doing that would simulate reading part of the buffer that's never been written to, which
	// would be a little weird.
	if (d.m_Size - d.m_Offset < bufferSize) {
		bufferSize = d.m_Size - d.m_Offset;
	}

	d.m_Offset += bufferSize;

	return bufferSize;
}

fiHandle fiDeviceCount::Create(const char *filename) const
{
	if (strncmp(filename, "count:", 7))
	{
		return fiHandleInvalid; // not our device
	}

	// Borrow one of the DeviceMemoryState objects to hold the state.
	SYS_CS_SYNC(s_DeviceMemoryCS);
	for (int i=0; i<maxMem; i++) {
		if (s_MemoryState[i].m_Storage == NULL) {
			DeviceMemoryState &d = s_MemoryState[i];
			d.m_Storage = reinterpret_cast<const unsigned char*>(&d.m_Storage); // has to be non-NULL - this should be nice and unique.
			d.m_Size = 0;				// total size
			d.m_Offset = 0;				// current write position
			d.m_FreeOnClose = false;
			return (fiHandle)(size_t)i;
		}
	}

	return fiHandleInvalid;
}

int fiDeviceCount::Write(fiHandle _handle,const void * /*buffer*/,int bufferSize) const
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	d.m_Offset += bufferSize;
	if (d.m_Offset > d.m_Size)
	{
		d.m_Size = d.m_Offset;
	}
	return bufferSize;
}

int fiDeviceCount::Close(fiHandle _handle) const {
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	d.m_Storage = NULL;
	return 0;
}

int fiDeviceCount::Seek(fiHandle _handle,int offset,fiSeekWhence whence) const
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	if (whence == seekEnd)
	{
		d.m_Offset = d.m_Size - offset;
	}
	else if (whence == seekSet)
	{
		d.m_Offset = offset;
	}
	else if (whence == seekCur)
	{
		d.m_Offset += offset;
	}
	if (d.m_Offset < 0)
	{
		d.m_Offset = 0;
	}

	if (d.m_Offset > d.m_Size)
	{
		d.m_Size = d.m_Offset;
	}
	return d.m_Offset;
}

u64 fiDeviceCount::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const
{
	return Seek(handle, (int)offset, whence);
}

int fiDeviceCount::Size(fiHandle _handle) const
{
	int handle = (int)(size_t)_handle;
	if (handle<0||handle>=maxMem||!s_MemoryState[handle].m_Storage)
		return -1;
	DeviceMemoryState &d = s_MemoryState[handle];
	return d.m_Size;
}

u64 fiDeviceCount::Size64(fiHandle handle) const
{
	return Size(handle);
}

u64 fiDeviceCount::GetFileTime(const char * /*filename*/) const 
{
	return 0;
}

bool fiDeviceCount::SetFileTime(const char * /*filename*/,u64 /*timestamp*/) const
{
	return false;
}

static fiDeviceCount s_DeviceCount;

const fiDevice& fiDeviceCount::GetInstance() {
	return s_DeviceCount;
}

//////////////////////////////////////////////////////////////////////////
//  fiDeviceConsole
//////////////////////////////////////////////////////////////////////////
#if !__NO_OUTPUT
fiHandle fiDeviceConsole::Open(const char* filename, bool /*readOnly*/) const {
	if (strncmp(filename, "console:", 7))
	{
		return fiHandleInvalid; // not our device
	}

	return (fiHandle)stdin;
}

fiHandle fiDeviceConsole::Create(const char *filename) const
{
	if (strncmp(filename, "console:", 7))
	{
		return fiHandleInvalid; // not our device
	}

	return (fiHandle)stdout;
}

int fiDeviceConsole::Read(fiHandle handle,void *outBuffer,int bufferSize) const
{
	return (int)fread(outBuffer, 1, bufferSize, (FILE*)handle);
}

int fiDeviceConsole::Write(fiHandle handle,const void *buffer,int bufferSize) const
{
	return (int)fwrite(buffer, 1, bufferSize, (FILE*)handle);
}

int fiDeviceConsole::Seek(fiHandle handle,int offset,fiSeekWhence whence) const
{
	int origin = SEEK_SET;
	switch(whence)
	{
	case seekSet: origin = SEEK_SET; break;
	case seekCur: origin = SEEK_CUR; break;
	case seekEnd: origin = SEEK_END; break;
	}
	return fseek((FILE*)handle, offset, origin);
}

u64 fiDeviceConsole::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const
{
	int origin = SEEK_SET;
	switch(whence)
	{
	case seekSet: origin = SEEK_SET; break;
	case seekCur: origin = SEEK_CUR; break;
	case seekEnd: origin = SEEK_END; break;
	}

#if __WIN32
	return _fseeki64((FILE*)handle, offset, origin);
#else
	// Use whatever system specific function is available in place of this
	return fseek((FILE*)handle, offset, origin);
#endif
}

int fiDeviceConsole::Size(fiHandle handle) const
{
	// Not entirely accurate since we don't know what size the stream started at - but close enough
	return ftell((FILE*)handle);
}

u64 fiDeviceConsole::Size64(fiHandle handle) const
{
#if __WIN32
	return (u64)_ftelli64((FILE*)handle);
#else
	// Use whatever system specific function is available in place of this
	return ftell((FILE*)handle);
#endif
}

int fiDeviceConsole::Close(fiHandle /*handle*/) const
{
	return 0;
}

u64 fiDeviceConsole::GetFileTime(const char * /*filename*/) const 
{
	return 0;
}

bool fiDeviceConsole::SetFileTime(const char * /*filename*/,u64 /*timestamp*/) const
{
	return false;
}

static fiDeviceConsole s_DeviceConsole;

const fiDevice& fiDeviceConsole::GetInstance() {
	return s_DeviceConsole;
}
		   
const char* fiDeviceConsole::GetDebugName() const { return "Console"; }
#endif // !__NO_OUTPUT

const char *fiDeviceLocal::GetDebugName() const { return "Local"; }

const char *fiDeviceMemory::GetDebugName() const { return "Memory"; }

const char *fiDeviceRemote::GetDebugName() const { return "Remote"; }

const char *fiDeviceCount::GetDebugName() const { return "Count"; }


//////////////////////////////////////////////////////////////////////////
//  fiDeviceGrowBuffer
//////////////////////////////////////////////////////////////////////////

extern unsigned char* atou(const char *s);

static fiHandle ParseGrowBuffer(const char *filename)
{
	if(strncmp(filename,"growbuffer:",11))
	{
		return fiHandleInvalid;	// not our device
	}

	// Some cstd libraries put 0x in front of a %p, so skip it if it's there.
	return (fiHandle) atou(filename[11]=='x'?filename+12:filename+11);
}

static fiDeviceGrowBuffer s_DeviceGrowBuffer;

const fiDevice& fiDeviceGrowBuffer::GetInstance()
{
	return s_DeviceGrowBuffer;
}

fiHandle
fiDeviceGrowBuffer::Open(const char *filename,bool readOnly) const
{
	if (!readOnly)
	{
		return fiHandleInvalid;
	}

	return ParseGrowBuffer(filename);
}


fiHandle
fiDeviceGrowBuffer::OpenBulk(const char *filename,u64& /*outBias*/) const
{
	return ParseGrowBuffer(filename);
}


int
fiDeviceGrowBuffer::ReadBulk(fiHandle /*handle*/,u64 /*offset*/,void* /*outBuffer*/,int /*bufferSize*/) const
{
	return 0;
}


fiHandle
fiDeviceGrowBuffer::Create(const char *filename) const
{
	return ParseGrowBuffer(filename);
}

int
fiDeviceGrowBuffer::Read(fiHandle _handle,void* outBuffer,int bufferSize) const
{
	datGrowBuffer* gb = (datGrowBuffer*) _handle;
	return gb->Consume(outBuffer, bufferSize);
}

int
fiDeviceGrowBuffer::Write(fiHandle _handle,const void*buffer,int bufferSize) const
{
	datGrowBuffer* gb = (datGrowBuffer*) _handle;
	return gb->Append(buffer, bufferSize);
}

int
fiDeviceGrowBuffer::Close(fiHandle /*_handle*/) const
{
	return 0;
}

int
fiDeviceGrowBuffer::CloseBulk(fiHandle /*_handle*/) const
{
	return 0;
}

int
fiDeviceGrowBuffer::Seek(fiHandle /*_handle*/,int /*offset*/,fiSeekWhence /*whence*/) const
{
	return -1;
}

u64
fiDeviceGrowBuffer::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const
{
	return Seek(handle,(int)offset,whence);
}

int
fiDeviceGrowBuffer::Size(fiHandle _handle) const
{
	int size = -1;
	Assign(size, Size64(_handle));
	return size;
}

u64
fiDeviceGrowBuffer::Size64(fiHandle _handle) const
{
	datGrowBuffer* gb = (datGrowBuffer*)_handle;
	return gb->Length();
}

u64
fiDeviceGrowBuffer::GetFileTime(const char *) const
{
	return 0;
}

bool
fiDeviceGrowBuffer::SetFileTime(const char *,u64) const
{
	return false;
}

u64
fiDeviceGrowBuffer::GetFileSize(const char *filename) const
{
	if (strncmp(filename,"growbuffer:",11))
		return 0;

	// Some cstd libraries put 0x in front of a %p, so skip it if it's there.
	datGrowBuffer* gb = (datGrowBuffer*) atou(filename[11]=='x'?filename+12:filename+11);
	return gb->Length();
}

void
fiDevice::MakeGrowBufferFileName(char *dest,int destSize,datGrowBuffer* gb)
{
	formatf(dest,destSize,"growbuffer:%p",gb);
}

u32
fiDeviceGrowBuffer::GetAttributes(const char*) const
{
	return FILE_ATTRIBUTE_INVALID;
}

bool
fiDeviceGrowBuffer::SafeRead(fiHandle _handle,void *outBuffer,int size) const
{
	const datGrowBuffer* gb = (const datGrowBuffer*) _handle;
	if(gb->Length() >= (unsigned)size)
	{
		return fiDevice::SafeRead(_handle, outBuffer, size);
	}

	return false;
}

bool
fiDeviceGrowBuffer::SafeWrite(fiHandle _handle,const void *buffer,int size) const
{
	datGrowBuffer* gb = (datGrowBuffer*) _handle;
	if(gb->Preallocate(size))
	{
		return fiDevice::SafeWrite(_handle, buffer, size);
	}

	return false;
}

const char*
fiDeviceGrowBuffer::GetDebugName() const
{
	return "GrowBuffer";
}



}	// namespace rage
