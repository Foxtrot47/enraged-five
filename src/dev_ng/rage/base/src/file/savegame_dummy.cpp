// 
// file/savegame_dummy.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if __XENON
#include <xdk.h>
#endif

#include "savegame.h"
#include "device.h"
#include "asset.h"

#include "atl/array.h"

#if __PPU
#include <sdk_version.h>
#endif

#if !__XENON && (!__PPU || CELL_SDK_VERSION < 0x150004) && ! __WIN32PC && !RSG_ORBIS && ! RSG_DURANGO

namespace rage {

// This is intentionally the same Title ID as the XContent sample.
char *XEX_TITLE_ID = "FFFF011D";

fiSaveGame SAVEGAME;

void fiSaveGame::InitClass()
{
}

void fiSaveGame::ShutdownClass()
{
}

static atRangeArray<int*,fiSaveGame::SIGNIN_COUNT> s_Counts;
static atRangeArray<bool*,fiSaveGame::SIGNIN_COUNT> s_Saves;
static atRangeArray<u32*,fiSaveGame::SIGNIN_COUNT> s_Loads;
static atRangeArray<bool,fiSaveGame::SIGNIN_COUNT> s_Selects;

bool fiSaveGame::BeginSelectDevice(int signInId,u32 /*contentType*/,u32 /*minSpaceAvailable*/,bool /*forceShow*/,bool /*manageStorage*/) {
	s_Selects[signInId] = true;
	return true;
}


bool fiSaveGame::CheckSelectDevice(int signInId) {
	if (s_Selects[signInId]) {
		s_Selects[signInId] = false;
		return true;
	}
	else
		return false;
}


void fiSaveGame::EndSelectDevice(int) {
}


u32 fiSaveGame::GetSelectedDevice(int) {
	return 1;	// same as Xenon Hard Drive
}


void fiSaveGame::SetSelectedDevice(int, u32) {
}


bool fiSaveGame::BeginEnumeration(int signInId,u32 /*contentType*/,Content *outContent,int maxCount)
{
	char buf[32];
	formatf(buf,sizeof(buf),"c:\\XContent\\%s\\%d",XEX_TITLE_ID,signInId);
	const fiDevice *device = fiDevice::GetDevice(buf);
	
	fiFindData data;
	fiHandle h = device->FindFileBegin(buf,data);
	*(s_Counts[signInId] = rage_new int) = 0;
	if (fiIsValidHandle(h)) {
		USES_CONVERSION;
		do {
			if (data.m_Name[0] != '.' && (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) {
				Content &c = outContent[(*s_Counts[signInId])++];
				c.DeviceId = 1;
				c.ContentType = 1;
				safecpy(c.DisplayName,A2W(data.m_Name),128);
			}
		} while (*s_Counts[signInId] < maxCount && device->FindFileNext(h,data));
		device->FindFileEnd(h);
	}
	// An empty enumeration is still valid.
	return true;
}


bool fiSaveGame::CheckEnumeration(int signInId,int &outCount)
{
	if (s_Counts[signInId]) {
		outCount = *s_Counts[signInId];
		return true;
	}
	else
		return false;
}


void fiSaveGame::EndEnumeration(int signInId)
{
	if (s_Counts[signInId]) {
		delete s_Counts[signInId];
		s_Counts[signInId] = NULL;
	}
}


bool fiSaveGame::BeginSave(int signInId,u32 /*contentType*/,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool /*overwrite*/)
{
	if (!saveSize)
		return false;
	char fullfilename[RAGE_MAX_PATH];
	USES_CONVERSION;
	formatf(fullfilename,sizeof(fullfilename),"c:\\XContent\\%s\\%d\\%s\\%s",XEX_TITLE_ID,signInId,W2A(displayName),filename);
	ASSET.CreateLeadingPath(fullfilename);
	fiStream *S = fiStream::Create(fullfilename);
	if (!S)
		return false;
	else {
		s_Saves[signInId] = rage_new bool;
		*s_Saves[signInId] = S->Write(saveData,saveSize) == (int)saveSize;
		S->Close();
		return true;
	}
}


bool fiSaveGame::CheckSave(int signInId,bool &outIsValid,bool& /*fileExists*/)
{
	if (s_Saves[signInId]) {
		outIsValid = *s_Saves[signInId];
		return true;
	}
	else
		return false;
}


void fiSaveGame::EndSave(int signInId)
{
	delete s_Saves[signInId];
	s_Saves[signInId] = NULL;
}

bool fiSaveGame::BeginFreeSpaceCheck(int /*signInId*/,const char* /*filename*/,u32 /*saveSize*/)
{
	return true;
}

bool fiSaveGame::CheckFreeSpaceCheck(int /*signInId*/, int& /*ExtraSpaceRequired*/)
{
	return true;
}

void fiSaveGame::EndFreeSpaceCheck(int /*signInId*/)
{
}

bool fiSaveGame::BeginGetFileSize(int /*signInId*/, const char* /*filename*/, bool /*bCalculateSizeOnDisk*/)
{
	return true;
}

bool fiSaveGame::CheckGetFileSize(int /*signInId*/, u64 &fileSize)
{
	fileSize = 0;
	return true;
}

void fiSaveGame::EndGetFileSize(int /*signInId*/)
{
}


#if HACK_GTA4
bool fiSaveGame::BeginExtraSpaceCheck(int /*signInId*/, const char * /*filename*/, u32 /*saveSize*/)
{
	return true;
}

bool fiSaveGame::CheckExtraSpaceCheck(int /*signInId*/, int & /*ExtraSpaceRequired*/, int & /*TotalHDDFreeSize*/, int & /*SizeOfSystemFileKB*/)
{
	return true;
}

void fiSaveGame::EndExtraSpaceCheck(int /*signInId*/)
{
}
#endif	//	HACK_GTA4

#if HACK_GTA4
int fiSaveGame::CalculateSizeOfSaveGameFile(int /*DataSize*/, int /*SizeOfSystemFiles*/)
{
	Errorf("CalculateSizeOfSaveGameFile only supported for PS3");
	return -1;
}
#endif	//	HACK_GTA4

#if HACK_GTA4
bool fiSaveGame::BeginReadNamesAndSizesOfAllSaveGames(int /*signInId*/, Content* /*outContent*/, int* /*outFileSizes*/, int /*maxCount*/)
{
	return true;
}

bool fiSaveGame::CheckReadNamesAndSizesOfAllSaveGames(int /*signInId*/, int& /*outCount*/)
{
	return true;
}

void fiSaveGame::EndReadNamesAndSizesOfAllSaveGames(int /*signInId*/)
{
}
#endif	//	HACK_GTA4

bool fiSaveGame::BeginLoad(int signInId,u32 /*contentType*/,u32 /*deviceId*/,const char *filename,void *loadData,u32 loadSize)
{
	if (!loadSize)
		return false;
	char fullfilename[RAGE_MAX_PATH];
	formatf(fullfilename,sizeof(fullfilename),"c:\\XContent\\%s\\%d\\%s",XEX_TITLE_ID,signInId,filename);
	ASSET.CreateLeadingPath(fullfilename);
	fiStream *S = fiStream::Open(fullfilename);
	if (!S)
		return false;
	else {
		s_Loads[signInId] = rage_new u32;
		*s_Loads[signInId] = S->Read(loadData,loadSize);
		S->Close();
		return true;
	}
}


bool fiSaveGame::CheckLoad(int signInId,bool& ,u32 &loadSize)
{
	if (s_Loads[signInId]) {
		loadSize = *s_Loads[signInId];
		return true;
	}
	else
		return false;
}


void fiSaveGame::EndLoad(int signInId)
{
	delete s_Loads[signInId];
	s_Loads[signInId] = NULL;
}


bool fiSaveGame::BeginIconSave(int /*signInId*/,u32 /*contentType*/,const char* /*filename*/,const void* /*iconData*/,u32 /*iconSize*/)
{
	return true;
}


bool fiSaveGame::CheckIconSave(int /*signInId*/)
{
	return true;
}


void fiSaveGame::EndIconSave(int /*signInId*/)
{
}


bool fiSaveGame::BeginContentVerify(int /*signInId*/,u32 /*contentType*/, u32 /*deviceId*/, const char* /*filename*/)
{
	return true;
}


bool fiSaveGame::CheckContentVerify(int /*signInId*/,bool& /*verified*/)
{
	return true;
}


void fiSaveGame::EndContentVerify(int /*signInId*/)
{
}


bool fiSaveGame::BeginDelete(int /*signInId*/,u32 /*contentType*/,const char* /*filename*/)
{
	Warningf("Delete is not supported for this platform!");
	return true;
}


bool fiSaveGame::CheckDelete(int /*signInId*/)
{
	return true;
}


void fiSaveGame::EndDelete(int /*signInId*/)
{
}

bool fiSaveGame::BeginGetCreator(int /*signInId*/, u32 /*contentType*/,u32 /*deviceId*/,const char* /*filename*/)
{
	return true;
}

bool fiSaveGame::CheckGetCreator(int /*signInId*/, bool& /*bIsCreator*/)
{
	return true;
}

void fiSaveGame::EndGetCreator(int /*signInId*/)
{
}


u64 fiSaveGame::GetTotalAvailableSpace(int)
{
   return 0;
}

u64 fiSaveGame::CalculateDataSizeOnDisk(int , rage::u32)
{
   return 0;
}

fiSaveGameState::State fiSaveGame::GetState(int /*signInId*/) const
{
	return fiSaveGameState::IDLE;	// a lie!
}

int fiSaveGame::GetMaxUsers()
{
   return 0;
}

bool fiSaveGame::IsStorageDeviceChanged(int /*signInId*/)
{
   return false;
}

fiSaveGame::Errors fiSaveGame::GetError(int /*signInId*/)
{
   return fiSaveGame::SAVE_GAME_SUCCESS;
}

void fiSaveGame::SetStateToIdle(int /*signInId*/)
{   
}

void fiSaveGame::SetSelectedDeviceToAny(int /*signInId*/)
{
}

bool fiSaveGame::IsCurrentDeviceValid(int /*signInId*/)
{
   return false;
}

bool fiSaveGame::IsDeviceValid(int /*signInId*/, u32 /*deviceId*/)
{
	return false;
}

bool fiSaveGame::GetCurrentDeviceData(int /*signInId*/, fiSaveGame::Device* /*outDevice*/)
{
	return false;
	//return GetDeviceData(m_DeviceID, outDevice);
}

bool fiSaveGame::IsStorageDeviceRemoved(int /*signInId*/)
{
	return false;
}

bool fiSaveGame::GetDeviceData(int /*signInId*/, u32 /*deviceId*/, fiSaveGame::Device* /*outDevice*/)
{
	return false;
}

bool fiSaveGame::HasFileBeenAccessed(int , const char* )
{
   return false;
}

void fiSaveGame::SetIcon(char *, const u32)
{

}

void fiSaveGame::SetMaxNumberOfSaveGameFilesToEnumerate(int /*MaxNumSaveGames*/)
{
}

void fiSaveGame::SetSaveGameTitle(const char* /*pSaveGameTitle*/)
{
}

void fiSaveGame::SetCheckForUserBindErrors(const char* /*pBindErrorMessage*/)
{
}

void fiSaveGame::SetUserServiceId(int /* signInId */ , int /* userId */)
{
}

void fiSaveGame::SetMountIndex(int /* signInId */, int /* index */)
{
}

void fiSaveGame::FillDevice(int)
{

}


bool fiSaveGame::BeginGetFileModifiedTime(int /*signInId*/, const char* /*filename*/)
{
	return true;
}

bool fiSaveGame::CheckGetFileModifiedTime(int /*signInId*/, u32& /*modTimeHigh*/, u32& /*modTimeLow*/)
{
	return true;
}

void fiSaveGame::EndGetFileModifiedTime(int /*signInId*/)
{
}

bool fiSaveGame::BeginGetFileExists(int /*signInId*/, const char* /*filename*/)
{
	return true;
}

bool fiSaveGame::CheckGetFileExists(int /*signInId*/, bool& /*exists*/)
{
	return true;
}

void fiSaveGame::EndGetFileExists(int /*signInId*/)
{	
}

}		// namespace rage

#endif	// !__XENON
