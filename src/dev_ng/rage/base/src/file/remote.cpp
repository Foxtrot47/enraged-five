//
// file/remote.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "remote.h"

#if __ENABLE_RFS

#include "assert.h"
#include "stream.h"
#include "atl/string.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/tcpip.h"
#include "math/amath.h"
#include "system/bootmgr.h"
#include "system/exec.h"
#include "system/criticalsection.h"
#include "system/endian.h"
#include "system/exception.h"
#include "system/memory.h"
#include "system/namedpipe.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timemgr.h"
#include "system/timer.h"
#include "system/tmcommands.h"
#include "grcore/device.h"

#include <set>

#include <stdio.h>		// for debugging
#include <stdlib.h>

#if __WIN32
#include "system/xtl.h"
#include "bank/XMessageBox.h"
#endif

#if __XENON
#include "system/cache.h"
#endif

#if __PPU		// For turning Nagle off
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

#if RSG_ORBIS
#include <net.h>
#endif

#if RSG_PC
namespace rage {
	unsigned CustomMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType, UINT uOptions);
}
#endif

#ifdef SYSTRAY
extern int AssertMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);
#endif

#define USE_CACHED_DIRECTORY_INFO	0


namespace rage {

XPARAM(nopopups);
PARAM(disablerfscaching, "[file] Don't use the directory caching with SysTrayRFS");
PARAM(rfschunksize, "[file] Size of chunk to use when transfering data, in bytes");

// DOM-IGNORE-BEGIN
#define MAX_READ_CHUNK_SIZE		(65536)

#ifdef IS_SERVER
const int remoteReadTransferSize = MAX_READ_CHUNK_SIZE;
#else // IS_SERVER
const int remoteReadTransferSize = 16384;
#endif // IS_SERVER
const int remoteWriteTransferSize = 4096;	// write performance isn't as critical and we want to 
											// save stack space so that Displayf when file logging is enabled doesn't crash.
											// (no longer relevant -- the buffer is static and protected by a critsec)

static u32 s_LastMessageBoxShownTimestamp;

// Bump this on major version changes.
#define PROTO_CORE	6

// Enumeration of all commands supported by RFS
enum {
	CORE_CMD_RESET,
	CORE_CMD_RESETR,
	CORE_CMD_OPEN,
	CORE_CMD_OPENR,
	CORE_CMD_SEEK,
	CORE_CMD_SEEKR,
	CORE_CMD_WRITE,
	CORE_CMD_WRITER,
	CORE_CMD_READ,
	CORE_CMD_READR,
	CORE_CMD_CLOSE,
	CORE_CMD_CLOSER,
	CORE_CMD_TTY,
	CORE_CMD_TTYR,
	CORE_CMD_EXIT,
	CORE_CMD_OUTOFDATE,
	CORE_CMD_OUTOFDATER,
	CORE_CMD_UNLINK,
	CORE_CMD_UNLINKR,
	CORE_CMD_MESSAGEBOX,
	CORE_CMD_MESSAGEBOXR,
	CORE_CMD_GETFILETIME,
	CORE_CMD_GETFILETIMER,
	CORE_CMD_SETFILETIME,
	CORE_CMD_SETFILETIMER,
	CORE_CMD_FINDFIRSTFILE,
	CORE_CMD_FINDFIRSTFILER,
	CORE_CMD_FINDNEXTFILE,
	CORE_CMD_FINDNEXTFILER,
	CORE_CMD_FINDCLOSE,
	CORE_CMD_FINDCLOSER,
	CORE_CMD_MKDIR,
	CORE_CMD_MKDIRR,
	CORE_CMD_RMDIR,
	CORE_CMD_RMDIRR,
	CORE_CMD_GETATTRIBUTE,
	CORE_CMD_GETATTRIBUTER,
	CORE_CMD_SETATTRIBUTE,
	CORE_CMD_SETATTRIBUTER,
	CORE_CMD_EXEC,
	CORE_CMD_EXECR,
    CORE_CMD_PING,
	CORE_CMD_OPENBULK,
	CORE_CMD_OPENBULKR,
	CORE_CMD_READBULK,
	CORE_CMD_READBULKR,
	CORE_CMD_CLOSEBULK,
	CORE_CMD_CLOSEBULKR,
	CORE_CMD_GETENV,
	CORE_CMD_GETENVR,
	CORE_CMD_SEEK64,
	CORE_CMD_SEEK64R,
	CORE_CMD_ASSERTMESSAGEBOX,
	CORE_CMD_GETFILECOUNT,
	CORE_CMD_GETFILECOUNTR,
	CORE_CMD_GETATTRIBUTEDIR,
	CORE_CMD_GETATTRIBUTEDIRR,
	CORE_CMD_PREFETCHDIR,
	CORE_CMD_PREFETCHDIRR,
	CORE_CMD_INVALIDATE_PREFETCHDIR,
	CORE_CMD_INVALIDATE_PREFETCHDIRR,
	CORE_CMD_RESET_CACHE,
	CORE_CMD_RESET_CACHER,
};

struct remotePrefetchRequest {
	remotePrefetchRequest() {}
	remotePrefetchRequest(const char *directory) : m_Directory(directory) {}

	atString m_Directory;
};

#ifdef IS_SERVER
// Server: Outstanding cache requests
static sysMessageQueue<remotePrefetchRequest, 1024> s_PrefetchRequests;

// Server: Prefetched directories, key is a hash of the normalized directory
static atMap<u32, atArray<fiRemoteDirEntry> > s_FetchedDirectories;

// Server: CS for s_FetchedDirectories
static sysCriticalSectionToken s_FetchedDirectoriesCs;
#endif // IS_SERVER

// Average throughput, measured by full-size reads.
#if !__FINAL
static float s_AverageThroughput;
#endif // !__FINAL

#if __BE
inline u32 remote_u32(u32 v) {
	return sysEndian::Swap(v);
}
inline u64 remote_u64(u64 v) {
	return sysEndian::Swap(v);
}
inline u64 remote_s64(s64 v) {
	return sysEndian::Swap(v);
}
#else
inline u32 remote_u32(u32 v) { return v; }
inline u64 remote_u64(u64 v) { return v; }
inline s64 remote_s64(s64 v) { return v; }
#endif
inline int remote_int(int v) { return (int) remote_u32((u32)(v)); }

struct remoteAny {
	remoteAny(int c,int r,int l) : protocol(remote_int(PROTO_CORE)), command(remote_int(c)), result(remote_int(r)), length(remote_int(l)) { }
	int protocol;
	int command;
	int result;
	int length;
};

// Callbacks to actually transfer data; setup by higher code
extern bool (*coreRemoteSend)(remoteAny*);
extern bool (*coreRemoteRecv)(remoteAny*);

struct coreReset : public remoteAny {
	coreReset() : remoteAny(CORE_CMD_RESET,0,sizeof(coreReset)) { }
};

struct coreResetReply : public remoteAny {
	coreResetReply() : remoteAny(CORE_CMD_RESETR,0,sizeof(coreReset)) { }
};

struct coreOpen : public remoteAny {
	coreOpen(const char *nm,bool ro,bool cr) : remoteAny(CORE_CMD_OPEN,0,sizeof(coreOpen) + StringLength(nm) - (RAGE_MAX_PATH-1)), readonly(remote_int(ro)), create(remote_int(cr)) {
		safecpy(filename,nm,sizeof(filename));
	}
	int readonly;
	int create;
	char filename[RAGE_MAX_PATH];
};

struct coreOpenReply : public remoteAny {
	coreOpenReply(int r) : remoteAny(CORE_CMD_OPENR,r,sizeof(coreOpenReply)) { }
};

struct coreOpenBulk : public remoteAny {
	coreOpenBulk(const char *nm) : remoteAny(CORE_CMD_OPENBULK,0,sizeof(coreOpenBulk) + StringLength(nm) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,nm,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreOpenBulkReply : public remoteAny {
	coreOpenBulkReply(int r) : remoteAny(CORE_CMD_OPENBULKR,r,sizeof(coreOpenBulkReply)) { }
};

struct coreSeek : public remoteAny {
	coreSeek(int h,int o,fiSeekWhence s) : remoteAny(CORE_CMD_SEEK,0,sizeof(coreSeek)), handle(remote_int(h)), offset(remote_int(o)), whence(remote_int(s)) { }
	int handle;
	int offset;
	int whence;
};

struct coreSeekReply : public remoteAny {
	coreSeekReply(int r) : remoteAny(CORE_CMD_SEEKR,r,sizeof(coreSeekReply)) { }
};

struct coreSeek64 : public remoteAny {
	coreSeek64(int h,s64 o,fiSeekWhence s) : remoteAny(CORE_CMD_SEEK64,0,sizeof(coreSeek64)), handle(remote_int(h)), offset(remote_s64(o)), whence(remote_int(s)) { }
	int handle;
	int whence;
	s64 offset;
};

struct coreSeek64Reply : public remoteAny {
	coreSeek64Reply(u64 r) : remoteAny(CORE_CMD_SEEK64R,0,sizeof(coreSeek64Reply)) , newpos(r) { }
	u64 newpos;
};

struct coreRead : public remoteAny {
	coreRead(int h,int a) : remoteAny(CORE_CMD_READ,0,sizeof(coreRead)), handle(remote_int(h)), amount(remote_int(a)) { }
	int handle;
	int amount;
};

struct coreReadReply : public remoteAny {
	coreReadReply(int r) : remoteAny(CORE_CMD_READR,r,r<0?sizeof(remoteAny):sizeof(coreReadReply) + r) { }
};

struct coreReadBulk : public remoteAny {
	coreReadBulk(int h,u64 o,int a) : remoteAny(CORE_CMD_READBULK,0,sizeof(coreReadBulk)), handle(remote_int(h)), amount(remote_int(a)), offset(remote_u64(o)) { }
	int handle;
	int amount;
	u64 offset;
};

struct coreReadBulkReply : public remoteAny {
	coreReadBulkReply(int r) : remoteAny(CORE_CMD_READBULKR,r,r<0?sizeof(remoteAny):sizeof(coreReadBulkReply) + r) { }
};

struct coreWrite : public remoteAny {
	coreWrite(int h,int a) : remoteAny(CORE_CMD_WRITE,0,sizeof(coreWrite) + a), handle(remote_int(h)), amount(remote_int(a)) { }
	int handle;
	int amount;
};

struct coreWriteReply : public remoteAny {
	coreWriteReply(int r) : remoteAny(CORE_CMD_WRITER,r,sizeof(coreWriteReply)) { }
};

struct coreClose : public remoteAny {
	coreClose(int h) : remoteAny(CORE_CMD_CLOSE,0,sizeof(coreClose)), handle(remote_int(h)) { }
	int handle;
};

struct coreCloseReply : public remoteAny {
	coreCloseReply(int r) : remoteAny(CORE_CMD_CLOSER,r,sizeof(coreCloseReply)) { }
};

struct coreCloseBulk : public remoteAny {
	coreCloseBulk(int h) : remoteAny(CORE_CMD_CLOSEBULK,0,sizeof(coreCloseBulk)), handle(remote_int(h)) { }
	int handle;
};

struct coreCloseBulkReply : public remoteAny {
	coreCloseBulkReply(int r) : remoteAny(CORE_CMD_CLOSEBULKR,r,sizeof(coreCloseBulkReply)) { }
};

struct coreTty: public remoteAny {
	coreTty(const char *m) : remoteAny(CORE_CMD_TTY,0,sizeof(coreTty) + StringLength(m) - 511) { 
		safecpy(message, m, sizeof(message));
	}
	char message[512];
};

struct coreTtyReply : public remoteAny {
	coreTtyReply() : remoteAny(CORE_CMD_TTYR,0,sizeof(coreTtyReply)) { }
};

struct coreExit: public remoteAny {
	coreExit() : remoteAny(CORE_CMD_EXIT,0,sizeof(coreExit)) { }
};

struct coreOOD: public remoteAny {
	coreOOD(const char *f1,const char *f2) : remoteAny(CORE_CMD_OUTOFDATE,0,sizeof(coreOOD)) { 
		safecpy(filename1,f1,sizeof(filename1));
		safecpy(filename2,f2,sizeof(filename1));
	}
	char filename1[RAGE_MAX_PATH];
	char filename2[RAGE_MAX_PATH];
};

struct coreOODReply: public remoteAny {
	coreOODReply(int r) : remoteAny(CORE_CMD_OUTOFDATER,r,sizeof(coreOODReply)) { }
};

struct coreUnlink: public remoteAny {
	coreUnlink(const char *m) : remoteAny(CORE_CMD_UNLINK,0,sizeof(coreUnlink) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreUnlinkReply: public remoteAny {
	coreUnlinkReply(int r) : remoteAny(CORE_CMD_UNLINKR,r,sizeof(coreUnlinkReply)) { }
};

struct coreGetAttr: public remoteAny {
	coreGetAttr(const char *m) : remoteAny(CORE_CMD_GETATTRIBUTE,0,sizeof(coreGetAttr) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreGetAttrReply: public remoteAny {
	coreGetAttrReply(u32 r) : remoteAny(CORE_CMD_GETATTRIBUTER,r,sizeof(coreGetAttrReply)) { }
};

struct coreGetFileCount: public remoteAny {
	coreGetFileCount(const char *m) : remoteAny(CORE_CMD_GETFILECOUNT,0,sizeof(coreGetFileCount) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(directory,m,sizeof(directory));
	}
	char directory[RAGE_MAX_PATH];
};

struct coreGetFileCountReply: public remoteAny {
	coreGetFileCountReply(u32 r) : remoteAny(CORE_CMD_GETFILECOUNTR,r,sizeof(coreGetFileCountReply)) { }
};

struct coreGetAttrDir: public remoteAny {
	coreGetAttrDir(const char *m, u32 toSkipCount, u32 expectedResultCount) : remoteAny(CORE_CMD_GETATTRIBUTEDIR,0,sizeof(coreGetAttrDir)) {
		safecpy(directory,m,sizeof(directory));
		resultCount = remote_u32(expectedResultCount);
		skipCount = remote_u32(toSkipCount);
	}
	char directory[RAGE_MAX_PATH];
	u32 resultCount;
	u32 skipCount;
};

// Max number of fiRemoteDirEntry to send in one batch.
#ifdef IS_SERVER
	const u32 remoteMaxDirEntries = ((u32) remoteReadTransferSize - (u32) sizeof(coreGetAttrDir)) / (u32) sizeof(fiRemoteDirEntry);
	u32 fiDeviceRemote::GetRemoteMaxDirEntries() const
	{
		return remoteMaxDirEntries;
	}
#else // IS_SERVER
	u32 fiDeviceRemote::GetRemoteMaxDirEntries() const
	{
		return ((u32) GetReadChunkSize() - (u32) sizeof(coreGetAttrDir)) / (u32) sizeof(fiRemoteDirEntry);
	}
#endif // IS_SERVER

struct coreGetAttrDirReply: public remoteAny {
	coreGetAttrDirReply(int resultCount) : remoteAny(CORE_CMD_GETATTRIBUTEDIRR,resultCount,sizeof(coreGetAttrDirReply) + sizeof(fiRemoteDirEntry) * resultCount) {
	}
};

struct corePrefetchDir: public remoteAny {
	corePrefetchDir(const char *m) : remoteAny(CORE_CMD_PREFETCHDIR,0,sizeof(corePrefetchDir) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(directory,m,sizeof(directory));
	}
	char directory[RAGE_MAX_PATH];
};

struct corePrefetchDirReply: public remoteAny {
	corePrefetchDirReply(u32 r) : remoteAny(CORE_CMD_PREFETCHDIRR,r,sizeof(corePrefetchDirReply)) { }
};

struct coreInvalidatePrefetchDir: public remoteAny {
	coreInvalidatePrefetchDir(const char *m) : remoteAny(CORE_CMD_INVALIDATE_PREFETCHDIR,0,sizeof(coreInvalidatePrefetchDir) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(directory,m,sizeof(directory));
	}
	char directory[RAGE_MAX_PATH];
};

struct coreInvalidatePrefetchDirReply: public remoteAny {
	coreInvalidatePrefetchDirReply(u32 r) : remoteAny(CORE_CMD_INVALIDATE_PREFETCHDIRR,r,sizeof(coreInvalidatePrefetchDirReply)) { }
};

struct coreResetCache: public remoteAny {
	coreResetCache() : remoteAny(CORE_CMD_RESET_CACHE,0,sizeof(coreResetCache)) {
	}
};

struct coreResetCacheReply: public remoteAny {
	coreResetCacheReply(u32 r) : remoteAny(CORE_CMD_RESET_CACHER,r,sizeof(coreResetCache)) { }
};


struct coreSetAttr: public remoteAny {
	coreSetAttr(const char *m,u32 a) : remoteAny(CORE_CMD_SETATTRIBUTE,a,sizeof(coreSetAttr) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreSetAttrReply: public remoteAny {
	coreSetAttrReply(u32 r) : remoteAny(CORE_CMD_SETATTRIBUTER,r,sizeof(coreSetAttrReply)) { }
};

struct coreMkdir: public remoteAny {
	coreMkdir(const char *m) : remoteAny(CORE_CMD_MKDIR,0,sizeof(coreMkdir) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreMkdirReply: public remoteAny {
	coreMkdirReply(int r) : remoteAny(CORE_CMD_MKDIRR,r,sizeof(coreMkdirReply)) { }
};

struct coreRmdir: public remoteAny {
	coreRmdir(const char *m, bool deleteContents) : remoteAny(CORE_CMD_RMDIR,0,sizeof(coreRmdir) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
		removeContents = remote_u32( deleteContents ? 1 : 0 );
	}
	u32 removeContents;
	char filename[RAGE_MAX_PATH];
};

struct coreRmdirReply: public remoteAny {
	coreRmdirReply(int r) : remoteAny(CORE_CMD_RMDIRR,r,sizeof(coreRmdirReply)) { }
};

struct coreMessageBox: public remoteAny {
	coreMessageBox(const char *t,const char *m,unsigned f) : remoteAny(CORE_CMD_MESSAGEBOX,0,sizeof(coreMessageBox)) {
		safecpy(title,t,sizeof(title));
		safecpy(message,m,sizeof(message));
		flags = remote_u32(f);
	}
	char title[128];
	char message[512];
	unsigned flags;
};

struct coreAssertMessageBox: public remoteAny {
	coreAssertMessageBox(const char *t,const char *m,unsigned f) : remoteAny(CORE_CMD_ASSERTMESSAGEBOX,0,sizeof(coreAssertMessageBox)) {
		safecpy(title,t,sizeof(title));
		safecpy(message,m,sizeof(message));
		flags = remote_u32(f);
	}
	char title[128];
	char message[512];
	unsigned flags;
};

struct coreMessageBoxReply: public remoteAny {
	coreMessageBoxReply(int r) : remoteAny(CORE_CMD_MESSAGEBOXR,r,sizeof(coreMessageBoxReply)) { }
};

struct coreGetFileTime: public remoteAny {
	coreGetFileTime(const char *m) : remoteAny(CORE_CMD_GETFILETIME,0,sizeof(coreGetFileTime) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreSetFileTime: public remoteAny {
	coreSetFileTime(const char *m,u64 ft) : remoteAny(CORE_CMD_SETFILETIME,0,sizeof(coreSetFileTime) + StringLength(m) - (RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
		filetime = remote_u64(ft);
	}
	char filename[RAGE_MAX_PATH];
	u64 filetime;
};

struct coreGetFileTimeReply: public remoteAny {
	coreGetFileTimeReply(u64 ft) : remoteAny(CORE_CMD_GETFILETIMER,0,sizeof(coreGetFileTimeReply)), filetime(ft) {	}
	u64 filetime;
};

struct coreSetFileTimeReply: public remoteAny {
	coreSetFileTimeReply(bool result) : remoteAny(CORE_CMD_SETFILETIMER,result,sizeof(coreSetFileTimeReply)) {	}
};

struct coreFindFirstFile: public remoteAny {
	coreFindFirstFile(const char *m) : remoteAny(CORE_CMD_FINDFIRSTFILE,0,sizeof(coreFindFirstFile)+StringLength(m)-(RAGE_MAX_PATH-1)) {
		safecpy(filename,m,sizeof(filename));
	}
	char filename[RAGE_MAX_PATH];
};

struct coreFindFirstFileReply: public remoteAny {
	coreFindFirstFileReply(int handle) : remoteAny(CORE_CMD_FINDFIRSTFILER,handle,sizeof(coreFindFirstFileReply)) { }
	fiFindData data;
};

struct coreFindNextFile: public remoteAny {
	coreFindNextFile(int h) : remoteAny(CORE_CMD_FINDNEXTFILE,0,sizeof(coreFindNextFile)), handle(remote_int(h)) { }
	int handle;
};

struct coreFindNextFileReply: public remoteAny {
	coreFindNextFileReply(bool flag) : remoteAny(CORE_CMD_FINDNEXTFILER,flag,sizeof(coreFindNextFileReply)) { }
	fiFindData data;
};

struct coreFindClose : public remoteAny {
	coreFindClose(int h) : remoteAny(CORE_CMD_FINDCLOSE,0,sizeof(coreFindClose)), handle(remote_int(h)) { }
	int handle;
};

struct coreFindCloseReply : public remoteAny {
	coreFindCloseReply(int r) : remoteAny(CORE_CMD_FINDCLOSER,r,sizeof(coreFindCloseReply)) { }
};

struct coreExec : public remoteAny {
	coreExec(const char *cmdline) : remoteAny(CORE_CMD_EXEC,0,sizeof(coreExec)) {
		safecpy(buffer,cmdline,sizeof(buffer));
	}
	char buffer[RAGE_MAX_PATH];
};

struct coreExecReply: public remoteAny {
	coreExecReply(int result) : remoteAny(CORE_CMD_EXECR,result,sizeof(coreExecReply)) { }
};

struct coreGetEnv: public remoteAny {
	coreGetEnv(const char *env) : remoteAny(CORE_CMD_GETENV,0,sizeof(coreGetEnv)) {
		safecpy(buffer,env,sizeof(buffer));
	}
	char buffer[RAGE_MAX_PATH];
};

struct coreGetEnvReply: public remoteAny {
	coreGetEnvReply(const char *env) : remoteAny(CORE_CMD_GETENVR,env?0:1,sizeof(coreGetEnvReply)) {
		if (env)
			safecpy(buffer,env,sizeof(buffer));
	}
	char buffer[RAGE_MAX_PATH];
};


#define DEVICE	(fiDeviceLocal::GetInstance())

#if defined IS_SERVER
bool g_RfsNeverTimeOut = false;
#endif

#if !defined(IS_SERVER)
// Disable inlining so function is more obvious in the callstack
#ifdef __GNUC__
__attribute__((__noinline__))
#elif defined (_MSC_VER)
__declspec(noinline)
#endif
static void SysTrayRFSDisconnected()
{
	if(1 EXCEPTION_HANDLING_ONLY(&& !sysException::HasBeenThrown()))
	{
#if !__NO_OUTPUT
		char errmsg[128];
		snprintf(errmsg, sizeof(errmsg)-1, "The SysTrayRFS connection appears to have been lost.\n%s", fiDeviceTcpIp::GetLastError());
		errmsg[sizeof(errmsg)-1] = '\0';
		diagPrintDefault(errmsg);
		sysIpcSleep(30); // Make sure any buffer from diagPrintDefault is flushed
		fiRemoteShowMessageBox(errmsg, "FATAL ERROR", MB_OK, IDOK);
#endif
#if SYSTMCMD_ENABLE
		XENON_ONLY(if (sysBootManager::IsDevkit()))
		{
			sysTmCmdStopNoErrorReport();
		}
#endif
	}
	for (;;) sysIpcSleep(1);
}
#endif // !defined(IS_SERVER)

static bool __recv(fiHandle hSocket,void *data,int length)
{
#if !defined(IS_SERVER)

    bool result = fiDeviceTcpIp::GetInstance().SafeRead(hSocket,data,length);
	if (!result)
	{
		SysTrayRFSDisconnected();
	}
	return result;

#else   //defined(IS_SERVER)

    int done = 0;
	while (done < length)
    {
        fd_set readFds;

        //Select on the socket with a timeout so we don't block indefinitely.
        //We don't want to block b/c we want to send pings every so often.

        FD_ZERO( &readFds );
        FD_SET( (SOCKET)hSocket, &readFds );
        struct timeval timeout = { 2, 0 };
        int result = select( 1, &readFds, NULL, NULL, &timeout );

        if( 1 == result && FD_ISSET( (SOCKET)hSocket, &readFds ) )
        {
	        result = recv((SOCKET)hSocket,(char*)data+done,length-done,0);
	        if(0 == result)
            {
                //Return value of zero indicates the other end of the connection
                //has been shut down.
                break;
            }
	        else if(result < 0)
            {
#if !__FINAL
				Errorf("__recv: recv returned %d(%x)",WSAGetLastError(),WSAGetLastError());
#endif
                break;
	        }

            done += result;
        }
        else if (!g_RfsNeverTimeOut)
        {
            //Send a ping to make sure the other end is still active.
            //The ping needn't be answered, we simply need to know it got
            //through.  If it doesn't get through the next recv will fail,
            //probably with a CONNRESET error.
            remoteAny ping( CORE_CMD_PING, 0, 0 );
            result = send((SOCKET)hSocket,(const char*)&ping,sizeof(ping),0);

	        if (result < 0)
            {
#if !__FINAL
				Errorf("__recv: ping failed");
#endif
                break;
	        }
        }
    }

    return done == length;

#endif  //defined(IS_SERVER)
}

static bool __send(fiHandle hSocket,const void *data,int length)
{
#if !defined(IS_SERVER)

    bool result = fiDeviceTcpIp::GetInstance().SafeWrite( hSocket, data, length );
	if (!result)
	{
		SysTrayRFSDisconnected();
	}
	return result;

#else   //defined(IS_SERVER)

    int done = 0;
	while (done < length)
    {
	    int result = send((SOCKET)hSocket,(const char*)data+done,length-done,0);
	    if (!g_RfsNeverTimeOut && result < 0)
        {
#if !__FINAL
			Errorf("__send: send returned %d(%x)",WSAGetLastError(),WSAGetLastError());
#endif
            break;
	    }

        done += result;
    }

	return done == length;

#endif  //defined(IS_SERVER)
}

static fiHandle s_ClientSocket = fiHandleInvalid;
static char s_ClientAddr[64];
static int s_ClientPort;			// nonzero if we were able to connect

static bool clientInit(const char *addr,int port) {
	// OutputDebugString("Connecting to rfs server at ");
	// OutputDebugString(addr);
	// OutputDebugString("...");
	safecpy(s_ClientAddr, addr);

	int tryCount = 20;
	s_ClientSocket = fiHandleInvalid;

	while(tryCount-- && (s_ClientSocket == fiHandleInvalid))
	{
		s_ClientSocket = fiDeviceTcpIp::Connect(addr,port);
		sysIpcSleep(50);
	}

	if (s_ClientSocket != fiHandleInvalid) {
		s_ClientPort = port;
		return true;
	}
	else
		return false;
}

static sysCriticalSectionToken clientToken;

static void clientRequestAndWaitReplyEx(fiHandle h,remoteAny* sendPkt,unsigned sendSize,const void *sendExtra,unsigned sendExtraLen,remoteAny* recvPkt,unsigned recvSize,void *recvExtra,unsigned recvExtraSize) {
	/* static const char *cmds[] =
	{
		"CORE_CMD_RESET",
		"CORE_CMD_RESETR",
		"CORE_CMD_OPEN",
		"CORE_CMD_OPENR",
		"CORE_CMD_SEEK",
		"CORE_CMD_SEEKR",
		"CORE_CMD_WRITE",
		"CORE_CMD_WRITER",
		"CORE_CMD_READ",
		"CORE_CMD_READR",
		"CORE_CMD_CLOSE",
		"CORE_CMD_CLOSER",
		"CORE_CMD_TTY",
		"CORE_CMD_TTYR",
		"CORE_CMD_EXIT",
		"CORE_CMD_OUTOFDATE",
		"CORE_CMD_OUTOFDATER",
		"CORE_CMD_UNLINK",
		"CORE_CMD_UNLINKR",
		"CORE_CMD_MESSAGEBOX",
		"CORE_CMD_MESSAGEBOXR",
		"CORE_CMD_GETFILETIME",
		"CORE_CMD_GETFILETIMER",
		"CORE_CMD_SETFILETIME",
		"CORE_CMD_SETFILETIMER",
		"CORE_CMD_FINDFIRSTFILE",
		"CORE_CMD_FINDFIRSTFILER",
		"CORE_CMD_FINDNEXTFILE",
		"CORE_CMD_FINDNEXTFILER",
		"CORE_CMD_FINDCLOSE",
		"CORE_CMD_FINDCLOSER",
		"CORE_CMD_MKDIR",
		"CORE_CMD_MKDIRR",
		"CORE_CMD_RMDIR",
		"CORE_CMD_RMDIRR",
		"CORE_CMD_GETATTRIBUTE",
		"CORE_CMD_GETATTRIBUTER",
		"CORE_CMD_SETATTRIBUTE",
		"CORE_CMD_SETATTRIBUTER",
		"CORE_CMD_EXEC",
		"CORE_CMD_EXECR",
		"CORE_CMD_PING",
		"CORE_CMD_OPENBULK",
		"CORE_CMD_OPENBULKR",
		"CORE_CMD_READBULK",
		"CORE_CMD_READBULKR",
		"CORE_CMD_CLOSEBULK",
		"CORE_CMD_CLOSEBULKR",
		"CORE_CMD_GETENV",
		"CORE_CMD_GETENVR",
		"CORE_CMD_SEEK64",
		"CORE_CMD_SEEK64R"
		"CORE_CMD_ASSERTMESSAGEBOX",
	}; */

	HANG_DETECT_SAVEZONE_ENTER();

	// printf("%08u remoteRequestAndWaitReply - SEND %d %s %d %d (%u+%u)\n",sysTimer::GetSystemMsTime(),remote_int(sendPkt->protocol), cmds[remote_int(sendPkt->command)],remote_int(sendPkt->result),remote_int(sendPkt->length),sendSize,sendExtraLen);
	__send(h,sendPkt,sendSize);
	if (sendExtraLen)
		__send(h,sendExtra,sendExtraLen);

	if( recvPkt && recvSize )
    {
	    int command = remote_int(recvPkt->command);
        do 
        {
    	    // Receive incoming header
	        if (!__recv(h,recvPkt,sizeof(remoteAny)))
				break;
	        recvPkt->protocol = remote_int(recvPkt->protocol);
	        recvPkt->command = remote_int(recvPkt->command);
	        recvPkt->result = remote_int(recvPkt->result);
	        recvPkt->length = remote_int(recvPkt->length);

            if( CORE_CMD_PING == recvPkt->command )
            {
                continue;
            }

			// printf("%08u remoteRequestAndWaitReply - RECV %d %s %d %d (%u+%u)\n",sysTimer::GetSystemMsTime(),recvPkt->protocol,cmds[recvPkt->command],recvPkt->result,recvPkt->length,recvSize,recvExtraSize);
			if (recvPkt->command == command && recvPkt->length <= (int)(recvSize+recvExtraSize)) {
		        // Receive payload
				if (recvPkt->length > (int)sizeof(remoteAny)) {
					unsigned remain = recvPkt->length - sizeof(remoteAny);
					unsigned thisPart = remain;
					if (thisPart > recvSize - sizeof(remoteAny))
						thisPart = recvSize - sizeof(remoteAny);
					if (thisPart)
						__recv(h,(recvPkt+1),thisPart);
					remain -= thisPart;
					if (remain)
						__recv(h,recvExtra,remain);
				}
			}
        } while( CORE_CMD_PING == recvPkt->command );
    }

	HANG_DETECT_SAVEZONE_EXIT("clientRequestAndWaitReplyEx");
}

static void clientRequestAndWaitReply(remoteAny* sendPkt,remoteAny* recvPkt,unsigned recvSize) {
	sysCriticalSection clientSection(clientToken);
	clientRequestAndWaitReplyEx(s_ClientSocket,sendPkt,remote_int(sendPkt->length),NULL,0,recvPkt,recvSize,NULL,0);
}



static void clientRequestReset() {
	coreReset reset;
	coreResetReply resetr;
	clientRequestAndWaitReply(&reset,&resetr,sizeof(resetr));
}

#if !__FINAL
float fiDeviceRemote::GetAverageThroughput() const {
	return s_AverageThroughput;
}
#endif // !__FINAL

/* If we ever go to 64-bit windows this code needs to be revised! */

fiHandle fiDeviceRemote::Open(const char *filename,bool readOnly) const {
	coreOpen open(filename,readOnly,false);
	coreOpenReply openr(-1);
	clientRequestAndWaitReply(&open,&openr,sizeof(openr));

#if __DEVPROF
	if ((fiHandle)(size_t)openr.result != fiHandleInvalid)
		sm_DeviceProfiler->OnOpen((fiHandle)(size_t)openr.result, filename);
#endif // __DEVPROF

	return (fiHandle)(size_t)openr.result;
}

fiHandle fiDeviceRemote::OpenBulk(const char *filename,u64 &outBias) const {
	coreOpenBulk open(filename);
	coreOpenBulkReply openr(-1);
	clientRequestAndWaitReply(&open,&openr,sizeof(openr));
	outBias = 0;

#if __DEVPROF
	if ((fiHandle)(size_t)openr.result != fiHandleInvalid)
		sm_DeviceProfiler->OnOpen((fiHandle)(size_t)openr.result, filename);
#endif // __DEVPROF

	return (fiHandle)(size_t)openr.result;
}

fiHandle fiDeviceRemote::Create(const char *filename) const {
	coreOpen open(filename,false,true);
	coreOpenReply openr(-1);
	clientRequestAndWaitReply(&open,&openr,sizeof(openr));

#if __DEVPROF
	if ((fiHandle)(size_t)openr.result != fiHandleInvalid)
		sm_DeviceProfiler->OnOpen((fiHandle)(size_t)openr.result, filename);
#endif // __DEVPROF

	return (fiHandle)(size_t)openr.result;
}

int fiDeviceRemote::Seek(fiHandle handle,int offset,fiSeekWhence whence) const {
	coreSeek seek((int)(size_t)handle,offset,whence);
	coreSeekReply seekr(-1);
	clientRequestAndWaitReply(&seek,&seekr,sizeof(seekr));

#if __DEVPROF
	sm_DeviceProfiler->OnSeek(handle, offset);
#endif // __DEVPROF

	return seekr.result;
}

u64 fiDeviceRemote::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const {
	coreSeek64 seek((int)(size_t)handle,offset,whence);
	coreSeek64Reply seekr((u64)(s64)-1);
	clientRequestAndWaitReply(&seek,&seekr,sizeof(seekr));

#if __DEVPROF
	sm_DeviceProfiler->OnSeek(handle, offset);
#endif // __DEVPROF

	return remote_u64(seekr.newpos);
}

int fiDeviceRemote::Write(fiHandle handle,const void *buffer,int length) const {
	int amount = 0;
	sysCriticalSection clientSection(clientToken);

#if __DEVPROF
	sm_DeviceProfiler->OnWrite(handle, length);
#endif // __DEVPROF

	while (length) {
		int tocopy = length;
		if (tocopy > remoteWriteTransferSize)
			tocopy = remoteWriteTransferSize;
		coreWrite write((int)(size_t)handle,tocopy);
		coreWriteReply writer(-1);
		clientRequestAndWaitReplyEx(s_ClientSocket,&write,sizeof(write),buffer,tocopy,&writer,sizeof(writer),NULL,0);
		if (writer.result < 0)		// error
			return writer.result;
		if (writer.result) {
			amount += writer.result;
			length -= writer.result;
			buffer = (const void *)((char*)buffer + writer.result);
		}
		else		// disk full?
			break;
	}
	return amount;
}

#if __PPU && !__FINAL
extern void SimulateFatalError();
#endif

int fiDeviceRemote::GetReadChunkSize() const {
	static int cachedChunkSize;

	if (cachedChunkSize) {
		return cachedChunkSize;
	}

	int chunkSize = 0;

	if (PARAM_rfschunksize.Get(chunkSize) && chunkSize > 0) {
		if (Verifyf(chunkSize <= MAX_READ_CHUNK_SIZE, "RFS chunk size must be less than max size (%d)", MAX_READ_CHUNK_SIZE)) {
			cachedChunkSize = chunkSize;
			return chunkSize;
		}

		cachedChunkSize = remoteReadTransferSize;
	}

	return remoteReadTransferSize;
}

int fiDeviceRemote::Read(fiHandle handle,void *buffer,int length) const {
#if __XENON
	void *base = buffer;
#endif
#if __PPU && !__FINAL
	SimulateFatalError();
#endif

#if __DEVPROF
	sm_DeviceProfiler->OnRead(handle, length, 0);
#endif // __DEVPROF

	int transferSize = GetReadChunkSize();

	int amount = 0;
	sysCriticalSection clientSection(clientToken);
	while (length) {
		int tocopy = length;
		if (tocopy > transferSize)
			tocopy = transferSize;
		coreRead read((int)(size_t)handle,tocopy);
		coreReadReply readr(-1);

#if !__FINAL
		sysTimer readTimer;
#endif // !__FINAL

		clientRequestAndWaitReplyEx(s_ClientSocket,&read,sizeof(read),NULL,0,&readr,sizeof(readr),buffer,tocopy);
		if (readr.result < 0)		// error
			return readr.result;

#if !__FINAL
		// Only consider a) successful b) full-size reads.
		if (readr.result == transferSize) {
			s_AverageThroughput = s_AverageThroughput * 0.95f + ((float) readr.result) / readTimer.GetTime() * 0.05f;
		}
#endif // !__FINAL

		if (readr.result) {
			amount += readr.result;
			length -= readr.result;
			buffer = (void*)((char*)buffer + readr.result);
		}
		else		// end of file?
			break;
	}
#if __XENON
	WritebackDC(base,amount);
#endif

#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(handle);
#endif // __DEVPROF

	return amount;
}

int fiDeviceRemote::ReadBulk(fiHandle handle,u64 offset,void *buffer,int length) const {
#if __XENON
	void *base = buffer;
#endif
#if __PPU && !__FINAL
	SimulateFatalError();
#endif

#if __DEVPROF
	sm_DeviceProfiler->OnRead(handle, length, offset);
#endif // __DEVPROF

	int transferSize = GetReadChunkSize();

	int amount = 0;
	sysCriticalSection clientSection(clientToken);
	while (length) {
		int tocopy = length;
		if (tocopy > transferSize)
			tocopy = transferSize;
		coreReadBulk read((int)(size_t)handle,offset,tocopy);
		coreReadBulkReply readr(-1);

#if !__FINAL
		sysTimer readTimer;
#endif // !__FINAL

		clientRequestAndWaitReplyEx(s_ClientSocket,&read,sizeof(read),NULL,0,&readr,sizeof(readr),buffer,tocopy);
		if (readr.result < 0)		// error
			return readr.result;

#if !__FINAL
		// Only consider a) successful b) full-size reads.
		if (readr.result == transferSize) {
			s_AverageThroughput = s_AverageThroughput * 0.95f + ((float) readr.result) / readTimer.GetTime() * 0.05f;
		}
#endif // !__FINAL

		if (readr.result) {
			offset += readr.result;
			amount += readr.result;
			length -= readr.result;
			buffer = (void*)((char*)buffer + readr.result);
		}
		else		// end of file?
			break;
	}
#if __XENON
	WritebackDC(base,amount);
#endif

#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(handle);
#endif // __DEVPROF

	return amount;
}

int fiDeviceRemote::Close(fiHandle handle) const {
	coreClose close((int)(size_t)handle);
	coreCloseReply closer(-1);
	clientRequestAndWaitReply(&close,&closer,sizeof(closer));

#if __DEVPROF
	sm_DeviceProfiler->OnClose(handle);
#endif // __DEVPROF

	return closer.result;
}

int fiDeviceRemote::CloseBulk(fiHandle handle) const {
	coreCloseBulk close((int)(size_t)handle);
	coreCloseBulkReply closer(-1);
	clientRequestAndWaitReply(&close,&closer,sizeof(closer));

#if __DEVPROF
	sm_DeviceProfiler->OnClose(handle);
#endif // __DEVPROF

	return closer.result;
}

int fiDeviceRemote::Size(fiHandle handle) const
{
	return (int)Size64(handle);
}

u64 fiDeviceRemote::Size64(fiHandle handle) const
{
	int current = Seek(handle, 0, seekCur);
	int rv = Seek(handle, 0, seekEnd);
	Seek(handle, current, seekSet);
	return rv;
}

static void remoteTty(const char *message) {
	coreTty tty(message);
	coreTtyReply ttyr;
	clientRequestAndWaitReply(&tty,&ttyr,sizeof(ttyr));
}

bool fiDeviceRemote::Delete(const char *name) const {
	coreUnlink unlink(name);
	coreUnlinkReply unlinkr(-1);
	clientRequestAndWaitReply(&unlink,&unlinkr,sizeof(unlinkr));
	return unlinkr.result == 0;
}

fiRemoteDirEntry *fiDeviceRemote::GetCachedEntry(const char *name) const {
	USE_DEBUG_MEMORY();

	char path[RAGE_MAX_PATH];
	ASSET.RemoveNameFromPath(path, sizeof(path), name);

	// Let's use SysTrayRFS's batch-grabbing function.
	if (!m_FetchedDirectories.Access(atStringHash(path))) {
		sysTimer timer;

		// Batch-grab information about all files in this directory.
		u32 count = GetFileCount(path);

		atArray<u32> &dirContents = m_FetchedDirectories.Insert(atStringHash(path)).data;

		if (count) {
			fiRemoteDirEntry *entries = rage_new fiRemoteDirEntry[count];
			GetAttributesDir(path, entries, count);

			for (u32 x=0; x<count; x++) {
				char fullName[RAGE_MAX_PATH];
				safecpy(fullName, path);
				safecat(fullName, "/");
				safecat(fullName, entries[x].m_Filename);

				u32 nameHash = atStringHash(fullName);
				m_FileInfo.Insert(nameHash, entries[x]);
				dirContents.Grow(64) = nameHash;
			}

			delete[] entries;
		}
	}

	fiRemoteDirEntry *entry = m_FileInfo.Access(atStringHash(name));

	return entry;
}

void fiDeviceRemote::InvalidateDirectoryCache(const char *directory) const {

	char normalizedPath[RAGE_MAX_PATH];
	safecpy(normalizedPath, directory);

	// Remove the trailing slash, if present.
	int dirLen = ustrlen(directory);
	if (dirLen > 0 && (normalizedPath[dirLen-1] == '/' || normalizedPath[dirLen-1] == '\\')) {
		normalizedPath[dirLen-1] = 0;
	}

	u32 dirNameHash = atStringHash(normalizedPath);
	atArray<u32> *dirContents = m_FetchedDirectories.Access(dirNameHash);

	if (dirContents) {
		for (int x=dirContents->GetCount()-1; x>=0; x--) {
			m_FileInfo.Delete((*dirContents)[x]);
		}

		m_FetchedDirectories.Delete(dirNameHash);
	}

	if (s_ClientPort)
	{
		coreInvalidatePrefetchDir ga(normalizedPath);
		coreInvalidatePrefetchDirReply gar(~0U);
		clientRequestAndWaitReply(&ga,&gar,sizeof(gar));
	}
}


u32 fiDeviceRemote::GetAttributes(const char *name) const {
#if USE_CACHED_DIRECTORY_INFO
	if (!PARAM_disablerfscaching.Get())
	{
		fiRemoteDirEntry *entry = GetCachedEntry(name);

		if (entry) {
			return entry->m_Attribute;
		} else {
			return FILE_ATTRIBUTE_INVALID;
		}
	}
#endif // USE_CACHED_DIRECTORY_INFO
	coreGetAttr ga(name);
	coreGetAttrReply gar(~0U);
	clientRequestAndWaitReply(&ga,&gar,sizeof(gar));
	return gar.result;
}

u64 fiDeviceRemote::GetFileSize(const char *filename) const {
#if USE_CACHED_DIRECTORY_INFO
	if (!PARAM_disablerfscaching.Get())
	{
		fiRemoteDirEntry *entry = GetCachedEntry(filename);

		if (entry) {
			return entry->m_Filesize;
		} else {
			return 0;
		}
	}
#endif // USE_CACHED_DIRECTORY_INFO

	fiHandle h = Open(filename, true);
	if( h == fiHandleInvalid )
	{
		return 0;
	}
	u64 retval = Size64(h);
	Close(h);
	return retval;
}

int fiDeviceRemote::GetResourceInfo(const char *name,datResourceInfo &outHeader) const {
#if USE_CACHED_DIRECTORY_INFO
	if (!PARAM_disablerfscaching.Get())
	{
		fiRemoteDirEntry *entry = GetCachedEntry(name);

		if (entry) {
			if (entry->m_ResourceFileHeader.Magic == datResourceFileHeader::c_MAGIC) {
				outHeader = entry->m_ResourceFileHeader.Info;
				return (int) entry->m_ResourceFileHeader.Version;
			}
		}

		return 0;
	}
#endif // USE_CACHED_DIRECTORY_INFO
	return fiDevice::GetResourceInfo(name, outHeader);
}


u32 fiDeviceRemote::GetFileCount(const char *directory) const {
	coreGetFileCount ga(directory);
	coreGetFileCountReply gar(~0U);
	clientRequestAndWaitReply(&ga,&gar,sizeof(gar));
	return gar.result;
}

bool fiDeviceRemote::PrefetchDir(const char *directory) const {
	corePrefetchDir ga(directory);
	corePrefetchDirReply gar(~0U);
	clientRequestAndWaitReply(&ga,&gar,sizeof(gar));
	return gar.result != 0;
}

u32 fiDeviceRemote::GetAttributesDir(const char *directory, fiRemoteDirEntry *result, int resultCount) const {

	sysCriticalSection clientSection(clientToken);

	// We may have to split it up - we can't send too much in one pass.
	u32 skipCount = 0;
	u32 leftToDo = resultCount;

	while (leftToDo > 0)
	{
		resultCount = Min(leftToDo, GetRemoteMaxDirEntries());

		coreGetAttrDir ga(directory, skipCount, resultCount);
		coreGetAttrDirReply gar(resultCount);
		int bufferSize = resultCount * sizeof(fiRemoteDirEntry);
		fiRemoteDirEntry *buffer = rage_new fiRemoteDirEntry[resultCount];
		clientRequestAndWaitReplyEx(s_ClientSocket,&ga,remote_int(ga.length),NULL,0,&gar,sizeof(gar),buffer,bufferSize);

		for (int x=0; x<resultCount; x++) {
			fiRemoteDirEntry *target = &result[x + skipCount];
			Assertf(strlen(buffer[x].m_Filename) < sizeof(target->m_Filename), "Bad string passed in at %p from SysTrayRFS", &buffer[x]);
			strcpy(target->m_Filename, buffer[x].m_Filename);
			target->m_Attribute = remote_u32(buffer[x].m_Attribute);
			target->m_Filesize = remote_u64(buffer[x].m_Filesize);
			target->m_PhysSize = remote_u64(buffer[x].m_PhysSize);
			target->m_VirtSize = remote_u64(buffer[x].m_VirtSize);
			target->m_ResourceFileHeader = buffer[x].m_ResourceFileHeader;
		}

		delete[] buffer;

		skipCount += resultCount;
		leftToDo -= resultCount;
	}

	return 0;
}

bool fiDeviceRemote::SetAttributes(const char *name,u32 attr) const {
	coreSetAttr sa(name,attr);
	coreSetAttrReply sar(~0U);
	clientRequestAndWaitReply(&sa,&sar,sizeof(sar));
	return sar.result != 0;
}

bool fiDeviceRemote::MakeDirectory(const char *name) const {
	coreMkdir mkdir(name);
	coreMkdirReply mkdirr(-1);
	clientRequestAndWaitReply(&mkdir,&mkdirr,sizeof(mkdirr));
	return mkdirr.result == 0;
}

bool fiDeviceRemote::DeleteDirectory(const char *pathname, bool deleteContents) const {
	coreRmdir rmdir(pathname, deleteContents);
	coreRmdirReply rmdirr(-1);
	clientRequestAndWaitReply(&rmdir,&rmdirr,sizeof(rmdirr));
	return rmdirr.result == 0;
}

u64 fiDeviceRemote::GetFileTime(const char *filename) const {
	coreGetFileTime gft(filename);
	coreGetFileTimeReply gftr(0);
	clientRequestAndWaitReply(&gft,&gftr,sizeof(gftr));
	return remote_u64(gftr.filetime);
}

bool fiDeviceRemote::SetFileTime(const char *filename,u64 timestamp) const {
	coreSetFileTime sft(filename,timestamp);
	coreSetFileTimeReply sftr(0);
	clientRequestAndWaitReply(&sft,&sftr,sizeof(sftr));
	return (sftr.result != 0);
}

/* static bool remoteOutOfDate(const char *f1,const char *f2) {
	coreOOD ood(f1,f2);
	coreOODReply oodr(-1);
	clientRequestAndWaitReply(&ood,&oodr,sizeof(oodr));
	return oodr.result == 1;
} */

static void remote_find_data(fiFindData &out,const fiFindData &data) {
	sysMemCpy(out.m_Name,data.m_Name,sizeof(out.m_Name));
	out.m_Attributes = remote_u32(data.m_Attributes);
	out.m_LastWriteTime = remote_u64(data.m_LastWriteTime);
	out.m_Size = remote_u64(data.m_Size);
}

fiHandle fiDeviceRemote::FindFileBegin(const char *filename,fiFindData &outData) const {
	coreFindFirstFile fff(filename);
	coreFindFirstFileReply fffr(0);
	clientRequestAndWaitReply(&fff,&fffr,sizeof(fffr));
	remote_find_data(outData,fffr.data);
	return (fiHandle)(size_t)fffr.result;
}

bool fiDeviceRemote::FindFileNext(fiHandle handle,fiFindData &outData) const {
	coreFindNextFile fnf((int)(size_t)handle);
	coreFindNextFileReply fnfr(false);
	clientRequestAndWaitReply(&fnf,&fnfr,sizeof(fnfr));
	remote_find_data(outData,fnfr.data);
	return fnfr.result != 0;
}

int fiDeviceRemote::FindFileEnd(fiHandle handle) const {
	coreFindClose fc((int)(size_t)handle);
	coreFindCloseReply fcr(0);
	clientRequestAndWaitReply(&fc,&fcr,sizeof(fcr));
	return fcr.result;
}

#if __XENON
static void remoteCleanup() {
	if (s_ClientSocket != fiHandleInvalid) {
		remoteTty("Closing rfs socket...\n");
		coreExit exitPacket;
		clientRequestAndWaitReply(&exitPacket,NULL,0);
		fiDeviceTcpIp::GetInstance().Close(s_ClientSocket);
		s_ClientSocket = fiHandleInvalid;
	}
}
#endif

// We don't use this, but it suppresses the error about an unknown option and documents it.
PARAM(rfsport,"[startup] Specify non-default port for rfs usage.");

#if __PPU
XPARAM(localhost);
#endif

static fiDeviceRemote s_DeviceRemote;

bool fiRemoteServerIsRunning;

static ShouldAcceptRemoteConnectionFunc s_ShouldAcceptProcessConnectionDelegate;

bool fiRemoteInstall(int argc,char** argv) {
#if !__WIN32PC
	int port = 9001;
#else
	int port = 9000;
#endif

	const char *addr = fiDeviceTcpIp::GetLocalHost();

    for( int i = 0; i < argc; ++i )
    {
	    if (!strncmp(argv[i],"-rfsport=",9))
		    port = atoi(argv[i] + 9);
		else if (!strcmp(argv[i],"-rfsport") && i+1<argc)
			port = atoi(argv[++i]);
		else if (!strncmp(argv[i],"-localhost=",11))
			addr = argv[i] + 11;
		else if (!strcmp(argv[i],"-localhost") && i+1<argc)
			addr = argv[++i];
    }

	if (!clientInit(addr,port)) {
#if __XENON
		OutputDebugString("***** coreRemoteInstall: Bad connect address:");
		OutputDebugString(addr);
		OutputDebugString(", or server not running!\n");
#elif __PPU || __PSP2
		Displayf("***** coreRemoteInstall: Bad connect address: %s, or server not running!",addr);
#endif
		return false;
	}

	clientRequestReset();
	remoteTty("coreRemoteInstall: Hello, world!\n");

#if __XENON
	atexit(remoteCleanup);
#endif
	return true;
}

void fiDeviceRemote::InitClass(int argc,char **argv) {
	if (fiRemoteInstall(argc,argv)) {
			// Mount the D: drive explicitly for local access,
			// and default fallback device is remote for everything else
			fiDevice::Unmount("default");
			fiDevice::Mount("default",s_DeviceRemote,false);
			fiRemoteServerIsRunning = true;
	}
}

const fiDevice& fiDeviceRemote::GetInstance() {
	return s_DeviceRemote;
}

void fiDeviceRemote::ShutdownClass() {
	if (fiRemoteServerIsRunning) {
		fiDevice::Unmount("default");
	}
#if __XENON
	// It's okay to call this multiple times
	remoteCleanup();
#endif
}

///////////////////////////////////////////////////////////////////////////////
// Server
///////////////////////////////////////////////////////////////////////////////

#if defined(IS_SERVER)

int g_RfsDebug = 0;
#define RFS(x)	x

struct RfsServerData
{
    RfsServerData( fiHandle hSocket )
        : m_TrackedHandleCount( 0 )
        , m_Socket( hSocket )
        , m_Quit( false )
    {
        ::memset( m_TrackedHandles, 0, sizeof( m_TrackedHandles ) );
        ::memset( m_TrackedNames, 0, sizeof( m_TrackedNames ) );
        RfsServerData::AddServer( this );
    }

    ~RfsServerData()
    {
        RfsServerData::RemoveServer( this );
    }

    int m_TrackedHandleCount;
    static const int MaxTrackedHandles = 4096;
    fiHandle m_TrackedHandles[MaxTrackedHandles];
    char *m_TrackedNames[MaxTrackedHandles];
    fiHandle m_Socket;
    bool m_Quit;

    typedef std::set< RfsServerData* > Servers;

    static void AddServer( RfsServerData* rfsd )
    {
        sm_Cs.Enter();
        sm_Servers.insert( rfsd );
        sm_Cs.Exit();
    }

    static void RemoveServer( RfsServerData* rfsd )
    {
        sm_Cs.Enter();
        sm_Servers.erase( rfsd );
        sm_Cs.Exit();
    }

    static void KillServers()
    {
        sm_Cs.Enter();

        Servers::iterator it = sm_Servers.begin();
        Servers::const_iterator stop = sm_Servers.end();

        for( ; it != stop; ++it )
        {
            RfsServerData* rfsd = *it;
            fiDeviceTcpIp::GetInstance().Close(rfsd->m_Socket);
        }

        sm_Cs.Exit();
    }

	static void DumpServers();
    static sysCriticalSection sm_Cs;
    static Servers sm_Servers;
};

RfsServerData::Servers RfsServerData::sm_Servers;
static sysCriticalSectionToken sm_Token;
sysCriticalSection RfsServerData::sm_Cs(sm_Token);

static void serverReply(fiHandle hSocket,remoteAny *sendPkt) {
	// Send outgoing packet
	// printf("serverReply - %d %d %d %d\n",sendPkt->protocol,sendPkt->command,sendPkt->result,sendPkt->length);
	__send(hSocket,sendPkt,remote_int(sendPkt->length));
}

void serverDumpActiveFiles(RfsServerData& rfsd) {
	fiStream *S = fiStream::Create("c:\\sysTrayRfs.txt");
	if (S) {
		for (int i=0; i<rfsd.m_TrackedHandleCount; i++)
			fprintf(S,"%d. %x '%s'\r\n",i, rfsd.m_TrackedHandles[i],rfsd.m_TrackedNames[i]);
		S->Close();
	}
}

void RfsServerData::DumpServers()
{
	sm_Cs.Enter();

	Servers::iterator it = sm_Servers.begin();
	Servers::const_iterator stop = sm_Servers.end();

	fiStream *S = fiStream::Create("c:\\sysTrayRfs.txt");

	for( ; it != stop; ++it )
	{
		RfsServerData* rfsd = *it;
		if (S) {
			fprintf(S,"Server %p:\r\n",rfsd);
			for (int i=0; i<rfsd->m_TrackedHandleCount; i++)
				fprintf(S,"%d. %x '%s'\r\n",i, rfsd->m_TrackedHandles[i],rfsd->m_TrackedNames[i]);
		}
	}

	if (S)
		S->Close();

	sm_Cs.Exit();
}

void serverDumpAllServerFiles()
{
	RfsServerData::DumpServers();
}

static void serverAddHandle( RfsServerData& rfsd, fiHandle handle,const char *filename) {
	for (int i=0; i<rfsd.m_TrackedHandleCount; i++) {
		if (rfsd.m_TrackedHandles[i] == handle) {
			Errorf("remoteAddHandle - handle already present: %d",handle);
			return;
		}
	}
	if (rfsd.m_TrackedHandleCount >= RfsServerData::MaxTrackedHandles) {
		MessageBox(NULL,"Too many files open (4096), maybe something is leaking handles?  See c:\\sysTrayRfs.txt for list of active files.","sysTrayRfs",MB_OK);
		serverDumpActiveFiles(rfsd);
		return;
	}

	rfsd.m_TrackedHandles[rfsd.m_TrackedHandleCount] = handle;
	rfsd.m_TrackedNames[rfsd.m_TrackedHandleCount] = StringDuplicate(filename);
	++rfsd.m_TrackedHandleCount;
}

static void serverRemoveHandle(RfsServerData& rfsd, fiHandle handle) {
	for (int i=0; i<rfsd.m_TrackedHandleCount; i++) {
		if (rfsd.m_TrackedHandles[i] == handle) {
			StringFree(rfsd.m_TrackedNames[i]);
			rfsd.m_TrackedNames[i] = rfsd.m_TrackedNames[rfsd.m_TrackedHandleCount-1];
			rfsd.m_TrackedHandles[i] = rfsd.m_TrackedHandles[rfsd.m_TrackedHandleCount-1];
			--rfsd.m_TrackedHandleCount;
			return;
		}
	}
	Errorf("remoteRemoveHandle - handle not in list: %d",handle);
}

static void serverResetFiles(RfsServerData& rfsd) {
	if (rfsd.m_TrackedHandleCount) {
		Warningf("Closing down %d orphaned files...",rfsd.m_TrackedHandleCount);
		do {
			DEVICE.Close(rfsd.m_TrackedHandles[0]);
			serverRemoveHandle(rfsd, rfsd.m_TrackedHandles[0]);
		} while (rfsd.m_TrackedHandleCount);
	}
}

void fiRemoteResetFiles() {
    RfsServerData::KillServers();
}

bool RestrictedAccess = true;

static void AccessFailure(const char *msg,const char *file) {
	char buffer[1024];
	static const char cmt[] = "Do you wish to allow general access from now on?";
	formatf(buffer,sizeof(buffer),"Security Alert!\nFile:    %s\nMessage: %s\n\n%s",file,msg,cmt);
	if (MessageBox(NULL,buffer,"SysTrayRfs Security Alert",MB_TOPMOST | MB_ICONERROR | MB_YESNO) == IDYES)
		RestrictedAccess = false;
	else
		Quitf("Security Alert!\nFile:    %s\nMessage: %s",file,msg);
}

const char *CheckName(const char *name) {
	if (RestrictedAccess && ((name[0] == 'c' || name[0] == 'C') && name[1]==':')) {
		AccessFailure("SysTrayRfs is not allowed to access C: drive for security reasons.",name);
	}
	else if (RestrictedAccess && name[1]!=':') {
		AccessFailure("SysTrayRfs is not allowed to access any file without a drive letter for security reasons.",name);
	}
	return name;
}

#ifdef IS_SERVER

static bool IsDot(const char *name)
{
	if (name[0] == '.')
	{
		if (name[1] == 0)
		{
			return true;
		}

		if (name[1] == '.' && name[2] == 0)
		{
			return true;
		}
	}

	return false;
}

static u32 GetFileCount(const char *directory)
{
	fiFindData data;
	u32 count = 0;
	fiHandle handle = DEVICE.FindFileBegin(CheckName(directory),data);
	while (DEVICE.FindFileNext(handle,data))
	{
		if (IsDot(data.m_Name))
			continue;

		count++;
	}
	DEVICE.FindFileEnd(handle);

	return count;
}

static u32 ReadDirAttributes(const char *directory, fiRemoteDirEntry *entries, u32 skipCount, u32 maxCount)
{
	fiFindData data;
	u32 count = 0;
	fiHandle handle = DEVICE.FindFileBegin(CheckName(directory),data);
	char fullName[RAGE_MAX_PATH];
	safecpy(fullName, directory);
	size_t fullNameLen = strlen(fullName);
	char *namePart = fullName + fullNameLen;
	*(namePart++) = '\\';

	while (DEVICE.FindFileNext(handle,data))
	{
		if (count >= maxCount)
			break;

		if (IsDot(data.m_Name))
			continue;

		if (skipCount > 0) {
			skipCount--;
			continue;
		}

		safecpy(namePart, data.m_Name, RAGE_MAX_PATH - 1 - fullNameLen);

		fiRemoteDirEntry *entry = &entries[count++];

		safecpy(entry->m_Filename, data.m_Name);
		entry->m_Attribute = DEVICE.GetAttributes(CheckName(fullName));
		entry->m_Filesize = DEVICE.GetFileSize(CheckName(fullName));

		u64 bias;
		fiHandle h = DEVICE.OpenBulk(CheckName(fullName), bias);
		if (fiIsValidHandle(h)) {
			int result = DEVICE.ReadBulk(h, bias, &entry->m_ResourceFileHeader, sizeof(entry->m_ResourceFileHeader));
			if (result != sizeof(datResourceFileHeader)) {
				// Invalidate.
				entry->m_ResourceFileHeader.Magic = 0;
			}

			DEVICE.CloseBulk(h);
		} else {
			entry->m_ResourceFileHeader.Magic = 0;
		}
	}

	DEVICE.FindFileEnd(handle);

	return count;
}

static void ResetPrefetchCache()
{
	SYS_CS_SYNC(s_FetchedDirectoriesCs);
	s_FetchedDirectories.Reset();
}

static void PrefetchDirWorker(void * /*data*/)
{
	while (true)
	{
		// Block thread until we get a new request.
		remotePrefetchRequest request = s_PrefetchRequests.Pop();

		// An empty string indicates that we want to quit.
		if (request.m_Directory.length() == 0)
		{
			return;
		}


		const char *dir = request.m_Directory.c_str();
		u32 dirHash = atStringHash(dir);

		// Do we already have it?
		{
			SYS_CS_SYNC(s_FetchedDirectoriesCs);
			if (s_FetchedDirectories.Access(dirHash))
			{
				continue;
			}
		}

		// Prefetch this directory.
		u32 fileCount = GetFileCount(dir);

		atArray<fiRemoteDirEntry> data;
		data.Reserve(fileCount);

		if (fileCount)
		{
			data.Resize(ReadDirAttributes(dir, data.GetElements(), 0, fileCount));
		}

		// Officially add the data to our list.
		SYS_CS_SYNC(s_FetchedDirectoriesCs);
		s_FetchedDirectories.Insert(dirHash, data);
	}
}

#endif // IS_SERVER

char* s_commands[] = 
{
	"CORE_CMD_RESET",
	"CORE_CMD_RESETR",
	"CORE_CMD_OPEN",
	"CORE_CMD_OPENR",
	"CORE_CMD_SEEK",
	"CORE_CMD_SEEKR",
	"CORE_CMD_WRITE",
	"CORE_CMD_WRITER",
	"CORE_CMD_READ",
	"CORE_CMD_READR",
	"CORE_CMD_CLOSE",
	"CORE_CMD_CLOSER",
	"CORE_CMD_TTY",
	"CORE_CMD_TTYR",
	"CORE_CMD_EXIT",
	"CORE_CMD_OUTOFDATE",
	"CORE_CMD_OUTOFDATER",
	"CORE_CMD_UNLINK",
	"CORE_CMD_UNLINKR",
	"CORE_CMD_MESSAGEBOX",
	"CORE_CMD_MESSAGEBOXR",
	"CORE_CMD_GETFILETIME",
	"CORE_CMD_GETFILETIMER",
	"CORE_CMD_SETFILETIME",
	"CORE_CMD_SETFILETIMER",
	"CORE_CMD_FINDFIRSTFILE",
	"CORE_CMD_FINDFIRSTFILER",
	"CORE_CMD_FINDNEXTFILE",
	"CORE_CMD_FINDNEXTFILER",
	"CORE_CMD_FINDCLOSE",
	"CORE_CMD_FINDCLOSER",
	"CORE_CMD_MKDIR",
	"CORE_CMD_MKDIRR",
	"CORE_CMD_RMDIR",
	"CORE_CMD_RMDIRR",
	"CORE_CMD_GETATTRIBUTE",
	"CORE_CMD_GETATTRIBUTER",
	"CORE_CMD_SETATTRIBUTE",
	"CORE_CMD_SETATTRIBUTER",
	"CORE_CMD_EXEC",
	"CORE_CMD_EXECR",
	"CORE_CMD_PING",
	"CORE_CMD_OPENBULK",
	"CORE_CMD_OPENBULKR",
	"CORE_CMD_READBULK",
	"CORE_CMD_READBULKR",
	"CORE_CMD_CLOSEBULK",
	"CORE_CMD_CLOSEBULKR",
	"CORE_CMD_GETENV",
	"CORE_CMD_GETENVR",
	"CORE_CMD_SEEK64",
	"CORE_CMD_SEEK64R",
	"CORE_CMD_ASSERTMESSAGEBOX",
	"CORE_CMD_GETFILECOUNT",
	"CORE_CMD_GETFILECOUNTR",
	"CORE_CMD_GETATTRIBUTEDIR",
	"CORE_CMD_GETATTRIBUTEDIRR",
	"CORE_CMD_PREFETCHDIR",
	"CORE_CMD_PREFETCHDIRR",
	"CORE_CMD_INVALIDATE_PREFETCHDIR",
	"CORE_CMD_INVALIDATE_PREFETCHDIRR",
	"CORE_CMD_RESET_CACHE",
	"CORE_CMD_RESET_CACHER",

};

static void serverHandleRequest(RfsServerData& rfsd, remoteAny *any) {
	if (g_RfsDebug>2) Displayf("Received command %d", s_commands[any->command]);
	switch (any->command) {
	case CORE_CMD_RESET:
		{
			coreResetReply resetr;
			// NO-OP for now
			if (g_RfsDebug) Displayf(RFS("Reset"));
			serverResetFiles(rfsd);
			serverReply(rfsd.m_Socket,&resetr);
			break;
		}
	case CORE_CMD_OPEN: 
		{
			coreOpen *open = (coreOpen*) any;
			coreOpenReply openr((int)(size_t)(open->create? DEVICE.Create(CheckName(open->filename)) : DEVICE.Open(CheckName(open->filename),remote_int(open->readonly) != 0)));
			if (remote_int(openr.result) >= 0)
				serverAddHandle(rfsd, (fiHandle)remote_int(openr.result),open->filename);
			if (g_RfsDebug>1) Displayf(RFS("%s(%s) -> %d"),open->create?"Create":remote_int(open->readonly)?"Open[RO]":"Open[R/W]",open->filename,remote_int(openr.result));
			serverReply(rfsd.m_Socket,&openr);
			break;
		}
	case CORE_CMD_OPENBULK: 
		{
			coreOpenBulk *open = (coreOpenBulk*) any;
			u64 dummyBias;
			coreOpenBulkReply openr((int)(size_t)(DEVICE.OpenBulk(open->filename,dummyBias)));
			// Assert(dummyBias==0);
			if (remote_int(openr.result) >= 0)
				serverAddHandle(rfsd, (fiHandle)remote_int(openr.result),open->filename);
			if (g_RfsDebug>1) Displayf(RFS("OpenBulk(%s) -> %d"),open->filename,remote_int(openr.result));
			serverReply(rfsd.m_Socket,&openr);
			break;
		}
	case CORE_CMD_SEEK:
		{
			coreSeek *seek = (coreSeek*) any;
			coreSeekReply seekr(DEVICE.Seek((fiHandle)remote_int(seek->handle),remote_int(seek->offset),(fiSeekWhence)remote_int(seek->whence)));
			if (g_RfsDebug>2) Displayf(RFS("Seek(%d,%d,%d) -> %d"),remote_int(seek->handle),remote_int(seek->offset),remote_int(seek->whence),seekr.result);
			serverReply(rfsd.m_Socket,&seekr);
			break;
		}
	case CORE_CMD_SEEK64:
		{
			coreSeek64 *seek = (coreSeek64*) any;
			coreSeek64Reply seekr(DEVICE.Seek64((fiHandle)remote_int(seek->handle),remote_s64(seek->offset),(fiSeekWhence)remote_int(seek->whence)));
			if (g_RfsDebug>2) Displayf(RFS("Seek64(%d,%d,%d) -> %d"),remote_int(seek->handle),(int)remote_s64(seek->offset),remote_int(seek->whence),seekr.result);
			serverReply(rfsd.m_Socket,&seekr);
			break;
		}
	case CORE_CMD_WRITE:
		{
			coreWrite *write = (coreWrite*) any;
			coreWriteReply writer(DEVICE.Write((fiHandle)remote_int(write->handle),write+1,remote_int(write->amount)));
			if (g_RfsDebug>2) Displayf(RFS("Write(%d,%d) -> %d"),remote_int(write->handle),remote_int(write->amount),remote_int(writer.result));
			serverReply(rfsd.m_Socket,&writer);
			break;
		}
	case CORE_CMD_READ:
		{
			coreRead *read = (coreRead*) any;
			struct coreReadReplyTemp: public coreReadReply {
				coreReadReplyTemp() : coreReadReply(0) { }
				char bounce[remoteReadTransferSize];
			} readr;
			if (read->amount > remoteReadTransferSize)
				Quitf("Packet read too large in CORE_CMD_READ");
#if __WIN32PC
			DWORD amt = 0;
			BOOL result=PeekNamedPipe((HANDLE) read->handle, readr.bounce, read->amount, (LPDWORD)&amt, NULL, NULL);
			int lastError = 0;
			if (!result)
				lastError=GetLastError();
			// if the handle is a file, ERROR_INVALID_FUNCTION is returned, so we'll read if the peek fails and we get that error:
			if ((result && amt) || (!result && lastError==ERROR_INVALID_FUNCTION))
#endif
			{
				int amtRead = DEVICE.Read((fiHandle)remote_int(read->handle),readr.bounce,remote_int(read->amount));
				readr.result = remote_int(amtRead);
				readr.length = remote_int(sizeof(coreReadReply) + (amtRead>0?amtRead:0));
				if (g_RfsDebug>2) Displayf(RFS("Read(%d,%d) -> %d"),remote_int(read->handle),remote_int(read->amount),remote_int(readr.result));
				serverReply(rfsd.m_Socket,&readr);
			}
#if __WIN32PC
			// send a zero size message to signify we have nothing to read - prevents the client from blocking:
			else 
			{
				coreReadReply readr(0);
				serverReply(rfsd.m_Socket,&readr);
			}
#endif
			
			break;
		}
	case CORE_CMD_READBULK:
		{
			coreReadBulk *read = (coreReadBulk*) any;
			struct coreReadBulkReplyTemp: public coreReadBulkReply {
				coreReadBulkReplyTemp() : coreReadBulkReply(0) { }
				char bounce[remoteReadTransferSize];
			} readr;
			if (read->amount > remoteReadTransferSize)
				Quitf("Packet read too large in CORE_CMD_READ");
			int amtRead = DEVICE.ReadBulk((fiHandle)remote_int(read->handle),remote_u64(read->offset),readr.bounce,remote_int(read->amount));
			readr.result = remote_int(amtRead);
			readr.length = remote_int(sizeof(coreReadReply) + (amtRead>0?amtRead:0));
			if (g_RfsDebug>2) Displayf(RFS("ReadBulk(%d,%llu,%d) -> %d"),remote_int(read->handle),remote_u64(read->offset),remote_int(read->amount),remote_int(readr.result));
			serverReply(rfsd.m_Socket,&readr);
			break;
		}
	case CORE_CMD_CLOSE:
		{
			coreClose *close = (coreClose*) any;
			coreCloseReply closer(DEVICE.Close((fiHandle)remote_int(close->handle)));
			if (remote_int(closer.result) >= 0)
				serverRemoveHandle(rfsd, (fiHandle)remote_int(close->handle));
			if (g_RfsDebug>1) Displayf(RFS("Close(%d) -> %d"),remote_int(close->handle),remote_int(closer.result));
			serverReply(rfsd.m_Socket,&closer);
			break;
		}
	case CORE_CMD_CLOSEBULK:
		{
			coreCloseBulk *close = (coreCloseBulk*) any;
			coreCloseBulkReply closer(DEVICE.CloseBulk((fiHandle)remote_int(close->handle)));
			if (remote_int(closer.result) >= 0)
				serverRemoveHandle(rfsd, (fiHandle)remote_int(close->handle));
			if (g_RfsDebug>1) Displayf(RFS("CloseBulk(%d) -> %d"),remote_int(close->handle),remote_int(closer.result));
			serverReply(rfsd.m_Socket,&closer);
			break;
		}
	case CORE_CMD_TTY:
		{
			coreTty *tty = (coreTty*) any;
			// diagPrintCallback(tty->message);
			// OutputDebugString(tty->message);
#if __WIN32PC
			if (strncmp(tty->message,"@EXEC:",6) == 0)
			{
				STARTUPINFO si;
				PROCESS_INFORMATION pi;

				ZeroMemory( &si, sizeof(si) );
				si.cb = sizeof(si);
				ZeroMemory( &pi, sizeof(pi) );


				// Start the child process. 
				if(CreateProcess(NULL, tty->message+6, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi)) 
				{
					WaitForSingleObject( pi.hProcess, INFINITE );
					CloseHandle( pi.hProcess );
					CloseHandle( pi.hThread );
				}
				else
				{
					printf("Couldn't create process  (Err: %d).\n", GetLastError());
				}
			}
#endif
			coreTtyReply ttyr;
			if (g_RfsDebug) 
			{
				Displayf(RFS("CORE_CMD_TTY: ")"%s", tty->message);
			}

			serverReply(rfsd.m_Socket,&ttyr);
			break;
		}
	case CORE_CMD_EXIT:
		{
			printf("[Received exit request]\n");
			if (g_RfsDebug) Displayf(RFS("CORE_CMD_EXIT"));
			rfsd.m_Quit = true;
			break;
		}
	case CORE_CMD_UNLINK:
		{
			coreUnlink *unlink = (coreUnlink*) any;
			coreUnlinkReply unlinkr(DEVICE.Delete(CheckName(unlink->filename)));
			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_UNLINK"));
			serverReply(rfsd.m_Socket,&unlinkr);
			break;
		}
	case CORE_CMD_GETATTRIBUTE:
		{
			coreGetAttr *ga = (coreGetAttr*) any;
			coreGetAttrReply gar(DEVICE.GetAttributes(CheckName(ga->filename)));
			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_GETATTRIBUTE"));
			serverReply(rfsd.m_Socket,&gar);
			break;
		}
	case CORE_CMD_GETFILECOUNT:
		{
			coreGetFileCount *ga = (coreGetFileCount*) any;

			u32 count = GetFileCount(ga->directory);

			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_GETFILECOUNT"));

			coreGetFileCountReply gar(count);
			serverReply(rfsd.m_Socket,&gar);
			break;
		}
	case CORE_CMD_PREFETCHDIR:
		{
			corePrefetchDir *ga = (corePrefetchDir*) any;

			s_PrefetchRequests.Push(remotePrefetchRequest(CheckName(ga->directory)));

			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_PREFETCHDIR"));

			corePrefetchDirReply gar(1);
			serverReply(rfsd.m_Socket,&gar);
			break;
		}
	case CORE_CMD_INVALIDATE_PREFETCHDIR:
		{
			corePrefetchDir *ga = (corePrefetchDir*) any;

			{
				SYS_CS_SYNC(s_FetchedDirectoriesCs);

				s_FetchedDirectories.Delete(atStringHash(CheckName(ga->directory)));
			}

			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_INVALIDATE_PREFETCHDIR"));

			corePrefetchDirReply gar(1);
			serverReply(rfsd.m_Socket,&gar);
			break;
		}
	case CORE_CMD_RESET_CACHE:
		{
			ResetPrefetchCache();

			coreResetCacheReply gar(1);
			serverReply(rfsd.m_Socket,&gar);
			break;

		}
	case CORE_CMD_GETATTRIBUTEDIR:
		{
			coreGetAttrDir *ga = (coreGetAttrDir*) any;
			u32 skipCount = remote_u32(ga->skipCount);
			u32 maxCount = remote_u32(ga->resultCount);

			struct coreGetAttrDirReplyTemp: public coreGetAttrDirReply {
				coreGetAttrDirReplyTemp() : coreGetAttrDirReply(0) { }
				fiRemoteDirEntry entries[remoteMaxDirEntries];
			} attrdirr;

			if (maxCount > remoteMaxDirEntries)
			{
				Errorf("Requesting too many entries - %d vs %d", maxCount, remoteMaxDirEntries);
				maxCount = remoteMaxDirEntries;
			}

			u32 dirHash = atStringHash(CheckName(ga->directory));
			s_FetchedDirectoriesCs.Lock();

			atArray<fiRemoteDirEntry> *dirEntries = s_FetchedDirectories.Access(dirHash);

			if (dirEntries)
			{
				u32 count = 0;
				u32 dirSize = dirEntries->GetCount();

				if (dirSize > skipCount)
				{
					count = Min(dirSize - skipCount, remoteMaxDirEntries);
					sysMemCpy(attrdirr.entries, &(*dirEntries)[skipCount], sizeof(fiRemoteDirEntry) * count);
				}

				attrdirr.length = remote_int(sizeof(coreGetAttrDirReply) + sizeof(fiRemoteDirEntry) * count);
				s_FetchedDirectoriesCs.Unlock();
				serverReply(rfsd.m_Socket,&attrdirr);
				return;
			}

			s_FetchedDirectoriesCs.Unlock();

			u32 count = ReadDirAttributes(ga->directory, attrdirr.entries, skipCount, maxCount);

			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_GETATTRIBUTEDIR"));

			attrdirr.length = remote_int(sizeof(coreGetAttrDirReply) + sizeof(fiRemoteDirEntry) * count);
			serverReply(rfsd.m_Socket,&attrdirr);
			break;
		}
	case CORE_CMD_SETATTRIBUTE:
		{
			coreSetAttr *sa = (coreSetAttr*) any;
			coreSetAttrReply sar(DEVICE.SetAttributes(CheckName(sa->filename),sa->result));
			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_SETATTRIBUTE"));
			serverReply(rfsd.m_Socket,&sar);
			break;
		}
	case CORE_CMD_OUTOFDATE:
		{
			coreOOD *ood = (coreOOD*) any;
			coreOODReply oodr(fiDevice::IsOutOfDate(CheckName(ood->filename1),CheckName(ood->filename2)));
			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_OUTOFDATE"));
			serverReply(rfsd.m_Socket,&oodr);
			break;
		}
	case CORE_CMD_MESSAGEBOX:
		{
#if __WIN32PC
			coreMessageBox *mb = (coreMessageBox*) any;
			coreMessageBoxReply mbr(MessageBox(NULL,mb->message,mb->title,remote_u32(mb->flags)));
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_MESSAGEBOX"));
			serverReply(rfsd.m_Socket,&mbr);
			break;
#endif
		}
	case CORE_CMD_ASSERTMESSAGEBOX:
		{
#if __WIN32PC
			coreAssertMessageBox *mb = (coreAssertMessageBox*) any;
#if SYSTRAY
			coreMessageBoxReply mbr(AssertMessageBox(NULL,mb->message,mb->title,remote_u32(mb->flags)));
#else
			coreMessageBoxReply mbr(MessageBox(NULL,mb->message,mb->title,remote_u32(mb->flags)));
#endif
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_ASSERTMESSAGEBOX"));
			serverReply(rfsd.m_Socket,&mbr);
#endif
			break;
		}
	case CORE_CMD_GETFILETIME:
		{
			coreGetFileTime *gft = (coreGetFileTime*) any;
			coreGetFileTimeReply gftr(DEVICE.GetFileTime(CheckName(gft->filename)));
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_GETFILETIME"));
			serverReply(rfsd.m_Socket,&gftr);
			break;
		}
	case CORE_CMD_SETFILETIME:
		{
			coreSetFileTime *sft = (coreSetFileTime*) any;
			coreSetFileTimeReply sftr(DEVICE.SetFileTime(CheckName(sft->filename),remote_u64(sft->filetime)));
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_SETFILETIME"));
			serverReply(rfsd.m_Socket,&sftr);
			break;
		}
	case CORE_CMD_FINDFIRSTFILE:
		{
			coreFindFirstFile *fff = (coreFindFirstFile*) any;
			fiFindData data;
			coreFindFirstFileReply fffr((int)(size_t)DEVICE.FindFileBegin(CheckName(fff->filename),data));
			fffr.data = data;
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_FINDFIRSTFILE"));
			serverReply(rfsd.m_Socket,&fffr);
			break;
		}
	case CORE_CMD_FINDNEXTFILE:
		{
			coreFindNextFile *fnf = (coreFindNextFile*) any;
			fiFindData data;
			coreFindNextFileReply fnfr(DEVICE.FindFileNext((fiHandle)remote_u32(fnf->handle),data));
			fnfr.data = data;
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_FINDNEXTFILE"));
			serverReply(rfsd.m_Socket,&fnfr);
			break;
		}
	case CORE_CMD_FINDCLOSE:
		{
			coreFindClose *fc = (coreFindClose*) any;
			coreFindCloseReply fcr(DEVICE.FindFileEnd((fiHandle)remote_u32(fc->handle)));
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_FINDCLOSE"));
			serverReply(rfsd.m_Socket,&fcr);
			break;
		}
	case CORE_CMD_MKDIR:
		{
			coreMkdir *mkdir = (coreMkdir*) any;
			coreMkdirReply mkdirr(DEVICE.MakeDirectory(CheckName(mkdir->filename)));
			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_MKDIR"));
			serverReply(rfsd.m_Socket,&mkdirr);
			break;
		}
	case CORE_CMD_RMDIR:
		{
			coreRmdir *rmdir = (coreRmdir*) any;
			coreRmdirReply rmdirr(DEVICE.DeleteDirectory(CheckName(rmdir->filename), rmdir->removeContents != 0));
			if (g_RfsDebug>1) Displayf(RFS("CORE_CMD_RMDIR"));
			serverReply(rfsd.m_Socket,&rmdirr);
			break;
		}
	case CORE_CMD_EXEC:
		{
			coreExec *exec = (coreExec*) any;
			if (RestrictedAccess)
				AccessFailure("EXEC command not allowed",exec->buffer);
			coreExecReply execr(sysExec(exec->buffer));
			if (g_RfsDebug) Displayf(RFS("CORE_CMD_EXEC"));
			serverReply(rfsd.m_Socket,&execr);
			break;
		}
	case CORE_CMD_GETENV:
		{
			coreGetEnv *env = (coreGetEnv*) any;
			coreGetEnvReply envr(getenv(env->buffer));
			if (g_RfsDebug>2) Displayf(RFS("CORE_CMD_GETENV"));
			serverReply(rfsd.m_Socket,&envr);
			break;
		}
	default:
		Quitf("remoteRequest -- unknown command %d",any->command);
	}

	if (g_RfsDebug>1) Displayf("Finished command %d", s_commands[any->command]);
}

static void ServerWorker( void* data )
{
    RfsServerData* rfsd = rage_new RfsServerData( ( fiHandle ) data );

    // Displayf("remoteServer: Installed and waiting.");
	char buffer[remoteReadTransferSize + RAGE_MAX_PATH];
	char connectionInfo[RAGE_MAX_PATH] = {0};

	remoteAny *any = (remoteAny*) buffer;

	fiDeviceTcpIp::GetRemoteName(rfsd->m_Socket, connectionInfo, sizeof(connectionInfo));

	Displayf("*** remoteServer: Pick up new connection with IP: %s and file handle: %p", connectionInfo, data);

	bool acceptConnection = true;
	if (s_ShouldAcceptProcessConnectionDelegate.IsBound())
	{
		acceptConnection = s_ShouldAcceptProcessConnectionDelegate.Invoke(connectionInfo);
	}

	if (acceptConnection)
	{
		while(__recv(rfsd->m_Socket,buffer,sizeof(remoteAny)))
		{
			if(any->length < (int)sizeof(remoteAny)
			   || any->length > int(sizeof(buffer) - sizeof(remoteAny)))
			{
				Errorf("Invalid packet length:%d", any->length);
			}
			else if(any->protocol != PROTO_CORE && any->protocol != 5 && any->protocol != 4 && any->protocol != 2)	// Backward compatible to 2 and 4
			{
				Quitf("SysTrayRfs: Received unknown protocol %d, was expecting %d.\n",any->protocol, PROTO_CORE);
			}
			else if( CORE_CMD_PING != any->command )
			{
    			// Read rest of payload
				if (__recv(rfsd->m_Socket,(any + 1),any->length - sizeof(remoteAny)))
				{
    				// and dispatch it
	    			serverHandleRequest(*rfsd, any);
					if (rfsd->m_Quit) {
						rfsd->m_Quit = false;
						break;
					}
				}
			}
		}

		Displayf("*** remoteServer: Lost connection %p",data);
	}
	else
	{
		Displayf("*** remoteServer: Ignoring connection with IP: %s and file handle: %p", connectionInfo, data);
	}

#if __WIN32
	fiDeviceTcpIp::GetInstance().Close(rfsd->m_Socket);
	rfsd->m_Socket = fiHandleInvalid;
#endif

    serverResetFiles( *rfsd );

    delete rfsd;
}

void fiRemoteClearAcceptConnectionDelegate()
{
	s_ShouldAcceptProcessConnectionDelegate.Clear();
}

void fiRemoteSetAcceptConnectionDelegate(const ShouldAcceptRemoteConnectionFunc& fn)
{
	s_ShouldAcceptProcessConnectionDelegate = fn;
}

void fiRemoteServer(int port,const char *ip) {
	
#if __WIN32
	sysIpcCreateThread( &PrefetchDirWorker, NULL, sysIpcMinThreadStackSize, PRIO_NORMAL, "RFSServer Prefetcher" );

	fiHandle listener = fiDeviceTcpIp::Listen(port,1,ip);
#endif
	while (1) {
#if __WIN32
		fiHandle hSocket = fiDeviceTcpIp::Pickup(listener);
		if (hSocket != fiHandleInvalid) {
			ResetPrefetchCache();
			sysIpcCreateThread( &ServerWorker, hSocket, sysIpcMinThreadStackSize, PRIO_NORMAL, "RFSServer" );
		}
		else
			::Sleep(1000);
#endif
	}
#if __WIN32
	// Terminate the prefetcher thread.
	s_PrefetchRequests.Push(remotePrefetchRequest(""));
	fiDeviceTcpIp::GetInstance().Close(listener);
#endif
}

#endif  //defined(IS_SERVER)

// DOM-IGNORE-END

volatile unsigned int fiIsShowingMessageBox;

#if __NO_OUTPUT 

unsigned fiRemoteShowMessageBox(const char *,const char *,unsigned,unsigned defaultAnswer,unsigned) { return s_ClientPort? defaultAnswer : defaultAnswer; }
unsigned fiRemoteShowAssertMessageBox(const char *,const char *,unsigned,unsigned defaultAnswer) { return s_ClientPort? defaultAnswer : defaultAnswer; }

#elif !__WIN32PC

static sysCriticalSectionToken messageToken;

unsigned fiRemoteShowMessageBox(const char *message,const char *title,unsigned flags,unsigned defaultAnswer,unsigned) {
	if (PARAM_nopopups.Get())
		return defaultAnswer;

	sysCriticalSection messageSection(messageToken);
	fiHandle tempSocket;
	if (s_ClientPort == 0 || ((tempSocket = fiDeviceTcpIp::Connect(s_ClientAddr,s_ClientPort)) == fiHandleInvalid)) {
		static int spewed = 0;
		if (spewed < 5)
		{
			spewed++; 
			Errorf("MessageBox requires RFS in order to work");
		}
		return defaultAnswer;
	}
	s_LastMessageBoxShownTimestamp = TIME.GetFrameCount();

	coreMessageBox mb(title,message,flags);
	coreMessageBoxReply mbr(-1);
	sysInterlockedIncrement(&fiIsShowingMessageBox);
	clientRequestAndWaitReplyEx(tempSocket,&mb,sizeof(mb),NULL,0, &mbr,sizeof(mbr),NULL,0);
	fiDeviceTcpIp::GetInstance().Close(tempSocket);
	sysInterlockedDecrement(&fiIsShowingMessageBox);
	return mbr.result;
}

unsigned fiRemoteShowAssertMessageBox(const char *message,const char *title,unsigned flags,unsigned defaultAnswer) {
	if (PARAM_nopopups.Get())
		return defaultAnswer;

	sysCriticalSection messageSection(messageToken);
	fiHandle tempSocket;
	if (s_ClientPort == 0 || ((tempSocket = fiDeviceTcpIp::Connect(s_ClientAddr,s_ClientPort)) == fiHandleInvalid)) {
		static int spewed = 0;
		if (spewed < 5)
		{
			spewed++; 
			Errorf("MessageBox requires RFS in order to work");
		}
		return defaultAnswer;
	}

	s_LastMessageBoxShownTimestamp = TIME.GetFrameCount();

	coreAssertMessageBox mb(title,message,flags);
	coreMessageBoxReply mbr(-1);
	sysInterlockedIncrement(&fiIsShowingMessageBox);
	clientRequestAndWaitReplyEx(tempSocket,&mb,sizeof(mb),NULL,0, &mbr,sizeof(mbr),NULL,0);
	fiDeviceTcpIp::GetInstance().Close(tempSocket);
	sysInterlockedDecrement(&fiIsShowingMessageBox);
	return mbr.result;
}
#elif __WIN32
#include "system/xtl.h"

extern bool diagWindowClosed;

unsigned fiRemoteShowMessageBox(const char *message,const char *title,unsigned flags,unsigned defaultAnswer,unsigned options) 
{
#if RSG_PC && !__TOOL
	if(diagWindowClosed)
		return IDIGNORE;
#endif

	if (PARAM_nopopups.Get())
		return defaultAnswer;
	sysInterlockedIncrement(&fiIsShowingMessageBox);
	unsigned result = CustomMessageBox(NULL,message,title,flags,options);
	sysInterlockedDecrement(&fiIsShowingMessageBox);
	return result;
}


unsigned CustomMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType, UINT uOptions)
{
	unsigned rc = 0;
	XMSGBOXPARAMS xmb;
	xmb.nTimeoutSeconds = uOptions & MBO_TIMEOUT;
	xmb.bIsAbortRetryIgnoreWindow = (uOptions & MBO_ASSERTKEYBOARD) > 0;

	const char *term = strchr(lpText,'\r');
	if ((uType & MB_TYPEMASK) == MB_XMESSAGEBOX && term)
	{
		uType &= ~MB_TYPEMASK;
		memcpy(xmb.szCustomButtons, lpText, term - lpText);
		xmb.szCustomButtons[term - lpText] = '\0';
		lpText = term + 1;

		rc = XMessageBox(hWnd, lpText, lpCaption, uType, &xmb);
		if ((rc & 0xFF) >= IDCUSTOM1 && (rc & 0xFF) <= IDCUSTOM4)
			return (rc & 0xFF) - (IDCUSTOM1-1);
		else
			return 0;
	}
	else if((uType & MB_TYPEMASK) == MB_ABORTRETRYIGNORE)
	{
		uType &= ~MB_TYPEMASK;
		_tcscpy(xmb.szCustomButtons, _T("Abort\nRetry\nIgnore"));

		rc = XMessageBox(hWnd, lpText, lpCaption, uType, &xmb);

		// Patch up the return code
		if ((rc & 0xFF) == IDCUSTOM1)
		{
			rc = IDABORT;
		}
		else if ((rc & 0xFF) == IDCUSTOM2)
		{
			rc = IDRETRY;
		}
		else if ((rc & 0xFF) == IDCUSTOM3)
		{
			rc = IDIGNORE;
		}
	}
	else
	{
		rc = XMessageBox(hWnd, lpText, lpCaption, uType, &xmb);
	}

	return rc;
}


unsigned fiRemoteShowAssertMessageBox(const char *message,const char *title,unsigned flags,unsigned defaultAnswer) {
	return fiRemoteShowMessageBox(message,title,flags,defaultAnswer);
}
#endif

unsigned fiGetFramesSinceLastMessageBox() {
	return TIME.GetFrameCount() - s_LastMessageBoxShownTimestamp;
}

int fiRemoteExec(const char *cmdline) {
	coreExec exec(cmdline);
	coreExecReply execr(-1);
	clientRequestAndWaitReply(&exec,&execr,sizeof(execr));
	return execr.result;
}

int fiRemoteTty(const char *msg) {
	coreTty tty(msg);
	coreTtyReply ttyr;
	clientRequestAndWaitReply(&tty,&ttyr,sizeof(ttyr));

	return ttyr.result;
}

int fiRemoteGetEnv(const char *envName,char *dest,int destSize) {
	coreGetEnv env(envName);
	coreGetEnvReply envr(NULL);

	if (!s_ClientPort)
		return -1;

	clientRequestAndWaitReply(&env,&envr,sizeof(envr));
	if (!envr.result)
		safecpy(dest,envr.buffer,destSize);
	return envr.result;
}

} // namespace rage

#endif	// !__FINAL && __WIN32
