// 
// file/tcpip.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_TCPIP_H
#define FILE_TCPIP_H

#include "file/device.h"

namespace rage {

class fiDeviceTcpIp: public fiDevice {
public:
	fiDeviceTcpIp();
	~fiDeviceTcpIp();

	static void InitClass(int argc, char** argv);
	static void WaitForNetworkReady();
	static void ShutdownClass();
	static void SetLocalHost(const char *hostname);
	static const char *GetLocalHost();
	static int PrintNetStats();
	static int PrintInterfaceState();
	static void GetRemoteName(fiHandle handle,char *buffer,int bufferSize);

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle Create(const char *filename) const;
	static int StaticRead(fiHandle handle,void *outBuffer,int bufferSize);
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const { return StaticRead(handle,outBuffer,bufferSize); }
	static int StaticWrite(fiHandle handle,const void *buffer,int bufferSize);
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const { return StaticWrite(handle,buffer,bufferSize); }
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return NETWORK; }
	
	// PURPOSE: Return a string describing the last error that was detected.
	static const char *GetLastError();

	// PURPOSE: Set the string that will be returned on the next call to GetLastError().
	static void SetLastError(const char *fmt, ...);

	// PURPOSE: Sets blocking mode for this socket - defaults to ON
	static void SetBlocking(fiHandle handle, bool blocking);

	// PURPOSE:	Connect to an existing server at an address and port.
	// PARAMS:	address - address of server; either hostname or raw ip dot address
	//			port - port number to connect on
	// RETURNS:	Valid handle if connection is successful, fiHandleInvalid if bad
	//			address, port, or other connection problem
	// NOTES:	Open and Create both call this function internally after parsing
	//			the name.
	static fiHandle Connect(const char *address,int port);

	// PURPOSE:	Install a listen socket on the specified port
	// PARAMS:	port - Port number to install connection on
	//			maxIncoming - Maximum number of connections to allow
	//			ip - If not null, bind on this named ip address instead of default; WIN32PC only.
	// RETURNS:	Socket to pass to Pickup when waiting for a new connection,
	//			or fiHandleInvalid if it failed (most likely because the local
	//			port is already in use)
	static fiHandle Listen(int port,int maxIncoming,const char *ip = NULL);

	// PURPOSE:	Pickup an incoming connection (for use on the server side)
	// PARAMS:	listenSocket - Socket returned by Listen
	// RETURNS:	New data connection socket we can send and receive data on,
	//			or fiHandleInvalid if something went wrong.
	static fiHandle Pickup(fiHandle listenSocket);

	// PURPOSE:	Returns amount of data bytes waiting on the socket
	// PARAMS:	socket - socket to check
	// RETURNS:	Number of bytes waiting to read, or zero if none available
	// NOTES:	Call this first to avoid blocking on a read.
	static int GetReadCount(fiHandle socket);

	// PURPOSE: Returns the local IP address as a string
	// NOTES: This address is actually set by the rage netHardware class, so regular rage networking must be initted or it will return '127.0.0.1'
	static const char* GetLocalIpAddrName();

	// PURPOSE: For use by the netHardware class, once the local IP is discovered.
	static void SetLocalIpAddrName(const char* name);

	// Singleton accessor
	static const fiDeviceTcpIp& GetInstance();
};

}	// namespace rage

#endif	// FILE_TCPIP_H
