// 
// file/decompress.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_DECOMPRESS_H
#define FILE_DECOMPRESS_H

namespace rage {

/*	PURPOSE	Decompress data compressed with fiCompress
	PARAMS	dest - Output buffer
			destSize - size of output buffer
			src - Compressed data stream
			srcSize - Size of input compressed data stream
	RETURNS	True if decompression was successful, or false on failure (due to bad input data).
			Should never crash on malformed inputs. */
bool fiDecompress(u8 *dest,u32 destSize,const u8 *src,u32 srcSize);

}	// namespace rage

#endif
