// 
// file/token.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_TOKEN_H
#define FILE_TOKEN_H

namespace rage {

class fiStream;
class Vector2;
class Vector3;
class Vector4;
class Vec_xyz;
class Vec_xyzw;
class ScalarV;
class Vec2V;
class Vec3V;
class Vec4V;
class Mat33V;
class Mat34V;

/*
fiBaseTokenizer implements a simple word-based file tokenizer.

Its use is deprecated in new code so it will not be documented further.
<FLAG Component>
*/
class fiBaseTokenizer {
public:
	enum {PUT_BUFFER_SIZE=512, PUSH_BUFFER_SIZE=512};
public:
	virtual ~fiBaseTokenizer() { }

	void Init(const char *name,fiStream *s);
	const char * GetName () {return filename;}
	fiStream * GetStream() { return S; }
	virtual bool IsBinary() const = 0;
	void Reset();

	// read functions:
	virtual int		GetToken(char *dest,int maxLen);
	int		GetBlock(char *dest,int length);	// get a block of "length" characters
	void	MatchToken(const char *match);
	void	MatchIToken(const char *match);
	bool	CheckToken(const char *check, bool consume=true);
	bool	CheckIToken(const char *check, bool consume=true);
	int		GetTokenToChar(char *dest,int maxLen,char terminator,bool stripWhiteSpace=true);
	int		GetTokenToChars(char *dest,int maxLen,const char* terminators, int numTerminators, char& terminator, bool stripWhiteSpace=true);
	void	SkipToEndOfLine();
	int		GetLine(char *dest,int maxLen,bool stripWhiteSpace=true)
	{ int retval; do retval = GetTokenToChar(dest,maxLen,'\n',stripWhiteSpace); while (retval == 0); return retval; }
	void	IgnoreToken();
	void	Pop();	// pop the current block as defined by "{" and "}" delimiters.  this will also pop any sub-blocks.  useful for file formats in a state of flux.
	void	SetStreamPosition(int position);
	int		GetStreamPosition();

	// PURPOSE: Sets a list of characters that should be used for terminating tokens
	//	and skipped over.
	// NOTES:
	//	This *must* be a pointer to static string storage (ie a C string constant such as ",:/")
	void SetTerminators(const char* t) { terminators = t; }

	// PURPOSE: Reads a numerical value and returns it
	// PARAMS:
	//		v - (if present) The vector return value
	// RETURNS: The value read in (for non-vector Get* calls)
	virtual int		GetByte(const bool error=true)=0;
	virtual int		GetShort(const bool error=true)=0;				// <ALIAS fiBaseTokenizer::GetByte>
	virtual int		GetInt(const bool error=true)=0;				// <ALIAS fiBaseTokenizer::GetByte>
	virtual float	GetFloat(const bool error=true)=0;				// <ALIAS fiBaseTokenizer::GetByte>
	virtual void	GetVector(Vector2& v,const bool error=true)=0;	// <ALIAS fiBaseTokenizer::GetByte>
	virtual void	GetVector(Vector3& v,const bool error=true)=0;	// <ALIAS fiBaseTokenizer::GetByte>
	virtual void	GetVector(Vector4& v,const bool error=true)=0;	// <ALIAS fiBaseTokenizer::GetByte>
	virtual void	GetVector(Vec_xyz& v,const bool error=true)=0;	// <ALIAS fiBaseTokenizer::GetByte>
	virtual void	GetVector(Vec_xyzw& v,const bool error=true)=0;	// <ALIAS fiBaseTokenizer::GetByte>

	virtual void	GetDelimiter(const char* de)=0;

	// PURPOSE: Match a token and return the subsequent value
	// RETURNS: The matched value (for non-vector Match* calls)
	// PARAMS:
	//		match - The token to match (matching is case sensitive)
	//		v - (if present) The vector return value
	// NOTES: For reading values in a format like "MaxVelocity 10.0"
	virtual int		MatchInt(const char *match)=0;
	virtual float	MatchFloat(const char *match)=0;				// <ALIAS fiBaseTokenizer::MatchInt@const char *>
	virtual void	MatchVector(const char *match,Vector2& v)=0;	// <ALIAS fiBaseTokenizer::MatchInt@const char *>
	virtual void	MatchVector(const char *match,Vector3& v)=0;	// <ALIAS fiBaseTokenizer::MatchInt@const char *>
	virtual void	MatchVector(const char *match,Vector4& v)=0;	// <ALIAS fiBaseTokenizer::MatchInt@const char *>
	virtual void	MatchVector(const char *match,Vec_xyz& v)=0;	// <ALIAS fiBaseTokenizer::MatchInt@const char *>
	virtual void	MatchVector(const char *match,Vec_xyzw& v)=0;	// <ALIAS fiBaseTokenizer::MatchInt@const char *>

	// PURPOSE: Case insensitive version of Match*. Match a token and return the subsequent value
	// RETURNS: The matched value (for non-vector Match* calls)
	// PARAMS:
	//		match - The token to match (matching is not case sensitive)
	//		v - (if present) The vector return value
	// NOTES: For reading values in a format like "MaxVelocity 10.0"
	virtual int		MatchIInt(const char *match)=0;
	virtual float	MatchIFloat(const char *match)=0;				// <ALIAS fiBaseTokenizer::MatchIInt@const char *>
	virtual void	MatchIVector(const char *match,Vector2& v)=0;	// <ALIAS fiBaseTokenizer::MatchIInt@const char *>
	virtual void	MatchIVector(const char *match,Vector3& v)=0;	// <ALIAS fiBaseTokenizer::MatchIInt@const char *>
	virtual void	MatchIVector(const char *match,Vector4& v)=0;	// <ALIAS fiBaseTokenizer::MatchIInt@const char *>
	virtual void	MatchIVector(const char *match,Vec_xyz& v)=0;	// <ALIAS fiBaseTokenizer::MatchIInt@const char *>
	virtual void	MatchIVector(const char *match,Vec_xyzw& v)=0;	// <ALIAS fiBaseTokenizer::MatchIInt@const char *>

	// Formatting functions
		
	virtual void StartBlock()	{;}// Increases the tab by one
	virtual void EndBlock()		{;}// Decreases the tab by one
	virtual void StartLine()	{;}// Indents by tab
	virtual void EndLine()		{;}// Starts a new line
	virtual void Indent(int)	{;}

	// write functions:
	virtual bool	Put(const char* src,unsigned int tabs=0)=0;
	virtual bool	PutDelimiter(const char* de)=0;
			bool	PutStr(const char* fmt, ...);
	/*virtual*/ bool	Put(char c);
			bool	Put(bool b)		{return Put((int)b);}
	virtual bool	PutByte(int i)=0;
	virtual bool	PutShort(int i)=0;
	virtual bool	Put(int i)=0;
	virtual bool	Put(float f)=0;
	virtual bool	Put(const Vector2& f)=0;
	virtual bool	Put(const Vector3& f)=0;
	virtual bool	Put(const Vector4& f)=0;

	// convenience functions
	bool PutStrLine (const char* fmt, ...);

	const char *filename;
	int line;
	static int CommentChar;

	virtual bool	EndOfFile();

	int GetNextCh() const { return nextch; }
	void SetNextCh(int nextchvalue) { nextch = nextchvalue; }
	int GetWriteState() const { return m_WriteState; }
	void SetWriteState( int writestate ) 
	{ 
		Assert( writestate >= (int)OTHER && writestate <= (int)ENDLINE );
		m_WriteState = (fiBaseTokenizer::enState)writestate; 
	}

protected:
	void SkipComment();

	fiStream *S;
	int nextch;
	enum enState{ OTHER, STARTLINE, ENDLINE } m_WriteState;

	inline int iswhitespace(int ch) {
		return ch == 32 || ch == 9 || ch == 10 || ch == 13 || ch == 0;
	}

	int GetTokenCh();

private:
	void PushBack(const char*,int);
	int pushcount;
	char pushbuf[PUSH_BUFFER_SIZE];
	const char *terminators;
};

/*
fiAsciiTokenizer implements a concrete subclass of fiTokenizer for handling text files.
<FLAG Component>
*/
class fiAsciiTokenizer : public fiBaseTokenizer {
public:
	fiAsciiTokenizer():m_nNumTabs(0){}
	// read functions:
	int		GetByte(const bool error=true);
	int		GetShort(const bool error=true);
	int		GetInt(const bool error=true);
	u32		GetHexU32(const bool error=true);
	float	GetFloat(const bool error=true);
	void	GetVector(Vector2&, const bool error=true);
	void	GetVector(Vector3&, const bool error=true);
	void	GetVector(Vector4&, const bool error=true);
	void	GetVector(Vec_xyz&, const bool error=true);
	void	GetVector(Vec_xyzw&, const bool error=true);
	void	GetDelimiter(const char* de);
	bool	IsBinary() const { return false; }

	int		MatchInt(const char *match);
	float	MatchFloat(const char *match);
	void	MatchVector(const char *match,Vector2& v);
	void	MatchVector(const char *match,Vector3& v);
	void	MatchVector(const char *match,Vector4& v);
	void	MatchVector(const char *match,Vec_xyz& v);
	void	MatchVector(const char *match,Vec_xyzw& v);

	int		MatchIInt(const char *match);
	float	MatchIFloat(const char *match);
	void	MatchIVector(const char *match,Vector2& v);
	void	MatchIVector(const char *match,Vector3& v);
	void	MatchIVector(const char *match,Vector4& v);
	void	MatchIVector(const char *match,Vec_xyz& v);
	void	MatchIVector(const char *match,Vec_xyzw& v);

	// Formatting functions
	virtual void StartBlock();	// Increases the tab by one
	virtual void EndBlock();	// Decreases the tab by one
	virtual void StartLine();	// Indents by tab
	virtual void EndLine();		// Starts a new line
	virtual void Indent(int i) { m_nNumTabs += i; }

	// write functions:
	bool	Put(const char* src,unsigned int tabs=0);
	bool	PutDelimiter(const char* de);
	bool	PutByte(int i);
	bool	PutShort(int i);
	bool	Put(int i);
	bool	Put(float f);
	bool	Put(const Vector2& v);
	bool	Put(const Vector3& v);
	bool	Put(const Vector4& v);
	
	bool	Put(const ScalarV& v);
	bool	Put(const Vec2V& v);
	bool	Put(const Vec3V& v);
	bool	Put(const Vec4V& v);
	bool	Put(const Mat33V& v);
	bool	Put(const Mat34V& v);

protected:
	void WriteTabs(int nNumTabs);
protected:
	int m_nNumTabs; // Holds the current indentation tabs

};

/*
PURPOSE
fiBinTokenizer implements a concrete subclass of fiTokenizer for handling binary files.
<FLAG Component>
*/
class fiBinTokenizer : public fiBaseTokenizer {
public:
	// read functions:
	int		GetByte(const bool error=true);
	int		GetShort(const bool error=true);
	int		GetInt(const bool error=true);
	float	GetFloat(const bool error=true);
	void	GetVector(Vector2&,const bool error=true);
	void	GetVector(Vector3&,const bool error=true);
	void	GetVector(Vector4&,const bool error=true);
	void	GetVector(Vec_xyz&,const bool error=true);
	void	GetVector(Vec_xyzw&,const bool error=true);
	void	GetDelimiter(const char* de);
	bool	IsBinary() const { return true; }

	int		MatchInt(const char *match);
	float	MatchFloat(const char *match);
	void	MatchVector(const char *match,Vector2& v);
	void	MatchVector(const char *match,Vector3& v);
	void	MatchVector(const char *match,Vector4& v);
	void	MatchVector(const char *match,Vec_xyz& v);
	void	MatchVector(const char *match,Vec_xyzw& v);

	int		MatchIInt(const char *match);
	float	MatchIFloat(const char *match);
	void	MatchIVector(const char *match,Vector2& v);
	void	MatchIVector(const char *match,Vector3& v);
	void	MatchIVector(const char *match,Vector4& v);
	void	MatchIVector(const char *match,Vec_xyz& v);
	void	MatchIVector(const char *match,Vec_xyzw& v);

	// write functions:
	bool	Put(const char* src,unsigned int tabs=0);
	bool	PutDelimiter(const char* de);
	bool	PutByte(int i);
	bool	PutShort(int i);
	bool	Put(int i);
	bool	Put(float f);
	bool	Put(const Vector2& v);
	bool	Put(const Vector3& v);
	bool	Put(const Vector4& v);
};

/*
	fiMultiTokenizer allows
*/
class fiMultiTokenizer
{
public:
	fiMultiTokenizer();
	fiBaseTokenizer& GetReadTokenizer(const char* name,fiStream* s,const char* asciiString,const char* binString);
	fiBaseTokenizer& GetWriteTokenizer(const char* name,fiStream* s,bool bin,const char* versString);

private:
	fiAsciiTokenizer	Ascii;
	fiBinTokenizer		Bin;
};

/*
PURPOSE
This class is to keep current code compiling:
*/
class fiTokenizer : public fiAsciiTokenizer
{
public:
	fiTokenizer();
	fiTokenizer(const char *name,fiStream *s);
};

}		// namespace rage

#endif
