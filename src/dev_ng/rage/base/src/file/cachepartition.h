//
// file/cachpartition.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef FILE_CACHEPARTITION_H
#define FILE_CACHEPARTITION_H

#include "file/handle.h"

#if RSG_SCE || RSG_DURANGO
#define CUSTOM_PERSISTENT_CACHE 1
#else
#define CUSTOM_PERSISTENT_CACHE 0
#endif

#if CUSTOM_PERSISTENT_CACHE
#define CUSTOM_PERSISTENT_CACHE_ONLY(...) __VA_ARGS__
#else
#define CUSTOM_PERSISTENT_CACHE_ONLY(...)
#endif

namespace rage {

struct fiFindData;

typedef void(*fiListFileFunc)(const char*, fiHandle, const fiFindData&);

class fiCachePartition
{
public:
	// PARAMS: forceClean - True to force partition to be reset during mount.  Note
	//		that this is ignored if you were not the first client of the cache.
	// RETURNS: True if cache partition could be mounted.  Safe to call
	// multiple times; just make sure you have matching calls to fiShutdownCachePartition.
	// Note that even if the game was installed to a hard drive on 360, this function will
	// still work properly.
	static bool Init(bool forceClean = false);

	// RETURNS: True if cache could be reset.  Note that you are not allowed to reset the
	// cache drive unless you are the only client.
	static bool Reset();

	// RETURNS: True if Init succeeded.
	static bool IsAvailable() { return sm_RefCount != 0; }

	// RETURNS: Cache drive prefix (including trailing slash if necessary)
	static const char *GetCachePrefix() { return sm_CachePrefix; }

	// NOTES: This should be called when you're done with the partition
	// but only call it if the init function returns true.  Current only does something on 360.
	static void Shutdown();

	// Delete all files in the cache folder. WARNING: Use at your own risk
	static void DeleteCacheFiles();

private:
	static int sm_RefCount;
	static char sm_CachePrefix[];
};

//PURPOSE
//  Persistent cache points to the cache folders which are not automatically deleted
//  by the console. There are still ways for the user to delete them manually
//  but usually the files will be available much longer.
//  The consoles don't automatically free these files up so the management is up to the game here.
class fiPersistentCachePartition
{
public:

	//PURPOSE
	//  Initialize the cache with the specified folder name. Can be ""
	static void Init(const char* folderName);

	//PURPOSE
	//  Shutdown the cache
	static void Shutdown();

	// RETURNS: Cache drive prefix (including trailing slash if necessary)
	static const char *GetCachePrefix();

	//PURPOSE
	// Delete all files in the cache folder. WARNING: Use at your own risk
	static void DeleteCacheFiles();

	//PURPOSE
	//  Delete all files which are older than a month
	static void DeleteOldFiles();

	//PURPOSE
	//  Returns the path to access the root of the persistent storage partition.
	//  Returns null if the platform has no specific persistent storage location.
	//  Does not necessarily match the fiPersistentCachePartition path in
	//  GetCachePrefix() which can be a sub-folder of GetPersistentStorageRoot.
	static const char* GetPersistentStorageRoot();

private:
	//PURPOSE
	//  Create the directory the files will live in
	static bool CreateDir();

private:
	static char sm_CachePrefix[];
};

class fiCacheUtils
{
public:
	//PURPOSE
	//  Iterate through all files and folders in pathname and call func for each
	static bool ListFiles(const char* pathname, fiListFileFunc func);

	//PURPOSE
	//  Can be used as func for ListFiles to print a file to the tty and log
	static void PrintFileFunc(const char* path, fiHandle handle, const fiFindData& data);

	//PURPOSE
	//  Can be used as func for ListFiles to delete the file if it's older than a month
	static void DeleteOldFileFunc(const char* path, fiHandle UNUSED_PARAM(handle), const fiFindData& data);

	//PURPOSE
	//  Set how old a file can be before DeleteOldFileFunc deletes it.
	//  A value of 0 means no file will be deleted.
	static void SetFileCacheDurationDays(const unsigned days);

protected:
	static unsigned sm_FileCacheDurationDays;
};

}

#endif
