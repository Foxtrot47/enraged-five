// 
// file/compress.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "compress.h"
#include "compress_internal.h"

#if LOG_EVERYTHING
#include "file/stream.h"
#endif

#include <string.h>

#include "system/new.h"

namespace rage {

struct EncoderState {
	u32 m_Accum;
	u32 m_NextAccum;
	u32 m_Offset;
	u8 *m_Output;
	u32 m_OutputSize;
};


static inline u32 leading_match(const u8 *a,const u8 *b,u32 maxLen) {
	u32 count = 0;
	while (count < maxLen && *a == *b) {
		++count;
		++a;
		++b;
	}
	return count;
}


inline void put_bits(EncoderState &state,u32 value,u32 width) {
	Assert(width < 32);
	Assert(value < (1U<<width));
	//for (u32 i=width; i; )
	//	Printf("%d",value&(1<<(--i))? 1:0);
	//Displayf("");
	while (width) {
		if (state.m_Accum == 1) {
			state.m_NextAccum = state.m_Offset++;
			Assert(state.m_NextAccum < state.m_OutputSize);
		}
		state.m_Accum = (state.m_Accum << 1) | ((value >> --width) & 1);
		if (state.m_Accum & 0x100) {
			state.m_Output[state.m_NextAccum] = u8(state.m_Accum);
			state.m_Accum = 1;
		}
	}
}


// 2/2: 56.25M->11.21M
// 2/3:       ->10.65M
// 2/4:		  ->10.43M

inline u32 encode_offset(EncoderState &state,u32 len) {
	// 0xx = 1,2,3,4 (when c_InitialWidth=2)
	// 10xxxxxx = 5,6,7...
	// 110xxxxxxxxxx
	// Every leading one adds c_WidthStep more bits (and builds on the previous range)
	Assert(len);
	u32 leader = 1;
	u32 width = c_InitialWidth;
	u32 mini = 1;
	u32 maxi = 1<<c_InitialWidth;
	while (len > maxi) {
		put_bits(state,1,1);
		mini = maxi + 1;
		width += c_WidthStep;
		maxi += (1<<width);
		++leader;
	}
	put_bits(state,0,1);
	Assert(len >= mini && len <= maxi);
	put_bits(state,len - mini,width);
	return leader + width;
}


static u32 cost_for_offset(u32 len) {
	u32 leader = 1;
	u32 width = c_InitialWidth;
	// u32 mini = 1;
	u32 maxi = 1<<c_InitialWidth;
	while (len > maxi) {
		// mini = maxi + 1;
		width += c_WidthStep;
		maxi += (1<<width);
		++leader;
	}
	return leader+width;
}

inline u32 encode_length(EncoderState &state,u32 len) {
	// 1   -> 1
	// 000 -> 2
	// 001 -> 3
	// 010 -> 4
	// 011xxx -> as per encode_offset
	if (len == 1) {
		put_bits(state,1,1);
		return 1;
	}
	else if (len==2||len==3||len==4) {
		put_bits(state,len-2,3);
		return 3;
	}
	else {
		// fall through to common encoding (albeit with longer prefix)
		put_bits(state,1,2);
		return 2 + encode_offset(state,len);
	}
}


struct node {
	u32 offset;		// offset of the two leader bytes
	node *next;		// pointer to next node
};

u32 fiCompress(u8 *dest,u32 destSize,const u8 *src,u32 srcSize,u32 maxLookback,bool fullUpdate) {
	if (srcSize < 4)
		return srcSize;

	const u32 c_MaxLookback = 1U << maxLookback;
	const int lookup_size = 65536;
	node **lookup = rage_new node*[lookup_size];
	u32 poolSize = srcSize + 1024;
	node *pool = rage_new node[poolSize];
	u32 poolCount = 0;
	memset(lookup,0,sizeof(node*) * lookup_size);
	// const u8 *origSrc = src;

	u32 literalCount = 0, windowCount = 0;
	u32 literalRun = 0;
	u64 estimate = 0;

	EncoderState state;
	state.m_Accum = 1;
	state.m_Offset = 2;
	state.m_NextAccum = 2;
	state.m_Output = dest;
	state.m_OutputSize = destSize;

	dest[0] = c_Magic_1;
	dest[1] = c_Magic_2;

#if LOG_EVERYTHING
	fiSafeStream logfile(fiStream::Create("t:\\compress.txt"));
#endif

#define SPEW	0

	u32 offset = 0;
#if SPEW
	u32 lastPct = 0;
	u32 denom = srcSize / 100;
	if (!denom) denom = 1;
#endif
	// Displayf("[COMP]");
	while (offset < srcSize-2) {
#if SPEW
		u32 pct = offset / denom;
		if (pct != lastPct) {
#if !LOG_EVERYTHING
			Printf("%3u%% (%3u%% of orig size)    \b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b",pct,
				(u32(estimate>>3) * 100) / offset);
#endif
			lastPct = pct;
		}
#endif
		int key = src[offset] | (src[offset+1]<<8);
		node **pi = &lookup[key];
		node *i = *pi;
		u32 bestRun = 0;
		u32 bestOffset = 0;
		while (i) {
			// Runs are stored with closest ones first in the list, so earlier runs will always be cheaper.
			// For short runs, do a sanity check to make sure that storing it as a literal run wouldn't be cheaper.
			u32 thisRun = leading_match(src + i->offset,src + offset,srcSize-offset);
			if (thisRun > bestRun && 
				( (thisRun == 2 && 2+cost_for_offset(offset - i->offset) < (2+2*8) /* size of a 2-literal run */)
				||(thisRun == 3 && 4+cost_for_offset(offset - i->offset) < (4+3*8) /* size of a 3-literal run */)
				||(thisRun == 4 && 4+cost_for_offset(offset - i->offset) < (4+4*8) /* size of a 4-literal run */)
				||(thisRun > 4)
				)) {
				bestRun = thisRun;
				bestOffset = i->offset;
			}
			else if (offset - i->offset > c_MaxLookback) {
				// Delete nodes that are too far back.
				*pi = i->next;
				i = i->next;
				continue;
			}
			pi = &i->next;
			i = *pi;
		}
		Assert(poolCount < poolSize);
		node *nn = pool + poolCount++;
		nn->offset = offset;
		nn->next = lookup[key];
		lookup[key] = nn;

#define FLUSH_RUN \
	if (literalRun) { \
		LOG1("**L%u\r\n",literalRun); \
		estimate += 1 + encode_length(state,literalRun); \
		put_bits(state,0,1); \
		for (u32 i=0; i<literalRun; i++) { \
			estimate += 8; \
			state.m_Output[state.m_Offset++] = src[offset - literalRun + i]; \
			LOG1(" **[%d]\r\n",src[offset - literalRun + i]); \
			Assert(state.m_Offset < state.m_OutputSize); \
		} \
		literalRun = 0; \
	}

		if (bestRun>1) {
			FLUSH_RUN

			LOG2("**W%u,%u\r\n",bestRun,offset-bestOffset);
			estimate += encode_length(state,bestRun-1);
			put_bits(state,1,1);
			estimate += 1 + encode_offset(state,offset - bestOffset);

			++windowCount;

			// Add "closer" copy of the windowed data to the work queue.
			// (the initial two bytes of the window run were already added above)
			while (--bestRun) {
				++offset;
				// This produces slightly better results but is never practical except for lookbacks 64k or less
				if (fullUpdate) {
					int key = src[offset] | (src[offset+1]<<8);
					Assert(poolCount < poolSize);
					node *nn = pool + poolCount++;
					nn->offset = offset;
					nn->next = lookup[key];
					lookup[key] = nn;
				}
			}
			++offset;
		}
		else {
			++literalRun;
			++literalCount;
			++offset;
		}
	}

	literalRun += (srcSize-offset);
	offset = srcSize;
	FLUSH_RUN

	// Flush the last control byte if necessary.
	if (state.m_Accum > 1)
		put_bits(state,0,8);

	// clean up allocations
	delete[] pool;
	delete[] lookup;

	return state.m_Offset;
}


u32 fiCompressUpperBound(u32 srcSize) {
	if (srcSize < 256)
		return 512;
	else
		return (srcSize * 5) >> 2;
}

}	// namespace rage
