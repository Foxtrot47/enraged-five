#ifndef FILE_CONFIG_H
#define FILE_CONFIG_H

#if !defined(RSG_DURANGO)
#define RSG_DURANGO 0
#endif

#if !defined(__GFWL)
#define __GFWL                  (0 && __WIN32PC)
#endif

#ifndef __RGSC_DLL
#define __RGSC_DLL					(0)
#endif

#if __RGSC_DLL
#define RGSC_DLL_ONLY(...) __VA_ARGS__
#else
#define RGSC_DLL_ONLY(...)
#endif

#if !defined(__STEAM_BUILD)
// Turn this on, only when building for the Steam platform.
#define __STEAM_BUILD				(0 && __WIN32PC && !__RGSC_DLL)
#endif

// Epic API
#if RSG_PC && !__GAMETOOL && !__TOOL && !__RESOURCECOMPILER  && !__RGSC_DLL
#define EPIC_API_SUPPORTED 1
#define EPIC_API_ONLY(...) __VA_ARGS__
#else
#define EPIC_API_SUPPORTED 0
#define EPIC_API_ONLY(...)
#endif

// OSX Builds
#if !defined(__MAC_APPSTORE_BUILD)
#define __MAC_APPSTORE_BUILD		(0 && !__RGSC_DLL)
#endif

#if __STEAM_BUILD
#define STEAMBUILD_ONLY(...) __VA_ARGS__
#else
#define STEAMBUILD_ONLY(...)
#endif

// Turn this on when building for a PC platform that must be run from a launcher
#define RSG_LAUNCHER_CHECK	(RSG_FINAL && RSG_PC)

//__LIVE should be defined for titles that uses XDK code
#if !defined(__LIVE)
#define __LIVE                  (__GFWL || __XENON)
#endif

#if __LIVE
#define LIVE_ONLY(x) x
#else
#define LIVE_ONLY(x)
#endif

// XBox and PS4 support player parties between titles
#if __LIVE || RSG_ORBIS || RSG_DURANGO
#define PARTY_PLATFORM 1
#define PARTY_PLATFORMS_ONLY(x) x
#else
#define PARTY_PLATFORM 0
#define PARTY_PLATFORMS_ONLY(x)
#endif

#if __WIN32PC
#ifndef DBG
#if __DEV
#define DBG 1
#else
#define DBG 0
#endif
#endif
#endif

#if __GFWL
#define _WINSOCKAPI_
#endif //__GFWL

#if !defined(__USE_TUS)
#define __USE_TUS					(0)	
#endif

#endif //FILE_CONFIG_H
