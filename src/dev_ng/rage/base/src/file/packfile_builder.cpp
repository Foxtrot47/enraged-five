// 
// file/packfile_builder.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "packfile_builder.h"

#if __WIN32PC && !defined(__UNITYBUILD) && (!defined(__RGSC_DLL) || !__RGSC_DLL)

#include "data/aes.h"
#include "data/struct.h"
#include "diag/output.h"
#include "file/asset.h"
#include "system/param.h"
#include "system/xtl.h"
#include "paging/xcompress_7776.h"
#include "system/xcompress_settings.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/magicnumber.h"
#include "system/endian.h"
#include "system/memory.h"
#include "system/platform.h"
#include "zlib/zlib.h"

#if RSG_CPU_X64
#pragma comment(lib,"xcompress_x64_2008.lib")
#elif RSG_CPU_X86
#pragma comment(lib,"xcompress_2008.lib")
#endif

namespace rage {

CompileTimeAssertSize(datResourceFileHeader,16,16);

void fiRpf7Builder::Insert(u32 * idxRef,const char * name,const char *srcName,u32 dataLen,bool compressed) {
	u32 nameMask = (1<<m_nNameHeapShift)-1;

	while (*name) {
		// Parse out normalized path element
		char element[256];
		int len = 0;
		while (*name && *name != '/' && *name != '\\') {
			char ch = *name++;
			if (ch >= 'A' && ch <= 'Z')
				ch += 'a'-'A';
			if (len < (int)sizeof(element)-1)
				element[len++] = ch;
			else
				Quitf(ERR_DEFAULT,"Rpf7Entry::Insert - name part too long!");
		}
		while (*name == '/' || *name == '\\')
			++name;
		element[len] = 0;

		// See if this name has already been used before or not (common with directories, obviously)
		u32 scanOffset = 0;
		u32 nameOffset = 0;
		while (scanOffset < m_nNameHeapSize) {
			if (!stricmp(m_pNameHeap+scanOffset,element)) {
				nameOffset = scanOffset;
				break;
			}
			else
				scanOffset += (StringLength(m_pNameHeap+scanOffset) + 1 + nameMask) & ~nameMask;
		}

		// If name wasn't found, attempt to add it now
		if (!nameOffset) {
			nameOffset = m_nNameHeapSize;
			m_nNameHeapSize += (StringLength(element) + 1 + nameMask) & ~nameMask;
			if (m_nNameHeapSize > m_nNameHeapMaxSize) {
				size_t i = 0;
				Displayf("BEGIN CONTENTS OF NAMEHEAP");
				while (i != nameOffset) {
					Displayf("%s",m_pNameHeap + i);
					i += (ustrlen(m_pNameHeap+i)+1+nameMask) & ~nameMask;
				}
				Displayf("END CONTENTS OF NAMEHEAP");
				Quitf(ERR_DEFAULT,"Archive name heap full, use shorter names or pass in bigger offset shift (currently %d)",m_nNameHeapShift);
			}
			strcpy(m_pNameHeap + nameOffset,element);
		}

		// Locate the containing directory
		while (*idxRef && nameOffset != m_Nodes[*idxRef].NameOffset)
			idxRef = &m_Nodes[*idxRef].Next;

		// If this directory (or file) doesn't already exist, add it now.  Duplicates ought to be silently ignored by this code.
		if (!*idxRef) {
			*idxRef = m_Nodes.GetCount();
			m_Nodes.Append();
			m_Nodes[*idxRef].NameOffset = nameOffset;
			if (!*name) {
				m_Nodes[*idxRef].SourceName = (dataLen? (char*)srcName : StringDuplicate(srcName));
				m_Nodes[*idxRef].DataSize = dataLen;
				m_Nodes[*idxRef].Compressed = compressed;
			}
			else {
				m_Nodes[*idxRef].SourceName = NULL;
				m_Nodes[*idxRef].DataSize = 0;
				m_Nodes[*idxRef].Compressed = false;
			}
		}
		idxRef = &m_Nodes[*idxRef].Child;
	}
}

static const char *nameheap;
static int nameshift;

static int cmpEntry(const void *a,const void *b)
{
	return stricmp(nameheap + (((fiPackEntry*)a)->m_NameOffset << nameshift),nameheap + (((fiPackEntry*)b)->m_NameOffset << nameshift));
}


u32 fiRpf7Builder::BuildTree(u32 entry) 
{
	// Count number of items in this run
	u32 localCount = 0;
	for (u32 i = entry; i; i=m_Nodes[i].Next)
		++localCount;
	// Now add them to the flattened tree
	u32 j = m_nEntryCount, origEntryCount = j;
	m_nEntryCount += localCount;

	for (u32 i = entry; i; i=m_Nodes[i].Next, ++j) {
		fiPackEntry &e = m_Entries[j];
		e.m_NameOffset = (m_Nodes[i].NameOffset >> m_nNameHeapShift);
		e.m_ConsumedSize = 0;
		e.m_IsResource = 0;
		if (!m_Nodes[i].SourceName) {	// directory, not a file
			e.m_FileOffset = fiPackEntry::FileOffset_IsDir;
			e.u.directory.m_DirectoryIndex = m_nEntryCount;
			if (m_Nodes[i].Child)
				e.u.directory.m_DirectoryCount = BuildTree(m_Nodes[i].Child);
			else
				e.u.directory.m_DirectoryCount = 0;
		}
		else {	// file or resource (remember pointer to original entry so we can get name)
			e.m_FileOffset = 0;
			e.u.file.m_UncompressedSize = i;
			e.u.file.m_Encrypted = 0;
		}
	}

	nameheap = m_pNameHeap;
	nameshift = m_nNameHeapShift;
	qsort(m_Entries + origEntryCount,localCount,sizeof(fiPackEntry),cmpEntry);
	nameheap = NULL;
	nameshift = -1;
	return localCount;
}

PARAM(nameheapshift,"Specify name heap shift (default=0, 64k max size, can be up to 3 for 512k max size)");

// Constructor
fiRpf7Builder::fiRpf7Builder(int shift)
: m_Entries( NULL )
, m_nEntryCount( 0L )
, m_nNameHeapSize( 0L )
{
	PARAM_nameheapshift.Get(shift);
	m_nNameHeapShift = shift;
	Assert(shift <= 3);

	m_nNameHeapMaxSize = 65536 << shift;
	m_pNameHeap = rage_new char[m_nNameHeapMaxSize];

	u32 dummy = 0;
	Insert(&dummy,"/","",0,0);
}

// Destructor
fiRpf7Builder::~fiRpf7Builder( )
{
	if ( m_Entries )
		delete[] m_Entries;
	delete[] m_pNameHeap;
	m_nEntryCount = 0;
	m_nNameHeapSize = 0;
}


// Add a file to the pack file as the item specified in destination.
bool fiRpf7Builder::AddFile( const char* source, const char* destination )
{
	const fiDevice* pDevice = fiDevice::GetDevice(source);
	if ( !pDevice || (FILE_ATTRIBUTE_INVALID == pDevice->GetAttributes(source)) )
		return ( false );

	while (*destination == '/' || *destination == '\\')
		++destination;
	// Displayf("Add '%s' as '%s'",source,destination);
	const char *ext = strrchr(destination,'.');

	bool compressed = ext && strcmp(ext,".bik") && strcmp(ext,".awc") && strcmp(ext,".gxt") && strcmp(ext,".rpf");

	Insert(&m_Nodes[0].Child,destination,source,0,compressed);
	return ( true );
}


bool fiRpf7Builder::AddData(void *srcData,u32 dataSize,const char *destName,bool compressed)
{
	if (!dataSize)
		return false;

	Insert(&m_Nodes[0].Child,destName,(char*)srcData,dataSize,compressed);
	return ( true );
}


// Save the pack file 
void fiRpf7Builder::Save(fiStream* pack)
{
	InitializeWrite();
	Write(pack);
}


// Initialize the pack file after adding the files.
// We have the directory structure but have no idea what sizes the files will have etc.
void  fiRpf7Builder::InitializeWrite()
{
	// Allocate fiPackEntry buffer.
	++m_nEntryCount;
	m_Entries = new fiPackEntry[m_Nodes.GetCount()];

	m_Entries[0].m_NameOffset = 0;
	m_Entries[0].m_ConsumedSize = 0;
	m_Entries[0].m_FileOffset = fiPackEntry::FileOffset_IsDir;
	m_Entries[0].m_IsResource = 0;
	m_Entries[0].u.directory.m_DirectoryIndex = 1;
	m_Entries[0].u.directory.m_DirectoryCount = BuildTree( 1 );

	// Set the random number seed based on some stable quantities.
	m_Random.SetFullSeed((u64)m_Nodes.GetCount() * m_nNameHeapSize);

	// Round name heap size up for encryption, filling it with random crap.
	if (m_nNameHeapSize & 15) {
		RandomFill((u8*)m_pNameHeap + m_nNameHeapSize, 16 - (m_nNameHeapSize & 15));
		m_nNameHeapSize = (m_nNameHeapSize + 15) & ~15;
	}

	// Displayf( "%d nodes in tree, %d bytes in nameheap.", m_nEntryCount, m_nNameHeapSize );
}


void fiRpf7Builder::RandomFill(u8 *dest,u32 destSize)
{
	while (destSize--)
		*dest++ = (u8)m_Random.GetInt();
}


inline u32 TargetSwap(bool swap,u32 x) { return swap? sysEndian::Swap(x) : x; }

// Avoid datSwapper, it doesn't work on x64 right now
inline void Swapper(bool swap,u32 &x) { if (swap) x = sysEndian::Swap(x); }
inline void Swapper(bool swap,u64 &x) { if (swap) x = sysEndian::Swap(x); }


// Write the data out.
void fiRpf7Builder::Write( fiStream* output )
{
	AES aes;
	bool swap = sysGetByteSwap(g_sysPlatform);

	// Write the header; once the metadata offset is determined the header is
	// re-written at the end of this method (and made big-endian).
	fiPackHeader header;
	memset( &header, 0, sizeof(fiPackHeader) );
	header.m_Magic = 'RPF7';
	header.m_EntryCount = m_nEntryCount;
	header.m_NameHeapSize = m_nNameHeapSize;
	header.m_Encrypted = aes.GetKeyId();
	output->Write( &header.m_Magic, sizeof( fiPackHeader ) );

	// Write Entry Directory.  (not encrypted or byte swapped yet)
	output->Write( m_Entries, m_nEntryCount * sizeof( fiPackEntry ) );
	output->Write( m_pNameHeap, m_nNameHeapSize);

	WritePadding( output, 1 << fiPackEntry::FileOffset_Shift );
	// int lastPct = -1;

	fiPackEntry** original_order = new fiPackEntry*[m_nEntryCount];
	memset(original_order,0,sizeof(fiPackEntry*) * m_nEntryCount);

	for (u32 i=0; i<m_nEntryCount; ++i) 
	{
		fiPackEntry& pe = m_Entries[i];

		if (pe.IsDir()) {
			continue;
		}

		original_order[pe.u.file.m_UncompressedSize] = &pe;
	}

	for (u32 i=0; i<m_nEntryCount; ++i) 
	{
		if(original_order[i] == NULL)
			continue;

		//fiPackEntry& pe = m_Entries[i];
		fiPackEntry& pe = *(original_order[i]);

		/* int pct = (i * 100) / m_nEntryCount;
		if (pct != lastPct)
			rbTracef3("%d%% complete...\r",lastPct = pct); */

		if (pe.IsDir()) {
			// Displayf("%u. DIRECTORY [%s] %d %d",i,m_NameHeap + pe.m_NameOffset,pe.u.directory.m_DirectoryIndex,pe.u.directory.m_DirectoryCount);
			continue;
		}

		// Copy the file into its final position
		fiRpf7Entry &node = m_Nodes[pe.u.file.m_UncompressedSize];
		const char *srcName = node.SourceName;

		// Don't compress audio files, text files or nested rpf's.
		// Note - DO NOT add any more types here if they're human-readable.
		// TODO: Figure out why we cannot compress .gxt files, because files that are not
		// compressed are not encrypted in any way either.  Not an issue for awc (audio)
		// or rpf (which is already internally encrypted anyway)
		bool canCompress = node.Compressed;

		u64 offset64 = output->Tell64();
		u32 offset = (u32) offset64;
		pe.SetFileOffset(offset);
		if (pe.GetFileOffset() != offset || offset != offset64)
			Quitf(ERR_DEFAULT,"File not aligned or archive is too big, cannot represent offset 0x%" I64FMT "x",offset64);

		u8 *buffer;
		u32 size = node.DataSize, compSize = size;
		if (!size) {
			const fiDevice* device = fiDevice::GetDevice(srcName);
			fiHandle h = device->Open(srcName,true);
			size = u32(device->GetFileSize(srcName));
			compSize = size;

			// Read file into memory
			Assert(fiIsValidHandle(h));
			buffer = new u8[size];
			Assert(buffer);
			AssertVerify(device->Read(h, buffer, size) == (int) size);
			device->Close(h);
		}
		else
			buffer = (u8*) node.SourceName;

		datResourceFileHeader *hdr = (datResourceFileHeader*) buffer;
		if (TargetSwap(swap,hdr->Magic) != datResourceFileHeader::c_MAGIC)
		{
			u32 destSize = (size * 2) + 4096;
			u8 *dest = NULL;
			if (canCompress)
			{
				dest = new u8[destSize];
#if __WIN32
				if (g_sysPlatform == platform::XENON) 
				{
					XMEMCOMPRESSION_CONTEXT ctxt = NULL;
					XMEMCODEC_PARAMETERS_LZX params = XCOMPRESS_SETTINGS;
					if (FAILED(XMemCreateCompressionContext(XMEMCODEC_LZX, &params, XMEMCOMPRESS_STREAM, &ctxt)))
						Quitf(ERR_DEFAULT,"CreateCompressionContext failed");
					SIZE_T inSize = size;
					SIZE_T outSize = destSize;
					if (FAILED(XMemCompress(ctxt,dest,&outSize,buffer,inSize)))
						Quitf(ERR_DEFAULT,"XMemCompress failed.");
					XMemDestroyCompressionContext(ctxt);
					compSize = (u32) outSize;
				}
				else
#endif
				{
					z_stream c_stream;
					memset(&c_stream,0,sizeof(c_stream));
					if (deflateInit2(&c_stream,Z_BEST_COMPRESSION,Z_DEFLATED,-MAX_WBITS,MAX_MEM_LEVEL,Z_DEFAULT_STRATEGY) < 0)
						Quitf(ERR_DEFAULT,"Error in deflateInit");
					c_stream.next_in = buffer;
					c_stream.avail_in = size;
					c_stream.next_out = dest;
					c_stream.avail_out = destSize;
					if (deflate(&c_stream, Z_FINISH) != Z_STREAM_END)
						Quitf(ERR_DEFAULT,"Error in deflate");
					deflateEnd(&c_stream);
					compSize = (destSize - c_stream.avail_out);
				}
			}

			if (compSize < size)
			{
				Assert(compSize);
				pe.m_ConsumedSize = compSize;
				pe.u.file.m_UncompressedSize = size;
				if (pe.m_ConsumedSize != compSize)
					Quitf(ERR_DEFAULT,"Normal file %s is too big to mark as compressed!",srcName);

				// Note that the last part of the file may not be encrypted if it's not a multiple of 16.
				pe.u.file.m_Encrypted = 1;
#if RSG_CPU_X64
				if (g_sysPlatform == platform::WIN64PC || g_sysPlatform == platform::ORBIS || g_sysPlatform == platform::DURANGO)
				{
					unsigned int transformitSelector = 0;
					const char* entryName = m_pNameHeap + (pe.m_NameOffset);
					transformitSelector = atStringHash(entryName);
					transformitSelector += size;
					transformitSelector = transformitSelector % TFIT_NUM_KEYS;
					aes.Encrypt(header.m_Encrypted, transformitSelector, dest, compSize);
				}
				else
				{
				aes.Encrypt(dest,compSize);
				}
#else
				aes.Encrypt(dest,compSize);
#endif

				delete[] buffer;
				buffer = dest;
				size = compSize;
			}
			else
			{
				// TODO: Issue warnings on these files, or compress them anyway even though they're bigger?
				// Otherwise, small files run the risk of not being encrypted.
				pe.m_ConsumedSize = 0;
				pe.u.file.m_UncompressedSize = size;

				delete[] dest;
			}
			pe.m_IsResource = 0;
		}
		else	// Resource (always internally compressed)
		{
			bool bigResource = size >= fiPackEntry::MaxConsumedSize;

			// Big resources are only supported on PC (currently little-endian) targets
			if (bigResource && swap)
				Quitf(ERR_DEFAULT,"Cannot represent a resource this big (%u bytes) on console targets.",size);

			if (bigResource)
				pe.m_ConsumedSize = fiPackEntry::MaxConsumedSize;
			else
				pe.m_ConsumedSize = size;

			pe.m_IsResource = 1;
			// We have to swap the resource info on read because we unconditionally swap the entire entry on write.
			(u32&)pe.u.resource.m_Info.Virtual = TargetSwap(swap,(u32&)hdr->Info.Virtual);
			(u32&)pe.u.resource.m_Info.Physical = TargetSwap(swap,(u32&)hdr->Info.Physical);

			// We don't actually need the resource header here so scramble it.
			RandomFill(buffer,sizeof(datResourceFileHeader));

			// If it's a resource larger than 24M, hide its actual size in the first 16 bytes we just scrambled.
			if (bigResource) {
				buffer[2] = u8(size >> 24);
				buffer[5] = u8(size >> 16);
				buffer[14] = u8(size >> 8);
				buffer[7] = u8(size);
			}
		}

		// Displayf("%u. %s size %u isRsc %d",i,m_NameHeap + pe.m_NameOffset,pe.m_ConsumedSize,pe.m_IsResource);

		// Our Write routine accepts a signed integer so break the write up into two parts for that case.
		// Ideally we're not putting a file larger than 2G in another archive in the first place, but
		// it's not difficult to handle this properly.
		if (size > MAXINT) {
			if ((output->Write(buffer, MAXINT) != MAXINT) || output->Write(buffer + MAXINT, size - MAXINT) != int(size - MAXINT))
				Quitf(ERR_DEFAULT,"Error writing %u bytes to archive, is your disk full?",size);
		}
		else if (output->Write(buffer, size) != (int)size)
			Quitf(ERR_DEFAULT,"Error writing %u bytes to archive, is your disk full?",size);

		WritePadding( output, 1 << fiPackEntry::FileOffset_Shift );

		delete[] buffer;
	}

	delete[] original_order;
 
	// TODO - Need better padding here for disk cache?  But we'll have a lot more archives than in rdr2.
	WritePadding(output, 2048);

	// For tool use, let them know data is in XCompress format instead of zlib format
	if (g_sysPlatform == platform::XENON)
		header.m_NameHeapSize |= 0x80000000;
	header.m_NameHeapSize |= (m_nNameHeapShift) << 28;

	// Write header.  Magic number tells you which endianness to use.
	// TODO: Make header 32 bytes, not 16.  Put magic and encryption key id in first 16 bytes,
	// and the entry count and name heap size (both encrypted) in the next 16 bytes.
	Swapper(swap,header.m_Magic);
	Swapper(swap,header.m_EntryCount);
	Swapper(swap,header.m_NameHeapSize);
	Swapper(swap,header.m_Encrypted);
	output->Seek( 0 );
	Assertf( 0 == output->Tell(), "Invalid file position." );
	output->Write( &header, sizeof( fiPackHeader ) );

	for (u32 i=0; i<m_nEntryCount; i++) {
		Swapper(swap,*(u64*)&m_Entries[i]);
		Swapper(swap,m_Entries[i].u.file.m_UncompressedSize);
		Swapper(swap,m_Entries[i].u.file.m_Encrypted);
	}

	// Write the entry directory
	Assertf( sizeof( fiPackHeader ) == output->Tell(), "Invalid file position." );

#if RSG_CPU_X64
	if (g_sysPlatform == platform::WIN64PC || g_sysPlatform == platform::ORBIS || g_sysPlatform == platform::DURANGO)
	{
		unsigned int transformitSelector = 0;
		transformitSelector = atStringHash(ASSET.FileName(output->GetName()));
		transformitSelector += output->Size();
		transformitSelector = transformitSelector % TFIT_NUM_KEYS;
		aes.Encrypt(header.m_Encrypted, transformitSelector,  m_Entries, m_nEntryCount * sizeof(fiPackEntry) );
		aes.Encrypt(header.m_Encrypted, transformitSelector,  m_pNameHeap, m_nNameHeapSize );
	}
	else
	{
	aes.Encrypt( m_Entries, m_nEntryCount * sizeof(fiPackEntry) );
	aes.Encrypt( m_pNameHeap, m_nNameHeapSize );
	}
#else
	aes.Encrypt( m_Entries, m_nEntryCount * sizeof(fiPackEntry) );
	aes.Encrypt( m_pNameHeap, m_nNameHeapSize );
#endif


	output->Write( m_Entries, m_nEntryCount * sizeof(fiPackEntry) );
	output->Write( m_pNameHeap, m_nNameHeapSize );
}


void fiRpf7Builder::WritePadding( ::rage::fiStream* fp, u32 byteBoundary )
{
	u64 currPos = fp->Tell( );
	if (currPos & (byteBoundary-1))
	{
		u32 fillCount = u32(((currPos + byteBoundary - 1) & ~(byteBoundary-1)) - currPos);
		u8 *garbage = Alloca(u8,fillCount);
		RandomFill( garbage, fillCount );
		fp->Write( garbage, fillCount );
	}
}


}	// namespace rage

#endif		// __UNITYBUILD
