// 
// file/packfile.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "zipfile.h"

#include "file/asset.h"
#include "system/criticalsection.h"
#include "system/magicnumber.h"
#include "system/memory.h"
#include "system/param.h"
#include "string/stringhash.h"
#include "atl/staticpool.h"

#include "zlib/zlib.h"

#if __WIN32PC
#include "system/xtl.h"
#endif

#include <stdlib.h>

namespace rage {

struct MsDosTime {
	u16 DECLARE_BITFIELD_3(second,5,minute,6,hour,5);
};
struct MsDosDate {
	u16 DECLARE_BITFIELD_3(day,5,month,4,yearsFrom1980,7); // day is 1-31, month is 1-12
};

static inline bool ZipIsLeapYear(int year) {
	// 2000 is a leap year; 2100 is not.
	// 2004 and 2008 are leap years.
	return ((year % 4) == 0 && (year % 100) != 0) || (year % 400 == 0);
}

/*
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

void main()
{
	SYSTEMTIME _1980 = { 1980, 1, 0, 1, 0, 0, 0, 0 };
	unsigned __int64 ft;
	SystemTimeToFileTime(&_1980, (FILETIME*)&ft);
	char buffer[100];
	_ui64toa(ft,buffer,10);
	printf("filetime of 1980 %s\n",buffer);
}
*/

static u64 ZipTimeToFileTime(MsDosTime dosTime,MsDosDate dosDate) {
	static u8 dpm[13] = { 
		0,
		31,  //; January
		28,  //; February
		31,  //; March
		30,  //; April
		31,  //; May
		30,  //; June
		31,  //; July
		31,  //; August
		30,  //; September
		31,  //; October
		30,  //; November
		31   //; December
	};

	int year = dosDate.yearsFrom1980 + 1980;
	// Determine the contribution of all years previous to now.
	u32 result = 0;
	for (int now=1980; now<year; now++)
		result += (365 + ZipIsLeapYear(now)) * 24 * 3600;
	// Determine the contribution of all months previous to now.
	dpm[2] = u8(28 + ZipIsLeapYear(year));
	for (int now=1; now<dosDate.month; now++)
		result += dpm[now] * 24 * 3600;
	// Determine contribution for days of months previous to now
	result += (dosDate.day - 1) * 24 * 3600;
	// Determine contribution from the time of day.
	result += dosTime.hour * 3600 + dosTime.minute * 60 + dosTime.second * 2;

	const u64 FILETIME_1980 = 119600064000000000ULL;
	const u64 CENTINANOSECONDS_PER_SECOND = 10000000ULL;
	return (u64) (result * CENTINANOSECONDS_PER_SECOND) + FILETIME_1980;
};


struct zipDirEntry
{
	zipDirEntry() : Hash(0), Next(NULL), Child(NULL) { }
	~zipDirEntry() { 
		delete Child; 
		// avoid massive recursion on large datasets.
		for (zipDirEntry *i = Next; i;) {
			zipDirEntry *j = i->Next;
			i->Next = NULL;
			delete i;
			i = j;
		}
	}

	zipDirEntry *Next, *Child;
	u32 Hash;
	u32 Offset;					// Only for files (zero if it's a directory)
	u32 CompSize:31, Compressed:1, UncompSize;	// Only for files
	u64 FileTime;
	char Name[0];

	bool IsCompressed() const { return Compressed; }
	bool IsDir() const { return Offset == 0; }

	static void Dump(zipDirEntry *entry,int intent);
	static void Insert(zipDirEntry **nodeRef,const char *name,u32 offset,int compSize,int uncompSize,bool compressed,u64 filetime);
};


void zipDirEntry::Insert(zipDirEntry **nodeRef,const char * name,u32 offset,int compSize,int uncompSize,bool compressed,u64 filetime) {
	while (*name) {
		// Parse out normalized path element
		char element[256];
		int len = 0;
		while (*name && *name != '/' && *name != '\\') {
			char ch = *name++;
			if (ch >= 'A' && ch <= 'Z')
				ch += 'a'-'A';
			if (len < (int)sizeof(element)-1)
				element[len++] = ch;
			else
				Quitf(ERR_FIL_ZIP_1,"zipDirEntry::Insert - name part too long!");
		}
		if (*name)
			++name;
		element[len] = 0;
		u32 elementHash = atStringHash(element);
		while (*nodeRef && elementHash != (*nodeRef)->Hash)
			nodeRef = &(*nodeRef)->Next;
		if (!*nodeRef) {
			*nodeRef = rage_placement_new(new char[sizeof (zipDirEntry) + strlen(element) + 1]) zipDirEntry;
			strcpy((*nodeRef)->Name, element);
			(*nodeRef)->Hash = elementHash;
			(*nodeRef)->FileTime = (*name)? 0 : filetime;	// if directory, zero out the filetime since it doesn't mean anything.  Prevents random variation.
			(*nodeRef)->Offset = (*name)? 0 : offset;		// If there's more to name, this is a directory, so set offset to 0
			(*nodeRef)->CompSize = compSize;
			(*nodeRef)->UncompSize = uncompSize;
			(*nodeRef)->Compressed = compressed;
		}
		nodeRef = &(*nodeRef)->Child;
	}
}

void zipDirEntry::Dump(zipDirEntry *i,int indent) 
{
	while (i) {
		Displayf("%s%s%s","                                "+32-indent,i->Name,!i->Offset?"/:":"");
		if (i->Child)
			Dump(i->Child,indent+2);
		i = i->Next;
	}
}


fiZipfile::fiZipfile() 
	: m_Handle(fiHandleInvalid)
	, m_Root(NULL)
	, m_Device(NULL)
{
	m_Name[0] = 0;
}


fiZipfile::~fiZipfile() {
	if (m_Device && fiIsValidHandle(m_Handle))
		m_Device->CloseBulk(m_Handle);
	m_Device = NULL;
	m_Handle = fiHandleInvalid;

	delete m_Root;
}

bool fiZipfile::Init(const char *filename) {
	const u32 ZIP_LOCAL_MAGIC = 0x04034b50;

	char fullname[RAGE_MAX_PATH];
	ASSET.FullReadPath(fullname,sizeof(fullname),filename,""); // To allow other extensions than .rpf
	safecpy(m_Name,ASSET.FileName(fullname));

	m_Device = fiDevice::GetDevice(fullname,true);
	if(!Verifyf(m_Device,"Cannot find device for filename '%s'",fullname))
	{
		return false;
	}

	m_RelativeOffset = int(ASSET.FileName(fullname) - fullname);

	fiStream *S = fiStream::Open(fullname,true);

	if (!S) {
		Warningf("%s: File not found.",fullname);
		return false;
	}

	m_ArchiveSize = S->Size64();
	zipDirEntry *root = NULL;

	// unsigned heapTop = 0;
	sysMemStartTemp();
	u32 magic;
	while (S->ReadInt(&magic,1) && magic == ZIP_LOCAL_MAGIC) {
		/*
		version made by                 2 bytes
		version needed to extract       2 bytes
		general purpose bit flag        2 bytes
		compression method              2 bytes
		last mod file time              2 bytes
		last mod file date              2 bytes
		crc-32                          4 bytes
		compressed size                 4 bytes
		uncompressed size               4 bytes
		filename length                 2 bytes
		extra field length              2 bytes
		filename (variable size)
		extra field (variable size)
		*/
		char name[256], extra[256];
		u16 minVersion, flags, filenameLen, extraLen, method;
		u32 crc32, compLen, uncompLen;
		S->ReadShort(&minVersion,1);
		S->ReadShort(&flags,1);
		S->ReadShort(&method,1);
		if (method != 0 && method != Z_DEFLATED) {
			Warningf("%s: Compression method besides store or deflate encountered.",filename);
			S->Close();
			return false;
		}
		MsDosTime time;
		MsDosDate date;
		S->ReadShort((u16*)&time,1);
		S->ReadShort((u16*)&date,1);
		u64 rpftime = ZipTimeToFileTime(time,date);
		S->ReadInt(&crc32,1);
		S->ReadInt(&compLen,1);
		S->ReadInt(&uncompLen,1);
		S->ReadShort(&filenameLen,1);
		S->ReadShort(&extraLen,1);
		Assert(filenameLen < sizeof(name));
		Assert(extraLen < sizeof(extra));
		S->Read(name,filenameLen); 
		name[filenameLen++] = 0;
		S->Read(extra,extraLen); // if (extraLen) Displayf("extraLen=%d",extraLen);

		//char debug[64];
		//rpfTimeToString(debug,sizeof(debug),rpftime);
		//Displayf("%s | %08x %08x %s",name,compLen,uncompLen,method?"compressed":"stored");

		int payload = (int)S->Tell64();
		// Displayf("zipfile '%s' file '%s' offset %d(%x)",filename,name,payload,payload);
		zipDirEntry::Insert(&root,name,payload,compLen,uncompLen,method!=0,rpftime);

		// Skip to next file in archive.
		if (flags & 0x08)	// http://en.wikipedia.org/wiki/ZIP_(file_format)
			payload += 16;	// skip extra 16-byte structure containing crc-32 and sizes after compressed data
		S->Seek64(payload + compLen);
	}

	S->Close();

	// zipDirEntry::Dump(m_Root, 2);
	m_Root = rage_placement_new(rage_new char[sizeof(zipDirEntry)+2]) zipDirEntry;
	strcpy(m_Root->Name,"/");
	m_Root->Offset = m_Root->UncompSize = m_Root->CompSize = m_Root->Hash = 0;
	m_Root->Child = root;
	m_Root->Next = NULL;
	sysMemEndTemp();

	m_Handle = m_Device->OpenBulk(fullname,m_ArchiveBias);
	if (!fiIsValidHandle(m_Handle))
		return false;
	m_Root->FileTime = m_Device->GetFileTime(fullname);
	if (m_ArchiveSize < 16 /* 2048 - if you ENABLE_NO_BUFFERING in file/device_win32.cpp */) {
		Errorf("'%s' is not a valid rpf or zip file (way too small)",fullname);
		m_Device->CloseBulk(m_Handle);
		m_Handle = fiHandleInvalid;
		return false;
	}

	return true;
}


bool fiZipfile::MountAs(const char *path) {
	if( fiDevice::Mount( path, *this, true ) )
	{
		m_RelativeOffset = StringLength(path); 
		return true;
	}
	
	return false;
}

fiHandle fiZipfile::Create(const char * /*filename*/) const {
	// Never legal to create a file inside a zipfile
	return fiHandleInvalid;
}


fiHandle fiZipfile::Open(const char * filename,bool readOnly) const {
	return OpenInternal(filename + m_RelativeOffset,readOnly);
}


static sysCriticalSectionToken s_ZipfileOpen;

struct fiZipfileState {
	static const u32 BufferSize = 8192;
	bool IsCompressed() const;
	char						*m_Buffer;
	char						m_DebugName[32];
	zipDirEntry					*m_Entry;			// Pointer to the file directory entry
	u32							m_Offset;			// Current offset within the (virtual, uncompressed) file
	u32							m_CompOffset;		// Current offset within the compressed file
	z_stream					m_zStream;			// Stream object used by Zlib

	fiZipfileState() : m_Entry(NULL) {}

	void Reset() {
		m_Entry = 0;
		m_Offset = 0;
		m_CompOffset = 0;
		memset(&m_zStream, 0, sizeof(z_stream)); // should we be nulling this?
	}
};

bool fiZipfileState::IsCompressed() const { 
	return m_Entry->IsCompressed();
}

atStaticPool<fiZipfileState,16> s_ZipStates;

#if __LTCG
#pragma warning(push)
#pragma warning(disable:4702)	// Unreachable code
#endif // __LTCG

fiHandle fiZipfile::OpenInternal(const char *lookupName,bool readOnly) const {
	// Never legal to modify a file inside a zipfile
	if (!readOnly)
		return fiHandleInvalid;

	zipDirEntry *e = FindEntry(lookupName);
	if (e && !e->IsDir()) {	// Cannot open a directory this way
		SYS_CS_SYNC(s_ZipfileOpen);

		if (s_ZipStates.IsFull()) {
			for (int i=0; i<s_ZipStates.GetSize(); i++)
				Displayf("%d. %s",i,s_ZipStates.GetElement(i).m_DebugName);
			Quitf(ERR_FIL_ZIP_2,"Out of zipfile descriptors opening %s, too many files in zips open at once (see tty for open files)",lookupName);
		}

		fiZipfileState *s = s_ZipStates.New();

		s->Reset();
		if (e->IsCompressed())
		{
			RAGE_TRACK(PackfileState);
			sysMemStartTemp();
			s->m_Buffer = rage_new char[fiZipfileState::BufferSize];
			if (inflateInit2(&s->m_zStream,-MAX_WBITS) != Z_OK) {
				Errorf("fiZipfile::Open(%s) - inflateInit failed.",lookupName);
				sysMemEndTemp();
				return fiHandleInvalid;
			}
			sysMemEndTemp();
		}
		s->m_Entry = e;
		s->m_Offset = 0;
		safecpy(s->m_DebugName,ASSET.FileName(lookupName));

		return (fiHandle)s;
	}
	else {
		// Displayf("Open(%s) failed",filename + m_RelativeOffset);
		return fiHandleInvalid;
	}
}
#if __LTCG
#pragma warning(pop)
#endif // __LTCG


void fiZipfile::FillData(fiFindData &outData,zipDirEntry *e) const {
	safecpy(outData.m_Name,(const char*)e->Name,sizeof(outData.m_Name));
	bool isDir = e->IsDir();
	outData.m_Size = isDir?0:e->UncompSize;
	outData.m_LastWriteTime		= e->FileTime;
	outData.m_Attributes		= FILE_ATTRIBUTE_READONLY | (isDir? FILE_ATTRIBUTE_DIRECTORY : 0);
}


fiHandle fiZipfile::FindFileBegin(const char *directoryName,fiFindData &outData) const {
	zipDirEntry *e = FindEntry(directoryName + m_RelativeOffset);
	if (e && e->IsDir() && e->Child) {	// must be a non-empty directory
		SYS_CS_SYNC(s_ZipfileOpen);

		if (s_ZipStates.IsFull())
			return fiHandleInvalid;

		fiZipfileState *s = s_ZipStates.New();
		s->m_Entry = e->Child;
		FillData(outData,e->Child);
		return (fiHandle)(size_t)s;
	}
	else
		return fiHandleInvalid;
}


bool fiZipfile::FindFileNext(fiHandle _handle,fiFindData &outData) const {
	fiZipfileState &s = *(fiZipfileState*)_handle;
	if (s.m_Entry->Next) {
		s.m_Entry = s.m_Entry->Next;
		FillData(outData,s.m_Entry);
		return true;
	}
	else
		return false;
}


int fiZipfile::FindFileEnd(fiHandle _handle) const {
	SYS_CS_SYNC(s_ZipfileOpen);
	s_ZipStates.Delete((fiZipfileState*)_handle);
	return 0;
}

zipDirEntry* fiZipfile::FindEntry(const char *name) const {
	zipDirEntry *e = m_Root;
	if (!e)
		return NULL;

	// remove any leading slash since the root dir is the starting point
	// but it may not be present in the name.
	if (*name == '/' || *name == '\\')
		++name;
	// return the root dir, really only for FindFirst and siblings
	if (!*name)
		return e;
	else
		e = e->Child;

	for(;;) {
		// Get min and max entries to search
		char currentPart[RAGE_MAX_PATH], *cp = currentPart;
		while (*name && *name!='/' && *name!='\\')
			*cp++ = *name++;
		while (*name == '/' || *name == '\\')	// skip slash or backslash
			++name;
		*cp = 0;
		u32 currentHash = atStringHash(currentPart);
		while (e && e->Hash != currentHash)
			e = e->Next;
		if (*name && e && e->IsDir())
			e = e->Child;
		else
			return *name? NULL : e;
	}
}


int fiZipfile::Close(fiHandle _handle) const {
	fiZipfileState *s = (fiZipfileState*)_handle;
	SYS_CS_SYNC(s_ZipfileOpen);
	if (s->IsCompressed()) {
		sysMemStartTemp();
		inflateEnd(&s->m_zStream);
		delete[] s->m_Buffer;
		s->m_Buffer = NULL;
		sysMemEndTemp();
	}
	s_ZipStates.Delete(s);
	return 0;
}


int fiZipfile::Read(fiHandle _handle,void *outBuffer,int bufferSize) const {
	fiZipfileState &s = *(fiZipfileState*)_handle;
	return ReadInternal(s,outBuffer,bufferSize);
}

int fiZipfile::ReadInternalBulk(const fiZipfileState& s,u32 relativeOffset,void *outBuffer,int toRead) const
{
	// Eliminate zero-byte requests before getting any further.
	if (toRead == 0)
		return 0;
	// If we got here, assume it's an optical read and therefore worth warning the cache thread about.
	return m_Device->ReadBulk(m_Handle, m_ArchiveBias + ((u64)s.m_Entry->Offset) + relativeOffset,outBuffer,toRead);
}

int	fiZipfile::ReadInternal(fiZipfileState& s,void *outBuffer,int bufferSize) const
{
	if (s.IsCompressed()) {
		s.m_zStream.next_out = (Bytef*) outBuffer;
		s.m_zStream.avail_out = bufferSize;

		while (s.m_zStream.avail_out) {
			// queue up some source data if we need some
			if (s.m_zStream.avail_in == 0) {
				u32 toRead = fiZipfileState::BufferSize;
				if (toRead > s.m_Entry->CompSize - s.m_CompOffset)
					toRead = s.m_Entry->CompSize - s.m_CompOffset;
				int thisRead = ReadInternalBulk(s,s.m_CompOffset,s.m_Buffer,toRead);
				s.m_CompOffset += (s.m_zStream.avail_in = thisRead);
				s.m_zStream.next_in = (Bytef*) s.m_Buffer;
			}

			// set destination for inflation.
			sysMemStartTemp();
			int err = inflate(&s.m_zStream,Z_SYNC_FLUSH);
			sysMemEndTemp();

			if (err == Z_STREAM_END) 
			{	
				// this is fine, but won't necessarily happen because we don't have a zlib header.
				// Displayf("zipHandle::Read - Normal EOF reached.");
				//inflateDeallocWindow(&s.m_zStream);
				break;
			}

			if (err < 0)
			{
				switch (err)
				{
					case Z_ERRNO:
						Displayf("Disc error: Error in the file system");
						break;

					case Z_STREAM_ERROR:
						Displayf("Disc error: Streaming error");
						break;

					case Z_DATA_ERROR:
						Displayf("Disc error: Data error: %s", s.m_zStream.msg);
						break;

					case Z_MEM_ERROR:
						Displayf("Disc error: Memory error");
						break;

					case Z_BUF_ERROR:
						Displayf("Disc error: Buffer error");
						break;

					case Z_VERSION_ERROR:
						Displayf("Disc error: Version error");
						break;

					default:
						Displayf("Disc error: Unknown error!");
						break;
				}
				Quitf(ERR_FIL_ZIP_3,"Fatal disc error");
			}
		}

		// Figure out how much we actually read by subtracting out what was left unfilled.
		bufferSize -= s.m_zStream.avail_out;
		s.m_Offset += bufferSize;
		return bufferSize;
	}
	else {
		u32 amtToRead = bufferSize;
		if (amtToRead > s.m_Entry->UncompSize - s.m_Offset)
			amtToRead = s.m_Entry->UncompSize - s.m_Offset;
		amtToRead = ReadInternalBulk(s,s.m_Offset,outBuffer,amtToRead);
		s.m_Offset += amtToRead;
		return amtToRead;
	}
}

int fiZipfile::Size(fiHandle _handle) const {
	fiZipfileState &s = *(fiZipfileState*)_handle;
	return s.m_Entry->UncompSize;
}


u64 fiZipfile::Size64(fiHandle _handle) const {
	fiZipfileState &s = *(fiZipfileState*)_handle;
	return s.m_Entry->UncompSize;
}


int fiZipfile::Write(fiHandle /*handle*/,const void * /*buffer*/,int /*bufferSize*/) const {
	return -1;
}


int fiZipfile::Seek(fiHandle _handle,int offset,fiSeekWhence whence) const {
	return (int)Seek64(_handle,offset,whence);
}

u64 fiZipfile::Seek64(fiHandle _handle,s64 offset,fiSeekWhence whence) const {
	fiZipfileState &s = *(fiZipfileState*)_handle;

	if (s.IsCompressed()) {
		u32 destPos;
		switch (whence) {
		case seekSet: destPos = (u32) offset; break;
		case seekEnd: destPos = (u32)(s.m_Entry->UncompSize + offset); break;
		case seekCur: destPos = (u32)(s.m_Offset + offset); break;
		default:
			return (u64)(s64)-1;
		}
		if (destPos > s.m_Entry->UncompSize)
			destPos  = s.m_Entry->UncompSize;

		if (destPos < s.m_Offset) {
			s.m_Offset = s.m_CompOffset = 0;
			sysMemStartTemp();
			inflateReset(&s.m_zStream);
			s.m_zStream.avail_in = 0;
			sysMemEndTemp();
		}

		while (destPos > s.m_Offset) {
			char buffer[4096];
			unsigned toRead = destPos - s.m_Offset;
			if (toRead > sizeof(buffer))
				toRead = sizeof(buffer);
			if (ReadInternal(s,buffer,toRead) == -1)
				return (u64)(s64)-1;
		}

		return destPos;
	}
	else {
		u32 newPos =
			(whence == seekEnd)? u32(s.m_Entry->UncompSize + offset) :
			(whence == seekCur)? u32(s.m_Offset + offset) :
			u32(offset);
		if (newPos > s.m_Entry->UncompSize)
			newPos = s.m_Entry->UncompSize;
		s.m_Offset = (u32) newPos;		// TODO: Support >4G files within archives?
		return newPos;
	}
}


u64 fiZipfile::GetFileTime(const char * filename) const {
	zipDirEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e) {
#if __WIN32PC
		u64 localTime;
		FileTimeToLocalFileTime((FILETIME*)&e->FileTime,(FILETIME*)&localTime);
		return localTime;
#else
		return e->FileTime;
#endif
	}
	else
		return 0;
}

bool fiZipfile::SetFileTime(const char * /*filename*/,u64 /*timestamp*/) const {
	return false;
}


u32 fiZipfile::GetAttributes(const char * filename) const {
	zipDirEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return FILE_ATTRIBUTE_READONLY;
	else
		return FILE_ATTRIBUTE_INVALID;
}


u64 fiZipfile::GetFileSize(const char * filename) const {
	zipDirEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return e->UncompSize;
	else
		return 0;
}


fiHandle fiZipfile::OpenBulk(const char *filename,u64 &outBias) const {
	zipDirEntry *e = FindEntry(filename + m_RelativeOffset);
	if (e) {
		if (!e->IsCompressed()) {
			outBias = (u64) e->Offset;
			return m_Handle;
		}
		else
			Warningf("fiZipfile::OpenBulk - '%s' is externally compressed, cannot open with OpenBulk.",filename);
	}
	return fiHandleInvalid;
}


int fiZipfile::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const {
	AssertMsg(handle == m_Handle,"You are probably trying to load a zip with STREAM_FROM_ARCHIVES_ONLY enabled.");
	offset += m_ArchiveBias;

	u64 totalSize = m_ArchiveSize;
	if (offset + bufferSize > totalSize)
		bufferSize = (int)(totalSize - offset);
	return m_Device->ReadBulk(handle,offset,outBuffer,bufferSize);
}

int fiZipfile::CloseBulk(fiHandle /*handle*/) const {
	// Nothing to do here, no real handle allocated.
	return 0;
}

const char *fiZipfile::GetDebugName() const {
	return m_Name;
}

}	// namespace rage

