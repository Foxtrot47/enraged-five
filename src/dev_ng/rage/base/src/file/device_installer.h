// 
// file/device_installer.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_DEVICE_INSTALLER_H
#define FILE_DEVICE_INSTALLER_H

#include "data/callback.h"
#include "file/device.h"
#include "file/packfile.h"
#include "parser/macros.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/platform.h"
#include "system/xtl.h"

#if __PS3
#include <sysutil/sysutil_gamecontent.h>

#if __DEV
#include "atl/hashstring.h"
#endif
#endif

#if __XENON
#define INVALID_DEVICE 0xFFFF
#endif

namespace rage {

#define INSTALL_SLICE_SIZE (1024 * 1024)
#define INSTALL_SLICE_NUM 3

class fiPackfile;
class sysTimer;

// the order of these enums makes it easier to combine several results into an overall one
// by taking the smallest value. this will indicate what's left to wait for or the "worst" result
enum eInstallDeviceStatus
{
	IDS_FAILED = 0,
	IDS_FAILED_WRONG_DISC,
	IDS_INSTALLING,
	IDS_IDLE,
	IDS_INSTALLED
};

struct sInstallProgress
{
	float fileTransferredSize; // in MB
	float fileSize;
	float totalTransferredSize;
	float totalSize;
	const char* currentFile;

#if __BANK
    float readThroughput;
    float writeThroughput;
    float readWaited;
    float writeWaited;
	float remoteWaited;

    float totalTime;
#endif
};

//
// name:		fiDeviceRelative
// description:	Specialised version of device that can install whole rpfs to a harddrive when explicitly asked.
/* Notes:
	The device should be used when running off a disc. It will initially point at partition rpfs on the disc.
	When told to, it will create partitions on the harddrive and install these rpfs to their respective partitions,
	at which point the mount will point to the rpfs on the HDD.
*/
class fiDeviceInstaller : public fiDevice
{
public:
	struct InitData {
		ConstString m_sourceFile;
		ConstString m_partitionName;
		ConstString m_partitionDesc;
		ConstString m_mount;
		ConstString m_platform;
		bool m_cached;
		bool m_readOnly;

		bool IsThisPlatform() const
		{
			// No string = all platforms.
			if (!m_platform || !m_platform[0])
			{
				return true;
			}

			return stricmp(m_platform.c_str(), RSG_PLATFORM_ID) == 0;
		}

		PAR_SIMPLE_PARSABLE;
	};
	struct InitDataList {
		atArray<InitData> m_initDatas;
		PAR_SIMPLE_PARSABLE;
	};


	fiDeviceInstaller();
	virtual ~fiDeviceInstaller(); 

	bool Init(const InitData& data, const char* overridePath, bool remote = false);
	
	bool MountAs(const char *path, bool mountDevice);
	eInstallDeviceStatus GetInstallStatus() const { return m_status; }
	const char* GetPartitionName() const { return m_partitionName; }

	const char *FixName(char *dest,int destSize,const char *src) const;

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64&bias) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	virtual bool Delete(const char *filename) const;
	virtual bool Rename(const char *from,const char *to) const;
	virtual bool MakeDirectory(const char *filename) const;
	virtual bool UnmakeDirectory(const char *filename) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;

	virtual fiHandle FindFileBegin(const char *filename,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;
	virtual bool SetEndOfFile(fiHandle handle) const;

	virtual u32 GetAttributes(const char *filename) const;
	virtual bool SetAttributes(const char *filename,u32 attributes) const;
	virtual u32 GetPhysicalSortKey(const char* filename) const;
	virtual int GetResourceInfo(const char* name,datResourceInfo &info) const;
	virtual u64 GetBulkOffset(const char* name) const;
	virtual bool IsRpf() const { return m_partitionDevice != NULL; }
	virtual const fiDevice* GetRpfDevice() const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return HARDDRIVE; }

#if __XENON
	bool IsTargetDeviceValid() const { return m_device != INVALID_DEVICE; }
	void StorePackageTimestamp();
	static bool IsPartitionPath(const char* filename);
    static void RefreshDeviceIds();
	static void FinishInstallGond();
	static void Reboot(u32 id);
	static bool HasRebooted(u32 id);
#else
	static bool HasRebooted(u32) { return false; }
#endif

	static void InstallerThread(void* ptr);
	static void WriteInstallFile(void* ptr);
	static bool ReadInstallFile(fiDeviceInstaller* partition, sysTimer& progressTimer);

	static void StartInstall(XENON_ONLY(XCONTENTDEVICEID device));
	static void FinishInstall();

	static void PauseDiscRead() { sm_pauseRead = true; }
	static void ResumeDiscRead() { sm_pauseRead = false; }

	static const sInstallProgress& GetInstallProgress() { return sm_installProgress; }

	static void RegisterFileDoneCallback(datCallback callback) { fileDoneCallback = callback; }

	static const char* GetHddBootPath() { return sm_hddPath; }
	static void SetIsBootedFromHdd(bool val, const char *hddPath);
	static bool GetIsBootedFromHdd() { return sm_IsBootedFromHdd; }

	static void SetKeepaliveCallback(datCallback callback, u32 interval) { sm_keepAliveCallback = callback; sm_keepAliveCallbackInterval = interval; }

    static void SetAsync(bool val) { sm_asyncInstall = val; }

#if __PS3
	static bool IsUsingCache() { return sm_usingCache; }
#endif // __PS3

	u64	GetPackFileSize() { return m_partitionDevice->GetPackfileSize(); }

	bool HasFailed() const { return m_status == IDS_FAILED || m_status == IDS_FAILED_WRONG_DISC; }
	bool IsCached() const { return m_IsCached; }

	bool IsContentAvailable();

protected:
	virtual bool InitInstall(const char* origSourceFile, bool remote);
#if __XENON
	void FoundContentPackage(s32 contentIndex, const fiDevice& localDevice, bool remote);
#endif

	bool InitPartitionDevice(const char* archiveFile, bool setTimeStamp);
	void FlagInstalledRpfs() const;

	void GetInstallName(char* dst, s32 dstSize, const char* src);
	bool DoTimestampsMatch(const char* oddFile, const char* hddFile);
	bool DoFileSizesMatch(const char* oddFile, const char* hddFile);
	void ScheduleFile(const char* filename);

	const char* StripPath(const char* fullpath);

	bool CopyContentInfoFile(const char* filename, const char* contentInfoPath);

	static void InstallProgress(u64 fileSize, u64 fileTransferredSize, u64 totalTransferredSize);

private:
	class InstallSliceQueue
	{
	public:
		InstallSliceQueue()
		{
			emptySema = sysIpcCreateSema(0);
			fullSema = sysIpcCreateSema(INSTALL_SLICE_NUM);
			put = 0;
			get = 0;
		}

		~InstallSliceQueue()
		{
			sysIpcDeleteSema(emptySema);
			sysIpcDeleteSema(fullSema);
		}

		void Put(void* slice)
		{
			sysIpcWaitSema(fullSema);
			sysCriticalSection lock(token);
			slices[put] = slice;
			put = ++put % INSTALL_SLICE_NUM;
			sysIpcSignalSema(emptySema);
		}

		void* Get()
		{
			sysIpcWaitSema(emptySema);
			sysCriticalSection lock(token);
			void* slice = slices[get];
			get = ++get % INSTALL_SLICE_NUM;
			sysIpcSignalSema(fullSema);
			return slice;
		}

	private:
		sysCriticalSectionToken token;
		sysIpcSema emptySema;
		sysIpcSema fullSema;
		void* slices[INSTALL_SLICE_NUM];
		u8 put;
		u8 get;
	};

	class InstallBuffer
	{
	public:
		static void Init()
		{
			for (s32 i = 0; i < INSTALL_SLICE_NUM; ++i)
			{
				buffers[i] = rage_new InstallBuffer();
				readQueue.Put(buffers[i]);
			}
		}

		static void Shutdown()
		{
			for (s32 i = 0; i < INSTALL_SLICE_NUM; ++i)
			{
				delete buffers[i];
			}
		}

		static InstallBuffer* GetReadBuffer() { return (InstallBuffer*)readQueue.Get(); }
		static InstallBuffer* GetWriteBuffer() { return (InstallBuffer*)writeQueue.Get(); }
		static void PutReadBuffer(InstallBuffer* buffer) { readQueue.Put(buffer); }
		static void PutWriteBuffer(InstallBuffer * buffer) { writeQueue.Put(buffer); }
		
		static InstallBuffer* buffers[INSTALL_SLICE_NUM];
		static InstallSliceQueue readQueue;
		static InstallSliceQueue writeQueue;

		void Reset()
		{
			timeStamp = 0;
			fileSize = 0;
			sliceSize = 0;
		}

		u64 timeStamp;
		u64 fileSize;
		u8* buffer;
		s32 sliceSize;

	private:
		InstallBuffer()
		{
			Reset();
			buffer = rage_new u8[INSTALL_SLICE_SIZE];
		}

		~InstallBuffer()
		{
			delete[] buffer;
			buffer = NULL;
		}
	};

	struct HddFile
	{
		u64 fileSize;
		const fiDeviceInstaller* partition;
		const char* fileName;
		bool success;
		bool blocking;
	};

	struct InstallFile
	{
		char filename[128];
		u64 filesize;
	};

	fiPackfile* m_partitionDevice;

	char m_sourceFile[RAGE_MAX_PATH];
	char m_partitionName[64];
	char m_mountPath[32];
	char m_partitionDesc[32];
	char m_prefix[64];


	eInstallDeviceStatus m_status;
	s32 m_filenameOffset;
	bool m_IsReadOnly;
	bool m_IsInstalling;
	bool m_IsHddMounted;
	bool m_IsCached;

#if __XENON
	XCONTENTDEVICEID m_device;
	u64 m_timestamp; // no timestamps for xlast packages so we store it ourselves
	char m_contentFileName[XCONTENT_MAX_FILENAME_LENGTH + 1]; // need to do this better, store the content data and enumerate only once, but i don't dare change that now
#endif

	atArray<InstallFile> looseFiles;

	static sysIpcThreadId sm_Thread;
	static sysIpcThreadId sm_WriteThread;
	static atArray<fiDeviceInstaller*> sm_installPartitions;
	static bool sm_pauseRead;
	static bool sm_asyncInstall;

	static const char *sm_hddPath;

	static sInstallProgress sm_installProgress;

	static datCallback fileDoneCallback;

	static bool sm_IsBootedFromHdd;
#if __PS3
	static char sm_contentInfoPath[CELL_GAME_PATH_MAX];
	static char sm_gameDataPath[CELL_GAME_PATH_MAX];
	static s32 sm_availableHddSpaceKB;
	static bool sm_usingCache;
#if __DEV
	static atArray<atHashString> sm_filesToKeep;
#endif
#endif

	static datCallback sm_keepAliveCallback;
	static u32 sm_keepAliveCallbackInterval;

};

}	// namespace rage

#endif // !FILE_DEVICE_INSTALLER_H
