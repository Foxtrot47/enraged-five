// 
// file/savegame_pc.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/file_config.h"
#include "savegame.h"

#if RSG_PC && !USE_STEAM_CLOUD_SAVES

#include "device.h"
#include "asset.h"
#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "data/bitbuffer.h"
#include "diag/channel.h"
#include "diag/seh.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlpc.h"
#include "rline/rltitleid.h"
#include "string/unicode.h"
#include "system/xtl.h"

#define PC_FILE_SIZE		50
#define MAX_DISPLAY_NAME	128
#define SAVEGAME_STACK_SIZE	16384

#if !__FINAL || defined(MASTER_NO_SCUI)
#pragma warning(disable: 4668)
#include <shlobj.h>
#pragma warning(error: 4668)
#endif //!__FINAL || defined(MASTER_NO_SCUI)

namespace rage {

	RAGE_DEFINE_CHANNEL(savegamepc);

	#define sgDebug1(fmt, ...)  RAGE_DEBUGF1(savegamepc, fmt, ##__VA_ARGS__)
	#define sgDebug2(fmt, ...)  RAGE_DEBUGF2(savegamepc, fmt, ##__VA_ARGS__)
	#define sgDebug3(fmt, ...)  RAGE_DEBUGF3(savegamepc, fmt, ##__VA_ARGS__)
	#define sgWarning(fmt, ...) RAGE_WARNINGF(savegamepc, fmt, ##__VA_ARGS__)
	#define sgError(fmt, ...)   RAGE_ERRORF(savegamepc, fmt, ##__VA_ARGS__)

#if defined(MASTER_NO_SCUI)
	NOSTRIP_XPARAM(noSocialClub);
#elif !__FINAL
	XPARAM(noSocialClub);
#endif

    class Header
    {
	public:
        static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(u32) +						// m_Version
                                                        sizeof(char16) * MAX_DISPLAY_NAME +	// m_DisplayName
														sizeof(u32);						// m_Hash

        static const unsigned FILE_VERSION = 1;

        u32 m_Version;
        char16 m_DisplayName[MAX_DISPLAY_NAME];
		u32 m_Hash;

        u32 GetExpectedVersion() const {return FILE_VERSION;}

        u32 GetVersion() {return m_Version;}
        char16* GetDisplayName() {return m_DisplayName;}

        Header()
        {
            Clear();
        }

        void Clear()
        {
            m_Version = 0;
            memset(m_DisplayName, 0, sizeof(m_DisplayName));
			m_Hash = 0;
        }

		u32 CalculateHash(u32 version, const char16 (&displayName)[MAX_DISPLAY_NAME]) const
		{
			u32 hash = atDataHash((const char*)&version, sizeof(version));
			return atDataHash((const char*)displayName, sizeof(displayName), hash);
		}

        bool Import(const void* buf,
                    const unsigned sizeOfBuf,
                    unsigned* size = 0)
        {
            datImportBuffer bb;
            bb.SetReadOnlyBytes(buf, sizeOfBuf);

            bool success = false;

            Clear();

            rtry
            {
                rverify(buf, catchall, );

                // read
                rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
                rverify(bb.ReadBytes(m_DisplayName, sizeof(m_DisplayName)), catchall, );
				rverify(bb.ReadUns(m_Hash, sizeof(m_Hash) << 3), catchall, );

                // verify
                rcheck(GetVersion() == GetExpectedVersion(), catchall, );
				u32 hash = CalculateHash(m_Version, m_DisplayName);
				rverify(hash == m_Hash, catchall, );

				success = true;        
            }
            rcatchall
            {
            }

            if(size){*size = success ? bb.GetNumBytesRead() : 0;}

            return success;
        }

        bool Export(const char16 (&displayName)[MAX_DISPLAY_NAME],
					void* buf,
                    const unsigned sizeOfBuf,
                    unsigned* size) const
        {
            datExportBuffer bb;
            bb.SetReadWriteBytes(buf, sizeOfBuf);

            bool success = false;

            unsigned version = GetExpectedVersion();

            rtry
            {
                rverify(buf, catchall, );
                rverify(sizeOfBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

				u32 hash = CalculateHash(version, displayName);

                rverify(bb.WriteUns(version, sizeof(version) << 3), catchall, );
                rverify(bb.WriteBytes(displayName, sizeof(displayName)), catchall, );
				rverify(bb.WriteUns(hash, sizeof(hash) << 3), catchall, );
                
                success = true;        
            }
            rcatchall
            {
            }

            if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

            return success;
        }
    };

	// This is intentionally the same Title ID as the XContent sample.
	//char *XEX_TITLE_ID = "FFFF011D";
	extern char* XEX_TITLE_ID;

	fiSaveGame SAVEGAME;
	static fiSaveGameState::State m_State = fiSaveGameState::IDLE;

	static bool s_bFileMissing = false, s_bFileCorrupt = false, s_bCreateDirectory = false;

	//**************************************************************************************************
	// PC specific functions
	//**************************************************************************************************
	static u64 s_filetime;	// in seconds	
	static u64 s_fileSize;

	bool GetSaveFilePath(u32 signInId, char (&path)[RAGE_MAX_PATH], const char* strFileName);
	bool GetDisplayNameAndModifiedTime(u32 signInId, fiSaveGame::Content &outContent, const char* fileName);
	int CompareModifiedTimes(const void* _a, const void* _b);

	static atRangeArray<int,fiSaveGame::SIGNIN_COUNT> s_Counts;
	static atRangeArray<u32,fiSaveGame::SIGNIN_COUNT> s_Loads;

	static sysIpcThreadId s_ThreadId = sysIpcThreadIdInvalid;

	static volatile bool s_FileOperationDone = false;
	static volatile bool s_FileOperationError = false;
	static const char* s_Filename;
	static const char16* s_DisplayName;
	static const void* s_SaveData;
	static volatile void* s_ValidateFunc;
	static volatile int s_MaxCount = 0, s_UserId = 0;
	static fiSaveGame::Content* s_OutContent;
	static volatile u32 s_SaveSize, s_ExtraSpaceRequired;
	static volatile fiSaveGame::Errors s_ErrorState;

#if CREATE_BACKUP_SAVES
	static volatile bool s_IsBackup;
#endif	//CREATE_BACKUP_SAVES

	static bool sm_IsLoadedDataValid;
	static s32 sm_ExtraSpaceRequired = 0;

#if !__FINAL || defined(MASTER_NO_SCUI)
	bool DirectoryExists(const char * dirPath)
	{
		DWORD fAttr = GetFileAttributesA(dirPath);
		if (fAttr == INVALID_FILE_ATTRIBUTES)
		{
			return false; 
		}

		if (fAttr & FILE_ATTRIBUTE_DIRECTORY)
		{
			return true;
		}

		return false;
	}
#endif //!__FINAL || defined(MASTER_NO_SCUI)

	//==================================================================================================
	//==================================================================================================
	bool GetSaveFilePath(u32 OUTPUT_ONLY(signInId), char (&path)[RAGE_MAX_PATH], const char* strFileName)
	{
		sgDebug1("Trace: GetSaveFilePath(signInId:%d, strFileName:%s)", signInId, strFileName);

		bool success = false;

#if RSG_PC
		char fullPath[rgsc::RGSC_MAX_PATH] = {0};

		sysMemSet(path, 0, sizeof(path));
		sysMemSet(fullPath, 0, sizeof(fullPath));

#if !__FINAL || defined(MASTER_NO_SCUI)
		if (PARAM_noSocialClub.Get())
		{
			char myDocPath[RAGE_MAX_PATH] = {0};
			if ( SUCCEEDED( SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, myDocPath) ) )
			{	
				safecpy(fullPath, myDocPath);
				safecat(fullPath, "\\Rockstar Games\\");
				safecat(fullPath, g_rlTitleId->m_RosTitleId.GetTitleDirectoryName());
				safecat(fullPath, "\\Default\\");

				// check if directory exists
				if (!s_bCreateDirectory)
				{
					if (!DirectoryExists(fullPath))
					{
						int result = SHCreateDirectoryEx(NULL, fullPath, NULL);
						if(result != ERROR_SUCCESS && result != ERROR_ALREADY_EXISTS)
						{	
							sgWarning("Can't create directory: %s \n", fullPath);
						}
					}

					s_bCreateDirectory = true;
				}

				if(strFileName != NULL)
				{
					safecat(fullPath, strFileName);
				}

				safecpy(path, fullPath);
				success = true;
			}
			else
			{
				return false;
			}
		}
		else
#endif //!__FINAL || defined(MASTER_NO_SCUI)
		{
			// you need to be signed in to save profile-specific data.
			if(AssertVerify(g_rlPc.GetProfileManager()->IsSignedIn()))
			{
				if(AssertVerify(SUCCEEDED(g_rlPc.GetFileSystem()->GetTitleProfileDirectory(fullPath, true))))
				{
					if(strFileName != NULL)
					{
						safecat(fullPath, strFileName);
					}

					safecpy(path, fullPath);
					success = true;
				}
			}
		}
#else
		Assert(false);
#endif

		sgDebug1("Trace: GetSaveFilePath() success:%s, path:%s", success ? "true" : "false", path);

		return success;
	}

	//==================================================================================================
	//==================================================================================================
	bool GetDisplayNameAndModifiedTime(u32 signInId, fiSaveGame::Content &outContent, const char* fileName)
	{
		memset(outContent.DisplayName, 0, sizeof(outContent.DisplayName));

		char fullFileName[RAGE_MAX_PATH];
		if(GetSaveFilePath(signInId, fullFileName, fileName) == false)
		{
			return false;
		}

		fiStream *S = fiStream::Open(fullFileName);
		if (!S)
		{
			return false;
		}

		Header header;
		u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD] = {0};		
		int bytesRead = S->Read(buf, sizeof(buf));
		S->Close();

		if(bytesRead != sizeof(buf))
		{
			return false;
		}

		if(header.Import(buf, sizeof(buf)) == false)
		{
			return false;
		}

		safecpy(outContent.DisplayName, header.GetDisplayName());

		const fiDevice *device = fiDevice::GetDevice(fullFileName);
		if (device)
		{
			u64 fileTime = device->GetFileTime(fullFileName);
			outContent.ModificationTimeHigh = (u32) (fileTime >> 32);
			outContent.ModificationTimeLow = (u32) (fileTime & 0xffffffff);
		}
		else
		{
			return false;
		}

		return true;
	}

	int CompareModifiedTimes(const void* _a, const void* _b)
	{
		fiSaveGame::Content* a = (fiSaveGame::Content*) _a;
		fiSaveGame::Content* b = (fiSaveGame::Content*) _b;

		if (a->ModificationTimeHigh == b->ModificationTimeHigh)
		{
			return (a->ModificationTimeLow < b->ModificationTimeLow) ? 1 : -1;
		}
		return (a->ModificationTimeHigh < b->ModificationTimeHigh) ? 1 : -1;
	}


	//**************************************************************************************************
	// fiSaveGame functions
	//**************************************************************************************************

	void fiSaveGame::InitClass()
	{
		sgDebug1("Trace: InitClass");
		m_State = fiSaveGameState::IDLE;
	}

	void fiSaveGame::ShutdownClass()
	{
		sgDebug1("Trace: ShutdownClass");
		m_State = fiSaveGameState::IDLE;
	}

	bool fiSaveGame::BeginSelectDevice(int /*signInId*/ ,u32 /*contentType*/,u32 /*minSpaceAvailable*/,bool /*forceShow*/,bool /*manageStorage*/) 
	{
		sgDebug1("Trace: BeginSelectDevice");

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgWarning("Cannot begin device selection, operation already in progress");
			return false;
		}

		m_State = fiSaveGameState::SELECTING_DEVICE;

		return true;
	}


	bool fiSaveGame::CheckSelectDevice(int /* signInId */) 
	{
		sgDebug1("Trace: CheckSelectDevice");

		if (m_State != fiSaveGameState::SELECTING_DEVICE)
		{
			sgWarning("CheckSelectDevice called when not in the device selection state");
			return false;
		}

		m_State = fiSaveGameState::SELECTED_DEVICE;

		return true;
	}


	void fiSaveGame::EndSelectDevice(int) 
	{
		sgDebug1("Trace: EndSelectDevice");

		if (m_State == fiSaveGameState::SELECTED_DEVICE)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}


	u32 fiSaveGame::GetSelectedDevice(int) 
	{
		sgDebug1("Trace: GetSelectedDevice");

		return 1;	// same as Xenon Hard Drive
	}


	void fiSaveGame::SetSelectedDevice(int, u32) 
	{
		sgDebug1("Trace: SetSelectedDevice");
	}

#define REPLAY_SAVE_FILENAME  "SGTA50000"

	void s_BeginEnumeration(void*)
	{
		char fullFileName[RAGE_MAX_PATH] = {0};

		if(GetSaveFilePath(s_UserId, fullFileName, NULL) == false)
		{
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		const fiDevice *device = fiDevice::GetDevice(fullFileName);

		fiFindData data;
		fiHandle h = device->FindFileBegin(fullFileName,data);

		if (fiIsValidHandle(h)) 
		{
			do
			{
				if (data.m_Name[0] != '.' && !(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
				{
					bool	isValidSave = true;

#if CREATE_BACKUP_SAVES
					// Look for the .bak extension and exclude it from the list, while making sure both files exist
					char *pBak = strrchr(data.m_Name,'.');
					if( pBak != NULL )
					{
						// No need for a further check since normal saves have no extension.
						// "pc_settings.bin" does come in, but it's also to be ignored here.
						isValidSave = false;	// Will be excluded from the list
					}
#endif	//CREATE_BACKUP_SAVES

					fiSaveGame::Content &c = s_OutContent[s_Counts[s_UserId]];
					c.ModificationTimeHigh = 0;
					c.ModificationTimeLow = 0;
					if( isValidSave && GetDisplayNameAndModifiedTime(s_UserId, c, data.m_Name) )
					{
						safecpy(c.Filename, data.m_Name);
						sgDebug1("Trace: BeginEnumeration() adding file to list: filename:%s, displayname:%s, ModTimeHigh=%u ModTimeLow=%u", c.Filename, c.DisplayName, c.ModificationTimeHigh, c.ModificationTimeLow);
						s_Counts[s_UserId]++;
					}
				}
			} while (s_Counts[s_UserId] < s_MaxCount && device->FindFileNext(h,data));
			device->FindFileEnd(h);
		}

		qsort(s_OutContent, s_Counts[s_UserId], sizeof(fiSaveGame::Content), CompareModifiedTimes);

		s_FileOperationError = false;
		s_FileOperationDone = true;
	}

	bool fiSaveGame::BeginEnumeration(int signInId,u32 OUTPUT_ONLY(contentType),Content* outContent,int maxCount)
	{
		sgDebug1("Trace: BeginEnumeration(signInId:%d, contentType:%d, maxCount:%d)", signInId, contentType, maxCount);
		if(!AssertVerify(signInId >= 0))
		{
			sgWarning("Invalid sign in ID %d", signInId);
			return false;
		}

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgWarning("Cannot begin enumeration, operation already in progress");
			return false;
		}

		s_Counts[signInId] = 0;

		if(maxCount <= 0)
		{
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgError("s_BeginEnumeration - didn't finish properly, can't start.");
			return false;
		}

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_MaxCount = maxCount;
		s_OutContent = outContent;
		s_UserId = signInId;

		s_ThreadId = sysIpcCreateThread(s_BeginEnumeration,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] EnumerationThread");

		sgDebug1("BeginEnumeration started %d",sysTimer::GetSystemMsTime());
		m_State = fiSaveGameState::ENUMERATING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool fiSaveGame::CheckEnumeration(int signInId,int &outCount)
	{
		sgDebug1("Trace: CheckEnumeration");

		if (m_State != fiSaveGameState::ENUMERATING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				s_Counts[signInId] = 0;
			}
			else
			{
				s_ErrorState = fiSaveGame::SAVE_GAME_SUCCESS;
			}

			outCount = s_Counts[signInId];
			m_State = fiSaveGameState::ENUMERATED_CONTENT;
			return true;
		}

		return false;
	}

	void fiSaveGame::EndEnumeration(int signInId)
	{
		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgDebug1("EndEnumeration time %d",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::ENUMERATED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}

		s_Counts[signInId] = 0;
	}

	static bool ConvertDisplayName(const char16* displayName, char16 (&title)[MAX_DISPLAY_NAME])
	{
		memset(title, 0, sizeof(title));
		safecpy(title, displayName, MAX_DISPLAY_NAME);

		// now add the date in the form "mm/dd/yy hh:mm:ss"
		char16	dateTime[22];
		
		// we are using the windows functions as the game ones are not in rage but are in framework.
		SYSTEMTIME time;
		GetLocalTime( &time );

		// Needed for UTF8_TO_WIDE
		USES_CONVERSION;

		formatf( dateTime,
				 UTF8_TO_WIDE(" - %02d/%02d/%02d %02d:%02d:%02d"),
				 time.wMonth,
				 time.wDay,
				 time.wYear%100,
				 time.wHour,
				 time.wMinute,
				 time.wSecond );

		safecat(title, dateTime, MAX_DISPLAY_NAME);

		return true;
	}

	void s_BeginFileSave(void*)
	{
		char fullfilename[RAGE_MAX_PATH];

		// Get home directory
		if(GetSaveFilePath(s_UserId, fullfilename, s_Filename) == false)
		{
			s_FileOperationDone = true;
			s_FileOperationError = true;
			return;
		}

#if CREATE_BACKUP_SAVES
		if( s_IsBackup )
		{
			strcat(fullfilename,".bak");
		}
#endif	//CREATE_BACKUP_SAVES

		char16 title[MAX_DISPLAY_NAME] = {0};
		ConvertDisplayName(s_DisplayName, title);

		Header header;
		u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
		if(header.Export(title, buf, sizeof(buf), NULL) == false)
		{
			s_FileOperationDone = true;
			s_FileOperationError = true;
			Errorf("Exporting the header failed!");
			return;
		}

		const fiDevice *device = fiDevice::GetDevice(fullfilename);
		if (device)
		{
			fiHandle h = device->Create(fullfilename);
			if (h != fiHandleInvalid)
			{
				int bytesWritten = device->Write(h, buf, sizeof(buf));
				if (bytesWritten == 0 || Header::MAX_BYTE_SIZEOF_PAYLOAD != bytesWritten)
				{
					Errorf("Device write did not write the header bytes.");
					s_FileOperationDone = true;
					s_FileOperationError = true;
					device->Close(h);
					return;
				}

				bytesWritten = device->Write(h, s_SaveData, s_SaveSize);
				if (bytesWritten == 0 || bytesWritten != (int)s_SaveSize)
				{
					Errorf("Device write did not write the save size.");
					s_FileOperationDone = true;
					s_FileOperationError = true;
					device->Close(h);
					return;
				}

				device->Close(h);
			}
			else
			{
				Errorf("CreateFile was unable to open the file for writing. /n");
				s_FileOperationDone = true;
				s_FileOperationError = true;
				return;
			}
		}
		else
		{
			Errorf("CreateFile was unable to open the file for writing. /n");
			s_FileOperationDone = true;
			s_FileOperationError = true;
			return;
		}

		m_State = fiSaveGameState::SAVING_CONTENT;

		s_FileOperationDone = true;
	}

	bool fiSaveGame::BeginSave(int signInId,u32 /*contentType*/,const char16 * displayName,const char * filename,const void *saveData,u32 saveSize,bool OUTPUT_ONLY(overwrite))
	{
		sgDebug1("Trace: BeginSave(signInId:%d, displayName:%s, filename:%s, saveSize:%d, overwrite:%s)", signInId, displayName, filename, saveSize, overwrite ? "true" : "false");

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgWarning("Cannot begin saving, operation already in progress");
			return false;
		}

		if (saveSize == 0)
		{
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgError("s_BeginFileSave - didn't finish properly, can't start.");
			return false;
		}

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = signInId;
		s_Filename = filename;
		s_DisplayName = displayName;
		s_SaveData = saveData;
		s_SaveSize = saveSize;
#if CREATE_BACKUP_SAVES
		s_IsBackup = false;
#endif	//CREATE_BACKUP_SAVES

		s_ThreadId = sysIpcCreateThread(s_BeginFileSave,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] SaveThread");

		sgDebug1("BeginSave started %d",sysTimer::GetSystemMsTime());
		m_State = fiSaveGameState::SAVING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool fiSaveGame::CheckSave(int /*signInId*/,bool &outIsValid,bool& fileExists)
	{
		sgDebug1("Trace: CheckSave");

		if (m_State != fiSaveGameState::SAVING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				outIsValid = false;
				fileExists = true;
			}
			else
			{
				m_State = fiSaveGameState::SAVED_CONTENT;
				outIsValid = true;
				fileExists = true;
			}
			return true;
		}

		return false;
	}


	void fiSaveGame::EndSave(int /*signInId*/)
	{
		sgDebug1("Trace: EndSave");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgDebug1("EndSave time %d",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::SAVED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

#if CREATE_BACKUP_SAVES
	
	bool fiSaveGame::BeginBackupSave(int signInId,u32 /*contentType*/,const char16 * displayName,const char * filename,const void *saveData,u32 saveSize,bool OUTPUT_ONLY(overwrite))
	{
		sgDebug1("Trace: BeginBackupSave(signInId:%d, displayName:%s, filename:%s, saveSize:%d, overwrite:%s)", signInId, displayName, filename, saveSize, overwrite ? "true" : "false");

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgWarning("Cannot begin saving, operation already in progress");
			return false;
		}

		if (saveSize == 0)
		{
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgError("s_BeginFileSave - didn't finish properly, can't start.");
			return false;
		}

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = signInId;
		s_Filename = filename;
		s_DisplayName = displayName;
		s_SaveData = saveData;
		s_SaveSize = saveSize;
		s_IsBackup = true;
		s_ThreadId = sysIpcCreateThread(s_BeginFileSave,NULL,SAVEGAME_STACK_SIZE,PRIO_NORMAL,"[RAGE] SaveThread");

		sgDebug1("BeginSave started %d",sysTimer::GetSystemMsTime());
		m_State = fiSaveGameState::SAVING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool fiSaveGame::CheckBackupSave(int signInId,bool &outIsValid,bool& fileExists)
	{
		sgDebug1("Trace: CheckBackupSave");
		return CheckSave(signInId, outIsValid, fileExists);
	}

	void fiSaveGame::EndBackupSave(int signInId)
	{
		sgDebug1("Trace: EndBackupSave");
		EndSave(signInId);
	}

#endif	//CREATE_BACKUP_SAVES
	
	bool fiSaveGame::BeginFreeSpaceCheck(int signInId,const char* /*filename*/,u32 saveSize)
	{
		sgDebug1("Trace: BeginFreeSpaceCheck");

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		if(saveSize == 0)
		{
			return false;
		}

		m_State = fiSaveGameState::CHECKING_FREE_SPACE;

		u64 freeSpace = GetTotalAvailableSpace(signInId);

		// leave some extra space for background processes
		static const u32 BUFFER = (2 * 1024 * 1024);

		sm_ExtraSpaceRequired = 0;
		saveSize = (u32)CalculateDataSizeOnDisk(signInId, saveSize);
		if(freeSpace < (saveSize + BUFFER))
		{
			sm_ExtraSpaceRequired = (u32)(freeSpace - (saveSize + BUFFER));
		}

		return true;
	}

	bool fiSaveGame::CheckFreeSpaceCheck(int /*signInId*/, int& ExtraSpaceRequired)
	{
		sgDebug1("Trace: CheckFreeSpaceCheck");

		if (m_State != fiSaveGameState::CHECKING_FREE_SPACE)
		{
			return false;
		}

		m_State = fiSaveGameState::HAVE_CHECKED_FREE_SPACE;

		ExtraSpaceRequired = sm_ExtraSpaceRequired;

		return true;
	}

	void fiSaveGame::EndFreeSpaceCheck(int /*signInId*/)
	{
		sgDebug1("Trace: EndFreeSpaceCheck");

		if (m_State == fiSaveGameState::HAVE_CHECKED_FREE_SPACE)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	bool fiSaveGame::BeginExtraSpaceCheck(int /*signInId*/, const char * /*filename*/, u32 /*saveSize*/)
	{
		sgDebug1("Trace: BeginExtraSpaceCheck");

		return true;
	}

	bool fiSaveGame::CheckExtraSpaceCheck(int /*signInId*/, int & /*ExtraSpaceRequired*/, int & /*TotalHDDFreeSize*/, int & /*SizeOfSystemFileKB*/)
	{
		sgDebug1("Trace: CheckExtraSpaceCheck");

		return true;
	}

	void fiSaveGame::EndExtraSpaceCheck(int /*signInId*/)
	{
		sgDebug1("Trace: EndExtraSpaceCheck");
	}

	bool fiSaveGame::BeginReadNamesAndSizesOfAllSaveGames(int /*signInId*/, Content * /*outContent*/, int * /*outFileSizes*/, int /*maxCount*/)
	{
		sgDebug1("Trace: BeginReadNamesAndSizesOfAllSaveGames");

		return true;
	}

	bool fiSaveGame::CheckReadNamesAndSizesOfAllSaveGames(int /*signInId*/, int & /*outCount*/)
	{
		sgDebug1("Trace: CheckReadNamesAndSizesOfAllSaveGames");

		return true;
	}

	void fiSaveGame::EndReadNamesAndSizesOfAllSaveGames(int /*signInId*/)
	{
		sgDebug1("Trace: EndReadNamesAndSizesOfAllSaveGames");
	}

	bool fiSaveGame::BeginLoad(int signInId,u32 /*contentType*/,u32 /*deviceId*/,const char* filename,void *loadData,u32 loadSize)
	{
		s_bFileMissing = false; //@MP3MOD
		s_bFileCorrupt = false; //@MP3MOD

		sgDebug1("Trace: BeginLoad(signInId:%d, filename:%s, loadSize:%d)", signInId, filename, loadSize);

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgWarning("Cannot begin loading, operation already in progress");
			return false;
		}

		s_Loads[signInId] = 0;
		sm_IsLoadedDataValid = false;

		if (loadSize == 0)
		{
			return false;
		}

		char fullfilename[RAGE_MAX_PATH];

		// Get home directory
		if(GetSaveFilePath(signInId, fullfilename, filename) == false)
		{
			s_bFileMissing = true; //@MP3MOD
			return false;
		}

		fiStream *S = fiStream::Open(fullfilename);
		if (!S)
		{
			s_bFileMissing = true; //@MP3MOD
			return false;
		}
		else 
		{
			Header header;
			u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD] = {0};		
			int bytesRead = S->Read(buf, sizeof(buf));

			if((bytesRead != sizeof(buf)) ||
				(header.Import(buf, sizeof(buf)) == false))
			{
				S->Close();
				return false;
			}

			s_Loads[signInId] = S->Read(loadData,loadSize);
			S->Close();

			sm_IsLoadedDataValid = true;
			m_State = fiSaveGameState::LOADING_CONTENT;
			return true;
		}
	}

	bool fiSaveGame::CheckLoad(int signInId, bool &outIsValid, u32 &loadSize)
	{
		sgDebug1("Trace: CheckLoad");

		if (m_State != fiSaveGameState::LOADING_CONTENT)
		{
			return false;
		}

		m_State = fiSaveGameState::LOADED_CONTENT;

		outIsValid = sm_IsLoadedDataValid;
		loadSize = s_Loads[signInId];

		return true;
	}

	void fiSaveGame::EndLoad(int signInId)
	{
		sgDebug1("Trace: EndLoad");

		if (m_State == fiSaveGameState::LOADED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}

		s_Loads[signInId] = 0;
		sm_IsLoadedDataValid = false;
	}

	bool fiSaveGame::BeginIconSave(int /*signInId*/,u32 /*contentType*/,const char* /*filename*/,const void* /*iconData*/,u32 /*iconSize*/)
	{
		sgDebug1("Trace: BeginIconSave");
		return true;
	}


	bool fiSaveGame::CheckIconSave(int /*signInId*/)
	{
		sgDebug1("Trace: CheckIconSave");
		return true;
	}


	void fiSaveGame::EndIconSave(int /*signInId*/)
	{
		sgDebug1("Trace: EndIconSave");
	}


	bool fiSaveGame::BeginContentVerify(int /*signInId*/,u32 /*contentType*/, u32 /*deviceId*/, const char* /*filename*/)
	{
		sgDebug1("Trace: BeginContentVerify");
		return true;
	}


	bool fiSaveGame::CheckContentVerify(int /*signInId*/,bool& verified)
	{
		sgDebug1("Trace: CheckContentVerify");
		verified = true;
		return true;
	}


	void fiSaveGame::EndContentVerify(int /*signInId*/)
	{
		sgDebug1("Trace: EndContentVerify");
	}


	bool fiSaveGame::BeginDelete(int signInId,u32 /*contentType*/,const char* filename)
	{
		sgDebug1("Trace: BeginDelete");

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		char szPath[RAGE_MAX_PATH];

		if(GetSaveFilePath(signInId, szPath, filename) == false)
		{
			return false;
		}

		const fiDevice *device = fiDevice::GetDevice(szPath);
		if (!device)
		{
			return false;
		}

		if (!device->Delete(szPath))
		{
			return false;
		}

		m_State = fiSaveGameState::DELETING_CONTENT;

		return true;
	}


	bool fiSaveGame::CheckDelete(int /*signInId*/)
	{
		sgDebug1("Trace: CheckDelete");

		if (m_State != fiSaveGameState::DELETING_CONTENT)
		{
			return false;
		}

		m_State = fiSaveGameState::DELETED_CONTENT;

		return true;
	}


	void fiSaveGame::EndDelete(int /*signInId*/)
	{
		sgDebug1("Trace: EndDelete");

		if (m_State == fiSaveGameState::DELETED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	bool fiSaveGame::BeginGetCreator(int OUTPUT_ONLY(signInId), u32 OUTPUT_ONLY(contentType), u32 OUTPUT_ONLY(deviceId),const char* OUTPUT_ONLY(fileName))
	{
		sgDebug1("Trace: BeginGetCreator(signInId:%d, contentType:%d, deviceId:%d, fileName:%s)", signInId, contentType, deviceId, fileName);

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		m_State = fiSaveGameState::GETTING_CREATOR;

		return true;
	}

	bool fiSaveGame::CheckGetCreator(int /*signInId*/, bool& bIsCreator)
	{
		sgDebug1("Trace: CheckGetCreator");

		if (m_State != fiSaveGameState::GETTING_CREATOR)
		{
			return false;
		}

		m_State = fiSaveGameState::GOT_CREATOR;

		bIsCreator = true;

		return true;
	}

	void fiSaveGame::EndGetCreator(int /*signInId*/)
	{
		sgDebug1("Trace: EndGetCreator");

		if (m_State == fiSaveGameState::GOT_CREATOR)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}


	u64 fiSaveGame::GetTotalAvailableSpace(int signInId)
	{
		sgDebug1("Trace: GetTotalAvailableSpace");

		char fullfilename[RAGE_MAX_PATH];

		if(GetSaveFilePath(signInId, fullfilename, NULL) == false)
		{
			return 0;
		}

		ULARGE_INTEGER freeBytesAvailable;
		wchar_t widePath[RAGE_MAX_PATH] = {0};
		MultiByteToWideChar(CP_UTF8 , NULL , fullfilename, -1, widePath, RAGE_MAX_PATH);
		BOOL result = GetDiskFreeSpaceExW(widePath, &freeBytesAvailable, NULL, NULL);

		if(result == FALSE)
		{
			return 0;
		}

		return freeBytesAvailable.QuadPart;
	}

	u64 fiSaveGame::CalculateDataSizeOnDisk(int , rage::u32 dataSize)
	{
		sgDebug1("Trace: CalculateDataSizeOnDisk");

		// account for security and filesystem alignment (assume 4k?)
#define ALIGN_4K(x) ((x) + ((4 * 1024) - 1)) & ~((4 * 1024) - 1);

		return ALIGN_4K(dataSize + Header::MAX_BYTE_SIZEOF_PAYLOAD);
	}

	fiSaveGameState::State fiSaveGame::GetState(int /*signInId*/) const
	{
		sgDebug1("Trace: GetState");

		return m_State;
	}

	int fiSaveGame::GetMaxUsers()
	{
		sgDebug1("Trace: GetMaxUsers");

		return 1;
	}

	bool fiSaveGame::IsStorageDeviceChanged(int /*signInId*/)
	{
		sgDebug1("Trace: IsStorageDeviceChanged");

		return false;
	}

	fiSaveGame::Errors fiSaveGame::GetError(int /*signInId*/)
	{
		sgDebug1("Trace: GetError");

		return fiSaveGame::SAVE_GAME_SUCCESS;
	}

	void fiSaveGame::SetStateToIdle(int /*signInId*/)
	{
		sgDebug1("Trace: SetStateToIdle");
		m_State = fiSaveGameState::IDLE;
	}

	void fiSaveGame::SetSelectedDeviceToAny(int /*signInId*/)
	{
		sgDebug1("Trace: SetSelectedDeviceToAny");
	}

	bool fiSaveGame::IsCurrentDeviceValid(int /*signInId*/)
	{
		sgDebug1("Trace: IsCurrentDeviceValid");

		return true;
	}

	bool fiSaveGame::HasFileBeenAccessed(int , const char* )
	{
		sgDebug1("Trace: HasFileBeenAccessed");

		return false;
	}

	void fiSaveGame::SetIcon(char *, const u32)
	{
		sgDebug1("Trace: SetIcon");
	}

	void fiSaveGame::SetMaxNumberOfSaveGameFilesToEnumerate(int /*MaxNumSaveGames*/)
	{
		sgDebug1("Trace: SetMaxNumberOfSaveGameFilesToEnumerate");
	}

	void fiSaveGame::SetCheckForUserBindErrors(const char* /*pBindErrorMessage*/)
	{
		sgDebug1("Trace: SetCheckForUserBindErrors");
	}

	static fiHandle s_Handle = NULL;
	static const fiDevice* s_Device = NULL;

	bool fiSaveGame::BeginGetFileSize(int signInId, const char* filename, bool /*bCalculateSizeOnDisk*/)
	{
		sgDebug1("Trace: BeginGetFileSize");

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		char fullfilename[RAGE_MAX_PATH];
		if(GetSaveFilePath(signInId, fullfilename, filename) == false)
		{
			return false;
		}

		const fiDevice *device = fiDevice::GetDevice(fullfilename);

		// GetFileTime returns a value representing the number of 100-nanosecond intervals( 1 nanosecond == 1E-09 seconds)  
		s_fileSize = device->GetFileSize(fullfilename);

		if(s_fileSize < Header::MAX_BYTE_SIZEOF_PAYLOAD)
		{
			return false;
		}

		s_fileSize -= Header::MAX_BYTE_SIZEOF_PAYLOAD;

		m_State = fiSaveGameState::GETTING_FILE_SIZE;

		return true;
	}

	bool fiSaveGame::CheckGetFileSize(int /*signInId*/, u64 &fileSize)
	{
		sgDebug1("Trace: CheckGetFileSize");

		if (m_State != fiSaveGameState::GETTING_FILE_SIZE)
		{
			return false;
		}

		m_State = fiSaveGameState::HAVE_GOT_FILE_SIZE;
		fileSize = s_fileSize;

		return true;
	}

	void fiSaveGame::EndGetFileSize(int /*signInId*/)
	{
		sgDebug1("Trace: EndGetFileSize");

		if(m_State == fiSaveGameState::HAVE_GOT_FILE_SIZE)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	bool fiSaveGame::BeginGetFileModifiedTime(int /*signInId*/, const char* /*filename*/)
	{
		sgDebug1("Trace: BeginGetFileModifiedTime");

		return true;
	}

	bool fiSaveGame::CheckGetFileModifiedTime(int /*signInId*/, u32& /*modTimeHigh*/, u32& /*modTimeLow*/)
	{
		sgDebug1("Trace: CheckGetFileModifiedTime");

		return true;
	}

	void fiSaveGame::EndGetFileModifiedTime(int /*signInId*/)
	{
		sgDebug1("Trace: EndGetFileModifiedTime");
	}

	void fiSaveGame::SetUserServiceId(int /* index */ , int /* userId */)
	{
	}

	void fiSaveGame::SetMountIndex(int /* signInId */, int /* index */)
	{
	}

	bool fiSaveGame::BeginGetFileExists(int /*signInId*/, const char* /*filename*/)
	{
		return true;
	}

	bool fiSaveGame::CheckGetFileExists(int /*signInId*/, bool& /*exists*/)
	{
		return true;
	}

	void fiSaveGame::EndGetFileExists(int /*signInId*/)
	{	
	}


}		// namespace rage

#endif	// __WIN32PC
