// 
// file/tcpip_psp2.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#include "tcpip.h"
#include "winsock.h"

#if __PSP2 || RSG_ORBIS

#include <net.h>
// #include <../include/net/resolver.h>
#include <libnetctl.h>

#if RSG_ORBIS
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

#include "system/ipc.h"
#include "system/new.h"


typedef int SOCKET;

#include <string.h>

#if !defined(__UNITYBUILD)
// unity builds end up including this more than once and gives warnings
#pragma comment(lib,"SceNetCtl_stub_weak")
#pragma comment(lib,"SceNet_stub_weak")
#endif

namespace rage {

const char *error_string()
{
#if !__FINAL
	switch (sce_net_errno) {
		case SCE_NET_EPERM: return "SCE_NET_EPERM";
		case SCE_NET_ENOENT: return "SCE_NET_ENOENT";
		case SCE_NET_ESRCH: return "SCE_NET_ESRCH";
		case SCE_NET_EINTR: return "SCE_NET_EINTR";
		case SCE_NET_EIO: return "SCE_NET_EIO";
		case SCE_NET_ENXIO: return "SCE_NET_ENXIO";
		case SCE_NET_E2BIG: return "SCE_NET_E2BIG";
		case SCE_NET_ENOEXEC: return "SCE_NET_ENOEXEC";
		case SCE_NET_EBADF: return "SCE_NET_EBADF";
		case SCE_NET_ECHILD: return "SCE_NET_ECHILD";
		case SCE_NET_EDEADLK: return "SCE_NET_EDEADLK";
		case SCE_NET_ENOMEM: return "SCE_NET_ENOMEM";
		case SCE_NET_EACCES: return "SCE_NET_EACCES";
		case SCE_NET_EFAULT: return "SCE_NET_EFAULT";
		case SCE_NET_ENOTBLK: return "SCE_NET_ENOTBLK";
		case SCE_NET_EBUSY: return "SCE_NET_EBUSY";
		case SCE_NET_EEXIST: return "SCE_NET_EEXIST";
		case SCE_NET_EXDEV: return "SCE_NET_EXDEV";
		case SCE_NET_ENODEV: return "SCE_NET_ENODEV";
		case SCE_NET_ENOTDIR: return "SCE_NET_ENOTDIR";
		case SCE_NET_EISDIR: return "SCE_NET_EISDIR";
		case SCE_NET_EINVAL: return "SCE_NET_EINVAL";
		case SCE_NET_ENFILE: return "SCE_NET_ENFILE";
		case SCE_NET_EMFILE: return "SCE_NET_EMFILE";
		case SCE_NET_ENOTTY: return "SCE_NET_ENOTTY";
		case SCE_NET_ETXTBSY: return "SCE_NET_ETXTBSY";
		case SCE_NET_EFBIG: return "SCE_NET_EFBIG";
		case SCE_NET_ENOSPC: return "SCE_NET_ENOSPC";
		case SCE_NET_ESPIPE: return "SCE_NET_ESPIPE";
		case SCE_NET_EROFS: return "SCE_NET_EROFS";
		case SCE_NET_EMLINK: return "SCE_NET_EMLINK";
		case SCE_NET_EPIPE: return "SCE_NET_EPIPE";
		case SCE_NET_EDOM: return "SCE_NET_EDOM";
		case SCE_NET_ERANGE: return "SCE_NET_ERANGE";
		case SCE_NET_EAGAIN: return "SCE_NET_EAGAIN";
		case SCE_NET_EINPROGRESS: return "SCE_NET_EINPROGRESS";
		case SCE_NET_EALREADY: return "SCE_NET_EALREADY";
		case SCE_NET_ENOTSOCK: return "SCE_NET_ENOTSOCK";
		case SCE_NET_EDESTADDRREQ: return "SCE_NET_EDESTADDRREQ";
		case SCE_NET_EMSGSIZE: return "SCE_NET_EMSGSIZE";
		case SCE_NET_EPROTOTYPE: return "SCE_NET_EPROTOTYPE";
		case SCE_NET_ENOPROTOOPT: return "SCE_NET_ENOPROTOOPT";
		case SCE_NET_EPROTONOSUPPORT: return "SCE_NET_EPROTONOSUPPORT";
		case SCE_NET_ESOCKTNOSUPPORT: return "SCE_NET_ESOCKTNOSUPPORT";
		case SCE_NET_EOPNOTSUPP: return "SCE_NET_EOPNOTSUPP";
		case SCE_NET_EPFNOSUPPORT: return "SCE_NET_EPFNOSUPPORT";
		case SCE_NET_EAFNOSUPPORT: return "SCE_NET_EAFNOSUPPORT";
		case SCE_NET_EADDRINUSE: return "SCE_NET_EADDRINUSE";
		case SCE_NET_EADDRNOTAVAIL: return "SCE_NET_EADDRNOTAVAIL";
		case SCE_NET_ENETDOWN: return "SCE_NET_ENETDOWN";
		case SCE_NET_ENETUNREACH: return "SCE_NET_ENETUNREACH";
		case SCE_NET_ENETRESET: return "SCE_NET_ENETRESET";
		case SCE_NET_ECONNABORTED: return "SCE_NET_ECONNABORTED";
		case SCE_NET_ECONNRESET: return "SCE_NET_ECONNRESET";
		case SCE_NET_ENOBUFS: return "SCE_NET_ENOBUFS";
		case SCE_NET_EISCONN: return "SCE_NET_EISCONN";
		case SCE_NET_ENOTCONN: return "SCE_NET_ENOTCONN";
		case SCE_NET_ESHUTDOWN: return "SCE_NET_ESHUTDOWN";
		case SCE_NET_ETOOMANYREFS: return "SCE_NET_ETOOMANYREFS";
		case SCE_NET_ETIMEDOUT: return "SCE_NET_ETIMEDOUT";
		case SCE_NET_ECONNREFUSED: return "SCE_NET_ECONNREFUSED";
		case SCE_NET_ELOOP: return "SCE_NET_ELOOP";
		case SCE_NET_ENAMETOOLONG: return "SCE_NET_ENAMETOOLONG";
		case SCE_NET_EHOSTDOWN: return "SCE_NET_EHOSTDOWN";
		case SCE_NET_EHOSTUNREACH: return "SCE_NET_EHOSTUNREACH";
		case SCE_NET_ENOTEMPTY: return "SCE_NET_ENOTEMPTY";
		case SCE_NET_EPROCLIM: return "SCE_NET_EPROCLIM";
		case SCE_NET_EUSERS: return "SCE_NET_EUSERS";
		case SCE_NET_EDQUOT: return "SCE_NET_EDQUOT";
		case SCE_NET_ESTALE: return "SCE_NET_ESTALE";
		case SCE_NET_EREMOTE: return "SCE_NET_EREMOTE";
		case SCE_NET_EBADRPC: return "SCE_NET_EBADRPC";
		case SCE_NET_ERPCMISMATCH: return "SCE_NET_ERPCMISMATCH";
		case SCE_NET_EPROGUNAVAIL: return "SCE_NET_EPROGUNAVAIL";
		case SCE_NET_EPROGMISMATCH: return "SCE_NET_EPROGMISMATCH";
		case SCE_NET_EPROCUNAVAIL: return "SCE_NET_EPROCUNAVAIL";
		case SCE_NET_ENOLCK: return "SCE_NET_ENOLCK";
		case SCE_NET_ENOSYS: return "SCE_NET_ENOSYS";
		case SCE_NET_EFTYPE: return "SCE_NET_EFTYPE";
		case SCE_NET_EAUTH: return "SCE_NET_EAUTH";
		case SCE_NET_ENEEDAUTH: return "SCE_NET_ENEEDAUTH";
		case SCE_NET_EIDRM: return "SCE_NET_EIDRM";
		case SCE_NET_ENOMSG: return "SCE_NET_ENOMSG";
		case SCE_NET_EOVERFLOW: return "SCE_NET_EOVERFLOW";
		// case SCE_NET_EILSEQ: return "SCE_NET_EILSEQ";
		// case SCE_NET_ENOTSUP: return "SCE_NET_ENOTSUP";
		case SCE_NET_ECANCELED: return "SCE_NET_ECANCELED";
		// case SCE_NET_EBADMSG: return "SCE_NET_EBADMSG";
		case SCE_NET_ENODATA: return "SCE_NET_ENODATA";
		// case SCE_NET_ENOSR: return "SCE_NET_ENOSR";
		// case SCE_NET_ENOSTR: return "SCE_NET_ENOSTR";
		// case SCE_NET_ETIME: return "SCE_NET_ETIME";
		case SCE_NET_EADHOC: return "SCE_NET_EADHOC";
		// case SCE_NET_EDISABLEDIF: return "SCE_NET_EDISABLEDIF";
		// case SCE_NET_ERESUME: return "SCE_NET_ERESUME";
		case SCE_NET_ENOTINIT: return "SCE_NET_ENOTINIT";
		case SCE_NET_ENOLIBMEM: return "SCE_NET_ENOLIBMEM";
		case SCE_NET_ETLS: return "SCE_NET_ETLS";
		case SCE_NET_ECALLBACK: return "SCE_NET_ECALLBACK";
		case SCE_NET_EINTERNAL: return "SCE_NET_EINTERNAL";
		case SCE_NET_ERETURN: return "SCE_NET_ERETURN";
		case SCE_NET_RESOLVER_EINTERNAL: return "SCE_NET_RESOLVER_EINTERNAL";
		case SCE_NET_RESOLVER_EBUSY: return "SCE_NET_RESOLVER_EBUSY";
		case SCE_NET_RESOLVER_ENOSPACE: return "SCE_NET_RESOLVER_ENOSPACE";
		case SCE_NET_RESOLVER_EPACKET: return "SCE_NET_RESOLVER_EPACKET";
		case SCE_NET_RESOLVER_ERESERVED224: return "SCE_NET_RESOLVER_ERESERVED224";
		case SCE_NET_RESOLVER_ENODNS: return "SCE_NET_RESOLVER_ENODNS";
		case SCE_NET_RESOLVER_ETIMEDOUT: return "SCE_NET_RESOLVER_ETIMEDOUT";
		case SCE_NET_RESOLVER_ENOSUPPORT: return "SCE_NET_RESOLVER_ENOSUPPORT";
		case SCE_NET_RESOLVER_EFORMAT: return "SCE_NET_RESOLVER_EFORMAT";
		case SCE_NET_RESOLVER_ESERVERFAILURE: return "SCE_NET_RESOLVER_ESERVERFAILURE";
		case SCE_NET_RESOLVER_ENOHOST: return "SCE_NET_RESOLVER_ENOHOST";
		case SCE_NET_RESOLVER_ENOTIMPLEMENTED: return "SCE_NET_RESOLVER_ENOTIMPLEMENTED";
		case SCE_NET_RESOLVER_ESERVERREFUSED: return "SCE_NET_RESOLVER_ESERVERREFUSED";
		case SCE_NET_RESOLVER_ENORECORD: return "SCE_NET_RESOLVER_ENORECORD";
		case SCE_NET_RESOLVER_EALIGNMENT: return "SCE_NET_RESOLVER_EALIGNMENT";
	}
#endif
	return "unknown";
}

const fiDeviceTcpIp& fiDeviceTcpIp::GetInstance() {
	static fiDeviceTcpIp s_TcpIpInstance;
	return s_TcpIpInstance;
}


fiDeviceTcpIp::fiDeviceTcpIp() {
}


fiDeviceTcpIp::~fiDeviceTcpIp() {
}


void fiDeviceTcpIp::InitClass(int argc, char** argv) {
	InitWinSock(argc, argv);
}

void fiDeviceTcpIp::ShutdownClass() {
	ShutdownWinSock();
}


fiHandle fiDeviceTcpIp::Open(const char *filename, bool /*readOnly*/) const {
	if (strncmp(filename,"tcpip:",6))
		return fiHandleInvalid;
	filename += 6;
	int port = 0;
	while (*filename && *filename != ':')
		port = port * 10 + (*filename++)-'0';
	if (!*filename)
		return fiHandleInvalid;
	else
		++filename;
	if (!strcasecmp(filename,"LISTEN")) {
		fiHandle listener = Listen(port,1);
		if (fiIsValidHandle(listener)) {
			Displayf("Waiting for connection on port %d...",port);
			fiHandle data = Pickup(listener);
			Close(listener);
			return data;
		}
		else {
			Errorf("Unable to establish server on port %d, already in use?",port);
			return listener;
		}
	}
	else
		return Connect(filename,port);
}


fiHandle fiDeviceTcpIp::Create(const char * filename) const {
	return Open(filename,false);
}


int fiDeviceTcpIp::StaticWrite(fiHandle fd,const void *buffer,int count) {
	int result = sceNetSend((SOCKET)fd,(const char*)buffer,count,0);
	if (result < 0)
	{
		switch(sce_net_errno)
		{
		case SCE_NET_EWOULDBLOCK:
			result = 0;
			break;
		case SCE_NET_EINPROGRESS:
			// ignore
			break;
		default:
			printf("fiDeviceTcpIp::StaticWrite %d\n", result);
			break;
		}
	}
	return result;
}


int fiDeviceTcpIp::StaticRead(fiHandle fd,void *buffer,int count) {
	int result = sceNetRecv((SOCKET)fd,(char*)buffer,count,0);
	if (result < 0)
	{
		switch(sce_net_errno)
		{
		case SCE_NET_EWOULDBLOCK:
			result = 0;
			break;
		case SCE_NET_EINPROGRESS:
			// ignore
			break;
		default:
			printf("fiDeviceTcpIp::StaticRead %d\n", sce_net_errno);
			break;
		}
	}
	return result;
}

int fiDeviceTcpIp::GetReadCount(fiHandle fd) {
	SceNetMsghdr hdr;
	int result = sceNetRecv((SOCKET)fd,&hdr,sizeof(hdr),SCE_NET_MSG_DONTWAIT | SCE_NET_MSG_PEEK);
	if (result < 0)
	{
		switch(sce_net_errno)
		{
		case SCE_NET_EWOULDBLOCK:
		case SCE_NET_EINPROGRESS:
			// ignore
			break;
		default:
			Errorf("fiDeviceTcpIp::GetReadCount %d", sce_net_errno);
			break;
		}
	}
	return (result>0)? result : 0;
}


void fiDeviceTcpIp::SetBlocking(fiHandle handle, bool blocking)
{
#if RSG_ORBIS
	int nonblocking = blocking ? 0 : 1;
	setsockopt((int)handle, SOL_SOCKET, SCE_NET_SO_NBIO, (char*)&nonblocking, sizeof(nonblocking));
#else
	// psp2 might be the same as the above
	Assertf(0, "SetBlocking is unimplemented");
#endif
}

int fiDeviceTcpIp::Seek(fiHandle /*handle*/,int /*offset*/,fiSeekWhence /*whence*/) const {
	return -1;
}


u64 fiDeviceTcpIp::Seek64(fiHandle /*handle*/,s64 /*offset*/,fiSeekWhence /*whence*/) const {
	return (u64)(s64)-1;
}


u64 fiDeviceTcpIp::GetFileTime(const char*) const {
	return 0;
}


bool fiDeviceTcpIp::SetFileTime(const char*,u64) const {
	return false;
}


int fiDeviceTcpIp::Close(fiHandle handle) const {
	return sceNetSocketClose((int)handle);
}

// Copied from target\samples\sample_code\network\api_net\console\resolver.c.
/* static void *allocateCbFunction(SceSize size,
								SceNetId rid, const char *name, void *user)
{
	void *ptr;

	Displayf("%s, size=%zd, rid=%d, name=%s, user=%p",
		__FUNCTION__, size, rid, name, user);
	ptr = rage_new char[size];
	return ptr;
}

static void freeCbFunction(void *ptr,
						   SceNetId rid, const char *name, void *user)
{
	Displayf("%s, ptr=%p, rid=%d, name=%s, user=%p",
		__FUNCTION__, ptr, rid, name, user);
	delete[] (char*)ptr;
} */

/* static int doResolverNtoa(const char *hostname, SceNetInAddr *addr)
{
	SceNetResolverParam rparam;
	SceNetId rid = -1;
	int ret;

	rparam.allocate = allocateCbFunction;
	rparam.free = freeCbFunction;
	rparam.user = NULL;
	rid = sceNetResolverCreate("resolver", &rparam, 0);
	if (rid < 0) {
		Errorf("sceNetResolverCreate() failed (0x%x errno=%d)",
			rid, sce_net_errno);
		return (int)rid;
	}
	ret = sceNetResolverStartNtoa(rid, hostname, addr, 0, 0, 0);
	if (ret < 0) {
		Errorf("sceNetResolverStartNtoa() failed (0x%x errno=%d)",
			ret, sce_net_errno);
		sceNetResolverDestroy(rid);
		return ret;
	}
	ret = sceNetResolverDestroy(rid);
	if (ret < 0) {
		Errorf("sceNetResolverDestroy() failed (0x%x errno=%d)",
			rid, sce_net_errno);
		return ret;
	}
	return ret;
} */

fiHandle fiDeviceTcpIp::Connect(const char * address, int port) {
	SOCKET s;

#if RSG_ORBIS
	SceNetSockaddrIn sa	= {0};
	sa.sin_len			= sizeof(sa);
	sa.sin_family       = SCE_NET_AF_INET;
	sa.sin_addr.s_addr  = SCE_NET_INADDR_ANY;
	sa.sin_port			= sceNetHtons(port);
#else
	SceNetSockaddrIn sa = {0}; // implies SCE_NET_INADDR_ANY
	sa.sin_len = sizeof(sa);
	sa.sin_family = SCE_NET_AF_INET;
	sa.sin_port = sceNetHtons(port);
#endif

	if (sceNetInetPton(SCE_NET_AF_INET,address,&sa.sin_addr) <= 0
		/*&& doResolverNtoa(address,&sa.sin_addr) < 0*/ ) {
		Errorf("fiDeviceTcpIp::Connect - cannot resolve '%s'", address);
		return fiHandleInvalid;
	}

	if ((s = sceNetSocket(address, SCE_NET_AF_INET,SCE_NET_SOCK_STREAM,0)) < 0)
	{
		Errorf("sceNetSocket - %s (Addr: %s, Port: %d)", error_string(), address, port);
		return fiHandleInvalid;
	}

	if (sceNetConnect(s,(SceNetSockaddr*)&sa,sizeof(sa)) < 0) {
		Errorf("sceNetConnect - %s (Addr: %s, Port: %d)", error_string(), address, port);
		sceNetSocketClose(s);
		return fiHandleInvalid;
	}

	int noNagle = 1;
	socklen_t len = sizeof(int);
	if (sceNetSetsockopt(s, IPPROTO_TCP, TCP_NODELAY, &noNagle, len) < 0) {
		Errorf("sceNetConnect - %s (Addr: %s, Port: %d)", error_string(), address, port);
		sceNetSocketClose(s);
		return fiHandleInvalid;
	}

	return (fiHandle) s;
}


fiHandle fiDeviceTcpIp::Listen(int port,int maxIncoming,const char *ip) {
	SOCKET s;
#if RSG_ORBIS
	SceNetSockaddrIn sa	= {0};
	sa.sin_len			= sizeof(sa);
	sa.sin_family       = SCE_NET_AF_INET;
	sa.sin_addr.s_addr  = SCE_NET_INADDR_ANY;
	sa.sin_port			= sceNetHtons(port);
#else
	SceNetSockaddrIn sa = {0}; // implies SCE_NET_INADDR_ANY
	sa.sin_len = sizeof(sa);
	sa.sin_port = sceNetHtons(port);
#endif

	if ((s = sceNetSocket(ip,SCE_NET_AF_INET,SCE_NET_SOCK_STREAM,0)) < 0) {
		Errorf("sceNetConnect - %s",error_string());
		return fiHandleInvalid;
	}

	if (sceNetBind(s,(SceNetSockaddr*)&sa,sizeof(sa)) < 0) {
		Errorf("sceNetBind - %s",error_string());
		sceNetSocketClose(s);
		return fiHandleInvalid;
	}

	if (sceNetListen(s, maxIncoming) < 0) {
		sceNetSocketClose(s);
		return fiHandleInvalid;
	}

	return (fiHandle) s;
}


fiHandle fiDeviceTcpIp::Pickup(fiHandle listenSocket) {
	SceNetSockaddr isa;
	SceNetSocklen_t i;
	int c;

	i = sizeof(isa);
	if (sceNetGetsockname((int)listenSocket,(SceNetSockaddr*)&isa,&i) < 0)
		Errorf("sceNetGetsockname - %s",error_string());

	// Displayf("Waiting for client connection...");
	if ((c = sceNetAccept((int)listenSocket,(SceNetSockaddr*)&isa,&i)) < 0) {
		Errorf("sceNetAccept - %s",error_string());
		return fiHandleInvalid;
	}

	return (fiHandle) c;
}


int fiDeviceTcpIp::PrintNetStats()
{
	return 0;
}


}	// namespace rage

#endif	// __WIN32
