//
// file/archive.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_ARCHIVE_H
#define FILE_ARCHIVE_H

#include "file/asset.h"
#include "file/stream.h"
#include "vector/vector3.h"
#include "vector/quaternion.h"

namespace rage 
{

class fiArchive
{
public:
    fiArchive(fiStream* f, bool loading, u16 version) : m_Flags(0), m_Version(0), m_Stream(0)
    {
        FastAssert(f);
        m_Stream = f;
        m_Version = version;
        m_Flags |= loading ? Loading : 0;
    }

    bool IsLoading() const  
    {
        return (m_Flags & Loading) > 0;
    }

    bool IsStoring() const
    {
        return !IsLoading();
    }

    u16 GetVersion() const
    {
        return m_Version;
    }

    void operator&(unsigned char& c)
    {
        if(IsLoading())
            m_Stream->ReadByte(&c, 1);
        else
            m_Stream->WriteByte(&c, 1);
    }
    void operator&(char& c)
    {
        if(IsLoading())
            m_Stream->ReadByte(&c, 1);
        else
            m_Stream->WriteByte(&c, 1);
    }

    void operator&(unsigned short& i)
    {
        if(IsLoading())
            m_Stream->ReadShort(&i, 1);
        else
            m_Stream->WriteShort(&i, 1);
    }
    void operator&(short& i)
    {
        if(IsLoading())
            m_Stream->ReadShort(&i, 1);
        else
            m_Stream->WriteShort(&i, 1);
    }

    void operator&(unsigned int& i)
    {
        if(IsLoading())
            m_Stream->ReadInt(&i, 1);
        else
            m_Stream->WriteInt(&i, 1);
    }
    void operator&(int& i)
    {
        if(IsLoading())
            m_Stream->ReadInt(&i, 1);
        else
            m_Stream->WriteInt(&i, 1);
    }

    void operator&(float& f)
    {
        if(IsLoading())
            m_Stream->ReadFloat(&f, 1);
        else
            m_Stream->WriteFloat(&f, 1);
    }

    void operator&(Vector3& v)
    {
        *this & v.x;
        *this & v.y;
        *this & v.z;
    }

    void operator&(Quaternion& q)
    {
        *this & q.x;
        *this & q.y;
        *this & q.z;
        *this & q.w;
    }

    /*
    PURPOSE
        Serialize arbitrary STL vectors.
    */
    template <class T>    
    void operator&(std::vector<T>& array)    
    {
        unsigned int size = array.size();
        *this & size;
        if (this->IsLoading())
        {
            array.resize(size);
        }

        for (unsigned int i = 0; i < size; ++i)
        {
            *this & array[i];
        }
    }

    /*
    PURPOSE
        Serialize arbitrary STL pairs.
    */
    template <class X, class Y>
    void operator&(std::pair<X, Y>& tuple)
    {
        *this & tuple.first;
        *this & tuple.second;
    }

    /*
    PURPOSE
        Default serialization operator that calls a user defined member 
        function.  If the compiler fails here, then your data could not
        be serialized by any of the functions above, so try writing a custom 
        function:
            void Serialize(rage::fiArchive&);
    */
    template <class T>
    void operator&(T& t)
    {
        t.Serialize(*this);
    }


private:
    enum
    {
        Loading = 0x01,
    };
    u16 m_Flags;
    u16 m_Version;
    fiStream* m_Stream;
};

} // namespace rage

#endif
