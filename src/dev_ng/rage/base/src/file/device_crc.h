// 
// file/device_relative.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_DEVICE_CRC_H
#define FILE_DEVICE_CRC_H

#include "atl/hashstring.h"
#include "atl/map.h"
#include "atl/binmap.h"
#include "file/device_relative.h"

namespace rage {

//
// name:		fiDeviceRelative
// description:	Specialised version of device that creates a Crc while loading a file
class fiDeviceCrc : public fiDeviceRelative
{
public:
	fiDeviceCrc() : m_crc(0),fiDeviceRelative() {}
	virtual ~fiDeviceCrc() { }

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int Close(fiHandle handle) const;
	
	static atBinaryMap<int, atFinalHashString>& GetFilenameCrcMap() {return sm_filenameCrcMap;}

	static bool		IsCRCTableConsistent(void);
	static void		RemoveCRCFromConsistencyCheck(u32 crc)			 { ms_crcAccumulator ^= crc;	}

	static bool		HasCRCTableCheckFailed(void) { return(ms_bCRCTableCheckFailed); }
private:
	mutable u32 m_crc;
	mutable atFinalHashString m_filenameHash;
	static atBinaryMap<int,atFinalHashString> sm_filenameCrcMap;

	static u32	ms_crcAccumulator;
	static bool ms_bCRCTableCheckFailed;
};

}	// namespace rage

#endif // !FILE_DEVICE_RELATIVE_H
