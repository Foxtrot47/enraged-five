//
// file/diskcache.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef FILE_DISKCACHE_H
#define FILE_DISKCACHE_H

#include "file/device.h"
#include "file/handle.h"
#include "atl/array.h"

namespace rage {

class fiDevice;
class fiPackfile;

#define ENABLE_GAMEDATA_FOR_DISKCACHE		(__PS3 && 0)

class fiCachedDevice: public fiDevice {
	friend class fiDiskCache;
public:
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return HARDDRIVE; }

	// RETURNS:	LSN of the request; will be relative to original location if the file isn't cached
	u32 GetBlockPhysicalSortKey(u64 offset,int count) const;

	// RETURNS: True if the data is already in the cache, or false if it wasn't (and schedules it to become resident)
	bool Prefetch(u64 offset,int count) const;

private:
	bool PrefetchCommon(u32 firstBlock,u32 lastBlock) const;
	void InvalidateCacheBlock(u32 errorBlock) const;

	// vptr
	const fiDevice *m_InnerDevice;

	mutable fiHandle m_InnerHandle;
	u32 m_SelfIndex;

	u64 m_InnerSize;
	u32 m_SortKey;
	char m_Name[32];
};



class fiDiskCache {
	friend class fiCachedDevice;
public:
	// PURPOSE:	Initialize the disk cache.
	// PARAMS:	cpu - which cpu to run the disk cache on (360 only)
	//			gameTitle - Human-readable game title (can be NULL for syscache)
	// NOTES:	If the disk cache is never initialized, or this function returns false,
	//			then GetCachedDevice will just fall through to fiDevice::GetDevice.
	//			If gameTitle is NULL, we'll use syscache instead of gamedata for caching.  
	//			gamedata is about 33% faster than syscache.
	// RETURNS:	True on success, false on some failure (with diagnostic sent to tty)
	static bool InitClass(int XENON_ONLY(cpu),const char *PS3_ONLY(gameTitle));

	// PURPOSE:	Shuts the disk cache back down again.
	static void ShutdownClass();

	// RETURNS:	Current holdoff time, how many milliseconds optical drive goes idle
	//			before we attempt to background copy.
	static int GetHoldoffTime();

	// PURPOSE:	Set the holdoff time.
	// PARAMS:	ms - New holdoff time, in milliseconds.  Default is 1000.
	static void SetHoldoffTime(int ms);

	// PURPOSE:	Obtain a cached device.
	// PARAMS:	fullPathName - Path to the inner device to be cached.
	//			pf - Pointer to the packfile we will be managing (partially constructed at this time)
	// RETURNS:	New cached device, or NULL if the cache was disabled or the original file wasn't accessible.
	// NOTES:	On a cached device, most operations are dummies or invalid.
	//			OpenBulk returns a valid handle that is otherwise ignored by
	//			all other functions.  ReadBulk performs the actual cache maintenance.
	static const fiCachedDevice* GetCachedDevice(const char *fullPathName,const fiPackfile *pf);

	// PURPOSE:	Determine if the cache is enabled.
	static bool IsCacheEnabled();

	// PURPOSE: Returns true if we either installed to hdd, or the cache was already present.
	//			It will return false if no cache is available, or it was regenerated.
	static bool IsCachePrimed();

	// PURPOSE:	Returns a timestamp of the last cache copy completion, or zero if we haven't done one yet.
	static unsigned GetLastCacheCopyCompletion() { return sm_LastCacheCopyCompletion; }

	// PURPOSE:	Returns true if a cache copy is currently in progress.
	static bool IsCacheCopying() { return sm_Copying != 0; }

	// PURPOSE: Higher-level code should call this when we're no longer reading load-ordered data.
	//			Cache misses incurred in boot mode have a higher priority than all other cache misses
	//			and will tend to be copied sooner.
	static void LeaveBootMode();

	// PURPOSE:	Returns a normalized float between 0.0 (no valid blocks) and 1.0 (all blocks in cache are valid).
	//			It's up to higher level code to know how to interpret this value.  It's guaranteed to be
	//			zero if the cache is disabled or unavailable, and will always return 1.0 on 360 "Play From Hard Drive"
	static float GetCacheOccupancy();

	// PURPOSE:	Returns a normalized float indicating the percent of the cache that is pending.  Once the
	//			cache fills up, Occupancy plus progress will add up to 1.0; the progress percentage is
	//			capped to prevent too much cache churn.
	static float GetCacheInProgress();

private:
	static bool CopyCacheSector(u32 dstBlock);
	static void CacheWorker(void*);
	static volatile unsigned sm_LastCacheCopyCompletion;
	static volatile unsigned sm_Copying;
};

}	// namespace rage


#endif
