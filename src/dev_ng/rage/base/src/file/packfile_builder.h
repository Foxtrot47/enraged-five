// 
// file/packfile_builder.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_PACKFILE_BUILDER_H
#define FILE_PACKFILE_BUILDER_H

#include "atl/array.h"
#include "file/stream.h"
#include "math/random.h"
#include "file/packfile.h"

namespace rage {

struct fiRpf7Entry 
{	// Needed during construction only
	fiRpf7Entry() : NameOffset(0), DataSize(0), Compressed(false), SourceName(NULL), Next(0), Child(0) { }
	~fiRpf7Entry() { 
		if (!DataSize)
			delete[] SourceName;
	}

	u32 NameOffset;
	u32 DataSize:31, Compressed:1;
	char *SourceName;		// NULL if directory.  Otherwise pointer to name if DataSize is 0, or actual data if DataSize is nonzero.
	u32 Next, Child;
};

class fiRpf7Builder 
{
public:
	fiRpf7Builder(int shift);
	~fiRpf7Builder();

	bool AddFile(const char *srcName, const char *destName);

	// Data is assumed to be allocated by rage_new, and will be freed when the archive is written.
	bool AddData(void *srcData,u32 dataSize,const char *destName,bool compressed);

	// NOTE - g_sysPlatform must be correct, and PARAM_aeskey needs to be correct
	// or contain the full output path so we can identify the right project's key to use.
	void Save(fiStream *dest);

private:
	void Insert(u32 * nodeRef,const char * name,const char *srcName,u32 dataSize,bool compressed);
	u32 BuildTree(u32 entry);
	void InitializeWrite( );
	void Write( ::rage::fiStream* output );
	void WritePadding( ::rage::fiStream* fp, u32 byteBoundary );
	void RandomFill(u8 *dest,u32 destSize);

	atFixedArray<fiRpf7Entry,16384> m_Nodes;		// limit is somewhat arbitrary but nameheap limit will be hit long before this
	::rage::fiPackEntry*	m_Entries;
	u32						m_nEntryCount;

	u32						m_nNameHeapSize;
	u32						m_nNameHeapMaxSize;
	char					*m_pNameHeap;		// sized per fiPackEntry bitfield allocation.
	int						m_nNameHeapShift;
	mthRandom				m_Random;
};


}

#endif
