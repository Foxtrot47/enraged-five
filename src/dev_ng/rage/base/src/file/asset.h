// 
// file/asset.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_ASSET_H
#define FILE_ASSET_H

#include "file/stream.h"
#include "file/limits.h"

namespace rage {

struct fiFindData;

/*
	fiAssetManager defines the notion of an asset root directory and a current
	asset folder.  The folders can be stacked up to eight levels deep.  The class
	acts as a virtual class factory for fiStream objects, applying several rules
	for inserting an implicit leading path and a file extension.  See the documentation
	of PushFolder and FullPath for details of the rules.
<FLAG Component>
*/
class fiAssetManager {
public:
	// Maximum length of the filename part of the filename on all supported platforms.
	// Note that the entire pathname can be longer, this is just the longest allowed
	// on any one segment of it.  Limit is currently tied to Xbox FATX limit.
	static const int MAX_FILENAME_LENGTH = 42;

	// PURPOSE:	Constructor
	fiAssetManager();

	// PURPOSE:	Destructor
	~fiAssetManager();

	// PURPOSE:	Sets the asset root path.
	// PARAMS:	path - New fully-qualified asset root path.
	// NOTES:	In general, you should point SetPath at the root of your game asset
	//			directory tree and use PushFolder and PopFolder to change your current
	//			working directory.  You can pass multiple locations in a single string
	//			by separating them with semicolons; they will be searched in order in
	//			any Open call.  All Create calls use only the first element listed by
	//			default, although you can switch to a different element by putting an
	//			'*' sign before the path: for example, -path=game:\assets;*t:\game\assets.
	void SetPath(const char *path);

	// RETURNS:	Current asset root path (index based at zero)
	inline const char *GetPath(int pathIndex = 0);

	// RETURNS:	Current asset root path plus Pushed entries (if any)
	void GetStackedPath(char *dest,int maxLen,int pathIndex = 0);

	// RETURNS: Number of paths
	inline int GetPathCount() const;

	// PURPOSE:	Returns a fully-qualified pathname based on the input base name,
	//			input extension, current asset root, and current asset folder.
	// PARAMS:	outDest - Destination buffer to receive fully-qualified filename
	//			maxLen - sizeof(outDest)
	//			base - base name of file
	//			ext - extension for file.
	//			pathIndex - Which path (starting from zero) to use
	// NOTES:	If <I>base</I> begins with a drive letter, a slash, or "..", it is
	//			treated as an absolute path and does not have the asset root
	//			or current asset folder inserted before it; otherwise, if <I>base</I> begins 
	//			with '$' it has the asset root inserted before it but not the current
	//			asset folder.  Otherwise, if it is not an absolute path and does not
	//			begin with '$', both the asset root and current asset folder are inserted.
	//			Finally, if <I>ext</I> is not an empty string and <I>base</I> does not
	//			already have an extension, or the extension it has does not match <I>ext</I>,
	//			<I>ext</I> is appended to the resulting filename.
	//			If the asset root is "c:/assets" and the topmost folder is "textures" then:
	//			* FullPath(...,"image","dds") will be "c:/assets/textures/image.dds".
	//			* FullPath(...,"image.dds","dds") will be "c:/assets/textures/image.dds".
	//			* FullPath(...,"c:/whatever/image.dds","") will be "c:/whatever/image.dds".
	//			* FullPath(...,"$/tuning.txt","txt") will be "c:/assets/tuning.txt".
	void FullPath(char *outDest,int maxLen,const char *base,const char *ext,int pathIndex = 0);

	// PURPOSE:	Returns a fully-qualified pathname based on the input base name,
	//			input extension, current asset root, and current asset folder.
	//			Will search through all registered paths in order for the first match.
	// PARAMS:	outDest - Destination buffer to receive fully-qualified filename
	//			maxLen - sizeof(outDest)
	//			base - base name of file
	//			ext - extension for file.
	// RETURNS:	True if the file could be found.
	// NOTES:	See FullPath notes for details on exact algorithm.
	bool FullReadPath(char *outDest,int maxLen,const char *base,const char *ext);

	// PURPOSE:	Returns a fully-qualified pathname based on the input base name,
	//			input extension, current asset root, and current asset folder.
	//			Always uses the current write path (which is usually the first element,
	//			but that can be changed via the '*' special marker).
	// PARAMS:	outDest - Destination buffer to receive fully-qualified filename
	//			maxLen - sizeof(outDest)
	//			base - base name of file
	//			ext - extension for file.
	// NOTES:	See FullPath notes for details on exact algorithm.
	void FullWritePath(char *outDest,int maxLen,const char *base,const char *ext) {
		FullPath(outDest,maxLen,base,ext,m_WritePath);
	}

	// PURPOSE:	Returns the index of the path that contains the file given.
	//			Give this index to FullPath so that the appropriate full path is
	//			used, when there is more than one.
	// PARAMS:	base - base name of file
	//			ext - extension for file
	//          printFailures (optional)
	//            - Outputs warnings giving the locations where a file was 
	//              looked for but not found
	// RETURNS:	-1 = file not found in any paths
	//			0+ = the index of the path that the file was found in
	// NOTES:	When multiple paths are used by the game ("-path=T:\rdr2\game;*T:\rdr2\assets")
	//			FullPath only uses the first one.  This is because FullPath has
	//			no way of knowing which one is correct (Open() in contrast scans
	//			them all because it knows exactly what file it's looking for).
	//			This is a huge problem when game code tries to expand a relative
	//			path before searching for files.  And while FullPath lets the
	//			calling code ask for a particular path index, the game may not
	//			know (for example, FILE A doesn't know which zips
	//			were mounted by FILE B, and even if it does, it doesn't know what
	//			internal "index" code this class is using for them).
	//			Using this function, game code can determine that index code by
	//			passing a filename known to exist in the archive.
	int FindPath(const char* base, const char* ext, bool printFailures=false);

	// PURPOSE:	Pushes new folder onto current asset folder stack
	// PARAMS:	folder - New folder
	// NOTES:	If folder begins with '$', it is treated as being relative to
	//			the path specified by SetPath.  Otherwise, it is treated as being
	//			relative to the next folder up the stack (or the empty string if
	//			the folder stack is empty).  The stack is currently a maximum of
	//			eight levels deep and will assert if you overflow it.
	void PushFolder(const char *folder);

	// PURPOSE:	Pops the last folder off of the current asset folder stack.
	// NOTES:	Will assert out on stack underflow.
	void PopFolder();

	// PURPOSE:	Attempts to open a file
	// PARAMS:	base - Base name of file as per FullPath
	//			ext - Extension of file as per FullPath
	//			probeOnly - If false (the default), report missing files to tty.
	//			readOnly - If true (the default), open file for read-only access.
	// RETURNS:	Pointer to stream or NULL on failure.
	// NOTES:	Tries all known path entries in order listed.
	fiStream *Open(const char *base,const char *ext,bool probeOnly = false,bool readOnly = true);

	// PURPOSE:	Attempts to create a file
	// PARAMS:	base - Base name of file as per FullPath
	//			ext - Extension of file as per FullPath
	//			probeOnly - If false (the default), report missing files to tty.
	// RETURNS:	Pointer to stream or NULL on failure.
	// NOTES:	Only uses the "write path", which is normally the first one listed
	//			in the SetPath call, but this can be changed by putting '*' in front
	//			of the path element you want write paths to go to.
	fiStream *Create(const char *base,const char *ext,bool probeOnly = false);

	// PURPOSE:	Attempts to determine if a file exists
	// PARAMS:	base - Base name of file as per FullPath
	//			ext - Extension of file as per FullPath
	// RETURNS:	True if file exists.
	// NOTES:	This function internally calls FullReadPath, which doesn't actually
	//			open the file; instead, it just checks that it can successfully retrieve
	//			file attributes.
	bool Exists(const char *base,const char *ext);

	// PURPOSE:	Utility function which returns the filename portion of a fully-qualified name.
	// PARAMS:	name - filename to scan
	// RETURNS:	Pointer to filename portion of the name; this is always within <I>name</I> itself
	//			and is simply the first character after the last slash in the string.
	static const char *FileName(const char * name);

	// PURPOSE:	Utility function which returns the base name of a fully-qualified name.
	// PARAMS:	outBase - Output buffer
	//			bufSize - sizeof(outBase)
	//			name - Input name
	// NOTES:	Simply copies <I>name</I> up to the first '.' character, so it doesn't deal with
	//			relative pathnames the way you'd expect.
	static void BaseName(char *outBase,int bufSize,const char * name);

	// PURPOSE:	Creates any intermediate directories necessary for 'filename'
	// PARAMS:	filename - Path to create (last part is ignored)
	// NOTES:	CreateLeadingPath("$/some/path/name") will create directory
	//			some/path, NOT some/path/name.  This is so that you can pass the
	//			same name to ASSET.Create immediately after calling this function.
	// RETURNS:	True on success
	bool CreateLeadingPath(const char *filename);

	// PURPOSE:	Enumerate all files in a given directory through a user-supplied callback
	// PARAMS:	directory - Directory to enumerate
	//			callback - Callback function to invoke
	//			userArg - closure for user callback
	//			bCheckAllExtraMountDevices - extra mount points are those whose name begins with "dlc".
	//											An extra mount point can have more than one device associated with it.
	//											Call this function with bCheckAllExtraMountDevices=true if you want all 
	//												devices of an extra mount point to be checked for files.
	//											The default behavior (bCheckAllExtraMountDevices=false) will only check the device 
	//												that was last added to the mount point.
	// RETURNS:	Total number of files enumerated (ie number of times callback was invoked)
	// NOTES:	You will need to #include "file/device.h" to get the definition of fiFindData.
	//			We don't include it here to reduce dependencies.
	int EnumFiles(const char *directory,void (*callback)(const fiFindData &data,void *userArg),void *userArg, bool bCheckAllExtraMountDevices=false);

	// PURPOSE:	Removes the extension from the specified filename if present.  This function
	//			is more robust that just using strrchr because it stops at the first slash
	//			character so it doesn't screw up relative pathnames.
	// PARAMS:	dest - Output buffer
	//			destSize - sizeof(dest)
	//			src - Source filename
	// NOTES:	dest and src can be the same.
	static void RemoveExtensionFromPath(char *dest,int destSize,const char *src);

	//
	// PURPOSE
	//	Finds file extension in the specified filename.
	// PARAMS
	//	path - the path to search for the file extension
	// RETURNS
	//	NULL if the extension isn't found, otherwise a pointer to the extension including the '.'.
	//
	static const char* FindExtensionInPath(const char* path);

	// PURPOSE:	Removes the last name in the path (and the resulting trailing slash or backslash)
	// PARAMS:	dest - Output buffer
	//			destSize - sizeof(dest)
	//			src - Source filename
	// NOTES:	dest and src can be the same.
	static void RemoveNameFromPath(char *dest,int destSize,const char *src);

	// PURPOSE:	Inserts a path element between the path part (if present) and the filename part.
	// PARAMS:	inoutBuffer - Buffer containing the path
	//			bufferSize - Size of the buffer
	//			insert - String to insert
	//			insertAfter - place in inoutBuffer after which insert will be added (NULL means after last '/')
	// RETURNS:	False if name would become too long, else true on success
	// NOTES:	The input buffer should already be normalized to forward slashes (ie probably the
	//			output of ASSET.FullPath) and the insert string should not start or end with a
	//			slash)
	static bool InsertPathElement(char *inoutBuffer,int bufferSize,const char *insert,const char* insertAfter=NULL);

	// PURPOSE:	Retrieve file attributes of an asset
	// PARAMS:	name - Base name (as per FullPath)
	//			ext - Extension (as per FullPath)
	// RETURNS:	FILE_ATTRIBUTE_INVALID on failure, or current file attributes.
	u32 GetAttributes(const char *name,const char *ext);

	// PURPOSE:	Set file attributes of an asset
	// PARAMS:	name - Base name (as per FullPath)
	//			ext - Extension (as per FullPath)
	//			attributes - New attributes to set on file
	// RETURNS:	True on success, else false.
	bool SetAttributes(const char *name,const char *ext,u32 attributes);

	// PURPOSE:	Clear specific attributes of an asset
	// PARAMS:	name - Base name (as per FullPath)
	//			ext - Extension (as per FullPath)
	//			attrToClear - Attributes to clear on file
	// RETURNS:	True on success, else false.
	bool ClearAttributes(const char *name,const char *ext,u32 attrToClear);

	// PURPOSE: Returns true if the path name is an absolute path 
	// PARAMS: path - Path to a file or directory
	// RETURNS: True if the path name is absolute
	bool IsAbsolutePath(const char* path);

	// PURPOSE: Returns a relative path to an existing file based on the current path roots (if possible)
	// PARAMS:	dest - Output buffer
	//			destSize - sizeof(dest)
	//			src - Source filename
	// RETURNS: True if the result path is relative
	// NOTES:	This function will assert if the result of MakeRelative would actually point
	//			to a different file than the input. For example your path was "T:/dir/;N:/dir/"
	//			both roots had a file named file.file and you called MakeRelative with "N:/dir/file.file"
	bool MakeRelative(char* dest, int destSize, const char* src);

	// PURPOSE: Returns a folder relative path to an existing file based on the current path roots and pushed folders (if possible)
	// PARAMS:	dest - Output buffer
	//			destSize - sizeof(dest)
	//			src - Source filename
	// RETURNS: True if the result path is relative
	bool MakeFolderRelative(char* dest, int destSize, const char* src);

	// PURPOSE: Appends the "base.ext" to the dest buffer (unless base already ended with the right extensions) and normalizes slashes
	// PARAMS:	dest - Input and output buffer
	//			destSize - sizeof(dest)
	//			base - Source filename
	//			ext - the extension to append
	void AddExtension(char* dest,int maxLen,const char* base,const char *ext);

private:
	static const int c_MaxPaths = 4;
	char m_Paths[c_MaxPaths][RAGE_MAX_PATH];
	struct Entry {
		char folder[RAGE_MAX_PATH];
	};
#if __TOOL || __RESOURCECOMPILER
	enum { MaxEntries = 16 };
#else
	enum { MaxEntries = 8 };
#endif
	Entry m_Entries[MaxEntries];
	int m_SP;
	int m_PathCount;	// Number of path entires
	int m_WritePath;	// Index of the path entry where writes should go
	bool m_WritePathIsWriteOnly;	// If TRUE, the m_WritePath path is for write access ONLY
};

inline const char *fiAssetManager::GetPath(int pathIndex) { 
	FastAssert(pathIndex >= 0 && pathIndex < m_PathCount);
	return m_Paths[pathIndex];
}

inline int fiAssetManager::GetPathCount() const {
	return m_PathCount;
}

extern fiAssetManager ASSET;

}	// namespace rage

#endif
