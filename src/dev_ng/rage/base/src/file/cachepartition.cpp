//
// file/cachepartition.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "cachepartition.h"
#include "system/param.h"
#include "system/service.h"
#include "fwutil/Gen9Settings.h"

#if RSG_PC
#include "system/xtl.h"
#elif RSG_SCE
#include <libsysmodule.h>
#include <app_content.h>
#pragma comment(lib,"SceAppContent_stub_weak")
#pragma comment(lib,"SceSysmodule_stub_weak")
#endif

#if IS_GEN9_PLATFORM
#if RSG_GDK
#include <XPersistentLocalStorage.h>
#endif //RSG_GDK
#endif //IS_GEN9_PLATFORM

#include <time.h>

#include "file/device.h"
#include "file/limits.h"
#include "string/string.h"
#include "system/exec.h"
#include "system/nelem.h"

namespace rage {

NOSTRIP_PARAM(clearhddcache, "[file] Force console disk cache to be cleared", "Disabled", "All", "");

#if RSG_PC
char fiCachePartition::sm_CachePrefix[RAGE_MAX_PATH] = "";
#elif RSG_DURANGO
char fiCachePartition::sm_CachePrefix[RAGE_MAX_PATH] = "t:\\";
#elif RSG_SCE
char fiCachePartition::sm_CachePrefix[RAGE_MAX_PATH] = "";
#else
char fiCachePartition::sm_CachePrefix[] = "c:/cache_drive/";
#endif

int fiCachePartition::sm_RefCount;

char fiPersistentCachePartition::sm_CachePrefix[RAGE_MAX_PATH] = "";

unsigned fiCacheUtils::sm_FileCacheDurationDays = 60;

bool fiCachePartition::Init(bool forceClean)
{
	// If we have a reference, we know it already worked once.
	// Otherwise it's okay to let other subsystems fail their init as well.
	if (sm_RefCount) {
		++sm_RefCount;
		return true;
	}

#if RSG_SCE
	int ret = sceSysmoduleLoadModule(SCE_SYSMODULE_APP_CONTENT);
	if (SCE_OK!=ret)
		Quitf("Failed to load appcontent");

	// sceAppContentInitialize is called in appcontent.cpp so not required here

	SceAppContentMountPoint mountPoint;
	ret = sceAppContentTemporaryDataMount2( (forceClean || PARAM_clearhddcache.Get()) ?SCE_APP_CONTENT_TEMPORARY_DATA_OPTION_FORMAT:SCE_APP_CONTENT_TEMPORARY_DATA_OPTION_NONE, &mountPoint );
	if (SCE_OK==ret)
	{
		Displayf("Temporary data mount succeeded. Using: %s", mountPoint.data);
		safecpy(sm_CachePrefix, mountPoint.data);
		safecat(sm_CachePrefix,"/");
	}
	else
	{
#if !__FINAL
		safecpy(sm_CachePrefix, "/data/");
		Assertf(ret==SCE_OK,"Temporary data mount failed. [0x%08X]  Using alternate path for development only: %s",ret,sm_CachePrefix);
#else
		Quitf("Failed to initialize cache volume");
#endif	// !__FINAL
	}
	++sm_RefCount;
	return true;
#elif RSG_PC
	++sm_RefCount;
	(void)forceClean;
	
	wchar_t pathW[RAGE_MAX_PATH] = {0};
	DWORD dwRetVal = GetTempPathW(COUNTOF(pathW), pathW);
	if((dwRetVal > COUNTOF(pathW)) || (dwRetVal == 0))
	{
		sm_CachePrefix[0] = '\0';
		return false;
	}

	int result = WideCharToMultiByte(CP_UTF8, 0, pathW, -1, sm_CachePrefix, COUNTOF(sm_CachePrefix), 0, 0);
	if(result == 0)
	{
		sm_CachePrefix[0] = '\0';
		return false;
	}

	return true;
#elif RSG_DURANGO
	// Use temp volume available as T:\ drive
	// 2 GB fixed size
	// Content on this drive is hidden from other titles
	// use xbdir xt:\ /x/title to view via cmd line
	++sm_RefCount;
	(void) forceClean;

	fiDevice::GetDevice(sm_CachePrefix);
	return true;
#else
	(void)forceClean;
	return false;
#endif
}

bool fiCachePartition::Reset()
{
	if (sm_RefCount != 1) {
		Errorf("Cannot reset cache partition when there's more than one client!");
	}
	return false;
}

void fiCachePartition::Shutdown()
{
}

void fiCachePartition::DeleteCacheFiles()
{
	fiDevice::DeleteDirectory(sm_CachePrefix, true);
}

//#################################################################################

void fiPersistentCachePartition::Init(const char* CUSTOM_PERSISTENT_CACHE_ONLY(folderName))
{
#if CUSTOM_PERSISTENT_CACHE
	if (!Verifyf(sm_CachePrefix[0] == 0, "Init has been called twice"))
	{
		return;
	}

	const char* basePath = GetPersistentStorageRoot();

	if (basePath != nullptr && basePath[0] != 0)
	{
		formatf(sm_CachePrefix, "%s%s", basePath, folderName ? folderName : "");

		CreateDir();
	}
	else
	{
		Errorf("GetPersistentStorageRoot is invalid"); //TODO_ANID: No verify assert yet, Scarlett doesn't fully work yet
	}
#endif
}

void fiPersistentCachePartition::Shutdown()
{
	sm_CachePrefix[0] = 0;
}

const char* fiPersistentCachePartition::GetCachePrefix()
{
	Assert(fiCachePartition::IsAvailable());

#if CUSTOM_PERSISTENT_CACHE
	return sm_CachePrefix[0] != 0 ? sm_CachePrefix : fiCachePartition::GetCachePrefix();
#else
	return fiCachePartition::GetCachePrefix();
#endif
}

void fiPersistentCachePartition::DeleteCacheFiles()
{
#if CUSTOM_PERSISTENT_CACHE
	if (!Verifyf(sm_CachePrefix[0] != 0, "Invalid path"))
	{
		return;
	}

	fiDevice::DeleteDirectory(sm_CachePrefix, true);

	CreateDir();
#endif //CUSTOM_PERSISTENT_CACHE
}

void fiPersistentCachePartition::DeleteOldFiles()
{
#if CUSTOM_PERSISTENT_CACHE
	if (sm_CachePrefix[0] == 0)
	{
		Warningf("DeleteOldFiles skipped. Path not set.");
		return;
	}
	
	fiCacheUtils::ListFiles(sm_CachePrefix, fiCacheUtils::DeleteOldFileFunc);
#endif //CUSTOM_PERSISTENT_CACHE
}

bool fiPersistentCachePartition::CreateDir()
{
	if (!Verifyf(sm_CachePrefix[0] != 0, "Invalid path"))
	{
		return false;
	}

	bool created = false;

	const fiDevice* device = fiDevice::GetDevice(sm_CachePrefix);
	if (device)
	{
		created = device->MakeDirectory(sm_CachePrefix);
		// Trying to create an existing folder returns false so no assert here
	}

	return created;
}

const char* fiPersistentCachePartition::GetPersistentStorageRoot()
{
#if IS_GEN9_PLATFORM
#if RSG_GDK
	static char s_PersistentPath[RAGE_MAX_PATH] = { 0 };
	static bool s_PathRetrieved = false;

	if (!s_PathRetrieved)
	{
		s_PathRetrieved = true;

		const HRESULT hr = XPersistentLocalStorageGetPath(sizeof(s_PersistentPath), s_PersistentPath, nullptr);

		if (!SUCCEEDED(hr))
		{
			Errorf("Failed to get persistent storage space path - Error: 0x%x!", hr);
		}
	}

	return s_PersistentPath;
#endif //RSG_GDK
#endif //IS_GEN9_PLATFORM


#if RSG_DURANGO
	return g_SysService.GetPersistentStorageRoot();
#elif RSG_SCE
	return "/download0/";
#else
	return nullptr;
#endif
}

//#################################################################################

bool fiCacheUtils::ListFiles(const char* pathname, fiListFileFunc func)
{
	const fiDevice* dev = fiDevice::GetDevice(pathname);
	if (dev == nullptr)
	{
		return false;
	}

	bool result = true;
	fiFindData data;
	fiHandle handle = dev->FindFileBegin(pathname, data);
	if (!fiIsValidHandle(handle))
	{
		return false;
	}

	// Avoid a double / in the path
	const int len = istrlen(pathname);
	const char* format = (len == 0 || pathname[len - 1] != '/') ? "%s/%s" : "%s%s";

	while (result)
	{
		char currentPath[RAGE_MAX_PATH];
		if ((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
		{
			formatf(currentPath, format, pathname, data.m_Name);

			// Found a directory, so deal with it
			if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
			{
				if (!ListFiles(currentPath, func))
				{
					dev->FindFileEnd(handle);
					return false;
				}
			}

			func(currentPath, handle, data);
		}

		result = dev->FindFileNext(handle, data);
	}

	dev->FindFileEnd(handle);

	return true;
}

void fiCacheUtils::PrintFileFunc(const char* OUTPUT_ONLY(path), fiHandle UNUSED_PARAM(handle), const fiFindData& OUTPUT_ONLY(data))
{
#if !__NO_OUTPUT
	const bool isDir = (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY;

	fiDevice::SystemTime t;
	fiDevice::ConvertFileTimeToSystemTime(data.m_LastWriteTime, t);

	char timeBuffer[64];
	formatf(timeBuffer, "%02u/%02u/%04u  %02u:%02u", t.wDay, t.wMonth, t.wYear, t.wHour, t.wMinute);

	char buffer[1048];
	formatf(buffer, "%16s  %6s  %8u KiB  %s", timeBuffer, isDir ? "<DIR>" : "", static_cast<unsigned>(data.m_Size / 1024), path);

	const int len = static_cast<int>(strlen(buffer));

	diagPrintLn(buffer, len);

#endif //!__NO_OUTPUT
}

void fiCacheUtils::DeleteOldFileFunc(const char* path, fiHandle UNUSED_PARAM(handle), const fiFindData& data)
{
	if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
	{
		return;
	}

	fiDevice::SystemTime timeNow;
	fiDevice::GetSystemTimeUtc(timeNow);

	fiDevice::SystemTime timeFil;
	fiDevice::ConvertFileTimeToSystemTime(data.m_LastWriteTime, timeFil);

	// Precise enough. If the time is in the future we'll delete it too.
	const u64 daysNow  = timeNow.wDay + ((timeNow.wMonth - 1) + timeNow.wYear * 12) * 31;
	const u64 daysFile = timeFil.wDay + ((timeFil.wMonth - 1) + timeFil.wYear * 12) * 31;
	const u64 daysDiff = daysNow - daysFile;

	if (sm_FileCacheDurationDays == 0 || daysDiff <= static_cast<u64>(sm_FileCacheDurationDays))
	{
		return;
	}

	const fiDevice* dev = fiDevice::GetDevice(path);

	if (dev)
	{
		Displayf("Deleting %s. %" I64FMT "u days old", path, daysDiff);

		ASSERT_ONLY(bool ok = ) dev->Delete(path);
		Assertf(ok, "Deleting [%s] failed", path);
	}
}

void fiCacheUtils::SetFileCacheDurationDays(const unsigned days)
{
	sm_FileCacheDurationDays = days;
}

}	// namespace rage
