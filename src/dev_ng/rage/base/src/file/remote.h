// 
// file/remote.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FILE_REMOTE_H
#define FILE_REMOTE_H

#include "file/device.h"
#include "string/string.h"
#include "atl/delegate.h"

namespace rage {

#if __WIN32
#pragma warning(disable: 4514)	// Unreferenced inline removed
#endif

#define __ENABLE_RFS	(!__FINAL || __FINAL_LOGGING)

#if __ENABLE_RFS
	
typedef atDelegate<bool(const char* ip)> ShouldAcceptRemoteConnectionFunc;

// PURPOSE:	Clears the delegate for determining whether to accept a connection from an IP address.
void fiRemoteClearAcceptConnectionDelegate();

// PURPOSE:	Sets the delegate for determining whether to accept a connection from an IP address.
// PARAMS:	fn - The delegate to set.
void fiRemoteSetAcceptConnectionDelegate(const ShouldAcceptRemoteConnectionFunc& fn);

// PURPOSE:	Install a remote file server on specified port
// PARAMS:	port - Port number to install
//			ip - ip address to install on (or NULL for default)
// NOTES:	This is generally running on a PC.
void fiRemoteServer(int port,const char *ip);

// PURPOSE:	Reset all open files; intended only for use by rfs itself.
void fiRemoteResetFiles();

// PURPOSE:	Indicates whether rfs is properly installed or not; valid after fiDeviceRemote::InitClass is called.
extern bool fiRemoteServerIsRunning;

// PURPOSE: Indicates whether a message box is currently being displayed or not (useful for avoiding timeouts)
extern volatile unsigned int fiIsShowingMessageBox;

// PURPOSE: Shows a message box on the remote PC (as per Win32 API MessageBox call)
// PARAMS:	message - Contents of the message
//			title - Title of the message box
//			flags - MB_... flags as per Win32 API
//			defaultAnswer - Answer to return if the message box could not or should not be displayed
// RETURNS:	As per Win32 API (generally one of the MB_... button defines)
unsigned fiRemoteShowMessageBox(const char *message,const char *title,unsigned flags,unsigned defaultAnswer,unsigned options=0);

// PURPOSE: Shows a message box with the option of logging a bug on the remote PC (as per Win32 API MessageBox call)
// PARAMS:	message - Contents of the message
//			title - Title of the message box
//			flags - MB_... flags as per Win32 API
//			defaultAnswer - Answer to return if the message box could not or should not be displayed
// RETURNS:	As per Win32 API (generally one of the MB_... button defines)
unsigned fiRemoteShowAssertMessageBox(const char *message,const char *title,unsigned flags,unsigned defaultAnswer);

// PURPOSE: Return the number of frames since a message box was shown the last time.
unsigned fiGetFramesSinceLastMessageBox();

// PURPOSE:	Invoke a process; use sysExec instead of this directly.
int fiRemoteExec(const char *commandLine);

// PURPOSE:	Get env var; use sysGetEnv instead of this directly.
int fiRemoteGetEnv(const char *env,char *dest,int destSize);

// PURPOSE: Send remote tty. 
int fiRemoteTty(const char* msg);

// Flags used by fiRemoteShowMessageBox on all platforms.  Taken from Win32 SDK.
#define MB_OK				0x00000000L		// Ok button only
#define MB_OKCANCEL			0x00000001L		// Ok and cancel buttons
#define MB_ABORTRETRYIGNORE	0x00000002L		// Abort / Retry / Ignore buttons
#define MB_YESNOCANCEL		0x00000003L		// Yes / No / Cancel buttons
#define MB_YESNO			0x00000004L		// Yes / No buttons
#define MB_RETRYCANCEL      0x00000005L
#define MB_CANCELTRYCONTINUE 0x00000006L
#define MB_XMESSAGEBOX		0x0000000FL		// If this is specified, list one or more \n-terminated buttons (up to four) followed by \r prior to your actual message box text (not the caption).  Result will be 1 for button 1, 2, etc.
#define MB_ICONHAND			0x00000010L		// Display hand icon
#define MB_ICONERROR		MB_ICONHAND		// Display hand icon
#define MB_ICONQUESTION		0x00000020L		// Display question icon
#define MB_ICONASTERISK		0x00000040L		// Display asterisk icon
#define MB_ICONINFORMATION	MB_ICONASTERISK
#define MB_DEFBUTTON1		0x00000000L
#define MB_DEFBUTTON2		0x00000100L
#define MB_DEFBUTTON3		0x00000200L
#define MB_DEFBUTTON4		0x00000300L
#define MB_APPLMODAL		0x00000000L
#define MB_SYSTEMMODAL		0x00001000L
#define MB_TASKMODAL		0x00002000L
#define MB_HELP				0x00004000L // Help Button
#define MB_SETFOREGROUND	0x00010000L		// Force to foreground window
#define MB_TOPMOST			0x00040000L		// Force to topmost window
#define MB_NOFOCUS			0x00008000L
#define MB_SETFOREGROUND	0x00010000L
#define MB_DEFAULT_DESKTOP_ONLY	0x00020000L
#define MB_RIGHT			0x00080000L

// Return values of fiRemoteShowMessageBox indicating which button was pressed.
#define IDOK				1		// Ok
#define IDCANCEL			2		// Cancel
#define IDABORT				3		// Abort
#define IDRETRY				4		// Retry
#define IDIGNORE			5		// Ignore
#define IDYES				6		// Yes
#define IDNO				7		// No
#define IDTRYAGAIN			10
#define IDCONTINUE			11

// following 4 ids MUST be sequential
#define IDCUSTOM1			23
#define IDCUSTOM2			24
#define IDCUSTOM3			25
#define IDCUSTOM4			26

// Optional parameters for message box
#define MBO_TIMEOUT			0x000000FFL	// 255 seconds max
#define MBO_ASSERTKEYBOARD	0x00000100L	// Enable A/R/I keyboard shortcuts
// add other options here...

#endif // __ENABLE_RFS

}	// namespace rage

#endif
