// 
// file/serialize.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "serialize.h"

#include "stream.h"
#include "token.h"

static const int BINARY = 26;

// Version History:
// 1 -- First RAGE release -- In ascii mode, simple behavior for strings, and relied on higher code to
//		delimit text.
// 2 -- Relying on higher level code was bad, so serialized text not ending in whitespace will now append 
//		a space character.  Also, any strings containing white- and non-whitespaced characters will be
//		delimited by quotes (") to ensure complete string is retrieved from tokenizer.  All of this valid
//		only for ascii files.
static const int FILE_VERSION = 2;

using namespace rage;

inline bool IsWhiteSpace(char c) {
	return (c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\f' || c == '\v');
}

fiSerialize::fiSerialize(fiStream *stream, bool read, bool binary) : datSerialize(), m_Version(FILE_VERSION) {
	// Handle read cases
	const int version = m_Version;
	m_Stream = stream;
	if ( read ) {
		SetRead();
		
		int c = stream->GetCh();
		if ( c == BINARY ) {
			SetBinary();
		}
		else {
			m_Token.Init(stream->GetName(), stream);
			// Ignore the "serialize_vers:" label
			m_Token.IgnoreToken();
		}		
		// Read the version
		int fileVersion;
		Serialize(fileVersion);
		AssertMsg(fileVersion <= version, "Not able to read serialized file version");
		m_Version = fileVersion;
	}
	else {
		// Write case
		if ( binary ) {
			SetBinary();
			stream->PutCh(BINARY);
		}
		else {
			m_Token.Init(stream->GetName(), stream);
			m_Token.Put("serialize_vers: ");
		}
		int fileVersion = version;
		Serialize(fileVersion);
		if ( !binary ) {
			// Add a newline
			m_Token.PutStr("\r\n\r\n");
		}
	}
}

fiSerialize::~fiSerialize() {
	/* EMPTY */
}

void fiSerialize::Serialize(bool &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			val = (m_Stream->GetCh() == 1);
		}
		else {
			val = (m_Token.GetByte() == 1);
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->PutCh(val ? 1 : 0);
		}
		else {
			m_Token.PutByte(val ? 1 : 0);
		}
	}
}

void fiSerialize::Serialize(u8 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadByte(&val, 1);
		}
		else {
			val = (u8) m_Token.GetByte();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteByte(&val, 1);
		}
		else {
			m_Token.PutByte(val);
		}
	}
}

void fiSerialize::Serialize(s8 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadByte((u8*) &val, 1);
		}
		else {
			val = (s8) m_Token.GetByte();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteByte((u8*) &val, 1);
		}
		else {
			m_Token.PutByte(val);
		}
	}
}

void fiSerialize::Serialize(u16 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadShort(&val, 1);
		}
		else {
			val = (u16) m_Token.GetShort();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteShort(&val, 1);
		}
		else {
			m_Token.PutShort(val);
		}
	}
}

void fiSerialize::Serialize(s16 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadShort(&val, 1);
		}
		else {
			val = (s16) m_Token.GetShort();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteShort(&val, 1);
		}
		else {
			m_Token.PutShort(val);
		}
	}
}

void fiSerialize::Serialize(u32 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadInt(&val, 1);
		}
		else {
			val = m_Token.GetInt();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteInt(&val, 1);
		}
		else {
			m_Token.Put((int) val);
		}
	}
}

void fiSerialize::Serialize(s32 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadInt(&val, 1);
		}
		else {
			val = m_Token.GetInt();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteInt(&val, 1);
		}
		else {
			m_Token.Put(val);
		}
	}
}

void fiSerialize::Serialize(f32 &val) {
	if ( IsRead() ) {
		if ( IsBinary() ) {
			m_Stream->ReadFloat(&val, 1);
		}
		else {
			val = m_Token.GetFloat();
		}
	}
	else {
		if ( IsBinary() ) {
			m_Stream->WriteFloat(&val, 1);
		}
		else {
			m_Token.Put(val);
		}
	}
}

void fiSerialize::Serialize(u64 &/*val*/) {
	AssertMsg(0, "Need stream/token support for 64 bit values to maintain endian-ness");
}

void fiSerialize::Put(const char *txt) {
	Assert(IsRead() == false);
	if ( IsBinary() ) {
		s32 len = (int) strlen(txt) + 1;
		m_Stream->WriteInt(&len, 1);
		m_Stream->Write(txt, len);
	}
	else {
		bool forceQuotes = false;
		if ( m_Version >= 2 ) {
			// See if there is any whitespace/non-whitespace mixture in this text
			// and surround with quotes if so
			int idx = 0;
			bool whitespace = false;
			bool realText = false;
			while(txt[idx] != '\0' && (whitespace == false || realText == false)) {
				bool ws = IsWhiteSpace(txt[idx++]);
				if ( ws ) {
					whitespace = true;
				}
				else {
					realText = true;
				}
			}
			if ( whitespace && realText ) {
				forceQuotes = true;
				m_Token.Put("\"");
				AssertMsg(strchr(txt, '"') == 0, "ASCII text can not contain quotes character & whitespace" );
			}
		}
		m_Token.Put(txt);
		// See if we need to force quotes or whitespace
		int len = (int) strlen(txt);
		if ( m_Version >=2 && forceQuotes ) {
			m_Token.Put("\" ");
		}
		else if ( len && !IsWhiteSpace(txt[len-1]) ) {
			m_Token.Put(" ");
		}
	}
}

void fiSerialize::Get(char *outString, int size) {
	Assert(IsRead());
	if ( IsBinary() ) {
		s32 count;
		m_Stream->ReadInt(&count, 1);
		int readCnt = size < count ? size : count;
		m_Stream->Read(outString, readCnt);
		outString[readCnt-1] = '\0';
		while (readCnt++ < count) {
			m_Stream->GetCh();
		}
	}
	else {
		m_Token.GetToken(outString, size);
	}
}

