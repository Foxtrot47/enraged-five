// 
// file/device_installer.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "device_installer.h"
#include "device_installer_parser.h"

#include "diag/output.h"
#include "file/asset.h"
#include "file/cachepartition.h"
#include "math/amath.h"
#include "paging/streamer.h"
#include "paging/streamer_internal.h"
#include "string/string.h"
#include "system/criticalsection.h"
#include "system/memops.h"
#include "system/param.h"
#include "system/timer.h"

#include "tcpip.h"



#define SKIP_CACHED_PARTITIONS			(1)				// If true, we simply won't install partitions on 12GB devices that are marked as 'cached''

PARAM(installstat, "Output some network stats during install");

namespace rage {

extern char *XEX_TITLE_ID;	//	defined in gta5\src\dev\game\Core\main.cpp

#if __XENON
#define MAX_XCONTENT_HANDLES 64
static XCONTENT_DATA g_enumeratedContent[MAX_XCONTENT_HANDLES];
static DWORD g_numContent = 0;
#endif

const char *fiDeviceInstaller::sm_hddPath;
sysIpcThreadId fiDeviceInstaller::sm_Thread;
sysIpcThreadId fiDeviceInstaller::sm_WriteThread;
atArray<fiDeviceInstaller*> fiDeviceInstaller::sm_installPartitions;
bool fiDeviceInstaller::sm_pauseRead = false;
bool fiDeviceInstaller::sm_asyncInstall = false;
sInstallProgress fiDeviceInstaller::sm_installProgress;
datCallback fiDeviceInstaller::fileDoneCallback;
bool fiDeviceInstaller::sm_IsBootedFromHdd = false;
#if __PS3
char fiDeviceInstaller::sm_gameDataPath[CELL_GAME_PATH_MAX] = {0};
char fiDeviceInstaller::sm_contentInfoPath[CELL_GAME_PATH_MAX] = {0};
s32 fiDeviceInstaller::sm_availableHddSpaceKB = -1;
bool fiDeviceInstaller::sm_usingCache = false;
#if __DEV
atArray<atHashString> fiDeviceInstaller::sm_filesToKeep;
#endif // __DEV
#endif // __PS3
datCallback fiDeviceInstaller::sm_keepAliveCallback;
u32 fiDeviceInstaller::sm_keepAliveCallbackInterval = 50;

fiDeviceInstaller::InstallBuffer* fiDeviceInstaller::InstallBuffer::buffers[INSTALL_SLICE_NUM];
fiDeviceInstaller::InstallSliceQueue fiDeviceInstaller::InstallBuffer::readQueue;
fiDeviceInstaller::InstallSliceQueue fiDeviceInstaller::InstallBuffer::writeQueue;

bool gExitThread = false;

#ifdef CONST_FREQ
utimer_t gWaitForIo = (utimer_t)(CONST_FREQ / 1000) * 500; // 500 ms
#else 
utimer_t gWaitForIo = 0;
#endif

fiDeviceInstaller::fiDeviceInstaller() : m_partitionDevice(NULL)
{
#if __XENON
	m_device = INVALID_DEVICE;
	m_timestamp = 0;
#endif
}

fiDeviceInstaller::~fiDeviceInstaller()
{
	sm_installPartitions.DeleteMatches(this);

	if (m_partitionDevice)
	{
		fiDevice::Unmount(*m_partitionDevice);
		m_partitionDevice->Shutdown();
	}
	else
		fiDevice::Unmount(*this);
#if __XENON
	XContentClose(m_partitionName, NULL);
#endif // __XENON
	delete m_partitionDevice;
}

bool fiDeviceInstaller::Init(const InitData& data, const char* overridePath, bool remote)
{
	m_IsReadOnly = data.m_readOnly;
	m_IsCached = data.m_cached;
	m_status = IDS_IDLE;

	m_IsHddMounted = false;

	if (!data.m_sourceFile)
		m_sourceFile[0] = '\0';
	else
	    safecpy(m_sourceFile, StripPath(data.m_sourceFile), RAGE_MAX_PATH);

    char fullSourcePath[RAGE_MAX_PATH] = {0};
    const char* fileToUse = data.m_sourceFile.c_str();
    if (overridePath && overridePath[0] != '\0')
    {
        safecpy(fullSourcePath, overridePath);
        safecat(fullSourcePath, m_sourceFile);
        fileToUse = fullSourcePath;
    }

#if __PS3
    // hack to deal with us installing common.rpf and not having a custom installpartitions.meta
    // the rpf will be mounted in FileMgr.cpp, no need to do it again here (plus we don't have the encryption key here)
	if (sm_IsBootedFromHdd && !stricmp(m_sourceFile, "common.rpf"))
	{
        m_status = IDS_INSTALLED;
        return true;
	}
#endif

	safecpy(m_partitionName, data.m_partitionName, sizeof(m_partitionName));
	safecpy(m_partitionDesc, data.m_partitionDesc, sizeof(m_partitionDesc));
	safecpy(m_mountPath, data.m_mount, sizeof(m_mountPath));

	safecpy(m_prefix, m_partitionName, sizeof(m_prefix));
	safecat(m_prefix, ":\\", sizeof(m_prefix));

	Assert(!m_partitionDevice);
	m_partitionDevice = rage_new fiPackfile();

	// init the partition device io the source file. this will fall back to the default local device and grab
	// either off the disc or off the hdd
	if (InitInstall(fileToUse, remote))
	{
		char targetFile[RAGE_MAX_PATH];
		safecpy(targetFile, m_prefix, RAGE_MAX_PATH);
		safecat(targetFile, m_sourceFile, RAGE_MAX_PATH);

		if (!InitPartitionDevice(targetFile, !remote))
		{
			return false;
		}

#if __XENON && !__NO_OUTPUT
		char finalStr[256];
		formatf(finalStr, "Performed InitPD on %s\n", targetFile);
		OutputDebugString(finalStr);
#endif

		m_IsHddMounted = true;
	}
	else
	{
#if !__XENON
		// on xenon we support only packaged installs with our rpfs inside xcontent packages.
		// this means the source rpf won't be freely available on the ODD so skip the init here.
		// we could potentially mount the xcontent package on the ODD and use the rpf inside it but for what?
		if (!m_partitionDevice->Init(fileToUse, true, fiPackfile::CACHE_INSTALLER_FILE))
		{
			m_status = IDS_FAILED;
			Assertf(false, "fiDeviceInstaller::Init: Can't find archive '%s', it's supposed to be at %s", m_sourceFile, fileToUse);
			return false;
		}
#endif
	}
	m_partitionDevice->SetStreaming(false);
	if (!remote || m_IsHddMounted)
		MountAs(data.m_mount, false);

	return true;
}

void fiDeviceInstaller::GetInstallName(char* dst, s32 dstSize, const char* src)
{
	safecpy(dst, m_prefix, dstSize);
	safecat(dst, ASSET.FileName(src), dstSize);
}

bool fiDeviceInstaller::DoTimestampsMatch(const char* oddFile, const char* hddFile)
{
	const fiDevice* sourceDevice = fiDevice::GetDevice(oddFile);
	if (!sourceDevice)
	{
		Warningf("No device found for '%s'", oddFile);
		return false;
	}

	const fiDevice& localDevice = fiDeviceLocal::GetInstance();
	u64 oddTimestamp = sourceDevice->GetFileTime(oddFile);
	u64 hddTimestamp = localDevice.GetFileTime(hddFile);

#if __PS3
	// Time is in 100ns units.  FAT filesystem is only accurate to two seconds.
	const u64 minTime = 3 * 10000000;
	if (oddTimestamp > hddTimestamp)
		return oddTimestamp - hddTimestamp < minTime;
	else
		return hddTimestamp - oddTimestamp < minTime;
#else
	return oddTimestamp == hddTimestamp;
#endif
}

bool fiDeviceInstaller::DoFileSizesMatch(const char* oddFile, const char* hddFile)
{
	const fiDevice* sourceDevice = fiDevice::GetDevice(oddFile);
	if (!sourceDevice)
	{
		Warningf("No device found for '%s'", oddFile);
		return false;
	}

	const fiDevice& localDevice = fiDeviceLocal::GetInstance();
	u64 oddSize = sourceDevice->GetFileSize(oddFile);
	u64 hddSize = localDevice.GetFileSize(hddFile);

	return oddSize == hddSize;
}

void fiDeviceInstaller::ScheduleFile(const char* filename)
{
	const char* ext = ASSET.FindExtensionInPath(filename);
	if (ext && !strcmp(ext, ".rpf"))
	{
		const fiDevice* dev = fiDevice::GetDevice(filename);
		if (dev && dev->GetAttributes(filename) != FILE_ATTRIBUTE_INVALID)
		{
			InstallFile newFile;
			safecpy(newFile.filename, filename, sizeof(newFile.filename));
			looseFiles.PushAndGrow(newFile);
		}
	}
}

const char* fiDeviceInstaller::StripPath(const char* fullpath)
{
	const char* ret = strrchr(fullpath, '\\');
	if (!ret)
		ret = strrchr(fullpath, '/');
	if (!ret)
		ret = fullpath;
	else
		ret += 1; // skip slash
	return ret;
}

#if __XENON
void fiDeviceInstaller::FoundContentPackage(s32 contentIndex, const fiDevice& localDevice, bool remote)
{
	if (remote)
	{
		// we can do checks here to see if we want to re-download the remote games on demand packages
		// if they should ever need to change
		m_status = IDS_INSTALLED;
	}
	else
	{
		char destname[2048];
		StorePackageTimestamp();

		GetInstallName(destname, sizeof(destname), m_sourceFile);

		char sourcename[256];
		safecpy(sourcename, "game:\\", sizeof(sourcename));
		safecat(sourcename, m_sourceFile, sizeof(sourcename));

		// check if file already exists
		// if source file doesn't exist, we're probably running from the second disc
		if (localDevice.GetAttributes(sourcename) != FILE_ATTRIBUTE_INVALID && (localDevice.GetAttributes(destname) == FILE_ATTRIBUTE_INVALID || /*!DoTimestampsMatch(sourcename, destname) ||*/ !DoFileSizesMatch(sourcename, destname)))
		{
			localDevice.Delete(destname);
		}
		else
		{
			m_status = IDS_INSTALLED;
		}
	}

	// keep device id where this partition was found
	m_device = g_enumeratedContent[contentIndex].DeviceID;
}
#endif // __XENON

bool fiDeviceInstaller::InitInstall(const char* NOTFINAL_ONLY(PS3_ONLY(origSourceFile)), bool XENON_ONLY(remote))
{
#if __XENON
	// enumerate content and check if we've already installed
	const u32 maxHandles = 64;

	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

	// store partition name so we can identify it later
	safecpy(m_contentFileName, m_partitionName, XCONTENT_MAX_FILENAME_LENGTH + 1);

	DWORD bufferSize;
	HANDLE contentEnum;
	DWORD rt = XContentCreateEnumerator(XUSER_INDEX_NONE, XCONTENTDEVICE_ANY, XCONTENTTYPE_MARKETPLACE, 0, maxHandles, &bufferSize, &contentEnum);
	if(rt == ERROR_SUCCESS)
	{
		Assert(bufferSize == maxHandles * sizeof(XCONTENT_DATA));

		DWORD numContent = 0;
		while (true)
		{
			rt = XEnumerateEx(contentEnum, ENUMERATEFLAG_INCLUDEOPTICALMEDIA, g_enumeratedContent, bufferSize, &numContent, NULL);
			if (rt == ERROR_ACCESS_DENIED)
			{
				Warningf("XEnumerateEx returned ERROR_ACCESS_DENIED, trying again...");
				sysIpcSleep(200);
			}
			else if (rt != ERROR_SUCCESS)
			{
				m_status = IDS_FAILED_WRONG_DISC; // we didn't find any partitions on the hdd or the odd
				Warningf("InitInstall XEnumerateEx returned error 0x%x\n", rt);
				CloseHandle(contentEnum);

				if (remote)
					sm_installPartitions.PushAndGrow(this);

				return false;
			}
			else
			{
				break;
			}
		}
		g_numContent = numContent;

		CloseHandle(contentEnum);
	}
	else if(rt != ERROR_NO_SUCH_USER)
	{
		m_status = IDS_FAILED;
		Warningf("InitInstall: XContentCreateEnumerator returned error 0x%x\n", rt);
		return false;
	}

	// if we find this partition on the current disc we'll change the status, but for now assume it's missing
	m_status = IDS_FAILED_WRONG_DISC;

	// skip install if our partition is already on the hdd (and contains all files)
	char filename[XCONTENT_MAX_FILENAME_LENGTH + 1];
	for (s32 i = 0; i < (s32)g_numContent; ++i)
	{
		sysMemCpy(filename, g_enumeratedContent[i].szFileName, XCONTENT_MAX_FILENAME_LENGTH);
		filename[XCONTENT_MAX_FILENAME_LENGTH] = 0;

		// mount the xcontent so we can access it
		DWORD ret = XContentCreate(XUSER_INDEX_ANY, m_partitionName, &g_enumeratedContent[i], XCONTENTFLAG_OPENEXISTING, NULL, NULL, NULL);
		if (ret == ERROR_FILE_CORRUPT)
		{
			XContentClose(m_partitionName, NULL);
			XContentDelete(XUSER_INDEX_ANY, &g_enumeratedContent[i], NULL);
			continue;
		}
		else if (ret != ERROR_SUCCESS)
		{
			m_status = IDS_FAILED;
			Errorf("InitInstall: XContentCreate failed with error %d", ret);
			continue;
		}
		else
		{
			// does this content package have our partition file in it? if not unmount and keep looking
			char destname[2048];
			GetInstallName(destname, sizeof(destname), m_sourceFile); // TODO: change sourceFile to something safer?

			if (localDevice.GetAttributes(destname) != FILE_ATTRIBUTE_INVALID)
			{
				XDEVICE_DATA devData = {0};
				if (XContentGetDeviceData(g_enumeratedContent[i].DeviceID, &devData) == ERROR_SUCCESS)
				{
					if (devData.DeviceType == XCONTENTDEVICETYPE_ODD)
					{
						if (m_status == IDS_FAILED_WRONG_DISC)
							m_status = IDS_IDLE;
						XContentClose(m_partitionName, NULL);
						continue;
					}
				}
				else
				{
					Errorf("InitInstall: XContentGetDeviceData failed!");
					XContentClose(m_partitionName, NULL);
					continue;
				}

				// store actual name of package
				safecpy(m_contentFileName, g_enumeratedContent[i].szFileName, XCONTENT_MAX_FILENAME_LENGTH + 1);

				m_status = IDS_IDLE;
				FoundContentPackage(i, localDevice, remote);
				break;
			}

			XContentClose(m_partitionName, NULL);
		}
	}

	// if no files have been scheduled for install, no partition was found so add them all
	if (looseFiles.GetCount() == 0 && m_status != IDS_INSTALLED)
	{
#if __XENON
		InstallFile newFile;
		safecpy(newFile.filename, "game:\\", sizeof(newFile.filename));
		safecat(newFile.filename, m_partitionName, sizeof(newFile.filename));
		looseFiles.PushAndGrow(newFile);
#else
		char destname[2048];
		safecpy(destname, "game:\\", sizeof(destname));
		safecat(destname, m_sourceFile, sizeof(destname));
		ScheduleFile(destname);
#endif
	}

#elif __PS3

	// get content path if this is the first time we query it
	if (!fiDeviceInstaller::sm_IsBootedFromHdd && sm_availableHddSpaceKB == -1)
	{
		char dirName[64] = {0};
		formatf(dirName, "%s_install", XEX_TITLE_ID);

		CellGameContentSize contentSize;
		u32 result = cellGameDataCheck(CELL_GAME_GAMETYPE_GAMEDATA, dirName, &contentSize);

        bool installIcon = false;
		if (result == CELL_GAME_RET_NONE)
		{
			char tmp_contentInfoPath[CELL_GAME_PATH_MAX];
			char tmp_usrdirPath[CELL_GAME_PATH_MAX];
			CellGameSetInitParams init;
			memset(&init, 0x00, sizeof(init));
			strncpy(init.title,   "Grand Theft Auto V - Install", CELL_GAME_SYSP_TITLE_SIZE - 1) ;
			strncpy(init.titleId, dirName, CELL_GAME_SYSP_TITLEID_SIZE - 1) ;
			strncpy(init.version, "01.00", CELL_GAME_SYSP_VERSION_SIZE - 1) ;
			result = cellGameCreateGameData( &init, tmp_contentInfoPath, tmp_usrdirPath ) ;

			if(result != CELL_OK)
			{
				Errorf("InitInstall: Failed to create the gamedata directory (%x)", result);
				m_status = IDS_FAILED;

				// Re-enable calls to cellGameDataCheck()
				cellGameContentPermit(sm_contentInfoPath, sm_gameDataPath);

				return false;
			}	

            installIcon = true;
		}

        if (result != CELL_OK)
		{
			Warningf("InitInstall: Failed to retrieve the gamedata directory (%d)", result);
			m_status = IDS_FAILED;

			// Re-enable calls to cellGameDataCheck()
			cellGameContentPermit(sm_contentInfoPath, sm_gameDataPath);

			return false;
		}

        result = cellGameContentPermit(sm_contentInfoPath, sm_gameDataPath);
        
		if (result != CELL_OK)
		{
			Warningf("InitInstall: Failed to retrieve the gamedata directory (%d)", result);
			m_status = IDS_FAILED;
			return false;
		}
        
        if (installIcon)
        {
			// copy install data icon
			if (!CopyContentInfoFile("ICON0.PNG", sm_contentInfoPath) && __FINAL)
			{
				cellGameContentErrorDialog(CELL_GAME_ERRDIALOG_BROKEN_EXIT_GAMEDATA, 0, dirName);
				m_status = IDS_FAILED;
				return false;
			}
        }

		s32 trophySpace = 0;
#if HACK_GTA4
		// KS - Check for required trophy HDD space.
		trophySpace = 50 * 1024; // reserve space for trophies TRC 30.0 [R154]
#endif	//	HACK_GTA4

		sm_availableHddSpaceKB = contentSize.hddFreeSizeKB - contentSize.sysSizeKB - trophySpace;

		// if available memory is less than 12gb, use the game cache for some partitions.
		// We'll use 10GB as a threshold though.
		static const s32 USE_CACHE_THRESHOLD_KB = 10 << 20;
		if (sm_availableHddSpaceKB < USE_CACHE_THRESHOLD_KB)
			sm_usingCache = true;
	}
	else if (fiDeviceInstaller::sm_IsBootedFromHdd)
	{
		// if booted from hdd get the location of the game instead of creating a new folder ourselves
		Assert(sm_hddPath && sm_hddPath[0]);	// That's set at the same time sm_isBootedFromHdd is true
		safecpy(sm_gameDataPath, sm_hddPath);
	}

	if (!IsUsingCache())
	{
		m_IsCached = false;
	}
	else
	{
#if SKIP_CACHED_PARTITIONS
		// If this is a cached file, don't install it on 12GB PS3s.
		if (m_IsCached && !fiDeviceInstaller::sm_IsBootedFromHdd)
		{
			m_status = IDS_INSTALLED;

			safecpy(m_prefix, "game:\\");

			Displayf("12GB PS3 detected - skipping file %s. Prefix set to %s", m_partitionName, m_prefix);
			sm_installPartitions.PushAndGrow(this);
			return true;
		}
#endif // SKIP_CACHED_PARTITIONS
	}

	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

    // on ps3 we don't have a symbolic link so prefix needs to be correct path to content
	if (m_IsCached)
	{
		formatf(m_prefix, "%sdi", fiCachePartition::GetCachePrefix());
	}
	else
	{
		safecpy(m_prefix, sm_gameDataPath, sizeof(m_prefix));
		safecat(m_prefix, "/", sizeof(m_prefix));
	}

	if (fiDeviceInstaller::sm_IsBootedFromHdd)
	{
		m_status = IDS_INSTALLED;
	}
	else
	{
		char destname[2048];

		safecpy(destname, m_prefix, sizeof(destname));
		safecat(destname, m_sourceFile, sizeof(destname));

		char sourcename[256];
#if !__FINAL
		if (origSourceFile && strcmp(origSourceFile, m_sourceFile))
		{
			safecpy(sourcename, origSourceFile, sizeof(sourcename));
		}
		else
#endif
		{
			safecpy(sourcename, "game:\\", sizeof(sourcename));
			safecat(sourcename, m_sourceFile, sizeof(sourcename));
		}

		// check if file already exists
		if (localDevice.GetAttributes(destname) != FILE_ATTRIBUTE_INVALID && DoTimestampsMatch(sourcename, destname) && DoFileSizesMatch(sourcename, destname))
		{
			m_status = IDS_INSTALLED;
#if __DEV
			sm_filesToKeep.PushAndGrow(atHashString(destname));
#endif
		}
		else
		{
			// we need to install this partition
#if !__FINAL
			if (origSourceFile && strcmp(origSourceFile, m_sourceFile))
			{
				safecpy(destname, origSourceFile, sizeof(destname));

				if (!m_IsCached)
				{
					const fiDevice* sourceDevice = fiDevice::GetDevice(destname);
					sm_availableHddSpaceKB -= (u32)(sourceDevice->GetFileSize(destname) / 1024);
				}
			}
			else
#endif
			{
				safecpy(destname, "game:\\", sizeof(destname));
				safecat(destname, m_sourceFile, sizeof(destname));

				if (!m_IsCached)
					sm_availableHddSpaceKB -= (u32)(localDevice.GetFileSize(destname) / 1024);
			}

			if (sm_availableHddSpaceKB < 0)
			{
				m_status = IDS_FAILED;
				return false;
			}

			ScheduleFile(destname);
		}
	}
#endif

	sm_installPartitions.PushAndGrow(this);
	return m_status == IDS_INSTALLED;
}

void fiDeviceInstaller::StartInstall(XENON_ONLY(XCONTENTDEVICEID device))
{
	if (fiDeviceInstaller::sm_IsBootedFromHdd)
		return;

#if __DEV && __PS3
	// go through all files on the hdd and remove the ones we haven't flagged for keeping
	// otherwise we might end up with files that get mounted by the engine later and
	// we run out of file handles
	if (sm_gameDataPath[0] == '\0')
	{
		u32 result = cellGameContentPermit(sm_contentInfoPath, sm_gameDataPath);
		if (result != CELL_OK)
		{
			Warningf("StartInstall: Failed to retrieve the gamedata directory (%d)", result);
			return;
		}
	}

	if (sm_gameDataPath[0] == '\0')
	{
		Warningf("StartInstall: Failed to retrieve the gamedata directory");
		return;
	}

	fiFindData fd;
	const fiDevice& localDevice = fiDeviceLocal::GetInstance();
	fiHandle fh = localDevice.FindFileBegin(sm_gameDataPath, fd);
	s32 count = 0;
	char destname[512];
	if (fh != fiHandleInvalid)
	{
		do 
		{
			if (fd.m_Name[0] == '.')
				continue;

			safecpy(destname, sm_gameDataPath, sizeof(destname));
			safecat(destname, "/", sizeof(destname));
			safecat(destname, fd.m_Name, sizeof(destname));

			bool keep = false;
			atHashString fileHash(destname);
			for (s32 i = 0; i < sm_filesToKeep.GetCount(); ++i)
			{
				if (sm_filesToKeep[i].GetHash() == fileHash.GetHash())
				{
					keep = true;
					break;
				}
			}

			if (!keep)
			{
				localDevice.Delete(destname);
				count++;
			}

		} while (localDevice.FindFileNext(fh, fd));
		localDevice.FindFileEnd(fh);
	}

	Printf("Removed %d files from the hdd for safety!\n", count);
	sm_filesToKeep.Reset();
#endif // __DEV && __PS3

	// start install thread only if we have something to install
	bool install = false;
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		if (sm_installPartitions[i]->m_status == IDS_IDLE)
		{
			sm_installPartitions[i]->m_status = IDS_INSTALLING;
#if __XENON
			if (sm_installPartitions[i]->m_device == INVALID_DEVICE)
				sm_installPartitions[i]->m_device = device;
#endif
			install = true;
		}
	}

	if (install)
	{
		sm_Thread = sysIpcCreateThread(&fiDeviceInstaller::InstallerThread, NULL, sysIpcMinThreadStackSize, sm_asyncInstall ? PRIO_BELOW_NORMAL : PRIO_TIME_CRITICAL, "Installer");
	}
}

void fiDeviceInstaller::InstallProgress(u64 fileSize, u64 fileTransferredSize, u64 totalTransferredSize)
{
	sm_installProgress.fileSize = fileSize / 1024.f / 1024.f;
	sm_installProgress.fileTransferredSize = fileTransferredSize / 1024.f / 1024.f;
	sm_installProgress.totalTransferredSize += totalTransferredSize / 1024.f / 1024.f;

	if (PARAM_installstat.Get())
		fiDeviceTcpIp::PrintInterfaceState();
}

void fiDeviceInstaller::InstallerThread(void* ptr)
{
	(void)ptr;

	InstallBuffer::Init();

	NOTFINAL_ONLY(sysIpcCritSecDisableTimeout++;)

	// accumulate total install size
	u64 totalSize = 0;
#if __XENON
	char filename[XCONTENT_MAX_FILENAME_LENGTH + 1];
#endif
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		if (sm_installPartitions[i]->m_status == IDS_INSTALLING)
		{
			for (s32 f = 0; f < sm_installPartitions[i]->looseFiles.GetCount(); ++f)
			{
#if __XENON
				for (s32 g = 0; g < (s32)g_numContent; ++g)
				{
					sysMemCpy(filename, g_enumeratedContent[g].szFileName, XCONTENT_MAX_FILENAME_LENGTH);
					filename[XCONTENT_MAX_FILENAME_LENGTH] = 0;

					if (strstr(sm_installPartitions[i]->looseFiles[f].filename, filename))
					{
						XDEVICE_DATA devData = {0};
						if (XContentGetDeviceData(g_enumeratedContent[g].DeviceID, &devData) == ERROR_SUCCESS)
						{
							if (devData.DeviceType != XCONTENTDEVICETYPE_ODD)
							{
								continue;
							}

							// open the package on the disc and get the size
							HANDLE srcHandle;
							DWORD ret = XContentOpenPackage(XUSER_INDEX_ANY, &g_enumeratedContent[g], &srcHandle);
							if (ret != ERROR_SUCCESS)
							{
								Warningf("InstallerThread: XContentOpenPackage failed with error %d on %s", ret, filename);
								continue;
							}
							
							sm_installPartitions[i]->looseFiles[f].filesize = (u64)::GetFileSize(srcHandle, NULL);
							totalSize += sm_installPartitions[i]->looseFiles[f].filesize;
							::CloseHandle(srcHandle);
						}
						else
						{
							Warningf("InstallerThread: XContentGetDeviceData failed!");
							continue;
						}
					}
				}
#else
				const fiDevice* sourceDevice = fiDevice::GetDevice(sm_installPartitions[i]->looseFiles[f].filename);
				if (!sourceDevice)
				{
					continue;
				}
				sm_installPartitions[i]->looseFiles[f].filesize = sourceDevice->GetFileSize(sm_installPartitions[i]->looseFiles[f].filename);
				totalSize += sm_installPartitions[i]->looseFiles[f].filesize;
#endif
			}
		}
	}
	sm_installProgress.totalSize = totalSize / 1024.f / 1024.f;

	sysTimer progressTimer;
	u32 numPartitions = 0;
	float totalTime = 0.f;
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		fiDeviceInstaller* partition = sm_installPartitions[i];
		if (partition->m_status != IDS_INSTALLING)
		{
			continue;
		}

		Displayf("Installing %s (%s)...", partition->m_partitionDesc, partition->m_partitionName);

		sysTimer timer;

		// fire off thread to write to hdd
		gExitThread = false;
		sm_WriteThread = sysIpcCreateThread(&fiDeviceInstaller::WriteInstallFile, partition, 32768, sm_asyncInstall ? PRIO_BELOW_NORMAL : PRIO_TIME_CRITICAL, "Installer - Write");

		// read from disc on this thread
		if (!ReadInstallFile(partition, progressTimer))
		{
			partition->m_status = IDS_FAILED;
#if __XENON
			XContentClose(partition->m_partitionName, NULL);
#endif
			// make sure write thread finished
			gExitThread = true;
			sysIpcWaitThreadExit(sm_WriteThread);
			continue;
		}

		// wait for write thread to finish
		sysIpcWaitThreadExit(sm_WriteThread);

#if __XENON
		// need to close the mount right away for the write to be flushed
		XContentClose(partition->m_partitionName, NULL);
#endif

		if (partition->HasFailed())
		{
			continue;
		}

		partition->m_status = IDS_INSTALLED;

		Displayf("Installed %s (%s) in %d minute(s) and %d second(s)!", partition->m_partitionDesc, partition->m_partitionName, ((int)timer.GetTime()) / 60, ((int)timer.GetTime()) % 60);
		totalTime += timer.GetTime();
		numPartitions++;
	}

	Displayf("Installed %d partitions in %d minute(s) and %d second(s)!", numPartitions, ((int)totalTime) / 60, ((int)totalTime) % 60);

	NOTFINAL_ONLY(sysIpcCritSecDisableTimeout--;)

    InstallBuffer::Shutdown();
}

bool fiDeviceInstaller::ReadInstallFile(fiDeviceInstaller* partition, sysTimer& BANK_ONLY(progressTimer))
{
	sysTimer waitTimer;
#if __BANK || !__NO_OUTPUT
	float secondsWaited = 0.f;
#endif

#if __BANK
    sysTimer timer;
    u32 readThroughput = 0;
    float throughputReset = timer.GetTime() + 1.f;
	float waitedPerSec = 0.f;
	float remoteWaited = 0.f;
#endif

	for (s32 i = 0; i < partition->looseFiles.GetCount(); ++i)
	{
		const fiDevice* sourceDevice = fiDevice::GetDevice(partition->looseFiles[i].filename);
		if (!sourceDevice)
		{
			Warningf("No device found for '%s', skipping install...", partition->looseFiles[i].filename);
			continue;
		}

#if __XENON
		// find the package which we enumerated previously, make sure it's on the ODD
		char filename[XCONTENT_MAX_FILENAME_LENGTH + 1];
		s32 contentIndex = -1;
		for (u32 f = 0; f < g_numContent; ++f)
		{
			sysMemCpy(filename, g_enumeratedContent[f].szFileName, XCONTENT_MAX_FILENAME_LENGTH);
			filename[XCONTENT_MAX_FILENAME_LENGTH] = 0;

			if (!strcmp(filename, partition->m_partitionName))
			{
				XDEVICE_DATA devData = {0};
				if (XContentGetDeviceData(g_enumeratedContent[f].DeviceID, &devData) == ERROR_SUCCESS)
				{
					if (devData.DeviceType == XCONTENTDEVICETYPE_ODD)
					{
						contentIndex = (s32)f;
						break;
					}
				}
				else
				{
					Warningf("ReadInstallFile: XContentGetDeviceData failed!");
				}
			}
		}

		if (contentIndex == -1)
		{
			partition->m_status = IDS_FAILED;
			Errorf("ReadInstallFile: Couldn't find source package '%s' on disc", partition->m_partitionName);
			return false;
		}

		HANDLE srcHandle;
		DWORD ret = XContentOpenPackage(XUSER_INDEX_ANY, &g_enumeratedContent[contentIndex], &srcHandle);
		if (ret != ERROR_SUCCESS)
		{
			partition->m_status = IDS_FAILED;
			Errorf("ReadInstallFile: XContentOpenPackage failed with error %d on partition %s", ret, partition->m_partitionName);
			return false;
		}
		Assert(partition->looseFiles.GetCount() == 1);

		fiHandle sourceFile = srcHandle;
		u64 timeStamp = 1; // dummy value, won't be used for 360 packages
		u64 fileSize = partition->looseFiles[i].filesize;
#else
		fiHandle sourceFile = sourceDevice->Open(partition->looseFiles[i].filename, true);
		if (sourceFile == fiHandleInvalid)
		{
			Errorf("Error opening %s for read", partition->looseFiles[i].filename);
			partition->m_status = IDS_FAILED;
			return false;
		}
		
		u64 timeStamp = sourceDevice->GetFileTime(partition->looseFiles[i].filename);
		u64 fileSize = partition->looseFiles[i].filesize;
#endif

		u32 lastCallbackTime = 0;

		u64 read = 0;
		while (read < fileSize)
		{
			u32 timeMs = sysTimer::GetSystemMsTime();
			if ((timeMs - lastCallbackTime) >= sm_keepAliveCallbackInterval)
			{
				sm_keepAliveCallback.Call();
				lastCallbackTime = timeMs;
			}

			if (sm_pauseRead)
			{
				sysIpcSleep(100);
				continue;
			}

            // don't read while the streaming or audio does
            utimer_t now = sysTimer::GetTicks();
            utimer_t thenOptical = pgLastActivity[pgStreamer::OPTICAL];

            // thenOptical == 0 means a read is in progress, we need to wait until gWaitForIo has passed
            // since last read before we can proceed
            if (!pgStreamer::IsLocked(pgStreamer::OPTICAL) && thenOptical && now - thenOptical >= gWaitForIo)
            {
                BANK_ONLY(waitTimer.Reset();)

                s32 amtRead;
                s32 leftToRead = amtRead = Min((u32)INSTALL_SLICE_SIZE, (u32)(fileSize - read));
                InstallBuffer* readBuffer = InstallBuffer::GetReadBuffer();
                u8* targetBuffer = readBuffer->buffer;

                while (leftToRead)
                {
                    int dataRead = sourceDevice->Read(sourceFile, targetBuffer, leftToRead);

                    if (dataRead < 0)
                    {
                        Quitf(ERR_DEFAULT,"Error reading %d bytes from install file", leftToRead);
                    }

                    leftToRead -= dataRead;
                    targetBuffer += dataRead;

                    if (leftToRead)
                    {
                        // We should be able to read the entire chunk in one go. If that didn't work,
                        // give the I/O breathing space.
                        Displayf("Couldn't read %d bytes of install data in one chunk, only got %d.", leftToRead + dataRead, dataRead);
                        sysIpcSleep(10);
                    }
                }

                BANK_ONLY(remoteWaited += waitTimer.GetTime();)

                    readBuffer->sliceSize = amtRead;
                if (read == 0)
                {
                    readBuffer->timeStamp = timeStamp;
                    readBuffer->fileSize = fileSize;
                }
                read += amtRead;
                BANK_ONLY(readThroughput += amtRead;)

                    OUTPUT_ONLY(waitTimer.Reset();)
                    InstallBuffer::PutWriteBuffer(readBuffer);
                OUTPUT_ONLY(secondsWaited += waitTimer.GetTime();)

#if __BANK
                    waitedPerSec += waitTimer.GetTime();
                if (timer.GetTime() >= throughputReset)
                {
                    sm_installProgress.totalTime = progressTimer.GetTime();
                    sm_installProgress.readThroughput = readThroughput / 1024.f / 1024.f;
                    sm_installProgress.readWaited = waitedPerSec;
                    sm_installProgress.remoteWaited = remoteWaited;
                    readThroughput = 0;
                    throughputReset += 1.f;
                    waitedPerSec = 0.f;
                    remoteWaited = 0.f;
                } 
#endif
            }
            else
                sysIpcSleep(100);

			//if (g_RequestSysutilExit)
			//	break;
		}

		sourceDevice->Close(sourceFile);

		if (read != fileSize)
		{
			Errorf("Read %" I64FMT "u when file size is %" I64FMT "u for '%s'", read, fileSize, partition->looseFiles[i].filename);
			partition->m_status = IDS_FAILED;
			return false;
		}
	}

	Displayf("Spent %.2f seconds waiting for Write thread!", secondsWaited);

	return true;
}

void fiDeviceInstaller::WriteInstallFile(void* ptr)
{
	sysTimer timer;
	sysTimer waitTimer;
	float nextProgressReport = 0.f;
	OUTPUT_ONLY(float secondsWaited = 0.f;)

	fiDeviceInstaller* partition = (fiDeviceInstaller*)ptr;
	const float progressInterval = partition->looseFiles.GetCount() > 1 ? 0.25f : 1.f;

#if __BANK
    u32 writeThroughput = 0;
    float throughputReset = timer.GetTime() + 1.f;
	float waitedPerSec = 0.f;
#endif

	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

#if __XENON
	XCONTENT_DATA contentData;
	contentData.DeviceID = partition->m_device;
	contentData.dwContentType = XCONTENTTYPE_MARKETPLACE;
	safecpy(contentData.szDisplayName, partition->m_partitionDesc, XCONTENT_MAX_DISPLAYNAME_LENGTH);
	safecpy(contentData.szFileName, partition->m_partitionName, XCONTENT_MAX_FILENAME_LENGTH);

	HANDLE destHandle;
	DWORD ret = XContentCreatePackage(XUSER_INDEX_ANY, &contentData, FALSE, &destHandle);
	if (ret != ERROR_SUCCESS && ret != ERROR_ALREADY_EXISTS)
	{
		partition->m_status = IDS_FAILED;
		Errorf("WriteInstallFile: XContentCreatePackage failed with error %d on partition %s", ret, partition->m_partitionName);
		return;
	}
	Assert(partition->looseFiles.GetCount() == 1);
#endif

#if !__XENON || !__NO_OUTPUT
	char destname[256];
#endif
	for (s32 i = 0; i < partition->looseFiles.GetCount(); ++i)
	{
#if __XENON
		fiHandle destFile = destHandle;
#else
		partition->GetInstallName(destname, sizeof(destname), partition->looseFiles[i].filename);

		localDevice.Delete(destname);
		fiHandle destFile = fiHandleInvalid;
#endif
		u64 timeStamp = 0;
		u64 copied = 0;
		u64 fileSize = copied + 1; // this is passed along from the read thread on first file read, together with the timestamp
		u32 totalProgressCopied = 0;
		while (copied < fileSize)
		{
			if (gExitThread)
			{
				break;
			}

#if __BANK || !__NO_OUTPUT
			waitTimer.Reset();
#endif

			InstallBuffer* writeBuffer = InstallBuffer::GetWriteBuffer();
			u8* getPtr = writeBuffer->buffer;

			OUTPUT_ONLY(secondsWaited += waitTimer.GetTime();)
            BANK_ONLY(waitedPerSec += waitTimer.GetTime();)

			// if this is first read of the file, grab the time stamp and the file size and create the file
			// we can't create the file earlier because the disc read thread might get a handle to this file when
			// doing GetDevice on the filename, since this device might be mounted to platform:/ too
			if (copied == 0)
			{
#if !__XENON
				destFile = localDevice.Create(destname);
				if (destFile == fiHandleInvalid)
				{
					partition->m_status = IDS_FAILED;
					gExitThread = true;
					Errorf("Error opening %s for write", destname);
					return;
				}
#endif // !__XENON

				Assertf(timeStamp == 0 && writeBuffer->timeStamp != 0, "Missing time stamp, installation of '%s' is probably corrupted", destname);
				timeStamp = writeBuffer->timeStamp;
				Assertf(fileSize == copied + 1 && writeBuffer->fileSize != 0, "Missing file size, installation of '%s' is probably corrupted", destname);
				fileSize = writeBuffer->fileSize;

				// set progress
				sm_installProgress.currentFile = (partition->looseFiles[i].filename);
				sm_installProgress.fileTransferredSize = 0;
			}

			s32 sliceSize = writeBuffer->sliceSize;
			while (sliceSize)
			{
				s32 wrote = localDevice.Write(destFile, getPtr, sliceSize);
				if (wrote < 0)
				{
#if __XENON
					Reboot(ATSTRINGHASH("R*V0", 0x91AA9A78));
#else
					Quitf(ERR_DEFAULT,"Error writing install file");
#endif
				}

				BANK_ONLY(writeThroughput += (u32) wrote;)
				copied += (u64)wrote;
				totalProgressCopied += (u64)wrote;
				
				getPtr += wrote;
				sliceSize -= wrote;

				if (sliceSize)
				{
					// We should be able to read the entire chunk in one go. If that didn't work,
					// give the I/O breathing space.
					Displayf("Couldn't write %d bytes of install data in one chunk, only managed to write %d.", sliceSize + wrote, wrote);
					sysIpcSleep(10);
				}
			}

			writeBuffer->Reset();
			InstallBuffer::PutReadBuffer(writeBuffer);

			if (timer.GetTime() > nextProgressReport)
			{
				nextProgressReport += progressInterval;
				InstallProgress(fileSize, copied, totalProgressCopied);
				totalProgressCopied = 0;
			}

#if __BANK
            if (timer.GetTime() >= throughputReset)
            {
                sm_installProgress.writeThroughput = writeThroughput / 1024.f / 1024.f;
                sm_installProgress.writeWaited = waitedPerSec;
                writeThroughput = 0;
                throughputReset += 1.f;
                waitedPerSec = 0.f;
            }
#endif
			//if (g_RequestSysutilExit)
			//	break;
		}

		localDevice.Close(destFile);

		if (copied != fileSize)
		{
			Errorf("Wrote %" I64FMT "u when file size is %" I64FMT "u for '%s'", copied, fileSize, destname);
			partition->m_status = IDS_FAILED;
			return;
		}

#if !__XENON
		localDevice.SetFileTime(destname, timeStamp);
#endif

    
		InstallProgress(fileSize, copied, totalProgressCopied);

		fileDoneCallback.Call(CallbackData(partition->looseFiles[i].filename));
	}
	gExitThread = true;
	
	Displayf("Spent %.2f seconds waiting for Read thread!", secondsWaited);
}

void fiDeviceInstaller::SetIsBootedFromHdd(bool val, const char *hddPath)
{
	sm_IsBootedFromHdd = val;
	sm_hddPath = hddPath;
}

bool fiDeviceInstaller::IsContentAvailable()
{
#if __XENON
	const u32 maxHandles = 64;
	XCONTENT_DATA* enumeratedContent = rage_new XCONTENT_DATA[maxHandles];

	DWORD bufferSize;
	HANDLE contentEnum;
	DWORD numContent = 0;
	DWORD rt = XContentCreateEnumerator(XUSER_INDEX_NONE, XCONTENTDEVICE_ANY, XCONTENTTYPE_MARKETPLACE, 0, maxHandles, &bufferSize, &contentEnum);
	if(rt == ERROR_SUCCESS)
	{
		Assert(bufferSize == maxHandles * sizeof(XCONTENT_DATA));

		rt = XEnumerateEx(contentEnum, ENUMERATEFLAG_INCLUDEOPTICALMEDIA, enumeratedContent, bufferSize, &numContent, NULL);
		if (rt != ERROR_SUCCESS)
		{
			Warningf("IsContentAvailable: XEnumerateEx returned error 0x%x\n", rt);
			CloseHandle(contentEnum);
			delete[] enumeratedContent;
			return false;
		}

		CloseHandle(contentEnum);
	}

	for (s32 i = 0; i < (s32)numContent; ++i)
	{
		if (!strcmp(enumeratedContent[i].szFileName, m_contentFileName))
		{
			// skip if we found a package on the odd
			XDEVICE_DATA devData = {0};
			if (XContentGetDeviceData(enumeratedContent[i].DeviceID, &devData) == ERROR_SUCCESS)
			{
				if (devData.DeviceType == XCONTENTDEVICETYPE_ODD)
				{
					continue;
				}
			}
			else
			{
				continue;
			}

			if (enumeratedContent[i].DeviceID != m_device)
			{
				//XContentClose(m_partitionName, NULL);
				m_device = enumeratedContent[i].DeviceID;
				//DWORD ret = XContentCreate(XUSER_INDEX_ANY, m_partitionName, &enumeratedContent[i], XCONTENTFLAG_OPENEXISTING, NULL, NULL, NULL);
				//return ret == ERROR_SUCCESS;
			}

			// don't add the partition twice
			bool alreadyAdded = false;
			for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
			{
				if (sm_installPartitions[i] == this)
				{
					alreadyAdded = true;
					break;
				}
			}
			if (!alreadyAdded)
				sm_installPartitions.PushAndGrow(this);

			m_status = IDS_INSTALLED;
			delete[] enumeratedContent;
			return true;
		}
	}

	delete[] enumeratedContent;
#endif // __XENON

	return false;
}

// NOTE: this function isn't safe to call while files are held open through this device
void fiDeviceInstaller::FinishInstall()
{
	// block for installer thread if install isn't finished
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		if (sm_installPartitions[i]->m_status == IDS_INSTALLING)
		{
			sysIpcWaitThreadExit(sm_Thread);
			break;
		}
	}
	
	// mount hdd rpfs for partitions successfully installed
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		fiDeviceInstaller* partition = sm_installPartitions[i];

#if __XENON && !__NO_OUTPUT
		char str2[256];
		formatf(str2, "## Part %d: Status=%d, Mounted=%d\n", i, partition->m_status, (int) partition->m_IsHddMounted);
		OutputDebugString(str2);
#endif

		if (partition->m_status == IDS_INSTALLED && !partition->m_IsHddMounted)
		{
#if __XENON
			XCONTENT_DATA contentData;
			contentData.DeviceID = partition->m_device;
			contentData.dwContentType = XCONTENTTYPE_MARKETPLACE;
			safecpy(contentData.szFileName, partition->m_partitionName, XCONTENT_MAX_FILENAME_LENGTH);

			DWORD contentFlags = XCONTENTFLAG_OPENEXISTING;
			DWORD ret = XContentCreate(XUSER_INDEX_ANY, partition->m_partitionName, &contentData, contentFlags, NULL, NULL, NULL);
			if (ret != ERROR_SUCCESS)
			{
				// TODO: handle ERROR_FILE_CORRUPT here

				partition->m_status = IDS_FAILED;
				Errorf("InitInstall: XContentCreate failed with error %d", ret);
				continue;
			}

			// on xenon we have the timestamp stored in a separate .timestamp file inside the xcontent package,
			// we have just mounted the package so this is the earliest we can access its contents
			partition->StorePackageTimestamp();
#endif
			if (partition->m_partitionDevice)
			{
				char hddFile[RAGE_MAX_PATH] = {0};
				safecpy(hddFile, partition->m_prefix, sizeof(hddFile));
				safecat(hddFile, partition->m_sourceFile, sizeof(hddFile));

				partition->m_partitionDevice->Shutdown();

				// mount the partition with the recently installed rpf on the hdd
				// it will again fail to find a mount for it and will fall back on the local device which should
				// handle our part:// prefix correctly
				if (!partition->InitPartitionDevice(hddFile, true))
				{
					continue;
				}
			}

			if (!partition->MountAs(partition->m_mountPath, partition->m_partitionDevice == NULL))
			{
				partition->m_status = IDS_FAILED;
#if __XENON
				XContentClose(partition->m_partitionName, NULL);
#endif
				Errorf("fiDeviceInstaller::Init: Failed to mount device to '%s'", partition->m_mountPath);
				continue;
			}

			partition->m_IsHddMounted = true;
		}

		if (partition->m_status == IDS_INSTALLED && partition->m_partitionDevice)
		{
			partition->FlagInstalledRpfs();
		}
	}

	sm_installPartitions.clear();
}

#if __XENON
void fiDeviceInstaller::FinishInstallGond()
{
	// block for installer thread if install isn't finished
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		if (sm_installPartitions[i]->m_status == IDS_INSTALLING)
		{
			sysIpcWaitThreadExit(sm_Thread);
			break;
		}
	}

	// mount hdd rpfs for partitions successfully installed
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		fiDeviceInstaller* partition = sm_installPartitions[i];
		if (partition->m_status == IDS_INSTALLED && !partition->m_IsHddMounted)
		{
			XCONTENT_DATA contentData;
			contentData.DeviceID = partition->m_device;
			contentData.dwContentType = XCONTENTTYPE_MARKETPLACE;
			safecpy(contentData.szFileName, partition->m_contentFileName, XCONTENT_MAX_FILENAME_LENGTH + 1);

			DWORD contentFlags = XCONTENTFLAG_OPENEXISTING;
			DWORD ret = XContentCreate(XUSER_INDEX_ANY, partition->m_partitionName, &contentData, contentFlags, NULL, NULL, NULL);
			if (ret != ERROR_SUCCESS)
			{
				// TODO: handle ERROR_FILE_CORRUPT here

				partition->m_status = IDS_FAILED;
				Errorf("InitInstall: XContentCreate failed with error %d", ret);
				continue;
			}

			// on xenon we have the timestamp stored in a separate .timestamp file inside the xcontent package,
			// we have just mounted the package so this is the earliest we can access its contents
			partition->StorePackageTimestamp();
			if (partition->m_partitionDevice)
			{
				char hddFile[RAGE_MAX_PATH] = {0};
				safecpy(hddFile, partition->m_prefix, sizeof(hddFile));
				safecat(hddFile, partition->m_sourceFile, sizeof(hddFile));

				partition->m_partitionDevice->Shutdown();

				// mount the partition with the recently installed rpf on the hdd
				// it will again fail to find a mount for it and will fall back on the local device which should
				// handle our part:// prefix correctly
				if (!partition->InitPartitionDevice(hddFile, true))
				{
					continue;
				}
			}

			if (!partition->MountAs(partition->m_mountPath, partition->m_partitionDevice == NULL))
			{
				partition->m_status = IDS_FAILED;
				XContentClose(partition->m_partitionName, NULL);
				Errorf("fiDeviceInstaller::Init: Failed to mount device to '%s'", partition->m_mountPath);
				continue;
			}

			partition->m_IsHddMounted = true;
		}

		if (partition->m_status == IDS_INSTALLED && partition->m_partitionDevice)
		{
			partition->FlagInstalledRpfs();
		}
	}

	sm_installPartitions.clear();
}
#endif // __XENON

#if __XENON
void fiDeviceInstaller::RefreshDeviceIds()
{
	const u32 maxHandles = 64;
	static XCONTENT_DATA enumeratedContent[MAX_XCONTENT_HANDLES];
	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

    // enumerate all content to get the data for newly downloaded packages
	DWORD numContent = 0;
	DWORD bufferSize;
	HANDLE contentEnum;
	DWORD rt = XContentCreateEnumerator(XUSER_INDEX_NONE, XCONTENTDEVICE_ANY, XCONTENTTYPE_MARKETPLACE, 0, maxHandles, &bufferSize, &contentEnum);
	if(rt == ERROR_SUCCESS)
	{
		Assert(bufferSize == maxHandles * sizeof(XCONTENT_DATA));

		while (true)
		{
			rt = XEnumerateEx(contentEnum, ENUMERATEFLAG_NONE, enumeratedContent, bufferSize, &numContent, NULL);
			if (rt == ERROR_ACCESS_DENIED)
			{
				Warningf("XEnumerateEx returned ERROR_ACCESS_DENIED, trying again...");
				sysIpcSleep(200);
			}
			else if (rt != ERROR_SUCCESS)
			{
				Warningf("InitInstall XEnumerateEx returned error 0x%x\n", rt);
				CloseHandle(contentEnum);
				return;
			}
			else
			{
				break;
			}
		}

		CloseHandle(contentEnum);
	}

	// for all partitions tagged as "wrong disc", we want to check if they're now found in the above enumeration
	// and in that case store the device id so we know where we can find them, store the file name to later be able
    // to identify the packages, and mark them as installed so they can be mounted in FinishInstall
	char filename[XCONTENT_MAX_FILENAME_LENGTH + 1];
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		fiDeviceInstaller* partition = sm_installPartitions[i];
        if (partition->m_status != IDS_FAILED_WRONG_DISC)
            continue;

		for (s32 i = 0; i < (s32)numContent; ++i)
		{
			sysMemCpy(filename, enumeratedContent[i].szFileName, XCONTENT_MAX_FILENAME_LENGTH);
			filename[XCONTENT_MAX_FILENAME_LENGTH] = 0;

			// mount the xcontent so we can access it
			DWORD ret = XContentCreate(XUSER_INDEX_ANY, partition->m_partitionName, &enumeratedContent[i], XCONTENTFLAG_OPENEXISTING, NULL, NULL, NULL);
			if (ret != ERROR_SUCCESS)
			{
				Warningf("InitInstall: XContentCreate failed with error %d", ret);
				continue;
			}
			else
			{
				// does this content package have our partition file in it? if not unmount and keep looking
				char destname[2048];
				partition->GetInstallName(destname, sizeof(destname), partition->m_sourceFile); // TODO: change sourceFile to something safer?

				if (localDevice.GetAttributes(destname) != FILE_ATTRIBUTE_INVALID)
				{
					// store actual name of package
					safecpy(partition->m_contentFileName, enumeratedContent[i].szFileName, XCONTENT_MAX_FILENAME_LENGTH + 1);
                    partition->m_status = IDS_INSTALLED;
                    partition->m_device = enumeratedContent[i].DeviceID;
					XContentClose(partition->m_partitionName, NULL);
					break;
				}

				XContentClose(partition->m_partitionName, NULL);
			}
		}
	}
}
#endif // __XENON

bool fiDeviceInstaller::InitPartitionDevice(const char* archiveFile, bool XENON_ONLY(setTimeStamp))
{
#if __XENON
	// We need to fake this to be on ODD if we're on USB and disc 2 is on HDD,
	// or if we're on HDD and disc 2 is on USB.
	// Or, in other words, if they're on two different devices, and disc 2 is not on ODD.
	XDEVICE_DATA packData = {0};
	if (XContentGetDeviceData(m_device, &packData) == ERROR_SUCCESS)
	{
		// check "game" symbol
		XCONTENTDEVICETYPE gameDeviceType;
		if (XContentQueryVolumeDeviceType("GAME", &gameDeviceType, NULL) == ERROR_SUCCESS)
		{
#if !__NO_OUTPUT
			char finalString[256];
			formatf(finalString, "Inst=%d, game=%d\n", packData.DeviceType, gameDeviceType);
			OutputDebugString(finalString);
#endif

			if (gameDeviceType != XCONTENTDEVICETYPE_ODD && gameDeviceType != packData.DeviceType)
			{
				m_partitionDevice->SetForceOnOdd(true);
			}
		}
		else
		{
			OUTPUT_ONLY(OutputDebugString("Cannot get device type of game\n");)
		}
	}
	else
	{
		OUTPUT_ONLY(OutputDebugString("Cannot get device type of install partition\n");)
	}
#endif


	if (!m_partitionDevice->Init(archiveFile, true, fiPackfile::CACHE_INSTALLER_FILE))
	{
		m_status = IDS_FAILED;
#if __XENON
		XContentClose(m_partitionName, NULL);
#endif
		Errorf("fiDeviceInstaller::InitPartitionDevice: Can't find and mount archive '%s'", archiveFile);
		return false;
	}

#if __XENON
	if (setTimeStamp)
		m_partitionDevice->SetPackfileTime(m_timestamp);
#endif
	return true;
}

void fiDeviceInstaller::FlagInstalledRpfs() const
{
	if (!HasFailed())
	{
		Assert(m_partitionDevice->GetNameHeap());
		char filename[256];
		s32 mountLength = (s32)strlen(m_mountPath);

		// go through all packfile entries and call the install done callback
		const fiPackEntry* entries = m_partitionDevice->GetEntries();
		for (u32 i = 0; i < m_partitionDevice->GetEntryCount(); ++i)
		{
			if (entries[i].IsFile())
			{
				safecpy(filename, m_mountPath, 256);
				m_partitionDevice->GetEntryFullName(i, filename + mountLength, 256);
				fileDoneCallback.Call(CallbackData(filename));
			}
		}
	}
}

bool fiDeviceInstaller::MountAs(const char* path, bool mountDevice)
{
    if (mountDevice)
    {
		if (fiDevice::Mount(path, *this, m_IsReadOnly))
		{
			m_filenameOffset = StringLength(path) - 1;
			return true;
		}
	}
	else
	{
	    if (m_partitionDevice->MountAs(path))
	    {
	        m_filenameOffset = StringLength(path) - 1;
	        return true;
	    }
	}
	return false;
}

const char *fiDeviceInstaller::FixName(char *dest,int destSize,const char *src) const
{
	strncpy(dest, m_prefix, destSize);
	strncat(dest, ASSET.FileName(src), destSize);
	return dest;
}

fiHandle fiDeviceInstaller::Open(const char *filename,bool readOnly) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().Open(path, readOnly);
}

fiHandle fiDeviceInstaller::OpenBulk(const char *filename,u64 &bias) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().OpenBulk(path,bias);
}

fiHandle fiDeviceInstaller::Create(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().Create(path);
}

int fiDeviceInstaller::Read(fiHandle handle,void *outBuffer,int bufferSize) const
{
	return fiDeviceLocal::GetInstance().Read(handle, outBuffer, bufferSize);
}

int fiDeviceInstaller::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const
{
	return fiDeviceLocal::GetInstance().ReadBulk(handle, offset, outBuffer, bufferSize);
}

int fiDeviceInstaller::Write(fiHandle handle,const void *buffer,int bufferSize) const
{
	return fiDeviceLocal::GetInstance().Write(handle, buffer, bufferSize);
}

int fiDeviceInstaller::Seek(fiHandle handle,int offset,fiSeekWhence whence) const
{
	return fiDeviceLocal::GetInstance().Seek(handle, offset, whence);
}

u64 fiDeviceInstaller::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const
{
	return fiDeviceLocal::GetInstance().Seek64(handle, offset, whence);
}

int fiDeviceInstaller::Close(fiHandle handle) const
{
	return fiDeviceLocal::GetInstance().Close(handle);
}

int fiDeviceInstaller::CloseBulk(fiHandle handle) const
{
	return fiDeviceLocal::GetInstance().CloseBulk(handle);
}

u64 fiDeviceInstaller::GetBulkOffset(const char* name) const {
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, name);
	return fiDeviceLocal::GetInstance().GetBulkOffset(path);
}


int fiDeviceInstaller::GetResourceInfo(const char* name,datResourceInfo &info) const {
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, name);
	return fiDeviceLocal::GetInstance().GetResourceInfo(path, info);
}


bool fiDeviceInstaller::Delete(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().Delete(path);
}

bool fiDeviceInstaller::Rename(const char *from,const char *to) const
{
	char path[RAGE_MAX_PATH];
	char path2[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, from);
	FixName(path2, RAGE_MAX_PATH, to);
	return fiDeviceLocal::GetInstance().Rename(path, path2);
}

bool fiDeviceInstaller::MakeDirectory(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().MakeDirectory(path);
}

bool fiDeviceInstaller::UnmakeDirectory(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().UnmakeDirectory(path);
}

u64 fiDeviceInstaller::GetFileSize(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().GetFileSize(path);
}

u64 fiDeviceInstaller::GetFileTime(const char *filename) const
{
#if __XENON
	if (m_timestamp != 0)
		return m_timestamp;
#endif

	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().GetFileTime(path);
}

bool fiDeviceInstaller::SetFileTime(const char *filename,u64 timestamp) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().SetFileTime(path, timestamp);
}

fiHandle fiDeviceInstaller::FindFileBegin(const char *filename,fiFindData &outData) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().FindFileBegin(path, outData);
}

bool fiDeviceInstaller::FindFileNext(fiHandle handle,fiFindData &outData) const
{
	return fiDeviceLocal::GetInstance().FindFileNext(handle, outData);
}

int fiDeviceInstaller::FindFileEnd(fiHandle handle) const
{
	return fiDeviceLocal::GetInstance().FindFileEnd(handle);
}

bool fiDeviceInstaller::SetEndOfFile(fiHandle handle) const
{
	return fiDeviceLocal::GetInstance().SetEndOfFile(handle);
}

u32 fiDeviceInstaller::GetAttributes(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().GetAttributes(path);
}

bool fiDeviceInstaller::SetAttributes(const char *filename,u32 attributes) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().SetAttributes(path, attributes);
}

u32 fiDeviceInstaller::GetPhysicalSortKey(const char *filename) const
{
	char path[RAGE_MAX_PATH];
	FixName(path, RAGE_MAX_PATH, filename);
	return fiDeviceLocal::GetInstance().GetPhysicalSortKey(path);
}

const fiDevice* fiDeviceInstaller::GetRpfDevice() const
{
	return m_partitionDevice;
}

const char *fiDeviceInstaller::GetDebugName() const
{
	return m_partitionName;
}

#if __XENON
void fiDeviceInstaller::StorePackageTimestamp()
{
	char destname[128];
	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

	// extract timestamp from the .timestamp file in the package
	char partitionIndex = m_partitionName[strlen(m_partitionName) - 1];
	if (partitionIndex >= '0' && partitionIndex <= '4')
	{
		formatf(destname, "%spart%c.timestamp", m_prefix, partitionIndex); // last character as index
	}
	else
	{
		formatf(destname, "%scommon.timestamp", m_prefix);
	}
	fiHandle timeStampFile = localDevice.Open(destname, true);

	if (Verifyf(fiIsValidHandle(timeStampFile), "Invalid file handle for timestamp file '%s'", destname))
	{
		// timestamp string is of format: yyyymmddhhmmss
		char timestamp[32] = {0};
		localDevice.Read(timeStampFile, timestamp, (s32)localDevice.GetFileSize(destname));
		localDevice.Close(timeStampFile);

		// convert to file time
		SYSTEMTIME st;
		char tmp[8];
		safecpy(tmp, timestamp, 5);
		st.wYear = (WORD)atoi(tmp);
		safecpy(tmp, timestamp + 4, 3);
		st.wMonth = (WORD)atoi(tmp);
		safecpy(tmp, timestamp + 6, 3);
		st.wDay = (WORD)atoi(tmp);
		safecpy(tmp, timestamp + 8, 3);
		st.wHour = (WORD)atoi(tmp);
		safecpy(tmp, timestamp + 10, 3);
		st.wMinute = (WORD)atoi(tmp);
		safecpy(tmp, timestamp + 12, 3);
		st.wSecond = (WORD)(atoi(tmp) + 1); // add a second to make sure these files don't automatically become older just because we don't have the millisecond data
		st.wMilliseconds = 0;

		FILETIME ft;
		if (SystemTimeToFileTime(&st, &ft))
		{
			m_timestamp = ((u64)ft.dwHighDateTime << 32) | ft.dwLowDateTime;
		}
	}
}

bool fiDeviceInstaller::IsPartitionPath(const char* filename)
{
	for (s32 i = 0; i < sm_installPartitions.GetCount(); ++i)
	{
		const char* c = strchr(filename, ':');
		s32 count = (s32)(c - filename);
		if (count > 0 && !strncmp(filename, sm_installPartitions[i]->m_prefix, count))
			return true;
	}
	return false;
}
#endif // __XENON

#if __PS3
bool fiDeviceInstaller::CopyContentInfoFile(const char* filename, const char* contentInfoPath)
{
	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

	char srcFile[64];
	char dstFile[CELL_GAME_PATH_MAX];
	formatf(srcFile, "/dev_bdvd/PS3_GAME/%s", filename);
	formatf(dstFile, "%s/%s", contentInfoPath, filename);
	fiHandle h = localDevice.Open(srcFile, true);
#if !__FINAL
	if (!fiIsValidHandle(h))
	{
		formatf(srcFile, "/app_home/%s", filename);
		h = localDevice.Open(srcFile, true);
	}
#endif

	if (!fiIsValidHandle(h))
		return false;
	
	u32 copySize = (u32)localDevice.GetFileSize(srcFile);
	char* buffer = Alloca(char, copySize);
	if (localDevice.Read(h, buffer, copySize) != copySize)
	{
		localDevice.Close(h);
		return false;
	}

	localDevice.Close(h);
	h = localDevice.Create(dstFile);
	if (!fiIsValidHandle(h))
		return false;

	if (localDevice.Write(h, buffer, copySize) != copySize)
	{
		localDevice.Close(h);
		localDevice.Delete(dstFile);
		return false;
	}

	localDevice.Close(h);
	return true;
}
#endif // __PS3

#if __XENON
struct sRebootData
{
	static const u32 MAX_DATA_SIZE = MAX_LAUNCH_DATA_SIZE - sizeof(u32);

	u32 id;
	u8 data[MAX_DATA_SIZE];
};

void fiDeviceInstaller::Reboot(u32 id)
{
	sRebootData data = {0};
	data.id = id;
	XSetLaunchData(&data, sizeof(data));
# if !__FINAL
	const char* progName = sysParam::GetProgramName();
	Displayf("Rebooting executable: %s", progName);
	XLaunchNewImage(progName,0);
# else
	Displayf("Rebooting executable: default.xex");
	XLaunchNewImage("default.xex",0);   // see xbox360 docs for filename rules
# endif
}

bool fiDeviceInstaller::HasRebooted(u32 id)
{
	static u32 rebootId = 0;
	static bool bQueried = false;

	if (!bQueried)
	{
		bQueried = true;
		DWORD dwLaunchDataSize = 0;
		DWORD dwStatus = XGetLaunchDataSize(&dwLaunchDataSize);
		if (dwStatus == ERROR_SUCCESS)
		{
			sRebootData data = {0};
			dwStatus = XGetLaunchData((BYTE*)&data, dwLaunchDataSize);
			Assert(dwStatus == ERROR_SUCCESS);
			rebootId = data.id;
		}
	}

	return rebootId == id;
}
#endif // __XENON
}		// namespace rage
