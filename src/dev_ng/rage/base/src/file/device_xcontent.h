//
// filename:	device_xcontent.h
// description:	
//

#ifndef INC_DEVICE_XCONTENT_H_
#define INC_DEVICE_XCONTENT_H_

#if __XENON

#include "file/device.h"

/*
#define XCONTENT_MAX_DISPLAYNAME_LENGTH 128
#define XCONTENT_MAX_FILENAME_LENGTH    42
typedef DWORD                           XCONTENTDEVICEID, *PXCONTENTDEVICEID;
struct XCONTENT_DATA
{
XCONTENTDEVICEID                    DeviceID;
DWORD                               dwContentType;
WCHAR                               szDisplayName[XCONTENT_MAX_DISPLAYNAME_LENGTH]
CHAR                                szFileName[XCONTENT_MAX_FILENAME_LENGTH];
} */

struct _XCONTENT_DATA;

namespace rage {

const int XCONTENT_DATA_SIZE = (4+4+2*128+42+2/*padding*/);

class fiDeviceXContent : public fiDeviceLocal
{
public:
	fiDeviceXContent();
	~fiDeviceXContent() {Shutdown();}

	bool Init(s32 userIndex, _XCONTENT_DATA* pData);
	bool MountAs(const char* pName);
	void Shutdown();

private:
	s32				m_userIndex;
	u8				m_contentData[XCONTENT_DATA_SIZE];
	char			m_mountName[32];
};

}	// namespace rage

#endif // __XENON

#endif // !INC_DEVICE_XCONTENT_H_

