// 
// file/asynchstream.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if 0
#include "asynchstream.h"
#include "stream.h"
#include "asset.h"
#include "math/amath.h"
#include "system/new.h"

namespace rage {

fiAsynchStream::fiAsynchStream() : m_Handle(pgStreamer::Error), m_Storage(NULL), m_Size(0), m_Completed(false)
{
}


fiAsynchStream::~fiAsynchStream() {
	AssertMsg(!m_Completed,"fiAsynchStream finished but nobody called Poll");
}


bool fiAsynchStream::Init(const char *filename,const char *ext) {
	char buf[RAGE_MAX_PATH];
	if (!ASSET.FullReadPath(buf,sizeof(buf),filename,ext))
		return false;
	Assert(m_Handle == pgStreamer::Error);
	m_Handle = pgStreamer::Open(buf,&m_Size,0);
	if (m_Handle == pgStreamer::Error)
		return false;
	m_Storage = rage_new char[m_Size];
	m_Completed = false;
	datResourceChunk chunk;
	chunk.SrcAddr = 0;
	chunk.DestAddr = m_Storage;
	chunk.Size = m_Size;
	if (!pgStreamer::Read(m_Handle,&chunk,1,0,Callback,this)) {
		delete[] m_Storage;
		m_Storage = NULL;
		pgStreamer::Close(m_Handle);
		m_Handle = pgStreamer::Error;
		return false;
	}
	return true;
}


bool fiAsynchStream::Init(const char *filename,const char *ext, int offset, int size) {
	char buf[RAGE_MAX_PATH];
	if (!ASSET.FullReadPath(buf,sizeof(buf),filename,ext))
		return false;
	Assert(m_Handle == pgStreamer::Error);
	m_Handle = pgStreamer::Open(buf,&m_Size,false);
	if (m_Handle == pgStreamer::Error)
		return false;
	size = Min(size, (int)m_Size - offset);
	m_Size = size;
	m_Storage = rage_new char[size];
	m_Completed = false;
	datResourceChunk chunk;
	chunk.SrcAddr = 0;
	chunk.DestAddr = m_Storage;
	chunk.Size = size;
	if (!pgStreamer::Read(m_Handle,&chunk,1,offset,Callback,this)) {
		delete[] m_Storage;
		m_Storage = NULL;
		pgStreamer::Close(m_Handle);
		m_Handle = pgStreamer::Error;
		return false;
	}
	return true;
}

void fiAsynchStream::Callback(void *userArg,void*,u32,u32) {
	fiAsynchStream *that = (fiAsynchStream*)userArg;
	that->m_Completed = true;
}


bool fiAsynchStream::GetMemoryFileName(char *dest,int destSize) {
	if (m_Storage && m_Completed) {
		m_Completed = false;
		pgStreamer::Close(m_Handle);
		m_Handle = pgStreamer::Error;
		fiDevice::MakeMemoryFileName(dest,destSize,m_Storage,m_Size,true,"fiAsynchStream file");
		return true;
	}
	else
		return false;
}


fiStream* fiAsynchStream::Poll() {
	char temp[RAGE_MAX_PATH];
	if (GetMemoryFileName(temp,sizeof(temp))) {
		fiStream *stream = fiStream::Open(temp);
		if (!stream) {
			// If stream didn't open, reclaim the memory now since fiStream obviously never will.
			Errorf("fiAsynchStream - Unable to create stream after file loaded?");
			delete[] m_Storage;
		}
		return stream;
	}
	else
		return NULL;
}


}	// namespace rage

#endif
