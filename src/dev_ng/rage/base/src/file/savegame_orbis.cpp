// 
// file/savegame_orbis.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include "savegame.h"

#include <scebase_common.h>
#include <_kernel.h>
#include <_fs.h>
#include <libsysmodule.h>
#include <save_data.h>
#include <save_data_dialog.h>
#include <sdk_version.h>

#pragma comment(lib,"libSceSaveData_stub_weak.a")


#include "atl/array.h"
#include "data/bitbuffer.h"
#include "diag/channel.h"
#include "diag/seh.h"
#include "file/device.h"
#include "file/stream.h"
#include "rline/rlnp.h"
#include "system/memmanager.h"
#include "system/nelem.h"
#include "string/string.h"
#include "string/unicode.h"
#include "system/ipc.h"
#include "system/param.h"
#include "system/timer.h"

#include <string.h>

#if __BANK
PARAM(usePS3saves, "Skip header check to allow for PS3 save files");
#endif

namespace rage
{

	extern char *XEX_TITLE_ID;

	RAGE_DEFINE_CHANNEL(savegameorbis);

#define sgoAssertf(cond,fmt,...)	RAGE_ASSERTF(savegameorbis,cond,fmt,##__VA_ARGS__)
#define sgoDisplayf(fmt,...) RAGE_DISPLAYF(savegameorbis, fmt, ##__VA_ARGS__)
#define sgoDebug1(fmt, ...)  RAGE_DEBUGF1(savegameorbis, fmt, ##__VA_ARGS__)
#define sgoDebug2(fmt, ...)  RAGE_DEBUGF2(savegameorbis, fmt, ##__VA_ARGS__)
#define sgoDebug3(fmt, ...)  RAGE_DEBUGF3(savegameorbis, fmt, ##__VA_ARGS__)
#define sgoWarning(fmt, ...) RAGE_WARNINGF(savegameorbis, fmt, ##__VA_ARGS__)
#define sgoError(fmt, ...)   RAGE_ERRORF(savegameorbis, fmt, ##__VA_ARGS__)


	RAGE_DEFINE_SUBCHANNEL(savegameorbis,timing)

#define sgotimingWarningf(fmt,...)					RAGE_WARNINGF(savegameorbis_timing,fmt,##__VA_ARGS__)
#define sgotimingDisplayf(fmt,...)					RAGE_DISPLAYF(savegameorbis_timing,fmt,##__VA_ARGS__)
#define sgotimingDebugf1(fmt,...)					RAGE_DEBUGF1(savegameorbis_timing,fmt,##__VA_ARGS__)
#define sgotimingDebugf2(fmt,...)					RAGE_DEBUGF2(savegameorbis_timing,fmt,##__VA_ARGS__)
#define sgotimingDebugf3(fmt,...)					RAGE_DEBUGF3(savegameorbis_timing,fmt,##__VA_ARGS__)


	using namespace fiSaveGameState;

	fiSaveGame SAVEGAME;

	char *pIconImage = NULL;
	u32 SizeOfIconImage = 0;

	char sLocalisedBackupString[SCE_SAVE_DATA_SUBTITLE_MAXSIZE] = "Backup";

#define SAVE_DATA_MAX_DISPLAY_NAME	(128)
#define SAVE_DATA_ICON_DATASIZE		(128)
#define SAVE_DATA_FILEPATH_LENGTH	(63)
#define SAVE_DATA_ICON_BUFSIZE		(5*1024)

#define SAVE_DATA_DIRNAME_PREFIX	"SAVEDATA"
#define SAVE_DATA_SAVEGAME_PREFIX_FOR_ENUMERATION	SAVE_DATA_DIRNAME_PREFIX "%"
#define SAVE_DATA_DIRNAME_DEFAULT	SAVE_DATA_DIRNAME_PREFIX "00"

#define SAVE_DATA_DIRNAME_PROFILE	SAVE_DATA_DIRNAME_PREFIX "PROFILE"
#define SAVE_DATA_FILENAME_PROFILE	"Profile"

#define SAVE_DATA_DIRNAME_PHOTO		SAVE_DATA_DIRNAME_PREFIX "PGTA5"
#define SAVE_DATA_FILENAME_PHOTO	"PGTA5"

#define SAVE_DATA_DIRNAME_MAIN_SAVEGAME SAVE_DATA_DIRNAME_PREFIX "SGTA5"

#define SAVE_DATA_DIRNAME_BACKUP		SAVE_DATA_DIRNAME_PREFIX "SBGTA5"
#define SAVE_DATA_FILENAME_BACKUP		"SBGTA5"

#define SAVE_DATA_DIRNAME_SPACE_CHECK	SAVE_DATA_DIRNAME_PREFIX "CHECK"
#define SAVE_DATA_FILENAME_SPACE_CHECK	"CHECK"

#define REPLAY_DIRNAME_PREFIX		"REPLAY"
#define REPLAY_DIRNAME_PREFIX_FOR_ENUMERATION		REPLAY_DIRNAME_PREFIX "%"

#define REPLAY_DIRNAME_CLIP		REPLAY_DIRNAME_PREFIX "CLIP"
#define REPLAY_FILENAME_CLIP		"CLIP"

#define REPLAY_DIRNAME_MONTAGE		REPLAY_DIRNAME_PREFIX "MONT"
#define REPLAY_FILENAME_MONTAGE		"MONT"

#define SAVE_DATA_MAX_PROFILE_SIZE		(1 * 1024 * 1024) // 1 MB, 1 048 576 bytes
#define SAVE_DATA_MAX_GAME_SIZE			(2 * 1024 * 1024) // 2 MB, 2 097 152 bytes
#define SAVE_DATA_MAX_PHOTO_SIZE		(800 * 1024) // 800 KB, 819 200 bytes
#define SAVE_DATA_MAX_CLIP_SIZE			(37 * 4 * 1024 * 1024) // 148 MB, 155 189 248 bytes
#define SAVE_DATA_MAX_MONTAGE_SIZE		(50 * 1024 * 1024) // 50 MB, 52 428 800 bytes
#define SAVE_DATA_MAX_NUMBER_OF_PHOTOS	(96)

#define SAVEGAME_STACK_SIZE	16384

#if USE_SAVE_DATA_MEMORY
#define SAVEDATAMEMORY_FULL_NAME "SAVEDATASGTA50015"
#define SAVEDATAMEMORY_NAME "SGTA50015"
#define SAVEDATAMEMORY_SIZE (1024*1024)
#define THREAD_PRIORITY PRIO_BELOW_NORMAL
#else
#define THREAD_PRIORITY PRIO_NORMAL
#endif

#if USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP
#define DOWNLOAD0_AUTOSAVE_BACKUP_FULL_NAME		"SAVEDATASBGTA50015"
#define DOWNLOAD0_AUTOSAVE_BACKUP_NAME			"SBGTA50015"
#endif	//	USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP

#if USE_DOWNLOAD0_FOR_PROFILE_BACKUP
//	#define DOWNLOAD0_PROFILE_BACKUP_FULL_NAME		"SAVEDATAPROFILEB"
#define DOWNLOAD0_PROFILE_BACKUP_NAME			"PROFILEB"
#endif	//	USE_DOWNLOAD0_FOR_PROFILE_BACKUP

	typedef bool(*tValidateFunc)(fiHandle handle, const char* path);

	// When calculating free space, add some extra space to save file size for background processes.
	static const u32 EXTRA_BUFFER_SIZE = (2 * 1024 * 1024);

	static sysIpcThreadId s_ThreadId = sysIpcThreadIdInvalid;
	static volatile bool s_FileOperationDone, s_FileOperationError, s_Overwrite;
	static const char* s_Filename;
	static const char16* s_DisplayName;
	static const void* s_SaveData;
	static fiSaveGame::Content* s_OutContent;
	static volatile u32 s_SizeOfCurrentFile, s_ExtraSpaceRequired;
	static volatile int s_Count, s_MaxCount;
	static volatile int s_iEnumerateType;
	static volatile u32 s_modTimeHigh, s_modTimeLow;
	static volatile bool s_fileExists;
	static tValidateFunc s_ValidateFunc;
	static void *s_pBufferToLoadInto;
	static volatile u32 s_SizeOfBufferToLoadInto;
	static volatile u32 s_SizeOfLoadedData;

	static volatile u64 s_FileSize;

	static const char* s_MountNameToDelete;
	static volatile bool s_bMountNameToDeleteIsAFilename;

	static char s_TitleString[SCE_SAVE_DATA_TITLE_MAXSIZE];
	static volatile SceUserServiceUserId s_UserId;

	class OrbisProfile
	{
	public:
		enum eSavegameType
		{
			SG_TYPE_SAVEGAME,
			SG_TYPE_PROFILE,
			SG_TYPE_PHOTO,
			SG_TYPE_CLIP,
			SG_TYPE_MONTAGE,
			SG_TYPE_SPACE_CHECK,
			SG_TYPE_BACKUP
		};

	public:
		OrbisProfile();
		~OrbisProfile();

		bool BeginSelectDevice(u32 contentType,u32 minSpaceAvailable,bool forceShow,bool manageStorage);
		bool CheckSelectDevice();
		void EndSelectDevice();

		bool BeginEnumeration(u32 contentType,fiSaveGame::Content *outContent,int maxCount,int saveGameType, void* validateFunc);
		bool CheckEnumeration(int &outCount);
		void EndEnumeration();

		bool BeginSave(u32 contentType,const char16 *contentName,const char *filename,const void *saveData,u32 saveSize,bool overwrite);
		bool CheckSave(bool &outIsValid,bool &fileExists);
		void EndSave();

		bool BeginLoad(u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize);
		bool CheckLoad(bool &outIsValid,u32 &loadSize);
		void EndLoad();

		bool BeginDeleteFile(u32 contentType,const char *filename);
		bool CheckDeleteFile();
		void EndDeleteFile();

		bool BeginDeleteMount(const char *pName, bool bIsFilename);
		bool CheckDeleteMount();
		void EndDeleteMount();

		bool BeginGetCreator(u32 contentType,u32 deviceId,const char *filename);
		bool CheckGetCreator(bool &bIsCreator);
		void EndGetCreator();

		bool BeginFreeSpaceCheck(const char *filename,u32 saveSize);
		bool CheckFreeSpaceCheck(int &ExtraSpaceRequired);
		void EndFreeSpaceCheck();

		bool BeginGetFileSize(const char *filename, bool bCalculateSizeOnDisk);
		bool CheckGetFileSize(u64 &fileSize);
		void EndGetFileSize();

		bool BeginGetFileModifiedTime(const char *filename);
		bool CheckGetFileModifiedTime(u32& modTimeHigh, u32& modTimeLow);
		void EndGetFileModifiedTime();

		bool BeginGetFileExists(const char *filename);
		bool CheckGetFileExists(bool& exists);
		void EndGetFileExists();

		static bool GetDisplayNameAndModifiedTime(u32 signInId, fiSaveGame::Content &outContent, const char* mountName, const char* fileName);
		static u64 CalculateDataSizeOnDisk(rage::u32 dataSize);
		u64 GetTotalAvailableSpace();

		static int SetParam(const SceSaveDataMountPoint *mountPoint, bool isBackup);
		static int GetParam(const SceSaveDataMountPoint *mountPoint);

		void SetStateToIdle(){m_State = IDLE;}
		fiSaveGameState::State GetState() const { return m_State; }
		void SetUserId(int userId) { m_UserId = userId; };
		void SetSelectedDirectory(const char* directory) { safecpy(m_SelectedDirName.data, directory); }

		static int SaveFile(const char *mountPoint, const char *fileName, const char16* displayName, const void *saveData, const size_t dataSize);
		static int LoadFile(const char *mountPoint, const char *fileName, void *loadData, u32 loadSize);

#if USE_SAVE_DATA_MEMORY
		u32 InitialiseSaveDataMemory(void);
		static u32 InitialiseSaveDataMemory(SceUserServiceUserId);
#endif

	private:
		static void SetupSceSaveDataMount(const SceUserServiceUserId userId, const SceSaveDataMountMode mode, SceSaveDataMount *m, u32 maxMountSize, eSavegameType savegameType, const char* fileName);

		static eSavegameType GetSavegameTypeFromFilename(const char *pFilename, bool bCheckDirName);
		static u32 GetMaxMountSizeFromSavegameType(eSavegameType savegameType);

		static void GetDirectoryNameFromFileNameAndType(eSavegameType savegameType, const char* fileName, SceSaveDataDirName &outDirName);

		static void ShowNoSpaceDialog(const SceSaveDataMount &mount, const SceSaveDataMountResult &mountResult);
		static bool MountForSaving(const char *pFilename, SceSaveDataMountResult &mountResult, bool &bHasBeenCreated);
		static bool MountForFileDeletion(const char *pFilename, SceSaveDataMountResult &mountResult);
		static bool MountForReading(const char *pFilename, SceSaveDataMountPoint &mountPoint, bool &bUnmountWhenFinished);
		static int MountForEnumeration(const char *pFilename, SceSaveDataMountPoint &mountPoint, bool &bUnmountWhenFinished);

		static void MountPhotoDirectoryForReading(const SceSaveDataMountPoint &mountPoint);
		static bool CheckIfPhotoDirectoryIsMountedForReading(SceSaveDataMountPoint &mountPoint);
		static int UnmountPhotoDirectoryForReading();


		static int DeleteMount(const SceSaveDataDirName* dirName);

		static void s_BeginFileSave(void*);
		static void s_FileLoad(void*);
		static void s_BeginEnumeration(void*);
		static void s_BeginFreeSpaceCheck(void*);
		static void s_GetFileSize(void*);
		static void s_FileDeletion(void*);
		static void s_DeleteMount(void*);
		static void s_GetFileModifiedTime(void*);
		static void s_GetFileExists(void*);

		static bool MountPhotosForEnumeration(const char* fileName);
		static bool MountClipsForEnumeration(const char* fileName);

		static int CompareModifiedTimes(const void* _a, const void* _b);
				
#if USE_SAVE_DATA_MEMORY
		static void s_BeginFileSaveDataMemory(void*);
		static void s_FileLoadSaveDataMemory(void*);
		static void s_GetFileSizeSaveDataMemory(void*);
		static void s_FileDeletionSaveDataMemory(void*);
		static void s_GetFileModifiedTimeSaveDataMemory(void*);
		static void s_GetFileExistsSaveDataMemory(void*);
		static bool ReadSaveDataMemoryHeader(class Header*);
		static bool ReadSaveDataMemory(void * buffer, size_t size);
		static bool WriteSaveDataMemory(void * buffer, size_t size);
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
		enum eDownload0Folder
		{
			DOWNLOAD0_USER_FOLDER,
			DOWNLOAD0_SAVEGAME_FOLDER
		};

		static void GetNameOfDownload0Path(char *path, s32 stringLength, const char *pFilename, eDownload0Folder folderType);
		static bool CreateFolderInDownload0(const char *pFolderName);

		static bool ShouldUseDownload0(const char *pFilename);

		static void s_FileSaveDownload0Backup(void*);
		static void s_FileLoadDownload0Backup(void*);
		static void s_GetFileSizeDownload0Backup(void*);
		static void s_FileDeletionDownload0Backup(void*);
		static void s_GetFileModifiedTimeDownload0Backup(void*);
		static void s_GetFileExistsDownload0Backup(void*);
		static bool ReadDownload0BackupHeader(const char *pPath, class Header*);
		static bool ReadDownload0Backup(const char *pPath, void * buffer, size_t size);
		static bool WriteDownload0Backup(const char *pPath, void * buffer, size_t size, bool forceSync=false);
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

#if !__FINAL
		static void VerifySaveLocation(const SceSaveDataMountPoint *mountPoint);
#endif // !__FINAL
		
		SceUserServiceUserId m_UserId;

		static SceSaveDataTitleId	m_TitleId;
		static SceSaveDataDirName	m_SelectedDirName;
		static SceSaveDataDirName	m_ProfileDirName;

		static SceSaveDataMountPoint	ms_ReadPhotoMountPoint;
		static bool						ms_bPhotoDirectoryIsMountedForReading;

#if USE_DOWNLOAD0_FOR_BACKUP
		static bool					ms_bDownload0FolderExistsForUser;	//	Should this be static? In theory, there should be one flag per profile so shouldn't be static, but the player can't change profile once the game is running.
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

		State m_State;
	};

	class Header
	{
	public:
		static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(u32) +	// m_Version
			sizeof(char16) * SAVE_DATA_MAX_DISPLAY_NAME +				// m_DisplayName
			sizeof(u32)													// m_Hash
#if USE_SAVE_DATA_MEMORY || USE_DOWNLOAD0_FOR_BACKUP
			+ sizeof(u32)												// m_size
			+ sizeof(time_t)											// m_time
#endif
			;



		static const unsigned FILE_VERSION = 1;

		u32		m_Version;
		char16	m_DisplayName[SAVE_DATA_MAX_DISPLAY_NAME];
		u32		m_Hash;
#if USE_SAVE_DATA_MEMORY || USE_DOWNLOAD0_FOR_BACKUP
		u32		m_size;
		time_t	m_mtime;
#endif

		u32 GetExpectedVersion() const {return FILE_VERSION;}

		u32 GetVersion() {return m_Version;}
		char16* GetDisplayName() {return m_DisplayName;}

		Header()
		{
			Clear();
		}

		void Clear()
		{
			m_Version = 0;
			memset(m_DisplayName, 0, sizeof(m_DisplayName));
			m_Hash = 0;
#if USE_SAVE_DATA_MEMORY || USE_DOWNLOAD0_FOR_BACKUP
			m_size = 0;
			m_mtime = 0;
#endif
		}

		u32 CalculateHash(u32 version, const char16 (&displayName)[SAVE_DATA_MAX_DISPLAY_NAME]) const
		{
			u32 hash = atDataHash((const char*)&version, sizeof(version));
			return atDataHash((const char*)displayName, sizeof(displayName), hash);
		}

		bool Import(const void* buf,
			const unsigned sizeOfBuf,
			unsigned* size = 0)
		{
			datImportBuffer bb;
			bb.SetReadOnlyBytes(buf, sizeOfBuf);

			bool success = false;

			Clear();

			rtry
			{
				rverify(buf, catchall, );

				// read
				rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
				rverify(bb.ReadBytes(m_DisplayName, sizeof(m_DisplayName)), catchall, );
				rverify(bb.ReadUns(m_Hash, sizeof(m_Hash) << 3), catchall, );
#if USE_SAVE_DATA_MEMORY || USE_DOWNLOAD0_FOR_BACKUP
				rverify(bb.ReadUns(m_size, sizeof(m_size) << 3), catchall, );
				rverify(bb.ReadBytes(&m_mtime, sizeof(m_mtime)), catchall, );
#endif

				// verify
				rcheck(GetVersion() == GetExpectedVersion(), catchall, );
				u32 hash = CalculateHash(m_Version, m_DisplayName);
				rverify(hash == m_Hash, catchall, );

				success = true;        
			}
			rcatchall
			{
			}

			if(size){*size = success ? bb.GetNumBytesRead() : 0;}

			return success;
		}

		bool Export(const char16 (&displayName)[SAVE_DATA_MAX_DISPLAY_NAME],
			void* buf,
			const unsigned sizeOfBuf,
			unsigned* size) const
		{
			datExportBuffer bb;
			bb.SetReadWriteBytes(buf, sizeOfBuf);

			bool success = false;

			unsigned version = GetExpectedVersion();

			rtry
			{
				rverify(buf, catchall, );
				rverify(sizeOfBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

				u32 hash = CalculateHash(version, displayName);

				rverify(bb.WriteUns(version, sizeof(version) << 3), catchall, );
				rverify(bb.WriteBytes(displayName, sizeof(displayName)), catchall, );
				rverify(bb.WriteUns(hash, sizeof(hash) << 3), catchall, );
#if USE_SAVE_DATA_MEMORY || USE_DOWNLOAD0_FOR_BACKUP
				rverify(bb.WriteUns(m_size, sizeof(m_size) << 3), catchall, );
				rverify(bb.WriteBytes(&m_mtime, sizeof(m_mtime)), catchall, );
#endif
				success = true;        
			}
			rcatchall
			{
			}

			if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

			return success;
		}
	};


	SceSaveDataTitleId	OrbisProfile::m_TitleId;
	SceSaveDataDirName	OrbisProfile::m_SelectedDirName;
	SceSaveDataDirName	OrbisProfile::m_ProfileDirName;

	SceSaveDataMountPoint	OrbisProfile::ms_ReadPhotoMountPoint;
	bool					OrbisProfile::ms_bPhotoDirectoryIsMountedForReading = false;

#if USE_DOWNLOAD0_FOR_BACKUP
	bool					OrbisProfile::ms_bDownload0FolderExistsForUser = false;
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

	static atRangeArray<OrbisProfile,fiSaveGame::SIGNIN_COUNT> s_UserProfiles;


	OrbisProfile::OrbisProfile()
	{
		m_UserId = SCE_USER_SERVICE_USER_ID_INVALID;

		// userId
		memset(&m_UserId, 0x00, sizeof(m_UserId));

		// titleid
		memset(&m_TitleId, 0, sizeof(m_TitleId));
		memcpy(m_TitleId.data, XEX_TITLE_ID, SCE_SAVE_DATA_TITLE_ID_DATA_SIZE);

		// dirName
		memset(&m_SelectedDirName, 0x00, sizeof(m_SelectedDirName));
		strncpy(m_SelectedDirName.data, SAVE_DATA_DIRNAME_DEFAULT, sizeof(SAVE_DATA_DIRNAME_DEFAULT)-1);
		m_SelectedDirName.data[sizeof(SAVE_DATA_DIRNAME_DEFAULT)-1] = '\0';

		// profile dirName
		memset(&m_ProfileDirName, 0x00, sizeof(m_ProfileDirName));
		strncpy(m_ProfileDirName.data, SAVE_DATA_DIRNAME_PROFILE, sizeof(SAVE_DATA_DIRNAME_PROFILE)-1);
		m_ProfileDirName.data[sizeof(SAVE_DATA_DIRNAME_PROFILE)-1] = '\0';


		memset(&ms_ReadPhotoMountPoint, 0, sizeof(ms_ReadPhotoMountPoint));
		ms_bPhotoDirectoryIsMountedForReading = false;

#if USE_DOWNLOAD0_FOR_BACKUP
		ms_bDownload0FolderExistsForUser = false;
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
	}

	OrbisProfile::~OrbisProfile() {}

#if !__FINAL
	void OrbisProfile::VerifySaveLocation(const SceSaveDataMountPoint *mountPoint)
	{
		SceSaveDataMountInfo mountInfo;
		memset(&mountInfo, 0x00, sizeof(mountInfo));
		if (sceSaveDataGetMountInfo(mountPoint, &mountInfo) == SCE_OK)
		{
			if (mountInfo.blocks == mountInfo.freeBlocks)
			{
				Warningf("Save data is being saved to a development location.");
			}
		}
	}
#endif // !__FINAL


	void OrbisProfile::MountPhotoDirectoryForReading(const SceSaveDataMountPoint &mountPoint)
	{
		sgoDebug1("OrbisProfile::MountPhotoDirectoryForReading - called for %s", mountPoint.data);
		memcpy(&ms_ReadPhotoMountPoint, &mountPoint, sizeof(ms_ReadPhotoMountPoint));
		ms_bPhotoDirectoryIsMountedForReading = true;
	}

	bool OrbisProfile::CheckIfPhotoDirectoryIsMountedForReading(SceSaveDataMountPoint &mountPoint)
	{
		if (ms_bPhotoDirectoryIsMountedForReading)
		{
			sgoDebug1("OrbisProfile::CheckIfPhotoDirectoryIsMountedForReading - directory is already mounted so returning %s", ms_ReadPhotoMountPoint.data);
			memcpy(&mountPoint, &ms_ReadPhotoMountPoint, sizeof(mountPoint));
			return true;
		}

		return false;
	}

	int OrbisProfile::UnmountPhotoDirectoryForReading()
	{
		if (ms_bPhotoDirectoryIsMountedForReading)
		{
			ms_bPhotoDirectoryIsMountedForReading = false;
			int ret = sceSaveDataUmount(&ms_ReadPhotoMountPoint);
			if(ret < SCE_OK)
			{
				sgoError("OrbisProfile::UnmountPhotoDirectoryForReading - sceSaveDataUmount - Couldn't unmount ms_ReadPhotoMountPoint %s, 0x%08x\n", ms_ReadPhotoMountPoint.data, ret);
			}
			else
			{
				sgoDebug1("OrbisProfile::UnmountPhotoDirectoryForReading - ms_ReadPhotoMountPoint %s has been unmounted", ms_ReadPhotoMountPoint.data);
			}
			memset(&ms_ReadPhotoMountPoint, 0, sizeof(ms_ReadPhotoMountPoint));

			return ret;
		}

		return SCE_OK;
	}


	void OrbisProfile::ShowNoSpaceDialog(const SceSaveDataMount &mount, const SceSaveDataMountResult &mountResult)
	{
		g_rlNp.GetCommonDialog().ShowSaveDialogNoSpace(mount.userId, mount.titleId, mount.dirName, mountResult.requiredBlocks);
		g_rlNp.GetCommonDialog().Update();

		while(g_rlNp.GetCommonDialog().GetStatus() == SCE_COMMON_DIALOG_STATUS_RUNNING)
		{
			sysIpcSleep(1);
			g_rlNp.GetCommonDialog().Update();
		}
	}

	bool OrbisProfile::MountForSaving(const char *pFilename, SceSaveDataMountResult &mountResult, bool &bHasBeenCreated)
	{
		//	Save (delete if damaged, create if doesn't already exist)
		//	Display a warning if there's not enough free space

		bHasBeenCreated = false;

		SceSaveDataMount mount;
		SceSaveDataMountMode mode = SCE_SAVE_DATA_MOUNT_MODE_RDWR;	//	SCE_SAVE_DATA_MOUNT_MODE_WRONLY;
		eSavegameType savegameType = GetSavegameTypeFromFilename(pFilename, false);
		u32 maxMountSize = GetMaxMountSizeFromSavegameType(savegameType);

		if (savegameType == SG_TYPE_PHOTO)
		{
			sgoAssertf(s_SizeOfCurrentFile <= SAVE_DATA_MAX_PHOTO_SIZE, "OrbisProfile::MountForSaving - actual size of photo file (%u) is greater than SAVE_DATA_MAX_PHOTO_SIZE (%u)", s_SizeOfCurrentFile, SAVE_DATA_MAX_PHOTO_SIZE);

			UnmountPhotoDirectoryForReading();	//	Should I return false if this call returns an error?
		}

//	First pass - try to overwrite an existing mount directory
//	Second pass - try to create a new mount directory. We'll only do this if the directory didn't already exist or we deleted the directory in the first pass because it was broken.
		u32 nAttempt = 0;
		while (nAttempt < 2)
		{
			OrbisProfile::SetupSceSaveDataMount(s_UserId, mode, &mount, maxMountSize, savegameType, pFilename);

			if (nAttempt == 0)
			{
				sgotimingDebugf1("%d - about to call sceSaveDataMount for writing",sysTimer::GetSystemMsTime());
			}
			else
			{
				sgotimingDebugf1("%d - about to call sceSaveDataMount for creation",sysTimer::GetSystemMsTime());
			}

			// set the mount point
			memset(&mountResult, 0x00, sizeof(mountResult));
			int ret = sceSaveDataMount(&mount, &mountResult);

			sgotimingDebugf1("%d - sceSaveDataMount has finished",sysTimer::GetSystemMsTime());

			if (ret == SCE_OK)
			{
				if (nAttempt == 1)
				{
					bHasBeenCreated = true;
				}
				return true;
			}

			switch (ret)
			{
				case SCE_SAVE_DATA_ERROR_NOT_FOUND :
					//	Proceed to create a new directory
					break;

				case SCE_SAVE_DATA_ERROR_BROKEN :
					//	Delete the broken mount directory
					//	then proceed to create a new directory
					{
						SceSaveDataDelete del;
						memset(&del, 0x00, sizeof(del));
						del.userId = mount.userId;
						del.titleId = mount.titleId;
						del.dirName = mount.dirName;

						sgotimingDebugf1("%d - about to call sceSaveDataDelete for an existing corrupt file",sysTimer::GetSystemMsTime());

						int deleteResult = sceSaveDataDelete(&del);

						sgotimingDebugf1("%d - sceSaveDataDelete has finished",sysTimer::GetSystemMsTime());

						if ( deleteResult == SCE_OK )
						{
							sgoDebug1("OrbisProfile::MountForSaving - existing directory was corrupt so deleted it");
						}
						else
						{
							sgoError("OrbisProfile::MountForSaving - existing directory was corrupt so attempted to delete it. sceSaveDataDelete returned 0x%08x\n", deleteResult);
							return false;
						}
					}
					break;

				case SCE_SAVE_DATA_ERROR_NO_SPACE_FS :
					ShowNoSpaceDialog(mount, mountResult);
					return false;
//					break;

//				SCE_SAVE_DATA_ERROR_NO_SPACE	//	Apparently this can no longer happen
				default:
					sgoError("OrbisProfile::MountForSaving - sceSaveDataMount returned 0x%08x\n", ret);
					return false;
//					break;
			}

			//	Try to mount again, but this time Create the directory
			mode = SCE_SAVE_DATA_MOUNT_MODE_CREATE|SCE_SAVE_DATA_MOUNT_MODE_RDWR;
			nAttempt++;
		}

		sgoError("OrbisProfile::MountForSaving - failed to write or create %s\n", mount.dirName->data);
		return false;
	}

	int OrbisProfile::DeleteMount(const SceSaveDataDirName* dirName)
	{
		int ret = SCE_OK;

		// Check that the directory to be deleted actually exists
		// Obtain list of directories. There should only be one match
		SceSaveDataDirNameSearchCond cond;
		memset(&cond, 0x00, sizeof(SceSaveDataDirNameSearchCond));
		cond.userId = s_UserId;
		cond.titleId = NULL;
		cond.dirName = dirName;
		cond.key = SCE_SAVE_DATA_SORT_KEY_MTIME;
		cond.order = SCE_SAVE_DATA_SORT_ORDER_DESCENT;

		SceSaveDataDirNameSearchResult result;
		memset(&result, 0x00, sizeof(result));

		SceSaveDataDirName dirNameOfResults;

		result.dirNames = &dirNameOfResults;
		result.dirNamesNum = 1;

		ret = sceSaveDataDirNameSearch(&cond, &result);
		if(ret < SCE_OK)
		{
			sgoWarning("OrbisProfile::DeleteMount - sceSaveDataDirNameSearch : 0x%08x\n", ret);
			return ret;
		}

		if (result.hitNum == 1)
		{
			sgoDebug1("OrbisProfile::DeleteMount - sceSaveDataDirNameSearch found a match for %s\n", dirName->data);
		}
		else
		{
			sgoWarning("OrbisProfile::DeleteMount - expected to find 1 directory with the name %s but found %u", dirName->data, result.hitNum);
		}

		SceSaveDataDelete del;
		memset(&del, 0x00, sizeof(SceSaveDataDelete));
		del.userId = s_UserId;
		del.titleId = NULL; // To mount save data created by the caller application, specify NULL for titleId.
		del.dirName = dirName;
		ret = sceSaveDataDelete(&del);
		if(ret < SCE_OK)
		{
			sgoWarning("OrbisProfile::DeleteMount - sceSaveDataDelete : 0x%08x\n", ret);
			return ret;
		}

		return SCE_OK;
	}

	void OrbisProfile::GetDirectoryNameFromFileNameAndType(eSavegameType savegameType, const char* fileName, SceSaveDataDirName &outDirName)
	{
		switch (savegameType)
		{
		case SG_TYPE_SAVEGAME :
			if (Verifyf(fileName != NULL, "OrbisProfile::GetDirectoryNameFromFileNameAndType - didn't expect fileName pointer to be NULL for a savegame"))
			{
				int prefixLength = strlen(SAVE_DATA_DIRNAME_PREFIX);
				if (!strncmp(fileName, SAVE_DATA_DIRNAME_PREFIX, prefixLength))
				{	//	If fileName already has the directory prefix then don't add it again
					formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s", fileName);
				}
				else
				{
					formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s%s", SAVE_DATA_DIRNAME_PREFIX, fileName);
				}
			}
			break;

			case SG_TYPE_BACKUP :
				if (fileName != NULL)
				{
					int prefixLength = strlen(SAVE_DATA_DIRNAME_PREFIX);
					if (!strncmp(fileName, SAVE_DATA_DIRNAME_PREFIX, prefixLength))
					{	//	If fileName already has the directory prefix then don't add it again
						formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s", fileName);
					}
					else
					{
						formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s%s", SAVE_DATA_DIRNAME_PREFIX, fileName);
					}
				}
				break;
		case SG_TYPE_PROFILE :
			safecpy(outDirName.data, m_ProfileDirName.data);
			break;

		case SG_TYPE_PHOTO :
			formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s", SAVE_DATA_DIRNAME_PHOTO);
			break;
		
		case SG_TYPE_CLIP:
			{
				if (Verifyf(fileName != NULL, "OrbisProfile::GetDirectoryNameFromFileNameAndType - didn't expect fileName pointer to be NULL for a replay clip"))
				{
					char clipName[SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE];
					safecpy(clipName, fileName, NELEM(clipName));

					char * period = NULL;
					period = strrchr( clipName, '.' );
					if( period )
					{
						*period = '\0';
					}

					atString path = atString(clipName);
					path.Replace("_", "");
					
					formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s%s", REPLAY_DIRNAME_PREFIX, path.c_str());
				}
			}
			break;
		case SG_TYPE_MONTAGE:
			formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s", REPLAY_DIRNAME_MONTAGE);
			break;

		case SG_TYPE_SPACE_CHECK:
			formatf(outDirName.data, SCE_SAVE_DATA_DIRNAME_DATA_MAXSIZE, "%s", SAVE_DATA_DIRNAME_SPACE_CHECK);
			break;
		}
	}

	void OrbisProfile::SetupSceSaveDataMount(const SceUserServiceUserId userId, const SceSaveDataMountMode mode, SceSaveDataMount *mount, u32 maxMountSize, eSavegameType savegameType, const char* fileName)
	{
		memset(mount, 0x00, sizeof(SceSaveDataMount));
		mount->userId = userId;
		mount->titleId = NULL; // To mount save data created by the caller application, specify NULL for titleId.

		GetDirectoryNameFromFileNameAndType(savegameType, fileName, m_SelectedDirName);
		mount->dirName = &m_SelectedDirName;	//	Graeme - SG_TYPE_PROFILE used to do mount->dirName = &m_ProfileDirName; Hopefully my change won't break anything

#if SCE_ORBIS_SDK_VERSION >= 0x00990020u
		mount->fingerprint = NULL;
		mount->blocks = (maxMountSize/SCE_SAVE_DATA_BLOCK_SIZE) + ((maxMountSize%SCE_SAVE_DATA_BLOCK_SIZE)?1:0);
		if (mount->blocks < SCE_SAVE_DATA_BLOCKS_MIN)
		{
			mount->blocks = SCE_SAVE_DATA_BLOCKS_MIN;
		}
#else
		mount->passCode = NULL;
		mount->totalSizeKiB = maxMountSize;
#endif
		mount->mountMode = mode;
	}

	OrbisProfile::eSavegameType OrbisProfile::GetSavegameTypeFromFilename(const char *pFilename, bool bCheckDirName)
	{
		if (bCheckDirName)
		{
			if (!strncmp(pFilename, SAVE_DATA_DIRNAME_PROFILE, strlen(SAVE_DATA_DIRNAME_PROFILE)))
			{
				return SG_TYPE_PROFILE;
			}
			else if (!strncmp(pFilename, SAVE_DATA_DIRNAME_PHOTO, strlen(SAVE_DATA_DIRNAME_PHOTO)))
			{
				return SG_TYPE_PHOTO;
			}
			else if (!strncmp(pFilename, REPLAY_DIRNAME_CLIP, strlen(REPLAY_DIRNAME_CLIP)))
		    {
		   		return SG_TYPE_CLIP;
		    }
			else if (!strncmp(pFilename, REPLAY_DIRNAME_MONTAGE, strlen(REPLAY_DIRNAME_MONTAGE)))
			{
				return SG_TYPE_MONTAGE;
			}
			else if (!strncmp(pFilename, SAVE_DATA_DIRNAME_SPACE_CHECK, strlen(SAVE_DATA_DIRNAME_SPACE_CHECK)))
			{
				return SG_TYPE_SPACE_CHECK;
			}
			else if (!strncmp(pFilename, SAVE_DATA_DIRNAME_BACKUP, strlen(SAVE_DATA_DIRNAME_BACKUP)))
			{
				return SG_TYPE_BACKUP;
			}
		}
		else
		{	//	Check the filename only
			if (!strncmp(pFilename, SAVE_DATA_FILENAME_PROFILE, strlen(SAVE_DATA_FILENAME_PROFILE)))
			{
				return SG_TYPE_PROFILE;
			}
			else if (!strncmp(pFilename, SAVE_DATA_FILENAME_PHOTO, strlen(SAVE_DATA_FILENAME_PHOTO)))
			{
				return SG_TYPE_PHOTO;
			}
			else if (!strncmp(pFilename, REPLAY_FILENAME_CLIP, strlen(REPLAY_FILENAME_CLIP)))
			{
				return SG_TYPE_CLIP;
			}
			else if (!strncmp(pFilename, REPLAY_FILENAME_MONTAGE, strlen(REPLAY_FILENAME_MONTAGE)))
			{
				return SG_TYPE_MONTAGE;
			}
			else if (!strncmp(pFilename, SAVE_DATA_FILENAME_SPACE_CHECK, strlen(SAVE_DATA_FILENAME_SPACE_CHECK)))
			{
				return SG_TYPE_SPACE_CHECK;
			}
			else if (!strncmp(pFilename, SAVE_DATA_FILENAME_BACKUP, strlen(SAVE_DATA_FILENAME_BACKUP)))
			{
				return SG_TYPE_BACKUP;
			}
		}

		return SG_TYPE_SAVEGAME;
	}

	u32 OrbisProfile::GetMaxMountSizeFromSavegameType(eSavegameType savegameType)
	{
		switch (savegameType)
		{
			case SG_TYPE_SAVEGAME :
				return SAVE_DATA_MAX_GAME_SIZE;

			case SG_TYPE_PROFILE :
				return SAVE_DATA_MAX_PROFILE_SIZE;

			case SG_TYPE_PHOTO :
				return (SAVE_DATA_MAX_PHOTO_SIZE * SAVE_DATA_MAX_NUMBER_OF_PHOTOS);

			case SG_TYPE_CLIP :
				return SAVE_DATA_MAX_CLIP_SIZE;

			case SG_TYPE_MONTAGE :
				return SAVE_DATA_MAX_MONTAGE_SIZE;

			case SG_TYPE_SPACE_CHECK: 
				return s_SizeOfCurrentFile; 
			case SG_TYPE_BACKUP :
				return SAVE_DATA_MAX_GAME_SIZE;
		}

		return SAVE_DATA_MAX_GAME_SIZE;
	}


	bool OrbisProfile::BeginSelectDevice(u32 /*contentType*/,u32 /*minSpaceAvailable*/,bool /*forceShow*/,bool /*manageStorage*/) 
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("Cannot begin device selection, operation already in progress");
			return false;
		}

		m_State = fiSaveGameState::SELECTING_DEVICE;

		return true;
	}

	bool OrbisProfile::CheckSelectDevice() 
	{
		if (m_State != fiSaveGameState::SELECTING_DEVICE)
		{
			sgoWarning("CheckSelectDevice called when not in the device selection state");
			return false;
		}

		m_State = fiSaveGameState::SELECTED_DEVICE;

		return true;
	}

	void OrbisProfile::EndSelectDevice() 
	{
		if (m_State == fiSaveGameState::SELECTED_DEVICE)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	int OrbisProfile::CompareModifiedTimes(const void* _a, const void* _b)
	{
		fiSaveGame::Content* a = (fiSaveGame::Content*) _a;
		fiSaveGame::Content* b = (fiSaveGame::Content*) _b;

		if (a->ModificationTimeHigh == b->ModificationTimeHigh)
		{
			return (a->ModificationTimeLow < b->ModificationTimeLow) ? 1 : -1;
		}
		return (a->ModificationTimeHigh < b->ModificationTimeHigh) ? 1 : -1;
	}

	int OrbisProfile::MountForEnumeration(const char *pFilename, SceSaveDataMountPoint &mountPoint, bool &bUnmountWhenFinished)
	{
		memset(&mountPoint, 0x00, sizeof(SceSaveDataMountPoint));
		bUnmountWhenFinished = true;

		eSavegameType savegameType = GetSavegameTypeFromFilename(pFilename, false);
		if (savegameType == SG_TYPE_PHOTO)
		{
			if (CheckIfPhotoDirectoryIsMountedForReading(mountPoint))
			{
				bUnmountWhenFinished = false;
				return SCE_OK;
			}
		}

		// mount save data for read only
		SceSaveDataMount mount;
		SetupSceSaveDataMount(s_UserId, SCE_SAVE_DATA_MOUNT_MODE_RDONLY, &mount, 0, savegameType, pFilename);	//	I'll try passing SG_TYPE_SAVEGAME instead of false here

		sgotimingDebugf1("%d - about to call sceSaveDataMount for file enumeration",sysTimer::GetSystemMsTime());

		SceSaveDataMountResult mountResult;
		memset(&mountResult, 0x00, sizeof(mountResult));
		int ret = sceSaveDataMount(&mount, &mountResult);

		sgotimingDebugf1("%d - sceSaveDataMount has finished",sysTimer::GetSystemMsTime());

		if (ret == SCE_OK)
		{
			memcpy(&mountPoint, &mountResult.mountPoint, sizeof(SceSaveDataMountPoint));
			sgoDebug1("OrbisProfile::MountForEnumeration - mount succeeded. mountPoint is %s\n", mountPoint.data);

			if (savegameType == SG_TYPE_PHOTO)
			{
				MountPhotoDirectoryForReading(mountPoint);
				bUnmountWhenFinished = false;
			}
		}

		return ret;
	}

	void CleanUpResults(SceSaveDataDirNameSearchResult &result)
	{
		if (result.dirNames)
		{
			delete [] result.dirNames;
			result.dirNames = NULL;
		}
		if (result.params)
		{
			delete [] result.params;
			result.params = NULL;
		}
	}

	bool OrbisProfile::MountPhotosForEnumeration(const char* fileName)
	{
		SceSaveDataMountPoint mountPoint;
		bool bUnmountWhenFinished = true;
		int errorCode = MountForEnumeration(fileName, mountPoint, bUnmountWhenFinished);

		if (errorCode < SCE_OK)
		{
			if (errorCode == SCE_SAVE_DATA_ERROR_NOT_FOUND ||
				errorCode == SCE_SAVE_DATA_ERROR_BROKEN)
			{
				return true;
			}
			else
			{
				sgoDebug1("Trace: BeginEnumeration save mount error: %d", errorCode);

				return false;
			}
		}

			sgoDisplayf("%s has been mounted", fileName);

			// prepare device using the savedata mount
			const fiDevice *device = fiDevice::GetDevice(mountPoint.data);
			fiFindData data;
			fiHandle h = device->FindFileBegin(mountPoint.data,data);
			if (fiIsValidHandle(h)) 
			{
				do
				{
					// enumerate files only, skip directories
					if (data.m_Name[0] != '.' && !(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
					{
						fiSaveGame::Content &c = s_OutContent[s_Count++];
						c.ModificationTimeHigh = 0;
						c.ModificationTimeLow = 0;
						safecpy(c.Filename, data.m_Name);

						if(GetDisplayNameAndModifiedTime(s_UserId, c, mountPoint.data, data.m_Name))
						{
						#if !__NO_OUTPUT
							USES_CONVERSION;
							sgoDebug1("Trace: BeginEnumeration() adding file to list: filename:%s, displayname:%s", c.Filename, W2A(c.DisplayName));
							sgoDisplayf("Adding %s to results list. ModTimeHigh=%u ModTimeLow=%u", c.Filename, c.ModificationTimeHigh, c.ModificationTimeLow);
						#endif
						}
						else
						{
							s_Count--;
						}
					}
				} while (s_Count < s_MaxCount && device->FindFileNext(h,data));
				device->FindFileEnd(h);
			}

			// Try to unmount the save data
			if (bUnmountWhenFinished)
			{
				int ret = sceSaveDataUmount(&mountPoint);
				if(ret < SCE_OK)
				{
					sgoError("sceSaveDataUmount : 0x%08x\n", ret);

					return false;
				}
			}
			return true;
	}

	bool OrbisProfile::MountClipsForEnumeration(const char* fileName)
	{
		SceSaveDataMountPoint mountPoint;
		bool bUnmountWhenFinished = true;
		int errorCode = MountForEnumeration(fileName, mountPoint, bUnmountWhenFinished);

		if (errorCode < SCE_OK)
		{
			if (errorCode == SCE_SAVE_DATA_ERROR_NOT_FOUND ||
				errorCode == SCE_SAVE_DATA_ERROR_BROKEN)
			{
				return true;
			}
			else
			{
				sgoDebug1("Trace: BeginEnumeration save mount error: %d", errorCode);

				return false;
			}
		}

		const fiDevice* device = fiDevice::GetDevice( mountPoint.data );

		// Enumerate directory for files
		fiFindData findData;
		fiHandle hFileHandle = fiHandleInvalid;
		bool validDataFound = true;

		for( hFileHandle = device->FindFileBegin( mountPoint.data, findData );
			fiIsValidHandle( hFileHandle ) && validDataFound; 
			validDataFound = device->FindFileNext( hFileHandle, findData ) )
		{
			bool isClipEnumeration = false;
			bool isMontageEnumeration = false;

			if( strstr(findData.m_Name, ".clip") )
			{
				isClipEnumeration = true;
			}

			if( strstr(findData.m_Name, ".vid") )
			{
				isMontageEnumeration = true;
			}

			if (findData.m_Name[0] != '.' && !(findData.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
			{
				bool add = true;

				if( isClipEnumeration )
				{
					char path[64];
					sprintf_s(path, 64, "%s/%s", mountPoint.data, findData.m_Name);

					fiStream *S = fiStream::Open(path);
					
					if( S != NULL && s_ValidateFunc != NULL )
					{
						if( !s_ValidateFunc((fiHandle)S, findData.m_Name) )
						{
							add = false;
						}
					}

					S->Close();
				}

				if( add )
				{
					fiSaveGame::Content &c = s_OutContent[s_Count++];
					c.ModificationTimeHigh = 0;
					c.ModificationTimeLow = 0;
					safecpy(c.Filename, findData.m_Name);
				}
			}
		}

		if( fiIsValidHandle( hFileHandle ) )
		{
			device->FindFileEnd( hFileHandle );
		}

		int ret = sceSaveDataUmount(&mountPoint);
		if(ret < SCE_OK)
		{
			sgoError("sceSaveDataUmount : 0x%08x\n", ret);

			return false;
		}

		return true;
	}


#if USE_DOWNLOAD0_FOR_BACKUP
	void OrbisProfile::GetNameOfDownload0Path(char *path, s32 stringLength, const char *pFilename, eDownload0Folder folderType)
	{
		switch (folderType)
		{
		case DOWNLOAD0_USER_FOLDER :
			if (pFilename == NULL)
			{
				formatf(path, stringLength, "/download0/saves/%d", s_UserId);
			}
			else
			{
				formatf(path, stringLength, "/download0/saves/%d/%s", s_UserId, pFilename);		//	Should the file have an extension? It might complicate other bits of code that are strcmp'ing against SBGTA50015
			}
			break;

		case DOWNLOAD0_SAVEGAME_FOLDER :
			if (pFilename == NULL)
			{
				formatf(path, stringLength, "/download0/saves");
			}
			else
			{
				formatf(path, stringLength, "/download0/saves/%s", pFilename);		//	Should the file have an extension? It might complicate other bits of code that are strcmp'ing against SBGTA50015
			}
			break;
		}
	}

	bool OrbisProfile::ShouldUseDownload0(const char *pFilename)
	{
#if USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP
		if(!strcmp(DOWNLOAD0_AUTOSAVE_BACKUP_NAME, pFilename))
		{
			return true;
		}
#endif

#if USE_DOWNLOAD0_FOR_PROFILE_BACKUP
		if(!strcmp(DOWNLOAD0_PROFILE_BACKUP_NAME, pFilename))
		{
			return true;
		}
#endif

		return false;
	}

#endif	//	USE_DOWNLOAD0_FOR_BACKUP


	void OrbisProfile::s_BeginEnumeration(void*)
	{
		const char *pDirectoryPrefix = SAVE_DATA_DIRNAME_PREFIX;
		const char *pDirectoryForEnumeration = SAVE_DATA_SAVEGAME_PREFIX_FOR_ENUMERATION;

		if ( (s_iEnumerateType == (int)SG_TYPE_CLIP) || (s_iEnumerateType == (int)SG_TYPE_MONTAGE) )
		{
			pDirectoryPrefix = REPLAY_DIRNAME_PREFIX;
			pDirectoryForEnumeration = REPLAY_DIRNAME_PREFIX_FOR_ENUMERATION;
		}
		const int FILE_NAME_SIZE = 32;
		char fileName[FILE_NAME_SIZE];
		SceSaveDataDirName searchDirName;
		safecpy(searchDirName.data, pDirectoryForEnumeration);

		// Obtain list of directories to mount
		SceSaveDataDirNameSearchCond cond;
		memset(&cond, 0x00, sizeof(SceSaveDataDirNameSearchCond));
		cond.userId = s_UserId;
		cond.titleId = NULL;
		cond.dirName = &searchDirName;
		cond.key = SCE_SAVE_DATA_SORT_KEY_MTIME;
		cond.order = SCE_SAVE_DATA_SORT_ORDER_DESCENT;

		SceSaveDataDirNameSearchResult result;
		memset(&result, 0x00, sizeof(result));

		result.dirNames = rage_new SceSaveDataDirName[s_MaxCount];
		if ( result.dirNames == NULL ) {
			// Error handling
		}
		result.dirNamesNum = s_MaxCount;

		result.params = rage_new SceSaveDataParam[s_MaxCount];

		if ( sceSaveDataDirNameSearch(&cond, &result) < 0 ) {
			// Error handling
			result.hitNum = 0;
			CleanUpResults(result);
		}

		int prefixLength = strlen(pDirectoryPrefix);
		int photoPrefixLength = strlen(SAVE_DATA_DIRNAME_PHOTO);
		int clipPrefixLength = strlen(REPLAY_DIRNAME_CLIP);
		int montagePrefixLength = strlen(REPLAY_DIRNAME_MONTAGE);
		int mainSavegamePrefixLength = strlen(SAVE_DATA_DIRNAME_MAIN_SAVEGAME);
		int backupPrefixLength = strlen(SAVE_DATA_DIRNAME_BACKUP);

		//check the SaveDataMemory before we iterate over all the files 		
#if USE_SAVE_DATA_MEMORY
		if ( s_iEnumerateType == (int)SG_TYPE_SAVEGAME )
		{
			Header header;
			ReadSaveDataMemoryHeader(&header);
			if(header.m_mtime != 0)
			{
				fiSaveGame::Content &c = s_OutContent[s_Count++];
				c.ModificationTimeHigh = header.m_mtime >> 32;
				c.ModificationTimeLow = header.m_mtime;
				safecpy(c.Filename, SAVEDATAMEMORY_NAME);

				safecpy( c.DisplayName, header.GetDisplayName() );
#if !__NO_OUTPUT
				USES_CONVERSION;
				sgoDebug1("Trace: BeginEnumeration() adding file to list: filename:%s, displayname:%s", c.Filename, W2A(c.DisplayName));
				sgoDisplayf("Adding %s to results list. ModTimeHigh=%u ModTimeLow=%u", c.Filename, c.ModificationTimeHigh, c.ModificationTimeLow);
#endif			
			}
			
		}
#endif

#if USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP
		if ( s_iEnumerateType == (int)SG_TYPE_SAVEGAME )
		{
			// create path - i.e. /download0/saves/<userId>/filename
			char download0Path[SAVE_DATA_FILEPATH_LENGTH + 1];
			GetNameOfDownload0Path(download0Path, sizeof(download0Path), DOWNLOAD0_AUTOSAVE_BACKUP_NAME, DOWNLOAD0_USER_FOLDER);

			//	The documentation says that sceKernelCheckReachability is slow. Maybe I can use SceKernelStat instead.
			if (sceKernelCheckReachability(download0Path) == SCE_OK)
			{
				Header autosaveBackupHeader;
				if (ReadDownload0BackupHeader(download0Path, &autosaveBackupHeader))
				{
					if(autosaveBackupHeader.m_mtime != 0)
					{
						fiSaveGame::Content &c = s_OutContent[s_Count++];
						c.ModificationTimeHigh = autosaveBackupHeader.m_mtime >> 32;
						c.ModificationTimeLow = autosaveBackupHeader.m_mtime;
						safecpy(c.Filename, DOWNLOAD0_AUTOSAVE_BACKUP_NAME);

						safecpy( c.DisplayName, autosaveBackupHeader.GetDisplayName() );
#if !__NO_OUTPUT
						USES_CONVERSION;
						sgoDebug1("Trace: BeginEnumeration() adding file to list: filename:%s, displayname:%s", c.Filename, W2A(c.DisplayName));
						sgoDisplayf("Adding %s to results list. ModTimeHigh=%u ModTimeLow=%u", c.Filename, c.ModificationTimeHigh, c.ModificationTimeLow);
#endif			
					}
				}
			}
		}
#endif	//	USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP


		// Enumerate the valid the directories
		for (u32 index = 0; index < result.hitNum; ++index)
		{
			bool bThisIsThePhotoDirectory = false;
			bool bThisIsAClipDirectory = false;
			bool bThisIsTheMontageDirectory = false;
			bool bThisIsASavegameDirectory = false;
			bool bThisIsABackupSavegameDirectory = false;

			if (!strncmp(result.dirNames[index].data, pDirectoryPrefix, prefixLength))
			{
				bThisIsAClipDirectory = !strncmp(result.dirNames[index].data, REPLAY_DIRNAME_CLIP, clipPrefixLength);
				bThisIsThePhotoDirectory = !strncmp(result.dirNames[index].data, SAVE_DATA_DIRNAME_PHOTO, photoPrefixLength);
				bThisIsTheMontageDirectory = !strncmp(result.dirNames[index].data, REPLAY_DIRNAME_MONTAGE, montagePrefixLength);
				bThisIsASavegameDirectory = !strncmp(result.dirNames[index].data, SAVE_DATA_DIRNAME_MAIN_SAVEGAME, mainSavegamePrefixLength);
				bThisIsABackupSavegameDirectory = !strncmp(result.dirNames[index].data, SAVE_DATA_DIRNAME_BACKUP, backupPrefixLength);
				
				if ((bThisIsThePhotoDirectory && s_iEnumerateType == (int)SG_TYPE_PHOTO) || 
					(bThisIsAClipDirectory && s_iEnumerateType == (int)SG_TYPE_CLIP) || 
					(bThisIsTheMontageDirectory && s_iEnumerateType == (int)SG_TYPE_MONTAGE) || 
					( (bThisIsASavegameDirectory || bThisIsABackupSavegameDirectory) && (s_iEnumerateType == (int)SG_TYPE_SAVEGAME) )
					)
				{
					sgoDisplayf("savegame %u is %s", index, result.dirNames[index].data);
					safecpy(fileName, result.dirNames[index].data, FILE_NAME_SIZE);
					memmove(fileName, fileName + prefixLength, FILE_NAME_SIZE - prefixLength);
				}
				else
				{
					sgoDisplayf("savegame %u is %s s_iEnumerateType= %d so skip this directory", index, result.dirNames[index].data, s_iEnumerateType);
					continue;
				}
			}
			else
			{
				AssertMsg(false, "OrbisProfile::s_BeginEnumeration - Directory name does not begin with valid prefix.");
				continue;
			}

			if(bThisIsThePhotoDirectory)
			{
				if(!MountPhotosForEnumeration(fileName))
				{
					CleanUpResults(result);
					s_FileOperationError = true;
					s_FileOperationDone = true;
					return;
				}
			}//bThisIsThePhotoDirectory
			else if(bThisIsAClipDirectory || bThisIsTheMontageDirectory)
			{
				if(!MountClipsForEnumeration(fileName))
				{
					CleanUpResults(result);
					s_FileOperationError = true;
					s_FileOperationDone = true;
					return;
				}
			}//bThisIsAClipDirectory || bThisIsTheMontageDirectory
			else
			{
#if USE_SAVE_DATA_MEMORY
				//we do not iterate over old auto savegame data stuff
				if(!strncmp(result.dirNames[index].data, SAVEDATAMEMORY_FULL_NAME, strlen(SAVEDATAMEMORY_FULL_NAME)))
				{
					continue;
				}
#endif

#if USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP
				if(!strncmp(result.dirNames[index].data, DOWNLOAD0_AUTOSAVE_BACKUP_FULL_NAME, strlen(DOWNLOAD0_AUTOSAVE_BACKUP_FULL_NAME)))
				{
					continue;
				}
#endif	//	USE_DOWNLOAD0_FOR_AUTOSAVE_BACKUP

				//don't mount savegames
				if(s_Count < s_MaxCount)
				{
					fiSaveGame::Content &c = s_OutContent[s_Count++];
					c.ModificationTimeHigh = result.params[index].mtime >> 32;
					c.ModificationTimeLow = result.params[index].mtime;
					safecpy(c.Filename, fileName);

					USES_CONVERSION;
					safecpy(c.DisplayName,UTF8_TO_WIDE(result.params[index].subTitle));
#if !__NO_OUTPUT
					sgoDebug1("Trace: BeginEnumeration() adding file to list: filename:%s, displayname:%s", c.Filename, W2A(c.DisplayName));
					sgoDisplayf("Adding %s to results list. ModTimeHigh=%u ModTimeLow=%u", c.Filename, c.ModificationTimeHigh, c.ModificationTimeLow);
#endif
				}

			}
		}

		qsort(s_OutContent, s_Count, sizeof(fiSaveGame::Content), CompareModifiedTimes);

		CleanUpResults(result);

		s_FileOperationDone = true;
	}

	bool OrbisProfile::BeginEnumeration(u32 contentType,fiSaveGame::Content *outContent,int maxCount,int enumerateType, void* validateFunc)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		if(maxCount <= 0)
		{
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("s_BeginEnumeration - didn't finish properly, can't start.");
			return false;
		}

		s_ValidateFunc = (tValidateFunc)validateFunc;
		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_Count = 0;
		s_MaxCount = maxCount;
		s_OutContent = outContent;
		s_UserId = m_UserId;
		s_iEnumerateType = enumerateType;

		sgotimingDebugf1("%d - BeginEnumeration started",sysTimer::GetSystemMsTime());

		s_ThreadId = sysIpcCreateThread(OrbisProfile::s_BeginEnumeration,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] EnumerationThread");

		m_State = ENUMERATING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckEnumeration(int &outCount)
	{
		if (m_State != fiSaveGameState::ENUMERATING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				outCount = 0;
			}
			else
			{
				m_State = fiSaveGameState::ENUMERATED_CONTENT;
				outCount = s_Count;
			}

			return true;
		}

		return false;
	}

	void OrbisProfile::EndEnumeration()
	{
		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndEnumeration time",sysTimer::GetSystemMsTime());
		}

		if (m_State == ENUMERATED_CONTENT)
			m_State = IDLE;
	}

	void OrbisProfile::s_BeginFileSave(void*)
	{
		int ret = SCE_OK;

		SceSaveDataMountResult mountResult;
		bool bHasBeenCreated = false;

		if (!MountForSaving(s_Filename, mountResult, bHasBeenCreated))
		{
			sgoError("OrbisProfile::s_BeginFileSave - MountForSaving failed");
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}


//	Sony's sample file does things in this order
//	sceSaveDataSetParam, sceSaveDataSaveIcon (when creating a new mount directory), write the data, read the data (for verification?)
//	Does the order matter?

		sgotimingDebugf1("%d - about to call SaveFile",sysTimer::GetSystemMsTime());

		// Save filename to the mount point using displayName
		ret = OrbisProfile::SaveFile(mountResult.mountPoint.data, s_Filename, s_DisplayName, s_SaveData, s_SizeOfCurrentFile);
		if(ret < SCE_OK)
		{
			sgotimingDebugf1("%d - SaveFile failed",sysTimer::GetSystemMsTime());

			sgoError("SaveFile : 0x%08x(%s/%s)\n", ret, mountResult.mountPoint.data, s_Filename);
			s_FileOperationError = true;
		}
		else
		{
			sgotimingDebugf1("%d - SaveFile completed. About to call SetParam",sysTimer::GetSystemMsTime());

			// Save the save data text information
			eSavegameType savegameType = GetSavegameTypeFromFilename(s_Filename, false);
			ret = OrbisProfile::SetParam(&(mountResult.mountPoint), savegameType == SG_TYPE_BACKUP);
			if(ret < SCE_OK)
			{
				sgotimingDebugf1("%d - SetParam failed",sysTimer::GetSystemMsTime());

				sgoError("SetParam : 0x%08x\n", ret);
				s_FileOperationError = true;
			}
			else
			{
				sgotimingDebugf1("%d - SetParam completed",sysTimer::GetSystemMsTime());

				if (bHasBeenCreated)
				{
					sgotimingDebugf1("%d - about to call sceSaveDataSaveIcon",sysTimer::GetSystemMsTime());

					SceSaveDataIcon icon;
					memset(&icon, 0x00, sizeof(icon));
					icon.buf = (void*)pIconImage;
					icon.bufSize = SizeOfIconImage;
					icon.dataSize = SizeOfIconImage;
					ret = sceSaveDataSaveIcon(&mountResult.mountPoint, &icon);
					if(ret < SCE_OK)
					{
						sgotimingDebugf1("%d - sceSaveDataSaveIcon failed",sysTimer::GetSystemMsTime());

						sgoError("sceSaveDataSaveIcon : 0x%08x\n", ret);
						s_FileOperationError = true;
					}
					else
					{
						sgotimingDebugf1("%d - sceSaveDataSaveIcon completed",sysTimer::GetSystemMsTime());
					}
				}
			}
		}	

		sgotimingDebugf1("%d - about to call sceSaveDataUmount",sysTimer::GetSystemMsTime());

		// Try to unmount the save data
		ret = sceSaveDataUmount(&mountResult.mountPoint);
		if(ret < SCE_OK)
		{
			sgotimingDebugf1("%d - sceSaveDataUmount failed",sysTimer::GetSystemMsTime());

			sgoError("sceSaveDataUmount : 0x%08x\n", ret);
			s_FileOperationError = true;
		}
		else
		{
			sgotimingDebugf1("%d - sceSaveDataUmount completed",sysTimer::GetSystemMsTime());
		}

		s_FileOperationDone = true;
	}
#if USE_SAVE_DATA_MEMORY
	void OrbisProfile::s_BeginFileSaveDataMemory(void *)
	{
		SceRtcDateTime datetime;
		time_t time;
		sceRtcGetCurrentClockUtc(&datetime);
		sceRtcConvertDateTimeToTime_t(&datetime, &time);

		u32 uBufferSize = Header::MAX_BYTE_SIZEOF_PAYLOAD + s_SizeOfCurrentFile;
		// Create buffer to contain header + data.  data is point offset by header size.
		u8* buf = rage_new u8[uBufferSize];
		u8* data = buf + Header::MAX_BYTE_SIZEOF_PAYLOAD; 
		memset(buf, '\0', uBufferSize);

		char16 title[SAVE_DATA_MAX_DISPLAY_NAME] = {0};
		safecpy(title, s_DisplayName, NELEM(title));		
		
		// add header and copy game data to offset pointer
		Header header;
		header.m_mtime = time;
		header.m_size = s_SizeOfCurrentFile;
		if(header.Export(title, buf, uBufferSize, NULL) == false)
		{
			sgoError("Exporting the header failed!");
			delete[] buf;
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}
		sysMemCpy(data, s_SaveData,  s_SizeOfCurrentFile);		
		if(!WriteSaveDataMemory(buf, uBufferSize))
			s_FileOperationError = true;
		delete[] buf;
		s_FileOperationDone = true;	
	}
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
	bool OrbisProfile::CreateFolderInDownload0(const char *pFolderName)
	{
		if (sceKernelCheckReachability(pFolderName) == SCE_OK)
		{
			return true;
		}
		else
		{
			int ret = sceKernelMkdir(pFolderName, SCE_KERNEL_S_IRWU);
			if (ret != SCE_OK)
			{
				sgoError("OrbisProfile::CreateFolderInDownload0 - sceKernelMkdir failed for %s, returned 0x%x", pFolderName, ret);
				return false;
			}
			else
			{
				ret = sceKernelCheckReachability(pFolderName);
				if (ret == SCE_OK)
				{
					return true;
				}
				else
				{
					sgoError("OrbisProfile::CreateFolderInDownload0 - path %s has just been created but sceKernelCheckReachability returned 0x%x", pFolderName, ret);
					return false;
				}
			}
		}

		return false;
	}

	void OrbisProfile::s_FileSaveDownload0Backup(void *)
	{
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];

		if (!ms_bDownload0FolderExistsForUser)
		{
			GetNameOfDownload0Path(path, sizeof(path), NULL, DOWNLOAD0_SAVEGAME_FOLDER);

			if (!OrbisProfile::CreateFolderInDownload0(path))
			{
				s_FileOperationError = true;
				s_FileOperationDone = true;
				return;
			}

			GetNameOfDownload0Path(path, sizeof(path), NULL, DOWNLOAD0_USER_FOLDER);

			if (!OrbisProfile::CreateFolderInDownload0(path))
			{
				s_FileOperationError = true;
				s_FileOperationDone = true;
				return;
			}

			ms_bDownload0FolderExistsForUser = true;
		}


		SceRtcDateTime datetime;
		time_t time;
		sceRtcGetCurrentClockUtc(&datetime);
		sceRtcConvertDateTimeToTime_t(&datetime, &time);

		u32 uBufferSize = Header::MAX_BYTE_SIZEOF_PAYLOAD + s_SizeOfCurrentFile;
		// Create buffer to contain header + data.  data is point offset by header size.
		u8* buf = nullptr;
		{
#if ENABLE_CHUNKY_ALLOCATOR
			ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), true, "s_FileSaveDownload0Backup");
#endif // ENABLE_CHUNKY_ALLOCATOR
			buf = rage_new u8[uBufferSize];
		}
		u8* data = buf + Header::MAX_BYTE_SIZEOF_PAYLOAD; 
		memset(buf, '\0', uBufferSize);

		char16 title[SAVE_DATA_MAX_DISPLAY_NAME] = {0};
		safecpy(title, s_DisplayName, NELEM(title));
		safecat(title, " (", NELEM(title));

		USES_CONVERSION;
		safecat(title, UTF8_TO_WIDE(sLocalisedBackupString), NELEM(title));
		safecat(title, ")", NELEM(title));

		// add header and copy game data to offset pointer
		Header header;
		header.m_mtime = time;
		header.m_size = s_SizeOfCurrentFile;
		if(header.Export(title, buf, uBufferSize, NULL) == false)
		{
			sgoError("Exporting the header failed!");
			{
#if ENABLE_CHUNKY_ALLOCATOR
				ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), false, "s_FileSaveDownload0Backup");
#endif // ENABLE_CHUNKY_ALLOCATOR
				delete[] buf;
			}
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}
		sysMemCpy(data, s_SaveData,  s_SizeOfCurrentFile);


		GetNameOfDownload0Path(path, sizeof(path), s_Filename, DOWNLOAD0_USER_FOLDER);

		bool forceSync = false;
#if USE_DOWNLOAD0_FOR_PROFILE_BACKUP
		if(!strcmp(DOWNLOAD0_PROFILE_BACKUP_NAME, s_Filename))
		{
			forceSync = true;
		}
#endif

		if(!WriteDownload0Backup(path, buf, uBufferSize, forceSync))
		{
			s_FileOperationError = true;
		}

		{
#if ENABLE_CHUNKY_ALLOCATOR
			ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), false, "s_FileSaveDownload0Backup");
#endif // ENABLE_CHUNKY_ALLOCATOR
			delete[] buf;
		}
		s_FileOperationDone = true;	
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

	bool OrbisProfile::BeginSave(u32 OUTPUT_ONLY(signInId),const char16 * displayName,const char *filename,const void *saveData,u32 saveSize, bool overwrite)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("Cannot begin saving, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("BeginSave - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginSave - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_Filename = filename;
		s_DisplayName = displayName;
		s_SaveData = saveData;
		s_SizeOfCurrentFile = saveSize;
		s_Overwrite = overwrite;

		sgotimingDebugf1("%d - BeginSave started",sysTimer::GetSystemMsTime());

#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, filename))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_BeginFileSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveThread");	}
		else		
#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(filename))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileSaveDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_BeginFileSave,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveThread");
			}
		}

		m_State = SAVING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckSave(bool &outIsValid, bool &fileExists)
	{
		if (m_State != fiSaveGameState::SAVING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				outIsValid = false;
				fileExists = true;		// TODO: ?
			}
			else
			{
				m_State = fiSaveGameState::SAVED_CONTENT;
				outIsValid = true;
				fileExists = true;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndSave()
	{
		sgoDebug1("Trace: EndSave");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndSave time",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::SAVED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	int OrbisProfile::SetParam(const SceSaveDataMountPoint *mountPoint, bool isBackup)
	{
		int ret = SCE_OK;

		SceSaveDataParam param;
		memset(&param, 0x00, sizeof(param));

		if(!isBackup)
			formatf(param.title, "%s", s_TitleString);
		else
			formatf(param.title, "%s (%s)", s_TitleString, sLocalisedBackupString);
		param.title[SCE_SAVE_DATA_TITLE_MAXSIZE-1] = '\0';

		USES_CONVERSION;
		if(!isBackup)
			formatf(param.subTitle, "%s", WIDE_TO_UTF8(s_DisplayName));
		else
			formatf(param.subTitle, "%s (%s)", WIDE_TO_UTF8(s_DisplayName), sLocalisedBackupString);
		param.subTitle[SCE_SAVE_DATA_SUBTITLE_MAXSIZE-1] = '\0';

		strncpy(param.detail, "", SCE_SAVE_DATA_DETAIL_MAXSIZE-1);
		param.detail[SCE_SAVE_DATA_DETAIL_MAXSIZE-1] = '\0';

		param.userParam = 0;

		ret = sceSaveDataSetParam(mountPoint, SCE_SAVE_DATA_PARAM_TYPE_ALL, &param, sizeof(param));
		if(ret < SCE_OK)
		{
			sgoError("sceSaveDataSetParam : 0x%08x\n", ret);
			return ret;
		}

		return SCE_OK;
	}

	int OrbisProfile::GetParam(const SceSaveDataMountPoint *mountPoint)
	{
		int ret = SCE_OK;

		SceSaveDataParam param;
		memset(&param, 0x00, sizeof(param));

		size_t gotSize = 0;
		ret = sceSaveDataGetParam(mountPoint, SCE_SAVE_DATA_PARAM_TYPE_ALL, &param, sizeof(param), &gotSize);
		if(ret < SCE_OK)
		{
			sgoError("sceSaveDataGetParam : 0x%08x\n", ret);
			return ret;
		}

		return SCE_OK;
	}


	int OrbisProfile::SaveFile(const char *mountPoint, const char *fileName, const char16* displayName, const void *saveData, const size_t dataSize)
	{
		int ret = SCE_OK;

		// create path - i.e. /savedata01/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		snprintf(path, sizeof(path), "%s/%s", mountPoint, fileName);

		SceKernelMode mode = SCE_KERNEL_O_WRONLY;
		mode |= s_Overwrite ? SCE_KERNEL_O_TRUNC|SCE_KERNEL_O_CREAT : SCE_KERNEL_O_APPEND;

		// open file for edit or create
		int fd = sceKernelOpen(path, mode, SCE_KERNEL_S_IRWU);
		if(fd < SCE_OK)
		{
			sgoError("sceKernelOpen : 0x%08x(%s)\n", fd, path);
			return fd;
		}

		// convert display name into header title
		char16 title[SAVE_DATA_MAX_DISPLAY_NAME] = {0};
		safecpy(title, displayName, NELEM(title));

		eSavegameType savegameType = GetSavegameTypeFromFilename(fileName, false);

		bool isClip = savegameType == SG_TYPE_CLIP;

		int nBytes = 0;
		u32 uBufferSize = isClip ? dataSize : Header::MAX_BYTE_SIZEOF_PAYLOAD + dataSize;

		// Create buffer to contain header + data.  data is point offset by header size.
		u8* buf = rage_new u8[uBufferSize];
		u8* data = isClip ? buf : buf + Header::MAX_BYTE_SIZEOF_PAYLOAD; 

		memset(buf, '\0', uBufferSize);

		if (!isClip
#if __BANK
		&& !PARAM_usePS3saves.Get()
#endif
		)
		{
			// add header and copy game data to offset pointer
			Header header;
			
			if(header.Export(title, buf, uBufferSize, NULL) == false)
			{
				sgoError("Exporting the header failed!");
				delete[] buf;
				return false;
			}
		}		

		sysMemCpy(data, saveData,  dataSize);

		// single write to minimize corruption possibilities.
		nBytes = static_cast<int>(sceKernelWrite(fd, buf, uBufferSize));
		delete[] buf;

		if(nBytes < SCE_OK)
		{
			sgoError("sceKernelWrite : 0x%08x(%s)\n", ret, path);
			sceKernelClose(fd);

			return ret;
		}

		// close file
		sceKernelClose(fd);
		return SCE_OK;
	}

	int OrbisProfile::LoadFile(const char *mountPoint, const char *fileName, void *loadData,u32 loadSize)
	{
		int ret = SCE_OK;

		// create path - i.e. /savedata01/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		snprintf(path, sizeof(path), "%s/%s", mountPoint, fileName);

		// open file for reading
		fiStream *S = fiStream::Open(path);
		if (!S)
		{
			return false;
		}
		else 
		{
#if __BANK
			if (PARAM_usePS3saves.Get())
			{
				// Read directly without reading data
				ret = S->Read(loadData,loadSize);
				S->Close();
				return ret;
			}
#endif

			// Read the header first to avoid it getting into the loadData
			Header header;
			u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD] = {0};		
			int bytesRead = S->Read(buf, sizeof(buf));

			if((bytesRead != sizeof(buf)) ||
				(header.Import(buf, sizeof(buf)) == false))
			{
				S->Close();
				return false;
			}

			// Read only the save data into
			ret = S->Read(loadData,loadSize);
			S->Close();
			return ret;
		}
	}

	bool OrbisProfile::MountForReading(const char *pFilename, SceSaveDataMountPoint &mountPoint, bool &bUnmountWhenFinished)
	{
		memset(&mountPoint, 0x00, sizeof(SceSaveDataMountPoint));
		bUnmountWhenFinished = true;

		// mount save data for read only
		SceSaveDataMount mount;
		eSavegameType savegameType = GetSavegameTypeFromFilename(pFilename, false);

		if (savegameType == SG_TYPE_PHOTO)
		{
			if (CheckIfPhotoDirectoryIsMountedForReading(mountPoint))
			{
				bUnmountWhenFinished = false;
				return true;
			}
		}

		SetupSceSaveDataMount(s_UserId, SCE_SAVE_DATA_MOUNT_MODE_RDONLY, &mount, 0, savegameType, pFilename);

		SceSaveDataMountResult mountResult;
		memset(&mountResult, 0x00, sizeof(mountResult));
		int ret = sceSaveDataMount(&mount, &mountResult);

		if (ret == SCE_OK)
		{
			sgoDebug1("OrbisProfile::MountForReading - sceSaveDataMount succeeded\n");
			memcpy(&mountPoint, &mountResult.mountPoint, sizeof(SceSaveDataMountPoint));
			sgoDebug1("OrbisProfile::MountForReading - mountPoint is %s\n", mountPoint.data);

			if (savegameType == SG_TYPE_PHOTO)
			{
				MountPhotoDirectoryForReading(mountPoint);
				bUnmountWhenFinished = false;
			}
			return true;
		}

		switch (ret)
		{
		case SCE_SAVE_DATA_ERROR_NO_SPACE_FS :
			sgoAssertf(0, "OrbisProfile::MountForReading - didn't expect sceSaveDataMount to ever return SCE_SAVE_DATA_ERROR_NO_SPACE_FS when mounted for SCE_SAVE_DATA_MOUNT_MODE_RDONLY");
			ShowNoSpaceDialog(mount, mountResult);
			return false;
//			break;

		case SCE_SAVE_DATA_ERROR_NOT_FOUND :
		case SCE_SAVE_DATA_ERROR_BROKEN :
//		SCE_SAVE_DATA_ERROR_NO_SPACE	//	Apparently this can no longer happen
		default:
			sgoError("OrbisProfile::MountForReading - sceSaveDataMount returned 0x%08x\n", ret);
			return false;
//			break;
		}

		return false;
	}


	void OrbisProfile::s_FileLoad(void*)
	{
		int ret = SCE_OK;
		SceSaveDataMountPoint mountPoint;
		bool bUnmountWhenFinished = true;
		
		if (!MountForReading(s_Filename, mountPoint, bUnmountWhenFinished))
		{
			sgoError("OrbisProfile::s_FileLoad - MountForReading failed");
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		ret = LoadFile(mountPoint.data, s_Filename, s_pBufferToLoadInto, s_SizeOfBufferToLoadInto);
		if(ret < SCE_OK)
		{
			s_FileOperationError = true;
			sgoError("s_FileLoad - Couldn't read file, 0x%08x(%s/%s)\n", ret, mountPoint.data, s_Filename);
		}
		else
		{
			// ret has successfully returned the file size
			s_SizeOfLoadedData = ret;
		}

		if (bUnmountWhenFinished)
		{
			ret = sceSaveDataUmount(&mountPoint);
			if(ret < SCE_OK)
			{
				sgoError("sceSaveDataUmount : 0x%08x\n", ret);
				s_FileOperationError = true;
			}
		}

		s_FileOperationDone = true;
	}
#if USE_SAVE_DATA_MEMORY
	void OrbisProfile::s_FileLoadSaveDataMemory(void*)
	{
		Header header;
		u8* buf = rage_new u8[SAVEDATAMEMORY_SIZE];

		if(!ReadSaveDataMemory(buf, SAVEDATAMEMORY_SIZE))
		{
			delete[] buf;
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		if(header.Import(buf, sizeof(u8)*Header::MAX_BYTE_SIZEOF_PAYLOAD) == false)
		{
			delete[] buf;	
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		u8* data = buf + Header::MAX_BYTE_SIZEOF_PAYLOAD;
		sysMemCpy(s_pBufferToLoadInto, data, s_SizeOfBufferToLoadInto);		
		delete[] buf;
		s_FileOperationDone = true;
	}
#endif


#if USE_DOWNLOAD0_FOR_BACKUP
	void OrbisProfile::s_FileLoadDownload0Backup(void*)
	{
		Header header;

		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		GetNameOfDownload0Path(path, sizeof(path), s_Filename, DOWNLOAD0_USER_FOLDER);

		if (!ReadDownload0BackupHeader(path, &header))
		{
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		if (header.m_size != s_SizeOfBufferToLoadInto)
		{	//	s_SizeOfBufferToLoadInto needs to be >= header.m_size
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		const u32 totalBufferSize = header.m_size + Header::MAX_BYTE_SIZEOF_PAYLOAD;
		u8* buf = rage_new u8[totalBufferSize];

		if(!ReadDownload0Backup(path, buf, totalBufferSize))
		{
			delete[] buf;
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		u8* data = buf + Header::MAX_BYTE_SIZEOF_PAYLOAD;
		sysMemCpy(s_pBufferToLoadInto, data, s_SizeOfBufferToLoadInto);		
		delete[] buf;

		s_SizeOfLoadedData = header.m_size;

		s_FileOperationDone = true;
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

	bool OrbisProfile::BeginLoad(u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize)
	{
		s_SizeOfLoadedData = 0;

		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("Cannot begin loading, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("BeginLoad - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginLoad - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_Filename = filename;
		s_pBufferToLoadInto = loadData;
		s_SizeOfBufferToLoadInto = loadSize;

		sgotimingDebugf1("%d - BeginLoad started",sysTimer::GetSystemMsTime());
#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, filename))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileLoadSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] LoadThread"); }
		else
#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(filename))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileLoadDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] LoadThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileLoad,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] LoadThread");
			}
		}

		m_State = fiSaveGameState::LOADING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckLoad(bool &outIsValid,u32 &loadSize)
	{
		if (m_State != fiSaveGameState::LOADING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				outIsValid = false;
				loadSize = 0;
			}
			else
			{
				m_State = fiSaveGameState::LOADED_CONTENT;
				outIsValid = true;
				loadSize = s_SizeOfLoadedData;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndLoad()
	{
		sgoDebug1("Trace: EndLoad");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndLoad time",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::LOADED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}


	bool OrbisProfile::MountForFileDeletion(const char *pFilename, SceSaveDataMountResult &mountResult)
	{
		SceSaveDataMount mount;
		eSavegameType savegameType = GetSavegameTypeFromFilename(pFilename, false);
		u32 maxMountSize = GetMaxMountSizeFromSavegameType(savegameType);

		if (savegameType == SG_TYPE_PHOTO)
		{
			UnmountPhotoDirectoryForReading();	//	Should I return false if this call returns an error?
		}

		//	I'm not sure if it's necessary to pass maxMountSize for deletion. I think it's only required when creating a new mount directory
		SetupSceSaveDataMount(s_UserId, SCE_SAVE_DATA_MOUNT_MODE_RDWR, &mount, maxMountSize, savegameType, pFilename);

		sgotimingDebugf1("%d - about to call sceSaveDataMount for file deletion",sysTimer::GetSystemMsTime());

		memset(&mountResult, 0x00, sizeof(SceSaveDataMountResult));
		int ret = sceSaveDataMount(&mount, &mountResult);

		sgotimingDebugf1("%d - sceSaveDataMount has finished",sysTimer::GetSystemMsTime());

		switch (ret)
		{
		case SCE_OK :
			return true;
//			break;

		case SCE_SAVE_DATA_ERROR_NO_SPACE_FS :
			sgoAssertf(0, "OrbisProfile::MountForFileDeletion - didn't expect sceSaveDataMount to return SCE_SAVE_DATA_ERROR_NO_SPACE_FS when mounting for file deletion");
			ShowNoSpaceDialog(mount, mountResult);
			return false;
//			break;

		case SCE_SAVE_DATA_ERROR_NOT_FOUND :	//	if the mount directory doesn't exist at all, should I consider that an error? Maybe it would be okay to consider that a successful file deletion
//		case SCE_SAVE_DATA_ERROR_NO_SPACE :		//	Apparently this can no longer happen
		case SCE_SAVE_DATA_ERROR_BROKEN :
		default :
			sgoDebug1("OrbisProfile::MountForFileDeletion - sceSaveDataMount returned 0x%08x\n", ret);
			return false;
//			break;
		}

		return false;
	}

	void OrbisProfile::s_FileDeletion(void*)
	{
		SceSaveDataMountResult mountResult;

		if (!MountForFileDeletion(s_Filename, mountResult))
		{
			sgoError("OrbisProfile::s_FileDeletion - MountForFileDeletion failed");
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		// create path - i.e. /savedata01/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		snprintf(path, sizeof(path), "%s/%s", mountResult.mountPoint.data, s_Filename);

		SceKernelStat st;
		int ret = sceKernelStat(path, &st);
		if(ret < SCE_OK)
		{
			sgoError("sceKernelStat - Couldn't read file, err: 0x%08x(%s)\n", ret, path);
			s_FileOperationError = true;
		}
		else
		{
			ret= sceKernelUnlink(path);
			if(ret < SCE_OK)
			{
				sgoError("sceKernelUnlink(delete) - Couldn't delete file, 0x%08x(%s)\n", ret, path);
				s_FileOperationError = true;
			}
		}

		// attempt to unmount data prior to exit
		ret = sceSaveDataUmount(&mountResult.mountPoint);
		if(ret < SCE_OK)
		{
			sgoError("sceSaveDataUmount : 0x%08x\n", ret);
			s_FileOperationError = true;
		}

		s_FileOperationDone = true;
	}
#if USE_SAVE_DATA_MEMORY
	void OrbisProfile::s_FileDeletionSaveDataMemory(void*)
	{
		u32 uBufferSize = Header::MAX_BYTE_SIZEOF_PAYLOAD;
		// Create buffer to contain header + data.  data is point offset by header size.
		u8* buf = rage_new u8[uBufferSize];		
		memset(buf, '\0', uBufferSize);
		char16 title[SAVE_DATA_MAX_DISPLAY_NAME] = {0};
		memset(title, '\0', sizeof(title));
		// add header and copy game data to offset pointer
		Header header;
		header.m_mtime = 0; //this signals the file's deletion!
		header.m_size = 0;
		if(header.Export(title, buf, uBufferSize, NULL) == false)
		{
			sgoError("Exporting the header failed!");
			delete[] buf;			
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}		
		if(!WriteSaveDataMemory(buf, uBufferSize))
			s_FileOperationError = true;

		delete[] buf;
		s_FileOperationDone = true;
	}
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
	void OrbisProfile::s_FileDeletionDownload0Backup(void*)
	{
		// create path - i.e. /download0/saves/<userId>/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		GetNameOfDownload0Path(path, sizeof(path), s_Filename, DOWNLOAD0_USER_FOLDER);

		SceKernelStat st;
		int ret = sceKernelStat(path, &st);
		if(ret < SCE_OK)
		{
			sgoError("OrbisProfile::s_FileDeletionDownload0Backup - sceKernelStat - Couldn't read file, err: 0x%08x(%s)\n", ret, path);
			s_FileOperationError = true;
		}
		else
		{
			ret= sceKernelUnlink(path);
			if(ret < SCE_OK)
			{
				sgoError("OrbisProfile::s_FileDeletionDownload0Backup - sceKernelUnlink(delete) - Couldn't delete file, 0x%08x(%s)\n", ret, path);
				s_FileOperationError = true;
			}
		}

		s_FileOperationDone = true;
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP


	bool OrbisProfile::BeginDeleteFile(u32 /*contentType*/, const char *filename)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("BeginDeleteFile - Cannot begin deleting file, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("BeginDeleteFile - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginDeleteFile - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_Filename = filename;

		sgotimingDebugf1("%d - BeginDeleteFile started",sysTimer::GetSystemMsTime());
		#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, filename))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileDeletionSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] FileDelThread");	}
		else
		#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(filename))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileDeletionDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] FileDelThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileDeletion,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] FileDelThread");
			}
		}

		m_State = DELETING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckDeleteFile()
	{
		if (m_State != fiSaveGameState::DELETING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
			}
			else
			{
				m_State = fiSaveGameState::DELETED_CONTENT;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndDeleteFile()
	{
		sgoDebug1("Trace: EndDeleteFile");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndDeleteFile time",sysTimer::GetSystemMsTime());
		}
			
		if (m_State == fiSaveGameState::DELETED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}


	void OrbisProfile::s_DeleteMount(void*)
	{
		SceSaveDataDirName saveDataDirName;
		memset(&saveDataDirName, 0, sizeof(saveDataDirName));

		if (s_bMountNameToDeleteIsAFilename)
		{
			eSavegameType savegameType = GetSavegameTypeFromFilename(s_MountNameToDelete, false);
			GetDirectoryNameFromFileNameAndType(savegameType, s_MountNameToDelete, saveDataDirName);
		}
		else
		{
			safecpy(saveDataDirName.data, s_MountNameToDelete);
		}

		int ret = DeleteMount(&saveDataDirName);
		if (ret < SCE_OK)
		{
			s_FileOperationError = true;
		}

		s_FileOperationDone = true;
	}

	bool OrbisProfile::BeginDeleteMount(const char *pName, bool bIsFilename)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("Cannot begin deleting a mount, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("BeginDeleteMount - didn't finish properly, can't start.");
			return false;
		}

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_MountNameToDelete = pName;
		s_bMountNameToDeleteIsAFilename = bIsFilename;

		sgotimingDebugf1("%d - BeginDeleteMount started",sysTimer::GetSystemMsTime());
#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, s_MountNameToDelete))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileDeletionSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] DelMountThread");}
		else
#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(s_MountNameToDelete))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_FileDeletionDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] DelMountThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_DeleteMount,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] DelMountThread");
			}
		}

		m_State = DELETING_CONTENT;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckDeleteMount()
	{
		if (m_State != fiSaveGameState::DELETING_CONTENT)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
			}
			else
			{
				m_State = fiSaveGameState::DELETED_CONTENT;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndDeleteMount()
	{
		sgoDebug1("Trace: EndDeleteMount");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndDeleteMount time",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::DELETED_CONTENT)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}


	bool OrbisProfile::BeginGetCreator(u32 /*contentType */,u32 /* deviceId */,const char * /* filename */)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		m_State = fiSaveGameState::GETTING_CREATOR;
		return true;
	}

	bool OrbisProfile::CheckGetCreator(bool &bIsCreator)
	{
		if (m_State != fiSaveGameState::GETTING_CREATOR)
		{
			return false;
		}

		m_State = fiSaveGameState::GOT_CREATOR;
		bIsCreator = true;
		return true;
	}


	void OrbisProfile::EndGetCreator()
	{
		if (m_State == fiSaveGameState::GOT_CREATOR)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	void OrbisProfile::s_BeginFreeSpaceCheck(void*)
	{
		SceSaveDataMountResult mountResult;
		bool bDataCreated = true;

		if (!MountForSaving(s_Filename, mountResult, bDataCreated))
		{
			sgoError("OrbisProfile::s_BeginFreeSpaceCheck - MountForReading failed");
			s_FileOperationError = true;
			s_ExtraSpaceRequired = mountResult.requiredBlocks * SCE_SAVE_DATA_BLOCK_SIZE;
		}
		else
		{
			sceSaveDataUmount(&mountResult.mountPoint);

			//since the data was only temporary let's delete it once 
			//we are sure we have enough space
			SceSaveDataDirName saveDataDirName;
			memset(&saveDataDirName, 0, sizeof(saveDataDirName));
			eSavegameType savegameType = GetSavegameTypeFromFilename(s_Filename, false);
			GetDirectoryNameFromFileNameAndType(savegameType, s_Filename, saveDataDirName);		
			if (DeleteMount(&saveDataDirName) < SCE_OK)
			{
				//perhaps we don't care if there was an error on trying to delete this file
				//s_FileOperationError = true;
			}
		}

		s_FileOperationDone = true;
	}

	bool OrbisProfile::BeginFreeSpaceCheck(const char* filename,u32 saveSize)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			return false;
		}

		if(saveSize == 0)
		{
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("s_BeginFreeSpaceCheck - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginFreeSpaceCheck - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_SizeOfCurrentFile = saveSize;
		s_Filename = filename;

		sgotimingDebugf1("%d - BeginFreeSpaceCheck started",sysTimer::GetSystemMsTime());

		s_ThreadId = sysIpcCreateThread(OrbisProfile::s_BeginFreeSpaceCheck,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] FreeSpaceThread");

		m_State = CHECKING_FREE_SPACE;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckFreeSpaceCheck(int& ExtraSpaceRequired)
	{
		if (m_State != fiSaveGameState::CHECKING_FREE_SPACE)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				ExtraSpaceRequired = s_ExtraSpaceRequired;
				sgoWarning("CheckFreeSpaceCheck: failed so setting extra space required to 0");
			}
			else
			{
				m_State = fiSaveGameState::HAVE_CHECKED_FREE_SPACE;
				ExtraSpaceRequired = s_ExtraSpaceRequired;
				sgoDebug1("CheckFreeSpaceCheck: extra space required %d", ExtraSpaceRequired);
			}

			return true;
		}

		return false;
	}

	void OrbisProfile::EndFreeSpaceCheck()
	{
		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndFreeSpaceCheck time",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::HAVE_CHECKED_FREE_SPACE)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}


	void OrbisProfile::s_GetFileSize(void*)
	{
		int ret = SCE_OK;
		SceSaveDataMountPoint mountPoint;
		bool bUnmountWhenFinished = true;

		if (!MountForReading(s_Filename, mountPoint, bUnmountWhenFinished))
		{
			sgoError("OrbisProfile::s_GetFileSize - MountForReading failed");
			s_FileOperationError = true;
			s_FileOperationDone = true;
			return;
		}

		// create path - i.e. /savedata01/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		snprintf(path, sizeof(path), "%s/%s", mountPoint.data, s_Filename);

		const fiDevice *device = fiDevice::GetDevice(mountPoint.data);
		s_FileSize = device->GetFileSize(path);

		// File size should at the very least be larger than the header
		if(s_FileSize < Header::MAX_BYTE_SIZEOF_PAYLOAD)
		{
			sgoError("OrbisProfile::s_GetFileSize - s_FileSize is less than Header::MAX_BYTE_SIZEOF_PAYLOAD (%u)\n", Header::MAX_BYTE_SIZEOF_PAYLOAD);
			s_FileOperationError = true;
		}
		else
		{
			// subtract header size from file size for ps4 save files
#if __BANK
			if (!PARAM_usePS3saves.Get())
#endif
			{
				s_FileSize -= Header::MAX_BYTE_SIZEOF_PAYLOAD;
			}
		}

		if (bUnmountWhenFinished)
		{
			ret = sceSaveDataUmount(&mountPoint);
			if(ret < SCE_OK)
			{
				sgoError("sceSaveDataUmount : 0x%08x\n", ret);
				s_FileOperationError = true;
			}
		}

		s_FileOperationDone = true;
	}
#if USE_SAVE_DATA_MEMORY
	void OrbisProfile::s_GetFileSizeSaveDataMemory(void*)
	{
		Header header;
		if(!ReadSaveDataMemoryHeader(&header))
		{
			s_FileOperationError = true;
		}
		else
		{
			s_FileSize = header.m_size;
		}
		s_FileOperationDone = true;		
	}
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
	void OrbisProfile::s_GetFileSizeDownload0Backup(void*)
	{
		Header header;

		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		GetNameOfDownload0Path(path, sizeof(path), s_Filename, DOWNLOAD0_USER_FOLDER);

		if(!ReadDownload0BackupHeader(path, &header))
		{
			s_FileOperationError = true;
		}
		else
		{
			s_FileSize = header.m_size;
		}
		s_FileOperationDone = true;
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

	bool OrbisProfile::BeginGetFileSize(const char* filename, bool /*bCalculateSizeOnDisk*/)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("OrbisProfile::BeginGetFileSize - Cannot begin getting file size, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("OrbisProfile::BeginGetFileSize - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginGetFileSize - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_Filename = filename;
		s_FileSize = 0;

		sgotimingDebugf1("%d - BeginGetFileSize started",sysTimer::GetSystemMsTime());
#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, filename))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileSizeSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] GetFileSizeThread");}
		else
#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(filename))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileSizeDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] GetFileSizeThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileSize,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] GetFileSizeThread");
			}
		}

		m_State = fiSaveGameState::GETTING_FILE_SIZE;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckGetFileSize(u64 &fileSize)
	{
		if (m_State != fiSaveGameState::GETTING_FILE_SIZE)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				fileSize = 0;
			}
			else
			{
				m_State = fiSaveGameState::HAVE_GOT_FILE_SIZE;
				fileSize = s_FileSize;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndGetFileSize()
	{
		sgoDebug1("Trace: EndGetFileSize");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndGetFileSize time",sysTimer::GetSystemMsTime());
		}

		if(m_State == fiSaveGameState::HAVE_GOT_FILE_SIZE)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	u64 OrbisProfile::CalculateDataSizeOnDisk(rage::u32 dataSize)
	{
		// account for security and filesystem alignment (assume 4k?)
#define ALIGN_4K(x) ((x) + ((4 * 1024) - 1)) & ~((4 * 1024) - 1);
		return ALIGN_4K(dataSize + Header::MAX_BYTE_SIZEOF_PAYLOAD);
	}

	u64 OrbisProfile::GetTotalAvailableSpace()
	{
		return 0;	// This function is not actually used at this time (May 26/14)
	}

	bool OrbisProfile::GetDisplayNameAndModifiedTime(u32 signInId, fiSaveGame::Content &outContent, const char* mountName, const char* filename)
	{
		// Clear display name
		memset(outContent.DisplayName, 0, sizeof(outContent.DisplayName));

#if __BANK
		if (PARAM_usePS3saves.Get())
		{
			// convert display name into header title
			char16 title[SAVE_DATA_MAX_DISPLAY_NAME] = {0};
			safecpy(title, "PS3 Save");
			safecpy(outContent.DisplayName, title);
			return true;
		}
#endif

		// create path - i.e. /savedata01/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		snprintf(path, sizeof(path), "%s/%s", mountName, filename);

		fiStream *S = fiStream::Open(path);
		if (!S)
		{
			return false;
		}


		// Read header from file
		Header header;
		u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD] = {0};		
		int bytesRead = S->Read(buf, sizeof(buf));
		S->Close();

		if(bytesRead != sizeof(buf))
		{
			return false;
		}

		if(header.Import(buf, sizeof(buf)) == false)
		{
			return false;
		}

		// write the display name component of the header
		safecpy(outContent.DisplayName, header.GetDisplayName());


		SceKernelStat st;
		int ret = sceKernelStat(path, &st);
		if(ret < SCE_OK)
		{
			sgoError("OrbisProfile::GetDisplayNameAndModifiedTime - sceKernelStat - Couldn't read file, err: 0x%08x(%s)\n", ret, path);
			return false;
		}

		outContent.ModificationTimeHigh = st.st_mtime >> 32;
		outContent.ModificationTimeLow = st.st_mtime;

		return true;
	}

	void OrbisProfile::s_GetFileModifiedTime(void*)
	{
		SceSaveDataDirName searchDirName;

		eSavegameType savegameType = GetSavegameTypeFromFilename(s_Filename, false);
		GetDirectoryNameFromFileNameAndType(savegameType, s_Filename, searchDirName);

		// Obtain list of directories to mount. There should only be one match
		SceSaveDataDirNameSearchCond cond;
		memset(&cond, 0x00, sizeof(SceSaveDataDirNameSearchCond));
		cond.userId = s_UserId;
		cond.titleId = NULL;
		cond.dirName = &searchDirName;
		cond.key = SCE_SAVE_DATA_SORT_KEY_MTIME;
		cond.order = SCE_SAVE_DATA_SORT_ORDER_DESCENT;

		SceSaveDataDirNameSearchResult result;
		memset(&result, 0x00, sizeof(result));

		SceSaveDataDirName dirNameOfResults;
		SceSaveDataParam paramsForResults;

		result.dirNames = &dirNameOfResults;
		result.params = &paramsForResults;

		result.dirNamesNum = 1;

		if ( sceSaveDataDirNameSearch(&cond, &result) < 0 ) {
			// Error handling
			result.hitNum = 0;
			result.setNum = 0;
		}

		if (result.hitNum > 0)
		{
			sgoAssertf(result.hitNum == 1, "OrbisProfile::s_GetFileModifiedTime - sceSaveDataDirNameSearch found %u matching directories. I only expected 1 at most", result.hitNum);
			sgoAssertf(result.hitNum == result.setNum, "OrbisProfile::s_GetFileModifiedTime - sceSaveDataDirNameSearch results - result.hitNum (%u) doesn't equal result.setNum (%u)", result.hitNum, result.setNum);
			
			s_modTimeHigh = paramsForResults.mtime >> 32;
			s_modTimeLow = paramsForResults.mtime;
		}

		s_FileOperationDone = true;
	}
#if USE_SAVE_DATA_MEMORY
	void OrbisProfile::s_GetFileModifiedTimeSaveDataMemory(void*)
	{
		Header header;
		if(!ReadSaveDataMemoryHeader(&header))
		{
			s_FileOperationError = true;
		}
		else
		{
			s_modTimeHigh = header.m_mtime >> 32;
			s_modTimeLow = header.m_mtime;			
		}
		s_FileOperationDone = true;
	}
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
	void OrbisProfile::s_GetFileModifiedTimeDownload0Backup(void*)
	{
		//	I could probably use SceKernelStat for this
		Header header;

		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		GetNameOfDownload0Path(path, sizeof(path), s_Filename, DOWNLOAD0_USER_FOLDER);

		if(!ReadDownload0BackupHeader(path, &header))
		{
			s_FileOperationError = true;
		}
		else
		{
			s_modTimeHigh = header.m_mtime >> 32;
			s_modTimeLow = header.m_mtime;			
		}
		s_FileOperationDone = true;
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

	bool OrbisProfile::BeginGetFileModifiedTime(const char *filename)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("Cannot begin getting file modified time, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("BeginGetFileModifiedTime - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginGetFileModifiedTime - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_Filename = filename;
		s_modTimeHigh = 0;
		s_modTimeLow = 0;

		sgotimingDebugf1("%d - BeginGetFileModifiedTime started",sysTimer::GetSystemMsTime());
#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, filename))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileModifiedTimeSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveModTimeThread");}
		else
#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(filename))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileModifiedTimeDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveModTimeThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileModifiedTime,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveModTimeThread");
			}
		}

		m_State = GETTING_FILE_MODIFIED_TIME;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckGetFileModifiedTime(u32& modTimeHigh, u32& modTimeLow)
	{
		if (m_State != fiSaveGameState::GETTING_FILE_MODIFIED_TIME)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				modTimeHigh = 0;
				modTimeLow = 0;
			}
			else
			{
				m_State = fiSaveGameState::HAVE_GOT_FILE_MODIFIED_TIME;
				modTimeHigh = s_modTimeHigh;
				modTimeLow = s_modTimeLow;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndGetFileModifiedTime()
	{
		sgoDebug1("Trace: EndGetFileModifiedTime");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndGetFileModifiedTime time",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::HAVE_GOT_FILE_MODIFIED_TIME)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}

	void OrbisProfile::s_GetFileExists(void*)
	{
		SceSaveDataDirName searchDirName;

		eSavegameType savegameType = GetSavegameTypeFromFilename(s_Filename, false);
		GetDirectoryNameFromFileNameAndType(savegameType, s_Filename, searchDirName);

		// Obtain list of directories to mount. There should only be one match
		SceSaveDataDirNameSearchCond cond;
		memset(&cond, 0x00, sizeof(SceSaveDataDirNameSearchCond));
		cond.userId = s_UserId;
		cond.titleId = NULL;
		cond.dirName = &searchDirName;
		cond.key = SCE_SAVE_DATA_SORT_KEY_DIRNAME;
		cond.order = SCE_SAVE_DATA_SORT_ORDER_DESCENT;

		SceSaveDataDirNameSearchResult result;
		memset(&result, 0x00, sizeof(result));

		SceSaveDataDirName dirNameOfResults;
		SceSaveDataParam paramsForResults;

		result.dirNames = &dirNameOfResults;
		result.params = &paramsForResults;

		result.dirNamesNum = 1;

		if ( sceSaveDataDirNameSearch(&cond, &result) < 0 ) {
			// Error handling
			s_fileExists = false;
		}

		if (result.hitNum > 0)
		{
			s_fileExists = true;
		}

		s_FileOperationDone = true;
	}
#if USE_SAVE_DATA_MEMORY
	void OrbisProfile::s_GetFileExistsSaveDataMemory(void*)
	{
		Header header;
		ReadSaveDataMemoryHeader(&header);
		s_fileExists = header.m_mtime != 0;
		s_FileOperationDone = true;
	}
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
	void OrbisProfile::s_GetFileExistsDownload0Backup(void*)
	{
		// create path - i.e. /download0/saves/<userId>/filename
		char path[SAVE_DATA_FILEPATH_LENGTH + 1];
		GetNameOfDownload0Path(path, sizeof(path), s_Filename, DOWNLOAD0_USER_FOLDER);

		s_fileExists = false;
		//	The documentation says that sceKernelCheckReachability is slow. Maybe I can use SceKernelStat instead.
		if (sceKernelCheckReachability(path) == SCE_OK)
		{
			s_fileExists = true;
		}

		s_FileOperationDone = true;
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP

	bool OrbisProfile::BeginGetFileExists(const char *filename)
	{
		if (m_State != fiSaveGameState::IDLE && m_State != fiSaveGameState::HAD_ERROR)
		{
			sgoWarning("Cannot begin getting file exists, operation already in progress");
			return false;
		}

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sgoError("BeginGetFileExists - didn't finish properly, can't start.");
			return false;
		}

		sgoAssertf(filename && strlen(filename) > 0, "OrbisProfile::BeginGetFileExists - filename is empty");

		s_FileOperationDone = false;
		s_FileOperationError = false;
		s_UserId = m_UserId;
		s_Filename = filename;		
		s_fileExists = false;

		sgotimingDebugf1("%d - BeginGetFileExists started",sysTimer::GetSystemMsTime());
#if USE_SAVE_DATA_MEMORY
		if(!strcmp(SAVEDATAMEMORY_NAME, filename))
		{	s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileExistsSaveDataMemory,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveExistsThread");}
		else
#endif
		{
#if USE_DOWNLOAD0_FOR_BACKUP
			if (ShouldUseDownload0(filename))
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileExistsDownload0Backup,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveExistsThread");
			}
			else
#endif	//	USE_DOWNLOAD0_FOR_BACKUP
			{
				s_ThreadId = sysIpcCreateThread(OrbisProfile::s_GetFileExists,NULL,SAVEGAME_STACK_SIZE,THREAD_PRIORITY,"[RAGE] SaveExistsThread");
			}
		}

		m_State = GETTING_FILE_EXISTS;
		return s_ThreadId != sysIpcThreadIdInvalid;
	}

	bool OrbisProfile::CheckGetFileExists(bool& exists)
	{
		if (m_State != fiSaveGameState::GETTING_FILE_EXISTS)
		{
			return false;
		}

		if (s_FileOperationDone)
		{
			// Wait for thread to finish.
			if (s_FileOperationError)
			{
				m_State = fiSaveGameState::HAD_ERROR;
				exists = false;
			}
			else
			{
				m_State = fiSaveGameState::HAVE_GOT_FILE_EXISTS;
				exists = s_fileExists;
			}
			return true;
		}

		return false;
	}

	void OrbisProfile::EndGetFileExists()
	{
		sgoDebug1("Trace: EndGetFileExists");

		if (s_ThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_ThreadId);
			s_ThreadId = sysIpcThreadIdInvalid;
			sgotimingDebugf1("%d - EndGetFileExists time",sysTimer::GetSystemMsTime());
		}

		if (m_State == fiSaveGameState::HAVE_GOT_FILE_EXISTS)
		{
			m_State = fiSaveGameState::IDLE;
		}
	}
#if USE_SAVE_DATA_MEMORY
	u32 OrbisProfile::InitialiseSaveDataMemory(void)
	{
		return OrbisProfile::InitialiseSaveDataMemory(m_UserId);
	}

	u32 OrbisProfile::InitialiseSaveDataMemory(SceUserServiceUserId userID)
	{
		//In PS4 OS this will always be known as autosave
		//we will update the subtitle periodically in game so that 
		//in game the description matches the mission
		SceSaveDataMemoryParam  sdmParam;
		memset(&sdmParam, 0x00, sizeof(SceSaveDataMemoryParam));
		strncpy(sdmParam.title, "Autosave", sizeof(sdmParam.title)-1);
		sdmParam.title[sizeof(sdmParam.title)-1] = '\0';
		int ret = sceSaveDataSetupSaveDataMemory(userID, SAVEDATAMEMORY_SIZE, &sdmParam);
		return ret;

	}

	bool OrbisProfile::ReadSaveDataMemoryHeader(Header* pHeader)
	{
		Assert(pHeader != NULL);		
		u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD];

		int ret = sceSaveDataGetSaveDataMemory(s_UserId, (void*)buf, Header::MAX_BYTE_SIZEOF_PAYLOAD, 0);		
		if( ret != SCE_OK)
		{
			InitialiseSaveDataMemory(s_UserId);
			ret = sceSaveDataGetSaveDataMemory(s_UserId, (void*)buf, Header::MAX_BYTE_SIZEOF_PAYLOAD, 0);		
		}

		if( ret != SCE_OK)
		{
			sgoError("sceSaveDataGetSaveDataMemory: failed, returned %x", ret);
			return false;
		}		
		else
		{
			pHeader->Import(buf, Header::MAX_BYTE_SIZEOF_PAYLOAD);
		}
		return true;
	}
	
	bool OrbisProfile::ReadSaveDataMemory(void * buffer, size_t size)
	{
		int ret = sceSaveDataGetSaveDataMemory(s_UserId, buffer, size, 0);		
		if( ret != SCE_OK)
		{
			InitialiseSaveDataMemory(s_UserId);
			ret = sceSaveDataGetSaveDataMemory(s_UserId, buffer, size, 0);		
		}
		if( ret != SCE_OK)
		{
			sgoError("sceSaveDataGetSaveDataMemory: failed, returned %x", ret);
			return false;
		}		
		return true;
	}

	bool OrbisProfile::WriteSaveDataMemory(void* buffer, size_t size)
	{
		int ret = sceSaveDataSetSaveDataMemory(s_UserId, buffer, size, 0);		
		if( ret != SCE_OK)
		{
			InitialiseSaveDataMemory(s_UserId);
			ret = sceSaveDataSetSaveDataMemory(s_UserId, buffer, size, 0);		
		}
		if( ret != SCE_OK)
		{
			sgoError("sceSaveDataSetSaveDataMemory: failed, returned %x", ret);
			return false;
		}		
		return true;
	}
#endif

#if USE_DOWNLOAD0_FOR_BACKUP
	bool OrbisProfile::ReadDownload0BackupHeader(const char *pPath, Header* pHeader)
	{
		Assert(pHeader != NULL);
		u8 buf[Header::MAX_BYTE_SIZEOF_PAYLOAD];

		int fd = sceKernelOpen(pPath, SCE_KERNEL_O_RDONLY, 0);

		if (fd < 0)
		{
			sgoError("OrbisProfile::ReadDownload0BackupHeader - sceKernelOpen failed, returned %x", fd);
			return false;
		}

		ssize_t numBytesRead = sceKernelRead(fd, &buf, Header::MAX_BYTE_SIZEOF_PAYLOAD);

		if (numBytesRead != Header::MAX_BYTE_SIZEOF_PAYLOAD)
		{
			sgoError("OrbisProfile::ReadDownload0BackupHeader - sceKernelRead failed, returned %x. Expected to read %d bytes", (int) numBytesRead, Header::MAX_BYTE_SIZEOF_PAYLOAD);
			sceKernelClose(fd);
			return false;
		}

		pHeader->Import(buf, Header::MAX_BYTE_SIZEOF_PAYLOAD);

		sceKernelClose(fd);

		return true;
	}

	bool OrbisProfile::ReadDownload0Backup(const char *pPath, void * buffer, size_t size)
	{
		int fd = sceKernelOpen(pPath, SCE_KERNEL_O_RDONLY, 0);

		if (fd < 0)
		{
			sgoError("OrbisProfile::ReadDownload0Backup - sceKernelOpen failed, returned %x", fd);
			return false;
		}

		ssize_t numBytesRead = sceKernelRead(fd, buffer, size);

		if (numBytesRead != size)
		{
			sgoError("OrbisProfile::ReadDownload0Backup - sceKernelRead failed, returned %x. Expected to read %d bytes", (int) numBytesRead, (int) size);
			sceKernelClose(fd);
			return false;
		}

		sceKernelClose(fd);

		return true;
	}

	bool OrbisProfile::WriteDownload0Backup(const char *pPath, void* buffer, size_t size, bool forceSync)
	{
		int fd = sceKernelOpen(pPath, SCE_KERNEL_O_WRONLY|SCE_KERNEL_O_TRUNC|SCE_KERNEL_O_CREAT, SCE_KERNEL_S_IRWU);

		if (fd < 0)
		{
			sgoError("OrbisProfile::WriteDownload0Backup - sceKernelOpen failed, returned %x", fd);
			return false;
		}

		ssize_t numBytesWritten = sceKernelWrite(fd, buffer, size);

		if (numBytesWritten != size)
		{
			sgoError("OrbisProfile::WriteDownload0Backup - sceKernelWrite failed, returned %x. Expected to write %d bytes", (int) numBytesWritten, (int) size);
			sceKernelClose(fd);
			return false;
		}

		if(forceSync)
		{
			sceKernelFsync(fd);
		}

		sceKernelClose(fd);

		return true;
	}
#endif	//	USE_DOWNLOAD0_FOR_BACKUP


	//**************************************************************************************************
	// fiSaveGame functions
	//**************************************************************************************************
	void fiSaveGame::InitClass()
	{
		sgoDebug1("Trace: InitClass");

		SceSaveDataInitParams init_param;
		memset(&init_param, 0x00, sizeof(init_param));
		init_param.priority = SCE_KERNEL_PRIO_FIFO_DEFAULT;
		int ret = sceSaveDataInitialize(&init_param);
		if (ret < SCE_OK)
		{
			sgoWarning("sceSaveDataInitialize : 0x%08x\n", ret);
		}
	}
	
	void fiSaveGame::ShutdownClass()
	{
		sgoDebug1("Trace: ShutdownClass");

		int ret = sceSaveDataTerminate();
		if(ret < SCE_OK)
		{
			sgoWarning("sceSaveDataTerminate : 0x%08x\n", ret);
		}
	}

	void fiSaveGame::SetIcon(char *pIcon, const u32 SizeOfIconFile)
	{
		sgoDebug1("Trace: SetIcon");
		Assert(pIconImage == NULL);		//	Should only need to call SetIcon() once at the start of the game.
		Assert(SizeOfIconImage == 0);	//	Just remove these two Asserts if that is not the case.
		pIconImage = pIcon;
		SizeOfIconImage = SizeOfIconFile;
	}

	void fiSaveGame::SetLocalisedBackupTitleString(const char *pLocalisedBackupString)
	{
		if (Verifyf(pLocalisedBackupString && (strlen(pLocalisedBackupString) > 0), "fiSaveGame::SetLocalisedBackupTitleString - pLocalisedBackupString is empty"))
		{
			safecpy(sLocalisedBackupString, pLocalisedBackupString);
		}
	}

	u64 fiSaveGame::CalculateDataSizeOnDisk(int signInId , rage::u32 dataSize)
	{
		sgoDebug1("Trace: CalculateDataSizeOnDisk");
		return s_UserProfiles[signInId].CalculateDataSizeOnDisk(dataSize);
	}

	fiSaveGameState::State fiSaveGame::GetState(int signInId) const 
	{ 
		sgoDebug1("Trace: GetState");
		return s_UserProfiles[signInId].GetState(); 
	}

	bool fiSaveGame::BeginSelectDevice(int signInId,u32 contentType,u32 minSpaceAvailable,bool forceShow,bool manageStorage)
	{
		sgoDebug1("Trace: BeginSelectDevice");
		return s_UserProfiles[signInId].BeginSelectDevice(contentType,minSpaceAvailable,forceShow,manageStorage);
	}

	bool fiSaveGame::CheckSelectDevice(int signInId)
	{
		sgoDebug1("Trace: CheckSelectDevice");
		return s_UserProfiles[signInId].CheckSelectDevice();
	}

	u32 fiSaveGame::GetSelectedDevice(int) 
	{
		sgoDebug1("Trace: GetSelectedDevice");
		return 1;	// same as Xenon Hard Drive
	}


	void fiSaveGame::SetSelectedDevice(int, u32) 
	{
		sgoDebug1("Trace: SetSelectedDevice");
	}

	void fiSaveGame::EndSelectDevice(int signInId)
	{
		sgoDebug1("Trace: EndSelectDevice");
		return s_UserProfiles[signInId].EndSelectDevice();
	}

	bool fiSaveGame::BeginEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount)
	{
		sgoDebug1("Trace: BeginEnumeration");
		return s_UserProfiles[signInId].BeginEnumeration(contentType,outContent,maxCount,(int)OrbisProfile::SG_TYPE_SAVEGAME, NULL);	//	will also find SG_TYPE_BACKUP
	}

	bool fiSaveGame::BeginPhotoEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount)
	{
		sgoDebug1("Trace: BeginPhotoEnumeration");
		return s_UserProfiles[signInId].BeginEnumeration(contentType,outContent,maxCount,(int)OrbisProfile::SG_TYPE_PHOTO, NULL);
	}

	bool fiSaveGame::BeginClipEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount, void* validateFunc)
	{
		sgoDebug1("Trace: BeginClipEnumeration");
		return s_UserProfiles[signInId].BeginEnumeration(contentType, outContent, maxCount, (int)OrbisProfile::SG_TYPE_CLIP, validateFunc);
	}

	bool fiSaveGame::BeginMontageEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount,void *validateFunc)
	{
		sgoDebug1("Trace: BeginMontageEnumeration");
		return s_UserProfiles[signInId].BeginEnumeration(contentType, outContent, maxCount, (int)OrbisProfile::SG_TYPE_MONTAGE, validateFunc);
	}

	bool fiSaveGame::CheckEnumeration(int signInId,int &outCount)
	{
		sgoDebug1("Trace: CheckEnumeration");
		return s_UserProfiles[signInId].CheckEnumeration(outCount);
	}

	void fiSaveGame::EndEnumeration(int signInId)
	{
		sgoDebug1("Trace: EndEnumeration");
		s_UserProfiles[signInId].EndEnumeration();
	}

	bool fiSaveGame::BeginSave(int signInId,u32 contentType,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool overwrite)
	{
		sgoDebug1("Trace: BeginSave");
		return s_UserProfiles[signInId].BeginSave(contentType,displayName,filename,saveData,saveSize,overwrite);
	}

	bool fiSaveGame::CheckSave(int signInId,bool &outIsValid,bool &fileExists)
	{
		sgoDebug1("Trace: CheckSave");
		return s_UserProfiles[signInId].CheckSave(outIsValid,fileExists);
	}

	void fiSaveGame::EndSave(int signInId)
	{
		sgoDebug1("Trace: EndSave");
		s_UserProfiles[signInId].EndSave();
	}

	bool fiSaveGame::BeginLoad(int signInId,u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize)
	{
		sgoDebug1("Trace: BeginLoad");
		return s_UserProfiles[signInId].BeginLoad(contentType,deviceId,filename,loadData,loadSize);
	}

	bool fiSaveGame::CheckLoad(int signInId,bool &outIsValid,u32 &loadSize)
	{
		sgoDebug1("Trace: CheckLoad");
		return s_UserProfiles[signInId].CheckLoad(outIsValid,loadSize);
	}

	void fiSaveGame::EndLoad(int signInId)
	{
		sgoDebug1("Trace: EndLoad");
		s_UserProfiles[signInId].EndLoad();
	}

	bool fiSaveGame::BeginIconSave(int /*signInId*/,u32 /*contentType*/,const char* /*filename*/,const void* /*iconData*/,u32 /*iconSize*/)
	{
		sgoDebug1("Trace: BeginIconSave - does nothing");
		return true;
	}

	bool fiSaveGame::CheckIconSave(int /*signInId*/)
	{
		sgoDebug1("Trace: CheckIconSave - does nothing");
		return true;
	}

	void fiSaveGame::EndIconSave(int /*signInId*/)
	{
		sgoDebug1("Trace: EndIconSave - does nothing");
	}

	bool fiSaveGame::BeginDelete(int signInId,u32 contentType,const char *filename)
	{
		sgoDebug1("Trace: BeginDelete");
		return s_UserProfiles[signInId].BeginDeleteFile(contentType,filename);
	}


	bool fiSaveGame::CheckDelete(int signInId)
	{
		sgoDebug1("Trace: CheckDelete");
		return s_UserProfiles[signInId].CheckDeleteFile();
	}


	void fiSaveGame::EndDelete(int signInId)
	{
		sgoDebug1("Trace: EndDelete");
		s_UserProfiles[signInId].EndDeleteFile();
	}

	bool fiSaveGame::BeginGetCreator(int signInId, u32 contentType,u32 deviceId,const char *filename)
	{
		sgoDebug1("Trace: BeginGetCreator");
		return s_UserProfiles[signInId].BeginGetCreator(contentType,deviceId,filename);
	}

	bool fiSaveGame::CheckGetCreator(int signInId, bool &bIsCreator)
	{
		sgoDebug1("Trace: CheckGetCreator");
		return s_UserProfiles[signInId].CheckGetCreator(bIsCreator);
	}

	void fiSaveGame::EndGetCreator(int signInId)
	{
		sgoDebug1("Trace: EndGetCreator");
		s_UserProfiles[signInId].EndGetCreator();
	}

	bool fiSaveGame::BeginFreeSpaceCheck(int signInId,const char *filename,u32 saveSize)
	{
		sgoDebug1("Trace: BeginFreeSpaceCheck");
		return s_UserProfiles[signInId].BeginFreeSpaceCheck(filename,saveSize);
	}

	bool fiSaveGame::CheckFreeSpaceCheck(int signInId, int &ExtraSpaceRequired)
	{
		sgoDebug1("Trace: CheckFreeSpaceCheck");
		return s_UserProfiles[signInId].CheckFreeSpaceCheck(ExtraSpaceRequired);
	}

	void fiSaveGame::EndFreeSpaceCheck(int signInId)
	{
		sgoDebug1("Trace: EndFreeSpaceCheck");
		s_UserProfiles[signInId].EndFreeSpaceCheck();
	}

	bool fiSaveGame::BeginExtraSpaceCheck(int /*signInId*/, const char * /*filename*/, u32 /*saveSize*/)
	{
		sgoDebug1("Trace: BeginExtraSpaceCheck");
		return true;
	}

	bool fiSaveGame::CheckExtraSpaceCheck(int /*signInId*/, int & /*ExtraSpaceRequired*/, int & /*TotalHDDFreeSize*/, int & /*SizeOfSystemFileKB*/)
	{
		sgoDebug1("Trace: CheckExtraSpaceCheck");
		return true;
	}

	void fiSaveGame::EndExtraSpaceCheck(int /*signInId*/)
	{
		sgoDebug1("Trace: EndExtraSpaceCheck");
	}

	bool fiSaveGame::BeginReadNamesAndSizesOfAllSaveGames(int /*signInId*/, Content * /*outContent*/, int * /*outFileSizes*/, int /*maxCount*/)
	{
		sgoDebug1("Trace: BeginReadNamesAndSizesOfAllSaveGames");
		return true;
	}

	bool fiSaveGame::CheckReadNamesAndSizesOfAllSaveGames(int /*signInId*/, int & /*outCount*/)
	{
		sgoDebug1("Trace: CheckReadNamesAndSizesOfAllSaveGames");
		return true;
	}

	void fiSaveGame::EndReadNamesAndSizesOfAllSaveGames(int /*signInId*/)
	{
		sgoDebug1("Trace: EndReadNamesAndSizesOfAllSaveGames");
	}

	u64 fiSaveGame::GetTotalAvailableSpace(int signInId)
	{
		sgoDebug1("Trace: GetTotalAvailableSpace");
		return s_UserProfiles[signInId].GetTotalAvailableSpace();
	}

	bool fiSaveGame::BeginGetFileSize(int signInId, const char *filename, bool bCalculateSizeOnDisk)
	{
		sgoDebug1("Trace: BeginGetFileSize");
		return s_UserProfiles[signInId].BeginGetFileSize(filename, bCalculateSizeOnDisk);
	}

	bool fiSaveGame::CheckGetFileSize(int signInId, u64 &fileSize)
	{
		sgoDebug1("Trace: CheckGetFileSize");
		return s_UserProfiles[signInId].CheckGetFileSize(fileSize);
	}

	void fiSaveGame::EndGetFileSize(int signInId)
	{
		sgoDebug1("Trace: EndGetFileSize");
		s_UserProfiles[signInId].EndGetFileSize();
	}

	fiSaveGame::Errors fiSaveGame::GetError(int /*signInId*/)
	{
		sgoDebug1("Trace: GetError - always returns SAVE_GAME_SUCCESS");
		return fiSaveGame::SAVE_GAME_SUCCESS;
	}

	void fiSaveGame::SetStateToIdle(int signInId)
	{
		sgoDebug1("Trace: SetStateToIdle");
		s_UserProfiles[signInId].SetStateToIdle();
	}

	void fiSaveGame::SetSelectedDeviceToAny(int /*signInId*/)
	{
		sgoDebug1("Trace: SetSelectedDeviceToAny");
	}

	bool fiSaveGame::IsCurrentDeviceValid(int /*signInId*/)
	{
		sgoDebug1("Trace: IsCurrentDeviceValid");
		return true;
	}

	void fiSaveGame::SetSaveGameTitle(const char *pSaveGameTitle)
	{
		safecpy(s_TitleString, pSaveGameTitle, sizeof(s_TitleString));
	}

	bool fiSaveGame::HasFileBeenAccessed(int , const char* )
	{
		sgoDebug1("Trace: HasFileBeenAccessed");
		return false;
	}

	bool fiSaveGame::BeginGetFileModifiedTime(int signInId, const char* filename)
	{
		sgoDebug1("Trace: BeginGetFileModifiedTime");
		return s_UserProfiles[signInId].BeginGetFileModifiedTime(filename);
	}

	bool fiSaveGame::CheckGetFileModifiedTime(int signInId, u32& modTimeHigh, u32& modTimeLow)
	{
		sgoDebug1("Trace: CheckGetFileModifiedTime");
		return s_UserProfiles[signInId].CheckGetFileModifiedTime(modTimeHigh, modTimeLow);
	}

	void fiSaveGame::EndGetFileModifiedTime(int signInId)
	{
		sgoDebug1("Trace: EndGetFileModifiedTime");
		s_UserProfiles[signInId].EndGetFileModifiedTime();
	}

	bool fiSaveGame::BeginGetFileExists(int signInId, const char* filename)
	{
		sgoDebug1("Trace: BeginGetFileExists");
		return s_UserProfiles[signInId].BeginGetFileExists(filename);
	}

	bool fiSaveGame::CheckGetFileExists(int signInId, bool& exists)
	{
		sgoDebug1("Trace: CheckGetFileExists");
		return s_UserProfiles[signInId].CheckGetFileExists(exists);
	}

	void fiSaveGame::EndGetFileExists(int signInId)
	{
		sgoDebug1("Trace: EndGetFileExists");
		s_UserProfiles[signInId].EndGetFileExists();
	}

	void fiSaveGame::SetUserServiceId(int signInId, int userId)
	{
		sgoDebug1("Trace: SetUserServiceId");
		s_UserProfiles[signInId].SetUserId(userId);
	}

	void fiSaveGame::SetMountIndex(int /*signInId*/, int /*index*/)
	{
		sgoDebug1("Trace: SetMountIndex - does nothing");
	}

	void fiSaveGame::SetSelectedDirectory(int signInId, const char* directory)
	{
		sgoDebug1("Trace: SetSelectedDirectory");
		s_UserProfiles[signInId].SetSelectedDirectory(directory);
	}

	bool fiSaveGame::BeginDeleteMount(int signInId, const char *pName, bool bIsFilename)
	{
		sgoDebug1("Trace: BeginDeleteMount");
		return s_UserProfiles[signInId].BeginDeleteMount(pName, bIsFilename);
	}

	bool fiSaveGame::CheckDeleteMount(int signInId)
	{
		sgoDebug1("Trace: CheckDeleteMount");
		return s_UserProfiles[signInId].CheckDeleteMount();
	}

	void fiSaveGame::EndDeleteMount(int signInId)
	{
		sgoDebug1("Trace: EndDeleteMount");
		s_UserProfiles[signInId].EndDeleteMount();
	}
#if USE_SAVE_DATA_MEMORY
	void fiSaveGame::SetSaveDataMemory(int signInId)
	{
		s_UserProfiles[signInId].InitialiseSaveDataMemory();
	}
#endif
	// TODO: JRM
	bool fiSaveGame::BeginContentVerify(int,u32,u32,const char*)
	{
		return true;
	}

	bool fiSaveGame::CheckContentVerify(int,bool &verified)
	{
		verified = true;
		return true;
	}

	void fiSaveGame::EndContentVerify(int)
	{
	}

} // namespace rage

#endif // RSG_ORBIS
