// 
// file/file_channel.h 
// 
// Copyright (C) 1999-2018 Rockstar Games.  All Rights Reserved. 
// 

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __file_channel
#define __file_channel file

#ifndef FILE_CHANNEL_H 
#define FILE_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(file)

#define fileAssert(cond)						RAGE_ASSERT(__file_channel,cond)
#define fileAssertf(cond,fmt,...)				RAGE_ASSERTF(__file_channel,cond,fmt,##__VA_ARGS__)
#define fileFatalAssertf(cond,fmt,...)			RAGE_FATALASSERTF(__file_channel,cond,fmt,##__VA_ARGS__)
#define fileVerify(cond)						RAGE_VERIFY(__file_channel,cond)
#define fileVerifyf(cond,fmt,...)				RAGE_VERIFYF(__file_channel,cond,fmt,##__VA_ARGS__)
#define fileFatalf(fmt,...)						RAGE_FATALF(__file_channel,fmt,##__VA_ARGS__)
#define fileErrorf(fmt,...)						RAGE_ERRORF(__file_channel,fmt,##__VA_ARGS__)
#define fileWarningf(fmt,...)					RAGE_WARNINGF(__file_channel,fmt,##__VA_ARGS__)
#define fileDisplayf(fmt,...)					RAGE_DISPLAYF(__file_channel,fmt,##__VA_ARGS__)
#define fileDebugf1(fmt,...)					RAGE_DEBUGF1(__file_channel,fmt,##__VA_ARGS__)
#define fileDebugf2(fmt,...)					RAGE_DEBUGF2(__file_channel,fmt,##__VA_ARGS__)
#define fileDebugf3(fmt,...)					RAGE_DEBUGF3(__file_channel,fmt,##__VA_ARGS__)
#define fileLogf(severity,fmt,...)				RAGE_LOGF(__file_channel,severity,fmt,##__VA_ARGS__)
#define fileCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,__file_channel,severity,fmt,##__VA_ARGS__)

#endif // FILE_CHANNEL_H 