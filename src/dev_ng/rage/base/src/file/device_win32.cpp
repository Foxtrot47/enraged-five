// 
// file/device_win32.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if __WIN32

#include "device.h"
#include "limits.h"

#include "system/bootmgr.h"
#include "system/xtl.h"
#include "system/nelem.h"
#include "string/string.h"
#include "diag/diagerrorcodes.h"

using namespace rage;

#if __XENON
static enum {
	LSN_RANGE_UNKNOWN,
	LSN_RANGE_NORMAL,
	LSN_RANGE_OVERSIZED,

} s_LSNRange = LSN_RANGE_UNKNOWN;
#endif // __XENON

const char *fiDeviceLocal::FixName(char *dest,int destSize,const char *src) {
#if __XENON
	if (!strnicmp(src,"a:",2))
	{
		strcpy(dest, "game:");
		strcpy(&dest[5], &src[2]);
		src = dest;
	}
#elif RSG_DURANGO
	if (!strnicmp(src,"xg:",3))
	{
		src++;	// skip the 'x'
	}
	else if (!strnicmp(src,"xd:",3))
	{
		src++;	// skip the 'x'
	}
#endif

	const char *origDest = dest;
	while (--destSize && *src) {
		char ch = *src++;
		if (ch == '/')
			ch = '\\';
		*dest++ = ch;
	}
	*dest = 0;
	return origDest;
}

namespace rage {
bool g_fiCaseSensitive = false;
}

static const wchar_t *FixNameW(wchar_t *dest,int destSize,const char *src) {
	char namebuf[RAGE_MAX_PATH];
	fiDeviceLocal::FixName(namebuf,RAGE_MAX_PATH,src);	
	MultiByteToWideChar(CP_UTF8 , NULL , namebuf, -1, dest, destSize);
	return dest;
}

static bool VerifyCaseW(const wchar_t * WIN32PC_ONLY(exactName)) {
	if (!g_fiCaseSensitive)
		return true;

#if __WIN32PC
	WIN32_FIND_DATAW data;
	HANDLE newHandle = ::FindFirstFileW(exactName,&data);
	if (newHandle != INVALID_HANDLE_VALUE) {
		const wchar_t *slash = wcsrchr(exactName,L'\\');
		if (!slash)
			slash = exactName;
		else
			++slash;
		::FindClose(newHandle);
		if (wcscmp(data.cFileName,slash)) {
			Errorf("VerifyCaseW - Looking for '%s', actually found '%s'",slash,data.cFileName);
			MessageBoxW(NULL,L"Case-sensitive file handling is enabled, and the named file doesn't exactly match even though it exists.",
				exactName,MB_OK | MB_TOPMOST | MB_ICONERROR);
			return false;
		}
	}
	else
		Warningf("VerifyCaseW - couldn't find '%s' again",exactName);
#endif
	return true;
}

fiHandle fiDeviceLocal::Open(const char *filename,bool readOnly) const {
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);

#if RSG_WINRT
	CREATEFILE2_EXTENDED_PARAMETERS createExParams;
	memset(&createExParams, 0, sizeof(createExParams));
	createExParams.dwSize = sizeof(CREATEFILE2_EXTENDED_PARAMETERS);
	createExParams.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;

	HANDLE h = readOnly?
		CreateFile2(fixedNameW, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, OPEN_EXISTING, &createExParams) :
		CreateFile2(fixedNameW, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, OPEN_EXISTING, &createExParams);
#else
	HANDLE h = readOnly?
		CreateFileW(fixedNameW, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL) :
		CreateFileW(fixedNameW, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
#endif // RSG_WINRT

	if (h != INVALID_HANDLE_VALUE && !VerifyCaseW(fixedNameW)) {
		CloseHandle(h);
		h = INVALID_HANDLE_VALUE;
	}

#if __DEVPROF
	if (h != INVALID_HANDLE_VALUE)
		sm_DeviceProfiler->OnOpen(h, filename);
#endif // __DEVPROF

	return h;
}

#define ENABLE_NO_BUFFERING 0

fiHandle fiDeviceLocal::OpenBulk(const char *filename,u64 &outBias) const {
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);
	outBias = 0;

#if RSG_WINRT
	CREATEFILE2_EXTENDED_PARAMETERS createExParams;
	memset(&createExParams, 0, sizeof(createExParams));
	createExParams.dwSize = sizeof(CREATEFILE2_EXTENDED_PARAMETERS);
	createExParams.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;
#if ENABLE_NO_BUFFERING
	createExParams.dwFileFlags = FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING;
#endif
	HANDLE h = CreateFile2(fixedNameW, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, &createExParams);
#else
	HANDLE h = CreateFileW(fixedNameW, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL 
#if ENABLE_NO_BUFFERING
		| FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING
#endif
		, NULL);
#endif // RSG_WINRT
	
	if (h != INVALID_HANDLE_VALUE && !VerifyCaseW(fixedNameW)) {
		CloseHandle(h);
		h = INVALID_HANDLE_VALUE;
	}

#if __DEVPROF
	if (h != INVALID_HANDLE_VALUE)
		sm_DeviceProfiler->OnOpen(h, filename);
#endif // __DEVPROF

	return h;
}


fiHandle fiDeviceLocal::CreateBulk(const char *filename) const {
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);
#if RSG_WINRT
	CREATEFILE2_EXTENDED_PARAMETERS createExParams;
	memset(&createExParams, 0, sizeof(createExParams));
	createExParams.dwSize = sizeof(CREATEFILE2_EXTENDED_PARAMETERS);
	createExParams.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;

	fiHandle result = CreateFile2(fixedNameW, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, CREATE_ALWAYS, &createExParams);
#else
	fiHandle result = CreateFileW(fixedNameW, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL /* | FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING*/, NULL);
#endif // RSG_WINRT

#if __DEVPROF
	if (result != INVALID_HANDLE_VALUE)
		sm_DeviceProfiler->OnOpen(result, filename);
#endif // __DEVPROF

	return result;
}


fiHandle fiDeviceLocal::Create(const char *filename) const 
{
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);

#if RSG_WINRT
	CREATEFILE2_EXTENDED_PARAMETERS createExParams;
	memset(&createExParams, 0, sizeof(createExParams));
	createExParams.dwSize = sizeof(CREATEFILE2_EXTENDED_PARAMETERS);
	createExParams.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;

	HANDLE h = CreateFile2(fixedNameW, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, &createExParams);
#else
	HANDLE h = CreateFileW(fixedNameW, GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
#endif // RSG_WINRT

#if __WIN32PC
	// Did it work?
	if(h == INVALID_HANDLE_VALUE)
	{
		// An error occured creating the file
#if !__NO_OUTPUT
		char msgBuff[256];
		int iErrorCode = GetLastError();
		Warningf("Failed to create file '%s' error code %d : %s",filename,iErrorCode,
			diagErrorCodes::Win32ErrorToString(iErrorCode, msgBuff, NELEM(msgBuff)));
#endif
	}
#endif

#if __DEVPROF
	if (h != INVALID_HANDLE_VALUE)
		sm_DeviceProfiler->OnOpen(h, filename);
#endif // __DEVPROF

	return h;
}

int fiDeviceLocal::Seek(fiHandle fd,int offset,fiSeekWhence whence) const {
	return (int)Seek64(fd,offset,whence);
}

u64 fiDeviceLocal::Seek64(fiHandle fd,s64 offset,fiSeekWhence whence) const {
	LARGE_INTEGER in, out;
	in.QuadPart = offset;

#if __DEVPROF
	sm_DeviceProfiler->OnSeek(fd, offset);
#endif // __DEVPROF

if (SetFilePointerEx(fd,in,&out,whence))
		return out.QuadPart;
	else
		return (u64)(s64)-1;
;
}

int fiDeviceLocal::Write(fiHandle fd,const void *buffer,int count) const {
	DWORD numRead;

#if __DEVPROF
	sm_DeviceProfiler->OnWrite(fd, count);
#endif // __DEVPROF

	return WriteFile(fd, buffer, count, &numRead, NULL)? numRead : -1;
}

void (*fiDevice::sm_FatalReadError)(void);
#if !__FINAL
u32 fiDevice::sm_InjectReadError;
#endif


int fiDeviceLocal::Read(fiHandle fd,void *buffer,int count) const {
	DWORD numRead;
	AssertMsg((buffer != NULL), "File Read Buffer is NULL");
#if __DEVPROF
	sm_DeviceProfiler->OnRead(fd, count, 0);
#endif // __DEVPROF

	while (!ReadFile(fd, buffer, count, &numRead, NULL))
		Errorf("ReadFile returned %d, retrying",GetLastError());

#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(fd);
#endif // __DEVPROF

	return numRead;
}

#if ENABLE_NO_BUFFERING
__THREAD HANDLE hEvent;
#endif

int fiDeviceLocal::ReadBulk(fiHandle fd,u64 offset,void *buffer,int count) const {
#if ENABLE_NO_BUFFERING
	if (!hEvent)
		hEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
#endif

	OVERLAPPED overlapped = {0};
	overlapped.Internal = 0;
	overlapped.InternalHigh = 0;
	overlapped.Offset = (u32) offset;
	overlapped.OffsetHigh = (u32) (offset >> 32);
#if ENABLE_NO_BUFFERING
	overlapped.hEvent = hEvent;
#else
	overlapped.hEvent = NULL;
#endif

#if __DEVPROF
	sm_DeviceProfiler->OnRead(fd, count, offset);
#endif // __DEVPROF

	int amount = 0;
	while (count)
	{
		DWORD result = 0;
		if (!::ReadFile(fd, buffer, count, &result, &overlapped )) {
#if ENABLE_NO_BUFFERING
			if (GetLastError() == ERROR_IO_PENDING) {
				::WaitForSingleObject(overlapped.hEvent,INFINITE);
				result = overlapped.InternalHigh;
				break;
			}
#endif
			Errorf("ReadFile(ReadBulk) returned %d, result is %u",GetLastError(),result);
			break;
		}

		if (result)
		{
			offset += result;
			amount += result;
			count -= result;
			buffer = (void*)((char*)buffer + result);
		}
		else
		{
			break;
		}
	}
	Assert(count == 0);

#if __DEVPROF
	sm_DeviceProfiler->OnReadEnd(fd);
#endif // __DEVPROF

	return amount;
}

int fiDeviceLocal::WriteBulk(fiHandle fd,u64 offset,const void *buffer,int count) const {
	OVERLAPPED overlapped = {0};
	overlapped.Internal = 0;
	overlapped.InternalHigh = 0;
	overlapped.Offset = (u32) offset;
	overlapped.OffsetHigh = (u32) (offset >> 32);
	overlapped.hEvent = NULL;

#if __DEVPROF
	sm_DeviceProfiler->OnWrite(fd, count);
#endif // __DEVPROF

	// int result = -1;
	DWORD result = 0;
	if (!::WriteFile(fd, buffer, count, &result, &overlapped )) {
		Errorf("WriteFile(WriteBulk) returned %d",GetLastError());
		return -1;
	}

	return result;
}

int fiDeviceLocal::Close(fiHandle fd) const {

#if __DEVPROF
	sm_DeviceProfiler->OnClose(fd);
#endif // __DEVPROF

	return CloseHandle(fd)? 0 : -1;
}

int fiDeviceLocal::CloseBulk(fiHandle fd) const {

#if __DEVPROF
	sm_DeviceProfiler->OnClose(fd);
#endif // __DEVPROF

	return CloseHandle(fd)? 0 : -1;
}

int fiDeviceLocal::Size(fiHandle handle) const
{
	return (int)Size64(handle);
}

u64 fiDeviceLocal::Size64(fiHandle handle) const
{
	LARGE_INTEGER large;
	::GetFileSizeEx(handle, &large);
	return large.QuadPart;
}

u64 fiDeviceLocal::GetFileSize(const char *filename) const {
	WIN32_FILE_ATTRIBUTE_DATA fd;
	wchar_t namebuf[RAGE_MAX_PATH];
	if (!GetFileAttributesExW(FixNameW(namebuf,RAGE_MAX_PATH,filename),GetFileExInfoStandard,&fd))
		return 0;
	return ((u64)fd.nFileSizeHigh << 32) | fd.nFileSizeLow;
}

u64 fiDeviceLocal::GetFileTime(const char *filename) const {
	WIN32_FILE_ATTRIBUTE_DATA fd;
	wchar_t namebuf[RAGE_MAX_PATH];
	if (!GetFileAttributesExW(FixNameW(namebuf,RAGE_MAX_PATH,filename),GetFileExInfoStandard,&fd))
		return 0;
	return ((u64)fd.ftLastWriteTime.dwHighDateTime << 32) | fd.ftLastWriteTime.dwLowDateTime;
}

bool fiDeviceLocal::SetFileTime(const char *filename,u64 timestamp) const {	
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);
#if RSG_WINRT
	CREATEFILE2_EXTENDED_PARAMETERS createExParams;
	memset(&createExParams, 0, sizeof(createExParams));
	createExParams.dwSize = sizeof(CREATEFILE2_EXTENDED_PARAMETERS);
	createExParams.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;

	HANDLE h = CreateFile2(fixedNameW, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, OPEN_EXISTING, &createExParams);
#else
	HANDLE h = CreateFileW(fixedNameW, GENERIC_READ | GENERIC_WRITE, 
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, NULL);
#endif // RSG_WINRT

	if (h == INVALID_HANDLE_VALUE)
		return 0;
	FILETIME ft;
	ft.dwHighDateTime = u32(timestamp >> 32);
	ft.dwLowDateTime = (u32)timestamp;
	bool result = ::SetFileTime(h,NULL,NULL,&ft) != 0;
	CloseHandle(h);
	return result;
}


u32 fiDeviceLocal::GetAttributes(const char *filename) const {
	wchar_t namebuf[RAGE_MAX_PATH];
	u32 attr = ::GetFileAttributesW(FixNameW(namebuf,RAGE_MAX_PATH,filename));
	if (attr != ~0U && !VerifyCaseW(namebuf))
		attr = ~0U;
	return attr;
}

bool fiDeviceLocal::SetAttributes(const char *filename,u32 attr) const {
	wchar_t namebuf[RAGE_MAX_PATH];
	return ::SetFileAttributesW(FixNameW(namebuf,RAGE_MAX_PATH,filename),attr) != FALSE;
}

static void TranslateW(fiFindData &dest,const WIN32_FIND_DATAW &src) {
	char namebuf[RAGE_MAX_PATH];
	WideCharToMultiByte(CP_UTF8, 0, src.cFileName, -1, namebuf, RAGE_MAX_PATH, 0, 0);
	fiDeviceLocal::FixName(dest.m_Name,sizeof(dest.m_Name),namebuf);
	dest.m_LastWriteTime = ((u64)src.ftLastWriteTime.dwHighDateTime<<32) | src.ftLastWriteTime.dwLowDateTime;
	dest.m_Attributes = src.dwFileAttributes;
	dest.m_Size = src.nFileSizeLow | (((u64)src.nFileSizeHigh)<<32);
}

fiHandle fiDeviceLocal::FindFileBegin(const char *filename,fiFindData &outData) const {
	WIN32_FIND_DATAW data;
	wchar_t star[RAGE_MAX_PATH];
	wchar_t namebuf[RAGE_MAX_PATH];
	int sl = (int) strlen(filename);
	bool endsInSlash = sl && (filename[sl-1]=='/' || filename[sl-1]=='\\');
	formatf(star,RAGE_MAX_PATH,endsInSlash?L"%s*":L"%s\\*",FixNameW(namebuf,RAGE_MAX_PATH,filename)); //takes string length not size!
	HANDLE handle = ::FindFirstFileW(star,&data);
	if (handle != INVALID_HANDLE_VALUE) {
		TranslateW(outData,data);
		return handle;
	}
	else
		return fiHandleInvalid;
}

bool fiDeviceLocal::FindFileNext(fiHandle handle,fiFindData &outData) const {
	WIN32_FIND_DATAW data;
	bool result = ::FindNextFileW(handle,&data) != 0;
	if (result)
		TranslateW(outData,data);
	return result;
}

int fiDeviceLocal::FindFileEnd(fiHandle handle) const {
	return ::FindClose(handle) ? 0 : -1;
}

bool fiDeviceLocal::SetEndOfFile(fiHandle handle) const {
	return ::SetEndOfFile(handle) ? true : false;
}

bool fiDeviceLocal::Delete(const char *filename) const {
	wchar_t namebuf[RAGE_MAX_PATH];
	return DeleteFileW(FixNameW(namebuf,RAGE_MAX_PATH,filename)) != FALSE;
}

bool fiDeviceLocal::Rename(const char *from,const char *to) const {
#if !RSG_DURANGO
	wchar_t namebuf[RAGE_MAX_PATH];
	wchar_t namebuf2[RAGE_MAX_PATH];
	return MoveFileExW(FixNameW(namebuf,RAGE_MAX_PATH,from),FixNameW(namebuf2,RAGE_MAX_PATH,to),MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING) != FALSE;
#else //!RSG_DURANGO
	/*
	char namebuf[RAGE_MAX_PATH];
	char namebuf2[RAGE_MAX_PATH];
	return MoveFileWithProgress(FixName(namebuf,sizeof(namebuf),from),FixName(namebuf2,sizeof(namebuf2),to),MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING) != FALSE;
	*/
	(void)from; (void)to;
	return false;
#endif //!RSG_DURANGO
}

bool fiDeviceLocal::MakeDirectory(const char *filename) const 
{
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);

	bool bSuccess = (CreateDirectoryW(fixedNameW,NULL) != FALSE);

#if __WIN32PC
	// Did it work?
	if(!bSuccess && GetLastError() != ERROR_ALREADY_EXISTS)
	{
		// An error occured creating the directory
#if !__NO_OUTPUT
		char msgBuff[256];
		int iErrorCode = GetLastError();
		Warningf("Failed to create folder '%s' error code %d : %s",filename,iErrorCode,
			diagErrorCodes::Win32ErrorToString(iErrorCode, msgBuff, NELEM(msgBuff)));
#endif
	}
#endif

	return bSuccess;
}

bool fiDeviceLocal::UnmakeDirectory(const char *filename) const 
{
	wchar_t fixedNameW[RAGE_MAX_PATH];
	FixNameW(fixedNameW,RAGE_MAX_PATH,filename);

	bool bSuccess = (RemoveDirectoryW(fixedNameW) != FALSE);

#if __WIN32PC
	// Did it work?
	if(!bSuccess)
	{
		// An error occured creating the directory
#if !__NO_OUTPUT
		char msgBuff[256];
		int iErrorCode = GetLastError();
		Warningf("Failed to remove folder '%s' error code %d : %s",filename,iErrorCode,
			diagErrorCodes::Win32ErrorToString(iErrorCode, msgBuff, NELEM(msgBuff)));
#endif
	}
#endif

	return bSuccess;
}

void fiDeviceLocal::OnDiscSwap()
{
#if __XENON
	s_LSNRange = LSN_RANGE_UNKNOWN;
#endif // __XENON
}

fiDevice::RootDeviceId fiDeviceLocal::GetRootDeviceId(const char *XENON_ONLY(name)) const
{
#if __XENON
	// Ideally we never get here - the child device should handle it.
	char namebuf[RAGE_MAX_PATH];
	FixName(namebuf,sizeof(namebuf),name);

	if (strnicmp(namebuf,"a:",2) && strnicmp(namebuf,"game:",5))
	{
		return HARDDRIVE;
	}

	// Check for PFHD.
	XTITLE_DEPLOYMENT_TYPE deploymentType;
	if (XTitleGetDeploymentType(&deploymentType, NULL) == ERROR_SUCCESS)
	{
		if (deploymentType == XTITLE_DEPLOYMENT_INSTALLED_TO_HDD)
		{
			return HARDDRIVE;
		}
	}

	XCONTENTDEVICETYPE deviceType;
	if (XContentQueryVolumeDeviceType("GAME", &deviceType, NULL) == ERROR_SUCCESS)
	{
		if (deviceType == XCONTENTDEVICETYPE_HDD)
		{
			return HARDDRIVE;
		}
	}

	return OPTICAL;
#else // __XENON
	// This is not technically true, but it's not trivial to detect, so let's do that later.
	return HARDDRIVE;
#endif // __XENON
}

u32 fiDeviceLocal::GetPhysicalSortKey(const char* XENON_ONLY(filename)) const
{
#if __XENON
	char namebuf[RAGE_MAX_PATH];

	// If the game is installed to HDD, nothing gets a sortkey, ever.
	if (GetRootDeviceId("A:") == HARDDRIVE)
		return HARDDRIVE_LSN;

	HANDLE h = CreateFile(FixName(namebuf,sizeof(namebuf),filename), GENERIC_READ, 
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (h == INVALID_HANDLE_VALUE)
		return 0;
	u32 key = XGetFilePhysicalSortKey(h);
	if (key == ~0U)
		key = HARDDRIVE_LSN;
	else
	{
		if (s_LSNRange == LSN_RANGE_UNKNOWN) {
			s_LSNRange = (key >= HARDDRIVE_LSN) ? LSN_RANGE_OVERSIZED : LSN_RANGE_NORMAL;

			if (s_LSNRange == LSN_RANGE_OVERSIZED) {
				Warningf("You are using an oversized disc image.");
			}
		}

		if (s_LSNRange == LSN_RANGE_OVERSIZED) {
			key -= 0x60000000;
		}

		if (key >= HARDDRIVE_LSN) {
			Errorf("Invalid LSN: %x, LSN range %d", key, s_LSNRange);
		}
	}

	CloseHandle(h);
	return key;
#else
	return HARDDRIVE_LSN;
#endif
}

void fiDevice::ConvertFileTimeToSystemTime(u64 inTime,SystemTime &outTime) {
	CompileTimeAssert(sizeof(SystemTime) == sizeof(SYSTEMTIME)); // make sure the cast below doesn't do anything bad
	if (inTime) {
		FILETIME LocalFileTime;
		FileTimeToLocalFileTime( (FILETIME*)&inTime, &LocalFileTime );
		FileTimeToSystemTime( &LocalFileTime, (LPSYSTEMTIME)&outTime );
	}
	else {
		outTime.wYear = 1980;
		outTime.wMonth = 1;
		outTime.wDayOfWeek = 0;
		outTime.wDay = 1;
		outTime.wHour = 0;
		outTime.wMinute = 0;
		outTime.wSecond = 0;
		outTime.wMilliseconds = 0;
	}
}

void fiDevice::GetLocalSystemTime(SystemTime &outTime)
{
	GetLocalTime((LPSYSTEMTIME)&outTime);
}

void fiDevice::GetSystemTimeUtc(SystemTime & outTime)
{
	GetSystemTime((LPSYSTEMTIME)&outTime);
}

#endif
