// 
// file/compress.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_COMPRESS_H
#define FILE_COMPRESS_H

namespace rage {

/*	PURPOSE	Compress data using a sliding window algorithm
	PARAMS	dest - Destination buffer
			destSize - Size of destination buffer; should be slightly larger than input for worse-case.  Adding 1k should be sufficient.
			src - Source buffer (data to compress)
			srcSize - Amount of data to compress
			lookback - Maximum lookback (log2).  15 or 16 is pretty fast and still yields very good results.
			fullUpdate - If true, does more exhaustive table update which is substantially more expensive but
				improves compression by about 10% or so.  Varies by data.
	RETURNS	Number of bytes written to destination buffer.  If this is greater than or equal to srcSize,
			calling code should assume the compression failed (common with very small datasets) */
u32 fiCompress(u8 *dest,u32 destSize,const u8 *src,u32 srcSize,u32 lookback,bool fullUpdate);

/*	PURPOSE	Compute a reasonable upper bound on the compression space needed
	PARAMS	srcSize - Size of source buffer
	RETURNS	Estimated upper bound (for use in fiCompress destSize) */
u32 fiCompressUpperBound(u32 srcSize);

}	// namespace rage

#endif
