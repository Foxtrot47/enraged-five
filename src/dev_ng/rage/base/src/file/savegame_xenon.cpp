// 
// file/savegame_xenon.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if __XENON
#include <xdk.h>
#endif

#include "savegame.h"
#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "diag/tracker.h"

#if __XENON
#include "system/xtl.h"

#include <xbox.h>
#define XCONTENT_MAX_NAME_LENGTH XCONTENT_MAX_DISPLAYNAME_LENGTH

namespace rage {

CompileTimeAssert(sizeof(XCONTENT_DATA) == sizeof(fiSaveGame::Content));
CompileTimeAssert(sizeof(XDEVICE_DATA) == sizeof(fiSaveGame::Device));

fiSaveGame SAVEGAME;

using namespace fiSaveGameState;

//define Xenon specific error codes
#define USER_CANCELLED_DEVICE_SELECTION_CODE 0x000004c7
#define NO_FILES_EXIST_CODE 0x80070012
#define CORRUPT_FILE_CODE 0x80070570
#define COULD_NOT_MAP_DEVICE_CODE 0x8007048f
#define THE_DEVICE_IS_NOT_READY_CODE 0x80070015
#define MAX_DEVICE_NAME_LENGTH	27

#pragma warning(push)
#pragma warning(disable: 4702)

#if HACK_GTA4
//	Changelist 130919 by Graeme - Added ExtraSpaceCheck. 
//	It's almost identical to FreeSpaceCheck but gives a bit more information that I require when checking 
//	if there is enough space for 13 save games and a profile file at the start of a PS3 game.
enum eTypeOfSpaceCheck
{
	FREE_SPACE_CHECK,
	EXTRA_SPACE_CHECK
};
#endif	//	HACK_GTA4

class XenonProfile {
public:
	XenonProfile();
   ~XenonProfile();

	bool BeginSelectDevice(u32 contentType,u32 minSpaceAvailable,bool forceShow,bool manageStorage);
	bool CheckSelectDevice();
	u32 GetSelectedDevice() const { return m_DeviceID; }
	void SetSelectedDevice(u32 deviceId) { m_DeviceID = deviceId; }
   void SetSelectedDeviceToAny() {m_DeviceID = XCONTENTDEVICE_ANY;} 
	void EndSelectDevice();

	bool BeginEnumeration(u32 contentType,fiSaveGame::Content *outContent,int maxCount);
	bool CheckEnumeration(int &outCount);
	void EndEnumeration();

	bool BeginSave(u32 contentType,const char16 *contentName,const char *filename,const void *saveData,u32 saveSize,bool overwrite);
	bool CheckSave(bool &outIsValid,bool &fileExists);
	u32 GetSaveSize() const { return m_State==SAVED_CONTENT? m_SaveDataSize : 0; }
	void EndSave();

	bool BeginLoad(u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize);
	bool CheckLoad(bool &outIsValid,u32 &loadSize);
	u32 GetLoadSize() const { return m_State==LOADED_CONTENT? m_LoadDataSize : 0; }
	void EndLoad();

	bool BeginIconSave(u32 contentType,const char *filename,const void *iconData,u32 iconSize);
	bool CheckIconSave();
	void EndIconSave();

	bool BeginContentVerify(u32 contentType,u32 deviceId,const char *filename);
	bool CheckContentVerify(bool &verified);
	void EndContentVerify();

	bool BeginGetFileSize(const char *filename, bool bCalculateSizeOnDisk);
	bool CheckGetFileSize(u64 &fileSize);
	void EndGetFileSize();

#if HACK_GTA4
	bool BeginFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType, const char *filename, u32 saveSize);
	bool CheckFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType, int &ExtraSpaceRequired, int &TotalHDDFreeSizeKB);
	void EndFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType);
#else	//	HACK_GTA4
	bool BeginFreeSpaceCheck(const char *filename,u32 saveSize);
	bool CheckFreeSpaceCheck(int &ExtraSpaceRequired);
	void EndFreeSpaceCheck();
#endif	//	HACK_GTA4

	bool BeginDelete(u32 contentType,const char *filename);
	bool CheckDelete();
	void EndDelete();

	bool BeginGetCreator(u32 contentType,u32 deviceId,const char *filename);
	bool CheckGetCreator(bool &bIsCreator);
	void EndGetCreator();

   u64 GetTotalAvailableSpace();
   bool GetDeviceData(u32 deviceId, fiSaveGame::Device *outDevice);
   bool GetCurrentDeviceData(fiSaveGame::Device *outDevice);
   u64 CalculateDataSizeOnDisk(u32 dataSize);
   bool IsDeviceValid(u32 deviceId);
   bool IsCurrentDeviceValid();
   bool IsStorageDeviceChanged();
   bool IsStorageDeviceRemoved();
	void FillDevice();

	fiSaveGame::Errors GetError();
	void SetStateToIdle(){m_State = IDLE;}
	bool HasFileBeenAccessed(const char* filename);
	fiSaveGameState::State GetState() const { return m_State; }

	bool CompareTimestamps(HANDLE hFile);
private:
	DWORD GetResult(DWORD &cbBuffer);
	int GetSelfIndex() const;
	int GetUserIndex() const;
	int GetSelectUserIndex() const;
	const char *GetSaveRoot() const;
	void SetTimeStamp();
	void BeginOverlapped()
	{
		ZeroMemory( &m_Overlapped, sizeof( XOVERLAPPED ) );
	}
	void EndOverlapped()
	{
	}
	void BeginOverlapped2()
	{
		ZeroMemory( &m_Overlapped2, sizeof( OVERLAPPED ) );
	}
	void EndOverlapped2()
	{
	}

	State				m_State;
	int					m_SubState;
	XOVERLAPPED			m_Overlapped;                   // Overlapped object for device selector UI
	OVERLAPPED			m_Overlapped2;					// Read/WriteFile need a different version!
	XCONTENTDEVICEID	m_DeviceID;						// Device identifier returned by device selector UI
	XCONTENTDEVICEID	m_DeviceIDBeforeDeviceSelector; // saved value for device id in case we error out when changing device

	HANDLE				m_hEnum;

	char				m_Filename[32];
	HANDLE				m_hFile;

	bool				m_bCalculateSizeOnDisk;

	const void			*m_SaveData;
	u32					m_SaveDataSize;
	bool				m_SaveValid;
	bool				m_Overwrite;
	bool				m_FileExists;

	void				*m_LoadData;
	u32					m_LoadDataSize;

	atMap<atString,FILETIME> m_TimeStamp;	// TODO: Consider atStringMap

	BOOL				m_bUserIsCreator;

	u64					m_SpaceRequiredForNewFile;
	u64					m_SizeOfExistingFile;

   HANDLE         m_hNotification;
};


static atRangeArray<XenonProfile,fiSaveGame::SIGNIN_COUNT> s_Profiles;

XenonProfile::XenonProfile() 
{
   // Register a notification listener so that we can listen to device removal
   m_hNotification = XNotifyCreateListener( XNOTIFY_SYSTEM );
   if( m_hNotification == NULL || m_hNotification == INVALID_HANDLE_VALUE )
   { 
      Displayf("[savegame] Unable to create the listener for device removal.");
   }

   m_State = IDLE;
   m_DeviceID = XCONTENTDEVICE_ANY;
}

XenonProfile::~XenonProfile() 
{
   CloseHandle( m_hNotification );
}

bool XenonProfile::IsStorageDeviceChanged()
{
   // Check for system notifications
   DWORD dwNotificationID=0;
   ULONG_PTR ulParam;
   
   if (XNotifyGetNext( m_hNotification, XN_SYS_STORAGEDEVICESCHANGED, &dwNotificationID, &ulParam )==0)
   {
      return false;
   }
   else
   {
      return true;
   }
}

bool XenonProfile::IsStorageDeviceRemoved()
{
	return (XContentGetDeviceState(m_DeviceID,NULL) == ERROR_DEVICE_NOT_CONNECTED);
}

fiSaveGame::Errors XenonProfile::GetError()
{
   switch(m_Overlapped.dwExtendedError)
   {
      case USER_CANCELLED_DEVICE_SELECTION_CODE:
         return fiSaveGame::USER_CANCELLED_DEVICE_SELECTION;
      break;
      case NO_FILES_EXIST_CODE:
         return fiSaveGame::NO_FILES_EXIST;
      case CORRUPT_FILE_CODE:
         return fiSaveGame::FILE_CORRUPT;
	  case COULD_NOT_MAP_DEVICE_CODE:
		  return fiSaveGame::COULD_NOT_MAP_DEVICE;
	  case THE_DEVICE_IS_NOT_READY_CODE:
		  return fiSaveGame::THE_DEVICE_IS_NOT_READY;		  
      default:
         return fiSaveGame::SAVE_GAME_SUCCESS;
   }
}

int XenonProfile::GetSelfIndex() const
{
	return this - &s_Profiles[0];
}


int XenonProfile::GetUserIndex() const
{
	int selfIndex = GetSelfIndex();
	return selfIndex<XUSER_MAX_COUNT ? selfIndex : XUSER_INDEX_NONE;
}


int XenonProfile::GetSelectUserIndex() const
{
	int selfIndex = GetSelfIndex();
	return selfIndex<XUSER_MAX_COUNT ? selfIndex : XUSER_INDEX_ANY;
}

const char *XenonProfile::GetSaveRoot() const
{
	static const char *devices[] = { "save0", "save1", "save2", "save3", "save4" };
	return devices[GetSelfIndex()];
}

u64 XenonProfile::GetTotalAvailableSpace()
{
   /*if (IsCurrentDeviceValid())
   {
      XDEVICE_DATA deviceData;

      XContentGetDeviceData(GetSelectedDevice(),&deviceData);

      return deviceData.ulDeviceFreeBytes;
   }
   else
   {
      return 0;
   }*/
	fiSaveGame::Device deviceData;

	if (GetDeviceData(GetSelectedDevice(), &deviceData))
	{
		return deviceData.DeviceFreeBytes;
   }
	else
	{
		return 0;
	}

}

bool XenonProfile::GetCurrentDeviceData(fiSaveGame::Device *outDevice)
{
	return GetDeviceData(m_DeviceID, outDevice);
}


bool XenonProfile::GetDeviceData(u32 deviceId, fiSaveGame::Device *outDevice)
{
	if (IsDeviceValid(deviceId))
	{
		XDEVICE_DATA deviceData;
		XContentGetDeviceData(deviceId, &deviceData);
		//Copy it all over
		outDevice->DeviceBytes = deviceData.ulDeviceBytes;
		outDevice->DeviceFreeBytes = deviceData.ulDeviceFreeBytes;
		outDevice->DeviceId = deviceData.DeviceID;
		outDevice->DeviceType = deviceData.DeviceType;
		safecpy(outDevice->FriendlyName, deviceData.wszFriendlyName, MAX_DEVICE_NAME_LENGTH);

		return true;
	}
	else
	{
		return false;
	}
}

u64 XenonProfile::CalculateDataSizeOnDisk(u32 dataSize)
{
   return XContentCalculateSize(dataSize,0);
}

bool XenonProfile::BeginSelectDevice(u32 contentType,u32 minSpaceAvailable,bool forceShow,bool manageStorage)
{
	if (m_State != IDLE && m_State != HAD_ERROR) {
		Warningf("Cannot begin device selection, operation already in progress");
		return false;
	}

	BeginOverlapped();
	m_DeviceIDBeforeDeviceSelector = m_DeviceID;
	m_DeviceID = XCONTENTDEVICE_ANY;

	ULARGE_INTEGER iBytesRequested;
#if _XDK_VER >= 2417
	if (minSpaceAvailable){
		iBytesRequested.QuadPart = XContentCalculateSize(minSpaceAvailable,0);
	}else{
		//don't perform space requirement check
		iBytesRequested.QuadPart = minSpaceAvailable;
	}
#else
	iBytesRequested.QuadPart = minSpaceAvailable;
#endif

	DWORD dwContentFlags = 0;
	if (manageStorage)
	{
		dwContentFlags |= XCONTENTFLAG_MANAGESTORAGE;
	}
	if (forceShow)
	{
#if _XDK_VER < 20500
		dwContentFlags |= XCONTENTFLAG_FORCE_SHOW_UI;
#endif // _XDK_VER < 20500
	}

	DWORD dwRet = XShowDeviceSelectorUI( GetSelectUserIndex(),
		contentType,	// List only devices with specified content type
		dwContentFlags,
		iBytesRequested,         // Size of the device data struct
		&m_DeviceID,            // Return selected device information
		&m_Overlapped );

	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XShowDeviceSelectorUI failed.");
		//restore the previous device id
		m_DeviceID = m_DeviceIDBeforeDeviceSelector;
		return false;
	}
	else {
		m_State = SELECTING_DEVICE;
		return true;
	}
}


bool XenonProfile::CheckSelectDevice()
{
	if (m_State != SELECTING_DEVICE)
		return false;

	DWORD cbBuffer;
	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if (dwRet == ERROR_SUCCESS) {
		m_State = SELECTED_DEVICE;
		EndOverlapped();
		return true;
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during device select.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
		//restore the previous device id
		m_DeviceID = m_DeviceIDBeforeDeviceSelector;
		EndOverlapped();
		return true;
	}
}


void XenonProfile::EndSelectDevice()
{
	if (m_State == SELECTED_DEVICE)
		m_State = IDLE;

	//device may have changed so lets clear the access time map
	m_TimeStamp.Kill();
}


bool XenonProfile::IsCurrentDeviceValid()
{
	return IsDeviceValid(GetSelectedDevice());
}


bool XenonProfile::IsDeviceValid(u32 deviceId)
{
	XDEVICE_DATA deviceData;
	if (deviceId ==XCONTENTDEVICE_ANY)
	{
		return false;
	}

	DWORD Result = XContentGetDeviceData(deviceId, &deviceData);

	if (Result == ERROR_SUCCESS)
	{
		return true;
	}
	else if (Result == ERROR_DEVICE_NOT_CONNECTED)
	{
		return false;
	}
	else
	{
		Errorf("[savegame] IsDeviceValid failed with deviceId %d - unexpected return value from XContentGetDeviceData", deviceId);
		return false;
	}
}

bool XenonProfile::BeginEnumeration(u32 contentType,fiSaveGame::Content *outContent,int maxCount)
{
	if (m_State != IDLE && m_State != HAD_ERROR) {
		if (m_DeviceID!=XCONTENTDEVICE_ANY)
		{
			Warningf("Cannot begin enumeration, operation already in progress");
			return false;
		}
	}
	if (!maxCount)
		return false;

	// Displayf("[savegame] Enumerating content on device %x",m_DeviceID);

	int userIndex = GetUserIndex();

	m_hEnum = INVALID_HANDLE_VALUE;

#ifndef XCONTENTFLAG_ENUM_EXCLUDECOMMON	// Older XeDK?
#define XCONTENTFLAG_ENUM_EXCLUDECOMMON 0
#endif

	DWORD cbBuffer;
	DWORD dwRet = XContentCreateEnumerator( userIndex,
		m_DeviceID,         // Pass in selected device
		contentType,
		userIndex==XUSER_INDEX_NONE ? 0 : XCONTENTFLAG_ENUM_EXCLUDECOMMON,
		maxCount,
		&cbBuffer,
		&m_hEnum );

	if (dwRet != ERROR_SUCCESS) {
		m_State = HAD_ERROR;
		return false;
	}

	if ((sizeof(fiSaveGame::Content) * maxCount) != cbBuffer)
	{
		Errorf("[savegame] XenonProfile::BeginEnumeration - unexpected size returned from XContentCreateEnumerator");
	}

	BeginOverlapped();

	if (XEnumerate(m_hEnum, outContent, cbBuffer, NULL, &m_Overlapped) != ERROR_IO_PENDING) {
		Errorf("[savegame] XEnumerate failed.");
		EndOverlapped();
		m_State = HAD_ERROR;
		CloseHandle(m_hEnum);
		m_hEnum = INVALID_HANDLE_VALUE;
		return false;
	}
	else {
		m_State = ENUMERATING_CONTENT;
		return true;
	}
}


bool XenonProfile::CheckEnumeration(int &outCount)
{
	if (m_State != ENUMERATING_CONTENT) {
		// Errorf("[savegame] Cannot check an enumeration, none is in progress");
		return false;
	}

	DWORD cbBuffer;
	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);
	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
   else if (dwRet == ERROR_SUCCESS || GetError() == fiSaveGame::NO_FILES_EXIST) {	
		m_State = ENUMERATED_CONTENT;
		// This is a count of items, not of bytes!
		outCount = cbBuffer;
		EndOverlapped();
		CloseHandle(m_hEnum);
		m_hEnum = INVALID_HANDLE_VALUE;
		return true;
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during enumeration.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
		outCount = -1;
		EndOverlapped();
		CloseHandle(m_hEnum);
		m_hEnum = INVALID_HANDLE_VALUE;
		return true;
	}
}


void XenonProfile::EndEnumeration()
{
	if (m_State == ENUMERATED_CONTENT)
		m_State = IDLE;
}


bool XenonProfile::BeginSave(u32 contentType,const char16 *contentName,const char *filename,const void *saveData,u32 saveSize,bool overwrite)
{
	if (m_State != IDLE)
		return false;

	if (!saveSize)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szDisplayName, contentName, XCONTENT_MAX_NAME_LENGTH);
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = contentType;
	contentData.DeviceID = m_DeviceID;

	BeginOverlapped();
	DWORD dwRet = XContentCreate(GetUserIndex(), GetSaveRoot(), &contentData, 
			XCONTENTFLAG_CREATEALWAYS
#if __FINAL
#if _XDK_VER >= 2417
			| XCONTENTFLAG_NOPROFILE_TRANSFER
#endif
#else
			| XCONTENTFLAG_ALLOWPROFILE_TRANSFER
#endif
			, NULL, NULL, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XContentCreate failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = SAVING_CONTENT;
		m_SubState = 1;
		safecpy(m_Filename,GetSaveRoot(),sizeof(m_Filename));
		safecat(m_Filename,":\\",sizeof(m_Filename));
		safecat(m_Filename,filename,sizeof(m_Filename));
		m_SaveData = saveData;
		m_SaveDataSize = saveSize;
		m_SaveValid = false;
		m_FileExists = false;
		m_Overwrite = overwrite;
		return true;
	}
}


DWORD XenonProfile::GetResult(DWORD &cbBuffer)
{
	if (m_SubState == 2)
	{
		if (GetOverlappedResult(m_hFile,&m_Overlapped2,&cbBuffer,FALSE))
			return ERROR_SUCCESS;
		else
			return GetLastError();
	}
	else
		return XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);
}


bool XenonProfile::CheckSave(bool &outIsValid, bool &fileExists)
{
	if (m_State != SAVING_CONTENT)
		return false;

	DWORD cbBuffer;
	DWORD dwRet = GetResult(cbBuffer);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if (dwRet == ERROR_SUCCESS) {
		if (m_SubState == 1) {
			EndOverlapped();

			DWORD dwCreationDisposition = m_Overwrite ? CREATE_ALWAYS : CREATE_NEW;

			m_hFile = CreateFile(m_Filename, GENERIC_WRITE,
				FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL, dwCreationDisposition, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL );
			
			SetTimeStamp();//set timestamp here too just in case we error out

			if (m_hFile == INVALID_HANDLE_VALUE) {
				if( GetLastError()==ERROR_FILE_EXISTS )
				{
					Displayf("[savegame] File already exists, cannot overwrite it.");
					m_FileExists = true;
				}
				else
				{
					Errorf("[savegame] Unexpected error (%x) in CreateFile during save",GetLastError());
				}
			}
			else
			{
				m_FileExists = (GetLastError()==ERROR_ALREADY_EXISTS);
				m_SubState = 2;
				BeginOverlapped2();
				if (!WriteFile(m_hFile, m_SaveData, m_SaveDataSize, NULL, &m_Overlapped2) && GetLastError() != ERROR_IO_PENDING) {
					Errorf("[savegame] Unexpected error (%x) in WriteFile during save",GetLastError());
					EndOverlapped2();
					CloseHandle(m_hFile);
					m_hFile = INVALID_HANDLE_VALUE;
				}
				else {
					return false;
				}
			}			
		}
		else if (m_SubState == 2) {
			EndOverlapped2();
			// Displayf("[savegame] Save completed, %d bytes",cbBuffer);
			m_SaveValid = (cbBuffer == m_SaveDataSize);

			if (!m_SaveValid) {
				m_FileExists = false;
				Displayf("[savegame] WriteFile error [%d], bytes written [%d] does not match save data bytes [%d]!", GetLastError(), cbBuffer, m_SaveDataSize);
			}

			CloseHandle(m_hFile);
			m_hFile = INVALID_HANDLE_VALUE;

			SetTimeStamp();

			BeginOverlapped();
			if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING) {
				EndOverlapped();
				Errorf("[savegame] Unexpected error (%x) in XContentClose during save",GetLastError());
				Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
				m_State = HAD_ERROR;
				outIsValid = false;
				fileExists = false;
				return true;
			}
			else {
				m_SubState = 3;
				m_SaveDataSize = cbBuffer;
				return false;
			}
		}
		else if (m_SubState == 3) {
			EndOverlapped();
			// Displayf("[savegame] XContentClose completed.");
			m_State = SAVED_CONTENT;
			outIsValid = m_SaveValid;
			fileExists = m_FileExists;
			return true;
		}
		else if (m_SubState == 4) {
			EndOverlapped();
			// Displayf("[savegame] XContentClose completed.");
			m_State = m_FileExists ? SAVED_CONTENT : HAD_ERROR;
			outIsValid = false;
			fileExists = m_FileExists;
			return true;
		}
		else {
			Quitf("invalid substate in CheckSave");
			return true;
		}
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during save.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		EndOverlapped();
		if (m_SubState == 2) {
			CloseHandle(m_hFile);
			m_hFile = INVALID_HANDLE_VALUE;
		}
		else {
			m_State = HAD_ERROR;
			outIsValid = false;
			fileExists = false;
			return true;
		}
	}

	// There was an error if we haven't returned at this point, close the content!
	BeginOverlapped();
	if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING) {
		EndOverlapped();
		Errorf("[savegame] Unexpected error (%x) in XContentClose during save",GetLastError());
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
		outIsValid = false;
		fileExists = false;
		return true;
	}
	else {
		if (m_SubState != 1)
			m_FileExists = false;
		m_SubState = 4;
		return false;
	}
}


void XenonProfile::EndSave()
{
	if (m_State == SAVED_CONTENT)
		m_State = IDLE;
}


bool XenonProfile::BeginLoad(u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize)
{
	if (m_State != IDLE)
		return false;

	if (!loadSize)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = contentType;
	contentData.DeviceID = deviceId;

	BeginOverlapped();
	DWORD dwRet = XContentCreate(GetUserIndex(), GetSaveRoot(), &contentData, 
		XCONTENTFLAG_OPENEXISTING, NULL, NULL, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XContentCreate failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = LOADING_CONTENT;
		m_SubState = 1;
		safecpy(m_Filename,GetSaveRoot(),sizeof(m_Filename));
		safecat(m_Filename,":\\",sizeof(m_Filename));
		safecat(m_Filename,filename,sizeof(m_Filename));
		m_LoadData = loadData;
		m_LoadDataSize = loadSize;
		return true;
	}
}


bool XenonProfile::CheckLoad(bool &outIsValid,u32 &loadSize)
{
	if (m_State != LOADING_CONTENT)
		return false;

	DWORD cbBuffer;
	DWORD dwRet = GetResult(cbBuffer);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if (dwRet == ERROR_SUCCESS) {
		if (m_SubState == 1) {
			EndOverlapped();
			m_hFile = CreateFile(m_Filename, GENERIC_READ,
				FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL );

			SetTimeStamp();//set timestamp here too just in case we error out

			if (m_hFile == INVALID_HANDLE_VALUE) {
				Errorf("[savegame] Unexpected error (%x) in CreateFile during load",GetLastError());
			}
			else {
				m_SubState = 2;
				BeginOverlapped2();				
				if (!ReadFile(m_hFile, m_LoadData, m_LoadDataSize, NULL, &m_Overlapped2) && GetLastError() != ERROR_IO_PENDING) {
					Errorf("[savegame] Unexpected error (%x) in ReadFile during load",GetLastError());
					EndOverlapped2();
					CloseHandle(m_hFile);
					m_hFile = INVALID_HANDLE_VALUE;
				}
				else {					
					return false;
				}
			}			
		}
		else if (m_SubState == 2) {
			EndOverlapped2();
			// Displayf("[savegame] Load completed, %d bytes",cbBuffer);
			CloseHandle(m_hFile);
			m_hFile = INVALID_HANDLE_VALUE;

			SetTimeStamp();

			BeginOverlapped();
			if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING) {
				EndOverlapped();
				Errorf("[savegame] Unexpected error (%x) in XContentClose during load",GetLastError());
				Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
				m_State = HAD_ERROR;
				outIsValid = false;
				loadSize = 0;
				return true;
			}
			else {
				m_SubState = 3;
				m_LoadDataSize = cbBuffer;
				return false;
			}
		}
		else if (m_SubState == 3) {
			EndOverlapped();
			// Displayf("[savegame] XContentClose completed.");
			m_State = LOADED_CONTENT;
			loadSize = m_LoadDataSize;
			return true;
		}
		else if (m_SubState == 4){
			EndOverlapped();
			// Displayf("[savegame] XContentClose completed.");
			m_State = HAD_ERROR;
			outIsValid = false;
			loadSize = 0;
			return true;
		}
		else {
			Quitf("invalid substate in CheckLoad");
			return true;
		}
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during load.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		EndOverlapped();
		if (m_SubState == 2) {
			CloseHandle(m_hFile);
			m_hFile = INVALID_HANDLE_VALUE;
		}
		else {
			m_State = HAD_ERROR;
			outIsValid = false;
			loadSize = 0;
			return true;
		}
	}

	// There was an error if we haven't returned at this point, close the content!
	BeginOverlapped();
	if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING)
	{
		EndOverlapped();
		Errorf("[savegame] Unexpected error (%x) in XContentClose during load",GetLastError());
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
		outIsValid = false;
		loadSize = 0;
		return true;
	}
	else
	{
		m_SubState = 4;
		return false;
	}
}


void XenonProfile::EndLoad()
{
	if (m_State == LOADED_CONTENT)
		m_State = IDLE;
}


bool XenonProfile::BeginIconSave(u32 contentType,const char *filename,const void *iconData,u32 iconSize)
{
	if (m_State != IDLE)
		return false;

	if (!iconSize)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = contentType;
	contentData.DeviceID = m_DeviceID;

	BeginOverlapped();
	DWORD dwRet = XContentSetThumbnail(GetUserIndex(), &contentData, static_cast<const BYTE*>(iconData), iconSize, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XContentSetThumbnail failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = SAVING_ICON;
		return true;
	}
}


bool XenonProfile::CheckIconSave()
{
	if (m_State != SAVING_ICON)
		return false;

	DWORD cbBuffer;
	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if (dwRet == ERROR_SUCCESS) {
		m_State = SAVED_ICON;
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during icon save.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
	}

	EndOverlapped();
	return true;
}


void XenonProfile::EndIconSave()
{
	if (m_State == SAVED_ICON)
		m_State = IDLE;
}


bool XenonProfile::BeginContentVerify(u32 contentType,u32 deviceId,const char *filename)
{
	if (m_State != IDLE)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = contentType;
	contentData.DeviceID = deviceId;

	BeginOverlapped();
	DWORD dwRet = XContentCreate(GetUserIndex(), GetSaveRoot(), &contentData, 
		XCONTENTFLAG_OPENEXISTING, NULL, NULL, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XContentCreate failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = VERIFYING_CONTENT;
		m_SubState = 1;
		m_SaveValid = false;
		safecpy(m_Filename,GetSaveRoot(),sizeof(m_Filename));
		safecat(m_Filename,":\\",sizeof(m_Filename));
		safecat(m_Filename,filename,sizeof(m_Filename));
		return true;
	}
}

bool XenonProfile::CompareTimestamps(HANDLE hFile)
{
	FILETIME ft;
	char* filename;
	FILETIME* oldft;

	//Displayf("[savegame] NumSlots = %d, num used = %d",m_TimeStamp.GetNumSlots(),m_TimeStamp.GetNumUsed());

	//rage::atMap<atString,FILETIME>::Iterator cctEntry = m_TimeStamp.CreateIterator();
	//for( cctEntry.Start(); ! cctEntry.AtEnd();  cctEntry.Next() )
	//{
	//	Displayf("[savegame] %s = %d,%d",cctEntry.GetKey().c_str(),cctEntry.GetData().dwHighDateTime,cctEntry.GetData().dwLowDateTime);
	//}

	//strip off the path of the filename (if any exists)
	filename = strrchr(m_Filename,'\\') + 1;
	if (filename==NULL) {filename = strrchr(m_Filename,'/')+1;}

	if (filename!=NULL)
	{
		oldft = m_TimeStamp.Access(filename);
	}
	else
	{
		oldft = m_TimeStamp.Access(m_Filename);
	}

	if (oldft == NULL)
	{
		//first time accessing file
		m_SaveValid = true;
	}
	else
	{
		if( GetFileTime(hFile, &ft, NULL, NULL)!=0 )
			m_SaveValid = (oldft->dwHighDateTime==ft.dwHighDateTime && oldft->dwLowDateTime==ft.dwLowDateTime);
		//Displayf("[savegame] m_Filename = %s",m_Filename);
		//Displayf("[savegame] m_SaveValid = (oldft->dwHighDateTime==ft.dwHighDateTime && oldft->dwLowDateTime==ft.dwLowDateTime)");
		//Displayf("[savegame]      %d               %d                       %d                      %d                %d",m_SaveValid,oldft->dwHighDateTime,ft.dwHighDateTime,oldft->dwLowDateTime,ft.dwLowDateTime);
	}
	return m_SaveValid;
}

bool XenonProfile::CheckContentVerify(bool &verified)
{
	if (m_State != VERIFYING_CONTENT)
		return false;

	DWORD cbBuffer;
	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if (dwRet == ERROR_SUCCESS) {
		if( m_SubState==1 )
		{
			EndOverlapped();
			HANDLE hFile = CreateFile(m_Filename, GENERIC_READ,
				FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );

			if (hFile != INVALID_HANDLE_VALUE)
			{
				FILETIME ft;
				char* filename;
				FILETIME* oldft;

				//strip off the path of the filename (if any exists)
				filename = strrchr(m_Filename,'\\') + 1;
				if (filename==NULL) {filename = strrchr(m_Filename,'/')+1;}

				if (filename!=NULL)
				{
					oldft = m_TimeStamp.Access(filename);
				}
				else
				{
					oldft = m_TimeStamp.Access(m_Filename);
				}
            
            if (oldft == NULL)
            {
               //first time accessing file
               m_SaveValid = true;
            }
            else
            {
				   if( GetFileTime(hFile, &ft, NULL, NULL)!=0 )
					   m_SaveValid = (oldft->dwHighDateTime==ft.dwHighDateTime && oldft->dwLowDateTime==ft.dwLowDateTime);
            }
				CloseHandle(hFile);
			}
			else
			{
				Errorf("[savegame] Unexpected error (%x) in CreateFile during content verify",GetLastError());
			}			

			BeginOverlapped();
			if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING)
			{
				EndOverlapped();
				Errorf("[savegame] Unexpected error (%x) in XContentClose during content verify",GetLastError());
				Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
				m_State = HAD_ERROR;
				return true;
			}
			else
			{
				m_SubState = 2;
				return false;
			}
		}
		else if( m_SubState==2 )
		{
			EndOverlapped();
			m_State = VERIFIED_CONTENT;
			verified = m_SaveValid;
			return true;
		}
		else
		{
			Quitf("invalid substate in ContentVerify");
			m_State = HAD_ERROR;
			return true;
		}
	}
	else
	{
		Errorf("[savegame] Unexpected error (%x) during content verify.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		verified = false;
		EndOverlapped();
		m_State = HAD_ERROR;
		return true;
	}
}


void XenonProfile::EndContentVerify()
{
	if(m_State == VERIFIED_CONTENT)
		m_State = IDLE;
}


bool XenonProfile::BeginGetFileSize(const char *filename, bool bCalculateSizeOnDisk)
{
	if (m_State != IDLE)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = XCONTENTTYPE_SAVEDGAME;
	contentData.DeviceID = m_DeviceID;

	BeginOverlapped();
	DWORD dwRet = XContentCreate(GetUserIndex(), GetSaveRoot(), &contentData, 
		XCONTENTFLAG_OPENEXISTING, NULL, NULL, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] BeginGetFileSize - XContentCreate failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = GETTING_FILE_SIZE;
		m_SubState = 1;

		safecpy(m_Filename,GetSaveRoot(),sizeof(m_Filename));
		safecat(m_Filename,":\\",sizeof(m_Filename));
		safecat(m_Filename,filename,sizeof(m_Filename));

		m_bCalculateSizeOnDisk = bCalculateSizeOnDisk;

		return true;
	}
}

bool XenonProfile::CheckGetFileSize(u64 &fileSize)
{
	if (m_State != GETTING_FILE_SIZE)
		return false;

	DWORD cbBuffer;
	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if (dwRet == ERROR_SUCCESS) {
		if( m_SubState==1 )
		{
			EndOverlapped();
			HANDLE hFile = CreateFile(m_Filename, GENERIC_READ,
				FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );	

			SetTimeStamp();//set timestamp here too just in case we error out

			if (hFile != INVALID_HANDLE_VALUE)
			{
				CompareTimestamps(hFile);

				DWORD dwSizeOfExistingFile = GetFileSize(hFile, NULL);

				if (dwSizeOfExistingFile == -1)
				{
					Errorf("[savegame] Failed to GetFileSize in XenonProfile::CheckGetFileSize - error(%x)", GetLastError());
				}
				else
				{
					if (m_bCalculateSizeOnDisk)
					{
						fileSize = CalculateDataSizeOnDisk(dwSizeOfExistingFile);
					}
					else
					{
						fileSize = dwSizeOfExistingFile;
					}
				}
				CloseHandle(hFile);

				SetTimeStamp();//set timestamp here too just in case we error out
			}
			else
			{
				Errorf("[savegame] Unexpected error (%x) in CreateFile during get file size",GetLastError());
			}

			BeginOverlapped();
			if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING)
			{
				EndOverlapped();
				Errorf("[savegame] Unexpected error (%x) in XContentClose during get file size",GetLastError());
				Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
				m_State = HAD_ERROR;
				return true;
			}
			else
			{
				m_SubState = 2;
				return false;
			}
		}
		else if( m_SubState==2 )
		{
			EndOverlapped();
			m_State = HAVE_GOT_FILE_SIZE;
			return true;
		}
		else
		{
			Quitf("invalid substate in get file size");
			return true;
		}
	}
	else
	{
		Errorf("[savegame] Unexpected error (%x) during get file size.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		EndOverlapped();
		m_State = HAD_ERROR;
		return true;
	}
}

void XenonProfile::EndGetFileSize()
{
	if (m_State == HAVE_GOT_FILE_SIZE)
		m_State = IDLE;
}


#if HACK_GTA4
bool XenonProfile::BeginFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType, const char *filename,u32 saveSize)
#else	//	HACK_GTA4
bool XenonProfile::BeginFreeSpaceCheck(const char *filename,u32 saveSize)
#endif	//	HACK_GTA4
{
	if (m_State != IDLE)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = XCONTENTTYPE_SAVEDGAME;
	contentData.DeviceID = m_DeviceID;

	m_SpaceRequiredForNewFile = CalculateDataSizeOnDisk(saveSize);
	m_SizeOfExistingFile = 0;

	BeginOverlapped();
	DWORD dwRet = XContentCreate(GetUserIndex(), GetSaveRoot(), &contentData, 
		XCONTENTFLAG_OPENEXISTING, NULL, NULL, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
//	If this ever returns an error, is it safe to assume that the file doesn't exist?
//	When I've tested this with a file that doesn't exist, XContentCreate has always succeeded then 
//	XGetOverlappedResult returns an error in CheckFreeSpaceCheck
#if HACK_GTA4
		if (SpaceCheckType == FREE_SPACE_CHECK)
		{
			Errorf("[savegame] BeginFreeSpaceCheck - XContentCreate failed (%x).",dwRet);
		}
		else if (SpaceCheckType == EXTRA_SPACE_CHECK)
		{
			Errorf("[savegame] BeginExtraSpaceCheck - XContentCreate failed (%x).",dwRet);
		}
#else	//	HACK_GTA4
		Errorf("[savegame] BeginFreeSpaceCheck - XContentCreate failed (%x).",dwRet);
#endif	//	HACK_GTA4
		EndOverlapped();
#if HACK_GTA4
		if (SpaceCheckType == FREE_SPACE_CHECK)
		{
			m_State = CHECKING_FREE_SPACE_NO_EXISTING_FILE;
		}
		else if (SpaceCheckType == EXTRA_SPACE_CHECK)
		{
			m_State = CHECKING_EXTRA_SPACE_NO_EXISTING_FILE;
		}
#else	//	HACK_GTA4
		m_State = CHECKING_FREE_SPACE_NO_EXISTING_FILE;
#endif	//	HACK_GTA4
		m_SubState = 1;
		return true;
	}
	else {
#if HACK_GTA4
		if (SpaceCheckType == FREE_SPACE_CHECK)
		{
			m_State = CHECKING_FREE_SPACE;
		}
		else if (SpaceCheckType == EXTRA_SPACE_CHECK)
		{
			m_State = CHECKING_EXTRA_SPACE;
		}
#else	//	HACK_GTA4
		m_State = CHECKING_FREE_SPACE;
#endif	//	HACK_GTA4
		m_SubState = 1;

		safecpy(m_Filename,GetSaveRoot(),sizeof(m_Filename));
		safecat(m_Filename,":\\",sizeof(m_Filename));
		safecat(m_Filename,filename,sizeof(m_Filename));
		return true;
	}
}

#if HACK_GTA4
bool XenonProfile::CheckFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType, int &ExtraSpaceRequired, int &TotalHDDFreeSizeKB)
#else	//	HACK_GTA4
bool XenonProfile::CheckFreeSpaceCheck(int &ExtraSpaceRequired)
#endif	//	HACK_GTA4
{
#if HACK_GTA4
	if (SpaceCheckType == FREE_SPACE_CHECK)
	{
		if ( (m_State != CHECKING_FREE_SPACE_NO_EXISTING_FILE) && (m_State != CHECKING_FREE_SPACE) )
			return false;
	}
	else if (SpaceCheckType == EXTRA_SPACE_CHECK)
	{
		if ( (m_State != CHECKING_EXTRA_SPACE_NO_EXISTING_FILE) && (m_State != CHECKING_EXTRA_SPACE) )
			return false;
	}
#else	//	HACK_GTA4
	if ( (m_State != CHECKING_FREE_SPACE_NO_EXISTING_FILE) && (m_State != CHECKING_FREE_SPACE) )
		return false;
#endif	//	HACK_GTA4

	if (m_State == CHECKING_FREE_SPACE_NO_EXISTING_FILE)
	{	//	Not sure if this will ever happen. Non-existent files seem to allow XContentCreate but then cause an error in XGetOverlappedResult below
		m_State = HAVE_CHECKED_FREE_SPACE;
	}
#if HACK_GTA4
	else if (m_State == CHECKING_EXTRA_SPACE_NO_EXISTING_FILE)
	{	//	Not sure if this will ever happen. Non-existent files seem to allow XContentCreate but then cause an error in XGetOverlappedResult below
		m_State = HAVE_CHECKED_EXTRA_SPACE;
	}
	else if ( (m_State == CHECKING_FREE_SPACE) || (m_State == CHECKING_EXTRA_SPACE) )
#else	//	HACK_GTA4
	else if (m_State == CHECKING_FREE_SPACE)
#endif	//	HACK_GTA4
	{
		DWORD cbBuffer;
		DWORD dwRet = XGetOverlappedResult(&m_Overlapped, &cbBuffer, FALSE);

		if (dwRet == ERROR_IO_INCOMPLETE) {
			return false;
		}
		else if (dwRet == ERROR_SUCCESS) {
			if( m_SubState==1 )
			{
				EndOverlapped();
				HANDLE hFile = CreateFile(m_Filename, GENERIC_READ,
					FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );				

				if (hFile != INVALID_HANDLE_VALUE)
				{
					CompareTimestamps(hFile);

					DWORD dwSizeOfExistingFile = GetFileSize(hFile, NULL);

					if (dwSizeOfExistingFile == -1)
					{
						Errorf("[savegame] Failed to GetFileSize in free space check - error(%x)", GetLastError());
					}
					else
					{
						m_SizeOfExistingFile = CalculateDataSizeOnDisk(dwSizeOfExistingFile);
					}
					CloseHandle(hFile);
				}
				else
				{
#if HACK_GTA4
					Errorf("[savegame] Unexpected error (%x) in CreateFile during free/extra space check",GetLastError());
#else	//	HACK_GTA4
					Errorf("[savegame] Unexpected error (%x) in CreateFile during free space check",GetLastError());
#endif	//	HACK_GTA4
				}			

				BeginOverlapped();
				if (XContentClose(GetSaveRoot(),&m_Overlapped) != ERROR_IO_PENDING)
				{
					EndOverlapped();
#if HACK_GTA4
					if (m_State == CHECKING_FREE_SPACE)
					{
						Errorf("[savegame] Unexpected error (%x) in XContentClose during free space check",GetLastError());
						m_State = HAVE_CHECKED_FREE_SPACE;
					}
					else
					{
						Errorf("[savegame] Unexpected error (%x) in XContentClose during extra space check",GetLastError());
						m_State = HAVE_CHECKED_EXTRA_SPACE;
					}
#else	//	HACK_GTA4
					Errorf("[savegame] Unexpected error (%x) in XContentClose during free space check",GetLastError());
					Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
					m_State = HAVE_CHECKED_FREE_SPACE;
#endif	//	HACK_GTA4
				}
				else
				{
					m_SubState = 2;
					return false;
				}
			}
			else if( m_SubState==2 )
			{
				EndOverlapped();
#if HACK_GTA4
				if (m_State == CHECKING_FREE_SPACE)
				{
					m_State = HAVE_CHECKED_FREE_SPACE;
				}
				else
				{
					m_State = HAVE_CHECKED_EXTRA_SPACE;
				}
#else	//	HACK_GTA4
				m_State = HAVE_CHECKED_FREE_SPACE;
#endif	//	HACK_GTA4
			}
			else
			{
#if HACK_GTA4
				Quitf("invalid substate in free/extra space check");
#else	//	HACK_GTA4
				Quitf("invalid substate in free space check");
#endif	//	HACK_GTA4
				return true;
			}
		}
#if HACK_GTA4
		else
		{
			Errorf("[savegame] Unexpected error (%x) during free/extra space check. Maybe file doesn't already exist",dwRet);
			EndOverlapped();
			if (m_State == CHECKING_FREE_SPACE)
			{
				m_State = HAVE_CHECKED_FREE_SPACE;
			}
			else
			{
				m_State = HAVE_CHECKED_EXTRA_SPACE;
			}
		}
	}	//	end of if ( (m_State == CHECKING_FREE_SPACE) || (m_State == CHECKING_EXTRA_SPACE) )
#else	//	HACK_GTA4
		else
		{
			Errorf("[savegame] Unexpected error (%x) during free space check. Maybe file doesn't already exist",dwRet);
			Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
			EndOverlapped();
			m_State = HAVE_CHECKED_FREE_SPACE;
		}
	}	//	end of if (m_State == CHECKING_FREE_SPACE)
#endif	//	HACK_GTA4

	if (m_State == HAVE_CHECKED_FREE_SPACE)
	{
		if (m_SpaceRequiredForNewFile <= m_SizeOfExistingFile)
		{	//	if the existing file is the same size or larger than the rage_new file then we won't need any extra space
			ExtraSpaceRequired = 0;
		}
		else
		{
			u64 NewFileNeedsExtra = m_SpaceRequiredForNewFile - m_SizeOfExistingFile;
			u64 TotalFreeSpace = GetTotalAvailableSpace();

			if (TotalFreeSpace >= NewFileNeedsExtra)
			{
				ExtraSpaceRequired = 0;
			}
			else
			{
				int TotalFreeSpaceKB = (int) ( (TotalFreeSpace + 1023) >> 10);
				int NewFileNeedsExtraKB = (int) ( (NewFileNeedsExtra + 1023) >> 10);

				ExtraSpaceRequired = TotalFreeSpaceKB - NewFileNeedsExtraKB;	//	expect a negative value here
			}
		}

#if HACK_GTA4
		TotalHDDFreeSizeKB = 0;	//	not actually required for FreeSpace check
#endif	//	HACK_GTA4

		return true;
	}
#if HACK_GTA4
	else if (m_State == HAVE_CHECKED_EXTRA_SPACE)
	{
		u64 TotalFreeSpace = GetTotalAvailableSpace();
		TotalHDDFreeSizeKB = (int) ( (TotalFreeSpace + 1023) >> 10);

		if (m_SpaceRequiredForNewFile == m_SizeOfExistingFile)
		{	//	if the existing file is the same size as the new file then we won't need any extra space
			ExtraSpaceRequired = 0;
		}
		else
		{	//	Difference will be negative if the new file is smaller than the existing file
			u64 NewFileNeedsExtra = m_SpaceRequiredForNewFile - m_SizeOfExistingFile;
			if (NewFileNeedsExtra > 0)
			{
				ExtraSpaceRequired = (int) ( (NewFileNeedsExtra + 1023) / 1024);
			}
			else
			{
				ExtraSpaceRequired = (int) ( (NewFileNeedsExtra - 1023) / 1024);
			}
		}

		return true;
	}
#endif	//	HACK_GTA4

	Quitf("Mistake in logic of XenonProfile::CheckFreeSpaceCheck - should have hit a return before reaching the end of the function");
	return false;
}

#if HACK_GTA4
void XenonProfile::EndFreeSpaceCheck(eTypeOfSpaceCheck SpaceCheckType)
{
	if (SpaceCheckType == FREE_SPACE_CHECK)
	{
		if(m_State == HAVE_CHECKED_FREE_SPACE)
			m_State = IDLE;
	}
	else if (SpaceCheckType == EXTRA_SPACE_CHECK)
	{
		if(m_State == HAVE_CHECKED_EXTRA_SPACE)
			m_State = IDLE;
	}
}
#else	//	HACK_GTA4
void XenonProfile::EndFreeSpaceCheck()
{
	if(m_State == HAVE_CHECKED_FREE_SPACE)
		m_State = IDLE;
}
#endif	//	HACK_GTA4


bool XenonProfile::BeginDelete(u32 contentType, const char *filename)
{
	if (m_State != IDLE && m_State != HAD_ERROR)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = contentType;
	contentData.DeviceID = m_DeviceID;

	BeginOverlapped();
	DWORD dwRet = XContentDelete(GetUserIndex(), &contentData, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XContentDelete failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = DELETING_CONTENT;
		m_SubState = 1;
		return true;
	}
}


bool XenonProfile::CheckDelete()
{
	if (m_State != DELETING_CONTENT)
		return false;

	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, NULL, FALSE);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if(dwRet == ERROR_SUCCESS) {
		m_State = DELETED_CONTENT;
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during delete.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
	}

	EndOverlapped();
	return true;
}


void XenonProfile::EndDelete()
{
	if (m_State == DELETED_CONTENT)
		m_State = IDLE;
}


bool XenonProfile::BeginGetCreator(u32 contentType,u32 deviceId,const char *filename)
{
	if (m_State != IDLE && m_State != HAD_ERROR)
		return false;

	XCONTENT_DATA contentData = {0};
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = contentType;
	contentData.DeviceID = deviceId;

	m_bUserIsCreator = false;

	BeginOverlapped();
	DWORD dwRet = XContentGetCreator(GetUserIndex(), &contentData, &m_bUserIsCreator, NULL, &m_Overlapped);
	if (dwRet != ERROR_IO_PENDING) {
		Errorf("[savegame] XContentGetCreator failed (%x).",dwRet);
		EndOverlapped();
		m_State = HAD_ERROR;
		return false;
	}
	else {
		m_State = GETTING_CREATOR;
		return true;
	}
}

bool XenonProfile::CheckGetCreator(bool &bIsCreator)
{
	if (m_State != GETTING_CREATOR)
		return false;

	DWORD dwRet = XGetOverlappedResult(&m_Overlapped, NULL, FALSE);

	if (dwRet == ERROR_IO_INCOMPLETE) {
		return false;
	}
	else if(dwRet == ERROR_SUCCESS) {
		m_State = GOT_CREATOR;
		bIsCreator = false;
		if (m_bUserIsCreator)
		{
			bIsCreator = true;
		}
	}
	else {
		Errorf("[savegame] Unexpected error (%x) during getting creator.",dwRet);
		Errorf("[savegame] Overlapped extended error (%x).",m_Overlapped.dwExtendedError);
		m_State = HAD_ERROR;
	}

	EndOverlapped();
	return true;
}


void XenonProfile::EndGetCreator()
{
	if (m_State == GOT_CREATOR)
		m_State = IDLE;
}


bool XenonProfile::HasFileBeenAccessed(const char* filename)
{
   return (m_TimeStamp.Access(filename) != NULL);
}

void XenonProfile::SetTimeStamp()
{
	FILETIME newTime;
    char* filename;
	FILETIME* pFileTime;

	//Displayf("[savegame] NumSlots = %d, num used = %d",m_TimeStamp.GetNumSlots(),m_TimeStamp.GetNumUsed());

	//rage::atMap<atString,FILETIME>::Iterator cctEntry = m_TimeStamp.CreateIterator();
	//for( cctEntry.Start(); ! cctEntry.AtEnd();  cctEntry.Next() )
	//{
	//	Displayf("[savegame] %s = %d,%d",cctEntry.GetKey().c_str(),cctEntry.GetData().dwHighDateTime,cctEntry.GetData().dwLowDateTime);
	//}

	HANDLE hFile = CreateFile(m_Filename, GENERIC_READ,
		FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );

	if (hFile != INVALID_HANDLE_VALUE) 
	{
		RAGE_TRACK(XenonProfileTimestamps);

		GetFileTime(hFile, &newTime, NULL, NULL);

		//strip off the path of the filename (if any exists)
		filename = strrchr(m_Filename,'\\') + 1;
		if (filename==NULL) {filename = strrchr(m_Filename,'/')+1;}

		if (filename!=NULL)
		{
			pFileTime = m_TimeStamp.Access(filename);
			if (pFileTime)
			{
				pFileTime->dwHighDateTime = newTime.dwHighDateTime;	
				pFileTime->dwLowDateTime = newTime.dwLowDateTime;
			}
			else
			{
				m_TimeStamp.Insert(atString(filename),newTime);
			}
		}
		else
		{
			pFileTime = m_TimeStamp.Access(m_Filename);
			if (pFileTime)
			{
				pFileTime->dwHighDateTime = newTime.dwHighDateTime;	
				pFileTime->dwLowDateTime = newTime.dwLowDateTime;
			}
			else
			{
				m_TimeStamp.Insert(atString(m_Filename),newTime);
			}
		}

		//Displayf("[savegame] Setting new timestamp");
		CompareTimestamps(hFile);

		CloseHandle(hFile);
	}	
}


void fiSaveGame::InitClass()
{
}


void fiSaveGame::ShutdownClass()
{
}


void fiSaveGame::UpdateClass()
{
}


bool fiSaveGame::BeginSelectDevice(int signInId,u32 contentType,u32 minSpaceAvailable,bool forceShow,bool manageStorage)
{
	return s_Profiles[signInId].BeginSelectDevice(contentType,minSpaceAvailable,forceShow,manageStorage);
}


bool fiSaveGame::CheckSelectDevice(int signInId)
{
	return s_Profiles[signInId].CheckSelectDevice();
}


u32 fiSaveGame::GetSelectedDevice(int signInId)
{
	return s_Profiles[signInId].GetSelectedDevice();
}

void fiSaveGame::SetSelectedDevice(int signInId, u32 deviceId)
{
	return s_Profiles[signInId].SetSelectedDevice(deviceId);
}


void fiSaveGame::EndSelectDevice(int signInId)
{
	return s_Profiles[signInId].EndSelectDevice();
}


bool fiSaveGame::BeginEnumeration(int signInId,u32 contentType,Content *outContent,int maxCount)
{
	return s_Profiles[signInId].BeginEnumeration(contentType,outContent,maxCount);
}


bool fiSaveGame::CheckEnumeration(int signInId,int &outCount)
{
	return s_Profiles[signInId].CheckEnumeration(outCount);
}


void fiSaveGame::EndEnumeration(int signInId)
{
	s_Profiles[signInId].EndEnumeration();
}


bool fiSaveGame::BeginSave(int signInId,u32 contentType,const char16 *displayName,const char *filename,const void *saveData,u32 saveSize,bool overwrite)
{
	return s_Profiles[signInId].BeginSave(contentType,displayName,filename,saveData,saveSize,overwrite);
}


bool fiSaveGame::CheckSave(int signInId,bool &outIsValid,bool &fileExists)
{
	return s_Profiles[signInId].CheckSave(outIsValid,fileExists);
}


void fiSaveGame::EndSave(int signInId)
{
	s_Profiles[signInId].EndSave();
}

bool fiSaveGame::BeginLoad(int signInId,u32 contentType,u32 deviceId,const char *filename,void *loadData,u32 loadSize)
{
	return s_Profiles[signInId].BeginLoad(contentType,deviceId,filename,loadData,loadSize);
}


bool fiSaveGame::CheckLoad(int signInId,bool &outIsValid,u32 &loadSize)
{
	return s_Profiles[signInId].CheckLoad(outIsValid,loadSize);
}


void fiSaveGame::EndLoad(int signInId)
{
	s_Profiles[signInId].EndLoad();
}


bool fiSaveGame::BeginIconSave(int signInId,u32 contentType,const char *filename,const void *iconData,u32 iconSize)
{
	return s_Profiles[signInId].BeginIconSave(contentType,filename,iconData,iconSize);
}


bool fiSaveGame::CheckIconSave(int signInId)
{
	return s_Profiles[signInId].CheckIconSave();
}


void fiSaveGame::EndIconSave(int signInId)
{
	s_Profiles[signInId].EndIconSave();
}


bool fiSaveGame::BeginContentVerify(int signInId,u32 contentType,u32 deviceId,const char *filename)
{
	return s_Profiles[signInId].BeginContentVerify(contentType,deviceId,filename);
}


bool fiSaveGame::CheckContentVerify(int signInId,bool &verified)
{
	return s_Profiles[signInId].CheckContentVerify(verified);
}


void fiSaveGame::EndContentVerify(int signInId)
{
	s_Profiles[signInId].EndContentVerify();
}


bool fiSaveGame::BeginGetFileSize(int signInId, const char *filename, bool bCalculateSizeOnDisk)
{
	return s_Profiles[signInId].BeginGetFileSize(filename, bCalculateSizeOnDisk);
}

bool fiSaveGame::CheckGetFileSize(int signInId, u64 &fileSize)
{
	return s_Profiles[signInId].CheckGetFileSize(fileSize);
}

void fiSaveGame::EndGetFileSize(int signInId)
{
	s_Profiles[signInId].EndGetFileSize();
}

bool fiSaveGame::BeginGetFileModifiedTime(int /*signInId*/, const char* /*filename*/)
{
	Assertf(0, "fiSaveGame::BeginGetFileModifiedTime not implemented for 360 so far");
	return true;
}

bool fiSaveGame::CheckGetFileModifiedTime(int /*signInId*/, u32& /*modTimeHigh*/, u32& /*modTimeLow*/)
{
	Assertf(0, "fiSaveGame::CheckGetFileModifiedTime not implemented for 360 so far");
	return true;
}

void fiSaveGame::EndGetFileModifiedTime(int /*signInId*/)
{
	Assertf(0, "fiSaveGame::EndGetFileModifiedTime not implemented for 360 so far");
}


bool fiSaveGame::BeginFreeSpaceCheck(int signInId,const char *filename,u32 saveSize)
{
#if HACK_GTA4
	return s_Profiles[signInId].BeginFreeSpaceCheck(FREE_SPACE_CHECK, filename,saveSize);
#else	//	HACK_GTA4
	return s_Profiles[signInId].BeginFreeSpaceCheck(filename,saveSize);
#endif	//	HACK_GTA4
}

bool fiSaveGame::CheckFreeSpaceCheck(int signInId, int &ExtraSpaceRequired)
{
#if HACK_GTA4
	int UnusedParam = 0;
	return s_Profiles[signInId].CheckFreeSpaceCheck(FREE_SPACE_CHECK, ExtraSpaceRequired, UnusedParam);
#else	//	HACK_GTA4
	return s_Profiles[signInId].CheckFreeSpaceCheck(ExtraSpaceRequired);
#endif	//	HACK_GTA4
}

void fiSaveGame::EndFreeSpaceCheck(int signInId)
{
#if HACK_GTA4
	s_Profiles[signInId].EndFreeSpaceCheck(FREE_SPACE_CHECK);
#else	//	HACK_GTA4
	s_Profiles[signInId].EndFreeSpaceCheck();
#endif	//	HACK_GTA4
}

#if HACK_GTA4
bool fiSaveGame::BeginExtraSpaceCheck(int signInId, const char *filename, u32 saveSize)
{
	return s_Profiles[signInId].BeginFreeSpaceCheck(EXTRA_SPACE_CHECK, filename,saveSize);
}

bool fiSaveGame::CheckExtraSpaceCheck(int signInId, int &ExtraSpaceRequired, int &TotalHDDFreeSizeKB, int & /*SizeOfSystemFileKB*/)
{
	return s_Profiles[signInId].CheckFreeSpaceCheck(EXTRA_SPACE_CHECK, ExtraSpaceRequired, TotalHDDFreeSizeKB);
}

void fiSaveGame::EndExtraSpaceCheck(int signInId)
{
	s_Profiles[signInId].EndFreeSpaceCheck(EXTRA_SPACE_CHECK);
}


int fiSaveGame::CalculateSizeOfSaveGameFile(int /*DataSize*/, int /*SizeOfSystemFiles*/)
{
	Errorf("[savegame] CalculateSizeOfSaveGameFile only supported for PS3");
	return -1;
}
#endif	//	HACK_GTA4

bool fiSaveGame::GetCurrentDeviceData(int signInId, Device *outDevice)
{
	return s_Profiles[signInId].GetCurrentDeviceData(outDevice);
}


bool fiSaveGame::GetDeviceData(int signInId, u32 deviceId, Device *outDevice)
{
	return s_Profiles[signInId].GetDeviceData(deviceId, outDevice);
}

#if HACK_GTA4
bool fiSaveGame::BeginReadNamesAndSizesOfAllSaveGames(int /*signInId*/, Content* /*outContent*/, int* /*outFileSizes*/, int /*maxCount*/)
{
	return true;
}

bool fiSaveGame::CheckReadNamesAndSizesOfAllSaveGames(int /*signInId*/, int& /*outCount*/)
{
	return true;
}

void fiSaveGame::EndReadNamesAndSizesOfAllSaveGames(int /*signInId*/)
{
}
#endif	//	HACK_GTA4

bool fiSaveGame::BeginDelete(int signInId,u32 contentType,const char *filename)
{
	return s_Profiles[signInId].BeginDelete(contentType,filename);
}


bool fiSaveGame::CheckDelete(int signInId)
{
	return s_Profiles[signInId].CheckDelete();
}


void fiSaveGame::EndDelete(int signInId)
{
	s_Profiles[signInId].EndDelete();
}

bool fiSaveGame::BeginGetCreator(int signInId, u32 contentType,u32 deviceId,const char *filename)
{
	return s_Profiles[signInId].BeginGetCreator(contentType,deviceId,filename);
}

bool fiSaveGame::CheckGetCreator(int signInId, bool &bIsCreator)
{
	return s_Profiles[signInId].CheckGetCreator(bIsCreator);
}

void fiSaveGame::EndGetCreator(int signInId)
{
	s_Profiles[signInId].EndGetCreator();
}

u64 fiSaveGame::GetTotalAvailableSpace(int signInId)
{
   return s_Profiles[signInId].GetTotalAvailableSpace();
}

u64 fiSaveGame::CalculateDataSizeOnDisk(int signInId , u32 dataSize)
{
   return s_Profiles[signInId].CalculateDataSizeOnDisk(dataSize);
}

void fiSaveGame::SetSelectedDeviceToAny(int signInId)
{
   s_Profiles[signInId].SetSelectedDeviceToAny();
}

bool fiSaveGame::IsDeviceValid(int signInId, u32 deviceId)
{
	return s_Profiles[signInId].IsDeviceValid(deviceId);
}

bool fiSaveGame::IsCurrentDeviceValid(int signInId)
{
   return s_Profiles[signInId].IsCurrentDeviceValid();
}

int fiSaveGame::GetMaxUsers()
{
   return XUSER_MAX_COUNT;
}

bool fiSaveGame::IsStorageDeviceChanged(int signInId)
{
   return s_Profiles[signInId].IsStorageDeviceChanged();
}

bool fiSaveGame::IsStorageDeviceRemoved(int signInId)
{
	return s_Profiles[signInId].IsStorageDeviceRemoved();
}

fiSaveGame::Errors fiSaveGame::GetError(int signInId)
{
   return s_Profiles[signInId].GetError();
}

fiSaveGameState::State fiSaveGame::GetState(int signInId) const { 
	return s_Profiles[signInId].GetState(); 
}

void fiSaveGame::SetStateToIdle(int signInId)
{
   s_Profiles[signInId].SetStateToIdle(); 
}

bool fiSaveGame::HasFileBeenAccessed(int signInId, const char* filename)
{
   return s_Profiles[signInId].HasFileBeenAccessed(filename); 
}

void fiSaveGame::SetIcon(char* /*pIcon*/, const u32 /*SizeOfIconFile*/)
{

}

void fiSaveGame::SetMaxNumberOfSaveGameFilesToEnumerate(int /*MaxNumSaveGames*/)
{
}

void fiSaveGame::SetSaveGameTitle(const char* /*pSaveGameTitle*/)
{
}

void fiSaveGame::SetUserServiceId(int /* index */ , int /* userId */)
{
}

void fiSaveGame::SetMountIndex(int /* signInId */, int /* index */)
{
}

void fiSaveGame::SetCheckForUserBindErrors(const char* /*pBindErrorMessage*/)
{
}

#define FILLDEVICE_MAX_FILE_SIZE 0x0000000070000000 
__int64 IncreaseFileSize(HANDLE hf, __int64 distance, DWORD MoveMethod)
{
	LARGE_INTEGER li;

	li.QuadPart = distance;

	li.LowPart = SetFilePointer (hf, li.LowPart, &li.HighPart, MoveMethod);

	if (li.LowPart == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
	{
		li.QuadPart = 0;
	}

	if(!SetEndOfFile(hf))
	{
		return 0;
	}

	return li.QuadPart;
}

bool CreateFillFile(const char* filename , XCONTENTDEVICEID deviceID, int userIndex, const char* saveRoot )
{
	char filenameAndPath[XCONTENT_MAX_FILENAME_LENGTH];

	// Initialize content type and device struct
	XCONTENT_DATA contentData = {0};

	CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
	AsciiToWide( reinterpret_cast<char16*>(contentData.szDisplayName), filename , XCONTENT_MAX_FILENAME_LENGTH);
	safecpy(contentData.szFileName, filename, XCONTENT_MAX_FILENAME_LENGTH);
	contentData.dwContentType = XCONTENTTYPE_SAVEDGAME;
	contentData.DeviceID = deviceID;  

	// Mount the device to the dynamic drive name "savedrive"
	DWORD dwRet = XContentCreate(userIndex, saveRoot, &contentData, 
		XCONTENTFLAG_CREATEALWAYS, NULL, NULL,NULL);
	if (dwRet != 0) {
		Errorf("[savegame] XContentCreate failed (%x).",dwRet);
		Errorf("[savegame] Unexpected error (%x) in XContentCreate during save",GetLastError());
		return false;
	}

	safecpy(filenameAndPath,saveRoot,sizeof(filenameAndPath));
	safecat(filenameAndPath,":\\",sizeof(filenameAndPath));
	safecat(filenameAndPath,filename,sizeof(filenameAndPath));
	
	HANDLE file = CreateFile(filenameAndPath, GENERIC_WRITE,
		FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL , NULL );

	if (file == INVALID_HANDLE_VALUE) {
		Errorf("[savegame] Unexpected error (%x) in CreateFile during save",GetLastError());
	}

	//find a large file size to set this file to
	__int64 distance = FILLDEVICE_MAX_FILE_SIZE;
	while (!IncreaseFileSize(file, distance,FILE_BEGIN))
	{
		distance >>= 1;
	}	

	CloseHandle(file);

	XContentClose(saveRoot, NULL );

	return true;
}

void XenonProfile::FillDevice()
{
	u64 size = GetTotalAvailableSpace();

	char filename[64];
	int index=0;

	while(size > 38976)
	{
		sprintf(filename,"fill_file_%d",index);

		if (!CreateFillFile(filename , GetSelectedDevice() , GetUserIndex() , GetSaveRoot()))
		{
			return;
		}

		size = GetTotalAvailableSpace();
		index++;
	}
}

void fiSaveGame::FillDevice(int signInId)
{
	s_Profiles[signInId].FillDevice();
}

}	// namespace rage

#pragma warning(pop)

#endif	// __XENON
