// 
// file/tcpip_win32.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#include "tcpip.h"
#include "winsock.h"
#include "file/file_config.h"
#include "system/memops.h"

#if __WIN32

#include "system/xtl.h"

// TODO: NS - move define to more global location
#define IPV6_SOCKETS (RSG_DURANGO)

#if IPV6_SOCKETS
#include <ws2tcpip.h>
#endif

#include "string/string.h"

namespace rage {

static fiDeviceTcpIp s_TcpIpInstance;

const fiDeviceTcpIp& fiDeviceTcpIp::GetInstance() {
	return s_TcpIpInstance;
}


fiDeviceTcpIp::fiDeviceTcpIp() {
}


fiDeviceTcpIp::~fiDeviceTcpIp() {
}


void fiDeviceTcpIp::InitClass(int argc, char** argv) {
	InitWinSock(argc, argv);
}

int fiDeviceTcpIp::PrintNetStats()
{
	return 0;
}

int fiDeviceTcpIp::PrintInterfaceState()
{
	return 0;
}

void fiDeviceTcpIp::GetRemoteName(fiHandle handle,char *buffer,int bufferSize)
{
#if IPV6_SOCKETS
	struct sockaddr_storage isa;
#else
	struct sockaddr_in isa;
#endif

	int i = sizeof(isa);
	getpeername((SOCKET)handle,(sockaddr*)&isa,&i);
	Assert(buffer && bufferSize >= 46); // Max size for IPv6 address

#if IPV6_SOCKETS
	inet_ntop(AF_INET6, (void*)&isa, buffer, bufferSize);
#else
	formatf(buffer, bufferSize, "%d.%d.%d.%d", isa.sin_addr.S_un.S_un_b.s_b1,
		isa.sin_addr.S_un.S_un_b.s_b2, isa.sin_addr.S_un.S_un_b.s_b3,
		isa.sin_addr.S_un.S_un_b.s_b4);
#endif
}

void fiDeviceTcpIp::WaitForNetworkReady() {
}


void fiDeviceTcpIp::ShutdownClass() {
	ShutdownWinSock();
}

// Filename format for fiDeviceTcpIp should be:
// "tcpip:<port>:<host>" or "tcpip:<port>:LISTEN"
// e.g.
// "tcpip:1234:localhost", "tcpip:80:10.0.23.14", "tcpip:23:LISTEN"
fiHandle fiDeviceTcpIp::Open(const char *filename, bool /*readOnly*/) const {
#if __ASSERT
	const char* origName = filename;
#endif
	if (strncmp(filename,"tcpip:",6))
		return fiHandleInvalid;
	filename += 6;
	int port = 0;
	while (*filename && *filename != ':')
		port = port * 10 + (*filename++)-'0';
	Assertf(*filename == ':', "Badly formed tcpip filename '%s'", origName);
	if (!*filename)
		return fiHandleInvalid;
	else
		++filename;
	if (!stricmp(filename,"LISTEN")) {
		fiHandle listener = Listen(port,1);
		if (fiIsValidHandle(listener)) {
			Displayf("Waiting for connection on port %d...",port);
			fiHandle data = Pickup(listener);
			Close(listener);
			return data;
		}
		else {
			Errorf("Unable to establish server on port %d, already in use?",port);
			return listener;
		}
	}
	else
		return Connect(filename,port);
}


fiHandle fiDeviceTcpIp::Create(const char * filename) const {
	return Open(filename,false);
}


int fiDeviceTcpIp::StaticWrite(fiHandle fd,const void *buffer,int count) {
	int result = send((SOCKET)fd,(const char*)buffer,count,0);
	if (result == -1 && WSAGetLastError() == WSAEWOULDBLOCK)
		result = 0;
#if !__FINAL && 0
	else if (result == -1) {
		char buf[128];
		int lastError = WSAGetLastError();
		formatf(buf,sizeof(buf),"fiDeviceTcpIp::StaticWrite(%d) returned %d(%x)\n",count,lastError,lastError);
		OutputDebugString(buf);
		SetLastError("Write error %d (%x)", lastError, lastError);
	}
#endif
	return result;
}


int fiDeviceTcpIp::StaticRead(fiHandle fd,void *buffer,int count) {
	int result = recv((SOCKET)fd,(char*)buffer,count,0);
	if (result == -1 && WSAGetLastError() == WSAEWOULDBLOCK)
		result = 0;
#if !__FINAL
	else if (result == -1) {
		char buf[128];
		int lastError = WSAGetLastError();
		formatf(buf,sizeof(buf),"fiDeviceTcpIp::StaticRead(%d) returned %d(%x)\n",count,lastError,lastError);
		OutputDebugString(buf);
		SetLastError("Read error %d (%x)", lastError, lastError);
	}
#endif
	return result;
}


int fiDeviceTcpIp::GetReadCount(fiHandle fd) {
	unsigned long readCount = 0;
	ioctlsocket((SOCKET)fd,FIONREAD,&readCount);
	return readCount;
}

void fiDeviceTcpIp::SetBlocking(fiHandle handle, bool blocking)
{
	u_long nonblocking = blocking ? 0 : 1;
#if __XENON
	XSocketIOCTLSocket((SOCKET)handle, FIONBIO, &nonblocking);
#else
	ioctlsocket((SOCKET)handle, FIONBIO, &nonblocking);
#endif
}

int fiDeviceTcpIp::Seek(fiHandle /*handle*/,int /*offset*/,fiSeekWhence /*whence*/) const {
	return -1;
}


u64 fiDeviceTcpIp::Seek64(fiHandle /*handle*/,s64 /*offset*/,fiSeekWhence /*whence*/) const {
	return (u64)(s64)-1;
}


u64 fiDeviceTcpIp::GetFileTime(const char*) const {
	return 0;
}


bool fiDeviceTcpIp::SetFileTime(const char*,u64) const {
	return false;
}


int fiDeviceTcpIp::Close(fiHandle handle) const {
	return closesocket((SOCKET)handle);
}

#if IPV6_SOCKETS
static bool GetAddress(const char *name,int port,struct sockaddr_storage *sa) {
#else
static bool GetAddress(const char *name,int port,struct sockaddr_in *sa) {
#endif
	char myname[256];
#if !__XENON && !__GFWL && !IPV6_SOCKETS
	struct hostent *hp;
#endif
	memset(sa,0,sizeof(*sa));

#if IPV6_SOCKETS
	bool anyIp = (name == NULL) || (stricmp(name, "localhost") == 0);
	if(anyIp)
	{
		sockaddr_in6 addr = {0};
		addr.sin6_addr = in6addr_any;
		addr.sin6_family = AF_INET6;
		addr.sin6_port = (unsigned short) htons((unsigned short)port); 
		*(sockaddr_in6*)sa = addr;
		return true;
	}
#endif

	if (!name) {
#if __XENON || __GFWL
		strcpy(myname,"unknown");
#else
		gethostname(myname,sizeof(myname));
#endif
		name = myname;
	}

#if IPV6_SOCKETS
	ADDRINFO hints;
	memset(&hints, 0, sizeof (hints));
	hints.ai_family = AF_INET6;		// IPv6 address family
	hints.ai_flags = AI_V4MAPPED;	// if no IPv6 address exists, ask for an IPv4-mapped IPv6 address

#if 1
	// TODO: NS - there appears to be a bug on Durango: if you pass getaddrinfo
	// a string containing an IPv4 IP, it throws an exception stating
	// "the binding handle is invalid". Format the string as an IPv4-mapped IPv6
	// address as a workaround.
	char tmp[256] = {0};
	sockaddr_storage tmpAddr;
	if(inet_pton(AF_INET, name, &tmpAddr) == 1)
	{
		formatf(tmp, "::ffff:%s", name);
		name = tmp;
	}
#endif

	// use AI_NUMERICHOST first to see if it's already in numeric format, otherwise a DNS lookup will be performed
	hints.ai_flags |= AI_NUMERICHOST;
	ADDRINFO* addrInfo = NULL;
	bool resolved = (getaddrinfo(name, NULL, &hints, &addrInfo) == 0) && (addrInfo != NULL);
	if(resolved == false)
	{
		// DNS lookup
		hints.ai_flags &= ~(int)AI_NUMERICHOST;
		resolved = (getaddrinfo(name, NULL, &hints, &addrInfo) == 0) && (addrInfo != NULL);
	}

	if(resolved)
	{
		Assert(addrInfo->ai_family == AF_INET6);
		sockaddr_in6 sin = *(sockaddr_in6*)addrInfo->ai_addr;
		sin.sin6_port = (unsigned short) htons((unsigned short)port);
		*(sockaddr_in6*)sa = sin;
		freeaddrinfo(addrInfo);
		addrInfo = NULL;
	}

#else
	if ((sa->sin_addr.s_addr = inet_addr(name)) != -1)
		sa->sin_family = AF_INET;
#if !__XENON && !__GFWL
	else if ((hp = gethostbyname(name)) != 0) {
		sa->sin_family = hp->h_addrtype;
		sysMemCpy(&sa->sin_addr,hp->h_addr, hp->h_length);
	}
#endif
#endif
	else
	{
		// If you're on Xbox and running .net, did you remember to copy rfs.dat
		// into your project-specific executable subdirectory?
		return false;
	}

#if !IPV6_SOCKETS
	sa->sin_port = (unsigned short) htons((unsigned short)port);
#endif

	return true;
}

fiHandle fiDeviceTcpIp::Connect(const char *address,int port) {
	WaitForNetworkReady();

#if	IPV6_SOCKETS
	struct sockaddr_storage sa;
#else
	struct sockaddr_in sa;
#endif

	SOCKET s;

	if (!GetAddress(address,port,&sa))
		return fiHandleInvalid;

#if IPV6_SOCKETS
	int addressFamily = AF_INET6;
#else
	int addressFamily = AF_INET;
#endif

	if ((s = socket(addressFamily,SOCK_STREAM,IPPROTO_TCP)) == INVALID_SOCKET)
		return fiHandleInvalid;

#if IPV6_SOCKETS
		// disable IPv6-only mode, we support IPv4-mapped IPv6 addresses
		int v6only = 0;
		AssertVerify(setsockopt(s,
								IPPROTO_IPV6,
								IPV6_V6ONLY,
								(const char*) &v6only,
								sizeof(v6only)) == 0);
#endif

#if __XENON && 0
	// New behavior in Feb XeDK: Local port doesn't automatically reset to 1024
	// when stopping a debug session, which causes the PC-side server to get confused.
	struct sockaddr_in local;
	memset(&local,0,sizeof(local));
	local.sin_family = AF_INET;
	local.sin_port = htons(1024);
	if (bind(s,(struct sockaddr*)&local,sizeof(local)) < 0) {
		closesocket(s);
		return fiHandleInvalid;
	}
#endif

	if (connect(s,(sockaddr*)&sa,sizeof(sa)) < 0) {
		closesocket(s);
		return fiHandleInvalid;
	}

	bool noNagle = true;
	int len = sizeof(bool);
	if (setsockopt( (SOCKET)s, IPPROTO_TCP, TCP_NODELAY, (char *)(&noNagle), len ) < 0) {
		closesocket(s);
		return fiHandleInvalid;
	}

	return (fiHandle) s;
}


fiHandle fiDeviceTcpIp::Listen(int port,int maxIncoming,const char * WIN32PC_ONLY(ip) DURANGO_ONLY(ip)) {
	SOCKET s;

#if IPV6_SOCKETS
	sockaddr_storage sa;
#else
	sockaddr_in sa;
#endif

#if __LIVE
	sa.sin_family = AF_INET;
	sa.sin_addr.S_un.S_addr = INADDR_ANY;
	sa.sin_port = htons((u_short)port);
#else
	if (!GetAddress(ip, port, &sa))
		return fiHandleInvalid;
#endif

#if IPV6_SOCKETS
	int addressFamily = AF_INET6;
#else
	int addressFamily = AF_INET;
#endif

	if ((s = socket(addressFamily,SOCK_STREAM,IPPROTO_TCP)) == INVALID_SOCKET)
		return fiHandleInvalid;

#if IPV6_SOCKETS
		// disable IPv6-only mode, we support IPv4-mapped IPv6 addresses
		int v6only = 0;
		AssertVerify(setsockopt(s,
								IPPROTO_IPV6,
								IPV6_V6ONLY,
								(const char*) &v6only,
								sizeof(v6only)) == 0);
#endif

	if (bind(s,(struct sockaddr*)&sa,sizeof(sa)) == SOCKET_ERROR) {
		closesocket(s);
		return fiHandleInvalid;
	}

	if (listen(s, maxIncoming) == SOCKET_ERROR) {
		closesocket(s);
		return fiHandleInvalid;
	}

	return (fiHandle) s;
}


fiHandle fiDeviceTcpIp::Pickup(fiHandle listenSocket) {
#if IPV6_SOCKETS
	struct sockaddr_storage isa;
#else
	struct sockaddr_in isa;
#endif

	int i;
	SOCKET c;

	i = sizeof(isa);
	getsockname((SOCKET)listenSocket,(sockaddr*)&isa,&i);

	// Displayf("Waiting for client connection...");
	if ((c = accept((SOCKET)listenSocket,(sockaddr*)&isa,&i)) == INVALID_SOCKET)
		return fiHandleInvalid;

	return (fiHandle) c;
}

}	// namespace rage

#endif	// __WIN32
