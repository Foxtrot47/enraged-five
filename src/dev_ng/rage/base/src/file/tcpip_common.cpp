// 
// file/tcpip_common.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "tcpip.h"

#include "string/string.h"

namespace rage {

static char s_LocalHost[64] = "localhost";
static char s_LocalIpAddr[64] = "0.0.0.0";

#if !__FINAL
static char s_TcpIpLastError[128];
#endif // !__FINAL

void fiDeviceTcpIp::SetLocalHost(const char *addr) 
{
	safecpy(s_LocalHost,addr,sizeof(s_LocalHost));
}


const char* fiDeviceTcpIp::GetLocalHost()
{
	return s_LocalHost;
}

void fiDeviceTcpIp::SetLocalIpAddrName(const char* name)
{
	safecpy(s_LocalIpAddr, name);
}

const char* fiDeviceTcpIp::GetLocalIpAddrName()
{
	return s_LocalIpAddr;
}

const char* fiDeviceTcpIp::GetDebugName() const
{
	return "TcpIp";
}

#if !__FINAL
const char *fiDeviceTcpIp::GetLastError()
{
	return s_TcpIpLastError;
}

void fiDeviceTcpIp::SetLastError(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	vformatf(s_TcpIpLastError, sizeof(s_TcpIpLastError), fmt, args);
	va_end(args);
}

#else // !__FINAL

const char *fiDeviceTcpIp::GetLastError()
{
	return NULL;
}

void fiDeviceTcpIp::SetLastError(const char * /*fmt*/, ...)
{
}
#endif // !__FINAL

}	// namespace rage
