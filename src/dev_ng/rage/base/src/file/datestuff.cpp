/* Reference code which gives us the conversion factors
	for Win32-style dates */
#define STRICT
#include <windows.h>
#include <stdio.h>

int main() {
	SYSTEMTIME _1970 = { 1970, 1, 0, 1, 0, 0, 0, 0 };
	unsigned __int64 ft = 0;
	SYSTEMTIME _1601;
	
	FileTimeToSystemTime((FILETIME*)&ft,&_1601);
	printf("%4d/%2d/%2d %2d:%02d:%02d\n",
		_1601.wYear,
		_1601.wMonth,
		_1601.wDay,
		_1601.wHour,
		_1601.wMinute,
		_1601.wSecond);
	SystemTimeToFileTime(&_1970, (FILETIME*)&ft);
	char buffer[100];
	_ui64toa(ft,buffer,10);
	printf("filetime of 1970 %s\n",buffer);
}
