// 
// file/tcpip_psn.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#include "tcpip.h"
#include "winsock.h"

#if __PPU

#include <sdk_version.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netex/net.h>
#include <netex/libnetctl.h>
#include <netex/errno.h>
#include <netex/ifctl.h>
#include <netex/libnetctl.h>
#include <netex/sockinfo.h>
#include <stdio.h>

#include "system/ipc.h"
#include "system/memops.h"

typedef int SOCKET;

#include <string.h>

#pragma comment(lib,"net_stub")
	
namespace rage {

#if !__FINAL
	// see <netex/errno.h>
	static const char* s_netError[] = {
		"UNKNOWN ERROR",			// 0
		"SYS_NET_EPERM",			// 1 
		"SYS_NET_ENOENT",
		"SYS_NET_ESRCH",
		"SYS_NET_EINTR",
		"SYS_NET_EIO",
		"SYS_NET_ENXIO",
		"SYS_NET_E2BIG",
		"SYS_NET_ENOEXEC",
		"SYS_NET_EBADF",
		"SYS_NET_ECHILD",			// 10
		"SYS_NET_EDEADLK",
		"SYS_NET_ENOMEM",
		"SYS_NET_EACCES",
		"SYS_NET_EFAULT",
		"SYS_NET_ENOTBLK",
		"SYS_NET_EBUSY",
		"SYS_NET_EEXIST",
		"SYS_NET_EXDEV",
		"SYS_NET_ENODEV",
		"SYS_NET_ENOTDIR",			// 20
		"SYS_NET_EISDIR",
		"SYS_NET_EINVAL",
		"SYS_NET_ENFILE",
		"SYS_NET_EMFILE",
		"SYS_NET_ENOTTY",
		"SYS_NET_ETXTBSY",
		"SYS_NET_EFBIG",
		"SYS_NET_ENOSPC",
		"SYS_NET_ESPIPE",
		"SYS_NET_EROFS",			// 30
		"SYS_NET_EMLINK",
		"SYS_NET_EPIPE",
		"SYS_NET_EDOM",
		"SYS_NET_ERANGE",
		"SYS_NET_EAGAIN/SYS_NET_EWOULDBLOCK",
		"SYS_NET_EINPROGRESS",
		"SYS_NET_EALREADY",
		"SYS_NET_ENOTSOCK",
		"SYS_NET_EDESTADDRREQ",
		"SYS_NET_EMSGSIZE",			// 40
		"SYS_NET_EPROTOTYPE",	
		"SYS_NET_ENOPROTOOPT",
		"SYS_NET_EPROTONOSUPPORT",
		"SYS_NET_ESOCKTNOSUPPORT",
		"SYS_NET_EOPNOTSUPP",
		"SYS_NET_EPFNOSUPPORT",
		"SYS_NET_EAFNOSUPPORT",
		"SYS_NET_EADDRINUSE",
		"SYS_NET_EADDRNOTAVAIL",
		"SYS_NET_ENETDOWN",			// 50
		"SYS_NET_ENETUNREACH",
		"SYS_NET_ENETRESET",
		"SYS_NET_ECONNABORTED",
		"SYS_NET_ECONNRESET",
		"SYS_NET_ENOBUFS",
		"SYS_NET_EISCONN",
		"SYS_NET_ENOTCONN",
		"SYS_NET_ESHUTDOWN",
		"SYS_NET_ETOOMANYREFS",
		"SYS_NET_ETIMEDOUT",		// 60
		"SYS_NET_ECONNREFUSED",
		"SYS_NET_ELOOP",
		"SYS_NET_ENAMETOOLONG",
		"SYS_NET_EHOSTDOWN",
		"SYS_NET_EHOSTUNREACH",
		"SYS_NET_ENOTEMPTY",
		"SYS_NET_EPROCLIM",
		"SYS_NET_EUSERS",
		"SYS_NET_EDQUOT",
		"SYS_NET_ESTALE",			// 70
		"SYS_NET_EREMOTE",
		"SYS_NET_EBADRPC",
		"SYS_NET_ERPCMISMATCH",
		"SYS_NET_EPROGUNAVAIL",
		"SYS_NET_EPROGMISMATCH",
		"SYS_NET_EPROCUNAVAIL",
		"SYS_NET_ENOLCK",
		"SYS_NET_ENOSYS",
		"SYS_NET_EFTYPE",
		"SYS_NET_EAUTH",			// 80
		"SYS_NET_ENEEDAUTH",	
		"SYS_NET_EIDRM",
		"SYS_NET_ENOMSG",
		"SYS_NET_EOVERFLOW",
		"SYS_NET_EILSEQ",
		"SYS_NET_ENOTSUP",
		"SYS_NET_ECANCELED",
		"SYS_NET_EBADMSG",
		"SYS_NET_ENODATA",
		"SYS_NET_ENOSR",			// 90
		"SYS_NET_ENOSTR",
		"SYS_NET_ETIME",
		"SYS_NET_ELAST"
	};
	static const int s_netErrorCount = sizeof(s_netError)/sizeof(s_netError[0]);
	static const char* s_location = NULL;
	static int s_count = 0;
	static int s_lastErrorCode = -1;
	static void sysNetError(const char* location, int errorcode) { 
		if (s_lastErrorCode != errorcode || strcmp(location, s_location)) {
			s_location = location;
			s_lastErrorCode = errorcode;
			s_count = 0;

			fiDeviceTcpIp::SetLastError("%s - %s (%d)", location, s_netError[(errorcode >= 0 && errorcode < s_netErrorCount)?errorcode:0], errorcode);
		}

		if (++s_count < 5) { // don't spew
			printf("sysNetError - %s - %s (%d) - probably sysTrayRfs problem\n", location, s_netError[(errorcode >= 0 && errorcode < s_netErrorCount)?errorcode:0], errorcode);	
			sys_net_show_ifconfig();
		}
	}
#else
	static void sysNetError(const char*, int) {}
#endif

const fiDeviceTcpIp& fiDeviceTcpIp::GetInstance() {
	static fiDeviceTcpIp s_TcpIpInstance;
	return s_TcpIpInstance;
}


fiDeviceTcpIp::fiDeviceTcpIp() {
}


fiDeviceTcpIp::~fiDeviceTcpIp() {
}


void fiDeviceTcpIp::InitClass(int argc, char** argv) {
	InitWinSock(argc, argv);
}

static struct sys_net_sockinfo_ex info_table[256];

int fiDeviceTcpIp::PrintNetStats()
{
	sys_net_show_ifconfig();

	struct sys_net_sockinfo_ex *p = info_table;
	int n, k;
	char tmp[128];

	n = sys_net_get_sockinfo_ex(-1, p, sizeof(info_table) / sizeof(*p), SYS_NET_SOCKINFO_EX_PCBTABLES);
	if (n < 0) {
		Printf("sys_net_get_sockinfo_ex() failed.(%d)\n", sys_net_errno);
		return (n);
	}
	if (n == 0) {
		Printf("netstat: No socket info\n");
		return (-1);
	}
	Printf("Proto Recv-Q Send-Q Local Address"
		"        Foreign Address      State\n");
	while (0 < n--) {
		switch(p->socket_type){
		case SOCK_STREAM:
			Printf("tcp  ");
			break;
		case SOCK_DGRAM:
			Printf("udp  ");
			break;
		case SOCK_RAW:
			Printf("ip   ");
			break;
		case SOCK_STREAM_P2P:
			Printf("tcp2p");
			break;
		case SOCK_DGRAM_P2P:
			Printf("udp2p");
			break;
		default:
			Printf("0x%x ", p->proto);
			break;
		}
		Printf(" %6d", p->recv_queue_length);
		Printf(" %6d", p->send_queue_length);
		if (inet_ntop(AF_INET, &p->local_adr, tmp, sizeof(tmp)) == NULL) {
			strcpy(tmp, "---");
		}
		if (!strcmp("0.0.0.0", tmp)) {
			strcpy(tmp, "*");
		}
		if (p->local_port != 0) 
		{
			char text[128];
			sprintf( text, " %s:%d", tmp, p->local_port );
			
			Printf( "%s", text );
			k = strlen( text );
		}
		else 
		{
			char text[128];
			sprintf( text, " %s:*", tmp );
			
			Printf( "%s", text );
			k = strlen( text );
		}
		Printf("%*s", 21 - k, "");
		if (inet_ntop(AF_INET, &p->remote_adr, tmp, sizeof(tmp)) == NULL) {
			strcpy(tmp, "---");
		}
		if (!strcmp("0.0.0.0", tmp)) {
			strcpy(tmp, "*");
		}
		if (p->remote_port != 0) 
		{
			char text[128];
			sprintf( text, " %s:%d", tmp, p->remote_port );

			Printf( "%s", text );
			k = strlen( text );
		}
		else
		{
			char text[128];
			sprintf( text, " %s:*", tmp );

			Printf( "%s", text );
			k = strlen( text );
		}

		// Socket type  
		if(SOCK_STREAM_P2P == p->socket_type
            || SOCK_DGRAM_P2P == p->socket_type)
		{
    		char text[128];
			if (p->remote_vport != 0) 
			{
				// UDPP2P: Remote virtual port number (network byte order)  
				// TCP over UDPP2P: Remote UDP port number (network byte order)  
    			sprintf( text, ":%d", ntohl(p->remote_vport) );
			}
			else 
			{
    			sprintf( text, ":*" );
			}

            Printf( "%s", text );
            k += strlen(text);
		}

		Printf("%*s", 21 - k, "");
		switch(p->state){
		case SYS_NET_STATE_UNKNOWN:
			Printf(" UNKNOWN");
			break;
		case SYS_NET_STATE_CLOSED:
			Printf(" CLOSED");
			break;
		case SYS_NET_STATE_CREATED:
			Printf(" CREATED");
			break;
		case SYS_NET_STATE_OPENED:
			Printf(" OPENED");
			break;
		case SYS_NET_STATE_LISTEN:
			Printf(" LISTEN");
			break;
		case SYS_NET_STATE_SYN_SENT:
			Printf(" SYN_SENT");
			break;
		case SYS_NET_STATE_SYN_RECEIVED:
			Printf(" SYN_RECEIVED");
			break;
		case SYS_NET_STATE_ESTABLISHED:
			Printf(" ESTABLISHED");
			break;
		case SYS_NET_STATE_FIN_WAIT_1:
			Printf(" FIN_WAIT_1");
			break;
		case SYS_NET_STATE_FIN_WAIT_2:
			Printf(" FIN_WAIT_2");
			break;
		case SYS_NET_STATE_CLOSE_WAIT:
			Printf(" CLOSE_WAIT");
			break;
		case SYS_NET_STATE_CLOSING:
			Printf(" CLOSING");
			break;
		case SYS_NET_STATE_LAST_ACK:
			Printf(" LAST_ACK");
			break;
		case SYS_NET_STATE_TIME_WAIT:
			Printf(" TIME_WAIT");
			break;
		default:
			Printf(" 0x%x", p->state);
			break;
		}
		Printf("\n");
		++p;
	}
	return (0);
}

int fiDeviceTcpIp::PrintInterfaceState()
{
	return sys_net_show_ifconfig();
}

void fiDeviceTcpIp::GetRemoteName(fiHandle,char *,int)
{
	// Unsupported for now
	Assertf(false, "fiDeviceTcpIp::GetRemoteName not yet supported on this platform");
}

void fiDeviceTcpIp::WaitForNetworkReady() {
	static bool ready;

	if (ready)
		return;

	ready = true;

	int ret;
	int state;

	// Wait 15 seconds usually, or an hour (!!) on bank builds because otherwise the game is probably going to break.
	int tries = 0;
	const int maxTries = __BANK? 7200 : 30;
	while (tries < 30) {
		ret = cellNetCtlGetState(&state);
		if (ret < 0) {
			Errorf("cellNetCtlGetState() failed(%x)", ret);
			return;
		}
		if (state == CELL_NET_CTL_STATE_IPObtained) {
			break;
		}
		sysIpcSleep(500);
		++tries;
		if ((tries % 20) == 0)
			Warningf("...spent %d seconds trying to obtain an IP address, still no luck...",tries>>1);
	}
	if (tries != maxTries)
		Displayf("...got ip address in %.1f seconds",tries/2.0f);
	else
		Errorf("**** unable to obtain an ip address");
}


void fiDeviceTcpIp::ShutdownClass() {
	ShutdownWinSock();
}


fiHandle fiDeviceTcpIp::Open(const char *filename, bool /*readOnly*/) const {
	if (strncmp(filename,"tcpip:",6))
		return fiHandleInvalid;
	filename += 6;
	int port = 0;
	while (*filename && *filename != ':')
		port = port * 10 + (*filename++)-'0';
	if (!*filename)
		return fiHandleInvalid;
	else
		++filename;
	if (!strcasecmp(filename,"LISTEN")) {
		fiHandle listener = Listen(port,1);
		if (fiIsValidHandle(listener)) {
			Displayf("Waiting for connection on port %d...",port);
			fiHandle data = Pickup(listener);
			Close(listener);
			return data;
		}
		else {
			Errorf("Unable to establish server on port %d, already in use?",port);
			return listener;
		}
	}
	else
		return Connect(filename,port);
}


fiHandle fiDeviceTcpIp::Create(const char * filename) const {
	return Open(filename,false);
}


int fiDeviceTcpIp::StaticWrite(fiHandle fd,const void *buffer,int count) {
	int result = send((SOCKET)fd,(const char*)buffer,count,0);
	if (result < 0)
	{
		result = -1;
		switch(sys_net_errno)
		{
		case SYS_NET_EWOULDBLOCK:
			result = 0;
			break;
		case SYS_NET_EINPROGRESS:
			// ignore
			break;
		default:
			sysNetError("fiDeviceTcpIp::StaticWrite", sys_net_errno);
			break;
		}
	}
	return result;
}


int fiDeviceTcpIp::StaticRead(fiHandle fd,void *buffer,int count) {
	int result = recv((SOCKET)fd,(char*)buffer,count,0);
	if (result < 0)
	{
		result = -1;
		switch(sys_net_errno)
		{
		case SYS_NET_EWOULDBLOCK:
			result = 0;
			break;
		case SYS_NET_EINPROGRESS:
			// ignore
			break;
		default:
			sysNetError("fiDeviceTcpIp::StaticRead", sys_net_errno);
			break;
		}
	}
	return result;
}


int fiDeviceTcpIp::GetReadCount(fiHandle fd) {
	char buffer[256];
	int result = recv((SOCKET)fd,buffer,sizeof(buffer),MSG_DONTWAIT | MSG_PEEK);
	if (result < 0)
	{
		result = -1;
		switch(sys_net_errno)
		{
		case SYS_NET_EWOULDBLOCK:
		case SYS_NET_EINPROGRESS:
			// ignore
			break;
		default:
			sysNetError("fiDeviceTcpIp::ReadCount", sys_net_errno);
			break;
		}
	}
	return (result>0)? result : 0;
}

void fiDeviceTcpIp::SetBlocking(fiHandle handle, bool blocking)
{
	int nonblocking = blocking ? 0 : 1;
	setsockopt((int)handle, SOL_SOCKET, SO_NBIO, (char*)&nonblocking, sizeof(nonblocking));
}


int fiDeviceTcpIp::Seek(fiHandle /*handle*/,int /*offset*/,fiSeekWhence /*whence*/) const {
	return -1;
}


u64 fiDeviceTcpIp::Seek64(fiHandle /*handle*/,s64 /*offset*/,fiSeekWhence /*whence*/) const {
	return (u64)(s64)-1;
}


u64 fiDeviceTcpIp::GetFileTime(const char*) const {
	return 0;
}


bool fiDeviceTcpIp::SetFileTime(const char*,u64) const {
	return false;
}


int fiDeviceTcpIp::Close(fiHandle handle) const {
	return socketclose((int)handle);
}


static bool GetAddress(const char *name,int port,struct sockaddr_in *sa) {
	struct hostent *hp;
	memset(sa,0,sizeof(*sa));
	if (!name || (sa->sin_addr.s_addr = inet_addr(name)) != ~0U)
		sa->sin_family = AF_INET;
	else if ((hp = gethostbyname(name)) != 0) {
		sa->sin_family = hp->h_addrtype;
		sysMemCpy(&sa->sin_addr,hp->h_addr, hp->h_length);
	}
	else
		// If you're on Xbox and running .net, did you remember to copy rfs.dat
		// into your project-specific executable subdirectory?
		return false;

	sa->sin_port = (unsigned short) htons((unsigned short)port);

	return true;
}


fiHandle fiDeviceTcpIp::Connect(const char *address,int port) {
	WaitForNetworkReady();

	struct sockaddr_in sa;
	SOCKET s;

	if (!GetAddress(address,port,&sa))
		return fiHandleInvalid;

	if ((s = socket(AF_INET,SOCK_STREAM,0)) < 0)
		return fiHandleInvalid;

#if __XENON
	// New behavior in Feb XeDK: Local port doesn't automatically reset to 1024
	// when stopping a debug session, which causes the PC-side server to get confused.
	struct sockaddr_in local;
	memset(&local,0,sizeof(local));
	local.sin_family = AF_INET;
	local.sin_port = htons(1024);
	bind(s,(struct sockaddr*)&local,sizeof(local));
#endif

	if (connect(s,(sockaddr*)&sa,sizeof(sa)) < 0) {
		// Errorf("Connect failed: %d(%x)",sys_net_errno,sys_net_errno);
		socketclose(s);
		return fiHandleInvalid;
	}

	int noNagle = 1;
	socklen_t len = sizeof(int);
	if (setsockopt(s, IPPROTO_TCP, TCP_NODELAY, &noNagle, len) < 0) {
		socketclose(s);
		return fiHandleInvalid;
	}

	return (fiHandle) s;
}


fiHandle fiDeviceTcpIp::Listen(int port,int maxIncoming,const char *) {
	SOCKET s;
	sockaddr_in sa;

	if (!GetAddress(0, port, &sa))
		return fiHandleInvalid;

	if ((s = socket(AF_INET,SOCK_STREAM,0)) < 0)
		return fiHandleInvalid;

	if (bind(s,(struct sockaddr*)&sa,sizeof(sa)) < 0) {
		socketclose(s);
		return fiHandleInvalid;
	}

	if (listen(s, maxIncoming) < 0) {
		socketclose(s);
		return fiHandleInvalid;
	}

	return (fiHandle) s;
}


fiHandle fiDeviceTcpIp::Pickup(fiHandle listenSocket) {
	struct sockaddr_in isa;
	socklen_t i;
	int c;

	i = sizeof(isa);
	getsockname((int)listenSocket,(sockaddr*)&isa,&i);

	// Displayf("Waiting for client connection...");
	if ((c = accept((int)listenSocket,(sockaddr*)&isa,&i)) < 0)
		return fiHandleInvalid;

	return (fiHandle) c;
}

}	// namespace rage

#endif	// __WIN32
