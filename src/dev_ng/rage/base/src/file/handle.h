// 
// file/handle.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_HANDLE_H
#define FILE_HANDLE_H

#if __WIN32
typedef void *fiHandle;
#define HANDLEFMT	"p"
#elif RSG_PS3
typedef int fiHandle;
#define HANDLEFMT	"x"
#elif RSG_ORBIS
typedef long fiHandle;		// Needs to be at least as wide as a pointer
#define HANDLEFMT	"lx"
#else
typedef struct fiHandle__ *fiHandle;
#define HANDLEFMT	"p"
#endif

#define fiHandleInvalid	((fiHandle)-1)

inline bool fiIsValidHandle(fiHandle h)
{ 
	return h != fiHandleInvalid;
}

#endif
