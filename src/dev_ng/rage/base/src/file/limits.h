// 
// file/limits.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FILE_LIMITS_H
#define FILE_LIMITS_H

namespace rage {

// Maximum length of a path, including the terminating NUL character.
const int RAGE_MAX_PATH = 256;

};

#endif
