//
// file/default_paths.h
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#ifndef FILE_DEFAULT_PATHS_H
#define FILE_DEFAULT_PATHS_H

#if defined(RS_USING_PROP_SHEET)
#ifndef RS_PROJECT
#error RS_PROJECT isn't defined, please check the prop sheet
#endif
#ifndef RS_BRANCHSUFFIX
#error RS_BRANCHSUFFIX isn't defined, please check the prop sheet
#endif
#ifndef RS_PROJROOT
#error RS_PROJROOT isn't defined, please check the prop sheet
#endif
#ifndef RS_PROJDIR
#error RS_PROJDIR isn't defined, please check the prop sheet
#endif
#ifndef RS_BUILDROOT
#error RS_BUILDROOT isn't defined, please check the prop sheet
#endif
#ifndef RS_BUILDBRANCH
#error RS_BUILDBRANCH isn't defined, please check the prop sheet
#endif
#ifndef RS_TITLE_UPDATE
#error RS_TITLE_UPDATE isn't defined, please check the prop sheet
#endif
#ifndef RS_CODEBRANCH
#error RS_CODEBRANCH isn't defined, please check the prop sheet
#endif
#ifndef RS_TOOLSROOT
#error RS_TOOLSROOT isn't defined, please check the prop sheet
#endif

#define RS_ASSETS		"assets:"

#else	//defined(RS_USING_PROP_SHEET)

#define RS_PROJECT		"gta5"
#define RS_BRANCHSUFFIX	"_ng"
#define RS_PROJROOT		"x:/gta5"
#define RS_PROJDIR		"X:/gta5/src/dev_ng/game/VS_Project"
#define RS_BUILDROOT	"x:/gta5/build"
#define RS_BUILDBRANCH	"x:/gta5/build/dev_ng"
#define RS_TITLE_UPDATE	"x:/gta5/titleupdate/dev_ng"
#define RS_CODEBRANCH	"x:/gta5/src/dev_temp"
#define RS_ASSETS		"assets:"
#define RS_TOOLSROOT	"x:/gta5/tools_ng"

#endif	//defined(RS_USING_PROP_SHEET)

#endif
