//
// file/device.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef FILE_DEVICE_H
#define FILE_DEVICE_H

#include <stddef.h>		// for size_t

#include "atl/array.h"
#include "atl/map.h"
#include "file/handle.h"
#include "file/limits.h"


#define __DEVPROF		(!__FINAL)


namespace rage {

struct sysIpcSemaTag;
struct datResourceInfo;
class datGrowBuffer;
struct Mount_t;

// Define the maximum number of extra mounts that can be mounted by the game.
// This defines maximum number of DLC packs we support
enum { MAX_EXTRA_MOUNTS = 256 };

/*
PURPOSE
Enumerant passed to fiDevice::Seek which determines the origin (start point)
of the seek operation
*/
enum fiSeekWhence 
{ 
	seekSet, // Position is relative to start of file
	seekCur, // Position is relative to current file position (can be negative)
	seekEnd  // Position is relative to end of file. Reliable operation is only guaranteed if pos is zero or negative
};

/*
PURPOSE
Structure returned by FindFileBegin / FindFileNext
*/
struct fiFindData {
	char m_Name[RAGE_MAX_PATH];	// Name of the file
	u64 m_Size;			// Size of the file
	u64 m_LastWriteTime;// Last time the file was written
	u32 m_Attributes;	// Bitfield using the FILE_ATTRIBUTE_... values
};

#if __DEVPROF
struct fiDeviceProfiler {
	virtual void OnOpen(fiHandle /*handle*/, const char * /*file*/) {}
	virtual void OnRead(fiHandle /*handle*/, size_t /*size*/, u64 /*offset*/) {}
	virtual void OnReadEnd(fiHandle /*handle*/) {}
	virtual void OnWrite(fiHandle /*handle*/, size_t /*size*/) {}
	virtual void OnSeek(fiHandle /*handle*/, u64 /*offset*/) {}
	virtual void OnClose(fiHandle /*handle*/) {}

	virtual ~fiDeviceProfiler() {}
};
#endif // __DEVPROF

/*
PURPOSE
Implements a device driver abstraction for the filesystem so that higher level
code can access persistent storage portably.  All filenames are assumed to start
with some sort of device id which is used to route the file to the approperiate
device.  This may be something like cdrom0: or even a drive letter like D:\;
furthermore, we will tend to fall back on fiDeviceLocal if no other device
match is found.
<FLAG Component>
*/
class fiDevice {
public:
	enum RootDeviceId {
		UNKNOWN,			// Need to query the parent device to find out
		OPTICAL,
		HARDDRIVE,
		USB,
		NETWORK,
		MEMORY,
	};

	// These numbers are currently approximate.
#if __XENON
	static const u32 LAYER1_LSN = 1783936;
#elif __PS3 || RSG_ORBIS
	static const u32 LAYER1_LSN = 13107200;
#else
	static const u32 LAYER1_LSN = 0;		// Unknown.
#endif
	// Fake offset applied to all sector addresses on the hard drive
	// Small enough to avoid problems with sign changes on 32bit arithmetic.
	static const u32 HARDDRIVE_LSN = 0x40000000;

	// Callback on fatal read errors; system will retry several times before calling this function.
	// The function is not expected to return, although if it does, we will simply continue to retry
	// the read indefinitely with no further notifications.  On Xbox 360, you should call XShowDirtyDiscErrorUI,
	// with an appropriate user index (good luck with that), and heed this important caveat: "Titles should 
	// continue rendering while the dialog box is displayed; to do so, a title must call this function from 
	// a thread other than the thread responsible for rendering."
	static void (*sm_FatalReadError)(void);
#if !__FINAL
	// Set this to nonzero and it will inject a read error once the counter reaches zero again.
	static u32 sm_InjectReadError;		
#endif

	fiDevice() { }

	virtual ~fiDevice() { }

	static void InitClass();

	// PURPOSE:	Attempt to open a file on this device
	// PARAMS:	filename - Name of file to open (including device id)
	//			readOnly - True if file will be read-only, else false.
	// RETURNS:	Non-negative handle value on success, negative value on error.
	// NOTES:	If a file is read-only, we attempt to open in a sharing-friendly
	//			manner.  If this function returns a value file handle, you
	//			must eventually call Close on this device with that handle or
	//			else you would suffer a resource leak.
	virtual fiHandle Open(const char *filename,bool readOnly) const = 0;

	// PURPOSE:	Attempt to open a file on this device for bulk read
	// PARAMS:	filename - Name of file to open (including device id)
	//			outBias - Reference to a u64 containing a bias you should add
	//				to any ReadBulk call using this handle.  Allows archives
	//				to avoid having to maintain any internal state when using
	//				uncompressed files.
	// RETURNS:	Non-negative handle value on success, negative value on error.
	// NOTES:	The handle returned by this function can only be used with
	//			ReadBulk and CloseBulk.
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;

	// PURPOSE:	Attempt to open a drm secured file on this device for bulk read
	// PARAMS:	filename - Name of file to open (including device id)
	//			outBias - Reference to a u64 containing a bias you should add
	//				to any ReadBulk call using this handle.  Allows archives
	//				to avoid having to maintain any internal state when using
	//				uncompressed files.
	//			pDrmKey - pointer to the drm key we will attempt to use to decrypt/unlock this file
	//				can be passed NULL in which case OpenBulk is called
	// RETURNS:	Non-negative handle value on success, negative value on error.
	// NOTES:	The handle returned by this function can only be used with
	//			ReadBulk and CloseBulk.
	virtual fiHandle OpenBulkDrm(const char *filename,u64 &outBias,const void * pDrmKey) const;

	// PURPOSE:	Attempt to open a file on this device for bulk write
	// PARAMS:	filename - Name of file to open (including device id)
	// RETURNS:	Non-negative handle value on success, negative value on error.
	// NOTES:	The handle returned by this function can only be used with
	//			ReadBulk, WriteBulk, and CloseBulk.
	virtual fiHandle CreateBulk(const char *filename) const;

	// PURPOSE:	Attempt to create a file on this device
	// PARAMS:	filename - Name of file to open (including device id)
	//			readOnly - True if file will be read-only, else false.
	// RETURNS:	Non-negative handle value on success, negative value on error.
	//	NOTES:	If this function returns a value file handle, you
	//			must eventually call Close on this device with that handle or
	//			else you would suffer a resource leak.
	virtual fiHandle Create(const char *filename) const = 0;

	// PURPOSE:	Read binary data from file
	// PARAMS:	handle - Handle for file (must have been opened or created with
	//				the same device)
	//			outBuffer - Destination to receive data that is read
	//			bufferSize - Maximum amount of data to read
	// RETURNS:	Non-negative integer on success representing number of bytes
	//			of data read (zero typically means end-of-file).  Negative integer
	//			indicates an error.
	// NOTES:	No translation of any kind is done on the data.
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const = 0;

	// PURPOSE:	Seek and read binary data from file
	// PARAMS:	handle - Handle for file (must have been opened or created with
	//				the same device)
	//			offset - File offset, must be multiple of block size (2048 is safest)
	//				Note that you should add any bias returned by OpenBulk in here yourself.
	//			outBuffer - Destination to receive data that is read (should be aligned
	//				to cache line for maximum speed)
	//			bufferSize - Maximum amount of data to read, must be multiple of
	//				block size (again, 2048 is safest)
	// RETURNS:	Non-negative integer on success representing number of bytes
	//			of data read (zero typically means end-of-file).  Negative integer
	//			indicates an error.
	// NOTES:	No translation of any kind is done on the data.  The data is not
	//			cached. 
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;

	// PURPOSE:	Seek and write binary data to file
	// PARAMS:	handle - Handle for file (must have been opened or created with
	//				the same device)
	//			offset - File offset, must be multiple of block size (2048 is safest)
	//				Note that you should add any bias returned by OpenBulk in here yourself.
	//			inBuffer - Source for data that is written (should be aligned
	//				to cache line for maximum speed)
	//			bufferSize - Maximum amount of data to write, must be multiple of
	//				block size (again, 2048 is safest)
	// RETURNS:	Non-negative integer on success representing number of bytes
	//			of data written (zero typically means disk full).  Negative integer
	//			indicates an error.
	// NOTES:	No translation of any kind is done on the data.  The data is not
	//			cached.
	virtual int WriteBulk(fiHandle handle,u64 offset,const void *inBuffer,int bufferSize) const;

	// PURPOSE:	Write binary data to file
	// PARAMS:	handle - Handle for file (must have been opened or created with
	//				the same device)
	//			buffer - Source buffer for data to write
	//			bufferSize - Maximum amount of data to read
	// RETURNS:	Non-negative integer on success representing number of bytes
	//			of data written (zero typically means media full).  Negative integer
	//			indicates an error.
	// NOTES:	No translation of any kind is done on the data.
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const = 0;

	// PURPOSE:	Seek to a location in file
	// PARAMS:	handle - Handle for file (must have been opened or created with
	//				the same device)
	//			offset - Offset from 'whence' to seek to
	//			whence - Origin for seek operation (as per stdio fseek)
	// RETURNS: Lower 32 bits of new file position, or -1 on error
	// NOTES:	Not all devices can implement seek (pipes, etc)
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const = 0;

	// PURPOSE:	Seek to a location in file
	// PARAMS:	handle - Handle for file (must have been opened or created with
	//				the same device)
	//			offset - Offset from 'whence' to seek to
	//			whence - Origin for seek operation (as per stdio fseek)
	// RETURNS: New file position, or 0xFFFF FFFF FFFF FFFF on error.
	// NOTES:	Not all devices can implement seek (pipes, etc)
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const = 0;

	// PURPOSE:	Close a file
	// PARAMS:	handle - Handle of file to close (must have been opened or created
	// 				with the same device).
	// NOTES:	You must eventually Close any handle successfully returned by an Open
	//			or Create call or else you will have a resource leak.
	// RETURNS:	zero if the call succeeded, non-zero if it failed
	virtual int Close(fiHandle handle) const = 0;

	// PURPOSE:	Close a file opened via OpenBulk
	// PARAMS:	handle - Handle of file to close (must have been opened or created
	// 				with the same device via OpenBulk
	// NOTES:	You must eventually Close any handle successfully returned by an Open
	//			or Create call or else you will have a resource leak.
	// RETURNS:	zero if the call succeeded, non-zero if it failed
	virtual int CloseBulk(fiHandle handle) const;

	// PURPOSE:	Determine size of file
	// PARAMS:	handle - Handle of file to determine size of (must have been opened or
	//				created with the same device)
	// RETURNS:	Size of the file
	// NOTES:	Base class implements this as three calls to Seek, subclasses that may know
	//			the size of the file ahead of time are encouraged to improve upon this.
	virtual int Size(fiHandle handle) const;

	// PURPOSE:	Determine size of file
	// PARAMS:	handle - Handle of file to determine size of (must have been opened or
	//				created with the same device)
	// RETURNS:	Size of the file
	// NOTES:	Base class implements this as three calls to Seek, subclasses that may know
	//			the size of the file ahead of time are encouraged to improve upon this.
	virtual u64 Size64(fiHandle handle) const;

	// PURPOSE:	Flushes all file data
	// PARAMS:	handle - Handle of file to flush (must have been opened or created with the same device)
	// RETURNS:	Zero on success, negative value on error
	// NOTES:	This is intended for removable media like memory cards.
	virtual int Flush(fiHandle handle) const;

	// PURPOSE:	Delete a file from device
	// PARAMS:	filename - Name of file to delete
	// RETURNS:	True on success (file deleted), or false on failure (file not found,
	//			marked read-only, etc)
	virtual bool Delete(const char *filename) const;

	// PURPOSE:	Rename a file on device.  Behavior is undefined if you attempt to change
	//			its directory during a rename.  New name must not exist, delete any file
	//			with the new name first if necessary.
	// PARAMS:	from - Original filename
	//			to - New name for file
	// RETURNS:	True on success (file renamed), or false on failure (file not found, etc)
	virtual bool Rename(const char *from,const char *to) const;

	// PURPOSE:	Make a directory on device
	// PARAMS:	pathname - Name of directory to create
	// RETURNS:	True on success, or false on failure (directory already existed, etc)
	virtual bool MakeDirectory(const char *pathname) const;

	// PURPOSE:	Remove an empty directory on device.  Lame is name to avoid #define conflict with Windows SDK
	// PARAMS:	pathname - Name of directory to remove
	// RETURNS:	True on success, or false on failure (directory not empty, etc)
	virtual bool UnmakeDirectory(const char *pathname) const;

	// PURPOSE: Sanitize the internal buffers. Prior to this, they're left plaintext in memory
	// PARAMS : handle - Handle to sanitize
	// RETURNS: None
	virtual void Sanitize(fiHandle handle) const;

	// PURPOSE: Remove a directory and optionally all of its contents from a device
	// PARAMS: pathname - Name of the directory to remove
	//			deleteContents - auto delete everything inside the directory as well
	// RETURNS: True on success, false on failure (invalid path, deleteContents is false 
	//			but directory isn't empty, etc.)
	static bool DeleteDirectory(const char *pathname, bool deleteContents=false, bool ignoreReadOnly=true);

#if __TOOL
	// PURPOSE: Find File in a directory by name and other options
	// PARAMS:	path - Name of the directory to look in
	//			filename - the filename to check against
	//			options - bit flags for additional options
	// RETURNS: full filepath on success, NULL on failure
	enum eFindFileOptions
	{
		FINDFILE_ALL	=	0,
		FINDFILE_TGA	=	1 << 1,
		FINDFILE_JPG	=	1 << 2,
		FINDFILE_BMP	=	1 << 3,
		FINDFILE_DDS	=	1 << 4,
		FINDFILE_IMAGES	=	FINDFILE_TGA|FINDFILE_JPG|FINDFILE_BMP|FINDFILE_DDS,
	};
	const char *FindFile(const char *path, const char *filename, u32 options=FINDFILE_ALL) const;
#endif
	// PURPOSE: Copy a directory and all its contents
	// PARAMS: fromDirectory - Name of the directory to copy from
	//		   toDirectory - Name of the directory to copy to
	// RETURNS: True on success, false on failure (invalid path)
	static bool CopyDirectory(const char *fromDirectory, const char *toDirectory);

	// PURPOSE: Copy a file
	// PARAMS: fromFile - Name of the file to copy from
	//		   toFile - Name of the file to copy to
	// RETURNS: True on success, false on failure (invalid path, insufficient space, etc)
	static bool CopySingleFile(const char *fromFile,const char *toFile);

	// PURPOSE: Returns whether or not the specified mount path exists.
	// PARAMS: mountPath - path to check
	// RETURNS: True if it exists, false if not.
	static bool IsAnythingMountedAtMountPoint(const char* mountPath);

	// PURPOSE:	Determine size of file
	// PARAMS:	filename - Name of file to request timestamp for
	// RETURNS:	Size of the file
	// NOTES:	Base class implements this as three calls to Seek, subclasses that may know
	//			the size of the file ahead of time are encouraged to improve upon this.
	virtual u64 GetFileSize(const char *filename) const;

	// PURPOSE:	Return time-of-date information for a file
	// PARAMS:	filename - Name of file to request timestamp for
	// RETURNS:	Large integer (in unspecified units) which represents the file time.
	//			Will return 0 if a file did not exist.  If a filetime <I>fa</I> is larger than
	//			another filetime <I>fb</I> then <I>fa</I> is newer than <I>fb</I>.
	virtual u64 GetFileTime(const char *filename) const = 0;

	// PURPOSE:	Set time-of-date information for a file
	// PARAMS:	filename - Name of file to request timestamp for
	//			timestamp - Timestamp to set
	// RETURNS:	True on success, else false on failure.
	// NOTES:	The only portable way to set a filetime is from a previous GetFileTime result.
	virtual bool SetFileTime(const char *filename,u64 timestamp) const = 0;

	// PURPOSE:	Iterate over all files in a directory
	// PARAMS:	directoryName - Name of the directory to search (no wildcard necessary
	//				or supported; do filtering yourself in higher-level code).
	//			outData - Info for first matching file (if return value was not -1)
	// RETURNS:	A "find handle" that is used to identify this search so that more
	//			that one can be in progress at a time, or -1 if we were unable to
	//			find any matches.  Must eventually call FindFileEnd on this handle
	//			if it did not return -1.
	// NOTES:	Some platforms will return "." and ".." and some will not, so make
	//			sure you handle them properly.
	virtual fiHandle FindFileBegin(const char *directoryName,fiFindData &outData) const;

	// PURPOSE:	Continue a search begun with FindFileBegin
	// PARAMS:	handle - Find handle returned by FindFileBegin
	//			outData - Info for next match file (if return value was not false)
	// RETURNS:	True on success (outData is valid), or false if there were no more matches
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;

	// PURPOSE:	Close a search begun with FindFileBegin
	// PARAMS:	handle - Find handle to close
	// RETURNS: Zero if the call succeeded, non-zero otherwise.
	virtual int FindFileEnd(fiHandle handle) const;

	// PURPOSE: Get the actual low-level device that drives I/O on this device.
	virtual const fiDevice *GetLowLevelDevice() const	{ return this; }

	// RETURNS: The name of this file according to the "low level" device
	virtual char* FixRelativeName(char* dest, int destSize, const char* src) const;

	// PURPOSE:	Sets the end of the file to the current file position.
	// PARAMS:	handle - File handle to set the eof marker to
	// RETURNS: Zero if the call succeeded, non-zero otherwise.
	virtual bool SetEndOfFile(fiHandle handle) const;

	// PURPOSE:	Retrieves file attributes
	// PARAMS:	filename - name of file to retrieve attributes
	// RETURNS:	File attributes (see FILE_ATTRIBUTE_...) or FILE_ATTRIBUTE_INVALID (~0U) if file doesn't exist.
	virtual u32 GetAttributes(const char *filename) const;

	// PURPOSE:	Tell the file system to prefetch information about this directory. On some devices (currently only fiDeviceRemote),
	//          this will speed up subsequent calls to get more information about files within this particular directory.
	// PARAMS:	filename - name of directory to parse
	// RETURNS:	True if successful, false otherwise.
	virtual bool PrefetchDir(const char * /*directory*/) const { return true; }

	// PURPOSE:	Sets file attributes
	// PARAMS:	filename - name of file to set attributes for
	//			attributes - new attributes for file
	// RETURNS:	True on success, false on failure.
	virtual bool SetAttributes(const char *filename,u32 attributes) const;

	// PURPOSE:	Lock the filesystem (for serialization).
	// NOTES:	Only necessary on PS2 when doing file access with other Sony routines
	//			or the CDVD library.  No operation on other platforms.
	static void Lock();

	// PURPOSE:	Unlock the filesystem (for serialization).
	// NOTES:	Only necessary on PS2 when doing file access with other Sony routines
	//			or the CDVD library.  No operation on other platforms.
	static void Unlock();

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const = 0;

	// PURPOSE:	Read data from file, with retry until error
	// PARAMS:	handle - handle of file to read
	//			outBuffer - Destination buffer for read data
	//			size - Number of bytes to read
	// RETURNS:	True if exactly <I>size</I> bytes could be read,
	//			false if Read returned negative value at any time.
	// NOTES:	This is useful for things like named pipes and TCP connections
	//			where data sometimes arrives in smaller pieces than we expect.
	virtual bool SafeRead(fiHandle handle,void *outBuffer,int size) const;

	// PURPOSE:	Write data to file, with retry until error
	// PARAMS:	handle - handle of file to read
	//			buffer - Source buffer for written data
	//			size - Number of bytes to write
	// RETURNS:	True if exactly <I>size</I> bytes could be written,
	//			false if Write returned negative value at any time.
	// NOTES:	This is useful for things like named pipes and TCP connections
	//			where data is sometimes consumed in smaller pieces than we expect.
	virtual bool SafeWrite(fiHandle handle,const void *buffer,int size) const;

	// PURPOSE:	Retrieve resource information for a file.  It only works files that
	//			have a standard resource header.
	// PARAMS:	name - Name of file to query
	//			outHeader - If return value is nonzero, contains header information
	// RETURNS:	Nonzero if file exists and has a standard resource header, else zero.
	//			A return of -1 means it's a valid resource but the version number
	//			is unavailable.
	// NOTES:	For raw files, it reads the header (which would cause tons of seeking
	//			on final media).  For archived files, it (will eventually) use the
	//			metadata present in the directory structure.  
	virtual int GetResourceInfo(const char *name,datResourceInfo &outHeader) const;

	// PURPOSE: Return the AES key ID used to decrypt the encrypted files on this device.
	virtual u32 GetEncryptionKey() const	{ return 0; }

	// PURPOSE:	Convenience function which compares to files.
	// PARAMS:	original - Original file (source data)
	//			dependent - Dependent file (data derived from and dependent
	//				upon <I>original</I>)
	// RETURNS:	True if <I>original</I> is newer than <I>dependent</I> or
	//			<I>original</I> does not exist while <I>dependent</I> does.
	//			Otherwise returns false (including when neither file exists)
	static bool IsOutOfDate(const char *original,const char *dependent);

	// PURPOSE:	Returns appropriate device for file
	// PARAMS:	filename - Name of file, including device id
	//			readOnly - True if file is being opened for readonly access
	// RETURNS:	Reference to appropriate fiDevice subclass, or NULL if
	//			no device found (bad path, or device is mounted readonly)
	static const fiDevice* GetDevice(const char *filename,bool readOnly = true);

	// PURPOSE: Enumerate all files for a given mount point through a user-supplied callback
	// PARAMS:	filename - Name of file, including device id
	//			callback - Callback function to invoke
	//			userArg - closure for user callback
	//			bCheckAllExtraMountDevices - extra mount points are those whose name begins with "dlc".
	//											An extra mount point can have more than one device associated with it.
	//											Calling this function with bCheckAllExtraMountDevices=true will enumerate 
	//												the files on all devices of an extra mount point.
	//											Calling this function with bCheckAllExtraMountDevices=false will only enumerate 
	//												the files on the device that was last added to the mount point.
	// RETURNS:	Total number of files enumerated (ie number of times callback was invoked)
	static int EnumFiles(const char *filename,void (*callback)(const fiFindData &data,void *userArg),void *userArg,bool bCheckAllExtraMountDevices);

	// PURPOSE:	Normalize a filename in-place
	// NOTES:	Converts all letters to lower-case, and changes all slashes
	//			to be appropriate for the platform's native filesystem.
	static void Normalize(char *buffer);

	// PURPOSE:	Establish a mount point in the global file namespace
	// PARAMS:	sourcePath - Pathname of the directory to mount (or "default")
	//			device - Reference to device to associate with this source path
	//			readonly - True if this device is mounted for read-only access
	// RETURNS:	True on success, false on failure (possibly already mounted)
	// NOTES:	The sourcePath "default" is special, and determines the device
	//			to use when the supplied filename doesn't match any mount point.
	//			If no default device is mounted, GetDevice will throw an assert/
	//			By default, the local file device is mounted by default but if RFS
	//			is enabled the startup code will use that instead by default,
	//			except for the D: drive which is still mounted locally.
	static bool Mount(const char *sourcePath,const fiDevice &device,bool readonly);

	// PURPOSE:	Re-establish a mount point in the global file namespace for an existing device.
	// PARAMS:	newdevice - Reference to device to associate with this source path
	//			olddevice - Reference to device to be replaced with this source path and new device
	//			readonly - True if this device is mounted for read-only access
	// RETURNS:	True on success, false on failure (possibly already mounted)
	// NOTES:	The sourcePath "default" is special, and determines the device
	//			to use when the supplied filename doesn't match any mount point.
	//			If no default device is mounted, GetDevice will throw an assert/
	//			By default, the local file device is mounted by default but if RFS
	//			is enabled the startup code will use that instead by default,
	//			except for the D: drive which is still mounted locally.
	static bool Remount(const fiDevice &newdevice, const fiDevice &olddevice, bool readonly);

	// PURPOSE:	Remove a previously established mount point
	// PARAMS:	sourcePath - Pathname of directory to unmount (as per Mount)
	// RETURNS:	True on success, false on failure (didn't match anything already mounted,
	//			or there are open files on the mount)
	static bool Unmount(const char *sourcePath);

	// PURPOSE:	Remove a previously mounted device
	// PARAMS:	device - Pathname of directory to unmount (as per Mount)
	// RETURNS:	True on success, false on failure (didn't match anything already mounted,
	//			or there are open files on the mount)
	// NOTES:	Unmount does not delete the device, it just removes it from the 
	static bool Unmount(const fiDevice &olddevice);

	// PURPOSE:	Default implementation of GetDevice function
	// PARAMS:	filename - Name of file, including device id
	//			readOnly - True if file is being opened for readonly access
	// RETURNS:	Reference to appropriate fiDevice subclass.
	// NOTES:	This is used by RFS support to patch into the device chain.
	//			We could generalize this to some sort of device list but that
	//			may be overkill for our needs.
	static const fiDevice& DefaultGetDevice(const char *filename,bool readonly);

	// PURPOSE:	Create a filename that can be used to reference a file in memory
	// PARAMS:	dest - Destination buffer
	//			destSize - sizeof(dest)
	//			data - Pointer to the file data
	//			dataSize - size of the file data
	//			freeOnClose - True if we should reclaim the memory with delete[] when the file is closed.
	//			comment - Arbitrary comment, the original filename would make most sense here
	static void MakeMemoryFileName(char *dest,int destSize,const void *data,size_t dataSize,bool freeOnClose,const char *comment);

	// PURPOSE: Create a filename that can be used to reference a growable buffer
	// PARAMS:	dest - Destination buffer
	//			destSize - sizeof(dest)
	//			gb - The growbuffer that contains the file data
	static void MakeGrowBufferFileName(char* dest, const int destSize, datGrowBuffer* gb);

	// PURPOSE:	Specify the number of archives that can be mounted. This needs to be called before the first
	//			archive gets mounted.
	// PARAMS:	mounts - maximum number of archives that can be mounted.
	static void SetMaxMounts(int mounts) { sm_MaxMounts = mounts; }

	// PURPOSE:	Specify the number of extra mounts that can be mounted. This needs to be called before the first
	//			extra gets mounted.
	// PARAMS:	mounts - maximum number of extra mounts that can be mounted.
	static void SetMaxExtraMounts(int mounts) { sm_MaxExtraMounts = mounts; }


	static bool GetIsReadOnly(const char* device);
#if __DEVPROF
	static void SetDeviceProfiler(fiDeviceProfiler *profiler)		{ sm_DeviceProfiler = profiler; }
#endif // __DEVPROF
	// This is a portable version of Win32 SYSTEMTIME structure.  (Field names are kept identical for clarity)
	struct SystemTime {
		u16 wYear;			// Two digit year
		u16 wMonth;			// 1=Jan, 2=Feb, etc
		u16 wDayOfWeek;		// 0=Sun, 1=Mon, etc.
		u16 wDay;			// Day of month (1, 2, 3..)
		u16 wHour;
		u16 wMinute;
		u16 wSecond;
		u16 wMilliseconds;
	};

	// PURPOSE:	Convert a file time (u64, equivalent to a Win32 FILETIME) into human-readable form.
	// NOTES:	Assumes input is in UTC, and output compensates for current local time zone.
	// wDayOfWeek is WIN32 only, always 0 on other platforms
	static void ConvertFileTimeToSystemTime(u64 inTime,SystemTime &outTime);

	//Gets the local time from the console
	// NOTES:
	// wDayOfWeek is WIN32 only, always 0 on other platforms
	static void GetLocalSystemTime(SystemTime &outTime);

	// Gets the UTC time from the console
	static void GetSystemTimeUtc(SystemTime &outTime);

	// PURPOSE: Enables or disables ability to look through all devices mounted to a path and select
	//			the one containing the file with newest timestamp
	static void SetFindNewest(bool findNewest);

	// RETURNS: Bulk offset of file, if found (can skip OpenBulk and just call ReadBulk directly)
	virtual u64 GetBulkOffset(const char*) const { return 0; }

	// RETURNS:	Returns a sort key for file on device. For DVD this would be sector address.
	//			If it's HARDDRIVE_LSN, we don't know anything about it, or it's on a hard drive.
	virtual u32 GetPhysicalSortKey(const char*) const { return HARDDRIVE_LSN; }

	virtual bool IsRpf() const { return false; }
	//many devices do not override IsRpf, even when they mask an internal RPF
	//IsMaskingAnRpf is specifically for relative devices that hide RPFs 
	// internally, see url:bugstar:4201722 
	virtual bool IsMaskingAnRpf () const { return false; }

	virtual const fiDevice* GetRpfDevice() const { return this; }

    //RETURNS: True if the device is a fiDeviceCloud.  See rline/cloud/rlcloud.h
    virtual bool IsCloud() const { return false; }

	// PURPOSE:	Returns a small nonzero number uniquely identifying this device as a packfile
	//			which can then be passed to pgStreamer::GetArchive later on.
	virtual u32 GetPackfileIndex() const { return 0; }

	virtual const char *GetDebugName() const = 0;

	//PURPOSE: Returns if the file is coming from one of the registered mounts
	static bool CheckFileValidity(const char* pFilename);
protected:
#if __DEVPROF
	static fiDeviceProfiler *sm_DeviceProfiler;
#endif // __DEVPROF

private:
	int EnumFilesForDevice(const char *filename,void (*callback)(const fiFindData &data,void *userArg),void *userArg) const;

	static bool IsExtraMount(const char* filename);
	static void GetExtraDeviceName(const char* filename, char *deviceName, int maxNameLength);
	static Mount_t *AddExtraMount(const char* filename);
	static Mount_t *FindExtraMount(const char* filename);
	static const fiDevice *FindExtraDevice(const char* filename);

	static const fiDevice* GetDeviceImpl(const char* filename,bool readOnly = true);

	static bool UnmountExtra(const char* filename);

	static struct sysIpcSemaTag *sm_FileSemaphore;
	static int sm_MaxMounts;
	static int sm_MaxExtraMounts;
};

/*  PURPOSE:
	Concrete subclass of fiDevice which implements the local filesystem
*/
class fiDeviceLocal: public fiDevice {
public:
	// PURPOSE: Normalize a name for the underlying local device (insert host0:, change
	//			forward slashes to backslashes, etc)
	// PARAMS:	dest - Destination buffer
	//			destSize - sizeof(dest)
	//			src - Source buffer
	// RETURNS:	dest
	// NOTES:	dest and src are NOT allowed to overlap.
	static const char *FixName(char *dest,int destSize,const char *src);

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual fiHandle CreateBulk(const char *filename) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int WriteBulk(fiHandle handle,u64 offset,const void *inBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	int Size(fiHandle handle) const override;
	u64 Size64(fiHandle handle) const override;
	virtual bool Delete(const char *filename) const;
	virtual bool Rename(const char *from,const char *to) const;
	virtual bool MakeDirectory(const char *filename) const;
	virtual bool UnmakeDirectory(const char *filename) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;

	virtual fiHandle FindFileBegin(const char *filename,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;
	virtual bool SetEndOfFile(fiHandle handle) const;

	virtual u32 GetAttributes(const char *filename) const;
	virtual bool SetAttributes(const char *filename,u32 attributes) const;
	virtual u32 GetPhysicalSortKey(const char* filename) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const;

	static void OnDiscSwap();

	// PURPOSE: Singleton accessor
	static const fiDevice& GetInstance();
};


/*  PURPOSE:
	Concrete subclass of fiDeviceLocal which implements the local filesystem with DRM support (i.e. PS3 downloadable content)
*/
class fiDeviceLocalDrm: public fiDeviceLocal {
public:
	virtual fiHandle OpenBulkDrm(const char *filename,u64 &outBias,const void * pDrmKey) const;
};

struct fiRemoteDirEntry {
	char m_Filename[96];
	u32 m_Attribute;
	u64 m_Filesize;
	u64 m_VirtSize;
	u64 m_PhysSize;
	datResourceFileHeader m_ResourceFileHeader;
};

/*  PURPOSE:
	Concrete subclass of fiDevice which implements a simple TCP/IP-based
	remote filesystem.  Intended for Xbox and Xenon, neither of which
	support simple read/write access to your host PC.
*/
class fiDeviceRemote: public fiDevice {
public:
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	int Size(fiHandle handle) const override;
	u64 Size64(fiHandle handle) const override;
	virtual bool Delete(const char *filename) const;
	virtual bool MakeDirectory(const char *pathname) const;
	virtual bool DeleteDirectory(const char *pathname, bool deleteContents=false) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual fiHandle FindFileBegin(const char *filename,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;
	virtual u32 GetAttributes(const char *filename) const;
	virtual u64 GetFileSize(const char *filename) const;
	u32 GetFileCount(const char *directory) const;
	bool PrefetchDir(const char *directory) const;
	u32 GetAttributesDir(const char *directory, fiRemoteDirEntry *results, int resultCount) const;
	void InvalidateDirectoryCache(const char *directory) const;
	virtual int GetResourceInfo(const char *name,datResourceInfo &outHeader) const;
	virtual bool SetAttributes(const char *filename,u32 attributes) const;
	virtual const char *GetDebugName() const;
	virtual u32 GetPhysicalSortKey(const char*) const { return HARDDRIVE_LSN; }			// Use a low LSN to indicate that this is "harddrive", since that's how we treat RFS

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return NETWORK; }

#if !__FINAL
	float GetAverageThroughput() const;
#endif // !__FINAL

	// PURPOSE: Singleton accessor
	static const fiDevice& GetInstance();

	static void InitClass(int argc,char **argv);
	static void ShutdownClass();

private:
	int GetReadChunkSize() const;
	u32 GetRemoteMaxDirEntries() const;

	fiRemoteDirEntry *GetCachedEntry(const char *name) const;


	// Directories in this map have already been processed with GetAttributeDir.
	mutable atMap<u32, atArray<u32> > m_FetchedDirectories;

	// Prefetched information about individual files
	mutable atMap<u32, fiRemoteDirEntry> m_FileInfo;
};

/*  PURPOSE:
	Concrete subclass of fiDevice which implements memory-base streaming.
	The filename passed to Open must be of the form "memory:%u,%u,%d:comment"
	where the first value is the memory buffer and the second value is
	the size of the buffer.  The memory is assumed to be allocated by
	operator new (and will be freed on close) if the third parameter
	is nonzero.
*/
class fiDeviceMemory: public fiDevice {
public:
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	int Size(fiHandle handle) const override;
	u64 Size64(fiHandle handle) const override;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual u32 GetAttributes(const char*) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual bool SafeRead(fiHandle handle,void *outBuffer,int size) const;
	virtual bool SafeWrite(fiHandle handle,const void *buffer,int size) const;
	virtual const char *GetDebugName() const;
    virtual void Sanitize(fiHandle handle) const;
	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return MEMORY; }

	// Singleton accessor
	static const fiDevice& GetInstance();
};

class fiDeviceCount : public fiDevice
{
public:
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Size(fiHandle handle) const;
	virtual u64 Size64(fiHandle handle) const;
	virtual int Close(fiHandle handle) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return MEMORY; }

	// Singleton accessor
	static const fiDevice& GetInstance();
};

#if !__NO_OUTPUT
class fiDeviceConsole : public fiDevice
{
public:
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Size(fiHandle handle) const;
	virtual u64 Size64(fiHandle handle) const;
	virtual int Close(fiHandle handle) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return MEMORY; }

	// Singleton accessor
	static const fiDevice& GetInstance();
};
#endif // !__NO_OUTPUT

class fiDeviceGrowBuffer: public fiDevice
{
public:
	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Size(fiHandle handle) const;
	virtual u64 Size64(fiHandle handle) const;
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual u32 GetAttributes(const char*) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual bool SafeRead(fiHandle handle,void *outBuffer,int size) const;
	virtual bool SafeWrite(fiHandle handle,const void *buffer,int size) const;
	virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return MEMORY; }

	// Singleton accessor
	static const fiDevice& GetInstance();
};

#define FILE_ATTRIBUTE_READONLY             0x00000001
#define FILE_ATTRIBUTE_HIDDEN               0x00000002
#define FILE_ATTRIBUTE_SYSTEM               0x00000004
#define FILE_ATTRIBUTE_DIRECTORY            0x00000010
#define FILE_ATTRIBUTE_ARCHIVE              0x00000020
#define FILE_ATTRIBUTE_NORMAL               0x00000080
#define FILE_ATTRIBUTE_TEMPORARY            0x00000100
#define FILE_ATTRIBUTE_SPARSE_FILE          0x00000200
#define FILE_ATTRIBUTE_REPARSE_POINT        0x00000400
#define FILE_ATTRIBUTE_COMPRESSED           0x00000800
#define FILE_ATTRIBUTE_OFFLINE              0x00001000
#define FILE_ATTRIBUTE_NOT_CONTENT_INDEXED  0x00002000
#define FILE_ATTRIBUTE_INVALID				~0U

}	// namespace rage

#endif
