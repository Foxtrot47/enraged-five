// 
// spatialdata/spatialdata.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "spatialdata/spatialdata.h"
#include "spatialdata/sphere.h"
#include "spatialdata/plane.h"
#include "spatialdata/aabb.h"
#include "spatialdata/transposedplaneset.h"
//#include "spatialdata/metadata_parser.h"

namespace rage {

} // namespace rage
