// 
// spatialdata/bkdtree.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_BKDTREE_H
#define SPATIALDATA_BKDTREE_H

#include "atl/array.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include <algorithm>

namespace rage {

	namespace spd
	{
	//
	// PURPOSE : Fast AABB implementation with a packed index
	//
	// REMARKS: Could use other component to store flags and other info
	//
	// NOTES: Packs index into w component of vector so size is 32 bytes
	// allowing 4 per cache line
	//
	class AABBIndexed // TODO -- can we just use spdAABB and use one of the w components as the index?
	{
	public:
		//
		// PURPOSE : Functor to allow intersection and retrieval of the indices for lists
		//	of objects
		struct TraversalIntersector 
		{
			Vector3  m_min;
			Vector3	 m_max;
			int*	 m_indices;
			int		 m_cnt;
			int		 m_maxAmt;
			TraversalIntersector( int* indices, const Vector3& min, const Vector3& max , int maxAmt = 256):
			m_min( min), m_max( max ) , m_cnt(0), m_indices(indices), m_maxAmt( maxAmt )
			{}

			int GetCount() const { return m_cnt; }

			void operator()( AABBIndexed& b )
			{
				if (m_cnt<m_maxAmt)
				{
					int increment =  b.Intersects( m_min, m_max );
					m_indices[m_cnt] = b.GetIndex();
					m_cnt += increment;
				}
			}
		};
	private:

		Vector4 m_min;
		Vector3 m_max;  // index is packed into w component to help with memory access

	public:
		AABBIndexed() {}

		AABBIndexed( const Vector3& min, const Vector3& max, int index )
			: m_max( max)
		{
			m_min.SetVector3( min);
			m_min.iw = index;
		}
		AABBIndexed( const Vector3& min, const Vector3& max )
			: m_max( max )
		{
			m_min.SetVector3ClearW( min);
		}
		int GetIndex() const 
		{
			//Vector4 idx;
			//idx.SplatW(m_min);
			//idx.FloatToInt();
			return m_min.iw;
		}
		bool Intersects( const Vector3& minIn, const Vector3& maxIn ) const 
		{
			Vector3 in = minIn.IsGreaterThanV( GetMax() );
			in.Or( maxIn.IsLessThanV( GetMin() ) );
			return in.IsZero(); 
		}
		void SetMax( int Axis, float value )
		{
			m_max[Axis] = value;
		}
		void SetMin( int Axis, float value )
		{
			m_min[Axis] = value;
		}
		const Vector3& GetMax() const { return m_max; }
		const Vector3 GetMin() const 
		{ 
			Vector3 bMin;
			m_min.GetVector3( bMin);
			return bMin;
		}
		// Invariant for bbx
		bool IsValid()  { return GetMin().IsGreaterThanV( GetMax()).IsZero(); }

		Vector3 GetPosition()  const { return ( GetMin() + GetMax() ) * VEC3_HALF ; }

		static const int DesiredSize = 32;
	};
	

	// PURPOSE : Override able function to get the minimum and maximum values from an object
	template<class T>
	void GetMinMax( const T& b, Vector3& min, Vector3& max )
	{
		max = b.GetMax();
		min = b.GetMin();
		return;
	}

	// PURPOSE : Functor which Calculates a bounding box from a list of objects
	struct GetBoundBox
	{
		Vector3 min;
		Vector3 max;

		GetBoundBox()
		{
			max  = Vector3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
			min = Vector3( FLT_MAX, FLT_MAX, FLT_MAX );
		}
		template<class T>
		void operator()( const T& value)
		{
			Vector3 vMin;
			Vector3 vMax;
			GetMinMax( value, vMin, vMax );
			max.Max( max,  vMax );  
			min.Min( min,  vMin );
		}	
	};
	// PURPOSE : Predicate to determine which side of  a plane a object is
	struct OnLeftSide
	{
		float centre;
		int  ax;
		OnLeftSide( int Axis, float c ) : centre( c), ax( Axis ) {}

		template<class OBJECT>
		bool operator()( const OBJECT& a )
		{
			return a.GetPosition()[ax] < centre;
		}
	};

	// PURPOSE : functor to get the minimum value on a Axis
	struct GetAxisMin
	{
		int  ax;
		GetAxisMin( int Axis) : ax( Axis ) {}

		template<class OBJECT>
		float operator()( const OBJECT& a,  const OBJECT& b)
		{
			return a.GetMin()[ax] < b.GetMin()[ax];
		}
	};
	// PURPOSE : functor to get the maximum value on a Axis
	struct GetAxisMax
	{
		int  ax;
		GetAxisMax( int Axis) : ax( Axis ) {}

		template<class OBJECT>
		float operator()( const OBJECT& a ,  const OBJECT& b)
		{
			return a.GetMax()[ax] < b.GetMax()[ax];
		}
	};
	// PURPOSE : functor to calculate creation of leaf depending on number of elements remaining
	struct MinCellPredicate
	{
		int	m_amt;
	public:
		MinCellPredicate( int amt ) : m_amt( amt ) {}

		template<class T>
		bool operator()( const T& start, const T& end, const Vector3& , const Vector3&   )
		{
			return ( ( end - start ) < m_amt );
		}
	};
	inline int GetMaximumAxis( const Vector3& diff )
	{
		if ( diff.x > diff.y )
		{
			return ( diff.x > diff.z ) ? 0 : 2;
		}
		else
		{
			return  ( diff.y > diff.z ) ? 1 : 2;
		}
	}

	// PURPOSE :  Bounded KdTree Node which is both leaf and normal node
	// NOTES: takes 12 bytes (16 on x64) so uses little memory 
	template<class T>
	struct BoundedNode
	{
		BoundedNode() : m_index(0)
		{
			m_clip[0] = m_clip[1] = 0.0f;
		}
		size_t m_index; 
		union 
		{
			u32		m_items[2];
			float	m_clip[2];  
		};
		void SetPackedPart( int v )
		{
			FastAssert( v >=0 && v<=3 );
			m_index = ( m_index & ~0x3 ) + v ;
		}
		size_t GetIndex() const { return (m_index & ~0x3);}
		void SetChildIndex( size_t v ) {		m_index = (size_t)( v ) + (m_index & 0x3); }
	public:
		typedef T* iterator;

		int					GetAxis()  const { return m_index& 0x3; }
		bool				IsLeaf() const  { return (m_index& 0x3) == 3; }
		BoundedNode<T>*		GetLeftIndex() const { return (BoundedNode<T>*)( (m_index & ~0x3) ) ; }
		BoundedNode<T>*		GetRightIndex() const { return GetLeftIndex() + 1; }

		void	SetIndex( BoundedNode<T>* v ) {		m_index = ( (size_t) v & ~0x3) + (m_index & 0x3); }
		void	SetAxis( int ax )	{	SetPackedPart(ax); }
		void	SetLeaf()			{	SetPackedPart(3);  }

		void	SetChildren(T* firstChild, int size )
		{
			SetLeaf();
			SetChildIndex( (size_t)firstChild );
			m_items[0] = size;
		}
		void GetChildren( T*& start, T*& end ) const
		{
			FastAssert( IsLeaf() ); 
			start = (T*)GetIndex();
			end = start + m_items[0];
		}
		void	SetMinSplit( float min ) { m_clip[0] = min; }
		void	SetMaxSplit( float max ) { m_clip[1] = max; }

		u32	GetMinUIntSplit( float min ) { return m_items[0]; }
		u32	GetMaxUIntSplit( float max ) { return m_items[1]; }

		float	GetMinSplit() const { FastAssert( !IsLeaf() ); return m_clip[0]; }
		float	GetMaxSplit() const { FastAssert( !IsLeaf() ); return m_clip[1]; }

		static const int DesiredSize = __64BIT? 16 : 12;
	};

	template<class B, class T>
	struct BuildStack
	{
		Vector3				minV;
		Vector3				maxV;
		BoundedNode<B>*		node;
		T					start;
		T					end;
		

		BuildStack() {}
		BuildStack( BoundedNode<B>* 	n, T s, T e, const Vector3& minBox, const Vector3& maxBox )
			: node(n), start(s), end( e ), minV( minBox), maxV( maxBox )
		{}
	};

	// PURPOSE :  Template function to actually build bound bkdtree
	// NOTES: Separate from base class to allow for easier specialization
	//
	template<class B, class T, class Predicate>
	void BoundedKdTreeBuilderRecurse( Vector3& gMin, Vector3& gMax, BoundedNode<B>* n, BoundedNode<B> NodeList[], T start, T end ,  Predicate predicate, int& idx , int ASSERT_ONLY( MaxAmount ) )
	{
		// Vector3 bottomCorner = gMin;
		atFixedArray< BuildStack< B, T >, 128 >  stack;

		while( 1)
		{
			if (predicate( start, end, gMin, gMax  ))
			{
				n->SetChildren( start, (int) std::distance( start, end ) );

				if ( stack.empty() )
				{
					return;
				}
				const BuildStack< B, T >& stnode = stack.Pop();

				start = stnode.start;
				end = stnode.end;
				n = stnode.node;
				gMin = stnode.minV;
				gMax = stnode.maxV;  // stack.Pop();
			}
			else
			{
				int Axis = GetMaximumAxis( gMax - gMin );
				float centre =  ( gMin[ Axis ] + gMax[ Axis ] ) * 0.5f;

				T mid = std::partition( start, end, OnLeftSide( Axis, centre ));
				if ( mid == start || mid == end )
				{
					GetBoundBox bbx = std::for_each( start, end, GetBoundBox() );
					gMax = bbx.max;
					gMin = bbx.min;
					Axis = GetMaximumAxis( gMax - gMin );
					centre =  ( gMin[ Axis ] + gMax[ Axis ] ) * 0.5f;
					mid = std::partition( start, end, OnLeftSide( Axis, centre ));
				}
				
				if ( mid == start )
				{
					++mid;
				}
				else if  ( mid == end )
				{
					--mid;
				}
				T minEl = std::min_element( mid, end, GetAxisMin( Axis));
				T maxEl = std::max_element( start, mid, GetAxisMax( Axis));


				FastAssert( mid != start );
				FastAssert( mid != end );

				// float offset = bottomCorner[Axias];
				// n->SetMinSplit( minEl->GetMin()[Axis] - offset );
				// n->SetMaxSplit( maxEl->GetMax()[Axis] - offset );
				n->SetMinSplit( minEl->GetMin()[Axis] );
				n->SetMaxSplit( maxEl->GetMax()[Axis] );

				int oldIdx = idx;
				idx +=2;				// as they are parallel
				n->SetIndex( &(NodeList[ oldIdx] ));
				n->SetAxis( Axis );

				FastAssert( idx <= MaxAmount );
				Vector3 maxLeft = gMax;
				maxLeft[Axis] = centre;

				Vector3 minRight = gMin;
				minRight[Axis] = centre;

				// Push right side onto stack
				stack.Push ( BuildStack< B, T >( &(NodeList[ oldIdx + 1 ]), mid, end, minRight, gMax ) );
			
				// do left side
				gMax = maxLeft;
				n = &(NodeList[ oldIdx  ]); 
				end = mid;	
			}
		}
	}
	template<class B, class T, class Predicate>
	void BoundedKdTreeBuilder( BoundedNode<B> NodeList[], T start, T end ,  Predicate predicate, int& idx , int MaxAmount )
	{
		GetBoundBox bbx = std::for_each( start, end, GetBoundBox() );
		idx = 1;
		BoundedKdTreeBuilderRecurse( bbx.min, bbx.max, &(NodeList[ 0 ]), NodeList, start, end, predicate, idx, MaxAmount);
		// gMin = bbx.min;
		// gMax = bbx.max;
	}
	// PURPOSE :  Template traversal for bounding boxes
	// NOTES: Most of loops have been removed and changed to data
	//
	template<class T, class OPERATOR>
	void Traverse( const BoundedNode<T> NodeList[], const Vector3& min, const Vector3& max, OPERATOR& op )
	{
		FastAssert( min.IsGreaterThanV(max).IsZero() );

		atRangeArray<const BoundedNode<T>*, 256> stack;
		int		size = 1;
		stack[0] = 0;

	
		const BoundedNode<T>* node = &NodeList[0];
		while( node )
		{
			if ( node->IsLeaf() )
			{
				typename BoundedNode<T>::iterator start;
				typename BoundedNode<T>::iterator end;
				node->GetChildren( start, end );
				while( start != end )
				{
					op( *start++ );
				}
				node = stack[ --size ];
			}
			else 
			{
				int Axis = node->GetAxis();
				float minAxis= min[Axis];
				float maxAxis = max[Axis];
				/*int increment;

				stack[size ] = node->GetLeftIndex();
				increment =  minAxis <= node->GetMaxSplit() ? 1: 0;
				size += increment;

				FastAssert( size <  256);
				stack[size ] = node->GetRightIndex();
				increment =  maxAxis >= node->GetMinSplit() ? 1: 0;
				size += increment;*/

				if ( minAxis <= node->GetMaxSplit() )// do left
				{
					if ( maxAxis >= node->GetMinSplit() )
					{
						stack[size++] = node->GetRightIndex(); // put right index on stack
						FastAssert( size < 256);
					}
					node = node->GetLeftIndex(); // go left
				}
				else if ( maxAxis >= node->GetMinSplit() )
				{
						node = node->GetRightIndex(); // go right
				}
				else
				{
					node = stack[ --size ];
					FastAssert( size >= 0);
				}

			}
		}
	}

};

// PURPOSE :  Bounded Kd Tree to allow for spatially separating objects
//
// NOTES: A bounded kd tree is a mixture of a kdtree and and a heirachical bounding volume.
//	It has the following advantage:
//		1. Low memory usage
//		2. No dynamic memory allocations during construction
//		3. Limit on build size
//		4. Fast traversal
//		5. It can be traverse in sorted order
//
//	This makes is very well suited to building the tree on the fly ( per frame )
//  and getting fast spatial lookups
//	For more information see http://graphics.uni-ulm.de/BIH.pdf for more detailed description
//	and http://graphics.uni-ulm.de/BIH.pdf
// 
//
	template<class T>
	class BoundKdTree
	{
		atArray< spd::BoundedNode<T>, 0, unsigned int > 	m_boundNodes;
		static const int		    NumPlanes = 6;
	public:
		void Init( int MaxSize, int SizeFactor = NumPlanes / 2 )
		{
			m_boundNodes.Reset();
			m_boundNodes.Reserve( MaxSize * SizeFactor);
		}

		void Create( typename spd::BoundedNode<T>::iterator start,  typename spd::BoundedNode<T>::iterator end , int maxNodeSize = 1  )
		{
			int amount = 1;
			m_boundNodes.Resize( 1 );
			spd::BoundedKdTreeBuilder(  &m_boundNodes[0], start, end,  spd::MinCellPredicate( maxNodeSize + 1) , amount, m_boundNodes.GetCapacity() );
			m_boundNodes.Resize( amount );
		}
		int GetNodeCount() { return m_boundNodes.GetCount(); }

		template<class OPERATOR>
		void Traverse( const Vector3& min, const Vector3& max, OPERATOR& op )
		{	
			FastAssert( min.IsGreaterThanV( max).IsZero() );
			spd::Traverse( m_boundNodes.begin(), min, max , op );
		}
		template<class OPERATOR>
		void Traverse( const Vector3& pos, const float radius, OPERATOR& op )
		{	
			Vector3 offset( radius, radius, radius );
			Vector3 min = pos - offset;
			Vector3 max = pos + offset;
			spd::Traverse( m_boundNodes.begin(), min, max , op );
		}
	};

	void UnitTestBoundedKdTree();


};
#endif
