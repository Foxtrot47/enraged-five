// 
// spatialdata/transposedplaneset.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "grcore/viewport.h"
#include "spatialdata/transposedplaneset.h"

// Russ wrote -
// "I can't make a separate metadata.cpp file that only #includes "metadata_parser.h", since that ends
// up being a translation unit with no referenced symbols and the compiler can strip it - so I need to
// include the _parser.h in some .cpp file that does get referenced"
#include "spatialdata/plane.h"
#include "spatialdata/metadata_parser.h"

/*
TODO -- the following functions implement the same functionality as spdTransposedPlaneSet8 (except they use negated planes)

x:\gta5\src\dev\rage\base\src\grcore\viewport_inline.h
	IsSphereVisibleInline
	IsAABBVisibleInline
	GetSphereCullStatusInline
	GetAABBCullStatusInline

x:\gta5\src\dev\rage\base\src\grmodel\grmodelspu.cpp
	IsAABBVisibleInline
	GetAABBCullStatusInline

x:\gta5\src\dev\game\renderer\PlantsGrassRendererSPU.cpp
	_IsSphereVisibleInline

x:\gta5\src\dev\rage\suite\src\gpuptfx\ptxrainupdatespu.cpp
	_IsSphereVisible

perhaps grcViewport could replace m_FrustumLRTB, m_FrustumNFNF with a single spdTransposedPlaneSet8?
*/

namespace rage {

void spdTransposedPlaneSet8::Set(const grcViewport& viewport, bool bNearPlane, bool bFarPlane, Vec4V_In plane6, Vec4V_In plane7 NV_SUPPORT_ONLY(, bool useStereoFrustum))
{
	// grcViewport planes point inwards, we want planes pointing outwards
	SetPlanes(
#if NV_SUPPORT
		useStereoFrustum ? -viewport.GetCullFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT  ) : -viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT  ),
		useStereoFrustum ? -viewport.GetCullFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT ) : -viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT ),
#else
		-viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT  ),
		-viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT ),
#endif
		-viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_TOP   ),
		-viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_BOTTOM),
		bNearPlane ? -viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_NEAR) : Vec4V(V_ZERO),
		bFarPlane  ? -viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_FAR ) : Vec4V(V_ZERO),
		plane6,
		plane7
	);
}

#if NV_SUPPORT
void spdTransposedPlaneSet8::SetStereo(const grcViewport& viewport, bool bNearPlane, bool bFarPlane, Vec4V_In plane6, Vec4V_In plane7)
{
	Set(viewport, bNearPlane, bFarPlane, plane6, plane7, true);
}
#endif

} // namespace rage
