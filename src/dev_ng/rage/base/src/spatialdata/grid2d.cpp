//
// spatialdata/grid2d.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "grid2d.h"

#include "grid2d_parser.h"

#include "data/struct.h"
#include "file/token.h"


#define		FAST_GRID2D		1

using namespace rage;

////////////////////////////////////////////////////////////////
// spdGrid2D

spdGrid2D::spdGrid2D ()
  : m_MinCellX(0),
    m_MaxCellX(0),
    m_MinCellY(0),
    m_MaxCellY(0),
	m_MinX(0.0f),
    m_CellDimX(1.0f),
    m_CellDimY(1.0f)
{
	InitCalculatedMembers();
}


#if __DECLARESTRUCT

void spdGrid2D::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(spdGrid2D);
	STRUCT_FIELD(m_NumCells);
	STRUCT_FIELD(m_MinCellX);
	STRUCT_FIELD(m_MaxCellX);
	STRUCT_FIELD(m_MinCellY);
	STRUCT_FIELD(m_MaxCellY);
	STRUCT_FIELD(m_MinX);
	STRUCT_FIELD(m_MaxX);
	STRUCT_FIELD(m_MinY);
	STRUCT_FIELD(m_MaxY);
	STRUCT_FIELD(m_CellDimX);
	STRUCT_FIELD(m_CellDimY);
	STRUCT_FIELD(m_CellInvDimX);
	STRUCT_FIELD(m_CellInvDimY);
	STRUCT_END();
}

#endif

#define GRID_EPSILON 0.001f

void spdGrid2D::Init (float minX, float maxX, float dimX, float minY, float maxY, float dimY)
{
	Assert(dimX > 0.0f);
	Assert(dimY > 0.0f);
	Assert(maxX >= minX);
	Assert(maxY >= minY);

	m_CellDimX = dimX;
	m_CellDimY = dimY;

	ComputeCellDimInverse();

	// The grid epsilon is to ensure that the specified boundary points will
	// be contained inside the grid.
	m_MinCellX = (int)floorf(minX * m_CellInvDimX - GRID_EPSILON);
	m_MaxCellX = (int)floorf(maxX * m_CellInvDimX + GRID_EPSILON);

	m_MinCellY = (int)floorf(minY * m_CellInvDimY - GRID_EPSILON);
	m_MaxCellY = (int)floorf(maxY * m_CellInvDimY + GRID_EPSILON);

	InitCalculatedMembers();
}


/*
PURPOSE
	Initialize a grid with the extents known in cell coordinates rather than
	the world space (real) coordinates required by the normal Init() function.
PARAMS
	minX		- The minimum X coordinate, compatible with what GetMinCellX() would return.
	maxX		- The maximum X coordinate, compatible with what GetMaxCellX() would return.
	dimX		- The size of each cell in the X direction.
	minY		- The minimum Y coordinate, compatible with what GetMinCellY() would return.
	maxY		- The maximum Y coordinate, compatible with what GetMaxCellY() would return.
	dimY		- The size of each cell in the Y direction.
*/
void spdGrid2D::InitCells(int minX, int maxX, float dimX, int minY, int maxY, float dimY)
{
	Assert(dimX > 0.0f);
	Assert(dimY > 0.0f);
	Assert(maxX >= minX);
	Assert(maxY >= minY);

	m_CellDimX = dimX;
	m_CellDimY = dimY;

	m_MinCellX = minX;
	m_MaxCellX = maxX;

	m_MinCellY = minY;
	m_MaxCellY = maxY;

	InitCalculatedMembers();
}


void spdGrid2D::Load (fiAsciiTokenizer & tok)
{
	ASSERT_ONLY(int numCells);

	tok.MatchIToken("GridCells");
	ASSERT_ONLY(numCells =) tok.GetInt();

	tok.MatchIToken("GridDimX");
	m_CellDimX = tok.GetFloat();
	tok.MatchIToken("GridDimY");
	m_CellDimY = tok.GetFloat();

	tok.MatchIToken("GridMinX");
	m_MinCellX = tok.GetInt();
	tok.MatchIToken("GridMaxX");
	m_MaxCellX = tok.GetInt();
	tok.MatchIToken("GridMinY");
	m_MinCellY = tok.GetInt();
	tok.MatchIToken("GridMaxY");
	m_MaxCellY = tok.GetInt();

	Assert(m_CellDimX > 0.0f);
	Assert(m_CellDimY > 0.0f);
	Assert(m_MinCellX <= m_MaxCellX);
	Assert(m_MinCellY <= m_MaxCellY);

	InitCalculatedMembers();

	Assert(numCells == GetNumCells());
}


void spdGrid2D::InitCalculatedMembers ()
{
	ComputeCellDimInverse();
	InitCalculatedMembers2();
}

void spdGrid2D::InitCalculatedMembers2()
{
	m_MinX = m_MinCellX * m_CellDimX;
	m_MaxX = (m_MaxCellX+1) * m_CellDimX;
	m_MinY = m_MinCellY * m_CellDimY;
	m_MaxY = (m_MaxCellY+1) * m_CellDimY;

	m_NumCells = (m_MaxCellX-m_MinCellX+1) * (m_MaxCellY-m_MinCellY+1);
}

void spdGrid2D::ComputeCellDimInverse ()
{
	m_CellInvDimX = 1.0f / m_CellDimX;
	m_CellInvDimY = 1.0f / m_CellDimY;
}

void spdGrid2D::Save (fiAsciiTokenizer & tok)
{
	tok.PutStrLine("GridCells %d", GetNumCells());
	tok.PutStrLine("GridDimX %f", GetCellDimX());
	tok.PutStrLine("GridDimY %f", GetCellDimY());
	tok.PutStrLine("GridMinX %d", GetMinCellX());
	tok.PutStrLine("GridMaxX %d", GetMaxCellX());
	tok.PutStrLine("GridMinY %d", GetMinCellY());
	tok.PutStrLine("GridMaxY %d", GetMaxCellY());
}


bool spdGrid2D::CalcCellIndex (int &cellIndex, float x, float y) const
{
	bool inGrid;
	int cellX=0, cellY=0;
	inGrid = CalcCellX(cellX,x);
	inGrid = inGrid && CalcCellY(cellY,y);
	if (inGrid)
	{
		cellIndex = CalcCellIndex(cellX,cellY);
	}
	else
	{
		cellIndex = -1;
	}
	
	return inGrid;
}


void spdGrid2D::CalcClampedCellIndex (int &cellIndex, float x, float y) const
{
#if FAST_GRID2D
	float cellX = floorf(x * m_CellInvDimX); 
	float cellY = floorf(y * m_CellInvDimY);

	cellX = Clamp(cellX, (float)m_MinCellX, (float)m_MaxCellX);	 
	cellY = Clamp(cellY, (float)m_MinCellY, (float)m_MaxCellY);

	cellIndex = CalcCellIndex((int)cellX,(int)cellY);
#else
	// like the above, but clamps the point onto the grid if it's outside.
	int cellX=0, cellY=0;
	CalcClampedCellX(cellX,x);
	CalcClampedCellY(cellY,y);
	cellIndex = CalcCellIndex(cellX,cellY);
#endif
}


/*
PURPOSE
	Find a rectangular region of cells that contains the given rectangle.
PARAMS
	rectX1	- Min X for the input rectangle.
	rectY1	- Min Y for the input rectangle.
	rectX2	- Max X for the input rectangle.
	rectY2	- Max Y for the input rectangle.
	x1Out	- Output min X cell.
	y1Out	- Output min Y cell.
	x2Out	- Output max X cell, inclusive.
	y2Out	- Output max Y cell, inclusive.
RETURNS
	'true' if the function successfully found a valid
	region containing the rectangle.
NOTES
	The function will fail and return false if the rectangle is completely outside
	of the grid, or if it's entirely on grid cell borders.
*/
bool spdGrid2D::CalcCellRegion(float rectX1, float rectY1,
		float rectX2, float rectY2,
		int &x1Out, int &y1Out, int &x2Out, int &y2Out) const
{

// The optimized version was disabled, mostly because its behavior is a bit dangerous
// in that if input parameters contain invalid floating point numbers for some reason,
// then the function may return true but produce output which is outside of the grid.
// Also, the 360 compiler doesn't seem to automatically make use of fsel() instructions
// for the float comparisons, so it might actually make performance worse there.
// If we had more time to test, and manually switched over to fsel() intrinsics, it would
// have been nice to use. /FF
#if FAST_GRID2D && 0

	float x1 = floorf(rectX1*m_CellInvDimX);
	float y1 = floorf(rectY1*m_CellInvDimY);
	float x2 = ceilf(rectX2*m_CellInvDimX) - 1.0f;
	float y2 = ceilf(rectY2*m_CellInvDimY) - 1.0f;

	float res = 1.0f;

	if(x1 < (float)m_MinCellX)
		x1 = (float)m_MinCellX;
	if(x2 > (float)m_MaxCellX)
		x2 = (float)m_MaxCellX;

	if(x1 > x2)
	{	// The whole rectangle is outside of the grid, in the X direction.
		// Note that it could also happen if rectX1 and rectX2 are both exactly on the same grid cell boundary.
		res = -1.0f;
	}

	if(y1 < (float)m_MinCellY)
		y1 = (float)m_MinCellY;
	if(y2 > (float)m_MaxCellY)
		y2 = (float)m_MaxCellY;

	if(y1 > y2)
	{	// The whole rectangle is outside of the grid, in the Y direction.
		// Note that it could also happen if rectY1 and rectY2 are both exactly on the same grid cell boundary.
		res = -1.0f;
	}

	x1Out = (int)x1;
	y1Out = (int)y1;
	x2Out = (int)x2;
	y2Out = (int)y2;

	return res > 0.0f? true: false;

#else
	int x1, y1, x2, y2;

	x1 = (int)floorf(rectX1*m_CellInvDimX);
	y1 = (int)floorf(rectY1*m_CellInvDimY);
	x2 = (int)ceilf(rectX2*m_CellInvDimX) - 1;
	y2 = (int)ceilf(rectY2*m_CellInvDimY) - 1;

	if(x1 < m_MinCellX)
		x1 = m_MinCellX;
	if(x2 > m_MaxCellX)
		x2 = m_MaxCellX;

	if(x1 > x2)
	{	// The whole rectangle is outside of the grid, in the X direction.
		// Note that it could also happen if rectX1 and rectX2 are both
		// exactly on the same grid cell boundary.
		return false;
	}

	if(y1 < m_MinCellY)
		y1 = m_MinCellY;
	if(y2 > m_MaxCellY)
		y2 = m_MaxCellY;

	if(y1 > y2)
	{	// The whole rectangle is outside of the grid, in the Y direction.
		// Note that it could also happen if rectY1 and rectY2 are both
		// exactly on the same grid cell boundary.
		return false;
	}

	x1Out = x1;
	y1Out = y1;
	x2Out = x2;
	y2Out = y2;

	return true;
#endif
}


/*
PURPOSE
	Test if a given cell touches a circle with the specified center and radius.
PARAMS
	centerX		- X position of circle center.
	centerY		- Y position of circle center.
	radius		- Radius of the circle.
	cellX		- X coordinate of the cell to test.
	cellZ		- Y coordinate of the cell to test.
NOTES
	Adapted from TestSphereToBox() in 'vector/geometry.cpp'.
*/
bool spdGrid2D::TestCircle(float centerX, float centerY, float radius, int cellX, int cellY) const
{

#if FAST_GRID2D

	// there will be hit from LHS int to float, pre-compute everything to avoid at least the float branch flushes

	float dMin = 0.0f;
	float cellMinX = CalcCellMinX(cellX);
	float cellMinY = CalcCellMinY(cellY);

	float newMinXSqr = square(centerX - cellMinX);
	float cellMaxX = cellMinX + m_CellDimX;
	float newMaxXSqr = square(centerX - cellMaxX);

	float newMinYSqr = square(centerY - cellMinY);

	float cellMaxY = cellMinY + m_CellDimY;
	float newMaxYSqr = square(centerY - cellMaxY);

	dMin = Selectf( (cellMinX - centerX), 
			(dMin + newMinXSqr),
			Selectf( (centerX - cellMaxX), (dMin + newMaxXSqr), dMin ) );
		
	dMin = Selectf( (cellMinY - centerY), 
			(dMin + newMinYSqr),
			Selectf( (centerY - cellMaxY), (dMin + newMaxYSqr), dMin ) );

#else
	float dMin = 0.0f;
	float cellMinX = CalcCellMinX(cellX);
	if(centerX < cellMinX)
		dMin += square(centerX - cellMinX);
	else
	{	float cellMaxX = cellMinX + m_CellDimX;
		if(centerX > cellMaxX)
			dMin += square(centerX - cellMaxX);
	}
	float cellMinY = CalcCellMinY(cellY);
	if(centerY < cellMinY)
		dMin += square(centerY - cellMinY);
	else
	{	float cellMaxY = cellMinY + m_CellDimY;
		if(centerY > cellMaxY)
			dMin += square(centerY - cellMaxY);
	}

#endif

	return dMin <= square(radius);
}

//-----------------------------------------------------------------------------
// Resource stuff only below here.

spdGrid2D::spdGrid2D(datResource &/*rsc*/)
{
}


#if !__NO_OUTPUT
void spdGrid2D::Print(char* header) const
{
	Displayf("%s",header);
	Displayf("number of cells: %u", m_NumCells);
	Displayf("min cell x: %i", m_MinCellX);
	Displayf("max cell x: %i", m_MaxCellX);
	Displayf("min cell y: %i", m_MinCellY);
	Displayf("max cell y: %i", m_MaxCellY);

	Displayf("grid extents");
	Displayf("X: %f - %f", m_MinX, m_MaxX);
	Displayf("Y: %f - %f", m_MinY, m_MaxY);

	Displayf("cell dimensions");
	Displayf("Dim X: %f", m_CellDimX);
	Displayf("Dim Y: %f", m_CellDimY);
	Displayf("Inv dim X: %f", m_CellInvDimX);
	Displayf("Inv dim Y: %f", m_CellInvDimY);
}
#else
void spdGrid2D::Print(char*) const {}
#endif



IMPLEMENT_PLACE(spdGrid2D);

/* End of file spatialdata/grid2d.cpp */
