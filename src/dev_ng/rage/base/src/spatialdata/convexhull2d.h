// 
// spatialdata/convexhull2d.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SPATIALDATA_CONVEXHULL2D_H
#define SPATIALDATA_CONVEXHULL2D_H


#include "data/base.h"
#include "grcore/viewport.h"


namespace rage
{

class spdAABB;
class Vector2;


class spdConvexHull2D : public datBase
{
public:
	spdConvexHull2D();

	~spdConvexHull2D();

	void CreateFromAABBXZ(const spdAABB &aabb, Vector2 *externalBuffer = NULL, int externalBufferCount = 0);

	void CreateAsUnion(const spdConvexHull2D &hull1, const spdConvexHull2D &hull2, Vector2 *externalBuffer = NULL, int externalBufferCount = 0);

	// PURPOSE: Create this convex hull from a list of
	// unsorted vertices.
	void Init(int Vertices, const Vector2 *RESTRICT vertexArray, Vector2 *externalBuffer = NULL, int externalBufferCount = 0);

	// PURPOSE: Release all memory occupied by this object.
	void Release();

	grcCullStatus Contains(const spdConvexHull2D &hull) const;

	grcCullStatus Contains(const spdAABB &box) const;

	bool IsInside(const Vector2 &point) const;

	bool Intersects(const Vector2 &p1, const Vector2 &p2) const;

	int GetVertexCount() const
	{
		return m_Vertices;
	}

	const Vector2 *GetVertexList() const
	{
		return m_VertexList;
	}


protected:
	// Number of vertices in this hull
	int m_Vertices;

	// Capacity of the array in m_VertexList
	int m_Capacity;

	// List of all vertices. NOTE that this array may be bigger than
	// indicated in m_Vertices.
	Vector2 *m_VertexList;

	// Boundaries of this hull.
	Vector2 m_Min, m_Max;

	// If TRUE, the buffer in m_VertexList was provided by the user
	// and may not be deleted.
	bool m_ExternalBuffer;
};



}


#endif // SPATIALDATA_GRID2D_H
