//
// spatialdata/sphere.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SPATIALDATA_SPHERE_H
#define SPATIALDATA_SPHERE_H

#include "spatialdata/spatialdata.h"

namespace rage {

class fiAsciiTokenizer;

/*
PURPOSE
A class for storing a sphere.
*/

class spdSphere
{
public:
	inline spdSphere() : m_centerAndRadius(V_ZERO) {}
	inline spdSphere(class datResource&) {}
	inline const spdSphere& operator =(const spdSphere& b) { m_centerAndRadius = b.m_centerAndRadius; return *this; }
	inline bool operator ==(const spdSphere& rhs) const { return IsEqualAll(m_centerAndRadius, rhs.m_centerAndRadius) != 0; }
	inline bool operator !=(const spdSphere& rhs) const { return IsEqualAll(m_centerAndRadius, rhs.m_centerAndRadius) == 0; }

	// explicit c'tors
	explicit inline spdSphere(Vec3V_In center, ScalarV_In radius) : m_centerAndRadius(center, radius) {}
	explicit inline spdSphere(Vec4V_In centerAndRadius) : m_centerAndRadius(centerAndRadius) {}

	// set
	inline void Set(Vec3V_In centre, ScalarV_In radius) { m_centerAndRadius = Vec4V(centre, radius); }
	inline void SetV4(Vec4V_In centreAndRadius) { m_centerAndRadius = centreAndRadius; }

	// get
	inline Vec4V_Out   GetV4() const { return m_centerAndRadius; }
	inline Vec4V_Ref   GetV4Ref() { return m_centerAndRadius; }
	inline Vec3V_Out   GetCenter() const { return m_centerAndRadius.GetXYZ(); }
	inline Vec2V_Out   GetCenterFlat() const { return m_centerAndRadius.GetXY(); }
	inline ScalarV_Out GetRadius() const { return m_centerAndRadius.GetW(); }
	inline ScalarV_Out GetRadiusSquared() const { const ScalarV r = m_centerAndRadius.GetW(); return r*r; }
	inline float       GetRadiusf() const { return m_centerAndRadius.GetWf(); }
	inline float       GetRadiusSquaredf() const { const float r = m_centerAndRadius.GetWf(); return r*r; }

	// setting radius preserves center
	inline void SetRadius(ScalarV_In radius) { m_centerAndRadius.SetW(radius); }
	inline void SetRadiusf(float radius) { m_centerAndRadius.SetWf(radius); }

	// grow
	static inline Vec4V_Out MergeSphereAndPoint(Vec4V_In v0, Vec3V_In point);
	static inline Vec4V_Out MergeSpheres(Vec4V_In v0, Vec4V_In v1);

	inline void GrowPoint(Vec3V_In point);
	inline void GrowSphere(const spdSphere& sphere);
	inline void GrowAABB(Vec3V_In boxMin, Vec3V_In boxMax);
	inline void GrowUniform(ScalarV_In distance);

	// inflate (grows without changing the center, returning the new radius)
	inline FASTRETURNCHECK(ScalarV_Out) InflatePoint(Vec3V_In point) const;
	inline FASTRETURNCHECK(ScalarV_Out) InflateSphere(const spdSphere& sphere) const;
	inline FASTRETURNCHECK(ScalarV_Out) InflateAABB(Vec3V_In boxMin, Vec3V_In boxMax) const;

	// transform
	inline void TransformSphereScaled(Mat34V_In mat);
	inline void TransformSphere(Mat34V_In mat); // more efficient, assumes matrix is orthonormal

	// contains
	inline spdBool_Out ContainsPoint(Vec3V_In point) const;
	inline spdBool_Out ContainsPointFlat(Vec3V_In point) const { return ContainsPointFlat(point.GetXY()); }
	inline spdBool_Out ContainsPointFlat(Vec2V_In point) const;
	inline spdBool_Out ContainsSphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainsSphereFlat(const spdSphere& sphere) const;

	// intersects
	inline spdBool_Out IntersectsSphere(const spdSphere& sphere) const;
	inline spdBool_Out IntersectsSphereFlat(const spdSphere& sphere) const;

	grcCullStatus GetCullStatusIntersectsSphere(const spdSphere& sphere) const;

	DECLARE_PLACE(spdSphere);
#if !__SPU
	void SaveData(fiAsciiTokenizer& tok) const;
	void LoadData(fiAsciiTokenizer& tok);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif // __DECLARESTRUCT
#endif // !__SPU

protected:
	Vec4V m_centerAndRadius;

	PAR_SIMPLE_PARSABLE;
};

inline spdSphere TransformOrthoSphere(Mat33V_In localToWorld, const spdSphere& localSphere);
inline spdSphere TransformOrthoSphere(Mat34V_In localToWorld, const spdSphere& localSphere);
inline spdSphere UnTransformOrthoSphere(Mat33V_In localToWorld, const spdSphere& worldSphere);
inline spdSphere UnTransformOrthoSphere(Mat34V_In localToWorld, const spdSphere& worldSphere);

inline spdSphere TransformAffineSphere(Mat33V_In localToWorld, const spdSphere& localSphere);
inline spdSphere TransformAffineSphere(Mat34V_In localToWorld, const spdSphere& localSphere);

// ===

inline spdSphere TransformOrthoSphere(Mat33V_In localToWorld, const spdSphere& localSphere)
{
	return spdSphere(Multiply(localToWorld, localSphere.GetCenter()), localSphere.GetRadius());
}

inline spdSphere TransformOrthoSphere(Mat34V_In localToWorld, const spdSphere& localSphere)
{
	return spdSphere(Transform(localToWorld, localSphere.GetCenter()), localSphere.GetRadius());
}

inline spdSphere UnTransformOrthoSphere(Mat33V_In localToWorld, const spdSphere& worldSphere)
{
	return spdSphere(Multiply(worldSphere.GetCenter(), localToWorld), worldSphere.GetRadius());
}

inline spdSphere UnTransformOrthoSphere(Mat34V_In localToWorld, const spdSphere& worldSphere)
{
	return spdSphere(UnTransformOrtho(localToWorld, worldSphere.GetCenter()), worldSphere.GetRadius());
}

inline spdSphere TransformAffineSphere(Mat33V_In localToWorld, const spdSphere& localSphere)
{
	const ScalarV magSqrX = MagSquared(localToWorld.GetCol0());
	const ScalarV magSqrY = MagSquared(localToWorld.GetCol1());
	const ScalarV magSqrZ = MagSquared(localToWorld.GetCol2());
	const ScalarV scale   = Sqrt(Max(Max(magSqrX, magSqrY), magSqrZ));

	return spdSphere(Multiply(localToWorld, localSphere.GetCenter()), localSphere.GetRadius()*scale);
}

inline spdSphere TransformAffineSphere(Mat34V_In localToWorld, const spdSphere& localSphere)
{
	const ScalarV magSqrX = MagSquared(localToWorld.GetCol0());
	const ScalarV magSqrY = MagSquared(localToWorld.GetCol1());
	const ScalarV magSqrZ = MagSquared(localToWorld.GetCol2());
	const ScalarV scale   = Sqrt(Max(Max(magSqrX, magSqrY), magSqrZ));

	return spdSphere(Transform(localToWorld, localSphere.GetCenter()), localSphere.GetRadius()*scale);
}

// ================================================================================================

inline Vec4V_Out spdSphere::MergeSphereAndPoint(Vec4V_In v0, Vec3V_In point)
{
	const Vec3V   diff   = v0.GetXYZ() - point;
	const ScalarV length = Mag(diff);

	Vec4V v2 = (v0 + Vec4V(AddScaled(point, diff, v0.GetW()/length), length))*ScalarV(V_HALF);

	v2 = SelectFT(IsLessThan(v2.GetW(), v0.GetW()), v2, v0);

	return v2;
}

inline Vec4V_Out spdSphere::MergeSpheres(Vec4V_In v0, Vec4V_In v1)
{
	const Vec4V   diff   = v0 - v1;
	const ScalarV length = Mag(diff.GetXYZ());
	const Vec3V   dirXYZ = NormalizeSafe(diff.GetXYZ(), Vec3V(V_ZERO));

	Vec4V v2 = (v0 + v1 + Vec4V(dirXYZ*diff.GetW(), length))*ScalarV(V_HALF);

	v2 = SelectFT(IsLessThan(v2.GetW(), v0.GetW()), v2, v0);
	v2 = SelectFT(IsLessThan(v2.GetW(), v1.GetW()), v2, v1);

	return v2;
}

inline void spdSphere::GrowPoint(Vec3V_In point)
{
	m_centerAndRadius = MergeSphereAndPoint(m_centerAndRadius, point);
}

inline void spdSphere::GrowSphere(const spdSphere& sphere)
{
	m_centerAndRadius = MergeSpheres(m_centerAndRadius, sphere.m_centerAndRadius);
}

inline void spdSphere::GrowAABB(Vec3V_In boxMin, Vec3V_In boxMax)
{
	const Vec3V point = SelectFT(IsGreaterThan((boxMax + boxMin)*ScalarV(V_HALF), GetCenter()), boxMin, boxMax);
	m_centerAndRadius = MergeSphereAndPoint(m_centerAndRadius, point);
}

inline void spdSphere::GrowUniform(ScalarV_In distance)
{
	m_centerAndRadius.SetW(GetRadius() + distance);
}

inline FASTRETURNCHECK(ScalarV_Out) spdSphere::InflatePoint(Vec3V_In point) const
{
	return Max(GetRadius(), Mag(GetCenter() - point));
}

inline FASTRETURNCHECK(ScalarV_Out) spdSphere::InflateSphere(const spdSphere& sphere) const
{
	return Max(GetRadius(), Mag(GetCenter() - sphere.GetCenter()) + sphere.GetRadius());
}

inline FASTRETURNCHECK(ScalarV_Out) spdSphere::InflateAABB(Vec3V_In boxMin, Vec3V_In boxMax) const
{
	const Vec3V boxCenter = (boxMax + boxMin)*ScalarV(V_HALF);
	const Vec3V boxExtent = (boxMax - boxMin)*ScalarV(V_HALF);

	return Max(GetRadius(), Mag(Abs(GetCenter() - boxCenter) + boxExtent));
}

inline void spdSphere::TransformSphereScaled(Mat34V_In mat)
{
	const Vec3V   center  = Transform(mat, GetCenter());
	const ScalarV radius  = GetRadius();
	const ScalarV magSqrX = MagSquared(mat.GetCol0());
	const ScalarV magSqrY = MagSquared(mat.GetCol1());
	const ScalarV magSqrZ = MagSquared(mat.GetCol2());
	const ScalarV scale   = Sqrt(Max(Max(magSqrX, magSqrY), magSqrZ));

	m_centerAndRadius = Vec4V(center, radius*scale);
}

inline void spdSphere::TransformSphere(Mat34V_In mat)
{
	const Vec3V   center = Transform(mat, GetCenter());
	const ScalarV radius = GetRadius();

	m_centerAndRadius = Vec4V(center, radius);
}

inline spdBool_Out spdSphere::ContainsPoint(Vec3V_In point) const
{
	const Vec3V   v = GetCenter() - point;
	const ScalarV r = GetRadius();

	return spdBool_Return(IsLessThanOrEqualAll(MagSquared(v), r*r));
}

inline spdBool_Out spdSphere::ContainsPointFlat(Vec2V_In point) const
{
	const Vec2V   v = GetCenterFlat() - point;
	const ScalarV r = GetRadius();

	return spdBool_Return(IsLessThanOrEqualAll(MagSquared(v), r*r));
}

inline spdBool_Out spdSphere::ContainsSphere(const spdSphere& sphere) const
{
	const Vec4V   v = m_centerAndRadius - sphere.m_centerAndRadius;
	const ScalarV d = v.GetW();

	return spdBool_Return(IsLessThanOrEqualAll(MagSquared(v.GetXYZ()), d*d) & IsLessThanOrEqualAll(ScalarV(V_ZERO), d));
}

inline spdBool_Out spdSphere::ContainsSphereFlat(const spdSphere& sphere) const
{
	const Vec4V   v = m_centerAndRadius - sphere.m_centerAndRadius;
	const ScalarV d = v.GetW();

	return spdBool_Return(IsLessThanOrEqualAll(MagSquared(v.GetXY()), d*d) & IsLessThanOrEqualAll(ScalarV(V_ZERO), d));
}

inline spdBool_Out spdSphere::IntersectsSphere(const spdSphere& sphere) const
{
	const Vec3V   v = (m_centerAndRadius - sphere.m_centerAndRadius).GetXYZ();
	const ScalarV r = (m_centerAndRadius + sphere.m_centerAndRadius).GetW();

	return spdBool_Return(IsLessThanOrEqualAll(MagSquared(v), r*r));
}

inline spdBool_Out spdSphere::IntersectsSphereFlat(const spdSphere& sphere) const
{
	const Vec2V   v = (m_centerAndRadius - sphere.m_centerAndRadius).GetXY();
	const ScalarV r = (m_centerAndRadius + sphere.m_centerAndRadius).GetW();

	return spdBool_Return(IsLessThanOrEqualAll(MagSquared(v), r*r));
}

} // namespace rage

#endif // SPATIALDATA_SPHERE_H
