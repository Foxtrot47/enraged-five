//
// spatialdata/aabb.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
// 

#include "file/token.h"
#include "data/safestruct.h"
#include "parser/treenode.h"

#include "spatialdata/aabb.h"

using namespace rage;

#if !__SPU

void spdAABB::SaveData(fiAsciiTokenizer& tok) const
{
	Assertf(0, "spdAABB::SaveData?");
	tok.StartLine();
	tok.PutStr("AABB");
	tok.PutStr(" %f %f %f", m_min.GetXf(), m_min.GetYf(), m_min.GetZf());
	tok.PutStr(" %f %f %f", m_max.GetXf(), m_max.GetYf(), m_max.GetZf());
	tok.EndLine();
}

void spdAABB::LoadData(fiAsciiTokenizer& tok)
{
	Assertf(0, "spdAABB::LoadData?");
	tok.CheckToken("AABB");
	tok.GetVector((Vector3&)m_min);
	tok.GetVector((Vector3&)m_max);
}

void spdAABB::PreLoad(parTreeNode* node)
{
	node->FindValueFromPath("MinAndBoundingRadius", (Vector3&)m_min);
	node->FindValueFromPath("MaxAndInscribedRadius", (Vector3&)m_max);
}

#if __DECLARESTRUCT

void spdAABB::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(spdAABB)
	SSTRUCT_FIELD(spdAABB, m_min)
	SSTRUCT_FIELD(spdAABB, m_max)
	SSTRUCT_END(spdAABB)
}

#endif // __DECLARESTRUCT
#endif // !__SPU
