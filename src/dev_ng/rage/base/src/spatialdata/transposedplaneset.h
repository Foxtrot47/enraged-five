// 
// spatialdata/transposedplaneset.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_TRANSPOSEDPLANESET_H
#define SPATIALDATA_TRANSPOSEDPLANESET_H

#include "spatialdata/spatialdata.h"
#include "spatialdata/sphere.h"
#include "spatialdata/aabb.h"
#include "grcore/device.h"

namespace rage {

class grcViewport;

class spdTransposedPlaneSet8
{
public:
	inline spdTransposedPlaneSet8() {}
	inline spdTransposedPlaneSet8(const grcViewport& viewport) { Set(viewport); }

	inline void SetInfinite();

	void Set(const grcViewport& viewport, bool bNearPlane = true, bool bFarPlane = true, Vec4V_In plane6 = Vec4V(V_ZERO), Vec4V_In plane7 = Vec4V(V_ZERO) NV_SUPPORT_ONLY(, bool useStereoFrustum=false));

#if NV_SUPPORT
	void SetStereo(const grcViewport& viewport, bool bNearPlane = true, bool bFarPlane = true, Vec4V_In plane6 = Vec4V(V_ZERO), Vec4V_In plane7 = Vec4V(V_ZERO));
#endif

	inline void SetPlanes(Vec4V_In plane0, Vec4V_In plane1, Vec4V_In plane2, Vec4V_In plane3, Vec4V_In plane4, Vec4V_In plane5, Vec4V_In plane6, Vec4V_In plane7);
	inline void SetPlanes(const Vec4V planes[8]);
	
	inline void GetPlanes(Vec4V_InOut plane0, Vec4V_InOut plane1, Vec4V_InOut plane2, Vec4V_InOut plane3, Vec4V_InOut plane4, Vec4V_InOut plane5, Vec4V_InOut plane6, Vec4V_InOut plane7) const;
	inline void GetPlanes(Vec4V planes[8]) const;

	inline Vec4V_Out GetPlanes0123X() const { return m_ax; }
	inline Vec4V_Out GetPlanes0123Y() const { return m_ay; }
	inline Vec4V_Out GetPlanes0123Z() const { return m_az; }
	inline Vec4V_Out GetPlanes0123W() const { return m_aw; }

	inline Vec4V_Out GetPlanes4567X() const { return m_bx; }
	inline Vec4V_Out GetPlanes4567Y() const { return m_by; }
	inline Vec4V_Out GetPlanes4567Z() const { return m_bz; }
	inline Vec4V_Out GetPlanes4567W() const { return m_bw; }

	inline spdBool_Out ContainsPoint(Vec3V_In point) const;
	inline spdBool_Out ContainsSphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainsAABB(const spdAABB& box) const;

	inline spdBool_Out IntersectsSphere(const spdSphere& sphere) const;
	inline spdBool_Out IntersectsAABB(const spdAABB& box) const;

	// these functions are just wrappers, please do no use them
	inline spdBool_Out IntersectsOrContainsSphere(const spdSphere& sphere) const { return IntersectsSphere(sphere); }
	inline spdBool_Out IntersectsOrContainsAABB(const spdAABB& box) const { return IntersectsAABB(box); }

	// only use these if you need _both_ the intersection and containment tests
	inline bool IntersectsOrContainsSphere(const spdSphere& sphere, u32* contains) const;
	inline bool IntersectsOrContainsAABB(const spdAABB& box, u32* contains) const;

protected:
	Vec4V m_ax; // transposed planes set A
	Vec4V m_ay;
	Vec4V m_az;
	Vec4V m_aw;

	Vec4V m_bx; // transposed planed set B
	Vec4V m_by;
	Vec4V m_bz;
	Vec4V m_bw;
};

class spdTransposedPlaneSet4
{
public:
	inline spdTransposedPlaneSet4() {}

	inline void SetInfinite();

	inline void SetPlanes(Vec4V_In plane0, Vec4V_In plane1, Vec4V_In plane2, Vec4V_In plane3);
	inline void SetPlanes(const Vec4V planes[4]);

	inline void GetPlanes(Vec4V_InOut plane0, Vec4V_InOut plane1, Vec4V_InOut plane2, Vec4V_InOut plane3) const;
	inline void GetPlanes(Vec4V planes[4]) const;

	inline Vec4V_Out GetPlanes0123X() const { return m_ax; }
	inline Vec4V_Out GetPlanes0123Y() const { return m_ay; }
	inline Vec4V_Out GetPlanes0123Z() const { return m_az; }
	inline Vec4V_Out GetPlanes0123W() const { return m_aw; }

	inline spdBool_Out ContainsPoint(Vec3V_In point) const;
	inline spdBool_Out ContainsSphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainsAABB(const spdAABB& box) const;

	inline spdBool_Out IntersectsSphere(const spdSphere& sphere) const;
	inline spdBool_Out IntersectsAABB(const spdAABB& box) const;

	// these functions are just wrappers, please do no use them
	inline spdBool_Out IntersectsOrContainsSphere(const spdSphere& sphere) const { return IntersectsSphere(sphere);	}
	inline spdBool_Out IntersectsOrContainsAABB(const spdAABB& box) const { return IntersectsAABB(box); }

	// only use these if you need _both_ the intersection and containment tests
	inline bool IntersectsOrContainsSphere(const spdSphere& sphere, u32* contains) const;
	inline bool IntersectsOrContainsAABB(const spdAABB& box, u32* contains) const;

protected:
	Vec4V m_ax; // transposed planes set A
	Vec4V m_ay;
	Vec4V m_az;
	Vec4V m_aw;
};

// ================================================================================================

inline void spdTransposedPlaneSet8::SetInfinite()
{
	const Vec4V zero(V_ZERO);
	m_ax = zero;
	m_ay = zero;
	m_az = zero;
	m_aw = zero;
	m_bx = zero;
	m_by = zero;
	m_bz = zero;
	m_bw = zero;
}

inline void spdTransposedPlaneSet8::SetPlanes(Vec4V_In plane0, Vec4V_In plane1, Vec4V_In plane2, Vec4V_In plane3, Vec4V_In plane4, Vec4V_In plane5, Vec4V_In plane6, Vec4V_In plane7)
{
	Transpose4x4(m_ax, m_ay, m_az, m_aw, plane0, plane1, plane2, plane3);
	Transpose4x4(m_bx, m_by, m_bz, m_bw, plane4, plane5, plane6, plane7);
}

inline void spdTransposedPlaneSet8::SetPlanes(const Vec4V planes[8])
{
	Transpose4x4(m_ax, m_ay, m_az, m_aw, planes[0], planes[1], planes[2], planes[3]);
	Transpose4x4(m_bx, m_by, m_bz, m_bw, planes[4], planes[5], planes[6], planes[7]);
}

inline void spdTransposedPlaneSet8::GetPlanes(Vec4V_InOut plane0, Vec4V_InOut plane1, Vec4V_InOut plane2, Vec4V_InOut plane3, Vec4V_InOut plane4, Vec4V_InOut plane5, Vec4V_InOut plane6, Vec4V_InOut plane7) const
{
	Transpose4x4(plane0, plane1, plane2, plane3, m_ax, m_ay, m_az, m_aw);
	Transpose4x4(plane4, plane5, plane6, plane7, m_bx, m_by, m_bz, m_bw);
}

inline void spdTransposedPlaneSet8::GetPlanes(Vec4V planes[8]) const
{
	Transpose4x4(planes[0], planes[1], planes[2], planes[3], m_ax, m_ay, m_az, m_aw);
	Transpose4x4(planes[4], planes[5], planes[6], planes[7], m_bx, m_by, m_bz, m_bw);
}

inline spdBool_Out spdTransposedPlaneSet8::ContainsPoint(Vec3V_In point) const
{
	const ScalarV x = SplatX(point);
	const ScalarV y = SplatY(point);
	const ScalarV z = SplatZ(point);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);
	const Vec4V bd = AddScaled(AddScaled(AddScaled(m_bw, m_bz, z), m_by, y), m_bx, x);

	return spdBool_Return(IsLessThanOrEqualAll(Max(ad, bd), Vec4V(V_ZERO)));
}

inline spdBool_Out spdTransposedPlaneSet8::ContainsSphere(const spdSphere& sphere) const
{
	const Vec3V   center = sphere.GetCenter();
	const ScalarV radius = sphere.GetRadius();

	const ScalarV x = SplatX(center);
	const ScalarV y = SplatY(center);
	const ScalarV z = SplatZ(center);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);
	const Vec4V bd = AddScaled(AddScaled(AddScaled(m_bw, m_bz, z), m_by, y), m_bx, x);

	return spdBool_Return(IsLessThanOrEqualAll(Max(ad, bd), -Vec4V(radius)));
}

inline spdBool_Out spdTransposedPlaneSet8::ContainsAABB(const spdAABB& box) const
{
	const Vec3V boxMin = box.GetMin();
	const Vec3V boxMax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(boxMin));
	const Vec4V ymin = Vec4V(SplatY(boxMin));
	const Vec4V zmin = Vec4V(SplatZ(boxMin));
	const Vec4V xmax = Vec4V(SplatX(boxMax));
	const Vec4V ymax = Vec4V(SplatY(boxMax));
	const Vec4V zmax = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const VecBoolV ax_GT_0 = IsGreaterThan(m_ax, zero);
	const VecBoolV ay_GT_0 = IsGreaterThan(m_ay, zero);
	const VecBoolV az_GT_0 = IsGreaterThan(m_az, zero);

	const VecBoolV bx_GT_0 = IsGreaterThan(m_bx, zero);
	const VecBoolV by_GT_0 = IsGreaterThan(m_by, zero);
	const VecBoolV bz_GT_0 = IsGreaterThan(m_bz, zero);

	const Vec4V ax_p = SelectFT(ax_GT_0, xmin, xmax); // select p-vertex
	const Vec4V ay_p = SelectFT(ay_GT_0, ymin, ymax);
	const Vec4V az_p = SelectFT(az_GT_0, zmin, zmax);

	const Vec4V bx_p = SelectFT(bx_GT_0, xmin, xmax);
	const Vec4V by_p = SelectFT(by_GT_0, ymin, ymax);
	const Vec4V bz_p = SelectFT(bz_GT_0, zmin, zmax);

	const Vec4V ad_p = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_p), m_ay, ay_p), m_ax, ax_p);
	const Vec4V bd_p = AddScaled(AddScaled(AddScaled(m_bw, m_bz, bz_p), m_by, by_p), m_bx, bx_p);

	return spdBool_Return(IsLessThanOrEqualAll(Max(ad_p, bd_p), zero));
}

inline spdBool_Out spdTransposedPlaneSet8::IntersectsSphere(const spdSphere& sphere) const
{
	const Vec3V   center = sphere.GetCenter();
	const ScalarV radius = sphere.GetRadius();

	const ScalarV x = SplatX(center);
	const ScalarV y = SplatY(center);
	const ScalarV z = SplatZ(center);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);
	const Vec4V bd = AddScaled(AddScaled(AddScaled(m_bw, m_bz, z), m_by, y), m_bx, x);

	return spdBool_Return(IsLessThanOrEqualAll(Max(ad, bd), Vec4V(radius)));
}

inline spdBool_Out spdTransposedPlaneSet8::IntersectsAABB(const spdAABB& box) const
{
	const Vec3V boxMin = box.GetMin();
	const Vec3V boxMax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(boxMin));
	const Vec4V ymin = Vec4V(SplatY(boxMin));
	const Vec4V zmin = Vec4V(SplatZ(boxMin));
	const Vec4V xmax = Vec4V(SplatX(boxMax));
	const Vec4V ymax = Vec4V(SplatY(boxMax));
	const Vec4V zmax = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const VecBoolV ax_GT_0 = IsGreaterThan(m_ax, zero);
	const VecBoolV ay_GT_0 = IsGreaterThan(m_ay, zero);
	const VecBoolV az_GT_0 = IsGreaterThan(m_az, zero);

	const VecBoolV bx_GT_0 = IsGreaterThan(m_bx, zero);
	const VecBoolV by_GT_0 = IsGreaterThan(m_by, zero);
	const VecBoolV bz_GT_0 = IsGreaterThan(m_bz, zero);

	const Vec4V ax_n = SelectFT(ax_GT_0, xmax, xmin); // select n-vertex
	const Vec4V ay_n = SelectFT(ay_GT_0, ymax, ymin);
	const Vec4V az_n = SelectFT(az_GT_0, zmax, zmin);

	const Vec4V bx_n = SelectFT(bx_GT_0, xmax, xmin);
	const Vec4V by_n = SelectFT(by_GT_0, ymax, ymin);
	const Vec4V bz_n = SelectFT(bz_GT_0, zmax, zmin);

	const Vec4V ad_n = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_n), m_ay, ay_n), m_ax, ax_n);
	const Vec4V bd_n = AddScaled(AddScaled(AddScaled(m_bw, m_bz, bz_n), m_by, by_n), m_bx, bx_n);

	return spdBool_Return(IsLessThanOrEqualAll(Max(ad_n, bd_n), zero));
}

inline bool spdTransposedPlaneSet8::IntersectsOrContainsSphere(const spdSphere& sphere, u32* contains) const
{
	const Vec3V   center = sphere.GetCenter();
	const ScalarV radius = sphere.GetRadius();

	const ScalarV x = SplatX(center);
	const ScalarV y = SplatY(center);
	const ScalarV z = SplatZ(center);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);
	const Vec4V bd = AddScaled(AddScaled(AddScaled(m_bw, m_bz, z), m_by, y), m_bx, x);

	const Vec4V d = Max(ad, bd);

	if (IsLessThanOrEqualAll(d, Vec4V(radius)) != 0)
	{
		*contains = IsLessThanOrEqualAll(d, -Vec4V(radius));
		return true;
	}

	return false;
}

inline bool spdTransposedPlaneSet8::IntersectsOrContainsAABB(const spdAABB& box, u32* contains) const
{
	const Vec3V boxMin = box.GetMin();
	const Vec3V boxMax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(boxMin));
	const Vec4V ymin = Vec4V(SplatY(boxMin));
	const Vec4V zmin = Vec4V(SplatZ(boxMin));
	const Vec4V xmax = Vec4V(SplatX(boxMax));
	const Vec4V ymax = Vec4V(SplatY(boxMax));
	const Vec4V zmax = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const VecBoolV ax_GT_0 = IsGreaterThan(m_ax, zero);
	const VecBoolV ay_GT_0 = IsGreaterThan(m_ay, zero);
	const VecBoolV az_GT_0 = IsGreaterThan(m_az, zero);

	const VecBoolV bx_GT_0 = IsGreaterThan(m_bx, zero);
	const VecBoolV by_GT_0 = IsGreaterThan(m_by, zero);
	const VecBoolV bz_GT_0 = IsGreaterThan(m_bz, zero);

	const Vec4V ax_n = SelectFT(ax_GT_0, xmax, xmin); // select n-vertex
	const Vec4V ay_n = SelectFT(ay_GT_0, ymax, ymin);
	const Vec4V az_n = SelectFT(az_GT_0, zmax, zmin);

	const Vec4V bx_n = SelectFT(bx_GT_0, xmax, xmin);
	const Vec4V by_n = SelectFT(by_GT_0, ymax, ymin);
	const Vec4V bz_n = SelectFT(bz_GT_0, zmax, zmin);

	const Vec4V ad_n = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_n), m_ay, ay_n), m_ax, ax_n);
	const Vec4V bd_n = AddScaled(AddScaled(AddScaled(m_bw, m_bz, bz_n), m_by, by_n), m_bx, bx_n);

	if (IsLessThanOrEqualAll(Max(ad_n, bd_n), zero) != 0)
	{
		const Vec4V ax_p = SelectFT(ax_GT_0, xmin, xmax); // select p-vertex
		const Vec4V ay_p = SelectFT(ay_GT_0, ymin, ymax);
		const Vec4V az_p = SelectFT(az_GT_0, zmin, zmax);

		const Vec4V bx_p = SelectFT(bx_GT_0, xmin, xmax);
		const Vec4V by_p = SelectFT(by_GT_0, ymin, ymax);
		const Vec4V bz_p = SelectFT(bz_GT_0, zmin, zmax);

		const Vec4V ad_p = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_p), m_ay, ay_p), m_ax, ax_p);
		const Vec4V bd_p = AddScaled(AddScaled(AddScaled(m_bw, m_bz, bz_p), m_by, by_p), m_bx, bx_p);

		*contains = IsLessThanOrEqualAll(Max(ad_p, bd_p), zero);
		return true;
	}

	return false;
}

// ================================================================================================

inline void spdTransposedPlaneSet4::SetInfinite()
{
	const Vec4V zero(V_ZERO);
	m_ax = zero;
	m_ay = zero;
	m_az = zero;
	m_aw = zero;
}

inline void spdTransposedPlaneSet4::SetPlanes(Vec4V_In plane0, Vec4V_In plane1, Vec4V_In plane2, Vec4V_In plane3)
{
	Transpose4x4(m_ax, m_ay, m_az, m_aw, plane0, plane1, plane2, plane3);
}

inline void spdTransposedPlaneSet4::SetPlanes(const Vec4V planes[4])
{
	Transpose4x4(m_ax, m_ay, m_az, m_aw, planes[0], planes[1], planes[2], planes[3]);
}

inline void spdTransposedPlaneSet4::GetPlanes(Vec4V_InOut plane0, Vec4V_InOut plane1, Vec4V_InOut plane2, Vec4V_InOut plane3) const
{
	Transpose4x4(plane0, plane1, plane2, plane3, m_ax, m_ay, m_az, m_aw);
}

inline void spdTransposedPlaneSet4::GetPlanes(Vec4V planes[4]) const
{
	Transpose4x4(planes[0], planes[1], planes[2], planes[3], m_ax, m_ay, m_az, m_aw);
}

inline spdBool_Out spdTransposedPlaneSet4::ContainsPoint(Vec3V_In point) const
{
	const ScalarV x = SplatX(point);
	const ScalarV y = SplatY(point);
	const ScalarV z = SplatZ(point);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);

	return spdBool_Return(IsLessThanOrEqualAll(ad, Vec4V(V_ZERO)));
}

inline spdBool_Out spdTransposedPlaneSet4::ContainsSphere(const spdSphere& sphere) const
{
	const Vec3V   center = sphere.GetCenter();
	const ScalarV radius = sphere.GetRadius();

	const ScalarV x = SplatX(center);
	const ScalarV y = SplatY(center);
	const ScalarV z = SplatZ(center);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);

	return spdBool_Return(IsLessThanOrEqualAll(ad, -Vec4V(radius)));
}

inline spdBool_Out spdTransposedPlaneSet4::ContainsAABB(const spdAABB& box) const
{
	const Vec3V boxMin = box.GetMin();
	const Vec3V boxMax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(boxMin));
	const Vec4V ymin = Vec4V(SplatY(boxMin));
	const Vec4V zmin = Vec4V(SplatZ(boxMin));
	const Vec4V xmax = Vec4V(SplatX(boxMax));
	const Vec4V ymax = Vec4V(SplatY(boxMax));
	const Vec4V zmax = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const VecBoolV ax_GT_0 = IsGreaterThan(m_ax, zero);
	const VecBoolV ay_GT_0 = IsGreaterThan(m_ay, zero);
	const VecBoolV az_GT_0 = IsGreaterThan(m_az, zero);

	const Vec4V ax_p = SelectFT(ax_GT_0, xmin, xmax); // select p-vertex
	const Vec4V ay_p = SelectFT(ay_GT_0, ymin, ymax);
	const Vec4V az_p = SelectFT(az_GT_0, zmin, zmax);

	const Vec4V ad_p = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_p), m_ay, ay_p), m_ax, ax_p);

	return spdBool_Return(IsLessThanOrEqualAll(ad_p, zero));
}

inline spdBool_Out spdTransposedPlaneSet4::IntersectsSphere(const spdSphere& sphere) const
{
	const Vec3V   center = sphere.GetCenter();
	const ScalarV radius = sphere.GetRadius();

	const ScalarV x = SplatX(center);
	const ScalarV y = SplatY(center);
	const ScalarV z = SplatZ(center);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);

	return spdBool_Return(IsLessThanOrEqualAll(ad, Vec4V(radius)));
}

inline spdBool_Out spdTransposedPlaneSet4::IntersectsAABB(const spdAABB& box) const
{
	const Vec3V boxMin = box.GetMin();
	const Vec3V boxMax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(boxMin));
	const Vec4V ymin = Vec4V(SplatY(boxMin));
	const Vec4V zmin = Vec4V(SplatZ(boxMin));
	const Vec4V xmax = Vec4V(SplatX(boxMax));
	const Vec4V ymax = Vec4V(SplatY(boxMax));
	const Vec4V zmax = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const VecBoolV ax_GT_0 = IsGreaterThan(m_ax, zero);
	const VecBoolV ay_GT_0 = IsGreaterThan(m_ay, zero);
	const VecBoolV az_GT_0 = IsGreaterThan(m_az, zero);

	const Vec4V ax_n = SelectFT(ax_GT_0, xmax, xmin); // select n-vertex
	const Vec4V ay_n = SelectFT(ay_GT_0, ymax, ymin);
	const Vec4V az_n = SelectFT(az_GT_0, zmax, zmin);

	const Vec4V ad_n = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_n), m_ay, ay_n), m_ax, ax_n);

	return spdBool_Return(IsLessThanOrEqualAll(ad_n, zero));
}

inline bool spdTransposedPlaneSet4::IntersectsOrContainsSphere(const spdSphere& sphere, u32* contains) const
{
	const Vec3V   center = sphere.GetCenter();
	const ScalarV radius = sphere.GetRadius();

	const ScalarV x = SplatX(center);
	const ScalarV y = SplatY(center);
	const ScalarV z = SplatZ(center);

	const Vec4V ad = AddScaled(AddScaled(AddScaled(m_aw, m_az, z), m_ay, y), m_ax, x);

	if (IsLessThanOrEqualAll(ad, Vec4V(radius)) != 0)
	{
		*contains = IsLessThanOrEqualAll(ad, -Vec4V(radius));
		return true;
	}

	return false;
}

inline bool spdTransposedPlaneSet4::IntersectsOrContainsAABB(const spdAABB& box, u32* contains) const
{
	const Vec3V boxMin = box.GetMin();
	const Vec3V boxMax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(boxMin));
	const Vec4V ymin = Vec4V(SplatY(boxMin));
	const Vec4V zmin = Vec4V(SplatZ(boxMin));
	const Vec4V xmax = Vec4V(SplatX(boxMax));
	const Vec4V ymax = Vec4V(SplatY(boxMax));
	const Vec4V zmax = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const VecBoolV ax_GT_0 = IsGreaterThan(m_ax, zero);
	const VecBoolV ay_GT_0 = IsGreaterThan(m_ay, zero);
	const VecBoolV az_GT_0 = IsGreaterThan(m_az, zero);

	const Vec4V ax_n = SelectFT(ax_GT_0, xmax, xmin); // select n-vertex
	const Vec4V ay_n = SelectFT(ay_GT_0, ymax, ymin);
	const Vec4V az_n = SelectFT(az_GT_0, zmax, zmin);

	const Vec4V ad_n = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_n), m_ay, ay_n), m_ax, ax_n);

	if (IsLessThanOrEqualAll(ad_n, zero) != 0)
	{
		const Vec4V ax_p = SelectFT(ax_GT_0, xmin, xmax); // select p-vertex
		const Vec4V ay_p = SelectFT(ay_GT_0, ymin, ymax);
		const Vec4V az_p = SelectFT(az_GT_0, zmin, zmax);

		const Vec4V ad_p = AddScaled(AddScaled(AddScaled(m_aw, m_az, az_p), m_ay, ay_p), m_ax, ax_p);

		*contains = IsLessThanOrEqualAll(ad_p, zero);
		return true;
	}

	return false;
}

} // namespace rage

#endif // SPATIALDATA_TRANSPOSEDPLANESET_H
