<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::spdGrid2D" onPostLoad="InitCalculatedMembers">
	<pad bytes="4"/> <!-- int numcells -->
	<int name="m_MinCellX"/>
	<int name="m_MaxCellX"/>
	<int name="m_MinCellY"/>
	<int name="m_MaxCellY"/>
	<pad bytes="16"/> <!-- 4x floats -->
	<float name="m_CellDimX"/>
	<float name="m_CellDimY"/>
	<pad bytes="8"/> <!-- 2x floats -->
</structdef>

</ParserSchema>