//
// spatialdata/quadtree.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "quadtree.h"


const int rage::QuadTreeConstants::kLut[4][4] = 
{
	{ 0, 1, 2, 3 },
	{ 1, 3, 0, 2 },
	{ 2, 0, 3, 1 },
	{ 3, 2, 1, 0 },
};
