//
// spatialdata/grid2d.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SPATIALDATA_GRID2D_H
#define SPATIALDATA_GRID2D_H

#include "data/base.h"
#include "parser/macros.h"
#include "vector/vector2.h"
#include "vector/vector3.h"

namespace rage {

class fiAsciiTokenizer;
class datResource;


//=============================================================================
// spdGrid2D

/*
PURPOSE:
	Defines a 2-dimensional grid, including maps to/from cell
	indices, calculating cell extents, etc.  spdGrid2D is only
	the grid itself, and does not contain anytyhing in the cells
	at this level.
<FLAG Component>
*/
class spdGrid2D : public datBase
{
public:
	spdGrid2D ();
	spdGrid2D (datResource &rsc);

#if __DECLARESTRUCT
	// PURPOSE:	Needed for offline resource generation.
	// PARAMS:	s		- The datTypeStruct object to use.
	void DeclareStruct(datTypeStruct &s);
#endif

	DECLARE_PLACE(spdGrid2D);

	void Init (float minX, float maxX, float dimX, float minY, float maxY, float dimY);
	void InitCells(int minX, int maxX, float dimX, int minY, int maxY, float dimY);

	void Load (fiAsciiTokenizer & tok);
	void Save (fiAsciiTokenizer & tok);

	// accessors
	int GetMaxCellX () const 
	{	return m_MaxCellX;	}

	int GetMinCellX () const
	{	return m_MinCellX;	}

	int GetMaxCellY () const
	{	return m_MaxCellY;	}

	int GetMinCellY () const
	{	return m_MinCellY;	}

	float GetMaxGridX () const
	{ return m_MaxX; }

	float GetMinGridX () const
	{ return m_MinX; }

	float GetMaxGridY () const
	{ return m_MaxY; }

	float GetMinGridY () const
	{ return m_MinY; }

	float GetCellDimX () const
	{	return m_CellDimX;	}

	float GetCellDimY () const
	{	return m_CellDimY;	}

	int GetNumCells () const
	{	return m_NumCells;	}

	int CalcCellIndex (int cellX, int cellY) const
	{	return (cellY - m_MinCellY) * (m_MaxCellX-m_MinCellX+1) + (cellX - m_MinCellX);	}

	int CalcCellXFromIndex (int cellIndex) const
	{	return cellIndex % (m_MaxCellX-m_MinCellX+1) + m_MinCellX;	}

	int CalcCellYFromIndex (int cellIndex) const
	{	return cellIndex / (m_MaxCellX-m_MinCellX+1) + m_MinCellY;	}

	void CalcCellExtents (int cellX, int cellY, Vector2 & cellMin, Vector2 & cellMax) const
	{	cellMin.x = cellX * m_CellDimX; cellMin.y = cellY * m_CellDimY; cellMax.x = cellMin.x + m_CellDimX; cellMax.y = cellMin.y + m_CellDimY;	}

	void CalcCellExtents (int cellIndex, Vector2 & cellMin, Vector2 & cellMax) const
	{	CalcCellExtents(CalcCellXFromIndex(cellIndex),CalcCellYFromIndex(cellIndex),cellMin,cellMax);	}

	float CalcCellMinX (int cellX) const
	{	return cellX * m_CellDimX;	}

	float CalcCellMaxX (int cellX) const
	{	return (cellX + 1) * m_CellDimX;	}

	float CalcCellMinY (int cellY) const
	{	return cellY * m_CellDimY;	}

	float CalcCellMaxY (int cellY) const
	{	return (cellY + 1) * m_CellDimY;	}

	void CalcCellCenter (int cellIndex, Vector2 & center) const
	{	center.x = CalcCellXFromIndex(cellIndex) * m_CellDimX + m_CellDimX/2.0f; center.y = CalcCellYFromIndex(cellIndex) * m_CellDimY + m_CellDimY/2.0f;	}

	void CalcCellCenter (int cellIndex, Vector3 & center) const
	{	center.x = CalcCellXFromIndex(cellIndex) * m_CellDimX + m_CellDimX/2.0f; center.y = 0.0f; center.z = CalcCellYFromIndex(cellIndex) * m_CellDimY + m_CellDimY/2.0f;	}

	bool CalcCellX (int &cellX, float x) const
	{	cellX = (int)floorf(x * m_CellInvDimX); return (cellX>=m_MinCellX && cellX<=m_MaxCellX);	}

	bool CalcCellY (int &cellY, float y) const
	{	cellY = (int)floorf(y * m_CellInvDimY); return (cellY>=m_MinCellY && cellY<=m_MaxCellY);	}

	void CalcClampedCellX (int &cellX, float x) const
	{	cellX = (int)floorf(x * m_CellInvDimX); cellX = Clamp(cellX, m_MinCellX, m_MaxCellX); }

	void CalcClampedCellY (int &cellY, float y) const
	{	cellY = (int)floorf(y * m_CellInvDimY); cellY = Clamp(cellY, m_MinCellY, m_MaxCellY); }

	bool CalcCellXY (int & cellX, int & cellY, const Vector2 & v) const
	{	bool inX = CalcCellX(cellX,v.x); bool inY = CalcCellY(cellY,v.y); return inX && inY;	}

	bool CalcCellIndex (int &cellIndex, float x, float y) const;

	void CalcClampedCellIndex (int &cellIndex, float x, float y) const;

	bool CalcCellRegion(float rectX1, float rectY1, float rectX2, float rectY2,
			int &x1Out, int &y1Out, int &x2Out, int &y2Out) const;

	bool TestCircle(float centerX, float centerY, float radius,
			int cellX, int cellY) const;

	bool CheckInGrid (int cellX, int cellY) const
	{	return (cellX>=m_MinCellX && cellX<=m_MaxCellX && cellY>=m_MinCellY && cellY<=m_MaxCellY);	}

	// Displays grid information to standard output
	void Print(char* header) const;

	void InitCalculatedMembers ();

protected:
	PAR_PARSABLE;

	void ComputeCellDimInverse();
	void InitCalculatedMembers2();

	int m_NumCells;

	// grid extents in cells
	int m_MinCellX;
	int m_MaxCellX;
	int m_MinCellY;
	int m_MaxCellY;

	// grid extents in reals
	float m_MinX;
	float m_MaxX;
	float m_MinY;
	float m_MaxY;

	// cell dimensions
	float m_CellDimX;
	float m_CellDimY;
	float m_CellInvDimX;
	float m_CellInvDimY;
};


//==============================================================
// spdGrid2DContainer

/*
PURPOSE:
	Contains the elements in the grid.
<FLAG Component>
*/
template<typename T> class spdGrid2DContainer : public spdGrid2D
{
public:
	spdGrid2DContainer();
	~spdGrid2DContainer();

	void Init (float minX, float maxX, float dimX, float minY, float maxY, float dimY);

	const T & GetData (int cellIndex) const
	{
		FastAssert(m_Initialized && cellIndex>=0 && cellIndex<=m_NumCells);
		return m_Elements[cellIndex];
	}

	T & GetData (int cellIndex)
	{
		FastAssert(m_Initialized && cellIndex>=0 && cellIndex<=m_NumCells);
		return m_Elements[cellIndex];
	}

	const T & GetData (int cellX, int cellY) const
	{
		FastAssert(m_Initialized);
		int cellIndex = CalcCellIndex(cellX,cellY);
		return m_Elements[cellIndex];
	}

	T & GetData (int cellX, int cellY)
	{
		FastAssert(m_Initialized);
		int cellIndex = CalcCellIndex(cellX,cellY);
		return m_Elements[cellIndex];
	}

	const T * GetData (const Vector2 & point) const
	{
		FastAssert(m_Initialized);
		int cellIndex;
		bool insideGrid = CalcCellIndex(cellIndex,point.x,point.y);
		if (insideGrid)
		{
			return &m_Elements[cellIndex];
		}
		else
		{
			return NULL;
		}
	}


	T * GetData (const Vector2 & point)
	{
		FastAssert(m_Initialized);
		int cellIndex;
		bool insideGrid = CalcCellIndex(cellIndex,point.x,point.y);
		if (insideGrid)
		{
			return &m_Elements[cellIndex];
		}
		else
		{
			return NULL;
		}
	}

protected:
	bool m_Initialized;
	T * m_Elements;
};


template<typename T> spdGrid2DContainer<T>::spdGrid2DContainer()
	: spdGrid2D(), m_Initialized(false), m_Elements(NULL)
{
}


template<typename T> spdGrid2DContainer<T>::~spdGrid2DContainer()
{
	delete [] m_Elements;
}


template<typename T> void spdGrid2DContainer<T>::Init
	(float minX, float maxX, float dimX, float minY, float maxY, float dimY)
{
	FastAssert(!m_Initialized);
	spdGrid2D::Init(minX,maxX,dimX,minY,maxY,dimY);
	m_Elements = rage_new T [m_NumCells];
	m_Initialized = true;
}

} // namespace rage

#endif

/* End of file spatialdata/grid2d.h */
