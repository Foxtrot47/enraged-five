// 
// spatialdata/aabb_inline.h 
// 
// Copyright (C) 2013 Rockstar Games.  All Rights Reserved. 
// 

inline spdAABB TransformAABB(Mat33V_In localToWorld, const spdAABB& localBox)
{
	const Vec3V center = Multiply(localToWorld, localBox.GetCenter());
	const Vec3V extent = Multiply(Mat33V(Abs(localToWorld.GetCol0()), Abs(localToWorld.GetCol1()), Abs(localToWorld.GetCol2())), localBox.GetExtent());
	const Vec3V boxMin = center - extent;
	const Vec3V boxMax = center + extent;

	return spdAABB(boxMin, boxMax);
}

inline spdAABB TransformAABB(Mat34V_In localToWorld, const spdAABB& localBox)
{
	const Vec3V center = Transform(localToWorld, localBox.GetCenter());
	const Vec3V extent = Multiply(Mat33V(Abs(localToWorld.GetCol0()), Abs(localToWorld.GetCol1()), Abs(localToWorld.GetCol2())), localBox.GetExtent());
	const Vec3V boxMin = center - extent;
	const Vec3V boxMax = center + extent;

	return spdAABB(boxMin, boxMax);
}

inline spdAABB TransformAABB_alt(Mat33V_In localToWorld, const spdAABB& localBox)
{
	const Vec3V minX = localToWorld.GetCol0()*localBox.GetMin().GetX();
	const Vec3V minY = localToWorld.GetCol1()*localBox.GetMin().GetY();
	const Vec3V minZ = localToWorld.GetCol2()*localBox.GetMin().GetZ();
	const Vec3V maxX = localToWorld.GetCol0()*localBox.GetMax().GetX();
	const Vec3V maxY = localToWorld.GetCol1()*localBox.GetMax().GetY();
	const Vec3V maxZ = localToWorld.GetCol2()*localBox.GetMax().GetZ();
#if __SPU
	const VecBoolV cmpX = IsLessThan(minX, maxX);
	const VecBoolV cmpY = IsLessThan(minY, maxY);
	const VecBoolV cmpZ = IsLessThan(minZ, maxZ);

	const Vec3V minV = SelectFT(cmpX, maxX, minX) + SelectFT(cmpY, maxY, minY) + SelectFT(cmpZ, maxZ, minZ);
	const Vec3V maxV = SelectFT(cmpX, minX, maxX) + SelectFT(cmpY, minY, maxY) + SelectFT(cmpZ, minZ, maxZ);
#else
	const Vec3V minV = Min(minX, maxX) + Min(minY, maxY) + Min(minZ, maxZ);
	const Vec3V maxV = Max(minX, maxX) + Max(minY, maxY) + Max(minZ, maxZ);
#endif
	return spdAABB(minV, maxV);
}

inline spdAABB TransformAABB_alt(Mat34V_In localToWorld, const spdAABB& localBox)
{
	const Vec3V minX = localToWorld.GetCol0()*localBox.GetMin().GetX();
	const Vec3V minY = localToWorld.GetCol1()*localBox.GetMin().GetY();
	const Vec3V minZPlusCol3 = AddScaled(localToWorld.GetCol3(), localToWorld.GetCol2(), localBox.GetMin().GetZ());
	const Vec3V maxX = localToWorld.GetCol0()*localBox.GetMax().GetX();
	const Vec3V maxY = localToWorld.GetCol1()*localBox.GetMax().GetY();
	const Vec3V maxZPlusCol3 = AddScaled(localToWorld.GetCol3(), localToWorld.GetCol2(), localBox.GetMax().GetZ());
#if __SPU
	const VecBoolV cmpX = IsLessThan(minX, maxX);
	const VecBoolV cmpY = IsLessThan(minY, maxY);
	const VecBoolV cmpZ = IsLessThan(minZPlusCol3, maxZPlusCol3);

	const Vec3V minV = SelectFT(cmpX, maxX, minX) + SelectFT(cmpY, maxY, minY) + SelectFT(cmpZ, maxZPlusCol3, minZPlusCol3);
	const Vec3V maxV = SelectFT(cmpX, minX, maxX) + SelectFT(cmpY, minY, maxY) + SelectFT(cmpZ, minZPlusCol3, maxZPlusCol3);
#else
	const Vec3V minV = Min(minX, maxX) + Min(minY, maxY) + Min(minZPlusCol3, maxZPlusCol3);
	const Vec3V maxV = Max(minX, maxX) + Max(minY, maxY) + Max(minZPlusCol3, maxZPlusCol3);
#endif
	return spdAABB(minV, maxV);
}

inline spdAABB UnTransformOrthoAABB(Mat33V_In localToWorld, const spdAABB& worldBox)
{
	const Vec3V center = Multiply(worldBox.GetCenter(), localToWorld);
	const Vec3V extent = Multiply(worldBox.GetExtent(), Mat33V(Abs(localToWorld.GetCol0()), Abs(localToWorld.GetCol1()), Abs(localToWorld.GetCol2())));
	const Vec3V boxMin = center - extent;
	const Vec3V boxMax = center + extent;

	return spdAABB(boxMin, boxMax);
}

inline spdAABB UnTransformOrthoAABB(Mat34V_In localToWorld, const spdAABB& worldBox)
{
	const Vec3V center = UnTransformOrtho(localToWorld, worldBox.GetCenter());
	const Vec3V extent = Multiply(worldBox.GetExtent(), Mat33V(Abs(localToWorld.GetCol0()), Abs(localToWorld.GetCol1()), Abs(localToWorld.GetCol2())));
	const Vec3V boxMin = center - extent;
	const Vec3V boxMax = center + extent;

	return spdAABB(boxMin, boxMax);
}

inline spdAABB TransformProjectiveAABB(Mat44V_In mat, const spdAABB& worldBox)
{
	const Vec4V col0 = mat.GetCol0();
	const Vec4V col1 = mat.GetCol1();
	const Vec4V col2 = mat.GetCol2();
	const Vec4V col3 = mat.GetCol3();

	const ScalarV xmin = worldBox.GetMin().GetX();
	const ScalarV ymin = worldBox.GetMin().GetY();
	const ScalarV zmin = worldBox.GetMin().GetZ();

	const ScalarV xmax = worldBox.GetMax().GetX();
	const ScalarV ymax = worldBox.GetMax().GetY();
	const ScalarV zmax = worldBox.GetMax().GetZ();

	Vec4V v0 = AddScaled(AddScaled(AddScaled(col3, col0, xmin), col1, ymin), col2, zmin);
	Vec4V v1 = AddScaled(AddScaled(AddScaled(col3, col0, xmax), col1, ymin), col2, zmin);
	Vec4V v2 = AddScaled(AddScaled(AddScaled(col3, col0, xmin), col1, ymax), col2, zmin);
	Vec4V v3 = AddScaled(AddScaled(AddScaled(col3, col0, xmax), col1, ymax), col2, zmin);
	Vec4V v4 = AddScaled(AddScaled(AddScaled(col3, col0, xmin), col1, ymin), col2, zmax);
	Vec4V v5 = AddScaled(AddScaled(AddScaled(col3, col0, xmax), col1, ymin), col2, zmax);
	Vec4V v6 = AddScaled(AddScaled(AddScaled(col3, col0, xmin), col1, ymax), col2, zmax);
	Vec4V v7 = AddScaled(AddScaled(AddScaled(col3, col0, xmax), col1, ymax), col2, zmax);

	v0 *= Invert(v0.GetW());
	v1 *= Invert(v1.GetW());
	v2 *= Invert(v2.GetW());
	v3 *= Invert(v3.GetW());
	v4 *= Invert(v4.GetW());
	v5 *= Invert(v5.GetW());
	v6 *= Invert(v6.GetW());
	v7 *= Invert(v7.GetW());

	const Vec3V boxMin = Min(Min(v0, v1, v2, v3), Min(v4, v5, v6, v7)).GetXYZ();
	const Vec3V boxMax = Max(Max(v0, v1, v2, v3), Max(v4, v5, v6, v7)).GetXYZ();

	return spdAABB(boxMin, boxMax);
}

// ================================================================================================

inline spdAABB::spdAABB(const float* pointData, int numPoints, int strideInBytes)
{
	Invalidate();

	while (numPoints--) 
	{
		// consider using V4LoadUnaligned
		GrowPoint(Vec3V(pointData[0], pointData[1], pointData[2]));
		pointData = (const float*)((const char*)pointData + strideInBytes);
	}
}

inline void spdAABB::SetAsSphereV4(Vec4V_In centerAndRadius)
{
	const Vec4V radiusSplat = Vec4V(centerAndRadius.GetW());

	m_min = centerAndRadius - radiusSplat;
	m_max = centerAndRadius + radiusSplat;
}

inline void spdAABB::IntersectWithAABB(const spdAABB& box)
{
	m_min = Max(m_min, box.m_min);
	m_max = Min(m_max, box.m_max);
}

inline void spdAABB::Transform(Mat33V_In mat)
{
	*this = TransformAABB(mat, *this);
}

inline void spdAABB::Transform(Mat34V_In mat)
{
	*this = TransformAABB(mat, *this);
}

inline void spdAABB::TransformProjective(Mat44V_In mat)
{
	*this = TransformProjectiveAABB(mat, *this);
}

inline ScalarV_Out spdAABB::DistanceToPoint(Vec3V_In point) const
{
	const Vec3V center = GetCenter();
	const Vec3V extent = GetExtent();
	const Vec3V v      = Max(Vec3V(V_ZERO), Abs(point - center) - extent);

	return Mag(v);
}

inline ScalarV_Out spdAABB::DistanceToPointFlat(Vec2V_In point) const
{
	const Vec2V center = GetCenterFlat();
	const Vec2V extent = GetExtentFlat();
	const Vec2V v      = Max(Vec2V(V_ZERO), Abs(point - center) - extent);

	return Mag(v);
}

inline ScalarV_Out spdAABB::DistanceToPointSquared(Vec3V_In point) const
{
	const Vec3V center = GetCenter();
	const Vec3V extent = GetExtent();
	const Vec3V v      = Max(Vec3V(V_ZERO), Abs(point - center) - extent);

	return MagSquared(v);
}

inline ScalarV_Out spdAABB::DistanceToPointSquaredFlat(Vec2V_In point) const
{
	const Vec2V center = GetCenterFlat();
	const Vec2V extent = GetExtentFlat();
	const Vec2V v      = Max(Vec2V(V_ZERO), Abs(point - center) - extent);

	return MagSquared(v);
}

inline spdBool_Out spdAABB::ContainsPoint(Vec3V_In point) const
{
	return spdBool_Return(IsGreaterThanOrEqualAll(point, GetMin()) & IsLessThanOrEqualAll(point, GetMax()));
}

inline spdBool_Out spdAABB::ContainsPointFlat(Vec2V_In point) const
{
	return spdBool_Return(IsGreaterThanOrEqualAll(point, GetMinFlat()) & IsLessThanOrEqualAll(point, GetMaxFlat()));
}

inline spdBool_Out spdAABB::ContainsSphere(const spdSphere& sphere) const
{
	return ContainsAABB(spdAABB(sphere));
}

inline spdBool_Out spdAABB::ContainsSphereFlat(const spdSphere& sphere) const
{
	return ContainsAABBFlat(spdAABB(sphere));
}

inline spdBool_Out spdAABB::ContainsAABB(const spdAABB& box) const
{
	return spdBool_Return(IsGreaterThanOrEqualAll(box.GetMin(), GetMin()) & IsLessThanOrEqualAll(box.GetMax(), GetMax()));
}

inline spdBool_Out spdAABB::ContainsAABBFlat(const spdAABB& box) const
{
	return spdBool_Return(IsGreaterThanOrEqualAll(box.GetMinFlat(), GetMinFlat()) & IsLessThanOrEqualAll(box.GetMaxFlat(), GetMaxFlat()));
}

inline spdBool_Out spdAABB::ContainedBySphere(const spdSphere& sphere) const
{
	const ScalarV d2 = MagSquared(Abs(sphere.GetCenter() - GetCenter()) + GetExtent());
	const ScalarV r2 = sphere.GetRadiusSquared();

	return spdBool_Return(IsLessThanOrEqualAll(d2, r2));
}

inline spdBool_Out spdAABB::ContainedBySphereFlat(const spdSphere& sphere) const
{
	const ScalarV d2 = MagSquared(Abs(sphere.GetCenterFlat() - GetCenterFlat()) + GetExtentFlat());
	const ScalarV r2 = sphere.GetRadiusSquared();

	return spdBool_Return(IsLessThanOrEqualAll(d2, r2));
}

inline spdBool_Out spdAABB::IntersectsSphere(const spdSphere& sphere) const
{
	const ScalarV d2 = DistanceToPointSquared(sphere.GetCenter());
	const ScalarV r2 = sphere.GetRadiusSquared();

	return spdBool_Return(IsLessThanOrEqualAll(d2, r2));
}

inline spdBool_Out spdAABB::IntersectsSphereFlat(const spdSphere& sphere) const
{
	const ScalarV d2 = DistanceToPointSquaredFlat(sphere.GetCenterFlat());
	const ScalarV r2 = sphere.GetRadiusSquared();

	return spdBool_Return(IsLessThanOrEqualAll(d2, r2));
}

inline spdBool_Out spdAABB::IntersectsAABB(const spdAABB& box) const
{
	return spdBool_Return(IsGreaterThanOrEqualAll(box.GetMax(), GetMin()) & IsLessThanOrEqualAll(box.GetMin(), GetMax()));
}

inline spdBool_Out spdAABB::IntersectsAABBFlat(const spdAABB& box) const
{
	return spdBool_Return(IsGreaterThanOrEqualAll(box.GetMaxFlat(), GetMinFlat()) & IsLessThanOrEqualAll(box.GetMinFlat(), GetMaxFlat()));
}

// ================================================================================================

inline ScalarV_Out spdRect::DistanceToPoint(Vec2V_In point) const
{
	const Vec2V center = GetCenter();
	const Vec2V extent = GetExtent();
	const Vec2V v      = Max(Vec2V(V_ZERO), Abs(point - center) - extent);

	return Mag(v);
}

inline ScalarV_Out spdRect::DistanceToPointSquared(Vec2V_In point) const
{
	const Vec2V center = GetCenter();
	const Vec2V extent = GetExtent();
	const Vec2V v      = Max(Vec2V(V_ZERO), Abs(point - center) - extent);

	return MagSquared(v);
}

inline spdAABB::spdAABB(const spdRect& rect, ScalarV_In zmin, ScalarV_In zmax)
{
	Set(Vec3V(rect.GetMin(), zmin), Vec3V(rect.GetMax(), zmax));
}

// ================================================================================================

inline void spdRect::GetCorners(Vec2V out[4])
{
	Vec2V posMin = GetMin();
	Vec2V posMax = GetMax();

	out[0] = posMin;
	out[1] = GetFromTwo<Vec::X1, Vec::Y2>(posMin, posMax);
	out[2] = posMax;
	out[3] = GetFromTwo<Vec::X2, Vec::Y1>(posMin, posMax);
}

// ================================================================================================

inline spdOrientedBB::spdOrientedBB(const spdAABB& localBox, Mat34V_In localToWorld)
{
	const Vec3V offset = UnTransformOrtho(localToWorld, Vec3V(V_ZERO));
	const Vec3V boxMin = localBox.GetMin() + offset;
	const Vec3V boxMax = localBox.GetMax() + offset;

	m_localBox.Set(boxMin, boxMax);
	m_localToWorld = localToWorld.GetMat33();
}

inline void spdOrientedBB::Set(const spdAABB& localBox, Mat34V_In localToWorld)
{
	const Vec3V offset = UnTransformOrtho(localToWorld, Vec3V(V_ZERO));
	const Vec3V boxMin = localBox.GetMin() + offset;
	const Vec3V boxMax = localBox.GetMax() + offset;

	m_localBox.Set(boxMin, boxMax);
	m_localToWorld = localToWorld.GetMat33();
}

inline spdAABB spdOrientedBB::GetWorldAABB() const
{
	return TransformAABB(m_localToWorld, m_localBox);
}

inline spdBool_Out spdOrientedBB::ContainsPoint(Vec3V_In point) const
{
	const Vec3V temp = UnTransformOrtho(m_localToWorld, point);
	return m_localBox.ContainsPoint(temp);
}

inline spdBool_Out spdOrientedBB::ContainsSphere(const spdSphere& sphere) const
{
	const spdSphere temp = UnTransformOrthoSphere(m_localToWorld, sphere);
	return m_localBox.ContainsSphere(temp);
}

inline spdBool_Out spdOrientedBB::ContainsAABB(const spdAABB& box) const
{
	const spdAABB temp = UnTransformOrthoAABB(m_localToWorld, box);
	return m_localBox.ContainsAABB(temp);
}

inline spdBool_Out spdOrientedBB::ContainsOrientedBB(const spdOrientedBB& box) const
{
	Mat33V mat;
	Transpose(mat, box.m_localToWorld);
	Multiply(mat, mat, m_localToWorld);

	const spdAABB temp = TransformAABB(mat, m_localBox); // in box's space
	return box.m_localBox.ContainsAABB(temp);
}

inline spdBool_Out spdOrientedBB::ContainedBySphere(const spdSphere& sphere) const
{
	const spdSphere temp = UnTransformOrthoSphere(m_localToWorld, sphere);
	return m_localBox.ContainedBySphere(temp);
}

inline spdBool_Out spdOrientedBB::ContainedByAABB(const spdAABB& box) const
{
	const spdAABB temp = TransformAABB(m_localToWorld, m_localBox); // in box's space (worldspace)
	return box.ContainsAABB(temp);
}

inline spdBool_Out spdOrientedBB::IntersectsSphere(const spdSphere& sphere) const
{
	const spdSphere temp = UnTransformOrthoSphere(m_localToWorld, sphere);
	return m_localBox.IntersectsSphere(temp);
}

inline spdBool_Out spdOrientedBB::IntersectsAABB(const spdAABB& box) const
{
	if (!box.IntersectsAABB(TransformAABB(m_localToWorld, m_localBox)))
	{
		return false;
	}

	const spdAABB temp = UnTransformOrthoAABB(m_localToWorld, box);
	return m_localBox.IntersectsAABB(temp);
}

inline spdBool_Out spdOrientedBB::IntersectsOrientedBB(const spdOrientedBB& box) const
{
	Mat33V mat;
	Transpose(mat, box.m_localToWorld);
	Multiply(mat, mat, m_localToWorld);

	if (!box.m_localBox.IntersectsAABB(TransformAABB(mat, m_localBox)))
	{
		return false;
	}

	const spdAABB temp = UnTransformOrthoAABB(mat, box.m_localBox);
	return m_localBox.IntersectsAABB(temp);
}
