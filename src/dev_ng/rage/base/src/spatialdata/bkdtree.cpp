// 
// spatialdata/bkdtree.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "bkdtree.h"
#include "math/random.h"

using namespace rage;

#if __ASSERT

CompileTimeAssert( sizeof( spd::AABBIndexed) == spd::AABBIndexed::DesiredSize );
CompileTimeAssert( sizeof( spd::BoundedNode<int>) == spd::BoundedNode<int>::DesiredSize );


void UnitTestTreeBuilder()
{
	int indices[256];
	spd::AABBIndexed					boxList[4];
	boxList[0] = spd::AABBIndexed( Vector3( 0.0f, 0.0f, 0.0f), Vector3( 1.0f, 0.0f, 0.0f ));
	boxList[1] = spd::AABBIndexed( Vector3( 2.0f, 0.0f, 0.0f), Vector3( 3.0f, 0.0f, 0.0f ));
	boxList[2] = spd::AABBIndexed( Vector3( 4.0f, 0.0f, 0.0f), Vector3( 5.0f, 0.0f, 0.0f ));
	boxList[3] = spd::AABBIndexed( Vector3( 6.0f, 0.0f, 0.0f), Vector3( 7.0f, 0.0f, 0.0f ));

	int amount;
	spd::BoundedNode<spd::AABBIndexed>		bkdTree[8];
	BoundedKdTreeBuilder( bkdTree, boxList, boxList + 4, spd::MinCellPredicate(2) , amount, 8);

	Assert( amount == 7 );
	Assert( bkdTree[0].IsLeaf() == false );
	Assert( bkdTree[0].GetLeftIndex() == &(bkdTree[1]));
	Assert( bkdTree[0].GetRightIndex() == &(bkdTree[2]) );
	Assert( bkdTree[1].IsLeaf() == false );
	Assert( bkdTree[1].GetLeftIndex() == &(bkdTree[3]) );
	Assert( bkdTree[1].GetRightIndex() == &(bkdTree[4])  );

	Assert( bkdTree[0].GetAxis() == 0  );
	Assert( bkdTree[0].GetLeftIndex() == &(bkdTree[1]) );
	Assert( bkdTree[0].GetRightIndex() == &(bkdTree[2]) );

	Assert( bkdTree[3].IsLeaf() );

	// Test traversal
	Vector3 bMin( 0.0f, 0.0f, 0.0f );
	Vector3 bMax( 100.0f, 0.0f, 0.0f );

	spd::AABBIndexed::TraversalIntersector trav( indices, bMin, bMax );
	
	Traverse( bkdTree, bMin, bMax, trav);
	Assert( trav.GetCount() == 4);
	bMin = Vector3(-1.5f, -1.5f, -1.5f );
	bMax = Vector3(-1.0f, -1.0f, -1.0f );

	trav = spd::AABBIndexed::TraversalIntersector( indices, bMin, bMax );

	Traverse( bkdTree, bMin, bMax, trav);
	Assert( trav.GetCount() ==0);

}

template<class T>
bool CheckEqual( T s1, T e1, T s2  )
{
	std::sort( s1, e1 );
	std::sort( s2, s2 + std::distance( s1, e1) );
	return std::equal( s1, e1, s2 );
}
typedef spd::AABBIndexed::TraversalIntersector Intersector;

void StocasticTestTreeBuilder( int AmtBoxes, float width, float range, int NumTests )
{
	atArray<spd::AABBIndexed>			boxes;
	int indices[256];

	boxes.Resize( AmtBoxes );

	mthRandom rnd;
	for( int i = 0; i < AmtBoxes; i++ )
	{
		Vector3 min= Vector3( rnd.GetRanged( -range, range  ), rnd.GetRanged( -range, range  ),rnd.GetRanged( -range, range  ) );
		Vector3 max = Vector3( rnd.GetRanged( width * 0.3f, width  ), rnd.GetRanged( width * 0.3f, width  ),rnd.GetRanged( width * 0.3f, width  ) );
		max += min;
		boxes[i] = spd::AABBIndexed( min, max, i );
	}

	// Create the tree
	BoundKdTree<spd::AABBIndexed>	bkdTree;
	bkdTree.Init(boxes.GetCount());
	bkdTree.Create( boxes.begin(), boxes.end(), 2 );

	Assert( bkdTree.GetNodeCount() < AmtBoxes * 3);
	int indices2[256];

	for ( int i = 0; i < NumTests; i++ )
	{
		Vector3 min= Vector3( rnd.GetRanged( -range, range  ), rnd.GetRanged( -range, range  ),rnd.GetRanged( -range, range  ) );
		Vector3 max = Vector3( rnd.GetRanged( width * 0.3f, width  ), rnd.GetRanged( width * 0.3f, width  ),rnd.GetRanged( width * 0.3f, width  ) );
		max += min;
		Intersector travList( indices2, min, max );
		Intersector resList = std::for_each(boxes.begin(), boxes.end(), travList );

		spd::AABBIndexed::TraversalIntersector trav( indices, min, max );
		bkdTree.Traverse(  min, max, trav);
		Assert( trav.GetCount() == resList.GetCount() );
		Assert( CheckEqual( indices, indices + trav.GetCount(), indices2 ) );
	}
}

void UnitTestBoundedNode()
{
	spd::BoundedNode<int>	node;

	// check axis indexes
	node.SetAxis( 1 );
	node.SetIndex( (spd::BoundedNode<int>*)(17 * 4) );

	Assert( node.GetAxis() == 1 );
	Assert( (size_t)node.GetLeftIndex() == (17 * 4));
	Assert( (size_t)node.GetRightIndex() ==  ((17 * 4) + sizeof(spd::BoundedNode<int>) ));

	// check split values
	node.SetMinSplit( 10.0f);
	node.SetMaxSplit( 100.0f);
	Assert( node.GetMinSplit() == 10.0f );
	Assert( node.GetMaxSplit() == 100.0f );

	// Check leaf node
	int intVals[]={ 1, 2, 3 };
	node.SetChildren( intVals, 2 );
	Assert( node.IsLeaf() );

	int* start;
	int* end;
	node.GetChildren( start, end );
	Assert( start == intVals );
	Assert( end == &intVals[2] );

	// Check size packing
	spd::BoundedNode<int>	nodelist[16];
	int size = sizeof( spd::BoundedNode<int>);
	Assert( size == spd::BoundedNode<int>::DesiredSize );
	size = sizeof( nodelist);
	Assert( size == spd::BoundedNode<int>::DesiredSize * 16 );
}

void UnitTestAABBIndexed()
{
	spd::AABBIndexed	bound( Vector3( 0.0f, 0.0f, 0.0f), Vector3( 1.0f, 1.0f, 0.0f ), 12);
	Assert( bound.GetIndex() == 12 );
	Assert( bound.GetMin() == Vector3( 0.0f, 0.0f, 0.0f));
	Assert( bound.GetMax() == Vector3( 1.0f, 1.0f, 0.0f ));

	Assert( bound.Intersects( Vector3( .5f, 0.0f, 0.0f), Vector3( .7f, 1.0f, 0.0f ) ));

	// Check the intersector
	int indices[256];
	spd::AABBIndexed::TraversalIntersector intersector( indices, Vector3( 3.5f, 0.0f, 0.0f), Vector3( 7.5f, 2.0f, 2.0f ) );

	spd::AABBIndexed					boxList[4];
	boxList[0] = spd::AABBIndexed( Vector3( 0.0f, 0.0f, 0.0f), Vector3( 1.0f, 0.0f, 0.0f ), 0);
	boxList[1] = spd::AABBIndexed( Vector3( 2.0f, 0.0f, 0.0f), Vector3( 3.0f, 0.0f, 0.0f ), 1);
	boxList[2] = spd::AABBIndexed( Vector3( 4.0f, 0.0f, 0.0f), Vector3( 5.0f, 0.0f, 0.0f ), 2);
	boxList[3] = spd::AABBIndexed( Vector3( 6.0f, 0.0f, 0.0f), Vector3( 7.0f, 0.0f, 0.0f ), 3);

	spd::AABBIndexed::TraversalIntersector  res = std::for_each( boxList, boxList + 4, intersector );
	Assert( res.GetCount() == 2 );
	Assert( indices[0] == 2 );
	Assert( indices[1] == 3 );

	// check maximum clamp
	intersector.m_maxAmt = 1;
	spd::AABBIndexed::TraversalIntersector  res2 = std::for_each( boxList, boxList + 4, intersector );
	Assert( res2.GetCount() == 1 );
	Assert( indices[0] == 2 );


	// Check size packing
	spd::AABBIndexed	boundlist[16];
	int size = sizeof( spd::AABBIndexed);
	Assert( size == spd::AABBIndexed::DesiredSize );
	size = sizeof( boundlist);
	Assert( size == spd::AABBIndexed::DesiredSize * 16 );

}




void rage::UnitTestBoundedKdTree()
{
	UnitTestAABBIndexed();

	UnitTestBoundedNode();
	UnitTestTreeBuilder();
	// do small test
	StocasticTestTreeBuilder( 64, 7.0f, 10.0f, 200 );
	// do medium size
	StocasticTestTreeBuilder( 512, 1.0f, 10.0f, 100 );
	// do large test
	StocasticTestTreeBuilder( 32 * 1024, 0.3f, 10.0f, 5 );
}

#else
void rage::UnitTestBoundedKdTree() {}
#endif

