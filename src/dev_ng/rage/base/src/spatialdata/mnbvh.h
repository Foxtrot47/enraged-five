// 
// spatialdata/mnbvh.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_MNBVH_H 
#define SPATIALDATA_MNBVH_H 
#include "atl/array.h"
#include "vectormath/classes.h"
#include "spatialdata/aabb.h"

namespace rage {

#if __ASSERT
#define MNBVH_INLINE	
#else
#define MNBVH_INLINE	__forceinline 
#endif
	// building structures
	struct spdMNBVHBounds
	{
		Vec3V m_bmin;
		Vec3V m_bmax;
		void* m_data;
	};

	// PURPOSE : wraps a list of data elements
	struct spdMNBVHBoundList
	{
		Vec3V m_bmin;
		Vec3V m_bmax;

		spdMNBVHBounds* m_start;
		spdMNBVHBounds* m_end;

		spdMNBVHBoundList() {}
		spdMNBVHBoundList( spdMNBVHBounds* s, spdMNBVHBounds* e );
		int Amt()						const { return (int) (m_end - m_start); }
		bool IsLeaf( int maxPerLeaf )	const  {		return Amt() <= maxPerLeaf;	}

#if __BANK
		bool IsValid()
		{
			bool tlist=true;
			for (spdMNBVHBounds* v=m_start;v!= m_end; ++v){
				bool isValid = IsTrueXYZ(IsNotNan(v->m_bmin)) &&  IsTrueXYZ(IsNotNan(v->m_bmax));
				Assertf( isValid, " Invalid Data passed into spdMNBVH ");
				tlist = tlist &&  isValid;
			}
			return tlist;
		}
#endif


		ScalarV_Out AreaV() const;

		int GetSplitIndex() const;

		Vec3V_Out GetSplitPlane( int idx ) const;

		void Split( spdMNBVHBoundList& left, spdMNBVHBoundList& right) const;
	};


	// PURPOSE : Multi-Node Bounding Volume Hierchary 
	// REMAKRS: A spatial tree where each node consists of 16 child nodes. This is similar to a btree. 
	// It suits the 360/PPU/SPU architectures as it reduces cache misses ( simplifies dmaing the tree)
	// and allows for testing multiple boxes in SOA format. Which reduces instruction count and 
	// dependent instructions, also is faster to build.
	// Based on http://www.gdcvault.com/play/1012452/R-Trees-Adapting-out-of
	// and 
	#define MNBVH_NODESIZE	16
	#define MNBVH_NODESIMDSIZE MNBVH_NODESIZE/4

	struct ALIGNAS(128) MNBVHNode    // four cachelines for 16 entries compressed
	{
		typedef MNBVHNode* (*AllocateFunction)(int depth);
		typedef void (*DeleteFunction)(const MNBVHNode* );

		// SOA layout
		Vec4V minX[MNBVH_NODESIMDSIZE];
		Vec4V maxX[MNBVH_NODESIMDSIZE];

		Vec4V minY[MNBVH_NODESIMDSIZE];
		Vec4V maxY[MNBVH_NODESIMDSIZE];

		Vec4V minZ[MNBVH_NODESIMDSIZE];
		Vec4V maxZ[MNBVH_NODESIMDSIZE];

		// packed version
		// Use SIMD 16 - 16 boxes in one cacheline
		//Vec4V minXmaxX[2];//32
		//Vec4V minYmaxY[2];//64
		//Vec4V minZmaxZ[2];//96
		//u16 leafIndices[16]; // 128
		// flags[16];


		MNBVHNode* leafs[MNBVH_NODESIZE];  // 256  -> bottom bit marks it as a leaf node or not
#if !__64BIT
		u32		   flags[MNBVH_NODESIZE];  // flags to kill traversal on type info ( unused to allow for fast 64 bit implementation)
#endif
		// no space for flags

		// slow helpers for building
		void SetLeaf( int i, void* v );
		void SetChild( int i , MNBVHNode* node)
		{
			Assert( node != 0);
			leafs[i] = node;
		}
		bool IsLeaf(int i ) const	{		return ((size_t)leafs[i]&0x1 ) != 0;	}
		bool IsChild(int i ) const	{		return !IsLeaf(i);	}

		bool IsEmpty(int i ) const 
		{
			return leafs[i] == 0;
		}
		void* GetLeaf(int i ) const;
		void Clear();
		void CreateBounds( spdMNBVHBoundList cnodes[16], int amt );
		void GetBounds( spdMNBVHBounds blist[MNBVH_NODESIZE] ) const;
		void CreateBounds(const spdMNBVHBounds* blist, int amt);
	};

	// Memory allocation control
	MNBVHNode* MNBVHAllocateDefault(int depth);
	void MNBVHDeleteDefault( const MNBVHNode* node );
	void MNBVHDeleteNull( const MNBVHNode* node );
	int MNBVHCalcMaxNodes( int numObjects );

	//-------------------------------------------------
	// creation and destruction
	
	// PURPOSE : builds a MNBVH tree
	// PARAMS : nodes is a list of elements to be placed in the tree.
	//			tempBoundsMem is a buffer of bounds the num of elements in the boundlist
	//			max per leaf controls the number of elements in a leaf, 16 gives the best occupancy and memory usage
	//
	MNBVHNode* MNBVHBuildMultiNodeBVHTree( spdMNBVHBoundList& nodes, MNBVHNode::AllocateFunction allocator,int MaxPerLeaf =MNBVH_NODESIZE, int depth=0 );

	//PURPOSE: deletes a node and all child nodes
	int MNBVHDeleteNode(const MNBVHNode* node, MNBVHNode::DeleteFunction deleter);

	// REMARKS : returns a list of void pointers to the data and the amount of the results.	
	int MNBVHTraverseBoxInternal(const MNBVHNode* root, Vec3V_In bmin, Vec3V_In bmax, void** results, int /*maxAmt*/=256 );
	int MNBVHTraverseRayInternal(const MNBVHNode* root, Vec3V_In start, Vec3V_In end, void** results, int /*maxAmt*/=256);

	template<class T,int Size>
	int MNBVHIntersectBox(const MNBVHNode* root, Vec3V_In bmin, Vec3V_In bmax, T*(&results)[Size])
	{
		return MNBVHTraverseBoxInternal( root, bmin, bmax, (void**)results, Size );
	}
	template<class T>
	int MNBVHIntersectBox(const MNBVHNode* root, Vec3V_In bmin, Vec3V_In bmax, T** results, int maxAmt )
	{
		return MNBVHTraverseBoxInternal( root, bmin, bmax, (void**)results, maxAmt );
	}

	template<class T,int Size>
	int MNBVHIntersectRay(const MNBVHNode* root, Vec3V_In start, Vec3V_In end, T*(&results)[Size])
	{
		return MNBVHTraverseRayInternal(root, start, end, (void**)results, Size );
	}
	template<class T>
	int MNBVHIntersectRay(const MNBVHNode* root, Vec3V_In start, Vec3V_In end, T** results, int maxAmt)
	{
		return MNBVHTraverseRayInternal(root, start, end, (void**)results, maxAmt );
	}

#if __BANK
	void MNBVHCalcDiagnostics( const MNBVHNode* node, int& numNodes, int& numObjects, int& maxTreeDepth, float& nodeOccupancy );
	void MNBVHDraw(  const MNBVHNode* node,  int sd, int fd, float alpha, bool drawSolid = false);



	class spdMNBVHTreeVis
	{
		int			m_numNodes;
		int			m_numObjects;
		int			m_maxTreeDepth;
		float		m_nodeOccupancy;

		bool		m_update;
		bool		m_debugDraw;
		int			m_startDrawDepth;
		int			m_FinishDrawDepth;
		bool		m_drawLeafs;
		char		m_treeName[128];
		float		m_alpha;
		bool		m_drawSolid;
	public:
		spdMNBVHTreeVis( const char* treeName  );

		void Calc( MNBVHNode* tree);
		void DebugDraw( MNBVHNode* tree);
		void AddWidgets( bkBank& bk);
	};


#endif

	// PURPOSE : very simple interface to hide some of the complexity of MNBVH usage for high level game code
	class MNBVHSimpleInterface
	{
	public:
		MNBVHSimpleInterface(const atArray<spdAABB>& boxList);
		MNBVHSimpleInterface(atArray<spdMNBVHBounds>& boxList);
		~MNBVHSimpleInterface();

		s32 PerformBoxSearch(const spdAABB& searchAABB, void** results, int maxResults)
		{
			return MNBVHIntersectBox( m_pTree, searchAABB.GetMin(), searchAABB.GetMax(), results, maxResults );
		}

		s32 PerformLineSearch(Vec3V_In vPos1, Vec3V_In vPos2, void** results, int maxResults)
		{
			return MNBVHIntersectRay( m_pTree, vPos1, vPos2, results, maxResults );
		}

		s32 GetSearchResult(void** results, s32 index) const
		{
			return ( (s32)(ptrdiff_t)results[index] >> 2 );	// mnbvh makes use of the LSB
		}

	private:
		static const int MaxTreeDepth=16;
		static MNBVHNode* ArrayNewCB( int /*depth*/) { return &ms_nodeStorage[ms_numNodesAllocated++]; }
		static MNBVHNode* StackAllocatorCB( int depth) 
		{	
			ms_numNodesAllocated++; 
			Assert( depth < MaxTreeDepth);
			return &ms_nodeStorage[depth]; 
		}
		// used for node allocation
		static MNBVHNode* ms_nodeStorage;
		static s32 ms_numNodesAllocated;

		MNBVHNode* m_nodes;

		// tree pointer
		MNBVHNode* m_pTree;

		const bool m_bUseArrayAlloc;
	};

	
} // namespace rage

#endif // SPATIALDATA_MNBVH_H 
