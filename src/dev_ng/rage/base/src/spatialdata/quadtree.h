//
// spatialdata/quadtree.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SPATIALDATA_QUADTREE_H
#define SPATIALDATA_QUADTREE_H

#include "aabb.h"
#include "sphere.h"
#include "atl/array.h"
#include "atl/slist.h"

namespace rage {

	namespace QuadTreeConstants
	{
		extern const int	kLut[4][4];
	}

	template<typename _Type>
	class spdQuadtreeArray : public atArray<_Type>
	{
		void IterateBegin() const {};
		void IterateEnd() const {};
	};

// PURPOSE: A node in the quadtree. It can have up to 4 children.
// PARAMS:
//		_Data - The type of object that is stored in the quadtree. 
//		_BboxFn - A function object that, given a _Data returns an spdAABB that encloses the _Data.
//      _ContainerType - Data structure used to hold the data stored in the tree.
	template<typename _Data, typename _BboxFn, typename _ContainerType = spdQuadtreeArray<_Data> >
class spdQuadtreeNode {
		template<typename __D, typename __B, typename __C> friend class spdQuadtree;
public:

		// -------------------------- resourcing -----------------------------------
	enum
	{
		RORC_VERSION = 1
	};
	spdQuadtreeNode(datResource &rsc)
		: m_CellData(rsc)
		, m_Children(rsc)
		, m_Parent(rsc)
		, m_CellBound(rsc)
	{
	}
		void Place(spdQuadtreeNode *that, ::rage::datResource &rsc) 
	{ 
			::new (that) spdQuadtreeNode(rsc);
	}
	int Release() const {delete this; return 0;}	// Not true reference counting yet!
#if !__FINAL
		void DeclareStruct(datTypeStruct &s);
#endif
		// -------------------------- resourcing -----------------------------------


	spdQuadtreeNode();
	~spdQuadtreeNode();

	void BuildTree(atSList<_Data> &objects, float looseQuadtreeScale, float minCellSize, _BboxFn bbox);

	template <typename _ArrayType> void FindIntersectingData(const Vector3 &pos, _ArrayType& data, int maxData, int &count, _BboxFn bbox) const;
	template <typename _ArrayType> void FindIntersectingData(const spdSphere &sphere, bool test, _ArrayType& data, int maxData, int &count, _BboxFn bbox) const;

	template <typename _ArrayType> void FindAllData(_ArrayType& data, int maxData, int& count) const;

	const spdAABB& GetCellBound() {return m_CellBound;}

protected:

		typedef spdQuadtreeNode<_Data, _BboxFn, _ContainerType>		myType;
		typedef atRangeArray<datOwner<myType>, 4>					childrenType;

		spdAABB				m_CellBound;
		_ContainerType			m_CellData;
		childrenType			m_Children;
		datOwner<myType>		m_Parent;

		// padding
		u32 pad;
	};




	template<typename _Data, typename _BboxFn, typename _ContainerType = spdQuadtreeArray<_Data> >
class spdQuadtree {
public:

		// -------------------------- resourcing -----------------------------------
	enum
	{
		RORC_VERSION = 1
	};
	spdQuadtree(datResource &rsc)
		: m_Root(rsc)
	{
	}
		void Place(spdQuadtree *that, ::rage::datResource &rsc)
	{ 
			::new (that) spdQuadtree(rsc);
	}
	int Release() const {delete this; return 0;}	// Not true reference counting yet!
#if !__FINAL
		void DeclareStruct(datTypeStruct &s);
#endif
		// -------------------------- resourcing -----------------------------------


#if 0 //rs TODO: Test the iterator
	// This iterator will NOT return elements in the same order that the FindIntersectingData functions do.
	template<typename _Region>
	class SpatialIterator {
	public:
		SpatialIterator& operator++();
		_Data& operator*() const;
		bool operator==(const SpatialIterator& other) const;
		bool operator!=(const SpatialIterator& other) const;
		SpatialIterator& operator=(const SpatialIterator& other);
	protected:
		_Region					m_Region;
		spdQuadtreeNode<_Data, _BboxFn, _ContainerType>* m_CurrNode;
		int						m_CurrData;
		int						m_CurrChild;
	};
#endif

	spdQuadtree();
	~spdQuadtree();

	void BuildTree(atSList<_Data> &objects);

	template<typename _ArrayType> int FindIntersectingData(const Vector3 &pos, _ArrayType& data, int maxData) const;
	template<typename _ArrayType> int FindIntersectingData(const spdSphere &sphere, _ArrayType& data, int maxData) const;

	template<typename _ArrayType> int FindAllData(_ArrayType& data, int maxData) const;

	void SetLooseQuadtreeScale(float scale);
	void SetMinCellSize(float size);
	void SetBoundingBoxFunction(_BboxFn function);

protected:

		typedef spdQuadtreeNode<_Data, _BboxFn, _ContainerType> nodeType;

		datOwner<nodeType>			m_Root;
	float										m_LooseQuadtreeScale;
	float										m_MinCellSize;
	_BboxFn										m_BboxFn;
};

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::spdQuadtreeNode() 
: m_Parent(NULL)
{
	m_CellBound.Invalidate();
	for(int i = 0; i < 4; i++) {
		m_Children[i] = NULL;
	}
}

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::~spdQuadtreeNode()
{
	for(int i = 0; i < 4; i++) {
		delete m_Children[i];
	}

}


#if __DECLARESTRUCT
	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::DeclareStruct(datTypeStruct &s)
{
		STRUCT_BEGIN(spdQuadtreeNode);
		STRUCT_FIELD(m_CellBound);
		STRUCT_FIELD(m_CellData);
		STRUCT_FIELD(m_Children);
		STRUCT_FIELD(m_Parent);
		STRUCT_FIELD(pad);
		STRUCT_END();
	}
#endif


#if __DECLARESTRUCT
	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtree<_Data, _BboxFn,_ContainerType>::DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(spdQuadtree);
		STRUCT_FIELD(m_Root);
		STRUCT_FIELD(m_LooseQuadtreeScale);
		STRUCT_FIELD(m_MinCellSize);
		//	STRUCT_FIELD_VP(m_BboxFn);			NOTE: RDR2 fix 3/28: our _BboxFn is now a class mass-used for all dustspots, and trying to resource it here crashes.
		STRUCT_END();
	}
#endif


#if 0
	// unscaled.Scale line below doesn't even compile
	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::BuildTree(atSList<_Data> &objects, float looseQuadtreeScale, float minCellSize, _BboxFn bbox)
	{
		FastAssert(m_CellBound.IsValid());

	atSList<_Data> childObjects[4];
	spdAABB childCells[4];
	spdAABB unscaled = m_CellBound;
	unscaled.Scale(1.0f / looseQuadtreeScale);

	float minx = unscaled.GetMinVector3().x;
	float miny = unscaled.GetMinVector3().y;
	float minz = unscaled.GetMinVector3().z;
	float maxx = unscaled.GetMaxVector3().x;
	float maxy = unscaled.GetMaxVector3().y;
	float maxz = unscaled.GetMaxVector3().z;
	float halfx = unscaled.GetCenter().x; ggg
	float halfz = unscaled.GetCenter().z;

	childCells[0].Set( Vector3(minx, miny, minz), Vector3(halfx, maxy, halfz) );
	childCells[1].Set( Vector3(halfx, miny, minz), Vector3(maxx, maxy, halfz) );
	childCells[2].Set( Vector3(minx, miny, halfz), Vector3(halfx, maxy, maxz) );
	childCells[3].Set( Vector3(halfx, miny, halfz), Vector3(maxx, maxy, maxz) );

	childCells[0].Scale(looseQuadtreeScale);
	childCells[1].Scale(looseQuadtreeScale);
	childCells[2].Scale(looseQuadtreeScale);
	childCells[3].Scale(looseQuadtreeScale);

	// Shuffle stuff from the object list into the child object lists.

	atSNode<_Data> *prev = NULL;
	atSNode<_Data> *node = objects.GetHead(); if (!node) return;
	atSNode<_Data> *next = node->GetNext();

	int i;

	if (m_CellBound.GetWidth() > minCellSize &&
//rs?		m_CellBound.GetHeight() > minCellSize &&
		m_CellBound.GetDepth() > minCellSize)
	{
		while (node)
		{
			int child = -1;

			for (i = 0; i < 4 && child < 0; i++)
			{
				if (childCells[i].ContainsAABBFlat(bbox(node->Data)))
					child = i;
			}

			if (child >= 0)
			{
					FastAssert(child >= 0 && child < 4);
				objects.RemoveNext(prev);
				childObjects[child].Append(*node);
			}
			else
				prev = node;

			node = next;
			if (node)
				next = node->GetNext();
		}
	}
	else
	{
		// Horrible warning, totally spews on quadtrees with point data.
		//Warningf("Teeny tiny cell: %f, %f, %f", m_CellBound.GetWidth(), m_CellBound.GetHeight(), m_CellBound.GetDepth());
	}

	// Populate this cell.
	int count = objects.GetNumItems();
	if (count > 0)
	{
		m_CellData.Reserve(count);
			atSNode<_Data> *node = objects.GetHead(); FastAssert(node);
		while (node)
		{
			m_CellData.Append() = node->Data;
			node = node->GetNext();
		}
			FastAssert(m_CellData.GetCount() == count);
	}

/*rs
	// Give birth to the children.
	int childCount = 0;
	for (i = 0; i < 4; i++)
		if (childObjects[i].GetNumItems() > 0)
			childCount++;
#if __BANK
	Owner->TotalCells += childCount;
#endif
*/

	for (i = 0; i < 4; i++)
	{
		if (childObjects[i].GetNumItems() > 0)
		{
				m_Children[i] = rage_new myType;
//rs			BANK_ONLY(Children[i]->Owner = Owner);
			m_Children[i]->m_CellBound = childCells[i];
			m_Children[i]->BuildTree(childObjects[i], looseQuadtreeScale, minCellSize, bbox);
		}
		else
		{
			m_Children[i] = NULL;
		}
	}
}
#endif	// #if 00


#if 0
	template<typename _Data, typename _BboxFn, typename _ContainerType> template<typename _ArrayType> 
	inline void spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::FindIntersectingData(const Vector3 &pos, _ArrayType& data, int maxData, int &count, _BboxFn bbox) const
{
	if (!m_CellBound.Contains(pos))
		return;

	int i = 0;
		m_CellData.IterateBegin();
	while (i < m_CellData.GetCount() && count < maxData)
	{
		if (bbox(m_CellData[i]).Contains(pos))
			data[count++] = &m_CellData[i];
		i++;
	}
		m_CellData.IterateEnd();

	if (count >= maxData)
		return;

	// Optimized. /FF
	//int a = IsPointInFrontOfPlane(pos, CellBound.GetCenter(), Vector3(0.0f, 0.0f, 1.0f)) ? 1 : 0;
	//a |= IsPointInFrontOfPlane(pos, CellBound.GetCenter(), Vector3(1.0f, 0.0f, 0.0f)) ? 2 : 0;

	const Vector3 &bndCenter = m_CellBound.GetCenter();
	int a = ((pos.z - bndCenter.z) > -SMALL_FLOAT) ? 1 : 0;
	a	 |= ((pos.x - bndCenter.x) > -SMALL_FLOAT) ? 2 : 0;

		FastAssert(a >= 0 && a < 4);

	for (i = 0; i < 4; i++)
	{
			int b = QuadTreeConstants::kLut[a][i];
			FastAssert(b >= 0 && b < 4);
		if (m_Children[b] && m_Children[b]->GetCellBound().Contains(pos))
			m_Children[b]->FindIntersectingData(pos, data, maxData, count, bbox);
	}
}
#endif	// #if 0

#if 0
	template<typename _Data, typename _BboxFn, typename _ContainerType> template<typename _ArrayType> 
	inline void spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::FindIntersectingData(const spdSphere &sphere, bool test, _ArrayType& data, int maxData, int &count, _BboxFn bbox) const
{

	grcCullStatus intersect;
	bool inside;

	if (test)
	{
		intersect = sphere.Intersects(m_CellBound);
		if (!intersect)
			return;
		inside = intersect == cullInside;
	}
	else
	{
		intersect = cullInside;
		inside = true;
	}

	int i = 0;
		m_CellData.IterateBegin();
	while (i < m_CellData.GetCount() && count < maxData)
	{
		if (!test || sphere.Intersects(bbox(m_CellData[i])))
			data[count++] = &m_CellData[i];
		i++;
	}
		m_CellData.IterateEnd();

	if (count >= maxData)
		return;

	// Optimized. /FF
	//int a = IsPointInFrontOfPlane(sphere.GetCenter(), CellBound.GetCenter(), Vector3(0.0f, 0.0f, 1.0f)) ? 1 : 0;
	//a |= IsPointInFrontOfPlane(sphere.GetCenter(), CellBound.GetCenter(), Vector3(1.0f, 0.0f, 0.0f)) ? 2 : 0;

	const Vector3 &sphereCenter = sphere.GetCenter();
	const Vector3 &bndCenter = m_CellBound.GetCenter();
	int a = ((sphereCenter.z - bndCenter.z) > -SMALL_FLOAT) ? 1 : 0;
	a	 |= ((sphereCenter.x - bndCenter.x) > -SMALL_FLOAT) ? 2 : 0;

		FastAssert(a >= 0 && a < 4);

	for (i = 0; i < 4; i++)
	{
			int b = QuadTreeConstants::kLut[a][i];
			FastAssert(b >= 0 && b < 4);
		if (m_Children[b])
			m_Children[b]->FindIntersectingData(sphere, !inside, data, maxData, count, bbox);
	}
}
#endif	// #if 0

	template<typename _Data, typename _BboxFn, typename _ContainerType> template<typename _ArrayType> 
	inline void spdQuadtreeNode<_Data, _BboxFn,_ContainerType>::FindAllData(_ArrayType& data, int maxData, int &count) const
{
	if (m_Children[0]) {
		m_Children[0]->FindAllData(data, maxData, count);
	}
	if (m_Children[1]) {
		m_Children[1]->FindAllData(data, maxData, count);
	}

	int i = 0;
		m_CellData.IterateBegin();
	while(i < m_CellData.GetCount() && count < maxData) {
		data[count] = &m_CellData[i];
		++count;
		++i;
	}
		m_CellData.IterateEnd();

	if (m_Children[2]) {
		m_Children[2]->FindAllData(data, maxData, count);
	}
	if (m_Children[3]) {
		m_Children[3]->FindAllData(data, maxData, count);
	}
}

#if 0 //rs TODO test these functions
	template<typename _Data, typename _BboxFn, typename _ContainerType>
template<typename _Region>
	inline spdQuadtree<_Data,_BboxFn,_ContainerType>::SpatialIterator<_Region>& spdQuadtree<_Data,_BboxFn,_ContainerType>::SpatialIterator<_Region>::operator++()
{
	if (m_CurrNode == NULL) {
		return;
	}

NEXT_DATA:
	// Step 1: Scan through the current data array 
	for(; m_CurrData < m_CurrNode->m_CellData.GetCount(); m_CurrData++) {
		if (_BboxFn(m_CurrNode->m_CellData[m_CurrData]).Contains(m_Region)) {
			return;
		}
	}

	// Step 2: Scan through all the children that contain the point
NEXT_CHILD:
	for(; m_CurrChild < 4; m_CurrChild++) {
		if (m_CurrNode->m_Children[m_CurrChild] && m_CurrNode->m_Children[m_CurrChild]->m_CellBound.Contains(m_Region)) {
			m_CurrNode = m_CurrNode->m_Children[m_CurrChild];
			m_CurrData = 0;
			m_CurrChild = 0;
			goto NEXT_DATA;
		}
	}

	// Step 3: Go back to the parent, find the next child
	spdQuadtreeNode<_Data>* myParent = m_CurrNode->m_Parent;
	for(int i = 0; i < 4; i++) {
		if (myParent->m_Children[i] == m_CurrNode) {
			break;
		}
	}
	m_CurrNode = m_Parent;
	if (m_Parent) {
		m_CurrData = m_Parent->m_CellData.GetCount();
		m_CurrChild = i+1;
	}
	else {
		m_CurrData = -1;
		m_CurrChild = -1;
		return;
	}
	goto NEXT_CHILD;
		
}

	template<typename _Data, typename _BboxFn, typename _ContainerType>
template<typename _Region>
	inline _Data& spdQuadtree<_Data,_BboxFn,_ContainerType>::SpatialIterator<_Region>::operator*() const
{
		FastAssert(m_CurrNode);
	return m_CurrNode->m_CellData[m_CurrData];
}

template<typename _D, typename _B>
template<typename _Region>
inline bool spdQuadtree<_D,_B>::SpatialIterator<_Region>::operator==(const spdQuadtree<_D,_B>::SpatialIterator<_Region>& other) const
{
	return m_CurrNode == other.m_CurrNode &&
		m_CurrData == other.m_CurrData &&
		m_CurrChild == other.m_CurrChild &&
		m_Region == other.m_Region;
}

template<typename _D, typename _B>
template<typename _Region>
inline bool spdQuadtree<_D,_B>::SpatialIterator<_Region>::operator!=(const spdQuadtree<_D,_B>::SpatialIterator<_Region>& other) const
{
	return m_CurrNode != other.m_CurrNode ||
		m_CurrData != other.m_CurrData ||
		m_CurrChild != other.m_CurrChild ||
		m_Region != other.m_Region;
}

template<typename _D, typename _B>
template<typename _Region>
inline SpatialIterator& spdQuadtree<_D,_B>::SpatialIterator<_Region>::operator=(const spdQuadtree<_D,_B>::SpatialIterator<_Region>& other)
{
	m_Region = other.m_Region;
	m_CurrNode = other.m_CurrNode;
	m_CurrData = other.m_CurrData;
	m_CurrChild = other.m_CurrChild;
}
#endif

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline spdQuadtree<_Data,_BboxFn,_ContainerType>::spdQuadtree()
: m_Root(NULL)
, m_LooseQuadtreeScale(1.5f)
, m_MinCellSize(10.0f)
{
}


	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline spdQuadtree<_Data,_BboxFn,_ContainerType>::~spdQuadtree()
{
	delete m_Root;
}

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtree<_Data,_BboxFn,_ContainerType>::BuildTree(atSList<_Data> &objects)
{
	FastAssert(!m_Root && "Tree has already been constructed");
	m_Root = rage_new spdQuadtreeNode<_Data, _BboxFn, _ContainerType>;

	// loop over all nodes, set the root's cell size
	for(atSNode<_Data>* iter = objects.GetHead(); iter; iter = iter->GetNext())
	{
		m_Root->m_CellBound.GrowAABB(m_BboxFn(iter->Data));
	}

	m_Root->BuildTree(objects, m_LooseQuadtreeScale, m_MinCellSize, m_BboxFn);
}

	template<typename _Data, typename _BboxFn, typename _ContainerType> template<typename _ArrayType>
	inline int spdQuadtree<_Data,_BboxFn,_ContainerType>::FindIntersectingData(const Vector3 &pos, _ArrayType& data, int maxData) const
{
	int count = 0;
	if (m_Root) {
		m_Root->FindIntersectingData(pos, data, maxData, count, m_BboxFn);
	}
	return count;
}

	template<typename _Data, typename _BboxFn, typename _ContainerType> template<typename _ArrayType>
	inline int spdQuadtree<_Data,_BboxFn,_ContainerType>::FindIntersectingData(const spdSphere &sphere, _ArrayType& data, int maxData) const
{
	int count = 0;
	if (m_Root) {
		m_Root->FindIntersectingData(sphere, true, data, maxData, count, m_BboxFn);
	}
	return count;
}

	template<typename _Data, typename _BboxFn, typename _ContainerType> template<typename _ArrayType>
	inline int spdQuadtree<_Data,_BboxFn,_ContainerType>::FindAllData(_ArrayType& data, int maxData) const
{
	int count = 0;
	if (m_Root) {
		m_Root->FindAllData(data, maxData, count);
	}
	return count;
}

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtree<_Data,_BboxFn,_ContainerType>::SetLooseQuadtreeScale(float scale)
{
	FastAssert(!m_Root && "Error: Can't change loose quadtree scale after building the tree");
	m_LooseQuadtreeScale = scale;
}

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtree<_Data,_BboxFn,_ContainerType>::SetMinCellSize(float size)
{
	FastAssert(!m_Root && "Error: Can't change min cell size after building the tree");
	m_MinCellSize = size;
}

	template<typename _Data, typename _BboxFn, typename _ContainerType>
	inline void spdQuadtree<_Data,_BboxFn,_ContainerType>::SetBoundingBoxFunction(_BboxFn function)
{
	FastAssert(!m_Root && "Error: Can't set bounding box function after building the tree");
	m_BboxFn=function;
}


} // namespace rage


#endif // SPATIALDATA_QUADTREE_H
