// 
// spatialdata/mnbvh.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "mnbvh.h"
#include "string.h"
#include "float.h"
#include "vector/color32.h"
#include "grcore/im.h"
#include "vectormath/legacyconvert.h"
#include "system/cache.h"
#include "system/memory.h"
#include "system/memmanager.h"


#if __BANK
#include "bank/bank.h"
#include "grcore/im.h"
#include "grcore/stateblock.h"
#endif

#define CACHE_LINE 128
template<unsigned int N>
__forceinline	 void PrefetchObjectInlineHelper( char* p ) 
{
	PrefetchObjectInlineHelper<N-1>(p);
	PrefetchDC( p + CACHE_LINE*(N-1));
}
template<>
__forceinline	 void PrefetchObjectInlineHelper<0>( char* /*p*/ ) {}

template<typename T>
__forceinline	 void PrefetchObjectInline( T* p )
{
	PrefetchObjectInlineHelper<sizeof(T)/CACHE_LINE>((char*)p);
}


namespace rage {

	void MNBVHNode::CreateBounds( const spdMNBVHBounds* blist, int amt )
	{
		Vec3V tmin[MNBVH_NODESIZE];
		Vec3V tmax[MNBVH_NODESIZE];

		for (int i = 0; i < MNBVH_NODESIZE; i++)
		{
			if ( i < amt )
			{
				tmin[i] = blist[i].m_bmin;
				tmax[i] = blist[i].m_bmax;
			}
			else
			{
				tmin[i] = Vec3V( V_FLT_MAX);
				tmax[i] = Vec3V( V_NEG_FLT_MAX);
			}
		}
		for (int i = 0; i < MNBVH_NODESIZE; i+=4)
		{
			Transpose3x4to4x3(
				minX[i/4], minY[i/4], minZ[i/4],
				tmin[i], tmin[i+1], tmin[i+2], tmin[i+3] );
			Transpose3x4to4x3(
				maxX[i/4], maxY[i/4], maxZ[i/4],
				tmax[i], tmax[i+1], tmax[i+2], tmax[i+3] );
		}
	}
	void MNBVHNode::GetBounds( spdMNBVHBounds blist[MNBVH_NODESIZE] ) const
	{
		for (int i = 0; i < MNBVH_NODESIZE; i+=4)
		{
			Transpose4x3to3x4(
				blist[i].m_bmin, blist[i+1].m_bmin, blist[i+2].m_bmin, blist[i+3].m_bmin,
				minX[i/4], minY[i/4], minZ[i/4] );
			Transpose4x3to3x4(
				blist[i].m_bmax, blist[i+1].m_bmax, blist[i+2].m_bmax, blist[i+3].m_bmax,
				maxX[i/4], maxY[i/4], maxZ[i/4] );
		}
	}

	void MNBVHNode::CreateBounds( spdMNBVHBoundList cnodes[16], int amt )
	{
		CompileTimeAssert( (sizeof(MNBVHNode)&127)==0);
		for (int i = amt; i < MNBVH_NODESIZE; i++)
		{
			cnodes[i].m_bmin = Vec3V( V_FLT_MAX);
			cnodes[i].m_bmax = Vec3V( V_NEG_FLT_MAX);
		}

		for (int i = 0; i < MNBVH_NODESIZE; i+=4)
		{
			Transpose3x4to4x3(
				minX[i/4], minY[i/4], minZ[i/4],
				cnodes[i].m_bmin, cnodes[i+1].m_bmin, cnodes[i+2].m_bmin, cnodes[i+3].m_bmin );
			Transpose3x4to4x3(
				maxX[i/4], maxY[i/4], maxZ[i/4],
				cnodes[i].m_bmax, cnodes[i+1].m_bmax, cnodes[i+2].m_bmax, cnodes[i+3].m_bmax );
		}
	}

	void MNBVHNode::Clear()
	{
		memset( leafs, 0, sizeof(MNBVHNode*)*MNBVH_NODESIZE);
		Vec4V fltMax =  Vec4V( V_FLT_MAX);
		Vec4V fltNegMax =  Vec4V( V_NEG_FLT_MAX);
		
		for (int i = 0; i < MNBVH_NODESIMDSIZE; i++)  // note could unroll
		{
			minX[i] = fltMax;
			minY[i] = fltMax;
			minZ[i] = fltMax;

			maxX[i] = fltNegMax;
			maxY[i] = fltNegMax;
			maxZ[i] = fltNegMax;
		}
	}

	void MNBVHNode::SetLeaf( int i, void* v )
	{
		size_t* lp = (size_t*)&leafs[i];
		*lp = (size_t)v |0x1;
	}

	void* MNBVHNode::GetLeaf( int i ) const
	{
		Assert( IsLeaf(i));
		size_t* lp = (size_t*)&leafs[i];
		return (void*)(*lp&~0x1);
	}
MNBVH_INLINE void mnbvhCompactResults( const MNBVHNode* const * __restrict leafs, VecBoolV_In r0in, VecBoolV_In r1in, VecBoolV_In r2in, VecBoolV_In r3in,
	const MNBVHNode** __restrict cnodes, void** __restrict results, 
	int& amtNodes, int& amtResults  );

void mnbvhCleanResults( void** results, int amt );

// Intersection Calculations
void MNBVHNodePreCalculateRay( Vec3V_In start, Vec3V_In end, 
	Vec3V_InOut origScaled,  Vec3V_InOut invDir, Vec4V_InOut maxT );

MNBVH_INLINE void MNBVHNodeIntersectRay( const MNBVHNode& n, Vec3V_In origScaled, Vec3V_In invDir, Vec4V_In maxT,
	VecBoolV_InOut r0,  VecBoolV_InOut r1, VecBoolV_InOut r2, VecBoolV_InOut r3);

MNBVH_INLINE void MNBVHNodeIntersectBox( const MNBVHNode& n, Vec3V_In bmin, Vec3V_In bmax,
	VecBoolV_InOut r0,  VecBoolV_InOut r1, VecBoolV_InOut r2, VecBoolV_InOut r3);


MNBVH_INLINE void MNBVHNodeIntersect( const MNBVHNode& n, Vec3V_In bmin, Vec3V_In bmax,
	const MNBVHNode**  cnodes, void** __restrict results, 
	int& amtNodes, int& amtResults );


#if __64BIT
MNBVH_INLINE void mnbvhCompactResults64( const MNBVHNode* const * __restrict leafs, VecBoolV_In r0in, VecBoolV_In r1in, VecBoolV_In r2in, VecBoolV_In r3in,
									  const MNBVHNode** __restrict cnodes, void** __restrict results, 
									  int& amtNodes, int& amtResults  )
{
	Vec4V isSelected[4];
	isSelected[0] = Vec4V(r0in);
	isSelected[1] = Vec4V(r1in);
	isSelected[2] = Vec4V(r2in);
	isSelected[3] = Vec4V(r3in); 

	u32* select = (u32*)isSelected;

	int cntr = 0;
	int cntcn = 0; 
	for (int i=0;i <16;i++)
	{
		size_t r = (size_t)leafs[i];
		if (( select[i] & r)!=0)
		{
			if( r&0x1)
			{	
				results[cntr++]=(void*)leafs[i];				
			}
			else
			{
				cnodes[cntcn++]=leafs[i];				
			}	
		}
	}
	amtNodes += cntcn;
	amtResults += cntr;
}
#endif

	// fast order independent stream compaction 

/*
// do pass and pack masks
// load hit store back in masks
// then loop over data
u32 val = GeneratePackedValidMask(r0,r1,r2,r3);
int count = count;
for (int i=0; i<4;i++){  // should be able to unroll this
	mask = val&0xF;
	Vec4V compacted = Shuffle(ShuffleIndices[mask]);
	VecStoreUnaligned(&res[count], compacted);
	val>>=4;
	count += bitCount[mask];
}
*/
	MNBVH_INLINE void mnbvhCompactResults( const MNBVHNode* const * __restrict leafs, VecBoolV_In r0in, VecBoolV_In r1in, VecBoolV_In r2in, VecBoolV_In r3in,
		const MNBVHNode** __restrict cnodes, void** __restrict results, 
		int& amtNodes, int& amtResults  )
	{
		
#if __64BIT
		mnbvhCompactResults64( leafs, r0in, r1in, r2in, r3in, cnodes, results, amtNodes, amtResults );
#else
		Vec4V resNodes[4];
		Vec4V resChild[4];
		int* crNodes = (int*)resNodes;
		int* crChild = (int*)resChild;
		Vec4V SetFirstBit(V_INT_1);
		Vec4V zero = Vec4V(V_ZERO);
		const Vec4V* childnodes =reinterpret_cast<const Vec4V*>(leafs);

		Vec4V c0= childnodes[0];
		Vec4V c1= childnodes[1];
		Vec4V c2= childnodes[2];
		Vec4V c3= childnodes[3];

		Vec4V r0 = Vec4V(r0in) & SetFirstBit;
		Vec4V r1 = Vec4V(r1in) & SetFirstBit;
		Vec4V r2 = Vec4V(r2in) & SetFirstBit;
		Vec4V r3 = Vec4V(r3in) & SetFirstBit;

		// clear empty nodes
		r0 = r0 & Vec4V(InvertBits( IsEqualInt( c0,zero)));
		r1 = r1 & Vec4V(InvertBits( IsEqualInt( c1,zero)));
		r2 = r2 & Vec4V(InvertBits( IsEqualInt( c2,zero)));
		r3 = r3 & Vec4V(InvertBits( IsEqualInt( c3,zero)));


		// get the leaf nodes
		resChild[0] = r0 & c0;
		resChild[1] = r1 & c1;
		resChild[2] = r2 & c2;
		resChild[3] = r3 & c3;

		// get the child nodes
		resNodes[0] = r0 & InvertBits(c0);
		resNodes[1] = r1 & InvertBits(c1);
		resNodes[2] = r2 & InvertBits(c2);
		resNodes[3] = r3 & InvertBits(c3);

		register int cntn = 0;
		register int cnt = 0;
		for( register int i = 0; i < 16; i++ )
		{
			cnodes[cntn] = leafs[i]; cntn+=crNodes[i];
			results[cnt] = (void*)leafs[i]; cnt+=crChild[i];
		}

		amtNodes += cntn;
		amtResults += cnt;

#endif
	}

	void mnbvhCleanResults( void** results, int amt )
	{
#if !__64BIT	// This would need to be rewritten to do two at a time, not worth the trouble
		if ( ((int)results&15)==0 )  // do fast version if 16 byte aligned
		{
			Vec4V SetFirstBit(V_INT_1);
			Vec4V* r = (Vec4V*)results;
			int amt4 = ((amt+3)&~3)>>2;
			Assert( amt4*4 >= amt );
			for (int i = 0; i < amt4; i+=4)
			{
				r[0] = Andc(r[0], SetFirstBit);
				r[1] = Andc(r[1], SetFirstBit);
				r[2] = Andc(r[2], SetFirstBit);
				r[3] = Andc(r[3], SetFirstBit);
				r +=4;
			}
		}
		else
#endif
		{
			ptrdiff_t* r = (ptrdiff_t*)results;
			for (int i = 0; i < amt; i++)
			{
				r[i] = r[i] & ~0x1;
			}
		}
	}

	void MNBVHNodePreCalculateRay( Vec3V_In start, Vec3V_In end, 
									Vec3V_InOut origScaled,  Vec3V_InOut invDir, Vec4V_InOut maxT )
	{
		Vec3V delta = end - start;
		maxT = Vec4V(Mag( delta ));
		origScaled = start;
		invDir = InvertSafe( delta ) * maxT.GetX();
	}
	MNBVH_INLINE void MNBVHNodeIntersectRay( const MNBVHNode& n, Vec3V_In origScaled, Vec3V_In invDir, Vec4V_In maxT,
		VecBoolV_InOut r0,  VecBoolV_InOut r1, VecBoolV_InOut r2, VecBoolV_InOut r3)
	{

		Vec4V  ox = Vec4V(origScaled.GetX());	
		Vec4V  oy = Vec4V(origScaled.GetY());
		Vec4V  oz = Vec4V(origScaled.GetZ());	  

		const Vec4V  invDx = Vec4V(invDir.GetX());            
		const Vec4V  invDy = Vec4V(invDir.GetY());            
		const Vec4V  invDz = Vec4V(invDir.GetZ());  
		ox = -ox * invDx;
		oy = -oy * invDy;
		oz = -oz * invDz;

		Vec4V t0x_0 = AddScaled( ox, n.minX[0] ,invDx );           
		Vec4V t1x_0 = AddScaled( ox, n.maxX[0] ,invDx );
		Vec4V t0x_1 = AddScaled( ox, n.minX[1] ,invDx );           
		Vec4V t1x_1 = AddScaled( ox, n.maxX[1] ,invDx );
		Vec4V t0x_2 = AddScaled( ox, n.minX[2] ,invDx );           
		Vec4V t1x_2 = AddScaled( ox, n.maxX[2] ,invDx );
		Vec4V t0x_3 = AddScaled( ox, n.minX[3] ,invDx );           
		Vec4V t1x_3 = AddScaled( ox, n.maxX[3] ,invDx );

		Vec4V tmin0 = Min(t0x_0, t1x_0);           
		Vec4V tmax0 = Max(t0x_0, t1x_0); 
		Vec4V tmin1 = Min(t0x_1, t1x_1);           
		Vec4V tmax1 = Max(t0x_1, t1x_1); 
		Vec4V tmin2 = Min(t0x_2, t1x_2);           
		Vec4V tmax2 = Max(t0x_2, t1x_2); 
		Vec4V tmin3 = Min(t0x_3, t1x_3);           
		Vec4V tmax3 = Max(t0x_3, t1x_3); 

		Vec4V  t0y_0 = AddScaled( oy, n.minY[0], invDy);           
		Vec4V  t1y_0 = AddScaled( oy, n.maxY[0], invDy);
		Vec4V  t0y_1 = AddScaled( oy, n.minY[1], invDy);           
		Vec4V  t1y_1 = AddScaled( oy, n.maxY[1], invDy);
		Vec4V  t0y_2 = AddScaled( oy, n.minY[2], invDy);           
		Vec4V  t1y_2 = AddScaled( oy, n.maxY[2], invDy);
		Vec4V  t0y_3 = AddScaled( oy, n.minY[3], invDy);           
		Vec4V  t1y_3 = AddScaled( oy, n.maxY[3], invDy);

		Vec4V  neary_0 = Min(t0y_0, t1y_0);           
		Vec4V  fary_0  = Max(t0y_0, t1y_0);	   
		Vec4V  neary_1 = Min(t0y_1, t1y_1);           
		Vec4V  fary_1  = Max(t0y_1, t1y_1);	  
		Vec4V  neary_2 = Min(t0y_2, t1y_2);           
		Vec4V  fary_2  = Max(t0y_2, t1y_2);	  
		Vec4V  neary_3 = Min(t0y_3, t1y_3);           
		Vec4V  fary_3  = Max(t0y_3, t1y_3);	  

		tmax0 = Min( fary_0, tmax0 );	 
		tmin0 = Max( neary_0, tmin0 );	 
		tmax1 = Min( fary_1, tmax1 );	 
		tmin1 = Max( neary_1, tmin1 );	
		tmax2 = Min( fary_2, tmax2 );	 
		tmin2 = Max( neary_2, tmin2 );	
		tmax3 = Min( fary_3, tmax3 );	 
		tmin3 = Max( neary_3, tmin3 );	

		Vec4V  t0z_0 = AddScaled( oz, n.minZ[0], invDz);           
		Vec4V  t1z_0 = AddScaled( oz, n.maxZ[0], invDz);
		Vec4V  t0z_1 = AddScaled( oz, n.minZ[1], invDz);           
		Vec4V  t1z_1 = AddScaled( oz, n.maxZ[1], invDz);
		Vec4V  t0z_2 = AddScaled( oz, n.minZ[2], invDz);           
		Vec4V  t1z_2 = AddScaled( oz, n.maxZ[2], invDz);
		Vec4V  t0z_3 = AddScaled( oz, n.minZ[3], invDz);           
		Vec4V  t1z_3 = AddScaled( oz, n.maxZ[3], invDz);

		Vec4V  nearz_0 = Min(t0z_0, t1z_0);           
		Vec4V  farz_0  = Max(t0z_0, t1z_0);	   
		Vec4V  nearz_1 = Min(t0z_1, t1z_1);           
		Vec4V  farz_1  = Max(t0z_1, t1z_1);	  
		Vec4V  nearz_2 = Min(t0z_2, t1z_2);           
		Vec4V  farz_2  = Max(t0z_2, t1z_2);	  
		Vec4V  nearz_3 = Min(t0z_3, t1z_3);           
		Vec4V  farz_3  = Max(t0z_3, t1z_3);	  

		tmax0 = Min( farz_0, tmax0 );	 
		tmin0 = Max( nearz_0, tmin0 );	 
		tmax1 = Min( farz_1, tmax1 );	 
		tmin1 = Max( nearz_1, tmin1 );	
		tmax2 = Min( farz_2, tmax2 );	 
		tmin2 = Max( nearz_2, tmin2 );	
		tmax3 = Min( farz_3, tmax3 );	 
		tmin3 = Max( nearz_3, tmin3 );	

		Vec4V	zero(V_ZERO);
		VecBoolV rv0 =   tmax0 > zero &  tmax0 >= tmin0 & tmin0 < maxT ;
		VecBoolV rv1 =   tmax1 > zero &  tmax1 >= tmin1 & tmin1 < maxT ;
		VecBoolV rv2 =   tmax2 > zero &  tmax2 >= tmin2 & tmin2 < maxT ;
		VecBoolV rv3 =   tmax3 > zero &  tmax3 >= tmin3 & tmin3 < maxT ;

		r0 = rv0;
		r1 = rv1;
		r2 = rv2;
		r3 = rv3;
	}
	MNBVH_INLINE void MNBVHNodeIntersectBox( const MNBVHNode& n, Vec3V_In bmin, Vec3V_In bmax,
		VecBoolV_InOut r0,  VecBoolV_InOut r1, VecBoolV_InOut r2, VecBoolV_InOut r3)
	{
		Vec4V bminX = Vec4V( bmin.GetX());
		Vec4V bminY = Vec4V( bmin.GetY());
		Vec4V bminZ = Vec4V( bmin.GetZ());

		Vec4V bmaxX = Vec4V( bmax.GetX());
		Vec4V bmaxY = Vec4V( bmax.GetY());
		Vec4V bmaxZ = Vec4V( bmax.GetZ());

		VecBoolV in0 = bminX < n.maxX[0];
		VecBoolV in1 = bminX < n.maxX[1];
		VecBoolV in2 = bminX < n.maxX[2];
		VecBoolV in3 = bminX < n.maxX[3];

		in0 = in0 & bminY < n.maxY[0];
		in1 = in1 & bminY < n.maxY[1];
		in2 = in2 & bminY < n.maxY[2];
		in3 = in3 & bminY < n.maxY[3];

		in0 = in0 & bminZ < n.maxZ[0];
		in1 = in1 & bminZ < n.maxZ[1];
		in2 = in2 & bminZ < n.maxZ[2];
		in3 = in3 & bminZ < n.maxZ[3];

		in0 = in0 & bmaxX > n.minX[0];
		in1 = in1 & bmaxX > n.minX[1];
		in2 = in2 & bmaxX > n.minX[2];
		in3 = in3 & bmaxX > n.minX[3];

		in0 = in0 & bmaxY > n.minY[0];
		in1 = in1 & bmaxY > n.minY[1];
		in2 = in2 & bmaxY > n.minY[2];
		in3 = in3 & bmaxY > n.minY[3];

		in0 = in0 & bmaxZ > n.minZ[0];
		in1 = in1 & bmaxZ > n.minZ[1];
		in2 = in2 & bmaxZ > n.minZ[2];
		in3 = in3 & bmaxZ > n.minZ[3];

		r0 = (in0);
		r1 = (in1);
		r2 = (in2);
		r3 = (in3);
	}

	
	void MNBVHNodeIntersect( const MNBVHNode& n, Vec3V_In bmin, Vec3V_In bmax,
		const MNBVHNode**  cnodes, void** __restrict results, 
		int& amtNodes, int& amtResults)
	{
		VecBoolV r0,r1,r2,r3;
		MNBVHNodeIntersectBox( n, bmin, bmax,r0,r1,r2,r3);
		mnbvhCompactResults( &n.leafs[0], r0,r1,r2,r3, cnodes, results, amtNodes, amtResults );
	}	


	
	int MNBVHTraverseBoxInternal(const MNBVHNode* root, Vec3V_In bmin, Vec3V_In bmax, void** results, int maxAmt)
	{
		const MNBVHNode*	stack[512];
		int  stackSize=2;
		stack[0]=root;  // saves a prefetch check by setting it to 2
		stack[1]=root;

		int maxResults  = maxAmt - MNBVH_NODESIZE;
		int amtResults = 0;
		const MNBVHNode* node = NULL;

		MNBVHNodeIntersect( *root, bmin, bmax, stack + stackSize, results + amtResults, stackSize, amtResults);
		while( stackSize > 2)
		{	
			node = stack[--stackSize];
			PrefetchObjectInline(stack[stackSize-1]);

			MNBVHNodeIntersect( *node, bmin, bmax, stack + stackSize, results + amtResults, stackSize, amtResults);
			amtResults = Min( amtResults, maxResults );
		}

		mnbvhCleanResults(results, amtResults);
		return amtResults;
	}
	int MNBVHTraverseRayInternal(const MNBVHNode* root, Vec3V_In start, Vec3V_In end, void** results, int maxAmt)
	{
		Assert( maxAmt > MNBVH_NODESIZE*2);

		const MNBVHNode*	stack[512];
		int  stackSize=1;
		stack[0]=root;

		Vec3V orig, invd;
		Vec4V maxT;
		MNBVHNodePreCalculateRay( start, end, orig,  invd, maxT);

		int maxResults  = maxAmt - MNBVH_NODESIZE;
		int amtResults = 0;
		while( stackSize > 0)
		{
			const MNBVHNode*  node = stack[--stackSize]; 

			VecBoolV r0,r1,r2,r3;
			MNBVHNodeIntersectRay( *node, orig, invd, maxT,r0,r1,r2,r3);
			mnbvhCompactResults( &node->leafs[0], r0,r1,r2,r3, stack + stackSize, results + amtResults, stackSize, amtResults );

			amtResults = Min( amtResults, maxResults );
			// TODO - add sorting to results along ray
		}
		mnbvhCleanResults(results, amtResults);
		return amtResults;
	}

#if __BANK
	// Debug and Anaylsis functions
	
void MNBVHCalcDiagnosticsRecurse(  const MNBVHNode* node, int& numNodes, int& numObjects, int& maxTreeDepth, int &amtInNodes, int depth )
{
	depth++;
	if ( depth > maxTreeDepth)
	{
		maxTreeDepth = depth;
	}
	numNodes++;
	for (int i =0; i <MNBVH_NODESIZE; i++ )
	{
		if ( node->IsEmpty(i))
		{
			amtInNodes += i;
			return;
		}
		if ( node->IsLeaf(i))
		{
			numObjects++;
		}
		else
		{
			MNBVHCalcDiagnosticsRecurse( node->leafs[i], numNodes, numObjects, maxTreeDepth, amtInNodes, depth );
		}
	}
	amtInNodes += MNBVH_NODESIZE;
}
void MNBVHCalcDiagnostics( const MNBVHNode* node, int& numNodes, int& numObjects, int& maxTreeDepth, float& nodeOccupancy)
{
	int amtInNodes = 0;
	numNodes = numObjects = maxTreeDepth = 0;
	MNBVHCalcDiagnosticsRecurse( node, numNodes, numObjects, maxTreeDepth, amtInNodes, 0 );

	nodeOccupancy = (float)amtInNodes/(float)(numNodes * MNBVH_NODESIZE );
}


void MNBVHDrawRecurse(  const MNBVHNode* node, int sd, int fd, Vec4V_In col,  Vec4V_In dcol, int depth, bool drawSolid )
{
	Vec4V ncol = Saturate( col + dcol);
	depth++;
	
	spdMNBVHBounds bs[MNBVH_NODESIZE];
	node->GetBounds(bs);

	for (int i =0; i <MNBVH_NODESIZE; i++ )
	{
		if ( node->IsEmpty(i))			
		{				
			return;
		}
		if ( depth >= sd && depth <= fd)
		{
			Color32 col32;
			col32.SetFromRGBA(ncol );
			if ( drawSolid)
			{
				grcDrawSolidBox( VEC3V_TO_VECTOR3(bs[i].m_bmin), VEC3V_TO_VECTOR3(bs[i].m_bmax), col32 );
			}
			else
			{
				grcDrawBox( VEC3V_TO_VECTOR3(bs[i].m_bmin), VEC3V_TO_VECTOR3(bs[i].m_bmax), col32 );
			}
		}
		if ( !node->IsLeaf(i))
		{
			MNBVHDrawRecurse( node->leafs[i], sd, fd, ncol, dcol, depth, drawSolid );
		}
	}
}
void MNBVHDraw(  const MNBVHNode* node,  int sd, int fd, float alpha, bool drawSolid/* = false */)
{
	Vec4V col(0.1f, 0.2f, 0.1f, alpha);
	MNBVHDrawRecurse( node, sd, fd, col, col, 0 ,drawSolid);
}
#endif


void MNBVHDeleteNode(const MNBVHNode* node, int& delCnt,  MNBVHNode::DeleteFunction  deleter )
{
	
	for (int i =0; i <MNBVH_NODESIZE; i++ )
	{
		if ( node->IsEmpty(i))			
		{	
			i = MNBVH_NODESIZE;
		}
		else if ( !node->IsLeaf(i))
		{
			MNBVHDeleteNode( node->leafs[i], delCnt, deleter );
		}
	}
	deleter(node);
	delCnt +=1;
	return;
}
int MNBVHDeleteNode(const MNBVHNode* node , MNBVHNode::DeleteFunction deleter )
{
	if ( !node)
	{
		return 0;
	}
	int delCnt  = 0;
	MNBVHDeleteNode( node, delCnt, deleter);
	return delCnt;
}
int MNBVHCalcMaxNodes( int numObjects )
{
	// assume 40% occupancy
	float leafNodeCount = (float)numObjects/7.f;
	float leafNodeCount2 = leafNodeCount/7.f;
	float leafNodeCount3 = leafNodeCount2/7.f;
	return Max(1, (int)(leafNodeCount + leafNodeCount2 + leafNodeCount3));
}

MNBVHNode* MNBVHAllocateDefault( int /*depth*/)
{
	USE_MEMBUCKET(MEMBUCKET_SYSTEM);
	return rage_aligned_new(128) MNBVHNode();
}
void MNBVHDeleteDefault( const MNBVHNode* node )
{
	delete node;
}
void MNBVHDeleteNull( const MNBVHNode* /*node*/ )
{
}

// Building tree algorithm from  http://graphics.stanford.edu/~boulos/papers/multi_rt08.pdf
// page 4
MNBVHNode* MNBVHBuildMultiNodeBVHTree( spdMNBVHBoundList& nodes,  MNBVHNode::AllocateFunction allocator, int MaxPerLeaf, int depth  )
{
	MNBVHNode* node = allocator(depth);
	Assert128(node);
	node->Clear();

	int amtInNodeList = nodes.Amt();
	// fast path for less than nodeSize nodes
	if ( Likely( amtInNodeList <= MNBVH_NODESIZE ))  // add a fast child node
	{
		node->CreateBounds(nodes.m_start, amtInNodeList);
		for (int i = 0; i < amtInNodeList; i++ )
		{
			node->SetLeaf( i, nodes.m_start[i].m_data );  // could make this faster
		}
		return node;
	}
	int numCNodes =1;
	spdMNBVHBoundList cnodes[MNBVH_NODESIZE];
	cnodes[0]=nodes;
	while ( numCNodes < MNBVH_NODESIZE)
	{
		int maxAreaIdx = -1;
		float maxAreaF = -FLT_MAX;
		for (int i = 0; i < numCNodes; i++)
		{
			float areaf = cnodes[i].AreaV().Getf();
			if ( !cnodes[i].IsLeaf(MaxPerLeaf) && areaf > maxAreaF)
			{
				maxAreaF = areaf;
				maxAreaIdx = i;
			}
		}
		if ( maxAreaIdx == -1 )
		{
			break; // 
		}
		// now split it
		spdMNBVHBoundList left;
		cnodes[maxAreaIdx].Split( left, cnodes[numCNodes++]);		
		cnodes[maxAreaIdx] = left;
	}

	node->CreateBounds(cnodes, numCNodes);
	for (int i = 0; i <numCNodes; i++)
	{
		if ( !cnodes[i].IsLeaf(1))
		{
			node->SetChild( i, MNBVHBuildMultiNodeBVHTree( cnodes[i], allocator, MaxPerLeaf, depth+1));
		}
		else
		{
			node->SetLeaf( i, cnodes[i].m_start->m_data );
		}
	}
	return node;
}


spdMNBVHBoundList::spdMNBVHBoundList( spdMNBVHBounds* s, spdMNBVHBounds* e )
{
	int amt = (int) (e - s);
	Vec3V lmin(s[0].m_bmin), lmax(s[0].m_bmax);
	for (int i = 1; i < amt; i++)
	{
		lmin = Min( s[i].m_bmin, lmin);
		lmax = Max( s[i].m_bmax, lmax);
	}
	m_bmin = lmin;
	m_bmax = lmax;
	m_start = s;
	m_end = e;
}

	ScalarV_Out spdMNBVHBoundList::AreaV() const
	{
		Vec3V a = (m_bmax - m_bmin);
		a  = a * a.Get<Vec::Y, Vec::Z, Vec::X>();
		return ScalarV(V_TWO) *( a.GetX()  + a.GetY()  + a.GetZ());
	}

	int spdMNBVHBoundList::GetSplitIndex() const
	{
		// keep everything in vector pipe
		Vec3V diff = m_bmax - m_bmin;
		/// Vec3V mid = ( m_bmin + m_bmax ) * Vec3V(V_HALF);

		Vec3V r = Vec3VConstant<0,1,2>();

		BoolV XgtY = diff.GetX() > diff.GetY();
		BoolV XgtZ = diff.GetX() > diff.GetZ();
		BoolV YgtZ = diff.GetY() > diff.GetZ();

		ScalarV res = SelectFT( XgtY, 
			SelectFT( YgtZ,  r.GetZ(), r.GetY() ),
			SelectFT( XgtZ, r.GetZ(), r.GetX() ) );
		return *((int*)&res);	
	}

	Vec3V_Out spdMNBVHBoundList::GetSplitPlane( int idx ) const
	{
		static const VecBoolV table[3] = { VecBoolV(V_T_F_F_F),VecBoolV(V_F_T_F_F),VecBoolV(V_F_F_T_F) };
		Vec3V mid = ( m_bmin + m_bmax ) * Vec3V(V_HALF);

		return SelectFT( table[idx], Vec3V(V_FLT_MAX), mid );
	}

	void spdMNBVHBoundList::Split( spdMNBVHBoundList& left, spdMNBVHBoundList& right ) const
	{
		int splitIdx = GetSplitIndex();
		int intialSplitIndex= splitIdx;

		bool done = false;
		spdMNBVHBounds* leftside = 0;
		spdMNBVHBounds* rightside = 0;
		while(!done)
		{
			// sort into two lists around split plane
			leftside = m_start;
			rightside = m_end;
			Vec3V   splitPlane = GetSplitPlane(splitIdx);
				for ( leftside = m_start; leftside < rightside ; )
			{
				Vec3V c = ( leftside->m_bmin + leftside->m_bmax ) * Vec3V(V_HALF);
				if (IsLessThanAll(c, splitPlane) )  // TODO - get rid of branch
					leftside++;					
				else
				{
					rightside--;	// DOES THIS CREATE A BUNCH OF LOAD HIT STORES?
					spdMNBVHBounds b = *rightside;
					*rightside = *leftside;
					*leftside=b;					 
				}
			}
			Assert( rightside == leftside);
			done = true;
			if (Unlikely( rightside == m_start|| leftside == m_start ))
			{
				done = false;
				splitIdx = (splitIdx + 1 ) %3;
				if ( Unlikely( splitIdx == intialSplitIndex ))
				{
					// got in a infinite loop so just break into two dependent on median idx
					u32 idx=0;
					for( spdMNBVHBounds* v=m_start; v !=m_end;v++ ){
						idx+=(u32)(uintptr_t)(v->m_data);
					}
					idx /= Amt();

					rightside = m_end;
					for ( leftside = m_start; leftside < rightside ; )
					{
						u32 c = (u32)(uintptr_t)(leftside->m_data);				
						if ( c <idx )  
							leftside++;					
						else
						{
							rightside--;
							spdMNBVHBounds b = *rightside;
							*rightside = *leftside;
							*leftside=b;					 
						}
					}
					done = true;

					Assert( rightside != m_end);
					Assert( leftside != m_start);
				}
			}
		}
		Assert( rightside != m_start);
		Assert( leftside != m_start);

		ASSERT_ONLY(int amountRight = (int) (m_end-rightside);)
		left = spdMNBVHBoundList( m_start, leftside );

		Assert( m_end == leftside + amountRight );
		right = spdMNBVHBoundList(leftside, m_end );

		Assert( left.Amt() + right.Amt() == Amt());
	}

#if __BANK
	spdMNBVHTreeVis::spdMNBVHTreeVis( const char* treeName )
	{
		m_numNodes = m_numObjects = m_maxTreeDepth = 0;
		m_update = true;
		m_debugDraw = m_drawLeafs = false;
		m_alpha = 0.2f;
		m_startDrawDepth = 0;
		m_FinishDrawDepth = 30;
		m_drawSolid = false;
		safecpy( m_treeName, treeName );
	}

	void spdMNBVHTreeVis::Calc( MNBVHNode* tree )
	{
		if ( m_update)
			MNBVHCalcDiagnostics( tree, m_numNodes, m_numObjects, m_maxTreeDepth, m_nodeOccupancy);
	}

	void spdMNBVHTreeVis::DebugDraw( MNBVHNode* tree )
	{
		if ( !m_debugDraw)
		{
			return;
		}
		grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
		MNBVHDraw( tree, m_startDrawDepth, m_FinishDrawDepth, m_alpha, m_drawSolid );
	}

	void spdMNBVHTreeVis::AddWidgets( bkBank& bk )
	{
		bk.PushGroup(m_treeName);
		bk.AddToggle("Update", &m_update);

		bk.PushGroup("Stats");
		bk.AddText("Num Nodes", &m_numNodes);
		bk.AddText("Num Objects", &m_numObjects);
		bk.AddText("Max Tree Depth", &m_maxTreeDepth);
		bk.AddText("Node Occupancy %i", &m_nodeOccupancy);
		bk.PopGroup();

		bk.PushGroup("Draw");
		bk.AddToggle("Debug Draw", &m_debugDraw);
		bk.AddToggle("Draw Solid", &m_drawSolid);
		bk.AddSlider("Draw Start Depth", &m_startDrawDepth, 0, 30,1);
		bk.AddSlider("Draw Finish Depth", &m_FinishDrawDepth, 0, 30,1);
		bk.AddSlider("Draw Alpha", &m_alpha, 0.0f, 1.0f,0.1f);
		bk.PopGroup();
		bk.PopGroup();
	}

#endif

	MNBVHNode* MNBVHSimpleInterface::ms_nodeStorage = NULL;
	s32 MNBVHSimpleInterface::ms_numNodesAllocated = 0;
	
	MNBVHSimpleInterface::MNBVHSimpleInterface(const atArray<spdAABB>& boxList)
		: m_nodes(NULL), m_pTree(NULL), m_bUseArrayAlloc(true)
	{
		USE_MEMBUCKET(MEMBUCKET_SYSTEM);

		// count number of valid entries
		u32 numEntries = 0;
		for (s32 i=0; i<boxList.GetCount(); i++)
		{
			if (boxList[i].IsValid())
			{
				numEntries++;
			}
		}

		// create temp memory used in constructing mnbvh
		spdMNBVHBounds* boundsArray;

#if SCRATCH_ALLOCATOR
		const bool useScratch = sysMemManager::GetInstance().GetScratchAllocator()->IsReady();

		if (useScratch)
		{
			sysMemAutoUseScratchMemory mem;
			boundsArray = rage_aligned_new(128) spdMNBVHBounds[numEntries];
		}
		else
#endif
		{
#if ENABLE_CHUNKY_ALLOCATOR
 			ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), true, "MNBVHSimpleInterface");
#endif // ENABLE_CHUNKY_ALLOCATOR
			boundsArray = rage_aligned_new(128) spdMNBVHBounds[numEntries];
		}		

		// populate bounds
		u32 current = 0;
		for (s32 i=0; i<boxList.GetCount(); i++)
		{
			const spdAABB& box = boxList[i];
			if (box.IsValid())
			{
				boundsArray[current].m_bmin = box.GetMin();
				boundsArray[current].m_bmax = box.GetMax();
				boundsArray[current].m_data = (void*) (size_t) (i << 2);	//NB: mnbvh makes use of LSB internally
				current++;
			}
		}
		Assert(current == numEntries);

		spdMNBVHBoundList boundsList( boundsArray, boundsArray+numEntries );
#if __BANK
		if (!boundsList.IsValid()) {
			boundsList = spdMNBVHBoundList( boundsArray, boundsArray);
		}
#endif
	
		// create tree
		if (m_bUseArrayAlloc)
		{
			MNBVHNode* stackBuffer;

#if SCRATCH_ALLOCATOR
			if (useScratch)
			{
				sysMemAutoUseScratchMemory mem;
				stackBuffer = rage_aligned_new(128) MNBVHNode[MaxTreeDepth];
			}
			else
#endif
			{
#if ENABLE_CHUNKY_ALLOCATOR
				ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), true, "MNBVHSimpleInterface");
#endif // ENABLE_CHUNKY_ALLOCATOR
				stackBuffer = rage_aligned_new(128) MNBVHNode[MaxTreeDepth];
			}	

			ms_nodeStorage = stackBuffer;
			ms_numNodesAllocated = 0;
			MNBVHBuildMultiNodeBVHTree( boundsList,  StackAllocatorCB );

#if SCRATCH_ALLOCATOR
			// HACK: DO NOT call free if you are using scratch buffer!
			if (!useScratch)
#endif
			{
#if ENABLE_CHUNKY_ALLOCATOR
				ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), false, "MNBVHSimpleInterface");
#endif // ENABLE_CHUNKY_ALLOCATOR
				delete[] stackBuffer;
			}				

			s32 maxNumNodes = ms_numNodesAllocated;
			m_nodes = rage_aligned_new(128) MNBVHNode[maxNumNodes];
			ms_nodeStorage = m_nodes;
			ms_numNodesAllocated = 0;
			m_pTree = MNBVHBuildMultiNodeBVHTree( boundsList,  ArrayNewCB );
			Assert( ms_numNodesAllocated <= maxNumNodes );			
		}
		else
		{
			m_pTree = MNBVHBuildMultiNodeBVHTree( boundsList, MNBVHAllocateDefault );
		}

#if SCRATCH_ALLOCATOR
		// HACK: DO NOT call free if you are using scratch buffer!
		if (!useScratch)
#endif
		{
#if ENABLE_CHUNKY_ALLOCATOR
			ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), false, "MNBVHSimpleInterface");
#endif // ENABLE_CHUNKY_ALLOCATOR
			delete [] boundsArray;
		}		
	}

	MNBVHSimpleInterface::MNBVHSimpleInterface(atArray<spdMNBVHBounds>& bounds)
		: m_nodes(NULL), m_pTree(NULL), m_bUseArrayAlloc(true)
	{
		USE_MEMBUCKET(MEMBUCKET_SYSTEM);
		USE_DEBUG_MEMORY();

		// create temp memory used in constructing mnbvh
		spdMNBVHBounds* boundsArray = bounds.GetElements();
		u32 numEntries = bounds.GetCount();

		spdMNBVHBoundList boundsList( boundsArray, boundsArray+numEntries );

#if __BANK
		if (!boundsList.IsValid()) {
			boundsList = spdMNBVHBoundList( boundsArray, boundsArray);
		}
#endif
		// create tree
		if (m_bUseArrayAlloc)
		{
			MNBVHNode*	stackBuffer = nullptr;
			{
#if ENABLE_CHUNKY_ALLOCATOR
				ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), true, "MNBVHSimpleInterface");
#endif // ENABLE_CHUNKY_ALLOCATOR
				stackBuffer = rage_aligned_new(128) MNBVHNode[MaxTreeDepth];
			}

			ms_nodeStorage = stackBuffer;
			ms_numNodesAllocated = 0;
			MNBVHBuildMultiNodeBVHTree( boundsList, StackAllocatorCB );

			s32 maxNumNodes = ms_numNodesAllocated;

			{
#if ENABLE_CHUNKY_ALLOCATOR
				ChunkyWrapper a(*sysMemManager::GetInstance().GetChunkyAllocator(), false, "MNBVHSimpleInterface");
#endif // ENABLE_CHUNKY_ALLOCATOR
				delete[] stackBuffer;
			}

			m_nodes = rage_aligned_new(128) MNBVHNode[maxNumNodes];
			ms_nodeStorage = m_nodes;
			ms_numNodesAllocated = 0;
			m_pTree = MNBVHBuildMultiNodeBVHTree( boundsList, ArrayNewCB );
			Assert( ms_numNodesAllocated <= maxNumNodes );			
		}
		else
		{
			m_pTree = MNBVHBuildMultiNodeBVHTree( boundsList,  MNBVHAllocateDefault );
		}

	}

	MNBVHSimpleInterface::~MNBVHSimpleInterface()
	{
		if (m_pTree)
		{
			MNBVHDeleteNode( m_pTree, ( m_bUseArrayAlloc ? MNBVHDeleteNull : MNBVHDeleteDefault ) );
			m_pTree = NULL;
		}
		if (m_nodes)
		{
			delete[] m_nodes;
			m_nodes = NULL;
		}
	}

} // namespace rage
