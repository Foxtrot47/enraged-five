//
// spatialdata/kdop2d.h
//
// Copyright (C) 2013 Rockstar Games.  All Rights Reserved.
//

#ifndef SPATIALDATA_KDOP2D_H
#define SPATIALDATA_KDOP2D_H

#include "system/codecheck.h"
#include "vectormath/classes.h"

namespace rage {

template <typename VecType, int N> inline int GetUniquePointsInLoop(VecType* unique, const VecType points[N], ScalarV_In threshold = ScalarV(V_ZERO))
{
	int count = 0;
	VecType prev;

	unique[count++] = prev = points[0];

	for (int i = 1; i < N; i++)
	{
		if (!IsLessThanOrEqualAll(Abs(points[i] - prev), VecType(threshold)))
		{
			unique[count++] = prev = points[i];
		}
	}

	if (count >= 2 && IsLessThanOrEqualAll(Abs(unique[0] - prev), VecType(threshold)))
	{
		count--;
	}

	return count;
}

// slow O(N^2) algorithm, ok for small point sets or in debug code
template <typename VecType> inline int GetUniquePoints(VecType* unique, const VecType* points, int numPoints, ScalarV_In threshold = ScalarV(V_ZERO))
{
	int count = 0;

	for (int i = 0; i < numPoints; i++)
	{
		const VecType point = points[i];
		bool bUnique = true;

		for (int j = 0; j < count; j++)
		{
			if (IsLessThanOrEqualAll(Abs(unique[j] - point), VecType(threshold)))
			{
				bUnique = false;
				break;
			}
		}

		if (bUnique)
		{
			unique[count++] = point;
		}
	}

	return count;
}

class spdkDOP2D_4
{
public:
	inline void SetIdentity()
	{
		m_axis = Vec4V(V_ONE);
	}

	inline void SetEmpty()
	{
		m_axis = Vec4V(V_NEGINF);
	}

	inline void SetRect(Vec2V_In minV, Vec2V_In maxV)
	{
		m_axis = Vec4V(maxV, -minV);
	}

	inline void SetPoint(Vec2V_In point)
	{
		m_axis = Vec4V(point, -point);
	}

	inline void AddPoint(Vec2V_In point)
	{
		spdkDOP2D_4 kDOP;
		kDOP.SetPoint(point);
		Grow(kDOP);
	}

	inline void Grow(const spdkDOP2D_4& kDOP)
	{
		m_axis = Max(m_axis, kDOP.m_axis);
	}

	inline void Clip(const spdkDOP2D_4& kDOP)
	{
		m_axis = Min(m_axis, kDOP.m_axis);
	}

	inline Vec2V_Out GetMin() const
	{
		return -m_axis.GetZW();
	}

	inline Vec2V_Out GetMax() const
	{
		return +m_axis.GetXY();
	}

	inline void AddToAxis(Vec4V_In daxis)
	{
		m_axis += daxis;
	}

	inline FASTRETURNCHECK(bool) Intersects(const spdkDOP2D_4& kDOP) const
	{
		return IsGreaterThanAll(m_axis, -kDOP.m_axis.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>()) != 0;
	}

	inline FASTRETURNCHECK(bool) Contains(const spdkDOP2D_4& kDOP) const
	{
		return IsGreaterThanOrEqualAll(m_axis, kDOP.m_axis) != 0;
	}

	inline FASTRETURNCHECK(bool) ContainsPoint(Vec2V_In point) const
	{
		spdkDOP2D_4 kDOP;
		kDOP.SetPoint(point);

		return Contains(kDOP);
	}

	FASTRETURNCHECK(bool) GetPoints(Vec2V points[4]) const
	{
		if (IsEqualAll(m_axis, Vec4V(V_NEGINF)))
		{
			return false;
		}

		const ScalarV px = m_axis.GetX();
		const ScalarV py = m_axis.GetY();
		const ScalarV nx = m_axis.GetZ();
		const ScalarV ny = m_axis.GetW();

		points[0] = Vec2V(-nx, -ny);
		points[1] = Vec2V(+px, -ny);
		points[2] = Vec2V(+px, +py);
		points[3] = Vec2V(-nx, +py);

		return true;
	}

private:
	// minV = -m_axis.zw
	// maxV = +m_axis.xy

	Vec4V m_axis; // {px,py,nx,ny}
};

class spdkDOP2D_8
{
public:
	inline void SetIdentity()
	{
		m_axis = Vec4V(V_ONE);
		m_diag = Vec4V(V_TWO);
	}

	inline void SetEmpty()
	{
		m_axis = Vec4V(V_NEGINF);
		m_diag = Vec4V(V_NEGINF);
	}

	inline void SetRect(Vec2V_In minV, Vec2V_In maxV)
	{
		const Vec4V pnpn = MergeXY(Vec4V(V_ONE), Vec4V(V_NEGONE)); // {-1,+1,-1,+1}
		const Vec4V ppnn = Vec4V  (Vec2V(V_ONE), Vec2V(V_NEGONE)); // {+1,+1,-1,-1}

		const Vec4V x = pnpn*GetFromTwo<Vec::X2,Vec::X1,Vec::X2,Vec::X1>(Vec4V(minV.GetIntrin128()), Vec4V(maxV.GetIntrin128())); // {px,nx,px,nx}
		const Vec4V y = ppnn*GetFromTwo<Vec::Y2,Vec::Y2,Vec::Y1,Vec::Y1>(Vec4V(minV.GetIntrin128()), Vec4V(maxV.GetIntrin128())); // {py,py,ny,ny}

		const Vec4V axis = Vec4V(maxV, -minV);
		const Vec4V diag = x + y;

		m_axis = axis;
		m_diag = diag;
	}

	inline void SetPoint(Vec2V_In point)
	{
		const Vec4V pnpn = MergeXY(Vec4V(V_ONE), Vec4V(V_NEGONE)); // {-1,+1,-1,+1}
		const Vec4V ppnn = Vec4V  (Vec2V(V_ONE), Vec2V(V_NEGONE)); // {+1,+1,-1,-1}

		const Vec4V x = pnpn*point.GetX();
		const Vec4V y = ppnn*point.GetY();

		const Vec4V axis = Vec4V(point, -point);
		const Vec4V diag = x + y;

		m_axis = axis;
		m_diag = diag;
	}

	inline void AddPoint(Vec2V_In point)
	{
		spdkDOP2D_8 kDOP;
		kDOP.SetPoint(point);
		Grow(kDOP);
	}

	inline void Grow(const spdkDOP2D_8& kDOP)
	{
		m_axis = Max(m_axis, kDOP.m_axis);
		m_diag = Max(m_diag, kDOP.m_diag);
	}

	inline void Clip(const spdkDOP2D_8& kDOP)
	{
		m_axis = Min(m_axis, kDOP.m_axis);
		m_diag = Min(m_diag, kDOP.m_diag);
	}

	inline Vec2V_Out GetMin() const
	{
		return -m_axis.GetZW();
	}

	inline Vec2V_Out GetMax() const
	{
		return +m_axis.GetXY();
	}

	inline void AddToAxis(Vec4V_In daxis)
	{
		m_axis += daxis;
	}

	inline void AddToDiag(Vec4V_In ddiag)
	{
		m_diag += ddiag;
	}

	inline FASTRETURNCHECK(bool) Intersects(const spdkDOP2D_8& kDOP) const
	{
		const VecBoolV axis = IsGreaterThan(m_axis, -kDOP.m_axis.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>());
		const VecBoolV diag = IsGreaterThan(m_diag, -kDOP.m_diag.Get<Vec::W,Vec::Z,Vec::Y,Vec::X>());

		return IsTrueAll(And(axis, diag)); 
	}

	inline FASTRETURNCHECK(bool) Contains(const spdkDOP2D_8& kDOP) const
	{
		const VecBoolV axis = IsGreaterThanOrEqual(m_axis, kDOP.m_axis);
		const VecBoolV diag = IsGreaterThanOrEqual(m_diag, kDOP.m_diag);

		return IsTrueAll(And(axis, diag)); 
	}

	inline FASTRETURNCHECK(bool) ContainsPoint(Vec2V_In point) const
	{
		spdkDOP2D_8 kDOP;
		kDOP.SetPoint(point);

		return Contains(kDOP);
	}

	FASTRETURNCHECK(bool) GetPoints(Vec2V points[8]) const
	{
		if (IsEqualAll(m_axis, Vec4V(V_NEGINF)))
		{
			return false;
		}

		const ScalarV px = m_axis.GetX();
		const ScalarV py = m_axis.GetY();
		const ScalarV nx = m_axis.GetZ();
		const ScalarV ny = m_axis.GetW();

		const ScalarV pxpy = m_diag.GetX();
		const ScalarV nxpy = m_diag.GetY();
		const ScalarV pxny = m_diag.GetZ();
		const ScalarV nxny = m_diag.GetW();

		points[0] = Vec2V(+ny - nxny, -ny       );
		points[1] = Vec2V(-ny + pxny, -ny       );
		points[2] = Vec2V(+px       , +px - pxny);
		points[3] = Vec2V(+px       , -px + pxpy);
		points[4] = Vec2V(-py + pxpy, +py       );
		points[5] = Vec2V(+py - nxpy, +py       );
		points[6] = Vec2V(-nx       , -nx + nxpy);
		points[7] = Vec2V(-nx       , +nx - nxny);

		return true;
	}

	FASTRETURNCHECK(int) GetUniquePoints(Vec2V unique[8], ScalarV_In threshold = ScalarV(V_ZERO)) const
	{
		Vec2V temp[8];

		if (GetPoints(temp))
		{
			return GetUniquePointsInLoop<Vec2V,8>(unique, temp, threshold);
		}
		else
		{
			return 0;
		}
	}

private:
	Vec4V m_axis; // {px,py,nx,ny}
	Vec4V m_diag; // {pxpy,nxpy,pxny,nxny}
};

class spdkDOP2D_16
{
public:
	inline void SetIdentity()
	{
		m_axis    = Vec4V(V_ONE);
		m_diag_11 = Vec4V(V_TWO);
		m_diag_12 = Vec4V(V_THREE);
		m_diag_21 = Vec4V(V_THREE);
	}

	inline void SetEmpty()
	{
		m_axis    = Vec4V(V_NEGINF);
		m_diag_11 = Vec4V(V_NEGINF);
		m_diag_12 = Vec4V(V_NEGINF);
		m_diag_21 = Vec4V(V_NEGINF);
	}

	inline void SetRect(Vec2V_In minV, Vec2V_In maxV)
	{
		const Vec4V pnpn = MergeXY(Vec4V(V_ONE), Vec4V(V_NEGONE)); // {-1,+1,-1,+1}
		const Vec4V ppnn = Vec4V  (Vec2V(V_ONE), Vec2V(V_NEGONE)); // {+1,+1,-1,-1}

		const Vec4V x = pnpn*GetFromTwo<Vec::X2,Vec::X1,Vec::X2,Vec::X1>(Vec4V(minV.GetIntrin128()), Vec4V(maxV.GetIntrin128())); // {px,nx,px,nx}
		const Vec4V y = ppnn*GetFromTwo<Vec::Y2,Vec::Y2,Vec::Y1,Vec::Y1>(Vec4V(minV.GetIntrin128()), Vec4V(maxV.GetIntrin128())); // {py,py,ny,ny}

		const Vec4V axis = Vec4V(maxV, -minV);
		const Vec4V diag = x + y;

		m_axis    = axis;
		m_diag_11 = diag;
		m_diag_12 = diag + y;
		m_diag_21 = diag + x;
	}

	inline void SetPoint(Vec2V_In point)
	{
		const Vec4V pnpn = MergeXY(Vec4V(V_ONE), Vec4V(V_NEGONE)); // {-1,+1,-1,+1}
		const Vec4V ppnn = Vec4V  (Vec2V(V_ONE), Vec2V(V_NEGONE)); // {+1,+1,-1,-1}

		const Vec4V x = pnpn*point.GetX();
		const Vec4V y = ppnn*point.GetY();

		const Vec4V axis = Vec4V(point, -point);
		const Vec4V diag = x + y;

		m_axis    = axis;
		m_diag_11 = diag;
		m_diag_12 = diag + y;
		m_diag_21 = diag + x;
	}

	inline void AddPoint(Vec2V_In point)
	{
		spdkDOP2D_16 kDOP;
		kDOP.SetPoint(point);
		Grow(kDOP);
	}

	inline void Grow(const spdkDOP2D_16& kDOP)
	{
		m_axis    = Max(m_axis   , kDOP.m_axis   );
		m_diag_11 = Max(m_diag_11, kDOP.m_diag_11);
		m_diag_12 = Max(m_diag_12, kDOP.m_diag_12);
		m_diag_21 = Max(m_diag_21, kDOP.m_diag_21);
	}

	inline void Clip(const spdkDOP2D_16& kDOP)
	{
		m_axis    = Min(m_axis   , kDOP.m_axis   );
		m_diag_11 = Min(m_diag_11, kDOP.m_diag_11);
		m_diag_12 = Min(m_diag_12, kDOP.m_diag_12);
		m_diag_21 = Min(m_diag_21, kDOP.m_diag_21);
	}

	inline Vec2V_Out GetMin() const
	{
		return -m_axis.GetZW();
	}

	inline Vec2V_Out GetMax() const
	{
		return +m_axis.GetXY();
	}

	inline void AddToAxis(Vec4V_In daxis)
	{
		m_axis += daxis;
	}

	inline void AddToDiag_11(Vec4V_In ddiag)
	{
		m_diag_11 += ddiag;
	}

	inline void AddToDiag_12(Vec4V_In ddiag)
	{
		m_diag_12 += ddiag;
	}

	inline void AddToDiag_21(Vec4V_In ddiag)
	{
		m_diag_21 += ddiag;
	}

	inline FASTRETURNCHECK(bool) Intersects(const spdkDOP2D_16& kDOP) const
	{
		const VecBoolV axis    = IsGreaterThan(m_axis   , -kDOP.m_axis   .Get<Vec::Z,Vec::W,Vec::X,Vec::Y>());
		const VecBoolV diag_11 = IsGreaterThan(m_diag_11, -kDOP.m_diag_11.Get<Vec::W,Vec::Z,Vec::Y,Vec::X>());
		const VecBoolV diag_12 = IsGreaterThan(m_diag_12, -kDOP.m_diag_12.Get<Vec::W,Vec::Z,Vec::Y,Vec::X>());
		const VecBoolV diag_21 = IsGreaterThan(m_diag_21, -kDOP.m_diag_21.Get<Vec::W,Vec::Z,Vec::Y,Vec::X>());

		return IsTrueAll(And(And(axis, diag_11), And(diag_12, diag_21))); 
	}

	inline FASTRETURNCHECK(bool) Contains(const spdkDOP2D_16& kDOP) const
	{
		const VecBoolV axis    = IsGreaterThanOrEqual(m_axis   , kDOP.m_axis   );
		const VecBoolV diag_11 = IsGreaterThanOrEqual(m_diag_11, kDOP.m_diag_11);
		const VecBoolV diag_12 = IsGreaterThanOrEqual(m_diag_12, kDOP.m_diag_12);
		const VecBoolV diag_21 = IsGreaterThanOrEqual(m_diag_21, kDOP.m_diag_21);

		return IsTrueAll(And(And(axis, diag_11), And(diag_12, diag_21))); 
	}

	inline FASTRETURNCHECK(bool) ContainsPoint(Vec2V_In point) const
	{
		spdkDOP2D_16 kDOP;
		kDOP.SetPoint(point);
		return Contains(kDOP);
	}

	//FASTRETURNCHECK(bool) GetPoints(Vec2V points[16]) const
	//{
	//}

private:
	Vec4V m_axis; // {px,py,nx,ny}
	Vec4V m_diag_11; // {pxpy,nxpy,pxny,nxny}
	Vec4V m_diag_12; // {px2py,nx2py,px2ny,nx2ny}
	Vec4V m_diag_21; // {2pxpy,2nxpy,2pxny,2nxny}
};

} // namespace rage

#endif // SPATIALDATA_KDOP2D_H
