// 
// spatialdata/spatialdata.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPATIALDATA_SPATIALDATA_H
#define SPATIALDATA_SPATIALDATA_H

#if 0 // too clever, apparently ..
	#define spdBool_Out u32
	#define spdBool_Return(x) x
#else
	#define spdBool_Out bool
	#define spdBool_Return(x) ((x) != 0)
#endif

#include "data/struct.h"
#include "parser/macros.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"
#include "grcore/cullstatus.h"

namespace rage {

} // namespace rage

#endif // SPATIALDATA_SPATIALDATA_H
