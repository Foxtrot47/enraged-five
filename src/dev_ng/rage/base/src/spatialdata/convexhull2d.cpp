// 
// spatialdata/convexhull2d.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "convexhull2d.h"

#include "aabb.h"
#include "system/alloca.h"
#include "vector/geometry.h"
#include "vector/vector2.h"

#if __XENON
//	#include "xbox.h"
#endif // __XENON

using namespace rage;


spdConvexHull2D::spdConvexHull2D()
	: m_VertexList(NULL)
	, m_ExternalBuffer(false)
{
}

spdConvexHull2D::~spdConvexHull2D()
{
	Release();
}

void spdConvexHull2D::Release()
{
	if (m_VertexList && !m_ExternalBuffer)
	{
		delete[] m_VertexList;
	}

	m_VertexList = NULL;
	m_ExternalBuffer = false;
}

void spdConvexHull2D::Init(int Vertices, const Vector2 *RESTRICT vertexArray, Vector2 *externalBuffer, int externalBufferCount)
{
	Assert(vertexArray);
	Assert(Vertices > 2);	// A convex hull needs at least three vertices

	Release();

	int *convexShapeIndices = Alloca(int, Vertices);

	/*
		Test case that used to cause trouble

		Vector2 *va = (Vector2 *) vertexArray;

	*(va++) = Vector2(2040.04797, 3458.45947);
	*(va++) = Vector2(-2887.31519, -2795.84131);
	*(va++) = Vector2(-4666.45898, -1394.16748);
	*(va++) = Vector2(260.903931, 4860.13330);
	*(va++) = Vector2(4668.44336, 1387.71484);
	*(va++) = Vector2(-258.919800, -4866.58594);
	*(va++) = Vector2(-2038.06384, -3464.91211);
	*(va++) = Vector2(2889.29956, 2789.38867);
*/

	// Find the bottom point. In case of a tie, take the rightmost.
	float highestValue = 999999999.0f;
	float bestX = 0.0f;

	for (int x=0; x<Vertices; x++)
	{
		// Is this one better than our current best one?
		if (vertexArray[x].y < highestValue)
		{
			// It is - take it.
			highestValue = vertexArray[x].y;
			bestX = vertexArray[x].x;
			convexShapeIndices[0] = x;
		}
		else if (vertexArray[x].y == highestValue)
		{
			// This is a tie - is this one further right?
			if (vertexArray[x].x > bestX)
			{
				// It is, so it's the new winner.
				bestX = vertexArray[x].x;
				convexShapeIndices[0] = x;
			}
		}
	}

	// Now initialize our list of vertices in the order in which we got them...
	for (int x=1; x<Vertices; x++)
	{
		convexShapeIndices[x] = x;
	}

	// Swap 0 with the one we determined to be 0
	convexShapeIndices[ convexShapeIndices[0] ] = 0;

	// Sort the others in ccw order
	// TODO: We might consider heap sort instead of bubble sort here.
	for (int x=1; x<Vertices; x++)
	{
		int point0 = convexShapeIndices[0];

		bool swapped = false;

		const Vector2 *pt0 = &vertexArray[point0];

		for (int y=1; y<Vertices-x; y++)
		{
			int point1 = convexShapeIndices[y];
			int point2 = convexShapeIndices[y+1];
			const Vector2 *pt1 = &vertexArray[point1];
			const Vector2 *pt2 = &vertexArray[point2];

			// This is an inexcusably lame patch to fix a certain condition (colinear
			// vectors) that creates incorrect results. This will be cleaned up and
			// optimized shortly.
			Vector2 normPt1(*pt1 - *pt0);
			Vector2 normPt2(*pt2 - *pt0);

			normPt1.Normalize();
			normPt2.Normalize();

			float angle = normPt1.Dot(normPt2);

			float sideCalc = pt2->ComputeSide(*pt0, *pt1);

			if (fabs(sideCalc) < 0.00001f || angle > 0.9999f)
			{
				// These points are on the same line. We can ignore the one that is closer to the point.
				float dist1 = pt0->Dist2(*pt1);
				float dist2 = pt0->Dist2(*pt2);

				//int startPoint = (dst1 < dst2) ? 
				if (dist1 < dist2)
				{
					// Point1 is closer and has to go.
					for (int z=y; z<Vertices-1; z++)
					{
						convexShapeIndices[z] = convexShapeIndices[z+1];
					}
				}
				else
				{
					// Point2 is closer.
					for (int z=y+1; z<Vertices-1; z++)
					{
						convexShapeIndices[z] = convexShapeIndices[z+1];
					}
				}

				Vertices--;

				// Restart.
				y = 0;
				continue;
			}

			// Is y further to the right than x?
			if (sideCalc < 0)
			{
				// It is. Swap 'em.
				convexShapeIndices[y+1] = point1;
				convexShapeIndices[y] = point2;
				swapped = true;
			}
		}

		if (!swapped)
		{
			break;
		}
	}

	// Next step: Create the shape.
	if (externalBuffer)
	{
		m_VertexList = externalBuffer;
		m_Capacity = externalBufferCount;
		m_ExternalBuffer = true;
	}
	else
	{
		m_VertexList = rage_new Vector2[Vertices];
		m_Capacity = Vertices;
		m_ExternalBuffer = false;
	}

	Vector2 *vtxListPtr = m_VertexList;

	vtxListPtr[0] = vertexArray[ convexShapeIndices[0] ];
	vtxListPtr[1] = vertexArray[ convexShapeIndices[1] ];


	int top = 2;

	// Graham Scan at work: Go through all the points (which are now ordered counter-clockwise), push them
	// onto our stack of vertices - if they cover a previous vertex, pop that one off the stack.
	int nextIndex = 2;

	while (nextIndex < Vertices)
	{
		const Vector2 &point0 = vtxListPtr[top-2];
		const Vector2 &point1 = vtxListPtr[top-1];
		int point2 = convexShapeIndices[nextIndex];

		if (vertexArray[point2].ComputeSide(point0, point1) < 0)
		{
			top--;

			// We HAVE to push the next vertex on the stack to prevent reading
			// invalid memory.
			if (top < 2)
			{
				vtxListPtr[top++] = vertexArray[ convexShapeIndices[nextIndex++] ];
			}
		}
		else
		{
			vtxListPtr[top++] = vertexArray[ convexShapeIndices[nextIndex++] ];
		}
	}

	Assert(top > 2);

	// Get the final vertex count.
	m_Vertices = top;

	// Get the boundaries.
	m_Min = m_VertexList[0];
	m_Max = m_VertexList[0];

	for (int x=1; x<m_Vertices; x++)
	{
		m_Min.x = Min(m_Min.x, m_VertexList[x].x);
		m_Min.y = Min(m_Min.y, m_VertexList[x].y);
		m_Max.x = Max(m_Max.x, m_VertexList[x].x);
		m_Max.y = Max(m_Max.y, m_VertexList[x].y);
	}
}

void spdConvexHull2D::CreateAsUnion(const spdConvexHull2D &hull1, const spdConvexHull2D &hull2, Vector2 *externalBuffer, int externalBufferCount)
{
	// Create a temporary array with both hulls' vertices in it.
	int unionSize = hull1.GetVertexCount() + hull2.GetVertexCount();
	Vector2 *unionArray = Alloca(Vector2, unionSize);

	// Populate the 
	Vector2 *vtxPtr = unionArray;

//#if __XENON
//	XMemCpy(vtxPtr, hull1.GetVertexList(), sizeof(Vector2) * hull1.GetVertexCount());
//	XMemCpy(vtxPtr + hull1.GetVertexCount(), hull2.GetVertexList(), sizeof(Vector2) * hull2.GetVertexCount());
//#else // __XENON
	memcpy(vtxPtr, hull1.GetVertexList(), sizeof(Vector2) * hull1.GetVertexCount());
	memcpy(vtxPtr + hull1.GetVertexCount(), hull2.GetVertexList(), sizeof(Vector2) * hull2.GetVertexCount());
//#endif // !__XENON

	Init(unionSize, unionArray, externalBuffer, externalBufferCount);
}

void spdConvexHull2D::CreateFromAABBXZ(const spdAABB &aabb, Vector2 *externalBuffer, int externalBufferCount)
{
	Release();

	if (externalBuffer)
	{
		Assert(externalBufferCount >= 4);

		m_Capacity = externalBufferCount;
		m_ExternalBuffer = true;
		m_VertexList = externalBuffer;
	}
	else
	{
		m_VertexList = rage_new Vector2[4];
	}

	m_Vertices = 4;

//	m_VertexList[0] = Vector2(aabb.GetMinVector3().x, aabb.GetMaxVector3().z);
//	m_VertexList[1] = Vector2(aabb.GetMaxVector3().x, aabb.GetMaxVector3().z);
//	m_VertexList[2] = Vector2(aabb.GetMaxVector3().x, aabb.GetMinVector3().z);
//	m_VertexList[3] = Vector2(aabb.GetMinVector3().x, aabb.GetMinVector3().z);

	m_VertexList[3] = Vector2(aabb.GetMinVector3().x, aabb.GetMaxVector3().z);
	m_VertexList[2] = Vector2(aabb.GetMaxVector3().x, aabb.GetMaxVector3().z);
	m_VertexList[1] = Vector2(aabb.GetMaxVector3().x, aabb.GetMinVector3().z);
	m_VertexList[0] = Vector2(aabb.GetMinVector3().x, aabb.GetMinVector3().z);
}

bool spdConvexHull2D::IsInside(const Vector2 &point) const
{
	int vertices = m_Vertices - 1;

	// The point has to be on the left side of all sides.
	for (int x=0; x<vertices; x++)
	{
		if (point.ComputeSide(m_VertexList[x], m_VertexList[x+1]) < 0.0f)
		{
			// Right side - this point is out.
			return false;
		}
	}

	// Don't forget the last line from (vertices-1) to 0
	return (point.ComputeSide(m_VertexList[vertices], m_VertexList[0]) >= 0.0f);
}

bool spdConvexHull2D::Intersects(const Vector2 &p1, const Vector2 &p2) const
{
	int vertices = m_Vertices - 1;
	float dummy1, dummy2;

	for (int x=0; x<vertices; x++)
	{
		if (geom2D::Test2DSegVsSeg(dummy1, dummy2, p1, p2, m_VertexList[x], m_VertexList[x+1]))
		{
			return true;
		}
	}

	return (geom2D::Test2DSegVsSeg(dummy1, dummy2, p1, p2, m_VertexList[vertices], m_VertexList[0]));
}

grcCullStatus spdConvexHull2D::Contains(const spdAABB &box) const
{
	// Quick reject: Are we completely outside the bounds?
	if (box.GetMaxVector3().z < m_Min.y || box.GetMinVector3().z > m_Max.y ||
		box.GetMaxVector3().x < m_Min.x || box.GetMinVector3().x > m_Max.x)
	{
		return cullOutside;
	}

	int insideCount = 0;

	Vector2 minCorner = Vector2(box.GetMinVector3().x, box.GetMinVector3().z);
	Vector2 maxCorner = Vector2(box.GetMaxVector3().x, box.GetMaxVector3().z);
	Vector2 topRightCorner = Vector2(maxCorner.x, minCorner.y);
	Vector2 bottomLeftCorner = Vector2(minCorner.x, maxCorner.y);

	insideCount += IsInside(minCorner);
	insideCount += IsInside(maxCorner);
	insideCount += IsInside(topRightCorner);
	insideCount += IsInside(bottomLeftCorner);

	if (insideCount == 4)
	{
		// ALL vertices are inside. We're enclosing it.
		return cullInside;
	}

	if (insideCount > 0)
	{
		// At least one of them is inside. We're at least clipping it.
		return cullClipped;
	}
	
	insideCount = 0;
	int verts = GetVertexCount();

	for (int x=0; x<verts; x++)
	{
		const Vector2 v = GetVertexList()[x];
		if (box.ContainsPointFlat(Vec2V(v.x, v.y)))
		{
			return cullClipped;
		}
	}

	return cullInside;
}

grcCullStatus spdConvexHull2D::Contains(const spdConvexHull2D &hull) const
{
	Assert(hull.GetVertexList());

	// How many of the other hull's vertices are inside this one?
	int insideCount = 0;
	int otherVerts = hull.GetVertexCount();

	for (int x=0; x<otherVerts; x++)
	{
		if (IsInside(hull.GetVertexList()[x]))
		{
			insideCount++;
		}
	}

	if (insideCount == otherVerts)
	{
		// ALL vertices are inside. We're enclosing it.
		return cullInside;
	}

	if (insideCount > 0)
	{
		// At least one of them is inside. We're at least clipping it.
		return cullClipped;
	}

	// We could be completely inside the other hull. Let's check.
	insideCount = 0;
	int verts = GetVertexCount();

	for (int x=0; x<verts; x++)
	{
		if (hull.IsInside(GetVertexList()[x]))
		{
			return cullClipped;
		}
	}

	// Do any lines intersect?
	for (int x=0; x<verts-1; x++)
	{
        if (hull.Intersects(GetVertexList()[x], GetVertexList()[x+1]))
		{
			return cullClipped;
		}
	}

	// Don't forget the last line.
    if (hull.Intersects(GetVertexList()[verts-1], GetVertexList()[0]))
	{
		return cullClipped;
	}

	// Finally, check to see if any of the sides intersect.
	return cullOutside;
}
