// 
// spatialdata/plane.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "spatialdata/plane.h"

#include "vector/geometry.h"

using namespace rage;

IMPLEMENT_PLACE(spdPlane);

// Returns: 0=no intersection, 1=intersection, 2=coplanar.
int spdPlane::IntersectLine(const Vector3 &p1, const Vector3 &p2, Vector3 &outPoint, float *outT) const
{
	Vector3 se = p2 - p1;
	Vector3 sp = m_PlaneCoeffs.GetVector3()*m_PlaneCoeffs.w - p1;

	float nse = GetNormal().Dot(se);

	if (nse == 0.0f)
	{
		// Line and plane are parallel.
		outPoint = p1;
		if (outT)
			*outT = 0.0f;
		return 2;
	}

	float t = GetNormal().Dot(sp);
	t /= nse;

	outPoint = p1 + (se * t);

	if (outT)
		*outT = t;

	// Prevents some spew.
	//	return (t >= 0.0f && t <= 1.0f) ? 1 : 0;
	return (t >= -0.005f && t <= 1.005f) ? 1 : 0;
}


BoolV_Out spdPlane::IntersectLineV(Vec3V_In start, Vec3V_In end, Vec3V_InOut outPoint) const
{
	ScalarV t;
	Vec3V dir = end-start;
	BoolV resCol = geomSegments::CollideRayPlane(start, dir, VECTOR4_TO_VEC4V(m_PlaneCoeffs), t);
	
	// expected results F_F_F_F or T_T_T_T
	BoolV tLessThanZeroV = IsGreaterThanOrEqual(t, ScalarV(V_ZERO));
	BoolV tGreaterThanOneV = IsLessThanOrEqual(t, ScalarV(V_ONE));
	BoolV resT = And(tLessThanZeroV,tGreaterThanOneV);
	
	outPoint = SelectFT( resT, start, Add(start, Scale(dir,t)) );
	
	return And(resCol,resT);
}

BoolV_Out spdPlane::IntersectRayV(Vec3V_In origin, Vec3V_In dir, Vec3V_InOut outPoint) const
{
	ScalarV t;
	BoolV res = geomSegments::CollideRayPlaneNoBackfaceCullingV(origin, dir, VECTOR4_TO_VEC4V(m_PlaneCoeffs), t);
	
	// expected results F_F_F_F or T_T_T_T
	BoolV tLessThanZeroV = IsGreaterThanOrEqual(t, ScalarV(V_ZERO));

	outPoint = SelectFT( tLessThanZeroV, origin, Add(origin, Scale(dir,t)) );
	return (res & tLessThanZeroV);
}

/*
PURPOSE
	A generic function to clip a polygon against a plane
	using the Sutherland-Hodgman algorithm.
PARAMS
	pIn		- array of points describing the input polygon.
	pOut	- array of points that will describe the output polygon.
	plane	- the plane to clip against.
RETURNS
	The number of points in the output polygon.
	pOut describes the output polygon.
NOTES
	This function will assert out if reallocation occurs while adding points to pOut,
	but you can guard against that by pre-allocating pOut in some way.
*/
int spdPlane::Clip(const atArray<Vector3> *pIn, atArray<Vector3> *pOut) const
{
	FastAssert(pIn && pOut && (pIn != pOut));

	int result = Clip(pIn->GetElements(), pIn->GetCount(), pOut->GetElements(), pOut->GetCapacity());
	pOut->Resize(result);

	return result;
}

int spdPlane::Clip(const Vector3 *pIn, int inCount, Vector3 *pOut, int ASSERT_ONLY(outBufferSize)) const
{
	int internalOutSize = 0;
	//	PF_START(PolygonClip);

	FastAssert(pIn && pOut && (pIn != pOut));

	Vector3 s = pIn[inCount - 1];
	Vector3 i;

	for (int j = 0; j < inCount; j++)
	{
		const Vector3 &p = pIn[j];

		if (DistanceToPlane(s) < 0.0f)
		{
			if (DistanceToPlane(s) < 0.0f)
			{
				FastAssert(internalOutSize < outBufferSize);
				pOut[internalOutSize++] = p;
			}
			else
			{
				float t;
				if (!IntersectLine(s, p, i, &t))
					Warningf("Clip: no intersection, t=%f", t);

				FastAssert(internalOutSize < outBufferSize - 1);
				pOut[internalOutSize++] = i;
				pOut[internalOutSize++] = p;
			}
		}
		else
			if (DistanceToPlane(s) < 0.0f)
			{
				float t;
				if (!IntersectLine(s, p, i, &t))
					Warningf("Clip: no intersection, t=%f", t);
				FastAssert(internalOutSize < outBufferSize);
				pOut[internalOutSize++] = i;
			}

			s = p;
	}

	//	PF_STOP(PolygonClip);

	//Assert(pOut->GetCapacity() == alloc && "Clip() reallocated");

	// After we're done, we need to check for coincidents.
	// Although it is very rare, it is possible that this function adds the
	// same vert twice, which will later end up crashing the system.
	// TODO: This could be made more efficient if we knew exactly WHERE those
	// double-verts happen and only checked in those places. This is
	// a brute-force check, but hell, it works.
	
	for (int x=1; x<internalOutSize; x++)
	{
		for (int y=0; y<x; y++)
		{
			if (pOut[x].Dist2(pOut[y]) < 0.0000001f)
			{
				pOut[y] = pOut[--internalOutSize];
				break;
			}
		}
	}

	return internalOutSize;
}

#if __DECLARESTRUCT
	void spdPlane::DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(spdPlane);
		STRUCT_FIELD(m_PlaneCoeffs);
		STRUCT_END();
	}
#endif // __DECLARESTRUCT
