//
// spatialdata/aabb.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SPATIALDATA_AABB_H
#define SPATIALDATA_AABB_H

#include "spatialdata/spatialdata.h"
#include "spatialdata/sphere.h"

namespace rage {

class datTypeStruct;
class fiAsciiTokenizer;
class parTreeNode;

class spdRect;

/*
PURPOSE
A class for storing an axis-aligned bounding box.
This class internally uses two Vector4s, so it has two unused .w components that may be
used by the application using functions like SetUserFloat1 and SetUserFloat2. Note that
the class makes no attempt to preserve those values, so any function modifying the
bounding box (growing, setting, merging, etc) will leave the user floats in an undefined
state.
*/

class spdAABB
{
public:
	inline spdAABB() : m_min(V_ZERO), m_max(V_ZERO) {}
	inline spdAABB(class datResource&) {}
	inline spdAABB(const spdAABB& b) : m_min(b.m_min), m_max(b.m_max) {}
	inline const spdAABB& operator =(const spdAABB& b) { m_min = b.m_min; m_max = b.m_max; return *this; }
	inline bool operator ==(const spdAABB& rhs) const { return (IsEqualAll(GetMin(), rhs.GetMin()) & IsEqualAll(GetMax(), rhs.GetMax())) != 0; }
	inline bool operator !=(const spdAABB& rhs) const { return (IsEqualAll(GetMin(), rhs.GetMin()) & IsEqualAll(GetMax(), rhs.GetMax())) == 0; }

	// explicit c'tors
	explicit inline spdAABB(Vec3V_In boxMin, Vec3V_In boxMax) : m_min(boxMin), m_max(boxMax) {}
	explicit inline spdAABB(const spdSphere& sphere) { SetAsSphereV4(sphere.GetV4()); }
	explicit inline spdAABB(const spdRect& rect, ScalarV_In zmin, ScalarV_In zmax);
	explicit inline spdAABB(const float* pointData, int numPoints, int strideInBytes); // NOTE -- used in grmModel::Load, may want to optimise this

	inline void Invalidate() { m_min = Vec4V(V_FLT_MAX); m_max = -m_min; }
	inline bool IsValid() const { return IsLessThanOrEqualAll(GetMin(), GetMax()) != 0; }

	// set
	inline void Set(Vec3V_In boxMin, Vec3V_In boxMax) { m_min = Vec4V(boxMin); m_max = Vec4V(boxMax); }
	inline void SetMin(Vec3V_In boxMin) { m_min = Vec4V(boxMin); }
	inline void SetMax(Vec3V_In boxMax) { m_max = Vec4V(boxMax); }
	inline void SetAsSphereV4(Vec4V_In centerAndRadius);

	// get
	inline Vec3V_Out GetCenter() const { return ((m_max + m_min)*ScalarV(V_HALF)).GetXYZ(); }
	inline Vec2V_Out GetCenterFlat() const { return ((m_max + m_min)*ScalarV(V_HALF)).GetXY(); }
	inline Vec3V_Out GetExtent() const { return ((m_max - m_min)*ScalarV(V_HALF)).GetXYZ(); }
	inline Vec2V_Out GetExtentFlat() const { return ((m_max - m_min)*ScalarV(V_HALF)).GetXY(); }
	inline Vec3V_Out GetMin() const { return m_min.GetXYZ(); }
	inline Vec2V_Out GetMinFlat() const { return m_min.GetXY(); }
	inline Vec3V_Out GetMax() const { return m_max.GetXYZ(); }
	inline Vec2V_Out GetMaxFlat() const { return m_max.GetXY(); }

	// TODO -- these go away soon
	inline const Vector3& GetMinVector3() const { return (const Vector3&)m_min; }
	inline const Vector3& GetMaxVector3() const { return (const Vector3&)m_max; }

	inline spdSphere GetBoundingSphere() const { return spdSphere(GetBoundingSphereV4()); }
	inline Vec4V_Out GetBoundingSphereV4() const { return Vec4V(GetCenter(), Mag(GetExtent())); }
	inline spdSphere GetInscribedSphere() const { return spdSphere(GetInscribedSphereV4()); }
	inline Vec4V_Out GetInscribedSphereV4() const { return Vec4V(GetCenter(), MinElement(GetExtent())); }

	// user data set/get
	inline void SetUserFloat1(ScalarV_In value) { m_min.SetW(value); }
	inline void SetUserFloat2(ScalarV_In value) { m_max.SetW(value); }
	inline void SetUserFloat1f(float value) { m_min.SetWf(value); }
	inline void SetUserFloat2f(float value) { m_max.SetWf(value); }
	inline void SetUserInt1(int value) { m_min.SetWi(value); }
	inline void SetUserInt2(int value) { m_max.SetWi(value); }

	inline ScalarV_Out GetUserFloat1() const { return m_min.GetW(); }
	inline ScalarV_Out GetUserFloat2() const { return m_max.GetW(); }
	inline float       GetUserFloat1f() const { return m_min.GetWf(); }
	inline float       GetUserFloat2f() const { return m_max.GetWf(); }
	inline int         GetUserInt1() const { return m_min.GetWi(); }
	inline int         GetUserInt2() const { return m_max.GetWi(); }

	// set min/max preserving user data
	inline void SetMinPreserveUserData(Vec3V_In boxMin) { m_min = GetFromTwo<Vec::X1,Vec::Y1,Vec::Z1,Vec::W2>(Vec4V(boxMin), m_min); }
	inline void SetMaxPreserveUserData(Vec3V_In boxMax) { m_max = GetFromTwo<Vec::X1,Vec::Y1,Vec::Z1,Vec::W2>(Vec4V(boxMax), m_max); }

	// grow
	inline void GrowPoint(Vec3V_In point) { m_min = Min(Vec4V(point), m_min); m_max = Max(Vec4V(point), m_max); }
	inline void GrowSphere(const spdSphere& sphere) { GrowAABB(spdAABB(sphere)); }
	inline void GrowAABB(const spdAABB& box) { m_min = Min(box.m_min, m_min); m_max = Max(box.m_max, m_max); }
	inline void GrowUniform(ScalarV_In distance) { m_min -= Vec4V(distance); m_max += Vec4V(distance); }

	inline void IntersectWithAABB(const spdAABB& box);

	// transform
	inline void Transform(Mat33V_In mat);
	inline void Transform(Mat34V_In mat);
	inline void TransformProjective(Mat44V_In mat);

	// distance to point
	inline ScalarV_Out DistanceToPoint(Vec3V_In point) const;
	inline ScalarV_Out DistanceToPointFlat(Vec3V_In point) const { return DistanceToPointFlat(point.GetXY()); }
	inline ScalarV_Out DistanceToPointFlat(Vec2V_In point) const;
	inline ScalarV_Out DistanceToPointSquared(Vec3V_In point) const;
	inline ScalarV_Out DistanceToPointSquaredFlat(Vec3V_In point) const { return DistanceToPointSquaredFlat(point.GetXY()); }
	inline ScalarV_Out DistanceToPointSquaredFlat(Vec2V_In point) const;

	// contains
	inline spdBool_Out ContainsPoint(Vec3V_In point) const;
	inline spdBool_Out ContainsPointFlat(Vec3V_In point) const { return ContainsPointFlat(point.GetXY()); }
	inline spdBool_Out ContainsPointFlat(Vec2V_In point) const;
	inline spdBool_Out ContainsSphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainsSphereFlat(const spdSphere& sphere) const;
	inline spdBool_Out ContainsAABB(const spdAABB& box) const;
	inline spdBool_Out ContainsAABBFlat(const spdAABB& box) const;

	inline spdBool_Out ContainedBySphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainedBySphereFlat(const spdSphere& sphere) const;

	// intersects
	inline spdBool_Out IntersectsSphere(const spdSphere& sphere) const;
	inline spdBool_Out IntersectsSphereFlat(const spdSphere& sphere) const;
	inline spdBool_Out IntersectsAABB(const spdAABB& box) const;
	inline spdBool_Out IntersectsAABBFlat(const spdAABB& box) const;

	IMPLEMENT_PLACE_INLINE(spdAABB);
#if !__SPU
	void SaveData(fiAsciiTokenizer& tok) const;
	void LoadData(fiAsciiTokenizer& tok);
	void PreLoad(parTreeNode* node);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif // __DECLARESTRUCT
#endif // !__SPU

protected:
	Vec4V m_min; // w-component is user data
	Vec4V m_max; // w-component is user data

	PAR_SIMPLE_PARSABLE;
};

// ================================================================================================

inline spdAABB TransformAABB(Mat33V_In localToWorld, const spdAABB& localBox);
inline spdAABB TransformAABB(Mat34V_In localToWorld, const spdAABB& localBox);
inline spdAABB TransformAABB_alt(Mat33V_In localToWorld, const spdAABB& localBox);
inline spdAABB TransformAABB_alt(Mat34V_In localToWorld, const spdAABB& localBox);
inline spdAABB UnTransformOrthoAABB(Mat33V_In localToWorld, const spdAABB& worldBox);
inline spdAABB UnTransformOrthoAABB(Mat34V_In localToWorld, const spdAABB& worldBox);
inline spdAABB TransformProjectiveAABB(Mat44V_In mat, const spdAABB& worldBox);

// ================================================================================================

class spdRect
{
public:
	inline spdRect() {}
	inline spdRect(class datResource&) {}
	inline spdRect(const spdRect& b) : m_v(b.m_v) {}
	inline const spdRect& operator =(const spdRect& b) { m_v = b.m_v; return *this; }
	inline bool operator ==(const spdRect& rhs) const { return (IsEqualAll(m_v, rhs.m_v)) != 0; }
	inline bool operator !=(const spdRect& rhs) const { return (IsEqualAll(m_v, rhs.m_v)) == 0; }

	// explicit c'tors
	explicit inline spdRect(Vec2V_In rectMin, Vec2V_In rectMax) : m_v(-rectMin, rectMax) {}
	explicit inline spdRect(const spdSphere& sphere) : m_v(RectPointV4(sphere.GetCenterFlat()) + Vec4V(sphere.GetRadius())) {}
	explicit inline spdRect(const spdAABB& box) : m_v(GetFromTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(-box.GetMin(), box.GetMax())) {}

	inline void Invalidate() { m_v = Vec4V(V_NEG_FLT_MAX); }
	inline bool IsValid() const { return IsLessThanOrEqualAll(GetMin(), GetMax()) != 0; }

	// set
	inline void Set(Vec2V_In rectMin, Vec2V_In rectMax) { m_v = Vec4V(-rectMin, rectMax); }

	// get
	inline Vec2V_Out GetCenter() const { return (GetMax() - m_v.GetXY())*ScalarV(V_HALF); }
	inline Vec2V_Out GetExtent() const { return (GetMax() + m_v.GetXY())*ScalarV(V_HALF); }
	inline Vec2V_Out GetMin() const { return -m_v.GetXY(); }
	inline Vec2V_Out GetMax() const { return  m_v.GetZW(); }

	inline void GetCorners(Vec2V out[4]);

	// grow
	inline void GrowPoint(Vec2V_In point) { m_v = Max(m_v, RectPointV4(point)); }
	inline void GrowRect(const spdRect& rect) { m_v = Max(m_v, rect.m_v); }
	inline void GrowUniform(ScalarV_In distance) { m_v += Vec4V(distance); }

	inline void IntersectWithRect(const spdRect& rect) { m_v = Min(m_v, rect.m_v); }

	// distance to point
	inline ScalarV_Out DistanceToPoint(Vec2V_In point) const;
	inline ScalarV_Out DistanceToPointSquared(Vec2V_In point) const;

	// contains
	inline spdBool_Out ContainsPoint(Vec2V_In point) const { return spdBool_Return(IsLessThanOrEqualAll(RectPointV4(point), m_v)); }
	inline spdBool_Out ContainsRect(const spdRect& rect) const { return spdBool_Return(IsLessThanOrEqualAll(rect.m_v, m_v)); }

	// intersects
	inline spdBool_Out IntersectsRect(const spdRect& rect) const { return spdBool_Return(IsLessThanOrEqualAll(-rect.m_v.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>(), m_v)); }

protected:
#if 1
	static __forceinline Vec4V_Out RectPointV4(Vec2V_In point) { return Vec4V(-point, point); }
#else
	static __forceinline Vec4V_Out RectPointV4(Vec2V_In point) { return Vec4V(V4Permute<Vec::X,Vec::Y,Vec::X,Vec::Y>(point.GetIntrin128())) ^ Vec4VConstant<0x80000000,0x80000000,0,0>(); }
#endif

	Vec4V m_v; // {-xmin,-ymin,+xmax,+ymax}
};

// ================================================================================================

class spdOrientedBB
{
public:
	inline spdOrientedBB() : m_localBox(Vec3V(V_ZERO), Vec3V(V_ZERO)), m_localToWorld(Mat33V(V_ZERO)) {}
	inline spdOrientedBB(class datResource&) {}
	inline spdOrientedBB(const spdOrientedBB& b) : m_localBox(b.m_localBox), m_localToWorld(b.m_localToWorld) {}
	inline const spdOrientedBB& operator =(const spdOrientedBB& b) { m_localBox = b.m_localBox; m_localToWorld = b.m_localToWorld; return *this; }

	inline spdOrientedBB(const spdAABB& localBox, Mat33V_In localToWorld) : m_localBox(localBox), m_localToWorld(localToWorld) {}
	inline spdOrientedBB(const spdAABB& localBox, Mat34V_In localToWorld);
	
	inline void Set(const spdAABB& localBox, Mat34V_In localToWorld);

	inline spdAABB GetWorldAABB() const;

	// contains
	inline spdBool_Out ContainsPoint(Vec3V_In point) const;
	inline spdBool_Out ContainsSphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainsAABB(const spdAABB& box) const;
	inline spdBool_Out ContainsOrientedBB(const spdOrientedBB& box) const;

	inline spdBool_Out ContainedBySphere(const spdSphere& sphere) const;
	inline spdBool_Out ContainedByAABB(const spdAABB& box) const;

	// intersects
	inline spdBool_Out IntersectsSphere(const spdSphere& sphere) const;
	inline spdBool_Out IntersectsAABB(const spdAABB& box) const;
	inline spdBool_Out IntersectsOrientedBB(const spdOrientedBB& box) const;

	

	spdAABB m_localBox;
	Mat33V  m_localToWorld;
};

#include "spatialdata/aabb_inline.h"

} // namespace rage

#endif // SPATIALDATA_AABB_H
