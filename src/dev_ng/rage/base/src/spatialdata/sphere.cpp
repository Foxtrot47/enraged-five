//
// spatialdata/sphere.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "sphere.h"

#include "file/token.h"
#include "grcore/im.h"
#include "grcore/viewport.h"

using namespace rage;

IMPLEMENT_PLACE(spdSphere);


grcCullStatus spdSphere::GetCullStatusIntersectsSphere(const spdSphere& sphere) const
{
	Vec3V posV = m_centerAndRadius.GetXYZ();
	Vec3V centerV = sphere.m_centerAndRadius.GetXYZ();

	ScalarV radiusV = SplatW( m_centerAndRadius );
	ScalarV sphereRadiusV = SplatW( sphere.m_centerAndRadius );

	Vec3V deltaV = Subtract( centerV, posV );
	ScalarV distSqV = Dot( deltaV, deltaV );		// this will do Dot on 3 component vector, so it is not needed to clear w

	ScalarV radiusSumV = Add( radiusV, sphereRadiusV );
	ScalarV radiusSumSqV = Scale( radiusSumV, radiusSumV );

	if( IsGreaterThanAll(distSqV, radiusSumSqV) )
		return cullOutside;

	ScalarV radiusDiffV = Subtract( radiusV, sphereRadiusV );
	ScalarV radiusDiffSqV = Scale( radiusDiffV, radiusDiffV );

	if( IsGreaterThanAll( distSqV, radiusDiffSqV ) )	// observe that I flipped the compare fn from <= to > (greater than all ) - svetli
		return cullClipped;

	return cullInside;
}

#if !__SPU

void spdSphere::SaveData(fiAsciiTokenizer& tok) const
{
	tok.StartLine();
	tok.PutStr("boundingSphere { %f %f %f %f }", m_centerAndRadius.GetXf(), m_centerAndRadius.GetYf(), m_centerAndRadius.GetZf(), m_centerAndRadius.GetWf());
	tok.EndLine();
}

void spdSphere::LoadData(fiAsciiTokenizer& tok)
{
	tok.GetDelimiter("boundingSphere");
	tok.GetDelimiter("{");
	const float x = tok.GetFloat();
	const float y = tok.GetFloat();
	const float z = tok.GetFloat();
	const float w = tok.GetFloat();
	m_centerAndRadius = Vec4V(x, y, z, w);
	tok.GetDelimiter("}");
}

#if __DECLARESTRUCT
void spdSphere::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(spdSphere);
	STRUCT_FIELD(m_centerAndRadius);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

#endif // !__SPU

#if 0
void sphSphere_UnitTests()
{
	spdSphere sph( Vector3( 1.0f, 2.0f, 3.0) , 4.0f);

	Assert( sph.GetCenterVector3() == Vector3( 1.0f, 2.0f, 3.0f ));
	Assert( sph.GetRadiusf() == 4.0f );

	Matrix34 mtx;
	mtx.Identity();
	mtx.Translate( Vector3( 2.0f, 3.0f, 4.0f ));

	sph.TransformSphereScaled( RCC_MAT34V(mtx) );
	Assert( sph.GetCenterVector3() == Vector3( 3.0f, 5.0f, 7.0f ));
	Assert( sph.GetRadiusf() == 4.0f );
}
#endif