// 
// spatialdata/plane.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SPATIALDATA_PLANE_H
#define SPATIALDATA_PLANE_H

#include "atl/array.h"
#include "parser/macros.h"
#include "vector/matrix34.h"
#include "vector/vector4.h"
#include "vectormath/legacyconvert.h"

namespace rage {

// PURPOSE: Represents an infinite plane or half-space.
class spdPlane
{
public:

	DECLARE_PLACE(spdPlane);

	spdPlane() {}
	spdPlane(const spdPlane &p) : m_PlaneCoeffs(p.m_PlaneCoeffs) {}
	spdPlane(datResource &UNUSED_PARAM(rsc)) {};
	spdPlane(const Vector4 &v) : m_PlaneCoeffs(v) {}
	spdPlane(const Vector3 &p, const Vector3 &n)
	{
		ComputePlaneV( VECTOR3_TO_VEC3V(p), VECTOR3_TO_VEC3V(n) );
	}
	spdPlane(Vec3V_In p, Vec3V_In n)
	{
		ComputePlaneV( p, n );
	}

	void Set(const Vector3 &p, const Vector3 &n)
	{
		ComputePlaneV( VECTOR3_TO_VEC3V(p), VECTOR3_TO_VEC3V(n) );
	}

	void Invalidate()
	{
		m_PlaneCoeffs.Zero();
	}

	const Vector3 &GetNormal() const
	{
		return (const Vector3&)(m_PlaneCoeffs);
	}

	const Vector4& GetCoefficientVector() const
	{
		return m_PlaneCoeffs;
	}

	void SetCoefficientVector(const Vector4& coeffs)
	{
		m_PlaneCoeffs.Set(coeffs);
	}

	void ComputePlaneV(Vec3V_In p, Vec3V_In n)
	{
		RC_VEC4V(m_PlaneCoeffs).SetXYZ(n);
		RC_VEC4V(m_PlaneCoeffs).SetW( Dot( n, p ) );
	}

	void SetPair(const Vector3 &p, const Vector3 &n, const Matrix34 &mat, spdPlane &out)
	{
		ComputePlaneV( VECTOR3_TO_VEC3V(p), VECTOR3_TO_VEC3V(n) );

		Vector3 p2 = p, n2 = n;
		mat.Transform(p2);
		mat.Transform3x3(n2);

		out.ComputePlaneV( VECTOR3_TO_VEC3V(p2), VECTOR3_TO_VEC3V(n2) );
	}

	void Transform(const Matrix34 &mat)
	{
		// TODO: There must be a better way.
		Vector3 p = VEC3V_TO_VECTOR3(Scale( RCC_VEC4V(m_PlaneCoeffs).GetXYZ(), RCC_VEC4V(m_PlaneCoeffs).GetW() ));
		Vector3 n = GetNormal();

		mat.Transform(p);
		mat.Transform3x3(n);
		ComputePlaneV( VECTOR3_TO_VEC3V(p), VECTOR3_TO_VEC3V(n) );
	}

	void Transform(const Matrix34 &mat, spdPlane &out) const
	{
		out = *this;
		out.Transform(mat);
	}

	float DistanceToPlane(const Vector3& point) const
	{
		return m_PlaneCoeffs.DistanceToPlane(point);
	}

	__forceinline ScalarV_Out DistanceToPlaneV(Vec3V_In point) const
	{
		Vec4V coeffs = RCC_VEC4V( m_PlaneCoeffs );
		ScalarV coeffsW = SplatW( coeffs );
		return Dot( coeffs.GetXYZ(), point ) - coeffsW;
	}

	// Returns: 0=no intersection, 1=intersection, 2=coplanar.
	int IntersectLine(const Vector3 &p1, const Vector3 &p2, Vector3 &outPoint, float *outT = NULL) const;
	BoolV_Out IntersectLineV(Vec3V_In start, Vec3V_In end, Vec3V_InOut outPoint) const;
	int Clip(const atArray<Vector3> *pIn, atArray<Vector3> *pOut) const;
	int Clip(const Vector3 *pIn, int inCount, Vector3 *pOut, int outBufferSize) const;
	BoolV_Out IntersectRayV(Vec3V_In origin, Vec3V_In dir, Vec3V_InOut outPoint) const;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

protected:
	Vector4 m_PlaneCoeffs;

	PAR_SIMPLE_PARSABLE;
};

} // namespace rage

#endif // SPATIALDATA_PLANE_H

