<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="rage::spdAABB" onPreLoad="PreLoad" simple="true">
    <Vec4V name="m_min"/>
    <Vec4V name="m_max"/>
  </structdef>

  <structdef type="rage::spdPlane">
    <Vec4V name="m_PlaneCoeffs"/>
  </structdef>

  <structdef type="rage::spdSphere" simple="true">
    <Vec4V name="m_centerAndRadius"/>
  </structdef>

  <!-- Add this when we need it - either serialize member vars individually or could serialize as an array of Vec4Vs
	<structdef type="rage::spdTransposedPlaneSet">
		<array name="m_planes" type="member" size="8">
			<Vec4V/>
		</array>
	</structdef>
  -->

</ParserSchema>