/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef EMPTY_ALGORITH
#define EMPTY_ALGORITH
#include "btCollisionAlgorithm.h"

#if ENABLE_UNUSED_PHYSICS_CODE

#include "btCollisionCreateFunc.h"

#define ATTRIBUTE_ALIGNED(a)

///EmptyAlgorithm is a stub for unsupported collision pairs.
///The dispatcher can dispatch a persistent btEmptyAlgorithm to avoid a search every frame.
class btEmptyAlgorithm : public btCollisionAlgorithm
{

public:
	
	btEmptyAlgorithm();

    virtual void DetectCollision (const rage::phCollisionInput& input, rage::Vector3::Param offsetA, rage::phManifold& manifold, DiscreteCollisionDetectorInterface::ResultProcessor& manifoldResult);

} ATTRIBUTE_ALIGNED(16);

#endif // ENABLE_UNUSED_PHYSICS_CODE

#endif //EMPTY_ALGORITH
