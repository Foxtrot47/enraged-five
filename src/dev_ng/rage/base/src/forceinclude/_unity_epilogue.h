#if defined(_MSC_VER)
#pragma warning(pop)
#endif

#if __PS3

#if __DEV
# if !__OPTIMIZED
	#pragma	control O=0			// Debug  p4 submit check: PRAGMA-OPTIMIZE-ALLOW
# else
	#pragma	control O=d			// Beta
# endif
#elif __BANK
	#pragma	control O=s			// Bank release
#elif !__FINAL
	#pragma	control O=s			// Release 
#else
	#pragma	control O=s			// Final
#endif 

#ifdef __SNC__ 
#pragma control notocrestore=2
#pragma control fastmath=1
#pragma control assumecorrectalignment=1
#pragma control assumecorrectsign=1
#pragma control relaxalias=0
#pragma control checknew=0
#endif

#elif defined(_MSC_VER) 
#pragma optimize("", on)	// PRAGMA-OPTIMIZE-ALLOW
#endif
