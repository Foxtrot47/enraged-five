#define __NO_OUTPUT	__FINAL

#define PRINTF_LIKE_N(a) __attribute__((format (printf, (a), (a)+1)))

#if __NO_OUTPUT
#define OUTPUT_ONLY(...)
#define Quitf(...) __debugbreak()
#define Printf(...) do { } while (0)
#define Displayf(...) do { } while (0)
#define Warningf(...) do { } while (0)
#define Errorf(...) do { } while (0)
#define Debugf1(...) do { } while (0)
#define Debugf2(...) do { } while (0)
#define Debugf3(...) do { } while (0)
#else	// __NO_OUTPUT
#define OUTPUT_ONLY(...)    __VA_ARGS__
#ifdef __cplusplus
extern "C" 
#endif
int _spu_call_event_va_arg(unsigned int _spup, const char *fmt, ...) __attribute__((format (printf, 2, 3)));
#define EVENT_PRINTF_COMMAND	(1U<<24)

// Only Printf supports a non-constant format string on SPU.
#define Printf(fmt,args...)	_spu_call_event_va_arg(EVENT_PRINTF_COMMAND,"<"DEBUG_SPU_JOB_NAME"> " fmt,##args)
#define Displayf(fmt,args...)	_spu_call_event_va_arg(EVENT_PRINTF_COMMAND,"<"DEBUG_SPU_JOB_NAME"> " fmt "\n",##args)
#define Warningf(fmt,args...)	_spu_call_event_va_arg(EVENT_PRINTF_COMMAND,"<"DEBUG_SPU_JOB_NAME"> Warning: " fmt "\n",##args)
#define Errorf(fmt,args...)	_spu_call_event_va_arg(EVENT_PRINTF_COMMAND,"<"DEBUG_SPU_JOB_NAME"> Error: " fmt "\n",##args)
#define Quitf(errorCode,fmt,args...)	do { _spu_call_event_va_arg(EVENT_PRINTF_COMMAND,"<"DEBUG_SPU_JOB_NAME"> Fatal Error (halt): " fmt "\n",##args); __debugbreak(); } while (0) errorCode;
#endif //__NO_OUTPUT
