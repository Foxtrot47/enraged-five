#define _CPPRTTI

#define I64FMT          "l"
#define LI64FMT         L"l"
#define SIZETFMT        "l"
#define PTRDIFFTFMT     "l"

// Networking
#define RSG_NP          1
#define RSG_XBL         0
#define RSG_GAMESPY     0

// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long u64;
typedef __uint128_t u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long s64;

typedef float f32;
typedef u64 uptr;

END_NS_RAGE

#define GTA_REPLAY_RAGE

// Platform definitions
#define __BE		0
#define __CONSOLE	1
#define __PS3		0
#define __PPU		0
#define __SPU		0
#define __WIN32		0
#define __OSX		0
#define __POSIX		0
#define __XENON		0
#define __PSP2		0
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define RSG_BE          0
#define RSG_PS3         0
#define RSG_PPU         0
#define RSG_SPU         0
#define RSG_WIN32       0
#define RSG_XENON       0
#define RSG_PC          0
#define RSG_DURANGO     0
#define RSG_ORBIS       1
#define RSG_WINRT		0
#define RSG_XDK			0
#define RSG_GDK			0
#define RSG_LINUX		0
#define RSG_IOS			0
#define RSG_ANDROID		0
#define RSG_WINPHONE	0
#define RSG_MOBILE		(RSG_IOS | RSG_ANDROID | RSG_WINPHONE)
#define RSG_NX			0
#define RSG_P			0
#define RSG_GGP			0
#define RSG_OSX			0
#define RSG_SCE			1
#define RSG_XBOX		0
#define RSG_PROSPERO	0
#define RSG_SCARLETT	0

#define RSG_CPU_PPC     0
#define RSG_CPU_SPU     0
#define RSG_CPU_INTEL   1
#define RSG_CPU_X86     0
#define RSG_CPU_X64     1

#define CONSOLE_ONLY(...) 	__VA_ARGS__
#define PS3_ONLY(...)
#define PPU_ONLY(...)
#define SPU_ONLY(...)
#define WIN32_ONLY(...)
#define XENON_ONLY(...)
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)
#define ORBIS_ONLY(...)		__VA_ARGS__
#define PROSPERO_ONLY(...)
#define NP_ONLY(...)		__VA_ARGS__
#define SCE_ONLY(...)		__VA_ARGS__
#define DURANGO_ONLY(...)
#define WINRT_ONLY(...)
#define SCARLETT_ONLY(...)
#define XBL_ONLY(...)
#define XDK_ONLY(...)
#define GDK_ONLY(...)
#define XBOX_ONLY(...)

#define ALIGNAS(x)		__attribute__((aligned(x)))	//alignas(x) // <-- in the way we use it, alignas() isn't supported under SDK 1.7 (compiler is now very strict about its use in types - see C++11 docs)
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)      __attribute__((aligned(x)))
#define DEPRECATED      __attribute__((__deprecated__))
#define RESTRICT        __restrict
#define MAY_ALIAS       __attribute__((__may_alias__))
#define __forceinline   __attribute__((__always_inline__)) inline

#define PRAGMA_OPTIMIZE_OFF()	__pragma(optimize("", off))	// PRAGMA-OPTIMIZE-ALLOW

#pragma GCC diagnostic ignored "-Wreorder"
#pragma GCC diagnostic ignored "-Wmultichar"
#pragma GCC diagnostic ignored "-Wmissing-braces"
#pragma GCC diagnostic ignored "-Wconstant-logical-operand"
#pragma GCC diagnostic ignored "-Wc++11-narrowing"	// until parser can get cleaned up
#pragma GCC diagnostic error   "-Wundef"

