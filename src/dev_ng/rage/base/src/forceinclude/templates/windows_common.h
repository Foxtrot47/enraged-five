// Ensure that the correct compiler version is being used

// 2008 ?
#if _MSC_VER == 1500
#	if _MSC_FULL_VER != 150030729 || _MSC_BUILD != 01
#		error "When compiling with Visual Studio 2008, service pack 1 is required"
#	endif

// 2010 ?
#elif _MSC_VER == 1600
#	if _MSC_FULL_VER != 160040219 || _MSC_BUILD != 01
#		error "When compiling with Visual Studio 2010, service pack 1 is required"
#	endif

#endif

#define I64FMT 		"I64"
#define LI64FMT 	L"I64"
#ifdef _M_X64
#define SIZETFMT	"I"
#define PTRDIFFTFMT	"I"
#else
#define SIZETFMT	""
#define PTRDIFFTFMT	""
#endif

// Networking
#define RSG_NP		0
#define RSG_XBL		0
#define RSG_GAMESPY	1

