#define I64FMT          "I64"
#define LI64FMT         L"I64"
#define SIZETFMT        ""
#define PTRDIFFTFMT     ""

// Networking
#define RSG_NP          0
#define RSG_XBL         1
#define RSG_GAMESPY     0

// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned __int64 u64;
typedef struct __declspec(intrin_type) __declspec(align(16)) __u128 {
   float m128_f32[4];
} __u128;
typedef __u128 u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed __int64 s64;

typedef float f32;
typedef u32 uptr;

END_NS_RAGE

// Platform definitions
#define __BE		1
#define __CONSOLE	1
#define __PS3		0
#define __PPU		0
#define __SPU		0
#define __WIN32		1
#define __OSX		0
#define __POSIX		0
#define __XENON		1
#define __PSP2		0
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define RSG_BE          1
#define RSG_PS3         0
#define RSG_PPU         0
#define RSG_SPU         0
#define RSG_WIN32       1
#define RSG_XENON       1
#define RSG_PC          0
#define RSG_DURANGO     0
#define RSG_ORBIS       0
#define RSG_WINRT		0
#define RSG_XDK			0
#define RSG_GDK			0
#define RSG_LINUX		0
#define RSG_IOS			0
#define RSG_ANDROID		0
#define RSG_WINPHONE	0
#define RSG_MOBILE		(RSG_IOS | RSG_ANDROID | RSG_WINPHONE)
#define RSG_NX			0
#define RSG_P			0
#define RSG_GGP			0
#define RSG_OSX			0
#define RSG_SCE			0
#define RSG_XBOX		0
#define RSG_PROSPERO	0
#define RSG_SCARLETT	0

#define RSG_CPU_PPC     1
#define RSG_CPU_SPU     0
#define RSG_CPU_INTEL   0
#define RSG_CPU_X86     0
#define RSG_CPU_X64     0

#ifndef _XBOX
	#define _XBOX
#endif

#define CONSOLE_ONLY(...)   __VA_ARGS__
#define PS3_ONLY(...)
#define PPU_ONLY(...)
#define SPU_ONLY(...)
#define WIN32_ONLY(...)     __VA_ARGS__
#define XENON_ONLY(...)     __VA_ARGS__
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)
#define ORBIS_ONLY(...)		
#define PROSPERO_ONLY(...)
#define NP_ONLY(...)		
#define SCE_ONLY(...)		
#define DURANGO_ONLY(...)	
#define WINRT_ONLY(...)		
#define SCARLETT_ONLY(...)
#define XBL_ONLY(...)		
#define XDK_ONLY(...)		
#define GDK_ONLY(...)
#define XBOX_ONLY(...)		

#define ALIGNAS(x)			__declspec(align(x))
#define BEGIN_ALIGNED(x) __declspec(align(x))
#define ALIGNED(x)
#define DEPRECATED			__declspec(deprecated)
#define RESTRICT			__restrict
#define MAY_ALIAS

#pragma warning(disable: 4127)
#pragma warning(disable: 4200)
#pragma warning(disable: 4291)
#pragma warning(disable: 4512)
#pragma warning(disable: 4530)
#pragma warning(disable: 4505)
#pragma warning(disable: 4710)
#pragma warning(error: 4062)
#pragma warning(error: 4263)
#pragma warning(error: 4265)
#pragma warning(error: 4668)

// Static analysis
#pragma warning(disable: 6011)	// NULL dereference
#pragma warning(disable: 6211)	// Leaking memory due to missing exception handling
#pragma warning(disable: 6255)	// Use of _alloca vs _malloca
#pragma warning(disable: 6294)	// Ill-defined for-loop
#pragma warning(disable: 6297)	// Arithmetic overflow: 32-bit value is shifted & cast to 64-bit. TEMPORARY.
#pragma warning(disable: 6323)	// Use of arithmetic operator on Boolean type(s)
#pragma warning(disable: 6326)	// Comparison of constant with constant
#pragma warning(disable: 6334)	// sizeof() on expression
#pragma warning(disable: 6384)	// Dividing sizeof a pointer by another value
#pragma warning(disable: 6385)	// Possible buffer overrun, read
#pragma warning(disable: 6386)	// Possible buffer overrun, write


#define __builtin_trap()		__debugbreak()

#define PRAGMA_OPTIMIZE_OFF()	__pragma(optimize("", off))	// PRAGMA-OPTIMIZE-ALLOW
