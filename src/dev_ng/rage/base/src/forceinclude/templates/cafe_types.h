// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef struct { float x,y,z,w; } u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef unsigned long long u64;
typedef signed long long s64;

typedef float f32;
typedef u32 uptr;

END_NS_RAGE

// Platform definitions
#define __BE		1
#define __CONSOLE	1
#define __PS3		0
#define __PPU		0
#define __SPU		0
#define __WIN32		0
#define __OSX		0
#define __POSIX		0
#define __XENON		0
#define __PSP2		0
#define __CAFE		1
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define CONSOLE_ONLY(...)   __VA_ARGS__
#define PS3_ONLY(...)
#define PPU_ONLY(...)
#define SPU_ONLY(...)
#define WIN32_ONLY(...)
#define XENON_ONLY(...)
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)      __VA_ARGS__
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)

#define ALIGNAS(x)	__attribute__((aligned(x)))
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)	__attribute__((aligned(x)))
#define DEPRECATED	__attribute__((__deprecated__))
#define RESTRICT	__restrict
#define MAY_ALIAS	__attribute__((__may_alias__))
#define __forceinline	__attribute__((__always_inline__)) inline

// VC.Net-specific keyword, meaningless to us
#define __w64

#define __debugbreak() 		(*(int*)(0xD1E)=0)
#define __builtin_trap()	(*(int*)(0xD1E)=3)

#define PRAGMA_OPTIMIZE_OFF()	_Pragma("control O=0") // PRAGMA-OPTIMIZE-ALLOW


