#define I64FMT		"ll"
#define LI64FMT		L"ll"
#define SIZETFMT 	"z"
#define PTRDIFFTFMT	"t"

// Networking
#define RSG_NP          1
#define RSG_XBL         0
#define RSG_GAMESPY     0

// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef vector unsigned int u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;

typedef float f32;
typedef u32 uptr;

END_NS_RAGE

// Platform definitions
#define __BE		1
#define __CONSOLE	1
#define __PS3		1
#define __PPU		0
#define __SPU		1
#define __WIN32		0
#define __OSX		0
#define __POSIX		0
#define __XENON		0
#define __PSP2		0
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define RSG_BE          1
#define RSG_PS3         1
#define RSG_PPU         0
#define RSG_SPU         1
#define RSG_WIN32       0
#define RSG_XENON       0
#define RSG_PC          0
#define RSG_DURANGO         0
#define RSG_ORBIS       0
#define RSG_WINRT		0
#define RSG_LINUX		0
#define RSG_IOS			0
#define RSG_ANDROID		0
#define RSG_WINPHONE	0
#define RSG_MOBILE		(RSG_IOS | RSG_ANDROID | RSG_WINPHONE)
#define RSG_NX			0
#define RSG_P			0
#define RSG_GGP			0
#define RSG_OSX			0
#define RSG_SCE			0
#define RSG_XBOX		0
#define RSG_PROSPERO	0
#define RSG_SCARLETT	0

#define RSG_CPU_PPC     0
#define RSG_CPU_SPU     1
#define RSG_CPU_INTEL   0
#define RSG_CPU_X86     0
#define RSG_CPU_X64     0

#define CONSOLE_ONLY(...)   __VA_ARGS__
#define PS3_ONLY(...)       __VA_ARGS__
#define PPU_ONLY(...)
#define SPU_ONLY(...)       __VA_ARGS__
#define WIN32_ONLY(...)
#define XENON_ONLY(...)
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)
#define DURANGO_ONLY(...)
#define ORBIS_ONLY(...)
#define WINRT_ONLY(...)

#define ALIGNAS(x)	// Not supported on SPU
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)	__attribute__((aligned(x)))
#define DEPRECATED
#define RESTRICT
#define MAY_ALIAS	__attribute__((__may_alias__))
#define __forceinline	__attribute__((__always_inline__)) inline

// VC.Net-specific keyword, meaningless to us
#define __w64

static __forceinline void __debugbreak() { __asm__ __volatile__("stopd 0,1,1"); }

#define PRAGMA_OPTIMIZE_OFF() // PRAGMA-OPTIMIZE-ALLOW

