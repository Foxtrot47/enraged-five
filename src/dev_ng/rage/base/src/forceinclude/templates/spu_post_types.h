#define RSG_DEV         __DEV
#define RSG_BANK        __BANK
#define RSG_ASSERT      __ASSERT
#define RSG_TOOL        __TOOL
#define RSG_RSC         __RESOURCECOMPILER
#define RSG_FINAL       __FINAL

// Decode build system macros set above.
#if __DEV
#	define DEV_ONLY(...)    __VA_ARGS__
#else
#	define DEV_ONLY(...)
#endif

#if __BANK
#	define BANK_ONLY(...)   __VA_ARGS__
#else
#	define BANK_ONLY(...)
#endif

#define Likely(x)	__builtin_expect(!!(x),1)
#define Unlikely(x)	__builtin_expect(!!(x),0)

#if __DEV
#	define OPTIMISATIONS_OFF()  PRAGMA_OPTIMIZE_OFF() // PRAGMA-OPTIMIZE-ALLOW
#else
#	define OPTIMISATIONS_OFF()
#endif

#if __ASSERT
#	define ASSERT_ONLY(...)         __VA_ARGS__
#	define Assert(x)                (void)(Likely(x) || (::rage::AssertFailed(#x,__FILE__,__LINE__)))
#	define FastAssert(x)            Assert(x)
#	define FatalAssert(x)           do { if (!Likely(x)) { ::rage::AssertFailed(#x,__FILE__,__LINE__); __debugbreak(); } } while(0)
#	define FatalAssertf(x,fmt,...)  do { if (!Likely(x)) { Errorf(fmt,##__VA_ARGS__); ::rage::AssertFailed(#x,__FILE__,__LINE__); __debugbreak(); } } while(0)
#	define Assertf(x,fmt,...)       do { if (!Likely(x)) { Errorf(fmt,##__VA_ARGS__); ::rage::AssertFailed(#x,__FILE__,__LINE__); } } while(0)
#	define AssertMsg(x,msg)         (void)(Likely(x) || (::rage::AssertFailed(msg,__FILE__,__LINE__)))
#	define AssertVerify(x)          (Likely(x) || (::rage::AssertFailed(#x,__FILE__,__LINE__)))
#	define Verifyf(x,fmt,...)       AssertVerify(x)

	BEGIN_NS_RAGE
	extern int AssertFailed(const char *msg,const char *file,int line);
	extern int DebugAssertFailed(void);
	END_NS_RAGE

#	define DebugAssert(x)	        (void)(Likely(x) || ::rage::DebugAssertFailed())

#else
#	define ASSERT_ONLY(...)
#	define Assert(x)
#	define Assertf(x,fmt,...)
#	define FastAssert(x)
#	define FatalAssert(x)
#	define FatalAssertf(x,fmt,...)
#	define AssertMsg(x,msg)
#	define AssertVerify(x)          (x)
#	define Verifyf(x,fmt,...)       (x)
#	define DebugAssert(x)
#endif

#define AlignedAssert(x,a)              Assert(((::rage::uptr)(x) & ((a)-1)) == 0)
#define AlignedAssertf(x,a,fmt,...)     Assertf(((::rage::uptr)(x) & ((a)-1)) == 0,fmt,##__VA_ARGS__)
#define Assert16(x)                     AlignedAssert(x,16)
#define Assert128(x)                    AlignedAssert(x,128)

#define MacroJoin( X, Y )               MacroDoJoin(X, Y)
#define MacroDoJoin( X, Y )             MacroDoJoin2(X, Y)
#define MacroDoJoin2( X, Y )            X##Y
#define CompileTimeAssert(e)            typedef char MacroJoin(MacroJoin(__COMPILETIME_ASSERT, __LINE__), __)[(e)?1:-1]
#define AlignedCompileTimeAssert(x,a)   CompileTimeAssert(((::rage::uptr)(x) & ((a)-1)) == 0)
#define CompileTimeAssert16(x)          AlignedCompileTimeAssert(x,16)
#define CompileTimeAssert128(x)         AlignedCompileTimeAssert(x,128)

#if __EDITOR
#	define EDITOR_ONLY(...)             __VA_ARGS__
#else
#	define EDITOR_ONLY(...)
#endif

#if __TOOL
#	define TOOL_ONLY(...)               __VA_ARGS__
#else
#	define TOOL_ONLY(...)
#endif

#if __EXPORTER
#	define EXPORTER_ONLY(...)           __VA_ARGS__
#else
#	define EXPORTER_ONLY(...)
#endif

#if __PAGING
#	define	PAGING_ONLY(...)            __VA_ARGS__
#else
#	define PAGING_ONLY(...)
#endif

#if __FINAL
#	define NOTFINAL_ONLY(...)
#else
#	define NOTFINAL_ONLY(...)           __VA_ARGS__
#endif

#if __ASSERT
#	define AssertType(type,ptr)         Assert(SafeCast(type,ptr) && #type)
#else
#	define AssertType(type,ptr)
#endif

#ifdef __cplusplus
	template <class L,class R> const L& Assign(L& left,const R right) {
		left = static_cast<L>(right); FastAssert(left == right); return left; }
	template <typename _Type, int actualSize,int expectedSize> struct CTASHelper { CompileTimeAssert(actualSize == expectedSize); };
#endif
#define BitfieldAssign(x,y) do { (x)=(y); FastAssert((x) == (y)); } while (0)

#define __LTCG	0
#define ATTR_UNUSED
#define __64BIT	0
#define OffsetOf(s,m)   ((size_t)&(((s*)1)->m)-1)

#define CompileTimeAssertSize(s,if32,if64) static __attribute__((unused)) CTASHelper<s,int(sizeof(s)),if32> MacroJoin(MacroJoin(s,__LINE__),Helper)

#define ptrdiff_t_to_int(x)	(x)

#define istrlen(x)      ((int)strlen(x))
#define ustrlen(x)      ((unsigned)strlen(x))

BEGIN_NS_RAGE
	typedef const u8 	dev_u8;
	typedef const s8 	dev_s8;
	typedef const u16 	dev_u16;
	typedef const s16 	dev_s16;
	typedef const u32 	dev_u32;
	typedef const s32 	dev_s32;
	typedef const u32 	dev_u64;
	typedef const s32 	dev_s64;
	typedef const float dev_float;
# ifdef __cplusplus
	typedef const bool 	dev_bool;
# endif // __cplusplus

	typedef const u8 	bank_u8;
	typedef const s8 	bank_s8;
	typedef const u16 	bank_u16;
	typedef const s16 	bank_s16;
	typedef const u32 	bank_u32;
	typedef const s32 	bank_s32;
	typedef const u64 	bank_u64;
	typedef const s64 	bank_s64;
	typedef const float bank_float;
# ifdef __cplusplus
	typedef const bool 	bank_bool;
# endif // __cplusplus
END_NS_RAGE


