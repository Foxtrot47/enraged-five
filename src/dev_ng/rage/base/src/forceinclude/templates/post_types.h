#define RSG_DEV		__DEV
#define RSG_BANK	__BANK
#define RSG_ASSERT	__ASSERT
#define RSG_TOOL	__TOOL
#define RSG_RSC		__RESOURCECOMPILER
#define RSG_FINAL	__FINAL

#define RSG_LAUNCHER	0

#define GTA_SCRIPT_LEGACY_PLATFORM_VERSION 0

// Decode build system macros set above.
#if __DEV
#	define DEV_ONLY(...)    __VA_ARGS__
#else
#	define DEV_ONLY(...)
#endif

#if __BANK
#	define BANK_ONLY(...)  __VA_ARGS__
#else
#	define BANK_ONLY(...)
#endif

#if __PPU || __PSP2
#	define Likely(x)    __builtin_expect(!!(x),1)
#	define Unlikely(x)  __builtin_expect(!!(x),0)
#else
#	define Likely(x)    (x)
#	define Unlikely(x)  (x)
#endif

#ifdef __GNUG__
#	define PRINTF_LIKE_N(a) __attribute__((format (printf, (a), (a)+1)))
#else
#	define PRINTF_LIKE_N(a)
#endif

#if __DEV
#	define OPTIMISATIONS_OFF()  PRAGMA_OPTIMIZE_OFF() // PRAGMA-OPTIMIZE-ALLOW
#else
#	define OPTIMISATIONS_OFF()
#endif

#if __DEV && __XENON
#	define __forceinline inline
#endif

#if __ASSERT

#	define RAGE_MINIMAL_ASSERTS	(__XENON && __OPTIMIZED)

#	if RAGE_MINIMAL_ASSERTS
#		ifdef __SNC__
#			pragma diag_suppress 178
#			pragma diag_suppress 552
#			pragma diag_suppress 828
#		elif defined(_MSC_VER)
#			pragma warning(disable: 4100)
#			pragma warning(disable: 4101)
#			pragma warning(disable: 4189)
#		endif
#		define Assertf(x,fmt,...)      Assert(x)
#		define Verifyf(x,fmt,...)      AssertVerify(x)
#	else
#		define Assertf(x,fmt,...)      (void)(Likely(x) || ::rage::diagAssertHelper(__FILE__,__LINE__,"%s: " fmt,#x,##__VA_ARGS__) || (__debugbreak(),0))
#		define Verifyf(x,fmt,...)      (Likely(x) || ((::rage::diagAssertHelper(__FILE__,__LINE__,"%s: " fmt,#x,##__VA_ARGS__) || (__debugbreak(),0)),false))
#	endif
#	define Assert(x)               (void)(Likely(x) || ::rage::diagAssertHelper2(__FILE__,__LINE__,#x) || (__debugbreak(),0))
#	define FatalAssert(x)          do{if(Unlikely(!(x))){Quitf(0,__FILE__"(%d): %s",__LINE__,#x);}}while(0)
#	define FatalAssertf(x,fmt,...) do{if(Unlikely(!(x))){Errorf(__FILE__"(%d): %s",__LINE__,#x);Quitf(0,fmt,##__VA_ARGS__);}}while(0)
#	define AssertVerify(x)         (Likely(x) || ((::rage::diagAssertHelper2(__FILE__,__LINE__,#x) || (__debugbreak(),0)),false))

#	define ASSERT_ONLY(...)        __VA_ARGS__
#	define AssertMsg(x,msg)        Assertf(x,"%s",msg)
#	define FatalAssertMsg(x,msg)   FatalAssertf(x,"%s",msg)

#	if __TOOL || !__OPTIMIZED || __WIN32PC
#		define FastAssert(x)		Assert(x)
#	else
#		define FastAssert(x)		(Likely(x) || (__debugbreak(),0))
#	endif

	BEGIN_NS_RAGE
	extern int diagAssertHelper(const char*,int,const char*,...) 
#ifdef __ORBIS__
	PRINTF_LIKE_N(3)
#endif
	;
	extern int diagAssertHelper2(const char*,int,const char*);
	extern int DebugAssertFailed(void);
	END_NS_RAGE

#	define DebugAssert(x)	(void)(Likely(x) || ::rage::DebugAssertFailed())

#	if RSG_ORBIS
		// Enable Sony's asserts in GNM
#		define SCE_GNM_DEBUG
#	endif

#else // !__ASSERT
#	define ASSERT_ONLY(...)
#	define Assert(x)
#	define Assertf(x,fmt,...)
#	define FastAssert(x)
#	define FatalAssert(x)
#	define FatalAssertf(x,fmt,...)
#	define FatalAssertMsg(x,msg)
#	define AssertMsg(x,msg)
#	define AssertVerify(x) (x)
#	define Verifyf(x,fmt,...) (x)
#	define DebugAssert(x)
#endif

#define AlignedAssert(x,a) Assert(((::rage::uptr)(x) & ((a)-1)) == 0)
#define AlignedAssertf(x,a,fmt,...) Assertf(((::rage::uptr)(x) & ((a)-1)) == 0,fmt,##__VA_ARGS__)
#define Assert16(x) AlignedAssert(x,16)
#define Assert128(x) AlignedAssert(x,128)

#define MacroJoin( X, Y ) MacroDoJoin(X, Y)
#define MacroDoJoin( X, Y ) MacroDoJoin2(X, Y)
#define MacroDoJoin2( X, Y ) X##Y
#define CompileTimeAssert(e) typedef char MacroJoin(MacroJoin(__COMPILETIME_ASSERT, __LINE__), __)[(e)?1:-1]
#define AlignedCompileTimeAssert(x,a) CompileTimeAssert(((::rage::uptr)(x) & ((a)-1)) == 0)
#define CompileTimeAssert16(x) AlignedCompileTimeAssert(x,16)
#define CompileTimeAssert128(x) AlignedCompileTimeAssert(x,128)

#if __EDITOR
#	define EDITOR_ONLY(...)     __VA_ARGS__
#else
#	define EDITOR_ONLY(...)
#endif

#if __TOOL
#	define TOOL_ONLY(...)       __VA_ARGS__
#	define NOTTOOL_ONLY(...)
#else
#	define TOOL_ONLY(...)
#	define NOTTOOL_ONLY(...)    __VA_ARGS__
#endif

#if __EXPORTER
#	define EXPORTER_ONLY(...)   __VA_ARGS__
#else
#	define EXPORTER_ONLY(...)
#endif

#if __PAGING
#	define PAGING_ONLY(...)     __VA_ARGS__
#else
#	define PAGING_ONLY(...)
#endif

#if __FINAL
#	define NOTFINAL_ONLY(...)
#else
#	define NOTFINAL_ONLY(...)   __VA_ARGS__
#endif

#if __ASSERT
#	define AssertType(type,ptr)	Assert(SafeCast(type,ptr) && #type)
#else
#	define AssertType(type,ptr)
#endif

#ifdef __cplusplus
	template <class L,class R> __forceinline const L& Assign(L& left,const R right) {
	    left = static_cast<L>(right); FastAssert(left == right); return left; }
	template <typename _Type, int actualSize,int expectedSize> struct CTASHelper { CompileTimeAssert(actualSize == expectedSize); };
#endif

#define BitfieldAssign(x,y) do { (x)=(y); FastAssert(unsigned(x) == unsigned(y)); } while (0)

#define __LTCG	(__XENON && 0)

#if RSG_ORBIS
#define ATTR_UNUSED __attribute__((unused))
#else
#define ATTR_UNUSED
#endif

#if defined(__LP64__) || defined(_M_X64)
#	define __64BIT	1
#	define CompileTimeAssertSize(s,if32,if64) ATTR_UNUSED static CTASHelper<s,int(sizeof(s)),if64> MacroJoin(MacroJoin(s,__LINE__),Helper)
#	define OffsetOf(s,m)   ((size_t)((::rage::s64)&(((s*)0)->m)))
#else
#	define __64BIT	0
#	if __PPU
#		define OffsetOf(s,m)   ((size_t)&(((s*)1)->m)-1)
#		define CompileTimeAssertSize(s,if32,if64) static __attribute__((unused)) CTASHelper<s,int(sizeof(s)),if32> MacroJoin(MacroJoin(s,__LINE__),Helper)
#	else
#		define OffsetOf(s,m)   ((size_t)&(((s*)0)->m))
#		define CompileTimeAssertSize(s,if32,if64) static CTASHelper<s,int(sizeof(s)),if32> MacroJoin(MacroJoin(s,__LINE__),Helper)
#	endif // __PPU
#endif

#if defined(__SNC__) && __SN_VER__ < 43001
#	define ATTR_COLD __attribute__((cold))              // same as =Xsaverestorefuncs=1 for this function
#	define ATTR_HOT __attribute__((hot))                // inlinesrf and also likely virtual target
#	define ATTR_INLINESRF __attribute__((inlinesrf))    // same as =Xsaverestorefuncs=0 for this function
#else
#	define ATTR_COLD
#	define ATTR_HOT
#	define ATTR_INLINESRF
#endif

#if __64BIT && !__PROFILE && !__FINAL && defined(__cplusplus)
	inline int ptrdiff_t_to_int(long long v) { if (v != int(v)) __debugbreak(); return int(v); }
#elif __64BIT
#	define ptrdiff_t_to_int(x)  ((int)(x))
#else
#	define ptrdiff_t_to_int(x)  (x)
#endif

#define istrlen(x)	((int)strlen(x))
#define ustrlen(x)	((unsigned)strlen(x))

BEGIN_NS_RAGE
#if __DEV
	typedef u8 			dev_u8;
	typedef s8 			dev_s8;
	typedef u16 		dev_u16;
	typedef s16 		dev_s16;
	typedef u32 		dev_u32;
	typedef s32 		dev_s32;
	typedef u64 		dev_u64;
	typedef s64 		dev_s64;
	typedef float 		dev_float;
# ifdef __cplusplus
	typedef bool 		dev_bool;
# endif // __cplusplus
#else // __DEV
	typedef const u8 	dev_u8;
	typedef const s8 	dev_s8;
	typedef const u16 	dev_u16;
	typedef const s16 	dev_s16;
	typedef const u32 	dev_u32;
	typedef const s32 	dev_s32;
	typedef const u64 	dev_u64;
	typedef const s64 	dev_s64;
	typedef const float dev_float;
# ifdef __cplusplus
	typedef const bool 	dev_bool;
# endif // __cplusplus
#endif // __DEV

#if __BANK
	typedef u8 			bank_u8;
	typedef s8 			bank_s8;
	typedef u16 		bank_u16;
	typedef s16 		bank_s16;
	typedef u32 		bank_u32;
	typedef s32 		bank_s32;
	typedef u64 		bank_u64;
	typedef s64 		bank_s64;
	typedef float 		bank_float;
# ifdef __cplusplus
	typedef bool 		bank_bool;
# endif // __cplusplus
#else // __BANK
	typedef const u8 	bank_u8;
	typedef const s8 	bank_s8;
	typedef const u16 	bank_u16;
	typedef const s16 	bank_s16;
	typedef const u32 	bank_u32;
	typedef const s32 	bank_s32;
	typedef const u64 	bank_u64;
	typedef const s64 	bank_s64;
	typedef const float bank_float;
# ifdef __cplusplus
	typedef const bool 	bank_bool;
# endif // __cplusplus
#endif // __BANK
END_NS_RAGE

