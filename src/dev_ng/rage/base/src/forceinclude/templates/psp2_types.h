#pragma control notocrestore=2
#pragma control fastmath=1
#pragma control assumecorrectalignment=1
#pragma control assumecorrectsign=0
#pragma control relaxalias=0
#pragma control checknew=0
#pragma control c+=rtti
#pragma control c-=exceptions
#define _CPPRTTI 

// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef vector unsigned int u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef unsigned long long u64;
typedef signed long long s64;

typedef float f32;
typedef u32 uptr;

END_NS_RAGE

// Platform definitions
#define __BE		0
#define __CONSOLE	1
#define __PS3		0
#define __PPU		0
#define __SPU		0
#define __WIN32		0
#define __OSX		0
#define __POSIX		0
#define __XENON		0
#define __PSP2		1
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define CONSOLE_ONLY(...)   __VA_ARGS__
#define PS3_ONLY(...)
#define PPU_ONLY(...)
#define SPU_ONLY(...)
#define WIN32_ONLY(...)
#define XENON_ONLY(...)
#define PSP2_ONLY(...)      __VA_ARGS__
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)

#define ALIGNAS(x)	// Not supported on psp2
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)	__attribute__((aligned(x)))
#define DEPRECATED	__attribute__((__deprecated__))
#define RESTRICT	__restrict
#define MAY_ALIAS	__attribute__((__may_alias__))
#define __forceinline	__attribute__((__always_inline__)) inline

// VC.Net-specific keyword, meaningless to us
#define __w64

#define __debugbreak() __builtin_breakpoint(0)
#define __builtin_trap() __builtin_breakpoint(3)

#define PRAGMA_OPTIMIZE_OFF()	_Pragma("control O=0") // PRAGMA-OPTIMIZE-ALLOW

#pragma diag_error 194		// zero used for undefined preprocessing identifier "xxxx"
#pragma diag_error 828		// entity-kind "entity" was never referenced
#pragma diag_suppress 613	// overloaded virtual function "blah" is only partially overridden in class "blahChild"
#pragma diag_suppress 739	//  using-declaration ignored -- it refers to the current namespace
#pragma diag_suppress 1421	//  support for trigraphs is disabled
#pragma diag_suppress 1437	//  multicharacter character literal (potential portability problem)
#pragma diag_suppress 1669	//  Potential uninitialized reference to "blah" in function "bar" (nice but spurious)
#pragma diag_suppress 1787	//  nnn is not a valid value for the enumeration type "blah"
#pragma diag_suppress 237
#pragma diag_suppress 1774
#pragma diag_suppress 1783


