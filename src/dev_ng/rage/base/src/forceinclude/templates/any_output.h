#define __FINAL_LOGGING 0
#define __NO_OUTPUT	(__FINAL && !__FINAL_LOGGING)
#define RSG_OUTPUT	(!__NO_OUTPUT)

#if __NO_OUTPUT
#define OUTPUT_ONLY(...)
#if !RSG_PC
#define Quitf(...) __debugbreak()
#else
extern void QuitNoOutput(int errorCode);
#define Quitf(errorCode,...) do { (void)(__VA_ARGS__); QuitNoOutput(errorCode); } while(0)
#endif // !RSG_PC
#define Printf(...) do { } while (0)
#define Displayf(...) do { } while (0)
#define Warningf(...) do { } while (0)
#define Errorf(...) do { } while (0)
#define Debugf1(...) do { } while (0)
#define Debugf2(...) do { } while (0)
#define Debugf3(...) do { } while (0)
#else
#define OUTPUT_ONLY(...) __VA_ARGS__
#ifdef __cplusplus
extern void Printf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void Displayf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void Warningf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void Errorf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void Quitf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void Quitf(int errorCode,const char *fmt,...) PRINTF_LIKE_N(2);
extern void Debugf1(int level,const char *fmt,...) PRINTF_LIKE_N(2);
extern void Debugf2(int level,const char *fmt,...) PRINTF_LIKE_N(2);
extern void Debugf3(int level,const char *fmt,...) PRINTF_LIKE_N(2);
#endif
#endif	// __NO_OUTPUT
