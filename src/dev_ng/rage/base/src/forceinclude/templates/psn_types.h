#ifdef __SNC__
#pragma control notocrestore=2
#pragma control fastmath=1
#pragma control assumecorrectalignment=1
#pragma control assumecorrectsign=1
#pragma control relaxalias=0
#pragma control checknew=0
#if __SN_VER__ < 43001
#pragma control c+=rtti
#pragma control c-=exceptions
#endif // __SN_VER__ < 43001
#pragma control saverestorefuncs=1
#define _CPPRTTI 
#endif

#define I64FMT          "ll"
#define LI64FMT         L"ll"
#define SIZETFMT        "z"
#define PTRDIFFTFMT     "t"

// Networking
#define RSG_NP          1
#define RSG_XBL         0
#define RSG_GAMESPY     0

// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef vector unsigned int u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;

#ifdef __LP64__
typedef unsigned long u64;
typedef signed long s64;
#else
typedef unsigned long long u64;
typedef signed long long s64;
#endif

typedef float f32;
typedef u32 uptr;

END_NS_RAGE

// Platform definitions
#define __BE		1
#define __CONSOLE	1
#define __PS3		1
#define __PPU		1
#define __SPU		0
#define __WIN32		0
#define __OSX		0
#define __POSIX		0
#define __XENON		0
#define __PSP2		0
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define	RSG_BE			1
#define RSG_PS3			1
#define RSG_PPU			1
#define RSG_SPU			0
#define RSG_WIN32		0
#define RSG_XENON		0
#define RSG_PC			0
#define RSG_DURANGO		0
#define RSG_ORBIS		0
#define RSG_WINRT		0
#define RSG_XDK			0
#define RSG_GDK			0
#define RSG_LINUX		0
#define RSG_IOS			0
#define RSG_ANDROID		0
#define RSG_WINPHONE	0
#define RSG_MOBILE		(RSG_IOS | RSG_ANDROID | RSG_WINPHONE)
#define RSG_NX			0
#define RSG_P			0
#define RSG_GGP			0
#define RSG_OSX			0
#define RSG_SCE			0
#define RSG_XBOX		0
#define RSG_PROSPERO	0
#define RSG_SCARLETT	0

#define RSG_CPU_PPC	1
#define RSG_CPU_SPU	0
#define RSG_CPU_INTEL	0
#define RSG_CPU_X86	0
#define RSG_CPU_X64	0

#define CONSOLE_ONLY(...)   __VA_ARGS__
#define PS3_ONLY(...)       __VA_ARGS__
#define PPU_ONLY(...)       __VA_ARGS__
#define SPU_ONLY(...)
#define WIN32_ONLY(...)
#define XENON_ONLY(...)
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)
#define ORBIS_ONLY(...)		
#define PROSPERO_ONLY(...)
#define NP_ONLY(...)		
#define SCE_ONLY(...)		
#define DURANGO_ONLY(...)
#define WINRT_ONLY(...)
#define SCARLETT_ONLY(...)
#define XBL_ONLY(...)
#define XDK_ONLY(...)
#define GDK_ONLY(...)
#define XBOX_ONLY(...)

#define ALIGNAS(x)	// Not supported on PSN
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)	__attribute__((aligned(x)))
#define DEPRECATED	__attribute__((__deprecated__))
#define RESTRICT	__restrict
#if defined(__SNC__) && __SN_VER__ < 43001
#define MAY_ALIAS	__attribute__((__may_alias__))
#else
#define MAY_ALIAS
#endif
#define __forceinline	__attribute__((__always_inline__)) inline

// VC.Net-specific keyword, meaningless to us
#define __w64

#ifdef __SNC__
#define __debugbreak() __builtin_snpause()
#else
static inline void __debugbreak() { __asm__ volatile("tw 31,1,1"); }
#endif

#if defined(__SNC__)
#define PRAGMA_OPTIMIZE_OFF()	_Pragma("control O=0") // PRAGMA-OPTIMIZE-ALLOW
#else 
#define PRAGMA_OPTIMIZE_OFF()	// PRAGMA-OPTIMIZE-ALLOW _Pragma("option set \"-O0\"") Disabled as it crashes a couple of files
#endif

#ifdef __SNC__
#pragma diag_error 194		// zero used for undefined preprocessing identifier "xxxx"
#pragma diag_error 828		// entity-kind "entity" was never referenced
#pragma diag_suppress 613	// overloaded virtual function "blah" is only partially overridden in class "blahChild"
#pragma diag_suppress 739	//  using-declaration ignored -- it refers to the current namespace
#pragma diag_suppress 1421	//  support for trigraphs is disabled
#pragma diag_suppress 1437	//  multicharacter character literal (potential portability problem)
#pragma diag_suppress 1669	//  Potential uninitialized reference to "blah" in function "bar" (nice but spurious)
#if __SN_VER__ >= 33001
#pragma diag_suppress 1787	//  nnn is not a valid value for the enumeration type "blah"
#endif
#endif


