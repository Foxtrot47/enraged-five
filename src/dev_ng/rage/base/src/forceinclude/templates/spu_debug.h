// Build type definitions
#define __DEV		1
#define __OPTIMIZED	1		// SPECIAL CASE ON SPU DUE TO CODE SIZE
#define __BANK		1
#define __ASSERT	1
#define __FINAL		0
#define __EDITOR	0
#define __TOOL		0
#define __EXPORTER	0
#define	__PROFILE	0

#define PRAGMA_OPTIMIZE_OFF() // PRAGMA-OPTIMIZE-ALLOW
