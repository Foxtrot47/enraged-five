@echo off
setlocal
setlocal EnableDelayedExpansion

pushd %~dp0\..
p4 edit *.h
popd

call :make_header "windows_common.h" "win32rsc_types.h" "any_" "post_types.h" "any_output.h" "win32_rsc" "debug beta release" || goto :fail
call :make_header "windows_common.h" "win64rsc_types.h" "any_" "post_types.h" "any_output.h" "win64_rsc" "debug beta release" || goto :fail

call :make_header "windows_common.h" "win64gametool_types.h" "any_" "post_types.h" "any_output.h" "win64_gametool" "debug beta release" || goto :fail

call :make_header "windows_common.h" "win32pg_types.h" "any_" "post_types.h" "any_output.h" "win32_" "debug beta release bankrelease final" || goto :fail
call :make_header "windows_common.h" "win64pg_types.h" "any_" "post_types.h" "any_output.h" "win64_" "debug beta final master profile release bankrelease" || goto :fail

call :make_header "windows_common.h" "win32dx11_types.h" "any_" "post_types.h" "any_output.h" "win32_dx11_" "debug beta release bankrelease final" || goto :fail

call :make_header "windows_common.h" "win32_types.h" "any_tool" "post_types.h" "any_output.h" "win32_tool" "debug beta release" || goto :fail
call :make_header "windows_common.h" "win64_types.h" "any_tool" "post_types.h" "any_output.h" "win64_tool" "debug beta release" || goto :fail

call :make_header "" "durango_types.h" "any_" "post_types.h" "any_output.h" "durango_" "debug beta final master profile release bankrelease" || goto :fail

call :make_header "" "orbis_types.h" "any_" "post_types.h" "any_output.h" "orbis_" "debug beta final master profile release bankrelease" || goto :fail

call :make_header "" "xenon_types.h" "any_" "post_types.h" "any_output.h" "xenon_" "debug beta profile release bankrelease final" || goto :fail

call :make_header "" "psn_types.h" "any_" "post_types.h" "any_output.h" "psn_" "debug beta profile release bankrelease final" || goto :fail

call :make_header "" "spu_types.h" "spu_" "spu_post_types.h" "spu_output.h" "spu_psn_" "debug" || goto :fail
call :make_header "" "spu_types.h" "any_" "spu_post_types.h" "spu_output.h" "spu_psn_" "beta profile release bankrelease final" || goto :fail

:success
pushd %~dp0\..
p4 revert -a *.h
popd
exit /b 0

:fail
echo FAILED
exit /b 1

:make_header
setlocal
set common=%1
set type=%2
set any=%3
set post=%4
set any_out=%5
set out=%6
set configs=%7

set any=%any:"=%
set out=%out:"=%

if [%any%] == [] set any=any

set "pre=top_header_lines.h+version.h+any_types.h"

if not %common% == "" set "pre=%pre%+%common%"
set configs=%configs:"=%
for %%i in (%configs%) do (
	echo Process %type% %%i
	set "any=%any%%%i.h"
rem	if not "%5" == "" set "any=any%5%%i.h"
	REM /b flag is necessary to avoid a CTRL-Z at the end of the file.
	set includes=%pre%+%type%+!any!+%post%+%any_out%+..\hacks.h+bottom_header_lines.h
	call :validate_files !includes! || exit /b 1
	echo copy/b !includes! ..\%out%%%i.h
	copy/b !includes! ..\%out%%%i.h > nul || copy/b !includes! ..\%out%%%i.h && goto :fail
)
exit /b 0

:validate_files
setlocal
set files=%1
set files=%files:"=%
set files=%files:+= %
rem echo test %files%
for %%i in (%files%) do (
	if not exist %%i (
		echo MISSING %%i
		exit /b 1
	)
)
exit /b 0




