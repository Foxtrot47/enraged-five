// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned __int64 u64;
typedef struct __declspec(intrin_type) __declspec(align(16)) __u128 {
   float m128_f32[4];
} __u128;
typedef __u128 u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed __int64 s64;

typedef float f32;
#if defined(_M_X64)
typedef u64 uptr;
#else
typedef u32 uptr;
#endif

END_NS_RAGE

#define GTA_REPLAY_RAGE

// Platform definitions
#define __BE		0
#define __CONSOLE	0
#define __PS3		0
#define __PPU		0
#define __SPU		0
#define __WIN32		1
#define __OSX		0
#define __POSIX		0
#define __XENON		0
#define __PSP2		0
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	1
#define __PAGING	0
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define RSG_BE          0
#define RSG_PS3         0
#define RSG_PPU         0
#define RSG_SPU         0
#define RSG_WIN32       1
#define RSG_XENON       0
#define RSG_PC          1
#define RSG_DURANGO     0
#define RSG_ORBIS       0
#define RSG_WINRT		0
#define RSG_XDK			0
#define RSG_GDK			0
#define RSG_LINUX		0
#define RSG_IOS			0
#define RSG_ANDROID		0
#define RSG_WINPHONE	0
#define RSG_MOBILE		(RSG_IOS | RSG_ANDROID | RSG_WINPHONE)
#define RSG_NX			0
#define RSG_P			0
#define RSG_GGP			0
#define RSG_OSX			0
#define RSG_SCE			0
#define RSG_XBOX		0
#define RSG_PROSPERO	0
#define RSG_SCARLETT	0

#define RSG_CPU_PPC     0
#define RSG_CPU_SPU     0
#define RSG_CPU_INTEL   1
#define RSG_CPU_X86     1
#define RSG_CPU_X64     0

#define CONSOLE_ONLY(...)
#define PS3_ONLY(...)
#define PPU_ONLY(...)
#define SPU_ONLY(...)
#define WIN32_ONLY(...)     __VA_ARGS__
#define XENON_ONLY(...)
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)   __VA_ARGS__
#define ORBIS_ONLY(...)		
#define PROSPERO_ONLY(...)
#define NP_ONLY(...)		
#define SCE_ONLY(...)		
#define DURANGO_ONLY(...)	
#define WINRT_ONLY(...)		
#define SCARLETT_ONLY(...)
#define XBL_ONLY(...)		
#define XDK_ONLY(...)		
#define GDK_ONLY(...)
#define XBOX_ONLY(...)		

#define ALIGNAS(x)			__declspec(align(x))
#define BEGIN_ALIGNED(x)	__declspec(align(x))
#define ALIGNED(x)
#define DEPRECATED			__declspec(deprecated)
#define RESTRICT			__restrict
#define MAY_ALIAS

#pragma warning(disable: 4127)
#pragma warning(disable: 4200)
#pragma warning(disable: 4291)
#pragma warning(disable: 4512)
#pragma warning(disable: 4530)
#pragma warning(disable: 4505)
#pragma warning(disable: 4710)
#pragma warning(error: 4062)
#pragma warning(error: 4263)
#pragma warning(error: 4265)
#pragma warning(error: 4668)
#define _CRT_SECURE_NO_DEPRECATE
#define _SCL_SECURE_NO_WARNINGS

#define __builtin_trap()		__debugbreak()

#define PRAGMA_OPTIMIZE_OFF()	__pragma(optimize("", off))	// PRAGMA-OPTIMIZE-ALLOW

