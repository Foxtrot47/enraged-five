// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
// typedef unsigned int u128 __attribute__ ((mode (TI)));

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;

typedef float f32;

END_NS_RAGE

// Platform definitions
#define __BE		1
#define __CONSOLE	0
#define __PS2		0
#define __SPU		0
#define __WIN32		0
#define __OSX		1
#define __POSIX		1
#define __XBOX		0
#define __XENON		0
#define __GCUBE		0
#define __PSP		0
#define __CAFE		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __GAMETOOL	0
#define __RESOURCECOMPILER	0

#define SPU_ONLY(...)
#define WIN32_ONLY(...)
#define OSX_ONLY(...)	    __VA_ARGS__
#define XENON_ONLY(...)
#define PSP2_ONLY(...)
#define CAFE_ONLY(...)
#define LINUX_ONLY(...)
#define WIN32PC_ONLY(...)

#define ALIGNAS(x)	alignas(x)
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)
#define DEPRECATED
#define RESTRICT
#define MAY_ALIAS	__attribute__((__may_alias__))
#define __forceinline inline

// VC.Net-specific keyword, meaningless to us
#define __w64

inline void __debugbreak() { *(int*)0 = 0; }

