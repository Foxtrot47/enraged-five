TITLE Audio
	MODULE audioasiolib
	MODULE audioeffecttypes
	MODULE audioengine
	MODULE audiohardware
	MODULE audioscriptsoundtypes
	MODULE audiosoundtypes
ENDTITLE	
	
TITLE Core
	MODULE atl
	MODULE bank
	MODULE data
	MODULE diag
	MODULE file
	MODULE forceinclude
	MODULE math
	MODULE mathext
	MODULE paging
	MODULE parser
	MODULE parsercore
	MODULE profile
	MODULE qa
	MODULE string
	MODULE system
	MODULE vector
	MODULE vectormath
	MODULE zlib
ENDTITLE

TITLE Creature
	MODULE cranimation
	MODULE crbody
	MODULE creature
ENDTITLE
	
TITLE Graphics
	MODULE crskeleton
	MODULE edge
	MODULE grblendshapes
	MODULE grcore
	MODULE grpostfx
	MODULE grmodel
	MODULE input
	MODULE jpeg
	MODULE mesh
	MODULE rageShaderMaterial
	MODULE rmcore
	MODULE shaderlib
ENDTITLE
		
TITLE Graphics2
	MODULE flash
	MODULE grshadowmap
	MODULE ptcore
	MODULE rmdraw
	MODULE rmportal
	MODULE rmocclude
	MODULE spatialdata
	MODULE text
ENDTITLE
		
TITLE Net
	MODULE gamespy
	MODULE net
	MODULE rline
ENDTITLE
	
TITLE OldAudio
	MODULE audiocommand
	MODULE audiocontrol
	MODULE audiodriver
ENDTITLE
		
TITLE Physics
	MODULE curve
	MODULE physics
	MODULE pharticulated
	MODULE phbound
	MODULE phbullet
	MODULE phcore
	MODULE pheffects
	MODULE phspurs
ENDTITLE

TITLE Miscellaneous
	MODULE devcam
	MODULE geo
	MODULE gizmo
	MODULE grcustom
	MODULE idmgr
	MODULE init
	MODULE obsolete
	MODULE rpc
	MODULE shaders
	MODULE xmldata
ENDTITLE