3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$@@Relationships$DTX_SECTION_NOT_FILLED_IN
3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Working$POSSIBLE_STRAY_TOPIC
1$grcRageLightImplementation$SYMBOL_NO_DESC
    2$grcRageLightImplementation::GetName$SYMBOL_NO_DESC
    2$grcRageLightImplementation::Init$SYMBOL_NO_DESC
    2$grcRageLightImplementation::SetLightingGroup@const grcLightGroup &$SYMBOL_NO_DESC
1$grcRdr2LightImplementation$SYMBOL_NO_DESC
    2$grcRdr2LightImplementation::GetName$SYMBOL_NO_DESC
    2$grcRdr2LightImplementation::Init$SYMBOL_NO_DESC
    2$grcRdr2LightImplementation::SetLightingGroup@const grcLightGroup &$SYMBOL_NO_DESC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::grcCustomLightImplementation::SetLightingGroup@const grcLightGroup &$FUNC_PARAM_NO_DESC
    2$rage::grcCustomLighting::RegisterInstance@grcCustomLightImplementation &$FUNC_PARAM_NO_DESC
    2$rage::grcCustomLighting::SetLightingGroup@const grcLightGroup &$FUNC_PARAM_NO_DESC
    2$rage::grcCustomLighting::SetLightingModel@const char*$FUNC_PARAM_NO_DESC
    2$rage::grcCustomLighting::UnregisterInstance@grcCustomLightImplementation &$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::grcCustomLightImplementation::SetLightingGroup@const grcLightGroup &$FUNC_PARAM_NO_NAME
    2$rage::grcCustomLighting::RegisterInstance@grcCustomLightImplementation &$FUNC_PARAM_NO_NAME
    2$rage::grcCustomLighting::SetLightingGroup@const grcLightGroup &$FUNC_PARAM_NO_NAME
    2$rage::grcCustomLighting::SetLightingModel@const char*$FUNC_PARAM_NO_NAME
    2$rage::grcCustomLighting::UnregisterInstance@grcCustomLightImplementation &$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::grcCustomLightImplementation::~grcCustomLightImplementation$FUNC_RETURN_VAL_NO_DESC
    2$rage::grcCustomLightImplementation::SetLightingGroup@const grcLightGroup &$FUNC_RETURN_VAL_NO_DESC
    2$rage::grcCustomLightImplementation::Shutdown$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::grcCustomLightImplementation::SetInitSuccessful@bool$SYMBOL_NO_DESC
    2$rage::grcCustomLightImplementation::WasInitSuccessful@const$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::~grcCustomLighting$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::AddWidgets$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::GetInstance$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::grcCustomLighting$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::InitClass$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::IsInstantiated$SYMBOL_NO_DESC
    2$rage::grcCustomLighting::ShutdownClass$SYMBOL_NO_DESC
3$_GRCUSTOM_CUSTOMLIGHTING_RAGE_H_$SYMBOL_NO_DESC
3$_GRCUSTOM_CUSTOMLIGHTING_RDR2_H_$SYMBOL_NO_DESC
3$_GRCUSTOMLIGHTING_H_$SYMBOL_NO_DESC
3$DIR_LIGHT_COUNT$SYMBOL_NO_DESC
3$new$SYMBOL_NO_DESC
3$POINT_LIGHT_COUNT$SYMBOL_NO_DESC
3$Rdr2PointLight$SYMBOL_NO_DESC
