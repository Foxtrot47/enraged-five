3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::FindInverseBilinear@float@float@float@float@float@float@float@float@float@float@float &@float &$FUNC_PARAM_NO_DESC
    2$rage::Float16::!=@const Float16 &@const$FUNC_PARAM_NO_DESC
    2$rage::Float16::=@float$FUNC_PARAM_NO_DESC
    2$rage::Float16::==@const Float16 &@const$FUNC_PARAM_NO_DESC
    2$rage::Float16::Float16@const Float16 &$FUNC_PARAM_NO_DESC
    2$rage::Float16::Float16@float$FUNC_PARAM_NO_DESC
    2$rage::Float16::Set@float$FUNC_PARAM_NO_DESC
    2$rage::g_ReplayRand@1@true$FUNC_PARAM_NO_DESC
    2$rage::invsqrtf_fast@float$FUNC_PARAM_NO_DESC
    2$rage::MathErr@_exception *$FUNC_PARAM_NO_DESC
    2$rage::mthRandom::mthRandom@int@bool$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::EnableNanSignal@bool$FUNC_PARAM_NO_NAME
    2$rage::FindInverseBilinear@float@float@float@float@float@float@float@float@float@float@float &@float &$FUNC_PARAM_NO_NAME
    2$rage::Float16::!=@const Float16 &@const$FUNC_PARAM_NO_NAME
    2$rage::Float16::=@float$FUNC_PARAM_NO_NAME
    2$rage::Float16::==@const Float16 &@const$FUNC_PARAM_NO_NAME
    2$rage::Float16::Float16@const Float16 &$FUNC_PARAM_NO_NAME
    2$rage::Float16::Float16@float$FUNC_PARAM_NO_NAME
    2$rage::Float16::Set@float$FUNC_PARAM_NO_NAME
    2$rage::g_ReplayRand@1@true$FUNC_PARAM_NO_NAME
    2$rage::invsqrtf_fast@float$FUNC_PARAM_NO_NAME
    2$rage::MathErr@_exception *$FUNC_PARAM_NO_NAME
    2$rage::mthRandom::mthRandom@int@bool$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::Float16::!=@const Float16 &@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::Float16::=@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::Float16::==@const Float16 &@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::Float16::Get@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::Float16::GetBoundingExponent@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::Float16::Release@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::g_ReplayRand@1@true$FUNC_RETURN_VAL_NO_DESC
    2$rage::invsqrtf_fast@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::MathErr@_exception *$FUNC_RETURN_VAL_NO_DESC
    2$rage::mthRandom::Reset@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::mthRandom::SetFullSeed@u64$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::__INIT_NAN$SYMBOL_NO_DESC
    2$rage::Float16$SYMBOL_NO_DESC
    2$rage::Float16::DeclareStruct@datTypeStruct &$SYMBOL_NO_DESC
    2$rage::Float16::enum@1::RORC_VERSION$SYMBOL_NO_DESC
    2$rage::Float16::Float16@datResource& UNUSED_PARAMrsc$SYMBOL_NO_DESC
    2$rage::Float16::GetData@const$SYMBOL_NO_DESC
    2$rage::Float16::Place@datResource &$SYMBOL_NO_DESC
    2$rage::Float16_Max$SYMBOL_NO_DESC
    2$rage::Float16_Min$SYMBOL_NO_DESC
    2$rage::Fpmax$SYMBOL_NO_DESC
    2$rage::Fpmin$SYMBOL_NO_DESC
    2$rage::M_E$SYMBOL_NO_DESC
    2$rage::M_SQRT1_2$SYMBOL_NO_DESC
    2$rage::M_SQRT2$SYMBOL_NO_DESC
    2$rage::M_SQRT3$SYMBOL_NO_DESC
    2$rage::mthRandom::SetReplayDebug@const bool$SYMBOL_NO_DESC
    2$rage::PI$SYMBOL_NO_DESC
    2$rage::s_InvSqrtSeed$SYMBOL_NO_DESC
3$__lvlx$SYMBOL_NO_DESC
3$__lvrx$SYMBOL_NO_DESC
3$__stvlx$SYMBOL_NO_DESC
3$__stvrx$SYMBOL_NO_DESC
3$__vaddfp$SYMBOL_NO_DESC
3$__vand$SYMBOL_NO_DESC
3$__vcfsx$SYMBOL_NO_DESC
3$__vcmpbfp$SYMBOL_NO_DESC
3$__vcmpeqfp$SYMBOL_NO_DESC
3$__vcmpequw$SYMBOL_NO_DESC
3$__vcmpgefp$SYMBOL_NO_DESC
3$__vcmpgtfp$SYMBOL_NO_DESC
3$__vcsxwfp$SYMBOL_NO_DESC
3$__vctsxs$SYMBOL_NO_DESC
3$__vector4$SYMBOL_NO_DESC
3$__vexptefp$SYMBOL_NO_DESC
3$__vlogefp$SYMBOL_NO_DESC
3$__vmaddfp$SYMBOL_NO_DESC
3$__vmaxfp$SYMBOL_NO_DESC
3$__vminfp$SYMBOL_NO_DESC
3$__vmrghw$SYMBOL_NO_DESC
3$__vmrglw$SYMBOL_NO_DESC
3$__vmulfp$SYMBOL_NO_DESC
3$__vnmsubfp$SYMBOL_NO_DESC
3$__vor$SYMBOL_NO_DESC
3$__vpkuwum$SYMBOL_NO_DESC
3$__vrefp$SYMBOL_NO_DESC
3$__vrfin$SYMBOL_NO_DESC
3$__vrfip$SYMBOL_NO_DESC
3$__vrfiz$SYMBOL_NO_DESC
3$__vrsqrtefp$SYMBOL_NO_DESC
3$__vsel$SYMBOL_NO_DESC
3$__vsldoi$SYMBOL_NO_DESC
3$__vspltw$SYMBOL_NO_DESC
3$__vsubfp$SYMBOL_NO_DESC
3$__vupkhsb$SYMBOL_NO_DESC
3$__vupkhsh$SYMBOL_NO_DESC
3$__vxor$SYMBOL_NO_DESC
3$_CountLeadingZeros$SYMBOL_NO_DESC
3$_CountLeadingZeros64$SYMBOL_NO_DESC
3$_cvector4$SYMBOL_NO_DESC
3$_hvector4$SYMBOL_NO_DESC
3$_uvector4$SYMBOL_NO_DESC
3$_vall1$SYMBOL_NO_DESC
3$_vequalfp$SYMBOL_NO_DESC
3$_vgequalfp$SYMBOL_NO_DESC
3$_vgreaterfp$SYMBOL_NO_DESC
3$_vlequalfp$SYMBOL_NO_DESC
3$_vlessfp$SYMBOL_NO_DESC
3$_vmuleq$SYMBOL_NO_DESC
3$_vnegeq$SYMBOL_NO_DESC
3$_vscalar$SYMBOL_NO_DESC
3$_vsplatf$SYMBOL_NO_DESC
3$SINPIDIV8$SYMBOL_NO_DESC
3$SMALLEST_SQUARE$SYMBOL_NO_DESC
3$SQRT3INVERSE$SYMBOL_NO_DESC
3$VEC_PERM_W$SYMBOL_NO_DESC
3$VEC_PERM_Y$SYMBOL_NO_DESC
3$VEC_PERM_Z$SYMBOL_NO_DESC
3$VERY_SMALL_FLOAT$SYMBOL_NO_DESC
