3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Command$POSSIBLE_STRAY_TOPIC
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::dcamCam::Draw@int@int@float@const$FUNC_PARAM_NO_DESC
    2$rage::dcamCam::DrawCameraName@int@int@float@const$FUNC_PARAM_NO_DESC
    2$rage::dcamCam::SetRequiresKey@bool$FUNC_PARAM_NO_DESC
    2$rage::dcamCam::SetWorldMtx@const Matrix34&$FUNC_PARAM_NO_DESC
    2$rage::dcamCamMgr::Frame@const spdSphere&$FUNC_PARAM_NO_DESC
    2$rage::dcamCamMgr::SetWorldMtx@const Matrix34&$FUNC_PARAM_NO_DESC
    2$rage::dcamFlyCam::Frame@const spdSphere&$FUNC_PARAM_NO_DESC
    2$rage::dcamLhCam::Init@float@float@float@Vector3::Vector3Param$FUNC_PARAM_NO_DESC
    2$rage::dcamMayaCam::Draw@int@int@float@const$FUNC_PARAM_NO_DESC
    2$rage::dcamMayaCam::SetAltZoom@bool$FUNC_PARAM_NO_DESC
    2$rage::dcamMayaCam::SetMaxZoom@float$FUNC_PARAM_NO_DESC
    2$rage::dcamPolarCam::Init@float@float@float@Vector3::Vector3Param$FUNC_PARAM_NO_DESC
    2$rage::dcamQuakeCam::Draw@int@int@float@const$FUNC_PARAM_NO_DESC
    2$rage::dcamRoamCam::Frame@const spdSphere&$FUNC_PARAM_NO_DESC
    2$rage::dcamTrackCam::Frame@const spdSphere&$FUNC_PARAM_NO_DESC
    2$rage::trk::ProjectToSphere@float@float@float$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::dcamCam::AddWidgets@class bkBank &$FUNC_PARAM_NO_NAME
    2$rage::dcamCam::SetWorldMtx@const Matrix34&$FUNC_PARAM_NO_NAME
    2$rage::dcamCamMgr::Frame@const spdSphere&$FUNC_PARAM_NO_NAME
    2$rage::dcamCamMgr::SetWorldMtx@const Matrix34&$FUNC_PARAM_NO_NAME
    2$rage::dcamFlyCam::Frame@const spdSphere&$FUNC_PARAM_NO_NAME
    2$rage::dcamMayaCam::SetMaxZoom@float$FUNC_PARAM_NO_NAME
    2$rage::dcamRoamCam::Frame@const spdSphere&$FUNC_PARAM_NO_NAME
    2$rage::dcamTrackCam::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::dcamTrackCam::Frame@const spdSphere&$FUNC_PARAM_NO_NAME
    2$rage::trk::BuildRotMatrix@Matrix34&@float q[4]$FUNC_PARAM_NO_NAME
    2$rage::trk::ProjectToSphere@float@float@float$FUNC_PARAM_NO_NAME
    2$rage::trk::TrackBall@float qOut[4]@float@float@float@float$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::~dcamCam$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Activated$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::AddWidgets@class bkBank &$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Deactivated$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Draw@int@int@float@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::DrawCameraName@int@int@float@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Frame@const spdSphere&$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Init@const Vector3 &@const Vector3 &$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::SetRequiresKey@bool$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::SetUseZUp@bool$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::SetWorldMtx@const Matrix34&$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCam::Update@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCamMgr::GetCurrentCamera@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCamMgr::GetInfoFadeTime@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCamMgr::SetUseZUp@bool$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamCamMgr::SetWorldMtx@const Matrix34&$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamFlyCam::Frame@const spdSphere&$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamFreeCam::SetSpeed@float@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamLhCam::AddWidgets@bkBank &$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamMayaCam::GetFocusDistance$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamPolarCam::AddWidgets@bkBank &$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamRoamCam::AddWidgets@bkBank &$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamRoamCam::Frame@const spdSphere&$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamTrackCam::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::dcamTrackCam::Frame@const spdSphere&$FUNC_RETURN_VAL_NO_DESC
    2$rage::trk::ProjectToSphere@float@float@float$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_COUNT$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_FLY$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_FREE$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_LH$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_MAYA$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_POLAR$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_QUAKE$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_ROAM$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_TRACK$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::eCamera::CAM_WAR$SYMBOL_NO_DESC
    2$rage::dcamCamMgr::SetEnableDrawHelp@bool$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::SetWorldMtx@const Matrix34&$SYMBOL_NO_DESC
    2$rage::dcamFlyCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::Frame@const spdSphere&$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamFreeCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamFreeCam2::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamFreeCam2::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamFreeCam2::Update@float$SYMBOL_NO_DESC
    2$rage::dcamLhCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamLhCam::Frame@const spdSphere&$SYMBOL_NO_DESC
    2$rage::dcamLhCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamLhCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamLhCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamLhCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamLhCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamLhCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::AddWidgets@class bkBank &$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::Frame@const spdSphere&$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamMayaCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::Frame@const spdSphere&$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamPolarCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::AddWidgets@class bkBank &$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::Frame@const spdSphere&$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamQuakeCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::GetWorldMtx$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamRoamCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamTrackCam::Update@float$SYMBOL_NO_DESC
    2$rage::dcamWarCam::Draw@int@int@float@const$SYMBOL_NO_DESC
    2$rage::dcamWarCam::Frame@const spdSphere&$SYMBOL_NO_DESC
    2$rage::dcamWarCam::GetName@const$SYMBOL_NO_DESC
    2$rage::dcamWarCam::GetWorldMtx@const$SYMBOL_NO_DESC
    2$rage::dcamWarCam::Init@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rage::dcamWarCam::Reset$SYMBOL_NO_DESC
    2$rage::dcamWarCam::SetWorldMtx@const Matrix34 &$SYMBOL_NO_DESC
    2$rage::dcamWarCam::Update@float$SYMBOL_NO_DESC
    2$rage::trk$SYMBOL_NO_DESC
3$fMaxAcceleration$SYMBOL_NO_DESC
3$fPitchDeadzone$SYMBOL_NO_DESC
3$fPitchRate$SYMBOL_NO_DESC
3$fSideSpeed$SYMBOL_NO_DESC
3$fVerticalSpeed$SYMBOL_NO_DESC
3$fYawDeadzone$SYMBOL_NO_DESC
3$s_RotateSpeed$SYMBOL_NO_DESC
3$s_TranslateSpeed$SYMBOL_NO_DESC
