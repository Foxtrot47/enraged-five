3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$@@Relationships$DTX_SECTION_NOT_FILLED_IN
3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::audAssertSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audAssertSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audAssertSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audAssertSound::Init@const void *@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audForLoopSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audForLoopSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audForLoopSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audForLoopSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audIfSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audIfSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audIfSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audIfSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audMathOperationSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audMathOperationSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audMathOperationSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audMathOperationSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audSwitchSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audSwitchSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audSwitchSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audSwitchSound::Init@const void *@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audVariableBlockSound::_GetVariableAddressUpHierarchy@const u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableBlockSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableBlockSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableBlockSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audVariableBlockSound::GetVariableAddressDownHierarchy@const u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableBlockSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audVariableCurveSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableCurveSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableCurveSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audVariableCurveSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audVariablePrintValueSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariablePrintValueSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariablePrintValueSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audVariablePrintValueSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
    2$rage::audVariableSetTimeSound::AudioPlay@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableSetTimeSound::AudioUpdate@u32$FUNC_PARAM_NO_DESC
    2$rage::audVariableSetTimeSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_DESC
    2$rage::audVariableSetTimeSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::audAssertSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audAssertSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audAssertSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audAssertSound::Init@const void *@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audForLoopSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audForLoopSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audForLoopSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audForLoopSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audIfSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audIfSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audIfSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audIfSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audMathOperationSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audMathOperationSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audMathOperationSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audMathOperationSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audSwitchSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audSwitchSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audSwitchSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audSwitchSound::Init@const void *@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audVariableBlockSound::_GetVariableAddressUpHierarchy@const u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableBlockSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableBlockSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableBlockSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audVariableBlockSound::GetVariableAddressDownHierarchy@const u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableBlockSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audVariableCurveSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableCurveSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableCurveSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audVariableCurveSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audVariablePrintValueSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariablePrintValueSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariablePrintValueSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audVariablePrintValueSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
    2$rage::audVariableSetTimeSound::AudioPlay@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableSetTimeSound::AudioUpdate@u32$FUNC_PARAM_NO_NAME
    2$rage::audVariableSetTimeSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_PARAM_NO_NAME
    2$rage::audVariableSetTimeSound::Init@const void*@const audSoundInternalInitParams*$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::audAssertSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audAssertSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audAssertSound::Init@const void *@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audForLoopSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audForLoopSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audForLoopSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audIfSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audIfSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audIfSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMathOperationSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMathOperationSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMathOperationSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audSwitchSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audSwitchSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audSwitchSound::Init@const void *@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableBlockSound::_GetVariableAddressUpHierarchy@const u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableBlockSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableBlockSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableBlockSound::GetVariableAddressDownHierarchy@const u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableBlockSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableCurveSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableCurveSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableCurveSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariablePrintValueSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariablePrintValueSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariablePrintValueSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableSetTimeSound::AudioUpdate@u32$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableSetTimeSound::ComputeDurationMsExcludingStartOffsetAndPredelay@bool*$FUNC_RETURN_VAL_NO_DESC
    2$rage::audVariableSetTimeSound::Init@const void*@const audSoundInternalInitParams*$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audAssertSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audForLoopSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audIfSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audMathOperationSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audSwitchSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audVariableBlockSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audVariableCurveSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audVariablePrintValueSound$SYMBOL_NO_DESC
    2$rage::AUD_IMPLEMENT_STATIC_WRAPPERS@audVariableSetTimeSound$SYMBOL_NO_DESC
    2$rage::audAssertSound::~audAssertSound$SYMBOL_NO_DESC
    2$rage::audAssertSound::audAssertSound$SYMBOL_NO_DESC
    2$rage::audForLoopSound::~audForLoopSound$SYMBOL_NO_DESC
    2$rage::audForLoopSound::audForLoopSound$SYMBOL_NO_DESC
    2$rage::audForLoopSound::ChildSoundCallback@const u32@const u32$SYMBOL_NO_DESC
    2$rage::audIfSound::~audIfSound$SYMBOL_NO_DESC
    2$rage::audIfSound::audIfSound$SYMBOL_NO_DESC
    2$rage::audMathOp$SYMBOL_NO_DESC
    2$rage::audMathOp::isParam1Immediate$SYMBOL_NO_DESC
    2$rage::audMathOp::isParam2Immediate$SYMBOL_NO_DESC
    2$rage::audMathOp::isParam3Immediate$SYMBOL_NO_DESC
    2$rage::audMathOp::op$SYMBOL_NO_DESC
    2$rage::audMathOp::param1$SYMBOL_NO_DESC
    2$rage::audMathOp::param1@1::immediate$SYMBOL_NO_DESC
    2$rage::audMathOp::param1@1::var$SYMBOL_NO_DESC
    2$rage::audMathOp::param2$SYMBOL_NO_DESC
    2$rage::audMathOp::param2@1::immediate$SYMBOL_NO_DESC
    2$rage::audMathOp::param2@1::var$SYMBOL_NO_DESC
    2$rage::audMathOp::param3$SYMBOL_NO_DESC
    2$rage::audMathOp::param3@1::immediate$SYMBOL_NO_DESC
    2$rage::audMathOp::param3@1::var$SYMBOL_NO_DESC
    2$rage::audMathOp::result$SYMBOL_NO_DESC
    2$rage::audMathOperationSound$SYMBOL_NO_DESC
    2$rage::audMathOperationSound::~audMathOperationSound$SYMBOL_NO_DESC
    2$rage::audMathOperationSound::audMathOperationSound$SYMBOL_NO_DESC
    2$rage::audSwitchSound::~audSwitchSound$SYMBOL_NO_DESC
    2$rage::audSwitchSound::audSwitchSound$SYMBOL_NO_DESC
    2$rage::audSwitchSound::ChildSoundCallback@const u32@const u32$SYMBOL_NO_DESC
    2$rage::audVariableBlockSound::~audVariableBlockSound$SYMBOL_NO_DESC
    2$rage::audVariableBlockSound::audVariableBlockSound$SYMBOL_NO_DESC
    2$rage::audVariableCurveSound::~audVariableCurveSound$SYMBOL_NO_DESC
    2$rage::audVariableCurveSound::audVariableCurveSound$SYMBOL_NO_DESC
    2$rage::audVariablePrintValueSound::~audVariablePrintValueSound$SYMBOL_NO_DESC
    2$rage::audVariablePrintValueSound::audVariablePrintValueSound$SYMBOL_NO_DESC
    2$rage::audVariableSetTimeSound::~audVariableSetTimeSound$SYMBOL_NO_DESC
    2$rage::audVariableSetTimeSound::audVariableSetTimeSound$SYMBOL_NO_DESC
    2$rage::enum@1::AUD_IF_BRANCH_FALSE$SYMBOL_NO_DESC
    2$rage::enum@1::AUD_IF_BRANCH_NEITHER$SYMBOL_NO_DESC
    2$rage::enum@1::AUD_IF_BRANCH_TRUE$SYMBOL_NO_DESC
    2$rage::IMPLEMENT_OPERATION$SYMBOL_NO_DESC
3$AUD_ASSERT_SOUND$SYMBOL_NO_DESC
