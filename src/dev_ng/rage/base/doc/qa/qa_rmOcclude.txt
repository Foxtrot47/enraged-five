3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$rmOcclude$POSSIBLE_STRAY_TOPIC
3$rmPvsCullModeBits::pvscm_ExcludeObjectsClippingOccluderBackfaces$POSSIBLE_STRAY_TOPIC
3$rmPvsCullModeBits::pvscm_ExcludeObjectsClippingOccluderFrontfaces$POSSIBLE_STRAY_TOPIC
3$rmPvsCullModeBits::pvscm_ExcludeObjectsContainedInOccluder$POSSIBLE_STRAY_TOPIC
3$Command$POSSIBLE_STRAY_TOPIC
3$Working$POSSIBLE_STRAY_TOPIC
3$fSpan_s::next$SYMBOL_NO_DESC
1$mcActiveOccluderPoly$FUNC_PARAM_NO_DESC
    2$mcActiveOccluderPoly::SetActiveMode@bool$FUNC_PARAM_NO_DESC
    2$mcActiveOccluderPoly::StoreDebugDrawInfo@rmPvsDebugDrawModeBits ASSERT_ONLYmodebits@pvsVtxClipFlags *@pvsFaceClipFlags *@pvsEdgeData *@pvsEdgeData *$FUNC_PARAM_NO_DESC
1$mcActiveOccluderPoly$FUNC_PARAM_NO_NAME
    2$mcActiveOccluderPoly::SetActiveMode@bool$FUNC_PARAM_NO_NAME
    2$mcActiveOccluderPoly::StoreDebugDrawInfo@rmPvsDebugDrawModeBits ASSERT_ONLYmodebits@pvsVtxClipFlags *@pvsFaceClipFlags *@pvsEdgeData *@pvsEdgeData *$FUNC_PARAM_NO_NAME
1$mcActiveOccluderPoly$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::GetDebug_PvsEdgeData@void$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::GetDebug_PvsReverseEdgeData@void$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::GetDebug_WasPvsEdgeRemoved@int@int$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::IsActive@void$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::IsFaceVisible@int$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::IsVertexVisible@int$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::m_occlusionPlanes$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::m_silh_x$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::m_silh_y$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::m_type$SYMBOL_NO_DESC
    2$mcActiveOccluderPoly::num_silh_verts$SYMBOL_NO_DESC
1$OccludedBoundInfo$SYMBOL_NO_DESC
    2$OccludedBoundInfo::friend class rmOccluderSystem$SYMBOL_NO_DESC
    2$OccludedBoundInfo::GetType@void$SYMBOL_NO_DESC
    2$OccludedBoundInfo::GetViewerID@void$SYMBOL_NO_DESC
    2$OccludedBoundInfo::Render@void$SYMBOL_NO_DESC
1$occluderCullModeBits$SYMBOL_NO_DESC
    2$occluderCullModeBits::ocmb_geomTestRead$SYMBOL_NO_DESC
    2$occluderCullModeBits::ocmb_mask$SYMBOL_NO_DESC
    2$occluderCullModeBits::ocmb_spanTestRead$SYMBOL_NO_DESC
    2$occluderCullModeBits::ocmb_spanTestReadWrite$SYMBOL_NO_DESC
1$PvsViewer$FUNC_PARAM_NO_DESC
    2$PvsViewer::GetCullMtx0@void$FUNC_PARAM_NO_DESC
    2$PvsViewer::GetCullMtx1@void$FUNC_PARAM_NO_DESC
    2$PvsViewer::GetPvsCullMtx0@void$FUNC_PARAM_NO_DESC
    2$PvsViewer::GetPvsCullMtx1@void$FUNC_PARAM_NO_DESC
    2$PvsViewer::PvsViewer@void$FUNC_PARAM_NO_DESC
    2$PvsViewer::SphereFrustumCheck_ClipStatus@const Vector4 &$FUNC_PARAM_NO_DESC
1$PvsViewer$FUNC_PARAM_NO_NAME
    2$PvsViewer::GetCullMtx0@void$FUNC_PARAM_NO_NAME
    2$PvsViewer::GetCullMtx1@void$FUNC_PARAM_NO_NAME
    2$PvsViewer::GetPvsCullMtx0@void$FUNC_PARAM_NO_NAME
    2$PvsViewer::GetPvsCullMtx1@void$FUNC_PARAM_NO_NAME
    2$PvsViewer::PvsViewer@void$FUNC_PARAM_NO_NAME
    2$PvsViewer::SphereFrustumCheck_ClipStatus@const Vector4 &$FUNC_PARAM_NO_NAME
1$PvsViewer$FUNC_RETURN_VAL_NO_DESC
    2$PvsViewer::GetCullMtx0@void$FUNC_RETURN_VAL_NO_DESC
    2$PvsViewer::GetCullMtx1@void$FUNC_RETURN_VAL_NO_DESC
    2$PvsViewer::GetPvsCullMtx0@void$FUNC_RETURN_VAL_NO_DESC
    2$PvsViewer::GetPvsCullMtx1@void$FUNC_RETURN_VAL_NO_DESC
    2$PvsViewer::SphereFrustumCheck_ClipStatus@const Vector4 &$FUNC_RETURN_VAL_NO_DESC
1$PvsViewer$SYMBOL_NO_DESC
    2$PvsViewer::ActiveMode@bool$SYMBOL_NO_DESC
    2$PvsViewer::Get_AspectRatio$SYMBOL_NO_DESC
    2$PvsViewer::Get_cosHFOV$SYMBOL_NO_DESC
    2$PvsViewer::Get_cosVFOV$SYMBOL_NO_DESC
    2$PvsViewer::Get_fovy$SYMBOL_NO_DESC
    2$PvsViewer::Get_sinHFOV$SYMBOL_NO_DESC
    2$PvsViewer::Get_sinVFOV$SYMBOL_NO_DESC
    2$PvsViewer::Get_tanFovy$SYMBOL_NO_DESC
    2$PvsViewer::Get_ZClipFar$SYMBOL_NO_DESC
    2$PvsViewer::Get_ZClipNear$SYMBOL_NO_DESC
    2$PvsViewer::GetCullingModeBits@void$SYMBOL_NO_DESC
    2$PvsViewer::GetMatrix@const rmPvsViewType@const rmPvsMatrixType$SYMBOL_NO_DESC
    2$PvsViewer::GetNumActiveOccluders@void$SYMBOL_NO_DESC
    2$PvsViewer::GetOrthoViewport_pvs@void$SYMBOL_NO_DESC
    2$PvsViewer::GetOrthoViewport_render@void$SYMBOL_NO_DESC
    2$PvsViewer::GetPerspectiveViewport_pvs@void$SYMBOL_NO_DESC
    2$PvsViewer::GetPerspectiveViewport_render@void$SYMBOL_NO_DESC
    2$PvsViewer::GetRenderCamMatrix@void$SYMBOL_NO_DESC
    2$PvsViewer::GetRenderPhaseBits@void$SYMBOL_NO_DESC
    2$PvsViewer::GetViewDirection@void$SYMBOL_NO_DESC
    2$PvsViewer::GetViewPosition@void$SYMBOL_NO_DESC
    2$PvsViewer::IsActive@void$SYMBOL_NO_DESC
    2$PvsViewer::Set_AspectRatio@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_cosHFOV@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_cosVFOV@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_fovy@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_sinHFOV@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_sinVFOV@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_tanFovy@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_ZClipFar@float$SYMBOL_NO_DESC
    2$PvsViewer::Set_ZClipNear@float$SYMBOL_NO_DESC
    2$PvsViewer::SetCamMatrix@const Matrix34 *$SYMBOL_NO_DESC
    2$PvsViewer::SetCullingModeBits@u32$SYMBOL_NO_DESC
    2$PvsViewer::SetMatrix@const rmPvsViewType@const rmPvsMatrixType@const Matrix44 *$SYMBOL_NO_DESC
    2$PvsViewer::SetRenderPhaseBits@const u32$SYMBOL_NO_DESC
    2$PvsViewer::SetView@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
1$rmOccludeDynamicInst$FUNC_PARAM_NO_DESC
    2$rmOccludeDynamicInst::~rmOccludeDynamicInst@void$FUNC_PARAM_NO_DESC
    2$rmOccludeDynamicInst::AddToView@const rmOccluderSystem *@const int$FUNC_PARAM_NO_DESC
    2$rmOccludeDynamicInst::LinkToBaseOccluder@rmOccludePolyhedron *$FUNC_PARAM_NO_DESC
    2$rmOccludeDynamicInst::rmOccludeDynamicInst@void$FUNC_PARAM_NO_DESC
    2$rmOccludeDynamicInst::Transform@Matrix34 &$FUNC_PARAM_NO_DESC
1$rmOccludeDynamicInst$FUNC_PARAM_NO_NAME
    2$rmOccludeDynamicInst::~rmOccludeDynamicInst@void$FUNC_PARAM_NO_NAME
    2$rmOccludeDynamicInst::AddToView@const rmOccluderSystem *@const int$FUNC_PARAM_NO_NAME
    2$rmOccludeDynamicInst::LinkToBaseOccluder@rmOccludePolyhedron *$FUNC_PARAM_NO_NAME
    2$rmOccludeDynamicInst::rmOccludeDynamicInst@void$FUNC_PARAM_NO_NAME
    2$rmOccludeDynamicInst::Transform@Matrix34 &$FUNC_PARAM_NO_NAME
3$rmOccludeDynamicInst::AddToView@const rmOccluderSystem *@const int$FUNC_RETURN_VAL_NO_DESC
1$rmOccludeDynamicInst$SYMBOL_NO_DESC
    2$rmOccludeDynamicInst::friend class rmOccludePolyhedron$SYMBOL_NO_DESC
    2$rmOccludeDynamicInst::GetLinkedOccluderPtr@void$SYMBOL_NO_DESC
1$rmOccludeEdge$SYMBOL_NO_DESC
    2$rmOccludeEdge::~rmOccludeEdge@void$SYMBOL_NO_DESC
    2$rmOccludeEdge::e0$SYMBOL_NO_DESC
    2$rmOccludeEdge::e1$SYMBOL_NO_DESC
    2$rmOccludeEdge::GetEdge@int$SYMBOL_NO_DESC
    2$rmOccludeEdge::rmOccludeEdge@void$SYMBOL_NO_DESC
    2$rmOccludeEdge::SetEdge@int@char$SYMBOL_NO_DESC
3$rmOccludeFacet::SetEdgeFlag@int@char$FUNC_PARAM_NO_DESC
3$rmOccludeFacet::SetEdgeFlag@int@char$FUNC_PARAM_NO_NAME
1$rmOccludeFacet$SYMBOL_NO_DESC
    2$rmOccludeFacet::~rmOccludeFacet@void$SYMBOL_NO_DESC
    2$rmOccludeFacet::GetEdgeIndex@int$SYMBOL_NO_DESC
    2$rmOccludeFacet::GetEdgeIndexReversed@int$SYMBOL_NO_DESC
    2$rmOccludeFacet::GetFacetIgnoredFlag@void$SYMBOL_NO_DESC
    2$rmOccludeFacet::GetNumVerts@void$SYMBOL_NO_DESC
    2$rmOccludeFacet::GetVertpoolIndex@int$SYMBOL_NO_DESC
    2$rmOccludeFacet::rmOccludeFacet@void$SYMBOL_NO_DESC
    2$rmOccludeFacet::SetEdgeIndex@int@char$SYMBOL_NO_DESC
    2$rmOccludeFacet::SetVertexIndex@int@char$SYMBOL_NO_DESC
1$rmOccludePoly$FUNC_PARAM_NO_DESC
    2$rmOccludePoly::CalculatePlane@void$FUNC_PARAM_NO_DESC
    2$rmOccludePoly::rmOccludePoly@void$FUNC_PARAM_NO_DESC
    2$rmOccludePoly::TestOutsideSinglePlane@void$FUNC_PARAM_NO_DESC
1$rmOccludePoly$FUNC_PARAM_NO_NAME
    2$rmOccludePoly::CalculatePlane@void$FUNC_PARAM_NO_NAME
    2$rmOccludePoly::rmOccludePoly@void$FUNC_PARAM_NO_NAME
    2$rmOccludePoly::TestOutsideSinglePlane@void$FUNC_PARAM_NO_NAME
3$rmOccludePoly::TestOutsideSinglePlane@void$FUNC_RETURN_VAL_NO_DESC
1$rmOccludePoly$SYMBOL_NO_DESC
    2$rmOccludePoly::~rmOccludePoly@void$SYMBOL_NO_DESC
    2$rmOccludePoly::ActiveDist@float$SYMBOL_NO_DESC
    2$rmOccludePoly::GetActiveDist@void$SYMBOL_NO_DESC
    2$rmOccludePoly::GetNumVerts@void$SYMBOL_NO_DESC
    2$rmOccludePoly::GetPlane@void$SYMBOL_NO_DESC
    2$rmOccludePoly::GetVertex@int$SYMBOL_NO_DESC
    2$rmOccludePoly::GetVertex4@int@Vector4 *$SYMBOL_NO_DESC
    2$rmOccludePoly::SetVertex@int@Vector3 &$SYMBOL_NO_DESC
    2$rmOccludePoly::SphereIntersectTestHalfspace@const Vector3 &@const float$SYMBOL_NO_DESC
1$rmOccludePolyhedron$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::AllocateEdges@int$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::AllocateFacetsAndPlanes@int$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::AllocateVertexPool@int$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::CalculateEdgeWindingData@void$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::CalculateFacetPlanes@void$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::CreateFacet@int@int$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::CreateSilhouettePolygonFromActiveEdges@t_polyEdgeData *@const u32$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::DebugDraw@bool$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::DestroyFacet@int$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::DestroyFacetsAndPlanes@void$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::GetNumVerts@void$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::GetPlane@int@Vector4 *$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::SetActiveMode@bool$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::SetEdgeIndices@char@char@char$FUNC_PARAM_NO_DESC
    2$rmOccludePolyhedron::SphereIntersectTest@const Vector3 &@const float$FUNC_PARAM_NO_DESC
1$rmOccludePolyhedron$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::AllocateEdges@int$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::AllocateFacetsAndPlanes@int$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::AllocateVertexPool@int$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::CalculateEdgeWindingData@void$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::CalculateFacetPlanes@void$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::CreateFacet@int@int$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::CreateSilhouettePolygonFromActiveEdges@t_polyEdgeData *@const u32$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::DebugDraw@bool$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::DestroyFacet@int$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::DestroyFacetsAndPlanes@void$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::GetNumVerts@void$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::GetPlane@int@Vector4 *$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::SetActiveMode@bool$FUNC_PARAM_NO_NAME
    2$rmOccludePolyhedron::SphereIntersectTest@const Vector3 &@const float$FUNC_PARAM_NO_NAME
1$rmOccludePolyhedron$FUNC_RETURN_VAL_NO_DESC
    2$rmOccludePolyhedron::AllocateEdges@int$FUNC_RETURN_VAL_NO_DESC
    2$rmOccludePolyhedron::AllocateFacetsAndPlanes@int$FUNC_RETURN_VAL_NO_DESC
    2$rmOccludePolyhedron::AllocateVertexPool@int$FUNC_RETURN_VAL_NO_DESC
    2$rmOccludePolyhedron::CreateSilhouettePolygonFromActiveEdges@t_polyEdgeData *@const u32$FUNC_RETURN_VAL_NO_DESC
    2$rmOccludePolyhedron::GetNumVerts@void$FUNC_RETURN_VAL_NO_DESC
    2$rmOccludePolyhedron::SphereIntersectTest@const Vector3 &@const float$FUNC_RETURN_VAL_NO_DESC
1$rmOccludePolyhedron$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::~rmOccludePolyhedron@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::ActiveDist@float$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::DestroyEdges@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::DestroyVertexPool@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::enum@1::MAX_OCCLUDERNAME_STRING_CHARACTERS$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetActiveDist@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetBoundSphere@Vector4 *$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetDebugStringNameLen@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetDebugStringNamePtr@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetEdgeVertexIndex@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetFacetEdgeIndex@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetFacetEdgeIndexReverseFlag@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetFacetIgnoredFlag@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetFacetNumVerts@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetFacetVertIndex@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetNumEdges@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetNumFacets@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetVertex@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::GetVertex4@int@Vector4 *$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::IsActive@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::rmOccludePolyhedron@void$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetBoundSphere@Vector4 &$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetDebugStringName@char *$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetFacetEdgeIndex@int@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetFacetEdgeIndexReverseFlag@int@int@char$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetFacetIgnoredFlag@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetFacetVertIndex@int@int@int$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::SetVertex@int@Vector3 &$SYMBOL_NO_DESC
    2$rmOccludePolyhedron::TestOutsideSinglePlane@pvsVtxClipFlags *@PvsViewer *$SYMBOL_NO_DESC
1$rmOccluderSystem$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::ActivateSpanBuffer@const int@int@int$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::AddOccluderToPvsView@const pvsViewerID@rmOccludePolyhedron *@const bool$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::AllocateSpan@void$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::CreateViewer@void$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::DrawDebugViewInfo@const pvsViewerID DEV_ONLYid@const rmPvsDebugDrawModeBits DEV_ONLYdrawmodebits$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::EnableDrawAllOccluders@const pvsViewerID DEV_ONLYid@const bool DEV_ONLYmode$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::GetViewActiveOccluderCount@const int$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::InlineFast2SpheresOutsideSinglePlane_A@const Vector4 &@const Vector4 &@const$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::InsertDepthSprite@int@int@int@int@float$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::IsFrameInProgress@void$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::IsSphereSpanBufferOccluded@const Vector3 &@const float@const bool$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::LoadDataset@const char *@const char *$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::LoadExternalGeomDataset@const char *@const char *@int *@rmOccludePolyhedron **@int *@rmOccludePoly **$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::OccludeOccluders@const pvsViewerID$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::ResetAllSpanBuffers@void$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::rmOccluderSystem@void$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::SetDebugAnimTimeDelta@float$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::SetRenderView@const pvsViewerID@const grcViewport*@const Vector3 &@const Vector3 &@const Matrix34 *$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::StartNewFrame@void$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::TestAABoxVisibility@const pvsViewerID@const Vector4 *@const Vector4 *@const Matrix34 *@const rmPvsCullModeBits$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::TestCapsuleVisibility@const pvsViewerID@const Vector3 &@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::TestDepthSprite@int@int@int@int@float$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::TestSphereVisibility@const pvsViewerID@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::TestVertexPoolVisibility@const pvsViewerID@const Vector3 *@const int@const rmPvsCullModeBits@const rmOccludePolyhedron *$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::TransformAndProject@Vector4 *@const Vector3 &$FUNC_PARAM_NO_DESC
    2$rmOccluderSystem::ViewSetupFinish@const pvsViewerID DEV_ONLYid@const Matrix34 * DEV_ONLYpCameraMatrix$FUNC_PARAM_NO_DESC
1$rmOccluderSystem$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::ActivateSpanBuffer@const int@int@int$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::AddOccluderToPvsView@const pvsViewerID@rmOccludePolyhedron *@const bool$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::AllocateSpan@void$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::CreateViewer@void$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::DrawDebugViewInfo@const pvsViewerID DEV_ONLYid@const rmPvsDebugDrawModeBits DEV_ONLYdrawmodebits$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::EnableDrawAllOccluders@const pvsViewerID DEV_ONLYid@const bool DEV_ONLYmode$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::GetViewActiveOccluderCount@const int$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::InlineFast2SpheresOutsideSinglePlane_A@const Vector4 &@const Vector4 &@const$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::InsertDepthSprite@int@int@int@int@float$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::IsFrameInProgress@void$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::IsSphereSpanBufferOccluded@const Vector3 &@const float@const bool$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::OccludeOccluders@const pvsViewerID$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::ResetAllSpanBuffers@void$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::rmOccluderSystem@void$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::SetDebugAnimTimeDelta@float$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::SetRenderView@const pvsViewerID@const grcViewport*@const Vector3 &@const Vector3 &@const Matrix34 *$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::StartNewFrame@void$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::TestCapsuleVisibility@const pvsViewerID@const Vector3 &@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::TestDepthSprite@int@int@int@int@float$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::TestSphereVisibility@const pvsViewerID@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::TransformAndProject@Vector4 *@const Vector3 &$FUNC_PARAM_NO_NAME
    2$rmOccluderSystem::ViewSetupFinish@const pvsViewerID DEV_ONLYid@const Matrix34 * DEV_ONLYpCameraMatrix$FUNC_PARAM_NO_NAME
1$rmOccluderSystem$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::AddOccluderToPvsView@const pvsViewerID@rmOccludePolyhedron *@const bool$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::AllocateSpan@void$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::CreateViewer@void$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::GetViewActiveOccluderCount@const int$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::InlineFast2SpheresOutsideSinglePlane_A@const Vector4 &@const Vector4 &@const$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::InsertDepthSprite@int@int@int@int@float$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::IsFrameInProgress@void$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::IsSphereSpanBufferOccluded@const Vector3 &@const float@const bool$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::LoadExternalGeomDataset@const char *@const char *@int *@rmOccludePolyhedron **@int *@rmOccludePoly **$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::OccludeOccluders@const pvsViewerID$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::SetDebugAnimTimeDelta@float$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::TestAABoxVisibility@const pvsViewerID@const Vector4 *@const Vector4 *@const Matrix34 *@const rmPvsCullModeBits$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::TestCapsuleVisibility@const pvsViewerID@const Vector3 &@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::TestDepthSprite@int@int@int@int@float$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::TestSphereVisibility@const pvsViewerID@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::TestVertexPoolVisibility@const pvsViewerID@const Vector3 *@const int@const rmPvsCullModeBits@const rmOccludePolyhedron *$FUNC_RETURN_VAL_NO_DESC
    2$rmOccluderSystem::TransformAndProject@Vector4 *@const Vector3 &$FUNC_RETURN_VAL_NO_DESC
1$rmOccluderSystem$SYMBOL_NO_DESC
    2$rmOccluderSystem::AddOccluderToActiveView@const pvsViewerID@rmOccludePolyhedron *@const bool$SYMBOL_NO_DESC
    2$rmOccluderSystem::AddWidgets@bkBank &$SYMBOL_NO_DESC
    2$rmOccluderSystem::AreOccludersEnabled@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::CreateEdgePlaneToActiveOccluder@const int@mcActiveOccluderPoly *@const Vector3 &@const Vector3 &@const Vector3 &$SYMBOL_NO_DESC
    2$rmOccluderSystem::DebugDrawOccluders@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::DeleteViewer@const pvsViewerID$SYMBOL_NO_DESC
    2$rmOccluderSystem::Destroy@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::EndFrame@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::FrustumVisCheck@const Vector4 &@float&$SYMBOL_NO_DESC
    2$rmOccluderSystem::FrustumVisCheck@const Vector4 &@float&@const int$SYMBOL_NO_DESC
    2$rmOccluderSystem::GetActivationDistanceScale@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::GetDebugAnimTimeDelta@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::GetMode_OccludedVolumeGlobalColor@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::GetOccludedCount@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::GetOccludedVolumeDebugColor@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::GetPvsViewerPtr@const pvsViewerID$SYMBOL_NO_DESC
    2$rmOccluderSystem::InsertPlaneAsActiveOccluder@const int@mcActiveOccluderPoly *@const Vector4 &$SYMBOL_NO_DESC
    2$rmOccluderSystem::IsHotDogOccluded@const Vector3 &@const Vector3 &@const float$SYMBOL_NO_DESC
    2$rmOccluderSystem::IsSphereOccluded@const Vector3 &@const float$SYMBOL_NO_DESC
    2$rmOccluderSystem::OccludedVolumesBuffered_GetMode@void$SYMBOL_NO_DESC
    2$rmOccluderSystem::OccludersEnabled@bool$SYMBOL_NO_DESC
    2$rmOccluderSystem::ProcessStaticOccluders@const pvsViewerID$SYMBOL_NO_DESC
    2$rmOccluderSystem::SetActivationDistanceScale@const float$SYMBOL_NO_DESC
    2$rmOccluderSystem::SetMode_OccludedVolumeGlobalColor@bool$SYMBOL_NO_DESC
    2$rmOccluderSystem::SetMode_OccludedVolumesBuffered@const bool$SYMBOL_NO_DESC
    2$rmOccluderSystem::SetOccludedVolumeDebugColor@Color32$SYMBOL_NO_DESC
    2$rmOccluderSystem::SetOccluderGlobalDebugDrawMode@const bool$SYMBOL_NO_DESC
    2$rmOccluderSystem::SetPvsView@const pvsViewerID@const grcViewport*@const Matrix34 &$SYMBOL_NO_DESC
    2$rmOccluderSystem::sm_debugColor@0.@1.@1.@1.$SYMBOL_NO_DESC
    2$rmOccluderSystem::TestSphereVisibility@const pvsViewerID@const Vector4 &@const rmPvsCullModeBits$SYMBOL_NO_DESC
1$rmPvsBoundType$SYMBOL_NO_DESC
    2$rmPvsBoundType::pvsbt_Box$SYMBOL_NO_DESC
    2$rmPvsBoundType::pvsbt_Capsule$SYMBOL_NO_DESC
    2$rmPvsBoundType::pvsbt_Null$SYMBOL_NO_DESC
    2$rmPvsBoundType::pvsbt_Sphere$SYMBOL_NO_DESC
    2$rmPvsBoundType::pvsbt_Vertex$SYMBOL_NO_DESC
1$rmPvsCullModeBits$SYMBOL_NO_DESC
    2$rmPvsCullModeBits::pvscm_AllBits_mask$SYMBOL_NO_DESC
    2$rmPvsCullModeBits::pvscm_Empty$SYMBOL_NO_DESC
1$rmPvsCullStatusBits$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_all_clear$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_AllBits_mask$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_bits_mask$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_inside_mask$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_negB$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_negF$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_negL$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_negN$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_negR$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_negT$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_outside_mask$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_posB$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_posF$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_posL$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_posN$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_posR$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_frust_plane_posT$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_InsideFrustum$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_NotOccluded$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_Occluded$SYMBOL_NO_DESC
    2$rmPvsCullStatusBits::pvscull_OutsideFrustum$SYMBOL_NO_DESC
1$rmPvsDebugDrawModeBits$SYMBOL_NO_DESC
    2$rmPvsDebugDrawModeBits::pvsdd_AllBits_mask$SYMBOL_NO_DESC
    2$rmPvsDebugDrawModeBits::pvsdd_DrawCulledBounds$SYMBOL_NO_DESC
    2$rmPvsDebugDrawModeBits::pvsdd_DrawFrustum$SYMBOL_NO_DESC
    2$rmPvsDebugDrawModeBits::pvsdd_DrawOccluders$SYMBOL_NO_DESC
    2$rmPvsDebugDrawModeBits::pvsdd_DrawUmbra$SYMBOL_NO_DESC
    2$rmPvsDebugDrawModeBits::pvsdd_Empty$SYMBOL_NO_DESC
1$rmPvsMatrixType$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_Composite$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_FullComposite$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_ModelView$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_NumTypes$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_Projection$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_Screen$SYMBOL_NO_DESC
    2$rmPvsMatrixType::pvsmt_View$SYMBOL_NO_DESC
1$rmPvsViewType$SYMBOL_NO_DESC
    2$rmPvsViewType::pvsvt_NumViewTypes$SYMBOL_NO_DESC
    2$rmPvsViewType::pvsvt_PVS$SYMBOL_NO_DESC
    2$rmPvsViewType::pvsvt_Render$SYMBOL_NO_DESC
3$s_poly::verts$SYMBOL_NO_DESC
1$s_polyEdgeData$SYMBOL_NO_DESC
    2$s_polyEdgeData::e0$SYMBOL_NO_DESC
    2$s_polyEdgeData::e1$SYMBOL_NO_DESC
1$s_vert$SYMBOL_NO_DESC
    2$s_vert::next$SYMBOL_NO_DESC
    2$s_vert::prev$SYMBOL_NO_DESC
    2$s_vert::v4screenVtx$SYMBOL_NO_DESC
    2$s_vert::x$SYMBOL_NO_DESC
    2$s_vert::y$SYMBOL_NO_DESC
1$SpriteSpanBuffer$FUNC_PARAM_NO_DESC
    2$SpriteSpanBuffer::ActivateSpanBuffer@s16@s16$FUNC_PARAM_NO_DESC
    2$SpriteSpanBuffer::ClearScanLineSpanListPtr@int$FUNC_PARAM_NO_DESC
    2$SpriteSpanBuffer::GetStartingSpanForScanline@int$FUNC_PARAM_NO_DESC
1$SpriteSpanBuffer$FUNC_PARAM_NO_NAME
    2$SpriteSpanBuffer::ActivateSpanBuffer@s16@s16$FUNC_PARAM_NO_NAME
    2$SpriteSpanBuffer::ClearScanLineSpanListPtr@int$FUNC_PARAM_NO_NAME
    2$SpriteSpanBuffer::GetStartingSpanForScanline@int$FUNC_PARAM_NO_NAME
3$SpriteSpanBuffer::GetStartingSpanForScanline@int$FUNC_RETURN_VAL_NO_DESC
1$SpriteSpanBuffer$SYMBOL_NO_DESC
    2$SpriteSpanBuffer::GetSpanBufferHeight@void$SYMBOL_NO_DESC
    2$SpriteSpanBuffer::GetSpanBufferWidth@void$SYMBOL_NO_DESC
3$TestAABoxVisibility@const pvsViewerID@const Vector4 *@const Vector4 *@const Matrix34 *@const rmPvsCullModeBits$FUNC_PARAM_NO_DESC
3$TestAABoxVisibility@const pvsViewerID@const Vector4 *@const Vector4 *@const Matrix34 *@const rmPvsCullModeBits$FUNC_PARAM_NO_NAME
3$TestAABoxVisibility@const pvsViewerID@const Vector4 *@const Vector4 *@const Matrix34 *@const rmPvsCullModeBits$FUNC_RETURN_VAL_NO_DESC
3$TestSphereToNgonHalfspace@const Vector3 &@const float@const int@const Vector3 *@const Vector4 *$SYMBOL_NO_DESC
3$TestSphereVisibility@const pvsViewerID@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_PARAM_NO_DESC
3$TestSphereVisibility@const pvsViewerID@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_PARAM_NO_NAME
3$TestSphereVisibility@const pvsViewerID@const Vector3 &@const float@const rmPvsCullModeBits$FUNC_RETURN_VAL_NO_DESC
3$__EARLY_OUT_AABOX_CULLBITS_TEST$SYMBOL_NO_DESC
3$__POLYSPAN_DEBUG$SYMBOL_NO_DESC
3$__SBUFFER_OCCLUDER_DEBUG$SYMBOL_NO_DESC
3$__SPAN_MERGE$SYMBOL_NO_DESC
3$__SPANBUFFER_ENABLED$SYMBOL_NO_DESC
3$_OCCLUDER_DEBUGGING$SYMBOL_NO_DESC
3$_OCCLUDER_TIMING$SYMBOL_NO_DESC
3$_OCCLUDER_WARNINGS$SYMBOL_NO_DESC
3$activeView$SYMBOL_NO_DESC
3$ALIGNED@16$FUNC_PARAM_NO_DESC
3$ALIGNED@16$FUNC_PARAM_NO_NAME
3$ALIGNED@16$FUNC_RETURN_VAL_NO_DESC
3$g_bFrustumTestAABoxes$SYMBOL_NO_DESC
3$g_bOccludeTestAABoxes$SYMBOL_NO_DESC
3$g_SpanMerges$SYMBOL_NO_DESC
3$g_SpansInUse$SYMBOL_NO_DESC
3$g_VU0_occlusion$SYMBOL_NO_DESC
3$m_Use_VU0_Occlusion$SYMBOL_NO_DESC
3$MAX_ACTIVE_OCCLUDERS_VU0$SYMBOL_NO_DESC
3$min$SYMBOL_NO_DESC
3$OCCLUDER_PREP_DMA_CHAIN_SIZE$SYMBOL_NO_DESC
3$ProcessStaticOccluders@pvsID$FUNC_PARAM_NO_DESC
3$ProcessStaticOccluders@pvsID$FUNC_PARAM_NO_NAME
3$ProcessStaticOccluders@pvsID$FUNC_RETURN_VAL_NO_DESC
3$pvsEdgeData$SYMBOL_NO_DESC
3$pvsFaceClipFlags$SYMBOL_NO_DESC
3$pvsID$SYMBOL_NO_DESC
3$pvsVtxClipFlags$SYMBOL_NO_DESC
3$RM_OCCLUDE_FILE_VERSION_2$SYMBOL_NO_DESC
3$RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS$SYMBOL_NO_DESC
3$RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL$SYMBOL_NO_DESC
3$RM_OCCLUDE_OCCLUDERTYPE_NGON$SYMBOL_NO_DESC
3$RM_OCCLUDE_SPANBUFF_MAX_ZDEPTH$SYMBOL_NO_DESC
3$s_pPrev$SYMBOL_NO_DESC
3$s_spanCoverage$SYMBOL_NO_DESC
3$SetPvsView@pvsID@this->GetViewport@this->GetCameraMatrix$SYMBOL_NO_DESC
3$SetRenderView@pvsID@camMtx.@camMtx.@&$FUNC_PARAM_NO_DESC
3$SetRenderView@pvsID@camMtx.@camMtx.@&$FUNC_PARAM_NO_NAME
3$SetRenderView@pvsID@camMtx.@camMtx.@&$FUNC_RETURN_VAL_NO_DESC
3$span_array$SYMBOL_NO_DESC
3$SPAN_MEMSIZE$SYMBOL_NO_DESC
3$sPoly$SYMBOL_NO_DESC
3$sVert$SYMBOL_NO_DESC
3$t_polyEdgeData$SYMBOL_NO_DESC
3$total_active$SYMBOL_NO_DESC
3$VMX_CLIP_STATUS$SYMBOL_NO_DESC
3$VU0_OCCLUDEPLANE_DOWNLOAD_OFFSET$SYMBOL_NO_DESC
3$VUDATA@u32@occludeTestBound_DmaTag$FUNC_PARAM_NO_DESC
3$VUDATA@u32@occludeTestBound_DmaTag$FUNC_PARAM_NO_NAME
