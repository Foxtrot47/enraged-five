@@MemTrackerWindowPlugin
<group AuthoringWindowPlugins>
<title Memory Tracker Window Plugin>

* Overview *
The Memory Tracker is a Rag Plugin for the presentation of
the state of memory usage at any given time. It is activated
by adding -trackerrag to the command line of the game or
\sample.



Due to the enormous amount of data involved, this Plugin does
not display information in real-time. Instead, certain points
in time are tagged and can be viewed as Reports. A Report
consists of every memory allocation that exists at that point
in time since the beginning of the game. Additionally,
deallocation information can only be tracked when Track
Deallocations is enabled. See <link MemTrackerTopBar, Top Bar>
to see how to change it.



It's important to note here that there are several items that
are not getting tracked. These are the items that were
created before the game-side memory tracker system was
activated, including the tracker system itself.



\Reports can be specified from the code with the
RAGE_TRACK_REPORT, RAGE_TRACK_REPORT_FREES,
RAGE_TRACK_REPORT_NAME, and RAGE_TRACK_REPORT_NAME_FREES
macros, and they can be created by pressing any of the
Generate Report buttons in the Plugin Window. To view a
different report, simply select it from one of the many
Report combo boxes and click Refresh. Similarly, many of the
display options require the user to click the Refresh button
after changing them. Again, this is due to the long amount of
time it takes to examine the data in order to create a Report
View.



In addition to a few items on the Top Bar, there are three
tabs of information: Overview, Allocations, and Memory
Layout. Each of these is outlined in the following sections.

@@MemTrackerTopBar
<group MemTrackerWindowPlugin>
<title Top Bar>

<image memtracker_topbar>



On the left, is the current status of the connection to the
game, including the Ports it connected with.

* Progress Bar *
Since refreshing a Report View takes a large amount time,
this will indicate how far along the process is.

* Generate Report (All) Button *
Tags a new Report and Refreshes the View on all 3 tabs with
the Report that was just tagged.

* Main Options Drop Down Menu *
<table>
Item            \Description
--------------  ---------------------------------------------------------
Save Report     You can save any of the Reports that have been generated
 Drop Down       from here. Simply select it from <i>Save Report</i>'s
 Menu            drop down menu. These reports can later be viewed in a
                 text editor or passed to tracker.exe.
Show Errors     Unfortunately, errors still exist in the game-side code
 Button          as far as tracking memory allocations goes. The user
                 will be prompted when an inconsistency is detected and
                 given an option on how to proceed. Every problem that
                 \occurs can be viewed on the Errors Window (not shown)
                 that appears by clicking this button.
Track           At times it may be useful to view the memory
 Deallocations   deallocations that occurred during a Report. Due to the
 Check Box       large memory impact that tracking this information
                 incurs, it must be turned on manually. When the state of
                 this check box is changed, a new Report is generated.
                 That way, a Report will always contain either all of
                 it's deallocation information or none at all.
</table>

* Refresh All Button *
Refreshes all 3 tabs with their currently selected Report. If
none is selected for a tab, the last Report that was tagged
is used.

@@MemTrackerOverviewTab
<group MemTrackerWindowPlugin>
<title Overview Tab>

At the moment, the Overview Tab is the only tab that allows
the user to compare Reports from two different moments in
time. The total size of each line is displayed for both
\Reports, as well as the difference between them.



It contains a right-click context menu with Copy and Select
All options, or the user can press Ctrl+C and Ctrl+A,
respectively. Those can be used to copy the selected rows to
the clipboard to past into a text file or email.



At the bottom is a status bar that shows basic information
about what the report contains. These fields update when the
Overview List is refreshed. Additionally, the <i>Selected
Size</i> item will contain the total number of bytes for all
highlighted rows.



<image memtracker_overview>
* Generate Report Button *
Clicking this button will tag a new Report and Refresh the
view with the new Report as the Left Report. If Left Report
already contains a report, it is moved to the Right Report
automatically.

* Left and Right Report Combo Boxes *
Select the more recent report in the <i>Left Report Combo Box</i>
and the earlier report in the <i>Right Report Combo Box</i>
and click Refresh. Report Names with an asterisk at the end
indicate that the Report contains deallocation information.

* Options Drop Drown Menu *
This menu contains several filtering options to help the user
choose what they want to display.

<table>
Item          \Description
------------  ----------------------------------------------------------
Show By       Contains 3 categories of items to display: Stack Name,
 Drop Down     Data Type and Memory Type.
 Menu          * Stack Names are specified by RAGE_TRACK,
                 RAGE_TRACK_NAME, and RAGE_NEW_TRACK in the game code.
                 These are typically used to denote blocks of memory
                 allocation code pertaining to a specific event or
                 subsystem of the game. Because the Stack Names are
                 hierarchical, this particular Show By has a column for
                 Inclusive size totals and Exclusive totals. Inclusive
                 shows the totals for the current Stack Name and all
                 Stacks underneath it. Exclusive shows only what was
                 allocated at the level of that Stack.
               * Data Type shows you the total memory consumed by each
                 data type in the game.
               * Memory Type (shown) displays the memory usage totals
                 for each sysMemAllocator in use and the total memory
                 consumption of the game.
               Also contains 2 options that can be applied to any
               category. These items are only enabled if both the Left
               Report and Right Report match as far as if they did or
               did not track deallocation information:
               * Allocations: Displays what was allocated during the
                 time of the Report.
               * Deallocations: Displays what was free'd during the time
                 \of the Report.
Stacks Drop   There may be an overwhelming number of Stack Names
 Down Menu     created and sifting through a long list can be difficult.
               Only active when <i>Show By</i> is set to Stack Name,
               this option allows the user to select which Stack Names
               to display data for.
Datatypes     Similar to the <i>Stacks Drop Down Menu</i>, this option
 Drop Down     allows the user to choose which Datatypes to display when
 Menu          <i>Show By</i> is set to Data Type.
Show Empty    By default, this is unchecked so that items with 0 bytes
 Items Check   are not displayed in the list.
 Box           
</table>

* Refresh Button *
Active when the Left Report, Right Report, or one of the
filtering Options has changed, this updates the contents of
the Overview List for what has been selected. After the
refresh, the information in the Status bar at the bottom of
the window is updated.

@@MemTrackerAllocationsTab
<group MemTrackerWindowPlugin>
<title Allocations Tab>

By far the most complex and memory-intensive Report View, the
Allocations Tab displays a Tree List view of each individual
allocation. Please be aware that it is very easy to run out
\of system memory when viewing a Report on this Tab. You may
be prompted to de-select Stacks, Data Types Memory Types,
and/or change the Size Range to display a sub-set of the data
within the bounds of system memory.



A right-click context menu exists over the Tree List that
allows you to quickly collapse or expand all Stack Name
nodes. Nodes that do not collapse are array nodes. For
thoroughness, each item in an array has its own line in the
Tree View. There is also a Copy menu item with Ctrl+C as its
shortcut. No Select All exists due to memory restrictions,
but Multi-Select can be enabled and the user can manually
select what they want to copy. Be warned, that enabling
Multi-Select may slow things down considerably and is not
recommended.



Similar to the Overview Tab, a Status bar at the bottom of
the window contains information pertinent to the current
Report. Again, the <i>Selected Size</i> item will contain a
total size of all of the item(s) selected. If a node is
selected that contains sub-nodes, only the top node's size is
added.



<image memtracker_allocations>
* Generate Report Button *
Functions just as it does on the <link MemTrackerOverviewTab, Overview Tab>.

* Report Combo Box *
Used to select the Report to view, similar to the <link MemTrackerOverviewTab, Overview Tab>'s
Left Report Combo Box.

* Min and Max Size Text Boxes *
Only Allocations whose size in bytes fits between the Min and
Max size are displayed.

* Options Drop Down Menu *
Contains options that are very similar to the <link MemTrackerOverviewTab, Overview Tab>.

<table>
Item           \Description
-------------  -----------------------------------------------------------------
Group By       Two settings govern how the nodes are organized
 Drop Down      hierarchically.
 Menu           * Allocation Order displays each Allocation in the
                  \order it was made.
                * Stack Name groups all Allocations by the full path
                  \of their Stack Name regardless of when the item was
                  allocated.
                Also contains 2 options that can be applied to either
                \of the above settings:
                * Allocations: Displays what was allocated during the
                  time of the Report.
                * Deallocations: Displays what was free'd during the
                  time of the Report.
Stacks Drop    Behaves the same as the like-named Drop Down Menu on
 Down Menu      the <link MemTrackerOverviewTab, Overview Tab>.
Datatypes      Also behaves like its <link MemTrackerOverviewTab, Overview Tab>
 DropDown       counterpart of the same name.
 Menu           
Memory         View Allocations that were made with certain
 Types Drop     sysMemAllocators.
 Down Menu      
Multi-Select   By default, this is unchecked. The TreeList control's
 Combo Box      performance suffers greatly with Multi-Select enabled.
                It is recommended that this option is only enabled
                when trying to select nodes for copying, and that it
                should be disabled immediately afterwards.
</table>

* Refresh Button *
Similar to the Refresh Button on the <link MemTrackerOverviewTab, Overview Tab>.

@@MemTrackerMemoryLayoutTab
<group MemTrackerWindowPlugin>
<title Memory Layout Tab>

Unlike the other tabs, all of the options except for the <i>Report
Combo Box</i> will update immediately after the option is
changed. The Memory Layout Tab is useful for viewing where
Allocations and blocks of Allocations are made and for seeing
fragmented memory.



The Left Pane contains the view of memory and the Right Pane
contains the color key. Holding the left mouse button and
dragging the area of the Left Pane will allow you to see
hidden areas of the memory view. Right-click and select
Re-center to reset the view back to the top left.



Once again, the bottom of the window contains a list of
Status items. These update according to the allocation that
is underneath the mouse when it is hovering inside the Left
Pane.



<image memtracker_memory>
* Generate Report Button *
Just like the ones on the <link MemTrackerOverviewTab, Overview Tab>
and <link MemTrackerAllocationsTab, Allocations Tab>.

* Report Combo Box *
Select which Report to view, just like on the <link MemTrackerOverviewTab, Overview Tab>
and <link MemTrackerAllocationsTab, Allocations Tab>.

* Refresh Button *
Refreshes the view for the current Report.

* Bytes / Pixel Combo Box *
Select how many bytes each pixel in the Left Panel
represents. Ranges from 4 to 1024 bytes.

* Width / Address Range Combo Box *
Specifies how many pixels across to display. The smaller the
number, the taller the display area needed to see all of
memory. Conversely, the larger the number the wider the Left
Pane will need to be to see everything. Ranges from 32 to
1024 pixels.

* Colorize By Combo Box *
To help differentiate different areas of the view, things are
colorized by Stack Name, the Full Stack Path, Allocation
Size, or Data Type. There is an upper limit to the colors
provided, and when that occurs, the items are truncated or
consolidated into one color. For example, when we run out of
colors for Colorize By Data Type, each of the native types
(char, int, etc.) receives a color, while all Structs get one
color and all Classes get another.

* Size Scale Combo Box *
When Colorizing by Allocation Size, you can select the
granularity of the colorization, ranging from Coarse to Very
Fine.

@@MemTrackerPerformanceNotes
<group MemTrackerWindowPlugin>
<title Performance Notes>

The Memory Tracker is a complicated piece of software. It was
written with speed in mind so it is a bit memory intensive,
even on the game-side. However, the Memory Tracker will still
affect the performance of your game.



Expect the startup of your game to take 3 to 4 times longer
than usual. At times, the initial load may appear to have
stopped. I believe this is due to thread priority and the
speed at which the Tracker can process the network messages
received from the game. This is unconfirmed, but opening the
Memory Tracker Window and Generating or Refreshing a Report
seems to help speed things along.



Once in the game, things should run pretty close to normal,
with minor frame rate jitters during intense run-time loads.
