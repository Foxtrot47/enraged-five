// 
// sample_gizmo/sample_all.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE: Demonstrate the creation and use of gizmos. Shows how rotation and translation gizmos can be
//          linked together by pointing them at the same matrix. 

#define ENABLE_RFS 1

#include "sample_grcore/sample_grcore.h"

#include "gizmo/manager.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "system/main.h"

#include "grcore/im.h"
#include "vector/geometry.h"

#include "input/mouse.h"

namespace ragesamples {

using namespace rage;

class gizmoSampleManager : public grcSampleManager
{
public:
	gizmoSampleManager()
		: m_Translation1(NULL)
		, m_Translation2(NULL)
		, m_Rotation1(NULL)
		, m_Rotation2(NULL)
	{
	}

	virtual void InitClient()
	{
		// STEP #1. Create a translation gizmo.

		//-- We initialize m_Matrix1 where we want our translation gizmo to
		//   start. Then we pass that matrix to the gizmo constructor, to inform
		//   our new gizmo of the matrix it should control.
		m_Matrix1.MakeRotate(Vector3(1.0f, 2.0f, 1.5f), 1.0f);
		m_Matrix1.MakeTranslate(2.0f, 0.0f, -2.0f);
		m_Translation1 = rage_new gzTranslation(m_Matrix1);
		m_Translation1->SetSize(1.5f);

		// STEP #2. Create a rotation gizmo.

		//-- Just as in Step 2, we initialize m_Matrix2 where we want our rotation
		//   gizmo to appear. We then again pass that matrix to the gizmo constructor.
		m_Matrix2.MakeRotateX(PI * 0.6f);
		m_Matrix2.MakeTranslate(-2.0f, 0.0f, 2.0f);
		m_Rotation1 = rage_new gzRotation(m_Matrix2);
		m_Rotation1->SetSize(0.7f);

		// STEP #3. Create two linked gizmos.

		//-- In step 3, we also initialize a matrix where we want our rotation
		//   gizmos. But in this case, we pass the same matrix to the constructors
		//   of two different gizmos, who will both control the matrix.
		m_Matrix3.MakeRotateY(PI * 0.1f);
		m_Matrix3.MakeTranslate(0.0f, 0.0f, 0.0f);
		m_Translation2 = rage_new gzTranslation(m_Matrix3);
		m_Translation2->SetSize(1.0f);
		m_Rotation2 = rage_new gzRotation(m_Matrix3);
		m_Rotation2->SetSize(1.5f);

		// Do bounding box tests.
		m_boxCount = 8;
		for ( int i =0; i < m_boxCount; i++ )
		{
			m_gizmos[i]=0;
			m_boxes[i].Identity();
			m_boxes[i].MakeTranslate((float)(i ) * 2.0f - (float)m_boxCount, 0.0f, 0.0f);
		}

		m_IsTranslationMode = true;
		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD, KEY_T,	m_TranslationToggle);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE,	m_ViewCameraControl);
		m_Mapper.Map(IOMS_MOUSE_BUTTON, ioMouse::MOUSE_LEFT,	m_Grab);
		m_Mapper.Map(IOMS_KEYBOARD, 	KEY_ALT,				m_CameraControl);

		m_ColorScheme = SCHEME_MAYA;
		m_ClearColor = GetBackGroundColor();

	}

	virtual void UpdateClient()
	{
		Vector3 from, to;

		m_Mapper.Update();

		if ( m_ViewCameraControl.IsReleased() )
		{
			m_ViewMode = m_ViewMode ? 0 : 1;
			UpdateCamera();
		}
		if ( m_TranslationToggle.IsPressed())
		{
			m_IsTranslationMode = !m_IsTranslationMode;
			GIZMOMGR.ResetGrabbed();
			for (int i =0; i < m_boxCount; i++)
			{
				if ( m_gizmos[i] )
				{
					delete m_gizmos[i];
					if ( m_IsTranslationMode )
						m_gizmos[i] = rage_new gzTranslation(m_boxes[i]);
					else
						m_gizmos[i] = rage_new gzRotation(m_boxes[i]);
				}
			}
		}
		if ( !grcViewport::GetCurrent())
		{
			return;
		}
		grcWorldIdentity();
		grcViewport::GetCurrent()->ReverseTransform(static_cast<float>(ioMouse::GetX()),
			static_cast<float>(ioMouse::GetY()),
			from,
			to);

		// test with each box and create gizmo if over box
		for (int i =0; i < m_boxCount; i++)
		{
			Vector3 localFrom, localTo;
			m_boxes[i].UnTransform(from, localFrom);
			m_boxes[i].UnTransform(to, localTo);

			Vector3 bmin = Vector3(-0.5f,-0.5f,-0.5f);
			Vector3 bmax = Vector3(0.5f,0.5f,0.5f);
			Vector3 bmin2 = bmin * 2.0f;
			Vector3 bmax2 = bmax * 2.0f;

			if ( geomBoxes::TestSegmentToBox (localFrom, localTo, bmin,bmax ))
			{
				if ( !m_gizmos[i] )
				{
					if ( m_IsTranslationMode )
						m_gizmos[i] = rage_new gzTranslation(m_boxes[i]);
					else
						m_gizmos[i] = rage_new gzRotation(m_boxes[i]);
				}
			}
			else
			{
				if (m_gizmos[i] && !GIZMOMGR.IsGrabbed(m_gizmos[i]) )
				{
					if ( !geomBoxes::TestSegmentToBox (localFrom, localTo, bmin2,bmax2))
					{
						delete m_gizmos[i];
						m_gizmos[i] = 0;
					}
				}
			}

		}
	}

	void InitLights()
	{
		m_LightMode = grclmNone;
		grcSampleManager::InitLights();
	}

	void InitCamera()
	{
		m_OrthoCam.InitViews();
		grcSampleManager::InitSampleCamera(Vector3(5.0f, 5.0f, 5.0f), Vector3(0.f, 0.f, 0.f));
	}

	virtual void DrawClient()
	{
		for ( int i =0; i < m_boxCount; i++ )
		{
			grcDrawSolidBox( Vector3(1.0,1.0f,1.0f), m_boxes[i], Color32(128,128,0,128));
		}
	}

	virtual void ShutdownClient()
	{
		// STEP #4. Clean up.

		//-- Delete the gizmos we made, and then invoke SHUTDOWN_GIZMOS.
		delete m_Rotation2;
		delete m_Translation2;
		delete m_Rotation1;
		delete m_Translation1;

		for (int i = 0; i < m_boxCount; i++)
		{
			delete m_gizmos[i];
		}
	}

protected:
	Matrix34 m_Matrix1;
	gzGizmo* m_Translation1;
	Matrix34 m_Matrix2;
	gzGizmo* m_Translation2;
	gzGizmo* m_Rotation1;
	Matrix34 m_Matrix3;
	gzGizmo* m_Rotation2;

	Matrix34	m_boxes[16];
	gzGizmo*	m_gizmos[16];
	int			m_boxCount;
	bool		m_IsTranslationMode;

	// Keyboard ui control
	ioMapper m_Mapper;			// The mapper for polling input devices
	ioValue m_TranslationToggle;				// Input control for the grab button (usually the left mouse button)
	ioValue m_ViewCameraControl;				// Input control for the grab button (usually the left mouse button)
	ioValue m_Grab;
	ioValue m_CameraControl;

};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::gizmoSampleManager sampleGizmo;
		sampleGizmo.Init();

		sampleGizmo.UpdateLoop();

		sampleGizmo.Shutdown();
	}

	return 0;
}
