// 
// sample_ragcompression/sample_ragcompression.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_ragcompression.h"


#include "compressiondata.h"
#include "compressionfactory.h"
#include "datcompressiondata.h"
#include "ficompressiondata.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/packet.h"
#include "math/random.h"
#include "sample_grcore/sample_grcore.h"
#include "system/param.h"

using namespace rage;
using namespace ragesamples;

namespace ragesamples
{

#if __DEV | __BANK

#if __BANK
#define COMPRESSION_PORT_OFFSET (bkRemotePacket::SOCKET_OFFSET_USER5 + 24)
#endif

rmcSampleCompression::rmcSampleCompression()
: m_compressionMethod(FILE_COMPRESSION)
, m_alternationMethod(DATA)
, m_TcpHandle(fiHandleInvalid)
, m_flushOption(AFTER_EACH)
, m_pCompressionFactory(NULL)
{

}

rmcSampleCompression::~rmcSampleCompression()
{

}

void rmcSampleCompression::InitClient()
{
	grcSampleManager::InitClient();

	m_Sema = sysIpcCreateSema(true);

	// wait until a connection is made to ragCompression
	while ( true )
	{
		int port = bkRemotePacket::GetRagSocketPort( COMPRESSION_PORT_OFFSET );
		Displayf( "Connecting to CompressionTest port %d... ", port );

		fiHandle handle = fiDeviceTcpIp::Connect( fiDeviceTcpIp::GetLocalHost(), port );
		if ( handle == fiHandleInvalid )
		{
			sysIpcSleep( 100 );
		}
		else
		{
			sysIpcWaitSema( m_Sema );
			m_TcpHandle = handle;
			sysIpcSignalSema( m_Sema );
			Displayf( "Connected." );
			break;
		}
	}

	m_pCompressionFactory = rage_new smplCompressionFactory();

	// turn on copying so that we can retain the compressed data for display purposes
	m_pCompressionFactory->SetCopyCompressedData( true );

	// register our types
	m_pCompressionFactory->RegisterCompressionData( rage_new smplFiCompressionData );
	m_pCompressionFactory->RegisterCompressionData( rage_new smplDatCompressionData );
}

void rmcSampleCompression::ShutdownClient()
{
	if ( m_pCompressionFactory )
	{
		delete m_pCompressionFactory;
		m_pCompressionFactory = NULL;
	}

	sysIpcWaitSema( m_Sema );
	if ( fiIsValidHandle(m_TcpHandle) )
	{
		fiDeviceTcpIp::GetInstance().Close( m_TcpHandle );
	}
	sysIpcSignalSema( m_Sema );

	grcSampleManager::ShutdownClient();
}

void rmcSampleCompression::DisplayReceivedMessage( const smplCompressionData *cData )
{
	Printf( "\n" );
	Displayf( "Message received %s %d bytes decompressed to %d bytes", cData->GetHeaderPrefix(), 
		cData->GetCompressedSize(), cData->GetDecompressedSize() );

	const u8* compressedData = cData->GetCompressedData();
	if ( m_pCompressionFactory->GetCopyCompressedData() && compressedData )
	{
		char *dataStr = rage_new char[ cData->GetCompressedSize() * 3 + 1 ];
		dataStr[0] = 0;
		for ( u32 i = 0; i < cData->GetCompressedSize(); ++i )
		{
			char buf[4];
			sprintf( buf, "%02X ", compressedData[i] );

			// 480 is a magic number so that we don't overrun the Rag display text message.
			if ( strlen(dataStr) + strlen(buf) >= 480 )
			{
				Printf( "%s", dataStr );
				dataStr[0] = 0;
			}

			strcat( dataStr, buf );
		}
		Printf( "%s\n", dataStr );
		delete dataStr;
	}

	if ( cData->GetDecompressedSize() > 0 )
	{
		const char *dataStr = (char *)cData->GetDecompressedData();
		int len = strlen(dataStr);
		if ( len >= 480 )
		{
			int index = 0;
			char buf[480];

			while ( index <= len )
			{
				strncpy( buf, &(dataStr[index]), 479 );
				buf[479] = 0;
				index += 479;

				Printf( "%s", buf );
			}

			Printf( "\n" );
		}
		else
		{
			Displayf( "%s", dataStr );
		}

		Printf( "\n" );
	}
}

void rmcSampleCompression::UpdateClient()
{
	grcSampleManager::UpdateClient();

	// receive and decompress all messages
	sysIpcWaitSema( m_Sema );
	if ( fiIsValidHandle(m_TcpHandle) )
	{
		int readCount = fiDeviceTcpIp::GetReadCount( m_TcpHandle );
		if ( readCount > 0 )
		{
			u8* readData = rage_new u8[readCount];

			// read the received data into our buffer
			if ( fiDeviceTcpIp::GetInstance().SafeRead( m_TcpHandle, readData, readCount ) )
			{
				m_pCompressionFactory->DecompressData( readData, readCount );

				// decompress all messages within the buffer				
				atArray<smplCompressionData*> &data = m_pCompressionFactory->GetData();
				for ( int i = 0; i < data.GetCount(); ++i )
				{
					// print out the contents of each message
					DisplayReceivedMessage( data[i] );

					// delete each item, we don't need it anymore
					delete data[i];
					data[i] = NULL;
				}

				// clear the data array for the next operation
				data.clear();				
			}
			
			// clean up
			delete readData;
		}
	}
	sysIpcSignalSema( m_Sema );
}

void rmcSampleCompression::AddMessage( const char* buf )
{
	sysIpcWaitSema( m_Sema );

	if ( m_flushOption != NONE )
	{
		switch ( m_alternationMethod )
		{
		case DATA:
			m_compressionMethod = DATA_COMPRESSION;
			break;
		case FILE:
			m_compressionMethod = FILE_COMPRESSION;
			break;
		case ALTERNATE:
			m_compressionMethod = (m_compressionMethod == FILE_COMPRESSION) ? DATA_COMPRESSION : FILE_COMPRESSION;
			break;
		case RANDOM:
			m_compressionMethod = (g_ReplayRand.GetRanged( 0, 100 ) <= 50) ? FILE_COMPRESSION : DATA_COMPRESSION;
			break;
		}
	}

	if ( m_flushOption == AFTER_EACH )
	{
		u32 dataSize;
		u8* data = NULL;
		
		smplDatCompressionData dData;
		smplFiCompressionData fData;
		smplCompressionData* cData;
		if ( m_compressionMethod == FILE_COMPRESSION ) 
		{
			cData = &fData;
		}
		else
		{
			cData = &dData;
		}

		// use the type function for compression; we don't need it in the factory's data array
		data = m_pCompressionFactory->CompressData( cData->GetHeaderPrefix(), (u8*)buf, (u32)strlen(buf) + 1, dataSize );
		
		// send the message right away
		if ( data && fiIsValidHandle(m_TcpHandle) )
		{
			bool result = fiDeviceTcpIp::GetInstance().SafeWrite( m_TcpHandle, data, dataSize );
			if ( !result )
			{
				Displayf( "Error sending message %s", buf );
			}
			delete data;    // cleanup
		}
	}
	else if ( m_flushOption == AFTER_ALL )
	{
		// create a new one
		smplCompressionData* cData;
		if ( m_compressionMethod == FILE_COMPRESSION ) 
		{
			cData = rage_new smplFiCompressionData;
		}
		else
		{
			cData = rage_new smplDatCompressionData;
		}

		// copy is true because buf is a temporary
		u32 len = (u32)strlen(buf) + 1; // +1 to make sure we get '\0'
		cData->SetDecompressedData( (u8*)buf, len, len, true );	

		if ( cData->Compress() )
		{
			// add to the list to send later
			m_pCompressionFactory->GetData().Grow() = cData;
		}
		else
		{
			Displayf( "Error compressing message %s", buf );
			delete cData;
		}
	}
		
	sysIpcSignalSema( m_Sema );
}

void rmcSampleCompression::SendMessages()
{
	sysIpcWaitSema( m_Sema );
	if ( m_flushOption == AFTER_ALL )
	{
		// use the factory's member function to compress everything in its data array
		u32 bufSize;
		u8* buf = m_pCompressionFactory->CompressData( bufSize );

		// now send the entire thing
		if ( buf && fiIsValidHandle(m_TcpHandle) )
		{
			fiDeviceTcpIp::GetInstance().SafeWrite( m_TcpHandle, buf, bufSize );
			delete buf;
		}

		// clear the data array for the next operation
		atArray<smplCompressionData*> &data = m_pCompressionFactory->GetData();
		for ( int i = 0; i < data.GetCount(); ++i )
		{
			delete data[i];
			data[i] = NULL;
		}
		data.clear();
	}
	sysIpcSignalSema( m_Sema );
}

#if __BANK

void rmcSampleCompression::AddWidgetsClient()
{
	grcSampleManager::AddWidgetsClient();

	m_flushOption = AFTER_EACH;

	const char* compressionOptionText[4] = { "Data", "File", "Alternate", "Random" };
	const char* flushOptionText[3] = { "None", "After Each Message", "After all Messages" };

	bkBank &bk = BANKMGR.CreateBank( "Compression Test" );
	{
		bk.AddCombo( "Compression Type", &m_alternationMethod, 4, compressionOptionText );
		bk.AddCombo( "Flush Option", &m_flushOption, 3, flushOptionText );
		bk.AddButton( "Send Small Message", datCallback(MFA(rmcSampleCompression::SendSmallMessage),this) );
		bk.AddButton( "Send 256byte Message", datCallback(MFA(rmcSampleCompression::Send256ByteMessage),this) );
		bk.AddButton( "Send 512byte Message", datCallback(MFA(rmcSampleCompression::Send512ByteMessage),this) );
		bk.AddButton( "Send 1k Message", datCallback(MFA(rmcSampleCompression::Send1KMessage),this) );
		bk.AddButton( "Send 2k Message", datCallback(MFA(rmcSampleCompression::Send2KMessage),this) );
		bk.AddButton( "Send 5k Message", datCallback(MFA(rmcSampleCompression::Send5KMessage),this) );
		bk.AddButton( "Send two 256byte Messages", datCallback(MFA(rmcSampleCompression::SendTwo256ByteMessages),this) );
		bk.AddButton( "Send three 1k Messages", datCallback(MFA(rmcSampleCompression::SendThree1KMessages),this) );
		bk.AddButton( "Send two 5k Messages", datCallback(MFA(rmcSampleCompression::SendTwo5KMessages),this) );
		bk.AddButton( "Send five 5k Messages", datCallback(MFA(rmcSampleCompression::SendFive5KMessages),this) );
	}
}

void rmcSampleCompression::SendSmallMessage()
{
	char buf[256];
	strcpy( buf, "How now brown cow" );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::Send256ByteMessage()
{
	char buf[256];
	strcpy( buf, "A long time ago (around 1994 or so) I went looking for information about Dr. Seuss on the net. I was"
		" expecting to find a lot; after all, he seemed like a natural subject for people to create pages abo"
		"ut. I didn't find any." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::Send512ByteMessage()
{
	char buf[512];
	strcpy( buf, "A long time ago (around 1994 or so) I went looking for information about Dr. Seuss on the net. I was"
		" expecting to find a lot; after all, he seemed like a natural subject for people to create pages abo"
		"ut. I didn't find any. So, in a fit of despair, disappointment and desperation, I set about creating"
		" one. I gathered together a few parodies I found on the net, copied in all the entries from the curr"
		"ent Books in Print did a couple of searches in the Internet Movie Database, and put it all in one pl"
		"ace." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::Send1KMessage()
{
	char buf[1024];
	strcpy( buf, "A long time ago (around 1994 or so) I went looking for information about Dr. Seuss on the net. I was"
		" expecting to find a lot; after all, he seemed like a natural subject for people to create pages abo"
		"ut. I didn't find any. So, in a fit of despair, disappointment and desperation, I set about creating"
		" one. I gathered together a few parodies I found on the net, copied in all the entries from the curr"
		"ent Books in Print did a couple of searches in the Internet Movie Database, and put it all in one pl"
		"ace. This inspired people to do two things. 1) Send me email thanking me for having this page, and 2"
		") Inspired them to create their own Dr. Seuss pages, having seen how empty mine were.  Move to 1997."
		" There are a number of Dr. Seuss sites out there. Some good, some bad, some official, some not. Mine"
		" still seems to be the only one with parodies on it, but y'know, that's not saying much these days. "
		"So, in a fit of frustration, and after-work energy, I go about re-vamping the pages." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::Send2KMessage()
{
	char buf[2*1024];
	strcpy( buf, "Geisel was born on March 2, 1904 in Springfield, Massachusetts. He grew up at 74 Fairfield Street, a"
		"n ideal location for a youngster, as it was only six blocks from the zoo where his father worked. Fu"
		"rthermore, 74 Fairfield was but three blocks from the library. He graduated from Dartmouth College i"
		"n 1925, where he was a member of Sigma Phi Epsilon, the Casque & Gauntlet Society, and wrote for the"
		" Dartmouth Jack O'Lantern humor magazine under his own name and the pen name 'Seuss.' He entered Lin"
		"coln College, Oxford, intending to earn a doctorate in literature. At Oxford he met Helen Palmer, ma"
		"rried her in 1927, and returned to the United States without earning his doctorate. He began submitt"
		"ing humorous articles and illustrations to Judge (a humor magazine), The Saturday Evening Post, Life"
		", Vanity Fair, and Liberty. One notable 'Technocracy Number' made fun of Technocracy, Inc. and featu"
		"red satirical rhymes at the expense of Frederick Soddy. He became nationally famous from his adverti"
		"sements for Flit, a common insecticide at the time. His slogan, 'Quick, Henry, the Flit!' became a p"
		"opular catchphrase. Geisel supported himself and his wife through the Great Depression by drawing ad"
		"vertising for General Electric, NBC, Standard Oil, and many other companies. He also wrote and drew "
		"a short lived comic strip called Hejji in 1935.  Even at this early stage, Geisel had started using "
		"the pen name 'Dr. Seuss'. His first work signed as 'Dr. Seuss' appeared six months into his work for"
		" Judge. Seuss was his mother's maiden name; as an immigrant from Germany, she would have pronounced "
		"it more or less as 'zoice' (as it is pronounced in German), but today it is universally pronounced i"
		"n English with an initial s sound and rhyming with 'juice'.[1] The 'Dr.' is an acknowledgement of hi"
		"s father's unfulfilled hopes that Seuss would earn a doctorate at Oxford." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::Send5KMessage()
{
	char buf[5*1024];
	strcpy( buf, "Geisel was born on March 2, 1904 in Springfield, Massachusetts. He grew up at 74 Fairfield Street, a"
		"n ideal location for a youngster, as it was only six blocks from the zoo where his father worked. Fu"
		"rthermore, 74 Fairfield was but three blocks from the library. He graduated from Dartmouth College i"
		"n 1925, where he was a member of Sigma Phi Epsilon, the Casque & Gauntlet Society, and wrote for the"
		" Dartmouth Jack O'Lantern humor magazine under his own name and the pen name 'Seuss.' He entered Lin"
		"coln College, Oxford, intending to earn a doctorate in literature. At Oxford he met Helen Palmer, ma"
		"rried her in 1927, and returned to the United States without earning his doctorate. He began submitt"
		"ing humorous articles and illustrations to Judge (a humor magazine), The Saturday Evening Post, Life"
		", Vanity Fair, and Liberty. One notable 'Technocracy Number' made fun of Technocracy, Inc. and featu"
		"red satirical rhymes at the expense of Frederick Soddy. He became nationally famous from his adverti"
		"sements for Flit, a common insecticide at the time. His slogan, 'Quick, Henry, the Flit!' became a p"
		"opular catchphrase. Geisel supported himself and his wife through the Great Depression by drawing ad"
		"vertising for General Electric, NBC, Standard Oil, and many other companies. He also wrote and drew "
		"a short lived comic strip called Hejji in 1935.  Even at this early stage, Geisel had started using "
		"the pen name 'Dr. Seuss'. His first work signed as 'Dr. Seuss' appeared six months into his work for"
		" Judge. Seuss was his mother's maiden name; as an immigrant from Germany, she would have pronounced "
		"it more or less as 'zoice' (as it is pronounced in German), but today it is universally pronounced i"
		"n English with an initial s sound and rhyming with 'juice'.[1] The 'Dr.' is an acknowledgement of hi"
		"s father's unfulfilled hopes that Seuss would earn a doctorate at Oxford. Geisel also used the pen n"
		"ame Theo. LeSieg (Geisel spelled backwards) for books he wrote but others illustrated.  In 1936, whi"
		"le Seuss sailed again to Europe, the rhythm of the ship's engines inspired the poem that became his "
		"first book, And to Think That I Saw It on Mulberry Street. Seuss wrote three more children's books b"
		"efore World War II (see list of works below), two of which are, atypically for him, in prose.  As Wo"
		"rld War II began, Dr. Seuss turned to political cartoons, drawing over 400 in two years as editorial"
		" cartoonist for the left-wing New York City daily newspaper, PM. Dr. Seuss's political cartoons oppo"
		"sed the viciousness of Hitler and Mussolini and were highly critical of isolationists, most notably "
		"Charles Lindbergh, who opposed American entry into the war. Some cartoons depicted Japanese American"
		"s as traitors. One of these appeared days before the internments started.  In 1942, Dr. Seuss turned"
		" his energies to direct support of the U.S. war effort. First, he worked drawing posters for the Tre"
		"asury Department and the War Production Board. Then, in 1943, he joined the Army and was commander o"
		"f the Animation Dept of the First Motion Picture Unit of the United States Army Air Forces, where he"
		" wrote films that included 'Your Job in Germany,' a 1945 propaganda film about peace in Europe after"
		" World War II, 'Design for Death,' a study of Japanese culture that won the Academy Award for Best D"
		"ocumentary in 1947, and the Private Snafu series of adult army training films. While in the Army, he"
		" was awarded the Legion of Merit. Dr. Seuss's non-military films from around this time were also wel"
		"l-received; Gerald McBoing-Boing won the Academy Award for Best Short Subject (Animated) in 1950.  D"
		"espite his numerous awards, Dr. Seuss never won the Caldecott Medal nor the Newbery. Three of his ti"
		"tles were chosen as Caldecott runners-up (now referred to as Caldecott Honor books): McElligot's Poo"
		"l (1947), Bartholomew and the Oobleck (1949), and If I Ran the Zoo (1950).  After the war, Dr. Seuss"
		" and his wife moved to La Jolla, California. Returning to children's books, he wrote what many consi"
		"der to be his finest works, including such favorites as If I Ran the Zoo, (1950), Scrambled Eggs Sup"
		"er! (1953), On Beyond Zebra! (1955), If I Ran the Circus (1956), and How the Grinch Stole Christmas!"
		" (1957).  At the same time, an important development occurred that influenced much of Seuss's later "
		"work. In May 1954, Life magazine published a report on illiteracy among school children, which concl"
		"uded that children were not learning to read because their books were boring. Accordingly, Seuss's p"
		"ublisher made up a list of 400 words he felt were important and asked Dr. Seuss to cut the list to 2"
		"50 words and write a book using only those words. Nine months later, Seuss, using 220 of the words g"
		"iven to him, completed The Cat in the Hat. This book was a tour de force-it retained the drawing sty"
		"le, verse rhythms, and all the imaginative power of Seuss's earlier works, but because of its simpli"
		"fied vocabulary could be read by beginning readers." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::SendTwo256ByteMessages()
{
	char buf[256];
	strcpy( buf, "A long time ago (around 1994 or so) I went looking for information about Dr. Seuss on the net. I was"
		" expecting to find a lot; after all, he seemed like a natural subject for people to create pages abo"
		"ut. I didn't find any." );
	AddMessage( buf );

	strcpy( buf, "Geisel was born on March 2, 1904 in Springfield, Massachusetts. He grew up at 74 Fairfield Street, a"
		"n ideal location for a youngster, as it was only six blocks from the zoo where his father worked." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::SendThree1KMessages()
{
	char buf[1024];
	strcpy( buf, "A long time ago (around 1994 or so) I went looking for information about Dr. Seuss on the net. I was"
		" expecting to find a lot; after all, he seemed like a natural subject for people to create pages abo"
		"ut. I didn't find any. So, in a fit of despair, disappointment and desperation, I set about creating"
		" one. I gathered together a few parodies I found on the net, copied in all the entries from the curr"
		"ent Books in Print did a couple of searches in the Internet Movie Database, and put it all in one pl"
		"ace. This inspired people to do two things. 1) Send me email thanking me for having this page, and 2"
		") Inspired them to create their own Dr. Seuss pages, having seen how empty mine were.  Move to 1997."
		" There are a number of Dr. Seuss sites out there. Some good, some bad, some official, some not. Mine"
		" still seems to be the only one with parodies on it, but y'know, that's not saying much these days. "
		"So, in a fit of frustration, and after-work energy, I go about re-vamping the pages." );
	AddMessage( buf );

	strcpy( buf, "Geisel was born on March 2, 1904 in Springfield, Massachusetts. He grew up at 74 Fairfield Street, a"
		"n ideal location for a youngster, as it was only six blocks from the zoo where his father worked. Fu"
		"rthermore, 74 Fairfield was but three blocks from the library. He graduated from Dartmouth College i"
		"n 1925, where he was a member of Sigma Phi Epsilon, the Casque & Gauntlet Society, and wrote for the"
		" Dartmouth Jack O'Lantern humor magazine under his own name and the pen name 'Seuss.' He entered Lin"
		"coln College, Oxford, intending to earn a doctorate in literature. At Oxford he met Helen Palmer, ma"
		"rried her in 1927, and returned to the United States without earning his doctorate. He began submitt"
		"ing humorous articles and illustrations to Judge (a humor magazine), The Saturday Evening Post, Life"
		", Vanity Fair, and Liberty. One notable 'Technocracy Number' made fun of Technocracy, Inc. and featu"
		"red satirical rhymes at the expense of Frederick Soddy." );
	AddMessage( buf );

	strcpy( buf, "Seuss was his mother's maiden name; as an immigrant from Germany, she would have pronounced "
		"it more or less as 'zoice' (as it is pronounced in German), but today it is universally pronounced i"
		"n English with an initial s sound and rhyming with 'juice'.[1] The 'Dr.' is an acknowledgement of hi"
		"s father's unfulfilled hopes that Seuss would earn a doctorate at Oxford. Geisel also used the pen n"
		"ame Theo. LeSieg (Geisel spelled backwards) for books he wrote but others illustrated.  In 1936, whi"
		"le Seuss sailed again to Europe, the rhythm of the ship's engines inspired the poem that became his "
		"first book, And to Think That I Saw It on Mulberry Street. Seuss wrote three more children's books b"
		"efore World War II (see list of works below), two of which are, atypically for him, in prose.  As Wo"
		"rld War II began, Dr. Seuss turned to political cartoons, drawing over 400 in two years as editorial"
		" cartoonist for the left-wing New York City daily newspaper, PM." );
	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::SendFive5KMessages()
{
	char buf[5*1024];
	strcpy( buf, "Geisel was born on March 2, 1904 in Springfield, Massachusetts. He grew up at 74 Fairfield Street, a"
		"n ideal location for a youngster, as it was only six blocks from the zoo where his father worked. Fu"
		"rthermore, 74 Fairfield was but three blocks from the library. He graduated from Dartmouth College i"
		"n 1925, where he was a member of Sigma Phi Epsilon, the Casque & Gauntlet Society, and wrote for the"
		" Dartmouth Jack O'Lantern humor magazine under his own name and the pen name 'Seuss.' He entered Lin"
		"coln College, Oxford, intending to earn a doctorate in literature. At Oxford he met Helen Palmer, ma"
		"rried her in 1927, and returned to the United States without earning his doctorate. He began submitt"
		"ing humorous articles and illustrations to Judge (a humor magazine), The Saturday Evening Post, Life"
		", Vanity Fair, and Liberty. One notable 'Technocracy Number' made fun of Technocracy, Inc. and featu"
		"red satirical rhymes at the expense of Frederick Soddy. He became nationally famous from his adverti"
		"sements for Flit, a common insecticide at the time. His slogan, 'Quick, Henry, the Flit!' became a p"
		"opular catchphrase. Geisel supported himself and his wife through the Great Depression by drawing ad"
		"vertising for General Electric, NBC, Standard Oil, and many other companies. He also wrote and drew "
		"a short lived comic strip called Hejji in 1935.  Even at this early stage, Geisel had started using "
		"the pen name 'Dr. Seuss'. His first work signed as 'Dr. Seuss' appeared six months into his work for"
		" Judge. Seuss was his mother's maiden name; as an immigrant from Germany, she would have pronounced "
		"it more or less as 'zoice' (as it is pronounced in German), but today it is universally pronounced i"
		"n English with an initial s sound and rhyming with 'juice'.[1] The 'Dr.' is an acknowledgement of hi"
		"s father's unfulfilled hopes that Seuss would earn a doctorate at Oxford. Geisel also used the pen n"
		"ame Theo. LeSieg (Geisel spelled backwards) for books he wrote but others illustrated.  In 1936, whi"
		"le Seuss sailed again to Europe, the rhythm of the ship's engines inspired the poem that became his "
		"first book, And to Think That I Saw It on Mulberry Street. Seuss wrote three more children's books b"
		"efore World War II (see list of works below), two of which are, atypically for him, in prose.  As Wo"
		"rld War II began, Dr. Seuss turned to political cartoons, drawing over 400 in two years as editorial"
		" cartoonist for the left-wing New York City daily newspaper, PM. Dr. Seuss's political cartoons oppo"
		"sed the viciousness of Hitler and Mussolini and were highly critical of isolationists, most notably "
		"Charles Lindbergh, who opposed American entry into the war. Some cartoons depicted Japanese American"
		"s as traitors. One of these appeared days before the internments started.  In 1942, Dr. Seuss turned"
		" his energies to direct support of the U.S. war effort. First, he worked drawing posters for the Tre"
		"asury Department and the War Production Board. Then, in 1943, he joined the Army and was commander o"
		"f the Animation Dept of the First Motion Picture Unit of the United States Army Air Forces, where he"
		" wrote films that included 'Your Job in Germany,' a 1945 propaganda film about peace in Europe after"
		" World War II, 'Design for Death,' a study of Japanese culture that won the Academy Award for Best D"
		"ocumentary in 1947, and the Private Snafu series of adult army training films. While in the Army, he"
		" was awarded the Legion of Merit. Dr. Seuss's non-military films from around this time were also wel"
		"l-received; Gerald McBoing-Boing won the Academy Award for Best Short Subject (Animated) in 1950.  D"
		"espite his numerous awards, Dr. Seuss never won the Caldecott Medal nor the Newbery. Three of his ti"
		"tles were chosen as Caldecott runners-up (now referred to as Caldecott Honor books): McElligot's Poo"
		"l (1947), Bartholomew and the Oobleck (1949), and If I Ran the Zoo (1950).  After the war, Dr. Seuss"
		" and his wife moved to La Jolla, California. Returning to children's books, he wrote what many consi"
		"der to be his finest works, including such favorites as If I Ran the Zoo, (1950), Scrambled Eggs Sup"
		"er! (1953), On Beyond Zebra! (1955), If I Ran the Circus (1956), and How the Grinch Stole Christmas!"
		" (1957).  At the same time, an important development occurred that influenced much of Seuss's later "
		"work. In May 1954, Life magazine published a report on illiteracy among school children, which concl"
		"uded that children were not learning to read because their books were boring. Accordingly, Seuss's p"
		"ublisher made up a list of 400 words he felt were important and asked Dr. Seuss to cut the list to 2"
		"50 words and write a book using only those words. Nine months later, Seuss, using 220 of the words g"
		"iven to him, completed The Cat in the Hat. This book was a tour de force-it retained the drawing sty"
		"le, verse rhythms, and all the imaginative power of Seuss's earlier works, but because of its simpli"
		"fied vocabulary could be read by beginning readers." );
	AddMessage( buf );

	AddMessage( buf );

	AddMessage( buf );

	AddMessage( buf );

	AddMessage( buf );

	SendMessages();
}

void rmcSampleCompression::SendTwo5KMessages()
{
	char buf[5*1024];
	strcpy( buf, "Geisel was born on March 2, 1904 in Springfield, Massachusetts. He grew up at 74 Fairfield Street, a"
		"n ideal location for a youngster, as it was only six blocks from the zoo where his father worked. Fu"
		"rthermore, 74 Fairfield was but three blocks from the library. He graduated from Dartmouth College i"
		"n 1925, where he was a member of Sigma Phi Epsilon, the Casque & Gauntlet Society, and wrote for the"
		" Dartmouth Jack O'Lantern humor magazine under his own name and the pen name 'Seuss.' He entered Lin"
		"coln College, Oxford, intending to earn a doctorate in literature. At Oxford he met Helen Palmer, ma"
		"rried her in 1927, and returned to the United States without earning his doctorate. He began submitt"
		"ing humorous articles and illustrations to Judge (a humor magazine), The Saturday Evening Post, Life"
		", Vanity Fair, and Liberty. One notable 'Technocracy Number' made fun of Technocracy, Inc. and featu"
		"red satirical rhymes at the expense of Frederick Soddy. He became nationally famous from his adverti"
		"sements for Flit, a common insecticide at the time. His slogan, 'Quick, Henry, the Flit!' became a p"
		"opular catchphrase. Geisel supported himself and his wife through the Great Depression by drawing ad"
		"vertising for General Electric, NBC, Standard Oil, and many other companies. He also wrote and drew "
		"a short lived comic strip called Hejji in 1935.  Even at this early stage, Geisel had started using "
		"the pen name 'Dr. Seuss'. His first work signed as 'Dr. Seuss' appeared six months into his work for"
		" Judge. Seuss was his mother's maiden name; as an immigrant from Germany, she would have pronounced "
		"it more or less as 'zoice' (as it is pronounced in German), but today it is universally pronounced i"
		"n English with an initial s sound and rhyming with 'juice'.[1] The 'Dr.' is an acknowledgement of hi"
		"s father's unfulfilled hopes that Seuss would earn a doctorate at Oxford. Geisel also used the pen n"
		"ame Theo. LeSieg (Geisel spelled backwards) for books he wrote but others illustrated.  In 1936, whi"
		"le Seuss sailed again to Europe, the rhythm of the ship's engines inspired the poem that became his "
		"first book, And to Think That I Saw It on Mulberry Street. Seuss wrote three more children's books b"
		"efore World War II (see list of works below), two of which are, atypically for him, in prose.  As Wo"
		"rld War II began, Dr. Seuss turned to political cartoons, drawing over 400 in two years as editorial"
		" cartoonist for the left-wing New York City daily newspaper, PM. Dr. Seuss's political cartoons oppo"
		"sed the viciousness of Hitler and Mussolini and were highly critical of isolationists, most notably "
		"Charles Lindbergh, who opposed American entry into the war. Some cartoons depicted Japanese American"
		"s as traitors. One of these appeared days before the internments started.  In 1942, Dr. Seuss turned"
		" his energies to direct support of the U.S. war effort. First, he worked drawing posters for the Tre"
		"asury Department and the War Production Board. Then, in 1943, he joined the Army and was commander o"
		"f the Animation Dept of the First Motion Picture Unit of the United States Army Air Forces, where he"
		" wrote films that included 'Your Job in Germany,' a 1945 propaganda film about peace in Europe after"
		" World War II, 'Design for Death,' a study of Japanese culture that won the Academy Award for Best D"
		"ocumentary in 1947, and the Private Snafu series of adult army training films. While in the Army, he"
		" was awarded the Legion of Merit. Dr. Seuss's non-military films from around this time were also wel"
		"l-received; Gerald McBoing-Boing won the Academy Award for Best Short Subject (Animated) in 1950.  D"
		"espite his numerous awards, Dr. Seuss never won the Caldecott Medal nor the Newbery. Three of his ti"
		"tles were chosen as Caldecott runners-up (now referred to as Caldecott Honor books): McElligot's Poo"
		"l (1947), Bartholomew and the Oobleck (1949), and If I Ran the Zoo (1950).  After the war, Dr. Seuss"
		" and his wife moved to La Jolla, California. Returning to children's books, he wrote what many consi"
		"der to be his finest works, including such favorites as If I Ran the Zoo, (1950), Scrambled Eggs Sup"
		"er! (1953), On Beyond Zebra! (1955), If I Ran the Circus (1956), and How the Grinch Stole Christmas!"
		" (1957).  At the same time, an important development occurred that influenced much of Seuss's later "
		"work. In May 1954, Life magazine published a report on illiteracy among school children, which concl"
		"uded that children were not learning to read because their books were boring. Accordingly, Seuss's p"
		"ublisher made up a list of 400 words he felt were important and asked Dr. Seuss to cut the list to 2"
		"50 words and write a book using only those words. Nine months later, Seuss, using 220 of the words g"
		"iven to him, completed The Cat in the Hat. This book was a tour de force-it retained the drawing sty"
		"le, verse rhythms, and all the imaginative power of Seuss's earlier works, but because of its simpli"
		"fied vocabulary could be read by beginning readers." );
	AddMessage( buf );

	AddMessage( buf );

	SendMessages();
}

#endif // __BANK

#endif // __DEV | __BANK

} // namespace ragesamples
