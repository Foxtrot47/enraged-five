// 
// sample_ragcompression/sample_compressiontest.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
//#############################################################################
// The purpose of this sample is to test the network compression code between
// the game and a Rag Plugin.
//
// REQUIRED:  Build rage/base/tools/ragCompressionTest.  This will put a Rag Plugin
//  called CompressionTest.dll into your tools/base/exes/rag/Plugins directory.
//
// Launch tools/base/exes/rag/rag.exe and run this sample with -rag on the command line.
//
// A bank called Compression Test is created by the sample.  Each button will 
// compress a predefined message and send it to the Compression Plugin.  Upon 
// receipt of that message, the Plugin will recompress the data and echo it back 
// to the game for display in the Output window.  Try different Flush Options
// to see how they effect the message send and receives.
//
// From the Plugins menu, select Compression Test to launch the Compression 
// Plugin and view the data received from the game.
//#############################################################################

#include "system/main.h"
#include "system/param.h"

#include "sample_ragcompression.h"

using namespace ragesamples;

int Main()
{
#if __DEV | __BANK
	rmcSampleCompression sc;
	sc.Init("$\\sample_grcore");

	sc.UpdateLoop();

	sc.Shutdown();
#endif // __DEV | __BANK

	return 0;
}
