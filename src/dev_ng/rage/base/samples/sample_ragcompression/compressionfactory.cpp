// 
// sample_ragcompression/compressionfactory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "compressionfactory.h"
#include "compressiondata.h"

using namespace rage;

namespace ragesamples
{

smplCompressionFactory::smplCompressionFactory( u32 typeCapacity, u32 arrayCapacity )
	: m_copyCompressedData(false)
	, m_readData(NULL)
	, m_readDataSize(0)
	, m_start(0)
	, m_end(0)
{
	m_registeredTypes.Reserve( typeCapacity );
	m_compressionData.Reserve( arrayCapacity );
}

smplCompressionFactory::~smplCompressionFactory()
{
	if ( m_readData )
	{
		delete m_readData;
		m_readData = NULL;
	}

	m_readDataSize = 0;

	m_compressionData.clear();

	atStringMap<smplCompressionData*>::Iterator end = m_registeredTypes.End();
	for ( atStringMap<smplCompressionData*>::Iterator iter = m_registeredTypes.Begin(); iter != end; ++iter )
	{
		delete *iter;
	}
	m_registeredTypes.Reset();
}

void smplCompressionFactory::RegisterCompressionData( smplCompressionData *cData )
{
	m_registeredTypes.SafeInsert( cData->GetHeaderPrefix(), cData );
	m_registeredTypes.FinishInsertion();
}

u8* smplCompressionFactory::CompressData( u32 &dataSize )
{
	int count = m_compressionData.GetCount();

	// Compress if needed and determine total length
	dataSize = 0;
	for ( int i = 0; i < count; ++i )
	{
		smplCompressionData* cData = m_compressionData[i];
		if ( cData->GetCompressedSize() == 0 )
		{
			cData->Compress();
		}

		if ( cData->GetCompressedSize() > 0 )
		{
			dataSize += cData->GetHeaderSize() + cData->GetCompressedSize();
		}
	}

	if ( dataSize == 0 )
	{
		return NULL;
	}

	// build the whole stream
	u8* compressedStream = rage_new u8[dataSize];
	memset( compressedStream, 0, dataSize );
	u8* writePtr = compressedStream;
	for ( int i = 0; i < count; ++i )
	{
		smplCompressionData* cData = m_compressionData[i];
		if ( cData->GetCompressedSize() > 0 )
		{
			int headerSize = cData->BuildHeader( writePtr, (compressedStream + dataSize) - writePtr );
			writePtr += headerSize;
			memcpy( writePtr, cData->GetCompressedData(), cData->GetCompressedSize() );
			writePtr += cData->GetCompressedSize();
		}
	}

	return compressedStream;
}

bool smplCompressionFactory::DecompressData( u8* data, u32 dataSize )
{
	bool rtn = false;

	bool ownsReadData = false;
	if ( m_readData == NULL )
	{
		m_readData = data;
		m_readDataSize = dataSize;
	}
	else
	{
		u32 newSize = dataSize + m_readDataSize;
		u8* newData = rage_new u8[newSize];
		u8* writePtr = newData;
		memcpy( writePtr, m_readData, m_readDataSize );
		writePtr += m_readDataSize;
		memcpy( writePtr, data, dataSize );

		delete m_readData;
		m_readData = newData;
		m_readDataSize = newSize;
		ownsReadData = true;
	}

	while ( true )
	{
		m_start = -1;

		char* startStr = (char *)0xffffffff;
		smplCompressionData* cData = NULL;
		char* readDataStr = (char *)(&(m_readData[m_end]));
		
		// loop through our registered types and try to match a prefix, the earliest if multiple are found
		atStringMap<smplCompressionData*>::Iterator end = m_registeredTypes.End();
		for ( atStringMap<smplCompressionData*>::Iterator iter = m_registeredTypes.Begin(); iter != end; ++iter )
		{
			char* s = strstr( readDataStr, (*iter)->GetHeaderPrefix() );
			if ( (s != NULL) && (s < startStr) )
			{
				startStr = s;
				cData = *iter;
			}
		}
		
		if ( cData != NULL )
		{
			m_start = startStr - (char *)m_readData;

			if ( cData->ParseHeader( &(m_readData[m_start]), m_readDataSize - m_start ) )
			{
				smplCompressionData* newData = cData->CreateNewInstance();

				newData->SetCompressedData( &(m_readData[m_start + cData->GetHeaderSize()]), 
					cData->GetCompressedSize(), cData->GetCompressedSize(), m_copyCompressedData );

				// have to pass in decompressedSize because we didn't call newData->ParseHeader(...)
				if ( newData->Decompress( cData->GetDecompressedSize() ) )
				{
					m_compressionData.Grow() = newData;
					m_end = m_start + cData->GetHeaderSize() + newData->GetCompressedSize();
				}
				else
				{
					Displayf( "Decompression failed" );
				}
			}
			else
			{
				// more data to receive for this set of compressed data
				rtn = true;
				break;
			}
		}
		else
		{
			// end of string
			rtn = false;
			break;
		}
	}

	if ( m_start > -1 )
	{
		// remove data we've already processed
		int newSize = m_readDataSize - m_start;
		u8* newData = rage_new u8[newSize];
		memcpy( newData, &(m_readData[m_start]), newSize );

		if ( ownsReadData )
		{
			delete m_readData;
		}

		m_readData = newData;
		m_readDataSize = newSize;
		m_start = m_end = 0;
	}
	else
	{
		m_start = m_end = 0;
		m_readData = NULL;
		m_readDataSize = 0;
	}

	return rtn;
}

u8* smplCompressionFactory::CompressData( const char* type, u8* uncompressedData, u32 uncompressedSize, u32 &dataSize )
{
	smplCompressionData** cData = m_registeredTypes.SafeGet( type );
	if ( (*cData) == NULL )
	{
		return NULL;
	}

	(*cData)->SetDecompressedData( uncompressedData, uncompressedSize, uncompressedSize );

	int headerSize = (*cData)->GetHeaderSize();
	u32 dataLength = (*cData)->GetCompressUpperBound( uncompressedSize ) + headerSize;
	u8* compressedStream = rage_new u8[dataLength];

	// use our buffer so we don't have to create a new one to add the header
	(*cData)->SetCompressedData( &(compressedStream[headerSize]), dataLength - headerSize );

	if ( (*cData)->Compress() )
	{
		int hSize = (*cData)->BuildHeader( compressedStream, dataLength );
		Assert( hSize == headerSize );

		dataSize = (*cData)->GetCompressedSize() + hSize;

		// zero out so we don't get confused or try to delete something we shouldn't
		(*cData)->SetDecompressedData( NULL, 0 );
		(*cData)->SetCompressedData( NULL, 0 );

		return compressedStream;
	}

	return NULL;
}

//#############################################################################

} // namespace ragesamples
