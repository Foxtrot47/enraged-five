// 
// sample_ragcompression/sample_ragcompression.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_RAGCOMPRESSION_SAMPLE_RAGCOMPRESSION_H
#define SAMPLE_RAGCOMPRESSION_SAMPLE_RAGCOMPRESSION_H

#include "sample_grcore/sample_grcore.h"

#include "file/tcpip.h"
#include "system/ipc.h"

namespace ragesamples
{

#if __DEV | __BANK

class smplCompressionFactory;
class smplCompressionData;

class rmcSampleCompression : public ragesamples::grcSampleManager
{
public:
	rmcSampleCompression();
	~rmcSampleCompression();

protected:
	virtual const char* GetSampleName() const
	{
		return "Compression Example";
	}

	enum ECompressionMethod
	{
		DATA_COMPRESSION,
		FILE_COMPRESSION,
	};

	enum EAlternationMethod
	{
		DATA,
		FILE,
		ALTERNATE,
		RANDOM,
	};

	enum EFlushOptions
	{
		NONE,
		AFTER_EACH,
		AFTER_ALL
	};

	virtual void InitClient();
	virtual void ShutdownClient();
	virtual void UpdateClient();

	void AddMessage( const char* buf );	
	void SendMessages();
	void DisplayReceivedMessage( const smplCompressionData *cData );

#if __BANK
	virtual void AddWidgetsClient();
#endif // __BANK


private:
#if __BANK
	void SendSmallMessage();
	void Send256ByteMessage();
	void Send512ByteMessage();
	void Send1KMessage();
	void Send2KMessage();
	void Send5KMessage();
	void SendTwo256ByteMessages();
	void SendThree1KMessages();
	void SendFive5KMessages();
	void SendTwo5KMessages();
#endif // __BANK

	ECompressionMethod m_compressionMethod;
	int m_alternationMethod;
	fiHandle m_TcpHandle;
	sysIpcSema m_Sema;
	int m_flushOption;
	ragesamples::smplCompressionFactory* m_pCompressionFactory;
};

#endif // __DEV | __BANK

} // namespace ragesamples

#endif // SAMPLE_RAGCOMPRESSION_SAMPLE_RAGCOMPRESSION_H
