// 
// sample_ragcompression/compressionfactory.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_RAGCOMPRESSION_COMPRESSIONFACTORY_H
#define SAMPLE_RAGCOMPRESSION_COMPRESSIONFACTORY_H

#include "atl/array.h"
#include "atl/binmap.h"

using namespace rage;

namespace ragesamples
{

class smplCompressionData;

// PURPOSE: A class to construct and destruct multiple sets of compressed and uncompressed
//  data into consecutive streams.  Mirrored in C# in rage/base/tools/ragCompressionTest, 
//  this class serves as a test bed for sysCompressionFactory.
//
//  Classes derived from smplCompressionData must be registered in order to operate the 
//  compress and decompress functions.
class smplCompressionFactory
{
public:
	smplCompressionFactory( u32 typeCapacity=4, u32 dataCapacity=16 );
	~smplCompressionFactory();

	// PURPOSE: Adds a derived smplCompressionData class to the types that can be
	//  handled by this factory.
	// PARAMS:
	//  data - the derived class to register.
	void RegisterCompressionData( smplCompressionData *cData );

	// PURPOSE: Retrieves the list of smplCompressionData objects.  This can be used
	//  to grow the list before calling CompressData or to read the list after calling
	//  DecompressData.  Clearing the list and deleting every item is up to the user.
	// RETURNS: array of smplCompressionData objects
	atArray<smplCompressionData*>& GetData();

	// PURPOSE: Return the state of m_copyCompressedData
	// RETURNS: m_copyCompressedData
	bool GetCopyCompressedData() const;

	// PURPOSE: Specifies whether each smplCompressionData should hold a copy of the compressed data when decompressed
	//  via DecompressData.
	// PARAMS: b - true or false
	void SetCopyCompressedData( bool b );

	// PURPOSE: Using all items in the smplCompressionData list, builds one massive stream out of each set of compressed data,
	//  adding the header info to each one.  Compression is performed if it wasn't already.
	// PARAMS:
	//  dataSize - return by reference the size of the buffer returned
	// RETURNS: the compressed data.  null on error.
	// NOTE:  The user will be responsible for deleting the buffer that was returned
	u8* CompressData( u32 &dataSize );

	// PURPOSE: Takes in an array of bytes and processes each compressed stream, decompressing as it goes.
	//  Multiple sets of compressed data could exist in the data array, and a partial set may exist at the end
	//  of the array.  When this partial data exists, we return true.  Call DecompressData again with the rest of the data
	//  (example: reading from a Socket) to process the remaining data and continue.
	// PARAMS:
	//  data - the data we want decompressed
	//  dataSize - length of data or the number of bytes we want processed
	// RETURNS: true if not all of the data could be processed
	bool DecompressData( u8* data, u32 dataSize );

	// PURPOSE: Compresses the given data and adds the header.  This will not add an entry to the compression data list.
	//  The user is responsible for deleting the memory.
	// PARAMS:
	//  type - which registered smplCompressionData to use.  What is returned by smplCompressionData::GetHeaderPrefix();
	//  uncompressedData - data that we want to compress
	//  uncompressedSize - number of bytes to compress
	//  dataSize - return by reference the size of the buffer returned
	// RETURNS: the compressed data.  null on error.
	// NOTE:  The user will be responsible for deleting the buffer that was returned
	u8* CompressData( const char* type, u8* uncompressedData, u32 uncompressedSize, u32 &dataSize );

private:
	bool m_copyCompressedData;
	u8* m_readData;
	int m_readDataSize;
	int m_start;
	int m_end;
	atStringMap<smplCompressionData*> m_registeredTypes;
	atArray<smplCompressionData*> m_compressionData;
};

inline atArray<smplCompressionData*>& smplCompressionFactory::GetData() 
{ 
	return m_compressionData; 
}

inline bool smplCompressionFactory::GetCopyCompressedData() const 
{ 
	return m_copyCompressedData; 
}

inline void smplCompressionFactory::SetCopyCompressedData( bool b ) 
{ 
	m_copyCompressedData = b; 
}

} // namespace ragesamples

#endif // SAMPLE_RAGCOMPRESSION_COMPRESSIONFACTORY_H
