// 
// sample_ragcompression/ficompressiondata.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "ficompressiondata.h"

#include <string.h>

#include "file/compress.h"
#include "file/decompress.h"

#include "system/new.h"

#if __DEV
#include "system/timer.h"
#endif

using namespace rage;
using namespace ragesamples;

namespace ragesamples
{

smplFiCompressionData::smplFiCompressionData()
{
}

smplFiCompressionData::~smplFiCompressionData()
{
}

smplFiCompressionData* smplFiCompressionData::CreateNewInstance() const
{
	return rage_new smplFiCompressionData;
}

u32 smplFiCompressionData::GetCompressUpperBound( u32 uncompressedSize ) const
{
	return fiCompressUpperBound( uncompressedSize );
}

int smplFiCompressionData::BuildHeader( u8* buf, int ASSERT_ONLY(length) ) const
{
	const char* prefix = GetHeaderPrefix();
	int len = strlen( prefix );
	for ( int i = 0; i < len; ++i )
	{
		buf[i] = prefix[i];
	}

	int index = len;

	u32 compressedSize = GetCompressedSize();
	buf[index++] = (u8)(compressedSize);
	buf[index++] = (u8)(compressedSize >> 8);
	buf[index++] = (u8)(compressedSize >> 16);
	buf[index++] = (u8)(compressedSize >> 24);
	buf[index++] = ':';

	u32 decompressedSize = GetDecompressedSize();
	buf[index++] = (u8)(decompressedSize);
	buf[index++] = (u8)(decompressedSize >> 8);
	buf[index++] = (u8)(decompressedSize >> 16);
	buf[index++] = (u8)(decompressedSize >> 24);
	buf[index++] = ':';

	Assert( index < length );
	return index;
}

bool smplFiCompressionData::ParseHeader( u8* data, u32 dataLength )
{
	if ( dataLength < (u32)GetHeaderSize() )
	{
		// not enough data
		return false;
	}

	const char* prefix = GetHeaderPrefix();
	int len = strlen( prefix );
	for ( int i = 0; i < len; ++i )
	{
		if ( data[i] != prefix[i] )
		{
			// bad header prefix
			return false;
		}
	}

	int index = len;
	
	u32 cSize = data[index++];
	cSize |= data[index++] << 8;
	cSize |= data[index++] << 16;
	cSize |= data[index++] << 24;

	if ( data[index++] != ':' )
	{
		// bad header
		return false;
	}

	u32 dSize = data[index++];
	dSize |= data[index++] << 8;
	dSize |= data[index++] << 16;
	dSize |= data[index++] << 24;

	if ( data[index++] != ':' )
	{
		// bad header
		return false;
	}

	if ( dataLength < GetHeaderSize() + cSize )
	{
		// not enough data
		return false;
	}

	SetCompressedSize( cSize );
	SetDecompressedSize( dSize );
	return true;
}

u32 smplFiCompressionData::Compress( u8* compressedData, u32 compressedDataLength, u8* uncompressedData, u32 uncompressedSize )
{
	memset( compressedData, 0, compressedDataLength );
	
	DEV_ONLY( sysTimer::BeginBenchmark() );	
	DEV_ONLY( sysTimer t );
	u32 compressedSize = fiCompress( compressedData, compressedDataLength, uncompressedData, uncompressedSize, 16, false );
	DEV_ONLY( float time = t.GetMsTime() );
	DEV_ONLY( sysTimer::EndBenchmark() );
	DEV_ONLY( Displayf( "Compress %s took %f ms", GetHeaderPrefix(), time ) );

	Assert( compressedSize <= compressedDataLength );
	return compressedSize;
}

bool smplFiCompressionData::Decompress( u8* decompressedData, u32 decompressedSize, u8* compressedData, u32 compressedSize )
{
	memset( decompressedData, 0, decompressedSize );

	DEV_ONLY( sysTimer::BeginBenchmark() );	
	DEV_ONLY( sysTimer t );
	bool success = fiDecompress( decompressedData, decompressedSize, compressedData, compressedSize );
	DEV_ONLY( float time = t.GetMsTime() );
	DEV_ONLY( sysTimer::EndBenchmark() );
	DEV_ONLY( Displayf( "Decompress %s took %f ms", GetHeaderPrefix(), time ) );

	return success;
}

} // namespace ragesamples
