// 
// sample_spatialdata/sample_bdam_iter.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/array.h"
#include "atl/atfunctor.h"
#include "spatialdata/bdamtree.h"
#include "system/main.h"
#include "system/param.h"

#include <algorithm>

using namespace rage;

PARAM(row, "");
PARAM(col, "");
PARAM(lod, "");
PARAM(dir, "");
PARAM(format, "");
PARAM(radius, "");
PARAM(size, "");
PARAM(location, "");
PARAM(minLod, "");
PARAM(maxLod, "");
PARAM(battleship, "");

const char* g_FormatString = NULL;

atArray<spdBdamLocation> g_BdamLocationArray;

bool TraversalFunc(const spdBdamLocation& loc)
{
	// Comment this out to only get the leaf nodes
	g_BdamLocationArray.Grow() = loc;
	if (loc.m_Level == 0) {
		return false;
	}
	return true;
}

void IterateAtLocation(spdBdamLocation root)
{
	spdBdamTreelessLocationIterator iter;
	atFunctor1<bool, const spdBdamLocation&> travCb;

	spdBdamSinglePointSplit* splitObj = NULL;


	// If we have a point of interest, use it
	if (PARAM_location.Get())
	{
		splitObj = rage_new spdBdamSinglePointSplit;
		float loc[3];
		PARAM_location.GetArray(loc, 3);
		splitObj->m_PointOfInterest.Set(loc[0], loc[1], loc[2]);
		PARAM_minLod.Get(splitObj->m_MinLod);
		PARAM_maxLod.Get(splitObj->m_MaxLod);
		PARAM_size.Get(splitObj->m_SplitSize);

		travCb.Reset<spdBdamSinglePointSplit, &spdBdamSinglePointSplit::SplitCallback>(splitObj);
	}
	else
	{
		// This func will put all nodes (not just leaf nodes) into the g_BdamLocationArray
		travCb.Reset<&TraversalFunc>();
	}
	iter.Init(root, travCb);

	do
	{
		g_BdamLocationArray.Grow() = iter.GetCurrent();
	}
	while(iter.MoveNext());

	delete splitObj;
}

void WriteLocations()
{
	g_BdamLocationArray.QSort(0, g_BdamLocationArray.GetCount(), &spdBdamLocation::Compare);
	for(int i = 0; i < g_BdamLocationArray.GetCount(); i++) {
		char buf[256];
		g_BdamLocationArray[i].FormatLocationString(buf, g_FormatString);
		Printf(buf);
		Printf("\n");
	}
}

int Main()
{
	int row = 0;
	int col = 0;
	int lod = 0;
	const char* dirName = NULL;
	const char* battleship = NULL;

	g_FormatString = "Level %L, Row %R, Col %C, Dir %D";

	PARAM_row.Get(row);
	PARAM_col.Get(col);
	PARAM_lod.Get(lod);
	PARAM_dir.Get(dirName);
	PARAM_format.Get(g_FormatString);
	PARAM_battleship.Get(battleship);

	if (battleship)
	{
		// Assume 512m battleship tiles, 128m LOD 0
		int b_row = battleship[0] - 'A';
		int b_col = atoi(battleship+1);
		row = b_row - 10;
		col = b_col - 15;

		lod = 4;

		spdBdamLocation dirN(lod, row, col, spdBdamLocation::DIR_N);
		spdBdamLocation dirS(lod, row, col, spdBdamLocation::DIR_S);

		spdBdamLocation parentA, parentB;
		dirN.GetParentLocation(parentA);
		dirS.GetParentLocation(parentB);

		IterateAtLocation(parentA);
		IterateAtLocation(parentB);
	}
	else if (PARAM_radius.Get() && PARAM_location.Get() && PARAM_maxLod.Get())
	{
		// find starting, ending row and column. Call iterateatlocation on all cells within radius.
		// Also cull locations outside of radius
		int maxLod = 5;
		PARAM_maxLod.Get(maxLod);

		float size = 128.0f;
		PARAM_size.Get(size);

		float gridSize = size * (1 << (maxLod >> 1));

		float radius = 0.0f;
		PARAM_radius.Get(radius);

		Vector3 location;
		PARAM_location.GetArray(&location.x, 3);

		int minCol, maxCol;
		
		minCol = (int)floorf((location.x - radius) / gridSize);
		maxCol = (int)floorf((location.x + radius) / gridSize) + 1;

		int minRow, maxRow;

		minRow = (int)floorf((location.z - radius) / gridSize);
		maxRow = (int)floorf((location.z + radius) / gridSize) + 1;

		for(int i = minRow; i < maxRow; i++) {
			for(int j = minCol; j < maxCol; j++) {
				if (maxLod % 2 == 0) {
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_N));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_E));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_S));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_W));
				}
				else
				{
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_NE));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_EN));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_ES));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_SE));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_SW));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_WS));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_WN));
					IterateAtLocation(spdBdamLocation(maxLod, i, j, spdBdamLocation::DIR_NW));
				}
			}
		}
	}
	else
	{
		spdBdamLocation::Direction dir = spdBdamLocation::GetDirFromString(dirName);

		spdBdamLocation loc(lod, row, col, dir);

		IterateAtLocation(loc);
	}

	WriteLocations();

	return 0;
}
