// 
// sample_spatialdata/sample_mnbvh.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#define _USE_UNIT_TESTING 0

#include "../../base\samples\sample_grcore\sample_grcore.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "system/main.h"
#include "system/param.h"
#include "grcore/im.h"
#include "grcore/stateblock.h"

#if _USE_UNIT_TESTING
#include "testspatialdata\test_mnbvh.h"
#include "unittestplusplus\UnitTest++.h"
#endif

#include "spatialdata\mnbvh.h"
#include "math/random.h"
#include "vectormath/legacyconvert.h"

#if _USE_UNIT_TESTING
PARAM( performUnitTest, "run the unit tests on this sample" );
PARAM( performUnitTestOnly, "just run the unit tests on this sample" );
#endif

using namespace rage;

MNBVHNode* g_ArrayBuffer;
int		   g_ArrayCount;

class grcSampleRasterizer : public ragesamples::grcSampleManager
{

public: 
	grcSampleRasterizer() : m_vizualizer("Debug MNBVH")
	{
	}
	void InitClient()
	{
		m_sceneIndex = 0;
#if __ASSERT
		m_numObjects = 100;
#else
		m_numObjects = 10000;
#endif
		m_tree = 0;
		m_oldNumObjects = -1;
		m_boxSize = 1.0f;
		m_boxSpacing = 40.0f;
		m_boxes = 0;
		m_drawBoxes = true;
		m_testAABB = true;
		m_testRay = false;
		m_maxPerLeaf = 16;
		m_oldMaxPerLeaf = -1;

		m_aabbIntersectTime=0.f;
		m_rayIntersectTime=0.f;
		m_useArrayAllocation = false;
		m_oldArrayAllocation = true;
	}


	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
	}

	void TestScene()
	{

		if ( m_testAABB)
		{
			mthRandom rand(14);

			ScalarV size = ScalarVFromF32( m_boxSize * 5.0f );
			ScalarV negsize = -size;
			ScalarV spacing = ScalarVFromF32(m_boxSpacing );
			int numAABBTests = 20;
			spdMNBVHBounds*  results[1024];
			for (int i =0; i < numAABBTests; i++)
			{
				Vec3V offset = rand.Get3FloatsV() * spacing;
				Vec3V p1 = rand.Get3FloatsV() * negsize + offset;
				Vec3V p2 = rand.Get3FloatsV() * size + offset;
				Vec3V bmin = Min( p1,p2);
				Vec3V bmax = Max( p1,p2);
				int amtResults = MNBVHIntersectBox( m_tree, bmin, bmax, results);

				Color32 red( 255,255,0,128);
				grcWorldIdentity();
				grcDrawBox( VEC3V_TO_VECTOR3(bmin), VEC3V_TO_VECTOR3(bmax), Color32(255,255,255,255) );
				for (int i = 0; i < amtResults; i++)
				{
					grcWorldIdentity();
					grcDrawSolidBox( VEC3V_TO_VECTOR3(results[i]->m_bmin),VEC3V_TO_VECTOR3( results[i]->m_bmax), red );
				}
			}

			sysTimer	timeAABBs;
			m_totalIntersections=0;
			for (int i =0; i < numAABBTests; i++)
			{
				Vec3V offset = rand.Get3FloatsV() * spacing;
				Vec3V p1 = rand.Get3FloatsV() * negsize + offset;
				Vec3V p2 = rand.Get3FloatsV() * size + offset;
				Vec3V bmin = Min( p1,p2);
				Vec3V bmax = Max( p1,p2);
				int amtResults = MNBVHIntersectBox( m_tree, bmin, bmax, results);
				m_totalIntersections +=amtResults;
			}
			m_aabbIntersectTime = timeAABBs.GetMsTime();


		}
		if ( m_testRay)
		{
			mthRandom rand(14);

			ScalarV spacing = ScalarVFromF32(m_boxSpacing );
			int numAABBTests = 20;
			spdMNBVHBounds*  results[1024];
			for (int i =0; i < numAABBTests; i++)
			{
				Vec3V start = rand.Get3FloatsV() * spacing;
				Vec3V end = rand.Get3FloatsV() * spacing;
				int amtResults = MNBVHIntersectRay( m_tree, start, end, results);

				Color32 red( 0,255,255, 128);
				grcWorldIdentity();
				grcDrawLine( VEC3V_TO_VECTOR3(start),VEC3V_TO_VECTOR3(end), Color32(255,255,255,255) );
				for (int i = 0; i < amtResults; i++)
				{
					grcWorldIdentity();
					grcDrawSolidBox( VEC3V_TO_VECTOR3(results[i]->m_bmin),VEC3V_TO_VECTOR3( results[i]->m_bmax), red );
				}
			}

			sysTimer	timeRay;
			for (int i =0; i < numAABBTests; i++)
			{
				Vec3V start = rand.Get3FloatsV() * spacing;
				Vec3V end = rand.Get3FloatsV() * spacing;
				int amtResults = MNBVHIntersectRay( m_tree, start, end, results);
				m_totalIntersections+=amtResults;
			}
			m_rayIntersectTime = timeRay.GetMsTime();
		}
	}
	// Our own memory management routinies
	static MNBVHNode* ArrayAllocator(int /*depth*/)
	{
		return &g_ArrayBuffer[g_ArrayCount++];
	}
	static void ArrayDeletor(const MNBVHNode*) {};

	// uses stack to get node, and increases count, this is way to get
	// the exact number of nodes used by the tree which only taking very little memory
	static MNBVHNode* MNBVHStackAllocator(int depth)
	{
		Assert( depth < 8); // needs around 1 bizillion elements to be higher than a depth of 8
		g_ArrayCount++;
		return &g_ArrayBuffer[depth];
	}

	bool GenerateScene()
	{
		if ( m_oldSceneId == m_sceneIndex && m_numObjects == m_oldNumObjects && m_maxPerLeaf == m_oldMaxPerLeaf
				&& m_useArrayAllocation== m_oldArrayAllocation )
		{
			return false;
		}
		
		MNBVHDeleteNode(m_tree, m_oldArrayAllocation ? ArrayDeletor : MNBVHDeleteDefault );

		
		
		delete [] m_boxes;
		m_boxes = rage_aligned_new(128) spdMNBVHBounds[m_numObjects];

		spdMNBVHBounds* boxes2 = rage_aligned_new(128) spdMNBVHBounds[m_numObjects];
		

		mthRandom rand(12);
		ScalarV size = ScalarVFromF32( m_boxSize );
		ScalarV spacing = ScalarVFromF32( m_boxSpacing );

		for (int i =0; i < m_numObjects; i++)
		{
			Vec3V offset = rand.Get3FloatsV() * spacing;
			Vec3V p1 = rand.Get3FloatsV() * size + offset;
			Vec3V p2 = rand.Get3FloatsV() * size + offset;
			m_boxes[i].m_bmin = Min( p1,p2);
			m_boxes[i].m_bmax = Max( p1,p2);
			m_boxes[i].m_data = m_boxes + i;	
			boxes2[i] = m_boxes[i];
		}

		sysTimer buildTimer;
		spdMNBVHBoundList	lst( boxes2, boxes2 + m_numObjects);

		if ( m_useArrayAllocation)
		{
			delete g_ArrayBuffer;
			MNBVHNode	stackBuffer[8];
			g_ArrayBuffer = stackBuffer;

			g_ArrayCount = 0;
			MNBVHBuildMultiNodeBVHTree( lst, MNBVHStackAllocator, m_maxPerLeaf );

			//int maxNumNodes = MNBVHCalcMaxNodes(m_numObjects);
			int maxNumNodes = g_ArrayCount;
			g_ArrayBuffer = rage_aligned_new(128) MNBVHNode[maxNumNodes];

			g_ArrayCount = 0;
			Displayf("Allocating %i Nodes ( Max Nodes %i ) ", maxNumNodes,MNBVHCalcMaxNodes(m_numObjects) );
		}

		MNBVHNode::AllocateFunction allocator = m_useArrayAllocation ? ArrayAllocator : MNBVHAllocateDefault;

		m_tree = MNBVHBuildMultiNodeBVHTree( lst,  allocator, m_maxPerLeaf );
		
		Displayf("Tree Build Time for %i Objects : %f", m_numObjects, buildTimer.GetMsTime() );

		if ( m_useArrayAllocation)
		{
			Assert(g_ArrayCount <= MNBVHCalcMaxNodes(m_numObjects)  );
		}
#if __BANK
		m_vizualizer.Calc( m_tree);
#endif

		m_oldSceneId = m_sceneIndex;
		m_oldNumObjects = m_numObjects;
		m_oldMaxPerLeaf = m_maxPerLeaf;
		m_oldArrayAllocation = m_useArrayAllocation;

		delete [] boxes2;
		return true;
	}
	void DrawScene()
	{
		// STEP #3. Set our transformation matrix.
		GenerateScene();
		TestScene();

	
		grcWorldIdentity();
		//-- Set the matrix that gets applied to all subsequent geometry.
		if ( m_drawBoxes)
		{
			Color32 red( 0,255,0);
			for (int i = 0; i < m_numObjects; i++)
			{
				grcDrawBox( VEC3V_TO_VECTOR3(m_boxes[i].m_bmin),VEC3V_TO_VECTOR3( m_boxes[i].m_bmax), red );
			}
		}
		grcWorldIdentity();

		grcStateBlock::SetBlendState( grcStateBlock::BS_Normal);
#if __BANK
		m_vizualizer.DebugDraw(m_tree);
#endif
	}

	void DrawClient()
	{
		DrawScene();
	}

	void ShutdownClient()
	{
		MNBVHDeleteNode(m_tree, m_useArrayAllocation ? ArrayDeletor : MNBVHDeleteDefault);
		delete [] m_boxes;
		delete g_ArrayBuffer;
	}

#if __BANK
	void AddWidgetsClient()
	{
		bkBank& bank = BANKMGR.GetInstance().CreateBank("MNBVH");
		bank.AddSlider("Scene Index", &m_sceneIndex, 0,6,1);
		bank.AddSlider("Number Objects", &m_numObjects, 0,100000,1);

		bank.AddToggle("Draw Boxes", &m_drawBoxes );
		bank.AddSlider("Box Spacing", &m_boxSpacing, 0.0f,10000.0f,0.5f);
		bank.AddSlider("Box Size", &m_boxSize, 0.0f,1000.0f,0.01f);		


		bank.AddSlider("Max Per Leaf", &m_maxPerLeaf, 0, MNBVH_NODESIZE, 1);

		bank.AddToggle("AABB test", &m_testAABB );
		bank.AddToggle("Ray test", &m_testRay );	
		bank.AddToggle("Use array allocation", &m_useArrayAllocation);

		bank.AddText("AABB Intersect Time", &m_aabbIntersectTime);
		bank.AddText("Ray Intersect Time", &m_rayIntersectTime);

		
		m_vizualizer.AddWidgets(bank);
	}
#endif

private:
	MNBVHNode*							m_tree;
	spdMNBVHTreeVis						m_vizualizer;
	atArray<spdMNBVHBounds,128,u32>				m_objects;
	atArray<ConstString,128,u32>		m_names;

	int				m_sceneIndex;
	int				m_numObjects;
	int				m_maxPerLeaf;

	int				m_oldSceneId;
	int				m_oldNumObjects;
	int				m_oldMaxPerLeaf;

	int				m_totalIntersections;

	float			m_aabbIntersectTime;
	float			m_rayIntersectTime;

	spdMNBVHBounds* m_boxes;
	float	m_boxSize;
	float	m_boxSpacing;
	bool	m_drawBoxes;
	bool	m_testAABB;
	bool	m_testRay;
	bool    m_useArrayAllocation;
	bool	m_oldArrayAllocation;

};

int Main()
{
#if _USE_UNIT_TESTING
	SpatialData_TestSetup();
	int ret = UnitTest::RunAllTests();
	if (PARAM_performUnitTestOnly.Get())
	{
		return ret;
	}
	if (ret)
	{
		Quitf("Failed on unit Tests (%i)", ret);
	}	
#endif
	grcSampleRasterizer sample;
	sample.Init("sample_mnbvh");
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}
