//
// sample_mesh/viewer_tristrips.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/slider.h"
#include "devcam/polarcam.h"
#include "devcam/trackcam.h"
#include "grcore/im.h"
#include "grcore/image.h"
#include "grcore/setup.h"
#include "input/input.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"
#include "system/param.h"
#include "vector/geometry.h"


using namespace rage;
using namespace ragesamples;


PARAM(strip,"[viewer_tristrips] strip the mesh");
PARAM(dumpascii,"[viewer_tristrips] dump an ascii version of the mesh file");
PARAM(drawWhiteWire,"[viewer_tristrips] start with wire frame drawing on and outline off");

namespace ragesamples
{
	XPARAM(oneframe);
}

#ifndef min
#define min(a, b)	( ((a) < (b)) ? (a) : (b) )
#endif

class mshTriStripViewer : public grcSampleManager
{
	class VertexInfo
	{
	public:
		VertexInfo() :
		  x(0.0f), y(0.0f), z(0.0f),
			  u0(0.0f), v0(0.0f),
			  u1(0.0f), v1(0.0f)
		  {}

		  float x;
		  float y;
		  float z;
		  float u0;
		  float v0;
		  float u1;
		  float v1;
	};

	// the largest multiple of 6 that is less than the maximum
	// number of vertices that can be sent in a single grcBegin call
	static const int maxVerts6 = grcBeginMax - (grcBeginMax % 6);

public:
	mshTriStripViewer() :
		m_Current(1), 
		m_Loaded(-1),
		m_Clean(false),
		m_Strip2(false),
		m_DrawNormals(false),
		m_DrawFaceNormals(false),
		m_DrawChecker(true),
		m_DrawWire(true),
		m_DrawSolid(true),
		m_InitFlag(false),
		m_DrawCenter(true),
		m_RainbowCounter(0),
		m_AnimateFlag(true),
		m_OutlineFlag(true),
		m_LightingFlag(false),   // lighting is turned off by default, so the viewer doesn't start off in the dark.
		m_QuickSave(false),
		m_TristripVis(false),
		m_TotalTriangles(0),
		m_MaterialIdx(0), 
		m_TristripIdx(0), 
		m_TriIdx(0),
		m_TriVertIdx(0),
		m_NormalLength(0.03f)
	{
	}

	void InitClient()
	{
		m_CurrentFilename[0] = '\0';
		m_Current = sysParam::GetArgCount() - 1;

		m_TristripHilite.Set(1.0f, 1.0f, 0.0f),
		m_TriHilite.Set(0.0f, 0.0f, 1.0f);

		m_Light.Set(0,1,0); 

#if __BANK
		m_MaterialIdxSlider = NULL;
		m_TristripIdxSlider = NULL;
		m_TriIdxSlider = NULL;
		m_MeshBank = NULL;
		m_MainWidgetCount = 0;
#endif


		if (PARAM_strip.Get()==true)
		{
			printf("Tristrip2 Save\n");
			m_QuickSave=true;
			PARAM_oneframe.Set("-oneframe");
		}

		if (PARAM_drawWhiteWire.Get()==true)
		{
			m_DrawWire = true;
			m_OutlineFlag = false;
			m_DrawSolid = false;
		}

		int width = 8;
		int height = 8;					
		grcImage *checker = grcImage::Create(width,height,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);

		unsigned char c;
		unsigned char offset=0;
		int counter=0;
		for (int x=0; x< (width*height); x++)
		{
			if ((x+offset)&1) c = 0xff;
			else c = 0x80;

			checker->GetBits()[(x*3)+0] = c;  
			checker->GetBits()[(x*3)+1] = c;
			checker->GetBits()[(x*3)+2] = c;

			counter++;
			if (counter>=width)
			{
				counter=0;
				offset ++;
			}
		}

		//grcTexture *tex = grcTexture::Create(checker);
		m_Tex = grcCreateTexture("mesh_texture");
		// m_Tex->SetFilter(grctfPoint);
	}

	void ShutdownClient()
	{
		m_Tex->Release();
		grcSampleManager::ShutdownClient();
	}

#if __BANK
	void AddWidgetsClient()
	{
		strcpy(m_CurrentFilename, sysParam::GetArg(m_Current));
		m_MeshBank  = &BANKMGR.CreateBank("Mesh/View Options");
		m_MeshBank->AddSlider("Size of Normals", &m_NormalLength, 0.f, 1000.f, 0.01f);

		m_MainWidgetCount=0;
		m_MeshBank->AddSlider("Model",&m_Current,1,sysParam::GetArgCount()-1,1);
		m_MainWidgetCount++;
		m_MeshBank->AddText("Filename:",m_CurrentFilename,FILENAME_BUFFER_SIZE);
		m_MainWidgetCount++;
		m_MeshBank->AddButton("Save",datCallback(MFA(mshTriStripViewer::DoSave),this));
		m_MainWidgetCount++;
		m_MeshBank->AddButton("SaveTemp",datCallback(MFA(mshTriStripViewer::DoSaveTemp),this));
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Clean",&m_Clean);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Strip2", &m_Strip2);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Vertex Normals",&m_DrawNormals);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Face Normals",&m_DrawFaceNormals);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Checker",&m_DrawChecker);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Solid",&m_DrawSolid);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Wireframe",&m_DrawWire);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Strip Outline (Turn Wireframe ON)",&m_OutlineFlag);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Animate Strips",&m_AnimateFlag);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Use Cheap Lighting",&m_LightingFlag);
		m_MainWidgetCount++;
		m_MeshBank->AddToggle("Draw Centered",&m_DrawCenter);
		m_MainWidgetCount++;

		bkBank &tristripInfo = BANKMGR.CreateBank("Tristrip Info");

		tristripInfo.AddToggle("Per-Tristrip Visualization", &m_TristripVis);

		m_MaterialIdxSlider = tristripInfo.AddSlider("Material Index", &m_MaterialIdx, 0, 0, 1, datCallback(MFA(mshTriStripViewer::ChangeMaterial), this));

		m_TristripIdxSlider = tristripInfo.AddSlider("Tristrip Index", &m_TristripIdx, 0, 0, 1, datCallback(MFA(mshTriStripViewer::ChangeTristrips), this));

		m_TriIdxSlider = tristripInfo.AddSlider("Triangle Index", &m_TriIdx, 0, 0, 1, datCallback(MFA(mshTriStripViewer::ChangeTriangle), this));

		tristripInfo.PushGroup("Per-Triangle Info");
		tristripInfo.AddSlider("Vertex Index", &m_TriVertIdx, 0, 2, 1, datCallback(MFA(mshTriStripViewer::ChangeTristripTriangleVertex), this));
		tristripInfo.AddSlider("X", &m_CurrentVertex.x, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.AddSlider("Y", &m_CurrentVertex.y, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.AddSlider("Z", &m_CurrentVertex.z, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.AddSlider("U0", &m_CurrentVertex.u0, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.AddSlider("V0", &m_CurrentVertex.v0, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.AddSlider("U1", &m_CurrentVertex.u1, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.AddSlider("V1", &m_CurrentVertex.v1, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.0f);
		tristripInfo.PopGroup();

		tristripInfo.PushGroup("Tristrip Highlight Color", false);
		tristripInfo.AddColor("", &m_TristripHilite);
		tristripInfo.PopGroup();

		tristripInfo.PushGroup("Triangle Highlight Color", false);
		tristripInfo.AddColor("", &m_TriHilite);
		tristripInfo.PopGroup();
	}
#endif

	void LoadMeshAndDumpAscii()
	{
		if (!SerializeFromFile(sysParam::GetArg(m_Current),m_Mesh,"mesh"))
			return;
		DoSaveTemp();
	}


private:

	// Given a primitive, it will draw the UV Map of it on the screen.
	void DrawEdgeMap(const mshMaterial &mtl, const mshPrimitive &prim)
	{
		grcBindTexture(NULL);
		grcColor3f(1,1,1);
		grcBegin(drawLines,prim.Idx.GetCount());
		for (int i=0; i<prim.Idx.GetCount(); i++) 
		{
			Vector2 TexCoord = mtl.GetTexCoord(prim.Idx[i],0);
			Vector3 Position = Vector3( TexCoord.x, 0.0f, TexCoord.y);
			grcVertex3f(Position);
		}
		grcEnd();
	}

	//enum mshPrimType { 
	//	mshPOINTS,		// Each index in primitive defines a point (min 1)
	//	mshLINES,		// Indices in primitive definite independent line segments (min 2, multiple of 2)
	//	mshTRIANGLES,	// Indices in primitive define a mesh of independent triangles (min 3, multiple of 3)
	//	mshTRISTRIP,	// Indices in primitive cumulatively define a normal-winding tristrip (min 3)
	//	mshTRISTRIP2,	// Indices in primitive cumulatively define a reverse-winding tristrip (min 3)
	//	mshQUADS,		// Indices in primitive define a mesh of independent quads (min 4, multiple of 4, in tristrip order).  If fourth index is zero, quad is a triangle.
	//	mshPOLYGON,		// Indices in primitive cumulative define a closed, convex polygon (min 3, as a trifan)
	//	FORCE_32 = 0x7FFFFFFF 
	//};
	// given a mesh, return the # of triangles it consists of.. mainly used for artist..
	int ComputeTriangleCount(const mshMesh &mesh)
	{
		int totalTriangles = 0;
		mshIndex totalMaterials = mesh.GetMtlCount();
		printf("Total Materials = (%d)\n", totalMaterials);
		for (int i =0; i < totalMaterials; i++)
		{
			printf("Material Name = [%s]\n", (const char*)mesh.GetMtl(i).Name);
			int totalPrimitives = mesh.GetMtl(i).Prim.GetCount();
			printf("Total Primitives in this material = (%d)\n", totalPrimitives);
			for (int j=0; j < totalPrimitives; j++)
			{
				//printf("[%d] Type = (%d)\n",j, mesh.GetMtl(i).Prim[j].Type);
				int primType = mesh.GetMtl(i).Prim[j].Type;
				int primIndexCount = mesh.GetMtl(i).Prim[j].Idx.GetCount();
				if (primType == mshTRIANGLES)
				{
					totalTriangles += (int)(primIndexCount / 3);
				}
				if ((primType == mshTRISTRIP) || (primType == mshTRISTRIP2))
				{
					totalTriangles += (int)(primIndexCount-2);
				}
				if (primType == mshQUADS) 
				{
					totalTriangles += (int) (primIndexCount/2);
				}
			}
		}
		printf("Total Triangles = (%d)\n", totalTriangles);
		return totalTriangles;
	}


	//=============================================================================
	//					Draw one tristrip primitive
	//=============================================================================
	void DrawPrim(grcTexture *tex,const mshMaterial &mtl,const mshPrimitive &prim,int color,bool drawSolid, bool drawWireframe,bool hilite) {
		int i, vertsLeft, numVerts;
		int red = (color & 7);
		int green = (color >> 3) & 7;
		int blue = (color >> 6) & 7;

		Vector3 center_offset;
		center_offset.x=0;
		center_offset.y=0;
		center_offset.z=0;

		if (m_DrawCenter)
		{
			center_offset=m_Center;
		}


		//=========================================================================
		//						Draw the TriStrip, solid or with checkerboard.
		//=========================================================================
		if(drawSolid)
		{
			grcBindTexture(tex);
			grcColor3f((red+1) / 8.0f, (green+1) / 8.0f, (blue+1) / 8.0f);
			grcBegin(drawTriStrip, prim.Idx.GetCount());
			for (i=0; i<prim.Idx.GetCount(); i++) {
				float intensity=1.0f;
				if (m_LightingFlag)	// simple shading
				{
					// get the normal.. and calculate the lighting based on this cross product
					Vector3 normal = mtl.GetNormal(prim.Idx[i]);

					intensity = normal.Dot(m_Light);	
					float inverse = (1.0f - intensity)/4;
					intensity -= inverse;	

					//intensity = abs(intensity);
					if (intensity <0) intensity=0;

					if (intensity > 1.0f)
						intensity=1.0f;
				}

				if (m_AnimateFlag)
				{
					int x=i+(m_RainbowCounter>>2)&7;
					if ((x == 7)|| (x==0))
					{
						grcColor3f(((green+1) / 8.0f) *intensity, ((blue+1) / 8.0f)*intensity, ((red+1) / 8.0f)*intensity);
					}
					else
					{
						grcColor3f(((red+1) / 8.0f)*intensity, ((green+1) / 8.0f)*intensity, ((blue+1) / 8.0f)*intensity);
					}
				}
				else
				{
						grcColor3f(((red+1) / 8.0f)*intensity, ((green+1) / 8.0f)*intensity, ((blue+1) / 8.0f)*intensity);
				}

				Vector2 vec(mtl.GetTexCoord(prim.Idx[i],0));
				grcTexCoord2f(vec.x,vec.y);
				grcVertex3f(mtl.GetPos(prim.Idx[i])-center_offset);
			}
			grcEnd();
		}

		//=========================================================================
		//						Draw Wire Frame
		//=========================================================================
		if (drawWireframe) {
			grcBindTexture(NULL);
			if(hilite)
				grcColor3f(m_TristripHilite);
			else
				grcColor3f(1,1,1);
			vertsLeft = (prim.Idx.GetCount() - 2) * 6;
			grcBegin(drawLines, min(vertsLeft, maxVerts6));
			for (i=0,numVerts=0; i<prim.Idx.GetCount()-2; i++,numVerts+=6) {
				if(numVerts == maxVerts6)
				{
					numVerts = 0;
					vertsLeft -= maxVerts6;
					grcEnd();
					grcBegin(drawLines, min(vertsLeft, maxVerts6));
				}
				if(hilite && i == (int)m_TriIdx)
					grcColor3f(m_TriHilite);
				if (m_OutlineFlag) grcColor3f(((red+1) / 8.0f), ((green+1) / 8.0f), ((blue+1) / 8.0f));
				grcVertex3f(mtl.GetPos(prim.Idx[i])-center_offset);
				grcVertex3f(mtl.GetPos(prim.Idx[i+1])-center_offset);
				if (m_OutlineFlag) grcColor3f(1,1,1);
				grcVertex3f(mtl.GetPos(prim.Idx[i+1])-center_offset);
				grcVertex3f(mtl.GetPos(prim.Idx[i+2])-center_offset);
				if (m_OutlineFlag) grcColor3f(1,1,1);
				grcVertex3f(mtl.GetPos(prim.Idx[i+2])-center_offset);
				grcVertex3f(mtl.GetPos(prim.Idx[i])-center_offset);
				if(hilite && i == (int)m_TriIdx)
					grcColor3f(m_TristripHilite);
			}
			grcEnd();
		}
		//=========================================================================
		//							Draw Normals
		//=========================================================================
		if (m_DrawNormals) {
			grcBindTexture(NULL);
			vertsLeft = prim.Idx.GetCount()*2;
			grcBegin(drawLines, min(vertsLeft, maxVerts6)); // 6 is a multiple of 2, might as well use the same constant...
			Vector3 offset;
			for (i=0,numVerts=0; i<prim.Idx.GetCount(); i++) {
				if(numVerts == maxVerts6)
				{
					numVerts = 0;
					vertsLeft -= maxVerts6;
					grcEnd();
					grcBegin(drawLines, min(vertsLeft, maxVerts6));
				}
				grcColor3f(0,0,1);
				grcVertex3f(mtl.GetPos(prim.Idx[i])-center_offset);
				grcColor3f(1,1,1);
				offset.Scale(mtl.GetNormal(prim.Idx[i]), m_NormalLength);				
				grcVertex3f((mtl.GetPos(prim.Idx[i])-center_offset) + offset);
			}
			grcEnd();
		}

		if (m_DrawFaceNormals) {
			grcBindTexture(NULL);
			Vector3 offset;
			switch (prim.Type)
			{
			case mshPOINTS:
				vertsLeft = prim.Idx.GetCount()*2;
				grcBegin(drawLines, min(vertsLeft, maxVerts6)); // 6 is a multiple of 2, might as well use the same constant...
				for (i=0,numVerts=0; i<prim.Idx.GetCount(); i++) {
					grcColor3f(1,0,0);
					grcVertex3f(mtl.GetPos(prim.Idx[i])-center_offset);
					grcColor3f(1,1,1);
					offset.Scale(mtl.GetNormal(prim.Idx[i]), m_NormalLength);
					grcVertex3f((mtl.GetPos(prim.Idx[i])-center_offset) + offset);
				}
				grcEnd();
				break;
			case mshLINES:
			{
				vertsLeft = (prim.Idx.GetCount()/2)*2;
				grcBegin(drawLines, min(vertsLeft, maxVerts6)); // 6 is a multiple of 2, might as well use the same constant...
				Vector3 finalPos,finalNorm;
				for (i=0,numVerts=0; i<prim.Idx.GetCount(); i+=2) {
					finalPos.Add(mtl.GetPos(prim.Idx[i]),mtl.GetPos(prim.Idx[i+1]));
					finalPos.Scale(0.5f);
					finalNorm.Add(mtl.GetNormal(prim.Idx[i]),mtl.GetNormal(prim.Idx[i+1]));
					finalNorm.Scale(0.5f);
					finalNorm.Normalize();
					grcColor3f(1,0,0);
					grcVertex3f(finalPos-center_offset);
					grcColor3f(1,1,1);
					offset.Scale(finalNorm, m_NormalLength);
					grcVertex3f((finalPos-center_offset) + offset);
				}
				grcEnd();
			}
				break;

			case mshTRIANGLES:
				{
					vertsLeft = (prim.Idx.GetCount()/3)*2;
					grcBegin(drawLines, min(vertsLeft, maxVerts6)); // 6 is a multiple of 2, might as well use the same constant...
					Vector3 finalPos,finalNorm;
					for (i=0,numVerts=0; i<prim.Idx.GetCount(); i+=3) {
						finalPos.Add(mtl.GetPos(prim.Idx[i]),mtl.GetPos(prim.Idx[i+1]));
						finalPos.Add(mtl.GetPos(prim.Idx[i+2]));
						finalPos.Scale(1.0f/3.0f);
						finalNorm.Add(mtl.GetNormal(prim.Idx[i]),mtl.GetNormal(prim.Idx[i+1]));
						finalNorm.Add(mtl.GetNormal(prim.Idx[i+2]));
						finalNorm.Scale(1.0f/3.0f);
						finalNorm.Normalize();
						grcColor3f(1,0,0);
						grcVertex3f(finalPos-center_offset);
						grcColor3f(1,1,1);
						offset.Scale(finalNorm, m_NormalLength);
						grcVertex3f((finalPos-center_offset) + offset);
					}
					grcEnd();
				}
				break;

			case mshTRISTRIP:
			case mshTRISTRIP2:
			case mshPOLYGON:
				{
					vertsLeft = (prim.Idx.GetCount()>3?(prim.Idx.GetCount()-2):1)*2;
					grcBegin(drawLines, min(vertsLeft, maxVerts6)); // 6 is a multiple of 2, might as well use the same constant...
					Vector3 finalPos,finalNorm;
					for (i=0,numVerts=0; i<prim.Idx.GetCount()-2; i++) {
						finalPos.Add(mtl.GetPos(prim.Idx[i]),mtl.GetPos(prim.Idx[i+1]));
						finalPos.Add(mtl.GetPos(prim.Idx[i+2]));
						finalPos.Scale(1.0f/3.0f);
						finalNorm.Add(mtl.GetNormal(prim.Idx[i]),mtl.GetNormal(prim.Idx[i+1]));
						finalNorm.Add(mtl.GetNormal(prim.Idx[i+2]));
						finalNorm.Scale(1.0f/3.0f);
						finalNorm.Normalize();
						grcColor3f(1,0,0);
						grcVertex3f(finalPos-center_offset);
						grcColor3f(1,1,1);
						offset.Scale(finalNorm, m_NormalLength);
						grcVertex3f((finalPos-center_offset) + offset);
					}
					grcEnd();
				}
				break;

			case mshQUADS:
				{
					vertsLeft = (prim.Idx.GetCount()/4)*2;
					grcBegin(drawLines, min(vertsLeft, maxVerts6)); // 6 is a multiple of 2, might as well use the same constant...
					Vector3 finalPos,finalNorm;
					for (i=0,numVerts=0; i<prim.Idx.GetCount(); i+=4) {
						finalPos.Add(mtl.GetPos(prim.Idx[i]),mtl.GetPos(prim.Idx[i+1]));
						finalPos.Add(mtl.GetPos(prim.Idx[i+2]));
						finalPos.Add(mtl.GetPos(prim.Idx[i+3]));
						finalPos.Scale(1.0f/4.0f);
						finalNorm.Add(mtl.GetNormal(prim.Idx[i]),mtl.GetNormal(prim.Idx[i+1]));
						finalNorm.Add(mtl.GetNormal(prim.Idx[i+2]));
						finalNorm.Add(mtl.GetNormal(prim.Idx[i+3]));
						finalNorm.Scale(1.0f/4.0f);
						finalNorm.Normalize();
						grcColor3f(1,0,0);
						grcVertex3f(finalPos-center_offset);
						grcColor3f(1,1,1);
						offset.Scale(finalNorm, m_NormalLength);
						grcVertex3f((finalPos-center_offset) + offset);
					}
					grcEnd();
				}
				break;
			default:
				break;
			}
		}
	}

	void setupTristripVis()
	{
		m_MaterialIdx = 0;
	#if __BANK
		m_MaterialIdxSlider->SetRange(0, (float)(m_Mesh.GetMtlCount() - 1));
	#endif

		ChangeMaterial();
	}

	void ChangeMaterial()
	{
		m_TristripIdx = 0;
	#if __BANK
		m_TristripIdxSlider->SetRange(0, (float)(m_Mesh.GetMtl(m_MaterialIdx).Prim.GetCount() - 1));
	#endif

		ChangeTristrips();
	}

	void ChangeTristrips()
	{
		m_TriIdx = 0;
	#if __BANK
		m_TriIdxSlider->SetRange(0, (float)(m_Mesh.GetMtl(m_MaterialIdx).Prim[m_TristripIdx].GetTriangleCount() - 1));
	#endif

		ChangeTriangle();
	}

	void ChangeTriangle()
	{
		m_TriVertIdx = 0;

		ChangeTristripTriangleVertex();
	}

	void ChangeTristripTriangleVertex()
	{
		int idx = m_TriIdx + (int)m_TriVertIdx;
		const mshMaterial &mtl = m_Mesh.GetMtl(m_MaterialIdx);
		const mshVertex &vtx = mtl.GetVertex(mtl.Prim[m_TristripIdx].Idx[idx]);

		const Vector3 &pos = vtx.Pos;
		m_CurrentVertex.x = pos.x;
		m_CurrentVertex.y = pos.y;
		m_CurrentVertex.z = pos.z;

		const Vector2 &t0 = vtx.Tex[0];
		m_CurrentVertex.u0 = t0.x;
		m_CurrentVertex.v0 = t0.y;

		if(vtx.TexCount > 1)
		{
			const Vector2 &t1 = vtx.Tex[1];
			m_CurrentVertex.u1 = t1.x;
			m_CurrentVertex.v1 = t1.y;
		}
	}

	void DoSave(mshMesh *mesh)
	{
	//	char m_CurrentFilename[512];
		//sprintf( m_CurrentFilename , "c:\\rob.mesh");

		Displayf("Saving to: %s", m_CurrentFilename);

		if(SerializeToAsciiFile(m_CurrentFilename, *mesh))
			Displayf("Save Successful.");
		else
			Errorf("Save Failed.");
	}

	void DoSaveTemp()
	{
		char m_CurrentFilename[512];
		sprintf( m_CurrentFilename , "c:\\rob.mesh");

		Displayf("Saving to: %s", m_CurrentFilename);

		if(SerializeToAsciiFile(m_CurrentFilename, m_Mesh))
			Displayf("Save Successful.");
		else
			Errorf("Save Failed.");
	}

	void UpdateClient()
	{
		if (m_Current != m_Loaded) 
		{
			m_Loaded = m_Current;
			// const char *ext = strrchr(sysParam::GetArg(m_Current),'.');
			GetSetup().BeginDraw();
			grcFont::GetCurrent().Draw(400,300,"Working...");
			GetSetup().EndDraw();
			if (!SerializeFromFile(sysParam::GetArg(m_Current),m_Mesh,"mesh"))
				return;
			// Center the object so polarcam is useful
			int i;
#if IS_CONSOLE && 0
			Vector4 sphere;
			ComputeBoundSphere(m_Mesh.GetPosCount(),&m_Mesh.GetPos(0),sphere);
			for (i=0; i<m_Mesh.GetPosCount(); i++)
				m_Mesh.GetPos(i) -= *(Vector3*)&sphere;
#endif	

			// Compute the extents of the mesh, so that we can center the object

			Vector3 min;
			Vector3 max;

			min = m_Mesh.GetMtl(0).GetPos(0);  // initialize min/max with the first valid point.. 
			max = min;

			for (i=0; i<m_Mesh.GetMtlCount(); i++)
			{
				for (int j=0; j<m_Mesh.GetMtl(i).GetVertexCount(); j++) {
					float x = m_Mesh.GetMtl(i).GetPos(j).x;
					float y = m_Mesh.GetMtl(i).GetPos(j).y;
					float z = m_Mesh.GetMtl(i).GetPos(j).z;

					if (x < min.x) min.x =x;
					if (y < min.y) min.y =y;
					if (z < min.z) min.z =z;

					if (x > max.x) max.x =x;
					if (y > max.y) max.y =y;
					if (z > max.z) max.z =z;
				}
			}

			m_Center.x = min.x + (max.x - min.x)/2;
			m_Center.y = min.y + (max.y - min.y)/2;
			m_Center.z = min.z + (max.z - min.z)/2;

			m_Mesh.Tristrip();			// Create Tristrips

#if IS_CONSOLE && 0
			GetCamMgr()
			PC.Init(sphere.w * 2);
#endif

			for (i=0; i<256; i++)
				m_Enable[i] = true;   // m_Enable all the materials to draw.

#if __BANK
			// Add the Material Name Toggle
			m_MeshBank->Truncate(m_MainWidgetCount);
			for (i=0; i<m_Mesh.GetMtlCount(); i++)
				m_MeshBank->AddToggle(m_Mesh.GetMtl(i).Name,&m_Enable[i]);

			setupTristripVis();
			m_TotalTriangles += ComputeTriangleCount(m_Mesh);
#endif
		}

		if (m_Clean) {
			GetSetup().BeginDraw();
			grcFont::GetCurrent().Draw(400,300,"Working...");
			GetSetup().EndDraw();
			m_Mesh.CleanModel();
			m_Mesh.Triangulate();
			m_Mesh.Tristrip();
			m_Clean = false;
		}
		if ((m_Strip2) || (m_QuickSave==1))
		{
			GetSetup().BeginDraw();
			grcFont::GetCurrent().Draw(400,300,"Tristripping...");
			GetSetup().EndDraw();
			m_Mesh.CleanModel();
			m_Mesh.Triangulate();
			m_Mesh.Tristrip();
			m_Strip2 = false;

		}

		if (KEYBOARD.KeyPressed(KEY_N) && m_Current < sysParam::GetArgCount()-1)
			++m_Current;
		if (KEYBOARD.KeyPressed(KEY_P) && m_Current > 1)
			--m_Current;
	}

	void DrawClient()
	{

		Matrix34 mat = GetCamMgr().GetWorldMtx();
		m_Light = mat.c;

		int color = 0;
		int verts = 0;
		int prims = 0;
		int drawnPrims=0;
		for (int i=0; i<m_Mesh.GetMtlCount(); i++)
		{
			color+=(i+1)*121;
			for (int j=0; j<m_Mesh.GetMtl(i).Prim.GetCount(); j++, prims++) {
				color += (j+1) * 13;
				verts += m_Mesh.GetMtl(i).Prim[j].Idx.GetCount();
				if (m_Enable[i])
				{
					drawnPrims++;
					//DrawEdgeMap(m_Mesh, m_Mesh.GetMtl(i).Prim[j]);

					DrawPrim(m_DrawChecker? m_Tex : NULL,m_Mesh.GetMtl(i),m_Mesh.GetMtl(i).Prim[j],color,m_DrawSolid,m_DrawWire,m_TristripVis && i == (int)m_MaterialIdx && j == (int)m_TristripIdx);
				}
			}
		}

		float average = ((float)verts / (float)prims);

		if (average > 13.0f)
		{
			if (m_InitFlag==false)
			{
				m_LightingFlag=true;
			}
		}
		m_InitFlag=true;

		//grcDrawFontf(30,400,"%d adj %d prim %5.2f verts/strip\n'%s'\nMaterial Count %d  drawn prims %d",m_Mesh.GetAdjCount(),prims,(float)verts / (float)prims,sysParam::GetArg(m_Current), m_Mesh.GetMtlCount(), drawnPrims);
		int fontHeight=grcFont::GetCurrent().GetHeight();
		int fontY=1;
		// grcFont::GetCurrent().Drawf( 30,((float)(fontY++*fontHeight))+10.0f+(.1f*fontHeight), "%d adj %d cpv %d total primitives %5.2f verts per strip", m_Mesh.GetAdjCount(), m_Mesh.GetCpvCount(), prims, (float) verts / (float) prims);
		grcFont::GetCurrent().Drawf( 30,((float)(fontY++*fontHeight))+10.0f+(.1f*fontHeight), "Material Count %d   drawn prims %d",m_Mesh.GetMtlCount(), drawnPrims);
		grcFont::GetCurrent().Drawf( 30,((float)(fontY++*fontHeight))+10.0f+(.1f*fontHeight), "File = %s", sysParam::GetArg(m_Current));
		grcFont::GetCurrent().Drawf( 30,((float)(fontY++*fontHeight))+10.0f+(.1f*fontHeight), "Total Triangles = %d", m_TotalTriangles);
		fontY++;
		SetCameraHelpY((fontY++*fontHeight)+10);

		m_RainbowCounter--;
		if (m_QuickSave==1)
		{
			DoSave(&m_Mesh);  // save it out and bail
			// we need to EXIT APP!
		}
	}

	mshMesh m_Mesh;

	// buffer to store the filename to save as
	enum {FILENAME_BUFFER_SIZE=512};
	char m_CurrentFilename[FILENAME_BUFFER_SIZE];
	int m_Current;
	int m_Loaded;
	bool m_Clean;
	bool m_Strip2;
	bool m_DrawNormals;
	bool m_DrawFaceNormals;
	bool m_DrawChecker;
	bool m_DrawWire;
	bool m_DrawSolid;
	bool m_Enable[256];
	bool m_InitFlag;
	bool m_DrawCenter;
	int	 m_RainbowCounter;
	bool m_AnimateFlag;
	bool m_OutlineFlag;
	bool m_LightingFlag;
	Vector3 m_Center; 
	bool m_QuickSave;

	bool m_TristripVis;

	int m_TotalTriangles;
	unsigned int m_MaterialIdx, m_TristripIdx, m_TriIdx;

	unsigned char m_TriVertIdx;

	Vector3 m_TristripHilite,m_TriHilite;

	VertexInfo m_CurrentVertex;

	Vector3 m_Light; 
	float m_NormalLength;
#if __BANK
	bkSlider *m_MaterialIdxSlider, *m_TristripIdxSlider, *m_TriIdxSlider;
	bkBank* m_MeshBank;
	int m_MainWidgetCount;
#endif
	grcTexture *m_Tex;

};


// main application
int Main()
{
	mshTriStripViewer viewer;

	// just dump an ascii version of the mesh file:
	if (PARAM_dumpascii.Get())
	{
		viewer.LoadMeshAndDumpAscii();
	}
	else 
	{
		viewer.Init("sample_mesh/viewer_tristrips");

		viewer.UpdateLoop();

		viewer.Shutdown();
	}

	return 0;
}
