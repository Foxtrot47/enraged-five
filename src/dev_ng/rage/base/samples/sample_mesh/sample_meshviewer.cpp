// 
// /sample_meshviewer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"
#include "system/param.h"


#include "atl/array.h"
#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/light.h"
#include "grcore/quads.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "system/xtl.h"
#include "grcore/device.h"

using namespace rage;
using namespace ragesamples;

PARAM(file, "[sample_mesh] Name of mesh file to load");



class mshViewer : public grcSampleManager {
public:
	mshViewer() : m_NormalLength(0.03f),
				 m_DrawMesh(true),
				 m_DrawNormals(false),
				 m_DrawTangents(false),
				 m_DrawBinormals(false),
				 m_DrawWireframe(false),
				 m_UseShaderMethod(false)
#if __BANK
				 ,m_ShowVertexInfo(false),
				 m_VertIndex(0),
				 m_VertPixelSize(4)
#endif
	{
		/* EMPTY*/
	}
	
	void InitClient() {
		// Init our mesh object
		//const char *filename = "DetectiveDCA_lod_01.mesh";
		const char *filename = "q_13_Q_R_13_14__1a__displacement1.mesh";

		PARAM_file.Get(filename);
		
		if ( SerializeFromFile(filename, m_Mesh, "mesh") == false ) 
		{
			Errorf("Can't load mesh file %s", filename);
		}
		// meshDisplace.Load( filename );

	}

	void DrawClient() {
		// We need a mesh renderer object
		mshRendererGrc r;

		grcWorldIdentity();

		// Draw the mesh
		if ( m_DrawMesh ) {
			if (m_DrawWireframe) {
				grcLightState::SetEnabled(false);

				grcColor3f(Vector3(1.0f, 1.0f, 1.0f));

				for(int i = 0; i < m_Mesh.GetMtlCount(); i++) {
					mshMaterial& mtl = m_Mesh.GetMtl(i);
					for(mshMaterial::TriangleIterator ti = mtl.BeginTriangles(); ti != mtl.EndTriangles(); ++ti) {
						int indices[3];
						ti.GetVertIndices(indices[0], indices[1], indices[2]);

						grcBegin(drawLines, 4);
						grcVertex3f(mtl.GetVertex(indices[0]).Pos);
						grcVertex3f(mtl.GetVertex(indices[1]).Pos);
						grcVertex3f(mtl.GetVertex(indices[2]).Pos);
						grcVertex3f(mtl.GetVertex(indices[0]).Pos);
						grcEnd();
					}
				}
			}
			else {
				m_Mesh.Draw(r);
			}
		}
		//const Matrix34& boundBoxMatrix = m_Mesh.GetBoundBoxMatrix();
		//Matrix34 boundInverse;
		//
		//Vector3 scale = m_Mesh.GetBoundBoxSize();
		//Matrix34	scaledBound = boundBoxMatrix;
		//scale.y = scale.x;// since this is a patch in x z keep uniform transformation in the y
		//scaledBound.Scale( scale );

		//scaledBound.RotateLocalX( PI / 2.0f ); // rotate by 90 degrees

		//boundInverse.Inverse( scaledBound );

		//grcWorldMtx( boundInverse);

		//// change Y axias to Z axias
		//

		// meshDisplace.Render();

		//// Draw the mesh
		//if ( m_DrawMesh ) {
		//	m_Mesh.Draw(r);
		//}

		// Draw normals / tangents / binormals as needed		
		grcLightState::SetEnabled(false);
		Color32 c(255,0,0);
		if ( m_DrawNormals ) {
			m_Mesh.DrawNormals(r, c, m_NormalLength);
		}
		
		if ( m_DrawTangents ) {
			c.Set(0,255,0);
			m_Mesh.DrawTangents(r, c, m_NormalLength);
		}
		
		if ( m_DrawBinormals ) {
			c.Set(0,0,255);
			m_Mesh.DrawBinormals(r, c, m_NormalLength, m_UseShaderMethod);
		}

#if __BANK
		// draw vertex position and bindings (if it's skinned):
		if (m_ShowVertexInfo)
		{
			Vector2 windowPos;
			grcViewport::GetCurrent()->Transform(m_CurrentVertex.Pos,windowPos.x,windowPos.y);

			grcViewport::SetCurrentWorldIdentity();
			PUSH_DEFAULT_SCREEN();
			grcBeginQuads(1);
			grcDrawQuadf(windowPos.x-m_VertPixelSize*0.5f,windowPos.y-m_VertPixelSize*0.5f,
								 windowPos.x+m_VertPixelSize*0.5f,windowPos.y+m_VertPixelSize*0.5f,0.0f,0.0f,0.0f,0.0f,0.0f,Color32(255,255,255));
			grcEndQuads();
			POP_DEFAULT_SCREEN();

			//grcViewport::GetCurrent()->Ortho(-1.0f,1.0f,-1.0f,1.0f,0.0f,1000.0f);

			char vertInfo[1024];
			if (m_Mesh.IsSkinned())
			{
				int numWeights=0;
				for (int i=0;i<m_CurrentVertex.Binding.Mtx.GetCount();i++)
				{

					if (m_CurrentVertex.Binding.Wgt[i]>0.00001f)
					{
						numWeights++;
					}
				}
				sprintf(vertInfo,"Pos: %.3f %.3f %.3f\nWeights (%d):",m_CurrentVertex.Pos.x,m_CurrentVertex.Pos.y,m_CurrentVertex.Pos.z,numWeights);
				for (int i=0;i<m_CurrentVertex.Binding.Mtx.GetCount();i++)
				{
					
					if (m_CurrentVertex.Binding.Wgt[i]>0.00001f)
					{
						char tempStr[32];
						sprintf(tempStr,"%d (%d%%)",m_CurrentVertex.Binding.Mtx[i],(int)(m_CurrentVertex.Binding.Wgt[i]*100));
						strcat(vertInfo,tempStr);
					}
				}
			}
			else 
				sprintf(vertInfo,"Pos: %.3f %.3f %.3f\n",m_CurrentVertex.Pos.x,m_CurrentVertex.Pos.y,m_CurrentVertex.Pos.z);

			grcFont::GetCurrent().DrawScaled(windowPos.x,windowPos.y,0.0f,Color32(255,255,255),1.0f,1.0f,vertInfo);
		}
#endif
	}

	void InitLights() {
		// Turn off the other 2 lights
		grcSampleManager::InitLights();
		m_Lights.SetColor(0, 0.5f, 0.5f, 0.5f);
		m_Lights.SetActiveCount(1);
	}

	void UpdateLights() {
		// Set the light to be the camera's position / orientation
		m_Lights.SetPosition(0, GetCameraMatrix().d);
		m_Lights.SetDirection(0, GetCameraMatrix().c);
	}


#if __BANK
	void FindVertex()
	{
		int vertCount=0;
		for (int i=0;i<m_Mesh.GetMtlCount();i++)
		{
			mshMaterial& mtl=m_Mesh.GetMtl(i);
			if (m_VertIndex<(vertCount+mtl.GetVertexCount()))
			{
				m_CurrentVertex=mtl.GetVertex(m_VertIndex-vertCount);
				break;
			}
		}

	}

	void NormalizeColors() {
		m_Mesh.NormalizeColors();
	}

	void AddWidgetsClient() {
		bkBank &bk = BANKMGR.CreateBank("Mesh Viewer");
		bk.AddSlider("Vector Length", &m_NormalLength, 0.0f, 1000.f, 0.01f);
		bk.AddToggle("Draw Mesh", &m_DrawMesh);
		bk.AddToggle("Draw Wireframe", &m_DrawWireframe);
		bk.AddToggle("Draw Normals", &m_DrawNormals);
		bk.AddToggle("Draw Tangents", &m_DrawTangents);
		bk.AddToggle("Draw Binormals", &m_DrawBinormals);
		bk.AddToggle("Compute Shader Binormal", &m_UseShaderMethod);

		int vertCount=0;
		for (int i=0;i<m_Mesh.GetMtlCount();i++)
		{
			mshMaterial& mtl=m_Mesh.GetMtl(i);
			vertCount+=mtl.GetVertexCount();
		}
		bk.AddToggle("Show Vertex Info",&m_ShowVertexInfo);
		bk.AddSlider("Vertex Index",&m_VertIndex,0,vertCount-1,1,datCallback(MFA(mshViewer::FindVertex),this));
		bk.AddSlider("Vert Pixel Size",&m_VertPixelSize,1,100,1);
		bk.AddButton("Normalize Colors",datCallback(MFA(mshViewer::NormalizeColors),this),"This action normalizes CPV values so that each color channel is between 0.0-1.0.  This can only be done one time after the mesh is loaded.");
	}
#endif

protected:
	mshMesh		m_Mesh;
	float		m_NormalLength;
	bool		m_DrawMesh;
	bool		m_DrawNormals;
	bool		m_DrawTangents;
	bool		m_DrawBinormals;
	bool		m_DrawWireframe;
	bool		m_UseShaderMethod;

	// WTF? MeshToDisplacementMap		meshDisplace;

#if __BANK
	bool		m_ShowVertexInfo;
	int			m_VertIndex;
	mshVertex	m_CurrentVertex;
	int			m_VertPixelSize;
#endif
};

// main application
int Main()
{
	mshViewer viewer;

	//viewer.Init("sample_terrain/viewer");
	viewer.Init("sample_terrainlighting");

	viewer.UpdateLoop();

	viewer.Shutdown();

	return 0;
}

