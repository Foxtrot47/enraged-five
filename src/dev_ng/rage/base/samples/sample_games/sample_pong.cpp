// 
// /sample_pong.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_grcore/sample_grcore.h"
#include "vector/vector2.h"
#include "grcore/device.h"
#include "grcore/im.h"
#include "grcore/viewport.h"
#include "grcore/state.h"
#include "input/pad.h"
#include "system/main.h"

#define MOVE_SPEED			3.0f
#define PADDLE_WIDTH		12
#define PADDLE_HEIGHT		45
#define PADDLE_MARGIN		5
#define BALL_RADIUS			6.0f
#define SERVE_MULTIPLIER	8.0f
#define TABLE_BORDER_WIDTH	10
#define NET_WIDTH			4
#define NUMBER_BOUND_WIDTH	30
#define NUMBER_BOUND_HEIGHT	50
#define NUMBER_WIDTH		10
#define BOUNCE_DAMPING		0.05f

using namespace rage;

Vector2 Rotate(Vector2& vVec, float fRads)
{
	float tsin = sinf(fRads);
	float tcos = cosf(fRads);
	Vector2 ret;
	ret.x = (vVec.x * tcos) - (vVec.y * tsin);
	ret.y = (vVec.x * tsin) + (vVec.y * tcos);
	return ret;
}

Vector2 Reflect(const Vector2& vDir, const Vector2& vNormal)
{
	Vector2 ret;
	ret.x = vDir.x - 2.0f * vDir.x * vNormal.x * vNormal.x;
	ret.y = vDir.y - 2.0f * vDir.y * vNormal.y * vNormal.y;
	return ret;
}

bool SegmentIntersection(const Vector2& vSeg1Start, const Vector2& vSeg1End, const Vector2& vSeg2Start, const Vector2& vSeg2End, Vector2& vIntersection)
{
	Vector2 vSeg1 = vSeg1End - vSeg1Start;
	Vector2 vSeg2 = vSeg2End - vSeg2Start;
	float d = vSeg1.Cross(vSeg2);
	if( fabs(d) < VERY_SMALL_FLOAT )
		return false;		// Parallel

	Vector2 vTemp = vSeg1Start - vSeg2Start;
	float s = vSeg2.Cross(vTemp) / d;
	if( s <= 0 || s > 1.0f )
		return false;

	float t = vSeg1.Cross(vTemp) / d;
	if( t <= 0 || t > 1.0f )
		return false;

	vIntersection.AddScaled(vSeg1Start, vSeg1, s);
	return true;
}

float RayPlaneIntersect(const Vector2& vRayOrigin, const Vector2& vRayDir, const Vector2& vPlanePosition, const Vector2& vPlaneDir)
{
	float planeW = vPlaneDir.Dot(vPlanePosition);

	float norm = vPlaneDir.Dot(vRayOrigin) - planeW;
	float det = vRayDir.Dot(vPlaneDir);
	if( det != 0.0f )
		det = -norm / det;
	return det;
}

void Collide(Vector2& vBallPreviousPosition, Vector2& vBallPosition, Vector2& vBallVelocity, const Vector2& vCollisionObjectStart, const Vector2& vCollisionObjectEnd, const Vector2& vCollisionObjectNormal, float fAdditionalVelocity = 0.0f, bool bInterpolate = false)
{
	Vector2 vIntersection;
	if( SegmentIntersection(vBallPreviousPosition, vBallPosition, vCollisionObjectStart, vCollisionObjectEnd, vIntersection) )
	{
		float fTraveled = vBallPreviousPosition.Dist(vIntersection);
		float fMoved = vBallPreviousPosition.Dist(vBallPosition);
		float fBounceDist = fMoved - fTraveled;

		Vector2 vNewDir;
		if( bInterpolate )
		{
			// Interpolate the normal based on the collision position
			Vector2 vTopToIsect = vIntersection - vCollisionObjectStart;
			Vector2 vPaddle = vCollisionObjectEnd - vCollisionObjectStart;
			float t = vTopToIsect.Mag() / vPaddle.Mag();
			t *= 2.0f;
			Vector2 vNormal1, vNormal2;
			if( t < 1.0f )
			{
				vNormal1.Set(0.0f, -1.0f);
				vNormal2 = vCollisionObjectNormal;
			}
			else
			{
				vNormal1 = vCollisionObjectNormal;
				vNormal2.Set(0.0f, 1.0f);
				t -= 1.0f;
			}
			vNewDir.Lerp(t, vNormal1, vNormal2);
			vNewDir.Normalize();
		}
		else
		{
			Vector2 vDir = vBallPosition - vBallPreviousPosition;
			vDir.Normalize();
			vNewDir = Reflect(vDir, vCollisionObjectNormal);
			vNewDir.Normalize();
		}
		
		float fVelocity = vBallVelocity.Mag();
		fVelocity -= (fVelocity * BOUNCE_DAMPING) + fAdditionalVelocity;
		vBallVelocity = vNewDir;
		vBallVelocity.Scale(fVelocity);

		vNewDir.Scale(fBounceDist);
		vBallPosition.Add(vIntersection, vNewDir);
	}
}

class AtariPongObject
{
public:
	AtariPongObject()			{}
	virtual ~AtariPongObject()	{}

	virtual void Draw() = 0;

	int GetWidth() const			{ return m_nWidth; }
	int GetHeight() const			{ return m_nHeight; }

protected:
	int				m_nWidth;
	int				m_nHeight;
};

class AtariPongPlayer;
class AtariPongBall : public AtariPongObject
{
public:
	AtariPongBall(float fRadius)	
	{
		m_fRadius = fRadius;
	}
	virtual ~AtariPongBall()		{}

	float GetRadius() const						{ return m_fRadius; }
	Vector2 GetPosition() const					{ return m_vPosition; }
	Vector2 GetVelocity() const					{ return m_vVelocity; }

	void SetPosition(Vector2 vPosition)
	{
		m_vPosition = vPosition;
		m_vVelocity = Vector2(0.0f, 0.0f);
	}

	void SetVelocity(Vector2 vVelocity)			{ m_vVelocity = vVelocity; }

	bool Update(AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2);

	virtual void Draw()	
	{
		grcBegin(drawTriFan, 18);
		grcColor(Color32(147, 223, 227));
		grcVertex2i((int)m_vPosition.x, (int)m_vPosition.y);

        grcColor(Color32(207, 241, 243));
		float fX = 0.0f;
		float fY = m_fRadius;
		float fStep = PI / 8;
		float fSin = sin(fStep);
		float fCos = cos(fStep);
		
		for( int i = 0; i < 17; i++ )
		{
			float t = (fX * fCos) - (fY * fSin);
			fY = (fX * fSin) + (fY * fCos);
			fX = t;
			grcVertex2i((int)(m_vPosition.x + fX), (int)(m_vPosition.y + fY));
		}
		grcEnd();
	}

protected:
	float			m_fRadius;
	Vector2			m_vPosition;
	Vector2			m_vVelocity;
};

class AtariPongPlayer : public AtariPongObject
{
public:
	AtariPongPlayer(bool bAIControlled, int nLeft, int nFront, Vector2 vNormal, int nWidth, int nHeight)			
	{ 
		m_bAIControlled = bAIControlled; 
		m_nScore = 0;
		m_fPosition = (float)(GRCDEVICE.GetHeight() / 2);
		m_fPreviousPosition = m_fPosition;
		m_nWidth = nWidth;
		m_nHeight = nHeight;
		m_nLeft = nLeft;
		m_fFront = (float)nFront;
		m_vNormal = vNormal;
		m_vNormal.Normalize();
		m_pBall = 0;
	}
	virtual ~AtariPongPlayer()					{}

	bool IsAIControlled() const					{ return m_bAIControlled; }
	int GetScore() const						{ return m_nScore; }
	float GetPosition() const					{ return m_fPosition; }
	float GetPreviousPosition() const			{ return m_fPreviousPosition; }
	int GetLeft() const							{ return m_nLeft; }
	Vector2 GetTop() const						{ float fHalfHeight = (float)m_nHeight * 0.5f; return Vector2(m_fFront, m_fPosition - fHalfHeight); }
	Vector2 GetBottom() const					{ float fHalfHeight = (float)m_nHeight * 0.5f; return Vector2(m_fFront, m_fPosition + fHalfHeight); } 
	Vector2 GetNormal() const					{ return m_vNormal; }
	void AttachBall(AtariPongBall* pBall)		{ m_pBall = pBall;	Move(0.0f);	}
	bool HasBall() const						{ return (m_pBall != 0); }
	void Score()								{ m_nScore++; }
	float GetVelocity() const					{ return m_fPosition - m_fPreviousPosition; }

	void Serve(float fStrength)
	{
		float fVelocity = (m_fPosition - m_fPreviousPosition);
		float fAngle = fVelocity * 10.0f;
		if( fAngle < -80.0f )
			fAngle = -80.0f;
		if( fAngle > 80.0f )
			fAngle = 80.0f;
		if( m_vNormal.x < 0.0f )
			fAngle = -fAngle;

		Vector2 vServeDir = Rotate(m_vNormal, fAngle * DtoR);
		vServeDir.Normalize();
		vServeDir.Scale((fStrength * SERVE_MULTIPLIER) + 1.0f);
		m_pBall->SetVelocity(vServeDir);
		m_pBall = 0;
	}

	void Move(float fMove)
	{
		m_fPreviousPosition = m_fPosition;
		m_fPosition += fMove;
		int nHalfHeight = m_nHeight / 2;
		if( (int)m_fPosition + nHalfHeight > GRCDEVICE.GetHeight() )
		{
			m_fPosition = (float)(GRCDEVICE.GetHeight() - nHalfHeight);
		}
		if( (int)m_fPosition - nHalfHeight < 0 )
		{
			m_fPosition = (float)nHalfHeight;
		}

		if( m_pBall )
		{
			Vector2 vFrontCenter(m_fFront, m_fPosition);
			Vector2 vDisplace = m_vNormal;
			vDisplace.Scale(m_pBall->GetRadius());
			vFrontCenter.Add(vDisplace);
			m_pBall->SetPosition(vFrontCenter);
		}
	}

	virtual void Draw()	
	{
		int nHalfHeight = m_nHeight / 2;
		int nCenter = (int)m_fPosition;
		grcBegin(drawTriStrip, 4);
		grcColor(Color32(202, 188, 155));
		grcVertex2i(m_nLeft, nCenter - nHalfHeight);
		grcVertex2i(m_nLeft + m_nWidth, nCenter - nHalfHeight);
		grcColor(Color32(182, 163, 118));
		grcVertex2i(m_nLeft, nCenter + nHalfHeight);
		grcVertex2i(m_nLeft + m_nWidth, nCenter + nHalfHeight);
		grcEnd();
	}

protected:
	bool			m_bAIControlled;
	int				m_nScore;
	float			m_fPosition;
	float			m_fPreviousPosition;
	int				m_nLeft;
	float			m_fFront;
	Vector2			m_vNormal;
	AtariPongBall*	m_pBall;
};

bool AtariPongBall::Update(AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2)
{
	// Move the ball along its velocity vector
	float fVel = m_vVelocity.Mag();
	if( fVel > 10.0f )
	{
		fVel = 10.0f;
		m_vVelocity.Normalize();
		m_vVelocity.Scale(fVel);
	}
	Vector2 vPrev = m_vPosition;
	m_vPosition.Add(m_vVelocity);

	// Collide with the walls
	Vector2 vTopWallLeft(-100.0f, 0.0f);
	Vector2 vTopWallRight((float)GRCDEVICE.GetWidth() + 100.0f, 0.0f);
	Vector2 vTopWallNormal(0.0f, -1.0f);
	Vector2 vBottomWallLeft(-100.0f, (float)GRCDEVICE.GetHeight());
	Vector2 vBottomWallRight((float)GRCDEVICE.GetWidth() + 100.0f, (float)GRCDEVICE.GetHeight());
	Vector2 vBottomWallNormal(0.0f, 1.0f);

	Collide(vPrev, m_vPosition, m_vVelocity, vTopWallLeft, vTopWallRight, vTopWallNormal);
	Collide(vPrev, m_vPosition, m_vVelocity, vBottomWallLeft, vBottomWallRight, vBottomWallNormal);

	// Collide with the paddles
	Collide(vPrev, m_vPosition, m_vVelocity, pPlayer1->GetTop(), pPlayer1->GetBottom(), pPlayer1->GetNormal(), pPlayer1->GetVelocity(), true);
	Collide(vPrev, m_vPosition, m_vVelocity, pPlayer2->GetTop(), pPlayer2->GetBottom(), pPlayer2->GetNormal(), pPlayer2->GetVelocity(), true);

	// Check for goals
	bool bScore = false;
	if( m_vPosition.x < 0.0f )
	{
		// Player2 scored
		pPlayer2->Score();
		bScore = true;
	}
	else if( m_vPosition.x > (float)GRCDEVICE.GetWidth() )
	{
		// Player1 scored
		pPlayer1->Score();
		bScore = true;
	}
	return bScore;
}

void AtariPongNextServe(int& nServe, AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2, AtariPongBall* pBall)
{
	if( nServe < 2 )
		pPlayer1->AttachBall(pBall);
	else
		pPlayer2->AttachBall(pBall);
	nServe++;
	nServe = nServe % 4;
}

void AtariPongUpdateInput(AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2)
{
	AtariPongPlayer* players[2];
	players[0] = pPlayer1;
	players[1] = pPlayer2;
	
	for( int i = 0; i < 2; i++ )
	{
		if( !players[i]->IsAIControlled() )
		{
			float fUp = ioPad::GetPad(i).GetNormAnalogButton(ioPad::LUP_INDEX);
			float fDown = ioPad::GetPad(i).GetNormAnalogButton(ioPad::LDOWN_INDEX);
			players[i]->Move(fUp * -MOVE_SPEED);
			players[i]->Move(fDown * MOVE_SPEED);

			u32 buttons = ioPad::GetPad(i).GetButtons();
			if( buttons & ioPad::LUP )
				players[i]->Move(-MOVE_SPEED);
			if( buttons & ioPad::LDOWN )
				players[i]->Move(MOVE_SPEED);

			if( players[i]->HasBall() && ((buttons & ioPad::RUP) || (buttons & ioPad::RRIGHT) || (buttons & ioPad::RDOWN) || (buttons & ioPad::RLEFT)) )
			{
				float fLeftTrigger = ioPad::GetPad(i).GetNormAnalogButton(ioPad::L2_INDEX);
				float fRightTrigger = ioPad::GetPad(i).GetNormAnalogButton(ioPad::R2_INDEX);
				float fStrength = (fLeftTrigger + fRightTrigger) * 0.5f;
				players[i]->Serve(fStrength);
			}
		}
	}
}

void AtariPongAI(AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2, AtariPongBall* pBall)
{
	AtariPongPlayer* players[2];
	players[0] = pPlayer1;
	players[1] = pPlayer2;

	for( int i = 0; i < 2; i++ )
	{
		if( players[i]->IsAIControlled() )
		{
			// Do I have the ball
			if( players[i]->HasBall() )
			{
				// Serve at half strength, no angle
				players[i]->Serve(0.5f);
			}

			// Is the ball coming toward me?
			Vector2 vNormal = players[i]->GetNormal();
			Vector2 vBallPos = pBall->GetPosition();
			Vector2 vBallDir = pBall->GetVelocity();
			vBallDir.Normalize();
			float fFrontPlaneT = RayPlaneIntersect(vBallPos, vBallDir, players[i]->GetTop(), vNormal);
			if( fFrontPlaneT > 0.0f )
			{
				// Ball is coming toward me, figure out where I need to be to return it

				// Find the intersection of the ball and the top wall
				float fTopWallT = RayPlaneIntersect(vBallPos, vBallDir, Vector2(0.0f, 0.0f), Vector2(0.0f, -1.0f));
				if( fTopWallT > 0.0f && fTopWallT < fFrontPlaneT )
				{
					// Ball will hit the top wall first, re raycast from the intersection point along the reflect vector
					vBallDir.Scale(fTopWallT);
					Vector2 vWallPos = vBallPos + vBallDir;
					vBallDir.Scale(2.0f);
					Vector2 vBallFar = vBallPos + vBallDir;
					Collide(vBallPos, vBallFar, vBallDir, Vector2(-100.0f, 0.0f), Vector2((float)GRCDEVICE.GetWidth() + 100.0f, 0.0f), Vector2(0.0f, -1.0f));
					vBallDir.Normalize();
					vBallPos = vWallPos;
					fFrontPlaneT = RayPlaneIntersect(vBallPos, vBallDir, players[i]->GetTop(), vNormal);
				}

				// Find the intersection of the ball and the bottom wall
				float fBottomWallT = RayPlaneIntersect(vBallPos, vBallDir, Vector2(0.0f, (float)GRCDEVICE.GetHeight()), Vector2(0.0f, 1.0f));
				if( fBottomWallT > 0.0f && fBottomWallT < fFrontPlaneT )
				{
					// Ball will hit the bottom wall first, re-raycast from the intersection point along the reflect vector
					vBallDir.Scale(fBottomWallT);
					Vector2 vWallPos = vBallPos + vBallDir;
					vBallDir.Scale(2.0f);
					Vector2 vBallFar = vBallPos + vBallDir;
					Collide(vBallPos, vBallFar, vBallDir, Vector2(-100.0f, (float)GRCDEVICE.GetHeight()), Vector2((float)GRCDEVICE.GetWidth() + 100.0f, (float)GRCDEVICE.GetHeight()), Vector2(0.0f, 1.0f));
					vBallDir.Normalize();
					vBallPos = vWallPos;
					fFrontPlaneT = RayPlaneIntersect(vBallPos, vBallDir, players[i]->GetTop(), vNormal);
				}

				// Find the intersection of the ball and my front plane and move to intercept
				vBallDir.Scale(fFrontPlaneT);
				Vector2 vFrontIntersect = vBallPos + vBallDir;
				if( players[i]->GetPosition() < vFrontIntersect.y )
					players[i]->Move(MOVE_SPEED);
				if( players[i]->GetPosition() > vFrontIntersect.y )
					players[i]->Move(-MOVE_SPEED);
			}
		}
	}
}

void AtariPongDrawBackground()
{
	Color32 bottomGreen(0, 120, 0);
	Color32 topGreen(0, 85, 0);

	grcBegin(drawTriStrip,4);
	grcColor(topGreen);
	grcVertex2i(0,0);
	grcVertex2i(GRCDEVICE.GetWidth(),0);
	
	grcColor(bottomGreen);
	grcVertex2i(0,GRCDEVICE.GetHeight());
	grcVertex2i(GRCDEVICE.GetWidth(),GRCDEVICE.GetHeight());
	grcEnd();
}

void AtariPongDrawNumber(int nLeft, int nTop, int nNumber)
{
	int nRight, nBottom, nWidth;
	int nTemp1, nTemp2, nTemp3, nTemp4;
	nRight = nLeft + NUMBER_BOUND_WIDTH;
	nBottom =  nTop + NUMBER_BOUND_HEIGHT;
	nWidth = NUMBER_WIDTH;
	switch( nNumber )
	{
		case 0:
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nLeft + nWidth, nTop);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nLeft + nWidth, nBottom);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nRight - nWidth, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		case 1:
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();

			nTemp1 = (nRight - nLeft) / 2;
			nTemp2 = nWidth / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft + nTemp1 - nTemp2, nTop);
			grcVertex2i(nLeft + nTemp1 + nTemp2, nTop);
			grcVertex2i(nLeft + nTemp1 - nTemp2, nBottom);
			grcVertex2i(nLeft + nTemp1 + nTemp2, nBottom);
			grcEnd();

			nTemp3 = nTemp1 / 2;
			nTemp4 = (nBottom - nTop) / 4;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft + nTemp1 - nTemp2, nTop);
			grcVertex2i(nLeft + nTemp1 + nTemp2, nTop);
			grcVertex2i(nLeft, nTop + nTemp4);
			grcVertex2i(nLeft + nWidth, nTop + nTemp4);
			grcEnd();
			break;
		case 2:
			nTemp4 = (nBottom - nTop) / 4;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nLeft + nWidth, nTop);
			grcVertex2i(nLeft, nTop + nTemp4);
			grcVertex2i(nLeft + nWidth, nTop + nTemp4);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nLeft + nWidth, nBottom - nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		case 3:
			nTemp3 = nWidth / 2;
			nTemp4 = (nBottom - nTop) / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nRight - nWidth, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 + nTemp3);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		case 4:
			nTemp1 = (nRight - nLeft) / 4;
			nTemp2 = (nBottom - nTop) / 4;
			nTemp3 = nWidth / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nTemp2 - nTemp3);
			grcVertex2i(nRight, nBottom - nTemp2 - nTemp3);
			grcVertex2i(nLeft, nBottom - nTemp2 + nTemp3);
			grcVertex2i(nRight, nBottom - nTemp2 + nTemp3);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nTemp1 - nTemp3, nTop);
			grcVertex2i(nRight - nTemp1 + nTemp3, nTop);
			grcVertex2i(nRight - nTemp1 - nTemp3, nBottom);
			grcVertex2i(nRight - nTemp1 + nTemp3, nBottom);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nTemp1 - nTemp3, nTop);
			grcVertex2i(nRight - nTemp1 + nTemp3, nTop);
			grcVertex2i(nLeft, nBottom - nTemp2 - nTemp3);
			grcVertex2i(nLeft + nWidth, nBottom - nTemp2 - nTemp3);
			grcEnd();
			break;
		case 5:
			nTemp3 = nWidth / 2;
			nTemp4 = (nBottom - nTop) / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nLeft + nWidth, nTop);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nLeft + nWidth, nTop + nTemp4 + nTemp3);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 + nTemp3);
			grcEnd();
		
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight - nWidth, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop + nBottom - nWidth);
			grcVertex2i(nRight, nTop + nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		case 6:
			nTemp3 = nWidth / 2;
			nTemp4 = (nBottom - nTop) / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nLeft + nWidth, nTop);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nLeft + nWidth, nBottom);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 + nTemp3);
			grcEnd();
		
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight - nWidth, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		case 7:
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nLeft + nWidth, nBottom);
			grcEnd();
			break;
		case 8:
			nTemp3 = nWidth / 2;
			nTemp4 = (nBottom - nTop) / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nLeft + nWidth, nTop);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nLeft + nWidth, nBottom);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 + nTemp3);
			grcEnd();
		
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nRight - nWidth, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		case 9:
			nTemp3 = nWidth / 2;
			nTemp4 = (nBottom - nTop) / 2;
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nLeft, nTop + nWidth);
			grcVertex2i(nRight, nTop + nWidth);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop);
			grcVertex2i(nLeft + nWidth, nTop);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nLeft + nWidth, nTop + nTemp4 + nTemp3);
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nTop + nTemp4 - nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 - nTemp3);
			grcVertex2i(nLeft, nTop + nTemp4 + nTemp3);
			grcVertex2i(nRight, nTop + nTemp4 + nTemp3);
			grcEnd();
		
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nRight - nWidth, nTop);
			grcVertex2i(nRight, nTop);
			grcVertex2i(nRight - nWidth, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			
			grcBegin(drawTriStrip, 4);
			grcVertex2i(nLeft, nBottom - nWidth);
			grcVertex2i(nRight, nBottom - nWidth);
			grcVertex2i(nLeft, nBottom);
			grcVertex2i(nRight, nBottom);
			grcEnd();
			break;
		default:
			break;
	}
};

void AtariPongDrawHud(AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2)
{	
	// Draw Table Edges
	int nTableBorderWidth = TABLE_BORDER_WIDTH;
	grcBegin(drawTriStrip, 4);
	grcColor(Color32(200, 200, 200));
	grcVertex2i(0, 0);
	grcVertex2i(GRCDEVICE.GetWidth(),0);
	grcVertex2i(0,nTableBorderWidth);
	grcVertex2i(GRCDEVICE.GetWidth(),nTableBorderWidth);
	grcEnd();

	grcBegin(drawTriStrip, 4);
	grcVertex2i(0, 0);
	grcVertex2i(nTableBorderWidth, 0);
	grcVertex2i(0, GRCDEVICE.GetHeight());
	grcVertex2i(nTableBorderWidth, GRCDEVICE.GetHeight());
	grcEnd();

	grcBegin(drawTriStrip, 4);
	grcVertex2i(GRCDEVICE.GetWidth() - nTableBorderWidth, 0);
	grcVertex2i(GRCDEVICE.GetWidth(), 0);
	grcVertex2i(GRCDEVICE.GetWidth() - nTableBorderWidth, GRCDEVICE.GetHeight());
	grcVertex2i(GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
	grcEnd();

	grcBegin(drawTriStrip, 4);
	grcVertex2i(0, GRCDEVICE.GetHeight() - nTableBorderWidth);
	grcVertex2i(GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight() - nTableBorderWidth);
	grcVertex2i(0, GRCDEVICE.GetHeight());
	grcVertex2i(GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
	grcEnd();

	// Draw Net
	int nNetWidth = NET_WIDTH;
	int nHalfWidth = GRCDEVICE.GetWidth() / 2;
	grcBegin(drawTriStrip, 4);
	grcColor(Color32(255, 255, 255));
	grcVertex2i(nHalfWidth - nNetWidth, 0);
	grcVertex2i(nHalfWidth + nNetWidth, 0);
	grcVertex2i(nHalfWidth - nNetWidth, GRCDEVICE.GetHeight());
	grcVertex2i(nHalfWidth + nNetWidth, GRCDEVICE.GetHeight());
	grcEnd();

	// Draw Score
	AtariPongDrawNumber(20, 20, pPlayer1->GetScore());
	AtariPongDrawNumber(GRCDEVICE.GetWidth() - 60, 20, pPlayer2->GetScore());
}

void AtariPongDraw(AtariPongPlayer* pPlayer1, AtariPongPlayer* pPlayer2, AtariPongBall* pBall)
{
	// Draw background
	AtariPongDrawBackground();

	// Draw Hud
	AtariPongDrawHud(pPlayer1, pPlayer2);

	// Draw Players
	pPlayer1->Draw();
	pPlayer2->Draw();

	// Draw Ball
	pBall->Draw();
}

class grcSamplePong : public ragesamples::grcSampleManager
{
public: 
	grcSamplePong()
	{
		// not sure what to do with this now, it's a structure instead of a bool
		// m_OrthoCam = true;
	}

	void Init(const char* path=NULL)
	{

		grcSampleManager::Init(path);

		// Initialize players
		m_pPlayer1 = rage_new AtariPongPlayer(false, PADDLE_MARGIN, PADDLE_MARGIN + PADDLE_WIDTH, Vector2(1.0f, 0.0f), PADDLE_WIDTH, PADDLE_HEIGHT);
		m_pPlayer2 = rage_new AtariPongPlayer(true, (GRCDEVICE.GetWidth() - PADDLE_WIDTH) - PADDLE_MARGIN, (GRCDEVICE.GetWidth() - PADDLE_WIDTH) - PADDLE_MARGIN, Vector2(-1.0f, 0.0f), PADDLE_WIDTH, PADDLE_HEIGHT);
		
		// Initialize the ball
		m_pBall = rage_new AtariPongBall(BALL_RADIUS);

		// Initialize game state
		m_nServe = 0;
		AtariPongNextServe(m_nServe, m_pPlayer1, m_pPlayer2, m_pBall);

	}

	void UpdateClient() 
	{
		// Update Inputs
		AtariPongUpdateInput(m_pPlayer1, m_pPlayer2);

		// AI Logic
		AtariPongAI(m_pPlayer1, m_pPlayer2, m_pBall);

		// Update Ball Position
		bool bScore = m_pBall->Update(m_pPlayer1, m_pPlayer2);
		if( bScore )
			AtariPongNextServe(m_nServe, m_pPlayer1, m_pPlayer2, m_pBall);
	}

	void DrawClient()
	{
		// Initialize render state
		grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcViewport::SetCurrentWorldIdentity();
		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcState::SetCullMode(grccmNone);
		grcState::SetState(grcsDepthFunc,grcdfAlways);
		grcState::SetState(grcsDepthWrite,false);

		AtariPongDraw(m_pPlayer1, m_pPlayer2, m_pBall);
	}
	
private:
	AtariPongPlayer*	m_pPlayer1;
	AtariPongPlayer*	m_pPlayer2;
	AtariPongBall*		m_pBall;
	int					m_nServe;
};

int Main()
{
	grcSamplePong sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
