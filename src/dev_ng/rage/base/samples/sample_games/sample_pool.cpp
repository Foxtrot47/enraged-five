// 
// /sample_pool.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Pool
// PURPOSE:
//		This sample creates a billiard game to test the physics of small ball motion, interaction with the table and the pool que.

#include "bank/bkmgr.h"
#include "math/random.h"
#include "phbound/boundgeom.h"
#include "phbound/boundsphere.h"
#include "phsolver/contactmgr.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"


PARAM(file, "Reserved for future expansion, not currently hooked up");


namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Active);
EXT_PFD_DECLARE_ITEM(Inactive);
EXT_PFD_DECLARE_ITEM(Fixed);

}

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

class snookerSampleManager : public physicsSampleManager
{
public:

	virtual void InitClient()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();

		// STEP #2. Define an offset for all objects in the world.
		//-- Change this vector to test physics behavior with roundoff errors far from the origin.
		Vector3 worldOffset(1500.0f,0.0f,0.0f);
		phDemoObject::SetWorldOffset(worldOffset);

		// STEP #3. Set the physics level parameters.
		//-- See sample_physics/sample_basic for a demonstration of how to set up a simple physics level and simulator.
		Vector3 position,rotation,constraintPos;

		// STEP #4. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.

		// demo world with a ground plane
		position.Set(0.0f,0.0f,0.0f);
		phDemoWorld& snookerWorld = MakeNewDemoWorld("Snooker",NULL,position);
		snookerWorld.SetFramerate(30.0f);

		// snooker table
		position.Set(0.0f,0.0f,0.0f);
		rotation.Set(-0.5f*PI,0.0f,0.0f);
		phInst* tableInstance = snookerWorld.CreateFixedObject("ss_jampooltable",position,false,rotation)->GetPhysInst();

		// cue ball
		float ballRadius = 0.04f;
		Vector3 tableSize(1.5f,0.9f,3.0f);
		position.y += tableSize.y+ballRadius;
		position.z += 0.4f*tableSize.z;
		phDemoObject* ball = snookerWorld.CreateObject("pool_ball",position);

		// sideways pyramid of balls
		position.z -= 0.7f*tableSize.z;
		snookerWorld.ConstructPyramid(position,5,true,"pool_ball",true,false);

		// large maximum angular speed for all the balls
		static_cast<phArchetypePhys*>(ball->GetPhysInst()->GetArchetype())->SetMaxAngSpeed(20.0f*PI);

		// motion damping for all the balls
		float linearC = 0.0f;
		float linearV = 0.0f;
		float linearV2 = 0.0;
		float angularC = 0.0f;
		float angularV = 0.0f;
		float angularV2 = 0.0f;
		ball->DampMotion(linearC,linearV,linearV2,angularC,angularV,angularV2);

		// cue stick
		position.y += 0.23f;
		position.z += 1.35f*tableSize.z;
		rotation.Set(-0.5f*PI,0.0f,0.0f);
		float stickRadius = 0.01f;
		float stickLength = 1.5f;
		phInst* cueInstance = snookerWorld.CreateCurvedGeometryCylinder(position,rotation,stickRadius,stickLength)->GetPhysInst();
		constraintPos.Set(position);
		constraintPos.y += 0.4f;
		constraintPos.z -= 0.5f;
		//snookerWorld.AttachObjectToWorld(constraintPos,position,cueInstance);
		position.z -= stickLength;
		constraintPos.y += 0.1f;
		constraintPos.z -= stickLength;
		//snookerWorld.AttachObjectToWorld(constraintPos,position,cueInstance);

		// Make the cue stick not collide with the table.
		tableInstance->GetArchetype()->SetTypeFlags(2);
		cueInstance->GetArchetype()->SetIncludeFlag(2,0);

		phImpactSet::SetAllowedPenetration(0.001f);

		// STEP #5. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		int startingDemo = 0;
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
	}


	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,2.0f,3.0f);
		lookFrom.Add(phDemoObject::GetWorldOffset());
		Vector3 lookTo(0.0f,1.0f,0.0f);
		lookTo.Add(phDemoObject::GetWorldOffset());
		InitSampleCamera(lookFrom,lookTo);
	}


	virtual void Update ()
	{
		physicsSampleManager::Update();
	}
};

} // namespace ragesamples


// main application
int Main()
{
	{
		ragesamples::snookerSampleManager sampleSnooker;
		sampleSnooker.Init();

		sampleSnooker.UpdateLoop();

		sampleSnooker.Shutdown();
	}

	return 0;
}
