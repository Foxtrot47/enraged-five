//
// sample_dx11/sample_dx11.h
//
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved.
//


#ifndef SAMPLE_RMCORE_SAMPLE_DX11_H
#define SAMPLE_RMCORE_SAMPLE_DX11_H

#include "sample_grcore/sample_grcore.h"

using namespace rage;

namespace ragesamples {

class dx11SampleManager : public grcSampleManager
{
public:
	dx11SampleManager();
	void Init(const char* path=NULL);

protected:
	virtual void CreateGfxFactories();
	virtual void DestroyGfxFactories();
	void GetFileList();
	grcSetup& AllocateSetup();
	int GetNumFileNames() const				{return m_NumFiles;}
	const char* GetFileName(int which)		{FastAssert(which>=0 && which<m_MaxFiles); return m_FileNames[which];}
	enum {NUM_BUCKETS=16};
	bool GetBucketEnable(int which)			{FastAssert(which>=0 && which<NUM_BUCKETS); return m_BucketEnable[which];}
	void SetDefaultFileName(int which, const char* text) {FastAssert(which>=0 && which<MAX_FILES); m_DefaultFileNames[which]=text;}
	void SetMaxFiles(int maxFiles)			{FastAssert(maxFiles>=0 && maxFiles<=MAX_FILES); m_MaxFiles = maxFiles; }
	void SetMinFiles(int minFiles)			{FastAssert(minFiles>=0 && minFiles<=MAX_FILES); m_MinFiles = minFiles; }

	virtual void InitShaderSystems();
	virtual void RegisterTechniqueGroups(){}

#if __BANK
	virtual void AddWidgetsClient();
	virtual void AddWidgetsBuckets();
	virtual void AddWidgetsPasses();
	virtual void AddWidgetsDebugDraw();
#endif

private:
	enum {MAX_FILES=10,FILENAME_SIZE=256};

	bool					m_BucketEnable[NUM_BUCKETS];
	const char*				m_FileNames[MAX_FILES];
	const char*				m_DefaultFileNames[MAX_FILES];
	char					m_FileNameBuf[MAX_FILES][FILENAME_SIZE];
	char					m_ParamNameBuf[FILENAME_SIZE*4];
	int						m_NumFiles;
	int						m_MinFiles;
	int						m_MaxFiles;
	grcMaterialLibrary*		m_MtlLib;
	grcEffect*				m_FxBump;
	grcEffect*				m_FxCSBlur;
	grcEffect*				m_FxDiffuse;
	grcEffect*				m_FxDiffuseAlpha;
};

} // namespace ragesamples

#endif // SAMPLE_RMCORE_SAMPLE_DX11_H
