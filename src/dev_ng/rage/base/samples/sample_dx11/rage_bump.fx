// Configure the megashder
//#define USE_SPECULAR
//#define IGNORE_SPECULAR_MAP
//#define USE_NORMAL_MAP
//#define USE_DEFAULT_TECHNIQUES

#pragma dcl position diffuse texcoord0 normal tangent0

#include "../../src/shaderlib/rage_megashader.fxh"

struct VS_RenderSceneInput
{
	float3 f3Position   : POSITION;  
	float4 f4Color		: COLOR0;
	float2 f2TexCoord   : TEXCOORD0;
	float3 f3Normal     : NORMAL;  
	float3 f3Tangent	: TANGENT;   
};

struct PS_RenderSceneInput
{
	float4 f4Position   : SV_Position;
	float3 f3Normal		: NORMAL;
	float3 f3Tangent	: TANGENT;
	float3 f3Position	: TEXCOORD0;
	float2 f2TexCoord   : TEXCOORD1;
	float4 f4Color		: TEXCOORD2;
};

cbuffer cbPNTriangles
{
	float4      g_f4TessFactors = float4(1, 1, 700, 1300);            // Tessellation factors ( x=Edge, y=Inside, z=MinDistance, w=Range )
	float4		g_f4Bumpiness = float4(0.01, 0, 0, 0);
}

Texture2D BumpSampler		  : BumpTex : register( t1 );    // Normal map and height map texture pair
SamplerState BumpSamplerState : register( s1 );

//=================================================================================================================================
// This vertex shader computes standard transform and lighting, with no tessellation stages following
//=================================================================================================================================
PS_RenderSceneInput VS_RenderScene( VS_RenderSceneInput I )
{
    PS_RenderSceneInput O;
    float3 f3NormalWorldSpace;
    
    // Transform the position from object space to homoeneous projection space
    O.f4Position = mul( float4( I.f3Position, 1.0f ), gWorldViewProj );
    O.f3Position = mul( float4( I.f3Position, 1.0f), gWorld ).xyz;
    
    // Transform the normal from object space to world space    
    O.f3Normal = normalize( mul( I.f3Normal, (float3x3)gWorld ) );
	O.f3Tangent = normalize( mul( I.f3Tangent, (float3x3)gWorld ) );
    // Pass through texture coords
    O.f2TexCoord = I.f2TexCoord; 
    O.f4Color = I.f4Color;
    
    return O;    
}
	
//=================================================================================================================================
// This shader outputs the pixel's color by passing through the lit 
// diffuse material color
//=================================================================================================================================
float4 PS_RenderScene( PS_RenderSceneInput I ) : COLOR
{
	// Normally, bump would be = (sampledColor - 0.5) * 2.0
    //	However, since we're pulling in a bumpiness term, we can factor in the scale there & save a mult
    float2 bumpN = (BumpSampler.Sample(BumpSamplerState, I.f2TexCoord).ga - 0.5) * 2;
    float3 f3Normal = normalize(I.f3Normal + (bumpN.x * I.f3Tangent) + (bumpN.y * cross(I.f3Tangent, I.f3Normal)) );

	// Calc diffuse color    
	float4 f4Color = tex2D(TextureSampler, I.f2TexCoord);
	float3 global_light = 0;
	for (int i = 0; i < 4; ++i) 
	{
		rageLightOutput lightData = rageComputeLightData(I.f3Position, i);
	    float3 L = lightData.lightPosDir.xyz;
	    float3 light = saturate(dot(f3Normal,L));
		// Sum up color contribution of light sources
		global_light += light.rgb;
	}
	f4Color.rgb *= global_light * I.f4Color.rgb;
	f4Color.a	*= gLightAmbient.w;
	return f4Color;
}


#if  ( __SHADERMODEL >= 50) 

#define USE_ADAPTIVE_TESSELLATION 1

struct HS_Input
{
	float3 f3Position   : TEXCOORD0;
	float3 f3Normal     : NORMAL;
	float3 f3Tangent	: TANGENT;
	float2 f2TexCoord   : TEXCOORD1;
	float4 f4Color		: TEXCOORD2;
};

struct HS_ConstantOutput
{
	// Tess factor for the FF HW block
	float fTessFactor[3]    : SV_TessFactor;
	float fInsideTessFactor : SV_InsideTessFactor;
    
	// Geometry cubic generated control points
	float3 f3B210    : TEXCOORD3;
	float3 f3B120    : TEXCOORD4;
	float3 f3B021    : TEXCOORD5;
	float3 f3B012    : TEXCOORD6;
	float3 f3B102    : TEXCOORD7;
	float3 f3B201    : TEXCOORD8;
	float3 f3B111    : CENTER;
    
	// Normal quadratic generated control points
	float3 f3N110    : NORMAL3;      
	float3 f3N011    : NORMAL4;
	float3 f3N101    : NORMAL5;

};

struct HS_ControlPointOutput
{
	float3    f3Position    : TEXCOORD0;
	float3    f3Normal      : NORMAL;
	float3	  f3Tangent		: TANGENT;
	float2    f2TexCoord    : TEXCOORD1;
	float4	  f4Color		: TEXCOORD2;
};

struct DS_Output
{
	float4 f4Position   : SV_Position;
	float3 f3Normal		: NORMAL;
	float3 f3Tangent	: TANGENT;
	float3 f3Position	: TEXCOORD0;
	float2 f2TexCoord   : TEXCOORD1;
	float4 f4Color		: TEXCOORD2;
};
//=================================================================================================================================
// This vertex shader is a pass through stage, with HS, tessellation, and DS stages following
//=================================================================================================================================
HS_Input VS_RenderSceneWithTessellation( VS_RenderSceneInput I )
{
	HS_Input O;
    
	// Pass through world space position
	O.f3Position = I.f3Position;
    
	// Pass through normalized world space normal    
	O.f3Normal = I.f3Normal;
	O.f3Tangent = I.f3Tangent;
	// Pass through texture coordinates
	O.f2TexCoord = I.f2TexCoord;
	
	O.f4Color = I.f4Color;
    
	return O;    
}
//=================================================================================================================================
// This hull shader passes the tessellation factors through to the HW tessellator, 
// and the 10 (geometry), 6 (normal) control points of the PN-triangular patch to the domain shader
//=================================================================================================================================
HS_ConstantOutput HS_PNTrianglesConstant( InputPatch<HS_Input, 3> I )
{
	HS_ConstantOutput O = (HS_ConstantOutput)0;
    
	 #ifdef USE_ADAPTIVE_TESSELLATION
                
		// Calculate the tessellation factor per edge, based on distance from
		// camera
		float fDistance;
		float3 f3MidPoint;
		// Edge 0
		f3MidPoint = ( I[2].f3Position + I[0].f3Position ) / 2.0f;
		fDistance = distance( f3MidPoint, gViewInverse[3].xyz ) - g_f4TessFactors.z;
		O.fTessFactor[0] = g_f4TessFactors.x * ( 1.0f - clamp( ( fDistance / g_f4TessFactors.w ), 0.0f, 1.0f - ( 1.0f / g_f4TessFactors.x ) ) );
		// Edge 1
		f3MidPoint = ( I[0].f3Position + I[1].f3Position ) / 2.0f;
		fDistance = distance( f3MidPoint, gViewInverse[3].xyz ) - g_f4TessFactors.z;
		O.fTessFactor[1] = g_f4TessFactors.x * ( 1.0f - clamp( ( fDistance / g_f4TessFactors.w ), 0.0f, 1.0f - ( 1.0f / g_f4TessFactors.x ) ) );
		// Edge 2
		f3MidPoint = ( I[1].f3Position + I[2].f3Position ) / 2.0f;
		fDistance = distance( f3MidPoint, gViewInverse[3].xyz ) - g_f4TessFactors.z;
		O.fTessFactor[2] = g_f4TessFactors.x * ( 1.0f - clamp( ( fDistance / g_f4TessFactors.w ), 0.0f, 1.0f - ( 1.0f / g_f4TessFactors.x ) ) );
		// Inside
		O.fInsideTessFactor = ( O.fTessFactor[0] + O.fTessFactor[1] + O.fTessFactor[2] ) / 3.0f;
             
	#else
    
		// Simply output the tessellation factors from constant space 
		// for use by the FF tessellation unit
		O.fTessFactor[0] = O.fTessFactor[1] = O.fTessFactor[2] = g_f4TessFactors.x;
		O.fInsideTessFactor = g_f4TessFactors.y;

	#endif

	// Assign Positions
	float3 f3B003 = I[0].f3Position.xyz;
	float3 f3B030 = I[1].f3Position.xyz;
	float3 f3B300 = I[2].f3Position.xyz;
	// And Normals
	float3 f3N002 = I[0].f3Normal;
	float3 f3N020 = I[1].f3Normal;
	float3 f3N200 = I[2].f3Normal;
	

	// Compute the cubic geometry control points
	// Edge control points
	O.f3B210 = ( ( 2.0f * f3B003 ) + f3B030 - ( dot( ( f3B030 - f3B003 ), f3N002 ) * f3N002 ) ) / 3.0f;
	O.f3B120 = ( ( 2.0f * f3B030 ) + f3B003 - ( dot( ( f3B003 - f3B030 ), f3N020 ) * f3N020 ) ) / 3.0f;
	O.f3B021 = ( ( 2.0f * f3B030 ) + f3B300 - ( dot( ( f3B300 - f3B030 ), f3N020 ) * f3N020 ) ) / 3.0f;
	O.f3B012 = ( ( 2.0f * f3B300 ) + f3B030 - ( dot( ( f3B030 - f3B300 ), f3N200 ) * f3N200 ) ) / 3.0f;
	O.f3B102 = ( ( 2.0f * f3B300 ) + f3B003 - ( dot( ( f3B003 - f3B300 ), f3N200 ) * f3N200 ) ) / 3.0f;
	O.f3B201 = ( ( 2.0f * f3B003 ) + f3B300 - ( dot( ( f3B300 - f3B003 ), f3N002 ) * f3N002 ) ) / 3.0f;
	// Center control point
	float3 f3E = ( O.f3B210 + O.f3B120 + O.f3B021 + O.f3B012 + O.f3B102 + O.f3B201 ) / 6.0f;
	float3 f3V = ( f3B003 + f3B030 + f3B300 ) / 3.0f;
	O.f3B111 = f3E + ( ( f3E - f3V ) / 2.0f );
    
	// Compute the quadratic normal control points, and rotate into world space
	float fV12 = 2.0f * dot( f3B030 - f3B003, f3N002 + f3N020 ) / dot( f3B030 - f3B003, f3B030 - f3B003 );
	O.f3N110 = normalize( f3N002 + f3N020 - fV12 * ( f3B030 - f3B003 ) );
	float fV23 = 2.0f * dot( f3B300 - f3B030, f3N020 + f3N200 ) / dot( f3B300 - f3B030, f3B300 - f3B030 );
	O.f3N011 = normalize( f3N020 + f3N200 - fV23 * ( f3B300 - f3B030 ) );
	float fV31 = 2.0f * dot( f3B003 - f3B300, f3N200 + f3N002 ) / dot( f3B003 - f3B300, f3B003 - f3B300 );
	O.f3N101 = normalize( f3N200 + f3N002 - fV31 * ( f3B003 - f3B300 ) );
          
	return O;
}

[domain("tri")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[patchconstantfunc("HS_PNTrianglesConstant")]
[outputcontrolpoints(3)]
HS_ControlPointOutput HS_PNTriangles( InputPatch<HS_Input, 3> I, uint uCPID : SV_OutputControlPointID )
{
	HS_ControlPointOutput O = (HS_ControlPointOutput)0;

	// Just pass through inputs = fast pass through mode triggered
	O.f3Position = I[uCPID].f3Position;
	O.f3Normal = I[uCPID].f3Normal;
	O.f3Tangent = I[uCPID].f3Tangent;
	O.f2TexCoord = I[uCPID].f2TexCoord;
	O.f4Color = I[uCPID].f4Color;
    
	return O;
}

//=================================================================================================================================
// This domain shader applies contol point weighting to the barycentric coords produced by the FF tessellator 
//=================================================================================================================================
[domain("tri")]
DS_Output DS_PNTriangles( HS_ConstantOutput HSConstantData, const OutputPatch<HS_ControlPointOutput, 3> I, float3 f3BarycentricCoords : SV_DomainLocation )
{
	DS_Output O = (DS_Output)0;

	// The barycentric coordinates
	float fU = f3BarycentricCoords.x;
	float fV = f3BarycentricCoords.y;
	float fW = f3BarycentricCoords.z;

	// Precompute squares and squares * 3 
	float fUU = fU * fU;
	float fVV = fV * fV;
	float fWW = fW * fW;
	float fUU3 = fUU * 3.0f;
	float fVV3 = fVV * 3.0f;
	float fWW3 = fWW * 3.0f;
    
	// Compute position from cubic control points and barycentric coords
	float3 f3Position = I[0].f3Position.xyz * fWW * fW +
						I[1].f3Position.xyz * fUU * fU +
						I[2].f3Position.xyz * fVV * fV +
						HSConstantData.f3B210 * fWW3 * fU +
						HSConstantData.f3B120 * fW * fUU3 +
						HSConstantData.f3B201 * fWW3 * fV +
						HSConstantData.f3B021 * fUU3 * fV +
						HSConstantData.f3B102 * fW * fVV3 +
						HSConstantData.f3B012 * fU * fVV3 +
						HSConstantData.f3B111 * 6.0f * fW * fU * fV;
    
    // Compute normal from quadratic control points and barycentric coords
	float3 f3Normal =   I[0].f3Normal * fW + I[1].f3Normal * fU + I[2].f3Normal * fV;
	f3Normal = normalize( f3Normal );
	
	// Compute tangent from quadratic control points and barycentric coords
	float3 f3Tangent =  I[0].f3Tangent * fW + I[1].f3Tangent * fU + I[2].f3Tangent * fV ;
	f3Tangent = normalize( f3Tangent );

	// Linearly interpolate the texture coords
	O.f2TexCoord = I[0].f2TexCoord * fW + I[1].f2TexCoord * fU + I[2].f2TexCoord * fV;
	O.f4Color = I[0].f4Color * fW + I[1].f4Color * fU + I[2].f4Color * fV;
	O.f3Normal = mul( f3Normal, gWorld ).xyz;
	O.f3Tangent = mul( f3Tangent, gWorld ).xyz;
	
	float3 bumpN = normalize(float3(1, (BumpSampler.SampleLevel(BumpSamplerState, O.f2TexCoord, 0).ga + 0.5) * 2));

	f3Position +=  normalize(f3Normal * bumpN.x + bumpN.y * f3Tangent + bumpN.z * cross(f3Normal, f3Tangent)) * g_f4Bumpiness.x;
	
	// Transform model position with view-projection matrix
	O.f4Position = mul( float4( f3Position, 1.0 ), gWorldViewProj );
	O.f3Position = mul( float4( f3Position, 1.0f), gWorld ).xyz;
	return O;
}
#endif //__SHADERMODEL >= 50

technique draw
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		VertexShader = compile VERTEXSHADER VS_RenderScene();
		PixelShader  = compile PIXELSHADER PS_RenderScene();
	}
}

technique drawskinned
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		
		VertexShader = compile VERTEXSHADER VS_RenderScene();
		PixelShader  = compile PIXELSHADER PS_RenderScene();
	}
}

technique tessellate_draw
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		
#if (__SHADERMODEL >= 50)
		VertexShader = compile VERTEXSHADER VS_RenderSceneWithTessellation();
		SetDomainShader( CompileShader( ds_5_0, DS_PNTriangles() ) );
		SetHullShader( CompileShader( hs_5_0, HS_PNTriangles() ) );
#else
		VertexShader = compile VERTEXSHADER VS_RenderScene();
#endif
		PixelShader  = compile PIXELSHADER PS_RenderScene();
	}
}

technique tessellate_drawskinned
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		
#if (__SHADERMODEL >= 50)
		VertexShader = compile VERTEXSHADER VS_RenderSceneWithTessellation();
		SetDomainShader( CompileShader( ds_5_0, DS_PNTriangles() ) );
		SetHullShader( CompileShader( hs_5_0, HS_PNTriangles() ) );
#else
		VertexShader = compile VERTEXSHADER VS_RenderScene();
#endif
		PixelShader  = compile PIXELSHADER PS_RenderScene();
	}
}
