// 
// sample_dx11\sample_geometryshader.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE:
//		This sample shows all the bank widgets in action.


// TITLE: Drawable
// PURPOSE:
//		This sample shows how to load and draw a skinned character.

// Useful command line besides the default one:
//
// Play an animation:
// -file t:/rage/assets/rage_male/rage_male.type -anim t:/rage/assets/animation/motion/male_wlk.anim
//
// Stress test the renderer
// -file t:/rage/assets/rage_male/rage_male.type -many=10 -perobjectlighting
//
// View GTA4 assets:
// -zup -archive x:/gta/build/ps3/data/maps/east/queens_e.img -rscLoad x:/gta/build/ps3/data/maps/east/ap_cargo_hangar -shaderlib x:/gta/build/common/shaders -shaderdb x:/gta/build/common/shaders/db
//
// Rage Wilderness Demo:
// -file C:\Rage\rageassets\dev\rageland\assets\rageland\ragewood\ragewood_robot\ragewood_robot.type -shaderlib C:\Rage\rageassets\dev/rage/assets/sample_wilderness/wildshaders -shaderdb C:\Rage\rageassets\dev/rage/assets/sample_wilderness/wildshaders/db

/*
    Basic performance numbers, 360 Release
    -file t:/rage/assets/rage_male/rage_male.type -anim t:/rage/assets/animation/motion/male_wlk.anim -many=6 -freeze
        Baseline:			CPU draw time 0.46ms, GPU render time 2.167ms
        MTX_IN_VB=1			CPU draw time 0.34ms, GPU render time 2.209ms (predication is faster)
        +MTX_IN_VB_HALF=1	CPU draw time 0.35ms, GPU render time 1.904ms (predication is slower)
        MTX_IN_TEX=1		CPU draw time 0.34ms, GPU render time 2.291ms
        +MTX_IN_TEX_HALF=1	CPU draw time 0.35ms, GPU render time 1.998ms
*/

#define GCM_BUFFER_SIZE	1024 // zero forces FIFO into VRAM, which is not supported right now for replay at least
#include "sample_dx11.h"

#define ENABLE_RFS		1

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
// #include "data/marker.h"	// included by main.h now, sigh
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "grmodel/shaderfx.h"
#include "grprofile/drawmanager.h"
#include "profile/timebars.h"
#include "rmcore/commandbufferset.h"
#include "rmcore/drawable.h"
#include "system/cache.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grcore/vertexBuffer.h"

// For debug visualization:
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

#include "embedded_rage_diffuse_win32_30.h"
#include "embedded_rage_diffuse_alpha_win32_30.h"

#if __PS3
#include "grcore/grcorespu.h"
#endif

using namespace rage;

PARAM(file,"[sample_geometryshader] The filename of the .type file to load (default entity.type)");
PARAM(anim,"[sample_geometryshader] The filename of the animation file to load (default is none)");
PARAM(rscAnim, "[sample_geometryshader] The filename of the animation resource file to load (default is none)");
PARAM(mesh,"[sample_geometryshader] The filename of a mesh to load for debug visualization");
PARAM(rscLoad,"[sample_geometryshader] Load drawable from existing named resource (default: resourceTest)");
PARAM(rscBuild,"[sample_geometryshader] Save drawable to resource (default: resourceTest)");
PARAM(center, "[sample_geometryshader] Put the drawable at the origin");
PARAM(many, "[sample_geometryshader] Draw N*N copies of the object for performance testing");
PARAM(perobjectlighting, "[sample_geometryshader] Fake per-object lighting to stress fragment shader patching");
PARAM(freeze,"[sample_geometryshader] Freeze animation playback after one frame to stabilize GPU time");

class rmcSampleGeometryShader : public ragesamples::dx11SampleManager
{
	static const int listSize = 32;

public:
	virtual const char* GetSampleName() const
	{
		return "sample_geometryshader";
	}

	void InitClient()
	{
		RAGE_FUNC();

		// Set this to something like 4000,0,0 to observe z fighting on skinned
		// models prior to my hackery involving world matrices.
		m_Fudge.Set(0,0,0);

		m_DebugShader = false;
		m_BlitTest = 0;

		// m_DrawGrid = m_DrawWorldAxes = false;

#if __PFDRAW
		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
		GetRageProfileDraw().SetEnabled(true);
#endif
		m_LightMode = grclmPoint;
		
		dx11SampleManager::InitClient();

		crAnimation::InitClass();

		// STEP #1. Allocate and load our drawable instance.

		//-- The first thing we do is allocate our <c>rmcDrawable</c> instance.
		m_Drawable = NULL;
		m_TexDict = NULL;
		const char *rscName = "resourceTest";

		const char *drwExt = "#dr";
		const char *tdExt = "#td";
		if (PARAM_rscLoad.Get() && PARAM_rscBuild.Get())
			Quitf("Cannot have both -rscLoad and -rscBuild on the command line at the same time");

		if (PARAM_rscLoad.Get()) {
			PARAM_rscLoad.GetParameter(rscName);
			Displayf("Loading resource heap from '%s'",rscName);
			pgRscBuilder::LoadDefragmentable(m_TexDict,rscName,tdExt,grcTexture::RORC_VERSION);
			if (m_TexDict) {
				pgDictionary<grcTexture>::SetCurrent(m_TexDict);
			}

			pgRscBuilder::LoadDefragmentable(m_Drawable,rscName,drwExt,rmcDrawable::RORC_VERSION);

			if (m_TexDict)
				pgDictionary<grcTexture>::SetCurrent(NULL);

			if (!m_Drawable)
				Quitf("Resource file %s file not found",rscName);
		}
		else {
				m_Drawable = rage_new rmcDrawable();

				//-- Now we load the drawable.  
				if( !m_Drawable->Load(GetFileName(0)) )
					Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );
				// m_Drawable->Optimize();
		}

		// STEP #2. Initialize our skeleton.

		//-- The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		crSkeletonData *skeldata = m_Drawable->GetSkeletonData();
		if (skeldata)
		{
			m_Skeleton = rage_new crSkeleton();

			m_Skeleton->Init(*skeldata,NULL);
			
			if(PARAM_anim.Get() && PARAM_rscAnim.Get())
			{
				Quitf("Cannot have both -anim and -rscAnim on the command line at the same time");
			}

			//-- In case we want to play animations on the skeleton, load an animation file
			if ( PARAM_anim.Get() ) 
			{
				const char *filename = "";
				PARAM_anim.Get(filename);
				m_Anim = crAnimation::AllocateAndLoad(filename);
			}
#if 0
			else if(PARAM_rscAnim.Get())
			{
				const char *animExt = "#an";
				const char *rscAnimName = NULL;

				PARAM_rscAnim.GetParameter(rscAnimName);
				Displayf("Loading animation resource from '%s'", rscAnimName);

//				pgRscBuilder::LoadDefragmentable(m_Anim, rscAnimName, animExt, crAnimation::RORC_VERSION);
				pgRscBuilder::Load(m_Anim, rscAnimName, animExt, crAnimation::RORC_VERSION);

				if(!m_Anim)
				{
					Quitf("Animation resource file %s file not found", rscAnimName);
				}
			}
#endif
			else 
			{
				m_Anim = 0;
			}
		}
		else 
		{
			m_Skeleton = 0;
			m_Anim = 0;
		}
		// -STOP

		m_Mesh = NULL;
		const char *meshName;
		if (PARAM_mesh.Get(meshName)) {
			m_Mesh = rage_new mshMesh;
			if (!SerializeFromFile(meshName,*m_Mesh,"mesh"))
				Quitf("Unable to load debug mesh '%s'",meshName);
		}

		m_PlaybackRate = 30.0f;
		m_AutoDolly = false;
		m_AutoSlide = false;
		m_UpdateAnim = m_Anim != 0;
		m_SlideRate.Set(0.75f, 0.55f, 0.35f);
		m_RotateRate = 0.5f;
		m_SlideTimer = 0.f;
		m_RotateTimer = 0.f;
		m_FrameTimer = 0.f;
		m_VertexNormalLength = 0.02f;
		m_VertexNormalColor = Color32(1.0f,0.0f,0.0f);
		m_ModelOpacity = 1.0f;
		m_NumRenderInstancesX = 1;
		m_NumRenderInstancesY = 1;
		m_NumRenderInstancesZ = 1;
		if (PARAM_many.Get()) {
			PARAM_many.Get(m_NumRenderInstancesX);
			PARAM_many.Get(m_NumRenderInstancesZ);
		}
		m_TotalRenderInstances = m_NumRenderInstancesX * m_NumRenderInstancesY * m_NumRenderInstancesZ;
		m_MatrixSet = m_Skeleton? grmMatrixSet::Create(*m_Skeleton) : NULL;
		m_PerObjectLighting = PARAM_perobjectlighting.Get();
		m_RenderDistance.Set(0.5f,0.5f,0.5f);
		m_CubeScale.Set(0.01f, 0.01f, 0.01f);
		m_TexCoordScale = 0.01f;
		m_Wireframe = false;
		m_EnableGS = false;
		// command buffer:
		m_DrawUsingCommandBuffers = false;
#if COMMAND_BUFFER_SUPPORT
		m_CommandBuffers = NULL;
#endif

#if __PS3
		m_List = GRCDEVICE.CreateDisplayList(listSize);
		for (int i=0; i<listSize-1; i++)
			m_List[i] = 0;
#endif
	}

	void ShutdownClient()
	{
		RAGE_FUNC();

#if __PS3
		GRCDEVICE.DeleteDisplayList(m_List);
#endif

#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif
		delete m_MatrixSet;
		delete m_Mesh;
		delete m_Skeleton;
		// It's okay to call pgRscBuilder::Delete on something that isn't on the
		// resource heap, but it's not okay to just delete something that is on
		// the resource heap.
		pgRscBuilder::Delete(m_Drawable);
		pgRscBuilder::Delete(m_TexDict);
		delete m_Anim;

#if COMMAND_BUFFER_SUPPORT
		delete m_CommandBuffers;
#endif

		crAnimation::ShutdownClass();
	}

#if COMMAND_BUFFER_SUPPORT
	static void SetCommandBufferState()
	{
		grcState::SetAlphaBlend(true);

		grcState::SetDepthFunc(grcdfLessEqual);
		grcState::SetDepthTest(true);
		grcState::SetDepthWrite(true);
		
		grcState::SetBlendSet(grcbsNormal);
	}
#endif

	void UpdateClient()
	{
		RAGE_FUNC();

		//-- If we want an animation, update it here
		//Note : We don't want to update the animation if vertex normals are being drawn, so the animation resumes from the
		//frame when the draw normalss button was pressed.
		if ( m_UpdateAnim ) 
		{
			m_FrameTimer += TIME.GetSeconds() * m_PlaybackRate;
			if (PARAM_freeze.Get())
				m_UpdateAnim = false;
		}
		if ( m_Anim ) 
		{
			crFrame frameBuf;
			frameBuf.InitCreateBoneAndMoverDofs(m_Skeleton->GetSkeletonData(), false);
			m_Anim->CompositeFrame(fmodf(m_Anim->Convert30FrameToTime(m_FrameTimer), m_Anim->GetDuration()), frameBuf);
			frameBuf.Pose(*m_Skeleton);
		}

		m_Center.Identity();

		//-- If we want to update orientation, do it here
		if ( m_AutoDolly ) {
			m_RotateTimer += TIME.GetSeconds();
		}
		m_Center.RotateLocalY( Wrap(m_RotateTimer * m_RotateRate, 0.f, 2.f * PI) );
		
		//-- If we want to update position, do it here
		if ( m_AutoSlide ) { 
			m_SlideTimer += TIME.GetSeconds();
		}
		m_Center.d.x = m_Drawable->GetLodGroup().GetCullRadius() * 2.0f * sinf(m_SlideTimer * m_SlideRate.x);
		m_Center.d.z = m_Drawable->GetLodGroup().GetCullRadius() * 2.0f * -sinf(m_SlideTimer * m_SlideRate.z);
		m_Center.d.y = m_Drawable->GetLodGroup().GetCullRadius() * sinf(m_SlideTimer * m_SlideRate.y);

		if (PARAM_center.Get()) {
			m_Center.d = -m_Drawable->GetLodGroup().GetCullSphere();
		}


		m_Center.d += m_Fudge;

		//-- If we have a skeleton, inform it of it's new position & update all child bones
		if ( m_Skeleton ) {
			m_Skeleton->SetParentMtx(&m_Center);
			m_Skeleton->Update();
			m_MatrixSet->Update(*m_Skeleton,m_Drawable->IsSkinned());
		}
	}

	void InitCamera()
	{
		m_OrthoCam.InitViews();
		InitSampleCamera(m_Fudge + Vector3(0.f, 2.5f, -5.f), m_Fudge);
	}
	void DrawClient()
	{
		RAGE_FUNC();

		PF_START_TIMEBAR("ProfileDraw");

		grcEffectGlobalVar debugShader = grcEffect::LookupGlobalVar("debugShader", false);
		if (debugShader)
			grcEffect::SetGlobalVar(debugShader,m_DebugShader);

#if __PFDRAW
		if(m_Skeleton)
			m_Skeleton->ProfileDraw(0.05f);
		if(m_Drawable)
			m_Drawable->ProfileDraw(m_Skeleton, &m_Center);

		const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
		GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
		GetRageProfileDraw().Render();
#endif

		m_DrawableDraw = 0.0f;
		m_TotalRenderInstances = m_NumRenderInstancesX * m_NumRenderInstancesY * m_NumRenderInstancesZ;

		{
			grcLightGroup group1 = m_Lights, group2 = m_Lights;
			group1.SetColor(0, 1,0,0);
			group2.SetColor(0, 0,1,0);

			for (int x=0;x<m_NumRenderInstancesX;x++)
			{
				for (int y=0;y<m_NumRenderInstancesY;y++)
				{
					for (int z=0;z<m_NumRenderInstancesZ;z++)
					{
#if RAGE_TIMEBARS
						char buf[64];
						PF_START_TIMEBAR(formatf(buf,"Object %d %d %d",x,y,z));
#endif

						if (m_PerObjectLighting)
							grcState::SetLightingGroup((x+y+z)&1? group2 : group1);
						Matrix34 mtx;
						mtx.Identity3x3();
						mtx.d.Set(x*m_RenderDistance.x,y*m_RenderDistance.y,z*m_RenderDistance.z);
		
						// STEP #3. Render our character.
						u8 lod;
						//-- First we do a visibility check versus the view frustrum.  If it 
						// is visible, the level of detail is returned in <c>lod</c>.
						if (m_Drawable->IsVisible(mtx,*grcViewport::GetCurrent(),lod)) {

							//-- Here we set the texture LOD based on the character's distance
							// from the viewer.
							float zDist = GetCamMgr().GetWorldMtx().d.Mag();
							grcTextureFactory::GetInstance().SetTextureLod(zDist);

							if (!m_DrawUsingCommandBuffers)
							{
								//-- Draw the character. 
								sysTimer drawableDraw;
								for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
								{
									if (GetBucketEnable(bucket) && (m_Drawable->GetBucketMask(lod) & (1 << bucket))) 
									{
										// bool bDrawSkinned = m_Drawable->GetLodGroup().GetLodModel0(lod).GetSkinFlag();

										if (m_Drawable->GetSkeletonData() /* && bDrawSkinned*/)
											m_Drawable->DrawSkinned(mtx,*m_MatrixSet,bucket,lod);
										else
											m_Drawable->Draw(mtx,bucket,lod); 
									}
								}
								m_DrawableDraw += drawableDraw.GetMsTime();
								// -STOP
							}
#if COMMAND_BUFFER_SUPPORT
							else
							{
								sysTimer drawableDraw;

								// create the command buffers:
								if (m_CommandBuffers==NULL)
								{
									m_CommandBuffers=rage_new rmcCommandBufferSet();
									grmModel::SetCommandBufferGraphicsState functor;
									functor.Reset<SetCommandBufferState>();
									m_Drawable->CreateCommandBuffers(*m_CommandBuffers,0,NUM_BUCKETS,functor);
								}

								// draw using the command buffers:
								for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
								{
									bool bDrawSkinned = m_Drawable->GetLodGroup().GetLodModel0(lod).GetSkinFlag();
									if (m_Drawable->GetSkeletonData() && bDrawSkinned)
										m_Drawable->DrawSkinned(mtx,*m_MatrixSet,bucket,lod,*m_CommandBuffers);
									else
										m_Drawable->Draw(mtx,bucket,lod,*m_CommandBuffers);

								}

								m_DrawableDraw += drawableDraw.GetMsTime(); 
							}
#endif

							if (m_Mesh && GetBucketEnable(NUM_BUCKETS-1)) { 
								grcBindTexture(NULL); 
								mshRendererGrc renderer;
								if (m_Drawable->GetSkeletonData()) 
								{
									Matrix43 *mtx = Alloca(Matrix43,m_Drawable->GetSkeletonData()->GetNumBones());
									m_Skeleton->Attach(true, mtx);
									for (int i=0; i<m_Drawable->GetSkeletonData()->GetNumBones(); i++) {
										Matrix34 m;
										mtx[i].ToMatrix34(m);
										grcDrawAxis(.2f,m);
									}
								}
								else
								{
									m_Mesh->Draw(renderer);
								}
							}
							// -STOP
						}
					}
				}
			}
		}

#if __PS3 && SPU_GCM_FIFO && 0
		SPU_COMMAND(grcDevice__Jump,0);
		cmd->jumpLocalOffset = gcm::GetOffset(m_List);
		cmd->jumpBack = &m_List[listSize-1];
#endif

		grcState::Default();
		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcFont::GetCurrent().Drawf(100,200,"drawable: %5.2fms",m_DrawableDraw);
		grcViewport::SetCurrent(old);
	}

#if __BANK
	static void EnableAllShaders()
	{
		/* s32 presetCount = grmShaderPreset::GetPresetCount();
		for(int i=0; i<presetCount; i++)
		{
			grmShaderPreset::SetDisable(i, false);
		} */
	}

	void UpdateModelOpacity()
	{
		const Vector4 &curAmbientColor = m_Lights.GetAmbient();
		m_Lights.SetAmbient(curAmbientColor.x, curAmbientColor.y, curAmbientColor.z, m_ModelOpacity);
	}

	void UpdateGeometryShader()
	{
		if ( m_EnableGS ) 
		{
			int groupId = grmShader::FindTechniqueGroupId("geometry");
			grmShader::SetForcedTechniqueGroupId(groupId);
		}
		else 
		{
			grmShader::SetForcedTechniqueGroupId(-1);
		}
	}

	void UpdateModelWireFrame()
	{
		const grmShaderGroup& shdrGrp = m_Drawable->GetShaderGroup();
		for(int i = 0; i < shdrGrp.GetCount(); ++i)
		{
			int flags = shdrGrp.GetShader(i).GetUserFlags();
			flags = (m_Wireframe) ? flags | grmShader::FLAG_DRAW_WIREFRAME : flags & (~grmShader::FLAG_DRAW_WIREFRAME);
			shdrGrp.GetShader(i).SetUserFlags(flags);
		}
	}
	void UpdateCubeScale()
	{
		const grmShaderGroup& shdrGrp = m_Drawable->GetShaderGroup();
		for(int i = 0; i < shdrGrp.GetCount(); ++i)
		{
			grcEffectVar var = shdrGrp.GetShader(i).LookupVar( "g_fCubeScale" );
			if (grcevNONE != var)
			{
				Vector4 cubescale = Vector4(m_CubeScale.x, m_CubeScale.y, m_CubeScale.z, 0.0f);
				shdrGrp.GetShader(i).SetVar( var, cubescale );
			}
		}
	}
	void UpdateTexCoordScale()
	{
		const grmShaderGroup& shdrGrp = m_Drawable->GetShaderGroup();
		for(int i = 0; i < shdrGrp.GetCount(); ++i)
		{
			grcEffectVar var = shdrGrp.GetShader(i).LookupVar( "g_fTexCoordScale" );
			if (grcevNONE != var)
			{
				Vector4 texscale = Vector4(m_TexCoordScale, m_TexCoordScale,0.0f, 0.0f);
				shdrGrp.GetShader(i).SetVar( var, texscale );
			}
		}
		
	}
	void AddWidgetsClient()
	{
		ragesamples::dx11SampleManager::AddWidgetsClient();

		m_Drawable->GetShaderGroup().AddWidgets(BANKMGR.CreateBank("Shaders"));
		bkBank &bk = BANKMGR.CreateBank("rage - SampleGeometryShader");
		bk.AddToggle("Debug Shader",&m_DebugShader,NullCallback,"Toggle 'shaderDebug' global shader bool (if present)");
		bk.AddSlider("Blit Test",&m_BlitTest,0,16,1.0f,NullCallback,"Take the first shader in the test case and do N screen blits to test fragment program thruput");
		bk.AddToggle("Wireframe", &m_Wireframe, datCallback(MFA(rmcSampleGeometryShader::UpdateModelWireFrame), this), "Toggle fill mode of the polys");
		bk.AddToggle("Enable GS", &m_EnableGS, datCallback(MFA(rmcSampleGeometryShader::UpdateGeometryShader), this), "Toggle geometry shader");
		bk.AddSlider("CubeScale", &m_CubeScale, 0.0f, 1.0f, 0.01f, datCallback(MFA(rmcSampleGeometryShader::UpdateCubeScale), this), "Change the size of the cubes");
		bk.AddSlider("TexScale", &m_TexCoordScale, 0.0f, 1.0f, 0.01f, datCallback(MFA(rmcSampleGeometryShader::UpdateTexCoordScale), this), "Change the size of the texcoords");


		bk.PushGroup("Movement",false,"Group of widgets for automatic movements of the current drawable");
		bk.AddToggle("Auto Dolly", &m_AutoDolly);
		bk.AddSlider("Rotation Rate", &m_RotateRate, -10000.f, 10000.f, 0.01f);
		bk.AddToggle("Auto Slide", &m_AutoSlide);
		bk.AddSlider("Slide Rate", &m_SlideRate, -10000.0f, 10000.0f, 0.01f);
		if ( m_Anim ) {
			bk.AddToggle("Update Animation", &m_UpdateAnim);
			bk.AddSlider("Animation Rate (fps)", &m_PlaybackRate, -1000.0, 1000.0f, 0.01f);
		}
		bk.PopGroup();

		bk.AddSlider("Model Opacity", &m_ModelOpacity, 0.0f, 1.0f, 0.1f, datCallback(MFA(rmcSampleGeometryShader::UpdateModelOpacity),this));
		// bk.AddButton("Enable All Shaders", (CFA(rmcSampleGeometryShader::EnableAllShaders)));

		bk.PushGroup("Multiple Instances",false,"Group of widgets for displaying multiple instances of the current drawable");
		bk.AddSlider("Num Render Instances X",&m_NumRenderInstancesX,0,1000,1);
		bk.AddSlider("Num Render Instances Y",&m_NumRenderInstancesY,0,1000,1);
		bk.AddSlider("Num Render Instances Z",&m_NumRenderInstancesZ,0,1000,1);
		bk.AddSlider("Total # of Instances",&m_TotalRenderInstances,0,1000*1000*1000,0);

		bk.AddSlider("Render Distance",&m_RenderDistance,0.0f,10000.0f,0.5f);   
		bk.AddSlider("Render Fudge",&m_Fudge,-100000,100000,1);
		bk.PopGroup();

#if COMMAND_BUFFER_SUPPORT
		bk.AddToggle("Draw Using Command Buffers", &m_DrawUsingCommandBuffers);
#endif
	}
#endif

private:
	rmcDrawable				*m_Drawable;
	pgDictionary<grcTexture>*m_TexDict;
	crSkeleton*				m_Skeleton;
	crAnimation*			m_Anim;
	Matrix34				m_Center;
	Vector3					m_SlideRate;
	mshMesh*				m_Mesh;
	float					m_PlaybackRate;
	float					m_RotateRate;
	float					m_SlideTimer;
	float					m_RotateTimer;
	float					m_FrameTimer;
	float					m_DrawableDraw;
	float					m_VertexNormalLength;
	Color32					m_VertexNormalColor;
	bool					m_AutoDolly;
	bool					m_AutoSlide;
	bool					m_UpdateAnim;
	float					m_ModelOpacity;
	int						m_NumRenderInstancesX;
	int						m_NumRenderInstancesY;
	int						m_NumRenderInstancesZ;
	int						m_TotalRenderInstances;
	Vector3					m_RenderDistance;
	int						m_BlitTest;
	bool					m_Wireframe;
	bool					m_EnableGS;
	Vector3					m_CubeScale;
	float					m_TexCoordScale;
	Vector3					m_Fudge;

	bool					m_DrawUsingCommandBuffers;
	bool					m_PerObjectLighting;
	bool					m_DebugShader;

	grmMatrixSet			*m_MatrixSet;
#if COMMAND_BUFFER_SUPPORT
	// command buffers:
	rmcCommandBufferSet*	m_CommandBuffers;
#endif
#if __PS3
	u32						*m_List;
#endif
};

#include "data/struct.h"

// main application
int Main()
{
	rmcSampleGeometryShader sampleRmc;
	sampleRmc.Init("sample_dx11\\tessellation");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
