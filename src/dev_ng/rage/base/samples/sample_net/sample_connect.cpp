// 
// sample_net/sample_connect.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "net/connectionmanager.h"
#include "net/nethardware.h"
#include "net/netsocket.h"
#include "net/netdiag.h"
#include "net/lag.h"
#include "net/timesync.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"
#include "diag/output.h"	// for TColors...

#if __XENON
#include "system/xtl.h"
#include <xtitleidbegin.h>
    XEX_TITLE_ID(0x545407EA)
#include <xtitleidend.h>
#endif  //__XENON

using namespace rage;

//  This sample demonstrates how to establish a network connection and send
//  and receive data to and from a remote peer.
//
//  Although this is a sample demonstrating networking concepts it is designed
//  to run completely on a single machine.  This is achieved by configuring
//  both peers within the same process and communicating on the loopback
//  device.

//PURPOSE
//  Encapsulates the data required to represent a simple peer.
class SampleConnectPeer
{
public:

    //IP ports used for host and guest, respectively.
    //We use different ports because this sample runs entirely
    //on one machine.  In a real application we'd probably use
    //the same port on every peer in order to simplify NAT traversal.
    static const unsigned short HOST_PORT   = 0xFFF0;
    static const unsigned short GUEST_PORT  = 0xFFF1;

    static const unsigned LOOPBACK_ADDR     = NET_MAKE_IP(127, 0, 0, 1);

    //The frequencies at which the host and guest will send frames.
    static const unsigned HOST_SEND_INTERVAL    = 1250;
    static const unsigned GUEST_SEND_INTERVAL   = 1000;

    enum
    {
        //Channel id used to open connections.
        //Channels are used to segregate messages.
        //Multiple connections can be opened to the same remote peer as long
        //as each is on a different channel.  When a connection is opened to
        //a remote peer, the remote peer must simultaneously open a connection
        //to us using the same channel.
        CHANNEL_ID  = 13,

        //Channel on which time synchronization messages will be sent/received.
        TIMESYNC_CHANNEL_ID,

        //Channel on which ping messages for lag calculations will be sent/received.
        LAG_CHANNEL_ID,
    };

    //Maximum number of connections managed by the connection manager.
    //This is only a hint to the connection manager so it can optimize
    //initial memory allocations.  This does not limit the number of
    //connections that can be opened.
    static const int MAX_CXNS               = 8;

    SampleConnectPeer()
        : m_CxnId(-1)
        , m_NetHw(0)
		, m_Allocator(m_Heap, sizeof(m_Heap), sysMemSimpleAllocator::HEAP_NET)
        , m_NextSendTime(0)
        , m_MigrateTimeSyncCountdown(10)
        , m_ShowBandwidthCountdown(5)
        , m_ShowLagCountdown(5)
        , m_IsHost(false)
        , m_IsInitialized(false)
    {
        //Bind our delegate (callback) to our connection event handler.
        //The connection manager will dispatch connection events to
        //this delegate.
        m_CxnDelegate.Bind(this, &SampleConnectPeer::OnCxnEvent);
    }

    //PURPOSE
    //  Initialize the sample peer.
    //PARAMS
    //  netHw       - Network hardware instance.
    //  isHost      - True if we're the host.
    void Init(netHardware* netHw,
               const bool isHost)
    {
        if(AssertVerify(!m_IsInitialized))
        {
            m_NetHw = netHw;

            //Create the socket we'll use to communicate game data.
            //This sample runs completely on a single machine so bind
            //the socket to different ports depending on if it's the
            //host or the guest.
            AssertVerify(netHw->CreateSocket(&m_GameSocket,
                                            isHost ? (unsigned short)HOST_PORT : GUEST_PORT,
                                            NET_PROTO_UDP));

            //Register our bandwidth recorder with the socket.
            m_GameSocket.RegisterPacketRecorder(&m_BwRecorder);

            //m_GameSocket.SetFakeOutboundBandwidthLimit(100);
            //m_GameSocket.SetFakeInboundBandwidthLimit(100);

            /*netSocket::FakeLatency fl;
            fl.m_BaselineLatency = 200;
            m_GameSocket.SetFakeLatency(fl);*/

            //Initialize the connection manager with the maximum number of
            //connections we expect to create, the network socket we wish to
            //use, the allocator that will be used to allocate everything
            //(i.e. connections, events, frames, etc.), the cpu on which we
            //wish to run the send/receive operations.
            m_CxnMgr.Init(&m_Allocator, MAX_CXNS, &m_GameSocket, 0);

            //Prevent the allocator from calling Quitf() if it fails to
            //allocate memory.  The connection manager will make an attempt
            //to recover from memory allocation failures by reclaiming memory
            //allocated to non-critical areas, such as unreliable messages.
            m_Allocator.SetQuitOnFail(false);

            //Register our delegate (callback) with the connection manager
            //so we can receive connection events.  We will receive events
            //for connections opened on the given channel.
            m_CxnMgr.AddDelegate(&m_CxnDelegate, CHANNEL_ID);

            //We're going to send frames on a regular interval.  This is
            //the next time we'll send a frame.  By setting the next time
            //to now, we'll send a frame as soon as we're connected.
            m_NextSendTime = sysTimer::GetSystemMsTime();

            //Open a connection.  If the remote peer opens a connection with
            //on the same channel then the connection will be established.

            m_IsHost = isHost;

            const netAddress addr(LOOPBACK_ADDR, m_IsHost ? (unsigned short)GUEST_PORT : HOST_PORT);

            //Synchronize our time.  If we're the host then guests will
            //synchronize to us.  If we're the guest then we'll sync to the
            //host.
            m_TimeSync.Start(&m_CxnMgr,
                            isHost ? netTimeSync::FLAG_SERVER : netTimeSync::FLAG_CLIENT,
                            &addr,
                            NULL,
                            TIMESYNC_CHANNEL_ID,
                            2 * 1000,
                            60 * 1000);

            //Calculate lag.
            m_Lag.Start(&m_CxnMgr,
                        addr,
                        LAG_CHANNEL_ID,
                        2 * 1000);

            m_IsInitialized = true;

			u8 mac[6];
			bool bSuccess = netAddress::GetLocalMacAddress(mac);
			AssertMsg(bSuccess, "Failed to Get Local Mac Address");
			
			Displayf("MAC Address - %02X %02X %02X %02X %02X %02X\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

			unsigned ip;
			bSuccess = netAddress::GetLocalIpAddress(&ip);
			AssertMsg(bSuccess, "Failed to Get Local IP Address");
			Displayf("IP: %d:%d:%d:%d", (0xFF000000 & ip) >> 24,(0xFF0000 & ip) >> 16,(0xFF00 & ip) >> 8,(0xFF & ip));
        }
    }

    void Shutdown()
    {
        if(m_IsInitialized)
        {
            //Stop synchronizing our time.
            m_TimeSync.Stop();

            //Stop computing lag.
            m_Lag.Stop();

            //Remove the delegate we registered in Init().
            m_CxnMgr.RemoveDelegate(&m_CxnDelegate);

            //Unregister our bandwidth recorder from the socket.
            m_GameSocket.UnregisterPacketRecorder(&m_BwRecorder);

            //Shut down the connection manager.
            m_CxnMgr.Shutdown();

            //Destroy our socket.
            m_NetHw->DestroySocket(&m_GameSocket);

            m_NetHw = 0;
            m_CxnId = -1;
            m_NextSendTime = 0;
            m_IsHost = false;

            m_IsInitialized = false;
        }
    }

    //PURPOSE
    void Update()
    {
        Assert(m_IsInitialized);

        const unsigned curTime = sysTimer::GetSystemMsTime();

        //Calling Update() performs the operations of sending frames,
        //receiving frames, and dispatching events to the app.
        m_CxnMgr.Update(curTime);

        //Update the time sync.
        m_TimeSync.Update();

        //Update lag calculation.
        m_Lag.Update();

        if(m_CxnId < 0)
        {
            //We're not yet connected

            const netAddress addr(LOOPBACK_ADDR, m_IsHost ? (unsigned short)GUEST_PORT : HOST_PORT);

            m_CxnId = m_CxnMgr.OpenConnection(addr, CHANNEL_ID, NULL, 0, NULL);

            m_NextSendTime = curTime;
        }
        //If our connection is open and it's time to send then send a frame.
        else if(m_CxnMgr.IsOpen(m_CxnId)
                 && int(curTime - m_NextSendTime) >= 0)
        {
            if(m_IsHost)
            {
                Displayf("HOST: Sending \"Hello!\"");

                //Send a host frame every so many ms.
                m_NextSendTime = curTime + HOST_SEND_INTERVAL;

                m_CxnMgr.Send(m_CxnId,
                               "Hello!",
                               sizeof("Hello!"),
                               NET_SEND_RELIABLE,
                               NULL);
            }
            else
            {
                Displayf("GUEST: Sending \"Good Bye!\"");

                //Send a guest frame every so many ms.
                m_NextSendTime = curTime + GUEST_SEND_INTERVAL;

                m_CxnMgr.Send(m_CxnId,
                               "Good Bye!",
                               sizeof("Good Bye!"),
                               NET_SEND_RELIABLE,
                               NULL);
            }

            if(!--m_MigrateTimeSyncCountdown)
            {
                Displayf(TPurple "\n%s Migrating time sync\n",
                        m_IsHost ? "HOST:" : "GUEST:");

                m_MigrateTimeSyncCountdown = 10;

                if(m_TimeSync.IsClient())
                {
                    m_TimeSync.Restart(netTimeSync::FLAG_SERVER, NULL);
                }
                else
                {
                    m_TimeSync.Restart(netTimeSync::FLAG_CLIENT,
                                        &m_CxnMgr.GetAddress(m_CxnId));
                }
            }

            if(!--m_ShowBandwidthCountdown)
            {
                Displayf(TBlue "\n%s Out:%d, In:%d\n",
                        m_IsHost ? "HOST:" : "GUEST:",
                        m_BwRecorder.GetOutboundBandwidth(),
                        m_BwRecorder.GetInboundBandwidth());

                m_ShowBandwidthCountdown = 5;
            }

            if(!--m_ShowLagCountdown)
            {
                Displayf(TGreen "\nLag:%d\n", m_Lag.GetAvgRtTime());

                m_ShowLagCountdown = 5;
            }
        }
    }

    unsigned GetNetworkTime()
    {
        return m_TimeSync.GetTime();
    }

    bool IsInitialized() const
    {
        return m_IsInitialized;
    }

private:

    //PURPOSE
    //  This function is bound to the delegate (callback) that we register
    //  with the connection manager.  The connection manager will pass all
    //  connection events to this function.
    void OnCxnEvent(netConnectionManager* cxnMgr,
                     const netEvent* evt)
    {
        //Get the event id.
        const unsigned eid = evt->GetId();

        if(NET_EVENT_CONNECTION_ESTABLISHED == eid)
        {
            //A connection to a remote peer was established.
            Assert(m_CxnId == evt->m_CxnEstablished->m_CxnId);
        }
        else if(NET_EVENT_CONNECTION_CLOSED == eid)
        {
            //One of our connections was closed.
            if(-1 != m_CxnId)
            {
                m_CxnId = -1;
            }
        }
        else if(NET_EVENT_CONNECTION_ERROR == eid)
        {
            //An error occurred on the connection this could be due to
            //a timeout, a send error, or a termination command sent
            //from the remote peer.
            //In any case the connection is not closed by the connection manager,
            //so we close it here.
            cxnMgr->CloseConnection(m_CxnId, NET_CLOSE_IMMEDIATELY);
        }
        else if(NET_EVENT_FRAME_RECEIVED == eid)
        {
            //Received a frame of data.
            //A frame is the basic unit of data sent over the network.
            //Typically we would know what kind of data it
            //is by looking at the value of the channel id.
            //IOW, we segregate different types of data onto different
            //channels.
            //
            //A negative connection id implies the frame was sent
            //"out of band", i.e. ouside of any connection.  Out of band
            //frames are sent using netConnectionManager::SendOutOfBand()
            //or netConnectionManager::SubnetBroadcast().
            netEventFrameReceived* fr = evt->m_FrameReceived;

            Displayf("%s: Received \"%s\"",
                      m_IsHost ? "HOST" : "GUEST",
                      (const char*) fr->m_Payload);
        }
        else if(NET_EVENT_ACK_RECEIVED == eid)
        {
            //We received an ACK for one of the frames we sent.
            //If we had previously recorded the sequence number generated
            //when we sent a frame (by passing a pointer to a netSequence
            //instance to netConnectionManager::Send()) we could compare it
            //to the sequence number here and determine if our frame was
            //received by the remote peer.
            //
            //Note that an ACK for an unreliable frame means only that that
            //particular frame arrived at the remote peer.  Other frames sent
            //may or may not have arrived.
            //
            //Whereas every reliable frame is guaranteed to generate an
            //ACK the same cannot be said for unreliable frames.
        }
        else if(NET_EVENT_OUT_OF_MEMORY == eid)
        {
            //Not much we can do here...
            Quitf("Ran out of memory :(");
        }
    }

    //The connection manager.
    netConnectionManager m_CxnMgr;

    //Used to synchronize time with remote peers.
    netTimeSync m_TimeSync;

    //Used to calculate lag to remote peers.
    netLag m_Lag;

    //The connection id we use to send/receive frames.
    int m_CxnId;

    //Instance of netHardware used to create new sockets.
    netHardware* m_NetHw;

    //The socket used for communicating game data.
    netSocket m_GameSocket;

    //We'll register this with the socket to record in/out bandwidth stats.
    netBandwidthRecorder m_BwRecorder;

    //The heap used for allocating connections, events, frames, etc.
    u8 m_Heap[64 * 1024];

    //The allocator used for allocating connections, events, frames, etc.
    sysMemSimpleAllocator m_Allocator;

    //We send on a regular interval - this is the next time we'll send.
    unsigned m_NextSendTime;

    //The delegate we register with the connection manager in order to
    //receive connection events.
    netConnectionManager::Delegate m_CxnDelegate;

    unsigned m_MigrateTimeSyncCountdown;

    unsigned m_ShowBandwidthCountdown;

    unsigned m_ShowLagCountdown;

    //The IP port on which we'll communicate game data.
    unsigned short m_GamePort;

    //True if we're the host.
    bool m_IsHost   : 1;

    //True if one of the Init* functions was called.
    bool m_IsInitialized    : 1;
};

int Main()
{
    //Create an instance of netHardware and start the network.
    netHardware hw;

    //Create our sample instances.
    SampleConnectPeer sampleHost;
    SampleConnectPeer sampleGuest;

    unsigned nextTime = sysTimer::GetSystemMsTime() + 2000;

    while(true)
    {
        hw.Update();
        if(hw.IsAvailable())
        {
            if(!sampleHost.IsInitialized())
            {
                sampleHost.Init(&hw, true);
            }
            else
            {
                sampleHost.Update();
            }

            if(!sampleGuest.IsInitialized())
            {
                sampleGuest.Init(&hw, false);
            }
            else
            {
                sampleGuest.Update();
            }

            const unsigned curTime = sysTimer::GetSystemMsTime();
            if(int(curTime - nextTime) >= 0)
            {
                Displayf(TCyan "\nHost network time:%u", sampleHost.GetNetworkTime());
                Displayf(TCyan "Guest network time:%u\n", sampleGuest.GetNetworkTime());
                nextTime = curTime + 2000;
            }
        }

        sysIpcSleep(1);
    }

	/* NOTREACHED */
    sampleHost.Shutdown();
    sampleGuest.Shutdown();

    return 0;
}
