// 
// sample_net/sample_message.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "net/connectionmanager.h"
#include "net/message.h"
#include "net/nethardware.h"
#include "net/netsocket.h"
#include "net/netdiag.h"
#include "net/stream.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"

#if __XENON
#include "system/xtl.h"
#include <xtitleidbegin.h>
    XEX_TITLE_ID(0x545407EA)
#include <xtitleidend.h>
#endif  //__XENON

using namespace rage;

//  This sample builds on the ideas from sample_connect and demonstrates
//  using net messages.  A net message is an app-defined class that
//  contains hooks that generate a unique id for the message, and permit
//  the message to be exported/imported to/from a network frame.
//
//  Although this is a sample demonstrating networking concepts it is designed
//  to run completely on a single machine.  This is achieved by configuring
//  both peers within the same process and communicating on the loopback
//  device.

//PURPOSE
//  A sample message
class netMsgHello
{
public:

    enum Type
    {
        TYPE_INVALID        = -1,
        TYPE_ANIMAL,
        TYPE_MINERAL,
        TYPE_VEGETABLE,

        TYPE_NUM_TYPES
    };

    //All net messages must contain an invocation of NET_MESSAGE_DECL
    //in their declarations.  The parameter to the macro is the name
    //of the class.  A corresponding invocation of NET_MESSAGE_IMPL
    //must exist in a .cpp file.
    NET_MESSAGE_DECL(netMsgHello);

    static Type NextType()
    {
        static int s_NextType;

        if(s_NextType >= TYPE_NUM_TYPES)
        {
            s_NextType = 0;
        }

        return (Type) s_NextType++;
    }

    netMsgHello()
        : m_Id(-1)
        , m_Type(TYPE_INVALID)
    {
        m_Text[0] = '\0';
    }

    netMsgHello(const int id,
                 const char* text)
        : m_Id(id)
        , m_Type(NextType())
    {
        safecpy(m_Text, text, sizeof(m_Text));
        for(int i = 0; i < (int) sizeof(m_Data);)
        {
            safecpy(&m_Data[i], text, sizeof(m_Data) - i);
            i += ::strlen(text);
        }
    }

    //PURPOSE
    //  Implement serialization.
    NET_MESSAGE_SER(bb, msg)
    {
        return
                //Add one for a signed int
                bb.SerInt(msg.m_Type, datBitsNeeded<TYPE_NUM_TYPES>::COUNT + 1)
                && bb.SerInt(msg.m_Id, 13)
                && bb.SerStr(msg.m_Text, sizeof(msg.m_Text))
                && bb.SerBytes(msg.m_Data, sizeof(msg.m_Data));
    }

    Type m_Type;
    int m_Id;
    char m_Text[64];
    u8 m_Data[256];
};

//PURPOSE
//  A sample message
class netMsgGoodbye
{
public:

    NET_MESSAGE_DECL(netMsgGoodbye);

    netMsgGoodbye()
        : m_Health(0)
    {
        m_Text[0] = '\0';
    }

    netMsgGoodbye(const float health,
                   const char* text)
        : m_Health(health)
    {
        safecpy(m_Text, text, sizeof(m_Text));
    }

    //If you prefer separate export/import functions it can
    //be done as follows...

    NET_MESSAGE_EXPORT(bb, msg)
    {
        return bb.WriteFloat(msg.m_Health)
                && bb.WriteStr(msg.m_Text, sizeof(msg.m_Text));
    }

    NET_MESSAGE_IMPORT(bb, msg)
    {
        return bb.ReadFloat(msg.m_Health)
                && bb.ReadStr(msg.m_Text, sizeof(msg.m_Text));
    }

    float m_Health;
    char m_Text[64];
};

//PURPOSE
//  This message is sent to notify a remote peer to begin receiving
//  a stream of data using the netStream interface.
class netMsgBeginStream
{
public:

    NET_MESSAGE_DECL(netMsgBeginStream);

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUns(msg.m_StreamLen, 32)
                && bb.SerUns(msg.m_StreamId, 32);
    }

    unsigned m_StreamLen;
    unsigned m_StreamId;
};

//For each class containing an invocation of NET_MESSAGE_DECL there must
//be a corresponding invocation of NET_MESSAGE_IMPL in a .cpp file.
NET_MESSAGE_IMPL(netMsgHello);
NET_MESSAGE_IMPL(netMsgGoodbye);
NET_MESSAGE_IMPL(netMsgBeginStream);

class SampleMsgPeer
{
public:

    //IP ports used for host and guest, respectively.
    //We use different ports because this sample runs entirely
    //on one machine.  In a real application we'd probably use
    //the same port on every peer in order to simplify NAT traversal.
    static const unsigned short HOST_PORT   = 0xFFF0;
    static const unsigned short GUEST_PORT  = 0xFFF1;

    static const unsigned LOOPBACK_ADDR     = NET_MAKE_IP(127, 0, 0, 1);

    //The frequencies at which the host and guest will send frames.
    static const unsigned HOST_SEND_INTERVAL    = 1250;
    static const unsigned GUEST_SEND_INTERVAL   = 1000;

    //Channel id used to open connections.
    //Channels are used to segregate messages.
    //Multiple connections can be opened to the same remote peer as long
    //as each is on a different channel.  When a connection is opened to a remote
    //peer, the remote peer must simultaneously open a connection to us using
    //the same channel.
    static const unsigned CHANNEL_ID    = 13;

    //Maximum number of connections managed by the connection manager.
    //This is only a hint to the connection manager so it can optimize
    //initial memory allocations.  This does not limit the number of
    //connections that can be opened.
    static const int MAX_CXNS               = 8;

    SampleMsgPeer()
        : m_CxnId(-1)
        , m_NetHw(0)
        , m_Allocator(m_Heap, sizeof(m_Heap), sysMemSimpleAllocator::HEAP_NET)
        , m_NextSendTime(0)
        , m_IsHost(false)
        , m_StreamComplete(false)
        , m_IsInitialized(false)
    {
        //Bind our delegate (callback) to our connection event handler.
        //The connection manager will dispatch connection events to
        //this delegate.
        m_CxnDelegate.Bind(this, &SampleMsgPeer::OnCxnEvent);
    }

    //PURPOSE
    //  Initialize the sample peer.
    //PARAMS
    //  netHw       - Network hardware instance.
    //  isHost      - True if we're the host.
    void Init(netHardware* netHw,
               const bool isHost)
    {
        if(AssertVerify(!m_IsInitialized))
        {
            m_NetHw = netHw;

            //Create the socket we'll use to communicate game data.
            //This sample runs completely on a single machine so bind
            //the socket to different ports depending on if it's the
            //host or the guest.
            AssertVerify(netHw->CreateSocket(&m_GameSocket,
                                            isHost ? (unsigned short)HOST_PORT : GUEST_PORT,
                                            NET_PROTO_UDP));

            //Initialize the connection manager with the maximum number of
            //connections we expect to create, the network socket we wish to
            //use, the allocator that will be used to allocate everything
            //(i.e. connections, events, frames, etc.), the cpu on which we
            //wish to run the send/receive operations.
            m_CxnMgr.Init(&m_Allocator, MAX_CXNS, &m_GameSocket, -1);

            //Prevent the allocator from calling Quitf() if it fails to
            //allocate memory.  The connection manager will make an attempt
            //to recover from memory allocation failures by reclaiming memory
            //allocated to non-critical areas, such as unreliable messages.
            m_Allocator.SetQuitOnFail(false);

            //Register our delegate (callback) with the connection manager
            //so we can receive connection events.  We will receive events
            //for connections opened on the given channel.
            m_CxnMgr.AddDelegate(&m_CxnDelegate, CHANNEL_ID);

            //We're going to send frames on a regular interval.  This is
            //the next time we'll send a frame.  By setting the next time
            //to now, we'll send a frame as soon as we're connected.
            m_NextSendTime = sysTimer::GetSystemMsTime();

            //Open a connection.  If the remote peer opens a connection with
            //on the same channel then the connection will be established.

            m_IsHost = isHost;

            const netAddress addr(LOOPBACK_ADDR, isHost ? (unsigned short)GUEST_PORT : HOST_PORT);

            m_CxnId = m_CxnMgr.OpenConnection(addr, CHANNEL_ID, NULL, 0, NULL);

            if(m_IsHost)
            {
                //Initialize the buffer that we're going to stream to the client.
                for(unsigned i = 0; i < sizeof(m_StreamBuf); ++i)
                {
                    m_StreamBuf[i] = (u8) i;
                }
            }

            m_IsInitialized = true;
        }
    }

    void Shutdown()
    {
        if(m_IsInitialized)
        {
            //Remove the delegate we registered in Init().
            m_CxnMgr.RemoveDelegate(&m_CxnDelegate);

            //Shut down the connection manager.
            m_CxnMgr.Shutdown();

            //Destroy our socket.
            m_NetHw->DestroySocket(&m_GameSocket);

            m_NetHw = 0;
            m_CxnId = -1;
            m_NextSendTime = 0;
            m_IsHost = false;
            m_StreamComplete = false;

            m_IsInitialized = false;
        }
    }

    //PURPOSE
    void Update()
    {
        Assert(m_IsInitialized);

        const unsigned curTime = sysTimer::GetSystemMsTime();

        //Calling Update() performs the operations of sending frames,
        //receiving frames, and dispatching events to the app.
        m_CxnMgr.Update(curTime);

        if(m_CxnId < 0)
        {
            //We're not yet connected

            m_NextSendTime = curTime;
        }
        //If our connection is open and it's time to send then send a frame.
        else if(m_CxnMgr.IsOpen(m_CxnId)
                 && int(curTime - m_NextSendTime) >= 0)
        {
            if(m_IsHost)
            {
                netMsgHello msg(123, "Hello!");

                Displayf("HOST: Sending \"%s\"", msg.GetMsgName());

                //Send a host frame every so many ms.
                m_NextSendTime = curTime + HOST_SEND_INTERVAL;

                m_CxnMgr.Send(m_CxnId, msg, NET_SEND_RELIABLE, NULL);
            }
            else
            {
                netMsgGoodbye msg(43.3f, "Good Bye!");

                Displayf("GUEST: Sending \"%s\"", msg.GetMsgName());

                //Send a guest frame every so many ms.
                m_NextSendTime = curTime + GUEST_SEND_INTERVAL;

                m_CxnMgr.Send(m_CxnId, msg, NET_SEND_RELIABLE, NULL);
            }
        }

        if(!m_StreamComplete && m_Stream.Complete())
        {
            m_StreamComplete = true;
            Displayf("%s complete stream", m_IsHost ? "Sent" : "Received");
        }
    }

private:

    //PURPOSE
    //  This function is bound to the delegate (callback) that we register
    //  with the connection manager.  The connection manager will pass all
    //  connection events to this function.
    void OnCxnEvent(netConnectionManager* cxnMgr,
                     const netEvent* evt)
    {
        Assert(&m_CxnMgr == cxnMgr);

        //Get the event id.
        const unsigned eid = evt->GetId();

        if(NET_EVENT_CONNECTION_ESTABLISHED == eid)
        {
            //A connection to a remote peer was established.
            Assert(m_CxnId == evt->m_CxnEstablished->m_CxnId);

            if(m_IsHost)
            {
                //Send a stream of data to the client.

                //Begin by calling PrepareToSend() to obtain a unique stream id.
                netMsgBeginStream msg;
                AssertVerify(m_Stream.PrepareToSend(&msg.m_StreamId));
                msg.m_StreamLen = sizeof(m_StreamBuf);

                //Send the stream id and stream length to the client.
                m_CxnMgr.Send(m_CxnId, msg, NET_SEND_RELIABLE, NULL);

                //Send the stream to the client.
                m_Stream.Send(&m_CxnMgr, m_CxnId, m_StreamBuf, sizeof(m_StreamBuf));
            }
        }
        else if(NET_EVENT_CONNECTION_CLOSED == eid)
        {
            //One of our connections was closed.
            if(-1 != m_CxnId)
            {
                m_CxnId = -1;
            }
        }
        else if(NET_EVENT_CONNECTION_ERROR == eid)
        {
            //An error occurred on the connection this could be due to
            //a timeout, a send error, or a termination command sent
            //from the remote peer.
            //In any case the connection is not closed by the connection manager,
            //so we close it here.
            cxnMgr->CloseConnection(m_CxnId, NET_CLOSE_IMMEDIATELY);
        }
        else if(NET_EVENT_FRAME_RECEIVED == eid)
        {
            //Received a frame of data.
            //A frame is the basic unit of data sent over the network.
            //Typically we would know what kind of data it
            //is by looking at the value of the channel id.
            //IOW, we segregate different types of data onto different
            //channels.
            //
            //A negative connection id implies the frame was sent
            //"out of band", i.e. ouside of any connection.  Out of band
            //frames are sent using netConnectionManager::SendOutOfBand()
            //or netConnectionManager::SubnetBroadcast().
            netEventFrameReceived* fr = evt->m_FrameReceived;

            //Because we're only sending net messages we assume it's
            //a net message and retrieve its message id.
            unsigned msgId;
            
            if(!netMessage::GetId(&msgId, fr->m_Payload, fr->m_SizeofPayload))
            {
                //Errorf("Invalid message");
            }
            //Import the data into a concrete message instance.
            else if(netMsgHello::MSG_ID() == msgId)
            {
                netMsgHello msg;
                AssertVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload));
                Displayf("%s: Received %s: %d, %s, \"%s\"",
                          m_IsHost ? "HOST" : "GUEST",
                          msg.GetMsgName(),
                          msg.m_Id,
                          msg.m_Type == netMsgHello::TYPE_ANIMAL ? "Animal"
                            : msg.m_Type == netMsgHello::TYPE_MINERAL ? "Mineral"
                            : msg.m_Type == netMsgHello::TYPE_VEGETABLE ? "Vegetable"
                            : "UNKNOWN",
                          msg.m_Text);
            }
            else if(netMsgGoodbye::MSG_ID() == msgId)
            {
                netMsgGoodbye msg;
                AssertVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload));
                Displayf("%s: Received %s: %f, \"%s\"",
                          m_IsHost ? "HOST" : "GUEST",
                          msg.GetMsgName(),
                          msg.m_Health,
                          msg.m_Text);
            }
            else if(netMsgBeginStream::MSG_ID() == msgId)
            {
                Assert(!m_IsHost);

                //The host is about to send us a stream of data.
                netMsgBeginStream msg;
                AssertVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload));

                //Receive the stream.
                AssertVerify(m_Stream.Receive(cxnMgr,
                                            evt->m_CxnId,
                                            msg.m_StreamId,
                                            m_StreamBuf,
                                            msg.m_StreamLen));
            }
        }
        else if(NET_EVENT_ACK_RECEIVED == eid)
        {
            //We received an ACK for one of the frames we sent.
            //If we had previously recorded the sequence number generated
            //when we sent a frame (by passing a pointer to a netSequence
            //instance to netConnectionManager::Send()) we could compare it
            //to the sequence number here and determine if our frame was
            //received by the remote peer.
            //
            //Note that an ACK for an unreliable frame means only that that
            //particular frame arrived at the remote peer.  Other frames sent
            //may or may not have arrived.
            //
            //Whereas every reliable frame is guaranteed to generate an
            //ACK the same cannot be said for unreliable frames.
        }
        else if(NET_EVENT_OUT_OF_MEMORY == eid)
        {
            //Not much we can do here...
            Quitf("Ran out of memory :(");
        }
    }

    //The connection manager.
    netConnectionManager m_CxnMgr;

    //The connection id we use to send/receive frames.
    int m_CxnId;

    //Instance of netHardware used to create new sockets.
    netHardware* m_NetHw;

    //The socket used for communicating game data.
    netSocket m_GameSocket;

    //The heap used for allocating connections, events, frames, etc.
    u8 m_Heap[64 * 1024];

    //The allocator used for allocating connections, events, frames, etc.
    sysMemSimpleAllocator m_Allocator;

    //We send on a regular interval - this is the next time we'll send.
    unsigned m_NextSendTime;

    //The delegate we register with the connection manager in order to
    //receive connection events.
    netConnectionManager::Delegate m_CxnDelegate;

    //Stream instance we'll use to demonstrate streaming large buffers.
    netStream m_Stream;

    //Buffer we'll stream from/into
    u8 m_StreamBuf[4096];

    //The IP port on which we'll communicate game data.
    unsigned short m_GamePort;

    //True if we're the host.
    bool m_IsHost   : 1;

    //True if we've completely sent/received the stream.
    bool m_StreamComplete   : 1;

    //True if one of the Init* functions was called.
    bool m_IsInitialized    : 1;
};

int Main()
{
    //Because we're using net messages we have to initialize the
    //net message system.
    netMessage::InitClass();

    //Create an instance of netHardware and start the network.
    netHardware hw;

    //Create our sample instances.
    SampleMsgPeer sampleHost;
    SampleMsgPeer sampleGuest;

    sampleHost.Init(&hw, true);
    sampleGuest.Init(&hw, false);

    bool done = false;

    while(!done)
    {
        hw.Update();
        sampleHost.Update();
        sampleGuest.Update();

        sysIpcSleep(1);
    }

    sampleHost.Shutdown();
    sampleGuest.Shutdown();

    netMessage::ShutdownClass();

    return 0;
}
