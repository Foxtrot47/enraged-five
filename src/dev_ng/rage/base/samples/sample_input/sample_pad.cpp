using namespace rage;

#include "system/main.h"

#include "grcore/setup.h"
#include "grcore/im.h"

#include "input/input.h"



void DumpPad(int pad,float x,float y) {
	const ioPad &P = ioPad::GetPad(pad);

	char buf[256];
	grcDraw2dText(x,y,formatf(buf,sizeof(buf),
		"%c %c BTN=%4x PR=%4x RE=%4x ",P.IsConnected()?'C':'D',P.HasRumble()? 'R' : ' ',
		P.GetButtons(), P.GetPressedButtons(), P.GetReleasedButtons()));

	y += 32;

	grcDraw2dText(x+P.GetLeftX(),y+P.GetLeftY(),"+");
	grcDraw2dText(x+P.GetRightX(),y+P.GetRightY(),"x");
	grcDraw2dText(x+P.GetAnalogButton(ioPad::L2_INDEX),y-16,"|");
	grcDraw2dText(x+P.GetAnalogButton(ioPad::R2_INDEX),y-16,"|");


	y += 32;
	if(P.HasSensors())
	{
		grcDraw2dText(x,y,formatf(buf,sizeof(buf),"Sensor X: %.3f",
			P.GetNormSensorAxis(ioPad::SENSOR_X)));
		y+=8;
		grcDraw2dText(x,y,formatf(buf,sizeof(buf),"Sensor Y: %.3f",
			P.GetNormSensorAxis(ioPad::SENSOR_Y)));
		y+=8;
		grcDraw2dText(x,y,formatf(buf,sizeof(buf),"Sensor Z: %.3f",
			P.GetNormSensorAxis(ioPad::SENSOR_Z)));
		y+=8;
		grcDraw2dText(x,y,formatf(buf,sizeof(buf),"Sensor G: %.3f",
			P.GetNormSensorAxis(ioPad::SENSOR_G)));
		y+=8;
	}
	else
	{
		grcDraw2dText(x,y,"No sensors!");
	}

	y += 32;

#define D(xx) if (P.GetButtons() & ioPad::xx) { grcDraw2dText(x,y,#xx " "); x += 8*strlen(#xx)+16; }

	D(LLEFT); D(LRIGHT); D(LUP); D(LDOWN);
	D(RLEFT); D(RRIGHT); D(RUP); D(RDOWN);
	D(L1); D(L2); D(R1); D(R2);
	D(L3); D(R3);
	D(SELECT); D(START);


	/* if (P.HasSensors())
		Displayf("  SENSORS X=%f Y=%f Z=%f G=%f",P.GetNormSensorAxis(ioPad::SENSOR_X),
		P.GetNormSensorAxis(ioPad::SENSOR_Y),P.GetNormSensorAxis(ioPad::SENSOR_Z),P.GetNormSensorAxis(ioPad::SENSOR_G)); */
}

int Main() {
	grcSetup *setup = rage_new grcSetup;
	setup->Init("","Sample Pad");
	setup->BeginGfx(true);
	setup->CreateDefaultFactories();

	rage::INPUT.Begin(true);

	Displayf("Use L2 to control left motor");
	Displayf("Use R2 to control right motor");
	Displayf("X to enable actuators (on by default)");
	Displayf("Square to disable actuators");

	do {
		setup->BeginUpdate();

			for (int i=0;i<ioPad::MAX_PADS;i++)
			{
				// 0 is the small motor on the right-hand side (ps2) (on/off only)
				// 1 is the big motor on the left-hand side (ps2) (variable speed, supposedly)
				ioPad *pP = (ioPad *)&ioPad::GetPad(i);
				pP->SetActuatorValue(0, pP->GetNormAnalogButton(ioPad::L2_INDEX));
				pP->SetActuatorValue(1, pP->GetNormAnalogButton(ioPad::R2_INDEX));
				if (pP->GetButtons() & ioPad::RDOWN)
					pP->SetActuatorsActivation(true);
				else if (pP->GetButtons() & ioPad::RLEFT)
					pP->SetActuatorsActivation(false);
			}

			rage::INPUT.Update();

		setup->EndUpdate();

		setup->BeginDraw();

			for (int i=0;i<ioPad::MAX_PADS;i++)
				if (ioPad::GetPad(i).IsConnected())
					DumpPad(i,100.0f+(i%2)*300,100.0f+(i/2)*300);

		setup->EndDraw();
	} while (!setup->WantExit());

	rage::INPUT.End();

	setup->DestroyFactories();
	setup->EndGfx();

	delete setup;

	return 0;
}
