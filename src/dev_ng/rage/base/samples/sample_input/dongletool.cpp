// 
// /dongletool.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//


#include <system/main.h>
#include <system/param.h>
#include <system/bootmgr.h>
#include <grcore/setup.h>
#include <grcore/device.h>
#include <grcore/font.h>
#include <input/dongle.h>
#include <input/input.h>
#include <bank/bkmgr.h>

//#if __XENON
//#include <xtitleidbegin.h>
//XEX_TITLE_ID( 0x545407DF )
//#include <xtitleidend.h>
//#endif

using namespace rage;


#define LOCAL_ASSET_PATH "T:/rage/assets/"
#define CONSOLE_ASSET_PATH "T:/rage/assets/"

///////////////////////////////////////////////////////////////////////////////////////////////////

PARAM(encodestring, "dongle string to encode");
//PARAM(mcfilename, "dongle file name");
PARAM(macaddrstring, " mac address to encode ");
PARAM(testdongle, " command argument ");

///////////////////////////////////////////////////////////////////////////////////////////////////

enum EnumCurrentOperation
{
	OP_IDLE,
	OP_READ,
	OP_WRITE,
	OP_WRITING,
	OP_QUIT
};

static EnumCurrentOperation sCurrentOperation=OP_WRITE;
static const char* sEncodeString=NULL;
static const char* sMacAddrString=NULL;

#if ENABLE_DONGLE
static const char* sMCFileName="root.sys";
#endif

static char sMessage[64];

#if ENABLE_DONGLE
static char errorString[500];
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////

void DisplayFunc(const char *message)
{
	Displayf(message);
	safecpy(sMessage, message, sizeof(sMessage));
}

///////////////////////////////////////////////////////////////////////////////////////////////////

#if __BANK
static void sWriteCodeFile()
{
	if( sCurrentOperation==OP_IDLE )
		sCurrentOperation = OP_WRITE;
}

static void sReadCodeFile()
{
	if( sCurrentOperation==OP_IDLE )
		sCurrentOperation = OP_READ;
}

static void sQuit()
{
	sCurrentOperation = OP_QUIT;
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////

void Update()
{
	switch(sCurrentOperation)
	{
	case OP_READ:
		{
#if ENABLE_DONGLE
			if (!ioDongle::ReadCodeFile(sEncodeString, sMCFileName, errorString))
			{
				Displayf(errorString);
			}
#endif
			sCurrentOperation = OP_IDLE;

		}
		break;

	case OP_WRITE:
		{
#if ENABLE_DONGLE
			fiDevice::SystemTime whoCares = {0};
			ioDongle::WriteCodeFile(sEncodeString, sMacAddrString, whoCares, false, sMCFileName);
#endif
			sCurrentOperation = OP_WRITING;
		}
		break;

	case OP_WRITING:
		{
#if ENABLE_DONGLE && !__PS3
			if( ioDongle::CheckWriteCodeFile() )
#endif
				sCurrentOperation = OP_QUIT;
		}
		break;

	default:
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void Render()
{
	grcFont::GetCurrent().Draw(60, 300, sMessage);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

int Main()
{
	//if( !PARAM_encodestring.Get(sEncodeString) || !PARAM_macaddrstring.Get(sMacAddrString) || !PARAM_mcfilename.Get(sMCFileName) )
	//	Quitf("usage: -encodestring <encode-string> -macaddrstring <macaddress-string> -mcfilename <memory-card-file-name>");	
	if( !PARAM_encodestring.Get(sEncodeString) || !PARAM_macaddrstring.Get(sMacAddrString) || !PARAM_testdongle.Get( ) )
		Quitf("usage: -encodestring <encode-string> -macaddrstring <macaddress-string> -testdongle");	

	char * pChar = (char*)sMacAddrString;
	while( *pChar )
	{
		*pChar = (char) toupper(*pChar);
		if (*pChar == '-')
			*pChar = ':';
		pChar++;
	}

	const bool inWindow = true;
	grcSetup setup;
	setup.Init(sysBootManager::IsBootedFromDisc() ? CONSOLE_ASSET_PATH : LOCAL_ASSET_PATH, "dongle");
	setup.BeginGfx(inWindow);
	setup.CreateDefaultFactories();
	setup.SetClearColor(Color32(0,0,0,255));
	INPUT.Begin(inWindow);

#if __DEBUGBUTTONS
	ioPad::GetPad(0).SetDebugButton(ioPad::SELECT);
#endif

#if ENABLE_DONGLE
	DisplayFunc("Use widgets to access dongle options");
	ioDongle::SetDisplayMessageFunc(DisplayFunc);
#else
	DisplayFunc("This platform does not have dongle support enabled!");
#endif

#if __BANK
	bkBank& bank=BANKMGR.CreateBank("dongle");
	bank.AddButton("write copy protection file", datCallback(CFA(sWriteCodeFile)));
	bank.AddButton("read copy protection file (can lock up console)", datCallback(CFA(sReadCodeFile)));
	bank.AddButton("quit", datCallback(CFA(sQuit)));
#endif

	do{
		setup.BeginUpdate();
		INPUT.Update();
		Update();
		setup.EndUpdate();

		setup.BeginDraw(true);
		Render();
		setup.EndDraw();

		sysIpcSleep(15);
	}while( !setup.WantExit() && sCurrentOperation!=OP_QUIT );

	setup.DestroyFactories();
	setup.EndGfx();
	setup.Shutdown();
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
 

