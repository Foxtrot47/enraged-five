using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;

namespace sample_ragui_xml
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem openMenuItem;
		private System.Windows.Forms.MenuItem saveMenuItem;
		private System.Windows.Forms.MenuItem exitMenuItem;
		private ragUi.ControlText controlText1;
		private ragUi.ControlToggle controlToggle1;
		private System.Windows.Forms.GroupBox groupBox1;
		private ragUi.ControlPanel controlPanel1;
		private ragUi.ControlFile controlFile1;
		private ragUi.ControlFile controlFile2;
		private ragUi.ControlColor controlColor1;
		private ragUi.ControlSlider controlSlider5;
		private ragUi.ControlSlider controlSlider6;
		private ragUi.ControlColor controlColor2;
		private ragUi.ControlSlider ceramicSlider;
		private ragUi.ControlSlider steelSlider;
		private ragUi.ControlSlider kevlarSlider;
		private ragUi.ControlSlider shieldSlider;
		private System.Windows.Forms.Label avgLabel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.openMenuItem = new System.Windows.Forms.MenuItem();
			this.saveMenuItem = new System.Windows.Forms.MenuItem();
			this.exitMenuItem = new System.Windows.Forms.MenuItem();
			this.controlText1 = new ragUi.ControlText();
			this.controlToggle1 = new ragUi.ControlToggle();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.controlPanel1 = new ragUi.ControlPanel();
			this.ceramicSlider = new ragUi.ControlSlider();
			this.steelSlider = new ragUi.ControlSlider();
			this.kevlarSlider = new ragUi.ControlSlider();
			this.shieldSlider = new ragUi.ControlSlider();
			this.controlFile1 = new ragUi.ControlFile();
			this.controlFile2 = new ragUi.ControlFile();
			this.controlColor1 = new ragUi.ControlColor();
			this.controlSlider5 = new ragUi.ControlSlider();
			this.controlSlider6 = new ragUi.ControlSlider();
			this.controlColor2 = new ragUi.ControlColor();
			this.avgLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.controlPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.openMenuItem,
																					  this.saveMenuItem,
																					  this.exitMenuItem});
			this.menuItem1.Text = "File";
			// 
			// openMenuItem
			// 
			this.openMenuItem.Index = 0;
			this.openMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.openMenuItem.Text = "Open...";
			this.openMenuItem.Click += new System.EventHandler(this.openMenuItem_Click);
			// 
			// saveMenuItem
			// 
			this.saveMenuItem.Index = 1;
			this.saveMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
			this.saveMenuItem.Text = "Save...";
			this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
			// 
			// exitMenuItem
			// 
			this.exitMenuItem.Index = 2;
			this.exitMenuItem.Shortcut = System.Windows.Forms.Shortcut.AltF4;
			this.exitMenuItem.Text = "Exit";
			this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
			// 
			// controlText1
			// 
			this.controlText1.Location = new System.Drawing.Point(8, 8);
			this.controlText1.Name = "controlText1";
			this.controlText1.Size = new System.Drawing.Size(328, 24);
			this.controlText1.TabIndex = 0;
			this.controlText1.Text = "Name";
			this.controlText1.Value = "";
			this.controlText1.ValueXPath = "Name";
			// 
			// controlToggle1
			// 
			this.controlToggle1.Location = new System.Drawing.Point(8, 48);
			this.controlToggle1.Name = "controlToggle1";
			this.controlToggle1.Size = new System.Drawing.Size(96, 24);
			this.controlToggle1.TabIndex = 3;
			this.controlToggle1.Text = "Two Handed";
			this.controlToggle1.Value = false;
			this.controlToggle1.ValueXPath = "IsTwoHanded";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.controlPanel1);
			this.groupBox1.Location = new System.Drawing.Point(16, 112);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(328, 120);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Effectiveness Multiplier";
			// 
			// controlPanel1
			// 
			this.controlPanel1.AutoScroll = true;
			this.controlPanel1.Controls.Add(this.ceramicSlider);
			this.controlPanel1.Controls.Add(this.steelSlider);
			this.controlPanel1.Controls.Add(this.kevlarSlider);
			this.controlPanel1.Controls.Add(this.shieldSlider);
			this.controlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.controlPanel1.Location = new System.Drawing.Point(3, 16);
			this.controlPanel1.Name = "controlPanel1";
			this.controlPanel1.Size = new System.Drawing.Size(322, 101);
			this.controlPanel1.TabIndex = 0;
			// 
			// ceramicSlider
			// 
			this.ceramicSlider.IsFloat = true;
			this.ceramicSlider.Location = new System.Drawing.Point(0, 72);
			this.ceramicSlider.Maximum = 10M;
			this.ceramicSlider.Minimum = 0M;
			this.ceramicSlider.Name = "ceramicSlider";
			this.ceramicSlider.Size = new System.Drawing.Size(322, 24);
			this.ceramicSlider.Step = 0.1M;
			this.ceramicSlider.TabIndex = 3;
			this.ceramicSlider.Text = "Vs. Ceramic";
			this.ceramicSlider.Value = new System.Decimal(new int[] {
																		0,
																		0,
																		0,
																		0});
			this.ceramicSlider.ValueXPath = "WepDmg/Effectiveness/Ceramic";
			this.ceramicSlider.ValueChanged += new System.EventHandler(this.UpdateAvgEffectivness);
			// 
			// steelSlider
			// 
			this.steelSlider.IsFloat = true;
			this.steelSlider.Location = new System.Drawing.Point(0, 48);
			this.steelSlider.Maximum = 10M;
			this.steelSlider.Minimum = 0M;
			this.steelSlider.Name = "steelSlider";
			this.steelSlider.Size = new System.Drawing.Size(322, 24);
			this.steelSlider.Step = 0.1M;
			this.steelSlider.TabIndex = 2;
			this.steelSlider.Text = "Vs. Steel";
			this.steelSlider.Value = new System.Decimal(new int[] {
																	  0,
																	  0,
																	  0,
																	  0});
			this.steelSlider.ValueXPath = "WepDmg/Effectiveness/Steel";
			this.steelSlider.ValueChanged += new System.EventHandler(this.UpdateAvgEffectivness);
			// 
			// kevlarSlider
			// 
			this.kevlarSlider.IsFloat = true;
			this.kevlarSlider.Location = new System.Drawing.Point(0, 24);
			this.kevlarSlider.Maximum = 10M;
			this.kevlarSlider.Minimum = 0M;
			this.kevlarSlider.Name = "kevlarSlider";
			this.kevlarSlider.Size = new System.Drawing.Size(322, 24);
			this.kevlarSlider.Step = 0.1M;
			this.kevlarSlider.TabIndex = 1;
			this.kevlarSlider.Text = "Vs. Kevlar";
			this.kevlarSlider.Value = new System.Decimal(new int[] {
																	   0,
																	   0,
																	   0,
																	   0});
			this.kevlarSlider.ValueXPath = "WepDmg/Effectiveness/Kevlar";
			this.kevlarSlider.ValueChanged += new System.EventHandler(this.UpdateAvgEffectivness);
			// 
			// shieldSlider
			// 
			this.shieldSlider.IsFloat = true;
			this.shieldSlider.Location = new System.Drawing.Point(0, 0);
			this.shieldSlider.Maximum = 10M;
			this.shieldSlider.Minimum = 0M;
			this.shieldSlider.Name = "shieldSlider";
			this.shieldSlider.Size = new System.Drawing.Size(322, 24);
			this.shieldSlider.Step = 0.1M;
			this.shieldSlider.TabIndex = 0;
			this.shieldSlider.Text = "Vs. Energy Shield";
			this.shieldSlider.Value = new System.Decimal(new int[] {
																	   0,
																	   0,
																	   0,
																	   0});
			this.shieldSlider.ValueXPath = "WepDmg/Effectiveness/EnergyShield";
			this.shieldSlider.ValueChanged += new System.EventHandler(this.UpdateAvgEffectivness);
			// 
			// controlFile1
			// 
			this.controlFile1.Location = new System.Drawing.Point(384, 0);
			this.controlFile1.Name = "controlFile1";
			this.controlFile1.Size = new System.Drawing.Size(320, 40);
			this.controlFile1.TabIndex = 7;
			this.controlFile1.Text = "Mesh";
			this.controlFile1.Value = null;
			this.controlFile1.ValueXPath = "Mesh";
			// 
			// controlFile2
			// 
			this.controlFile2.Location = new System.Drawing.Point(384, 48);
			this.controlFile2.Name = "controlFile2";
			this.controlFile2.Size = new System.Drawing.Size(320, 40);
			this.controlFile2.TabIndex = 8;
			this.controlFile2.Text = "Fire Particles";
			this.controlFile2.Value = null;
			this.controlFile2.ValueXPath = "Gfx/FireParticles";
			// 
			// controlColor1
			// 
			this.controlColor1.A = ((System.Byte)(255));
			this.controlColor1.Alpha = true;
			this.controlColor1.B = ((System.Byte)(0));
			this.controlColor1.Extended = true;
			this.controlColor1.G = ((System.Byte)(0));
			this.controlColor1.Location = new System.Drawing.Point(360, 96);
			this.controlColor1.Name = "controlColor1";
			this.controlColor1.R = ((System.Byte)(0));
			this.controlColor1.Size = new System.Drawing.Size(352, 96);
			this.controlColor1.TabIndex = 9;
			this.controlColor1.Text = "HUD Color";
			this.controlColor1.ValueXPath = "Hud/BaseColor";
			// 
			// controlSlider5
			// 
			this.controlSlider5.IsFloat = false;
			this.controlSlider5.Location = new System.Drawing.Point(16, 80);
			this.controlSlider5.Maximum = 1000M;
			this.controlSlider5.Minimum = 0M;
			this.controlSlider5.Name = "controlSlider5";
			this.controlSlider5.Size = new System.Drawing.Size(304, 24);
			this.controlSlider5.Step = 1M;
			this.controlSlider5.TabIndex = 10;
			this.controlSlider5.Text = "Base Damage";
			this.controlSlider5.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider5.ValueXPath = "WepDmg/Base";
			// 
			// controlSlider6
			// 
			this.controlSlider6.IsFloat = true;
			this.controlSlider6.Location = new System.Drawing.Point(16, 272);
			this.controlSlider6.Maximum = 1M;
			this.controlSlider6.Minimum = 0M;
			this.controlSlider6.Name = "controlSlider6";
			this.controlSlider6.Size = new System.Drawing.Size(280, 24);
			this.controlSlider6.Step = 0.01M;
			this.controlSlider6.TabIndex = 11;
			this.controlSlider6.Text = "Accuracy";
			this.controlSlider6.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider6.ValueXPath = "Accuracy";
			// 
			// controlColor2
			// 
			this.controlColor2.A = ((System.Byte)(255));
			this.controlColor2.Alpha = false;
			this.controlColor2.B = ((System.Byte)(0));
			this.controlColor2.Extended = true;
			this.controlColor2.G = ((System.Byte)(0));
			this.controlColor2.Location = new System.Drawing.Point(360, 192);
			this.controlColor2.Name = "controlColor2";
			this.controlColor2.R = ((System.Byte)(0));
			this.controlColor2.Size = new System.Drawing.Size(352, 96);
			this.controlColor2.TabIndex = 12;
			this.controlColor2.Text = "Tracer Color";
			this.controlColor2.ValueXPath = "Gfx/TracerColor";
			this.controlColor2.Load += new System.EventHandler(this.controlColor2_Load);
			// 
			// avgLabel
			// 
			this.avgLabel.Location = new System.Drawing.Point(16, 240);
			this.avgLabel.Name = "avgLabel";
			this.avgLabel.Size = new System.Drawing.Size(312, 23);
			this.avgLabel.TabIndex = 13;
			this.avgLabel.Text = "label1";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(728, 313);
			this.Controls.Add(this.avgLabel);
			this.Controls.Add(this.controlColor2);
			this.Controls.Add(this.controlSlider6);
			this.Controls.Add(this.controlSlider5);
			this.Controls.Add(this.controlColor1);
			this.Controls.Add(this.controlFile2);
			this.Controls.Add(this.controlFile1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.controlToggle1);
			this.Controls.Add(this.controlText1);
			this.Menu = this.mainMenu1;
			this.Name = "MainForm";
			this.Text = "RagUI XML sample";
			this.groupBox1.ResumeLayout(false);
			this.controlPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private XmlDocument m_Document;

		private void openMenuItem_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "XML files (*.xml)|*.xml";
			if (dlg.ShowDialog() == DialogResult.OK) {
				m_Document = new System.Xml.XmlDocument();
				System.IO.Stream str = dlg.OpenFile();
				m_Document.Load(str);
				str.Close();
				SetupWidgets();
			}
		}

		private void SetupWidgets() {
			foreach(Control c in Controls) {
				SetupWidgets(c);
			}
		}
		
		private void SetupWidgets(Control root) {
			ragUi.ControlBase ragCtl = root as ragUi.ControlBase;
			if (ragCtl != null) 
			{
				ragCtl.ConnectControlToXmlNode(m_Document.DocumentElement);
			}
			else 
			{
				foreach(Control c in root.Controls) 
				{
					// recursively setup container widgets
					SetupWidgets(c);
				}
			}
		}

		private void saveMenuItem_Click(object sender, System.EventArgs e)
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.DefaultExt = "xml";
			if (dlg.ShowDialog() == DialogResult.OK) 
			{
				m_Document.Save(dlg.FileName);
			}
		}

		private void exitMenuItem_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void controlColor2_Load(object sender, System.EventArgs e)
		{
		
		}

		private void UpdateAvgEffectivness(object sender, System.EventArgs e)
		{
			decimal avg = 1.0M;
			avg += shieldSlider.Value;
			avg += kevlarSlider.Value;
			avg += steelSlider.Value;
			avg += ceramicSlider.Value;
			avg = avg / 5.0M;
			avgLabel.Text = String.Format("Avg. Effectivness: {0}", avg);
		}
	}
}
