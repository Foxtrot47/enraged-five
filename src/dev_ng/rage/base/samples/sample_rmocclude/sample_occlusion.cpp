// 
// sample_rmocclude/sample_occlusion.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: sample_occlusion
//
// PURPOSE:
//		This sample shows software occlusion of a random arrangement of cubes.
//		The loaded .occlude file consists of a single geometry occluder in the shape of a cube placed at the origin.
//		Smaller wireframe cubes randomly placed in space are occlusion tested against the occluder and rendered as solid boxes when they would normally
//		become occluded to make it more apparent than simply not rendering them.

#include "sample_rmcore/sample_rmcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "math/random.h"

#include "system/param.h"
#include "system/timemgr.h"
//#include "rmdraw/drawlist.h" // outside the scope of this sample, another tester will be made later that will combine the use of a drawlist if needed.
#include "rmocclude/occluder.h"


#include "system/main.h" 

using namespace rage;

PARAM(file,"[sample_occlusion] filename for the sample file");

// TODO: combine sample with rmdraw??? or save that for a more advanced sample?

class SampleSphere
{
public:
	SampleSphere()
	{
		m_dt = 0.f;
		m_boxmtx = M34_IDENTITY;

		float	scale = (g_DrawRand.GetFloat() +1.f)* 2.f;

		m_boxsize.Set( .5f, .5f, .5f );
		m_boxcolor.Setf( 1.f, 1.f, 1.f, 1.f );// color of box we're hiding.
		m_boxmin.Set( -.25f, -.25f, -.25f, 1.f );
		m_boxmax.Set(  .25f,  .25f,  .25f, 1.f );
		m_radius = .25f;

		m_boxsize	*= scale;
		m_boxmin	*=scale;
		m_boxmax	*=scale;
		m_radius	*=scale;

		m_center.Set(0.f,0.f,0.f);

		SetName("sph");

		m_visibilityBits = 0U;
	}

	void	Randomize(void )
	{
		m_boxmtx.Translate(	(g_DrawRand.GetFloat() * 30.f)-15.f,
			g_DrawRand.GetFloat() *  5.f,
			(g_DrawRand.GetFloat() * 30.f)-15.f
			);
	}

	void SetVisibility( int viewIndex, bool IsVisible )
	{
		Assert( (viewIndex >= 0)&& (viewIndex <=31) );
		if( IsVisible )
		{
			// set
			m_visibilityBits |= 1 << viewIndex;
		}else{
			// clear
			m_visibilityBits &= ~(1<<(viewIndex & ~0U)); 
		}
	}

	bool	GetVisibility( int viewIndex )
	{
		Assert( (viewIndex >= 0)&& (viewIndex <=31) );

		if( m_visibilityBits & (1 << viewIndex) )
			return true;

		return false;
	}

	float	GetRadius(void){ return m_radius;}

	u32		GetVisBits(void)
	{
		return m_visibilityBits;
	}


	void Reset(void)
	{
		m_visibilityBits = 0;
	}

	void Draw( int viewIndex, bool drawname=true, bool forcedraw=false )
	{
		if( forcedraw || GetVisibility( viewIndex ) )
		{			
			grcState::SetAlphaBlend(false);
			grcState::SetBlendSet(grcbsNormal);
			grcState::SetDepthWrite(true);
			grcColor(m_boxcolor);
			grcDrawSphere( m_radius, m_boxmtx, 9, true , false );// wire frame
		}
		else
		{
			if( GetOverdrawDebugMode() )
			{
				// alpha blended, occluded or 'not visible' 
				grcState::SetAlphaBlend(true);
				grcState::SetBlendSet(grcbsAdd);
				grcState::SetDepthWrite(false);
				grcColor(GetOverdrawColor());
				grcDrawSphere( m_radius, m_boxmtx, 9, true , true );//solid
			}else{
				grcState::SetAlphaBlend(false);
				grcState::SetBlendSet(grcbsNormal);
				grcState::SetDepthWrite(false);
				grcColor(m_boxcolor);
				grcDrawSphere( m_radius, m_boxmtx, 9, true , true );//solid
			}						
		}

		if( drawname )
		{
			//Vector3 cullSphere(0.f, 0.f, 0.f);
			float x,y;

			if(grcProject( (Vector3&)m_boxmtx.d, x, y)//, pviewer->Get_view(), *pviewer->GetPerspectiveViewport()
				== grcProjectVisible)
			{
				grcState::SetAlphaBlend(true);
				grcState::SetBlendSet(grcbsAdd);
				grcState::SetDepthWrite(true);
				Color32 font_color;

				if( forcedraw || GetVisibility( viewIndex ) )
				{
					font_color.Setf( .4f, .7f, .4f);//green
					//font_color.Setf( .3f, .7f, .7f);//cyan
				}else{
					font_color.Setf( .3f, .2f, .2f);
				}

				grcFont::GetCurrent().DrawScaled(x+25.f, y-25.f, 1.0f, font_color , 1.f, 1.f, m_strName );
			}
		}
	}

	void	SetName(const char* name )
	{
		formatf(m_strName, sizeof(m_strName), "%s", name );
	}

	void	SetStringByNum(int num )
	{
		formatf(m_strName, sizeof(m_strName), "sphere_%d", num );
	}


	void Update( float dt )
	{
		m_dt += dt;
	}

	float		m_dt;


	Vector4*	GetMin(){return &m_boxmin; }
	Vector4*	GetMax(){return &m_boxmax; }
	Matrix34*	GetMatrix(){return &m_boxmtx; }

	static  bool	GetOverdrawDebugMode(void){return sm_overdrawDebugMode;}
	static	void	SetOverdrawDebugMode( bool enable )
	{
		sm_overdrawDebugMode = enable;
	}
	static	Color32	GetOverdrawColor(void){ return sm_overdrawColor;}
	static	void	SetOverdrawColor( Color32 overdrawcolor )
	{
		sm_overdrawColor = overdrawcolor;
	}


protected:
	Matrix34	m_boxmtx;

	Vector4		m_boxmin;
	Vector4		m_boxmax;

	Vector3		m_boxsize;
	Color32		m_boxcolor;
	float		m_radius;
	Vector3		m_center;

private:
	u32			m_visibilityBits; // bitfield to track viewport visibility (32 viewport max duh)

	char		m_strName[64];

	static		bool	sm_overdrawDebugMode;
	static		Color32 sm_overdrawColor;
};

class SampleBox /* : public rmDrawListItem */
{
public:
	SampleBox()
	{
		m_dt = 0.f;
		m_boxmtx = M34_IDENTITY;
		m_boxsize.Set( .5f, .5f, .5f );
		m_boxcolor.Setf( 1.f, 1.f, 1.f, 1.f );// color of box we're hiding.
		m_boxmin.Set( -.25f, -.25f, -.25f, 1.f );
		m_boxmax.Set(  .25f,  .25f,  .25f, 1.f );

		SetName("box");

		m_visibilityBits = 0U;
	}

	void	Randomize(void )
	{
		m_boxmtx.Translate(	(g_DrawRand.GetFloat() * 30.f)-15.f,
			g_DrawRand.GetFloat() *  5.f,
			(g_DrawRand.GetFloat() * 30.f)-15.f
			);
	}

	void SetVisibility( int viewIndex, bool IsVisible )
	{
		Assert( (viewIndex >= 0)&& (viewIndex <=31) );
		if( IsVisible )
		{
			// set
			m_visibilityBits |= 1 << viewIndex;
		}else{
			// clear
			m_visibilityBits &= ~(1<<(viewIndex & ~0U)); 
		}
	}

	bool	GetVisibility( int viewIndex )
	{
		Assert( (viewIndex >= 0)&& (viewIndex <=31) );

		if( m_visibilityBits & (1 << viewIndex) )
			return true;

		return false;
	}

	u32		GetVisBits(void)
	{
		return m_visibilityBits;
	}


	void Reset(void)
	{
		m_visibilityBits = 0;
	}

	void Draw( int viewIndex, bool drawname=true, bool forcedraw=false )
	{
		if( forcedraw || GetVisibility( viewIndex ) )
		{
			if( 0)// GetOverdrawDebugMode() )
			{
				grcState::SetAlphaBlend(true);
				grcState::SetBlendSet(grcbsAdd);
				grcState::SetDepthWrite(false);
				grcDrawBox( m_boxsize, m_boxmtx, GetOverdrawColor() );

			}
			else
			{
				grcState::SetAlphaBlend(false);
				grcState::SetBlendSet(grcbsNormal);
				grcState::SetDepthWrite(true);
				grcDrawBox( m_boxsize, m_boxmtx, m_boxcolor );
			}
		}
		else
		{
			grcState::SetAlphaBlend(true);
			grcState::SetBlendSet(grcbsAdd);
			grcState::SetDepthWrite(false);
			grcDrawSolidBox( m_boxsize, m_boxmtx, GetOverdrawColor() );

			/*
			grcState::SetAlphaBlend(false);
			grcState::SetBlendSet(grcbsNormal);
			grcDrawSolidBox( m_boxsize, m_boxmtx, m_boxcolor );
			*/
		}

		if( drawname )
		{
			//Vector3 cullSphere(0.f, 0.f, 0.f);
			float x,y;

			if(grcProject( (Vector3&)m_boxmtx.d, x, y)//, pviewer->Get_view(), *pviewer->GetPerspectiveViewport()
				== grcProjectVisible)
			{
				grcState::SetAlphaBlend(true);
				grcState::SetBlendSet(grcbsAdd);
				grcState::SetDepthWrite(true);
				Color32 font_color;

				if( forcedraw || GetVisibility( viewIndex ) )
				{
					font_color.Setf( .4f, .7f, .4f);//green
				}else{
					font_color.Setf( .3f, .2f, .2f);
				}

				grcFont::GetCurrent().DrawScaled(x+25.f, y-25.f, 1.0f, font_color , 1.f, 1.f, m_strName );
			}
		}
	}


	void	SetName(const char* name )
	{
		formatf(m_strName, sizeof(m_strName), "%s", name );
	}

	void	SetStringByNum(int num )
	{
		formatf(m_strName, sizeof(m_strName), "box_%d", num );
	}

	void Update( float dt )
	{
		m_dt += dt;
	}

	float		m_dt;


	Vector4*	GetMin(){return &m_boxmin; }
	Vector4*	GetMax(){return &m_boxmax; }
	Matrix34*	GetMatrix(){return &m_boxmtx; }

	static  bool	GetOverdrawDebugMode(void){return sm_overdrawDebugMode;}
	static	void	SetOverdrawDebugMode( bool enable )
	{
		sm_overdrawDebugMode = enable;
	}
	static	Color32	GetOverdrawColor(void){ return sm_overdrawColor;}
	static	void	SetOverdrawColor( Color32 overdrawcolor )
	{
		sm_overdrawColor = overdrawcolor;
	}

protected:
	Matrix34	m_boxmtx;

	Vector4		m_boxmin;
	Vector4		m_boxmax;

	Vector3		m_boxsize;
	Color32		m_boxcolor;

private:
	u32			m_visibilityBits; // bitfield to track viewport visibility (32 viewport max duh)

	char		m_strName[64];

	static		bool	sm_overdrawDebugMode;
	static		Color32	sm_overdrawColor;
};

class DynamicBox : public SampleBox
{
public:
	DynamicBox()
	{
		m_dt = 0.f;

		float size_x = g_DrawRand.GetFloat() + .1f;
		float size_y = g_DrawRand.GetFloat() + .1f;
		float size_z = g_DrawRand.GetFloat() + .1f;

		float half_x = size_x * 0.5f;
		float half_y = size_y * 0.5f;
		float half_z = size_z * 0.5f;

		m_boxcolor.Setf( 1.f, 1.f, 1.f, 1.f );// color of box we're hiding.

		m_boxmtx = M34_IDENTITY;

		m_boxsize.Set( size_x, size_y, size_z );		
		m_boxmin.Set( -half_x, -half_y, -half_z, 1.f );
		m_boxmax.Set(  half_x,  half_y,  half_z, 1.f );

		m_dx = g_DrawRand.GetFloat();
		m_dy = g_DrawRand.GetFloat();
		m_dz = g_DrawRand.GetFloat();

		m_rx = g_DrawRand.GetFloat();
		m_ry = g_DrawRand.GetFloat();
		m_rz = g_DrawRand.GetFloat();
	}

	void Update( float dt )
	{
		m_rx += (m_dx * dt);
		m_ry += (m_dy * dt);
		m_rz += (m_dz * dt);


		Vector3	tmp_v3( m_rx, m_ry, m_rz );

		m_boxmtx.FromEulersXYZ( tmp_v3 );

		m_dt += dt;
	}

	void	Translate( float x, float y, float z )
	{
		m_boxmtx.d.x = x;
		m_boxmtx.d.y = y;
		m_boxmtx.d.z = z;
	}

	void Randomize( void )
	{
		m_dx = g_DrawRand.GetFloat();
		m_dy = g_DrawRand.GetFloat();
		m_dz = g_DrawRand.GetFloat();

		m_rx = g_DrawRand.GetFloat();
		m_ry = g_DrawRand.GetFloat();
		m_rz = g_DrawRand.GetFloat();

		Vector3	tmp_v3( m_rx, m_ry, m_rz );

		m_boxmtx.FromEulersXYZ( tmp_v3 );

		m_boxmtx.Translate(	(g_DrawRand.GetFloat() * 30.f)-15.f,
			g_DrawRand.GetFloat() *  5.f,
			(g_DrawRand.GetFloat() * 30.f)-15.f
			);
	}

private:
	float	m_dx;
	float	m_dy;
	float	m_dz;

	float	m_rx;
	float	m_ry;
	float	m_rz;

	float	m_dt;
};

Color32 SampleSphere::sm_overdrawColor	(.1f, .1f, .1f, 1.f );
bool	SampleSphere::sm_overdrawDebugMode = true;

Color32 SampleBox::sm_overdrawColor		(.1f, .1f, .1f, 1.f );
bool	SampleBox::sm_overdrawDebugMode = true;

class grcSampleOcclusion : public ragesamples::rmcSampleManager
{
public: 
	grcSampleOcclusion() : ragesamples::rmcSampleManager()
	{
		m_bUseAnimatedGeomOccluder = true;
	}

	void Init(const char* path=NULL)
	{
		rmcSampleManager::Init(path);

		const char* name=NULL;
		if (GetNumFileNames()>0)
		{
			name=GetFileName(0);
		}

		m_OccludeSystem = rage_new rmOccluderSystem;// Create+Initialize static data in rmOccluder system class.
		m_testPvsViewID = m_OccludeSystem->CreateViewer();// should not be done every frame! (do this during setup)
		m_OccludeSystem->LoadDataset( name, "" );// Load a static occlusion data set that is managed by rmOccluderSystem.

		// Dynamic Occluder Sample --------------------------------------------------
		m_pDynamicOccluder = NULL;
		m_numOccluderGeoms = 0;
		m_pOccluderGeomArray = NULL;
		m_numOccluderPolys = 0;
		m_pOccluderPolyArray = NULL;

		bool load_ok = rmOccluderSystem::LoadExternalGeomDataset( "T:\\rage\\assets\\sample_rmOcclude\\cube.occlude", "",
			&m_numOccluderGeoms,
			&m_pOccluderGeomArray,
			&m_numOccluderPolys,
			&m_pOccluderPolyArray												
			);

		if( load_ok )
		{
			// occluder loaded? if yes then link it up
			if( m_pOccluderGeomArray && (m_numOccluderGeoms == 1) )
			{
				m_pDynamicOccluder = rage_new rmOccludeDynamicInst;

				// link to occluder object, copy its local space vertices out so we can retransform the occluder, poking in the worldspace vertex positions.
				m_pDynamicOccluder->LinkToBaseOccluder( &m_pOccluderGeomArray[0] );
			}
		}
		// Dynamic Occluder Setup End --------------------------------------------------

		// FIXME: adding widgets in 'AddWidgetsClient' doesnt work
#if __BANK
		bkBank& bank=BANKMGR.CreateBank("rage - rmOcclude");
		m_OccludeSystem->AddWidgets( bank );
#endif

		m_OccludeSystem->EnableDrawAllOccluders( m_testPvsViewID, true);// Default behavior for games would be off

		// Setup Initial position and orientation of the dynamic occlusion object------------------------------
		m_numAnimatedOccluders = 1;
		m_animatedOccluders = rage_new DynamicBox [m_numAnimatedOccluders];// just used for animated matrix data.
		m_animatedOccluders[0].Randomize();
		m_animatedOccluders[0].Translate( 1.f, 1.f, 1.f );

		// ---------------------------------------------------

		// --------------------------------------------------
		// Allocate a field of random spheres to occlusion+frustum cull.
		{
			m_numTestSpheres = 48;
			m_Spheres = rage_new SampleSphere [m_numTestSpheres];
			for(int i=0; i < m_numTestSpheres; i++ )
			{
				m_Spheres[i].Randomize();
				m_Spheres[i].SetStringByNum(i);
			}
		}
		// --------------------------------------------------
		// Allocate a field of random boxes to occlusion+frustum cull.
		{
			m_numTestBoxes = 64;
			m_Boxes = rage_new DynamicBox [m_numTestBoxes];
			for(int i=0; i < m_numTestBoxes; i++ )
			{
				m_Boxes[i].Randomize();
				m_Boxes[i].SetStringByNum(i);
			}
		}
		// --------------------------------------------------
	}

#if __WIN32
#if __BANK
	virtual void AddWidgetsClient() 
	{
	}
#endif

	void UpdateClient()
	{
		// TODO: move all this code to rmOcclude and put it in a helper function.
		// input parameters would be 

		// new and 'simpler' interface for setting up a view in the pvs system.
		// synthesizes all data that is passed into 'SetViewMatrices' as well as generating the appropriate culling plane data/matrices for
		// efficient frustum culling without the hassle of managing the 

		Matrix34	camMtx	= this->GetCameraMatrix();
		pvsViewerID pvsID	= m_testPvsViewID;

		m_OccludeSystem->SetPvsView( pvsID, this->GetViewport(), this->GetCameraMatrix() );
		m_OccludeSystem->SetRenderView( pvsID, this->GetViewport(), camMtx.d, camMtx.c, &camMtx );
		m_OccludeSystem->ProcessStaticOccluders( pvsID );


		float frame_dt = TIME.GetElapsedTime() - TIME.GetPrevElapsedTime();
		rmOccluderSystem::SetDebugAnimTimeDelta( frame_dt );// controls visualization animations right now.

		// Gracefully handle the case that noones made a vehicle geom occluder file yet.
		if( m_pDynamicOccluder && m_bUseAnimatedGeomOccluder)
		{	
			//Animate first!
			m_animatedOccluders[0].Update( frame_dt );
			Matrix34	vehLocalToWorld;
			vehLocalToWorld = *m_animatedOccluders[0].GetMatrix();

			m_pDynamicOccluder->Transform( vehLocalToWorld );	
			m_OccludeSystem->AddOccluderToPvsView( pvsID, m_pDynamicOccluder->GetLinkedOccluderPtr(), true);
		}

		//---------------------------------------------------------------------
		// Brute force loop over all spheres, occlusion test each one.
		//
		// Note: See how rmOcclude::IsVisible is used to turn the complex
		//			bitflag results of the visibility function into a bool result.
		//
		for(int i =0 ; i < this->m_numTestSpheres ;i++ )
		{
			m_Spheres[i].Update( frame_dt );


			bool	IsVisible = rmOcclude::IsVisible( m_OccludeSystem->TestSphereVisibility(	pvsID,
				m_Spheres[i].GetMatrix()->d,
				m_Spheres[i].GetRadius()
				));
			m_Spheres[i].SetVisibility( 0, IsVisible );// store result.
		}

		//---------------------------------------------------------------------
		// Brute force loop over all boxes, occlusion test each one.
		//
		// Note: See how rmOcclude::IsVisible is used to turn the complex
		//			bitflag results of the visibility function into a bool result.
		//
		for(int i =0 ; i < this->m_numTestBoxes ;i++ )
		{
			m_Boxes[i].Update( frame_dt );

			bool	IsVisible = rmOcclude::IsVisible( m_OccludeSystem->TestAABoxVisibility(	pvsID,
				m_Boxes[i].GetMin(),
				m_Boxes[i].GetMax(),
				m_Boxes[i].GetMatrix()
				));
			m_Boxes[i].SetVisibility( 0, IsVisible );// store result.
		}
		//---------------------------------------------------------------------
	}
#endif

	void DrawScene()
	{
#if __WIN32
		int viewIndex = 0;// loop over multiple views here...
		grcState::Default();
		grcState::Default();
		grcLightState::SetEnabled(false);

		for(int i =0 ; i < this->m_numTestBoxes ;i++ )
		{
			m_Boxes[i].Draw( viewIndex );// objects remember their visibility from UpdateClient() so no culling is needed here.
		}

		for(int i =0 ; i < this->m_numTestSpheres ;i++ )
		{
			m_Spheres[i].Draw( viewIndex );// objects remember their visibility from UpdateClient() so no culling is needed here.
		}

		// perform all debug drawing after scene has been rendered.

		grcState::SetAlphaBlend(false);
		grcState::SetBlendSet(grcbsNormal);
		m_OccludeSystem->DrawDebugViewInfo( m_testPvsViewID, pvsdd_DefaultSetting );

		grcViewport::SetCurrentWorldIdentity();
		
#endif
	}


	void DrawClient()
	{
		DrawScene();
	}
	int					m_numTestSpheres;
	SampleSphere		*m_Spheres;

	int					m_numTestBoxes;
	DynamicBox			*m_Boxes;

	int					m_numAnimatedOccluders;
	DynamicBox			*m_animatedOccluders;

	rmOccluderSystem	*m_OccludeSystem;
	bool				m_bUseAnimatedGeomOccluder;

	pvsViewerID			m_testPvsViewID;

	//======================================================================
	// local space occluder geometry, loaded by rmOccluder system, managed by the vehicle instance.
	// Assumes you have an rmOccluderSystem instance created already
	//
	// NOTE: dynamic occluders are being refactored to a simpler interface, this is for testing right now // ccoffin [4/6/2006]
	int						m_numOccluderGeoms;
	rmOccludePolyhedron		*m_pOccluderGeomArray;
	int						m_numOccluderPolys;
	rmOccludePoly			*m_pOccluderPolyArray;

	// new geom occluder data, transformed to worldspace
	rmOccludeDynamicInst	*m_pDynamicOccluder;// linked to static geom data (supports 1 object only right now)
	//======================================================================


private:
};

// main application
int Main()
{
#if __WIN32 && __DEV

#endif

	PARAM_file.Set("T:\\rage\\assets\\sample_rmOcclude\\cube.occlude");

	grcSampleOcclusion sample;
	sample.Init();
	sample.UpdateLoop();
	sample.Shutdown();

	sample.m_OccludeSystem->Destroy();
	delete [] sample.m_Boxes;
	delete [] sample.m_Spheres;

	return 0;
}
