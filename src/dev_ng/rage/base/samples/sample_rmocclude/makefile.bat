set ARCHIVE=sample_rmocclude
set FILES=sample_rmocclude
set TESTERS=sample_occlusion
set SAMPLE_LIBS=sample_rmcore %RAGE_SAMPLE_GRCORE_LIBS%
set LIBS=%SAMPLE_LIBS% rmocclude devcam gizmo parser parsercore %RAGE_GFX_LIBS% %RAGE_CORE_LIBS%
set XPROJ=%RAGE_DIR%/base/src %RAGE_DIR%/base/samples 
set XPROJ=%XPROJ% ../../src


