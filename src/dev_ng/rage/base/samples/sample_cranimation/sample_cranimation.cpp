//
// sample_cranimation/sample_cranimation.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/bkvector.h"
#include "bank/slider.h"
#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameiterators.h"
#include "creature/creature.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "grcore/im.h"
#include "grmodel/matrixset.h"
#include "rmcore/drawable.h"
#include "grblendshapes/manager.h"
#include "system/param.h"
#include "vectormath/legacyconvert.h"

using namespace rage;

#define DEFAULT_FILE "entity.type"
PARAM(file, "[sample_cranimation] The filename of the .type file to load (default entity.type)");
PARAM(forcevisible, "[sample_cranimation] Ignore visibility culling, force all meshes to be visible");
PARAM(hashnames, "[sample_cranimation] Optional file specifying additional animation id hashnames");

namespace ragesamples 
{

craSampleManager::craSampleManager() 
: m_Creature(NULL)
, m_LastDrawUpdateFrame(~0u)
, m_Drawable(NULL)
, m_TargetManager(NULL)
, m_Skeleton(NULL)
, m_MatrixSet(NULL)
, m_MoverMtx(NULL)
, m_PlaybackRate(1.0f)
, m_Play(true)
, m_DrawBones(false)
, m_DrawJoints(false)
, m_DrawSkin(true)
, m_ForceVisible(false)
, m_TrackEnable(false)
, m_TrackMover(true)
, m_TrackBone(0)
, m_TrackMtx(V_IDENTITY)
{
	m_CommonPool.Init(32, 5*1024);
	m_FrameDataFactory.Init(m_CommonPool);
}

void craSampleManager::Init(const char* path)
{
	crAnimation::InitClass();
	crCreature::InitClass();

	rmcSampleManager::Init(path);

	m_ForceVisible = PARAM_forcevisible.Get();
}

void craSampleManager::Shutdown()
{
#if __D3D11
	GRCDEVICE.LockContext();
#endif
	rmcSampleManager::Shutdown();

	crCreature::ShutdownClass();
	crAnimation::ShutdownClass();

#if __D3D11
	GRCDEVICE.UnlockContext();
#endif
}

void craSampleManager::InitClient()
{
#if __PFDRAW
	GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
	GetRageProfileDraw().SetEnabled(true);
#endif

	rmcSampleManager::InitClient();

	CreateDrawableSkeleton(m_Drawable, m_TargetManager, m_Skeleton, m_MoverMtx, m_Creature, m_MatrixSet);

	Assert(m_Drawable);
	// We may not have or need a targetmanager if it was a fragment.
	// Assert(m_TargetManager);
	Assert(m_Skeleton);
	Assert(m_MoverMtx); // optional?
	Assert(m_Creature);

#if __BANK
	const char* hashFilename = NULL;
	if(PARAM_hashnames.Get(hashFilename) && hashFilename && hashFilename[0])
	{
		LoadHashNames(hashFilename);
	}
#endif // __BANK

#if __BANK
	AddWidgetsAnimationManager();
#endif
}

void craSampleManager::CreateDrawableSkeleton(rmcDrawable*& drawable, grbTargetManager *& ENABLE_BLENDSHAPES_ONLY(manager), crSkeleton*& skeleton, Mat34V*& moverMtx, crCreature*& creature, grmMatrixSet*& matrixSet)
{
	// The first thing we do is allocate our rmcDrawable instance.
	drawable = rage_new rmcDrawable();

	// Get the drawable file name
	const char* defaultFile = GetDefaultFile();

	// Get the base filename
	char base[256];
	strcpy(base, fiAssetManager::FileName(defaultFile));

	// Get the path
	char path[256];
	fiAssetManager::RemoveNameFromPath(path, 256, defaultFile);

	// Push the path folder if the path is present
	bool pushedFolder = false;
	if( stricmp(base, defaultFile) )
	{
		ASSET.PushFolder(path);
		pushedFolder = true;
	}

	// Now we load the drawable.  
	if(!drawable->Load(base))
	{
		Quitf("Couldn't load file '%s'!", defaultFile);
	}

#if ENABLE_BLENDSHAPES
	manager = rage_new grbTargetManager;
	manager->Load(base, drawable);
#endif // ENABLE_BLENDSHAPES

	// Pop the folder if we pushed one
	if(pushedFolder)
	{
		ASSET.PopFolder();
	}

	// The drawable will have already loaded the skeleton data if it 
	// was specified in the .type file, but we still have to create a skeleton
	// instance.
	const crSkeletonData* skelData = drawable->GetSkeletonData();
	if(skelData != NULL)
	{
		// First we allocate out crSkeleton instance.
		skeleton = rage_new crSkeleton();

		// Now we initialize it with the skeleton data.
		skeleton->Init(*skelData);

		matrixSet = grmMatrixSet::Create(*skeleton);
	}
	else
	{
		Quitf("no skeleton referenced in file '%s'!", defaultFile);
	}

	// finally allocate a mover matrix
	moverMtx = rage_new Mat34V(V_IDENTITY);

	// set the parent matrix of the skeleton to be the mover matrix
	skeleton->SetParentMtx(reinterpret_cast<Mat34V*>(moverMtx));

	// create and initialize creature
	CreateCreature(creature);
}

void craSampleManager::CreateCreature(crCreature*& creature)
{
	Assert(m_Drawable);

	creature = rage_new crCreature;
	creature->Init(m_FrameDataFactory, m_FrameAccelerator, m_Skeleton, m_MoverMtx);
}

const char* craSampleManager::GetDefaultFile()
{
	const char* defaultFile = GetFileName(0);
	PARAM_file.Get(defaultFile);
	return defaultFile;
}

void craSampleManager::ShutdownClient()
{
	rmcSampleManager::ShutdownClient();

	ShutdownDrawableSkeleton();

	m_Skeleton = NULL;
	m_Drawable = NULL;
	m_TargetManager = NULL;
	m_MoverMtx = NULL;
	m_Creature = NULL;
	m_MatrixSet = NULL;

#if __PFDRAW
	GetRageProfileDraw().Shutdown();
#endif
}

void craSampleManager::ShutdownDrawableSkeleton()
{
	delete m_Creature;
	delete m_MatrixSet;
	delete m_Skeleton;
#if ENABLE_BLENDSHAPES
	if (m_TargetManager)
	{
		m_TargetManager->Release();
	}
#endif // ENABLE_BLENDSHAPES

	delete m_Drawable;
	delete m_MoverMtx;
}

void craSampleManager::UpdateMatrixSet()
{
	if (m_MatrixSet)
		m_MatrixSet->Update(*m_Skeleton, m_Drawable->IsSkinned());
}

void craSampleManager::UpdateClient()
{
	rmcSampleManager::UpdateClient();

	m_Skeleton->Update();
	UpdateMatrixSet();

	bool trackMover = m_TrackMover && m_MoverMtx;
	int trackBone = Clamp(m_TrackBone, 0, m_Skeleton->GetSkeletonData().GetNumBones()-1);
	Mat34V currMtx;
	if(trackMover)
	{
		currMtx = *m_MoverMtx;
	}
	else
	{
		m_Skeleton->GetGlobalMtx(trackBone, currMtx);
	}

	if(m_TrackEnable)
	{
		Mat34V mtx;
		InvertTransformOrtho(mtx, m_TrackMtx);
		Transform(mtx, currMtx, mtx);

		Matrix34 dcamMtx = GetCamMgr().GetCamera(GetCamMgr().GetCurrentCamera()).GetWorldMtx();
		dcamMtx.Dot(RCC_MATRIX34(mtx));
		dcamMtx.Normalize();
		GetCamMgr().GetCamera(GetCamMgr().GetCurrentCamera()).SetWorldMtx(dcamMtx);
	}

	m_TrackMtx = currMtx;
}


void craSampleManager::DrawClient()
{
	rmcSampleManager::DrawClient();

	if(m_Creature)
	{
		u32 thisFrame = GRCDEVICE.GetFrameCounter();
		if (m_LastDrawUpdateFrame != thisFrame)
		{
			m_Creature->SwapBuffers();
			m_Creature->DrawUpdate();
			m_LastDrawUpdateFrame = thisFrame;
		}
		m_Creature->PreDraw(true);
	}

	DrawSkeleton(*m_Skeleton, m_MatrixSet);

#if __PFDRAW
	grcBindTexture(NULL);
	grcLightState::SetEnabled(true);

	GetRageProfileDraw().SendDrawCameraForServer(GetCamMgr().GetWorldMtx());
	GetRageProfileDraw().Render(ShouldFlushPfDraw());
#endif
}

void craSampleManager::DrawSkeleton(crSkeleton& skel, grmMatrixSet* matrixSet)
{
	if(GetDrawSkin())
	{
		u8 lod = LOD_HIGH;
		if (GetForceVisible() || m_Drawable->IsVisible(RCC_MATRIX34(*m_MoverMtx), *grcViewport::GetCurrent(), lod)) 
		{
			float zDist = GetCamMgr().GetWorldMtx().d.Mag();
			grcTextureFactory::GetInstance().SetTextureLod(zDist);

			for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
			{
				if (GetBucketEnable(bucket)) 
				{
					u32 bucketMask = BUCKETMASK_GENERATE(bucket);
					if (m_Drawable->GetSkeletonData())
					{
						// render the skinned model with its posed skeleton
						m_Drawable->DrawSkinned(RCC_MATRIX34(*m_MoverMtx), *matrixSet, bucketMask, lod);
					}
					else
					{
						m_Drawable->Draw(RCC_MATRIX34(*m_MoverMtx), bucketMask, lod);
					}
				}
			}
		}
	}

	if (GetDrawBones())
	{
		DrawBones(skel);
	}

	if (GetDrawJoints())
	{
		DrawJoints(skel);
	}
}

void craSampleManager::DrawBones(crSkeleton &skel, Color32 c)
{
	// setup render state:
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);
	grcViewport::SetCurrentWorldIdentity();

	// render dem bones:
	grcColor(c);
	Vec3V a, b;
	int totalBones = skel.GetSkeletonData().GetNumBones();
	for (int i = 0; i < totalBones; i++)
	{
		const crSkeletonData &skeldata	= skel.GetSkeletonData();
		const crBoneData *boneData		= skeldata.GetBoneData(i);
		Mat34V mat;
		skel.GetGlobalMtx(i, mat);

		a = mat.GetCol3();
		const crBoneData *child;
		for (child = boneData->GetChild(); child; child = child->GetNext())
		{
			Mat34V childMat;
			skel.GetGlobalMtx(child->GetIndex(), childMat);
			b = childMat.GetCol3();
			grcBegin(drawLines,2);
			grcVertex3f(a);
			grcVertex3f(b);
			grcEnd();
		}
	}
}

void craSampleManager::DrawJoints(crSkeleton& skel, Color32 cx, Color32 cy, Color32 cz)
{
	// setup render state:
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);
	grcViewport::SetCurrentWorldIdentity();

	int totalBones = skel.GetSkeletonData().GetNumBones();
	float maxAxisSize = 0.001f;

	for(int i=totalBones-1; i>=0; i--)
	{
		const crSkeletonData &skeldata	= skel.GetSkeletonData();
		const crBoneData *boneData		= skeldata.GetBoneData(i);
		Mat34V mat;
		skel.GetGlobalMtx(i, mat);

		float axisSize = i ? (Mag(boneData->GetDefaultTranslation()).Getf() * 0.20f) : maxAxisSize;

		Vec3V vo(0.f, 0.f, 0.f);
		Vec3V vx(axisSize, 0.f, 0.f);
		Vec3V vy(0.f, axisSize, 0.f);
		Vec3V vz(0.f, 0.f, axisSize);

		grcViewport::SetCurrentWorldMtx(mat);

		grcColor(cx);
		grcBegin(drawLines,2);
		grcVertex3f(vo);
		grcVertex3f(vx);
		grcEnd();

		grcColor(cy);
		grcBegin(drawLines,2);
		grcVertex3f(vo);
		grcVertex3f(vy);
		grcEnd();

		grcColor(cz);
		grcBegin(drawLines,2);
		grcVertex3f(vo);
		grcVertex3f(vz);
		grcEnd();		

		maxAxisSize = Max(maxAxisSize, axisSize);
	}
}

void craSampleManager::DrawVectorInJointSpace(crSkeleton& skel, const char* joint, const Vec3V &vec, Color32 color, const Vec3V &ofs)
{
    // setup render state:
    grcBindTexture(NULL);
    grcLightState::SetEnabled(false);
    grcViewport::SetCurrentWorldIdentity();

    const crBoneData* bonedata = skel.GetSkeletonData().FindBoneData(joint);
    if (bonedata == NULL)
    {
        Warningf("Joint '%s' not found.", joint);
        return;
    }

    Mat34V mat;
	skel.GetGlobalMtx(bonedata->GetIndex(), mat);

    Mat34V jointOrient;
    bonedata->CalcCumulativeJointScaleOrients(jointOrient);

    Mat34V adjustedOrient;
	Transform(adjustedOrient, mat, jointOrient);

    grcViewport::SetCurrentWorldMtx(adjustedOrient);
    grcColor(color);
    grcBegin(drawLines,2);
    grcVertex3f(ofs);
    grcVertex3f(vec);
    grcEnd();
}


#if __BANK
craSampleManager::FrameEditorWidgets::FrameEditorWidgets()
: m_Frame(NULL)
, m_SkelData(NULL)
{

}

craSampleManager::FrameEditorWidgets::~FrameEditorWidgets()
{

}

void craSampleManager::FrameEditorWidgets::Init(crFrame& frame, const crSkeletonData* skelData)
{
	Assert(!m_Frame);

	m_Frame = &frame;
	m_SkelData = skelData;
}

class AddWidgetIterator : public crFrameIterator<AddWidgetIterator>
{
public:
	AddWidgetIterator(crFrame& frame, atArray<Vec3V>& widgetValues, atArray<bool>& widgetInvalid, bkBank& bank, const crSkeletonData* skelData)
		: crFrameIterator<AddWidgetIterator>(frame)
		, m_WidgetValues(&widgetValues)
		, m_WidgetInvalid(&widgetInvalid)
		, m_Bank(&bank)
		, m_SkelData(skelData)
	{
	}

	void IterateDof(const crFrameData::Dof& dof, const crFrame::Dof&, float)
	{
		char buf[512];
		sprintf(buf, "track %d id %d ", dof.m_Track, dof.m_Id);

		char buf2[256]; 
		buf2[0] = '\0';
		
		const char* name = crSkeletonData::DebugConvertBoneIdToName(dof.m_Id);
		const char* track = crAnimTrack::ConvertTrackIndexToName(dof.m_Track);
		if(name || track)
		{
			if(name)
			{
				sprintf(buf2, "'%s' ", name);
			}
			if(track)
			{
				safecat(buf2, track, 256);
			}
		}

		switch(dof.m_Track)
		{
		case kTrackBoneTranslation:
		case kTrackBoneRotation:
		case kTrackBoneScale:
			{
				int boneIdx;
				if(m_SkelData && m_SkelData->ConvertBoneIdToIndex(dof.m_Id, boneIdx))
				{
					const crBoneData* bd = m_SkelData->GetBoneData(boneIdx);
					if(bd)
					{
						switch(dof.m_Track)
						{
						case kTrackBoneTranslation:
							sprintf(buf2, "bone[%d] '%s' translation", bd->GetIndex(), bd->GetName());
							break;

						case kTrackBoneRotation:
							sprintf(buf2, "bone[%d] '%s' rotation", bd->GetIndex(), bd->GetName());
							break;

						case kTrackBoneScale:
							sprintf(buf2, "bone[%d] '%s' scale", bd->GetIndex(), bd->GetName());
							break;
						}
					}
				}
			}
			break;
		}

		strcat(buf, buf2);
		m_Bank->PushGroup(buf, false);

		bool& b = m_WidgetInvalid->Grow();
		b = true;

		m_Bank->AddToggle("invalid", &b);

		Vec3V& v = m_WidgetValues->Grow();
		v = Vec3V(V_ZERO);

		switch(dof.m_Type)
		{
		case kFormatTypeQuaternion:
			m_Bank->AddAngle("euler x", &v[0], bkAngleType::RADIANS);
			m_Bank->AddAngle("euler y", &v[1], bkAngleType::RADIANS);
			m_Bank->AddAngle("euler z", &v[2], bkAngleType::RADIANS);
			break;

		case kFormatTypeVector3:
			m_Bank->AddVector("vector xyz", reinterpret_cast<Vector3*>(&v), bkVector::FLOAT_MIN_VALUE, bkVector::FLOAT_MAX_VALUE, 0.001f);
			break;

		case kFormatTypeFloat:
			m_Bank->AddSlider("float", &v[0], -5.f, 5.f, 0.001f);
			break;
		}

		m_Bank->PopGroup();
	}

	atArray<Vec3V>* m_WidgetValues;
	atArray<bool>* m_WidgetInvalid;
	bkBank* m_Bank;
	const crSkeletonData* m_SkelData;
};

void craSampleManager::FrameEditorWidgets::AddWidgets(bkBank& bk)
{
	Assert(m_Frame);

	const int numDofs = m_Frame->GetNumDofs();

	m_WidgetValues.Reserve(numDofs);
	m_WidgetInvalid.Reserve(numDofs);

	AddWidgetIterator it(*m_Frame, m_WidgetValues, m_WidgetInvalid, bk, m_SkelData);
	it.Iterate(NULL, 1.f, false);
}

class ResetWidgetIterator : public crFrameIterator<ResetWidgetIterator>
{
public:
	ResetWidgetIterator(crFrame& frame, atArray<Vec3V>& widgetValues, atArray<bool>& widgetInvalid)
		: crFrameIterator<ResetWidgetIterator>(frame)
		, m_WidgetValues(&widgetValues)
		, m_WidgetInvalid(&widgetInvalid)
	{
		m_Index = 0;
	}

	void IterateDof(const crFrameData::Dof& dof, const crFrame::Dof& dest, float)
	{
		bool& b = (*m_WidgetInvalid)[m_Index];
		b = dest.IsInvalid();

		Vec3V& v = (*m_WidgetValues)[m_Index];
		if(!dest.IsInvalid())
		{
			switch(dof.m_Type)
			{
			case kFormatTypeQuaternion:
				v = QuatVToEulersXYZ(dest.GetUnsafe<QuatV>());
				break;

			case kFormatTypeVector3:
				v = dest.GetUnsafe<Vec3V>();
				break;

			case kFormatTypeFloat:
				v[0] = dest.GetUnsafe<float>();
				break;
			}
		}

		m_Index++;
	}

	atArray<Vec3V>* m_WidgetValues;
	atArray<bool>* m_WidgetInvalid;
	int m_Index;
};

void craSampleManager::FrameEditorWidgets::ResetWidgets()
{
	Assert(m_Frame);

	ResetWidgetIterator it(*m_Frame, m_WidgetValues, m_WidgetInvalid);
	it.Iterate(NULL, 1.f, false);
}

class UpdateWidgetIterator : public crFrameIterator<UpdateWidgetIterator>
{
public:
	UpdateWidgetIterator(crFrame& frame, atArray<Vec3V>& widgetValues, atArray<bool>& widgetInvalid)
		: crFrameIterator<UpdateWidgetIterator>(frame)
		, m_WidgetValues(&widgetValues)
		, m_WidgetInvalid(&widgetInvalid)
	{
		m_Index = 0;
	}

	void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		const Vec3V& v = (*m_WidgetValues)[m_Index];
		switch(dof.m_Type)
		{
		case kFormatTypeQuaternion:
			dest.Set<QuatV>(QuatVFromEulersXYZ(v));
			break;

		case kFormatTypeVector3:
			dest.Set<Vec3V>(v);
			break;

		case kFormatTypeFloat:
			dest.Set<float>(v[0]);
			break;
		}

		const bool& b = (*m_WidgetInvalid)[m_Index];
		dest.SetInvalid(b);

		m_Index++;
	}

	atArray<Vec3V>* m_WidgetValues;
	atArray<bool>* m_WidgetInvalid;
	int m_Index;
};

void craSampleManager::FrameEditorWidgets::UpdateFrame()
{
	Assert(m_Frame);

	UpdateWidgetIterator it(*m_Frame, m_WidgetValues, m_WidgetInvalid);
	it.Iterate(NULL, 1.f, false);
}

bool craSampleManager::LoadHashNames(const char* hashFilename)
{
	fiSafeStream f(ASSET.Open(hashFilename, "hashnames"));
	if(f)
	{
		fiTokenizer T(hashFilename, f);	

		const int maxBufSize = 256;
		char buf[maxBufSize];
		while(T.GetLine(buf, maxBufSize) > 0)
		{
			char* ch = buf;

			const char* trackIdxString = ch;
			while(*ch)
			{
				if(*ch == ',')
				{
					*ch = '\0';
					ch++;
					break;
				}
				ch++;
			}
			const char* dofIdString = ch;
			while(*ch)
			{
				if(*ch == ',')
				{
					*ch = '\0';
					ch++;
					break;
				}
				ch++;
			}
			const char* nameString = ch;

			if(*trackIdxString && *dofIdString && *nameString)
			{
//				u8 trackIdx = u8(atoi(trackIdxString));
				u16 dofId = u16(atoi(dofIdString));

				m_Skeleton->GetSkeletonData().DebugRegisterBoneIdToName(dofId, nameString);
			}
		}

		return true;
	}
	else
	{
		Errorf("craSampleManager::LoadHashNames - failed to open hash file '%s'", hashFilename);
		return false;
	}
}

void craSampleManager::AddWidgetsAnimationManager()
{
	bkBank &animBank=BANKMGR.CreateBank("Animation Manager",50,500);
	animBank.AddToggle("Draw Skeleton Bones", &m_DrawBones);
	animBank.AddToggle("Draw Skeleton Joints", &m_DrawJoints);
	animBank.AddToggle("Draw Skin", &m_DrawSkin);
	animBank.AddToggle("Force Visible", &m_ForceVisible);

	animBank.PushGroup("Playback");
	animBank.AddSlider("Rate", &m_PlaybackRate, 0.f, 10.0f, 0.01f);
	animBank.AddToggle("Play", &m_Play);
	animBank.PopGroup();

	animBank.PushGroup("CameraTracking");
	animBank.AddToggle("Enable Tracking", &m_TrackEnable);
	animBank.AddToggle("Track Mover", &m_TrackMover);
	animBank.AddSlider("Bone Index", &m_TrackBone, -1, 1000, 1);
	animBank.PopGroup();
}
#endif // __BANK

} // namespace ragesamples
