// 
// sample_cranimation/viewer_anim.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 



// TITLE: Viewer Animation
// PURPOSE:
//		This shows a more complicated animation viewer.



#include "sample_cranimation.h"

#include "sample_rmcore/sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "cranimation/animation.h"
#include "cranimation/framedof.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "devcam/cammgr.h"
#include "file/asset.h"
#include "file/token.h"
#include "grcore/im.h"
#include "grcore/image.h"
#include "grmodel/matrixset.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

XPARAM(file);

const Vector3 BUTTON_COLOR(0.8f, 0.8f, 0.6f);
const Vector3 BUTTON_BORDER_HI(1.f, 1.f, 1.f);
const Vector3 BUTTON_BORDER_LO(0.f, 0.f, 0.f);
const Vector3 BUTTON_TEXT_COLOR(0.f, 0.f, 0.f);
const Vector3 BUTTON_TEXT_OVER_COLOR(0.5f, 0.5f, 0.5f);
const Vector3 BUTTON_TEXT_HIGHLIGHT_COLOR(1.0f, 0.5f, 0.0f);
const Vector3 TOOLTIP_COLOR(0.99f, 0.99f, 0.7f);
const Vector3 TOOLTIP_BORDER_COLOR(0.f, 0.f, 0.f);
const Vector3 TOOLTIP_TEXT_COLOR(0.f, 0.f, 0.f);
const Vector3 ONSCREEN_TEXT_COLOR(1.f, 1.f, 1.f);

const int TEXT_HEIGHT = 10;
const int TEXT_PAD = 2;
const int DEFAULT_BUTTON_HEIGHT = TEXT_HEIGHT + (TEXT_PAD*2);
const int DEFAULT_BUTTON_PAD = (TEXT_PAD)*2;

const int MAX_ANIM_SLOTS = 8;

const float TOOLTIP_DEPTH = 0.f;

bool g_DisableCamera = false;

typedef void (WindowEventCB) (int, void*);

using namespace rage;
using namespace ragesamples;

class rsBase
{
public:
	rsBase(void);
	virtual ~rsBase(void);

	virtual void Draw(int x, int y) = 0;
	virtual void Update(int x, int y) = 0;

	void SetPosition(int x, int y, float depth);

protected:
	int m_X;
	int m_Y;
	float m_Depth;
};

rsBase::rsBase(void) :
	m_X(0),
	m_Y(0),
	m_Depth(0.f)
{
}

rsBase::~rsBase(void)
{
}

void rsBase::SetPosition(int x, int y, float depth)
{
	m_X = x;
	m_Y = y;
	m_Depth = depth;
}


class rsWindow : public rsBase
{
public:
	rsWindow(void);
	virtual ~rsWindow(void);

	virtual void Draw(int x, int y);
	virtual void Update(int x, int y);

	void SetCB(WindowEventCB *pCB, void *pCBData);
	void SetSize(int width, int height);
	void SetToolTip(const char* pTip);
	void SetHighlight(bool highlight);
		
	enum
	{
		EventMouseOver,
		EventMouseOut,
		EventMouseDown,
		EventMouseUp,
		EventMouseClick
	};

protected:
	static void DrawBox(int tlx, int tly, int brx, int bry, float depth, const Vector3& c);
	static void DrawBorder(int tlx, int tly, int brx, int bry, float depth, const Vector3& c, bool hi);

	static int TextLength(const char* pText);

	WindowEventCB *m_pCB;
	void* m_pCBData;
	char* m_pTip;
	int m_Width;
	int m_Height;
	bool m_IsHighlight;

	bool m_IsMouseOver;
};


rsWindow::rsWindow(void) :
	m_pCB(0),
	m_pCBData(0),
	m_pTip(0),
	m_Width(0),
	m_Height(0),
	m_IsHighlight(false),
	m_IsMouseOver(false)
{
}

rsWindow::~rsWindow(void)
{
	delete [] m_pTip;
}

void rsWindow::DrawBox(int tlx, int tly, int brx, int bry, float depth, const Vector3& c)
{
	grcBegin(drawTriStrip, 4);
	grcColor3f(c);
	grcVertex3f(float(brx), float(tly), depth);
	grcVertex3f(float(tlx), float(tly), depth);
	grcVertex3f(float(brx), float(bry), depth);
	grcVertex3f(float(tlx), float(bry), depth);
	grcEnd();
}

void rsWindow::DrawBorder(int tlx, int tly, int brx, int bry, float depth, const Vector3& c, bool hi)
{
	grcBegin(drawLineStrip, 3);
	grcColor3f(c);
	grcVertex3f(float(tlx), float(bry), depth);
	if(hi)
		grcVertex3f(float(tlx), float(tly), depth);
	else
		grcVertex3f(float(brx), float(bry), depth);
	grcVertex3f(float(brx), float(tly), depth);
	grcEnd();
}

int rsWindow::TextLength(const char* pText)
{
	int textLen = istrlen(pText);
	int fontWidth = grcFont::GetCurrent().GetWidth();
	return textLen * fontWidth;
}

void rsWindow::Draw(int, int)
{
	if(m_pTip && m_IsMouseOver)
	{
		int iMouseX = ioMouse::GetX();
		int iMouseY = ioMouse::GetY();

		int tipTextLen = TextLength(m_pTip) + (TEXT_PAD*2);
		int tipTextHeight = grcFont::GetCurrent().GetHeight() + (TEXT_PAD*2);
		int tipHeightOffset = tipTextHeight + TEXT_PAD;

		int tipTlX = iMouseX;
		int tipTlY = iMouseY-tipHeightOffset;
		int tipBrX = iMouseX+tipTextLen;
		int tipBrY = iMouseY-tipHeightOffset-tipTextHeight;

		DrawBox(tipTlX, tipTlY, tipBrX, tipBrY, TOOLTIP_DEPTH, TOOLTIP_COLOR);
		DrawBorder(tipTlX, tipTlY, tipBrX, tipBrY, TOOLTIP_DEPTH, TOOLTIP_TEXT_COLOR, true);
		DrawBorder(tipTlX, tipTlY, tipBrX, tipBrY, TOOLTIP_DEPTH, TOOLTIP_TEXT_COLOR, false);
		grcFont::GetCurrent().DrawScaled(float(tipTlX+TEXT_PAD), float(tipBrY+TEXT_PAD), TOOLTIP_DEPTH, Color32(TOOLTIP_TEXT_COLOR), 1.f, 1.f, m_pTip);
	}
}

void rsWindow::Update(int x, int y)
{
	int iMouseX = ioMouse::GetX();
	int iMouseY = ioMouse::GetY();

	if(iMouseX >= (m_X+x) && 
		iMouseX <= int(m_X+m_Width+x) &&
        iMouseY >= int(m_Y+y) && 
		iMouseY <= int(m_Y+m_Height+y))
	{
		// Mouse within window
		if(!m_IsMouseOver)
		{
			if(m_pCB)
			{
				m_pCB(EventMouseOver, m_pCBData);
			}
		}
		m_IsMouseOver = true;
	}
	else
	{
		// Mouse outside window
		if(m_IsMouseOver)
		{
			if(m_pCB)
			{
				m_pCB(EventMouseOut, m_pCBData);
			}
		}
		m_IsMouseOver = false;
	}

}

void rsWindow::SetCB(WindowEventCB* pCB, void* pCBData)
{
	m_pCB = pCB;
	m_pCBData = pCBData;
}


void rsWindow::SetSize(int width, int height)
{
	m_Width = width;
	m_Height = height;
}

void rsWindow::SetToolTip(const char* pTip)
{
	if(m_pTip)
		delete [] m_pTip;
	m_pTip = 0;
	if(pTip)
		m_pTip = StringDuplicate(pTip);
}

void rsWindow::SetHighlight(bool highlight)
{
	m_IsHighlight = highlight;
}


class rsButton : public rsWindow
{
public:
	rsButton(void);
	virtual ~rsButton(void);

	virtual void Draw(int x, int y);
	virtual void Update(int x, int y);

	bool IsMouseDown(void);

	void SetToggleOn(bool bOn);
	bool IsToggleOn(void);

protected:
	bool m_IsMouseDown;
	bool m_IsToggleOn;
};


rsButton::rsButton(void) :
	m_IsMouseDown(false),
	m_IsToggleOn(false)
{
}

rsButton::~rsButton(void)
{
}

void rsButton::Draw(int x, int y)
{
	DrawBox(m_X+x, m_Y+y, m_X+m_Width+x, m_Y+m_Height+y, m_Depth, BUTTON_COLOR);
	bool Depress = m_IsMouseDown ^ m_IsToggleOn;
	DrawBorder(m_X+x, m_Y+y, m_X+m_Width+x-1, m_Y+m_Height+y-1, m_Depth, BUTTON_BORDER_HI, !Depress);
	DrawBorder(m_X+x, m_Y+y, m_X+m_Width+x-1, m_Y+m_Height+y-1, m_Depth, BUTTON_BORDER_LO, Depress);

	rsWindow::Draw(x, y);
}


void rsButton::Update(int x, int y)
{
	rsWindow::Update(x, y);

	// Test for mouse lmb press/release
	if(m_IsMouseOver)
	{
		if(ioMouse::GetPressedButtons() & ioMouse::MOUSE_LEFT)
		{
			if(m_pCB)
			{
				m_pCB(EventMouseDown, m_pCBData);
			}
			m_IsMouseDown = true;
			g_DisableCamera = true;
		}
	}

	if(m_IsMouseDown && (ioMouse::GetReleasedButtons() & ioMouse::MOUSE_LEFT))
	{
		if(m_pCB)
		{
			m_pCB(EventMouseUp, m_pCBData);
		}
		if(m_IsMouseOver)
		{
			if(m_pCB)
			{
				m_pCB(EventMouseClick, m_pCBData);
			}
        }
		m_IsMouseDown = false;
	}
}

void rsButton::SetToggleOn(bool bOn)
{
	m_IsToggleOn = bOn;
}

bool rsButton::IsToggleOn(void)
{
	return m_IsToggleOn;
}

bool rsButton::IsMouseDown(void)
{
	return m_IsMouseDown;
}


class rsTextButton : public rsButton
{
public:
	rsTextButton(void);
	rsTextButton(int x, int y, float Depth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, const char* pText, u8 Alignment);
	virtual ~rsTextButton(void);

	virtual void Draw(int x, int y);

	void SetText(const char* pText);
	void SetAlignment(u8 alignment);

	enum
	{
		AlignLeft,
		AlignCenter,
		AlignRight
	};

protected:
	char* m_pText;
	u8 m_Alignment;
};


rsTextButton::rsTextButton(void) :
	m_pText(0),
	m_Alignment(AlignLeft)
{		
}

rsTextButton::rsTextButton(int x, int y, float Depth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, const char* pText, u8 Alignment) :
	m_pText(0)
{
	SetPosition(x, y, Depth);
	SetSize(Width, Height);
	SetCB(pCB, pCBData);
	SetToolTip(pTip);
	SetText(pText);
	SetAlignment(Alignment);
}


rsTextButton::~rsTextButton(void)
{
	delete [] m_pText;
}

void rsTextButton::SetText(const char* pText)
{
	if(m_pText)
		delete [] m_pText;
	m_pText = 0;
	if(pText)
		m_pText = StringDuplicate(pText);
}

void rsTextButton::SetAlignment(u8 alignment)
{
	m_Alignment = alignment;
}

void rsTextButton::Draw(int x, int y)
{
	rsButton::Draw(x, y);

	if(m_pText)
	{
		Color32 c(m_IsMouseOver ? BUTTON_TEXT_OVER_COLOR : (m_IsHighlight ? BUTTON_TEXT_HIGHLIGHT_COLOR : BUTTON_TEXT_COLOR));

		int textX = m_X+x+TEXT_PAD;
		switch(m_Alignment)
		{
		case AlignRight:
			textX += m_Width - TextLength(m_pText) - (TEXT_PAD*2);
			break;
		case AlignCenter:
			textX += (m_Width - TextLength(m_pText) - (TEXT_PAD*2)) / 2;
			break;
		}

		grcFont::GetCurrent().DrawScaled(float(textX), float(m_Y+y+TEXT_PAD), m_Depth, c, 1.f, 1.f, m_pText);
	}
}


class rsIconButton : public rsButton
{
public:
	rsIconButton(void);
	rsIconButton(int x, int y, float fDepth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, const char* pImagePath, const char* pImageFilename);
	rsIconButton(int x, int y, float fDepth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, grcImage* pImage);
	virtual ~rsIconButton(void);

	virtual void Draw(int x, int y);

	void SetImageFilename(const char* imagePath, const char* imageFilename);
	void SetImage(grcImage* pImage);

protected:
	grcTexture *m_pTex;
};


rsIconButton::rsIconButton(void) :
	m_pTex(0)
{
}

rsIconButton::rsIconButton(int x, int y, float Depth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, const char* pImagePath, const char* pImageFilename)
{
	SetPosition(x, y, Depth);
	SetSize(Width, Height);
	SetCB(pCB, pCBData);
	SetToolTip(pTip);
	SetImageFilename(pImagePath, pImageFilename);
}

rsIconButton::rsIconButton(int x, int y, float Depth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, grcImage* pImage)
{
	SetPosition(x, y, Depth);
	SetSize(Width, Height);
	SetCB(pCB, pCBData);
	SetToolTip(pTip);
	SetImage(pImage);
}

rsIconButton::~rsIconButton(void)
{
	if(m_pTex)
		m_pTex->Release();
}

void rsIconButton::Draw(int x, int y)
{
	if(m_pTex)
	{
		grcBindTexture(m_pTex);
		
		int TexWidth = m_Width;
		int TexHeight = m_Height;
		int Offset = (m_IsMouseDown ^ m_IsToggleOn) ? 1 : 0;
		Vector2 TexTl(0.f, 0.f);
		Vector2 TexBr(1.f, 1.f);
		Vector3 TexCol(1.f, 1.f, 1.f);

		grcColor3f(TexCol);
		grcBegin(drawTriStrip, 4);
		grcTexCoord2f(TexBr.x, TexTl.y);
		grcVertex3f(float(TexWidth+m_X+x+Offset), float(m_Y+y+Offset), m_Depth);
		grcTexCoord2f(TexTl.x, TexTl.y);
		grcVertex3f(float(m_X+x+Offset), float(m_Y+y+Offset), m_Depth);
		grcTexCoord2f(TexBr.x, TexBr.y);
		grcVertex3f(float(TexWidth+m_X+x+Offset), float(TexHeight+m_Y+y+Offset), m_Depth);
		grcTexCoord2f(TexTl.x, TexBr.y);
		grcVertex3f(float(m_X+x+Offset), float(TexHeight+m_Y+y+Offset), m_Depth);
		grcEnd();

		grcBindTexture(0);
	}

	// intentionally skipping rsButton
	rsWindow::Draw(x, y);
}

void rsIconButton::SetImageFilename(const char* imagePath, const char* imageFilename)
{
	// Clean up previous m_pTex?
	char sBuf[255];
	sprintf(sBuf, "%s\\%s", imagePath, imageFilename);
	m_pTex = grcCreateTexture(sBuf);
	// providing path and filename seperately doesn't seem to work?
//	m_pTex = grcCreateTexture(imagePath, imageFilename);
}

void rsIconButton::SetImage(grcImage* pImage)
{
	m_pTex = grcTextureFactory::GetInstance().Create(pImage);
}

class rsScrubber : public rsButton 
{
public:
	rsScrubber(void);
	rsScrubber(int x, int y, float Depth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, float progress, float minor, float major);
	virtual ~rsScrubber(void);

	virtual void Draw(int x, int y);
	virtual void Update(int x, int y);

	void SetProgress(float progress);
	void SetMarks(float minor, float major);

	float GetProgress(void);

protected:
	float m_Progress;
	float m_Minor;
	float m_Major;

private:
	int m_DownMouseX0;
};


rsScrubber::rsScrubber(void) :
	m_Progress(0.f),
	m_Minor(0.f),
	m_Major(0.f),
	m_DownMouseX0(0)
{
}

rsScrubber::rsScrubber(int x, int y, float Depth, int Width, int Height, WindowEventCB pCB, void* pCBData, const char* pTip, float progress, float minor, float major) :
	m_Progress(progress),
	m_Minor(minor),
	m_Major(major),
	m_DownMouseX0(0)
{
	SetPosition(x, y, Depth);
	SetSize(Width, Height);
	SetCB(pCB, pCBData);
    SetToolTip(pTip);
}

rsScrubber::~rsScrubber(void)
{
}

void rsScrubber::SetProgress(float progress)
{
	m_Progress = progress;
}

void rsScrubber::SetMarks(float minor, float major)
{
	m_Minor = minor;
	m_Major = major;
}

void rsScrubber::Update(int x, int y)
{
#ifdef MOVE_SCRUBBER
	int xNew = x - int(float(m_Width) * m_Progress);

	if(!m_IsMouseDown)
	{
		m_DownMouseX0 = ioMouse::GetX() + int(m_Progress * float(m_Width));
	}

	rsButton::Update(xNew, y);

	if(m_IsMouseDown)
	{
		// based on mouse position, move the percentage point
		int mouseXMove = m_DownMouseX0 - ioMouse::GetX();
		m_Progress = Clamp(float(mouseXMove) / float(m_Width), 0.f, 1.f);
	}
#else // MOVE_SCRUBBER
	rsButton::Update(x, y);

	if(m_IsMouseDown)
	{
		// based on mouse position, move the percentage point
		int mouseXMove = ioMouse::GetX() - x - m_X;
		m_Progress = Clamp(float(mouseXMove) / float(m_Width), 0.f, 1.f);
	}

#endif // MOVE_SCRUBBER
}

void rsScrubber::Draw(int x, int y)
{
#ifdef MOVE_SCRUBBER
	int xNew = x - int(float(m_Width) * m_Progress);

	rsButton::Draw(xNew, y);

	// Draw lots of crazy marks
	if(m_Minor > 0.f && m_Minor < 1.f && (m_Minor * float(m_Width)) >= 2.f)
	{
		grcColor3f(0.f, 0.f, 0.f);

		float minor = m_Minor;
		while(minor < 1.f)
		{
			grcBegin(drawLineStrip, 2);
			grcVertex3f(float(m_Width)*minor+float(m_X+xNew), float(m_Height-1+m_Y+y), m_Depth);
			grcVertex3f(float(m_Width)*minor+float(m_X+xNew), float(m_Height)*0.5f+float(m_Y+y), m_Depth);
			grcEnd();

			minor += m_Minor;
		}
	}

	if(m_Major > 0.f && m_Major < 1.f && (m_Major * m_Width) >= 2.f)
	{
		grcColor3f(0.f, 0.f, 0.f);

		float major = m_Major;
		while(major < 1.f)
		{
			grcBegin(drawLineStrip, 2);
			grcVertex3f(float(m_Width)*major+float(m_X+xNew), float(m_Height-1+m_Y+y), m_Depth);
			grcVertex3f(float(m_Width)*major+float(m_X+xNew), float(1+m_Y+y), m_Depth);
			grcEnd();

			major += m_Major;
		}
	}

	grcColor3f(1.f, 0.f, 0.f);
	grcBegin(drawLineStrip, 2);
	grcVertex3f(float(m_X+x), float(m_Y+y), m_Depth);
	grcVertex3f(float(m_X+x), float(m_Height+m_Y+y), m_Depth);
	grcEnd();
#else  // MOVE_SCRUBBER
	rsButton::Draw(x, y);

	// Draw lots of crazy marks
	if(m_Minor > 0.f && m_Minor < 1.f && (m_Minor * float(m_Width)) >= 2.f)
	{
		grcColor3f(0.f, 0.f, 0.f);

		float minor = m_Minor;
		while(minor < 1.f)
		{
			grcBegin(drawLineStrip, 2);
			grcVertex3f(float(m_Width)*minor+float(m_X+x), float(m_Height-1+m_Y+y), m_Depth);
			grcVertex3f(float(m_Width)*minor+float(m_X+x), float(m_Height)*0.5f+float(m_Y+y), m_Depth);
			grcEnd();

			minor += m_Minor;
		}
	}

	if(m_Major > 0.f && m_Major < 1.f && (m_Major * m_Width) >= 2.f)
	{
		grcColor3f(0.f, 0.f, 0.f);

		float major = m_Major;
		while(major < 1.f)
		{
			grcBegin(drawLineStrip, 2);
			grcVertex3f(float(m_Width)*major+float(m_X+x), float(m_Height-1+m_Y+y), m_Depth);
			grcVertex3f(float(m_Width)*major+float(m_X+x), float(1+m_Y+y), m_Depth);
			grcEnd();

			major += m_Major;
		}
	}

	grcColor3f(1.f, 0.f, 0.f);
	grcBegin(drawLineStrip, 2);
	grcVertex3f(float(m_X+x)+(float(m_Width)*m_Progress), float(m_Y+y), m_Depth);
	grcVertex3f(float(m_X+x)+(float(m_Width)*m_Progress), float(m_Height+m_Y+y), m_Depth);
	grcEnd();
#endif  // MOVE_SCRUBBER
}

float rsScrubber::GetProgress(void)
{
	return m_Progress;
}


class rsContainer : public rsBase
{
public:
	rsContainer(void);
	virtual ~rsContainer(void);

	virtual void Update(int x, int y);
	virtual void Draw(int x, int y);

	int GetChildCount() const { return m_apChildren.GetCount();}
protected:
	atArray<rsBase*> m_apChildren;
};


rsContainer::rsContainer(void)
{
}

rsContainer::~rsContainer(void)
{
	for(int i=0; i<m_apChildren.GetCount(); i++)
		delete m_apChildren[i];
}

void rsContainer::Update(int x, int y)
{
	for(int i=0; i<m_apChildren.GetCount(); i++)
		m_apChildren[i]->Update(m_X+x, m_Y+y);
}

void rsContainer::Draw(int x, int y)
{
	for(int i=0; i<m_apChildren.GetCount(); i++)
		m_apChildren[i]->Draw(m_X+x, m_Y+y);
}


class rsVcrControls : public rsContainer
{
public:
	rsVcrControls(const char*);

	virtual void Update(int x, int y);

	void ResetState(bool bCurrentPause);
	bool HasStateChanged(bool& rOutPause, bool& rOutAbsolute, float& rOutStepJump);

	void OnPause(bool pause);
	void OnRelativeStep(float step);
	void OnAbsoluteJump(float jump);

	static void OnStartCB(int, void*);
	static void OnStepBackCB(int, void*);
	static void OnPlayCB(int, void*);
	static void OnPauseCB(int, void*);
	static void OnStepFwdCB(int, void*);
	static void OnEndCB(int, void*);

protected:
	bool m_StateChanged;
	bool m_Pause;
	bool m_AbsoluteJump;
	float m_StepJump;

	rsIconButton* m_pPauseButton;
};


class rsStaticImage
{
public:
	rsStaticImage(u32 width, u32 height, u8* data)
	{
		m_Image = grcImage::Create(width, height, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);

		u8* b = m_Image->GetBits();
        for(u32 i=m_Image->GetSize(); i>0; --i, b++, data++)
			*b = *data;
	}

	grcImage* GetImage(void)  { return m_Image; }

protected:
	grcImage* m_Image;
};

extern u8 s_FastForwardIconData[];
extern u8 s_PauseIconData[];
extern u8 s_PlayIconData[];
extern u8 s_RewindIconData[];
extern u8 s_SkipBackIconData[];
extern u8 s_SkipForwardIconData[];
extern u8 s_StopIconData[];


rsVcrControls::rsVcrControls(const char*)
{
	ResetState(false);

	rsStaticImage EndIcon(33, 35, s_SkipForwardIconData);
	rsStaticImage StepFwdIcon(40, 35, s_FastForwardIconData);
	rsStaticImage PauseIcon(35, 35, s_PauseIconData);
	rsStaticImage PlayIcon(33, 35, s_PlayIconData);
	rsStaticImage StepBackIcon(40, 35, s_RewindIconData);
	rsStaticImage StartIcon(33, 35, s_SkipBackIconData);

	int IconSize = 32;
	m_apChildren.Grow() = rage_new rsIconButton(IconSize * 5, 0, 0.f, IconSize, IconSize, OnEndCB, this, "End", EndIcon.GetImage());
	m_apChildren.Grow() = rage_new rsIconButton(IconSize * 4, 0, 0.f, IconSize, IconSize, OnStepFwdCB, this, "Step Fwd (use Shift/Ctrl to adjust speed)", StepFwdIcon.GetImage());
	m_apChildren.Grow() = m_pPauseButton = rage_new rsIconButton(IconSize * 3, 0, 0.f, IconSize, IconSize, OnPauseCB, this, "Pause", PauseIcon.GetImage());
	m_apChildren.Grow() = rage_new rsIconButton(IconSize * 2, 0, 0.f, IconSize, IconSize, OnPlayCB, this, "Play", PlayIcon.GetImage());
	m_apChildren.Grow() = rage_new rsIconButton(IconSize, 0, 0.f, IconSize, IconSize, OnStepBackCB, this, "Step Back (use Shift/Ctrl to adjust speed)", StepBackIcon.GetImage());
	m_apChildren.Grow() = rage_new rsIconButton(0, 0, 0.f, IconSize, IconSize, OnStartCB, this, "Start", StartIcon.GetImage());
}

void rsVcrControls::Update(int x, int y)
{
	m_pPauseButton->SetToggleOn(m_Pause);

	// keyboard shortcuts
	if(ioKeyboard::KeyPressed(KEY_P))
	{
		OnPause(true);
	}
	else if(ioKeyboard::KeyPressed(KEY_D))
	{
		OnRelativeStep(1.f);
	}
	else if(ioKeyboard::KeyPressed(KEY_S))
	{
		OnRelativeStep(-1.f);
	}

	rsContainer::Update(x, y);
}

void rsVcrControls::ResetState(bool bCurrentPause)
{
	m_Pause = bCurrentPause;
	m_StateChanged = false;
	m_AbsoluteJump = false;
	m_StepJump = 0.f;
}

bool rsVcrControls::HasStateChanged(bool& rOutPause, bool& rOutAbsoluteJump, float& rOutStepJump)
{
	rOutPause = m_Pause;
	rOutAbsoluteJump = m_AbsoluteJump;
	rOutStepJump = m_StepJump;

	return m_StateChanged;
}

void rsVcrControls::OnPause(bool pause)
{
	m_StateChanged = true;
	if(m_Pause && pause)
		m_Pause = false;
	else
		m_Pause = pause;
}

void rsVcrControls::OnRelativeStep(float step)
{
	m_StateChanged = true;
	m_AbsoluteJump = false;
	m_Pause = true;
	m_StepJump = step;
}

void rsVcrControls::OnAbsoluteJump(float jump)
{
	m_StateChanged = true;
	m_AbsoluteJump = true;
	m_StepJump = jump;
}

void rsVcrControls::OnStartCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsVcrControls* pVcr = static_cast<rsVcrControls*>(pData);
		pVcr->OnAbsoluteJump(0.f);
	}
}

void rsVcrControls::OnStepBackCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsVcrControls* pVcr = static_cast<rsVcrControls*>(pData);
		pVcr->OnRelativeStep(-1.f * (ioKeyboard::KeyDown(KEY_LCONTROL) ? 0.1f : (ioKeyboard::KeyDown(KEY_LSHIFT) ? 2.f : 1.f)));
	}
}

void rsVcrControls::OnPlayCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsVcrControls* pVcr = static_cast<rsVcrControls*>(pData);
		pVcr->OnPause(false);
	}
}

void rsVcrControls::OnPauseCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsVcrControls* pVcr = static_cast<rsVcrControls*>(pData);
		pVcr->OnPause(true);
	}
}

void rsVcrControls::OnStepFwdCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsVcrControls* pVcr = static_cast<rsVcrControls*>(pData);
		pVcr->OnRelativeStep(1.f * (ioKeyboard::KeyDown(KEY_LCONTROL) ? 0.1f : (ioKeyboard::KeyDown(KEY_LSHIFT) ? 2.f : 1.f)));
	}
}

void rsVcrControls::OnEndCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsVcrControls* pVcr = static_cast<rsVcrControls*>(pData);
		pVcr->OnAbsoluteJump(1.f);
	}
}


class rsAnimBank;

class rsAnimSlot : public rsContainer
{
public:
	rsAnimSlot(rsAnimBank* pBank);
	virtual ~rsAnimSlot(void);

	virtual void Update(int x, int y);

	void OnSelect(void);
	void OnLoad(void);
	void OnRefresh(void);
	void OnChangeRepeat(int change);

	void ForceDeselect(void);

	void LoadAnim(const char* pAnimFileName);

	static void OnSelectCB(int event, void* pData);
	static void OnLoadCB(int event, void* pData);
	static void OnRefreshCB(int event, void* pData);
	static void OnDecCB(int event, void* pData);
	static void OnIncCB(int event, void* pData);

	crAnimation* GetAnimation(void);
	int GetRepeat(void);
	float GetWhichFrame(void);

	bool IsSelected(void);

	void SetWhichFrame(float frame);

protected:
	rsAnimBank* m_pBank;
	crAnimation* m_pAnimation;
	int m_Repeat;
	float m_Frame;

	rsTextButton* m_pAnimButton;
	rsTextButton* m_pRepeatButton;
};


class rsAnimBank : public rsContainer
{
public:
	rsAnimBank(void);

	virtual void Update(int x, int y);

	void AddSlot(void);

	void NotifyLoad(rsAnimSlot* pSlot);
	void NotifySelect(rsAnimSlot* pSlot);

	void LoadDefaultAnim(const char* pAnimFileName);
	crAnimation* GetCurrentAnim(void);
	void AdvanceCurrentAnim(void);
	int GetCurrentAnimRepeat(void);
	rsAnimSlot* GetSelectedSlot(void);

protected:
	rsAnimSlot* m_pSelectedSlot;
};


rsAnimSlot::rsAnimSlot(rsAnimBank* pBank)
{
	m_pAnimation = 0;
	m_pBank = pBank;
	m_Repeat = 1;
	m_Frame = 0.f;

	m_apChildren.Grow() = m_pAnimButton = rage_new rsTextButton(0, 0, 0.f, 400, DEFAULT_BUTTON_HEIGHT, OnSelectCB, this, "Click \"Open\" to load an animation...", "[NONE]", rsTextButton::AlignLeft);
	m_apChildren.Grow() = rage_new rsTextButton(410, 0, 0.f, 50, DEFAULT_BUTTON_HEIGHT, OnLoadCB, this, "Open an animation", "Open", rsTextButton::AlignCenter);
	m_apChildren.Grow() = rage_new rsTextButton(470, 0, 0.f, 70, DEFAULT_BUTTON_HEIGHT, OnRefreshCB, this, "Refresh (reload) an animation", "Refresh", rsTextButton::AlignCenter);
	m_apChildren.Grow() = rage_new rsTextButton(550, 0, 0.f, 20, DEFAULT_BUTTON_HEIGHT, OnDecCB, this, "Decrement repeat count", "-", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pRepeatButton = rage_new rsTextButton(580, 0, 0.f, 30, DEFAULT_BUTTON_HEIGHT, 0, 0, "Repeat count (used in Sequence mode)", "1", rsTextButton::AlignCenter);
	m_apChildren.Grow() = rage_new rsTextButton(620, 0, 0.f, 20, DEFAULT_BUTTON_HEIGHT, OnIncCB, this, "Increment repeat count", "+", rsTextButton::AlignCenter);
}

rsAnimSlot::~rsAnimSlot(void)
{
	if(m_pAnimation)
	{
		delete m_pAnimation;
	}
}

void rsAnimSlot::Update(int x, int y)
{
	if(IsSelected())
	{
		if(ioKeyboard::KeyPressed(KEY_O))
		{
			OnLoad();
		}
		else if(ioKeyboard::KeyPressed(KEY_A))
		{
			OnChangeRepeat(1);
		}
		else if(ioKeyboard::KeyPressed(KEY_Z))
		{
			OnChangeRepeat(-1);
		}
	}

	rsContainer::Update(x, y);
}

void rsAnimSlot::OnSelect(void)
{
	if(m_pAnimation)
	{
		m_pAnimButton->SetHighlight(true);
		m_pBank->NotifySelect(this);
	}
}

void rsAnimSlot::OnLoad(void)
{
	const char* pAnimFileName = 0;
#if __BANK
	pAnimFileName = BANKMGR.OpenFile("*.anim", false, "Animation (*.anim)");
#endif
	if (!pAnimFileName)
		return;
	LoadAnim(pAnimFileName);
}

void rsAnimSlot::OnRefresh(void)
{
	crAnimation* oldAnim = m_pAnimation;

	if(oldAnim)
	{
		m_pAnimation = crAnimation::AllocateAndLoad(oldAnim->GetName());

		if(m_pAnimation)
		{
			delete oldAnim;

			m_pBank->NotifyLoad(this);
		}
		else
		{
			m_pAnimation = oldAnim;
		}

		// select the loaded animation
		OnSelect();
	}
}

void rsAnimSlot::LoadAnim(const char* pAnimFileName)
{
	delete m_pAnimation;
	m_pAnimation = crAnimation::AllocateAndLoad(pAnimFileName);
	if(m_pAnimation)
	{
		m_pBank->NotifyLoad(this);

		// select the loaded animation
		OnSelect();

		static_cast<rsTextButton*>(m_apChildren[0])->SetText(fiAssetManager::FileName(m_pAnimation->GetName()));
		static_cast<rsTextButton*>(m_apChildren[0])->SetToolTip(pAnimFileName);
	}
}

void rsAnimSlot::OnChangeRepeat(int change)
{
	m_Repeat = Clamp(m_Repeat + change, 1, 9);

	char sRepeatNum[8];
	sprintf(sRepeatNum, "%d", m_Repeat);
	m_pRepeatButton->SetText(sRepeatNum);
}

void rsAnimSlot::ForceDeselect(void)
{
	m_pAnimButton->SetHighlight(false);
}

crAnimation* rsAnimSlot::GetAnimation(void)
{
	return m_pAnimation;
}

int rsAnimSlot::GetRepeat(void)
{
	return m_Repeat;
}

float rsAnimSlot::GetWhichFrame(void)
{
	return m_Frame;
}

void rsAnimSlot::SetWhichFrame(float frame)
{
	m_Frame = frame;
}

bool rsAnimSlot::IsSelected(void)
{
	return m_pBank->GetSelectedSlot() == this;
}

void rsAnimSlot::OnSelectCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsAnimSlot* pSlot = static_cast<rsAnimSlot*>(pData);
		pSlot->OnSelect();
	}
}

void rsAnimSlot::OnLoadCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsAnimSlot* pSlot = static_cast<rsAnimSlot*>(pData);
		pSlot->OnLoad();
	}
}

void rsAnimSlot::OnRefreshCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsAnimSlot* pSlot = static_cast<rsAnimSlot*>(pData);
		pSlot->OnRefresh();
	}
}

void rsAnimSlot::OnDecCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsAnimSlot* pSlot = static_cast<rsAnimSlot*>(pData);
		pSlot->OnChangeRepeat(-1);
	}
}

void rsAnimSlot::OnIncCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsAnimSlot* pSlot = static_cast<rsAnimSlot*>(pData);
		pSlot->OnChangeRepeat(1);
	}
}

rsAnimBank::rsAnimBank(void) :
	m_pSelectedSlot(0)
{
	AddSlot();
}

void rsAnimBank::Update(int x, int y)
{
	// keyboard shortcuts
	int selected = -1;
	for(int i=0; i<MAX_ANIM_SLOTS; i++)
	{
		if(ioKeyboard::KeyPressed(KEY_1+i))
		{
			selected = i;
			break;
		}
	}
	if(selected >= 0 && selected < m_apChildren.GetCount())
	{
		rsAnimSlot* pSlot = static_cast<rsAnimSlot*>(m_apChildren[selected]);
		if(pSlot->GetAnimation())
			m_pSelectedSlot = pSlot;
		m_pSelectedSlot->OnSelect();
	}

	rsContainer::Update(x, y);
}

void rsAnimBank::AddSlot(void)
{
	m_apChildren.Grow() = rage_new rsAnimSlot(this);

	int nChildren = m_apChildren.GetCount();
	m_apChildren[nChildren-1]->SetPosition(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*(nChildren-1), 0.f);
}

void rsAnimBank::NotifySelect(rsAnimSlot* pSlot)
{
	m_pSelectedSlot = pSlot;
	for(int i=0; i<m_apChildren.GetCount(); i++)
	{
		if(m_apChildren[i] != pSlot)
		{
			static_cast<rsAnimSlot*>(m_apChildren[i])->ForceDeselect();
		}
	}
}

void rsAnimBank::NotifyLoad(rsAnimSlot* pSlot)
{
	// If last child has successfully loaded, add a new child
	if(pSlot == m_apChildren[m_apChildren.GetCount()-1] && m_apChildren.GetCount() < MAX_ANIM_SLOTS)
	{
		AddSlot();
	}
}

void rsAnimBank::LoadDefaultAnim(const char* pAnimFileName)
{
	for(int i=0; i<m_apChildren.GetCount(); i++)
	{
		rsAnimSlot* slot = static_cast<rsAnimSlot*>(m_apChildren[i]);
		if(!slot->GetAnimation())
		{
			slot->LoadAnim(pAnimFileName);
			return;
		}
	}
}

crAnimation* rsAnimBank::GetCurrentAnim(void)
{
	if(m_pSelectedSlot)
	{
		return m_pSelectedSlot->GetAnimation();
	}
	return 0;
}

rsAnimSlot* rsAnimBank::GetSelectedSlot(void)
{
	return m_pSelectedSlot;
}

void rsAnimBank::AdvanceCurrentAnim(void)
{
	if(m_pSelectedSlot)
	{
		for(int i=0; i<m_apChildren.GetCount(); i++)
		{
			if(m_apChildren[i] == m_pSelectedSlot)
			{
				m_pSelectedSlot = static_cast<rsAnimSlot*>(m_apChildren[(i+1)%m_apChildren.GetCount()]);
				if(!m_pSelectedSlot->GetAnimation())
					m_pSelectedSlot = static_cast<rsAnimSlot*>(m_apChildren[0]);
				m_pSelectedSlot->OnSelect();
				break;
			}
		}
	}
}

int rsAnimBank::GetCurrentAnimRepeat(void)
{
	if(m_pSelectedSlot)
	{
		return m_pSelectedSlot->GetRepeat();
	}
	return 1;
}


class rsMiscControls : public rsContainer
{
public:
	rsMiscControls(void);

	virtual void Update(int x, int y);

	void ResetState(bool looping, bool sequence, bool blending, bool mover, bool noroot, bool boneDraw,bool wireframe);
	bool HasStateChanged(bool& rLooping, bool& rSequence, bool& rBlending, bool& rMover, bool& rNoRoot, bool& rBoneDraw, rmcDrawable*& rpDrawable,bool& wireframe);

	void OnToggleLooping(void);
	void OnToggleWireframe(void);
	void OnToggleSequence(void);
	void OnToggleBlending(void);
	void OnToggleMover(void);
	void OnToggleNoRoot(void);
	void OnToggleBoneDraw(void);
	void OnEntityLoad(void);

	static void OnToggleWireframeCB(int event, void* pData);
	static void OnToggleLoopingCB(int event, void* pData);
	static void OnToggleSequenceCB(int event, void* pData);
	static void OnToggleBlendingCB(int event, void* pData);
	static void OnToggleMoverCB(int event, void* pData);
	static void OnToggleNoRootCB(int event, void* pData);
	static void OnToggleBoneDrawCB(int event, void* pData);
	static void OnEntityLoadCB(int event, void* pData);

protected:
	bool m_HasStateChanged;
	bool m_Wireframe;
	bool m_Looping;
	bool m_Sequence;
	bool m_Blending;
	bool m_Mover;
	bool m_NoRoot;
	bool m_BoneDraw;
	rmcDrawable* m_pDrawable;
	
	rsTextButton* m_pWireframeButton;
	rsTextButton* m_pLoopingButton;
	rsTextButton* m_pSequenceButton;
	rsTextButton* m_pBlendingButton;
	rsTextButton* m_pMoverButton;
	rsTextButton* m_pNoRootButton;
	rsTextButton* m_pBoneDrawButton;
};


rsMiscControls::rsMiscControls(void)
{
	ResetState(false, false, false, false, false, false, false);

	m_apChildren.Grow() = m_pLoopingButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*1, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleLoopingCB, this, "Toggle Looping", "Looping", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pSequenceButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*2, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleSequenceCB, this, "Toggle Sequence Playing", "Seq Play", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pBlendingButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*3, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleBlendingCB, this, "Toggle Blending", "Blending", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pMoverButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*4, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleMoverCB, this, "Toggle Mover Channel", "Mover", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pNoRootButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*5, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleNoRootCB, this, "Toggle No Root Channel", "No Root", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pBoneDrawButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*6, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleBoneDrawCB, this, "Toggle Bone Drawing", "Bone Draw", rsTextButton::AlignCenter);
	m_apChildren.Grow() = m_pWireframeButton = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*7, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnToggleWireframeCB, this, "Toggle Wireframe", "Wireframe", rsTextButton::AlignCenter);
	m_apChildren.Grow() = rage_new rsTextButton(0, (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*8, 0.f, 150, DEFAULT_BUTTON_HEIGHT, OnEntityLoadCB, this, "Load Entity", "Load Entity", rsTextButton::AlignCenter);
	// TODO load save settings
}

void rsMiscControls::Update(int x, int y)
{
	m_pLoopingButton->SetToggleOn(m_Looping);
	m_pWireframeButton->SetToggleOn(m_Wireframe);
	m_pSequenceButton->SetToggleOn(m_Sequence);
	m_pBlendingButton->SetToggleOn(m_Blending);
	m_pMoverButton->SetToggleOn(m_Mover);
	m_pNoRootButton->SetToggleOn(m_NoRoot);
	m_pBoneDrawButton->SetToggleOn(m_BoneDraw);

	if(ioKeyboard::KeyPressed(KEY_L))
	{
		OnToggleLooping();
	}
	else if(ioKeyboard::KeyPressed(KEY_E))
	{
		OnEntityLoad();
	}

	rsContainer::Update(x, y);
}

void rsMiscControls::ResetState(bool looping, bool sequence, bool blending, bool mover, bool noroot, bool boneDraw,bool wireframe)
{
	m_HasStateChanged = false;

	m_Wireframe = wireframe;
	m_Looping = looping;
	m_Sequence = sequence;
	m_Blending = blending;
	m_Mover = mover;
	m_NoRoot = noroot;
	m_BoneDraw = boneDraw;

	m_pDrawable = 0;
}

bool rsMiscControls::HasStateChanged(bool& rLooping, bool& rSequence, bool& rBlending, bool& rMover, bool& rNoRoot, bool& rBoneDraw, rmcDrawable*& rpDrawable,bool& rWireframe)
{
	rWireframe = m_Wireframe;
	rLooping = m_Looping;
	rSequence = m_Sequence;
	rBlending = m_Blending;
	rMover = m_Mover;
	rNoRoot = m_NoRoot;
	rBoneDraw = m_BoneDraw;
	rpDrawable = m_pDrawable;

	return m_HasStateChanged;
}

void rsMiscControls::OnToggleWireframe(void)
{
	m_HasStateChanged = true;
	m_Wireframe = !m_Wireframe;
}

void rsMiscControls::OnToggleLooping(void)
{
	m_HasStateChanged = true;
	m_Looping = !m_Looping;
}

void rsMiscControls::OnToggleSequence(void)
{
	m_HasStateChanged = true;
	m_Sequence = !m_Sequence;
}

void rsMiscControls::OnToggleBlending(void)
{
	m_HasStateChanged = true;
	m_Blending = !m_Blending;
}

void rsMiscControls::OnToggleMover(void)
{
	m_HasStateChanged = true;
	m_Mover = !m_Mover;
}

void rsMiscControls::OnToggleNoRoot(void)
{
	m_HasStateChanged = true;
	m_NoRoot = !m_NoRoot;
}

void rsMiscControls::OnToggleBoneDraw(void)
{
	m_HasStateChanged = true;
	m_BoneDraw = !m_BoneDraw;
}

void rsMiscControls::OnEntityLoad(void)
{
    const char* pEntityFileName = 0;
#if __BANK
	pEntityFileName = BANKMGR.OpenFile("*.type", false, "Entity Type (*.type)");
#endif
	if (!pEntityFileName)
		return;

	char buf[255];
	ASSET.RemoveNameFromPath(buf, 255, pEntityFileName);
	ASSET.PushFolder(buf);

	m_pDrawable = rage_new rmcDrawable();
	bool success = m_pDrawable->Load(pEntityFileName);

	ASSET.PopFolder();

	if(!success)
	{
		delete m_pDrawable;
	}
	else
	{
		m_HasStateChanged = true;
	}
}

void rsMiscControls::OnToggleWireframeCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleWireframe();
	}
}

void rsMiscControls::OnToggleLoopingCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleLooping();
	}
}

void rsMiscControls::OnToggleSequenceCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleSequence();
	}
}

void rsMiscControls::OnToggleBlendingCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleBlending();
	}
}

void rsMiscControls::OnToggleMoverCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleMover();
	}
}

void rsMiscControls::OnToggleNoRootCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleNoRoot();
	}
}

void rsMiscControls::OnToggleBoneDrawCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnToggleBoneDraw();
	}
}

void rsMiscControls::OnEntityLoadCB(int event, void* pData)
{
	if(event == rsWindow::EventMouseClick)
	{
		rsMiscControls* pControls = static_cast<rsMiscControls*>(pData);
		pControls->OnEntityLoad();
	}
}


#define DEFAULT_ANIM "Gent_nor_hnd_for_wlk"
PARAM(anim,"[viewer_anim] Animation file to load (default is \"" DEFAULT_ANIM "\")");

class craViewerAnimation : public ragesamples::craSampleManager
{
public:
	craViewerAnimation() : ragesamples::craSampleManager(),
		m_Animation(0),
//		m_Drawable(0),
//		m_Skeleton(0),
		m_Frame(0),
		m_IsLooping(true),
		m_IsSequencePlayback(false),
		m_IsMoverChannel(false),
		m_IsNoRoot(false),
		m_Wireframe(false),
		m_SequenceCounter(0)
	{
	}

	virtual void Init(const char* path)
	{
		craSampleManager::Init(path);
		grcLightGroup* lightgroup = GetLightGroup();
		Assert(lightgroup);
		lightgroup->SetAmbient(1.0f,1.0f,1.0f,1.0f);//default to full ambient
	}

protected:

	void InitClient()
	{
		rmcSampleManager::InitClient();

		EnableCameraDraw(false);
		// EnableLogoDraw(false);
		EnableDrawHelp(false);
		EnableWorldAxesDraw(false);

		m_BlendFrame = true; 
		m_FrameTime = 0.f;

		// STEP #1. Allocate and load our drawable instance.

		//-- The first thing we do is allocate our <c>rmcDrawable</c> instance.
		m_Drawable = rage_new rmcDrawable();

		//-- Now we load the drawable.  
		if( !m_Drawable->Load(GetFileName(0)) )
			Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );

		// STEP #2. Initialize our skeleton.
		InitSkeleton();

		InitUI();
	}

	void ShutdownClient()
	{
//		delete m_Skeleton;
//		delete m_Drawable;
//		delete m_Animation;  // destruction now managed by AnimSlots
		delete m_Frame;

		ShutdownUI();

		ragesamples::craSampleManager::ShutdownClient();
	}

	void UpdateCamera()
	{
		if(g_DisableCamera)
		{
			if(ioMouse::GetButtons() & ioMouse::MOUSE_LEFT)
			{
				return;
			}
			
			g_DisableCamera = false;
		}
		
		craSampleManager::UpdateCamera();
	}

	void UpdateClient()
	{	
		if(m_Drawable->GetSkeletonData() && m_Animation != NULL )
		{	
			if (m_Animation)
			{
				float frameTime = m_BlendFrame ? m_FrameTime : floor(m_FrameTime+0.001f);
				float realTime = m_Animation->Convert30FrameToTime(frameTime);

				// NB this is a v.safe way of dealing with unexpected data,
				// not a good example to follow in a game
				// zero all data in frame, set bones to have identity values from skel, composite without clearing missing dofs.
				m_Frame->Zero();
				m_Frame->IdentityFromSkel(m_Skeleton->GetSkeletonData());

				m_Animation->CompositeFrame(realTime, *m_Frame, !m_BlendFrame, false);

				m_MoverPosition.Identity();
				if(m_IsMoverChannel)
				{
					// TODO --- if sequence playback, build mover offset for all steps in sequence up til now (including all repeats)

					if(m_Animation->HasMoverTracks())
					{
						crFrame TempFrame;
						TempFrame.InitCreateMoverDofs();

						m_Animation->CompositeFrameWithMover(realTime, realTime, TempFrame, !m_BlendFrame);
						TransformV trans;
						TempFrame.GetMoverSituation(trans);
						Mat34VFromTransformV(RC_MAT34V(m_MoverPosition), trans);
					}
				}

				if(m_IsNoRoot)
				{
					m_Frame->SetBoneTranslation(0, Vec3V(V_ZERO));
				}

				m_Skeleton->SetParentMtx(reinterpret_cast<Mat34V*>(&m_MoverPosition));

				// STEP #6. Pose the skeleton.

				//-- Now we take the frame data and pose the bones in the skeleton.  To do this, just pass the 
				// a reference to the skeleton to be posed.
				m_Frame->Pose(*m_Skeleton);

				// -STOP
			}
			else
			{
				m_Skeleton->Reset();
			}


			// STEP #7. Compute the global matrices of the skeleton bones.

			//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
			m_Skeleton->Update();
			UpdateMatrixSet();

			// -STOP

			if (GetPlay())
			{
				// STEP #8. Goto the next frame.

				//-- Now we goto the next frame.  We do this by multiplying the game clock with the playback rate.
				m_FrameTime = m_FrameTime+TIME.GetSeconds()*GetPlaybackRate()*30.f;

				// -STOP

				if(m_FrameTime >= (m_Animation->GetNum30Frames()-1))
				{	
					if(m_IsSequencePlayback)
					{
						m_SequenceCounter++;
						if(m_SequenceCounter > (m_AnimBank->GetCurrentAnimRepeat()-1))
						{
							m_AnimBank->AdvanceCurrentAnim();
							CheckForAnimChange();
						}
						m_FrameTime = 0.f;
					}
					else if(m_IsLooping)
					{
						m_FrameTime = 0.f;
					}
					else
					{
						m_FrameTime = float(m_Animation->GetNum30Frames()-1);
					}
				}
			}
		}

		UpdateUI();
	}

	bool ClientWantExit(void) const
	{
		return ioKeyboard::KeyPressed(KEY_ESCAPE) ? true : false;
	}

	void DrawDrawable(const Matrix34& center,u8 lod)
	{
		for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
			if (GetBucketEnable(bucket)) {
				if (m_Drawable->GetSkeletonData())
				{
					// STEP #9. Render the skinned model.

					//-- Lastly, we render the skinned model with its posed skeleton.
					m_Drawable->DrawSkinned(center,*m_MatrixSet,bucket,lod);

					// -STOP
				}
				else
					m_Drawable->Draw(center,bucket,lod);
			}
	}

	void DrawClient()
	{
		const Matrix34& center = RCC_MATRIX34(*m_Skeleton->GetParentMtx());

		if (GetDrawSkin())
		{
			u8 lod;
			if (m_Drawable->IsVisible(center,*grcViewport::GetCurrent(),lod)) {
				float zDist = GetCamMgr().GetWorldMtx().d.Mag();
				grcTextureFactory::GetInstance().SetTextureLod(zDist);

				DrawDrawable(center,lod);
				if (m_Wireframe)
				{
					// use the depth bias trick:
					float nearClip = grcViewport::GetCurrent()->GetNearClip();
					float farClip = grcViewport::GetCurrent()->GetFarClip();
					grcViewport::GetCurrent()->SetNearClip(nearClip + 0.001f);
					grcViewport::GetCurrent()->SetFarClip(farClip + 0.001f);

					// show wireframe:
					grcStateBlock::SetWireframeOverride(true);

					DrawDrawable(center,lod);
					
					// restore old state:
					grcStateBlock::SetWireframeOverride(false);
					grcViewport::GetCurrent()->SetNearClip(nearClip);
					grcViewport::GetCurrent()->SetFarClip(farClip);
				}
			}
		}

		if (GetDrawBones())
		{
			DrawBones(*m_Skeleton);
		}

		if (GetDrawJoints())
		{
			DrawJoints(*m_Skeleton);
		}

		DrawUI();
	}

	void InitUI(void)
	{
		int border = 10;
		int width = GRCDEVICE.GetWidth();
		int height = GRCDEVICE.GetHeight();

		m_VcrControls = rage_new rsVcrControls(NULL);
		m_AnimBank = rage_new rsAnimBank();
#ifdef MOVE_SCRUBBER
		m_AnimScrubber = rage_new rsScrubber(0, 0, 0.f, (width - (border*2)) / 2, DEFAULT_BUTTON_HEIGHT*2, 0, 0, 0, 0.f, 0.f, 0.f);
#else // MOVE_SCRUBBER
		m_AnimScrubber = rage_new rsScrubber(0, 0, 0.f, (width - (border*2)), DEFAULT_BUTTON_HEIGHT*2, 0, 0, 0, 0.f, 0.f, 0.f);
#endif // MOVE_SCRUBBER
		m_MiscControls = rage_new rsMiscControls();

		m_AnimBankX = border;
		m_AnimBankY = height - (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*MAX_ANIM_SLOTS;

#ifdef MOVE_SCRUBBER
		m_AnimScrubberX = width / 2;
#else // MOVE_SCRUBBER
		m_AnimScrubberX = border;
#endif // MOVE_SCRUBBER
		m_AnimScrubberY = m_AnimBankY - DEFAULT_BUTTON_HEIGHT*2 - DEFAULT_BUTTON_PAD;

		m_VcrControlsX = border;
		m_VcrControlsY = m_AnimScrubberY - 32 - DEFAULT_BUTTON_PAD;

		m_MiscControlsX = width - border - 150;
		m_MiscControlsY = height - (DEFAULT_BUTTON_HEIGHT+DEFAULT_BUTTON_PAD)*(m_MiscControls->GetChildCount()+1);

		// load default animation(s)
		const char* animNames[MAX_ANIM_SLOTS];
		const int ANIM_NAME_BUF_SIZE = 8192;
		char animNameBuf[ANIM_NAME_BUF_SIZE];
		int numAnimNames = PARAM_anim.GetArray(animNames, MAX_ANIM_SLOTS, animNameBuf, ANIM_NAME_BUF_SIZE);
		if(numAnimNames > 0)
		{
			for(int i=0; i<numAnimNames; i++)
			{
				m_AnimBank->LoadDefaultAnim(animNames[i]);
			}
		}
		else
		{
			m_AnimBank->LoadDefaultAnim(DEFAULT_ANIM);
		}

		CheckForAnimChange();
	}

	void ShutdownUI(void)
	{
		delete m_VcrControls;
		delete m_AnimBank;
		delete m_AnimScrubber;
		delete m_MiscControls;
	}

	void UpdateUI(void)
	{
		// anim bank 
		m_AnimBank->Update(m_AnimBankX, m_AnimBankY);

		CheckForAnimChange();

		// anim scrubber
		char sScrubberTip[256];
		sScrubberTip[0] = '\0';
		m_AnimScrubber->SetProgress(m_FrameTime / float(m_Animation->GetNum30Frames()-1));
		m_AnimScrubber->SetMarks(1.f / (m_Animation->GetNum30Frames()-1.f), 30.f / (m_Animation->GetNum30Frames()-1.f));
		sprintf(sScrubberTip, "%s: %d/%d (%.2f sec %.1f%%)", 
			fiAssetManager::FileName(m_Animation->GetName()), 
			int(m_FrameTime), 
			int(m_Animation->GetNum30Frames()), 
			m_FrameTime / 30.f, 
			m_FrameTime * 100.f / (m_Animation->GetNum30Frames()-1.f));
		
		m_AnimScrubber->SetToolTip(sScrubberTip);

		m_AnimScrubber->Update(m_AnimScrubberX, m_AnimScrubberY);

		if(m_AnimScrubber->IsMouseDown())
		{
			SetPlay(false);
			m_FrameTime = float(m_Animation->GetNum30Frames()-1) * m_AnimScrubber->GetProgress();
		}

		// vcr controls
		m_VcrControls->ResetState(!GetPlay());

		m_VcrControls->Update(m_VcrControlsX, m_VcrControlsY);

		bool pause;
		bool absoluteJump;
		float jumpStep;
		if(m_VcrControls->HasStateChanged(pause, absoluteJump, jumpStep))
		{
			SetPlay(!pause);
			if(absoluteJump)
			{
				// absolute jump
				m_FrameTime = float(m_Animation->GetNum30Frames()-1) * jumpStep;
			}
			else if(jumpStep != 0.f)
			{
				// relative step
				m_FrameTime = Clamp(m_FrameTime + jumpStep, 0.f, float(m_Animation->GetNum30Frames()-1));
			}
		}

		// misc controls
        m_MiscControls->ResetState(m_IsLooping, m_IsSequencePlayback, m_BlendFrame, m_IsMoverChannel, m_IsNoRoot, GetDrawBones(), m_Wireframe);
		if(!m_IsSequencePlayback)
			m_SequenceCounter = 0;

		m_MiscControls->Update(m_MiscControlsX, m_MiscControlsY);
		
		bool IsBlendFrame;
		bool IsBoneDraw;
		rmcDrawable* pDrawable;
		if(m_MiscControls->HasStateChanged(m_IsLooping, m_IsSequencePlayback, IsBlendFrame, m_IsMoverChannel, m_IsNoRoot, IsBoneDraw, pDrawable,m_Wireframe))
		{
			if(pDrawable)
			{
				if(m_Drawable)
					delete m_Drawable;
				m_Drawable = pDrawable;
				InitSkeleton();
			}
			m_BlendFrame = IsBlendFrame;
			SetDrawBones(IsBoneDraw);
			SetDrawJoints(IsBoneDraw);
			SetDrawSkin(!IsBoneDraw);
		}

		if(m_AnimBank->GetSelectedSlot())
			m_AnimBank->GetSelectedSlot()->SetWhichFrame(m_FrameTime);
	}

	void DrawUI(void)
	{
		grcBindTexture(NULL);

		grcLightState::SetEnabled(false);

		grcViewport* pVP = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcViewport* pCurVP = grcViewport::GetCurrent();
		if(pCurVP)
			pCurVP->Ortho(0.f, (float)pCurVP->GetWidth(), (float)pCurVP->GetHeight(), 0.f, -1.f, 1.f);

		m_VcrControls->Draw(m_VcrControlsX, m_VcrControlsY);
		m_AnimScrubber->Draw(m_AnimScrubberX, m_AnimScrubberY);
		m_MiscControls->Draw(m_MiscControlsX, m_MiscControlsY);
		m_AnimBank->Draw(m_AnimBankX, m_AnimBankY);

		// mover channel text
		char sMoverText[512];
		sMoverText[0] = '\0';
		if(m_IsMoverChannel)
		{
			Vector3 rotEulers = m_MoverPosition.GetEulers();
			sprintf(sMoverText, "MOVER (x%.3f y%.3f z%.3f, p%.3f r%.3f y%.3f)", 
				m_MoverPosition.d.x, m_MoverPosition.d.y, m_MoverPosition.d.z,
				rotEulers.x*RtoD, rotEulers.y*RtoD, rotEulers.z*RtoD);
		}

		char sStateText[255];
		sStateText[0] = '\0';
		if(m_Animation->IsLooped())
			strcat(sStateText, "looped ");
		if(m_Animation->HasMoverTracks())
			strcat(sStateText, "mover ");

		char sAnimName[255];
		strcpy(sAnimName, fiAssetManager::FileName(m_Animation->GetName()));

		// on screen text
		char sOnScreenText[2048];
		sprintf(sOnScreenText, "%s : %s\n\nFrame %d/%d (%.2f sec %.1f%%)\n\n%s %s %s", 
			sAnimName, sStateText,
			int(m_FrameTime), 
			int(m_Animation->GetNum30Frames()), 
			m_FrameTime / 30.f, m_FrameTime * 100.f / (m_Animation->GetNum30Frames()-1.f), 
			(GetPlay() ? "PLAY" : "PAUSE"), 
			(m_IsSequencePlayback ? "SEQUENCE" : (m_IsLooping ? "LOOP" : "")), 
			sMoverText);

		// TEMP - trying to render a string > ~170 characters causes a crash!
		sOnScreenText[168] = '\0';

		grcFont::GetCurrent().DrawScaled(10.f, 10.f, 0.f, Color32(ONSCREEN_TEXT_COLOR), 1.f, 1.f, sOnScreenText);

		grcViewport::SetCurrent(pVP);

		// draw mover channel marker
		if(m_IsMoverChannel)
		{
			for(int isMover=0; isMover<2; isMover++)
			{
				grcBegin(drawLines, 12);
				grcWorldIdentity();
				Matrix34 m;
				if(isMover)
				{
					grcColor3f(0.f, 1.0f, 0.f);
					m = m_MoverPosition;
				}
				else
				{
					grcColor3f(0.f, 0.25f, 0.f);
					m.Identity();
				}

				grcVertex3f(m.d);
				grcVertex3f(1.f, 0.f, 1.f);
				grcVertex3f(m.d);
				grcVertex3f(-1.f, 0.f, 1.f);
				grcVertex3f(m.d);
				grcVertex3f(-1.f ,0.f ,-1.0f);
				grcVertex3f(m.d);
				grcVertex3f(1.f, 0.f, -1.f);
				Vector3 To(0.f, 0.f, 1.f);
				m.Transform(To);
				grcVertex3f(m.d);
				grcVertex3f(To);
				Vector3 Up(0.f, 0.25f, 0.75f);
				m.Transform(Up);
				grcVertex3f(To);
				grcVertex3f(Up);
				grcEnd();
			}
		}
	}

	const crAnimation* GetAnimation() const
	{ 
		return m_Animation;
	}

	void CheckForAnimChange(void)
	{
		if(m_Animation != m_AnimBank->GetCurrentAnim())
		{
			m_Animation = m_AnimBank->GetCurrentAnim();
#if __DEV
			Printf("animation '%s' size = %d\n", m_Animation->GetName(), m_Animation->ComputeSize());
#endif
			if(m_Frame)
				delete m_Frame;
			m_Frame = rage_new crFrame;
			if(m_Animation)
			{
				m_Frame->InitCreateBoneAndMoverDofs(m_Skeleton->GetSkeletonData(), false);
				m_FrameTime = Clamp(m_AnimBank->GetSelectedSlot()->GetWhichFrame(), 0.f, m_Animation->GetNum30Frames()-1.f);
			}
			else
			{
				m_FrameTime = 0.f;
			}
			m_SequenceCounter = 0;
		}		
	}

	void InitSkeleton(void)
	{
		// The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		crSkeletonData *skeldata = m_Drawable->GetSkeletonData();
		if (m_MatrixSet)
		{
			delete m_MatrixSet;
		}
		if(m_Skeleton)
		{
			delete m_Skeleton;
		}
		m_Skeleton = rage_new crSkeleton();
		if (skeldata)
			m_Skeleton->Init(*skeldata, NULL);

		m_MatrixSet = grmMatrixSet::Create(*m_Skeleton);
	}

private:
	crAnimation* m_Animation;
//	rmcDrawable* m_Drawable;
//	crSkeleton* m_Skeleton;
	crFrame* m_Frame;

	bool m_IsLooping;
	bool m_IsSequencePlayback;
	bool m_IsMoverChannel;
	bool m_IsNoRoot;
	bool m_Wireframe;

	Matrix34 m_MoverPosition;

	int m_SequenceCounter;

	bool m_BlendFrame;
	float m_FrameTime;

	// on screen user interface
	rsVcrControls* m_VcrControls;
	rsAnimBank* m_AnimBank;
	rsScrubber* m_AnimScrubber;
	rsMiscControls* m_MiscControls;

	int m_VcrControlsX;
	int m_VcrControlsY;

	int m_AnimBankX;
	int m_AnimBankY;

	int m_AnimScrubberX;
	int m_AnimScrubberY;

	int m_MiscControlsX;
	int m_MiscControlsY;

};


// main application
int Main()
{
	craViewerAnimation sampleCra;

	sampleCra.Init("sample_cranimation\\NewSkel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}

u8 s_FastForwardIconData[] = 
// 40 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,90,0,0,0,38,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,54,0,0,0,54,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,75,26,8,8,255,
	8,3,3,247,0,0,0,118,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,17,12,4,4,243,27,10,10,255,0,0,0,155,0,0,0,14,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,70,22,22,255,161,52,52,255,43,14,14,255,
	0,0,0,200,0,0,0,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,35,26,10,10,255,196,75,75,255,86,33,33,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,70,22,22,255,188,61,61,255,189,63,63,255,103,34,34,255,5,2,2,247,
	0,0,0,118,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,35,26,10,10,255,207,79,79,255,208,81,81,255,156,62,62,255,27,11,11,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,70,22,22,255,189,61,61,255,198,87,87,255,192,66,66,255,161,55,55,255,38,13,13,255,0,0,0,200,
	0,0,0,40,0,0,0,0,0,0,0,35,26,10,10,255,207,79,79,255,215,104,104,255,211,84,84,255,202,81,81,255,89,36,36,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,70,23,23,255,189,62,62,255,210,119,119,255,207,109,109,255,198,77,77,255,193,68,68,254,96,35,35,255,3,1,1,247,0,0,0,118,
	0,0,0,38,26,10,10,255,207,80,80,255,222,132,132,255,223,129,129,255,216,96,96,255,215,87,87,255,162,66,66,255,28,12,12,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,92,71,23,23,255,189,62,62,255,210,120,120,255,211,122,122,255,212,122,122,255,206,97,97,255,199,72,72,255,158,58,58,255,32,12,12,255,0,0,0,217,26,10,10,255,
	208,80,80,255,223,132,132,255,225,140,140,255,227,141,141,255,222,116,116,255,217,90,90,255,209,87,87,255,92,39,39,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,92,71,23,23,255,190,62,62,255,211,120,120,255,212,122,122,255,213,124,124,255,215,127,127,255,213,119,119,255,205,86,86,255,197,72,72,254,90,34,34,255,27,10,10,255,208,81,81,255,223,132,132,255,
	226,140,140,255,228,142,142,255,229,144,144,255,228,136,136,255,221,102,102,255,221,93,93,255,166,71,71,255,29,13,13,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,71,23,23,255,
	190,63,63,255,211,121,121,255,212,122,122,255,214,124,124,255,215,127,127,255,216,129,129,255,218,130,130,255,212,108,108,255,206,80,80,255,170,65,65,255,209,81,81,255,223,132,132,255,226,141,141,255,228,143,143,255,
	229,144,144,255,231,146,146,255,232,147,147,255,227,122,122,255,223,97,97,255,214,92,92,255,94,41,41,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,71,23,23,255,191,63,63,255,211,122,122,255,
	212,123,123,255,214,125,125,255,215,127,127,255,217,129,129,255,219,131,131,255,220,133,133,255,219,128,128,255,212,96,96,255,209,81,81,255,224,132,132,255,227,142,142,255,228,144,144,255,229,145,145,255,231,147,147,255,
	232,148,148,255,234,150,150,255,233,142,142,255,228,108,108,255,227,100,100,255,171,75,75,255,30,13,13,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,71,23,23,255,191,64,64,255,211,122,122,255,213,124,124,255,215,126,126,255,
	215,127,127,255,217,129,129,255,219,131,131,255,220,133,133,255,222,135,135,255,223,137,137,255,219,119,119,255,224,136,136,255,227,142,142,255,228,144,144,255,230,146,146,255,231,147,147,255,232,149,149,255,234,151,151,255,
	235,152,152,255,236,153,153,255,233,128,128,255,230,103,103,255,220,99,99,255,97,44,44,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,72,23,23,255,192,64,64,255,211,122,122,255,213,124,124,255,215,127,127,255,216,128,128,255,218,129,129,255,
	219,131,131,255,220,134,134,255,222,136,136,255,223,137,137,255,225,139,139,255,226,140,140,255,228,142,142,255,228,144,144,255,230,146,146,255,231,148,148,255,233,150,150,255,234,151,151,255,236,153,153,255,236,155,155,255,
	238,156,156,255,237,147,147,255,234,115,115,255,234,106,106,255,176,80,80,255,31,14,14,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,72,24,24,255,192,64,64,255,212,122,122,255,214,124,124,255,215,127,127,255,216,129,129,255,218,130,130,255,220,132,132,255,221,134,134,255,
	222,136,136,255,223,138,138,255,225,140,140,255,226,141,141,255,228,142,142,255,229,144,144,255,231,146,146,255,231,148,148,255,233,150,150,255,234,151,151,255,236,153,153,255,237,155,155,255,238,157,157,255,239,158,158,255,
	240,159,159,255,238,134,134,255,236,109,109,255,227,105,105,255,100,47,47,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,92,72,24,24,255,192,65,65,255,212,124,124,255,214,125,125,255,215,127,127,255,217,129,129,255,219,131,131,255,220,133,133,255,221,135,135,255,222,136,136,255,224,138,138,255,
	223,130,130,255,222,123,123,255,222,120,120,255,224,122,122,255,229,140,140,255,232,148,148,255,234,150,150,255,235,152,152,255,236,154,154,255,237,155,155,255,238,157,157,255,240,158,158,255,241,160,160,255,242,162,162,255,
	242,154,154,255,239,120,120,255,240,112,112,255,180,85,85,255,32,15,15,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,92,72,24,24,255,193,65,65,255,213,124,124,255,214,126,126,255,216,128,128,255,217,129,129,255,219,131,131,255,220,133,133,255,218,123,123,255,214,106,106,255,211,90,90,255,211,83,83,255,213,85,85,255,
	215,87,87,255,217,89,89,255,218,91,91,255,223,106,106,255,232,142,142,255,235,152,152,255,236,154,154,255,238,156,156,255,238,158,158,255,240,160,160,255,241,160,160,255,242,162,162,255,244,164,164,255,245,165,165,255,
	243,140,140,255,242,115,115,255,233,111,111,255,102,49,49,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,72,24,24,255,
	193,66,66,255,213,124,124,255,215,126,126,255,216,128,128,255,215,121,121,255,209,100,100,255,206,83,83,255,206,78,78,255,208,80,80,255,210,82,82,255,212,84,84,255,213,86,86,255,215,88,88,255,217,89,89,255,
	219,91,91,255,221,93,93,255,224,101,101,255,234,148,148,255,236,154,154,255,238,156,156,255,239,158,158,255,240,160,160,255,242,162,162,255,242,163,163,255,244,164,164,255,245,166,166,255,246,168,168,255,247,159,159,255,
	246,127,127,255,246,118,118,255,185,89,89,255,32,16,16,255,0,0,0,154,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,72,24,24,255,194,66,66,255,207,107,107,255,
	207,101,101,255,204,87,87,255,201,73,73,255,203,75,75,255,205,77,77,255,206,79,79,255,208,81,81,255,210,82,82,255,212,84,84,255,214,86,86,255,216,88,88,255,217,90,90,255,219,92,92,255,221,93,93,255,
	223,95,95,255,230,122,122,255,237,155,155,255,238,157,157,255,239,158,158,255,240,160,160,255,242,162,162,255,243,163,163,255,244,165,165,255,245,166,166,255,246,168,168,255,248,169,169,255,249,170,170,255,248,142,142,255,
	249,121,121,255,226,111,111,255,24,12,12,255,0,0,0,57,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,72,24,24,255,194,67,67,255,196,68,68,255,198,70,70,255,200,72,72,255,
	201,74,74,255,203,76,76,255,205,77,77,255,207,79,79,255,209,81,81,255,211,83,83,255,212,85,85,255,214,87,87,255,216,88,88,255,218,90,90,255,220,92,92,255,222,94,94,255,223,96,96,255,226,104,104,255,
	237,155,155,255,238,157,157,255,240,158,158,255,241,160,160,255,242,162,162,255,243,163,163,255,244,165,165,255,246,167,167,255,247,168,168,255,248,170,170,255,248,153,153,255,247,123,123,255,186,90,90,255,32,16,16,255,
	0,0,0,154,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,73,24,24,255,194,67,67,255,196,69,69,255,198,71,71,255,200,72,72,255,202,74,74,255,204,76,76,255,
	205,78,78,255,207,80,80,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,216,89,89,255,218,91,91,255,220,92,92,255,222,94,94,255,224,96,96,255,226,98,98,255,235,146,146,255,238,157,157,255,
	240,159,159,255,241,161,161,255,242,162,162,255,244,164,164,255,244,166,166,255,246,167,167,255,247,164,164,255,245,134,134,255,234,112,112,255,103,50,50,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,73,24,24,255,195,67,67,255,197,69,69,255,198,71,71,255,200,73,73,255,202,75,75,255,204,76,76,255,206,78,78,255,208,80,80,255,
	209,82,82,255,211,84,84,255,213,86,86,255,215,87,87,255,217,89,89,255,219,91,91,255,220,93,93,255,222,95,95,255,224,96,96,255,226,98,98,255,233,129,129,255,239,158,158,255,240,160,160,255,241,162,162,255,
	243,163,163,255,244,164,164,255,245,166,166,255,245,149,149,255,242,119,119,255,182,86,86,255,32,15,15,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,92,73,25,25,255,195,68,68,255,197,70,70,255,199,71,71,255,201,73,73,255,203,75,75,255,204,77,77,255,206,79,79,255,208,80,80,255,210,82,82,255,212,84,84,255,
	214,86,86,255,215,88,88,255,217,90,90,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,226,99,99,255,230,110,110,255,239,158,158,255,240,160,160,255,242,162,162,255,243,163,163,255,243,161,161,255,
	241,130,130,255,230,108,108,255,101,48,48,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,92,73,25,25,255,196,68,68,255,197,70,70,255,199,72,72,255,201,74,74,255,203,75,75,255,205,77,77,255,207,79,79,255,208,81,81,255,210,83,83,255,212,85,85,255,214,86,86,255,216,88,88,255,
	218,90,90,255,219,92,92,255,221,94,94,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,237,148,148,255,241,161,161,255,242,162,162,255,241,145,145,255,239,115,115,255,179,83,83,255,31,15,15,255,
	0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,73,25,25,255,
	196,69,69,255,198,70,70,255,200,72,72,255,202,74,74,255,203,76,76,255,205,78,78,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,214,87,87,255,216,89,89,255,218,90,90,255,220,92,92,255,
	222,94,94,255,224,96,96,255,225,98,98,255,227,99,99,255,229,101,101,255,232,108,108,255,239,149,149,255,238,126,126,255,225,104,104,255,99,46,46,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,73,25,25,255,196,69,69,255,198,71,71,255,
	200,73,73,255,202,74,74,255,204,76,76,255,206,78,78,255,207,80,80,255,209,82,82,255,211,84,84,255,213,85,85,255,215,87,87,255,217,89,89,255,218,91,91,255,220,93,93,255,222,94,94,255,224,96,96,255,
	226,98,98,255,228,100,100,255,229,102,102,255,231,104,104,255,233,105,105,255,175,80,80,255,31,14,14,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,73,26,26,255,197,69,69,255,199,71,71,255,201,73,73,255,202,75,75,255,
	204,77,77,255,206,78,78,255,208,80,80,255,210,82,82,255,212,84,84,255,196,79,79,255,215,88,88,255,217,89,89,255,219,91,91,255,221,93,93,255,223,95,95,255,224,97,97,255,226,98,98,255,228,100,100,255,
	230,102,102,255,221,99,99,255,97,44,44,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,73,26,26,255,197,70,70,255,199,72,72,255,201,73,73,255,203,75,75,255,205,77,77,255,206,79,79,255,
	208,81,81,255,209,83,83,255,124,49,49,255,37,15,15,255,216,88,88,255,217,90,90,255,219,92,92,255,221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,228,101,101,255,171,77,77,255,30,13,13,255,
	0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,74,26,26,255,198,70,70,255,200,72,72,255,201,74,74,255,203,76,76,255,205,77,77,255,207,79,79,255,182,71,71,255,54,21,21,255,
	0,0,0,217,27,11,11,255,216,88,88,255,218,90,90,255,220,92,92,255,222,94,94,255,223,96,96,255,225,97,97,255,216,94,94,255,95,42,42,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,92,74,26,26,255,198,71,71,255,200,72,72,255,202,74,74,255,204,76,76,255,204,78,78,255,121,47,47,255,10,4,4,247,0,0,0,118,0,0,0,38,27,11,11,255,
	216,89,89,255,218,91,91,255,220,92,92,255,222,94,94,255,224,96,96,255,168,73,73,255,29,13,13,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,92,74,26,26,255,199,71,71,255,200,73,73,255,202,75,75,255,178,66,66,255,53,20,20,255,0,0,0,200,0,0,0,40,0,0,0,0,0,0,0,35,27,11,11,255,217,89,89,255,219,91,91,255,
	221,93,93,255,212,91,91,255,93,40,40,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,74,26,26,255,
	199,71,71,255,200,73,73,255,119,44,44,255,10,4,4,247,0,0,0,118,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,35,27,11,11,255,217,90,90,255,219,91,91,255,165,69,69,255,29,12,12,255,
	0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,92,75,26,26,255,173,63,63,255,51,19,19,255,
	0,0,0,200,0,0,0,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,35,27,11,11,255,208,86,86,255,91,38,38,255,1,0,0,226,0,0,0,71,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,73,28,10,10,255,9,3,3,247,0,0,0,118,0,0,0,3,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,21,12,5,5,250,28,12,12,255,0,0,0,155,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,87,0,0,0,37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,68,0,0,0,57,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

u8 s_PauseIconData[] = 
// 35 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,0,0,0,40,0,0,0,40,
	0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,40,
	0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,29,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,89,24,6,6,255,43,12,12,255,44,12,12,255,45,13,13,255,45,13,13,255,46,14,14,255,46,14,14,255,47,15,15,255,47,16,16,255,48,16,16,255,
	49,17,17,255,10,3,3,240,0,0,0,15,0,0,0,0,0,0,0,12,11,4,4,242,52,20,20,255,52,21,21,255,53,21,21,255,54,22,22,255,54,22,22,255,55,23,23,255,55,23,23,255,56,24,24,255,
	56,24,24,255,57,25,25,255,32,14,14,255,0,0,0,98,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,96,25,25,255,174,47,47,255,177,49,49,255,
	179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,37,13,13,255,0,0,0,42,0,0,0,0,0,0,0,42,39,15,15,255,208,80,80,255,
	210,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,92,92,255,222,94,94,255,224,96,96,255,226,98,98,255,228,100,100,255,129,58,58,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,97,26,26,255,176,50,50,255,189,78,78,255,191,81,81,255,192,83,83,255,194,86,86,255,197,87,87,255,199,90,90,255,201,92,92,255,200,86,86,255,
	196,68,68,255,37,13,13,255,0,0,0,42,0,0,0,0,0,0,0,42,39,15,15,255,209,81,81,255,216,102,102,255,221,114,114,255,223,117,117,255,224,118,118,255,226,122,122,255,229,123,123,255,230,126,126,255,
	232,127,127,255,230,105,105,255,130,58,58,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,98,26,26,255,177,52,52,255,200,107,107,255,
	202,109,109,255,203,112,112,255,205,114,114,255,207,117,117,255,209,119,119,255,211,122,122,255,207,106,106,255,197,69,69,255,37,13,13,255,0,0,0,42,0,0,0,0,0,0,0,42,39,15,15,255,210,82,82,255,
	221,120,120,255,228,144,144,255,230,146,146,255,231,148,148,255,233,150,150,255,234,153,153,255,236,155,155,255,238,157,157,255,231,109,109,255,130,59,59,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,98,27,27,255,178,53,53,255,201,109,109,255,203,111,111,255,204,114,114,255,206,116,116,255,208,118,118,255,210,120,120,255,212,123,123,255,208,108,108,255,
	198,70,70,255,38,14,14,255,0,0,0,42,0,0,0,0,0,0,0,42,39,15,15,255,211,83,83,255,222,122,122,255,229,145,145,255,231,147,147,255,232,149,149,255,234,151,151,255,235,153,153,255,237,155,155,255,
	238,157,157,255,232,110,110,255,131,59,59,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,99,27,27,255,179,54,54,255,202,110,110,255,
	204,112,112,255,205,115,115,255,207,117,117,255,209,120,120,255,211,122,122,255,213,125,125,255,208,108,108,255,199,71,71,255,38,14,14,255,0,0,0,42,0,0,0,0,0,0,0,42,40,15,15,255,212,84,84,255,
	223,123,123,255,229,146,146,255,232,148,148,255,233,150,150,255,234,152,152,255,236,155,155,255,238,157,157,255,239,158,158,255,233,111,111,255,131,60,60,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,99,28,28,255,180,56,56,255,203,112,112,255,205,114,114,255,206,117,117,255,208,119,119,255,210,121,121,255,212,123,123,255,214,126,126,255,210,110,110,255,
	200,72,72,255,38,14,14,255,0,0,0,42,0,0,0,0,0,0,0,42,40,16,16,255,213,85,85,255,224,124,124,255,230,148,148,255,232,150,150,255,234,151,151,255,235,153,153,255,237,156,156,255,239,158,158,255,
	240,160,160,255,234,112,112,255,132,61,61,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,100,29,29,255,181,57,57,255,204,112,112,255,
	205,114,114,255,207,117,117,255,209,120,120,255,211,122,122,255,213,124,124,255,214,127,127,255,210,111,111,255,200,73,73,255,38,14,14,255,0,0,0,42,0,0,0,0,0,0,0,42,40,16,16,255,214,86,86,255,
	224,125,125,255,231,148,148,255,233,150,150,255,234,152,152,255,236,154,154,255,237,157,157,255,239,158,158,255,240,154,154,255,234,108,108,255,132,61,61,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,100,29,29,255,182,58,58,255,205,114,114,255,206,116,116,255,208,119,119,255,210,121,121,255,212,123,123,255,214,125,125,255,215,127,127,255,211,112,112,255,
	201,74,74,255,38,14,14,255,0,0,0,42,0,0,0,0,0,0,0,42,40,16,16,255,215,87,87,255,225,126,126,255,232,150,150,255,234,151,151,255,235,153,153,255,237,155,155,255,238,158,158,255,240,159,159,255,
	235,119,119,255,235,107,107,255,133,62,62,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,101,30,30,255,183,59,59,255,206,115,115,255,
	207,117,117,255,209,120,120,255,211,122,122,255,212,125,125,255,215,127,127,255,216,129,129,255,212,113,113,255,202,75,75,255,39,14,14,255,0,0,0,42,0,0,0,0,0,0,0,42,40,16,16,255,216,88,88,255,
	226,127,127,255,232,151,151,255,235,153,153,255,236,155,155,255,237,157,157,255,238,158,158,255,235,126,126,255,234,106,106,255,236,108,108,255,133,62,62,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,102,30,30,255,184,60,60,255,206,116,116,255,208,118,118,255,210,120,120,255,211,123,123,255,213,125,125,255,215,127,127,255,217,129,129,255,213,114,114,255,
	203,76,76,255,39,15,15,255,0,0,0,42,0,0,0,0,0,0,0,42,40,16,16,255,217,89,89,255,227,128,128,255,233,152,152,255,235,154,154,255,237,156,156,255,237,151,151,255,232,116,116,255,233,105,105,255,
	235,107,107,255,237,109,109,255,134,63,63,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,102,31,31,255,185,61,61,255,207,117,117,255,
	209,120,120,255,210,120,120,255,204,99,99,255,201,86,86,255,204,88,88,255,207,95,95,255,208,94,94,255,204,77,77,255,39,15,15,255,0,0,0,42,0,0,0,0,0,0,0,42,41,17,17,255,218,90,90,255,
	228,128,128,255,234,153,153,255,235,152,152,255,232,128,128,255,229,103,103,255,231,104,104,255,234,106,106,255,236,108,108,255,238,110,110,255,135,63,63,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,103,31,31,255,186,62,62,255,208,119,119,255,208,116,116,255,196,75,75,255,194,67,67,255,196,69,69,255,199,71,71,255,201,73,73,255,203,76,76,255,
	205,78,78,255,39,15,15,255,0,0,0,42,0,0,0,0,0,0,0,42,41,17,17,255,219,91,91,255,223,101,101,255,225,103,103,255,226,99,99,255,228,100,100,255,230,102,102,255,232,105,105,255,235,107,107,255,
	237,109,109,255,239,111,111,255,135,63,63,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,103,31,31,255,187,63,63,255,208,120,120,255,
	196,78,78,255,193,65,65,255,195,68,68,255,197,70,70,255,200,72,72,255,202,74,74,255,204,77,77,255,206,79,79,255,39,15,15,255,0,0,0,42,0,0,0,0,0,0,0,42,41,17,17,255,220,92,92,255,
	222,94,94,255,224,97,97,255,227,99,99,255,229,101,101,255,231,103,103,255,233,106,106,255,236,108,108,255,238,110,110,255,240,112,112,255,136,64,64,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,104,32,32,255,188,64,64,255,201,98,98,255,192,64,64,255,194,66,66,255,196,69,69,255,198,71,71,255,201,73,73,255,203,75,75,255,205,78,78,255,
	207,80,80,255,40,15,15,255,0,0,0,42,0,0,0,0,0,0,0,42,41,17,17,255,221,93,93,255,223,95,95,255,225,98,98,255,228,100,100,255,230,102,102,255,232,104,104,255,234,107,107,255,237,109,109,255,
	239,111,111,255,241,113,113,255,136,64,64,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,104,33,33,255,189,65,65,255,194,76,76,255,
	193,65,65,255,195,67,67,255,197,70,70,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,40,16,16,255,0,0,0,42,0,0,0,0,0,0,0,42,41,17,17,255,222,94,94,255,
	224,96,96,255,226,99,99,255,229,101,101,255,231,103,103,255,233,105,105,255,235,108,108,255,238,110,110,255,240,112,112,255,242,114,114,255,137,65,65,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,105,33,33,255,189,63,63,255,191,64,64,255,194,66,66,255,196,68,68,255,198,71,71,255,200,73,73,255,203,75,75,255,205,77,77,255,207,80,80,255,
	209,82,82,255,40,16,16,255,0,0,0,42,0,0,0,0,0,0,0,42,42,18,18,255,223,95,95,255,225,97,97,255,227,100,100,255,230,102,102,255,232,104,104,255,234,106,106,255,236,109,109,255,239,111,111,255,
	241,113,113,255,243,115,115,255,137,66,66,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,105,34,34,255,190,63,63,255,192,65,65,255,
	195,67,67,255,197,69,69,255,199,72,72,255,201,74,74,255,204,76,76,255,206,78,78,255,208,81,81,255,210,83,83,255,40,16,16,255,0,0,0,42,0,0,0,0,0,0,0,42,42,18,18,255,224,96,96,255,
	226,98,98,255,228,101,101,255,231,103,103,255,233,105,105,255,235,107,107,255,237,109,109,255,240,112,112,255,242,114,114,255,244,116,116,255,138,66,66,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,106,34,34,255,191,64,64,255,193,66,66,255,196,68,68,255,198,70,70,255,200,73,73,255,202,75,75,255,205,77,77,255,207,79,79,255,209,82,82,255,
	211,84,84,255,40,16,16,255,0,0,0,42,0,0,0,0,0,0,0,42,42,18,18,255,225,97,97,255,227,99,99,255,229,102,102,255,232,104,104,255,234,106,106,255,236,108,108,255,238,110,110,255,241,113,113,255,
	243,115,115,255,245,117,117,255,139,67,67,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,107,35,35,255,192,65,65,255,194,67,67,255,
	197,69,69,255,199,71,71,255,201,74,74,255,203,76,76,255,206,78,78,255,208,80,80,255,210,82,82,255,212,85,85,255,40,16,16,255,0,0,0,42,0,0,0,0,0,0,0,42,42,18,18,255,226,98,98,255,
	228,100,100,255,230,103,103,255,233,105,105,255,235,107,107,255,237,109,109,255,239,111,111,255,242,114,114,255,244,116,116,255,246,118,118,255,139,67,67,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,107,35,35,255,193,66,66,255,195,68,68,255,198,70,70,255,200,72,72,255,202,75,75,255,204,77,77,255,207,79,79,255,209,81,81,255,211,83,83,255,
	213,86,86,255,41,17,17,255,0,0,0,42,0,0,0,0,0,0,0,42,42,18,18,255,227,99,99,255,229,101,101,255,231,104,104,255,234,106,106,255,236,108,108,255,238,110,110,255,240,112,112,255,243,115,115,255,
	245,117,117,255,247,119,119,255,140,68,68,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,108,36,36,255,194,67,67,255,196,69,69,255,
	199,71,71,255,201,73,73,255,203,75,75,255,205,78,78,255,208,80,80,255,210,82,82,255,212,84,84,255,214,87,87,255,41,17,17,255,0,0,0,42,0,0,0,0,0,0,0,42,43,18,18,255,228,100,100,255,
	230,102,102,255,232,105,105,255,235,107,107,255,237,109,109,255,239,111,111,255,241,113,113,255,244,116,116,255,246,118,118,255,248,120,120,255,140,68,68,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,108,36,36,255,195,68,68,255,197,70,70,255,200,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,
	215,88,88,255,41,17,17,255,0,0,0,42,0,0,0,0,0,0,0,42,43,19,19,255,229,101,101,255,231,103,103,255,233,106,106,255,236,108,108,255,238,110,110,255,240,112,112,255,242,114,114,255,245,117,117,255,
	247,119,119,255,249,121,121,255,141,69,69,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,109,37,37,255,196,69,69,255,198,71,71,255,
	201,73,73,255,203,75,75,255,205,77,77,255,207,80,80,255,210,82,82,255,212,84,84,255,214,86,86,255,216,89,89,255,41,17,17,255,0,0,0,42,0,0,0,0,0,0,0,42,43,19,19,255,230,102,102,255,
	232,104,104,255,234,107,107,255,237,109,109,255,239,111,111,255,241,113,113,255,243,115,115,255,246,118,118,255,248,120,120,255,250,122,122,255,141,70,70,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,109,38,38,255,197,70,70,255,199,72,72,255,202,74,74,255,204,76,76,255,206,78,78,255,208,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,
	217,90,90,255,41,17,17,255,0,0,0,42,0,0,0,0,0,0,0,42,43,19,19,255,231,103,103,255,233,105,105,255,235,108,108,255,238,110,110,255,240,112,112,255,242,114,114,255,244,116,116,255,247,119,119,255,
	249,121,121,255,251,123,123,255,142,70,70,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,140,110,38,38,255,198,71,71,255,200,73,73,255,
	203,75,75,255,205,77,77,255,207,79,79,255,209,82,82,255,212,84,84,255,214,86,86,255,216,88,88,255,218,91,91,255,42,18,18,255,0,0,0,42,0,0,0,0,0,0,0,42,43,19,19,255,232,104,104,255,
	234,106,106,255,236,109,109,255,239,111,111,255,241,113,113,255,243,115,115,255,245,117,117,255,247,120,120,255,250,122,122,255,252,124,124,255,142,71,71,255,0,0,0,140,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,98,28,10,10,255,50,18,18,255,50,19,19,255,51,19,19,255,52,20,20,255,52,20,20,255,53,21,21,255,53,21,21,255,54,22,22,255,54,22,22,255,
	55,23,23,255,11,4,4,240,0,0,0,14,0,0,0,0,0,0,0,15,12,5,5,240,58,26,26,255,59,27,27,255,59,27,27,255,60,28,28,255,61,29,29,255,61,29,29,255,62,30,30,255,62,30,30,255,
	63,31,31,255,63,31,31,255,36,18,18,255,0,0,0,98,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,29,0,0,0,40,0,0,0,40,
	0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,0,0,0,40,
	0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,40,0,0,0,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

u8 s_PlayIconData[] =
// 33 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,97,0,0,0,9,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,184,45,21,21,255,0,0,0,217,0,0,0,59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,180,84,84,255,157,72,72,255,19,9,9,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,180,85,85,255,236,109,109,255,213,96,96,255,
	76,34,34,255,0,0,0,217,0,0,0,59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,199,181,85,85,255,239,127,127,255,236,128,128,255,229,101,101,255,150,65,65,255,18,8,8,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,181,85,85,255,240,130,130,255,242,161,161,255,237,143,143,255,227,105,105,255,204,86,86,255,73,30,30,255,
	0,0,0,217,0,0,0,59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,182,86,86,255,241,132,132,255,
	242,162,162,255,240,158,158,255,237,152,152,255,228,118,118,255,219,91,91,255,143,58,58,255,17,7,7,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,199,182,87,87,255,241,133,133,255,242,163,163,255,240,159,159,255,237,155,155,255,234,152,152,255,228,134,134,255,217,95,95,255,194,77,77,255,69,27,27,255,0,0,0,217,
	0,0,0,59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,183,87,87,255,242,133,133,255,243,163,163,255,240,160,160,255,238,156,156,255,235,152,152,255,
	232,148,148,255,229,142,142,255,218,108,108,255,208,81,81,255,136,51,51,255,16,6,6,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,184,88,88,255,
	243,134,134,255,244,164,164,255,241,160,160,255,238,157,157,255,236,153,153,255,233,150,150,255,230,146,146,255,227,142,142,255,220,123,123,255,207,84,84,255,184,67,67,255,66,23,23,255,0,0,0,217,0,0,0,59,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,184,88,88,255,244,133,133,255,244,165,165,255,242,162,162,255,239,158,158,255,236,154,154,255,234,150,150,255,231,146,146,255,228,142,142,255,225,138,138,255,
	221,131,131,255,209,97,97,255,198,70,70,255,129,44,44,255,16,5,5,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,184,89,89,255,243,129,129,255,244,166,166,255,242,162,162,255,239,158,158,255,
	236,155,155,255,234,151,151,255,231,148,148,255,228,144,144,255,225,140,140,255,222,136,136,255,219,131,131,255,211,113,113,255,196,74,74,255,174,58,58,255,62,20,20,255,0,0,0,217,0,0,0,58,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,
	185,89,89,255,244,123,123,255,245,166,166,255,243,163,163,255,240,159,159,255,237,155,155,255,234,151,151,255,231,148,148,255,228,144,144,255,225,140,140,255,222,136,136,255,219,132,132,255,216,129,129,255,212,122,122,255,
	199,87,87,255,187,60,60,255,122,37,37,255,15,4,4,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,186,90,90,255,244,116,116,255,245,157,157,255,243,163,163,255,240,160,160,255,238,156,156,255,235,152,152,255,232,149,149,255,229,145,145,255,
	226,141,141,255,223,137,137,255,220,133,133,255,217,129,129,255,214,124,124,255,211,120,120,255,202,101,101,255,187,63,63,255,165,49,49,255,59,16,16,255,0,0,0,217,0,0,0,58,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,187,91,91,255,245,117,117,255,243,131,131,255,243,162,162,255,
	241,161,161,255,238,157,157,255,236,153,153,255,233,150,150,255,230,146,146,255,227,142,142,255,224,138,138,255,221,134,134,255,218,129,129,255,215,126,126,255,211,122,122,255,208,117,117,255,204,111,111,255,189,76,76,255,
	177,49,49,255,115,30,30,255,14,3,3,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,199,187,91,91,255,245,118,118,255,241,114,114,255,238,115,115,255,236,129,129,255,235,139,139,255,235,151,151,255,233,151,151,255,230,147,147,255,227,143,143,255,224,139,139,255,221,135,135,255,218,131,131,255,
	215,127,127,255,211,122,122,255,208,118,118,255,205,114,114,255,201,109,109,255,193,91,91,255,176,53,53,255,155,39,39,255,55,13,13,255,0,0,0,217,0,0,0,45,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,187,91,91,255,246,118,118,255,242,114,114,255,238,110,110,255,234,106,106,255,230,102,102,255,226,100,100,255,226,114,114,255,
	229,139,139,255,228,144,144,255,225,140,140,255,222,136,136,255,219,131,131,255,216,128,128,255,212,124,124,255,209,119,119,255,206,115,115,255,202,111,111,255,199,106,106,255,193,97,97,255,172,48,48,255,166,39,39,255,
	61,13,13,255,0,0,0,165,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,188,92,92,255,247,119,119,255,243,115,115,255,
	239,111,111,255,235,107,107,255,231,103,103,255,227,99,99,255,223,95,95,255,219,93,93,255,223,122,122,255,226,140,140,255,223,136,136,255,220,132,132,255,216,128,128,255,213,124,124,255,210,119,119,255,206,115,115,255,
	203,111,111,255,194,93,93,255,178,55,55,255,157,40,40,255,56,13,13,255,0,0,0,217,0,0,0,45,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,199,189,93,93,255,248,120,120,255,244,116,116,255,240,112,112,255,236,108,108,255,232,104,104,255,228,100,100,255,224,96,96,255,220,92,92,255,216,89,89,255,224,133,133,255,223,137,137,255,
	220,133,133,255,217,129,129,255,214,125,125,255,211,121,121,255,207,114,114,255,192,79,79,255,180,52,52,255,117,32,32,255,14,4,4,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,189,94,94,255,248,121,121,255,244,117,117,255,240,113,113,255,236,109,109,255,232,105,105,255,228,101,101,255,
	224,97,97,255,220,93,93,255,216,89,89,255,217,106,106,255,223,138,138,255,220,134,134,255,217,130,130,255,214,126,126,255,206,107,107,255,191,69,69,255,169,52,52,255,60,18,18,255,0,0,0,217,0,0,0,59,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,190,94,94,255,249,121,121,255,
	245,117,117,255,241,113,113,255,237,109,109,255,233,105,105,255,229,101,101,255,225,97,97,255,221,93,93,255,217,90,90,255,214,92,92,255,224,139,139,255,221,135,135,255,217,128,128,255,205,94,94,255,193,66,66,255,
	126,41,41,255,15,5,5,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,199,190,94,94,255,250,122,122,255,246,118,118,255,242,114,114,255,238,110,110,255,234,106,106,255,230,102,102,255,226,98,98,255,222,94,94,255,218,90,90,255,215,89,89,255,
	225,140,140,255,218,121,121,255,204,81,81,255,182,64,64,255,65,22,22,255,0,0,0,217,0,0,0,59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,251,123,123,255,247,119,119,255,243,115,115,255,239,111,111,255,235,107,107,255,
	231,103,103,255,227,99,99,255,223,95,95,255,219,91,91,255,215,88,88,255,217,107,107,255,207,79,79,255,135,50,50,255,16,6,6,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,
	251,124,124,255,247,120,120,255,243,116,116,255,239,112,112,255,235,108,108,255,231,104,104,255,227,100,100,255,223,96,96,255,219,92,92,255,215,88,88,255,194,77,77,255,69,27,27,255,0,0,0,217,0,0,0,59,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,252,124,124,255,248,120,120,255,244,116,116,255,240,112,112,255,236,108,108,255,232,104,104,255,228,100,100,255,224,96,96,255,220,92,92,255,
	144,59,59,255,17,7,7,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,253,125,125,255,249,121,121,255,245,117,117,255,241,113,113,255,
	237,109,109,255,233,105,105,255,229,101,101,255,206,89,89,255,74,31,31,255,0,0,0,217,0,0,0,58,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,
	191,95,95,255,254,126,126,255,250,122,122,255,246,118,118,255,242,114,114,255,238,110,110,255,234,106,106,255,153,68,68,255,19,8,8,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,255,127,127,255,250,123,123,255,246,119,119,255,242,115,115,255,218,102,102,255,78,36,36,255,0,0,0,217,0,0,0,58,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,255,127,127,255,251,123,123,255,247,119,119,255,
	162,77,77,255,20,9,9,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,199,191,95,95,255,255,127,127,255,231,114,114,255,83,40,40,255,0,0,0,217,0,0,0,58,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,199,191,95,95,255,170,85,85,255,21,10,10,253,0,0,0,141,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,183,48,24,24,255,0,0,0,217,0,0,0,58,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,40,0,0,0,94,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

u8 s_RewindIconData[] = 
// 40 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,0,0,0,65,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,37,0,0,0,91,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,29,13,13,255,13,5,5,247,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,3,0,0,0,118,11,5,5,247,34,16,16,255,0,0,0,77,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,93,40,40,255,215,94,94,255,29,13,13,255,0,0,0,35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,0,0,0,200,
	55,26,26,255,210,100,100,255,93,45,45,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,
	29,12,12,255,166,71,71,255,225,97,97,255,227,99,99,255,29,13,13,255,0,0,0,35,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,118,7,3,3,247,129,61,61,255,241,114,114,255,245,117,117,255,
	93,45,45,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,91,38,38,255,212,90,90,255,223,97,97,255,
	229,120,120,255,227,99,99,255,29,13,13,255,0,0,0,35,0,0,0,0,0,0,0,40,0,0,0,200,46,21,21,255,199,93,93,255,242,114,114,255,245,133,133,255,245,122,122,255,93,45,45,255,0,0,0,92,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,28,12,12,255,162,68,68,255,220,92,92,255,224,104,104,255,233,140,140,255,235,144,144,255,228,100,100,255,
	29,13,13,255,0,0,0,39,0,0,0,117,4,2,2,247,115,53,53,255,235,109,109,254,240,117,117,255,245,149,149,255,248,169,169,255,246,124,124,255,93,45,45,255,0,0,0,92,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,89,37,37,255,207,85,85,255,219,92,92,255,227,120,120,255,233,147,147,255,234,150,150,255,235,144,144,255,228,100,100,255,29,13,13,255,0,0,0,217,
	37,17,17,255,186,86,86,255,237,109,109,255,241,129,129,255,246,161,161,255,248,168,168,255,248,169,169,255,246,124,124,255,93,45,45,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,
	27,11,11,255,159,64,64,255,216,88,88,255,219,100,100,255,229,135,135,255,232,146,146,255,234,148,148,255,235,150,150,255,235,145,145,255,228,101,101,255,30,13,13,255,102,46,46,255,227,102,102,254,237,113,113,255,
	242,145,145,255,245,165,165,255,246,166,166,255,248,168,168,255,249,169,169,255,247,125,125,255,94,46,46,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,87,35,35,255,202,81,81,255,214,88,88,255,
	222,116,116,255,230,142,142,255,231,144,144,255,232,146,146,255,234,149,149,255,235,150,150,255,235,145,145,255,229,101,101,255,189,84,84,255,232,105,105,255,236,124,124,255,242,156,156,255,244,163,163,255,246,165,165,255,
	247,167,167,255,248,168,168,255,249,170,170,255,247,125,125,255,94,46,46,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,27,10,10,255,156,60,60,255,211,83,83,255,216,96,96,255,225,130,130,255,228,141,141,255,230,143,143,255,
	231,145,145,255,233,147,147,255,234,149,149,255,236,151,151,255,235,146,146,255,229,101,101,255,232,107,107,255,238,140,140,255,242,160,160,255,243,162,162,255,244,163,163,255,246,165,165,255,247,167,167,255,248,169,169,255,
	249,171,171,255,248,126,126,255,94,46,46,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,86,32,32,255,198,76,76,255,209,83,83,255,218,111,111,255,226,136,136,255,227,139,139,255,229,142,142,255,231,144,144,255,231,146,146,255,233,148,148,255,
	234,149,149,255,236,151,151,255,236,146,146,255,233,118,118,255,239,153,153,255,241,159,159,255,242,160,160,255,244,162,162,255,245,164,164,255,246,166,166,255,247,168,168,255,248,169,169,255,249,171,171,255,248,126,126,255,
	94,46,46,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,
	26,10,10,255,152,57,57,255,206,79,79,255,211,91,91,255,221,125,125,255,225,136,136,255,227,138,138,255,228,140,140,255,229,142,142,255,231,144,144,255,232,146,146,255,234,148,148,255,235,150,150,255,236,152,152,255,
	237,152,152,255,239,154,154,255,240,157,157,255,241,159,159,255,243,161,161,255,244,162,162,255,245,164,164,255,246,166,166,255,248,168,168,255,249,169,169,255,250,171,171,255,248,126,126,255,94,46,46,255,0,0,0,92,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,84,30,30,255,193,71,71,255,205,77,77,255,
	207,79,79,255,209,84,84,255,214,96,96,255,218,107,107,255,222,120,120,255,225,129,129,255,228,134,134,255,229,139,139,255,232,145,145,255,234,148,148,255,235,150,150,255,236,152,152,255,238,154,154,255,239,156,156,255,
	240,158,158,255,241,159,159,255,243,161,161,255,244,163,163,255,246,165,165,255,246,167,167,255,248,168,168,255,249,169,169,255,250,171,171,255,249,127,127,255,94,46,46,255,0,0,0,92,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,26,9,9,255,149,54,54,255,202,74,74,255,203,76,76,255,205,78,78,255,207,79,79,255,209,81,81,255,
	211,83,83,255,213,85,85,255,214,87,87,255,216,89,89,255,218,90,90,255,220,92,92,255,222,94,94,255,224,98,98,255,227,107,107,255,230,115,115,255,233,123,123,255,236,138,138,255,240,153,153,255,242,160,160,255,
	243,162,162,255,244,163,163,255,246,165,165,255,247,167,167,255,248,168,168,255,249,170,170,255,250,172,172,255,249,128,128,255,94,46,46,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,81,29,29,255,189,68,68,255,200,73,73,255,202,74,74,255,204,76,76,255,206,78,78,255,207,80,80,255,209,82,82,255,211,83,83,255,213,85,85,255,
	215,87,87,255,217,89,89,255,218,91,91,255,220,93,93,255,222,94,94,255,224,96,96,255,226,98,98,255,228,100,100,255,229,102,102,255,231,104,104,255,233,106,106,255,238,128,128,255,243,155,155,255,245,164,164,255,
	246,165,165,255,247,167,167,255,248,169,169,255,249,171,171,255,251,172,172,255,250,129,129,255,94,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,154,
	25,9,9,255,145,51,51,255,197,69,69,255,199,71,71,255,201,73,73,255,202,75,75,255,204,77,77,255,206,78,78,255,208,80,80,255,210,82,82,255,212,84,84,255,213,86,86,255,215,88,88,255,217,89,89,255,
	219,91,91,255,221,93,93,255,223,95,95,255,224,97,97,255,226,98,98,255,228,100,100,255,230,102,102,255,232,104,104,255,234,106,106,255,235,108,108,255,237,112,112,255,243,144,144,255,246,166,166,255,248,168,168,255,
	249,169,169,255,249,171,171,255,251,172,172,255,250,129,129,255,95,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,57,18,6,6,255,176,60,60,255,195,68,68,255,
	197,70,70,255,199,72,72,255,201,73,73,255,203,75,75,255,205,77,77,255,206,79,79,255,208,81,81,255,210,82,82,255,212,84,84,255,214,86,86,255,216,88,88,255,217,90,90,255,219,92,92,255,221,93,93,255,
	223,95,95,255,225,97,97,255,227,99,99,255,228,101,101,255,230,103,103,255,232,104,104,255,234,106,106,255,236,108,108,255,238,110,110,255,239,112,112,255,243,131,131,255,247,166,166,255,249,169,169,255,250,171,171,255,
	251,173,173,255,250,130,130,255,95,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,154,25,9,9,255,146,51,51,255,198,70,70,255,200,72,72,255,
	201,74,74,255,203,76,76,255,205,77,77,255,207,79,79,255,209,81,81,255,211,83,83,255,212,85,85,255,214,87,87,255,216,88,88,255,218,90,90,255,220,92,92,255,222,94,94,255,223,96,96,255,225,97,97,255,
	227,99,99,255,229,101,101,255,231,103,103,255,233,105,105,255,234,107,107,255,236,108,108,255,238,110,110,255,240,112,112,255,242,114,114,255,246,143,143,255,249,169,169,255,250,171,171,255,251,173,173,255,251,130,130,255,
	95,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,82,30,30,255,191,69,69,255,202,74,74,255,204,76,76,255,
	205,78,78,255,207,80,80,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,216,89,89,255,218,91,91,255,220,92,92,255,222,94,94,255,224,96,96,255,226,98,98,255,227,100,100,255,229,101,101,255,
	231,103,103,255,233,105,105,255,235,107,107,255,237,109,109,255,238,111,111,255,240,112,112,255,242,114,114,255,244,118,118,255,249,169,169,255,251,172,172,255,251,174,174,255,251,130,130,255,95,47,47,255,0,0,0,92,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,26,9,9,255,151,56,56,255,204,76,76,255,206,78,78,255,208,80,80,255,
	209,82,82,255,211,84,84,255,213,86,86,255,215,87,87,255,217,89,89,255,219,91,91,255,220,93,93,255,222,95,95,255,224,96,96,255,226,98,98,255,228,100,100,255,230,102,102,255,231,104,104,255,233,106,106,255,
	235,107,107,255,237,109,109,255,239,111,111,255,241,113,113,255,242,115,115,255,244,116,116,255,249,170,170,255,251,172,172,255,252,174,174,255,252,131,131,255,95,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,85,32,32,255,196,75,75,255,208,80,80,255,210,82,82,255,212,84,84,255,
	214,86,86,255,215,88,88,255,217,90,90,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,226,99,99,255,228,100,100,255,230,102,102,255,232,104,104,255,234,106,106,255,236,108,108,255,237,110,110,255,
	239,111,111,255,241,113,113,255,243,115,115,255,245,120,120,255,250,171,171,255,251,173,173,255,252,174,174,255,252,131,131,255,96,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,27,10,10,255,155,60,60,255,210,83,83,255,212,85,85,255,214,86,86,255,216,88,88,255,
	218,90,90,255,219,92,92,255,221,94,94,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,230,103,103,255,232,105,105,255,234,106,106,255,236,108,108,255,238,110,110,255,240,112,112,255,241,114,114,255,
	243,115,115,255,246,132,132,255,250,171,171,255,251,173,173,255,253,175,175,255,252,132,132,255,96,47,47,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,88,35,35,255,203,81,81,255,214,87,87,255,216,89,89,255,218,90,90,255,220,92,92,255,
	222,94,94,255,224,96,96,255,225,98,98,255,227,99,99,255,229,101,101,255,231,103,103,255,233,105,105,255,235,107,107,255,236,109,109,255,238,110,110,255,240,112,112,255,242,114,114,255,244,116,116,255,248,141,141,255,
	250,172,172,255,251,173,173,255,253,175,175,255,253,132,132,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,28,11,11,255,160,65,65,255,217,89,89,255,218,91,91,255,220,93,93,255,222,94,94,255,224,96,96,255,
	226,98,98,255,228,100,100,255,229,102,102,255,231,104,104,255,233,105,105,255,235,107,107,255,237,109,109,255,239,111,111,255,240,113,113,255,242,114,114,255,244,116,116,255,248,145,145,255,251,172,172,255,252,174,174,255,
	253,176,176,255,253,132,132,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,90,37,37,255,209,87,87,255,221,93,93,255,223,95,95,255,224,97,97,255,226,98,98,255,228,100,100,255,
	230,102,102,255,232,104,104,255,234,106,106,255,235,108,108,255,218,100,100,255,239,111,111,255,241,113,113,255,243,115,115,255,245,117,117,255,248,142,142,255,251,172,172,255,252,174,174,255,253,176,176,255,254,133,133,255,
	96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,28,12,12,255,165,69,69,255,223,95,95,255,225,97,97,255,227,99,99,255,228,101,101,255,230,103,103,255,232,104,104,255,
	234,106,106,255,236,108,108,255,41,19,19,255,140,65,65,255,240,113,113,255,243,115,115,255,245,117,117,255,247,123,123,255,251,171,171,255,252,175,175,255,253,176,176,255,254,133,133,255,96,48,48,255,0,0,0,92,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,93,40,40,255,214,92,92,255,227,99,99,255,229,101,101,255,231,103,103,255,233,105,105,255,234,107,107,255,236,108,108,255,
	30,14,14,255,0,0,0,217,62,29,29,255,212,101,101,255,245,118,118,255,247,119,119,255,250,150,150,255,253,175,175,255,254,176,176,255,255,134,134,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,29,13,13,255,169,75,75,255,229,102,102,255,231,103,103,255,233,105,105,255,235,107,107,255,237,109,109,255,30,14,14,255,0,0,0,38,
	0,0,0,118,11,5,5,247,144,69,69,255,247,120,120,255,249,123,123,255,252,155,155,255,254,176,176,255,255,134,134,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,96,42,42,255,221,99,99,255,233,106,106,255,235,107,107,255,237,109,109,255,30,14,14,255,0,0,0,35,0,0,0,0,0,0,0,40,
	0,0,0,200,63,31,31,255,218,106,106,255,252,125,125,255,254,158,158,255,255,134,134,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,155,30,13,13,255,174,79,79,255,236,108,108,255,237,110,110,255,30,14,14,255,0,0,0,35,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,118,
	12,6,6,247,147,72,72,255,253,127,127,255,255,128,128,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,71,1,0,0,226,98,45,45,255,227,105,105,255,30,14,14,255,0,0,0,35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,0,0,0,200,
	65,32,32,255,222,111,111,255,96,48,48,255,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,14,0,0,0,155,31,14,14,255,13,6,6,250,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,118,12,6,6,247,
	36,18,18,255,0,0,0,73,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,57,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,37,0,0,0,87,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

u8 s_SkipBackIconData[] = 
// 33 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,36,0,0,0,124,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,
	0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,57,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,84,0,0,0,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,175,
	62,14,14,255,84,20,20,255,85,21,21,255,86,22,22,255,87,24,24,255,88,25,25,255,90,26,26,255,91,27,27,255,92,28,28,255,93,29,29,255,94,30,30,255,0,0,0,242,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,54,0,0,0,214,41,17,17,255,0,0,0,168,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,
	186,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,136,16,7,7,252,
	144,59,59,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,
	174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,54,0,0,0,214,67,27,27,255,196,80,80,255,218,90,90,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,136,14,5,5,252,136,53,53,255,213,86,86,255,218,98,98,255,218,92,92,255,165,69,69,255,0,0,0,188,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,
	183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,54,0,0,0,214,61,23,23,255,189,73,73,255,211,84,84,255,220,116,116,255,
	230,151,151,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,
	172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,136,12,4,4,252,
	127,47,47,255,206,79,79,255,212,91,91,255,224,133,133,255,229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,250,
	0,0,0,0,0,0,0,54,0,0,0,214,55,20,20,255,180,66,66,255,204,77,77,255,214,108,108,255,225,142,142,255,227,147,147,255,229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,
	181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,251,0,0,0,136,10,3,3,252,119,42,42,255,199,72,72,255,205,83,83,255,218,124,124,255,223,142,142,255,226,144,144,255,227,147,147,255,
	229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,
	169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,0,0,0,255,50,17,17,255,171,59,59,255,197,71,71,255,208,100,100,255,
	219,133,133,255,222,138,138,255,223,142,142,255,226,144,144,255,227,147,147,255,229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,
	112,37,37,255,191,65,65,255,195,67,67,255,202,87,87,255,208,102,102,255,211,105,105,255,214,110,110,255,221,134,134,255,226,144,144,255,227,147,147,255,229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,
	0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,
	179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,70,70,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,216,108,108,255,
	227,146,146,255,229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,
	167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,186,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,70,70,255,
	199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,209,81,81,255,220,117,117,255,229,150,150,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,185,58,58,255,
	188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,70,70,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,209,81,81,255,212,86,86,255,228,146,146,255,231,153,153,255,219,94,94,255,
	165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,
	176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,
	208,81,81,255,211,83,83,255,223,124,124,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,
	124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,
	197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,217,102,102,255,231,153,153,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,54,54,255,183,56,56,255,
	185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,214,89,89,255,231,153,153,255,
	219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,
	174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,
	206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,229,145,145,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,
	195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,226,133,133,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,
	183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,
	223,121,121,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,
	172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,
	204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,220,108,108,255,219,94,94,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,190,63,63,255,
	192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,217,96,96,255,219,94,94,255,165,69,69,255,0,0,0,188,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,
	181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,130,43,43,255,192,65,65,255,195,67,67,255,197,69,69,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,
	213,85,85,255,215,88,88,255,218,91,91,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,
	169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,0,0,0,255,64,22,22,255,179,61,61,255,197,69,69,255,199,72,72,255,
	202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,215,88,88,255,218,90,90,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,
	0,0,0,251,0,0,0,139,16,6,6,253,131,46,46,255,199,72,72,255,202,74,74,255,204,76,76,255,206,79,79,255,208,81,81,255,211,83,83,255,213,85,85,255,215,88,88,255,218,90,90,255,165,69,69,255,
	0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,
	179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,57,0,0,0,216,66,24,24,255,185,68,68,255,204,76,76,255,206,79,79,255,208,81,81,255,
	211,83,83,255,213,85,85,255,215,88,88,255,218,90,90,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,
	167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,8,
	0,0,0,139,17,6,6,253,136,51,51,255,206,78,78,255,208,81,81,255,211,83,83,255,213,85,85,255,215,88,88,255,218,90,90,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,
	188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,0,0,0,216,69,26,26,255,191,74,74,255,211,83,83,255,213,85,85,255,215,88,88,255,218,90,90,255,
	165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,
	176,49,49,255,179,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,139,
	17,7,7,253,141,55,55,255,213,85,85,255,215,88,88,255,218,90,90,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,
	124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,178,51,51,255,181,53,53,255,183,56,56,255,185,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,0,0,0,216,71,28,28,255,197,81,81,255,218,90,90,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,124,28,28,255,167,40,40,255,169,42,42,255,172,44,44,255,174,47,47,255,176,49,49,255,178,51,51,255,181,53,53,255,183,56,56,255,
	185,58,58,255,188,60,60,255,0,0,0,250,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,139,18,7,7,253,
	145,60,60,255,165,69,69,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,175,62,14,14,255,84,20,20,255,85,21,21,255,86,22,22,255,
	87,24,24,255,88,25,25,255,89,26,26,255,91,27,27,255,92,28,28,255,93,29,29,255,94,30,30,255,0,0,0,237,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,0,0,0,216,41,17,17,255,0,0,0,169,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,36,0,0,0,124,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,63,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,86,0,0,0,31,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

u8 s_SkipForwardIconData[] =
// 33 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,33,0,0,0,96,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,39,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,127,
	0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,124,0,0,0,36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,172,
	38,14,14,255,0,0,0,214,0,0,0,54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,
	116,53,53,255,117,54,54,255,118,55,55,255,119,56,56,255,120,57,57,255,121,58,58,255,122,59,59,255,123,60,60,255,124,60,60,255,125,61,61,255,95,47,47,255,0,0,0,175,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,136,52,52,255,15,6,6,252,0,0,0,136,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,232,105,105,255,234,107,107,255,236,109,109,255,238,111,111,255,240,113,113,255,242,115,115,255,244,117,117,255,246,119,119,255,248,121,121,255,
	250,123,123,255,189,93,93,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,190,74,74,255,66,26,26,255,
	0,0,0,214,0,0,0,54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,232,105,105,255,234,107,107,255,236,109,109,255,238,111,111,255,
	240,113,113,255,242,115,115,255,244,117,117,255,246,119,119,255,248,121,121,255,250,123,123,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,188,154,58,58,255,207,80,80,255,211,90,90,255,211,83,83,255,137,55,55,255,14,6,6,252,0,0,0,136,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,251,232,107,107,255,238,134,134,255,240,136,136,255,241,138,138,255,243,140,140,255,244,142,142,255,246,144,144,255,248,146,146,255,249,147,147,255,251,138,138,255,189,94,94,255,0,0,0,188,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,223,135,135,255,217,108,108,255,213,86,86,255,194,78,78,255,64,26,26,255,0,0,0,214,
	0,0,0,54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,233,109,109,255,242,161,161,255,243,163,163,255,244,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,249,172,172,255,
	251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,
	225,139,139,255,224,128,128,255,217,95,95,255,217,89,89,255,137,57,57,255,13,5,5,252,0,0,0,136,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,251,233,109,109,255,242,161,161,255,243,163,163,255,
	244,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,140,140,255,222,112,112,255,219,91,91,255,197,83,83,255,62,26,26,255,0,0,0,214,0,0,0,54,
	0,0,0,0,0,0,0,251,233,109,109,255,242,161,161,255,243,163,163,255,244,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,230,144,144,255,
	228,132,132,255,222,100,100,255,223,95,95,255,136,59,59,255,12,5,5,252,0,0,0,136,0,0,0,251,233,109,109,255,242,161,161,255,243,163,163,255,244,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,
	249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,
	224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,230,144,144,255,231,146,146,255,232,146,146,255,228,118,118,255,225,97,97,255,199,87,87,255,60,27,27,255,0,0,0,255,233,109,109,255,242,161,161,255,
	243,163,163,255,244,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,230,144,144,255,231,146,146,255,233,148,148,255,234,150,150,255,233,137,137,255,
	228,106,106,255,228,101,101,255,136,61,61,255,234,109,109,255,242,161,161,255,243,163,163,255,244,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,
	0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,
	230,144,144,255,231,146,146,255,233,148,148,255,234,150,150,255,235,152,152,255,236,151,151,255,233,123,123,255,231,103,103,255,234,109,109,255,242,161,161,255,244,163,163,255,244,165,165,255,246,166,166,255,247,168,168,255,
	248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,
	208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,230,144,144,255,231,146,146,255,233,148,148,255,234,150,150,255,235,152,152,255,237,154,154,255,238,156,156,255,237,142,142,255,234,114,114,255,
	242,161,161,255,244,163,163,255,245,165,165,255,246,166,166,255,247,168,168,255,248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,230,144,144,255,231,146,146,255,233,148,148,255,234,150,150,255,
	235,152,152,255,237,154,154,255,238,156,156,255,240,158,158,255,241,157,157,255,242,161,161,255,244,163,163,255,245,165,165,255,246,166,166,255,248,168,168,255,248,170,170,255,249,172,172,255,251,174,174,255,251,153,153,255,
	189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,
	228,142,142,255,230,144,144,255,231,146,146,255,233,148,148,255,234,150,150,255,235,152,152,255,237,154,154,255,238,156,156,255,240,158,158,255,241,159,159,255,242,161,161,255,244,163,163,255,245,165,165,255,246,166,166,255,
	248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,251,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,
	154,58,58,255,208,82,82,255,224,137,137,255,225,139,139,255,227,140,140,255,228,142,142,255,230,144,144,255,231,146,146,255,233,148,148,255,234,150,150,255,235,152,152,255,237,154,154,255,238,156,156,255,240,158,158,255,
	241,159,159,255,242,161,161,255,244,163,163,255,245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,189,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,208,81,81,255,220,123,123,255,225,139,139,255,227,141,141,255,228,142,142,255,229,143,143,255,228,132,132,255,227,120,120,255,
	226,108,108,255,226,103,103,255,227,101,101,255,231,111,111,255,236,133,133,255,241,159,159,255,242,161,161,255,244,163,163,255,245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,
	252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,213,91,91,255,
	216,99,99,255,218,100,100,255,217,91,91,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,231,103,103,255,235,119,119,255,242,161,161,255,244,163,163,255,245,165,165,255,
	246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,
	231,103,103,255,233,105,105,255,239,140,140,255,244,163,163,255,245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,91,91,255,
	221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,231,103,103,255,233,105,105,255,236,112,112,255,244,162,162,255,245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,
	251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,
	211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,231,103,103,255,233,105,105,255,235,107,107,255,242,149,149,255,
	245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,
	229,101,101,255,231,103,103,255,233,105,105,255,235,107,107,255,240,133,133,255,245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,
	219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,227,99,99,255,229,101,101,255,159,71,71,255,233,105,105,255,235,107,107,255,239,122,122,255,245,165,165,255,246,166,166,255,248,168,168,255,249,170,170,255,
	250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,
	209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,91,91,255,221,93,93,255,223,95,95,255,225,97,97,255,208,91,91,255,76,34,34,255,0,0,0,255,233,105,105,255,235,107,107,255,
	237,111,111,255,245,164,164,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,219,91,91,255,221,93,93,255,223,95,95,255,150,65,65,255,
	19,8,8,253,0,0,0,139,0,0,0,252,233,105,105,255,235,107,107,255,237,109,109,255,244,154,154,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,
	0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,
	217,89,89,255,219,91,91,255,203,85,85,255,74,32,32,255,0,0,0,215,0,0,0,56,0,0,0,0,0,0,0,251,233,105,105,255,235,107,107,255,237,109,109,255,242,141,141,255,246,166,166,255,248,168,168,255,
	249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,
	207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,215,87,87,255,217,89,89,255,146,61,61,255,18,8,8,253,0,0,0,139,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,251,233,105,105,255,
	235,107,107,255,237,109,109,255,241,128,128,255,246,166,166,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,213,85,85,255,197,80,80,255,72,30,30,255,0,0,0,215,0,0,0,56,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,233,105,105,255,235,107,107,255,237,109,109,255,239,113,113,255,246,160,160,255,248,168,168,255,249,170,170,255,250,172,172,255,251,174,174,255,252,153,153,255,
	190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,207,79,79,255,209,81,81,255,211,83,83,255,142,57,57,255,
	18,7,7,253,0,0,0,139,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,233,105,105,255,235,107,107,255,237,109,109,255,239,111,111,255,241,118,118,255,
	245,141,141,255,247,144,144,255,249,146,146,255,250,147,147,255,251,138,138,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,
	154,58,58,255,207,79,79,255,192,74,74,255,70,28,28,255,0,0,0,215,0,0,0,56,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,
	233,105,105,255,235,107,107,255,237,109,109,255,239,111,111,255,241,113,113,255,243,115,115,255,245,117,117,255,247,119,119,255,249,121,121,255,251,123,123,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,188,154,58,58,255,138,53,53,255,17,7,7,253,0,0,0,139,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,251,233,105,105,255,235,107,107,255,237,109,109,255,239,111,111,255,241,113,113,255,243,115,115,255,245,117,117,255,247,119,119,255,249,121,121,255,
	251,123,123,255,190,94,94,255,0,0,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,169,39,14,14,255,0,0,0,215,0,0,0,56,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,237,117,53,53,255,118,54,54,255,119,55,55,255,120,56,56,255,
	121,57,57,255,122,58,58,255,123,59,59,255,124,60,60,255,125,61,61,255,126,62,62,255,95,47,47,255,0,0,0,175,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,31,0,0,0,86,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,63,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,128,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,127,0,0,0,124,0,0,0,36,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

u8 s_StopIconData[] =
// 33 x 35
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,123,0,0,0,248,0,0,0,253,0,0,0,253,
	0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,
	0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,252,0,0,0,147,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,223,223,111,111,255,255,127,127,255,255,127,127,255,255,127,127,255,254,126,126,255,253,125,125,255,252,124,124,255,251,123,123,255,250,122,122,255,249,121,121,255,247,120,120,255,246,119,119,255,
	245,117,117,255,244,116,116,255,243,115,115,255,242,114,114,255,241,113,113,255,240,112,112,255,239,111,111,255,238,110,110,255,236,109,109,255,235,108,108,255,205,94,94,255,0,0,0,223,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,222,110,110,255,253,125,125,255,252,124,124,255,250,123,123,255,249,121,121,255,248,120,120,255,247,119,119,255,
	246,118,118,255,245,117,117,255,244,116,116,255,243,115,115,255,242,114,114,255,240,113,113,255,239,112,112,255,238,110,110,255,237,109,109,255,236,108,108,255,235,107,107,255,234,106,106,255,233,105,105,255,232,104,104,255,
	231,103,103,255,200,89,89,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,218,106,106,255,248,120,120,255,
	250,173,173,255,250,173,173,255,249,172,172,255,248,171,171,255,247,170,170,255,246,169,169,255,246,168,168,255,245,167,167,255,244,161,161,255,238,116,116,255,236,108,108,255,235,107,107,255,233,106,106,255,232,105,105,255,
	231,104,104,255,230,102,102,255,229,101,101,255,228,100,100,255,227,99,99,255,226,98,98,255,197,85,85,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,223,213,101,101,255,243,115,115,255,247,170,170,255,246,169,169,255,246,168,168,255,245,167,167,255,245,166,166,255,243,165,165,255,243,164,164,255,241,159,159,255,234,115,115,255,
	232,104,104,255,231,103,103,255,230,102,102,255,229,101,101,255,228,100,100,255,226,99,99,255,225,98,98,255,224,97,97,255,223,95,95,255,222,94,94,255,221,93,93,255,192,80,80,255,0,0,0,223,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,209,97,97,255,238,110,110,255,244,165,165,255,243,164,164,255,243,163,163,255,242,163,163,255,
	241,161,161,255,241,161,161,255,240,160,160,255,231,116,116,255,228,101,101,255,227,99,99,255,226,98,98,255,225,97,97,255,224,96,96,255,223,95,95,255,222,94,94,255,221,93,93,255,219,92,92,255,218,91,91,255,
	217,90,90,255,216,89,89,255,188,76,76,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,206,94,94,255,
	233,106,106,255,241,161,161,255,240,160,160,255,239,159,159,255,239,158,158,255,238,157,157,255,237,156,156,255,234,140,140,255,225,97,97,255,224,96,96,255,222,95,95,255,221,94,94,255,220,92,92,255,219,91,91,255,
	218,90,90,255,217,89,89,255,216,88,88,255,215,87,87,255,214,86,86,255,212,85,85,255,211,84,84,255,184,73,73,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,201,89,89,255,229,101,101,255,238,157,157,255,237,156,156,255,236,155,155,255,235,153,153,255,235,153,153,255,234,151,151,255,224,109,109,255,220,92,92,255,
	219,91,91,255,218,90,90,255,217,89,89,255,215,88,88,255,214,87,87,255,213,86,86,255,212,84,84,255,211,83,83,255,210,82,82,255,209,81,81,255,208,80,80,255,207,79,79,255,179,68,68,255,0,0,0,223,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,197,85,85,255,224,96,96,255,234,152,152,255,234,151,151,255,233,150,150,255,
	232,150,150,255,231,148,148,255,229,143,143,255,216,89,89,255,215,87,87,255,214,86,86,255,213,85,85,255,212,84,84,255,211,83,83,255,210,82,82,255,208,81,81,255,207,80,80,255,206,79,79,255,205,77,77,255,
	204,76,76,255,203,75,75,255,202,74,74,255,176,64,64,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,
	192,80,80,255,219,91,91,255,231,148,148,255,230,147,147,255,229,146,146,255,229,145,145,255,228,144,144,255,224,132,132,255,211,84,84,255,210,83,83,255,209,82,82,255,208,80,80,255,207,79,79,255,206,78,78,255,
	205,77,77,255,204,76,76,255,203,75,75,255,201,74,74,255,200,73,73,255,199,72,72,255,198,71,71,255,197,69,69,255,171,59,59,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,188,77,77,255,214,87,87,255,227,144,144,255,227,142,142,255,226,141,141,255,225,140,140,255,224,139,139,255,219,123,123,255,207,79,79,255,
	205,78,78,255,204,77,77,255,203,76,76,255,202,75,75,255,201,73,73,255,200,72,72,255,199,71,71,255,198,70,70,255,197,69,69,255,196,68,68,255,194,67,67,255,193,66,66,255,192,65,65,255,167,56,56,255,
	0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,185,73,73,255,209,82,82,255,224,139,139,255,223,138,138,255,
	222,137,137,255,221,135,135,255,220,134,134,255,217,123,123,255,202,74,74,255,201,73,73,255,200,72,72,255,198,71,71,255,197,70,70,255,196,69,69,255,195,68,68,255,194,67,67,255,193,65,65,255,192,64,64,255,
	191,63,63,255,190,62,62,255,189,61,61,255,187,60,60,255,163,52,52,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,223,180,68,68,255,205,77,77,255,220,134,134,255,219,133,133,255,218,131,131,255,217,131,131,255,217,129,129,255,216,129,129,255,199,76,76,255,196,68,68,255,195,67,67,255,194,66,66,255,193,65,65,255,
	191,64,64,255,190,63,63,255,189,62,62,255,188,61,61,255,187,60,60,255,186,58,58,255,185,57,57,255,184,56,56,255,183,55,55,255,159,47,47,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,176,64,64,255,200,72,72,255,217,129,129,255,216,127,127,255,215,127,127,255,213,125,125,255,213,125,125,255,212,123,123,255,
	200,90,90,255,191,64,64,255,190,62,62,255,189,61,61,255,188,60,60,255,187,59,59,255,186,58,58,255,184,57,57,255,183,56,56,255,182,55,55,255,181,54,54,255,180,53,53,255,179,52,52,255,178,50,50,255,
	155,43,43,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,171,60,60,255,195,68,68,255,213,124,124,255,
	212,122,122,255,211,122,122,255,210,120,120,255,209,120,120,255,208,118,118,255,205,113,113,255,188,64,64,255,185,58,58,255,184,57,57,255,183,55,55,255,182,54,54,255,181,53,53,255,180,52,52,255,179,51,51,255,
	177,50,50,255,176,49,49,255,175,48,48,255,174,47,47,255,173,46,46,255,150,39,39,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,223,167,56,56,255,190,63,63,255,208,119,119,255,208,118,118,255,207,116,116,255,206,115,115,255,205,114,114,255,204,113,113,255,203,111,111,255,196,95,95,255,180,53,53,255,179,52,52,255,
	178,51,51,255,177,50,50,255,176,49,49,255,175,47,47,255,174,46,46,255,173,45,45,255,172,44,44,255,170,43,43,255,169,42,42,255,168,41,41,255,146,35,35,255,0,0,0,223,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,164,52,52,255,185,58,58,255,204,114,114,255,204,113,113,255,203,111,111,255,202,110,110,255,201,109,109,255,
	200,107,107,255,199,106,106,255,198,105,105,255,189,82,82,255,175,49,49,255,173,46,46,255,172,45,45,255,171,44,44,255,170,43,43,255,169,42,42,255,168,40,40,255,167,39,39,255,166,38,38,255,165,37,37,255,
	163,36,36,255,142,31,31,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,159,47,47,255,181,53,53,255,
	201,109,109,255,199,107,107,255,198,106,106,255,197,105,105,255,197,104,104,255,196,102,102,255,195,102,102,255,194,99,99,255,193,99,99,255,189,90,90,255,175,57,57,255,167,40,40,255,166,39,39,255,165,38,38,255,
	164,37,37,255,163,36,36,255,162,35,35,255,161,34,34,255,160,32,32,255,159,31,31,255,138,26,26,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,223,155,44,44,255,176,49,49,255,196,102,102,255,196,102,102,255,195,100,100,255,193,99,99,255,192,98,98,255,191,97,97,255,190,96,96,255,189,95,95,255,188,93,93,255,
	188,92,92,255,186,91,91,255,182,81,81,255,175,64,64,255,167,49,49,255,160,35,35,255,159,34,34,255,159,35,35,255,160,38,38,255,160,40,40,255,154,27,27,255,134,22,22,255,0,0,0,223,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,150,39,39,255,171,44,44,255,192,98,98,255,191,97,97,255,190,95,95,255,189,94,94,255,
	188,92,92,255,187,92,92,255,186,90,90,255,185,89,89,255,184,88,88,255,183,86,86,255,182,85,85,255,181,84,84,255,180,83,83,255,179,81,81,255,178,80,80,255,176,78,78,255,175,77,77,255,174,76,76,255,
	173,75,75,255,149,22,22,255,129,18,18,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,146,35,35,255,
	166,39,39,255,188,92,92,255,186,91,91,255,186,90,90,255,185,88,88,255,184,88,88,255,183,85,85,255,182,85,85,255,180,83,83,255,179,82,82,255,178,81,81,255,177,80,80,255,176,78,78,255,175,77,77,255,
	174,76,76,255,173,75,75,255,172,72,72,255,171,71,71,255,170,70,70,255,168,68,68,255,144,17,17,255,125,14,14,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,143,31,31,255,162,34,34,255,183,87,87,255,182,85,85,255,181,85,85,255,180,83,83,255,179,81,81,255,178,81,81,255,177,79,79,255,176,77,77,255,
	175,76,76,255,173,75,75,255,172,73,73,255,171,72,72,255,170,71,71,255,170,70,70,255,168,68,68,255,168,67,67,255,166,66,66,255,165,65,65,255,164,62,62,255,139,12,12,255,121,10,10,255,0,0,0,223,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,138,27,27,255,157,29,29,255,179,81,81,255,177,80,80,255,176,78,78,255,
	175,77,77,255,174,76,76,255,173,75,75,255,172,73,73,255,171,72,72,255,170,71,71,255,170,70,70,255,168,67,67,255,166,66,66,255,165,65,65,255,164,63,63,255,163,62,62,255,162,61,61,255,161,60,60,255,
	161,59,59,255,160,58,58,255,135,8,8,255,117,5,5,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,
	134,23,23,255,152,25,25,255,174,76,76,255,173,73,73,255,172,72,72,255,170,71,71,255,170,70,70,255,168,68,68,255,168,67,67,255,166,66,66,255,165,65,65,255,164,63,63,255,163,62,62,255,162,61,61,255,
	161,59,59,255,160,58,58,255,159,57,57,255,158,57,57,255,157,56,56,255,157,55,55,255,156,54,54,255,130,3,3,255,113,2,2,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,129,18,18,255,147,20,20,255,146,19,19,255,145,18,18,255,144,17,17,255,143,16,16,255,142,14,14,255,140,13,13,255,139,12,12,255,
	138,11,11,255,137,10,10,255,136,9,9,255,135,8,8,255,134,7,7,255,133,6,6,255,132,5,5,255,131,3,3,255,129,2,2,255,128,1,1,255,127,0,0,255,127,0,0,255,127,0,0,255,111,0,0,255,
	0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,125,14,14,255,142,15,15,255,141,14,14,255,140,13,13,255,
	139,12,12,255,138,11,11,255,137,10,10,255,136,9,9,255,135,7,7,255,133,6,6,255,132,5,5,255,131,4,4,255,130,3,3,255,129,2,2,255,128,1,1,255,127,0,0,255,127,0,0,255,127,0,0,255,
	127,0,0,255,127,0,0,255,127,0,0,255,127,0,0,255,111,0,0,255,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,147,0,0,0,252,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,
	0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,253,0,0,0,147,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};


