set TESTERS=sample_anim sample_player sample_blend sample_blendn sample_filter sample_mirror
set TESTERS=%TESTERS% viewer_anim sample_body sample_jlimits
set SAMPLE_LIBS=sample_cranimation sample_rmcore %RAGE_SAMPLE_LIBS%
set LIBS=%SAMPLE_LIBS% %RAGE_CR_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS%
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples
