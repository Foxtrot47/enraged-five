// 
// sample_cranimation/sample_iknorth.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: IK Body North
// PURPOSE:
//		This sample shows how to use the IK system to look at and pick up objects
//      using the R* North skeleton.

using namespace rage;

#include "sample_cranimation/sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crbody/ikbody.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crskeleton/skeletondata.h"
#include "data/callback.h"
#include "grcore/im.h"
#include "vector/geometry.h"
#include "input/mouse.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"
#include "parser/manager.h"
#include "crskeleton/jointdata.h"

class craSampleJLimits : public ragesamples::craSampleManager
{
public:

	craSampleJLimits()
	: m_SelectedJointIndex(-1)
	{
		m_LimitsFileName[0] = '\0';
	}

protected:

	void InitClient()
	{
		craSampleManager::InitClient();

		m_DrawSkin = true;

#if __BANK
		CreateJointLimitBank().Show();
		bkBank& bank = BANKMGR.CreateBank("Joint Limit Editor", 50, 400);
		bank.AddToggle("Draw Skin", &m_DrawSkin);
		bank.Show();
#endif // __BANK
	}

	void ShutdownClient()
	{
		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{	
		crSkeleton& skel = GetSkeleton();

		skel.Reset();
		skel.Update();
		UpdateMatrixSet();
	}

	void UpdateMouse()
	{
		if((ioMouse::GetButtons() & (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT)) == (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT))
		{
			Vec3V mouseScreen, mouseFar;
			grcWorldIdentity();
			grcViewport::GetCurrent()->ReverseTransform(
				static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()),
				mouseScreen, mouseFar);

			grcWorldIdentity();
			grcBegin(drawLines,2);
			grcVertex3fv(&mouseScreen[0]);
			grcVertex3fv(&mouseFar[0]);
			grcEnd();
		}
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();
		UpdateMouse();

#if __PFDRAW
		GetSkeleton().ProfileDraw();
#endif // __PFDRAW
	}

#if __BANK

	// Creates the joint limit bank and populates it with widgets.
	bkBank& CreateJointLimitBank()
	{
		bkBank &bank = BANKMGR.CreateBank("rage - Joint Limits");

		bank.AddText("Limits File", m_LimitsFileName, RAGE_MAX_PATH, true);

		bank.AddButton("Load Limits", datCallback(CFA2(LoadLimitsCallback),this));
		bank.AddButton("Save Limits", datCallback(CFA2(SaveLimitsCallback),this));

		crSkeletonData* skelData = GetDrawable().GetSkeletonData();
		const int numBones = skelData->GetNumBones();

		const char** boneNames = Alloca(const char*, numBones + 1);

		boneNames[0] = "None Selected";

		for(int i=0; i<numBones; ++i)
			boneNames[i+1] = skelData->GetBoneData(i)->GetName();

		bank.AddCombo("Select Joint", &m_SelectedJointIndex, numBones + 1, boneNames, -1, datCallback(CFA2(SelectBoneCallback),this));

		return bank;
	}

	// Removes the per-joint widgets from the bank when switching joints
	void ClearBank(bkBank& bank)
	{
		bank.Truncate(5);
	}
	
	// Bank callback to load a joint limit file.
	void LoadLimitsCallback(bkWidget& button)
	{
		if(BANKMGR.OpenFile(m_LimitsFileName, RAGE_MAX_PATH, "*.jlimits", false, "Limits file (*.jlimits)"))
		{
			bkBank* bank = dynamic_cast<bkBank*>(button.GetParent());
			Assert(bank);

			m_SelectedJointIndex = -1;
			ClearBank(*bank);

			crJointData jointDataFile;
			jointDataFile.Load(m_LimitsFileName);
		}
	}


	// Bank callback to save a joint limit file.
	void SaveLimitsCallback(bkWidget& )
	{
		char saveFileName[RAGE_MAX_PATH];
		strcpy(saveFileName, m_LimitsFileName);
		if(BANKMGR.OpenFile(saveFileName, RAGE_MAX_PATH, "*.jlimits", true, "Limits file (*.jlimits)"))
		{
			PARSER.SaveObject(saveFileName, "jlimits", &m_JointData);
		}
	}

	// Bank callback to convert the Euler angle limits in the .skel file to an appoximation of a swing/twist joint limit.
	void InitFromEulersCallback()
	{/*
		crSkeletonData* skelData = GetDrawable().GetSkeletonData();
		if(m_SelectedJointIndex >= 0)
		{
			crBoneData* boneData = skelData->GetBoneData(m_SelectedJointIndex);
			crJointRotationLimit* jointLimit = m_JointData->FindJointRotationLimit(boneData->GetBoneId());
			if(jointLimit)
			{
				jointLimit->InitFromEulers(boneData->GetDefaultTranslation(), boneData->GetRotationMin(), RCC_VECTOR3(boneData->GetRotationMax()));
			}
		}*/
	}

	// Bank callback to allocate a new joint limit structure for the selected bone that does not currently have joint limits.
	void CreateLimitsCallback(bkWidget& /*button*/)
	{/*
		u16 boneID;

		Assert(m_SelectedJointIndex == u16(m_SelectedJointIndex));

		crSkeletonData* skelData = GetDrawable().GetSkeletonData();
		if (!skelData->ConvertBoneIndexToId(u16(m_SelectedJointIndex), boneID))
			return;

		crJointData& newJoint = m_JointData.m_JointData.Grow();
		newJoint.Reset();
		newJoint.SetBoneID(boneID);

		bkBank* bank = dynamic_cast<bkBank*>(button.GetParent());
		Assert(bank);

		ClearBank(*bank);
		bank->AddButton("Delete Limits", datCallback(CFA2(DeleteLimitsCallback),this));

		bank->AddButton("Load .skel Limits", datCallback(CFA1(InitFromEulersCallback),this));

		newJoint.AddWidgets(*bank);*/
	}

	// Bank callback to remove the joint limit structure from the selected bone.
	void DeleteLimitsCallback(bkWidget& /*button*/)
	{/*
		crSkeletonData* skelData = GetDrawable().GetSkeletonData();

		u16 boneID;

		Assert(m_SelectedJointIndex == u16(m_SelectedJointIndex));
		if (!skelData->ConvertBoneIndexToId(u16(m_SelectedJointIndex), boneID))
			return;

		// Clear the joint data for the selected bone
		skelData->GetBoneData(m_SelectedJointIndex)->SetJointData(NULL);

		int n = m_JointData.m_JointData.GetCount();

		Assert(n > 0);

		// Shrink the joint data array by moving the last entry to the empty slot
		if (m_SelectedJointIndex < n - 1)
		{
			// First find the joint data for the currently selected bone
			int deletedJointDataIndex;
			for (deletedJointDataIndex = 0; deletedJointDataIndex < n; deletedJointDataIndex++)
				if (m_JointData.m_JointData[deletedJointDataIndex].GetBoneID() == boneID)
					break;
			Assert(deletedJointDataIndex < n);

			// Next find the bone for the last entry in the joint data file
			int lastJointDataBoneIndex;
			if (!skelData->ConvertBoneIdToIndex(u16(m_JointData.m_JointData[n - 1].GetBoneID()), lastJointDataBoneIndex))
				return;

			// Copy the last entry into the unused entry
			m_JointData.m_JointData[deletedJointDataIndex] = m_JointData.m_JointData[n - 1];
			// Point the bone that had the last entry to the unused entry
			skelData->GetBoneData(lastJointDataBoneIndex)->SetJointData(&m_JointData.m_JointData[deletedJointDataIndex]);
		}

		// Delete the now unused last entry
		m_JointData.m_JointData.Resize(n - 1);

		bkBank* bank = dynamic_cast<bkBank*>(button.GetParent());
		Assert(bank);

		ClearBank(*bank);
		bank->AddButton("Create Limits", datCallback(CFA2(CreateLimitsCallback),this));*/
	}

	// Bank callback when the user selects a different bone in the bone selection
	// dropdown.  Updates the bank to show the limits if any exist for this joint.
	void SelectBoneCallback(bkWidget& combo)
	{
		crSkeletonData* skelData = GetDrawable().GetSkeletonData();
		bkBank* bank = dynamic_cast<bkBank*>(combo.GetParent());
		Assert(bank);

		ClearBank(*bank);

		if (m_SelectedJointIndex >= 0)
		{
			if (m_SelectedJointIndex >= skelData->GetNumBones())
				return;

			crJointRotationLimit* jointData = m_JointData.FindJointRotationLimit(skelData->GetBoneData(m_SelectedJointIndex)->GetBoneId());

			if (jointData == NULL)
			{
				bank->AddButton("Create Limits", datCallback(CFA2(CreateLimitsCallback),this));
			}
			else
			{
				bank->AddButton("Delete Limits", datCallback(CFA2(DeleteLimitsCallback),this));

				bank->AddButton("Load .skel Limits", datCallback(CFA1(InitFromEulersCallback),this));

				jointData->AddWidgets(*bank);
			}
		}
	}

	static void LoadLimitsCallback(CallbackData sample, CallbackData button) { reinterpret_cast<craSampleJLimits*>(sample)->LoadLimitsCallback(*reinterpret_cast<bkWidget*>(button)); }
	static void SaveLimitsCallback(CallbackData sample, CallbackData button) { reinterpret_cast<craSampleJLimits*>(sample)->SaveLimitsCallback(*(bkWidget*)button); }
	static void InitFromEulersCallback(CallbackData sample) { reinterpret_cast<craSampleJLimits*>(sample)->InitFromEulersCallback(); }
	static void CreateLimitsCallback(CallbackData sample, CallbackData button){ reinterpret_cast<craSampleJLimits*>(sample)->CreateLimitsCallback(*reinterpret_cast<bkWidget*>(button)); }
	static void DeleteLimitsCallback(CallbackData sample, CallbackData button) {  reinterpret_cast<craSampleJLimits*>(sample)->DeleteLimitsCallback(*reinterpret_cast<bkWidget*>(button));}
	static void SelectBoneCallback(CallbackData sample, CallbackData combo){ reinterpret_cast<craSampleJLimits*>(sample)->SelectBoneCallback(*reinterpret_cast<bkWidget*>(combo)); }

#endif // __BANK

private:
	bool m_DrawSkin;

	crJointData m_JointData;

	// Which joint to draw when the joints profile draw is enabled and sm_DrawAllJoints is false.
	int m_SelectedJointIndex;

	// Temporary space used by the bank to provide a text entry field for the joint limit filename.
	char m_LimitsFileName[RAGE_MAX_PATH];
};

// main application
int Main()
{
	craSampleJLimits sampleCra;

	sampleCra.Init("sample_cranimation\\NewSkel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
