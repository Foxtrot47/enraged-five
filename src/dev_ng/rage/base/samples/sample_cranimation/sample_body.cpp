// 
// sample_cranimation/sample_body.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Animation
// PURPOSE:
//		This sample shows how to use the low level IK and body systems.


using namespace rage;

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crbody/ikbody.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crskeleton/skeletondata.h"
#include "data/callback.h"
#include "grcore/im.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define DEFAULT_ANIM "Gent_gun_walk_rht2"
PARAM(anim,"[sample_body] Animation file to load (default is \"" DEFAULT_ANIM "\")");

class craSampleBody : public ragesamples::craSampleManager
{
public:
	craSampleBody() : ragesamples::craSampleManager()
	{
		m_EnableIK = true;
		m_UpdateIKGoal = true;
		m_DrawIKGoal = true;
		m_IKGoalIconSize = 0.1f;
		m_IKGoalIconScale = 1.0f;
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();


		// STEP #1. Load our animation.
		const char *animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);

		m_Animation=crAnimation::AllocateAndLoad(animName);

		crSkeleton& skeleton = GetSkeleton();
		const crSkeletonData& skelData = skeleton.GetSkeletonData();


		// STEP #2. Set up our animation player
		m_Player = rage_new crAnimPlayer;
		m_Player->SetAnimation(m_Animation);

		// force animation to continuously loop - regardless of animation's own internal loop flag
		m_Player->SetLooped(true, true);


		// STEP #3. Initialize our animation frame.
		m_Frame=rage_new crFrame;

		if( m_Animation )
		{
			m_Frame->InitCreateBoneAndMoverDofs(skelData, false);
		}

		// reset the values in the frame to identity
		m_Frame->IdentityFromSkel(skelData);


		// STEP #4. Initialize the kinematics class

		// initialize kinematics, to change goals.
		m_JointData = rage_new crJointData;
		m_Body = rage_new crIKBodyHumanoid;
		if(!m_Body->InitByDefaultNames(skelData, *m_JointData))
		{
			Quitf("failed to attach body to skeleton");
		}

		m_Kinematics = rage_new crKinematics;
		m_Kinematics->Init(skeleton);

		// Turn on IK for arms and legs.
		m_ArmLeft = rage_new crIKSolverGleicherLimb(crIKBodyHumanoid::IK_HUMANOID_PART_ARM_LEFT, *m_Body, *m_JointData);
		m_ArmRight = rage_new crIKSolverGleicherLimb(crIKBodyHumanoid::IK_HUMANOID_PART_ARM_RIGHT, *m_Body, *m_JointData);
		m_LegLeft = rage_new crIKSolverGleicherLimb(crIKBodyHumanoid::IK_HUMANOID_PART_LEG_LEFT, *m_Body, *m_JointData);
		m_LegRight = rage_new crIKSolverGleicherLimb(crIKBodyHumanoid::IK_HUMANOID_PART_LEG_RIGHT, *m_Body, *m_JointData);
		m_Kinematics->AddSolver(m_ArmLeft);
		m_Kinematics->AddSolver(m_ArmRight);
		m_Kinematics->AddSolver(m_LegLeft);
		m_Kinematics->AddSolver(m_LegRight);

#if __BANK
		AddWidgetsBody();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Player;
		delete m_Animation;
		delete m_Frame;
		delete m_Kinematics;
		delete m_Body;
		delete m_JointData;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{	
		crSkeleton& skeleton = GetSkeleton();
		skeleton.Reset();


		// STEP #5. Update the animation player.

		// update the current playback rate
		m_Player->SetRate(GetPlaybackRate());

		// pass in the elapsed time
		m_Player->Update(TIME.GetSeconds());

		// composite the current animation frame
		m_Player->Composite(*m_Frame);
		

		// STEP #7. Compute the global matrices of the skeleton bones.
		skeleton.Update();

		// STEP #8. Update the kinematics

		// Kinematics, this update automatically updates the skeleton, 
		// tries to reach current IK goals
		m_Kinematics->Iterate();
		m_Kinematics->Solve();

		UpdateMatrixSet();
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();

		// Draw the IK Goals
		if(m_DrawIKGoal)
		{
			DrawIKGoal();
		}
	}

	void DrawIKGoalIcon(Vec3V_In center)
	{
		Vec3V a, b; 

		a = b = center;
		a.SetXf(a.GetXf() - m_IKGoalIconSize * m_IKGoalIconScale);
		b.SetXf(b.GetXf() + m_IKGoalIconSize * m_IKGoalIconScale);
		grcBegin(drawLines, 2);
		grcVertex3f(a);
		grcVertex3f(b);
		grcEnd();

		a = b = center;
		a.SetYf(a.GetYf() - m_IKGoalIconSize * m_IKGoalIconScale);
		b.SetYf(b.GetYf() + m_IKGoalIconSize * m_IKGoalIconScale);
		grcBegin(drawLines, 2);
		grcVertex3f(a);
		grcVertex3f(b);
		grcEnd();

		a = b = center;
		a.SetZf(a.GetZf() - m_IKGoalIconSize * m_IKGoalIconScale);
		b.SetZf(b.GetZf() + m_IKGoalIconSize * m_IKGoalIconScale);
		grcBegin(drawLines, 2);
		grcVertex3f(a);
		grcVertex3f(b);
		grcEnd();
	}

	void DrawIKGoal()
	{
		grcBindTexture(NULL);
		grcColor(Color32(0.9f,0.9f,0.9f));
		grcViewport::SetCurrentWorldIdentity();

		const crIKGoal &armGoal1 = m_ArmLeft->GetGoal();
		const crIKGoal &armGoal2 = m_ArmRight->GetGoal();
		const crIKGoal &legGoal1 = m_LegLeft->GetGoal();
		const crIKGoal &legGoal2 = m_LegRight->GetGoal();

		// Draw ik goal icons.
		DrawIKGoalIcon(armGoal1.GetPrimaryPosition());
		DrawIKGoalIcon(armGoal2.GetPrimaryPosition());
		DrawIKGoalIcon(legGoal1.GetPrimaryPosition());
		DrawIKGoalIcon(legGoal2.GetPrimaryPosition());
	}

    void EnableIKCB()
	{
		if(m_Kinematics == NULL)
			return;

		float blend = 0.0f;
		if(m_EnableIK)
			blend = 1.0f;

		m_ArmLeft->SetAlpha(blend);
		m_ArmRight->SetAlpha(blend);
		m_LegLeft->SetAlpha(blend);
		m_LegRight->SetAlpha(blend);
	}

	void AddWidgetsBody()
	{
#if __BANK
		bkBank &ikBank = BANKMGR.CreateBank("testbody",50,500);
		ikBank.AddToggle("Draw IK Goal", &m_DrawIKGoal);
		ikBank.AddSlider("IK Icon Size", &m_IKGoalIconSize, 0.0f, 10.0f, 0.1f);
		ikBank.AddSlider("IK Icon Scale", &m_IKGoalIconScale, 0.0f, 10.0f, 0.1f);
		ikBank.AddToggle("Enable IK", &m_EnableIK, datCallback(MFA(craSampleBody::EnableIKCB), this));
		ikBank.AddToggle("Update IK Goal", &m_UpdateIKGoal);
//		ikBank.PopGroup();

		BANKMGR.ActivateBank("testbody");
#endif // __BANK
	}

private:
	crAnimation* m_Animation;
	crFrame* m_Frame;
	crAnimPlayer* m_Player;
	crKinematics* m_Kinematics;
	crIKBodyHumanoid* m_Body;
	crJointData* m_JointData;
	crIKSolverGleicherLimb* m_LegLeft, *m_LegRight;
	crIKSolverGleicherLimb* m_ArmLeft, *m_ArmRight;
	bool m_EnableIK;
	bool m_UpdateIKGoal;
	bool m_DrawIKGoal;
	float m_IKGoalIconSize;
	float m_IKGoalIconScale;
};

// main application
int Main()
{
	craSampleBody sampleCra;
	sampleCra.Init("sample_rmcore\\drawable");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
