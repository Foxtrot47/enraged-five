//
// sample_cranimation/sample_anim.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Animation
// PURPOSE:
//		This sample shows how to load an animation, pose a skeleton, and render a skinned model.
//		The purpose of this sample is to demonstrate the lowest level steps involved in animation
//		playback.
//		In reality, it is easier to use a medium or higher level class to play animations, such as 
//		the simple animation player <c>crAnimPlayer</c> or motion trees <c>crmtMotionTree</c>.

using namespace rage;

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define DEFAULT_FILE "$/rage_male/rage_male.type"
XPARAM(file);

#define DEFAULT_ANIM "$/animation/motion/male_wlk.anim"
PARAM(anim,"[sample_anim] Animation file to load (default is \"" DEFAULT_ANIM "\")");
PARAM(animrsc,"[sample_anim] Animation resource file to load");

class craSampleAnimation : public ragesamples::craSampleManager
{
public:
	craSampleAnimation() : ragesamples::craSampleManager()
		, m_Animation(NULL)
		, m_Frame(NULL)
		, m_AnimTime(0.f)
	{
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		// STEP #1. Load our animation.
		const char *animRsc = NULL;
		PARAM_animrsc.Get(animRsc);
		if(animRsc)
		{
			datResourceMap map;
			datResourceInfo hdr;
			datResource rsc(map, NULL);
			m_Animation = (crAnimation*) pgRscBuilder::LoadBuild(animRsc, "#an", crAnimation::RORC_VERSION,map,hdr);
			m_Animation->Place(m_Animation,rsc);
			pgStreamer::ShutdownClass();
		}
		else
		{
			// -- Call <c>crAnimation::AllocateAndLoad()</c> with the 
			// animation name and it'll return you a loaded animation.
			const char *animName = DEFAULT_ANIM;
			PARAM_anim.Get(animName);
			m_Animation=crAnimation::AllocateAndLoad(animName, NULL, true);
		}

		// STEP #2. Initialize our animation frame.
			
		// -- If we need to get a single frame from the animation, we must have a data
		// structure that will store it.  We create an instance of <c>crFrame</c>
		// to store this data.
		m_Frame=rage_new crFrame;

		// -STOP

		if(m_Animation != NULL)
		{
			// -- Initialize the animation frame based on the degrees of freedom
			// found in the skeleton data.
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
			m_Frame->InitCreateBoneAndMoverDofs(skelData, false);

			// -- Initialize the values of the degrees of freedom in the newly created 
			// frame to identity.
			m_Frame->IdentityFromSkel(skelData);

			// -STOP
		}

#if __BANK
		AddWidgetsAnimation();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Animation;
		delete m_Frame;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{	
		crSkeleton& skeleton = GetSkeleton();

		if (m_Animation != NULL)
		{
			// STEP #3. Request frame data from the animation.

			//-- To retrieve a frame from an animation we call <c>crFrame::Composite()</c>.
			// We pass a reference to the animation as the first parameter
			// and we pass as the second parameter the time index (in seconds) of the animation we wish to access as the first
			m_Frame->Composite(*m_Animation, m_AnimTime);

			// -STOP

			// STEP #4. Pose the skeleton.

			//-- Now we take the frame data and pose the bones in the skeleton.  To do this, just pass the 
			// a reference to the skeleton to be posed.  
			// Posing sets the local matrices in skeleton.
			m_Frame->Pose(skeleton);

			// -STOP
		}
		else
		{
			skeleton.Reset();
		}
			
		// STEP #5. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
		skeleton.Update();
		UpdateMatrixSet();

		// -STOP

		if (GetPlay() && m_Animation != NULL)
		{
			// STEP #6. Advance time.

			//-- Advance time in the animation.  
			// We do this by multiplying the game clock with the playback rate.
			m_AnimTime += TIME.GetSeconds() * GetPlaybackRate();

			// -STOP

			if(m_AnimTime > m_Animation->GetDuration())
			{
				m_AnimTime = fmodf(m_AnimTime, m_Animation->GetDuration());
			}
		}
	}

#if __BANK
	void AddWidgetsAnimation()
	{
		bkBank &animBank=BANKMGR.CreateBank("Animation",50,500);

		float maxTime = m_Animation ? m_Animation->GetDuration() : 0.f;
		animBank.AddSlider("Animation Time", &m_AnimTime, 0.f, maxTime, 0.01f);
	}
#endif // __BANK

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

private:
	crAnimation*			m_Animation;
	crFrame*			m_Frame;
	float					m_AnimTime;
};

// main application
int Main()
{
	craSampleAnimation sampleCra;
	sampleCra.Init("sample_cranimation\\NewSkel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
