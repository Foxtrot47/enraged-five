// 
// sample_cranimation/sample_blendn.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Animation blending (N-way)
// PURPOSE:
//		This sample shows how to blend multiple animations together, pose the result
//		on a skeleton, and render this using a skinned model.
//		There are many different ways to blend multiple animations or frames of
//		data together.  This sample demonstrates how to use one of the N-way operations,
//		crFrame::CompositeBlendN to both composite and blend multiple animations
//		simulatenously, whilst using only a single intermediate frame.


using namespace rage;

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timemgr.h"

const char* DefaultAnims[] = 
{
	"gent_nor_hnd_lft_dwn_pck",
	"gent_nor_hnd_lft_mid_pck",
	"gent_nor_hnd_lft_upp_pck"
};

PARAM(anims,"[sample_blendn] Animation files to load (Example: file0.anim,file1.anim,file2.anim");

class craSampleBlendN : public ragesamples::craSampleManager
{
public:
	craSampleBlendN() : ragesamples::craSampleManager()
		, m_Frame(NULL)
		, m_PercentageWeights(true)
	{
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		// STEP #1. Load the animations, set up the players.

		const int maxAnimNames = 10;
		const char* animNames[maxAnimNames];
		const int animNameBufSize = 8192;
		char animNameBuf[animNameBufSize];

		// -- animations are specified as a comma seperated parameter
		int numAnims = PARAM_anims.GetArray(animNames, maxAnimNames, animNameBuf, animNameBufSize);
		if(!numAnims)
		{
			// -- or use default animations, if none provided on the command line
			numAnims = NELEM(DefaultAnims);
			for(int i=0; i<numAnims; i++)
			{
				animNames[i] = DefaultAnims[i];
			}
		}

		// -- a helper class, Constituent, takes care of allocation, loading and playback
		m_Constituents.Resize(numAnims);
		for(int i=0; i<numAnims; i++)
		{
			Constituent& c = m_Constituents[i];
			c.Init(animNames[i]);
			c.SetWeight(1.f / float(numAnims));
		}

		// -STOP


		// STEP #2. Initialize our single intermediate animation frame.

		m_Frame=rage_new crFrame;

		const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
		m_Frame->InitCreateBoneAndMoverDofs(skelData, false);
		m_Frame->IdentityFromSkel(skelData);

		m_CompositeFrames.Resize(numAnims);
		for(int i=0; i < numAnims; ++i)
		{
			m_CompositeFrames[i] = rage_new crFrame;
			m_CompositeFrames[i]->InitCreateBoneAndMoverDofs(skelData, false);
		}
		// -STOP

#if __BANK
		AddWidgetsBlendN();
#endif // __BANK
	}

	void ShutdownClient()
	{
		m_Constituents.Reset();
		delete m_Frame;
		for(int i=0; i < m_CompositeFrames.GetCount(); ++i)
		{
			delete m_CompositeFrames[i];
		}
		m_CompositeFrames.Reset();

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		// normalize the weights, prevents percentage based weights exceeding 100%
		NormalizeWeights();

		crSkeleton& skeleton = GetSkeleton();


		// STEP #3. Composite the frame using n-way compositing.

		const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
		m_Frame->IdentityFromSkel(skelData);

		const int numConstituents = m_Constituents.GetCount();

		// -- The N-way composite operation on the animation frame needs 
		// arrays of weights, animation pointers and time indexes as parameters.  
		// So we allocate some temporary buffers on the stack for this.
		float* weights = Alloca(float, numConstituents);

		// -- Setup the temporary arrays with the values retrieved from the 
		// Constituent classes

		// Compute weights
		for(int i=0; i<numConstituents; i++)
		{
			const Constituent& c = m_Constituents[i];

			weights[i] = c.GetWeight();
			m_CompositeFrames[i]->Composite(*c.GetAnimation(), c.GetTime());
		}

		// -- Call the n-way composite and blend frame function on the destination
		// intermediate frame.  
		// - In addition to the number of animations we are compositing and blending, 
		// we pass in the the temporary weight, animation pointer and time index arrays.
		// - The percentage weights parameter controls how the weights are interpreted,
		// they can either be considered as expressing percentages of the final frame, or
		// directly as raw weights that will be used in sequence when applying the blends.
		// Whilst the latter method gives more control, allowing more complex blend operations, 
		// typically the former, weights as percentages, is the method you will want to use.
		// - There's more detailed discussion of this on the notes to the N-way function calls.
		m_Frame->BlendN(numConstituents, weights, &m_CompositeFrames[0]);

		// -STOP


		// STEP #4. Pose the skeleton.

		m_Frame->Pose(skeleton);

		// -STOP


		// STEP #6. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
		skeleton.Update();
		UpdateMatrixSet();

		// -STOP
		if (GetPlay())
		{
			// STEP #7. Update the animation players.

			for(int i=0; i<m_Constituents.GetCount(); i++)
			{
				Constituent& c = m_Constituents[i];
				c.Update(TIME.GetSeconds(), GetPlaybackRate());
			}
		}
	}

	void NormalizeWeights()
	{
		// If weights are expressing a percentage, check that they don't exceed 100%, and normalize if they do.
		if(m_PercentageWeights)
		{
			float sum = 0.f;
			for(int i=0; i<m_Constituents.GetCount(); i++)
			{
				const Constituent& c = m_Constituents[i];
				sum += c.GetWeight();
			}

			if(sum > 1.f)
			{
				float invSum = 1.f / sum;
				for(int i=0; i<m_Constituents.GetCount(); i++)
				{
					Constituent& c = m_Constituents[i];
					c.SetWeight(c.GetWeight()*invSum);
				}
			}
		}
	}
#if __BANK
	void AddWidgetsBlendN()
	{
		bkBank &blendNBank = BANKMGR.CreateBank("BlendN", 50, 500);
		blendNBank.AddToggle("Percentage Weights", &m_PercentageWeights);

		for(int i=0; i<m_Constituents.GetCount(); i++)
		{
			Constituent& c = m_Constituents[i];

			char buf[64];
			sprintf(buf, "Constituent[%d]", i);

			blendNBank.PushGroup(buf);
			c.AddWidgets(blendNBank);
			blendNBank.PopGroup();
		}
	}
#endif // __BANK

private:
	struct Constituent
	{
		Constituent()
			: m_Player(NULL)
			, m_Weight(0.f)
		{
		}

		~Constituent()
		{
			delete m_Player;
		}

		void Init(const char* animName)
		{
			crAnimation* anim = crAnimation::AllocateAndLoad(animName);
			if(!anim)
			{
				Quitf("Failed to load animation '%s'", animName);
			}
			m_Player = rage_new crAnimPlayer(anim);

			// we release our reference to the animation, so the player has the controlling reference to it
			// this way when the player is destroyed, the animation will automatically be destroyed as well.
			anim->Release();
		}

		const crAnimation* GetAnimation() const
		{
			return m_Player->GetAnimation();
		}

		float GetTime() const
		{
			return m_Player->GetTime();
		}

		float GetWeight() const
		{
			return m_Weight;
		}

		void SetWeight(float weight) 
		{
			m_Weight = weight;
		}

		void Update(float delta, float rate)
		{
			m_Player->SetRate(rate);
			m_Player->Update(delta);	
		}

#if __BANK
		void AddWidgets(bkBank& bk)
		{
			bk.AddSlider("Weight", &m_Weight, 0.f, 1.f, 0.01f);
		}
#endif // __BANK

	private:
		crAnimPlayer* m_Player;
		float m_Weight;
	};

	atArray<Constituent> m_Constituents;
	crFrame* m_Frame;
	atArray<crFrame*> m_CompositeFrames;
	bool m_PercentageWeights;
};


// main application
int Main()
{
	craSampleBlendN sampleCraBlendN;
	sampleCraBlendN.Init("sample_cranimation\\NewSkel");

	sampleCraBlendN.UpdateLoop();

	sampleCraBlendN.Shutdown();

	return 0;
}
