// 
// sample_cranimation/sample_filter.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Animation frame filters
// PURPOSE:
//		This sample shows how to use a basic frame filter.
//		Frame filters are optional parameters that can be passed into almost any operation
//		performed on a an animation frame (initializing, setting, blending, posing etc).
//		They provide control of the frame operation on a per degree-of-freedom level.
//		As each of the dofs within the frame(s) are iterated, the filter 
//		receives a callback.  It can use this call to control whether the operation
//		will take place on that dof (and also in non-binary operations, provide a 
//		multiplier to the weight used in the operation).
//		In this sample, a filter is used to control which dofs participate in a set
//		operation between two frames.

using namespace rage;

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "cranimation/animplayer.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"


#define NUM_ANIMATIONS (2)
#define DEFAULT_ANIM0 "Gent_nor_hnd_for_wlk"
#define DEFAULT_ANIM1 "gent_nor_hnd_lft_mid_pck"
PARAM(anim0,"[sample_blend] First animation file to load (default is \"" DEFAULT_ANIM0 "\")");
PARAM(anim1,"[sample_blend] Second animation file to load (default is \"" DEFAULT_ANIM1 "\")");

class craSampleFilter : public ragesamples::craSampleManager
{
public:
	craSampleFilter() : ragesamples::craSampleManager()
		, m_Animation0(NULL)
		, m_Animation1(NULL)
		, m_Frame0(NULL)
		, m_Frame1(NULL)
		, m_Player0(NULL)
		, m_Player1(NULL)
		, m_Filter(NULL)
	{
	}


protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		// STEP #1. Load our animations.
		const char* animName0 = DEFAULT_ANIM0;
		PARAM_anim0.Get(animName0);

		const char* animName1 = DEFAULT_ANIM1;
		PARAM_anim1.Get(animName1);

		// -- Call <c>crAnimation::AllocateAndLoad()</c> with the 
		// animation name and it'll return you a loaded animation.
		m_Animation0=crAnimation::AllocateAndLoad(animName0);
		m_Animation1=crAnimation::AllocateAndLoad(animName1);


		// STEP #2. Initialize the simple animation players.
		m_Player0 = rage_new crAnimPlayer(m_Animation0);
		m_Player1 = rage_new crAnimPlayer(m_Animation1);

		// -- Force the players to loop the animations.
		m_Player0->SetLooped(true, true);
		m_Player1->SetLooped(true, true);

		// -STOP


		// STEP #3. Initialize our animation frames.

		// -- If we need to get a single frame from the animation, we must have a data
		// structure that will store it.  We create an instance of <c>crFrame</c>
		// to store this data.
		m_Frame0=rage_new crFrame;
		m_Frame1=rage_new crFrame;

		// -STOP

		if(m_Animation0 != NULL && m_Animation1 != NULL)
		{
			// -- Initialize the animation frame based on the degrees of freedom
			// found in the skeleton data.
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
			m_Frame0->InitCreateBoneAndMoverDofs(skelData, false);
			m_Frame1->InitCreateBoneAndMoverDofs(skelData, false);

			// -- Initialize the values of the degrees of freedom in the newly created 
			// frame to identity.
			m_Frame0->IdentityFromSkel(skelData);
			m_Frame1->IdentityFromSkel(skelData);

			// -STOP


			// STEP #4. Intialize the frame filter.
			m_Filter = rage_new crFrameFilterBoneBasic(skelData);

			// -- Modify the filter to accept upper body bones.
			// This is accomplished by finding the start of the spine,
			// and adding this (and all it's children) to the filter.
			// This will cause all the upper body bones to be added, whilst
			// excluding all the lower body bones (as they are parented off the 
			// pelvis, not the spine).
			const crBoneData* bd = skelData.FindBoneData("spine00");
			if(bd)
			{
				// - The second parameter ensures all child bones are added as well.
				m_Filter->AddBoneIndex(bd->GetIndex(), true);
			}

			// -STOP
		}

#if __BANK
		AddWidgetsFilter();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Player0;
		delete m_Player1;
		delete m_Animation0;
		delete m_Animation1;
		delete m_Frame0;
		delete m_Frame1;
		delete m_Filter;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		crSkeleton& skeleton = GetSkeleton();

		if(m_Animation0 != NULL && m_Animation1 != NULL)
		{
			// STEP #5. Request frame data from the players.

			//-- Retrieve the current frames from the players by calling
			// <c>crAnimPlayer::Composite()</c> passing in a reference
			// to the frame structure.
			m_Player0->Composite(*m_Frame0);
			m_Player1->Composite(*m_Frame1);


			// STEP #6. Perform the set operation between the frames.

			//-- Set the contents of frame 0 from frame 1 using <c>crFrame::Set()</c>.
			// The contents of frame 0 would usually be completely replaced 
			// by those in frame 1 (assuming the dofs in frame 1 match or are a superset of 
			// those in frame 0).  However, the filter controls the set operation on a per
			// dof level, allowing selected dofs in frame 0 to survive the set operation.
			m_Frame0->Set(*m_Frame1, m_Filter);


			// STEP #7. Pose the skeleton.

			//-- Now we take the frame data and pose the bones in the skeleton.  
			// To do this, just pass the frame a reference to the skeleton to be posed.  
			// Posing sets the local matrices in skeleton.
			m_Frame0->Pose(skeleton);

			// -STOP

		}
		else
		{
			skeleton.Reset();
		}

		// STEP #8. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
		skeleton.Update();
		UpdateMatrixSet();

		// -STOP

		if (GetPlay() && m_Animation0 != NULL && m_Animation1 != NULL)
		{
			// STEP #9. Update the players.

			// -- First we ensure that the players are updated with the current playback rate.
			m_Player0->SetRate(GetPlaybackRate());
			m_Player1->SetRate(GetPlaybackRate());

			// -- Now we advance time in the player by passing in the game clock delta.
			// The player automatically takes care of looping the animation for us.
			m_Player0->Update(TIME.GetSeconds());
			m_Player1->Update(TIME.GetSeconds());
		}
	}

#if __BANK
	void AddWidgetsFilter()
	{
		bkBank &filterBank = BANKMGR.CreateBank("Filter", 50, 500);
		if(m_Filter)
		{
			m_Filter->AddWidgets(filterBank);
		}
	}
#endif // __BANK

private:
	crAnimation* m_Animation0;
	crAnimation* m_Animation1;
	crFrame* m_Frame0;
	crFrame* m_Frame1;
	crAnimPlayer* m_Player0;
	crAnimPlayer* m_Player1;
	crFrameFilterBoneBasic* m_Filter;
};


// main application
int Main()
{
	craSampleFilter sampleCraFilter;
	sampleCraFilter.Init("sample_cranimation\\NewSkel");

	sampleCraFilter.UpdateLoop();

	sampleCraFilter.Shutdown();

	return 0;
}
