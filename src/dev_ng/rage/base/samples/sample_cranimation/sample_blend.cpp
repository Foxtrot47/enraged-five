// 
// sample_cranimation/sample_blend.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


// TITLE: Animation blending (pairs)
// PURPOSE:
//		This sample shows how to blend a pair of animations together, pose the result
//		on a skeleton, and render this using a skinned model.

using namespace rage;

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define NUM_ANIMATIONS (2)
#define DEFAULT_ANIM0 "Gent_nor_hnd_for_wlk"
#define DEFAULT_ANIM1 "gent_nor_hnd_lft_mid_pck"
PARAM(anim0,"[sample_blend] First animation file to load (default is \"" DEFAULT_ANIM0 "\")");
PARAM(anim1,"[sample_blend] Second animation file to load (default is \"" DEFAULT_ANIM1 "\")");

class craSampleBlend : public ragesamples::craSampleManager
{
public:
	craSampleBlend() : ragesamples::craSampleManager()
		, m_Animation0(NULL)
		, m_Animation1(NULL)
		, m_Frame(NULL)
		, m_Player0(NULL)
		, m_Player1(NULL)
		, m_Weight(0.5f)
	{
	}


protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		// STEP #1. Load our animations.
		const char* animName0 = DEFAULT_ANIM0;
		PARAM_anim0.Get(animName0);

		const char* animName1 = DEFAULT_ANIM1;
		PARAM_anim1.Get(animName1);

		// -- Call <c>crAnimation::AllocateAndLoad()</c> with the 
		// animation name and it'll return you a loaded animation.
		m_Animation0=crAnimation::AllocateAndLoad(animName0);
		m_Animation1=crAnimation::AllocateAndLoad(animName1);


		// STEP #2. Initialize the simple animation players.
		m_Player0 = rage_new crAnimPlayer(m_Animation0);
		m_Player1 = rage_new crAnimPlayer(m_Animation1);

		// -- Force the players to loop the animations.
		m_Player0->SetLooped(true, true);
		m_Player1->SetLooped(true, true);

		// -STOP


		// STEP #3. Initialize our animation frame.

		// -- If we need to get a single frame from the animation, we must have a data
		// structure that will store it.  We create an instance of <c>crFrame</c>
		// to store this data.
		m_Frame=rage_new crFrame;

		// -STOP

		if(m_Animation0 != NULL && m_Animation1 != NULL)
		{
			// -- Initialize the animation frame based on the degrees of freedom
			// found in the skeleton data.
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
			m_Frame->InitCreateBoneAndMoverDofs(skelData, false);

			// -- Initialize the values of the degrees of freedom in the newly created 
			// frame to identity.
			m_Frame->IdentityFromSkel(skelData);

			// -STOP
		}

#if __BANK
		AddWidgetsBlend();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Player0;
		delete m_Player1;
		delete m_Animation0;
		delete m_Animation1;
		delete m_Frame;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		crSkeleton& skeleton = GetSkeleton();

		if(m_Animation0 != NULL && m_Animation1 != NULL)
		{
			// STEP #4. Request frame data from the player.

			//-- To retrieve a frame from animation 0 we call 
			// <c>crFrame::Composite()</c>.  We pass in a reference
			// to the animation, and the time index (in seconds) of the point
			// in the animation we wish to retrieve.
			m_Frame->Composite(*m_Animation0, m_Player0->GetTime());

			// -- Next, we want to retrieve a frame from animation 1, and 
			// blend this with the frame already retrieved from animation 0.
			// We could do this with a composite using a second intermediate 
			// frame, and then blending together the two frames, but there is 
			// a call that combines these two operations for us more efficiently
			// <c>crFrame::CompositeBlend()</c>.  
			// As with <c>crFrame::Composite()</c> we pass in an animation and 
			// time index, but we also supply a weight parameter.  This controls the
			// blend amount of blending between the data already in the frame, and
			// the new data coming from the composite with animation 1.
			m_Frame->CompositeBlend(m_Weight, *m_Animation1, m_Player1->GetTime());

			// -STOP


			// STEP #5. Pose the skeleton.

			//-- Now we take the frame data and pose the bones in the skeleton.  
			// To do this, just pass the frame a reference to the skeleton to be posed.  
			// Posing sets the local matrices in skeleton.
			m_Frame->Pose(skeleton);

			// -STOP

		}
		else
		{
			skeleton.Reset();
		}

		// STEP #6. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
		skeleton.Update();
		UpdateMatrixSet();

		// -STOP

		if (GetPlay() && m_Animation0 != NULL && m_Animation1 != NULL)
		{
			// STEP #7. Update the players.

			// -- First we ensure that the players are updated with the current playback rate.
			m_Player0->SetRate(GetPlaybackRate());
			m_Player1->SetRate(GetPlaybackRate());

			// -- Now we advance time in the player by passing in the game clock delta.
			// The player automatically takes care of looping the animation for us.
			m_Player0->Update(TIME.GetSeconds());
			m_Player1->Update(TIME.GetSeconds());
		}
	}

#if __BANK
	void AddWidgetsBlend()
	{
		bkBank &blendBank = BANKMGR.CreateBank("Blend", 50, 500);
		blendBank.AddSlider("Weight", &m_Weight, 0.f, 1.f, 0.001f);
	}
#endif // __BANK

private:
	crAnimation* m_Animation0;
	crAnimation* m_Animation1;
	crFrame* m_Frame;
	crAnimPlayer* m_Player0;
	crAnimPlayer* m_Player1;
	float m_Weight;
};


// main application
int Main()
{
	craSampleBlend sampleCraBlend;
	sampleCraBlend.Init("sample_cranimation\\NewSkel");

	sampleCraBlend.UpdateLoop();

	sampleCraBlend.Shutdown();

	return 0;
}
