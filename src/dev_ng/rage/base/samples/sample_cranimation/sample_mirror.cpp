#include "sample_cranimation.h"

#include "cranimation/animation.h"
#include "cranimation/animplayer.h"
#include "cranimation/frame.h"
#include "creature/creature.h"
#include "crskeleton/skeletondata.h"
#include "grblendshapes/manager.h"
#include "grmodel/matrixset.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define DEFAULT_ANIM "walk.anim"
PARAM(anim, "[sample_mirror] Animation file to load (default is \"" DEFAULT_ANIM "\")");

#define DEFAULT_FILE "entity.type"
XPARAM(file);

class craSampleMirror : public ragesamples::craSampleManager
{
	crAnimation* m_Animation;
	crFrame* m_Frame;
	crAnimPlayer* m_Player;

	struct DrawableSkeleton {
		crCreature* m_Creature;
		grmMatrixSet* m_MatrixSet;
		rmcDrawable* m_Drawable;
		grbTargetManager* m_TargetManager;
		crSkeleton* m_Skeleton;
		Matrix34* m_MoverMtx;
	} m_Mirror;

	crFrame* m_MirrorFrame;
	crAnimPlayer* m_MirrorPlayer;
	u32 m_MirrorLastDrawUpdateFrame;
public:
	craSampleMirror() 
		:  ragesamples::craSampleManager()
		,	m_Animation (NULL)
		,	m_Frame (NULL)
		,	m_Player (NULL)
		,	m_MirrorFrame (NULL)
		,	m_MirrorPlayer (NULL)
		,	m_MirrorLastDrawUpdateFrame (~0u)
	{
		PARAM_file.Set(DEFAULT_FILE);
	}
protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		SetZUp(true);

		const char* anim = DEFAULT_ANIM;
		PARAM_anim.Get(anim);

		m_Animation = crAnimation::AllocateAndLoad(anim);

		m_Player = rage_new crAnimPlayer(m_Animation);
		m_Player->SetLooped(true, true);
		m_Frame = rage_new crFrame;

		if (m_Animation) {
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
			m_Frame->InitCreateBoneAndMoverDofs(skelData, false);
			m_Frame->IdentityFromSkel(skelData);
		};

		// Setup the mirrored creature
		CreateDrawableSkeleton(m_Mirror.m_Drawable, m_Mirror.m_TargetManager, m_Mirror.m_Skeleton, (Mat34V*&)m_Mirror.m_MoverMtx, m_Mirror.m_Creature, m_Mirror.m_MatrixSet);

		m_MirrorPlayer = rage_new crAnimPlayer(m_Animation);
		m_MirrorPlayer->SetLooped(true, true);
		m_MirrorFrame = rage_new crFrame;

		if (m_Animation) {
			const crSkeletonData& skelData = m_Mirror.m_Skeleton->GetSkeletonData();
			m_MirrorFrame->InitCreateBoneAndMoverDofs(skelData, false);
			m_MirrorFrame->IdentityFromSkel(skelData);
		}

		m_Mirror.m_MoverMtx->Translate(-1.f, 0.f, 0.f);
	}

	void ShutdownClient()
	{
		delete m_Mirror.m_MatrixSet;
		delete m_Mirror.m_Creature;
		delete m_Mirror.m_Drawable;
		delete m_Mirror.m_Skeleton;
		delete m_Mirror.m_MoverMtx;
#if ENABLE_BLENDSHAPES
		m_Mirror.m_TargetManager->Release();
#endif // ENABLE_BLENDSHAPES

		delete m_MirrorPlayer;
		delete m_MirrorFrame;

		delete m_Player;
		delete m_Animation;
		delete m_Frame;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		crSkeleton& skeleton = GetSkeleton();
		if (m_Animation != NULL)
		{
			m_Player->Composite(*m_Frame);
			m_Frame->Pose(skeleton);

			m_MirrorPlayer->Composite(*m_MirrorFrame);
			m_MirrorFrame->Mirror(m_Mirror.m_Skeleton->GetSkeletonData());
			m_MirrorFrame->Pose(*m_Mirror.m_Skeleton);
		}
		else 
		{
			skeleton.Reset();
			m_Mirror.m_Skeleton->Reset();
		}

		UpdateCharacter(GetSkeleton(), GetMatrixSet());
		UpdateCharacter(*m_Mirror.m_Skeleton, *m_Mirror.m_MatrixSet);

		if (GetPlay() && m_Animation != NULL)
		{
			m_Player->SetRate(GetPlaybackRate());
			m_Player->Update(TIME.GetSeconds());

			m_MirrorPlayer->SetRate(GetPlaybackRate());
			m_MirrorPlayer->Update(TIME.GetSeconds());
		}
	}

	void DrawClient()
	{
		craSampleManager::DrawClient(); // Draw the 'default' creature

		if (m_Mirror.m_Creature)
		{
			u32 thisFrame = GRCDEVICE.GetFrameCounter();
			if (m_MirrorLastDrawUpdateFrame != thisFrame)
			{
				m_Mirror.m_Creature->SwapBuffers();
				m_Mirror.m_Creature->DrawUpdate();
				m_MirrorLastDrawUpdateFrame = thisFrame;
			}
			m_Mirror.m_Creature->PreDraw(true);
		}

		// Draw the creature
		u8 lod;
		if (m_Mirror.m_Drawable->IsVisible(*m_Mirror.m_MoverMtx, *grcViewport::GetCurrent(), lod))
		{
			float zDist = GetCamMgr().GetWorldMtx().d.Mag();
			grcTextureFactory::GetInstance().SetTextureLod(zDist);
			for (int bucket = 0; bucket < NUM_BUCKETS; ++bucket)
			{
				if (GetBucketEnable(bucket))
				{
					if (m_Mirror.m_Drawable->GetSkeletonData())
					{
						m_Mirror.m_Drawable->DrawSkinned(*m_Mirror.m_MoverMtx, *m_Mirror.m_MatrixSet, BUCKETMASK_GENERATE(bucket), lod);
					}
					else
					{
						m_Mirror.m_Drawable->Draw(*m_Mirror.m_MoverMtx, BUCKETMASK_GENERATE(bucket), lod);
					}
				}
			}
		}
	}

	static void UpdateCharacter(crSkeleton& skeleton, grmMatrixSet& matrixSet)
	{
		//-- Update procedural roll bones
		UpdateRollBone(skeleton,	Neck,		Neck_Roll,			X_AXIS,		1.0f, false);
		UpdateRollBone(skeleton,	L_Forearm,	L_ForeTwist,		X_AXIS,		1.0f, false);
		UpdateRollBone(skeleton,	R_Forearm,	R_ForeTwist,		X_AXIS,		1.0f, false);
		UpdateRollBone(skeleton,	L_UpperArm,	L_UpperArmRoll,		X_AXIS,		0.5f, true);
		UpdateRollBone(skeleton,	R_UpperArm,	R_UpperArmRoll,		X_AXIS,		0.5f, true);

		skeleton.Update();
		matrixSet.Update(skeleton, true);
	}

	enum BoneIndex {Root,Pelvis,L_Thigh,L_Calf,L_Foot,L_Toe0,L_Toe0Nub,L_Calf_Roll,R_Thigh,R_Calf,R_Foot,R_Toe0,R_Toe0Nub,R_Calf_Roll,Spine,Spine1,Spine2,Neck,Head,HeadNub,Neck_Roll,L_Clavicle,L_UpperArm,L_Forearm,L_Hand,L_Finger0,L_Finger01,L_Finger02,L_Finger0Nub,L_Finger1,L_Finger11,L_Finger12,L_Finger1Nub,L_Finger2,L_Finger21,L_Finger22,L_Finger2Nub,L_Finger3,L_Finger31,L_Finger32,L_Finger3Nub,L_ForeTwist,L_ForeTwist1,L_UpperArmRoll,R_Clavicle,R_UpperArm,R_Forearm,R_Hand,R_Finger0,R_Finger01,R_Finger02,R_Finger0Nub,R_Finger1,R_Finger11,R_Finger12,R_Finger1Nub,R_Finger2,R_Finger21,R_Finger22,R_Finger2Nub,R_Finger3,R_Finger31,R_Finger32,R_Finger3Nub,R_ForeTwist,R_ForeTwist1,R_UpperArmRoll,Spine_2_roll	};
	enum RollBoneAxis { X_AXIS, Y_AXIS, Z_AXIS };

	static float LimitAngle(float fLimitAngle)
	{
		while (fLimitAngle >= PI)
			fLimitAngle -= 2.0f*PI;

		while (fLimitAngle < -PI)
			fLimitAngle += 2.0f*PI;

		return(fLimitAngle);
	}

	static void UpdateRollBone( crSkeleton& skeleton, const int originalBoneIndex, const int rollBoneIndex, const RollBoneAxis rollBoneAxis, float percentageRoll, bool child)
	{
		// Check the axis is valid
		Assert(rollBoneAxis == X_AXIS || rollBoneAxis == Y_AXIS || rollBoneAxis == Z_AXIS);

		// Check the percentageRoll is valid
		Assert(percentageRoll >= 0.0f && percentageRoll <= 1.0f);

		// Get the original bone matrix
		Mat34V originalBoneMat = skeleton.GetLocalMtx(originalBoneIndex);

		//
		// Calculate the new rotation and position of the roll bone
		//

		// Convert the original bones rotation from matrix to Euler angle form
		Vec3V rotationInEulers = Mat33VToEulersXYZ(originalBoneMat.GetMat33());

		// Rotate the chosen axis by the specified percentage while conserving the other two axes
		switch(rollBoneAxis)
		{
		case X_AXIS: 
			if (child)
			{
				rotationInEulers[0] *= percentageRoll; 
				rotationInEulers[0] += PI; 
				rotationInEulers[0] = LimitAngle(rotationInEulers[0]);
				rotationInEulers[1] = 0.0f;
				rotationInEulers[2] = 0.0f;
			}
			else
			{
				rotationInEulers[0] *= percentageRoll; 
			}
			break;

		case Y_AXIS: 
			if (child)
			{
				rotationInEulers[0] = 0.0f; 
				rotationInEulers[1] *= percentageRoll;
				rotationInEulers[1] += PI; 
				rotationInEulers[1] = LimitAngle(rotationInEulers[1]);
				rotationInEulers[2] = 0.0f;
			}
			else
			{
				rotationInEulers[1] *= percentageRoll;
			}
			break;

		case Z_AXIS: 
			if (child)
			{
				rotationInEulers[0] = 0.0f; 
				rotationInEulers[1] = 0.0f; 
				rotationInEulers[2] *= percentageRoll;
				rotationInEulers[2] += PI; 
				rotationInEulers[2] = LimitAngle(rotationInEulers[2]);
			}
			else
			{
				rotationInEulers[2] *= percentageRoll; 
			}
			break;

		default:
			break;
		}

		// Get the roll bone matrix
		Mat34V& rollBoneMat = skeleton.GetLocalMtx(rollBoneIndex);

		// Convert the new rotation from Euler angles back to matrix form
		Mat34VFromEulersXYZ(rollBoneMat, rotationInEulers);

		if (!child)
		{
			// Retain the original position of the roll bone
			rollBoneMat.SetCol3(originalBoneMat.GetCol3());
		}
	}
};

int Main()
{
	craSampleMirror sample;
	sample.Init("sample_cranimation\\NorthSkel");
	sample.UpdateLoop();
	sample.Shutdown();

	return 0;
}