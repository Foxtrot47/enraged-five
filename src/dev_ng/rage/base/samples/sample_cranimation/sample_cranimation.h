//
// sample_cranimation/sample_cranimation.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SAMPLE_CRANIMATION_SAMPLE_CRANIMATION_H
#define SAMPLE_CRANIMATION_SAMPLE_CRANIMATION_H

#include "cranimation/animation.h"
#include "cranimation/commonpool.h"
#include "cranimation/frameaccelerator.h"
#include "cranimation/framedatafactory.h"
#include "crskeleton/skeleton.h"

#include "sample_rmcore/sample_rmcore.h"

// pre declarations
namespace rage
{
	class rmcDrawable;
	class crCreature;
	class crSkeleton;
	class grbTargetManager;
	class grmMatrixSet;
};


namespace ragesamples 
{

using namespace rage;

class craSampleManager : public rmcSampleManager
{
public:
	craSampleManager();

	virtual void Init(const char* path=NULL);
	virtual void Shutdown();

	virtual void InitClient();
	virtual void ShutdownClient();

	virtual void UpdateClient();
	virtual void DrawClient();

protected:
	void UpdateMatrixSet();

	// PURPOSE: Override this to provide your own skeleton (ie fragments)
	virtual void CreateDrawableSkeleton(rmcDrawable*& drawable, grbTargetManager*& manager, crSkeleton*& skeleton, Mat34V*& moverMtx, crCreature*& creature, grmMatrixSet*& matrixSet);

	// PURPOSE: Override this to provide/initialize your own creature
	virtual void CreateCreature(crCreature*& creature);


	// PURPSOE: Override this to destroy your own skeleton
	virtual void ShutdownDrawableSkeleton();

	// PURPOSE: Override this to provide an alternate default file
	virtual const char* GetDefaultFile();

	crCreature& GetCreature()		{ Assert(m_Creature); return *m_Creature; }
	rmcDrawable& GetDrawable()		{ Assert(m_Drawable); return *m_Drawable; }
	grbTargetManager& GetTargetManager() { Assert(m_TargetManager); return *m_TargetManager; }
	const crSkeleton& GetSkeleton() const { Assert(m_Skeleton); return *m_Skeleton; }
	crSkeleton& GetSkeleton()		{ Assert(m_Skeleton); return *m_Skeleton; }
	Mat34V& GetMoverMtx()			{ Assert(m_MoverMtx); return *m_MoverMtx; }
	grmMatrixSet& GetMatrixSet()	{ Assert(m_MatrixSet); return *m_MatrixSet; }

	void DrawSkeleton(crSkeleton& skel, grmMatrixSet* matrixSet);
	void DrawBones(crSkeleton& skel, Color32 c = Color32(255,0,255));
	void DrawJoints(crSkeleton& skel, Color32 cx = Color32(255,0,0), Color32 cy = Color32(0,255,0), Color32 cz = Color32(0,0,255));
    void DrawVectorInJointSpace(crSkeleton& skel, const char* joint, const Vec3V &vec = Vec3V(0.0f, 0.0f, 1.0f), Color32 color = Color32(255,255,255), const Vec3V &ofs = Vec3V(0.0f, 0.0f, 0.0f));

	void SetPlaybackRate(float rate){ m_PlaybackRate = rate; }
	float GetPlaybackRate() const	{ return m_PlaybackRate; }
	void SetPlay(bool play)			{ m_Play = play; }
	bool GetPlay() const			{ return m_Play; }
	void SetDrawBones(bool draw)	{ m_DrawBones = draw; }
	bool GetDrawBones() const		{ return m_DrawBones; }
	void SetDrawJoints(bool draw)	{ m_DrawJoints = draw; }
	bool GetDrawJoints() const		{ return m_DrawJoints; }
	void SetDrawSkin(bool draw)		{ m_DrawSkin = draw; }
	bool GetDrawSkin() const		{ return m_DrawSkin; }
	void SetForceVisible(bool force){ m_ForceVisible = force; }
	bool GetForceVisible() const	{ return m_ForceVisible; }
	
	virtual bool ShouldFlushPfDraw(){ return true; }

	void SetTrackEnable(bool track) { m_TrackEnable = track; }
	
#if __BANK
	// PURPOSE:
	class FrameEditorWidgets
	{
	public:

		// PURPOSE:
		FrameEditorWidgets();

		// PURPOSE:
		~FrameEditorWidgets();

		// PURPOSE:
		void Init(crFrame& frame, const crSkeletonData* skelData);

		// PURPOSE:
		void AddWidgets(bkBank&);

		// PURPOSE:
		void ResetWidgets();

		// PURPOSE:
		void UpdateFrame();

	private:
		crFrame* m_Frame;
		const crSkeletonData* m_SkelData;

		atArray<Vec3V> m_WidgetValues;
		atArray<bool> m_WidgetInvalid;
	};
#endif // __BANK

protected:

#if __BANK
	bool LoadHashNames(const char* hashFile);
#endif // __BANK

#if __BANK
	void AddWidgetsAnimationManager();
#endif // __BANK

	crCreature*		m_Creature;
	grmMatrixSet*	m_MatrixSet;
	u32				m_LastDrawUpdateFrame;
	rmcDrawable*	m_Drawable;
	grbTargetManager* m_TargetManager;
	crSkeleton*		m_Skeleton;
	Mat34V*			m_MoverMtx;

	float			m_PlaybackRate;
	bool			m_Play;
	bool			m_DrawBones;
	bool			m_DrawJoints;
	bool			m_DrawSkin;
	bool			m_ForceVisible;

	bool			m_TrackEnable;
	bool			m_TrackMover;
	int				m_TrackBone;
	Mat34V			m_TrackMtx;

	crFrameDataFactory	m_FrameDataFactory;
	crFrameAccelerator	m_FrameAccelerator;
	crCommonPool		m_CommonPool;
};

} // namespace ragesamples

#endif // SAMPLE_CRANIMATION_SAMPLE_CRANIMATION_H
