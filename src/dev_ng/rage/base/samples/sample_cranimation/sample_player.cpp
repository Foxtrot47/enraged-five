// 
// sample_cranimation/sample_player.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Animation player
// PURPOSE:
//	This sample demonstrates how to play an animation using the animation player.

using namespace rage;

#include "sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define DEFAULT_FILE "$/rage_male/rage_male.type"
XPARAM(file);

#define DEFAULT_ANIM "$/animation/motion/male_wlk.anim"
PARAM(anim,"[sample_player] Animation file to load (default is \"" DEFAULT_ANIM "\")");

class craSamplePlayer : public ragesamples::craSampleManager	
{
public:
	craSamplePlayer() : ragesamples::craSampleManager()
		, m_Animation(NULL)
		, m_Frame(NULL)
		, m_Player(NULL)
	{
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		// STEP #1. Load our animation.
		const char *animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);

		// -- Call <c>crAnimation::AllocateAndLoad()</c> with the 
		// animation name and it'll return you a loaded animation.
		m_Animation=crAnimation::AllocateAndLoad(animName);


		// STEP #2. Initialize the simple animation player.
		m_Player = rage_new crAnimPlayer();

		// -- Pass the animation into the player
		m_Player->SetAnimation(m_Animation);
		
		// -- Force the player to loop the animation.
		// By default the player will use the animation's own looping flag to determine
		// the looping status of the animation.
		m_Player->SetLooped(true, true);

		// -STOP


		// STEP #3. Initialize our animation frame.

		// -- If we need to get a single frame from the animation, we must have a data
		// structure that will store it.  We create an instance of <c>crFrame</c>
		// to store this data.
		m_Frame=rage_new crFrame;

		// -STOP

		if(m_Animation != NULL)
		{
			// -- Initialize the animation frame based on the degrees of freedom
			// found in the skeleton data.
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
			m_Frame->InitCreateBoneAndMoverDofs(skelData, false);

			// -- Initialize the values of the degrees of freedom in the newly created 
			// frame to identity.
			m_Frame->IdentityFromSkel(skelData);

			// -STOP
		}

#if __BANK
		AddWidgetsPlayer();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Player;
		delete m_Animation;
		delete m_Frame;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		crSkeleton& skeleton = GetSkeleton();

		if(m_Animation != NULL)
		{
			// STEP #4. Request frame data from the player.

			// -- To retrieve a frame from the player we call <c>crSimplePlayer::Composite()</c>.
			m_Player->Composite(*m_Frame);
			
			// -STOP


			// STEP #5. Pose the skeleton.

			//-- Now we take the frame data and pose the bones in the skeleton.  
			// To do this, just pass the frame a reference to the skeleton to be posed.  
			// Posing sets the local matrices in skeleton.
			m_Frame->Pose(skeleton);

			// -STOP

		}
		else
		{
			skeleton.Reset();
		}

		// STEP #6. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
		skeleton.Update();
		UpdateMatrixSet();

		// -STOP

		if (GetPlay() && m_Animation != NULL)
		{
			// STEP #7. Update the player.

			// -- First we ensure that the player is updated with the current playback rate.
			m_Player->SetRate(GetPlaybackRate());

			// -- Now we advance time in the player by passing in the game clock delta.
			// The player automatically takes care of looping the animation for us.
			m_Player->Update(TIME.GetSeconds());
		}
	}

#if __BANK
	void AddWidgetsPlayer()
	{
		bkBank &playerBank = BANKMGR.CreateBank("Player", 50, 500);
		m_Player->AddWidgets(playerBank);
	}
#endif // __BANK

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

private:
	crAnimation* m_Animation;
	crFrame* m_Frame;
	crAnimPlayer* m_Player;
};


// main application
int Main()
{
	craSamplePlayer sampleCraPlayer;
	sampleCraPlayer.Init("sample_cranimation\\NewSkel");

	sampleCraPlayer.UpdateLoop();

	sampleCraPlayer.Shutdown();

	return 0;
}
