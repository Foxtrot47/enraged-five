// 
// sample_bank\sample_bank.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_BANK_SAMPLE_BANK_H
#define SAMPLE_BANK_SAMPLE_BANK_H

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "grcore/setup.h"

class bkSampleManager
{
public:
	bkSampleManager(const char* appName);
	virtual ~bkSampleManager();

	virtual void UpdateLoop();

	virtual void Update();
	virtual void Draw();

protected:
    rage::grcSetup& GetSetup() 
    {
        Assert(m_Setup); return *m_Setup;
    }

    rage::grcSetup& AllocateSetup();

    void InitSetup();

private:
	bool	m_Remote;	// am I using remote widgets?

    rage::Color32	m_ClearColor;
    rage::grcSetup* m_Setup;
};

#endif // SAMPLE_BANK_SAMPLE_BANK_H
