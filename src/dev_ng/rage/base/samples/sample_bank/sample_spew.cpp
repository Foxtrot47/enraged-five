// 
// sample_bank/sample_spew.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


#include "sample_bank.h"

#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

PARAM(spew, "The amount of spew you want per frame");
PARAM(printonly, "Just show Printfs");
PARAM(framecount, "Run for this many frames");

class spewSample : public bkSampleManager
{
public:
	spewSample(const char* appName)
		: bkSampleManager(appName)
	{
		m_BadFrames = 0;
		m_LastStall = 0.0f;
		m_SinceLast = 0;
		m_SinceLastCtr = 0;
	}

	virtual void Update()
	{
		int howMuchSpew = 10;
		PARAM_spew.Get(howMuchSpew);


		if (TIME.GetUnwarpedRealtimeSeconds() > 0.1)
		{
			m_BadFrames++;
			m_LastStall = TIME.GetUnwarpedRealtimeSeconds();
			m_SinceLast = m_SinceLastCtr;
			m_SinceLastCtr = 0;
		}

		char outputString[128];
		formatf(outputString, 128, " %4d dt: %0.2f t: %4.2f | %d bad, l %.3f, g %d, l/g: %.2f", TIME.GetFrameCount(), TIME.GetUnwarpedRealtimeSeconds() * 1000.0f, TIME.GetElapsedTime(), m_BadFrames, m_LastStall, m_SinceLast, (m_LastStall / m_SinceLast)*1000.0f);
		

		for(int i = 0; i < howMuchSpew; i++)
		{
			if (PARAM_printonly.Get() || i % 2 == 0)
			{
				Printf("         %03d  %s\n",i, outputString); 
			}
			else
			{
				Warningf("%03d  %s",i, outputString);
			}
		}

		m_SinceLastCtr++;

		bkSampleManager::Update();

		TIME.Update();

		int frameCount = 0;
		if (PARAM_framecount.Get(frameCount) && (int)TIME.GetFrameCount() == frameCount)
		{
			Quitf("Quitting");
		}
	}

	int m_BadFrames;
	float m_LastStall;
	int m_SinceLast;
	int m_SinceLastCtr;
};

// main application
int Main()
{
#if __BANK
	spewSample sampleBk("sample_simple");

	// run the update loop:
	sampleBk.UpdateLoop();

#endif
	return 0;
}

