// 
// sample_bank\sample_widgets.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Widgets
// PURPOSE:
//		This tester shows all the bank widgets in action.


#include "sample_bank.h"

#include "bank/color.h"
#include "bank/data.h"
#include "bank/list.h"
#include "bank/slider.h"
#include "bank/title.h"
#include "bank/treelist.h"
#include "data/base.h"
#include "diag/output.h"
#include "math/constants.h"
#include "system/main.h"
#include "vector/matrix33.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#if __BANK

#include "bank/combo.h"

// static int s_Toggle;

using namespace rage;

// static functions that will be used by our widgets:
static void HelloCB(const char *msg) 
{
	Displayf("Hello, %s",msg);
}

static void UpdateColorCB( CallbackData msg, CallbackData widget )
{
	bkColor *pColor = reinterpret_cast<bkColor *>( widget );

	char colorStr[64];
	sprintf( colorStr, "ARGBColor:%d:%d:%d:%d", pColor->GetAlpha(), pColor->GetRed(), pColor->GetGreen(), pColor->GetBlue() );

	HelloCB( reinterpret_cast<char *>( msg ) );

	pColor->SetFillColor( colorStr );
}

/* static void OutputToggleCB()
{
	Displayf("Toggle: %d",s_Toggle);
} */

static void NudgeCB(bkBank &bank) 
{
	bool shown;
	int x,y,width,height;
	bank.GetState(shown,x,y,width,height);
	x += 4;
	++y;
	bank.SetState(shown,x,y,width,height);
}

static int s_buttonNum = 0;

static void AddAButtonCB( CallbackData grp, CallbackData button )
{
	bkGroup *pGroup = reinterpret_cast<bkGroup *>( grp );
	if ( pGroup != NULL )
	{
		char name[64];
		sprintf( name, "Button %d", s_buttonNum );

		pGroup->AddButton( name );

		++s_buttonNum;
	}
}

static int s_groupNum = 0;

static const char* s_groupColors[] = {
	"NamedColor:Red",
	"NamedColor:Yellow",
	"NamedColor:Orange",
	"NamedColor:Green",
	"NamedColor:Blue",
	"NamedColor:Purple"
};

static int s_groupColorsLen = 6;

static void AddAGroupCB( CallbackData grp, CallbackData button )
{	
	bkGroup *pGroup = reinterpret_cast<bkGroup *>( grp );
	if ( pGroup != NULL )
	{
		char groupName[64];
		sprintf( groupName, "Group %d", s_groupNum );

		bkGroup* newGrp = pGroup->AddGroup( groupName, true, 0, s_groupColors[s_groupNum % s_groupColorsLen] );
		newGrp->AddButton( "Add A Button To This Group", datCallback(CFA2(AddAButtonCB),newGrp) );
		newGrp->AddButton( "Add A SubGroup To This Group", datCallback(CFA2(AddAGroupCB),newGrp) );

		++s_groupNum;
	}
}

static void TruncateCB( bkBank *pBank )
{
	s_groupNum = 0;
	s_buttonNum = 0;

	pBank->Truncate( 3 );
}

static void MakeExtraBankCB(bkBank &bank)
{
	static bool foo;
	static char buf[8];
	static float val;
	bank.AddToggle("Foo",&foo,datCallback(CFA1(HelloCB),(char*)"foo"));
	bank.AddText("Text",buf,sizeof(buf));
	bank.AddButton("Hello");
	bank.AddSlider("Slider",&val,-1,1,0.1f);
	static int val2;
	static const char *list[] = {"a","b"};
	bank.AddCombo("Combo",&val2,2,list);
}

static void ReplaceComboItemCB( bkCombo *pCombo )
{
    static int item = 0;
    static const char *pStr[3] = 
    {
        "one",
        "a",
        "false"
    };

    ++item;
    if ( item > 2 )
    {
        item = 0;
    }

    pCombo->SetString( 0, pStr[item] );
}

static void GoodbyeCB( const char *msg ) 
{
    Displayf( "Goodbye, %s", msg );
}

static void ReplaceComboItemsCB( bkCombo *pCombo )
{
    static int stringSet = 0;
    static const char *pStringSets[3][3] = 
    {
        { "one", "two", "three" },
        { "a", "b", "c" },
        { "false", "true", "" },
    };
    static int numInSet[3] = { 3, 3, 2 };

    ++stringSet;
    if ( stringSet > 2 )
    {
        stringSet = 0;
    }

    if ( stringSet == 0 )
    {
        pCombo->UpdateCombo( pCombo->GetTitle(), pCombo->GetValuePtr(), 
            numInSet[stringSet], pStringSets[stringSet], datCallback(CFA1(HelloCB),(char*)"int combo") );
    }
    else if ( stringSet == 1 )
    {
        pCombo->UpdateCombo( pCombo->GetTitle(), pCombo->GetValuePtr(), 
            numInSet[stringSet], pStringSets[stringSet], datCallback(CFA1(GoodbyeCB),(char*)"int combo") );
    }
    else
    {
        pCombo->UpdateCombo( pCombo->GetTitle(), pCombo->GetValuePtr(), 
            numInSet[stringSet], pStringSets[stringSet], *pCombo->GetCallback() );
    }
}

void PrintToCustomOutputWindowCB( const char *prefix )
{
	Displayf( "[%s] Here is a sample of custom text sent to output window '%s'.", prefix, prefix );
}

void CreateOutputWindowUser()
{
	BANKMGR.CreateOutputWindow( "USER", "ARGBColor:0:0:200:0" );
}

void AddALotOfWidgetsCB( bkBank *pBank )
{
	// remove all but the button that adds the widgets
	pBank->Truncate( 1 );
	
	static Matrix44 m;
	m.Identity();

	static int sliderval = 25;
	static Vector3 vec;
	vec.Zero();
	static bool toggle = true;

	// add the widgets
	for ( int i = 0; i < 10; ++i )
	{
		char buf[32];
		sprintf( buf, "Matrix%02d", i );
		pBank->AddMatrix( buf, &m, -LARGE_FLOAT, LARGE_FLOAT, 1.0f );
		
		sprintf( buf, "Button%02d", i );
		pBank->AddButton( buf );

		sprintf( buf, "Slider%02d", i );
		pBank->AddSlider( buf, &sliderval, 0, 100, 1 );

		sprintf( buf, "Vector%02d", i );
		pBank->AddVector( buf, &vec, -LARGE_FLOAT, LARGE_FLOAT, 1.0f );

		sprintf( buf, "Toggle%02d", i );
		pBank->AddToggle( buf, &toggle );
	}
}

class bkSampleBank: public datBase {
public:
	bkSampleBank() : Bar(false) { }
	void AddWidgets(bkBank &B);
private:
	bool Bar;
};

bkList* g_List = NULL;
bkTreeList *g_WatchWindowTreeList = NULL;
bkTreeList *g_ControlTestTreeList = NULL;

void bkSampleBank::AddWidgets(bkBank &bank) {
	bank.AddToggle("Bar",&Bar,datCallback(CFA1(HelloCB),(char*)"bar"));
}

class ListTest
{
public:
    enum ENodeKeys
    {
        EGFloatNode,
        EGIntNode,
        EGStringNode,
        EGBoolNode,
        EGVectorNode,
        EGMyStructNode,
        EMyFloatNode,
        EMyIntNode,
        EMyStringNode,
        EMyVectorNode,
        EMyVectorXNode,
        EMyVectorYNode,
        EMyVectorZNode,
    };

    struct MyStruct
    {
        float m_myFloat;
        int m_myInt;
        char m_myString[128];
        Vector3 m_myVector;
    };

	// for the watch window list
	float m_listFloat;
	int m_listInt;
	char m_listString[128];

    // for the watch window tree list
    float m_treeListFloat;
    int m_treeListInt;
    bool m_treeListBool;
    Vector3 m_treeListVector;
    char m_treeListString[128];
    MyStruct m_treeListMyStruct;

    // for the control sample
    int m_node0CheckBox;
    char m_node0Date[16];
    char m_node0ComboBoxStr[16];

    char m_node2Time[16];
    char m_node2ComboBoxStr[16];
    char m_node2ProgressBarValue;

    int m_node3CheckBox;

    int m_node2NumericValue;
    float m_node0NumericValue;

    int m_node0ButtonValue;

    float m_node3SliderValue;

    ListTest()
    {
		m_listFloat = -1.0f;
		m_listInt = 10;
		safecpy( m_listString, "a string", 128 );

        m_treeListFloat = 5.0f;
        m_treeListInt = -593;
        m_treeListBool = false;
        m_treeListVector.Set( 34.4f, 324.09f, 23.32423f );

        safecpy( m_treeListString, "test string", 128 );
        m_treeListMyStruct.m_myFloat = 33.0f;
        m_treeListMyStruct.m_myInt = -1138;
        safecpy( m_treeListMyStruct.m_myString, "filler", 128 );
        m_treeListMyStruct.m_myVector.Set( 0.0f, -1.0f, 0.0f );

        safecpy( m_node0Date, "10/03/2006", 16 );
        safecpy( m_node0ComboBoxStr, "item1", 16 );

        m_node0CheckBox = (int)bkTreeList::CheckBoxStateChecked;
        safecpy( m_node2Time, "11:00:00 AM", 16 );
        safecpy( m_node2ComboBoxStr, "item2", 16 );
        m_node2ProgressBarValue = 10;

        m_node3CheckBox = (int)bkTreeList::CheckBoxStateUnchecked;

        m_node2NumericValue = 17;
        m_node0NumericValue = 33.3f;

        m_node3SliderValue = 50.0f;
    }

	void UpdateListItem( s32 key, s32 column, const char* newValue )
	{
		if ( column == 2 )
		{
			if ( key == (int)&m_listFloat )
			{
				Displayf( "Float changed from %f to %s", m_listFloat, newValue );

				m_listFloat = (float)atof( newValue );

				// This updates the value displayed on the widget
				g_List->AddItem( key, column, newValue );
			}
			else if ( key == (int)&m_listInt )
			{
				Displayf( "Int changed from %d to %s", m_listInt, newValue );

				m_listInt = atoi( newValue );

				// This updates the value displayed on the widget
				g_List->AddItem( key, column, newValue );
			}
			else if ( key == (int)&m_listString )
			{
				Displayf( "String changed from %s to %s", m_listString, newValue );

				safecpy( m_listString, newValue, 128 );

				// This updates the value displayed on the widget
				g_List->AddItem( key, column, newValue );
			}
		}
	}

    void UpdateTreeListItem( s32 key, s32 type, const char *newValue )
    {
        if ( key == -1 )
        {
            return;
        }

        bkTreeList::ItemDataType dataType = static_cast<bkTreeList::ItemDataType>( type );

        switch ( dataType )
        {
        case bkTreeList::ItemDataTypeFloat:
            {
                float* f = reinterpret_cast<float *>( key );
                char output[128];

                sprintf( output, "Float changed from %f", *f );
                *f = static_cast<float>( atof(newValue) );
                sprintf( output, "%s to %f\n", output, *f );
                Displayf( "%s", output );

                // This updates the value displayed on the widget (call both cuz we don't know which tree list the key belongs to)
                g_WatchWindowTreeList->SetItemValue( key, *f );
                g_ControlTestTreeList->SetItemValue( key, *f );
            }
            break;

        case bkTreeList::ItemDataTypeInt:
            {
                int* i = reinterpret_cast<int *>( key );
                char output[128];
                
                sprintf( output, "Int changed from %d", *i );
                *i = atoi( newValue );
                sprintf( output, "%s to %d\n", output, *i );
                Displayf( "%s", output );

                // This updates the value displayed on the widget (call both cuz we don't know which tree list the key belongs to)
                g_WatchWindowTreeList->SetItemValue( key, *i );
                g_ControlTestTreeList->SetItemValue( key, *i );
            }
            break;

        case bkTreeList::ItemDataTypeString:
            {
                char* s = reinterpret_cast<char *>( key );                
                char output[512];
                
                sprintf( output, "String changed from %s", s );
                safecpy( s, newValue, 128 );
                sprintf( output, "%s to %s\n", output, s );
                Displayf( "%s", output );

                // This updates the value displayed on the widget (call both cuz we don't know which tree list the key belongs to)
                g_WatchWindowTreeList->SetItemValue( key, s );
                g_ControlTestTreeList->SetItemValue( key, s );
            }
            break;

        case bkTreeList::ItemDataTypeBool:
            {
                bool *b = reinterpret_cast<bool *>( key );
                char output[128];

                sprintf( output, "Bool changed from %s", *b ? "true" : "false" );
                *b = strcmp(newValue,"true") == 0;
                sprintf( output, "%s to %s\n", output, *b ? "true" : "false" );
                Displayf( "%s", output );

                // This updates the value displayed on the widget (call both cuz we don't know which tree list the key belongs to)
                g_WatchWindowTreeList->SetItemValue( key, *b );
                g_ControlTestTreeList->SetItemValue( key, *b );
            }
            break;

        case bkTreeList::ItemDataTypeVector:
            {
                Vector3 *v = reinterpret_cast<Vector3 *>( key );

                char output[128];
                sprintf( output, "Vector changed from <<%g,%g,%g>>", v->x, v->y, v->z );

                if (*newValue=='<')
                    ++newValue;
                if (*newValue=='<')
                    ++newValue;
                v->x = (float) atof(newValue);
                newValue = strchr(newValue,',');
                if (newValue) 
                {
                    v->y = (float) atof(newValue+1);
                    newValue = strchr(newValue+1,',');
                    if (newValue)
                        v->z = (float) atof(newValue+1);
                }

                sprintf( output, "%s to <<%g,%g,%g>>\n", output, v->x, v->y, v->z );
                Displayf( "%s", output );

                // This updates the value displayed on the widget (call both cuz we don't know which tree list the key belongs to)
                g_WatchWindowTreeList->SetItemValue( key, *v );
                g_ControlTestTreeList->SetItemValue( key, *v );
            }
            break;

        default:
            break;
        }
    }

    void TreeListItemChecked( s32 checkBoxKey, s32 newState )
    {
        if ( checkBoxKey == -1 )
        {
            return;
        }
        
        int *checkBox = reinterpret_cast<int *>( checkBoxKey );

        char output[128];
        strcpy( output, "CheckBox changed from " );

        bkTreeList::CheckBoxState state = static_cast<bkTreeList::CheckBoxState>( *checkBox );
        switch ( state )
        {
        case bkTreeList::CheckBoxStateIndeterminate:
            strcat( output, "Indeterminate" );
            break;
        case bkTreeList::CheckBoxStateUnchecked:
            strcat( output, "Unchecked" );
            break;
        case bkTreeList::CheckBoxStateChecked:
            strcat( output, "Checked" );
            break;
        }

        *checkBox = newState;

        state = static_cast<bkTreeList::CheckBoxState>( *checkBox );
        switch ( state )
        {
        case bkTreeList::CheckBoxStateIndeterminate:
            strcat( output, " to Indeterminate\n" );
            break;
        case bkTreeList::CheckBoxStateUnchecked:
            strcat( output, " to Unchecked\n" );
            break;
        case bkTreeList::CheckBoxStateChecked:
            strcat( output, " to Checked\n" );
            break;
        }

        Displayf( "%s", output );

        // This updates the checkBox displayed on the widget (call both cuz we don't know which tree list the key belongs to)
        g_WatchWindowTreeList->SetItemCheckBox( checkBoxKey, (bkTreeList::CheckBoxState)newState );
        g_ControlTestTreeList->SetItemCheckBox( checkBoxKey, (bkTreeList::CheckBoxState)newState );
    }

    void TreeListButtonClicked( s32 key )
    {
        Displayf( "Tree List Button %d clicked", key );
    }
};

ListTest g_listTest;

static bkData* s_pDataWidgets[4];
static char* s_pDataWidgetMessages[4] = {
    "How now brown cow?",
    "What's happening",
    "I'm not home right now, please leave a message.  CDV Software Entertainment USA and Buka Entertainme"
    "nt are pleased to announce that Pacific Storm has shipped to North American retail outlets. Pacific "
    "Storm is a combination RTS/Action title set in the Pacific Theater of World War II. Pacific Storm wi"
    "ll appeal to both serious strategists and action-oriented gamers alike, with its combination of rich"
    " strategic elements and in-the-cockpit combat controls.  An exciting new hybrid title, Pacific Storm"
    " mixes the strategic elements of RTS titles with the action of an arcade title. RTS fans and serious"
    " wargamers alike will enjoy classic strategy elements including resource management, unit and buildi"
    "ng construction and unit deployment, while action fans will be able to take the skies or seas as the"
    "y directly control units in intense combat, changing the tide of individual battles, or perhaps the "
    "entire war.",
    "This is a lame message"
};

static void SendWidgetDataCB( int *index )
{
    Assert( (int)index >= 0 && (int)index < 4 );
    u8* data = s_pDataWidgets[(int)index]->GetData();
    u16 length = s_pDataWidgets[(int)index]->GetLength();

    u16 msgLength = (u16)strlen( s_pDataWidgetMessages[(int)index] ) + 1;

    // Managed Unbound 
    if ( (int)index == 2 )
    {
        // needs an allocation if we press the button before receiving anything, or
        // if our buffer is smaller than the message
        if ( (data == NULL) || (length < msgLength) )
        {
            length = msgLength;
            s_pDataWidgets[(int)index]->SetData( NULL, length );
            data = s_pDataWidgets[(int)index]->GetData();
        }
    }

    length = msgLength;
    memcpy( data, s_pDataWidgetMessages[(int)index], length );
    s_pDataWidgets[(int)index]->RemoteUpdate();
}

static void ReceiveWidgetDataCB( int *index )
{
    Assert( (int)index >= 0 && (int)index < 4 );
    u8* data = s_pDataWidgets[(int)index]->GetData();
    u16 length = s_pDataWidgets[(int)index]->GetLength();

    Assert( data != NULL );
    Assert( length > 0 );

    data[length-1] = 0;
    length = (u16)strlen( (char *)data );

    Printf( "Received: " );

    int offset = 0;
    while ( offset < length )
    {
        char s[256];
        int len = (length - offset > 255) ? 255 : length - offset;
        strncpy( s, (char *)&(data[offset]), len );
        s[len] = 0;
        Printf( "%s", s );
        offset += len;
    }    
    Printf( "\n" );
}

#endif // __BANK

// main application
int Main()
{
#if __BANK
	bkSampleManager sampleBk("sample_simple");
	
	// STEP #1. Create a bank to show the bank widgets in action.
    char text[32] = "SAMPLE TEXT";
    bool				ta = false;
    u32					tb	= 0;
    atBitSet			bs(64);
    atFixedBitSet<64>	fbs;
    Vector4 color1;
    Vector3 color2;
    int sa = 0;
    int se = 1;
    u8 sb = 0;
    float sc = 0;
    static const char *strings[] = { "one","two","three" };
    u8 ca = 1;
    int cb = 1;

	Vector2 vec2( 0.0f, 1.0f );
	Vector3 vec3( 1.0f, 1.0f, 1.0f );
	Vector4 vec4( 5.0f, 0.0f, 10.0f, 0.0f );

	float angleDegrees = 90.0f;
	float angleFraction = 0.25;
	float angleRadians = PI / 2.0f;
	Vector2 angleVector2;
	angleVector2.Set( 0.5f, 0.5f );

	Matrix33 mtx33;
	mtx33.Identity();
	Matrix34 mtx34;
	mtx34.Identity();
	Matrix44 mtx44;
	mtx44.Identity();

    //-- create the bank.
	bkBank& bank=BANKMGR.CreateBank("Test Widgets");
    {
	    // STEP #2. Create a group to show the text widgets in action.
	
		bkGroup& textWidgets = *bank.AddGroup("Text Widgets",true,0,"NamedColor:White");
        {
	        //-- add the static title widget.  A static title widget cannot be edited by the user.  The FillColor cannot be set
			// in the function, so you must make a separate call to SetFillColor.
	        bkTitle *pTitle = bank.AddTitle("Static Title");
			pTitle->SetFillColor( "NamedColor:Orange" );

	        //-- add the text widget.  This widget can be edited by the user.
			textWidgets.AddText("Text (char*)",text,sizeof(text),false,datCallback(CFA1(HelloCB),(char*)"Text (char*)"), "NamedColor:Red" );
			
			textWidgets.AddText("Text (bool)",&ta,false,datCallback(CFA1(HelloCB),(char*)"Text (bool)"), "ARGBColor:128:0:200:0");
			
			textWidgets.AddText("Text (float)",&sc,false,datCallback(CFA1(HelloCB),(char*)"Text (float)"), "NamedColor:Blue");

			textWidgets.AddText("Text (int)",&sa,false,datCallback(CFA1(HelloCB),(char*)"Text (int)"), "ARGBColor:128:128:0:128");
        }
		//-STOP
	
	    // STEP #3. Create a group to show the button widget in action.
		bkGroup& buttonGroup = *bank.AddGroup("Button Widgets");
        {
	        //-- When this button is pressed, you should see the message "Red Alert!" output to the console window:
	        buttonGroup.AddButton("Button",datCallback(CFA1(HelloCB),(char*)"pushbutton"),"Red Alert!");

	        //-- When this button is pressed, you should see the bank visually mover over:
	        buttonGroup.AddButton("Nudge",datCallback(CFA1(NudgeCB),&bank));
        }
	    //-STOP

	    // STEP #4. Create a group to show the toggle widgets in action.
	    bkGroup& toggleGroup = *bank.AddGroup("Toggle Widgets");
        {
	        //-- this widget shows the boolean toggle in action:
	        toggleGroup.AddToggle("Bool toggle",&ta,datCallback(CFA1(HelloCB),(char*)"bool toggle"));

	        //-- this widget shows the bitmask toggle in action.  Notice that tb is used twice, but 
	        //the bitmask is different:
	        toggleGroup.AddToggle("Bit toggle",&tb,0x0001,datCallback(CFA1(HelloCB),(char*)"bit toggle 1"));
	        toggleGroup.AddToggle("Bit toggle",&tb,0x0002,datCallback(CFA1(HelloCB),(char*)"bit toggle 2"));

	        //-- These widgets showing the atl bitsets templates working with the toggle widgets:

	        bs.Reset();
	        fbs.Reset();
        	
	        toggleGroup.AddToggle("BitSet toggle",&bs,40,datCallback(CFA1(HelloCB),(char*)"bitset toggle"));
	        toggleGroup.AddToggle("FixedBitSet toggle",&fbs,23,datCallback(CFA1(HelloCB),(char*)"fixed bitset toggle"));
        }
	    //-STOP

	    // STEP #5. Create a group to show the slider widgets in action.

		// -- As an alternative to creating groups and adding widgets to them, you can Push and Pop groups 
		// on a stack the bank maintains, any subsequent Add() function adds a widget to the last pushed group.
	    bank.PushGroup("Slider Widgets");
        {
	        //-- These widgets show the color widget in action.
	        color1.Set(1.0f,0.0f,0.0f,0.5f);
			char colorStr1[64];
			sprintf( colorStr1, "ARGBColor:%d:%d:%d:%d", 
				(int)(color1.GetW() * 255.0f), (int)(color1.GetX() * 255.0f), (int)(color1.GetY() * 255.0f), (int)(color1.GetZ() * 255.0f) );

	        color2.Set(0.0f,0.0f,1.0f);
			char colorStr2[64];
			sprintf( colorStr2, "ARGBColor:%d:%d:%d:%d", 
				255, (int)(color2.GetX() * 255.0f), (int)(color2.GetY() * 255.0f), (int)(color2.GetZ() * 255.0f) );

	        bank.AddColor( "Color1", &color1, 0.2f, datCallback(CFA2(UpdateColorCB),(char*)"color"), 0, colorStr1 );
	        bank.AddColor( "Color2", &color2, 0.2f, datCallback(CFA2(UpdateColorCB),(char*)"color"), 0, colorStr2 );

			bank.AddSeparator( "sep1" );

	        //-- These widgets show the slider widgets for integer values:

	        bank.AddSlider("Int Slider",&sa,-10,10,1,datCallback(CFA1(HelloCB),(char*)"int slider"));
	        
			bank.AddSlider("u8 Slider",&sb,0,255,1,datCallback(CFA1(HelloCB),(char*)"u8 slider"));

	        //-- This widget shows the slider widgets for adjusting floating point values:
	        bank.AddSlider("Float Slider",&sc,-1,1,0.01f,datCallback(CFA1(HelloCB),(char*)"float slider"),"x16");

	        //-- This slider widget demonstrates how to create a read-only slider.  If you save the bkSlider returned
			//by this function, you can later change the read-only state by setting step to something greater than 0.
			bank.AddSlider("Read-only Float Slider",&sc,-1,1,0.0f,datCallback(CFA1(HelloCB),(char*)"read-only float slider (should never see this)"));

            //-- This slider demonstrates
            bank.AddSlider("Exponential Int Slider",&se,0,65536,1,datCallback(CFA1(HelloCB),(char*)"exponential int slider"),0,0,true);

			bank.AddSeparator( "sep2" );

			//-- These widgets show the angle widgets for float values
			bank.AddAngle( "Angle in Degrees", &angleDegrees, bkAngleType::DEGREES, -90.0f, 90.0f, datCallback(CFA1(HelloCB),(char*)"angle degrees"));
			bank.AddAngle( "Angle 0 to 1", &angleFraction, bkAngleType::FRACTION, datCallback(CFA1(HelloCB),(char*)"angle 0 to 1"));
			bank.AddAngle( "Angle in Radians", &angleRadians, bkAngleType::RADIANS, datCallback(CFA1(HelloCB),(char*)"angle radians"));
			bank.AddAngle( "Angle Vector2", &angleVector2, datCallback(CFA1(HelloCB),(char*)"angle vector2"));
        }
        bank.PopGroup();
	    //-STOP

	    // STEP #6. Create a group to show the combo widgets in action.
	    bkGroup& comboGroup = *bank.AddGroup("Combo Widgets");
        {
	        //-- Show a combo widget in action.
	        bkCombo *pCombo1 = comboGroup.AddCombo("u8 Combo",&ca,3,strings,1,datCallback(CFA1(HelloCB),(char*)"u8 combo"));

			//-- Demonstrate how to dynamically replace items in the combo box.
			comboGroup.AddButton( "Replace u8 Combo Item", datCallback(CFA1(ReplaceComboItemCB),pCombo1), "Changes the first string in the u8 combo list" );
	        
			comboGroup.AddSeparator( "sep3" );

			//-- Show another combo widget in action.
			bkCombo *pCombo2 = comboGroup.AddCombo("int Combo",&cb,3,strings,1,datCallback(CFA1(HelloCB),(char*)"int combo"));

			//-- Demonstrate how to dynamically replace items in the second combo box.
			comboGroup.AddButton( "Replace int Combo Items", datCallback(CFA1(ReplaceComboItemsCB),pCombo2), "Changes the strings in the int combo list as well as the callback" );
        }
	    //-STOP

        // STEP #7.  Create a group to show the data widgets in action
	    bkGroup& dataGroup = *bank.AddGroup("Data Widgets");
        {
            s_pDataWidgets[0] = dataGroup.AddDataWidget( "Owner Data", rage_new u8[64], 64, datCallback(CFA1(ReceiveWidgetDataCB),(int*)0), 0, true );
            dataGroup.AddButton( "Send Owner Data From Game", datCallback(CFA1(SendWidgetDataCB),(int*)0) );

			dataGroup.AddSeparator( "sep4" );

            s_pDataWidgets[1] = dataGroup.AddDataWidget( "Managed Data (256)", NULL, 64, datCallback(CFA1(ReceiveWidgetDataCB),(int*)1), 0, true );
            dataGroup.AddButton( "Send Managed Data (256) From Game", datCallback(CFA1(SendWidgetDataCB),(int*)1) );

			dataGroup.AddSeparator( "sep5" );

            s_pDataWidgets[2] = dataGroup.AddDataWidget( "Managed Data (Unbound)", NULL, 0, datCallback(CFA1(ReceiveWidgetDataCB),(int*)2), 0, true );
            dataGroup.AddButton( "Send Managed Data (Unbound) From Game", datCallback(CFA1(SendWidgetDataCB),(int*)2) );

			dataGroup.AddSeparator( "sep6" );

            s_pDataWidgets[3] = dataGroup.AddDataWidget( "Hidden Data", rage_new u8[64], 64, datCallback(CFA1(ReceiveWidgetDataCB),(int*)3) );
            dataGroup.AddButton( "Send Hidden Data From Game", datCallback(CFA1(SendWidgetDataCB),(int*)3) );
        }
        //-STOP

		// STEP #8.  Create a group to show the Custom Output Windows in action.
		bkGroup& outputGroup = *bank.AddGroup( "Custom Output Windows" );
		{
			//-- Create a new output window called CUSTOM with blue text.  Since this window is being created before
			// the first frame of the game, it's positioning and docking information will be saved in the Rag layout.
			BANKMGR.CreateOutputWindow( "CUSTOM", "NamedColor:Blue" );

			//-- When this button is pressed, you should see some text displayed in a new output window that was created, called "CUSTOM".
			outputGroup.AddButton( "Print To Output Window 'CUSTOM'", datCallback(CFA1(PrintToCustomOutputWindowCB),(char*)"CUSTOM") );

			//-- When this button is pressed, a new output window called "USER" will be created with green text.  Since this window
			// won't be created until after the first frame of the game, it's positioning and docking information will not be
			// saved in the Rag layout.
			outputGroup.AddButton( "Create Output Window 'USER'", datCallback(CFA(CreateOutputWindowUser)) );
			
			//-- When this button is pressed after output window USER has been created, you should see some text displayed there.
			outputGroup.AddButton( "Print To Output Window 'USER'", datCallback(CFA1(PrintToCustomOutputWindowCB),(char*)"USER") );
		}
		//-STOP

		// STEP #9.  Create a group to show the vector widgets in action
		bkGroup& vectorGroup = *bank.AddGroup( "Vector Widgets" );
		{
			//-- With Step of 0, you make this a read-only widget.
			vectorGroup.AddVector( "Vector2", &vec2, -10.0f, 10.0f, 0.0f, datCallback(CFA1(HelloCB),(char*)"Vector2"), "This is read-only." );

			vectorGroup.AddVector( "Vector3", &vec3, -10.0f, 10.0f, 0.1f, datCallback(CFA1(HelloCB),(char*)"Vector3") );

			//-- As you may have noticed above, when using the various vector operations, the components of the vector get
			// clamped to the min and max values.  By passing in -LARGE_FLOAT and LARGE_FLOAT, your
			// operations are unhindered.
			vectorGroup.AddVector( "Vector4", &vec4, -LARGE_FLOAT, LARGE_FLOAT, 1.0f, datCallback(CFA1(HelloCB),(char*)"Vector4") );

			vectorGroup.AddSeparator( "sep7" );

			//-- For comparison, here is an "old style" way to add a vector using slider widgets.  The advantage to this method
			// is you can add each component separately to your Favorite's view.  The disadvantage is the amount of 
			// screen space it occupies, and the lack of vector-specific manipulation that the Vector Widget provides.
			vectorGroup.AddSlider( "Vector4 Slider", &vec4, -LARGE_FLOAT, LARGE_FLOAT, 1.0f, datCallback(CFA1(HelloCB),(char*)"Vector4 slider") );
		}
		// -STOP

		// STEP #10.  Create a group to show the matrix widgets in action
		bkGroup& matrixGroup = *bank.AddGroup( "Matrix Widgets" );
		{
			//-- With Step of 0, you make this a read-only widget.
			matrixGroup.AddMatrix( "Matrix33", &mtx33, -10.0f, 10.0f, 0.0f, datCallback(CFA1(HelloCB),(char*)"Matrix33"), "This is read-only." );

			matrixGroup.AddMatrix( "Matrix34", &mtx34, -10.0f, 10.0f, 0.1f, datCallback(CFA1(HelloCB),(char*)"Matrix34") );

			//-- As you may have noticed above, when using the various vector operations, the components of the vector get
			// clamped to the min and max values.  By passing in -LARGE_FLOAT and LARGE_FLOAT, your
			// operations are unhindered.
			matrixGroup.AddMatrix( "Matrix44", &mtx44, -LARGE_FLOAT, LARGE_FLOAT, 1.0f, datCallback(CFA1(HelloCB),(char*)"Matrix44") );

			matrixGroup.AddSeparator( "sep8" );

			//-- For comparison, here is an way to add a matrix using vector widgets.  The advantage to this method
			// is you can add each row of the matrix separately to your Favorite's view, and manipulate each row with the vector operations.  
			// The disadvantage is none of the matrix operations are available.
			matrixGroup.AddVector( "Matrix44 Vector", &mtx44, -LARGE_FLOAT, LARGE_FLOAT, 1.0f, datCallback(CFA1(HelloCB),(char*)"Matrix44 vector") );
		}

		// STEP #11.  Create a group to show the list widgets in action
		bkGroup& listGroup = *bank.AddGroup( "List Widgets" );
		{
			//-- Add a List that shows the old method of implementing a Watch Window that was once used for the Script Debugger.
			g_List = listGroup.AddList( "Watch Window List", false, "Edit the 'Value' column." );
			{
				bkList::UpdateItemFuncType listFunctor;
				listFunctor.Reset<ListTest,&ListTest::UpdateListItem>(&g_listTest);
				g_List->SetUpdateItemFunc( listFunctor );

				// add column headers
				g_List->AddColumnHeader( 0, "#", bkList::INT );
				g_List->AddColumnHeader( 1, "Name", bkList::STRING );
				g_List->AddColumnHeader( 2, "Value", bkList::STRING );
				g_List->AddColumnHeader( 3, "Type", bkList::STRING );

				char text[128];

				// add row for float
				sprintf( text, "%f", g_listTest.m_listFloat );
				g_List->AddItem( (int)&g_listTest.m_listFloat, 0, 1 );
				g_List->AddItem( (int)&g_listTest.m_listFloat, 1, "m_listFloat" );
				g_List->AddItem( (int)&g_listTest.m_listFloat, 2, text );
				g_List->AddItem( (int)&g_listTest.m_listFloat, 3, "FLOAT" );

				// add row for int
				sprintf( text, "%d", g_listTest.m_listInt );
				g_List->AddItem( (int)&g_listTest.m_listInt, 0, 2 );
				g_List->AddItem( (int)&g_listTest.m_listInt, 1, "m_listInt" );
				g_List->AddItem( (int)&g_listTest.m_listInt, 2, text );
				g_List->AddItem( (int)&g_listTest.m_listInt, 3, "INT" );

				// add row for string
				g_List->AddItem( (int)&g_listTest.m_listString, 0, 3 );
				g_List->AddItem( (int)&g_listTest.m_listString, 1, "m_listString" );
				g_List->AddItem( (int)&g_listTest.m_listString, 2, g_listTest.m_listString );
				g_List->AddItem( (int)&g_listTest.m_listString, 3, "STRING" );
			}
			//-STOP

			int itemKey = 1;

			//-- Add a TreeList that shows the new method of implementing a Watch Window for the Script Debugger.
			g_WatchWindowTreeList = listGroup.AddTreeList( "Watch Window TreeList", "Edit the 'Value' column." );
			{
				bkTreeList::UpdateItemFuncType treeListFunctor;
				treeListFunctor.Reset<ListTest,&ListTest::UpdateTreeListItem>(&g_listTest);
				g_WatchWindowTreeList->SetUpdateItemFunc( treeListFunctor );

				// add column headers
				g_WatchWindowTreeList->AddReadOnlyColumn( "Name" );
				g_WatchWindowTreeList->AddTextBoxColumn( "Value", bkTreeList::EditConditionDoubleClick );
				g_WatchWindowTreeList->AddReadOnlyColumn( "Type" );

				// add node for native float
				g_WatchWindowTreeList->AddNode( "Float Node", ListTest::EGFloatNode, bkTreeList::NodeDataTypeNative );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_treeListFloat", ListTest::EGFloatNode );
				g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListFloat, g_listTest.m_treeListFloat, ListTest::EGFloatNode );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "FLOAT", ListTest::EGFloatNode );

				// add node for native int
				g_WatchWindowTreeList->AddNode( "Int Node", ListTest::EGIntNode, bkTreeList::NodeDataTypeNative );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_treeListInt", ListTest::EGIntNode );
				g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListInt, g_listTest.m_treeListInt, ListTest::EGIntNode );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "INT", ListTest::EGIntNode );

				// add node for native string
				g_WatchWindowTreeList->AddNode( "String Node", ListTest::EGStringNode, bkTreeList::NodeDataTypeNative );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_treeListString", ListTest::EGStringNode );
				g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListString, g_listTest.m_treeListString, ListTest::EGStringNode );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "STRING", ListTest::EGStringNode );

				// add node for native bool
				g_WatchWindowTreeList->AddNode( "Bool Node", ListTest::EGBoolNode, bkTreeList::NodeDataTypeNative );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_treeListBool", ListTest::EGBoolNode );
				g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListBool, g_listTest.m_treeListBool, ListTest::EGBoolNode );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "BOOL", ListTest::EGBoolNode );

				// add node for native vector
				g_WatchWindowTreeList->AddNode( "String Node", ListTest::EGVectorNode, bkTreeList::NodeDataTypeNative );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_treeListVector", ListTest::EGVectorNode );
				g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListVector, g_listTest.m_treeListVector, ListTest::EGVectorNode );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "VECTOR", ListTest::EGVectorNode );

				// add node for struct
				g_WatchWindowTreeList->AddNode( "MyStruct Node", ListTest::EGMyStructNode, bkTreeList::NodeDataTypeStruct, -1, false );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_treeListMyStruct", ListTest::EGMyStructNode );
				g_WatchWindowTreeList->AddTextBoxItem( itemKey++, "", ListTest::EGMyStructNode, true );
				g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "MyStruct", ListTest::EGMyStructNode );
				{
					g_WatchWindowTreeList->AddNode( "MyFloat Node", ListTest::EMyFloatNode, bkTreeList::NodeDataTypeNative, ListTest::EGMyStructNode );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_myFloat", ListTest::EMyFloatNode );
					g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListMyStruct.m_myFloat, g_listTest.m_treeListMyStruct.m_myFloat, ListTest::EMyFloatNode, false, true );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "FLOAT", ListTest::EMyFloatNode );

					g_WatchWindowTreeList->AddNode( "MyInt Node", ListTest::EMyIntNode, bkTreeList::NodeDataTypeNative, ListTest::EGMyStructNode );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_myInt", ListTest::EMyIntNode );
					g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListMyStruct.m_myInt, g_listTest.m_treeListMyStruct.m_myInt, ListTest::EMyIntNode, false, true );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "INT", ListTest::EMyIntNode );

					g_WatchWindowTreeList->AddNode( "MyString Node", ListTest::EMyStringNode, bkTreeList::NodeDataTypeNative, ListTest::EGMyStructNode );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_myString", ListTest::EMyStringNode );
					g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListMyStruct.m_myString, g_listTest.m_treeListMyStruct.m_myString, ListTest::EMyStringNode, false, true );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "STRING", ListTest::EMyStringNode );

					// an alternate way to display/edit a vector.  only the "Value" column of the children will be editable
					g_WatchWindowTreeList->AddNode( "MyVector Node", ListTest::EMyVectorNode, bkTreeList::NodeDataTypeVector, ListTest::EGMyStructNode, false );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "m_myVector", ListTest::EMyVectorNode );
					g_WatchWindowTreeList->AddTextBoxItem( itemKey++, "", ListTest::EMyVectorNode, true, true );
					g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "VECTOR", ListTest::EMyVectorNode );
					{
						g_WatchWindowTreeList->AddNode( "X Node", ListTest::EMyVectorXNode, bkTreeList::NodeDataTypeNative, ListTest::EMyVectorNode );
						g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "x", ListTest::EMyVectorXNode );
						g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListMyStruct.m_myVector.x, g_listTest.m_treeListMyStruct.m_myVector.x, ListTest::EMyVectorXNode, false, true );
						g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "FLOAT", ListTest::EMyVectorXNode );

						g_WatchWindowTreeList->AddNode( "Y Node", ListTest::EMyVectorYNode, bkTreeList::NodeDataTypeNative, ListTest::EMyVectorNode );
						g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "y", ListTest::EMyVectorYNode );
						g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListMyStruct.m_myVector.y, g_listTest.m_treeListMyStruct.m_myVector.y, ListTest::EMyVectorYNode, false, true );
						g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "FLOAT", ListTest::EMyVectorYNode );

						g_WatchWindowTreeList->AddNode( "Z Node", ListTest::EMyVectorZNode, bkTreeList::NodeDataTypeNative, ListTest::EMyVectorNode );
						g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "z", ListTest::EMyVectorZNode );
						g_WatchWindowTreeList->AddTextBoxItem( (int)&g_listTest.m_treeListMyStruct.m_myVector.z, g_listTest.m_treeListMyStruct.m_myVector.z, ListTest::EMyVectorZNode, false, true );
						g_WatchWindowTreeList->AddReadOnlyItem( itemKey++, "FLOAT", ListTest::EMyVectorZNode );
					}
				}
			}
			//-STOP

			listGroup.AddSeparator( "sep9" );

			//-- A more complex TreeList control example
			g_ControlTestTreeList = listGroup.AddTreeList( "Mixed Control TreeList" );
			{
				bkTreeList::UpdateItemFuncType updateFunc;
				updateFunc.Reset<ListTest,&ListTest::UpdateTreeListItem>(&g_listTest);
				g_ControlTestTreeList->SetUpdateItemFunc( updateFunc );

				bkTreeList::ItemCheckedFuncType checkedFunc;
				checkedFunc.Reset<ListTest,&ListTest::TreeListItemChecked>(&g_listTest);
				g_ControlTestTreeList->SetItemCheckedFunc( checkedFunc );

				bkTreeList::SpecialFuncType buttonFunc;
				buttonFunc.Reset<ListTest,&ListTest::TreeListButtonClicked>(&g_listTest);
				g_ControlTestTreeList->SetItemButtonFunc( buttonFunc );

				// add column headers
				g_ControlTestTreeList->AddReadOnlyColumn( "Check Box", bkTreeList::CheckBoxStyleTwoState, bkTreeList::CheckBoxAlignRight );
				g_ControlTestTreeList->AddDateTimePickerColumn( "DateTime Picker" );
				g_ControlTestTreeList->AddComboBoxColumn( "Combo Box", bkTreeList::EditConditionSingleClick );
				g_ControlTestTreeList->AddColumnTemplateColumn( "Column Template (numeric)", bkTreeList::ItemControlTypeNumericUpDown, bkTreeList::EditConditionAlwaysVisible );
				g_ControlTestTreeList->AddInPlaceTemplateColumn( "InPlace Template", bkTreeList::EditConditionAlwaysVisible );

				static char *comboBoxItems1[3] = 
				{
					"item1", "item2", "item3"
				};

				// add the rows
				g_ControlTestTreeList->AddNode( "Node0", 0, bkTreeList::NodeDataTypeNative );
				g_ControlTestTreeList->AddTextBoxItem( itemKey++, "With Check Box", 0, false, false, (int)&g_listTest.m_node0CheckBox, (bkTreeList::CheckBoxState)g_listTest.m_node0CheckBox );
				g_ControlTestTreeList->AddDateTimePickerItem( (int)&g_listTest.m_node0Date, g_listTest.m_node0Date, 0, bkTreeList::DateTimeFormatShort, "1/1/2006", "12/31/2006" );
				g_ControlTestTreeList->AddComboBoxItem( (int)&g_listTest.m_node0ComboBoxStr, g_listTest.m_node0ComboBoxStr, 0, 3, comboBoxItems1 );
				g_ControlTestTreeList->AddNumericItem( (int)&g_listTest.m_node0NumericValue, g_listTest.m_node0NumericValue, 0, 0.0f, 50.0f, 0.1f );
				g_ControlTestTreeList->AddButtonItem( (int)&g_listTest.m_node0ButtonValue, "Test Button", 0 );

				g_ControlTestTreeList->AddNode( "Node1 (empty)", 1, bkTreeList::NodeDataTypeNative );

				g_ControlTestTreeList->AddNode( "Node2", 2, bkTreeList::NodeDataTypeNative );
				g_ControlTestTreeList->AddTextBoxItem( itemKey++, "Without Check Box", 2 );
				g_ControlTestTreeList->AddDateTimePickerItem( (int)&g_listTest.m_node2Time, g_listTest.m_node2Time, 2, bkTreeList::DateTimeFormatTime, "9:00:00 AM", "1:00:00 PM" );
				g_ControlTestTreeList->AddComboBoxItem( (int)&g_listTest.m_node2ComboBoxStr, g_listTest.m_node2ComboBoxStr, 2, 3, comboBoxItems1 );
				g_ControlTestTreeList->AddNumericItem( (int)&g_listTest.m_node2NumericValue, g_listTest.m_node2NumericValue, 2, 0, 20 );
				g_ControlTestTreeList->AddProgressBarItem( (int)&g_listTest.m_node2ProgressBarValue, g_listTest.m_node2ProgressBarValue, 2 );

				g_ControlTestTreeList->AddNode( "Node3", 3, bkTreeList::NodeDataTypeNative, 2 );
				g_ControlTestTreeList->AddTextBoxItem( itemKey++, "Check Box", 3, false, false, (int)&g_listTest.m_node3CheckBox, (bkTreeList::CheckBoxState)g_listTest.m_node3CheckBox );
				g_ControlTestTreeList->AddReadOnlyItem( itemKey++, "", 3 );
				g_ControlTestTreeList->AddReadOnlyItem( itemKey++, "", 3 );
				g_ControlTestTreeList->AddReadOnlyItem( itemKey++, "", 3 );
				g_ControlTestTreeList->AddSliderItem( (int)&g_listTest.m_node3SliderValue, g_listTest.m_node3SliderValue, 3, 10.0f, 60.0f );
			}
			//-STOP
		}
    }

	// STEP #12.  Create a bank to show how to dynamically add and remove widgets.
	bkBank *pDynamicBank = &BANKMGR.CreateBank( "Dynamic Widgets" );
	{		
		pDynamicBank->AddButton( "Add A Button", datCallback(CFA2(AddAButtonCB),pDynamicBank), "Adds a button to this bank." );

		pDynamicBank->AddButton( "Add A Group", datCallback(CFA2(AddAGroupCB),pDynamicBank), "Adds a group to this bank." );

		pDynamicBank->AddButton( "Truncate", datCallback(CFA1(TruncateCB),pDynamicBank), "Resets this bank back to its original 3 buttons." );
	}

    //-- This bank will only get created when the user presses the button to show that bank.
    BANKMGR.RegisterBank("Static On-Demand Bank",datCallback(CFA1(MakeExtraBankCB),0,true));

    //-- This example shows how to create an on-demand bank via a class member function.
    bkSampleBank extraBank;
    BANKMGR.RegisterBank("Class On-Demand Bank",datCallback(MFA1(bkSampleBank::AddWidgets),&extraBank,0,true));

	// STEP #13 Create a bank to check the performance of adding a lot of widgets at the same time.
	bkBank* pAddLotsBank = &BANKMGR.CreateBank( "Add A Lot Test" );
	{
		pAddLotsBank->AddButton( "Add A Lot of Widgets", datCallback(CFA1(AddALotOfWidgetsCB),pAddLotsBank) );
	}
	//-STOP

	bkGroup* pOutOfOrderGroup = &BANKMGR.CreateBank("Out of order groups");
	bkGroup* pKidOne = pOutOfOrderGroup->AddGroup("KidOne");
	bkGroup* pKidTwo = pOutOfOrderGroup->AddGroup("KidTwo");
	bkGroup* pKidThree = pOutOfOrderGroup->AddGroup("KidThree");
	bkGroup* pKidFour = pOutOfOrderGroup->AddGroup("KidFour");

	pKidOne->AddSlider("1", &sc, 0.0f, 100.0f, 0.01f);
	pKidTwo->AddSlider("2", &sc, 0.0f, 100.0f, 0.01f);
	pKidThree->AddSlider("3", &sc, 0.0f, 100.0f, 0.01f);
	pKidFour->AddSlider("4", &sc, 0.0f, 100.0f, 0.01f);
	bkWidget* sl1 = pKidOne->AddSlider("11", &sc, 0.0f, 100.0f, 0.01f);
	bkWidget* sl2 = pKidTwo->AddSlider("22", &sc, 0.0f, 100.0f, 0.01f);
	bkWidget* sl3 = pKidThree->AddSlider("33", &sc, 0.0f, 100.0f, 0.01f);
	pKidFour->AddSlider("44", &sc, 0.0f, 100.0f, 0.01f);
	pKidOne->AddSlider("111", &sc, 0.0f, 100.0f, 0.01f);
	pKidTwo->AddSlider("222", &sc, 0.0f, 100.0f, 0.01f);
	pKidThree->AddSlider("333", &sc, 0.0f, 100.0f, 0.01f);
	pKidFour->AddSlider("4444", &sc, 0.0f, 100.0f, 0.01f);

	sl3->Destroy();
	sl2->Destroy();
	sl1->Destroy();

	pKidTwo->Destroy();
	

	// run the update loop:
	sampleBk.UpdateLoop();

#endif
	return 0;
}

