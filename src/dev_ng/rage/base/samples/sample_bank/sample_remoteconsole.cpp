// 
// sample_bank\sample_remoteconsole.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 


#include "bank/remoteconsole.h"
#include "system/main.h"

using namespace rage;

int Main()
{
	bkRemoteConsole console;
	
	if (!console.Connect())
	{
		Warningf("Error: couldn't connect!");
	}

	console.SendCommand("remoteecho true"); // echo all of the commands to the rag console

	atArray<bkRemoteConsole::WidgetEntry> widgets;
	console.ListWidgets("Test Widgets/Slider Widgets", widgets);

	for(int i = 0; i < widgets.GetCount(); i++)
	{
		Displayf("Widget %s, effective type %d", widgets[i].m_Name.c_str(), widgets[i].m_EffectiveType);
	}

	float oldValue = console.ReadFloatWidget("Test Widgets/Slider Widgets/Angle in Radians");
	Displayf("Widget 'Angle In Radians' is %f", oldValue);

	console.WriteFloatWidget("Test Widgets/Slider Widgets/Angle in Radians", 1.0f);

	float newValue = console.ReadFloatWidget("Test Widgets/Slider Widgets/Angle in Radians");
	Displayf("Widget 'Angle In Radians' is now %f", newValue);

	// try to connect, and write some widgets 
	return 0;
}