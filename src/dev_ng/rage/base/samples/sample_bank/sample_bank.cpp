// 
// sample_bank\sample_bank.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "sample_bank.h"

#include "bank/bkmgr.h"
#include "file/asset.h"
#include "input/input.h"

using namespace rage;

bkSampleManager::bkSampleManager(const char* appName)
{
    m_ClearColor = Color32(0,10,100);

    InitSetup();
    m_Setup->Init( ASSET.GetPath(), appName );  // this will call bkManager::CreateBankManager(appName)
    m_Setup->BeginGfx(true);
	INPUT.Begin(true);

    m_Setup->CreateDefaultFactories();
}

bkSampleManager::~bkSampleManager()
{
    m_Setup->DestroyFactories();

	INPUT.End();
    m_Setup->EndGfx();
    m_Setup->Shutdown(); // this will call bkManager::DeleteBankManager()

    delete m_Setup;
    m_Setup = NULL;
}

void bkSampleManager::UpdateLoop()
{
	do 
	{
		// we must call this:
		Update();
        Draw();
	} 
	while ( !m_Setup->WantExit() );
}

void bkSampleManager::Update()
{
    m_Setup->BeginUpdate(); // this will call BANKMGR.Update()

	INPUT.Update();

    m_Setup->EndUpdate();
    m_Setup->SetClearColor(m_ClearColor);
}

void bkSampleManager::Draw()
{
    m_Setup->BeginDraw();

    m_Setup->EndDraw(); // this will call BANKMGR.Draw()
}

void bkSampleManager::InitSetup()
{
    m_Setup = &AllocateSetup();
}

grcSetup& bkSampleManager::AllocateSetup()
{
    return *(rage_new grcSetup());
}
