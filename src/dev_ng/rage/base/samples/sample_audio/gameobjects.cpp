/******************************************************************

gameobjects.cpp - automatically generated metadata structure helper functions

Generated on 11/20/2008 4:38:39 PM by alastair.macgregor on machine EDIW-AMACGRE1

*******************************************************************/

#include "gameobjects.h"
#include "string/stringhash.h"

namespace rage {
void *WeaponSettings::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 1179030945U: return &FireSound;
		case 1697606446U: return &EchoSound;
		case 2636327962U: return &ShellCasingSound;
		case 4221676888U: return &SwipeSound;
		case 2003585540U: return &GeneralStrikeSound;
		case 1660834751U: return &PedStrikeSound;
		case 1253983631U: return &HeftSound;
		case 3162076005U: return &PutDownSound;
		case 1452522607U: return &RattleSound;
		case 1517392850U: return &PickupSound;
		case 1076348275U: return &ShellCasing;
		case 1356569853U: return &SafetyOn;
		case 3859729578U: return &SafetyOff;
		default: return NULL;
	}
}
void *AmbientZone::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 997976339U: return &ActivationRange;
		case 4183967195U: return &BoxMin;
		case 3670263069U: return &BoxMax;
		case 3405967894U: return &BuiltUpFactor;
		case 3256966771U: return &NumRulesToPlay;
		case 3613402342U: return &Rules;
		default: return NULL;
	}
}
void *AmbientRule::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 2618193740U: return &Weight;
		case 1256197303U: return &MinDist;
		case 2707249109U: return &MaxDist;
		case 885463319U: return &MinTime;
		case 4025685724U: return &MaxTime;
		case 1990439652U: return &MinRepeatTime;
		case 3255258383U: return &Sound;
		case 2052871693U: return &Category;
		case 1898878336U: return &LastPlayTime;
		default: return NULL;
	}
}
void *AmbientZoneList::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 2319609287U: return &Zones;
		default: return NULL;
	}
}
void *Test8bitFlags::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 1979027800U: return &TestFlags;
		default: return NULL;
	}
}
void *Test16bitFlags::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 1979027800U: return &TestFlags;
		default: return NULL;
	}
}
void *Test32bitFlags::GetFieldPtr(const u32 fieldNameHash)
{
	switch(fieldNameHash)
	{
		case 1979027800U: return &TestFlags;
		default: return NULL;
	}
}
const char *ShellCasingType_ToString(const ShellCasingType val)
{
	switch(val)
	{
		case SHELLCASING_METAL: return "SHELLCASING_METAL";
		case SHELLCASING_PLASTIC: return "SHELLCASING_PLASTIC";
		case SHELLCASING_NONE: return "SHELLCASING_NONE";
		case NUM_SHELLCASINGTYPE: return "NUM_SHELLCASINGTYPE";
	}
	return NULL;
}

ShellCasingType ShellCasingType_Parse(const char *str, const ShellCasingType defaultVal)
{
	const u32 strHash = atStringHash(str);
	switch(strHash)
	{
		case 1724066032U: return SHELLCASING_METAL;
		case 3693593974U: return SHELLCASING_PLASTIC;
		case 3207818827U: return SHELLCASING_NONE;
	}
	return defaultVal;
}

const char *eTest8BitFlags_ToString(const eTest8BitFlags val)
{
	switch(val)
	{
		case BF8_On0: return "BF8_On0";
		case BF8_On1: return "BF8_On1";
		case BF8_Off2: return "BF8_Off2";
		case BF8_On3: return "BF8_On3";
		case BF8_Off4: return "BF8_Off4";
		case BF8_Off5: return "BF8_Off5";
	}
	return NULL;
}

eTest8BitFlags eTest8BitFlags_Parse(const char *str, const eTest8BitFlags defaultVal)
{
	const u32 strHash = atStringHash(str);
	switch(strHash)
	{
		case 2561443643U: return BF8_On0;
		case 2772902000U: return BF8_On1;
		case 3410779731U: return BF8_Off2;
		case 3400854347U: return BF8_On3;
		case 1827742110U: return BF8_Off4;
		case 1588299027U: return BF8_Off5;
	}
	return defaultVal;
}

const char *eTest16BitFlags_ToString(const eTest16BitFlags val)
{
	switch(val)
	{
		case BF16_Off0: return "BF16_Off0";
		case BF16_On1: return "BF16_On1";
		case BF16_Off2: return "BF16_Off2";
		case BF16_On3: return "BF16_On3";
		case BF16_On4: return "BF16_On4";
		case BF16_Off5: return "BF16_Off5";
		case BF16_On6: return "BF16_On6";
		case BF16_On7: return "BF16_On7";
		case BF16_On8: return "BF16_On8";
		case BF16_On9: return "BF16_On9";
		case BF16_Off10: return "BF16_Off10";
		case BF16_Off11: return "BF16_Off11";
		case BF16_Off12: return "BF16_Off12";
		case BF16_On13: return "BF16_On13";
	}
	return NULL;
}

eTest16BitFlags eTest16BitFlags_Parse(const char *str, const eTest16BitFlags defaultVal)
{
	const u32 strHash = atStringHash(str);
	switch(strHash)
	{
		case 677508499U: return BF16_Off0;
		case 1611868152U: return BF16_On1;
		case 2395586949U: return BF16_Off2;
		case 1252097301U: return BF16_On3;
		case 349704579U: return BF16_On4;
		case 2235608691U: return BF16_Off5;
		case 3747718811U: return BF16_On6;
		case 4054567727U: return BF16_On7;
		case 977263702U: return BF16_On8;
		case 3430809812U: return BF16_On9;
		case 1972998767U: return BF16_Off10;
		case 2480459501U: return BF16_Off11;
		case 3607319873U: return BF16_Off12;
		case 1549309284U: return BF16_On13;
	}
	return defaultVal;
}

const char *eTest32BitFlags_ToString(const eTest32BitFlags val)
{
	switch(val)
	{
		case BF32_Off0: return "BF32_Off0";
		case BF32_On1: return "BF32_On1";
		case BF32_Off2: return "BF32_Off2";
		case BF32_On3: return "BF32_On3";
		case BF32_On4: return "BF32_On4";
		case BF32_Off5: return "BF32_Off5";
		case BF32_On6: return "BF32_On6";
		case BF32_On7: return "BF32_On7";
		case BF32_On8: return "BF32_On8";
		case BF32_On9: return "BF32_On9";
		case BF32_Off10: return "BF32_Off10";
		case BF32_Off11: return "BF32_Off11";
		case BF32_Off12: return "BF32_Off12";
		case BF32_On13: return "BF32_On13";
		case BF32_On14: return "BF32_On14";
		case BF32_On15: return "BF32_On15";
		case BF32_On16: return "BF32_On16";
		case BF32_Off17: return "BF32_Off17";
		case BF32_Off18: return "BF32_Off18";
		case BF32_Off19: return "BF32_Off19";
		case BF32_Off20: return "BF32_Off20";
		case BF32_Off21: return "BF32_Off21";
		case BF32_On22: return "BF32_On22";
		case BF32_Off23: return "BF32_Off23";
		case BF32_On24: return "BF32_On24";
	}
	return NULL;
}

eTest32BitFlags eTest32BitFlags_Parse(const char *str, const eTest32BitFlags defaultVal)
{
	const u32 strHash = atStringHash(str);
	switch(strHash)
	{
		case 1863326374U: return BF32_Off0;
		case 2456988145U: return BF32_On1;
		case 4222006225U: return BF32_Off2;
		case 3317993620U: return BF32_On3;
		case 1222121149U: return BF32_On4;
		case 225728368U: return BF32_Off5;
		case 2655437209U: return BF32_On6;
		case 2956289398U: return BF32_On7;
		case 238985607U: return BF32_On8;
		case 1618331124U: return BF32_On9;
		case 1125901536U: return BF32_Off10;
		case 1028839758U: return BF32_Off11;
		case 722973912U: return BF32_Off12;
		case 740806328U: return BF32_On13;
		case 911074052U: return BF32_On14;
		case 2644455845U: return BF32_On15;
		case 2942620976U: return BF32_On16;
		case 3465083836U: return BF32_Off17;
		case 3171571903U: return BF32_Off18;
		case 2930752522U: return BF32_Off19;
		case 2977642969U: return BF32_Off20;
		case 2663519335U: return BF32_Off21;
		case 688477375U: return BF32_On22;
		case 3934006246U: return BF32_Off23;
		case 3315731954U: return BF32_On24;
	}
	return defaultVal;
}

}

