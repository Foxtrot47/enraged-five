#ifndef ENTITY_H
#define ENTITY_H

#include "entitytracker.h"

#include "vector/vector3.h"

using namespace rage;

namespace ragesamples
{
// PURPOSE
//  This sample entity represents something physical in the gameworld, that has a position, and can be rendered.
//  In a real game, it would be something that most physical objects would inherit from. It has an audEntityTracker,
//  so that the audio engine can query its position without ever knowing its type.
class audSampleGameEntityBase
{
public:

	audSampleGameEntityBase()
	{
		m_Vec.Zero();
		m_EntityTracker.Init(this);
	};
	virtual ~audSampleGameEntityBase() {};

	// PURPOSE
	//	Service function, called by the game once per frame
	virtual void Update(u32 timeInMs) = 0;

	// PURPOSE
	//	Render this entity
	virtual void Render() = 0;

	// PURPOSE
	//	Sets the entity's position
	virtual void SetPosition(Vector3 &pos)
	{
		m_Vec = pos;
	}

	// PURPOSE
	//	Gets the entity's position
	virtual Vector3 &GetPosition() 
	{
		return m_Vec;
	}


	// PURPOSE
	//	Gets a ptr to the entity's audEntityTracker.
	audSampleGameEntityTracker *GetEntityTracker(void)
	{
		return &m_EntityTracker;
	}


private:

	Vector3 m_Vec;
	audSampleGameEntityTracker m_EntityTracker;
};
}
#endif // ENTITY_H
