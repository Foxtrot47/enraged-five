/******************************************************************

gameobjects.h - automatically generated metadata structure definitions

Generated on 11/20/2008 4:38:39 PM by alastair.macgregor on machine EDIW-AMACGRE1

*******************************************************************/

#ifndef AUD_GAMEOBJECTS_H
#define AUD_GAMEOBJECTS_H

// handy macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) (TristateValue)((flagvar |= ((trival&0x03) << (flagid<<1))))

namespace rage
{
// tristate values
enum TristateValue
{
		AUD_TRISTATE_FALSE,
		AUD_TRISTATE_TRUE,
		AUD_TRISTATE_UNSPECIFIED,
};
} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE
namespace rage
{
#define GAMEOBJECTS_SCHEMA_VERSION 2
// NOTE: doesn't include base object
#define AUD_NUM_GAMEOBJECTS 6

// disable struct alignment
#if !__SPU
#pragma pack(push, r1, 1)
#endif // !__SPU

enum ShellCasingType
{
		SHELLCASING_METAL = 0,
		SHELLCASING_PLASTIC,
		SHELLCASING_NONE,
		NUM_SHELLCASINGTYPE,
		SHELLCASINGTYPE_MAX = NUM_SHELLCASINGTYPE,
};

const char *ShellCasingType_ToString(const ShellCasingType val);

ShellCasingType ShellCasingType_Parse(const char *str, const ShellCasingType defaultVal);
enum eTest8BitFlags
{
		BF8_On0 = (1<<0),
		BF8_On1 = (1<<1),
		BF8_Off2 = (1<<2),
		BF8_On3 = (1<<3),
		BF8_Off4 = (1<<4),
		BF8_Off5 = (1<<5),
};

const char *eTest8BitFlags_ToString(const eTest8BitFlags val);

eTest8BitFlags eTest8BitFlags_Parse(const char *str, const eTest8BitFlags defaultVal);
enum eTest16BitFlags
{
		BF16_Off0 = (1<<0),
		BF16_On1 = (1<<1),
		BF16_Off2 = (1<<2),
		BF16_On3 = (1<<3),
		BF16_On4 = (1<<4),
		BF16_Off5 = (1<<5),
		BF16_On6 = (1<<6),
		BF16_On7 = (1<<7),
		BF16_On8 = (1<<8),
		BF16_On9 = (1<<9),
		BF16_Off10 = (1<<10),
		BF16_Off11 = (1<<11),
		BF16_Off12 = (1<<12),
		BF16_On13 = (1<<13),
};

const char *eTest16BitFlags_ToString(const eTest16BitFlags val);

eTest16BitFlags eTest16BitFlags_Parse(const char *str, const eTest16BitFlags defaultVal);
enum eTest32BitFlags
{
		BF32_Off0 = (1<<0),
		BF32_On1 = (1<<1),
		BF32_Off2 = (1<<2),
		BF32_On3 = (1<<3),
		BF32_On4 = (1<<4),
		BF32_Off5 = (1<<5),
		BF32_On6 = (1<<6),
		BF32_On7 = (1<<7),
		BF32_On8 = (1<<8),
		BF32_On9 = (1<<9),
		BF32_Off10 = (1<<10),
		BF32_Off11 = (1<<11),
		BF32_Off12 = (1<<12),
		BF32_On13 = (1<<13),
		BF32_On14 = (1<<14),
		BF32_On15 = (1<<15),
		BF32_On16 = (1<<16),
		BF32_Off17 = (1<<17),
		BF32_Off18 = (1<<18),
		BF32_Off19 = (1<<19),
		BF32_Off20 = (1<<20),
		BF32_Off21 = (1<<21),
		BF32_On22 = (1<<22),
		BF32_Off23 = (1<<23),
		BF32_On24 = (1<<24),
};

const char *eTest32BitFlags_ToString(const eTest32BitFlags val);

eTest32BitFlags eTest32BitFlags_Parse(const char *str, const eTest32BitFlags defaultVal);
enum WeaponSettingsFlagIds
{
};

struct WeaponSettings
{
	static const u32 TYPE_ID = 0;

	WeaponSettings()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	,FireSound(3817852694U) //NULL_SOUND
	,EchoSound(3817852694U) //NULL_SOUND
	,ShellCasingSound(3817852694U) //NULL_SOUND
	,SwipeSound(3817852694U) //NULL_SOUND
	,GeneralStrikeSound(3817852694U) //NULL_SOUND
	,PedStrikeSound(3817852694U) //NULL_SOUND
	,HeftSound(3817852694U) //NULL_SOUND
	,PutDownSound(3817852694U) //NULL_SOUND
	,RattleSound(3817852694U) //NULL_SOUND
	,PickupSound(2328742057U) //FRONTEND_GAME_PICKUP_WEAPON
	,ShellCasing(SHELLCASING_METAL)
	,SafetyOn(3817852694U) //NULL_SOUND
	,SafetyOff(3817852694U) //NULL_SOUND
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	u32 FireSound;
	u32 EchoSound;
	u32 ShellCasingSound;
	u32 SwipeSound;
	u32 GeneralStrikeSound;
	u32 PedStrikeSound;
	u32 HeftSound;
	u32 PutDownSound;
	u32 RattleSound;
	u32 PickupSound;
	s8 ShellCasing;
	u32 SafetyOn;
	u32 SafetyOff;
}SPU_ONLY(__attribute__((packed)));

enum AmbientZoneFlagIds
{
};

struct AmbientZone
{
	static const u32 TYPE_ID = 1;

	AmbientZone()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	,ActivationRange(250.0f)
	,BuiltUpFactor(0.0f)
	,NumRulesToPlay(4)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	f32 ActivationRange;
	struct tBoxMin
	{
		f32 x;
		f32 y;
	}BoxMin;

	struct tBoxMax
	{
		f32 x;
		f32 y;
	}BoxMax;

	f32 BuiltUpFactor;
	u8 NumRulesToPlay;
	u8 numRuless;
	struct tRules
	{
		u32 Rule;
	}Rules[32];

}SPU_ONLY(__attribute__((packed)));

enum AmbientRuleFlagIds
{
		FLAG_ID_AMBIENTRULE_STOPWHENRAINING, // StopWhenRaining
		FLAG_ID_AMBIENTRULE_STOPONLOUDSOUND, // StopOnLoudSound
};

struct AmbientRule
{
	static const u32 TYPE_ID = 2;

	AmbientRule()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	,Weight(1.0f)
	,MinDist(70.0f)
	,MaxDist(250.0f)
	,MinTime(0)
	,MaxTime(24)
	,MinRepeatTime(0)
	,Sound(3817852694U) //NULL_SOUND
	,Category(1155669136U) //BASE
	,LastPlayTime(0U)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	f32 Weight;
	f32 MinDist;
	f32 MaxDist;
	u8 MinTime;
	u8 MaxTime;
	u16 MinRepeatTime;
	u32 Sound;
	u32 Category;
	u32 LastPlayTime;
}SPU_ONLY(__attribute__((packed)));

enum AmbientZoneListFlagIds
{
};

struct AmbientZoneList
{
	static const u32 TYPE_ID = 3;

	AmbientZoneList()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	u8 numZoness;
	struct tZones
	{
		u32 Zone;
	}Zones[255];

}SPU_ONLY(__attribute__((packed)));

enum Test8bitFlagsFlagIds
{
};

struct Test8bitFlags
{
	static const u32 TYPE_ID = 4;

	Test8bitFlags()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	union
	{
		u8 Value;
		struct 
		{
#if __BE
			bool p7:1; // padding
			bool p6:1; // padding
			bool Off5:1;
			bool Off4:1;
			bool On3:1;
			bool Off2:1;
			bool On1:1;
			bool On0:1;
#else // !__BE
			bool On0:1;
			bool On1:1;
			bool Off2:1;
			bool On3:1;
			bool Off4:1;
			bool Off5:1;
			bool p6:1; // padding
			bool p7:1; // padding
#endif // !__BE
		}BitFields;
	}TestFlags;

}SPU_ONLY(__attribute__((packed)));

enum Test16bitFlagsFlagIds
{
};

struct Test16bitFlags
{
	static const u32 TYPE_ID = 5;

	Test16bitFlags()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	union
	{
		u16 Value;
		struct 
		{
#if __BE
			bool p15:1; // padding
			bool p14:1; // padding
			bool On13:1;
			bool Off12:1;
			bool Off11:1;
			bool Off10:1;
			bool On9:1;
			bool On8:1;
			bool On7:1;
			bool On6:1;
			bool Off5:1;
			bool On4:1;
			bool On3:1;
			bool Off2:1;
			bool On1:1;
			bool Off0:1;
#else // !__BE
			bool Off0:1;
			bool On1:1;
			bool Off2:1;
			bool On3:1;
			bool On4:1;
			bool Off5:1;
			bool On6:1;
			bool On7:1;
			bool On8:1;
			bool On9:1;
			bool Off10:1;
			bool Off11:1;
			bool Off12:1;
			bool On13:1;
			bool p14:1; // padding
			bool p15:1; // padding
#endif // !__BE
		}BitFields;
	}TestFlags;

}SPU_ONLY(__attribute__((packed)));

enum Test32bitFlagsFlagIds
{
};

struct Test32bitFlags
{
	static const u32 TYPE_ID = 6;

	Test32bitFlags()
	: ClassID(0xff)
	,NameTableOffset(0XFFFFFF)
	,Flags(0xAAAAAAAA)
	{
	}

	// PURPOSE
	//  Returns a pointer the field requested by name hash
	void *GetFieldPtr(const u32 fieldNameHash);

	u32 ClassID : 8;
	u32 NameTableOffset : 24;
	u32 Flags;
	struct tParentOverrides
	{
	}ParentOverrides;

	union
	{
		u32 Value;
		struct 
		{
#if __BE
			bool p31:1; // padding
			bool p30:1; // padding
			bool p29:1; // padding
			bool p28:1; // padding
			bool p27:1; // padding
			bool p26:1; // padding
			bool p25:1; // padding
			bool On24:1;
			bool Off23:1;
			bool On22:1;
			bool Off21:1;
			bool Off20:1;
			bool Off19:1;
			bool Off18:1;
			bool Off17:1;
			bool On16:1;
			bool On15:1;
			bool On14:1;
			bool On13:1;
			bool Off12:1;
			bool Off11:1;
			bool Off10:1;
			bool On9:1;
			bool On8:1;
			bool On7:1;
			bool On6:1;
			bool Off5:1;
			bool On4:1;
			bool On3:1;
			bool Off2:1;
			bool On1:1;
			bool Off0:1;
#else // !__BE
			bool Off0:1;
			bool On1:1;
			bool Off2:1;
			bool On3:1;
			bool On4:1;
			bool Off5:1;
			bool On6:1;
			bool On7:1;
			bool On8:1;
			bool On9:1;
			bool Off10:1;
			bool Off11:1;
			bool Off12:1;
			bool On13:1;
			bool On14:1;
			bool On15:1;
			bool On16:1;
			bool Off17:1;
			bool Off18:1;
			bool Off19:1;
			bool Off20:1;
			bool Off21:1;
			bool On22:1;
			bool Off23:1;
			bool On24:1;
			bool p25:1; // padding
			bool p26:1; // padding
			bool p27:1; // padding
			bool p28:1; // padding
			bool p29:1; // padding
			bool p30:1; // padding
			bool p31:1; // padding
#endif // !__BE
		}BitFields;
	}TestFlags;

}SPU_ONLY(__attribute__((packed)));


#if !__SPU
#pragma pack(pop, r1)
#endif // __SPU


} // namespace rage
#endif // AUD_GAMEOBJECTS_H
