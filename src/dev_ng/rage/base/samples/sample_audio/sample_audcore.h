// 
// sample_audcore.h 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SAMPLE_AUDIO_H
#define AUD_SAMPLE_AUDIO_H


#include "entity.h"

#include "audiodata/container.h"
#include "audioengine/controller.h"
#include "audioengine/engine.h"
#include "audioengine/entity.h"
#include "audioengine/engineutil.h"
#include "audioengine/metadatamanager.h"
#include "audioengine/soundmanager.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "parser/manager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/param.h"
#include "system/xtl.h"
#include "vector/vector3.h"

namespace rage
{
	class audWaveSlot;
	class audSound;
	class audSpeechSound;
}

namespace ragesamples
{

class audWaveTestEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audWaveTestEntity);
	audWaveTestEntity();

	virtual void PreUpdateService(u32 timeInMs);

	void SetNumPasses(const u32 numPasses) { m_NumPasses = numPasses; }
	void StopTest() { m_ShouldStop = true; }

	bool HasTestFinished() const { return m_Pass >= m_NumPasses; }
	u32 GetNumErrors() const { return m_NumErrors; }
	void SetNumVoicesToUse(const u32 numVoices) { m_NumVoicesToUse = numVoices; }

private:

	enum {kMaxSounds = 96};
	atRangeArray<audSound *, kMaxSounds> m_Sounds;
	u32 m_WaveIndex;
	u32 m_SlotIndex;
	u32 m_Pass;
	u32 m_NumPasses;
	u32 m_NumErrors;
	u32 m_NumWaves;
	u32 m_NumPlayed;

	u32 m_NumVoicesToUse;
	bool m_ShouldStop;
	bool m_RandomStartOffsets;
};

class audBankLoadTestEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audBankLoadTestEntity);
	audBankLoadTestEntity();

	virtual void PreUpdateService(u32 timeInMs);

	void SetNumPasses(const u32 numPasses) { m_NumPasses = numPasses; }
	
	bool HasTestFinished() const { return m_Pass >= m_NumPasses; }
	u32 GetNumErrors() const { return m_NumErrors; }

	void SetWaveSlot(audWaveSlot *slot) { m_Slot = slot; }
	void SetMetadataChunkId(s32 chunkId) { m_ChunkId = chunkId; }

private:

	enum {kMaxSounds = 64};
	atRangeArray<audSound *, kMaxSounds> m_Sounds;
	u32  m_WaveIndex;
	audWaveSlot *m_Slot;
	u32 m_BankIndex;
	
	u32 m_Pass;
	u32 m_NumPasses;
	u32 m_NumErrors;
	u32 m_NumWaves;
	u32 m_NumPlayed;
	s32 m_ChunkId;

	bool m_RandomStartOffsets;
};

class audLoopTestEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audLoopTestEntity);
	audLoopTestEntity();

	virtual void Init();
	virtual void PreUpdateService(u32 timeInMs);

private:

	enum {kNumLoopsToPlay = 64};
	atRangeArray<audSound *, kNumLoopsToPlay> m_Sounds;
};

class audSpeechTestEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audSpeechTestEntity);
	audSpeechTestEntity();

	virtual void PreUpdateService(u32 timeInMs);

	void AddWaveSlot(audWaveSlot *slot);
	void AddWaveSlot(const char *slotName);

	void SetNumPasses(const u32 numPasses) { m_NumPasses = numPasses; }
	void StopTest() { m_ShouldStop = true; }

	bool HasTestFinished() const { return m_Pass >= m_NumPasses; }
	u32 GetNumErrors() const { return m_NumErrors; }
	void SetNumVoicesToUse(const u32 numVoices) { m_NumVoicesToUse = numVoices; }

	f32 GetPercentComplete() const { return m_PercentageComplete - m_PercentageStarted; }
	u32 GetTimeStarted() const { return m_TimeStartedMs; }	

private:

	enum SlotState
	{
		kIdle = 0,
		kLoading,
		kPlaying,
	};

	struct audManagedSlot
	{
		audWaveSlot *slot;
		audSpeechSound *sound;
		SlotState state;
	};

	enum {kMaxSounds = 96};
	atRangeArray<audSound *, kMaxSounds> m_Sounds;
	atArray<audManagedSlot> m_Slots;

	u32 m_BankIndex;
	u32 m_BankId;
	u32 m_WaveIndex;

	void *m_HeaderBuffer;

	adatContainer m_Container;

	u32 m_Pass;
	u32 m_NumPasses;
	u32 m_NumErrors;
	u32 m_NumWaves;
	u32 m_NumPlayed;

	f32 m_PercentageComplete;
	f32 m_PercentageStarted;
	u32 m_TimeStartedMs;

	u32 m_NumVoicesToUse;
	bool m_ShouldStop;
	bool m_RandomStartOffsets;
	bool m_MoveOnToNextBank;
	bool m_CalculatedStartedPercentage;
};

// PURPOSE
//	Audio sample base class
class audSample : public grcSampleManager 
{
public:
	audSample();

	virtual void InitClient();	
	virtual void InitLights();
	virtual void InitCamera();
	
	virtual void UpdateClient();
	virtual void UpdateLights();

#if __BANK
	virtual void AddWidgetsClient()=0;
	void UpdateAuditioning();
#endif

	virtual void DrawClient();
	
	virtual void ShutdownClient();

	virtual const char* GetSampleName() const;

protected:

	void SetLightingGroup(grcLightGroup &UNUSED_PARAM(lightGroup)){}
	void RegisterEntity(audSampleGameEntityBase *ent);
#if __BANK
	void AddSampleWidgets(bkBank &bnk);
#endif

	static void DeleteEntityCallback(audSample *_this);
	static void StopTestCallback(audSample *_this);

	atArray<audSampleGameEntityBase *> m_Entities;

	bool m_DrawReverbZones;

	audController m_AudioController;

private:

	bool RunMetadataTests();

	audMetadataManager m_MetadataMgr;

	audWaveTestEntity m_WaveTestEntity;
	audSpeechTestEntity m_SpeechTestEntity;
	audBankLoadTestEntity m_BankLoadTestEntity;
	audLoopTestEntity m_LoopTestEntity;

	char m_AuditionWaveSlot[128];
};


} // namespace rage
#endif // AUD_SAMPLE_AUDIO_H
