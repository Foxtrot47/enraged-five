// 
// sample_audcore/sample_audioentities.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 
#include "audiohardware/driverutil.h"
#include "audioengine/engineutil.h"
#include "audioengine/entity.h"
#include "audioengine/curverepository.h"
#include "audioengine/categorymanager.h"

#include "system/main.h"
#include "sample_audcore.h"
#include "entity.h"
#include "grcore/im.h"

using namespace rage;

namespace ragesamples
{
// defines complexity of sphere, reduce to improve rendering performance
const u32 g_SphereSegments = 16;
const f32 g_RotAngle = PI/180.f;

f32 g_SomeValue = 0.f;

audSound *g_ShadowSoundParent = NULL;

class audFrontendMenuAudioEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audFrontendMenuAudioEntity);
	audFrontendMenuAudioEntity(){}

	void Select()
	{
		audSound *sound = NULL;
		audSoundInitParams params;
		params.Category = g_AudioEngine.GetCategoryManager().GetCategoryPtr(atStringHash("HUD"));
		CreateSound_LocalReference("HUD_DEFAULT_SELECT_MASTER", &sound, &params);
		if(sound != NULL)
		{
			// for fun, pitch it down 2 semitones
			sound->SetRequestedPitch(-200);
			sound->PrepareAndPlay();
		}
	}

	void Highlight()
	{
		audSoundInitParams params;
		CreateAndPlaySound("HUD_DEFAULT_PICK_UP_MASTER", &params);
	}

	void Back()
	{
		audSoundInitParams params;
		CreateAndPlaySound("HUD_DEFAULT_NAV_UP_DOWN_MASTER", &params);
	}
};

class audDummyGameEntityAudioEntity : audEntity
{

public:
	AUDIO_ENTITY_NAME(audDummyGameEntityAudioEntity);

	audDummyGameEntityAudioEntity() : m_DummyEntityLoop(NULL)
	{

	}

	void InitDummy(audTracker *tracker)
	{
		// _must_ call base class Init first
		audEntity::Init();

		// Immediately fire off a looping sound, pointing to the audTracker that is passed to us. This will
		// then update its position automatically, without any need for manual intervention.
		// We store a persistent reference to the sound, so that we can update its pitch and volume later.
		audSoundInitParams params;
		params.RemoveHierarchy = false;
		params.Tracker = tracker;

		params.Volume = 100.f;
		params.BucketId = 0;
		CreateAndPlaySound_Persistent("SAMPLE_ENTITY_LOOP", &m_DummyEntityLoop, &params);

		

	}

	// Called by the audController, this gives us a chance to update various parameters every frame.
	void PreUpdateService(u32 UNUSED_PARAM(timeInMs))
	{
		if (m_DummyEntityLoop)
		{
			s32 pitch = (s32)(2400 * m_SomeValue);
			m_DummyEntityLoop->SetRequestedPitch(pitch);
		}
	}

	void SetSomeValue(f32 val)
	{
		m_SomeValue = val;
	}

private:
	
	f32 m_SomeValue;

	audSound *m_DummyEntityLoop;

};

class audDummyGameEntity : public audSampleGameEntityBase
{
public:

	audDummyGameEntity(const Color32 &color, const f32 size)
	{
		m_Color = color;
		m_Size = size;
		m_Offset.Zero();

		m_RotatedVec.x = audEngineUtil::GetRandomNumberInRange(-25.f, 25.f);
		m_RotatedVec.y = audEngineUtil::GetRandomNumberInRange(-25.f, 25.f);
		m_RotatedVec.z = audEngineUtil::GetRandomNumberInRange(-25.f, 25.f);

		m_RotAngle = g_RotAngle;

		if(audEngineUtil::ResolveProbability(0.5f))
		{
			m_RotAngle *= -1.0f;
		}

		m_Speed = audEngineUtil::GetRandomNumberInRange(0.2f, 1.3f);
		m_Scalar = 1.f;

		m_ShouldDraw = true;
	
		// make sure we have a valid position
		Update(0);

		m_AudioEntity.InitDummy(GetEntityTracker());
	}

	void Update(u32 UNUSED_PARAM(timeInMs))
	{
		Vector3 pos;
		// move us around
		m_RotatedVec.RotateY(m_RotAngle * m_Speed);
		m_RotatedVec.RotateX(m_RotAngle * m_Speed);
		m_RotatedVec.RotateZ(m_RotAngle * m_Speed);

		pos = m_RotatedVec;
		pos.Scale(m_Scalar);
		pos.Add(m_Offset);

		SetPosition(pos);

		m_AudioEntity.SetSomeValue(g_SomeValue);
	}	

	void Render()
	{
		if(m_ShouldDraw)
		{
			Vector3 pos = GetPosition();
			grcColor(m_Color);
			grcDrawSphere(m_Size, pos, g_SphereSegments, false, true);
	
			// note:  for now coors specified in model space
			//Vector3 start = Vector3(0.f,0.f,0.f), end = Vector3(5.f,5.f,5.f);
			//grcDrawSpiral(start, end, 5.f, 1.f, 3.f, 1.f, 1.f, 12);
		}
	}

protected:
	f32 GetSomeValue()
	{
		return g_SomeValue;
	}

private:

	Vector3 m_RotatedVec, m_Offset;
	f32 m_Speed, m_Scalar;
	f32 m_Size, m_RotAngle;
	bool m_ShouldDraw;
	Color32 m_Color;

	audDummyGameEntityAudioEntity m_AudioEntity;
};

class audSampleAudioEntities : public audSample
{
public:
	audSampleAudioEntities()
	{

	}

	void InitClient()
	{
		audSample::InitClient();

		m_FrontendMenuAudioEntity.Init();
	}

	void CreateEntity()
	{
		RegisterEntity(rage_new audDummyGameEntity(Color32(0.0f,0.0f, 1.0f), 1.5f));
	}

	// PURPOSE
	//	Called when the user selects a menu item
	void MenuSelect()
	{
		m_FrontendMenuAudioEntity.Select();
	}

	// PURPOSE
	//	Called when the user selects back from the menu
	void MenuBack()
	{
		m_FrontendMenuAudioEntity.Back();
	}

	// PURPOSE
	//	Called when the user highlights a menu item
	void MenuHighlight()
	{
		m_FrontendMenuAudioEntity.Highlight();
	}

#if __BANK
	static void CreateEntityCallback(audSampleAudioEntities *_this)
	{
		_this->CreateEntity();
	}

	static void MenuSelectCallback(audSampleAudioEntities *_this)
	{
		_this->MenuSelect();
	}

	static void MenuHighlightCallback(audSampleAudioEntities *_this)
	{
		_this->MenuHighlight();
	}

	static void MenuBackCallback(audSampleAudioEntities *_this)
	{
		_this->MenuBack();
	}

	virtual void AddWidgetsClient()
	{
		bkBank &bk = BANKMGR.CreateBank("rage - Audio Entities Sample", 400, 200);

		bk.AddButton("Create Dummy Entity", datCallback(CFA1(CreateEntityCallback), this));
		bk.AddSlider("Some Value", &g_SomeValue, 0.f, 1.f, 0.1f);

		bk.AddButton("Menu Select", datCallback(CFA1(MenuSelectCallback), this));
		bk.AddButton("Menu Highlight", datCallback(CFA1(MenuHighlightCallback), this));
		bk.AddButton("Menu Back", datCallback(CFA1(MenuBackCallback), this));

		AddSampleWidgets(bk);

		
	}
#endif

private:
		
	audFrontendMenuAudioEntity m_FrontendMenuAudioEntity;

};
}// namespace ragesamples

// main application
using namespace ragesamples;
int Main()
{
	audSampleAudioEntities sample;

	// There's probably a better way to do this, but I want it to run out of the box.
	// Maybe we should have an AUDIO_PLATFORM_FOLDER_NAME string in an audio header somewhere?
#if __WIN32PC
	sample.Init("");
#elif __XENON
	sample.Init("");
#elif __PS3
	sample.Init("");
#endif
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}

