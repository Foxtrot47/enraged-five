// 
// sample_audcore/sample_speech.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_audcore.h"

#include "audioengine/entity.h"
#include "audiosoundtypes/speechsound.h"
#include "system/main.h"

using namespace rage;

namespace ragesamples
{

const u32 g_MaxVoiceNameLength		= 50;
const u32 g_MaxContextNameLength	= 50;

class audSpeechAudioEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audSpeechAudioEntity);

	audSpeechAudioEntity() : m_WaveSlot(NULL), m_SpeechSound(NULL)
	{
	}

	void PlaySpeech(char *voiceName, char *contextName, u16 startOffsetPercentage)
	{
		if(m_WaveSlot == NULL)
		{
			//Ensure we have access to our host wave slot.
			m_WaveSlot = audWaveSlot::FindWaveSlot("SPEECH");
		}

		if((m_SpeechSound != NULL) || (m_WaveSlot->GetReferenceCount() > 0))
		{
			//Only prepare and play a single speech sound at a time in this slot.
			return;
		}

		u32 numVariations = audSpeechSound::FindNumVariationsForVoiceAndContext(voiceName, contextName);
		if(numVariations)
		{
			s32 variation = audEngineUtil::GetRandomNumberInRange(1, numVariations - 1);

			audSoundInitParams initParams;
			// force sound to play frontend centre
			initParams.Pan = 0;
			initParams.StartOffset = startOffsetPercentage;
			initParams.IsStartOffsetPercentage = true;
			CreateSound_PersistentReference("AMBIENT_SPEECH", (audSound**)&m_SpeechSound, &initParams);
			if(m_SpeechSound != NULL)
			{
				if(m_SpeechSound->InitSpeech(voiceName, contextName, variation))
				{
					printf("Playing variation %d of %d for %s (%s)\n", variation, numVariations, contextName, voiceName);
					m_SpeechSound->PrepareAndPlay(m_WaveSlot, true, -1);
				}
				else
				{
					//Something went wrong, so kill the sound.
					m_SpeechSound->StopAndForget();
				}
			}
		}
	}

private:
	audWaveSlot *m_WaveSlot;
	audSpeechSound *m_SpeechSound;
};

class audSampleSpeech : public audSample
{
public:
	audSampleSpeech()
	{
	}

	void InitClient()
	{
		audSample::InitClient();

		m_SpeechAudioEntity.Init();
	}

	void PlaySpeech(char *voiceName, char *contextName, u16 startOffsetPercentage)
	{
		m_SpeechAudioEntity.PlaySpeech(voiceName, contextName, startOffsetPercentage);
	}

#if __BANK
	static void PlaySpeechCallback(audSampleSpeech *_this)
	{
		_this->PlaySpeech(m_VoiceName, m_ContextName, m_StartOffsetPercentage);
	}

	virtual void AddWidgetsClient()
	{
		bkBank &bk = BANKMGR.CreateBank("rage - Speech Sample", 400, 200);

		bk.AddText("Voice", m_VoiceName, g_MaxVoiceNameLength);
		bk.AddText("Speech Context", m_ContextName, g_MaxContextNameLength);
		bk.AddSlider("Start Offset (%)", &m_StartOffsetPercentage, 0, 100, 1);
		bk.AddButton("Play Speech", datCallback(CFA1(PlaySpeechCallback), this));

		AddSampleWidgets(bk);
	}
private:
	static u16 m_StartOffsetPercentage;
	static char m_VoiceName[g_MaxVoiceNameLength];
	static char m_ContextName[g_MaxContextNameLength];
#endif

private:
	audSpeechAudioEntity m_SpeechAudioEntity;

};

#if __BANK
u16 audSampleSpeech::m_StartOffsetPercentage = 0;
char audSampleSpeech::m_VoiceName[g_MaxVoiceNameLength] = {0};
char audSampleSpeech::m_ContextName[g_MaxContextNameLength] = {0};
#endif // __BANK

}// namespace ragesamples

// main application
using namespace ragesamples;
int Main()
{
	audSampleSpeech sample;
	sample.Init("");
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}
