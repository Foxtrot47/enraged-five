//
// sample_audio/entitytracker.cpp
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "entitytracker.h"

#include "entity.h"

namespace ragesamples
{

const Vector3 audSampleGameEntityTracker::GetPosition() const
{
	Assert(m_ParentEntity);
	return m_ParentEntity->GetPosition();
}

} // namespace ragesamples
