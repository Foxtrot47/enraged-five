// 
// sample_audcore.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundmanager.h"
#include "audioengine/controller.h"
#include "audioengine/categorymanager.h"
#include "audiohardware/driver.h"
#include "audiohardware/waveslot.h"
#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/speechsound.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/device_relative.h"
#include "file/packfile.h"
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "parser/manager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/simpleallocator.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/xtl.h"
#include "vector/vector3.h"

#include "gameobjects.h"
#include "sample_audcore.h"

#include "entity.h"

using namespace rage;

XPARAM(audiodesigner);

namespace ragesamples
{
	
PARAM(speechdemo, "Runs the Speech Demo"); 
PARAM(virtualisingdemo, "Runs the Virtualisation Demo");
PARAM(smallvirtualisingdemo, "Runs the Small Virtualisation Demo");
PARAM(audiofolder, "the folder to me mounted as [audio:/]");
PARAM(audiopack, "the packfile to me mounted as [audio:/]");
PARAM(audiopacksfx, "second packfile to me mounted as [audio:/sfx]");
PARAM(audiopacksfx2, "third packfile to me mounted as [audio:/sfx]");
PARAM(testmetadata, "Test metadata format");
PARAM(testwavedata, "Play all waves in memory");
PARAM(testspeechdata, "Play all speech waves");
PARAM(testbankloading, "Play all waves from banks that fit in the specified slot");
PARAM(looptest, "Test loop playback");
XPARAM(path);

const char *g_AudioDeviceName = "audio:/";

fiDeviceRelative gAudioDevice;
fiPackfile gAudioPackfileDevice;
fiPackfile gAudioPackfileDeviceSfx;
fiPackfile gAudioPackfileDeviceSfx2;

u32 g_NumVoicesToUseForWaveTest = 64;

#define AUDIO_DISPLAY_FFT 0

#if AUDIO_DISPLAY_FFT
static const u16 g_graphOriginX = 60;
static const u16 g_graphOriginY = 460;
static const u16 g_graphBarWidth = 2;
static const u16 g_graphBarMaxHeight = 50;
#endif // AUDIO_DISPLAY_FFT


audLoopTestEntity::audLoopTestEntity()
{
	for(u32 i = 0; i < kNumLoopsToPlay; i++)
	{
		m_Sounds[i] = NULL;
	}
}

void audLoopTestEntity::Init()
{
	audEntity::Init();

	for(u32 i = 0; i < kNumLoopsToPlay; i++)
	{
		audSoundInitParams initParams;
		initParams.Volume = -24.f;
		Assign(initParams.Pan, audEngineUtil::GetRandomNumberInRange(0,359));
		initParams.StartOffset = audEngineUtil::GetRandomNumberInRange(0,99);
		initParams.IsStartOffsetPercentage = true;
		CreateAndPlaySound_Persistent("RESIDENT_WEATHER_TARMAC_RAIN_LOOP_1_A", &m_Sounds[i], &initParams);
	}
}

void audLoopTestEntity::PreUpdateService(u32 ASSERT_ONLY(timeInMs))
{
	for(u32 i = 0; i < kNumLoopsToPlay; i++)
	{
		Assertf(m_Sounds[i], "Sound stopped in slot %u, time %ums", i, timeInMs);
	}
}

audBankLoadTestEntity::audBankLoadTestEntity()
{
	for(u32 i = 0; i < kMaxSounds; i++)
	{
		m_Sounds[i] = NULL;
	}
	m_WaveIndex = 0;
	m_Slot = NULL;	
	m_RandomStartOffsets = false;
	m_Pass = 1;
	m_NumPasses = 5;
	m_NumWaves = 0;
	m_NumPlayed = 0;
	m_NumErrors = 0;

	m_ChunkId = 0;

}

void audBankLoadTestEntity::PreUpdateService(u32 UNUSED_PARAM(timeInMs))
{

	if(!PARAM_testbankloading.Get())
	{
		return;
	}

	if(m_Pass >= m_NumPasses)
	{
		// we're finished
		return;
	}

	if(!m_Slot)
	{
		return;
	}


	const audMetadataManager &mgr = g_AudioEngine.GetSoundManager().GetFactory().GetMetadataManager();

	if(m_BankIndex >= mgr.GetNumStringsInChunkTable(m_ChunkId)-1)
	{
		m_BankIndex = 0;
		// for all subsequent passes, use random offsets
		m_RandomStartOffsets = true;

		m_Pass++;

		if(m_Pass >= m_NumPasses)
		{
			Displayf("Test finished; %u waves, passes %u (%u voices played), errors %u", m_NumWaves, m_Pass, m_NumPlayed, m_NumErrors);
			m_Pass = m_NumPasses;

			for(u32 i = 0; i < kMaxSounds; i++)
			{
				if(m_Sounds[i])
				{
					m_Sounds[i]->StopAndForget();
				}
			}
		}

		return;
	}

	
	u32 numPlaying = 0;
	for(u32 i = 0; i < kMaxSounds; i++)
	{
		if(m_Sounds[i])
		{
			numPlaying++;
		}
	}

	if(m_Slot->GetReferenceCount() == 0 && numPlaying == 0 && (m_Slot->GetLoadedBankId() >= AUD_INVALID_BANK_ID || m_WaveIndex >= m_Slot->GetContainer().GetNumObjects()))
	{
		// move onto next bank
		const char *bankName = mgr.GetStringFromChunkTableFromIndex(m_ChunkId, ++m_BankIndex);
		
		char bankFilePath[RAGE_MAX_PATH]={0};
		// if we're using RPF files, remove any special characters (ie $/) so map works
		char fullWaveRootPath[RAGE_MAX_PATH];
		ASSET.FullPath(fullWaveRootPath, sizeof(fullWaveRootPath), audDriver::GetConfig().GetWaveRootPath(), "");
		formatf(bankFilePath, "%s%s",fullWaveRootPath,bankName);

		fiStream *stream = ASSET.Open(bankFilePath,"awc");
		if(!stream)
		{
			Warningf("Failed to find bank %s (check for invalid wave reference)", bankName);
			return;
		}
		u32 size = (u32)stream->Size();
		stream->Close();
		if(size == 0 || size > m_Slot->GetSlotSize())
		{
			Warningf("Skipping bank %s (%u bytes)", bankName, size);
			return;
		}		

		Displayf("Loading bank %u:%s (%u bytes)",m_BankIndex, bankName, size);

		m_WaveIndex = 0;
		u32 bankId = g_AudioEngine.GetSoundManager().GetFactory().GetBankIndexFromName(bankName);
		

		audWaveSlot::audWaveSlotLoadStatus loadStatus = audWaveSlot::NOT_REQUESTED;
		do 
		{
			
			loadStatus = m_Slot->GetBankLoadingStatus(bankId);
			if(loadStatus == audWaveSlot::NOT_REQUESTED)
			{
				m_Slot->LoadBank(bankId);
			}
			sysIpcSleep(100);
		} while (loadStatus != audWaveSlot::LOADED);

		if(m_Slot->GetContainer().FindObject("") != adatContainer::InvalidId)
		{
			m_WaveIndex = ~0U;
			Warningf("Skipping stream bank %s", bankName);
		}
		return;
	}

	for(u32 i = 0; i < kMaxSounds; i++)
	{
		if(m_Sounds[i] && m_Sounds[i]->GetPlayState() == AUD_SOUND_PLAYING)
		{
			bool isLooping = false;
			const s32 durationMs = m_Sounds[i]->ComputeDurationMsIncludingStartOffsetAndPredelay(&isLooping);
			const s32 playTime = m_Sounds[i]->GetCurrentPlayTime(g_AudioEngine.GetSoundManager().GetTimeInMilliseconds(0));// ((audSpeechSound*)m_Sounds[i])->GetWavePlaytime();
			// extra 150ms to compensate for voice starting/stopping latency
			if(playTime > durationMs*3 + 500)
			{
				if(!isLooping)
				{
					m_NumErrors++;
					Errorf("[%d] non-looping wave didn't stop (playtime %dms, duration %dms", i, playTime, durationMs);
				}
				m_Sounds[i]->StopAndForget();
			}
		}
		if(m_Sounds[i] == NULL)
		{
			const u32 numWaves = m_Slot->GetContainer().GetNumObjects();
			if(m_WaveIndex < numWaves)
			{
				const adatObjectId waveId = (adatObjectId)m_WaveIndex++;

				const u32 waveNameHash = m_Slot->GetContainer().GetObjectNameHash(waveId);

				audSpeechSoundParams speechSoundParams;
				speechSoundParams.BankId = m_Slot->GetLoadedBankId();
				speechSoundParams.WaveNameHash = waveNameHash;

				audSoundInitParams initParams;
				Assign(initParams.Pan, audEngineUtil::GetRandomNumberInRange(0,359));
				if(m_RandomStartOffsets)
				{
					initParams.IsStartOffsetPercentage = true;
					initParams.StartOffset = audEngineUtil::GetRandomNumberInRange(0,100);
				}
				CreateSound_PersistentReference("WAVE_TEST_SPEECH_SOUND",&m_Sounds[i],&initParams);

				m_NumPlayed++;

				if(m_Pass == 1)
				{
					// only count waves the first time through
					m_NumWaves++;
				}

				Assert(m_Sounds[i]);
				Assert(m_Sounds[i]->GetSoundTypeID() == SpeechSound::TYPE_ID);

				audSpeechSound *speechSound = (audSpeechSound*)m_Sounds[i];
				speechSound->InitSpeech(speechSoundParams);

				speechSound->PrepareAndPlay(m_Slot, false, 0);
				Displayf("[%d] Playing wave %s:%u/%u (%u) from %d%%", i, m_Slot->GetLoadedBankName(), m_WaveIndex-1, m_Slot->GetContainer().GetNumObjects(), waveNameHash,initParams.StartOffset);
			}
		}
	}
}

audWaveTestEntity::audWaveTestEntity()
{
	for(u32 i = 0; i < kMaxSounds; i++)
	{
		m_Sounds[i] = NULL;
	}
	m_WaveIndex = 0;
	m_SlotIndex = 0;	
	m_RandomStartOffsets = false;
	m_Pass = 1;
	m_NumPasses = ~0U;
	m_NumWaves = 0;
	m_NumPlayed = 0;
	m_NumErrors = 0;
	m_NumVoicesToUse = 64;
	m_ShouldStop = false;
}

void audWaveTestEntity::PreUpdateService(u32 UNUSED_PARAM(timeInMs))
{
	
	if(!PARAM_testwavedata.Get())
	{
		return;
	}

	if(m_Pass >= m_NumPasses)
	{
		// we're finished
		return;
	}

	if(m_SlotIndex >= audWaveSlot::GetNumWaveSlots() || m_ShouldStop)
	{
		m_SlotIndex = 0;
		// for all subsequent passes, use random offsets
		m_RandomStartOffsets = true;

		m_Pass++;

		if(m_Pass >= m_NumPasses || m_ShouldStop)
		{
			Displayf("Test finished; %u waves, passes %u (%u voices played), errors %u", m_NumWaves, m_Pass, m_NumPlayed, m_NumErrors);
			m_Pass = m_NumPasses;

			for(u32 i = 0; i < kMaxSounds; i++)
			{
				if(m_Sounds[i])
				{
					m_Sounds[i]->StopAndForget();
				}
			}
		}

		return;
	}

	audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndex((s32)m_SlotIndex);
	const u32 bankId = slot->GetLoadedBankId();
	if(bankId >= AUD_INVALID_BANK_ID)
	{
		// move on
		m_SlotIndex++;
		//Warningf("no bank loaded in %s", slot->GetSlotName());
		return;
	}
	else if(slot->IsStreaming())
	{
		//Displayf("Skipping stream slot %s", slot->GetSlotName());
	}
	
	s32 finishedCount = 0;
	bool allDone = true;
	for(u32 i = 0; i < kMaxSounds; i++)
	{
		bool isThisSlotPlaying = false;
		if(m_Sounds[i])
		{
			isThisSlotPlaying = true;
		}
		if(m_Sounds[i] && m_Sounds[i]->GetPlayState() == AUD_SOUND_DORMANT)
		{
			if(m_Sounds[i]->Prepare(slot) != AUD_PREPARED)
			{
				Warningf("Dormant sound (%d)", i);
				m_Sounds[i]->StopAndForget();
			}
		}
		else if(m_Sounds[i] && m_Sounds[i]->GetPlayState() == AUD_SOUND_PLAYING)
		{
			bool isLooping = false;
			const s32 durationMs = m_Sounds[i]->ComputeDurationMsIncludingStartOffsetAndPredelay(&isLooping);
			const s32 playTime = m_Sounds[i]->GetCurrentPlayTime(g_AudioEngine.GetSoundManager().GetTimeInMilliseconds(0));// ((audSpeechSound*)m_Sounds[i])->GetWavePlaytime();
			// extra 150ms to compensate for voice starting/stopping latency
			if(playTime > durationMs*3 + 1000)
			{
				if(!isLooping)
				{
					m_NumErrors++;
					//Errorf("[%d] non-looping wave didn't stop (playtime %dms, duration %dms", i, playTime, durationMs);
				}
				m_Sounds[i]->StopAndForget();
			}
		}
		if(m_Sounds[i] == NULL && i < m_NumVoicesToUse)
		{
			const u32 numWaves = slot->GetContainer().GetNumObjects();
			if(m_WaveIndex < numWaves)
			{
				const adatObjectId waveId = (adatObjectId)m_WaveIndex++;

				const u32 waveNameHash = slot->GetContainer().GetObjectNameHash(waveId);

				audWaveRef waveRef;
				waveRef.Init(slot, waveNameHash);
				const audWaveFormat *format = waveRef.FindFormat();

				Assert(format);

				audSpeechSoundParams speechSoundParams;
				speechSoundParams.BankId = bankId;
				speechSoundParams.WaveNameHash = waveNameHash;

				audSoundInitParams initParams;
				Assign(initParams.Pan, audEngineUtil::GetRandomNumberInRange(0,359));
				if(m_RandomStartOffsets)
				{
					//initParams.IsStartOffsetPercentage = true;
					//initParams.StartOffset = audEngineUtil::GetRandomNumberInRange(0,100);

					// intentionally allow >100% start offsets
					const f32 startOffsetFrac = audEngineUtil::GetRandomNumberInRange(0.f,1.1f);
					const f32 startOffsetSamples = format->LengthSamples * startOffsetFrac;
					const u32 startOffsetMs = audDriverUtil::ConvertSamplesToMs((u32)startOffsetSamples,(u32)format->SampleRate);

					initParams.StartOffset = startOffsetMs;
					initParams.IsStartOffsetPercentage = false;

					Assign(initParams.Pitch, audEngineUtil::GetRandomNumberInRange(-600,24000));
				}

				
				CreateSound_PersistentReference("WAVE_TEST_SPEECH_SOUND",&m_Sounds[i],&initParams);

				m_NumPlayed++;

				if(m_Pass == 1)
				{
					// only count waves the first time through
					m_NumWaves++;
				}

				Assert(m_Sounds[i]);
				Assert(m_Sounds[i]->GetSoundTypeID() == SpeechSound::TYPE_ID);

				audSpeechSound *speechSound = (audSpeechSound*)m_Sounds[i];
				speechSound->InitSpeech(speechSoundParams);

				speechSound->PrepareAndPlay(slot, false, 0);
				//Displayf("[%d] Playing wave %s->%s:%u (%u) from %d%ms", i, slot->GetSlotName(), slot->GetLoadedBankName(), m_WaveIndex-1, waveNameHash,initParams.StartOffset);
				isThisSlotPlaying = true;
			}
			else
			{
				finishedCount++;
			}
		}

		if(isThisSlotPlaying)
		{
			allDone = false;
		}
	}

	if(allDone)
	{
		//Displayf("Finished slot %u...", m_SlotIndex);

		m_SlotIndex++;
		m_WaveIndex = 0;
	}
}
enum {kMaxHeaderSize = 16 * 1024};
audSpeechTestEntity::audSpeechTestEntity()
{
	m_Pass = 0;
	m_PercentageComplete = 0.f;
	m_PercentageStarted = 0.f;
	m_NumErrors = 0;
	m_NumPasses = 2;
	m_BankIndex = 0;
	m_WaveIndex = 0;
	m_HeaderBuffer = rage_new u8[kMaxHeaderSize];
	m_ShouldStop = false;
	m_RandomStartOffsets = false;
	m_MoveOnToNextBank = true;
	m_CalculatedStartedPercentage = false;
	m_TimeStartedMs = ~0U;
	
}

void audSpeechTestEntity::AddWaveSlot(const char *slotName)
{
	audWaveSlot *slot = audWaveSlot::FindWaveSlot(slotName);
	if(slot)
	{
		AddWaveSlot(slot);
	}
}

void audSpeechTestEntity::AddWaveSlot(audWaveSlot *slot)
{
	audManagedSlot &ref = m_Slots.Grow();
	ref.slot = slot;
	ref.state = kIdle;
	ref.sound = NULL;
}

void audSpeechTestEntity::PreUpdateService(u32 UNUSED_PARAM(timeInMs))
{
	if(!PARAM_testspeechdata.Get())
	{
		return;
	}

	if(m_TimeStartedMs == ~0U)
	{
		m_TimeStartedMs = audEngineUtil::GetCurrentTimeInMilliseconds();
	}

	if(m_Pass >= m_NumPasses)
	{
		return;
	}

	if(m_MoveOnToNextBank)
	{
		const audMetadataManager &mgr = g_AudioEngine.GetSoundManager().GetFactory().GetMetadataManager();
		const s32 speechChunkId = mgr.FindChunkId("audSpeechSound");	

		if(m_BankIndex >= mgr.GetNumStringsInChunkTable(speechChunkId))
		{
			m_BankIndex = 0;
			m_Pass++;
			if(m_Pass >= m_NumPasses)
			{
				Displayf("Finished speech test - %u passes, played %u waves.  %u errors.", m_Pass, m_NumPlayed, m_NumErrors);
			}
			else
			{
				Displayf("Finished pass %u",m_Pass-1);
			}
			m_RandomStartOffsets = true;
			// leave moveOnToNextBank flag set
			return;
		}
		const char *bankName = mgr.GetStringFromChunkTableFromIndex(speechChunkId,m_BankIndex);
		if(!strncmp(bankName, "VFX\\", 4) || !strncmp(bankName, "SPEECH_WALLA\\", 13))
		{
			Warningf("Skipping bank %s", bankName);
			m_BankIndex++;
			return;
		}
		m_BankId = g_AudioEngine.GetSoundManager().GetFactory().GetBankIndexFromName(bankName);
		
		// need to load the header here so that we know how many waves are in it
		char bankFilePath[RAGE_MAX_PATH]={0};
		// if we're using RPF files, remove any special characters (ie $/) so map works
		char fullWaveRootPath[RAGE_MAX_PATH];
		ASSET.FullPath(fullWaveRootPath, sizeof(fullWaveRootPath), audDriver::GetConfig().GetWaveRootPath(), "");
		formatf(bankFilePath, "%s%s",fullWaveRootPath,bankName);

		fiStream *stream = ASSET.Open(bankFilePath,"awc");
		Assert(stream);

		stream->Read(m_HeaderBuffer, Min<s32>(kMaxHeaderSize,stream->Size()));
		stream->Close();

		m_Container.SetHeader((const adatContainerHeader*const)m_HeaderBuffer);
		Assert(m_Container.IsValid() && m_Container.IsNativeEndian());
		Assert(m_Container.GetHeaderSize() < kMaxHeaderSize);

		Displayf("Moving on to bank %s (%u/%u) with %u waves.  Header size %u", bankName, m_BankIndex, mgr.GetNumStringsInChunkTable(speechChunkId),m_Container.GetNumObjects(), m_Container.GetHeaderSize());

		m_PercentageComplete = (m_BankIndex / (f32)mgr.GetNumStringsInChunkTable(speechChunkId)) * 100.f * ((m_Pass+1) / (f32)m_NumPasses);
		if(!m_CalculatedStartedPercentage)
		{
			m_PercentageStarted = m_PercentageComplete;
			m_CalculatedStartedPercentage = true;
		}
		m_WaveIndex = 0;
		m_MoveOnToNextBank = false;
		m_BankIndex++;
	}

	for(atArray<audManagedSlot>::iterator iter = m_Slots.begin(); iter != m_Slots.end(); iter++)
	{
		switch(iter->state)
		{
		case kPlaying:
			if(iter->sound != NULL)
			{
				bool isLooping = false;
				const s32 durationMs = iter->sound->ComputeDurationMsIncludingStartOffsetAndPredelay(&isLooping);
				if(isLooping)
				{
					m_NumErrors++;
					Errorf("Wave %u from %s has loop", iter->slot->GetLoadedWaveNameHash(),iter->slot->GetLoadedBankName());	
				}
				const s32 playTime = iter->sound->GetCurrentPlayTime(g_AudioEngine.GetSoundManager().GetTimeInMilliseconds(0));// ((audSpeechSound*)m_Sounds[i])->GetWavePlaytime();
				// extra 250ms to compensate for voice starting/stopping latency
				if(playTime > durationMs + 250)
				{
					m_NumErrors++;
					Errorf("Wave %u from %s didn't stop after %ums (duration %ums)", iter->slot->GetLoadedWaveNameHash(),iter->slot->GetLoadedBankName(), playTime, durationMs);	
					iter->sound->StopAndForget();
				}
				break;
			}
			else
			{
				iter->state = kIdle;
				// intentional fall-through to idle case
			}
		case kIdle:

			if(!m_MoveOnToNextBank)
			{
				audSoundInitParams initParams;
				initParams.AllowLoad = true;
				initParams.WaveSlot = iter->slot;
				Assign(initParams.Pan, audEngineUtil::GetRandomNumberInRange(0,359));
				if(m_RandomStartOffsets)
				{
					initParams.StartOffset = audEngineUtil::GetRandomNumberInRange(0,100);
					initParams.IsStartOffsetPercentage = true;
				}
				Assert(!iter->sound);
				CreateSound_PersistentReference("WAVE_TEST_SPEECH_SOUND",(audSound**)&iter->sound,&initParams);
				Assert(iter->sound);

				audSpeechSoundParams speechSoundParams;
				speechSoundParams.BankId = m_BankId;
				speechSoundParams.WaveNameHash = m_Container.GetObjectNameHash((adatObjectId)m_WaveIndex++);

				iter->sound->InitSpeech(speechSoundParams);
				iter->sound->Prepare(iter->slot, true);

				iter->state = kLoading;

				if(m_WaveIndex >= m_Container.GetNumObjects())
				{
					// we've played every wave in this bank...
					m_MoveOnToNextBank = true;
				}
			}

			break;
		case kLoading:
			Assert(iter->sound);
			const audPrepareState prepState = iter->sound->Prepare(iter->slot,true);
			if(prepState == AUD_PREPARED)
			{
				// check viseme
				audWaveRef waveRef;
				waveRef.Init(iter->slot, iter->slot->GetLoadedWaveNameHash());
				
				Displayf("Playing %u from %s", iter->slot->GetLoadedWaveNameHash(), iter->slot->GetLoadedBankName());

				u32 visemeChunkSize = 0;
				const void *visemeData = waveRef.FindChunk(atStringHash("VISEME"), visemeChunkSize);
				if(visemeData)
				{
					Displayf("Found viseme chunk: %u bytes", visemeChunkSize);					
				}

				iter->sound->Play();
				m_NumPlayed++;
				iter->state = kPlaying;
			}
			else if(prepState == AUD_PREPARE_FAILED)
			{
				Errorf("Failed to prepare wave");
				m_NumErrors++;
			}
			break;			
		}
	}
	
}

audSample::audSample()
{

}

#ifdef GetObject
#undef GetObject
#endif

#define audAssertReturn(x) if(!AssertVerify(x) || !(x)) { return false; }
bool audSample::RunMetadataTests()
{
	// test flag functionality
	Test8bitFlags *test8Bit = m_MetadataMgr.GetObject<Test8bitFlags>("TESTERS_8BIT_FLAGS");
	audAssertReturn(test8Bit);

	// 6 flags, packed into 1 byte
	CompileTimeAssert(sizeof(test8Bit->TestFlags) == 1);

	// check generated enum mask matches data
	audAssertReturn(test8Bit->TestFlags.Value & BF8_On0);
	audAssertReturn(test8Bit->TestFlags.Value & BF8_On1);
	audAssertReturn(!(test8Bit->TestFlags.Value & BF8_Off2));
	audAssertReturn(test8Bit->TestFlags.Value & BF8_On3);
	audAssertReturn(!(test8Bit->TestFlags.Value & BF8_Off4));
	audAssertReturn(!(test8Bit->TestFlags.Value & BF8_Off5));

	// check bitfields match data
	audAssertReturn(test8Bit->TestFlags.BitFields.On0 == true);
	audAssertReturn(test8Bit->TestFlags.BitFields.On1 == true);
	audAssertReturn(test8Bit->TestFlags.BitFields.Off2 == false);
	audAssertReturn(test8Bit->TestFlags.BitFields.On3 == true);
	audAssertReturn(test8Bit->TestFlags.BitFields.Off4 == false);
	audAssertReturn(test8Bit->TestFlags.BitFields.Off5 == false);

	// 16bit flags
	Test16bitFlags *test16Bit = m_MetadataMgr.GetObject<Test16bitFlags>("TESTERS_16BIT_FLAGS");
	audAssertReturn(test16Bit);
	// 14 flags, packed into 2 bytes
	CompileTimeAssert(sizeof(test16Bit->TestFlags) == 2);

	// check generated enum mask matches data
	/*
	bool Off0:1;
	bool On1:1;
	bool Off2:1;
	bool On3:1;
	bool On4:1;
	bool Off5:1;
	bool On6:1;
	bool On7:1;
	bool On8:1;
	bool On9:1;
	bool Off10:1;
	bool Off11:1;
	bool Off12:1;
	bool On13:1;
	*/
	audAssertReturn(!(test16Bit->TestFlags.Value & BF16_Off0));
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On1);
	audAssertReturn(!(test16Bit->TestFlags.Value & BF16_Off2));
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On3);
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On4);
	audAssertReturn(!(test16Bit->TestFlags.Value & BF16_Off5));
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On6);
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On7);
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On8);
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On9);
	audAssertReturn(!(test16Bit->TestFlags.Value & BF16_Off10));
	audAssertReturn(!(test16Bit->TestFlags.Value & BF16_Off11));
	audAssertReturn(!(test16Bit->TestFlags.Value & BF16_Off12));
	audAssertReturn(test16Bit->TestFlags.Value & BF16_On13);

	// test generated bitfields
	audAssertReturn(test16Bit->TestFlags.BitFields.Off0 == false);
	audAssertReturn(test16Bit->TestFlags.BitFields.On1 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.Off2 == false);
	audAssertReturn(test16Bit->TestFlags.BitFields.On3 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.On4 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.Off5 == false);
	audAssertReturn(test16Bit->TestFlags.BitFields.On6 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.On7 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.On8 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.On9 == true);
	audAssertReturn(test16Bit->TestFlags.BitFields.Off10 == false);
	audAssertReturn(test16Bit->TestFlags.BitFields.Off11 == false);
	audAssertReturn(test16Bit->TestFlags.BitFields.Off12 == false);
	audAssertReturn(test16Bit->TestFlags.BitFields.On13 == true);

	// 32bit flags
	Test32bitFlags *test32Bit = m_MetadataMgr.GetObject<Test32bitFlags>("TESTERS_32BIT_FLAGS");
	audAssertReturn(test32Bit);
	// 25 flags, packed into 4 bytes
	CompileTimeAssert(sizeof(test32Bit->TestFlags) == 4);

	// test enum mask
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off0));
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On1);
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off2));
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On3);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On4);
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off5));
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On6);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On7);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On8);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On9);
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off10));
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off11));
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off12));
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On13);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On14);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On15);
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On16);
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off17));
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off18));
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off19));
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off20));
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off21));
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On22);
	audAssertReturn(!(test32Bit->TestFlags.Value & BF32_Off23));
	audAssertReturn(test32Bit->TestFlags.Value & BF32_On24);

	// test generated bitfields
	audAssertReturn(test32Bit->TestFlags.BitFields.Off0 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.On1 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off2 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.On3 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On4 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off5 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.On6 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On7 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On8 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On9 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off10 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off11 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off12 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.On13 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On14 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On15 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.On16 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off17 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off18 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off19 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off20 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off21 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.On22 == true);
	audAssertReturn(test32Bit->TestFlags.BitFields.Off23 == false);
	audAssertReturn(test32Bit->TestFlags.BitFields.On24 == true);

	return true;
}

void audSample::InitClient() 
{
	PARAM_audiodesigner.Set("yes");
	// bank loading relies on pgStreamer being initialised
	pgStreamer::InitClass();
	INIT_PARSER;

	m_DrawTimeBars = false;

	if(!PARAM_audiofolder.Get())
	{
		const char *path;
		PARAM_path.Get(path);
		PARAM_audiofolder.Set(path);
	}

	// setup "audio:/" device if we have specified a folder or path.
	const char* pAudioFolder = NULL;
	if(PARAM_audiopack.Get(pAudioFolder))
	{
		if(!gAudioPackfileDevice.Init(pAudioFolder, true, fiPackfile::CACHE_NONE))
		{
			Errorf("Unable to find audio archive '%s', check that RFS connected properly.", pAudioFolder);
		}

		gAudioPackfileDevice.MountAs(g_AudioDeviceName);

		if (PARAM_audiopacksfx.Get(pAudioFolder))
		{
			if(!gAudioPackfileDeviceSfx.Init(pAudioFolder, true, fiPackfile::CACHE_NONE))
			{
				Errorf("Unable to find audio archive '%s', check that RFS connected properly.", pAudioFolder);
			}

			char tempBase[128];
			ASSET.BaseName(tempBase,sizeof(tempBase),pAudioFolder);

			char sfxPath[128];
			formatf(sfxPath, "audio:/sfx/%s/", ASSET.FileName(tempBase));
			gAudioPackfileDeviceSfx.MountAs(sfxPath);
		}

		if (PARAM_audiopacksfx2.Get(pAudioFolder))
		{
			if(!gAudioPackfileDeviceSfx2.Init(pAudioFolder, true, fiPackfile::CACHE_NONE))
			{
				Errorf("Unable to find audio archive '%s', check that RFS connected properly.", pAudioFolder);
			}

			char tempBase[128];
			ASSET.BaseName(tempBase,sizeof(tempBase),pAudioFolder);

			char sfxPath[128];
			formatf(sfxPath, "audio:/sfx/%s/", ASSET.FileName(tempBase));
			gAudioPackfileDeviceSfx2.MountAs(sfxPath);
		}
	}
	else if(PARAM_audiofolder.Get(pAudioFolder))
	{
		gAudioDevice.Init(pAudioFolder, true);
		gAudioDevice.MountAs(g_AudioDeviceName);
	}

#if __WIN32PC || RSG_ORBIS
	sysMemAllocator *physicalAllocator = rage_new sysMemSimpleAllocator(256 * 1024 * 1024, MEMTYPE_COUNT+1);
#else
	sysMemAllocator *physicalAllocator = sysMemAllocator::GetMaster().GetAllocator(1);
#endif
	if(!InitializeAudio(m_AudioController, physicalAllocator, sysMemAllocator::GetMaster().GetAllocator(0), "audio:/config/"))
	{
		AssertMsg(0, "Audio Engine failed to initialise!");
		return;
	}

	g_AudioEngine.GetSoundManager().GetFactory().LoadMetadataChunk("TEST", "audio:/test_sounds.dat");

	// enable a listener
	g_AudioEngine.GetEnvironment().SetListenerContribution(1.0f, 0);
	g_AudioEngine.CommitGameSettings(0);

	m_DrawBackground = false;

	if(PARAM_testmetadata.Get())
	{
		// Initialise a metadata manager to load gameobjects.dat
		if(!AssertVerify(m_MetadataMgr.Init("GameObjects", "config/game.dat", audMetadataManager::NameTable_Always, GAMEOBJECTS_SCHEMA_VERSION)))
		{
			Quitf("Failed to load game.dat");
		}

		if(!AssertVerify(RunMetadataTests()))
		{
			Quitf("RunMetadataTests() failed");
		}
	}

	m_WaveTestEntity.Init();
	m_SpeechTestEntity.Init();

	if(PARAM_looptest.Get())
	{
		m_LoopTestEntity.Init();
	}

	for(s32 i = 0; i < 128; i++)
	{
		char buf[128];
		formatf(buf, "SPEECH_%02d", i);
		m_SpeechTestEntity.AddWaveSlot(buf);
	}

	for(s32 i = 0; i < 128; i++)
	{
		char buf[128];
		formatf(buf, "SCRIPT_SPEECH_%02d", i);
		m_SpeechTestEntity.AddWaveSlot(buf);
	}



	m_BankLoadTestEntity.Init();
	m_BankLoadTestEntity.SetWaveSlot(audWaveSlot::FindWaveSlot("STATIC_SFX"));
}

void audSample::DrawClient() 
{
	// draw background
	grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());

	grcViewport::SetCurrentWorldIdentity();
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);
	grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
	grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);

	grcBegin(drawTriStrip,4);
	grcColor(Color32(0,0,0));
	grcVertex2i(0,0);
	grcVertex2i(GRCDEVICE.GetWidth(),0);		
	grcVertex2i(0,GRCDEVICE.GetHeight());
	grcVertex2i(GRCDEVICE.GetWidth(),GRCDEVICE.GetHeight());
	grcEnd();

	grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_TestOnly_LessEqual);
	grcViewport::SetCurrent(old);

	for(int i = 0 ; i < m_Entities.GetCount(); i++)
	{
		m_Entities[i]->Render();
	}

#if AUDIO_DISPLAY_FFT
	//Draw FFT.
	old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());

	u16 numMasterChannels = 0;
	u32 numFrequencyTaps = 0;
	f32 *frequencyTaps = NULL;
	g_AudioEngine.GetMasterChannelFrequencyTaps(&frequencyTaps, numMasterChannels, numFrequencyTaps);

	f32 baseX = (f32)g_graphOriginX;
	f32 barTopY;

	for(u32 i=0; i<numFrequencyTaps-7; i+=8)
	{
		for(u32 i2=0; i2<4; i2++)
		{
			if(frequencyTaps)
			{
				barTopY = (f32)g_graphOriginY - ((f32)g_graphBarMaxHeight * sqrt(pow(frequencyTaps[i + i2], 2.0f) +
					pow(frequencyTaps[i + i2 + 4], 2.0f)));
			}
			else
			{
				barTopY = (f32)g_graphOriginY;
			}

			grcBegin(drawTriStrip, 5);
			grcColor3f((f32)(i2%2), (f32)((i2+1)%2), 0.0f);
			grcVertex2f(baseX, (f32)g_graphOriginY);
			grcVertex2f(baseX + (f32)g_graphBarWidth, (f32)g_graphOriginY);
			grcVertex2f(baseX + (f32)g_graphBarWidth, barTopY);
			grcVertex2f(baseX, barTopY);
			grcVertex2f(baseX, (f32)g_graphOriginY);
			grcEnd();

			baseX += g_graphBarWidth;
		}
	}

	grcViewport::SetCurrent(old);
#endif // AUDIO_DISPLAY_FFT

	if(!g_AudioEngine.IsGameInControlOfMusicPlayback())
	{
		grcDraw2dText(20.f, 460.f, "game is not in control of music playback");
	}

	if(PARAM_testspeechdata.Get())
	{
		const u32 timeStarted = m_SpeechTestEntity.GetTimeStarted();
		const u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();
		const f32 timeElapsedSeconds = (now - timeStarted) * 0.001f;

		const u32 timeElapsedHours = (u32)Floorf(timeElapsedSeconds / 3600.f);
		const u32 timeElapsedMins = (u32)Floorf((timeElapsedSeconds - ((f32)timeElapsedHours*3600.f)) / 60.f);
		const u32 timeElapsedSecs = (u32)Floorf((timeElapsedSeconds - (timeElapsedMins*60.f + timeElapsedHours*3600.f)));

		const f32 percentComplete = m_SpeechTestEntity.GetPercentComplete();

		grcColor3f(0.9f,0.8f,1.f);

		if(percentComplete > 0.f)
		{
			static f32 lastEstimate = 0.f;
			const f32 realEstimatedTimeRemaining = (timeElapsedSeconds / percentComplete) * (100.f-percentComplete);

			const f32 estimatedTimeRemaining = 0.01f*realEstimatedTimeRemaining + 0.99f*lastEstimate;

			lastEstimate = estimatedTimeRemaining;
			const u32 remainingHours = (u32)Floorf(estimatedTimeRemaining / 3600.f);
			const u32 remainingMins = (u32)Floorf((estimatedTimeRemaining - ((f32)remainingHours*3600.f)) / 60.f);
			
			

			char buf[128];
			formatf(buf, "Speech Test %02.2f%% complete, estimated time remaining %02dh%02d (elapsed: %02d:%02d:%02d)", 
				percentComplete, 
				remainingHours, remainingMins, 
				timeElapsedHours, timeElapsedMins, timeElapsedSecs);

			grcDraw2dText(50.f, 280.f, buf);
		}
		else
		{
			grcDraw2dText(50.f, 280.f, "Speech Test: 0% complete");
		}
	}

	g_AudioEngine.DrawDebug();
}

void audSample::ShutdownClient()
{

	while(m_Entities.GetCount())
	{
		audSampleGameEntityBase *ent = m_Entities[m_Entities.GetCount()-1];
		m_Entities.Delete(m_Entities.GetCount()-1);
		delete ent;
	}

	g_AudioEngine.Shutdown();
	SHUTDOWN_PARSER;
	pgStreamer::ShutdownClass();
}

void audSample::InitLights() 
{
	grcSampleManager::InitLights();
	m_Lights.SetAmbient(0.f,0.f,0.f);
}

void audSample::InitCamera()
{
	InitSampleCamera(Vector3(0.f, 15.f, -25.f), Vector3(0.f, 0.f, 0.f));
//	InitSampleCamera(Vector3(0.f, 7.f, -10.f), Vector3(0.f, 0.f, 0.f));
}

void audSample::UpdateLights() 
{
	grcSampleManager::UpdateLights();
}

void audSample::UpdateClient()
{
	audDriver::GetMixer()->WaitOnThreadCommandBufferProcessing();

	u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();

//	Matrix34 camera = GetCameraMatrix();
	// move the mic 1.5m in front of the camera
	Matrix34 mic = GetCameraMatrix();
/*
	Vector3 lookFrom, lookTo, nm;
	mic.GetLookAt(&lookFrom, &lookTo);
	nm = lookTo;
	nm.Subtract(lookFrom);
	nm.Normalize();
	nm.Multiply(Vector3(0.f,0.f,1.5f));
	lookFrom.Add(nm);
	mic.LookAt(lookFrom, lookTo);
*/
	Vector3 boom, lookAt, lookFrom;
	mic.GetLookAt(&lookFrom, &lookAt, 1.5f);

	boom = lookAt;
	boom.Subtract(lookFrom);
	// boom.Normalize(); // GetLookAt() takes a distance parameter.
	// boom *= 1.5f;
	
	mic.d.Add(boom);

	m_WaveTestEntity.SetNumVoicesToUse(g_NumVoicesToUseForWaveTest);

	g_AudioEngine.SetVolumeListenerMatrix(mic);
	g_AudioEngine.SetPanningListenerMatrix(mic);
	m_AudioController.PreUpdate(now);
	
	static u32 s_LastTimeMs = 0;
	audEntity::ProcessBatchedSoundRequests(now - s_LastTimeMs);	
	s_LastTimeMs = now;
	m_AudioController.Update(now);

	for(int i = 0 ; i < m_Entities.GetCount(); i++)
	{
		m_Entities[i]->Update(now);
	}
	
	g_AudioEngine.CommitGameSettings((u32)(TIME.GetElapsedTime()*1000.f));

	//g_AudioEngine.SetListenerReverbParams(g_ListenerRoomSize, g_ListenerDamping, g_ListenerWet, g_ListenerDry);

	BANK_ONLY(UpdateAuditioning());
	audDriver::GetMixer()->FlagThreadCommandBufferReadyToProcess();
}

#if __BANK
void audSample::UpdateAuditioning()
{
	u32 soundNameHash;
	if(SOUNDFACTORY.GetAuditionStartSound(soundNameHash))
	{
		audMetadataObjectInfo info;
		if(SOUNDFACTORY.GetMetadataManager().GetObjectInfo(soundNameHash, info))
		{
			if(gSoundsIsOfType(info.GetType(), Sound::TYPE_ID))
			{
				audSound **auditionSoundPtr = g_AudioEngine.GetRemoteControl().GetAuditionSoundPtr();

				if(*auditionSoundPtr)
				{
					(*auditionSoundPtr)->StopAndForget();
				}

				audSoundInitParams initParams;
				initParams.RemoveHierarchy = false;
				initParams.IsAuditioning = true;
				
				// stick it in the last bucket, which should be reserved
				Assign(initParams.BucketId, audSound::GetStaticPool().GetNumBuckets()-1);
				
				audWaveSlot *auditionWaveSlot = audWaveSlot::FindWaveSlot(m_AuditionWaveSlot);
				initParams.WaveSlot = auditionWaveSlot;
				initParams.AllowLoad = true;
				
				initParams.Pan = 0;
				
				m_WaveTestEntity.CreateSound_PersistentReference(soundNameHash, auditionSoundPtr, &initParams);

				if(*auditionSoundPtr == NULL)
				{
					audWarningf("Couldn't instantiate auditioned sound with hash %u / name %s", soundNameHash, SOUNDFACTORY.GetMetadataManager().GetObjectName(soundNameHash));
					return;
				}
				else
				{
					audSound *auditionSound = *auditionSoundPtr;
					auditionSound->PrepareAndPlay(auditionWaveSlot, true, 3000);
				}
			}
		}
		else
		{
			audWarningf("RAVE Auditioning: Failed to find sound with hash %u", soundNameHash);
		}
	}

	u32 auditionStopNameHash;
	if(SOUNDFACTORY.GetAuditionStopSound(auditionStopNameHash))
	{
		audSound *auditionSound = *g_AudioEngine.GetRemoteControl().GetAuditionSoundPtr();
		if(auditionSound)
		{
			auditionSound->StopAndForget();
		}
	}
}
#endif

const char* audSample::GetSampleName() const
{
	return "RAGE Audio Sample";
}

void audSample::RegisterEntity(audSampleGameEntityBase *ent)
{
	m_Entities.PushAndGrow(ent);
}


#if __BANK
void audSample::DeleteEntityCallback(audSample *_this)
{		
	if(_this->m_Entities.GetCount())
	{
		audSampleGameEntityBase *ent = _this->m_Entities[_this->m_Entities.GetCount()-1];
		_this->m_Entities.Delete(_this->m_Entities.GetCount()-1);
		delete ent;
	}
}

void audSample::StopTestCallback(audSample *_this)
{
	_this->m_WaveTestEntity.StopTest();
}

void audSample::AddSampleWidgets(bkBank &bk) 
{
	bk.AddSlider("NumVoicesToUse", &g_NumVoicesToUseForWaveTest, 0, 96, 1);
	bk.AddButton("StopTest", datCallback(CFA1(StopTestCallback), this));
	bk.AddButton("Delete Entity", datCallback(CFA1(DeleteEntityCallback), this));
	strcpy(m_AuditionWaveSlot, "STREAM_ENGINE_GRANULAR_HI_1");
	bk.AddText("Audition Waveslot", m_AuditionWaveSlot, sizeof(m_AuditionWaveSlot), false);
	g_AudioEngine.AddWidgets(bk);
}
#endif


} // namespace ragesamples
