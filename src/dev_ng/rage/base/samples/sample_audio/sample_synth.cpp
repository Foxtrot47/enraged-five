#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "input/keyboard.h"
#include "input/mouse.h"
#include "system/main.h"
#include "system/simpleallocator.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/geometry.h"
#include "math/random.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grcore/device.h"
#include "grmodel/modelfactory.h"
#include "grmodel/setup.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"

#include "audioengine/engine.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/grainplayer.h"
#include "audiohardware/midi.h"
#include "audiosynth/synthdefs.h"
#include "audiosynth/synthesizer.h"
#include "audiosynth/synthesizerview.h"

#include "audiosoundtypes/modularsynthsound.h"
#include "audiosoundtypes/granularsound.h"
#include "audiosoundtypes/sounddefs.h"

#include "sample_audcore.h"

#include "audioengine/entity.h"

#include "audiohardware/debug.h"
#include "audiohardware/driver.h"
#include "audiohardware/submix.h"
#include "audioeffecttypes/biquadfiltereffect.h"
#include "audioeffecttypes/filterdefs.h"
#include "profile/profiler.h"
#include "system/performancetimer.h"

#include "audiosoundtypes/simplesound.h"
#include "audiosoundtypes/streamingsound.h"
#include "audiohardware/streamingwaveslot.h"
#include "audiosoundtypes/soundcontrol.h"

#include "audiosynth/synthcore.h"

using namespace rage;

PARAM(file,"Ignored.");
XPARAM(fullscreen);

namespace rage
{
	u32 g_DataSetToUse = 0;
	u32 g_EngineToUse = 0;
	XPARAM(width);
	XPARAM(height);
	XPARAM(applypreset);
	XPARAM(noaudiogap);
	
#if __SYNTH_EDITOR
	EXT_PF_PAGE(AMP_Tool);
	EXT_PF_GROUP(SynthEditorUI);
	PF_LINK(AMP_Tool, SynthEditorUI);
	PF_TIMER(UIUpdate, SynthEditorUI);
	PF_TIMER(UIDraw, SynthEditorUI);

	
#endif

#if !__FINAL
	PARAM(renderSynth, "[AMP] Render X seconds of synth output then quit");
	XPARAM(synthname);

	PARAM(synthsound, "[AMP] Specify the ModularSynthSound used to render the synth being edited");
	PARAM(dspmode, "[AMP] Attach AMP to the master submix and run as a dsp effect, rather than a pcm source");
#endif
}

#if __WIN32PC
XPARAM(autodepthstencil);
#endif

class audTestAudioEntity : public rage::audEntity
{
public:
	AUDIO_ENTITY_NAME(audTestAudioEntity);

	audTestAudioEntity(){}
	~audTestAudioEntity(){}
};

#if __WIN32
#pragma warning(disable: 4748)
#endif
class audSampleSynth: public ragesamples::audSample
{
	
public:

	float RenderPcmSource(const char *destFileName, audPcmSource *source, float duration)
	{
		fiStream *outputStream = ASSET.Create(destFileName, "raw");
		if(!outputStream)
			return false;

		float timeRendered = 0.0f;
		source->Start();
		source->StartPhys(0);

		const float *outputBuffer = g_FrameBufferPool->GetBuffer_ReadOnly(source->GetBufferId(0));
		const float frameLengthS = kMixBufNumSamples / (float)kMixerNativeSampleRate;

		sysPerformanceTimer timer("Timer");
		while(timeRendered < duration)
		{
			timer.Start();
			source->GenerateFrame();
			timer.Stop();
			for(u32 i = 0; i < kMixBufNumSamples; i++)
			{
				short sample = (short)(outputBuffer[i] * 32767.f);
				outputStream->WriteShort(&sample,1);
			}
			timeRendered += frameLengthS;
			if(source->IsFinished())
			{
				audDisplayf("Finished after %.2fS", timeRendered);
				break;
			}
		}
		source->Stop();
		source->StopPhys(0);

		outputStream->Close();
		return timer.GetTimeMS();
	}

	void RenderSynth(const char *synthName, const float duration)
	{
		audDisplayf("Rendering %fs of synth '%s'...", duration, synthName);

		synthCorePcmSource *synth = rage_new synthCorePcmSource();

		float optTime = 0.0f;
		

		sysPerformanceTimer optTimer("Optimised");
		optTimer.Start();

		if(!synth->Init(atStringHash(synthName)))
		{
			audErrorf("Failed to find compiled synth %s", synthName);
		}
		else
		{
			const char *presetName = "";
			if(PARAM_applypreset.Get(presetName))
			{
				synth->ApplyPreset(atStringHash(presetName));
			}
			optTimer.Stop();
			char fileName[64];
			formatf(fileName, "renders\\%s_compiled", synthName);
			optTime = RenderPcmSource(fileName, synth, duration);
			delete synth;

			optTime += optTimer.GetTimeMS();
		}
			
		float ampTime = 0.0f;
		sysPerformanceTimer amp("AMP");
		amp.Start();
		s32 synthId = synthSynthesizer::Create(synthName);					
		if(synthId == -1)
		{
			audErrorf("Failed to find AMP synth %s", synthName);
		}
		else
		{	
			synthSynthesizer *synth = reinterpret_cast<synthSynthesizer*>(audDriver::GetMixer()->GetPcmSource(synthId));
			const char *presetName = "";
			if(PARAM_applypreset.Get(presetName))
			{
				synth->ApplyPreset(synthSynthesizer::FindPreset(presetName));
			}

			amp.Stop();
			char fileName[64];
			formatf(fileName, "renders\\%s_original", synthName);
			ampTime = RenderPcmSource(fileName, synth, duration);
			audPcmSourceInterface::StopAndRelease(synthId);

			ampTime += amp.GetTimeMS();
		}
		

		audDisplayf("Optimised: %.2fms AMP: %.2fms - %.1fx", optTime, ampTime, ampTime / Max(1.f,optTime));
	}

	void InitClient()
	{
		audSample::InitClient();

#if !__FINAL
		float duration = 1.0f;
		if(PARAM_renderSynth.Get(duration))
		{

			// Prevent the mixer getting in the way
			audDriver::GetMixer()->EnterBML();

			struct TestCases
			{
				const char *baseName;
				u32 numTests;
			};

			const char *synthName = "";
			if(PARAM_synthname.Get(synthName))
			{
				RenderSynth(synthName, duration);
			}
			else
			{			
				const char *testNames[] = { 
					"TESTS_01_OSC",
					"TESTS_01_1POLE",
					"TESTS_01_MIX",
					"TESTS_01_BIQUAD",
					"TESTS_01_INVERTER",
					"TESTS_01_BASIC",
					"TESTS_01_ASSET",
					"TESTS_01_ENV",
				};
				const u32 numTests = sizeof(testNames) / sizeof(testNames[0]);
				
				for(u32 testIndex = 0; testIndex < numTests; testIndex++)
				{
					for(u32 i = 0; i < 200; i++)
					{
						char synthName[64];
						formatf(synthName, "%s_%02d", testNames[testIndex], i+1);
						if(synthSynthesizer::GetMetadataManager().GetObjectMetadataPtr(synthName))
						{
							RenderSynth(synthName, duration);
						}
						else
						{
							break;
						}
					}
				}	
			}

			audDriver::GetMixer()->ExitBML();

			m_WantsExit = true;
			audDisplayf("Done");
		}
#endif
		grmShaderFactory::CreateStandardShaderFactory(true);

		m_DrawBackground = false;
		m_DrawGrid = false;
		m_DrawHelp = false;
		m_DrawWorldAxes = false;
		m_DrawTimeBars = false;

		m_CameraActive = true;

		m_TestEntity.Init();
		m_SynthSound = NULL;

#if __SYNTH_EDITOR

		synthSynthesizerView::InitClass();
		
		m_SynthView = rage_new synthSynthesizerView();
		const char *synthName = "Synth1";
		PARAM_synthname.Get(synthName);

		m_SynthView->Init(synthName);
	
		if(!m_WantsExit)
		{
			if(PARAM_dspmode.Get())
			{
				g_audEnvironment->GetMasterSubmix()->SetEffect(0, rage_new synthSynthesizerDspEffect(m_SynthView->GetSynth()), 0xff);
			}
			else
			{
				audSoundInitParams initParams;
				initParams.Pan = 0;
				initParams.IsAuditioning = true;
				const char *synthEditorSound = "SYNTH_EDITOR_SOUND";
				PARAM_synthsound.Get(synthEditorSound);
				m_TestEntity.CreateAndPlaySound_Persistent(synthEditorSound, &m_SynthSound, &initParams);
			}			
		}
#endif

		s32 numMidiDevices = audMidiIn::GetNumDevices();
		Displayf("%d MIDI inputs found", numMidiDevices);
		for(s32 i = 0; i < numMidiDevices; i++)
		{
			audMidiIn midiIn;
			if(midiIn.Init(i))
			{
				Displayf("MIDI Device %d: %s (%u:%u)", i, midiIn.GetName(), midiIn.GetManufacturerId(), midiIn.GetProductId());
				midiIn.Shutdown();
			}
		}
	}

#if __BANK
	static void WriteSynthXmlCB()
	{
#if __SYNTH_EDITOR
		fiStream *stream = ASSET.Create("synth", "xml");
        synthSynthesizerView::Get()->GetSynth()->Serialize(stream, synthSynthesizerView::Get()->GetSynth()->GetName());
		stream->Close();
#endif
	}

	void AddWidgetsClient()
	{
		bkBank &bank = BANKMGR.CreateBank("Sample synth");		
		bank.AddToggle("CameraActive", &m_CameraActive);
		bank.AddToggle("EnableSynth", &m_EnableSynth);
		
		bank.AddButton("WriteSynthXml", CFA(WriteSynthXmlCB));
		AddSampleWidgets(bank);
	}

#endif

	void UpdateClient()
	{
		SYNTH_EDITOR_ONLY(PF_FUNC(UIUpdate));

		audSample::UpdateClient();

	
#if __SYNTH_EDITOR
		m_SynthView->DisableCamera(!m_CameraActive);
		m_SynthView->Update();

		enum AssetChangeStates
		{
			Normal = 0,
			Waiting1,
			Waiting2,
			Waiting3,
			Creating,
		};

		static AssetChangeStates changingAssetState = Normal;
		static const char *newAssetName = NULL;

		if(changingAssetState == Normal && m_SynthView->WantsToChangeAsset())
		{
			newAssetName = StringDuplicate(m_SynthView->GetNewAssetName());

			if(m_SynthSound)
			{
				m_SynthSound->StopAndForget();
			}

			audDriver::GetMixer()->EnterBML();

			m_SynthView->Shutdown();
			delete m_SynthView;
			m_SynthView = NULL;
			
			m_SynthView = rage_new synthSynthesizerView();
			m_SynthView->Init(newAssetName);
	
			audDriver::GetMixer()->ExitBML();

			audSoundInitParams initParams;
			initParams.Pan = 0;
			initParams.IsAuditioning = true;
			const char *synthEditorSound = "SYNTH_EDITOR_SOUND";
			PARAM_synthsound.Get(synthEditorSound);
			m_TestEntity.CreateAndPlaySound_Persistent(synthEditorSound, &m_SynthSound, &initParams);

			changingAssetState = Normal;
		}

#endif // __SYNTH_EDITOR
		if(!m_CameraActive && grcViewport::GetCurrent())
		{
			grcViewport::GetCurrent()->SetCameraMtx(RCC_MAT34V(GetCameraMatrix()));
		}
	}

	void DrawClient()
	{
#if __SYNTH_EDITOR
		PF_FUNC(UIDraw);
		if(m_SynthView)
		{
			m_SynthView->Draw();
		}
#else
		g_AudioEngine.DrawDebug();
#endif
	}

	void ShutdownClient()
	{
		/*SYNTH_EDITOR_ONLY(m_SynthView->Shutdown());
		SYNTH_EDITOR_ONLY(delete m_SynthView);
*/
		audSample::ShutdownClient();
		synthSynthesizerView::ShutdownClass();
	}

	Matrix34 m_LastCamMtx;
	Matrix34 m_CurrentCamMtx;

	SYNTH_EDITOR_ONLY(synthSynthesizerView *m_SynthView);

	audTestAudioEntity m_TestEntity;
	audSound *m_SynthSound;
	bool m_EnableSynth;
	bool m_CameraActive;
};


int Main()
{

	audSampleSynth sample;

	SYNTH_EDITOR_ONLY(PARAM_autodepthstencil.Set(""));
	SYNTH_EDITOR_ONLY(PARAM_noaudiogap.Set(""));

	grcDevice::SetDefaultEffectName("x:\\gta5\\build\\dev\\common\\shaders\\waveform");
	sample.Init("");

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
