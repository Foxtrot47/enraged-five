// 
// sample_audcore/sample_streaming.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 
// Test with gta5 assets: -path x:\gta5\build\dev\PLATFORM\audio -audiopack x:\gta5\build\dev\PLATFORM\audio\audio.rpf -audiopacksfx x:\gta5\build\dev\pc\audio\sfx\resident.rpf -audiopacksfx2 x:\gta5\build\dev\pc\audio\sfx\test.rpf
// (PLATFORM is ps3, xbox360, pc, etc) 

#include "sample_audcore.h"

#include "audioengine/entity.h"
#include "system/main.h"
#include "system/performancetimer.h"
#include "audiosoundtypes/streamingsound.h"
#include "audiohardware/waveslot.h"

#if __BANK
const u32 g_MaxSoundNameLength = 64;
#endif

using namespace rage;

namespace ragesamples
{
PARAM(teststreams, "Test streaming sound");

class audStreamingAudioEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audStreamingAudioEntity);

	audStreamingAudioEntity() : 
		 m_LPFCutoff(23900),
		 m_HPFCutoff(0),
		 m_StreamingBucketId(~0U),
		 m_StreamingSound(NULL),
		 m_LastPlayStreamTimeMs(0)
	{
		m_StreamingSoundName = NULL;
	}

	void Init()
	{
		audEntity::Init();
		m_StreamingBucketId = audSound::GetStaticPool().GetNextReservedBucketId();
	}


	void SetFilters(const u32 lowPassCutoff, const u32 highPassCutoff)
	{
		m_LPFCutoff = lowPassCutoff;
		m_HPFCutoff = highPassCutoff;
	}

	void PlayStream(const char *soundName, const char *slotName, const s32 startOffsetMs)
	{
		audWaveSlot *slot = audWaveSlot::FindWaveSlot(slotName);
		Assert(slot);
		m_Slot = slot;
		StopStream();

		audSoundInitParams params;
		params.StartOffset = startOffsetMs;
		params.IsStartOffsetPercentage = true;
		Assign(params.BucketId, m_StreamingBucketId);
		CreateSound_PersistentReference(soundName, &m_StreamingSound, &params);

		if(m_StreamingSound != NULL)
		{
			//TODO: Re-implement audStreamingSound::SetIsLooping()
			//((audStreamingSound*)m_StreamingSound)->SetIsLooping(true);
			Displayf("Played stream %p", m_StreamingSound);
			m_StreamingSound->PrepareAndPlay(slot, true, -1);
		}
		m_StreamingSoundName = soundName;
		m_LastPlayStreamTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();
	}

	void StopStream(void)
	{
		if(m_StreamingSound)
		{
			Displayf("Stopped stream");
			m_StreamingSound->StopAndForget();
		}
		m_StreamingSoundName = NULL;
	}

private:

	void PreUpdateService(u32 timeInMs)
	{
		const bool testMode = PARAM_teststreams.Get();
		if(m_StreamingSound)
		{
			m_StreamingSound->SetRequestedHPFCutoff(m_HPFCutoff);
			m_StreamingSound->SetRequestedLPFCutoff(m_LPFCutoff);
			
			m_LastKnownPosition = ((audStreamingSound*)m_StreamingSound)->GetCurrentPlayTimeOfWave();

			if(testMode)
			{
				if(timeInMs % 5000 < 30 && audEngineUtil::ResolveProbability(0.5f))
				{
					Displayf("%u: Randomly stopping the stream ...", audEngineUtil::GetCurrentTimeInMilliseconds());
					m_StreamingSound->StopAndForget();
				}
			}
		}
		else if(testMode && m_StreamingSoundName)
		{
			const u32 offset = 10000;
			Sound *compressedMetadata = (Sound*)g_AudioEngine.GetSoundManager().GetFactory().GetMetadataPtr(atStringHash(m_StreamingSoundName));
			Sound uncompressedMetadata;
			const StreamingSound *streamingSoundMetadata = audSound::DecompressMetadata<StreamingSound>(compressedMetadata, uncompressedMetadata);

			Assert(compressedMetadata && streamingSoundMetadata && compressedMetadata->ClassId == StreamingSound::TYPE_ID);

			const u32 soundLengthMs = streamingSoundMetadata->Duration;
			Assert(soundLengthMs);

			u32 computedStartOffset = (m_LastKnownPosition + offset) % soundLengthMs;
			if(computedStartOffset > soundLengthMs * 0.9f)
			{
				computedStartOffset = 0;
			}

			Displayf("%u: %s has stopped, resuming from %u (was %u length %u)", audEngineUtil::GetCurrentTimeInMilliseconds(),
				m_StreamingSoundName,computedStartOffset, m_LastKnownPosition, soundLengthMs );
			audSoundInitParams params;
			params.StartOffset = computedStartOffset;
	
			Assign(params.BucketId, m_StreamingBucketId);
			CreateSound_PersistentReference(m_StreamingSoundName, &m_StreamingSound, &params);

			if(m_StreamingSound != NULL)
			{
				// TODO: Re-implement audStreamingSound::SetIsLooping()
				//((audStreamingSound*)m_StreamingSound)->SetIsLooping(true);
				Displayf("Played stream %p", m_StreamingSound);
				m_StreamingSound->PrepareAndPlay(m_Slot, true, -1);
			}
			
			m_LastPlayStreamTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();
		}
	}

	audWaveSlot *m_Slot;
	const char *m_StreamingSoundName;
	u32 m_LastKnownPosition;
	u32 m_LPFCutoff, m_HPFCutoff;
	u32 m_StreamingBucketId;
	audSound *m_StreamingSound;
	u32 m_LastPlayStreamTimeMs;
};

class audSampleStreaming : public audSample
{
public:
	audSampleStreaming()
	{
		m_HPFCutoff = 0;
		m_LPFCutoff = 23900;
		m_ShouldPlay = true;
		m_TimeToMove = 0;
	}

	void InitClient()
	{
		audSample::InitClient();
		m_StreamingAudioEntity.Init();		
	}

	void UpdateClient()
	{
		audSample::UpdateClient();

		if(PARAM_teststreams.Get())
		{
			const u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();
		
			if(now > m_TimeToMove)
			{
				if(m_ShouldPlay)
				{
					s32 startOffsetMs = audEngineUtil::GetRandomNumberInRange(0, 100);
					
					const char *songNames[] = {
						"RADIO_01_CLASS_ROCK_BLACK_BETTY",
						"RADIO_01_CLASS_ROCK_RADIO_GA_GA",
						"RADIO_02_POP_COOLER_THAN_ME"

/* 
						"MEX_MEX_SONG_01",
						"MEX_MEX_SONG_02",
						"MEX_MEX_SONG_03",
						"MEX_MEX_SONG_04",
						"MEX_MEX_SONG_05",
						"MEX_MEX_SONG_06",
						"MEX_MEX_SONG_07",

						"FTR_FTR_SONG_01",
						"FTR_FTR_SONG_02",
						"FTR_FTR_SONG_03",
						"FTR_FTR_SONG_04",
						"FTR_FTR_SONG_05",
						"FTR_FTR_SONG_06",
						"FTR_FTR_SONG_07",
						"FTR_FTR_SONG_08",
						"FTR_FTR_SONG_09",

						"NRT_NRT_SONG_01",
						"NRT_NRT_SONG_02",
						"NRT_NRT_SONG_03",
						"NRT_NRT_SONG_04",
						"NRT_NRT_SONG_05",
						"NRT_NRT_SONG_06",

						"MP_COUNTDOWN_SONG_01",
						"MP_COUNTDOWN_SONG_02" */
					};
					const char *songName = songNames[audEngineUtil::GetRandomNumberInRange(0,sizeof(songNames) / sizeof(songNames[0]) - 1)];
					
					
					Displayf("%s startOffset: %d%%", songName, startOffsetMs);

					m_StreamingAudioEntity.PlayStream(songName, "INTERACTIVE_MUSIC", startOffsetMs);
					if(audEngineUtil::ResolveProbability(0.1f))
					{
						m_TimeToMove = now + (u32)audEngineUtil::GetRandomNumberInRange(60000,500000);
					}
					else
					{
						m_TimeToMove = now + (u32)audEngineUtil::GetRandomNumberInRange(0,5000);
					}
				}
				else
				{
					m_StreamingAudioEntity.StopStream();
					m_TimeToMove = now + (u32)audEngineUtil::GetRandomNumberInRange(0,60);				
				}

				Displayf("%u: %s, next change in %f", now, m_TimeToMove ? "played" : "stopped", (m_TimeToMove-now)*0.001f);
				m_ShouldPlay = !m_ShouldPlay;
			}
		}

	}

	bool m_ShouldPlay;
	u32 m_TimeToMove;
	

	void PlayStream(const char *soundName, const char *slotName, const s32 startOffsetMs)
	{
		m_StreamingAudioEntity.PlayStream(soundName, slotName, startOffsetMs);
	}

	void StopStream()
	{
		m_StreamingAudioEntity.StopStream();
	}

#if __BANK
	static void PlayStreamCallback(audSampleStreaming *_this)
	{
		_this->PlayStream((const char *)m_SoundName, (const char *)m_SlotName, (const s32)m_StartOffsetMs);
	}

	static void StopStreamCallback(audSampleStreaming *_this)
	{
		_this->StopStream();
	}

	static void CutoffChangedCB(audSampleStreaming *_this)
	{
		_this->m_StreamingAudioEntity.SetFilters(_this->m_LPFCutoff, _this->m_HPFCutoff);
	}

	virtual void AddWidgetsClient()
	{
		bkBank &bk = BANKMGR.CreateBank("rage - Streaming Sample", 400, 200);

		bk.AddText("Sound Name", m_SoundName, g_MaxSoundNameLength, false);
		bk.AddText("Slot Name", m_SlotName, g_MaxSoundNameLength, false);
		bk.AddSlider("Start Offset (ms)", &m_StartOffsetMs, 0, 300000, 1000);
		bk.AddSlider("HPFCutoff",&m_HPFCutoff, 0, 16000, 1, datCallback(CFA1(CutoffChangedCB), this));
		bk.AddSlider("LPFCutoff",&m_LPFCutoff, 100, 23900, 1, datCallback(CFA1(CutoffChangedCB), this));
		bk.AddButton("Play Stream", datCallback(CFA1(PlayStreamCallback), this));
		bk.AddButton("Stop Stream", datCallback(CFA1(StopStreamCallback), this));

		AddSampleWidgets(bk);
	}

	static char m_SoundName[g_MaxSoundNameLength];
	static char m_SlotName[g_MaxSoundNameLength];
	static s32 m_StartOffsetMs;
#endif

private:
	u32 m_HPFCutoff, m_LPFCutoff;
	audStreamingAudioEntity m_StreamingAudioEntity;

};

#if __BANK
char audSampleStreaming::m_SoundName[] = "RADIO_01_CLASS_ROCK_BLACK_BETTY";
char audSampleStreaming::m_SlotName[] = "RADIO1_A";
s32 audSampleStreaming::m_StartOffsetMs = 0;
#endif // __BANK

}// namespace ragesamples

// main application
using namespace ragesamples;
int Main()
{
	audSampleStreaming sample;
	sample.Init("");
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}
