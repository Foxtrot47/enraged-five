set TESTERS=sample_audio_console sample_audioentities sample_virtualisation sample_speech sample_streaming sample_synth
set LIBS=%RAGE_GFX_LIBS%
set SAMPLE_LIBS=sample_audio %RAGE_SAMPLE_GRCORE_LIBS%
set LIBS=%LIBS% %SAMPLE_LIBS% %RAGE_CORE_LIBS% %RAGE_AUD_LIBS%
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples
