//
// sample_audcore/sample_audio_console.cpp
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
#include "audiohardware/driverutil.h"
#include "audiohardware/waveslot.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/curverepository.h"
#include "audioengine/categorymanager.h"
// #include "audioengine/effectmanager.h"
#include "audioeffecttypes/biquadfiltereffect.h"
#include "system/main.h"
#include "sample_audcore.h"
#include "entity.h"
#include "grcore/im.h"
#include "input/pad.h"

using namespace rage;

namespace ragesamples
{
	extern f32 g_ListenerRoomSize, g_ListenerDamping, g_ListenerWet, g_ListenerDry;
audEffect *sm_EffectsChain;
class audSampleAudioConsole : public audSample
{
public:
	audSampleAudioConsole()
	{
		m_x = 0.5f;
		m_y = 0.6f;
	}

	void InitClient()
	{
		audSample::InitClient();
	}

	virtual void DrawClient()
	{
		// Initialize render state
		grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcViewport::SetCurrentWorldIdentity();
		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcStateBlock::SetStates(grcStateBlock::RS_NoBackfaceCull,grcStateBlock::DSS_IgnoreDepth,grcStateBlock::BS_Default);

		DrawBackground();
		DrawEmitter();

		g_AudioEngine.DrawDebug();
	}

	void DrawBackground()
	{
		Color32 bottomGreen(0, 120, 0);
		Color32 topGreen(0, 85, 0);

		grcBegin(drawTriStrip,4);
		grcColor(topGreen);
		grcVertex2i(0,0);
		grcVertex2i(GRCDEVICE.GetWidth(),0);

		grcColor(bottomGreen);
		grcVertex2i(0,GRCDEVICE.GetHeight());
		grcVertex2i(GRCDEVICE.GetWidth(),GRCDEVICE.GetHeight());
		grcEnd();

		// Work out our screen limits, and hence our positions
		f32 screenWidth = (f32)GRCDEVICE.GetWidth();
		f32 screenHeight = (f32)GRCDEVICE.GetHeight();
		f32 width = screenWidth/40.0f;
		f32 height = screenHeight/40.0f;
		for (f32 x=0.0f; x<=screenWidth; x+=width)
		{
			grcBegin(drawTriStrip, 4);
			grcColor(Color32(128, 128, 128));
			grcVertex2f(x, 0.0f);
			grcVertex2f(x+1.0f, 0.0f);
			grcVertex2f(x, screenHeight);
			grcVertex2f(x+1.0f, screenHeight);
			grcEnd();
		}
		for (f32 y=0.0f; y<=screenHeight; y+=height)
		{
			grcBegin(drawTriStrip, 4);
			grcColor(Color32(128, 128, 128));
			grcVertex2f(0.0f, y);
			grcVertex2f(screenWidth, y);
			grcVertex2f(0.0f, y+1.0f);
			grcVertex2f(screenWidth, y+1.0f);
			grcEnd();
		}
		// Draw cross in the centre
		grcBegin(drawTriStrip, 4);
		grcColor(Color32(255, 255, 255));
		grcVertex2f(screenWidth/2.0f, screenHeight/2.0f - height);
		grcVertex2f(screenWidth/2.0f + 1.0f, screenHeight/2.0f - height);
		grcVertex2f(screenWidth/2.0f, screenHeight/2.0f + height);
		grcVertex2f(screenWidth/2.0f + 1.0f, screenHeight/2.0f + height);
		grcEnd();
		grcBegin(drawTriStrip, 4);
		grcColor(Color32(255, 255, 255));
		grcVertex2f(screenWidth/2.0f - width, screenHeight/2.0f);
		grcVertex2f(screenWidth/2.0f + width, screenHeight/2.0f);
		grcVertex2f(screenWidth/2.0f - width, screenHeight/2.0f + 1.0f);
		grcVertex2f(screenWidth/2.0f + width, screenHeight/2.0f + 1.0f);
		grcEnd();
	}

	void DrawEmitter()
	{
		// Turn our emitter location into screen coords.
		f32 screenWidth = (f32)GRCDEVICE.GetWidth();
		f32 screenHeight = (f32)GRCDEVICE.GetHeight();

		f32 width = screenWidth/80.0f;
		f32 height = screenHeight/80.0f;

		f32 x = m_x * screenWidth;
		f32 y = (1.0f - m_y) * screenHeight;

		grcBegin(drawTriStrip, 4);
		grcColor(Color32(255, 255, 255));
		grcVertex2f(x - width/2.0f, y - height/2.0f);
		grcVertex2f(x + width/2.0f, y - height/2.0f);
		grcVertex2f(x - width/2.0f, y + height/2.0f);
		grcVertex2f(x + width/2.0f, y + height/2.0f);
		grcEnd();
	}


#if __BANK

	virtual void AddWidgetsClient()
	{
		bkBank &bk = BANKMGR.CreateBank("rage - Audio Entities Sample", 400, 200);

		AddSampleWidgets(bk);
	}
#endif

	void UpdateClient()
	{
		// Update Inputs
		UpdateInput();
		u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();

		Matrix34 mic;
		mic.Identity();
		g_AudioEngine.SetVolumeListenerMatrix(mic);
		g_AudioEngine.SetPanningListenerMatrix(mic);

		// now update our audition position
		f32 x = (m_x - 0.5f) * 40.0f;
		f32 y = (m_y - 0.5f) * 40.0f;
		Vector3 pos;
		pos.x = x;
		pos.y = 0.0f;//y;
		pos.z = y;//0.0f;
		g_AudioEngine.SetAuditionPosition(pos, true);

		m_AudioController.Update(now);

		for(int i = 0 ; i < m_Entities.GetCount(); i++)
		{
			m_Entities[i]->Update(now);
		}

		// Update the CategoryManager, so any settings changed will take effect.
		g_AudioEngine.GetCategoryManager().CommitCategorySettings();

		audWaveSlot::UpdateSlots();
		
		//AUDIOENGINE.SetListenerReverbParams(g_ListenerRoomSize, g_ListenerDamping, g_ListenerWet, g_ListenerDry);
	}

	void UpdateInput()
	{
		static f32 MOVE_SPEED = 0.0075f;
		u32 buttons = ioPad::GetPad(0).GetButtons();
		if( buttons & ioPad::LUP )
			m_y+=MOVE_SPEED;
		if( buttons & ioPad::LDOWN )
			m_y-=MOVE_SPEED;
		if( buttons & ioPad::LLEFT )
			m_x-=MOVE_SPEED;
		if( buttons & ioPad::LRIGHT )
			m_x+=MOVE_SPEED;

		m_x = Min(1.0f, Max(0.0f, m_x));
		m_y = Min(1.0f, Max(0.0f, m_y));

	}


private:

	f32 m_x, m_y;

};
}// namespace ragesamples

// main application
using namespace ragesamples;
int Main()
{
	audSampleAudioConsole console;
	console.Init("");
	console.UpdateLoop();
	console.Shutdown();
	return 0;
}

