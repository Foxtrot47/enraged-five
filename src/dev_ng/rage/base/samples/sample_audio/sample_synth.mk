top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = sample_synth
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib

..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageGraphics -f RageGraphics.mk

LIBS += ..\sample_grcore/$(INTDIR)/sample_grcore.lib

..\sample_grcore/$(INTDIR)/sample_grcore.lib: FORCE
	$(MAKE) -C ..\sample_grcore -f sample_grcore.mk

LIBS += ..\..\..\..\rage\base\src\init/$(INTDIR)/init.lib

..\..\..\..\rage\base\src\init/$(INTDIR)/init.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\init -f init.mk

LIBS += ..\sample_file/$(INTDIR)/sample_file.lib

..\sample_file/$(INTDIR)/sample_file.lib: FORCE
	$(MAKE) -C ..\sample_file -f sample_file.mk

LIBS += ..\..\..\..\rage\base\src\devcam/$(INTDIR)/devcam.lib

..\..\..\..\rage\base\src\devcam/$(INTDIR)/devcam.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\devcam -f devcam.mk

LIBS += ..\..\..\..\rage\base\src\gizmo/$(INTDIR)/gizmo.lib

..\..\..\..\rage\base\src\gizmo/$(INTDIR)/gizmo.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\gizmo -f gizmo.mk

LIBS += ..\..\..\..\rage\base\src\grcustom/$(INTDIR)/grcustom.lib

..\..\..\..\rage\base\src\grcustom/$(INTDIR)/grcustom.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\grcustom -f grcustom.mk

LIBS += ..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\..\..\..\rage\base\src\\vcproj\RageAudio\$(INTDIR)\RageAudio.lib

..\..\..\..\rage\base\src\\vcproj\RageAudio\$(INTDIR)\RageAudio.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\\vcproj\RageAudio -f RageAudio.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\rage\base\src -I ..\..\..\..\rage\base\samples -I ..\..\..\..\rage\stlport\STLport-5.0RC5\src -I ..\..\..\..\rage\base\tools\cli -I ..\..\..\..\rage\base\tools\dcc\libs -I ..\..\..\..\rage\base\tools\libs

$(INTDIR)\sample_synth.obj: sample_synth.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\sample_synth.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\sample_synth.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
