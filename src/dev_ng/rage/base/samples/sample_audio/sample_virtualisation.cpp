// 
// sample_audcore/sample_virtualisation.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#include "audioengine/engineutil.h"
#include "audioengine/curverepository.h"
#include "audioengine/entity.h"
#include "audiohardware/driver.h"
#include "audiohardware/device.h"

#include "system/timemgr.h"

#include "input/pad.h"
#include "system/main.h"
#include "sample_audcore.h"
#include "entity.h"
#include "grcore/im.h"

#include "rockstarbmp.h"
#include "psbmp.h"

#include "audiosynth/uicomponent.h"
#include "grmodel/modelfactory.h"
#include "grmodel/setup.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"

PARAM(bluralpha, "Alpha value for trails");
using namespace rage;

namespace ragesamples
{
// defines complexity of sphere, reduce to improve rendering performance
const u32 g_SphereSegments = 8;
const f32 g_RotAngle = PI/180.f;
const f32 g_MaxFrameMoveStep = 0.4f;
enum DemoEvent_t
	{
		DEMO_EVENT_NONE,
		DEMO_EVENT_START_EXPLODING,
		DEMO_EVENT_MOVE_TO_LOGO_0,
		DEMO_EVENT_MOVE_TO_LOGO_1,
		DEMO_EVENT_MOVE_BACK_TO_NORMAL,
		DEMO_EVENT_ADD_DRUM_LOOPS,
		DEMO_EVENT_REMOVE_DRUM_LOOPS,
		DEMO_EVENT_NEXT_TRACK_PAIR,
		DEMO_EVENT_CAM_MODE_MANUAL,
		DEMO_EVENT_CAM_MODE_TRACK,
	}DemoEvents;

enum CameraMode_t
	{
		CAMERA_MODE_MANUAL,
		CAMERA_MODE_TRACK,
	}CameraModes;

	
typedef struct
{
	u32 eventId;
	u32 time;
}DemoEvent;

typedef struct
{
	u32 PositionIndex;
	u32 TrackIndex;
}TrackCamPair;


#if __BANK
TrackCamPair g_TrackCamPairs[] = { {0,40}, {3,50}, {90,4}, {5, 10}};
#else
TrackCamPair g_TrackCamPairs[] = { {400,401}, {850,860}, {2200,1793}, {2390, 410}};
#endif

class audDemoAudioEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audDemoAudioEntity);

	audDemoAudioEntity()
		: m_DeadeyeSound(NULL)
		, m_IcecreamSound(NULL)
		, m_SpeedScalar(0.f)
		, m_Direction(1.f)
		, m_HeatStressSound(NULL)
	{

	}

	void TriggerSound(const char *soundName, const float volume = 0.f)
	{
		audSoundInitParams initParams;
		initParams.Pan = 0;
		initParams.Volume = volume;
		CreateAndPlaySound(soundName, &initParams);
		Displayf("Playing %s", soundName);
	}

	void TriggerDeadeye(const float time)
	{
		if(m_DeadeyeSound)
		{
			return;
		}

		m_DeadeyeTimer = time;

		TriggerSound("DEAD_EYE_START_MASTER");
		
		audSoundInitParams initParams;
		initParams.Pan = 0;
		initParams.UpdateEntity = true;
		initParams.Volume = 3.f;
		CreateSound_PersistentReference("DEADEYE_LOOP_MASTER", &m_DeadeyeSound, &initParams);
		if(m_DeadeyeSound)
		{
			m_DeadeyeSound->SetVariableValueDownHierarchy(atStringHash("DEADEYE_TIME"), m_DeadeyeTimer);
			m_DeadeyeSound->PrepareAndPlay();
		}
	}

	void TriggerIcecreamVan()
	{
		if(m_IcecreamSound)
		{
			m_IcecreamSound->StopAndForget();
		}
		else
		{
			audSoundInitParams initParams;
			initParams.Pan = 0;
			initParams.UpdateEntity = true;
			initParams.Volume = -18.f;
			
			CreateSound_PersistentReference("ICEVAN", &m_IcecreamSound, &initParams);
			if(m_IcecreamSound)
			{
				m_IcecreamSound->SetRequestedFrequencySmoothingRate(0.0f);

				static int s_IceVanTune = 0;

				// step through all ice van tunes since playing the same one more than once at a time causes bad things to happen
				f32 *varPtr = m_IcecreamSound->GetVariableAddressDownHierarchy(atStringHash("variation"));
				const u32 numTunes = (u32)(varPtr?*varPtr+1:1);
				*varPtr = static_cast<f32>(s_IceVanTune);
				// lookup this variation tempo scaling
				float tempoScaling[] = { 
					1.f,
					0.5f,
					0.6f,
					0.45f,
					0.35f,
					0.5000f
					};
				m_IcecreamSound->SetVariableValueDownHierarchyFromName("tempoScaling", tempoScaling[s_IceVanTune]);
				
				s_IceVanTune = (s_IceVanTune + 1) % numTunes;


				m_IcecreamSound->PrepareAndPlay();
			}
		}
	}

	void ToggleHeatStress()
	{
		if(m_HeatStressSound)
		{
			Displayf("Stopping heat stress");
			m_HeatStressSound->StopAndForget();
		}
		else
		{
			audSoundInitParams initParams;
			initParams.UpdateEntity = true;
			initParams.Pan = 0;
			Displayf("Starting heat stress");
			CreateAndPlaySound_Persistent("HEAT_STRESS_HEAT_TICK_LOOP", &m_HeatStressSound, &initParams);
		}
	}

	void UpdateSound(audSound* sound, u32 timeInMs)
	{
		if(sound == m_DeadeyeSound)
		{
			m_DeadeyeTimer -= TIME.GetElapsedTime()-TIME.GetPrevElapsedTime();
			if(m_DeadeyeTimer <= 0.f)
			{
				m_DeadeyeTimer = 0.f;
			}
			sound->SetVariableValueDownHierarchy(atStringHash("DEADEYE_TIME"), m_DeadeyeTimer);
			if(m_DeadeyeTimer == 0.f)
			{
				TriggerSound("DEAD_EYE_STOP_MASTER");
				sound->StopAndForget();
			}
		}
		else if(sound == m_IcecreamSound)
		{
			m_Direction = -ioPad::GetPad(0).GetNormLeftY();
			m_SpeedScalar = Abs(m_Direction);
			
			const f32 damageFactor = Abs(ioPad::GetPad(0).GetNormRightY());
			const f32 pitchRange = 0.1f + (damageFactor*0.8f);
			const f32 speedRatio = m_SpeedScalar;
			const f32 delay = (0.250f * (1.f - speedRatio));
			static const audStringHash minPitchVar("minPitch");
			static const audStringHash maxPitchVar("maxPitch");
			static const audStringHash delayTimeVar("delayTime");
			static const audStringHash playDir("playDirection");
			sound->SetVariableValueDownHierarchy(minPitchVar, -pitchRange);
			sound->SetVariableValueDownHierarchy(maxPitchVar, pitchRange);
			sound->SetVariableValueDownHierarchy(delayTimeVar, delay);
			sound->SetVariableValueDownHierarchy(playDir, (m_Direction < -0.5f ? -1.f : 1.f));

			sound->SetRequestedPosition(g_AudioEngine.GetEnvironment().GetPanningListenerPosition());
			sound->GetRequestedSettings()->GetEnvironmentGameMetric().SetSourceEnvironmentReverbSize(0.7f);
			sound->GetRequestedSettings()->GetEnvironmentGameMetric().SetSourceEnvironmentReverbWet(0.5f);
		}
		else if(sound == m_HeatStressSound)
		{
			const float temp = Lerp(Abs(ioPad::GetPad(0).GetNormLeftY()), 120.f, 55.f);
			sound->SetVariableValueDownHierarchy(atStringHash("temperature"), temp);
		}
	}
private:

	float m_DeadeyeTimer;
	audSound *m_DeadeyeSound;
	audSound *m_IcecreamSound;
	float m_SpeedScalar;
	float m_Direction;

	audSound *m_HeatStressSound;
	
};

class audParticleAudioEntity : public audEntity
{
public:
	AUDIO_ENTITY_NAME(audParticleAudioEntity);

	// When the audio entity is created, immediately start playing the requested sound, updating its position via
	// an audTracker.
	void InitEntity(const char *soundName, audTracker* tracker)
	{
		audEntity::Init();

		audSoundInitParams params;
		params.Tracker = tracker;
		params.AttackTime = 1000;
		params.Volume = -15.f;
		
		audSound *sound;
		CreateSound_LocalReference(soundName, &sound, &params);
		if(sound)
		{
			sound->SetRequestedDopplerFactor(0.f);
			sound->PrepareAndPlay();
		}
		else
		{
			Warningf("Failed to create sound %s", soundName);
		}
	}
    
private:
};

class audParticleEntity : public audSampleGameEntityBase
{
public:
	audParticleEntity(Vector3 &centrePoint, Color32 color, const char *soundName)
	{
		m_Color = color;
		m_CurrentColor = color;
		m_TargetColor = color;

		m_Offset = centrePoint;

		m_CurrentLogo = 0;

		m_LogoPosition[0] = m_LogoPosition[1] = Vector3(100.f,100.f,-100.f);
		m_LogoColor[0] = m_LogoColor[1] = Color32(0,0,0);
		m_LogoColumn[0] = m_LogoColumn[1] = 0;

		m_RotatedVec.x = audEngineUtil::GetRandomNumberInRange(-14.f, 14.f);
		m_RotatedVec.y = audEngineUtil::GetRandomNumberInRange(-14.f, 14.f);
		m_RotatedVec.z = audEngineUtil::GetRandomNumberInRange(-14.f, 14.f);

		m_RotAngle = g_RotAngle;

		if(audEngineUtil::ResolveProbability(0.5f))
		{
			m_RotAngle *= -1.0f;
		}

		m_Speed = audEngineUtil::GetRandomNumberInRange(0.2f, 1.3f);
		m_Scalar = 1.f;

		m_EntityState = NORMAL_ENTITY_STATE;

		m_ShouldDraw = true;
		m_ShouldHighlight = false;

		// make sure we have a valid position
		Update(0);

		m_AudioEntity.InitEntity(soundName, GetEntityTracker());
	}

	void Shutdown()
	{
		m_AudioEntity.Shutdown();
	}

	void Update(u32 UNUSED_PARAM(timeInMs))
	{
		Vector3 temp, pos;
		float diffx, diffy, diffz;
		u8 r,g,b;

		switch(m_EntityState)
		{
		case NORMAL_ENTITY_STATE:
			// move us around
			m_RotatedVec.RotateY(m_RotAngle * m_Speed);
			m_RotatedVec.RotateX(m_RotAngle * m_Speed);
			m_RotatedVec.RotateZ(m_RotAngle * m_Speed);

			pos = m_RotatedVec;
			pos.Scale(m_Scalar);
			pos.Add(m_Offset);

			SetPosition(pos);
			break;
		case MOVING_TO_LOGO_ENTITY_STATE:
			pos = GetPosition();
			diffx = fabs(pos.x - m_LogoPosition[m_CurrentLogo].x);
			diffy = fabs(pos.y - m_LogoPosition[m_CurrentLogo].y);
			diffz = fabs(pos.z - m_LogoPosition[m_CurrentLogo].z);

			if(diffx > g_MaxFrameMoveStep)
			{
				if(pos.x < m_LogoPosition[m_CurrentLogo].x)
				{
					pos.x += g_MaxFrameMoveStep;
				}
				else
				{
					pos.x -= g_MaxFrameMoveStep;
				}
			}
			else
			{
				pos.x = m_LogoPosition[m_CurrentLogo].x;
			}

			if(diffy > g_MaxFrameMoveStep)
			{
				if(pos.y < m_LogoPosition[m_CurrentLogo].y)
				{
					pos.y += g_MaxFrameMoveStep;
				}
				else
				{
					pos.y -= g_MaxFrameMoveStep;
				}
			}
			else
			{
				pos.y = m_LogoPosition[m_CurrentLogo].y;
			}

			if(diffz > g_MaxFrameMoveStep)
			{
				if(pos.z < m_LogoPosition[m_CurrentLogo].z)
				{
					pos.z += g_MaxFrameMoveStep;
				}
				else
				{
					pos.z -= g_MaxFrameMoveStep;
				}
			}
			else
			{
				pos.z = m_LogoPosition[m_CurrentLogo].z;
			}

			SetPosition(pos);

			if(diffx < 0.01f && diffy < 0.01f && diffz < 0.01f)
			{
				m_EntityState = LOGO_ENTITY_STATE;
			}
			break;

		case LOGO_ENTITY_STATE:
		/*	m_Position = m_LogoPosition[m_CurrentLogo];
			if(m_LogoColumn[m_CurrentLogo])
			{
				m_Position.z += sin((m_LogoColumn[m_CurrentLogo] + (timeInMs%360)/PI));
			}*/
			break;
		case MOVING_TO_NORMAL_ENTITY_STATE:
			
			temp = m_RotatedVec;
			temp.Scale(m_Scalar);
			temp.Add(m_Offset);

			pos = GetPosition();

			diffx = fabs(temp.x - pos.x);
			diffy = fabs(temp.y - pos.y);
			diffz = fabs(temp.z - pos.z);

			
			if(diffx > g_MaxFrameMoveStep)
			{
				if(pos.x < temp.x)
				{
					pos.x += g_MaxFrameMoveStep;
				}
				else
				{
					pos.x -= g_MaxFrameMoveStep;
				}
			}
			else
			{
				pos.x = temp.x;
			}

			if(diffy > g_MaxFrameMoveStep)
			{
				if(pos.y < temp.y)
				{
					pos.y += g_MaxFrameMoveStep;
				}
				else
				{
					pos.y -= g_MaxFrameMoveStep;
				}
			}
			else
			{
				pos.y = temp.y;
			}

			if(diffz > g_MaxFrameMoveStep)
			{
				if(pos.z < temp.z)
				{
					pos.z += g_MaxFrameMoveStep;
				}
				else
				{
					pos.z -= g_MaxFrameMoveStep;
				}
			}
			else
			{
				pos.z = temp.z;
			}

			if(diffx < 0.01f && diffy < 0.01f && diffz < 0.01f)
			{
				m_EntityState = NORMAL_ENTITY_STATE;
			}
			SetPosition(pos);
			break;
		}

		// transition colour
		r = m_CurrentColor.GetRed();
		g = m_CurrentColor.GetGreen();
		b = m_CurrentColor.GetBlue();

		u8 tr, tg, tb;

		tr = m_TargetColor.GetRed();
		tg = m_TargetColor.GetGreen();
		tb = m_TargetColor.GetBlue();

		const u8 colorStep = 2;

		if(abs(r - tr) <= colorStep)
		{
			m_CurrentColor.SetRed(tr);
		}
		else
		{
			if(r > tr)
			{
				m_CurrentColor.SetRed(r-colorStep);
			}
			else
			{
				m_CurrentColor.SetRed(r+colorStep);
			}
		}

		if(abs(g - tg) <= colorStep)
		{
			m_CurrentColor.SetGreen(tg);
		}
		else
		{
			if(g > tg)
			{
				m_CurrentColor.SetGreen(g-colorStep);
			}
			else
			{
				m_CurrentColor.SetGreen(g+colorStep);
			}
		}

		if(abs(b - tb) <= colorStep)
		{
			m_CurrentColor.SetBlue(tb);
		}
		else
		{
			if(b > tb)
			{
				m_CurrentColor.SetBlue(b-colorStep);
			}
			else
			{
				m_CurrentColor.SetBlue(b+colorStep);
			}
		}
	}

	void SetScalar(float scalar)
	{
		m_Scalar = scalar;
	}

	void SetShouldDraw(bool enable)
	{
		m_ShouldDraw = enable;
	}

	void SetHighlight(bool enable)
	{
		m_ShouldHighlight = enable;
	}

	void MoveToLogoState(u32 logoId)
	{
		m_CurrentLogo = logoId;
		m_TargetColor = m_LogoColor[m_CurrentLogo];
		m_EntityState = MOVING_TO_LOGO_ENTITY_STATE;
	}

	void MoveToNormalState()
	{
		m_TargetColor = m_Color;
		m_EntityState = MOVING_TO_NORMAL_ENTITY_STATE;
	}

	void SetLogoPositionAndColor(u32 logoId, u32 logoColumn, f32 x, f32 y, f32 z, u8 r, u8 g, u8 b)
	{
		m_LogoPosition[logoId].x = x;
		m_LogoPosition[logoId].y = y;
		m_LogoPosition[logoId].z = z;

		m_LogoColor[logoId] = Color32(r,g,b);

		m_LogoColumn[logoId] = logoColumn;
		m_CurrentLogo = logoId;
	}

	void Render()
	{
		if(m_ShouldDraw)
		{
			Vector3 pos = GetPosition();
			/*
			if(m_ShouldHighlight)
			{
				grcColor(Color32(1.f,1.f,1.f));
			}
			else
			{
				grcColor(m_CurrentColor);
			}*/
			Vector3 extents(0.1f,0.1f,0.1f);
			Vector3 min = pos - extents;
			Vector3 max = pos + extents;
			grcDrawBox(min, max, m_CurrentColor);
			//grcDrawSphere(0.1f, pos, g_SphereSegments, false, true);
			/*Matrix34 worldMat;
			worldMat.Identity();
			worldMat.Scale(Vector3(0.25f,0.25f,0.25f));
			worldMat.d = pos;
			grcWorldMtx(MATRIX34_TO_MAT34V(worldMat));
			
			synthUIComponent::SetShaderVar("g_TextColour", m_CurrentColor.GetV3());
			synthUIComponent::DrawVB("drawparticle");*/
		}
	}

private:
	audParticleAudioEntity m_AudioEntity;

	f32 m_Intensity, m_IntensityChange;
	s32 m_RedVal, m_GreenVal, m_BlueVal;
	Color32 m_Color, m_CurrentColor, m_TargetColor;
	f32 m_Speed, m_Scalar, m_CurrentScalar;
	
	Vector3 m_Offset, m_RotatedVec;

	Vector3 m_LogoPosition[2];
	Color32 m_LogoColor[2];
	u32 m_LogoColumn[2];

	u32		m_CurrentLogo;

	bool m_ShouldDraw, m_ShouldHighlight;
	

	f32 m_Size, m_RotAngle;

	bool m_RotateX, m_RotateY, m_RotateZ;

	enum EntityState
	{
		NORMAL_ENTITY_STATE,
		MOVING_TO_LOGO_ENTITY_STATE,
		LOGO_ENTITY_STATE,
		MOVING_TO_NORMAL_ENTITY_STATE,
	};

	EntityState m_EntityState;
};

class audSampleVirtualisation : public audSample
{
public:
	audSampleVirtualisation()
	{
		m_ParticleState = NORMAL_STATE;
		m_MoveToLogo = true;
		m_MoveToNormal = false;
		m_CurrentEventIndex = 0;

		m_CurrentTrackCamPair = 0;

		m_CameraMode = CAMERA_MODE_MANUAL;

		m_RunningVirtualisationDemo = false;

	}

	grcRenderTarget *m_BlurRT;
	grcRenderTarget *m_DepthRT;
	grcViewport *m_OrthoViewport;
	float m_BlurAlpha;

	void InitClient()
	{
		audSample::InitClient();
		grmShaderFactory::CreateStandardShaderFactory(true);

		m_BlurAlpha = 0.5f;

		if(PARAM_bluralpha.Get())
		{
			PARAM_bluralpha.Get(m_BlurAlpha);
		}
		m_DemoEntity.Init();

		const u32 rtWidth = GRCDEVICE.GetWidth();//>>2;
		const u32 rtHeight = GRCDEVICE.GetHeight();//>>2;
		m_BlurRT = grcTextureFactory::GetInstance().CreateRenderTarget("__synthview_rt",
			grcrtPermanent, rtWidth, rtHeight, 32);
		m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("__synthview_depth",
			grcrtDepthBuffer, rtWidth, rtHeight, 16);

		// ensure the render targets start clear
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_BlurRT, m_DepthBuffer);
		GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		 m_OrthoViewport = new grcViewport();

		 synthUIComponent::InitClass();

	}

	void audSampleVirtualisation::DrawClient()
	{
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_BlurRT, m_DepthBuffer);
		GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);

		audSample::DrawClient();

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		// blit to screen
		// need to blit our blurred RT to screen
		//GRCDEVICE.Clear(true, Color32(1.f,0.f,0.f,1.f),true, 1.f,false,0);
		grcViewport::SetCurrent(m_OrthoViewport);
		m_OrthoViewport->SetWorldIdentity();
		Matrix34 orthoCamMtx(M34_IDENTITY);
		orthoCamMtx.d.y = 0.f;
		orthoCamMtx.d.x = 0.f;
		orthoCamMtx.d.z = 1.f;
		m_OrthoViewport->SetCameraMtx(RCC_MAT34V(orthoCamMtx));
		m_OrthoViewport->Ortho(1.f, -1.f, 
			1.f, -1.f,
			0.f,1.f );

		synthUIComponent::SetShaderVar("FontTex",m_BlurRT);
		synthUIComponent::SetShaderVar("g_AlphaFade", m_BlurAlpha);
		synthUIComponent::DrawVB("drawvirtualisation");
	}

		void AddDemoEvent(u32 eventId, u32 timeMs)
		{
			DemoEvent *event = rage_new DemoEvent;

			event->eventId = eventId;
			event->time = timeMs;

			m_DemoEvents.PushAndGrow(event);
		}

	void VirtualisationDemo()
	{	
		// Vector3 centrePoint = Vector3(0.0f, 0.1f, 0.9f);
		Vector3 orbitSize;
		Color32 color = Color32(1.0f,0.0f,0.0f);

#if __BANK
		const u32 numParticlesPerSphere = 250;
#else
		const u32 numParticlesPerSphere = 250;
#endif

		Matrix34 trans;
		trans.Identity();


		trans.MakeTranslate(-24.f,0.f,0.f);
		trans.MakeRotateUnitAxis(Vector3(0.f,1.f,0.f), PI);
		BuildSpheres(numParticlesPerSphere, trans);

		trans.Identity();
		trans.MakeTranslate(24.f,0.f,0.f);
		BuildSpheres(numParticlesPerSphere, trans);

		ComputePositionsFromLogoMap(0, &gRockstarBmp_data[0]);
		ComputePositionsFromLogoMap(1, &gPSBmp_data[0]);

		m_DemoStartTime = audEngineUtil::GetCurrentTimeInMilliseconds();

		m_RunningVirtualisationDemo = true;

		Printf("particles: %d\n", m_Entities.GetCount());
	}

	bool m_RunningVirtualisationDemo;

	void BuildSpheres(const u32 numParticlesPerSphere, const Matrix34 &trans)
	{
		u32 firstParticleId = 0;

		Color32 color = Color32(1.f,0.f,0.f);
		Vector3 centrePoint = Vector3(0.f, 0.f, 0.f);
		trans.Transform(centrePoint);
		
		Printf("Creating bottom particles ...\n");
		firstParticleId = m_Entities.GetCount();
		for(u32 i=0; i<numParticlesPerSphere; i++)
		{
			RegisterEntity(rage_new audParticleEntity(centrePoint, color, "PARTICLE_BOTTOM"));
		}
		Printf("Bottom particles: %d - %d\n", firstParticleId, m_Entities.GetCount());

		centrePoint = Vector3(16.f, 16.f, 0.f);
		color = Color32(0.0f, 1.0f, 0.0f);
		trans.Transform(centrePoint);
		
		Printf("Creating left particles ...\n");
		firstParticleId = m_Entities.GetCount();
		for(u32 i=0; i<numParticlesPerSphere; i++)
		{
			RegisterEntity(rage_new audParticleEntity(centrePoint, color, "PARTICLE_LEFT"));
		}
		Printf("Left particles: %d - %d\n", firstParticleId, m_Entities.GetCount());
		
		centrePoint = Vector3(-16.f, 16.f, 0.f);
		color = Color32(0.0f,0.5f, 1.0f);
		trans.Transform(centrePoint);
		Printf("Creating right particles ...\n");
		firstParticleId = m_Entities.GetCount();
		for(u32 i=0; i<numParticlesPerSphere; i++)
		{
			RegisterEntity(rage_new audParticleEntity(centrePoint, color, "PARTICLE_RIGHT"));
		}
		Printf("Right particles: %d - %d\n", firstParticleId, m_Entities.GetCount());

		centrePoint = Vector3(0.f, 32.f, 0.f);
		color = Color32(1.f,1.f, 0.3f);
		trans.Transform(centrePoint);
		Printf("Creating top particles ...\n");
		firstParticleId = m_Entities.GetCount();
		for(u32 i=0; i<numParticlesPerSphere; i++)
		{
			RegisterEntity(rage_new audParticleEntity(centrePoint, color, "PARTICLE_TOP"));
		}
		Printf("Top particles: %d - %d\n", firstParticleId, m_Entities.GetCount());
	}

	void ComputePositionsFromLogoMap(u32 logoId, u8 *map)
	{
		const f32 sqsize = 15.f;
		const f32 zpos = 0.f, xpos = 0-(sqsize/2), ypos = (sqsize/2);
		const f32 floorSize = 100.f;
		const f32 floorzpos = 0-floorSize, floorxpos =((0-floorSize)/2), floorypos = 0.f;

		u32 numParticlesPerRow = (u32)floor(sqrt((f32)m_Entities.GetCount()));

		const u32 colorThreshold = 80;

		u8 r,g,b;
		f32 x,y,z;

		u8 logox, logoy;
		u32 c = 0;

		for(u32 i = 0; i < numParticlesPerRow; i++)
		{
			y = ypos + ((i/(float)numParticlesPerRow) * sqsize);
			logoy = m_LogoHeight - (u8)((i/(float)numParticlesPerRow) * m_LogoHeight);

			for(u32 j = 0; j < numParticlesPerRow; j++)
			{
				x = xpos + ((j/(float)numParticlesPerRow) * sqsize);
				logox = m_LogoWidth - (u8)((j/(float)numParticlesPerRow) * m_LogoWidth);


				r = map[(logoy * m_LogoWidth * 3) + (logox*3) + 0];
				g = map[(logoy * m_LogoWidth * 3) + (logox*3) + 1];
				b = map[(logoy * m_LogoWidth * 3) + (logox*3) + 2];

				f32 color = (f32)(r+g+b);
				color /= (255*3.f);

				z = zpos;// - color;

			
				if((u32)(r + g + b) >= colorThreshold)
				{
					dynamic_cast<audParticleEntity*>(m_Entities[c++])->SetLogoPositionAndColor(logoId,j,x,y,z,r,g,b);
				}
			}
		}

		numParticlesPerRow = (u32)floor(sqrt((f32)m_Entities.GetCount() - c));

		y = floorypos;
		for(u32 i = 0; i < numParticlesPerRow; i++)
		{
			x = floorxpos + ((i/(float)numParticlesPerRow) * floorSize);
			for(u32 j = 0; j < numParticlesPerRow; j++)
			{
				z = floorzpos + ((j/(float)numParticlesPerRow) * floorSize);
				dynamic_cast<audParticleEntity*>(m_Entities[c++])->SetLogoPositionAndColor(logoId,0,x,y,z,0,0,0);
			}
		}


	}


	void UpdateClient()
	{
		static float scalar = 1.f, delta = 0.f;
	
		audDriver::GetMixer()->WaitOnThreadCommandBufferProcessing();

		u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();

		Matrix34 camera = GetCameraMatrix();
		// move the mic 1.5m in front of the camera
		Matrix34 mic = GetCameraMatrix();
		Vector3 lookFrom, lookTo, nm;
		mic.GetLookAt(&lookFrom, &lookTo);
		nm = lookTo;
		nm.Subtract(lookFrom);
		nm.Normalize();
		nm.Multiply(Vector3(0.f,0.f,1.5f));
		lookFrom.Add(nm);
		mic.LookAt(lookFrom, lookTo);
		g_AudioEngine.SetVolumeListenerMatrix(mic);
		g_AudioEngine.SetPanningListenerMatrix(mic);

		m_AudioController.PreUpdate(now);
		m_AudioController.Update(now);

		static bool s_RolloffToggle = true;
		if(ioPad::GetPad(0).GetPressedButtons() & ioPad::RDOWN)
		{
			m_DemoEntity.TriggerSound("WREN_BIRD_SONG_1", -10.f);
		}
		else if(ioPad::GetPad(0).GetPressedButtons() & ioPad::RUP)
		{
			m_DemoEntity.TriggerDeadeye(10.f);
		}
		else if(ioPad::GetPad(0).GetPressedButtons() & ioPad::RLEFT)
		{
			m_DemoEntity.TriggerIcecreamVan();
		}
		else if(ioPad::GetPad(0).GetPressedButtons() & ioPad::LDOWN)
		{
			s_RolloffToggle = !s_RolloffToggle;	
			Displayf("Roll off: %s", s_RolloffToggle ? "large" : "small");
		}
		else if(ioPad::GetPad(0).GetPressedButtons() & ioPad::RRIGHT)
		{
			m_DemoEntity.ToggleHeatStress();
		}

		static audSmoother rolloffSmoother;
		static bool s_InitialisedSmoother = false;
		if(!s_InitialisedSmoother)
		{
			rolloffSmoother.Init(0.0025f,0.0025f,0.f,10.f);
			s_InitialisedSmoother = true;
		}

		const f32 rolloff = rolloffSmoother.CalculateValue(s_RolloffToggle ? 3.f : 0.85f, now);
		g_AudioEngine.GetCategoryManager().GetCategoryFromIndex(0)->SetDistanceRollOffScale(rolloff);

		if(m_RunningVirtualisationDemo && ioPad::GetPad(0).GetPressedButtons() & ioPad::START)
		{
			for(int i = 0 ; i < m_Entities.GetCount(); i++)
			{
				((audParticleEntity*)m_Entities[i])->Shutdown();
				((audParticleEntity*)m_Entities[i])->~audParticleEntity();
			}
			m_Entities.Reset();
			m_DemoEvents.Reset();
			m_RunningVirtualisationDemo = false;
		}
		else if(!m_RunningVirtualisationDemo &&  ioPad::GetPad(0).GetPressedButtons() & ioPad::START)
		{
			Matrix34 mat;
			mat.LookAt(Vector3(0.f,20.f,-50.f), Vector3(0.f,0.f,0.f), Vector3(0.f,1.f,0.f));
			m_CamMgr.SetWorldMtx(mat);
			m_CamMgr.SetCamera(dcamCamMgr::CAM_ROAM);
			scalar = 1.f;
			delta = 0.f;

			m_CurrentEventIndex = 0;

			AddDemoEvent(DEMO_EVENT_MOVE_TO_LOGO_0, 0);
			AddDemoEvent(DEMO_EVENT_MOVE_BACK_TO_NORMAL, 10000);
			//AddDemoEvent(DEMO_EVENT_CAM_MODE_TRACK, 25000);
			//AddDemoEvent(DEMO_EVENT_NEXT_TRACK_PAIR, 23000);
			AddDemoEvent(DEMO_EVENT_START_EXPLODING, 30000);
			//AddDemoEvent(DEMO_EVENT_NEXT_TRACK_PAIR, 35000);
			//AddDemoEvent(DEMO_EVENT_CAM_MODE_TRACK, 35000);
			//AddDemoEvent(DEMO_EVENT_NEXT_TRACK_PAIR, 47000);
			//AddDemoEvent(DEMO_EVENT_CAM_MODE_MANUAL, 55000);
			//AddDemoEvent(DEMO_EVENT_MOVE_TO_LOGO_1, 55000);
			//AddDemoEvent(DEMO_EVENT_START_EXPLODING, 60000);
			//AddDemoEvent(DEMO_EVENT_MOVE_BACK_TO_NORMAL, 55000);
			//AddDemoEvent(DEMO_EVENT_MOVE_TO_LOGO_0, 60000);

			AddDemoEvent(DEMO_EVENT_MOVE_BACK_TO_NORMAL, 60000);

			VirtualisationDemo();
		}

		

		if(m_RunningVirtualisationDemo)
		{
			u32 currentEvent = DEMO_EVENT_NONE;

			if(m_CurrentEventIndex < (u32)m_DemoEvents.GetCount() && (now-m_DemoStartTime) >= m_DemoEvents[m_CurrentEventIndex]->time)
			{
				Printf("Demo: %d (%d ms, should have been %d ms)\n", m_DemoEvents[m_CurrentEventIndex]->eventId, m_DemoEvents[m_CurrentEventIndex]->time, now-m_DemoStartTime);
				currentEvent = m_DemoEvents[m_CurrentEventIndex]->eventId;
				m_CurrentEventIndex++;
			}

			switch(currentEvent)
			{
			case DEMO_EVENT_CAM_MODE_TRACK:
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].PositionIndex])->SetShouldDraw(false);
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].TrackIndex])->SetHighlight(true);
				Printf("switching to track cam\n");
				m_CameraMode = CAMERA_MODE_TRACK;
				break;
			case DEMO_EVENT_CAM_MODE_MANUAL:
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].PositionIndex])->SetShouldDraw(true);
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].TrackIndex])->SetHighlight(false);
				Printf("switching to manual cam\n");
				m_CameraMode = CAMERA_MODE_MANUAL;
				break;
			case DEMO_EVENT_NEXT_TRACK_PAIR:
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].PositionIndex])->SetShouldDraw(true);
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].TrackIndex])->SetHighlight(false);
				m_CurrentTrackCamPair++;
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].PositionIndex])->SetShouldDraw(false);
				dynamic_cast<audParticleEntity*>(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].TrackIndex])->SetHighlight(true);
				break;
			}


			// update camera stuff
			switch(m_CameraMode)
			{
			case CAMERA_MODE_MANUAL:
				break;
			case CAMERA_MODE_TRACK:
				camera.Identity();
				camera.LookAt(m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].PositionIndex]->GetPosition(), m_Entities[g_TrackCamPairs[m_CurrentTrackCamPair].TrackIndex]->GetPosition());
				m_CamMgr.SetWorldMtx(camera);
				break;
			}

			for(int i = 0 ; i < m_Entities.GetCount(); i++)
			{
				dynamic_cast<audParticleEntity*>(m_Entities[i])->Update(now);
				dynamic_cast<audParticleEntity*>(m_Entities[i])->SetScalar(scalar);

				if(currentEvent == DEMO_EVENT_MOVE_TO_LOGO_0)
				{
					dynamic_cast<audParticleEntity*>(dynamic_cast<audParticleEntity*>(m_Entities[i]))->MoveToLogoState(0);
				}
				else if(currentEvent == DEMO_EVENT_MOVE_TO_LOGO_1)
				{
					dynamic_cast<audParticleEntity*>(m_Entities[i])->MoveToLogoState(1);
				}
				else if(currentEvent == DEMO_EVENT_MOVE_BACK_TO_NORMAL)
				{
					dynamic_cast<audParticleEntity*>(m_Entities[i])->MoveToNormalState();
				}
			}


			m_MoveToLogo = false;
			m_MoveToNormal = false;

			switch(m_ParticleState)
			{
			case NORMAL_STATE:
				if(currentEvent == DEMO_EVENT_START_EXPLODING)
				{
					Printf("Entering IMPLODING_STATE\n");
					delta = -0.045f;
					m_ParticleState = IMPLODING_STATE;
					// change to small rolloff
					s_RolloffToggle = false;
				}
				break;
			case IMPLODING_STATE:
				if(scalar <= 0.05f)
				{
					delta = 0.05f;
					Printf("Entering EXPLODING_STATE\n");
					m_ParticleState = EXPLODING_STATE;
				}
				break;
			case EXPLODING_STATE:
				if(scalar >= 5.5f)
				{
					m_ParticleState = EXPLODED_STATE;
					delta = 0.f;
					Displayf("Entered exploded state");
				}
				break;
			case EXPLODED_STATE:
				{
					if(currentEvent == DEMO_EVENT_MOVE_BACK_TO_NORMAL)
					{
						delta = -0.01f;
						Printf("Entering POST_EXPLODING_STATE\n");
						m_ParticleState = POST_EXPLODING_STATE;
					}
				}
			case POST_EXPLODING_STATE:
				if(scalar >= 0.99f && scalar <= 1.01f)
				{
					delta = 0.f;
					Printf("Entering NORMAL_STATE\n");
					m_ParticleState = NORMAL_STATE;
				}
				break;
			}

			scalar += delta;
		}
		

		g_AudioEngine.GetEnvironment().CommitListenerSettings(now);

		// Update the CategoryManager, so any settings changed will take effect.
		g_AudioEngine.GetCategoryManager().CommitCategorySettings();

		//g_AudioEngine.SetListenerReverbParams(g_ListenerRoomSize, g_ListenerDamping, g_ListenerWet, g_ListenerDry);

		audDriver::GetMixer()->FlagThreadCommandBufferReadyToProcess();
	}

	void AddWidgetsClient()
	{
#if __BANK
		bkBank &bk = BANKMGR.CreateBank("rage - Audio Entities Sample", 400, 200);


		AddSampleWidgets(bk);
#endif
	}
	

protected:
	// virtalisation demo
	atArray<DemoEvent *> m_DemoEvents;
	bool m_MoveToLogo, m_MoveToNormal;
	u32 m_DemoStartTime;

	u32 m_CameraMode;
	u32 m_CurrentTrackCamPair;
	u32 m_CurrentEventIndex;

	enum ParticleState
	{
		NORMAL_STATE,
		IMPLODING_STATE,
		EXPLODING_STATE,
		EXPLODED_STATE,
		POST_EXPLODING_STATE
	};

	u32 m_ParticleState;

	static const u8 m_LogoWidth = 200, m_LogoHeight = 197;

	audDemoAudioEntity m_DemoEntity;
	
}; // class audSampleVirtualisation

} // namespace ragesamples

using namespace ragesamples;
int Main()
{
	audSampleVirtualisation sample;
	sample.Init();
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}
