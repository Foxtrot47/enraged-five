//
// sample_audio/tracker.h
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_ENTITY_TRACKER_H
#define AUD_ENTITY_TRACKER_H

#include "audioengine/tracker.h"

using namespace rage;

namespace ragesamples
{

// forward declaration of game-specific base entity class
class audSampleGameEntityBase;

// PURPOSE
//  The custom audTracker for this sample - it will live within entities created by the sameple,
//  and provides a method of having a sound automatically track the physical position of a gameworld object.
class audSampleGameEntityTracker : public audTracker
{
public:
	audSampleGameEntityTracker() : m_ParentEntity(NULL)
	{
	}

	virtual ~audSampleGameEntityTracker()
	{
	}

	// PURPOSE
	//  Initializes the tracker. Just stores a pointer to its parent.
	virtual void Init(audSampleGameEntityBase *parentEntity)
	{
		Assert(parentEntity);
		m_ParentEntity = parentEntity;
	}

	// PURPOSE
	//  Returns the parent's Position vector.
	virtual const Vector3 GetPosition() const;

	virtual audCompressedQuat GetOrientation() const { return audCompressedQuat(); }

protected:
	audSampleGameEntityBase *m_ParentEntity;
};

} // namespace ragesamples

#endif // AUD_ENTITY_TRACKER_H
