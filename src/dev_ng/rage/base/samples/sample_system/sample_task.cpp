// 
// /sample_task.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"
#include "system/task.h"
#include "system/timer.h"
#include "system/param.h"
#include "diag/output.h"

// Our example tasks.
#include "lamesum.h"
#include "lamesum2.h"

using namespace rage;

struct test {
	u64 number;
	u64 pad;
};

int Main() 
{
	const int taskCount = 12;
	sysTaskHandle handles[taskCount];
	test *inputs = rage_aligned_new(16) test[taskCount];
	test *outputs = rage_aligned_new(16) test[taskCount];

	// Create all of the tasks
	sysTimer T;
	for (int i=0; i<taskCount; i++) {
		inputs[i].number = __OPTIMIZED? 1000001 : 100001;
		sysTaskParameters p;
		memset(&p, 0, sizeof(p));
		p.Input.Data = &inputs[i];
		p.Input.Size = sizeof(inputs[i]);
		p.Output.Data = &outputs[i];
		p.Output.Size = sizeof(outputs[i]);
		handles[i] = sysTaskManager::Create(TASK_INTERFACE(LameSum),p);
	}

	// Wait for all tasks to complete.
	sysTaskManager::WaitMultiple(taskCount,handles);

	Displayf("Separate Buffer Tasks complete in %f ms",T.GetMsTime());
	for (int i=0; i<taskCount; i++) {
		// A u64 cannot be portably printf'd!
		Displayf("PPU: %" I64FMT "d (%p)",outputs[i].number,&outputs[i].number);
	}

	T.Reset();
	for (int i=0; i<taskCount; i++) {
		inputs[i].number = __OPTIMIZED? 1000002 : 100002;
		sysTaskParameters p;
		memset(&p, 0, sizeof(p));
		p.Input.Data = &inputs[i];
		p.Input.Size = sizeof(inputs[i]);
		p.Output.Data = &inputs[i];
		p.Output.Size = sizeof(inputs[i]);
		handles[i] = sysTaskManager::Create(TASK_INTERFACE(LameSum2),p,__PPU);
	}

	// Wait for all tasks to complete.
	sysTaskManager::WaitMultiple(taskCount,handles);

	Displayf("Input/output Tasks complete in %f ms",T.GetMsTime());
	for (int i=0; i<taskCount; i++) {
		// A u64 cannot be portably printf'd!
		Displayf("PPU: %" I64FMT "d (%p)",inputs[i].number,&inputs[i].number);
	}

	delete[] outputs;
	delete[] inputs;

	Displayf("**** completed ***");

	return 0;
}
