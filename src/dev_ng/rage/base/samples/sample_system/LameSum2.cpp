// 
// sample_system/LameSum2.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "LameSum2.h"
#include "diag/output.h"

using namespace rage;

void LameSum2(sysTaskParameters &p)
{
	Assert(p.Input.Size >= sizeof(u64));
	u64 inputOutput = *(u64*) p.Input.Data;

#if __DEV
	Displayf("input = %" I64FMT "d",inputOutput);
#endif
	u64 total = 0;
	while (inputOutput > 0) {
		total += inputOutput--;
	}
#if __DEV
	Displayf("output = %" I64FMT "d",total);
#endif
	// Displayf("end task");

	*(u64*) p.Input.Data = total;
}

