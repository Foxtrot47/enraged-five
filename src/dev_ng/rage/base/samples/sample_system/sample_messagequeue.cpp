// 
// /sample_messagequeue.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// This sample demonstrates the messagequeue class

#include "system/messagequeue.h"
#include "system/main.h"

using namespace rage;

static sysMessageQueue<u32,16> s_Queue;

DECLARE_THREAD_FUNC(Worker)
{
	while (ptr) {
		Displayf("%s Wait:",(char*)ptr);
		u32 result = s_Queue.Pop();
		if (!result)
			break;
		Displayf("%s Request: %u (sleep %d)",(char*)ptr,result,result*200);
		sysIpcSleep(result * 200);
	}
	Displayf("Quit.");
}


int Main() 
{
	sysIpcCreateThread(Worker,(void*)"Worker 1",16384,PRIO_ABOVE_NORMAL,"Worker 1");
	sysIpcCreateThread(Worker,(void*)"Worker 2",16384,PRIO_ABOVE_NORMAL,"Worker 2");
	sysIpcCreateThread(Worker,(void*)"Worker 3",16384,PRIO_ABOVE_NORMAL,"Worker 3");

	Displayf("Startup (sleep 1000).");
	sysIpcSleep(1000);
	Displayf("Push(1)");
	s_Queue.Push(1);
	Displayf("Push(2)");
	s_Queue.Push(2);
	Displayf("Push(3)");
	s_Queue.Push(3);
	Displayf("Sleep(4000)");
	sysIpcSleep(4000);
	Displayf("Push(4)");
	s_Queue.Push(4);
	sysIpcSleep(2000);
	Displayf("Push(5)");
	s_Queue.Push(5);
	Displayf("Push(Quit)");
	s_Queue.Push(0);
	Displayf("Shutdown.");
	sysIpcSleep(1000);

	return 0;
}
