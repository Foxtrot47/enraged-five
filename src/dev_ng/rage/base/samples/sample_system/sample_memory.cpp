// 
// /sample_memory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:	Validates a memory allocator by exercising it from two threads at once,
//			where each thread allocates memory, fills it with debug values, and then
//			later deallocates the memory after checking it is unchanged.

// #define USE_EXTERNAL_ALLOCATOR	8192
// #define BUDDY_ALLOCATOR_LEAF_SIZE	32
// #define BUDDY_ALLOCATOR_LEAF_COUNT	60000

#define SIMPLE_HEAP_SIZE	(64*1024)

#include "math/random.h"
#include "system/main.h"
#include "system/timer.h"
#include "system/fixedallocator.h"
#include "system/tinyheap.h"

#define TEST_TINY_HEAP 1

using namespace rage;

#if TEST_TINY_HEAP
sysTinyHeap TINYHEAP(sysMemVirtualAllocate(64 * 1024 * 1024),64 * 1024 * 1024);
#endif

struct TestChunk {
	TestChunk(int size,int 
#if !TEST_TINY_HEAP
		align
#endif
		) {
#if TEST_TINY_HEAP
		Data = (int*) TINYHEAP.Allocate((Size = size) * sizeof(int),16,size & 7);
#else
		Data = rage_aligned_new (1<<(align+4)) int[Size = size];
#endif
		for (int i=0; i<size; i++)
			Data[i] = i+size;
	}

	~TestChunk() {
#if __ASSERT
		for (int i=0; i<Size; i++) {
			Assert(Data[i]  == i+Size);
		}
#endif
#if TEST_TINY_HEAP
		TINYHEAP.Free(Data);
#else
		delete[] Data;
#endif
	}
	int *Data;
	int Size;
};

struct Test {
	static const int ChunkCount = 16384;
	TestChunk *Chunks[ChunkCount];

	Test() {
	}

	void DoTest()
	{
		// Set all chunk ptrs to NULL.
		for (int i=0; i<ChunkCount; i++)
			Chunks[i] = NULL;
		mthRandom myrand;

		const int testCount = 1000000;
		// Choose a random chunk, delete it if it exists or fill it if if doesn't.
		for (int i=0; i<testCount; i++) {
			int who = myrand.GetRanged(0,ChunkCount-1);
			if (Chunks[who]) {
				delete Chunks[who];
				Chunks[who] = NULL;
			}
			else
				Chunks[who] = rage_new TestChunk(myrand.GetRanged(0,512),myrand.GetInt()&3);

			if ((i % (testCount / 10)) == 0)
				Displayf("%d%% complete",i / (testCount/100));
		}

		// delete all chunks (should all be either NULL or previously allocated).
		for (int i=0; i<ChunkCount; i++)
			delete Chunks[i];
	}

	static DECLARE_THREAD_FUNC(DoTestWrapper)
	{
		((Test*)ptr)->DoTest();
	}
};


#include "system/fixedallocator.h"

int Main() {
#if TEST_TINY_HEAP
	sysTimer T;
	Test TEST;
	TEST.DoTest();
	Displayf("Iterations complete in %fms",T.GetMsTime());
	return 0;
#else
#if !defined(BUDDY_ALLOCATOR_LEAF_SIZE)
	static size_t sizes[] = {32, 1024};
	static u8 counts[] = {64,128};
	void *fiHeap = rage_new char[sizes[0]*counts[0] + sizes[1]*counts[1]];
	sysMemFixedAllocator FI(fiHeap,2,sizes,counts);
	void *a = FI.Allocate(16,1,0);
	void *b = FI.Allocate(256,1,0);
	void *c = FI.Allocate(1,1,0);
	void *d = FI.Allocate(512,1,0);
	Displayf("%p %p %p %p",a,b,c,d);
	FI.DumpStats();	
	FI.Free(b);
	FI.Free(d);
	b = FI.Allocate(8,1,0);
	d = FI.Allocate(768,1,0);
	Displayf("%p %p %p %p",a,b,c,d);
	FI.DumpStats();	
	FI.Free(a);
	FI.Free(b);
	FI.Free(c);
	FI.Free(d);
	FI.DumpStats();	
	delete [] (char*) fiHeap;
#endif

	// Times reported are the higher of the two threads.
	// Baseline (no spin count), 360 Release:		23404ms
	// With spin count of....			zero:		23726ms
	//									 256:		17343ms
	//									 512:		16446ms
	//									1024:		16395ms
	//									2048:		15267ms
	//									4096:		14821ms
	//									8192:		14597ms
	Displayf("before test %d",sysMemGetMemoryAvailable());
	sysTimer T;
	{
		Test TEST, TEST2;
		sysIpcThreadId test1 = sysIpcCreateThread(Test::DoTestWrapper,&TEST,65536,PRIO_NORMAL,"Test::DoTestWrapper",true,__XENON);
		sysIpcThreadId test2 = sysIpcCreateThread(Test::DoTestWrapper,&TEST2,65536,PRIO_NORMAL,"Test::DoTestWrapper",true,1+__XENON);
		sysIpcWaitThreadExit(test1);
		sysIpcWaitThreadExit(test2);
		sysMemAllocator::GetCurrent().SanityCheck();

		Displayf("Iterations complete in %fms",T.GetMsTime());
	}
	Displayf("after test %d",sysMemGetMemoryAvailable());
	return 0;
#endif
}
