// 
// /sample_timer.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// This sample verifies that the system timer is strictly increasing.

#include "system/main.h"
#include "system/timer.h"
#include "diag/output.h"

using namespace rage;

int Main() {
	for (;;) {
		utimer_t a = sysTimer::GetTicks();
		utimer_t b = sysTimer::GetTicks();
		if (a > b) {
			Displayf("%" I64FMT "x > " "%" I64FMT "x...",a,b);
			break;
		}
	}
	return 0;
}
