// 
// sample_system/stackdecoder.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Cut-and-paste hack job from system/stack.cpp
#include "system/main.h"
#include "system/param.h"
#include "file/stream.h"

using namespace rage;

struct Symbol {
	u32 Addr;
	char *Name;

	static int compare(const void *a,const void *b) {
		u32 addrA = ((Symbol*)a)->Addr;
		u32 addrB = ((Symbol*)b)->Addr;
		if (addrA > addrB)
			return 1;
		else if (addrA < addrB)
			return -1;
		else
			return 0;	// this shouldn't happen
	}
};

static Symbol *s_Symbols;
static char *s_SymbolNameHeap;
static int s_SymbolCount;

int Main() 
{
	if (sysParam::GetArgCount() < 2) {
		Displayf("Usage: stackdecoder map-file-name");
	}
	const char *mapFile = sysParam::GetArg(1);

	fiStream *S = fiStream::Open(mapFile);
	char buffer[2048];
	if (S) {
		Displayf("[traceback] Parsing symbols from [%s]",mapFile);
		for (int pass=1; pass<=2; pass++) {
			S->Seek(0);
			int symCount = 0;
			int heapSize = 0;
			bool seen_publics = false;
			char symname[128];
			while (fgets(buffer,sizeof(buffer),S)) {
				if (!seen_publics) {
					if (strstr(buffer,"Publics by Value"))
						seen_publics = true;
					continue;
				}
				// _0001:00000000       __setargv                  00401000 f   setargv.obj
				if (buffer[1] == '0' && buffer[5] == ':' && buffer[15] == ' ') {
					char *start = buffer+21;
					char *stop = start;
					while (*stop && *stop != 32)
						++stop;
					*stop = 0;
					while (stop[1]==32)
						++stop;
					u32 addr = strtoul(stop+1,NULL,16);
					safecpy(symname,start,sizeof(symname));
					if (pass == 2 && s_Symbols) {
						s_Symbols[symCount].Addr = addr;
						strcpy(s_Symbols[symCount].Name = s_SymbolNameHeap + heapSize,symname);
					}
					++symCount;
					heapSize += (int) strlen(symname) + 1;
				}
			}
			if (pass == 1) {
				// We don't use our memory manager here, although we probably could
				// because we call this before enabling the traceback support.
				s_SymbolCount = symCount;
				int size = sizeof(Symbol) * symCount + heapSize;
				Displayf("[traceback] Allocating space for %d symbols, heap %d, total %d",symCount,heapSize,size);
				s_Symbols = (Symbol*) (rage_new char[size]);
				s_SymbolNameHeap = (char*)s_Symbols + sizeof(Symbol) * symCount;
				if (!s_Symbols)
					Errorf("[traceback] Not enough space to allocate symbol heap.");
			}
			else {
				if (s_Symbols)
					qsort(s_Symbols,s_SymbolCount,sizeof(Symbol),Symbol::compare);
			}
		}
		S->Close();

		Displayf("Paste your backtrace in now.");
		Displayf("Use Ctrl-C to abort.");
		for (;;) {
			gets(buffer);
			if (buffer[0] != '8')
				continue;
			u32 lookupAddr = strtoul(buffer,NULL,16);
			int low = 0, high = s_SymbolCount - 2;
			while (low<high) {
				int mid = (low + high) >> 1;
				// Displayf("[%d,%d,%d] lookup=%p current=%p",low,mid,high,lookupAddr,s_Symbols[mid].Addr);
				if (lookupAddr >= s_Symbols[mid].Addr && lookupAddr < s_Symbols[mid+1].Addr) {
					Displayf("%s+%x",s_Symbols[mid].Name,lookupAddr - s_Symbols[mid].Addr);
					break;
				}
				else if (lookupAddr > s_Symbols[mid].Addr)
					low = mid+1;
				else
					high = mid;
			}
		}
	}
	return 0;
}
