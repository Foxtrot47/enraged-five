// 
// /sample_thread.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "system/ipc.h"
#include "system/timer.h"
#include "system/main.h"

// TITLE: Semaphores
// PURPOSE:
//		This sample shows how to create semaphores and use them to communicate between threads

using namespace rage;

sysIpcSema s_Sema;

// STEP #1. Declare our thread function

//-- We declare the function that will be run in our separate thread.
DECLARE_THREAD_FUNC(foobar) {
	Displayf("foobar: Polling on s_Sema... %p",ptr);
	if (sysIpcPollSema(s_Sema)) {
		Displayf("foobar: got it!");
	}
	else {
		Displayf("foobar: Poll failed, blocking now");
		sysIpcWaitSema(s_Sema);
	}

	Displayf("foobar: have the sema now, sleeping for a bit");
	sysIpcSleep(5000);

	Displayf("foobar: releasing sema");
	sysIpcSignalSema(s_Sema);

	Displayf("foobar: done");
}
// -STOP


// main application
int Main()
{
	s_Sema = sysIpcCreateSema(false);

	// STEP #2. Create our thread

	//-- We create the thread.  This thread runs the function <c>foobar(void* ptr)</c>, with a stack size of 4 KB, and the thread is suspended (not started.)
	sysIpcCreateThread(foobar,NULL,4096,PRIO_NORMAL,"foobar",true);
	// -STOP

	sysTimer T;

	Displayf("main: created thread, waiting to release sema");
	sysIpcSleep(3000);

	Displayf("main: signalling shared sema (%f s)",T.GetTime());
	sysIpcSignalSema(s_Sema);

	Displayf("main: waiting 1000ms to get it back (%f s)",T.GetTime());
	if (!sysIpcWaitSemaTimed(s_Sema,1000))
		Displayf("main: timed out after 1000 ms (this is expected) (%f s)",T.GetTime());
	else
		Errorf("main: semaphore didn't timeout like expected (%f s)",T.GetTime());

	Displayf("main: waiting 8000ms to get it back (%f s)",T.GetTime());
	if (!sysIpcWaitSemaTimed(s_Sema,8000))
		Errorf("main: timed out after 1000 ms (this is NOT expected) (%f s)",T.GetTime());
	else
		Displayf("main: got semaphore as expected (%f s)",T.GetTime());

	Displayf("main: done (%f s)",T.GetTime());

	Displayf("now see how many semaphores we can make:");
	int count = 1;
	while (sysIpcCreateSema(false))
		++count;
	Displayf("created %d semaphores total",count);

	return 0;
}
