// 
// /sample_memtest.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE	(460*1024)
#define SIMPLE_PHYSICAL_SIZE (1 * 1024)

#include "system/main.h"
#include "system/timer.h"
#include "diag/output.h"

using namespace rage;

#define START_TEST(x) do { Displayf("Starting: %s", #x); sysTimer T; 
#define END_TEST(x) Displayf("Ending: %s. took %fms", #x, T.GetMsTime()); } while(false);

void CheckMemory(u32* testBlock, int blockSize, u32 testVal)
{
	u32* blockEnd = testBlock + (blockSize / sizeof(u32));
	while(testBlock != blockEnd)
	{
		if (*testBlock != testVal)
		{
			Errorf("Block at %p should be %x, is actually %x", testBlock, testVal, *testBlock);
		}
		++testBlock;
	}
}

void charMemSet(u8* testBlock, u8 newVal, size_t blockSize)
{
	u8* end = testBlock + blockSize;
	while(testBlock != end)
	{
		*testBlock = newVal;
		testBlock++;
	}
}

int Main()
{
	size_t testBlockSize = (SIMPLE_HEAP_SIZE - 1024) * 1024; // minus 1mb for overhead

	Displayf("Allocating %" SIZETFMT "dM block", testBlockSize / (1024*1024));

	char* testBlock = rage_new char[testBlockSize]; // tbs is in bytes

	for (int i = 0; i < 100; i++)
	{
		START_TEST(sysMemSet_0x0);
		sysMemSet(testBlock, 0x0, testBlockSize);
		END_TEST(sysMemSet_0x0);

		START_TEST(checkZero);
		CheckMemory((u32*)testBlock, testBlockSize, 0x0);
		END_TEST(checkZero);

		START_TEST(sysMemSet_0xFF);
		sysMemSet(testBlock, 0xFF, testBlockSize);
		END_TEST(sysMemSet_0xFF);

		START_TEST(checkFF);
		CheckMemory((u32*)testBlock, testBlockSize, 0xFFFFFFFF);
		END_TEST(checkFF);

		START_TEST(charMemSet_0x0);
		charMemSet((u8*)testBlock, 0x0, testBlockSize);
		CheckMemory((u32*)testBlock, testBlockSize, 0x0);
		END_TEST(oldMemSet_0x0);

		START_TEST(charMemSet_0xFF);
		charMemSet((u8*)testBlock, 0xFF, testBlockSize);
		CheckMemory((u32*)testBlock, testBlockSize, 0xFFFFFFFF);
		END_TEST(oldMemSet_0xFF);
	}

	delete [] testBlock;

	return 0;
}

