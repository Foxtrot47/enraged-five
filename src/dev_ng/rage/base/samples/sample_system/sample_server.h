#ifndef SAMPLE_SERVER_H
#define SAMPLE_SERVER_H

#include "system/taskserver.h"

using namespace rage;

void LameSum(sysTaskHeader &hdr)
{
	Assert(hdr.InputBufferSize == sizeof(u64));
	Assert(hdr.OutputBufferSize == sizeof(u64));
	u64 input = *(u64*) hdr.InputBuffer;
	u64 &output = *(u64*) hdr.OutputBuffer;

	// Displayf("begin task");
	u64 total = 0;
	while (input > 0) {
		total += input--;
	}
	// Displayf("end task");

	output = total;
}




#endif	// SAMPLE_SERVER_H
