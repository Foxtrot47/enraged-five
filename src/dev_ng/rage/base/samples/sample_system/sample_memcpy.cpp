//--------------------------------------------------------------------------------------
// Based on FastMemCopy.cpp
//
// This sample shows how to use the various memory copy APIs, and how to measure their 
// performance.
//
// Original copywrite notice:
//
// XNA Developer Connection
// Copyright (C) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

#include "math/amath.h"
#include "system/cache.h"
#include "system/nelem.h"
#include "system/main.h"
#include "system/memops.h"
#include "system/timer.h"

#include <cstring>
#include <cstdio>

#if __XENON
#include "system/xtl.h"
#endif

using namespace rage;

//--------------------------------------------------------------------------------------
// Variables which we can change at runtime using the controller:
//--------------------------------------------------------------------------------------

// What routine are we testing:
const char*         g_strMemCopyRoutineNames[] =
{
	"memcpy",
	"sysMemCpy",
	"sysMemCpy128",
	"sysMemCpyStreaming",
	"memset",
	"sysMemSet",
	"sysMemSet128",
};
enum MemCopyRoutine
{
	MEM_COPY_ROUTINE_MEMCPY,
	MEM_COPY_ROUTINE_SYSMEMCPY,
	MEM_COPY_ROUTINE_SYSMEMCPY128,
	MEM_COPY_ROUTINE_SYSMEMCPYSTREAMING,
	MEM_COPY_ROUTINE_MEMSET,
	MEM_COPY_ROUTINE_SYSMEMSET,
	MEM_COPY_ROUTINE_SYSMEMSET128,

	MEM_COPY_ROUTINE_MAX,
};
static size_t        g_dwMemCopyRoutine = MEM_COPY_ROUTINE_MEMCPY;

// Is the destination in cacheable or write-combined memory:
static int         g_bDestCacheable = 1;

// What is the relative alignment between src and dst?
// If the following value is zero, then src and dst use the same sets of cache lines.  
// We guarantee this by allocating with 128 Kb alignment, since L2 cache address equals 
// virtual address modulo 128 Kb.
// Accesses to the same 'set' of the cache are serialized (i.e. accesses to addresses 
// separated by a multiple of 128 Kb).
const size_t g_dwDefaultRelativeAlignments[] =
{
	0,
	1,      // misaligned buffers are very slow for memcpy
	2,
	4,
	8,
	16,
	128,    // one L1/L2 cache line
	1024,
	1024 + 128,
	8192,   // size of one 'way' of the L1 cache
	8192 + 128,
	65536,  // last must be largest
};
static size_t        g_dwRelativeAlignmentIndex = 0;

// Is the data in the cache (if true, src is prefetched and dst is pre-zeroed; if false,
// src and dst are purged):
static int         g_bCacheWarm = 0;

// What size buffer are we copying/setting:
const size_t g_dwDefaultBufferSizes[] =
{
	128,
	4 * 1024,
	64 * 1024,
	4 * 1024 * 1024, // last must be largest
};
const size_t         g_dwLargestBufferSize = g_dwDefaultBufferSizes[NELEM( g_dwDefaultBufferSizes ) - 1];
static size_t        g_dwBufferSizeIndex = 0;

// If memset, is the value being set zero or non-zero:
static unsigned char         g_cMemSetValue = 0;


//--------------------------------------------------------------------------------------
// Named constant values:
//--------------------------------------------------------------------------------------

// Include some padding to test relative alignments.  We allocate so that src and dst 
// will have a cache collision with each other by default.
const size_t         g_dwAllocationPadding = g_dwDefaultRelativeAlignments[NELEM( g_dwDefaultRelativeAlignments ) -
1];

// The size of the allocations for the src/dst data:
const size_t         g_dwAllocationSize = g_dwLargestBufferSize + g_dwAllocationPadding;

// Stats about the L2 cache:
const size_t         g_dwCacheLineSize = 128;
const size_t         g_dwL2CacheSize = __PS3 ? 512 * 1025 : 1024 * 1024;
#if __XENON
const size_t         g_dwL2CacheWays = 8;
#endif

// Units:
#ifndef CONST_FREQ
const size_t         g_dwTicksPerSecond = sysTimer::GetTicksToSeconds();
#else
const size_t         g_dwTicksPerSecond = (size_t)CONST_FREQ;
#endif
const size_t         g_dwBytesPerGbyte = 1024 * 1024 * 1024;

// How many times we repeat the test to measure avg performance:
const size_t         g_dwNumRepetitions = 512;


//--------------------------------------------------------------------------------------
// Non-constant globals:
//--------------------------------------------------------------------------------------

static bool         g_bCheckCorrectness = true;
static char*        g_pCacheableSrc = NULL;
static char*        g_pCacheableDst = NULL;
static char*        g_pWriteCombinedDst = NULL;


//--------------------------------------------------------------------------------------
// Helper functions:
//--------------------------------------------------------------------------------------

template <typename t_type> static inline bool AlignedToPowerOf2( const t_type& t, size_t dwPowerOf2 )
{
	return ( ( ( size_t )t ) & ( dwPowerOf2 - 1 ) ) == 0;
}

template <typename t_type> static inline t_type RoundDownToPowerOf2( const t_type& t, size_t dwPowerOf2 )
{
	return ( t_type )( ( ( size_t )t ) & ~( dwPowerOf2 - 1 ) );
}

template <typename t_type> static inline t_type RoundUpToPowerOf2( const t_type& t, size_t dwPowerOf2 )
{
	return ( t_type )( ( ( ( size_t )t ) + ( dwPowerOf2 - 1 ) ) & ~( dwPowerOf2 - 1 ) );
}

static double GBPerSec( u64 ticks, size_t count )
{
	double sec = ticks / ( double )g_dwTicksPerSecond;
	return ( count / ( double )g_dwBytesPerGbyte ) / sec;
}


//--------------------------------------------------------------------------------------
// Cache modification functions:
//--------------------------------------------------------------------------------------

// Kick data out of the cache
static void Purge( const char* pData, size_t dwSize )
{
	// Expand input range to be cache-line aligned:
	const char* pEnd = pData + dwSize;
	pData = RoundDownToPowerOf2( pData, g_dwCacheLineSize );
	dwSize = RoundUpToPowerOf2( pEnd, g_dwCacheLineSize ) - pData;

	for( size_t i = 0; i < dwSize; i += g_dwCacheLineSize )
	{
		FreeDC( pData, i );
	}
}

// Kick data into the cache.  
static void Prefetch( const char* pData, size_t dwSize )
{
	// Expand input range to be cache-line aligned:
	const char* pEnd = pData + dwSize;
	pData = RoundDownToPowerOf2( pData, g_dwCacheLineSize );
	dwSize = RoundUpToPowerOf2( pEnd, g_dwCacheLineSize ) - pData;

	for( size_t i = 0; i < dwSize; i += g_dwCacheLineSize )
	{
		PrefetchDC2( pData, i );
	}
}

// Initialize the cache to zero.  
static void PreZero( char* pData, size_t dwSize )
{
	// For correctness, the inputs cannot share cache lines with other data.
	Assert( AlignedToPowerOf2( pData, g_dwCacheLineSize ) );
	Assert( AlignedToPowerOf2( dwSize, g_dwCacheLineSize ) );

	for( size_t i = 0; i < dwSize; i += g_dwCacheLineSize )
	{
		ZeroDC( pData, i );
	}
}


//--------------------------------------------------------------------------------------
// Name: DoTests
// Desc: Perform one of the memory tests using current settings
//--------------------------------------------------------------------------------------
size_t DoTests( bool bCheckCorrectness )
{
	bool bAllowed = true;   // Check if the operation meets constraints, or we'll crash 

	if (g_cMemSetValue != 0 && 
		(g_dwMemCopyRoutine == MEM_COPY_ROUTINE_MEMCPY || 
		 g_dwMemCopyRoutine == MEM_COPY_ROUTINE_SYSMEMCPY ||
		 g_dwMemCopyRoutine == MEM_COPY_ROUTINE_SYSMEMCPY128 ||
		 g_dwMemCopyRoutine == MEM_COPY_ROUTINE_SYSMEMCPYSTREAMING))
	{
		return 0;
	}

	if (!g_bDestCacheable && !g_pWriteCombinedDst)
	{
		return 0;
	}

	char* pSrc = g_pCacheableSrc;
	char* pDst = ( g_bDestCacheable ? g_pCacheableDst : g_pWriteCombinedDst )
		+ g_dwDefaultRelativeAlignments[g_dwRelativeAlignmentIndex];

	if( bCheckCorrectness )
	{
		// Clear Dst to something other than the correct result
		for( size_t i = 0; i < g_dwDefaultBufferSizes[g_dwBufferSizeIndex]; ++i )
		{
			pDst[i] = g_cMemSetValue + 1;
		}
	}

	// Even though we never read from pDst, having it in the cache makes a big 
	// difference for memcpy and memset.  The sysMem functions internally zero out pDst 
	// cache lines with __dcbz128.
	if( g_bCacheWarm )
	{
		// Use at most half the cache for src and for dst
		Prefetch( pSrc, Min( g_dwDefaultBufferSizes[g_dwBufferSizeIndex], g_dwL2CacheSize / 2 ) );
		if( g_bDestCacheable ) PreZero( pDst, Min( g_dwDefaultBufferSizes[g_dwBufferSizeIndex],
			g_dwL2CacheSize / 2 ) );
	}
	else
	{
		Purge( pSrc, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		if( g_bDestCacheable ) Purge( pDst, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
	}

	// We incur some measurement overhead from the switch statement, function call etc.
	// Use a size_t to avoid rare errors with bit 32 of __mftb. 32-bits of precision
	// is more than enough.
	utimer_t start = sysTimer::GetTicks();

	switch( g_dwMemCopyRoutine )
	{
	case MEM_COPY_ROUTINE_MEMCPY:
		// No requirements on alignment, or cache-ability.
		memcpy( pDst, pSrc, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		break;

	case MEM_COPY_ROUTINE_SYSMEMCPY:
		// Dst must be cacheable.
		if( g_bDestCacheable )
			sysMemCpy( pDst, pSrc, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		else
			bAllowed = false;
		break;

	case MEM_COPY_ROUTINE_SYSMEMCPY128:
		// Dst must be cacheable; Src must be 16-byte aligned; Dst and Size must be 
		// cache-line aligned.
		if( g_bDestCacheable && AlignedToPowerOf2( pSrc, 16 )
			&& AlignedToPowerOf2( pDst, g_dwCacheLineSize )
			&& AlignedToPowerOf2( g_dwDefaultBufferSizes[g_dwBufferSizeIndex], g_dwCacheLineSize ) )
			sysMemCpy128( pDst, pSrc, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		else
			bAllowed = false;
		break;

	case MEM_COPY_ROUTINE_SYSMEMCPYSTREAMING:
		// No requirements on alignment, or cache-ability.
		sysMemCpyStreaming( pDst, pSrc, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		break;

	case MEM_COPY_ROUTINE_MEMSET:
		// No requirements on alignment, or cache-ability.
		memset( pDst, g_cMemSetValue, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		break;

	case MEM_COPY_ROUTINE_SYSMEMSET:
		// Dst must be cacheable.
		if( g_bDestCacheable )
			sysMemSet( pDst, g_cMemSetValue, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		else
			bAllowed = false;
		break;

	case MEM_COPY_ROUTINE_SYSMEMSET128:
		// Dst must be cacheable; Src must be 16-byte aligned; Size must be 
		// cache-line aligned.
		if( g_bDestCacheable && AlignedToPowerOf2( pDst, g_dwCacheLineSize )
			&& AlignedToPowerOf2( g_dwDefaultBufferSizes[g_dwBufferSizeIndex], g_dwCacheLineSize ) )
			sysMemSet128( pDst, g_cMemSetValue, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] );
		else
			bAllowed = false;
		break;

	default:
		Assert( false );    // unreachable
		bAllowed = false;
	}

	utimer_t lDuration = ( bAllowed ) ? ( ( sysTimer::GetTicks() ) - start ) : 0; // 0 signals invalid

	// Check correctness
	if( bAllowed && bCheckCorrectness )
	{
		switch( g_dwMemCopyRoutine )
		{
		case MEM_COPY_ROUTINE_MEMCPY:
		case MEM_COPY_ROUTINE_SYSMEMCPY:
		case MEM_COPY_ROUTINE_SYSMEMCPY128:
		case MEM_COPY_ROUTINE_SYSMEMCPYSTREAMING:
			for( size_t i = 0; i < g_dwDefaultBufferSizes[g_dwBufferSizeIndex]; ++i )
			{
				Assert( pDst[i] == pSrc[i] );
				if ( pDst[i] != pSrc[i] )
					printf("ERROR\n");
			}
			break;

		case MEM_COPY_ROUTINE_MEMSET:
		case MEM_COPY_ROUTINE_SYSMEMSET:
		case MEM_COPY_ROUTINE_SYSMEMSET128:
			for( size_t i = 0; i < g_dwDefaultBufferSizes[g_dwBufferSizeIndex]; ++i )
			{
				Assert( pDst[i] == g_cMemSetValue );
				if ( pDst[i] != g_cMemSetValue )
					printf("ERROR\n");
			}
			break;
		}
	}

	return lDuration;
}


//--------------------------------------------------------------------------------------
// Name: main
// Desc: Entry point to the program
//--------------------------------------------------------------------------------------
int Main() 
{
	Assert( NELEM( g_strMemCopyRoutineNames ) == MEM_COPY_ROUTINE_MAX );

	// Allocate the buffers.  We use an alignment of 128 Kb, so that src and dst always
	// map to the same cache lines, unless we copy with an offset.
#if __XENON
	g_pCacheableSrc = ( char* )XPhysicalAlloc( g_dwAllocationSize, MAXULONG_PTR,
		g_dwL2CacheSize / g_dwL2CacheWays, MEM_LARGE_PAGES | PAGE_READWRITE );
	g_pCacheableDst = ( char* )XPhysicalAlloc( g_dwAllocationSize, MAXULONG_PTR,
		g_dwL2CacheSize / g_dwL2CacheWays, MEM_LARGE_PAGES | PAGE_READWRITE );
	g_pWriteCombinedDst = ( char* )XPhysicalAlloc( g_dwAllocationSize, MAXULONG_PTR,
		g_dwL2CacheSize / g_dwL2CacheWays, MEM_LARGE_PAGES | PAGE_READWRITE | PAGE_WRITECOMBINE );
#else
	g_pCacheableSrc = ( char* )memalign(128*1024, g_dwAllocationSize);
	char* pCacheableDstStartFence = ( char* )memalign(128*1024, g_dwAllocationSize + 256*1024);
	g_pCacheableDst = pCacheableDstStartFence + 128*1024;
#endif

	// Initialize the source buffer with some distinctive data.
	for( size_t i = 0; i < g_dwAllocationSize; ++i )
	{
		g_pCacheableSrc[i] = ( char )rand();
	}

	// Print results & instructions to console
	printf( "%20s%20s%20s%20s%20s%20s%20s\n", "Copy routine", "Destination", "Relative alignment", "Cache", "Buffer size", "MemSet value", "Test results (Gb/sec)");

	//for (g_bDestCacheable = 0; g_bDestCacheable < 2; g_bDestCacheable++)
	{
		for (g_cMemSetValue = 0; g_cMemSetValue < 2; g_cMemSetValue++)
		{
			//for (g_bCacheWarm = 0; g_bCacheWarm < 2; g_bCacheWarm++)
			{
				for (g_dwBufferSizeIndex = 0; g_dwBufferSizeIndex < NELEM( g_dwDefaultBufferSizes ); g_dwBufferSizeIndex++)
				{
					for (g_dwRelativeAlignmentIndex = 0; g_dwRelativeAlignmentIndex < NELEM( g_dwDefaultRelativeAlignments ); g_dwRelativeAlignmentIndex++)
					{
						for (g_dwMemCopyRoutine = 0; g_dwMemCopyRoutine < MEM_COPY_ROUTINE_MAX; g_dwMemCopyRoutine++)
						{

#if !__XENON
							char* pCacheableDstEndFence = g_pCacheableDst + g_dwDefaultBufferSizes[g_dwBufferSizeIndex] + g_dwDefaultRelativeAlignments[g_dwRelativeAlignmentIndex];

							if( g_bCheckCorrectness )
							{
								// Initialize the dest buffer with some fenceposts
								for (int i = 0; i < 128*1024 + g_dwDefaultRelativeAlignments[g_dwRelativeAlignmentIndex]; i++)
									pCacheableDstStartFence[i] = i + 17;
								for (int i = 0; i < 128*1024; i++)
									pCacheableDstEndFence[i] = i + 17;
							}
#endif

							// Check duration.  Use __int64 to support a large number of repetitions.
							u64 lDuration = 0;
							for( size_t i = 0; i < g_dwNumRepetitions; ++i )
							{
								lDuration += DoTests( false );
							}

#if !__XENON
							if( g_bCheckCorrectness )
							{
								// Check the fenceposts
								for (int i = 0; i < 128*1024 + g_dwDefaultRelativeAlignments[g_dwRelativeAlignmentIndex]; i++)
								{
									Assertf(pCacheableDstStartFence[i] == (char)(i + 17), "%d %d %d", i, (char)(i + 17), pCacheableDstStartFence[i]);
									if (pCacheableDstStartFence[i] != (char)(i + 17))
										printf("ERROR %d %d %d!\n", i, (char)(i + 17), pCacheableDstStartFence[i]);
								}

								for (int i = 0; i < 128*1024; i++)
								{
									Assertf(pCacheableDstEndFence[i] == (char)(i + 17), "%d %d %d", i, (char)(i + 17), pCacheableDstEndFence[i]);
									if (pCacheableDstEndFence[i] != (char)(i + 17))
										printf("ERROR %d %d %d!\n", i, (char)(i + 17), pCacheableDstStartFence[i]);
								}
							}
#endif

							if( g_bCheckCorrectness )
							{
								DoTests( true );    // Only do this once, as it is slow.
							}
							if (lDuration > 0)
							{
								printf( "%20s%20s%20" SIZETFMT "d%20s%20" SIZETFMT "d%20s%20.4f\n", 
									g_strMemCopyRoutineNames[g_dwMemCopyRoutine],
									g_bDestCacheable ? "cacheable" : "write-combined",
									g_dwDefaultRelativeAlignments[g_dwRelativeAlignmentIndex],
									g_bCacheWarm ? "warm" : "cold",
									g_dwDefaultBufferSizes[g_dwBufferSizeIndex],
									g_cMemSetValue ? "non-zero" : "zero",
									( float )GBPerSec( lDuration, g_dwDefaultBufferSizes[g_dwBufferSizeIndex] * g_dwNumRepetitions ));
							}
						}
					}
				}
			}
		}
	}

#if __XENON
	XPhysicalFree( g_pCacheableSrc );
	XPhysicalFree( g_pCacheableDst );
	XPhysicalFree( g_pWriteCombinedDst );
#else
	delete g_pCacheableSrc;
	delete pCacheableDstStartFence;
#endif

	return 0;
}
