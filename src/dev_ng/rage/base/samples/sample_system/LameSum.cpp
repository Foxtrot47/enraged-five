// 
// sample_system/LameSum.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "LameSum.h"
#include "diag/output.h"

using namespace rage;

void LameSum(sysTaskParameters &p)
{
	Assert(p.Input.Size >= sizeof(u64));
	Assert(p.Output.Size >= sizeof(u64));
	u64 input = *(u64*) p.Input.Data;
	u64 &output = *(u64*) p.Output.Data;

#if __DEV
	Displayf("input = %" I64FMT "d",input);
#endif
	u64 total = 0;
	while (input > 0) {
		total += input--;
	}
#if __DEV
	Displayf("output = %" I64FMT "d",total);
#endif
	// Displayf("end task");

	output = total;
}

