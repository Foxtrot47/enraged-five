// 
// sample_system/sample_defrag.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:	Demonstrates memory defragmentation including pointer fixup.

#include "math/random.h"
#include "system/main.h"
#include "system/timer.h"
#include "system/buddyheap.h"


using namespace rage;

/*
	For real production use, every node needs a backpointer to the toplevel object; that allows us
	to reinvoke the rsc ctor on the entire object, using a nonzero fixup only for the leaf that is
	being moved.  Unfortunately this does mean we probably have to invoke the rsc ctor several times
	per defragment since we have no idea which nodes below to which resources.

	This sample just demonstrates the simple case of test objects containing their own addresses,
	and a means to update them during defragmentation.
*/
class TestDefrag
{
public:
	struct Object
	{
		Object() : ptr(NULL), size(0) { }
		~Object() { }

		bool Init(size_t _size,TestDefrag &parent)
		{
			ptr = (size_t*) parent.New((size = _size) * sizeof(size_t));
			if (!ptr)
				return false;

			for (size_t i=0; i<_size; i++)
				ptr[i] = (size_t) &ptr[i];

			Validate();

			return true;
		}

		void Validate()
		{
#if __ASSERT
			for (size_t i=0; i<size; i++)
				Assert(ptr[i] == (size_t)&ptr[i]);
#endif
		}

		void Shutdown(TestDefrag &parent)
		{
			Validate();

			parent.Delete(ptr);

			ptr = NULL;
			size = 0;
		}

		void Fixup(intptr_t delta)
		{
			Assert(ptr);
			ptr = (size_t*)((intptr_t)ptr + delta);
			for (size_t i=0; i<size; i++)
				ptr[i] += delta;
		}

		size_t *ptr;
		size_t size;
	};

	TestDefrag(size_t leafCount, size_t leafSize, size_t objectCount)
	{
		m_Workspace = rage_new u8[COMPUTE_BUDDYHEAP_WORKSPACE_SIZE(leafCount)];
		m_Heap.Init(leafCount, m_Workspace);
		m_HeapBase = rage_new char[leafCount * leafSize];
		m_LeafCount = leafCount;
		m_LeafSize = leafSize;
		m_SelfIndex = rage_new u16[leafCount];
		m_ObjectCount = objectCount;
		m_Objects = rage_new Object[objectCount];
		m_AllocCounter = 0;
	}

	void TestReclaim()
	{
		sysMemDistribution dist, dist1, dist2;
		m_Heap.GetMemoryDistribution(dist);

		Displayf("%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d",
			dist.FreeBySize[0],dist.FreeBySize[1],
			dist.FreeBySize[2],dist.FreeBySize[3],
			dist.FreeBySize[4],dist.FreeBySize[5],
			dist.FreeBySize[6],dist.FreeBySize[7],
			dist.FreeBySize[8],dist.FreeBySize[9],
			dist.FreeBySize[10],dist.FreeBySize[11],
			dist.FreeBySize[12],dist.FreeBySize[13],
			dist.FreeBySize[14],dist.FreeBySize[15]);

		sysBuddyNodeIdx a = m_Heap.Allocate(3);
		sysBuddyNodeIdx b = m_Heap.Allocate(8);
		sysBuddyNodeIdx c = m_Heap.Allocate(101);
		sysBuddyNodeIdx d = m_Heap.Allocate(23);
		sysBuddyNodeIdx e = m_Heap.Allocate(43);

		m_Heap.GetMemoryDistribution(dist1);

		Displayf("%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d",
			dist1.FreeBySize[0],dist1.FreeBySize[1],
			dist1.FreeBySize[2],dist1.FreeBySize[3],
			dist1.FreeBySize[4],dist1.FreeBySize[5],
			dist1.FreeBySize[6],dist1.FreeBySize[7],
			dist1.FreeBySize[8],dist1.FreeBySize[9],
			dist1.FreeBySize[10],dist1.FreeBySize[11],
			dist1.FreeBySize[12],dist1.FreeBySize[13],
			dist1.FreeBySize[14],dist1.FreeBySize[15]);

		m_Heap.Free(c);
		m_Heap.Free(a);
		m_Heap.Free(d);
		m_Heap.Free(b);
		m_Heap.Free(e);

		m_Heap.GetMemoryDistribution(dist2);

		Displayf("%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d%4d",
			dist2.FreeBySize[0],dist2.FreeBySize[1],
			dist2.FreeBySize[2],dist2.FreeBySize[3],
			dist2.FreeBySize[4],dist2.FreeBySize[5],
			dist2.FreeBySize[6],dist2.FreeBySize[7],
			dist2.FreeBySize[8],dist2.FreeBySize[9],
			dist2.FreeBySize[10],dist2.FreeBySize[11],
			dist2.FreeBySize[12],dist2.FreeBySize[13],
			dist2.FreeBySize[14],dist2.FreeBySize[15]);

		Assert(memcmp(&dist,&dist1,sizeof(dist)) != 0);
		Assert(memcmp(&dist,&dist2,sizeof(dist)) == 0);
	}

	void *New(size_t size)
	{
		sysBuddyNodeIdx result = m_Heap.Allocate((size + m_LeafSize - 1) / m_LeafSize);
		if (result != sysBuddyHeap::c_NONE)
		{
			m_Heap.SetIsResource(result,true);
			// Mark the node as defragmentable, except for a few early nodes to give us something to test
			++m_AllocCounter;
			if ((m_AllocCounter % 7) || m_AllocCounter > 100)
				m_Heap.UnlockBlock(result, 1);
			m_SelfIndex[result] = (u16) m_Current;
			return m_HeapBase + m_LeafSize * result;
		}
		else
			return NULL;
	}

	void Delete(void *ptr)
	{
		if (ptr) {
			sysBuddyNodeIdx idx = (sysBuddyNodeIdx)(((char*)ptr-m_HeapBase) / m_LeafSize);
			m_Heap.SetIsResource(idx,false);
			m_Heap.Free(idx);
		}
	}

	void TestOnce()
	{
		int index = m_Rand.GetRanged(0,m_ObjectCount-1);
		if (m_Objects[index].ptr)
		{
			m_Objects[index].Shutdown(*this);
			// Displayf("Free.");
		}
		else
		{
			int request = m_Rand.GetRanged(1,128);
			m_Current = index;
			if (!m_Objects[index].Init(request,*this))
			{
				/* Displayf("Alloc failed, triggering defrag..."); */
				sysBuddyHeapDefragInfo info;
				m_Heap.SanityCheck();
				if (m_Heap.Defragment(info,m_HeapBase,m_LeafCount * m_LeafSize))	// returns true if there was anything to do
				{
					for (int i=0; i<info.Count; i++)
						DefragOne(info.Nodes[i].from,info.Nodes[i].to,info.Nodes[i].curSize);
						
					if (m_Objects[index].Init(request,*this))
						/* Displayf("[success!]")*/;
					else
						/* Displayf("[still failed...]") */;

					m_Heap.SanityCheck();
				}
				/* else
					Displayf("[can't defrag]"); */
			}
			/* else
				Displayf("Alloc."); */
		}
	}

	~TestDefrag()
	{
		delete [] m_Objects;
		delete [] m_SelfIndex;
		delete [] m_HeapBase;
		delete [] (char*)m_Workspace;
	}
private:
	void DefragOne(sysBuddyNodeIdx from,sysBuddyNodeIdx to,size_t leafCount);
	sysBuddyHeap m_Heap;
	void *m_Workspace;
	size_t m_LeafCount, m_LeafSize, m_ObjectCount;
	char *m_HeapBase;
	u16 *m_SelfIndex;
	Object *m_Objects;
	mthRandom m_Rand;
	int m_Current;
	int m_AllocCounter;
};


void TestDefrag::DefragOne(sysBuddyNodeIdx from,sysBuddyNodeIdx to,size_t leafCount)
{
	// Make sure that both the source and destination blocks are properly allocated
	Assert(m_Heap.GetSize(from));
	Assert(m_Heap.GetSize(to));

	// Remember our own mapping changes so that we can find the containing object given the node index
	u16 selfIndex = m_SelfIndex[from];
	m_SelfIndex[to] = selfIndex;
	// Displayf("DefragCallback(%x,%x,%d) -> [%d]",from, to, leafCount, selfIndex);

	// Locate the containing object
	Object &o = m_Objects[selfIndex];
	char *fromPtr = m_HeapBase + m_LeafSize * from;
	Assert((char*)o.ptr == fromPtr);
	char *toPtr = m_HeapBase + m_LeafSize * to;
	intptr_t delta = (intptr_t)toPtr - (intptr_t)fromPtr;

	// Move the object's contents
	Assert(leafCount * m_LeafSize >= o.size * sizeof(size_t));
	memcpy(toPtr, fromPtr, leafCount * m_LeafSize);

	m_Heap.UnlockBlock(from,1);
	m_Heap.UnlockBlock(to,1);

	// Free the source memory
	m_Heap.Free(from);

	// Tell the object about its new location
	o.Fixup(delta);	

	// Make sure it worked
	o.Validate();
}


int Main()
{
	{
		TestDefrag testDefrag(60000,8,2000);

		testDefrag.TestReclaim();
	}

	{
		TestDefrag testDefrag(1000,8,200);

		for (int iterations = 0; iterations < 10000000; iterations++)
		{
			if ((iterations % 100000) == 99999)
				Displayf("%d iterations...",iterations+1);
			testDefrag.TestOnce();
		}
	}

	return 0;
}
