// 
// /sample_stack.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// This minimal sample demonstrates our exception handlers with stack traceback support.
//
#include "system/main.h"

#if __WIN32
#pragma inline_depth(0)
#endif

void Four() {
	Displayf("Crash!");
	*(volatile int*)0 = 0;
}

void Three() {
	Four();
}

void Two() {
	Three();
}

void One() {
	Two();
}

int Main() {
	One();
	return 0;
}
