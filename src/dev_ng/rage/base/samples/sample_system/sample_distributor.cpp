// 
// sample_system/sample_distrubtor.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "system/distributor.h"
#include "system/main.h"

// TITLE: Distributor
// PURPOSE:
//		This sample shows how to use the sysDistributorNode class.

using namespace rage;


int Main()
{
	const int numNodes = 7;
	sysDistributorNode nodes[numNodes];

	nodes[0].AddSelf(0);
	nodes[1].AddSelf(0);
	nodes[2].AddSelf(0);
	nodes[3].AddSelf(1);
	nodes[4].AddSelf(2);
	nodes[5].AddSelf(2);

	nodes[6].AddSelf(3);
	nodes[6].Sleep(5);

	for (int i=0; i<20; i++) {
		sysDistributorNode::Update();
		for (int j=0; j<numNodes; j++) {
			if (nodes[j].IsMyTurn()) {
				Displayf("Node %d has turn on frame %d",j,i);
				if (j == 1 || j == 6) {
					Displayf("Putting node %d to sleep...",j);
					nodes[j].Sleep(5);
				}
			}
		}
	}

	return 0;
}
