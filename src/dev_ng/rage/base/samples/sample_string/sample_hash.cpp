// 
// /sample_hash.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "string/stringhash.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

int Main()
{
	for(int i = 1; i < sysParam::GetArgCount(); i++) {
		Printf("0x%x\n", atStringHash(sysParam::GetArg(i)));
	}
	return 0;
}
