// 
// /sample_stringheap.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"
#include "system/timer.h"
#include "string/stringheap.h"
#include "math/random.h"


int Main()
{
	using namespace rage;

	atStringHeap stringHeap;
	char *heapStore = rage_new char[atStringHeap::MaxHeapSize];
	stringHeap.Init(heapStore,atStringHeap::MaxHeapSize,atStringHeap::MaxHandles);
	char **handles[atStringHeap::MaxHandles];
	memset(handles, 0, sizeof(handles));
	mthRandom R;

	sysTimer T;
	const int maxi = 10000000;
	for (int i=0; i<maxi; i++) {
		if (i % (maxi/100) == 0) {
			printf(".");
			fflush(stdout);
			stringHeap.Defragment();
		}
		int h = R.GetRanged(0,atStringHeap::MaxHandles-1);
		if (handles[h]) {
			Assert(**handles[h] == (h & 63) + 32);
			stringHeap.Free(handles[h]);
			handles[h] = 0;
		}
		else {
			int slen = R.GetRanged(2,150);
			handles[h] = stringHeap.Allocate(slen);
			if (handles[h]) {
				memset(*handles[h],(h & 63) + 32,slen-1);
				(*handles[h])[slen-1] = 0;
			}
		}
	}
	Displayf("\nCompleted in %f seconds",T.GetTime());

	delete[] heapStore;

	return 0;
}
