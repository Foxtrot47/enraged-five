// 
// /sample_hash_perf.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"
#include "system/timer.h"
#include "system/nelem.h"

#include "string/stringhash.h"

using namespace rage;

const char *testStrings[] = 
{
	"T:/Hash/String/1",
	"T:/Hash/String/12",
	"T:/Hash/String/123",
	"T:/Hash/String/1234"
};

int Main()
{
	u32 sum = 0;
	sysTimer T;
	for (int i=0; i<100000; i++)
	{
		for (int j=0; j<NELEM(testStrings); j++)
			sum += atStringHash(testStrings[j]);
	}
	Displayf("%fms elapsed time",T.GetMsTime());
	Displayf("final sum %x",sum);

	/*
	Matt Shepcar's version:
	74.740349ms elapsed time
	final sum b6d940e0

	Remap / branchless version:
	78.479118ms elapsed time
	final sum b6d940e0

	Combined version:
	62.664410ms elapsed time
	final sum b6d940e0

	Baseline version:
	159.490616ms elapsed time
	final sum b6d940e0
	*/

	return 0;
}