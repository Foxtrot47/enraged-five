#ifndef SPUFRAG_DMASPUTASK_H
#define SPUFRAG_DMASPUTASK_H

u32 SPUFRAG_myadd[] ALIGNED(128) = 
{
	0x18010183,	// a	$3,$3,$4
	0x35000000,	// bi	$0
};

u32 SPUFRAG_mysub[] ALIGNED(128) = 
{
	0x0800c203,	// sf	$3,$4,$3
	0x35000000,	// bi	$0
};

#endif // SPUFRAG_DMASPUTASK_H
