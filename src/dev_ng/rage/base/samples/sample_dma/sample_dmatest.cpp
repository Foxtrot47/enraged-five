// 
// /sample_dmatest.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

// Sample is only meant for PS3.
#if __PPU

// SPU-TESTING STUFF
#include "system/timer.h"
#include "system/param.h"
#include "system/task.h"
#include "dmasputask.h" // Our example task.

void KickoffSPUTask();

#include "vectormath/test_benchmarks.h"
#include "math/random.h"
#include "system/timer.h"

#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

using namespace rage;
using namespace Vec;

#endif // __PPU


int Main()
{
#if __PPU
	KickoffSPUTask();
#endif // __PPU

	return 0;
}


#if __PPU

// DMA of SPU code fragments test
#include "spufrag_dmasputask.h"
// Our example task's data structures.
#include "dmastructs.h"

// SPU-TESTING STUFF
void KickoffSPUTask()
{
	const int taskCount = 1;

	sysTaskHandle handles[taskCount];
	ppuAvailableFuncAddrTable *inputs = rage_aligned_new(16) ppuAvailableFuncAddrTable[taskCount];
	ppuAvailableFuncAddrTable *outputs = rage_aligned_new(16) ppuAvailableFuncAddrTable[taskCount];

	// Create all of the tasks
	sysTimer T;
	for (int i=0; i<taskCount; i++) {
		inputs[i].pAddFunc = (void*)(SPUFRAG_myadd);
		inputs[i].addFuncSize = sizeof(SPUFRAG_myadd);
		inputs[i].pSubFunc = (void*)(SPUFRAG_mysub);
		inputs[i].subFuncSize = sizeof(SPUFRAG_mysub);
		Printf( "myadd() func addr = 0x%X.\n", (u32)inputs[i].pAddFunc );
		sysTaskParameters p;
		memset(&p, 0, sizeof(p));
		p.Input.Data = &inputs[i];
		p.Input.Size = sizeof(inputs[i]);
		p.Scratch.Data = 0; // This should be a scratch address when it reaches the SPU.
		p.Scratch.Size = 10000; // 10k
		p.Output.Data = &outputs[i];
		p.Output.Size = sizeof(outputs[i]);
		handles[i] = sysTaskManager::Create(TASK_INTERFACE(dmasputask),p);
	}

	// Wait for all tasks to complete.
	sysTaskManager::WaitMultiple(taskCount,handles,true);

	Displayf("Separate Buffer Tasks complete in %f ms",T.GetMsTime());
	for (int i=0; i<taskCount; i++) {
		// A u64 cannot be portably printf'd!
		//Displayf("PPU: %lld (%p)",outputs[i].number,&outputs[i].number);
	}

	delete[] outputs;
	delete[] inputs;

	Displayf("**** completed ***");
};

#endif // __PPU
