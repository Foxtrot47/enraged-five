#ifndef DMA_STRUCTS_H
#define DMA_STRUCTS_H

namespace rage
{

	typedef int (intTakingIntInt_t)(int, int);

	// NEW STRUCT
	// Multiple-of-16-sized struct.
	struct ppuAvailableFuncAddrTable
	{
		void* pAddFunc;	
		void* pSubFunc;
		u32 addFuncSize;
		u32 subFuncSize;
	} ;

};

#endif // DMA_STRUCTS_H
