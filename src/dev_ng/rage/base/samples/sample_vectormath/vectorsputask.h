// 
// sample_vectormath/vectorsputask.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SYSTEM_VECTORSPUTASK_H
#define SAMPLE_SYSTEM_VECTORSPUTASK_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(vectorsputask);

#endif

