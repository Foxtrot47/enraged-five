// 
// /sample_vector.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

// SPU-TESTING STUFF
#if __PPU
#include "system/timer.h"
#include "system/param.h"
#include "system/task.h"
#include "vectorsputask.h" // Our example task.

void KickoffSPUTask();
#endif
// END SPU-TESTING STUFF

#include "atl/binmap.h"
#include "atl/map.h"
#include "atl/string.h"

#include "vectormath/test_benchmarks.h"
#include "math/random.h"
#include "system/timer.h"

#include "vectormath/classes.h"
#include "vectormath/classes_soa.h"
#include "vectormath/legacyconvert.h"
#include "vectormath/layoutconvert.h"

#include "vector/color32.h"
#include "vector/matrix33.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/quaternion.h"
//#include "vector/vector3.h"

#include "math/random.h"
#include "system/new.h"

#include <vector>

using namespace rage;


int Main()
{
	//================================================
	// SSE SUPPORT OUTPUT
	//================================================
#if __WIN32PC
	Vec::eSSESupport supp = Vec::GetSSESupportLevel();
	Vec::PrintSSESupportLevelInfo( supp );
#endif

	//================================================
	// VECTOR BENCHMARKS
	//================================================
	//vecBenchmarks::Run();


	//atHashedMap<u32, atString, u32, atStringHashObj> evoMap;
	//evoMap.Insert( "propColor", 1 );
	//evoMap.Insert( "propTheta", 1 );
	//evoMap.Insert( "propBounceDmp", 0 );
	//evoMap.FinishInsertion();

	//u32 hashProp0 = evoMap.Hash( "propColor" );
	//u32 hashProp1 = evoMap.Hash( "propTheta" );
	//u32 hashProp2 = evoMap.Hash( "propBounceDmp" );

	//// ...

	//u32* u0 = evoMap.SafeGet( hashProp0 );
	//u32* u1 = evoMap.SafeGet( hashProp1 );
	//u32* u2 = evoMap.SafeGet( hashProp2 );

	//Printf( "Has evo for propColor? %i\n", *u0 );
	//Printf( "Has evo for propTheta? %i\n", *u1 );
	//Printf( "Has evo for propBounceDmp? %i\n", *u2 );

	//atMap<u32, u32> evoMap;
	//evoMap.Insert( 14124, 1 );
	//evoMap.Insert( 23566643, 1 );
	//evoMap.Insert( 2321212352, 0 );

	//// ...

	//u32* u0 = evoMap.Access( 14124 );
	//u32* u1 = evoMap.Access( 23566643 );
	//u32* u2 = evoMap.Access( 2321212352 );

	//Printf( "Has evo for 14124? %i\n", *u0 );
	//Printf( "Has evo for 23566643? %i\n", *u1 );
	//Printf( "Has evo for 2321212352? %i\n", *u2 );

	


	



















#if __PPU
	//KickoffSPUTask();
#endif
	//	XTraceStopRecording();
	return 0;




	//================================================
	// Vectorized Colo32 compression test case.
	//================================================
	//// The original color.
	//// R=5, G=6, B=7, A=4
	//Color32 c(5, 6, 7, 4);

	//// Decode using Get*f() methods.
	//Vec4V cv = Vec4V(c.GetAlphaf(),c.GetRedf(),c.GetGreenf(),c.GetBluef());
	//cv.Print(); // Prints "A,R,G,B" = "<(f32),(f32),(f32),(f32)>"

	//// Encode using old method.
	//c.Setf( cv.GetYf(), cv.GetZf(), cv.GetWf(), cv.GetXf() ); // Set "R,G,B,A"
	//Printf( "<%u, %u, %u, %u>\n", c.GetAlpha(), c.GetRed(), c.GetGreen(), c.GetBlue() ); // Prints "A,R,G,B" = "4,5,6,7"

	//// Encode using new method.
	//c.SetFromARGB( cv );
	//Printf( "<%u, %u, %u, %u>\n", c.GetAlpha(), c.GetRed(), c.GetGreen(), c.GetBlue() ); // Prints "A,R,G,B" = "4,5,6,7"
	//c.SetFromRGBA( Vec4V(cv.GetYf(),cv.GetZf(),cv.GetWf(),cv.GetXf()) ); // Re-org as RGBA
	//Printf( "<%u, %u, %u, %u>\n", c.GetAlpha(), c.GetRed(), c.GetGreen(), c.GetBlue() ); // Prints "A,R,G,B" = "4,5,6,7"


	


	//================================================
	//  QuatVFromMat33VOrtho() fail case.
	//================================================
//	Vec::Vector_4V col0 = VECTOR4V_LITERAL(-1.00000000f, -3.25841341e-007f, 1.06581410e-013f, -3.25841341e-007f);
//	Vec::Vector_4V col1 = VECTOR4V_LITERAL(-3.25841341e-007f, 0.999999940f, -3.25841370e-007f, 0.999999940f);
//	Vec::Vector_4V col2 = VECTOR4V_LITERAL(0.000000000f, -3.25841370e-007f, -1.00000000f, -3.25841370e-007f);
//	Vec::Vector_4V col3 = VECTOR4V_LITERAL(1.69622865e-007f, -0.520568848f, 1.69622865e-007f, -0.520568848f);
//	Mat34V m34(col0, col1, col2, col3);
//
//#pragma warning(disable:4996)
//	Mat34V lastToCurrent = m34;
//	QuatV tempQuat = QuatVFromMat33V( lastToCurrent.GetMat33() );
//	ScalarV v_rotationAngle = GetAngle( tempQuat );
//	Printf( "Quatf (current) = %f,%f,%f,%f\n", tempQuat.GetXf(), tempQuat.GetYf(), tempQuat.GetZf(), tempQuat.GetWf() );
//	Printf( "Rotation angle (current) = %f\n", v_rotationAngle.Getf() );
//
//	Mat34V lastToCurrent_new = m34;
//	QuatV tempQuat_new = QuatVFromMat33VOrtho( lastToCurrent_new.GetMat33() );
//	ScalarV v_rotationAngle_new = GetAngle( tempQuat_new );
//	Printf( "Quatf (new) = %f,%f,%f,%f\n", tempQuat_new.GetXf(), tempQuat_new.GetYf(), tempQuat_new.GetZf(), tempQuat_new.GetWf() );
//	Printf( "Rotation angle (new) = %f\n", v_rotationAngle_new.Getf() );
//
//	rage::Matrix34 lastToCurrent_old = RCC_MATRIX34(m34);
//	rage::Quaternion tempQuat_old; 
//	lastToCurrent_old.ToQuaternion(tempQuat_old);
//	float rotationAngle_old = tempQuat_old.GetAngle();
//	Printf( "Quatf (old) = %f,%f,%f,%f\n", tempQuat_old.x, tempQuat_old.y, tempQuat_old.z, tempQuat_old.w );
//	Printf( "Rotation angle (old) = %f\n", rotationAngle_old );

	//================================================
	// IsFinite()/IsNotNan() tests.
	//================================================
	//ScalarV v1( Vec::V4VConstant(V_NAN) );
	//ScalarV v2( Vec::V4VConstant(V_INF) );
	//ScalarV v3( Vec::V4VConstant(V_FIVE) );
	//ScalarV v4( Vec::V4VConstant(V_ZERO) );
	//Vec4V v5( v1, v2, v3, v4 );
	//VecBoolV isF = IsFinite( v5 );
	//VecBoolV isN = IsNotNan( v5 );
	//isF.Print();
	//isN.Print();

	//================================================
	// Float divide vs vectorized divide
	// speed tests
	//================================================
	//AoS::Vec3V* g_sum = new AoS::Vec3V;
	//*g_sum = AoS::Vec3V(AoS::Vec3V::ONE);
	//float* g_fSum = new float;
	//*g_fSum = 1.0f;

	//sysTimer s;
	//AoS::Vec3V sum = *g_sum;
	//// Loop is manually unrolled 10 times to make the measurement fair,
	//// since the compiler unrolled the scalar float version that many times.
	//for( int i = 0; i < 1000000/10; i ++ )
	//{
	//	AoS::Vec3V temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//	temp = InvScale( sum, sum );
	//	sum *= temp;
	//}
	//float fDone = s.GetUsTime();
	//sum.Print();
	//s.Reset();
	//float fSum = *g_fSum;
	//for( int i = 0; i < 1000000; i ++ )
	//{
	//	float fTemp = fSum / fSum;
	//	fSum *= fTemp;
	//}
	//float fDone2 = s.GetUsTime();
	//Printf( "%f\n", fSum );
	//Printf( "TIME_V: %f\n", fDone );
	//Printf( "TIME_F: %f\n", fDone2 );

	//================================================
	// Vector denormals test case.
	// 'test2' will be a denormalized result on
	// Win32 if Vec::DisableDenormals() isn't called.
	//================================================
	//Vec::DisableDenormals();
	//
	//ScalarV test = ScalarVFromF32( FLT_MIN );
	//ScalarV test2 = test / ScalarV(V_TWO);
	//test2.Print();

	//================================================
	// Vectorized random number generator
	// speed tests (vs scalar float)
	//================================================

	//sysTimer s;
	//AoS::Vec3V sum(AoS::V_ZERO);
	//for( int i = 0; i < 1000000; i ++ )
	//{
	//	AoS::Vec3V temp = g_ReplayRand.Get3FloatsV();
	//	sum += temp;
	//}
	//float fDone = s.GetUsTime();
	//sum.Print();

	//s.Reset();
	//sum = AoS::Vec3V(AoS::V_ZERO);
	//for( int i = 0; i < 1000000; i ++ )
	//{
	//	AoS::Vec3V temp( g_ReplayRand.GetFloat(), g_ReplayRand.GetFloat(), g_ReplayRand.GetFloat() );
	//	sum += temp;
	//}
	//float fDone2 = s.GetUsTime();
	//sum.Print();
	//Printf( "TIMEV: %f\n", fDone );
	//Printf( "TIME2: %f\n", fDone2 );

	//================================================
	// Vectorized random number generator
	// distribution tests
	//================================================

	//std::vector<int> buckets;
	//buckets.resize( 11, 0 );

	//float f;
	//for( int i = 0; i < 100000; i ++ )
	//{
	//	AoS::Vec3V temp = Get3FloatsV();
	//	f = temp.GetZf();

	//	//f = random();
	//	if( f >= 0.0f && f < 0.1f )
	//		buckets[0]++;
	//	else if( f >= 0.1f && f < 0.2f )
	//		buckets[1]++;
	//	else if( f >= 0.2f && f < 0.3f )
	//		buckets[2]++;
	//	else if( f >= 0.3f && f < 0.4f )
	//		buckets[3]++;
	//	else if( f >= 0.4f && f < 0.5f )
	//		buckets[4]++;
	//	else if( f >= 0.5f && f < 0.6f )
	//		buckets[5]++;
	//	else if( f >= 0.6f && f < 0.7f )
	//		buckets[6]++;
	//	else if( f >= 0.7f && f < 0.8f )
	//		buckets[7]++;
	//	else if( f >= 0.8f && f < 0.9f )
	//		buckets[8]++;
	//	else if( f >= 0.9f && f < 1.0f )
	//		buckets[9]++;
	//	else
	//	{
	//		buckets[10]++;
	//		Printf( "Weird one: %f\n", f );
	//	}
	//	//v_sfrand();
	//}
	//for( unsigned int i = 0; i < buckets.size(); i++ )
	//{
	//	Printf( "[%ff, %ff): %i\n", i/10.0f, (i+1)/10.0f, buckets[i] );
	//}

	//================================================
	// V4MergeXY*() tests.
	//================================================
	//Vec::Vector_4V a = Vec::V4VConstant<1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16>();
	//Vec::Vector_4V b = Vec::V4VConstant<17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32>();
	//Vec::Vector_4V c = Vec::V4MergeXYByte(a,b);
	//Vec::Vector_4V d = Vec::V4MergeZWByte(a,b);
	//Vec::Vector_4V e = Vec::V4MergeXYShort(a,b);
	//Vec::Vector_4V f = Vec::V4MergeZWShort(a,b);
	//Vec::Vector_4V c_shouldbe = Vec::V4VConstant<1,17,2,18,3,19,4,20,5,21,6,22,7,23,8,24>();
	//Vec::Vector_4V d_shouldbe = Vec::V4VConstant<9,25,10,26,11,27,12,28,13,29,14,30,15,31,16,32>();
	//Vec::Vector_4V e_shouldbe = Vec::V4VConstant<1,2,17,18,3,4,19,20,5,6,21,22,7,8,23,24>();
	//Vec::Vector_4V f_shouldbe = Vec::V4VConstant<9,10,25,26,11,12,27,28,13,14,29,30,15,16,31,32>();
	//Vec::V4PrintHex(c);
	//Vec::V4PrintHex(c_shouldbe);
	//Vec::V4PrintHex(d);
	//Vec::V4PrintHex(d_shouldbe);
	//Vec::V4PrintHex(e);
	//Vec::V4PrintHex(e_shouldbe);
	//Vec::V4PrintHex(f);
	//Vec::V4PrintHex(f_shouldbe);

	//================================================
	// SoA example (for Ray / MC4)
	//================================================

	//// Incoming plane data and CenterV and radius.
	//AoS::Vec4V planes[16];
	//AoS::Vec4V CenterV;
	//AoS::Vec4V r;
	//
	//// Convert to SoA.
	//SoA::Vec4V a, b, c, d;
	//ToSoA( a, planes[0], planes[1], planes[2], planes[3] );
	//ToSoA( b, planes[4], planes[5], planes[6], planes[7] );
	//ToSoA( c, planes[8], planes[9], planes[10], planes[11] );
	//ToSoA( d, planes[12], planes[13], planes[14], planes[15] );

	//// Initialize the other half of the dot4, where each component is a component of CenterV.
	//SoA::Vec4V e = SoA::Vec4V( AoS::SplatX(CenterV).GetIntrin128(), AoS::SplatY(CenterV).GetIntrin128(), AoS::SplatZ(CenterV).GetIntrin128(), AoS::SplatW(CenterV).GetIntrin128() );

	//// Do the four SoA dot4 dot products (which really does 16 latency-hiding dot4 dot products, using multiply-add's).
	//SoA::ScalarV result0 = SoA::Dot( a, e );
	//SoA::ScalarV result1 = SoA::Dot( b, e );
	//SoA::ScalarV result2 = SoA::Dot( c, e );
	//SoA::ScalarV result3 = SoA::Dot( d, e );

	//// Find the max of the four results.
	//SoA::ScalarV maxResult = SoA::Max( SoA::Max( result0, result1 ), SoA::Max( result2, result3 ) );

	//// Convert back to AoS at this point.
	//AoS::Vec4V aosMaxResult;
	//ToAoS( aosMaxResult, maxResult );

	//// Now do the branch.
	//if( IsLessThanAll( aosMaxResult, r ) != 0 )
	//{
	//	return false;
	//}
	//// ...

	//================================================
	// Fast float-as-int compare tests.
	//================================================
	//union
	//{
	//	float f;
	//	u32 u;
	//} Temp;
	//Temp.u = 0x80000000;
	//float f1 = 0.0f;
	//float f2 = Temp.f;	// -0.0
	//float f3 = 1.0f;
	//float f4 = -5.0f;
	//float f5 = 5.0f;
	//
	//if( !FloatEqualsFloat( &f1, &f2 ) && !FloatEqualsFloat( &f1, &f3 ) && FloatEqualsFloat( &f4, &f4 ) )
	//{
	//	Printf("FloatEqualsFloat() works!\n");
	//}
	//if( FloatNeqFloat( &f1, &f2 ) && FloatNeqFloat( &f1, &f3 ) && !FloatNeqFloat( &f4, &f4 ) )
	//{
	//	Printf("FloatNeqFloat() works!\n");
	//}
	//if( FloatEqualsFloatSafe( &f1, &f2 ) && !FloatEqualsFloatSafe( &f1, &f4 ) && !FloatEqualsFloatSafe( &f4, &f5 ) )
	//{
	//	Printf("FloatEqualsFloatSafe() works!\n");
	//}
	//if( !FloatNeqFloatSafe( &f1, &f2 ) && FloatNeqFloatSafe( &f1, &f4 ) && FloatNeqFloatSafe( &f4, &f5 ) )
	//{
	//	Printf("FloatNeqFloatSafe() works!\n");
	//}
	//if( FloatEqualsZero( &f1 ) && FloatEqualsZero( &f2 ) && !FloatEqualsZero( &f3 ) && !FloatEqualsZero( &f4 ) )
	//{
	//	Printf("FloatEqualsZero() works!\n");
	//}
	//if( FloatNeqZero( &f3 ) && FloatNeqZero( &f4 ) && !FloatNeqZero( &f1 ) && !FloatNeqZero( &f2 ) )
	//{
	//	Printf("FloatNeqZero() works!\n");
	//}
	//if( FloatGreaterThanZero( &f3 ) && !FloatGreaterThanZero( &f4 ) && !FloatGreaterThanZero( &f1 ) && !FloatGreaterThanZero( &f2 ) )
	//{
	//	Printf("FloatGreaterThanZero() works!\n");
	//}
	//if( FloatLessThanZero( &f4 ) && !FloatLessThanZero( &f3 ) && !FloatLessThanZero( &f1 ) && !FloatLessThanZero( &f2 ) )
	//{
	//	Printf("FloatLessThanZero() works!\n");
	//}
	//if( FloatGreaterThanOrEqualZero( &f1 ) && FloatGreaterThanOrEqualZero( &f2 ) && FloatGreaterThanOrEqualZero( &f3 ) && !FloatGreaterThanOrEqualZero( &f4 ) )
	//{
	//	Printf("FloatGreaterThanOrEqualZero() works!\n");
	//}
	//if( FloatLessThanOrEqualZero( &f1 ) && FloatLessThanOrEqualZero( &f2 ) && !FloatLessThanOrEqualZero( &f3 ) && FloatLessThanOrEqualZero( &f4 ) )
	//{
	//	Printf("FloatLessThanOrEqualZero() works!\n");
	//}

	//================================================
	// Benchmarks of floating point compares vs.
	// CR-setting vector compares.
	//================================================

	//Vector_4V vec1 = getvec();
	//Vector_4V vec2 = getvec();
	//sysTimer s;
	//s.Reset();
	//for( int i = 0; i < 10000000/4; i++ )
	//{
	//	if( V4IsEqualAll(vec1, vec2) != 0 )
	//	{
	//		vec1 = V4Add( vec1, vec1 );
	//		vec2 = V4Add( vec2, vec2 );
	//	}
	//	if( V4IsEqualAll(vec1, vec2) != 0 )
	//	{
	//		vec1 = V4Add( vec1, vec1 );
	//		vec2 = V4Add( vec2, vec2 );
	//	}
	//	if( V4IsEqualAll(vec1, vec2) != 0 )
	//	{
	//		vec1 = V4Add( vec1, vec1 );
	//		vec2 = V4Add( vec2, vec2 );
	//	}
	//	if( V4IsEqualAll(vec1, vec2) != 0 )
	//	{
	//		vec1 = V4Add( vec1, vec1 );
	//		vec2 = V4Add( vec2, vec2 );
	//	}
	//}
	//float fTime = s.GetUsTime();

	////float flt1 = getflt();
	////float flt2 = getflt();
	////sysTimer s;
	////s.Reset();
	////for( int i = 0; i < 10000000; i++ )
	////{
	////	if( flt1 == flt2 )
	////	{
	////		flt1 = flt1 + flt1;
	////		flt2 = flt2 + flt2;
	////	}
	////}
	////float fTime = s.GetUsTime();


	//Printf( "TIME = %f\n", fTime );

	//================================================
	// V3ClampMag() test.
	//================================================
	//Scalar test.
	//Vector_4 a = VEC4_LITERAL( 3.0f, 4.0f, 0.0f, 100.0f );
	//Vector_4 b = VEC4_LITERAL( 4.0f, 4.0f, 4.0f, 4.0f );
	//Vector_4 c = VEC4_LITERAL( 4.5f, 6.0f, 6.0f, 6.0f );
	//Vector_4 d;
	//d = V3ClampMag( a, b, c ); // mag is 5, should clamp mag between b and c.
	//V4Print( d );
	//d = V3MagV( d );
	//V4Print( d );
	//// Equivalent SoA test.
	//Vec::Vector_4V _a0 = VECTOR4V_LITERAL( 3.0f, 3.0f, 3.0f, 3.0f );
	//Vec::Vector_4V _a1 = VECTOR4V_LITERAL( 4.0f, 4.0f, 4.0f, 4.0f );
	//Vec::Vector_4V _a2 = VECTOR4V_LITERAL( 0.0f, 0.0f, 0.0f, 0.0f );
	//Vec::Vector_4V _b = VECTOR4V_LITERAL( 4.0f, 5.5f, 4.0f, 4.0f );
	//Vec::Vector_4V _c = VECTOR4V_LITERAL( 4.5f, 6.0f, 6.0f, 6.0f );
	//Vec3V a( _a0, _a1, _a2 );
	//Vec1V b( _b );
	//Vec1V c( _c );
	//Vec3V d;
	//Vec1V mag;
	//ClampMag( d, a, b, c ); // mag is 5, should clamp mag between b and c.
	//d.Print();
	//mag = Mag( d );
	//mag.Print();
	//mag.Print();

	//================================================
	// DotTranspose() <==> UnTransformOrtho()
	// equality test.
	//================================================
	//Matrix34 a(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f);
	//Matrix34 b(13.0f, 14.0f, 15.0f, 16.0f, 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f);
	//Matrix34 c;
	//c.DotTranspose( a, b );
	//c.Print();

	//Mat34V a2 = MATRIX34_TO_MAT34V(a);//(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f);
	//Mat34V b2 = MATRIX34_TO_MAT34V(b);//(13.0f, 14.0f, 15.0f, 16.0f, 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f);
	//Mat34V c2;
	//UnTransformOrtho(c2, b2, a2);
	//Matrix34 c3 = MAT34V_TO_MATRIX34(c2);
	//c3.Print();


	//================================================
	// BytePermute tests.
	//================================================
	//union
	//{
	//	Vec::Vector_4V v;
	//	u8 ch[16];
	//} Temp, Temp2, TempResult;

	//Temp.ch[0] = 0;
	//Temp.ch[1] = 1;
	//Temp.ch[2] = 2;
	//Temp.ch[3] = 3;
	//Temp.ch[4] = 4;
	//Temp.ch[5] = 5;
	//Temp.ch[6] = 6;
	//Temp.ch[7] = 7;
	//Temp.ch[8] = 8;
	//Temp.ch[9] = 9;
	//Temp.ch[10] = 10;
	//Temp.ch[11] = 11;
	//Temp.ch[12] = 12;
	//Temp.ch[13] = 13;
	//Temp.ch[14] = 14;
	//Temp.ch[15] = 15;
	//Temp2.ch[0] = 16;
	//Temp2.ch[1] = 17;
	//Temp2.ch[2] = 18;
	//Temp2.ch[3] = 19;
	//Temp2.ch[4] = 20;
	//Temp2.ch[5] = 21;
	//Temp2.ch[6] = 22;
	//Temp2.ch[7] = 23;
	//Temp2.ch[8] = 24;
	//Temp2.ch[9] = 25;
	//Temp2.ch[10] = 26;
	//Temp2.ch[11] = 27;
	//Temp2.ch[12] = 28;
	//Temp2.ch[13] = 29;
	//Temp2.ch[14] = 30;
	//Temp2.ch[15] = 31;
	//TempResult.v = Vec::V4BytePermuteTwo<16+0,16+15,16+1,16+2,16+3,16+4,16+5,16+6,16+7,16+8,16+9,16+10,16+11,16+12,16+13,16+14>( Temp.v, Temp2.v );

	//Printf( "%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\n",
	//	TempResult.ch[0],
	//	TempResult.ch[1],
	//	TempResult.ch[2],
	//	TempResult.ch[3],
	//	TempResult.ch[4],
	//	TempResult.ch[5],
	//	TempResult.ch[6],
	//	TempResult.ch[7],
	//	TempResult.ch[8],
	//	TempResult.ch[9],
	//	TempResult.ch[10],
	//	TempResult.ch[11],
	//	TempResult.ch[12],
	//	TempResult.ch[13],
	//	TempResult.ch[14],
	//	TempResult.ch[15]
	//	);

	//================================================
	// Test of code generation (or lack thereof) for
	// legacy conversion macros
	//================================================

	//Matrix33 from_1;
	//Mat33V to_1;

	//Matrix34 from_2;
	//Mat34V to_2;

	//Matrix44 from_3;
	//Mat44V to_3;

	//from_1 = Matrix33( 1.0f,1.0f,1.0f,2.0f,2.0f,2.0f,3.0f,3.0f,3.0f );
	//from_2 = Matrix34( 1.0f,1.0f,1.0f,2.0f,2.0f,2.0f,3.0f,3.0f,3.0f,4.0f,4.0f,4.0f );
	//from_3 = Matrix44( 1.0f,1.0f,1.0f,1.0f,2.0f,2.0f,2.0f,2.0f,3.0f,3.0f,3.0f,3.0f,4.0f,4.0f,4.0f,4.0f );

	//from_1.Print();
	//from_2.Print();
	//from_3.Print();

	//// These 6 lines generate no code in release mode (which is exactly what we want the macros to accomplish!)
	//// Except on Win32... which sucks.
	//to_1 = MATRIX33_TO_MAT33V( from_1 );
	//to_2 = MATRIX34_TO_MAT34V( from_2 );
	//to_3 = MATRIX44_TO_MAT44V( from_3 );

	//from_1 = MAT33V_TO_MATRIX33( to_1 );
	//from_2 = MAT34V_TO_MATRIX34( to_2 );
	//from_3 = MAT44V_TO_MATRIX44( to_3 );

	//from_1.Print();
	//from_2.Print();
	//from_3.Print();

	//================================================
	// Test of speed of IsLessThanAll() methods
	//================================================

//#define NUM_TEST 100000
//
//	sysTimer tm;
//	float fTime;
//	Vector_4V* pV = new Vector_4V;
//
//	Vector_4V vecTemp = V4VConstant(V_ZERO);
//	unsigned int intTemp;
//	float floatTemp;
//	tm.Reset();
//	for(int i = 0; i < NUM_TEST; i++)
//	{
//		//intTemp = V4IsLessThanAll( vecTemp, vecTemp );
//		intTemp = V4IsLessThanAll_2( vecTemp, vecTemp );
//		floatTemp = (float)(intTemp);
//		V4Set( vecTemp, floatTemp );
//	}
//	fTime = tm.GetUsTime();
//	(*pV) = vecTemp;
//	delete pV;
//	Printf("TIME: %f us\n", fTime);

	//================================================
	// GetFromTwo<>(Vec3V,Vec3V) specialization test
	//================================================

	//Vec3V a = Vec3V(V_MASKXYZ);
	//Vec3V b = Vec3V(V_ZERO);
	//Vec3V c = GetFromTwo<Z1,Z2,W1>( a, b ); // Add __declspec(noinline) to the call to easily see that branches are optimized out
	//c.Print();

	//================================================
	// fsel tests
	//================================================

//	XTraceStartRecording( "devkit:\\trace.pix2" );

//	Vec::Vector_3 temp1;
////	Vec::Vector_3 temp2;
//	int numTests = 1000000;
//
//	float* aArray = new float[numTests];
//	float* bArray = new float[numTests];
////	Vec::Vector_3* outTemp3 = new Vec::Vector_3[numTests];
////	u32* uoutArray = new u32[numTests];
//
//	for(int i = 0; i < numTests; i++)
//	{
//		aArray[i] = (g_ReplayRand.GetFloat())*200.0f;
//		bArray[i] = (g_ReplayRand.GetFloat())*200.0f;
//	}
//
//	float totalFSELTime = 0.0f;
//	float totalBRANCHTime = 0.0f;
//	sysTimer timerForFSEL;
//	sysTimer timerForBRANCH;
//
//	timerForFSEL.Reset();
//	for(int i = 0; i < numTests; i++)
//	{
//		temp1.f.x = aArray[i];
//		float localSqrt = FPSqrt( temp1.f.x );
//		bArray[i] = localSqrt;
//	}
//	totalFSELTime += timerForFSEL.GetMsTime();
//
//	timerForBRANCH.Reset();
//	for(int i = 0; i < numTests; i++)
//	{
//		temp1.f.x = aArray[i];
//		float localSqrt = FPSqrt( temp1.f.x );
//		bArray[i] = localSqrt;
//	}
//	totalBRANCHTime += timerForBRANCH.GetMsTime();
//
//	
//
//	// RESULTS
//	Errorf( "TOTAL BRANCH TIME: %fms\n", totalBRANCHTime );
//	Errorf( "TOTAL FSEL   TIME: %fms\n", totalFSELTime );
//
//	delete [] aArray;
//	delete [] bArray;
////	delete [] outTemp3;


	
	//================================================
	// Trig tests
	//================================================

//	std::ofstream outFile;
//	outFile.open( "devkit:\\tanResults.csv" );
	//const int numSamples = 100;
	//const float topOfRange = 200.0f;

	//Vec::Vector_4V* bigVecArray1;
	//Vec::Vector_4V* bigVecArray2;
	//Vec::Vector_4V* outVecArray;

	//bigVecArray1 = new Vec::Vector_4V[numSamples];
	//bigVecArray2 = new Vec::Vector_4V[numSamples];
	//outVecArray = new Vec::Vector_4V[numSamples];

	//for(int i = 0; i < numSamples; i++)
	//{
	//	Vec::V4Set( bigVecArray1[i], (topOfRange*i/(numSamples-1)) );
	//	Vec::V4Set( bigVecArray2[i], (topOfRange*i/(numSamples-1)) );
	//}

	//// Get the control vector into cache.
	//g_vec = Vec::V4PermuteTwo<Vec::X1,Vec::X1,Vec::X1,Vec::W2>( bigVecArray1[0], bigVecArray2[0] );

	//// Time this.
	//sysTimer tm;
	//for(int i = 0; i < numSamples; i++)
	//{
	//	outVecArray[i] = Vec::V4PermuteTwo<Vec::X1,Vec::X1,Vec::X1,Vec::W2>( bigVecArray1[i], bigVecArray2[i] );
	//}
	//float fTime = tm.GetUsTime();

	//Errorf("%f us total\n", fTime);

	//delete [] bigVecArray1;
	//delete [] bigVecArray2;
	//delete [] outVecArray;

	
	//================================================
	// Very minimal fast divide test.
	//================================================
	
	//float f1=0.0f,f2=0.0f,f3=0.0f,f4=0.0f;
	//float a=1.0f,b=10.0f,c=1.0f,d=2.0f,e=1.0f,f=100.0f,g=1.0f,h=1000.0f;
	//Printf( "Normal division:      f1 = %.25f, f2 = %.25f, f3 = %.25f, f4 = %.25f\n", a/b,c/d,e/f,g/h );
	//FP_FAST_DIVIDE_4( f1,f2,f3,f4,a,b,c,d,e,f,g,h );
	//Printf( "Fast 4-fold division: f1 = %.25f, f2 = %.25f, f3 = %.25f, f4 = %.25f\n", f1,f2,f3,f4 );
	//FP_FAST_DIVIDE_3( f1,f2,f3,a,b,c,d,e,f );
	//Printf( "Fast 3-fold division: f1 = %.25f, f2 = %.25f, f3 = %.25f, f4 = %.25f\n", f1,f2,f3,f4 );
	//FP_FAST_DIVIDE_2( f1,f2,a,b,c,d );
	//Printf( "Fast 2-fold division: f1 = %.25f, f2 = %.25f, f3 = %.25f, f4 = %.25f\n", f1,f2,f3,f4 );
	
	//================================================
	// Very-fast divide speed tests.
	//================================================

	//float f1=0.0f,f2=0.0f,f3=0.0f,f4=0.0f;
	//float a=1.0f,b=10.0f,c=1.0f,d=2.0f,e=1.0f,f=100.0f,g=1.0f,h=1000.0f;
	//sysTimer s;
	//for( int i = 0; i < 100000; i++ )
	//{
	//	FP_VERYFAST_DIVIDE_4_2( f1,f2,f3,f4,a,b,c,d,e,f,g,h );
	//	a = f1, b = f2, c = f3, d = f4;
	//	e = f1+1.0f, f = f2+1.0f, g = f3+1.0f, h = f4+1.0f;
	//}
	//float fTime = s.GetUsTime();
	//Printf("TIME: %f us\n", fTime);

}

struct test {
	u64 number;
	u64 pad;
};



// SPU-TESTING STUFF
#if __PPU
void KickoffSPUTask()
{
	const int taskCount = 1;

	sysTaskHandle handles[taskCount];
	test *inputs = rage_aligned_new(16) test[taskCount];
	test *outputs = rage_aligned_new(16) test[taskCount];

	// Create all of the tasks
	sysTimer T;
	for (int i=0; i<taskCount; i++) {
		inputs[i].number = __OPTIMIZED? 1000001 : 100001;
		sysTaskParameters p;
		memset(&p, 0, sizeof(p));
		p.Input.Data = &inputs[i];
		p.Input.Size = sizeof(inputs[i]);
		p.Output.Data = &outputs[i];
		p.Output.Size = sizeof(outputs[i]);
		handles[i] = sysTaskManager::Create(TASK_INTERFACE(vectorsputask),p);
	}

	// Wait for all tasks to complete.
	sysTaskManager::WaitMultiple(taskCount,handles,true);

	Displayf("Separate Buffer Tasks complete in %f ms",T.GetMsTime());
	for (int i=0; i<taskCount; i++) {
		// A u64 cannot be portably printf'd!
		Displayf("PPU: %lld (%p)",outputs[i].number,&outputs[i].number);
	}

	delete[] outputs;
	delete[] inputs;

	Displayf("**** completed ***");
};
#endif
// END SPU-TESTING STUFF
