@echo off
for /d %%c in (City01) do (
	pushd %%c

	for /d %%i in (*) do (
		pushd %%i
		echo ======= %%i ========
		c:\soft\rage\base\tools\rorc\win32_rscrelease\rorc_win32_rscrelease.exe entity.type %%i
		c:\soft\rage\base\tools\rorc\win32_pagingrelease\rorc_win32_pagingrelease.exe entity.type %%i
		popd
	)
	
	popd

	pushd textures

	for /d %%i in (*d) do (
		pushd %%i
		echo ======= %%i =======
		dir/b *.dds > textureList.txt
		c:\soft\rage\base\tools\rorc\win32_rscrelease\rorc_win32_rscrelease.exe textureList.txt %%i
		c:\soft\rage\base\tools\rorc\win32_pagingrelease\rorc_win32_pagingrelease.exe textureList.txt %%i
		popd
	)

	popd

	if exist %%cx.zip del %%cx.zip 
	if exist %%cw.zip del %%cw.zip
	zip -q -0 -m -j -r %%cx.zip %%c/* -i *.xdr
	zip -q -0 -m -j -r %%cw.zip %%c/* -i *.wdr
	zip -q -0 -m -j -r %%cx.zip textures/* -i *.xtd
	zip -q -0 -m -j -r %%cw.zip textures/* -i *.wtd
)

