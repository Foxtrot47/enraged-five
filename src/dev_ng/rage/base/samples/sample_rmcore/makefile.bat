set TESTERS=sample_drawable sample_rendertarget sample_shadervars sample_level sample_instance
set TESTERS=%TESTERS% sample_resource sample_rdr2 sample_vertexbuffer 
set TESTERS=%TESTERS% sample_swaptexdict sample_multirender
set TESTERS=%TESTERS% sample_mtlscan sample_meshblend
set LIBS=%RAGE_SAMPLE_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS% squish
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples
