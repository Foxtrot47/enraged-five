// 
// /sample_mtlscan.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"
#include "system/param.h"
#include "file/asset.h"
#include "file/stream.h"
#include "file/packfile.h"
#include "file/token.h"
#include "grcore/effect.h"
#include "atl/map.h"

using namespace rage;

// Hack to get GTA4's AES keys into this sample.
#undef HACK_GTA4
#define HACK_GTA4		1
#include "../../base/src/data/aes.cpp"

struct Preset {	// a fake sps file
	// Load preset from tokenizer
	void Load(fiTokenizer &T) {
		Redundant = 0;
		Referenced = 0;
		Items.Reset();
		if (T.CheckToken("shader"))
			T.GetToken(Shader,sizeof(Shader));
		else
			Shader[0] = 0;
		if (T.CheckToken("comment"))
			T.SkipToEndOfLine();
		char id[64];
		while (T.GetToken(id,sizeof(id))) {
			T.MatchToken("{");
			Item I;
			safecpy(I.Name,id);
			char type[16];
			T.GetToken(type,sizeof(type));
			if (!strcmp(type,"float"))
				I.Type = 1;
			else if (!strcmp(type,"Vector2"))
				I.Type = 2;
			else if (!strcmp(type,"Vector3"))
				I.Type = 3;
			else if (!strcmp(type,"Vector4"))
				I.Type = 4;
			else if (!strcmp(type,"grcTexture") || !strcmp(type,"int")) {
				I.Type = 0;
				T.IgnoreToken();
			}
			else {
				Errorf("Unknown type '%s'",type);
				I.Type = 0;
			}
			for (int i=0; i<4; i++)
				I.Payload[i] = i<I.Type? T.GetFloat() : 0.0f;
			I.Referenced = I.Redundant = 0;
			// SpecularMapIntensityMask is a weird case, it's the only per-instance numeric
			// parameter GTA4 uses.  Pretend it doesn't exist so it doesn't throw off our results.
			if (I.Type && strcmp(id,"SpecularMapIntensityMask"))
				Items.PushAndGrow(I);
			T.MatchToken("}");
		}
	}

	// Save preset to file
	void Save(fiStream *S) {
		if (Shader[0])
			fprintf(S,"shader %s\r\n",Shader);
		for (int i=0; i<Items.GetCount(); i++) {
			const char *types[] = { "", "float", "Vector2","Vector3","Vector4" };
			fprintf(S,"%s {\r\n\t%s\t",Items[i].Name,types[Items[i].Type]);
			for (int j=0; j<Items[i].Type; j++)
				fprintf(S,"%s%.8g",j?" ":"",Items[i].Payload[j]);
			fprintf(S,"\r\n}\r\n");
		}
	}

	// Remove entries that match
	void Filter(Preset &that) {
		bool fullyRedundant = true;
		for (int i=0; i<Items.GetCount(); i++) {
			int otherIndex = that.Items.Find(Items[i]);
			if (otherIndex == -1)
				Errorf("Variable '%s' not in preset %s.",Items[i].Name,that.Shader);
			else if (Items[i].Type != that.Items[otherIndex].Type)
				Errorf("Variable '%s' is different type in preset %s.",Items[i].Name,that.Shader);
			else {
				int j;
				for (j=0; j<Items[i].Type && Items[i].Payload[j] == that.Items[otherIndex].Payload[j]; j++)
					;
				that.Items[otherIndex].Referenced++;
				// Fully matched, just eliminate it.
				if (j == Items[i].Type) {
					that.Items[otherIndex].Redundant++;
					Items[i].Redundant++;
				}
				// At least one thing was different.
				else 
					fullyRedundant = false;
			}
		}
		that.Referenced++;
		that.Redundant += fullyRedundant;
	}

	// Merge entries in the other shader that don't exist in this one
	void Merge(const Preset &that) {
		for (int i=0; i<that.Items.GetCount(); i++) {
			int myIndex = Items.Find(that.Items[i]);
			if (myIndex == -1)
				Items.PushAndGrow(that.Items[i]);
		}
	}

	void Dump(const char *presetName) const {
		Displayf("Preset '%s' uses shader '%s', was referenced %u times, %u unchanged",presetName,Shader,Referenced,Redundant);
		for (int i=0; i<Items.GetCount(); i++)
			Displayf("   %s was referenced %u times, %u unchanged",Items[i].Name,Items[i].Referenced,Items[i].Redundant);
	}

	char Shader[64];
	u32 Referenced, Redundant;
	struct Item {
		char Name[64-16-4-8];
		int Type;
		u32 Referenced, Redundant;
		float Payload[4];
		bool operator==(const Item &that) { return !strcmp(Name,that.Name); }
	};
	atArray<Item> Items;
};

/*
	PURPOSE:	Given a list of archive files to process, look for GTA-style independent
				assets of either drawable (idr) or fragment (ift) type.  Within those files,
				scan for entity.type files, and manually parse them to determine numeric
				shader values and compare them to a specified set of presets.

	args: x:\gta\build\independent\img.list c:\soft\rage\rad\src\gta_hacked_files\

	generate img.list with "dir/b/s *.img > img.list" in that directory.

*/
int Main()
{
	if (sysParam::GetArgCount() < 3) {
		Errorf("usage: sample_mtlscan img.list preload-dir (see source comments)");
		return 1;
	}

	struct myPackfile : public fiPackfile {
#if 0
		bool Init(const char *name) {
			return InitFromImg(name);
		}
#endif
	};

	atMap<ConstString, Preset> shaderMap;
	atMap<ConstString, Preset> presetMap;

	char buf[256];
	ASSET.PushFolder(sysParam::GetArg(2));
	fiStream *presets = ASSET.Open("preload","list");
	if (!presets) {
		Errorf("Cannot open sps preload.list in directory '%s'",sysParam::GetArg(2));
		return 1;
	}
	while (fgetline(buf,sizeof(buf),presets)) {
		fiStream *preset = ASSET.Open(buf,"");
		if (preset) {
			char *ext = strrchr(buf,'.');
			fiTokenizer T;
			T.Init(buf,preset);
			Preset sps;
			sps.Load(T);
			if (!strcmp(ext,".txt")) {
				*ext = '\0';
				shaderMap.Insert(ConstString(buf),sps);
			}
			else {
				Preset *shader = shaderMap.Access(sps.Shader);
				if (!shader)
					Errorf("Preset '%s' accesses unknown shader '%s'",buf,sps.Shader);
				else {
					sps.Merge(*shader);
					presetMap.Insert(ConstString(buf),sps);
				}
			}
			preset->Close();
		}
		else
			Errorf("Cannot open preset '%s'",buf);
	}
	presets->Close();
	ASSET.PopFolder();

	fiStream *imageList = fiStream::Open(sysParam::GetArg(1));
	if (!imageList) {
		Errorf("Cannot open input image list '%s'",sysParam::GetArg(1));
		return 1;
	}

	u32 seen = 0, seenCheck = 0;
	while (fgetline(buf,sizeof(buf),imageList)) {
		myPackfile pf;
		if (pf.Init(buf,true,fiPackfile::CACHE_NONE)) {
			fiHandle fi;
			fiFindData data;
			pf.MountAs("/outer/");
			if (fiIsValidHandle(fi = pf.FindFileBegin("/outer/",data))) {
				Displayf("Processing archive '%s'...",buf);
				do {
					const char *ext = strrchr(data.m_Name,'.');
					if (!stricmp(ext,".idr") || !stricmp(ext,"ift")) {
						// Displayf("scanning '%s'...",data.m_Name);
						char buf2[256];
						safecpy(buf2,"/outer/");
						safecat(buf2,data.m_Name);
						fiPackfile pf2;
						if (pf2.Init(buf2,true,fiPackfile::CACHE_NONE)) {
							pf2.MountAs("/inner/");
							ASSET.PushFolder("/inner");
							fiStream *S = ASSET.Open("entity","type");
							if (S) {
								// Displayf("**** %s/%s/entity.type ****",buf,data.m_Name);
								/*
								Version: 103
								shadinggroup {
									shadinggroup {
										Count 1
										Shaders 3 {
											Vers: 1
											gta_normal_spec.sps 1 oorCIA_gta_normal_spec_000.sva
								: : : (as per Shaders N above)
								*/
								fiTokenizer T;
								T.Init(data.m_Name,S);
								if (T.MatchInt("Version:") != 103)
									Errorf("%s: Bad Version",data.m_Name);
								// Scan for the shadinggroup token.
								// (fragments start with a skeleton we're too lazy to parse properly)
								char shadinggroup[256];
								while (T.GetToken(shadinggroup,sizeof(shadinggroup)) && strcmp(shadinggroup,"shadinggroup"))
									;
								T.MatchToken("{");
								T.MatchToken("shadinggroup"); T.MatchToken("{");
								if (T.MatchInt("Count") != 1)
									Errorf("%s: Bad Count",data.m_Name);
								int shaderCount = T.MatchInt("Shaders");
								T.MatchToken("{");
								if (T.MatchInt("Vers:") != 1)
									Errorf("%s: Bad subversion",data.m_Name);
								for (int i=0; i<shaderCount; i++) {
									char preset[64], svaName[256];
									T.GetToken(preset,sizeof(preset));
									if (!strchr(preset,'.'))
										safecat(preset,".sps");
									if (T.GetInt() != 1)
										Errorf("%s: Bad param count to shader",data.m_Name);
									T.GetToken(svaName,sizeof(svaName));
									fiStream *S2 = ASSET.Open(svaName,"sva");
									if (S2) {
										fiTokenizer T2;
										T2.Init(svaName,S2);
										Preset svaData;
										svaData.Load(T2);

										formatf(svaName,sizeof(svaName),"x:\\gta\\build\\dump\\%s\\%s_%03d",ASSET.FileName(buf),data.m_Name,i);
										ASSET.CreateLeadingPath(svaName);
										fiStream *S3 = ASSET.Create(svaName,"txt");
										if (S3) {
											Preset *sps = presetMap.Access(preset);
											if (!sps)
												Errorf("Material '%s' references unknown preset '%s'",svaName,preset);
											else {
												fprintf(S3,"preset %s\r\n",preset,sps->Shader);
												svaData.Filter(*sps);
											}
											svaData.Save(S3);
											S3->Close();
											++seen;
										}
										else
											Errorf("Unable to create '%s'",svaName);
										S2->Close();
									}
									else
										Errorf("%s: missing sva file?",svaName);
								}
								S->Close();
							}
							else
								Errorf("No entity.type file found in inner file '%s'",data.m_Name);
							ASSET.PopFolder();
							fiDevice::Unmount("/inner/");
						}
						else
							Errorf("Unable to mount inner archive '%s'",buf2);
					}
				} while (pf.FindFileNext(fi,data));
				pf.FindFileEnd(fi);
			}
			else
				Errorf("Root directory is empty in '%s'",buf);
			fiDevice::Unmount("/outer/");
		}
		else
			Errorf("Cannot open '%s'",buf);
	}

	imageList->Close();

	atMap<ConstString,Preset>::Iterator it = presetMap.CreateIterator();
	while (!it.AtEnd()) {
		Preset &sps = it.GetData();
		sps.Dump(it.GetKey());
		it.Next();
		seenCheck += sps.Referenced;
	}

	Displayf("%u total instances seen, %u referenced presets.",seen,seenCheck);

	return 0;
}