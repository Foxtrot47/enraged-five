// 
// sample_rmcore\sample_playerped.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE:
//		This sample shows all the bank widgets in action.


// TITLE: Drawable
// PURPOSE:
//		This sample shows how to load and draw a GTA main character.

// Useful command line besides the default one:
//
// Run the sample with these shader paths set:
// -shaderlib x:/gta/build/common/shaders -shaderdb x:/gta/build/common/shaders/db
//

#define GCM_BUFFER_SIZE	8192
#include "sample_rmcore.h"

#define ENABLE_RFS		1
#define	FAKE_DEFERRED	1

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "grmodel/shaderfx.h"
#include "grprofile/drawmanager.h"
#include "rmcore/commandbufferset.h"
#include "rmcore/drawable.h"
#include "system/cache.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grcore/vertexBuffer.h"
#include "fragment/type.h"
#include "fragment/drawable.h"
#include "file/packfile.h"
#include "phcore/materialmgrimpl.h"
#include "fragment/manager.h"
#include "grcore/edge_profile.h"
#include "paging/rscbuilder.h"

// For debug visualization:
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

using namespace rage;

PARAM(anim,"[sample_playerped] The filename of the animation file to load (default is none)");
PARAM(mesh,"[sample_playerped] The filename of a mesh to load for debug visualization");
PARAM(center, "[sample_playerped] Put the drawable at the origin");
PARAM(many, "[sample_playerped] Draw N*N copies of the object for performance testing");
PARAM(perobjectlighting, "[sample_playerped] Fake per-object lighting to stress fragment shader patching");
PARAM(file,"[sample_playerped] The filename of the .type file to load (default entity.type)");
XPARAM(edgegeom);

enum ePedVarComp
{
	PV_COMP_INVALID = -1,
	PV_COMP_HEAD = 0,
	PV_COMP_UPPR,
	PV_COMP_LOWR,
	PV_COMP_SUSE,
	PV_COMP_HAND,
	PV_COMP_FEET,
	PV_COMP_JACK,
	PV_COMP_HAIR,
	PV_COMP_SUS2,
	PV_COMP_TEEF,
	PV_MAX_COMP
};

class rmcSampleDrawable : public ragesamples::rmcSampleManager
{
public:
	virtual const char* GetSampleName() const
	{
		return "sample_playerped";
	}

#if FAKE_DEFERRED
	int m_DeferredTechGroup;
	grcRenderTarget *m_DeferredBuffer2, *m_DeferredBuffer3;
#endif//FAKE_DEFERRED

	void InitClient()
	{
#if FAKE_DEFERRED
		m_DeferredTechGroup = grmShaderFx::RegisterTechniqueGroup("deferred");
		m_DeferredBuffer2 = grcTextureFactory::GetInstance().CreateRenderTarget("db2",grcrtPermanent,
			1280,720,32,NULL);
		m_DeferredBuffer3 = grcTextureFactory::GetInstance().CreateRenderTarget("db2",grcrtPermanent,
			1280,720,32,NULL);
#endif//FAKE_DEFERRED

		char* components[PV_MAX_COMP] = 
		{
			"head_000_r/",
			"uppr_000_u/",
			"lowr_000_u/",
			"", // suse
			"hand_000_r/",
			"feet_000_u/",
			"", // jack
			"hair_000_u/",
			"", // sus2
			"" // teef
		};


		// Set this to something like 4000,0,0 to observe z fighting on skinned
		// models prior to my hackery involving world matrices.
		m_Fudge.Set(0,0,0);

		m_DebugShader = false;
		m_BlitTest = 0;

		// m_DrawGrid = m_DrawWorldAxes = false;

#if __PFDRAW
		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
		GetRageProfileDraw().SetEnabled(true);
#endif
		m_LightMode = grclmPoint;
		
		rmcSampleManager::InitClient();

		crAnimation::InitClass();

		phMaterialMgrImpl<phMaterial>::Create();

		m_FragManager = rage_new fragManager();

		// STEP #1. Allocate and load our playerped fragment.
		m_Fragment = fragType::Load("playerped/entity.type");

		memset(m_ComponentDrawable, 0, sizeof(m_ComponentDrawable));
		memset(m_TexDict, 0, sizeof(m_TexDict));

		grmModel::ResetBoundingBox();
		for (int i = 0; i != PV_MAX_COMP; ++i)
		{
			if (components[i][0] != '\0')
			{
				ASSET.PushFolder(components[i]);
				m_ComponentDrawable[i] = rage_new rmcDrawable;

				if (!m_ComponentDrawable[i]->Load("entity.type"))
				{
					delete m_ComponentDrawable[i];
					m_ComponentDrawable[i] = NULL;
				}
				ASSET.PopFolder();
			}
		
		}
		m_RenderDistance.Set(grmModel::sm_ModelMax[0]-grmModel::sm_ModelMin[0],
			grmModel::sm_ModelMax[1]-grmModel::sm_ModelMin[1],
			grmModel::sm_ModelMax[2]-grmModel::sm_ModelMin[2]);
		m_RenderCenter.Set((0.5f*grmModel::sm_ModelMax[0]+grmModel::sm_ModelMin[0]),
			0.5f*(grmModel::sm_ModelMax[1]+grmModel::sm_ModelMin[1]),
			0.5f*(grmModel::sm_ModelMax[2]+grmModel::sm_ModelMin[2]));

		// STEP #2. Initialize our skeleton.

		//-- The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		int numBones = 128;//64;
		m_FragDrawable = m_Fragment->GetCommonDrawable();
		crSkeletonData *skeldata = m_FragDrawable->GetSkeletonData();
		if (skeldata)
		{
			numBones = skeldata->GetNumBones();
			m_Skeleton = rage_new crSkeleton();

			m_Skeleton->Init(*skeldata,NULL);

			//-- In case we want to play animations on the skeleton, load an animation file
			if ( PARAM_anim.Get() ) 
			{
				const char *filename = "";
				PARAM_anim.Get(filename);
				m_Anim = crAnimation::AllocateAndLoad(filename);
			}
			else {
				m_Anim = 0;
			}
		}
		else {
			m_Skeleton = 0;
			m_Anim = 0;
		}
		// -STOP

		m_PlaybackRate = 30.0f;
		m_AutoDolly = false;
		m_AutoSlide = false;
		m_UpdateAnim = m_Anim != 0;
		m_SlideRate.Set(0.75f, 0.55f, 0.35f);
		m_RotateRate = 0.5f;
		m_SlideTimer = 0.f;
		m_RotateTimer = 0.f;
		m_FrameTimer = 0.f;
		m_VertexNormalLength = 0.02f;
		m_VertexNormalColor = Color32(1.0f,0.0f,0.0f);
		m_ModelOpacity = 1.0f;
		m_NumRenderInstancesX = 1;
		m_NumRenderInstancesY = 1;
		m_NumRenderInstancesZ = 1;
		if (PARAM_many.Get()) {
			PARAM_many.Get(m_NumRenderInstancesX);
			PARAM_many.Get(m_NumRenderInstancesZ);
		}
		m_TotalRenderInstances = m_NumRenderInstancesX * m_NumRenderInstancesY * m_NumRenderInstancesZ;
		if (m_Skeleton) {
			m_MatrixSet = grmMatrixSet::Create(*m_Skeleton);
		}
		else {
			m_MatrixSet = grmMatrixSet::Create(1);
		}

		m_PerObjectLighting = PARAM_perobjectlighting.Get();

		// command buffer:
		m_DrawUsingCommandBuffers = false;
#if COMMAND_BUFFER_SUPPORT
		m_CommandBuffers = NULL;
#endif
	} 

	void ShutdownClient()
	{
#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif
		delete m_Skeleton;
		// It's okay to call pgRscBuilder::Delete on something that isn't on the
		// resource heap, but it's not okay to just delete something that is on
		// the resource heap.
		pgRscBuilder::Delete(m_FragDrawable);
		pgRscBuilder::Delete(m_TexDict);
		delete m_Anim;

#if COMMAND_BUFFER_SUPPORT
		delete m_CommandBuffers;
#endif

		crAnimation::ShutdownClass();
	}

#if COMMAND_BUFFER_SUPPORT
	static void SetCommandBufferState()
	{
		grcState::SetAlphaBlend(true);

		grcState::SetDepthFunc(grcdfLessEqual);
		grcState::SetDepthTest(true);
		grcState::SetDepthWrite(true);
		
		grcState::SetBlendSet(grcbsNormal);
	}
#endif

	#define ASSERT(x) Assert(x)
	#define MAX(a,b)	((a) > (b) ? (a) : (b))
	#define MIN(a,b)	((a) < (b) ? (a) : (b))
	enum RollBoneAxis
	{
		X_AXIS = 0,
		Y_AXIS,
		Z_AXIS,
	};
	///////////////////////////////////////////////////////////////////////////////
	//  DoCopyRollBoneGlobal
	///////////////////////////////////////////////////////////////////////////////
	void DoCopyRollBoneGlobal(crSkeleton *pSkeleton, const int originalBoneIndex, const int rollBoneIndex, const RollBoneAxis ASSERT_ONLY(rollBoneAxis), float ASSERT_ONLY(percentage) )
	{
		// Check the axis is valid
		ASSERT(rollBoneAxis == iX_AXIS || rollBoneAxis == iY_AXIS || rollBoneAxis == iZ_AXIS);

		// Check the percentage is valid
		ASSERT(percentage >= 0.0f && percentage <= 1.0f);


		// Get the roll bone matrix (in global space)
		if (rollBoneIndex < 0)
		{
			return;
		}
		Matrix34 &rollBoneGlobalMat = pSkeleton->GetGlobalMtx(rollBoneIndex);

		// Get the original bone matrix	
		const Matrix34 &originalBoneGlobalMat = pSkeleton->GetBone(originalBoneIndex).GetGlobalMtx();

		rollBoneGlobalMat.Set(originalBoneGlobalMat);
	}


	///////////////////////////////////////////////////////////////////////////////
	//  DoRollBoneGlobal
	///////////////////////////////////////////////////////////////////////////////
	void DoRollBoneGlobal(crSkeleton *pSkeleton, const int originalBoneIndex, const int rollBoneIndex, const RollBoneAxis rollBoneAxis, float percentageRoll, bool child)
	{
		ASSERT(pSkeleton);

		// Check the axis is valid
		ASSERT(rollBoneAxis == X_AXIS || rollBoneAxis == Y_AXIS || rollBoneAxis == Z_AXIS);

		// Check the percentageRoll is valid
		ASSERT(percentageRoll >= 0.0f && percentageRoll <= 1.0f);

		// Get the roll bone matrix (in global space)
		if (rollBoneIndex < 0)
		{
			return;
		}

		Matrix34 &rollBoneGlobalMat = pSkeleton->GetGlobalMtx(rollBoneIndex);

		// Get the original bone matrix (in global space)
		const Matrix34 &originalBoneGlobalMat = pSkeleton->GetGlobalMtx(originalBoneIndex);

		// Get the parent of the original bone matrix (in global space)
		ASSERT(pSkeleton->GetSkeletonData().GetBoneData(originalBoneIndex)->GetParent());
		int parentBoneIndex = pSkeleton->GetSkeletonData().GetParentIndex(originalBoneIndex);
		const Matrix34 &parentBoneGlobalMat = pSkeleton->GetGlobalMtx(parentBoneIndex);

		// Calculate the original bone matrix (in local space)
		Matrix34 originalBoneLocalMat = originalBoneGlobalMat;
		originalBoneLocalMat.Dot3x3Transpose(parentBoneGlobalMat);

		//
		// Calculate the new rotation and position of the roll bone (in local space)
		//

		// Convert the original bones rotation from matrix to Euler angle form
		Vector3 rotationInEulers = originalBoneLocalMat.GetEulers();

		// Rotate the chosen axis by the specified percentage while conserving the other two axes
		switch(rollBoneAxis)
		{
		case X_AXIS:
			if (child)
			{
				rotationInEulers.x *= percentageRoll;
				rotationInEulers.x += 3.14159265358979323846264338327950288f;

				rotationInEulers.x = MAX(-1500.0f, rotationInEulers.x);
				rotationInEulers.x = MIN(1500.0f, rotationInEulers.x);

				while (rotationInEulers.x >= 180.0f)
					rotationInEulers.x -= 360.0f;

				while (rotationInEulers.x < -180.0f)
					rotationInEulers.x += 360.0f;

				rotationInEulers.y = 0.0f;
				rotationInEulers.z = 0.0f;
			}
			else
			{
				rotationInEulers.x *= percentageRoll;
			}
			break;

		default:
			break;
		}

		// Convert the new rotation from Euler angles to matrix form
		rollBoneGlobalMat.FromEulersXYZ(rotationInEulers);

		// Transform into global space
		rollBoneGlobalMat.Dot3x3(parentBoneGlobalMat);

		if (!child)
		{
			// Retain the original position of the roll bone (in global space)
			rollBoneGlobalMat.d = originalBoneGlobalMat.d;
		}
	}


	///////////////////////////////////////////////////////////////////////////////
	//  DoRollBones
	///////////////////////////////////////////////////////////////////////////////
	void DoRollBones(crSkeleton *pSkeleton)
	{
		float fCalfRoll		= 1.00000000f;
		float fUpperArmRoll	= 0.150000006f;	
		float fForeArmRoll	= 1.00000000f;	
		float fArmRoll		= 0.750000000f;	
		float fNeckRoll		= 1.00000000f;

		int nCOMMON_PED_BONE_HEAD = 17;
		int nCOMMON_PED_BONE_NECKROLL = 43;

		int nCOMMON_PED_BONE_L_UPPERARM = 45;
		int nCOMMON_PED_BONE_L_ARMROLL = 67;

		int nCOMMON_PED_BONE_R_UPPERARM = 69;
		int nCOMMON_PED_BONE_R_ARMROLL = 89;

		int nCOMMON_PED_BONE_L_FOREARM = 46;
		int nCOMMON_PED_BONE_L_FORETWIST = 65;

		int nCOMMON_PED_BONE_R_FOREARM	= 70;
		int nCOMMON_PED_BONE_R_FORETWIST = 87;

		//int nCOMMON_PED_BONE_L_UPPERARM = 45;
		int nCOMMON_PED_BONE_L_UPPPERARMROLL = 66;

		//int nCOMMON_PED_BONE_R_UPPERARM = 69;
		int nCOMMON_PED_BONE_R_UPPPERARMROLL = 88;

		int nCOMMON_PED_BONE_L_CALF = 3;
		int nCOMMON_PED_BONE_L_CALFROLL = 6;

		int nCOMMON_PED_BONE_R_CALF = 8;
		int nCOMMON_PED_BONE_R_CALFROLL = 11;

		ASSERT(pSkeleton);
		// 17 38
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_HEAD, nCOMMON_PED_BONE_NECKROLL, X_AXIS, fNeckRoll, false); 

		// 45 - 67
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_L_UPPERARM, nCOMMON_PED_BONE_L_ARMROLL, X_AXIS, fArmRoll, false); 

		// 69 - 89
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_R_UPPERARM, nCOMMON_PED_BONE_R_ARMROLL, X_AXIS, fArmRoll, false); 

		// 46 - 65
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_L_FOREARM, nCOMMON_PED_BONE_L_FORETWIST, X_AXIS, fForeArmRoll, false); 

		// 70 - 87
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_R_FOREARM, nCOMMON_PED_BONE_R_FORETWIST, X_AXIS, fForeArmRoll, false); 

		// 45 - 66
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_L_UPPERARM, nCOMMON_PED_BONE_L_UPPPERARMROLL, X_AXIS, fUpperArmRoll, true);

		// 69 - 88
		DoRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_R_UPPERARM, nCOMMON_PED_BONE_R_UPPPERARMROLL, X_AXIS, fUpperArmRoll, true);		

		// 3 - 6
		DoCopyRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_L_CALF, nCOMMON_PED_BONE_L_CALFROLL, X_AXIS, fCalfRoll);

		// 8 - 11
		DoCopyRollBoneGlobal(pSkeleton, nCOMMON_PED_BONE_R_CALF, nCOMMON_PED_BONE_R_CALFROLL, X_AXIS, fCalfRoll);
	}

	void UpdateClient()
	{
		//-- If we want an animation, update it here
		//Note : We don't want to update the animation if vertex normals are being drawn, so the animation resumes from the
		//frame when the draw normalss button was pressed.
		if ( m_UpdateAnim ) 
		{
			m_FrameTimer += TIME.GetSeconds() * m_PlaybackRate;
		}
		
		m_Skeleton->Reset();
		
		if ( m_Anim ) 
		{
			crFrame frameBuf;
			frameBuf.InitCreateBoneAndMoverDofs(m_Skeleton->GetSkeletonData(), false);
			frameBuf.IdentityFromSkel(m_Skeleton->GetSkeletonData());
			m_Anim->CompositeFrame(fmodf(m_Anim->Convert30FrameToTime(m_FrameTimer), m_Anim->GetDuration()), frameBuf);
			frameBuf.Pose(*m_Skeleton);
		}
		
		int nCOMMON_PED_BONE_HEAD = 17;
		m_Skeleton->PartialReset(nCOMMON_PED_BONE_HEAD);

		m_Center.Identity();

		//-- If we want to update orientation, do it here
		if ( m_AutoDolly ) {
			m_RotateTimer += TIME.GetSeconds();
		}
		m_Center.RotateLocalY( Wrap(m_RotateTimer * m_RotateRate, 0.f, 2.f * PI) );
		
		//-- If we want to update position, do it here
		if ( m_AutoSlide ) { 
			m_SlideTimer += TIME.GetSeconds();
		}
		m_Center.d.x = m_FragDrawable->GetLodGroup().GetCullRadius() * 2.0f * sinf(m_SlideTimer * m_SlideRate.x);
		m_Center.d.z = m_FragDrawable->GetLodGroup().GetCullRadius() * 2.0f * -sinf(m_SlideTimer * m_SlideRate.z);
		m_Center.d.y = m_FragDrawable->GetLodGroup().GetCullRadius() * sinf(m_SlideTimer * m_SlideRate.y);

		if (PARAM_center.Get()) {
			m_Center.d = -m_FragDrawable->GetLodGroup().GetCullSphere();
		}


		m_Center.d += m_Fudge;


		//-- If we have a skeleton, inform it of it's new position & update all child bones
		if ( m_Skeleton ) {
			m_Skeleton->SetParentMtx(&m_Center);
			m_Skeleton->Update();
			DoRollBones(m_Skeleton);
			
			m_MatrixSet->Update(*m_Skeleton,true,false);
		}
	}

	void InitCamera()
	{
		m_OrthoCam.InitViews();
		InitSampleCamera(m_Fudge - m_RenderCenter + Vector3(0.f, 2.5f, -5.f) , m_Fudge);
	}
	void DrawClient()
	{
		grcEffectGlobalVar debugShader = grcEffect::LookupGlobalVar("debugShader", false);
		if (debugShader)
			grcEffect::SetGlobalVar(debugShader,m_DebugShader);
		
#if __PFDRAW
		if(m_Skeleton)
			m_Skeleton->ProfileDraw(0.05f);
		if(m_FragDrawable)
			m_FragDrawable->ProfileDraw(m_MatrixSet, &m_Center);

		const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
		GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
		GetRageProfileDraw().Render();
#endif


#if FAKE_DEFERRED
		// use deferred_ entry points for now
		const rage::grcRenderTarget* renderTargets[4];
		grmShaderFx::SetForcedTechniqueGroupId(m_DeferredTechGroup);
		renderTargets[0]=grcTextureFactory::GetInstance().GetBackBuffer();
		renderTargets[1]=m_DeferredBuffer2;
		renderTargets[2]=m_DeferredBuffer3;
		renderTargets[3]=NULL;
		grcTextureFactory::GetInstance().LockMRT(renderTargets, 
			grcTextureFactory::GetInstance().GetBackBufferDepth());
		grcState::SetAlphaBlend(false); // TODO: ??? 
#endif //FAKE_DEFERRED

		m_DrawableDraw = 0.0f;
		m_TotalRenderInstances = m_NumRenderInstancesX * m_NumRenderInstancesY * m_NumRenderInstancesZ;

		{
			grcLightGroup group1 = m_Lights, group2 = m_Lights;
			group1.SetColor(0, 1,0,0);
			group2.SetColor(0, 0,1,0);

			for (int x=0;x<m_NumRenderInstancesX;x++)
			{
				for (int y=0;y<m_NumRenderInstancesY;y++)
				{
					for (int z=0;z<m_NumRenderInstancesZ;z++)
					{
						if (m_PerObjectLighting)
							grcState::SetLightingGroup((x+y+z)&1? group2 : group1);
						Matrix34 mtx;
						mtx.Identity3x3();
						mtx.d.Set(x*m_RenderDistance.x,y*m_RenderDistance.y,z*m_RenderDistance.z);
		
						// STEP #3. Render our character.
						u8 lod;
						//-- First we do a visibility check versus the view frustrum.  If it 
						// is visible, the level of detail is returned in <c>lod</c>.
						if (m_FragDrawable->IsVisible(m_Center,*grcViewport::GetCurrent(),lod)) {

							//-- Here we set the texture LOD based on the character's distance
							// from the viewer.
							float zDist = GetCamMgr().GetWorldMtx().d.Mag();
							grcTextureFactory::GetInstance().SetTextureLod(zDist);

							if (!m_DrawUsingCommandBuffers)
							{
								//-- Draw the character. 
								for (int i = 0; i < PV_MAX_COMP;++i)
								{
									if (m_ComponentDrawable[i])
									{
										sysTimer drawableDraw;
										for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
											if (GetBucketEnable(bucket) && (m_ComponentDrawable[i]->GetBucketMask(lod) & (1 << bucket))) 
											{
												// bool bDrawSkinned = m_Drawable->GetLodGroup().GetLodModel0(lod).GetSkinFlag();
												if (m_FragDrawable->GetSkeletonData())
													m_ComponentDrawable[i]->DrawSkinned(&mtx,*m_MatrixSet,bucket,lod);
												else
													m_ComponentDrawable[i]->Draw(m_Center,bucket,lod); 
											}
											m_DrawableDraw += drawableDraw.GetMsTime();
									}
								}
								
								// -STOP
							}


							/*if (m_Mesh && GetBucketEnable(NUM_BUCKETS-1)) { 
								grcBindTexture(NULL); 
								mshRendererGrc renderer;
								if (m_Drawable->GetSkeletonData()) {
									Matrix43 *mtx = Alloca(Matrix43,m_Drawable->GetSkeletonData()->GetNumBones());
									m_Skeleton->Attach(true, mtx);
									for (int i=0; i<m_Drawable->GetSkeletonData()->GetNumBones(); i++) {
										grcDrawAxis(.2f,mtx[i]);
									}
									grcWorldIdentity();
									m_Mesh->DrawSkinned(renderer,mtx);
								}
								else
									m_Mesh->Draw(renderer);
							}*/
							// -STOP
						}
					}
				}
			}
		}

		// return to normal fwd rendering
#if FAKE_DEFERRED
		grmShaderFx::SetForcedTechniqueGroupId(-1);
		grcTextureFactory::GetInstance().UnlockMRT();
#endif //FAKE_DEFERRED

		grcState::Default();
		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcFont::GetCurrent().Drawf(100,200,"drawable: %5.2fms",m_DrawableDraw);		
		grcViewport::SetCurrent(old);
		edgeProfilerDraw(400, 100);
	}

#if __BANK
	static void EnableAllShaders()
	{
	}

	void UpdateModelOpacity()
	{
		const Vector4 &curAmbientColor = m_Lights.GetAmbient();
		m_Lights.SetAmbient(curAmbientColor.x, curAmbientColor.y, curAmbientColor.z, m_ModelOpacity);
	}

	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
		
		m_FragDrawable->GetShaderGroup().AddWidgets(BANKMGR.CreateBank("Shaders"));
		bkBank &bk = BANKMGR.CreateBank("rage - SampleDrawable");
		bk.AddToggle("Debug Shader",&m_DebugShader,NullCallback,"Toggle 'shaderDebug' global shader bool (if present)");
		bk.AddSlider("Blit Test",&m_BlitTest,0,16,1.0f,NullCallback,"Take the first shader in the test case and do N screen blits to test fragment program thruput");
		bk.AddToggle("Auto Dolly", &m_AutoDolly);
		bk.AddSlider("Rotation Rate", &m_RotateRate, -10000.f, 10000.f, 0.01f);
		bk.AddToggle("Auto Slide", &m_AutoSlide);
		bk.AddSlider("Slide Rate", &m_SlideRate, -10000.0f, 10000.0f, 0.01f);
		if ( m_Anim ) {
			bk.AddToggle("Update Animation", &m_UpdateAnim);
			bk.AddSlider("Animation Rate (fps)", &m_PlaybackRate, -1000.0, 1000.0f, 0.01f);
		}
		bk.AddSlider("Model Opacity", &m_ModelOpacity, 0.0f, 1.0f, 0.1f, datCallback(MFA(rmcSampleDrawable::UpdateModelOpacity),this));
		bk.AddButton("Enable All Shaders", (CFA(rmcSampleDrawable::EnableAllShaders)));

		bk.PushGroup("Multiple Instances",false,"Group of widgets for displaying multiple instances of the current drawable");
		bk.AddSlider("Num Render Instances X",&m_NumRenderInstancesX,0,1000,1);
		bk.AddSlider("Num Render Instances Y",&m_NumRenderInstancesY,0,1000,1);
		bk.AddSlider("Num Render Instances Z",&m_NumRenderInstancesZ,0,1000,1);
		bk.AddSlider("Total # of Instances",&m_TotalRenderInstances,0,1000*1000*1000,0);

		bk.AddSlider("Render Distance",&m_RenderDistance,0.0f,10000.0f,0.5f);   
		bk.AddSlider("Render Fudge",&m_Fudge,-100000,100000,1);
		bk.PopGroup();

#if COMMAND_BUFFER_SUPPORT
		bk.AddToggle("Draw Using Command Buffers", &m_DrawUsingCommandBuffers);
#endif
	}
#endif

private:

	fragType				*m_Fragment;
	rmcDrawable				*m_ComponentDrawable[PV_MAX_COMP];
	fragDrawable			*m_FragDrawable;
	fragManager				*m_FragManager;
	pgDictionary<grcTexture>*m_TexDict[PV_MAX_COMP];
	crSkeleton*				m_Skeleton;
	crAnimation*			m_Anim;
	Matrix34				m_Center;
	Vector3					m_SlideRate;
	mshMesh*				m_Mesh;
	float					m_PlaybackRate;
	float					m_RotateRate;
	float					m_SlideTimer;
	float					m_RotateTimer;
	float					m_FrameTimer;
	float					m_DrawableDraw;
	float					m_VertexNormalLength;
	Color32					m_VertexNormalColor;
	bool					m_AutoDolly;
	bool					m_AutoSlide;
	bool					m_UpdateAnim;
	float					m_ModelOpacity;
	int						m_NumRenderInstancesX;
	int						m_NumRenderInstancesY;
	int						m_NumRenderInstancesZ;
	int						m_TotalRenderInstances;
	Vector3					m_RenderDistance;
	Vector3					m_RenderCenter;
	int						m_BlitTest;

	Vector3					m_Fudge;

	bool					m_DrawUsingCommandBuffers;
	bool					m_PerObjectLighting;
	bool					m_DebugShader;

	grmMatrixSet			*m_MatrixSet;
#if COMMAND_BUFFER_SUPPORT
	// command buffers:
	rmcCommandBufferSet*	m_CommandBuffers;
#endif
};

#include "data/struct.h"

// main application
int Main()
{
	rmcSampleDrawable sampleRmc;
	sampleRmc.Init("sample_rmcore\\playerped");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
