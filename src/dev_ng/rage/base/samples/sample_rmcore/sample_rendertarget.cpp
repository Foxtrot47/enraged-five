// 
// sample_rmcore\sample_rendertarget.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE:
//		This sample demonstrates how to create and use a rendertarget
//		with RAGE.  Also demonstrated is a method to take an existing
//		rendertarget and use it as a texture (i.e. in a following pass
//		or on another model).


// TITLE: Rendertarget
// PURPOSE:
//		This sample how to create and use rendertargets.

#include "sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "file/asset.h"
#include "grcore/quads.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

PARAM(file,"[sample_rendertarget] Takes 2 values: value0 is ModelA's filename, value1 is ModelB's filename (default is gent.type,box.type)");


class rmcSampleRendertarget : public ragesamples::rmcSampleManager
{
public:
	rmcSampleRendertarget() :
		m_RenderTgtImage(0),
		m_RenderTgtImageO1(0),
		m_RenderTgtImageO2(0),
		m_RenderTgtImageO3(0),
		m_RenderTgtDepth(0),
		m_RenderTgtPool(kRTPoolIDInvalid),
		m_DrawableA(0),
		m_DrawableB(0),
		m_SkeletonA(0),
		m_SkeletonB(0),
		m_MatrixSetA(0),
		m_MatrixSetB(0),
		m_ShowRenderTarget(false),
		m_ShowDepthTarget(false)
		{

		// Override default names	
		SetMinFiles(2);
		SetMaxFiles(2);
		SetDefaultFileName(0, "testchar.type");
		SetDefaultFileName(1, "box.type");
	}
	
protected:
	void InitClient()
	{
		m_ShowRenderTarget = true;

		rmcSampleManager::InitClient();

		// STEP # 0 create a render target pool
		grcRTPoolCreateParams poolParams;
#if __PS3	
		poolParams.m_Size = gcm::GetSharedSurfaceSize(	256,	// width
														256,	// height
														32,		// bits per pixel
														1U,		// Mip count
														1U,		// Face count
														true,	// local mem
														true,	// tiled memory
														false,	// swizzled
														false,	// Is a cube map?
														false,	// zcull
														1,		// Number of surfaces (i.e. 2 for double buffered)
														NULL),	// memory offsets
		poolParams.m_Type = grcrtPermanent;
		poolParams.m_Pitch = gcm::GetSurfacePitch(256,32,false);
		poolParams.m_Alignment = 65536;
#else
		poolParams.m_Size = 256*256*4; 
#endif

		m_RenderTgtPool = grcRenderTargetPoolMgr::CreatePool("Color RT Pool", poolParams);

		// STEP #1. Create any rendertargets you need.

		//-- We want to reuse this rendertarget as a texture, so we are specifying a unique
		// name that does not exist on disk.
		grcTextureFactory::CreateParams rtParams(m_RenderTgtPool);
		m_RenderTgtImage = grcTextureFactory::GetInstance().CreateRenderTarget("__test_tgt", grcrtPermanent, 256, 256, 32, &rtParams);

		//-- These targets ares assigned to the same pool as the first one, we will overlap them in memory, so this one take no addition memory
		m_RenderTgtImageO1 = grcTextureFactory::GetInstance().CreateRenderTarget("OverlapTarget1",grcrtPermanent, 128, 128, 32, &rtParams);
		
		m_RenderTgtImageO2 = grcTextureFactory::GetInstance().CreateRenderTarget("OverlapTarget2",grcrtPermanent, 128, 128, 32, &rtParams);

		// this target will use a specific offset into the pool
		rtParams.PoolOffset = (256-64)*256*4;
		m_RenderTgtImageO3 = grcTextureFactory::GetInstance().CreateRenderTarget("OffsetTarget",grcrtPermanent, 64, 64, 32, &rtParams);

		// Create a depth buffer -- 
		// Since it was not created in a pool, it is always implicitly allocated.
		m_RenderTgtDepth = grcTextureFactory::GetInstance().CreateRenderTarget("__test_depth", grcrtDepthBuffer, 256, 256, 32);

		// STEP #2. Register your custom rendertargets with the texture dictionary.

		//-- Any further requests to load a texture with a filename matching what you pass in, will
		// automatically receive a pointer to the rendertarget.

		//-- For this sample, we're only going to register our simple render target image
		grcTextureFactory::GetInstance().RegisterTextureReference(m_RenderTgtImage->GetName(), m_RenderTgtImage);

		// STEP #3.  Load any models that use that render target.
		
		//-- If loading from a .type file that contains texture file
		// names, any texture filenames matching the custom name you registered will automatically
		// link to the custom rendertarget.
		
		//-- This model is used to render into our rendertarget
		m_DrawableA = rage_new rmcDrawable();
		if( !m_DrawableA->Load(GetFileName(0)) )
			Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );

		//-- This model knows about the rendertarget due to the fact that one of the
		// textures it tries to laod is called "__test_tgt".
		m_DrawableB = rage_new rmcDrawable();
		if( !m_DrawableB->Load(GetFileName(1)) )
			Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(1) );
		// -STOP
		
		// If any of our models need a skeleton, go ahead and create them
		int numBones = 64;
		crSkeletonData *skeldata = m_DrawableA->GetSkeletonData();
		if (skeldata)
			numBones = skeldata->GetNumBones();
		m_SkeletonA = rage_new crSkeleton();
		m_MatrixSetA = grmMatrixSet::Create(numBones);
		if (skeldata) {
			m_SkeletonA->Init(*skeldata,NULL);
			m_MatrixSetA->Update(*m_SkeletonA,true);
		}
		skeldata = m_DrawableB->GetSkeletonData();
		if (skeldata)
			numBones = skeldata->GetNumBones();
		m_SkeletonB = rage_new crSkeleton();
		m_MatrixSetB = grmMatrixSet::Create(numBones);
		if (skeldata) {
			m_SkeletonB->Init(*skeldata,NULL);
			m_MatrixSetB->Update(*m_SkeletonB,true);
		}
	}

	void ShutdownClient()
	{
		m_RenderTgtImage->Release();
		m_RenderTgtImageO1->Release();
		m_RenderTgtImageO2->Release();
		m_RenderTgtImageO3->Release();
		m_RenderTgtDepth->Release();
		delete m_SkeletonA;
		delete m_SkeletonB;
		delete m_MatrixSetA;
		delete m_MatrixSetB;
		delete m_DrawableA;
		delete m_DrawableB;
	}

	void DrawClient()
	{
		// Step #4. Set the rendertarget
		
		// Lock the target for reading or writing.
		// This allocates the render target from the pool.
		m_RenderTgtImage->AllocateMemoryFromPool();    
		
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTgtImage, m_RenderTgtDepth);

		// Step #5. Render whatever you need into the rendertarget
		//-- We're going to make sure the target is cleared first
		// Do not clear color target if we're doing depth-only rendering.
		GRCDEVICE.Clear(true,Color32(50,50,50),true,1.0f,0);

		// Force a better nearclip so we can see the depth render target
		grcViewport::GetCurrent()->SetNearClip(0.1f);

		//-- We're using a custom camera for our rendertarget, so we need to store the current camera's settings
		Matrix34 mtx(M34_IDENTITY);
		mtx.d.y = 1.f;
		mtx.d.z = 1.5f;
		mtx.RotateFullY(TIME.GetElapsedTime() * 0.4f);	// Spin camera
		Mat34V oldCam(grcViewport::GetCurrent()->GetCameraMtx());
		grcViewport::GetCurrent()->SetCameraMtx(RCC_MAT34V(mtx));
		
		//-- Render our model A into the render target.  See sample_drawable for more info on how
		// to render a drawable
		Matrix34 center;
		center.Identity();
		u8 lod;
		if (m_DrawableA->IsVisible(center,*grcViewport::GetCurrent(),lod)) {
			float zDist = GetCamMgr().GetWorldMtx().d.Mag();
			grcTextureFactory::GetInstance().SetTextureLod(zDist);
			for (int bucket=0; bucket<NUM_BUCKETS; bucket++) {
				if (GetBucketEnable(bucket)) {
					if (m_DrawableA->GetSkeletonData()) {
						m_DrawableA->DrawSkinned(center,*m_MatrixSetA,BUCKETMASK_GENERATE(bucket),lod);
					}
					else {
						m_DrawableA->Draw(center,BUCKETMASK_GENERATE(bucket),lod);
					}
				}
			}
		}
		
		// STEP #6. Unlock the rendertarget for rendering into

		//-- This also makes the rendertarget ready for use in a model		
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		//-- Restore any cameras previously screwed with
		grcViewport::GetCurrent()->SetCameraMtx(oldCam);
		
		// STEP #7. Render everything that needs that particular render target
		
		//-- Here, we'll render model B, since it is assumed that model B will use
		// the modified render target as a texture.  Again, nothing new here -- this code
		// is the same as sample_drawable
		if (m_DrawableB->IsVisible(center,*grcViewport::GetCurrent(),lod)) {
			float zDist = GetCamMgr().GetWorldMtx().d.Mag();
			grcTextureFactory::GetInstance().SetTextureLod(zDist);
			for (int bucket=0; bucket<NUM_BUCKETS; bucket++) {
				if (GetBucketEnable(bucket)) {
					if (m_DrawableB->GetSkeletonData()) {
						m_DrawableB->DrawSkinned(center,*m_MatrixSetB,BUCKETMASK_GENERATE(bucket),lod);
					}
					else {
						m_DrawableB->Draw(center,BUCKETMASK_GENERATE(bucket),lod);
					}
				}
			}
		}
		// -STOP

		// If we want to debug the render target, we can render it to the screen as well
		PUSH_DEFAULT_SCREEN();
		if ( m_ShowRenderTarget ) {
			grcBindTexture( m_RenderTgtImage);
			grcBeginQuads(1);
			grcDrawQuadf(32, 32, 32+256, 32+256, 0.f, 0, 0, 1, 1, Color32(255,255,255));
			grcEndQuads();
		}
		if (m_ShowDepthTarget) {
			grcBindTexture( m_RenderTgtDepth);
			grcBeginQuads(1);
			grcDrawQuadf(256, 23, 512, 32+256, 0.f, 0, 0, 1, 1, Color32(255,255,255));
			grcEndQuads();
		}
		POP_DEFAULT_SCREEN();

		// Unlock the target for reading or writing.
		// This frees the render target pool memory for other targets to use it
		m_RenderTgtImage->ReleaseMemoryToPool();   
		
		// lock the overlap targets so they get to use the render target pool memory now
		m_RenderTgtImageO1->AllocateMemoryFromPool();
		m_RenderTgtImageO2->AllocateMemoryFromPool();

		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);

		// now we use a second render target that overlaps with the first (in the same pool)
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTgtImageO1, NULL);
		GRCDEVICE.Clear(true,Color32(255,0,0,255),false,1.0f,0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTgtImageO2, NULL);
		GRCDEVICE.Clear(true,Color32(0,255,255,255),false,1.0f,0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);


		if ( m_ShowRenderTarget ) {
			PUSH_DEFAULT_SCREEN();
			grcBindTexture( m_RenderTgtImageO1);
			grcBeginQuads(1);
			grcDrawQuadf(32, 256+32, 32+128, 256+32+128, 0.f, 0, 0, 1, 1, Color32(255,255,255,255));
			grcEndQuads();
			grcBindTexture( m_RenderTgtImageO2);
			grcBeginQuads(1);
			grcDrawQuadf(32+128, 256+32, 32+128+128, 256+32+128, 0.f, 0, 0, 1, 1, Color32(255,255,255,255));
			grcEndQuads();
			POP_DEFAULT_SCREEN();
		}

		m_RenderTgtImageO1->ReleaseMemoryToPool();
		m_RenderTgtImageO2->ReleaseMemoryToPool();
		
		// clear the direct offset render target with blue.
		// NOTE: since it specifies it's offset directly, we could just leave it locked, since it does not affect pool memory allocation.
		m_RenderTgtImageO3->AllocateMemoryFromPool();
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTgtImageO3, NULL);
		GRCDEVICE.Clear(true,Color32(0,0,255,255),false,1.0f,0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		m_RenderTgtImageO3->ReleaseMemoryToPool();

		if ( m_ShowRenderTarget ) {
			PUSH_DEFAULT_SCREEN();
			// Lock the main target again, so we can show the final resulting memory.
			m_RenderTgtImage->AllocateMemoryFromPool();		
			grcBindTexture( m_RenderTgtImage); 
			grcBeginQuads(1);
			grcDrawQuadf(32, 256+48+128, 32+256, 256+48+128+256, 0.f, 0, 0, 1, 1, Color32(255,255,255));
			grcEndQuads();
			m_RenderTgtImage->ReleaseMemoryToPool();

			m_RenderTgtImageO3->AllocateMemoryFromPool();		
			grcBindTexture( m_RenderTgtImageO3); 
			grcBeginQuads(1);
			grcDrawQuadf(256+32, 96+256+48+128, 256+32+64, 96+256+48+128+64, 0.f, 0, 0, 1, 1, Color32(255,255,255));
			grcEndQuads();
			m_RenderTgtImageO3->ReleaseMemoryToPool();    

			// Lock the main target again, so we can show the final resulting memory.
			m_RenderTgtImage->AllocateMemoryFromPool();		
			grcBindTexture( m_RenderTgtImage); 
			grcBeginQuads(1);
			grcDrawQuadf(32, 256+48+128, 32+256, 256+48+128+256, 0.f, 0, 0, 1, 1, Color32(255,255,255));
			grcEndQuads();
			m_RenderTgtImage->ReleaseMemoryToPool();

			POP_DEFAULT_SCREEN();	
		}
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
	}

#if __BANK
	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
		bkBank &appBk = BANKMGR.CreateBank("Render Target");
		appBk.AddToggle("Show Render Target", &m_ShowRenderTarget );
		appBk.AddToggle("Show Depth Target", &m_ShowDepthTarget );
		// Drawable Widgets:
		bkBank &bk = BANKMGR.CreateBank("Shaders");
			bk.PushGroup("Model A");
				m_DrawableA->GetShaderGroup().AddWidgets(bk);
			bk.PopGroup();
			bk.PushGroup("Model B");
				m_DrawableB->GetShaderGroup().AddWidgets(bk);
			bk.PopGroup();
	}
#endif

	virtual const char* GetSampleName() const
	{
		return "RenderTarget Sample";
	}

private:
	grcRenderTarget*		m_RenderTgtImage;
	grcRenderTarget*		m_RenderTgtImageO1;
	grcRenderTarget*		m_RenderTgtImageO2;
	grcRenderTarget*		m_RenderTgtImageO3;
	grcRenderTarget*		m_RenderTgtDepth;
	u16						m_RenderTgtPool;
	rmcDrawable*			m_DrawableA;
	rmcDrawable*			m_DrawableB;
	crSkeleton*				m_SkeletonA;
	crSkeleton*				m_SkeletonB;
	grmMatrixSet*			m_MatrixSetA;
	grmMatrixSet*			m_MatrixSetB;
	bool					m_ShowRenderTarget;
	bool					m_ShowDepthTarget;
};

// main application
int Main()
{
	rmcSampleRendertarget sampleRmc;
	sampleRmc.Init("sample_rmcore/rendertarget");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
