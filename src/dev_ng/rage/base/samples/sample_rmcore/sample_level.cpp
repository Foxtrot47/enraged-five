// 
// sample_rmcore\sample_level.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Level Sample
// PURPOSE:
//		This sample shows how to load and draw a simple level

//		Suggested usage: -shaderpath=t:\pong\assets -file=t:\pong\assets\level\arena_brazil%02d

#include "sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "file/asset.h"
#include "grmodel/setup.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#if __XENON || __WIN32PC
#include "system/xtl.h"
#include <d3d9.h>
#endif

using namespace rage;

PARAM(file,"[sample_level] The pathname of the entity.type files");


class rmcSampleLevel: public ragesamples::rmcSampleManager
{
protected:
	void InitClient()
	{
		m_LightMode = true;
		
		rmcSampleManager::InitClient();

		const char *file = NULL;
		PARAM_file.Get(file);
		Assert(file);

		int fileCount = 1;
		if (strchr(file,'%')) {
			char buf[256];
			do {
				formatf(buf,sizeof(buf),file,fileCount);
				safecat(buf,"/entity",sizeof(buf));
			} while (ASSET.Exists(buf,"type") && ++fileCount);
			--fileCount;
		}
		Assert(fileCount);

		m_Drawables.Reserve(fileCount);

		for (int i=1; i<=fileCount; i++) {
			char buf[256];
			formatf(buf,sizeof(buf),file,i);
			ASSET.PushFolder(buf);
			Displayf("Loading '%s'...",buf);
			(m_Drawables.Append() = rage_new rmcDrawable)->Load("entity.type");
			ASSET.PopFolder();
		}

		m_Center.Identity();
		m_SphereCull = false;
		m_SortByState = true;
		m_EnableZPass = false;		// About 3ms slower on my test case!
	}

	void ShutdownClient()
	{
		for (int i=0; i<m_Drawables.GetCount(); i++)
			delete m_Drawables[i];
	}

	void UpdateClient()
	{
		//-- If we want an animation, update it here
	}

	void DelayTest()
	{
		sysTimer T;
		u64 silly = 0;
		while (T.GetMsTime() < 1.0f)
			++silly;
	}

	void DrawClient()
	{
		using namespace grcStateBlock;

		sysTimer drawableDraw;

		for (int bucket=0; bucket<NUM_BUCKETS; bucket++) {
#if __XENON
			if (!bucket && m_EnableZPass)
				GRCDEVICE.BeginZPass();
#endif
			for (int i=0; i<m_Drawables.GetCount(); i++)
				if (GetBucketEnable(bucket) && (m_Drawables[i]->GetBucketMask(LOD_HIGH) & (1<<bucket)))
					m_Drawables[i]->Draw(m_Center,bucket,LOD_HIGH);
#if __XENON
			if (!bucket && m_EnableZPass)
				GRCDEVICE.EndZPass();
#endif
		}
		float drawTime = drawableDraw.GetUsTime();

		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		static float accumTime;
		if (!accumTime)
			accumTime = drawTime;
		else
			accumTime = 0.99f * accumTime + 0.01f * drawTime;
		grcFont::GetCurrent().Drawf(100,200,"drawable: %5.2fus",accumTime);
		grcViewport::SetCurrent(old);
	}

#if __BANK
	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();

		bkBank &B = BANKMGR.CreateBank("Tester");
		B.AddToggle("Sphere Cull",&m_SphereCull);
		B.AddToggle("Sort by State",&m_SortByState);
		B.AddToggle("Enable Z Pass",&m_EnableZPass);

		for (int i=0; i<m_Drawables.GetCount(); i++)
			m_Drawables[i]->GetShaderGroup().AddWidgets(BANKMGR.CreateBank("Shaders"));
	}
#endif

private:
	atArray< datOwner<rmcDrawable> > m_Drawables;
	Matrix34				m_Center;
	// bool					m_SortByShader;
	bool					m_SphereCull;
	bool					m_SortByState;
	bool					m_EnableZPass;
};


// main application
int Main()
{
	rmcSampleLevel sampleRmc;
	sampleRmc.Init("");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
