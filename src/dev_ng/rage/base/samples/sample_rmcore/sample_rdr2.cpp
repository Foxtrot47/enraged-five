// 
// sample_rmcore\sample_rdr2.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// TITLE: RDR2 Level Sample
// PURPOSE:
//		This sample shows how to load and draw a simple level

#include "sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "file/asset.h"
#include "file/token.h"
#include "grmodel/setup.h"
#include "grmodel/shadersorter.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "paging/dictionary.h"

using namespace rage;

PARAM(file,"[sample_rdr2] The pathname of the .reslist file");
PARAM(preload,"[sample_rdr2] Preload all data before starting");

static int s_InstanceCount;

class rmcSampleLevel: public ragesamples::rmcSampleManager
{
	struct Txd_t {
		const char *Name;
		pgDictionary<grcTexture> *Dictionary;
		~Txd_t() { if (Dictionary) Dictionary->Release(); StringFree(Name); }
	};
	struct Drawable_t {
		const char *Name;
		int Txd;
		rmcDrawable *Drawable;
		~Drawable_t() { if (Drawable) Drawable->Release(); StringFree(Name); }
	};
	struct Instance_t {
		Matrix34 Matrix;
		int Drawable;
	};

	atArray<Txd_t> m_Txds;
	atArray<Drawable_t> m_Drawables;
	atArray<Instance_t> m_Instances;

	const char *m_District;
	const char *m_Level;
	Vector3 m_StartPos;

protected:
	u32 get_file_size(const char *name,const char *ext) {
		fiStream *S = ASSET.Open(name,ext,true,true);
		u32 result = 0;
		if (S) {
			result = S->Size();
			S->Close();
		}
		return result;
	}

	void RecurseChildren(fiStream *output,const char *level,const char *district,const Matrix34 &xform)
	{
		ASSET.PushFolder(level);
		ASSET.PushFolder(district);
		fiStream *S = ASSET.Open("rsChild","txt");
		bool renderable = ASSET.Exists("entity","type");
		ASSET.PopFolder();
		ASSET.PopFolder();

		if (renderable) {
			++s_InstanceCount;
			TransformV s;
			Matrix34 temp = xform;
			temp.Normalize();
			TransformVFromMat34V(s, RC_MAT34V(temp));
			fprintf(output,"level/%s/%s  %g %g %g  %g %g %g %g\r\n",
				level,district,
				s.GetPosition().GetXf(),s.GetPosition().GetYf(),s.GetPosition().GetZf(),
				s.GetRotation().GetXf(),s.GetRotation().GetYf(),s.GetRotation().GetZf(),s.GetRotation().GetWf());
		}

		if (S) {
			fiTokenizer T("rsChild.txt",S);
			char mbName[128];
			char blockName[128];
			while (T.GetToken(mbName,sizeof(mbName))) {
				T.GetToken(blockName,sizeof(blockName));
				Matrix34 mtx;
				T.GetVector(mtx.a); T.GetFloat();
				T.GetVector(mtx.b); T.GetFloat();
				T.GetVector(mtx.c); T.GetFloat();
				T.GetVector(mtx.d); T.GetFloat();
				Matrix34 composite;
				composite.Dot(xform,mtx);
				RecurseChildren(output,level,blockName,composite);
			}
			S->Close();
		}
	}

	void InitClient()
	{
		m_LightMode = true;
		
		rmcSampleManager::InitClient();

		const char *file = "saloonPhysicsDemo_runme";
		PARAM_file.Get(file);

		ASSET.PushFolder("reslist");
		fiStream *S = ASSET.Open(file,"reslist");
		ASSET.PopFolder();
		if (!S)
			Quitf("Cannot open reslist");

		char buffer[256];
		int state = 0;
		u32 pcTexSize = 0, xenonTexSize = 0, pcDrawableSize = 0, xenonDrawableSize = 0;
		while (state != -1) {
			fgetline(buffer,sizeof(buffer),S);
			StringNormalize(buffer,buffer,sizeof(buffer));
			if (state == 1) {
				char *space = strchr(buffer,' ');
				if (space)
					*space = 0;
				Txd_t &t = m_Txds.Append();
				t.Name = StringDuplicate(buffer);
				if (PARAM_preload.Get())
					pgRscBuilder::Load(t.Dictionary,t.Name,"#td",grcTextureDefault::RORC_VERSION);
				else
					t.Dictionary = NULL;
				pcTexSize += get_file_size(t.Name,"wtd");
				xenonTexSize += get_file_size(t.Name,"xtd");
				if (m_Txds.GetCount() == m_Txds.GetCapacity())
					state = 0;
			}
			else if (state == 2) {
				char *comma = strchr(buffer,',');
				Assert(comma);
				*comma = 0;
				Drawable_t &d = m_Drawables.Append();
				d.Name = StringDuplicate(buffer);
				d.Txd = atoi(comma + 1);
				if (PARAM_preload.Get()) {
					pgDictionary<grcTexture> *prev = pgDictionary<grcTexture>::SetCurrent(m_Txds[d.Txd].Dictionary);
					pgRscBuilder::Load(d.Drawable,d.Name,"#dr",rmcDrawable::RORC_VERSION);
					pgDictionary<grcTexture>::SetCurrent(prev);
				}
				else
					d.Drawable = NULL;
				pcDrawableSize += get_file_size(d.Name,"wdr");
				xenonDrawableSize += get_file_size(d.Name,"xdr");
				if (m_Drawables.GetCount() == m_Drawables.GetCapacity())
					state = 0;
			}
			else if (!strncmp(buffer,"[texdict]=",10)) {
				m_Txds.Reserve(atoi(buffer+10));
				state = 1;
			}
			else if (!strncmp(buffer,"[drawable]=",11)) {
				m_Drawables.Reserve(atoi(buffer+11));
				state = 2;
			}
			else if (!strncmp(buffer,"[district]=",11)) {
				m_District = StringDuplicate(buffer+11);
			}
			else if (!strncmp(buffer,"[level]=",8)) {
				m_Level = StringDuplicate(buffer+8);
			}
			else if (!strncmp(buffer,"[startpos]=",11)) {
				m_StartPos.Zero();
				m_StartPos.x = (float)atof(buffer+11);
				char *comma = strchr(buffer,',');
				if (comma) {
					m_StartPos.y = (float)atof(comma+1);
					comma = strchr(comma+1,',');
					if (comma)
						m_StartPos.z = (float)atof(comma+1);
				}
				state = -1;
			}
			else if (buffer[0]=='[')
				Errorf("ignoring unrecognized directive line %s",buffer);
		}
		S->Close();

		Displayf("WinPC tex(%u) + drawable(%u) = %uk total",pcTexSize>>10,pcDrawableSize>>10,(pcTexSize+pcDrawableSize)>>10);
		Displayf("Xenon tex(%u) + drawable(%u) = %uk total",xenonTexSize>>10,xenonDrawableSize>>10,(xenonTexSize+xenonDrawableSize)>>10);

		bool needToCreate = false;
		ASSET.PushFolder("inst");
		S = ASSET.Open(file,"inst");
		if (!S) {
			S = ASSET.Create(file,"inst");
			if (!S)
				Quitf("Unable to create %s, maybe inst directory is missing?",file);
			else
				Displayf("Making %s.inst...",file);
			needToCreate = true;
		}
		ASSET.PopFolder();

 		if (needToCreate) {
			s_InstanceCount = 0;
			fprintf(S,"%6d\r\n",0);
			ASSET.PushFolder("level");
				RecurseChildren(S,m_Level,m_District,M34_IDENTITY);
			ASSET.PopFolder();
			S->Seek(0);
			fprintf(S,"%6d",s_InstanceCount);
			S->Close();
			ASSET.PushFolder("inst");
			S = ASSET.Open(file,"inst");
			ASSET.PopFolder();
		}

		fiTokenizer T(file,S);
		int instanceCount = T.GetInt();
		Displayf("Loading %d instances...",instanceCount);
		m_Instances.Reserve(instanceCount);
		for (int i=0; i<instanceCount; i++) {
			Instance_t &newInst = m_Instances.Append();
			char instName[128];
			T.GetToken(instName,sizeof(instName));
			TransformV s;
			float x = T.GetFloat(), y = T.GetFloat(), z = T.GetFloat(), w;
			s.SetPosition(Vec3V(x,y,z));
			x = T.GetFloat(); y = T.GetFloat(); z = T.GetFloat(); w = T.GetFloat();
			s.SetRotation(QuatV(x,y,z,w));
			Mat34VFromTransformV(RC_MAT34V(newInst.Matrix), s);
			newInst.Drawable = -1;
			for (int j=0; j<m_Drawables.GetCount(); j++) {
				if (!strnicmp(m_Drawables[j].Name,instName,strlen(instName))) {
					newInst.Drawable = j;
					break;
				}
			}
			if (newInst.Drawable == -1)
				Quitf("Unable to find instance named %s",instName);
		}
		S->Close();
	}

	void InitCamera()
	{
		InitSampleCamera(m_StartPos + ZAXIS,m_StartPos);
	}

	void ShutdownClient()
	{
		m_Drawables.Reset();
		m_Txds.Reset();
		StringFree(m_District);
		StringFree(m_Level);
	}

	void UpdateClient()
	{
		//-- If we want an animation, update it here
	}

	void DrawDrawable(int drawable,const Matrix34 &mtx) {
		rmcDrawable *d = m_Drawables[drawable].Drawable;
		if (d && m_Txds[m_Drawables[drawable].Txd].Dictionary) {
			d->Draw(mtx,0,0);
		}
	}

	void DrawClient()
	{
		for (int i=0; i<m_Instances.GetCount(); i++) {
			DrawDrawable(m_Instances[i].Drawable,m_Instances[i].Matrix);
		}
	}

#if __BANK
	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
	}
#endif

};


XPARAM(shaderpath);

// main application
int Main()
{
	rmcSampleLevel sampleRmc;
	if (!PARAM_shaderpath.Get())
		PARAM_shaderpath.Set("t:/rdr2/assets");
	sampleRmc.Init("t:/rdr2/assets");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
