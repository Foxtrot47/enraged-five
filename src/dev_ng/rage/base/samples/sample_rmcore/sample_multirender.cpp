// 
// sample_rmcore\sample_drawable.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE:
//		This sample shows all the bank widgets in action.


// TITLE: Drawable
// PURPOSE:
//		This sample shows how to load and draw a skinned character.

// Useful command line besides the default one:
//
// Play an animation:
// -file x:/rage/assets/rage_male/rage_male.type -anim x:/rage/assets/animation/motion/male_wlk.anim
//
// Stress test the renderer
// -file x:/rage/assets/rage_male/rage_male.type -many=10 -perobjectlighting
//
// View GTA4 assets:
// -zup -archive x:/gta/build/ps3/data/maps/east/queens_e.img -rscLoad x:/gta/build/ps3/data/maps/east/ap_cargo_hangar -shaderlib x:/gta/build/common/shaders -shaderdb x:/gta/build/common/shaders/db
//
// Rage Wilderness Demo:
// -file X:\rageland\assets\rageland\ragewood\ragewood_robot\ragewood_robot.type -shaderlib x:/rage/assets/sample_wilderness/wildshaders -shaderdb t:/rage/assets/sample_wilderness/wildshaders/db

/*
    Basic performance numbers, 360 Release
    -file t:/rage/assets/rage_male/rage_male.type -anim t:/rage/assets/animation/motion/male_wlk.anim -many=6 -freeze
        Baseline:			CPU draw time 0.46ms, GPU render time 2.167ms
        MTX_IN_VB=1			CPU draw time 0.34ms, GPU render time 2.209ms (predication is faster)
        +MTX_IN_VB_HALF=1	CPU draw time 0.35ms, GPU render time 1.904ms (predication is slower)
        MTX_IN_TEX=1		CPU draw time 0.34ms, GPU render time 2.291ms
        +MTX_IN_TEX_HALF=1	CPU draw time 0.35ms, GPU render time 1.998ms
*/

#define GCM_BUFFER_SIZE	1024 // zero forces FIFO into VRAM, which is not supported right now for replay at least
#include "sample_rmcore.h"

#define ENABLE_RFS		1

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
// #include "data/marker.h"	// included by main.h now, sigh
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "grmodel/shaderfx.h"
#include "grprofile/drawmanager.h"
#include "profile/timebars.h"
#include "rmcore/drawable.h"
#include "system/cache.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/messagequeue.h"
#include "grcore/vertexBuffer.h"

// For debug visualization:
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

#if __PS3
#include "grcore/grcorespu.h"
#endif

using namespace rage;

PARAM(file,"[sample_drawable] The filename of the .type file to load (default entity.type)");
PARAM(anim,"[sample_drawable] The filename of the animation file to load (default is none)");
PARAM(rscAnim, "[sample_drawable] The filename of the animation resource file to load (default is none)");
PARAM(mesh,"[sample_drawable] The filename of a mesh to load for debug visualization");
PARAM(rscLoad,"[sample_drawable] Load drawable from existing named resource (default: resourceTest)");
PARAM(rscBuild,"[sample_drawable] Save drawable to resource (default: resourceTest)");
PARAM(center, "[sample_drawable] Put the drawable at the origin");
PARAM(many, "[sample_drawable] Draw N*N copies of the object for performance testing");
PARAM(perobjectlighting, "[sample_drawable] Fake per-object lighting to stress fragment shader patching");
PARAM(freeze,"[sample_drawable] Freeze animation playback after one frame to stabilize GPU time");

struct smpDrawListItem {
	typedef void (smpDrawListItem::*ExecFn)();
	static ExecFn sm_ExecuteTable[256];
	u8 m_Opcode, m_User;
	u16 m_Size;

	smpDrawListItem(u8 opcode,size_t bytes) : m_Opcode(opcode), m_User(0), m_Size(u16((bytes + 15) & ~15)) { 
	}

	static void InitClass(size_t numBuffers,size_t bytesPerBuffer) {
		sm_Update = sm_Render = 0;
		sm_Max = numBuffers;
		sm_Buffers = rage_new char*[numBuffers];
		sm_BufferSize = bytesPerBuffer;
		for (size_t i=0; i<numBuffers; i++)
			sm_Buffers[i] = rage_new char[bytesPerBuffer];
		sm_Offset = 0;
		sm_Top = sm_BufferSize;
		sm_Fences = rage_new grcFenceHandle[numBuffers];
		for (size_t i=0; i<numBuffers; i++)
			sm_Fences[i] = NULL;
	}
	static void *Allocate(size_t bytes) {
		void *result = sm_Buffers[sm_Update] + sm_Offset;
		sm_Offset += (bytes + 15) & ~15;
		TrapGT(sm_Offset,sm_BufferSize);
		return result;
	}
	static grmMatrixSet* CreateMatrixSet(int boneCount) {
		sm_Top -= grmMatrixSet::ComputeSize(boneCount);
		TrapGT(sm_Offset,sm_Top);
		grmMatrixSet *result = (grmMatrixSet*)(sm_Buffers[sm_Update] + sm_Top);
		grmMatrixSet::Create(result,boneCount);
		return result;
	}
	static void StartList() {
		sm_Current = (smpDrawListItem*)(sm_Buffers[sm_Update] + sm_Offset);
	}
	static void EndList() {
		// Maintain 16-byte alignment
		smpDrawListItem *end = (smpDrawListItem*) Allocate(sizeof(smpDrawListItem));
		end->m_Opcode = end->m_User = 0;
		end->m_Size = 16;
		sm_Queue.Push(sm_Current);
		// Displayf("%" SIZETFMT "d. Kicking item %p",sm_Update,sm_Current);
	}
	static void InsertFence() {
		sm_Fences[sm_Render] = GRCDEVICE.InsertFence();
	}
	static void NextList() {
		sm_Queue.Push(NULL);
		sm_Render = sm_Update;
		if (++sm_Update == sm_Max)
			sm_Update = 0;
		if (sm_Fences[sm_Update])
			GRCDEVICE.BlockOnFence(sm_Fences[sm_Update]);
		sm_Offset = 0;
		sm_Top = sm_BufferSize;
	}
	void ExecuteList() {
		// Displayf("%" SIZETFMT "d. Executing item %p",sm_Render,this);
		smpDrawListItem *i = this;
		while (i->m_Opcode) {
			(i->*sm_ExecuteTable[i->m_Opcode])();
			i = (smpDrawListItem*)((char*)i + i->m_Size);
		}
	}

	static size_t sm_Update, sm_Render, sm_Max, sm_Offset, sm_Top;
	static size_t sm_BufferSize;
	static char **sm_Buffers;
	static smpDrawListItem *sm_Current;
	static sysMessageQueue<smpDrawListItem*,32> sm_Queue;
	static grcFenceHandle *sm_Fences;
};

smpDrawListItem::ExecFn smpDrawListItem::sm_ExecuteTable[256];
size_t smpDrawListItem::sm_Update, smpDrawListItem::sm_Render, smpDrawListItem::sm_Max, 
	smpDrawListItem::sm_Offset, smpDrawListItem::sm_BufferSize, smpDrawListItem::sm_Top;
char **smpDrawListItem::sm_Buffers;
smpDrawListItem *smpDrawListItem::sm_Current;
sysMessageQueue<smpDrawListItem*,32> smpDrawListItem::sm_Queue;
grcFenceHandle* smpDrawListItem::sm_Fences;

struct smpDrawListRegister {
	smpDrawListRegister(smpDrawListItem::ExecFn exec) : m_Next(sm_First) {
		sm_First = m_Next;
		smpDrawListItem::sm_ExecuteTable[m_Opcode = ++sm_NextOpcode] = exec;
		TrapZ(sm_NextOpcode);	// catch overflow
	}
	static smpDrawListRegister *sm_First;
	smpDrawListRegister *m_Next;
	static u8 sm_NextOpcode;
	u8 m_Opcode;
};

smpDrawListRegister* smpDrawListRegister::sm_First;
u8 smpDrawListRegister::sm_NextOpcode;

#define SMP_DECLARE_REGISTER(class) \
	static smpDrawListRegister sm_##class; \
	void Execute(); \
	static void* operator new(size_t bytes) { return smpDrawListItem::Allocate(bytes); }

#define SMP_CTOR(class) smpDrawListItem(sm_##class.m_Opcode,sizeof(*this))

#define SMP_DEFINE_REGISTER(class) smpDrawListRegister class::sm_##class((ExecFn)&class::Execute);

struct smpSetViewport: public smpDrawListItem {
	SMP_DECLARE_REGISTER(smpSetViewport);
	smpSetViewport(grcViewport *vp) : SMP_CTOR(smpSetViewport) {
		m_Viewport = *vp;
	}
	grcViewport m_Viewport;
};

void smpSetViewport::Execute() {
	grcViewport::SetCurrent(&m_Viewport);
}

SMP_DEFINE_REGISTER(smpSetViewport);

struct smpDraw: public smpDrawListItem {
	SMP_DECLARE_REGISTER(smpDraw);
	smpDraw(rmcDrawable *dr,Matrix34 &mtx,u16 bucketMask,u8 lod) 
		: m_Drawable(dr)
		, m_RootMtx(mtx)
		, m_BucketMask(bucketMask)
		, m_Lod(lod)
		, SMP_CTOR(smpDraw) {
	}
	rmcDrawable *m_Drawable;
	u16 m_BucketMask;
	u8 m_Lod, pad;
	Matrix34 m_RootMtx;
};

void smpDraw::Execute() {
	m_Drawable->Draw(m_RootMtx,m_BucketMask,m_Lod);
}

SMP_DEFINE_REGISTER(smpDraw);

struct smpDrawSkinned: public smpDrawListItem {
	SMP_DECLARE_REGISTER(smpDrawSkinned);
	smpDrawSkinned(rmcDrawable *dr,Matrix34 &mtx,grmMatrixSet *ms,u16 bucketMask,u8 lod) 
		: m_Drawable(dr)
		, m_RootMtx(mtx)
		, m_MatrixSet(ms)
		, m_BucketMask(bucketMask)
		, m_Lod(lod)
		, SMP_CTOR(smpDrawSkinned) {
	}
	rmcDrawable *m_Drawable;
	grmMatrixSet *m_MatrixSet;
	u16 m_BucketMask;
	u8 m_Lod, pad;
	Matrix34 m_RootMtx;
};

void smpDrawSkinned::Execute() {
	m_Drawable->DrawSkinned(m_RootMtx,*m_MatrixSet,m_BucketMask,m_Lod);
}

SMP_DEFINE_REGISTER(smpDrawSkinned);

struct smpSetLightEnable: public smpDrawListItem {
	SMP_DECLARE_REGISTER(smpSetLightEnable);
	smpSetLightEnable(bool flag) : SMP_CTOR(smpSetLightEnable) {
		m_User = flag;
	}
};

void smpSetLightEnable::Execute() {
	grcLightState::SetEnabled(m_User != 0);
}

SMP_DEFINE_REGISTER(smpSetLightEnable);


#if MULTIPLE_RENDER_THREADS
static char s_SubNames[MULTIPLE_RENDER_THREADS][16];
sysIpcEvent s_SubThreadCompletes[MULTIPLE_RENDER_THREADS];
struct subItem {
	smpDrawListItem *input;
	grcContextCommandList *output;
};
sysMessageQueue<subItem*,32> s_SubQueues[MULTIPLE_RENDER_THREADS];
#endif

class rmcSampleMultiRender : public ragesamples::rmcSampleManager
{
	static const int listSize = 32;

public:
	virtual const char* GetSampleName() const
	{
		return "sample_multirender";
	}

	rmcSampleMultiRender()
	{
		// This needs to be set up before InitClient etc get called.
		m_BeginDraw = sysIpcCreateEvent();
		m_FinishedDraw = sysIpcCreateEvent();
	}

	void InitClient()
	{
		RAGE_FUNC();

		m_LightMode = true;
		
		rmcSampleManager::InitClient();

		crAnimation::InitClass();

		smpDrawListItem::InitClass(4,64*1024);

		// STEP #1. Allocate and load our drawable instance.

		//-- The first thing we do is allocate our <c>rmcDrawable</c> instance.
		m_Drawable = NULL;
		m_TexDict = NULL;

		m_Drawable = rage_new rmcDrawable();

		//-- Now we load the drawable.  
		if( !m_Drawable->Load("rage_male.type") )
			Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );

		// STEP #2. Initialize our skeleton.

		//-- The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		crSkeletonData *skeldata = m_Drawable->GetSkeletonData();
		if (skeldata)
		{
			m_Skeleton = rage_new crSkeleton();

			m_Skeleton->Init(*skeldata,NULL);
			
			//-- In case we want to play animations on the skeleton, load an animation file
			const char *filename = "x:/rage/assets/animation/motion/male_wlk.anim";
			PARAM_anim.Get(filename);
			m_Anim = crAnimation::AllocateAndLoad(filename);
		}
		else 
		{
			m_Skeleton = 0;
			m_Anim = 0;
		}
		// -STOP

		m_PlaybackRate = 30.0f;
		m_UpdateAnim = m_Anim != 0;
		m_SlideRate.Set(0.75f, 0.55f, 0.35f);
		m_SlideTimer = 0.f;
		m_RotateTimer = 0.f;
		m_FrameTimer = 0.f;
		m_VertexNormalLength = 0.02f;
		m_VertexNormalColor = Color32(1.0f,0.0f,0.0f);
		m_ModelOpacity = 1.0f;
		m_NumRenderInstancesX = 20;
		m_NumRenderInstancesY = 1;
		m_NumRenderInstancesZ = 20;
		if (PARAM_many.Get()) {
			PARAM_many.Get(m_NumRenderInstancesX);
			PARAM_many.Get(m_NumRenderInstancesZ);
		}
		m_TotalRenderInstances = m_NumRenderInstancesX * m_NumRenderInstancesY * m_NumRenderInstancesZ;
		m_PerObjectLighting = PARAM_perobjectlighting.Get();
		m_RenderDistance.Set(0.5f,0.5f,0.5f);

		// command buffer:
		m_DrawUsingCommandBuffers = false;

	}

	void ShutdownClient()
	{
		RAGE_FUNC();

		delete m_Skeleton;
		delete m_Anim;

		crAnimation::ShutdownClass();
	}


	void UpdateClient()
	{
		RAGE_FUNC();

		//-- If we want an animation, update it here
		//Note : We don't want to update the animation if vertex normals are being drawn, so the animation resumes from the
		//frame when the draw normalss button was pressed.
		if ( m_UpdateAnim ) 
		{
			m_FrameTimer += TIME.GetSeconds() * m_PlaybackRate;
			if (PARAM_freeze.Get())
				m_UpdateAnim = false;
		}
		if ( m_Anim ) 
		{
			crFrame frameBuf;
			frameBuf.InitCreateBoneAndMoverDofs(m_Skeleton->GetSkeletonData(), false);
			m_Anim->CompositeFrame(fmodf(m_Anim->Convert30FrameToTime(m_FrameTimer), m_Anim->GetDuration()), frameBuf);
			frameBuf.Pose(*m_Skeleton);
		}

		m_Center.Identity();

		m_Center.d.x = m_Drawable->GetLodGroup().GetCullRadius() * 2.0f * sinf(m_SlideTimer * m_SlideRate.x);
		m_Center.d.z = m_Drawable->GetLodGroup().GetCullRadius() * 2.0f * -sinf(m_SlideTimer * m_SlideRate.z);
		m_Center.d.y = m_Drawable->GetLodGroup().GetCullRadius() * sinf(m_SlideTimer * m_SlideRate.y);

		if (PARAM_center.Get()) {
			m_Center.d = -m_Drawable->GetLodGroup().GetCullSphere();
		}

		grmMatrixSet *ms = smpDrawListItem::CreateMatrixSet(m_Skeleton->GetBoneCount());
		//-- If we have a skeleton, inform it of it's new position & update all child bones
		if ( m_Skeleton ) {
			m_Skeleton->SetParentMtx(reinterpret_cast<Mat34V*>(&m_Center));
			m_Skeleton->Update();
			ms->Update(*m_Skeleton,m_Drawable->IsSkinned());
		}


		for (int x=0;x<m_NumRenderInstancesX;x++)
		{
			smpDrawListItem::StartList();

			new smpSetViewport(m_Viewport);

			new smpSetLightEnable(m_LightMode);

			for (int y=0;y<m_NumRenderInstancesY;y++)
			{
				for (int z=0;z<m_NumRenderInstancesZ;z++)
				{
					Matrix34 mtx;
					mtx.Identity3x3();
					mtx.d.Set(x*m_RenderDistance.x,y*m_RenderDistance.y,z*m_RenderDistance.z);

					// new smpDraw(m_Drawable,mtx,0xFFFF,LOD_HIGH);
					new smpDrawSkinned(m_Drawable,mtx,ms,0xFFFF,LOD_HIGH);
				}
			}

			smpDrawListItem::EndList();
		}

		smpDrawListItem::NextList();
	}


	void DrawClient()
	{
#if MULTIPLE_RENDER_THREADS
		// Copy jobs to sub render queues
		int buffer = 0;
		atFixedArray<subItem,32> subItems;

		while (smpDrawListItem *i = smpDrawListItem::sm_Queue.Pop()) {
			subItem &si = subItems.Append();
			si.input = i;
			si.output = NULL;
			s_SubQueues[buffer].Push(&si);
			if (++buffer == MULTIPLE_RENDER_THREADS)
				buffer = 0;
		}

		// Tell all subthreads to finish up
		for (int i=0; i<MULTIPLE_RENDER_THREADS; i++)
			s_SubQueues[i].Push(NULL);

		// Wait for all subthreads to finish
		for (int i=0; i<MULTIPLE_RENDER_THREADS; i++)
			sysIpcWaitEvent(s_SubThreadCompletes[i]);

		// Collect and dispatch all the results.
 		GRCDEVICE.BeginCommandList();
		for (int i=0; i<subItems.GetCount(); i++)
			GRCDEVICE.ExecuteCommandList(subItems[i].output);
		GRCDEVICE.EndCommandList();
#else
		while (smpDrawListItem *i = smpDrawListItem::sm_Queue.Pop())
			i->ExecuteList();
#endif
		// Insert a render fence; we wait for it on Update to make sure we're not
		// getting too far ahead.
		smpDrawListItem::InsertFence();
	}

#if MULTIPLE_RENDER_THREADS
	static void SubRender(void *indexAsPtr)
	{
		u8 index = (u8)(size_t)indexAsPtr;
		PF_INIT_TIMEBARS(s_SubNames[index],1000,30.0);
		g_IsSubRenderThread = true;
		g_RenderThreadIndex = index;
		for (;;) {
			PF_FRAMEINIT_TIMEBARS(GRCDEVICE.GetFrameCounter());

			while (subItem *i = s_SubQueues[index].Pop()) {
				GRCDEVICE.BeginContext();

				grcStateBlock::MakeDirty();
				grcStateBlock::Default();

				i->input->ExecuteList();

				i->output = GRCDEVICE.EndContext();
			}
			sysIpcSetEvent(s_SubThreadCompletes[index]);
		}
	}
#endif

#if !__D3D11
	void Draw()
	{
		PF_INIT_TIMEBARS("RenderThread",1000,30.0);

		// Create the device on the render thread!
		grcSampleManager::InitSetup("x:/rage/assets/rage_male","sample_multirender");
		sysIpcSetEvent(m_FinishedDraw);

#if MULTIPLE_RENDER_THREADS
		for (int i=0; i<MULTIPLE_RENDER_THREADS; i++) {
			s_SubThreadCompletes[i] = sysIpcCreateEvent();
			formatf(s_SubNames[i],"SubRender:%d",i);
			sysIpcCreateThread(SubRender,(void*)(size_t)i,65536,PRIO_NORMAL,s_SubNames[i],1+i);
		}
#endif

		// Now enter normal loop
		for (;;) {
			PF_FRAMEINIT_TIMEBARS(GRCDEVICE.GetFrameCounter());
			sysIpcWaitEvent(m_BeginDraw);
			grcSampleManager::Draw();
			sysIpcSetEvent(m_FinishedDraw);
		}
	}

	static void DrawStatic(void *closure)
	{
		((rmcSampleMultiRender*)closure)->Draw();
	}

	void InitSetup(const char *,const char *)
	{
		sysIpcCreateThread(DrawStatic,this,65536,PRIO_NORMAL,"RenderThread",1);
		sysIpcWaitEvent(m_FinishedDraw);
	}

	void UpdateLoop()
	{
		sysIpcSetEvent(m_FinishedDraw);

		do {
			PF_FRAMEINIT_TIMEBARS(TIME.GetFrameCount());

			// This basically means update and render never execute at the same time,
			// which is not what we want for production code but is fine for this sample
			// (since there's very little update in the first place).
			sysIpcWaitEvent(m_FinishedDraw);
			Update();
			sysIpcSetEvent(m_BeginDraw);
		} while (!WantsExit());
	}
#endif

#if __BANK
	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
		
#if !RSG_ORBIS
		m_Drawable->GetShaderGroup().AddWidgets(BANKMGR.CreateBank("Shaders"));
#endif
		bkBank &bk = BANKMGR.CreateBank("rage - SampleDrawable");
		if ( m_Anim ) {
			bk.AddToggle("Update Animation", &m_UpdateAnim);
			bk.AddSlider("Animation Rate (fps)", &m_PlaybackRate, -1000.0, 1000.0f, 0.01f);
		}

		bk.PushGroup("Multiple Instances",false,"Group of widgets for displaying multiple instances of the current drawable");
		bk.AddSlider("Num Render Instances X",&m_NumRenderInstancesX,0,1000,1);
		bk.AddSlider("Num Render Instances Y",&m_NumRenderInstancesY,0,1000,1);
		bk.AddSlider("Num Render Instances Z",&m_NumRenderInstancesZ,0,1000,1);
		bk.AddSlider("Total # of Instances",&m_TotalRenderInstances,0,1000*1000*1000,0);

		bk.AddSlider("Render Distance",&m_RenderDistance,0.0f,10000.0f,0.5f);   
		bk.PopGroup();

#if COMMAND_BUFFER_SUPPORT
		bk.AddToggle("Draw Using Command Buffers", &m_DrawUsingCommandBuffers);
#endif
	}
#endif

private:
	rmcDrawable				*m_Drawable;
	pgDictionary<grcTexture>*m_TexDict;
	crSkeleton*				m_Skeleton;
	crAnimation*			m_Anim;
	Matrix34				m_Center;
	Vector3					m_SlideRate;
	float					m_PlaybackRate;
	float					m_SlideTimer;
	float					m_RotateTimer;
	float					m_FrameTimer;
	float					m_VertexNormalLength;
	Color32					m_VertexNormalColor;
	bool					m_UpdateAnim;
	float					m_ModelOpacity;
	int						m_NumRenderInstancesX;
	int						m_NumRenderInstancesY;
	int						m_NumRenderInstancesZ;
	int						m_TotalRenderInstances;
	Vector3					m_RenderDistance;

	bool					m_DrawUsingCommandBuffers;
	bool					m_PerObjectLighting;

	sysIpcEvent				m_BeginDraw, m_FinishedDraw;
};

#include "data/struct.h"

// main application
int Main()
{
	rmcSampleMultiRender sampleRmc;
	sampleRmc.Init("x:/rage/assets/rage_male/");

	// We subclass the update loop to launch a separate render thread which in turn has sub-render threads.
	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
