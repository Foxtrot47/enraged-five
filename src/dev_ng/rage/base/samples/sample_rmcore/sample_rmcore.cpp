//
// sample_rmcore/sample_rmcore.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "sample_rmcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/channel.h"
#include "grmodel/setup.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"
#include "system/param.h"
#include "system/exec.h"

#include <stdio.h>

using namespace rage;

//PARAM(file,"[sample_rmcore] File to load (default is \"entity.type\")");
PARAM(shaderlibs,"[sample_rmcore] Set shader library path (default is \"tune/shaders/lib\")");
PARAM(shaderdb,"[sample_rmcore] Set shader database path (default is \"tune/shaders/db\")");
PARAM(shaderpath,"[sample_rmcore] Set shader root path (tune/shaders/lib and tune/shaders/db are appended)");
PARAM(shaderlist, "[sample_rmcore] Load shaders from <name>.list instead of preload.list");
PARAM(shaderdblist, "[sample_rmcore] Load SPS files from <name>.list instead of preload.list");
PARAM(autotexdict, "[sample_rmcore] Automatically create a texture dictionary for textures loaded without one");

PARAM(gta,"[sample_rmcore] Use GTA path setup");
PARAM(jimmy,"[sample_rmcore] Use Jimmy path setup");
PARAM(mc4,"[sample_rmcore] Use MC4 path setup");
PARAM(rdr2,"[sample_rmcore] Use RDR2 path setup");
PARAM(mp3,"psample_rmcore] Use MP3 path setup");
PARAM(gta5,"[sample_rmcore] Use GTA5 path setup");
PARAM(gta5local, "[sample_rmcore] Use GTA5 path setup with local shaders");

// NOTE: This is XPARAM'd, so that each sample can provide its own documentation as to what this is for
XPARAM(file);

namespace ragesamples {
XPARAM(zup);
}
namespace asset {
XPARAM(path);
}

char g_DefaultEffectName[RAGE_MAX_PATH];

namespace ragesamples {

rmcSampleManager::rmcSampleManager() : grcSampleManager(), m_NumFiles(0), m_MinFiles(1), m_MaxFiles(1), m_MtlLib(NULL)
{
	using namespace asset;

	if (PARAM_gta.Get()) {
		PARAM_zup.Set("");
		if (!PARAM_path.Get())
		{
			PARAM_path.Set("x:/gta/build");
		}
		if (!PARAM_shaderlibs.Get())
		{
			PARAM_shaderlibs.Set("x:/gta/build/common/shaders");
		}
		if (!PARAM_shaderdb.Get())
		{
			PARAM_shaderdb.Set("x:/gta/build/common/shaders/db");
		}
	}
	else if (PARAM_jimmy.Get()) {
		PARAM_zup.Set("");
		if (!PARAM_path.Get())
		{
			PARAM_path.Set("x:/jimmy/build/dev");
		}
		if (!PARAM_shaderlibs.Get())
		{
			PARAM_shaderlibs.Set("x:/jimmy/build/dev/common/shaders");
		}
		if (!PARAM_shaderdb.Get())
		{
			PARAM_shaderdb.Set("x:/jimmy/build/dev/common/shaders/db");
		}
	}
	else if (PARAM_gta5.Get()) {
		PARAM_zup.Set("");
		if (!PARAM_path.Get())
		{
			PARAM_path.Set("X:/rageassets/dev/rage/assets");
		}
		if (!PARAM_shaderdb.Get())
		{
			PARAM_shaderdb.Set("t:/rage/assets/tune/shaders/db");
		}
		if (!PARAM_shaderlibs.Get())
		{
			PARAM_shaderlibs.Set("t:/rage/assets/tune/shaders/lib");
		}
	}
	else if (PARAM_gta5local.Get()) {
		PARAM_zup.Set("");
		if (!PARAM_path.Get())
		{
			PARAM_path.Set("X:/rageassets/dev/rage/assets");
		}
		if (!PARAM_shaderdb.Get())
		{
#if RSG_ORBIS || RSG_DURANGO
			PARAM_shaderdb.Set("X:/gta5/build/dev_ng/common/shaders/db");
#else
			PARAM_shaderdb.Set("X:/gta5/build/dev/common/shaders/db");
#endif
		}
		if (!PARAM_shaderlibs.Get())
		{
#if RSG_ORBIS || RSG_DURANGO
			PARAM_shaderlibs.Set("X:/gta5/build/dev_ng/common/shaders");
#else
			PARAM_shaderlibs.Set("X:/gta5/build/dev/common/shaders");
#endif
		}
	}
	else if (PARAM_mc4.Get()) {
		if (!PARAM_path.Get())
		{
			PARAM_path.Set("t:/mc4/assets");
		}
		// MC4 has two shader paths, not sure what to do here...
	}
	else if (PARAM_rdr2.Get()) {
		// Are these correct?
		if (!PARAM_path.Get())
		{
			PARAM_path.Set("t:/rdr2/assets");
		}
		if (!PARAM_shaderdb.Get())
		{
			PARAM_shaderdb.Set("t:/rdr2/tune/shaders/db");
		}
		if (!PARAM_shaderlibs.Get())
		{
			PARAM_shaderlibs.Set("t:/rdr2/tune/shaders/lib");
		}
	}
	else if (PARAM_mp3.Get()) {
		// No clue, fill this in pls.
	}

	const char* shaderlibsName = NULL;
	if (PARAM_shaderlibs.Get(shaderlibsName))
	{
		safecpy(g_DefaultEffectName, shaderlibsName);
		safecat(g_DefaultEffectName, "/im");
		GRCDEVICE.SetDefaultEffectName(g_DefaultEffectName);
	}

	for (int i = 0; i < m_MinFiles; ++i)
	{
		m_DefaultFileNames[i] = "entity.type";
	}
	for (int i = m_MinFiles; i < m_MaxFiles; ++i)
	{
		m_DefaultFileNames[i] = "unknown.type";
	}
}

grcSetup& rmcSampleManager::AllocateSetup()
{
	return *(rage_new grmSetup());
}

void rmcSampleManager::CreateGfxFactories()
{
	const char* libPathPtr;
	if (PARAM_shaderlibs.Get(libPathPtr))
		grcEffect::SetDefaultPath(libPathPtr);

	grcSampleManager::CreateGfxFactories();

	// We always want auto tex dict's now because the shader system
	// no longer owns texture references.
	grmShaderGroup::SetAutoTexDict(true);

#if __WIN32 && __D3D
	grmShaderFx::RegisterTechniqueGroup("normals");
#endif
	RegisterTechniqueGroups();
	InitShaderSystems();
}

void rmcSampleManager::InitShaderSystems() 
{
	char fullShaderLibPath[RAGE_MAX_PATH*4];	// potentially the path is several path's appended together with ; as a seperator
	char fullShaderDbPath[RAGE_MAX_PATH*4];		// potentially the path is several path's appended together with ; as a seperator
	// bool bLoadShaderPresets = true;

	// Default is t:/rage/assets, but this can be overridden by an environment variable, then a command line flag.
	char cmdLineShaderPathBuf[RAGE_MAX_PATH];
	const char* cmdLineShaderPath = "x:/rage/assets";

	if (PARAM_shaderpath.Get(cmdLineShaderPath))
	{
		grcDisplayf("Using explicit shader path '%s' from command line",cmdLineShaderPath);
	}
	else if (sysGetEnv("RAGE_ASSET_ROOT",cmdLineShaderPathBuf,sizeof(cmdLineShaderPathBuf)))
	{
		cmdLineShaderPath = cmdLineShaderPathBuf;
		grcDisplayf("Using RAGE_ASSET_ROOT environment variable value '%s'",cmdLineShaderPath);
	}
	else
	{
		grcDisplayf("Falling back to hard-coded default value, '%s'",cmdLineShaderPath);
	}

	formatf(fullShaderLibPath,"%s/%s", cmdLineShaderPath, "tune/shaders/lib");
	formatf(fullShaderDbPath,"%s/%s", cmdLineShaderPath, "tune/shaders/db");

	//Check to see if the user overrode the -shaderpath setting on the command line, or what was in the module settings file by using
	//the -shaderlibs cmd line option
	const char* cmdLineShaderLibsPath;
	if(PARAM_shaderlibs.Get(cmdLineShaderLibsPath))
	{
		grcDisplayf("Overriding Module Setting for 'PathToCompiledShaders' with -shaderlib cmd line specification");
		safecpy(fullShaderLibPath, cmdLineShaderLibsPath);
	}

	const char* preloadListName = "preload";
	PARAM_shaderlist.Get(preloadListName);

	//Load the shaders... multiple paths can be separ
	char * pPathName = &fullShaderLibPath[0];
	while(pPathName && *pPathName)
	{
		char * pSeperator = strchr(pPathName, ';');
		if (pSeperator)
			*pSeperator++ = 0;
		//Load the shaders...
		grcDisplayf("Loading shaders from : '%s'", pPathName);
		grmShaderFactory::GetInstance().PreloadShaders(pPathName, true, preloadListName);
		if (pSeperator)
		{
			pSeperator[-1] = ';';
			pPathName = pSeperator;
		}
		else
			pPathName = NULL;
	}	

	//Check to see if the user overrode the -shaderpath setting on the command line, or what was in the module settings file by using
	//the -shaderdb cmd line option
	const char* cmdLineShaderDbPath;
	if(PARAM_shaderdb.Get(cmdLineShaderDbPath))
	{
		// bLoadShaderPresets = true;
		Printf("Overriding Module Setting for 'PathToShaderDatabase' with -shaderdb cmd line specification\n");
		safecpy(fullShaderDbPath, cmdLineShaderDbPath);
	}


	const char* preloadDbListName = "preload";
	PARAM_shaderdblist.Get(preloadDbListName);

	m_MtlLib = grcMaterialLibrary::Preload(fullShaderDbPath, preloadDbListName);
	if (m_MtlLib)
		grcMaterialLibrary::SetCurrent(m_MtlLib);
	else
		grcWarningf("Unable to preload materials (sps files) from '%s', you will probably encounter more issues later.",fullShaderDbPath);
}

void rmcSampleManager::DestroyGfxFactories() {
	if (m_MtlLib) 
	{
		grcMaterialLibrary::SetCurrent(NULL);
		LastSafeRelease(m_MtlLib);
	}
	grcSampleManager::DestroyGfxFactories();
}

void rmcSampleManager::Init(const char* path)
{
	GetFileList();

	grcSampleManager::Init(path);	

	for (int i=0;i<NUM_BUCKETS;i++)
		m_BucketEnable[i]=true;
}


void rmcSampleManager::GetFileList()
{
	const char* filePath=NULL;

	// get all file names:
	m_NumFiles=0;
	m_NumFiles=PARAM_file.GetArray(m_FileNames,MAX_FILES,m_ParamNameBuf,sizeof(m_ParamNameBuf));
	if (m_NumFiles < m_MinFiles)
	{
		for (int i = m_NumFiles; i < m_MinFiles; ++i) {
			m_FileNames[i] = m_DefaultFileNames[i];
		}
		m_NumFiles = m_MinFiles;
	}

	for (int i=0;i<m_NumFiles;i++)
	{
		// find the path separator:
		char *fp = (char*)m_FileNames[i] + strlen(m_FileNames[i]) - 1;
		while (fp > m_FileNames[i] && *fp != '/' && *fp != '\\')
			--fp;

		// if we 've found it, we grab the path from the file name:
		if (*fp == '/' || *fp == '\\') {
			filePath=m_FileNames[i];
			strcpy(m_FileNameBuf[i],fp+1);
			m_FileNames[i] = m_FileNameBuf[i];
			fp[1] = 0; // zero out path separator
			SetFullAssetPath(filePath);
			ASSET.PushFolder(filePath);
		}
	}
}

#if __BANK
static bool sRenderNormals = false;
void RenderNormals() {
#if __WIN32 && __D3D
	if ( sRenderNormals ) {
		int groupId = rage::grmShaderFx::FindTechniqueGroupId("normals");
		rage::grmShaderFx::SetForcedTechniqueGroupId(groupId);
	}
	else {
		rage::grmShaderFx::SetForcedTechniqueGroupId(-1);
	}
#endif
}

void rmcSampleManager::AddWidgetsClient()
{
	AddWidgetsBuckets();
	AddWidgetsPasses();
	AddWidgetsDebugDraw();
}

void rmcSampleManager::AddWidgetsBuckets()
{
	// Bucket Widgets:
	bkBank &bankBuckets = BANKMGR.CreateBank("rage - Buckets");
	for (int i=0;i<NUM_BUCKETS;i++)
	{
		char bucketName[32];
		sprintf(bucketName,"Bucket %d",i);
		bankBuckets.AddToggle(bucketName,&m_BucketEnable[i]);
	}
}

void rmcSampleManager::AddWidgetsPasses()
{
	// passes widgets:
	bkBank &bankPasses = BANKMGR.CreateBank("rage - Passes");
	bankPasses.AddSlider("Max Passes",&grmShader::GetMaxPasses(),0,8,1);
}

void rmcSampleManager::AddWidgetsDebugDraw()
{
	bkBank &bk = BANKMGR.CreateBank("rage - Debug Draw");
	bk.AddToggle("Render Normals", &sRenderNormals, CFA(RenderNormals));
	bk.AddButton("Reload Shaders", datCallback(grcEffect::ReloadAll));

    bk.AddToggle( "Draw Grid", &m_DrawGrid );
    bk.AddToggle( "Draw Help", &m_DrawHelp );
    bk.AddToggle( "Draw World Axes", &m_DrawWorldAxes );
}
#endif


} // namespace ragesamples
