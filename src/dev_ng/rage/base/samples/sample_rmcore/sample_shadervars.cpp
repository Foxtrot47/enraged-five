// 
// sample_rmcore\sample_shadervariables.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// TITLE: ShaderVariables
// PURPOSE:
//		This sample shows how to utilize the shader variables, in particular, local variables.  
//		In this example, we have a model with two textures - an undamaged face and a damaged
//		face.  A simple shader exists that blends between the two textures based on the amount
//		of damage applied.  The default assets are old ps2 assets, and the damaged texture is
//		programmer art, so don't judge the visuals based on beauty :)

#include "sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "file/asset.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

PARAM(file,"[sample_shadervariables] The filename of the .type file to load (default entityfx.type)");

class rmcSampleShaderVariables : public ragesamples::rmcSampleManager
{
public:
	rmcSampleShaderVariables() : 
		m_Drawable(0),
		m_Skeleton(0),
		m_MatrixSet(0),
		m_DamageId(grmsgvNONE)
	{
		SetDefaultFileName(0, "entityfx");
	}

protected:
	void InitClient()
	{
		rmcSampleManager::InitClient();

		// STEP #1. Load our drawable as usual

		//-- See the sample_drawable example for details

		// -STOP

		// The first thing we do is allocate our <c>rmcDrawable</c> instance.
		if ((m_Drawable = rmcDrawable::LoadResource(GetFileName(0),GetFileName(0))) == NULL) {
			m_Drawable = rage_new rmcDrawable();

			// Now we load the drawable.  
			if( !m_Drawable->Load(GetFileName(0)) )
				Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );
		}

		// The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		crSkeletonData *skeldata = m_Drawable->GetSkeletonData();
		m_Skeleton = rage_new crSkeleton();
		if (skeldata) {
			m_Skeleton->Init(*skeldata,NULL);
			m_MatrixSet = grmMatrixSet::Create(*m_Skeleton);
			m_MatrixSet->Update(*m_Skeleton,true);
		}

		// STEP #2. Lookup the shader variables we care about
		
		//-- In this example, we are only looking for a variable named "damage",
		// and this variable is local to the shader.
		m_DamageId = m_Drawable->GetShaderGroup().LookupVar("Damage");
		// -STOP
	}

	void InitCamera()
	{
		// Pick a location closer to the model
		Vector3 midPoint(m_Drawable->GetLodGroup().GetCullSphere());
		Vector3 camPos;
		float cullRadius = m_Drawable->GetLodGroup().GetCullRadius();
		camPos.Set( 0.f, cullRadius * 0.7f, cullRadius * 0.5f);
		camPos.Add( midPoint );
		Vector3 target(midPoint);
		target.y += cullRadius * 0.9f;
		rmcSampleManager::InitSampleCamera(camPos, target);

	}

	void ShutdownClient()
	{
		delete m_Skeleton;
		delete m_MatrixSet;
		delete m_Drawable;
	}

	void Update()
	{
		// STEP #3. Store the value to use in the variable

		// Valid handles are nonzero; the reserved invalid handle grmsgvNONE is zero.
		if ( m_DamageId )
		{
			// We have to know the type, in this case it's a normalized float (0-1)
			// So, for now, we'll just use a sin function to show the variable transitioning
			float damage = sinf(TIME.GetElapsedTime()) * 0.5f + 0.5f;
			m_Drawable->GetShaderGroup().SetVar(m_DamageId, damage);
		}
		// -STOP
	
		rmcSampleManager::Update();
	}

	void DrawClient()
	{
		// STEP #4. Render our character as usual

		//-- See the sample_drawable example for details

		// -STOP		

		// Disable lighting
		grcLighting(false);

		// Our character is at the origin, so we just create a identity matrix
		// for the visibility check.
		Matrix34 center;
		center.Identity();

		u8 lod;
		// First we do a visibility check versus the view frustrum.  If it 
		// is visible, the level of detail is returned in <c>lod</c>.
		if (m_Drawable->IsVisible(center,*grcViewport::GetCurrent(),lod)) {

			// Here we set the texture LOD based on the character's distance
			// from the viewer.
			float zDist = GetCamMgr().GetWorldMtx().d.Mag();
			grcTextureFactory::GetInstance().SetTextureLod(zDist);

			// Draw the character. If it has a skeletondata, then we assume
			// it's a skinned character and use <c>rmcDrawable::DrawSkinned()</c>;
			// otherwise, we use <c>rmcDrawable::Draw()</c>.
			for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
				if (GetBucketEnable(bucket)) {
					if (m_MatrixSet)
						m_Drawable->DrawSkinned(center,*m_MatrixSet,BUCKETMASK_GENERATE(bucket),lod);
					else
						m_Drawable->Draw(center,BUCKETMASK_GENERATE(bucket),lod);
				}
		}
	}

#if __BANK
	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
	}
#endif

private:
	rmcDrawable*			m_Drawable;
	crSkeleton*				m_Skeleton;
	grmMatrixSet*			m_MatrixSet;
	grmShaderGroupVar		m_DamageId;
};

// main application
int Main()
{
	rmcSampleShaderVariables sampleRmc;
	sampleRmc.Init("sample_rmcore/shadervariables");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
