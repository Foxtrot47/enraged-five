// 
// sample_rmcore\sample_swaptexdict.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// TITLE: SwapTexDict
// PURPOSE:
//		This sample shows how to swap between different texture dictionaries on a drawable.

#include "sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "file/asset.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

PARAM(file,"[sample_swaptexdict] The filename of the .type file to load (default entityfx.type)");


class rmcSampleSwapTexDict: public ragesamples::rmcSampleManager
{
public:
	rmcSampleSwapTexDict() : 
		m_Drawable(0),
		m_Skeleton(0),
		m_MatrixSet(0),
		m_Normal(0),
		m_Proxy(0),
		m_Frame(0)
	{
		SetDefaultFileName(0, "entity");
	}

protected:
	void InitClient()
	{
		rmcSampleManager::InitClient();

		// STEP #1. Load our drawable as usual

		//-- See the sample_drawable example for details
		// One thing we do differently here is turn on automatic texture dictionary
		// generation so that we have something to swap with!
		grmShaderGroup::SetAutoTexDict(true);

		// -STOP

		// The first thing we do is allocate our <c>rmcDrawable</c> instance.
		if ((m_Drawable = rmcDrawable::LoadResource(GetFileName(0),GetFileName(0))) == NULL) {
			m_Drawable = rage_new rmcDrawable();

			// Now we load the drawable.  
			if( !m_Drawable->Load(GetFileName(0)) )
				Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );
		}

		// The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		crSkeletonData *skeldata = m_Drawable->GetSkeletonData();
		m_Skeleton = rage_new crSkeleton();
		if (skeldata) {
			m_Skeleton->Init(*skeldata,NULL);
			m_MatrixSet = grmMatrixSet::Create(*m_Skeleton);
			m_MatrixSet->Update(*m_Skeleton,true);
		}

		// STEP #2. Load the alternative texture dictionaries.
		pgRscBuilder::Load(m_Normal,"resourceTestNormal","#td",grcTexture::RORC_VERSION);
		pgRscBuilder::Load(m_Proxy,"resourceTestProxy","#td",grcTexture::RORC_VERSION);
		
		// -STOP
	}

	void InitCamera()
	{
		// Pick a location closer to the model
		Vector3 midPoint(m_Drawable->GetLodGroup().GetCullSphere());
		Vector3 camPos;
		float cullRadius = m_Drawable->GetLodGroup().GetCullRadius();
		camPos.Set( 0.f, cullRadius * 0.7f, cullRadius * 0.5f);
		camPos.Add( midPoint );
		Vector3 target(midPoint);
		target.y += cullRadius * 0.9f;
		rmcSampleManager::InitSampleCamera(camPos, target);

	}

	void ShutdownClient()
	{
		m_Normal->Release();
		m_Proxy->Release();

		delete m_Skeleton;
		delete m_MatrixSet;
		delete m_Drawable;
	}

	void Update()
	{
		++m_Frame;
		if (m_Frame == 120)
			m_Drawable->GetShaderGroup().SetTexDict(m_Proxy);
		else if (m_Frame == 240) {
			m_Drawable->GetShaderGroup().SetTexDict(m_Normal);
			m_Frame = 0;
		}

		rmcSampleManager::Update();
	}

	void DrawClient()
	{
		// STEP #4. Render our character as usual

		//-- See the sample_drawable example for details

		// -STOP		

		// Disable lighting
		grcLighting(false);

		// Our character is at the origin, so we just create a identity matrix
		// for the visibility check.
		Matrix34 center;
		center.Identity();

		u8 lod;
		// First we do a visibility check versus the view frustrum.  If it 
		// is visible, the level of detail is returned in <c>lod</c>.
		if (m_Drawable->IsVisible(center,*grcViewport::GetCurrent(),lod)) {

			// Here we set the texture LOD based on the character's distance
			// from the viewer.
			float zDist = GetCamMgr().GetWorldMtx().d.Mag();
			grcTextureFactory::GetInstance().SetTextureLod(zDist);

			// Draw the character. If it has a skeletondata, then we assume
			// it's a skinned character and use <c>rmcDrawable::DrawSkinned()</c>;
			// otherwise, we use <c>rmcDrawable::Draw()</c>.
			for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
				if (GetBucketEnable(bucket)) {
					if (m_MatrixSet)
						m_Drawable->DrawSkinned(center,*m_MatrixSet,bucket,lod);
					else
						m_Drawable->Draw(center,bucket,lod);
				}
		}
	}

#if __BANK
	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
	}
#endif

private:
	rmcDrawable*			m_Drawable;
	crSkeleton*				m_Skeleton;
	grmMatrixSet*			m_MatrixSet;
	pgDictionary<grcTexture> *m_Proxy, *m_Normal;
	int						m_Frame;
};

// main application
int Main()
{
	rmcSampleSwapTexDict sampleRmc;
	sampleRmc.Init("sample_rmcore/drawable");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
