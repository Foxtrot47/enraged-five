// 
// sample_rmcore/sample_vertexbuffer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: Vertex / Index Buffer Sample
// PURPOSE:
//		This sample shows how to draw primitives using vertex and index buffers.

#include "sample_rmcore.h"
#include "system/main.h"
#include "system/simpleallocator.h"
#include "system/param.h"

#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grcore/device.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"

using namespace rage;

PARAM(file,"Ignored.");

class grcSampleVertexBuffer: public ragesamples::rmcSampleManager
{
	static const int gridSize = 10;
public:
	void InitClient()
	{
		rmcSampleManager::InitClient();

		// STEP #1. Set up a vertex format.
		grcFvf fvf;
		fvf.SetPosChannel(true);
		fvf.SetNormalChannel(true);
		fvf.SetDiffuseChannel(true);
		fvf.SetTextureChannel(0,true);
		m_VertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

		// STEP #2. Create a vertex buffer and fill it with some interesting data
		m_VertexBuffer = grcVertexBuffer::Create(gridSize*gridSize,fvf,true,false,NULL);
		grcVertexBufferEditor vertexBufferEditor(m_VertexBuffer);
		float cx = (gridSize-1)/-2.0f;
		float cy = (gridSize-1)/-2.0f;
		for (int i=0; i<gridSize; i++) {
			for (int j=0; j<gridSize; j++) {
				int idx = i * gridSize + j;
				Vector3 v(cx + j,sinf((i+j)* 0.25f),cy + i);
				Vector3 n(0,1,0);
				vertexBufferEditor.SetPosition(idx,v);
				vertexBufferEditor.SetNormal(idx,n);
				vertexBufferEditor.SetCPV(idx,Color32(i * 20,j * 20,0));
				vertexBufferEditor.SetUV(idx,0,Vector2(i / float(gridSize),j / float(gridSize)));
			}
		}
		vertexBufferEditor.Unlock();

		// STEP #3. Create an index buffer and fill it with data as well.
		m_IndexBuffer = grcIndexBuffer::Create((gridSize-1)*(gridSize-1)*6);
		u16 *lockPtr = m_IndexBuffer->LockWO();
		int offset = 0;
		for (int i=0; i<gridSize-1; i++) {
			for (int j=0; j<gridSize-1; j++) {
				// First triangle for cell
				Assign(lockPtr[offset++], j+gridSize*i);
				Assign(lockPtr[offset++], j+gridSize*i+1);
				Assign(lockPtr[offset++], j+gridSize*i+gridSize);
				// Second triangle for cell
				Assign(lockPtr[offset++], j+gridSize*i+1);
				Assign(lockPtr[offset++], j+gridSize*i+gridSize+1);
				Assign(lockPtr[offset++], j+gridSize*i+gridSize);
			}
		}
		m_IndexBuffer->UnlockWO();

		// STEP #4. Get a shader to render the geometry with, set up appropriate texture
		(m_Shader = grmShaderFactory::GetInstance().Create())->Load("rage_diffuse");
		m_Shader->SetVar(m_Shader->LookupVar("DiffuseTex"),grcTexture::None);
	}

	void UpdateClient()
	{
	}

	void DrawClient()
	{
		// STEP #5. Render!
		if (m_Shader->BeginDraw(grmShader::RMC_DRAW,true)) {
			m_Shader->Bind();
			GRCDEVICE.DrawIndexedPrimitive(drawTris,m_VertexDecl,*m_VertexBuffer,*m_IndexBuffer,m_IndexBuffer->GetIndexCount());
			m_Shader->UnBind();
			m_Shader->EndDraw();
		}
		else
			Errorf("BeginDraw failed.");
	}

	void ShutdownClient()
	{
		delete m_Shader;
		delete m_IndexBuffer;
		delete m_VertexBuffer;
		grmModelFactory::FreeDeclarator(m_VertexDecl);
	}

	grcVertexDeclaration *m_VertexDecl;
	grcVertexBuffer *m_VertexBuffer;
	grcIndexBuffer *m_IndexBuffer;
	grmShader *m_Shader;
	grmShaderGroup m_ShaderGroup;	// necessary to hold the locals
};


int Main()
{
	grcSampleVertexBuffer sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
