// 
// sample_rmcore\sample_resource.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Resource
// PURPOSE:
//	This sample demonstrates how to create resourced models and load and render
//	them asynchronously.  It's not very useful these days because we don't support
//  creating resources on the target platform any longer.

#if !__PAGING
#error Only paging builds are supported.
#endif

#include "sample_rmcore/sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "file/asset.h"
#include "grmodel/setup.h"
#include "rmcore/drawable.h"
#include "grmodel/modelfactory.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "paging/rscbuilder.h"
#include "paging/streamer.h"

using namespace rage;

PARAM(file,"[sample_resource] The filename of the .mesh file to load");
PARAM(raw,"[sample_resource] Load file raw instead of through resources");
#if __XENON
PARAM(testharddrive,"[sample_resource] Write resource heap to hard drive instead of T: (rfs) drive");
#endif

struct SampleDrawable {
#if 0
	SampleDrawable(const char *meshName) {
		// Load the model through the standard data path.
		m_Model = grmModelFactory::GetInstance().Create(meshName, 0, NULL);
		Assert(m_Model);

		// Load the shader group for the model.
		m_ShaderGroup = grmShaderFactory::GetInstance().LoadGroup(meshName);
		Assert(m_ShaderGroup);
	}
#endif

	SampleDrawable(datResource &/*rsc*/) {
		// datOwner takes care of fixups
	}

	~SampleDrawable() {
		delete m_Model;
		delete m_ShaderGroup;
	}

	IMPLEMENT_PLACE_INLINE(SampleDrawable)
	void PostPlace() { }

	void Draw() {
#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
		m_Model->Draw(*m_ShaderGroup,0,0);
#endif
	}

	datOwner<grmModel> m_Model;
	datOwner<grmShaderGroup> m_ShaderGroup;
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s) {
		STRUCT_BEGIN(SampleDrawable);
		STRUCT_FIELD(m_Model);
		STRUCT_FIELD(m_ShaderGroup);
		STRUCT_END();
	}
#endif
};

class rmcSampleDrawable
{
	static const int VERSION = 1;		// Increment this if the data format changes.

public:
	rmcSampleDrawable() : m_Drawable(NULL) { }
	~rmcSampleDrawable() { delete m_Drawable; }

	void InitRsc(const char *meshName)
	{
#if 0
		// STEP #1. Build a resource heap.
		for (int pass=0; pass<2; pass++) {
			//-- Begin a heap, specifying the largest we ever expect this object
			// to be up-front.
			pgRscBuilder::BeginBuild(pass?true:false,4 * 1024 * 1024);

			//-- Always make sure your toplevel object is the first thing you allocate
			// after beginning the build
			SampleDrawable *sd = rage_new SampleDrawable(meshName);

			//-- Save the build out to disc, but only on the second pass.
			// DeclareStruct is responsible for performing byte swapping and pointer rebasing.
			if (pass == 1) {
				datTypeStruct s;
				sd->DeclareStruct(s);
				pgRscBuilder::SaveBuild(meshName, "#dr", VERSION);
			}
			else {
				//-- Remove the original object (this also now frees the heap memory if it was a toplevel pointer)
				delete sd;
			}

			//-- Shut down the build
			pgRscBuilder::EndBuild(sd);
		}
#endif

		Displayf("Streaming from '%s'",meshName);

		// -STOP

		// STEP #2. Set up the streaming information.
		
		//-- By now we're guaranteed to have a valid resource heap.
		// Set up the streaming information.  Opening a streamable file is synchronous
		// on some configurations so do it ahead of time.
		AssertVerify(pgRscBuilder::OpenStream(meshName,"#dr",VERSION,m_Info));

		//-- The model is not currently loaded.
		m_Drawable = NULL;

		// -STOP
	}

	void InitRaw(const char * /*meshName*/) {
#if 0
		m_Drawable = rage_new SampleDrawable(meshName);
#endif
	}

	void PageIn()
	{
		// STEP #3. page in our drawable.

		//-- Verify we don't already have it (for clarity).
		Assert(!m_Drawable);

		//-- Schedule the streaming operation (will return immediately).
		AssertVerify(pgRscBuilder::ContinueStream(m_Info,m_Map));

		// -STOP
	}

	bool IsReady()
	{
		// STEP #4. see if our drawable has paged in.

		//-- Here we check to see if the operation is complete.
		bool blockUntilComplete = false;
		bool closeHandleWhenDone = false;
		return pgRscBuilder::EndStream(m_Drawable, m_Info, m_Map, blockUntilComplete, closeHandleWhenDone, "sample resource");

		// -STOP
	}

	void Draw()
	{
		// STEP #5. draw our drawable to the screen.

		//-- Verify that the model is in fact available
		Assert(m_Drawable);

		//-- Tell the drawable to render itself
		m_Drawable->Draw();

		// -STOP
	}

	
	void PageOut()
	{
		// STEP #6. page out our drawable.
		// Note that deleting a toplevel resource pointer will work correctly
		// because the underlying memory manager contains special checks to
		// catch addresses that it knows match resource heaps.
		delete m_Drawable;

		//-- Mark the model as unavailable.
		m_Drawable = NULL;
		
		// -STOP
	}

	void Cancel()
	{
		Assert(!m_Drawable);

		bool closeHandleWhenDone = false;
		pgRscBuilder::CancelStream(m_Info,closeHandleWhenDone);
	}

private:
	pgStreamerInfo		m_Info;
	SampleDrawable*		m_Drawable;
	datResourceMap		m_Map;
};

class rmcSampleResource : public ragesamples::rmcSampleManager
{
	rmcSampleDrawable *m_Mesh;
	int m_State, m_Frame;
public:

protected:
	void InitClient()
	{
		// Init our sample framework.
		grcSampleManager::InitClient();

		// Initialize the streamer class.  Parameter is the maximum number of
		// pending requests at once.
		pgStreamer::InitClass(16);

		// Load a sample mesh.
		bool raw = PARAM_raw.Get();
		const char *name = "Bldng_03_L1_26";	// "room" is too big to keep in cvs.
		PARAM_file.Get(name);
		m_Mesh = rage_new rmcSampleDrawable;
		if (raw) {
			m_Mesh->InitRaw(name);
			m_State = 2;
			m_Frame = 999;
		}
		else {
			m_Mesh->InitRsc(name);
			m_State = 0;
			m_Frame = 0;
		}
	}

	void ShutdownClient()
	{
		delete m_Mesh;

		pgStreamer::ShutdownClass();

		grcSampleManager::ShutdownClient();
	}

	void InitCamera()
	{
		// Pick a location closer to the model
		// Unfortunately we have no idea where that would be because
		// we haven't loaded the model yet!
		Vector3 camPos(0,0,50);
		Vector3 target(0,0,0);
		grcSampleManager::InitSampleCamera(camPos, target);
	}

	void Update()
	{
		grcSampleManager::Update();
		++m_Frame;
		if (m_State == 0 && m_Frame == 100) 
		{
			Displayf("Initiate pagein frame %d",m_Frame);
			m_Mesh->PageIn();
			m_State = 1;
		}
		else if (m_State == 1)
		{
			static int cancel;
			if (++cancel == 10) {
				Displayf("Hah!  Cancel!");
				cancel = 0;
				m_State = 0;
				m_Frame = 0;
				m_Mesh->Cancel();
			}
			else if (m_Mesh->IsReady())
			{
				Displayf("Pagein complete frame %d",m_Frame);
				m_State = 2;
			}
		}
		else if (m_State == 2 && m_Frame == 200)
		{
			Displayf("Stop using model for one frame so we can evict it");
			m_State = 3;
		}
		else if (m_State == 3)
		{
			Displayf("Evicting model");
			m_Mesh->PageOut();
			m_State = 0;
			m_Frame = 0;
		}
	}

	void DrawClient()
	{
		if (m_State == 2) {
			// sysTimer T;
			m_Mesh->Draw();
			// Displayf("draw time %f",T.GetMsTime());
		}
	}

#if __BANK
	void AddWidgetsClient()
	{
	}
#endif
};

// main application
int Main()
{
	rmcSampleResource sampleRmc;
	sampleRmc.Init("sample_rmcore/resource");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
