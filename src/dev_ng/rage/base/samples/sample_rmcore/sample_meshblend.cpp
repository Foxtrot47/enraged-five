// 
// sample_rmcore/sample_meshblend.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:
//		This sample shows how to blend two different drawables geometry at runtime

#include "sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"

#include "bank/bkmgr.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "file/packfile.h"
#include "grcore/quads.h"
#include "grcore/texturedefault.h"
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "paging/dictionary.h"
#include "rmcore/drawable.h"
#include "squish/squishspu.h"
#include "system/timemgr.h"

// Hackity hack hack, hack-hack!
#include "../../../framework/src/fwsys/fileExts.h"

/* to actually see anything, you need to modify ped_default.fxh so that unlit_draw and unlit_drawskinned both use this pixel shader:
half4 PS_PedTextured_Forward(pedVertexOutput IN, float2 vPos:VPOS): COLOR
{
	return tex2D(DiffuseSampler, IN.texCoord.xy);
}
*/

using namespace rage;

PARAM(file,"Ignored.");

#if __XENON
#define PATH "x:\\gta5\\build\\dev\\xbox360\\models\\cdimages\\"
#elif __PS3
#define PATH "x:\\gta5\\build\\dev\\ps3\\models\\cdimages\\"
#elif __WIN32PC || RSG_ORBIS
#define PATH "x:\\gta5\\build\\dev\\pc\\models\\cdimages\\"
#endif

class grcSampleMeshBlend: public ragesamples::rmcSampleManager
{
	// fiPackfile *pf;
	pgDictionary<rmcDrawable> *dds[6];
	pgDictionary<grcTexture> *td;
	atArray<rmcDrawable*> heads;
	atArray<grmGeometry*> geoms;
	grmGeometry *blendedGeom;
	grmMatrixSet *ms;
	crSkeleton *skeleton;
	float test, lastTest;
	grcRenderTarget *rt;
	sqCompressState sq;
	enum { STATE_RENDERING, STATE_STILL_RENDERING, STATE_LOCKING, STATE_COMPRESSING } state;
	u32 startTime, lastComp;
#if __WIN32PC
	static const int srcEntry1 = 13, srcEntry2 = 3, destEntry = 9;
#else
	static const int srcEntry1 = 24, srcEntry2 = 8, destEntry = 5;
#endif

public:
	void InitClient()
	{
#if __WIN32PC
		// Can't have two outstanding RT locks on PC builds right now
		m_DrawWorldAxes = false;
#endif

		rmcSampleManager::InitClient();

#if 0
		// Mount the archive containing the test assets
		pf = rage_new fiPackfile;
		pf->Init(PATH "componentpeds.rpf",true,fiPackfile::CACHE_NONE);
		pf->MountAs("z:\\");

		// Load the texture dictionary and then the drawable dictionary.
		pgRscBuilder::Load(td, "z:\\z_z_testface", TXD_FILE_EXT_PATTERN, TXD_FILE_VERSION);
		td->SetCurrent(td);
		pgRscBuilder::Load(dd, "z:\\z_z_testface", DWD_FILE_EXT_PATTERN, DWD_FILE_VERSION);
		td->SetCurrent(NULL);
#else
		// Load the texture dictionary and then the drawable dictionary.
		pgRscBuilder::Load(td, "x:\\z_z_testface", TXD_FILE_EXT_PATTERN, TXD_FILE_VERSION);
		td->SetCurrent(td);

		for (int i=0; i<6; i++) {
			char buf[64];
			formatf(buf,PATH "z_z_testface_%d",i);
			pgRscBuilder::Load(dds[i], buf, DWD_FILE_EXT_PATTERN, DWD_FILE_VERSION);
		}

		td->SetCurrent(NULL);
#endif

		Displayf("%d textures in texture dictionary.",td->GetCount());

		for (int i=0; i<6; i++) {
			// char name[32];
			// formatf(name,"head_%03d_r",i);
			rmcDrawable *d = dds[i]->GetEntry(0);
			if (d) {
				heads.PushAndGrow(d);
				geoms.PushAndGrow(&d->GetLodGroup().GetLod(LOD_HIGH).GetModel(0)->GetGeometry(0));
			}
			else
				break;
		}

		Displayf("%d head meshes found.",heads.GetCount());
		skeleton = rage_new crSkeleton();
		skeleton->Init(*heads[0]->GetSkeletonData(),NULL);
		ms = grmMatrixSet::Create(*skeleton);

		blendedGeom = &heads[1]->GetLodGroup().GetLod(LOD_HIGH).GetModel(0)->GetGeometry(0);

		m_LightMode = false;
		test = 0.5f;
		lastTest = test + 1;		// doesn't matter as long as it's different
		
		// Create a render target to hold the texture composite
		// main memory is much faster to read; if the texture is in VRAM, it takes about half again as long
		// to finish the compression and there may be glitches in the result.
		grcTexture *dst = td->GetEntry(destEntry);
		grcTextureFactory::CreateParams params;
#if __WIN32PC
		params.Lockable = true;
#elif __PS3
		params.InLocalMemory = false;		
#endif

		params.MipLevels = dst->GetMipMapCount();
		// The last two (smaller than 4x4) miplevels aren't supported but you should never
		// be able to see the difference anyway.  The reason is that the task system requires 16byte-aligned
		// data and these two levels "break" that on DXT1 textures.
		if (dst->GetBitsPerPixel() == 4)
			while (Max(dst->GetWidth(),dst->GetHeight())>>params.MipLevels < 4)
				params.MipLevels--;

		rt = grcTextureFactory::GetInstance().CreateRenderTarget("composite",grcrtPermanent,dst->GetWidth(),dst->GetHeight(),32,&params);

		state = STATE_RENDERING;
	}

	void UpdateClient()
	{
#if !__BANK
		// Animate the blend if there are no widgets available, otherwise this sample is useless
		test = fmodf(TIME.GetElapsedTime(),1.0f);
#endif
		blendedGeom->BlendPositionsFromOtherGeometries(geoms[0],geoms[5],1.0f - test,test);
	}

	void DrawClient()
	{
		int count = heads.GetCount();
		Matrix34 m;
		m.Identity();
		m.d.Set(-(count-1) * 0.125f,0,-0.5f);
		skeleton->Update();
		ms->Update(*skeleton,heads[0]->IsSkinned());
		for (int i=0; i<count; i++) {
			heads[i]->DrawSkinned(m,*ms,0,LOD_HIGH);
			m.d.x += 0.25f;
		}

		// Very simple state machine.  First frame, we render to the render target.
		// We wait a frame to make sure the GPU really has written everything back.
		// Third frame, we begin compression and remain in that state until done.
		if (state == STATE_RENDERING && test != lastTest) {
			lastTest = test;
			grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
			grcStateBlock::SetBlendState(grcStateBlock::BS_Normal);
			// Composite the render target based on master blend
			grcTextureFactory::GetInstance().LockRenderTarget(0,rt,NULL);
			grcViewport vp;
			vp.Screen();
			grcViewport *old = grcViewport::SetCurrent(&vp);
			GRCDEVICE.Clear(true,Color32(0,0,0,0),false,0,0);
			if (test != 1.0f) {
				grcBindTexture(td->GetEntry(srcEntry1));
				grcBeginQuads(1);
					grcDrawQuadf(0,0,(float)rt->GetWidth(),(float)rt->GetHeight(),0,0,0,1,1,Color32(255,255,255,255));
				grcEndQuads();
			}
			if (test) {
				grcBindTexture(td->GetEntry(srcEntry2));
				grcBeginQuads(1);
					grcDrawQuadf(0,0,(float)rt->GetWidth(),(float)rt->GetHeight(),0,0,0,1,1,Color32(255,255,255,int(test * 255)));
				grcEndQuads();
			}
			grcTextureFactory::GetInstance().UnlockRenderTarget(0);
			grcViewport::SetCurrent(old);
			rt->CreateMipMaps(NULL,0);
			state = STATE_STILL_RENDERING;
		}
		else if (state == STATE_STILL_RENDERING)
			state = STATE_LOCKING;
		else if (state == STATE_LOCKING) {
			/// IMPORTANT!  In a real game, you will need to guarantee that neither the source (usually not a problem) nor
			/// the destination is defragmented during this process.  Call MarkIsDirty on the texture dictionary.
			startTime = sysTimer::GetSystemMsTime();
			int flags = squish::kColourIterativeClusterFit;		// default is kColourClusterFit which takes about 100ms for a 128x128.
			sq.Compress(rt,td->GetEntry(destEntry),flags,0);			// may want to set up a new scheduler slot for this.
			state = STATE_COMPRESSING;
		}
		else if (state == STATE_COMPRESSING && sq.Poll()) {
			lastComp = sysTimer::GetSystemMsTime() - startTime;
			state = STATE_RENDERING;
		}

		// Debug visualization
		PUSH_DEFAULT_SCREEN();
		grcStateBlock::SetBlendState(grcStateBlock::BS_Default);
		grcBindTexture(rt);
		grcBeginQuads(1);
		grcDrawQuadf(600,50,800,250,0,0,0,1,1,Color32(255,255,255,255));
		grcEndQuads();
		grcBindTexture(td->GetEntry(destEntry));
		grcBeginQuads(1);
		grcDrawQuadf(800,50,1000,250,0,0,0,1,1,Color32(255,255,255,255));
		grcEndQuads();
		char buf[64];
		grcDraw2dText(600,35,formatf(buf,"Last compression completed in %u ms",lastComp));
		POP_DEFAULT_SCREEN();

		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
	}

	void ShutdownClient()
	{
	}

#if __BANK
	void AddWidgetsClient()
	{
		/// ragesamples::rmcSampleManager::AddWidgetsClient();
		bkBank &bk = BANKMGR.CreateBank("sample_meshblend");
		bk.AddSlider("Blend between 0 and 5",&test,0.0f,1.0f,0.1f);
	}
#endif
};

XPARAM(shaderlibs);
XPARAM(shaderdb);
namespace ragesamples { XPARAM(zup); }

int Main()
{
	PARAM_shaderlibs.Set("x:\\gta5\\build\\dev\\common\\shaders\\");
	PARAM_shaderdb.Set("x:\\gta5\\build\\dev\\common\\shaders\\db\\");
	ragesamples::PARAM_zup.Set("");

	grcSampleMeshBlend sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
