// 
// sample_rmcore/sample_instance.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define GCM_BUFFER_SIZE	4096


#include "sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "grcore/instancebuffer.h"
#include "grcore/matrix43.h"
#include "grmodel/matrixset.h"
#include "grmodel/setup.h"
#include "rmcore/instance.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grprofile/drawmanager.h"
#include "grmodel/geometry.h"
#include "grmodel/shaderfx.h"
#include "grcore/vertexBuffer.h"
#include "data/struct.h"

// For debug visualization:
#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

#define INSTANCE_METHOD 2

using namespace rage;

PARAM(file," [sample_instance] The filename of the .type file to load (default entity.type)");

class rmcSampleInstance : public ragesamples::rmcSampleManager
{
	static const int nGridSide = 15;
	static const int nInstances = nGridSide * nGridSide;

protected:
	virtual const char* GetSampleName() const
	{
		return "sample_instance";
	}

	static void ForceInstancing(const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams* p)
	{
		// A real callback would examine the shader for some sort of "supports instancing" flag or look for
		// special instancing-related techniques
		p->Edge = false;
		// PS3 never does true GPU instancing, so we don't want or need the index buffer replication.
		p->IsGpuInstanced = __XENON;
	}

	void InitClient()
	{
		m_Culling = true;
		m_DrawWorldAxes = false;

#if __PFDRAW
		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
		GetRageProfileDraw().SetEnabled(true);
#endif
		m_LightMode = true;

		m_Setup->SetShowFrameTime(true);

		rmcSampleManager::InitClient();

		grcInstanceBuffer::InitClass();

		// STEP #1. Allocate and load our drawable instance.

		//-- The first thing we do is allocate our <c>rmcDrawable</c> instance.
		m_Drawable = NULL;
		m_Drawable = rage_new rmcInstance();

		grmModel::GeometryParamsHook hook;
		hook.Reset<&ForceInstancing>();
		grmModel::SetGeometryParamsHook(hook);

		//-- Now we load the drawable.  
		if( !m_Drawable->Load(GetFileName(0)) )
			Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), GetFileName(0) );

		// STEP #2. Initialize our skeleton.

		//-- The drawable will have already loaded the skeleton data if it 
		// was specified in the .type file, but we still have to create a skeleton
		// instance.
		crSkeletonData *skeldata = m_Drawable->GetSkeletonData();
		if (skeldata)
		{
			m_Skeleton = rage_new crSkeleton();
			m_Skeleton->Init(*skeldata,NULL);
			m_MatrixSet = grmMatrixSet::Create(*m_Skeleton);

			m_Anim = 0;
		}
		else 
		{
			m_Skeleton = 0;
			m_Anim = 0;
			m_MatrixSet = 0;
		}
		// -STOP

		// STEP #3. Initialize instances
		Matrix44 mWorld(M44_IDENTITY);

#if INSTANCE_METHOD == 1
		// Our instance data is located at register 64 in the shaders the default data is using
		// Tell the rmcInstance this is where it needs to upload data to.
		m_Drawable->SetConstantStartRegister(0);

		// Reserve memory for our instance pointers
		m_Drawable->ReserveInstances(nInstances);
		for( int i = 0; i < nGridSide; i++ )
		{
			for( int j = 0; j < nGridSide; j++ )
			{
				// Allocate a new isntance data object for this instance
				rmcInstanceData* pData = rage_new rmcInstanceData();

				// Set the instance world matrix
				pData->SetWorldMatrix(mWorld);

				// Give the instance data to the rmcInstance
				m_Drawable->AddInstance(pData);

				// Move the world matrix so the next instance is in a different spot
				mWorld.d.x += 2.0f;
			}
			mWorld.d.x = 0.0f;
			mWorld.d.z += 2.0f;
		}
		// -STOP
#endif

		m_PlaybackRate = 30.0f;
		m_AutoDolly = false;
		m_AutoSlide = false;
		m_UpdateAnim = m_Anim != 0;
		m_SlideRate.Set(0.75f, 0.55f, 0.35f);
		m_RotateRate = 0.5f;
		m_SlideTimer = 0.f;
		m_RotateTimer = 0.f;
		m_FrameTimer = 0.f;
		m_VertexNormalLength = 0.02f;
		m_VertexNormalColor = Color32(1.0f,0.0f,0.0f);
		m_ModelOpacity = 1.0f;
		m_NumRenderInstancesX = 1;
		m_NumRenderInstancesY = 1;
		m_NumRenderInstancesZ = 1;
		m_TotalRenderInstances = 1;
		m_RenderDistance.Set(0.5f,0.5f,0.5f);
	}

	void ShutdownClient()
	{
#if INSTANCE_METHOD == 1
		// Cleanup all the instance data
		for( int i = 0; i < m_Drawable->GetInstanceCount(); i++ )
		{
			const rmcInstanceData* pData = (const rmcInstanceData*)m_Drawable->GetInstanceData(i);
			delete pData;
		}
#endif

#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif
		delete m_MatrixSet;
		delete m_Skeleton;
		delete m_Drawable;
		delete m_Anim;

		grcInstanceBuffer::ShutdownClass();
	}

	void UpdateClient()
	{
		//-- If we want an animation, update it here
		//Note : We don't want to update the animation if vertex normals are being drawn, so the animation resumes from the
		//frame when the draw normalss button was pressed.
		if ( m_UpdateAnim ) 
		{
			m_FrameTimer += TIME.GetSeconds() * m_PlaybackRate;
		}
		if ( m_Anim ) 
		{
			crFrame frameBuf;
			frameBuf.InitCreateBoneAndMoverDofs(m_Skeleton->GetSkeletonData(), false);
			m_Anim->CompositeFrame(fmodf(m_Anim->Convert30FrameToTime(m_FrameTimer), m_Anim->GetDuration()), frameBuf);
			frameBuf.Pose(*m_Skeleton);
		}

		m_Center.Identity();
		
		//-- If we want to update orientation, do it here
		if ( m_AutoDolly ) {
			m_RotateTimer += TIME.GetSeconds();
		}
		m_Center.RotateLocalY( Wrap(m_RotateTimer * m_RotateRate, 0.f, 2.f * PI) );
		
		//-- If we want to update position, do it here
		if ( m_AutoSlide ) {
			m_SlideTimer += TIME.GetSeconds();
		}
		m_Center.d.x = m_Drawable->GetLodGroup().GetCullRadius() * 2.0f * sinf(m_SlideTimer * m_SlideRate.x);
		m_Center.d.z = m_Drawable->GetLodGroup().GetCullRadius() * 2.0f * -sinf(m_SlideTimer * m_SlideRate.z);
		m_Center.d.y = m_Drawable->GetLodGroup().GetCullRadius() * sinf(m_SlideTimer * m_SlideRate.y);

		//-- If we have a skeleton, inform it of it's new position & update all child bones
		if ( m_Skeleton ) {
			m_Skeleton->SetParentMtx(&RCC_MAT34V(m_Center));
			m_Skeleton->Update();
			m_MatrixSet->Update(*m_Skeleton, m_Drawable->IsSkinned());
		}

#if INSTANCE_METHOD == 1
		sysTimer T;
		// Update the instance to compute the lod and visibility
		if( m_Culling )
			m_RenderLod = m_Drawable->Update(*m_Viewport);
		else
			m_RenderLod = 0;
		m_DrawableCull = T.GetUsTime();
#endif
	}

	void DrawClient()
	{
		
#if __PFDRAW
		if(m_Skeleton)
			m_Skeleton->ProfileDraw();
		if(m_Drawable)
			m_Drawable->ProfileDraw(m_Skeleton, &m_Center);

		const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
		GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
		GetRageProfileDraw().Render();
#endif
		// STEP #4. Make sure the world matrix is set to identity since the instances will have their own world matrices
		grcViewport::SetCurrentWorldIdentity();
		
		// -STOP
		
		// STEP #5. Render the instances.
		
		sysTimer T;
		// Set the texture lod to 0.
		grcTextureFactory::GetInstance().SetTextureLod(0.0f);

#if INSTANCE_METHOD == 2

		// Internally we maintain a list of buffers allocated over the last three frames
		// so that the GPU can treat the data as non-volatile.
		grcInstanceBuffer::NextFrame();

		T.Reset();

		// Construct the instance lists, sorted by LOD
		Matrix34 mWorld(M34_IDENTITY);
		grcTypedInstanceBufferList<Matrix43> lists[LOD_COUNT];
		for( int i = 0; i < nGridSide; i++ )
		{
			for( int j = 0; j < nGridSide; j++ )
			{
				u8 lod;
				if (m_Drawable->IsVisible(mWorld,*grcViewport::GetCurrent(),lod))
					lists[lod].Append().FromMatrix34(RCC_MAT34V(mWorld));

				// Move the world matrix so the next instance is in a different spot
				mWorld.d.x += 2.0f;
			}
			mWorld.d.x = 0.0f;
			mWorld.d.z += 2.0f;
		}

		m_DrawableCull = T.GetUsTimeAndReset();

		grcInstanceBuffer::BeginInstancing();
		// ...and render the visible ones
		for (int i=0; i<LOD_COUNT; i++)
			if (lists[i].GetFirst())
				m_Drawable->DrawInstanced(lists[i],i);
		grcInstanceBuffer::EndInstancing();

#else
		for( int bucket = 0; bucket < NUM_BUCKETS; bucket++ )
		{
			if( GetBucketEnable(bucket) && (m_Drawable->GetBucketMask(m_RenderLod) & (1 << bucket)) )
			{
				bool bDrawSkinned = false; // m_Drawable->GetLodGroup().GetLodModel0(m_RenderLod).GetSkinFlag();
				if( m_Drawable->GetSkeletonData() && bDrawSkinned )
					m_Drawable->DrawSkinned(m_Center, *m_MatrixSet, bucket, m_RenderLod);
				else
					m_Drawable->Draw(m_Center, bucket, m_RenderLod);
			}
		}
#endif
		m_DrawableDraw = T.GetUsTime();
		// -STOP

		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcFont::GetCurrent().Drawf(100,200,"drawable: %6.0fus cull, %6.0fus draw = %6.0fus total",m_DrawableCull,m_DrawableDraw,m_DrawableCull+m_DrawableDraw);
		grcViewport::SetCurrent(old);
	}

#if __BANK
	static void EnableAllShaders()
	{
	}

	void UpdateModelOpacity()
	{
		const Vec4V curAmbientColor = m_Lights.GetAmbient();
		m_Lights.SetAmbient(curAmbientColor.GetXf(), curAmbientColor.GetYf(), curAmbientColor.GetZf(), m_ModelOpacity);
	}

	void AddWidgetsClient()
	{
		ragesamples::rmcSampleManager::AddWidgetsClient();
		
		m_Drawable->GetShaderGroup().AddWidgets(BANKMGR.CreateBank("Shaders"));
		bkBank &bk = BANKMGR.CreateBank("rage - SampleInstance");
		bk.AddToggle("Frustum Culling", &m_Culling);
		bk.AddToggle("Auto Dolly", &m_AutoDolly);
		bk.AddSlider("Rotation Rate", &m_RotateRate, -10000.f, 10000.f, 0.01f);
		bk.AddToggle("Auto Slide", &m_AutoSlide);
		bk.AddSlider("Slide Rate", &m_SlideRate, -10000.0f, 10000.0f, 0.01f);
		if ( m_Anim ) {
			bk.AddToggle("Update Animation", &m_UpdateAnim);
			bk.AddSlider("Animation Rate (fps)", &m_PlaybackRate, -1000.0, 1000.0f, 0.01f);
		}
		bk.AddSlider("Model Opacity", &m_ModelOpacity, 0.0f, 1.0f, 0.1f, datCallback(MFA(rmcSampleInstance::UpdateModelOpacity),this));
		bk.AddButton("Enable All Shaders", (CFA(rmcSampleInstance::EnableAllShaders)));

		bk.PushGroup("Multiple Instances",false,"Group of widgets for displaying multiple instances of the current drawable");
		bk.AddSlider("Num Render Instances X",&m_NumRenderInstancesX,0,1000,1);
		bk.AddSlider("Num Render Instances Y",&m_NumRenderInstancesY,0,1000,1);
		bk.AddSlider("Num Render Instances Z",&m_NumRenderInstancesZ,0,1000,1);
		bk.AddSlider("Total # of Instances",&m_TotalRenderInstances,0,1000*1000*1000,0);
		bk.AddSlider("Render Distance",&m_RenderDistance,0.0f,10000.0f,0.5f);   
		bk.PopGroup();
	}
#endif

private:
#if INSTANCE_METHOD == 2
	rmcDrawable*			m_Drawable;
#else
	rmcInstance*			m_Drawable;
#endif
	crSkeleton*				m_Skeleton;
	grmMatrixSet*			m_MatrixSet;
	crAnimation*			m_Anim;
	Matrix34				m_Center;
	Vector3					m_SlideRate;
	float					m_PlaybackRate;
	float					m_RotateRate;
	float					m_SlideTimer;
	float					m_RotateTimer;
	float					m_FrameTimer;
	float					m_DrawableCull;
	float					m_DrawableDraw;
	float					m_VertexNormalLength;
	Color32					m_VertexNormalColor;
	bool					m_AutoDolly;
	bool					m_AutoSlide;
	bool					m_UpdateAnim;
	float					m_ModelOpacity;
	int						m_NumRenderInstancesX;
	int						m_NumRenderInstancesY;
	int						m_NumRenderInstancesZ;
	int						m_TotalRenderInstances;
	Vector3					m_RenderDistance;
	bool					m_Culling;
};

// main application
int Main()
{
	// datTypeStruct myStruct;
	// grcTextureDefault::DeclareStruct(myStruct);

	rmcSampleInstance sampleRmc;
	sampleRmc.Init("sample_plantsgrass");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
