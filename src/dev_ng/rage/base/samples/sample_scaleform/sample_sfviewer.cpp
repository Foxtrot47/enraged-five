// 
// sample_scaleform/sample_sfviewer.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "projdata.h"

#include "bank/bkmgr.h"
#include "bank/msgbox.h"
#include "grcore/im.h"
#include "grcore/quads.h"
#include "grcore/stateblock.h"
#include "math/random.h"
#include "parser/manager.h"
#include "profile/profiler.h"
#include "sample_rmcore/sample_rmcore.h"

#include "script/thread.h" // Need to include _before_ scaleform because MS #defines GetCurrentThread via system/xtl.h
#include "scaleform/scaleform.h"
#include "scr_scaleform/scaleform.h"

#include "script/wrapper.h"
#include "system/main.h"
#include "system/param.h"
#include "system/ipc.h"
#include "vector/colors.h"

using namespace rage;

PARAM(file, "name of the flash movie to load (.swf or .gfx)");
PARAM(asyncLoad, "1 = loading movies async, 0 = loading movies sync");
PARAM(asyncUpdate, "1 = update movies async, 0 = update movies sync");
PARAM(oneMoviePerThread, "1 = every movie has its own update thread, 0 = all movies share one update thread");


PF_PAGE(ScaleformViewerPage, "Scaleform Viewer");
PF_GROUP(ScaleformViewer);
PF_LINK(ScaleformViewerPage, ScaleformViewer);
PF_TIMER(UpdateMovies, ScaleformViewer);
PF_TIMER(DrawMovies, ScaleformViewer);
PF_TIMER(DrawOffscreen, ScaleformViewer);
PF_TIMER(DrawText, ScaleformViewer);


void RegisterSampleBindings();

void ShowNextMovie();
void ShowPrevMovie();



#define		OPEN_MOVIE_STACK_SIZE		16384
#define		UPDATE_MOVIE_STACK_SIZE		16384
static sysIpcThreadId s_LoadThreadId = sysIpcThreadIdInvalid;
static sysIpcThreadId s_UpdateThreadId = sysIpcThreadIdInvalid;

static void s_LoadMovieAsync(void* data);
static void s_UpdateAllMoviesAsync(void* data);
static void s_UpdateMovieAsync(void* data);

volatile int		g_ReadyToUpdate = 0;
volatile void*		g_UpdatingMovieSettings = 0;
volatile unsigned	g_TotalUpdated = 0;


//////////////////////////////////////////////////////////////////////////
// TITLE: sample_sfviewer
// PURPOSE:
//		This sample shows more advanced use of the scaleform flash player
//		and can be used for testing movies

class sfSampleManager : public ragesamples::rmcSampleManager
{
public:

	sfSampleManager()
		: m_Scaleform(NULL)
		, m_Project(NULL)
		, m_LastScreenWidth(0)
		, m_LastScreenHeight(0)
#if __BANK
		, m_ProjectBank(NULL)
#endif
		, m_RenderTarget3d(NULL)
		, m_RenderTarget2d(NULL)
		, m_DepthBuffer(NULL)
		, m_DebugDrawRenderTargets(false)
		, m_TestDrawText(false)
		, m_FontMovie(NULL)
		, m_LoadingMovieIdx(-1)
		, m_End(false)
		, m_DrawFlashIn3d(false)
	{
		m_ObjectTranslate.Zero();
		m_ObjectRotateEulers.Zero();
		m_ObjectScale.Set(1.0f);
		m_ObjectMatrix.Identity();

		sm_Instance = this;
		m_ProjectPath[0] = '\0';
		EnableWorldAxesDraw(false);
		EnableDrawHelp(false);

		m_DrawText1 = m_DrawText2 = m_DrawText3 = NULL;
	}

	~sfSampleManager()
	{
		KillLoadThread();
		KillUpdateThread();
		sm_Instance = NULL;
	}

	void KillLoadThread()
	{
		if( s_LoadThreadId != sysIpcThreadIdInvalid )
		{
			sysIpcWaitThreadExit(s_LoadThreadId);
			s_LoadThreadId = sysIpcThreadIdInvalid;
		}
	}

	void KillUpdateThread()
	{
		m_End = true;
		if( s_UpdateThreadId != sysIpcThreadIdInvalid )
		{
			sysIpcWaitThreadExit(s_UpdateThreadId);
			s_UpdateThreadId = sysIpcThreadIdInvalid;
		}
		m_End = false;
	}	

	virtual void InitClient()
	{
		INIT_PARSER;

		rmcSampleManager::InitClient();

		CreateOffscreenRenderTargets();

		scrThread::InitClass();
		scrThread::RegisterBuiltinCommands();

		// -- Create a new scaleform manager and initialize it
		m_Scaleform = rage_new sfScaleformManager;
		m_Scaleform->Init(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL));
		m_Scaleform->InitExternalInterface(*(new sfCallRageScriptFromFlash));

		// -- Add the rage script bindings for use from rage script or the rag console
		sfRageScriptBindings::Register(m_Scaleform);

		RegisterSampleBindings();

		// -- Now load a movie.
		const char* filename = NULL;
		if (PARAM_file.Get(filename))
		{
			LoadCommandlineFile(filename);
		}
		else
		{
			CreateNewProject();
		}
		
		u32 asyncUpdate = 0;	
		PARAM_asyncUpdate.Get(asyncUpdate);
		Assert( m_Project );
		if( asyncUpdate )
		{
			m_Project->m_AsyncUpdate = true;
			StartUpdate();
		}

		u32 oneMoviePerThread = 0;
		PARAM_oneMoviePerThread.Get(oneMoviePerThread);
		if( oneMoviePerThread )
		{
			m_Project->m_OneMoviePerThread = true;
		}
		
	}

	void LoadCommandlineFile(const char* filename)
	{
		Assertf(!m_Project, "Hey there's already a project... are you sure you're loading a command line-specified file?");

		if (!filename)
		{
			return;
		}

		const char* extn = ASSET.FindExtensionInPath(filename);
		if (!strcmp(extn, ".sfproj"))
		{
			LoadProjectFile(filename);
			return;
		}

		if (!strcmp(extn, ".swf") || !strcmp(extn, ".gfx"))
		{
			// Create a new project and load this file
			CreateNewProject();
			AddFileToProject(filename);
		}
	}

	void CreateNewProject()
	{
		Assertf(!m_Project, "Need to unload the current project first");
		m_Project = rage_new sfPlayerProject;
		m_ProjectPath[0] = '\0';
		CreateProjectWidgets();
	}


	void RequestMovieAsync( int movieIndex )
	{
		Assert( m_Project && movieIndex > -1 && movieIndex < m_Project->GetNumMovies() );
		const int loadingMovieIndex = GetLoadingMovieIndex();
		if( loadingMovieIndex == -1 )
		{
			KillLoadThread();
			
			SetLoadingMovieIndex( movieIndex );
			s_LoadThreadId = sysIpcCreateThread( s_LoadMovieAsync, &m_Project->GetSettings( movieIndex ), OPEN_MOVIE_STACK_SIZE, PRIO_NORMAL,"[RAGE] OpenMovie Common");
		}
		else
		{
			Printf("Movie with index %d is still loading. \n", loadingMovieIndex );
		}
	}


	void RequestMovieAsync( const char* movieName )
	{
		Assert( m_Project && movieName );
		const int movieIndex = FindMovieIndex( movieName );

		Assert( movieIndex > -1 );
		RequestMovieAsync( movieIndex );
	}

	void StartUpdate()
	{
		KillUpdateThread();

		s_UpdateThreadId = sysIpcCreateThread( s_UpdateAllMoviesAsync, sfSampleManager::GetInstance(), UPDATE_MOVIE_STACK_SIZE, PRIO_NORMAL,"[RAGE] UpdateMovie Common");
	}

	
	const int FindMovieIndex ( const char* movieName ) const
	{
		Assert( m_Project && movieName );
		for(int i = 0; i < m_Project->GetNumMovies(); i++)
		{
			sfMovieViewSettings& settings = m_Project->GetSettings(i);
			if( settings.m_MovieName.c_str() == movieName)
				return i;
		}
		return -1;
	}


	void LoadProjectFile(const char* filename)
	{
		if (!PARSER.LoadObjectPtr(filename, "sfproj", m_Project))
		{
			Errorf("Coudln't load project file %s", filename);
			return;
		}

		if (!m_Project)
		{
			return;
		}

		safecpy(m_ProjectPath, filename);

		/* No more string table support, we'll use the North string library instead */
#if 0
		if (ASSET.Exists(m_Project->m_StringTableName, "strtbl"))
		{
			Assertf(!m_Project->m_StringTable, "Old string table hasn't been cleaned up!");
			m_Project->m_StringTable = rage_new txtStringTable;
			m_Project->m_StringTable->Load(m_Project->m_StringTableName, m_Project->m_Language, 0xFFFFFFFF, false, false, false);
			m_Scaleform->GetTranslator().SetStringTable(m_Project->m_StringTable);
		}
#endif

		u32 asyncLoading = 0;
		PARAM_asyncLoad.Get(asyncLoading);
		if( asyncLoading )
		{
			RequestMovieAsync( 0 );
		}
		else
		{
			for(int i = 0; i < m_Project->GetNumMovies(); i++)
			{
				sfMovieViewSettings& settings = m_Project->GetSettings(i);

				// need to see if this movie is already loaded, or just let sf handle it internally?
				sfScaleformMovie* movie = LoadMovie(settings.m_MovieName.c_str());
				if (!movie)
				{
					Errorf("Couldn't load movie %s", settings.m_MovieName.c_str());
					continue;
				}

				sfScaleformMovieView* movieView = m_Scaleform->CreateMovieView(*movie);
				settings.SetMovieView( movieView );
				if ( !movieView )
				{
					Errorf("Couldn't create instance of movie %s", settings.m_MovieName.c_str());
				}
			}

			UpdateAllMovieSettings();
		}

		CreateProjectWidgets();
	}

	void UpdateAllMovieSettings()
	{
		if (!m_Project)
		{
			return;
		}
		for(int i = 0; i < m_Project->GetNumMovies(); i++)
		{
			sfMovieViewSettings& settings = m_Project->GetSettings(i);
			settings.UpdateSettings();
		}
	}

	void UpdateMovieTime(sfMovieViewSettings* settings, sfScaleformManager* scaleform)
	{
		Assert( settings && scaleform );

		float updateTime = 0.0f;

		sfScaleformMovieView* movieView = settings->GetMovieView();
		Assert( movieView );

		sfMovieViewSettings::PlaybackRate rateType = settings->m_PlaybackRate;
		switch(rateType)
		{
		case sfMovieViewSettings::WALL_TIME:
			updateTime = TIME.GetSeconds();
			break;
		case sfMovieViewSettings::FRAME_PER_FRAME:
			updateTime = 1.0f / movieView->GetMovieView().GetFrameRate();
			break;
		case sfMovieViewSettings::FIXED_THIRTY_HZ:
			updateTime = 1.0f / 30.0f;
			break;
		case sfMovieViewSettings::ONE_HZ:
			updateTime = TIME.GetSeconds() / movieView->GetMovieView().GetFrameRate();
			break;
		}

		movieView->ApplyInput( scaleform->GetInput() );			
		movieView->Update(updateTime);
	}


	void AddFileToProject(const char* filename)
	{
		Assertf(m_Project, "No project to add file to");

		sfMovieViewSettings settings;
		settings.m_MovieName = filename;

		// need to see if this movie is already loaded, or just let sf handle it internally?
		sfScaleformMovie* movie = LoadMovie(filename);
		if (!movie)
		{
			Errorf("Couldn't load movie %s", filename);
			return;
		}

		sfScaleformMovieView* movieView = m_Scaleform->CreateMovieView(*movie);
		settings.SetMovieView( movieView );
		if ( !movieView )
		{
			Errorf("Couldn't create instance of movie %s", filename);
			return;
		}

		settings.m_BackAlpha = movieView->GetMovieView().GetBackgroundAlpha();

		m_Project->m_MovieViews.PushAndGrow(settings);

		CreateProjectWidgets();
	}

	virtual void UpdateClient()
	{
		rmcSampleManager::UpdateClient();

		PF_FUNC(UpdateMovies);

		if (!m_Project)
		{
			return;
		}

		if( !m_Project->m_AsyncUpdate )
		{
			m_Scaleform->UpdateManagerOnly();

			for(int i = 0; i < m_Project->GetNumMovies(); i++)
			{
				if ( m_LoadingMovieIdx == i ||
					!m_Project->GetMovieView(i) || 
					!m_Project->GetMovieView(i)->GetIsActive())
				{
					continue;
				}

				sfMovieViewSettings& settings = m_Project->GetSettings(i);
				UpdateMovieTime( &settings, m_Scaleform );
			}
		}
		else
		{
			if( m_Project->m_OneMoviePerThread )
			{
				if( s_UpdateThreadId != sysIpcThreadIdInvalid )
				{
					KillUpdateThread();
				}
				
				for(int i = 0; i < m_Project->GetNumMovies(); i++)
				{
					if ( m_LoadingMovieIdx == i || !m_Project->GetMovieView(i) || !m_Project->GetMovieView(i)->GetIsActive())
					{
						continue;
					}

					sfMovieViewSettings& settings = m_Project->GetSettings(i);
					if( settings.m_UpdateThreadId == sysIpcThreadIdInvalid )
					{
						settings.m_UpdateThreadId = sysIpcCreateThread( s_UpdateMovieAsync, &settings, UPDATE_MOVIE_STACK_SIZE, PRIO_NORMAL,"[RAGE] OpenMovie Private");
					}
				}
			}		
			else
			{
				for(int i = 0; i < m_Project->GetNumMovies(); i++)
				{
					if ( m_LoadingMovieIdx == i || !m_Project->GetMovieView(i) || !m_Project->GetMovieView(i)->GetIsActive())
					{
						continue;
					}

					sfMovieViewSettings& settings = m_Project->GetSettings(i);					
					if( settings.m_UpdateThreadId != sysIpcThreadIdInvalid )
					{
						settings.KillUpdateThread();
					}
				}

				if( s_UpdateThreadId == sysIpcThreadIdInvalid )
				{
					g_TotalUpdated = 0;
					StartUpdate();			
				}
			}
			
		}
	}

	struct CompareDrawOrder
	{
		bool operator()(sfMovieViewSettings* const a, sfMovieViewSettings* const b)
		{
			return a->m_DrawOrder < b->m_DrawOrder;
		}
	};

	virtual void DrawClient()
	{
		rmcSampleManager::DrawClient();

		Mat34V scale, rotate;

		Mat34VFromScale(scale, RCC_VEC3V(m_ObjectScale));
		Mat34VFromEulersXYZ(rotate, RCC_VEC3V(m_ObjectRotateEulers));

		Transform(RC_MAT34V(m_ObjectMatrix), rotate, scale);
		m_ObjectMatrix.d = m_ObjectTranslate;

		grcDrawAxis(1.0f, m_ObjectMatrix, true);

		// Copy the viewport data out (we change the viewport later - so grab the info now)
		//grcViewport* vp = grcViewport::GetCurrent();

		DrawOffscreenRenderTargets();

		// for PC - check to see if the screen changed size
		if (m_LastScreenWidth != GRCDEVICE.GetWidth() ||
			m_LastScreenHeight != GRCDEVICE.GetHeight())
		{
			m_LastScreenWidth = GRCDEVICE.GetWidth();
			m_LastScreenHeight = GRCDEVICE.GetHeight();
			UpdateAllMovieSettings();
		}

		m_Scaleform->BeginFrame();

		// -- Set up a default render state
		grcStateBlock::Default();
		grcViewport::SetCurrent(grcViewport::GetDefaultScreen());

		if (!m_Project)
		{
			m_Scaleform->EndFrame();
			return;
		}


		PF_START(DrawMovies);

		// Draw in project order, not in whatever the sfManager's internal order is.

		sfMovieViewSettings** sortedSettings = Alloca(sfMovieViewSettings*, m_Project->GetNumMovies());

		for(int i = 0; i < m_Project->GetNumMovies(); i++)
		{
			sortedSettings[i] = &m_Project->GetSettings(i);
		}

		std::sort(sortedSettings, sortedSettings + m_Project->GetNumMovies(), CompareDrawOrder());		


// Note: When drawing a lot of movies ( 10 for example ), rendering will go ahead of updating
// It is possible rendering to catch up with previous update before previous update has finished yet ->
// -> rendering has to wait for the update to finish first.
// it is possible to render one movie and update another at the same time from different threads.
// it is impossible to render and update the same movie at the same time from different threads -> will crash in scaleform code

// here the code basically wait for the update thread to finish first and then will continue with rendering

// it is possible to update every movie in its own thread but again make sure update and render are not happening at the same time



		if( m_Project->m_AsyncUpdate )
		{
			if( !m_Project->m_OneMoviePerThread  )
			{
				while ( g_ReadyToUpdate ) 
				{}
			}
			else
			{
				unsigned int totalActiveMovies = 0;

				for(int i = 0; i < m_Project->GetNumMovies(); i++)
				{
					if ( m_LoadingMovieIdx == i || !m_Project->GetMovieView(i) || !m_Project->GetMovieView(i)->GetIsActive())
					{
						continue;
					}

					totalActiveMovies++;
				}

				while ( g_TotalUpdated < totalActiveMovies ) 
				{}
			}
			
		}		


		for(int i = 0; i < m_Project->GetNumMovies(); i++)
		{
			if ( m_LoadingMovieIdx == i || !sortedSettings[i]->GetMovieView() || !sortedSettings[i]->GetMovieView()->GetIsVisible() )
			{
				continue;
			}
					
			sfMovieViewSettings& settings = *sortedSettings[i];

			if (m_DrawFlashIn3d)
			{
				settings.GetMovieView()->GetMovieView().SetBackgroundAlpha(0.0f);

				settings.GetMovieView()->DrawInWorldSpace(*m_Scaleform);
			}
			else
			{
				settings.GetMovieView()->Draw();
			}				


		}

		if( m_Project->m_AsyncUpdate  )
		{
			if( !m_Project->m_OneMoviePerThread )
			{
				g_ReadyToUpdate = 1;
			}
			else
			{
				g_TotalUpdated = 0;

				for(int i = 0; i < m_Project->GetNumMovies(); i++)
				{
					if ( m_LoadingMovieIdx == i || !sortedSettings[i]->GetMovieView() || !sortedSettings[i]->GetMovieView()->GetIsVisible() )
					{
						continue;
					}

					sfMovieViewSettings& settings = *sortedSettings[i];
					sysIpcSignalSema(settings.m_ReadyToUpdate);					
				}

			}
		}


		PF_STOP(DrawMovies);

		if (m_DebugDrawRenderTargets)
		{
			// Blit the offscreen targets onto the back buffer - for testing
			grcStateBlock::SetStates(grcStateBlock::RS_NoBackfaceCull, grcStateBlock::DSS_Default, grcStateBlock::BS_Default);
			grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
			grcBindTexture(m_RenderTarget2d);
			grcBeginQuads(1);
			grcDrawQuadf(0.0f, 0.0f, 256.0f, 256.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, Color_white);
			grcEndQuads();

			grcBindTexture(m_RenderTarget3d);
			grcBeginQuads(1);
			grcDrawQuadf(256.0f, 0.0f, 512.0f, 256.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, Color_white);
			grcEndQuads();
		}

		if (m_TestDrawText && m_FontMovie)
		{
			TestDrawText();
		}

		m_Scaleform->EndFrame();
	}

	virtual void TestDrawText()
	{
		PF_FUNC(DrawText);

		sfScaleformManager::AutoLockDrawTextManager text(*m_Scaleform);

		if (!m_DrawText1)
		{
			GRectF rect;
			rect.SetRect(100, 400, 400, 700);

			// You can build a TextParams object to specify the font
			GFxDrawTextManager::TextParams font;
			font.FontName = "Tw Cen MT Condensed Extra Bold";
			font.TextColor.SetRGBA(0x00, 0x00, 0x00, 0xFF);
			font.FontSize = 16;
			font.Multiline = true;
			font.WordWrap = true;

			// GFxDrawText can handle raw text (utf8 or utf16) or HTML
			m_DrawText1 = text.GetDrawTextManager()->CreateHtmlText(
				"<font size='30' color='#0000FF'>L</font>orem ipsum dolor sit amet, consectetur <i>adipiscing elit. Sed at ante. Mauris eleifend</i>, quam a vulputate dictum, massa quam <font color=\"#FF0000\">dapibus</font> leo, <font color=\"#80FF80\">eget vulputate orci purus ut lorem</font>. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.",
				rect,
				&font);
		}

		if (!m_DrawText2)
		{
			// You can omit the TextParams object and set the properties directly on the drawtext object
			m_DrawText2 = text.GetDrawTextManager()->CreateText();
			m_DrawText2->SetFont("Eurostile Next LT Pro");
			m_DrawText2->SetFontSize(100);
			m_DrawText2->SetColor(GColor(0x80, 0x80, 0xFF, 0x80));
			GMatrix2D mtx = GMatrix2D::Rotation(0.5f);
			m_DrawText2->SetMatrix(mtx);
		}
		if (!m_DrawText3)
		{
			GFxDrawTextManager::TextParams font;
			font.FontName = "Rage Italic";
			font.TextColor.SetRGBA(0xBB, 0x44, 0x00, 0xFF);
			font.FontSize = 40;
			m_DrawText3 = text.GetDrawTextManager()->CreateText("", GRectF(0, 0, 800, 800), &font);
		}


		GViewport vp(GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight(), 0, 0, GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
		text.GetDrawTextManager()->BeginDisplay(vp);

		// Text Block 1 - draw some big blocks of HTML text
		// (one is in the HTML-specified color, the other we re-tint to a dark yellow)
		// Note that changing the color transform and matrix do NOT cause a re-layout of the text,
		// so can be done very cheaply

		m_DrawText1->SetCxform(GFxDrawText::Cxform::Identity);
		m_DrawText1->SetMatrix(GMatrix2D::Identity);
		m_DrawText1->Display();

		GFxDrawText::Cxform turnTextYellow; // [RGBA][*+]
		turnTextYellow.M_[0][0] = 0.0f;
		turnTextYellow.M_[1][0] = 0.0f;
		turnTextYellow.M_[2][0] = 0.0f;
		turnTextYellow.M_[3][0] = 0.5f;

		turnTextYellow.M_[0][1] = 120.0f;
		turnTextYellow.M_[1][1] = 120.0f;
		turnTextYellow.M_[2][1] = 0.0f;
		turnTextYellow.M_[3][1] = 0.0f;


		m_DrawText1->SetCxform(turnTextYellow); // overrides the <font>-set color
		m_DrawText1->SetMatrix(GMatrix2D::Translation(300, -100));
		m_DrawText1->Display();


		// Text Block 2 - use a GFxDrawText with a rotation matrix applied
		// and change the rect and text to change what gets drawn
		// Note that this is pretty much a pessimal use of GFxDrawText - it has some caches
		// it can maintain from frame to frame for things like glyph batches that we're not
		// giving it a chance to do here because we're changing the text each frame - forcing a re-layout

		m_DrawText2->SetRect(GRectF(100, 0, 600, 700));
		m_DrawText2->SetText("BIG");
		m_DrawText2->Display();

		m_DrawText2->SetRect(GRectF(130, 50, 600, 700));
		m_DrawText2->SetText("TEXT");
		m_DrawText2->Display();

		// Text Block 3 - Pretty much the same as Text Block 2 - we randomly change
		// the string we draw and the position for 50 different strings.
		static const char* strings[8] = 

		{
			"Lorem",
			"ipsum",
			"dolor",
			"sit",
			"amet",
			"consectetur",
			"adipiscing",
			"elit"
		};

		mthRandom rands;
		rands.SetFullSeed(10); //TIME.GetFrameCount() / 100);
		for(int i = 0; i < 50; i++)
		{
			GRectF position(
				(float)rands.GetRanged(0, GRCDEVICE.GetWidth()), 
				(float)rands.GetRanged(0, GRCDEVICE.GetHeight()),
				(float)GRCDEVICE.GetWidth(),
				(float)GRCDEVICE.GetHeight());
			m_DrawText3->SetRect(position);
			m_DrawText3->SetText(strings[rands.GetRanged(0, 7)]);
			m_DrawText3->Display();
		}

		text.GetDrawTextManager()->EndDisplay();
	}

	virtual void AddWidgetsClient()
	{
#if __BANK
		bkBank& bank = BANKMGR.CreateBank("Scaleform Viewer");
		bank.AddButton("New Project...", datCallback(MFA(sfSampleManager::NewProjectCB), this));
		bank.AddButton("Load Project...", datCallback(MFA(sfSampleManager::LoadProjectCB), this));
		bank.AddButton("Save Project", datCallback(MFA(sfSampleManager::SaveProjectCB), this));
		bank.AddButton("Save Project As...", datCallback(MFA(sfSampleManager::SaveProjectAsCB), this));
		bank.AddToggle("Debug Draw Render Targets", &m_DebugDrawRenderTargets);
		bank.AddButton("Load Font Movie...", datCallback(MFA(sfSampleManager::LoadFontMovieCB), this));
		bank.AddToggle("Test DrawText", &m_TestDrawText);

		bank.AddToggle("Draw in 3d", &m_DrawFlashIn3d);
		bank.AddVector("3d Translation", &m_ObjectTranslate, -10000.0f, 10000.0f, 0.01f);
		bank.AddVector("3d Rotation", &m_ObjectRotateEulers, -10000.0f, 10000.0f, 0.01f);
		bank.AddVector("3d Scale", &m_ObjectScale, -10000.0f, 10000.0f, 0.01f);

		m_ProjectBank = &BANKMGR.CreateBank("Scaleform Viewer Project");
		CreateProjectWidgets();
#endif
	}

	void CreateProjectWidgets()
	{
#if __BANK
		if (!m_ProjectBank)
		{
			return;
		}

		m_ProjectBank->Truncate(0);

		if (!m_Project)
		{
			m_ProjectBank->AddTitle("No project");
			return;
		}

		m_ProjectBank->AddTitle(m_ProjectPath);
		m_ProjectBank->AddButton("Add Movie...", datCallback(MFA(sfSampleManager::AddMovieCB), this));

		PARSER.AddWidgets(*m_ProjectBank, m_Project);

		m_ProjectBank->AddButton("Next Movie (slideshow)", datCallback(CFA(ShowNextMovie)));
		m_ProjectBank->AddButton("Prev Movie (slideshow)", datCallback(CFA(ShowPrevMovie)));

		// Create the groups ourselves because the parser calls em all 'item'
		for(int i = 0; i < m_Project->GetNumMovies(); i++)
		{
			m_ProjectBank->PushGroup(m_Project->GetSettings(i).m_MovieName.c_str());
			m_ProjectBank->AddButton("Reload", datCallback(MFA1(sfSampleManager::ReloadMovieCB), this, (CallbackData)(size_t)i));
			m_ProjectBank->AddButton("Delete", datCallback(MFA1(sfSampleManager::DeleteMovieCB), this, (CallbackData)(size_t)i));
			PARSER.AddWidgets(*m_ProjectBank, &m_Project->GetSettings(i));
			m_ProjectBank->PopGroup();
		}
#endif
	}

	void AddMovieCB()
	{
#if __BANK
		char newfile[RAGE_MAX_PATH];
		newfile[0] = '\0';

		if (BANKMGR.OpenFile(newfile, RAGE_MAX_PATH, "*.gfx;*.swf", false, "Scaleform Movie Files (*.gfx; *.swf)"))
		{
			AddFileToProject(newfile);
			CreateProjectWidgets();
		}
#endif
	}

	void LoadFontMovieCB()
	{
#if __BANK
		char newfile[RAGE_MAX_PATH];
		newfile[0] = '\0';
		if (BANKMGR.OpenFile(newfile, RAGE_MAX_PATH, "*.gfx;*.swf", false, "Scaleform Movie Files (*.gfx; *.swf)"))
		{
			LoadFontMovie(newfile);
		}
#endif
	}

	void LoadFontMovie(const char* moviename)
	{
		Assertf(!m_FontMovie, "You already have a font movie");
		sfScaleformManager::AutoLockDrawTextManager text(*m_Scaleform);
		if (text.GetDrawTextManager())
		{
			m_Scaleform->ShutdownDrawTextManager();
		}
		m_FontMovie = m_Scaleform->LoadMovie(moviename);
		if (m_FontMovie)
		{
			m_Scaleform->InitDrawTextManager(*m_FontMovie);
		}
	}

	void UnloadFontMovie()
	{
		sfScaleformManager::AutoLockDrawTextManager text(*m_Scaleform);
		if (text.GetDrawTextManager())
		{
			m_Scaleform->ShutdownDrawTextManager();
		}
		if (m_FontMovie)
		{
			m_Scaleform->DeleteMovie(m_FontMovie);
			m_FontMovie = NULL;
			m_TestDrawText = false;
		}
	}

	void ReplaceTextureReference(sfScaleformMovie* movie, const char* refName)
	{
		// see if we loaded in any images that we want to replace with render targets
		GFxResource* resource = movie->GetMovie().GetResource(refName);
		if (resource && resource->GetResourceType() == GFxResource::RT_Image)
		{
			GFxImageResource* imageRes = (GFxImageResource*)resource;

			// build a new texture
			grcTexture* nativeTexture = grcTextureFactory::GetInstance().LookupTextureReference(refName);
			if (nativeTexture)
			{
				GTexture* texture = m_Scaleform->GetRenderer().CreateTexture(nativeTexture);
				if (texture)
				{
					GImageInfo* imageInfo = (GImageInfo*)imageRes->GetImageInfo();
					imageInfo->SetTexture(texture, imageInfo->GetWidth(), imageInfo->GetHeight());

					texture->Release(); // don't need to hold a reference to the texture, the movie will hold one
				}
			}
			else
			{
				Warningf("Couldn't find a texture named %s", refName);
			}
		}

	}

	sfScaleformMovie* LoadMovie(const char* filename)
	{
		sfScaleformMovie* movie = m_Scaleform->LoadMovie(filename);

		if (movie)
		{
			ReplaceTextureReference(movie, "rendertarget_2d");
			ReplaceTextureReference(movie, "rendertarget_3d");
		}

		return movie;
	}


	void OpenMovie( sfMovieViewSettings* settings )
	{
		Assert( settings );

		// need to see if this movie is already loaded, or just let sf handle it internally?
		sfScaleformMovie* movie = LoadMovie(settings->m_MovieName.c_str());
		if (!movie)
		{
			Errorf("Couldn't load movie %s", settings->m_MovieName.c_str());
			return;
		}

						
		sfScaleformMovieView* movieView = m_Scaleform->CreateMovieView(*movie);
		settings->SetMovieView(movieView);
		if (!movieView)
		{
			Errorf("Couldn't create instance of movie %s", settings->m_MovieName.c_str());
			return;
		}

		settings->UpdateSettings();
	}

	void ReloadMovieCB(int i)
	{
		sfMovieViewSettings& settings = m_Project->GetSettings(i);

		sfScaleformMovie* movie = settings.GetMovieView()->GetMovie();

		m_Scaleform->DeleteMovieView(settings.GetMovieView());
		m_Scaleform->DeleteMovie(movie);
		settings.SetMovieView( NULL );

		movie = LoadMovie(settings.m_MovieName);
		if (!movie)
		{
			Errorf("Couldn't reload movie %s", settings.m_MovieName.c_str());
			return;
		}

		sfScaleformMovieView* movieView = m_Scaleform->CreateMovieView(*movie);
		settings.SetMovieView( movieView );
		if ( !movieView )
		{
			Errorf("Couldn't create view for movie %s", settings.m_MovieName.c_str());
		}
	}

	void DeleteMovieCB(int i)
	{
		sfMovieViewSettings& settings = m_Project->GetSettings(i);

		sfScaleformMovie* movie = settings.GetMovieView()->GetMovie();

		m_Scaleform->DeleteMovieView(settings.GetMovieView());
		m_Scaleform->DeleteMovie(movie);
		settings.SetMovieView( NULL );

		m_Project->m_MovieViews.Delete(i);

		CreateProjectWidgets();
	}

	virtual void ShutdownClient()
	{
		if (m_DrawText1)
		{
			m_DrawText1->Release();
		}
		if (m_DrawText2)
		{
			m_DrawText2->Release();
		}
		if (m_DrawText3)
		{
			m_DrawText3->Release();
		}

		rmcSampleManager::ShutdownClient();
		m_Scaleform->FinishedDrawing();
		if (m_FontMovie)
		{
			m_Scaleform->DeleteMovie(m_FontMovie);
		}
		DestroyOffscreenRenderTargets();
		UnloadProject();
		m_Scaleform->Shutdown();
		delete m_Scaleform;
	}

	void NewProjectCB()
	{
#if __BANK
		if (m_Project && m_Project->GetNumMovies() > 0)
		{
			int status = bkMessageBox("Discard current project?",
				"Discard any unsaved changes to the current project and create a new one?",
				bkMsgOkCancel, bkMsgQuestion);
			if (status != 1)
			{
				return;
			}
		}

		UnloadProject();
		CreateNewProject();
#endif
	}

	void LoadProjectCB()
	{
#if __BANK
		if (m_Project && m_Project->GetNumMovies() > 0)
		{
			int status = bkMessageBox("Discard current project?",
				"Discard any unsaved changes to the current project and load a new one?",
				bkMsgOkCancel, bkMsgQuestion);
			if (status != 1)
			{
				return;
			}
		}

		char newfile[RAGE_MAX_PATH];
		safecpy(newfile, m_ProjectPath);

		if (BANKMGR.OpenFile(newfile, RAGE_MAX_PATH, "*.sfproj", false, "Scaleform Viewer Projects (*.sfproj)"))
		{
			UnloadProject();
			LoadProjectFile(newfile);			
		}
#endif
	}

	void SaveProjectCB()
	{
		if (!m_Project)
		{
			Errorf("No project to save");
			return;
		}
		if (m_ProjectPath[0] == '\0')
		{
			SaveProjectAsCB();
			return;
		}

		PARSER.SaveObject(m_ProjectPath, "sfproj", m_Project);
	}

	void SaveProjectAsCB()
	{
#if __BANK
		if (!m_Project)
		{
			Errorf("No project to save");
			return;
		}

		char newfile[RAGE_MAX_PATH];
		safecpy(newfile, m_ProjectPath);

		if (BANKMGR.OpenFile(newfile, RAGE_MAX_PATH, "*.sfproj", true, "Scaleform Viewer Projects (*.sfproj)"))
		{
			safecpy(m_ProjectPath, newfile);
			SaveProjectCB();
		}
#endif
	}

	void UnloadProject()
	{
#if __BANK
		if (m_ProjectBank)
		{
			m_ProjectBank->Truncate(0);
		}
#endif

		if (!m_Project) 
		{
			return;
		}

		for(int i = 0; i < m_Project->GetNumMovies(); i++)
		{
			// remove each movieView and since we're creating one movie per movieView 
			// remove the movie too
			sfScaleformMovieView* movieView = m_Project->m_MovieViews[i].GetMovieView();
			if (movieView)
			{
				sfScaleformMovie* movie = movieView->GetMovie();

				m_Scaleform->DeleteMovieView(movieView);
				m_Scaleform->DeleteMovie(movie);
			}
		}

#if 0 // No string tables
		if (m_Project->m_StringTable)
		{
			m_Project->m_StringTable->Kill();
		}
		delete m_Project->m_StringTable;
#endif

		delete m_Project;
		m_Project = NULL;
	}
	
	static sfSampleManager* GetInstance()	{ return sm_Instance; }
	sfPlayerProject*		GetProject()	{ return m_Project; }
	sfScaleformManager*		GetScaleform()	{ return m_Scaleform; }

	// Create two offscreen targets that the flash movies can pull from for testing
	void CreateOffscreenRenderTargets()
	{
		m_Viewport3d = rage_new grcViewport;
		m_RenderTarget3d = grcTextureFactory::GetInstance().CreateRenderTarget("rendertarget_3d", grcrtPermanent, 256, 256, 32);
		grcTextureFactory::GetInstance().RegisterTextureReference("rendertarget_3d", m_RenderTarget3d);

		m_Viewport2d = rage_new grcViewport;
		m_RenderTarget2d = grcTextureFactory::GetInstance().CreateRenderTarget("rendertarget_2d", grcrtPermanent, 256, 256, 32);
		grcTextureFactory::GetInstance().RegisterTextureReference("rendertarget_2d", m_RenderTarget2d);

		m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("offscreen_depth", grcrtDepthBuffer, 256, 256, 32);
	}

	// Draw to the two offscreen targets.
	void DrawOffscreenRenderTargets()
	{
		PF_FUNC(DrawOffscreen);

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTarget2d, 0);
		Draw2dSample();
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTarget3d, m_DepthBuffer);
		Draw3dSample();
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
	}

	void DestroyOffscreenRenderTargets()
	{
		if (m_RenderTarget2d)
		{
			m_RenderTarget2d->Release();
		}
		delete m_Viewport2d;

		if (m_RenderTarget3d)
		{
			m_RenderTarget3d->Release();
		}
		delete m_Viewport3d;

		m_DepthBuffer->Release();
	}

	// This will be sort of a map screen, so draw a simple "map".
	void Draw2dSample()
	{
		grcStateBlock::SetStates(grcStateBlock::RS_NoBackfaceCull, grcStateBlock::DSS_Default, grcStateBlock::BS_Default);

		m_Viewport2d->ResetWindow();
		m_Viewport2d->Ortho(0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);
		grcViewport* oldVP = grcViewport::SetCurrent(m_Viewport2d);

		GRCDEVICE.Clear(true, Color_white, false, 0.0f, 0x0);

		// draw a grid of 10x10 quads
		grcBeginQuads(20);
		for(float f = 0.0f; f < 1.0f; f += 0.1f)
		{
			grcDrawQuadf(0.0f, f-0.01f, 1.0f, f + 0.01f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, Color_black);
		}
		for(float f = 0.0f; f < 1.0f; f += 0.1f)
		{
			grcDrawQuadf(f-0.01f, 0.0f, f + 0.01f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, Color_grey);
		}
		grcEndQuads();

		// Now draw some "blips" on the map

		grcColor3f(1.0f, 0.0f, 0.0f);
		grcDrawCircle(0.05f, Vector3(0.5f, 0.5f, 0.0f), XAXIS, YAXIS, 20, false, true);
		grcColor3f(0.8f * (0.5f + 0.5f * sin(TIME.GetElapsedTime())), 0.0f, 0.0f);
		grcDrawCircle(0.04f, Vector3(0.5f, 0.5f, 0.0f), XAXIS, YAXIS, 20, false, true);

		grcColor4f(0.0f, 0.0f, 1.0f, 1.0f * (0.6f + 0.3f * cos(TIME.GetElapsedTime())));
		grcDrawCircle(0.04f, Vector3(0.5f, 0.8f, 0.0f), XAXIS, YAXIS, 20, false, true);

		grcColor3f(1.0f * (0.5f + 0.5f * cos(5.0f * TIME.GetElapsedTime())), 0.0f, 0.0f);
		float pos = sin(TIME.GetElapsedTime() * 0.3f) * 0.3f;
		grcDrawCircle(0.04f, Vector3(0.5f + pos, 0.2f, 0.0f), XAXIS, YAXIS, 20, false, true);

		grcViewport::SetCurrent(oldVP);
	}

	void Draw3dSample()
	{
		grcStateBlock::Default();

		grcLightState::SetEnabled(true);
		
		m_Viewport3d->ResetWindow();
		m_Viewport3d->Perspective(45.0f, 0, 0.1f, 20.0f);

		grcViewport* oldVP = grcViewport::SetCurrent(m_Viewport3d);

		GRCDEVICE.Clear(true, Color32(0x0, 0x0, 0x0, 0x0), true, grcDepthFarthest, 0x0);

		Matrix34 camMtx;
		camMtx.Identity();
		camMtx.MakeTranslate(0.0f, 0.0f, 6.0f);
		camMtx.RotateFullX(-0.2f);
		camMtx.RotateFullY(TIME.GetElapsedTime());

		m_Viewport3d->SetCameraMtx(RCC_MAT34V(camMtx));
		grcDrawSolidBox(Vector3(-1.5f, -0.3f, -0.7f), Vector3(1.5f, 0.5f, 0.7f), Color_green);

		grcDrawSolidBox(Vector3(-0.5f, 0.5f, -0.7f), Vector3(0.8f, 1.0f, 0.7f), Color_green);

		grcColor(Color_grey50);
		Matrix34 wheelMtx;
		wheelMtx.MakeRotateX(PI * 0.5f);

		wheelMtx.MakeTranslate(Vector3(1.0f, -0.2f, 0.0f));
		grcDrawCapsule(1.6f, 0.3f, wheelMtx, 20, true);

		wheelMtx.MakeTranslate(Vector3(-1.0f, -0.2f, 0.0f));
		grcDrawCapsule(1.6f, 0.3f, wheelMtx, 20, true);

		grcViewport::SetCurrent(oldVP);
	}

	void SetLoadingMovieIndex( int movieIndex )
	{
		m_LoadingMovieIdx = movieIndex;	
	}

	const int GetLoadingMovieIndex() const
	{
		return m_LoadingMovieIdx;	
	}

	const bool End() const
	{
		return m_End;
	}

protected:
	sfScaleformManager*				m_Scaleform;
	sfPlayerProject*				m_Project;
	char							m_ProjectPath[RAGE_MAX_PATH];
#if __BANK
	bkBank*							m_ProjectBank;
#endif
	int								m_LastScreenWidth, m_LastScreenHeight;


	grcRenderTarget*				m_RenderTarget3d;
	grcViewport*					m_Viewport3d;
	grcRenderTarget*				m_RenderTarget2d;
	grcViewport*					m_Viewport2d;
	grcRenderTarget*				m_DepthBuffer;

	bool							m_DebugDrawRenderTargets;
	bool							m_End;

	sfScaleformMovie*				m_FontMovie;
	GFxDrawText*					m_DrawText1;
	GFxDrawText*					m_DrawText2;
	GFxDrawText*					m_DrawText3;
	bool							m_TestDrawText;

	static sfSampleManager*			sm_Instance;
	int								m_LoadingMovieIdx;

	bool							m_DrawFlashIn3d;
	Vector3							m_ObjectTranslate;
	Vector3							m_ObjectRotateEulers;
	Vector3							m_ObjectScale;
	Matrix34						m_ObjectMatrix; 
};

SCR_DEFINE_NEW_POINTER_PARAM_AND_RET(sfScaleformMovieView);

sfScaleformMovieView* GetMovie(const char* movieName)
{
	sfPlayerProject* proj = sfSampleManager::GetInstance()->GetProject();
	if (!movieName || !proj)
	{
		return NULL;
	}
	for(int i = 0; i < proj->GetNumMovies(); i++)
	{
		if (!strcmp(movieName, proj->GetSettings(i).m_ScriptName))
		{
			return proj->GetSettings(i).GetMovieView();
		}
	}
	return NULL;
}

void ShowNextMovie()
{
	sfPlayerProject* proj = sfSampleManager::GetInstance()->GetProject();
	if (!proj->m_IsSlideshow)
	{
		Errorf("You should only call ShowNextMovie with slideshow projects");
		return;
	}

	// turn off all the movies, then enable just the navigators and the new one we want
	int lastVisIndex = 0;
	for(int i = 0; i < proj->GetNumMovies(); i++)
	{
		if (!proj->GetSettings(i).m_AlwaysOnInSlideshow)
		{
			if (proj->GetSettings(i).m_Visible)
			{
				lastVisIndex = i;
			}
			proj->GetSettings(i).m_Visible = false;
			proj->GetSettings(i).m_Active = false;
		}
	}

	for(int i = 0; i < proj->GetNumMovies(); i++)
	{
		int realIndex = (i + lastVisIndex + 1) % proj->GetNumMovies();
		if (!proj->GetSettings(realIndex).m_AlwaysOnInSlideshow)
		{
			proj->GetSettings(realIndex).m_Visible = true;
			proj->GetSettings(realIndex).m_Active = true;
			proj->GetSettings(realIndex).RequestMovie();
			break;
		}
	}

	sfSampleManager::GetInstance()->UpdateAllMovieSettings();
}

void ShowPrevMovie()
{
	sfPlayerProject* proj = sfSampleManager::GetInstance()->GetProject();
	if (!proj->m_IsSlideshow)
	{
		Errorf("You should only call ShowPrevMovie with slideshow projects");
		return;
	}

	// turn off all the movies, then enable just the navigators and the new one we want
	int lastVisIndex = 0;
	for(int i = proj->GetNumMovies()-1; i >= 0; i--)
	{
		if (!proj->GetSettings(i).m_AlwaysOnInSlideshow)
		{
			if (proj->GetSettings(i).m_Visible)
			{
				lastVisIndex = i;
			}
			proj->GetSettings(i).m_Visible = false;
			proj->GetSettings(i).m_Active = false;
		}
	}

	for(int i = proj->GetNumMovies()-1; i >= 0; i--)
	{
		int realIndex = (i + lastVisIndex) % proj->GetNumMovies();
		if (!proj->GetSettings(realIndex).m_AlwaysOnInSlideshow)
		{
			proj->GetSettings(realIndex).m_Visible = true;
			proj->GetSettings(realIndex).m_Active = true;
			proj->GetSettings(realIndex).RequestMovie();
			break;
		}
	}

	sfSampleManager::GetInstance()->UpdateAllMovieSettings();
}

const char* GetCurrMovieName()
{
	sfPlayerProject* proj = sfSampleManager::GetInstance()->GetProject();
	if (!proj->m_IsSlideshow)
	{
		Errorf("You should only call GetCurrMovieName with slideshow projects");
		return "";
	}

	for(int i = 0; i < proj->GetNumMovies(); i++)
	{
		if (!proj->GetSettings(i).m_AlwaysOnInSlideshow && 
			proj->GetSettings(i).m_Visible)
		{
			return proj->GetSettings(i).m_MovieName;
		}
	}
	return "";
}

const char* GetCurrMovieDesc()
{
	sfPlayerProject* proj = sfSampleManager::GetInstance()->GetProject();
	if (!proj->m_IsSlideshow)
	{
		Errorf("You should only call GetCurrMovieDesc with slideshow projects");
		return "";
	}

	for(int i = 0; i < proj->GetNumMovies(); i++)
	{
		if (!proj->GetSettings(i).m_AlwaysOnInSlideshow && 
			proj->GetSettings(i).m_Visible)
		{
			return proj->GetSettings(i).m_Description;
		}
	}
	return "";
}

void RegisterSampleBindings()
{
	SCR_REGISTER_SECURE_EXPORT(GET_MOVIE, 0x8cfaf96b, GetMovie);
	SCR_REGISTER_SECURE_EXPORT(SHOW_NEXT_MOVIE, 0xd6758fbd, ShowNextMovie);
	SCR_REGISTER_SECURE_EXPORT(SHOW_PREV_MOVIE, 0x66f6f784, ShowPrevMovie);
	SCR_REGISTER_SECURE_EXPORT(GET_CURR_MOVIE_NAME, 0x1e432e19, GetCurrMovieName);
	SCR_REGISTER_SECURE_EXPORT(GET_CURR_MOVIE_DESC, 0x46be085f, GetCurrMovieDesc);
}

sfSampleManager* sfSampleManager::sm_Instance = NULL;

int Main()
{
	sfSampleManager* sample = rage_new sfSampleManager;
	sample->Init();
	sample->UpdateLoop();
	sample->Shutdown();

	delete sample;

	return 0;
}


void s_UpdateMovie( void* data )
{
	sfMovieViewSettings* movieSettings = (sfMovieViewSettings*) data;
	sfSampleManager* sManager = sfSampleManager::GetInstance();
	Assert( sManager );

	sfPlayerProject* project =  sManager->GetProject();
	Assert( project );

	movieSettings->UpdateSettings();

	if( project->m_AsyncUpdate && project->m_OneMoviePerThread )
	{
		if( !movieSettings->m_Active )
		{
			sysInterlockedDecrement((volatile unsigned*)&g_TotalUpdated);
		}
	}	

}

void s_RequestMovie( const char* movieName )
{
	Assert( movieName );

	sfSampleManager* sfSManager = sfSampleManager::GetInstance();
	Assert( sfSManager );

	sfSManager->RequestMovieAsync( movieName );
}


static void s_LoadMovieAsync(void* data)
{
	sfMovieViewSettings* settings = static_cast<sfMovieViewSettings*>(data);	
	Assert( settings );
	sfSampleManager* sManager = sfSampleManager::GetInstance();
	Assert( sManager );
	sfPlayerProject* project =  sManager->GetProject();
	Assert( project );

	sManager->OpenMovie( settings );
	if( project->m_AsyncUpdate && project->m_OneMoviePerThread )
	{
		if( settings->m_UpdateThreadId == sysIpcThreadIdInvalid )
		{
			settings->m_UpdateThreadId = sysIpcCreateThread( s_UpdateMovieAsync, settings, UPDATE_MOVIE_STACK_SIZE, PRIO_NORMAL,"[RAGE] OpenMovie Private");
		}		
	}
	sManager->SetLoadingMovieIndex(-1);
}


static void s_UpdateAllMoviesAsync(void* data)
{
	sfSampleManager* sManager = static_cast<sfSampleManager*> (data);
	Assert( sManager );

	sfScaleformManager* scaleform = sManager->GetScaleform();
	Assert( scaleform );

	sfPlayerProject* project =  sManager->GetProject();
	Assert( project );

	while(1)
	{		
		if( g_ReadyToUpdate && !project->m_OneMoviePerThread  )
		{
			scaleform->UpdateManagerOnly();

			const int loadingMovieIndex = sManager->GetLoadingMovieIndex();
			for(int i = 0; i < project->GetNumMovies(); i++)
			{
				if ( loadingMovieIndex == i || !project->GetMovieView(i) || !project->GetMovieView(i)->GetIsActive())
				{
					continue;
				}			

				sfMovieViewSettings& settings = project->GetSettings(i);

				g_UpdatingMovieSettings = &settings;

				sManager->UpdateMovieTime( &settings, scaleform );
			}

			g_ReadyToUpdate = 0;
		}
		g_UpdatingMovieSettings = 0;

		if( sManager->End() )
			break;
	}
}


static void s_UpdateMovieAsync(void* data)
{
	sfMovieViewSettings* settings = static_cast<sfMovieViewSettings*>(data);
	Assert( settings );

	sfSampleManager* sManager = sfSampleManager::GetInstance();
	Assert( sManager );

	sfScaleformManager* scaleform = sManager->GetScaleform();
	Assert( scaleform );
	
	// the assumptions here are:
	// - movie is loaded
	// - movie view exists

	while(1)
	{			
		scaleform->UpdateManagerOnly();
//		if( sysIpcPollSema( settings->m_ReadyToUpdate ) )
		{
			sManager->UpdateMovieTime( settings, scaleform );
			sysInterlockedIncrement((volatile unsigned*)&g_TotalUpdated);

			sysIpcWaitSema(settings->m_ReadyToUpdate);
		}

		if( settings->End() )
			break;
	}
}

