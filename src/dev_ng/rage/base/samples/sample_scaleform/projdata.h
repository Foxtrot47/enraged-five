// 
// sample_scaleform/projdata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SCALEFORM_PROJDATA_H
#define SAMPLE_SCALEFORM_PROJDATA_H

#include "atl/array.h"
#include "data/base.h"
#include "file/limits.h"
#include "parser/macros.h"
#include "scaleform/scaleformheaders.h"
#include "script/value.h"
#include "string/string.h"
#include "system/ipc.h"
#include "vector/vector2.h"

namespace rage
{
	class sfScaleformMovieView;
	class txtStringTable;
}

class sfMovieViewSettings : public rage::datBase
{

private:

	rage::sfScaleformMovieView*	m_MovieView;

public:
	sfMovieViewSettings();

	enum PlaybackRate
	{
		WALL_TIME,			// Use wall time for playback
		FRAME_PER_FRAME,	// Play one movie frame for each real frame
		FIXED_THIRTY_HZ,	// Update the flash movie at 30hz regardless of real update time
		ONE_HZ,				// Play one movie frame per second
	};

	rage::ConstString			m_MovieName;
	char						m_ScriptName[32];
	char						m_Description[128];
//	rage::sfScaleformMovieView*	m_MovieView;
	rage::Vector2				m_Pos, m_Size;
	float						m_BackAlpha;
	PlaybackRate				m_PlaybackRate;
	GFxMovieView::ScaleModeType m_ScaleMode;
	GFxMovieView::AlignType		m_AlignType;
	bool						m_Visible;
	bool						m_Active;
	bool						m_Mouse, m_Keyboard, m_Gamepad;
	bool						m_AlwaysOnInSlideshow;
//	volatile bool				m_ReadyToUpdate;
	
	int							m_DrawOrder;

	rage::sysIpcThreadId		m_UpdateThreadId;
	rage::sysIpcSema 			m_ReadyToUpdate;
	bool						m_End;

	void UpdateSettings();
	void RequestMovie();
	void KillUpdateThread();

	rage::sfScaleformMovieView* GetMovieView() 
	{ 
		if( !m_MovieView )
		{
//			m_Active = false;
//			m_Visible = false;
		}
		return m_MovieView; 
	}

	void SetMovieView( rage::sfScaleformMovieView* movieView ) 
	{ 
		m_MovieView = movieView ; 
	}

	const bool End() const
	{ 
		return m_End;
	}

	PAR_PARSABLE;
};

class sfPlayerProject
{
public:
	sfPlayerProject();

	int GetNumMovies() const		{return m_MovieViews.GetCount();}
	sfMovieViewSettings& GetSettings(int i) { return m_MovieViews[i]; }
	rage::sfScaleformMovieView* GetMovieView(int i) { return m_MovieViews[i].GetMovieView(); }

	rage::atArray<sfMovieViewSettings>	m_MovieViews;
	bool								m_IsSlideshow;
	int									m_SlideshowIndex;
	bool								m_TranslateEverything;
	bool								m_AsyncUpdate;
	bool								m_OneMoviePerThread;

	PAR_SIMPLE_PARSABLE;
};

#endif