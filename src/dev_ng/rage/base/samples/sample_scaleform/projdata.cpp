// 
// sample_scaleform/projdata.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "projdata.h"

#include "grcore/device.h"
#include "scaleform/scaleform.h"

using namespace rage;

#include "projdata_parser.h"


extern void s_RequestMovie( const char* movieName );
extern void s_UpdateMovie( void* data );

extern volatile unsigned g_TotalActiveMovies;


sfMovieViewSettings::sfMovieViewSettings()
: m_MovieView(NULL)
, m_PlaybackRate(WALL_TIME)
, m_ScaleMode(GFxMovieView::SM_ShowAll)
, m_AlignType(GFxMovieView::Align_Center)
, m_Visible(true)
, m_Active(true)
, m_Mouse(false)
, m_Keyboard(false)
, m_Gamepad(false)
, m_AlwaysOnInSlideshow(false)
//, m_ReadyToUpdate(false)
, m_DrawOrder(0)
, m_End(false)
{
	m_Pos.Set(0.0f, 0.0f);
	m_Size.Set(1.0f, 1.0f);
	m_ScriptName[0] = '\0';
	m_Description[0] = '\0';
	m_UpdateThreadId = sysIpcThreadIdInvalid;

	m_ReadyToUpdate = sysIpcCreateSema(0);
}


void sfMovieViewSettings::KillUpdateThread()
{
	m_End = true;
	sysIpcSignalSema( m_ReadyToUpdate );
	if( m_UpdateThreadId != sysIpcThreadIdInvalid )
	{
		sysIpcWaitThreadExit(m_UpdateThreadId);
		m_UpdateThreadId = sysIpcThreadIdInvalid;
	}
	m_End = false;
}



void sfMovieViewSettings::RequestMovie()
{
	if( !m_MovieView )
	{
		s_RequestMovie( m_MovieName.c_str() );
	}	
	else
	{
		s_UpdateMovie( this );
	}
}


void sfMovieViewSettings::UpdateSettings()
{
	if ( !GetMovieView() )
	{
		return;
	}

	// Update the viewport

	GFxMovieView& movie = m_MovieView->GetMovieView();

	movie.SetViewScaleMode(m_ScaleMode);
	movie.SetViewAlignment(m_AlignType);

	GViewport vp;
	movie.GetViewport(&vp);

	// This assumes we're always rendering into the screen - not an offscreen buffer.
	vp.BufferWidth = GRCDEVICE.GetWidth();
	vp.BufferHeight = GRCDEVICE.GetHeight();

	vp.Left = (int)(m_Pos.x * vp.BufferWidth);
	vp.Top = (int)(m_Pos.y * vp.BufferHeight);
	vp.Width = (int)(m_Size.x * vp.BufferWidth);
	vp.Height = (int)(m_Size.y * vp.BufferHeight);

//	Printf( " %s \n        Pos X = %5.2f, Pos X = %5.2f, Size X = %5.2f, Size X = %5.2f  \n", m_MovieName, m_Pos.x, m_Pos.y, m_Size.x, m_Size.y );

	movie.SetViewport(vp);

	movie.SetBackgroundAlpha(m_BackAlpha);

	m_MovieView->SetIsActive(m_Active);
	m_MovieView->SetIsVisible(m_Visible);

	m_MovieView->SetFlag(sfScaleformMovieView::FLAG_MOUSE_INPUT, m_Mouse);
	m_MovieView->SetFlag(sfScaleformMovieView::FLAG_KEYBOARD_INPUT, m_Keyboard);
	m_MovieView->SetFlag(sfScaleformMovieView::FLAG_PAD_INPUT, m_Gamepad);
}

sfPlayerProject::sfPlayerProject()
: m_IsSlideshow(false)
, m_SlideshowIndex(0)
, m_AsyncUpdate(false)
, m_OneMoviePerThread(false)
{
}
