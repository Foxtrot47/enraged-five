// 
// sample_scaleform/sample_sfbasic.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "math/random.h"
#include "sample_rmcore/sample_rmcore.h"
#include "scaleform/commandbuffer.h"
#include "scaleform/scaleform.h"
#include "scr_scaleform/scaleform.h"
#include "script/wrapper.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(file, "name of the flash movie to load (.swf or .gfx)");
PARAM(flasharg, "A string that the flash movie can get (via an externalinterface command");

void RegisterSampleBindings();


void s_RequestMovie( const char* ASSERT_ONLY(movieName) )
{
	Assert( movieName );
	// dummy function
}

void s_UpdateMovie( void* ASSERT_ONLY(movieSettings) )
{
	Assert( movieSettings );
	// dummy function
}

//////////////////////////////////////////////////////////////////////////
// TITLE: sample_sfbasic
// PURPOSE:
//		This sample shows the basic steps in initting the scaleform system,
//		loading a movie, and updating and drawing that movie

class sfSampleManager : public ragesamples::rmcSampleManager
{
public:

	sfSampleManager()
		: m_Scaleform(NULL)
		, m_Movie(NULL)
		, m_MovieView(NULL)
	{
		sm_Instance = this;
	}

	~sfSampleManager()
	{
	}

	// STEP #1. Initialize the scaleform system.
	// -- Here we're initting it as part of the rage sample's InitClient function.
	virtual void InitClient()
	{
		rmcSampleManager::InitClient();

		// This is sample code, don't spam an assert
		ASSERT_ONLY(sysMemStreamingCount++);

		// -- First thing we do is init the rage scripting system. We have a few
		// script commands that can manipulate the scaleform movies, and also the 
		// scaleform movies are able to call into any exported script function.
		// This step is optional if you don't want script integration.
		scrThread::InitClass();
		scrThread::RegisterBuiltinCommands();

		// -- Create a new scaleform manager and initialize it
		m_Scaleform = rage_new sfScaleformManager;
		m_Scaleform->Init();


		// Turn off edgeAA
		u32 renderFlags = m_Scaleform->GetLoader()->GetRenderConfig()->GetRenderFlags();
		renderFlags &= ~GFxRenderConfig::RF_EdgeAA;
		m_Scaleform->GetLoader()->GetRenderConfig()->SetRenderFlags(renderFlags);

		// -- Calling InitExternalInterface lets the scaleform manager know which
		// callbacks to use when there is an actionscript ExternalInterface() function
		// call. In this case we pass in a new instance of our CallRageScriptFromFlash
		// object. This means for any ExternalInterface call we look to see if that
		// call corresponds to an exported script command and call that script command.
		m_Scaleform->InitExternalInterface(*(rage_new sfCallRageScriptFromFlash));

		// -- Now we register some utility functions - these aren't necessary but
		// provide some commands for getting and setting flash variables and invoking
		// actionscript functions from ragescript or the rag console.
		sfRageScriptBindings::Register(m_Scaleform);

		// -- This registers a few more functions we've defined here - like a way to 
		// get the current movie, and some functions that actionscript can call into.
		RegisterSampleBindings();

		// -- Now load a movie.
		const char* filename = NULL;
		if (PARAM_file.Get(filename))
		{
			LoadScaleformMovie(filename);
		}
	}

	// STEP #2. Load a scaleform movie
	// -- Movies come in two parts - an sfScaleformMovie which wraps a GFxMovieDef 
	// provides the movie definition. And a sfScaleformMovieView which wraps a GFxMovieView
	// provides all the instance data for an instance of that movie. 
	// In this case since we're not creating multiple instances we'll need one of each. 
	void LoadScaleformMovie(const char* filename)
	{
		if (!filename)
		{
			return;
		}

		m_Filename = filename;

		UnloadScaleformMovie();

		// -- The first step then is to load the movie from a file. This returns
		// movie object if successful. It can load SWF files, or GFX files with an 
		// associated texture dictionary.
		m_Movie = m_Scaleform->LoadMovie(filename);

		if (!m_Movie)
		{
			Errorf("Couldn't create movie");
			return;
		}

		// -- Now that the movie is create we create a movieview. The movie view
		// by default will be active and visible, and is sized to fill the whole screen.
		// It will not accept any input though.
		m_MovieView = m_Scaleform->CreateMovieView(*m_Movie);
		if (!m_MovieView)
		{
			Errorf("Couldn't create movie instance");
			return;
		}

		// -- But in this case we want it to accept input - so we set some flags
		// telling it to recieve input from the gamepads, mouse, and keyboard.
		m_MovieView->SetFlag(sfScaleformMovieView::FLAG_PAD_INPUT, true);
		m_MovieView->SetFlag(sfScaleformMovieView::FLAG_MOUSE_INPUT, true);
		m_MovieView->SetFlag(sfScaleformMovieView::FLAG_KEYBOARD_INPUT, true);


		GFxMovieView& mv = m_MovieView->GetMovieView();
		mv.GetVariable(&m_Root, "_root");

		// DisplayMapTile(36, 21);

		CaptureData();
	}

	void DisplayMapTile(int row, int col)
	{
		if (m_MapTile.IsDefined() && m_MapTile.IsDisplayObject())
		{
			m_MapTile.Invoke("removeMovieClip");
		}

		char mapTileName[256];
		formatf(mapTileName, "MAP_high_r%d_col%d", row, col);

		m_Root.AttachMovie(&m_MapTile, mapTileName, "maptile", 10);

		m_MapTile.SetMember("rendererString", "MapTile");
	}

	void CaptureData()
	{
#if SF_RECORD_COMMAND_BUFFER
		m_Scaleform->GetRageRenderer().RecordCommandBuffer("MapTile");
#endif
	}

	// STEP #3. Updating a movie.
	// -- The simple way to update your movies it to just call UpdateAll(). The
	// scaleform manager maintains a list of movies and will update all of the active ones.
	// You can pass in a custom timestep if you need. Here we get the movie's frame rate so
	// we make sure the movie updates one frame per graphics frame, regardless of its
	// original rate.
	virtual void UpdateClient()
	{
		rmcSampleManager::UpdateClient();

		float movieRate = 30.0f;

		// -- GetMovieView gets the underlying GFxMovieView object
		if (m_MovieView)
		{
			movieRate = m_MovieView->GetMovieView().GetFrameRate();
		}

		m_Scaleform->UpdateAll(1.0f / movieRate);
		m_Scaleform->PerformSafeModeOperations();
	}

	// STEP #4. Drawing a movie.
	// Like update, draw can be handled completely by the manager or you can draw
	// each movie on its own.
	virtual void DrawClient()
	{
		rmcSampleManager::DrawClient();

		// -- Before doing any drawing, you need to call the manager's BeginFrame function
		// once per frame. You'll get asserts if you don't.
		m_Scaleform->BeginFrame();

		// -- Set up a default render state
		grcViewport::SetCurrent(grcViewport::GetDefaultScreen());

		// -- And tell the manager to draw all its visible movies.
		m_Scaleform->DrawAll();

#if SF_RECORD_COMMAND_BUFFER
		sfCommandBuffer* cb = m_Scaleform->GetRageRenderer().GetRecordedCommandBuffer();

		static bool doOnce = false;
		if (cb && !doOnce)
		{
			m_MapTile.Invoke("removeMovieClip");
			m_MapTile = GFxValue(); // undefined

			cb->DumpAscii("X:\\debug\\cb.txt");


			cb->CreateRageBuffers();

			m_Root.AttachMovie(&m_Proxy, "Proxy", "proxy1");
//			GFxValue::DisplayInfo disp;
//			m_Proxy.GetDisplayInfo(&disp);
//			disp.SetRotation(45.0f);
//			disp.SetScale(50.f, 50.f);
//			m_Proxy.SetDisplayInfo(disp);

			m_Proxy.SetMember("rendererString", "ReplayCB");

			doOnce = true;
		}
#endif

		if (m_Proxy.IsDefined())
		{
			static float frameCount = 0.0f;
			frameCount += 1.0f;
			GFxValue::DisplayInfo disp;
			m_Proxy.GetDisplayInfo(&disp);
			disp.SetRotation(frameCount);
			float scale = sin((float)frameCount / 100.0f) * 100.0f + 50.0f;
			disp.SetScale(scale, scale);
			m_Proxy.SetDisplayInfo(disp);
		}

		m_Scaleform->EndFrame();
	}

	// STEP #5. Unloading a movie
	// Unloading needs to be done through the manager, so it can keep track of which movies
	// and views are still active. Here we delete both.
	void UnloadScaleformMovie()
	{
		if (m_MovieView)
		{
			m_Scaleform->DeleteMovieView(m_MovieView);
			m_MovieView = NULL;
		}

		if (m_Movie)
		{
			m_Scaleform->DeleteMovie(m_Movie);
			m_Movie = NULL;
		}
	}

	// STEP #6. Shutting down.
	// If you haven't deleted all your movies and movie views already, Shutdown() will
	// do it for you, but will give you warnings in case you have leaks.
	virtual void ShutdownClient()
	{
		rmcSampleManager::ShutdownClient();
		UnloadScaleformMovie();
		m_Scaleform->Shutdown();
		delete m_Scaleform;
	}

	sfScaleformMovieView* GetMovieView() {return m_MovieView;}
	static sfSampleManager* GetInstance() {return sm_Instance;}

protected:
	sfScaleformManager*				m_Scaleform;
	sfScaleformMovieView*			m_MovieView;
	sfScaleformMovie*				m_Movie;
	GFxValue						m_Root;
	GFxValue						m_MapTile;
	GFxValue						m_Proxy;
	ConstString						m_Filename;

	static sfSampleManager*			sm_Instance;
};

// STEP #7. Script bindings.
// We have two types of script-bound functions here. One is meant to 
// be called from the rag console, and is used for interactive testing of the flash
// movie, and the others are meant to be called from actionscript.

// -- This is a script function that's meant for use by the rag console (or from
// a sample script running in the viewer). Once you have a handle to the current
// movie you can use the SF_* functions from scr_scaleform/scaleform.cpp to 
// get and set variables, jump to frames, or invoke actionscript functions.
SCR_DEFINE_NEW_POINTER_PARAM_AND_RET(sfScaleformMovieView);
sfScaleformMovieView* GetMovie()
{
	if (sfSampleManager::GetInstance())
	{
		return sfSampleManager::GetInstance()->GetMovieView();
	}
	return NULL;
}

// -- These two are simple functions that actionscript can call. One gets
// a gaussian distributed random variable, the other gets the values (if any)
// of the -flasharg commandline argment.
float GetGaussianRandom(float mean, float variance)
{
	return g_ReplayRand.GetGaussian(mean, variance);
}

const char* GetFlashArg()
{
	const char* value = NULL;
	PARAM_flasharg.Get(value);
	return value;
}

// -- Finally we register the bound functions. They all get registered for export
// because they will either be called by the rag console or flash.
void RegisterSampleBindings()
{
	SCR_REGISTER_SECURE_EXPORT(GET_MOVIE, 0x8cfaf96b, GetMovie);
	SCR_REGISTER_SECURE_EXPORT(GET_GAUSSIAN_RANDOM, 0xa02e741e, GetGaussianRandom);
	SCR_REGISTER_SECURE_EXPORT(GET_FLASH_ARG, 0xf66fc731, GetFlashArg);
}

sfSampleManager* sfSampleManager::sm_Instance = NULL;

int Main()
{
	sfSampleManager* sample = rage_new sfSampleManager;
	sample->Init();
	sample->UpdateLoop();
	sample->Shutdown();

	delete sample;

	return 0;
}
