<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<enumdef type="PlaybackRate">
	<enumval name="WallTime"/>
	<enumval name="FramePerFrame"/>
	<enumval name="FixedThirtyHz"/>
	<enumval name="OneHz"/>
</enumdef>

<enumdef type="ScaleModeType">
	<enumval name="NoScale"/>
	<enumval name="ShowAll"/>
	<enumval name="ExactFit"/>
	<enumval name="NoBorder"/>
</enumdef>

<enumdef type="AlignType">
	<enumval name="Center"/>
	<enumval name="TopCenter"/>
	<enumval name="BottomCenter"/>
	<enumval name="CenterLeft"/>
	<enumval name="CenterRight"/>
	<enumval name="TopLeft"/>
	<enumval name="TopRight"/>
	<enumval name="BottomLeft"/>
	<enumval name="BottomRight"/>
</enumdef>

<structdef type="sfMovieViewSettings">
  <string name="m_MovieName" type="ConstString"/>
  <string name="m_ScriptName" size="32" type="member"/>
  <string name="m_Description" size="128" type="member"/>
  <Vector2 max="10.0" min="-10.0" name="m_Pos" onWidgetChanged="UpdateSettings"/>
  <Vector2 max="10.0" min="-10.0" name="m_Size" onWidgetChanged="UpdateSettings"/>
  <float max="1.0" min="0.0" name="m_BackAlpha" onWidgetChanged="UpdateSettings"/>
  <enum name="m_PlaybackRate" onWidgetChanged="UpdateSettings" type="PlaybackRate"/>
  <enum name="m_ScaleMode" onWidgetChanged="UpdateSettings" type="ScaleModeType"/>
  <enum name="m_AlignType" onWidgetChanged="UpdateSettings" type="AlignType"/>
  <bool name="m_Visible" onWidgetChanged="UpdateSettings"/>
  <bool name="m_Active" onWidgetChanged="RequestMovie"/>
  <bool name="m_Mouse" onWidgetChanged="UpdateSettings"/>
  <bool name="m_Keyboard" onWidgetChanged="UpdateSettings"/>
  <bool name="m_Gamepad" onWidgetChanged="UpdateSettings"/>
  <bool name="m_AlwaysOnInSlideshow"/>
  <int name="m_DrawOrder"/>
</structdef>

<const name="RAGE_MAX_PATH" value="256"/>

<structdef name="ScaleformPlayerProject" type="sfPlayerProject">
	<array hideWidgets="true" name="m_MovieViews" type="atArray">
		<struct type="sfMovieViewSettings"/>
	</array>
	<bool name="m_IsSlideshow"/>
	<bool name="m_TranslateEverything"/>
	<bool name="m_AsyncUpdate"/>
	<bool name="m_OneMoviePerThread"/>	
</structdef>

</ParserSchema>