#include "atl/dynapool.h"

#include "atl/slist.h"
#include "system/main.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// The tests need some example structure to work with.
// Here's a goofy half-baked little tree

class TreeNode
{
public:
	TreeNode()
		: m_Data(0)
		, m_Parent(NULL)
		, m_Eldest(NULL)
		, m_Younger(NULL)
	{	}

	void RemoveFromParent()
	{
		if (!m_Parent)
			return;
		
		TreeNode* elder		= NULL;
		TreeNode* younger	= m_Parent->m_Eldest;
		while (younger != this)
		{			
			elder	= younger;
			younger	= younger->m_Younger;
		}
		Assert(younger==this);
		if (elder)
		{
			elder->m_Younger = m_Younger;
		}
		else
		{
			m_Parent->m_Eldest = m_Younger;
		}
	}

	// Yeah, this and RemoveFromParent aren't even symmetric.  Whee.
	void AddChild(TreeNode& child)
	{	
		child.RemoveFromParent();
		child.m_Parent	= this;
		child.m_Younger	= m_Eldest;
		m_Eldest		= &child;
	}	

	char		m_Data;
	TreeNode*	m_Parent;
	TreeNode*	m_Eldest;
	TreeNode*	m_Younger;	
};

//////////////////////////////////////////////////////////////////////////

// This should usually be set to 1; set it to 0 to make sure 
// that nothing breaks when we're not using the stats functionality.
// Really we ought to test both at once.
#define SAMPLE_DYNAPOOL_STATS 1

// Some pool clients might just use a simple list to keep track  
typedef	atSNode<TreeNode>					WrappedTreeNode;
typedef	atSList<TreeNode>					WrappedTreeNodeList;

#if SAMPLE_DYNAPOOL_STATS
typedef	atDynaPoolWithStats<TreeNode>			TreeNodePool;
typedef	atDynaPoolWithStats<WrappedTreeNode>	WrappedTreeNodePool;
#else
typedef	atDynaPool<TreeNode>					TreeNodePool;
typedef	atDynaPool<WrappedTreeNode>				WrappedTreeNodePool;
#endif

//////////////////////////////////////////////////////////////////////////

int Main() 
{
	const int nodePoolSize			= 417;
	const int nodePoolAdditional	= 129;

	//////////////////////////////////////////////////////////////////////////	
	// Test: Append as many nodes as originally allocated, then delete 'em.
	
	WrappedTreeNodePool* nodePool		= rage_new WrappedTreeNodePool(nodePoolSize);
	WrappedTreeNodeList nodeList;

	for (int i=0; i<nodePoolSize; i++)
	{
		nodeList.Append(nodePool->CreateObject());
	}
	for (int i=0; i<nodePoolSize; i++)
	{
		nodePool->DestroyObject(*(nodeList.PopHead()));
	}

	Assert(nodeList.GetNumItems() == 0);
#if SAMPLE_DYNAPOOL_STATS
	Assert(nodePool->StatsGetNumUsed() == 0);
	Assert(nodePool->StatsGetNumAllocatedDynamically() == 0);
#endif

	delete nodePool;

	//////////////////////////////////////////////////////////////////////////
	// Test: Now try actually using the contents of the nodes.
	// This first test just adds a bunch of nodes as chidlren of 
	// a single root node

	TreeNodePool* examplePool	= rage_new TreeNodePool(nodePoolSize);
	TreeNode& root				= examplePool->CreateObject();
	
	for (int i=1; i<nodePoolSize; i++)
	{
		root.AddChild(examplePool->CreateObject());
	}
	
	for (TreeNode* removeMe=root.m_Eldest; removeMe; removeMe=root.m_Eldest)
	{
		removeMe->RemoveFromParent();
		examplePool->DestroyObject(*removeMe);
	}
	
#if SAMPLE_DYNAPOOL_STATS
	Assert(examplePool->StatsGetNumUsed() == 1);
	Assert(examplePool->StatsGetNumAllocatedDynamically() == 0);
#endif

	//////////////////////////////////////////////////////////////////////////
	// Test: Interleave creation and deletion
	
	for (int i=1; i<nodePoolSize-1; i++)
	{
		root.AddChild(examplePool->CreateObject());
		root.AddChild(examplePool->CreateObject());
		TreeNode* removeMe=root.m_Eldest;
		removeMe->RemoveFromParent();
		examplePool->DestroyObject(*removeMe);		
	}
	
	for (TreeNode* removeMe=root.m_Eldest; removeMe; removeMe=root.m_Eldest)
	{
		removeMe->RemoveFromParent();
		examplePool->DestroyObject(*removeMe);
	}
	
	examplePool->DestroyObject(root);

#if SAMPLE_DYNAPOOL_STATS
	Assert(examplePool->StatsGetNumUsed() == 0);
	Assert(examplePool->StatsGetNumAllocatedDynamically() == 0);
#endif
	
	delete examplePool;
	
	//////////////////////////////////////////////////////////////////////////
	// Test: Append more nodes than originally allocated.

	nodePool = rage_new WrappedTreeNodePool(nodePoolSize);
	
	for (int i=0; i<nodePoolSize; i++)
	{
		Assert(nodeList.GetNumItems() == i);
		nodeList.Append(nodePool->CreateObject());
	}

	Assert(nodeList.GetNumItems() == nodePoolSize);
	
	Printf("*** Dynamic alloc warning should appear once, below. ***\n");
	nodeList.Append(nodePool->CreateObject());
	Printf("*** Dynamic alloc warning should appear once, above. ***\n");
	
	for (int i=1; i<nodePoolAdditional; i++)
	{
#if SAMPLE_DYNAPOOL_STATS
	Assert(nodePool->StatsGetNumAllocatedDynamically() == i);
#endif
		nodeList.Append(nodePool->CreateObject());
	}
	
	// Test: Delete all nodes.  
	for (int i=0; i<nodePoolSize+nodePoolAdditional; i++)
	{
		nodePool->DestroyObject(*(nodeList.PopHead()));
		// Note: arguably the next line isn't needed.  The current implementation
		// doesn't delete unused dynamically-allocated nodes, but that's not
		// necessarily a property we need to verify.
#if	SAMPLE_DYNAPOOL_STATS
		Assert(nodePool->StatsGetNumAllocatedDynamically() == nodePoolAdditional);
#endif
	}

	Assert(nodeList.GetNumItems() == 0);
#if SAMPLE_DYNAPOOL_STATS
	Assert(nodePool->StatsGetNumAllocatedDynamically() == nodePoolAdditional);
#endif

	//////////////////////////////////////////////////////////////////////////
	
	// Test: Leave some objects in the pool on shutdown.
	// This will intentionally leak 23 allocations of sizeof(WrappedTreeNode) bytes
	for (int i=0; i<23; i++)
	{
		nodeList.Append(nodePool->CreateObject());
	}
			
	delete nodePool;

	//////////////////////////////////////////////////////////////////////////

	return 0;
}

