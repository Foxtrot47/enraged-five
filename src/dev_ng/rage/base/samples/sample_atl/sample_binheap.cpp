#include "atl/binheap.h"
#include "atl/fibheap.h"

#include "diag/output.h"
#include "math/random.h"
#include "system/main.h"
#include "system/timer.h"


namespace rage {

typedef	atBinHeap<float, int>		atHeap;
typedef atBinHeap<float, int>::Node	atHeapNode;

typedef	atFibHeap<float, int>		atFib;
typedef atFibHeap<float, int>::Node	atFibNode;

}

using namespace rage;


int Main() 
{
	const int batchSize					=10000;
	const bool randomInsert				=true;
	const bool randomDecrease			=true;
	const bool negate					=false;
	
	// WARNING:  this may decrease keys of nodes that are already extracted.  Better not to use it.  :)
	const bool decreaseDuringExtraction	=false;	

	// PHILSOSOPHICAL WARNING:
	//
	// Sometimes we will end up inserting two keys with the same value.  This should be really
	// unlikely but I've encountered test cases where it actually happens.  
	//
	// In the event that a heap contains two identically-keyed nodes, and that key becomes
	// the minimum, which one of them will be returned is not specified, and either one
	// is okay.
	// 
	// Most importantly:  the two implementations of heap, binary and Fibonacci, are not obligated
	// to produce the same behavior as one another.


	atHeap	Heap(batchSize);
	atFib	FibHeap;

	// create the nodes to insert into heaps
	atHeapNode* Nodes[batchSize];
	atFibNode*	FibNodes[batchSize];

	int i, j, data, fibData;
	float key, fibKey;
	float prevKey, prevFibKey;

	data = fibData = 0;
	key = fibKey = prevKey = 0.0f;

	/*
	Printf("Inserting keys...\n");
	for (i=0; i<batchSize; i++)
	{
		key = makeRandom ? g_DrawRand.GetRanged(0.0f, 10000.0f) : float(i);
		if (negate)
			key = makeRandom ? 10000.0f - key : (batchSize-1.0f) - key ;

		// NOTE:  a node's pointer will become invalid after the node is
		// extracted, so don't use 'em after we start extracting nodes.
		Nodes[i] = Heap.Insert(key, i);
	}

	Printf("Deleting keys...\n");
	for (i=batchSize-1; i>=0; i--)
	{
		Heap.Delete(Nodes[g_DrawRand.GetRanged(0, i)]);
	}
	*/
	
	Printf("Inserting keys...\n");
	for (i=0; i<batchSize; i++)
	{
		// make up a key value for data element i
		if (randomInsert)
		{
			key = g_DrawRand.GetRanged(0.0f, 10000.0f);
			if (negate)
				key = 10000.0f-key;
		}
		else
		{
			key = negate ? float(i) : batchSize - 1.0f - float(i);
		}

		// NOTE:  a node's pointer will become invalid after the node is
		// extracted, so don't use 'em after we start extracting nodes.
		// insert data element i with key key
		Heap.Insert(key, i, &(Nodes[i]));
		FibNodes[i] = FibHeap.Insert(key, i);
		Assert(Nodes[i]->Key == FibNodes[i]->Key);
	}

	// randomly decrease keys, decrease keys non randomly
	if (randomDecrease)
	{
		Printf("Decreasing keys...\n");
		for (i=0; i<batchSize; i++)
		{
			// select a random index
			int index = g_DrawRand.GetRanged(0, batchSize-1);
			// get nodes of that index
			atHeapNode* node = Nodes[index];
			atFibNode* fibNode = FibNodes[index];
			// assert they are the same nodes
			Assert(node->Key == fibNode->Key);
			// get a key value and randomly decrease it
			key = node->Key;
			key -= g_DrawRand.GetRanged(0.0f, 1000.0f);
			// call the decreasekey method to actually decrease it
			Heap.DecreaseKey(node, key);
			FibHeap.DecreaseKey(fibNode, key);
			
			// Note: tricky.
			atHeapNode* node2;
			node2 = Nodes[index];
			Assert(node2->Key == fibNode->Key);
		}
	}
	else
	{
		// statically decrease keys in a simliar fashion
		Printf("Decreasing keys...\n");
		for (i=0; i<batchSize; i++)
		{
			key = Nodes[i]->Key;
			key -= 5.0f;
			Heap.DecreaseKey(Nodes[i], key);
			FibHeap.DecreaseKey(FibNodes[i], key);
		}
	}

	// asserts minmum keys from both heaps are equal
	Printf("Extracting minimum keys...\n");
	bool check = Heap.ExtractMin(prevKey, data);
	FibHeap.ExtractMin(prevFibKey, fibData);
	Assert(prevKey==prevFibKey);
	Assert(data==fibData);

	// Ensure that we got back valid data.
	if (!check)
		Quitf("BOGOSITY!  Ran out of nodes.\n");

	// remove all the keys one by one until we are done
	for (i=1; i<batchSize; i++)
	{
		check = Heap.ExtractMin(key, data);
		FibHeap.ExtractMin(fibKey, fibData);

		Assert(key==fibKey);

		// if data is unequal print warning
		if (data!=fibData)
			Warningf("Warning: a key with value %f had data %d in Fibonacci heap, but %d in binary heap.\n", key, fibData, data);
	
		// Ensure that we got back valid data.
		if (!check)
			Quitf("BOGOSITY!  Ran out of nodes.\n");

		int nodeCount = Heap.GetNodeCount()-1;

		if (decreaseDuringExtraction)
		{
			// decreases more keys in the heaps
			Printf("Decreasing keys during extraction...\n");
			int howManyToDecrease = Min(nodeCount, 10);
			for (j=0; j<howManyToDecrease; j++)
			{
				int index = g_DrawRand.GetRanged(0, nodeCount);
				atHeapNode* node = Nodes[index];
				atFibNode* fibNode = FibNodes[index];
				Assert( node->Key == fibNode->Key);
				key = node->Key;
				if (randomDecrease)
					key -= g_DrawRand.GetRanged(0.0f, 1000.0f);
				else
					key -= 5.0f;
				Heap.DecreaseKey(node, key);
				FibHeap.DecreaseKey(fibNode, key);
			}
		}
		else
		{
			// Ensure current key is always greater than or equal to previous
			if (i>0 && key < prevKey)
				Quitf("BOGOSITY!  Keys were not sorted correctly.\n");

			if (!randomInsert && i>0)
			{
				// Ensure current key is always greater than or equal to previous
				if (key - prevKey != 1.0f)
					Quitf("BOGOSITY!  Keys were munged.\n");
			}
		}

		prevKey = key;
		prevFibKey = fibKey;
	}

	Printf("All good!\n");

	return 0;
}

