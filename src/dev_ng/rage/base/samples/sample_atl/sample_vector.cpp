//
// sample_vector.cpp
// Copyright (C) 1999-2012 Rockstar Games. All Rights Reserved.
//

#include "system/main.h"

#include "atl/string.h"
#include "atl/vector.h"

using namespace rage;

class StringHolder {
public:
	StringHolder() {
		sm_NumInstances++;

		Displayf("%d: Default construct", sm_NumInstances);
	}

	StringHolder(const char* str) {
		sm_NumInstances++;
		m_String = str;

		Displayf("%d: Construct %s", sm_NumInstances, str);
	}

	StringHolder(const StringHolder& str) {
		sm_NumInstances++;
		m_String = str.m_String;

		Displayf("%d: CopyConstruct %s", sm_NumInstances, m_String.c_str());
	}

	StringHolder& operator=(const StringHolder& str) {
		if (this != &str)
		{
			Displayf("Assign %s <- %s", m_String.c_str(), str.m_String.c_str());
			m_String = str.m_String;
		}

		return *this;
	}

	~StringHolder() {
		Displayf("%d: Destruct %s", sm_NumInstances-1, m_String.c_str());
		sm_NumInstances--;
	}

	static int sm_NumInstances;
	atString m_String;
};

int StringHolder::sm_NumInstances = 0;

typedef atVector<StringHolder> StringVector;
typedef atFixedVector<StringHolder, 10> StringFixedVector;

void CheckVector(StringVector& vec, int expectedCount, int expectedCapacity, int expectedInstances, const char* expectedContents)
{
	Assertf(vec.GetCount() == expectedCount, "Expected a count of %d, found %d", expectedCount, vec.GetCount());
	Assertf(vec.GetCapacity() == expectedCapacity, "Expected a capacity of %d, found %d", expectedCapacity, vec.GetCapacity());
	Assertf(StringHolder::sm_NumInstances == expectedInstances, "Expected %d instances, found %d", expectedInstances, StringHolder::sm_NumInstances);

	if (expectedContents)
	{
		char buf[256];
		safecpy(buf, expectedContents);

		int index = 0;
		const char* tok = strtok(buf, " ");
		while(tok)
		{
			Assertf(!strcmp(vec[index].m_String.c_str(), tok), "Expected vec[%d] to be \"%s\", got \"%s\"", index, tok, vec[index].m_String.c_str());
			index++;
			tok = strtok(NULL, " ");
		}
	}
	Displayf("Test passed: %s", expectedContents);
}

void CheckVector(StringFixedVector& vec, int expectedCount, int /*expectedCapacity*/, int expectedInstances, const char* expectedContents)
{
	Assertf(vec.GetCount() == expectedCount, "Expected a count of %d, found %d", expectedCount, vec.GetCount());
	Assertf(StringHolder::sm_NumInstances == expectedInstances, "Expected %d instances, found %d", expectedInstances, StringHolder::sm_NumInstances);

	if (expectedContents)
	{
		char buf[256];
		safecpy(buf, expectedContents);

		int index = 0;
		const char* tok = strtok(buf, " ");
		while(tok)
		{
			Assertf(!strcmp(vec[index].m_String.c_str(), tok), "Expected vec[%d] to be \"%s\", got \"%s\"", index, tok, vec[index].m_String.c_str());
			index++;
			tok = strtok(NULL, " ");
		}
	}
	Displayf("Test passed: %s", expectedContents);
}

int Main() 
{

	StringVector testA;

	CheckVector(testA, 0, 0, 0, NULL);

	testA.PushAndGrow("a", 4);
	testA.PushAndGrow("b", 4);
	testA.PushAndGrow("c", 4);
	CheckVector(testA, 3, 4, 3, "a b c");

	testA.PushAndGrow("d", 4);
	testA.PushAndGrow("e", 4);
	testA.PushAndGrow("f", 4);
	CheckVector(testA, 6, 8, 6, "a b c d e f");

	StringHolder& holder = testA.Insert(3);
	holder.m_String = "3";

	CheckVector(testA, 7, 8, 7, "a b c 3 d e f");

	testA.Reset();

	CheckVector(testA, 0, 0, 0, NULL);

	testA.Reserve(4);

	CheckVector(testA, 0, 4, 0, NULL);

	testA.Append("A");
	testA.ResizeGrow(4, "B");

	CheckVector(testA, 4, 4, 4, "A B B B");

	testA.Reset();
	testA.Reserve(4);
	testA.Append("A");
	testA.ResizeGrow(5, "B");

	CheckVector(testA, 5, 5, 5, "A B B B B");

	testA.PushAndGrow("C", 4);
	testA.DeleteFast(2);

	CheckVector(testA, 5, 9, 5, "A B C B B");

	testA.Delete(2);

	CheckVector(testA, 4, 9, 4, "A B B B");

	testA.Insert(2) = "C";

	CheckVector(testA, 5, 9, 5, "A B C B B");


	StringVector testB;
	testB.PushAndGrow("zero", 4);
	testB.PushAndGrow("one", 4);
	testB.PushAndGrow("two", 4);

	// 4th arg is _total_ instances
	CheckVector(testB, 3, 4, 8, "zero one two");

	testA.Swap(testB);

	CheckVector(testA, 3, 4, 8, "zero one two");

	testB = testA;

	CheckVector(testA, 3, 4, 6, "zero one two");
	CheckVector(testB, 3, 9, 6, "zero one two");

	testA.Insert(1) = "half";

	CheckVector(testA, 4, 4, 7, "zero half one two");
	CheckVector(testB, 3, 9, 7, "zero one two");

	testA.Assume(testB);

	CheckVector(testA, 3, 9, 3, "zero one two");
	CheckVector(testB, 0, 0, 3, NULL);

	testA.Reset();
	testB.Reset();

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	StringFixedVector fixedA;

	CheckVector(fixedA, 0, 0, 0, NULL);

	fixedA.Push("a");
	fixedA.Push("b");
	fixedA.Push("c");
	CheckVector(fixedA, 3, 4, 3, "a b c");

	fixedA.Push("d");
	fixedA.Push("e");
	fixedA.Push("f");
	CheckVector(fixedA, 6, 8, 6, "a b c d e f");

	fixedA.Insert(3) = "3";

	CheckVector(fixedA, 7, 8, 7, "a b c 3 d e f");

	fixedA.Reset();

	CheckVector(fixedA, 0, 0, 0, NULL);

	fixedA.Push("A");
	fixedA.SetCount(4, "B");

	CheckVector(fixedA, 4, 4, 4, "A B B B");

	fixedA.Reset();
	fixedA.Push("A");
	fixedA.SetCount(5, "B");

	CheckVector(fixedA, 5, 5, 5, "A B B B B");

	fixedA.Push("C");
	fixedA.DeleteFast(2);

	CheckVector(fixedA, 5, 9, 5, "A B C B B");

	fixedA.Delete(2);

	CheckVector(fixedA, 4, 9, 4, "A B B B");

	fixedA.Insert(2) = "C";

	CheckVector(fixedA, 5, 9, 5, "A B C B B");

	fixedA.insert(fixedA.begin() + 3, 5, "D");

	CheckVector(fixedA, 10, 10, 10, "A B C D D D D D B B");

	fixedA.erase(fixedA.begin() + 5, fixedA.begin() + 9);

	CheckVector(fixedA, 6, 6, 6, "A B C D D B");


	StringFixedVector fixedB;
	fixedB.Push("zero");
	fixedB.Push("one");
	fixedB.Push("two");

	// 4th arg is _total_ instances
	CheckVector(fixedB, 3, 4, 9, "zero one two");

	fixedA = fixedB;

	CheckVector(fixedA, 3, 4, 6, "zero one two");
	CheckVector(fixedB, 3, 9, 6, "zero one two");

	fixedA.Insert(1) = "half";

	CheckVector(fixedA, 4, 4, 7, "zero half one two");
	CheckVector(fixedB, 3, 9, 7, "zero one two");

	fixedA.Reset();
	fixedB.Reset();

	CheckVector(fixedA, 0, 0, 0, NULL);

	return 0;
}