#include "atl/fibheap.h"

#include "diag/output.h"
#include "file/stream.h"
#include "file/token.h"
#include "math/random.h"
#include "system/main.h"
#include "system/param.h"


namespace rage {

typedef	atFibHeap<float, int>		atHeap;
typedef atFibHeap<float, int>::Node	atHeapNode;

}

using namespace rage;


int Main() 
{
	atHeap Heap;
	mthRandom randSource;

	const int batchSize		=2000;
	const bool makeRandom	=true;
	const bool negate		=false;
	const bool debugPrint	=true;

	atHeapNode* Nodes[batchSize];

	int i, data;
	float key;
	float prevKey;

	Displayf("Inserting nodes...\n");
	for (i=0; i<batchSize; i++)
	{
		key = makeRandom ? randSource.GetRanged(0.0f, 10000.0f) : float(i);
		if (negate)
			key = makeRandom ? 10000.0f - key : (batchSize-1.0f) - key ;

		if (debugPrint)
		{
			Displayf("Inserted node with key %.3f, value %d", key, i);
		}

		// NOTE:  a node's pointer will become invalid after the node is
		// extracted, so don't use them after we start extracting nodes.
		Nodes[i] = Heap.Insert(key, i);
	}

	Displayf("Decreasing node keys...\n");
	for (i=0; i<batchSize; i++)
	{
		int whichNode		= randSource.GetRanged(0, batchSize-1);
		atHeapNode* node	= Nodes[whichNode];
		float oldKey		= node->Key;
		float newKey		= randSource.GetRanged(0.0f, oldKey);

		if (debugPrint)
		{
			Displayf("Decreasing node %d's key from %.3f to %.3f", whichNode, oldKey, newKey);
		}

		Heap.DecreaseKey(node, newKey);
	}


	Displayf("Extracting nodes in sorted order...\n");
	prevKey = 0.0f;
	bool check;
	for (i=0; i<batchSize; i++)
	{
		check = Heap.ExtractMin(key, data);

		// Ensure that we got back valid data.
		if (!check)
		{
			Quitf("Expected more nodes in the heap during extraction.\n");
		}

		if (debugPrint)
		{
			Displayf("Extracted minimum node:  key %.3f, data %d", key, data);
		}

		// Ensure current key is always greater than or equal to previous
		if (key < prevKey)
		{
			Quitf("Keys were not sorted correctly.\n");
		}

		prevKey = key;
	}

	Displayf("Testing of fibheap complete; all tests passed.\n");
	
	return 0;
}
