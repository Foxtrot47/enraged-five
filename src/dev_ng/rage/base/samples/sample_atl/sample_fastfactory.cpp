// 
// /sample_fastfactory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "atl/fastfactory.h"
#include "diag/output.h"
#include "system\main.h"

//---------------------------------------------------------------------------------------------
//
//		Particle Example
//---------------------------------------------------------------------------------------------


using namespace rage;

//
//	STEP #1 Create your abstract base class

//
//	PURPOSE
//		abstract base class 
//
class FastParticleUpdater
{
public:
	virtual ~FastParticleUpdater() { }
	virtual void updateParticle( float time ) = 0;
};





//
//	STEP #2 Create your concrete class which does has all the static branches
//
//

//
//	PURPOSE
//		Concrete class which has a series of int template parameters
//		each parameter is a the equivialent of a static branch in the GPU.
//	    So using macros below code for both sides of the static branch is created.
//
template<int DoHits, int DoCollisionResponse, int IsAnimated, int isWraped, int isModel, int isBillboard>
class ParticleUpdate : public FastParticleUpdater
{
public:
	void updateParticle( float time )
	{
		Printf( " Time  %f \n", time );

		if ( DoHits)
		{
			if ( DoCollisionResponse )
			{
				Printf(" Do hits and collsion response" );
			}
		}
		if ( IsAnimated )
		{
			Printf("Is Animated");
			if ( isWraped )
			{
				Printf(" Animation wraps ");
			}
			else
			{
				Printf("Animation clamps");
			}
		}
		if ( isModel )
		{
			Printf("isModel" );
		}
		if ( isBillboard )
		{
			Printf("isBillboard" );
		}

	}
};


// It is important to do this bottom up or else the functions would be created for the start
// Note the nested if's aren't really needed but are good to minimize code bloat
//
//	So the format is
//	FAST_FACTORY_FINISH< N >( abstract base class,  concrete class with the ifs )
//	FAST_FACTORY_BOOL<N-1>( abstract base class )
//	..
//	..
//	FAST_FACTORY_BOOL0( abstract base class )
//
//	where N is the number of template ints in the concrete class ( ParticleUpdate )
//
//
//	For nested ifs you have to specifey how deep the ifs are nested 
//	currently I only support one level of depth. But as this is only for code bloat you are use that until I do more
//
//	I have tabbed the defines to show how the nested works.
//
//	I only have up to 8 static branches set up with macros so far which mean the class with be created 256 times if you don't use
//	nesting, so it's probably enough. In the particle function you have seven static branches as far as I can see so that
// should be fine.
//


//
//	STEP #2 Create the FastFactory Defination of the branches
//
//
FAST_FACTORY_FINISH6( FastParticleUpdater, ParticleUpdate);	// generate
FAST_FACTORY_BOOL5( FastParticleUpdater);			//isBillboard
FAST_FACTORY_BOOL4( FastParticleUpdater);			// isModel
FAST_FACTORY_BOOL3( FastParticleUpdater );			// isWraped
FAST_FACTORY_NEST1_BOOL2( FastParticleUpdater );	//IsAnimated
FAST_FACTORY_BOOL1( FastParticleUpdater);		//DoCollisionResponse
FAST_FACTORY_NEST1_BOOL0( FastParticleUpdater); // DoHits has a nested if of one level 



int Main() 
{
	//
	//	STEP #2 Create the code ( actually selects the correct code ) using the current setup
	//
	//
	// You could do this part per frame but it would probably be best to do it on the load
	// If you are doing hits and you want to dynamically change the number of hit spheres or something you could change it per
	// frame
	//
	FastParticleUpdater* updater = FastFactoryCreate<FastParticleUpdater>( FASTFACTORY_CREATOR( FastParticleUpdater), true, true, false, false, true, true  );

	// This calls the statically compiled optimized code.
	updater->updateParticle( 0.1f );

	delete updater;

	return 0;
}
