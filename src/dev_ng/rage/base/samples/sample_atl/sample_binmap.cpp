// 
// /sample_binmap.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/binmap.h"
#include "system/main.h"

using namespace rage;


atBinaryMap<const char*, int> g_Map;

void Check(int i)
{
	const char** item;
	item = g_Map.SafeGet(i);
	if (item)
	{
		Printf("%d == %s\n", i, *item);
	}
	else
	{
		Printf("%d not found\n", i);
	}
}

int Main()
{

	g_Map.Insert(0, "Zero");
	g_Map.Insert(2, "Two");
	g_Map.Insert(4, "Four");
	g_Map.Insert(6, "Six");
	g_Map.Insert(8, "Eight");
	g_Map.Insert(10, "Ten");
	g_Map.Insert(-1, "Negative One");
	g_Map.Insert(-2, "Negative Two");
	g_Map.Insert(-3, "Negative Three");

	g_Map.FinishInsertion();

	Check(0);
	Check(-3);
	Check(6);
	Check(2);
	Check(10);
	Check(1);
	Check(11);
	Check(-4);

	return 0;
}
