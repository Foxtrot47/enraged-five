#include "atl/bintree.h"

#include "diag/output.h"
#include "math/amath.h"
#include "math/random.h"
#include "system/main.h"
#include "system/timer.h"

using namespace rage;


int Main ()
{
	// control parameters
	const bool kDebugPrintouts = true;
	const bool kValidateOnAddition = true;
	const bool kValidateOnRemoval = true;
	const bool kTraverse = true;
	const bool kTestIntKey = true;
	const bool kTestConstStringKey = true;
	const bool kTestPrealloc = true;
	//const bool kTestTreeAllocated = true;

	sysTimer timer;
	mthRandom randSource;

	if (kTestIntKey)
	{
		// int-key parameters
		const int kRunSize = 2000;
		const int kKeyRange = (int)(kRunSize * logf((float)kRunSize) * 2.5f); // allows for some collisions
		const int kBufferSize = 35;

		atBinTree<int,ConstString> tree;

		char buffer[kBufferSize];

		atBinTree<int,ConstString>::Node * nodes = rage_new atBinTree<int,ConstString>::Node [kRunSize];
		atBinTree<int,ConstString>::Node * lookupNode;
		bool nodeInTree[kRunSize];

		// make some nodes
		int i;
		for (i=0; i<kRunSize; i++)
		{
			// make random data
			for (int j=0; j<kBufferSize-1; j++)
			{
				buffer[j] = (char)('a' + (char)(randSource.GetRanged(0,25)));
			}
			buffer[kBufferSize-1] = 0;

			// make a new node
			nodes[i].SetKey(randSource.GetRanged(0,kKeyRange));
			nodes[i].m_Data = buffer;
		}

		timer.Reset();

		// insert them into the tree
		for (i=0; i<kRunSize; i++)
		{
			//Displayf("node (%d,'%s')",nodes[i].GetKey(),nodes[i].GetData().m_String);

			// insert it into the tree
			bool rv = tree.AddNode(nodes[i]);

			if (kValidateOnAddition)
			{
				tree.Validate();
			}

			if (rv)
			{
				// node was inserted into the tree
				nodeInTree[i] = true;
				if (kDebugPrintouts)
				{
					Displayf("added node %d, nodes %d",nodes[i].GetKey(),tree.GetNumNodes());
				}
			}
			else
			{
				// the tree already has a node with this key
				nodeInTree[i] = false;
				if (kDebugPrintouts)
				{
					Displayf("node %d already in the tree, nodes %d",nodes[i].GetKey(),tree.GetNumNodes());
				}
				Assert(tree.FindNode(nodes[i].GetKey()));
			}

			// check min
			lookupNode = tree.FindMinimumNode();
			Assert(lookupNode);
			//Displayf("lookupNode(min) (%d,'%s')",lookupNode->GetKey(),lookupNode->GetData().m_String);
			Assert(lookupNode->GetKey()<nodes[i].GetKey() || lookupNode==nodes+i || !nodeInTree[i]);

			// check max
			lookupNode = tree.FindMaximumNode();
			Assert(lookupNode);
			//Displayf("lookupNode(max) (%d,'%s')",lookupNode->GetKey(),lookupNode->GetData().m_String);
			Assert(lookupNode->GetKey()>nodes[i].GetKey() || lookupNode==nodes+i || !nodeInTree[i]);
		}
		
		// validate tree
		tree.Validate();

		// get them in order
		if (kTraverse)
		{
			lookupNode = tree.FindMinimumNode();
			atBinTree<int,ConstString>::Node * prevNode = NULL;
			while (lookupNode)
			{
				if (kDebugPrintouts)
				{
					Displayf("next node, '%d'",lookupNode->GetKey());
				}
				Assert(!prevNode || atOrderLT(prevNode->GetKey(),lookupNode->GetKey()));
				prevNode = lookupNode;
				lookupNode = tree.FindSuccessorNode(*lookupNode);
				if (lookupNode)
				{
					Assert(prevNode==tree.FindPredecessorNode(*lookupNode));
				}
			}
		}

		// look them up
		for (i=0; i<kRunSize; i++)
		{
			Assert(tree.FindNode(nodes[i].GetKey()));
		}

		// remove them
		for (i=0; i<kRunSize; i++)
		{
			if (nodeInTree[i])
			{
				if (kDebugPrintouts)
				{
					Displayf("removing node %d, nodes %d",nodes[i].GetKey(),tree.GetNumNodes());
				}
				tree.RemoveNode(nodes[i]);
				if (kValidateOnRemoval)
				{
					tree.Validate();
				}
			}
			Assert(!tree.FindNode(nodes[i].GetKey()));
		}

		// print timing results
		if (!kDebugPrintouts && !kValidateOnRemoval && !kValidateOnAddition)
		{
#if !__FINAL
			float time = timer.GetMsTime();
			Displayf("time(ms) %f ms",time);
			Displayf("time/runSize %f us",1000.0f*time/kRunSize);
			Displayf("time/(runSize*log(runSize)) %f us",1000.0f*time/(kRunSize*logf((float)kRunSize)));
#endif
		}

		delete [] nodes;
	}

	if (kTestConstStringKey)
	{
		// ConstString-key parameters
		const int kRunSize = 30;
		const int kKeyMinLen = 10;
		const int kKeyMaxLen = 15;

		atBinTree<ConstString,int> tree;

		char buffer[kKeyMaxLen];

		atBinTree<ConstString,int>::Node * nodes = rage_new atBinTree<ConstString,int>::Node [kRunSize];
		atBinTree<ConstString,int>::Node * lookupNode;
		bool nodeInTree[kRunSize];

		// make some nodes
		int i;
		for (i=0; i<kRunSize; i++)
		{
			// make random key
			int stringLen = randSource.GetRanged(kKeyMinLen,kKeyMaxLen);
			for (int j=0; j<stringLen-1; j++)
			{
				buffer[j] = (char)('a' + (char)(randSource.GetRanged(0,25)));
			}
			buffer[stringLen-1] = 0;

			// setup the node
			nodes[i].SetKey(ConstString(buffer));
			nodes[i].m_Data = 0;
		}

		timer.Reset();

		// insert them into the tree
		for (i=0; i<kRunSize; i++)
		{
			// insert it into the tree
			bool rv = tree.AddNode(nodes[i]);

			if (kValidateOnAddition)
			{
				tree.Validate();
			}

			if (rv)
			{
				// node was inserted into the tree
				nodeInTree[i] = true;
				if (kDebugPrintouts)
				{
					Displayf("added node '%s', nodes %d",nodes[i].GetKey().m_String,tree.GetNumNodes());
				}
			}
			else
			{
				// the tree already has a node with this key
				nodeInTree[i] = false;
				if (kDebugPrintouts)
				{
					Displayf("node '%s' already in the tree, nodes %d",nodes[i].GetKey().m_String,tree.GetNumNodes());
				}
				Assert(tree.FindNode(nodes[i].GetKey()));
			}

			// check min
			lookupNode = tree.FindMinimumNode();
			Assert(lookupNode);
			Assert(atOrderLT(lookupNode->GetKey(),nodes[i].GetKey()) || lookupNode==nodes+i || !nodeInTree[i]);

			// check max
			lookupNode = tree.FindMaximumNode();
			Assert(lookupNode);
			Assert(atOrderLT(nodes[i].GetKey(),lookupNode->GetKey()) || lookupNode==nodes+i || !nodeInTree[i]);
		}

		// validate tree
		tree.Validate();

		// get them in order
		if (kTraverse)
		{
			lookupNode = tree.FindMinimumNode();
			atBinTree<ConstString,int>::Node * prevNode = NULL;
			while (lookupNode)
			{
				if (kDebugPrintouts)
				{
					Displayf("next node, '%s'",lookupNode->GetKey().m_String);
				}
				Assert(!prevNode || atOrderLT(prevNode->GetKey(),lookupNode->GetKey()));
				prevNode = lookupNode;
				lookupNode = tree.FindSuccessorNode(*lookupNode);
				if (lookupNode)
				{
					Assert(prevNode==tree.FindPredecessorNode(*lookupNode));
				}
			}
		}

		// look them up
		for (i=0; i<kRunSize; i++)
		{
			Assert(tree.FindNode(nodes[i].GetKey()));
		}

		// remove them
		for (i=0; i<kRunSize; i++)
		{
			if (nodeInTree[i])
			{
				if (kDebugPrintouts)
				{
					Displayf("removing node '%s', nodes %d",nodes[i].GetKey().m_String,tree.GetNumNodes());
				}
				tree.RemoveNode(nodes[i]);
				if (kValidateOnRemoval)
				{
					tree.Validate();
				}
			}
			Assert(!tree.FindNode(nodes[i].GetKey()));
		}

		// print timing results
		if (!kDebugPrintouts && !kValidateOnRemoval && !kValidateOnAddition)
		{
#if !__FINAL
			float time = timer.GetMsTime();
			Displayf("time(ms) %f ms",time);
			Displayf("time/runSize %f us",1000.0f*time/kRunSize);
			Displayf("time/(runSize*log(runSize)) %f us",1000.0f*time/(kRunSize*logf((float)kRunSize)));
#endif
		}

		delete [] nodes;
	}

	if (kTestPrealloc)
	{
		// ConstString-key parameters
		const int kAllocSize = 100;
		const int kRunSize = 100;

		atBinTree<float,int> tree(kAllocSize);

		int i;
		for (i=0; i<kRunSize; i++)
		{
			float key = randSource.GetRanged(0.0f,1000.0f);
			int data = randSource.GetRanged(0,1000);
			if (tree.Insert(key,data))
			{
				Displayf("inserted (%f,%d)",key,data);
			}
			else
			{
				Displayf("already in (%f,%d)",key,data);
			}
		}

		tree.Validate();

		atBinTree<float,int>::Node * foundNode = NULL;
		while ((foundNode=tree.FindMaximumNode())!=NULL)
		{
			Displayf("deleting %f,%d",foundNode->GetKey(),foundNode->m_Data);
			tree.Delete(foundNode->GetKey());
		}
	}

	Displayf("Done");
	
	return 0;
}
