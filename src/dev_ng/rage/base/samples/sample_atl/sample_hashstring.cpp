// 
// /sample_string.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


#include "system/main.h"

#include "atl/hashstring.h"

using namespace rage;

void TakesS32(s32) {}
void TakesU32(u32) {}
void TakesString(const char*) {}
void TakesHashValue(atHashValue) {}
void TakesHashString(atNonFinalHashString) {}
void TakesFinalHashString(atFinalHashString) {}
void TakesLiteralHashString(atLiteralHashString) {}
void TakesLiteralHashValue(atLiteralHashValue) {}
void TakesDiagHashString(atDiagHashString) {}
void TakesDiagHashValue(atDiagHashValue) {}

template<typename T> void TestHash(const char* str)
{
	T hs(str);
	Displayf("0x%x \"%s\" \"%s\"", hs.GetHash(), hs.TryGetCStr(), str);
}

int Main()
{
	TestHash<atNonFinalHashString>("this is a test");
	TestHash<atNonFinalHashString>("foo");
	TestHash<atNonFinalHashString>("FOO");

	TestHash<atFinalHashString>("this is a final test");
	TestHash<atFinalHashString>("foo");
	TestHash<atFinalHashString>("FOO");

	TestHash<atLiteralHashString>("this is a final test");
	TestHash<atLiteralHashString>("foo");
	TestHash<atLiteralHashString>("FOO");

	TestHash<atDiagHashString>("this is a diag test");
	TestHash<atDiagHashString>("foo");
	TestHash<atDiagHashString>("FOO");

	for(int i = 0; i < HSNS_NUM_NAMESPACES; i++)
	{
		atHashStringStats stats = atHashStringNamespaceSupport::FindStats((atHashStringNamespaces)i);
		Displayf("Namespace %d %s: count: %d, chars: %d", i, atHashStringNamespaceSupport::GetNamespaceName((atHashStringNamespaces)i), stats.m_refdStringCount, stats.m_refdStringChars);
	}

	atNonFinalHashString hs("a nonfinal hash string");
	atFinalHashString fhs("a final hash string");
	atLiteralHashString lhs("a literal hash string");
	atDiagHashString dhs("a diag hash string");

	Displayf("%p %s %p %s %p %s %p %s", &hs, hs.TryGetCStr(), &fhs, fhs.TryGetCStr(), &lhs, lhs.TryGetCStr(), &dhs, dhs.TryGetCStr());

	// Test out various conversions, to make sure only the desired ones will compile

	// atNonFinalHashString, atFinalHashString and atHashValue are all "lenient" in regards to their implicit conversions
	// atLiteralHashString/Value and atDiagHashString/Value are much more restricted
	// The tests below check for conversions among the lenient types, between the lenient and restricted types, and between the restricted types

	// To test this. search for "/ / O K" (without spaces), replace with "/ * O K * /" (without spaces). Build, make sure compile errors only show up on OK lines, and then revert

	/////////////////////////////////////////////////////////
	// atNonFinalHashString
	{
		/*
		atNonFinalHashString operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																		y			y																						
		NULL																						
		int												y							y			y
		u32							y					y				y			y			y
		const char*					y									y			y			y
		atHashValue					y					y				y			y			y
		atNonFinalHashString		y					y				y			y			y
		atFinalHashString															y			y
		atLiteralHashString																			
		atLiteralHashValue																			
		*/

		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atNonFinalHashString hs0(NULL);
		//OK atNonFinalHashString hs1(-1);
		atNonFinalHashString hs2(0U);
		atNonFinalHashString hs3("foo");
		atNonFinalHashString hs4(hashval);
		atNonFinalHashString hs5(hashstr);
		//OK atNonFinalHashString hs6(finalhashstr);
		//OK atNonFinalHashString hs7(literalhashstr);	
		//OK atNonFinalHashString hs8(literalval);		

		atNonFinalHashString teststr;

		// Implicit conversions
		TakesS32(teststr);
		TakesU32(teststr);
		//OK TakesString(teststr);	
		TakesHashValue(teststr);
		TakesHashString(teststr);
		//OK TakesFinalHashString(teststr); 
		//OK TakesLiteralHashString(teststr); 
		//OK TakesLiteralHashValue(teststr); 

		// Assignments
		//OK teststr = NULL;
		teststr = 1;
		teststr = 0xABCD1234U; 
		teststr = "foo";
		teststr = hashval;
		teststr = hashstr;
		//OK teststr = finalhashstr;
		//OK teststr = literalhashstr;	
		//OK teststr = literalval;		

		// Comparisons
		if (teststr) {}
		//OK if (teststr == NULL) {}
		if (teststr == 1) {}
		if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		if (teststr == hashval) {}
		if (teststr == hashstr) {}
		if (teststr == finalhashstr) {}	
		//OK if (teststr == literalhashstr) {}
		//OK if (teststr == literalval) {}

		if (!teststr) {}
		//OK if (teststr != NULL) {}
		if (teststr != 1) {}
		if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		if (teststr != hashval) {}
		if (teststr != hashstr) {}
		if (teststr != finalhashstr) {}
		//OK if (teststr != literalhashstr) {}
		//OK if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		if (1				== teststr) {}
		if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}	
		if (hashval			== teststr) {}
		if (hashstr			== teststr) {}
		if (finalhashstr	== teststr) {} 
		//OK if (literalhashstr	== teststr) {}
		//OK if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		if (1				!= teststr) {}
		if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}		
		if (hashval			!= teststr) {}
		if (hashstr			!= teststr) {}
		if (finalhashstr	!= teststr) {}
		//OK if (literalhashstr	!= teststr) {}
		//OK if (literalval		!= teststr) {}
	}


	/////////////////////////////////////////////////////////
	// atFinalHashString
	{
		/*
		atFinalHashString operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																		y			y
		NULL															y							
		int												y							y			y
		u32							y					y							y			y
		const char*					y									y			y			y
		atHashValue					y					y							y			y
		atNonFinalHashString														y			y
		atFinalHashString			y					y				y			y			y
		atLiteralHashString																			
		atLiteralHashValue																			
		*/


		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atFinalHashString hs0(NULL);
		//OK atFinalHashString hs1(-1);
		atFinalHashString hs2(0U);
		atFinalHashString hs3("foo");
		atFinalHashString hs4(hashval);	
		//OK atFinalHashString hs5(hashstr);
		atFinalHashString hs6(finalhashstr);
		//OK atFinalHashString hs7(literalhashstr);
		//OK atFinalHashString hs8(literalval);

		atFinalHashString teststr;

		// Implicit conversions
		TakesS32(teststr);
		TakesU32(teststr);
		//OK TakesString(teststr);
		TakesHashValue(teststr);
		//OK TakesHashString(teststr);
		TakesFinalHashString(teststr);
		//OK TakesLiteralHashString(teststr);
		//OK TakesLiteralHashValue(teststr);

		// Assignments
		teststr = NULL;
		//OK teststr = -1;
		//OK teststr = 0xABCD1234U; 
		teststr = "foo";
		//OK teststr = hashval;
		//OK teststr = hashstr;
		teststr = finalhashstr;
		//OK teststr = literalhashstr;
		//OK teststr = literalval;

		// Comparisons
		if (teststr) {}
		//OK if (teststr == NULL) {}
		if (teststr == -1) {}
		if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		if (teststr == hashval) {}
		if (teststr == hashstr) {}
		if (teststr == finalhashstr) {}
		//OK if (teststr == literalhashstr) {}
		//OK if (teststr == literalval) {}

		if (!teststr) {}
		//OK if (teststr != NULL) {}
		if (teststr != -1) {}
		if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		if (teststr != hashval) {}
		if (teststr != hashstr) {}
		if (teststr != finalhashstr) {}
		//OK if (teststr != literalhashstr) {}
		//OK if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		//OK if (-1				== teststr) {}
		if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}
		if (hashval			== teststr) {}
		if (hashstr			== teststr) {}
		if (finalhashstr	== teststr) {}
		//OK if (literalhashstr	== teststr) {}
		//OK if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		//OK if (-1				!= teststr) {}
		if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}
		if (hashval			!= teststr) {}
		if (hashstr			!= teststr) {}
		if (finalhashstr	!= teststr) {}
		//OK if (literalhashstr	!= teststr) {}
		//OK if (literalval		!= teststr) {}
	}

	/////////////////////////////////////////////////////////
	// atLiteralHashString
	{
		/*
		atLiteralHashString operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																					
		NULL															y			y			y	
		int																						
		u32							y															
		const char*					y									y			y			y
		atHashValue																				
		atNonFinalHashString																	
		atFinalHashString																		
		atLiteralHashString			y					y				y			y			y	
		atLiteralHashValue								y							y			y	
		*/


		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atLiteralHashString hs0(NULL);
		//OK atLiteralHashString hs1(-1);
		atLiteralHashString hs2(0U);
		atLiteralHashString hs3("foo");
		//OK atLiteralHashString hs4(hashval);
		//OK atLiteralHashString hs5(hashstr);
		//OK atLiteralHashString hs6(finalhashstr);
		atLiteralHashString hs7(literalhashstr);
		//OK atLiteralHashString hs8(literalval);

		atLiteralHashString teststr;

		// Implicit conversions
		//OK TakesS32(teststr);
		//OK TakesU32(teststr);
		//OK TakesString(teststr);
		//OK TakesHashValue(teststr);
		//OK TakesHashString(teststr);
		//OK TakesFinalHashString(teststr);
		TakesLiteralHashString(teststr);
		TakesLiteralHashValue(teststr);

		// Assignments
		teststr = NULL;
		//OK teststr = -1;
		//OK teststr = 0xABCD1234U; 
		teststr = "foo";
		//OK teststr = hashval;
		//OK teststr = hashstr;
		//OK teststr = finalhashstr;
		teststr = literalhashstr;
		//OK teststr = literalval;

		// Comparisons
		//OK if (teststr) {}
		if (teststr == NULL) {}
		//OK if (teststr == -1) {}
		//OK if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		//OK if (teststr == hashval) {}
		//OK if (teststr == hashstr) {}
		//OK if (teststr == finalhashstr) {}
		if (teststr == literalhashstr) {}
		if (teststr == literalval) {}

		//OK if (!teststr) {}
		if (teststr != NULL) {}
		//OK if (teststr != -1) {}
		//OK if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		//OK if (teststr != hashval) {}
		//OK if (teststr != hashstr) {}
		//OK if (teststr != finalhashstr) {}
		if (teststr != literalhashstr) {}
		if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		//OK if (-1				== teststr) {}
		//OK if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}
		//OK if (hashval			== teststr) {}
		//OK if (hashstr			== teststr) {}
		//OK if (finalhashstr	== teststr) {}
		if (literalhashstr	== teststr) {}
		if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		//OK if (-1				!= teststr) {}
		//OK if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}
		//OK if (hashval			!= teststr) {}
		//OK if (hashstr			!= teststr) {}
		//OK if (finalhashstr	!= teststr) {}
		if (literalhashstr	!= teststr) {}
		if (literalval		!= teststr) {}
	}

	/////////////////////////////////////////////////////////
	// atDiagHashString
	{
		/*
		atDiagHashString operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																					
		NULL															y			y			y	
		int																						
		u32							y															
		const char*					y									y			y			y
		atHashValue																				
		atNonFinalHashString																	
		atFinalHashString																		
		atLiteralHashString																			
		atLiteralHashValue																			
		*/


		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atDiagHashString hs0(NULL);
		//OK atDiagHashString hs1(-1);
		atDiagHashString hs2(0U);
		atDiagHashString hs3("foo");
		//OK atDiagHashString hs4(hashval);
		//OK atDiagHashString hs5(hashstr);	
		//OK atDiagHashString hs6(finalhashstr); 
		//OK atDiagHashString hs7(literalhashstr);
		//OK atDiagHashString hs8(literalval);

		atDiagHashString teststr;

		// Implicit conversions
		//OK TakesS32(teststr);
		//OK TakesU32(teststr);
		//OK TakesString(teststr);
		//OK TakesHashValue(teststr);
		//OK TakesHashString(teststr);
		//OK TakesFinalHashString(teststr);
		//OK TakesLiteralHashString(teststr);
		//OK TakesLiteralHashValue(teststr);

		// Assignments
		teststr = NULL;
		//OK teststr = -1;
		//OK teststr = 0xABCD1234U; 
		teststr = "foo";
		//OK teststr = hashval;
		//OK teststr = hashstr;
		//OK teststr = finalhashstr;
		//OK teststr = literalhashstr;
		//OK teststr = literalval;

		// Comparisons
		//OK if (teststr) {}
		if (teststr == NULL) {}
		//OK if (teststr == -1) {}
		//OK if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		//OK if (teststr == hashval) {}
		//OK if (teststr == hashstr) {}
		//OK if (teststr == finalhashstr) {}
		//OK if (teststr == literalhashstr) {}
		//OK if (teststr == literalval) {}

		//OK if (!teststr) {}
		if (teststr != NULL) {}
		//OK if (teststr != -1) {}
		//OK if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		//OK if (teststr != hashval) {}
		//OK if (teststr != hashstr) {}
		//OK if (teststr != finalhashstr) {}
		//OK if (teststr != literalhashstr) {}
		//OK if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		//OK if (-1				== teststr) {}
		//OK if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}
		//OK if (hashval			== teststr) {}
		//OK if (hashstr			== teststr) {}
		//OK if (finalhashstr	== teststr) {}
		//OK if (literalhashstr	== teststr) {}
		//OK if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		//OK if (-1				!= teststr) {}
		//OK if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}
		//OK if (hashval			!= teststr) {}
		//OK if (hashstr			!= teststr) {}
		//OK if (finalhashstr	!= teststr) {}
		//OK if (literalhashstr	!= teststr) {}
		//OK if (literalval		!= teststr) {}
	}

	/////////////////////////////////////////////////////////
	// atHashValue
	{
		/*
		atHashValue operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																		y			y
		NULL																						
		int												y							y			y
		u32							y					y				y			y			y
		const char*					y									y			y			y
		atHashValue					y					y				y			y			y
		atNonFinalHashString		y									y			y			y
		atFinalHashString			y									y			y			y
		atLiteralHashString																			
		atLiteralHashValue																			
		*/

		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atHashValue hs0(NULL);
		//OK atHashValue hs1(-1);
		atHashValue hs2(0U);
		atHashValue hs3("foo");
		atHashValue hs4(hashval);
		atHashValue hs5(hashstr);
		atHashValue hs6(finalhashstr);
		//OK atHashValue hs7(literalhashstr);
		//OK atHashValue hs8(literalval);

		atHashValue teststr;

		// Implicit conversions
		TakesS32(teststr);
		TakesU32(teststr);
		//OK TakesString(teststr);
		TakesHashValue(teststr);
		//OK TakesHashString(teststr);
		//OK TakesFinalHashString(teststr);
		//OK TakesLiteralHashString(teststr);
		//OK TakesLiteralHashValue(teststr);

		// Assignments
		//OK teststr = NULL;
		//OK teststr = -1;
		teststr = 0xABCD1234U; 
		teststr = "foo";
		teststr = hashval;
		teststr = hashstr;
		teststr = finalhashstr;
		//OK teststr = literalhashstr;
		//OK teststr = literalval;

		// Comparisons
		if (teststr) {}
		//OK if (teststr == NULL) {}
		if (teststr == -1) {}
		if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		if (teststr == hashval) {}
		if (teststr == hashstr) {}
		if (teststr == finalhashstr) {}
		//OK if (teststr == literalhashstr) {}
		//OK if (teststr == literalval) {}

		if (!teststr) {}
		//OK if (teststr != NULL) {}
		if (teststr != -1) {}
		if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		if (teststr != hashval) {}
		if (teststr != hashstr) {}
		if (teststr != finalhashstr) {}
		//OK if (teststr != literalhashstr) {}
		//OK if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		//OK if (-1				== teststr) {}
		if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}
		if (hashval			== teststr) {}
		if (hashstr			== teststr) {}
		if (finalhashstr	== teststr) {}
		//OK if (literalhashstr	== teststr) {}
		//OK if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		//OK if (-1				!= teststr) {}
		if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}
		if (hashval			!= teststr) {}
		if (hashstr			!= teststr) {}
		if (finalhashstr	!= teststr) {}
		//OK if (literalhashstr	!= teststr) {}
		//OK if (literalval		!= teststr) {}
	}

	/////////////////////////////////////////////////////////
	// atLiteralHashValue
	{
		/*
		atLiteralHashValue operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																					
		NULL															y			y			y	
		int																						
		u32							y															
		const char*					y									y			y			y
		atHashValue																				
		atNonFinalHashString																	
		atFinalHashString																		
		atLiteralHashString			y									y			y			y	
		atLiteralHashValue			y					y				y			y			y	
		*/


		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atLiteralHashValue hs0(NULL);
		//OK atLiteralHashValue hs1(-1);
		atLiteralHashValue hs2(0U);
		atLiteralHashValue hs3("foo");
		//OK atLiteralHashValue hs4(hashval);
		//OK atLiteralHashValue hs5(hashstr);	
		//OK atLiteralHashValue hs6(finalhashstr);
		atLiteralHashValue hs7(literalhashstr);
		atLiteralHashValue hs8(literalval);

		atLiteralHashValue teststr;

		// Implicit conversions
		//OK TakesS32(teststr);
		//OK TakesU32(teststr);
		//OK TakesString(teststr);
		//OK TakesHashValue(teststr);
		//OK TakesHashString(teststr);
		//OK TakesFinalHashString(teststr);
		//OK TakesLiteralHashString(teststr);
		TakesLiteralHashValue(teststr);

		// Assignments
		teststr = NULL;
		//OK teststr = -1;
		//OK teststr = 0xABCD1234U; 
		teststr = "foo";
		//OK teststr = hashval;
		//OK teststr = hashstr;
		//OK teststr = finalhashstr;
		teststr = literalhashstr;
		teststr = literalval;

		// Comparisons
		//OK if (teststr) {}
		if (teststr == NULL) {}
		//OK if (teststr == -1) {}
		//OK if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		//OK if (teststr == hashval) {}
		//OK if (teststr == hashstr) {}
		//OK if (teststr == finalhashstr) {}
		if (teststr == literalhashstr) {}
		if (teststr == literalval) {}

		//OK if (!teststr) {}
		if (teststr != NULL) {}
		//OK if (teststr != -1) {}
		//OK if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		//OK if (teststr != hashval) {}
		//OK if (teststr != finalhashstr) {}
		if (teststr != literalhashstr) {}
		if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		//OK if (-1				== teststr) {}
		//OK if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}
		//OK if (hashval			== teststr) {}
		//OK if (hashstr			== teststr) {}
		//OK if (finalhashstr	== teststr) {}
		if (literalhashstr	== teststr) {}
		if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		//OK if (-1				!= teststr) {}
		//OK if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}
		//OK if (hashval			!= teststr) {}
		//OK if (hashstr			!= teststr) {}
		//OK if (finalhashstr	!= teststr) {}
		if (literalhashstr	!= teststr) {}
		if (literalval		!= teststr) {}
	}


	/////////////////////////////////////////////////////////
	// atDiagHashValue
	{
		/*
		atDiagHashValue operations table:

								construct?			convert-to?		 assign?	  op==		  op!=
		bool																					
		NULL															y			y			y	
		int																						
		u32							y															
		const char*					y									y			y			y
		atHashValue																				
		atNonFinalHashString																	
		atFinalHashString																		
		atLiteralHashString																			
		atLiteralHashValue																			
		*/

		atHashValue hashval;
		atNonFinalHashString hashstr;
		atFinalHashString finalhashstr;
		atLiteralHashString literalhashstr;
		atLiteralHashValue literalval;

		// Constructions
		//OK atDiagHashValue hs0(NULL);
		//OK atDiagHashValue hs1(-1);
		atDiagHashValue hs2(0U);
		atDiagHashValue hs3("foo");
		//OK atDiagHashValue hs4(hashval);
		//OK atDiagHashValue hs5(hashstr);
		//OK atDiagHashValue hs6(finalhashstr);
		//OK atDiagHashValue hs7(literalhashstr);
		//OK atDiagHashValue hs8(literalval); 

		atDiagHashValue teststr;

		// Implicit conversions
		//OK TakesS32(teststr);
		//OK TakesU32(teststr);
		//OK TakesString(teststr);
		//OK TakesHashValue(teststr);
		//OK TakesHashString(teststr);
		//OK TakesFinalHashString(teststr);
		//OK TakesLiteralHashString(teststr);
		//OK TakesLiteralHashValue(teststr);

		// Assignments
		teststr = NULL;
		//OK teststr = -1;
		//OK teststr = 0xABCD1234U; 
		teststr = "foo";
		//OK teststr = hashval;
		//OK teststr = hashstr;
		//OK teststr = finalhashstr;
		//OK teststr = literalhashstr;
		//OK teststr = literalval;

		// Comparisons
		//OK if (teststr) {}
		if (teststr == NULL) {}
		//OK if (teststr == -1) {}
		//OK if (teststr == 0xABCD1234u) {}
		if (teststr == "foo") {}
		//OK if (teststr == hashval) {}
		//OK if (teststr == hashstr) {}
		//OK if (teststr == finalhashstr) {}
		//OK if (teststr == literalhashstr) {}
		//OK if (teststr == literalval) {}

		//OK if (!teststr) {}
		if (teststr != NULL) {}
		//OK if (teststr != -1) {}
		//OK if (teststr != 0xABCD1234u) {}
		if (teststr != "foo") {}
		//OK if (teststr != hashval) {}
		//OK if (teststr != finalhashstr) {}
		//OK if (teststr != literalhashstr) {}
		//OK if (teststr != literalval) {}

		//OK if (NULL			== teststr) {}
		//OK if (-1				== teststr) {}
		//OK if (0xABCD1234		== teststr) {}
		//OK if ("foo"			== teststr) {}
		//OK if (hashval			== teststr) {}
		//OK if (hashstr			== teststr) {}
		//OK if (finalhashstr	== teststr) {}
		//OK if (literalhashstr	== teststr) {}
		//OK if (literalval		== teststr) {}

		//OK if (NULL			!= teststr) {}
		//OK if (-1				!= teststr) {}
		//OK if (0xABCD1234		!= teststr) {}
		//OK if ("foo"			!= teststr) {}
		//OK if (hashval			!= teststr) {}
		//OK if (hashstr			!= teststr) {}
		//OK if (finalhashstr	!= teststr) {}
		//OK if (literalhashstr	!= teststr) {}
		//OK if (literalval		!= teststr) {}
	}

	return 0;
}
