// 
// /sample_functor.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/atfunctor.h"
#include "atl/functor.h"
#include "atl/delegate.h"
#include "data/callback.h"
#include "system/main.h"

using namespace rage;


class testClass1 : public datBase
{
public:
	testClass1() {id = "tc1";}
	void Func0()					{Printf("in (%s) Func0\n", id);}
	void Func1(const char* s)		{Printf("in (%s) Func1: %s\n",id, s);}
	int  FuncRet1(const char* s)	{Printf("in (%s) Func2: %s\n", id, s); return 1;}
	virtual void VFunc0()					{Printf("in tc1(%s) VFunc0\n", id);}
	virtual void VFunc1(const char* s)		{Printf("in tc1(%s) VFunc1: %s\n", id, s);}
	virtual int  VFuncRet1(const char* s)	{Printf("in tc1(%s) VFunc2: %s\n", id, s); return 1;}
	const char* id;
};

class testClassDerived : public testClass1
{
public:
	testClassDerived() {derivedid = "tcd";}
	virtual void VFunc0()					{Printf("in tcd(%s:%s) VFunc0\n", id, derivedid);}
	virtual void VFunc1(const char* s)		{Printf("in tcd(%s:%s) VFunc1: %s\n",id, derivedid, s);}
	virtual int  VFuncRet1(const char* s)	{Printf("in tcd(%s:%s) VFunc2: %s\n",id, derivedid, s); return 1;}
	const char* derivedid;
};

class testClassDerivedVirtual1 : public virtual testClass1
{
public:
	testClassDerivedVirtual1() {derivedid = "tcdv1";}
	const char* derivedid;
};

#if __WIN32
#pragma warning (disable: 4250 ) // warning about derived classes hiding base class functions
#endif

class testClassDerivedVirtual2 : public virtual testClass1
{
public:
	testClassDerivedVirtual2() {derivedid = "tcdv2";}
	virtual void VFunc0()					{Printf("in tcdv2(%s:%s) VFunc0\n", id, derivedid);}
	virtual void VFunc1(const char* s)		{Printf("in tcdv2(%s:%s) VFunc1: %s\n",id, derivedid, s);}
	virtual int  VFuncRet1(const char* s)	{Printf("in tcdv2(%s:%s) VFunc2: %s\n",id, derivedid, s); return 1;}
	const char* derivedid;
};

class testClassVirtualInheritance : public testClassDerivedVirtual1, public testClassDerivedVirtual2
{
public:
};

static void Func0Inst1()
{
}

static void Func0Inst2()
{
}

#define TEST_DATCALLBACK(realtype, calltype, function) \
{ \
	Printf(#realtype "." #calltype "::" #function " "); \
	realtype inst; \
	datCallback callback(MFA(calltype::function), &inst); \
	callback.Call(); \
}

#define TEST_FUNCTOR(realtype, calltype, function) \
{ \
	Printf(#realtype "." #calltype "::" #function " "); \
	realtype inst; \
	Functor0 func0 = MakeFunctor(inst, &calltype::function); \
	func0(); \
}

#define TEST_ATFUNCTOR(realtype, calltype, function) \
{ \
	Printf(#realtype "." #calltype "::" #function " "); \
	realtype inst; \
	atFunctor0<void> callback; \
	callback.Reset<calltype, &calltype::function>(&inst); \
	callback(); \
}

#define TEST_DELEGATE(realtype, calltype, function) \
{ \
	Printf(#realtype "." #calltype "::" #function " "); \
	realtype inst; \
	atDelegate<void ()> callback; \
	callback.Bind(&inst, &calltype::function); \
	callback(); \
}



int Main ()
{
	// test the null functor:
	Functor0 nullFunctor1 = Functor0::NullFunctor();
	Functor0 nullFunctor2 = Functor0::NullFunctor();
	Functor1<int> nullFunctor3 = Functor1<int>::NullFunctor();

	// pointer to static functions:
	Functor0 func0Inst1a = MakeFunctor(Func0Inst1);
	Functor0 func0Inst1b = MakeFunctor(Func0Inst1);
	Functor0 func0Inst2 = MakeFunctor(Func0Inst2);

	// pointer to member functions:
	testClass1 test1;
	testClass1 test2;
	Functor0 testClass1Func0Inst1a = MakeFunctor(test1,&testClass1::Func0);
	Functor0 testClass1Func0Inst1b = MakeFunctor(test1,&testClass1::Func0);
	Functor0 testClass1Func0Inst2 = MakeFunctor(test2,&testClass1::Func0);

	Displayf("\nEQUALITY TESTS");
	Displayf("-----------------------------------------------------------------");
	Displayf("Functor0::NullFunctor()==Functor0::NullFunctor() should be 'TRUE': %s",nullFunctor1==nullFunctor1?"TRUE":"FALSE");
	Displayf("Functor0::NullFunctor()==Functor0::NullFunctor() should be 'TRUE': %s",nullFunctor1==nullFunctor2?"TRUE":"FALSE");
	Displayf("Functor0::NullFunctor()==Functor1<int>::NullFunctor() should be 'FALSE': %s",nullFunctor1==nullFunctor3?"TRUE":"FALSE");

	Displayf("Func0Inst1==Func0Inst1 should be 'TRUE': %s",func0Inst1a==func0Inst1a?"TRUE":"FALSE");
	Displayf("Func0Inst1==Func0Inst1 should be 'TRUE': %s",func0Inst1a==func0Inst1b?"TRUE":"FALSE");
	Displayf("Func0Inst1==Func0Inst2 should be 'FALSE': %s",func0Inst1a==func0Inst2?"TRUE":"FALSE");

	Displayf("test1.Func0==test1.Func0 should be 'TRUE': %s",testClass1Func0Inst1a==testClass1Func0Inst1a?"TRUE":"FALSE");
	Displayf("test1.Func0==test1.Func0 should be 'TRUE': %s",testClass1Func0Inst1a==testClass1Func0Inst1b?"TRUE":"FALSE");
	Displayf("test1.Func0==test1.Func0 should be 'TRUE': %s",testClass1Func0Inst1a==testClass1Func0Inst1b?"TRUE":"FALSE");
	Displayf("test1.Func0==test2.Func0 should be 'FALSE': %s",testClass1Func0Inst1a==testClass1Func0Inst2?"TRUE":"FALSE");

	Displayf("\nINEQUALITY TESTS");
	Displayf("-----------------------------------------------------------------");
	Displayf("Functor0::NullFunctor()!=Functor0::NullFunctor() should be 'FALSE': %s",nullFunctor1!=nullFunctor1?"TRUE":"FALSE");
	Displayf("Functor0::NullFunctor()!=Functor0::NullFunctor() should be 'FALSE': %s",nullFunctor1!=nullFunctor2?"TRUE":"FALSE");
	Displayf("Functor0::NullFunctor()!=Functor1<int>::NullFunctor() should be 'TRUE': %s",nullFunctor1!=nullFunctor3?"TRUE":"FALSE");

	Displayf("Func0Inst1!=Func0Inst1 should be 'FALSE': %s",func0Inst1a!=func0Inst1a?"TRUE":"FALSE");
	Displayf("Func0Inst1!=Func0Inst1 should be 'FALSE': %s",func0Inst1a!=func0Inst1b?"TRUE":"FALSE");
	Displayf("Func0Inst1!=Func0Inst2 should be 'TRUE': %s",func0Inst1a!=func0Inst2?"TRUE":"FALSE");

	Displayf("test1.Func0!=test1.Func0 should be 'FALSE': %s",testClass1Func0Inst1a!=testClass1Func0Inst1a?"TRUE":"FALSE");
	Displayf("test1.Func0!=test1.Func0 should be 'FALSE': %s",testClass1Func0Inst1a!=testClass1Func0Inst1b?"TRUE":"FALSE");
	Displayf("test1.Func0!=test1.Func0 should be 'FALSE': %s",testClass1Func0Inst1a!=testClass1Func0Inst1b?"TRUE":"FALSE");
	Displayf("test1.Func0!=test2.Func0 should be 'TRUE': %s",testClass1Func0Inst1a!=testClass1Func0Inst2?"TRUE":"FALSE");

#define TEST TEST_DATCALLBACK
	Displayf("--------------------- CALLBACK ----------------");
	Displayf("Simple");
	TEST(testClass1, testClass1, Func0);
	TEST(testClass1, testClass1, VFunc0);
	Displayf("Derived");
	TEST(testClassDerived, testClassDerived, Func0);
	TEST(testClassDerived, testClassDerived, VFunc0);
	TEST(testClassDerived, testClass1, Func0);
	TEST(testClassDerived, testClass1, VFunc0);
	Displayf("MI/VI");
	TEST(testClassVirtualInheritance, testClassVirtualInheritance, Func0);
	//TEST(testClassVirtualInheritance, testClassVirtualInheritance, VFunc0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, Func0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, VFunc0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, Func0);
	//TEST(testClassVirtualInheritance, testClassDerivedVirtual2, VFunc0);
	TEST(testClassVirtualInheritance, testClass1, Func0);
	TEST(testClassVirtualInheritance, testClass1, VFunc0);
#undef TEST

#define TEST TEST_FUNCTOR
	Displayf("--------------------- FUNCTOR ----------------");
	Displayf("Simple");
	TEST(testClass1, testClass1, Func0);
	TEST(testClass1, testClass1, VFunc0);
	Displayf("Derived");
	TEST(testClassDerived, testClassDerived, Func0);
	TEST(testClassDerived, testClassDerived, VFunc0);
	TEST(testClassDerived, testClass1, Func0);
	TEST(testClassDerived, testClass1, VFunc0);
	Displayf("MI/VI");
	TEST(testClassVirtualInheritance, testClassVirtualInheritance, Func0);
//	TEST(testClassVirtualInheritance, testClassVirtualInheritance, VFunc0); // runtime error
	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, Func0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, VFunc0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, Func0);
//	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, VFunc0); // runtime error
	TEST(testClassVirtualInheritance, testClass1, Func0);
	TEST(testClassVirtualInheritance, testClass1, VFunc0);
#undef TEST



#define TEST TEST_ATFUNCTOR
	Displayf("--------------------- ATFUNCTOR ----------------");
	Displayf("Simple");
	{ 
		testClass1 inst; 
		atFunctor0<void> callback; 
		callback.Reset<testClass1, &testClass1::Func0>(&inst); 
		callback(); 
	}

	TEST(testClass1, testClass1, Func0);
	TEST(testClass1, testClass1, VFunc0);
	Displayf("Derived");
//	TEST(testClassDerived, testClassDerived, Func0);
	TEST(testClassDerived, testClassDerived, VFunc0);
	TEST(testClassDerived, testClass1, Func0);
	TEST(testClassDerived, testClass1, VFunc0);
	Displayf("MI/VI");
//	TEST(testClassVirtualInheritance, testClassVirtualInheritance, Func0);
//	TEST(testClassVirtualInheritance, testClassVirtualInheritance, VFunc0);
//	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, Func0);
//	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, VFunc0);
//	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, Func0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, VFunc0);
	TEST(testClassVirtualInheritance, testClass1, Func0);
	TEST(testClassVirtualInheritance, testClass1, VFunc0);
#undef TEST



#define TEST TEST_DELEGATE
	Displayf("--------------------- DELEGATE ----------------");
	Displayf("Simple");
	TEST(testClass1, testClass1, Func0);
	TEST(testClass1, testClass1, VFunc0);
	Displayf("Derived");
	TEST(testClassDerived, testClassDerived, Func0);
	TEST(testClassDerived, testClassDerived, VFunc0);
	TEST(testClassDerived, testClass1, Func0);
	TEST(testClassDerived, testClass1, VFunc0);
	Displayf("MI/VI");
	TEST(testClassVirtualInheritance, testClassVirtualInheritance, Func0);
	TEST(testClassVirtualInheritance, testClassVirtualInheritance, VFunc0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, Func0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual1, VFunc0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, Func0);
	TEST(testClassVirtualInheritance, testClassDerivedVirtual2, VFunc0);
	TEST(testClassVirtualInheritance, testClass1, Func0);
	TEST(testClassVirtualInheritance, testClass1, VFunc0);
#undef TEST


	return 0;
}
