// 
// /sample_string.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "atl/string.h"
#include "atl/wstring.h"

using namespace rage;

int Main()
{
    // test constructors
    atString c1( "const char* s" );
    Displayf( "Constructed with 'const char* s' constructor:  %s", c1.c_str() );

    atWideString wideString( "const unsigned short *s" );
    atString c2( wideString );  // uses the auto-conversion of atWideString
    Displayf( "Constructed with 'const unsigned short *s' constructor:  %s", c2.c_str() );

    atString that( "const atString &that" );
    atString c3( that );
    Displayf( "Constructed with 'const atString &that' constructor:  %s", c3.c_str() );

    atString c4( that, "const char *s" );
    Displayf( "Constructed with 'const atString &that, const char* s' constructor:  %s", c4.c_str() );

    atString that2( "const atString &that2" );
    atString c5( that, that2 );
    Displayf( "Constructed with 'const atString &that, const atString &that2' constructor:  %s", c5.c_str() );

    // test assignment operators
    c1 = "const char* s";
    Displayf( "Assignment of 'const char* s':  %s", c1.c_str() );

    c2 = that;
    Displayf( "Assignment of 'const atString &that':  %s", c2.c_str() );

    c3 += " const char* s";
    Displayf( "'const atString &that' += ' const char* s' = %s", c3.c_str() );

    c4 += that;
    Displayf( "'const atString &thatconst char *s' += 'const atString &that' = %s", c4.c_str() );

    c5 += '5';
    Displayf( "'const atString &thatconst atString &that2' += '5' = %s", c5.c_str() );
    
    // test other functions
    c1 = "truncate";
    c2 = c1;
    c2.Truncate( 3 );
    Displayf( "Truncate 'truncate' to 3:  %s", c2.c_str() );

    c2.Set( c1, 3 );
    Displayf( "Set to 'truncate' (atString), position 3 to end:  %s", c2.c_str() );

    c3.Set( c1, 4, 3 );
    Displayf( "Set to 'truncate' (atString), position 4, length 3:  %s", c3.c_str() );

    char str[5] = { "have" };
    c4.Set( str, 4, 2 );
    Displayf( "Set to 'have' (const char*) position 2 to end:  %s", c4.c_str() );

    c5.Set( str, 4, 1, 2 );
    Displayf( "Set to 'have' (const char*) position 1, length 2:  %s", c5.c_str() );

    c1 = "UpPeRcAsE";
    c1.Uppercase();
    Displayf( "Make 'UpPeRcAsE' all uppercase:  %s", c1.c_str() );

    c2 = "LoWeRcAsE";
    c2.Lowercase();
    Displayf( "make 'LoWeRcAsE' all lowercase:  %s", c2.c_str() );

    c3 = "EndWith";
    c4 = "With";
    Displayf( "'EndsWith' ends with 'With':  %s", c3.EndsWith( c4 ) ? "true" : "false" );

    Displayf( "'EndsWith' ends with 'with':  %s", c3.EndsWith( "with" ) ? "true" : "false" );

    c4 = "StartsWith";
    c5 = "Starts";
    Displayf( "'StartsWith' starts with 'Starts':  %s", c4.StartsWith( c5 ) ? "true" : "false" );

    Displayf( "'StartsWith' starts with 'starts':  %s", c4.StartsWith( "starts" ) ? "true" : "false" );

    c1 = "IndexOfIndexOf";
    Displayf( "IndexOf 'I' (char) in 'IndexOfIndexOf':  %d", c1.IndexOf( 'I' ) );
    Displayf( "IndexOf 'I' (char) in 'IndexOfIndexOf', starting at position 3:  %d", c1.IndexOf( 'I', 3 ) );

    c2 = "dex";
    Displayf( "IndexOf 'dex' (atString) in 'IndexOfIndexOf':  %d", c1.IndexOf( c2 ) );
    Displayf( "IndexOf 'dex' (atString) in 'IndexOfIndexOf', starting at position 3:  %d", c1.IndexOf( c2, 3 ) );

    Displayf( "IndexOf 'dex' (const char*) in 'IndexOfIndexOf':  %d", c1.IndexOf( c2.c_str() ) );
    Displayf( "IndexOf 'dex' (const char*) in 'IndexOfIndexOf', starting at position 3:  %d", c1.IndexOf( c2.c_str(), 3 ) );

    c3 = "LastIndexOfLastIndexOf";
    Displayf( "LastIndexOf 'I' (char) in 'LastIndexOfLastIndexOf':  %d", c3.LastIndexOf( 'I' ) );

    c4 = "dex";
    Displayf( "LastIndexOf 'dex' (atString) in 'LastIndexOfLastIndexOf':  %d", c3.LastIndexOf( c4 ) );

    Displayf( "LastIndexOf 'dex' (const char*) in 'LastIndexOfLastIndexOf':  %d", c3.LastIndexOf( c4.c_str() ) );

    c5 = "ReplaceReplace";
    c5.Replace( "R", "r" );
    Displayf( "In 'ReplaceReplace', replace 'R' (const char*) with 'r' (const char*):  %s", c5.c_str() );

    c1 = "place";
    c2 = "boot";
    c5.Replace( c1, c2 );
    Displayf( "In 'replacereplace', replace 'place' (atString) with 'boot' (atString):  %s", c5.c_str() );

    c3 = "furbish";
    c5.Replace( c2, c3 );
    Displayf( "In 'rebootreboot', replace 'boot' (atString) with 'furbish' (atString):  %s", c5.c_str() );

    c4 = "key=value";
    c4.Split( c1, c2, '=' );
    Displayf( "Split 'key=value' at '=' (char):  %s = %s", c1.c_str(), c2.c_str() );

    c4 = "=value";
    c4.Split( c1, c2, '=' );
    Displayf( "Split '=value' at '=' (char):  %s = %s", c1.c_str(), c2.c_str() );

    c4 = "keyvalue";
    c4.Split( c1, c2, '=' );
    Displayf( "Split 'keyvalue' at '=' (char):  %s = %s", c1.c_str(), c2.c_str() );

    c4 = "key=";
    c4.Split( c1, c2, '=' );
    Displayf( "Split 'key=' at '=' (char):  %s = %s", c1.c_str(), c2.c_str() );

    atArray<atString> split1;
    c1 = "one";
    c1.Split( split1, '=' );
    Displayf( "Split 'one' at '=' (atArray):" );
    for ( int i = 0; i < split1.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split1[i].c_str() );
    }

    atArray<atString> split2;
    c2 = "one=two=three";
    c2.Split( split2, '=' );
    Displayf( "Split 'one=two=three' at '=' (atArray):" );
    for ( int i = 0; i < split2.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split2[i].c_str() );
    }

    atArray<atString> split3;
    c3 = "=";
    c3.Split( split3, '=' );
    Displayf( "Split '=' at '=' (atArray):" );
    for ( int i = 0; i < split3.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split3[i].c_str() );
    }

    atArray<atString> split4;
    c4 = "=";
    c4.Split( split4, '=', true );
    Displayf( "Split '=' at '=' (atArray), remove empty strings:" );
    for ( int i = 0; i < split4.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split4[i].c_str() );
    }

    atArray<atString> split5;
    c5 = "=two=three";
    c5.Split( split5, '=' );
    Displayf( "Split '=two=three' at '=' (atArray):" );
    for ( int i = 0; i < split5.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split5[i].c_str() );
    }

    atArray<atString> split6;
    atString c6 ( "=two=three" );
    c6.Split( split6, '=', true );
    Displayf( "Split '=two=three' at '=' (atArray), remove empty strings:" );
    for ( int i = 0; i < split6.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split6[i].c_str() );
    }

    atArray<atString> split7;
    atString c7( "one=two=three=" );
    c7.Split( split7, '=' );
    Displayf( "Split 'one=two=three=' at '=' (atArray):" );
    for ( int i = 0; i < split7.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split7[i].c_str() );
    }

    atArray<atString> split8;
    atString c8( "one=two=three=" );
    c8.Split( split8, '=', true );
    Displayf( "Split 'one=two=three=' at '=' (atArray), remove empty strings:" );
    for ( int i = 0; i < split8.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split8[i].c_str() );
    }

    atArray<atString> split9;
    atString c9( "one=two==four" );
    c9.Split( split9, '=' );
    Displayf( "Split 'one=two==four' at '=' (atArray):" );
    for ( int i = 0; i < split9.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split9[i].c_str() );
    }

    atArray<atString> split10;
    atString c10( "one=two==four" );
    c10.Split( split10, '=', true );
    Displayf( "Split 'one=two==four' at '=' (atArray), remove empty strings:" );
    for ( int i = 0; i < split10.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split10[i].c_str() );
    }

    atArray<atString> split11;
    atString c11( "==one=two==four====" );
    c11.Split( split11, "==" );
    Displayf( "Split '==one=two==four====' at '==' (atArray):" );
    for ( int i = 0; i < split11.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split11[i].c_str() );
    }

    atArray<atString> split12;
    atString c12( "==one=two==four====" );
    c12.Split( split12, "==", true );
    Displayf( "Split '==one=two==four====' at '==' (atArray), remove empty strings:" );
    for ( int i = 0; i < split12.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split12[i].c_str() );
    }

    atArray<atString> split13;
    atString c13( "==one===four=five" );
    c13.Split( split13, "=" );
    Displayf( "Split '==one===four=five' at '=' (atArray):" );
    for ( int i = 0; i < split13.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split13[i].c_str() );
    }

    atArray<atString> split14;
    atString c14( "==one===four=five" );
    c14.Split( split14, "=", true );
    Displayf( "Split '==one===four=five' at '=' (atArray), remove empty strings:" );
    for ( int i = 0; i < split14.GetCount(); ++i )
    {
        Displayf( "\t%d) %s", i, split14[i].c_str() );
    }

    c1 = "\r\n  trim\t\t\r";
    c1.Trim();
    Displayf( "Trim '\\r\\n  trim\\t\\t\\r':  %s", c1.c_str() );

    return 0;
}