// 
// /sample_terrain.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE:
//	This sample demonstrates using the pgStreamable class to asynchronously
//	load in a large terrain dataset.
//
//	You need AB assets from rage/assets/sample_paging, and you need to invoke
//	rorc_all.bat in that directory to create all of the resources.  This requires
//	a Win32 Release build of rorc in c:\soft\rage\base\tools\rorc\win32_release.

#include "sample_rmcore/sample_rmcore.h"
#include "paging/streamable.h"
#include "paging/basicscheduler.h"
#include "paging/queuescheduler.h"
#include "rmcore/drawable.h"
#include "system/param.h"
#include "file/asset.h"
#include "system/main.h"

PARAM(usecompressed,"[sample_terrain] If specified, use compressed resources");
PARAM(file,"Not used by this sample");
PARAM(queuescheduler, "[sample_terrain] If specified, use the queue scheduler instead of the basic scheduler");



//
// NOTE about the contents of this file:
//
// The first class is pgSampleTerrain which demonstrates the "classic" use of
// the paging system. It defaults to the basic scheduler and uses "ScheduleAndPlace" to
// load assets.
//
//
// The second class is pgSampleReferenceTerrain, which demonstrates using the queue scheduler
// and the referencing system to load assets.
//




//
// EXAMPLE 1: pgSampleTerrain.
// Using the basic scheduler and ScheduleAndPlace() to load assets.
//

class pgSampleTerrain: public ragesamples::rmcSampleManager
{
protected:
	atArray< pgStreamable<rmcDrawable> > m_Items;

	virtual const char* GetSampleName() const
	{
		return "sample_terrain";
	}

	virtual void InitClient()
	{
		// STEP #1. Initialized the streamer and scheduler.
		// Run the streamer on a different core on Xenon so that decompression doesn't affect responsiveness.
		pgStreamer::InitClass(2);

		// OPTIONAL STEP: Use a different scheduler.
		// By default, the game uses the basic scheduler. However, you can use any other scheduler
		// simply by initializing it.
		if (PARAM_queuescheduler.Get())
		{
			// The game will now use the queue scheduler.
			pgQueueScheduler::InitClass();
		}

		// STEP #2. Open list of files to stream (all are assumed to be in world space)
		fiStream *items = ASSET.Open("items","txt");
		Assert(items);

		// Count the number of items in the file so we can presize the array
		int count = 0;
		char buf[128];
		while (fgetline(buf,sizeof(buf),items))
			++count;
		items->Seek(0);
		m_Items.Reserve(count);

		// Choose the appropriate resource directory.
		ASSET.PushFolder(PARAM_usecompressed.Get()? "resource\\compressed" : "resource\\uncompressed");

		// Reprocess the file and create the streamable items.
		while (fgetline(buf,sizeof(buf),items)) {
			pgStreamable<rmcDrawable> &i = m_Items.Append();
			AssertVerify(i.Init(buf,"#dr",rmcDrawable::RORC_VERSION));
		}
		items->Close();

		// Restore the previous folder.
		ASSET.PopFolder();

		// This demo needs a really far draw distance.
		m_ClipFar = 10000;
	}

	void ShutdownClient()
	{
		m_Items.Reset();

		if (PARAM_queuescheduler.Get())
		{
			pgQueueScheduler::ShutdownClass();
		}

		pgStreamer::ShutdownClass();
	}

	virtual void UpdateClient()
	{
		// STEP #3. Simply scan every item and try to schedule everything.
		// If you use the basic scheduler, you need to do this every frame until everything
		// is loaded. If you are using the queue scheduler, you only need to schedule each
		// file once.
		for (int i=0; i<m_Items.GetCount(); i++)
			m_Items[i].ScheduleAndPlace(PG_SCHEDULER_DEFAULT_PRIORITY);

		// And let the scheduler object do its work now.
		// THIS needs to be done every frame, even with the queue scheduler.
		// If you forget this call, nothing will ever show up.
		pgQueueScheduler::GetScheduler().Update();
	}

	void DrawClient()
	{
		// STEP #4. Draw anything that is available.
		for (int i=0; i<m_Items.GetCount(); i++)
			if (m_Items[i].GetState() == pgStreamableBase::RESIDENT)
				m_Items[i].GetObject()->Draw(M34_IDENTITY,0,0);
	}
};



//
// EXAMPLE 2: pgSampleReferenceTerrain.
// Using the queue scheduler and the referencing system to load assets.
//

class pgSampleReferenceTerrain: public pgSampleTerrain
{
protected:

	// An array of references to pgStreamable<rmcDrawable> objects.
	atArray< pgStreamableRef<rmcDrawable> > m_References;

	virtual void InitClient()
	{
		// First of all, enforce the use of the queue scheduler.
		// Using the referencing system without the queue scheduler is
		// not recommended.
		PARAM_queuescheduler.Set("1");

		// Now initialize the system as demonstrated in pgSampleTerrain.
		pgSampleTerrain::InitClient();

		// We will now create references to every object.
		// By creating a reference, we automatically queue it up for scheduling.
		m_References.Reserve(m_Items.GetCount());

		for (int i=0; i < m_Items.GetCount(); i++)
		{
			pgStreamableRef<rmcDrawable> &reference = m_References.Append();

			// Here, we add a reference to a drawable. This will automatically
			// queue up the asset if it is not already in memory, which is why
			// we specify an (optional) priority here.
			reference.AddRef(m_Items[i], PG_SCHEDULER_DEFAULT_PRIORITY);
		}
	}

	virtual void UpdateClient()
	{
		// Note how we don't have to re-request the assets here because the
		// queue scheduler remembers every single request. All we need to do
		// is to pump the heartbeat of the scheduler itself.

		// Also note that "GetScheduler" gets the currently active scheduler,
		// so don't be surprised about the "pgBasicScheduler" here.
		pgQueueScheduler::GetScheduler().Update();
	}

	virtual void ShutdownClient()
	{
		// Note that by destroying the references, we automatically unrequest
		// the objects that are referenced. If we were to keep the sample
		// running, the items would be removed from memory.
		m_References.Reset();

		pgSampleTerrain::ShutdownClient();
	}
};




// main application
int Main()
{
	pgSampleTerrain sample;			// Simple demonstration using basic scheduler and direct load requests
	//pgSampleReferenceTerrain sample;	// Demonstration using queue scheduler and the referencing system


	sample.Init("sample_paging");

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}

