// 
// sample_grcore/sample_texdict.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE: Demonstrate using a texture dictionary to load and display a texture.

#include "sample_grcore.h"

#include "grcore/im.h"
#include "grcore/texture.h"
#include "paging/dictionary.h"
#include "paging/rscbuilder.h"
#include "system/main.h"
#include "file/asset.h"

using namespace rage;

class grcSampleTexDict : public ragesamples::grcSampleManager
{
	pgDictionary<grcTexture> *m_TexDict;
	grcTexture *m_Tex;

public: 
	void InitClient()
	{
		SetFullAssetPath(RAGE_ASSET_ROOT "sample_grcore/sample_texdict");

		const char *dictname = "skybox";
		const char *texname = "skybox3.dds";

        ASSET.PushFolder( "sample_grcore/sample_texdict" );

		// STEP #1. Load the dictionary off of the storage device
		pgRscBuilder::Load(m_TexDict,dictname,"#td",grcTextureDefault::RORC_VERSION);

		// STEP #2. Make the dictionary current so that future texture requests will query it
		pgDictionary<grcTexture>*prev = pgDictionary<grcTexture>::SetCurrent(m_TexDict);

		// STEP #3. Load the texture normally
		m_Tex = grcTextureFactory::GetInstance().Create(texname);

		// STEP #4. Restore the previously active dictionary.
		pgDictionary<grcTexture>::SetCurrent(prev);

        ASSET.PopFolder();
	}

	void InitLights()
	{
		m_LightMode = false;
		grcSampleManager::InitLights();
	}

	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
	}

	void DrawClient()
	{
		// STEP #5. Set identity matrix in the current viewport.
		grcViewport::SetCurrentWorldIdentity();

		// STEP #6. Set the texture we loaded earlier as active in the device.
		grcBindTexture(m_Tex);

		// STEP #7. Draw a quad with the texture
		grcBegin(drawTriStrip,4);
			grcColor3f(1,1,1);
			grcTexCoord2f(0,0);
			grcVertex3f(-50,0,-50);
			grcTexCoord2f(1,0);
			grcVertex3f(+50,0,-50);
			grcTexCoord2f(0,1);
			grcVertex3f(-50,0,+50);
			grcTexCoord2f(1,1);
			grcVertex3f(+50,0,+50);
		grcEnd();
	}

	void ShutdownClient()
	{
		// STEP #8. Release the resources.  Note that we still need to release the texture
		// even though it came from the dictionary because otherwise all of the reference
		// counts will not be correct and the dictionary won't release properly.
		m_Tex->Release();
		m_TexDict->Release();
	}
};


// main application
int Main()
{
	grcSampleTexDict sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
