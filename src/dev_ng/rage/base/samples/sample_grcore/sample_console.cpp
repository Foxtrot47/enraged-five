//
// sample_grcore\sample_console.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

// TITLE: Console
// PURPOSE:
//		This sample shows how to interact with the console.

#include "sample_grcore.h"

#include "bank/console.h"
#include "system/main.h"

using namespace rage;

class grcSampleConsole : public ragesamples::grcSampleManager
{
public: 
	grcSampleConsole() 
	{
		m_String[0]='\0';

	}

	void InitClient()
	{
		// STEP #1.  Bind a functor with a console command.

		//-- Create a functor and set that functor the our function that 
		// will process the command.
		bkConsole::CommandFunctor functor;
		functor.Reset<grcSampleConsole,&grcSampleConsole::GetString>(this);

		// -- Bind the functor to the command string.  In this example, "GetString"
		// is the name of the console command.  "GetString" accepts and prints to the 
		// screen the first argument passed in.
		bkConsole::AddCommand("GetString",functor);
		// -STOP
	}

	// STEP #2.  Declare and define the function that processes the console command.

	// -- Here's where we declare the function.  We use the convenience macro CONSOLE_ARG_LIST
	// to declare our function's arguments.
	void GetString(CONSOLE_ARG_LIST)
	{
		// -- Use CONSOLE_HELP_TEXT to associate a help string with the command.  Whenever 
		// the console user types "help GetString", this string will be outputed to the console.
		CONSOLE_HELP_TEXT("Gets a string from the command line and prints it to the screen.");

		// -- Next we parse the arguments using the numArgs and args variables declared in the
		// CONSOLE_ARG_LIST macro.
		if (numArgs)
		{
			safecpy(m_String,args[0],256);
		}

		// -- The last step is to send output to the console by calling the output functor (which is declared 
		// in the CONSOLE_ARG_LIST macro.)
		output("got a string");
		// -STOP
	}

	void DrawClient()
	{
		Color32 white(255, 255, 255, 255);
		grcFont::GetCurrent().DrawScaled(200,200,0,white,1.0f,1.0f, m_String);

	}

private:
	char		m_String[256];
};

// main application
int Main()
{
	grcSampleConsole sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
