//
// sample_grcore\sample_multiplewindows.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

// TITLE: Multiple Windows
// PURPOSE:
//		This sample shows how to draw to multiple windows.  This sample only works under Windows.

#include "sample_grcore.h"

#include "grcore/im.h"
#include "system/main.h"
#include "system/param.h"
#include "system/wndproc.h"

#include <stdio.h>

using namespace rage;

#if !__WIN32PC
#pragma message("This sample only compiles and works on Windows")
#else
#include "system/xtl.h"
#endif

PARAM(setOtherHwnds,"[sample_multiplewindows] pass in window handles other than the main window");

class grcSampleMultipleWindows : public ragesamples::grcSampleManager
{
public: 
	grcSampleMultipleWindows() : grcSampleManager()
	{
		int winHandles[NUM_WINDOWS];
		for (int i=0;i<NUM_WINDOWS;i++)
			winHandles[i]=0;


		PARAM_setOtherHwnds.GetArray(winHandles,NUM_WINDOWS-1);
#if __WIN32PC
		for (int i=1;i<NUM_WINDOWS;i++)
		{

			if (winHandles[i-1]!=0)
				m_HwndMains[i]=(HWND)winHandles[i-1];
			else 
				m_HwndMains[i]=0;
		}
#endif
	}

	enum {NUM_WINDOWS=3};
	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
#if __WIN32PC
		// STEP #1. Create our own windows

		//-- Here we create our own windows and viewports for the # of extra windows besides the main window.  The 
		// main window handle is in g_hwndMain.

		m_HwndMains[0] = g_hwndMain;
		m_Viewports[0] = NULL;

		for (int i=1;i<NUM_WINDOWS;i++)
		{
			char windowName[128];
			sprintf(windowName,"View %d",i);
			if (m_HwndMains[i]==0)
				m_HwndMains[i] = ::CreateWindow("STATIC",windowName,WS_VISIBLE,100,300*(i-1),400,300,NULL,NULL,NULL,0);
			m_Viewports[i] = rage_new grcViewport();
		}
		//-STOP
#endif
	}

	void ShutdownCamera()
	{
		grcSampleManager::ShutdownCamera();
#if __WIN32PC
		for (int i=1;i<NUM_WINDOWS;i++)
		{
			delete m_Viewports[i];
			::DestroyWindow(m_HwndMains[i]);
		}
#endif
	}

#if __WIN32PC
	void SetupCamera()
	{
		grcSampleManager::SetupCamera();

		Matrix34 mat=GetCameraMatrix();
		float dist=mat.d.Mag();

		// STEP #2. Create a unique view 

		//-- In each of the separate windows we create a unique view.
		// This code is called for the main window and for every window that we've created.
		// it is called before we draw anything into that window.
		// For our test purposes, we just move each camera view based on 
		// the distance of the main window's camera to the origin.
		// All extra windows just look at the origin.
		int whichView=m_CurrentIteration%3;
		grcViewport* viewport=m_Viewports[m_CurrentIteration];
		switch (whichView)
		{
		case 0:
			return;
			
		case 1:
			mat.d.Set(dist,0.0f,0.0f);
			break;

		case 2:
			mat.d.Set(0.0f,dist,0.0f);
			break;
		}

		mat.LookAt(Vector3(0.0f,0.0f,0.0f), YAXIS);

		viewport->SetCameraMtx(mat);
		viewport->SetWorldMtx(M34_IDENTITY);
		grcViewport::SetCurrent(viewport);
		//-STOP
	}

	virtual bool DoWeKeepDrawing(int drawIteration)
	{

		if (drawIteration>=NUM_WINDOWS)
			return false;

		m_CurrentIteration = drawIteration;

		// STEP #3. Set the window handle for each viewport so that Direct 3D will render into that viewport
		switch (drawIteration)
		{
		case 0:
			{
				//-- Here's how we make RAGE render into the main window that it creates.  We set <c>g_hwndOverride</c> to <c>NULL</c> so that g_hwndMain
				// is used as the window to render to.  This code is called right before a draw iteration.
				g_hwndOverride = NULL;	
				//-STOP
			}
			break;

		default:
			{
				//-- Here's how we make RAGE render into one of the windows created by our sample application.  We set <c>g_hwndOverride</c> to the <c>HWND</c> of one of the windows created
				// in STEP #1.  This code is called right before a draw iteration.  
				g_hwndOverride = m_HwndMains[drawIteration];	
				//-STOP
			}
			break;
		}

		return true;
	}
#endif

	void DrawClient()
	{
		// STEP #4. Draw a couple of objects

		//-- We draw a couple of objects to see the differences in the windows.  This called is called for each window.
		// Ideally your application would have separate update and draw iterations so you can call update once and draw many times for multiple windows.
		grcDrawSolidBox(Vector3(10.0f,10.0f,10.0f),M34_IDENTITY,Color32(Vector3(1.0f,0.0f,0.0f)));
		Matrix34 mat;
		mat.Identity();
		mat.d.x-=10.0f;
		grcColor3f(0.0f,1.0f,0.0f);
		grcDrawSphere(5.0f,mat,8,false,true);
		mat.Identity();
		mat.d.x+=10.0f;
		grcColor3f(0.0f,0.0f,1.0f);
		grcDrawEllipsoid(Vector3(2.0f,10.0f,2.0f),mat,8,false,true);
		//-STOP
	}

private:
#if __WIN32PC
	HWND			m_HwndMains[NUM_WINDOWS];
	int				m_CurrentIteration;
	grcViewport*	m_Viewports[NUM_WINDOWS];
#endif
};

// main application
int Main()
{
	grcSampleMultipleWindows sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
