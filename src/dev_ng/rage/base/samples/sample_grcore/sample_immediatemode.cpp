//
// sample_grcore\sample_immediatemode.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

// TITLE: Immediate Mode
// PURPOSE:
//		This sample shows how to draw immediate mode primitives.

#include "sample_grcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "system/main.h"
#include "system/namedpipe.h"
#include "system/param.h"

using namespace rage;

PARAM(pipeclient,"[sample_immediatemode] specify that this instance of the executable will send IM commands");
PARAM(pipeserver,"[sample_immediatemode] specify that this instance of the executable will receive IM commands");
PARAM(perftest,"[sample_immediatemode] repeat render 1000 times for performance testing");

class grcSampleIM : public ragesamples::grcSampleManager
{
public: 
	grcSampleIM() : m_IsServer(false),m_Pipe(NULL)
	{
#if __BANK
		const char* param;
		if (PARAM_pipeclient.Get(param))
		{
			Printf("Connecting to server...");
			m_IsServer=false;
			m_Pipe=rage_new sysNamedPipe;
			while (m_Pipe->Open(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))!=true);
			Displayf("connected.");
		}
# if __WIN32PC
		else if (PARAM_pipeserver.Get(param))
		{
			m_IsServer=true;
			Printf("Waiting for client to connect...");
			m_Pipe=rage_new sysNamedPipe;
			while (m_Pipe->Create(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))!=true);
			GRCDEVICE.SetBlockOnLostFocus(false);
			Displayf("client connected.");
		}
# endif
#endif
	}

	void InitLights()
	{
		m_LightMode = false;
		grcSampleManager::InitLights();
	}
	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
	}

	void DrawScene()
	{
		// STEP #1. Set our transformation matrix.

		//-- Set the matrix that gets applied to all subsequent geometry.
		grcViewport::SetCurrentWorldIdentity();

		// STEP #2. Setup our graphics state.

		//-- We don't want texturing 
		grcBindTexture(NULL);

		int passCount = PARAM_perftest.Get()? 1000 : 1;
		for (int passes=0; passes<passCount; passes++) {
			// STEP #3. Draw a set of points.

			// -- Here we draw 25 white colored points. 
			// 
			// To start drawing a set of primitives, you must call <c>grcBegin()</c>.  You 
			// must specify the primitive type (in this case, <c>drawPoints</c>), and the vertex
			// count (in this case, 25 points.)  The number of vertices must be at least 1.
			grcBegin(drawPoints,5*5);

			// -- Here's where we send down the verts.
			for (int i=0;i<5;i++)
				for (int j=0;j<5;j++)
					grcVertex(-45.0f+j*2.0f,-5.0f+i*2.0f,0.0f,0,0,1,Color32(255,255,255),0.0f,0.0f);

			// -- Call <c>grcEnd()</c> to signify we're done drawing our primitives.  <c>grcEnd()</c>
			// will assert if the number of verts in the previous call to <c>grcBegin()</c> does not 
			// match the number of verts sent down.
			grcEnd();

			// STEP #4. Draw a triangle.

			// -- Here we draw a single, multi-colored triangle.  The number of verts must be 
			// <c>numTris*3</c>.
			grcBegin(drawTris,3);
			grcVertex(-30.0f,-5.0f,0.0f,0,1,0,Color32(255,0,0),0.0f,0.0f);
			grcVertex(-20.0f,-5.0f,0.0f,1,0,0,Color32(0,0,255),0.0f,0.0f);
			grcVertex(-25.0f,5.0f,0.0f,-1,0,0,Color32(0,255,0),0.0f,0.0f);
			grcEnd();

			// STEP #5.  Draw a set of disconnected lines.

			// -- Here we draw two green colored disconnected lines.  The number of verts must be
			// <c>numLines*2</c>.  Notice that we set the color once using <c>grcColor()</c> and 
			// send down verts using <c>grcVertex3f()</c>.
			grcBegin(drawLines,4);
			grcNormal3f(0,0,1);
			grcColor(Color32(0,255,0));
			grcVertex3f(-15.0f,-5.0f,0.0f);
			grcVertex3f(-10.0f,-5.0f,0.0f);
			grcVertex3f(-10.0f,5.0f,0.0f);
			grcVertex3f(-15.0f,5.0f,0.0f);
			grcEnd();

			// STEP #6.  Draw a set of connected lines.
			
			// -- Here we draw 4 red colored connected lines.  The number of verts must be 
			// <c>numLines+1</c>. 
			grcBegin(drawLineStrip,5);
			grcColor(Color32(255,0,0));
			grcVertex3f(-5.0f,-5.0f,0.0f);
			grcVertex3f(0.0f,-5.0f,0.0f);
			grcVertex3f(0.0f,5.0f,0.0f);
			grcVertex3f(-5.0f,5.0f,0.0f);
			grcVertex3f(-5.0f,-5.0f,0.0f);
			grcEnd();

			// STEP #7. Draw a tri-strip primitive.

			// -- Here we draw two multi-colored triangles.  The number of verts must be 
			// <c>NumTriangles+1</c>.
			grcBegin(drawTriStrip,4);
			grcVertex(10.0f,5.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
			grcVertex(5.0f,-5.0f,0.0f,0,0,-1,Color32(0,0,255),0.0f,0.0f);
			grcVertex(20.0f,5.0f,0.0f,0,0,-1,Color32(255,0,0),0.0f,0.0f);
			grcVertex(15.0f,-5.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
			grcEnd();

			// STEP #8. Draw a tri-fan primitive.

			// -- Here we draw two multi-colored triangles.  The number of verts must be 
			// <c>NumTriangles+1</c>.
			grcBegin(drawTriFan,4);
			grcVertex(35.0f,-5.0f,0.0f,0,0,-1,Color32(0,255,255),0.0f,0.0f);
			grcVertex(45.0f,5.0f,0.0f,0,0,-1,Color32(255,255,0),0.0f,0.0f);
			grcVertex(35.0f,5.0f+2.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
			grcVertex(25.0f,5.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
			grcEnd();
		}

			// -STOP
	}

	void DrawClient()
	{
		DrawScene();
	}

private:
	bool m_IsServer;
	sysNamedPipe* m_Pipe;
};

#include "file/remote.h"

// main application
int Main()
{
	grcSampleIM sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
