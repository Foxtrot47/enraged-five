Project sample_console 
ConfigurationType exe 
RootDirectory . 
Files { 
sample_console.cpp 
} 
Libraries { 
 %RAGE_DIR%\base\samples\vcproj\RageBaseSample\RageBaseSample 
 %RAGE_DIR%\base\samples\sample_rmcore\sample_rmcore
 %RAGE_DIR%\base\samples\sample_grcore\sample_grcore
 %RAGE_DIR%\base\src\vcproj\RageCore\RageCore 
 %RAGE_DIR%\base\src\vcproj\RageGraphics\RageGraphics
 %RAGE_DIR%\base\src\vcproj\RageNet\RageNet 
} 

Folder Resources {
        sample_console.appxmanifest
        ../durango_files/Logo.png
        ../durango_files/SmallLogo.png
        ../durango_files/SplashScreen.png
        ../durango_files/StoreLogo.png
}