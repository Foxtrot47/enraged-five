// 
// sample_grcore/sample_culling.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define AABB_TEST_IMPL

#include "sample_grcore.h"

#include "atl/array.h"
#include "grcore/im.h"
#include "math/random.h"
#include "system/main.h"
#include "system/timer.h"
#include "vector/vector4.h"
#include "grcore/im.h"
#include "grcore/viewport_inline.h"
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"

#if __XENON && 0
#include "system/xtl.h"
#include <TraceRecording.h>
#pragma comment(lib,"TraceRecording.lib")
#endif

using namespace rage;

class grcSampleCulling : public ragesamples::grcSampleManager
{
public:
	atArray<Vec4V> m_Spheres;
	static const int SPHERE_COUNT = __OPTIMIZED? 2048 : 2048;
	grcViewport *m_VP;
	bool m_Ortho, m_DrawAll;

	grcSampleCulling()
	{
		// STEP #1. Create a bunch of randomly-sized spheres.
		mthRandom RAND;
		m_Spheres.Reserve(SPHERE_COUNT);
		for (int i=0; i<SPHERE_COUNT; i++)
		{
			float x = RAND.GetRanged(-1000.0f,1000.0f);
			float y = RAND.GetRanged(-1000.0f,1000.0f);
			float z = RAND.GetRanged(-1000.0f,1000.0f);
			float w = RAND.GetRanged(5.0f,10.0f);
			m_Spheres.Append() = Vec4V(x,y,z,w);
		}

		m_VP = NULL;
		m_Ortho = false;
		m_DrawAll = false;
	}

	~grcSampleCulling()
	{
		delete m_VP;
	}

	void DrawClient();

#if __BANK
	void AddWidgetsClient()
	{
		bkBank &B = BANKMGR.CreateBank("Tester");
		B.AddToggle("Ortho Viewport",&m_Ortho);
		B.AddToggle("Draw Everything",&m_DrawAll);
	}
#endif
};


void grcSampleCulling::DrawClient()
{
	if (!m_VP)
		m_VP = rage_new grcViewport;

	int numDrawn = 0;

	// STEP #2. Draw the spheres with cull checks
	if (m_Ortho)
		m_VP->Ortho(-140,+140,-140,+140,-500,500);
	else
		m_VP->Perspective(60,0,.1f,5000);
	grcViewport *old = grcViewport::SetCurrent(m_VP);

	// Use correct camera matrix on this viewport since the framework
	// doesn't know about our custom viewport.
	m_VP->SetCameraMtx(RCC_MAT34V(GetCameraMatrix()));
	m_VP->SetWorldIdentity();
	grcLightState::SetEnabled(false);
	grcColor3f(1,0,0);

	for (int i=0; i<SPHERE_COUNT; i++)
	{
		Vec4V V = m_Spheres[i];
		if (m_VP->IsSphereVisible(V) || m_DrawAll)
		{
			grcDrawSphere(V.GetWf(),(Vector3&)V);
			++numDrawn;
		}
	}

	// STEP #3. Repeat sphere checks for performance tuning.
	sysTimer T;
	int numChecked = 0;
	// static int foo;
	// if (++foo==10) XTraceStartRecording("e:\\trace.bin");
	for (int i=0; i<SPHERE_COUNT; i++)
	{
		numChecked += m_VP->IsSphereVisibleInline(m_Spheres[i]);
		// numChecked += m_VP->IsSphereVisible(m_Spheres[i].x,m_Spheres[i].y,m_Spheres[i].z,m_Spheres[i].w);
	}
	// if (foo==10) XTraceStopRecording();
	float usTime = T.GetUsTime();

	// STEP #4. Try them with AABB's in world space.
	m_VP->SetWorldIdentity();
	T.Reset();
	int numChecked2 = 0;
	for (int i=0; i<SPHERE_COUNT; i++)
	{
		spdAABB temp;
		temp.SetAsSphereV4(m_Spheres[i]);
		numChecked2 += (m_VP->GetAABBCullStatusInline(temp.GetMin().GetIntrin128(),temp.GetMax().GetIntrin128(),m_VP->GetFrustumLRTB()) != cullOutside);
	}
	float usTime2 = T.GetUsTime();

	// STEP #5. Flag the tests that don't match
	int failures = 0;
	for (int i=0; i<SPHERE_COUNT; i++)
	{
		spdAABB temp;
		temp.SetAsSphereV4(m_Spheres[i]);
		grcCullStatus aabb = m_VP->GetAABBCullStatusInline(temp.GetMin().GetIntrin128(),temp.GetMax().GetIntrin128(),m_VP->GetFrustumLRTB());
		grcCullStatus aabbt = m_VP->GetAABBCullStatusInline_Test(temp.GetMin(),temp.GetMax());
		// grcCullStatus aabbt2 = m_VP->GetAABBCullStatusInline_Test2(temp.GetMin(),temp.GetMax());
		// grcCullStatus sph = m_VP->GetSphereCullStatusInline(m_Spheres[i]);
		// Displayf("....... %d %d",aabbt2,sph);
		if (aabbt != aabb)
		{
			Warningf("Sphere %d Mismatch, aabb status %d doesn't match ref status %d",i,aabb,aabbt);
			m_Spheres[i].Print();
			for (int j=0; j<6; j++)
				Displayf("Distance to plane %d is %f",j,
					planeDistance(m_VP->GetFrustumClipPlane(j),m_Spheres[i].GetXYZ()));
			temp.GetMin().Print();
			temp.GetMax().Print();
			aabb = m_VP->GetAABBCullStatusInline(temp.GetMin().GetIntrin128(),temp.GetMax().GetIntrin128(),m_VP->GetFrustumLRTB());
			aabbt = m_VP->GetAABBCullStatusInline_Test(temp.GetMin(),temp.GetMax());
			++failures;
		}
		int fast = m_VP->IsAABBVisibleInline(temp.GetMin().GetIntrin128(),temp.GetMax().GetIntrin128(),m_VP->GetFrustumLRTB());
		if ((aabb != 0) != fast)
			Warningf("Sphere %d fast check inconsistent!",i);
		/* if (aabb2 != sph)
		{
			Warningf("Mismatch, aabb status %d doesn't match sphere status %d",aabb2,sph);

			temp.GetMin().Print();
			temp.GetMax().Print();
		} */
		/* if (aabb1 != aabb2)
		{
			Warningf("Mismatch, aabb status %d doesn't match ref status %d",aabb1,aabb2);
			temp.GetMin().Print();
			temp.GetMax().Print();
		} */
	}
	if (failures)
		Displayf("**** %d/%d mismatches",failures,SPHERE_COUNT);

	grcViewport::SetCurrent(old);

	char buf[128];
	sprintf(buf,"%d/%d/%d drawn; %6.2fus  %6.2fus",numDrawn,numChecked,numChecked2,usTime,usTime2);
	grcColor3f(1,1,1);
	grcDraw2dText(100,200,buf);
}




// main application
int Main()
{
	grcSampleCulling sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
