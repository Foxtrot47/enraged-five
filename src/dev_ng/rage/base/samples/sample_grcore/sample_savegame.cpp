// 
// sample_grcore/sample_savegame.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_grcore.h"

#include "system/main.h"
#include "file/savegame.h"
#include "grcore/im.h"

#if __XENON
#include <xdk.h>
#endif

#if __XENON
#include "system/xtl.h"
// This is intentionally the same Title ID as the XContent sample.
#include <xtitleidbegin.h>
XEX_TITLE_ID(0xFFFF011D)
#include <xtitleidend.h>

#pragma comment(lib,"xonline.lib")
#endif

using namespace rage;


class grcSampleSavegame : public ragesamples::grcSampleManager
{
	int signInId;
	int saveMode;
	int counter;
	int contentType;
	int state;
	int currentSlot;
	static const int maxContent = 8;
	static const bool allowOverwrite = true;
	fiSaveGame::Content content[maxContent];
	int contentCount;

	void InitClient()
	{
#if __XENON
		// Savegame code shouldn't manage signin itself so do it here in the sample
		XOnlineStartup();
		// XShowSigninUI(1,0);
		contentType = XCONTENTTYPE_SAVEDGAME;
#else
		contentType = 0;
#endif
		currentSlot = 0;

		fiSaveGame::InitClass();
		signInId = 0;
		saveMode = 0;
		counter = 0;		
		state = 0;
	}

	void UpdateClient()
	{
		/// Displayf("state = %d",state);
		if (SAVEGAME.GetState(signInId) == fiSaveGameState::HAD_ERROR) {
			Displayf("Error detected, re-selecting device.");
			state = 0;
		}
		if (state == 0 && SAVEGAME.BeginSelectDevice(signInId,contentType))
			state = 1;
		if (state == 1 && SAVEGAME.CheckSelectDevice(signInId)) {
			Displayf("Device %x selected",SAVEGAME.GetSelectedDevice(signInId));
			SAVEGAME.EndSelectDevice(signInId);
			if (SAVEGAME.BeginEnumeration(signInId,contentType,content,maxContent))
				state = 2;
			else
				Errorf("No profiles on this console?");
		}
		USES_CONVERSION;
		if (state == 2 && SAVEGAME.CheckEnumeration(signInId,contentCount)) {
			Displayf("Enumeration complete, %d found",contentCount);
			SAVEGAME.EndEnumeration(signInId);
			for (int i=0; i<contentCount; i++)
				Displayf("content %d. '%s'",i,content[i].Filename);
			static char saveData[] = "_ - This is dummy savedata.";
			saveData[0] = char((saveMode++)%26)+'A';
			Displayf("Saving [[%s]]",saveData);
			if (SAVEGAME.BeginSave(signInId,contentType,A2W("Rage Sample Content"),"DEFAULT",saveData,sizeof(saveData),allowOverwrite))
				state = 3;
			else
				Errorf("BeginSave failed.");
		}
		bool valid, fileExists;
		static char scratch[32];
		if (state == 3 && SAVEGAME.CheckSave(signInId,valid,fileExists)) {
			Displayf("Save complete - %s, file exists - %s",valid?"SUCCESS!":"failed",fileExists?"YES":"NO");
			SAVEGAME.EndSave(signInId);
			if (valid) {
				if (SAVEGAME.BeginLoad(signInId,contentType,SAVEGAME.GetSelectedDevice(signInId),"DEFAULT",scratch,sizeof(scratch)))
					state = 4;
				else
					Errorf("BeginLoad failed.");
			}
		}
		u32 amtRead;
		bool isValid;
		if (state == 4 && SAVEGAME.CheckLoad(signInId,isValid,amtRead)) {
			if (amtRead && isValid) {
				Displayf("Load complete - (%u bytes read)",amtRead);
				Displayf("Loaded [[%s]]",scratch);
				SAVEGAME.EndLoad(signInId);
				if (SAVEGAME.BeginEnumeration(signInId,contentType,content,maxContent))
					state = 2;
				else
					Errorf("BeginEnumeration failed.");
			}
			else
				Displayf("Load failed!");
		}
	}

	void ShutdownClient()
	{
		fiSaveGame::ShutdownClass();
	}

	void DrawClient()
	{
		++counter;
		char buf[16];
		grcDraw2dText(GRCDEVICE.GetWidth()-200.0f,200.0f,formatf(buf,sizeof(buf),"%08d",counter));
	}
};


// main application
int Main()
{
	grcSampleSavegame sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
