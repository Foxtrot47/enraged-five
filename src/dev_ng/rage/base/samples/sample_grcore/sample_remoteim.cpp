//
// sample_grcore\sample_immediatemode.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

// TITLE: Remote Immediate Mode
// PURPOSE:
//		This sample shows to create a server that will receive immediate mode commands over a named pipe from another application.

#include "sample_grcore.h"

#include "bank/bkmgr.h"
#include "bank/packet.h"
#include "grcore/im.h"
#include "system/ipc.h"
#include "system/main.h"
#include "system/namedpipe.h"
#include "system/param.h"

using namespace rage;



class grcRemoteIM : public ragesamples::grcSampleManager
{
private:
	static DECLARE_THREAD_FUNC(ThreadFunc)
	{
		ptr=ptr;
		Assert(ptr);
#if __WIN32PC && __BANK
		grcRemoteIM& remote=*(static_cast<grcRemoteIM*>(ptr));
		if (remote.m_Pipe->Create(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))==true)
		{
			remote.m_IsServer=true;
			Displayf("client connected.");
		}
#endif
	}

public: 
	grcRemoteIM() : m_IsServer(false),m_Pipe(NULL)
	{

		m_DrawGrid=false;
		m_DrawBackground=false;
#if __WIN32	
#if __WIN32PC
		GRCDEVICE.SetBlockOnLostFocus(false);
#endif
		m_Pipe=rage_new sysNamedPipe;
		Displayf("waiting for connection...");
		m_ThreadId = sysIpcCreateThread(ThreadFunc,this,4096,PRIO_NORMAL,false);
#endif
	}

	void InitLights()
	{
		m_LightMode = grclmNone;
		grcSampleManager::InitLights();
	}

	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
	}
 
	void DrawClient()
	{
#if __DEV && __WIN32PC
		if (m_IsServer)
		{
			m_DrawGrid=true;
			m_DrawBackground=true;

			grcViewport::SetCurrentWorldIdentity();
			grcBindTexture(NULL);

			// receives packets and renders them:
			while (!grcReceivePackets(*m_Pipe))
			{
#if __BANK
				// Make sure bank is up-to-date
				if (bkManager::IsEnabled())
					bkManager::GetInstance().Update();
				if (WantsExit())  
					break;
#endif
			}
		}
		else 
#endif
		{
			grcFont::GetCurrent().Draw((float)(GRCDEVICE.GetWidth()/2)-(strlen("waiting for connection...")/2*grcFont::GetCurrent().GetWidth()),
									   (float)(GRCDEVICE.GetHeight()/2),"waiting for connection...");
#if __WIN32PC
			static bool firstFrame=true;
			if (!firstFrame)
			{	
				sysIpcResumeThread(m_ThreadId);
			}
			firstFrame=false;
#endif
		}
	}

private:
	bool m_IsServer;
	sysNamedPipe* m_Pipe;
	sysIpcThreadId m_ThreadId;
};

// main application
int Main()
{
	grcRemoteIM sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
