// 
// sample_grcore/sample_multithreaded.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// For compatibility with PC / newer targets that require rendering to be on the thread that created
// the device, we no longer support Release/AcquireThreadOwnership as required by this sample, so
// it will likely not work on many platforms.

#include "sample_grcore.h"

#include "system/ipc.h"
#include "system/main.h"
#include "system/param.h"

#include "grcore/im.h"

#if !__IM_BATCHER
#error "This sample is pointless if there's no batcher batcher batcher."
#endif

#define DRAW_THREAD_STACK		(128 * 1024)

using namespace rage;

#if __WIN32PC
namespace rage {
XPARAM(d3dmt);
}
#endif

PRE_DECLARE_THREAD_FUNC(DrawThreadMain);

// This structure is used to contain all of the events needed to manage a thread.
// It also contains functions to perform management of the thread
// This can be done however you like, this method is very clean for demonstration purposes
typedef struct _ThreadParams
{
	sysIpcSema		m_eTrigger;
	sysIpcSema		m_eFinished;
	sysIpcSema		m_eKill;
	sysIpcSema		m_eDead;
	
	_ThreadParams()
	{
		m_eTrigger = sysIpcCreateSema(false);
		m_eFinished = sysIpcCreateSema(true);
		m_eKill = sysIpcCreateSema(false);
		m_eDead = sysIpcCreateSema(false);
	}

	~_ThreadParams()
	{
		sysIpcDeleteSema(m_eTrigger);
		sysIpcDeleteSema(m_eFinished);
		sysIpcDeleteSema(m_eKill);
		sysIpcDeleteSema(m_eDead);
	}

	bool WaitTriggered()
	{
		return sysIpcWaitSema(m_eTrigger);
	}

	bool IsFinished()
	{
		return sysIpcPollSema(m_eFinished);
	}

	bool IsKilled()
	{
		return sysIpcPollSema(m_eKill);
	}

	bool IsDead()
	{
		return sysIpcPollSema(m_eDead);
	}

	void Trigger()
	{
		sysIpcSignalSema(m_eTrigger);
	}

	void SetFinished()
	{
		sysIpcSignalSema(m_eFinished);
	}

	void Finish()
	{
		sysIpcWaitSema(m_eFinished);
	}
	void Kill()
	{
		sysIpcSignalSema(m_eKill);
		sysIpcWaitSema(m_eDead);
	}

	void MarkDead()
	{
		sysIpcSignalSema(m_eDead);
	}
} ThreadParams;

class SampleMT: public ragesamples::grcSampleManager
{
public:
	void InitClient()
	{
		for (int i=0; i<2; i++) {
			m_Batchers[i] = rage_new grcBatcher;
			m_Batchers[i]->Init(100000);
		}
		m_UpdateBatcher = 0;
		m_DrawBatcher = 0;

		// Relinquish control of the graphics device so the draw thread can take over
		// GRCDEVICE.ReleaseThreadOwnership();

		// Create the draw thread
		sysIpcCreateThread(DrawThreadMain, this, DRAW_THREAD_STACK, PRIO_BELOW_NORMAL, "[RAGE] DrawThreadMain", 1);
	}

	void UpdateClient()
	{
		// Do your scene updating here
		static float t = 0.0f;
		grcBatcher::SetCurrent(m_Batchers[m_UpdateBatcher]);
		grcColor3f(1.0f, 1.0f, 1.0f);
		for (int i=0; i<100; i++)
			grcDrawSphere((float)i * 0.33f,ORIGIN + Vector3(sinf(t),0,0));
		grcBatcher::SetCurrent(NULL);
		t += 0.1f;

		// Wait for the draw thread to be done
		m_DrawThreadParams.Finish();

		// Swap the draw lists around
		m_DrawBatcher = m_UpdateBatcher;			// Draw the one we just updated
		m_UpdateBatcher++;							// Increment the update batcher
		if( m_UpdateBatcher >= 2 )
			m_UpdateBatcher = 0;					// Wrap back to the beginning

		// Start the draw thread
		m_DrawThreadParams.Trigger();
	}

	virtual void Draw()
	{
		// Override draw so that the main thread doesnt render anything
	}

	void DrawFrame()
	{
		// This is the draw call executed from the draw thread.  
		// Call the base class draw function which will in turn call our DrawClient function
		ragesamples::grcSampleManager::Draw();
	}

	void DrawClient()
	{
		// This is the actual draw function, do whatever you need to render your frame here
		grcBatcher *b = m_Batchers[m_DrawBatcher];
		b->Render(true);
	}

	// Accessors
	ThreadParams*	GetDrawThreadParams()			{ return &m_DrawThreadParams; }

protected:
	ThreadParams		m_DrawThreadParams;
	grcBatcher*			m_Batchers[2];
	int					m_UpdateBatcher;
	int					m_DrawBatcher;
};

DECLARE_THREAD_FUNC(DrawThreadMain)
{
	// Get the sample object
	SampleMT* pSample = (SampleMT*)ptr;

	// Get the draw thread parameters
	ThreadParams* pParams = pSample->GetDrawThreadParams();

	// Take control of the graphics device
	// GRCDEVICE.AcquireThreadOwnership();

	// Loop until we are told to stop
	while( !pParams->IsKilled() )
	{
		// Check for trigger
		pParams->WaitTriggered();

		// Draw this frame
		pSample->DrawFrame();

		// Mark that we are done
		pParams->SetFinished();

		// Sleep to let other threads on this processor execute
		sysIpcSleep(0);
	}

	// Thread is done, mark it as dead
	pParams->MarkDead();
}

int Main()
{
#if __WIN32PC
	// Force multithreading enabled for this demo.
	PARAM_d3dmt.Set("");
#endif
	SampleMT smt;

	smt.Init();

	smt.UpdateLoop();

	smt.Shutdown();

	return 0;
}
