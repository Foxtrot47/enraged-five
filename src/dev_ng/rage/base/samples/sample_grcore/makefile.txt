Project sample_grcore

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\samples
IncludePath $(RAGE_DIR)\suite\src

Files {
	sample_grcore.cpp
	sample_grcore.h
}
