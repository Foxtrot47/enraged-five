// 
// sample_grcore/sample_texture.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:	Demonstrates basic loading of a few different texture types.

#include "sample_grcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "grcore/image.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

class grcSampleTexture : public ragesamples::grcSampleManager
{
public: 
	void InitClient()
	{
		// STEP #1. Load a luminance texture.
		m_TextureHdr = grcCreateTexture("L8");

		// STEP #2. Load a 3D texture.
#if __WIN32PC
		if (GRCDEVICE.GetDxVersion() >= 1000)
		{
			// DX10: cannot use 3d texture with 2d sampler. Temp fix, use 2d texture.
			m_Texture3d = grcCreateTexture("hdr_texture");
		}
		else
#endif // __WIN32PC
		{
			m_Texture3d = grcCreateTexture("char_china_shorthair");
		}

		// This is unrelated.  Test JPEG compression.
		grcImage *temp = grcImage::LoadDDS("hdr_texture");
		Assert(temp);

		// Create a memory stream
		int bufferSize = 128*1024;
		char *buffer = rage_new char[bufferSize];
		char namebuf[64];
		fiDeviceMemory::MakeMemoryFileName(namebuf,sizeof(namebuf),buffer,bufferSize,false,"jpeg test file");
		fiStream *memStream = fiStream::Create(namebuf);

		// Save to jpeg.  Error recovery within jpeg would require longjmp, which is a bit dodgy.
		// So use a buffer larger than we ever expect to need, and then figure out how much space we used.
		if (!temp->SaveJPEG(memStream))
			Errorf("SaveJPEG failed, try larger buffer or lower quality");
		Displayf("memStream is %d bytes",memStream->Tell());
		memStream->Close();
		delete[] buffer;

		// Free image.
		temp->Release();
	}

	void InitLights()
	{
		m_LightMode = false;
		grcSampleManager::InitLights();
	}

	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
	}

	void DrawScene()
	{
		// STEP #3. Set our transformation matrix.

		//-- Set the matrix that gets applied to all subsequent geometry.
		Matrix34 m;
		m.Identity();
		m.d.z = 2.0f * sinf(TIME.GetElapsedTime());
		grcViewport::SetCurrentWorldMtx(RCC_MAT34V(m));

		using namespace grcStateBlock;
		SetRasterizerState(RS_NoBackfaceCull);

		// STEP #4. Setup our graphics state and draw two quads, once for each texture.
		grcBindTexture(m_Texture3d);

		grcBegin(drawTriStrip,4);
		grcVertex(-10.0f,-10.0f,-50.0f,0,0,-1,Color32(255,255,255),0.0f,0.0f);
		grcVertex(-10.0f,+10.0f,-50.0f,0,0,-1,Color32(255,255,255),0.0f,1.0f);
		grcVertex(+10.0f,-10.0f,-50.0f,0,0,-1,Color32(255,255,255),1.0f,0.0f);
		grcVertex(+10.0f,+10.0f,-50.0f,0,0,-1,Color32(255,255,255),1.0f,1.0f);
		grcEnd();

		grcBindTexture(m_TextureHdr);
		grcBegin(drawTriStrip,4);
		grcVertex(-20.0f,-20.0f,20.0f,0,0,-1,Color32(255,255,255),0.0f,1.0f);
		grcVertex(-20.0f,+20.0f,20.0f,0,0,-1,Color32(255,255,255),0.0f,0.0f);
		grcVertex(+20.0f,-20.0f,20.0f,0,0,-1,Color32(255,255,255),1.0f,1.0f);
		grcVertex(+20.0f,+20.0f,20.0f,0,0,-1,Color32(255,255,255),1.0f,0.0f);
		grcEnd();

		// -STOP

		SetRasterizerState(RS_Default);
	}

	void DrawClient()
	{
		DrawScene();
	}

	void ShutdownClient()
	{
		// STEP #5. Release resources.
		m_Texture3d->Release();
		m_TextureHdr->Release();
	}

private:
	grcTexture *m_TextureHdr, *m_Texture3d;
};


// main application
int Main()
{
	grcSampleTexture sample;
	sample.Init("sample_grcore\\sample_texture");

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
