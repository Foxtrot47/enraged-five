set TESTERS=sample_immediatemode sample_brickout sample_multiplewindows 
set TESTERS=%TESTERS% sample_texdict sample_texture sample_effect
set TESTERS=%TESTERS% sample_savegame sample_multithreaded sample_culling sample_console
set LIBS=%RAGE_SAMPLE_LIBS% %RAGE_CORE_LIBS% %RAGE_GFX_LIBS%
set XPROJ=%RAGE_DIR%\base\src
