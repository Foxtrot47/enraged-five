// 
// /sample_effect.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// Runs an effect through its paces.  Actually intended to be a general shader performance analyzer.

#include "sample_grcore.h"

#include "system/main.h"
#include "system/param.h"

#include "grcore/effect.h"
#include "grcore/im.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexdecl.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"

#include "math/float16.h"

PARAM(file,"[sample_effect] Effect file to load");
PARAM(tech,"[sample_effect] Technique to run (default is 'Draw')");
PARAM(tex1,"[sample_effect] First texture to apply");
PARAM(tex2,"[sample_effect] Second texture to apply");
PARAM(tex3,"[sample_effect] Third texture to apply");
PARAM(tex4,"[sample_effect] Fourth texture to apply");
PARAM(dense,"[sample_effect] Use dense mesh (excercises vertex unit)");
PARAM(small,"[sample_effect] Use small mesh (relaxes frag unit)");
PARAM(nodiffuse,"[sample_effect] Disable diffuse channel");
PARAM(nonormal,"[sample_effect] Disable normal channel");
PARAM(notc,"[sample_effect] Disable tex coord channel");
XPARAM(frametime);

#define TEST_ATTR_PACKING	(0 && __PPU)

using namespace rage;

class grcSampleEffect : public ragesamples::grcSampleManager
{
public:
	grcSampleEffect()
	{
		m_DrawHelp = false;
		m_DrawGrid = false;
		m_DrawWorldAxes = false;
		// m_DrawLogo = false;
		m_DrawBackground = false;

		PARAM_frametime.Set("");

		m_BlitTest = 10;		// enough times that overhead won't matter, can just divide frame time by ten
	}

	struct Vtx 
	{
		Vector3 pos;
		Vector3 nrm;
		Vector2 tc0;
		Vector4 cpv;
	};

	float Bilerp(float x,float y,float f0,float f1,float f2,float f3)
	{
		return Lerp(y, Lerp(x,f0,f1), Lerp(x,f2,f3));
	}

	Vector2 Bilerp(float x,float y,const Vector2 &v0,const Vector2 &v1,const Vector2 &v2,const Vector2&v3)
	{
		return Vector2(Bilerp(x,y,v0.x,v1.x,v2.x,v3.x),Bilerp(x,y,v0.y,v1.y,v2.y,v3.y));
	}

	Vector3 Bilerp(float x,float y,const Vector3 &v0,const Vector3 &v1,const Vector3 &v2,const Vector3&v3)
	{
		return Vector3(Bilerp(x,y,v0.x,v1.x,v2.x,v3.x),Bilerp(x,y,v0.y,v1.y,v2.y,v3.y),Bilerp(x,y,v0.z,v1.z,v2.z,v3.z));
	}

	Vector4 Bilerp(float x,float y,const Vector4 &v0,const Vector4 &v1,const Vector4 &v2,const Vector4&v3)
	{
		return Vector4(Bilerp(x,y,v0.x,v1.x,v2.x,v3.x),Bilerp(x,y,v0.y,v1.y,v2.y,v3.y),Bilerp(x,y,v0.z,v1.z,v2.z,v3.z),Bilerp(x,y,v0.w,v1.w,v2.w,v3.w));
	}

	// Make a grid: v0 --- v1
	//				|       |
	//				|       |
	//				v2 --- v3
	void FillVertexBuffer(const Vtx v[4],int resX,int resY,grcVertexBuffer* vb)
	{
		grcVertexBufferEditor ve(vb);
#if TEST_ATTR_PACKING
		struct PV {
			Float16 x, y, z, s, nx, ny, nz, t;
		} *pv = (PV*) vb->GetLockPtr();
#endif
		int vi = 0;
		for (int y=0; y<resY; y++)
		{
			float lerpY = float(y)/float(resY-1);
			for (int x=0; x<resX; x++, vi++)
			{
				float lerpX = float(x)/float(resX-1);
				Vector3 pos = Bilerp(lerpX,lerpY,v[0].pos,v[1].pos,v[2].pos,v[3].pos);
				Vector3 nrm = Bilerp(lerpX,lerpY,v[0].nrm,v[1].nrm,v[2].nrm,v[3].nrm);
				Vector4 cpv = Bilerp(lerpX,lerpY,v[0].cpv,v[1].cpv,v[2].cpv,v[3].cpv);
				Vector2 tc = Bilerp(lerpX,lerpY,v[0].tc0,v[1].tc0,v[2].tc0,v[3].tc0);
#if TEST_ATTR_PACKING
				pv->x.Set(pos.x);
				pv->y.Set(pos.y);
				pv->z.Set(pos.z);
				pv->s.Set(tc.x);
				pv->nx.Set(nrm.x);
				pv->ny.Set(nrm.y);
				pv->nz.Set(nrm.z);
				pv->t.Set(tc.y);
				pv++;
#else
				ve.SetPosition(vi, pos);
				ve.SetNormal(vi, nrm);
				ve.SetCPV(vi, cpv);
				ve.SetUV(vi, 0, tc, false);
#endif
			}
		}
	}

	void MakeIndexBuffer(int resX,int resY,grcIndexBuffer* &outIb)
	{
		// We attempt to get good vertex cache utilization by doing 4x4 cells.
		Assert((resX%3) == 1 && (resY%3) == 1);

		int idxCount = (resX-1) * (resY-1) * 6;
		outIb = grcIndexBuffer::Create(idxCount);
		u16 *lockPtr = outIb->LockRW();
		ASSERT_ONLY(u16 *save = lockPtr);
		for (int cellY=0; cellY<resY-1; cellY+=3)
		{
			for (int cellX=0; cellX<resX-1; cellX+=3)
			{
				int base = (cellY * resX) + cellX;
				for (int y=0; y<3; y++, base+=resX)
				{
					for (int x=0; x<3; x++, lockPtr+=6)
					{
						Assign(lockPtr[0],base+x+0);
						Assign(lockPtr[1],base+x+1+resX);
						Assign(lockPtr[2],base+x+1);
						Assign(lockPtr[3],base+x);
						Assign(lockPtr[4],base+x+resX);
						Assign(lockPtr[5],base+x+1+resX);
					}
				}
			}
		}
		Assert(save + idxCount == lockPtr);
		outIb->UnlockRW();
	}

	void MakeGrid(float x0,float y0,float x1,float y1,int resX,int resY,grcVertexDeclaration *&outDecl,grcVertexBuffer *&outVb,grcIndexBuffer *&outIb)
	{
		Vtx v[4];
		v[0].pos.Set(x0,y0,0);
		v[0].nrm.Set(-1,-1,+1); v[0].nrm.Normalize();
		v[0].cpv.Set(1,0,0,1);
		v[0].tc0.Set(0,0);

		v[1].pos.Set(x1,y0,0);
		v[1].nrm.Set(1,-1,+1); v[1].nrm.Normalize();
		v[1].cpv.Set(1,1,0,1);
		v[1].tc0.Set(1,0);

		v[2].pos.Set(x0,y1,0);
		v[2].nrm.Set(-1,+1,+1); v[2].nrm.Normalize();
		v[2].cpv.Set(0,1,1,1);
		v[2].tc0.Set(0,1);

		v[3].pos.Set(x1,y1,0);
		v[3].nrm.Set(1,+1,+1); v[3].nrm.Normalize();
		v[3].cpv.Set(1,1,1,1);
		v[3].tc0.Set(1,1);

#if TEST_ATTR_PACKING
		grcFvf fvf;
		fvf.SetPosChannel(true,grcFvf::grcdsFloat4);	// a lie!

		grcVertexElement ve[2];
		int ch = 0;
		ve[ch].type = grcVertexElement::grcvetPosition;
		ve[ch].size = 8;
		ve[ch++].format = grcFvf::grcdsHalf4;

		ve[ch].type = grcVertexElement::grcvetNormal;
		ve[ch].size = 8;
		ve[ch++].format = grcFvf::grcdsHalf4;
#else
		grcFvf fvf;
		fvf.SetPosChannel(true,grcFvf::grcdsFloat3);
		if (!PARAM_nonormal.Get())
		{
#if __WIN32PC
			fvf.SetNormalChannel(true,grcFvf::grcdsFloat3);
#else
			fvf.SetNormalChannel(true,grcFvf::grcdsPackedNormal);
#endif
		}

		if (!PARAM_nodiffuse.Get())
			fvf.SetDiffuseChannel(true,grcFvf::grcdsColor);
		if (!PARAM_notc.Get())
			fvf.SetTextureChannel(0,true,grcFvf::grcdsHalf2);

		grcVertexElement ve[4];
		int ch = 0;
		ve[ch].type = grcVertexElement::grcvetPosition;
		ve[ch].size = 12;
		ve[ch++].format = grcFvf::grcdsFloat3;

		if (!PARAM_nonormal.Get())
		{
#if __WIN32PC
			ve[ch].type = grcVertexElement::grcvetNormal;
			ve[ch].size = 12;
			ve[ch++].format = grcFvf::grcdsFloat3;
#else
			ve[ch].type = grcVertexElement::grcvetNormal;
			ve[ch].size = 4;
			ve[ch++].format = grcFvf::grcdsPackedNormal;
#endif
		}

		if (!PARAM_nodiffuse.Get())
		{
			ve[ch].type = grcVertexElement::grcvetColor;
			ve[ch].size = 4;
			ve[ch++].format = grcFvf::grcdsColor;
		}

		if (!PARAM_notc.Get())
		{
			ve[ch].type = grcVertexElement::grcvetTexture;
			ve[ch].size = 4;
			ve[ch++].format = grcFvf::grcdsHalf2;
		}
#endif
		outVb = grcVertexBuffer::Create(resX*resY,fvf,true,false,NULL);

		outDecl = GRCDEVICE.CreateVertexDeclaration(ve,ch);

		FillVertexBuffer(v,resX,resY,outVb);
		MakeIndexBuffer(resX,resY,outIb);
	}

	void InitClient()
	{
		const char *file = "t:/rage/assets/tune/shaders/lib/rage_diffuse";
		PARAM_file.Get(file);

		const char *tech = "draw";
		PARAM_tech.Get(tech);

		if ((m_Effect = grcEffect::Create(file)) == NULL)
			Quitf("Unable to load effect '%s'",file);

		m_Tech = m_Effect->LookupTechnique(tech, true);
		if (m_Tech == grcetNONE)
			Quitf("Unable to find techinque '%s' in effect '%s'",tech,file);

		int nextTexture = 0;

		const char *tex1 = "t:/rage/assets/rage_male/rage_male001_head.dds";
		const char *tex2 = "t:/rage/assets/rage_male/rage_male001_head.dds";
		const char *tex3 = "t:/rage/assets/rage_male/rage_male001_head.dds";
		const char *tex4 = "t:/rage/assets/rage_male/rage_male001_head.dds";
		PARAM_tex1.Get(tex1);
		PARAM_tex2.Get(tex2);
		PARAM_tex3.Get(tex3);
		PARAM_tex4.Get(tex4);
		m_Textures[0] = grcTextureFactory::GetInstance().Create(tex1);
		m_Textures[1] = grcTextureFactory::GetInstance().Create(tex2);
		m_Textures[2] = grcTextureFactory::GetInstance().Create(tex3);
		m_Textures[3] = grcTextureFactory::GetInstance().Create(tex4);

		// Hook up any textures.
		for (int i=0; i<m_Effect->GetVarCount(); i++)
		{
			grcEffectVar ev = m_Effect->GetVarByIndex(i);
			const char *name;
			grcEffect::VarType type;
			int annoCount;
			bool isGlobal;
			m_Effect->GetVarDesc(ev,name,type,annoCount,isGlobal);
			if (type == grcEffect::VT_TEXTURE)
			{
				Assert(nextTexture < 4);
				Displayf("Texture '%s' = '%s'",name,m_Textures[nextTexture]->GetName());
				m_Effect->SetVar(ev,m_Textures[nextTexture++]);
			}
		}
		for (int i=0; i<grcEffect::GetGlobalVarCount(); i++)
		{
			const char *name;
			grcEffect::VarType type;
			grcEffect::GetGlobalVarDesc(i,name,type);
			if (type == grcEffect::VT_TEXTURE)
			{
				Assert(nextTexture < 4);
				Displayf("Global Texture '%s' = '%s'",name,m_Textures[nextTexture]->GetName());
				// Interface here is a little weird but it gets the job done.
				grcEffect::SetGlobalVar(grcEffect::LookupGlobalVar(name,true),m_Textures[nextTexture++]);
			}
		}

		int resX = 16, resY = 9;
		float x0 = 0, y0 = 0;
		float x1 = (float)GRCDEVICE.GetWidth(), y1 = (float)GRCDEVICE.GetHeight();
		if (PARAM_dense.Get())
		{
			resX = 320;
			resY = 180;
		}
		if (PARAM_small.Get())
		{
			x0 = x1/2;
			y0 = y1/2;
			x1 = x0 + x1 / 100;
			y1 = y0 + y1 / 100;
		}
		resX = (resX/3) * 3 + 1;
		resY = (resY/3) * 3 + 1;
		MakeGrid(x0,y0,x1,y1,resX,resY,m_VtxDecl,m_VB,m_IB);
	}

	void DrawClient()
	{
		using namespace grcStateBlock;
		PUSH_DEFAULT_SCREEN();
		SetDepthStencilState(DSS_IgnoreDepth);
		m_Effect->Bind(m_Tech);
		for (int passes=0; passes<m_BlitTest; passes++) 
		{
			GRCDEVICE.DrawIndexedPrimitive(drawTris,m_VtxDecl,*m_VB,*m_IB,m_IB->GetIndexCount());
		}
		m_Effect->UnBind();

		SetDepthStencilState(DSS_Default);
		POP_DEFAULT_SCREEN();

		grcDraw2dText(100,500,m_Effect->GetTechniqueName(m_Tech));
	}

	void ShutdownClient()
	{
		GRCDEVICE.DestroyVertexDeclaration(m_VtxDecl);
		delete m_IB;
		delete m_VB;
		m_Textures[3]->Release();
		m_Textures[2]->Release();
		m_Textures[1]->Release();
		m_Textures[0]->Release();
		m_Effect->Shutdown();
	}

#if __BANK
	void AddWidgetsClient()
	{
		bkBank &bank = BANKMGR.CreateBank("Effect");
		bank.AddSlider("Blit Count",&m_BlitTest,0,10,1);
		bank.AddSlider("Technique",(int*)&m_Tech,1,m_Effect->GetTechniqueCount(),1);
		m_Effect->AddWidgets(bank);
	}
#endif


	grcEffect *m_Effect;
	grcEffectTechnique m_Tech;
	grcTexture *m_Textures[4];
	int m_BlitTest;

	grcVertexBuffer *m_VB;
	grcIndexBuffer *m_IB;
	grcVertexDeclaration *m_VtxDecl;
};


// main application
int Main()
{
	grcSampleEffect sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
