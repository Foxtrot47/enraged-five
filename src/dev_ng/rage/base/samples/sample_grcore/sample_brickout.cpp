//
// sample_grcore/sample_brickout.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Brickout
// PURPOSE:
//		This sample is just for fun. Is a little brickout game.

#include "sample_grcore.h"

#include "grcore/im.h"
#include "grcore/quads.h"
#include "input/pad.h"
#include "system/main.h"

namespace rage
{
class grcSampleBrickout : public ragesamples::grcSampleManager
{
public: 
	grcSampleBrickout();

	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(35.113f, 10.368f, 69.9586f), Vector3(0.f, 0.f, 0.f));
	}
	
	void Init(const char* path=NULL);

	void DrawClient();

private:
	void DrawDigits(int x,int y,Color32 c,int n);
	void DrawDigit(int x,int y,Color32 c,int n);
	void DrawBox(int x,int y,int w,int h,Color32 c);
	void DrawCellBox(int x,int y,int w,int h,Color32 c);
	void DrawDashedPaddle(int x, int y, int w, int h, Color32 c, int steps);
	int  PsuedoRand();
	void NewBall();
	void NewScreen();
	bool CollCheck(int x,int y);
	void StartMinigame();

private:
	static const int ROW_COUNT = 8;
	static const int PADDLE_WIDTH = 14;
	static const int PADDLE_Y = 100;
	static const int LEFT_WALL = 8;
	static const int RIGHT_WALL = 152;
	static const int TOP_WALL = 20;
	static const int BOTTOM_WALL = 119;
	static const int PADDLE2_Y = 100;

	int m_Rows[ROW_COUNT];
	int m_PaddleX;
	int m_Paddle2X;

	int m_BallX;
	int m_BallY;
	int m_BallDx;
	int m_BallDy;

	bool m_Frozen;
	bool m_Frozen2;
	
	bool m_GameOver;
	int m_Balls;
	int m_Balls2;
	int m_Counter;
	int m_Angle;
	int m_RandCount;
	bool m_TwoPlayerGame;
	int m_LastB;
	int m_Score;
	int m_Score2;
	int m_HiScore;
	int m_CollisionPitch;

	static int sm_Pitches[4][ROW_COUNT];
};

} // namespace Rage

using namespace rage;

int grcSampleBrickout::sm_Pitches[4][ROW_COUNT] = {
	{0, 300, 500, 600, 700, 1000, 1200, 1500 },//blues
	{0, 200, 400, 700, 900, 1200, 1400, 1600 },//pentatonic
	{0, 200, 400, 600, 800, 1000, 1200, 1400 },//whole tone
	{0, 200, 300, 500, 700, 800, 1100,  1200 }//harmonic minor
};

grcSampleBrickout::grcSampleBrickout() :
	m_PaddleX(70),
	m_Paddle2X(70),
	m_BallX(0),
	m_BallY(0),
	m_BallDx(0),
	m_BallDy(0),
	m_Frozen(false),
	m_Frozen2(false),
	m_GameOver(false),
	m_Balls(0),
	m_Balls2(0),
	m_Counter(0),
	m_Angle(0),
	m_RandCount(0),
	m_TwoPlayerGame(false),
	m_LastB(1),
	m_Score(0),
	m_Score2(0),
	m_HiScore(0),
	m_CollisionPitch(0)
{
	EnableGridDraw(false);
//	EnableLogoDraw(false);
	EnableCameraDraw(false);
}

// main application
int Main()
{
	grcSampleBrickout sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}

void grcSampleBrickout::Init(const char* path) 
{
	StartMinigame();
	grcSampleManager::Init(path);
}

void grcSampleBrickout::DrawClient()
{
	grcViewport::SetCurrentWorldIdentity();
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);

	DrawCellBox(0,4,20,1,Color32(120,120,120));
	DrawCellBox(0,4,1,25,Color32(120,120,120));
	DrawCellBox(19,4,1,25,Color32(120,120,120));

	for (int i=0; i<ROW_COUNT; i++) {
		static Color32 colors[ROW_COUNT] = { Color32(200,0,0), Color32(0,200,0), Color32(0,0,200), Color32(200,0,200),
			Color32(200,100,200), Color32(100,100,200), Color32(100,0,50), Color32(0,100,200) };
		int row = m_Rows[i];
		for (int j=0; j<18; j++) {
			if (row & (1 << j))
				DrawCellBox(j+1,i+7,1,1,colors[i]);
		}
	}

	if (ioPad::MAX_PADS>1) // PSP only has 1 gamepad!
	{
		if( !m_TwoPlayerGame && ioPad::GetPad(1).GetButtons() & ioPad::START ) {
			m_TwoPlayerGame = true;
			m_Score2 = m_Score;
			m_Balls2 = m_Balls;
		}
	}

	float paddle = ioPad::GetPad(0).GetNormLeftX();
	if( ioPad::GetPad(0).GetButtons() & ioPad::LLEFT ) paddle = -1.0f;
	if( ioPad::GetPad(0).GetButtons() & ioPad::LRIGHT ) paddle = 1.0f;
	m_PaddleX += int(paddle * 4);
	if (m_PaddleX < LEFT_WALL) m_PaddleX = LEFT_WALL;
	else if (m_PaddleX > RIGHT_WALL-PADDLE_WIDTH) m_PaddleX = RIGHT_WALL-PADDLE_WIDTH;
	if(!m_TwoPlayerGame)
		DrawBox(m_PaddleX,PADDLE_Y,PADDLE_WIDTH,2,Color32(100,100,100));

	if(m_TwoPlayerGame) {
		float paddle2 = ioPad::GetPad(1).GetNormLeftX();
		if( ioPad::GetPad(1).GetButtons() & ioPad::LLEFT ) paddle2 = -1.0f;
		if( ioPad::GetPad(1).GetButtons() & ioPad::LRIGHT ) paddle2 = 1.0f;
		m_Paddle2X += int(paddle2 * 4);
		if (m_Paddle2X < LEFT_WALL) m_Paddle2X = LEFT_WALL;
		else if (m_Paddle2X > RIGHT_WALL-PADDLE_WIDTH) m_Paddle2X = RIGHT_WALL-PADDLE_WIDTH;
		if(m_LastB==1)
		{
			DrawDashedPaddle(m_Paddle2X,PADDLE2_Y,PADDLE_WIDTH,2,Color32(50,50,200),PADDLE_WIDTH/2);
			DrawBox(m_PaddleX,PADDLE_Y,PADDLE_WIDTH,2,Color32(200,50,50));
		}
		else
		{
			DrawDashedPaddle(m_PaddleX,PADDLE_Y,PADDLE_WIDTH,2,Color32(200,50,50),PADDLE_WIDTH/2);
			DrawBox(m_Paddle2X,PADDLE2_Y,PADDLE_WIDTH,2,Color32(50,50,200));
		}
	}

	if (m_Frozen) {
		m_BallX = m_PaddleX + PADDLE_WIDTH/2;
		if (ioPad::GetPad(0).GetPressedButtons() & ioPad::RDOWN) {
			if(m_TwoPlayerGame) m_LastB = 0;
			m_Frozen = false;
		}
	}
	else if(m_TwoPlayerGame && m_Frozen2) {
		m_BallX = m_Paddle2X + PADDLE_WIDTH/2;
		if (ioPad::GetPad(1).GetPressedButtons() & ioPad::RDOWN) {
			m_LastB = 1;
			m_Frozen2 = false;
		}
	}
	else if (m_GameOver) {
		if ((ioPad::GetPad(0).GetPressedButtons() & ioPad::RDOWN) ||
			(m_TwoPlayerGame && (ioPad::GetPad(1).GetPressedButtons() & ioPad::RDOWN))) {
				StartMinigame();
			}
	}
	else {
//		bool coll = false;
		if (CollCheck(m_BallX+m_BallDx,m_BallY))
		{
//			coll = true;
			m_BallDx = -m_BallDx;
		}
		if (CollCheck(m_BallX,m_BallY+m_BallDy) && m_BallY < 80)
		{
//			coll = true;
			m_BallDy = -m_BallDy;
		}

		//		if (coll && audio_up)
		//			SAGAUDIOMGR->PlayAuxSound("SAG_Hud:UI_SELECT", 0.4f, NULL, m_CollisionPitch);

		// Special case: paddle
		int px, py;
		if(!m_TwoPlayerGame || m_LastB == 1) {
			px = m_PaddleX;
			py = PADDLE_Y;
		}
		else
		{
			px = m_Paddle2X;
			py = PADDLE2_Y;
		}

		if (m_BallDy > 0 && m_BallX >= px && m_BallX < px + PADDLE_WIDTH && m_BallY >= py-1 && m_BallY <= py + 2) {
			int twicedelta = m_BallX - (px + PADDLE_WIDTH/2);
			int delta = ((twicedelta == -1 || twicedelta == 0) ? -1 : (twicedelta == 1 ? 1 : twicedelta / 2));
			if (delta < -2)
				m_BallDx = -2;
			else if (delta > 2)
				m_BallDx = 2;
			else
				m_BallDx = delta;

			//paddle hit sound
			//			if (audio_up)
			//				SAGAUDIOMGR->PlayAuxSound("SAG_Hud:UI_SELECT", 0.4f, NULL, sm_Pitches[g_lv%3][ROW_COUNT-1] - 1200);

			m_BallDy = -m_BallDy;
			if(m_TwoPlayerGame)
				m_LastB = (m_LastB == 0 ? 1 : 0);
			int i=0;
			for (i=0; i<ROW_COUNT; i++)
				if (m_Rows[i])
					break;
			if (i == ROW_COUNT)
				NewScreen();

			if(!m_TwoPlayerGame && m_BallDx == m_Angle) {
				m_Counter++;
				if(m_Counter>2) {
					int new_g1_bdx=1;
					switch(m_Angle) {
						case -2: new_g1_bdx = m_BallDx + (PsuedoRand()%2); break;
						case -1: 
						case 0:
						case 1:	new_g1_bdx = m_BallDx + (PsuedoRand()%3) - 1; break;
						case 2: new_g1_bdx = m_BallDx - (PsuedoRand()%2); break;
					}
					m_BallDx = (new_g1_bdx ? new_g1_bdx : -m_BallDx);
				}
			}
			else { 
				m_Angle = m_BallDx; 
				m_Counter = 0; 
			}
		}

		if (!m_Frozen && !m_GameOver) {
			m_BallX += m_BallDx;
			m_BallY += m_BallDy;
		}
	}

	DrawBox(m_BallX-1,m_BallY-1,3,3,Color32(120,120,125));
	DrawBox(m_BallX,m_BallY,1,1,Color32(240,240,250));

	if(!m_TwoPlayerGame)
	{
		DrawDigits(100,10,Color32(80,200,10),m_Score);
		DrawDigits(50,10,Color32(180,50,10),m_HiScore);
		DrawDigits(20,10,Color32(10,100,50),m_Balls);
	}
	else
	{
		DrawDigits(110,10,Color32(180,50,10),m_Score);
		DrawDigits(130,10,Color32(10,50,180),m_Score2);
		DrawDigits(76,10,Color32(80,200,10),m_HiScore);
		DrawDigits(10,10,Color32(100,50,10),m_Balls);
		DrawDigits(30,10,Color32(10,50,100),m_Balls2);
	}

	//	if (audio_up)
	//		sagAudioManager::UpdateInstance();
}

void grcSampleBrickout::DrawBox(int x,int y,int w,int h,Color32 c) {
	// map to virtual 160x120 screen (round up)
	float x1 = (x * GRCDEVICE.GetWidth() + 159) / 160.0f;
	float w1 = (w * GRCDEVICE.GetWidth() + 159) / 160.0f;
	float y1 = (y * GRCDEVICE.GetHeight() + 119) / 120.0f;
	float h1 = (h * GRCDEVICE.GetHeight() + 119) / 120.0f;
	grcBeginQuads(1);
	grcDrawQuadf(x1, y1, x1+w1, y1+h1, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,c);
	grcEndQuads();
}


void grcSampleBrickout::DrawCellBox(int x,int y,int w,int h,Color32 c) {
	// cells are 20x30 stretched to 160x120 virtual resolution
	DrawBox(x*8,y*4,w*8,h*4,c);
}

void grcSampleBrickout::DrawDashedPaddle(int x, int y, int w, int h, Color32 c, int steps) {
	int delta = w / steps;
	for( int i = 0; i < steps; i += 2 )
		DrawBox(x + i*delta, y, delta, h, c);
}

void grcSampleBrickout::DrawDigit(int x,int y,Color32 c,int n) {
	switch (n) {
		case 8:
			DrawBox(x+1,y+2,1,1,c);
			// fall through to 0
		case 0: 
			DrawBox(x,y,3,1,c);
			DrawBox(x,y+1,1,3,c);
			DrawBox(x+2,y+1,1,3,c);
			DrawBox(x,y+4,3,1,c);
			break;
		case 1:
			DrawBox(x+1,y,1,5,c);
			break;
		case 2:
			DrawBox(x,y,3,1,c);
			DrawBox(x,y+2,3,1,c);
			DrawBox(x,y+4,3,1,c);
			DrawBox(x+2,y+1,1,1,c);
			DrawBox(x,y+3,1,1,c);
			break;
		case 3:
			DrawBox(x,y,3,1,c);
			DrawBox(x,y+2,3,1,c);
			DrawBox(x,y+4,3,1,c);
			DrawBox(x+2,y,1,5,c);
			break;
		case 4:
			DrawBox(x,y,1,3,c);
			DrawBox(x+2,y,1,5,c);
			DrawBox(x+1,y+2,1,1,c);
			break;
		case 5:
			DrawBox(x,y,3,1,c);
			DrawBox(x,y+2,3,1,c);
			DrawBox(x,y+4,3,1,c);
			DrawBox(x,y+1,1,1,c);
			DrawBox(x+2,y+3,1,1,c);
			break;
		case 6:
			DrawBox(x,y,3,1,c);
			DrawBox(x,y+2,3,1,c);
			DrawBox(x,y+4,3,1,c);
			DrawBox(x,y,1,5,c);
			DrawBox(x+2,y+3,1,1,c);
			break;
		case 7:
			DrawBox(x,y,3,1,c);
			DrawBox(x+2,y+1,1,4,c);
			break;
		case 9:
			DrawBox(x,y,3,1,c);
			DrawBox(x,y+2,3,1,c);
			DrawBox(x,y+4,3,1,c);
			DrawBox(x+2,y,1,5,c);
			DrawBox(x,y+1,1,1,c);
			break;
	}
}

void grcSampleBrickout::DrawDigits(int x,int y,Color32 c,int n) {
	DrawDigit(x+12,y,c,n % 10); n /= 10;
	DrawDigit(x+8,y,c,n % 10); n /= 10;
	DrawDigit(x+4,y,c,n % 10); n /= 10;
	DrawDigit(x,y,c,n % 10); n /= 10;
}

int grcSampleBrickout::PsuedoRand()
{
	int shift[8] = { 3,7,5,8,3,9,1,3 }; // from a real random generator, honest
	int count = m_RandCount++;
	for (int i = 0; i<ROW_COUNT; i++)
		count += (m_Rows[i] >> shift[i]);
	return count;
}

void grcSampleBrickout::NewBall() {
	if(m_LastB==1) {
		m_Frozen = true;
		m_Frozen2 = false;
	}
	else {
		m_Frozen = false;
		m_Frozen2 = true;
	}
	m_BallX = 70;
	m_BallY = PADDLE_Y - 2;
	m_BallDx = 1;
	m_BallDy = -2;
	m_Counter=0;
	m_Angle=0;
}

static int g_lv=0;

void  grcSampleBrickout::NewScreen() {
	switch(g_lv%6) {
		//		case 0: 
		//			for (int i=0; i<ROW_COUNT; i++)
		//					m_Rows[i] = 0x3FFFF;
		//			break;
		case 0: 
			for (int i=0; i<ROW_COUNT; i+=2) {
				m_Rows[i]   = 0x2aaaa;
				m_Rows[i+1] = 0x15555;
			}
			break;
		case 1: 
			for (int i=0; i<ROW_COUNT; i++) {
				m_Rows[i] = 0x2aaaa;
			}
			break;
		case 2: 
			for (int i=0; i<ROW_COUNT; i++) {
				if(i==2||i==5) continue;
				m_Rows[i] = 0x2aaaa;
			}
			break;
		case 3: 
			for (int i=0; i<ROW_COUNT; i+=4) {
				m_Rows[i]   = 0x33333;
				m_Rows[i+1] = 0x33333;
				m_Rows[i+2] = 0xcccc;
				m_Rows[i+3] = 0xcccc;
			}
			break;
		case 4: 
			m_Rows[0] = 0xffff;
			m_Rows[1] = 0x0000;
			m_Rows[2] = 0x0000;
			m_Rows[3] = 0x3fffc;
			m_Rows[4] = 0x0000;
			m_Rows[5] = 0x0000;
			m_Rows[6] = 0xffff;
			m_Rows[7] = 0xffff;
			break;
		case 5:	
			m_Rows[0] = 0x0035;
			m_Rows[1] = 0x0015;
			m_Rows[2] = 0x8017;
			m_Rows[3] = 0x8003;
			m_Rows[4] = 0xeee1;
			m_Rows[5] = 0xaaad;
			m_Rows[6] = 0xaaa9;
			m_Rows[7] = 0xeeef;
			break;
	}
	++g_lv;
	++m_Balls;
	++m_Balls2;
}

void grcSampleBrickout::StartMinigame() {
	//	for (int i=0; i<ROW_COUNT; i++)
	//		m_Rows[i] = 0x3FFFF;
	NewScreen();
	m_Score = 0;
	m_Score2 = 0;
	m_GameOver = false;
	m_Balls = 3;
	m_Balls2 = 3;
	m_RandCount = 0;
	NewBall();
}

bool grcSampleBrickout::CollCheck(int x,int y) {
	// Blocks take precedence over walls so that they will self-clear
	int cx = (x / 8) - 1;
	int cy = (y / 4) - 7;
	if (cx >= 0 && cx < 18 && cy >= 0 && cy < ROW_COUNT) {
		if (m_Rows[cy] & (1 << cx)) {
			m_Rows[cy] &= ~(1 << cx);
			if(!m_TwoPlayerGame || m_LastB==0) {
				m_Score += ROW_COUNT-cy;
				if (m_Score > m_HiScore)
					m_HiScore = m_Score;
			}
			else {
				m_Score2 += ROW_COUNT-cy;
				if (m_Score2 > m_HiScore)
					m_HiScore = m_Score2;
			}
			m_CollisionPitch = sm_Pitches[g_lv%4][ROW_COUNT - cy];
			return true;
		}
	}

	// Easy cases: the four walls
	if (x <= LEFT_WALL || x >= RIGHT_WALL || y <= TOP_WALL || y >= BOTTOM_WALL) {
		if (y >= BOTTOM_WALL) {

			//			if (audio_up)
			//				SAGAUDIOMGR->PlayAuxSound("SAG_Hud:YOU_LOSE", 0.4f, NULL);

			if(!m_TwoPlayerGame || m_LastB==1) {
				if (--m_Balls) {
					NewBall();
					m_BallX = m_PaddleX + PADDLE_WIDTH/2;
				}
				else
				{
					m_GameOver = true;
					m_CollisionPitch = 0;
				}
			} 
			else {
				if (--m_Balls2) {
					NewBall();
					m_BallX = m_Paddle2X + PADDLE_WIDTH/2;
				}
				else
				{
					m_GameOver = true;
					m_CollisionPitch = 0;
				}
			}
			return false;
		}
		else if (y <= TOP_WALL && m_BallDy == -2)
			m_BallDy = -2;
		return true;
	}
	else
		return false;
}
