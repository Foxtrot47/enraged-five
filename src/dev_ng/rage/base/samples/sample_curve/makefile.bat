set ARCHIVE=
set FILES=
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS%
set LIBS=%LIBS% devcam curve gizmo parsercore parser
set LIBS=%LIBS% sample_file sample_grcore init phcore grcustom

set TP_BASE=%RAGE_DIR%\suite\3rdparty
set WML_BASE=%TP_BASE%\MagicSoftware\WildMagic2

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples
set XPROJ=%XPROJ% %WML_BASE%\Include %WML_BASE%\Source %TP_BASE%

set TESTERS=sample_curve
