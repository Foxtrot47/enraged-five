#include "sample_grcore/sample_grcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "curve/curve.h"
#include "curve/curvecubic.h"
#include "curve/curvecatrom.h"
#include "gizmo/manager.h"
#include "gizmo/translation.h"
#include "parser/manager.h"
#include "grprofile/chart.h"
#include "grprofile/drawmanager.h"
#include "profile/profiler.h"
#include "system/main.h"
#include "vector/colors.h"
#include "vector/vectorn.h"

namespace ragesamples {

using namespace rage;


template<class _Vector> void GetVector3AsVectorGeneric(_Vector & vOut, const Vector3 & v)
{
	vOut.Zero();
	int vOutSize = sizeof(vOut)/sizeof(float);
	for(int i=0; i<vOutSize; i++)
	{
		vOut[i] = v[i];
	}
}


//=============================================================================
// testControlPointSet

class testControlPointSet : public datBase
{
public:
	testControlPointSet(gzManager & gizmoManager)
		: m_NumPoints(0)
		, m_NumPointsOld(0)
		, m_IsMonotonicInX(false)
		, m_GizmoManager(gizmoManager)
	{
	}

	~testControlPointSet()
	{
	}

	void AllocatePoints(int maxPoints)
	{
		Assert(m_NumPoints==0 && m_NumPointsOld==0);
		Assert(m_Points.GetCapacity()==0);

		m_Points.Reserve(maxPoints);
		m_Points.Resize(maxPoints);

		m_Gizmos.Reserve(maxPoints);
		m_Gizmos.Resize(maxPoints);

		for(int i=0; i<maxPoints; i++)
		{
			m_Points[i].x = (2.0f * i) / maxPoints - 1.0f;
			m_Points[i].y = 0.0f;
			m_Points[i].z = 0.0f;
			m_Gizmos[i] = rage_new gzTranslation(m_Points[i]);
			m_Gizmos[i]->SetManager(&m_GizmoManager);
			m_Gizmos[i]->SetSize(0.1f);
			m_Gizmos[i]->SetAxisGrabWidth(0.02f);
		}

		m_Points.Resize(0);
		m_Gizmos.Resize(0);
	}

#if __BANK
	void AddWidgets(bkBank & bank)
	{
		Assert(m_Points.GetCapacity()>0);
		bank.AddSlider("Number of Points",&m_NumPoints,0,m_Points.GetCapacity(),1,datCallback(MFA(testControlPointSet::UpdateNumPoints),this));
	}
#endif

	int GetNumPoints() const
	{
		Assert(m_Points.GetCount()==m_Gizmos.GetCount());
		return m_Points.GetCount();
	}

	template<class _Vector> void GetPoint(_Vector & vOut, int i) const
	{
		GetVector3AsVectorGeneric(vOut,m_Points[i]);
	}

	void UpdateNumPoints()
	{
		m_Points.Resize(m_NumPoints);

		for(int i=m_NumPoints; i<m_NumPointsOld; i++)
		{
			m_Gizmos[i]->SetManager(NULL);
		}
		m_Gizmos.Resize(m_NumPoints);
		for(int i=m_NumPointsOld; i<m_NumPoints; i++)
		{
			Assert(m_Gizmos[i]);
			m_Gizmos[i]->SetManager(&m_GizmoManager);
		}

		m_NumPointsOld = m_NumPoints;
	}

	void Update(const grcViewport & viewport)
	{
		Assert(m_Points.GetCount()==m_Gizmos.GetCount());
		Assert(m_Points.GetCapacity()==m_Gizmos.GetCapacity());

		// Place gizmos in the correct manager.
		// Requires some array schenanigans until count changing is 
		// wrapped in a function.
		int count = m_Gizmos.GetCount();
		m_Gizmos.Resize(m_Gizmos.GetCapacity());
		for(int i=0; i<m_Gizmos.GetCount(); i++)
		{
			m_Gizmos[i]->SetManager(NULL);
			if(i<count)
			{
				m_Gizmos[i]->SetManager(&m_GizmoManager);
			}
		}
		m_Gizmos.Resize(count);

		// Update our gizmo manager.
		grcViewport * pushedViewport = viewport.GetCurrent();
		Assert(pushedViewport);
		grcViewport::SetCurrent(&viewport);
		m_GizmoManager.Update();
		grcViewport::SetCurrent(pushedViewport);

		if(m_IsMonotonicInX)
		{
			for(int i=1; i<m_Points.GetCount(); i++)
			{
				m_Points[i].x = Max(m_Points[i].x,m_Points[i-1].x+0.01f);
				m_Points[i-1].x = Min(m_Points[i-1].x,m_Points[i].x-0.01f);
			}
		}
	}

protected:
	int m_NumPoints;

	int m_NumPointsOld;

	atArray<Vector3> m_Points;

	atArray<gzTranslation*> m_Gizmos;

	bool m_IsMonotonicInX;

	gzManager & m_GizmoManager;

	// undefined
	testControlPointSet& operator=(const testControlPointSet&);
};


class curveChart
{
public:
	curveChart()
		: m_ControlPoints(m_GizmoManager)
		, m_CurveCatmullRomOpenActive(false)
		, m_CurveCatmullRomClosedActive(false)
		, m_CurveNurbsActive(false)
		, m_CurveNurbsFitActive(false)
		, m_CurveNurbsN3Active(false)
		, m_CurveNurbsN2Active(false)
		, m_CurveCubicActive(false)
		, m_CurveClosedCubicActive(false)
#if __BANK
		, m_Bank(NULL)
#endif
	{
	}

	~curveChart()
	{
#if __BANK
		BANKMGR.DestroyBank(*m_Bank);
#endif
	}

	void Init()
	{
		const int MAX_POINTS = 20;

		// Initialize set of control points (includes gizmos to control them onscreen)
		m_ControlPoints.AllocatePoints(MAX_POINTS);

		// Allocate space for the Catmull-Rom open curve
		m_CatmullRomOpenCurve.AllocateVertices(MAX_POINTS);
		m_CatmullRomOpenCurve.SetLooping(false);

		// Allocate space for the Catmull-Rom close curve
		m_CatmullRomClosedCurve.AllocateVertices(MAX_POINTS);
		m_CatmullRomClosedCurve.SetLooping(true);

		m_NurbsCurve.AllocateVertices(MAX_POINTS);

		m_NurbsFitCurve.AllocateVertices(MAX_POINTS);

		m_NurbsN3Curve.AllocateVertices(MAX_POINTS * 5);

		m_NurbsN2Curve.AllocateVertices(MAX_POINTS);

		m_CubicCurve.AllocateVertices(MAX_POINTS);

		m_ClosedCubicCurve.AllocateVertices(MAX_POINTS);
		m_ClosedCubicCurve.SetLooping(true);

#if __BANK
		Assert(m_Bank==NULL);
		m_Bank = & BANKMGR.CreateBank("Curve Chart");
		m_Bank->AddToggle("Catmull-Rom Open Curve",&m_CurveCatmullRomOpenActive);
		m_Bank->AddToggle("Catmull-Rom Closed Curve",&m_CurveCatmullRomClosedActive);
		m_Bank->AddToggle("NURBS",&m_CurveNurbsActive);
		m_Bank->AddToggle("NURBS VectorN3",&m_CurveNurbsN3Active);
		m_Bank->AddToggle("NURBS VectorN2",&m_CurveNurbsN2Active);
		m_Bank->AddToggle("NURBS Global Interp",&m_CurveNurbsFitActive);
		m_Bank->AddToggle("Cubic Open Curve",&m_CurveCubicActive);
		m_Bank->AddToggle("Cubic Closed Curve",&m_CurveClosedCubicActive);
		m_ControlPoints.AddWidgets(*m_Bank);
#endif
	}

	void Update()
	{
		if(grcViewport::GetCurrent())
		{
			m_ControlPoints.Update(m_CurveChart.GetViewport());
			if(m_CurveCatmullRomOpenActive)
			{
				UpdateCurve(m_ControlPoints,m_CatmullRomOpenCurve,4);
			}
			if(m_CurveCatmullRomClosedActive)
			{
				UpdateCurve(m_ControlPoints,m_CatmullRomClosedCurve,4);
			}
			if(m_CurveNurbsActive)
			{
				UpdateCurve(m_ControlPoints,m_NurbsCurve,4);
			}
			if(m_CurveNurbsFitActive)
			{
				UpdateCurve(m_ControlPoints,m_NurbsFitCurve,4);
				m_NurbsFitCurve.InterpolateGlobal();
			}
			if(m_CurveNurbsN3Active)
			{
				UpdateCurve2(m_ControlPoints,m_NurbsN3Curve,4);
			}
			if(m_CurveNurbsN2Active)
			{
				UpdateCurve(m_ControlPoints,m_NurbsN2Curve,4);
			}
			if(m_CurveCubicActive)
			{
				UpdateCurve(m_ControlPoints,m_CubicCurve,4);
			}
			if(m_CurveClosedCubicActive)
			{
				UpdateCurve(m_ControlPoints,m_ClosedCubicCurve,4);
			}
		}
	}

	template <class _Vector> void UpdateCurve(testControlPointSet & controlPoints, cvCurve<_Vector> & curve, int minNumPoints)
	{
		int numPoints = controlPoints.GetNumPoints();
		if(numPoints>=minNumPoints)
		{
			curve.SetNumVertices(numPoints);
			for(int i=0; i<numPoints; i++)
			{
				_Vector & v = curve.GetVertex(i);
				controlPoints.GetPoint(v,i);
			}
			curve.PostInit();
		}
	}

	template <class _Vector> void UpdateCurve2(testControlPointSet & controlPoints, cvCurve<_Vector> & curve, int minNumPoints)
	{
		int numPoints = controlPoints.GetNumPoints();
		if(numPoints>=minNumPoints)
		{
			curve.SetNumVertices(numPoints * 2);
			for(int i=0; i<numPoints; i++)
				for (int j = 0; j < 2; j++)
					controlPoints.GetPoint(curve.GetVertex(i * 2 + j),i);
			curve.PostInit();
		}
	}

	void Draw()
	{
		Assert(grcViewport::GetCurrent());
		int border = 13;
		m_CurveChart.SetBorderWidth(border*8.0f/13.0f,border*5.0f/13.0f);
		m_CurveChart.SetDrawPosition(border,grcViewport::GetCurrent()->GetHeight()-border,border,grcViewport::GetCurrent()->GetWidth()-border);
		m_CurveChart.SetXRange(-1.0f,1.0f);
		m_CurveChart.SetYRange(-1.0f,1.0f);

		// Begin drawing chart
		m_CurveChart.DrawOrthoBegin();
		m_CurveChart.DrawBackground();
		m_CurveChart.DrawGrid(1.0f,1.0f);

		if(m_CurveCatmullRomOpenActive)
		{
			grcColor(Color_SpringGreen1);
			DrawCurve(m_ControlPoints,m_CatmullRomOpenCurve,m_ControlPoints.GetNumPoints()-1,4);
		}
		if(m_CurveCatmullRomClosedActive)
		{
			grcColor(Color_purple1);
			DrawCurve(m_ControlPoints,m_CatmullRomClosedCurve,m_ControlPoints.GetNumPoints(),4);
		}
		if(m_CurveNurbsActive)
		{
			grcColor(Color_salmon2);
			DrawCurve(m_ControlPoints,m_NurbsCurve,m_ControlPoints.GetNumPoints(),4);
		}
		if(m_CurveNurbsFitActive)
		{
			grcColor(Color_green1);
			DrawCurve(m_ControlPoints,m_NurbsFitCurve,m_ControlPoints.GetNumPoints(),4);
		}
		if(m_CurveNurbsN3Active)
		{
			grcColor(Color_azure3);
			DrawCurve(m_ControlPoints,m_NurbsN3Curve,m_ControlPoints.GetNumPoints(),4);
		}
		if(m_CurveNurbsN2Active)
		{
			grcColor(Color_aquamarine2);
			DrawCurve(m_ControlPoints,m_NurbsN2Curve,m_ControlPoints.GetNumPoints(),4);
		}
		if(m_CurveCubicActive)
		{
			grcColor(Color_orange);
			DrawCurve(m_ControlPoints,m_CubicCurve,m_ControlPoints.GetNumPoints() - 1,4);
		}
		if(m_CurveClosedCubicActive)
		{
			grcColor(Color_yellow);
			DrawCurve(m_ControlPoints,m_ClosedCubicCurve,m_ControlPoints.GetNumPoints(),4);
		}

		m_GizmoManager.Draw();

		// End drawing chart
		m_CurveChart.DrawOrthoEnd();
	}

	template<class _Vector> void DrawCurve(testControlPointSet & , cvCurve<_Vector> & curve, int numSegments, int minNumPoints)
	{
		int numPoints = curve.GetNumVertices();
		if(numPoints<minNumPoints)
		{
			return;
		}

		Assert(numPoints==curve.GetNumVertices());
		Vector2 v;
		Vector2 vPrev;
		_Vector vTmp;

		for(int seg=0; seg<numSegments; seg++)
		{
			for(float t = 0.0f; t < 1.0f; t += 0.01f)
			{
				curve.SolveSegment(vTmp,seg,t);
				v[0] = vTmp[0];
				v[1] = vTmp[1];
				//vTmp.GetVector2XY(v);
				if(t>0)
				{
					m_CurveChart.DrawSegment(vPrev,v);
				}
				vPrev = v;
			}
		}
	}

protected:
	pfChart m_CurveChart;
	gzManager m_GizmoManager;

	testControlPointSet m_ControlPoints;

	bool m_CurveCatmullRomOpenActive;
	bool m_CurveCatmullRomClosedActive;
	bool m_CurveNurbsActive;
	bool m_CurveNurbsFitActive;
	bool m_CurveNurbsN3Active;
	bool m_CurveNurbsN2Active;
	bool m_CurveCubicActive;
	bool m_CurveClosedCubicActive;

	cvCurveCatRom m_CatmullRomOpenCurve;
	cvCurveCatRom m_CatmullRomClosedCurve;
	cvCurveNurbs<Vector3> m_NurbsCurve;
	cvCurveNurbs<Vector3> m_NurbsFitCurve;
	cvCurveNurbs< VectorN<3> > m_NurbsN3Curve;
	cvCurveNurbs< VectorN<2> > m_NurbsN2Curve;
	cvCurveCubic<Vector3> m_CubicCurve;
	cvCurveCubic<Vector3> m_ClosedCubicCurve;

	// undefined
	curveChart& operator=(const curveChart&);

	BANK_ONLY(bkBank * m_Bank;)
};


//=============================================================================
// TITLE: Sample Curve
// PURPOSE:
//		This sample shows the basics of using the rage/curve module

class curveSampleManager : public grcSampleManager
{
public:
	curveSampleManager()
	{
	}

	virtual void InitClient()
	{
		INIT_PARSER;

#if __PFDRAW
		GetRageProfileDraw().Init(1000000, true, grcBatcher::BUF_FULL_IGNORE);
		GetRageProfileDraw().SetEnabled(true);
#endif

		m_CurveChart.Init();
	}


	virtual void AddWidgetsClient()
	{
#if __BANK
		//STEP #2. Add widgets for curve system
		/*bkBank& bank =*/ BANKMGR.CreateBank("Curve");
		//-STOP
#endif
	}

	virtual void Update()
	{
#if __STATS
		GetRageProfiler().BeginFrame();
#endif // __STATS

		grcSampleManager::Update();
	}

	virtual void UpdateClient()
	{
		// STEP #5. The game update loop.

		//-- First update the input mapper.
		m_Mapper.Update();

		//-- Update the curve chart
		m_CurveChart.Update();

		//-STOP
	}

	virtual void DrawClient()
	{
		//-- Draw all the debug draw requests that have accumulated over this frame.
#if __PFDRAW
		GetRageProfileDraw().Render();
#endif
#if __STATS
		GetRageProfiler().Draw();
#endif

		//-- Draw the 2-d data if any
		m_CurveChart.Draw();

		//-STOP
	}

	virtual void ShutdownClient()
	{
#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif
#if __STATS
		GetRageProfiler().Shutdown();
#endif

		SHUTDOWN_PARSER;
	}

private:
	ioMapper m_Mapper;

	curveChart m_CurveChart;

	// undefined
	curveSampleManager& operator=(const curveSampleManager&);
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::curveSampleManager sampleCurve;
		sampleCurve.Init();

		sampleCurve.UpdateLoop();

		sampleCurve.Shutdown();
	}

	return 0;
}
