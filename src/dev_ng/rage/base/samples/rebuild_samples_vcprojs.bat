@echo off
pushd %~dp0
set started=%time% 
@echo REBUILD SAMPLE PROJECTS STARTED : %started%

if not defined RS_TOOLSCONFIG call setenv

if "%1"=="VS2008" goto VS2008

@echo.
@echo.
@echo.
@echo ****************************************************************************
@echo ****************************************************************************
@echo *** VS2010 Project generation started ( New Project Generator ) %time% 
@echo *** https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 
@echo ****************************************************************************
@echo ****************************************************************************
@echo.

REM sync to latest project generator config and binaries - only during development, do not remove I'll change sync.bat when no longer reqd.
call %RS_TOOLSROOT%\script\util\projGen\sync.bat

REM Build the sample dependencies.

pushd %RAGE_DIR%\base\src
call %RS_TOOLSROOT%\script\util\projGen\projgen.bat
popd 

REM Build the samples.

pushd %RAGE_DIR%\base\samples
call %RS_TOOLSROOT%\script\util\projGen\projgen.bat
popd 

@echo.
@echo ****************************************************************************
@echo ****************************************************************************
@echo *** VS2010 Project generation complete %time%
@echo ****************************************************************************
@echo ****************************************************************************
@echo.
@echo.
@echo.
if "%1"=="VS2010" goto END

:VS2008
@echo 2008 Project generation started ( Old Project Generator )
@echo https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder

pushd %RAGE_DIR%\base\samples
@call %RS_TOOLSROOT%\script\coding\projbuild\convert_dir.bat
popd

:END
@echo STARTED  : %started% FINISHED : %time% 