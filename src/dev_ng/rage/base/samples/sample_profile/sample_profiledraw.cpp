//
// sample_grcore\sample_profilerdraw.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Profile Drawing
// PURPOSE:
//		This sample shows how to use the profile drawing library.

#include "sample_grcore/sample_grcore.h"

#include "bank/bkmgr.h"
#include "grcore/setup.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "math/random.h"
#include "grprofile/ekg.h"
#include "profile/profiler.h"
#include "system/main.h"
#include "system/ipc.h"

using namespace rage;

namespace pfProfileTest
{
	// STEP #1. Create pages, groups and values that you'd like to display.

	//-- The following code declares the first visible page.  Since 
	// these macros are declaring instances of objects, they should be 
	// global data or exist in a class that will live throughout the lifetime
	// of the game.
	PF_PAGE(TestPage1,"Profiler Test - page 1");
	//-- We create a group and link that group to the first page.
	PF_GROUP(TestGroup1);
	PF_LINK(TestPage1,TestGroup1);
	//-- We add a timer that displays the frame time in that group.
	PF_TIMER(FrameTime,TestGroup1);

	//-- This is the declaration of second visible page.  It is 
	// similar to the first page, except it adds a float value instead
	// of a timer.
	PF_PAGE(TestPage2,"Profiler Test - page 2");
	PF_GROUP(TestGroup2);
	PF_LINK(TestPage2,TestGroup2);
	PF_VALUE_FLOAT(TestValue,TestGroup2);

	//-- This is the declaration of third (and last) visible page.  It 
	// declares 5 counters.
	PF_PAGE(TestPage3,"Profiler Test - page 3");
	PF_GROUP(TestGroup3);
	PF_LINK(TestPage3,TestGroup3);
	PF_COUNTER(Counter0,TestGroup3);
	PF_COUNTER(Counter1,TestGroup3);
	PF_COUNTER(Counter2,TestGroup3);
	PF_COUNTER(Counter3,TestGroup3);
	PF_COUNTER(Counter4,TestGroup3);
	// -STOP
};

using namespace pfProfileTest; 


class pfSample : public ragesamples::grcSampleManager
{
public:
	pfSample()
	{
	}


	void InitClient()
	{
		EnableCameraDraw(false);

#if __STATS
		//-- Here we bring up the first page of the profiler.
		// The profiler should have already been initialized 
		// by the grcSampleManager from which we are derived.
		GetRageProfiler().GetEkgMgr(0)->NextPage();
		// -STOP
#endif			
	}


	void UpdateClient()
	{
		// STEP #3. Update input.

		//-- This function process input from the controller
		// and input from the keyboard under windows.  See
		// the documentation on the rage::pfProfiler class 
		// to see what the controls are for the profile drawing.
		GetRageProfiler().UpdateInput();

		// STEP #4. Specify that the frame has begun.

		//-- This function clears out timers and counters.
		GetRageProfiler().BeginFrame();

		// STEP #5. Start our timers.

		//-- Start the timer that keeps track of the time to render
		// this frame.
		PF_START(FrameTime);

		// -STOP
#if __STATS
		static mthRandom sRandSource;

		sysIpcSleep(14);

		// STEP #6. Increment our counters.

		//-- For example purposes, we just randomly increment one of our 
		// 5 counters over 1000 iterations.
		const int RUN_SIZE = 1000;
		const int NUM_BINS = 5;
		CompileTimeAssert(NUM_BINS==5);
		for (int i=0; i<RUN_SIZE; i++)
		{
			int bin = sRandSource.GetRanged(0,(NUM_BINS-1)*(NUM_BINS-1));
			bin = (int)sqrtf((float)bin);
			switch (bin)
			{
			case 0:
				PF_INCREMENT(Counter0);
				break;
			case 1:
				PF_INCREMENT(Counter1);
				break;
			case 2:
				PF_INCREMENT(Counter2);
				break;
			case 3:
				PF_INCREMENT(Counter3);
				break;
			case 4:
				PF_INCREMENT(Counter4);
				break;
			}
		}

		// STEP #7. Set our test value.

		//-- Here we just set our <c>TestValue</c> stat to a random value each frame.
		float value = sRandSource.GetRanged(2.0f,3.0f);
		PF_SET(TestValue,value);

		// STEP #8. Stop our frame timer.

		//-- We stop our running timer.
		PF_STOP(FrameTime);
		// -STOP
#endif
		// STEP #9. End the frame for the profiler.

		//-- Signify that we've reached the end of our update frame.
		GetRageProfiler().EndFrame();
		// -STOP
	}

	void DrawClient()
	{
		// STEP #10. Draw the profiling information.

		//-- Now that we've collected all of our profiling information,
		// we can now draw it:
		GetRageProfiler().Draw();

		// STEP #11. Update our profiler manager.

		//-- Now we update the information in the profiler manager.
		// One thing this call does is to output to the log file for 
		// the profiler.
		GetRageProfiler().Update();
		// -STOP
	}

};


int Main()
{
	pfSample sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
