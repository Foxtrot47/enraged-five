// 
// sample_parser/objects_simple.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "objects_simple.h"
#include "objects_simple_parser.h"


int SimpleDtor1Obj::sm_CtorCount = 0;
int SimpleDtor2Obj::sm_CtorCount = 0;
int SimpleDtor4Obj::sm_CtorCount = 0;
int SimpleDtor16Obj::sm_CtorCount = 0;

int SimpleDtor1Obj::sm_DtorCount = 0;
int SimpleDtor2Obj::sm_DtorCount = 0;
int SimpleDtor4Obj::sm_DtorCount = 0;
int SimpleDtor16Obj::sm_DtorCount = 0;


RawArrayTester::~RawArrayTester()
{
	delete [] m_Pod1;
	delete [] m_Pod2;
	delete [] m_Pod4;
	delete [] m_Pod16;
	delete [] m_Dtor1;
	delete [] m_Dtor2;
	delete [] m_Dtor4;
	delete [] m_Dtor16;
}
