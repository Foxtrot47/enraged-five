// 
// sample_parser/sample_multipleinheritance.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "parser/manager.h"
#include "parser/rtstructure.h"
#include "system/main.h"

using namespace rage;

#include "objects_hierarchy.h"

#define PRINT_OFFSET(obj, var) Printf(#obj "->" #var " = 0x%" PTRDIFFTFMT "x\n", (char*)&(obj->var) - (char*)obj)

int Main()
{
	INIT_PARSER;

	Printf("NonVirtual:\n");

	MIBaseClass* base = NULL;
	MIDerivedA* derivedA = NULL;
	MIDerivedA2* derivedA2 = NULL;
	MIDerivedB* derivedB = NULL;
	MIDerivedB2* derivedB2 = NULL;

	PRINT_OFFSET(base, m_Base);
	PRINT_OFFSET(derivedA, m_Base);
	PRINT_OFFSET(derivedA, m_DerivedA);
	PRINT_OFFSET(derivedA2, m_Base);
	PRINT_OFFSET(derivedA2, m_DerivedA);
	PRINT_OFFSET(derivedA2, m_DerivedA2);
	PRINT_OFFSET(derivedA2, m_Mixin);
	PRINT_OFFSET(derivedB, m_Base);
	PRINT_OFFSET(derivedB, m_DerivedB);
	PRINT_OFFSET(derivedB2, m_Base);
	PRINT_OFFSET(derivedB2, m_DerivedB);
	PRINT_OFFSET(derivedB2, m_DerivedB2);
	PRINT_OFFSET(derivedB2, m_Mixin);


	MIDerivedA2* realA2p1 = (MIDerivedA2*)0x8;
	MIDerivedB2* realB2p1 = (MIDerivedB2*)0x8;

	Printf("DA as Base: %p\n", static_cast<MIBaseClass*>(derivedA));
	Printf("DA2 as DA: %p\n", static_cast<MIDerivedA*>(derivedA2));
	Printf("DA2 as Mixin: %p\n", static_cast<NonParsableMixin*>(derivedA2));
	Printf("DA2 as Base: %p\n", static_cast<MIBaseClass*>(derivedA2));
	Printf("realDA2 as Mixin: %p\n", static_cast<NonParsableMixin*>(realA2p1) - 0x8);
	Printf("realDA2 as Base: %p\n", static_cast<MIBaseClass*>(realA2p1) - 0x8);
	Printf("realDB2 as Mixin: %p\n", static_cast<NonParsableMixin*>(realB2p1) - 0x8);
	Printf("realDB2 as Base: %p\n", static_cast<MIBaseClass*>(realB2p1) - 0x8);
	Printf("DB as Base: %p\n", static_cast<MIBaseClass*>(derivedB));
	Printf("DB2 as DB: %p\n", static_cast<MIDerivedB*>(derivedB2));
	Printf("DB2 as Mixin: %p\n", static_cast<NonParsableMixin*>(derivedB2));
	Printf("DB2 as Base: %p\n", static_cast<MIBaseClass*>(derivedB2));

	Printf("Virtual:\n");

	MIVBaseClass* vbase = NULL;
	MIVDerivedA* vderivedA = NULL;
	MIVDerivedA2* vderivedA2 = NULL;
	MIVDerivedB* vderivedB = NULL;
	MIVDerivedB2* vderivedB2 = NULL;

	PRINT_OFFSET(vbase, m_Base);
	PRINT_OFFSET(vderivedA, m_Base);
	PRINT_OFFSET(vderivedA, m_DerivedA);
	PRINT_OFFSET(vderivedA2, m_Base);
	PRINT_OFFSET(vderivedA2, m_DerivedA);
	PRINT_OFFSET(vderivedA2, m_DerivedA2);
	PRINT_OFFSET(vderivedA2, m_Mixin);
	PRINT_OFFSET(vderivedB, m_Base);
	PRINT_OFFSET(vderivedB, m_DerivedB);
	PRINT_OFFSET(vderivedB2, m_Base);
	PRINT_OFFSET(vderivedB2, m_DerivedB);
	PRINT_OFFSET(vderivedB2, m_DerivedB2);
	PRINT_OFFSET(vderivedB2, m_Mixin);

//	MIVDerivedA2 realA2;
	MIVDerivedA2* realA2p = (MIVDerivedA2*)0x8;
	MIVDerivedB2* realB2p = (MIVDerivedB2*)0x8;

	Printf("DA as Base: %p\n", static_cast<MIVBaseClass*>(vderivedA));
	Printf("DA2 as DA: %p\n", static_cast<MIVDerivedA*>(vderivedA2));
	Printf("DA2 as Mixin: %p\n", static_cast<VNonParsableMixin*>(vderivedA2));
	Printf("DA2 as Base: %p\n", static_cast<MIVBaseClass*>(vderivedA2));
	Printf("realDA2 as Mixin: %p\n", static_cast<VNonParsableMixin*>(realA2p) - 0x8);
	Printf("realDA2 as Base: %p\n", static_cast<MIVBaseClass*>(realA2p) - 0x8);
	Printf("realDB2 as Mixin: %p\n", static_cast<VNonParsableMixin*>(realB2p) - 0x8);
	Printf("realDB2 as Base: %p\n", static_cast<MIVBaseClass*>(realB2p) - 0x8);
	Printf("DB as Base: %p\n", static_cast<MIVBaseClass*>(vderivedB));
	Printf("DB2 as DB: %p\n", static_cast<MIVDerivedB*>(vderivedB2));
	Printf("DB2 as Mixin: %p\n", static_cast<VNonParsableMixin*>(vderivedB2));
	Printf("DB2 as Base: %p\n", static_cast<MIVBaseClass*>(vderivedB2));

	SHUTDOWN_PARSER;

	return 0;
}

