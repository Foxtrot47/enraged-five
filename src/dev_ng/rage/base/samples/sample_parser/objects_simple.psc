<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">


<structdef type="TestString">
    <string name="m_String" type="pointer"/>
</structdef>

<enumdef type="SimpleStruct::TestEnum">
    <enumval name="BONEMASK_ALL" value="0"/>
    <enumval name="BONEMASK_UPPERONLY" value="3633914286"/>
    <enumval name="BONEMASK_SPINEONLY" value="2984602496"/>
    <enumval name="BONEMASK_ARMONLY_L" value="1739926164"/>
    <enumval name="BONEMASK_ARMONLY_R" value="3772128476"/>
    <enumval name="BONEMASK_BODYONLY" value="1432835604"/>
</enumdef>

<structdef preserveNames="true" type="SimpleStruct">
    <enum name="m_TestEnum" type="SimpleStruct::TestEnum"/>
    <Vector3 name="m_Position"/>
    <float max="1000.0f" min="0.0f" name="m_MinSpeed" step="0.1f"/>
    <float max="1000.0f" min="0.0f" name="m_MaxSpeed" step="0.1f"/>
    <int name="m_NumConnections"/>
    <string name="m_Name" type="pointer"/>
    <Matrix34 init="1.0f, 0.0f, 5.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f" name="m_Location"/>
    <string name="m_WideName" type="wide_pointer"/>
    <Color32 name="m_ForeColor"/>
</structdef>

<structdef type="TestMap">
    <map key="atHashString" name="m_Map" type="atMap">
        <pointer policy="owner" type="SimpleStruct"/>
    </map>
    <map key="atHashString" name="m_BinMap" type="atBinaryMap">
        <pointer policy="owner" type="SimpleStruct"/>
    </map>
</structdef>

<structdef preserveNames="true" type="SimpleContainer">
    <struct name="m_Struct1" type="SimpleStruct"/>
    <struct name="m_Struct2" type="SimpleStruct"/>
</structdef>

<enumdef type="AllSimpleData::SimpleEnum">
    <enumval name="ONE" value="1"/>
    <enumval name="TWO"/>
    <enumval name="THREE"/>
    <enumval name="FOUR"/>
    <enumval name="FIVE"/>
    <enumval name="SIX"/>
    <enumval name="SEVEN"/>
    <enumval name="EIGHT"/>
    <enumval name="NINE"/>
    <enumval name="TEN"/>
    <enumval name="ELEVEN"/>
    <enumval name="TWELVE"/>
    <enumval name="THIRTEEN"/>
    <enumval name="FOURTEEN"/>
    <enumval name="FIFTEEN"/>
    <enumval name="SIXTEEN"/>
    <enumval name="SEVENTEEN"/>
    <enumval name="EIGHTEEN"/>
    <enumval name="NINETEEN"/>
    <enumval name="TWENTY"/>
</enumdef>

<structdef type="AllSimpleData">
    <float name="m_Float"/>
    <char name="m_Char"/>
    <s8 name="m_AnotherChar"/>
    <u8 name="m_UChar"/>
    <short name="m_Short"/>
    <s16 name="m_AnotherShort"/>
    <u16 name="m_UShort"/>
    <int name="m_Int"/>
    <s32 name="m_AnotherInt"/>
    <u32 name="m_UInt"/>
    <bool name="m_Bool"/>
    <Vector2 name="m_Vector2"/>
    <Vector3 name="m_Vector3"/>
    <Vector4 name="m_Vector4"/>
    <Matrix34 name="m_Matrix34"/>
    <Matrix44 name="m_Matrix44"/>
    <Vec2V name="m_Vec2V"/>
    <Vec3V name="m_Vec3V"/>
    <Vec4V name="m_Vec4V"/>
    <ScalarV name="m_ScalarV"/>
    <BoolV name="m_BoolV"/>
    <VecBoolV name="m_VecBoolV"/>
    <Mat33V name="m_Mat33V"/>
    <Mat34V name="m_Mat34V"/>
    <Mat44V name="m_Mat44V"/>

    <float name="m_Angle" type="angle"/>
    <Color32 name="m_ColorA"/>
    <u32 name="m_ColorB" type="color"/>
    <Vector3 name="m_ColorC" type="color"/>
    <Vec3V name="m_ColorE" type="color"/>

    <enum name="m_EnumAsU8" size="8" type="AllSimpleData::SimpleEnum"/>
    <enum name="m_EnumAsU16" size="16" type="AllSimpleData::SimpleEnum"/>
    <enum name="m_EnumAsU32" type="AllSimpleData::SimpleEnum"/>

	<bitset name="m_FixedBitset" type="fixed" values="AllSimpleData::SimpleEnum"/>
	<bitset name="m_VariableBitset" type="atBitSet" values="AllSimpleData::SimpleEnum"/>

    <!-- doesn't work yet 
    <Vector4 name="m_ColorD" type="color"/>
    -->

    <string name="m_PointerString" type="pointer"/>
    <string name="m_MemberString" size="32" type="member"/>
    <!-- can't put this type in a PSO
    <string name="m_StdString" type="std::string"/>
    -->
    <string name="m_ConstString" type="ConstString"/>
    <string name="m_AtString" type="atString"/>
    <string name="m_AtWideString" type="atWideString"/>
    <string name="m_WidePointerString" type="wide_pointer"/>
    <string name="m_WideMemberString" size="32" type="wide_member"/>
</structdef>

<structdef type="SimpleDtor1Obj">
	<u8 name="m_Data"/>
</structdef>

<structdef type="SimpleDtor2Obj">
	<u16 name="m_Data"/>
</structdef>

<structdef type="SimpleDtor4Obj">
	<u32 name="m_Data"/>
</structdef>

<structdef type="SimpleDtor16Obj">
	<Vec4V name="m_Data"/>
</structdef>

<structdef type="TestBigInstance" simple="true">
  <u32 name="m_Data"/>
  <pad bytes="100000"/>
</structdef>
  
<structdef type="RawArrayTester">
	<array name="m_Pod1" size="4" type="pointer"><u8/></array>
	<array name="m_Pod2" size="4" type="pointer"><u16/></array>
	<array name="m_Pod4" size="4" type="pointer"><u32/></array>
	<array name="m_Pod16" size="4" type="pointer"><Vec4V/></array>
	<array name="m_Dtor1" size="4" type="pointer"><struct type="SimpleDtor1Obj"/></array>
	<array name="m_Dtor2" size="4" type="pointer"><struct type="SimpleDtor2Obj"/></array>
	<array name="m_Dtor4" size="4" type="pointer"><struct type="SimpleDtor4Obj"/></array>
	<array name="m_Dtor16" size="4" type="pointer"><struct type="SimpleDtor16Obj"/></array>
</structdef>

</ParserSchema>