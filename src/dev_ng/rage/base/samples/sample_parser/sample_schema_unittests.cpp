//
// sample_parser/sample_schema_unittests.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/psoparserbuilder.h"

#include "diag/output.h"
#include "math/simplemath.h"
#include "system/main.h"
#include "system/param.h"

#include "objects_unittests.h"

#include <set>

using namespace rage;

parManager* g_MainManager = NULL;
parManager* g_ExternalPscManager = NULL;
bool g_Verbose = false;

PARAM(testname, "Only run tests that contain this substring");



inline bool AreNearlyEqual(double x, double y, double tolerance)
{
	return (Abs(x - y) <= tolerance * Max(1.0, Abs(x), Abs(y)));
}

bool CompareAttributeLists(parAttributeList& a, parAttributeList& b, std::string path)
{
	bool match = true;

	for(int i = 0; i < a.GetAttributeArray().GetCount(); i++)
	{
		parAttribute& aAttr = a.GetAttributeArray()[i];
		if (!b.FindAttribute(aAttr.GetName()))
		{
			Displayf("%s MISSING ATTRIBUTE INPUT had %s", path.c_str(), aAttr.GetName());
			match = false;
		}
	}

	for(int i = 0; i < b.GetAttributeArray().GetCount(); i++)
	{
		parAttribute& bAttr = b.GetAttributeArray()[i];
		if (!a.FindAttribute(bAttr.GetName()))
		{
			Displayf("%s MISSING ATTRIBUTE REF had %s", path.c_str(), bAttr.GetName());
			match = false;
		}
	}


	for(int i = 0; i < a.GetAttributeArray().GetCount(); i++)
	{
		parAttribute* aAttr = &a.GetAttributeArray()[i];
		parAttribute* bAttr = b.FindAttribute(aAttr->GetName());
		parAttribute::Type type = aAttr->GetType();


		if (bAttr)
		{
			if (type == parAttribute::STRING)
			{
				type = bAttr->GetType();
			}

			switch(type)
			{
			case parAttribute::STRING:
				if (strcmp(aAttr->GetStringValue(), bAttr->GetStringValue()) != 0)
				{
					Displayf("%s/@%s VALUE MISMATCH INPUT=\"%s\" REF=\"%s\"", path.c_str(), aAttr->GetName(), aAttr->GetStringValue(), bAttr->GetStringValue());
					match = false;
				}
				break;
			case parAttribute::INT64:
				if (aAttr->FindInt64Value() != bAttr->FindInt64Value())
				{
					Displayf("%s/@%s VALUE MISMATCH INPUT=%" I64FMT "d REF=%" I64FMT "d", path.c_str(), aAttr->GetName(), aAttr->FindInt64Value(), bAttr->FindInt64Value());
					match = false;
				}
				break;
			case parAttribute::BOOL:
				if (aAttr->FindBoolValue() != bAttr->FindBoolValue())
				{
					Displayf("%s/@%s VALUE MISMATCH INPUT=%s REF=%s", path.c_str(), aAttr->GetName(), aAttr->FindBoolValue() ? "true" : "false", bAttr->FindBoolValue() ? "true" : "false");
					match = false;
				}
				break;
			case parAttribute::DOUBLE:
				{
					double aF = aAttr->FindDoubleValue();
					double bF = bAttr->FindDoubleValue();
					if (!AreNearlyEqual(aF, bF, 0.0001))
					{
						Displayf("%s/@%s VALUE MISMATCH INPUT=%f REF=%f", path.c_str(), aAttr->GetName(), aF, bF);
						match = false;
					}
				}
				break;
			}
		}
	}

	return match;
}

template<typename _Type>
bool CompareFloatArray( char* aData, char* bData, parTreeNode* a, std::string &path, int floats, int stride ) 
{
	_Type* aFData = (_Type*)aData;
	_Type* bFData = (_Type*)bData;

	size_t endIdx = a->GetDataSize()/sizeof(_Type);

	for(size_t i = 0; i < endIdx; i += stride)
	{
		for(size_t j = i; j < i+floats; j++)
		{
			if (!AreNearlyEqual(aFData[j], bFData[j], (_Type)0.0001f))
			{
				Displayf("%s DATA VALUE MISMATCH @%" SIZETFMT "x, INPUT=%f REF=%f", path.c_str(), j, aFData[j], bFData[j]);
				return false;
			}
		}
	}
	return true;
}


bool CompareDataArray(parTreeNode* a, parTreeNode* b, std::string path)
{
	char* aData = a->GetData();
	char* bData = b->GetData();

	if (!aData && !bData)
	{
		return true;
	}

	if ((aData && !bData) || (bData && !aData))
	{
		Displayf("%s DATA MISMATCH. INPUT %s data, REF %s data", path.c_str(), aData ? "had" : "didn't have", bData ? "had" : "didn't have");
		return false;
	}

	parStream::DataEncoding aEnc = a->FindDataEncoding();
	parStream::DataEncoding bEnc = b->FindDataEncoding();

	if (aEnc != bEnc)
	{
		Displayf("%s DATA ENCODING MISMATCH. INPUT=%d REF=%d", path.c_str(), aEnc, bEnc);
		return false;
	}

	if (a->GetDataSize() != b->GetDataSize())
	{
		Displayf("%s DATA SIZE MISMATCH. INPUT=%u REF=%u", path.c_str(), a->GetDataSize(), b->GetDataSize());
	}

	switch(aEnc)
	{
	case parStream::ASCII:
	case parStream::UTF16:
	case parStream::BINARY:
	case parStream::RAW_XML:
	case parStream::INT_ARRAY:
	case parStream::CHAR_ARRAY:
	case parStream::SHORT_ARRAY:
	case parStream::INT64_ARRAY:
	case parStream::UNSPECIFIED:
		if (memcmp(aData, bData, a->GetDataSize()) != 0)
		{
			Displayf("%s DATA VALUE MISMATCH", path.c_str());
			return false;
		}
		break;
	case parStream::FLOAT_ARRAY:		return CompareFloatArray<float>(aData, bData, a, path, 1, 1);
	case parStream::VECTOR2_ARRAY:		return CompareFloatArray<float>(aData, bData, a, path, 2, 2);
	case parStream::VEC2V_ARRAY:		return CompareFloatArray<float>(aData, bData, a, path, 2, 4);
	case parStream::VECTOR3_ARRAY:		return CompareFloatArray<float>(aData, bData, a, path, 3, 4);
	case parStream::VECTOR4_ARRAY:		return CompareFloatArray<float>(aData, bData, a, path, 4, 4);
	case parStream::MATRIX33:			return CompareFloatArray<float>(aData, bData, a, path, 3, 4);
	case parStream::MATRIX34:			return CompareFloatArray<float>(aData, bData, a, path, 3, 4);
	case parStream::MATRIX43:			return CompareFloatArray<float>(aData, bData, a, path, 4, 4);
	case parStream::MATRIX44:			return CompareFloatArray<float>(aData, bData, a, path, 4, 4);

	case parStream::DOUBLE_ARRAY:		return CompareFloatArray<double>(aData, bData, a, path, 1, 1);
	}

	return true;
}

bool CompareXmlNodesInner(parTreeNode* a, parTreeNode* b, std::string path)
{
	if (a && !b)
	{
		Displayf("%s REF IS EMPTY", path.c_str());
		return false;
	}
	if (b && !a)
	{
		Displayf("%s INPUT IS EMPTY", path.c_str());
		return false;
	}
	if (!a && !b)
	{
		return true;
	}

	if (strcmp(a->GetElement().GetName(), b->GetElement().GetName()) != 0)
	{
		Displayf("%s NAME MISMATCH INPUT=\"%s\" REF=\"%s\"", path.c_str(), a->GetElement().GetName(), b->GetElement().GetName());
		return false;
	}

	std::string newPath = path;
	newPath += "/";
	newPath += a->GetElement().GetName();

	bool match = true;

	match = match && CompareAttributeLists(a->GetElement().GetAttributeListRef(), b->GetElement().GetAttributeListRef(), newPath);

	match = match && CompareDataArray(a, b, newPath);

	// Check all nodes in sequence
	parTreeNode::ChildNodeIterator aKids = a->BeginChildren();
	parTreeNode::ChildNodeIterator bKids = b->BeginChildren();

	while(aKids != a->EndChildren())
	{
		if (bKids == b->EndChildren())
		{
			Displayf("%s ELEMENT COUNT MISMATCH INPUT=%d REF=%d", newPath.c_str(), a->FindNumChildren(), b->FindNumChildren());
			return false;
		}
		match = match && CompareXmlNodesInner(*aKids, *bKids, newPath);
		++aKids;
		++bKids;
	}
	if (bKids != b->EndChildren())
	{
		Displayf("%s ELEMENT COUNT MISMATCH INPUT=%d REF=%d", newPath.c_str(), a->FindNumChildren(), b->FindNumChildren());
		return false;
	}

	return match;
}

bool CompareXmlNodes(parTreeNode* a, parTreeNode* b, std::string path)
{
	bool result = CompareXmlNodesInner(a, b, path);
	if (!result && g_Verbose)
	{
		Displayf("Final XML");
		PARSER.SaveTree("console:", "", a, parManager::XML, &parSettings::sm_EmbeddedSettings);
		Displayf("\nReference XML");
		PARSER.SaveTree("console:", "", b, parManager::XML, &parSettings::sm_EmbeddedSettings);
		Displayf("");
	}
	return result;
}


parTree* LoadFromString(const char* str)
{
	char path[RAGE_MAX_PATH];
	fiDevice::MakeMemoryFileName(path, RAGE_MAX_PATH, str, strlen(str)+1, false, NULL);
	return PARSER.LoadTree(path, "");
}

//bool CompilePsoFromAssembly(const char* filename, std::string psoData)
//{
//	system("%SCE_PS3_ROOT%/host-win32/ppu/bin/ppu-lv2-as.exe -s <filename>");
//	system("%SCE_PS3_ROOT%/host-win32/ppu/bin/ppu-lv2-objcopy.exe -O binary -j .data <infile> <outfile>");
//	return false;
//}

bool FindInOutData(parTreeNode* testItem, parTreeNode*& inData, parTreeNode*& outData)
{
	int kids = testItem->FindNumChildren();
	if (kids == 1)
	{
		inData = testItem->GetChild();
		outData = testItem->GetChild();
		return true;
	}
	else if (kids == 2)
	{
		inData = testItem->GetChild();
		outData = inData->GetSibling();
		return true;
	}
	
	return false;
}

bool Test_XmlToObjectRoundTrip(parTreeNode* testItem)
{
	parTreeNode* inData;
	parTreeNode* referenceData;
	if (!FindInOutData(testItem, inData, referenceData))
	{
		return false;
	}

	const char* ptrType = testItem->GetElement().FindAttributeStringValue("ptrType", NULL, NULL, 0);

	parStructure* outStructure = NULL;

	if (ptrType)
	{
		outStructure = PARSER.FindStructure(ptrType);
	}

	parPtrToStructure outStorage = NULL;
	bool verifyStructure = (ptrType != NULL);
	PARSER.CreateAndLoadAnyType(inData, outStructure, outStorage, verifyStructure);

	if (!outStructure || !outStorage)
	{
		return false;
	}

	parTreeNode* resultTree = PARSER.BuildTreeNodeFromStructure(*outStructure, outStorage);

	bool result = CompareXmlNodes(resultTree, referenceData, "");

	delete resultTree;
	outStructure->Destroy(outStorage);
	delete [] (char*)outStorage;

	return result;
}

bool Test_XmlToExternalObjectRoundTrip(parTreeNode* testItem)
{
	parManager::sm_Instance = g_ExternalPscManager;
	bool result = Test_XmlToObjectRoundTrip(testItem);
	parManager::sm_Instance = g_MainManager;
	return result;
}


bool Test_XmlToPsoRoundTripCore(parTreeNode* testItem, bool inPlace)
{
	parTreeNode* inData;
	parTreeNode* referenceData;
	if (!FindInOutData(testItem, inData, referenceData))
	{
		return false;
	}

	const char* ptrType = testItem->GetElement().FindAttributeStringValue("ptrType", NULL, NULL, 0);

	parStructure* outStructure = NULL;

	if (ptrType)
	{
		outStructure = PARSER.FindStructure(ptrType);
	}

	parPtrToStructure outStorage = NULL;
	bool verifyStructure = (ptrType != NULL);
	PARSER.CreateAndLoadAnyType(inData, outStructure, outStorage, verifyStructure);

	if (!outStructure || !outStorage)
	{
		Warningf("Couldn't load XML data");
		return false;
	}

	psoSaveFromStructure("temp.pso", *outStructure, const_cast<parConstPtrToStructure>(outStorage), true);

	outStructure->Destroy(outStorage);
	delete [] (char*)outStorage;

	psoFile* pso = psoLoadFile("temp.pso", inPlace ? PSOLOAD_PREP_FOR_PARSER_LOADING : PSOLOAD_NO_PREP);

	if (!pso)
	{
		Warningf("Couldn't load PSO file");
		return false;
	}

	psoLoadInPlaceResult res = psoLoadObjectInPlace(*pso, PSO_HEAP_TEMP, PSO_HEAP_TEMP);

	delete pso;

	if (!res.IsOk() || !res.m_RootObject || !res.m_RootObjectType)
	{
		Warningf("Couldn't convert PSO to objects");
		return false;
	}

	parTreeNode* xmlifiedPso = PARSER.BuildTreeNodeFromStructure(*res.m_RootObjectType, res.m_RootObject);

	bool result = CompareXmlNodes(xmlifiedPso, referenceData, "");

	delete xmlifiedPso;

	if (res.m_InplaceBuffer)
	{
		delete [] (char*)res.m_InplaceBuffer;
	}
	else
	{
		res.m_RootObjectType->Destroy(res.m_RootObject);
		delete [] (char*)res.m_RootObject;
	}

	return result;
}

bool Test_XmlToPsoRoundTrip(parTreeNode* testItem)
{
	return Test_XmlToPsoRoundTripCore(testItem, false);
}

bool Test_XmlToPsoInPlaceRoundTrip(parTreeNode* testItem)
{
	return Test_XmlToPsoRoundTripCore(testItem, true);
}

bool Test_ExternXmlToPsoRoundTrip(parTreeNode* testItem)
{
	parManager::sm_Instance = g_ExternalPscManager;
	bool result = Test_XmlToPsoRoundTripCore(testItem, false);
	parManager::sm_Instance = g_MainManager;
	return result;
}

bool Test_ExternXmlToPsoInPlaceRoundTrip(parTreeNode* testItem)
{
	parManager::sm_Instance = g_ExternalPscManager;
	bool result = Test_XmlToPsoRoundTripCore(testItem, true);
	parManager::sm_Instance = g_MainManager;
	return result;
}

bool Test_CompareXml(parTreeNode* testItem)
{
	parTreeNode* inData;
	parTreeNode* outData;
	if (!FindInOutData(testItem, inData, outData))
	{
		return false;
	}

	return CompareXmlNodes(inData, outData, "");
}

typedef bool(*TestFn)(parTreeNode* item);

atMap<atHashString, TestFn> g_Tests;

enum TestResult
{
	TRUE_POSITIVE,
	TRUE_NEGATIVE,
	FALSE_POSITIVE,
	FALSE_NEGATIVE
};

void RunTestsOnNode(parTreeNode* node, std::string path, int nodeNumber, const char* testString, bool expectedResult, int* results)
{
	testString = node->GetElement().FindAttributeStringValue("tests", testString, NULL, 0);
	expectedResult = node->GetElement().FindAttributeBoolValue("expect", expectedResult);

	atArray<atString> testArr;
	atString(testString).Split(testArr, ';', true);

	std::string testPath = path;
	const char* testName = node->GetElement().FindAttributeStringValue("name", NULL, NULL, 0);
	if (testName)
	{
		testPath += " ";
		testPath += testName;
	}

	const char* selectedTestName = NULL;
	if (PARAM_testname.Get(selectedTestName) && selectedTestName)
	{
		if (testPath.find(selectedTestName) == std::string::npos)
		{
			return;
		}
	}

	char buf[32];
	formatf(buf, "%d", nodeNumber);
	DIAG_CONTEXT_MESSAGE("Test: %s #%s", path.c_str(), buf);

	for(int i = 0; i < testArr.GetCount(); i++)
	{
		DIAG_CONTEXT_MESSAGE(testArr[i].c_str());
		TestFn* testFn = g_Tests.Access(testArr[i].c_str());
		if (testFn && *testFn)
		{
			bool result = (*testFn)(node); // Run the test
			
			if ((result == true) && (expectedResult == true))
			{
				results[TRUE_POSITIVE]++; 
				// keep going
			}
			else if ((result == true) && (expectedResult == false))
			{
				results[FALSE_POSITIVE]++;
				Displayf("");
				Warningf("");
				Warningf(">>>>>>>>>>>>>>> %s #%d, Test %s passed and shouldn't have", testPath.c_str(), nodeNumber, testArr[i].c_str());
				
				g_Verbose = true;
				(*testFn)(node);
				g_Verbose = false;

				Warningf("<<<<<<<<<<<<<<");
				Warningf("");
				Displayf("");

				return;
			}
			else if ((result == false) && (expectedResult == false))
			{
				results[TRUE_NEGATIVE]++;
				// keep going?
			}
			else 
			{
				results[FALSE_NEGATIVE]++;
				Displayf("");
				Warningf("");
				Warningf(">>>>>>>>>>>>>>> %s #%d, Test %s failed", testPath.c_str(), nodeNumber, testArr[i].c_str());

				g_Verbose = true;
				(*testFn)(node);
				g_Verbose = false;

				Warningf("<<<<<<<<<<<<<<");
				Warningf("");
				Displayf("");
				return;
			}
		}
	}
}

void RunTestsOnGroup(parTreeNode* node, std::string path, const char* testString, bool expectedResult, int* results)
{
	testString = node->GetElement().FindAttributeStringValue("tests", testString, NULL, 0);
	expectedResult = node->GetElement().FindAttributeBoolValue("expect", expectedResult);

	int subIdx = 1;
	int testIdx = 1;

	for(parTreeNode::ChildNodeIterator kid = node->BeginChildren(); kid != node->EndChildren(); ++kid)
	{
		if (!strcmp((*kid)->GetElement().GetName(), "Test"))
		{
			RunTestsOnNode(*kid, path, testIdx, testString, expectedResult, results);
			testIdx++;
		}
		else if (!strcmp((*kid)->GetElement().GetName(), "Subgroup"))
		{
			const char* subName = (*kid)->GetElement().FindAttributeStringValue("name", "", NULL, 0);
			char subPathName[256];
			if (subName && subName[0] != '\0')
			{
				formatf(subPathName, ", %s", subName);
			}
			else
			{
				formatf(subPathName, " (sub %d)", subIdx);
			}
			std::string newPath = path;
			newPath += subPathName;
			RunTestsOnGroup(*kid, newPath, testString, expectedResult, results);
			subIdx++;
		}
	}
}


int g_FullResults[4] = {};

void RunTestsOnTopGroup(parTreeNode* node)
{

	const char* groupName = node->GetElement().FindAttributeStringValue("name", "", NULL, 0);
	if (!groupName)
	{
		Errorf("All groups need names!");
		return;
	}

	Displayf("-----------------------------------------------------------------");
	Displayf("-----------------------------------------------------------------");
	Displayf("Group %s", groupName);

	int results[4];
	results[0] = results[1] = results[2] = results[3] = 0;

	RunTestsOnGroup(node, std::string(groupName), "", true, results);

	Displayf("Results: ");
	Displayf("%d Correctly Passed, %d Correctly Failed", results[TRUE_POSITIVE], results[TRUE_NEGATIVE]);
	if (results[FALSE_POSITIVE] > 0 || results[FALSE_NEGATIVE] > 0)
	{
		Errorf("%d Incorrectly Failed, %d Incorrectly Passed", results[FALSE_NEGATIVE], results[FALSE_POSITIVE]);
	}
	else
	{
		Displayf("All tests correct");
	}
	Displayf("-----------------------------------------------------------------");
	Displayf("-----------------------------------------------------------------");
	Displayf("");
	Displayf("");

	for(int i = 0; i < 4; i++)
	{
		g_FullResults[i] += results[i];
	}
}

int Main()
{
	INIT_PARSER;

	g_MainManager = parManager::sm_Instance;

	// Set up the external manager
	g_ExternalPscManager = rage_new parManager;
	g_ExternalPscManager->Initialize(parSettings::sm_StandardSettings, false);
	parManager::sm_Instance = g_ExternalPscManager;
	parAddReflectionClassesToManager(*g_ExternalPscManager);
	PARSER.GetExternalStructureManager().LoadStructdefs(".");
	parManager::sm_Instance = g_MainManager;


	g_Tests["compareXml"] = Test_CompareXml;
	g_Tests["xmlToObj"] = Test_XmlToObjectRoundTrip;
	g_Tests["extXmlToObj"] = Test_XmlToExternalObjectRoundTrip;
	g_Tests["xmlToPso"] = Test_XmlToPsoRoundTrip;
	g_Tests["extXmlToPso"] = Test_ExternXmlToPsoRoundTrip;
	g_Tests["xmlToPsoInPlace"] = Test_XmlToPsoInPlaceRoundTrip;
	g_Tests["extXmlToPsoInPlace"] = Test_ExternXmlToPsoInPlaceRoundTrip;

	parTree* testXml = PARSER.LoadTree("unittests.xml", "");

	for(parTreeNode::ChildNodeIterator kid = testXml->GetRoot()->BeginChildren(); kid != testXml->GetRoot()->EndChildren(); ++kid)
	{
		RunTestsOnTopGroup(*kid);
	}

	int correct = g_FullResults[TRUE_POSITIVE] + g_FullResults[TRUE_NEGATIVE];
	int incorrect = g_FullResults[FALSE_POSITIVE] + g_FullResults[FALSE_NEGATIVE];

	Displayf("-----------------------------------------------------------------");
	Displayf("-----------------------------------------------------------------");
	Displayf("------------------------  SUMMARY  ------------------------------");
	Displayf("%d Total Tests", correct + incorrect);
	Displayf("%d Correct: %d Correctly Passed, %d Correctly Failed", correct, g_FullResults[TRUE_POSITIVE], g_FullResults[TRUE_NEGATIVE]);
	if (incorrect > 0)
	{
		Errorf("%d Incorrect: %d Incorrectly Failed, %d Incorrectly Passed", incorrect, g_FullResults[FALSE_NEGATIVE], g_FullResults[FALSE_POSITIVE]);
	}
	else
	{
		Displayf("All tests correct");
	}


	delete testXml;

	g_ExternalPscManager->Uninitialize();
	delete g_ExternalPscManager;

	SHUTDOWN_PARSER;

	return 0;
}