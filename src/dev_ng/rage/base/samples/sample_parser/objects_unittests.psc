<?xml version="1.0"?>

<!-- Unit tests for the parser. objects_simple and objects_hierarchy have subsets of this, objects_container has big test objects, 
this tests the individual types and provides examples for people writing PSC files -->

<!-- All the unit tests will have structdefs that contain a description of the test, grouped together with a prefix number. -->

<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
				generate="class">
  <hinsert>
#include "objects_simple.h"
  </hinsert>
  
	<!--
  ########################################################################################################################
  ########################################################################################################################
  1000s - tests of basic structdef features 
  -->
	<structdef type="UT_1000_Empty"/>
	
	<structdef type="UT_1010_EmptySimple" simple="true"/>

	<structdef name="UT-1020-EmptyNamed" type="UT_1020_EmptyNamed_RealCppName"/>
	<!-- note name has to be an XML identifier, but does not need to be a C++ identifier -->
	
	<structdef type="UT_1030_Base"/>
	<structdef type="UT_1030_Derived" base="UT_1030_Base"/>

	<structdef type="UT_1035_BaseSimple" simple="true"/>
	<structdef type="UT_1035_DerivedSimple" base="UT_1035_BaseSimple" simple="true"/>

	<!-- Expected failure. Non-simple can't inherit from simple
	<structdef type="UT_1037_DerivedNotSimple" base="UT_1035_BaseSimple"/>
	-->
	
	<structdef type="UT_1040_Abstract" constructible="false"/>
	<structdef type="UT_1040_Concrete" base="UT_1040_Abstract"/>
	
	<structdef type="UnitTests::UT_1050_Namespace"/>
	
	<structdef type="UnitTests::LongNs::UT_1060_Namespace"/>
	
	<structdef type="UT_1070_KeepNames" preserveNames="true"/>

	<structdef type="UT_1080_Member">
		<u8 name="m_A"/>
	</structdef>
	
	<structdef type="UT_1090_BaseMember">
		<u8 name="m_A"/>
	</structdef>
	<structdef type="UT_1090_DerivedNoMember" base="UT_1090_BaseMember"/>
	
	<structdef type="UT_1100_DerivedMember" base="UT_1030_Base">
		<u8 name="m_A"/>
	</structdef>
	
	<!-- Expected failure. Name conflict
	<structdef type="UT_1110_BaseAndDerivedMembers" base="UT_1090_BaseMember">
		<bool name="m_A"/>
	</structdef>
	-->

	<structdef type="UT_1111_BaseAndDerivedMembers" base="UT_1090_BaseMember">
		<bool name="m_B"/>
	</structdef>

	
  <!--
  ########################################################################################################################
  ########################################################################################################################
  2000s - tests of basic enumdef features 
  -->
	
	<enumdef type="UT_2000_Empty"/>
	
	<enumdef type="UT_2010_Simple">
		<enumval name="ENUM_A"/>
		<enumval name="ENUM_B"/>
	</enumdef>

  <enumdef type="UT_2020_Namespace::Enum"> <!-- from here down we'll use namespaces for enums to avoid pollting the global NS -->
    <enumval name="ENUM_A"/>
    <enumval name="ENUM_B"/>
  </enumdef>
  
  <enumdef type="UT_2030_Explicit::Enum">
		<enumval name="A" value="100"/>
		<enumval name="B" value="200"/>
	</enumdef>
	
	<enumdef type="UT_2040_AutoIncr::Enum">
		<enumval name="A"/> <!-- 0 -->
		<enumval name="B"/> <!-- 1 -->
		<enumval name="C" value="100"/> <!-- 100 -->
		<enumval name="D"/> <!-- 101 -->
	</enumdef>
	
	<enumdef type="UT_2050_Incr::Enum" values="incr">
		<enumval name="A"/>
		<enumval name="B"/>
	</enumdef>

	<enumdef type="UT_2060_Hash::Enum" values="hash">
		<enumval name="A"/>
		<enumval name="B"/>
	</enumdef>

	<enumdef type="UT_2070_LitHash::Enum" values="literalhash">
		<enumval name="A"/>
		<enumval name="a"/>
	</enumdef>

	<enumdef type="UT_2080_AtHash16::Enum" values="atHash16">
		<enumval name="A"/>
		<enumval name="B"/>
	</enumdef>

	<enumdef type="UT_2090_AtHash16U::Enum" values="atHash16U">
		<enumval name="A"/>
		<enumval name="B"/>
	</enumdef>

  <!-- TODO: Fix namespaces and generated bitset objects
  <enumdef type="UT_2100_Generated::Enum" generate="bitset">
    <enumval name="A"/>
    <enumval name="B"/>
  </enumdef>

  <enumdef type="UT_2010_GeneratedLarge::Enum" generate="bitset">
    <enumval name="flag_A00"/>
    <enumval name="flag_A01"/>
    <enumval name="flag_A02"/>
    <enumval name="flag_A03"/>
    <enumval name="flag_A04"/>
    <enumval name="flag_A05"/>
    <enumval name="flag_A06"/>
    <enumval name="flag_A07"/>
    <enumval name="flag_A08"/>
    <enumval name="flag_A09"/>
    <enumval name="flag_A0a"/>
    <enumval name="flag_A0b"/>
    <enumval name="flag_A0c"/>
    <enumval name="flag_A0d"/>
    <enumval name="flag_A0e"/>
    <enumval name="flag_A0f"/>
    <enumval name="flag_B00"/>
    <enumval name="flag_B01"/>
    <enumval name="flag_B02"/>
    <enumval name="flag_B03"/>
    <enumval name="flag_B04"/>
    <enumval name="flag_B05"/>
    <enumval name="flag_B06"/>
    <enumval name="flag_B07"/>
    <enumval name="flag_B08"/>
    <enumval name="flag_B09"/>
    <enumval name="flag_B0a"/>
    <enumval name="flag_B0b"/>
    <enumval name="flag_B0c"/>
    <enumval name="flag_B0d"/>
    <enumval name="flag_B0e"/>
    <enumval name="flag_B0f"/>
    <enumval name="flag_C00"/>
    <enumval name="flag_C01"/>
    <enumval name="flag_C02"/>
    <enumval name="flag_C03"/>
    <enumval name="flag_C04"/>
    <enumval name="flag_C05"/>
    <enumval name="flag_C06"/>
    <enumval name="flag_C07"/>
    <enumval name="flag_C08"/>
    <enumval name="flag_C09"/>
    <enumval name="flag_C0a"/>
    <enumval name="flag_C0b"/>
    <enumval name="flag_C0c"/>
    <enumval name="flag_C0d"/>
    <enumval name="flag_C0e"/>
    <enumval name="flag_C0f"/>
  </enumdef>
-->
  <!--
  ########################################################################################################################
  ########################################################################################################################
	3000s - POD types 
  -->
	
	<structdef type="UT_3000_Bool">
		<bool name="m_A"/>
	</structdef>
	
	<structdef type="UT_3010_Bools">
		<bool name="m_A"/>
		<bool name="m_B" description="this is some data"/>
		<bool name="m_C" init="false"/>
		<bool name="m_D" init="true"/>
		<bool name="m_E" hideWidgets="true"/>
		<bool name="m_F" noInit="true"/>
		<bool name="m_G" parName="DataG"/>
	</structdef>
	
	<structdef type="UT_3020_BoolV">
		<BoolV name="m_A"/>
	</structdef>

	<structdef type="UT_3030_BoolVs">
		<BoolV name="m_A"/>
		<BoolV name="m_B" description="this is some data"/>
		<BoolV name="m_C" init="false"/>
		<BoolV name="m_D" init="true"/>
		<BoolV name="m_E" hideWidgets="true"/>
		<BoolV name="m_F" noInit="true"/>
		<BoolV name="m_G" parName="DataG"/>
	</structdef>
	
	<structdef type="UT_3040_VecBoolV">
		<VecBoolV name="m_A"/>
	</structdef>

	<structdef type="UT_3050_VecBoolVs">
		<VecBoolV name="m_A"/>
		<VecBoolV name="m_B" description="this is some data"/>
		<VecBoolV name="m_C" init="false"/>
		<VecBoolV name="m_D" init="true"/>
		<VecBoolV name="m_E" hideWidgets="true"/>
		<VecBoolV name="m_F" noInit="true"/>
		<VecBoolV name="m_G" parName="DataG"/>
	</structdef>
	
	<structdef type="UT_3060_S8">
		<s8 name="m_A"/>
	</structdef>

	<structdef type="UT_3061_S8">
		<char name="m_A"/>
	</structdef>
	
	<structdef type="UT_3070_S8s">
		<s8 name="m_A" init="12"/>
		<s8 name="m_B" description="this is some data" hideWidgets="true"/>
		<s8 name="m_C" init="-10" parName="DataC"/>
	</structdef>
	
	<structdef type="UT_3080_U8">
		<u8 name="m_A"/>
	</structdef>

	<structdef type="UT_3090_U8s">
		<u8 name="m_A" init="12"/>
		<u8 name="m_B" description="this is some data" hideWidgets="true"/>
		<u8 name="m_C" init="10" parName="DataC"/>
	</structdef>
	
		
	<structdef type="UT_3100_S16">
		<s16 name="m_A"/>
	</structdef>
	
	<structdef type="UT_3101_S16">
		<short name="m_A"/>
	</structdef>
	
	<structdef type="UT_3110_S16s">
		<s16 name="m_A" init="12"/>
		<s16 name="m_B" init="-100"/>
	</structdef>
	
	<structdef type="UT_3120_U16">
		<u16 name="m_A"/>
	</structdef>

	<structdef type="UT_3130_U16s">
		<u16 name="m_A" init="12"/>
		<u16 name="m_B" init="40000"/>
	</structdef>
	
	<structdef type="UT_3140_S32">
		<s32 name="m_A"/>
	</structdef>
	
	<structdef type="UT_3160_U32">
		<u32 name="m_A"/>
	</structdef>
	
	<structdef type="UT_3180_Sizet">
		<size_t name="m_A"/>
	</structdef>
	
	<structdef type="UT_3200_Ptrdifft">
		<ptrdiff_t name="m_A"/>
	</structdef>

  <structdef type="UT_3210_S64">
    <s64 name="m_A"/>
  </structdef>

  <structdef type="UT_3211_U64">
    <u64 name="m_A"/>
  </structdef>
	
	<structdef type="UT_3220_Float16">
		<Float16 name="m_A"/>
	</structdef>
	
	<structdef type="UT_3240_Float">
		<float name="m_A"/>
	</structdef>

	<structdef type="UT_3260_ScalarV">
		<ScalarV name="m_A"/>
	</structdef>

  <structdef type="UT_3270_Double">
    <double name="m_A"/>
  </structdef>

	<structdef type="UT_3280_Vector2">
		<Vector2 name="m_A"/>
	</structdef>

	<structdef type="UT_3281_Vec2V">
		<Vec2V name="m_A"/>
	</structdef>
	
	<structdef type="UT_3300_Vector3">
		<Vector3 name="m_A"/>
	</structdef>

	<structdef type="UT_3301_Vec3V">
		<Vec3V name="m_A"/>
	</structdef>
	
	<structdef type="UT_3320_Vector4">
		<Vector4 name="m_A"/>
	</structdef>

	<structdef type="UT_3321_Vec4V">
		<Vec4V name="m_A"/>
	</structdef>

	<structdef type="UT_3340_Matrix34">
		<Matrix34 name="m_A"/>
	</structdef>

	<structdef type="UT_3341_Mat34V">
		<Mat34V name="m_A"/>
	</structdef>

	<structdef type="UT_3360_Matrix44">
		<Matrix44 name="m_A"/>
	</structdef>

	<structdef type="UT_3361_Mat44V">
		<Mat44V name="m_A"/>
	</structdef>

	<structdef type="UT_3380_Mat33V">
		<Mat33V name="m_A"/>
	</structdef>
	
  <!--
  ########################################################################################################################
  ########################################################################################################################
	4000s - String types 
  -->
  
  <structdef type="UT_4000_MemString">
	<string name="m_A" type="member" size="2"/>
  </structdef>

  <structdef type="UT_4001_StringAttributes">
	<string name="m_A" type="member" size="20" hideWidgets="true"/>
	<string name="m_B" type="member" size="20" description="this is a string"/>
	<string name="m_C" type="member" size="20" init="CCCCCC"/>
	<string name="m_D" type="member" size="20" init=""/>
	<string name="m_E" type="member" size="20" parName="DataE"/>
	<string name="m_F" type="member" size="20" init="This init value is toooooooo long for the string"/>
  </structdef>

  <structdef type="UT_4002_LargeMemString">
	<string name="m_A" type="member" size="70000"/>
  </structdef>

  <structdef type="UT_4010_PtrString">
	<string name="m_A" type="pointer"/>
  </structdef>

  <structdef type="UT_4011_PtrString">
	<string name="m_A" type="pointer" init=""/>
	<string name="m_B" type="pointer" init="this is an init string"/>
  </structdef>
  
  <structdef type="UT_4020_ConstString">
	<string name="m_A" type="ConstString"/>
  </structdef>

  <structdef type="UT_4021_ConstString">
	<string name="m_A" type="ConstString" init=""/>
	<string name="m_B" type="ConstString" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4030_atString">
	<string name="m_A" type="atString"/>
  </structdef>

  <structdef type="UT_4031_atString">
	<string name="m_A" type="atString" init=""/>
	<string name="m_B" type="atString" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4040_WidePtrString">
	<string name="m_A" type="wide_pointer"/>
  </structdef>

  <structdef type="UT_4041_WidePtrString">
	<string name="m_A" type="wide_pointer" init=""/>
	<string name="m_B" type="wide_pointer" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4050_WideMemString">
	<string name="m_A" type="wide_member" size="2"/>
  </structdef>

  <structdef type="UT_4051_WideMemString">
	<string name="m_A" type="wide_member" size="20" init=""/>
	<string name="m_B" type="wide_member" size="20" init="this is an init string that is tooo long for the member"/>
  </structdef>

  <structdef type="UT_4060_atWideString">
	<string name="m_A" type="atWideString"/>
  </structdef>

  <structdef type="UT_4061_atWideString">
	<string name="m_A" type="atWideString" init=""/>
	<string name="m_B" type="atWideString" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4070_atHashString">
	<string name="m_A" type="atHashString"/>
  </structdef>
  
  <structdef type="UT_4071_atHashString">
	<string name="m_A" type="atHashString" init=""/>
	<string name="m_B" type="atHashString" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4080_atFinalHashString">
	<string name="m_A" type="atFinalHashString"/>
  </structdef>
  
  <structdef type="UT_4081_atFinalHashString">
	<string name="m_A" type="atFinalHashString" init=""/>
	<string name="m_B" type="atFinalHashString" init="this is an init string"/>
  </structdef>
  
  <structdef type="UT_4090_atHashValue">
	<string name="m_A" type="atHashValue"/>
  </structdef>
  
  <structdef type="UT_4091_atHashValue">
	<string name="m_A" type="atHashValue" init=""/>
	<string name="m_B" type="atHashValue" init="this is an init string"/>
  </structdef>
  
  <structdef type="UT_4100_atHashWithStringDev">
	<string name="m_A" type="atHashWithStringDev"/>
  </structdef>
  
  <structdef type="UT_4101_atHashWithStringDev">
	<string name="m_A" type="atHashWithStringDev" init=""/>
	<string name="m_B" type="atHashWithStringDev" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4110_atHashWithStringBank">
	<string name="m_A" type="atHashWithStringBank"/>
  </structdef>
  
  <structdef type="UT_4111_atHashWithStringBank">
	<string name="m_A" type="atHashWithStringBank" init=""/>
	<string name="m_B" type="atHashWithStringBank" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4120_atHashWithStringNotFinal">
	<string name="m_A" type="atHashWithStringNotFinal"/>
  </structdef>
  
  <structdef type="UT_4121_atHashWithStringNotFinal">
	<string name="m_A" type="atHashWithStringNotFinal" init=""/>
	<string name="m_B" type="atHashWithStringNotFinal" init="this is an init string"/>
  </structdef>

  <structdef type="UT_4130_atDiagHashString">
    <string name="m_A" type="atDiagHashString"/>
  </structdef>

  <structdef type="UT_4131_atDiagHashString">
    <string name="m_A" type="atDiagHashString" init=""/>
    <string name="m_B" type="atDiagHashString" init="this is an init string"/>
  </structdef>

 
  <!--
  ########################################################################################################################
  ########################################################################################################################
	5000s - Struct types 
  -->
  
  <structdef type="UT_5000_EmptyStr">
	<struct name="m_A" type="UT_1000_Empty"/>
  </structdef>
  
  <structdef type="UT_5001_EmptyStrAndPod">
	<struct name="m_A" type="UT_1000_Empty"/>
	<u8 name="m_B"/>
  </structdef>
  
  <structdef type="UT_5002_PodAndEmptyStr">
	<u8 name="m_A"/>
	<struct name="m_B" type="UT_1000_Empty"/>
  </structdef>
  
  <structdef type="UT_5003_EmptyStrAndEmptyStr">
	<struct name="m_A" type="UT_1000_Empty"/>
	<struct name="m_B" type="UT_1000_Empty"/>
  </structdef>
  
  <structdef type="UT_5010_EmptySimple">
	<struct name="m_A" type="UT_1010_EmptySimple"/>
  </structdef>
  
  <structdef type="UT_5011_EmptySimpleAndPod">
	<struct name="m_A" type="UT_1010_EmptySimple"/>
	<u8 name="m_B"/>
  </structdef>
  
  <structdef type="UT_5012_PodAndEmptySimple">
	<u8 name="m_A"/>
	<struct name="m_B" type="UT_1010_EmptySimple"/>
  </structdef>
  
  <structdef type="UT_5013_EmptySimpleAndEmptySimple">
	<struct name="m_A" type="UT_1010_EmptySimple"/>
	<struct name="m_B" type="UT_1010_EmptySimple"/>
  </structdef>
  
  <structdef type="UT_5020_EmptyBase">
	<struct name="m_A" type="UT_1030_Base"/>
  </structdef>
  
  <structdef type="UT_5021_EmptyDerived">
	<struct name="m_A" type="UT_1030_Derived"/>
  </structdef>

  <structdef type="UT_5030_EmptyBaseSimple">
	<struct name="m_A" type="UT_1035_BaseSimple"/>
  </structdef>
  
  <structdef type="UT_5031_EmptyDerivedSimple">
	<struct name="m_A" type="UT_1035_DerivedSimple"/>
  </structdef>


  <!--
  ########################################################################################################################
  ########################################################################################################################
	6000s - Array types 
  -->

  <structdef type="UT_6000_PodArray_Bool">
    <array name="m_A" type="atArray">
      <bool/>
    </array>
  </structdef>

  <structdef type="UT_6010_PodArray_BoolV">
    <array name="m_A" type="atArray">
      <BoolV/>
    </array>
  </structdef>
  
  <structdef type="UT_6020_PodArray_VecBoolV">
    <array name="m_A" type="atArray">
      <VecBoolV/>
    </array>
  </structdef>

  <structdef type="UT_6030_PodArray_S8">
    <array name="m_A" type="atArray">
      <s8/>
    </array>
  </structdef>

  <structdef type="UT_6040_PodArray_U8">
    <array name="m_A" type="atArray">
      <u8/>
    </array>
  </structdef>

  <structdef type="UT_6050_PodArray_S16">
    <array name="m_A" type="atArray">
      <s16/>
    </array>
  </structdef>

  <structdef type="UT_6060_PodArray_U16">
    <array name="m_A" type="atArray">
      <u16/>
    </array>
  </structdef>

  <structdef type="UT_6070_PodArray_S32">
    <array name="m_A" type="atArray">
      <s32/>
    </array>
  </structdef>

  <structdef type="UT_6080_PodArray_U32">
    <array name="m_A" type="atArray">
      <u32/>
    </array>
  </structdef>

  <structdef type="UT_6090_PodArray_S64">
    <array name="m_A" type="atArray">
      <s64/>
    </array>
  </structdef>

  <structdef type="UT_6100_PodArray_U64">
    <array name="m_A" type="atArray">
      <u64/>
    </array>
  </structdef>

  <structdef type="UT_6110_PodArray_Float16">
    <array name="m_A" type="atArray">
      <Float16/>
    </array>
  </structdef>

  <structdef type="UT_6120_PodArray_Float">
    <array name="m_A" type="atArray">
      <float/>
    </array>
  </structdef>

  <structdef type="UT_6100_PodArray_ScalarV">
    <array name="m_A" type="atArray">
      <ScalarV/>
    </array>
  </structdef>

  <structdef type="UT_6100_PodArray_Double">
    <array name="m_A" type="atArray">
      <double/>
    </array>
  </structdef>

  <!-- 
  ########################################################################################################################
  ########################################################################################################################
	7000s - Map tests
  -->

  <structdef type="UT_7000_Map_S8_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="s8">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7010_Map_U8_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="u8">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7020_Map_S16_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="s16">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7030_Map_U16_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="u16">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7040_Map_S32_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="s32">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7050_Map_U32_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="u32">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7060_Map_S64_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="s64">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7070_Map_U64_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="u64">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7080_Map_atHashString_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="atHashString">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7100_Map_atHashValue_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="atHashValue">
      <string type="atHashString"/>
    </map>
  </structdef>

  <structdef type="UT_7140_Map_enum_to_hashstring">
    <map name="m_A" type="atBinaryMap" key="enum" enumKeySize="32" enumKeyType="UT_2040_AutoIncr::Enum">
      <string type="atHashString"/>
    </map>
  </structdef>
  
  
  
  <!-- 
  ########################################################################################################################
  ########################################################################################################################
	9000s - Misc (or uncategorized) tests
  -->

  <structdef type="UT_9001_LargePsoStructarrays">
    <array name="m_Array" type="atArray">
      <pointer type="TestBigInstance" policy="simple_owner"/>
    </array>
  </structdef>

  <structdef type="UT_9002_PhysicalAllocs">
    <array name="m_Array" type="pointer" size="256" heap="physical">
      <u16/>
    </array>
  </structdef>
  
</ParserSchema>