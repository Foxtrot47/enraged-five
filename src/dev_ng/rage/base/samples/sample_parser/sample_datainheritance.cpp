//
// sample_parser/sample_datainheritance.cpp
//
// Copyright (C) 1999-2011 Rockstar Games. All Rights Reserved.
//


#include "data/growbuffer.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "system/main.h"

#include "objects_container.h"

using namespace rage;

int Main()
{
	INIT_PARSER;

	datGrowBuffer output;
	output.Init(&sysMemAllocator::GetCurrent(), datGrowBuffer::NULL_TERMINATE);
	char gbname[RAGE_MAX_PATH];
	fiDevice::MakeGrowBufferFileName(gbname, RAGE_MAX_PATH, &output);

	output.Clear();

	// Step 1 - Simple example of data inheritance (A -> B)
	Prop p1;
	PARSER.LoadObject("crate_simple", "prop", p1);

	PARSER.SaveObject(gbname, "", &p1);
	Displayf("%s", (char*)output.GetBuffer());
	output.Clear();

	// Step 2 - Example of using multiple parents (A -> B, A.x -> C)
	Prop p2;
	PARSER.LoadObject("crate_multiparent1", "prop", p2);

	PARSER.SaveObject(gbname, "", &p2);
	Displayf("%s", (char*)output.GetBuffer());
	output.Clear();

	// Step 3 - Another example of using multiple parents (A -> B, B.x -> C)
	Prop p3;
	PARSER.LoadObject("crate_multiparent2", "prop", p3);

	PARSER.SaveObject(gbname, "", &p3);
	Displayf("%s", (char*)output.GetBuffer());
	output.Clear();

	// Step 4 - Loading a pointer - concrete type is specified in child file, data is in parent file
	Prop* p4;
	PARSER.LoadObjectPtr("derived", "prop", p4);

	PARSER.SaveObject(gbname, "", p4);
	Displayf("%s", (char*)output.GetBuffer());
	output.Clear();

	// Step 5- Loading a pointer - concrete type of sub-object is specified in child file
	Vehicle v;
	PARSER.LoadObject("lowrider", "veh", v);

	PARSER.SaveObject(gbname, "", &v);
	Displayf("%s", (char*)output.GetBuffer());
	output.Clear();

	SHUTDOWN_PARSER;

	return 0;
}
