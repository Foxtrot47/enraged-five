// 
// sample_parser/objects_hierarchy.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_PARSER_OBJECT_HIERARCHY_H
#define SAMPLE_PARSER_OBJECT_HIERARCHY_H

#include "parser/manager.h"
#include "vector/vector3.h"

/*//////////////////////////////////////////////
// The class hierarchy looks like this:
//
//                        BaseClass
//                     /      |      \						//
//                    /       |       \						//
//                   /        |        \					//
//        DerivedClassA DerivedClassB AbstractClass
//                                         |
//                                         |
//                                    ConcreteSubclass
*/

class BaseClass 
{
public:
	BaseClass() : m_BaseFloat(0.0f) {}
	virtual ~BaseClass() { }

	virtual const char* GetName() const {
		return "Base";
	}

	float m_BaseFloat;

	PAR_PARSABLE;
};

class DerivedClassA : public BaseClass
{
public:
	DerivedClassA() : m_DerivedInt(0) {}

	virtual const char* GetName() const {
		return "DerivedA";
	}

	int m_DerivedInt;

	PAR_PARSABLE;
};

class DerivedClassB : public BaseClass
{
public:
	DerivedClassB() : m_DerivedVector(0.0f, 0.0f, 0.0f) {}

	virtual const char* GetName() const {
		return "DerivedB";
	}

	rage::Vector3 m_DerivedVector;

	PAR_PARSABLE;
};

class AbstractClass : public BaseClass
{
public:
	virtual float GetSize() = 0;

	float m_AbstractFloat;

	PAR_PARSABLE;
};

class ConcreteSubclass : public AbstractClass
{
public:
	virtual float GetSize() {return m_AbstractFloat * m_ConcreteFloat;}

	float m_ConcreteFloat;

	PAR_PARSABLE;
};

class SimplePointerContainer  {
public:
	SimplePointerContainer() : 
	  m_ItemOne(NULL),
	  m_ItemTwo(NULL)
	  {
	  }

	~SimplePointerContainer()
	{
	  delete m_ItemOne;
	  delete m_ItemTwo;
	}

	BaseClass* m_ItemOne;
	BaseClass* m_ItemTwo;

	PAR_SIMPLE_PARSABLE;
};

/*//////////////////////////////////////////////
// The class hierarchy looks like this:
//                               MIBaseClass
//                              /         \									//
//  NonParsableMixin  MIDerivedA       MIDerivedB     NonParsableMixin
//           \         /                         \     /	
//            MIDerivedA2                      MIDerivedB2
*/

class NonParsableMixin
{
public:
	float m_Mixin;
};

class MIBaseClass
{
public:
	virtual ~MIBaseClass() {}
	float m_Base;
	PAR_PARSABLE;
};

class MIDerivedA : public MIBaseClass
{
public:
	virtual ~MIDerivedA() {}
	float m_DerivedA;
	PAR_PARSABLE;
};

class MIDerivedB : public MIBaseClass
{
public:
	virtual ~MIDerivedB() {}
	float m_DerivedB;
	PAR_PARSABLE;
};

class MIDerivedA2 : public NonParsableMixin, public MIDerivedA
{
public:
	virtual ~MIDerivedA2() {}
	float m_DerivedA2;
	PAR_PARSABLE;
};

class MIDerivedB2 : public MIDerivedB, public NonParsableMixin
{
public:
	virtual ~MIDerivedB2() {}
	float m_DerivedB2;
	PAR_PARSABLE;
};


class VNonParsableMixin
{
public:
	virtual ~VNonParsableMixin() {}
	float m_Mixin;

	virtual void DoSomethingV() {};
};


class MIVBaseClass
{
public:
	virtual ~MIVBaseClass() {}
	float m_Base;
	PAR_PARSABLE;

	virtual void DoSomething() {};
};

class MIVDerivedA : public MIVBaseClass
{
public:
	virtual ~MIVDerivedA() {}
	float m_DerivedA;
	PAR_PARSABLE;

	virtual void DoSomethingA() {};
};

class MIVDerivedB : public MIVBaseClass
{
public:
	virtual ~MIVDerivedB() {}
	float m_DerivedB;
	PAR_PARSABLE;

	virtual void DoSomethingB() {};
};

class MIVDerivedA2 : public VNonParsableMixin, public MIVDerivedA
{
public:
	virtual ~MIVDerivedA2() {}
	float m_DerivedA2;
	PAR_PARSABLE;

	virtual void DoSomethingA2() {};
};

class MIVDerivedB2 : public MIVDerivedB, public VNonParsableMixin
{
public:
	virtual ~MIVDerivedB2() {}
	float m_DerivedB2;
	PAR_PARSABLE;

	virtual void DoSomethingB2() {};
};


#endif
