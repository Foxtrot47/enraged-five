// 
// sample_parser/sample_rest.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "objects_simple.h"
#include "objects_container.h"
#include "objects_hierarchy.h"

#include "data/growbuffer.h"
#include "diag/restcommand.h"
#include "diag/restservice.h"
#include "math/random.h"
#include "parser/manager.h"
#include "parser/restparserservices.h"
#include "system/main.h"
#include "system/timer.h"

#include "diag/civetweb.h"

#include "file/tcpip.h"

#include <ctime>

using namespace rage;

namespace rage
{
	extern diagChannel Channel_Legacy;
}

void TestRest(restCommand::Verb verb, const char* url, restStatus::Enum expectedStatus, const char* data = NULL)
{
	datGrowBuffer gb;
	gb.Init(NULL, datGrowBuffer::NULL_TERMINATE);

	char gbName[RAGE_MAX_PATH];

	fiDevice::MakeGrowBufferFileName(gbName, RAGE_MAX_PATH, &gb);

	fiStream* stream = ASSET.Create(gbName, "");
	restCommand cmd;
	restStatus::Enum status;
	
	if (cmd.Init(verb, url, stream, data))
	{
		status = REST.ProcessCommand(cmd);
	}
	else
	{
		status = restStatus::REST_BAD_REQUEST;
	}

	cmd.m_OutputStream->Flush();
	cmd.m_OutputStream->Close();

	if (status != expectedStatus)
	{
		Warningf("Query: %d %s\nExpected status %d, Got response: %d\n%s", verb, url, expectedStatus, status, gb.GetBuffer() ? gb.GetBuffer() : "");
	}
	else
	{
		Displayf("Query: %d %s\nResponse: %d", verb, url, status);
		Displayf("%s", gb.GetBuffer() ? (const char*)gb.GetBuffer() : "");
	}
	Displayf("---------------------------------------");
}

bool g_runWebServer = true;

int Main()
{
	using namespace restStatus;

	INIT_PARSER;

	SimpleStruct ss;
	ss.m_Position = Vector3(30.0, 20.0f, 10.0f);
	ss.m_Location.Identity();
	ss.m_MinSpeed = 0.001f;
	ss.m_MaxSpeed = 100.0f;
	ss.m_NumConnections = -1;
	ss.m_TestEnum = SimpleStruct::BONEMASK_ARMONLY_L;

	SimpleContainer sc;
	sc.m_Struct1 = ss;
	sc.m_Struct1.m_Name = StringDuplicate("My ASCII Name");
	sc.m_Struct1.m_WideName = WideStringDuplicate(_C16("My Wide Name!"));
	sc.m_Struct2 = ss;
	sc.m_Struct2.m_Name = StringDuplicate("My Other Ascii Name");
	sc.m_Struct2.m_WideName = WideStringDuplicate(_C16("My Wide Name!"));

	PointerArray arr;
	arr.m_Name = "ArrayOfPointers!";

	BaseClass* item = NULL;
	
	item = rage_new ConcreteSubclass();
	arr.m_Children.PushAndGrow(item, 4);

	item = rage_new DerivedClassA();
	arr.m_Children.PushAndGrow(item, 4);

	item = rage_new DerivedClassB();
	arr.m_Children.PushAndGrow(item, 4);

	item = rage_new DerivedClassA();
	arr.m_Children.PushAndGrow(item, 4);

    TestMap tm;
    SimpleStruct* newSS = NULL;
    newSS = rage_new SimpleStruct();
    newSS->m_Position = Vector3(30.0, 20.0f, 10.0f);
    newSS->m_Location.Identity();
    newSS->m_MinSpeed = 0.001f;
    newSS->m_MaxSpeed = 100.0f;
    newSS->m_NumConnections = -1;
    newSS->m_TestEnum = SimpleStruct::BONEMASK_ARMONLY_R;
    tm.m_Map.Insert(atHashString("TestHash"), newSS);

    newSS = rage_new SimpleStruct();
    newSS->m_Position = Vector3(30.0, 20.0f, 10.0f);
    newSS->m_Location.Identity();
    newSS->m_MinSpeed = 0.001f;
    newSS->m_MaxSpeed = 100.0f;
    newSS->m_NumConnections = -1;
    newSS->m_TestEnum = SimpleStruct::BONEMASK_ARMONLY_L;
    tm.m_Map.Insert(atHashString("TestHash2"), newSS);

    newSS = rage_new SimpleStruct();
    newSS->m_Position = Vector3(30.0, 20.0f, 10.0f);
    newSS->m_Location.Identity();
    newSS->m_MinSpeed = 0.001f;
    newSS->m_MaxSpeed = 100.0f;
    newSS->m_NumConnections = -1;
    newSS->m_TestEnum = SimpleStruct::BONEMASK_ARMONLY_R;
    tm.m_BinMap.Insert(atHashString("TestHash"), newSS);

    newSS = rage_new SimpleStruct();
    newSS->m_Position = Vector3(30.0, 20.0f, 10.0f);
    newSS->m_Location.Identity();
    newSS->m_MinSpeed = 0.001f;
    newSS->m_MaxSpeed = 100.0f;
    newSS->m_NumConnections = -1;
    newSS->m_TestEnum = SimpleStruct::BONEMASK_ARMONLY_L;
    tm.m_BinMap.Insert(atHashString("TestHash2"), newSS);
    tm.m_BinMap.FinishInsertion();

#if 0
Displayf("Fake server!");
	fiDeviceTcpIp::InitClass(sysParam::GetArgCount(), sysParam::GetArgArray());


	fiHandle h = fiDeviceTcpIp::Listen(80, 3);
	
	fiHandle client = fiDeviceTcpIp::Pickup(h);

	char buff[128];

	fiDeviceTcpIp::GetInstance().Read(client, buff, sizeof(buff));

	fiDeviceTcpIp::GetInstance().Close(client);
	fiDeviceTcpIp::GetInstance().Close(h);
	

	Displayf(buff);

Displayf("Real server!");
#endif

	REST.Init();

	// Build some fake session ID

	u32 seed = static_cast<u32>(sysTimer::GetTicks());
	g_DrawRand.Reset(seed);
	u32 sessionId = g_DrawRand.GetInt();
	// If I linked in the net library I might xor in my IP addr or mac addr or something here.

	parRestParserGuidService::InitClass(sessionId);

	// Register a few REST services...
    parRestRegisterSingleton("Parser/MAP", tm, "");
	parRestRegisterSingleton("Parser/AOP", arr, "");
	parRestRegisterSingleton("Vehicles/Colors", sc, "");
	parRestRegisterSingleton("PickupData", sc, "");
	parRestRegisterSingleton("Scenarios", sc, "");
	parRestRegisterSingleton("Vehicles/Layouts", sc, "");
	parRestRegisterSingleton("Graphics/Clouds", sc, "");
	parRestRegisterSingleton("Weapons/WeaponComponents", sc, "");
	parRestRegisterSingleton("Weapons/WeaponInfo", sc, "");
	parRestRegisterSingleton("Weapons/FiringPatterns", sc, "");

#if 1
	// Basic directory searches
	TestRest(restCommand::REST_GET, ""								, REST_OK);
	TestRest(restCommand::REST_GET, "/"								, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/"			, REST_OK);
	TestRest(restCommand::REST_GET, "Vehicles"						, REST_OK);
	TestRest(restCommand::REST_GET, "/Vehicles"						, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/Graphics"	, REST_OK);
	TestRest(restCommand::REST_GET, "Graphics/"						, REST_OK);

	// Some bad commands
	TestRest(restCommand::REST_GET, "MissingService/"				, REST_NOT_FOUND);
	TestRest(restCommand::REST_GET, "Graphics/MissingService"		, REST_NOT_FOUND);
	TestRest(restCommand::REST_DELETE, "Graphics/"					, REST_METHOD_NOT_ALLOWED);

	// Searches with a query
	TestRest(restCommand::REST_GET, "?x=1"							, REST_OK);
	TestRest(restCommand::REST_GET, "/?x=1"							, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/?x=1"		, REST_OK);
	TestRest(restCommand::REST_GET, "Vehicles?x=1"					, REST_OK);
	TestRest(restCommand::REST_GET, "/Vehicles?x=1"					, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/Graphics?x=1", REST_OK);
	TestRest(restCommand::REST_GET, "Graphics/?x=1"					, REST_NOT_FOUND);

	// Queries can have name=value pairs or just names (or name=)
	TestRest(restCommand::REST_GET, "?x=1&y=2&z&w="					, REST_OK);
	TestRest(restCommand::REST_GET, "/?x=1&y=2&z&w="				, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/?x=1&y=2&z&w=", REST_OK);

	// Some malformed URIs
	TestRest(restCommand::REST_GET, "http:"							, REST_BAD_REQUEST);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/?=2"			, REST_BAD_REQUEST);

	// Tests that get to a real service...
	TestRest(restCommand::REST_GET, "http://10.0.23.15/Vehicles/Layouts"				, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/Vehicles/Layouts/Struct1"		, REST_OK);
	TestRest(restCommand::REST_GET, "http://10.0.23.15/Vehicles/Layouts/StructDoesntExist", REST_NOT_FOUND);
	TestRest(restCommand::REST_GET, "Vehicles/Layouts/Struct1/Position"					, REST_OK);
	TestRest(restCommand::REST_GET, "Scenarios/Struct2/Position"						, REST_OK);
	TestRest(restCommand::REST_DELETE, "Vehicles/Layouts/Struct1"						, REST_METHOD_NOT_ALLOWED);

	// Test to set a value
	TestRest(restCommand::REST_PUT, "Vehicles/Layouts/Struct1/MinSpeed"	, REST_OK_NO_CONTENT,	"2.0");

// This one requires more error checking in our low level string to number routines
//	TestRest(restCommand::REST_PUT, "Vehicles/Layouts/Struct1/MinSpeed"	, REST_BAD_REQUEST,		"notANumber!");

	TestRest(restCommand::REST_PUT, "Vehicles/Layouts/Struct1"				, REST_OK_NO_CONTENT,	"<SimpleStruct><MinSpeed value=\"123.0\"/></SimpleStruct>");
//	TestRest(restCommand::REST_PUT, "Vehicles/Layouts/Struct1"				, REST_BAD_REQUEST,		"this is not XML.");
	TestRest(restCommand::REST_PUT, "Vehicles/Layouts/Struct1/Name"			, REST_OK_NO_CONTENT,	"MyNewName");
	TestRest(restCommand::REST_PUT, "Vehicles/Layouts/Struct1"				, REST_OK_NO_CONTENT,	"<SimpleStruct><MinSpeed value=\"234.0\"/><Name>This is my name</Name></SimpleStruct>");

	// Try loading data from a file
	TestRest(restCommand::REST_PUT, "Vehicles/Layouts?src=structin.xml"		, REST_OK_NO_CONTENT);
	TestRest(restCommand::REST_PUT, "Vehicles/Layouts?src=nonexistant.xml"	, REST_BAD_REQUEST);

	// Test deleting a pointer
	TestRest(restCommand::REST_DELETE, "Parser/AOP/Children/0"				, REST_OK_NO_CONTENT);
	TestRest(restCommand::REST_GET, "Parser/AOP/Children"					, REST_OK);
	// Test creating new data for a pointer
	TestRest(restCommand::REST_PUT, "Parser/AOP/Children/0"					, REST_OK_NO_CONTENT,	"<ConcreteSubclass/>");

	// Can't delete a structure!
	TestRest(restCommand::REST_DELETE, "Vehicles/Layouts/Struct1"			, REST_METHOD_NOT_ALLOWED);

	// Delete a pointer again
	TestRest(restCommand::REST_DELETE, "Parser/AOP/Children/0"				, REST_OK_NO_CONTENT);
	// And fill it with the contents of a file
	TestRest(restCommand::REST_PUT, "Parser/AOP/Children/0?src=derivedB.xml"	, REST_OK_NO_CONTENT);
	TestRest(restCommand::REST_GET, "Parser/AOP/Children"					, REST_OK);

	// Test getting an array element
	TestRest(restCommand::REST_GET, "Parser/AOP/Children/1"					, REST_OK);
	TestRest(restCommand::REST_GET, "Parser/AOP/Children/1/BaseFloat"		, REST_OK);

	// Test deleting an array element
	TestRest(restCommand::REST_POST, "Parser/AOP/Children?delete=1"			, REST_OK_NO_CONTENT);
	TestRest(restCommand::REST_POST, "Parser/AOP/Children?delete=300"		, REST_NOT_FOUND);
	// And get the array again to make sure it still exists
	TestRest(restCommand::REST_GET, "Parser/AOP/Children"					, REST_OK);

	TestRest(restCommand::REST_POST, "Parser/AOP/Children?insert=0"			, REST_CREATED, "<A_Base_Class><BaseFloat value=\"765.321\"/></A_Base_Class>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children?insert=2&src=derivedB.xml", REST_CREATED);
	
	// Get the array again
	TestRest(restCommand::REST_GET, "Parser/AOP/Children"					, REST_OK);

	// Overwrite part of a pointer
	TestRest(restCommand::REST_PUT, "Parser/AOP/Children/2"					, REST_OK_NO_CONTENT, "<DerivedClassB><DerivedVector x=\"45.0\" y=\"12.0\" z=\".245\"/></DerivedClassB>");
	// Try to change the type and fail
	TestRest(restCommand::REST_PUT, "Parser/AOP/Children/2"					, REST_BAD_REQUEST, "<DerivedClassA><DerivedInt value=\"13\"/></DerivedClassA>");
	// But this is OK for now (setting properties using a base class' name)
	TestRest(restCommand::REST_PUT, "Parser/AOP/Children/2"					, REST_OK_NO_CONTENT, "<A_Base_Class><BaseFloat value=\"234.563\"/></A_Base_Class>");


	// Add a bunch so the array resizes
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");
	TestRest(restCommand::REST_POST, "Parser/AOP/Children"					, REST_CREATED, "<ConcreteSubclass><BaseFloat value=\"1.0\"/><AbstractFloat value=\"2.0\"/><ConcreteFloat value=\"3.0\"/></ConcreteSubclass>");

	// Get the array again
	TestRest(restCommand::REST_GET, "Parser/AOP/Children"					, REST_OK);

	// Get some GUIDs
	u32 arrGuid = parRestParserGuidService::GetInstance()->LookupGuidForObject(arr.parser_GetPointer());
	parAssertf(arrGuid == 0, "shouldn't have a guid assigned yet!");

	TestRest(restCommand::REST_GET, "Parser/AOP?guid"						, REST_OK);

	arrGuid = parRestParserGuidService::GetInstance()->LookupGuidForObject(arr.parser_GetPointer());
	parAssertf(arrGuid != 0, "OK now it has one");

	TestRest(restCommand::REST_GET, "Parser/AOP/Children/1?guid"			, REST_BAD_REQUEST);
	TestRest(restCommand::REST_GET, "Vehicles/Layouts?guid"					, REST_OK);

	u32 scGuid = parRestParserGuidService::GetInstance()->LookupGuidForObject(sc.parser_GetPointer());
	parAssertf(scGuid != 0, "No guid for simplecontainer");

	TestRest(restCommand::REST_GET, "Graphics/Clouds?guid"					, REST_OK);
	TestRest(restCommand::REST_GET, "Bad/Path?guid"							, REST_NOT_FOUND);

	// Convert the GUIDs into names
	char arrGuidName[16];
	parRestParserGuidService::GetInstance()->MakeFullGuidName(arrGuid, arrGuidName);

	char scGuidName[16];
	parRestParserGuidService::GetInstance()->MakeFullGuidName(scGuid, scGuidName);

	char queryString[256];
	formatf(queryString, "ParGUID/%s/Children/1/BaseFloat", arrGuidName);
	TestRest(restCommand::REST_GET, queryString, REST_OK);

	formatf(queryString, "ParGUID/%s/Struct1/Name", scGuidName);
	TestRest(restCommand::REST_PUT, queryString, REST_OK_NO_CONTENT, "A new name");

	formatf(queryString, "ParGUID/%s/Children?delete=4", arrGuidName);
	TestRest(restCommand::REST_POST, queryString, REST_OK_NO_CONTENT);

//	parRestUnregisterSingleton("Parser/AOP");
	REST.RemoveAndDeleteService("Parser/AOP");

	// Make sure the unregistered service is no longer accessable

	TestRest(restCommand::REST_GET, "Parser/AOP/Children/3", REST_NOT_FOUND);

	formatf(queryString, "ParGUID/%s/Children?delete=4", arrGuidName);
	TestRest(restCommand::REST_POST, queryString, REST_NOT_FOUND);

    // Test getting everything in the map
    TestRest(restCommand::REST_GET, "Parser/MAP/Map", REST_OK);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap", REST_OK);

    // Test getting a mapped element
    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash", REST_OK);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash2", REST_OK);

    TestRest(restCommand::REST_GET, "Parser/MAP/Map/NotInMap", REST_NOT_FOUND);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/NotInMap", REST_NOT_FOUND);

    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash/MaxSpeed", REST_OK);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash2/MinSpeed", REST_OK);

    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash/NotInStruct", REST_NOT_FOUND);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash2/NotInStruct", REST_NOT_FOUND);

    // Test setting a mapped element
    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash/Name", REST_OK);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash2/Name", REST_OK);

    TestRest(restCommand::REST_PUT, "Parser/MAP/Map/TestHash/Name", REST_OK_NO_CONTENT, "NEW NAME");
    TestRest(restCommand::REST_PUT, "Parser/MAP/BinMap/TestHash2/Name", REST_OK_NO_CONTENT, "NEW NAME");

    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash/Name", REST_OK);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash2/Name", REST_OK);

    // Test deleting a mapped element
    TestRest(restCommand::REST_POST, "Parser/MAP/Map?delete=TestHash"	, REST_OK_NO_CONTENT);
    TestRest(restCommand::REST_POST, "Parser/MAP/Map?delete=4"		    , REST_NOT_FOUND);

    TestRest(restCommand::REST_POST, "Parser/MAP/BinMap?delete=TestHash2"	, REST_OK_NO_CONTENT);
    TestRest(restCommand::REST_POST, "Parser/MAP/BinMap?delete=4"		    , REST_NOT_FOUND);

    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash"	            , REST_NOT_FOUND);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash2"		    , REST_NOT_FOUND);

    // Test inserting a mapped element
    TestRest(restCommand::REST_POST, "Parser/MAP/Map?insert=TestHash3"	    , REST_CREATED, "<item type=\"SimpleStruct\"><TestEnum>BONEMASK_ARMONLY_R</TestEnum><Position x=\"0.000000\" y=\"0.000000\" z=\"0.000000\"/><MinSpeed value=\"0.000000\"/><MaxSpeed value=\"200.000000\"/><NumConnections value=\"3\"/></item>");
    TestRest(restCommand::REST_POST, "Parser/MAP/BinMap?insert=TestHash3"	, REST_CREATED, "<item type=\"SimpleStruct\"><TestEnum>BONEMASK_ARMONLY_L</TestEnum><Position x=\"0.000000\" y=\"0.000000\" z=\"0.000000\"/><MinSpeed value=\"0.000000\"/><MaxSpeed value=\"200.000000\"/><NumConnections value=\"3\"/></item>");

    TestRest(restCommand::REST_GET, "Parser/MAP/Map/TestHash3"	            , REST_OK);
    TestRest(restCommand::REST_GET, "Parser/MAP/BinMap/TestHash3"		    , REST_OK);

    // Test inserting the same key
    TestRest(restCommand::REST_POST, "Parser/MAP/Map?insert=TestHash4"	    , REST_CREATED, "<item type=\"SimpleStruct\"><TestEnum>BONEMASK_ARMONLY_R</TestEnum><Position x=\"0.000000\" y=\"0.000000\" z=\"0.000000\"/><MinSpeed value=\"0.000000\"/><MaxSpeed value=\"200.000000\"/><NumConnections value=\"3\"/></item>");
    TestRest(restCommand::REST_POST, "Parser/MAP/BinMap?insert=TestHash4"	, REST_CREATED, "<item type=\"SimpleStruct\"><TestEnum>BONEMASK_ARMONLY_L</TestEnum><Position x=\"0.000000\" y=\"0.000000\" z=\"0.000000\"/><MinSpeed value=\"0.000000\"/><MaxSpeed value=\"200.000000\"/><NumConnections value=\"3\"/></item>");

    TestRest(restCommand::REST_POST, "Parser/MAP/Map?insert=TestHash4"	    , REST_FORBIDDEN, "<item type=\"SimpleStruct\"><TestEnum>BONEMASK_ARMONLY_R</TestEnum><Position x=\"0.000000\" y=\"0.000000\" z=\"0.000000\"/><MinSpeed value=\"0.000000\"/><MaxSpeed value=\"200.000000\"/><NumConnections value=\"3\"/></item>");
    TestRest(restCommand::REST_POST, "Parser/MAP/BinMap?insert=TestHash4"	, REST_FORBIDDEN, "<item type=\"SimpleStruct\"><TestEnum>BONEMASK_ARMONLY_L</TestEnum><Position x=\"0.000000\" y=\"0.000000\" z=\"0.000000\"/><MinSpeed value=\"0.000000\"/><MaxSpeed value=\"200.000000\"/><NumConnections value=\"3\"/></item>");
#endif

	while(g_runWebServer)
	{
		REST.Update();
		sysIpcSleep(33); 
	}

	REST.Shutdown();

	SHUTDOWN_PARSER;


	return 0;
}
