// 
// sample_parser/sample_numericarray.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "parser/manager.h"
#include "parser/visitorxml.h"
#include "system/main.h"
#include "system/param.h"

#include "atl/binmap.h"

#include "objects_container.h"

using namespace rage;

PARAM(xmloutput, "Name of the XML formatted output file");
PARAM(rbfoutput, "Name of the RBF formatted output file");

////////////////////////////////////////////////////////
// TITLE: sample_numericarrays - Reading and writing arrays of numeric data with the parser
// PURPOSE: This sample builds on sample_struct and shows how you can
//		read and write arrays of numeric data

// STEP #1. Set up your objects for parsing.
// -- Add a PARSE= line to your makefile.bat. In this case PARSE=objects_container
// -- Define the parsable classes in objects_container.h
// -- Declare the structure metadata (the 'structdef' xml tags) in objects_container.cpp
// -- #include the generated code (objects_container_parser.h) in objects_container.cpp
// -STOP

int Main()
{
	// STEP #2. Initialize the parser. All the classes we'll be using are automatically
	// registered at this time.
	INIT_PARSER;

	// -STOP

	// STEP #3. Create a new object containing arrays of numeric data, and populate those arrays
	NumericArrays arr;

	arr.m_UnsignedBytes.Grow() = 0;
	arr.m_UnsignedBytes.Grow() = 128;
	arr.m_UnsignedBytes.Grow() = UCHAR_MAX;

	arr.m_SignedBytes.Grow() = 0;
	arr.m_SignedBytes.Grow() = -1;
	arr.m_SignedBytes.Grow() = CHAR_MAX;
	arr.m_SignedBytes.Grow() = CHAR_MIN;

	arr.m_UnsignedShorts.Grow() = 0;
	arr.m_UnsignedShorts.Grow() = 32768;
	arr.m_UnsignedShorts.Grow() = USHRT_MAX;

	arr.m_SignedShorts.Grow() = 0;
	arr.m_SignedShorts.Grow() = -1;
	arr.m_SignedShorts.Grow() = SHRT_MAX;
	arr.m_SignedShorts.Grow() = SHRT_MIN;
	// -- See the sample for more
	// -STOP

	arr.m_UnsignedInts.Grow() = 0;
	arr.m_UnsignedInts.Grow() = 2147483648U;
	arr.m_UnsignedInts.Grow() = UINT_MAX;

	arr.m_SignedInts.Grow() = 0;
	arr.m_SignedInts.Grow() = -1;
	arr.m_SignedInts.Grow() = INT_MAX;
	arr.m_SignedInts.Grow() = INT_MIN;

	arr.m_Floats.Grow() = 0.0f;
	arr.m_Floats.Grow() = 1.0f;
	arr.m_Floats.Grow() = FLT_MAX;
	arr.m_Floats.Grow() = FLT_MIN;
	arr.m_Floats.Grow() = PI;
	arr.m_Floats.Grow() = 1.0f / 3.0f;

	arr.m_Vector2s.Grow().Set(0.0f, 0.0f);
	arr.m_Vector2s.Grow().Set(-1.0f, 1.0f);
	arr.m_Vector2s.Grow().Set(M_SQRT2, M_SQRT2);
	arr.m_Vector2s.Grow().Set(FLT_MAX, FLT_MIN);

	arr.m_Vector3s.Grow().Set(0.0f, 0.0f, 0.0f);
	arr.m_Vector3s.Grow().Set(1.0f, -1.0f, 0.1f);
	arr.m_Vector3s.Grow().Set(1.0f, 0.1f, 0.01f);
	arr.m_Vector3s.Grow().Set(0.01f, 0.02f, 0.03f);
	arr.m_Vector3s.Grow().Set(1.0f, 10.0f, 100.0f);
	arr.m_Vector3s.Grow().Set(1000.0f, 10000.0f, 100000.0f);
	arr.m_Vector3s.Grow().Set(1000000.0f, 10000000.0f, 100000000.0f);
	arr.m_Vector3s.Grow().Set(1.0f, 11.0f, 101.0f);
	arr.m_Vector3s.Grow().Set(1001.0f, 10001.0f, 100001.0f);
	arr.m_Vector3s.Grow().Set(1000001.0f, 10000001.0f, 100000001.0f);
	arr.m_Vector3s.Grow().Set(M_SQRT3, M_SQRT3, M_SQRT3);
	arr.m_Vector3s.Grow().Set(FLT_MAX, FLT_MIN, 0.00000000000001f);

	arr.m_Vector4s.Grow().Set(0.0f, 0.0f, 0.0f, 0.0f);
	arr.m_Vector4s.Grow().Set(1.0f, -1.0f, 0.1f, -0.1f);
	arr.m_Vector4s.Grow().Set(1.0f, 0.1f, 0.01f, 0.001f);
	arr.m_Vector4s.Grow().Set(0.01f, 0.02f, 0.03f, 0.04f);
	arr.m_Vector4s.Grow().Set(M_SQRT2, M_SQRT2, M_SQRT2, M_SQRT2);
	arr.m_Vector4s.Grow().Set(FLT_MAX, FLT_MIN, 0.00000000000001f, -0.00000000000001f);
	arr.m_Vector4s.Grow().Set(0.0f, 1.0f, 10.0f, 100.0f);
	arr.m_Vector4s.Grow().Set(0.0f, 1000.0f, 10000.0f, 100000.0f);
	arr.m_Vector4s.Grow().Set(0.0f, 1000000.0f, 10000000.0f, 100000000.0f);
	arr.m_Vector4s.Grow().Set(0.0f, 1.0f, 11.0f, 101.0f);
	arr.m_Vector4s.Grow().Set(0.0f, 1001.0f, 10001.0f, 100001.0f);
	arr.m_Vector4s.Grow().Set(0.0f, 1000001.0f, 10000001.0f, 100000001.0f);

	arr.m_Vec2Vs.Grow() = Vec2V(0.0f, 0.0f);
	arr.m_Vec2Vs.Grow() = Vec2V(-1.0f, 1.0f);
	arr.m_Vec2Vs.Grow() = Vec2V((float)M_SQRT2, (float)M_SQRT2);
	arr.m_Vec2Vs.Grow() = Vec2V(FLT_MAX, FLT_MIN);

	arr.m_Vec3Vs.Grow() = Vec3V(0.0f, 0.0f, 0.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(1.0f, -1.0f, 0.1f);
	arr.m_Vec3Vs.Grow() = Vec3V(1.0f, 0.1f, 0.01f);
	arr.m_Vec3Vs.Grow() = Vec3V(0.01f, 0.02f, 0.03f);
	arr.m_Vec3Vs.Grow() = Vec3V(1.0f, 10.0f, 100.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(1000.0f, 10000.0f, 100000.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(1000000.0f, 10000000.0f, 100000000.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(1.0f, 11.0f, 101.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(1001.0f, 10001.0f, 100001.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(1000001.0f, 10000001.0f, 100000001.0f);
	arr.m_Vec3Vs.Grow() = Vec3V(M_SQRT3, M_SQRT3, M_SQRT3);
	arr.m_Vec3Vs.Grow() = Vec3V(FLT_MAX, FLT_MIN, 0.00000000000001f);

	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 0.0f, 0.0f, 0.0f);
	arr.m_Vec4Vs.Grow() = Vec4V(1.0f, -1.0f, 0.1f, -0.1f);
	arr.m_Vec4Vs.Grow() = Vec4V(1.0f, 0.1f, 0.01f, 0.001f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.01f, 0.02f, 0.03f, 0.04f);
	arr.m_Vec4Vs.Grow() = Vec4V((float)M_SQRT2, (float)M_SQRT2, (float)M_SQRT2, (float)M_SQRT2);
	arr.m_Vec4Vs.Grow() = Vec4V(FLT_MAX, FLT_MIN, 0.00000000000001f, -0.00000000000001f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 1.0f, 10.0f, 100.0f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 1000.0f, 10000.0f, 100000.0f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 1000000.0f, 10000000.0f, 100000000.0f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 1.0f, 11.0f, 101.0f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 1001.0f, 10001.0f, 100001.0f);
	arr.m_Vec4Vs.Grow() = Vec4V(0.0f, 1000001.0f, 10000001.0f, 100000001.0f);

	arr.m_Matrix34s.Grow().Identity();
	Matrix34& mtx = arr.m_Matrix34s.Grow();
	mtx.Identity();
	mtx.RotateLocalX(0.4f);
	mtx.RotateLocalZ(1.0f);
	
	Matrix34& mtx2 = arr.m_Matrix34s.Grow();
	mtx2.Identity();
	mtx2.Translate(100.0f, -103.0f, 234.0f);

	arr.m_Mat34Vs.Grow() = Mat34V(V_ZERO);
	arr.m_Mat34Vs.Grow() = Mat34V(V_IDENTITY);
	Mat34V& mv = arr.m_Mat34Vs.Grow();
	Mat34VFromEulersXYZ(mv, Vec3V(0.4f, 0.0f, 1.0f));
	Mat34V& mv2 = arr.m_Mat34Vs.Grow();
	Mat34VFromTranslation(mv2, Vec3V(100.0f, -103.0f, 234.0f));
	

	arr.m_BoolVs.Grow() = BoolV(V_FALSE);
	arr.m_BoolVs.Grow() = BoolV(V_TRUE);
	arr.m_BoolVs.Grow() = BoolV(V_TRUE);
	arr.m_BoolVs.Grow() = BoolV(V_FALSE);

	arr.m_ScalarVs.Grow() = ScalarV(V_ONE);
	arr.m_ScalarVs.Grow() = ScalarV(V_FLT_MAX);
	arr.m_ScalarVs.Grow() = ScalarV(V_ZERO);
	arr.m_ScalarVs.Grow() = ScalarV(V_FLT_SMALL_6);

	arr.m_VecBoolVs.Grow() = VecBoolV(V_T_F_F_F);
	arr.m_VecBoolVs.Grow() = VecBoolV(V_T_T_F_F);
	arr.m_VecBoolVs.Grow() = VecBoolV(V_T_F_T_F);
	arr.m_VecBoolVs.Grow() = VecBoolV(V_T_F_F_T);

	// STEP #4. Use the parser for serialization

	const char* xmlOutName = "numericarrays.xml";
	PARAM_xmloutput.Get(xmlOutName);

	const char* rbfOutName = "numericarrays.rbf";
	PARAM_rbfoutput.Get(rbfOutName);

	// -- Save the object in XML format
	PARSER.SaveObject(xmlOutName, "xml", &arr);

	fiStream* xmlstream = ASSET.Create("numericarrays_directxml", "xml");
	parXmlWriterVisitor xmlWriter(xmlstream);
	xmlWriter.Visit(arr);
	xmlstream->Close();

	// -- Save the object in RBF format
	PARSER.SaveObject(rbfOutName, "rbf", &arr, parManager::BINARY);

	// -- Load data from the XML file into a new NumericArrays object
	NumericArrays arr2;
	PARSER.LoadObject(xmlOutName, "xml", arr2);
	// -STOP
	
	// STEP #5. Cleanup
	SHUTDOWN_PARSER;
	// -STOP

	return 0;
}

