// 
// sample_parser/sample_widgets.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "objects_container.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "parser/manager.h"
#include "parser/visitortree.h"
#include "parser/visitorwidgets.h"
#include "parser/visitorxml.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"

// Dummy function, to make things compile.
#if 0
bool DerivedArray::VirtualRead(void* , ::rage::parVirtualReadData& )
{
	return false;
}
#endif

PARAM(file, "unused (sample_rmcore requires this)");

namespace ragesamples {

using namespace rage;


///////////////////////////////////////////////////////////////
// TITLE: sample_bigstructure - Showing some large structures created with the parser
// PURPOSE:
//		This samples uses structures that contain many examples of parsable data, so you can
//		see how the data is loaded and saved, and what the widgets created from the data look
//		like.

class widgetSampleManager : public rmcSampleManager
{
public:

	// STEP #1. Create callbacks for the load and save buttons
	// -- These callbacks just load and save the m_DataContainer object with the widgetdata.xml file
	void Load() {
		PARSER.InitObject(m_DataContainer);
		PARSER.LoadObject("bigstruct", "xml", m_DataContainer);
	}

	void Save() {
		PARSER.SaveObject("bigstruct", "xml", &m_DataContainer);
	}

	void SaveVisitor()
	{
		parBuildTreeVisitor treeVis(PARSER.Settings());
		treeVis.Visit(m_DataContainer);
		PARSER.SaveTree("bigstruct_treevis", "xml", &treeVis.m_Tree);
	}

	void SaveXml()
	{
		fiStream* xmlstream = ASSET.Create("bigstruct_xmlvis", "xml");
		if (xmlstream)
		{
			parXmlWriterVisitor xmlVis(xmlstream);
			xmlVis.Visit(m_DataContainer);
			xmlstream->Close();
		}
		else
		{
			Errorf("Couldn't open file");
		}
	}

typedef atBinaryMap<Vec3V, u32>::DataPair dataPair;


	// STEP #2. Initialize the objects and create the widgets
	// -- The InitClient() function is called from grcSampleManager::Init
	virtual void InitClient()
	{
		ASSET.PushFolder("sample_parser");

		PARSER.InitObject(m_DataContainer);
		PARSER.SaveObject("X:\\datacontainer.xml", "", &m_DataContainer);

		// -- Load the initial data into m_DataContainer
//		Load();

		m_DataContainer.m_SimpleData.m_EnumAsU32 = AllSimpleData::EIGHT;
		m_DataContainer.m_SimpleData.m_EnumAsU8 = AllSimpleData::TWELVE;

		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::ONE);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::TWO);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::THREE);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::FOUR);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::FIVE);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::SIX);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::EIGHT);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::NINE);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::TEN);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::TWELVE);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::THIRTEEN);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::FOURTEEN);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::FIFTEEN);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::SIXTEEN);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::SEVENTEEN);
		m_DataContainer.m_SimpleData.m_FixedBitset.Set(AllSimpleData::EIGHTEEN);

		m_DataContainer.m_SimpleData.m_VariableBitset.Init(50);
		m_DataContainer.m_SimpleData.m_VariableBitset.Set(AllSimpleData::THREE);
		m_DataContainer.m_SimpleData.m_VariableBitset.Set(AllSimpleData::FOUR);
		m_DataContainer.m_SimpleData.m_VariableBitset.Set(AllSimpleData::FIVE);
		m_DataContainer.m_SimpleData.m_VariableBitset.Set(AllSimpleData::THIRTEEN);
		m_DataContainer.m_SimpleData.m_VariableBitset.Set(AllSimpleData::FOURTEEN);
		m_DataContainer.m_SimpleData.m_VariableBitset.Set(AllSimpleData::FIFTEEN);
													   
		SaveVisitor();								   
		SaveXml();									   

		// -- Here we create a bank for the parser widgets, add a load and save button,
		// and finally call AddWidgets() to create widgets for the m_DataContainer object
#if __BANK
		m_ParserBank = &BANKMGR.CreateBank("Parser Widgets");

		m_ParserBank->AddButton("Load", datCallback(MFA(widgetSampleManager::Load), this));
		m_ParserBank->AddButton("Save", datCallback(MFA(widgetSampleManager::Save), this));

		m_ParserBank->AddButton("Save Tree Visitor", datCallback(MFA(widgetSampleManager::SaveVisitor), this));
		m_ParserBank->AddButton("Save XML Visitor", datCallback(MFA(widgetSampleManager::SaveXml), this));

		m_ParserBank->PushGroup("Old Widgets");
		PARSER.AddWidgets(*m_ParserBank, &m_DataContainer);
		m_ParserBank->PopGroup();

		m_ParserBank->PushGroup("New Widgets");
		parBuildWidgetsVisitor vis;
		vis.m_CurrBank = m_ParserBank;
		vis.Visit(m_DataContainer);
		m_ParserBank->PopGroup();
#endif

	}
	// -STOP

	BigStructure	m_DataContainer;
	bkBank*			m_ParserBank;
};

}

int Main()
{
	// STEP #3. Initialize the parser, run the game, and clean up

	// -- This creates the parser singleton and registers all parsable classes
	INIT_PARSER;

	// -- Now create the main game object, initialize it, run it, and shut down.
	ragesamples::widgetSampleManager sampleWidget;
	sampleWidget.Init();
	sampleWidget.UpdateLoop();
	sampleWidget.Shutdown();

	// -- Finally shut down the parser
	SHUTDOWN_PARSER;

	// -STOP

	return 0;
}
