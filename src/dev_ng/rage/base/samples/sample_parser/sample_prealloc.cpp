// 
// sample_parser/sample_struct.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

#include "objects_hierarchy.h"

using namespace rage;

PARAM(infile1a, "Name of first input file");
PARAM(infile1b, "Name of second input file (must have same structure as first");
PARAM(infile2, "Name of third input file (may have different structure)");

////////////////////////////////////////////////////////
// TITLE: sample_prealloc - Reading and writing pre-allocated structures with the parser
// PURPOSE: This sample builds on sample_struct and shows how you can
//		allocate objects and then load data into the allocated objects rather
//		than having the parser do the allocations.

// STEP #1. Set up your objects for parsing.
// -- Add a PARSE= line to your makefile.bat. In this case PARSE=objects_hierarchy
// -- Define the parsable classes in objects_hierarchy.h
// -- Declare the structure metadata (the 'structdef' xml tags) in objects_hierarchy.cpp
// -- #include the generated code (objects_hierarchy_parser.h) in objects_hierarchy.cpp
// -STOP

int Main()
{
	// STEP #2. Initialize the parser.
	INIT_PARSER;
	// -STOP

	// STEP #3. Create the new preallocated classes.
	// In this case we use SimplePointerContainer, which has two BaseClass members.

	// -- c1 will contain one DerivedClassA and one DerivedClassB
	SimplePointerContainer c1;
	c1.m_ItemOne = rage_new DerivedClassA;
	c1.m_ItemTwo = rage_new DerivedClassB;

	// -- c2 will contain one BaseClass and one DerivedClassB
	SimplePointerContainer c2;
	c2.m_ItemOne = rage_new BaseClass;
	c2.m_ItemTwo = rage_new DerivedClassB;
	// -STOP


	// STEP #4. Use the parser on the objects

	// -- Load data into c1
	// The "ItemOne" member in the file may be a DerivedClassA or a BaseClass.
	// The "ItemTwo" member in the file may be a DerivedClassB or a BaseClass.
	const char* infile1a = "prealloc_1a.xml";
	PARAM_infile1a.Get(infile1a);
	PARSER.LoadObject(infile1a, "xml", c1);
	Printf("c1.m_ItemOne val is: %f\n", c1.m_ItemOne->m_BaseFloat);

	// -- "reload" data into c1
	const char* infile1b = "prealloc_1b.xml";
	PARAM_infile1b.Get(infile1b);
	PARSER.LoadObject(infile1b, "xml", c1);
	Printf("c1.m_ItemOne val is: %f\n", c1.m_ItemOne->m_BaseFloat);

	// -- Load data into c2
	// The "ItemOne" member in the file must be a BaseClass. It may not be DerivedClassA.
	// The "ItemTwo" member in the file may be a DerivedClassB or a BaseClass.
	const char* infile2 = "prealloc_2a.xml";
	PARAM_infile2.Get(infile2);
	PARSER.LoadObject(infile2, "xml", c2);
	// -STOP

	// STEP #5. Cleanup
	SHUTDOWN_PARSER;
	// -STOP

	return 0;
}
