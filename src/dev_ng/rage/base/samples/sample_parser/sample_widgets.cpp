// 
// sample_parser/sample_widgets.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "objects_simple.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "parser/manager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"

namespace ragesamples {

using namespace rage;

///////////////////////////////////////////////////////////////
// TITLE: sample_widgets - Creating widgets with the parser
// PURPOSE:
//		This samples shows how to create widgets with the parser. It also shows
//		an example of using the parser with a game loop. This sample uses
//		some of the parsable objects defined in sample_struct

class widgetSampleManager : public grcSampleManager
{
public:

	// STEP #1. Create callbacks for the load and save buttons
	// -- These callbacks just load and save the m_DataContainer object with the widgetdata.xml file
	void Load() {
		PARSER.InitObject(m_DataContainer);
		PARSER.LoadObject("widgetdata", "xml", m_DataContainer);
	}

	void Save() {
		PARSER.SaveObject("widgetdata", "xml", &m_DataContainer);
	}

	// STEP #2. Initialize the objects and create the widgets
	// -- The InitClient() function is called from grcSampleManager::Init
	virtual void InitClient()
	{
		ASSET.PushFolder("sample_parser");

		// -- Load the initial data into m_DataContainer
		Load();

		// -- Here we create a bank for the parser widgets, add a load and save button,
		// and finally call AddWidgets() to create widgets for the m_DataContainer object
#if __BANK
		m_ParserBank = &BANKMGR.CreateBank("Parser Widgets");

		m_ParserBank->AddButton("Load", datCallback(MFA(widgetSampleManager::Load), this));
		m_ParserBank->AddButton("Save", datCallback(MFA(widgetSampleManager::Save), this));

		PARSER.AddWidgets(*m_ParserBank, &m_DataContainer);
#endif

	}
	// -STOP

	SimpleContainer m_DataContainer;
	bkBank*			m_ParserBank;
};

}

int Main()
{
	// STEP #3. Initialize the parser, run the game, and clean up

	// -- This creates the parser singleton and registers all parsable classes
	INIT_PARSER;

	// -- Now create the main game object, initialize it, run it, and shut down.
	ragesamples::widgetSampleManager sampleWidget;
	sampleWidget.Init();
	sampleWidget.UpdateLoop();
	sampleWidget.Shutdown();

	// -- Finally shut down the parser
	SHUTDOWN_PARSER;

	// -STOP

	return 0;
}
