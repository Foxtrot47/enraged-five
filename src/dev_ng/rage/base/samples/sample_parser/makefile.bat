set ARCHIVE=sample_parser

set LIBS=%RAGE_CORE_LIBS% sample_parser

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples

set TESTERS=sample_push sample_pull sample_tree sample_write sample_struct sample_rtstruct sample_pointerarray sample_prealloc sample_widgets
set TESTERS=%TESTERS% sample_numericarray sample_visitor sample_bigstructure sample_multipleinheritance sample_datasource sample_datainheritance
set TESTERS=%TESTERS% sample_psosave sample_psoload
set TESTERS=%TESTERS% sample_rest 
set TESTERS=%TESTERS% sample_schema_unittests


REM set LIBS_sample_pointerarray=%LIBS% input
REM set LIBS_sample_numericarray=%LIBS% input

set LIBS_sample_widgets=%LIBS% %RAGE_GFX_LIBS% %RAGE_SAMPLE_GRCORE_LIBS% sample_rmcore
set LIBS_sample_bigstructure=%LIBS% %RAGE_GFX_LIBS% %RAGE_SAMPLE_GRCORE_LIBS% sample_rmcore




