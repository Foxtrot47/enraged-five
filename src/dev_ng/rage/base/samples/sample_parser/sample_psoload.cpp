// 
// sample_parser/sample_psosave.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "objects_simple.h"
#include "objects_container.h"

#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/psoschemabuilder.h"
#include "parser/rtstructure.h"
#include "parser/visitorutils.h"
#include "string/unicode.h"
#include "system/main.h"
#include "system/timer.h"

#include "objects_hierarchy.h"

using namespace rage;


int Main()
{
	INIT_PARSER;

	// Load up a bunch of data
	BigStructure bs;
	PARSER.LoadObject("bigstruct.xml", "", bs);

	// Save out an XML reference of the data
	char path[RAGE_MAX_PATH];
	formatf(path, "C:\\psoload.%c.xml", g_sysPlatform);
	PARSER.SaveObject(path, "", &bs);

	// Save it as a PSO file
	char psopath[RAGE_MAX_PATH];
	formatf(psopath, "C:\\psoload.%c.pso", g_sysPlatform);
//	psoSaveObject(psopath, &bs);

	psoFile* objectFile;
	// Load the PSO, save out the "round trip" XML data using the fast loader
	/*
	objectFile = psoLoadFile(psopath);
	Assert(objectFile);

	if (objectFile)
	{
		BigStructure* obj = NULL;
		psoLoadObjectPtr(*objectFile, obj);

		formatf(path, "C:/psoload_rt_fast.%c.xml", g_sysPlatform);
		PARSER.SaveObject(path, "", obj);

		delete obj;
		delete objectFile;
	}*/

	// Load the PSO, save out the "round trip" XML data using the slow loader
	objectFile = psoLoadFile(psopath, PSOLOAD_NO_PREP);
	Assert(objectFile);

	if (objectFile)
	{
		BigStructure* obj = NULL;
		psoLoadObjectPtr(*objectFile, obj);

		formatf(path, "C:/psoload_rt_slow.%c.xml", g_sysPlatform);
		PARSER.SaveObject(path, "", obj);

		delete obj;
		delete objectFile;
	}

	SHUTDOWN_PARSER;

	return 0;
}
