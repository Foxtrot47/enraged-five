// 
// sample_parser/sample_datasource.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/map.h"
#include "atl/hashstring.h"
#include "parser/manager.h"
#include "parser/datasource.h"
#include "system/main.h"

#include "objects_hierarchy.h"
#include "objects_container.h"

using namespace rage;

/*//////////////////////////////////////////////
// The class hierarchy looks like this:
//
//                        BaseClass
//                     /      |      \						//
//                    /       |       \						//
//                   /        |        \					//
//        DerivedClassA DerivedClassB AbstractClass
//                                         |
//                                         |
//                                    ConcreteSubclass
*/


atMap<const char*, BaseClass*> g_SimpleMap;
atMap<const char*, AbstractClass*> g_SimpleMap2;

bool ValueFromHash(const atHashString& s, void*& result)
{
	void** res = reinterpret_cast<void**>(g_SimpleMap.Access(s.GetCStr()));
	if (res) {
		result = *res;
		return true;
	}
	return false;
}

bool NameFromValue(const void* value, atHashString& s)
{
	atMap<const char*, BaseClass*>::Iterator iter = g_SimpleMap.CreateIterator();
	for(; !iter.AtEnd(); ++iter)
	{
		if (iter.GetData() == value)
		{
			s.SetFromString(iter.GetKey());
			return true;
		}
	}
	return false;
}

bool ValueFromHash2(const atHashString& s, void*& result)
{
	void** res = reinterpret_cast<void**>(g_SimpleMap2.Access(s.GetCStr()));
	if (res) {
		result = *res;
		return true;
	}
	return false;
}


bool NameFromValue2(const void* value, atHashString& s)
{
	atMap<const char*, AbstractClass*>::Iterator iter = g_SimpleMap2.CreateIterator();
	for(; !iter.AtEnd(); ++iter)
	{
		if (iter.GetData() == value)
		{
			s.SetFromString(iter.GetKey());
			return true;
		}
	}
	return false;
}

bool EmptyDataSource(const atHashString& /*s*/, void*& /*result*/)
{
	return false;
}

parDataSourceId InitSimpleMap()
{
	parDataSource s(BaseClass::parser_GetStaticStructure());
	s.SetName("SimpleMap");
	s.SetFindValueFromHashDeleg(parDataSource::ValueFromHashDeleg(&ValueFromHash));
	s.SetFindNameFromValueDeleg(parDataSource::NameFromValueDeleg(&NameFromValue));
	return PARDATA.AddSource(s);
}

parDataSourceId InitSimpleMap2()
{
	parDataSource s2(AbstractClass::parser_GetStaticStructure());
	s2.SetName("SimpleMap2");
	s2.SetFindValueFromHashDeleg(parDataSource::ValueFromHashDeleg(&ValueFromHash2));
	s2.SetFindNameFromValueDeleg(parDataSource::NameFromValueDeleg(&NameFromValue2));
	return PARDATA.AddSource(s2);
}

parDataSourceId InitEmptySource()
{
	parDataSource s(BaseClass::parser_GetStaticStructure());
	s.SetName("EmptyMap");
	s.SetFindValueFromHashDeleg(parDataSource::ValueFromHashDeleg(&EmptyDataSource));
	return PARDATA.AddSource(s);
}

int Main()
{
	INIT_PARSER;

	// Test: init and shut down manager
	{
		parDataSourceManager::InitClass();
		parDataSourceManager::ShutdownClass();
	}

	// Test: init, query, find nothing, shutdown
	{
		parDataSourceManager::InitClass();
		parDataQuery q;
		Assert(!q.IsWellFormed());
		parDataQueryResult res = PARDATA.Find(q);
		Assert(!res.IsValid());
		parDataSourceManager::ShutdownClass();
	}

	// Test: real query, find nothing
	{
		parDataSourceManager::InitClass();

		parDataQuery q;
		q.Name("Test");
		q.Source(parDataUtils::PARDATA_ANY_SOURCE);
		q.Type<BaseClass>();

		Assert(q.IsWellFormed());
//		parDataQueryResult res = PARDATA.Find(q);
//		Assert(!res.IsValid());

		parDataSourceManager::ShutdownClass();
	}

	// Test: add a data source and shut down
	{
		parDataSourceManager::InitClass();

		parDataSourceId id = InitSimpleMap();

		Assert(parDataUtils::IsConcreteSource(id));

		parDataSourceManager::ShutdownClass();
	}

	// Test: add a data source, and remove it
	{
		parDataSourceManager::InitClass();

		parDataSourceId id = InitSimpleMap();
		Assert(parDataUtils::IsConcreteSource(id));
		PARDATA.RemoveSource(id);

		parDataSourceManager::ShutdownClass();
	}


	// Test: add a data source, do a (failing) query
	{
		parDataSourceManager::InitClass();

		parDataSourceId id = InitSimpleMap();
		Assert(parDataUtils::IsConcreteSource(id));

		parDataQuery q;
		q .Type<BaseClass>() .Name("Test");

		parDataQueryResult res = PARDATA.Find(q);
		Assert(!res.IsValid());

		PARDATA.RemoveSource(id);

		parDataSourceManager::ShutdownClass();
	}

	// Now actually add some data to the data source!
	BaseClass *sc1, *sc2;
	AbstractClass* sc3;
	PARSER.LoadObjectPtr("structin1.xml", "", sc1);
	PARSER.LoadObjectPtr("structin2.xml", "", sc2);
	PARSER.LoadObjectPtr("structin3.xml", "", sc3);

	g_SimpleMap.Insert("One", sc1);
	g_SimpleMap.Insert("Two", sc2);
	g_SimpleMap.Insert("Three", sc3);

	g_SimpleMap2.Insert("III", sc3);

	// Test: add a data source, do a (successful) query
	{
		parDataSourceManager::InitClass();

		parDataSourceId id = InitSimpleMap();
		Assert(parDataUtils::IsConcreteSource(id));

		parDataQuery q;
		q .Type<BaseClass>() .Name("One");

		parDataQueryResult res = PARDATA.Find(q);
		Assert(res.IsValid());

		parDataSourceManager::ShutdownClass();
	}

	// Test: add two data sources, do a (successful) query
	{
		parDataSourceManager::InitClass();

		parDataSourceId id = InitSimpleMap();
		Assert(parDataUtils::IsConcreteSource(id));

		parDataSourceId id2 = InitSimpleMap2();
		Assert(parDataUtils::IsConcreteSource(id2));

		parDataQuery q;
		q .Type<BaseClass>() .Name("One");

		parDataQueryResult res = PARDATA.Find(q);
		Assert(res.IsValid());

		parDataSourceManager::ShutdownClass();
	}

	// Test: add two data sources for the same type, do a (successful) query
	{
		parDataSourceManager::InitClass();

		parDataSourceId id = InitSimpleMap();
		Assert(parDataUtils::IsConcreteSource(id));

		parDataSourceId id2 = InitEmptySource();
		Assert(parDataUtils::IsConcreteSource(id2));

		parDataQuery q;
		q .Type<BaseClass>() .Name("One");

		parDataQueryResult res = PARDATA.Find(q);
		Assert(res.IsValid());

		parDataSourceManager::ShutdownClass();
	}

	// Test: add three data sources, do a few queries
	{
		parDataSourceManager::InitClass();

		parDataSourceId id1 = InitSimpleMap();
		parDataSourceId id2 = InitSimpleMap2();

		InitEmptySource();

		parDataQuery q;

		q .Type<AbstractClass>() .Name("One");
		parDataQueryResult res = PARDATA.Find(q);
		Assert(!res.IsValid());						// "One" is in a base class, we're only searching AbstactClass sources

		q .Type<AbstractClass>() .Name("One") .Source(parDataUtils::PARDATA_ANY_SOURCE);
		res = PARDATA.Find(q);
		Assert(!res.IsValid());						// "One" is a BaseClass instance, not an AbstractClass instance

		q .Type<AbstractClass>() .Name("III") .Source(parDataUtils::PARDATA_ANY_EXACT_SOURCE);
		res = PARDATA.Find(q);
		Assert(res.IsValid() && res.m_DataSourceId == id2);		// Now we're talking. "III" is a ConcreteSubclass, but its in an AbstractClass data source

		q .Type<ConcreteSubclass>() .Name("III") .Source(parDataUtils::PARDATA_ANY_SOURCE);
		res = PARDATA.Find(q);
		Assert(res.IsValid() && res.m_DataSourceId == id2);		// This works too - even though there aren't really any ConcreteSubclass data sources

		q .Type<DerivedClassA>() .Name("Two") .Source(parDataUtils::PARDATA_ANY_EXACT_OR_DERIVED_SOURCE);
		res = PARDATA.Find(q);
		Assert(!res.IsValid());						// Nope. "Two" is a DerivedClassA, but there are no DerivedClassA data sources

		q .Type<DerivedClassA>() .Name("Two") .Source(parDataUtils::PARDATA_ANY_SOURCE);
		res = PARDATA.Find(q);
		Assert(res.IsValid() && res.m_DataSourceId == id1);		// There we go. Found "Two" in data source 1.

		parDataSourceManager::ShutdownClass();
	}

	// Test: Same as above, but uses FindSimple
	{
		parDataSourceManager::InitClass();

		InitSimpleMap();
		InitSimpleMap2();
		InitEmptySource();

		AbstractClass* ac1 = PARDATA.FindSimple<AbstractClass>("One");
		Assert(!ac1);						// "One" is in a base class, we're only searching AbstactClass sources

		AbstractClass* ac2 = PARDATA.FindSimple<AbstractClass>("One", parDataUtils::PARDATA_ANY_SOURCE);
		Assert(!ac2);						// "One" is a BaseClass instance, not an AbstractClass instance

		AbstractClass* ac3 = PARDATA.FindSimple<AbstractClass>("III", parDataUtils::PARDATA_ANY_EXACT_SOURCE);
		Assert(ac3);						// Now we're talking. "III" is a ConcreteSubclass, but its in an AbstractClass data source

		ConcreteSubclass* cs1 = PARDATA.FindSimple<ConcreteSubclass>("III", parDataUtils::PARDATA_ANY_SOURCE);
		Assert(cs1);						// This works too - even though there aren't really any ConcreteSubclass data sources

		DerivedClassA* dca1 = PARDATA.FindSimple<DerivedClassA>("Two", parDataUtils::PARDATA_ANY_EXACT_OR_DERIVED_SOURCE);
		Assert(!dca1);						// Nope. "Two" is a DerivedClassA, but there are no DerivedClassA data sources

		DerivedClassA* dca2 = PARDATA.FindSimple<DerivedClassA>("Two", parDataUtils::PARDATA_ANY_SOURCE);
		Assert(dca2);						// There we go. Found "Two" in data source 1.

		parDataSourceManager::ShutdownClass();
	}

	// Test: Use the parser to load instances of link containers
	{
		parDataSourceManager::InitClass();

		InitSimpleMap();
		InitSimpleMap2();
		InitEmptySource();

		LinkContainer container;
		PARSER.InitAndLoadObject("links", "xml", container);

		container.m_BasePtr = PARDATA.FindSimple<AbstractClass>("III", parDataUtils::PARDATA_ANY_SOURCE);

		PARSER.SaveObject("links_out", "xml", &container);

		parDataSourceManager::ShutdownClass();
	}

	// Test: Make sure we assign good IDs for data sources
	{
		parDataSourceManager::InitClass();

		parDataSourceId id1 = InitSimpleMap();
		parDataSourceId id2 = InitSimpleMap2();

		for(int i = 0; i < 10; i++)
		{
			for(u32 j = 0; j < parDataUtils::PARDATA_LAST_SOURCE_ID; j++)
			{
				parDataSourceId id3 = InitEmptySource();
				Assertf(parDataUtils::IsConcreteSource(id3), "Bad source ID");
				Assertf(id3 != id1, "Source ID in use");
				Assertf(id3 != id2, "Source ID in use");
				PARDATA.RemoveSource(id3);
			}
			printf("%d\t", i);
		}

		parDataSourceManager::ShutdownClass();
	}

	g_SimpleMap.Reset();
	g_SimpleMap2.Reset();

	delete sc1;
	delete sc2;
	delete sc3;

	SHUTDOWN_PARSER;
	return 0;
}