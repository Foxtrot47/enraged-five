// 
// sample_parser/sample_psosave.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "objects_simple.h"
#include "objects_container.h"

#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/psoparserbuilder.h"
#include "parser/rtstructure.h"
#include "parser/visitorutils.h"
#include "string/unicode.h"
#include "system/main.h"

#include "objects_hierarchy.h"

using namespace rage;

int Main()
{
	INIT_PARSER;

	atFinalHashString foo("foo");
	atFinalHashString bar("bar");

	atLiteralHashString lfoo("foo");
	atLiteralHashString lbar("bar");

	atHashString dfoo("foo");
	atHashString dbar("bar");

	SimpleStruct ss;
	ss.m_Position = Vector3(30.0, 20.0f, 10.0f);
	ss.m_Location.Identity();
	ss.m_MinSpeed = 0.001f;
	ss.m_MaxSpeed = 100.0f;
	ss.m_Name = StringDuplicate("My ASCII Name!");
	ss.m_WideName = WideStringDuplicate(_C16("My Wide Name!"));
	ss.m_NumConnections = -1;
	ss.m_TestEnum = SimpleStruct::BONEMASK_ARMONLY_L;

	psoSaveObject("sample.pso", &ss);

	SHUTDOWN_PARSER;

	return 0;
}
