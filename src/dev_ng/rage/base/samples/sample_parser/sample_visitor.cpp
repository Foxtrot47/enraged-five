
// 
// sample_parser/sample_iterator.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser/manager.h"
#include "parser/visitorxml.h"

#include "file/serialize.h"
#include "system/main.h"
#include "objects_hierarchy.h"
#include "objects_simple.h"

using namespace rage;

class parHasSaveDelegateVisitor : public parInstanceVisitor
{
public:
	parHasSaveDelegateVisitor()
	{
		m_HasSaveDelegate = false;
	}

	virtual bool BeginStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata)
	{
		parStructure* structMeta = metadata.GetConcreteStructure(ptrToStruct);
		if (structMeta)
		{
			if (structMeta->HasDelegate("PreSave") || structMeta->HasDelegate("PostSave"))
			{
				m_HasSaveDelegate = true;
				EndTraversal();
				return false;
			}
		}
		return true;
	}

	virtual bool BeginToplevelStruct(parPtrToStructure /*ptrToStruct*/, parStructure& metadata)
	{
		if (metadata.HasDelegate("PreSave") || metadata.HasDelegate("PostSave"))
		{
			m_HasSaveDelegate = false;
			EndTraversal();
			return false;
		}
		return true;
	}

	bool m_HasSaveDelegate;
};

class parSimpleBinaryFormatWriter : public parInstanceVisitor
{
public:

	parSimpleBinaryFormatWriter(char* memory, int size)
	{
		char memoryFileName[RAGE_MAX_PATH];

		fiDeviceMemory::MakeMemoryFileName(memoryFileName, RAGE_MAX_PATH, memory, size, false, NULL);
		m_Stream = ASSET.Create(memoryFileName, "");

		m_File = rage_new fiSerialize(m_Stream, false, true);
	}

	~parSimpleBinaryFormatWriter()
	{
		delete m_File;
	}

	int Size()
	{
		return m_Stream->Tell();
	}

	template<typename _Type>
	void Visit(_Type& instance)
	{
		parInstanceVisitor::Visit(instance);
		m_Stream->Flush();
		m_Stream->Close();
	}

	enum TypeCode
	{
		// mostly copied from parMember
		BOOL,			// bool
		CHAR,			// s8
		UCHAR,			// u8
		SHORT,			// s16
		USHORT,			// u16
		INT,			// s32
		UINT,			// u32
		FLOAT,			// float
		VECTOR2,		// Vector2
		VECTOR3,		// Vector3
		VECTOR4,		// Vector4
		STRING,			// char*
		STRUCT,			// Another structure. Could be a nested structure or a owning pointer to another structure.
		ARRAY,			// An array. Could be an atArray, atRangeArray, raw pointer, etc.
		ENUM,			// enumerated type
		MATRIX34,		// Matrix34
		MATRIX44,		// Matrix44

		// new ones
		GROUP,			// anon. structure

		INVALID_TYPE = 255,
	};

	void WriteHeader(TypeCode typecode, const char* name)
	{
		u32 hash = atLiteralStringHash(name);
		u8 code = (u8)typecode;
		(*m_File) << hash << code;
	}

	virtual void BoolMember(bool& data, parMemberSimple& metadata)
	{
		WriteHeader(BOOL, metadata.GetName());
		(*m_File) << data;
	}

	virtual void CharMember(s8& data, parMemberSimple& metadata)
	{
		WriteHeader(CHAR, metadata.GetName());
		(*m_File) << data;
	}

	virtual void UCharMember(u8& data, parMemberSimple& metadata)
	{
		WriteHeader(UCHAR, metadata.GetName());
		(*m_File) << data;
	}

	virtual void ShortMember(s16& data, parMemberSimple& metadata)
	{
		WriteHeader(SHORT, metadata.GetName());
		(*m_File) << data;
	}

	virtual void UShortMember(u16& data, parMemberSimple& metadata)
	{
		WriteHeader(USHORT, metadata.GetName());
		(*m_File) << data;
	}

	virtual void IntMember(s32& data, parMemberSimple& metadata)
	{
		WriteHeader(INT, metadata.GetName());
		(*m_File) << data;
	}

	virtual void UIntMember(u32& data, parMemberSimple& metadata)
	{
		WriteHeader(UINT, metadata.GetName());
		(*m_File) << data;
	}

	virtual void FloatMember(float& data, parMemberSimple& metadata)
	{
		WriteHeader(FLOAT, metadata.GetName());
		(*m_File) << data;
	}

	virtual void Vector2Member(Vector2& data, parMemberVector& metadata)
	{
		WriteHeader(VECTOR2, metadata.GetName());
		(*m_File) << data;
	}

	virtual void Vector3Member(Vector3& data, parMemberVector& metadata)
	{
		WriteHeader(VECTOR3, metadata.GetName());
		(*m_File) << data;
	}

	virtual void Vector4Member(Vector4& data, parMemberVector& metadata)
	{
		WriteHeader(VECTOR4, metadata.GetName());
		(*m_File) << data;
	}

	virtual void Matrix34Member(Matrix34& data, parMemberMatrix& metadata)
	{
		WriteHeader(MATRIX34, metadata.GetName());
		(*m_File) << data;
	}

	virtual void Matrix44Member(Matrix44& data, parMemberMatrix& metadata)
	{
		WriteHeader(MATRIX44, metadata.GetName());
		(*m_File) << data;
	}

	// have to handle enum member somehow...

	virtual void StringMember(parPtrToMember, const char* ptrToString, parMemberString& metadata)
	{
		WriteHeader(STRING, metadata.GetName());
		if (!ptrToString)
		{
			int zero = 0;
			(*m_File) << zero;
		}
		else
		{
			m_File->Put(ptrToString);
		}
	}

	void WriteStructureInfo(parStructure& structmetadata)
	{
		u32 typehash = atLiteralStringHash(structmetadata.GetName());

		parVersionInfo vers = structmetadata.GetVersion();
		//		m_Stream->Write(&vers, sizeof(parVersionInfo));

		u16 major = (u16)vers.GetMajor();
		u16 minor = (u16)vers.GetMinor();

		u16 elts = (u16)structmetadata.GetNumMembers();

		(*m_File) << typehash << major << minor << elts;
	}

	virtual bool BeginStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata)
	{
		WriteHeader(STRUCT, metadata.GetName());

		parStructure* structmetadata = metadata.GetConcreteStructure(ptrToStruct);
		Assert(structmetadata);

		WriteStructureInfo(*structmetadata); 

		return true;
	}

	virtual bool BeginToplevelStruct(parPtrToStructure, parStructure& metadata)
	{
		WriteHeader(STRUCT, "Unnamed");
		WriteStructureInfo(metadata);
		return true;
	}

	virtual bool BeginArrayMember(parPtrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
	{
		WriteHeader(ARRAY, metadata.GetName());
		TypeCode subtype = INVALID_TYPE;

		switch(metadata.GetPrototypeMember()->GetType())
		{
		case parMemberType::TYPE_BOOL:		subtype = BOOL;			break;
		case parMemberType::TYPE_CHAR:		subtype = CHAR;			break;
		case parMemberType::TYPE_UCHAR:		subtype = UCHAR;		break;
		case parMemberType::TYPE_SHORT:		subtype = SHORT;		break;
		case parMemberType::TYPE_USHORT:	subtype = USHORT;		break;
		case parMemberType::TYPE_INT:		subtype = INT;			break;
		case parMemberType::TYPE_UINT:		subtype = UINT;			break;
		case parMemberType::TYPE_FLOAT:		subtype = FLOAT;		break;
		case parMemberType::TYPE_VECTOR2:	subtype = VECTOR2;		break;
		case parMemberType::TYPE_VECTOR3:	subtype = VECTOR3;		break;
		case parMemberType::TYPE_VECTOR4:	subtype = VECTOR4;		break;
		case parMemberType::TYPE_MATRIX34:	subtype = MATRIX34;		break;
		case parMemberType::TYPE_MATRIX44:	subtype = MATRIX44;		break;
		case parMemberType::TYPE_STRING:	subtype = STRING;		break;
		case parMemberType::TYPE_STRUCT:	subtype = STRUCT;		break;
		case parMemberType::TYPE_ARRAY:		subtype = ARRAY;		break;
		// enum?
		case parMemberType::TYPE_ENUM:
		case parMemberType::TYPE_BITSET:
		case parMemberType::INVALID_TYPE: 
		default:
			Assert(0);			break;
		}

		u8 subtypeU8 = (u8)subtype;
		int elts = (u16)numElements;

		(*m_File) << subtypeU8 << elts;

		bool needsToRecurse = false;

		switch(metadata.GetPrototypeMember()->GetType())
		{
		case parMemberType::TYPE_BOOL:
		case parMemberType::TYPE_CHAR:
		case parMemberType::TYPE_UCHAR:	
			m_Stream->WriteByte(reinterpret_cast<char*>(arrayContents), (int)numElements); 
			break;
		case parMemberType::TYPE_SHORT:
		case parMemberType::TYPE_USHORT:
			m_Stream->WriteShort(reinterpret_cast<s16*>(arrayContents), (int)numElements); 
			break;
		case parMemberType::TYPE_INT:
		case parMemberType::TYPE_UINT:
			m_Stream->WriteInt(reinterpret_cast<s32*>(arrayContents), (int)numElements); 
			break;
		case parMemberType::TYPE_FLOAT:
			m_Stream->WriteFloat(reinterpret_cast<float*>(arrayContents), (int)numElements); 
			break;
		case parMemberType::TYPE_VECTOR2:
			for(u32 i = 0; i < numElements; i++) {
				(*m_File) << reinterpret_cast<Vector2*>(arrayContents)[i];
			}
			break;
		case parMemberType::TYPE_VECTOR3:
			for(u32 i = 0; i < numElements; i++) {
				(*m_File) << reinterpret_cast<Vector3*>(arrayContents)[i];
			}
			break;
		case parMemberType::TYPE_VECTOR4:
			for(u32 i = 0; i < numElements; i++) {
				(*m_File) << reinterpret_cast<Vector4*>(arrayContents)[i];
			}
			break;
		case parMemberType::TYPE_MATRIX34:
			for(u32 i = 0; i < numElements; i++) {
				(*m_File) << reinterpret_cast<Matrix34*>(arrayContents)[i];
			}
			break;
		case parMemberType::TYPE_MATRIX44:
			for(u32 i = 0; i < numElements; i++) {
				(*m_File) << reinterpret_cast<Matrix44*>(arrayContents)[i];
			}
			break;

		case parMemberType::TYPE_ENUM: // handle this somehow?
		case parMemberType::TYPE_BITSET:
			Quitf("Not implemented yet");
			break;

		case parMemberType::TYPE_ARRAY:
		case parMemberType::TYPE_STRING:
		case parMemberType::TYPE_STRUCT:
			needsToRecurse = true;
			break;

		case parMemberType::INVALID_TYPE:
		default:
			Quitf("Shouldn't get here"); 
			break;
		}

		return needsToRecurse; 
	}

	// use m_File for convenience, m_Stream when we need to
	fiSerialize* m_File;
	fiStream* m_Stream;
};

int Main()
{
	INIT_PARSER;

	SimpleContainer sc;
	PARSER.InitObject(sc);
	PARSER.LoadObject("structout", "xml", sc);

	parHasSaveDelegateVisitor visit;
	visit.Visit(sc);
	Printf("%s save callbacks\n", visit.m_HasSaveDelegate ? "Has" : "Doesn't have");

	char buf[2048];
	parSimpleBinaryFormatWriter writer(buf, 2048);
	writer.Visit(sc);
	int size = writer.Size();

	Printf("Wrote message in %d bytes\n", size);

	// for writing:
	// If has save delegates or not writing XML: build a tree, write the tree
	// else: write the XML directly

	SHUTDOWN_PARSER;

	return 0;
}
