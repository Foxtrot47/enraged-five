// 
// /sample_pull.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "system/main.h"

using namespace rage;

////////////////////////////////////////////////////////////////
// TITLE: sample_pull - Reading data with the parser - "Pull" style
// PURPOSE:
//		This sample shows how to read data from the parser using the "pull" style. This
//		style lets the user explicitly read elements from the file one at a time, and 
//		is best suited to files where the structure of the file is well defined and 
//		known in advance.

int Main()
{
	// STEP #1. Initialize the parser.
	// -- Call INIT_PARSER to create the PARSER singleton. This only needs to be done once at the beginning of a program.
	INIT_PARSER;

	// STEP #2. Open a stream for reading.
	// -- This opens "pulldata.xml" from the current directory.
	parStreamIn* s = PARSER.OpenInputStream("pulldata", "xml");

	// STEP #3. Read the beginning of an element.
	//-- Create an element object to hold the results of the read
	parElement elt;
	//-- Read data into that element object. This reads the name of the element and
	// any attributes but does not read any data or child elements.
	s->ReadBeginElement(elt);
	//-- Display the results.
	printf("Read %s\n", elt.GetName());

	// STEP #4. Read a nested leaf element.
	//-- This is the same as calling ReadBeginElement() followed by ReadEndElement()
	s->ReadLeafElement(elt);
	printf("Read vector %s %f %f %f\n", elt.GetName(), elt.FindAttribute("x")->FindFloatValue(), elt.FindAttribute("y")->FindFloatValue(), elt.FindAttribute("z")->FindFloatValue());

	// STEP #5. Read a nested element that contains raw data
	//-- First read the element
	s->ReadBeginElement(elt);
	printf("Read %s\n", elt.GetName());

	//-- Then create a data buffer, and read data into it
	char buffer[64];
	s->ReadData(buffer, 64);
	printf("Data: %s\n", buffer);

	//-- Finally skip over any remaining data and read the end of the element
	s->SkipElement();

	// STEP #6. Finish up.
	//-- Skip over any extra data in the file, and read the end of the first element we read in.
	s->SkipElement(); 

	//-- Close and delete the stream
	s->Close();
	delete s;

	//-- And finally shut down the parser
	SHUTDOWN_PARSER;
	//-STOP.

	return 0;
}
