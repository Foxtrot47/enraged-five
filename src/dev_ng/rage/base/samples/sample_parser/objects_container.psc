<ParserSchema generate="class" xmlns="http://www.rockstargames.com/RageParserSchema">
  <hinsert>
#include "objects_hierarchy.h"
#include "objects_simple.h"
  </hinsert>

  <structdef type="LinkedList">
	<float name="m_Data"/>
	<pointer name="m_Next" policy="owner" type="LinkedList"/> 
</structdef>

<structdef type="BinaryTree">
	<float name="m_Data"/>
	<pointer name="m_Left" policy="owner" type="BinaryTree"/>
	<pointer name="m_Right" policy="owner" type="BinaryTree"/>
</structdef>

<structdef type="PointerArray">
  <string name="m_Name" type="atString"/>
	<array name="m_Children" type="atArray">
		<pointer type="BaseClass" policy="owner"/>
	</array>
</structdef>

<structdef type="FixedPointerArray">
	<array name="m_Children" type="member" size="5">
		<pointer type="BaseClass" policy="owner"/>
	</array>
</structdef>

<structdef type="DerivedArray"> 
	<array name="m_Children" type="atArray">
		<struct type="DerivedClassA"/>
	</array>
  <!-- If you uncomment this, make sure you implement a VirtualRead function somewhere 
	<array name="m_OtherChildren" type="virtual" onVirtualRead="VirtualRead">
		<pointer type="BaseClass" policy="owner"/>
	</array>
  -->
</structdef>


<structdef type="AllPointers">
	<pointer name="m_OwnerPointer" type="BaseClass" policy="owner"/>
	<pointer name="m_SimpleOwnerPointer" type="SimpleStruct" policy="simple_owner"/>
<!--	<pointer name="m_ExternalNamedPointer" type="::rage::grcTexture" policy="external_named" toString="::AllPointers::TexToString" fromString="::AllPointers::StringToTex"/>
-->
</structdef>


  
<structdef type="AllArrays">
	<!-- Floats -->
	<array name="m_AtArrayFloat" type="atArray">
		<float/>
	</array>
	<array name="m_AtFixedArrayFloat" type="atFixedArray" size="5">
		<float/>
	</array>
	<array name="m_AtRangeArrayFloat" type="atRangeArray" size="5">
		<float/>
	</array>
	<array name="m_MemberArrayFloat" type="member" size="5">
		<float/>
	</array>
	<array name="m_FixedPointerArrayFloat" type="pointer" size="5">
		<float/>
	</array>
  
  <!-- Can't go in PSO files yet 
  <array name="m_VarPointerArrayFloat" type="pointer" sizeVar="m_VarPointerArrayFloatCount">
		<float/>
	</array>
  -->
  
	<!-- Structs -->
	<array name="m_AtArrayStruct" type="atArray">
		<struct type="SimpleStruct"/>
	</array>
	<array name="m_AtFixedArrayStruct" type="atFixedArray" size="5">
		<struct type="SimpleStruct"/>
	</array>
	<array name="m_AtRangeArrayStruct" type="atRangeArray" size="5">
		<struct type="SimpleStruct"/>
	</array>
	<array name="m_MemberArrayStruct" type="member" size="5">
		<struct type="SimpleStruct"/>
	</array>
	<array name="m_FixedPointerArrayStruct" type="pointer" size="5">
		<struct type="SimpleStruct"/>
	</array>
  <!-- Can't go in PSO files yet 
  <array name="m_VarPointerArrayStruct" type="pointer" sizeVar="m_VarPointerArrayStructCount">
		<struct type="SimpleStruct"/>
	</array>
  -->

	<!-- Pointers -->
	<array name="m_AtArrayPointer" type="atArray">
		<pointer policy="owner" type="BaseClass"/>
	</array>
	<array name="m_AtFixedArrayPointer" type="atFixedArray" size="5">
		<pointer policy="owner" type="BaseClass"/>
	</array>
	<array name="m_AtRangeArrayPointer" type="atRangeArray" size="5">
		<pointer policy="owner" type="BaseClass"/>
	</array>
	<array name="m_MemberArrayPointer" type="member" size="5">
		<pointer policy="owner" type="BaseClass"/>
	</array>
	<array name="m_FixedPointerArrayPointer" type="pointer" size="5">
		<pointer policy="owner" type="BaseClass"/>
	</array>
  <!-- Can't go in PSO files yet 
	<array name="m_VarPointerArrayPointer" type="pointer" sizeVar="m_VarPointerArrayPointerCount">
		<pointer policy="owner" type="BaseClass"/>
	</array>
-->
  
	<!-- 2d arrays -->
  <!-- Need to make PSO files able to read these!
	<array name="m_AtArray2d" type="atArray">
		<array type="atArray">
			<float/>
		</array>
	</array>
	<array name="m_AtFixedArray2d" type="atFixedArray" size="3">
		<array type="atFixedArray" size="3">
			<struct type="BaseClass"/>
		</array>
	</array>
	<array name="m_AtRangeArray2d" type="atRangeArray" size="3">
		<array type="atRangeArray" size="3">
			<struct type="SimpleStruct"/>
		</array>
	</array>
	<array name="m_MemberArray2d" type="member" size="3">
		<array type="member" size="3">
			<struct type="SimpleContainer"/>
		</array>
	</array>
  <array name="m_AtArrayAtFixedArray2d" type="atArray">
    <array type="atFixedArray" size="3">
      <pointer policy="owner" type="BaseClass"/>
    </array>
  </array>
  <array name="m_AtFixedArrayAtArray2d" type="atFixedArray" size="3">
    <array type="atArray">
      <pointer policy="owner" type="BaseClass"/>
    </array>
  </array>
	<array name="m_PointerArray2dFixedFixed" type="pointer" size="3">
		<array type="pointer" size="3">
			<pointer policy="owner" type="BaseClass"/>
		</array>
	</array>
  -->
  
  <!-- Can't go in PSO files yet 
	<array name="m_PointerArray2dVarFixed" type="pointer" sizeVar="m_PointerArray2dVarFixedCount">
		<array type="pointer" size="3">
			<pointer policy="owner" type="BaseClass"/>
		</array>
	</array>
	<array name="m_PointerArray2dFixedVar" type="pointer" size="3">
		<array type="pointer" sizeVar="m_PointerArray2dFixedVarCount">
			<pointer policy="owner" type="BaseClass"/>
		</array>
	</array>
  -->
</structdef>

  <enumdef type="PhoneticAlphabet::Enum">
    <enumval name="Alfa"/>
    <enumval name="Bravo"/>
    <enumval name="Charlie"/>
    <enumval name="Delta"/>
    <enumval name="Echo"/>
    <enumval name="Foxtrot"/>
    <enumval name="Golf"/>
    <enumval name="Hotel"/>
    <enumval name="India"/>
    <enumval name="Juliet"/>
  </enumdef>

  <enumdef type="Greek::Enum" values="hash">
    <enumval name="Alpha"/>
    <enumval name="Beta"/>
    <enumval name="Gamma"/>
    <enumval name="Delta"/>
    <enumval name="Epsilon"/>
  </enumdef>

  <structdef type="AllMaps">
    <map name="m_U8toU8Map" key="u8" type="atMap">
      <u8/>
    </map>
    <map name="m_U8toU8BinaryMap" key="u8" type="atBinaryMap">
      <u8/>
    </map>
    <map name="m_U32toU32Map" key="u32" type="atMap">
      <u32/>
    </map>
    <map name="m_U32toU32BinaryMap" key="u32" type="atBinaryMap">
      <u32/>
    </map>
    <map name="m_U32toVectorMap" key="u32" type="atMap">
      <Vec3V/>
    </map>
    <map name="m_U32toVectorBinaryMap" key="u32" type="atBinaryMap">
      <Vec3V/>
    </map>
    <map name="m_HashToHashMap" key="atHashString" type="atMap">
      <string type="atHashString"/>
    </map>
    <map name="m_HashToStringMap" key="atHashString" type="atMap">
      <string type="atString"/>
    </map>
    <map name="m_HashToPointerMap" key="atHashString" type="atMap">
      <pointer type="BaseClass" policy="owner"/>
    </map>
    <map name="m_Enum8ToIntBinaryMap" key="enum" enumKeyType="PhoneticAlphabet::Enum" enumKeySize="8" type="atBinaryMap">
      <int/>
    </map>
    <map name="m_Enum16ToStringBinaryMap" key="enum" enumKeyType="PhoneticAlphabet::Enum" enumKeySize="16" type="atBinaryMap">
      <string type="atString"/>
    </map>
    <map name="m_Enum32ToEnumBinaryMap" key="enum" enumKeyType="Greek::Enum" type="atBinaryMap">
      <enum type="PhoneticAlphabet::Enum"/>
    </map>
    <map name="m_MapOfArrays" key="atHashString" type="atMap">
      <array type="atArray">
        <u8/>
      </array>
    </map>
    <map name="m_MapOfStructs" key="u8" type="atMap">
      <struct type="SimpleStruct"/>
    </map>
  </structdef>

  <structdef type="AllBinMaps">
    <map name="m_U8toU8BinaryMap" key="u8" type="atBinaryMap">
      <u8/>
    </map>
    <map name="m_U32toU32BinaryMap" key="u32" type="atBinaryMap">
      <u32/>
    </map>
    <map name="m_U32toVectorBinaryMap" key="u32" type="atBinaryMap">
      <Vec3V/>
    </map>
    <map name="m_HashToHashMap" key="atHashString" type="atBinaryMap">
      <string type="atHashString"/>
    </map>
    <map name="m_HashToStringMap" key="atHashString" type="atBinaryMap">
      <string type="atString"/>
    </map>
    <map name="m_HashToPointerMap" key="atHashString" type="atBinaryMap">
      <pointer type="BaseClass" policy="owner"/>
    </map>
    <map name="m_Enum8ToIntBinaryMap" key="enum" enumKeyType="PhoneticAlphabet::Enum" enumKeySize="8" type="atBinaryMap">
      <int/>
    </map>
    <map name="m_Enum16ToStringBinaryMap" key="enum" enumKeyType="PhoneticAlphabet::Enum" enumKeySize="16" type="atBinaryMap">
      <string type="atString"/>
    </map>
    <map name="m_Enum32ToEnumBinaryMap" key="enum" enumKeyType="Greek::Enum" type="atBinaryMap">
      <enum type="PhoneticAlphabet::Enum"/>
    </map>
    <map name="m_MapOfArrays" key="atHashString" type="atBinaryMap">
      <array type="atArray">
        <u8/>
      </array>
    </map>
    <map name="m_MapOfStructs" key="u8" type="atBinaryMap">
      <struct type="SimpleStruct"/>
    </map>
  </structdef>


  <enumdef type="LotsOfBits" generate="bitset">
    <enumval name="Bit00"/>
    <enumval name="Bit01"/>
    <enumval name="Bit02"/>
    <enumval name="Bit03"/>
    <enumval name="Bit04"/>
    <enumval name="Bit05"/>
    <enumval name="Bit06"/>
    <enumval name="Bit07"/>
    <enumval name="Bit08"/>
    <enumval name="Bit09"/>
    <enumval name="Bit10"/>
    <enumval name="Bit11"/>
    <enumval name="Bit12"/>
    <enumval name="Bit13"/>
    <enumval name="Bit14"/>
    <enumval name="Bit15"/>
    <enumval name="Bit16"/>
    <enumval name="Bit17"/>
    <enumval name="Bit18"/>
    <enumval name="Bit19"/>
    <enumval name="Bit20"/>
    <enumval name="Bit21"/>
    <enumval name="Bit22"/>
    <enumval name="Bit23"/>
    <enumval name="Bit24"/>
    <enumval name="Bit25"/>
    <enumval name="Bit26"/>
    <enumval name="Bit27"/>
    <enumval name="Bit28"/>
    <enumval name="Bit29"/>
    <enumval name="Bit30"/>
    <enumval name="Bit31"/>
    <enumval name="Bit32"/>
    <enumval name="Bit33"/>
    <enumval name="Bit34"/>
    <enumval name="Bit35"/>
    <enumval name="Bit36"/>
    <enumval name="Bit37"/>
    <enumval name="Bit38"/>
    <enumval name="Bit39"/>
    <enumval name="Bit40"/>
    <enumval name="Bit41"/>
    <enumval name="Bit42"/>
    <enumval name="Bit43"/>
    <enumval name="Bit44"/>
    <enumval name="Bit45"/>
    <enumval name="Bit46"/>
    <enumval name="Bit47"/>
    <enumval name="Bit48"/>
    <enumval name="Bit49"/>
    <enumval name="Bit50"/>
    <enumval name="Bit51"/>
    <enumval name="Bit52"/>
    <enumval name="Bit53"/>
    <enumval name="Bit54"/>
    <enumval name="Bit55"/>
    <enumval name="Bit56"/>
    <enumval name="Bit57"/>
    <enumval name="Bit58"/>
    <enumval name="Bit59"/>
    <enumval name="Bit60"/>
    <enumval name="Bit61"/>
    <enumval name="Bit62"/>
    <enumval name="Bit63"/>
  </enumdef>

<structdef type="GeneratedBitsets">
  <bitset name="Simple" type="fixed"/>
  <bitset name="Simple2" type="fixed" numBits="64"/>
  <bitset name="Named_Impl" type="fixed" values="LotsOfBits"/>
  <bitset name="Named_Const" type="fixed" values="LotsOfBits" numBits="96"/>
  <bitset name="Named_Named" type="fixed" values="LotsOfBits" numBits="use_values"/>
  <bitset name="Named_Named2" type="fixed" values="AllSimpleData::SimpleEnum" numBits="AllSimpleData::TWENTY + 1"/>
  <bitset name="Generated" type="generated" values="LotsOfBits"/>
</structdef>
  
<structdef type="AllGeneratedSimpleData">
	<float name="m_Float"/>
  <Float16 name="m_Float16"/>
	<char name="m_Char"/>
	<s8 name="m_AnotherChar"/>
	<u8 name="m_UChar"/>
	<short name="m_Short"/>
	<s16 name="m_AnotherShort"/>
	<u16 name="m_UShort"/>
	<int name="m_Int"/>
	<s32 name="m_AnotherInt"/>
	<u32 name="m_UInt"/>
	<bool name="m_Bool"/>
	<Vector2 name="m_Vector2"/>
	<Vector3 name="m_Vector3" description="Old style vector"/>
	<Vector4 name="m_Vector4"/>
	<Matrix34 name="m_Matrix34"/>
	<Matrix44 name="m_Matrix44"/>
	<Vec2V name="m_Vec2V" description="New style vector"/>
	<Vec3V name="m_Vec3V" description="New style vector"/>
	<Vec4V name="m_Vec4V"/>
  <Mat33V name="m_Mat33" init="0.707f, -0.707f, 0.0f, 0.707f, 0.707f, 0.0f, 0.0f, 0.0f, 1.0f"/>
  <Mat34V name="m_Mat34" min="-100.0f" max="6000.0f"/>
  <Mat44V name="m_Mat44"/>

  <BoolV name="m_BoolV"/>
  <ScalarV name="m_ScalarV"/>
  <VecBoolV name="m_VecBoolV"/>
  
  <float name="m_Angle" type="angle"/>
	<Color32 name="m_ColorA"/>
	<u32 name="m_ColorB" type="color"/>
	<Vector3 name="m_ColorC" type="color"/>
	<Vec3V name="m_ColorE" type="color"/>

  <enum name="m_EnumAsU8" type="AllSimpleData::SimpleEnum" size="8"/>
	<enum name="m_EnumAsU16" type="AllSimpleData::SimpleEnum" size="16"/>
	<enum name="m_EnumAsU32" type="AllSimpleData::SimpleEnum"/>

	<bitset name="m_FixedBitset" type="fixed" values="AllSimpleData::SimpleEnum"/>
  <bitset name="m_FixedBitset64" type="fixed" numBits="64" values="LotsOfBits"/>
  <bitset name="m_VariableBitset" type="atBitSet" values="AllSimpleData::SimpleEnum"/>
  <bitset name="m_UnnamedVariableBitset" type="atBitSet" />

  <string name="m_PointerString" type="pointer" init="pointerString!"/>
	<string name="m_MemberString" type="member" size="32" init="MemberString"/>
	<string name="m_ConstString" type="ConstString" init="This is a const string"/>
	<string name="m_AtString" type="atString" init="AtString"/>
	<string name="m_AtWideString" type="atWideString" init="AtWideString"/>
	<string name="m_WidePointerString" type="wide_pointer" init="A wide pointer string"/>
	<string name="m_WideMemberString" type="wide_member" size="32" init="A wide member string with too many characters than we have data for!"/>
  <string name="m_HashString" type="atHashString" init="A Hash String"/>
  <string name="m_FinalHashString" type="atFinalHashString" init="A Hash String"/>
  <string name="m_HashValue" type="atHashValue" init="A Hash Value"/>
  <string name="m_HashWithStringDev" type="atHashWithStringDev"/>
  <string name="m_HashWithStringBank" type="atHashWithStringBank"/>
  <string name="m_HashWithStringNotFinal" type="atHashWithStringNotFinal"/>
  <string name="m_DiagHashString" type="atDiagHashString" init="a diag hashstring"/>
</structdef>


  <structdef type="NumericArrays">
    <array name="m_UnsignedBytes" type="atArray">
      <u8/>
    </array>
    <array name="m_SignedBytes" type="atArray">
      <s8/>
    </array>
    <array name="m_UnsignedShorts" type="atArray">
      <u16/>
    </array>
    <array name="m_SignedShorts" type="atArray">
      <s16/>
    </array>
    <array name="m_UnsignedInts" type="atArray">
      <u32/>
    </array>
    <array name="m_SignedInts" type="atArray">
      <s32/>
    </array>

    <array name="m_Floats" type="atArray">
      <float/>
    </array>
    <array name="m_Vector2s" type="atArray">
      <Vector2/>
    </array>
    <array name="m_Vector3s" type="atArray">
      <Vector3/>
    </array>
    <array name="m_Vector4s" type="atArray">
      <Vector4/>
    </array>

    <array name="m_Vec2Vs" type="atArray">
      <Vec2V/>
    </array>
    <array name="m_Vec3Vs" type="atArray">
      <Vec3V/>
    </array>
    <array name="m_Vec4Vs" type="atArray">
      <Vec4V/>
    </array>

    <!-- Numeric arrays without a packed representation -->
    <array name="m_BoolVs" type="atArray">
      <BoolV/>
    </array>

    <array name="m_ScalarVs" type="atArray">
      <ScalarV/>
    </array>

    <array name="m_Matrix34s" type="atArray">
      <Matrix34/>
    </array>
    <array name="m_Matrix44s" type="atArray">
      <Matrix44/>
    </array>
    <array name="m_Mat33Vs" type="atArray">
      <Mat33V/>
    </array>
    <array name="m_Mat34Vs" type="atArray">
      <Mat34V/>
    </array>
    <array name="m_Mat44Vs" type="atArray">
      <Mat44V/>
    </array>
    <array name="m_VecBoolVs" type="atArray">
      <VecBoolV/>
    </array>

  </structdef>

  <structdef type="FixedNumericArrays">
    <array name="m_UnsignedBytes" type="atFixedArray" size="2">
      <u8/>
    </array>
    <array name="m_SignedBytes" type="atFixedArray" size="2">
      <s8/>
    </array>
    <array name="m_UnsignedShorts" type="atFixedArray" size="2">
      <u16/>
    </array>
    <array name="m_SignedShorts" type="atFixedArray" size="2">
      <s16/>
    </array>
    <array name="m_UnsignedInts" type="atFixedArray" size="2">
      <u32/>
    </array>
    <array name="m_SignedInts" type="atFixedArray" size="2">
      <s32/>
    </array>

    <array name="m_Floats" type="atFixedArray" size="2">
      <float/>
    </array>
    <array name="m_Vector2s" type="atFixedArray" size="2">
      <Vector2/>
    </array>
    <array name="m_Vector3s" type="atFixedArray" size="2">
      <Vector3/>
    </array>
    <array name="m_Vector4s" type="atFixedArray" size="2">
      <Vector4/>
    </array>

    <array name="m_Vec2Vs" type="atFixedArray" size="2">
      <Vec2V/>
    </array>
    <array name="m_Vec3Vs" type="atFixedArray" size="2">
      <Vec3V/>
    </array>
    <array name="m_Vec4Vs" type="atFixedArray" size="2">
      <Vec4V/>
    </array>

    <array name="m_BoolVs" type="atFixedArray" size="2">
      <BoolV/>
    </array>

    <array name="m_ScalarVs" type="atFixedArray" size="2">
      <ScalarV/>
    </array>

    <!-- Numeric arrays without a packed representation -->
    <array name="m_Matrix34s" type="atFixedArray" size="2">
      <Matrix34/>
    </array>
    <array name="m_Matrix44s" type="atFixedArray" size="2">
      <Matrix44/>
    </array>
    <array name="m_Mat33Vs" type="atFixedArray" size="2">
      <Mat33V/>
    </array>
    <array name="m_Mat34Vs" type="atFixedArray" size="2">
      <Mat34V/>
    </array>
    <array name="m_Mat44Vs" type="atFixedArray" size="2">
      <Mat44V/>
    </array>

    <array name="m_VecBoolVs" type="atFixedArray" size="2">
      <VecBoolV/>
    </array>

  </structdef>

  <structdef type="NonNumericArrays">
    <array name="m_Structs" type="atArray">
      <struct type="SimpleStruct"/>
    </array>

    <array name="m_Pointers" type="atArray">
      <pointer policy="owner" type="BaseClass"/>
    </array>

    <array name="m_EnumsAsU8s" type="atArray">
      <enum name="" type="AllSimpleData::SimpleEnum" size="8"/>
    </array>

    <array name="m_EnumsAsU16s" type="atArray">
      <enum type="AllSimpleData::SimpleEnum" size="16"/>
    </array>

    <array name="m_EnumsAsU32s" type="atArray">
      <enum type="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_FixedBitsets" type="atArray">
      <bitset type="fixed" values="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_FixedBitset64s" type="atArray">
      <bitset type="fixed" numBits="64" values="LotsOfBits"/>
    </array>

    <!-- these can't go in PSO files (since they're basically 2d arrays) 
    <array name="m_VariableBitsets" type="atArray">
      <bitset type="atBitSet" values="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_UnnamedVariableBitsets" type="atArray">
      <bitset type="atBitSet" values="AllSimpleData::SimpleEnum"/>
    </array>
    -->

    <array name="m_PointerStrings" type="atArray">
      <string type="pointer"/>
    </array>

    <!-- Can't have an atArray of member strings - because we can't instantiate the atArray ResizeGrow function (no op= for char[N])
    <array name="m_MemberStrings" type="atArray">
      <string type="member" size="32"/>
    </array>
    -->

    <array name="m_ConstStrings" type="atArray">
      <string type="ConstString"/>
    </array>

    <array name="m_AtStrings" type="atArray">
      <string type="atString"/>
    </array>

    <array name="m_AtWideStrings" type="atArray">
      <string type="atWideString"/>
    </array>

    <array name="m_WidePointerStrings" type="atArray">
      <string type="wide_pointer"/>
    </array>

    <!-- Can't have an atArray of member strings - because we can't instantiate the atArray ResizeGrow function (no op= for char[N])
    <array name="m_WideMemberStrings" type="atArray">
      <string type="wide_member" size="32"/>
    </array>
    -->

  </structdef>


  <structdef type="FixedNonNumericArrays">
    <array name="m_Structs" type="atFixedArray" size="2">
      <struct type="SimpleStruct"/>
    </array>

    <array name="m_Pointers" type="atFixedArray" size="2">
      <pointer policy="owner" type="BaseClass"/>
    </array>

    <array name="m_EnumsAsU8s" type="atFixedArray" size="2">
      <enum name="" type="AllSimpleData::SimpleEnum" size="8"/>
    </array>

    <array name="m_EnumsAsU16s"  type="atFixedArray" size="2">
      <enum type="AllSimpleData::SimpleEnum" size="16"/>
    </array>

    <array name="m_EnumsAsU32s"  type="atFixedArray" size="2">
      <enum type="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_FixedBitsets"  type="atFixedArray" size="2">
      <bitset type="fixed" values="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_FixedBitset64s"  type="atFixedArray" size="2">
      <bitset type="fixed" numBits="64" values="LotsOfBits"/>
    </array>

    <array name="m_VariableBitsets"  type="atFixedArray" size="2">
      <bitset type="atBitSet" values="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_UnnamedVariableBitsets"  type="atFixedArray" size="2">
      <bitset type="atBitSet" values="AllSimpleData::SimpleEnum"/>
    </array>

    <array name="m_PointerStrings"  type="atFixedArray" size="2">
      <string type="pointer"/>
    </array>

    <array name="m_MemberStrings"  type="atFixedArray" size="2">
      <string type="member" size="32"/>
    </array>

    <array name="m_ConstStrings"  type="atFixedArray" size="2">
      <string type="ConstString"/>
    </array>

    <array name="m_AtStrings"  type="atFixedArray" size="2">
      <string type="atString"/>
    </array>

    <array name="m_AtWideStrings"  type="atFixedArray" size="2">
      <string type="atWideString"/>
    </array>

    <array name="m_WidePointerStrings"  type="atFixedArray" size="2">
      <string type="wide_pointer"/>
    </array>

    <array name="m_WideMemberStrings"  type="atFixedArray" size="2">
      <string type="wide_member" size="32"/>
    </array>

  </structdef>

<structdef type="BigStructure">
  <struct name="m_SimpleData" type="AllGeneratedSimpleData"/>
  <struct name="m_SimpleContainer" type="SimpleContainer"/>
  <pointer name="m_SimpleContainerPtr" type="SimpleContainer" policy="owner"/>
  <struct name="m_SimplePointerContainer" type="SimplePointerContainer"/>
  <pointer name="m_SimplePointerContainerPtr" type="SimplePointerContainer" policy="owner"/>
  <struct name="m_NumericArrays" type="NumericArrays"/>
  <struct name="m_FixedNumericArrays" type="FixedNumericArrays"/>
  <struct name="m_NonNumericArrays" type="NonNumericArrays"/>
  <struct name="m_FixedNonNumericArrays" type="FixedNonNumericArrays"/>
  <struct name="m_AllArrays" type="AllArrays"/>
  <struct name="m_LinkedList" type="LinkedList"/>
  <struct name="m_BinaryTree" type="BinaryTree"/>
  <struct name="m_PointerArray" type="PointerArray"/>
  <struct name="m_FixedPointerArray" type="FixedPointerArray"/>
  <struct name="m_AllMaps" type="AllMaps"/>
</structdef>

<structdef type="BigStructure_PSO">
  <struct name="m_SimpleData" type="AllGeneratedSimpleData"/>
  <struct name="m_SimpleContainer" type="SimpleContainer"/>
  <pointer name="m_SimpleContainerPtr" type="SimpleContainer" policy="owner"/>
  <struct name="m_SimplePointerContainer" type="SimplePointerContainer"/>
  <pointer name="m_SimplePointerContainerPtr" type="SimplePointerContainer" policy="owner"/>
  <struct name="m_NumericArrays" type="NumericArrays"/>
  <struct name="m_FixedNumericArrays" type="FixedNumericArrays"/>
  <struct name="m_NonNumericArrays" type="NonNumericArrays"/>
  <struct name="m_FixedNonNumericArrays" type="FixedNonNumericArrays"/>
  <struct name="m_AllArrays" type="AllArrays"/>
  <struct name="m_LinkedList" type="LinkedList"/>
  <struct name="m_BinaryTree" type="BinaryTree"/>
  <struct name="m_PointerArray" type="PointerArray"/>
  <struct name="m_FixedPointerArray" type="FixedPointerArray"/>
  <struct name="m_AllBinMaps" type="AllBinMaps"/>
</structdef>


  <!-- These are for the data inheritance samples -->
  <structdef type="PhysicsSettings" usedi="true">
    <float name="m_Weight"/>
    <float name="m_Friction"/>
    <float name="m_Bounciness"/>
    <string name="m_Material" type="atString"/>
  </structdef>

  <structdef type="Prop" usedi="true">
    <string name="m_Name" type="atString"/>
    <float name="m_Price"/>
    <struct name="m_PhysicalProperties" type="PhysicsSettings"/>
  </structdef>

  <structdef type="PropDerived" base="Prop" usedi="true">
    <Vector3 name="m_Position"/>
  </structdef>

  <structdef type="BaseVehicleTuneData" usedi="true">
    <bool name="m_CanFloat"/>
    <bool name="m_HasEngine"/>
  </structdef>

  <enumdef type="DrivenWheelsEnum">
    <enumval name="FRONT"/>
    <enumval name="REAR"/>
    <enumval name="ALL"/>
  </enumdef>
  
  <structdef type="CarTuningData" base="BaseVehicleTuneData" usedi="true">
    <enum name="m_DrivenWheels" type="DrivenWheelsEnum"/>
    <float name="m_RideHeight"/>
  </structdef>

  <structdef type="Vehicle" base="Prop" usedi="true">
    <pointer name="m_TuningData" type="BaseVehicleTuneData" policy="owner"/>
    <map name="m_AtMap"  type="atMap" key="atHashString">
       <pointer type="PhysicsSettings" policy="owner"/>
    </map>
    <map name="m_AtMap2"  type="atMap" key="u8">
       <pointer type="BigStructure" policy="owner"/>
    </map>
    <map name="m_AtBinMap"  type="atBinaryMap" key="atHashString">
        <pointer type="PhysicsSettings" policy="owner"/>
    </map>
  </structdef>

</ParserSchema>
