// 
// /sample_write.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/versioninfo.h"
#include "parser/manager.h"
#include "system/main.h"

using namespace rage;

////////////////////////////////////////////////////////////////
// TITLE: sample_write - Writing data with the parser
// PURPOSE:
//		This sample shows how to perform low-level writing with the parser.
//		The low level write functions are analogous to the low-level "pull" style
//		read functions, where the user fills out parElement objects, and then
//		calls function to begin and end elements.

int Main()
{

	// STEP #1. Initialize the parser
	// -- This creates the PARSER singleton. This only needs to be done once at the beginning of a program.
	INIT_PARSER;

	// STEP #2. Open a file for writing
	// -- Opens "writedata.xml" for output. OpenOutputStream also takes an optional
	// argument specifying the preferred file format (e.g. XML, RBF)

	parStreamOut* s = PARSER.OpenOutputStream("writedata", "xml");

	// STEP #3. Write elements to the stream.
	// -- This is the basic pattern, create a parElement object, set its name, add additional attributes to it,
	// and then call WriteBeginElement()
	parElement topElt;
	topElt.SetName("TopLevelElement", false);

	s->WriteBeginElement(topElt);

	// -- You can also call WriteLeafElement to write a childless element (same as calling WriteBeginElement() 
	// followed by WriteEndElement())
	parElement subElt;
	subElt.SetName("Position", false);
	subElt.AddAttribute("x", 10.0f, false);
	subElt.AddAttribute("y", 40.2f, false);
	subElt.AddAttribute("z", -10.0f, false);
	s->WriteLeafElement(subElt);

	// -- The element objects can be used multiple files for writing. Here the element is reset and 
	// gets a new name and attribute.
	subElt.Reset();
	subElt.SetName("DataContainer");
	subElt.AddAttribute("content", "binary");
	s->WriteBeginElement(subElt);

	// STEP #4. Write raw data to the stream

	// -- Here we create an example structure and fill it with data
	struct someExampleStruct {
		float x,y,z;
		int a,b;
	};

	someExampleStruct binData;
	binData.x = 34.0e5f;
	binData.y = 3125.0f;
	binData.z = 2435.0f;
	binData.a = 7865;
	binData.b = 23567;

	// -- Now we can call WriteData to write the raw data. It will be embedded inside subElt. An element can
	// either have child elements or raw data, but not both.
	s->WriteData(reinterpret_cast<char*>(&binData), sizeof(binData), true, parStream::BINARY);

	// STEP #5. Cleanup.
	
	// -- Every WriteBeginElement() call must have a matching WriteEndElement() call that passes the same element in.
	// So here we end both subElt ("DataContainer" xml tag) and topElt ("TopLevelElement" xml tag)
	s->WriteEndElement(subElt);

	s->WriteEndElement(topElt);;

	// -- Finally, close and delete the stream, and shut down the parser.
	s->Close();
	delete s;

	SHUTDOWN_PARSER;

	//-STOP

	return 0;
}
