top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = sample_datasource
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib

..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageGraphics -f RageGraphics.mk

LIBS += ..\sample_parser/$(INTDIR)/sample_parser.lib

..\sample_parser/$(INTDIR)/sample_parser.lib: FORCE
	$(MAKE) -C ..\sample_parser -f sample_parser.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\rage\base\src -I ..\..\..\..\rage\base\samples -I ..\..\..\..\rage\stlport\STLport-5.0RC5\src -I ..\..\..\..\rage\base\tools\cli -I ..\..\..\..\rage\base\tools\dcc\libs -I ..\..\..\..\rage\base\tools\libs

$(INTDIR)\sample_datasource.obj: sample_datasource.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\sample_datasource.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\sample_datasource.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
