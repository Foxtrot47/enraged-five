// 
// /sample_push.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/functor.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

PARAM(file, "");

using namespace rage;

////////////////////////////////////////////////////////////////
// TITLE: sample_push - Reading data with the parser - "Push" style
// PURPOSE:
//		This sample shows how to read data with the parser using the "push" style of reading.
//		In this style, the parser itself iterates over the elements in the file and calls
//		user-defined callbacks on each one. It is similar to the SAX method of accessing XML files.

// STEP #1. Define the callbacks.
//-- When the parser iterates over a file, it calls three callbacks.

// The begin element callback takes a reference to the element that was just read.
// This element contains a name and an attribute list, which is a list of name, value pairs.
// It corresponds to an XML element like <Elem name="an element" size="20">
// The second argument, isLeaf, will be true if the parser knows in advance that the element
// has no child elements.
void OnBegin(parElement& elt, bool /*isLeaf*/)
{
	printf("Begin %s\n", elt.GetName());
}

//-- The end element callback takes a single argument, isLeaf. It will be true if the isLeaf
// parameter in the corrsponding call to the begin element callback was also true.
void OnEnd(bool /*isLeaf*/)
{
	printf("End\n");
}

//-- The data callback is called when reading a raw data block from the file. You can specify
// in the parStreamIn object what the maximum amount of data to read is. If the data block in
// the input file is larger than this maximum, the callback will be called repeatedly and 
// dataIncomplete will be true for all but the last call.
void OnData(char* data, int /*size*/, bool /*dataIncomplete*/)
{
	printf("Data: %s\n", data);
}
//-STOP

int Main()
{
	// STEP #2. Initialize the parser
	// -- This creates the PARSER singleton.  This only needs to be done once at the beginning of a program.
	INIT_PARSER;

	const char* name = "pushdata";
	PARAM_file.Get(name);

	// STEP #3. Open the input stream and configure it
	// -- This will open pushdata.xml for reading, and register the three read callbacks
	parStreamIn* s = PARSER.OpenInputStream(name, "xml");
	s->SetBeginElementCallback(atDelegate<void (parElement&, bool)>(&OnBegin));
	s->SetEndElementCallback(atDelegate<void (bool)>(&OnEnd));
	s->SetDataCallback(atDelegate<void (char*, int, bool)>(&OnData));

	// STEP #4. Read data from the file.
	//-- This function will iterate over the contents of the pushdata.xml file and
	// call the callbacks that were defined above.
	s->ReadWithCallbacks();

	// STEP #5. Cleanup
	// -- Close and delete the stream, then shutdown the parser singleton
	s->Close();
	delete s;

	SHUTDOWN_PARSER;
	//-STOP

	return 0;
}
