// 
// sample_parser/objects_simple.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_PARSER_OBJECTS_SIMPLE_H
#define SAMPLE_PARSER_OBJECTS_SIMPLE_H

#include "atl/bitset.h"
#include "atl/wstring.h"
#include "atl/string.h"
#include "atl/hashstring.h"
#include "atl/map.h"
#include "atl/binmap.h"
#include "parser/macros.h"
#include "string/string.h"
#include "string/unicode.h"
#include "vector/color32.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"

class TestString
{
public:
	char* m_String;
	PAR_SIMPLE_PARSABLE;
};

class SimpleStruct
{
public:
	SimpleStruct() 
		: m_TestEnum(BONEMASK_ALL) 
		, m_Position(0.0f, 0.0f, 0.0f)
		, m_MaxSpeed(10.0f)
		, m_MinSpeed(0.0f)
		, m_NumConnections(-1)
		, m_Name(NULL)
		, m_WideName(NULL)
		, m_ForeColor(1.0f, 0.5f, 0.0f, 0.1f)
	{
	}

	~SimpleStruct()
	{
		delete m_WideName;
		delete m_Name;
	}

	enum TestEnum
	{
		BONEMASK_ALL = 0,
		BONEMASK_UPPERONLY = 3633914286,
		BONEMASK_SPINEONLY = 2984602496,
		BONEMASK_ARMONLY_L = 1739926164,
		BONEMASK_ARMONLY_R = 3772128476,
		BONEMASK_BODYONLY = 1432835604,
	};

	TestEnum m_TestEnum;
	rage::Vector3 m_Position;
	float m_MaxSpeed;
	float m_MinSpeed;
	int m_NumConnections;
	const char* m_Name;
	rage::Matrix34 m_Location;
	const rage::char16* m_WideName;
	rage::Color32 m_ForeColor;

	PAR_SIMPLE_PARSABLE;
};

class TestMap
{
public:
    rage::atMap<rage::atHashString, SimpleStruct*> m_Map;
    rage::atBinaryMap<SimpleStruct*, rage::atHashString> m_BinMap;
    PAR_SIMPLE_PARSABLE;
};


class SimpleContainer
{
public:
	SimpleStruct m_Struct1, m_Struct2;

	PAR_SIMPLE_PARSABLE;
};

class AllSimpleData
{
public:
	~AllSimpleData() 
	{
		delete m_PointerString;
		delete m_WidePointerString;
	}
	AllSimpleData()
	{
		m_EnumAsU32 = m_EnumAsU16 = m_EnumAsU8 = ONE;
		m_PointerString = NULL;
		m_WidePointerString = NULL;
	}	

	enum SimpleEnum
	{
		ONE = 1,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		ELEVEN,
		TWELVE,
		THIRTEEN,
		FOURTEEN,
		FIFTEEN,
		SIXTEEN,
		SEVENTEEN,
		EIGHTEEN,
		NINTEEN,
		TWENTY
	};

	// Basic types

	float m_Float;
	char m_Char;
	char m_AnotherChar;
	unsigned char m_UChar;
	short m_Short;
	short m_AnotherShort;
	unsigned short m_UShort;
	int m_Int;
	int m_AnotherInt;
	unsigned int m_UInt;
	bool m_Bool;
	rage::Vector2 m_Vector2;
	rage::Vector3 m_Vector3;
	rage::Vector4 m_Vector4;
	rage::Matrix34 m_Matrix34;
	rage::Matrix44 m_Matrix44;

	rage::Vec2V m_Vec2V;
	rage::Vec3V m_Vec3V;
	rage::Vec4V m_Vec4V;

	rage::ScalarV m_ScalarV;
	rage::BoolV m_BoolV;
	rage::VecBoolV m_VecBoolV;

	rage::Mat33V m_Mat33V;
	rage::Mat34V m_Mat34V;
	rage::Mat44V m_Mat44V;

	// Special types
	float m_Angle;
	rage::Color32 m_ColorA;
	unsigned int m_ColorB;
	rage::Vector3 m_ColorC;
	rage::Vector4 m_ColorD;
	rage::Vec3V m_ColorE;

	unsigned char			m_EnumAsU8;
	unsigned short			m_EnumAsU16;
	unsigned int			m_EnumAsU32;
	rage::atFixedBitSet32	m_FixedBitset;
	rage::atBitSet		m_VariableBitset;

	// String types
	const char* m_PointerString;
	char m_MemberString[32];
	rage::ConstString m_ConstString;
	rage::atString	m_AtString;

	rage::atWideString m_AtWideString;
	const rage::char16* m_WidePointerString;
	rage::char16 m_WideMemberString[32];

	PAR_SIMPLE_PARSABLE;
};

// This tests arrays of objects containing trivial and non-trivial destructors, to ensure that we set the array cookies correctly

class SimpleDtor1Obj
{
public:
	SimpleDtor1Obj() { Displayf("Ctor1 %d", ++sm_CtorCount); }
	~SimpleDtor1Obj() { Displayf("Dtor1 %d", ++sm_DtorCount); }
	rage::u8 m_Data;
	static int sm_CtorCount;
	static int sm_DtorCount;
	PAR_SIMPLE_PARSABLE;
};

class SimpleDtor2Obj
{
public:
	SimpleDtor2Obj() { Displayf("Ctor2 %d", ++sm_CtorCount); }
	~SimpleDtor2Obj() { Displayf("Dtor2 %d", ++sm_DtorCount); }
	rage::u16 m_Data;
	static int sm_CtorCount;
	static int sm_DtorCount;
	PAR_SIMPLE_PARSABLE;
};

class SimpleDtor4Obj
{
public:
	SimpleDtor4Obj() { Displayf("Ctor4 %d", ++sm_CtorCount); }
	~SimpleDtor4Obj() { Displayf("Dtor4 %d", ++sm_DtorCount); }
	rage::u32 m_Data;
	static int sm_CtorCount;
	static int sm_DtorCount;
	PAR_SIMPLE_PARSABLE;
};

class SimpleDtor16Obj
{
public:
	SimpleDtor16Obj() { Displayf("Ctor16 %d", ++sm_CtorCount); }
	~SimpleDtor16Obj() { Displayf("Dtor16 %d", ++sm_DtorCount); }
	rage::Vec4V m_Data;
	static int sm_CtorCount;
	static int sm_DtorCount;
	PAR_SIMPLE_PARSABLE;
};

class TestBigInstance
{
public:
	rage::u32 m_Data;
	char padding[100000];
	PAR_SIMPLE_PARSABLE;
};

class RawArrayTester
{
public:
	~RawArrayTester();
	rage::u8* m_Pod1;
	rage::u16* m_Pod2;
	rage::u32* m_Pod4;
	rage::Vec4V* m_Pod16;

	SimpleDtor1Obj* m_Dtor1;
	SimpleDtor2Obj* m_Dtor2;
	SimpleDtor4Obj* m_Dtor4;
	SimpleDtor16Obj* m_Dtor16;

	PAR_SIMPLE_PARSABLE;
};

#endif
