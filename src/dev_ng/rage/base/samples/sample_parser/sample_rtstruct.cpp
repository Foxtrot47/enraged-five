// 
// sample_parser/sample_rtstruct.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "parser/manager.h"
#include "system/main.h"
#include "vector/vector3.h"

///////////////////////////////////////////////////////////
// TITLE: sample_rtstruct - Building structures at runtime with parRTStructure
// PURPOSE: 
//		This sample shows how to create parsable structures at runtime,
//		which is useful if the object is a collection of otherwise unrelated
//		data, or data from a number of different systems. A parRTStructure
//		contains the absolute memory addresses of each of the members added
//		to it, so it should be built based on existing objects.


// STEP #1. #include objects we'll be using
// -- We'll be building our own structures at runtime, but these structures can contain other
// predefined structures, so we're including some of them here
#include "objects_simple.h"
#include "objects_hierarchy.h"
//-STOP

using namespace rage;


// STEP #2. Create an example structure to load data to/from.
// -- This structure will just be a container for some data. We'll create an instance of 
// the structure and then load and save data from the instance.
struct TestRTStruct
{
	float m_Float;
	int m_Int;

	// -- One feature of structures demonstrated below is external named pointers. These
	// are pointers that point to some external data, and when saved out we just save a name
	// instead of the object that is pointed to. In this case, m_ColorPtr points to one of the
	// three static Vector3s below.
	Vector3* m_ColorPtr; // Points to one of the three statics below

	static Vector3 sm_Red;
	static Vector3 sm_Green;
	static Vector3 sm_Blue;

	// -- To use external named pointers, we need to provide two callbacks. One for converting
	// from const char* to Vector3*, and one for converting the other way.
	static Vector3* GetColorFromName(const char* name) {
		switch(name[0]) {
		case 'R': return &sm_Red;
		case 'G': return &sm_Green;
		case 'B': return &sm_Blue;
		}
		return NULL;
	}

	static const char* GetColorName(const Vector3* vec) {
		if (!vec) return "NULL";
		if (vec->x != 0.0f) return "Red";
		if (vec->y != 0.0f) return "Green";
		if (vec->z != 0.0f) return "Blue";
		return "NULL";
	}
};
// -STOP

Vector3 TestRTStruct::sm_Red(1.0f, 0.0f, 0.0f);
Vector3 TestRTStruct::sm_Green(0.0f, 1.0f, 0.0f);
Vector3 TestRTStruct::sm_Blue(0.0f, 0.0f, 1.0f);

int Main()
{

	// STEP #3. Initialize the parser
	// -- This creates the PARSER singleton
	INIT_PARSER;

    // STEP #4. Create sample data containers, and build the parRTStructure

	// -- These are two object instances that will have their data loaded by the structure,
	// as well as a bare float that we'll read data into.
	TestRTStruct structOne;
	SimpleStruct containedData;
	float immediateFloat;

	// -- We can also load derived classes through a base class pointer with parRTStructures
	BaseClass* bc = NULL;

	// -- Here the parRTStructure is created, with a name for the top level element in the file
	parRTStructure rts("RTTest");

	// -- AddValue adds a single value to the parRTStructure, and each of the values needs a name.
	// Here we add two members from structOne and the immediate float
	rts.AddValue("AFloat", &structOne.m_Float);
	rts.AddValue("AnInt", &structOne.m_Int);
	rts.AddValue("ImmediateFloat", &immediateFloat);

	// -- AddExternalNamedPtr takes a name, a pointer, and two callback functions for converting 
	// a string to a pointer, and a pointer to a string.
	rts.AddExternalNamedPtr("Color", &structOne.m_ColorPtr, TestRTStruct::GetColorFromName, TestRTStruct::GetColorName);

	// -- AddOwningPointer takes the address of a pointer. The pointer type must already be registered
	// with the parser.
	rts.AddOwningPtr("Owning", &bc);

	// -- AddStructureInstance takes a reference to an instance of a parsable type.
	rts.AddStructureInstance("ContainedStructure", containedData);

	// -- We can also add other parRTStructures to parRTStructures. Here an inner structure
	// containing two floats is added to the base structure. In this case the name "starts" used in the
	// parRTStructure is ignored, and just "StartData" will appear in the file.
	float startX = -123.0f, startY = 456.0f;
	parRTStructure* innerRTS = rage_new parRTStructure("starts");
	innerRTS->AddValue("Start_X", &startX);
	innerRTS->AddValue("Start_Y", &startY);
	rts.AddStructure("StartData", *innerRTS);


	// STEP #5. Load data
	// -- LoadFromStructure takes a parStructure that describes the object to be loaded, instead
	// of the normal LoadObject and LoadObjectPtr which get the parStructure from the type of the
	// object we're loading.
	// It can also
	// take a pointer to the object we're loading. In this case the parRTStructure contains absolute 
	// memory addresses to the data it's loading, so we don't need to specify an object's address.
	PARSER.InitFromStructure(rts);
	PARSER.LoadFromStructure("rtstructin", "xml", rts, NULL);

	// STEP #6. Modify the data
	// -- Here we make a few changes to the data we've just read in.
	structOne.m_Float *= structOne.m_Int;

	containedData.m_Position.Set(0.0f, 10.0f, 20.0f);
	containedData.m_MaxSpeed = containedData.m_MinSpeed = 100.0f;
	containedData.m_NumConnections = 10;

	delete bc;
	DerivedClassA* dca = rage_new DerivedClassA;
	dca->m_BaseFloat = FLT_MAX;
	dca->m_DerivedInt = 123;
	bc = dca;

	// STEP #7. Save out the data.
	// -- Like LoadFromStructure, SaveFromStructure takes a parStructure and optional pointer to
	// load data into. Again since the parRTStructure contains absolute addresses the pointer isn't necessary.
	PARSER.SaveFromStructure("rtstructout", "xml", rts, NULL);

	delete dca;

	delete innerRTS;
	// STEP #8. Clean up
	// -- SHUTDOWN_PARSER will unregister all registered objects and destory the PARSER singleton.
	SHUTDOWN_PARSER;
	// -STOP

	return 0;
}
