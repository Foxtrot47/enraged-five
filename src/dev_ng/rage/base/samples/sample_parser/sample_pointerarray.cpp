// 
// sample_parser/sample_pointerarray.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/atfunctor.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

#include "objects_hierarchy.h"
#include "objects_container.h"

using namespace rage;

PARAM(outfile, "Name of the output file to create");
PARAM(infile, "Name of the input file to load");

////////////////////////////////////////////////////////
// TITLE: sample_pointerarray - Reading and writing arrays of pointers to parsable objects
// PURPOSE: This sample builds on sample_struct and shows how the parser
//		can read and write arrays of pointers to parsable objects, where the
//		actual types of the objects in the array can vary from object to object
//		(so long as they inherit from a common parsable base class)

// STEP #1. Set up your objects for parsing.
// -- Add a PARSE= line to your makefile.bat. In this case PARSE=objects_hierarchy objects_container
// -- Define the parsable classes in objects_hierarchy.h and objects_container.h
// -- Declare the structure metadata (the 'structdef' xml tags) in objects_hierarchy.cpp and objects_container.cpp
// -- #include the generated code (objects_hierarchy_parser.h) in objects_hierarchy.cpp
// -- #include the generated code (objects_container_parser.h) in objects_container.cpp
// -STOP


int Main()
{

	// STEP #2. Initialize the parser. All the classes we'll be using get automatically registered here.
	INIT_PARSER;

	// -STOP

	// STEP #3. Create a PointerArray object and add children to the array

	// -- One child will be a DerivedClassA instance
	DerivedClassA* a = rage_new DerivedClassA;
	a->m_BaseFloat = 2.0f;
	a->m_DerivedInt = 45;
	
	// -- One child will be a DerivedClassB instance
	DerivedClassB* b = rage_new DerivedClassB;
	b->m_BaseFloat = 1234.0f;
	b->m_DerivedVector.x = 100.0f;

	// -- Add both children to the pointer array
	PointerArray p;
	p.m_Children.Grow() = b;
	p.m_Children.Grow() = a;
	// -STOP

	// STEP #4. Use the parser to save and load the PointerArray obejcts

	// -- Saving will write type information to the output file
	const char* outFileName = "pointerout.xml";
	PARAM_outfile.Get(outFileName);
	PARSER.SaveObject(outFileName, "xml", &p);

	// -- Loading will use the type information in the input file to
	//		create objects of the correct type
	const char* inFileName = "pointerarray.xml";
	PARAM_infile.Get(inFileName);

	PointerArray pointers;
	PARSER.LoadObject(inFileName, "xml", pointers);

	for(int i = 0; i < pointers.m_Children.GetCount(); i++) {
		Printf("Child is %s\n", pointers.m_Children[i]->GetName());
	}
	// -STOP

	// Step #5. Cleanup
	SHUTDOWN_PARSER;
	// -STOP

	return 0;
}
