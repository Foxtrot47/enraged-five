// 
// sample_parser/sample_struct.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser/manager.h"
#include "parser/rtstructure.h"
#include "system/main.h"

#include "atl/hashstring.h"
#include "vector/color32.h"

#include "objects_simple.h"

using namespace rage;

////////////////////////////////////////////////////////
// TITLE: sample_struct - Reading and writing structures with the parser
// PURPOSE: This sample shows how to declare and register structures
//		with the parser so they can be directly read and written.
//		In this sample, the objects are defined in sample_struct_objects.[cpp,h]
//		and referenced in sample_struct.

#if 0
// STEP #1. [makefile.bat] Add a PARSE= line to the makefile.bat file
// -- Adding a PARSE= line to the makefile.bat instructs the compiler to run the
//		parCodeGen program on all the .cpp files listed in that line, and generate
//		the parser registration functions, runtime metadata objects, and schemas.
set PARSE=objects_simple
// -STOP
#endif

#if 0
// STEP #2. [objects_simple.h] Define the parsable structures
// -- The parsable structures are defined as normal, but they need to
// contain either the PAR_PARSABLE or PAR_SIMPLE_PARSABLE macros.
#include "parser/manager.h"
#include "vector/vector3.h"

class SimpleStruct
{
public:
	SimpleStruct() 
		: m_Position(0.0f, 0.0f, 0.0f)
		, m_MaxSpeed(10.0f)
		, m_MinSpeed(0.0f)
		, m_NumConnections(-1)
	{}

	rage::Vector3 m_Position;
	float m_MaxSpeed;
	float m_MinSpeed;
	int m_NumConnections;

	PAR_PARSABLE;
};


class SimpleContainer
{
public:
	SimpleStruct m_Struct1, m_Struct2;

	PAR_PARSABLE;
};
//-STOP
#endif

#if 0
// STEP #3. [objects_simple.cpp] #include the generated code and define the structure metadata.
// -- parCodeGen will create a file named sample_struct_objects_parser.h, which should be included
// in sample_struct_objects.cpp
#include "objects_simple.h"
#include "objects_simple_parser.h"

// -- Now define the structure metadata. This is a bit of XML contained in a C++ comment that 
// describes the structure for the parser. It contains a list of the parsable data members in the structure
// as well and widget and validation information.
/*
<structdef type="SimpleStruct">
	<Vector3 name="m_Position"/>
	<float name="m_MinSpeed" min="0.0f" max="1000.0f" step="0.1f"/>
	<float name="m_MaxSpeed" min="0.0f" max="1000.0f" step="0.1f"/>
	<int name="m_NumConnections"/>
<\/structdef>

<structdef type="SimpleContainer">
	<struct name="m_Struct1" type="SimpleStruct"/>
	<struct name="m_Struct2" type="SimpleStruct"/>
<\/structdef>
*/
// -STOP
#endif

#include "objects_container.h"
#include "objects_hierarchy.h"

int Main()
{
	// STEP #4. [sample_struct.cpp] Initialize the parser
	// -- This creates the singleton PARSER object.
	INIT_PARSER;

	// -- If any of the classes you want to read and write with the parser aren't 
	// automatically registered (via the 'autoregister' xml tag), use the
	// REGISTER_PARSABLE_CLASS to register them with the parser before using them.

	// STEP #5. [sample_struct.cpp] Create a new SimpleContainer object and load data into it.
	// -- Create the new SimpleContainer object. All pointers in the object must be NULLed, or
	// else the parser will expect that you've already allocated space for the contained objects.
	SimpleContainer sc;

	// -- Call PARSER.LoadObject to load the contents of structin.xml into the sc object.
	PARSER.InitObject(sc);
	PARSER.LoadObject("structin", "xml", sc);

	// STEP #6. [sample_struct.cpp] Modify the object and save it out
	// -- Make some various modifications to the object
	sc.m_Struct1.m_MaxSpeed *= 2.0f;
	sc.m_Struct2.m_MaxSpeed *= 2.0f;
	sc.m_Struct1.m_NumConnections++;
	sc.m_Struct2.m_NumConnections += sc.m_Struct1.m_NumConnections;
	sc.m_Struct1.m_Name = "Name";
	sc.m_Struct2.m_Name = "\n\n  \n\n";

	// -- Now write out the object.
	PARSER.SaveObjectAnyBuild("structout", "xml", &sc);

	// STEP #7. [sample_struct.cpp] Clean up
	// -- This destroys the PARSER singleton and unregisters all classes.
	SHUTDOWN_PARSER;

	return 0;
}
