// 
// /sample_tree.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/functor.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "system/main.h"
#include "vector/vector3.h"

using namespace rage;

////////////////////////////////////////////////////////////////
// TITLE: sample_tree - Reading data with the parser - "Tree" style
// PURPOSE: 
//		This sample shows how to build a parTree structure from 
//		a data file, and how to get data from the parTree. This is
//		somewhat similar to reading XML data using a DOM reader.

int Main()
{
	// STEP #1. Initialize the parser.
	// -- This creates the PARSER singleton.  This only needs to be done once at the beginning of a program.
	INIT_PARSER;

	// STEP #2. Create the tree object.
	//-- This function will read treedata.xml and create a new parTree containing the contents of the file.
	parTree* tree = PARSER.LoadTree("treedata", "xml");

	// STEP #3. Get data from the tree
	//-- tree->GetRoot() gets the root parTreeNode. Each parTreeNode has an element, and each one could have
	// child nodes or raw data, but not both.
	printf("Root element: %s\n", tree->GetRoot()->GetElement().GetName());

	parElement& rootElt = tree->GetRoot()->GetElement();
	for(int i = 0; i < rootElt.GetAttributes().GetCount(); i++)
	{
		char buf[50];
		const char* outStr = rootElt.GetAttributes()[i].GetStringRepr(buf, 50);
		Printf("Attribute %s = \"%s\"\n", rootElt.GetAttributes()[i].GetName(), outStr);
	}

	//-- Here we iterator over all this children of the root node, and print the name of each child element.
	for(parTreeNode::ChildNodeIterator node = tree->GetRoot()->BeginChildren(); node != tree->GetRoot()->EndChildren(); ++node) {
		printf("Child: %s\n", (*node)->GetElement().GetName());
	}

	//-- Finally we get the second child (the sibling of the first child) and get it's data
	// (which we assume is an ascii string)
	printf("2nd child data: %s\n", tree->GetRoot()->GetChild()->GetSibling()->GetData());

	// STEP #4. Cleanup
	// -- Delete the root tree node and shut down the parser
	delete tree;

	SHUTDOWN_PARSER;
	// -STOP

	return 0;
}
