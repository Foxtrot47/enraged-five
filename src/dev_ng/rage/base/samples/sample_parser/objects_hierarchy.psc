<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef name="A_Base_Class" type="BaseClass">
	<float name="m_BaseFloat"/>
</structdef>

<structdef base="BaseClass" type="DerivedClassA">
	<int name="m_DerivedInt"/>
</structdef>

<structdef base="BaseClass" type="DerivedClassB">
	<Vector3 name="m_DerivedVector"/>
</structdef>

<structdef base="BaseClass" constructable="false" type="AbstractClass">
	<float name="m_AbstractFloat"/>
</structdef>

<structdef base="AbstractClass" type="ConcreteSubclass">
	<float name="m_ConcreteFloat"/>
</structdef>

<structdef type="SimplePointerContainer">
	<pointer name="m_ItemOne" policy="owner" type="BaseClass"/>
	<pointer name="m_ItemTwo" policy="owner" type="BaseClass"/>
</structdef>

<structdef type="MIBaseClass">
	<float name="m_Base"/>
</structdef>

<structdef base="MIBaseClass" type="MIDerivedA">
	<float name="m_DerivedA"/>
</structdef>

<structdef base="MIBaseClass" type="MIDerivedB">
	<float name="m_DerivedB"/>
</structdef>

<structdef base="MIDerivedA" type="MIDerivedA2">
	<float name="m_DerivedA2"/>
</structdef>

<structdef base="MIDerivedB" type="MIDerivedB2">
	<float name="m_DerivedB2"/>
</structdef>

<structdef type="MIVBaseClass">
<float name="m_Base"/>
</structdef>

<structdef base="MIVBaseClass" type="MIVDerivedA">
<float name="m_DerivedA"/>
</structdef>

<structdef base="MIVBaseClass" type="MIVDerivedB">
<float name="m_DerivedB"/>
</structdef>

<structdef base="MIVDerivedA" type="MIVDerivedA2">
<float name="m_DerivedA2"/>
</structdef>

<structdef base="MIVDerivedB" type="MIVDerivedB2">
<float name="m_DerivedB2"/>
</structdef>

</ParserSchema>