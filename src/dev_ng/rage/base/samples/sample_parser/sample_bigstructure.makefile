Project sample_bigstructure
ConfigurationType exe
RootDirectory .

Files {
	sample_bigstructure.cpp
}

Include sample_parser.libdefs

Libraries {
	%RAGE_DIR%\base\samples\vcproj\RageBaseSample\RageBaseSample
	%RAGE_DIR%\base\samples\sample_rmcore\sample_rmcore
	%RAGE_DIR%\base\samples\sample_grcore\sample_grcore
}