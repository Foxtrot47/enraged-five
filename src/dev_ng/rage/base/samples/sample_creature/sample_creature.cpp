// 
// /sample_creature.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_rmcore/sample_rmcore.h"
#include "system/param.h"
#include "system/main.h"
#include "system/timemgr.h"
#include "system/memory.h"
#include "system/performancetimer.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "creature/creature.h"
#include "creature/componentblendshapes.h"
#include "crskeleton/skeleton.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "file/asset.h"
#include "grblendshapes/morphable.h"
#include "grblendshapes/manager.h"
#include "grmodel/matrixset.h"

#include "profile/tracedump.h"

using namespace rage;
using namespace ragesamples;

namespace rage {
extern int g_TargetDataVertexCount;
}

#define INSTANCE_COUNT 1

#define DEFAULT_FILE "$/sample_creature/imtest_north/entity"
PARAM(file, "[sample_creature] file to open");

#define DEFAULT_ANIM "$/sample_creature/imtest_north/test.anim"
PARAM(anim, "[sample_creature] anim to open");

PARAM(blendonly, "[sample_creature] do blending only, no skeletal animation");

PARAM(rscLoad, "[sample_creature] load from a resource, not a type file");

/*
	MC4 character test:

	rorc command lines:
	rorc entity.type drv_mp_01_blendshapes_set -platform=xenon -autotexdict -path=T:\mc4\assets\character\drv_mp_01_blendshapes_set -shaderlibpath=T:\mc4\assets\shaders\characters
	rorc entity.type drv_mp_01_blendshapes_set -platform=xenon -blendtargets -path=T:\mc4\assets\character\drv_mp_01_blendshapes_set -shaderlibpath=T:\mc4\assets\shaders\characters

	Command line for this app:
	-blendonly -path=T:\mc4\assets -rscload -file $\character\drv_mp_01_blendshapes_set\drv_mp_01_blendshapes_set -anim $\anim\blendshape_test -shaderlib=T:\mc4\assets\shaders\characters

	If you want to test with raw assets, just remove the -rscload flag, you need to remove the -rscload and change the last part of -file:
	-blendonly -path=T:\mc4\assets -Xrscload -file $\character\drv_mp_01_blendshapes_set\entity -anim $\anim\blendshape_test -shaderlib=T:\mc4\assets\shaders\characters
*/


class sampleCreature : public rmcSampleManager {
public:
	sampleCreature() : m_tGetFrame("Get Frame"), m_tApplyFrame("Apply Frame")
	{
		m_pAnimation = 0;
		m_pPlayer = NULL;
		m_fSkeletalAnimSpeed = 1.0f;
		m_bUseAnimation = true;
		m_bShowPerformanceInfo = true;

		m_nMemoryBytesUsed = 0;
	}

    virtual ~sampleCreature()
	{
	}

    void InitClient()
	{
		crAnimation::InitClass();
		crCreature::InitClass();

		for( int i = 0; i < INSTANCE_COUNT; i++ )
		{
			m_pCreatureGroup[i] = 0;
			m_pMatrixSet[i] = 0;
			m_pDrawableGroup[i] = 0;
			m_pManagerGroup[i] = 0;
		}

#if !__DEV && __OPTIMIZED && 0
		ASSET.PushFolder("blendtest_003_a");
		m_pDrawable = rage_new rmcDrawable();
		m_pDrawable->Load("blendtest_003_a.type");
		ASSET.PopFolder();

		m_pCreature = rage_new crCreature();
		m_pCreature->Init(m_pDrawable);

		ASSET.PushFolder("cubeBlend");
		m_pAnimation = rage_new crAnimation();
		m_pAnimation->Load("simple_cubes.anim");
		ASSET.PopFolder();
#endif
		const char* entityFilename = DEFAULT_FILE;
		PARAM_file.Get(entityFilename);
		LoadEntity(entityFilename);

		const char* animFilename = DEFAULT_ANIM;
		PARAM_anim.Get(animFilename);
		LoadAnimation(animFilename);
	}

    void ShutdownClient()
	{
		ClearGroups();

		// Cleanup the player
		if( m_pPlayer )
			delete m_pPlayer;

		// Cleanup the animation
		if( m_pAnimation )
			delete m_pAnimation;

		crCreature::ShutdownClass();
		crAnimation::ShutdownClass();
	}

    void UpdateClient()
	{
		if( m_bUseAnimation && m_pAnimation && m_pCreatureGroup[0] )
		{
			// Get a frame appropriate for the creature
			crFrame frameBuf;
			crFrameData* frameData = rage_new crFrameData;
			m_pCreatureGroup[0]->InitDofs(*frameData);
			frameBuf.Init(*frameData);
			frameData->Release();
			m_pCreatureGroup[0]->Identity(frameBuf);

			// Get the animation data for the current time
			m_tGetFrame.Start();
			m_pPlayer->Composite(frameBuf, false);
			m_tGetFrame.Stop();

			// Apply the animation data to the creature
			m_tApplyFrame.Start();
			m_TraceDump.Start();
			for( int i = 0; i < INSTANCE_COUNT; i++ )
			{
				if( m_pCreatureGroup[i] )
				{
					m_pCreatureGroup[i]->Pose(frameBuf);

					if (PARAM_blendonly.Get()) {
						m_pCreatureGroup[i]->GetSkeleton()->Reset(); 
						m_pCreatureGroup[i]->GetSkeleton()->Update();
					}

					m_pMatrixSet[i]->Update(*m_pCreatureGroup[i]->GetSkeleton(),true);

					m_pCreatureGroup[i]->Finalize(TIME.GetSeconds());
				}
			}
			m_TraceDump.Stop();
			m_tApplyFrame.Stop();

			// Update the animation player
			m_pPlayer->Update(TIME.GetSeconds());
		}
	}

    void DrawClient()
	{
		// Draw the drawable
		if( m_pCreatureGroup[0] )
		{
			m_pCreatureGroup[0]->SwapBuffers();
			m_pCreatureGroup[0]->DrawUpdate();
			m_pCreatureGroup[0]->PreDraw(true);
			for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
			{
				if (GetBucketEnable(bucket)) 
				{
					u32 bucketMask = BUCKETMASK_GENERATE(bucket);
					u8 lod = LOD_HIGH;

					if( m_pCreatureGroup[0]->GetSkeleton() )
					{
						m_pDrawableGroup[0]->DrawSkinned(M34_IDENTITY, *m_pMatrixSet[0], bucketMask, lod);
					}
					else
					{
						m_pDrawableGroup[0]->Draw(M34_IDENTITY, bucketMask, lod);
					}
				}
			}
		}

		// Draw performance info
		grcLightState::SetEnabled(false);
		if( m_bShowPerformanceInfo )
		{
			PUSH_DEFAULT_SCREEN();
#if 0
			Displayf("Get Frame:    %f ms", m_tGetFrame.GetTimeMS());
			Displayf("Apply Frame:  %f ms", m_tApplyFrame.GetTimeMS());
			Displayf("Memory Used:  %d bytes", m_nMemoryBytesUsed);
#else
			static float applyFrame;
			static float average;
			float applyTime = m_tApplyFrame.GetTimeMS();
			float averageTime = g_TargetDataVertexCount? applyTime / g_TargetDataVertexCount : 0;
			applyFrame = applyFrame ? 0.98f * applyFrame + 0.02f * applyTime  : applyTime;
			average = average? 0.98f * average + 0.02f * averageTime : averageTime;
			grcFont::GetCurrent().Drawf(50, 100, "Get Frame:      %f ms", m_tGetFrame.GetTimeMS());
			grcFont::GetCurrent().Drawf(50, 115, "Apply Frame:    %f ms", applyFrame);
			grcFont::GetCurrent().Drawf(50, 145, "Memory Used:    %d bytes", m_nMemoryBytesUsed);
			grcFont::GetCurrent().Drawf(50, 160, "Verts blended:  %d",g_TargetDataVertexCount);
			grcFont::GetCurrent().Drawf(50, 175, "Cost per vert:  %f ms",average);
			g_TargetDataVertexCount = 0;
#endif
			POP_DEFAULT_SCREEN();
		}
		m_tGetFrame.Reset();
		m_tApplyFrame.Reset();
	}

	void ClearGroups()
	{
		for( int i = 0; i < INSTANCE_COUNT; i++ )
		{
			if( m_pCreatureGroup[i] )
				delete m_pCreatureGroup[i];
			if( m_pDrawableGroup[i] )
				delete m_pDrawableGroup[i];
			if( m_pManagerGroup[i] )
				m_pManagerGroup[i]->Release();
			if ( m_pMatrixSet[i] )
				delete m_pMatrixSet[i];
		}
	}

	void LoadEntity(const char* fullPresetPath)
	{
		ClearGroups();

		// Get the base filename
		char base[256];
		strcpy(base, fiAssetManager::FileName(fullPresetPath));

		// Get the path
		char path[256];
		fiAssetManager::RemoveNameFromPath(path, 256, fullPresetPath);

		// Push the path folder if the path is present
		if( strcmp(path, fullPresetPath) )
			ASSET.PushFolder(path);

		// Mark the free memory before loading
		size_t freeMemBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();
		RAGE_TRACK(PreLoad);

		// Load the entity
		for( int i = 0; i < INSTANCE_COUNT; i++ )
		{
			crCreature* pCreature = 0;
			rmcDrawable* pDrawable;
			grbTargetManager *pManager = 0;
			if (PARAM_rscLoad.Get()) {
				pDrawable = rmcDrawable::LoadResource(base);
				pgRscBuilder::Load(pManager,base,"#btm",grbTargetManager::RORC_VERSION);
			}
			else {
				pDrawable = rage_new rmcDrawable();
				if( !pDrawable->Load(base) )
				{
					delete pDrawable;
					pDrawable = 0;
				}
				pManager = rage_new grbTargetManager();
				if( !pManager->Load(base, pDrawable) )
				{
					pManager->Release();
					pManager = 0;
				}
			}

			if (pDrawable)
			{
				// Create the creature
				pCreature = rage_new crCreature();
				pCreature->Init(*pDrawable,pManager);	
			}
			m_pCreatureGroup[i] = pCreature;
			m_pDrawableGroup[i] = pDrawable;
			m_pManagerGroup[i] = pManager;
			m_pMatrixSet[i] = grmMatrixSet::Create(*pCreature->GetSkeleton());
		}

		// Get the memory after loading and compute the memory cost
		RAGE_TRACK(PostLoad);
		size_t freeMemAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
		m_nMemoryBytesUsed = freeMemBefore - freeMemAfter;

		// Pop the folder if we pushed one
		if( strcmp(path, fullPresetPath) )
			ASSET.PopFolder();
	}

	void LoadAnimation(const char* fullPresetPath)
	{
		// Delete any previous animation
		if( m_pAnimation )
		{
			delete m_pAnimation;
			m_pAnimation = 0;
		}

		// Get the base filename
		char base[256];
		strcpy(base, fiAssetManager::FileName(fullPresetPath));

		// Get the path
		char path[256];
		fiAssetManager::RemoveNameFromPath(path, 256, fullPresetPath);

		// Push the path folder if the path is present
		if( strcmp(path, fullPresetPath) )
			ASSET.PushFolder(path);

		// Load the animation
		m_pAnimation = crAnimation::AllocateAndLoad(base);

		// Pop the folder if we pushed one
		if( strcmp(path, fullPresetPath) )
			ASSET.PopFolder();

		// Setup the animation player
		if( !m_pPlayer )
		{
			m_pPlayer = rage_new crAnimPlayer;
		}

		m_pPlayer->SetAnimation( m_pAnimation );
		m_pPlayer->SetTime( 0.f );
		m_pPlayer->SetLooped(true, true);
	}

#if __BANK
	void LoadEntityWidget()
	{
		char fullPresetPath[256];
		memset(fullPresetPath, 0, 256);

		if( BANKMGR.OpenFile(fullPresetPath, 256, "*.type", false, "Entity (*.type)") )
		{
			LoadEntity(fullPresetPath);
		}
	}

	void LoadAnimationWidget()
	{
		char fullPresetPath[256];
		memset(fullPresetPath, 0, 256);

		if( BANKMGR.OpenFile(fullPresetPath, 256, "*.anim", false, "Animation (*.anim)") )
		{
			LoadAnimation(fullPresetPath);
		}
	}

    void AddWidgetsClient()
	{
        rmcSampleManager::AddWidgetsClient();

		bkBank& pBank = BANKMGR.CreateBank("sampleCreature");
		pBank.AddToggle("Show Performance Info", &m_bShowPerformanceInfo);
		pBank.AddToggle("Use Animation", &m_bUseAnimation);
		pBank.AddSlider("Skeletal Anim Speed", &m_fSkeletalAnimSpeed, 0.0f, 4.0f, 0.01f);

		pBank.AddButton("Load Entity", datCallback(MFA(sampleCreature::LoadEntityWidget), this));
		pBank.AddButton("Load Animation", datCallback(MFA(sampleCreature::LoadAnimationWidget), this));
	}
#endif

protected:
	crAnimation*		m_pAnimation;
	crAnimPlayer*		m_pPlayer;
		
	rmcDrawable*		m_pDrawableGroup[INSTANCE_COUNT];
	crCreature*			m_pCreatureGroup[INSTANCE_COUNT];
	grbTargetManager*	m_pManagerGroup[INSTANCE_COUNT];
	grmMatrixSet*		m_pMatrixSet[INSTANCE_COUNT];

	bool				m_bUseAnimation;
	float				m_fSkeletalAnimSpeed;

	bool				m_bShowPerformanceInfo;
	sysPerformanceTimer	m_tGetFrame;
	sysPerformanceTimer	m_tApplyFrame;
	size_t				m_nMemoryBytesUsed;

	pfTraceDump			m_TraceDump;
};

int Main()
{
    sampleCreature sample;

    sample.Init();
    sample.UpdateLoop();
    sample.Shutdown();

    return 0;
}
