//
// sample_physics/sample_articulated.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Articulated
// PURPOSE:
//		This sample arrangements of articulated objects of various types for testing their behavior moving and colliding.

#include "demoobject.h"

#include "sample_physics.h"

#include "grcore/im.h"
#include "grmodel/setup.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/colliderdispatch.h"
#include "physics/constraintcylindrical.h"
#include "physics/constraintspherical.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundsphere.h"

#define NM_TEST_ROTATIONS	0

PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

#if __WIN32
#pragma warning (disable : 4189)
#endif

class ArticulatedBodyManager : public physicsSampleManager
{
public:
	phDemoWorld* CreateSaloon ()
	{
		phDemoWorld& saloonWorld = *CreateNewDemoWorld("saloon");
		saloonWorld.SetFramerate(30.0f);
		Vector3 position(-4.4f,3.1f,0.0f);
		Vector3 rotation(0.0f,0.5f*PI,-0.3f);
        int numBodyParts = 13;
		phArticulatedObject* human = phArticulatedObject::CreateHuman(saloonWorld,position,rotation,ORIGIN,numBodyParts);
		m_TestHuman = human;
		SetMuscles(human->GetBody());
		position.Set(1.0f,5.3f,-4.7f);
		rotation.Set(0.3f,0.0f,0.0f);
		phArticulatedObject::CreateHuman(saloonWorld,position,rotation);
		position.Set(-1.2f,5.1f,-4.7f);
		rotation.Set(0.3f,0.0f,0.0f);
		human = phArticulatedObject::CreateHuman(saloonWorld,position,rotation);
		m_SpinningCollider = &human->GetCollider();
		m_InitialVelocity.Set(0.0f,6.0f,6.0f);
		if (m_SpinningCollider)
		{
			m_SpinningCollider->SetVelocity(m_InitialVelocity);
		}
		m_InitialAngVelocity.Set(2.0f,2.0f,0.0f);
		if (m_SpinningCollider)
		{
			m_SpinningCollider->SetAngVelocity(m_InitialAngVelocity);
		}

	#if NM_TEST_ROTATIONS
		// set desired rotation: get leans and twist from quat
		phArticulatedBody& testBody = m_TestHuman->GetBody();
		phJoint3Dof& joint = testBody.GetJoint3Dof(2);
		Vector3 rotAxis(1.0f,1.0f,0.0f); 
		rotAxis.Normalize();
		rage::Quaternion desQuat;
		desQuat.FromRotation(rotAxis,1);
		QuatV desQV = RCC_QUATV(desQuat);
		Vec3V desLeanTwistV = geomVectors::QuatVToLeanAndTwist(desQV); 
		joint.SetLean1TargetAngle(desLeanTwistV.GetXf());
		joint.SetLean2TargetAngle(desLeanTwistV.GetYf());
		joint.SetTwistTargetAngle(desLeanTwistV.GetZf());


		//	Matrix34 desTM;
		//	desTM.FromQuaternion(desQuat);
		//	desTM.Dot(parentMtx); // desired TM is parent TM multiplied by desired rotation
		//	desTM.d.Zero();
	#endif

		// right stairs
		position.Set(3.1f,0.0f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		float stairHeight = 0.2f;
		float stairDepth = 0.2f;
		int numStairs = 15;
		saloonWorld.ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.z += 0.9f;
		saloonWorld.ConstructRailing(position,rotation);
		position.z -= 1.8f;
		saloonWorld.ConstructRailing(position,rotation);

		// left stairs
		position.Set(-3.1f,0.0f,0.0f);
		rotation.Set(0.0f,0.5f*PI,0.0f);
		stairHeight = 0.2f;
		stairDepth = 0.2f;
		saloonWorld.ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.z += 0.9f;
		saloonWorld.ConstructRailing(position,rotation);
		position.z -= 1.8f;
		saloonWorld.ConstructRailing(position,rotation);

		// balcony
		position.Set(7.0f,2.4f,-1.0f);
		rotation.Zero();
		bool alignBottom = false;
		saloonWorld.CreateFixedObject("crate_flat",position,alignBottom,rotation);
		position.Set(-7.0f,2.4f,-1.0f);
		saloonWorld.CreateFixedObject("crate_flat",position,alignBottom,rotation);
		position.Set(4.0f,2.4f,-6.0f);
		rotation.Set(0.0f,0.5f*PI,0.0f);
		saloonWorld.CreateFixedObject("crate_flat",position,alignBottom,rotation);
		position.Set(-4.0f,2.4f,-6.0f);
		saloonWorld.CreateFixedObject("crate_flat",position,alignBottom,rotation);

		// balcony railings
		position.Set(6.0f,2.8f,-3.0f);
		rotation.Zero();
		bool sloped = false;
		saloonWorld.ConstructRailing(position,rotation,sloped);
		position.Set(-6.0f,2.8f,-3.0f);
		saloonWorld.ConstructRailing(position,rotation,sloped);
		position.Set(4.0f,2.8f,-5.0f);
		rotation.Set(0.0f,0.5f*PI,0.0f);
		saloonWorld.ConstructRailing(position,rotation,sloped);
		position.Set(-4.0f,2.8f,-5.0f);
		saloonWorld.ConstructRailing(position,rotation,sloped);
		position.Set(0.0f,2.8f,-5.0f);
		saloonWorld.ConstructRailing(position,rotation,sloped,"capsule_thin","capsule_small",1.0f,3.999f,0.5f);

		// bar
		position.Set(1.535334f,0.0f,-4.35f);
		rotation.Zero();
		saloonWorld.CreateObject("longer_crate",position,alignBottom,rotation);
		position.Set(-1.535334f,0.0f,-4.35f);
		saloonWorld.CreateObject("longer_crate",position,alignBottom,rotation);

		// tables
		position.Set(-1.9f,0.0f,-1.0f);
		rotation.Set(0.0f,-0.2f,0.0f);
		saloonWorld.ConstructTable(position,rotation);
		position.Set(3.5f,0.0f,2.5f);
		rotation.Set(0.0f,-0.15f,0.0f);
		saloonWorld.ConstructTable(position,rotation);
		position.Set(-3.5f,0.0f,2.5f);
		rotation.Set(0.0f,0.1f,0.0f);
		saloonWorld.ConstructTable(position,rotation);

		// chandeliers
#if 0 // Needs conversion to new constraint API
		position.Set(1.4f,6.5f,-4.1f);
		rotation.Set(0.0f,0.25f*PI,0.0f);
		phDemoObject* object = saloonWorld.CreateObject("chandelier",position,alignBottom,rotation);
		Assert(object);
		PHCONSTRAINT->AttachObjectToWorld('SaS0', object->GetPhysInst());
		position.Set(-1.4f,6.5f,-4.1f);
		object = saloonWorld.CreateObject("chandelier",position,alignBottom,rotation);
		Assert(object);
		PHCONSTRAINT->AttachObjectToWorld('SaS1', object->GetPhysInst());
		Assert(object->GetPhysInst() && object->GetPhysInst()->GetArchetype());
		phArchetypeDamp& archetype = *static_cast<phArchetypeDamp*>(object->GetPhysInst()->GetArchetype());
		const float angVelDamping = 0.2f;
		archetype.ActivateDamping(phArchetypeDamp::ANGULAR_V,Vector3(angVelDamping,angVelDamping,angVelDamping));
#endif // Needs conversion to new constraint API

		m_GravityWorldActive = false;
		m_SaloonWorldActive = true;

		return &saloonWorld;
	}
	
	phDemoWorld* CreateStaircase ()
	{
		phDemoWorld& stairsWorld = *CreateNewDemoWorld("stairs");
		stairsWorld.SetFramerate(30.0f);
		int numStairs = 15;
		Vector3 rotation(0.0f,-0.5f*PI,0.0f);
		Vector3 position(2.2f,4.0f,0.0f);
		Vector3 cgOffset(0.0f,1.0f,0.0f);
		phArticulatedObject* body = phArticulatedObject::CreateHuman(stairsWorld,position,rotation,cgOffset);
		position.Set(2.2f,5.0f,-5.0f);
		phArticulatedObject* puppet = phArticulatedObject::CreateHuman(stairsWorld,position,rotation);
		body->SetPuppetBody(&puppet->GetBody());
		position.Set(0.0f,0.0f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		float stairHeight = 0.2f;
		float stairDepth = 0.2f;
		stairsWorld.ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.z += 0.8f;
		stairsWorld.ConstructRailing(position,rotation);
		position.z -= 1.6f;
		stairsWorld.ConstructRailing(position,rotation);
		position.Set(0.0f,0.0f,-5.0f);
		stairsWorld.ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.z += 0.8f;
		stairsWorld.ConstructRailing(position,rotation);
		position.z -= 1.6f;
		stairsWorld.ConstructRailing(position,rotation);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return & stairsWorld;
	}


	phDemoWorld* CreatePommelHorse ()
	{
		phDemoWorld& pommelWorld = *CreateNewDemoWorld("pommel_horse");
		pommelWorld.SetFramerate(30.0f);
		Vector3 position(0.0f,4.0f,0.0f);
		Vector3 rotation(0.5f*PI,0.0f,0.0f);
		phArticulatedObject::CreateHuman(pommelWorld,position,rotation);
		position.Set(0.0f,2.0f,-0.4f);
		rotation.Set(0.0f,0.0f,0.5f*PI);
		bool alignBottom = false;
		pommelWorld.CreateFixedObject("capsule_fat",position,alignBottom,rotation);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &pommelWorld;
	}

#if 0 // Needs conversion to new constraint API
	phDemoWorld* CreateRagdollAndSphere ()
	{
		phDemoWorld& constrainRagdollWorld = *CreateNewDemoWorld("constrain_ragdoll");
		constrainRagdollWorld.SetFramerate(30.0f);
		Vector3 position(0.0f,4.0f,0.0f);
		Vector3 rotation(ORIGIN);
		int numBodyParts = 13;
		phArticulatedObject* human = phArticulatedObject::CreateHuman(constrainRagdollWorld,position,rotation,ORIGIN,numBodyParts);
		phConstraintMgr* constraintMgr = constrainRagdollWorld.GetSimulator()->GetConstraintMgr();
		position.Set(0.0f,4.4f,0.0f);
		int component = 0;
		phConstraint* constraint = constraintMgr->AttachObjectToWorld('SaR0', position,human->GetPhysInst(),component);
		float forceScale = 800.0f;
		float forceDamping = 100.0f;
		constraint->SetSoftTranslation(forceScale,forceDamping);
		position.Set(1.0f,2.84f,0.0f);
		phDemoObject* object = constrainRagdollWorld.CreateObject("sphere_056",position);
		Vector3 worldAttachPoint(1.0f,4.4f,0.0f);
		position.Set(1.0f,3.4f,0.0f);
		component = 0;
		float length = 0.0f;
		bool addInitialSeparation = false;
		constraint = constraintMgr->AttachObjectToWorld('SaR1', worldAttachPoint,position,object->GetPhysInst(),component,length,addInitialSeparation);
		float massRatio = object->GetPhysInst()->GetArchetype()->GetMass()/human->GetPhysInst()->GetArchetype()->GetMass();
		forceScale *= massRatio;
		forceDamping *= massRatio;
		constraint->SetSoftTranslation(forceScale,forceDamping);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &constrainRagdollWorld;
	}
#endif // Needs conversion to new constraint API

	phDemoWorld* CreateShakenRagdoll ()
	{
		phDemoWorld& shakeRagdollWorld = *CreateNewDemoWorld("constrain_ragdoll");
		shakeRagdollWorld.SetFramerate(30.0f);
		Vector3 position(0.0f,3.0f,0.0f);
		Vector3 rotation(ORIGIN);
		phArticulatedObject* human = phArticulatedObject::CreateHuman(shakeRagdollWorld,position,rotation);
		phConstraintMgr* constraintMgr = shakeRagdollWorld.GetSimulator()->GetConstraintMgr();
		position.Set(-0.85f,3.13f,0.0f);
		int component = 4;
		human->SetMovingConstraint(position,constraintMgr,component);
		human->SetFrequency(Vector3(2.0f,1.5f,0.9f));
		human->GetBody().SetStiffness(0.8f);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &shakeRagdollWorld;
	}

#if 0 // Needs conversion to new constraint API
	phDemoWorld* CreateRagdollAndCrate ()
	{
		phDemoWorld& crateRagdollWorld = *CreateNewDemoWorld("constrain_ragdoll_crate");
		crateRagdollWorld.SetFramerate(30.0f);
		Vector3 position(0.0f,3.0f,0.0f);
		Vector3 rotation(ORIGIN);
		phArticulatedObject* human = phArticulatedObject::CreateHuman(crateRagdollWorld,position,rotation);
		phConstraintMgr* constraintMgr = crateRagdollWorld.GetSimulator()->GetConstraintMgr();
		position.Set(-1.35f,3.03f,0.0f);
		int component = 4;
		phDemoObject* object = crateRagdollWorld.CreateObject("crate",position);
		position.Set(-0.85f,3.13f,0.0f);
		if (phDemoWorld::GetArticulatedRagdolls())
		{
			constraintMgr->AttachObjects('SaC0', position,object->GetPhysInst(),human->GetPhysInst(),0,component);
		}
		else
		{
			constraintMgr->AttachObjects('SaC1', position,object->GetPhysInst(),human->GetPartInst(component));
		}
		human->GetBody().SetStiffness(0.8f);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &crateRagdollWorld;
	}
#endif // Needs conversion to new constraint API

	phDemoWorld* CreateRagdollAndWall ()
	{
		phDemoWorld& wallWorld = *CreateNewDemoWorld("wall");
		Vector3 position(0.0f,2.0f,0.0f);
		Vector3 rotation(-0.2f,0.0f,0.0f);
		phArticulatedObject::CreateHuman(wallWorld,position,rotation);
		position.Set(0.0f,0.0f,-1.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		int numStairs = 2;
		float stairHeight = 1.0f;
		float stairDepth = 0.0f;
		wallWorld.ConstructBoxStairs("longer_crate",numStairs,position,rotation,stairHeight,stairDepth);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &wallWorld;
	}

	phDemoWorld* CreateChangingGravity ()
	{
		phDemoWorld* gravityWorld = CreateNewDemoWorld("gravity","small_plane");
		Vector3 rotation(0.0f,-0.5f*PI,0.0f);
		Vector3 position(2.2f,5.0f,1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(1.7f,5.0f,1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(1.2f,5.0f,1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(0.7f,5.0f,1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(0.2f,5.0f,1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(-0.3f,5.0f,1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(2.2f,5.0f,-1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(1.7f,5.0f,-1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(1.2f,5.0f,-1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(0.7f,5.0f,-1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(0.2f,5.0f,-1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		position.Set(-0.3f,5.0f,-1.0f);
		phArticulatedObject::CreateHuman(*gravityWorld,position,rotation);
		int numStairs = 15;
		position.Set(0.0f,0.0f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		float stairHeight = 0.2f;
		float stairDepth = 0.2f;
		gravityWorld->ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.z += 0.8f;
		gravityWorld->ConstructRailing(position,rotation);
		position.z -= 1.6f;
		gravityWorld->ConstructRailing(position,rotation);
		position.Set(3.0f,3.0f,0.0f);
		rotation.Set(0.0f,0.0f,0.5f*PI);
		gravityWorld->CreateFixedObject("small_plane",position,false,rotation);
		position.Set(-3.0f,3.0f,0.0f);
		rotation.Set(0.0f,0.0f,-0.5f*PI);
		gravityWorld->CreateFixedObject("small_plane",position,false,rotation);
		position.Set(0.0f,6.0f,0.0f);
		rotation.Set(0.0f,0.0f,PI);
		gravityWorld->CreateFixedObject("small_plane",position,false,rotation);
		position.Set(0.0f,3.0f,-3.0f);
		rotation.Set(0.5f*PI,0.0f,0.0f);
		gravityWorld->CreateFixedObject("small_plane",position,false,rotation);
		position.Set(0.0f,3.0f,3.0f);
		rotation.Set(-0.5f*PI,0.0f,0.0f);
		gravityWorld->CreateFixedObject("small_plane",position,false,rotation);
		position.Set(-1.5f,1.0f,0.0f);
		int height = 3;
		gravityWorld->ConstructPyramid(position,height,false,"crate");

		m_GravityWorldActive = true;
		m_SaloonWorldActive = false;

		return gravityWorld;
	}

	phDemoWorld* CreateZStairs ()
	{
		phDemoWorld& zStairsWorld = *CreateNewDemoWorld("stairs","big_wall");
		int numStairs = 15;
		Vector3 rotation(0.5f*PI,0.0f,0.0f);
		Vector3 position(0.0f,2.2f,5.0f);
		phArticulatedObject::CreateHuman(zStairsWorld,position,rotation);
		position.Set(0.0f,1.8f,4.6f);
		phArticulatedObject::CreateHuman(zStairsWorld,position,rotation);
		position.Set(0.0f,1.4f,4.2f);
		phArticulatedObject::CreateHuman(zStairsWorld,position,rotation);
		position.Set(0.0f,0.0f,0.0f);
		float stairHeight = 0.2f;
		float stairDepth = 0.2f;
		zStairsWorld.ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.x += 0.8f;
		zStairsWorld.ConstructRailing(position,rotation);
		position.x -= 1.6f;
		zStairsWorld.ConstructRailing(position,rotation);

		Vector3 zGravity(0.0f,0.0f,GRAVITY);
		phSimulator::SetGravity(zGravity);
		SetUnitUp(ZAXIS);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &zStairsWorld;
	}

	phDemoWorld* CreateDogpile ()
	{
		phDemoWorld& dogpileWorld = *CreateNewDemoWorld("dogpile",NULL,ORIGIN,3000,3000,6000,Vector3(-999.0f, -999.0f, -999.0f), Vector3(999.0f, 999.0f, 999.0f), 16, 10*1024*1024, 10240, 256);
		Vector3 position(0.0f,1.0f,0.0f);
		Vector3 rotation(1.0f,0.0f,0.0f);
		for (int i=0; i<9; i++)
		{
			position.y += 0.5f;
			//rotation.z += 0.1f;
			phArticulatedObject::CreateHuman(dogpileWorld,position,rotation);
		}

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &dogpileWorld;
	}

	phDemoWorld* CreateRagdollWorld ()
	{
		phDemoWorld& ragdollWorld = *CreateNewDemoWorld("ragdoll",NULL,ORIGIN,3000,3000,6000,Vector3(-999.0f, -999.0f, -999.0f), Vector3(999.0f, 999.0f, 999.0f), 16, 10*1024*1024, 10240, 256);
		Vector3 rotation(0.0f,0.0f,0.0f);

		//phConstraintMgr* pConstraintMgr=PHCONSTRAINT;

		const float xStart=-8;
		const float yStart=3;
		const float zStart=-8;
		int i;
		for(i=0;i<4;i++)
		{
			int j;
			for(j=0;j<5;j++)
			{
				Vector3 position;
				position.x=xStart+i*(16.0f/5.0f);
				position.y=yStart;
				position.z=zStart+j*(16.0f/5.0f);
				phArticulatedObject::CreateHuman(ragdollWorld,position,rotation);
			}
		}


		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &ragdollWorld;
	}


	phDemoWorld* CreatePrismaticJoints ()
	{
		phDemoWorld& prismWorld = *CreateNewDemoWorld("prism");
		Vector3 position(0.0f,2.0f,0.0f);
		Vector3 rotation(0.0f,0.0f,0.0f);
		bool withDoors = false;
		phArticulatedObject::CreateDresser(prismWorld,"dresser",position,rotation,withDoors);
		position.Set(2.0f,2.0f,0.0f);
		withDoors = true;
		phArticulatedObject::CreateDresser(prismWorld,"wardrobe",position,rotation,withDoors);

		m_GravityWorldActive = false;
		m_SaloonWorldActive = false;

		return &prismWorld;
	}


	virtual void InitClient ()
	{
		physicsSampleManager::InitClient();

		// Disable breaking to save a couple of percent speed
		phContactMgr::SetBreakingEnabled(false);

		////////////////
		// common variables
		Vector3 position,rotation,worldAttachPoint,frequency,amplitude,velocity,cgOffset;
	
		//////////////////
		// 0: saloon world
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateSaloon));

		/////////////////////
		// 1: staircase world
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateStaircase));

		////////////////////////
		// 2: pommel horse world
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreatePommelHorse));

		/////////////////////////////////////////////////
		// 3: ragdoll and sphere hanging from constraints
//		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateRagdollAndSphere));

		//////////////////////////////////////////////
		// 4: ragdoll hanging from a moving constraint
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateShakenRagdoll));

		/////////////////////////////////
		// 5: ragdoll attached to a crate
//		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateRagdollAndCrate));

		/////////////////
		// 6: wall world
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateRagdollAndWall));

		//////////////////////
		// 7: changing gravity
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateChangingGravity));

		////////////////////////////
		// 9: puppet staircase world
/*		phDemoWorld& puppetStairsWorld = MakeNewDemoWorld("puppetStairs");
		numStairs = 15;
		position.Set(2.2f,5.0f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		human = phArticulatedObject::CreateHuman(puppetStairsWorld,position,rotation);
		position.Set(0.0f,0.0f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		stairHeight = 0.2f;
		stairDepth = 0.2f;
		puppetStairsWorld.ConstructBoxStairs("stair",numStairs,position,rotation,stairHeight,stairDepth);
		position.z += 0.8f;
		puppetStairsWorld.ConstructRailing(position,rotation);
		position.z -= 1.6f;
		puppetStairsWorld.ConstructRailing(position,rotation);
		position.Set(0.0f,2.0f,2.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		phArticulatedObject* puppetHuman = phArticulatedObject::CreateHuman(puppetStairsWorld,position,rotation);
		human->SetPuppetBody(&puppetHuman->GetBody());
		constraintMgr = puppetStairsWorld.GetSimulator()->GetConstraintMgr();
		position.Set(0.0f,2.6f,2.0f);
		component = 1;
		constraintMgr->AttachObjectToWorld('SaP0', position,puppetHuman->GetPhysInst(),component);

		//////////////////////////////
		// 10: box collision test world
		phDemoWorld& boxCollisionTestWorld = MakeNewDemoWorld("boxCollisionTest","splashot");
		position.Set(0.0f,0.4f,0.0f);
		rotation.Set(0.5f*PI,0.0f,0.0f);
		phArticulatedObject::CreateHuman(boxCollisionTestWorld,position,rotation);
		position.Set(1.0f,1.8f,-0.6f);
		rotation.Set(1.0f,0.0f,0.0f);
		boxCollisionTestWorld.CreateObject("crate",position,alignBottom,rotation);*/

		////////////////////////////
		// 8: z-gravity staircase world
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateZStairs));

		////////////////////
		// 9: dogpile world
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateDogpile));

		////////////////////
		// 10: non-colliding ragdolls
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreateRagdollWorld));

		//////////////////////
		// 11: prismatic joints
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedBodyManager::CreatePrismaticJoints));

		///////////////////////
		// 11: prismatic wheels
	//	phDemoWorld& prismCarWorld = MakeNewDemoWorld("prismatic car");
	//	position.Set(0.0f,0.0f,0.0f);
	//	phJoint::JointType jointTypeList[] = {phJoint::PRISM_JNT,phJoint::PRISM_JNT,phJoint::PRISM_JNT,phJoint::PRISM_JNT};
	//	phArticulatedObject* prismCar = phArticulatedObject::CreateFromBound(prismCarWorld,"car_prism",jointTypeList,position);
	//	const float positionStrength = 2000.0f;
	//	const float speedStrength = 200.0f;
	//	for (int wheelIndex=0; wheelIndex<4; wheelIndex++)
	//	{
	//		phPrismaticJoint& wheelJoint = prismCar->GetBody().GetPrismaticJoint(wheelIndex);
	//		wheelJoint.SetStiffness(0.0f);
	//		wheelJoint.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
	//		wheelJoint.SetMusclePositionStrength(positionStrength);
	//		wheelJoint.SetMuscleTargetPosition(0.0f);
	//		wheelJoint.SetMuscleSpeedStrength(speedStrength);
	//		wheelJoint.SetMuscleTargetSpeed(0.0f);
	//	}

		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();
	}

	void SetMuscles (phArticulatedBody& body)
	{
		if (body.GetNumJoints() > 0 && !phDemoWorld::GetVerySimpleRagdolls())
		{
			Vector3 muscleTargetAngle(0.0f,0.25f*PI,0.0f);
			float muscleAngleStrength = 200.0f;
			float muscleSpeedStrength = 20.0f;
			phJoint3Dof& rightShoulder = body.GetJoint3Dof(2);
			rightShoulder.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			rightShoulder.SetMuscleAngleStrength(muscleAngleStrength);
			rightShoulder.SetMuscleSpeedStrength(muscleSpeedStrength);
			rightShoulder.SetMuscleTargetAngle(muscleTargetAngle);
			rightShoulder.SetMuscleTargetSpeed(ORIGIN);
			muscleTargetAngle.x = -muscleTargetAngle.x;
			muscleTargetAngle.y = -muscleTargetAngle.y;
			phJoint3Dof& leftShoulder = body.GetJoint3Dof(4);
			leftShoulder.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			leftShoulder.SetMuscleAngleStrength(muscleAngleStrength);
			leftShoulder.SetMuscleSpeedStrength(muscleSpeedStrength);
			leftShoulder.SetMuscleTargetAngle(muscleTargetAngle);
			leftShoulder.SetMuscleTargetSpeed(ORIGIN);

			muscleTargetAngle.Set(-0.5f*PI,0.0f,0.0f);
			phJoint3Dof& rightHip = body.GetJoint3Dof(6);
			rightHip.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			rightHip.SetMuscleAngleStrength(muscleAngleStrength);
			rightHip.SetMuscleSpeedStrength(muscleSpeedStrength);
			rightHip.SetMuscleTargetAngle(muscleTargetAngle);
			rightHip.SetMuscleTargetSpeed(ORIGIN);
			phJoint3Dof& leftHip = body.GetJoint3Dof(8);
			leftHip.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			leftHip.SetMuscleAngleStrength(muscleAngleStrength);
			leftHip.SetMuscleSpeedStrength(muscleSpeedStrength);
			leftHip.SetMuscleTargetAngle(muscleTargetAngle);
			leftHip.SetMuscleTargetSpeed(ORIGIN);

			float elbowTargetAngle = 0.5f*PI;
			phJoint1Dof& rightElbow = body.GetJoint1Dof(3);
			rightElbow.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			rightElbow.SetMuscleAngleStrength(muscleAngleStrength);
			rightElbow.SetMuscleSpeedStrength(muscleSpeedStrength);
			rightElbow.SetMuscleTargetAngle(elbowTargetAngle);
			rightElbow.SetMuscleTargetSpeed(0.0f);
			phJoint1Dof& leftElbow = body.GetJoint1Dof(5);
			leftElbow.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			leftElbow.SetMuscleAngleStrength(muscleAngleStrength);
			leftElbow.SetMuscleSpeedStrength(muscleSpeedStrength);
			leftElbow.SetMuscleTargetAngle(-elbowTargetAngle);
			leftElbow.SetMuscleTargetSpeed(0.0f);

			float kneeTargetAngle = 0.5f*PI;
			phJoint1Dof& rightKnee = body.GetJoint1Dof(7);
			rightKnee.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			rightKnee.SetMuscleAngleStrength(muscleAngleStrength);
			rightKnee.SetMuscleSpeedStrength(muscleSpeedStrength);
			rightKnee.SetMuscleTargetAngle(kneeTargetAngle);
			rightKnee.SetMuscleTargetSpeed(0.0f);
			phJoint1Dof& leftKnee = body.GetJoint1Dof(9);
			leftKnee.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
			leftKnee.SetMuscleAngleStrength(muscleAngleStrength);
			leftKnee.SetMuscleSpeedStrength(muscleSpeedStrength);
			leftKnee.SetMuscleTargetAngle(kneeTargetAngle);
			leftKnee.SetMuscleTargetSpeed(0.0f);
		}
	}

	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,4.0f,6.0f);
		Vector3 lookTo(0.0f,2.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update ()
	{

	#if NM_TEST_ROTATIONS
		phArticulatedBody& testBody = m_TestHuman->GetBody();
		phJoint3Dof& joint = testBody.GetJoint3Dof(2);
		joint.ComputeCurrentLeanAndTwist();
		Vec3V currentLeanTwistV,lean12TwistRate;
		joint.ComputeCurrentLeanAnglesAndRates(currentLeanTwistV,lean12TwistRate);
		QuatV actualQV = geomVectors::LeanAndTwistToQuatV(currentLeanTwistV.GetX(),currentLeanTwistV.GetY(),currentLeanTwistV.GetZ()); 
		Quaternion actualQ = RCC_QUATERNION(actualQV);
		Matrix34 actualTM; 
		actualTM.FromQuaternion(actualQ);
		Matrix34 orientParent = joint.GetOrientationParent();
		Matrix34 orientChild;
		joint.GetOrientationChild(orientChild);

		Matrix34 parentMtx = MAT34V_TO_MATRIX34(joint.GetParentLink()->GetMatrix());
		Matrix34 childMtx = MAT34V_TO_MATRIX34(joint.GetChildLink()->GetMatrix());
		parentMtx.Dot3x3(orientParent);
		childMtx.Dot3x3(orientChild);
		parentMtx.Transpose();
		childMtx.Transpose();
		actualTM.Dot(parentMtx);

		actualTM.d.Zero();
		childMtx.d.Zero();

		if (!childMtx.IsClose(actualTM,0.00001f))
		{
			int d;
			d=0;
		}
	#endif

		if (m_GravityWorldActive)
		{
			// Rotate gravity.
			const float angSpeed = 1.2f;
			Vector3 gravity(phSimulator::GetGravity());
			gravity.RotateZ(angSpeed*TIME.GetSeconds());
			phSimulator::SetGravity(gravity);
		}

		physicsSampleManager::Update();
	}

	virtual void Reset ()
	{
		physicsSampleManager::Reset();
		if (m_SaloonWorldActive && m_SpinningCollider)
		{
			m_SpinningCollider->SetVelocity(m_InitialVelocity);
			m_SpinningCollider->SetAngVelocity(m_InitialAngVelocity);

			// Resize the ragdoll bounds on reset.
		/*	ScalarV scale = ScalarVFromF32(1.5f);
			phArticulatedBody& body = *m_SpinningCollider->GetBody();
			phArchetypePhys& archetype = *(phArchetypePhys*)m_SpinningCollider->GetInstance()->GetArchetype();
			phBoundComposite& bound = *(phBoundComposite*)archetype.GetBound();
			int numParts = bound.GetNumBounds();
			for (int partIndex=0; partIndex<numParts; partIndex++)
			{
				ScalarV mass = Scale(scale,ScalarVFromF32(archetype.GetMass()));
				archetype.SetMass(mass);
				phBound* part = bound.GetBound(partIndex);
				if (part->GetType()==phBound::CAPSULE)
				{
					phBoundCapsule& capsule = *(phBoundCapsule*)part;
					ScalarV radius = Scale(scale,capsule.GetCapsuleRadius());
					ScalarV length = Scale(scale,capsule.GetCapsuleLength());
					capsule.SetCapsuleSize(radius,length);
				}
				else if (part->GetType()==phBound::SPHERE)
				{
					phBoundSphere& sphere = *(phBoundSphere*)part;
					ScalarV radius = Scale(scale,ScalarVFromF32(sphere.GetRadius()));
					sphere.SetSphereRadius(radius);
				}

				phArticulatedBodyPart& bodyPart = body.GetLink(partIndex);
				mass = Scale(scale,bodyPart.GetMass());
				Vec3V angInertia = Scale(bodyPart.GetAngInertia(),scale);
				bodyPart.SetMassAndAngInertia(mass,angInertia);
			}*/
		}
	}

	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}

protected:
	bool m_GravityWorldActive;
	bool m_SaloonWorldActive;
	phArticulatedCollider* m_SpinningCollider;
	Vector3 m_InitialVelocity,m_InitialAngVelocity;
	phArticulatedObject* m_TestHuman;
};

} // namespace ragesamples


int Main()
{
	ragesamples::ArticulatedBodyManager sampleArticulated;
	sampleArticulated.Init();
	sampleArticulated.UpdateLoop();
	sampleArticulated.Shutdown();
    return 0;
}
