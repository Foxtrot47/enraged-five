//
// physics/archmgr.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PHYSICS_ARCHMGR_H
#define PHYSICS_ARCHMGR_H

#include "atl/array.h"
#include "atl/hashsimple.h"
#include "data/base.h"
#include "phbound/bound.h"
#include "physics/archetype.h"

namespace rage {

#if __BANK
class bkGroup;
#endif
class phBoundComposite;
class fiTokenizer;


////////////////////////////////////////////////////////////////
// phArchetypeMgr
//
//	The archetype manager is a general-purpose manager for all 
//  bounds being used in an application.  Like a renderable model, 
//  a base bound only needs to be opened and stored once, but can 
//  be referenced by many objects.  the archetype manager provides the 
//  following pieces of functionality:
//		- object loading and initialization
//		- precomputation of collider data for faster instantiation
//

// singleton
#define ARCHMGR	(phArchetypeMgr::GetInstance())

/* 
Purpose: The archetype manager is a general-purpose manager for all bounds being used in an application.
	Like a renderable model, a base bound only needs to be opened and stored once, but can be referenced
	by many objects.  The archetype manager provides object loading and initialization, and precomputation
	of collider data for faster instantiation.
<FLAG Component>
*/
class phArchetypeMgr : public datBase
{
public:
	static bool InstanceExists ()									{ return Instance!=NULL; }
	static phArchetypeMgr & GetInstance ()						{ FastAssert(Instance); return *Instance; }

	// PURPOSE: Create the singleton physics archetype manager.
	// PARAMS
    //	maxArchetypes - optional maximum allowed number of physics archetypes (default value is 512)
	static phArchetypeMgr & CreateInstance (u16 maxArchetypes=512);

	static void DeleteInstance ();

public:
	phArchetypeMgr (u16 maxArchetypes);
	~phArchetypeMgr ();

	// PURPOSE: initialize the list of physics archetypes and the list of archetype names, and initialize the hash table
	// PARAMS:
    //	maxArchetypes - the maximum allowed number of physics archetypes
	void Initialize (u16 maxArchetypes);


	// PURPOSE: delete all the physics archetypes that are not in use, giving warnings for any that are in use, and delete the hash table information, and the archetype manager singleton
	// NOTES:
	//	Each archetype has a count of the number of physics instances that use it, plus one for the archetype
	//	manager.  Any archetypes with outstanding instance references will not be deleted here.
	void Shutdown ();

	void RecycleUnused (bool deleteNonexistent=false);				// move unreferenced archetypes back to usable state

	phArchetype * FindArchetype (const char * name);
	phArchetype * GetArchetype (const char * fileName, bool initPhysFields=true, fiTokenizer * token=NULL,
								int classType=phArchetype::INVALID_ARCHETYPE);
	phArchetype * GetUniqueArchetype (const char * fileName, bool initPhysFields=true, fiTokenizer * token=NULL,
										int classType=phArchetype::INVALID_ARCHETYPE);
	phArchetype * GetArchetypeNoWarning (const char * fileName, bool initPhysFields=true, fiTokenizer * token=NULL,
											int classType=phArchetype::INVALID_ARCHETYPE);
	phArchetype * RegisterArchetype (phBound * bound, const char * name, bool initPhysFields=false,
										int classType=phArchetype::ARCHETYPE_PHYS, float density=phArchetypePhys::GetDefaultDensity());
	phArchetype * RegisterArchetype (phBound * bound, const char * name, float mass, const Vector3 &angInertia,
										bool initPhysFields=true, int classType=phArchetype::ARCHETYPE_PHYS);
	phArchetype * RegisterNewArchetype (phArchetype * arch, const char * name);
	phArchetype * RegisterNewArchetype (phBound * bound, const char * name, bool initPhysFields=false, int classType=phArchetype::ARCHETYPE_PHYS);

	bool RemoveArchetype (const phArchetype *arch);
	bool RemoveArchetype (const char *name);

	void PruneHashtable();			// clean out m_Nonexistent archtype (load failures)

#if __BANK
	void AddWidgets(bkBank& bank);
	static bool WidgetsOpen;
#endif

#if __DEV
	const char* GetName (const phArchetype* archetype) const;
#endif

protected:
	struct HashInfo
	{
		char * Name;
		phArchetype * Archetype;
	};

protected:
	phArchetype * AddArchetype (phArchetype * arch, const char * name);
	HashInfo * AddHashInfoToTable (const char* id);
	bool RemoveHashInfoFromTable (int index);
	int GetFirstUnusedHashInfo () const;
	void PostLoadCompute (phArchetypePhys* archetype, phBound *bound, float density, bool initPhysFields);

#if __BANK
protected:
	void RefreshArchetypeBank();
	void KillWidgets();
	void AddArchetypeWidgets();
	int FindNextAlphabetizedArchetype(const char* name=NULL) const;

	bkBank *Bank;
	atArray<bkGroup*> WidgetGroups;
#endif

protected:
	HashInfo * ArchetypeHashInfo;
	atMap<const char*, HashInfo*> * ArchetypeHashTable;
	u16 MaxNumArchetypes;
#if __BANK
	u8 *Pad;
#else
	u8 LamePad[2];
#endif

	// PURPOSE: a dummy physics archetype entered in the hash table to avoid duplicate failed load attempts
	phArchetype m_Nonexistent;

	static phArchetypeMgr * Instance;
};


} // namespace rage

#endif
