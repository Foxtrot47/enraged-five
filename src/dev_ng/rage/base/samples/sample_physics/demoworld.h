// 
// sample_physics/demoworld.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_PHYSICS_DEMOWORLD_H
#define SAMPLE_PHYSICS_DEMOWORLD_H

#include "demoobject.h"

#include "atl/atfunctor.h"
#include "atl/bitset.h"
#include "atl/functor.h"
#include "bank/bank.h"
#include "devcam/cammgr.h"
#include "input/mapper.h"
#include "pheffects/mouseinput.h"
#include "physics/debugEvents.h"
#include "physics/instbreakable.h"
#include "physics/levelbase.h"
#include "physics/leveldefs.h"
#include "string/string.h"
#include "system/timer.h"
#include "vectormath/classes.h"

namespace rage {

class bkBank;
class gzGizmo;
class phCollider;
class phInst;
class phSimulator;
class Matrix34;
class rmcDrawable;
template<class _Key, class _Data> class atBinTree;

}

namespace ragesamples {

using namespace rage;

/////////////////////////
// application includes

#define DEFAULT_INACTIVE_IMPULSE_LIMIT	1000.0f
#define	DEFAULT_ACTIVE_IMPULSE_LIMIT	-1.0f
#define	DEMOWORLD_MAX_NUM_GIZMOS		32

/////////////////////////


class phDemoWorld
{
public:
	static void InitClass(); // initialize static data
	static void ClassShutdown(); // release static data

	phDemoWorld(const char* name = NULL);						// constructor
	virtual ~phDemoWorld() { }
	virtual void Init(const int knMaxOctreeNodes, int knMaxActiveObjects = 500, int maxObjects = 500, Vec3V_In worldMin=Vec3V(-999.0f,-999.0f,-999.0f), Vec3V_In worldMax=Vec3V(999.0f,999.0f,999.0f), const int maxInstBehaviors=16, int scratchpadSize = 6*1024*1024, int maxManifolds = 1024, int maxExternVelManifolds = 256, int numManagedConstraints = 32);
	virtual void Init(phLevelNew* level, phSimulator* sim, int maxObjects = 500);
																// init a demo

    phLevelNew* GetPhLevel()                                    { return m_PhysLevel; }

	void InitUpdateObjects(int maxUpdateObjects = 60);			// init objects that need to be updated each frame
	virtual void Shutdown();									// shutdown a demo
	void Activate (dcamCamMgr* cameraManager=NULL);				// set as active demo
	void Deactivate();											// deactivate this demo
	virtual void Reset();										// reset this demo
	virtual void Update();										// iterate frame
	void UpdateInput();											// capture/process input
	virtual void UpdateMouse(Vec::V3Param128 timeStep);									// capture/process mouse input
	static phDemoWorld* GetActiveDemo()							{ return sm_ActiveDemo; }
	static void ClearActiveDemo()								{ sm_ActiveDemo = NULL; }
    static rmcDrawable* GetDrawable(const char* name);
	phExplosionMgr* GetExplosionMgr ();
	static bool GetVerySimpleRagdolls()							{ return sm_VerySimpleRagdolls; }
	static bool GetArticulatedRagdolls ()						{ return sm_ArticulatedRagdolls; }
	static bool GetAddRotationLimitsToConstraintRagdolls()		{ return sm_ConstraintRotationLimitRagdolls; }

	typedef atFunctor1<void, float> UpdateCallback;
	void SetUpdateCallbacks(UpdateCallback preUpdate, UpdateCallback postUpdate);

	// drawing
	virtual void Draw();										// Draw all the demo objects plus any debug info
	static bool RedrawWhilePaused()								{ return sm_RedrawWhilePaused; }
	const char* GetName() const									{ return m_Name; }
	float GetFrameTime() const									{ return m_FrameTime; }
	float GetMaximumFrameTime() const							{ return sm_MaximumFrameTime; }
    void SetMaximumFrameTime( float time )                      { sm_MaximumFrameTime = time; }
	int GetOversampling() const									{ return sm_Oversampling; }
	float GetSimTimeLastFrame() const							{ return m_SimTimeLastFrame; }

	// physics control
	static void Pause()											{ sm_Paused = !sm_Paused; }
	void FrameAdvance()											{ m_AdvanceFrame = true; }
	float GetElapsedTime () const								{ return m_ElapsedTime; }
	void SetFramerate(float fps=60.0f);							// set the time per frame
	void SetTimeWarp(float timeWarp=1.0f);						// set fast/slow motion
	float GetTimeWarp() const									{ return sm_TimeWarp; }
	bool GetFixedFrame() const									{ return sm_FixedFrame; }
    bool GetAltFxiedFrame() const                               { return m_AltFixedFrameMode;}
	void SetFixedFrame(bool state,bool altMode = false);
    void SetGravityScale(float gravity=1.0f);					// set high/low gravity
	void SetGravity (const Vector3& gravity)					{ m_Gravity.Set(gravity); }
	float GetGravityScale() const								{ return m_GravityScale; }
	void SetSleepEnable(bool enabled = true);					// enable/disable sleep
	bool ShouldUpdate()	const									{ return (!sm_Paused || m_AdvanceFrame); }
	bool UpdatedLastFrame() const								{ return m_UpdatedLastFrame; }
	void EnableProfiler(bool enable)							{ m_ProfilerEnabled = enable; }
	const phMouseInput::Spring& GetGrabSpring()					{ return m_MouseInput->GetGrabSpring(); }
	void FixGrabForBreak(phInst* brokenInst, const atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS>& brokenParts, phInst* newInst);
	phCollider* GetCollider (phDemoObject* object) const;
	phSimulator* GetSimulator () { return m_PhysSim; }
	void AddTranslationGizmo (Matrix34& controlledMatrix);
	void AddRotationGizmo (Matrix34& controlledMatrix);

	// geometry info
	void GetWorldExtents(Vector3& wMin, Vector3& wMax);		// find the min and max of the world

	//////////////////////////////////////////////////////////////////////////
	// adding objects
	phDemoObject* RequestObject();

	bool AddActiveObject(phDemoObject* object, bool alwaysActive=false);
	void AddInactiveObject(phDemoObject* object);
	void AddFixedObject(phDemoObject* object);
	void InsertFixedObject(phDemoObject* object);
	void RegisterObject(phDemoObject* object, int initialStateFlag=phLevelBase::STATE_FLAG_INACTIVE);
	bool RemoveObject(phDemoObject* object);

	phDemoObject* CreateObject (const char* file, Mat34V_In mtx=Mat34V(V_IDENTITY), bool alignBottom=false,
								bool alwaysActive=false, bool startActive=true, Functor0Ret<phInst*> createFunc=NULL, bool uniqueArchetype=false);
    phDemoObject* CreateObject (const char* file, const Matrix34& mtx, bool alignBottom=false,
								bool alwaysActive=false, bool startActive=true,Functor0Ret<phInst*> createFunc=NULL,bool uniqueArchetype=false);

	phDemoObject* CreateObject(const char* file, Vec3V_In position, bool alignBottom=false, Vec3V_In rotation=Vec3V(V_ZERO),
								bool alwaysActive=false, bool startActive=true, Functor0Ret<phInst*> createFunc=NULL, bool uniqueArchetype=false);
	phDemoObject* CreateObject(const char* file, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN,
								bool alwaysActive=false, bool startActive=true,Functor0Ret<phInst*> createFunc=NULL,bool uniqueArchetype=false);

	phDemoObject* CreateFixedObject(const char* file, Mat34V_In mtx=Mat34V(V_IDENTITY), bool alignBottom=false);
	phDemoObject* CreateFixedObject(const char* file, const Matrix34& mtx, bool alignBottom=false);

	phDemoObject* CreateFixedObject(const char* file, Vec3V_In position, bool alignBottom=false, Vec3V_In rotation=Vec3V(V_ZERO));
	phDemoObject* CreateFixedObject(const char* file, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN);

	phDemoObject* CreateDoor (const char* file, const Vector3& position=ORIGIN, bool alignBottom=false, const Vector3& rotation=ORIGIN);
	phDemoObject* CreateBox(const Vector3& boxExtents, float density, const Matrix34& mtx=M34_IDENTITY, bool alignBottom=false,
		bool alwaysActive=false);
	phDemoObject* CreateBox(const Vector3& boxExtents, float density, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN,
		bool alwaysActive=false);
	phDemoObject* CreateCapsule(float length, float radius, float density, const Matrix34& mtx=M34_IDENTITY, bool alignBottom=false,
		bool alwaysActive=false);
	phDemoObject* CreateCapsule(float length, float radius, float density, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN,
		bool alwaysActive=false);
	phDemoObject* CreateSphere(float radius, float density, const Vector3& position, bool alignBottom=false, bool alwaysActive=false, float maxSpeed=DEFAULT_MAX_SPEED, float maxAngSpeed=DEFAULT_MAX_ANG_SPEED);
	phDemoObject* CreateFixedBox(const Vector3& boxExtents, const Matrix34& mtx=M34_IDENTITY, bool alignBottom=false);
	phDemoObject* CreateFixedBox(const Vector3& boxExtents, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN);
	phSwingingObject* CreateSwingingObject (const char* name, const Vector3& position, const Vector3& rotation, const Vector3& constraintPosition,
											const Vector3& frequency=DEFAULT_SWINGING_FREQUENCY, const Vector3& amplitude=DEFAULT_SWINGING_AMPLITUDE, const Vector3& phaseOffset=ORIGIN);
	phWhirlingObject* CreateWhirlingObject (const char* name, const Vector3& position, const Vector3& rotation, float period, float mag, float phase);
	phDemoObject* CreateRubiks(const Vector3& boxExtents, float spacing, u32 height, u32 width, u32 depth, float density, const Vector3& position, bool alignBottom=false,
										const Vector3& rotation=ORIGIN, bool alwaysActive=false);
	phDemoObject* CreateRubiks(const Vector3& boxExtents, float spacing, u32 height, u32 width, u32 depth, float density, const Matrix34& mtx, bool alignBottom=false,
										bool alwaysActive=false);

#if 0
	phDemoObject* CreateShovableObject(const char* file, const Matrix34& mtx=M34_IDENTITY, bool alignBottom=false);
	phDemoObject* CreateShovableObject(const char* file, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN);
	phDemoObject* CreateBreakableObject(const char* file, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN,
										float inactiveImpulseLimit=DEFAULT_INACTIVE_IMPULSE_LIMIT,
										float activeImpulseLimit=DEFAULT_ACTIVE_IMPULSE_LIMIT);
	phDemoObject* CreateBreakableObject(const char* file, const Matrix34& mtx=M34_IDENTITY, bool alignBottom=false,
										float inactiveImpulseLimit=DEFAULT_INACTIVE_IMPULSE_LIMIT,
										float activeImpulseLimit=DEFAULT_ACTIVE_IMPULSE_LIMIT);
	phDemoObject* CreateFixedObject(const char* file, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN);
	phDemoObject* CreateFixedObject(phArchetype* archetype, const Vector3& position, bool alignBottom=false, const Vector3& rotation=ORIGIN);
	phDemoObject* CreateFixedObject(phArchetype* archetype, const Matrix34& mtx=M34_IDENTITY, bool alignBottom=false);
	phDemoObject* CreateFixedObject(phInst* inst);

	phDemoCreature* CreateCreature(const char* name, float initX, float initZ, const Vector3& rotation=ORIGIN);
	phDemoCreature* CreateCreature(const char* name, const Vector3& position, const Vector3& rotation=ORIGIN);
	phDemoCreature* CreateCreature(const char* name, const Matrix34& mtx=M34_IDENTITY);
	phDemoObject* CreateCrushableObject(const char* file, const char* swapFile, const Vector3& position=ORIGIN,
											const float impulseLimit2=0.0f);
	phDemoObject* CreateBomb(const char* file="box", const char* swapFile="explosion", const Vector3& position=ORIGIN,
								float impulseLimit2=0.0f, bool stateActive=true);
#endif

	void AddUpdateObject(phUpdateObject* object);

	//////////////////////////////////////////////////////////////////////////
	// multi- or special object constructions
#if USE_GEOMETRY_CURVED
	phDemoObject* CreateCurvedGeometryCylinder (Vector3::Param position=ORIGIN, Vector3::Param rotation=ORIGIN, float radius=0.5f, float height=1.5f, float ringCurveRadius=-1.0f, float capsCurveRadius=-1.0f);
#endif
	phDemoObject* ConstructTerrainPlane(bool triangulated=true, u32 includeFlags=INCLUDE_FLAGS_ALL, bool zUp=false, const Vector3& position=ORIGIN);
	void ConstructPyramid(const Vector3& pos, int height, bool rotate=false, const char* archName=NULL, bool zGravity=false, bool xGravity=false);
	void ConstructJenga(const Vector3& pos, int height = 3, int levelSize = 2, const char* archName=NULL);
    void ConstructTower(const Vector3& position, int height, const char* archName=NULL);
    void ConstructRoundTower(const Vector3& position, int height, int around, const Vector3& boxExtents);
	void ConstructWall(const float brickWidth, const float brickHeight, const int wallLength, const int wallHeight, const float brickOffset, const Matrix34& transform);
	void ConstructWall(const float brickWidth, const float brickHeight, const int wallLength, const int wallHeight, const float brickOffset, Vector3::Param position, Vector3::Param rotation=ORIGIN);
	void ConstructRectangularTower(const float brickWidth, const float brickHeight, const int towerLength, const int towerWidth, const int towerHeight, const Matrix34& transform);
	void ConstructRectangularTower(const float brickWidth, const float brickHeight, const int towerLength, const int towerWidth, const int towerHeight, Vector3::Param position, Vector3::Param rotation=ORIGIN);
	void ConstructTable(const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN);
	void ConstructDominoRow(const Vector3& position, int num, float space);
	void ConstructDominoBezier(const Vector3 bezier[4], float delta);
	void ConstructTiers(const Vector3& position, int height);
	void ConstructPegRamp(const Vector3& pos);
	void ConstructBoxOfObjects(const char* file, int num, const Vector3& boxMin, const Vector3& boxMax);
	void ConstructBoxOfStackedObjects(const char* file, int numSpheres, const Vector3& boxMin, const Vector3& boxMax);
	void ConstructTrain();
	void ConstructBowlingPins(int rows, const Vector3& headPos);
	void ConstructBoxStairs (const char* name="box", int numStairs=6, const Vector3& position=ORIGIN,
								const Vector3& rotation=ORIGIN, float height=1.0f, float depth=1.0f, float radius=0.0f);
	void ConstructRailing (const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN, bool sloped=true,
							const char* banisterName="capsule_thin", const char* poleName="capsule_small",
							float height=1.0f, float length=4.0f, float poleSpacing=0.5f);

	void SetUpAxis (int upAxis, float gravity=GRAVITY);
	int GetUpAxis () const;

#if __BANK
    static void BankUpdateFrameRate();
    static void BankUpdateTimeWarp();
	virtual void AddWidgets(bkBank & bank);						// Add the world's widgets to the bank
	static void AddClassWidgets(bkBank & bank);
#endif

    //////////////////////////////////////////////////////////////////////////
    // external interface setup
   
    void RegisterResetWorldClbk(Functor0  clbk)
    {
        m_ResetWorldClbk = clbk;
    }

    void RegisterTakeControlClbk(Functor1<phInst*>  clbk)
    {
        m_TakeControlClbk = clbk;
    }
    
    void RegisterRecordApplyForceClbk(Functor4<int, const Vector3&, const Vector3&, int>   clbk)
    {
        m_ApplyForceClbk = clbk;
    }

    void RegisterRecordApplyImpulseClbk(Functor4<int, const Vector3&, const Vector3&, int>   clbk)
    {
        m_ApplyImpulseClbk = clbk;
    }

    void RegisterReleaseOwnershipClbk(Functor1<phInst*>  clbk)
    {
        m_ReleaseOnwershipClbk = clbk;
    }
    
    void RegisterResetObjectClbk(Functor1<phInst*>  clbk)
    {
        m_ResetObjectClbk = clbk;
    }

protected:

    //callback into external systems to take control of an object.
    void TakeControl(phInst* inst);
    void TakeControlAll();

    bool RecordApplyForce (int levelIndex, const Vector3& force, const Vector3& position, int component=0);
    bool RecordApplyImpulse (int levelIndex, const Vector3& impulse, const Vector3& position, int component=0);

	// static state
	static bool sm_ClassInitialized;							// static: is the class initialized?
	static phDemoWorld* sm_ActiveDemo;							// static: currently active instance
    typedef atBinTree<ConstString, rmcDrawable*> DrawableTree;
    static DrawableTree* sm_DrawableDictionary;
	PDR_ONLY(static debugPlayback::DebugRecorder* sm_DebugRecorder;)

	// physics
	phLevelNew* m_PhysLevel;
	phSimulator* m_PhysSim;										// the phSimulator being used

	// state
	ConstString m_Name;
	bool m_Active;												// am I active?
	bool m_ProfilerEnabled;										// Should the demo world own/control the profiler?
	bool m_CaptureReset;										// should I capture reset keys?
	float m_BaseFrameRate;										// unwarped frames per (wall clock
	float m_FrameRate;											// frames per (wall clock) second
	float m_SimTimePerFrame;									// time per frame for the simulator
	float m_SimTimeLastFrame;									// actual time used for the last simulator update
	static int sm_AddAndDeleteObjects;							// how many objects to randomly add and delete one object per frame, to stress test the simulator and physics level
	static float sm_TimeWarp;									// scaling factor for frame/clock time
	static bool sm_FixedFrame;									// whether the simulator should run with a fixed frame step
    static float sm_activeWorldFrameRate;                       // set to the m_BaseFrameRate of the active phDemoWorld
	static float sm_MaximumFrameTime;							// the frame time above which we clamp
	static float sm_FixedFrameAltStep;                          // the fixed timestep for the alternate fixed frame mode.
    static int   sm_FixedFrameStepLimit;                        // max num steps we can take in a frame
    static float sm_FixedFrameStepMultiplier;                   // how much to multiply the step amount by to try to catch up
    static int sm_Oversampling;									// the number of times Update is called per frame
	static bool sm_RedrawWhilePaused;							// whether to redraw profile drawing every frame
	static bool sm_RedrawNextFrameWhilePaused;					// whether to redraw just the next frame during a pause
	static bool sm_DrawLabels;									// whether to draw optional labels on phDemoObject
	static bool sm_ArticulatedRagdolls;							// Create ragdolls with articulated bodies...false for constraints
	static bool sm_ConstraintRotationLimitRagdolls;				// Add rotation limit constraints to constraint ragdolls
	static bool sm_VerySimpleRagdolls;							// Ragdolls with only 6 bones
	static bool sm_Paused;										// paused?
	bool m_AdvanceFrame;										// frame advance called?
	bool m_UpdatedLastFrame;									// was ShouldUpdate() true on the most recent frame?
	bool m_FirstFrameSinceActivation;							// Is this the first frame since this world was activated?
	float m_ElapsedTime;										// total update time since start or most recent reset

	UpdateCallback m_PreUpdate;
	UpdateCallback m_PostUpdate;

	// object information
	int m_MaxObjects;											// maximum number of objects
	int m_NumObjects;											// number of objects
	phDemoObject** m_Objects;									// objects in the world
	int m_MaxUpdateObjects;										// maximum update objects
	int m_NumUpdateObjects;										// number of update objects
	phUpdateObject** m_UpdateObjects;							// objects that need update calls

	ioMapper m_Mapper;
	ioValue m_ActivateAll;
	ioValue m_DeactivateAll;

	// Gravity control
	ioValue m_DecreaseGravity;
	ioValue m_IncreaseGravity;
	float m_GravityScale;
	Vector3 m_Gravity;

	// Time control
	ioValue  m_Pause;
	ioValue  m_FrameAdvance;
	ioValue  m_ToggleFixedFrame;
	ioValue  m_TimewarpSlower;
	ioValue  m_TimewarpFaster;
	ioValue  m_OversampleUp;
	ioValue  m_OversampleDown;
	ioValue  m_AdjustMaximumFrameTime;
	float    m_AdvanceTimer;
	float    m_AdvanceRepeatDelay;
	sysTimer m_FrameTimer;
	float    m_FrameTime;

	// mouse click stuff
	phMouseInput* m_MouseInput;

	gzGizmo* m_Gizmos[DEMOWORLD_MAX_NUM_GIZMOS];
	int m_NumGizmos;

	// PURPOSE: the up direction, for controlling gravity and camera motion (0==x, 1==y and 2==z)
	int m_UpAxis;

    bool m_AltFixedFrameMode;

    Functor0                                            m_ResetWorldClbk;                   // Functor to external function to reset the world
    Functor1<phInst*>                                   m_TakeControlClbk;                  // Functor to external function to take control of a phInst.
    Functor1<phInst*>                                   m_ReleaseOnwershipClbk;             // Functor to external function to take control of a phInst.
    Functor1<phInst*>                                   m_ResetObjectClbk;                  // Functor to external function to reset the phInst
    Functor4<int, const Vector3&, const Vector3&, int>  m_ApplyForceClbk;                   // Functor to external function to record apply force
    Functor4<int, const Vector3&, const Vector3&, int>  m_ApplyImpulseClbk;                 // Functor to external function to record apply impulse

#if __BANK
	// PURPOSE: Self-created bank for all physics widgets.
	static bkBank * sm_Bank;

	int m_CurrentObjectBank;									// index of the object currently displayed in the banks
	bkBank* m_ObjectBank;										// pointer to the bank for the object widgets.
	int m_CurrentUpdateObjectBank;								// index of the object currently displayed in the banks
	bkBank* m_UpdateObjectBank;									// pointer to the bank for the object widgets.
	static void ObjectBankCreation(phDemoWorld*);				// Function to create object bank
	static void UpdateObjectBankCreation(phDemoWorld*);			// Function to create object bank
	static void ResetFromBankCallback(phDemoWorld*);			// Resets the level - This can be removed if we inherited from Base
#endif
};


class phDemoMultiWorld
{
public:
	static const int MAX_DEMOS = 100;

	phDemoMultiWorld();
	~phDemoMultiWorld();

	void Reset();
	void Update (dcamCamMgr* cameraManager=NULL);
	void NextWorld (dcamCamMgr* cameraManager=NULL);
	void PreviousWorld (dcamCamMgr* cameraManager=NULL);
	void UpdateMouse(Vec::V3Param128 timeStep);
	void UpdateMouse();
	void Draw();
	void Shutdown();

	phDemoWorld* GetCurrentWorld();
	int GetCurrentNumber();
	int GetNumDemos();
	void SetCurrentDemo(int i);
	void SetCurrentDemo(const phDemoWorld* demoWorld);

	typedef atDelegate<phDemoWorld* (void)> Factory;

	void AddDemo(phDemoWorld* demo);
	void AddDemo(Factory demo);

	phDemoWorld*& operator[](int demo);

private:
	int				m_NumDemos;
	int				m_CurrentDemo;
	phDemoWorld*	m_Demos[MAX_DEMOS];
	Factory			m_DemoFactories[MAX_DEMOS];
	phDemoWorld*	m_FactoryProducedDemo;

	ioMapper		m_Mapper;
	ioValue			m_NextWorld;
	ioValue			m_PreviousWorld;
	ioValue			m_Reset;
};

int GetNumberPressed();										// convert keyboard char to 1-10 number

} // namespace ragesamples

#endif