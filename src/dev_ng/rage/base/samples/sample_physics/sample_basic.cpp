//
// sample_physics/sample_basic.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Basic Physics
// PURPOSE:
//		This sample shows how to create a physics level, populate it with some interacting objects,
//		update it, render it and shut it down in ten easy steps.


#include "diag/output.h"
#include "file/asset.h"
#include "grmodel/setup.h"
#include "input/mouse.h"
#include "math/random.h"
#include "phbound/bound.h"
#include "phcore/materialmgr.h"
#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "phsolver/contactmgr.h"
#include "physics/collider.h"
#include "physics/intersection.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/rageroot.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"


PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);
EXT_PFD_DECLARE_ITEM(Active);
EXT_PFD_DECLARE_ITEM(Inactive);
EXT_PFD_DECLARE_ITEM(Fixed);
EXT_PFD_DECLARE_ITEM(PhysicsDemoWorld);

}

namespace ragesamples {

using namespace rage;


////////////////////////////////////////////////////////////////
// 

class basicPhysicsSampleManager : public grcSampleManager
{
public:
	void InitClient()
	{
		// Set the sample to run at 30 frames per second of simulation time. The apparent speed will be faster or slower depending on
		// the time needed to compute one frame. TIME.SetRealTimeMode(30.0f) would fix the apparent speed at 30 fps and vary the
		// time spent computing each frame.
		TIME.SetFixedFrameMode(30.0f);

        phConfig::EnableRefCounting();

#if __PFDRAW
		// STEP #1. Initialize profile drawing.
		//-- Profile drawing is used to draw physics bounds. It is useful in games to diagnose any problems
		// involving interactions with physical objects. __PFDRAW is defined in rage/base/src/profile/drawcore.h,
		// and is normally on in debug and beta builds.

		// Set the number of bytes available for storing profile draw shapes (objects for drawing are collected during the
		// physics updates and drawn together), and initialize the buffer and widgets for profile drawing.
		const int pfDrawBufferSize = 2000000;
		GetRageProfileDraw().Init(pfDrawBufferSize);

		// Enable profile drawing.
		const bool pfDrawEnabled = true;
		GetRageProfileDraw().SetEnabled(pfDrawEnabled);

		// Turn on some drawing groups. Profile drawing is turned off by default, and in release builds it is compiled out.
		// It should only be turned on when needed to solve problems. For this example, it is necessary because some of the
		// objects do not have graphics models.
		PFD_GROUP_ENABLE(Physics, true);
		PFD_GROUP_ENABLE(Bounds, true);
		PFD_ITEM_ENABLE(Solid, true);
		PFD_ITEM_ENABLE(Wireframe, false);
#endif


		// STEP #2. Create and initialize the physics level.
		//-- The physics level (phLevel) keeps track of all the physical objects in the game. It uses a spatial partitioning scheme
		// to speed up queries about object location. It can be used to quickly find objects that are hit by a line segment, or
		// touch a sphere or capsule.
		phLevelNew::SetActiveInstance(rage_new phLevelNew);

		// Set the size of the physics level.
		// When any object goes outside the physics level, it is no longer updated, and virtual phInst::NotifyOutOfWorld() is called.
		// Any object that is added to the physics level outside the extents will cause an assert failure.
		const Vec3V worldExtentsMin(-999.0f,-999.0f,-999.0f);
		const Vec3V worldExtentsMax(999.0f,999.0f,999.0f);
		PHLEVEL->SetExtents(worldExtentsMin,worldExtentsMax);

		// Set the maximum allowed number of physical objects in the physics level. This normally varies from hundreds for a small
		// sample level to hundreds of thousands for a large game level.
		// Exceeding the maximum number of objects causes an assert failure.
		int maxNumObjects = 500;
		PHLEVEL->SetMaxObjects(maxNumObjects);

		// Set the maximum allowed number of physically active objects in the physics level. Physically active means the objects have
		// colliders and are reacting to collisions. This number is normally in the low hundreds.
		// Manually exceeding the maximum number of active objects (by calling AddActiveObject) causes an assert failure. If the maximum number
		// of active objects would be exceeded by the physics simulator activating objects, such as from a collision, then the object will not
		// become active.
		const int maxNumActiveObjects = 100;
		PHLEVEL->SetMaxActive(maxNumActiveObjects);

		// Set the maximum number of occupied nodes in the physics level's octree.
		// An assert failure results if the physics level tries to make more than the maximum number of octree nodes. This will be changed
		// soon to print a warning and not create the needed node instead (17 March 06).
		const int maxOctreeNodes = 1000;
		PHLEVEL->SetNumOctreeNodes(maxOctreeNodes);

		// Create and initialize the physics level's octree and object information, using the parameters set above.
		PHLEVEL->Init();


		// STEP #3. Create and initialize the simulator.
		//-- The physics simulator computes the locations, orientations, and speeds of objects in the physics level,
		// and handles collision detection and responses.

		// Call the static initialization of the simulator.
		phSimulator::InitClass();

		// Create the simulator and set it as the current active simulator, accessible by calling PHSIM->.
		phSimulator* simulator = rage_new phSimulator;
		phSimulator::SetActiveInstance(simulator);

		// Initialize the simulator with the currently active physics level, and the maximum number of managed active objects. This can be different
		// from the physics level's maximum number of active objects because the simulator's number only includes active objects that are managed
		// by the simulator. Users have the freedom to add physically active objects to the physics level with colliders that are not managed by
		// the simulator. This is normally used for derived colliders, such as vehicles and ragdolls.
		phSimulator::InitParams params;
		params.maxManagedColliders = maxNumActiveObjects;
		simulator->Init(PHLEVEL,params);


		// STEP #4. Initialize the material manager.
		//-- Physics bounds contain physics materials to define their properties for collisions (friction and elasticity) and other effects (such as sounds
		// and driving properties). Materials are shared among bounds and stored in the material manager.
		ASSET.PushFolder("materials");
		phMaterialMgrImpl<phMaterial>::Create();
		MATERIALMGR.Load();
		ASSET.PopFolder();


		// STEP #5. Create the terrain and put it in the physics level.
		//-- Load a file called "bound.bnd" from the "assets/physics/big_plane" folder. It has information about the physical shape of an instance
		// that will be used as the terrain. It is a large horizontal square made of two triangles.
		phBound* terrainBound = phBound::Load("physics/big_plane/bound");
		Assert(terrainBound);

		// Set the terrain bound in the terrain archetype. Physics archetypes can hold physical information such as include and type flags, mass
		// and damping, and a pointer to the bound. There is usually a 1-1 correspondence between archetypes and bounds, but it is possible for
		// more than one archetype to share a bound.
        m_TerrainArchetype = rage_new phArchetype;
		m_TerrainArchetype->SetBound(terrainBound);

		// Set the archetype in the physics instance. The physics level keeps track of objects by physics instances.
        m_TerrainInstance = rage_new phInst;
		m_TerrainInstance->SetArchetype(m_TerrainArchetype);

        // Release our ref on the terrain, so that deleting the archetype later also deletes the bound.
        terrainBound->Release();

		// Set the instance's matrix. This contains a 3x3 part for orientation and a position.
		Mat34V instanceMatrix(V_IDENTITY);
		m_TerrainInstance->SetMatrix( instanceMatrix );

		// Insert the terrain instance in the level, so that it will participate in the physics simulation. Since this instance is terrain,
		// AddFixedObject us used, which means that this object is fixed in space.
		PHSIM->AddFixedObject(m_TerrainInstance);


		// STEP #6. Load two objects and put them in the physics level.
		//-- Load the physical bound, and set it in the artchetype, following the same procedure as with the terrain.
		phBound* icosahedronBound = phBound::Load("physics/icosahedron/bound");
        m_ActiveArchetype = rage_new phArchetypePhys;
		m_ActiveArchetype->SetBound(icosahedronBound);

		// Initialize both instances of this moving object with the same archetype and bound.
        m_ActiveInstance1 = rage_new phInst;
		m_ActiveInstance1->SetArchetype(m_ActiveArchetype);
        m_ActiveInstance2 = rage_new phInst;
		m_ActiveInstance2->SetArchetype(m_ActiveArchetype);

        // Release our ref on the bound, so that deleting the archetype later also deletes the bound.
        icosahedronBound->Release();

		// Set the matrices of the two instances.
		Mat34VFromZAxisAngle( instanceMatrix, ScalarVFromF32(0.1f), Vec3V( 0.0f, 3.0f, 0.0f ) );
		//instanceMatrix.MakeTranslate(0.0f, 3.0f, 0.0f);
		//instanceMatrix.RotateZ(0.1f);
		m_ActiveInstance1->SetMatrix( instanceMatrix );
		m_ResetMatrix1 = instanceMatrix;
		instanceMatrix.SetCol3( Vec3V(0.05f, 5.0, 0.05f) );
		//instanceMatrix.MakeTranslate(0.05f, 5.0, 0.05f);
		m_ActiveInstance2->SetMatrix( instanceMatrix );
		m_ResetMatrix2 = instanceMatrix;

		// Place the two objects in the physics level. AddActiveObject is used this time, to tell the simulator to give the instances
		// colliders so that they will be simulated as physically active objects.
		// An optional second parameter in AddActiveObject can be used to make the object always active, which means it will never be
		// put in the inactive state.
		PHSIM->AddActiveObject(m_ActiveInstance1);
		PHSIM->AddActiveObject(m_ActiveInstance2);


		// Map a function keys for resetting the physics world and for grabbing objects with the mouse.
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Reset);
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_LEFT,m_Push);
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_RIGHT,m_Pull);
		// -STOP


		g_ReplayRand.Reset(72);

	#if __DEBUGLOG
		const bool replay = false;
		const bool abortOnFailure = true;
		diagDebugLogInit(replay,abortOnFailure);
		g_ReplayRand.SetReplayDebug(true);
	#endif

	}

	void UpdateMouse()
	{
		if (m_Push.IsPressed() || m_Pull.IsPressed())
		{
			// mouseScreen and mouseFar as where are the world space points on the near plane and far plane respectively which project to the current mouse cursor location.
			Vec3V mouseScreen, mouseFar;
			m_Viewport->ReverseTransformNoWorld(static_cast<float>(ioMouse::GetX()),static_cast<float>(ioMouse::GetY()),mouseScreen,mouseFar);
			PF_DRAW_LINE(PhysicsDemoWorld,*(reinterpret_cast<Vector3*>(&mouseScreen)),*(reinterpret_cast<Vector3*>(&mouseFar)));

			// Establish a world space direction for the mouse click.
			Vec3V direction;
			direction = mouseFar - mouseScreen;
			direction = Normalize( direction );

			Vec3V segA, segB;
			segA = mouseScreen;
			segB = AddScaled(mouseScreen, direction, ScalarVFromF32(100.0f));
			phSegment segment;
			segment.Set(*(reinterpret_cast<Vector3*>(&segA)), *(reinterpret_cast<Vector3*>(&segB)));
			phIntersection isect;
			if (PHLEVEL->TestProbe(segment, &isect))
			{
				// Do a probe test to apply an impulse.
				phIntersection isect;
				if (PHLEVEL->TestProbe(segment,&isect))
				{
					// A direct line of sight probe at the mouse icon location hit something.
					Assert(isect.GetInstance() && isect.GetInstance()->GetLevelIndex()!=phInst::INVALID_INDEX);

					// Find the impulse magnitude.
					float impulseMag = 40.0f;
					impulseMag = isect.GetInstance()->GetArchetype()->GetMass();
					if (m_Pull.IsPressed())
					{
						impulseMag *= -1.0f;
					}

					// Set the impulse.
					Vec3V impulse = direction;
					impulse = Scale(impulse, Vec3VFromF32(impulseMag));

					// Apply the impulse.
					PHSIM->ApplyImpulse(isect.GetInstance()->GetLevelIndex(), *(reinterpret_cast<Vector3*>(&impulse)), RCC_VECTOR3(isect.GetPosition()), isect.GetComponent());
				}
			}
		}
	}

	void UpdateClient()
	{
		// STEP #7. Update the physics simulator.
		//-- The simulator computes motion for the two moving objects, moves and rotates them though space, detects collisions between
		// themselves and between them and the terrain, and causes them to react to collisions.
		float frameTime = TIME.GetSeconds();
		bool finalUpdate = true;
		PHSIM->Update(frameTime,finalUpdate);

		// STEP #8. Update user input.
		UpdateMouse();
		m_Mapper.Update();
		if (m_Reset.IsPressed())
		{
			Reset();
		}
		// -STOP
	}

	void DrawClient()
	{

#if __PFDRAW

		// STEP #9. Draw the world
		//--  Tell the profile draw manager to draw all the objects in the physics level.
		// In a game this is normally used only for debugging.
		PHSIM->ProfileDraw();
		GetRageProfileDraw().Render();

#endif	// __PFDRAW

		// -STOP
	}

	void ResetObject (phInst& instance, const Matrix34& resetMatrix)
	{
		// Make sure the object is active.
		int levelIndex = instance.GetLevelIndex();
		if(!PHLEVEL->IsActive(levelIndex))
		{
			PHSIM->ActivateObject(levelIndex);
		}

		// Reset the object.
		instance.SetMatrix( *(const Mat34V*)(&resetMatrix) );
		PHSIM->GetCollider(levelIndex)->Reset();
		PHLEVEL->UpdateObjectLocation(levelIndex);
	}

	virtual void Reset ()
	{
		// Reset the two active objects.
		ResetObject(*m_ActiveInstance1,*(reinterpret_cast<Matrix34*>(&m_ResetMatrix1)));
		ResetObject(*m_ActiveInstance2,*(reinterpret_cast<Matrix34*>(&m_ResetMatrix2)));

		// Reset the physics simulator.
		PHSIM->Reset();

	#if __DEBUGLOG
		g_ReplayRand.SetReplayDebug(false);
		diagDebugLogShutdown();

		const bool replay = true;
		const bool abortOnFailure = true;
		diagDebugLogInit(replay,abortOnFailure);
		g_ReplayRand.SetReplayDebug(true);
	#endif

	}

	void ShutdownClient()
	{
		// STEP #10. Shut down the physics level and simulator.
		// -- The user has requested the termination of the sample, so remove the objects from the physics level.
		PHSIM->DeleteObject(m_ActiveInstance2,false);
		PHSIM->DeleteObject(m_ActiveInstance1,false);
		PHSIM->DeleteObject(m_TerrainInstance,false);

        delete m_ActiveInstance2;
        delete m_ActiveInstance1;
        delete m_TerrainInstance;

		// Delete the physics simulator.
		delete PHSIM;

		// Shut down and delete the physics level.
        phLevelNew* level = PHLEVEL;
		level->Shutdown();
		delete level;

		// Call the static shutdown of the simulator.
		phSimulator::ShutdownClass();

		// Delete the material manager.
		MATERIALMGR.Destroy();

#if __PFDRAW
		// Shut down profile drawing.
		GetRageProfileDraw().Shutdown();
#endif
		// -STOP
	}

private:
	// an archetype, two instances and two reset matrices for the icosahedrons
	phArchetypePhys*    m_ActiveArchetype;
	phInst*			    m_ActiveInstance1;
	phInst* 			m_ActiveInstance2;
	Mat34V	    		m_ResetMatrix1,m_ResetMatrix2;

	// an archetype and an instance for the terrain
	phArchetype*    	m_TerrainArchetype;
	phInst* 			m_TerrainInstance;

	// user interaction for resets and mouse controls
	ioMapper		    m_Mapper;
	ioValue			    m_Reset;
	ioValue			    m_Push;
	ioValue			    m_Pull;
};

} // namespace ragesamples

int Main()
{
	{
		ragesamples::basicPhysicsSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}
	
	return 0;
}
