//
// sample_physics/sample_articulated_constraint.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Constraint
// PURPOSE:
//		This sample creates sets of articulated objects with different types of physical constraints for testing the computation of constraint forces.
//		Physical constraints can be point constraints or rotational constraints. Point constraints can hold together points on different objects,
//		or hold a point on an object to a point in the world. Rotational contraints can fix the orientation of two objects relative to each other,
//		or fix the orientation of an object in world space. Both types of constraints can also exert a restoring force or torque to enforce
//		the constraint over time while allowing some separation.

#include "demoobject.h"
#include "sample_physics.h"

#include "curve/curvemgr.h"
#include "devcam/mayacam.h"
#include "devcam/polarcam.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/constraintcylindrical.h"
#include "physics/constraintfixedrotation.h"
#include "physics/constraintrotation.h"
#include "physics/constraintspherical.h"
#include "physics/constraintmgr.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"
#include "vectormath/legacyconvert.h"

#define	MAX_NUM_TRAIN_CARS	12

PARAM(world, "The demo world to initialize when the demo boots");
PARAM(file, "Reserved for future expansion, not currently hooked up");

#if __WIN32
#pragma warning (disable : 4189)
#endif

namespace ragesamples {

using namespace rage;


#if __WIN32
#pragma warning (disable : 4189)
#endif

// PURPOSE: Create sets of constrained objects for testing the computation of constraint forces on articulated bodies.
//			Constraints on rigid bodies are in sample_constraint.
class ArticulatedConstraintManager : public physicsSampleManager
{
public:
	virtual void InitClient ()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();

		// Disable breaking to save a couple of percent speed
		phContactMgr::SetBreakingEnabled(false);

#if 0 // Needs conversion to new constraint API
		// STEP #2. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.
		phSimulator* simulator;
		phConstraintMgr* constraintMgr;
		phConstraint* constraint;
		phInst* human;
		Vector3 position,rotation,worldAttachPoint,worldAttachPointA,worldAttachPointB;
		int component = 0;
		m_RotatingConstraint = NULL;


		//////////////////////////////////////////////////////////////////////////////////
		// Page 0: a ragdoll on a swing that releases its constraint above the pivot point
		m_SwingWorld = &MakeNewDemoWorld("swing");
		position.Set(-2.0f,4.0f,-4.0f);
		m_SwingingHuman = phArticulatedObject::CreateHuman(*m_SwingWorld,position);
		m_SwingConstraint = NULL;

		//////////////////////////////////////////////////////////////
		// Page 1: a ragdoll and a crate dragged by moving constraints
		phDemoWorld& dragWorld = MakeNewDemoWorld("dragging ragdoll","plane_complex");
		position.Set(-3.0f,1.8f,-2.0f);
		Vector3 constraintPosition(position);
		phSwingingObject* swingingHuman = phArticulatedObject::CreateHuman(dragWorld,position);
		constraintMgr = dragWorld.GetSimulator()->GetConstraintMgr();
		constraintPosition.y += 0.1f;
		component = 0;
		Vector3 frequency(1.0f,0.0f,0.5f);
		Vector3 amplitude(4.0f,0.0f,1.0f);
		swingingHuman->SetMovingConstraint(constraintPosition,constraintMgr,component,frequency,amplitude);
		position.Set(4.0f,0.0f,2.0f);
		rotation.Zero();
		worldAttachPoint.Set(position);
		worldAttachPoint.x += 2.0f;
		worldAttachPoint.y += 0.5f;
		frequency.Set(3.0f,0.0f,2.0f);
		Vector3 phaseOffset(PI,PI,PI);
		dragWorld.CreateSwingingObject("crate",position,rotation,worldAttachPoint,frequency,amplitude,phaseOffset);

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// Page 2: a set of constraints that make large pushes and turns on the first frame, designed to test
		//			how well physical constraints can handle attachment to non-physical objects like animating
		//			characters (when the constrained push or turn exceeds a limit, the rest is applied with no
		//			physical response)
		phDemoWorld& pushWorld = MakeNewDemoWorld("push");
		//pushWorld.Pause();
		constraintMgr = pushWorld.GetSimulator()->GetConstraintMgr();
		position.Set(0.0f,1.0f,-10.0f);
		human = phArticulatedObject::CreateHuman(pushWorld,position)->GetPhysInst();
		worldAttachPoint.Set(0.0f,4.0f,0.0f);
		constraintMgr->AttachObjectToWorld('SAP0', worldAttachPoint,position,human,0,0.0f,false);
		position.Set(0.0f,2.0f,0.0f);
		human = phArticulatedObject::CreateHuman(pushWorld,position,rotation)->GetPhysInst();
		constraintMgr->AttachObjectToWorld('SAP1', position,human);
		constraint = constraintMgr->AttachObjectToWorld('SAP2', position,human);
		constraint->SetFixedRotation();
		constraint->SetRelativeOrientation(ORIGIN);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Page 3: A ragdoll starting with a large velocity and a constraint on a hand, to test articulated body stability
		phDemoWorld& violentConstraintWorld = MakeNewDemoWorld("Test Violent Constraint");
		position.Set(0.0f,3.0f,0.0f);
		rotation.Set(0.0f,0.0f,-0.25f*PI);
		phArticulatedObject* humanObject = phArticulatedObject::CreateHuman(violentConstraintWorld,position,rotation);
		human = humanObject->GetPhysInst();
		simulator = violentConstraintWorld.GetSimulator();
		component = 1;
		position.Set(humanObject->GetWorldPositionOfBoundPart(component));	
		constraintMgr = simulator->GetConstraintMgr();
		if (phDemoWorld::GetArticulatedRagdolls())
		{
			constraintMgr->AttachObjectToWorld('SAV0', position,human,component);
		}
		else
		{
			constraintMgr->AttachObjectToWorld('SAV1', position,humanObject->GetPartInst(component));
		}
		position.Average(humanObject->GetWorldPositionOfBoundPart(8),humanObject->GetWorldPositionOfBoundPart(10));
		if (phDemoWorld::GetArticulatedRagdolls())
		{
			constraintMgr->AttachObjects('SAV2', position,human,human,8,10);
			Vec3V velocity = Scale(Vec3V(V_Y_AXIS_WZERO),ScalarV(V_TEN));
			phArticulatedCollider* articulatedCollider = static_cast<phArticulatedCollider*>(simulator->GetCollider(human));
			articulatedCollider->GetBody()->SetVelocity(velocity);
			humanObject->SaveResetState();
			articulatedCollider->Reset();
		}
		else
		{
			constraintMgr->AttachObjects('SAV3', position,humanObject->GetPartInst(8),humanObject->GetPartInst(10));
		}
		component = 6;
		position.Set(humanObject->GetWorldPositionOfBoundPart(component));
		rotation.Set(0.0f,0.0f,0.0f);
		phInst* stick = violentConstraintWorld.CreateObject("capsule_small",position,false,rotation)->GetPhysInst();
		if (phDemoWorld::GetArticulatedRagdolls())
		{
			constraintMgr->AttachObjects('SAV4', position,human,stick,component);
			constraint = constraintMgr->AttachObjects('SAV5', position,human,stick,component);
		}
		constraint->SetFixedRotation();
		human->GetArchetype()->SetTypeFlags(2);
		PHLEVEL->UpdateObjectArchetype(human ->GetLevelIndex());
		stick->GetArchetype()->RemoveIncludeFlags(2);
		PHLEVEL->UpdateObjectArchetype(stick->GetLevelIndex());

		//////////////////////////////////////////////////////////////////////////
		// Page 4: A ragdoll with a spinning rotation constraint
		m_RotatingConstraintWorld = &MakeNewDemoWorld("Test Rotation Constraint");
		position.Set(0.0f,3.0f,0.0f);
		rotation.Zero();
		humanObject = phArticulatedObject::CreateHuman(*m_RotatingConstraintWorld,position,rotation);
		human = humanObject->GetPhysInst();
		simulator = m_RotatingConstraintWorld->GetSimulator();
		component = 2;
		constraintMgr = simulator->GetConstraintMgr();
		if (phDemoWorld::GetArticulatedRagdolls())
		{
			phArticulatedCollider* articulatedCollider = static_cast<phArticulatedCollider*>(simulator->GetCollider(human));
			position.Set(articulatedCollider->GetWorldPositionOfBoundPart(component));
			constraintMgr->AttachObjectToWorld('SAR0', position,human,component);
			m_RotatingConstraint = constraintMgr->AttachObjectToWorld('SAR1', position,human,component);
		}
		else
		{
			position.Set(humanObject->GetWorldPositionOfBoundPart(component));
			constraintMgr->AttachObjectToWorld('SAR2', position,humanObject->GetPartInst(component));
			m_RotatingConstraint = constraintMgr->AttachObjectToWorld('SAR3', position,humanObject->GetPartInst(component));
		}
		m_FlatBox = m_RotatingConstraintWorld->CreateFixedObject("box_flat",position)->GetPhysInst();
		m_RotatingConstraint->SetFixedRotation();
		human->GetArchetype()->SetTypeFlags(2);
		PHLEVEL->UpdateObjectArchetype(human->GetLevelIndex());
		m_FlatBox->GetArchetype()->RemoveIncludeFlags(2);
		PHLEVEL->UpdateObjectArchetype(m_FlatBox->GetLevelIndex());
		m_FlatBoxAngVelocity.Set(5.0f,-5.0f,2.0f);
		m_FlatBoxOrientation.Identity();
#endif // Needs conversion to new constraint API

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Page 5: A world with rotation constraints on all combinations of rigid, articulated, and fixed objects
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ArticulatedConstraintManager::CreateRotationConstraintBestiary));

		// STEP #3. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		int startingDemo = 0;
		PARAM_world.Get(startingDemo);
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
		m_SavedCameraOffset.Zero();
	}
	
	Vector3 ComputeCirclePoint(int position)
	{
		static const int NUM_CIRCLE_POSITIONS = 15;
		static const float CIRCLE_DIAMETER = 10.0f;

		Assert(position < NUM_CIRCLE_POSITIONS);
		float theta = float(position) / float(NUM_CIRCLE_POSITIONS) * 2 * PI;
		return Vector3(CIRCLE_DIAMETER * sinf(theta), 2.0f, -CIRCLE_DIAMETER * cosf(theta));
	}

	phDemoWorld* CreateRotationConstraintBestiary ()
	{
		phDemoWorld& bestiaryWorld = *CreateNewDemoWorld("rotation constraint bestiary");

		//phConstraintMgr* constraintMgr = bestiaryWorld.GetSimulator()->GetConstraintMgr();

		int circlePosition = 0;

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			objectA->SetLabel("Hard rotation constraint\narticulated vs fixed");

			phInst* instA = objectA->GetPhysInst();
			phConstraintFixedRotation::Params constraintFixedRotation;
			constraintFixedRotation.instanceA = instA;
			PHCONSTRAINT->Insert(constraintFixedRotation);

			position.y -= 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			objectA->SetLabel("Sliding rotation constraint\narticulated vs fixed");

			phInst* instA = objectA->GetPhysInst();
			phConstraintRotation::Params constraintRotation;
			constraintRotation.instanceA = instA;
			constraintRotation.worldAxis = Vec3V(V_Z_AXIS_WZERO);
			constraintRotation.minLimit = -0.1f * PI;
			constraintRotation.maxLimit =  0.1f * PI;
			PHCONSTRAINT->Insert(constraintRotation);

			position.y -= 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			objectA->SetLabel("Pivoting rotation constraint\narticulated vs fixed");

			phInst* instA = objectA->GetPhysInst();
			phConstraintCylindrical::Params constraintCylindrical;
			constraintCylindrical.instanceA = instA;
			constraintCylindrical.worldAxis = Vec3V(0.0f, 1.0f, 0.0f);
			PHCONSTRAINT->Insert(constraintCylindrical);

			position.y -= 0.5f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			phDemoObject* objectA = bestiaryWorld.CreateBox(Vector3(0.4f, 1.0f, 1.6f), 1.0f, position);
			objectA->SetLabel("Hard rotation constraint\nrigid vs fixed");

			phInst* instA = objectA->GetPhysInst();
			phConstraintFixedRotation::Params constraintFixedRotation;
			constraintFixedRotation.instanceA = instA;
			PHCONSTRAINT->Insert(constraintFixedRotation);

			position.y -= 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			phDemoObject* objectA = bestiaryWorld.CreateBox(Vector3(0.4f, 1.0f, 1.6f), 1.0f, position);
			objectA->SetLabel("Sliding rotation constraint\nrigid vs fixed");

			phInst* instA = objectA->GetPhysInst();
			phConstraintRotation::Params constraintRotation;
			constraintRotation.instanceA = instA;
			constraintRotation.worldAxis = Vec3V(V_Z_AXIS_WZERO);
			constraintRotation.minLimit = -0.1f * PI;
			constraintRotation.maxLimit =  0.1f * PI;
			PHCONSTRAINT->Insert(constraintRotation);

			position.y -= 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, PI);
			phDemoObject* objectA = bestiaryWorld.CreateBox(Vector3(0.4f, 1.0f, 1.6f), 1.0f, position);;
			objectA->SetLabel("Pivoting rotation constraint\nrigid vs fixed");

			phInst* instA = objectA->GetPhysInst();
			phConstraintCylindrical::Params constraintCylindrical;
			constraintCylindrical.instanceA = instA;
			constraintCylindrical.worldAxis = Vec3V(0.0f, 1.0f, 0.0f);
			PHCONSTRAINT->Insert(constraintCylindrical);

			position.y -= 0.5f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			objectA->SetLabel("Hard rotation constraint\narticulated vs articulated");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintFixedRotation::Params constraintFixedRotation;
			constraintFixedRotation.instanceA = instA;
			constraintFixedRotation.instanceB = instB;
			PHCONSTRAINT->Insert(constraintFixedRotation);

			position.x += 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			objectA->SetLabel("Sliding rotation constraint\narticulated vs articulated");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintRotation::Params constraintRotation;
			constraintRotation.instanceA = instA;
			constraintRotation.instanceB = instB;
			constraintRotation.worldAxis = Vec3V(V_Z_AXIS_WZERO);
			constraintRotation.minLimit = -0.1f * PI;
			constraintRotation.maxLimit =  0.1f * PI;
			PHCONSTRAINT->Insert(constraintRotation);

			position.x += 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			objectA->SetLabel("Pivoting rotation constraint\narticulated vs articulated");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintCylindrical::Params constraintCylindrical;
			constraintCylindrical.instanceA = instA;
			constraintCylindrical.instanceB = instB;
			constraintCylindrical.worldAxis = Vec3V(1.0f, 0.0f, 0.0f);
			PHCONSTRAINT->Insert(constraintCylindrical);

			position.x += 0.5f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			objectA->SetLabel("Hard rotation constraint\narticulated vs rigid");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintFixedRotation::Params constraintFixedRotation;
			constraintFixedRotation.instanceA = instA;
			constraintFixedRotation.instanceB = instB;
			PHCONSTRAINT->Insert(constraintFixedRotation);

			position.x += 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			objectA->SetLabel("Sliding rotation constraint\narticulated vs rigid");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintRotation::Params constraintRotation;
			constraintRotation.instanceA = instA;
			constraintRotation.instanceB = instB;
			constraintRotation.worldAxis = Vec3V(V_Z_AXIS_WZERO);
			constraintRotation.minLimit = -0.1f * PI;
			constraintRotation.maxLimit =  0.1f * PI;
			PHCONSTRAINT->Insert(constraintRotation);

			position.x += 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = phArticulatedObject::CreateChain(bestiaryWorld, false, "box_elbow", position, rotation);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			objectA->SetLabel("Pivoting rotation constraint\narticulated vs rigid");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintCylindrical::Params constraintCylindrical;
			constraintCylindrical.instanceA = instA;
			constraintCylindrical.instanceB = instB;
			constraintCylindrical.worldAxis = Vec3V(1.0f, 0.0f, 0.0f);
			PHCONSTRAINT->Insert(constraintCylindrical);

			position.x += 0.5f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			objectA->SetLabel("Hard rotation constraint\nrigid vs rigid");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintFixedRotation::Params constraintFixedRotation;
			constraintFixedRotation.instanceA = instA;
			constraintFixedRotation.instanceB = instB;
			PHCONSTRAINT->Insert(constraintFixedRotation);

			position.x += 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			objectA->SetLabel("Sliding rotation constraint\nrigid vs rigid");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintRotation::Params constraintRotation;
			constraintRotation.instanceA = instA;
			constraintRotation.instanceB = instB;
			constraintRotation.worldAxis = Vec3V(V_Z_AXIS_WZERO);
			constraintRotation.minLimit = -0.1f * PI;
			constraintRotation.maxLimit =  0.1f * PI;
			PHCONSTRAINT->Insert(constraintRotation);

			position.x += 0.5f;
			position.z += 0.8f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
			constraintSpherical.worldPosition += Vec3V(0.0f, 0.0f, -1.6f);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		if (true)
		{
			Vector3 position(ComputeCirclePoint(circlePosition++));
			Vector3 rotation(0.0f, 0.0f, 0.5f * PI);
			phDemoObject* objectA = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			position.x -= 1.0f;
			rotation.Set(0.0f, 0.0f, -0.5f * PI);
			phDemoObject* objectB = bestiaryWorld.CreateBox(Vector3(1.0f, 0.4f, 1.6f), 1.0f, position);
			objectA->SetLabel("Pivoting rotation constraint\nrigid vs rigid");

			phInst* instA = objectA->GetPhysInst();
			phInst* instB = objectB->GetPhysInst();
			instA->GetArchetype()->SetTypeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instA->GetLevelIndex());
			instB->GetArchetype()->RemoveIncludeFlags(4);
			PHLEVEL->UpdateObjectArchetype(instB->GetLevelIndex());

			phConstraintCylindrical::Params constraintCylindrical;
			constraintCylindrical.instanceA = instA;
			constraintCylindrical.instanceB = instB;
			constraintCylindrical.worldAxis = Vec3V(1.0f, 0.0f, 0.0f);
			PHCONSTRAINT->Insert(constraintCylindrical);

			position.x += 0.5f;
			phConstraintSpherical::Params constraintSpherical;
			constraintSpherical.instanceA = instA;
			constraintSpherical.instanceB = instB;
			constraintSpherical.worldPosition = Vec3V(position);
			PHCONSTRAINT->Insert(constraintSpherical);
		}

		return &bestiaryWorld;
	}


	virtual void InitCamera ()
	{
		Vector3 lookFrom(-1.0f,3.5f,6.0f);
		Vector3 lookTo(0.0f,1.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Reset ()
	{
	}

	virtual void UpdateClient ()
	{
		if (m_Demos.GetCurrentWorld()->ShouldUpdate())
		{
#if 0 // Needs converstion to new constraint API
			if (m_Demos.GetCurrentWorld()==m_SwingWorld)
			{
				// The swinging human world is active, so see if the swing constraint should be turned on or off.
				Assert(m_SwingingHuman);
				if (m_SwingingHuman->GetPhysInst()->GetPosition().GetYf()<4.0f)
				{
					if (!m_SwingConstraint)
					{
						phConstraintMgr* constraintMgr = m_SwingWorld->GetSimulator()->GetConstraintMgr();
						Vector3 swingPivot(0.0f,4.0f,-4.0f);
						int component = 10;
						Vector3 bodyPartPosition;
						Assert(m_SwingingHuman->GetPhysInst());
						phInst& swingingInstance = *m_SwingingHuman->GetPhysInst();
						Assert(swingingInstance.GetArchetype() && swingingInstance.GetArchetype()->GetBound());
						if (phDemoWorld::GetArticulatedRagdolls())
						{
							phBound& humanBound = *swingingInstance.GetArchetype()->GetBound();
							Assert(humanBound.GetType()==phBound::COMPOSITE);
							phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(&humanBound);
							RCC_MATRIX34(swingingInstance.GetMatrix()).Transform(RCC_MATRIX34(compositeBound.GetCurrentMatrix(component)).d,bodyPartPosition);
							m_SwingConstraint = constraintMgr->AttachObjectToWorld('SAU0', swingPivot,bodyPartPosition,&swingingInstance,component);
						}
						else
						{
							RCC_MATRIX34(swingingInstance.GetMatrix()).Transform(m_SwingingHuman->GetWorldPositionOfBoundPart(component),bodyPartPosition);
							m_SwingConstraint = constraintMgr->AttachObjectToWorld('SAU1', swingPivot,bodyPartPosition,m_SwingingHuman->GetPartInst(component));
						}
					}
				}
				else if (m_SwingConstraint)
				{
					phConstraintMgr* constraintMgr = m_SwingWorld->GetSimulator()->GetConstraintMgr();
					constraintMgr->DeactivateConstraint(m_SwingConstraint);
					m_SwingConstraint = NULL;
				}
			}

			if (m_Demos.GetCurrentWorld()==m_RotatingConstraintWorld && m_RotatingConstraint)
			{
				Vector3 delAngVel(ORIGIN);
				m_FlatBoxAngVelocity.Add(delAngVel);
				Vector3 rotation(m_FlatBoxAngVelocity);
				rotation.Scale(TIME.GetSeconds());
				Quaternion rotationQ;
				rotationQ.FromRotation(rotation);
				m_FlatBoxOrientation.Multiply(rotationQ);
				Matrix34 flatBoxPose;
				flatBoxPose.FromQuaternion(m_FlatBoxOrientation);
				flatBoxPose.d.Set(RCC_VECTOR3(m_FlatBox->GetPosition()));
				m_FlatBox->SetMatrix(RCC_MAT34V(flatBoxPose));
				m_RotatingConstraint->SetRelativeOrientation(flatBoxPose);
			}
#endif
		}

		physicsSampleManager::UpdateClient();
	}

	virtual void DrawClient ()
	{
		physicsSampleManager::DrawClient();
	}

	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}


protected:
	phDemoWorld* m_SwingWorld;
#if 0 // Needs converstion to new constraint API
	phConstraint* m_RotatingConstraint;
	phConstraint* m_SwingConstraint;
#endif
	phArticulatedObject* m_SwingingHuman;

	phDemoWorld* m_RotatingConstraintWorld;
	phInst* m_FlatBox;
	Quaternion m_FlatBoxOrientation;
	Vector3 m_FlatBoxAngVelocity;
	Vector3 m_SavedCameraOffset;
};

} // namespace ragesamples


int Main()
{
	ragesamples::ArticulatedConstraintManager sampleConstraint;
	sampleConstraint.Init();
	sampleConstraint.UpdateLoop();
	sampleConstraint.Shutdown();
    return 0;
}

