//
// sample_physics/sample_maze.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "demoobject.h"
#include "sample_physics.h"

#include "input/pad.h"
#include "math/random.h"
#include "physics/collider.h"
#include "physics/colliderdispatch.h"
#include "physics/simulator.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

PARAM(world, "The demo world to initialize when the demo boots");
PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

class mazeSampleManager : public physicsSampleManager
{
public:
	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

		mthRandom random;

		// Use this to offset all objects in the world, to test physics behavior with roundoff errors far from the origin.
		Vector3 worldOffset(0.0f,0.0f,0.0f);
		phDemoObject::SetWorldOffset(worldOffset);

		// Set the physics level parameters.
		int maxOctreeNodes = 1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		Vec3V worldMin(-999.0f,-999.0f,-999.0f);
		Vec3V worldMax(999.0f,999.0f,999.0f);
		worldMin += RCC_VEC3V(phDemoObject::GetWorldOffset());
		worldMax += RCC_VEC3V(phDemoObject::GetWorldOffset());
		int maxInstBehaviors = 0;

		SetFullAssetPath(RAGE_ASSET_ROOT);

		/////////////////////////////////////////
		// complicated terrain with a few objects
		m_Demos.AddDemo(rage_new phDemoWorld("Marble Maze"));
		m_Demos.GetCurrentWorld()->Init(maxOctreeNodes,maxActiveObjects,maxObjects,worldMin,worldMax,maxInstBehaviors);
		m_Demos.GetCurrentWorld()->CreateFixedObject("sphere_227");

		Vector3 position(0.0f,TABLE_HEIGHT,0.0f);
		Vector3 rotation(0.0f, PI, 0.0f);
		bool alignBottom = false;
		m_Maze = m_Demos.GetCurrentWorld()->CreateObject("maze",position,alignBottom,rotation,true);
		Assert(m_Maze);

#if 0 // Needs conversion to new constraint API
		phConstraintMgr* constraintMgr = m_Demos.GetCurrentWorld()->GetSimulator()->GetConstraintMgr();
		Vector3 worldAttachPoint = VEC3V_TO_VECTOR3(m_Maze->GetPhysInst()->GetCenterOfMass());
		constraintMgr->AttachObjectToWorld('Smz0', worldAttachPoint,m_Maze->GetPhysInst());
#endif // Needs conversion to new constraint API

		position.Set(-6.0f, TABLE_HEIGHT + 0.5f, -9.0f);
		rotation.Set(0.0f, 0.0f, 0.0f);
		phDemoObject* ball = m_Demos.GetCurrentWorld()->CreateObject("sphere_056",position,alignBottom,rotation,true);
		ball->GetPhysInst()->GetArchetype()->SetMass(100000.0f);
		ball->GetPhysInst()->GetArchetype()->SetGravityFactor(8.0f);

		m_Demos.AddDemo(rage_new phDemoWorld("Marble Maze 2"));
		m_Demos.GetCurrentWorld()->Init(maxOctreeNodes,maxActiveObjects,maxObjects,worldMin,worldMax,maxInstBehaviors);
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane(false);

		position.Set(0.0f,TABLE_HEIGHT,0.0f);
		rotation.Set(0.0f, PI, 0.0f);
		alignBottom = false;
		m_Maze = m_Demos.GetCurrentWorld()->CreateObject("maze2",position,alignBottom,rotation,true);
		Assert(m_Maze);

		// worldAttachPoint;
#if 0 // Needs conversion to new constraint API
		constraintMgr = m_Demos.GetCurrentWorld()->GetSimulator()->GetConstraintMgr();
		worldAttachPoint = VEC3V_TO_VECTOR3(m_Maze->GetPhysInst()->GetCenterOfMass());
		constraintMgr->AttachObjectToWorld('Smz1', worldAttachPoint,m_Maze->GetPhysInst());
#endif // Needs conversion to new constraint API

		position.Set(-6.0f, TABLE_HEIGHT + 0.5f, -9.0f);
		rotation.Set(0.0f, 0.0f, 0.0f);
		ball = m_Demos.GetCurrentWorld()->CreateObject("sphere_056",position,alignBottom,rotation,true);
		ball->GetPhysInst()->GetArchetype()->SetMass(100000.0f);
		ball->GetPhysInst()->GetArchetype()->SetGravityFactor(8.0f);

		int startingDemo = 0;

		PARAM_world.Get(startingDemo);

		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
	}

	virtual void InitCamera()
	{
		Vector3 lookFrom(0.0f,20.0f,-10.0f);
		Vector3 lookTo(0.0f,TABLE_HEIGHT-4.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void UpdateClient()
	{
		float x = -ioPad::GetPad(0).GetNormLeftY();
		float z = ioPad::GetPad(0).GetNormLeftX();
		z += ioPad::GetPad(0).GetNormRightY();

		x = ioAddDeadZone(x, 0.2f);
		z = ioAddDeadZone(z, 0.2f);

		phCollider* collider = PHSIM->GetCollider(m_Maze->GetPhysInst());
		Assert(collider);

		float y = safe_atan2f(RCC_MATRIX34(collider->GetMatrix()).c.x,RCC_MATRIX34(collider->GetMatrix()).c.z) + PI;

		if (y > PI)
		{
			y -= PI*2.0f;
		}
		//Displayf("angle: %f", y);

		const float TORQUE_STRENGTH = 20000000.0f;
		Vec3V torque(x * TORQUE_STRENGTH, y * -TORQUE_STRENGTH, z * TORQUE_STRENGTH);

		collider->ApplyTorque(torque.GetIntrin128(), ScalarV(TIME.GetSeconds()).GetIntrin128());

		physicsSampleManager::UpdateClient();
	}

private:
	phDemoObject* m_Maze;

	static const float TABLE_HEIGHT;
};

const float mazeSampleManager::TABLE_HEIGHT = 5.0f;

} // namespace ragesamples


// main application
int Main()
{
	{
		ragesamples::mazeSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
