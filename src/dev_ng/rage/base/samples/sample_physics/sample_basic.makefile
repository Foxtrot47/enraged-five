Project sample_basic 
ConfigurationType exe 
RootDirectory . 
Files { 
sample_basic.cpp 
Folder Resources {
        sample_basic.appxmanifest
        ../durango_files/Logo.png
        ../durango_files/SmallLogo.png
        ../durango_files/SplashScreen.png
        ../durango_files/StoreLogo.png
}
} 
Libraries { 
 ..\..\..\base\src\vcproj\RageCore\RageCore 
 ..\..\..\base\src\vcproj\RagePhysics\RagePhysics 
 ..\..\..\base\samples\vcproj\RageBaseSample\RageBaseSample 
 ..\sample_rmcore\sample_rmcore 
 ..\..\..\base\samples\sample_rmcore\sample_rmcore 
 ..\..\..\base\src\vcproj\RageGraphics\RageGraphics 
 ..\sample_physics\sample_physics 
 ..\..\..\base\samples\sample_physics\sample_physics 
} 
