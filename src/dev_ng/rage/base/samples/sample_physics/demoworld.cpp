//
// sample_physics/demoworld.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "demoworld.h"
#include "archmgr.h"

#include "bank/bkmgr.h"
#include "devcam/polarcam.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "grcore/viewport.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "input/pad.h"
#include "math/random.h"
#include "pharticulated/articulatedcollider.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundcurvedgeom.h"
#include "phbound/support.h"
#include "phbullet/collisionmargin.h"
#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/phmath.h"
#include "phcore/segment.h"
#include "phcore/surface.h"
#include "pheffects/explosionmgr.h"
#include "physics/broadphase.h"
#include "physics/constrainthinge.h"
#include "physics/constraintmgr.h"
#include "physics/instbehavior.h"
#include "physics/instbreakable.h"
#include "physics/intersection.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "profile/log.h"
#include "profile/profiler.h"
#include "rmcore/drawable.h"
#include "string/string.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"
#include "vector/matrix34.h"

#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

namespace rage {

EXT_PFD_DECLARE_ITEM(Models);
EXT_PFD_DECLARE_ITEM(PhysicsDemoWorld);

} // namespace rage

namespace ragesamples {


int		phDemoWorld::sm_AddAndDeleteObjects			= 0;
float	phDemoWorld::sm_FixedFrameAltStep			= .04f;
int		phDemoWorld::sm_FixedFrameStepLimit			= 2;
float	phDemoWorld::sm_FixedFrameStepMultiplier	= 2.f; 
float	phDemoWorld::sm_TimeWarp					= 1.0f;
bool	phDemoWorld::sm_FixedFrame					= true;
float	phDemoWorld::sm_activeWorldFrameRate		= 60.0f;
float	phDemoWorld::sm_MaximumFrameTime			= 1.0f / 15.0f;
int		phDemoWorld::sm_Oversampling				= 1;
phDemoWorld::DrawableTree* phDemoWorld::sm_DrawableDictionary = NULL;
bool	phDemoWorld::sm_RedrawWhilePaused			= true;
bool	phDemoWorld::sm_RedrawNextFrameWhilePaused	= false;
bool	phDemoWorld::sm_DrawLabels					= true;
bool	phDemoWorld::sm_ArticulatedRagdolls			= true;
bool	phDemoWorld::sm_VerySimpleRagdolls			= false;
bool	phDemoWorld::sm_ConstraintRotationLimitRagdolls	= false;
bool	phDemoWorld::sm_Paused						= false;

PARAM(phmaterials, "[sample_physics] Specify the path where the materials (and materials.list) are to be found.");
PARAM(startpaused, "[sample_physics] Start the simulation in the pause state.");
PARAM(redrawwhilepaused, "[sample_physics] Redraw during pauses.");

/////////////////////////////////////////////////////////////////////

phDemoMultiWorld::phDemoMultiWorld()
{
	m_FactoryProducedDemo = NULL;
	Reset();

	for (int demo = 0; demo < m_NumDemos; ++demo)
	{
		m_Demos[demo] = NULL;
	}

	m_CurrentDemo = -1;

	m_Mapper.Reset();
	m_Mapper.Map(IOMS_KEYBOARD, KEY_EQUALS, m_NextWorld);
    m_Mapper.Map(IOMS_KEYBOARD, KEY_MINUS, 	m_PreviousWorld);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_F4,		m_Reset);
}


phDemoMultiWorld::~phDemoMultiWorld()
{
	Reset();
}


void phDemoMultiWorld::Reset()
{
	m_NumDemos = 0;
	m_CurrentDemo = -1;
	if (m_FactoryProducedDemo)
	{
		phDemoWorld::ClearActiveDemo();
		m_FactoryProducedDemo->Activate();
		m_FactoryProducedDemo->Shutdown();
		delete m_FactoryProducedDemo;
		m_FactoryProducedDemo = NULL;
	}
}

#ifndef DEBUG_BUTTONS_ONLY
#if __DEBUGBUTTONS
#define DEBUG_BUTTONS_ONLY(X) X
#else
#define DEBUG_BUTTONS_ONLY(X)
#endif
#endif

void phDemoMultiWorld::Update(dcamCamMgr* DEBUG_BUTTONS_ONLY(cameraManager))
{
	GetCurrentWorld()->Update();

	m_Mapper.Update();

#if __DEBUGBUTTONS
	if (m_NextWorld.IsPressed() || ioPad::GetPad(0).GetPressedDebugButtons() & ioPad::R2)
	{
		NextWorld(cameraManager);
	}


	if (m_PreviousWorld.IsPressed() || ioPad::GetPad(0).GetPressedDebugButtons() & ioPad::R1)
	{
		PreviousWorld(cameraManager);
	}

	if (m_Reset.IsPressed() || ioPad::GetPad(0).GetPressedDebugButtons() & ioPad::RRIGHT)
	{
		if (m_FactoryProducedDemo)
		{
			phDemoWorld::ClearActiveDemo();;
			m_FactoryProducedDemo->Shutdown();
			delete m_FactoryProducedDemo;
			m_FactoryProducedDemo = m_DemoFactories[m_CurrentDemo]();
			m_FactoryProducedDemo->Activate();
		}
		else
		{
			m_Demos[m_CurrentDemo]->Reset();
		}
	}
#endif
}


void phDemoMultiWorld::NextWorld (dcamCamMgr* cameraManager)
{
	SetCurrentDemo((m_CurrentDemo + 1) % m_NumDemos);
	GetCurrentWorld()->Activate(cameraManager);
}


void phDemoMultiWorld::PreviousWorld (dcamCamMgr* cameraManager)
{
	SetCurrentDemo((m_CurrentDemo + (m_NumDemos - 1)) % m_NumDemos);
	GetCurrentWorld()->Activate(cameraManager);
}


void phDemoMultiWorld::UpdateMouse (Vec::V3Param128 timeStep)
{
	GetCurrentWorld()->UpdateMouse(timeStep);
}

void phDemoMultiWorld::UpdateMouse ()
{
	GetCurrentWorld()->UpdateMouse( (TIME.GetSecondsV()).GetIntrin128() );
}

void phDemoMultiWorld::Draw()
{
//	char demoString[100];
//	formatf(demoString, 100, "Demo %d", CurrentDemo);
//	font.Draw (PIPE.GetWidth()-80, 10, demoString);
	
	GetCurrentWorld()->Draw();
}

void phDemoMultiWorld::Shutdown()
{
	for (int demo = 0; demo < m_NumDemos; ++demo)
	{
		if (m_Demos[demo] != NULL)
		{
			m_Demos[demo]->Activate();
			m_Demos[demo]->Shutdown();
		}
	}
}

phDemoWorld* phDemoMultiWorld::GetCurrentWorld()
{
	Assert(m_CurrentDemo >= 0);
	phDemoWorld* currentWorld = m_Demos[m_CurrentDemo];
	if (currentWorld)
	{
		return currentWorld;
	}
	else
	{
		return m_FactoryProducedDemo;
	}
}

int phDemoMultiWorld::GetCurrentNumber()
{
	Assert(m_CurrentDemo >= 0);
	
	return m_CurrentDemo;
}

int phDemoMultiWorld::GetNumDemos()
{
	return m_NumDemos;
}

void phDemoMultiWorld::SetCurrentDemo(int demo)
{
	if (m_FactoryProducedDemo)
	{
		phDemoWorld::ClearActiveDemo();
		m_FactoryProducedDemo->Shutdown();
		delete m_FactoryProducedDemo;
		m_FactoryProducedDemo = NULL;
	}

	m_CurrentDemo = Clamp(demo, 0, m_NumDemos - 1);

	if (m_Demos[m_CurrentDemo] == NULL)
	{
		m_FactoryProducedDemo = m_DemoFactories[m_CurrentDemo]();
		m_FactoryProducedDemo->Activate();
	}
}

void phDemoMultiWorld::SetCurrentDemo(const phDemoWorld* demoWorld)
{
	for (int worldIndex = 0; worldIndex < m_NumDemos; worldIndex++)
	{
		if (m_Demos[worldIndex] == demoWorld)
		{
			SetCurrentDemo(worldIndex);
			return;
		}
	}
}

void phDemoMultiWorld::AddDemo(phDemoWorld* demo)
{
	Assert(m_NumDemos < MAX_DEMOS);
	m_Demos[m_NumDemos] = demo;

	m_CurrentDemo = m_NumDemos++;
}

void phDemoMultiWorld::AddDemo(Factory demo)
{
	Assert(m_NumDemos < MAX_DEMOS);
	m_Demos[m_NumDemos] = NULL;
	m_DemoFactories[m_NumDemos] = demo;

	m_CurrentDemo = m_NumDemos++;
}

phDemoWorld*& phDemoMultiWorld::operator[](int demo)	{ return m_Demos[demo]; }


/////////////////////////////////////////////////////////////////////

#if __BANK
bkBank * phDemoWorld::sm_Bank = NULL;
#endif
PDR_ONLY(debugPlayback::DebugRecorder* phDemoWorld::sm_DebugRecorder = NULL;)
phDemoWorld* phDemoWorld::sm_ActiveDemo = NULL;
bool phDemoWorld::sm_ClassInitialized = false;

/////////////////////////////////////////////////////////////////////

void phDemoWorld::InitClass()
{
	Assert(!sm_ClassInitialized);

#ifdef CRANIMATION_ANIMATION_H
//	crAnimation::InitHashTables();
#endif

	if (!phArchetypeMgr::InstanceExists())
	{
		phArchetypeMgr::CreateInstance();
	}

	TIME.ShowFrameOutput(true);
	TIME.SetClamp(1.0f / 1000.0f, 1.0f / 60.0f);

	sm_ActiveDemo = NULL;

	const char* materialList = "";
	PARAM_phmaterials.Get(materialList);

	if (materialList[0])
	{
		ASSET.PushFolder(materialList);
	}
	else
	{
		ASSET.PushFolder("materials");
	}
	phMaterialMgrImpl<phSurface, phMaterialMgrFlag>::Create();
	MATERIALMGRFLAG.Load(64);
	ASSET.PopFolder();

    sm_DrawableDictionary = rage_new DrawableTree;

#if __BANK
	Assert(sm_Bank==NULL);
	sm_Bank = &BANKMGR.CreateBank("rage - Physics");
	sm_Bank->PushGroup("Level",false);
	phLevelNew::AddWidgets(*sm_Bank);
	sm_Bank->PopGroup();
	sm_Bank->PushGroup("Simulator",false);
	phSimulator::AddWidgets(*sm_Bank);
	sm_Bank->PopGroup();
	sm_Bank->PushGroup("Archetypes",false);
	ARCHMGR.AddWidgets(*sm_Bank);
	sm_Bank->PopGroup();
	sm_Bank->PushGroup("Materials",false);
	MATERIALMGR.AddWidgets(*sm_Bank);
	sm_Bank->PopGroup();
#endif // __BANK

	if (PARAM_startpaused.Get())
	{
		sm_Paused = true;
	}

#if PDR_ENABLED
	sm_DebugRecorder = rage_new debugPlayback::DebugRecorder;
	sm_DebugRecorder->AddWidgets(*sm_Bank);
	debugPlayback::Init();
#endif // PDR_ENABLED

	sm_ClassInitialized = true;
}

void phDemoWorld::ClassShutdown()
{
	Assert(sm_ClassInitialized);

#if PDR_ENABLED
	delete sm_DebugRecorder;
	sm_DebugRecorder = NULL;
#endif // PDR_ENABLED

#if __BANK
	Assert(sm_Bank);
	BANKMGR.DestroyBank(*sm_Bank);
	sm_Bank = NULL;
#endif

    for (DrawableTree::Iterator it = sm_DrawableDictionary->CreateIterator(); !it.AtEnd(); ++it)
    {
        delete it.GetData();
    }

    sm_DrawableDictionary->DeleteAll();

    delete sm_DrawableDictionary;

	sm_ClassInitialized = false;

	MATERIALMGR.Destroy();

	phArchetypeMgr::DeleteInstance();

#ifdef CRANIMATION_ANIMATION_H
//	crAnimation::InitHashTables();
#endif
}

#ifndef MESH_LIBRARY
#define MESH_LIBRARY 1
#endif

#if MESH_LIBRARY
	#define NO_MESHLIB_PARAM(x) x
#else
	#define NO_MESHLIB_PARAM(x)
#endif


rmcDrawable* phDemoWorld::GetDrawable(const char* NO_MESHLIB_PARAM(name))
{
#if MESH_LIBRARY	
    rmcDrawable** data = sm_DrawableDictionary->Access(name);
    rmcDrawable* drawable = data ? *data : NULL;
    
    if (drawable == NULL)
    {
		D3D11_ONLY(GRCDEVICE.LockContext());
        drawable = rage_new rmcDrawable;

        ASSET.PushFolder("physics");
        ASSET.PushFolder(name);

        if (!ASSET.Exists("entity","type") || !drawable->Load("entity") || !drawable->GetLodGroup().ContainsLod(LOD_HIGH))
        {
            delete drawable;
            drawable = NULL;
        }
        ASSET.PopFolder();
        ASSET.PopFolder();

        if (drawable != NULL)
        {
            sm_DrawableDictionary->Insert(name, drawable);
        }
		D3D11_ONLY(GRCDEVICE.UnlockContext());
    }

    return drawable;
#else
	return NULL;
#endif
}


phExplosionMgr* phDemoWorld::GetExplosionMgr ()
{
	Assert(m_MouseInput);
	return m_MouseInput->GetExplosionMgr();
}


/////////////////////////////////////////////////////////////////////

phDemoWorld::phDemoWorld(const char* name)
	: m_Name(name)
	, m_PreUpdate(UpdateCallback::NullFunctor())
	, m_PostUpdate(UpdateCallback::NullFunctor())
{
	AssertMsg(name , "phDemoWorld must be created with a name.");
	Assert(sm_ClassInitialized);

	const char* redrawWhilePausedParam;
	if (PARAM_redrawwhilepaused.Get(redrawWhilePausedParam))
	{
		if (stricmp(redrawWhilePausedParam, "false") == 0)
		{
			sm_RedrawWhilePaused = false;
		}
	}

	m_AdvanceFrame      = false;
    m_AltFixedFrameMode = false;

	// time parameters
	SetFramerate(60.0f);
	m_ElapsedTime = 0.0f;
	m_AdvanceRepeatDelay = 0.5f;
	m_GravityScale = 1.0f;
	m_Gravity.Set(0.0f,GRAVITY,0.0f);
	m_FrameTime = 0.0f;
    m_SimTimeLastFrame = 0.f;

	// data structures init
	m_MaxObjects = -1;
	m_NumObjects = -1;
	m_MaxUpdateObjects = -1;
	m_NumUpdateObjects = -1;
	m_Objects = NULL;
	m_UpdateObjects = NULL;
	m_PhysLevel = NULL;
	m_PhysSim = NULL;

	// mouse click parameters
	m_NumGizmos = 0;
	for (int gizmoIndex=0; gizmoIndex<DEMOWORLD_MAX_NUM_GIZMOS; gizmoIndex++)
	{
		m_Gizmos[gizmoIndex] = NULL;
	}

	// Set the up direction to y.
	m_UpAxis = 1;

	m_ProfilerEnabled = true;


#if __BANK
	m_CurrentObjectBank = 0;
	m_ObjectBank = NULL;
	m_CurrentUpdateObjectBank = 0;
	m_UpdateObjectBank = NULL;
#endif
}


////////////////////////////////////////////////////////////
// main iteration loop

void phDemoWorld::Update()
{
	PDR_ONLY(debugPlayback::RecordNewPhysicsFrame();)

	m_FrameTime = m_FrameTimer.GetTime();
	m_FrameTimer.Reset();

	m_UpdatedLastFrame = ShouldUpdate();

    if (ShouldUpdate())
    {
        float time = TIME.GetSeconds();

        m_SimTimeLastFrame += time;
        
        if ((m_AltFixedFrameMode && m_SimTimeLastFrame > sm_FixedFrameAltStep) || !m_AltFixedFrameMode)
        {
            //in alternate fixed frame mode, we timestep a fixed amount but we can step multiple times
            //to make up a deficit to try to maintain a a realtime update rate.

            float   stepTime    = 0.f;
            int     numSteps    = 1; 

            if (m_AltFixedFrameMode)
            {
                stepTime    = sm_FixedFrameAltStep;
                numSteps    = (int)(m_SimTimeLastFrame/stepTime);
             
                if (numSteps > sm_FixedFrameStepLimit)
                {
                    stepTime    = (sm_FixedFrameAltStep*sm_FixedFrameStepMultiplier);
                    numSteps    = (int)(m_SimTimeLastFrame/stepTime);
                }
                
                m_SimTimeLastFrame -= (numSteps*stepTime);

                //Warningf("m_SimTimeLastFrame %f, numSteps %d",m_SimTimeLastFrame, numSteps);
            } 
            else
            {
                stepTime = time;
                m_SimTimeLastFrame = time;              
            }

            for (int i = 0; i < numSteps; i++)
            {
                float subTime = stepTime / sm_Oversampling;

                for (int iteration = 0; iteration < sm_Oversampling; ++iteration)
                {
					bool finalUpdate = (iteration == (sm_Oversampling - 1));
					m_PreUpdate(subTime);

                    m_PhysSim->PreUpdateInstanceBehaviors(subTime);
                    m_PhysSim->Update(subTime,finalUpdate);
                    m_PhysSim->UpdateInstanceBehaviors(subTime);

                    for (int objectIndex=0; objectIndex<m_NumUpdateObjects; objectIndex++)
                    {
                        if (m_UpdateObjects[objectIndex] && m_UpdateObjects[objectIndex]->GetLevelIndex()!=phInst::INVALID_INDEX)
                        {
                            m_UpdateObjects[objectIndex]->Update(subTime);
                        }
                    }

					m_PostUpdate(subTime);
                }

                m_ElapsedTime += stepTime;
            }
#if USE_FRAME_PERSISTENT_GJK_CACHE
			GJKCacheSystemPostCollisionUpdate(GJKCACHESYSTEM);
#endif // USE_FRAME_PERSISTENT_GJK_CACHE
        }

		for (int object = 0; object < sm_AddAndDeleteObjects; ++object)
		{
			int objectIndex = g_ReplayRand.GetRanged(0,m_NumObjects-1);
			phInst* instance = m_Objects[objectIndex]->GetPhysInst();
			if (instance->IsInLevel())
			{
				PHSIM->DeleteObject(instance->GetLevelIndex());
			}
			else
			{
				int initialState = m_Objects[objectIndex]->GetInitialStateFlag();
				switch (initialState)
				{
					case phLevelBase::STATE_FLAG_ACTIVE:
					{
						PHSIM->AddActiveObject(instance);
						break;
					}
					case phLevelBase::STATE_FLAG_INACTIVE:
					{
						PHSIM->AddInactiveObject(instance);
						break;
					}
					case phLevelBase::STATE_FLAG_FIXED:
					{
						PHSIM->AddFixedObject(instance);
						break;
					}
				}
			}
		}
    }

    m_AdvanceFrame = false;

    UpdateInput();
}

////////////////////////////////////////////////////////////

void phDemoWorld::Activate (dcamCamMgr* cameraManager)
{
    if (sm_ActiveDemo==this)
    {
        return;
    }

	if (sm_ActiveDemo!=NULL)
	{
		sm_ActiveDemo->Deactivate();
	}

	// Set the global up direction to this demo world's up direction.
	Mat34V ident(V_IDENTITY);
	SetUnitUp(RCC_MATRIX34(ident).GetVector(m_UpAxis));

	// Set the gravity scale factor on all the demo objects.
	SetGravityScale(m_GravityScale);

	// Set gravity in the simulator.
	phSimulator::SetGravity(m_Gravity);

	if (cameraManager)
	{
		// Get the camera's previous up direction.
		int prevUpAxis = (cameraManager->GetUseZUp() ? 2 : 1);
		if (prevUpAxis!=m_UpAxis)
		{
			// Set the camera up direction.
			cameraManager->SetUseZUp(m_UpAxis==2);
			Matrix34 cameraWorld(cameraManager->GetWorldMtx());
			cameraWorld.Rotate(XAXIS,(m_UpAxis==2 ? 0.5f*PI : -0.5f*PI));
			cameraManager->SetWorldMtx(cameraWorld);
		}
	}

	m_PhysSim->SetActiveInstance();
	m_PhysLevel->SetActiveInstance();
	m_MouseInput->SetActiveInstance();

	m_Active = true;
	m_FirstFrameSinceActivation = true;
	sm_ActiveDemo = this;

	// Set the time warp after activating, so that the static TIME is set from this demo world.
	SetTimeWarp(sm_TimeWarp);
	m_ElapsedTime = 0.0f;

	for (int gizmoIndex=0; gizmoIndex<m_NumGizmos; gizmoIndex++)
	{
		m_Gizmos[gizmoIndex]->Activate();
	}

    sm_activeWorldFrameRate = m_BaseFrameRate;
}


void phDemoWorld::Deactivate()
{
#if __PFDRAW
	GetRageProfileDraw().Flush();
#endif // __PFDRAW

	if (m_Active)
	{
		Assert(sm_ActiveDemo==this);
		sm_ActiveDemo = NULL;
		m_Active = false;
		for (int gizmoIndex=0; gizmoIndex<m_NumGizmos; gizmoIndex++)
		{
			m_Gizmos[gizmoIndex]->Deactivate();
		}
	}
}


////////////////////////////////////////////////////////////////////
// physics control

void phDemoWorld::SetFramerate(float fps)
{
	m_BaseFrameRate = fps;

    if ( this == phDemoWorld::GetActiveDemo() )
    {
        sm_activeWorldFrameRate = fps;
    }

	SetTimeWarp(1.0f);
}


void phDemoWorld::SetTimeWarp(float timeWarp)
{
	// Find the new frame time that will result from this time warp.
	float newFrameTime = timeWarp/m_BaseFrameRate;

	// Get the minimum and maximum frame times from the time manager.
	float minFrameTime,maxFrameTime;
	TIME.GetFrameTimeClamp(minFrameTime,maxFrameTime);
	if (newFrameTime>=minFrameTime && newFrameTime<=maxFrameTime)
	{
		sm_TimeWarp = timeWarp;

		// keep fps const, but each has less simulation time
		m_FrameRate = m_BaseFrameRate;
		m_SimTimePerFrame = (1.0f / m_FrameRate) * timeWarp;
	}

	if (m_Active)
	{
		if (sm_FixedFrame)
		{
			TIME.SetFixedFrameMode(1.0f / m_SimTimePerFrame);
			TIME.SetTimeWarp(1.0f);
            TIME.SetClamp(1.0f / 1000.0f, sm_MaximumFrameTime);
		}
		else
		{
			TIME.SetRealTimeMode();
			TIME.SetTimeWarp(sm_TimeWarp);
            TIME.SetClamp((1.0f / 1000.0f),1.f);
		}
	}
}

void phDemoWorld::SetFixedFrame(bool state,bool altMode)
{
    sm_FixedFrame = state;
    SetTimeWarp(sm_TimeWarp);

    if (altMode)
    {
        //use the alternate fixed frame mode..
        m_AltFixedFrameMode = true;
    }
    else
    {
        m_AltFixedFrameMode = false;
    }
}

void phDemoWorld::SetGravityScale(float gravity)
{
	m_GravityScale = gravity;

	for (int i=0; i<m_NumObjects; i++)
	{
		if (m_Objects[i])
		{
			m_Objects[i]->GetPhysInst()->GetArchetype()->SetGravityFactor(gravity);
		}
	}
}

void phDemoWorld::SetSleepEnable(bool enabled)
{
	if (m_PhysSim)
	{
		m_PhysSim->SetSleepEnabled(enabled);
	}
}


void phDemoWorld::Reset()
{
	if (m_ResetWorldClbk)
	{
		m_ResetWorldClbk();
	}

	// reset the profiler
#if __STATS
	GetRageProfiler().Reset();
#endif // __STATS

	// reset the core m_PhysSim
	Assert(m_PhysSim!=NULL);
	m_PhysSim->Reset();
	m_PhysSim->ResetInstanceBehaviors();
	m_ElapsedTime = 0.0f;

	m_PhysSim->SortUnusedManagedColliderIndices();

	// and then do high level resetting
	int i;
	for (i=0; i<m_NumObjects; i++)
	{
		if (m_Objects[i])
		{
			m_Objects[i]->Reset(m_PhysLevel, m_PhysSim);
            m_PhysLevel->GetBroadPhase()->pruneActiveOverlappingPairs();
		}
	}

	for (i=0;i<m_NumUpdateObjects;i++)
	{
		if (m_UpdateObjects[i])
		{
			m_UpdateObjects[i]->Reset(m_PhysLevel, m_PhysSim);
		}
	}

    m_MouseInput->Reset();

#ifdef SHATTERMGR
	if (SHATTERMGR)
	{
		SHATTERMGR->Reset(m_PhysSim);
	}
#endif // SHATTERMGR

}

PARAM(phbroadphase, "Specify the type of broadphase to use (level, axisSweep1, or axisSweep3.");

void phDemoWorld::Init(const int knMaxOctreeNodes, int knMaxActiveObjects, int maxObjects, Vec3V_In worldMin, Vec3V_In worldMax, const int maxInstBehaviors/*=0*/, int scratchpadSize, int maxManifolds, int maxExternVelManifolds, int numManagedConstraints)
{
	phLevelNew* pLevelNew = rage_new phLevelNew;
	pLevelNew->SetExtents(worldMin, worldMax);
	pLevelNew->SetMaxObjects(maxObjects);
	pLevelNew->SetMaxActive(knMaxActiveObjects);
	pLevelNew->SetNumOctreeNodes(knMaxOctreeNodes);
	pLevelNew->SetMaxCollisionPairs(maxObjects * 10);

    const char* bpChoice = "axisSweep3";
    PARAM_phbroadphase.Get(bpChoice);

    if (stricmp(bpChoice, "level") == 0)
    {
        pLevelNew->SetBroadPhaseType(phLevelNew::LEVEL);
    }
    else if (stricmp(bpChoice, "axisSweep1") == 0 || stricmp(bpChoice, "axis1") == 0)
    {
        pLevelNew->SetBroadPhaseType(phLevelNew::AXISSWEEP1);
    }
    else if (stricmp(bpChoice, "axisSweep3") == 0 || stricmp(bpChoice, "axis3") == 0)
    {
        pLevelNew->SetBroadPhaseType(phLevelNew::AXISSWEEP3);
    }
	else if (stricmp(bpChoice, "spatialHash") == 0 )
	{
		pLevelNew->SetBroadPhaseType(phLevelNew::SPATIALHASH);
	}
	else if (stricmp(bpChoice, "nxn") == 0 )
	{
		pLevelNew->SetBroadPhaseType(phLevelNew::NXN);
	}

#if LEVELNEW_ENABLE_DEFERRED_OCTREE_UPDATE
	pLevelNew->SetMaxDeferredInstanceCount(knMaxActiveObjects * 2);
#endif	// LEVELNEW_ENABLE_DEFERRED_OCTREE_UPDATE
	pLevelNew->Init();

	phSimulator* sim = rage_new phSimulator;
	phSimulator::InitParams params;
	params.maxManagedColliders = knMaxActiveObjects;
	params.scratchpadSize = scratchpadSize;
	params.maxManifolds = maxManifolds; 
	params.maxCompositeManifolds = maxManifolds / 3;
	params.maxContacts = Min(65534, maxManifolds * 4);
	params.maxExternVelManifolds = maxExternVelManifolds;
	params.maxInstBehaviors = (u16)maxInstBehaviors;
	const int maxNumConstraints = 32 + numManagedConstraints;
	params.maxConstraints = maxNumConstraints;
    sim->Init(pLevelNew, params);

	Init(pLevelNew, sim, maxObjects);

	// We Activate() this demo world automatically upon initialization because, at least currently using sweep and prune, an active physics level is required
	//   in order to insert objects into the physics level (that sounds weird, but the sweep and prune algorithm tries to access the physics level using
	//   the static GetCurrent() function).
	Activate();
}

void phDemoWorld::Init(phLevelNew* level, phSimulator* sim, int maxObjects)
{
	m_PhysLevel = level;
	m_PhysSim = sim;

	m_MaxObjects = maxObjects;
	m_NumObjects = 0;
	m_Objects = rage_new phDemoObject*[m_MaxObjects];

	m_Mapper.Reset();
	m_Mapper.Map(IOMS_KEYBOARD,		KEY_LCONTROL,			m_AdjustMaximumFrameTime);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_ADD, 				m_OversampleUp);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_SUBTRACT,			m_OversampleDown);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_LBRACKET, 			m_TimewarpSlower);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_RBRACKET, 			m_TimewarpFaster);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_9,	 				m_DecreaseGravity);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_0,		 			m_IncreaseGravity);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_A,		 			m_ActivateAll);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_D,		 			m_DeactivateAll);

	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_COMMA, 				m_Pause);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_PERIOD, 			m_FrameAdvance);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_SLASH,				m_ToggleFixedFrame);

	m_MouseInput = rage_new phMouseInput;
	phMouseInput::EnableLeftClick();
}


void phDemoWorld::InitUpdateObjects(int maxUpdateObjects)
{
	Assert(m_UpdateObjects==NULL);
	m_MaxUpdateObjects = maxUpdateObjects;
	m_UpdateObjects = rage_new phUpdateObject*[m_MaxUpdateObjects];
	m_NumUpdateObjects = 0;
}


void phDemoWorld::Shutdown()
{
	delete m_MouseInput;

	if (m_Objects)
	{
		for (int i = 0; i < m_NumObjects; i++)
		{
			if (m_Objects[i] && m_Objects[i]->GetDeleteOnShutdown())
			{
				delete m_Objects[i];
			}
		}

		delete[] m_Objects;
	}
	if (m_UpdateObjects)
	{
		for (int i = 0; i < m_NumUpdateObjects; i++)
		{
			if (m_UpdateObjects[i] && m_UpdateObjects[i]->GetDeleteOnShutdown())
			{
				delete m_UpdateObjects[i];
			}
		}

		delete[] m_UpdateObjects;
	}

	m_PhysLevel->Shutdown();
	delete m_PhysLevel;
	m_PhysLevel = NULL;

	delete m_PhysSim;
	m_PhysSim = NULL;

	if (m_PhysSim==phSimulator::GetActiveInstance())
	{
		phSimulator::SetActiveInstance(NULL);
	}

	for (int gizmoIndex=0; gizmoIndex<m_NumGizmos; gizmoIndex++)
	{
		delete m_Gizmos[gizmoIndex];
	}

    m_TakeControlClbk = NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

void phDemoWorld::FixGrabForBreak(phInst* brokenInst, const atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS>& brokenParts, phInst* newInst)
{
	m_MouseInput->FixGrabForBreak(brokenInst, brokenParts, newInst);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

phCollider* phDemoWorld::GetCollider (phDemoObject* object) const
{
	return m_PhysSim->GetCollider(object->GetPhysInst());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

void phDemoWorld::AddTranslationGizmo (Matrix34& controlledMatrix)
{
	Assert(m_NumGizmos<DEMOWORLD_MAX_NUM_GIZMOS);
	m_Gizmos[m_NumGizmos++] = rage_new gzTranslation(controlledMatrix);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

void phDemoWorld::AddRotationGizmo (Matrix34& controlledMatrix)
{
	Assert(m_NumGizmos<DEMOWORLD_MAX_NUM_GIZMOS);
	m_Gizmos[m_NumGizmos++] = rage_new gzRotation(controlledMatrix);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

void phDemoWorld::GetWorldExtents(Vector3& wMin, Vector3& wMax)
{
	wMin.Set(FLT_MAX);
	wMax.Set(-FLT_MAX);
	for (int i=0; i<m_NumObjects; i++)
	{
		if (m_Objects[i])
		{
			float radius = m_Objects[i]->GetPhysInst()->GetArchetype()->GetBound()->GetRadiusAroundCentroid();
			Vector3 centroid = VEC3V_TO_VECTOR3(m_Objects[i]->GetPhysInst()->GetArchetype()->GetBound()->GetCentroidOffset());
			m_Objects[i]->GetInitialMatrix().Transform(centroid);
			wMin.x = Min(wMin.x, centroid.x - radius);
			wMin.y = Min(wMin.y, centroid.y - radius);
			wMin.z = Min(wMin.z, centroid.z - radius);
			wMax.x = Max(wMax.x, centroid.x + radius);
			wMax.y = Max(wMax.y, centroid.y + radius);
			wMax.z = Max(wMax.z, centroid.z + radius);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////

void phDemoWorld::Draw()
{
	Matrix34 offsetWorld(RCC_MATRIX34(grcViewport::GetCurrentWorldMtx()));
	offsetWorld.d.Add(phDemoObject::GetWorldOffset());
	grcViewport::SetCurrentWorldMtx(RC_MAT34V(offsetWorld));
	int objectIndex, levelIndex;
	for (objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
	{
		if (m_Objects[objectIndex])
		{
			levelIndex = m_Objects[objectIndex]->GetLevelIndex();
			if(levelIndex!=phInst::INVALID_INDEX)
			{
				if (phInst* inst = m_PhysLevel->GetInstance(levelIndex))
				{
					const Matrix34& objMtx = *(const Matrix34*)(&inst->GetMatrix());

#if __PFDRAW
					if (PFD_Models.GetEnabled())
					{
						m_Objects[objectIndex]->DrawModel(objMtx);
					}
#endif	// __PFDRAW

					if (sm_DrawLabels && m_Objects[objectIndex]->GetLabel())
					{
						grcColor(Color_white);
						grcLighting(false);
						grcDrawLabel(objMtx.d, 0, 0, m_Objects[objectIndex]->GetLabel());
					}
				}
			}
		}
	}
	for (objectIndex=0; objectIndex<m_NumUpdateObjects; objectIndex++)
	{
		if (m_UpdateObjects[objectIndex])
		{
			levelIndex = m_UpdateObjects[objectIndex]->GetLevelIndex();
			if(levelIndex!=phInst::INVALID_INDEX)
			{
				if (phInst* inst = m_PhysLevel->GetInstance(levelIndex))
				{
					const Matrix34& objMtx = *(const Matrix34*)(&inst->GetMatrix());

#if __PFDRAW
					if (PFD_Models.GetEnabled())
					{
						m_UpdateObjects[objectIndex]->DrawModel(objMtx);
					}
#endif	// __PFDRAW

					if (sm_DrawLabels && m_UpdateObjects[objectIndex]->GetLabel())
					{
						grcColor(Color_white);
						grcLighting(false);
						grcDrawLabel(objMtx.d, 0, 0, m_UpdateObjects[objectIndex]->GetLabel());
					}
				}
			}
		}
	}

#if __PFDRAW
    bool redrawThisFrame = sm_RedrawWhilePaused;
    static bool sm_RedrawWhilePausedWentFalseLastFrame = false;

    if (!redrawThisFrame)
    {
        if (sm_RedrawWhilePausedWentFalseLastFrame || sm_RedrawNextFrameWhilePaused)
		{
			redrawThisFrame = true;
		}

		sm_RedrawWhilePausedWentFalseLastFrame = false;
		sm_RedrawNextFrameWhilePaused = false;
    }
	else
	{
		sm_RedrawWhilePausedWentFalseLastFrame = true;
	}

	if (redrawThisFrame || UpdatedLastFrame() || m_FirstFrameSinceActivation)
	{
		m_PhysSim->ProfileDraw();
	}
#endif // __PFDRAW

	m_MouseInput->Draw();

	DR_ONLY(debugPlayback::DebugDraw(sm_Paused));

	m_FirstFrameSinceActivation = false;
}

void phDemoWorld::UpdateMouse(Vec::V3Param128 timeStep)
{
	m_MouseInput->Update(!ShouldUpdate(), timeStep);
}

void phDemoWorld::UpdateInput()
{
	m_Mapper.Update();

#if __DEBUGBUTTONS
	if (m_ActivateAll.IsPressed() || ioPad::GetPad(0).GetPressedDebugButtons() & ioPad::RDOWN)
	{
        TakeControlAll();
		m_PhysSim->ActivateAll();
	}
#endif
	if (m_DeactivateAll.IsPressed())
	{
		TakeControlAll();
#if ENABLE_PHYSICS_LOCK
		phIterator iterator(phIterator::PHITERATORLOCKTYPE_WRITELOCK);
#else // ENABLE_PHYSICS_LOCK
		phIterator iterator;
#endif // ENABLE_PHYSICS_LOCK

		iterator.InitCull_All();
		iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_ACTIVE);

		u16 levelIndex = iterator.GetFirstLevelIndex(m_PhysLevel);
		while(levelIndex != (u16)(-1))
		{
			PHSIM->DeactivateObject(levelIndex);

			levelIndex = iterator.GetNextLevelIndex(m_PhysLevel);
		}
	}

	if (m_Pause.IsPressed())
	{
		Pause();
	}

	if (m_FrameAdvance.IsPressed())
	{
		FrameAdvance();
	}
	else
	{
		if (m_FrameAdvance.IsDown())
		{
			m_AdvanceTimer += TIME.GetSeconds();

			if (m_AdvanceTimer > m_AdvanceRepeatDelay)
			{
				FrameAdvance();
			}
		}
		else
		{
			m_AdvanceTimer = 0.0f;
		}
	}
	
	if (m_TimewarpSlower.IsPressed())
	{
		if (m_AdjustMaximumFrameTime.IsDown())
		{
			sm_MaximumFrameTime -= 1.0f / 60.0f;
			if (sm_MaximumFrameTime < 1.0f / 60.0f)
			{
				sm_MaximumFrameTime = 1.0f / 60.0f;
			}
		}
		else
		{
			if (sm_TimeWarp >= 1.0f)
			{
				sm_TimeWarp -= 0.5f;
			}
			else
			{
				sm_TimeWarp *= 0.5f;
			}
		}

		SetTimeWarp(sm_TimeWarp);
	}

	if (m_TimewarpFaster.IsPressed())
	{
		if (m_AdjustMaximumFrameTime.IsDown())
		{
			sm_MaximumFrameTime += 1.0f / 60.0f;
		}
		else
		{
			if (sm_TimeWarp >= 0.5f)
			{
				sm_TimeWarp += 0.5f;
			}
			else
			{
				sm_TimeWarp *= 2.0f;
			}
		}

		SetTimeWarp(sm_TimeWarp);
	}

	if (m_OversampleUp.IsPressed())
	{
		sm_Oversampling++;
	}

	if (m_OversampleDown.IsPressed())
	{
		sm_Oversampling--;
		
		sm_Oversampling = Max(1, sm_Oversampling);
	}

	if (m_ToggleFixedFrame.IsPressed())
	{
		sm_FixedFrame = !sm_FixedFrame;

		SetTimeWarp(sm_TimeWarp);
	}

	if (m_DecreaseGravity.IsPressed())
	{
		if (m_GravityScale > 0.0f)
		{
			if (m_GravityScale > 0.001f)
			{
				SetGravityScale(m_GravityScale * 0.5f);
			}
			else
			{
				SetGravityScale(-0.001f);
			}
		}
		else
		{
			if (m_GravityScale > -2.0f)
			{
				SetGravityScale(m_GravityScale * 2.0f);
			}
		}
	}

	if (m_IncreaseGravity.IsPressed())
	{
		if (m_GravityScale > 0.0f)
		{
			if (m_GravityScale < 2.0f)
			{
				SetGravityScale(m_GravityScale * 2.0f);
			}
		}
		else
		{
			if (m_GravityScale < -0.001f)
			{
				SetGravityScale(m_GravityScale * 0.5f);
			}
			else
			{
				SetGravityScale(0.001f);
			}
		}
	}

	if (ioKeyboard::KeyPressed(KEY_BACKSLASH))
	{
		SetFramerate(m_BaseFrameRate);
	}

	for (int i=0; i<m_NumUpdateObjects; i++)
	{
		if (m_UpdateObjects[i] && m_UpdateObjects[i]->GetLevelIndex()!=phInst::INVALID_INDEX)
		{
			m_UpdateObjects[i]->UpdateInput();
		}
	}
}

void phDemoWorld::SetUpdateCallbacks(UpdateCallback preUpdate, UpdateCallback postUpdate)
{
	m_PreUpdate = preUpdate;
	m_PostUpdate = postUpdate;
}

//////////////////////////////////////////////////////////////////////////
// creation of objects, generic

phDemoObject* phDemoWorld::RequestObject()
{
	Assert(m_NumObjects<m_MaxObjects);

	phDemoObject* object = rage_new phDemoObject;
	m_Objects[m_NumObjects++] = object;

	return object;
}


// an active object, added by instance
bool phDemoWorld::AddActiveObject(phDemoObject* object, bool alwaysActive)
{
	m_PhysSim->AddActiveObject(object->GetPhysInst(), alwaysActive);
	if (object->GetLevelIndex()!=phInst::INVALID_INDEX)
	{
		Assert(m_PhysLevel->LegitLevelIndex(object->GetLevelIndex()));
		object->SetInitialStateFlag(phLevelBase::STATE_FLAG_ACTIVE);

		return true;
	}

	return false;
}


void phDemoWorld::AddInactiveObject(phDemoObject* object)
{
	m_PhysSim->AddInactiveObject(object->GetPhysInst());
	Assert(m_PhysLevel->LegitLevelIndex(object->GetLevelIndex()));
	object->SetInitialStateFlag(phLevelBase::STATE_FLAG_INACTIVE);
}


void phDemoWorld::RegisterObject(phDemoObject* object, int initialStateFlag)
{
	object->SetDeleteOnShutdown(false);
	object->SetInitialStateFlag(initialStateFlag);
	m_Objects[m_NumObjects++] = object;
}


bool phDemoWorld::RemoveObject(phDemoObject* object)
{
	for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
	{
		if (m_Objects[objectIndex]==object)
		{
			m_Objects[objectIndex] = NULL;
			return true;
		}
	}

	return false;
}


phDemoObject* phDemoWorld::CreateObject (const char* file, Mat34V_In mtx, bool alignBottom, bool alwaysActive, bool startActive, Functor0Ret<phInst*> createFunc, bool uniqueArchetype)
{
	return CreateObject(file,MAT34V_TO_MATRIX34(mtx),alignBottom,alwaysActive,startActive,createFunc,uniqueArchetype);
}


phDemoObject* phDemoWorld::CreateObject(const char* file, const Matrix34& mtx, bool alignBottom,
										  bool alwaysActive, bool startActive, Functor0Ret<phInst*> createFunc, bool uniqueArchetype)
{
	phDemoObject* object = RequestObject();

	object->InitGfx(file);

	if (uniqueArchetype)
	{
		object->InitPhys(file, mtx, uniqueArchetype, createFunc);
	}
	else
	{
		// See if a physics archetype with the given name already exists.
		phArchetype* archetype = ARCHMGR.FindArchetype(file);
		if (archetype)
		{
			object->InitPhys(archetype, mtx, createFunc);
		}
		else
		{
			object->InitPhys(file, mtx, uniqueArchetype, createFunc);
		}
	}

	if (alignBottom)
	{
		object->AlignBottom();
	}

	if (startActive)
	{
		AddActiveObject(object,alwaysActive);
	}
	else
	{
		AddInactiveObject(object);
	}

	return object;
}

phDemoObject* phDemoWorld::CreateObject(const char* file, Vec3V_In position, bool alignBottom, Vec3V_In rotation, bool alwaysActive, bool startActive, Functor0Ret<phInst*> createFunc, bool uniqueArchetype)
{
	return CreateObject(file,VEC3V_TO_VECTOR3(position),alignBottom,VEC3V_TO_VECTOR3(rotation),alwaysActive,startActive,createFunc,uniqueArchetype);
}

phDemoObject* phDemoWorld::CreateObject(const char* file, const Vector3& position, bool alignBottom,
										  const Vector3& rotation, bool alwaysActive, bool startActive, Functor0Ret<phInst*> createFunc, bool uniqueArchetype)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position,rotation));
	return CreateObject(file, mtx, alignBottom, alwaysActive, startActive, createFunc, uniqueArchetype);
}

phDemoObject* phDemoWorld::CreateBox(const Vector3& boxExtents, float density, const Matrix34& mtx, bool alignBottom,
										bool alwaysActive)
{
	phDemoObject* object = RequestObject();

	object->InitBoxPhys(boxExtents, density, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddActiveObject(object, alwaysActive);

	return object;
}

phDemoObject* phDemoWorld::CreateBox(const Vector3& boxExtents, float density, const Vector3& position, bool alignBottom,
										const Vector3& rotation, bool alwaysActive)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateBox(boxExtents, density, mtx, alignBottom, alwaysActive);
}

phDemoObject* phDemoWorld::CreateRubiks(const Vector3& boxExtents, float spacing, u32 height, u32 width, u32 depth, float density, const Matrix34& mtx, bool alignBottom,
										bool alwaysActive)
{
	phDemoObject* object = RequestObject();

	object->InitRubiksPhys(boxExtents, spacing, height, width, depth, density, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddActiveObject(object, alwaysActive);

	return object;
}

phDemoObject* phDemoWorld::CreateRubiks(const Vector3& boxExtents, float spacing, u32 height, u32 width, u32 depth, float density, const Vector3& position, bool alignBottom,
										const Vector3& rotation, bool alwaysActive)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateRubiks(boxExtents, spacing, height, width, depth, density, mtx, alignBottom, alwaysActive);
}

phDemoObject* phDemoWorld::CreateSphere(float radius, float density, const Vector3& position, bool alignBottom, bool alwaysActive, float maxSpeed, float maxAngSpeed)
{
	phDemoObject* object = RequestObject();

	object->InitSpherePhys(radius, density, position, maxSpeed, maxAngSpeed);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddActiveObject(object, alwaysActive);

	return object;
}

phDemoObject* phDemoWorld::CreateCapsule(float length, float radius, float density, const Matrix34& mtx, bool alignBottom,
										bool alwaysActive)
{
	phDemoObject* object = RequestObject();

	object->InitCapsulePhys(length, radius, density, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddActiveObject(object, alwaysActive);

	return object;
}

phDemoObject* phDemoWorld::CreateCapsule(float length, float radius, float density, const Vector3& position, bool alignBottom,
										const Vector3& rotation, bool alwaysActive)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateCapsule(length, radius, density, mtx, alignBottom, alwaysActive);
}


// a fixed object, added by instance
void phDemoWorld::AddFixedObject(phDemoObject* object)
{
	m_PhysSim->AddFixedObject(object->GetPhysInst());
	Assert(m_PhysLevel->LegitLevelIndex(object->GetLevelIndex()));
	object->SetInitialStateFlag(phLevelBase::STATE_FLAG_FIXED);
}


phSwingingObject* phDemoWorld::CreateSwingingObject (const char* name, const Vector3& position, const Vector3& rotation, const Vector3& constraintPosition,
														const Vector3& frequency, const Vector3& amplitude, const Vector3& phaseOffset)
{
	phSwingingObject* swingingObject = rage_new phSwingingObject;
	swingingObject->InitGfx(name);
	Matrix34 colliderMatrix(CreateRotatedMatrix(position,rotation));
	swingingObject->InitPhys(name,colliderMatrix);
	AddActiveObject(swingingObject,true);
	AddUpdateObject(swingingObject);
	swingingObject->SetMovingConstraint(constraintPosition,m_PhysSim->GetConstraintMgr());
	swingingObject->SetFrequency(frequency);
	swingingObject->SetAmplitude(amplitude);
	swingingObject->SetPhaseOffset(phaseOffset);
	return swingingObject;
}

phWhirlingObject* phDemoWorld::CreateWhirlingObject (const char* name, const Vector3& position, const Vector3& rotation, float period, float mag, float phase)
{
    phWhirlingObject* whirlingObject = rage_new phWhirlingObject(period, mag, phase);
    whirlingObject->InitGfx(name);
    Matrix34 colliderMatrix(CreateRotatedMatrix(position,rotation));
    whirlingObject->InitPhys(name,colliderMatrix);
    AddActiveObject(whirlingObject,true);
    AddUpdateObject(whirlingObject);
    return whirlingObject;
}


#if 0
phDemoObject* phDemoWorld::CreateShovableObject(const char*file, const Matrix34& mtx, bool alignBottom)
{
	phDemoObject* object = RequestObject();

	phShovableInstance* instance=rage_new phShovableInstance;
	object->InitPhys(file, instance, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddActiveObject(object);

	object->InitGfx(file);

	return object;
}


phDemoObject* phDemoWorld::CreateShovableObject(const char* file, const Vector3& position, bool alignBottom, const Vector3& rotation)
{
	Matrix34 mtx(CreateRotatedMatrix(position, rotation));
	return CreateShovableObject(file, mtx, alignBottom);
}


void phDemoWorld::InsertFixedObject(phDemoObject* object)
{
	Assert(m_NumObjects<m_MaxObjects);

	m_Objects[m_NumObjects++] = object;

	object->DeleteOnShutdown = false;

	AddFixedObject(object);

}


phDemoObject* phDemoWorld::CreateFixedObject(const char* file, const Vector3& position, bool alignBottom, const Vector3& rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateFixedObject(file, mtx, alignBottom);
}
#endif

phDemoObject* phDemoWorld::CreateFixedObject(const char* file, Mat34V_In mtx, bool alignBottom)
{
	return CreateFixedObject(file,MAT34V_TO_MATRIX34(mtx),alignBottom);
}

phDemoObject* phDemoWorld::CreateFixedObject(const char* file, const Matrix34& mtx, bool alignBottom)
{
	phDemoObject* object = RequestObject();

	object->InitPhys(file, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddFixedObject(object);

	object->InitGfx(file);

	return object;
}

phDemoObject* phDemoWorld::CreateFixedObject(const char* file, Vec3V_In position, bool alignBottom, Vec3V_In rotation)
{
	return CreateFixedObject(file,VEC3V_TO_VECTOR3(position),alignBottom,VEC3V_TO_VECTOR3(rotation));
}

phDemoObject* phDemoWorld::CreateFixedObject(const char* file, const Vector3& position, bool alignBottom, const Vector3& rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateFixedObject(file, mtx, alignBottom);
}

phDemoObject* phDemoWorld::CreateDoor (const char* file, const Vector3& position, bool alignBottom, const Vector3& rotation)
{
	phDemoObject* object = CreateObject(file,position,alignBottom,rotation);
	Assert(object && object->GetPhysInst() && object->GetPhysInst()->GetArchetype() && object->GetPhysInst()->GetArchetype()->GetBound());

	phConstraintHinge::Params hingeConstraint;
	hingeConstraint.instanceA = object->GetPhysInst();
	hingeConstraint.worldAxis = Vec3V(V_Y_AXIS_WZERO);
	PHCONSTRAINT->Insert(hingeConstraint);

	return object;
}


phDemoObject* phDemoWorld::CreateFixedBox(const Vector3& boxExtents, const Matrix34& mtx, bool alignBottom)
{
	phDemoObject* object = RequestObject();

	object->InitBoxPhys(boxExtents, 1.0f, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	AddFixedObject(object);

	return object;
}

phDemoObject* phDemoWorld::CreateFixedBox(const Vector3& boxExtents, const Vector3& position, bool alignBottom, const Vector3& rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateFixedBox(boxExtents, mtx, alignBottom);
}

#if 0
phDemoObject* phDemoWorld::CreateFixedObject(phArchetype* archetype, const Vector3& position, bool alignBottom, const Vector3& rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateFixedObject(archetype, mtx, alignBottom);
}


phDemoObject* phDemoWorld::CreateFixedObject(phArchetype* archetype, const Matrix34& mtx, bool alignBottom)
{
	phDemoObject* object = RequestObject();

	object->InitPhys(archetype, mtx);

	if (alignBottom)
	{
		object->AlignBottom();
	}
	m_PhysSim->AddFixedObject(object->GetPhysInst());
	Assert(m_PhysLevel->LegitLevelIndex(object->GetLevelIndex()));
	object->InitialStateFlag = phLevelBase::STATE_FLAG_FIXED;

	object->ModelRM = NULL;
	object->ModelGfx = NULL;

	return object;
}


phDemoObject* phDemoWorld::CreateFixedObject(phInst* inst)
{
	phDemoObject* object = RequestObject();

	object->InitPhys(inst, inst->GetMatrix());
	m_PhysSim->AddFixedObject(object->GetPhysInst());
	Assert(m_PhysLevel->LegitLevelIndex(object->GetLevelIndex()));
	object->InitialStateFlag = phLevelBase::STATE_FLAG_FIXED;

	object->ModelRM = NULL;
	object->ModelGfx = NULL;

	return object;
}


phDemoObject* phDemoWorld::CreateBreakableObject(const char* file, const Vector3& position, bool alignBottom,
												const Vector3& rotation, float inactiveImpulseLimit, float activeImpulseLimit)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateBreakableObject(file, mtx, alignBottom, inactiveImpulseLimit, activeImpulseLimit);
}


phDemoObject* phDemoWorld::CreateBreakableObject(const char* file, const Matrix34& mtx, bool alignBottom,
													float inactiveImpulseLimit, float activeImpulseLimit)
{
	phDemoObject* object = RequestObject();

	phBRInst* inst = rage_new phBRInst;
	object->SetInst(inst);

	object->InitPhys(file, mtx);
	object->InitGfx(file);
	inst->SetInactiveImpulseLimit(inactiveImpulseLimit);
	inst->SetActiveImpulseLimit(activeImpulseLimit);

	if (alignBottom)
	{
		object->AlignBottom();
	}

	inst->SetResetMatrix(object->InitialMatrix);

	AddInactiveObject(object);

	return object;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
// creatures

// This version takes x and z coorindates and places the creature vertically with a probe.
phDemoCreature* phDemoWorld::CreateCreature(const char* name, float initX, float initZ, const Vector3& rotation)
{
	phSegment seg;
	seg.A.Set(initX, 512.0f, initZ);
	seg.B.Set(initX, -512.0f, initZ);
	phIntersection isect;
	Assert(m_PhysLevel);
	if (m_PhysLevel->TestProbe(seg, &isect))
	{
		return CreateCreature(name, isect.Position, rotation);
	}
	Assert("Couldn't place creature in level" && 0);
	return NULL;
}


phDemoCreature* phDemoWorld::CreateCreature(const char* name, const Vector3& position, const Vector3& rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return CreateCreature(name, mtx);
}


// phDemoCreatures are phUpdateObjects, so their updates and inputs are managed by the phDemoWorld.
phDemoCreature* phDemoWorld::CreateCreature(const char* name, const Matrix34& mtx)
{
	// Make the creature manager and ragdoll manager.
	if (!CREATUREMGR)
	{
		phDemoCreatureMgr::CreateCreatureManager();
	}
	if (!RAGDOLLMGR)
	{
		crRagdollMgr::CreateRagdollManager();
		RAGDOLLMGR->InitTypes();
	}

	// Get the creature type or create a new one.
	phDemoCreatureType* creatureType = CREATUREMGR->GetType(name);
	Assert(creatureType);

	// Allocate ragdolls.
	if (creatureType->GetRagdollType() && !RAGDOLLMGR->HasRagdolls())
	{
		RAGDOLLMGR->InitRagdolls(12, false);
	}

	phDemoCreature* creature = rage_new phDemoCreature;
	creature->Init(creatureType, mtx);
	AddUpdateObject(creature);
	return creature;
}


phDemoObject* phDemoWorld::CreateCrushableObject(const char* file, const char* swapFile, const Vector3& position,
												   const float impulseLimit2)
{
	Assert(m_NumUpdateObjects<m_MaxUpdateObjects);
	phCrushableObject* object = rage_new phCrushableObject;
	m_UpdateObjects[m_NumUpdateObjects++] = object;

	Matrix34 mtx;
	mtx.Identity3x3();
	mtx.d.Set(position);

	phCrushableInstance* instance=rage_new phCrushableInstance;
	instance->Object=object;
	instance->SetImpulseLimit2(impulseLimit2);

	object->SetInst(instance);
	object->InitPhys(file, mtx);

	instance->SwapArchetype=ARCHMGR.GetArchetype(swapFile, true, 0, phArchetype::ARCHETYPE_DAMP);
	Assert(instance->SwapArchetype != NULL);
	m_PhysSim->AddActiveObject(object->GetPhysInst(), true);
	Assert(m_PhysLevel->LegitLevelIndex(object->GetLevelIndex()));
	object->InitialStateFlag = phLevelBase::STATE_FLAG_ACTIVE;

	object->InitGfx(file, swapFile);

	return object;
}


phDemoObject* phDemoWorld::CreateBomb(const char* file, const char* swapFile, const Vector3& position, float impulseLimit2,
										bool stateActive)
{
	Assert(m_NumUpdateObjects<m_MaxUpdateObjects);
	phDemoBomb* object = rage_new phDemoBomb(!stateActive);
	m_UpdateObjects[m_NumUpdateObjects++] = object;

	Matrix34 mtx;
	mtx.Identity3x3();
	mtx.d.Set(position);

	phCrushableInstance* instance=rage_new phCrushableInstance;
	instance->Object=object;
	instance->SetImpulseLimit2(impulseLimit2);

	object->SetInst(instance);
	object->InitPhys(file, mtx);

	instance->SwapArchetype=ARCHMGR.GetArchetype(swapFile, true, 0, phArchetype::ARCHETYPE_DAMP);
	Assert(instance->SwapArchetype != NULL);

	Assert(instance->SwapArchetype->GetBound()->GetType()==phBound::FORCESPHERE);
	object->SetForceSphere((phForceSphere*)instance->SwapArchetype->GetBound());

	if(stateActive)
	{
		m_PhysSim->AddActiveObject(object->GetPhysInst(), true);
		object->InitialStateFlag = phLevelBase::STATE_FLAG_ACTIVE;
	}
	else
	{
		m_PhysSim->AddInactiveObject(object->GetPhysInst());
		object->InitialStateFlag = phLevelBase::STATE_FLAG_INACTIVE;
	}


	object->InitGfx(file, swapFile);

	return object;
}

#endif

void phDemoWorld::AddUpdateObject(phUpdateObject* object)
{
	Assert(m_NumUpdateObjects<m_MaxUpdateObjects);
	m_UpdateObjects[m_NumUpdateObjects++]=object;
}


///////////////////////////////////////////////////////////////////////////
// multi- or special object constructions

#if USE_GEOMETRY_CURVED
phDemoObject* phDemoWorld::CreateCurvedGeometryCylinder (Vector3::Param position, Vector3::Param rotation, float radius, float height, float ringCurveRadius, float capsCurveRadius)
{
	// Get a demo object.
	phDemoObject& curvedObject = *RequestObject();

	// Create and initialize a curved geometry bound.
	phBoundCurvedGeometry& curvedBound = *(rage_new phBoundCurvedGeometry);
	int numVerts = 4;
	int numMaterials = 1;
	int numPolys = 0;
	bool sideIsCurved = (ringCurveRadius>0.0f);
	int numCurvedEdges = (sideIsCurved ? 6 : 4);
	int numCurvedFaces = 4;
	curvedBound.Init(numVerts,numMaterials,numPolys,numCurvedEdges,numCurvedFaces);
#if COMPRESSED_VERTEX_METHOD > 0
	Vector3 bbMin(-radius, 0.0f, -radius);
	Vector3 bbMax(radius, height, radius);
	bbMin.Subtract(VEC3V_TO_VECTOR3(curvedBound.GetMarginV()));
	bbMax.Add(VEC3V_TO_VECTOR3(curvedBound.GetMarginV()));
	curvedBound.InitQuantization(RCC_VEC3V(bbMin), RCC_VEC3V(bbMax));
#endif

	// Set four vertices.
	curvedBound.SetVertex(0,Vec3V(radius,0.0f,0.0f));
	curvedBound.SetVertex(1,Vec3V(-radius,0.0f,0.0f));
	curvedBound.SetVertex(2,Vec3V(radius,height,0.0f));
	curvedBound.SetVertex(3,Vec3V(-radius,height,0.0f));

	// Set two half-circular curved edges for the bottom.
	curvedBound.SetCurvedEdge(0,1,0,radius,-YAXIS,ZAXIS);
	curvedBound.SetCurvedEdge(1,0,1,radius,-YAXIS,-ZAXIS);

	// Set two half-circular curved edges for the top.
	curvedBound.SetCurvedEdge(2,2,3,radius,YAXIS,-ZAXIS);
	curvedBound.SetCurvedEdge(3,3,2,radius,YAXIS,ZAXIS);

	if (sideIsCurved)
	{
		// Set two curved edges for the sides.
		curvedBound.SetCurvedEdge(4,0,2,ringCurveRadius,ZAXIS,XAXIS);
		curvedBound.SetCurvedEdge(5,1,3,ringCurveRadius,-ZAXIS,-XAXIS);
	}

	// Set a curved face for the bottom.
	int faceVerts[4],faceCurvedEdges[4],faceCurvedPolyEdges[4];
	faceVerts[0] = 0;
	faceVerts[1] = 1;
	faceCurvedEdges[0] = 0;
	faceCurvedEdges[1] = 1;
	faceCurvedPolyEdges[0] = 0;
	faceCurvedPolyEdges[1] = 1;
	float capsCurvature = (capsCurveRadius>0.0f ? radius/capsCurveRadius : 0.0f);
	curvedBound.SetCurvedFace(0,2,faceVerts,2,faceCurvedEdges,faceCurvedPolyEdges,capsCurvature);

	// Set a curved face for the top.
	faceVerts[0] = 2;
	faceVerts[1] = 3;
	faceCurvedEdges[0] = 2;
	faceCurvedEdges[1] = 3;
	faceCurvedPolyEdges[0] = 0;
	faceCurvedPolyEdges[1] = 1;
	curvedBound.SetCurvedFace(1,2,faceVerts,2,faceCurvedEdges,faceCurvedPolyEdges,capsCurvature);

	// Set a curved face for half of the rounded side.
	int numFaceCurvedEdges = (sideIsCurved ? 4 : 2);
	faceVerts[0] = 0;
	faceVerts[1] = 1;
	faceVerts[2] = 3;
	faceVerts[3] = 2;
	faceCurvedEdges[0] = 1;
	faceCurvedEdges[1] = (sideIsCurved ? 5 : 2);
	faceCurvedEdges[2] = 2;	// not used if !sideIsCurved
	faceCurvedEdges[3] = 4;	// not used if !sideIsCurved
	faceCurvedPolyEdges[0] = 0;
	faceCurvedPolyEdges[1] = (sideIsCurved ? 1 : 2);
	faceCurvedPolyEdges[2] = 2;	// not used if !sideIsCurved
	faceCurvedPolyEdges[3] = 3;	// not used if !sideIsCurved
	float ringCurvature = (ringCurveRadius>0.0f ? 0.5f*height/ringCurveRadius : 0.0f);
	curvedBound.SetCurvedFace(2,4,faceVerts,numFaceCurvedEdges,faceCurvedEdges,faceCurvedPolyEdges,ringCurvature);

	// Set a curved face for the other half of the rounded side.
	faceVerts[0] = 1;
	faceVerts[1] = 0;
	faceVerts[2] = 2;
	faceVerts[3] = 3;
	faceCurvedEdges[0] = 0;
	faceCurvedEdges[1] = (sideIsCurved ? 4 : 3);
	faceCurvedEdges[2] = 3;	// not used if !sideIsCurved
	faceCurvedEdges[3] = 5;	// not used if !sideIsCurved
	faceCurvedPolyEdges[0] = 0;
	faceCurvedPolyEdges[1] = (sideIsCurved ? 1 : 2);
	faceCurvedPolyEdges[2] = 2;	// not used if !sideIsCurved
	faceCurvedPolyEdges[3] = 3;	// not used if !sideIsCurved
	curvedBound.SetCurvedFace(3,4,faceVerts,numFaceCurvedEdges,faceCurvedEdges,faceCurvedPolyEdges,ringCurvature);

	curvedBound.ComputeNeighbors("");
	curvedBound.CalculateGeomExtents();
	curvedBound.SetMaterial(0);

	phArchetype& archetype = *(rage_new phArchetypePhys());
	curvedBound.SetCGOffset(Vec3V(0.0f,0.5f*height,0.0f));
	archetype.SetBound(&curvedBound);
	ARCHMGR.RegisterNewArchetype(&archetype,"bullet_cylinder");
	curvedBound.Release(); // transfer ownership of the archetype

	Matrix34 pose(CreateRotatedMatrix(position,rotation));
	curvedObject.InitPhys(&archetype,pose);
	AddActiveObject(&curvedObject);
	return &curvedObject;
}
#endif // USE_GEOMETRY_CURVED

phDemoObject* phDemoWorld::ConstructTerrainPlane (bool triangulated, u32 includeFlags, bool zUp, const Vector3& position)
{

#if POLY_MAX_VERTICES==3
	triangulated = true;
#endif

	// This manually creates a terrain bound, instead of loading one from assets, so that it can work for
	// artists without requiring local Rage assets.
	phDemoObject& terrain = *RequestObject();
	phBoundGeometry& bound = *(rage_new phBoundGeometry());
	const int numVerts = 8;
	const int numMats = 1;
	const int numPolys = (triangulated ? 4 : 2);
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bound.Init(numVerts, 0, numMats, 0, numPolys, 0);
#else
	bound.Init(numVerts, 0, numMats, 0, numPolys);
#endif
#else
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bound.Init(numVerts, 0, numMats, 0, numPolys);
#else
	bound.Init(numVerts, 0, numMats, numPolys);
#endif
#endif
	const float halfWitdh = 50.0f;
	const float thickness = 10.0f;
	if (zUp)
	{
#if COMPRESSED_VERTEX_METHOD > 0
		Vector3 bbMin(-halfWitdh, -halfWitdh, -thickness);
		Vector3 bbMax(halfWitdh, halfWitdh, 0.0f);
		bbMin.Subtract(VEC3V_TO_VECTOR3(bound.GetMarginV()));
		bbMax.Add(VEC3V_TO_VECTOR3(bound.GetMarginV()));
		bound.InitQuantization(RCC_VEC3V(bbMin), RCC_VEC3V(bbMax));
#endif
		// top
		bound.SetVertex(0, Vec3V( halfWitdh,  halfWitdh, 0.0f));
		bound.SetVertex(1, Vec3V(-halfWitdh,  halfWitdh, 0.0f));
		bound.SetVertex(2, Vec3V( halfWitdh, -halfWitdh, 0.0f));
		bound.SetVertex(3, Vec3V(-halfWitdh, -halfWitdh, 0.0f));
		// bottom
		bound.SetVertex(4, Vec3V( halfWitdh,  halfWitdh, -thickness));
		bound.SetVertex(5, Vec3V(-halfWitdh,  halfWitdh, -thickness));
		bound.SetVertex(6, Vec3V( halfWitdh, -halfWitdh, -thickness));
		bound.SetVertex(7, Vec3V(-halfWitdh, -halfWitdh, -thickness));
	}
	else
	{
#if COMPRESSED_VERTEX_METHOD > 0
		Vector3 bbMin(-halfWitdh, -thickness, -halfWitdh);
		Vector3 bbMax(halfWitdh, 0.0f, halfWitdh);
		bbMin.Subtract(VEC3V_TO_VECTOR3(bound.GetMarginV()));
		bbMax.Add(VEC3V_TO_VECTOR3(bound.GetMarginV()));
		bound.InitQuantization(RCC_VEC3V(bbMin), RCC_VEC3V(bbMax));
#endif
		// top
		bound.SetVertex(0, Vec3V( halfWitdh, 0.0f, -halfWitdh));
		bound.SetVertex(1, Vec3V(-halfWitdh, 0.0f, -halfWitdh));
		bound.SetVertex(2, Vec3V( halfWitdh, 0.0f,  halfWitdh));
		bound.SetVertex(3, Vec3V(-halfWitdh, 0.0f,  halfWitdh));
		// bottom
		bound.SetVertex(4, Vec3V( halfWitdh, -thickness, -halfWitdh));
		bound.SetVertex(5, Vec3V(-halfWitdh, -thickness, -halfWitdh));
		bound.SetVertex(6, Vec3V( halfWitdh, -thickness,  halfWitdh));
		bound.SetVertex(7, Vec3V(-halfWitdh, -thickness,  halfWitdh));
	}

	if (triangulated)
	{
		// top
		phPolygon poly;
		poly.InitTriangle(3, 2, 0, bound.GetVertex(3), bound.GetVertex(2), bound.GetVertex(0));
		bound.SetPolygonMaterialIndex(0,0);
		bound.SetPolygon(0, poly);
		poly.InitTriangle(0, 1, 3, bound.GetVertex(0), bound.GetVertex(1), bound.GetVertex(3));
		bound.SetPolygonMaterialIndex(1,0);
		bound.SetPolygon(1, poly);
		//bottom
		poly.InitTriangle(7, 4, 6, bound.GetVertex(7), bound.GetVertex(4), bound.GetVertex(6));
		bound.SetPolygonMaterialIndex(2,0);
		bound.SetPolygon(2, poly);
		poly.InitTriangle(4, 7, 5, bound.GetVertex(4), bound.GetVertex(7), bound.GetVertex(5));
		bound.SetPolygonMaterialIndex(3,0);
		bound.SetPolygon(3, poly);
	}
#if POLY_MAX_VERTICES==4
	else
	{
		// top
		phPolygon poly;
		poly.InitQuad(3, 2, 0, 1, bound.GetVertex(3), bound.GetVertex(2), bound.GetVertex(0), bound.GetVertex(1));
		bound.SetPolygonMaterialIndex(0,0);
		bound.SetPolygon(0, poly);
		// bottom
		poly.InitQuad(7, 5, 4, 6, bound.GetVertex(7), bound.GetVertex(5), bound.GetVertex(4), bound.GetVertex(6));
		bound.SetPolygonMaterialIndex(1,0);
		bound.SetPolygon(1, poly);
	}
#endif

	bound.ComputeNeighbors("");
	bound.CalculateGeomExtents();

	phMaterialMgr::Id material = MATERIALMGR.FindMaterialId("terrain");

	if (phBound::MessagesEnabled() && material == phMaterialMgr::MATERIAL_NOT_FOUND)
	{
		Warningf("Terrain plane uses material terrain that was not in the materials list");
		material = 0;
	}

	bound.SetMaterial(material);

	phArchetype& archetype = *(rage_new phArchetypePhys());
	archetype.SetBound(&bound);
	archetype.SetIncludeFlags(includeFlags);
	ARCHMGR.RegisterNewArchetype(&archetype, "terrain");

	bound.Release(); // Transfer ownership to the archetype

	Mat34V ident(V_IDENTITY);
	Matrix34 terrainMtx = RCC_MATRIX34(ident);
	terrainMtx.d.Set(position);
	terrain.InitPhys(&archetype,terrainMtx);
	AddFixedObject(&terrain);

	terrain.SetDrawSolidIfNoModel(false);

	return &terrain;
}


void phDemoWorld::ConstructPyramid (const Vector3& pos, int height, bool rotate, const char* archName,
									bool zGravity, bool xGravity)
{
	if ( archName == NULL )
	{
		archName = "crate";
	}

	// TODO: make this get the archetype by calling CreateObject insead of using the archetype manager
	const phArchetype* archetype = ARCHMGR.FindArchetype(archName);
	if (!archetype)
	{
		char boundFilePath[RAGE_MAX_PATH];
		strcpy(boundFilePath, "physics\\");
		strcat(boundFilePath, archName);
		strcat(boundFilePath, "\\bound");
		archetype = ARCHMGR.GetArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP);
		Assert(archetype);
	}

	const phBound* bound = archetype->GetBound();
	Assert(bound);

	const Vector3 boxMax(VEC3V_TO_VECTOR3(bound->GetBoundingBoxMax()));
	const Vector3 boxMin(VEC3V_TO_VECTOR3(bound->GetBoundingBoxMin()));
	const Vector3 boxOffset(VEC3V_TO_VECTOR3(bound->GetCentroidOffset()));
	float allowedPenetration = phSimulator::GetAllowedPenetration();
	float size = boxMax.y - boxMin.y - allowedPenetration;
	float halfSize = 0.5f*size;
	float width = boxMax.x - boxMin.x - allowedPenetration;
	Vector3 boxPosition;
	float posWide,posHigh,posDeep=0.0f;
	for (int i=0; i<height; i++)
	{
		for (int j=0; j<height-i;j++)
		{
			posWide = float(j) * width - (height - i - 1) * width * 0.5f;
			posHigh = halfSize - 0.5f*allowedPenetration + float(i) * size;
			if (zGravity)
			{
				if (rotate)
				{
					boxPosition.Set(posWide,posDeep,posHigh);
				}
				else
				{
					boxPosition.Set(posDeep,posWide,posHigh);
				}
			}
			else if (xGravity)
			{
				if (rotate)
				{
					boxPosition.Set(posHigh,posWide,posDeep);
				}
				else
				{
					boxPosition.Set(posHigh,posDeep,posWide);
				}
			}
			else
			{
				if (rotate)
				{
					boxPosition.Set(posWide,posHigh,posDeep);
				}
				else
				{
					boxPosition.Set(posDeep,posHigh,posWide);
				}
			}

			boxPosition.Add(pos);
			boxPosition.Subtract(boxOffset);

			CreateObject(archName, boxPosition);
		}
	}
}

void phDemoWorld::ConstructJenga(const Vector3& pos, int height, int levelSize, const char* archName)
{
	if ( archName == NULL )
	{
		archName = "long_crate";
	}

	// TODO: make this get the archetype by calling CreateObject insead of using the archetype manager
	char boundFilePath[RAGE_MAX_PATH];
	strcpy(boundFilePath, "physics\\");
	strcat(boundFilePath, archName);
	strcat(boundFilePath, "\\bound");
	const phArchetype* archetype = ARCHMGR.GetArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP);
	Assert(archetype);
	const phBound* bound = archetype->GetBound();
	Assert(bound);

	const Vector3 boxMax(VEC3V_TO_VECTOR3(bound->GetBoundingBoxMax()));
	const Vector3 boxMin(VEC3V_TO_VECTOR3(bound->GetBoundingBoxMin()));
	float boxHeight = boxMax.y - boxMin.y;
	float boxWidth = (boxMax.z - boxMin.z);// * 0.5f;
	// const Vector3 boxOffset(bound->GetCentroidOffset());

	Matrix34 straight;
	straight.Identity3x3();

	Matrix34 rotated;
	rotated.MakeRotateY(PI * 0.5f);

	for (int level = 0; level < height; ++level)
	{
		for (int block = 0; block < levelSize; ++block)
		{
			float displacement = (block - levelSize * 0.5f + 0.5f);

			straight.d = pos;
			straight.d.y += boxHeight * 2 * level;
			straight.d.z = pos.z + boxWidth * displacement;

			CreateObject(archName, straight);

			rotated.d = pos;
			rotated.d.y += boxHeight * (2 * level + 1);
			rotated.d.x = pos.x + boxWidth * displacement;

			CreateObject(archName, rotated);
		}
	}
}

void phDemoWorld::ConstructTower(const Vector3& position, int height, const char* archName)
{
	if ( archName == NULL )
	{
		archName = "crate";
	}

	// TODO: make this get the archetype by calling CreateObject insead of using the archetype manager
	char boundFilePath[RAGE_MAX_PATH];
	strcpy(boundFilePath, "physics\\");
	strcat(boundFilePath, archName);
	strcat(boundFilePath, "\\bound");
	const phArchetype* archetype = ARCHMGR.GetArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP);
	Assert(archetype);
	const phBound* bound = archetype->GetBound();
	Assert(bound);

	Vector3 boxPosition;
	const Vector3 boxMax(VEC3V_TO_VECTOR3(bound->GetBoundingBoxMax()));
	const Vector3 boxMin(VEC3V_TO_VECTOR3(bound->GetBoundingBoxMin()));
	float size = boxMax.y - boxMin.y;
	float startY = -boxMin.y;
	for (int index=0; index<height; index++)
	{
		boxPosition.Set(position.x, position.y+startY+float(index)*size, position.z);
		//Vector3 offsetXZ;
		//offsetXZ.x = g_DrawRand.GetRanged(-0.1f,0.1f);
		//offsetXZ.y = 0.0f;
		//offsetXZ.z = g_DrawRand.GetRanged(-0.1f,0.1f);
		//boxPosition.Add(offsetXZ);
		CreateObject(archName, boxPosition);
	}
}

void phDemoWorld::ConstructRoundTower(const Vector3& positionInput, int height, int around, const Vector3& boxExtentsInput)
{
    Vector3 position(positionInput);
    Vector3 boxExtents(boxExtentsInput);
	const Vector3 CONVEX_DISTANCE_MARGIN_V(CONVEX_DISTANCE_MARGIN, CONVEX_DISTANCE_MARGIN, CONVEX_DISTANCE_MARGIN);
    boxExtents.Add(CONVEX_DISTANCE_MARGIN_V);
    position.y += CONVEX_DISTANCE_MARGIN * 3.0f;
    float circ = boxExtents.x * around;
    float diam = circ / (2.0f * PI);
    Matrix34 boxMatrix;
    for (int h = 0; h < height; ++h)
    {
        boxMatrix.Identity();
        boxMatrix.d.z += diam;
        boxMatrix.d.y += h * boxExtents.y + boxExtents.y * 0.5f;
        if (h & 1)
        {
            boxMatrix.RotateFullY(PI / float(around));
        }

        for (int a = 0; a < around; ++a)
        {
            boxMatrix.RotateFullY(2.0f * PI / around);
            boxMatrix.d += position;
            CreateBox(boxExtentsInput, 1.0f, boxMatrix);
            boxMatrix.d -= position;
        }
    }
}

void phDemoWorld::ConstructWall(const float brickWidth, const float brickHeight, const int wallLength, const int wallHeight, const float brickOffset, const Matrix34& transform)
{
	const float brickLength = brickWidth * 2.0f;
	const float brickRowOffset = brickWidth * brickOffset;
	const Vector3 brickExtents(brickLength, brickHeight, brickWidth);
	for (int row = 0; row < wallHeight; ++row)
	{
		float y = (row + 0.5f) * brickHeight;
		for (int column = 0; column < wallLength; ++column)
		{
			float x = brickLength * (column - wallLength * 0.5f + 0.25f) + brickRowOffset * (row % 2);
			Matrix34 boxTransform;
			boxTransform.Identity3x3();
			boxTransform.MakeTranslate(x, y, 0.0f);
			boxTransform.Dot(transform);
			CreateBox(brickExtents,1.0f,boxTransform);
		}
	}
}

void phDemoWorld::ConstructWall(const float brickWidth, const float brickHeight, const int wallLength, const int wallHeight, const float brickOffset, Vector3::Param position, Vector3::Param rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return ConstructWall(brickWidth, brickHeight, wallLength, wallHeight, brickOffset, mtx);
}

void phDemoWorld::ConstructRectangularTower(const float brickWidth, const float brickHeight, const int towerLength, const int towerWidth, const int towerHeight, const Matrix34& transform)
{
	Matrix34 wallMatrix;
	wallMatrix.Identity3x3();
	wallMatrix.MakeTranslate(0.0f, 0.0f, towerWidth * brickWidth);
	wallMatrix.Dot(transform);
	ConstructWall(brickWidth, brickHeight, towerLength, towerHeight, 1.0f, wallMatrix);
	wallMatrix.Identity3x3();
	wallMatrix.MakeTranslate(0.0f, 0.0f, towerWidth * brickWidth);
	wallMatrix.RotateFullY(PI);
	wallMatrix.Dot(transform);
	ConstructWall(brickWidth, brickHeight, towerLength, towerHeight, 1.0f, wallMatrix);
	wallMatrix.Identity3x3();
	wallMatrix.MakeTranslate(0.0f, 0.0f, towerLength * brickWidth);
	wallMatrix.RotateFullY(PI * 0.5f);
	wallMatrix.Dot(transform);
	ConstructWall(brickWidth, brickHeight, towerWidth, towerHeight, 1.0f, wallMatrix);
	wallMatrix.Identity3x3();
	wallMatrix.MakeTranslate(0.0f, 0.0f, towerLength * brickWidth);
	wallMatrix.RotateFullY(PI * 1.5f);
	wallMatrix.Dot(transform);
	ConstructWall(brickWidth, brickHeight, towerWidth, towerHeight, 1.0f, wallMatrix);
}

void phDemoWorld::ConstructRectangularTower(const float brickWidth, const float brickHeight, const int towerLength, const int towerWidth, const int towerHeight, Vector3::Param position, Vector3::Param rotation)
{
	Matrix34 mtx;
	mtx.Set(CreateRotatedMatrix(position, rotation));
	return ConstructRectangularTower(brickWidth, brickHeight, towerLength, towerWidth, towerHeight, mtx);
}

void phDemoWorld::ConstructTable (const Vector3& position, const Vector3& rotation)
{
	Matrix34 tableMtx(CreateRotatedMatrix(position,rotation));
	float legHorzOffset = 0.6f;
	float legVertOffset = 0.4f;
	Matrix34 objectMtx(tableMtx);
	Vector3 objectPos(-legHorzOffset,legVertOffset,-legHorzOffset);
	tableMtx.Transform(objectPos,objectMtx.d);
	CreateObject("table_leg",objectMtx,false,false,false);
	objectPos.Set(legHorzOffset,legVertOffset,-legHorzOffset);
	tableMtx.Transform(objectPos,objectMtx.d);
	CreateObject("table_leg",objectMtx,false,false,false);
	objectPos.Set(-legHorzOffset,legVertOffset,legHorzOffset);
	tableMtx.Transform(objectPos,objectMtx.d);
	CreateObject("table_leg",objectMtx,false,false,false);
	objectPos.Set(legHorzOffset,legVertOffset,legHorzOffset);
	tableMtx.Transform(objectPos,objectMtx.d);
	CreateObject("table_leg",objectMtx,false,false,false);
	objectPos.Set(0.0f,2.0f*legVertOffset+0.05f,0.0f);
	tableMtx.Transform(objectPos,objectMtx.d);
	CreateObject("table_top",objectMtx,false,false,false);
}


static float GetBezierLocation(const float p0, const float p1, const float p2, const float p3, const float t)
{
	// de Casteljau algorithm
	float A, B, C;

	A = p0 + (p1 - p0) * t;
	B = p1 + (p2 - p1) * t;
	C = A + (B - A) * t;

	return C + (B + (p2 + (p3 - p2) * t - B) * t - C) * t;
}


void phDemoWorld::ConstructDominoBezier(const Vector3 bezier[4], float delta)
{
	float t;
	Vector3 loc, nextloc, normal;
	float mag;

	loc = bezier[0];
	for (t = delta; t <= 1; t += delta)
	{
		nextloc.x = GetBezierLocation(bezier[0].x, bezier[1].x, bezier[2].x, bezier[3].x, t);
		nextloc.y = GetBezierLocation(bezier[0].y, bezier[1].y, bezier[2].y, bezier[3].y, t);
		nextloc.z = GetBezierLocation(bezier[0].z, bezier[1].z, bezier[2].z, bezier[3].z, t);

		normal.Subtract(nextloc, loc);
		mag = normal.Mag();
		normal.Scale(1.0f/mag);
		if (mag >= 0.2f)
		{
			Matrix34 dominoMtx;
			dominoMtx.Identity();
			dominoMtx.RotateY(asinf(normal.x));
			dominoMtx.d.Set(Vector3(loc.x, loc.y+0.2f, loc.z));

			CreateObject("domino", dominoMtx);

			loc = nextloc;
		}
	}
}


void phDemoWorld::ConstructDominoRow(const Vector3& pos, int num, float space)
{
	for (int i=0; i<num;i++)
	{
		CreateObject("domino", Vector3(pos.x+float(i)*space, pos.y+0.2f, pos.z));
	}
}


void phDemoWorld::ConstructPegRamp(const Vector3& pos)
{
	CreateObject("block", Vector3(pos.x-0.6f, pos.y+0.25f, pos.z));
	CreateObject("plank", Vector3(pos.x, pos.y+0.53f, pos.z));
	CreateObject("peg", Vector3(pos.x+0.85f, pos.y+0.25f, pos.z));
}


void phDemoWorld::ConstructTiers(const Vector3& pos, int height)
{
	for (int i=0;i<height;i++)
	{
		float offset=float(i)*0.6f;
		CreateObject("peg", Vector3(pos.x-0.24f, pos.y+0.25f+offset, pos.z-0.24f));
		CreateObject("peg", Vector3(pos.x+0.24f, pos.y+0.25f+offset, pos.z-0.24f));
		CreateObject("peg", Vector3(pos.x-0.24f, pos.y+0.25f+offset, pos.z+0.24f));
		CreateObject("peg", Vector3(pos.x+0.24f, pos.y+0.25f+offset, pos.z+0.24f));
		CreateObject("tier", Vector3(pos.x, pos.y+0.55f+offset, pos.z));
	}
}


void phDemoWorld::ConstructBoxOfObjects(const char* file, int num, const Vector3& boxMin, const Vector3& boxMax)
{
	int i;
	Vector3 position;
	for (i=0; i<num; i++)
	{
		float x, y, z;
		x = g_ReplayRand.GetRanged(boxMin.x, boxMax.x);
		y = g_ReplayRand.GetRanged(boxMin.y, boxMax.y);
		z = g_ReplayRand.GetRanged(boxMin.z, boxMax.z);
		position.Set(x, y, z);
		CreateObject(file, position);
	}
}


void phDemoWorld::ConstructBoxOfStackedObjects(const char* file, int num, const Vector3& boxMin, const Vector3& boxMax)
{
	int i;
	Vector3 position;
	for (i=0; i<num/2; i++)
	{
		float x, y, z;
		x = g_ReplayRand.GetRanged(boxMin.x, boxMax.x);
		y = g_ReplayRand.GetRanged(boxMin.y, boxMax.y);
		z = g_ReplayRand.GetRanged(boxMin.z, boxMax.z);
		position.Set(x, y, z);
		CreateObject(file, position);
		position.Set(x, y+5.0f, z);
		CreateObject(file, position);
	}
}


/*
void phDemoWorld::ConstructTrain()
{
	Assert(m_NumUpdateObjects<m_MaxUpdateObjects);
	int numTrainCars=Min(m_MaxObjects-m_NumObjects, 7);
	Assert(numTrainCars>0);
	phDemoObject* trainCar=rage_new phDemoObject[numTrainCars];
	for (int i=0;i<numTrainCars;i++)
	{
		m_Objects[m_NumObjects+i]=&trainCar[i];
	}
	m_NumObjects+=numTrainCars;
	phTrainTester* train=rage_new phTrainTester();
	train->Init(&m_Objects[m_NumObjects], numTrainCars);
	m_UpdateObjects[m_NumUpdateObjects]=train;
	m_NumUpdateObjects++;
}
*/


void phDemoWorld::ConstructBowlingPins(int rows, const Vector3& headPos)
{
	Vector3 pinPos;
	float zSpacing = 0.5f;
	float xSpacing = 0.5f;

	pinPos.y = headPos.y;

	for (int i=0; i<rows; i++)
	{
		pinPos.z = headPos.z + zSpacing * i;
		for (int j=0; j<i + 1; j++)
		{
			pinPos.x = headPos.x + xSpacing * (-(i - 1) / 2.0f + j);
			CreateObject("bowlingpin_01", pinPos, true);
		}
	}
}


void phDemoWorld::ConstructBoxStairs (const char* name, int numStairs, const Vector3& position, const Vector3& rotation,
										float height, float depth, float radius)
{
	// Set the staircase to spiral if the given radius is in a reasonable range.
	bool spiral = ((radius>0.4f && radius<100.0f) || (radius<-0.4f && radius>-100.0f));
	float angle = (spiral ? depth/radius : 0.0f);
	radius = fabsf(radius);

	// Create the stairs.
	Matrix34 stairMatrix(CreateRotatedMatrix(position,rotation));
	for (int stairIndex=0; stairIndex<numStairs; stairIndex++)
	{
		// Create the stair object.
		CreateFixedObject(name,stairMatrix);

		// Increase the height for the next stair.
		stairMatrix.d.AddScaled(stairMatrix.b,height);
		if (spiral)
		{
			// Rotate the next stair in the spiral.
			stairMatrix.RotateUnitAxis(stairMatrix.b,angle);
		}

		// Move the position back for the next stair.
		stairMatrix.d.SubtractScaled(stairMatrix.c,depth);
	}
}


void phDemoWorld::ConstructRailing (const Vector3& position, const Vector3& rotation, bool sloped, const char* banisterName,
									const char* poleName, float height, float length, float poleSpacing)
{
	// Make the banister.
	Mat34V ident(V_IDENTITY);
	Matrix34 railingMtx = RCC_MATRIX34(ident);
	railingMtx.d.y += height;
	if (sloped)
	{
		// Rotate the banister to 45 degrees from horizontal.
		railingMtx.RotateLocalX(-0.25f*PI);
		float sqrtHalfLength = sqrtf(0.5f*length);
		railingMtx.d.y += sqrtHalfLength;
		railingMtx.d.z -= sqrtHalfLength;
	}
	else
	{
		// Rotate the banister 90 degrees to make it horizontal.
		railingMtx.RotateLocalX(-0.5f*PI);
	}
	Matrix34 staircaseMtx(CreateRotatedMatrix(position,rotation));
	Matrix34 worldMtx(railingMtx);
	worldMtx.Dot(staircaseMtx);
	CreateFixedObject(banisterName,worldMtx);

	// Make the support poles.
	railingMtx.Identity();
	railingMtx.d.y += 0.5f*height;
	if (!sloped)
	{
		railingMtx.d.z -= 0.5f*length;
	}
	float offset = 0.0f;
	while (offset<=length)
	{
		worldMtx.Dot(railingMtx,staircaseMtx);
		CreateFixedObject(poleName,worldMtx);
		if (sloped)
		{
			railingMtx.d.y += poleSpacing*SQRT2DIV2;
			railingMtx.d.z -= poleSpacing*SQRT2DIV2;
		}
		else
		{
			railingMtx.d.z += poleSpacing;
		}
		offset += poleSpacing;
	}
}


void phDemoWorld::SetUpAxis (int upAxis, float gravity)
{
	m_UpAxis = upAxis;
	m_Gravity.Zero();
	m_Gravity[m_UpAxis] = gravity;
}


int phDemoWorld::GetUpAxis () const
{
	return m_UpAxis;
}


//PURPOSE : callback into external systems to attempt to take control of an instance.. Used by the network system mostly..
void phDemoWorld::TakeControl(phInst* inst)
{
    if (inst==NULL || !m_TakeControlClbk)
    {   
        return;
    }

    m_TakeControlClbk(inst);
}

void phDemoWorld::TakeControlAll()
{
    int i;
    for (i=0; i<m_NumObjects; i++)
    {
        if (m_Objects[i])
        {
            TakeControl( m_Objects[i]->GetPhysInst() );
        }
    }
}
//PURPOSE : callback into external systems to record the application of a force to an object
bool phDemoWorld::RecordApplyForce (int levelIndex, const Vector3& force, const Vector3& position, int component)
{ 
    if (!m_ApplyForceClbk)
    {   
        return false;
    }

    m_ApplyForceClbk(levelIndex,force,position,component);

    return true;
}

//PURPOSE : callback into external systems to record the application of an impulse to an object
bool phDemoWorld::RecordApplyImpulse (int levelIndex, const Vector3& impulse, const Vector3& position, int component)
{ 
    if (!m_ApplyImpulseClbk)
    {   
        return false;
    }

    m_ApplyImpulseClbk(levelIndex,impulse,position,component);

    return true;
}


#if __BANK

void phDemoWorld::ResetFromBankCallback(phDemoWorld* that)
{
	that->Reset();
}

void phDemoWorld::ObjectBankCreation(phDemoWorld* that)
{

	if (that->m_ObjectBank)
	{
		int widgetCount = that->m_ObjectBank->GetNumWidgets();
		while (widgetCount--)
		{
		}
	}

	that->m_CurrentObjectBank = Clamp(that->m_CurrentObjectBank, 0, that->m_NumObjects - 1);
	if(that->m_CurrentObjectBank>=0 && that->m_Objects[that->m_CurrentObjectBank])
	{
		that->m_Objects[that->m_CurrentObjectBank]->AddWidgets(*that->m_ObjectBank);
	}
}

void phDemoWorld::UpdateObjectBankCreation(phDemoWorld* that)
{
	that->m_CurrentUpdateObjectBank = Clamp(that->m_CurrentUpdateObjectBank, 0, that->m_NumUpdateObjects - 1);
	if (that->m_UpdateObjects[that->m_CurrentUpdateObjectBank])
	{
		that->m_UpdateObjects[that->m_CurrentUpdateObjectBank]->AddWidgets(*that->m_UpdateObjectBank);
	}
}

void phDemoWorld::BankUpdateFrameRate()
{
    phDemoWorld *world = phDemoWorld::GetActiveDemo();
    if ( world != NULL )
    {
        world->SetFramerate( sm_activeWorldFrameRate );
    }
}

void phDemoWorld::BankUpdateTimeWarp()
{
    phDemoWorld *world = phDemoWorld::GetActiveDemo();
    if ( world != NULL )
    {
        world->SetTimeWarp( sm_TimeWarp );
    }
}

void phDemoWorld::AddWidgets(bkBank& b)
{
	m_ObjectBank = &BANKMGR.CreateBank("Object");
	m_UpdateObjectBank = &BANKMGR.CreateBank("Update Object");

	b.AddButton("Reset", datCallback(CFA1(phDemoWorld::ResetFromBankCallback), this));
	b.AddSlider("Current Object", &m_CurrentObjectBank, 0, m_MaxObjects, 1, datCallback(CFA1(phDemoWorld::ObjectBankCreation), this));
	b.AddSlider("Current Update Object", &m_CurrentUpdateObjectBank, 0, m_MaxUpdateObjects, 1, datCallback(CFA1(phDemoWorld::UpdateObjectBankCreation), this));
}

void phDemoWorld::AddClassWidgets(bkBank& bank)
{    
    bank.AddToggle( "Clamp Frame Rate", &sm_FixedFrame, datCallback(CFA(phDemoWorld::BankUpdateTimeWarp)) );
    bank.AddSlider( "Clamped Frames Per Second", &sm_activeWorldFrameRate, 0.0, 60.0f, 1.0f, datCallback(CFA(phDemoWorld::BankUpdateFrameRate)) );
    bank.AddSlider( "Max Frame Time When Clamped (seconds)", &sm_MaximumFrameTime, 1.0f / 60.0f, 1.0f, 0.01f, datCallback(CFA(phDemoWorld::BankUpdateTimeWarp)) );
    bank.AddSlider( "Time Warp When Unclamped (1.0f is normal)", &sm_TimeWarp, 0.001f, 100.0f, 0.01f, datCallback(CFA(phDemoWorld::BankUpdateTimeWarp)) );

	bank.AddToggle( "Redraw while paused", &sm_RedrawWhilePaused );
	bank.AddToggle( "Redraw next frame while paused", &sm_RedrawNextFrameWhilePaused );

	bank.AddToggle( "Draw labels", &sm_DrawLabels );

	bank.AddToggle( "Very simple ragdolls", &sm_VerySimpleRagdolls );
	bank.AddToggle( "Articulated ragdolls", &sm_ArticulatedRagdolls );
	bank.AddToggle( "Add rotation limits to constraint ragdolls", &sm_ConstraintRotationLimitRagdolls );

	bank.AddSlider("AddAndDeleteObjects", &sm_AddAndDeleteObjects, 0, 1000, 1);
}
#endif

} // namespace ragesamples
