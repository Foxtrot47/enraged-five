//
// sample_physics/sample_physics.h
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#ifndef SAMPLE_PHYSICS_SAMPLE_PHYSICS_H
#define SAMPLE_PHYSICS_SAMPLE_PHYSICS_H

#include "demoworld.h"

#include "atl/atfunctor.h"
#include "sample_rmcore/sample_rmcore.h"

namespace ragesamples {

using namespace rage;

// 
// PURPOSE:
//	This is a base class for physics samples, adding physical world creation methods to rmcSampleManager.

class physicsSampleManager : public rmcSampleManager
{
public:
	physicsSampleManager();

	virtual grcSetup& AllocateSetup();
	virtual void InitClient();
	virtual void InitCamera();
	virtual void InitLights();
	virtual void ShutdownClient();
	virtual void UpdateClient();
	virtual void Reset();
	virtual void DrawClient();
	virtual void DrawHelpClient();
    virtual bool IsPaused();
    
	void SetExtDrawClient (atFunctor0<void>& draw);

	// PURPOSE: Create and activate a physics demo world
	// NOTES:
	//	A physics demo world is a manager to control and put objects into a physics simulator and a physics level.
	phDemoWorld& MakeNewDemoWorld (const char* worldName, const char* terrainName=NULL, const Vector3& terrainPosition=ORIGIN, const int knMaxOctreeNodes=1000,
									int knMaxActiveObjects = 500, int maxObjects = 500, const Vector3& worldMin=Vector3(-999.0f,-999.0f,-999.0f),
									const Vector3& worldMax=Vector3(999.0f,999.0f,999.0f), int maxInstBehaviors=16, int scratchpadSize = 5*1024*1024, int maxManifolds = 1024,
									int maxExternVelManifolds = 256);

	phDemoWorld& MakeNewDemoWorld (const char* worldName, const char* terrainName, const Matrix34& terrainMatrix, const int knMaxOctreeNodes=1000,
									int knMaxActiveObjects = 500, int maxObjects = 500, const Vector3& worldMin=Vector3(-999.0f,-999.0f,-999.0f),
									const Vector3& worldMax=Vector3(999.0f,999.0f,999.0f), int maxInstBehaviors=16, int scratchpadSize = 5*1024*1024, int maxManifolds = 1024,
									int maxExternVelManifolds = 256);

	phDemoWorld* CreateNewDemoWorld (const char* worldName, const char* terrainName, const Matrix34& terrainMatrix, const int knMaxOctreeNodes=1000,
									int knMaxActiveObjects = 500, int maxObjects = 500, const Vector3& worldMin=Vector3(-999.0f,-999.0f,-999.0f),
									const Vector3& worldMax=Vector3(999.0f,999.0f,999.0f), int maxInstBehaviors=16, int scratchpadSize = 5*1024*1024, int maxManifolds = 1024,
									int maxExternVelManifolds = 256);

	phDemoWorld* CreateNewDemoWorld (const char* worldName, const char* terrainName=NULL, const Vector3& terrainPosition=ORIGIN, const int knMaxOctreeNodes=1000,
									int knMaxActiveObjects = 500, int maxObjects = 500, const Vector3& worldMin=Vector3(-999.0f,-999.0f,-999.0f),
									const Vector3& worldMax=Vector3(999.0f,999.0f,999.0f), int maxInstBehaviors=16, int scratchpadSize = 5*1024*1024, int maxManifolds = 1024,
									int maxExternVelManifolds = 256);

	phDemoWorld* GetCurrentWorld ();
	phDemoMultiWorld& GetWorldContainer ();

protected:

    atFunctor0<void> m_ExtDraw;
    phDemoMultiWorld m_Demos;
	ioMapper m_Mapper;
	ioValue m_Reset;

	phMouseInput m_MouseInput;
};

} // namespace ragesamples

#endif
