//
// sample_physics/sample_callbacks.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Callbacks
// PURPOSE:
//		This sample uses the callback systems in RAGE to disable collisions in various ways.

#include "demoobject.h"
#include "sample_physics.h"

#include "physics/simulator.h"
#include "system/main.h"
#include "system/param.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class disables impacts at the first available opportunity, just after broadphase but before collision occurs.
// Disabling impacts at this point obviates the work of computing collisions for this pair.
class ShouldntFindImpactsInst : public phInst
{
    bool ShouldFindImpacts(const phInst* otherInst) const
    {
        return otherInst->GetArchetype()->GetBound()->GetType() != phBound::BOX;
    }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class disables impacts after the collision has occurred, but before it is fed to the force solver. Use
// this method in case you need the physics to compute the impacts for you but want to deal with them yourself.
class DisableInPreComputeImpactsInst : public phInst
{
    void PreComputeImpacts(phContactIterator iterator)
    {
        for ( ;!iterator.AtEnd(); ++iterator)
        {
            if (iterator.GetOtherInstance()->GetArchetype()->GetBound()->GetType() == phBound::BOX)
            {
                iterator.DisableImpact();
            }
        }
    }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class disables impacts after the force solver has generated forces. Note that if you roll the ball over
// by the boxes, it reacts strangely. This is because the floor impact was already computed assuming the box impact
// was actually going to occur
class DisableInPreApplyImpactsInst : public phInst
{
    void PreApplyImpacts(phContactIterator iterator)
    {
        for ( ;!iterator.AtEnd(); ++iterator)
        {
            if (iterator.GetOtherInstance()->GetArchetype()->GetBound()->GetType() == phBound::BOX)
            {
                iterator.DisableImpact();
            }
        }
    }
};

////////////////////////////////////////////////////////////////
// 

class contactsSampleManager : public physicsSampleManager
{
public:
	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

        phDemoWorld& cbWorld = MakeNewDemoWorld("Callback world");

        cbWorld.CreateBox(Vector3(1.0f, 2.0f, 1.0f), 1.0f, Vector3(-3.0f, 0.5f, 0.0f));
        cbWorld.CreateBox(Vector3(1.0f, 2.0f, 1.0f), 1.0f, Vector3( 3.0f, 0.5f, 0.0f));
        cbWorld.CreateBox(Vector3(7.0f, 1.0f, 1.0f), 1.0f, Vector3( 0.0f, 2.5f, 0.0f));

        cbWorld.CreateObject("sphere_056", Vector3(-1.2f, 4.5f, 0.0f), false, ORIGIN, false, true, MakeFunctorRet(CreateShouldntFindImpactsInst));
        cbWorld.CreateObject("sphere_056", Vector3( 0.0f, 4.5f, 0.0f), false, ORIGIN, false, true, MakeFunctorRet(CreateDisableInPreComputeImpactsInst));
        cbWorld.CreateObject("sphere_056", Vector3( 1.2f, 4.5f, 0.0f), false, ORIGIN, false, true, MakeFunctorRet(CreateDisableInPreApplyImpactsInst));

		int startingDemo = 0;
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
	}

    static phInst* CreateShouldntFindImpactsInst()
    {
        return rage_new ShouldntFindImpactsInst;
    }

    static phInst* CreateDisableInPreComputeImpactsInst()
    {
        return rage_new DisableInPreComputeImpactsInst;
    }

    static phInst* CreateDisableInPreApplyImpactsInst()
    {
        return rage_new DisableInPreApplyImpactsInst;
    }
};

} // namespace ragesamples


// main application
int Main()
{
	{
		ragesamples::contactsSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
