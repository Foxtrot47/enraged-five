#include "breakablemgr.h"
#include "demoworld.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "phbound/bound.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundcapsule.h"
#include "phbound/impactset.h"
#include "phcore/phmath.h"
#include "physics/archmgr.h"
#include "physics/inst.h"
#include "physics/simulator.h"

namespace ragesamples {

phShatterManager* phShatterManager::ShatterManager=NULL;


phShatterManager::phShatterManager()
{
	int index;
	for (index=0; index<TotalNumSingleParts; index++)
	{
		SinglePartObjects[index].SetPhysInst(&SinglePartInstances[index]);
	}

	for (index=0; index<TotalNumCompositeParts; index++)
	{
		CompositePartBounds[index].Init(phBRInst::MaxNumShatterParts);
		CompositePartBounds[index].SetNumBounds(0);
		CompositePartArchetypes[index].SetBound(&CompositePartBounds[index]);
		CompositePartInstances[index].SetArchetype(&CompositePartArchetypes[index]);
		CompositePartObjects[index].SetPhysInst(&CompositePartInstances[index]);
	}
}


void phShatterManager::CreateShatterManager ()
{
	phShatterManager* shatterManager = rage_new phShatterManager;
	phShatterManager::SetShatterManager(shatterManager);
}


phShatterableObjectType* phShatterManager::GetShatterType (const char* name)
{
	int typeIndex;
	for (typeIndex=0; typeIndex<maxNumShatterTypes; typeIndex++)
	{
		const char* typeName = ShatterTypes[typeIndex].GetName();
		if (!strcmp(typeName,name))
		{
			return &ShatterTypes[typeIndex];
		}
	}

	typeIndex = 0;
	while (strcmp(ShatterTypes[typeIndex].GetName(),"") && typeIndex<maxNumShatterTypes)
	{
		typeIndex++;
	}

	if (typeIndex<maxNumShatterTypes)
	{
		phShatterableObjectType* shatterType = &ShatterTypes[typeIndex];
		shatterType->InitPhys(name,M34_IDENTITY);
		shatterType->Init(name);
		return shatterType;
	}

	return NULL;
}


phDemoObject* phShatterManager::GetSinglePartObject (phShatterableObjectType* shatterType, int partIndex)
{
	for (int index=0; index<TotalNumSingleParts; index++)
	{
		phDemoObject& partObject = SinglePartObjects[index];
		if (!partObject.GetPhysInst() || !partObject.GetPhysInst()->IsInLevel())
		{
			partObject.GetPhysInst()->SetArchetype(shatterType->GetSinglePartArchetype(partIndex));
			phDemoWorld::GetActiveDemo()->RegisterObject(&partObject);
			return &partObject;
		}
	}

	return NULL;
}


phDemoObject* phShatterManager::GetCompositePartObject (phShatterableObjectType* shatterType, int* partIndices, int numParts)
{
	for (int index=0; index<TotalNumCompositeParts; index++)
	{
		phShatterableObject& partObject = CompositePartObjects[index];
		if (!partObject.GetPhysInst() || !partObject.GetPhysInst()->IsInLevel())
		{
			partObject.SetShatterType(shatterType);
			InitCompositePartArchetype(index,shatterType,partIndices,numParts);
			phDemoWorld::GetActiveDemo()->RegisterObject(&partObject);
			return &partObject;
		}
	}

	return NULL;
}

void phShatterManager::Reset(phSimulator *pSim)
{
	int index;
	for (index=0; index<TotalNumSingleParts; index++)
	{
		if (SinglePartInstances[index].IsInLevel())
		{
			pSim->DeleteObject(SinglePartInstances[index].GetLevelIndex());
			phDemoWorld::GetActiveDemo()->RemoveObject(&SinglePartObjects[index]);
		}
	}

	for (index=0; index<TotalNumCompositeParts; index++)
	{
		if (CompositePartInstances[index].IsInLevel())
		{
			pSim->DeleteObject(CompositePartInstances[index].GetLevelIndex());
			phDemoWorld::GetActiveDemo()->RemoveObject(&CompositePartObjects[index]);
		}
	}
}


void phShatterManager::InitCompositePartArchetype (int objectIndex, phShatterableObjectType* shatterType,
													int* partIndices, int numParts)
{
	// Get pointers to the archetype and the bound for the composite broken part, and the type's bound.
	phArchetypePhys* archetype = &CompositePartArchetypes[objectIndex];
	Assert(archetype->GetBound()->GetType()==phBound::COMPOSITE);
	phBoundComposite* bound = static_cast<phBoundComposite*>(archetype->GetBound());
	Assert(numParts<=bound->GetMaxNumBounds());
	phBoundComposite* typeBound = shatterType->GetCompositeBound();

	// Reset all the bound parts in the composite broken bound.
	int index;
	int totalNumBounds = typeBound->GetNumBounds();
	for (index=0; index<totalNumBounds; index++)
	{
		bound->RemoveBound(index);
	}

	// Copy the broken bound parts from the original into the broken part bound.
	float massList[phBRInst::MaxNumShatterParts];
	for (index=0; index<numParts; index++)
	{
		int partIndex = partIndices[index];
		bound->SetLocalMatrix(partIndex,typeBound->GetLocalMatrix(partIndex));
		bound->SetBound(partIndex,typeBound->GetBound(partIndex));
		massList[partIndex] = shatterType->GetSinglePartArchetype(partIndex)->GetMass();
	}

	// Set the number of bounds in the broken part composite bound.
	bound->SetNumBounds(typeBound->GetNumBounds());

	// Set the mass and calculate the angular inertia for the broken composite object.
	//archetype->InitCompositePhysics(0.0f,massList);
}


phShatterableObjectType::phShatterableObjectType()
{
	strcpy(Name,"");
	for (int partIndex=0; partIndex<phBRInst::MaxNumShatterParts; partIndex++)
	{
		phShatterPart& part = ShatterPart[partIndex];
		part.ParentIndex = BAD_INDEX;
		part.ImpulseLimit = -1.0f;
		part.TotalImpulse = 0.0f;
	}
}



void phShatterableObjectType::Init (const char* name)
{
	strcpy(Name,name);

	int numParts = GetCompositeBound()->GetNumBounds();

	ASSET.PushFolder(name);
	// Load the "mods" file to get the model names and the corresponding bound part indices.
	fiStream* file = ASSET.Open("model","mods",true,true);
	ASSET.PopFolder();
	Assert(file);
	fiAsciiTokenizer token;
	token.Init(name,file);
	token.MatchToken("numParts:");
	ASSERT_ONLY(int tempNumParts =) token.GetInt();
	Assert(numParts==tempNumParts);
	int partIndex,boundIndex;
	for (partIndex=0; partIndex<numParts; partIndex++)
	{
		if (token.CheckToken("index:"))
		{
			// Read in the optional index number.
			ASSERT_ONLY(int filePartIndex =) token.GetInt();
			Assert(filePartIndex==partIndex);
		}

		token.MatchToken("bound:");
		boundIndex = token.GetInt();
		token.MatchToken("name:");
		const int maxNameLength = 32;
		char partName[maxNameLength];
		token.GetToken(partName,maxNameLength);
		phShatterPart& part = ShatterPart[partIndex];

		part.InitGfx(partName);
		part.InitPhys(partName, GetCompositeBound()->GetCurrentMatrix(partIndex));

		if (token.CheckToken("parent:"))
		{
			part.ParentIndex = token.GetInt();
		}
		
		if (token.CheckToken("impulseLimit:"))
		{
			part.ImpulseLimit = token.GetFloat();
		}
	}

	file->Close();
}


phBoundComposite* phShatterableObjectType::GetCompositeBound ()
{
	phBound* bound = GetPhysInst()->GetArchetype()->GetBound();
	Assert(bound->GetType()==phBound::COMPOSITE);
	return static_cast<phBoundComposite*>(bound);
}


phShatterableObject::phShatterableObject()
{
	ShatterType = NULL;
}

void phShatterableObject::DrawModel (const Matrix34& mtx) const
{
	Matrix34 partMatrix;
	phBoundComposite* compositeBound = static_cast<phBoundComposite*>(GetPhysInst()->GetArchetype()->GetBound());
	Assert(compositeBound);
	int numParts = compositeBound->GetNumBounds();

//	grcState::Default();

	for (int partIndex=0; partIndex<numParts; partIndex++)
	{
		if (compositeBound->GetBound(partIndex))
		{
			partMatrix.Dot(compositeBound->GetCurrentMatrix(partIndex),mtx);
			ShatterType->GetModelPart(partIndex).DrawModel(partMatrix);
		}
	}

//	RSTATE.Default();
}


void phShatterableObject::SetShatterType (phShatterableObjectType* shatterType)
{
	ShatterType = shatterType;
	static_cast<phBRInst*>(GetPhysInst())->SetShatterType(ShatterType);
}

void phShatterableObject::Reset(phLevelNew *pLevelNew, phSimulator *pSim)
{
	if (ShatterType)
	{
		// Restore any broken pieces.
		phBoundComposite* compositeBound = static_cast<phBoundComposite*>(GetPhysInst()->GetArchetype()->GetBound());
		phBoundComposite* typeBound = ShatterType->GetCompositeBound();
		int numParts = compositeBound->GetNumBounds();
		for (int partIndex=0; partIndex<numParts; partIndex++)
		{
			if (!compositeBound->GetBound(partIndex))
			{
				compositeBound->SetBound(partIndex,typeBound->GetBound(partIndex));
			}
		}
	}

	if (GetPhysInst()->GetLevelIndex()==phInst::INVALID_INDEX)
	{
		PHSIM->AddInactiveObject(GetPhysInst());
	}
	phDemoObject::Reset(pLevelNew, pSim);
}

} // namespace ragesamples
