//
// sample_physics/testworlds.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "testworlds.h"

#include "demoworld.h"

#include "math/random.h"
#include "phcore/segment.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/timemgr.h"
#include "vector/vector3.h"
#include "vectormath/classes.h"

namespace ragesamples {


////////////////////////////////////////////////////////////////
// utility

void phTestWorlds::MakeSeg (phSegment & seg, const Vector3& boxMin, const Vector3& boxMax, float length)
{
	Vector3 start, end;

	//start.Set(g_ReplayRand.Range(boxMin.x,boxMax.x),g_ReplayRand.Range(boxMin.y,boxMax.y),g_ReplayRand.Range(boxMin.z,boxMax.z));
	float x = g_ReplayRand.GetRanged(boxMin.x,boxMax.x);
	float y = g_ReplayRand.GetRanged(boxMin.y,boxMax.y);
	float z = g_ReplayRand.GetRanged(boxMin.z,boxMax.z);
	start.Set(x,y,z);

	//end.Set(g_ReplayRand.Gaussian(0.0f,1.0f),g_ReplayRand.Gaussian(0.0f,1.0f),g_ReplayRand.Gaussian(0.0f,1.0f));
	x = g_ReplayRand.GetRanged(-1.0f,1.0f);
	y = g_ReplayRand.GetRanged(-1.0f,1.0f);
	z = g_ReplayRand.GetRanged(-1.0f,1.0f);
	end.Set(x,y,z);
	end.Normalize();
	end.Scale(length);
	end.Add(start);

	seg.A.Set(start);
	seg.B.Set(end);
}


void phTestWorlds::AddCratesInBox (phDemoWorld & demo, int num, const Vector3 & zoneMin, const Vector3 & zoneMax)
{
	for (int i=0; i<num; i++)
	{
		float x,y,z;
		x = g_ReplayRand.GetRanged(zoneMin.x+1,zoneMax.x-1);
		y = g_ReplayRand.GetRanged(zoneMin.y+1,zoneMax.y-1);
		z = g_ReplayRand.GetRanged(zoneMin.z+1,zoneMax.z-1);
		demo.CreateObject("crate",Vector3(x,y,z),false);
	}
}


//////////////////////////////////////////////////////////////////////////////
// a group of objects that load properly and have complete bounds

void AddLineOfTestedObjects (phDemoWorld * demo, const Vector3 & pos)
{
	char filenames[][100]=
	{
		"block",
		"block_big",
		"peg",
		"peg_box",
		"Brick1",
		"Brick2",
		"table_top",
		"table_leg",
		"table_leg_box",
		"sphere_r015",
		"sphere_r024",
		"sphere_r060",
		"sphere_r089",
		"pole_00",
		"pole_01",
		"pole_02",
		"pole_03",
		""
	};

	int i;

	Vector3 position;
	position.Set(pos);
	position.x -= 20.0f;

	for (i=0; filenames[i][0]; i++)
	{
		position.x += 2.0f;
		demo->CreateObject(filenames[i],position,true);
	}
}

//////////////////////////////////////////////////////////////////////////////

phDemoWorld * phTestWorlds::Blank ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);

	return demo;
}


phDemoWorld * phTestWorlds::Crate ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	demo->CreateObject("TrashCan_01",Vector3(-3.0f,0.0f,0.0f),true);

	return demo;
}


phDemoWorld * phTestWorlds::TypesOfObjects ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	demo->CreateObject("TrashCan_01",Vector3(-1.0f,0.0f,0.0f),true);
	demo->CreateFixedObject("TrashCan_01",Vector3(1.0f,0.0f,0.0f),true);

	return demo;
}


phDemoWorld * phTestWorlds::TestWorld1 ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	Vector3 position, rotation;
	position.Zero();
	position.y += 5.0f;
	position.z -= 1.0f;
	rotation.Set(PI*0.5f,0.0f,0.0f);

	demo->CreateObject("TrashCan_01",position,true,rotation);

	return demo;
}


phDemoWorld * phTestWorlds::BowlAndBalls ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	Vector3 position;
	position.Zero();
	position.y += 1.0f;

	demo->CreateObject("bowl_01",position,true);

	position.y += 25.0f;
	position.x += 0.7f;
	//demo->CreateObject("bowlingpin_01",position);
	position.y -= 6.0f;
	demo->CreateObject("sphere_r024",position);
	position.y += 6.0f;
	demo->CreateObject("sphere_r024",position);
	position.y += 6.0f;
	demo->CreateObject("sphere_r024",position);

	return demo;
}


phDemoWorld * phTestWorlds::PyramidsAndSpheres ()
{
	phDemoWorld * demo;

	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	Vector3 position;

	position.Set(0.0f, 0.0f, 3.0f);
	demo->ConstructPyramid(position,3,true);
//	demo->ConstructPyramid(position,1,true);

	// Add a falling sphere, for consoles only, because there's no mouse input to knock the pyramid over.
	position.Set(1.0f, 20.0f, 6.8f);
	demo->CreateObject("sphere",position,true);

//	position.Set(0.0f, 0.0f, 0.0f);
//	demo->CreateObject("sphere_r089",position,true);

	return demo;
}


phDemoWorld * phTestWorlds::DominoPark ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	return demo;
}


phDemoWorld * phTestWorlds::DominoBezier ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	/*
	Vector3 bezier1[4], bezier2[4], bezier3[4], bezier4[4];

	bezier1[0].Set(-0.7f, 0.0f, 0.0f);
	bezier1[1].Set(-3.5f, 0.0f, -1.0f);
	bezier1[2].Set(-3.5f, 0.0f, -4.50f);
	bezier1[3].Set(0.0f, 0.0f, -2.0f);

	bezier2[0].Set(-0.7f, 0.0f, -2.5f);
	bezier2[1].Set(3.0f, 0.0f, -4.0f);
	bezier2[2].Set(5.0f, 0.0f, -2.0f);
	bezier2[3].Set(1.0f, 0.0f, -1.0f);

	bezier3[0].Set(1.7f, 0.0f, -1.2f);
	bezier3[1].Set(1.2f, 0.0f, 2.8f);
	bezier3[2].Set(-0.3f, 0.0f, 3.0f);
	bezier3[3].Set(-0.7f, 0.0f, -0.8f);

	bezier4[0].Set(-1.0f, 0.0f, -0.5f);
	bezier4[1].Set(-5.0f, 0.0f, -4.0f);
	bezier4[2].Set(-9.0f, 0.0f, 4.0f);
	bezier4[3].Set(-13.0f, 0.0f, 0.0f);

	demos[0]->ConstructDominoBezier(bezier1,0.01f);
	demos[0]->ConstructDominoBezier(bezier2,0.01f);
	demos[0]->ConstructDominoBezier(bezier3,0.01f);
	*/

	return demo;
}


phDemoWorld * phTestWorlds::SphereSandbox ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;
	demo->Init(1600, 600, 600, Vec3V(-50,-0.1f,-50),Vec3V(50,99.9f,50));
	demo->ConstructTerrainPlane();

	demo->CreateFixedObject("long_wall",Vector3(0,0,-25),true,Vector3(0,PI/2.0f,0));
	demo->CreateFixedObject("long_wall",Vector3(0,0,25),true,Vector3(0,PI/2.0f,0));
	demo->CreateFixedObject("long_wall",Vector3(-25,0,0),true,Vector3(0,0,0));
	demo->CreateFixedObject("long_wall",Vector3(25,0,0),true,Vector3(0,0,0));

	demo->ConstructBoxOfObjects("sphere_r060",500,Vector3(-25,-0.1f,-25),Vector3(25,99.9f,25));

	return demo;
}


phDemoWorld * phTestWorlds::CratesAbound ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	demo->CreateObject("crate",Vector3(-0.6f,10.0f,0.0f),true);
	demo->CreateObject("crate",Vector3(-0.3f,10.0f,0.0f),true);
	demo->CreateObject("crate",Vector3(0.0f,10.0f,0.0f),true);
	demo->CreateObject("crate",Vector3(0.3f,10.0f,0.0f),true);
	demo->CreateObject("crate",Vector3(0.6f,10.0f,0.0f),true);
	demo->CreateObject("crate",Vector3(0.0f,0.0f,-0.6f),true);
	demo->CreateObject("crate",Vector3(0.0f,0.0f,-0.3f),true);
	demo->CreateObject("crate",Vector3(0.0f,0.0f,0.0f),true);
	demo->CreateObject("crate",Vector3(0.0f,0.0f,0.3f),true);
	demo->CreateObject("crate",Vector3(0.0f,0.0f,0.6f),true);

	return demo;
}


phDemoWorld * phTestWorlds::BowlingAlley ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	return demo;
}


phDemoWorld * phTestWorlds::PlankBallsAndDominos ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	return demo;
}


phDemoWorld * phTestWorlds::Ramps ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	return demo;
}


phDemoWorld * phTestWorlds::TablesGalore ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	return demo;
}


phDemoWorld * phTestWorlds::GeomHeaven ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	return demo;
}


phDemoWorld * phTestWorlds::TrainWreck ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	Vector3 position;

	float r0, r1, r;
	r0 = 8.0f;
	r1 = 1.5f;

	int i, divisions;
	divisions = 360;

	for (i=0; i<divisions; i++)
	{
		if (i%2)
		{
			r = r0 + r1 * sinf(2.0f * PI * i * 12.0f / divisions);
		}
		else
		{
			r = r0 + r1 * cosf(2.0f * PI * i * 12.0f / divisions + PI * 0.25f);
		}

		position.x = r * sinf(2.0f * PI * i / divisions);
		position.z = r * cosf(2.0f * PI * i / divisions);
		position.y = 0.0f;

		demo->CreateObject("table_leg",position,true);
	}

	position.Zero();
	position.x += 10.0f;

	divisions = 100;
	for (i=0; i<divisions; i++)
	{
		position.z = r1 * sinf(2.0f * PI * i * 3.0f / divisions);
		position.x += 30.0f / divisions;

		demo->CreateObject("table_leg",position,true);
	}

	position.Zero();
	demo->CreateObject("sphere_r060",position,true);

	position.x += 23.0f;
	demo->CreateObject("sphere_r089",position,true);

	return demo;
}


phDemoWorld * phTestWorlds::TestedObjects ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	AddLineOfTestedObjects(demo,Vector3(0.0f,0.0f,0.0f));

	return demo;
}


phDemoWorld * phTestWorlds::TestDegenerate ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	demo->CreateObject("table_top",Vector3(-3.0f,0.0f,0.0f),true);

	demo->CreateObject("table_top_degenerate",Vector3(3.0f,0.0f,0.0f),true);

	return demo;
}


phDemoWorld * phTestWorlds::TestMultipleFixedCollisions ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	demo->SetFramerate(60.0f);

	int i, j, k;
	if (1)
	{
		for (i=-5; i<5; i++)
		{
			for (j=-5; j<5; j++)
			{
				k = g_ReplayRand.GetRanged(1,2);
				switch (k)
				{
				case 0:
					demo->CreateFixedObject("sphere_r024",Vector3((float)i/2.0f,0.0f,(float)j/2.0f),true);
					break;
				case 1:
					demo->CreateFixedObject("crate",Vector3((float)i/2.0f,0.0f,(float)j/2.0f),true,Vector3(0.0f,0.0f,0.0f));
					break;
				case 2:
					demo->CreateFixedObject("table_leg",Vector3((float)i/2.0f,0.0f,(float)j/2.0f),true);
					break;
				case 3:
					demo->CreateFixedObject("table_leg_box",Vector3((float)i/2.0f,0.0f,(float)j/2.0f),true);
					break;
				case 4:
					demo->CreateFixedObject("cylinder",Vector3((float)i/2.0f,0.0f,(float)j/2.0f),true);
					break;
				}
			}
		}
	}

	demo->CreateFixedObject("table_leg",Vector3(10.0f,0.0f,0.2f));
	demo->CreateFixedObject("table_leg",Vector3(9.8f,0.0f,0.1f));
	demo->CreateFixedObject("table_leg",Vector3(10.2f,0.0f,0.1f));
	demo->CreateFixedObject("table_leg",Vector3(10.0f,0.05f,-0.2f));
	demo->CreateObject("sphere_r024",Vector3(10.0f,5.0f,0.0f));

	//demo->CreateObject("sphere_r024",Vector3(0.0f,5.0f,0.0f));
	//demo->CreateObject("crate",Vector3(0.1f,5.0f,0.4f));
	//demo->CreateObject("table_leg",Vector3(0.2f,5.0f,0.3f));
	//demo->CreateObject("sphere_r060",Vector3(0.3f,5.0f,0.2f));
	//demo->CreateObject("cylinder",Vector3(0.4f,5.0f,0.1f));
	//demo->CreateFixedObject("crate",Vector3((float)1/2.0f,0.0f,(float)0/2.0f),true,Vector3(0.0f,0.0f,0.0f));
	//demo->CreateObject("capsule_01",Vector3(0.4f,4.0f,0.1f));

	// and add a single test object to compare to
	demo->CreateFixedObject("tri_test",Vector3(-8.0f,0.0f,0.0f),true);
	demo->CreateObject("sphere_r040",Vector3(-8.0f,2.0f,0.0f),true);

	demo->CreateObject("tri_test",Vector3(-0.0f,5.0f,-0.0f),true,Vector3(0.0f,0.0f,3.14f));

	return demo;
}


phDemoWorld * phTestWorlds::TestLoadOctree ()
{
	phDemoWorld * demo;
	demo = rage_new phDemoWorld;

	demo->Init(1000);
	demo->ConstructTerrainPlane();

	demo->CreateFixedObject("stair_01",Vector3(-5.0f,0.0f,0.0f),true);
	//demo->CreateFixedObject("stair_01_ot",Vector3(5.0f,0.0f,0.0f),true);
	//demo->CreateFixedObject("stair_01",Vector3(5.0f,0.0f,0.0f),true);

	for (int i=1; i<5; i++)
	{
		//demo->CreateObject("sphere_r024",Vector3(5.0f,(float)(i*10),0.0f),true);
		//demo->CreateObject("sphere_r024",Vector3(-5.0f,(float)(i*10),0.0f),true);
		demo->CreateObject("crate",Vector3(-5.0f,(float)(i*10),0.0f),true);
		//demo->CreateObject("sphere_r024",Vector3(-5.0f,(float)(i*10),0.0f),true);
	}

	return demo;
}



phDemoWorld *phTestWorlds::TestSpacedSpheres()
{
	phDemoWorld *pDemo;
	pDemo = rage_new phDemoWorld;

	pDemo->Init(1000);
	pDemo->ConstructTerrainPlane();

	Matrix34 mtxObject;
	Vector3 vecRotation;

	const float kfFieldMinX = -24.0f;
	const float kfFieldMaxX =  24.0f;
	const float kfFieldMinY =  1.0f;
	const float kfFieldMaxY = 20.0f;
	const float kfFieldMinZ = -24.0f;
	const float kfFieldMaxZ =  24.0f;
	const float kfFieldSizeX = kfFieldMaxX - kfFieldMinX;
	const float kfFieldSizeY = kfFieldMaxY - kfFieldMinY;
	const float kfFieldSizeZ = kfFieldMaxZ - kfFieldMinZ;

	int nObjCtr;
	for(nObjCtr = 0; nObjCtr < 160; ++nObjCtr)
	{
		vecRotation.Set(0.0f, 0.0f, 0.0f);
		mtxObject.MakeRotate(vecRotation, 0.0f);
		mtxObject.d.Set(kfFieldSizeX * g_ReplayRand.GetFloat() + kfFieldMinX, kfFieldSizeY * g_ReplayRand.GetFloat() + kfFieldMinY, kfFieldSizeZ * g_ReplayRand.GetFloat() + kfFieldMinZ);
		pDemo->CreateObject("sphere_r060", mtxObject);
	}
	for(nObjCtr = 0; nObjCtr < 160; ++nObjCtr)
	{
		vecRotation.Set(0.0f, 0.0f, 0.0f);
		mtxObject.MakeRotate(vecRotation, 0.0f);
		mtxObject.d.Set(kfFieldSizeX * g_ReplayRand.GetFloat() + kfFieldMinX, kfFieldSizeY * g_ReplayRand.GetFloat() + kfFieldMinY, kfFieldSizeZ * g_ReplayRand.GetFloat() + kfFieldMinZ);
		pDemo->CreateObject("sphere_r089", mtxObject);
	}

	return pDemo;
}

} // namespace ragesamples
