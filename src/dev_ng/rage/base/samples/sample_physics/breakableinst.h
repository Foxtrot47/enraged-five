// 
// sample_physics/breakableinst.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PHTEST_BREAKABLEINST_H
#define PHTEST_BREAKABLEINST_H

#include "physics/archetype.h"
#include "physics/instbreakable.h"

namespace ragesamples {

using namespace rage;

class phShatterableObjectType;

class phBRInst : public phInstBreakable
{
public:
	phBRInst();
	~phBRInst();
	void SetResetMatrix (const Matrix34 & mtx)					{ ResetMatrix.Set(mtx); }
	float GetInactiveImpulseLimit () const;
	float GetActiveImpulseLimit () const;
	void SetActiveImpulseLimit (float limit)					{ ActiveImpulseLimit = limit; }
	void SetInactiveImpulseLimit (float limit)					{ InactiveImpulseLimit = limit; }
	bool UsingOldBreakSystem () const						{ return false; }
	//bool ShouldMakeActive (phCollider** collider, bool* alwaysActive) const;
	bool FindBreakStrength (const phImpact* impactList, float* breakStrength, const phImpactData* impactData,
							phBreakData* breakData) const;
	virtual bool FindBreakStrength (const Vector3* componentImpulses, float* breakStrength, phBreakData* breakData) const;
	int FindWeakestIndex (int partIndex) const;
	int BreakApart (phImpact* impactList, phInstBreakable** breakableInstList, int* numBreakInsts, phInst** brokenInstList,
					phImpactData* impactData, const phBreakData& breakData);
	virtual int BreakApart (phInstBreakable** breakableInstList, int* numBreakInsts, phInst** brokenInstList,
							const phBreakData& breakData);
	phInst* GetBrokenPartInstance (int partIndex);
	bool IsDescendedOrSame (int partIndex, int possibleParentIndex) const;
	void SetShatterType (phShatterableObjectType* shatterType);
	int GetNumParts () const;

	enum { MaxNumShatterParts = 12 };

protected:
	phShatterableObjectType* ShatterType;
	Matrix34 ResetMatrix;
	float ActiveImpulseLimit;
	float InactiveImpulseLimit;
	bool BrokenFree;
	bool BrokeOffPart;
};

} // namespace ragesamples

#endif
