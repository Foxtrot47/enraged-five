//
// sample_physics/sample_contacts.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Contacts
// PURPOSE:
//		This sample creates stacks of objects of various types for testing the computation of contact forces.

#include "archmgr.h"
#include "demoobject.h"
#include "sample_physics.h"

#include "bank/bkmgr.h"
#include "devcam/mayacam.h"
#include "math/random.h"
#include "phbound/boundbox.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundgeom.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/collider.h"
#include "physics/colliderdispatch.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vectormath/mat34v.h"


#if __WIN32
#pragma warning (disable : 4189) // local variable is initialized but not referenced
#pragma warning (disable : 4101) // unreferenced local variable
#endif


PARAM(file, "Reserved for future expansion, not currently hooked up");


namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Active);
EXT_PFD_DECLARE_ITEM(Inactive);
EXT_PFD_DECLARE_ITEM(Fixed);

}

namespace ragesamples {

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// const data
//
const float ConveyerSpeed        = 6.0f;
const float ActObjExtVelBigBoxHt = 1.0f;

const Vector3 ConveyorCameraPos(5.0f, 40.0f, 50.0f);
const Vector3 ConveyorCameraLookTo(5.0f, 30.0f, 0.0f);
const Vector3 DefaultCameraPos(0.0f, 5.0f, 10.0f);

static const ScalarV BoxHeightThrehold = ScalarVFromF32(ActObjExtVelBigBoxHt * 0.99f * 0.5f);


//////////////////////////////////////////////////////////////////////////
// globals
//
mthRandom				g_conveyerRandom;
physicsSampleManager*	g_sampleMgr			= NULL;


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Declared as static and passed to phSaveCamWorld below.  Saves 
//			Camera pos and look at for each demo page so that when the page
//			is returned to the camera is in the same position.
//
class CameraPosSaver
{
public:
				CameraPosSaver(const Vector3& cameraPos, const Vector3& lookAtPos);

	void		Init();
	void		Save();

private:

	Vector3		m_CameraPos;
	Vector3		m_LookAtPos;
};


//////////////////////////////////////////////////////////////////////////
CameraPosSaver::CameraPosSaver(const Vector3& cameraPos, const Vector3& lookAtPos)
	: m_CameraPos(cameraPos)
	, m_LookAtPos(lookAtPos)
{
}


//////////////////////////////////////////////////////////////////////////
void CameraPosSaver::Init()
{
	g_sampleMgr->GetCamMgr().Init(m_CameraPos, m_LookAtPos, dcamCamMgr::CAM_MAYA);
}

//////////////////////////////////////////////////////////////////////////
void CameraPosSaver::Save()
{
	// Save the camera position and look at.
	dcamCamMgr& camMgr          = g_sampleMgr->GetCamMgr();
	dcamCamMgr::eCamera camType = camMgr.GetCurrentCamera();

	if(camType == dcamCamMgr::CAM_MAYA)
	{
		dcamMayaCam*    mayaCam = static_cast<dcamMayaCam*>(&camMgr.GetCamera(dcamCamMgr::CAM_MAYA));
		const Matrix34& camPos  = mayaCam->GetWorldMtx();
		m_CameraPos = camPos.d;
		m_LookAtPos = camPos.d;
		m_LookAtPos.AddScaled(camPos.c, -mayaCam->GetFocusDistance());
	}
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: To hook dtor call from phDemoWorld in order to save camera position
//			so that is can be restored the next time the demo is run.
//
class phSaveCamWorld : public phDemoWorld
{
public:

	phSaveCamWorld(const char* name) 
		: phDemoWorld(name)
		, m_CamSaver(0) 
	{
	}

	~phSaveCamWorld()	
	{ 
		if(m_CamSaver) 
		{
			m_CamSaver->Save(); 
		}
	}

	void InitCamera(CameraPosSaver* saver) 
	{ 
		m_CamSaver = saver; 

		if(m_CamSaver) 
		{
			m_CamSaver->Init(); 
		}
	}

private:
	CameraPosSaver*		m_CamSaver;
};


//////////////////////////////////////////////////////////////////////////
//	PURPOSE: Represents a bucket on the bucket conveyor, animates bucket 
//			opening and closing, and overrides phInst::GetExternallyControlledLocalVelocity
//
class phConveyerBucket : public phInst
{
public:
				phConveyerBucket();
				~phConveyerBucket();

	void		Init();
	void		InitBucketPosition(Matrix34& pos)			{ SetMatrix(RC_MAT34V(pos)); }

	void		UpdateBucket(float timeDelta);
	void		SetBucketPosition(Matrix34& pos)			{ m_HingePos         = pos; }
	void		SetVelocity(const Vector3& v)				{ m_HingeVelocity    = v; }
	void		SetAngVelocity(const Vector3& v)			{ m_HingeAngVelocity = v; }
	void		OpenBucket()								{ m_State            = OPENING; }
	void		CloseBucket()								{ m_State            = CLOSING; }

	Vec3V_Out	GetExternallyControlledLocalVelocity(Vec::V3Param128 position, int component) const;

private:

	Matrix34			m_HingePos;
	Matrix34			m_RotatedPos;
	Vector3				m_HingeVelocity;
	Vector3				m_HingeAngVelocity;
	Vector3				m_AngVelDueToOpenClose;
	phBoundComposite	m_CompBound;
	phArchetypePhys		m_Arch;
	float				m_BucketAngX;

	enum State { OPEN, OPENING, CLOSED, CLOSING };
	State				m_State;
};


//////////////////////////////////////////////////////////////////////////
phConveyerBucket::phConveyerBucket():
	m_HingePos  (Matrix34::IdentityType),
	m_RotatedPos(Matrix34::IdentityType),
	m_BucketAngX(0.0f),
	m_State	    (CLOSED)
{
	SetInstFlag(FLAG_QUERY_EXTERN_VEL | FLAG_NEVER_ACTIVATE, true);
}


//////////////////////////////////////////////////////////////////////////
phConveyerBucket::~phConveyerBucket()
{
	SetArchetype(NULL,false);
	m_CompBound.Release(false);
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: builds a bucket
//
void phConveyerBucket::Init()
{
	phBoundBox* boxBot   = rage_new phBoundBox(Vector3(4.0f, 0.4f, 4.0f));
	phBoundBox* boxSideR = rage_new phBoundBox(Vector3(0.1f, 4.0f, 4.0f));
	phBoundBox* boxSideL = rage_new phBoundBox(Vector3(0.1f, 4.0f, 4.0f));
	phBoundBox* boxSideF = rage_new phBoundBox(Vector3(4.0f, 4.0f, 0.1f)); 
	phBoundBox* boxSideB = rage_new phBoundBox(Vector3(4.0f, 4.0f, 0.1f)); 

	// Create composite bound
	m_CompBound.Init(5,true);
	m_CompBound.SetBound(0,boxBot);
	m_CompBound.SetBound(1,boxSideR);
	m_CompBound.SetBound(2,boxSideL);
	m_CompBound.SetBound(3,boxSideF);
	m_CompBound.SetBound(4,boxSideB);

	boxBot->Release();
	boxSideR->Release();
	boxSideL->Release();
	boxSideF->Release();
	boxSideB->Release();

	Matrix34 mtx;
	mtx.Identity();
	mtx.d.Set( 0.0f, 0.2f, -2.0f);
	m_CompBound.SetCurrentMatrix(0,RCC_MAT34V(mtx));
	mtx.d.Set( 1.95f, 2.0f, -2.0f);
	m_CompBound.SetCurrentMatrix(1,RCC_MAT34V(mtx));
	mtx.d.Set(-1.95f, 2.0f, -2.0f);
	m_CompBound.SetCurrentMatrix(2,RCC_MAT34V(mtx));
	mtx.d.Set(0.0f, 2.0f, -0.05f);
	m_CompBound.SetCurrentMatrix(3,RCC_MAT34V(mtx));
	mtx.d.Set(0.0f, 2.0f, -3.95f);
	m_CompBound.SetCurrentMatrix(4,RCC_MAT34V(mtx));
	m_CompBound.UpdateLastMatricesFromCurrent();
	m_CompBound.CalcCenterOfGravity();
	m_CompBound.CalculateCompositeExtents();

	// Create archetype
	m_Arch.SetBound(&m_CompBound);

	// Init phInst
	mtx.d.Set(0.0f, 3.0f, 3.0f);
	phInst::Init(m_Arch, mtx);
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: animate bucket position when "opening" so it dumps contents, 
//			and when it is "closing", or returning to upright position.
//
void phConveyerBucket::UpdateBucket(float timeDelta)
{
	const float rotRate = 1.8f;
	m_AngVelDueToOpenClose.Zero();

	if(m_State == OPENING)
	{
		Approach(m_BucketAngX, 8*PI/9, rotRate, timeDelta);
		m_AngVelDueToOpenClose.AddScaled(m_HingePos.a, rotRate);

		if(m_BucketAngX == PI/4)
		{
			m_State = OPEN;
		}
	}
	else if(m_State == CLOSING)
	{
		Approach(m_BucketAngX, 0.0f, rotRate, timeDelta);
		m_AngVelDueToOpenClose.AddScaled(m_HingePos.a, -rotRate);

		if(m_BucketAngX == 0.0f)
		{
			m_State = CLOSED;
		}
	}

	Matrix34 oldPos(m_RotatedPos);
	// Build matrix with rotated position.
	m_RotatedPos.Identity();
	m_RotatedPos.RotateFullX(m_BucketAngX);

	// Here we add the rotation due to open/closing with 
	// the position that was set by the belt.
	m_RotatedPos.Dot(m_HingePos);

	SetMatrix(RC_MAT34V(m_RotatedPos)); 
	PHLEVEL->UpdateObjectLocation(GetLevelIndex(), &oldPos);
}


//////////////////////////////////////////////////////////////////////////
Vec3V_Out phConveyerBucket::GetExternallyControlledLocalVelocity(Vec::V3Param128 position, int) const
{
	Vector3 angVelocity(m_HingeAngVelocity + m_AngVelDueToOpenClose);
	Vector3 velocity = m_HingeVelocity;

	if(angVelocity.IsNonZero())
	{
		Vector3 relPos(position);
		relPos.Subtract(m_HingePos.d);
		velocity.AddCrossed(angVelocity,relPos);
	}

	return RCC_VEC3V(velocity);
}


//////////////////////////////////////////////////////////////////////////
//	PURPOSE: Animates buckets as they move around the bucket belt, computes 
//           bucket positions, and allows the belt to be moved, resized, 
//			 speed changed, open/close position changed, and number of buckets changed.
//
class phBucketBelt
{
public:
						~phBucketBelt()					{ UnInit(); }
	
	// Conveyer is centered on the center of one pulley, with the 
	// other pulley distBetweenPulleys meters up the y axis, and both pulley 
	// axes aligned with the x axis of conveyor space, or m_Pos.
	void				Init(int numBuckets, float pulleyRadius, float distBetweenPulleys, 
								const Matrix34& conveyerPos);
	void				UnInit();

	void				Update(float timeDelta);
	void				SetSpeed(float s)				{ m_Speed       = s; }
	void				SetOpenPhase(float p)			{ m_OpenPhase   = p; }
	void				SetClosePhase(float p)			{ m_ClosePhase  = p; }
	void				Reset()							{ m_ElapsedTime = 0.0f; }
	int					GetNumBuckets() const			{ return m_Buckets.GetCount(); }
	phConveyerBucket*	GetBucket(int idx)				{ return m_Buckets[idx]; }

private:

	void				CalcBucketPosition(int hookIdx, Matrix34& pos, float* angSpeed, 
											float* phase);

	Matrix34					m_Pos;
	float						m_Speed;
	float						m_ElapsedTime;
	float						m_DistBetwnPulleys;
	float						m_Radius;
	float						m_OpenPhase;
	float						m_ClosePhase;
	atArray<phConveyerBucket*>	m_Buckets;
};


//////////////////////////////////////////////////////////////////////////
void phBucketBelt::Init(int numBuckets, float pulleyRadius, float distBetweenPulleys, 
						const Matrix34& conveyerPos)
{
	m_Pos              = conveyerPos;
	m_Speed            = 1.0f;
	m_ElapsedTime      = 0.0f;
	m_DistBetwnPulleys = distBetweenPulleys;
	m_Radius           = pulleyRadius;
	m_OpenPhase        = 0.0f;
	m_ClosePhase       = 0.5f;

	m_Buckets.Resize(numBuckets);

	for(int i = 0; i < numBuckets; i++)
	{
		m_Buckets[i] = rage_new phConveyerBucket;
		m_Buckets[i]->Init();
		Matrix34 pos;
		float    phase;
		float    angSpeed;
		CalcBucketPosition(i, pos, &angSpeed, &phase);
		m_Buckets[i]->InitBucketPosition(pos);
		m_Buckets[i]->SetVelocity(pos.b * m_Speed);
		m_Buckets[i]->SetAngVelocity(pos.a * angSpeed);
	}
}


//////////////////////////////////////////////////////////////////////////
void phBucketBelt::UnInit()
{
	int size = m_Buckets.GetCount();

	for(int i = 0; i < size; i++)
	{
		phConveyerBucket* bucket = m_Buckets.Pop();
		if(bucket->IsInLevel())
		{
			PHSIM->DeleteObject(bucket->GetLevelIndex());
		}
		delete bucket;
	}
}


//////////////////////////////////////////////////////////////////////////
static bool IsTriggerCrossed(float trigger, float phase, float lastPhase)
{
	return (phase > lastPhase && phase > trigger && trigger >= lastPhase) ||
		   (phase < lastPhase && (phase > trigger || trigger >= lastPhase));
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Iterate over all the buckets on the belt and calc there: position, 
//			velocity, angular velocity, and determine if they should open or close.
//
void phBucketBelt::Update(float timeDelta)
{
	m_ElapsedTime   += timeDelta;
	float beltLenght = 2*PI*m_Radius + 2*m_DistBetwnPulleys;
	float phaseDelta = m_Speed * timeDelta / beltLenght;

	for(int i = 0; i < m_Buckets.GetCount(); i++)
	{
		Matrix34 pos;
		float    phase;
		float    angSpeed;
		CalcBucketPosition(i, pos, &angSpeed, &phase);
		m_Buckets[i]->SetBucketPosition(pos);
		m_Buckets[i]->SetVelocity(pos.b * m_Speed);
		m_Buckets[i]->SetAngVelocity(pos.a * angSpeed);

		float lastPhase = phase > phaseDelta ? phase - phaseDelta : phase - phaseDelta + 1.0f;

		if(IsTriggerCrossed(m_OpenPhase, phase, lastPhase))
		{
			m_Buckets[i]->OpenBucket();
		}

		if(IsTriggerCrossed(m_ClosePhase, phase, lastPhase))
		{
			m_Buckets[i]->CloseBucket();
		}

		m_Buckets[i]->UpdateBucket(timeDelta);
	}
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: calculate the position of a bucket on the bucket belt.
//
void phBucketBelt::CalcBucketPosition(int hookIdx, Matrix34& pos, float* angSpeed, float* phase)
{
	int   numHooks = m_Buckets.GetCount();
	Assert(hookIdx < numHooks);
	// Calc distance from the base position along the belt. base 
	// position is the bottom of straight section on the way up.
	float pulleyHalfCircumference = PI*m_Radius;
	float beltLength              = 2*pulleyHalfCircumference + 2*m_DistBetwnPulleys;
	float offsetFromBase          = hookIdx * beltLength / numHooks;
	float distanceTraveled        = m_ElapsedTime * m_Speed;
	float distanceFromBase        = distanceTraveled + offsetFromBase;
	distanceFromBase              = fmodf(distanceFromBase, beltLength);
	*phase                        = distanceFromBase / beltLength;
	*angSpeed                     = 0.0f;

	// Calc pos in conveyer space
	pos = M34_IDENTITY;

	// distanceFromBase is the distance along the belt from the point where the
	// belt leaves the base pulley on the way to pulley #2.  In other 
	// words distanceFromBase is measured from a point m_Radius meters in the z 
	// direction relative to m_Pos.
	//
	// Bucket is on straight section from base pulley to pulley #2.
	if(distanceFromBase < m_DistBetwnPulleys)
	{
		pos.d.AddScaled(pos.c,m_Radius);
		pos.d.AddScaled(pos.b,distanceFromBase);
	}
	// Bucket is on circular arc of pulley #2.
	else if(distanceFromBase < m_DistBetwnPulleys + pulleyHalfCircumference)
	{
		float arcLen = distanceFromBase - m_DistBetwnPulleys;
		float angle  = arcLen / m_Radius;
		pos.d.y = m_DistBetwnPulleys;
		pos.RotateLocalX(-angle);
		pos.d.AddScaled(pos.c,m_Radius);
		*angSpeed = m_Speed / m_Radius;
	}
	// Bucket is on straight section between other pulley and the base pulley.
	else if(distanceFromBase < 2*m_DistBetwnPulleys + pulleyHalfCircumference)
	{
		pos.RotateLocalX(-PI);
		float distFromTop = distanceFromBase - (m_DistBetwnPulleys + pulleyHalfCircumference);
		pos.d.AddScaled(pos.c,m_Radius);
		pos.d.AddScaled(-pos.b,m_DistBetwnPulleys - distFromTop);
	}
	else // Bucket is on circular arc of base pulley.
	{
		pos.RotateLocalX(-PI);
		float arcLen = distanceFromBase - (2*m_DistBetwnPulleys + pulleyHalfCircumference);
		float angle  = arcLen / m_Radius;
		pos.RotateLocalX(-angle);
		pos.d.AddScaled(pos.c,m_Radius);
		*angSpeed = m_Speed / m_Radius;
	}

	// Transform position from conveyer space to world space.
	pos.Dot(m_Pos);
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Provides derived class to override phInst::GetExternallyControlledVelocity()
//			for conveyor belt in demo.
//
struct phConveyorInst : public phInst
{
	phConveyorInst()			
	{ 
		SetInstFlag(FLAG_QUERY_EXTERN_VEL | FLAG_NEVER_ACTIVATE, true); 
	}

	Vec3V_Out GetExternallyControlledVelocity() const
	{
		Vector3 velocity;
		velocity.Set(ConveyerSpeed, 0.0f, 0.0f);
		return RCC_VEC3V(velocity);
	}
};


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Allows client to set a callback functor which will be called 
//			when this object's PreComputeImpacts fumction is called.
//
class phCollisionDelegateInst : public phInst
{
public:

	void	SetMaxFunctors(int maxFunctors)			{ m_FunctorObjs.Reserve(maxFunctors); }

	void	PreComputeImpacts(phContactIterator UNUSED_PARAM(impacts));

	// If the called function does not want to be called again at the 
	// next collision it should return false otherwise return true.
	typedef atDelegate<bool (phContactIterator UNUSED_PARAM(impacts))> Functor;

	void	AddFunctor(Functor func);

private:
	atArray<Functor>	m_FunctorObjs;
};


//////////////////////////////////////////////////////////////////////////
void phCollisionDelegateInst::PreComputeImpacts(phContactIterator impacts)
{
	int i = 0;

	while(i < m_FunctorObjs.GetCount())
	{
		if(m_FunctorObjs[i](impacts))
		{
			++i;
		}
		else
		{
			m_FunctorObjs.Delete(i);
		}
	} 
}


//////////////////////////////////////////////////////////////////////////
void phCollisionDelegateInst::AddFunctor(Functor func) 
{ 
	if(m_FunctorObjs.GetCount() < m_FunctorObjs.GetCapacity())
	{
		m_FunctorObjs.Push(func);
	}
	else
	{
		Errorf("Functor Add being dropped due to array too small = %d", 
			m_FunctorObjs.GetCapacity());
	}
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Provides initialization of conveyor belt, and bucket belt, as 
//			well as common code to add objects to the conveyor belt in an 
//			evenly distributed way.
//
class phConveyorWorld : public phSaveCamWorld
{
public:
					phConveyorWorld(const char* name);

	void			Update();
	virtual void	Shutdown();

protected:

	virtual int		AddObjects(float UNUSED_PARAM(timeSinceLastAdd)) { return 0; }

private:

	void			InitConveyer();
	bool			CollisionCallback(phContactIterator UNUSED_PARAM(impacts));
	void			InstOutOfBoundsCallback(phInst* inst);

	phBucketBelt	m_BucketBelt;
	float			m_TimeSinceLastAdd;
	float			m_Timer;
	int				m_NumObjs;

	enum State { ADDING_BEFORE_IMPACT, ADDING_AFTER_IMPACT, DONE_ADDING };
	State			m_AddingState;
};


//////////////////////////////////////////////////////////////////////////
phConveyorWorld::phConveyorWorld(const char* name)
	: phSaveCamWorld    (name)
	, m_Timer           (0.0f)
	, m_NumObjs         (0)
	, m_TimeSinceLastAdd(0.0f)
	, m_AddingState     (ADDING_BEFORE_IMPACT)
{
	InitConveyer();
}


//////////////////////////////////////////////////////////////////////////
void phConveyorWorld::InitConveyer()
{
	phDemoWorld::Init(1000, 2048, 2048, 
		Vec3V(-999.0f,-999.0f,-999.0f), 
		Vec3V(999.0f,999.0f,999.0f), 
		16, 5*1024*1024, 16384, 1024);

	InitUpdateObjects(256);

	phInst*       conveyorInst = rage_new phConveyorInst;
	phDemoObject* obj          = RequestObject();
	obj->SetInst(conveyorInst);

	const char* fileName = "conveyor";
	Matrix34 posMtx(Matrix34::IdentityType);
	posMtx.d.Set(-8.0f, 17.0f, 0.0f);
	obj->InitPhys(fileName, posMtx);
	obj->InitGfx(fileName);
	AddFixedObject(obj);

	Matrix34 mtx(Matrix34::IdentityType);
	mtx.RotateLocalY(-PI/2);
	mtx.d.Set(25.0f, 5.0f, 0.0f);
	const int   numBuckets = 11;
	const float radius     = 5.0f;
	const float height     = 45.0f;
	m_BucketBelt.Init(numBuckets, radius, height, mtx);
	m_BucketBelt.SetSpeed(3.5f);
	m_BucketBelt.SetOpenPhase(0.27f);
	m_BucketBelt.SetClosePhase(0.37f);

	mtx = M34_IDENTITY;
	mtx.RotateLocalZ(PI/9);
	mtx.d.Set(-4.43f, 28.89f, 0.0f);
	CreateFixedObject("slide_banked", mtx);

	mtx = M34_IDENTITY;
	mtx.d.Set(17.0f, 17.0f, 0.0f);
	CreateFixedObject("conveyor_chute", mtx);

	phCollisionDelegateInst* endCapInst = rage_new phCollisionDelegateInst;
	endCapInst->SetMaxFunctors(1);
	phCollisionDelegateInst::Functor collFunc(this, &phConveyorWorld::CollisionCallback);
	endCapInst->AddFunctor(collFunc);

	obj = RequestObject();
	obj->SetInst(endCapInst);

	fileName = "conveyor_end_cap";
	posMtx.Identity();
	posMtx.d.Set(-33.0f, 17.0f, 0.0f);
	obj->InitPhys(fileName, posMtx);
	obj->InitGfx(fileName);
	AddFixedObject(obj);

	// Add the buckets to the simulation.
	for(int i = 0; i < m_BucketBelt.GetNumBuckets(); i++)
	{
		GetSimulator()->AddInactiveObject(m_BucketBelt.GetBucket(i));
		GetPhLevel()->SetInactiveCollidesAgainstInactive(m_BucketBelt.GetBucket(i)->GetLevelIndex(), true);
	}

	phLevelNew::InstOutOfWorldCallback outFunc(this, &phConveyorWorld::InstOutOfBoundsCallback);
	PHLEVEL->SetNotifyOutOfWorldCallback(outFunc);
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Adds objects to the world until a timer expires which is started 
//			when the conveyor end cap is hit.  This is done to evenly distribute 
//			objects throughout the conveyor system regardless of conveyor speed 
//			or other factors...  could probably be simpler. 
//
void phConveyorWorld::Update()
{
	if(ShouldUpdate())
	{
		if(m_AddingState != DONE_ADDING)
		{
			m_TimeSinceLastAdd += TIME.GetSeconds();

			int numObjsAdded = AddObjects(m_TimeSinceLastAdd);
			m_NumObjs += numObjsAdded;

			if(numObjsAdded > 0)
			{
				m_TimeSinceLastAdd = 0.0f;
			}

			if(m_AddingState == ADDING_AFTER_IMPACT)
			{
				m_Timer += TIME.GetSeconds();

				if(m_Timer > 2.0f)
				{
					m_AddingState = DONE_ADDING;
					Printf("Num objs = %d\n", m_NumObjs);
				}
			}
		}

		m_BucketBelt.Update(TIME.GetSeconds());
	}

	phDemoWorld::Update();
}


void phConveyorWorld::Shutdown()
{
	m_BucketBelt.UnInit();
}


//////////////////////////////////////////////////////////////////////////
bool phConveyorWorld::CollisionCallback(phContactIterator UNUSED_PARAM(impacts))
{
	m_AddingState = ADDING_AFTER_IMPACT;
	return false;
}


//////////////////////////////////////////////////////////////////////////
void phConveyorWorld::InstOutOfBoundsCallback(phInst* inst)
{
	Matrix34 newMtx(Matrix34::IdentityType);
	newMtx.d.Set(g_conveyerRandom.GetRanged(-25.0f, 17.0f),
		g_conveyerRandom.GetRanged( 18.0f, 19.0f), 
		g_conveyerRandom.GetRanged( -3.0f,  3.0f));

	GetSimulator()->TeleportObject(*inst, newMtx);

	if(phCollider* collider = GetSimulator()->GetCollider(inst))
	{
		collider->SetVelocity(Vector3(0.0f, 0.0f, 0.0f));
	}
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates world and manages addition of objects to the world.
//
struct BoxConveyerWorld : public phConveyorWorld
{
						BoxConveyerWorld(const char* name) : phConveyorWorld(name) {}

	static phDemoWorld*	Create();

	int					AddObjects(float timeSinceLastAdd);
};


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates page 1: Box Conveyor
//
phDemoWorld* BoxConveyerWorld::Create()
{
	BoxConveyerWorld* world = rage_new BoxConveyerWorld("Box Conveyor");
	
	static CameraPosSaver saver(ConveyorCameraPos, ConveyorCameraLookTo);
	world->InitCamera(&saver);

	return world;
}


//////////////////////////////////////////////////////////////////////////
int	BoxConveyerWorld::AddObjects(float timeSinceLastAdd)
{
	int         numBoxesAdded = 0;
	const float incX          = 1.0f;

	if(timeSinceLastAdd > incX / ConveyerSpeed)
	{
		const float startPosZ = -2.25f;
		const float maxPosZ   =  2.251f;
		const float incZ      =  1.5f;

		for(float posZ = startPosZ; posZ < maxPosZ; posZ += incZ)
		{
			numBoxesAdded++;
			const Vector3 boxSize(0.5f, 0.5f, 0.5f);
			const float   posX = -32.0f;
			const float   posY =  17.3f;
			phDemoObject* obj  = CreateBox(boxSize, 1.0f, Vector3(posX, posY, posZ));
			phInst*       inst = obj->GetPhysInst();

			if(phCollider* collider = GetSimulator()->GetCollider(inst))
			{
				collider->SetVelocity(Vector3(ConveyerSpeed, 0.0f, 0.0f));
			}
		}
	}

	return numBoxesAdded;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates world and manages addition of objects to the world.
//
struct RagdollConveyerWorld : public phConveyorWorld
{
						RagdollConveyerWorld(const char* name) : phConveyorWorld(name) {}

	static phDemoWorld*	Create();

	int					AddObjects(float timeSinceLastAdd);
};


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates page 2: Ragdoll Conveyor
//
phDemoWorld* RagdollConveyerWorld::Create()
{
	RagdollConveyerWorld* world = rage_new RagdollConveyerWorld("Ragdoll Conveyor");
	
	static CameraPosSaver saver(ConveyorCameraPos, ConveyorCameraLookTo);
	world->InitCamera(&saver);

	return world;
}


//////////////////////////////////////////////////////////////////////////
int	RagdollConveyerWorld::AddObjects(float timeSinceLastAdd)
{
	int         numRagdollsAdded = 0;
	const float incX             = 4.0f;

	if(timeSinceLastAdd > incX / ConveyerSpeed)
	{
		const float startPosZ =  -0.1f;
		const float maxPosZ   =   0.1f;
		const float incZ      =   2.0f;

		for(float posZ = startPosZ; posZ < maxPosZ; posZ += incZ)
		{
			numRagdollsAdded++;
			const float posX  = -30.0f;
			const float posY  =  18.0f;
			Vector3     pos(posX, posY, posZ);
			Vector3     rot(0.0f, PI/2, 0.0f);
			phDemoObject* obj = phArticulatedObject::CreateHuman(*this, pos, rot);
			phInst*       inst = obj->GetPhysInst();

			if(phCollider* collider = GetSimulator()->GetCollider(inst))
			{
				collider->SetVelocity(Vector3(ConveyerSpeed, 0.0f, 0.0f));
			}
		}
	}

	return numRagdollsAdded;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Overrides GetExternallyControlledLocalVelocity for Box with 
//			circular externally controlled velocity on one face and none 
//			the the others.
//
struct ActiveObjWithExternVel : public phInst
{
		 ActiveObjWithExternVel() { SetInstFlag(FLAG_QUERY_EXTERN_VEL, true); }

	Vec3V_Out GetExternallyControlledLocalVelocity(Vec::V3Param128 position, int) const
	{
		Mat34V   m        = GetMatrix();
		Vec3V    wldPos   = m.GetCol3();
		Vec3V    dir      = Subtract(Vec3V(position), wldPos);
		Vec3V    up       = m.GetCol1();
		Vec3V    vel      = Cross(up, dir);
		ScalarV  locHt    = Dot(up, dir);

		BoolV selector    = IsGreaterThan(locHt, BoxHeightThrehold);
		Vec3V    newVel   = SelectFT(selector, Vec3V(V_ZERO), vel);
		ScalarV scale(V_THIRD);
		newVel           *= Vec3V(scale);
		return newVel;
	}
};


//////////////////////////////////////////////////////////////////////////
static phSaveCamWorld* CreateWorldWithActiveBoxWithExternVel(const char* worldName)
{
	phSaveCamWorld* world = rage_new phSaveCamWorld(worldName);

	world->Init(1000, 2048, 2048, 
		Vec3V(-999.0f,-999.0f,-999.0f), 
		Vec3V(999.0f,999.0f,999.0f), 
		16, 5*1024*1024, 16384, 1024);

	world->InitUpdateObjects(256);

	phDemoObject* obj = world->RequestObject();

	Vector3 boxExtents(4.0f, ActObjExtVelBigBoxHt, 4.0f);
	Matrix34 mtx(Matrix34::IdentityType);
	mtx.d.y = 1.0f;

	phInst* inst = rage_new ActiveObjWithExternVel;
	obj->SetInst(inst);
	obj->InitBoxPhys(boxExtents, 1.0f, mtx);

	world->CreateFixedObject("plane_complex", M34_IDENTITY);
	world->AddActiveObject(obj, false);

	return world;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates page 3: Box on Box with Extern Vel
//
phDemoWorld* CreateWorldBoxActiveExternVelBox()
{
	phSaveCamWorld* world = CreateWorldWithActiveBoxWithExternVel("Box on Box with Extern Vel");
	world->CreateBox(Vector3(0.5f, 0.5f, 0.5f), 1.0f, Vector3(0.5f, 3.0f, 0.5f));

	static CameraPosSaver saver(DefaultCameraPos, Vector3::ZeroType);
	world->InitCamera(&saver);

	return world;;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates page 4: Ragdoll on Box with Extern Vel
//
phDemoWorld* CreateWorldBoxActiveExternVelRagdoll()
{
	phSaveCamWorld* world = CreateWorldWithActiveBoxWithExternVel("Ragdoll on Box with Extern Vel");
	phArticulatedObject::CreateHuman(*world, Vector3(0.5f, 3.0f, 0.5f), Vector3::ZeroType);

	static CameraPosSaver saver(DefaultCameraPos, Vector3::ZeroType);
	world->InitCamera(&saver);

	return world;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Overrides phInst::GetExternallyControlledLocalVelocity in order
//			to create an active object with a velocity that is rotating.
//
struct phInstWithRotExternVel : public phInst
{
	phInstWithRotExternVel() 
		: m_Ang( 0.0f )
	{ 
		SetInstFlag(phInst::FLAG_QUERY_EXTERN_VEL, true); 
	}

	void UpdateVelocity() 
	{ 
		m_Ang += 0.01f;	
	}

	Vec3V_Out GetExternallyControlledLocalVelocity(Vec::V3Param128 , int) const
	{
		Vector3 velocity;
		velocity.Set(cosf(m_Ang), 0.0f, -sinf(m_Ang));
		velocity.Scale(2.0f);
		return RCC_VEC3V(velocity);
	}

private:
	float	m_Ang;
};


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Provides Update function for object which would have no other 
//			means of getting an update call per frame.
// NOTES:	If more than one object needs an update an array of functors 
//			should be added, and Update should loop over them.
//
class phUpdateWorld : public phSaveCamWorld
{
public:
			phUpdateWorld(const char* name) : phSaveCamWorld(name) {}

	typedef atDelegate<void ()> Functor;

	void	SetUpdateFunctor(Functor func)			{ m_Func = func; }

	void	Update() 
	{ 
		m_Func();
		phDemoWorld::Update(); 
	}

private:
	Functor		m_Func;
};


//////////////////////////////////////////////////////////////////////////
static phUpdateWorld* CreateExternVelRagdollWorld(const char* worldName)
{
	phUpdateWorld* world = rage_new phUpdateWorld(worldName);

	world->Init(1000, 2048, 2048, 
		Vec3V(-999.0f,-999.0f,-999.0f), 
		Vec3V(999.0f,999.0f,999.0f), 
		16, 5*1024*1024, 16384, 1024);

	world->InitUpdateObjects(256);

	phInstWithRotExternVel* inst = rage_new phInstWithRotExternVel;
	phUpdateWorld::Functor  func(inst, &phInstWithRotExternVel::UpdateVelocity);
	world->SetUpdateFunctor(func);

	Vector3 position(-2.0, 5.0, -2.0);
	phArticulatedObject* obj = rage_new phArticulatedObject;
	obj->SetInst(inst);
	obj->InitHuman(*world, position, VEC3_ZERO, ORIGIN, DEFAULT_HUMAN_PARTS);

	world->CreateFixedObject("plane_complex", M34_IDENTITY);

	return world;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates page 5: Extern Vel Ragdoll with Boxes
//
static phDemoWorld* CreateExternVelRagdollWithBoxesWorld()
{
	phUpdateWorld* world = CreateExternVelRagdollWorld("Extern Vel Ragdoll with Boxes");

	static CameraPosSaver saver(DefaultCameraPos, Vector3::ZeroType);
	world->InitCamera(&saver);

	for(float z = -8.0f; z < 8.0; z += 1.0f)
		for(float x = -8.0f; x < 8.0f; x += 1.0f)
			world->CreateBox(Vector3(0.5f, 0.5f, 0.5f), 0.05f, Vector3(x, 2.0f, z));

	return world;
}


//////////////////////////////////////////////////////////////////////////
// PURPOSE: Creates page 6: Extern Vel Ragdoll with Ragdolls
//
static phDemoWorld* CreateExternVelRagdollWithRagdollsWorld()
{
	phUpdateWorld* world = CreateExternVelRagdollWorld("Extern Vel Ragdoll with Ragdolls");

	static CameraPosSaver saver(DefaultCameraPos, Vector3::ZeroType);
	world->InitCamera(&saver);

	for(float z = -4.0f; z < 4.1; z += 2.0f)
	{
		for(float x = -4.0f; x < 4.1f; x += 2.0f)
		{
			phArticulatedObject* obj = phArticulatedObject::CreateHuman(*world, Vector3(x, 2.0f, z),VEC3_ZERO);
			phArticulatedBody& body  = obj->GetBody();

			for(int i = 0; i < body.GetNumBodyParts(); ++i)
			{
				//phArticulatedBodyPart& bodyPart = body.GetLink(i);
				Vec3V   angIntVec3 =  body.GetAngInertia(i);
				Vector3 angInt     = RC_VECTOR3(angIntVec3);
				body.SetMassAndAngInertia(i, body.GetMass(i).Getf() * 0.1f,angInt * 0.25f);
			}
		}
	}

	return world;
}


//////////////////////////////////////////////////////////////////////////
//
class ConveyerSampleManager : public physicsSampleManager
{
public:

	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

		// Disable breaking to save a couple of percent speed
		phContactMgr::SetBreakingEnabled(false);

		g_sampleMgr = this;

		m_Demos.AddDemo(phDemoMultiWorld::Factory(BoxConveyerWorld::Create));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(RagdollConveyerWorld::Create));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(CreateWorldBoxActiveExternVelBox));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(CreateWorldBoxActiveExternVelRagdoll));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(CreateExternVelRagdollWithBoxesWorld));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(CreateExternVelRagdollWithRagdollsWorld));

		m_Demos.SetCurrentDemo(0);
	}

	// InitCamera needs to be empty in order for the camera to be inited properly for the 
	// first page of this sample since InitCamera() gets called after the first page demo
	// is created and thereby overriding the camera state that the first demo just set.
	virtual void InitCamera() {}

	virtual void Update()     { physicsSampleManager::Update();	}
};

} // namespace ragesamples


//////////////////////////////////////////////////////////////////////////
// main application
int Main()
{
	ragesamples::ConveyerSampleManager samplePhysics;
	samplePhysics.Init();
	samplePhysics.UpdateLoop();
	samplePhysics.Shutdown();

	return 0;
}

