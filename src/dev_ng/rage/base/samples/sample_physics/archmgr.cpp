//
// physics/archmgr.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "archmgr.h"

#include "bank/bank.h"

#if __BANK
#include "bank/bkmgr.h"
#endif

#include "atl/hashsimple.h"
#include "file/token.h"
#include "paging/rscbuilder.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundgeom.h"
#include "phbound/boundsphere.h"
#include "phcore/phmath.h"
#include "string/string.h"
#include "system/memory.h"

using namespace rage;


phArchetypeMgr *phArchetypeMgr::Instance = NULL;

#if __BANK
bool phArchetypeMgr::WidgetsOpen = false;
#endif


phArchetypeMgr &phArchetypeMgr::CreateInstance (u16 maxArchetypes)
{
	Assert(Instance == NULL);

	Instance = rage_new phArchetypeMgr(maxArchetypes);	

	return *Instance;
}


void phArchetypeMgr::DeleteInstance ()
{
	if (Instance)
	{
		delete Instance;
	}
	
	Instance=0;
}


phArchetypeMgr::phArchetypeMgr (u16 maxArchetypes)
{
	ArchetypeHashInfo = NULL;

	MaxNumArchetypes = maxArchetypes;
	ArchetypeHashTable = rage_new atMap<const char*, HashInfo*>();

	m_Nonexistent.AddRef();

	Initialize(maxArchetypes);
}


phArchetypeMgr::~phArchetypeMgr ()
{
	Shutdown();
	m_Nonexistent.Release(false);
}


void phArchetypeMgr::Initialize (u16 maxArchetypes)
{
	int i;

	Shutdown();

	MaxNumArchetypes = maxArchetypes;

	ArchetypeHashTable->Create(MaxNumArchetypes, true);

	sysMemStartTemp();
	ArchetypeHashInfo=rage_new HashInfo[MaxNumArchetypes];
	sysMemEndTemp();
	for(i=0;i<MaxNumArchetypes;i++)
	{
		ArchetypeHashInfo[i].Name=NULL;
		ArchetypeHashInfo[i].Archetype=NULL;
	}
}


void phArchetypeMgr::Shutdown ()
{
	if (ArchetypeHashInfo)
	{
		int hashIndex;
		for (hashIndex=0; hashIndex<MaxNumArchetypes; hashIndex++)
		{
			phArchetype * archetype = ArchetypeHashInfo[hashIndex].Archetype;
			if (archetype)
			{
				// Reduce this phArchetype's reference count to zero and delete it.
				archetype->Release(true);
			}
		}

		delete ArchetypeHashTable;

		for (hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
		{
			sysMemStartTemp();
			delete[] ArchetypeHashInfo[hashIndex].Name;
			sysMemEndTemp();
			ArchetypeHashInfo[hashIndex].Name = NULL;
			ArchetypeHashInfo[hashIndex].Archetype = NULL;
		}
		sysMemStartTemp();
		delete[] ArchetypeHashInfo;
		sysMemEndTemp();
		ArchetypeHashInfo = NULL;
	}

	if (Instance==this)
	{
		Instance=NULL;
	}
}


/*
PURPOSE
    Reset the hash table information, remove from the hash table, release and delete all physics
	archetypes that do not have any external references.  Also, if deleteNonexistent is true 
    removed entries that were added during load time from missing files.
*/
void phArchetypeMgr::RecycleUnused (bool deleteNonexistent)
{
	for (int hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
	{
		phArchetype * archetype = ArchetypeHashInfo[hashIndex].Archetype;
		if (archetype)
		{
			Assert(archetype->GetRefCount()>0);
			if((deleteNonexistent && archetype == &m_Nonexistent) || archetype->GetRefCount()==1)
			{
				// Found an archetype with no external references, so remove it from the hash table,
				// and delete it and its hash info.
				ArchetypeHashTable->Delete(ArchetypeHashInfo[hashIndex].Name);
				sysMemStartTemp();
				delete[] ArchetypeHashInfo[hashIndex].Name;
				sysMemEndTemp();
				ArchetypeHashInfo[hashIndex].Name=NULL;
				ArchetypeHashInfo[hashIndex].Archetype=NULL;
				archetype->Release();
			}
		}
	}
}


/*
PURPOSE
    Get a physics archetype from the hash table, by archetype name.
PARAMS
	name	- The name of the archetype to retrieve from the hash table.
RETURN
	A pointer to the physics archetype with the given name. */
phArchetype* phArchetypeMgr::FindArchetype (const char* name)
{
	// Sure are a lot of consts here, huh?  It's a const pointer to a const pointer to a const HashInfo.
	const HashInfo* const* const hashInfo=ArchetypeHashTable->Access(name);
	if(hashInfo)
	{
		Assert(*hashInfo);
		return (*hashInfo)->Archetype;
	}
	return NULL;
}


/*
PURPOSE
    Get a physics archetype by name, loading from a file if a physics archetype with the given name
	is already in the hash table.
PARAMS
	fileName		- The name of the archetype to load or retrieve from the hash table.
	initPhysFields	- Optional boolean to tell whether or not to calculate physical properties for the archetype
					  after and if it is loaded.  Default value is true.
	token			- Optional pointer to the tokenizer of the file from which to load the archetype.  Default is NULL,
					  in which case a file named fileName will be loaded if an archetype with that name is not already
					  in the hash table.
	classType		- Optional class type of physics archetype to create (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS.
RETURN
	A pointer to the physics archetype with the given name, either obtained from the hash table or loaded. */
phArchetype * phArchetypeMgr::GetArchetype (const char * fileName, bool initPhysFields, fiTokenizer * token, int classType)
{
	phArchetype* archetype = FindArchetype(fileName);

	if (archetype==NULL)
	{
		// Load the bound file
		phBound* bound;
		if (token)
		{
			bound = phBound::Load(*token);
		}
		else
		{
			bound = phBound::Load(fileName);
		}

		if (bound)
		{
			if (classType==phArchetype::INVALID_ARCHETYPE)
			{
				// No class type was given, so make the class type match the bound's ability to be physically active.
				classType = (bound->CanBecomeActive() ? phArchetype::ARCHETYPE_PHYS : phArchetype::ARCHETYPE);
			}

			// Assign the phBound to a phArchetype.
			archetype = RegisterArchetype(bound,fileName,initPhysFields,classType);
		}
		else
		{
			if (phBound::GetBoundFlag(phBound::WARN_MISSING_BOUNDS))
			{
				if (token)
				{
					Warningf("phArchetypeMgr:GetArchetype - unable to load bound from stream '%s'(tokenizer)",token->filename);
				}
				else
				{
					Warningf("phArchetypeMgr:GetArchetype - unable to load bound file '%s'",fileName);
				}
			}
			// Add this name to the hash table with the archetype Nonexistent to avoid repeating load attempts.
			AddArchetype(&m_Nonexistent,fileName);
			return NULL;
		}
	}
	else if (archetype==&m_Nonexistent)
	{
		// A load attempt already failed on an archetype with this name.
		return NULL;
	}

	Assert(archetype->GetBound());

	return archetype;
}

/*
PURPOSE
    Get a unique physics archetype by name, loading from a file if a physics archetype with the given name
	is already in the hash table.
PARAMS
	fileName		- The name of the archetype to load or retrieve from the hash table.
	initPhysFields	- Optional boolean to tell whether or not to calculate physical properties for the archetype
					  after and if it is loaded.  Default value is true.
	token			- Optional pointer to the tokenizer of the file from which to load the archetype.  Default is NULL,
					  in which case a file named fileName will be loaded if an archetype with that name is not already
					  in the hash table.
	classType		- Optional class type of physics archetype to create (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS.
RETURN
	A pointer to the physics archetype with the given name, either obtained from the hash table or loaded. */
phArchetype * phArchetypeMgr::GetUniqueArchetype (const char* fileName, bool initPhysFields, fiTokenizer* token, int classType)
{
	phArchetype* archetype = FindArchetype(fileName);
	if (archetype)
	{
		phArchetype* uniqueArchetype = archetype->Clone();
		RegisterNewArchetype(archetype,fileName);
		return uniqueArchetype;
	}

	return GetArchetype(fileName,initPhysFields,token,classType);
}


/*
PURPOSE
    Get a physics archetype by name when its existence is not expected.  This is the same as GetArchetype() with the
	same arguments, but warnings are disabled.
PARAMS
	fileName		- The name of the archetype to load or retrieve from the hash table.
	initPhysFields	- Optional boolean to tell whether or not to calculate physical properties for the archetype
					  after and if it is loaded.  Default value is true.
	token			- Optional pointer to the tokenizer of the file from which to load the archetype.  Default is NULL,
					  in which case a file named fileName will be loaded if an archetype with that name is not already
					  in the hash table.
	classType		- Optional class type of physics archetype to create (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS.
RETURN
	A pointer to the physics archetype with the given name, either obtained from the hash table or loaded. */
phArchetype * phArchetypeMgr::GetArchetypeNoWarning (const char * fileName, bool initPhysFields, fiTokenizer * token, int classType)
{
	bool warnMissingBounds = phBound::GetBoundFlag(phBound::WARN_MISSING_BOUNDS);
	phBound::SetBoundFlag(phBound::WARN_MISSING_BOUNDS,false);
	phArchetype* archetype = GetArchetype(fileName,initPhysFields,token,classType);
	phBound::SetBoundFlag(phBound::WARN_MISSING_BOUNDS,warnMissingBounds);
	return archetype;
}


/*
PURPOSE
    Register a physics archetype with the archetype manager.  This should only be called from another class in order
	to register a project-specific derived physics archetype that cannot be loaded by the archetype manager.
PARAMS
	bound			- Pointer to the bound that will be used by the archetype.
	name			- The name of the archetype to load or retrieve from the hash table.
	initPhysFields	- Optional boolean to tell whether or not to calculate physical properties for the archetype.
					  Default value is false. This should be set to true in order to have mass and angular inertia
					  calculated from the bound.
	classType		- Optional class type of physics archetype to create (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS.
	density			- Optional density in units of the density of water (which is 1000 kg/m^3).  Default value is a call to
					  GetDefaultDensity(), which gets phArchetype::sDefaultDensity, which has a default vaue of 0.3f.
RETURN
	A pointer to the physics archetype with the given name.
NOTES
 1.	A file name.phys will be loaded if it is found, to specify archetype physical parametes and class type.
 2.	If initPhysFields is true, then the density will be used with the bound to calculate the mass and angular
	inertia, which will override any mass or angular inertia read in from a .phys file.
 3.	If mass and angular inertia are not set after calling RegisterArchetype, then initPhysFields should be true
	in order to make them calculated here. */
phArchetype* phArchetypeMgr::RegisterArchetype (phBound * bound, const char * name, bool initPhysFields,
												int classType, float density)
{
	// Load the phArchetype and set its bound.
	phArchetype * archetype = phArchetype::Load(name,bound);

	if (archetype==NULL)
	{
		// No boundName.phys file was found from which to load an archetype, so make one.
		if (classType==phArchetype::ARCHETYPE)
		{
			archetype = rage_new phArchetype;
		}
		else if(classType==phArchetype::ARCHETYPE_PHYS)
		{
			archetype = rage_new phArchetypePhys;
			if (initPhysFields)
			{
				PostLoadCompute((phArchetypePhys*)archetype,bound,density,initPhysFields);
			}
		}
		else if (classType==phArchetype::ARCHETYPE_DAMP)
		{
			archetype = rage_new phArchetypeDamp;
			if (initPhysFields)
			{
				PostLoadCompute((phArchetypeDamp*)archetype,bound,density,initPhysFields);
			}
		}
	}

	// Make sure any archetype loaded from a file has a class type equal to or greater than
	// the given class type (for Rage archetype class types, this guarantees that the archetype's
	// class type is either the same or derived from the given class type).
	Assert(archetype->GetClassType()>=classType);

	archetype->SetBound(bound);
	bound->Release(); // Transfer ownership into the archetype

	return AddArchetype(archetype,name);
}


/*
PURPOSE
    Register a physics archetype with the archetype manager, using the given physical parameters instead of trying
	to load	them from a phys file.  If an archetype with the given name is not already in the hash table, this
	method calls the previous version of RegisterArchetype and then sets the given physical parameters.
PARAMS
	bound			- Pointer to the bound that will be used by the archetype.
	name			- The name of the archetype to load or retrieve from the hash table.
	mass			- Mass of the physics archetype.
	angInertia		- Angular inertia of the physics archetype.
	initPhysFields	- Optional boolean to tell whether or not to calculate physical properties for the archetype.
					  Default value is true.
	classType		- Optional class type of physics archetype to create (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS.
RETURN
	A pointer to the physics archetype with the given name.
NOTES
 1.	A file name.phys will be loaded if it is found, and its values for mass and angular inertia and their inverses
	will be overridden with the values given here. */
phArchetype * phArchetypeMgr::RegisterArchetype (phBound * bound, const char * name, float mass,
									    const Vector3 & angInertia, bool initPhysFields, int classType)
{
	phArchetype* archetype = FindArchetype(name);
	if (archetype==NULL)
	{
		archetype = RegisterArchetype(bound,name,initPhysFields,classType);
		if (archetype!=NULL)
		{
			archetype->SetMass(mass);
			archetype->SetAngInertia(angInertia);
		}
	}

	if (archetype==&m_Nonexistent)
	{
		Errorf("A load attempt failed on the archetype %s before RegisterArchetype was called.",name);
		return NULL;
	}

	return archetype;
}


/*
PURPOSE
    Register a physics archetype with the archetype manager, guaranteeing uniqueness.  This is for archetypes that are
	modified in real-time, normally by deforming the bound of an animated character or a ragdoll.
PARAMS
	arch	- Pointer to the physics archetype.
	name	- The base name of the archetype.  The archetype's name may be modified to guarantee uniqueness.
RETURN
	A pointer to a unique physics archetype (one with exactly one external reference).
NOTES
 1.	If an archetype with the given name is already in the hash table, then an underscore followed by an integer is
	appended to the name, and the integer is incremented from 1 until a unique name is found. */
phArchetype * phArchetypeMgr::RegisterNewArchetype (phArchetype * arch, const char * name)
{
	char uniqueName[256];
	uniqueName[255] = '\0';
	strncpy(uniqueName,name,255);

	int extension = 1;
	while (FindArchetype(uniqueName))
	{
		formatf(uniqueName,255,"%s_%i",name,extension);
		extension++;
	}
	return AddArchetype(arch,uniqueName);
}


/*
PURPOSE
    Load or create a physics archetype and register it with the archetype manager, guaranteeing uniqueness.
	This is for archetypes that are modified in real-time, normally by deforming the bound of an animated
	character or a ragdoll.
PARAMS
	bound			- Pointer to the bound that will be used by the archetype.
	name			- The base name of the archetype.  The archetype's name may be modified to guarantee uniqueness.
	initPhysFields	- Optional boolean to tell whether or not to calculate physical properties for the archetype.
					  Default value is false.
	classType		- Optional class type of physics archetype to create (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS.
RETURN
	A pointer to a unique physics archetype (one with exactly one external reference).
NOTES
 1.	If an archetype with the given name is already in the hash table, then an underscore followed by an integer is
	appended to the name, and the integer is incremented from 1 until a unique name is found. */
phArchetype * phArchetypeMgr::RegisterNewArchetype (phBound * bound, const char * name, bool initPhysFields, int classType)
{
	char uniqueName[256];
	uniqueName[255] = '\0';
	strncpy(uniqueName,name,255);
	int extension = 1;
	while (FindArchetype(uniqueName))
	{
		formatf(uniqueName,255,"%s_%i",name,extension);
		extension++;
	}
	return RegisterArchetype(bound,uniqueName,initPhysFields,classType);
}


/*
PURPOSE
    Remove an entry from the archetype manager's hash table, by a pointer to the archetype.
PARAMS
	arch			- Pointer to the physics archetype.
RETURN
	true if archetype was removed, false otherwise. */
bool phArchetypeMgr::RemoveArchetype (const phArchetype *arch)
{
	for (int hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
	{
		if (ArchetypeHashInfo[hashIndex].Archetype==arch)
			return RemoveHashInfoFromTable(hashIndex);
	}

	return false;
}


/*
PURPOSE
	Remove an entry from the archetype manager's hash table, by name of the archetype.
PARAMS
	name			- Pointer to the physics archetype name.
RETURN
	true if archetype was removed, false otherwise. */
bool phArchetypeMgr::RemoveArchetype (const char *name)
{
	Assert(name);
	for (int hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
	{
		if (ArchetypeHashInfo[hashIndex].Name && 0 == strcmp(name, ArchetypeHashInfo[hashIndex].Name))
			return RemoveHashInfoFromTable(hashIndex);
	}

	return false;
}


/*
PURPOSE
    Add an entry to the archetype manager's hash table, by the name of the archetype.
PARAMS
	name			- The name of the archetype to be added to the hash table.
RETURN
	A pointer to the hash table information for the archetype, NULL if there is no room in the hash table
	or if the maximum number of archetypes would be exceeded. */
phArchetypeMgr::HashInfo * phArchetypeMgr::AddHashInfoToTable (const char * name)
{
	int index = GetFirstUnusedHashInfo();
	if (index>=0 && index<MaxNumArchetypes)
	{
		HashInfo* hashInfo=ArchetypeHashInfo+index;
		sysMemStartTemp();
		delete[] hashInfo->Name;
		hashInfo->Name=rage_new char[strlen(name)+1];
		sysMemEndTemp();
		strcpy(hashInfo->Name,name);
		ArchetypeHashTable->Insert(hashInfo->Name,hashInfo);
		return hashInfo;
	}
	return NULL;
}


/*
PURPOSE
    Remove an entry from the archetype manager's hash table, by HashInfo index.
PARAMS
	index			- Index of the HashInfo object to remove.
RETURN
	true if entry was successfully removed, false otherwise. */
bool phArchetypeMgr::RemoveHashInfoFromTable (int index)
{
	Assert(index >= 0 && index < MaxNumArchetypes);

	// if the bound is a composite, need to remove all the composite parts
	phBound *bound = ArchetypeHashInfo[index].Archetype->GetBound();
	if(phBound::COMPOSITE == bound->GetType())
	{
		char partName[64];
		phBoundComposite *composite = (phBoundComposite*)bound;
		for(int partIndex=composite->GetNumBounds()-1; partIndex>=0; partIndex--)
		{
			formatf(partName, sizeof(partName), "%s%i", ArchetypeHashInfo[index].Name, partIndex);
			RemoveArchetype(partName);
		}
	}

	bool success = ArchetypeHashTable->Delete(ArchetypeHashInfo[index].Name);

	if(success)
	{
		sysMemStartTemp();
		delete [] ArchetypeHashInfo[index].Name;
		sysMemEndTemp();
		ArchetypeHashInfo[index].Name = NULL;
		ArchetypeHashInfo[index].Archetype = NULL;

#if __BANK
		// reset the widgets so that invalid memory isn't modified
		KillWidgets();
#endif
	}

	return success;
}


/*
PURPOSE
    Remove from the hashtables the place holder Archetypes that are added during loading when the file is missing.*/
void phArchetypeMgr::PruneHashtable()
{
	RecycleUnused(true);
}

/*
PURPOSE
    Get the index number of the hash table information for the next available spot in the hash table.
RETURN
	The index number of the hash table information for the next available spot in the hash table,
	BAD_INDEX (==-1) if there is no room in the hash table or if the maximum number of archetypes
	would be exceeded. */
int phArchetypeMgr::GetFirstUnusedHashInfo () const
{
	for (int hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
	{
		if (ArchetypeHashInfo[hashIndex].Archetype==NULL)
		{
			return hashIndex;
		}
	}
	return BAD_INDEX;
}


/*
PURPOSE
    Add an archetype to the archetype manager's hash table.
PARAMS
	archetype		- Pointer to the physics archetype to be added to the hash table.
	name			- Name of the archetype.
RETURN
	A pointer to a the physics archetype (same as the given archetype), or NULL if it was not added to the hash table. */
phArchetype * phArchetypeMgr::AddArchetype (phArchetype * archetype, const char * name)
{
	HashInfo * hashInfo = AddHashInfoToTable(name);

	if (hashInfo)
	{
		hashInfo->Archetype = archetype;
		hashInfo->Archetype->AddRef();

		#if __BANK
		// Point the phArchetype's FileName to the HashInfo Name so the phArchetype can be saved.
		char *filename = hashInfo->Name;
		if(pgRscBuilder::IsBuilding())
		{
			// don't allocate extra space inside the resource heap for the filename
			// when the archetype is re-added after resourcing,
			// the SetFilename call below this block will run
			// which will set the filename to point somewhere on the main heap anyway
			// the code below is kept commented just in case filename should be baked into the resources
			filename = NULL;

			// NOTE: put filename onto the resource heap if there is one
			//filename = new char[strlen(hashInfo->Name)+1];
			//strcpy(filename,hashInfo->Name);
		}
		archetype->SetFilename(filename);
		#endif
	
		return archetype;
	}

	return NULL;
}


/*
PURPOSE
    Set the physical parameters for the given archetype, using the bound (if there is one) to calculate mass and
	angular inertia from the given density.
PARAMS
	name			- The name of the archetype to load or retrieve from the hash table.
	archetype		- Pointer to the physics archetype that will have its parameters set.
	bound			- Pointer to the bound to use to calculate mass and angular inertia from the given density.
	density			- Density in units of the density of water (which is 1000 kg/m^3).
	initPhysFields	- Whether or not to calculate physical properties for the archetype.
	classType		- Optional class type of physics archetype (choices are phArchetype::ARCHETYPE,
					  ARCHETYPE_PHYS, ARCHETYPE_DAMP, or project-specific derived physics archetype class types.
					  Default is ARCHETYPE_PHYS. */
void phArchetypeMgr::PostLoadCompute (phArchetypePhys* archetype, phBound *bound, float density,
									  bool initPhysFields)
{
	Vector3 angInertia(1.0f,1.0f,1.0f);
	float mass = 1.0f;

	if (bound && initPhysFields)
	{
		switch (bound->GetType())
		{
			case phBound::SPHERE:
			{
				float radius = ((phBoundSphere *)bound)->GetRadiusAroundCentroid();
				phMathInertia::FindSphereMassAngInertia(density,radius,mass,angInertia);
				break;
			}
			case phBound::GEOMETRY:
			{
				phBoundGeometry* boundGeom=(phBoundGeometry*)bound;
#if COMPRESSED_VERTEX_METHOD == 0
				const Vector3 *vertexPointer = boundGeom->GetVertexPointer();
#else
				Vector3 *vertexPointer = Alloca(Vector3, boundGeom->GetNumVertices());
				for(int vertIndex = 0; vertIndex < boundGeom->GetNumVertices(); ++vertIndex)
				{
					vertexPointer[vertIndex].Set(VEC3V_TO_VECTOR3(boundGeom->GetVertex(vertIndex)));
				}
#endif
				bool hasVolume = phMathInertia::FindGeomMassAngInertia(density,vertexPointer,boundGeom->GetPolygonPointer(),
																		boundGeom->GetNumPolygons(),VEC3V_TO_VECTOR3(boundGeom->GetCGOffset()),mass,angInertia);
				if (!hasVolume)
				{
					boundGeom->SetIsFlat();
				}

				break;
			}
			case phBound::BVH:
			{
				// Don't need to calculate any of this for these kinds of bounds, and it we don't have a real vertex pointer if we're using compressed vertices.
				break;
			}
#if USE_GEOMETRY_CURVED
			case phBound::GEOMETRY_CURVED:
			{
				phMathInertia::FindSphereMassAngInertia(density,bound->GetRadiusAroundCentroid(),mass,angInertia);
				break;
			}
#endif // USE_GEOMETRY_CURVED
			case phBound::BOX:
			{
				Vector3 boxSize = VEC3V_TO_VECTOR3(bound->GetBoundingBoxSize());
				phMathInertia::FindBoxMassAngInertia(density,boxSize.x,boxSize.y,boxSize.z,&mass,&angInertia);
				break;
			}
			case phBound::CAPSULE:
			USE_TAPERED_CAPSULE_ONLY(case phBound::TAPERED_CAPSULE:)
			{
				const phBoundCapsule* capsule = static_cast<const phBoundCapsule*>(bound);
				phMathInertia::FindCapsuleMassAngInertia(density,capsule->GetRadius(),capsule->GetLength(),&mass,&angInertia);
				break;
			}
			case phBound::DISC:
			case phBound::CYLINDER:
			{
				// TODO: Now that bounds store this information I think this is what should be getting done for every bound type.
				mass = density * bound->GetVolume();
				angInertia = VEC3V_TO_VECTOR3(bound->GetComputeAngularInertia(mass));
			}
			case phBound::COMPOSITE:
			{
				phBoundComposite* composite = static_cast<phBoundComposite*>(bound);
				mass = composite->GetVolume()*density*1000.0f;
				angInertia.Set(VEC3V_TO_VECTOR3(composite->GetComputeAngularInertia(mass)));
				break;
			}
#if USE_GRIDS
			case phBound::GRID:		// Is there any reason to have this here?  This doesn't need mass/angular inertia because it can't be active.
			{
				// This isn't right, but it's better than the default: case for now
				break;
			}
#endif // USE_GRIDS
#if USE_RIBBONS			
			case phBound::RIBBON:	// Is there any reason to have this here?  This doesn't need mass/angular inertia because it can't be active.
			{
				// This isn't right, but it's better than the default: case for now
				break;
			}
#endif // USE_RIBBONS
			default:
			{
				AssertMsg(0 , "unrecognized bound type");
				break;
			}
		}
	}

	archetype->SetMass(mass);
	archetype->SetAngInertia(angInertia);
}


#if __BANK

void phArchetypeMgr::AddWidgets(bkBank& bank)
{
	Bank = &bank;
	bank.AddButton("Refresh Archetype Bank",datCallback(MFA(phArchetypeMgr::RefreshArchetypeBank),this));
}

void phArchetypeMgr::RefreshArchetypeBank()
{
	KillWidgets();
	if (!WidgetsOpen)
	{
		AssertMsg(Bank , "phArchetypeMgr::OpenArchetypeBank()");
		AddArchetypeWidgets();
		WidgetsOpen = true;
	}
}

void phArchetypeMgr::KillWidgets()
{
	if (WidgetsOpen)
	{
		AssertMsg(Bank , "phArchetypeMgr::KillWidgets()");
		int numGroups = WidgetGroups.GetCount();
		for(int i = 0; i < numGroups; ++i)
			Bank->DeleteGroup(*WidgetGroups[i]);
		WidgetsOpen = false;
		// clear out the existing groups
		WidgetGroups.Reset();
	}
}

void phArchetypeMgr::AddArchetypeWidgets()
{
	// Make an alphabetized list of archetype names for the widget banks.
	int hashIndex = FindNextAlphabetizedArchetype();
	while (hashIndex!=BAD_INDEX)
	{
		Assert(ArchetypeHashInfo[hashIndex].Archetype);
		WidgetGroups.Grow() = Bank->PushGroup(ArchetypeHashInfo[hashIndex].Name,false);
		ArchetypeHashInfo[hashIndex].Archetype->AddWidgets(*Bank);
		Bank->PopGroup();
		hashIndex = FindNextAlphabetizedArchetype(ArchetypeHashInfo[hashIndex].Name);
	}
}


/*
PURPOSE
    Find the index number of the physics archetype with the name that comes next after the given archetype name
	in alphabetical order, or the first archetype name in alphabetical order if no name is given.
PARAMS
	name			- The name of the previous archetype in alphabetical order.
RETURN
	The index number of the physics archetype with the name that comes next after the given archetype name
	in alphabetical order, or the first archetype name in alphabetical order if no name is given.
NOTES
 1.	This is used only to get an alphabetical list of archetype names for easy access in the widget bank.
*/
int phArchetypeMgr::FindNextAlphabetizedArchetype (const char* name) const
{
	int nextIndex = BAD_INDEX;
	for (int hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
	{
		if (ArchetypeHashInfo[hashIndex].Archetype)
		{
			if (!name || strcmp(name,ArchetypeHashInfo[hashIndex].Name)<0)
			{
				// The name of the archetype hashIndex comes after the given name.
				if (nextIndex==BAD_INDEX || strcmp(ArchetypeHashInfo[hashIndex].Name,ArchetypeHashInfo[nextIndex].Name)<0)
				{
					// The name of the archetype hashIndex comes before the best one found so far,
					// or none was found yet, so now hashIndex is the best one found so far.
					nextIndex = hashIndex;
				}
			}
		}
	}
	return nextIndex;
}
#endif


#if __DEV
const char* phArchetypeMgr::GetName (const phArchetype* archetype) const
{
	for (int hashIndex=0;hashIndex<MaxNumArchetypes;hashIndex++)
	{
		if (ArchetypeHashInfo[hashIndex].Archetype==archetype)
		{
			return ArchetypeHashInfo[hashIndex].Name;
		}
	}
	return NULL;
}
#endif
