///////////////////////////
//   breakableinst.cpp   //
///////////////////////////

#include "breakableinst.h"
#include "breakablemgr.h"
#include "demoobject.h"

#include "phbound/boundcomposite.h"
#include "phsolver/contactmgr.h"
#include "physics/impact.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/timemgr.h"

namespace ragesamples {


phBRInst::phBRInst()
{
	ActiveImpulseLimit = -1.0f;
	InactiveImpulseLimit = 1.0f;
	BrokenFree = false;
	BrokeOffPart = false;
	ShatterType = NULL;
}


phBRInst::~phBRInst()
{
}


float phBRInst::GetInactiveImpulseLimit () const
{
	if (!BrokenFree)
	{
		// This instance can break while inactive.
		return InactiveImpulseLimit;
	}
	else
	{
		// This instance was already broken while inactive, so it has no resistance to breaking again. Returning zero
		// here will make it promote to active when colliding, and the active impulse limit will then be checked.
		return 0.0f;
	}
}


float phBRInst::GetActiveImpulseLimit () const
{
	if (ShatterType)
	{
		// This instance can break while active.
		return ActiveImpulseLimit;
	}
	else
	{
		// This instance can not break while active.
		return -1.0f;
	}
}

/*
bool phBRInst::ShouldMakeActive (phCollider** UNUSED_PARAM(collider), bool* UNUSED_PARAM(alwaysActive)) const
{
	// Return true if this object has been broken, to indicate that it should have no resistance to moving when it collides.
	// Return false if it has never broken, to indicate that it should have breaking resistance.
	return BrokenFree;
}
*/

bool phBRInst::FindBreakStrength (const phImpact* impactList, float* breakStrength, const phImpactData* impactData,
									phBreakData* breakData) const
{
	float weakestBreakStrength = 1.0f;
	float totalImpulseMag2;
	int weakestIndex = BAD_INDEX;

	if (ShatterType)
	{
		// Set all the accumulated impulses on the breakable parts to zero.
		int numParts = GetNumParts();
		int partIndex;
		for (partIndex=0; partIndex<numParts; partIndex++)
		{
			ShatterType->ResetPartTotalImpulse(partIndex);
		}

		// Find the total impulse on each part, and then add it to the accumulated total impulse of
		// the part or the part from which it is descended with the smallest breaking limit.
		Assert(GetArchetype()->GetBound()->GetType()==phBound::COMPOSITE);
		phBoundComposite* compositeBound = static_cast<phBoundComposite*>(GetArchetype()->GetBound());
		float totalImpulseMag;
		if (impactList)
		{
			Vector3 totalImpulse;
			float impulseLimit;
			for (partIndex=numParts-1; partIndex>=0; partIndex--)
			{
				if (compositeBound->GetBound(partIndex))
				{
					totalImpulse.Set(impactList->GetTotalImpulse(this,false,NULL,partIndex));
					totalImpulseMag2 = totalImpulse.Mag2();
					if (totalImpulseMag2>SMALL_FLOAT)
					{
						ShatterType->AddToPartTotalImpulse(FindWeakestIndex(partIndex),sqrtf(totalImpulseMag2));
					}
				}
			}
	
			// Find the breakable part that will be the first to break.
			for (partIndex=0; partIndex<numParts; partIndex++)
			{
				if (compositeBound->GetBound(partIndex))
				{
					impulseLimit = ShatterType->GetPartImpulseLimit(partIndex);
					totalImpulseMag = ShatterType->GetPartTotalImpulse(partIndex);
					if (totalImpulseMag>impulseLimit && totalImpulseMag*weakestBreakStrength>impulseLimit)
					{
						weakestBreakStrength = impulseLimit/totalImpulseMag;
						weakestIndex = partIndex;
					}
				}
			}
		}
		else
		{
			Assert(impactData);
			totalImpulseMag2 = impactData->GetImpetus().Mag2();
			if (impactData->IsContact())
			{
				totalImpulseMag2 *= square(TIME.GetSeconds());
			}

			if (totalImpulseMag2>SMALL_FLOAT)
			{
				weakestIndex = FindWeakestIndex(impactData->GetComponentA());
				if (weakestIndex!=BAD_INDEX)
				{
					weakestBreakStrength = ShatterType->GetPartImpulseLimit(weakestIndex)*invsqrtf(totalImpulseMag2);
				}
			}
		}
	}
	else
	{
		// This object is not breakable into parts. It can only break free from fixed.
		Assert(!BrokenFree);
		if (impactList)
		{
			totalImpulseMag2 = impactList->GetTotalImpulse(this).Mag2();
		}
		else
		{
			Assert(impactData);
			totalImpulseMag2 = impactData->GetImpetus().Mag2();
			if (impactData->IsContact())
			{
				totalImpulseMag2 *= square(TIME.GetSeconds());
			}
		}

		if (totalImpulseMag2>square(InactiveImpulseLimit))
		{
			weakestBreakStrength = InactiveImpulseLimit*invsqrtf(totalImpulseMag2);
			weakestIndex = 0;
		}
	}

	if (weakestIndex!=BAD_INDEX)
	{
		// At least one part can break from this collision, and the weakest part was found.
		(*breakStrength) = Min(Max(weakestBreakStrength, 0.0f), 1.0f);
		breakData->BreakData[0] = (u8)weakestIndex;
		return true;
	}

	return false;
}


bool phBRInst::FindBreakStrength (const Vector3* componentImpulses, float* breakStrength, phBreakData* breakData) const
{
	float weakestBreakStrength = 1.0f;
	float totalImpulseMag2;
	int weakestIndex = BAD_INDEX;

	if (ShatterType)
	{
		// Set all the accumulated impulses on the breakable parts to zero.
		int numParts = GetNumParts();
		int partIndex;
		for (partIndex=0; partIndex<numParts; partIndex++)
		{
			ShatterType->ResetPartTotalImpulse(partIndex);
		}

		// Find the total impulse on each part, and then add it to the accumulated total impulse of
		// the part or the part from which it is descended with the smallest breaking limit.
		Assert(GetArchetype()->GetBound()->GetType()==phBound::COMPOSITE);
		phBoundComposite* compositeBound = static_cast<phBoundComposite*>(GetArchetype()->GetBound());
		for (partIndex=0; partIndex<numParts; partIndex++)
		{
			if (compositeBound->GetBound(partIndex))
			{
				totalImpulseMag2 = componentImpulses[partIndex].Mag2() * square(TIME.GetSeconds());

				if (totalImpulseMag2>SMALL_FLOAT)
				{
					weakestIndex = FindWeakestIndex(partIndex);
					if (weakestIndex!=BAD_INDEX)
					{
						weakestBreakStrength = ShatterType->GetPartImpulseLimit(weakestIndex)*invsqrtf(totalImpulseMag2);
					}
				}
			}
		}
	}
	else
	{
		// This object is not breakable into parts. It can only break free from fixed.
		Assert(!BrokenFree);
		totalImpulseMag2 = componentImpulses->Mag2() * square(TIME.GetSeconds());

		if (totalImpulseMag2>square(InactiveImpulseLimit))
		{
			weakestBreakStrength = InactiveImpulseLimit*invsqrtf(totalImpulseMag2);
			weakestIndex = 0;
		}
	}

	if (weakestIndex!=BAD_INDEX)
	{
		// At least one part can break from this collision, and the weakest part was found.
		(*breakStrength) = Min(Max(weakestBreakStrength, 0.0f), 1.0f);
		breakData->BreakData[0] = (u8)weakestIndex;
		return true;
	}

	return false;
}

int phBRInst::FindWeakestIndex (int partIndex) const
{
	// Get the part's parent index.
	if (ShatterType)
	{
		int parentIndex = ShatterType->GetPartParentIndex(partIndex);
		if (parentIndex!=BAD_INDEX)
		{
			// The part has a parent.
			int weakestIndex = partIndex;
			float weakestImpulseLimit = ShatterType->GetPartImpulseLimit(weakestIndex);
			partIndex = parentIndex;
			while (partIndex!=BAD_INDEX)
			{
				// See if this part has a parent.
				parentIndex = ShatterType->GetPartParentIndex(partIndex);
				if (parentIndex!=BAD_INDEX || !BrokenFree)
				{
					// This part has a parent, or this instance has not been broken, so this part can break.
					float impulseLimit = ShatterType->GetPartImpulseLimit(partIndex);
					if (impulseLimit<weakestImpulseLimit)
					{
						weakestImpulseLimit = impulseLimit;
						weakestIndex = partIndex;
					}
				}

				// Set the part to its parent and repeat.
				partIndex = parentIndex;
			}

			return weakestIndex;
		}
	}
	
	if (!BrokenFree)
	{
		// This part has no parent and the instance has not been broken from fixed, so this part is the weakest.
		return partIndex;
	}
	
	// This part has no parent, and this instance has already been broken from fixed, so this part can't break.
	return BAD_INDEX;
}


int phBRInst::BreakApart (phImpact* impactList, phInstBreakable** breakableInstList, int* numBreakInsts, phInst** brokenInstList,
							phImpactData* impactData, const phBreakData& breakData)
{
	int breakPartIndex = breakData.BreakData[0];

	if (breakPartIndex==0)
	{
		// Break into itself.
		Assert(!BrokenFree);
		BrokenFree = true;
		const bool breakableAgain = (GetNumParts()>1);
		return phInstBreakable::BreakIntoActiveSelf(impactList,breakableInstList,numBreakInsts,brokenInstList,breakableAgain,
													impactData);
	}

	// Break a piece off.
	int numBrokenInsts = BreakIntoTwo(impactList,breakableInstList,numBreakInsts,brokenInstList,breakPartIndex,impactData);
	BrokeOffPart = true;
	return numBrokenInsts;
}

int phBRInst::BreakApart (phInstBreakable** breakableInstList, int* numBreakInsts, phInst** brokenInstList,
							const phBreakData& breakData)
{
	int breakPartIndex = breakData.BreakData[0];

	if (breakPartIndex==0)
	{
		// Break into itself.
		Assert(!BrokenFree);
		BrokenFree = true;
		const bool breakableAgain = (GetNumParts()>1);
		return phInstBreakable::BreakIntoActiveSelf(NULL,breakableInstList,numBreakInsts,brokenInstList,breakableAgain,
													NULL);
	}

	// Break a piece off.
	BrokeOffPart = true;
//	int numBrokenInsts = BreakIntoTwo(impactList,breakableInstList,numBreakInsts,brokenInstList,breakPartIndex,impactData);
	// Get the part to break off.
	phInst* partInstance = GetBrokenPartInstance(breakPartIndex);

	int numBrokenParts = 0;
	if (partInstance)
	{
		// Add the broken part to the physics level.
		if(PHSIM->AddActiveObject(partInstance, false))
		{
			if(PHLEVEL->IsActive(GetLevelIndex()))
			{
				// Find the post-breaking motion of this instance. This is used to throw the broken parts to account
				// for the application of the pre-breaking impetuses.
				Vector3 centerOfMass,velocity,angVelocity;
				FindPostBreakMotion(centerOfMass,velocity,angVelocity);

				// Set the broken part in motion to match its pre-breaking motion as part of this object.
				ThrowBrokenPart(partInstance,centerOfMass,velocity,angVelocity);
			}
			
			// Put the broken part in the broken part list.
			Assert(brokenInstList);
			brokenInstList[0] = partInstance;
			numBrokenParts = 1;

			// Put the broken part in the impact list.
			if (PHCONSTRAINT->ReplaceInstanceComponent(this,breakPartIndex,partInstance,breakPartIndex))
			{
				// Put this instance in the broken part list too, since it still has impacts on it.
				brokenInstList[1] = this;
				numBrokenParts = 2;
			}

			if (GetNumCompositeParts()==1)
			{
				// Remove this instance from the breakable instance list, since it is not breakable again.
				Assert(breakableInstList);
				RemoveFromInstList(breakableInstList,this,numBreakInsts);
			}

			if (partInstance->ClassTypeBreakable())
			{
				phInstBreakable* breakablePart = static_cast<phInstBreakable*>(partInstance);
				if (breakablePart->GetNumCompositeParts()>1)
				{
					breakableInstList[*numBreakInsts] = breakablePart;
					(*numBreakInsts)++;
				}
			}
		}
	}

	return numBrokenParts;
}

// Get the instance for the broken part.
phInst* phBRInst::GetBrokenPartInstance (int partIndex)
{
	// See if there are any other parts descended from the given part.
	int numPartsToBreak = 0;
	int brokenPartIndices[MaxNumShatterParts];
	int numParts = GetNumParts();
	for (int testPartIndex=0; testPartIndex<numParts; testPartIndex++)
	{
		if (IsDescendedOrSame(testPartIndex,partIndex))
		{
			brokenPartIndices[numPartsToBreak++] = testPartIndex;
		}
	}

	phArchetypePhys* archetype = static_cast<phArchetypePhys*>(GetArchetype());
	phBoundComposite* bound = static_cast<phBoundComposite*>(archetype->GetBound());

	Assert(numPartsToBreak>0 && numPartsToBreak<numParts);
	phDemoObject* demoObject = NULL;
	if (numPartsToBreak==1)
	{
		// Try to get a demo object from the shatter manager for the broken part.
		Assert(partIndex==brokenPartIndices[0]);
		demoObject = SHATTERMGR->GetSinglePartObject(ShatterType,partIndex);

		// Position the broken part.
		Matrix34 partMatrix(bound->GetLocalMatrix(partIndex));
		partMatrix.Dot(GetMatrix());
		demoObject->GetPhysInst()->SetMatrix(partMatrix);
	}
	else
	{
		demoObject = SHATTERMGR->GetCompositePartObject(ShatterType,brokenPartIndices,numPartsToBreak);
		demoObject->GetPhysInst()->SetMatrix(GetMatrix());
	}
	
	if (demoObject)
	{
		// Remove the broken parts from this bound.
		float mass = archetype->GetMass();
		for (int index=0; index<numPartsToBreak; index++)
		{
			bound->RemoveBound(brokenPartIndices[index]);
			mass -= ShatterType->GetSinglePartArchetype(brokenPartIndices[index])->GetMass();
		}

		// Reset the archetype's mass and calculate the archetype's angular inertia.
		Assert(mass>0.0f);
		archetype->SetMass(mass);

		return demoObject->GetPhysInst();
	}

	return NULL;
}


bool phBRInst::IsDescendedOrSame (int partIndex, int possibleParentIndex) const
{
	Assert(possibleParentIndex!=BAD_INDEX);
	while (partIndex!=BAD_INDEX && partIndex!=possibleParentIndex)
	{
		partIndex = ShatterType->GetPartParentIndex(partIndex);
	}

	return (partIndex==possibleParentIndex);
}


void phBRInst::SetShatterType (phShatterableObjectType* shatterType)
{
	ShatterType = shatterType;
}


int phBRInst::GetNumParts () const
{
	if (ShatterType)
	{
		Assert(ShatterType->GetCompositeBound());
		return ShatterType->GetCompositeBound()->GetNumActiveBounds();
	}

	return 0;
}

} // namespace ragesamples
