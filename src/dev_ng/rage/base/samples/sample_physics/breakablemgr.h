// 
// sample_physics/breakablemgr.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PHTEST_BREAKABLEMGR_H
#define PHTEST_BREAKABLEMGR_H

#include "breakableinst.h"
#include "demoobject.h"

#include "phbound/boundcomposite.h"
#include "physics/archetype.h"
#include "physics/instbreakable.h"
#include "vector/matrix34.h"

namespace rage {

class phCollider;

}

namespace ragesamples {

using namespace rage;

#define SHATTERMGR (phShatterManager::GetShatterManager())

class phShatterPart : public phDemoObject
{
public:
	float ImpulseLimit;			// the minimum impulse needed to break this part off from its parent or from fixed
	float TotalImpulse;			// accumulated impulse on this collision from its children
	int ParentIndex;			// pointer to its parent; if NULL then impulse limit applies to breaking from fixed
};


// Object class for breaking multiple parts off of a colliding object.
const int maxNumShatterTypes = 16;
const int maxShatterNameLength = 32;
class phShatterableObjectType : public phDemoObject
{
public:
	phShatterableObjectType();
	void Init (const char* name);
	phBoundComposite* GetCompositeBound ();
	phArchetype* GetSinglePartArchetype (int partIndex) { return ShatterPart[partIndex].GetPhysInst()->GetArchetype(); }
	const phShatterPart& GetModelPart(int partIndex) { return ShatterPart[partIndex]; }
	int GetPartParentIndex (int partIndex) { return ShatterPart[partIndex].ParentIndex; }
	void ResetPartTotalImpulse (int partIndex) { ShatterPart[partIndex].TotalImpulse = 0.0f; }
	void AddToPartTotalImpulse (int partIndex, float impulse) { ShatterPart[partIndex].TotalImpulse += impulse; }
	float GetPartImpulseLimit (int partIndex) const { return ShatterPart[partIndex].ImpulseLimit; }
	float GetPartTotalImpulse (int partIndex) const { return ShatterPart[partIndex].TotalImpulse; }
	const char* GetName () const { return Name; }

protected:
	phShatterPart ShatterPart[phBRInst::MaxNumShatterParts];
	char Name[maxShatterNameLength];
};


class phShatterableObject : public phDemoObject
{
public:
	phShatterableObject();
	virtual void DrawModel (const Matrix34& mtx) const;
	void SetShatterType (phShatterableObjectType* shatterType);
	void Reset (phLevelNew *pLevelNew, phSimulator *pSim);

protected:
	phShatterableObjectType* ShatterType;
};


class phShatterManager
{
public:
	phShatterManager();
	phShatterableObjectType* GetShatterType (const char* name);
	phDemoObject* GetSinglePartObject (phShatterableObjectType* shatterType, int partIndex);
	phDemoObject* GetCompositePartObject (phShatterableObjectType* shatterType, int* partIndices, int numParts);
	void Reset (phSimulator *pSim);
	void InitCompositePartArchetype (int objectIndex, phShatterableObjectType* shatterType, int* partIndices, int numParts);

	enum { TotalNumSingleParts = 256, TotalNumCompositeParts = 128 };

	static phShatterManager* GetShatterManager() { return phShatterManager::ShatterManager; }
	static void SetShatterManager(phShatterManager* shatterManager) { ShatterManager = shatterManager; }
	static void CreateShatterManager();

protected:
	phShatterableObjectType ShatterTypes[maxNumShatterTypes];
	phDemoObject SinglePartObjects[TotalNumSingleParts];
	phInst SinglePartInstances[TotalNumSingleParts];
	phShatterableObject CompositePartObjects[TotalNumCompositeParts];
	phBRInst CompositePartInstances[TotalNumCompositeParts];
	phArchetypePhys CompositePartArchetypes[TotalNumCompositeParts];
	phBoundComposite CompositePartBounds[TotalNumCompositeParts];

	static phShatterManager* ShatterManager;
};

} // namespace ragesamples

#endif // end of #ifndef PHTEST_BREAKABLEMGR_H
