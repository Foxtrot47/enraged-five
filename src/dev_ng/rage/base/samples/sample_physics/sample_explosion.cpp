//
// sample_physics/sample_explosion.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Explosion
// PURPOSE:	This sample demonstrates how to use explosions and how to use the explosion manager, along with showcasing the behavior of
//			explosions on objects of various shapes, sizes and orientations.
//			The usual camera controls apply.  Additionally, the space bar detonates an explosion at a random location and 'O' detonates
//			an explosion at the origin.

#include "sample_physics.h"

#include "input/keys.h"
#include "math/random.h"
#include "phbound/boundsphere.h"
#include "pheffects/explosionmgr.h"
#include "pheffects/instbehaviorexplosion.h"
#include "physics/colliderdispatch.h"
#include "physics/simulator.h"
#include "system/main.h"
#include "system/param.h"
#include "vectormath/classes.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

class explosionSampleManager : public physicsSampleManager
{
public:
	virtual void InitClient()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();

		// STEP #2. Create extra keyboard controls (space detonates an explosion at a random position and O detonates an explosion at the origin).
		m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE, m_TriggerExplosionRandom);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_O, m_TriggerExplosionOrigin);

		// STEP #3. Create a basic world with a flat terrain plane and some objects to react to explosions.
		m_Demos.AddDemo(rage_new phDemoWorld("Pyramid"));
		m_Demos.GetCurrentWorld()->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane();
		m_Demos.GetCurrentWorld()->CreateObject("crate",Vector3(-2.0f, 0.0f, 4.0f),true);
		m_Demos.GetCurrentWorld()->CreateObject("crate",Vector3(+3.0f, 0.0f, 5.0f),true);
		m_Demos.GetCurrentWorld()->CreateObject("crate",Vector3(-3.0f, 0.0f, -3.0f),true);
		m_Demos.GetCurrentWorld()->CreateObject("crate",Vector3(+2.0f, 0.0f, -1.0f),true);
		m_Demos.GetCurrentWorld()->CreateObject("cinderblock",Vector3(0.2f, 0.0f, -0.9f),true);
		m_Demos.GetCurrentWorld()->CreateObject("long_crate",Vector3(-1.0f, 0.0f, -2.0f),true);
		Matrix34 ObjectMatrix(M34_IDENTITY);
		ObjectMatrix.RotateY(0.35f);
		ObjectMatrix.d.Set(-1.0f, 0.0f, -3.0f);
		m_Demos.GetCurrentWorld()->CreateObject("escalade", ObjectMatrix, true);
		ObjectMatrix.d.Set(+1.0f, 0.0f, -20.0f);
		m_Demos.GetCurrentWorld()->CreateObject("small_wall", ObjectMatrix, true);
		ObjectMatrix.d.Set(-1.0f, 0.0f, +20.0f);
		m_Demos.GetCurrentWorld()->CreateObject("small_wall", ObjectMatrix, true);
		ObjectMatrix.d.Set(-20.0f, 0.0f, +1.0f);
		m_Demos.GetCurrentWorld()->CreateObject("small_wall", ObjectMatrix, true);
		ObjectMatrix.d.Set(+20.0f, 0.0f, -1.0f);
		m_Demos.GetCurrentWorld()->CreateObject("small_wall", ObjectMatrix, true);

		// STEP #4. Activate the demo world.
		const int STARTING_DEMO = 0;
		m_Demos.SetCurrentDemo(STARTING_DEMO);
		m_Demos.GetCurrentWorld()->Activate();
	}

	virtual void Update()
	{
		// STEP #5. Update the extra input controls.
		if (m_TriggerExplosionRandom.IsPressed())
		{
			// They've pressed the trigger explosion key.  Let's randomly pick a place to put our explosion and then set it off.
			Vector3 ExplosionPos;
			ExplosionPos.x = 100.0f * g_ReplayRand.GetFloat() - 50.0f;
			ExplosionPos.y = 25.0f * g_ReplayRand.GetFloat() - 25.0f;
			ExplosionPos.z = 100.0f * g_ReplayRand.GetFloat() - 50.0f;
			m_Demos.GetCurrentWorld()->GetExplosionMgr()->SpawnExplosion(ExplosionPos, &m_ExplosionType);
		}

		if (m_TriggerExplosionOrigin.IsPressed())
		{
			// They've specifically requested an explosion at the origin.  Let's comply.
			Vector3 ExplosionPos(ORIGIN);
			ExplosionPos.y = -2.0f;
			m_Demos.GetCurrentWorld()->GetExplosionMgr()->SpawnExplosion(ExplosionPos, &m_ExplosionType);
		}

		// STEP #6. Update the base physics sample manager.
		physicsSampleManager::Update();
	}

private:
	ioValue				m_TriggerExplosionOrigin, m_TriggerExplosionRandom;
	phExplosionType		m_ExplosionType;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::explosionSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
