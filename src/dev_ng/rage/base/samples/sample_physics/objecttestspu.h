//
// physics/shapetestspu.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef PHYSICS_OBJECTTESTSPU_H
#define PHYSICS_OBJECTTESTSPU_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../src/system/taskheader.h"

DECLARE_TASK_INTERFACE(objecttestspu);
void objecttestspu (rage::sysTaskParameters &/*taskParams*/);

#endif