//
// sample_physics/sample_contacts.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Contacts
// PURPOSE:
//		This sample creates stacks of objects of various types for testing the computation of contact forces.

#include "demoobject.h"
#include "sample_physics.h"

#include "sample_physics/archmgr.h"

#include "bank/bkmgr.h"
#include "math/random.h"
#include "phcore/materialmgrflag.h"
#include "phsolver/contactmgr.h"
#include "physics/collider.h"
#include "physics/colliderdispatch.h"
#include "physics/constraintmgr.h"
#include "physics/constraintdistance.h"
#include "physics/simulator.h"
#include "phbound/boundgeom.h"
#include "phcore/phmath.h"
#include "grprofile/drawmanager.h"
#include "system/main.h"
#include "system/param.h"
#include "vectormath/classes.h"

// TEMPORARY (only for building the new splash BVH)
#include "phbound/boundbvh.h"

#if __WIN32
#pragma warning (disable : 4189) // local variable is initialized but not referenced
#pragma warning (disable : 4101) // unreferenced local variable
#endif

PARAM(world, "The demo world to initialize when the demo boots");
PARAM(file, "Reserved for future expansion, not currently hooked up");
PARAM(nobjscale, "A scaling factor applied to the number of objects created");

namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Active);
EXT_PFD_DECLARE_ITEM(Inactive);
EXT_PFD_DECLARE_ITEM(Fixed);

}

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

class contactsSampleManager : public physicsSampleManager
{
public:
	virtual void InitClient()
	{
		m_FrameCounter = 0;
		physicsSampleManager::InitClient();

		m_DemoScale = 1.0f;
		PARAM_nobjscale.Get( m_DemoScale );

		//-- Change this vector to test physics behavior with roundoff errors far from the origin.
//		Vector3 worldOffset(0.0f,0.0f,0.0f);
		Vector3 worldOffset(100.0f,0.0f,0.0f);
		phDemoObject::SetWorldOffset(worldOffset);

		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateRubik3World));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateRubik4World));

		// Disable breaking to save a couple of percent speed
		phContactMgr::SetBreakingEnabled(false);

		int startingDemo = 0;
		PARAM_world.Get(startingDemo);
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
	}

	///////////////////////////
	// 0: the Bullet House
	// http://www.continuousphysics.com/ftp/pub/test/physics/movies/bullet_house_demolition_realtime.mov
	phDemoWorld* CreateBulletHouse()
	{
		int maxOctreeNodes = 1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int maxInstBehaviors = 16;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		const char* terrainName = NULL;
		const Vector3& terrainPosition = ORIGIN;
		int scratchpadSize = 5*1024*1024;
		int maxManifolds = 2048;
		phDemoWorld& bulletWorld = *CreateNewDemoWorld("Bullet House",terrainName,terrainPosition,maxOctreeNodes,maxActiveObjects,maxObjects,worldMin,worldMax,maxInstBehaviors,scratchpadSize,maxManifolds);
		position.Set(0.0f,0.0f,0.0f);
		bulletWorld.ConstructRectangularTower(0.5f,0.3f,5,4,9,position);

		bulletWorld.CreateBox(Vector3(5.0f,1.1f,0.5f), 0.2f, Vector3(0.0f,3.25f,0.0f));

		phDemoObject* brace1 = bulletWorld.CreateBox(Vector3(5.5f,0.1f,0.1f), 0.95f, Vector3(0.0f,2.7f, 2.15f));
		phDemoObject* brace2 = bulletWorld.CreateBox(Vector3(5.5f,0.1f,0.1f), 0.95f, Vector3(0.0f,2.7f,-2.15f));
		phConstraintDistance::Params distanceConstraint;
		distanceConstraint.instanceA = brace1->GetPhysInst();
		distanceConstraint.instanceB = brace2->GetPhysInst();
		distanceConstraint.worldAnchorA = Vec3V(2.25f, 2.7f, 2.15f);
		distanceConstraint.worldAnchorB = Vec3V(2.25f, 2.7f, -2.15f);
		bulletWorld.GetSimulator()->GetConstraintMgr()->Insert(distanceConstraint);
		distanceConstraint.worldAnchorA = Vec3V(-2.25f, 2.7f, 2.15f);
		distanceConstraint.worldAnchorB = Vec3V(-2.25f, 2.7f, -2.15f);
		bulletWorld.GetSimulator()->GetConstraintMgr()->Insert(distanceConstraint);

		for (int plank = 0; plank < 11; ++plank)
		{
			bulletWorld.CreateBox(Vector3(0.49f,0.1f,2.4f), 0.20f, Vector3(-2.5f + 0.5f * plank,3.3f,1.05f), false, Vector3(PI *  0.16f, 0.0f, 0.0f));
			bulletWorld.CreateBox(Vector3(0.49f,0.1f,2.4f), 0.20f, Vector3(-2.5f + 0.5f * plank,3.3f,-1.05f), false, Vector3(PI * -0.16f, 0.0f, 0.0f));
		}

        bulletWorld.CreateObject("sphere_056",Vector3( 2.5f,0.0f, 4.0f));
        bulletWorld.CreateObject("sphere_122",Vector3(-2.5f,0.0f, 4.0f));
        bulletWorld.CreateObject("sphere_227",Vector3( 0.0f,0.0f,-6.0f));

		return &bulletWorld;
	}

	///////////////////////////
	// 1: a few objects on a plane
	phDemoWorld* CreateParkWorld()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int numHigh = 2;
		int numAround;
		bool alignBottom = true;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& parkWorld = *CreateNewDemoWorld("Park","plane_complex");
#if 1
        position.Set(0.0f,0.0f,-2.0f);
	#if __XENON || __PS3
		numHigh = 8;
		numAround = 8;
	#else
		numHigh = 2;
		numAround = 4;
	#endif
		boxExtents.Set(1.0f,0.5f,0.5f);
		parkWorld.ConstructRoundTower(position,numHigh,numAround,boxExtents);
		position.Set(1.0f,0.0f,2.0f);
		rotation.Set(0.0f,0.0f,PI*0.5f);
		parkWorld.CreateObject("icosahedron",position,alignBottom,rotation);
		position.Set(4.0f,3.1f,4.7f);
		rotation.Zero();
		parkWorld.CreateObject("car_body",position,alignBottom,rotation);
		position.Set(5.1f,3.4f,-1.9f);
		parkWorld.CreateObject("longer_crate",position,alignBottom,rotation);
		position.Set(2.0f,0.0f,1.0f);
		parkWorld.CreateObject("hotdog",position,alignBottom,rotation);
		position.Set(-6.0f,0.2f,-2.0f);
		rotation.Set(0.0f,0.0f,-0.125f*PI);
		parkWorld.CreateFixedObject("small_plane",position,alignBottom,rotation);
		position.Set(-8.0f,4.0f,-2.0f);
		parkWorld.CreateObject("sphere_122",position);
//		position.Set(-3.0f,2.0f,2.0f);
//		parkWorld.CreateCurvedGeometryMailbox(position);
//		position.x += 4.0f;
//		parkWorld.CreateCurvedGeometryCylinder(position);
		position.Set(0.0f,4.0f,-5.0f);
		rotation.Set(1.0f,0.0f,1.0f);
		parkWorld.CreateObject("small_plane",position,false,rotation);
#else
		position.Set(0.0f,2.0f,0.0f);
		rotation.Set(0.2f, 0.0f, 0.0f);
		parkWorld.CreateObject("mill",position, false, rotation);

		position.Set(0.4f,5.0f,0.0f);
		rotation.Set(0.2f, 0.0f, 0.0f);
		parkWorld.CreateObject("mill",position, false, rotation);
#endif

		return &parkWorld;
	}

	//////////////////////////
	// 2: Linear stack
	phDemoWorld* CreateLinearStack()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int numHigh = 2;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		position.Set(0.0f,-4.0f,0.0f);
		phDemoWorld& linearWorld = *CreateNewDemoWorld("Linear Stack",NULL,position,100,20,20,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,1*1024*1024,256);		
		Vector3 boxSize(1.0f,1.0f,1.0f);
		numHigh = (int)(8.0f*m_DemoScale);
		{
			int iBox;
			for( iBox = 0; iBox < numHigh; iBox++ )
			{
				linearWorld.CreateBox( boxSize, 1.0f, Vector3( 0, iBox+position.y, 0), true );
			}
		}

		return &linearWorld;
	}

	/////////////////////////
	// 3: Jenga stack on a plane
	phDemoWorld* CreateJenga()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int numHigh = 2;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& jengaWorld = *CreateNewDemoWorld("Jenga (tm)",NULL,ORIGIN,100,20,20,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,1*1024*1024,256);
		numHigh = (int)(2.0f*m_DemoScale);
		int jengaLevelSize = 3;
		position.Set(0.0f,0.0f,0.0f);
		jengaWorld.ConstructJenga(position,numHigh,jengaLevelSize,"longer_crate");

		return &jengaWorld;
	}

	///////////////////////////////////////////
	// 4: pyramid of boxes and a sphere on a plane
	phDemoWorld* CreatePyramid()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int numHigh = 2;
		bool rotate = true;
		bool alignBottom = true;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& bowlingWorld = *CreateNewDemoWorld("Bowling",NULL,ORIGIN,100,20,20,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,1*1024*1024,256);
		position.Set(-2.0f,0.0f,0.0f);
		numHigh = (int)(5.0f*m_DemoScale);
		rotate = false;
		bowlingWorld.ConstructPyramid(position,numHigh,rotate);
		position.Set(3.0f,0.0f,0.0f);
		bowlingWorld.CreateObject("sphere_122",position,alignBottom);

		return &bowlingWorld;
	}

	/////////////////////////////////////////////////
	// 6: many icosahedrons on the splash-shaped terrain
	phDemoWorld* CreateSplash()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		bool alignBottom = true;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& splashWorld = *CreateNewDemoWorld("Splash BVH", "splashbvhwprim",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);
		position.Set(0.0f,-5.0f,0.0f);
		for (int icos = 0; icos < 50; ++icos)
		{
			position.x = random.GetRanged(-7.0f, 7.0f);
			position.y = 3.0f + icos * 1.0f;
			position.z = random.GetRanged(-7.0f, 7.0f);
            rotation.x = random.GetRanged(-PI, PI);
            rotation.y = random.GetRanged(-PI, PI);
            rotation.z = random.GetRanged(-PI, PI);
			const bool alwaysActive = false;
			const bool startActive = true;
			const Functor0Ret<phInst*> createFunc = NULL;
			const bool uniqueArchetype = false;
			phDemoObject* object = splashWorld.CreateObject("escalade",position,alignBottom,rotation,alwaysActive,startActive,createFunc,uniqueArchetype);
            Assert(object->GetPhysInst()->GetArchetype()->GetBound()->GetType() == phBound::GEOMETRY);
            phBoundGeometry* bound = static_cast<phBoundGeometry*>(object->GetPhysInst()->GetArchetype()->GetBound());
            float margin = random.GetRanged(0.02f, 0.368f);
            float polyOrVert = random.GetRanged(0.0f, 1.0f);
            bound->SetMarginAndShrink(margin, polyOrVert);
            bound->PostLoadCompute();
			splashWorld.GetPhLevel()->UpdateObjectLocationAndRadius(object->GetPhysInst()->GetLevelIndex(), (Mat34V_Ptr)(NULL));
		}

		return &splashWorld;
	}


	static int s_RandomSeed;
	phDemoWorld* CreateMillsOnSplash()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		bool alignBottom = false;
		Displayf("Using random seed %d", s_RandomSeed);
		mthRandom random(s_RandomSeed);
		++s_RandomSeed;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& splashWorld = *CreateNewDemoWorld("Splash BVH", "splashbvhwprim",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);
		position.Set(0.0f,-5.0f,0.0f);
		for (int icos = 0; icos < 40; ++icos)
		{
			position.x = random.GetRanged(-7.0f, 7.0f);
			position.y = 3.0f + icos * 1.0f;
			position.z = random.GetRanged(-7.0f, 7.0f);
			rotation.x = random.GetRanged(-PI, PI);
			rotation.y = random.GetRanged(-PI, PI);
			rotation.z = random.GetRanged(-PI, PI);
			const bool alwaysActive = false;
			const bool startActive = true;
			const Functor0Ret<phInst*> createFunc = NULL;
			const bool uniqueArchetype = false;
			splashWorld.CreateObject("mill",position,alignBottom,rotation,alwaysActive,startActive,createFunc,uniqueArchetype);
		}

		return &splashWorld;
	}


	/////////////////////////////////////////////////
	// 6: many icosahedrons on the splash-shaped terrain
	phDemoWorld* CreateBVHOnBVH()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		bool alignBottom = true;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& splashWorld = *CreateNewDemoWorld("Splash BVH", "splashbvh",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);
		position.Set(0.0f,-5.0f,0.0f);
		for (int icos = 0; icos < 50; ++icos)
		//for (int icos = 0; icos < 1; ++icos)
		{
			position.x = random.GetRanged(-7.0f, 7.0f);
			position.y = 3.0f + icos * 1.0f;
			position.z = random.GetRanged(-7.0f, 7.0f);
			rotation.x = random.GetRanged(-PI, PI);
			rotation.y = random.GetRanged(-PI, PI);
			rotation.x = random.GetRanged(-PI, PI);
			const bool alwaysActive = false;
			const bool startActive = true;
			const Functor0Ret<phInst*> createFunc = NULL;
			const bool uniqueArchetype = false;
			/*phDemoObject* object = */splashWorld.CreateObject("escaladebvh",position,alignBottom,rotation,alwaysActive,startActive,createFunc,uniqueArchetype);
#if 0
			Assert(object->GetPhysInst()->GetArchetype()->GetBound()->GetType() == phBound::GEOMETRY);
			phBoundGeometry* bound = static_cast<phBoundGeometry*>(object->GetPhysInst()->GetArchetype()->GetBound());
#if !PHYSLEVEL_OPT_CACHE
			float oldRadius = bound->GetRadiusAroundCentroid();
#endif
			float margin = random.GetRanged(0.02f, 0.368f);
			float polyOrVert = random.GetRanged(0.0f, 1.0f);
			bound->SetMarginAndShrink(margin, polyOrVert);
			bound->PostLoadCompute();
#if !PHYSLEVEL_OPT_CACHE
			float newRadius = bound->GetRadiusAroundCentroid();
			if (newRadius != oldRadius)
			{
				splashWorld.GetPhLevel()->UpdateObjectLocationAndRadius(object->GetPhysInst()->GetLevelIndex(), oldRadius);
			}
#else
			splashWorld.GetPhLevel()->UpdateObjectLocationAndRadius(object->GetPhysInst()->GetLevelIndex(), (Mat34V_Ptr)(NULL));
#endif
#endif
		}

		return &splashWorld;
	}


	///////////////////////////////////////////////////
	// 7: heavy objects on top of light objects in a plane
	phDemoWorld* CreateHeavyOnLight()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& heavyLightWorld = *CreateNewDemoWorld("Heavy on light");
		heavyLightWorld.CreateBox(Vector3(1.0f, 1.0f, 1.0f),0.1f,Vector3(0.0f, 0.5f, 0.0f));
		heavyLightWorld.CreateBox(Vector3(1.0f, 1.0f, 1.0f),1.0f,Vector3(0.0f, 1.5f, 0.0f));
		heavyLightWorld.CreateBox(Vector3(1.0f, 1.0f, 1.0f),0.1f,Vector3(2.0f, 0.5f, 0.0f));
		heavyLightWorld.CreateBox(Vector3(1.0f, 1.0f, 1.0f),1.0f,Vector3(2.0f, 1.5f, 0.0f));

		return &heavyLightWorld;
	}

	//////////////////////////////////////////////////////////////////////////
	// 8: a low wall
	phDemoWorld* CreateLowWall()
	{
		int maxOctreeNodes = 1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int maxInstBehaviors = 16;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		phDemoWorld& lowWallWorld = *rage_new phDemoWorld("Shao Chen");
		lowWallWorld.Init(maxOctreeNodes,maxActiveObjects,maxObjects,VECTOR3_TO_VEC3V(worldMin),VECTOR3_TO_VEC3V(worldMax),maxInstBehaviors);
		lowWallWorld.ConstructTerrainPlane();
		const float ratio = 1.618034f;
		const float basesize = 0.5f;
		lowWallWorld.CreateObject("sphere_056",Vector3(0.0f,0.0f,-3.0f),true);
		int nLength = int(m_DemoScale*5.0f);
		int nWall = 1 + (int)(3/m_DemoScale);
	#if __XENON || __PS3
		const int wallHeight = 7;
	#else
		const int wallHeight = 2;
	#endif
		int iWall;
		for( iWall = 0; iWall < nWall; iWall++ )
		{
			for (int i = -nLength/2; i < nLength/2; i++ )
			{
				float x = i * (basesize*ratio + 0.1f);
				for( int j = 0; j < wallHeight; j++ )
				{
					float h = basesize*0.5f + basesize*j;
					x += (( j % 2 ) == 0 ) ? -basesize*ratio*0.33f : basesize*ratio*0.33f;
					lowWallWorld.CreateBox(Vector3(basesize*ratio, basesize, basesize+(j*0.01f)),1.0f,Vector3(x, h, float(iWall)*2.0f));
				}
			}
		}

		return &lowWallWorld;
	}

	///////////////////////////////////////////////////////////////////////
	// 9: a bunch of with different frictions sliding down a plane
	phDemoWorld* CreateFriction()
	{
		int maxOctreeNodes = 1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		int maxInstBehaviors = 16;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		Matrix34 worldTilt = CreateRotatedMatrix(Vector3(0.0f, -6.0f, -10.0f), Vector3(0.15f, 0.0f, 0.0f));		
		phDemoWorld& frictionWorld = *rage_new phDemoWorld("Friction");
		frictionWorld.Init(maxOctreeNodes,maxActiveObjects,maxObjects,VECTOR3_TO_VEC3V(worldMin),VECTOR3_TO_VEC3V(worldMax),maxInstBehaviors);
		frictionWorld.CreateFixedObject("plane_complex", worldTilt);
		Vector3 boxSize(1.0f, 1.0f, 1.0f);
		Vector3 center(-21.7f, 2.0f, -1.0f);
		float friction = 0.5f;
		static const int NUM_FRICTION_BOXES = 14;
		static char boxNames[NUM_FRICTION_BOXES][256];
		for (int boxIndex = 0; boxIndex < NUM_FRICTION_BOXES; ++boxIndex)
		{
			center.x += 3.0f;
			Matrix34 boxMatrix = worldTilt;
			worldTilt.Transform(center, boxMatrix.d);
			float density = 3.0f;
			phDemoObject* boxObject = frictionWorld.CreateBox(boxSize, density, boxMatrix);
			sprintf(boxNames[boxIndex], "%f", friction);
			boxNames[boxIndex][5] = '\0';
			boxObject->SetLabel(boxNames[boxIndex]);
			phInst* boxInst = boxObject->GetPhysInst();
			phBound* boxBound = boxInst->GetArchetype()->GetBound()->Clone();
			char name[256];
			sprintf(name, "friction_box_%f", friction);
			phArchetype* boxArchetype = ARCHMGR.RegisterArchetype(boxBound, name, true, phArchetype::ARCHETYPE_DAMP, density);
			boxInst->SetArchetype(boxArchetype);

			phMaterialMgr::Id id = MATERIALMGRFLAG.FindMaterialId(name);
			if (id == phMaterialMgr::MATERIAL_NOT_FOUND)
			{
				phMaterial& boxMaterial = MATERIALMGRFLAG.AllocateMaterial(name);
				boxMaterial.SetFriction(friction);
				boxMaterial.SetElasticity(0.0f);
				id = MATERIALMGRFLAG.GetMaterialId(boxMaterial);
			}
			
			boxBound->SetMaterial(id);
			friction += 0.02f;
		}

		return &frictionWorld;
	}

	///////////////////////////////////////////////////////////////////////
	// 10: a bunch of objects blown around by invisible forces
	phDemoWorld* CreateHurricane()
	{
#if 1
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		mthRandom random;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		position.Set(117.f, -2.f, -773.f);
		rotation.Set(-0.5f * PI, 0.0f, 0.0f);
		Matrix34 terrainMtx(CreateRotatedMatrix(position, rotation));
        phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane","manhatbvh",terrainMtx);
        position.Set(87.f, -2.f, -773.f);
        hurricaneWorld.CreateFixedObject("manhatbvh", position, false, rotation);

		rotation.Set(0.0f, 0.0f, 0.0f);
		position.Set(10.0f, 2.2f, 10.0f);
//		hurricaneWorld.CreateFixedObject("composite_bvh",position,false,rotation);
		position.Set(-10.0f, 2.2f, 10.0f);
//		hurricaneWorld.CreateFixedObject("composite_bvh",position,false,rotation);
		position.Set(10.0f, 0.8f, -12.0f);
//		hurricaneWorld.CreateFixedObject("composite_bvh",position,false,rotation);
		position.Set(-10.0f, 0.8f, -12.0f);
//		hurricaneWorld.CreateFixedObject("composite_bvh",position,false,rotation);
	#if __XENON || __PS3
		const int numIcosahedrons = 128;
	#else
		const int numIcosahedrons = 128;
	#endif
        for (int icos = 0; icos < numIcosahedrons; ++icos)
        {
            position.x = random.GetRanged(-18.0f, 18.0f);
            position.y = random.GetRanged(6.0f, 9.0f); 
            position.z = random.GetRanged(-18.0f, 18.0f);
            float period = random.GetRanged(1.0f, 3.0f);
            float phase = random.GetRanged(1.0f, 5.0f);
            float mag;
            const char* object;
            if (random.GetRanged(0, 4) == 0)
            {
                object = "mill";
                mag = random.GetRanged(20000.0f, 60000.0f);
            }
            else
            {
                object = "icosahedron";
                mag = random.GetRanged(800.0f, 1600.0f);
            }

            hurricaneWorld.CreateWhirlingObject(object,position,Vector3(0.0f, 0.0f, 0.0f), period, mag, phase );
        }

		return &hurricaneWorld;
#else
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		position.Set(117.f, -2.f, -773.f);
		rotation.Set(-0.5f * PI, 0.0f, 0.0f);
		Matrix34 terrainMtx(CreateRotatedMatrix(position, rotation));
//		phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane","manhatbvh",terrainMtx);
//		phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane","plane_complex");
//		phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane");
		phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane", "plane_bvh_thin", Vector3(10.0f, 0.0f, 0.0f));
//		phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane", "plane_distant", Vector3(-100.0f, 0.0f, -100.0f));
//		phDemoWorld& hurricaneWorld = *CreateNewDemoWorld("Hurricane", "plane_bumpy");
		position.Set(87.f, -2.f, -773.f);
//		hurricaneWorld.CreateFixedObject("manhatbvh", position, false, rotation);

//		position.Set(1.0f,10.0f,2.0f);
//		position.Set(2.0f,10.0f,2.0f);
		//rotation.Set(0.0f,0.0f,PI*0.5f);
//		rotation.Set(0.0f,0.0f,0.0f);
//		hurricaneWorld.CreateObject("mill",position,false,rotation);
//		position.Set(1.0f,13.0f,2.0f);
//		hurricaneWorld.CreateObject("mill",position,false,rotation);
		//hurricaneWorld.CreateObject("car_body",position,false,rotation);
		//hurricaneWorld.CreateObject("escalade",position,false,rotation);
		//hurricaneWorld.CreateObject("box_med",position,false,rotation);
		//hurricaneWorld.CreateObject("box_222",position,false,rotation);
		position.Set(-40.0f, 10.0f, 0.0f);
		hurricaneWorld.CreateObject("sphere_122",position,false,rotation);

		return &hurricaneWorld;
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// 11: A fast moving box collision
	phDemoWorld* CreateBoxOnBoxWorld()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);

		position.Set(0.0f,0.0f,0.0f);
		phDemoWorld& boxWorld = *CreateNewDemoWorld("Box on box",NULL,position,100,20,20,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,1*1024*1024,256);		

		Vector3 boxSize(1.0f,1.0f,1.0f);

		// Create a box on the floor
		// Comment out for box vs ground
		boxWorld.CreateBox( boxSize, 1.0f, Vector3( 0, position.y, 0), true );

		position.y += 3.0f;	// Initial drop height

		Vector3 velocity(0.0f,-20.0f,0.0f);
		Vector3 angVelocity(0.0,0.0f,5.0f);

		phDemoObject* object = boxWorld.CreateBox( boxSize, 1.0f, Vector3( 0, position.y, 0), true );

		// Activate the object
		if(PHLEVEL->IsInactive(object->GetPhysInst()->GetLevelIndex()))
		{
			PHSIM->ActivateObject(object->GetPhysInst()->GetLevelIndex(),PHSIM->GetCollider(object->GetPhysInst()));
		}
		phCollider* pCollider = PHSIM->GetCollider(object->GetPhysInst());
		Assert(pCollider);

		pCollider->SetVelocity(velocity);
		pCollider->SetAngVelocity(angVelocity);


		return &boxWorld;
	}


	phDemoWorld* CreateRubik3World()
	{
#if 0
		{
			// Create a higher-res splash bound.
			const int gridCellResolution = 64;	// Number of grid cells on a side.
			const int halfGridCellResolution = (gridCellResolution >> 1);

			phBoundBVH *splashBVHBound = rage_new phBoundBVH;
			const int numVerts = (gridCellResolution + 1) * (gridCellResolution + 1);
			const int numTris = 2 * gridCellResolution * gridCellResolution;
			splashBVHBound->Init(numVerts, 1, 0, numTris, 0);
			splashBVHBound->SetMaterialId(0, phMaterialMgr::DEFAULT_MATERIAL_ID);

			// Set up the vertices.
			const float boundSize = 50.0f;
			const float boundHalfSize = 0.5f * boundSize;
			const float indexNormalizer = 1.0f / gridCellResolution;
			Vec3V *vertexArray = rage_new Vec3V[numVerts];
			int xIndex = 0;
			float xIndexAsFloat = 0.0f;
			for(; xIndex < gridCellResolution + 1; ++xIndex, xIndexAsFloat += 1.0f)
			{
				int yIndex = 0;
				float yIndexAsFloat = 0.0f;
				for(; yIndex < gridCellResolution + 1; ++yIndex, yIndexAsFloat += 1.0f)
				{
					const float xCoordinate = xIndexAsFloat * indexNormalizer * boundSize - boundHalfSize;
					const float zCoordinate = yIndexAsFloat * indexNormalizer * boundSize - boundHalfSize;
					const float distanceFromOrigin = sqrtf(square(xCoordinate) + square(zCoordinate));
					const float height = 2.5f * sinf(0.4f * distanceFromOrigin);
					Vec3V newVertex(xCoordinate, height, zCoordinate);
					const int vertexIndex = (gridCellResolution + 1) * yIndex + xIndex;
					vertexArray[vertexIndex] = newVertex;
				}
			}

			splashBVHBound->CalculateBoundingBox(&RCC_VECTOR3(vertexArray[0]));
			splashBVHBound->CalculateQuantizationValues();

			for(int vertexIndex = 0; vertexIndex < numVerts; ++vertexIndex)
			{
				splashBVHBound->SetVertex(vertexIndex, vertexArray[vertexIndex]);
			}

			// Set up the polygons.
			int curPolygonIndex = 0;
			for(int xPolygonIndex = 0; xPolygonIndex < gridCellResolution; ++xPolygonIndex)
			{
				for(int yPolygonIndex = 0; yPolygonIndex < gridCellResolution; ++yPolygonIndex)
				{
					phPolygon tempPoly;
					const int baseVertexIndex = (gridCellResolution + 1) * yPolygonIndex + xPolygonIndex;
					const bool alternateSplitIndex = (xPolygonIndex < halfGridCellResolution) != (yPolygonIndex < halfGridCellResolution);

					const int quadIndex0 = baseVertexIndex;
					const int quadIndex1 = baseVertexIndex + 1;
					const int quadIndex2 = baseVertexIndex + (gridCellResolution + 1) + 1;
					const int quadIndex3 = baseVertexIndex + (gridCellResolution + 1);
					{
						const int vertexIndex0 = quadIndex0;
						const int vertexIndex1 = alternateSplitIndex ? quadIndex2 : quadIndex3;
						const int vertexIndex2 = quadIndex1;
						tempPoly.InitTriangle((phPolygon::Index)vertexIndex0, (phPolygon::Index)vertexIndex1, (phPolygon::Index)vertexIndex2, splashBVHBound->GetVertex(vertexIndex0), splashBVHBound->GetVertex(vertexIndex1), splashBVHBound->GetVertex(vertexIndex2));
						splashBVHBound->SetPolygon(curPolygonIndex, tempPoly);
						splashBVHBound->SetPolygonMaterialIndex(curPolygonIndex, 0);
					}

					{
						const int vertexIndex0 = quadIndex3;
						const int vertexIndex1 = quadIndex2;
						const int vertexIndex2 = alternateSplitIndex ? quadIndex0 : quadIndex1;
						tempPoly.InitTriangle((phPolygon::Index)vertexIndex0, (phPolygon::Index)vertexIndex1, (phPolygon::Index)vertexIndex2, splashBVHBound->GetVertex(vertexIndex0), splashBVHBound->GetVertex(vertexIndex1), splashBVHBound->GetVertex(vertexIndex2));
						splashBVHBound->SetPolygon(curPolygonIndex + 1, tempPoly);
						splashBVHBound->SetPolygonMaterialIndex(curPolygonIndex + 1, 0);
					}

					curPolygonIndex += 2;
				}
			}

//			splashBVHBound->Build(NULL, 4);
			splashBVHBound->Save("physics\\splashhires\\bound.bnd");
		}
#endif
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);
		//phDemoWorld& rubiksWorld = *CreateNewDemoWorld("Splash BVH", "splashbvhwprim",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);
		phDemoWorld& rubiksWorld = *CreateNewDemoWorld("Splash BVH", "splashhires",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);

		Vector3 position,rotation;
		//		bool alignBottom = true;
				mthRandom random;

#if 0
		float yPosition = 6.0f;
		rotation.Set(0.2f, 0.0f, 0.0f);
		for(int objectIndex = 0; objectIndex < 50; ++objectIndex)
		{
			float xCoordinate = random.GetRanged(-5.0f, 5.0f);
			float zCoordinate = random.GetRanged(-5.0f, 5.0f);
			position.Set(xCoordinate, yPosition, zCoordinate);
			rubiksWorld.CreateObject("rubikcube3",position, false, rotation);

			yPosition += 2.0f;
		}
#else
		// This section results in some instability so I'm saving off this to look at later.  It might just be because it's a ridiculously large stack of
		//   complicated objects though.
		float yPosition = 6.0f;
		rotation.Set(0.2f, 0.0f, 0.0f);
		for(int objectIndex = 0; objectIndex < 50; ++objectIndex)
		{
			position.Set(0.0f, yPosition, 0.0f);
			rubiksWorld.CreateObject("rubikcube3",position, false, rotation);

			yPosition += 2.1f;
		}
#endif

		return &rubiksWorld;
	}


	phDemoWorld* CreateRubik4World()
	{
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		const Vector3 DEFAULT_WORLD_MAX( 999.0f,  999.0f,  999.0f);
		const Vector3 DEFAULT_WORLD_MIN(-999.0f, -999.0f, -999.0f);
		//phDemoWorld& rubiksWorld = *CreateNewDemoWorld("Splash BVH", "splashbvhwprim",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);
		phDemoWorld& rubiksWorld = *CreateNewDemoWorld("Splash BVH", "splashhires",ORIGIN,100,256,256,DEFAULT_WORLD_MIN,DEFAULT_WORLD_MAX,0,2*1024*1024,1024);

		Vector3 position,rotation;
		//		bool alignBottom = true;
		mthRandom random;

#if 1
		float yPosition = 6.0f;
		rotation.Set(0.2f, 0.0f, 0.0f);
		for(int objectIndex = 0; objectIndex < 50; ++objectIndex)
		{
			float xCoordinate = random.GetRanged(-5.0f, 5.0f);
			float zCoordinate = random.GetRanged(-5.0f, 5.0f);
			position.Set(xCoordinate, yPosition, zCoordinate);
			rubiksWorld.CreateObject("rubikcube4",position, false, rotation);

			yPosition += 2.0f;
		}
#else
		// This section results in some instability so I'm saving off this to look at later.  It might just be because it's a ridiculously large stack of
		//   complicated objects though.
		float yPosition = 6.0f;
		rotation.Set(0.2f, 0.0f, 0.0f);
		for(int objectIndex = 0; objectIndex < 50; ++objectIndex)
		{
			position.Set(0.0f, yPosition, 0.0f);
			rubiksWorld.CreateObject("rubikcube3",position, false, rotation);

			yPosition += 2.0f;
		}
#endif

		return &rubiksWorld;
	}


	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,5.0f,10.0f);
		lookFrom.Add(phDemoObject::GetWorldOffset());
		Vector3 lookTo(0.0f,0.0f,0.0f);
		lookTo.Add(phDemoObject::GetWorldOffset());
		InitSampleCamera(lookFrom,lookTo);
	}

	static int s_FrameToBreakOn;
	virtual void Update ()
	{
		if(!IsPaused())
		{
			++m_FrameCounter;
			if(m_FrameCounter >= s_FrameToBreakOn)
			{
				Assert(false);
			}
		}
		physicsSampleManager::Update();
	}

	float m_DemoScale;
	int m_FrameCounter;
};

int contactsSampleManager::s_RandomSeed = 1;
int contactsSampleManager::s_FrameToBreakOn = 158;

} // namespace ragesamples


// main application
int Main()
{
	{
		ragesamples::contactsSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
