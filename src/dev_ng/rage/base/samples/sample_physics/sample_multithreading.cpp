//
// sample_physics/sample_multithreading.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Multi-Threading Physics
// PURPOSE:
//		This sample tests physics running on multiple threads simultaneously. For now it just simulates multithreading by running multiple physics tests broken up into
//		pieces on a single thread. Later it will demonstrate multi-threading physics on Xenon and PS3.


#include "bank/bkmgr.h"
#include "diag/output.h"
#include "file/asset.h"
#include "grmodel/setup.h"
#include "input/mouse.h"
#include "math/random.h"
#include "phbound/bound.h"
#include "phcore/materialmgr.h"
#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "phsolver/contactmgr.h"
#include "physics/collider.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/rageroot.h"
#include "system/task.h"
#include "system/timemgr.h"
#include "vectormath/vec3v.h"


PARAM(file, "Reserved for future expansion, not currently hooked up");
PARAM(numobjects, "the number of active objects to use");


namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);
EXT_PFD_DECLARE_ITEM(Active);
EXT_PFD_DECLARE_ITEM(Inactive);
EXT_PFD_DECLARE_ITEM(Fixed);

}

namespace ragesamples {

using namespace rage;




const int NumShapesPerTypePerTask = 8;
static const int NumShapesPerType = phShapeTestTaskManager::s_NumShapeTestTasks*NumShapesPerTypePerTask;
const int NumBatchedProbes = 12;
const int NumBatchedSpheres = 12;
const int NumBatchedCapsules = 12;

#define	MULTITHREAD_MAX_ACTIVE_OBJECTS	256
#define MULTITHREAD_DEFAULT_NUM_OBJECTS	2

////////////////////////////////////////////////////////////////
// 

void InitializeRandomProbe (mthRandom& random, const Vector3& boxMin, const Vector3& boxMax, phSegmentV& segment)
{
#if 0
	Vec3V probeStart = VECTOR3_TO_VEC3V(Vector3(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z)));
	Vec3V probeEnd = VECTOR3_TO_VEC3V(Vector3(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z)));
#else
	// This code is here to make the probes probe in a manner that is likely to actually hit the 'large' bound involved (probably a BVH).
	// Without this change, the probes never seemed to hit the BVH so it's not really testing that aspect of the functionality.
	Vec3V probeStart = VECTOR3_TO_VEC3V(Vector3(random.GetRanged(boxMin.x,boxMax.x),boxMax.y + 0.1f,random.GetRanged(boxMin.z,boxMax.z)));
	Vec3V probeEnd = VECTOR3_TO_VEC3V(Vector3(random.GetRanged(boxMin.x,boxMax.x),boxMin.y - 0.1f,random.GetRanged(boxMin.z,boxMax.z)));
#endif
	segment.Set(probeStart,probeEnd);
}


void InitializeRandomSphere (mthRandom& random, const Vector3& boxMin, const Vector3& boxMax, float radiusMin, float radiusMax, Vector3& worldCenter, float& radius)
{
	worldCenter.Set(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z));
	if (random.GetBool())
	{
		radius = random.GetRanged(radiusMin,radiusMax);
	}
	else
	{
		radius = 0.0f;
	}
}


void InitializeRandomCapsule (mthRandom& random, const Vector3& boxMin, const Vector3& boxMax, float radiusMin, float radiusMax, phSegmentV& segment, ScalarV_InOut radius, bool& swept)
{
	Vector3 capsuleStart(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z));
	Vector3 capsuleEnd(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z));
	segment.Set(RCC_VEC3V(capsuleStart),RCC_VEC3V(capsuleEnd));
	radius = ScalarVFromF32(random.GetRanged(radiusMin,radiusMax));
	swept = random.GetBool();
}


void InitializeRandomBatch (mthRandom& random, const Vector3& batchBoxMin, const Vector3& batchBoxMax, float radiusMin, float radiusMax,
							phShapeTest<phShapeBatch>& batchTest,phIntersection* probeIsectList, phIntersection* sphereIsectList, phIntersection* capsuleIsectList)
{
	// Get a reference to the batched shape test shape.
	phShapeBatch& batch = *static_cast<phShapeBatch*>(&batchTest.GetShape());

	// Initialize a random number of random probes in the batch.
	int numProbes = random.GetRanged(0,NumBatchedProbes);
	batch.AllocateProbes(numProbes);
	for (int probeIndex=0; probeIndex<numProbes; probeIndex++)
	{
		phSegmentV segment;
		InitializeRandomProbe(random,batchBoxMin,batchBoxMax,segment);
		batchTest.InitProbe(segment,&probeIsectList[probeIndex]);
	}

	// Initialize a random number of random spheres in the batch.
	int numSpheres = random.GetRanged(0,NumBatchedSpheres);
	batch.AllocateSpheres(numSpheres);
	for (int sphereIndex=0; sphereIndex<numSpheres; sphereIndex++)
	{
		Vector3 worldCenter;
		float radius;
		InitializeRandomSphere(random,batchBoxMin,batchBoxMax,radiusMin,radiusMax,worldCenter,radius);
		if (radius>0.0f)
		{
			batchTest.InitSphere(worldCenter,radius,&sphereIsectList[sphereIndex]);
		}
		else
		{
			batchTest.InitPoint(worldCenter,&sphereIsectList[sphereIndex]);
		}
	}

	// Initialize a random number of random capsules in the batch, making sure the batch isn't empty.
	int minCapsules = (numProbes+numSpheres==0 ? 1: 0);
	int numCapsules = random.GetRanged(minCapsules,NumBatchedCapsules);
	batch.AllocateCapsules(numCapsules);
	for (int capsuleIndex=0; capsuleIndex<numCapsules; capsuleIndex++)
	{
		phSegmentV segment;
		ScalarV radius;
		bool swept;
		InitializeRandomCapsule(random,batchBoxMin,batchBoxMax,radiusMin,radiusMax,segment,radius,swept);
		if (swept)
		{
			batchTest.InitSweptSphere(segment,radius,&capsuleIsectList[capsuleIndex]);
		}
		else
		{
			batchTest.InitCapsule(segment,radius,&capsuleIsectList[capsuleIndex]);
		}
	}
}

phShapeTestTaskManager shapeTestTaskManager;

class threadedPhysicsSampleManager : public grcSampleManager
{
public:
	void InitClient()
	{
		// Set the sample to run at 30 frames per second of simulation time. The apparent speed will be faster or slower depending on
		// the time needed to compute one frame. TIME.SetRealTimeMode(30.0f) would fix the apparent speed at 30 fps and vary the
		// time spent computing each frame.
		TIME.SetFixedFrameMode(30.0f);

		phConfig::EnableRefCounting();

		m_AddAndDeleteObjects = false;

		m_ProbeTestList = rage_new phShapeTest<phShapeProbe>[NumShapesPerType];
		m_SphereTestList = rage_new phShapeTest<phShapeSphere>[NumShapesPerType];
		m_CapsuleTestList = rage_new phShapeTest<phShapeCapsule>[NumShapesPerType];
		m_BatchTestList = rage_new phShapeTest<phShapeBatch>[NumShapesPerType];
		m_ProbeIsectList = rage_new phIntersection[NumShapesPerType];
		m_SphereIsectList = rage_new phIntersection[NumShapesPerType];
		m_CapsuleIsectList = rage_new phIntersection[NumShapesPerType];
		for (int index=0; index<NumShapesPerType; index++)
		{
			m_BatchedProbeIsectList[index] = rage_new phIntersection[NumBatchedProbes];
			m_BatchedSphereIsectList[index] = rage_new phIntersection[NumBatchedSpheres];
			m_BatchedCapsuleIsectList[index] = rage_new phIntersection[NumBatchedCapsules];
		}

#if __PFDRAW
		// STEP #1. Initialize profile drawing.
		//-- Profile drawing is used to draw physics bounds. It is useful in games to diagnose any problems
		// involving interactions with physical objects. __PFDRAW is defined in rage/base/src/profile/drawcore.h,
		// and is normally on in debug and beta builds.

		// Set the number of bytes available for storing profile draw shapes (objects for drawing are collected during the
		// physics updates and drawn together), and initialize the buffer and widgets for profile drawing.
		const int pfDrawBufferSize = 2000000;
		GetRageProfileDraw().Init(pfDrawBufferSize);

		// Enable profile drawing.
		const bool pfDrawEnabled = true;
		GetRageProfileDraw().SetEnabled(pfDrawEnabled);

		// Turn on some drawing groups. Profile drawing is turned off by default, and in release builds it is compiled out.
		// It should only be turned on when needed to solve problems. For this example, it is necessary because some of the
		// objects do not have graphics models.
		PFD_GROUP_ENABLE(Physics, true);
		PFD_GROUP_ENABLE(Bounds, true);
		PFD_ITEM_ENABLE(Solid, true);
		PFD_ITEM_ENABLE(Wireframe, false);
#endif


		// STEP #2. Create and initialize the physics level.
		//-- The physics level (phLevel) keeps track of all the physical objects in the game. It uses a spatial partitioning scheme
		// to speed up queries about object location. It can be used to quickly find objects that are hit by a line segment, or
		// touch a sphere or capsule.
		phLevelNew::SetActiveInstance(rage_new phLevelNew);

		// Set the size of the physics level.
		// When any object goes outside the physics level, it is no longer updated, and virtual phInst::NotifyOutOfWorld() is called.
		// Any object that is added to the physics level outside the extents will cause an assert failure.
		const Vec3V worldExtentsMin(-999.0f,-999.0f,-999.0f);
		const Vec3V worldExtentsMax(999.0f,999.0f,999.0f);
		PHLEVEL->SetExtents(worldExtentsMin,worldExtentsMax);

		// Set the maximum allowed number of physical objects in the physics level. This normally varies from hundreds for a small
		// sample level to hundreds of thousands for a large game level.
		// Exceeding the maximum number of objects causes an assert failure.
		int maxNumObjects = 500;
		PHLEVEL->SetMaxObjects(maxNumObjects);

		// Set the maximum allowed number of physically active objects in the physics level. Physically active means the objects have
		// colliders and are reacting to collisions. This number is normally in the low hundreds.
		// Manually exceeding the maximum number of active objects (by calling AddActiveObject) causes an assert failure. If the maximum number
		// of active objects would be exceeded by the physics simulator activating objects, such as from a collision, then the object will not
		// become active.
		const int maxNumActiveObjects = 100;
		PHLEVEL->SetMaxActive(maxNumActiveObjects);

		// Set the maximum number of occupied nodes in the physics level's octree.
		// An assert failure results if the physics level tries to make more than the maximum number of octree nodes. This will be changed
		// soon to print a warning and not create the needed node instead (17 March 06).
		const int maxOctreeNodes = 1000;
		PHLEVEL->SetNumOctreeNodes(maxOctreeNodes);

		// Create and initialize the physics level's octree and object information, using the parameters set above.
		PHLEVEL->Init();


		// STEP #3. Create and initialize the simulator.
		//-- The physics simulator computes the locations, orientations, and speeds of objects in the physics level,
		// and handles collision detection and responses.

		// Call the static initialization of the simulator.
		phSimulator::InitClass();

		// Create the simulator and set it as the current active simulator, accessible by calling PHSIM->.
		phSimulator::SetActiveInstance(rage_new phSimulator);

		// Initialize the simulator with the currently active physics level, and the maximum number of managed active objects. This can be different
		// from the physics level's maximum number of active objects because the simulator's number only includes active objects that are managed
		// by the simulator. Users have the freedom to add physically active objects to the physics level with colliders that are not managed by
		// the simulator. This is normally used for derived colliders, such as vehicles and ragdolls.
		phSimulator::InitParams params;
		params.maxManagedColliders = maxNumActiveObjects;
		PHSIM->Init(PHLEVEL,params);


		// STEP #4. Initialize the material manager.
		//-- Physics bounds contain physics materials to define their properties for collisions (friction and elasticity) and other effects (such as sounds
		// and driving properties). Materials are shared among bounds and stored in the material manager.
		ASSET.PushFolder("materials");
		phMaterialMgrImpl<phMaterial>::Create();
		MATERIALMGR.Load();
		phMaterialMgrImpl<phSurface, phMaterialMgrFlag>::Create();
		MATERIALMGRFLAG.Load(64);
		ASSET.PopFolder();


		// STEP #5. Create the terrain and put it in the physics level.
		//-- Load a file called "bound.bnd" from the "assets/physics/big_plane" folder. It has information about the physical shape of an instance
		// that will be used as the terrain. It is a large horizontal square made of two triangles.
		phBound* terrainBound = phBound::Load("physics/splashbvh/bound");
		Assert(terrainBound);

		// Set the terrain bound in the terrain archetype. Physics archetypes can hold physical information such as include and type flags, mass
		// and damping, and a pointer to the bound. There is usually a 1-1 correspondence between archetypes and bounds, but it is possible for
		// more than one archetype to share a bound.
		m_TerrainArchetype.SetBound(terrainBound);

		// Set the archetype in the physics instance. The physics level keeps track of objects by physics instances.
		m_TerrainInstance.SetArchetype(&m_TerrainArchetype);

		// Set the instance's matrix. This contains a 3x3 part for orientation and a position.
		Matrix34 instanceMatrix;
		instanceMatrix.Identity();
		m_TerrainInstance.SetMatrix(RCC_MAT34V(instanceMatrix));

		// Insert the terrain instance in the level, so that it will participate in the physics simulation. Since this instance is terrain,
		// AddFixedObject us used, which means that this object is fixed in space.
		PHSIM->AddFixedObject(&m_TerrainInstance);


		// STEP #6. Load two objects and put them in the physics level.
		//-- Load the physical bound, and set it in the archetype, following the same procedure as with the terrain.
		phBound* icosahedronBound = phBound::Load("physics/icosahedron/bound");
//		phBound* icosahedronBound = phBound::Load("physics/dresser/bound");
		m_ActiveArchetype.SetBound(icosahedronBound);

		m_NumObjects = MULTITHREAD_DEFAULT_NUM_OBJECTS;
		if (PARAM_numobjects.Get(m_NumObjects))
		{
			m_NumObjects = Min(m_NumObjects,MULTITHREAD_MAX_ACTIVE_OBJECTS);
		}

		// Initialize both instances of this moving object with the same archetype and bound.
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			m_ActiveInstanceList[objectIndex].SetArchetype(&m_ActiveArchetype);
		}

		// Set the matrices of the two instances.
		instanceMatrix.MakeTranslate(0.0f,3.0,0.0f);
		instanceMatrix.RotateZ(0.1f);
		Vector3 offset(0.05f,1.0,0.05f);
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			m_ActiveInstanceList[objectIndex].SetMatrix(RCC_MAT34V(instanceMatrix));
			m_ResetMatrixList[objectIndex].Set(instanceMatrix);
			instanceMatrix.d.Add(offset);
		}

		// Place the two objects in the physics level. AddActiveObject is used this time, to tell the simulator to give the instances
		// colliders so that they will be simulated as physically active objects.
		// An optional second parameter in AddActiveObject can be used to make the object always active, which means it will never be
		// put in the inactive state.
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			PHSIM->AddActiveObject(&m_ActiveInstanceList[objectIndex]);
		}


		// Map a function keys for resetting the physics world and for grabbing objects with the mouse.
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Reset);
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_LEFT,m_Push);
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_RIGHT,m_Pull);

		// Initialize a set of random shape tests.
		mthRandom random;
		Vector3 boxMax(20.0f,10.0f,20.0f);
		Vector3 boxMin(-20.0f,0.0f,-20.0f);
		float radiusMin = 0.001f;
		float radiusMax = 10.0f;
		for (int shapeIndex=0; shapeIndex<NumShapesPerType; shapeIndex++)
		{
			// Initialize a random probe.
			phSegmentV segment;
			InitializeRandomProbe(random,boxMin,boxMax,segment);
			m_ProbeTestList[shapeIndex].InitProbe(segment,&m_ProbeIsectList[shapeIndex]);

			// Initialize a random sphere.
			Vector3 worldCenter;
			float sphereRadius;
			InitializeRandomSphere(random,boxMin,boxMax,radiusMin,radiusMax,worldCenter,sphereRadius);
			m_SphereTestList[shapeIndex].InitSphere(worldCenter,sphereRadius,&m_SphereIsectList[shapeIndex]);

			// Initialize a random capsule.
			ScalarV radius;
			bool swept;
			InitializeRandomCapsule(random,boxMin,boxMax,radiusMin,radiusMax,segment,radius,swept);
			if (swept)
			{
				m_CapsuleTestList[shapeIndex].InitSweptSphere(segment,radius,&m_CapsuleIsectList[shapeIndex]);
			}
			else
			{
				m_CapsuleTestList[shapeIndex].InitCapsule(segment,radius,&m_CapsuleIsectList[shapeIndex]);
			}

			// Initialize a random batch.
			Vector3 batchBoxMin(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z));
			Vector3 batchBoxMax(random.GetRanged(boxMin.x,boxMax.x),random.GetRanged(boxMin.y,boxMax.y),random.GetRanged(boxMin.z,boxMax.z));
			if (batchBoxMin.x>batchBoxMax.x) SwapEm(batchBoxMin.x,batchBoxMax.x);
			if (batchBoxMin.y>batchBoxMax.y) SwapEm(batchBoxMin.y,batchBoxMax.y);
			if (batchBoxMin.z>batchBoxMax.z) SwapEm(batchBoxMin.z,batchBoxMax.z);
			InitializeRandomBatch(random,batchBoxMin,batchBoxMax,radiusMin,radiusMax,m_BatchTestList[shapeIndex],&m_BatchedProbeIsectList[shapeIndex][0],
									&m_BatchedSphereIsectList[shapeIndex][0],&m_BatchedCapsuleIsectList[shapeIndex][0]);
		}
	}

	void AddWidgetsClient ()
	{
	#if __BANK
		bkBank& bank = BANKMGR.CreateBank("rage - Physics");
		bank.AddToggle("AddAndDeleteObjects",&m_AddAndDeleteObjects);
		bank.PushGroup("Level",false);
		phLevelNew::AddWidgets(bank);
		bank.PopGroup();
		bank.PushGroup("Simulator",false);
		phSimulator::AddWidgets(bank);
		bank.PopGroup();
		bank.PushGroup("Materials",false);
		MATERIALMGR.AddWidgets(bank);
		bank.PopGroup();
	#endif
	}

	void UpdateMouse()
	{
		if (m_Push.IsPressed() || m_Pull.IsPressed())
		{
			// mouseScreen and mouseFar as where are the world space points on the near plane and far plane respectively which project to the current mouse cursor location.
			Vector3 mouseScreen, mouseFar;
			m_Viewport->ReverseTransformNoWorld(static_cast<float>(ioMouse::GetX()),static_cast<float>(ioMouse::GetY()),RC_VEC3V(mouseScreen),RC_VEC3V(mouseFar));
			/*
			grcWorldIdentity();
			grcBegin(drawLines,2);
			grcVertex3fv(&mouseScreen[0]);
			grcVertex3fv(&mouseFar[0]);
			grcEnd();
			*/

			// Establish a world space direction for the mouse click.
			Vector3 direction;
			direction.Subtract(mouseFar, mouseScreen);
			direction.Normalize();

			Vector3 segA, segB;
			segA = mouseScreen;
			segB.AddScaled(mouseScreen, direction, 100.0f);
			phSegment segment;
			segment.Set(segA, segB);
			phIntersection isect;
			if (PHLEVEL->TestProbe(segment, &isect))
			{
				// Do a probe test to apply an impulse.
				phIntersection isect;
				if (PHLEVEL->TestProbe(segment,&isect))
				{
					// A direct line of sight probe at the mouse icon location hit something.
					Assert(isect.GetInstance() && isect.GetInstance()->GetLevelIndex()!=phInst::INVALID_INDEX);

					// Find the impulse magnitude.
					float impulseMag = 40.0f;
					impulseMag = isect.GetInstance()->GetArchetype()->GetMass();
					if (m_Pull.IsPressed())
					{
						impulseMag *= -1.0f;
					}

					// Set the impulse.
					Vector3 impulse(direction);
					impulse.Scale(impulseMag);

					// Apply the impulse.
					PHSIM->ApplyImpulse(isect.GetInstance()->GetLevelIndex(), impulse, RCC_VECTOR3(isect.GetPosition()), isect.GetComponent());
				}
			}
		}
	}

	void MatchIntersections (phIntersection& isectA, phIntersection& isectB)
	{
		Assert(isectA.IsAHit()==isectB.IsAHit());
		if (isectA.IsAHit() && isectB.IsAHit())
		{
			Assert(isectA.GetInstance() == isectB.GetInstance());
			ASSERT_ONLY(float allowedError = 1.0e-2f);	// 1.0e-6f fails in PC debug mode
			Assert(RCC_VECTOR3(isectA.GetPosition()).IsClose(RCC_VECTOR3(isectB.GetPosition()),allowedError));
			Assert(RCC_VECTOR3(isectA.GetNormal()).IsClose(RCC_VECTOR3(isectB.GetNormal()),allowedError));
			Assert(AreNearlyEqual(isectA.GetT(),isectB.GetT(),allowedError));
			Assert(AreNearlyEqual(isectA.GetDepth(),isectB.GetDepth(),allowedError));
			Assert(isectA.GetMaterialId()==isectB.GetMaterialId());
			Assert(isectA.GetPartIndex()==isectB.GetPartIndex());
			Assert(isectA.GetComponent()==isectB.GetComponent());
		}
	}

	void MatchIntersectionLists (phIntersection* isectListA, phIntersection* isectListB, int numIsects)
	{
		for (int isectIndex=0; isectIndex<numIsects; isectIndex++)
		{
			MatchIntersections(isectListA[isectIndex],isectListB[isectIndex]);
		}
	}

	void UpdateClient()
	{
		// Create a set of tasks in the system task manager.
		shapeTestTaskManager.Init();
		for (int taskIndex=0; taskIndex<phShapeTestTaskManager::s_NumShapeTestTasks; taskIndex++)
		{
			// Set the task parameter shapes.
			int firstShapeIndex = taskIndex*NumShapesPerTypePerTask;
			shapeTestTaskManager.SetProbes(taskIndex,&m_ProbeTestList[firstShapeIndex],NumShapesPerTypePerTask);
			shapeTestTaskManager.SetSpheres(taskIndex,&m_SphereTestList[firstShapeIndex],NumShapesPerTypePerTask);
			shapeTestTaskManager.SetCapsules(taskIndex,&m_CapsuleTestList[firstShapeIndex],NumShapesPerTypePerTask);
			shapeTestTaskManager.SetBatches(taskIndex,&m_BatchTestList[firstShapeIndex],NumShapesPerTypePerTask);
		}

		// Complete the tasks.
		shapeTestTaskManager.CompleteTasks();
/*
		// Repeat the same shape tests without multithreading, and verify that the results are the same.
		phIntersection multithreadedIsect;
#if !__PPU
		phIntersection batchMultiProbeIsectList[NumBatchedProbes];
		phIntersection batchMultiSphereIsectList[NumBatchedSpheres];
		phIntersection batchMultiCapsuleIsectList[NumBatchedCapsules];
#endif
		int listIndex = 0;
		for (int taskIndex=0; taskIndex<phShapeTestTaskManager::s_NumShapeTestTasks; taskIndex++)
		{
			for (int shapeIndex=0; shapeIndex<NumShapesPerTypePerTask; shapeIndex++)
			{
				// Test the probe again and verify the resulting intersection is the same.
				multithreadedIsect.Copy(m_ProbeIsectList[listIndex]);
				m_ProbeTestList[listIndex].TestInLevel();
				MatchIntersections(multithreadedIsect,m_ProbeIsectList[listIndex]);

				// Test the sphere again and verify the resulting intersection is the same.
				multithreadedIsect.Copy(m_SphereIsectList[listIndex]);
				m_SphereTestList[listIndex].TestInLevel();
				MatchIntersections(multithreadedIsect,m_SphereIsectList[listIndex]);

// Shape test tasks are not implemented on the SPU except for probes and spheres.
#if !__PPU
				// Test the capsule again and verify the resulting intersection is the same.
				multithreadedIsect.Copy(m_CapsuleIsectList[listIndex]);
				m_CapsuleTestList[listIndex].TestInLevel();
				MatchIntersections(multithreadedIsect,m_CapsuleIsectList[listIndex]);

				// Test the batch again and verify the resulting intersections are the same.
				phShapeBatch<phShapeTestTaskData::s_NumBatchedThreaded,phShapeTestTaskData::s_NumBatchedThreaded,phShapeTestTaskData::s_NumBatchedThreaded,phShapeTestTaskData::s_NumBatchedThreaded>& batchShape = m_BatchTestList[listIndex].GetShape();
				memcpy(batchMultiProbeIsectList,m_BatchedProbeIsectList[listIndex],sizeof(phIntersection)*batchShape.GetNumProbes());
				memcpy(batchMultiSphereIsectList,m_BatchedSphereIsectList[listIndex],sizeof(phIntersection)*batchShape.GetNumSpheres());
				memcpy(batchMultiCapsuleIsectList,m_BatchedCapsuleIsectList[listIndex],sizeof(phIntersection)*batchShape.GetNumCapsules());
				m_BatchTestList[listIndex].TestInLevel();
				MatchIntersectionLists(batchMultiProbeIsectList,m_BatchedProbeIsectList[listIndex],batchShape.GetNumProbes());
				MatchIntersectionLists(batchMultiSphereIsectList,m_BatchedSphereIsectList[listIndex],batchShape.GetNumSpheres());
				MatchIntersectionLists(batchMultiCapsuleIsectList,m_BatchedCapsuleIsectList[listIndex],batchShape.GetNumCapsules());
#endif
				// Increment the list index counter.
				listIndex++;
			}
		}
*/
		// Update the physics simulator.
		float frameTime = TIME.GetSeconds();
		bool finalUpdate = true;
		PHSIM->Update(frameTime,finalUpdate);

		// Update user input.
		UpdateMouse();
		m_Mapper.Update();
		if (m_Reset.IsPressed())
		{
			Reset();
		}

		if (m_AddAndDeleteObjects)
		{
			int objectIndex = g_ReplayRand.GetRanged(0,m_NumObjects-1);
			phInst& instance = m_ActiveInstanceList[objectIndex];
			if (instance.IsInLevel())
			{
				PHSIM->DeleteObject(instance.GetLevelIndex());
			}
			else
			{
				PHSIM->AddActiveObject(&instance);
			}
		}
	}

	void DrawClient()
	{

#if __PFDRAW

		// STEP #9. Draw the world
		//--  Tell the profile draw manager to draw all the objects in the physics level.
		// In a game this is normally used only for debugging.
		PHSIM->ProfileDraw();
		GetRageProfileDraw().Render();

#endif	// __PFDRAW

		// -STOP
	}

	void ResetObject (phInst& instance, const Matrix34& resetMatrix)
	{
		// Reset the instance.
		instance.SetMatrix(RCC_MAT34V(resetMatrix));

		// Make sure the object is active.
		int levelIndex = instance.GetLevelIndex();
		if (levelIndex==phInst::INVALID_INDEX)
		{
			// Put the object back in the world (this is needed when m_AddAndDeleteObjects has been on.
			PHSIM->AddActiveObject(&instance);
			levelIndex = instance.GetLevelIndex();
			Assert(levelIndex!=phInst::INVALID_INDEX);
		}
		else if(!PHLEVEL->IsActive(levelIndex))
		{
			PHSIM->ActivateObject(levelIndex);
		}

		// Reset the the collider.
		PHSIM->GetCollider(levelIndex)->Reset();

		// Inform the physics level the object was reset.
		PHLEVEL->UpdateObjectLocation(levelIndex);
	}

	virtual void Reset ()
	{
		// Reset the two active objects.
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			ResetObject(m_ActiveInstanceList[objectIndex],m_ResetMatrixList[objectIndex]);
		}

		// Reset the physics simulator.
		PHSIM->Reset();
	}

	void ShutdownClient()
	{
		// STEP #10. Shut down the physics level and simulator.
		// -- The user has requested the termination of the sample, so remove the objects from the physics level.
		PHSIM->DeleteObject(&m_TerrainInstance,false);
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			PHSIM->DeleteObject(&m_ActiveInstanceList[objectIndex],false);
		}

		// Delete the physics simulator.
		delete PHSIM;

		// Shut down and delete the physics level.
		PHLEVEL->Shutdown();
		delete PHLEVEL;

		// Call the static shutdown of the simulator.
		phSimulator::ShutdownClass();

		// Delete the material manager.
		MATERIALMGR.Destroy();

#if __PFDRAW
		// Shut down profile drawing.
		GetRageProfileDraw().Shutdown();
#endif
		// -STOP
	}

private:
	// an archetype, two instances and two reset matrices for the icosahedrons
	phArchetypePhys m_ActiveArchetype;
	int m_NumObjects;
	phInst m_ActiveInstanceList[MULTITHREAD_MAX_ACTIVE_OBJECTS];
	Matrix34 m_ResetMatrixList[MULTITHREAD_MAX_ACTIVE_OBJECTS];

	// an archetype and an instance for the terrain
	phArchetype m_TerrainArchetype;
	phInst m_TerrainInstance;

	// user interaction for resets and mouse controls
	ioMapper m_Mapper;
	ioValue m_Reset;
	ioValue m_Push;
	ioValue m_Pull;

	// PURPOSE: whether to randomly add and delete one object per frame, to stress test the simulator and physics level
	bool m_AddAndDeleteObjects;

	phShapeTest<phShapeProbe>* m_ProbeTestList;
	phShapeTest<phShapeSphere>* m_SphereTestList;
	phShapeTest<phShapeCapsule>* m_CapsuleTestList;
	phShapeTest<phShapeBatch>* m_BatchTestList;
	phIntersection* m_ProbeIsectList;
	phIntersection* m_SphereIsectList;
	phIntersection* m_CapsuleIsectList;
	phIntersection* m_BatchedProbeIsectList[NumShapesPerType];
	phIntersection* m_BatchedSphereIsectList[NumShapesPerType];
	phIntersection* m_BatchedCapsuleIsectList[NumShapesPerType];
};

} // namespace ragesamples

int Main()
{
	{
		ragesamples::threadedPhysicsSampleManager *sampleThreadedPhysics = rage_new ragesamples::threadedPhysicsSampleManager;
		sampleThreadedPhysics->Init();

		sampleThreadedPhysics->UpdateLoop();

		sampleThreadedPhysics->Shutdown();
		delete sampleThreadedPhysics;
	}
	
	return 0;
}
