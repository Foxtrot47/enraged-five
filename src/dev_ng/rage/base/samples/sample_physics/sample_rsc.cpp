//
// sample_physics/sample_rsc.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Bounds resourcing
// PURPOSE:
//		This sample shows how create and load a resource
//      containing a physics bound.

// Example command line:
// $\physics\park\bound -rscfile=$\physics\park\parkrsc

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "devcam/freecam.h"
#include "grcore/im.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/pad.h"
#include "paging/streamer.h"
#include "phbound/bound.h"
#include "phbound/boundgrid.h"
#include "phcore/materialmgrimpl.h"
#include "grprofile/drawmanager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"
#include "vector/colors.h"

PARAM(file,"the file containing the bound (.bnd)");

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

FPARAM(1, filename, "the file containing the bound (.bnd)");
PARAM(rscfile,"the resource file name");
PARAM(regen,"Force resource regeneration");

class rscSampleManager : public grcSampleManager
{
public:
	rscSampleManager()
	  : m_Bound(NULL)
	{
	}

	virtual void InitClient()
	{
		sysTimer timer;
		const char* filename;

		////////////////////////////////////////////////////////////
		// get bound filename
		if (!PARAM_file.Get(filename))
		{
			if (!PARAM_filename.Get(filename))
			{
				Quitf("Usage: sample_rsc boundfile.bnd -rscfile=resource");
			}
		}

		const char* pckname;
		if (!PARAM_rscfile.Get(pckname))
		{
			Quitf("Usage: sample_rsc boundfile.bnd -rscfile=resource");
		}

		////////////////////////////////////////////////////////////
		// Load the bnd file.

		phBound::SetBoundFlag(phBound::DELETE_BAD_POLYGONS,false);
		phBound::SetBoundFlag(phBound::ASSERT_MATH_ERRORS,false);

		// Make the material manager with a large maximum number of materials to handle grid bounds.
		phMaterialMgrImpl<phMaterial>::Create();


		// In a real program, you'd only init the streamer once.
		pgStreamer::InitClass();
		// This object must stay in scope and be unmodified until EndStream
		// completes.
		pgStreamerInfo info;
		datResourceMap map;
		if (!pgRscBuilder::BeginStream(pckname,"#bd",phBound::RORC_VERSION,info,map))
			Quitf("Cannot stream '%s'",pckname);
		pgRscBuilder::EndStream(m_Bound,info,map,true /*block*/,true /*close*/,pckname);
		pgStreamer::ShutdownClass();

#if __PFDRAW
		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_ONSCREEN_WARNING);
		GetRageProfileDraw().SetEnabled(true);
#endif
	}

	void InitLights()
	{
		m_Lights.Reset();
		m_Lights.SetColor(0, 1.0f,1.0f,1.0f);
		m_Lights.SetColor(1, 0.0f,0.0f,0.0f);
		m_Lights.SetColor(2, 0.0f,0.0f,0.0f);
		m_Lights.SetPosition(0, Vec3V(1.0f,2.0f,3.0f));
		m_Lights.SetPosition(1, Vec3V(-1.27f,-0.5f,-1.0f));
		m_Lights.SetPosition(2, Vec3V(1.2f,-0.575f,-2.15f));
		m_Lights.SetIntensity(0, 1.0f);
		m_Lights.SetIntensity(1, 0.0f);
		m_Lights.SetIntensity(2, 0.0f);
		m_Lights.SetAmbient(0.0f, 0.0f, 0.2f);
	}

	virtual void ShutdownClient()
	{
#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif

		delete m_Bound;

		MATERIALMGR.Destroy();
	}

	virtual void DrawClient()
	{
#if __PFDRAW
		grcBindTexture(NULL);
		grcLightState::SetEnabled(true);

		Matrix34 drawMatrix(M34_IDENTITY);
		drawMatrix.d = VEC3V_TO_VECTOR3(m_Bound->GetBoundingBoxMax() + m_Bound->GetBoundingBoxMin());
		drawMatrix.d.Scale(-0.5f);

		grcColor(Color32(255, 255, 255));
		m_Bound->Draw(RCC_MAT34V(drawMatrix), false, true);

		GetRageProfileDraw().Render();
#endif

	}

private:
	phBound*	m_Bound;
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::rscSampleManager viewbound;
	viewbound.Init();

	viewbound.UpdateLoop();

	viewbound.Shutdown();

	return 0;
}






