//
// sample_physics/demoobject.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "archmgr.h"
#include "demoobject.h"
#include "demoworld.h"

#include "bank/bank.h"
#include "file/asset.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/savedstate.h"
#include "phbound/boundbox.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundsphere.h"
#include "phbound/support.h"
#include "phcore/conversion.h"
#include "phcore/phmath.h"
#include "physics/colliderdispatch.h"
#include "physics/constraintdistance.h"
#include "physics/constraintmgr.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "rmcore/drawable.h"
#include "vector/colors.h"
#include "vectormath/classes.h"

namespace ragesamples {


////////////////////////////////////////////////////////////////

Vector3 phDemoObject::s_WorldOffset = ORIGIN;

phDemoObject::phDemoObject ()
{
	m_PhysInst = NULL;

	m_Drawable = NULL;
	m_ModelMatrix = NULL;

	m_InitialMatrix.Zero();
	m_InitialVelocity.Zero();
	m_InitialAngVelocity.Zero();
	m_Label = NULL;
	m_DeleteOnShutdown = true;

	m_Visible = true;
	m_DrawSolidIfNoModel = true;
}


phDemoObject::~phDemoObject()
{
    if (m_PhysInst != NULL)
    {
		if (GetLevelIndex()!=phInst::INVALID_INDEX)
		{
			if (phCollider* collider = PHSIM->GetCollider(GetLevelIndex()))
			{
				if (!PHSIM->ColliderIsManagedBySimulator(collider))
				{
					delete collider->GetSleep();
					delete collider;
				}
			}

			PHSIM->DeleteObject(m_PhysInst->GetLevelIndex());
		}

        delete m_PhysInst;
    }

	// Drawable is removed by DemoWorld

    m_PhysInst = NULL;
}


void phDemoObject::SetInst (phInst * inst)
{
	Assert(inst);

	if (m_PhysInst)
	{
		Warningf("phDemoObject:SetInstance - warning, m_PhysInst already non-NULL");
	}

	m_PhysInst = inst;
}


void phDemoObject::AlignBottom ()
{
	// align the object to the correct height
	Vector3 localDown;
	localDown.Set(0.0f,-1.0f,0.0f);
	localDown.Dot3x3Transpose(m_InitialMatrix);

	float alignDist;
	Assert(m_PhysInst->GetArchetype());
	alignDist = Dot(m_PhysInst->GetArchetype()->GetBound()->LocalGetSupportingVertex(localDown.xyzw), RCC_VEC3V(localDown)).Getf();
	m_InitialMatrix.d.y += alignDist;
	m_PhysInst->SetMatrix( *(const Mat34V*)(&m_InitialMatrix) );
}


void phDemoObject::SetResetMatrix (const Matrix34 & mtx)
{
	m_InitialMatrix.Set(mtx);
}


void phDemoObject::SetInitialVelocity (const Vector3& velocity)
{
	m_InitialVelocity.Set(velocity);
}


void phDemoObject::SetInitialAngVelocity (const Vector3& angVelocity)
{
	m_InitialAngVelocity.Set(angVelocity);
}


void phDemoObject::InitPhys (const char * boundFile, const Vector3 & position, const Vector3 & rotation, bool uniqueBound)
{
	Matrix34 initialMatrix(CreateRotatedMatrix(position,rotation));
	InitPhys(boundFile,initialMatrix,uniqueBound);
}


void phDemoObject::InitPhys (const char * boundFile, const Matrix34 & mtx, bool uniqueBound, Functor0Ret<phInst*> createFunc)
{
    m_name = boundFile;

	char boundFilePath[RAGE_MAX_PATH];
	strcpy(boundFilePath, "physics\\");
	strcat(boundFilePath, boundFile);
	strcat(boundFilePath, "\\bound");
	phArchetypeDamp* archetype;
	if (uniqueBound)
	{
		archetype = static_cast<phArchetypeDamp*>(ARCHMGR.GetUniqueArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP));
	}
	else
	{
		archetype = static_cast<phArchetypeDamp*>(ARCHMGR.GetArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP));
	}
	Assertf(archetype,"Physics archetype '%s' failed to load from path '%s'.",boundFile,boundFilePath);

	InitPhys(archetype, mtx, createFunc);

	if (archetype->GetDampingConstant(phArchetypeDamp::LINEAR_C).IsZero() && archetype->GetDampingConstant(phArchetypeDamp::LINEAR_V).IsZero() && archetype->GetDampingConstant(phArchetypeDamp::LINEAR_V2).IsZero() &&
		archetype->GetDampingConstant(phArchetypeDamp::ANGULAR_C).IsZero() && archetype->GetDampingConstant(phArchetypeDamp::ANGULAR_V).IsZero() && archetype->GetDampingConstant(phArchetypeDamp::ANGULAR_V2).IsZero())
	{
		// No damping was read from a phys file, so give this damped archetype some default damping.
		float linearC = 0.02f;
		float linearV = 0.02f;
		float linearV2 = 0.01f;
		float angularC = 0.02f;
		float angularV = 0.02f;
		float angularV2 = 0.01f;
		DampMotion(linearC,linearV,linearV2,angularC,angularV,angularV2);
	}
}

void phDemoObject::InitBoxPhys (const Vector3& boxExtents, float density, const Matrix34 & mtx)
{
	char name[RAGE_MAX_PATH];
	sprintf(name, "box%fx%fx%fdensity%f", boxExtents.x, boxExtents.y, boxExtents.z, density);

	phArchetype* archetype = NULL;

	if ((archetype = ARCHMGR.FindArchetype(name)) == NULL)
	{
		phBound* newBound = rage_new phBoundBox(boxExtents);

		archetype = ARCHMGR.RegisterArchetype(newBound, name, true, phArchetype::ARCHETYPE_DAMP, density);
	}

	InitPhys(archetype, mtx);

	float linearC = 0.02f;
	float linearV = 0.02f;
	float linearV2 = 0.01f;
	float angularC = 0.02f;
	float angularV = 0.02f;
	float angularV2 = 0.01f;
	DampMotion(linearC,linearV,linearV2,angularC,angularV,angularV2);
}

void phDemoObject::InitSpherePhys (float radius, float density, const Vector3& position, float maxSpeed, float maxAngSpeed)
{
	char name[RAGE_MAX_PATH];
	sprintf(name, "sphere%fdensity%f", radius, density);

	phArchetype* archetype = NULL;

	if ((archetype = ARCHMGR.FindArchetype(name)) == NULL)
	{
		phBound* newBound = rage_new phBoundSphere(radius);

		archetype = ARCHMGR.RegisterArchetype(newBound, name, true, phArchetype::ARCHETYPE_DAMP, density);

		phArchetypePhys* archetypePhys = static_cast<phArchetypePhys*>(archetype);
		archetypePhys->SetMaxSpeed(maxSpeed);
		archetypePhys->SetMaxAngSpeed(maxAngSpeed);
	}

	Matrix34 pose(M34_IDENTITY);
	pose.d.Set(position);
	InitPhys(archetype, pose);

	float linearC = 0.02f;
	float linearV = 0.02f;
	float linearV2 = 0.01f;
	float angularC = 0.02f;
	float angularV = 0.02f;
	float angularV2 = 0.01f;
	DampMotion(linearC,linearV,linearV2,angularC,angularV,angularV2);
}

void phDemoObject::InitCapsulePhys (float length, float radius, float density, const Matrix34 & mtx)
{
	char name[RAGE_MAX_PATH];
	sprintf(name, "capsule%fx%fdensity%f", length, radius, density);

	phArchetype* archetype = NULL;

	if ((archetype = ARCHMGR.FindArchetype(name)) == NULL)
	{
		phBoundCapsule* newBound = rage_new phBoundCapsule();
		newBound->SetCapsuleSize(radius, length);

		archetype = ARCHMGR.RegisterArchetype(newBound, name, true, phArchetype::ARCHETYPE_DAMP, density);
	}

	InitPhys(archetype, mtx);

	float linearC = 0.02f;
	float linearV = 0.02f;
	float linearV2 = 0.01f;
	float angularC = 0.02f;
	float angularV = 0.02f;
	float angularV2 = 0.01f;
	DampMotion(linearC,linearV,linearV2,angularC,angularV,angularV2);
}

void phDemoObject::InitRubiksPhys (const Vector3& boxExtents, float spacing, u32 height, u32 width, u32 depth, float density, const Matrix34 & mtx)
{
	char name[RAGE_MAX_PATH];
	sprintf(name, "rubiks_%.1fx%.1fx%.1f_%.1f_%dx%dx%ddensity%.1f", boxExtents.x, boxExtents.y, boxExtents.z, spacing, height, width, depth, density);

	phArchetype* archetype = NULL;

	if ((archetype = ARCHMGR.FindArchetype(name)) == NULL)
	{
		phBoundComposite* newBound = rage_new phBoundComposite();

		u32 totalCubes = height * width * depth;
		newBound->Init(totalCubes);

		phBoundBox* newPart = rage_new phBoundBox(boxExtents);

		for (u32 i = 0; i < height; ++i)
		{
			u32 layer = i * width * depth;
			for (u32 j = 0; j < width; ++j)
			{
				u32 row = j * depth;
				for (u32 k = 0; k < depth; ++k)
				{
					u32 partIndex = layer + row + k;
					Matrix34 partMtx(M34_IDENTITY);
					partMtx.d.Set(float(i), float(j), float(k));
					partMtx.d.Scale(spacing);

					newBound->SetCurrentMatrix(partIndex, RCC_MAT34V(partMtx));
					newBound->SetBound(partIndex, newPart);
				}
			}
		}

		newPart->Release();

		newBound->CalculateCompositeExtents();
		newBound->PostLoadCompute();
		newBound->CalcCenterOfGravity();
		newBound->ComputeCompositeAngInertia(density);

		archetype = ARCHMGR.RegisterArchetype(newBound, name, true, phArchetype::ARCHETYPE_DAMP, density);
	}

	InitPhys(archetype, mtx);

	float linearC = 0.02f;
	float linearV = 0.02f;
	float linearV2 = 0.01f;
	float angularC = 0.02f;
	float angularV = 0.02f;
	float angularV2 = 0.01f;
	DampMotion(linearC,linearV,linearV2,angularC,angularV,angularV2);
}


void phDemoObject::DampMotion (const Vector3& linearC, const Vector3& linearV, const Vector3& linearV2,
								const Vector3& angularC, const Vector3& angularV, const Vector3& angularV2)
{
	Assert(GetPhysInst()->GetArchetype()->IsDamped());
	phArchetypeDamp* archetype = static_cast<phArchetypeDamp*>(GetPhysInst()->GetArchetype());
	archetype->ActivateDamping(phArchetypeDamp::LINEAR_C,linearC);
	archetype->ActivateDamping(phArchetypeDamp::LINEAR_V,linearV);
	archetype->ActivateDamping(phArchetypeDamp::LINEAR_V2,linearV2);
	archetype->ActivateDamping(phArchetypeDamp::ANGULAR_C,angularC);
	archetype->ActivateDamping(phArchetypeDamp::ANGULAR_V,angularV);
	archetype->ActivateDamping(phArchetypeDamp::ANGULAR_V2,angularV2);
}


void phDemoObject::DampMotion (float linearC, float linearV, float linearV2,
								float angularC, float angularV, float angularV2)
{
	DampMotion(Vector3(linearC,linearC,linearC),Vector3(linearV,linearV,linearV),Vector3(linearV2,linearV2,linearV2),
				Vector3(angularC,angularC,angularC),Vector3(angularV,angularV,angularV),Vector3(angularV2,angularV2,angularV2));
}


void phDemoObject::InitPhys (phArchetype* archetype, const Matrix34& mtx, Functor0Ret<phInst*> createFunc)
{
	// Set the initial matrix to the given matrix.
	m_InitialMatrix.Set(mtx);

	// Add the static world offset (normally zero).
    m_InitialMatrix.d.Add(s_WorldOffset);

    if (m_PhysInst==NULL)
    {
        // This object doesn't already have a physics instance, so make one.
        // Use the create func if we have one otherwise use old code.
        if (createFunc)
        {
            m_PhysInst = createFunc();
        } 
        else 
        {
            m_PhysInst = rage_new phInst;
        }

        Assert(m_PhysInst!=NULL);
    }

	// Initialize the physics instance with the archetype and the initial matrix.
	Assert(archetype);
	m_PhysInst->SetArchetype(archetype);
	m_PhysInst->SetMatrix( *(const Mat34V*)(&m_InitialMatrix) );
}

void phDemoObject::InitGfx (const char* name)
{
    m_Drawable = phDemoWorld::GetDrawable(name);
}

void phDemoObject::Reset(phLevelNew *pLevelNew, phSimulator *pSim)
{
	int levelIndex = GetLevelIndex();
	if (levelIndex!=phInst::INVALID_INDEX)
	{
		if (m_InitialStateFlag==phLevelBase::STATE_FLAG_ACTIVE)
		{
			// Make sure the object is active.
			if(!pLevelNew->IsActive(levelIndex))
			{
				pSim->ActivateObject(levelIndex);
			}

			// Reset the object.
			m_PhysInst->SetMatrix(*(const Mat34V*)(&m_InitialMatrix));
			phCollider* collider = pSim->GetCollider(levelIndex);
			collider->Reset();
			collider->SetVelocity(m_InitialVelocity);
			collider->SetAngVelocity(m_InitialAngVelocity);
			pLevelNew->UpdateObjectLocationAndRadius(levelIndex,(Mat34V_Ptr)(NULL));
		}
		else if (m_InitialStateFlag==phLevelBase::STATE_FLAG_INACTIVE)
		{
			if(pLevelNew->IsActive(levelIndex))
			{
				// Deactivate the object.
				pSim->DeactivateObject(levelIndex);
			}

			m_PhysInst->SetMatrix(*(const Mat34V*)(&m_InitialMatrix));
			pLevelNew->UpdateObjectLocationAndRadius(levelIndex,(Mat34V_Ptr)(NULL));
		}
	}
}


void phDemoObject::DrawModel (
					const Matrix34& mtx
					) const
{
	if(!m_Visible)
		return;

	if (m_Drawable)
	{
		u8 lod = 0;

		grcViewport*	vp = grcViewport::GetCurrent();
		if ( m_Drawable->IsVisible( mtx, *vp, lod ) == cullOutside )
		{
			return;
		}

		const Matrix34& drawMatrix = m_ModelMatrix ? *m_ModelMatrix : mtx;
		for (int bucket = 0; bucket < 8; ++bucket)
		{
			//float dist = drawMatrix.d.Dist(grcViewport::GetCurrentWorldMtx().d);
			m_Drawable->Draw(drawMatrix,bucket, lod ); // This set to zero to allow rmcDrawableBase to be used instead m_Drawable->GetLodGroup().ComputeLod(dist)
		}


	}

#if __PFDRAW
	else if (m_DrawSolidIfNoModel)
	{
		grcBindTexture(NULL);
		grcColor(Color_white);
		m_PhysInst->GetArchetype()->GetBound()->Draw(RCC_MAT34V(mtx), false, true);
	}
#endif // __PFDRAW
}

#if __BANK
void phDemoObject::AddWidgets( bkBank &b )
{
	b.PushGroup( GetName(), false );
		b.AddSlider( "Initial Position", &m_InitialMatrix.d, -400.0f, 400.0f, 0.1f );
        b.AddToggle( "Draw Solid If No Model", &m_DrawSolidIfNoModel );
	b.PopGroup();
}
#endif

phSwingingObject::~phSwingingObject ()
{
}

void phSwingingObject::SetMovingConstraint (const Vector3& position, phConstraintMgr* constraintMgr, int component, const Vector3& frequency, const Vector3& amplitude,
														const Vector3& phaseOffset)
{
	// Set a constraint to the world that will be moved around by sine waves.
	phConstraintDistance::Params constraintDistance;
	constraintDistance.instanceA = GetPhysInst();
	constraintDistance.componentA = (u16)component;
	constraintDistance.worldAnchorA = constraintDistance.worldAnchorB = RCC_VEC3V(position);
	m_MovingConstraint = constraintMgr->Insert(constraintDistance);
	m_Frequency.Set(frequency);
	m_Amplitude.Set(amplitude);
	m_PhaseOffset.Set(phaseOffset);
}


void phSwingingObject::SetDrawable( rmcDrawableBase* newDrawableObject )
{
	delete m_Drawable;
	m_Drawable = newDrawableObject;
}

void phSwingingObject::Update (float timeStep)
{
	// Get the total time that the demo world has been running.
	float elapsedTime = phDemoWorld::GetActiveDemo()->GetElapsedTime();

	// See if this object has a moving (translation) constraint.
	if (phConstraintDistance* constraint = (phConstraintDistance*)PHCONSTRAINT->GetTemporaryPointer(m_MovingConstraint))
	{
		// Find the position offset during the current frame (the position follows a cosine wave and the velocity follows a sine wave).
		Vector3 phase(m_PhaseOffset);
		phase.AddScaled(m_Frequency,elapsedTime);
		Vector3 offset(sinf(phase.x),sinf(phase.y),sinf(phase.z));
		offset.Multiply(m_Frequency);
		offset.Multiply(m_Amplitude);
		offset.Scale(timeStep);

		// Move the constrained position.
		constraint->SetWorldPosB(RCC_VEC3V(offset));
	}
}

phWhirlingObject::phWhirlingObject (float period, float mag, float phase)
: m_OrgPos(-1.0f, -1.0f, -1.0f)
, m_Period(period)
, m_Phase(phase)
, m_InitialPhase(phase)
, m_Mag(mag)
{
}

phWhirlingObject::~phWhirlingObject()
{
}

void phWhirlingObject::Update (float timeStep)
{
    if (m_OrgPos.x == -1.0f && m_OrgPos.x == -1.0f && m_OrgPos.x == -1.0f)
    {
        m_OrgPos.Set((*(const Matrix34*)(&GetPhysInst()->GetMatrix())).d);
    }

    if (GetPhysInst()->GetMatrix().GetM13f() < m_OrgPos.y - 15.0f)
    {
		Matrix34 resetPos(M34_IDENTITY);
		resetPos.d = m_OrgPos;
        PHSIM->TeleportObject(*GetPhysInst(), resetPos);
        PHSIM->GetCollider(GetPhysInst())->SetColliderMatrixFromInstance();

        PHLEVEL->UpdateObjectLocation(GetPhysInst()->GetLevelIndex());
    }

    m_Phase += timeStep;
    m_Phase = fmod(m_Phase, m_Period);

    float radians = 2.0f * PI * m_Phase / m_Period;
    
    float windX = m_Mag * sinf(radians);
    float windZ = m_Mag * cosf(radians);

    Vector3 wind(windX, 0.0f, windZ);

    PHSIM->ApplyForce(ScalarV(timeStep).GetIntrin128ConstRef(), GetPhysInst()->GetLevelIndex(), wind);
}

void phWhirlingObject::Reset (phLevelNew *pLevelNew, phSimulator *pSim)
{
    m_Phase = m_InitialPhase;

    phUpdateObject::Reset(pLevelNew, pSim);
}

class phRagdollPartInst : public phInst
{
public:
	phRagdollPartInst(phArticulatedObject* parent);

	virtual bool ShouldFindImpacts(const phInst* otherInst) const;

private:
	//phArticulatedObject* m_Parent;
};

phRagdollPartInst::phRagdollPartInst(phArticulatedObject* /*parent*/)
	//: m_Parent(parent)
{
}

bool phRagdollPartInst::ShouldFindImpacts(const phInst* otherInst) const
{
	void* pa=GetUserData();
	void* pb=otherInst->GetUserData();
	if(pa && pb)
	{
		return ConstrainedHumanBodyPartCollTester::ShouldFindImpacts(*static_cast<const ConstrainedHumanBodyPart*>(pa),*static_cast<const ConstrainedHumanBodyPart*>(pb));
	}
	return true;

	/*
	if (const phRagdollPartInst* otherRagdoll = dynamic_cast<const phRagdollPartInst*>(otherInst))
	{
		if (otherRagdoll->m_Parent == m_Parent)
		{
			return false;
		}
	}

	return true;
	*/
}

phInst* phArticulatedObject::CreateRagdollInst()
{
	return rage_new phRagdollPartInst(this);
}

phArticulatedObject::~phArticulatedObject ()
{
	delete m_ResetState;

	if(m_Articulated)
	{
		for (int partIndex = 0; partIndex < m_Parts.GetCount(); ++partIndex)
		{
			delete m_Parts[partIndex];
		}
	}
	else
	{
		for (int partIndex = 1; partIndex < m_Parts.GetCount(); ++partIndex)
		{
			delete m_Parts[partIndex];
		}
	}

	if(m_CollisionTester) delete m_CollisionTester;
}

Vector3 phArticulatedObject::GetWorldPositionOfBoundPart (int index)
{
	if (m_Articulated)
	{
		return VEC3V_TO_VECTOR3(m_Collider->GetWorldPositionOfBoundPart(index));
	}
	else
	{
		if (index == 0)
		{
			return RCC_VECTOR3(GetPhysInst()->GetPosition());
		}
		else
		{
			return RCC_VECTOR3(m_Parts[index]->GetPhysInst()->GetPosition());
		}
	}
}

void phArticulatedObject::SetPuppetBody (phArticulatedBody* puppetBody)
{
	// Set an articulated body that will be controlled to mach this body.
	m_PuppetBody = puppetBody;
	m_PuppetBody->SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
}


void phArticulatedObject::Reset (phLevelNew *pLevelNew, phSimulator *pSim)
{
	if (m_Articulated)
	{
		m_ResetState->RestoreState(m_Body);
	}

	phUpdateObject::Reset(pLevelNew,pSim);
}


void phArticulatedObject::SaveResetState()
{
	m_ResetState->SaveState(m_Body);
}


void phArticulatedObject::Update (float timeStep)
{
	phSwingingObject::Update(timeStep);

	if (m_PuppetBody)
	{
		int numJoints = m_Body.GetNumJoints();
		Assert(m_PuppetBody->GetNumJoints()==numJoints);
		for (int jointIndex=0; jointIndex<numJoints; jointIndex++)
		{
			phJoint& puppetJoint = m_PuppetBody->GetJoint(jointIndex);
			phJoint& bodyJoint = m_Body.GetJoint(jointIndex);
			Assert(bodyJoint.GetJointType()==puppetJoint.GetJointType());
			if (puppetJoint.GetJointType()==phJoint::JNT_1DOF)
			{
				phJoint1Dof& bodyJoint1Dof = *static_cast<phJoint1Dof*>(&bodyJoint);
				bodyJoint1Dof.ComputeCurrentAngle(&GetBody());
				bodyJoint1Dof.ComputeRotationalVelocity(&GetBody());
				float targetAngle = bodyJoint1Dof.GetComputedAngle();
				static_cast<phJoint1Dof*>(&puppetJoint)->SetMuscleTargetAngle(targetAngle);
				float targetSpeed = bodyJoint1Dof.GetAngularSpeed(&GetBody());
				targetSpeed = Clamp(targetSpeed, -6.0f * PI, 6.0f * PI);
				static_cast<phJoint1Dof*>(&puppetJoint)->SetMuscleTargetSpeed(targetSpeed);
			}
			else
			{
				Assert(puppetJoint.GetJointType()==phJoint::JNT_3DOF);
				Vec3V targetAngle,targetSpeed;
				phJoint3Dof& bodyJoint3Dof = *static_cast<phJoint3Dof*>(&bodyJoint);
				bodyJoint3Dof.ComputeCurrentLeanAnglesAndRates(&GetBody(), targetAngle,targetSpeed);
				phJoint3Dof& puppetJoint3Dof = *static_cast<phJoint3Dof*>(&puppetJoint);
				puppetJoint3Dof.SetMuscleTargetAngle(RCC_VECTOR3(targetAngle));
				puppetJoint3Dof.SetMuscleTargetSpeed(RCC_VECTOR3(targetSpeed));
			}
		}
	}
}

void phArticulatedObject::SetMovingConstraint (const Vector3& position, phConstraintMgr* constraintMgr, int component, const Vector3& frequency, const Vector3& amplitude,
													 const Vector3& phaseOffset)
{
	// Set a constraint to the world that will be moved around by sine waves.
	phConstraintDistance::Params constraintDistance;
	constraintDistance.worldAnchorA = constraintDistance.worldAnchorB = RCC_VEC3V(position);
	if (m_Articulated || component == 0)
	{
		constraintDistance.instanceA = GetPhysInst();
		constraintDistance.componentA = (u16)component;
		m_MovingConstraint = constraintMgr->Insert(constraintDistance);
	}
	else
	{
		constraintDistance.instanceA = m_Parts[component]->GetPhysInst();
		m_MovingConstraint = constraintMgr->Insert(constraintDistance);
	}
	m_Frequency.Set(frequency);
	m_Amplitude.Set(amplitude);
	m_PhaseOffset.Set(phaseOffset);
}


void phArticulatedObject::InitBody (phArchetype* archetype, phInst* instance, phDemoWorld& demoWorld)
{
	// Add the mass and angular inertia of all the body parts.
	Vector3 totalMass(ORIGIN),totalAngInertia(ORIGIN);
	int numBodyParts = m_Body.GetNumBodyParts();
	for (int partIndex = 0; partIndex < numBodyParts; ++partIndex)
	{
		const phArticulatedBodyPart& bodyPart = m_Body.GetLink(partIndex);
		Vector3 partMass = SCALARV_TO_VECTOR3(m_Body.GetMass(partIndex));
		totalMass.Add(partMass);
		totalAngInertia.Add(VEC3V_TO_VECTOR3(m_Body.GetAngInertia(partIndex)));
		Vector3 offsetSquared(bodyPart.GetPosition());
		offsetSquared.Multiply(offsetSquared);
		Vector3 positionAngInertia(offsetSquared.y+offsetSquared.z,offsetSquared.x+offsetSquared.z,offsetSquared.x+offsetSquared.y);
		totalAngInertia.AddScaled(positionAngInertia,partMass);
	}

	// Set the total mass and angular inertia in the archetype.
	archetype->SetMass(totalMass.x);
	archetype->SetAngInertia(totalAngInertia);

	phBoundComposite* bound = static_cast<phBoundComposite*>(archetype->GetBound());
	Assert(bound->GetType() == phBound::COMPOSITE);
	if (bound->GetLastMatrices() == bound->GetCurrentMatrices())
	{
		bound->AllocateLastMatrices();
	}

	m_Collider = rage_new phArticulatedCollider(phArticulatedBodyType::MAX_NUM_JOINT_DOFS, 32);

	m_Collider->GetSelfCollisionPairsArrayRefA().Reserve(256);
	m_Collider->GetSelfCollisionPairsArrayRefB().Reserve(256);

	phSleep* sleep = rage_new phSleep;
	sleep->Init(m_Collider);
	m_Collider->SetBody(&m_Body);
	m_Collider->UpdateSavedVelocityArraySizes();
	m_Collider->Init(instance, sleep);
	m_Collider->UpdateCurrentAndLastMatrices();
	PHSIM->AddActiveObject(m_Collider);
	Assert(PHLEVEL->LegitLevelIndex(GetLevelIndex()));
	SetInitialStateFlag(phLevelBase::STATE_FLAG_ACTIVE);
	demoWorld.AddUpdateObject(this);

	m_ResetState = rage_new phArticulatedBodySavedState;
	m_ResetState->SaveState(m_Body);

#if __PS3
	m_DmaPlan = rage_new phArticulatedCollider::DmaPlan;
	m_Collider->GenerateCoreDmaPlan(*m_DmaPlan);
#endif
}


void phArticulatedObject::InitHuman (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation, const Vector3& cgOffset, int numBodyParts)
{
	m_Articulated = phDemoWorld::GetArticulatedRagdolls();

	const char* defaultName = "humanoid";
	char name[RAGE_MAX_PATH];
	if (numBodyParts==DEFAULT_HUMAN_PARTS)
	{
		formatf(name,255,"%s",defaultName);
	}
	else
	{
		formatf(name,255,"%s_%i",defaultName,numBodyParts);
	}

	float TORSO_MASS = 30.0f;
	float HEAD_MASS = 5.0f;
	float PELVIS_MASS = 5.0f;
	float UPPER_ARM_MASS = 3.0f;
	float LOWER_ARM_MASS = 2.0f;
	float UPPER_LEG_MASS = 10.0f;
	float LOWER_LEG_MASS = 6.0f;

	enum
	{
		TORSO=0,			//	0
		HEAD,				//	1
		PELVIS,				//	2
		UPPER_RIGHT_ARM,	//	3
		LOWER_RIGHT_ARM,	//	4
		UPPER_LEFT_ARM,		//	5
		LOWER_LEFT_ARM,		//	6
		UPPER_RIGHT_LEG,	//	7
		LOWER_RIGHT_LEG,	//	8
		UPPER_LEFT_LEG,		//	9
		LOWER_LEFT_LEG,		//	10
		RIGHT_FOOT,			//	11 (optional)
		LEFT_FOOT,			//	12 (optional)
		RIGHT_HAND,			//	13 (optional)
		LEFT_HAND			//	14 (optional)
	};


	if (m_Articulated)
	{
		InitGfx(name);
		Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
		InitPhys(name,bodyMatrix,true);
		phInst* instance = GetPhysInst();
		phArchetypePhys* archetype = static_cast<phArchetypePhys*>(instance->GetArchetype());
		Assert(archetype->GetBound() && archetype->GetBound()->GetType()==phBound::COMPOSITE);
		phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(archetype->GetBound());

		compositeBound.SetCGOffset(RCC_VEC3V(cgOffset));

		int numLinks = compositeBound.GetNumBounds();
		int linkIndex;
		for (linkIndex=0; linkIndex<numLinks; linkIndex++)
		{
			m_Body.SetBodyPart(linkIndex,rage_new phArticulatedBodyPart());
		}

		m_Body.SetJoint(0,rage_new phJoint3Dof());
		m_Body.SetJoint(1,rage_new phJoint3Dof());
		m_Body.SetJoint(2,rage_new phJoint3Dof());
		m_Body.SetJoint(3,rage_new phJoint1Dof());
		m_Body.SetJoint(4,rage_new phJoint3Dof());
		m_Body.SetJoint(5,rage_new phJoint1Dof());
		m_Body.SetJoint(6,rage_new phJoint3Dof());
		m_Body.SetJoint(7,rage_new phJoint1Dof());
		m_Body.SetJoint(8,rage_new phJoint3Dof());
		m_Body.SetJoint(9,rage_new phJoint1Dof());
		if (numLinks>LEFT_FOOT)
		{
			m_Body.SetJoint(10,rage_new phJoint3Dof());
			m_Body.SetJoint(11,rage_new phJoint3Dof());
			if (numLinks>LEFT_HAND)
			{
				m_Body.SetJoint(12,rage_new phJoint3Dof());
				m_Body.SetJoint(13,rage_new phJoint3Dof());
			}
		}

		// torso
		const float fractionOverlap = 0.3f;
		Assert(compositeBound.GetBound(TORSO));
		phBound& torsoBound = *compositeBound.GetBound(TORSO);
		float torsoWidth = torsoBound.GetBoundingBoxMax().GetXf() - torsoBound.GetBoundingBoxMin().GetXf();
		float torsoHeight = torsoBound.GetBoundingBoxMax().GetYf() - torsoBound.GetBoundingBoxMin().GetYf();
		float torsoRadius = 0.5f * torsoWidth;
		float torsoLength = torsoHeight - torsoWidth;
		float halfTorsoLength = 0.5f*torsoLength;
		Matrix34 initialMtx;
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		initialMtx.d.Zero();
		m_Body.GetLink(TORSO).SetMatrix(initialMtx);

		// head
		Assert(compositeBound.GetBound(HEAD));
		float headRadius = compositeBound.GetBound(HEAD)->GetRadiusAroundCentroid();
		float halfTorsoHeight = halfTorsoLength+torsoRadius;
		Vector3 partRotation(0.5f,0.0f,0.0f);
		Matrix34 partInitialMatrix(CreateRotatedMatrix(ORIGIN,partRotation));
		partInitialMatrix.Dot3x3(bodyMatrix);
		partInitialMatrix.Transpose();
		Vector3 neckPosition(bodyMatrix.b);
		neckPosition.Scale(halfTorsoHeight);
		partInitialMatrix.d.AddScaled(neckPosition,bodyMatrix.b,headRadius-fractionOverlap*Min(headRadius,torsoRadius));
		m_Body.GetLink(HEAD).SetMatrix(partInitialMatrix);
		
		// pelvis
		Assert(compositeBound.GetBound(PELVIS));
		float pelvisRadius = compositeBound.GetBound(PELVIS)->GetRadiusAroundCentroid();
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 waistPosition(bodyMatrix.b);
		waistPosition.Scale(-halfTorsoHeight);
		initialMtx.d.SubtractScaled(waistPosition,bodyMatrix.b,pelvisRadius-fractionOverlap*Min(pelvisRadius,torsoRadius));
		m_Body.GetLink(PELVIS).SetMatrix(initialMtx);

		// upper right arm
		Assert(compositeBound.GetBound(UPPER_RIGHT_ARM));
		phBound& upperArmBound = *compositeBound.GetBound(UPPER_RIGHT_ARM);
		float upperArmWidth = upperArmBound.GetBoundingBoxMax().GetXf() - upperArmBound.GetBoundingBoxMin().GetXf();
		float upperArmRadius = 0.5f * upperArmWidth;
		float upperArmHeight = upperArmBound.GetBoundingBoxMax().GetYf() - upperArmBound.GetBoundingBoxMin().GetYf();
		float upperArmLength = upperArmHeight - upperArmWidth;
		initialMtx.a.Set(-YAXIS);
		initialMtx.b.Set(XAXIS);
		initialMtx.c.Set(ZAXIS);
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 rightShoulderPosition(bodyMatrix.b);
		rightShoulderPosition.Scale(halfTorsoLength);
		rightShoulderPosition.SubtractScaled(bodyMatrix.a,torsoRadius);
		initialMtx.d.SubtractScaled(rightShoulderPosition,bodyMatrix.a,0.5f*upperArmHeight-
									fractionOverlap*Min(upperArmRadius,torsoRadius));
		m_Body.GetLink(UPPER_RIGHT_ARM).SetMatrix(initialMtx);

		// lower right arm
		Assert(compositeBound.GetBound(LOWER_RIGHT_ARM));
		phBound& lowerArmBound = *compositeBound.GetBound(LOWER_RIGHT_ARM);
		float lowerArmWidth = lowerArmBound.GetBoundingBoxMax().GetXf() - lowerArmBound.GetBoundingBoxMin().GetXf();
		float lowerArmHeight = lowerArmBound.GetBoundingBoxMax().GetYf() - lowerArmBound.GetBoundingBoxMin().GetYf();
		float lowerArmRadius = 0.5f*lowerArmWidth;
		initialMtx.a.Set(-YAXIS);
		initialMtx.b.Set(XAXIS);
		initialMtx.c.Set(ZAXIS);
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 rightElbowPosition(bodyMatrix.b);
		rightElbowPosition.Scale(halfTorsoLength);
		float elbowOffset = torsoRadius+upperArmHeight-fractionOverlap*Min(upperArmRadius,torsoRadius);
		rightElbowPosition.SubtractScaled(bodyMatrix.a,elbowOffset);
		float lowerArmOffset = 0.5f*lowerArmHeight-fractionOverlap*Min(lowerArmRadius,upperArmRadius);
		initialMtx.d.SubtractScaled(rightElbowPosition,bodyMatrix.a,lowerArmOffset);
		m_Body.GetLink(LOWER_RIGHT_ARM).SetMatrix(initialMtx);

		// upper left arm
		initialMtx.a.Set(YAXIS);
		initialMtx.b.Set(-XAXIS);
		initialMtx.c.Set(ZAXIS);
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 leftShoulderPosition(bodyMatrix.b);
		leftShoulderPosition.Scale(halfTorsoLength);
		leftShoulderPosition.AddScaled(bodyMatrix.a,torsoRadius);
		initialMtx.d.AddScaled(leftShoulderPosition,bodyMatrix.a,0.5f*upperArmLength+upperArmRadius-
								fractionOverlap*Min(upperArmRadius,torsoRadius));
		m_Body.GetLink(UPPER_LEFT_ARM).SetMatrix(initialMtx);

		// lower left arm
		initialMtx.a.Set(YAXIS);
		initialMtx.b.Set(-XAXIS);
		initialMtx.c.Set(ZAXIS);
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 leftElbowPosition(bodyMatrix.b);
		leftElbowPosition.Scale(halfTorsoLength);
		leftElbowPosition.AddScaled(bodyMatrix.a,elbowOffset);
		initialMtx.d.AddScaled(leftElbowPosition,bodyMatrix.a,lowerArmOffset);
		m_Body.GetLink(LOWER_LEFT_ARM).SetMatrix(initialMtx);

		// upper right leg
		Assert(compositeBound.GetBound(UPPER_RIGHT_LEG));
		phBound& upperLegBound = *compositeBound.GetBound(UPPER_RIGHT_LEG);
		float upperLegWidth = upperLegBound.GetBoundingBoxMax().GetXf() - upperLegBound.GetBoundingBoxMin().GetXf();
		float upperLegHeight = upperLegBound.GetBoundingBoxMax().GetYf() - upperLegBound.GetBoundingBoxMin().GetYf();
		float upperLegRadius = 0.5f*upperLegWidth;
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 rightHipPosition(bodyMatrix.b);
		float hipOffset = -halfTorsoHeight-pelvisRadius;
		rightHipPosition.Scale(hipOffset);
		float legHorizOffset = 0.75f*pelvisRadius;
		rightHipPosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
		float upperLegOffset = -0.5f*upperLegHeight;
		initialMtx.d.AddScaled(rightHipPosition,bodyMatrix.b,upperLegOffset);
		m_Body.GetLink(UPPER_RIGHT_LEG).SetMatrix(initialMtx);

		// lower right leg
		Assert(compositeBound.GetBound(LOWER_RIGHT_LEG));
		phBound& lowerLegBound = *compositeBound.GetBound(LOWER_RIGHT_LEG);
		float lowerLegHeight = lowerLegBound.GetBoundingBoxMax().GetYf() - lowerLegBound.GetBoundingBoxMin().GetYf();
		float lowerLegRadius = 0.5f*(lowerLegBound.GetBoundingBoxMax().GetXf()-lowerLegBound.GetBoundingBoxMin().GetXf());
		partRotation.Set(0.2f,0.0f,0.0f);
		initialMtx.Set(CreateRotatedMatrix(ORIGIN,partRotation));
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		float kneeOffset = hipOffset-upperLegHeight;
		Vector3 rightKneePosition(bodyMatrix.b);
		rightKneePosition.Scale(kneeOffset);
		rightKneePosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
		float lowerLegOffset = -0.5f*lowerLegHeight+fractionOverlap*Min(upperLegRadius,lowerLegRadius);
		initialMtx.d.AddScaled(rightKneePosition,bodyMatrix.b,lowerLegOffset);
		m_Body.GetLink(LOWER_RIGHT_LEG).SetMatrix(initialMtx);

		// upper left leg
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 leftHipPosition(bodyMatrix.b);
		leftHipPosition.Scale(hipOffset);
		leftHipPosition.AddScaled(bodyMatrix.a,legHorizOffset);
		initialMtx.d.AddScaled(leftHipPosition,bodyMatrix.b,upperLegOffset);
		m_Body.GetLink(UPPER_LEFT_LEG).SetMatrix(initialMtx);

		// lower left leg
		partRotation.Set(0.2f,0.0f,0.0f);
		initialMtx.Set(CreateRotatedMatrix(ORIGIN,partRotation));
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 leftKneePosition(bodyMatrix.b);
		leftKneePosition.Scale(kneeOffset);
		leftKneePosition.AddScaled(bodyMatrix.a,legHorizOffset);
		initialMtx.d.AddScaled(leftKneePosition,bodyMatrix.b,lowerLegOffset);
		m_Body.GetLink(LOWER_LEFT_LEG).SetMatrix(initialMtx);

		Vector3 rightAnklePosition,leftAnklePosition,rightWristPosition,leftWristPosition;
		if (numLinks>LEFT_FOOT)
		{
			// right foot
			Assert(compositeBound.GetBound(RIGHT_FOOT));
			phBound& footBound = *compositeBound.GetBound(RIGHT_FOOT);
			float ankleOffset = kneeOffset-lowerLegHeight;
			initialMtx.Transpose(bodyMatrix);
			rightAnklePosition.Scale(bodyMatrix.b,ankleOffset);
			rightAnklePosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
			float footHeight = footBound.GetBoundingBoxMax().GetYf() - footBound.GetBoundingBoxMin().GetYf();
			float footOffset = -0.5f*footHeight+fractionOverlap*Min(footHeight,lowerLegRadius);
			initialMtx.d.AddScaled(rightAnklePosition,bodyMatrix.b,footOffset);
			rightAnklePosition.SubtractScaled(bodyMatrix.c,0.06f);
			m_Body.GetLink(RIGHT_FOOT).SetMatrix(initialMtx);

			// left foot
			initialMtx.Transpose(bodyMatrix);
			leftAnklePosition.Scale(bodyMatrix.b,ankleOffset);
			leftAnklePosition.AddScaled(bodyMatrix.a,legHorizOffset);
			initialMtx.d.AddScaled(leftAnklePosition,bodyMatrix.b,footOffset);
			leftAnklePosition.SubtractScaled(bodyMatrix.c,0.06f);
			m_Body.GetLink(LEFT_FOOT).SetMatrix(initialMtx);

			if (numLinks>LEFT_HAND)
			{
				// right hand
				Assert(compositeBound.GetBound(RIGHT_HAND));
				phBound& handBound = *compositeBound.GetBound(RIGHT_HAND);
				initialMtx.Transpose(bodyMatrix);
				rightWristPosition.SubtractScaled(rightElbowPosition,bodyMatrix.a,lowerArmHeight);
				float handHeight = handBound.GetBoundingBoxMax().GetXf() - handBound.GetBoundingBoxMin().GetXf();
				float handOffset = -0.5f*handHeight+fractionOverlap*Min(handHeight,lowerArmRadius);
				initialMtx.d.AddScaled(rightWristPosition,bodyMatrix.a,handOffset);
		//		rightWristPosition.SubtractScaled(bodyMatrix.a,0.06f);
				m_Body.GetLink(RIGHT_HAND).SetMatrix(initialMtx);

				// left hand
				initialMtx.Transpose(bodyMatrix);
				leftWristPosition.AddScaled(leftElbowPosition,bodyMatrix.a,lowerArmHeight);
				initialMtx.d.SubtractScaled(leftWristPosition,bodyMatrix.a,handOffset);
		//		leftWristPosition.AddScaled(bodyMatrix.a,0.06f);
				m_Body.GetLink(LEFT_HAND).SetMatrix(initialMtx);
			}
		}

		// Position the body parts before joining them together.
		for (int i=0; i<numLinks; i++)
		{
			m_Body.ZeroLinkVelocities();
		}

		// Put the links and joints into the ArticulatedMultiBody.
		// Add the first link as the root.
		m_Body.AddRoot(m_Body.GetLink(TORSO));
		m_Body.SetMassAndAngInertia(TORSO,TORSO_MASS,VEC3V_TO_VECTOR3(torsoBound.GetComputeAngularInertia(TORSO_MASS)));

		// neck
		phJoint3Dof& neck = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(0));
		float maxLeanAngle = PH_DEG2RAD(60.0f);
		float maxTwistAngle = PH_DEG2RAD(100.0f);
#ifdef USE_SOFT_LIMITS
		const float softAngleLimitDiff = PH_DEG2RAD(10.0f);
#endif
		m_Body.AddChild(TORSO,neck,m_Body.GetLink(HEAD));
		m_Body.SetMassAndAngInertia(HEAD,HEAD_MASS,VEC3V_TO_VECTOR3(compositeBound.GetBound(HEAD)->GetComputeAngularInertia(HEAD_MASS)));
		neck.SetAxis(&m_Body, neckPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
		neck.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		neck.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// waist
		phJoint3Dof& waist = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(1));
		maxLeanAngle = PH_DEG2RAD(30.0f);
		maxTwistAngle = PH_DEG2RAD(30.0f);
		m_Body.AddChild(TORSO,waist,m_Body.GetLink(PELVIS));
		m_Body.SetMassAndAngInertia(PELVIS,PELVIS_MASS,VEC3V_TO_VECTOR3(compositeBound.GetBound(PELVIS)->GetComputeAngularInertia(PELVIS_MASS)));
		waist.SetAxis(&m_Body, waistPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		waist.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// right shoulder
		phJoint3Dof& rightShoulder = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(2));
		maxLeanAngle = PH_DEG2RAD(80.0f);
		maxTwistAngle = PH_DEG2RAD(60.0f);
		m_Body.AddChild(TORSO,rightShoulder,m_Body.GetLink(UPPER_RIGHT_ARM));
		m_Body.SetMassAndAngInertia(UPPER_RIGHT_ARM,UPPER_ARM_MASS,VEC3V_TO_VECTOR3(upperArmBound.GetComputeAngularInertia(UPPER_ARM_MASS)));
		rightShoulder.SetAxis(&m_Body, rightShoulderPosition,bodyMatrix.a,maxLeanAngle,maxTwistAngle);
		rightShoulder.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		rightShoulder.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// elbow and knee joint hard and soft angle limits
		const float hardMinAngle = PH_DEG2RAD(0.0f);
		const float hardMaxAngle = PH_DEG2RAD(80.0f);
		//const float softMinAngle = hardMinAngle+softAngleLimitDiff;
		//const float softMaxAngle = hardMaxAngle-softAngleLimitDiff;

		// right elbow
		phJoint1Dof& rightElbow = *static_cast<phJoint1Dof*>(&m_Body.GetJoint(3));
		m_Body.AddChild(UPPER_RIGHT_ARM,rightElbow,m_Body.GetLink(LOWER_RIGHT_ARM));
		m_Body.SetMassAndAngInertia(LOWER_RIGHT_ARM,LOWER_ARM_MASS,VEC3V_TO_VECTOR3(lowerArmBound.GetComputeAngularInertia(LOWER_ARM_MASS)));
		rightElbow.SetAxis(&m_Body, rightElbowPosition,bodyMatrix.b);
		rightElbow.SetAngleLimits(hardMinAngle,hardMaxAngle);
#ifdef USE_SOFT_LIMITS
		rightElbow.SetSoftAngleLimits(hardMinAngle+softAngleLimitDiff,hardMaxAngle-softAngleLimitDiff);
#endif

		// left shoulder
		phJoint3Dof& leftShoulder = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(4));
		m_Body.AddChild(TORSO,leftShoulder,m_Body.GetLink(UPPER_LEFT_ARM));
		m_Body.SetMassAndAngInertia(UPPER_LEFT_ARM,UPPER_ARM_MASS,VEC3V_TO_VECTOR3(upperArmBound.GetComputeAngularInertia(UPPER_ARM_MASS)));
		leftShoulder.SetAxis(&m_Body, leftShoulderPosition,bodyMatrix.a,maxLeanAngle,maxTwistAngle);
		leftShoulder.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		leftShoulder.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// left elbow
		phJoint1Dof& leftElbow = *static_cast<phJoint1Dof*>(&m_Body.GetJoint(5));
		m_Body.AddChild(UPPER_LEFT_ARM,leftElbow,m_Body.GetLink(LOWER_LEFT_ARM));
		m_Body.SetMassAndAngInertia(LOWER_LEFT_ARM,LOWER_ARM_MASS,VEC3V_TO_VECTOR3(lowerArmBound.GetComputeAngularInertia(LOWER_ARM_MASS)));
		leftElbow.SetAxis(&m_Body, leftElbowPosition,bodyMatrix.b);
		leftElbow.SetAngleLimits(-hardMaxAngle,hardMinAngle);
#ifdef USE_SOFT_LIMITS
		leftElbow.SetSoftAngleLimits(-hardMaxAngle+softAngleLimitDiff,hardMinAngle-softAngleLimitDiff);
#endif

		// right hip
		phJoint3Dof& rightHip = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(6));
		maxLeanAngle = PH_DEG2RAD(40.0f);
		maxTwistAngle = PH_DEG2RAD(45.0f);
		m_Body.AddChild(PELVIS,rightHip,m_Body.GetLink(UPPER_RIGHT_LEG));
		m_Body.SetMassAndAngInertia(UPPER_RIGHT_LEG,UPPER_LEG_MASS,VEC3V_TO_VECTOR3(upperLegBound.GetComputeAngularInertia(UPPER_LEG_MASS)));
		rightHip.SetAxis(&m_Body, rightHipPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
		rightHip.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		rightHip.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// right knee
		phJoint1Dof& rightKnee = *static_cast<phJoint1Dof*>(&m_Body.GetJoint(7));
		m_Body.AddChild(UPPER_RIGHT_LEG,rightKnee,m_Body.GetLink(LOWER_RIGHT_LEG));
		m_Body.SetMassAndAngInertia(LOWER_RIGHT_LEG,LOWER_LEG_MASS,VEC3V_TO_VECTOR3(lowerLegBound.GetComputeAngularInertia(LOWER_LEG_MASS)));
		rightKnee.SetAxis(&m_Body, rightKneePosition,bodyMatrix.a);
		rightKnee.SetAngleLimits(hardMinAngle,hardMaxAngle);
#ifdef USE_SOFT_LIMITS
		rightKnee.SetSoftAngleLimits(hardMinAngle+softAngleLimitDiff,hardMaxAngle-softAngleLimitDiff);
#endif

		// left hip
		phJoint3Dof& leftHip = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(8));
		m_Body.AddChild(PELVIS,leftHip,m_Body.GetLink(UPPER_LEFT_LEG));
		m_Body.SetMassAndAngInertia(UPPER_LEFT_LEG,UPPER_LEG_MASS,VEC3V_TO_VECTOR3(upperLegBound.GetComputeAngularInertia(UPPER_LEG_MASS)));
		leftHip.SetAxis(&m_Body, leftHipPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
		leftHip.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		leftHip.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// left knee
		phJoint1Dof& leftKnee = *static_cast<phJoint1Dof*>(&m_Body.GetJoint(9));
		m_Body.AddChild(UPPER_LEFT_LEG,leftKnee,m_Body.GetLink(LOWER_LEFT_LEG));
		m_Body.SetMassAndAngInertia(LOWER_LEFT_LEG,LOWER_LEG_MASS,VEC3V_TO_VECTOR3(lowerLegBound.GetComputeAngularInertia(LOWER_LEG_MASS)));
		leftKnee.SetAxis(&m_Body, leftKneePosition,bodyMatrix.a);
		leftKnee.SetAngleLimits(hardMinAngle,hardMaxAngle);
#ifdef USE_SOFT_LIMITS
		leftKnee.SetSoftAngleLimits(hardMinAngle+softAngleLimitDiff,hardMaxAngle-softAngleLimitDiff);
#endif

		if (numLinks>LEFT_FOOT)
		{
			// right ankle
			phJoint3Dof& rightAnkle = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(RIGHT_FOOT-1));
			m_Body.AddChild(LOWER_RIGHT_LEG,rightAnkle,m_Body.GetLink(RIGHT_FOOT));
			float FOOT_MASS = 3.0f;
			phBound& footBound = *compositeBound.GetBound(RIGHT_FOOT);
			m_Body.SetMassAndAngInertia(RIGHT_FOOT,FOOT_MASS,VEC3V_TO_VECTOR3(footBound.GetComputeAngularInertia(FOOT_MASS)));
			rightAnkle.SetAxis(&m_Body, rightAnklePosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
			rightAnkle.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
			rightAnkle.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

			// left ankle
			phJoint3Dof& leftAnkle = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(LEFT_FOOT-1));
			m_Body.AddChild(LOWER_LEFT_LEG,leftAnkle,m_Body.GetLink(LEFT_FOOT));
			m_Body.SetMassAndAngInertia(LEFT_FOOT,FOOT_MASS,VEC3V_TO_VECTOR3(footBound.GetComputeAngularInertia(FOOT_MASS)));
			leftAnkle.SetAxis(&m_Body, leftAnklePosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
			leftAnkle.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
			rightAnkle.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

			if (numLinks>LEFT_HAND)
			{
				// right wrist
				phJoint3Dof& rightWrist = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(RIGHT_HAND-1));
				m_Body.AddChild(LOWER_RIGHT_ARM,rightWrist,m_Body.GetLink(RIGHT_HAND));
				float HAND_MASS = 2.0f;
				phBound& handBound = *compositeBound.GetBound(RIGHT_HAND);
				m_Body.SetMassAndAngInertia(RIGHT_HAND,HAND_MASS,VEC3V_TO_VECTOR3(handBound.GetComputeAngularInertia(HAND_MASS)));
				rightWrist.SetAxis(&m_Body, rightWristPosition,bodyMatrix.a,maxLeanAngle,maxTwistAngle);
				rightWrist.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
				rightWrist.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

				// left wrist
				phJoint3Dof& leftWrist = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(LEFT_HAND-1));
				m_Body.AddChild(LOWER_LEFT_ARM,leftWrist,m_Body.GetLink(LEFT_HAND));
				m_Body.SetMassAndAngInertia(LEFT_HAND,HAND_MASS,VEC3V_TO_VECTOR3(handBound.GetComputeAngularInertia(HAND_MASS)));
				leftWrist.SetAxis(&m_Body, leftWristPosition,bodyMatrix.a,maxLeanAngle,maxTwistAngle);
				leftWrist.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
				leftWrist.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif
			}
		}

		m_Body.SetStiffness(0.2f);

		//m_Body.SetStiffness(0.5f);

		InitBody(archetype,instance,demoWorld);


		// Make the torso collidable with the lower arms and lower legs.
		m_Collider->SetPartsCanCollide(TORSO,LOWER_RIGHT_ARM);
		m_Collider->SetPartsCanCollide(TORSO,LOWER_LEFT_ARM);
		m_Collider->SetPartsCanCollide(TORSO,LOWER_RIGHT_LEG);
		m_Collider->SetPartsCanCollide(TORSO,LOWER_LEFT_LEG);

		// Make the head and pelvis collidable with the lower arms.
		m_Collider->SetPartsCanCollide(HEAD,LOWER_RIGHT_ARM);
		m_Collider->SetPartsCanCollide(HEAD,LOWER_LEFT_ARM);
		m_Collider->SetPartsCanCollide(PELVIS,LOWER_RIGHT_ARM);
		m_Collider->SetPartsCanCollide(PELVIS,LOWER_LEFT_ARM);

		// Make the lower arms collidable with the upper and lower legs.
		m_Collider->SetPartsCanCollide(LOWER_RIGHT_ARM,UPPER_RIGHT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_RIGHT_ARM,LOWER_RIGHT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_RIGHT_ARM,UPPER_LEFT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_RIGHT_ARM,LOWER_LEFT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_LEFT_ARM,UPPER_RIGHT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_LEFT_ARM,LOWER_RIGHT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_LEFT_ARM,UPPER_LEFT_LEG);
		m_Collider->SetPartsCanCollide(LOWER_LEFT_ARM,LOWER_LEFT_LEG);

		// Make the upper legs collidable with each other.
		m_Collider->SetPartsCanCollide(UPPER_RIGHT_LEG,UPPER_LEFT_LEG);

		// Make the lower legs collidable with each other.
		m_Collider->SetPartsCanCollide(LOWER_RIGHT_LEG,LOWER_LEFT_LEG);

		if (numBodyParts>RIGHT_FOOT)
		{
			// Make the feet collidable with each other.
			m_Collider->SetPartsCanCollide(RIGHT_FOOT,LEFT_FOOT);

			// Make the feet collidable with the opposite lower leg.
			m_Collider->SetPartsCanCollide(LOWER_RIGHT_LEG,LEFT_FOOT);
			m_Collider->SetPartsCanCollide(LOWER_LEFT_LEG,RIGHT_FOOT);
		}
	}
	else
	{
		Assertf(false, "Needs conversion to new constraint API");

#if 0 // Needs conversion to new constraint API
		// build ragdoll from rigid body insts with constraints
		m_Parts.Resize(11);

		//Add body parts and colliding body pairs to collision tester 
		if(m_CollisionTester) delete m_CollisionTester;
		m_CollisionTester = rage_new ConstrainedHumanBodyPartCollTester;
		ConstrainedHumanBodyPart* humanBodyParts[ConstrainedHumanBodyPartCollTester::MAX_NUM_BODY_PARTS];

		// torso
		const float fractionOverlap = 0.3f;
		InitGfx("humanoid\\torso");
		Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
		InitPhys("humanoid\\torso",bodyMatrix,true,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
		phRagdollPartInst* torsoInst = static_cast<phRagdollPartInst*>(GetPhysInst());
		phArchetypeDamp* torsoArchetype = static_cast<phArchetypeDamp*>(torsoInst->GetArchetype());
		phBound* torsoBound = torsoArchetype->GetBound();
		float torsoWidth = torsoBound->GetBoundingBoxMax().GetXf() - torsoBound->GetBoundingBoxMin().GetXf();
		float torsoHeight = torsoBound->GetBoundingBoxMax().GetYf() - torsoBound->GetBoundingBoxMin().GetYf();
		float torsoRadius = 0.5f * torsoWidth;
		float torsoLength = torsoHeight - torsoWidth;
		float halfTorsoLength = 0.5f*torsoLength;
		torsoArchetype->SetMass(TORSO_MASS);
		PHSIM->AddActiveObject(GetPhysInst());
		demoWorld.AddUpdateObject(this);
		humanBodyParts[TORSO]=m_CollisionTester->AddBodyPart(GetPhysInst());
		GetPhysInst()->SetUserData(humanBodyParts[TORSO]);

		float halfTorsoHeight = halfTorsoLength+torsoRadius;

		// head
		{
			m_Parts[HEAD] = rage_new phDemoObject;
			m_Parts[HEAD]->InitGfx("humanoid\\head");
			m_Parts[HEAD]->InitPhys("humanoid\\head",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[HEAD]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			float partRadius = partBound->GetRadiusAroundCentroid();
			partArchetype->SetMass(HEAD_MASS);
			Vector3 partRotation(0.5f,0.0f,0.0f);
			Matrix34 partInitialMatrix(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoHeight);
			Vector3 partPosition;
			partPosition.AddScaled(jointPosition,bodyMatrix.b,partRadius-fractionOverlap*Min(partRadius,torsoRadius));
			partInitialMatrix.d.Add(partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[HEAD]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));	
				pConstraintRotation->SetHardLimitMin(PH_DEG2RAD(-100.0f));
				pConstraintRotation->SetHardLimitMax(PH_DEG2RAD(100.0f));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(PH_DEG2RAD(-60.0f));
				pConstraintRotation->SetHardLimitMax(PH_DEG2RAD(60.0f));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(PH_DEG2RAD(-60.0f));
				pConstraintRotation->SetHardLimitMax(PH_DEG2RAD(60.0f));
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[HEAD]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[HEAD]);
		}

		// pelvis
		float pelvisRadius;
		{
			m_Parts[PELVIS] = rage_new phDemoObject;
			m_Parts[PELVIS]->InitGfx("humanoid\\pelvis");
			m_Parts[PELVIS]->InitPhys("humanoid\\pelvis",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[PELVIS]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(PELVIS_MASS);
			pelvisRadius = partBound->GetRadiusAroundCentroid();

			Matrix34 partInitialMatrix(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(-halfTorsoHeight);
			Vector3 partPosition;
			partPosition.SubtractScaled(jointPosition,bodyMatrix.b,pelvisRadius-fractionOverlap*Min(pelvisRadius,torsoRadius));
			partInitialMatrix.d.Add(partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[PELVIS]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				float maxLeanAngle = PH_DEG2RAD(30.0f);
				float maxTwistAngle = PH_DEG2RAD(30.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetHardLimitMin(-maxTwistAngle);
				pConstraintRotation->SetHardLimitMax(maxTwistAngle);
				pConstraintRotation->SetFixedRotation();
			
				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[PELVIS]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[PELVIS]);
		}

		// upper right arm
		float upperArmLength;
		float upperArmRadius;
		float upperArmHeight;
		{
			m_Parts[UPPER_RIGHT_ARM] = rage_new phDemoObject;
			m_Parts[UPPER_RIGHT_ARM]->InitGfx("humanoid\\upper_arm");
			m_Parts[UPPER_RIGHT_ARM]->InitPhys("humanoid\\upper_arm",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[UPPER_RIGHT_ARM]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(UPPER_ARM_MASS);
			float upperArmWidth = partBound->GetBoundingBoxMax().GetXf() - partBound->GetBoundingBoxMin().GetXf();
			upperArmRadius = 0.5f * upperArmWidth;
			upperArmHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			upperArmLength = upperArmHeight - upperArmWidth;
			Matrix34 partInitialMatrix;
			partInitialMatrix.a.Set(-YAXIS);
			partInitialMatrix.b.Set(XAXIS);
			partInitialMatrix.c.Set(ZAXIS);
			partInitialMatrix.Dot3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoLength);
			jointPosition.SubtractScaled(bodyMatrix.a,torsoRadius);
			Vector3 partPosition;
			partPosition.SubtractScaled(jointPosition,bodyMatrix.a,0.5f*upperArmHeight-
				fractionOverlap*Min(upperArmRadius,torsoRadius));
			partInitialMatrix.d.Add(bodyMatrix.d, partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[UPPER_RIGHT_ARM]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				float maxLeanAngle = PH_DEG2RAD(80.0f);
				float maxTwistAngle = PH_DEG2RAD(60.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxTwistAngle);
				pConstraintRotation->SetHardLimitMax(maxTwistAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[UPPER_RIGHT_ARM]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[UPPER_RIGHT_ARM]);
		}

		// lower right arm
		{
			m_Parts[LOWER_RIGHT_ARM] = rage_new phDemoObject;
			m_Parts[LOWER_RIGHT_ARM]->InitGfx("humanoid\\lower_arm");
			m_Parts[LOWER_RIGHT_ARM]->InitPhys("humanoid\\lower_arm",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[LOWER_RIGHT_ARM]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(LOWER_ARM_MASS);
			float lowerArmWidth = partBound->GetBoundingBoxMax().GetXf() - partBound->GetBoundingBoxMin().GetXf();
			float lowerArmHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			float lowerArmRadius = 0.5f*lowerArmWidth;
			Matrix34 partInitialMatrix;
			partInitialMatrix.a.Set(-YAXIS);
			partInitialMatrix.b.Set(XAXIS);
			partInitialMatrix.c.Set(ZAXIS);
			partInitialMatrix.Dot3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoLength);
			float elbowOffset = torsoRadius+upperArmHeight-fractionOverlap*Min(upperArmRadius,torsoRadius);
			jointPosition.SubtractScaled(bodyMatrix.a,elbowOffset);
			float lowerArmOffset = 0.5f*lowerArmHeight-fractionOverlap*Min(lowerArmRadius,upperArmRadius);
			partInitialMatrix.d.SubtractScaled(jointPosition,bodyMatrix.a,lowerArmOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[LOWER_RIGHT_ARM]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, m_Parts[UPPER_RIGHT_ARM]->GetPhysInst(), partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(2,Vector3(0,0,1));
				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				const float hardMaxAngle = PH_DEG2RAD(80.0f);
				pConstraintRotation->SetHardLimitMin(-hardMaxAngle);
				pConstraintRotation->SetHardLimitMax(hardMaxAngle);
			}

			humanBodyParts[LOWER_RIGHT_ARM]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[LOWER_RIGHT_ARM]);
		}

		// upper left arm
		{
			m_Parts[UPPER_LEFT_ARM] = rage_new phDemoObject;
			m_Parts[UPPER_LEFT_ARM]->InitGfx("humanoid\\upper_arm");
			m_Parts[UPPER_LEFT_ARM]->InitPhys("humanoid\\upper_arm",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[UPPER_LEFT_ARM]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			partArchetype->SetMass(UPPER_ARM_MASS);
			Matrix34 partInitialMatrix;
			partInitialMatrix.Identity();
			partInitialMatrix.a.Set(YAXIS);
			partInitialMatrix.b.Set(-XAXIS);
			partInitialMatrix.c.Set(ZAXIS);
			partInitialMatrix.Dot3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoLength);
			jointPosition.AddScaled(bodyMatrix.a,torsoRadius);
			Vector3 partPosition;
			partPosition.AddScaled(jointPosition,bodyMatrix.a,0.5f*upperArmLength+upperArmRadius-fractionOverlap*Min(upperArmRadius,torsoRadius));
			partInitialMatrix.d.Add(bodyMatrix.d, partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[UPPER_LEFT_ARM]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				float maxLeanAngle = PH_DEG2RAD(80.0f);
				float maxTwistAngle = PH_DEG2RAD(60.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(-1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxTwistAngle);
				pConstraintRotation->SetHardLimitMax(maxTwistAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,-1,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,-1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[UPPER_LEFT_ARM]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[UPPER_LEFT_ARM]);
		}

		// lower left arm
		{
			m_Parts[LOWER_LEFT_ARM] = rage_new phDemoObject;
			m_Parts[LOWER_LEFT_ARM]->InitGfx("humanoid\\lower_arm");
			m_Parts[LOWER_LEFT_ARM]->InitPhys("humanoid\\lower_arm",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[LOWER_LEFT_ARM]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(LOWER_ARM_MASS);
			float lowerArmWidth = partBound->GetBoundingBoxMax().GetXf() - partBound->GetBoundingBoxMin().GetXf();
			float lowerArmHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			float lowerArmRadius = 0.5f*lowerArmWidth;
			Matrix34 partInitialMatrix;
			partInitialMatrix.a.Set(YAXIS);
			partInitialMatrix.b.Set(-XAXIS);
			partInitialMatrix.c.Set(ZAXIS);
			partInitialMatrix.Dot3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoLength);
			float elbowOffset = torsoRadius+upperArmHeight-fractionOverlap*Min(upperArmRadius,torsoRadius);
			jointPosition.AddScaled(bodyMatrix.a,elbowOffset);
			float lowerArmOffset = 0.5f*lowerArmHeight-fractionOverlap*Min(lowerArmRadius,upperArmRadius);
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.a,lowerArmOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[LOWER_LEFT_ARM]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, m_Parts[UPPER_LEFT_ARM]->GetPhysInst(), partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(2,Vector3(0,0,-1));
				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,-1));
				const float hardMaxAngle = PH_DEG2RAD(80.0f);
				pConstraintRotation->SetHardLimitMin(-hardMaxAngle);
				pConstraintRotation->SetHardLimitMax(hardMaxAngle);
			}

			humanBodyParts[LOWER_LEFT_ARM]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[LOWER_LEFT_ARM]);
		}

		// upper right leg
		float upperLegRadius;
		float upperLegHeight;
		float hipOffset;
		float legHorizOffset;
		{
			m_Parts[UPPER_RIGHT_LEG] = rage_new phDemoObject;
			m_Parts[UPPER_RIGHT_LEG]->InitGfx("humanoid\\upper_leg");
			m_Parts[UPPER_RIGHT_LEG]->InitPhys("humanoid\\upper_leg",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[UPPER_RIGHT_LEG]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(UPPER_LEG_MASS);
			float upperLegWidth = partBound->GetBoundingBoxMax().GetXf() - partBound->GetBoundingBoxMin().GetXf();
			upperLegHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			upperLegRadius = 0.5f*upperLegWidth;
			Matrix34 partInitialMatrix;
			partInitialMatrix.Set3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			hipOffset = -halfTorsoHeight-pelvisRadius;
			jointPosition.Scale(hipOffset);
			legHorizOffset = 0.75f*pelvisRadius;
			jointPosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
			float upperLegOffset = -0.5f*upperLegHeight;
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.b,upperLegOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[UPPER_RIGHT_LEG]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, m_Parts[PELVIS]->GetPhysInst(), partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				const float maxLeanAngle = PH_DEG2RAD(40.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[UPPER_RIGHT_LEG]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[UPPER_RIGHT_LEG]);
		}

		// lower right leg
		{
			m_Parts[LOWER_RIGHT_LEG] = rage_new phDemoObject;
			m_Parts[LOWER_RIGHT_LEG]->InitGfx("humanoid\\lower_leg");
			m_Parts[LOWER_RIGHT_LEG]->InitPhys("humanoid\\lower_leg",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[LOWER_RIGHT_LEG]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(LOWER_LEG_MASS);
			float lowerLegHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			float lowerLegRadius = 0.5f*(partBound->GetBoundingBoxMax().GetXf()-partBound->GetBoundingBoxMin().GetXf());
			Vector3 partRotation(0.2f,0.0f,0.0f);
			Matrix34 partInitialMatrix;
			partInitialMatrix.Set(CreateRotatedMatrix(ORIGIN,partRotation));
			partInitialMatrix.Dot3x3(bodyMatrix);
			partInitialMatrix.Transpose();
			float kneeOffset = hipOffset-upperLegHeight;
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(kneeOffset);
			jointPosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
			float lowerLegOffset = -0.5f*lowerLegHeight+fractionOverlap*Min(upperLegRadius,lowerLegRadius);
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.b,lowerLegOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[LOWER_RIGHT_LEG]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, m_Parts[UPPER_RIGHT_LEG]->GetPhysInst(), partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(2,Vector3(1,0,0));
				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				const float hardMaxAngle = PH_DEG2RAD(80.0f);
				pConstraintRotation->SetHardLimitMin(0);
				pConstraintRotation->SetHardLimitMax(hardMaxAngle);
			}

			humanBodyParts[LOWER_RIGHT_LEG]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[LOWER_RIGHT_LEG]);
		}

		// upper left leg
		{
			m_Parts[UPPER_LEFT_LEG] = rage_new phDemoObject;
			m_Parts[UPPER_LEFT_LEG]->InitGfx("humanoid\\upper_leg");
			m_Parts[UPPER_LEFT_LEG]->InitPhys("humanoid\\upper_leg",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[UPPER_LEFT_LEG]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			partArchetype->SetMass(UPPER_LEG_MASS);
			Matrix34 partInitialMatrix;
			partInitialMatrix.Set3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			hipOffset = -halfTorsoHeight-pelvisRadius;
			jointPosition.Scale(hipOffset);
			legHorizOffset = 0.75f*pelvisRadius;
			jointPosition.AddScaled(bodyMatrix.a,legHorizOffset);
			float upperLegOffset = -0.5f*upperLegHeight;
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.b,upperLegOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[UPPER_LEFT_LEG]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, m_Parts[PELVIS]->GetPhysInst(), partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				const float maxLeanAngle = PH_DEG2RAD(40.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[UPPER_LEFT_LEG]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[UPPER_LEFT_LEG]);
		}

		// lower left leg
		{
			m_Parts[LOWER_LEFT_LEG] = rage_new phDemoObject;
			m_Parts[LOWER_LEFT_LEG]->InitGfx("humanoid\\lower_leg");
			m_Parts[LOWER_LEFT_LEG]->InitPhys("humanoid\\lower_leg",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[LOWER_LEFT_LEG]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(LOWER_LEG_MASS);
			float lowerLegHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			float lowerLegRadius = 0.5f*(partBound->GetBoundingBoxMax().GetXf()-partBound->GetBoundingBoxMin().GetXf());
			Vector3 partRotation(0.2f,0.0f,0.0f);
			Matrix34 partInitialMatrix;
			partInitialMatrix.Set(CreateRotatedMatrix(ORIGIN,partRotation));
			partInitialMatrix.Dot3x3(bodyMatrix);
			partInitialMatrix.Transpose();
			float kneeOffset = hipOffset-upperLegHeight;
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(kneeOffset);
			jointPosition.AddScaled(bodyMatrix.a,legHorizOffset);
			float lowerLegOffset = -0.5f*lowerLegHeight+fractionOverlap*Min(upperLegRadius,lowerLegRadius);
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.b,lowerLegOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[LOWER_LEFT_LEG]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, m_Parts[UPPER_LEFT_LEG]->GetPhysInst(), partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(2,Vector3(1,0,0));
				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				const float hardMaxAngle = PH_DEG2RAD(80.0f);
				pConstraintRotation->SetHardLimitMin(0);
				pConstraintRotation->SetHardLimitMax(hardMaxAngle);
			}

			humanBodyParts[LOWER_LEFT_LEG]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[LOWER_LEFT_LEG]);
		}

		// Make the torso collidable with the lower arms and lower legs.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[LOWER_RIGHT_ARM]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[LOWER_LEFT_ARM]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[LOWER_RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[LOWER_LEFT_LEG]);

		// Make the head and pelvis collidable with the lower arms.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[HEAD],humanBodyParts[LOWER_RIGHT_ARM]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[HEAD],humanBodyParts[LOWER_LEFT_ARM]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[PELVIS],humanBodyParts[LOWER_RIGHT_ARM]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[PELVIS],humanBodyParts[LOWER_LEFT_ARM]);

		// Make the lower arms collidable with the upper and lower legs.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_RIGHT_ARM],humanBodyParts[UPPER_RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_RIGHT_ARM],humanBodyParts[LOWER_RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_RIGHT_ARM],humanBodyParts[UPPER_LEFT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_RIGHT_ARM],humanBodyParts[LOWER_LEFT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_LEFT_ARM],humanBodyParts[UPPER_RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_LEFT_ARM],humanBodyParts[LOWER_RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_LEFT_ARM],humanBodyParts[UPPER_LEFT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_LEFT_ARM],humanBodyParts[LOWER_LEFT_LEG]);

		// Make the upper legs collidable with each other.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[UPPER_RIGHT_LEG],humanBodyParts[UPPER_LEFT_LEG]);

		// Make the lower legs collidable with each other.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LOWER_RIGHT_LEG],humanBodyParts[LOWER_LEFT_LEG]);
#endif
	}
}


void phArticulatedObject::InitHuman6Bone (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation, const Vector3& cgOffset, int UNUSED_PARAM(numBodyParts))
{
	m_Articulated = phDemoWorld::GetArticulatedRagdolls();

	const char* defaultName = "humanoid_6";
	char name[RAGE_MAX_PATH];
	formatf(name,255,"%s",defaultName);

	float TORSO_MASS = 35.0f;
	float HEAD_MASS = 5.0f;
	float ARM_MASS = 5.0f;
	float LEG_MASS = 16.0f;

	//	0	torso
	//	1	head
	//	2	right arm
	//	3	left arm
	//	4	right leg
	//	5	left leg

	enum
	{
		TORSO=0,
		HEAD,
		RIGHT_ARM,
		LEFT_ARM,
		RIGHT_LEG,
		LEFT_LEG,
		MAX_NUM_FOOTLESS_BODY_PARTS,
	};


	if (phDemoWorld::GetArticulatedRagdolls())
	{
		InitGfx(name);
		Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
		InitPhys(name,bodyMatrix,true);
		phInst* instance = GetPhysInst();
		phArchetypePhys* archetype = static_cast<phArchetypePhys*>(instance->GetArchetype());
		Assert(archetype->GetBound() && archetype->GetBound()->GetType()==phBound::COMPOSITE);
		phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(archetype->GetBound());

		compositeBound.SetCGOffset(RCC_VEC3V(cgOffset));

		int numLinks = compositeBound.GetNumBounds();
		int linkIndex;
		for (linkIndex=0; linkIndex<numLinks; linkIndex++)
		{
			m_Body.SetBodyPart(linkIndex,rage_new phArticulatedBodyPart());
		}

		m_Body.SetJoint(0,rage_new phJoint3Dof());
		m_Body.SetJoint(1,rage_new phJoint3Dof());
		m_Body.SetJoint(2,rage_new phJoint3Dof());
		m_Body.SetJoint(3,rage_new phJoint1Dof());
		m_Body.SetJoint(4,rage_new phJoint3Dof());

		// torso
		const float fractionOverlap = 0.3f;
		Assert(compositeBound.GetBound(TORSO));
		phBound& torsoBound = *compositeBound.GetBound(TORSO);
		float torsoWidth = torsoBound.GetBoundingBoxMax().GetXf() - torsoBound.GetBoundingBoxMin().GetXf();
		float torsoHeight = torsoBound.GetBoundingBoxMax().GetYf() - torsoBound.GetBoundingBoxMin().GetYf();
		float torsoRadius = 0.5f * torsoWidth;
		float torsoLength = torsoHeight - torsoWidth;
		float halfTorsoLength = 0.5f*torsoLength;
		m_Body.SetMassAndAngInertia(TORSO,TORSO_MASS,VEC3V_TO_VECTOR3(torsoBound.GetComputeAngularInertia(TORSO_MASS)));
		Matrix34 initialMtx;
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		initialMtx.d.Zero();
		m_Body.GetLink(TORSO).SetMatrix(initialMtx);

		// head
		Assert(compositeBound.GetBound(HEAD));
		float headRadius = compositeBound.GetBound(HEAD)->GetRadiusAroundCentroid();
		m_Body.SetMassAndAngInertia(HEAD,HEAD_MASS,VEC3V_TO_VECTOR3(compositeBound.GetBound(HEAD)->GetComputeAngularInertia(HEAD_MASS)));
		float halfTorsoHeight = halfTorsoLength+torsoRadius;
		Vector3 partRotation(0.5f,0.0f,0.0f);
		Matrix34 partInitialMatrix(CreateRotatedMatrix(ORIGIN,partRotation));
		partInitialMatrix.Dot3x3(bodyMatrix);
		partInitialMatrix.Transpose();
		Vector3 neckPosition(bodyMatrix.b);
		neckPosition.Scale(halfTorsoHeight);
		partInitialMatrix.d.AddScaled(neckPosition,bodyMatrix.b,headRadius-fractionOverlap*Min(headRadius,torsoRadius));
		m_Body.GetLink(HEAD).SetMatrix(partInitialMatrix);

		// right arm
		Assert(compositeBound.GetBound(RIGHT_ARM));
		phBound& armBound = *compositeBound.GetBound(RIGHT_ARM);
		float armWidth = armBound.GetBoundingBoxMax().GetXf() - armBound.GetBoundingBoxMin().GetXf();
		float armRadius = 0.5f * armWidth;
		float armHeight = armBound.GetBoundingBoxMax().GetYf() - armBound.GetBoundingBoxMin().GetYf();
		float armLength = armHeight - armWidth;
		m_Body.SetMassAndAngInertia(RIGHT_ARM,ARM_MASS,VEC3V_TO_VECTOR3(armBound.GetComputeAngularInertia(ARM_MASS)));
		initialMtx.a.Set(-YAXIS);
		initialMtx.b.Set(XAXIS);
		initialMtx.c.Set(ZAXIS);
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 rightShoulderPosition(bodyMatrix.b);
		rightShoulderPosition.Scale(halfTorsoLength);
		rightShoulderPosition.SubtractScaled(bodyMatrix.a,torsoRadius);
		initialMtx.d.SubtractScaled(rightShoulderPosition,bodyMatrix.a,0.5f*armHeight-
			fractionOverlap*Min(armRadius,torsoRadius));
		m_Body.GetLink(RIGHT_ARM).SetMatrix(initialMtx);

		// left arm
		m_Body.SetMassAndAngInertia(LEFT_ARM,ARM_MASS,VEC3V_TO_VECTOR3(armBound.GetComputeAngularInertia(ARM_MASS)));
		initialMtx.a.Set(YAXIS);
		initialMtx.b.Set(-XAXIS);
		initialMtx.c.Set(ZAXIS);
		initialMtx.Dot3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 leftShoulderPosition(bodyMatrix.b);
		leftShoulderPosition.Scale(halfTorsoLength);
		leftShoulderPosition.AddScaled(bodyMatrix.a,torsoRadius);
		initialMtx.d.AddScaled(leftShoulderPosition,bodyMatrix.a,0.5f*armLength+armRadius-
			fractionOverlap*Min(armRadius,torsoRadius));
		m_Body.GetLink(LEFT_ARM).SetMatrix(initialMtx);

		// right leg
		Assert(compositeBound.GetBound(RIGHT_LEG));
		phBound& legBound = *compositeBound.GetBound(RIGHT_LEG);
		//float legWidth = legBound.GetBoundingBoxMax().GetXf() - legBound.GetBoundingBoxMin().GetXf();
		float legHeight = legBound.GetBoundingBoxMax().GetYf() - legBound.GetBoundingBoxMin().GetYf();
		//float legRadius = 0.5f*legWidth;
		m_Body.SetMassAndAngInertia(RIGHT_LEG,LEG_MASS,VEC3V_TO_VECTOR3(legBound.GetComputeAngularInertia(LEG_MASS)));
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 rightHipPosition(bodyMatrix.b);
		float hipOffset = -halfTorsoLength-torsoRadius;
		rightHipPosition.Scale(hipOffset);
		float legHorizOffset = 0.75f*torsoRadius;
		rightHipPosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
		float legOffset = -0.5f*legHeight;
		initialMtx.d.AddScaled(rightHipPosition,bodyMatrix.b,legOffset);
		m_Body.GetLink(RIGHT_LEG).SetMatrix(initialMtx);

		// upper left leg
		m_Body.SetMassAndAngInertia(LEFT_LEG,LEG_MASS,VEC3V_TO_VECTOR3(legBound.GetComputeAngularInertia(LEG_MASS)));
		initialMtx.Set3x3(bodyMatrix);
		initialMtx.Transpose();
		Vector3 leftHipPosition(bodyMatrix.b);
		leftHipPosition.Scale(hipOffset);
		leftHipPosition.AddScaled(bodyMatrix.a,legHorizOffset);
		initialMtx.d.AddScaled(leftHipPosition,bodyMatrix.b,legOffset);
		m_Body.GetLink(LEFT_LEG).SetMatrix(initialMtx);

		// Position the body parts before joining them together.
		for (int i=0; i<numLinks; i++)
		{
			m_Body.ZeroLinkVelocities();
		}

		// Put the links and joints into the ArticulatedMultiBody.
		// Add the first link as the root.
		m_Body.AddRoot(m_Body.GetLink(TORSO));

		// neck
		phJoint3Dof& neck = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(0));
		float maxLeanAngle = PH_DEG2RAD(60.0f);
		float maxTwistAngle = PH_DEG2RAD(100.0f);
#ifdef USE_SOFT_LIMITS
		const float softAngleLimitDiff = PH_DEG2RAD(10.0f);
#endif
		m_Body.AddChild(TORSO,neck,m_Body.GetLink(HEAD));
		neck.SetAxis(&m_Body, neckPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
		neck.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		neck.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// right shoulder
		phJoint3Dof& rightShoulder = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(1));
		maxLeanAngle = PH_DEG2RAD(80.0f);
		maxTwistAngle = PH_DEG2RAD(60.0f);
		m_Body.AddChild(TORSO,rightShoulder,m_Body.GetLink(RIGHT_ARM));
		rightShoulder.SetAxis(&m_Body, rightShoulderPosition,bodyMatrix.a,maxLeanAngle,maxTwistAngle);
		rightShoulder.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		rightShoulder.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// left shoulder
		phJoint3Dof& leftShoulder = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(2));
		m_Body.AddChild(TORSO,leftShoulder,m_Body.GetLink(LEFT_ARM));
		leftShoulder.SetAxis(&m_Body, leftShoulderPosition,bodyMatrix.a,maxLeanAngle,maxTwistAngle);
		leftShoulder.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		leftShoulder.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// right hip
		phJoint3Dof& rightHip = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(3));
		maxLeanAngle = PH_DEG2RAD(40.0f);
		maxTwistAngle = PH_DEG2RAD(45.0f);
		m_Body.AddChild(TORSO,rightHip,m_Body.GetLink(RIGHT_LEG));
		rightHip.SetAxis(&m_Body, rightHipPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
		rightHip.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		rightHip.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		// left hip
		phJoint3Dof& leftHip = *static_cast<phJoint3Dof*>(&m_Body.GetJoint(4));
		m_Body.AddChild(TORSO,leftHip,m_Body.GetLink(LEFT_LEG));
		leftHip.SetAxis(&m_Body, leftHipPosition,bodyMatrix.b,maxLeanAngle,maxTwistAngle);
		leftHip.SetAngleLimits(maxLeanAngle,maxTwistAngle);
#ifdef USE_SOFT_LIMITS
		leftHip.SetSoftLimitRatio((maxLeanAngle-softAngleLimitDiff) / maxLeanAngle);
#endif

		m_Body.SetStiffness(0.2f);
		//m_Body.SetStiffness(0.5f);

		InitBody(archetype,instance,demoWorld);

		// Make the torso collidable with the lower arms and lower legs.
		//m_Collider->SetPartsCanCollide(TORSO,IGHT_ARM);
		//m_Collider->SetPartsCanCollide(TORSO,LEFT_ARM);
		//m_Collider->SetPartsCanCollide(TORSO,RIGHT_LEG);
		//m_Collider->SetPartsCanCollide(TORSO,LEFT_LEG);

		// Make the head and pelvis collidable with the lower arms.
		m_Collider->SetPartsCanCollide(HEAD,RIGHT_ARM);
		m_Collider->SetPartsCanCollide(HEAD,LEFT_ARM);

		// Make the lower arms collidable with the upper and lower legs.
		m_Collider->SetPartsCanCollide(RIGHT_ARM,RIGHT_LEG);
		m_Collider->SetPartsCanCollide(RIGHT_ARM,LEFT_LEG);
		m_Collider->SetPartsCanCollide(LEFT_ARM,RIGHT_LEG);
		m_Collider->SetPartsCanCollide(LEFT_ARM,LEFT_LEG);

		// Make the upper legs collidable with each other.
		m_Collider->SetPartsCanCollide(RIGHT_LEG,LEFT_LEG);
	}
	else
	{
		Assertf(false, "Needs conversion to new constraint API");

#if 0
		// build ragdoll from rigid body insts with constraints
		m_Parts.Resize(MAX_NUM_FOOTLESS_BODY_PARTS);

		//Add body parts and colliding body pairs to collision tester 
		if(m_CollisionTester) delete m_CollisionTester;
		m_CollisionTester = rage_new ConstrainedHumanBodyPartCollTester;
		ConstrainedHumanBodyPart* humanBodyParts[ConstrainedHumanBodyPartCollTester::MAX_NUM_BODY_PARTS];

		// torso
		const float fractionOverlap = 0.3f;
		InitGfx("humanoid_6\\torso");
		Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
		InitPhys("humanoid_6\\torso",bodyMatrix,true,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
		phRagdollPartInst* torsoInst = static_cast<phRagdollPartInst*>(GetPhysInst());
		phArchetypeDamp* torsoArchetype = static_cast<phArchetypeDamp*>(torsoInst->GetArchetype());
		phBound* torsoBound = torsoArchetype->GetBound();
		float torsoWidth = torsoBound->GetBoundingBoxMax().GetXf() - torsoBound->GetBoundingBoxMin().GetXf();
		float torsoHeight = torsoBound->GetBoundingBoxMax().GetYf() - torsoBound->GetBoundingBoxMin().GetYf();
		float torsoRadius = 0.5f * torsoWidth;
		float torsoLength = torsoHeight - torsoWidth;
		float halfTorsoLength = 0.5f*torsoLength;
		torsoArchetype->SetMass(TORSO_MASS);
		PHSIM->AddActiveObject(GetPhysInst());
		demoWorld.AddUpdateObject(this);
		humanBodyParts[TORSO]=m_CollisionTester->AddBodyPart(GetPhysInst());
		GetPhysInst()->SetUserData(humanBodyParts[TORSO]);

		float halfTorsoHeight = halfTorsoLength+torsoRadius;

		// head
		{
			m_Parts[HEAD] = rage_new phDemoObject;
			m_Parts[HEAD]->InitGfx("humanoid_6\\head");
			m_Parts[HEAD]->InitPhys("humanoid_6\\head",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[HEAD]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			float partRadius = partBound->GetRadiusAroundCentroid();
			partArchetype->SetMass(HEAD_MASS);
			Vector3 partRotation(0.5f,0.0f,0.0f);
			Matrix34 partInitialMatrix(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoHeight);
			Vector3 partPosition;
			partPosition.AddScaled(jointPosition,bodyMatrix.b,partRadius-fractionOverlap*Min(partRadius,torsoRadius));
			partInitialMatrix.d.Add(partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[HEAD]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));	
				pConstraintRotation->SetHardLimitMin(PH_DEG2RAD(-100.0f));
				pConstraintRotation->SetHardLimitMax(PH_DEG2RAD(100.0f));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(PH_DEG2RAD(-60.0f));
				pConstraintRotation->SetHardLimitMax(PH_DEG2RAD(60.0f));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(PH_DEG2RAD(-60.0f));
				pConstraintRotation->SetHardLimitMax(PH_DEG2RAD(60.0f));
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[HEAD]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[HEAD]);
		}

		// right arm
		float armLength;
		float armRadius;
		float armHeight;
		{
			m_Parts[RIGHT_ARM] = rage_new phDemoObject;
			m_Parts[RIGHT_ARM]->InitGfx("humanoid_6\\arm");
			m_Parts[RIGHT_ARM]->InitPhys("humanoid_6\\arm",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[RIGHT_ARM]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(ARM_MASS);
			float armWidth = partBound->GetBoundingBoxMax().GetXf() - partBound->GetBoundingBoxMin().GetXf();
			armRadius = 0.5f * armWidth;
			armHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			armLength = armHeight - armWidth;
			Matrix34 partInitialMatrix;
			partInitialMatrix.a.Set(-YAXIS);
			partInitialMatrix.b.Set(XAXIS);
			partInitialMatrix.c.Set(ZAXIS);
			partInitialMatrix.Dot3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoLength);
			jointPosition.SubtractScaled(bodyMatrix.a,torsoRadius);
			Vector3 partPosition;
			partPosition.SubtractScaled(jointPosition,bodyMatrix.a,0.5f*armHeight-
				fractionOverlap*Min(armRadius,torsoRadius));
			partInitialMatrix.d.Add(bodyMatrix.d, partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[RIGHT_ARM]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				float maxLeanAngle = PH_DEG2RAD(80.0f);
				float maxTwistAngle = PH_DEG2RAD(60.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxTwistAngle);
				pConstraintRotation->SetHardLimitMax(maxTwistAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[RIGHT_ARM]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[RIGHT_ARM]);
		}

		// left arm
		{
			m_Parts[LEFT_ARM] = rage_new phDemoObject;
			m_Parts[LEFT_ARM]->InitGfx("humanoid_6\\arm");
			m_Parts[LEFT_ARM]->InitPhys("humanoid_6\\arm",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[LEFT_ARM]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			partArchetype->SetMass(ARM_MASS);
			Matrix34 partInitialMatrix;
			partInitialMatrix.Identity();
			partInitialMatrix.a.Set(YAXIS);
			partInitialMatrix.b.Set(-XAXIS);
			partInitialMatrix.c.Set(ZAXIS);
			partInitialMatrix.Dot3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			jointPosition.Scale(halfTorsoLength);
			jointPosition.AddScaled(bodyMatrix.a,torsoRadius);
			Vector3 partPosition;
			partPosition.AddScaled(jointPosition,bodyMatrix.a,0.5f*armLength+armRadius-
				fractionOverlap*Min(armRadius,torsoRadius));
			partInitialMatrix.d.Add(bodyMatrix.d, partPosition);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[LEFT_ARM]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				float maxLeanAngle = PH_DEG2RAD(80.0f);
				float maxTwistAngle = PH_DEG2RAD(60.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(-1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxTwistAngle);
				pConstraintRotation->SetHardLimitMax(maxTwistAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,-1,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,-1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[LEFT_ARM]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[LEFT_ARM]);
		}

		// right leg
		//float legRadius;
		float legHeight;
		float hipOffset;
		float legHorizOffset;
		{
			m_Parts[RIGHT_LEG] = rage_new phDemoObject;
			m_Parts[RIGHT_LEG]->InitGfx("humanoid_6\\leg");
			m_Parts[RIGHT_LEG]->InitPhys("humanoid_6\\leg",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[RIGHT_LEG]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			phBound* partBound = partArchetype->GetBound();
			partArchetype->SetMass(LEG_MASS);
			//float upperLegWidth = partBound->GetBoundingBoxMax().GetXf() - partBound->GetBoundingBoxMin().GetXf();
			legHeight = partBound->GetBoundingBoxMax().GetYf() - partBound->GetBoundingBoxMin().GetYf();
			//legRadius = 0.5f*upperLegWidth;
			Matrix34 partInitialMatrix;
			partInitialMatrix.Set3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			hipOffset = -halfTorsoLength-torsoRadius;
			jointPosition.Scale(hipOffset);
			legHorizOffset = 0.75f*torsoRadius;
			jointPosition.SubtractScaled(bodyMatrix.a,legHorizOffset);
			float upperLegOffset = -0.5f*legHeight;
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.b,upperLegOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[RIGHT_LEG]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				const float maxLeanAngle = PH_DEG2RAD(40.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[RIGHT_LEG]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[RIGHT_LEG]);
		}

		// left leg
		{
			m_Parts[LEFT_LEG] = rage_new phDemoObject;
			m_Parts[LEFT_LEG]->InitGfx("humanoid_6\\leg");
			m_Parts[LEFT_LEG]->InitPhys("humanoid_6\\leg",bodyMatrix,false,MakeFunctorRet(*this, &phArticulatedObject::CreateRagdollInst));
			phInst* partInst = m_Parts[LEFT_LEG]->GetPhysInst();
			phArchetypeDamp* partArchetype = static_cast<phArchetypeDamp*>(partInst->GetArchetype());
			partArchetype->SetMass(LEG_MASS);
			Matrix34 partInitialMatrix;
			partInitialMatrix.Set3x3(bodyMatrix);
			Vector3 jointPosition(bodyMatrix.b);
			hipOffset = -halfTorsoLength-torsoRadius;
			jointPosition.Scale(hipOffset);
			legHorizOffset = 0.75f*torsoRadius;
			jointPosition.AddScaled(bodyMatrix.a,legHorizOffset);
			float legOffset = -0.5f*legHeight;
			partInitialMatrix.d.AddScaled(jointPosition,bodyMatrix.b,legOffset);
			partInitialMatrix.d.Add(bodyMatrix.d);
			jointPosition.Add(bodyMatrix.d);
			partInst->SetMatrix(*(const Mat34V*)(&partInitialMatrix));
			m_Parts[LEFT_LEG]->SetInitialMatrix(partInitialMatrix);
			PHSIM->AddActiveObject(partInst);
			PHCONSTRAINT->AttachObjects('AOIH', jointPosition, torsoInst, partInst);

			if(phDemoWorld::GetAddRotationLimitsToConstraintRagdolls())
			{
				const float maxLeanAngle = PH_DEG2RAD(40.0f);

				phConstraint* pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,1,0));
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(1,0,0));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();

				pConstraintRotation=PHCONSTRAINT->AttachObjectsRotation('AOIH', *torsoInst, *partInst);
				pConstraintRotation->SetDegreesConstrained(1,Vector3(0,0,1));
				pConstraintRotation->SetHardLimitMin(-maxLeanAngle);
				pConstraintRotation->SetHardLimitMax(maxLeanAngle);
				pConstraintRotation->SetFixedRotation();
			}

			humanBodyParts[LEFT_LEG]=m_CollisionTester->AddBodyPart(partInst);
			partInst->SetUserData(humanBodyParts[LEFT_LEG]);
		}

		// Make the torso collidable with the arms and legs.
		//m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[RIGHT_ARM]);
		//m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[LEFT_ARM]);
		//m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[RIGHT_LEG]);
		//m_CollisionTester->SetPartsCanCollide(humanBodyParts[TORSO],humanBodyParts[LEFT_LEG]);

		// Make the head collidable with the arms.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[HEAD],humanBodyParts[RIGHT_ARM]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[HEAD],humanBodyParts[LEFT_ARM]);

		// Make the arms collidable with the  legs.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[RIGHT_ARM],humanBodyParts[RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[RIGHT_ARM],humanBodyParts[LEFT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LEFT_ARM],humanBodyParts[RIGHT_LEG]);
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[LEFT_ARM],humanBodyParts[LEFT_LEG]);

		// Make the legs collidable with each other.
		m_CollisionTester->SetPartsCanCollide(humanBodyParts[RIGHT_LEG],humanBodyParts[LEFT_LEG]);
#endif
	}
}

void phArticulatedObject::InitFromBound (phDemoWorld& demoWorld, const char* name, phJoint::JointType* jointTypeList, const Vector3& position, const Vector3& rotation, bool allToRoot)
{
	InitGfx(name);
	Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
	Matrix34 colliderMatrix;
	colliderMatrix.Identity3x3();
	colliderMatrix.d.Set(position);
	InitPhys(name,colliderMatrix,true);
	phInst* instance = GetPhysInst();
	phArchetypePhys* archetype = static_cast<phArchetypePhys*>(instance->GetArchetype());
	Assert(archetype->GetBound() && archetype->GetBound()->GetType()==phBound::COMPOSITE);
	phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(archetype->GetBound());

	// Allocate the joints and body parts in the articulated body.
	int numParts = compositeBound.GetNumBounds();
	m_Body.AllocateJointsAndParts(numParts,jointTypeList);

	// Find the total volume, for distributing the mass.
	float totalVolume = compositeBound.GetVolume();

	// Set the total mass to the archetype's mass, if there is any, or to a density of 100kg/m^3
	float mass = archetype->GetMass();
	float totalMass = (mass>0.0f ? mass : totalVolume*100.0f);

	Matrix34 initialPartMtx;
	int partIndex;
	for (partIndex=0; partIndex<numParts; partIndex++)
	{
		initialPartMtx.Set(RCC_MATRIX34(compositeBound.GetCurrentMatrix(partIndex)));
		initialPartMtx.Transpose();
		m_Body.GetLink(partIndex).SetMatrix(initialPartMtx);
	}

	// Position the body parts before joining them together.
	for (partIndex=0; partIndex<numParts; partIndex++)
	{
		m_Body.ZeroLinkVelocities();
	}

	// Add the first part as the root of the body.
	m_Body.AddRoot(m_Body.GetLink(0));

	// Set the rotation axis and angle limits.
	Vector3 jointAxis;
	const float leanLimit1 = PH_DEG2RAD(90.0f);
	const float leanLimit2 = PH_DEG2RAD(90.0f);
	const float twistLimit = FLT_MAX;//PH_DEG2RAD();
	Vector3 jointPosition,bodyPartUp,boundingBox;
	int parentPartIndex=0,childPartIndex=1;
	int numJoints = numParts-1;
	for (int jointIndex=0; jointIndex<numJoints; jointIndex++)
	{
		// Set the child body part position as the joint position.
		const Matrix34& nextPartMatrix = RCC_MATRIX34(compositeBound.GetCurrentMatrix(jointIndex+1));
		jointPosition.Set(nextPartMatrix.d);

		// Get the child body part length.
		const phBound& childPartBound = *compositeBound.GetBound(jointIndex+1);
		float childLength;
		if (childPartBound.GetType()==phBound::CAPSULE)
		{
			childLength = static_cast<const phBoundCapsule*>(&childPartBound)->GetLength();
		}
		else
		{
			boundingBox = VEC3V_TO_VECTOR3(childPartBound.GetBoundingBoxSize());
			childLength = boundingBox.y;
		}
		
		// Move the joint position downward along the child body part's local vertical axis.
		bodyMatrix.Transform3x3(nextPartMatrix.b,bodyPartUp);
		jointPosition.SubtractScaled(bodyPartUp,0.5f*childLength);

		// Initialize the joint.
		jointAxis.Set(jointTypeList[jointIndex]==phJoint::JNT_1DOF ? bodyMatrix.c : bodyMatrix.b);
		m_Body.AddChild(parentPartIndex,m_Body.GetJoint(jointIndex),m_Body.GetLink(childPartIndex));
		m_Body.GetJoint(jointIndex).SetAxis(&m_Body, jointPosition,jointAxis);
		if (jointTypeList[jointIndex]==phJoint::JNT_1DOF)
		{
			static_cast<phJoint1Dof*>(&m_Body.GetJoint(jointIndex))->SetAngleLimits(-leanLimit1,leanLimit1);
		}
		if (jointTypeList[jointIndex]==phJoint::JNT_3DOF)
		{
			static_cast<phJoint3Dof*>(&m_Body.GetJoint(jointIndex))->SetThreeAngleLimits(leanLimit1,leanLimit2,twistLimit);
		}
		else if (jointTypeList[jointIndex]==phJoint::PRISM_JNT)
		{
			static_cast<phPrismaticJoint*>(&m_Body.GetJoint(jointIndex))->SetAngleLimits(-0.5f,0.5f);
		}

		childPartIndex++;
		if (!allToRoot)
		{
			parentPartIndex++;
		}
	}

	for (partIndex=0; partIndex<numParts; partIndex++)
	{
		Assert(compositeBound.GetBound(partIndex));
		phBound& boundPart = *compositeBound.GetBound(partIndex);
		float mass = totalMass*boundPart.GetVolume()/totalVolume;
		m_Body.SetMassAndAngInertia(partIndex,mass,VEC3V_TO_VECTOR3(boundPart.GetComputeAngularInertia(mass)));
	}

	m_Body.SetStiffness(0.0f);
	InitBody(archetype,instance,demoWorld);
}


void phArticulatedObject::InitChain (phDemoWorld& demoWorld, bool threeDofJoints, const char* name, const Vector3& position, const Vector3& rotation)
{
	InitGfx(name);
	Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
	const bool uniqueBound = true;
	InitPhys(name,bodyMatrix,uniqueBound);
	phInst* instance = GetPhysInst();
	phArchetypePhys* archetype = static_cast<phArchetypePhys*>(instance->GetArchetype());
	Assert(archetype->GetBound() && archetype->GetBound()->GetType()==phBound::COMPOSITE);
	phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(archetype->GetBound());
	m_Body.InitChain(compositeBound,threeDofJoints,bodyMatrix);
	InitBody(archetype,instance,demoWorld);

	int numParts = compositeBound.GetNumBounds();
	for (int i=0; i<numParts - 1; i++)
	{
		for (int j=i+2; j < numParts; j++)
		{
			m_Collider->SetPartsCanCollide(i, j, true, false);
		}
	}

	m_Collider->FinalizeSettingPartsCanCollide(m_Collider->GetSelfCollisionPairsArrayRefA(), m_Collider->GetSelfCollisionPairsArrayRefB());
}


void phArticulatedObject::InitDresser (phDemoWorld& demoWorld, const char* name, const Vector3& position, const Vector3& rotation, bool withDoors)
{
	InitGfx(name);
	Matrix34 bodyMatrix(CreateRotatedMatrix(position,rotation));
	const bool uniqueBound = true;
	InitPhys(name,bodyMatrix,uniqueBound);
	phInst* instance = GetPhysInst();
	phArchetypePhys* archetype = static_cast<phArchetypePhys*>(instance->GetArchetype());
	Assert(archetype->GetBound() && archetype->GetBound()->GetType()==phBound::COMPOSITE);
	phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(archetype->GetBound());

	int numParts = compositeBound.GetNumBounds();
	int numJoints = numParts-1;
	phJoint::JointType jointTypeList[phArticulatedBodyType::MAX_NUM_JOINTS];
	int numDrawers = (withDoors ? numJoints-2 : numJoints);
	for (int drawerIndex=0; drawerIndex<numDrawers; drawerIndex++)
	{
		jointTypeList[drawerIndex] = phJoint::PRISM_JNT;
	}
	if (withDoors)
	{
		jointTypeList[numJoints-2] = phJoint::JNT_1DOF;
		jointTypeList[numJoints-1] = phJoint::JNT_1DOF;
	}
	m_Body.AllocateJointsAndParts(numParts,jointTypeList);

	Matrix34 initialPartMtx;
	initialPartMtx.Set3x3(bodyMatrix);
	initialPartMtx.Transpose();
	initialPartMtx.d.Zero();
	Vector3 frameBoundMax(VEC3V_TO_VECTOR3(compositeBound.GetBound(0)->GetBoundingBoxMax()));
	const float drawerSpacing = 0.1f;
	for (int partIndex=0; partIndex<numParts; partIndex++)
	{
		if (partIndex==1)
		{
			// Put the first drawer near the top.
			float verticalOffset = frameBoundMax.y - compositeBound.GetBound(1)->GetBoundingBoxMax().GetYf() - drawerSpacing;
			initialPartMtx.d.y += verticalOffset;
		}
		else if (partIndex>1 && partIndex<numDrawers+1)
		{
			float verticalOffset = compositeBound.GetBound(partIndex-1)->GetBoundingBoxMin().GetYf() - compositeBound.GetBound(partIndex)->GetBoundingBoxMax().GetYf() - drawerSpacing;
			initialPartMtx.d.y += verticalOffset;
		}
		else if (partIndex>0)
		{
			initialPartMtx.d.x = (partIndex==numParts-2 ? 0.5f*frameBoundMax.x : -0.5f*frameBoundMax.x);
			initialPartMtx.d.y = 0.0f;
			initialPartMtx.d.z = frameBoundMax.z;
		}

		m_Body.GetLink(partIndex).SetMatrix(initialPartMtx);
	}

	// Position the body parts before joining them together.
	for (int i=0; i<numParts; i++)
	{
		m_Body.ZeroLinkVelocities();
	}

	// Add the first part as the root of the body.
	m_Body.AddRoot(m_Body.GetLink(0));

	Vector3 jointPosition;
	Vector3 jointAxis(ZAXIS);
	float limit1 = 0.0f;
	float limit2;
	int parentPartIndex=0,childPartIndex=1;
	for (int jointIndex=0; jointIndex<numDrawers; jointIndex++)
	{
		jointPosition.Set(m_Body.GetLink(childPartIndex).GetPosition());
		m_Body.AddChild(parentPartIndex,m_Body.GetJoint(jointIndex),m_Body.GetLink(childPartIndex));
		m_Body.GetJoint(jointIndex).SetAxis(&m_Body, jointPosition,jointAxis);
		limit2 = compositeBound.GetBound(childPartIndex)->GetBoundingBoxMax().GetXf()-compositeBound.GetBound(childPartIndex)->GetBoundingBoxMin().GetXf();
		m_Body.GetJoint(jointIndex).SetAngleLimits(limit1,limit2);
		childPartIndex++;
	}

	if (withDoors)
	{
		jointAxis.Set(YAXIS);
		jointPosition.Set(frameBoundMax.x,0.0f,frameBoundMax.z);
		m_Body.AddChild(parentPartIndex,m_Body.GetJoint(numJoints-2),m_Body.GetLink(childPartIndex));
		m_Body.GetJoint(numJoints-2).SetAxis(&m_Body, jointPosition,jointAxis);
		limit1 = 0.0f;
		limit2 = 0.9f*PI;
		m_Body.GetJoint(numJoints-2).SetAngleLimits(limit1,limit2);
		jointPosition.x = -frameBoundMax.x;
		childPartIndex++;
		m_Body.AddChild(parentPartIndex,m_Body.GetJoint(numJoints-1),m_Body.GetLink(childPartIndex));
		m_Body.GetJoint(numJoints-1).SetAxis(&m_Body, jointPosition,jointAxis);
		limit1 = -0.9f*PI;
		limit2 = 0.0f;
		m_Body.GetJoint(numJoints-1).SetAngleLimits(limit1,limit2);
	}

	if (withDoors)
	{
		m_Body.SetStiffness(0.0f);
	}
	else
	{
		float stiffness = 0.0f;
		for (int jointIndex=0; jointIndex<numJoints; jointIndex++)
		{
			m_Body.GetJoint(jointIndex).SetStiffness(stiffness);
			stiffness += 0.3f;
		}
	}

	const float firstPartMass = 20.0f;
	const float partMass = 5.0f;
	for (int partIndex=0; partIndex<numParts; partIndex++)
	{
		float mass = (partIndex>0 ? partMass : firstPartMass);
		Assert(compositeBound.GetBound(partIndex));
		phBound& boundPart = *compositeBound.GetBound(partIndex);
		Vector3 angInertia(VEC3V_TO_VECTOR3(boundPart.GetComputeAngularInertia(mass)));
		m_Body.SetMassAndAngInertia(partIndex,mass,angInertia);
	}

	InitBody(archetype,instance,demoWorld);

	if (withDoors)
	{
		int numParts = compositeBound.GetNumBounds();
		int door1 = numParts-2;
		int door2 = numParts-1;
		int numDrawers = numParts-3;
		for (int drawerIndex=0; drawerIndex<numDrawers; drawerIndex++)
		{
			m_Collider->SetPartsCanCollide(drawerIndex+1,door1);
			m_Collider->SetPartsCanCollide(drawerIndex+1,door2);
		}
	}
}


phArticulatedObject* phArticulatedObject::CreateHuman (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation, const Vector3& cgOffset, int numBodyParts)
{
	phArticulatedObject* object = rage_new phArticulatedObject;
	if(phDemoWorld::GetVerySimpleRagdolls())
	{
		object->InitHuman6Bone(demoWorld,position,rotation,cgOffset,numBodyParts);
	}
	else
	{
		object->InitHuman(demoWorld,position,rotation,cgOffset,numBodyParts);
	}
	return object;
}


phArticulatedObject* phArticulatedObject::CreateFromBound (phDemoWorld& demoWorld, const char* name, phJoint::JointType* jointTypeList, const Vector3& position, const Vector3& rotation, bool allToRoot)
{
	phArticulatedObject* object = rage_new phArticulatedObject;
	object->InitFromBound(demoWorld,name,jointTypeList,position,rotation,allToRoot);
	return object;
}


phArticulatedObject* phArticulatedObject::CreateChain (phDemoWorld& demoWorld, bool threeDofJoints, const char* name, const Vector3& position, const Vector3& rotation)
{
	phArticulatedObject* object = rage_new phArticulatedObject;
	object->InitChain(demoWorld,threeDofJoints,name,position,rotation);
	return object;
}


phArticulatedObject* phArticulatedObject::CreateDresser (phDemoWorld& demoWorld, const char* name, const Vector3& position, const Vector3& rotation, bool withDoors)
{
	phArticulatedObject* object = rage_new phArticulatedObject;
	object->InitDresser(demoWorld,name,position,rotation,withDoors);
	return object;
}


} // namespace ragesamples

