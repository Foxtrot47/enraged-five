//
// sample_physics/sample_water.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "system/main.h"

#if 0
#include "demoobject.h"
#include "sample_physics.h"

#include "input/keys.h"
#include "math/random.h"
#include "pheffects/instbehaviorliquid.h"
#include "pheffects/liquidmgr.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

///////////////////////////////////////////////////////////////
// TITLE: sample_water
// PURPOSE:	Demonstrate the behavior of liquids with phInstBehaviorLiquid and phBoundSurface.
//			Of particular note to look at is the behavior of the surface of the water (ripples, etc) and the behavior of objects
//			due to the buoyant forces exerted by the water. The usual sample controls apply.
class waterSampleManager : public physicsSampleManager
{
public:
	waterSampleManager() 
	{
		
	}

	virtual void InitClient()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();


		// STEP #2. Activate extra key controls (space drops a single new object, A activates objects).
		m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE, m_TriggerObjectNew);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_A, m_TriggerObjectRepeat);


		// STEP #3. Create a demo world with a flat floor, and set its frame rate to 30 (default is 60).
		m_Demos.AddDemo(rage_new phDemoWorld("Water World"));
		m_Demos.GetCurrentWorld()->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 25);
		m_Demos.GetCurrentWorld()->SetFramerate(30.0f);
		m_Demos.GetCurrentWorld()->InitUpdateObjects(256);
		const int STARTING_DEMO = 0;
		m_Demos.SetCurrentDemo(STARTING_DEMO);
		m_Demos.GetCurrentWorld()->Activate();


		// STEP #4. Initialize the liquid manager and tell it that we're going to be using up to 25 water bounds.
		u16 maxSurfaceGrids = 25;
		u16 maxLiquidInsts = 25;
		float beginLowLODDistance = 50.0f;
		float beginHighLODDistance = 50.0f;
		m_LiquidMgr.Init(maxSurfaceGrids,maxLiquidInsts,beginLowLODDistance,beginHighLODDistance);

		phInstBehaviorLiquidTuning waterTuning;
		waterTuning.m_fDampening = 0.99f;
		waterTuning.m_fPropogationSpeed = 18.0f;
		waterTuning.m_fInstBuoyancy = 1.0f;
		waterTuning.m_fPushDrag = -0.354f;
		waterTuning.m_fHorizPushDragScale = 0.2f;
		waterTuning.m_fSlideDrag = -0.131f;
		waterTuning.m_fHorizSlideDragScale = 1.5f; 
		waterTuning.m_fSurfaceNoiseRange = 0.0f;
		waterTuning.m_iSurfaceNoiseCount = 0;
		waterTuning.m_bEnableSurfaceReaction = false;
		waterTuning.m_fMaxWaveHeight = 0.7f;
		waterTuning.m_fSplashScale = 0.036f;
		waterTuning.m_fMaxSplash = 0.10f;
		waterTuning.m_fMaxLocalVelocity = 20.0f;
		waterTuning.m_fConstantPushDrag = 0.0f;
		waterTuning.m_fConstantSlideDrag = 0.0f;
		waterTuning.m_WaterDirection.Set(0.0f, 0.0f, 0.0f);
		waterTuning.m_fWaterVelocityScale = 0.0f;
		waterTuning.m_fPushDragRisingFactor = 0.977f;
		waterTuning.m_fPushDragSinkingFactor = 1.0f;

		// STEP #5. Create a 2x2 grid of water bounds.
		const int gridSize = 2;
		const float cellSize = 64.0f;
		Matrix34 Matrix(M34_IDENTITY);
		float minElevation = -30.0f;
		float maxElevation = 0.0f;
		float spacing = 0.0f;
		for (int Z = 0; Z < gridSize; ++Z)
		{
			for (int X = 0; X < gridSize; ++X)
			{
				int waterInstanceIndex = gridSize * Z + X;
			//	m_LiquidBounds[waterInstanceIndex].SetDepth(10.0f);
				m_LiquidBounds[waterInstanceIndex].SetRadiusAroundCentroid(ScalarVFromF32(25.0f));

				Matrix.d.x = cellSize*((float)X - 0.5f*(float)(gridSize-1));
				Matrix.d.z = cellSize*((float)Z - 0.5f*(float)(gridSize-1));
				float minX = Matrix.d.x - 0.5f*cellSize;
				float maxX = Matrix.d.x + 0.5f*cellSize;
				float minZ = Matrix.d.z - 0.5f*cellSize;
				float maxZ = Matrix.d.z + 0.5f*cellSize;
				m_LiquidBounds[waterInstanceIndex].SetRawData(NULL,NULL,spacing,(short)X,(short)Z,minElevation,maxElevation,minX,maxX,minZ,maxZ);
				m_LiquidBounds[waterInstanceIndex].CalculateBoundingVolumes();
				m_LiquidArchetypes[waterInstanceIndex].SetBound(&m_LiquidBounds[waterInstanceIndex]);
				m_LiquidArchetypes[waterInstanceIndex].AddRef();
				m_LiquidInsts[waterInstanceIndex].SetArchetype(&m_LiquidArchetypes[waterInstanceIndex]);

				Matrix.d.x = 0.0f;
				Matrix.d.z = 0.0f;
				m_LiquidInsts[waterInstanceIndex].SetMatrix(RCC_MAT34V(Matrix));
				m_LiquidBehaviors[waterInstanceIndex].SetInstance(m_LiquidInsts[waterInstanceIndex]);
				m_LiquidBehaviors[waterInstanceIndex].SetTuningData(waterTuning);
				PHSIM->AddFixedObject(&m_LiquidInsts[waterInstanceIndex]);
				PHSIM->AddInstBehavior(m_LiquidBehaviors[waterInstanceIndex]);
				m_LiquidMgr.AddLiquidInst(&m_LiquidInsts[waterInstanceIndex]);
			}
		}

		// Drop some objects into the water.
		phDemoWorld& demoWorld = *m_Demos.GetCurrentWorld();
		for (int Count = 0; Count < 10; ++Count)
		{
			Matrix.d.x = 16.0f * g_ReplayRand.GetFloat() - 8.0f;
			Matrix.d.y = 8.0f * g_ReplayRand.GetFloat() + 2.0f;
			Matrix.d.z = 16.0f * g_ReplayRand.GetFloat() - 8.0f;
			Vector3 Rotation;
			Rotation.x = 2.0f * g_ReplayRand.GetFloat() - 1.0f;
			Rotation.y = 2.0f * g_ReplayRand.GetFloat() - 1.0f;
			Rotation.z = 2.0f * g_ReplayRand.GetFloat() - 1.0f;
			Rotation.Normalize();
			float Angle = g_ReplayRand.GetFloat() * 2.0f * PI;
			Matrix.Rotate(Rotation, Angle);
			switch(Count % 5)
			{
				case 0:
				{
					m_DemoObject = demoWorld.CreateObject("sphere_056", Matrix);
					break;
				}
				case 1:
				{
					m_DemoObject = demoWorld.CreateObject("capsule_fat", Matrix);
					break;
				}
				case 2:
				{
					m_DemoObject = demoWorld.CreateObject("crate", Matrix);
					break;
				}
				case 3:
				{
					m_DemoObject = demoWorld.CreateObject("escalade", Matrix);
					break;
				}
				case 4:
				{
					m_DemoObject = demoWorld.CreateObject("crate", Matrix);
					m_DemoObject = phArticulatedObject::CreateHuman(demoWorld, Matrix.d);
					break;
				}
			}
		}

//		m_Demos.GetCurrentWorld()->CreateObject("twobox", Vector3(0.0f, 14.0f, 0.0f));
//		m_Demos.GetCurrentWorld()->CreateObject("longbox", Vector3(0.0f, 14.0f, 0.0f));
//		m_Demos.GetCurrentWorld()->CreateObject("crate", Vector3(0.0f, 14.0f, 0.0f));
//		m_Demos.GetCurrentWorld()->CreateObject("escalade", Vector3(0.0f, 14.0f, 0.0f));
//		m_Demos.GetCurrentWorld()->CreateObject("barbell", Vector3(0.0f, 14.0f, 0.0f));
//		m_Demos.GetCurrentWorld()->CreateObject("hotdog", Vector3(0.0f, 44.0f, 0.0f));
	}

	void InitCamera()
	{
		// STEP #7. Set the camera position and target for a good view of the water.
		Vector3 lookFrom(0.0f, 20.0f, 22.0f);
		Vector3 lookTo(0.0f, 2.0f, 0.0f);
		grcSampleManager::InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update()
	{
		// STEP #8. Update the input controls.
		if (m_TriggerObjectNew.IsPressed())
		{
			Matrix34 Matrix(M34_IDENTITY);
			Matrix.d.x = 0.0f;
			Matrix.d.y = 15.0f * g_ReplayRand.GetFloat() + 15.0f;
			Matrix.d.z = 0.0f;
			Vector3 Rotation;
			Rotation.x = 2.0f * g_ReplayRand.GetFloat() - 1.0f;
			Rotation.y = 2.0f * g_ReplayRand.GetFloat() - 1.0f;
			Rotation.z = 2.0f * g_ReplayRand.GetFloat() - 1.0f;
			Rotation.Normalize();
			float Angle = g_ReplayRand.GetFloat() * 2.0f * PI;
			Matrix.Rotate(Rotation, Angle);

			m_DemoObject->GetPhysInst()->SetMatrix(RCC_MAT34V(Matrix));
			u16 LevelIndex = m_DemoObject->GetPhysInst()->GetLevelIndex();
			if(PHLEVEL->IsActive(LevelIndex))
			{
				phCollider *Collider = PHSIM->GetCollider(LevelIndex);
				Assert(Collider != NULL);
//				Collider->Freeze();
				//Collider->SetColliderMatrixFromInstance();
				Collider->SetMatrix(Matrix);
			}
			else
			{
				PHSIM->ActivateObject(LevelIndex);
			}
		}

		// STEP #9. Update the water manager. The water class is derived from phInstBehavior, and it is updated when the physics
		// simulator updates all of the instance behaviors.
		m_LiquidMgr.SetCameraMatrix(GetCameraMatrix());
		m_LiquidMgr.Update();

		physicsSampleManager::Update();
	}

	virtual void Shutdown ()
	{
		const bool deleteAtZero = false;
		for (int instanceIndex=0; instanceIndex<25; instanceIndex++)
		{
			if (m_LiquidInsts[instanceIndex].IsInLevel())
			{
				PHSIM->RemoveInstanceAndBehavior(m_LiquidBehaviors[instanceIndex]);
				m_LiquidInsts[instanceIndex].SetArchetype(NULL);
				m_LiquidArchetypes[instanceIndex].Release(deleteAtZero);
			}

			m_LiquidBounds[instanceIndex].Release(deleteAtZero);
		}

		physicsSampleManager::Shutdown();
	}

private:
	phBoundSurface			m_LiquidBounds[25];
	phArchetype				m_LiquidArchetypes[25];
	phInst					m_LiquidInsts[25];
	phInstBehaviorLiquid	m_LiquidBehaviors[25];
	phLiquidMgr				m_LiquidMgr;
	phInstBehaviorLiquidTuning m_LiquidTuning;
	phDemoObject			*m_DemoObject;

	ioValue					m_TriggerObjectRepeat;
	ioValue					m_TriggerObjectNew;
};

} // namespace ragesamples
#endif

// main application
int Main()
{
#if 0
	{
		ragesamples::waterSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}
#endif

	return 0;
}
