set LIBS=%RAGE_CORE_LIBS% %RAGE_PH_LIBS% %RAGE_SAMPLE_GRCORE_LIBS% sample_rmcore
set LIBS=%LIBS% %RAGE_GFX_LIBS% sample_physics
REM LIBS_testername doesn't work on linux build system
set TESTERS=sample_asyncshapetestmgr sample_basic sample_bench sample_callbacks sample_contacts sample_conveyor sample_explosion sample_articulated
set TESTERS=%TESTERS% sample_water sample_rsc sample_constraint sample_articonstraint sample_maze sample_shape_tests
set TESTERS=%TESTERS% sample_character sample_multithreading sample_spuobjecttest sample_perftest
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples 
set COMBINED_VCPROJS=0
