//
// sample_physics/sample_multithreading.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Multi-Threading Physics
// PURPOSE:
//		This sample tests physics running on multiple threads simultaneously. For now it just simulates multithreading by running multiple physics tests broken up into
//		pieces on a single thread. Later it will demonstrate multi-threading physics on Xenon and PS3.


#include "bank/bkmgr.h"
#include "diag/output.h"
#include "file/asset.h"
#include "grmodel/setup.h"
#include "input/mouse.h"
#include "math/random.h"
#include "phbound/bound.h"
#include "phcore/materialmgr.h"
#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "phsolver/contactmgr.h"
#include "physics/collider.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/rageroot.h"
#include "system/task.h"
#include "system/timemgr.h"
#include "vectormath/vec3v.h"

#include "objecttestspu.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");
PARAM(numobjects, "the number of active objects to use");

RELOAD_TASK_DECLARE(objecttestspu);

namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);
EXT_PFD_DECLARE_ITEM(Active);
EXT_PFD_DECLARE_ITEM(Inactive);
EXT_PFD_DECLARE_ITEM(Fixed);

}

namespace ragesamples {

using namespace rage;



#define	MULTITHREAD_MAX_ACTIVE_OBJECTS	256
#define MULTITHREAD_DEFAULT_NUM_OBJECTS	0

////////////////////////////////////////////////////////////////
// 

class spuObjectTestSampleManager : public grcSampleManager
{
public:
	void InitClient()
	{
		// Set the sample to run at 30 frames per second of simulation time. The apparent speed will be faster or slower depending on
		// the time needed to compute one frame. TIME.SetRealTimeMode(30.0f) would fix the apparent speed at 30 fps and vary the
		// time spent computing each frame.
		TIME.SetFixedFrameMode(30.0f);

		phConfig::EnableRefCounting();

		m_AddAndDeleteObjects = false;

#if __PFDRAW
		// STEP #1. Initialize profile drawing.
		//-- Profile drawing is used to draw physics bounds. It is useful in games to diagnose any problems
		// involving interactions with physical objects. __PFDRAW is defined in rage/base/src/profile/drawcore.h,
		// and is normally on in debug and beta builds.

		// Set the number of bytes available for storing profile draw shapes (objects for drawing are collected during the
		// physics updates and drawn together), and initialize the buffer and widgets for profile drawing.
		const int pfDrawBufferSize = 2000000;
		GetRageProfileDraw().Init(pfDrawBufferSize);

		// Enable profile drawing.
		const bool pfDrawEnabled = true;
		GetRageProfileDraw().SetEnabled(pfDrawEnabled);

		// Turn on some drawing groups. Profile drawing is turned off by default, and in release builds it is compiled out.
		// It should only be turned on when needed to solve problems. For this example, it is necessary because some of the
		// objects do not have graphics models.
		PFD_GROUP_ENABLE(Physics, true);
		PFD_GROUP_ENABLE(Bounds, true);
		PFD_ITEM_ENABLE(Solid, true);
		PFD_ITEM_ENABLE(Wireframe, false);
#endif


		// STEP #2. Create and initialize the physics level.
		//-- The physics level (phLevel) keeps track of all the physical objects in the game. It uses a spatial partitioning scheme
		// to speed up queries about object location. It can be used to quickly find objects that are hit by a line segment, or
		// touch a sphere or capsule.
		phLevelNew::SetActiveInstance(rage_new phLevelNew);

		// Set the size of the physics level.
		// When any object goes outside the physics level, it is no longer updated, and virtual phInst::NotifyOutOfWorld() is called.
		// Any object that is added to the physics level outside the extents will cause an assert failure.
		const Vec3V worldExtentsMin(-999.0f,-999.0f,-999.0f);
		const Vec3V worldExtentsMax(999.0f,999.0f,999.0f);
		PHLEVEL->SetExtents(worldExtentsMin,worldExtentsMax);

		// Set the maximum allowed number of physical objects in the physics level. This normally varies from hundreds for a small
		// sample level to hundreds of thousands for a large game level.
		// Exceeding the maximum number of objects causes an assert failure.
		int maxNumObjects = 500;
		PHLEVEL->SetMaxObjects(maxNumObjects);

		// Set the maximum allowed number of physically active objects in the physics level. Physically active means the objects have
		// colliders and are reacting to collisions. This number is normally in the low hundreds.
		// Manually exceeding the maximum number of active objects (by calling AddActiveObject) causes an assert failure. If the maximum number
		// of active objects would be exceeded by the physics simulator activating objects, such as from a collision, then the object will not
		// become active.
		const int maxNumActiveObjects = 100;
		PHLEVEL->SetMaxActive(maxNumActiveObjects);

		// Set the maximum number of occupied nodes in the physics level's octree.
		// An assert failure results if the physics level tries to make more than the maximum number of octree nodes. This will be changed
		// soon to print a warning and not create the needed node instead (17 March 06).
		const int maxOctreeNodes = 1000;
		PHLEVEL->SetNumOctreeNodes(maxOctreeNodes);

		// Create and initialize the physics level's octree and object information, using the parameters set above.
		PHLEVEL->Init();


		// STEP #3. Create and initialize the simulator.
		//-- The physics simulator computes the locations, orientations, and speeds of objects in the physics level,
		// and handles collision detection and responses.

		// Call the static initialization of the simulator.
		phSimulator::InitClass();

		// Create the simulator and set it as the current active simulator, accessible by calling PHSIM->.
		phSimulator::SetActiveInstance(rage_new phSimulator);

		// Initialize the simulator with the currently active physics level, and the maximum number of managed active objects. This can be different
		// from the physics level's maximum number of active objects because the simulator's number only includes active objects that are managed
		// by the simulator. Users have the freedom to add physically active objects to the physics level with colliders that are not managed by
		// the simulator. This is normally used for derived colliders, such as vehicles and ragdolls.
		phSimulator::InitParams params;
		params.maxManagedColliders = maxNumActiveObjects;
		PHSIM->Init(PHLEVEL,params);


		// STEP #4. Initialize the material manager.
		//-- Physics bounds contain physics materials to define their properties for collisions (friction and elasticity) and other effects (such as sounds
		// and driving properties). Materials are shared among bounds and stored in the material manager.
		ASSET.PushFolder("materials");
		phMaterialMgrImpl<phMaterial>::Create();
		MATERIALMGR.Load();
		phMaterialMgrImpl<phSurface, phMaterialMgrFlag>::Create();
		MATERIALMGRFLAG.Load(64);
		ASSET.PopFolder();


		// STEP #5. Create the terrain and put it in the physics level.
		//-- Load a file called "bound.bnd" from the "assets/physics/big_plane" folder. It has information about the physical shape of an instance
		// that will be used as the terrain. It is a large horizontal square made of two triangles.
		//phBound* terrainBound = phBound::Load("physics/splashbvh/bound");
		phBound* terrainBound = phBound::Load("physics/plane_bvh/bound");
		//phBound* terrainBound = phBound::Load("physics/plane_quad_bvh/bound");
		Assert(terrainBound);

		// Set the terrain bound in the terrain archetype. Physics archetypes can hold physical information such as include and type flags, mass
		// and damping, and a pointer to the bound. There is usually a 1-1 correspondence between archetypes and bounds, but it is possible for
		// more than one archetype to share a bound.
		m_TerrainArchetype.SetBound(terrainBound);

		// Set the archetype in the physics instance. The physics level keeps track of objects by physics instances.
		m_TerrainInstance.SetArchetype(&m_TerrainArchetype);

		// Set the instance's matrix. This contains a 3x3 part for orientation and a position.
		Matrix34 instanceMatrix;
		instanceMatrix.Identity();
		m_TerrainInstance.SetMatrix(RCC_MAT34V(instanceMatrix));

		// Insert the terrain instance in the level, so that it will participate in the physics simulation. Since this instance is terrain,
		// AddFixedObject us used, which means that this object is fixed in space.
		PHSIM->AddFixedObject(&m_TerrainInstance);


		// STEP #6. Load two objects and put them in the physics level.
		//-- Load the physical bound, and set it in the archetype, following the same procedure as with the terrain.
		phBound* icosahedronBound = phBound::Load("physics/icosahedron/bound");
//		phBound* icosahedronBound = phBound::Load("physics/dresser/bound");
		m_ActiveArchetype.SetBound(icosahedronBound);

		m_NumObjects = MULTITHREAD_DEFAULT_NUM_OBJECTS;
		if (PARAM_numobjects.Get(m_NumObjects))
		{
			m_NumObjects = Min(m_NumObjects,MULTITHREAD_MAX_ACTIVE_OBJECTS);
		}

		// Initialize both instances of this moving object with the same archetype and bound.
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			m_ActiveInstanceList[objectIndex].SetArchetype(&m_ActiveArchetype);
		}

		// Set the matrices of the two instances.
		instanceMatrix.MakeTranslate(0.0f,3.0,0.0f);
		instanceMatrix.RotateZ(0.1f);
		Vector3 offset(0.05f,1.0,0.05f);
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			m_ActiveInstanceList[objectIndex].SetMatrix(RCC_MAT34V(instanceMatrix));
			m_ResetMatrixList[objectIndex].Set(instanceMatrix);
			instanceMatrix.d.Add(offset);
		}

		// Place the two objects in the physics level. AddActiveObject is used this time, to tell the simulator to give the instances
		// colliders so that they will be simulated as physically active objects.
		// An optional second parameter in AddActiveObject can be used to make the object always active, which means it will never be
		// put in the inactive state.
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			PHSIM->AddActiveObject(&m_ActiveInstanceList[objectIndex]);
		}


		// Map a function keys for resetting the physics world and for grabbing objects with the mouse.
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Reset);
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_LEFT,m_Push);
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_RIGHT,m_Pull);
#if __PPU
		// Clear the task parameters.
		sysMemZeroBytes<sizeof(sysTaskParameters)>(&m_TaskParameters);

		// These are not used by this job, but sysTaskManager::Create() will complain if you try and create a task with them set to NULL.
		m_TaskParameters.Input.Data = (void*)(this);
		m_TaskParameters.Input.Size = 0;
		m_TaskParameters.Output.Data = (void*)(this);
		m_TaskParameters.Output.Size = 0;
		m_TaskParameters.SpuStackSize = 40 * 1024;

		// Set up the user data parameters.
		m_TaskParameters.UserData[0].asPtr = PHLEVEL;
#if !ENABLE_PHYSICS_LOCK
		m_TaskParameters.UserDataCount = 1;
#else	// ENABLE_PHYSICS_LOCK
		m_TaskParameters.UserData[1].asPtr = g_GlobalPhysicsLock.GetGlobalReaderCountPtr();
		m_TaskParameters.UserData[2].asPtr = g_GlobalPhysicsLock.GetPhysicsMutexPtr();
#	if LOCKCONFIG_WRITERS_ONLY_WAIT_FOR_CURRENT_READERS
		m_TaskParameters.UserData[3].asPtr = g_GlobalPhysicsLock.GetAllowNewReaderMutex();
#	else	// LOCKCONFIG_WRITERS_ONLY_WAIT_FOR_CURRENT_READERS
		m_TaskParameters.UserData[3].asPtr = NULL;
#	endif	// LOCKCONFIG_WRITERS_ONLY_WAIT_FOR_CURRENT_READERS
		m_TaskParameters.UserData[4].asPtr = g_GlobalPhysicsLock.GetModifyReaderCountMutexPtr();
		m_TaskParameters.UserDataCount = 5;
#endif	// ENABLE_PHYSICS_LOCK

		m_TaskHandle = sysTaskManager::Create(TASK_INTERFACE_RELOADABLE(objecttestspu), m_TaskParameters, 0);
#endif	// __PPU
	}

	void AddWidgetsClient ()
	{
	#if __BANK
		bkBank& bank = BANKMGR.CreateBank("rage - Physics");
		bank.AddToggle("AddAndDeleteObjects",&m_AddAndDeleteObjects);
		bank.PushGroup("Level",false);
		phLevelNew::AddWidgets(bank);
		bank.PopGroup();
		bank.PushGroup("Simulator",false);
		phSimulator::AddWidgets(bank);
		bank.PopGroup();
		bank.PushGroup("Materials",false);
		MATERIALMGR.AddWidgets(bank);
		bank.PopGroup();
	#endif
	}

	void UpdateMouse()
	{
		if (m_Push.IsPressed() || m_Pull.IsPressed())
		{
			// mouseScreen and mouseFar as where are the world space points on the near plane and far plane respectively which project to the current mouse cursor location.
			Vector3 mouseScreen, mouseFar;
			m_Viewport->ReverseTransformNoWorld(static_cast<float>(ioMouse::GetX()),static_cast<float>(ioMouse::GetY()),RC_VEC3V(mouseScreen),RC_VEC3V(mouseFar));
			/*
			grcWorldIdentity();
			grcBegin(drawLines,2);
			grcVertex3fv(&mouseScreen[0]);
			grcVertex3fv(&mouseFar[0]);
			grcEnd();
			*/

			// Establish a world space direction for the mouse click.
			Vector3 direction;
			direction.Subtract(mouseFar, mouseScreen);
			direction.Normalize();

			Vector3 segA, segB;
			segA = mouseScreen;
			segB.AddScaled(mouseScreen, direction, 100.0f);
			phSegment segment;
			segment.Set(segA, segB);
			phIntersection isect;
			if (PHLEVEL->TestProbe(segment, &isect))
			{
				// Do a probe test to apply an impulse.
				phIntersection isect;
				if (PHLEVEL->TestProbe(segment,&isect))
				{
					// A direct line of sight probe at the mouse icon location hit something.
					Assert(isect.GetInstance() && isect.GetInstance()->GetLevelIndex()!=phInst::INVALID_INDEX);

					// Find the impulse magnitude.
					float impulseMag = 40.0f;
					impulseMag = isect.GetInstance()->GetArchetype()->GetMass();
					if (m_Pull.IsPressed())
					{
						impulseMag *= -1.0f;
					}

					// Set the impulse.
					Vector3 impulse(direction);
					impulse.Scale(impulseMag);

					// Apply the impulse.
					PHSIM->ApplyImpulse(isect.GetInstance()->GetLevelIndex(), impulse, RCC_VECTOR3(isect.GetPosition()), isect.GetComponent());
				}
			}
		}
	}

	void UpdateClient()
	{
		// Update the physics simulator.
		float frameTime = TIME.GetSeconds();
		bool finalUpdate = true;
		PHSIM->Update(frameTime,finalUpdate);

		// Update user input.
		UpdateMouse();
		m_Mapper.Update();
		if (m_Reset.IsPressed())
		{
			Reset();
		}

		if (m_AddAndDeleteObjects)
		{
			int objectIndex = g_ReplayRand.GetRanged(0,m_NumObjects-1);
			phInst& instance = m_ActiveInstanceList[objectIndex];
			if (instance.IsInLevel())
			{
				PHSIM->DeleteObject(instance.GetLevelIndex());
			}
			else
			{
				PHSIM->AddActiveObject(&instance);
			}
		}
	}

	void DrawClient()
	{

#if __PFDRAW

		// STEP #9. Draw the world
		//--  Tell the profile draw manager to draw all the objects in the physics level.
		// In a game this is normally used only for debugging.
		PHSIM->ProfileDraw();
		GetRageProfileDraw().Render();

#endif	// __PFDRAW

		// -STOP
	}

	void ResetObject (phInst& instance, const Matrix34& resetMatrix)
	{
		// Reset the instance.
		instance.SetMatrix(RCC_MAT34V(resetMatrix));

		// Make sure the object is active.
		int levelIndex = instance.GetLevelIndex();
		if (levelIndex==phInst::INVALID_INDEX)
		{
			// Put the object back in the world (this is needed when m_AddAndDeleteObjects has been on.
			PHSIM->AddActiveObject(&instance);
			levelIndex = instance.GetLevelIndex();
			Assert(levelIndex!=phInst::INVALID_INDEX);
		}
		else if(!PHLEVEL->IsActive(levelIndex))
		{
			PHSIM->ActivateObject(levelIndex);
		}

		// Reset the the collider.
		PHSIM->GetCollider(levelIndex)->Reset();

		// Inform the physics level the object was reset.
		PHLEVEL->UpdateObjectLocation(levelIndex);
	}

	virtual void Reset ()
	{
		// Reset the two active objects.
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			ResetObject(m_ActiveInstanceList[objectIndex],m_ResetMatrixList[objectIndex]);
		}

		// Reset the physics simulator.
		PHSIM->Reset();
	}

	void ShutdownClient()
	{
		// STEP #10. Shut down the physics level and simulator.
		// -- The user has requested the termination of the sample, so remove the objects from the physics level.
		PHSIM->DeleteObject(&m_TerrainInstance,false);
		for (int objectIndex=0; objectIndex<m_NumObjects; objectIndex++)
		{
			PHSIM->DeleteObject(&m_ActiveInstanceList[objectIndex],false);
		}

		// Delete the physics simulator.
		delete PHSIM;

		// Shut down and delete the physics level.
		PHLEVEL->Shutdown();
		delete PHLEVEL;

		// Call the static shutdown of the simulator.
		phSimulator::ShutdownClass();

		// Delete the material manager.
		MATERIALMGR.Destroy();

#if __PFDRAW
		// Shut down profile drawing.
		GetRageProfileDraw().Shutdown();
#endif
		// -STOP
	}

private:
	// an archetype, two instances and two reset matrices for the icosahedrons
	phArchetypePhys m_ActiveArchetype;
	int m_NumObjects;
	phInst m_ActiveInstanceList[MULTITHREAD_MAX_ACTIVE_OBJECTS];
	Matrix34 m_ResetMatrixList[MULTITHREAD_MAX_ACTIVE_OBJECTS];

	// an archetype and an instance for the terrain
	phArchetype m_TerrainArchetype;
	phInst m_TerrainInstance;

	// user interaction for resets and mouse controls
	ioMapper m_Mapper;
	ioValue m_Reset;
	ioValue m_Push;
	ioValue m_Pull;

	// PURPOSE: whether to randomly add and delete one object per frame, to stress test the simulator and physics level
	bool m_AddAndDeleteObjects;

#if !RSG_ORBIS
	sysTaskHandle m_TaskHandle;
#endif
	sysTaskParameters m_TaskParameters;
};

} // namespace ragesamples

int Main()
{
	{
		ragesamples::spuObjectTestSampleManager *sampleManager = rage_new ragesamples::spuObjectTestSampleManager;
		sampleManager->Init();

		sampleManager->UpdateLoop();

		sampleManager->Shutdown();
		delete sampleManager;
	}
	
	return 0;
}
