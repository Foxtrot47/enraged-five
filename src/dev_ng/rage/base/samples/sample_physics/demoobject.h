// 
// sample_physics/demoobject.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_PHYSICS_DEMOOBJECT_H
#define SAMPLE_PHYSICS_DEMOOBJECT_H

#include "atl/functor.h"
#include "atl/string.h"
#include "data/base.h"
#include "physics/constrainthandle.h"
#include "physics/inst.h"
#include "physics/leveldefs.h"
#include "vector/matrix34.h"

#include "pharticulated/articulatedbody.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/joint.h"

namespace rage {

class bkBank;
class phArticulatedBodySavedState;
class phArticulatedCollider;
class phConstraintMgr;
class phSimulator;
class rmcDrawableBase;	

}


namespace ragesamples {

#define	DEFAULT_HUMAN_PARTS			11
#define	DEFAULT_SWINGING_FREQUENCY	Vector3(0.8f,0.4f,0.6f)
#define	DEFAULT_SWINGING_AMPLITUDE	Vector3(1.0f,1.0f,1.0f)
#define DEFAULT_PHASE_OFFSET		Vector3(0.0f,PI,0.25f*PI)
#define	DEFAULT_ROTATION_FREQUENCY	Vector3(2.8f,2.4f,2.6f)
#define	DEFAULT_ROTATION_AMPLITUDE	Vector3(0.1f,1.5f,0.1f)

using namespace rage;

class phDemoWorld;

////////////////////////////////////////////////////////////////
// phDemoObject

class phDemoObject : public datBase
{
public:
	phDemoObject();
	virtual ~phDemoObject();

    // by default, m_name is the the bound file
    const char* GetName() const { return (const char*)m_name; }
    void SetName( const char *pName ) { m_name = pName; }

	void SetInst (phInst * inst);								// set the phInst to be used
	void SetLevelIndex(u16 levelIndex) {FastAssert(m_PhysInst); m_PhysInst->SetLevelIndex(levelIndex);}
	int GetLevelIndex() const {FastAssert(m_PhysInst); return m_PhysInst->GetLevelIndex();}
	void SetPhysInst(phInst* physInst) { m_PhysInst = physInst; }
	phInst* GetPhysInst() const { return m_PhysInst; }
	void SetDrawSolidIfNoModel(bool drawSolid) { m_DrawSolidIfNoModel = drawSolid; }
	void SetInitialStateFlag (int initial);
	int GetInitialStateFlag () const { return m_InitialStateFlag; }
	void SetDeleteOnShutdown(bool deleteOnShutdown) { m_DeleteOnShutdown = deleteOnShutdown; }
	bool GetDeleteOnShutdown() { return m_DeleteOnShutdown; }
	const Matrix34& GetInitialMatrix() { return m_InitialMatrix; }
	void SetInitialMatrix(const Matrix34& matrix) { m_InitialMatrix = matrix; }
	void SetLabel(const char* label)  { m_Label = label; }
	const char* GetLabel() const { return m_Label; }

	void InitPhys (const char * boundFile, const Vector3 & position, const Vector3 & rotation=ORIGIN, bool uniqueBound=false);
	void InitPhys (const char * boundFile, const Matrix34 & mtx, bool uniqueBound=false, Functor0Ret<phInst*> createFunc=NULL);
	void InitBoxPhys (const Vector3& boxExtents, float density, const Matrix34 & mtx);
	void InitSpherePhys (float radius, float density, const Vector3& position, float maxSpeed=DEFAULT_MAX_SPEED, float maxAngSpeed=DEFAULT_MAX_ANG_SPEED);
	void InitCapsulePhys (float length, float radius, float density, const Matrix34 & mtx);
	void InitRubiksPhys (const Vector3& boxExtents, float spacing, u32 height, u32 width, u32 depth, float density, const Matrix34 & mtx);
	void InitPhys (phArchetype* archetype, const Matrix34 & mtx, Functor0Ret<phInst*> createFunc = NULL);
	void DampMotion (const Vector3& linearC, const Vector3& linearV, const Vector3& linearV2,
						const Vector3& angularC, const Vector3& angularV, const Vector3& angularV2);
	void DampMotion (float linearC, float linearV, float linearV2, float angularC, float angularV, float angularV2);

	void AlignBottom ();
	void SetResetMatrix (const Matrix34 & mtx);
	void SetInitialVelocity (const Vector3& velocity);
	void SetInitialAngVelocity (const Vector3& angVelocity);

	void InitGfx (const char* name);
	void SetModelMatrix (const Matrix34* modelMatrix=NULL) { m_ModelMatrix = modelMatrix; }
	virtual void Reset(phLevelNew *pLevelNew, phSimulator *pSim);

	virtual void DrawModel (const Matrix34 & mtx) const;

#if __BANK
	virtual void AddWidgets( bkBank & );
#endif

	void SetVisibility (bool vis) { m_Visible = vis; }
	bool GetVisibility () { return m_Visible; }
	bool ToggleVisibility () { m_Visible = !m_Visible; return m_Visible; }

	static void SetWorldOffset (const Vector3& worldOffset) { s_WorldOffset.Set(worldOffset); }
	static const Vector3& GetWorldOffset () { return s_WorldOffset; }

protected:
    atString    m_name;
	int m_InitialStateFlag;				// what state the object should be in on reset (fixed, active or inactive)

	phInst* m_PhysInst;					// every demo object has a phys instance, or...
	Matrix34 m_InitialMatrix;				// reset matrix
	Vector3 m_InitialVelocity;
	Vector3 m_InitialAngVelocity;
	rmcDrawableBase* m_Drawable;			// for rm drawing
	const Matrix34* m_ModelMatrix;		// optional matrix for drawing separate from physical matrix

	const char* m_Label;

	bool m_DeleteOnShutdown;

	bool m_Visible;						// for hiding
	bool m_DrawSolidIfNoModel;

	static Vector3 s_WorldOffset;		// static member for placing all objects around a certain position
};

inline void phDemoObject::SetInitialStateFlag (int initial)
{
	m_InitialStateFlag = initial;
}


// A base class for demo objects that need updates, resets and/or inputs.
class phUpdateObject : public phDemoObject
{
public:
	phUpdateObject()											{ }
	virtual ~phUpdateObject()									{ }

	virtual void Update (float UNUSED_PARAM(timeStep))			{ }
	virtual void UpdateInput ()									{ }
};


class phSwingingObject : public phUpdateObject
{
public:
	phSwingingObject () { }
	virtual ~phSwingingObject ();
	virtual void SetMovingConstraint (const Vector3& position, phConstraintMgr* contactMgr, int component=0, const Vector3& frequency=DEFAULT_SWINGING_FREQUENCY,
										const Vector3& amplitude=DEFAULT_SWINGING_AMPLITUDE, const Vector3& phaseOffset=DEFAULT_PHASE_OFFSET);
	virtual void Update (float timeStep);
	virtual bool IsArticulated () const { return false; }
	void SetFrequency (const Vector3& frequency) { m_Frequency.Set(frequency); }
	void SetAmplitude (const Vector3& amplitude) { m_Amplitude.Set(amplitude); }
	void SetPhaseOffset (const Vector3& phaseOffset) { m_PhaseOffset.Set(phaseOffset); }
	void SetRotationFrequency (const Vector3& frequency) { m_RotationFrequency.Set(frequency); }
	void SetRotationAmplitude (const Vector3& amplitude) { m_RotationAmplitude.Set(amplitude); }
	void SetRotationPhaseOffset (const Vector3& phaseOffset) { m_RotationPhaseOffset.Set(phaseOffset); }

	void SetDrawable( rmcDrawableBase* newDrawableObject );
protected:
	phConstraintHandle m_MovingConstraint;
	Vector3 m_Frequency,m_Amplitude,m_PhaseOffset;
	Vector3 m_RotationFrequency,m_RotationAmplitude,m_RotationPhaseOffset;
};


class phWhirlingObject : public phUpdateObject
{
public:
    phWhirlingObject (float period, float mag, float phase = 0.0f);
    virtual ~phWhirlingObject ();

    virtual void Update (float timeStep);
    virtual void Reset (phLevelNew *pLevelNew, phSimulator *pSim);

private:
    Vector3 m_OrgPos;
    float m_Period;
    float m_Phase;
    float m_InitialPhase;
    float m_Mag;
};

class phArticulatedObject : public phSwingingObject
{
public:
	phArticulatedObject () { m_Collider=NULL; m_PuppetBody=NULL; m_ResetState=NULL; m_CollisionTester=NULL; }
	virtual ~phArticulatedObject ();
	phArticulatedCollider& GetCollider () { return *m_Collider; }
	phArticulatedBody& GetBody () { return m_Body; }
	phInst* GetPartInst (int index) { return m_Parts[index]->GetPhysInst(); }
	Vector3 GetWorldPositionOfBoundPart (int index);
	void SetPuppetBody (phArticulatedBody* puppetBody);
	virtual void Reset (phLevelNew *pLevelNew, phSimulator *pSim);
	void SaveResetState();
	virtual void Update (float timeStep);
	virtual void SetMovingConstraint (const Vector3& position, phConstraintMgr* contactMgr, int component=0, const Vector3& frequency=DEFAULT_SWINGING_FREQUENCY,
		const Vector3& amplitude=DEFAULT_SWINGING_AMPLITUDE, const Vector3& phaseOffset=DEFAULT_PHASE_OFFSET);

	phInst* CreateRagdollInst();

	void InitBody (phArchetype* archetype, phInst* instance, phDemoWorld& demoWorld);
	void InitHuman (phDemoWorld& demoWorld, const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN, const Vector3& cgOffset=ORIGIN, int numBodyParts=DEFAULT_HUMAN_PARTS);
	void InitHuman6Bone (phDemoWorld& demoWorld, const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN, const Vector3& cgOffset=ORIGIN, int numBodyParts=DEFAULT_HUMAN_PARTS);
	virtual void InitFromBound (phDemoWorld& demoWorld, const char* name, phJoint::JointType* jointTypeList, const Vector3& position, const Vector3& rotation=ORIGIN, bool allToRoot=true);
	void InitChain (phDemoWorld& demoWorld, bool threeDofJoints, const char* name, const Vector3& position, const Vector3& rotation);
	void InitDresser (phDemoWorld& demoWorld, const char* name="dresser", const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN, bool withDoors=false);

	virtual bool IsArticulated () const { return true; }

	static phArticulatedObject* CreateHuman (phDemoWorld& demoWorld, const Vector3& position=Vector3(0.0f,6.0f,0.0f), const Vector3& rotation=ORIGIN,
												const Vector3& cgOffset=ORIGIN, int numBodyParts=DEFAULT_HUMAN_PARTS);
	static phArticulatedObject* CreateFromBound (phDemoWorld& demoWorld, const char* name, phJoint::JointType* jointTypeList, const Vector3& position, const Vector3& rotation=ORIGIN, bool allToRoot=true);
	static phArticulatedObject* CreateChain (phDemoWorld& demoWorld, bool threeDofJoints, const char* name, const Vector3& position, const Vector3& rotation=ORIGIN);
	static phArticulatedObject* CreateDresser (phDemoWorld& demoWorld, const char* name="dresser", const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN, bool withDoors=false);

protected:
	bool m_Articulated;

	// articulated ragdoll
	phArticulatedCollider* m_Collider;
	phArticulatedBody m_Body;
	phArticulatedBody* m_PuppetBody;	// controlled body of another phArticulatedObject
	phArticulatedBodySavedState* m_ResetState;
#if __PS3
	phArticulatedCollider::DmaPlan* m_DmaPlan;
#endif

	// constrained ragdoll
	atArray<phDemoObject*> m_Parts;

	//If using constraints then we need to tell if body parts collide.
	class ConstrainedHumanBodyPartCollTester* m_CollisionTester;
};

class ConstrainedHumanBodyPart
{
public:

	friend class ConstrainedHumanBodyPartCollTester;

	ConstrainedHumanBodyPart()
		: m_Inst(0),
		  m_BodyPartId(-1)
	{
	}

	const phInst* GetInst() const {return m_Inst;}
	const ConstrainedHumanBodyPartCollTester* GetCollTester() const {return m_CollTester;}
	int GetBodyPartId() const {return m_BodyPartId;}

private:

	void SetCollisionTester(const ConstrainedHumanBodyPartCollTester* collTester)
	{
		m_CollTester=collTester;
	}
	void SetPhysics(const phInst* inst)
	{
		m_Inst=inst;
	}
	void SetBodyPart(const int id)
	{
		m_BodyPartId=id;
	}
	void Reset()
	{
		m_Inst=0;
		m_CollTester=0;
		m_BodyPartId=-1;
	}


	const phInst* m_Inst;
	const ConstrainedHumanBodyPartCollTester* m_CollTester;
	int m_BodyPartId;
};

class ConstrainedHumanBodyPartCollTester
{
public:

	enum
	{
		MAX_NUM_BODY_PARTS=16,
	};

	ConstrainedHumanBodyPartCollTester()
	{
		Reset();
	}

	~ConstrainedHumanBodyPartCollTester()
	{
	}

	void Reset()
	{
		int i;
		for(i=0;i<MAX_NUM_BODY_PARTS;i++)
		{
			m_bodyParts[i].Reset();
			m_bodyParts[i].SetCollisionTester(this);
			m_bodyParts[i].SetBodyPart(i);
		}

		//Set all body part pairs to not collide as default.
		for(i=0;i<MAX_NUM_BODY_PARTS;i++)
		{
			int j;
			for(j=0;j<MAX_NUM_BODY_PARTS;j++)
			{
				m_collisionMatrix[i][j]=false;
			}
		}

		m_NumBodyParts=0;
	}

	void SetPartsCanCollide(const ConstrainedHumanBodyPart* a, const ConstrainedHumanBodyPart* b)
	{
		Assert(a->GetCollTester()==b->GetCollTester() && a->GetCollTester()==this);
		const int bodyPartA=a->GetBodyPartId();
		const int bodyPartB=b->GetBodyPartId();
		Assert(bodyPartA!=bodyPartB && bodyPartA!=-1 && bodyPartB!=-1);
		Assert(!m_collisionMatrix[bodyPartA][bodyPartB]);
		m_collisionMatrix[bodyPartA][bodyPartB]=true;
		m_collisionMatrix[bodyPartB][bodyPartA]=true;
	}

	ConstrainedHumanBodyPart* AddBodyPart(const phInst* inst)
	{
		ConstrainedHumanBodyPart* pBodyPart=0;
		Assert(m_NumBodyParts<MAX_NUM_BODY_PARTS);
		if(m_NumBodyParts<MAX_NUM_BODY_PARTS)
		{
			pBodyPart=&m_bodyParts[m_NumBodyParts];
			m_bodyParts[m_NumBodyParts].SetPhysics(inst);
			m_NumBodyParts++;
		}
		return pBodyPart;
	}

	static bool ShouldFindImpacts(const ConstrainedHumanBodyPart& bodyPartA, const ConstrainedHumanBodyPart& bodyPartB)
	{
		if(bodyPartA.GetCollTester()==bodyPartB.GetCollTester())
		{
			return bodyPartA.m_CollTester->m_collisionMatrix[bodyPartA.GetBodyPartId()][bodyPartB.GetBodyPartId()];
		}
		return true;
	}

private:

	int m_NumBodyParts;
	ConstrainedHumanBodyPart m_bodyParts[MAX_NUM_BODY_PARTS];
	bool m_collisionMatrix[MAX_NUM_BODY_PARTS][MAX_NUM_BODY_PARTS];
};


} // namespace ragesamples

#endif
