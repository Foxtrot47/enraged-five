//
// sample_physics/sample_character.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Character Physics
// PURPOSE:
//		This sample shows how to create and use a simple mover class to move an animated character.

#include "demoobject.h"
#include "sample_physics.h"

#include "grcore/im.h"
#include "grmodel/setup.h"
#include "input/keyboard.h"
#include "input/pad.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/colliderdispatch.h"
#include "physics/constraintfixedrotation.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace rage {

EXT_PFD_DECLARE_ITEM(Models);
EXT_PFD_DECLARE_ITEM(Constraints);

}

namespace ragesamples {

using namespace rage;

static const float DEFAULT_MOVE = 4.0f;
static const float DEFAULT_SPIN = 1.0f;
static const float FLOAT_HEIGHT = 0.1f;

// Make the standing force 2k Newtons downward (200 kg mass with gravity).
static const Vector3 STAND_ON_FORCE(0.0f, -2000.0f, 0.0f);

enum
{
	BOUND_FLAG_GROUND	= 1 << 0,
	BOUND_FLAG_MOVER	= 1 << 1
};

class MoverInst : public phInstBreakable
{
public:
	virtual float GetInactiveImpulseLimit () const { return -1.0f; }
	virtual float GetActiveImpulseLimit () const { return -1.0f; }
	virtual Mat34V_ConstRef GetLastMatrix () const { return m_LastMatrix; }
	virtual void SetLastMatrix ( Mat34V_In last) { m_LastMatrix = last; }
	virtual Vec3V_Out GetExternallyControlledVelocity () const
	{
		// Save locals.
		Vec3V v_velocity = GetPosition() - m_LastMatrix.GetCol3();
		v_velocity = Scale( v_velocity, TIME.GetInvSecondsV() );
		return v_velocity;
	}

public:
	Mat34V m_LastMatrix;
};


class phDemoMover : public phUpdateObject
{

public:
	enum {PUSH,ACCELERATE};

	void Init (phInst* instance, float linearRate=DEFAULT_MOVE, float angularRate=DEFAULT_SPIN, int moveMode=PUSH, bool probe=true)
	{
		m_MoverInst = instance;
		m_InitialPushRate = linearRate;
		m_InitialTurnRate = angularRate;
		m_InitialAccel = linearRate;
		m_InitialAngAccel = angularRate;
		m_InitialMoveMode = moveMode;
		m_InitialProbe = probe;
		Reset(NULL, NULL);
	}

	virtual void Reset (phLevelNew *pLevelNew, phSimulator *pSim)
	{
		if (pLevelNew && pSim)
		{
			phDemoObject::Reset(pLevelNew, pSim);
		}

		m_MoveMode = m_InitialMoveMode;
		m_Probe = m_InitialProbe;
		m_PushRate = m_InitialPushRate;
		m_TurnRate = m_InitialTurnRate;
		m_Accel = m_InitialAccel;
		m_AngAccel = m_InitialAngAccel;
		if (m_MoveMode==PUSH)
		{
			m_LinMotionRate = m_PushRate;
			m_AngMotionRate = m_TurnRate;
		}
		else
		{
			Assert(m_MoveMode==ACCELERATE);
			m_LinMotionRate = m_Accel;
			m_AngMotionRate = m_AngAccel;
		}
		m_Isect.SetInstance(phInst::INVALID_INDEX,0);
		m_YSpeed = 0.0f;
	}

	void SetToPushMode (void) { m_MoveMode = PUSH; m_LinMotionRate = m_PushRate; m_AngMotionRate = m_TurnRate; }
	void SetToAccelMode (void) { m_MoveMode = ACCELERATE; m_LinMotionRate = m_Accel; m_AngMotionRate = m_AngAccel; }
	void SetProbe (bool probe) { m_Probe = probe; }
	void Update (float timeStep)
	{
		Assert(m_MoverInst);
		bool shiftDown = (ioKeyboard::KeyDown(KEY_LSHIFT) || ioKeyboard::KeyDown(KEY_RSHIFT));
		Vector3 linMotion(Vector3::ZeroType);
		Vector3 angMotion(Vector3::ZeroType);
		bool jump = false;
		if (ioKeyboard::KeyDown(KEY_UP))
		{
			if (shiftDown)
			{
				angMotion.z = -m_AngMotionRate;
			}
			else
			{
				linMotion.z = -m_LinMotionRate;
			}
		}
		else if (ioKeyboard::KeyDown(KEY_DOWN))
		{
			if (shiftDown)
			{
				angMotion.z = m_AngMotionRate;
			}
			else
			{
				linMotion.z = m_LinMotionRate;
			}
		}

		if (ioKeyboard::KeyDown(KEY_LEFT))
		{
			if (shiftDown)
			{
				angMotion.x = -m_AngMotionRate;
			}
			else
			{
				linMotion.x = -m_LinMotionRate;
			}
		}
		else if (ioKeyboard::KeyDown(KEY_RIGHT))
		{
			if (shiftDown)
			{
				angMotion.x = m_AngMotionRate;
			}
			else
			{
				linMotion.x = m_LinMotionRate;
			}
		}
		else if (ioKeyboard::KeyDown(KEY_S))
		{
			if (shiftDown)
			{
				angMotion.y = m_AngMotionRate;
			}
			else
			{
				linMotion.y = m_LinMotionRate;
			}
		}
		else if (ioKeyboard::KeyDown(KEY_X))
		{
			if (shiftDown)
			{
				angMotion.y = -m_AngMotionRate;
			}
			else
			{
				linMotion.y = -m_LinMotionRate;
			}
		}

		if (ioKeyboard::KeyPressed(KEY_SPACE) || (ioPad::GetPad(0).GetPressedButtons() & ioPad::RDOWN))
		{
			jump = true;
		}

		linMotion.x += ioAddDeadZone(ioPad::GetPad(0).GetNormLeftX(),0.5f) * m_LinMotionRate;
		linMotion.z += ioAddDeadZone(ioPad::GetPad(0).GetNormLeftY(),0.5f) * m_LinMotionRate;

		if(grcViewport* viewport = grcViewport::GetCurrent())
		{
			Matrix34 mtx(RCC_MATRIX34(viewport->GetCameraMtx()));
			mtx.MakeUpright();
			mtx.Transform3x3(linMotion);
		}

		if (m_Probe)
		{
			phSegment probeSeg;
			Vector3 probeStart = RCC_MATRIX34(m_PhysInst->GetMatrix()).d;
			Vector3 probeEnd = probeStart;
			probeEnd.y -= 10.0f;
			probeSeg.Set(probeStart,probeEnd);
			m_ProbeTester.InitProbe(probeSeg,&m_Isect);
			m_ProbeTester.TestInLevel();
		}

		phLevelNew *physLevel = PHLEVEL;
		int levelIndex = m_MoverInst->GetLevelIndex();
		if (physLevel->IsInactive(levelIndex))
		{
			if (m_MoveMode==PUSH)
			{
				// An instance is being pushed, so there's no need to promote it.
				linMotion.Scale(timeStep);
				angMotion.Scale(timeStep);
				Matrix34 creatureMtx = RCC_MATRIX34(m_MoverInst->GetMatrix());
				creatureMtx.d.Add(linMotion);
				float rotation = angMotion.Mag2();
				if (rotation>0.0f)
				{
					rotation = sqrtf(rotation);
					angMotion.InvScale(rotation);
					creatureMtx.RotateUnitAxis(angMotion,rotation);
				}

				if (jump)
				{
					m_YSpeed += 6.0f;
				}

				m_YSpeed += phSimulator::GetGravity().y * TIME.GetSeconds();
				creatureMtx.d.y	+= m_YSpeed * TIME.GetSeconds();

				if (m_Isect.GetInstance())
				{
					float floor = m_Isect.GetPosition().GetYf() - m_PhysInst->GetArchetype()->GetBound()->GetBoundingBoxMin().GetYf() + FLOAT_HEIGHT;

					if (floor > creatureMtx.d.y)
					{
						m_YSpeed = 0.0f;
						creatureMtx.d.y = floor;

						PHSIM->ApplyForce(ScalarV(timeStep).GetIntrin128(),
										  m_Isect.GetInstance()->GetLevelIndex(),
										  STAND_ON_FORCE,
										  RCC_VECTOR3(m_Isect.GetPosition()),
										  m_Isect.GetComponent());
					}
				}

				m_MoverInst->SetMatrix(RCC_MAT34V(creatureMtx));
				PHLEVEL->UpdateObjectLocation(GetLevelIndex());
			}
			else
			{
				// Tell the level to promote the instance before acceleration.
				phCollider* collider = PHSIM->ActivateObject(levelIndex);
				if (collider)
				{
					collider->ApplyAccel(linMotion,ScalarVFromF32(timeStep).GetIntrin128ConstRef());
					collider->ApplyAngAccel(angMotion,ScalarV(timeStep).GetIntrin128());
				}
			}
		}
		else if (physLevel->IsActive(levelIndex))
		{
			phCollider* collider = PHSIM->GetCollider(levelIndex);
			if (collider)
			{
				if (m_MoveMode==PUSH)
				{
					/*float timeStep = TIME.GetSeconds();
					linMotion.Scale(timeStep);
					angMotion.Scale(timeStep);
					collider->ApplyPush(linMotion);
					collider->ApplyTurn(angMotion);*/
				}
				else
				{
					collider->ApplyAccel(linMotion,ScalarV(timeStep).GetIntrin128());
					collider->ApplyAngAccel(angMotion,ScalarV(timeStep).GetIntrin128());

					if (jump)
					{
						collider->ApplyImpulse(Vector3(0.0f, 1000.0f, 0.0f), ORIGIN);
					}
				}
			}
		}
	}

public:
	phInst* m_MoverInst;
	phShapeTest<phShapeProbe> m_ProbeTester;
	float m_InitialPushRate,m_PushRate;			// velocity during user input when Mode==PUSH
	float m_InitialTurnRate,m_TurnRate;			// angular velocity during user input when Mode==PUSH
	float m_LinMotionRate,m_AngMotionRate;
	float m_InitialAccel,m_Accel;				// acceleration during user input when Mode==ACCELERATE
	float m_InitialAngAccel,m_AngAccel;			// angular acceleration during user input when Mode==ACCELERATE
	int m_InitialMoveMode,m_MoveMode;
	bool m_InitialProbe, m_Probe;
	phIntersection m_Isect;
	float m_YSpeed;
};


const int MaxNumBodies = 128;

class characterPhysicsManager : public physicsSampleManager
{
public:
	virtual void InitClient ()
	{
#if __PFDRAW
        PFD_Models.SetEnabled(false);
#endif // __PFDRAW

		physicsSampleManager::InitClient();
		m_NumBodies = 0;
		Vector3 position,rotation;

        phBound::DisableMessages();

		PFD_ITEM_ENABLE(Constraints, false);

		/////////////////
		// inactive world
		phDemoWorld& inactiveWallWorld = MakeNewCharacterPlayground("inactive world");
		position.Set(0.0f,1.0f,2.0f);
		rotation.Set(ORIGIN);
		bool active = false;
		bool probe = true;
		AddMover(inactiveWallWorld,"capsule_fat",position,rotation,active,probe);

		///////////////
		// active world
		phDemoWorld& activeWallWorld = MakeNewCharacterPlayground("active world");
		active = true;
		probe = false;
		AddMover(activeWallWorld,"capsule_fat",position,rotation,active,probe);

		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();

		ioPad::BeginAll();
	}

	phDemoWorld& MakeNewCharacterPlayground(const char* name)
	{
		Vector3 position,rotation;
		int numStairs;
		bool alignBottom;
		float stairDepth,stairHeight;

        Matrix34 terrainMatrix = CreateRotatedMatrix(Vector3(-948.17468f + 113.541565f, -14.5f, -92.098007f - 173.275284f),
                                                     Vector3(-0.5f * PI, 0.0f, 0.0f));
		phDemoWorld& playground = MakeNewDemoWorld(name, "brook_s", terrainMatrix);

		// Make a ragdoll.
		position.Set(-3.0f,2.0f,-6.0f);
		rotation.Set(-0.2f,0.0f,0.0f);
		phArticulatedObject::CreateHuman(playground,position,rotation);

		// Make a set of stairs.
		position.Set(-3.0f,0.0f,-7.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		numStairs = 2;
		stairHeight = 1.0f;
		stairDepth = 0.0f;
		playground.ConstructBoxStairs("longer_crate",numStairs,position,rotation,stairHeight,stairDepth);

		// Make two rotating doors.
		position.Set(3.0f,0.1f,0.0f);
		alignBottom = true;
		rotation.Set(0.5f*PI,0.0f,0.0f);
		phDemoObject* door = playground.CreateDoor("table_top",position,alignBottom,rotation);
		door->GetPhysInst()->GetArchetype()->AddIncludeFlags(BOUND_FLAG_MOVER);
		door->GetPhysInst()->GetArchetype()->RemoveIncludeFlags(BOUND_FLAG_GROUND);
		position.Set(-3.0f,0.3f,0.0f);
		rotation.Set(-0.5f*PI,0.0f,0.0f);
		door = playground.CreateDoor("table_top",position,alignBottom,rotation);
		door->GetPhysInst()->GetArchetype()->AddIncludeFlags(BOUND_FLAG_MOVER);
		door->GetPhysInst()->GetArchetype()->RemoveIncludeFlags(BOUND_FLAG_GROUND);

		// Make a seesaw
#if 0 // Needs conversion to new constraint API
		position.Set(-0.0f,0.3f,-9.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		phDemoObject* teeterTotter = playground.CreateObject("longer_crate",position,alignBottom,rotation);
		{
			phConstraint* rotationConstraint = PHCONSTRAINT->AttachObjectToWorldRotation('SrS0', *teeterTotter->GetPhysInst());
			rotationConstraint->SetDegreesConstrained(2,Vector3(0,0,1));
			PHCONSTRAINT->AttachObjectToWorld('SrS1', *teeterTotter->GetPhysInst());
		}

		// Make a sliding door.
		position.Set(-2.0f,0.3f,2.0f);
		rotation.Set(-0.5f*PI,0.0f,0.0f);
		phDemoObject* slidingDoor = playground.CreateObject("table_top",position,alignBottom,rotation);
		{
			PHCONSTRAINT->AttachObjectToWorldRotation('SrS2', *slidingDoor->GetPhysInst());
			phConstraint* translationConstraint = PHCONSTRAINT->AttachObjectToWorld('SrS3', *slidingDoor->GetPhysInst());
			translationConstraint->SetHardLimitMin(position + Vector3(-1.0f,0.75f,0.0f));
			translationConstraint->SetHardLimitMax(position + Vector3( 1.0f,0.75f,0.0f));
			translationConstraint->SetConstraintLimitType(phConstraint::LIMIT_TYPE_AXES);
			slidingDoor->GetPhysInst()->GetArchetype()->SetIncludeFlag(BOUND_FLAG_MOVER, true);
			slidingDoor->GetPhysInst()->GetArchetype()->SetIncludeFlag(BOUND_FLAG_GROUND, false);
		}
#endif // Needs conversion to new constraint API

		position.Set(0.0f,0.1f,-3.0f);
		rotation.Set(-0.5f*PI,0.0f,0.0f);
		playground.ConstructPyramid(position,4,false,"box_flat");

		position.Set(0.0f,1.0f,2.0f);
		rotation.Set(ORIGIN);

		position.Set(3.0f, 0.0f, -6.0f);
		playground.ConstructPyramid(position, 4, true);
		
		return playground;
	}

	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,3.0f,6.0f);
		Vector3 lookTo(0.0f,1.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update ()
	{
		physicsSampleManager::Update();
	}

	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}
	
	void AddMover (phDemoWorld& world, const char* name, const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN, bool active=false, bool probe=true)
	{
		if (active)
		{
			phInst* moverInst = rage_new phInst;
			phDemoMover* object = rage_new phDemoMover;
			world.AddUpdateObject(object);
			object->Init(moverInst, DEFAULT_MOVE * 16.0f, DEFAULT_SPIN, phDemoMover::ACCELERATE, probe);
			object->SetToAccelMode();
			object->SetPhysInst(moverInst);
			Matrix34 matrix(CreateRotatedMatrix(position,rotation));
			object->InitPhys(name,matrix);
			Vector3 linearC(2.0f, 0.0f, 2.0f);
			Vector3 linearV(1.0f, 0.0f, 1.0f);
			Vector3 linearV2(2.0f, 0.0f, 2.0f);
			Vector3 angularC(1.0f, 1.0f, 1.0f);
			Vector3 angularV(1.0f, 1.0f, 1.0f);
			Vector3 angularV2(1.0f, 1.0f, 1.0f);

			object->DampMotion(linearC, linearV, linearV2, angularC, angularV, angularV2);

			Assert(object->GetPhysInst());
			phInst& instance = *object->GetPhysInst();
			ASSERT_ONLY(int levelIndex =) world.GetSimulator()->AddActiveObject(&instance);
			Assert(levelIndex!=phInst::INVALID_INDEX);
			object->SetInitialStateFlag(phLevelBase::STATE_FLAG_ACTIVE);
			object->GetPhysInst()->GetArchetype()->AddTypeFlags(BOUND_FLAG_MOVER);

			phConstraintFixedRotation::Params fixedRotation;
			fixedRotation.instanceA = &instance;
			PHCONSTRAINT->Insert(fixedRotation);
		}
		else
		{
			MoverInst* moverInst = rage_new MoverInst;
			phDemoMover* object = rage_new phDemoMover;
			world.AddUpdateObject(object);
			object->Init(moverInst, DEFAULT_MOVE, DEFAULT_SPIN, phDemoMover::PUSH, probe);
			object->SetPhysInst(moverInst);
			Matrix34 matrix(CreateRotatedMatrix(position,rotation));
			object->InitPhys(name,matrix);
			world.AddInactiveObject(object);
			int levelIndex = moverInst->GetLevelIndex();
			world.GetPhLevel()->SetInactiveCollidesAgainstFixed(levelIndex,true);
			world.GetPhLevel()->SetInactiveCollidesAgainstInactive(levelIndex,true);
		}
	}

protected:
	// The next items are the data that define the articulated body
	phArticulatedBody* m_Bodies[MaxNumBodies];					// This structure contains the multibody information
	phArticulatedCollider* m_Colliders[MaxNumBodies];
	int m_NumBodies;

	phDemoWorld* m_GravityWorld;
};

} // namespace ragesamples


int Main()
{
	ragesamples::characterPhysicsManager sampleCharacter;
	sampleCharacter.Init();
	sampleCharacter.UpdateLoop();
	sampleCharacter.Shutdown();
    return 0;
}
