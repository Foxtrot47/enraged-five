// 
// sample_physics/testworlds.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

//
// Some sample worlds, compatible with phDemoWorld.
// These demos simply put passive objects together 
// into scenes.  Demos with programatic actions are
// in testworlds_active.
//

#ifndef SAMPLE_PHYSICS_TESTWORLDS_H
#define SAMPLE_PHYSICS_TESTWORLDS_H

////////////////////////////////////////////////////////////////
// external defines

namespace rage 
{

class phSegment;
class Vector3;

}

namespace ragesamples 
{
    class phDemoWorld;

////////////////////////////////////////////////////////////////

class phTestWorlds
{
public:
	static phDemoWorld * Blank ();								// empty world
	static phDemoWorld * TestWorld1 ();							// test world to change to anything you want
	static phDemoWorld * TypesOfObjects ();						// the various bound primitives
	static phDemoWorld * TestedObjects ();						// objects with
	static phDemoWorld * Crate ();								// only one crate

	static phDemoWorld * DominoBezier ();						// a row of dominos to knock over
	static phDemoWorld * DominoPark ();							// lots of domino rows
	static phDemoWorld * SphereSandbox ();						// many spheres in a box (tests dense rooms)
	static phDemoWorld * CratesAbound ();						// boxes on boxes
	static phDemoWorld * BowlingAlley ();						// bowling pins and a ball
	static phDemoWorld * PyramidsAndSpheres ();					// static pyramid with spheres to knock them over
	static phDemoWorld * PlankBallsAndDominos ();				// ramps to drop balls onto dominos
	static phDemoWorld * Ramps ();								// ramps to push spheres up
	static phDemoWorld * TablesGalore ();						// tables
	static phDemoWorld * GeomHeaven ();							// complicated geometries
	static phDemoWorld * BowlAndBalls ();						// a complicated bowl and a bunch of balls
	static phDemoWorld * TrainWreck ();							// lots of static objects to knock over

	static phDemoWorld * TestDegenerate ();						// bad .bnd files

	static phDemoWorld * TestMultipleFixedCollisions ();		// a collection of fixed objects to collide with simultaneously

	static phDemoWorld * TestLoadOctree ();						// test loading of octree objects

	static phDemoWorld * TestSpacedSpheres ();					// test a bunch of spheres that are in the same general area, but not too
																//   close together

	// utility functions
    static void MakeSeg (rage::phSegment & seg, const rage::Vector3& boxMin, const rage::Vector3& boxMax, float length);
	static void AddCratesInBox (phDemoWorld & demo, int num, const rage::Vector3 & zoneMin, const rage::Vector3 & zoneMax);
};

} // namespace ragesamples

#endif
