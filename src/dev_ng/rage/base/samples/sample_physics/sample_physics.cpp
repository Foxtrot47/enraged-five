//
// sample_physics/sample_physics.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "sample_physics.h"

#include "demoobject.h"

#include "bank/bkmgr.h"
#include "grcore/device.h"
#include "grmodel/setup.h"
#include "input/keys.h"
#include "input/pad.h"
#include "math/random.h"
#include "pharticulated/articulatedcollider.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/param.h"
#include "system/rageroot.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"

#if USE_CONSTRAINT_UPDATE_CHECK
namespace SmokeTests
{
	PARAM(smoketest,"Used to get rid of linker errors in the physics samples.");	// This is used in the physics to halt on certain asserts during smoketests. 
																					// It's not really needed in the samples. This is added to get rid of linker errors.
};
#endif // USE_CONSTRAINT_UPDATE_CHECK

#if !__FINAL && !__TOOL && !__RESOURCECOMPILER
// Hack to work around not linking the fragment cache allocator into the physics samples.
void ValidateFragCachePointer(const void * ptr, const char * ptrName)
{
	(void)ptr;
	(void)ptrName;
}
#endif

namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);
EXT_PFD_DECLARE_ITEM(Models);
EXT_PFD_DECLARE_ITEM(Constraints);
EXT_PFD_DECLARE_GROUP(Impetuses);
EXT_PFD_DECLARE_GROUP(Manifolds);
EXT_PFD_DECLARE_ITEM(ManifoldsB);
EXT_PFD_DECLARE_ITEM(PreviousFrame);
EXT_PFD_DECLARE_ITEM(MarginOriginalPolygons);
EXT_PFD_DECLARE_ITEM(MarginOriginalPolygonsSolid);
EXT_PFD_DECLARE_ITEM(MarginShrunkPolygons);
EXT_PFD_DECLARE_ITEM(MarginExpandedShrunkPolygons);
EXT_PFD_DECLARE_ITEM(MarginExpandedShrunkRoundEdges);
EXT_PFD_DECLARE_ITEM(MarginExpandedShrunkRoundCorners);
EXT_PFD_DECLARE_ITEM_SLIDER(BoundDrawDistance);
}

namespace ragesamples {

PARAM(drawphys,  "[sample_physics] Whether physics drawing is turned on by default");
PARAM(drawimpetuses, "[sample_physics] Whether impetus drawing is turned on by default");
PARAM(drawmanifolds, "[sample_physics] Whether manifold drawing is turned on by default");
PARAM(drawsolid, "[sample_physics] Whether solid physics drawing is turned on by default");
PARAM(drawpreviousframe, "[sample_physics] Whether the previous frame bound drawing is on by default");
PARAM(validatephysics,"[sample_physics] Turn on physics validation");

using namespace rage;

////////////////////////////////////////////////////////////////
// 

physicsSampleManager::physicsSampleManager()
{
}


void physicsSampleManager::SetExtDrawClient (atFunctor0<void>& draw)
{
	m_ExtDraw = draw;
}


grcSetup& physicsSampleManager::AllocateSetup()
{
	return *(rage_new grmSetup());
}

void physicsSampleManager::InitClient()
{
#if __PFDRAW
	GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
	GetRageProfileDraw().SetEnabled(true);
#endif

	PFD_GROUP_ENABLE(Physics, true);
    PFD_ITEM_ENABLE(Models, false);
    PFD_GROUP_ENABLE(Bounds, true);
	PFD_ITEM_ENABLE(Solid, true);
	PFD_ITEM_ENABLE(Wireframe, false);
	PFD_ITEM_ENABLE(Constraints, true);

#if __PFDRAW
	PFD_BoundDrawDistance.SetValue(250.0f);
#endif

	phConfig::EnableRefCounting();

	const char* drawPhysParam;
	if (PARAM_drawphys.Get(drawPhysParam))
	{
		if (stricmp(drawPhysParam, "false") == 0)
		{
			PFD_GROUP_ENABLE(Physics, false);
		}
	}

	if (PARAM_drawimpetuses.Get())
	{
		PFD_GROUP_ENABLE(Impetuses, true);
	}

	if (PARAM_drawmanifolds.Get())
	{
		PFD_GROUP_ENABLE(Manifolds, true);
		PFD_ITEM_ENABLE(ManifoldsB, true);
	}

	const char* drawSolidParam;
	if (PARAM_drawsolid.Get(drawSolidParam))
	{
		if (stricmp(drawSolidParam, "false") == 0)
		{
			PFD_ITEM_ENABLE(Solid, false);
			PFD_ITEM_ENABLE(Wireframe, true);
		}
	}

	if (PARAM_drawpreviousframe.Get())
	{
		PFD_ITEM_ENABLE(PreviousFrame, true);
	}

	if (PARAM_validatephysics.Get())
	{
		ASSERT_ONLY(phConfig::sm_ValidatePhysics = true;)
	}

	phSimulator::InitClass();
	phDemoWorld::InitClass();
#if __BANK
	m_MouseInput.SetActiveInstance();
	phMouseInput::AddClassWidgets(BANKMGR.CreateBank("rage - phMouseInput"));
	phDemoWorld::AddClassWidgets(BANKMGR.CreateBank("rage - Physics Demo World"));
#endif // __BANK
	m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Reset);
	g_ReplayRand.Reset(72);

#if __DEBUGLOG
	const bool replay = false;
	const bool abortOnFailure = true;
	diagDebugLogInit(replay,abortOnFailure);
	g_ReplayRand.SetReplayDebug(true);
#endif
}

void physicsSampleManager::InitCamera ()
{
	Vector3 lookFrom(0.0f,5.0f,-10.0f);
	lookFrom.Add(phDemoObject::GetWorldOffset());
	Vector3 lookTo(0.0f,0.0f,0.0f);
	lookTo.Add(phDemoObject::GetWorldOffset());
	InitSampleCamera(lookFrom,lookTo);
}

void physicsSampleManager::InitLights()
{
	m_Lights.SetColor(0, 1.0f,1.0f,0.71f);
	m_Lights.SetColor(1, 0.1f,0.1f,0.4f);
	m_Lights.SetColor(2, 0.3f,0.3f,0.1f);
	m_Lights.SetPosition(0, Vec3V(20.0f,6.0f,5.0f));
	m_Lights.SetDirection(0, Vec3V(-1.0f,-1.0f,0.0f));
	m_Lights.SetPosition(1, Vec3V(-20.0f,6.0f,-5.0f));
	m_Lights.SetDirection(1, Vec3V(1.0f,-1.0f,0.0f));
	m_Lights.SetPosition(2, Vec3V(5.0f,6.0f,-20.0f));
	m_Lights.SetDirection(2, Vec3V(0.0f,-1.0f,1.0f));
	m_Lights.SetIntensity(0, 1.f);
	m_Lights.SetIntensity(1, 1.f);
	m_Lights.SetIntensity(2, 1.f);
	m_Lights.SetFalloff(0, 5.f);
	m_Lights.SetFalloff(1, 5.f);
	m_Lights.SetFalloff(2, 5.f);

	m_Lights.SetLightType(0, grcLightGroup::LTTYPE_DIR);
	m_Lights.SetLightType(1, grcLightGroup::LTTYPE_DIR);
	m_Lights.SetLightType(2, grcLightGroup::LTTYPE_DIR);

	for (int i=0;i<grcLightGroup::MAX_LIGHTS;i++)
	{
		if (i>2)
		{	
			m_Lights.Reset(i);
		}
	}

	m_Lights.SetActiveCount(3);

	//Initialize the manipulators for the lights
	InitLightManipulators(m_Lights);
}

void physicsSampleManager::ShutdownClient()
{
	m_Demos.Shutdown();

	for (int demo = 0; demo < m_Demos.GetNumDemos(); ++demo)
	{
		delete m_Demos[demo];
	}

#if __PFDRAW
	GetRageProfileDraw().Shutdown();
#endif

	phSimulator::ShutdownClass();
	phDemoWorld::ClassShutdown();
}

void physicsSampleManager::UpdateClient()
{
	m_Demos.Update(&m_CamMgr);

	m_Mapper.Update();

	if (m_Reset.IsPressed() || ioPad::GetPad(0).GetPressedButtons() & ioPad::RRIGHT)
	{
		Reset();
	}
}


void physicsSampleManager::Reset()
{
	g_ReplayRand.Reset(72);

#if __DEBUGLOG
	g_ReplayRand.SetReplayDebug(false);
	diagDebugLogShutdown();

	const bool replay = true;
	const bool abortOnFailure = true;
	diagDebugLogInit(replay,abortOnFailure);
	g_ReplayRand.SetReplayDebug(true);
#endif
}


void physicsSampleManager::DrawClient()
{
	m_Demos.UpdateMouse(ScalarV(m_Demos.GetCurrentWorld()->GetSimTimeLastFrame()).GetIntrin128ConstRef());

    if (m_ExtDraw.IsValid())
    {
        m_ExtDraw();
    }

#if __PFDRAW
	grcBindTexture(NULL);
	grcLightState::SetEnabled(true);

	const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
	GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
	GetRageProfileDraw().Render(phDemoWorld::RedrawWhilePaused() || m_Demos.GetCurrentWorld()->ShouldUpdate());
#endif

	m_Demos.Draw();
}

void physicsSampleManager::DrawHelpClient()
{
	float textLeft = 30.0f;
	float textTop = GRCDEVICE.GetHeight() - 220.0f;

	SetDrawStringPos(textLeft, textTop);

    // GRCDEVICE.BeginBlit();
    DrawString("Awaken All:               A");
    DrawString("Sleep All:                D");
	DrawString("Reset:                    F4");
    DrawString("Grab:                     Left Mouse Button");
    DrawString("Grab with constraint:     Left Shift + Left Mouse Button");
    DrawString("Pin grab:                 Left Mouse Button + Click Mouse Wheel");
    DrawString("Release all pinned grabs: R");
    DrawString("In/out while grabbing:    Mouse Wheel");
    DrawString("Push:                     Click Mouse Wheel");
    DrawString("Pull:                     Left Shift + Click Mouse Wheel");
    DrawString("Push/Pull Scale:          Mouse Wheel");
    DrawString("Slow down time:           [");
    DrawString("Speed up time:            ]");

    DrawString("Pause:                    , ");
    DrawString("FrameAdvance:             .");
    DrawString("Temporary unpause:        Hold .");

	textLeft = GRCDEVICE.GetWidth() - 180.0f;
	textTop = (float)m_CameraHelpY;

	SetDrawStringPos(textLeft,textTop);

	if (m_Demos.GetNumDemos() > 0)
	{
		if (m_Demos.GetNumDemos()>1)
		{
			// There is more than one demo world, so draw instructions on paging through worlds.
			const bool fadeWithTime = false;
			DrawString("Next World: +", fadeWithTime);
			DrawString("Previous World: -", fadeWithTime);
		}

		char tmpString[100];

		phDemoWorld* world = m_Demos.GetCurrentWorld();
		Assert(world);

		formatf(tmpString, sizeof(tmpString), "World: %s", world->GetName());
		DrawString(tmpString, false);

		int separatorLen = StringLength(tmpString);
		for (int i=0; i<separatorLen; i++)
		{
			tmpString[i] = '-';
		}
		tmpString[separatorLen] = '\0';
		DrawString(tmpString, false);

		formatf(tmpString, sizeof(tmpString), "real time: %6.3f", world->GetFrameTime() * 1000.0f);

		DrawString(tmpString, false);

		formatf(tmpString,
			    sizeof(tmpString),
			    "sim time:  %6.3f %s%s",
			    world->GetSimTimeLastFrame() * 1000.0f,
				world->GetFixedFrame() ? "*" : "",
                world->GetAltFxiedFrame() ? "+" : "");

		DrawString(tmpString, false);

		formatf(tmpString, sizeof(tmpString), "max sim time: %3.f", world->GetMaximumFrameTime() * 1000.0f);
		DrawString(tmpString, false);

		DrawString(phMouseInput::GetLeftClickModeString(), false);
		DrawString(phMouseInput::GetScaleString(), false);

		formatf(tmpString, sizeof(tmpString), "timewarp:  %6.3f", world->GetTimeWarp());
		DrawString(tmpString, false);

		formatf(tmpString, sizeof(tmpString), "oversample:  %4d", world->GetOversampling());
		DrawString(tmpString, false);

		formatf(tmpString, sizeof(tmpString), "gravity:   %6.3f", world->GetGravityScale() * GRAVITY);
		DrawString(tmpString, false);

		if (!world->ShouldUpdate())
		{
			DrawString("Paused", false); 
		}
	}
	// GRCDEVICE.EndBlit();
}

bool physicsSampleManager::IsPaused()
{
    return !GetCurrentWorld()->ShouldUpdate();
}

phDemoWorld& physicsSampleManager::MakeNewDemoWorld (const char* worldName, const char* terrainName, const Vector3& terrainPosition, const int maxOctreeNodes, int maxActiveObjects,
                                                     int maxObjects, const Vector3& worldMin, const Vector3& worldMax, int maxInstBehaviors, int scratchpadSize, int maxManifolds,
													 int maxExternVelManifolds)
{
    Matrix34 terrainMatrix(M34_IDENTITY);
    terrainMatrix.d.Set(terrainPosition);
    return MakeNewDemoWorld(worldName, terrainName, terrainMatrix, maxOctreeNodes, maxActiveObjects, maxObjects, 
		worldMin, worldMax, maxInstBehaviors, scratchpadSize, maxManifolds, maxExternVelManifolds);
}

phDemoWorld& physicsSampleManager::MakeNewDemoWorld (const char* worldName, const char* terrainName, const Matrix34& terrainMatrix, const int maxOctreeNodes, int maxActiveObjects,
														int maxObjects, const Vector3& worldMin, const Vector3& worldMax, int maxInstBehaviors, int scratchpadSize, int maxManifolds,
														int maxExternVelManifolds)
{
	// Create and initialize the world.
	phDemoWorld& world = *CreateNewDemoWorld(worldName, terrainName, terrainMatrix, maxOctreeNodes, maxActiveObjects,
		maxObjects, worldMin, worldMax, maxInstBehaviors, scratchpadSize, maxManifolds, maxExternVelManifolds);

#if __BANK
	char worldWidgetName[64];
	formatf(worldWidgetName,sizeof(worldWidgetName),"Demo World: %s",worldName);;
	world.AddWidgets(BANKMGR.CreateBank(worldWidgetName));
#endif

	// Add the world to the list of worlds, activate it and return it.
    m_Demos.AddDemo(&world);
    world.Activate();
	return world;
}


phDemoWorld* physicsSampleManager::CreateNewDemoWorld (const char* worldName, const char* terrainName, const Vector3& terrainPosition, const int maxOctreeNodes, int maxActiveObjects,
													   int maxObjects, const Vector3& worldMin, const Vector3& worldMax, int maxInstBehaviors, int scratchpadSize, int maxManifolds,
													   int maxExternVelManifolds)
{
	Matrix34 terrainMatrix(M34_IDENTITY);
	terrainMatrix.d.Set(terrainPosition);
	return CreateNewDemoWorld(worldName, terrainName, terrainMatrix, maxOctreeNodes, maxActiveObjects, maxObjects, worldMin, 
		worldMax, maxInstBehaviors, scratchpadSize, maxManifolds, maxExternVelManifolds);
}

phDemoWorld* physicsSampleManager::CreateNewDemoWorld (const char* worldName, const char* terrainName, const Matrix34& terrainMatrix, const int maxOctreeNodes, int maxActiveObjects,
													 int maxObjects, const Vector3& worldMin, const Vector3& worldMax, int maxInstBehaviors, int scratchpadSize, int maxManifolds,
													 int maxExternVelManifolds)
{
	// Create and initialize the world.
	phDemoWorld& world = *(rage_new phDemoWorld(worldName));
	Vector3 offsetWorldMin(worldMin);
	offsetWorldMin.Add(phDemoObject::GetWorldOffset());
	Vector3 offsetWorldMax(worldMax);
	offsetWorldMax.Add(phDemoObject::GetWorldOffset());
	world.Init(maxOctreeNodes, maxActiveObjects, maxObjects, Vec3V(offsetWorldMin.xyzw), Vec3V(offsetWorldMax.xyzw), maxInstBehaviors, scratchpadSize, maxManifolds, maxExternVelManifolds,3000);
	world.InitUpdateObjects(256);

	// Create the terrain.
	if (terrainName)
	{
		world.CreateFixedObject(terrainName,terrainMatrix);
	}
	else
	{
		bool triangulated = true;
		u32 includeFlags = INCLUDE_FLAGS_ALL;
		bool zUp = false;
		world.ConstructTerrainPlane(triangulated,includeFlags,zUp,terrainMatrix.d);
	}

	return &world;
}

phDemoWorld* physicsSampleManager::GetCurrentWorld ()
{
	return m_Demos.GetCurrentWorld();
}


phDemoMultiWorld& physicsSampleManager::GetWorldContainer ()
{
	return m_Demos;
}


} // namespace ragesamples
