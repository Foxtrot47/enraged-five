//
// sample_physics/sample_shape_tests.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Shape Tests
// PURPOSE:
//		This sample demonstrates shape tests, such as probes and spheres, on objects in the physics level.

#include "sample_physics.h"

#include "grmodel/setup.h"
#include "physics/shapetest.h"
#include "system/main.h"
#include "system/param.h"
#include "vectormath/classes.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

#define SAMPLE_PERFORM_REPEATED_CAPSULE_TESTS	0

namespace rage {

EXT_PFD_DECLARE_ITEM(ProbeSegments);
EXT_PFD_DECLARE_ITEM(SphereSegments);
EXT_PFD_DECLARE_ITEM(CapsuleSegments);
EXT_PFD_DECLARE_ITEM(ProbeIsects);
EXT_PFD_DECLARE_ITEM(SphereIsects);
EXT_PFD_DECLARE_ITEM(CapsuleIsects);
EXT_PFD_DECLARE_ITEM(ProbeNormals);
EXT_PFD_DECLARE_ITEM(SphereNormals);
EXT_PFD_DECLARE_ITEM(CapsuleNormals);

}


namespace ragesamples {

using namespace rage;

enum {	OBJECT_TYPE_TERRAIN = BIT1,
		OBJECT_TYPE_VEHICLE = BIT2
};


class ShapeTestManager : public physicsSampleManager
{
public:
	virtual void InitClient ()
	{
#if SAMPLE_PERFORM_REPEATED_CAPSULE_TESTS
		phBound::DisableMessages(true);
#endif

		// Initialize the physics sample manager.
		physicsSampleManager::InitClient();

		// Create a physics level and simulator. The default terrain (a flat plane) is offset downward to allow room for the octree grid bound.
		Vector3 position(0.0f,-4.0f,0.0f);
		const char* terrainName = NULL;
		phDemoWorld& shapeTestWorld = MakeNewDemoWorld("shape tests",terrainName,position);

		// Add a small octree grid bound, and set its type flag to terrain for use by some shape tests.
		position.Set(2133.0f,position.y-16.5f,-2682.0f);
		phInst* terrainInstance = shapeTestWorld.CreateFixedObject("octree_grid",position)->GetPhysInst();
		terrainInstance->GetArchetype()->SetTypeFlags(OBJECT_TYPE_TERRAIN);

		// Add some fixed primitive bounds. The capsule's instance is saved to exclude from a shape test.
		position.Set(0.0f,2.0f,0.0f);
		shapeTestWorld.CreateFixedObject("sphere_056",position);
		position.Set(-4.0f,2.0f,0.0f);
		m_CapsuleInstance = shapeTestWorld.CreateFixedObject("capsule_fat",position)->GetPhysInst();
		position.Set(-4.0f,3.0f,-8.0f);
		shapeTestWorld.CreateFixedObject("crate",position);

		// Add a fixed geometry bound, and set its include flags to exclude vehicles, for use by a shape test.
		position.Set(4.0f,3.0f,-8.0f);
		phInst* icosahedronInstance = shapeTestWorld.CreateFixedObject("icosahedron",position)->GetPhysInst();
		PHLEVEL->ClearInstanceIncludeFlags(icosahedronInstance->GetLevelIndex(),OBJECT_TYPE_VEHICLE);

		// Add a curved geometry bound.
		position.Set(-3.0f,4.0f,-4.0f);
//		shapeTestWorld.CreateCurvedGeometryCylinder(position);

		m_BatchedTests.GetShape().AllocateProbes(16);
		m_BatchedTests.GetShape().AllocateSpheres(16);
		m_BatchedTests.GetShape().AllocateCapsules(16);

	#if __PFDRAW
		// Turn on the shape test line drawing.
		PFD_ProbeSegments.SetEnabled(true);
		PFD_SphereSegments.SetEnabled(true);
		PFD_CapsuleSegments.SetEnabled(true);
		PFD_ProbeIsects.SetEnabled(true);
		PFD_SphereIsects.SetEnabled(true);
		PFD_CapsuleIsects.SetEnabled(true);
		PFD_ProbeNormals.SetEnabled(true);
		PFD_SphereNormals.SetEnabled(true);
		PFD_CapsuleNormals.SetEnabled(true);
	#endif

	}

	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,3.0f,6.0f);
		Vector3 lookTo(0.0f,1.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update ()
	{
		/////////////////////////
		// Section 1: Probe Tests
		/////////////////////////

		// Initialize the probe test. This uses the default intersection pointer of NULL, which means the probe will only return whether or not it hit anything,
		// with no information about what it hit. This probe is directed, so exit intersections (going out of an object from the probe start to end) do not count.
		Vector3 probeStart(7.0f,-2.0f,-6.0f);
		Vector3 probeEnd(0.0f,-2.0f,-6.0f);
		phSegment segment;
		segment.Set(probeStart,probeEnd);
		m_ProbeTest.InitProbe(segment);

		// Test the probe in the level, and get the number of hits (0 or 1).
		int numHits = m_ProbeTest.TestInLevel();
		Assert(numHits==0 || numHits==1);

		// Re-initialize the probe with an intersection, which will be filled in with information on the first thing the probe hits.
		probeStart.Set(7.0f,-1.0f,-6.0f);
		probeEnd.Set(0.0f,-1.0f,-6.0f);
		segment.Set(probeStart,probeEnd);
		phIntersection intersection;
		m_ProbeTest.InitProbe(segment,&intersection);

		// Test the probe in the level, getting the first intersection from start to end along the probe.
		numHits = m_ProbeTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());

		// Re-initialize the probe with a specified number of intersections. Instead of finding the first thing it hits, it will fill intersections in the order it finds things
		// that it hits, stopping when it hits no more things or when the list of intersections is full.
		const int numIntersections = 12;
		phIntersection intersectionList[numIntersections];
		probeStart.Set(0.0f,6.0f,-10.0f);
		probeEnd.Set(0.0f,-6.0f,0.0f);
		segment.Set(probeStart,probeEnd);
		m_ProbeTest.InitProbe(segment,intersectionList,numIntersections);

		// Test the probe in the level, filling the intersection list in the order that it finds things, not necessarily in order from start to end along the probe.
		numHits = m_ProbeTest.TestInLevel();
		Assert(numHits>=0 && numHits<=numIntersections);
		for (int hitIndex=0; hitIndex<numHits; hitIndex++)
		{
			Assert(intersectionList[hitIndex].IsAHit());
		}

		// Re-initialize the probe as undirected. It will find the first thing it hits, going from the probe start to end, regardless of whether it's entering or exiting.
		// There's no need to reset the intersection, which is being reused from an earlier test, because TestInLevel() will reset any intersections that are given.
		m_ProbeTest.InitEdge(segment,&intersection);

		// Test the probe in the level, getting the first entry and last exit intersections from start to end along the probe.
		// If there is a hit, it could be either the entry intersection or the exit intersection or both.
		numHits = m_ProbeTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());



		//////////////////////////
		// Section 2: Sphere Tests
		//////////////////////////

		// Create a center and radius for sphere tests.
		Vector3 sphereCenter(0.0f,2.0f,0.0f);
		float sphereRadius = 1.0f;

		// Initialize the sphere test. This uses the default intersection pointer of NULL, which means the test will only return a boolean to tell whether or not it hit anything,
		// with no information about what it hit.
		m_SphereTest.InitSphere(sphereCenter,sphereRadius);

		// Test the sphere in the level, and get a boolean to tell whether or not it hit anything.
		numHits = m_SphereTest.TestInLevel();
		Assert(numHits==0 || numHits==1);

		// Re-initialize the sphere with an intersection, which will be filled in with information on the deepest thing penetrating the sphere.
		m_SphereTest.InitSphere(sphereCenter,sphereRadius,&intersection);

		// Test the sphere in the level, getting the deepest intersection.
		numHits = m_SphereTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());

		// Re-initialize the sphere with a specified number of intersections. Instead of finding the deepest thing it hits, it will fill intersections in the order it finds things
		// that it hits, stopping when it hits no more things or when the list of intersections is full.
		m_SphereTest.InitSphere(sphereCenter,sphereRadius,intersectionList,numIntersections);

		// Test the sphere in the level, filling the intersection list in the order that it finds things, not necessarily in order of depth in the sphere.
		numHits = m_SphereTest.TestInLevel();
		Assert(numHits>=0 && numHits<=numIntersections);
		for (int hitIndex=0; hitIndex<numHits; hitIndex++)
		{
			Assert(intersectionList[hitIndex].IsAHit());
		}

		// Re-initialize the sphere as a point and test it in the level. Point tests have limited use, since they don't work on polyhedral bounds.
		// It would be easy enough to make them work on convex bounds if there's any need for that.
		m_SphereTest.InitPoint(sphereCenter,&intersection);
		numHits = m_SphereTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());



		///////////////////////////
		// Section 3: Capsule Tests
		///////////////////////////

		// Create axis and points and a radius for capsule tests. The axis end points are the centers of the hemispheres on the ends.
		Vector3 capsuleAxisStart(0.0f,2.0f,2.0f);
		Vector3 capsuleAxisEnd(0.0f,2.0f,-2.0f);
		segment.Set(capsuleAxisStart,capsuleAxisEnd);
		float capsuleRadius = 1.0f;

		// Initialize the capsule test. This uses the default intersection pointer of NULL, which means the test will only return a boolean to tell whether or not it hit anything,
		// with no information about what it hit.
		m_CapsuleTest.InitCapsule(segment,capsuleRadius);

		// Test the capsule in the level, and get a boolean to tell whether or not it hit anything.
		numHits = m_CapsuleTest.TestInLevel();
		Assert(numHits==0 || numHits==1);

		// Re-initialize the capsule with an intersection, which will be filled in with information on the deepest thing penetrating the capsule.
		m_CapsuleTest.InitCapsule(segment,capsuleRadius,&intersection);

		// Test the capsule in the level, getting the deepest intersection.
		numHits = m_CapsuleTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());

		// Re-initialize the capsule with a specified number of intersections. Instead of finding the deepest thing it hits, it will fill intersections in the order it finds things
		// that it hits, stopping when it hits no more things or when the list of intersections is full.
		m_CapsuleTest.InitCapsule(segment,capsuleRadius,intersectionList,numIntersections);

		// Test the capsule in the level, filling the intersection list in the order that it finds things, not necessarily in order of depth in the capsule.
		numHits = m_CapsuleTest.TestInLevel();
		Assert(numHits>=0 && numHits<=numIntersections);
		for (int hitIndex=0; hitIndex<numHits; hitIndex++)
		{
			Assert(intersectionList[hitIndex].IsAHit());
		}

		// Re-initialize the capsule as directed. It will find the first thing a moving sphere hits, moving from the axis start to end.
		// It will not detect any intersections in the start position. This is intended for creature and camera movers to find collision-free
		// motion paths. A separate sphere test should be added at the start position to detect intersections there.
		m_CapsuleTest.InitSweptSphere(segment,capsuleRadius,&intersection);

		// Test the directed capsule (swept sphere) in the level, getting the first intersection from start to end along the axis, not including any intersections at the start.
		numHits = m_CapsuleTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());

#if SAMPLE_PERFORM_REPEATED_CAPSULE_TESTS
		///////////////////////////
		// Section 3b: Repeated Capsule Tests
		///////////////////////////

		static float s_RepeatedCapsuleTestTimer = 0.0f;
		static int s_RepeatedCapsuleTestCounter = 0;
		const int repeatedCapsuleTestMaxCount = 100;

		sysTimer oTimer;
#if __PS3
		const int shapeTestCount = 3000;
#else
		const int shapeTestCount = 1000;
#endif

		for(int x = shapeTestCount; x >= 0; --x)
		{
			// Test the directed capsule (swept sphere) in the level, getting the first intersection from start to end along the axis, not including any intersections at the start.
			numHits = m_CapsuleTest.TestInLevel();
			Assert(numHits==0 || numHits==1);
			Assert(numHits==0 || intersection.IsAHit());
		}
		s_RepeatedCapsuleTestTimer += oTimer.GetMsTime();
		++s_RepeatedCapsuleTestCounter;
		if(s_RepeatedCapsuleTestCounter == repeatedCapsuleTestMaxCount)
		{
			Displayf("Timing: %f (%f)", s_RepeatedCapsuleTestTimer, s_RepeatedCapsuleTestTimer / (float)(s_RepeatedCapsuleTestCounter));
		}
#if __XENON
		const float invShapeTestCount = 1.0f / (float)(shapeTestCount);
		const float shapeTestRadius = 2.0f;

		float floatCounter = 0.0f;
		for(int x = shapeTestCount; x >= 0; --x)
		{
			// Test the directed capsule (swept sphere) in the level, getting the first intersection from start to end along the axis, not including any intersections at the start.
			const float angle = floatCounter * 2.0f * PI * invShapeTestCount;
			float cosAngle = Cosf(angle);
			float sinAngle = Sinf(angle);
			segment.A.Set(shapeTestRadius * cosAngle, -2.0f, shapeTestRadius * sinAngle);
			segment.B.Set(shapeTestRadius * cosAngle, +2.0f, shapeTestRadius * sinAngle);
			m_CapsuleTest.InitSweptSphere(segment,capsuleRadius,&intersection);
			numHits = m_CapsuleTest.TestInLevel();
			Assert(numHits==0 || numHits==1);
			Assert(numHits==0 || intersection.IsAHit());
			floatCounter += 1.0f;
		}
#endif
#endif

		//////////////////////////
		// Section 4: Object Tests
		//////////////////////////

		// Load a bound file and make a position and orientation.
		phBound* bound = phBound::Load("physics/vp_elise_04");
		Assert(bound);
		Matrix34 pose(M34_IDENTITY);
		pose.RotateTo(XAXIS,YAXIS);
		pose.d.Set(1.0f,1.0f,-1.0f);

		// Initialize the object test. This uses the default intersection pointer of NULL, which means the sphere will only return a boolean to tell whether or not it hit anything,
		// with no information about what it hit.
		m_ObjectTest.InitObject(*bound,pose);

		// Test the object in the level, and get a boolean to tell whether or not it hit anything.
		numHits = m_ObjectTest.TestInLevel();
		Assert(numHits==0 || numHits==1);

		// Re-initialize the object test with an intersection, which will be filled in with information on the deepest thing penetrating the object.
		m_ObjectTest.InitObject(*bound,pose,&intersection);

		// Test the object in the level, getting the deepest intersection.
		numHits = m_ObjectTest.TestInLevel();
		Assert(numHits==0 || numHits==1);
		Assert(numHits==0 || intersection.IsAHit());

		// Re-initialize the test with a specified number of intersections. Instead of finding the deepest thing it hits, it will fill intersections in the order it finds things
		// that it hits, stopping when it hits no more things or when the list of intersections is full.
		m_ObjectTest.InitObject(*bound,pose,intersectionList,numIntersections);

		// Test the object in the level, filling the intersection list in the order that it finds things, not necessarily in order of depth in the object.
		numHits = m_ObjectTest.TestInLevel();
		Assert(numHits>=0 && numHits<=numIntersections);
		for (int hitIndex=0; hitIndex<numHits; hitIndex++)
		{
			Assert(intersectionList[hitIndex].IsAHit());
		}



		///////////////////////////
		// Section 5: Batched Tests
		///////////////////////////

		// Shape tests can be combined to save time by culling the physics level and cullable bounds only once for a collection of shapes that are nearby each other.
		// The only restriction is MAX_BATCHED_PER_SHAPE, which is defined to be 4 in physics/shapetest.h. This could be increased if there is any need.
		// Remove shapes added on the previous frame.
// 		m_BatchedTests.GetShape().ResetNumProbes();
// 		m_BatchedTests.GetShape().ResetNumSpheres();
// 		m_BatchedTests.GetShape().ResetNumCapsules();

		// Initialize two probe tests. There is an optional index number argument to initialize a particular test in the batch.
		phIntersection probe1Isect,probe2Isect;
		probeStart.Set(1.0f,0.0f,1.0f);
		probeEnd.Set(-1.0f,0.0f,-1.0f);
		segment.Set(probeStart,probeEnd);
		m_BatchedTests.InitProbe(segment,&probe1Isect);
		probeStart.Set(1.5f,1.0f,1.0f);
		probeEnd.Set(-0.5f,1.0f,-1.0f);
		segment.Set(probeStart,probeEnd);
		m_BatchedTests.InitProbe(segment,&probe2Isect);

		// Initialize a sphere test with multiple intersections.
		const int numSphereIsects = 6;
		phIntersection sphereIsectList[numSphereIsects];
		sphereCenter.Set(0.0f,0.5f,0.0f);
		sphereRadius = 0.3f;
		m_BatchedTests.InitSphere(sphereCenter,sphereRadius,sphereIsectList);

		// Test the batch of shapes in the level, and get the highest number of hits among the shapes (0 to 6 for this case).
		numHits = m_BatchedTests.TestInLevel();
		Assert(probe1Isect.IsAHit() || probe2Isect.IsAHit() || sphereIsectList[0].IsAHit() || numHits==0);



		/////////////////////////////////////////
		// Section 6: Exclude Instances and Flags
		/////////////////////////////////////////

		// Test another probe in the level, and make it ignore the object with the capsule bound.
		// The exclude instance is useful for making creature mover shape tests ignore the creature.
		probeStart.Set(2.0f,2.5f,0.0f);
		probeEnd.Set(-2.0f,2.0f,0.0f);
		segment.Set(probeStart,probeEnd);
		m_ProbeTest.InitProbe(segment,&intersection);
		m_ProbeTest.TestInLevel(m_CapsuleInstance);

		// Repeat the exclude test, using the exclude instance list instead, which can hold more than one.
		const int numExcludeInstances = 1;
		m_ProbeTest.SetExcludeInstanceList(&m_CapsuleInstance,numExcludeInstances);
		m_ProbeTest.TestInLevel();
		m_ProbeTest.ClearExcludeInstanceList();

		// Test another sphere in the level, and make it hit only the terrain.
		sphereCenter.Set(-3.0f,0.0f,0.0f);
		sphereRadius = 2.0f;
		m_SphereTest.InitSphere(sphereCenter,sphereRadius,&intersection);
		const phInst* excludeInstance = NULL;
		int includeFlags = OBJECT_TYPE_TERRAIN;
		m_SphereTest.TestInLevel(excludeInstance,includeFlags);

		// Test another sphere in the level, and make it miss anything that doesn't include vehicles.
		sphereCenter.Set(3.0f,0.0f,0.0f);
		sphereRadius = 1.5f;
		m_SphereTest.InitSphere(sphereCenter,sphereRadius,&intersection);
		includeFlags = -1;
		int typeFlags = OBJECT_TYPE_TERRAIN;
		m_SphereTest.TestInLevel(excludeInstance,includeFlags,typeFlags);

		// Test another swept sphere in the level, and make it hit only fixed objects (it will ignore the cylindrical object).
		capsuleAxisStart.Set(-1.0f,0.0f,1.0f);
		capsuleAxisEnd.Set(1.0f,0.0f,1.0f);
		segment.Set(capsuleAxisStart,capsuleAxisEnd);
		capsuleRadius = 0.2f;
		m_CapsuleTest.InitSweptSphere(segment,capsuleRadius,intersectionList,numIntersections);
		typeFlags = -1;
		m_CapsuleTest.TestInLevel(excludeInstance,includeFlags,typeFlags,phLevelBase::STATE_FLAG_FIXED);



		/////////////////////////////////
		// Section 7: Tests on One Object
		/////////////////////////////////

		// Test a probe on one single object.
		probeStart.Set(2.0f,2.5f,0.0f);
		probeEnd.Set(-2.0f,2.0f,0.0f);
		segment.Set(probeStart,probeEnd);
		m_ProbeTest.InitProbe(segment,&intersection);
		m_ProbeTest.TestOneObject(*const_cast<phInst*>(m_CapsuleInstance));

		// The instance is not required. Test again with only a bound.
		m_ProbeTest.TestOneObject(*bound);



		// Update the physics sample manager.
		physicsSampleManager::Update();
	}

	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}

protected:
	// A shape tester to do probe tests in the physics level.
	phShapeTest<phShapeProbe> m_ProbeTest;

	// A shape tester to do sphere tests in the physics level.
	phShapeTest<phShapeSphere> m_SphereTest;

	// A shape tester to do capsule tests in the physics level.
	phShapeTest<phShapeCapsule> m_CapsuleTest;

	phShapeTest<phShapeObject> m_ObjectTest;

	// A shape tester to do batched shape tests in the physics level.
	phShapeTest<phShapeBatch> m_BatchedTests;

	// An instance to exclude from some shape tests.
	const phInst* m_CapsuleInstance;
};

} // namespace ragesamples


int Main()
{
	ragesamples::ShapeTestManager sampleShapeTests;
	sampleShapeTests.Init();
	sampleShapeTests.UpdateLoop();
	sampleShapeTests.Shutdown();
    return 0;
}
