//
// sample_physics/sample_contacts.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Contacts
// PURPOSE:
//		This sample creates stacks of objects of various types for testing the computation of contact forces.

#include "demoobject.h"
#include "sample_physics.h"

#include "bank/bkmgr.h"
#include "math/random.h"
#include "phsolver/contactmgr.h"
#include "physics/simulator.h"
#include "physics/shapetest.h"
#include "phbound/boundgeom.h"
#include "phbound/support.h"
#include "phcore/phmath.h"
#include "grprofile/drawmanager.h"
#include "system/main.h"
#include "system/param.h"
#include "vectormath/classes.h"

#if __WIN32
#pragma warning (disable : 4189) // local variable is initialized but not referenced
#pragma warning (disable : 4101) // unreferenced local variable
#endif

PARAM(world, "The demo world to initialize when the demo boots");
PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

#define NUMRAYS 200

phShapeTest<phShapeProbe> probeTests[NUMRAYS];
phIntersection probeHits[NUMRAYS];
bool probesBuilt = false;

using namespace rage;

////////////////////////////////////////////////////////////////
// 

class contactsSampleManager : public physicsSampleManager
{
public:

	void createWall (phDemoWorld& scene, const Vector3& offsetPosition, int stackSize)
	{
		float bodyMass = 0.5f;
		Vector3 boxSize(1.0f, 1.0f, 1.0f);

		float diffY = boxSize[1] * 0.5f * 1.02f;
		float diffZ = boxSize[2] * 0.5f * 1.02f;

		float offset = -stackSize * (diffZ * 2.0f) * 0.5f;
		Vector3 pos(0.0f, diffY, 0.0f);

		while(stackSize) {
			for(int i=0;i<stackSize;i++) {
				pos[2] = offset + (float)i * (diffZ * 2.0f);

				scene.CreateBox (boxSize, bodyMass, offsetPosition + pos);
			}
			offset += diffZ;
			pos[1] += (diffY * 2.0f);
			stackSize--;
		}
	}

	void createPyramid (phDemoWorld& scene, const Vector3 &offsetPosition,int stackSize)
	{
		float space = 0.0001f;
		Vector3 boxSize2(1.0f, 1.0f, 1.0f);
		Vector3 boxSize(0.5f, 0.5f, 0.5f);
		Vector3 pos(0.0f, boxSize[1], 0.0f);


		float diffX = boxSize[0] * 1.02f;
		float diffY = boxSize[1] * 1.02f;
		float diffZ = boxSize[2] * 1.02f;

		float offsetX = -stackSize * (diffX * 2.0f + space) * 0.5f;
		float offsetZ = -stackSize * (diffZ * 2.0f + space) * 0.5f;
		while(stackSize) {
			for(int j=0;j<stackSize;j++) {
				pos[2] = offsetZ + (float)j * (diffZ * 2.0f + space);
				for(int i=0;i<stackSize;i++) {
					pos[0] = offsetX + (float)i * (diffX * 2.0f + space);
					scene.CreateBox (boxSize2, 1.0, offsetPosition + pos);
				}
			}
			offsetX += diffX;
			offsetZ += diffZ;
			pos[1] += (diffY * 2.0f + space);
			stackSize--;
		}
	}

	void createTowerCircle(phDemoWorld& scene, const Vector3 &offsetPosition,int stackSize,int rotSize)
	{
		Vector3 boxSize (0.5f, 0.5f, 0.5f);
		Vector3 boxSize2 (1.0f, 1.0f, 1.0f);

		float radius = 1.3f * rotSize * boxSize[0] / MY_PI;

		
		// create active boxes
		Quaternion rotY;
		rotY.Identity ();

		float posY = boxSize[1];

		for(int i=0;i<stackSize;i++) {
			for(int j=0;j<rotSize;j++) {
				Matrix34 xform;
				xform.Identity ();
				xform.FromQuaternion (rotY);
				Vector3 arm(0.0, posY, radius);
				Vector3 armout;
				rotY.Transform(arm, armout);
				xform.d = offsetPosition + armout;
				scene.CreateBox (boxSize2, 0.5f, xform);
				Quaternion q;
				q.FromRotation (Vector3(0.0f, 1.0f, 0.0f), MY_PI / (rotSize*0.5f));
				rotY.Multiply (q);
			}

			posY += boxSize[1] * 2.0f;
			Quaternion q;
			q.FromRotation (Vector3(0.0f, 1.0f, 0.0f), MY_PI / (float)rotSize);
			rotY.Multiply (q);
		}
	}

	phDemoWorld* Create3000Boxes()
	{
		Matrix34 groundMtx(M34_IDENTITY);
		groundMtx.d.Set(0.0f, -5.0f, 0.0f);
		phDemoWorld* scene2 = CreateNewDemoWorld("3000 Boxes", "box_large_flat", groundMtx, 3000, 3100, 3100, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);

		int size = 8;
		const float cubeSize = 1.0f;
		float spacing = cubeSize;
		Vector3 pos(0.0f, cubeSize * 2,0.f);
		float offset = -size * (cubeSize * 2.0f + spacing) * 0.5f;
		Vector3 doubleCube(2.0f*cubeSize, 2.0f*cubeSize, 2.0f*cubeSize);

		float mass = 2.f;

		for(int k=0;k<47;k++) {
			for(int j=0;j<size;j++) {
				pos[2] = offset + (float)j * (cubeSize * 2.0f + spacing);
				for(int i=0;i<size;i++) {
					pos[0] = offset + (float)i * (cubeSize * 2.0f + spacing);

					scene2->CreateBox(doubleCube, mass, Vector3(pos));
				}
			}
			offset -= 0.05f * spacing * (size-1);
			spacing *= 1.01f;
			pos[1] += (cubeSize * 2.0f + spacing);
		}

		return scene2;
	}

	phDemoWorld* CreateRubiksStack()
	{
		Matrix34 groundMtx(M34_IDENTITY);
		groundMtx.d.Set(0.0f, -5.0f, 0.0f);
		phDemoWorld* scene2 = CreateNewDemoWorld("Rubiks Stack", "box_large_flat", groundMtx, 3000, 3100, 3100, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);

		const float cubeSize = 3.0f;
		float spacing = cubeSize * 0.5f;
		Vector3 pos(0.0f, cubeSize,0.f);
		float offset = -(cubeSize * 2.0f + spacing);
		int rubiks = 5;
		Vector3 doubleCube;
		doubleCube.Set(cubeSize / float(rubiks + 1));

		float mass = 2.f;

		for(int k=0;k<5;k++) {
			for(int j=0;j<5;j++) {
				pos[2] = offset + (float)j * (cubeSize + spacing) * 0.75f;
				for(int i=0;i<5;i++) {
					pos[0] = offset + (float)i * (cubeSize + spacing) * 0.75f;

					scene2->CreateRubiks(doubleCube, cubeSize / float(rubiks), rubiks, rubiks, rubiks, mass, Vector3(pos));
				}
			}
			pos[1] += (cubeSize + 0.5f * spacing);
		}

		return scene2;
	}

	phDemoWorld* CreateStacksWorld()
	{
		phDemoWorld& scene8 = *CreateNewDemoWorld("scene8", NULL, M34_IDENTITY, 2000, 2000, 2000, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);

		createWall(scene8, Vector3(-2.0f,0.0f,0.0f), 12 );
		createWall(scene8, Vector3(4.0f,0.0f,0.0f),  12 );
		createWall(scene8, Vector3(10.0f,0.0f,0.0f), 12 );
		createPyramid(scene8, Vector3(-20.0f,0.0f,0.0f),12);
		createTowerCircle(scene8, Vector3(25.0f,0.0f,0.0f),8,24);

		return &scene8;
	}

	phDemoWorld* CreateRagdolls()
	{
		phDemoWorld* scene3 = CreateNewDemoWorld("Ragdolls", NULL, M34_IDENTITY, 200, 200, 200, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,3000);
		{
			int size = 16;

			float sizeX = 1.f;
			float sizeY = 1.f;

			Vector3 pos(0.0f, sizeY, 0.0f);
			while(size) {
				float offset = -size * (sizeX * 6.0f) * 0.5f;
				for(int i=0;i<size;i++) {
					pos[0] = offset + (float)i * (sizeX * 6.0f);

					phArticulatedObject::CreateHuman (*scene3, pos);
				}

				offset += sizeX;
				pos[1] += (sizeY * 7.0f);
				pos[2] -= sizeX * 2.0f;
				size--;
			}
		}

		return scene3;
	}

	phDemoWorld* CreateConvex()
	{
		phDemoWorld* scene4 = CreateNewDemoWorld("Convex", NULL, M34_IDENTITY, 2000, 2000, 2000, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);
		{

			int size = 8;
			const float cubeSize = 1.5f;
			float spacing = cubeSize;
			Vector3 pos(0.0f, cubeSize * 2, 0.0f);
			float offset = -size * (cubeSize * 2.0f + spacing) * 0.5f;

			for(int k=0;k<15;k++) {
				for(int j=0;j<size;j++) {
					pos[2] = offset + (float)j * (cubeSize * 2.0f + spacing);
					for(int i=0;i<size;i++) {
						pos[0] = offset + (float)i * (cubeSize * 2.0f + spacing);
						scene4->CreateObject ("convexcrate", pos);
					}
				}
				offset -= 0.05f * spacing * (size-1);
				spacing *= 1.01f;
				pos[1] += (cubeSize * 2.0f + spacing);
			}
		}
		return scene4;
	}

	phDemoWorld* CreatePrimsOnMesh()
	{
		phDemoWorld* scene5 = CreateNewDemoWorld("Prims On Mesh", NULL, M34_IDENTITY, 2000, 2000, 2000, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);

		{
			Vector3 boxSize(1.5f,1.5f,1.5f);
			float boxMass = 1.0f;
			float sphereRadius = 1.5f;
			float sphereMass = 1.0f;
			float capsuleHalf = 2.0f;
			float capsuleRadius = 1.0f;
			float capsuleMass = 1.0f;

			{
				int size = 10;
				int height = 10;

				const float cubeSize = boxSize[0];
				float spacing = 2.0f;
				Vector3 pos(0.0f, 20.0f, 0.0f);
				float offset = -size * (cubeSize * 2.0f + spacing) * 0.5f;

				int numBodies = 0;

				for(int k=0;k<height;k++) {
					for(int j=0;j<size;j++) {
						pos[2] = offset + (float)j * (cubeSize * 2.0f + spacing);
						for(int i=0;i<size;i++) {
							pos[0] = offset + (float)i * (cubeSize * 2.0f + spacing);
							Vector3 bpos = Vector3(0,25,0) + Vector3(5.0f,1.0f,5.0f)*pos;
							int idx = rand() % 9;

							switch(idx) {
						case 0:case 1:case 2:
							{
								float r = 0.5f * (idx+1);
								scene5->CreateBox (boxSize,boxMass*r, bpos);
							}
							break;

						case 3:case 4:case 5:
							{
								float r = 0.5f * (idx-3+1);
								scene5->CreateSphere(sphereRadius, sphereMass*r, bpos);
							}
							break;

						case 6:case 7:case 8:
							{
								float r = 0.5f * (idx-6+1);
								scene5->CreateCapsule (capsuleHalf*r, capsuleRadius*r, capsuleMass*r, bpos);
							}
							break;
							}

							numBodies++;
						}
					}
					offset -= 0.05f * spacing * (size-1);
					spacing *= 1.1f;
					pos[1] += (cubeSize * 2.0f + spacing);
				}
			}
			scene5->CreateFixedObject ("benchmesh1", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh2", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh3", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh4", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh5", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh6", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh7", Vector3(0.0, -25.0, 0.0));
			scene5->CreateFixedObject ("benchmesh8", Vector3(0.0, -25.0, 0.0));
		}
		return scene5;
	}

	phDemoWorld* CreateConvexOnMesh()
	{
		phDemoWorld* scene6 = CreateNewDemoWorld("Convex On Mesh", NULL, M34_IDENTITY, 2000, 2000, 2000, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);
		{
			Vector3 boxSize(1.5f,1.5f,1.5f);

			{
				int size = 10;
				int height = 10;

				const float cubeSize = boxSize[0];
				float spacing = 2.0f;
				Vector3 pos(0.0f, 20.0f, 0.0f);
				float offset = -size * (cubeSize * 2.0f + spacing) * 0.5f;


				for(int k=0;k<height;k++) {
					for(int j=0;j<size;j++) {
						pos[2] = offset + (float)j * (cubeSize * 2.0f + spacing);
						for(int i=0;i<size;i++) {
							pos[0] = offset + (float)i * (cubeSize * 2.0f + spacing);
							Vector3 bpos = Vector3(0,25,0) + Vector3(5.0f,1.0f,5.0f)*pos;

							scene6->CreateObject ("convexcrate", bpos);
						}
					}
					offset -= 0.05f * spacing * (size-1);
					spacing *= 1.1f;
					pos[1] += (cubeSize * 2.0f + spacing);
				}
			}
			scene6->CreateFixedObject ("benchmesh1", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh2", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh3", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh4", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh5", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh6", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh7", Vector3(0.0, -25.0, 0.0));
			scene6->CreateFixedObject ("benchmesh8", Vector3(0.0, -25.0, 0.0));
		}

		return scene6;
	}

	phDemoWorld* CreateProbeWorld()
	{
		phDemoWorld* scene7 = CreateNewDemoWorld("Probes", NULL, M34_IDENTITY, 2000, 2000, 2000, Vector3(-999,-999,-999), Vector3(999,999,999),0,20*1024*1024,30000);

		Vector3 rayStart = Vector3(0,50,0);
		float rayAngleY = 0.0f;
		float rayAngleZ = 0.3f;
		for(int i=0;i<NUMRAYS;i++) {
			rayAngleY += 2.0f * 3.14f / (float)NUMRAYS;
			Matrix33 Ry;
			Matrix33 Rz;
			Ry.MakeRotateY(rayAngleY);
			Rz.MakeRotateZ(rayAngleZ);
			Matrix33 R;
			R.Dot(Ry,Rz);
			Vector3 rayDir = Vector3(-1,0,0);
			R.Transform(rayDir);
			Vector3 source = rayStart;
			Vector3 dest = rayStart+150.0f*rayDir;
			phSegment segment;
			segment.Set(source, dest);
			probeTests[i].InitProbe (segment, &probeHits[i]);
		}
		probesBuilt = true;

		{
			Vector3 boxSize(1.5f,1.5f,1.5f);
			float boxMass = 1.0f;
			float sphereRadius = 1.5f;
			float sphereMass = 1.0f;
			float capsuleHalf = 2.0f;
			float capsuleRadius = 1.0f;
			float capsuleMass = 1.0f;

			{
				int size = 10;
				int height = 10;

				const float cubeSize = boxSize[0];
				float spacing = 2.0f;
				Vector3 pos(0.0f, 20.0f, 0.0f);
				float offset = -size * (cubeSize * 2.0f + spacing) * 0.5f;

				int numBodies = 0;

				for(int k=0;k<height;k++) {
					for(int j=0;j<size;j++) {
						pos[2] = offset + (float)j * (cubeSize * 2.0f + spacing);
						for(int i=0;i<size;i++) {
							pos[0] = offset + (float)i * (cubeSize * 2.0f + spacing);
							Vector3 bpos = Vector3(0,25,0) + Vector3(5.0f,1.0f,5.0f)*pos;
							int idx = rand() % 9;

							switch(idx) {
						case 0:case 1:case 2:
							{
								float r = 0.5f * (idx+1);
								scene7->CreateBox (boxSize,boxMass*r, bpos);
							}
							break;

						case 3:case 4:case 5:
							{
								float r = 0.5f * (idx-3+1);
								scene7->CreateSphere(sphereRadius, sphereMass*r, bpos);
							}
							break;

						case 6:case 7:case 8:
							{
								float r = 0.5f * (idx-6+1);
								scene7->CreateCapsule (capsuleHalf*r, capsuleRadius*r, capsuleMass*r, bpos);
							}
							break;
							}

							numBodies++;
						}
					}
					offset -= 0.05f * spacing * (size-1);
					spacing *= 1.1f;
					pos[1] += (cubeSize * 2.0f + spacing);
				}
			}
			scene7->CreateFixedObject ("benchmesh1", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh2", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh3", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh4", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh5", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh6", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh7", Vector3(0.0, -25.0, 0.0));
			scene7->CreateFixedObject ("benchmesh8", Vector3(0.0, -25.0, 0.0));
		}

		return scene7;
	}
	virtual void InitClient()
	{
		phSimulator::SetShouldFindImpactsEnabled(false);
		phSimulator::SetPreComputeImpactsEnabled(false);
		phSimulator::SetReportMovedBySimEnabled(false);
		phSimulator::SetMaintainLooseOctree(false);
		phContactMgr::SetLCPSolverIterations(5);
		phContactMgr::SetLCPSolverIterationsFinal(0);
		phSimulator::SetSortPairsByCost(false);

		// Disable breaking to save a couple of percent speed
		phContactMgr::SetBreakingEnabled(false);

		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();

		// STEP #2. Define an offset for all objects in the world.
		//-- Change this vector to test physics behavior with roundoff errors far from the origin.
		Vector3 worldOffset(0.0f,0.0f,0.0f);
		phDemoObject::SetWorldOffset(worldOffset);

		// STEP #3. Set the physics level parameters.
		//-- See sample_physics/sample_basic for a demonstration of how to set up a simple physics level and simulator.
		Vector3 worldMin(-999.0f,-999.0f,-999.0f);
		Vector3 worldMax(999.0f,999.0f,999.0f);
		worldMin.Add(phDemoObject::GetWorldOffset());
		worldMax.Add(phDemoObject::GetWorldOffset());
		Vector3 position,rotation,boxExtents;
        mthRandom random;

		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateRubiksStack));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::Create3000Boxes));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateStacksWorld));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateRagdolls));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateConvex));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreatePrimsOnMesh));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateConvexOnMesh));
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &contactsSampleManager::CreateProbeWorld));

		// STEP #5. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		int startingDemo = 0;
		PARAM_world.Get(startingDemo);
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
		PHSIM->SetSleepEnabled(false);
	}

	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,25.0f,50.0f);
		lookFrom.Add(phDemoObject::GetWorldOffset());
		Vector3 lookTo(0.0f,0.0f,0.0f);
		lookTo.Add(phDemoObject::GetWorldOffset());
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update ()
	{
		physicsSampleManager::Update();
		if (probesBuilt && m_Demos.GetCurrentNumber() == 0)
		{
			for (int i=0;i<NUMRAYS;i++)
			{
				probeTests[i].TestInLevel();
			}
		}
	}
};

} // namespace ragesamples


// main application
int Main()
{
	{
		ragesamples::contactsSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
