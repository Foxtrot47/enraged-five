#include "objecttestspu.h"

#include "phbound/boundsphere.h"
#include "physics/collider.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"

#if __SPU

using namespace rage;

rage::phCollider::phCollider() { }
rage::phCollider::~phCollider() { }

// Need to define this so that there's not a linker error - phManifold::AddManifoldPoint() expects this function to exist (although it would be nice if it
//   didn't even try to look for it.
void ReleaseManifold(rage::phManifold *UNUSED_PARAM(manifold))
{
}

void objecttestspu (::rage::sysTaskParameters &taskParams)
{
#if ENABLE_PHYSICS_LOCK
	phMultiReaderLockToken spuPhysicsLock;
	RageCellSyncMutex *ppuModifyReaderCountMutex = (RageCellSyncMutex *)taskParams.UserData[4].asPtr;
	spuPhysicsLock.SetPPUModifyReaderCountMutexPtr(ppuModifyReaderCountMutex);
	u32 *ppuReaderCount = (u32 *)taskParams.UserData[1].asPtr;
	spuPhysicsLock.SetPPUReaderCountPtr(ppuReaderCount);
	RageCellSyncMutex *ppuPhysicsLock = (RageCellSyncMutex *)taskParams.UserData[2].asPtr;
	spuPhysicsLock.SetPPUPhysicsMutexPtr(ppuPhysicsLock);
#		if LOCKCONFIG_WRITERS_ONLY_WAIT_FOR_CURRENT_READERS
	RageCellSyncMutex *ppuAllowNewReaderLock = (RageCellSyncMutex *)taskParams.UserData[3].asPtr;
	spuPhysicsLock.SetPPUAllowNewReaderMutexPtr(ppuAllowNewReaderLock);
#		endif	// LOCKCONFIG_WRITERS_ONLY_WAIT_FOR_CURRENT_READERS
#endif	// ENABLE_PHYSICS_LOCK

	phIterator levelIterator(phIterator::PHITERATORLOCKTYPE_READLOCK,&spuPhysicsLock);

	// DMA the 'root' part of the physics level over to the SPU so that we can use it.
	const phLevelNew *ppuLevel = (const phLevelNew *)(taskParams.UserData[0].asPtr);
	u8 levelBuffer[sizeof(phLevelNew)];
	cellDmaLargeGet(levelBuffer, (uint64_t)(ppuLevel), sizeof(phLevelNew), DMA_TAG(15), 0, 0);
	cellDmaWaitTagStatusAll(DMA_MASK(15));
	const phLevelNew *spuLevel = (const phLevelNew *)(levelBuffer);

	const int maxCulledPolys = 200;//MAX_SPU_NUM_CULLED_POLYS;

#if 1
	phBoundSphere sphereBound;
	sphereBound.SetSphereRadius(1.0f);
	phShapeTest<phShapeObject> objectTest;
	Mat34V mtx(V_IDENTITY);
	//mtx.SetCol3(Vec3V(-0.2f, 1.0f, 0.3f));
	mtx.SetCol3(Vec3V(0.0f, 0.8f, 0.0f));
	phIntersection isectResult;
	isectResult.Zero();
	objectTest.GetShape().InitObject(sphereBound, mtx, mtx, &isectResult, 1);
#else
	phShapeTest<phShapeProbe> objectTest;
	objectTest.GetShape().InitProbe(phSegmentV(Vec3V(V_ZERO), Vec3V(V_ZERO)), NULL, 0);
#endif

	phShapeTestCullResults cullResults;
	cullResults.AllocatePrimitiveIndexArray(maxCulledPolys);
	objectTest.SetCullResults(&cullResults);

	Assert(spuLevel != NULL);
	Displayf("Before TestInLevel");
	objectTest.TestInLevel(NULL, levelIterator, INCLUDE_FLAGS_ALL, TYPE_FLAGS_ALL, phLevelBase::STATE_FLAGS_ALL, TYPE_FLAGS_NONE, spuLevel);
	Displayf("After TestInLevel");
	const bool isAHit = isectResult.IsAHit();
	if(isAHit)
	{
		Displayf("Hit %p!", isectResult.GetInstance());
		const Vec3V hitPos = isectResult.GetPosition();
		Displayf("Position: <%f, %f, %f>", hitPos.GetXf(), hitPos.GetYf(), hitPos.GetZf());
		const Vec3V hitNormal = isectResult.GetNormal();
		Displayf("Normal: <%f, %f, %f>", hitNormal.GetXf(), hitNormal.GetYf(), hitNormal.GetZf());
		Displayf("Depth: %f", isectResult.GetDepth());
	}
	else
	{
		Displayf("Missed!");
	}

	cullResults.ReleasePrimitiveIndexArray();
}

#endif	// __SPU
