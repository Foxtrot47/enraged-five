//
// sample_physics/sample_constraint.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Constraint
// PURPOSE:
//		This sample creates sets of objects with different types of physical constraints for testing the computation of constraint forces.
//		Physical constraints can be point constraints or rotational constraints. Point constraints can hold together points on different objects,
//		or hold a point on an object to a point in the world. Rotational contraints can fix the orientation of two objects relative to each other,
//		or fix the orientation of an object in world space. Both types of constraints can also exert a restoring force or torque to enforce
//		the constraint over time while allowing some separation.
//		Another type of constrained motion, which is also included in this sample, is done with constrained colliders. These are colliders with
//		restricted degrees of freedom, such as motion or rotation only along certain axes. In some cases the same physical result can be obtained
//		with physical constraints or with constrained colliders.
//		Constraints on articulated bodies are in sample_articonstraint. This sample only has constraints between rigid bodies.

#include "demoobject.h"
#include "sample_physics.h"

#include "curve/curvemgr.h"
#include "devcam/mayacam.h"
#include "devcam/polarcam.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/conversion.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/constraintfixedrotation.h"
#include "physics/constraints.h"
#include "physics/constraintmgr.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"
#include "vectormath/legacyconvert.h"

#define	MAX_NUM_TRAIN_CARS	12

PARAM(world, "The demo world to initialize when the demo boots");
PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

#if ENABLE_UNUSED_CURVE_CODE
// PURPOSE: Manage a spline curve rail for constrained motion of train cars.
// NOTES:	This is used by the constraint manager to load and draw a curve that constrains the position and orientation of train cars.
//			An example of how to update constraints to keep objects moving along the curve is in the constraint manager's UpdateClient.
class TrainTracks
{
public:
	// PURPOSE: Initialize the spline curve for the train tracks.
	void Init ()
	{
		// Load the curve and give it to the curve manager.
		char curveFilePath[256];
		strcpy(curveFilePath,"physics\\train_tracks\\tracks");
		m_CurveManager.Load(curveFilePath,"curve");
		m_TrackCurve = m_CurveManager.GetCurve(0);

		// Set the minimum and maximum t-values for moving along the curve.
		m_CurveMinT = 0.01f;
		m_CurveMaxT = 0.99f;
	}

	// PURPOSE: Draw line segments along the otherwise graphics-free train tracks.
	void DrawTracks ()
	{
		Color32 oldColor(grcCurrentColor);
		grcColor(Color_DarkOrange);
		grcBindTexture(NULL);
		bool oldLighting = grcLighting(false);
		const int numCurvePoints = 32;
		const float tInterval = (m_CurveMaxT-m_CurveMinT)/(float)(numCurvePoints-1);
		float t = m_CurveMinT;
		const float trackHalfWidth = 0.6f;
		Vector3 curvePoint,tangent,offset;
		grcBegin(drawLineStrip,numCurvePoints);
		for (int pointIndex=0; pointIndex<numCurvePoints; pointIndex++)
		{
			m_TrackCurve->SolveSegment(curvePoint,0,t,0.0f,&tangent);
			curvePoint.x += tangent.z*trackHalfWidth;
			curvePoint.z -= tangent.x*trackHalfWidth;
			grcVertex3fv(&curvePoint.x);
			t += tInterval;
		}

		grcEnd();

		t = m_CurveMinT;
		grcBegin(drawLineStrip,numCurvePoints);
		for (int pointIndex=0; pointIndex<numCurvePoints; pointIndex++)
		{
			m_TrackCurve->SolveSegment(curvePoint,0,t,0.0f,&tangent);
			curvePoint.x -= tangent.z*trackHalfWidth;
			curvePoint.z += tangent.x*trackHalfWidth;
			grcVertex3fv(&curvePoint.x);
			t += tInterval;
		}

		grcEnd();

		grcLighting(oldLighting);
		grcColor(oldColor);
	}

	cvCurve<Vector3>* GetCurve () { return m_TrackCurve; }
	float GetCurveMinT () const { return m_CurveMinT; }
	float GetCurveMaxT () const { return m_CurveMaxT; }

protected:
	cvCurveMgr m_CurveManager;
	cvCurve<Vector3>* m_TrackCurve;
	float m_CurveMinT,m_CurveMaxT;
};
#endif // ENABLE_UNUSED_CURVE_CODE


// PURPOSE: Create sets of constrained objects for testing the computation of constraint forces.
class ConstraintManager : public physicsSampleManager
{
public:

	phDemoWorld* CreateRotationConstrainedCapsuleUnderHeavyContact()
	{
		phDemoWorld& world = *CreateNewDemoWorld("rotation constrained capsule under heavy contact");
		Vector3 position(1.5f,2.0f,0.0f);
		Vector3 rotation(ORIGIN);
		bool alignBottom = false;

		//Create a capsule that will be constrained to the world by 3 rotational constraints.
		//Rotate the capsule about the x-axis by 45 degrees.
		const float length=4.0f;
		const float radius=0.5f;
		const float density=1.0f;
		const float rotationX=0.0f;
		const float rotationY=0.0f;
		const float rotationZ=1.57f;
		phDemoObject* rotationConstrainedCapsule = world.CreateCapsule(length,radius,density,position,alignBottom,Vector3(rotationX,rotationY,rotationZ));

		//Constrain the capsule with 3 rotational constraints.
		phConstraintFixedRotation::Params constraintParams;
		constraintParams.instanceA = rotationConstrainedCapsule->GetPhysInst();
		PHCONSTRAINT->Insert(constraintParams);

		//Now create a single sphere that will collide with the constrained capsule.
		//Make this spheres 1000 times the mass of the constrained capsule to really test
		//the stability of the constrained capsule.
		//Make the sphere fall from a great height too.
		world.CreateSphere(radius,density*1000.0f,position+Vector3(length*0.5f,radius*5,0),alignBottom);

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &world;
	}


	phDemoWorld* CreateTranslationConstrainedSphereUnderHeavyContact()
	{
		phDemoWorld& world = *CreateNewDemoWorld("translation constrained sphere under heavy contact");
		Vector3 position(1.5f,2.0f,0.0f);
		Vector3 rotation(ORIGIN);
		bool alignBottom = false;

		//Create a sphere constrained to the world with a translation constraint through its centre.
		const float radius=0.5f;
		const float density=1.0f;
		phDemoObject* translationConstrainedSphere = world.CreateSphere(radius,density,position,alignBottom);

		//Add the translation constraint.
		Vector3 constraintOffset(0,0,0);
		Vector3 boxMin=VEC3V_TO_VECTOR3(translationConstrainedSphere->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMin());
		Vector3 boxMax=VEC3V_TO_VECTOR3(translationConstrainedSphere->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax());
		const Vector3 centreBody=(boxMin+boxMax)*0.5f;
		Matrix34 mat(M34_IDENTITY);
		mat.d.Set(position+constraintOffset);
		Vector3 centreWorld;
		mat.Transform(centreBody,centreWorld);

		phConstraintSpherical::Params constraint;
		constraint.instanceA = translationConstrainedSphere->GetPhysInst();
		constraint.worldPosition = RCC_VEC3V(centreWorld);
		PHCONSTRAINT->Insert(constraint);

		//Now create some spheres that will collide with the constrained sphere.
		//Make these spheres 1000 times the mass of the constrained sphere.
		const int iMaxNumSpheres=64;
		int i;
		for(i=0;i<iMaxNumSpheres;i++)
		{
			world.CreateSphere(radius,density*1000.0f,position+Vector3(0.001f,radius*10.0f*(i+1),0),alignBottom);
		}

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;


		return &world;
	}

	phDemoWorld* CreateTranslationAndRotationConstrainedSphereUnderHeavyContact()
	{
		phDemoWorld& world = *CreateNewDemoWorld("translation constrained sphere under heavy contact");
		Vector3 position(1.5f,2.0f,0.0f);
		Vector3 rotation(ORIGIN);
		bool alignBottom = false;

		//Create a sphere constrained to the world with a translation constraint at its side and rotation forbidden
		//except around the x-axis.
		const float radius=0.5f;
		const float density=1.0f;
		phDemoObject* translationConstrainedSphere = world.CreateSphere(radius,density,position,alignBottom);
		translationConstrainedSphere->SetInitialAngVelocity(Vector3(2,2,2));

		//Add the translation constraint.
		Vector3 constraintOffset(radius,0,0);
		Vector3 boxMin=VEC3V_TO_VECTOR3(translationConstrainedSphere->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMin());
		Vector3 boxMax=VEC3V_TO_VECTOR3(translationConstrainedSphere->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax());
		const Vector3 centreBody=(boxMin+boxMax)*0.5f;
		Matrix34 mat(M34_IDENTITY);
		mat.d.Set(position+constraintOffset);
		Vector3 centreWorld;
		mat.Transform(centreBody,centreWorld);

		phConstraintHinge::Params constraint;
		constraint.instanceA = translationConstrainedSphere->GetPhysInst();
		constraint.worldAnchor = RCC_VEC3V(centreWorld);
		constraint.worldAxis = Vec3V(V_X_AXIS_WZERO);
		PHCONSTRAINT->Insert(constraint);

		//Now create some spheres that will collide with the constrained sphere.
		//Make these spheres 10 times the mass of the constrained sphere.
		const int iMaxNumSpheres=16;
		int i;
		for(i=0;i<iMaxNumSpheres;i++)
		{
			world.CreateSphere(radius,density*10.0f,position+Vector3(0.001f,radius*10.0f*(i+1),0),alignBottom);
		}

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;


		return &world;
	}

	phDemoWorld* CreateConstrainedDoors()
	{
		phDemoWorld& world = *CreateNewDemoWorld("Swing doors under heavy contact");
		Vector3 positions[4]={Vector3(0.65f,1.0f,0.0f),Vector3(-0.65f,1.0,0.0),Vector3(0.65f,1.0f,-10.0f),Vector3(-0.65f,1.0,-10.0)};
		const Vector3 boxExtents(1.0f,1.0f,0.1f);
		Vector3 offsets[4]={Vector3(0.5f*boxExtents.x,0,0),Vector3(-0.5f*boxExtents.x,0,0),Vector3(0.5f*boxExtents.x,0,0),Vector3(-0.5f*boxExtents.x,0,0)};

		Vector3 rotation(ORIGIN);
		//bool alignBottom = false;

		//Create two pairs of swing doors.
		int i;
		for(i=0;i<4;i++)
		{
			//Create a door.
			Matrix34 boxTransform;
			boxTransform.Identity();
			boxTransform.d.Set(positions[i]);
			const float targetMass=1.0f;
			const float boxVolume=boxExtents.x*boxExtents.y*boxExtents.z;
			const float density=0.001f*targetMass/boxVolume;
			phDemoObject* constrainedDoor = world.CreateBox(boxExtents,density,boxTransform);

			Vector3 boxMin=VEC3V_TO_VECTOR3(constrainedDoor->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMin());
			Vector3 boxMax=VEC3V_TO_VECTOR3(constrainedDoor->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax());
			const Vector3 centreBody=(boxMin+boxMax)*0.5f;
			Matrix34 mat(M34_IDENTITY);
			mat.d.Set(positions[i]+offsets[i]);
			Vector3 centreWorld;
			mat.Transform(centreBody,centreWorld);
			if(0==i || 2==i)
			{
				phConstraintFixed::Params constraint;
				constraint.instanceA = constrainedDoor->GetPhysInst();
				PHCONSTRAINT->Insert(constraint);
			}
			else
			{
				phConstraintHinge::Params constraint;
				constraint.instanceA = constrainedDoor->GetPhysInst();
				constraint.worldAnchor = RCC_VEC3V(centreWorld);
				constraint.worldAxis = Vec3V(V_Y_AXIS_WZERO);
				PHCONSTRAINT->Insert(constraint);
			}
		}

		//Make some heavy balls that will roll along the ground and hit the swing doors.
		for(i=0;i<16;i++)
		{
			const float radius=0.5f;
			const float targetMass=1000;
			const float sphereVolume=1.333f*PI*radius*radius*radius;
			const float sphereDensity=0.001f*targetMass/sphereVolume;
			const Vector3 doorCentre=(positions[0]+positions[1])*0.5f;
			const Vector3 spherePosition=doorCentre+Vector3(0,0,5.0f+radius*20.0f*i);
			phDemoObject* sphere = world.CreateSphere(radius,sphereDensity,spherePosition);
			sphere->SetInitialVelocity(Vector3(0,0,-25));
			sphere->Reset(PHLEVEL,PHSIM);
		}

		for(i=0;i<16;i++)
		{
			const float radius=0.5f;
			const float targetMass=1000;
			const float sphereVolume=1.333f*PI*radius*radius*radius;
			const float sphereDensity=0.001f*targetMass/sphereVolume;			
			const Vector3 doorCentre=(positions[2]+positions[3])*0.5f;
			const Vector3 spherePosition=doorCentre+Vector3(0,0,-5.0f-radius*20.0f*i);
			phDemoObject* sphere = world.CreateSphere(radius,sphereDensity,spherePosition);
			sphere->SetInitialVelocity(Vector3(0,0,25));
			sphere->Reset(PHLEVEL,PHSIM);
		}

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &world;
	}


	phDemoWorld* CreateBoxLine ()
	{
		phDemoWorld& lengthWorld = *CreateNewDemoWorld("long constraints");
		Vector3 position(1.5f,1.0f,0.0f);
		Vector3 rotation(ORIGIN);
		bool alignBottom = false;
		Vector3 boxSize(1.0f,1.0f,1.0f);

		phDemoObject* crate1 = lengthWorld.CreateBox( boxSize, 100000.0f,position,alignBottom,rotation);
		position.Set(0.0f,1.1f,0.0f);
		phDemoObject* crate2 = lengthWorld.CreateBox( boxSize, 100000.0f,position,alignBottom,rotation);

		phConstraintDistance::Params params;
		params.minDistance = 0.5f; // New feature of phConstraintDistance: min distance

		params.instanceA = crate1->GetPhysInst();
		params.instanceB = crate2->GetPhysInst();
		params.worldAnchorA = Vec3V(1.0f, 1.5f, 0.0f);
		params.worldAnchorB = Vec3V(0.5f, 1.6f, 0.0f);
		PHCONSTRAINT->Insert(params);

		position.Set(-1.5f,1.2f,0.0f);
		phDemoObject* crate3 = lengthWorld.CreateBox( boxSize, 100000.0f,position,alignBottom,rotation);

		params.instanceA = crate2->GetPhysInst();
		params.instanceB = crate3->GetPhysInst();
		params.worldAnchorA = Vec3V(-0.5f, 1.6f, 0.0f);
		params.worldAnchorB = Vec3V(-1.0f, 1.7f, 0.0f);
		PHCONSTRAINT->Insert(params);

		position.Set(-3.0f,1.3f,0.0f);
		phDemoObject* crate4 = lengthWorld.CreateBox( boxSize, 100000.0f,position,alignBottom,rotation);

		params.instanceA = crate3->GetPhysInst();
		params.instanceB = crate4->GetPhysInst();
		params.worldAnchorA = Vec3V(-2.0f, 1.7f, 0.0f);
		params.worldAnchorB = Vec3V(-2.5f, 1.8f, 0.0f);
		PHCONSTRAINT->Insert(params);

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &lengthWorld;
	}

	phDemoWorld* CreateHangingSigns ()
	{
		phDemoObject* object; object = NULL;
		phDemoWorld& signWorld = *CreateNewDemoWorld("signs");
		Vector3 position(-2.0f,2.0f,0.0f);
		Vector3 rotation(ORIGIN);
		bool alignBottom = false;

#if 1
		// sign1 is attached to the world with two spherical constraints
		phInst* sign1 = signWorld.CreateObject("box_flat",position,alignBottom,rotation)->GetPhysInst();
		Vector3 worldAttachPoint(position);
		worldAttachPoint.x -= 0.4f;
		// This attaches sign1 to the world at one point.
		phConstraintSpherical::Params spherical;
		spherical.worldPosition = RCC_VEC3V(worldAttachPoint);
		spherical.instanceA = sign1;
		PHCONSTRAINT->Insert(spherical);
		worldAttachPoint.z -= 0.4f;
		spherical.worldPosition = RCC_VEC3V(worldAttachPoint);
		// This attaches sign1 to the world at another point, so that it can rotate only about one axis.
		PHCONSTRAINT->Insert(spherical);
#endif

#if 1
		// sign2 is attached to the world with a hinge constraint
		position.Set(0.0f,3.0f,2.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		object = signWorld.CreateObject("box_flat",position,alignBottom,rotation);
		phInst* sign2 = object->GetPhysInst();
		position.x += object->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax().GetXf();
		phConstraintHinge::Params hinge1;
		hinge1.worldAxis = Vec3V(V_Z_AXIS_WZERO);
		hinge1.worldAnchor = RCC_VEC3V(position);
		hinge1.instanceA = sign2;
		PHCONSTRAINT->Insert(hinge1);
#endif

#if 1
		// sign3 is attached to the world with a hinge constraint with limits
		position.Set(-2.0f,3.0f,2.0f);
		rotation.Set(0.0f*PI,0.0f,0.0f);
		object = signWorld.CreateObject("box_flat",position,alignBottom,rotation);
		Vector3 halfWidth(VEC3V_TO_VECTOR3(object->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax()));
		position.x += halfWidth.x;
		position.z += halfWidth.z;
		phConstraintHinge::Params hinge2;
		hinge2.worldAxis = -Vec3V(V_Z_AXIS_WZERO);
		hinge2.worldAnchor = RCC_VEC3V(position);
		hinge2.minLimit = 0.0f;
		hinge2.maxLimit = PI;
		hinge2.instanceA = object->GetPhysInst();
		PHCONSTRAINT->Insert(hinge2);
		position.z -= 2.0f*halfWidth.z;
#endif

#if 1
		// This sphere rests on sign3
		position.x -= halfWidth.x;
		position.y += 0.66f;
		position.z += halfWidth.z;
		signWorld.CreateObject("sphere_020",position);
#endif

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &signWorld;
	}

	phDemoWorld* CreateNewtonsCradle ()
	{
		phDemoWorld& newtonWorld = *CreateNewDemoWorld("newton");
		Vector3 worldAttachPoint(0.0f,2.0f,0.0f);
		Vector3 position(-0.7071f,2.0f-0.7071f,0.0f);
		phInst* sphere = newtonWorld.CreateObject("sphere_007",position)->GetPhysInst();

		phConstraintDistance::Params constraint;
		constraint.instanceA = sphere;
		constraint.worldAnchorA = sphere->GetPosition();
		constraint.worldAnchorB = RCC_VEC3V(worldAttachPoint);
		PHCONSTRAINT->Insert(constraint);
		
		worldAttachPoint.Set(0.12f,2.0f,0.0f);
		position.Set(0.12f,1.0f,0.0f);
		sphere = newtonWorld.CreateObject("sphere_007",position)->GetPhysInst();

		constraint.instanceA = sphere;
		constraint.worldAnchorA = sphere->GetPosition();
		constraint.worldAnchorB = RCC_VEC3V(worldAttachPoint);
		PHCONSTRAINT->Insert(constraint);

		worldAttachPoint.Set(0.24f,2.0f,0.0f);
		position.Set(0.24f,1.0f,0.0f);
		sphere = newtonWorld.CreateObject("sphere_007",position)->GetPhysInst();

		constraint.instanceA = sphere;
		constraint.worldAnchorA = sphere->GetPosition();
		constraint.worldAnchorB = RCC_VEC3V(worldAttachPoint);
		PHCONSTRAINT->Insert(constraint);

		worldAttachPoint.Set(0.36f,2.0f,0.0f);
		position.Set(0.36f,1.0f,0.0f);
		sphere = newtonWorld.CreateObject("sphere_007",position)->GetPhysInst();

		constraint.instanceA = sphere;
		constraint.worldAnchorA = sphere->GetPosition();
		constraint.worldAnchorB = RCC_VEC3V(worldAttachPoint);
		PHCONSTRAINT->Insert(constraint);

		worldAttachPoint.Set(0.48f,2.0f,0.0f);
		position.Set(0.48f,1.0f,0.0f);
		sphere = newtonWorld.CreateObject("sphere_007",position)->GetPhysInst();

		constraint.instanceA = sphere;
		constraint.worldAnchorA = sphere->GetPosition();
		constraint.worldAnchorB = RCC_VEC3V(worldAttachPoint);
		PHCONSTRAINT->Insert(constraint);

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &newtonWorld;
	}

/*	phDemoWorld* CreateTrain ()
	{
		m_NumTrainCars = 0;
		phDemoWorld* trainWorld = CreateNewDemoWorld("train");
		m_TrainTracks.Init();
		float trainTrackT = m_TrainTracks.GetCurveMinT()+0.01f;
		phInst& logCar = CreateTrainCar(*trainWorld,"train_log_car",trainTrackT);
		Matrix34 logMatrix = RCC_MATRIX34(logCar.GetMatrix());
		logMatrix.RotateLocalX(0.5f*PI);
		logMatrix.d.SubtractScaled(logMatrix.c,1.0f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.AddScaled(logMatrix.a,0.4f);
		logMatrix.d.SubtractScaled(logMatrix.c,0.4f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.SubtractScaled(logMatrix.a,0.8f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.AddScaled(logMatrix.a,0.4f);
		//logMatrix.RotateLocalZ(0.04f);
		logMatrix.d.SubtractScaled(logMatrix.c,0.5f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.AddScaled(logMatrix.a,0.4f);
		logMatrix.d.SubtractScaled(logMatrix.c,0.4f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.SubtractScaled(logMatrix.a,0.8f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.AddScaled(logMatrix.a,0.4f);
		//logMatrix.RotateLocalZ(0.04f);
		logMatrix.d.SubtractScaled(logMatrix.c,0.5f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.AddScaled(logMatrix.a,0.4f);
		logMatrix.d.SubtractScaled(logMatrix.c,0.4f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		logMatrix.d.SubtractScaled(logMatrix.a,0.8f);
		trainWorld->CreateObject("capsule_log",logMatrix);
		trainTrackT += 0.03f;
	//	CreateTrainCar(*trainWorld,"train_rock_car",trainTrackT);
	//	float density = 0.2f;
	//	trainWorld->CreateBox(Vector3(2.0f,2.0f,2.0f),density,Vector3(30.0f,0.0f,0.0f));

		m_TrainWorldActive = true;
		m_RotatingConstraintWorldActive = false;

		return trainWorld;
	}*/

#if 0 // Needs converstion to new constraint API
	phDemoWorld* CreateRopeBridge ()
	{
		const int maxOctreeNodes=1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		const Vector3& worldMin=Vector3(-999.0f,-999.0f,-999.0f);
		const Vector3& worldMax=Vector3(999.0f,999.0f,999.0f);
		int maxInstBehaviors=16;
		int scratchpadSize = 5*1024*1024;
		int maxManifolds = 1024;
		int maxExternVelManifolds = 256;
		int numConstraints = 300;

		phDemoWorld& ropeBridgeWorld = *(rage_new phDemoWorld("rope bridge"));
		Vector3 offsetWorldMin(worldMin);
		offsetWorldMin.Add(phDemoObject::GetWorldOffset());
		Vector3 offsetWorldMax(worldMax);
		offsetWorldMax.Add(phDemoObject::GetWorldOffset());
		ropeBridgeWorld.Init(maxOctreeNodes, maxActiveObjects, maxObjects, Vec3V(offsetWorldMin.xyzw), Vec3V(offsetWorldMax.xyzw), maxInstBehaviors, scratchpadSize, maxManifolds, maxExternVelManifolds, numConstraints);
		ropeBridgeWorld.InitUpdateObjects(256);

		const int numBridgeParts = 33;
		Vector3 boxSize(1.2f, 0.4f, 3.0f);
		Vector3 position(numBridgeParts * boxSize.x * -0.5f,4.0f,-4.0f);
		phInst* ropeBridgeParts[numBridgeParts];
		ropeBridgeParts[0] = ropeBridgeWorld.CreateBox(boxSize,1.0f,position)->GetPhysInst();
		phArchetypeDamp* archetype = static_cast<phArchetypeDamp*>(ropeBridgeParts[0]->GetArchetype());

		// Don't collide bridge parts with each other
		archetype->RemoveTypeFlags(1);
		archetype->AddTypeFlags(2);
		archetype->RemoveIncludeFlags(2);

		phContactMgr::SetSeparationBias(0.8f);
		phConstraintMgr* constraintMgr = ropeBridgeWorld.GetSimulator()->GetConstraintMgr();
		int componentLeft = 0;
		int componentRight = 0;

#define WITH_ROTATION_CONSTRAINT 0

		float hardLimitSeparationBias = 0.0f;
#if WITH_ROTATION_CONSTRAINT
		Vector3 worldAttachPoint(position);
		worldAttachPoint.x -= boxSize.x / 2.0f;
		{
			phConstraint* translationConstraint = constraintMgr->AttachObjectToWorld('ScB0', worldAttachPoint,ropeBridgeParts[0],componentLeft);
			translationConstraint->SetUsePushes(false, hardLimitSeparationBias);
			translationConstraint->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
		}
		{
			phConstraint* rotationConstraint = constraintMgr->AttachObjectToWorldRotation('ScB1', *ropeBridgeParts[0],componentRight);
			rotationConstraint->SetUsePushes(false);
			rotationConstraint->SetDegreesConstrained(1, Vector3(1.0f, 0.0f, 0.0f));
		}
#else
		float slackRemovalScalar = 0.2f;
		Vector3 slackRemoval(slackRemovalScalar, 0.0f, 0.0f);

		Vector3 worldAttachPoint1(position);
		Vector3 worldAttachPoint2(position);
		worldAttachPoint1.x -= boxSize.x / 2.0f;
		worldAttachPoint1.z += boxSize.z / 2.0f;
		worldAttachPoint2.x -= boxSize.x / 2.0f;
		worldAttachPoint2.z -= boxSize.z / 2.0f;
		phConstraint* constraintLeft = constraintMgr->AttachObjectToWorld('ScB2', worldAttachPoint1 - slackRemoval,worldAttachPoint1 + slackRemoval,ropeBridgeParts[0],componentLeft);
		constraintLeft->SetUsePushes(false, hardLimitSeparationBias);
		constraintLeft->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
		phConstraint* constraintRight = constraintMgr->AttachObjectToWorld('ScB3', worldAttachPoint2 - slackRemoval, worldAttachPoint2 + slackRemoval,ropeBridgeParts[0],componentLeft,0.0f,false);
		constraintRight->SetUsePushes(false, hardLimitSeparationBias);
		constraintRight->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
#endif

		for (int partIndex=1; partIndex<numBridgeParts; partIndex++)
		{
			position.x += boxSize.x;
			ropeBridgeParts[partIndex] = ropeBridgeWorld.CreateBox(boxSize,1.0f,position)->GetPhysInst();

#if WITH_ROTATION_CONSTRAINT
			worldAttachPoint.x += boxSize.x;
			phConstraint* translationConstraint = constraintMgr->AttachObjects('ScB4', worldAttachPoint,ropeBridgeParts[partIndex-1],ropeBridgeParts[partIndex],componentRight,componentLeft);
			translationConstraint->SetUsePushes(false, hardLimitSeparationBias);
			translationConstraint->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);

			phConstraint* rotationConstraint = constraintMgr->AttachObjectsRotation('ScB5', *ropeBridgeParts[partIndex-1],*ropeBridgeParts[partIndex],componentRight,componentLeft);
			rotationConstraint->SetUsePushes(false);
			rotationConstraint->SetDegreesConstrained(1, Vector3(1.0f, 0.0f, 0.0f));
#else
			worldAttachPoint1.x += boxSize.x;
			worldAttachPoint2.x += boxSize.x;
			constraintLeft = constraintMgr->AttachObjects('ScB6', worldAttachPoint1 - slackRemoval,worldAttachPoint1 + slackRemoval,ropeBridgeParts[partIndex-1],ropeBridgeParts[partIndex],componentRight,componentLeft,0.0f,true);
			constraintLeft->SetUsePushes(false, hardLimitSeparationBias);
			constraintLeft->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
			constraintRight = constraintMgr->AttachObjects('ScB7', worldAttachPoint2 - slackRemoval,worldAttachPoint2 + slackRemoval,ropeBridgeParts[partIndex-1],ropeBridgeParts[partIndex],componentRight,componentLeft,0.0f,true);
			constraintRight->SetUsePushes(false, hardLimitSeparationBias);
			constraintRight->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
#endif
		}

#if WITH_ROTATION_CONSTRAINT
		worldAttachPoint.x += boxSize.x;

		{
			phConstraint* translationConstraint = constraintMgr->AttachObjectToWorld('ScB8', worldAttachPoint,ropeBridgeParts[numBridgeParts-1],componentRight);
			translationConstraint->SetUsePushes(false, hardLimitSeparationBias);
			translationConstraint->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
		}

		{
			phConstraint* rotationConstraint = constraintMgr->AttachObjectToWorldRotation('ScB9', *ropeBridgeParts[numBridgeParts-1],componentRight);
			rotationConstraint->SetUsePushes(false);
			rotationConstraint->SetDegreesConstrained(1, Vector3(1.0f, 0.0f, 0.0f));
		}
#else
		worldAttachPoint1.x += boxSize.x;
		worldAttachPoint2.x += boxSize.x;
		constraintLeft = constraintMgr->AttachObjectToWorld('ScBA', worldAttachPoint1 - slackRemoval,worldAttachPoint1 + slackRemoval,ropeBridgeParts[numBridgeParts-1],componentRight,0.0f,false);
		constraintLeft->SetUsePushes(false, hardLimitSeparationBias);
		constraintLeft->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
		constraintRight = constraintMgr->AttachObjectToWorld('ScBB', worldAttachPoint2 - slackRemoval,worldAttachPoint2 + slackRemoval,ropeBridgeParts[numBridgeParts-1],componentRight,0.0f,false);
		constraintRight->SetUsePushes(false, hardLimitSeparationBias);
		constraintRight->AddSoftTranslation(0.4f, 0.2f, 0.0f, true);
#endif

		// Increase damping
		float massiveDamping = 0.2f;
		float lotsOfDamping = 0.1f;
		archetype->ActivateDamping(phArchetypeDamp::LINEAR_C, Vector3(massiveDamping, massiveDamping, massiveDamping));
		archetype->ActivateDamping(phArchetypeDamp::LINEAR_V, Vector3(massiveDamping, massiveDamping, massiveDamping));
		archetype->ActivateDamping(phArchetypeDamp::LINEAR_V2, Vector3(lotsOfDamping, lotsOfDamping, lotsOfDamping));
		archetype->ActivateDamping(phArchetypeDamp::ANGULAR_C, Vector3(massiveDamping, massiveDamping, massiveDamping));
		archetype->ActivateDamping(phArchetypeDamp::ANGULAR_V, Vector3(massiveDamping, massiveDamping, massiveDamping));
		archetype->ActivateDamping(phArchetypeDamp::ANGULAR_V2, Vector3(lotsOfDamping, lotsOfDamping, lotsOfDamping));

		ropeBridgeWorld.ConstructPyramid(Vector3(0.0f, 4.0f, -4.0f), 4, true, "cube_060");

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &ropeBridgeWorld;
	}

	phDemoWorld* CreateBoxForces ()
	{
		phDemoWorld& forceBoxWorld = *CreateNewDemoWorld("box_forces");
		Vector3 position(0.0f,0.0f,0.0f);
		Vector3 rotation(0.0f,0.0f,0.0f);
		int numStairs = 3;
		float stairHeight = 1.0f;
		float stairDepth = 0.0f;
		forceBoxWorld.ConstructBoxStairs("longer_crate",numStairs,position,rotation,stairHeight,stairDepth);
		position.Set(0.0f,3.5f,0.8f);
		phDemoObject* crate1 = forceBoxWorld.CreateObject("crate",position);
		position.Set(0.0f,3.5f,-0.4f);
		phDemoObject* crate2 = forceBoxWorld.CreateObject("crate",position);
		phConstraintMgr* constraintMgr = forceBoxWorld.GetSimulator()->GetConstraintMgr();
		position.Set(0.0f,4.5f,0.2f);
		phConstraint* constraint = constraintMgr->AttachObjects('ScF0', position,crate1->GetPhysInst(),crate2->GetPhysInst());
		float forceScale = 1800.0f;
		float forceDamping = 400.0f;
		constraint->SetSoftTranslation(forceScale,forceDamping);
		constraint = constraintMgr->AttachObjects('ScF1', position,crate1->GetPhysInst(),crate2->GetPhysInst());
		float torqueScale = 100.0f;
		float torqueDamping = 40.0f;
		constraint->SetSoftRotation(torqueScale,torqueDamping);

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &forceBoxWorld;
	}

	phDemoWorld* CreateBigPushes ()
	{
		phDemoWorld& pushWorld = *CreateNewDemoWorld("push");
	//	pushWorld.Pause();
		phConstraintMgr* constraintMgr = pushWorld.GetSimulator()->GetConstraintMgr();
		Vector3 position(-2.0f,1.0f,-10.0f);
		phInst* box = pushWorld.CreateObject("crate",position)->GetPhysInst();
		Vector3 worldAttachPoint(-2.0f,4.0f,0.0f);
		constraintMgr->AttachObjectToWorld('ScP0', worldAttachPoint,position,box,0,0.0f,false);
		position.Set(-2.0f,2.0f,0.0f);
		Vector3 rotation(0.0f,0.0f,0.5f*PI);
		box = pushWorld.CreateObject("crate",position,false,rotation)->GetPhysInst();
		constraintMgr->AttachObjectToWorld('ScP1', position,box);
		phConstraint* constraint = constraintMgr->AttachObjectToWorldRotation('ScP2', *box);
		constraint->SetRelativeOrientation(ORIGIN);

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &pushWorld;
	}

	phDemoWorld* CreateBreakingConstraints ()
	{
		phDemoWorld& breakingWorld1 = *CreateNewDemoWorld("BreakingConstraints1");
		phConstraintMgr* constraintMgr = breakingWorld1.GetSimulator()->GetConstraintMgr();
		Vector3 boxPosition, worldPosition;
		for (int boxIndex = 0; boxIndex < 5; ++boxIndex)
		{
			boxPosition.Set(-5.0f + (float)(boxIndex) * 3.0f,4.0f,-5.0f);
			worldPosition.Set(boxPosition);
			worldPosition.x += 0.1f;
			phInst* box = breakingWorld1.CreateObject("crate",boxPosition, false)->GetPhysInst();
			phConstraint* constraint = constraintMgr->AttachObjectToWorld('Scb0', worldPosition, boxPosition, box, 0, 2.0f, false);
			constraint->SetBreakingStrength(-1.0f + (float)(boxIndex) * 700.0f);
		}

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &breakingWorld1;
	}

	phDemoWorld* CreateSpinningConstraint ()
	{
		phDemoWorld* rotatingConstraintWorld = CreateNewDemoWorld("Test Rotation Constraint");
		Vector3 position(0.0f,3.0f,0.0f);
		Vector3 rotation(ORIGIN);
		phSimulator* simulator = rotatingConstraintWorld->GetSimulator();
		phInst* stick = rotatingConstraintWorld->CreateObject("capsule_small",position,false,rotation)->GetPhysInst();
		m_FlatBox = rotatingConstraintWorld->CreateFixedObject("box_flat",position)->GetPhysInst();
		phConstraintMgr* constraintMgr = simulator->GetConstraintMgr();
		constraintMgr->AttachObjectToWorld('Scs0', position,stick);
		m_RotatingConstraint = constraintMgr->AttachObjectToWorldRotation('Scs1', *stick);
		stick->GetArchetype()->SetTypeFlags(4);
		m_FlatBox->GetArchetype()->AddIncludeFlags(4);
		m_FlatBoxAngVelocity.Set(5.0f,-5.0f,2.0f);
		m_FlatBoxOrientation.Identity();

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = true;

		return rotatingConstraintWorld;
	}

	phDemoWorld* CreateDraftHorse ()
	{
		phConstraint* constraint = NULL;
		phDemoWorld& world = *CreateNewDemoWorld("horse");
		phConstraintMgr* constraintMgr = world.GetSimulator()->GetConstraintMgr();
		Vector3 position(-1.0f,1.0f,0.0f);
		Vector3 rotation(0.0f*PI,0.0f*PI,0.5f*PI);
		bool alignBottom = false;
		phInst* horse = world.CreateObject("capsule_fat",position,alignBottom,rotation)->GetPhysInst();
		Vector3 attachPoint(position);
		position.x += 4.5f;
		position.z -= 1.0f;
		rotation.Set(0.0f,0.5f*PI,0.0f);
		phInst* wagon = world.CreateObject("box_long",position,alignBottom,rotation)->GetPhysInst();
		attachPoint.x += 1.0f;
		attachPoint.z -= 1.0f;

		constraint = constraintMgr->AttachObjects('ScD0', attachPoint,*horse,*wagon);
		constraint->SetConstraintLimitType(phConstraint::LIMIT_TYPE_AXES);
		Vec3V limitMax = AddScaled(VECTOR3_TO_VEC3V(attachPoint),Vec3V(V_X_AXIS_WZERO),ScalarV(V_HALF));
		Vec3V limitMin = SubtractScaled(VECTOR3_TO_VEC3V(attachPoint),Vec3V(V_X_AXIS_WZERO),ScalarV(V_HALF));
		constraint->SetHardLimitMax(limitMax);
		constraint->SetHardLimitMin(limitMin);

	//	const float forceScale = 600.0f;
	//	const float forceDamping = 60.0f;
	//	constraint->AddSoftTranslation(forceScale,forceDamping);
	//	limitMax = Scale(limitMax,ScalarV(V_HALF));
	//	limitMin = Negate(limitMax);
	//	constraint->SetSoftLimitMax(limitMax);
	//	constraint->SetSoftLimitMin(limitMin);

		position.Set(3.0f,4.0f,0.0f);
		phInst& box1 = *world.CreateObject("crate",position)->GetPhysInst();
		position.Set(-3.0f,4.0f,0.0f);
		phInst& box2 = *world.CreateObject("crate",position)->GetPhysInst();
		attachPoint.Set(0.0f,4.0f,0.0f);
		constraint = constraintMgr->AttachObjects('ScD1', attachPoint,box1,box2);
		constraint->SetMassInvScales(1.0f,0.0f);

		Vector3 boxSize(1.0f,0.25f,0.1f);
		position.Set(0.0f,2.0f,0.0f);
		phInst* box = world.CreateBox(boxSize,1.0f,position)->GetPhysInst();
		constraint = constraintMgr->AttachObjectToWorld('ScD2', box);
		constraint->SetHardRotation();
		constraint = constraintMgr->AttachObjectToWorld('ScD3', box);
		constraint->SetConstraintLimitType(phConstraint::LIMIT_TYPE_AXES);
		constraint->SetHardLimitMax(Vector3(position.x+2.0f,position.y,position.z));
		constraint->SetHardLimitMin(Vector3(position.x-2.0f,position.y,position.z));

		m_TrainWorldActive = false;
		m_RotatingConstraintWorldActive = false;

		return &world;
	}
#endif

	phDemoWorld* CreateHangingChainWorld ()
	{
		phDemoWorld& chainWorld = *CreateNewDemoWorld("chain",NULL,Vector3(0.0f, -100.0f, 0.0f),3000,3000,6000,Vector3(-999.0f, -999.0f, -999.0f), Vector3(999.0f, 999.0f, 999.0f), 16, 10*1024*1024, 10240, 256);
		Vector3 rotation(0.0f,0.0f,0.0f);

		const float xStart=0;
		const float yStart=4;
		const float zStart=0;
		Vector3 rot(0.0f,0.0f,1.57f);

		const float fRadius=0.125f;
		int i;
		for(i=0;i<1;i++)
		{
			int j;
			phDemoObject* pOldObject=NULL;
			for(j=0;j<8;j++)
			{
				Vector3 pos(xStart+j*fRadius*2,yStart,zStart+i*1.0f);
				Vector3 vAttachPos(pos);
				vAttachPos.x-=fRadius;
				phDemoObject* pNewObject=chainWorld.CreateSphere(0.95f*fRadius,1.0f,pos);
				if(NULL==pOldObject)
				{
					phConstraintSpherical::Params constraint;
					constraint.instanceA = pNewObject->GetPhysInst();
					constraint.worldPosition = RCC_VEC3V(vAttachPos);
					PHCONSTRAINT->Insert(constraint);
				}
				else
				{
					phConstraintSpherical::Params constraint;
					constraint.instanceA = pNewObject->GetPhysInst();
					constraint.instanceB = pOldObject->GetPhysInst();
					constraint.worldPosition = RCC_VEC3V(vAttachPos);
					PHCONSTRAINT->Insert(constraint);
				}
				pOldObject=pNewObject;
			}

			phConstraintHalfSpace::Params halfSpaceConstraint;
			halfSpaceConstraint.instanceA = pOldObject->GetPhysInst();
			halfSpaceConstraint.localPosA = Vec3V(fRadius, 0.0f, 0.0f);
			halfSpaceConstraint.localPosB = Vec3V(0.0f, 3.5f, 0.0f);
			halfSpaceConstraint.worldNormal = Vec3V(V_Y_AXIS_WZERO);
			halfSpaceConstraint.constructUsingLocalPositions = true;
			PHCONSTRAINT->Insert(halfSpaceConstraint);
		}

		return &chainWorld;
	}

	virtual void InitClient ()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();


		// STEP #2. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.
		Vector3 position,rotation,worldAttachPoint,worldAttachPointA,worldAttachPointB;
		m_SwingWorldActive = false;
		m_TrainWorldActive = false;
		m_TrainWorldInactive = true;
		m_RotatingConstraintWorldActive = false;
//		m_RotatingConstraint = NULL;

		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateBoxLine));
		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 0: A chain of spheres hanging from a fixed point
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateHangingChainWorld));
		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 1: pairs of swing doors at 1kg mass with only 1 rotational dof about the y-axis axis which are then 
		//hit by a train of 1000kg spheres.
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateConstrainedDoors));

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 2: a 1kg capsule connected to the world with a rotation constraint about each axis which is then hit at one end
		// by a 1000kg sphere falling from a decent height.
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateRotationConstrainedCapsuleUnderHeavyContact));

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 3: a 1kg sphere connected to the world with a translation constraint which is then hit 
		// by a number of  1000kg spheres falling from a great height.
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateTranslationConstrainedSphereUnderHeavyContact));

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 4: a 1kg sphere connected to the world with a translation constraint offset from the centre along
		//the x-axis by the vector (radius,0,0) and rotation constraints about
		//the y and z axis which is then hit by a number of 10kg spheres falling from a great height.
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateTranslationAndRotationConstrainedSphereUnderHeavyContact));

		//m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateConstrainedBox));

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 5: a row of physically active crates connected by fixed point constraints with length,
		// which allow points on neighboring crates to separate up to a certain distance

		////////////////////////
		// Page 6: hanging signs
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateHangingSigns));

		//////////////////////////////////////////////////////
		// Page 7: Newton's cradle (colliding hanging spheres)
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateNewtonsCradle));

		//////////////////////////////////////////////////////////////
		// Page X: train cars constrained to move along a spline curve
	//	m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateTrain));

#if 0 // Needs converstion to new constraint API
		/////////////////////////////////////////////////////////////////////////
		// Page 8: a single-line rope bridge held together with point constraints
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateRopeBridge));

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Page 9: boxes with loose force and torque constraints that keep the boxes near each other and with similar orientations
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateBoxForces));

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// Page 10: a set of constraints that make large pushes and turns on the first frame, designed to test
		//			how well physical constraints can handle attachment to non-physical objects like animating
		//			characters (when the constrained push or turn exceeds a limit, the rest is applied with no
		//			physical response)
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateBigPushes));

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// Page 11: A set of boxes and constraints with varying breaking strengths to test breaking constraints
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateBreakingConstraints));

		///////////////////////////////////////////////////////
		// Page 12: A stick with a spinning rotation constraint
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateSpinningConstraint));

		////////////////////////
		// Page 13: draft horse
		m_Demos.AddDemo(phDemoMultiWorld::Factory(this, &ConstraintManager::CreateDraftHorse));
#endif


		// STEP #3. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		int startingDemo = 0;
		PARAM_world.Get(startingDemo);
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();
		m_SavedCameraOffset.Zero();
	}

	// PURPOSE: Get the current world matrix for a train car that is required to remain positioned and oriented on the tracks.
	// PARAMS:
	//	position -	the world position of the train car along the train tracks
	//	tangent -	the unit vector along the direction of the train tracks at the given position
	//	matrix -	reference in which to write the world matrix for a train car to be positioned and oriented along the tracks
	//				at the given position with the given tangent
	void GetTrainCarMatrix (const Vector3& position, const Vector3& tangent, Matrix34& matrix)
	{
		// Make the local x direction up crossed with the tangent.
		matrix.a.Set(tangent.z,0.0f,-tangent.x);
		matrix.a.Normalize();

		// Make the local y the tangent crossed with the local x direction.
		matrix.b.Cross(tangent,matrix.a);

		// Make the local z direction the tangent.
		matrix.c.Set(tangent);

		// Set the matrix position.
		matrix.d.Set(position);
	}
	
	// PURPOSE: Create a train car at the specified position along the train track.
	// PARAMS:
	//	trainTrackT -	the fraction of the distance along the tracks to place the train car
	// RETURN: a reference to the train car physics instance
/*	phInst& CreateTrainCar (phDemoWorld& trainWorld, const char* trainCarName, float trainTrackT)
	{
		Vector3 position,tangent;
		m_TrainTracks.GetCurve()->SolveSegment(position,0,trainTrackT,0.0f,&tangent);
		Matrix34 matrix;
		GetTrainCarMatrix(position,tangent,matrix);
		m_TrainCars[m_NumTrainCars] = trainWorld.CreateConstrainedObject(trainCarName,matrix);
		phInst& instance = *m_TrainCars[m_NumTrainCars]->GetPhysInst();
		phCollider* collider = PHSIM->GetCollider(&instance);
		Assert(collider && collider->IsConstrained());
		phConstrainedCollider* constrainedCollider = static_cast<phConstrainedCollider*>(collider);
		constrainedCollider->ConstrainRotationWorld(0);
		m_TrainCarT[m_NumTrainCars] = trainTrackT;
		m_LastCurveSlope[m_NumTrainCars] = 0.0f;
		m_LastCurveTangent[m_NumTrainCars].Zero();
		m_InitialTrainCarT[m_NumTrainCars] = trainTrackT;
		m_TrainCarInitialMatrix[m_NumTrainCars].Set(matrix);
		m_LastTrainCarPosition[m_NumTrainCars].Set(matrix.d);
		m_NumTrainCars++;
		return instance;
	}*/

	virtual void InitCamera ()
	{
		Vector3 lookFrom(-1.0f,3.5f,6.0f);
		Vector3 lookTo(0.0f,1.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Reset ()
	{
/*		if (m_TrainWorldActive)
		{
			for (int carIndex=0; carIndex<m_NumTrainCars; carIndex++)
			{
				Assert(m_TrainCars[carIndex] && m_TrainCars[carIndex]->GetPhysInst());
				phInst& instance = *m_TrainCars[carIndex]->GetPhysInst();
				instance.SetMatrix(RCC_MAT34V(m_TrainCarInitialMatrix[carIndex]));
				m_LastTrainCarPosition[carIndex].Set(m_TrainCarInitialMatrix[carIndex].d);
				PHSIM->GetCollider(instance.GetLevelIndex())->Reset();
				m_TrainCarT[carIndex] = m_InitialTrainCarT[carIndex];
				m_LastCurveSlope[carIndex] = 0.0f;
				m_LastCurveTangent[carIndex].Zero();
			}
		}*/
	}

	virtual void UpdateClient ()
	{
		if (m_Demos.GetCurrentWorld()->ShouldUpdate())
		{
			// See if the current active demo world is the train track world, and update the constrained train cars along the track if it is.
	/*		if (m_TrainWorldActive)
			{
				if (m_TrainWorldInactive)
				{
					// Set the camera to the polar camera to follow the train.
					m_CamMgr.SetCamera(dcamCamMgr::CAM_POLAR);
					m_SavedCameraOffset.Set(static_cast<dcamPolarCam*>(&m_CamMgr.GetCamera(dcamCamMgr::CAM_POLAR))->GetOffset());
					m_TrainWorldActive = true;
					m_TrainWorldInactive = false;
				}

				// Set the camera interest to one of the train cars.
				static_cast<dcamPolarCam*>(&m_CamMgr.GetCamera(dcamCamMgr::CAM_POLAR))->SetOffset( VEC3V_TO_VECTOR3(m_TrainCars[0]->GetPhysInst()->GetPosition()) );

				// Constrain the train cars to the tracks.
				cvCurve<Vector3>* curve = m_TrainTracks.GetCurve();
				Matrix34 matrix;
				Vector3 position,displacement;
				for (int carIndex=0; carIndex<m_NumTrainCars; carIndex++)
				{
					phInst* instance = m_TrainCars[carIndex]->GetPhysInst();
					phCollider* collider = PHSIM->GetCollider(instance);
					if (collider)
					{
						// Check to see if we have a valid slope and tangent from last frame.
						if(m_LastCurveSlope[carIndex] == 0.0f)
						{
							m_LastCurveSlope[carIndex] = curve->SolveSegment(position,0,m_TrainCarT[carIndex],0.0f,&m_LastCurveTangent[carIndex]);
						}

						// See if we've moved enough.
						displacement.Subtract(VEC3V_TO_VECTOR3(instance->GetPosition()),m_LastTrainCarPosition[carIndex]);
						float dispMag2 = displacement.Mag2();
						if (dispMag2>VERY_SMALL_FLOAT)
						{
							// Find the change in t-value along the curve that this displacement results in.
							float deltaT = sqrtf(dispMag2)/m_LastCurveSlope[carIndex];

							// Determine if this was a forward or backward displacement (relative to the direction of increasing t-value).
							if (displacement.Dot(m_LastCurveTangent[carIndex])<0.0f)
							{
								deltaT = -deltaT;
							}

							// Change the t-value along the curve and set the new position on the curve.
							m_TrainCarT[carIndex] += deltaT;
							bool hitEndOfTracks = false;
							if (m_TrainCarT[carIndex]<m_TrainTracks.GetCurveMinT())
							{
								// Reflect the t-value about the minimum, bouncing will be handled below.
								const float limitT = m_TrainTracks.GetCurveMinT();
								m_TrainCarT[carIndex] = 2.0f*limitT-m_TrainCarT[carIndex];
								hitEndOfTracks = true;
							}
							else if (m_TrainCarT[carIndex]>m_TrainTracks.GetCurveMaxT())
							{
								// Reflect the t-value about the maximum, bouncing will be handled below.
								const float limitT = m_TrainTracks.GetCurveMaxT();
								m_TrainCarT[carIndex] = 2.0f*limitT-m_TrainCarT[carIndex];
								hitEndOfTracks = true;
							}

							// Handle bouncing if we hit one of the ends of the tracks.
							if (hitEndOfTracks)
							{
								Vector3 velocity(collider->GetVelocity());
								velocity.Negate();
								collider->SetVelocity(velocity);
							}

							// Using the new t-value calculated above, find out where on the track that puts us.
							m_LastCurveSlope[carIndex] = curve->SolveSegment(position,0,m_TrainCarT[carIndex],0.0f,&m_LastCurveTangent[carIndex]);

							// Update the translational constraint to only move in the direction of our current tangent.
							Assert(collider->IsConstrained());
							phConstrainedCollider* constrainedCollider = static_cast<phConstrainedCollider*>(collider);
							constrainedCollider->ConstrainTranslationWorld(1,m_LastCurveTangent[carIndex]);

							// Using the current position and tangent, update the matrix of the car.
							GetTrainCarMatrix(position,m_LastCurveTangent[carIndex],matrix);
							instance->SetMatrix( RCC_MAT34V(matrix) );
							collider->SetColliderMatrixFromInstance();

							// Adjust the collider's velocity to stay on the curve without losing speed (in other words, redirect the velocity
							//   to be tangent to the curve).
							float speed2 = collider->GetVelocity().Mag2();
							if (speed2>VERY_SMALL_FLOAT)
							{
								Vector3 velocity(m_LastCurveTangent[carIndex]);
								float speed = ((deltaT>0.0f)!=hitEndOfTracks ? sqrtf(speed2) : -sqrtf(speed2));
								velocity.Scale(speed);
								collider->SetVelocity(velocity);
							}

							// Set the previous position.
							m_LastTrainCarPosition[carIndex] = RCC_VECTOR3(instance->GetPosition());

							// Tell the physics level the train car was moved.
							PHLEVEL->UpdateObjectLocation(instance->GetLevelIndex());
						}
					}
				}
			}
			else if (!m_TrainWorldInactive)
			{
				// Leaving the train world, so set the camera to maya.
				static_cast<dcamPolarCam*>(&m_CamMgr.GetCamera(dcamCamMgr::CAM_POLAR))->SetOffset(m_SavedCameraOffset);
				static_cast<dcamPolarCam*>(&m_CamMgr.GetCamera(dcamCamMgr::CAM_POLAR))->Update(TIME.GetSeconds());
				m_CamMgr.SetCamera(dcamCamMgr::CAM_MAYA);
				m_TrainWorldInactive = true;
			}*/

			if (m_SwingWorldActive)
			{
				// The swinging human world is active, so see if the swing constraint should be turned on or off.
				Assert(m_SwingingHuman);
				if (m_SwingingHuman->GetPhysInst()->GetPosition().GetYf()<4.0f)
				{
					if (!m_SwingConstraint.IsValid())
					{
						Vector3 swingPivot(0.0f,4.0f,-4.0f);
						int component = 10;
						Vector3 bodyPartPosition;
						Assert(m_SwingingHuman->GetPhysInst());
						phInst& swingingInstance = *m_SwingingHuman->GetPhysInst();
						Assert(swingingInstance.GetArchetype() && swingingInstance.GetArchetype()->GetBound());
						phBound& humanBound = *swingingInstance.GetArchetype()->GetBound();
						Assert(humanBound.GetType()==phBound::COMPOSITE);
						phBoundComposite& compositeBound = *static_cast<phBoundComposite*>(&humanBound);
						RCC_MATRIX34(swingingInstance.GetMatrix()).Transform(RCC_MATRIX34(compositeBound.GetCurrentMatrix(component)).d,bodyPartPosition);
						phConstraintDistance::Params constraintParams;
						constraintParams.instanceA = &swingingInstance;
						constraintParams.componentA = (u16)component;
						constraintParams.worldAnchorA = RCC_VEC3V(swingPivot);
						constraintParams.worldAnchorB = RCC_VEC3V(bodyPartPosition);
						PHCONSTRAINT->Insert(constraintParams);
					}
				}
				else if (m_SwingConstraint.IsValid())
				{
					PHCONSTRAINT->Remove(m_SwingConstraint);
				}
			}

// 			if (m_RotatingConstraintWorldActive && m_RotatingConstraint)
// 			{
// 				Vector3 delAngVel(ORIGIN);
// 				m_FlatBoxAngVelocity.Add(delAngVel);
// 				Vector3 rotation(m_FlatBoxAngVelocity);
// 				rotation.Scale(TIME.GetSeconds());
// 				Quaternion rotationQ;
// 				rotationQ.FromRotation(rotation);
// 				m_FlatBoxOrientation.Multiply(rotationQ);
// 				Matrix34 flatBoxPose;
// 				flatBoxPose.FromQuaternion(m_FlatBoxOrientation);
// 				flatBoxPose.d.Set(RCC_VECTOR3(m_FlatBox->GetPosition()));
// 				m_FlatBox->SetMatrix(RCC_MAT34V(flatBoxPose));
// 				m_RotatingConstraint->SetRelativeOrientation(flatBoxPose);
// 			}
		}

		physicsSampleManager::UpdateClient();
	}

	virtual void DrawClient ()
	{
/*		if (m_TrainWorldActive)
		{
			m_TrainTracks.DrawTracks();
		}*/

		physicsSampleManager::DrawClient();
	}

	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}


protected:
	bool m_SwingWorldActive;
	phConstraintHandle m_SwingConstraint;
	phDemoObject* m_SwingingHuman;

	bool m_RotatingConstraintWorldActive;
//	phConstraint* m_RotatingConstraint;
	phInst* m_FlatBox;
	Quaternion m_FlatBoxOrientation;
	Vector3 m_FlatBoxAngVelocity;

#if ENABLE_UNUSED_CURVE_CODE
	TrainTracks m_TrainTracks;
#endif // ENABLE_UNUSED_CURVE_CODE
	phDemoObject* m_TrainCars[MAX_NUM_TRAIN_CARS];
	Matrix34 m_TrainCarInitialMatrix[MAX_NUM_TRAIN_CARS];
	Vector3 m_LastTrainCarPosition[MAX_NUM_TRAIN_CARS];
	Vector3 m_SavedCameraOffset;
	float m_TrainCarT[MAX_NUM_TRAIN_CARS];
	float m_LastCurveSlope[MAX_NUM_TRAIN_CARS];
	Vector3 m_LastCurveTangent[MAX_NUM_TRAIN_CARS];
	float m_InitialTrainCarT[MAX_NUM_TRAIN_CARS];
	int m_NumTrainCars;
	bool m_TrainWorldActive;
	bool m_TrainWorldInactive;
};

} // namespace ragesamples


int Main()
{
	ragesamples::ConstraintManager sampleConstraint;
	sampleConstraint.Init();
	sampleConstraint.UpdateLoop();
	sampleConstraint.Shutdown();
    return 0;
}

