// 
// sample_cranimation/sample_ikhorse.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: IK Horse
// PURPOSE:
//		This sample shows how to use the IK system make a horse walk
//      over objects and look at stuff.

using namespace rage;

#include "sample_cranimation/sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crbody/ikbody.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crskeleton/skeletondata.h"
#include "data/callback.h"
#include "grcore/im.h"
#include "vector/geometry.h"
#include "input/mouse.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"


#define DEFAULT_ANIM_UPP "rdr2Horse_WalkAnimation"
#define DEFAULT_ANIM_MID "rdr2Horse_WalkAnimation"
#define DEFAULT_ANIM_DWN "rdr2Horse_WalkAnimation"

#define NUM_ANIMATIONS (3)

PARAM(anim_upp,"[sample_ikhorse] Upper animation file to load (default is \"" DEFAULT_ANIM_UPP "\")");
PARAM(anim_mid,"[sample_ikhorse] Middle animation file to load (default is \"" DEFAULT_ANIM_MID "\")");
PARAM(anim_dwn,"[sample_ikhorse] Lower animation file to load (default is \"" DEFAULT_ANIM_DWN "\")");


class craSampleIKHorse : public ragesamples::craSampleManager
{
public:
	craSampleIKHorse() : ragesamples::craSampleManager()
	{
		m_AnimTime = 0.f;
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		crSkeleton& skeleton = GetSkeleton();
		const crSkeletonData& skelData = skeleton.GetSkeletonData();

		const char *animNameUpp = DEFAULT_ANIM_UPP;
		const char *animNameMid = DEFAULT_ANIM_MID;
		const char* animNameDwn = DEFAULT_ANIM_DWN;
		PARAM_anim_upp.Get(animNameUpp);
		PARAM_anim_mid.Get(animNameMid);
		PARAM_anim_dwn.Get(animNameDwn);

		m_Animations[0] = crAnimation::AllocateAndLoad(animNameDwn);
		m_Animations[1] = crAnimation::AllocateAndLoad(animNameMid);
		m_Animations[2] = crAnimation::AllocateAndLoad(animNameUpp);

		m_FrameA = rage_new crFrame;
		m_FrameB = rage_new crFrame;

		if( m_Animations[0] && m_Animations[1] && m_Animations[2] )
		{
			m_FrameA->InitCreateBoneAndMoverDofs(skelData, false);
			m_FrameB->InitCreateBoneAndMoverDofs(skelData, false);
		}

		m_GoalPos.Set(0.1f, 0.7f, -0.55f);
		m_GoalRotInDegs.Set(90.f, 0.f, 0.f);

		// TODO --- if the animation was marked up with events, this could all be automatically extracted!
		m_Height[0] = 0.f; m_Height[1] = 1.1f; m_Height[2] = 2.f;
		m_StartFrame[0] = 12.f; m_EndFrame[0] = 17.f;
		m_StartFrame[1] = 10.f; m_EndFrame[1] = 16.f;
		m_StartFrame[2] = 10.f; m_EndFrame[2] = 16.f;
		m_BlendInFrames = 10.f;
		m_BlendOutFrames = 16.f;

		// STEP #1. Initialize the IK system

		//-- Create the core IK objects

		// Create a crIKBodyQuadruped to bind our IK system onto a horse skeleton
		m_Body = rage_new crIKBodyQuadruped();

		// Create bone finders for the horse skeleton
		const char* s_HorseArmBoneNames[] = { "spine+", "clavicle", "arm", 0, "elbow", 0, "nail", 0 };
		const char* s_HorseLegBoneNames[] = { "spine", "pelvis", "hip", 0, "knee", 0, "toe", 0 };

		crIKBodyBoneFinderByName findSpine(crIKBodyBoneFinderByName::sm_DefaultSpineBoneNames, crIKSpine::IK_SPINE_BONE_NUM);
		crIKBodyBoneFinderByName findHead(crIKBodyBoneFinderByName::sm_DefaultHeadBoneNames, crIKHead::IK_HEAD_BONE_NUM);
		crIKBodyBoneFinderByName findArm(s_HorseArmBoneNames, crIKArm::IK_ARM_BONE_NUM);
		crIKBodyBoneFinderByName findLeg(s_HorseLegBoneNames, crIKLeg::IK_LEG_BONE_NUM);

		// Initialize the crIKBodyHumanoid using the default R* North bone names
		atArray<const crIKBodyBoneFinder*> finders;
		finders.Reserve(6);
		finders.Append() = &findSpine;
		finders.Append() = &findHead;
		finders.Append() = &findArm;
		finders.Append() = &findArm;
		finders.Append() = &findLeg;
		finders.Append() = &findLeg;

		if(!m_Body->Init(skelData, finders))
		{
			Quitf("Couldn't initialize skeleton body!");
		}

		// Create the crKinematics object which stores our IK solvers and goals 
		m_IK = rage_new crKinematics();
		m_IK->Init(skeleton);

		//-- Create solvers for the head, spine, and legs
		m_HeadSolver = rage_new crIKSolverSimpleHead(crIKBodyQuadruped::IK_QUADRUPED_PART_HEAD, *m_Body);
		m_SpineSolver = rage_new crIKSolverSimpleSpine(crIKBodyQuadruped::IK_QUADRUPED_PART_SPINE, *m_Body);
		m_FrontLeftSolver = rage_new crIKSolverGleicherLimb(crIKBodyQuadruped::IK_QUADRUPED_PART_LEG_FRONT_LEFT, *m_Body);
		m_FrontRightSolver = rage_new crIKSolverGleicherLimb(crIKBodyQuadruped::IK_QUADRUPED_PART_LEG_FRONT_RIGHT, *m_Body);
		m_BackLeftSolver = rage_new crIKSolverGleicherLimb(crIKBodyQuadruped::IK_QUADRUPED_PART_LEG_BACK_LEFT, *m_Body);
		m_BackRightSolver = rage_new crIKSolverGleicherLimb(crIKBodyQuadruped::IK_QUADRUPED_PART_LEG_BACK_RIGHT, *m_Body);

		m_IK->AddSolver(m_HeadSolver);
		m_IK->AddSolver(m_SpineSolver);
		m_IK->AddSolver(m_FrontLeftSolver);
		m_IK->AddSolver(m_FrontRightSolver);
		m_IK->AddSolver(m_BackLeftSolver);
		m_IK->AddSolver(m_BackRightSolver);

		//-- Set some default values for the sample

		// Use some sane default values for the head and spine acceleration and speed
		m_MaxHeadSpeed = 5.0f * PI;
		m_MaxHeadAccel = 30.0f * PI;
		m_MaxSpineSpeed = 5.0f * PI;
		m_MaxSpineAccel = 30.0f * PI;

		// Set a good default for the amount of the head turn that should be done with the spine
		m_SpineRatio = 0.25f;

		//-STOP 

		m_EnableIK = true; 
		m_EnableLimits = false;
		m_EnableHead = false;
		m_EnableSpine = false;
		m_EnableArm = true;
		m_EnableHand = false;
		m_EnableLegs = false;

#if __BANK
		AddWidgetsIK();
#endif

	}

	void ShutdownClient()
	{
		for(int i=0; i<NUM_ANIMATIONS; i++)
		{
			delete m_Animations[i];
		}
		delete m_FrameA;
		delete m_FrameB;

		delete m_Body;
		delete m_IK;

		delete m_HeadSolver;
		delete m_SpineSolver;
		delete m_FrontLeftSolver;
		delete m_FrontRightSolver;
		delete m_BackLeftSolver;
		delete m_BackRightSolver;

		ragesamples::craSampleManager::ShutdownClient();
	}

#if __BANK
	void AddWidgetsIK()
	{
		bkBank &ikBank=BANKMGR.CreateBank("IK",50,500);
		ikBank.AddToggle("Enable IK", &m_EnableIK);
		ikBank.AddToggle("Enforce Limits", &m_EnableLimits);
		ikBank.AddToggle("Enable Head", &m_EnableHead);
		ikBank.AddToggle("Enable Spine", &m_EnableSpine);
		ikBank.AddToggle("Enable Arm", &m_EnableArm);
		ikBank.AddToggle("Enable Hand", &m_EnableHand);
		ikBank.AddToggle("Enable Legs", &m_EnableLegs);

		ikBank.AddSlider("Max Spine Accel", &m_MaxSpineAccel,   0.f, 1000.f, 5.f);
		ikBank.AddSlider("Max Spine Speed", &m_MaxSpineSpeed,  0.f, 100.f, 0.5f);
		ikBank.AddSlider("Max Head Accel", &m_MaxHeadAccel,    0.f, 1000.f, 5.f);
		ikBank.AddSlider("Max Head Speed", &m_MaxHeadSpeed,    0.f, 100.f, 0.5f);
		ikBank.AddSlider("Spine Track Ratio", &m_SpineRatio, 0.f, 1.f, 0.01f);


		ikBank.AddSlider("Goal Pos", &m_GoalPos, -1000.f, 1000.f, 0.05f);
		ikBank.AddSlider("Goal Hand Rot", &m_GoalRotInDegs, -1000.f, 1000.f, 1.f);
	}

#endif

	void UpdateClient()
	{	

		// reset is important when using Gleicher solver, as it adjusts offsets as well as rotations...
		crSkeleton& skeleton = GetSkeleton();
		skeleton.Reset();

		int idx0 = 1;
		int idx1 = 1;
		float blend = 1.f;

		if(m_GoalPos.y < m_Height[1])
		{
			idx1 = 0;
			blend =  Min(1.f, (m_Height[1] - m_GoalPos.y) / (m_Height[1] - m_Height[0]));
		}
		else
		{
			idx1 = 2;
			blend = Min(1.f, 1.f + ((m_GoalPos.y - m_Height[2]) / (m_Height[2] - m_Height[1])));
		}

		if (m_Animations[0] && m_Animations[1] && m_Animations[2])
		{
			m_Animations[idx0]->CompositeFrame(m_AnimTime, *m_FrameA);
			m_Animations[idx1]->CompositeFrame(m_AnimTime, *m_FrameB);

			m_FrameA->Blend(blend, *m_FrameB);
			m_FrameA->Pose(skeleton);
		}
		else
		{
			skeleton.Reset();
		}

		skeleton.Update();

		if (m_EnableIK)
		{
			// STEP #2. Update the IK goals every frame

			//-- Update the head goal
			crIKGoal& headGoal = m_HeadSolver->GetGoal();

			// Set blend ratio (head is always at 100% in this sample)
			headGoal.SetBlend(m_EnableHead ? 1.f : 0.f);

			if (m_EnableHead)
			{
				// Set the look at goal position
				headGoal.SetPrimaryPosition(RCC_VEC3V(m_GoalPos));

				// Set the max speed and acceleration
				m_Body->GetHead()->SetMaxAngularAcceleration(m_MaxHeadAccel);
				m_Body->GetHead()->SetMaxAngularVelocity(m_MaxHeadSpeed);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_HeadSolver->EnforceLimits(m_EnableLimits);


			//-- Update the spine goal
			crIKGoal& spineGoal = m_SpineSolver->GetGoal();

			// Set blend ratio (head is always at max spine ratio in this sample)
			spineGoal.SetBlend(m_EnableSpine ? m_SpineRatio : 0.0f);

			if (m_EnableSpine)
			{
				// Set the look at goal position
				spineGoal.SetPrimaryPosition(RCC_VEC3V(m_GoalPos));

				// Set the max speed and acceleration
				m_Body->GetSpine()->SetMaxAngularAcceleration(m_MaxSpineAccel);
				m_Body->GetSpine()->SetMaxAngularVelocity(m_MaxSpineSpeed);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_SpineSolver->EnforceLimits(m_EnableLimits);


			//-- Update the arm goal
			crIKGoal& armGoal = m_FrontRightSolver->GetGoal();

			// Set blend ratio (arm is always at max spine ratio in this sample)
			armGoal.SetBlend(m_EnableArm ? 1.0f : 0.0f);

			if (m_EnableArm)
			{
				Matrix34 anklePos;
				skeleton.GetGlobalMtx(m_Body->GetLeg(crIKBodyQuadruped::IK_QUADRUPED_LEG_FRONT_RIGHT)->GetAppendageBone()->GetIndex(), RC_MAT34V(anklePos));
				anklePos.d.y += 0.25f;
				armGoal.SetPrimaryPosition(RCC_VEC3V(anklePos.d));
				m_GoalPos = anklePos.d;

				// Set the desired hoof orientation
				if (m_EnableHand)
				{
					QuatV q = QuatVFromEulersXYZ(VECTOR3_TO_VEC3V(m_GoalRotInDegs * DtoR));
					armGoal.SetOrientation(q);
				}
				else
				{
					armGoal.SetUseOrientation(false);
				}
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_FrontRightSolver->EnforceLimits(m_EnableLimits);


			// STEP #3. Execute the solvers

			//-- Do it

			// Execute all of the IK solvers
			m_IK->Iterate();
			m_IK->Solve();
		}

		if (GetPlay())
		{
			m_AnimTime += TIME.GetSeconds()*GetPlaybackRate();

			if(m_AnimTime > m_Animations[1]->GetDuration())
			{
				m_AnimTime = 0.f;
			}
		}

		UpdateMatrixSet();
	}

	void UpdateMouse()
	{
		if((ioMouse::GetButtons() & (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT)) == (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT))
		{
			Vector3 mouseScreen, mouseFar;
			grcWorldIdentity();
			grcViewport::GetCurrent()->ReverseTransform(
				static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()),
				mouseScreen, mouseFar);

			grcWorldIdentity();
			grcBegin(drawLines,2);
			grcVertex3fv(&mouseScreen[0]);
			grcVertex3fv(&mouseFar[0]);
			grcEnd();

			Vector3 direction;
			direction.Subtract(mouseFar, mouseScreen);
			direction.Normalize();

			Vector4 plane;
			plane.ComputePlane(m_GoalPos, Vector3(0.f, 0.f, 1.f));

			float t;
			if(geomSegments::CollideRayPlane(mouseScreen, direction, plane, &t))
			{
				m_GoalPos = mouseScreen + direction * t;
			}
		}
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();


		if (1)
		{
			QuatV q = QuatVFromEulersXYZ(m_GoalRotInDegs * ScalarV(V_TO_RADIANS));
			DrawIKGoal(TransformV(q, m_GoalPos));
		}
		
		UpdateMouse();
	}

	void DrawIKGoal(TransformV_In goal)
	{
		Mat34V m;
		Mat34VFromTransformV(m, goal);
		grcWorldMtx(m);

		float offset = 0.1f;

		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcColor(Color_white);

		grcBegin(drawLines, 2);
		grcVertex3f(Vector3(-offset, 0.f, 0.f));
		grcVertex3f(Vector3(offset, 0.f, 0.f));
		grcEnd();

		grcBegin(drawLines, 2);
		grcVertex3f(Vector3(0.f, -offset, 0.f));
		grcVertex3f(Vector3(0.f, offset, 0.f));
		grcEnd();

		grcBegin(drawLines, 2);
		grcVertex3f(Vector3(0.f, 0.f, -offset));
		grcVertex3f(Vector3(0.f, 0.f, offset));
		grcEnd();
	}

private:
	crAnimation*			m_Animations[NUM_ANIMATIONS];
	crFrame*			m_FrameA;
	crFrame*			m_FrameB;
	float				m_AnimTime;

	Vector3 m_GoalPos;
	Vector3 m_GoalRotInDegs;

	float m_Height[NUM_ANIMATIONS];
	float m_StartFrame[NUM_ANIMATIONS];
	float m_EndFrame[NUM_ANIMATIONS];
	float m_BlendInFrames;
	float m_BlendOutFrames;

	float m_MaxHeadSpeed;
	float m_MaxHeadAccel;
	float m_MaxSpineSpeed;
	float m_MaxSpineAccel;

	float m_SpineRatio;

	crIKBodyQuadruped* m_Body;
	crKinematics* m_IK;

	crIKSolverSimpleHead* m_HeadSolver;
	crIKSolverSimpleSpine* m_SpineSolver;
	crIKSolverGleicherLimb *m_FrontLeftSolver, *m_FrontRightSolver, *m_BackRightSolver, *m_BackLeftSolver;

	bool m_EnableIK;
	bool m_EnableLimits;
	bool m_EnableHead;
	bool m_EnableSpine;
	bool m_EnableArm;
	bool m_EnableHand;
	bool m_EnableLegs;
};

// main application
int Main()
{
	craSampleIKHorse sampleCra;
	sampleCra.Init("sample_cranimation\\HorseSkel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
