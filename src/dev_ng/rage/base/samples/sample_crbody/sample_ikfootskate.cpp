// 
// sample_cranimation/sample_ikfootskate.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: IK FootSkate
// PURPOSE:
//		This sample shows how to clean up footskate using the IK system.
//		It takes a walk cycle, and simulates footskate by introducing a 
//		deliberate offset into the root translation.  It uses the Gleicher
//      IK solver to correct for the right foot sliding as a result.

using namespace rage;

#include "sample_cranimation/sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crbody/ikbody.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crskeleton/skeletondata.h"
#include "grcore/im.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"

#define DEFAULT_ANIM "Gent_nor_hnd_for_wlk_rpt.anim"
PARAM(anim,"[sample_anim] Animation file to load (default is \"" DEFAULT_ANIM "\")");

class craSampleIKFootSkate : public ragesamples::craSampleManager
{
public:
	craSampleIKFootSkate() 
	: ragesamples::craSampleManager()
	, m_ParentMtx(V_IDENTITY)
	{
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		crSkeleton& skeleton = GetSkeleton();
		const crSkeletonData& skelData = skeleton.GetSkeletonData();

		const char *animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);

		m_Animation=crAnimation::AllocateAndLoad(animName);
		if(!m_Animation)
		{
			Quitf("Couldn't load animation '%s'", animName);
		}

		m_Player=rage_new crAnimPlayer(m_Animation);
		m_Player->SetLooped(true, true);

		m_Frame=rage_new crFrame;

		if( m_Animation )
		{
			m_Frame->InitCreateBoneAndMoverDofs(skelData, true);
		}

		// STEP #1. Initialize the IK system

		//-- Create the core IK objects

		// Create a crIKBodyHumanoid to bind our IK system onto a human skeleton
		m_Body = rage_new crIKBodyHumanoid();

		// Initialize the crIKBodyHumanoid using the default R* San Diego bone names
		if(!m_Body->InitByDefaultNames(skelData))
		{
			Quitf("Couldn't initialize skeleton body!");
		}

		// Create the crKinematics object which stores our IK solvers and goals 
		m_IK = rage_new crKinematics();
		m_IK->Init(skeleton);


		// Create solvers for the right leg
		m_GleicherSolver = rage_new crIKSolverGleicherLimb(crIKBodyHumanoid::IK_HUMANOID_PART_LEG_RIGHT, *m_Body);
		m_IK->AddSolver(m_GleicherSolver);

		//-STOP

		m_EnableIK = true; 
		m_AlignRoll = true;
		m_SimulateFootSkate = 1.20f;

		// magic numbers, we need to be extracting these from events that mark up the animation
		m_IKStartFrame = 11.f;
		m_HeelContactFrame = 14.f;
		m_ToeContactFrame = 19.f;
		m_HeelReleaseFrame = 33.f;
		m_ToeReleaseFrame = 41.f;
		m_IKStopFrame = 45.f;
		m_NumCycleFrames = 40.f;

		m_State = STATE_NONE;

#if __BANK
		AddWidgetsIK();
#endif
	}

	void ShutdownClient()
	{
		delete m_Player;
		delete m_Animation;
		delete m_Frame;
		delete m_IK;
		delete m_Body;
		delete m_GleicherSolver;

		ragesamples::craSampleManager::ShutdownClient();
	}

#if __BANK
	void ResetStateCB()
	{
		m_State = STATE_NONE;
	}

	void AddWidgetsIK()
	{
		bkBank &ikBank=BANKMGR.CreateBank("IK",50,500);
		ikBank.AddToggle("Enable IK", &m_EnableIK);
		ikBank.AddToggle("Align Roll To Surface", &m_AlignRoll);
		ikBank.AddSlider("Simulate FootSkate", &m_SimulateFootSkate, 1.f, 10.f, 0.01f, datCallback(MFA(craSampleIKFootSkate::ResetStateCB), this));
	}

#endif

	void UpdateClient()
	{	
		crSkeleton& skeleton = GetSkeleton();
		skeleton.Reset(); 

		m_Player->Composite(*m_Frame);
        m_Frame->Pose(skeleton);

		Mat34V deltaMtx;
		m_Frame->GetMoverMatrix(deltaMtx);

		Mat34V& moverMtx = GetMoverMtx();
		moverMtx.SetCol3(moverMtx.GetCol3() + deltaMtx.GetCol3() * ScalarVFromF32(m_SimulateFootSkate));

		skeleton.Update();

		// STEP #2. Update the IK goals every frame

		float frame = fmodf((m_Player->GetTime()*30.f)-m_IKStartFrame*0.5f, m_NumCycleFrames) + m_IKStartFrame*0.5f;
		crIKGoal& goal = m_GleicherSolver->GetGoal();
		const crIKLeg& leg = *m_Body->GetLeg(crIKBodyHumanoid::IK_HUMANOID_LIMB_RIGHT);

		QuatV qSurface(V_IDENTITY);

		//-- Update the stride state machine

		if(frame < m_IKStartFrame)
		{
			m_State = STATE_NONE;
		}

		Mat34V appendageMtx, appendageTipMtx;

		switch(m_State)
		{
		case STATE_NONE:
			// When the foot is in the air, we set blending to 0
			goal.SetBlend(0.f);
			if(frame >= m_IKStartFrame)
			{
				m_State = STATE_IK_START;
				goal.SetBalance(0.f);
			}
			break;

		case STATE_IK_START:
			// Until the foot hits the ground, the position of the goals is
			// set to the current position of the feet.  At this point it is blending
			// into the proper foot roll and nothing else.
			skeleton.GetGlobalMtx(leg.GetAppendageBone()->GetIndex(), appendageMtx);
			skeleton.GetGlobalMtx(leg.GetAppendageTipBone()->GetIndex(), appendageTipMtx);

			goal.SetPrimaryPosition(appendageMtx.GetCol3());
			goal.SetSecondaryPosition(appendageTipMtx.GetCol3());

			goal.SetOrientation(qSurface);

			// Adjust blending from 0 at the IK start frame to 1 at the heel contact frame
			if(frame >= m_HeelContactFrame)
			{
				m_State = STATE_HEEL_CONTACT;
				goal.SetBlend(1.f);
			}
			else
			{
				goal.SetBlend((frame - m_IKStartFrame) / (m_HeelContactFrame - m_IKStartFrame));
			}
			break;

		case STATE_HEEL_CONTACT:
			// Once the heel has contacted the ground the heel goal is fixed at the contact point
			// and the toe goal is set to the same relative position to the heel as is was in the animation
			skeleton.GetGlobalMtx(leg.GetAppendageBone()->GetIndex(), appendageMtx);
			skeleton.GetGlobalMtx(leg.GetAppendageTipBone()->GetIndex(), appendageTipMtx);

			goal.SetPrimaryPosition(appendageMtx.GetCol3());
			goal.SetSecondaryPosition(appendageTipMtx.GetCol3());

			// The balance represents where the weight is distributed on the foot.
			// When walking the foot supports the weight initially on the heel (balance = 0),
			// then the entire foot (balance = 0.5), and finally on the ball of the foot (balance = 1).
			if(frame >= m_ToeContactFrame)
			{
				m_State = STATE_TOE_CONTACT;
				goal.SetBalance(0.5f);
			}
			else
			{
				goal.SetBalance(0.5f * (frame - m_HeelContactFrame) / (m_ToeContactFrame - m_HeelContactFrame));
			}
			break;

		case STATE_TOE_CONTACT:
			// The balance represents where the weight is distributed on the foot.
			// When walking the foot supports the weight initially on the heel (balance = 0),
			// then the entire foot (balance = 0.5), and finally on the ball of the foot (balance = 1).
			if(frame >= m_HeelReleaseFrame)
			{
				m_State = STATE_HEEL_RELEASE;
				goal.SetBalance(1.f);
			}
			else
			{
				goal.SetBalance(0.5f + 0.5f * (frame - m_ToeContactFrame) / (m_HeelReleaseFrame - m_ToeContactFrame));
			}
			break;

		case STATE_HEEL_RELEASE:
			if(frame >= m_ToeReleaseFrame)
			{
				m_State = STATE_TOE_RELEASE;
			}
			break;

		case STATE_TOE_RELEASE:
			// Adjust blending from 1 at the at the heel release frame to 0 at the IK stop frame
			goal.SetBlend(1.f - (frame - m_ToeReleaseFrame) / (m_IKStopFrame - m_ToeReleaseFrame));
			if(frame >= m_IKStopFrame)
			{
				goal.SetBlend(0.f);
				m_State = STATE_IK_STOP;
			}
			break;

		case STATE_IK_STOP:
			break;

		default:
			m_State = STATE_NONE;
			break;
		}

		// STEP #3. Execute the solvers

		//-- Do it

		if(m_EnableIK)
		{
			goal.SetUseOrientation(m_AlignRoll);

			skeleton.Update();

			m_IK->Iterate();
			m_IK->Solve();

			skeleton.Update();
		}

		//-STOP

		m_Player->SetRate(GetPlaybackRate());
		m_Player->Update(TIME.GetSeconds());

		UpdateMatrixSet();
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();

		crIKGoal& goal = m_GleicherSolver->GetGoal();
		DrawIKGoal(TransformV(goal.GetOrientation(), goal.GetPrimaryPosition()));
		DrawIKGoal(TransformV(QuatV(V_IDENTITY), goal.GetSecondaryPosition()));	
	}

	void DrawIKGoal(TransformV_In goal)
	{
		Mat34V m;
		Mat34VFromTransformV(m, goal);
		grcWorldMtx(RCC_MATRIX34(m));

		float offset = 0.1f;

		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcColor(Color_white);

		grcBegin(drawLines, 2);
		grcVertex3f(Vector3(-offset, 0.f, 0.f));
		grcVertex3f(Vector3(offset, 0.f, 0.f));
		grcEnd();

		grcBegin(drawLines, 2);
		grcVertex3f(Vector3(0.f, -offset, 0.f));
		grcVertex3f(Vector3(0.f, offset, 0.f));
		grcEnd();

		grcBegin(drawLines, 2);
		grcVertex3f(Vector3(0.f, 0.f, -offset));
		grcVertex3f(Vector3(0.f, 0.f, offset));
		grcEnd();
	}
	const crAnimation* GetAnimation() const { return m_Animation;}

private:
	crAnimation*			m_Animation;
	crAnimPlayer*			m_Player;
	crFrame*				m_Frame;
	crIKBodyHumanoid*		m_Body;
	crKinematics*			m_IK;
	crIKSolverGleicherLimb* m_GleicherSolver;
	Mat34V				m_ParentMtx;
	bool					m_EnableIK;
	bool					m_AlignRoll;
	float					m_SimulateFootSkate;
	int						m_State;

	float m_IKStartFrame;
	float m_HeelContactFrame;
	float m_ToeContactFrame;
	float m_HeelReleaseFrame;
	float m_ToeReleaseFrame;
	float m_IKStopFrame;
	float m_NumCycleFrames;

	enum
	{
		STATE_NONE,

		STATE_IK_START,
		STATE_HEEL_CONTACT,
		STATE_TOE_CONTACT,
		STATE_HEEL_RELEASE,
		STATE_TOE_RELEASE,
		STATE_IK_STOP,
	};
};

// main application
int Main()
{
	craSampleIKFootSkate sampleCra;
	sampleCra.Init("sample_cranimation\\newskel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
