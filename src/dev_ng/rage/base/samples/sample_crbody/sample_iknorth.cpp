// 
// sample_cranimation/sample_iknorth.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: IK Body North
// PURPOSE:
//		This sample shows how to use the IK system to look at and pick up objects
//      using the R* North skeleton.

using namespace rage;

#include "sample_cranimation/sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crbody/ikbody.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crskeleton/skeletondata.h"
#include "data/callback.h"
#include "grcore/im.h"
#include "vector/geometry.h"
#include "input/mouse.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"


#define NUM_ANIMATIONS (3)
#define IKDEBUG 0



class craSampleIKNorth : public ragesamples::craSampleManager
{
public:
	craSampleIKNorth() : ragesamples::craSampleManager()
	{
		m_AnimTime = 0.f;
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

#if IKDEBUG
		m_Batcher.Init(1024 * 1024);
#endif

		crSkeleton& skeleton = GetSkeleton();
		const crSkeletonData& skelData = skeleton.GetSkeletonData();

		m_Animations[0] = crAnimation::AllocateAndLoad("compression_idle");
		m_Animations[1] = crAnimation::AllocateAndLoad("no_compression_idle");
		m_Animations[2] = crAnimation::AllocateAndLoad("walk");

		m_FrameA = rage_new crFrame;
		m_FrameB = rage_new crFrame;

		if( m_Animations[0] && m_Animations[1] && m_Animations[2] )
		{
			m_FrameA->InitCreateBoneAndMoverDofs(skelData, true);
			m_FrameB->InitCreateBoneAndMoverDofs(skelData, true);
		}

		m_GoalPos = Vec3V(0.0f, 1.0f, 0.5f);
		m_GoalRotInDegs = Vec3V(0.f, 0.f, 90.f);

		m_animationIndex = 0;

		// TODO --- if the animation was marked up with events, this could all be automatically extracted!
		m_Height[0] = 0.f; m_Height[1] = 1.1f; m_Height[2] = 2.f;
		m_StartFrame[0] = 12.f; m_EndFrame[0] = 17.f;
		m_StartFrame[1] = 10.f; m_EndFrame[1] = 16.f;
		m_StartFrame[2] = 10.f; m_EndFrame[2] = 16.f;
		m_BlendInFrames = 10.f;
		m_BlendOutFrames = 16.f;

		// STEP #1. Initialize the IK system

		//-- Create the core IK objects

		// Create a crIKBodyHumanoid to bind our IK system onto a human skeleton
		m_Body = rage_new crIKBodyHumanoid();

		// Create bone finders for the North skeleton
		const char* s_NorthArmBoneNames[] = { "spine+", "clavicle", "upperarm", 0, "forearm", 0, "hand", 0 };
		const char* s_NorthLegBoneNames[] = { "spine", "pelvis", "thigh", 0, "calf", 0, "foot", "toe" };

		crIKBodyBoneFinderByName findSpine(crIKBodyBoneFinderByName::sm_DefaultSpineBoneNames, crIKSpine::IK_SPINE_BONE_NUM);
		crIKBodyBoneFinderByName findHead(crIKBodyBoneFinderByName::sm_DefaultHeadBoneNames, crIKHead::IK_HEAD_BONE_NUM);
		crIKBodyBoneFinderByName findArm(s_NorthArmBoneNames, crIKArm::IK_ARM_BONE_NUM);
		crIKBodyBoneFinderByName findLeg(s_NorthLegBoneNames, crIKLeg::IK_LEG_BONE_NUM);

		// Initialize the crIKBodyHumanoid using the default R* North bone names
		atArray<const crIKBodyBoneFinder*> finders;
		finders.Reserve(6);
		finders.Append() = &findSpine;
		finders.Append() = &findHead;
		finders.Append() = &findArm;
		finders.Append() = &findArm;
		finders.Append() = &findLeg;
		finders.Append() = &findLeg;

		if(!m_Body->Init(skelData, finders))
		{
			Quitf("Couldn't initialize skeleton body!");
		}

		// Create the crKinematics object which stores our IK solvers and goals 
		m_IK = rage_new crKinematics();
		m_IK->Init(skeleton);

		// Create solvers for the head, spine, and arm
		m_HeadSolver = rage_new crIKSolverSimpleHead(crIKBodyHumanoid::IK_HUMANOID_PART_HEAD, *m_Body);
		m_SpineSolver = rage_new crIKSolverSimpleSpine(crIKBodyHumanoid::IK_HUMANOID_PART_SPINE, *m_Body);
		m_ArmSolver = rage_new crIKSolverIterativeLimb(crIKBodyHumanoid::IK_HUMANOID_PART_ARM_RIGHT, *m_Body);

		m_IK->AddSolver(m_HeadSolver);
		m_IK->AddSolver(m_SpineSolver);
		m_IK->AddSolver(m_ArmSolver);

		//-- Fix the axes to work with the North skeleton orientation

		// Specify the up and forward axes for the spine
		m_Body->GetSpine()->SetUpAxis(Vec3V(1.f,0.f,0.f));
		m_Body->GetSpine()->SetFwdAxis(Vec3V(0.f,1.f,0.f));

		// Specify the up and forward axes for the head
		m_Body->GetHead()->SetUpAxis(Vec3V(1.f,0.f,0.f));
		m_Body->GetHead()->SetFwdAxis(Vec3V(0.f,1.f,0.f));

		// Specify the bend axis for the arm (the normal to the plane of the upper and lower arm)
		m_Body->GetArm(crIKBodyHumanoid::IK_HUMANOID_LIMB_RIGHT)->SetBendAxis(Vec3V(0.f,0.f,1.f));

		//-- Set some default values for the sample

		// Use some sane default values for the head and spine acceleration and speed
		m_MaxHeadSpeed = 5.0f * PI;
		m_MaxHeadAccel = 30.0f * PI;
		m_MaxSpineSpeed = 5.0f * PI;
		m_MaxSpineAccel = 30.0f * PI;

		// Set a good default for the amount of the head turn that should be done with the spine
		m_SpineRatio = 0.25f;

		//-STOP 

		m_EnableIK = true; 
		m_EnableLimits = false;
		m_EnableHead = false;
		m_EnableSpine = false;
		m_EnableArm = true;
		m_EnableHand = true;

		m_NumIter = 30;

		crBoneData* boneData = const_cast<crBoneData*>(skelData.GetBoneData(R_Clavicle));
		const_cast<Vec3V&>(boneData->GetRotationMin()) = Vec3V( -3.14f,  -3.14f, -3.14f);
		const_cast<Vec3V&>(boneData->GetRotationMax()) = Vec3V(  3.14f,   3.14f,  3.14f);

		boneData = const_cast<crBoneData*>(skelData.GetBoneData(R_UpperArm));
		const_cast<Vec3V&>(boneData->GetRotationMin()) = Vec3V( -1.57f,  -1.57f, -1.57f);
		const_cast<Vec3V&>(boneData->GetRotationMax()) = Vec3V(  1.0f,   1.57f,  0.0f);

		boneData = const_cast<crBoneData*>(skelData.GetBoneData(R_Forearm));
		const_cast<Vec3V&>(boneData->GetRotationMin()) = Vec3V( -0.14f, 0.f, -3.0f);
		const_cast<Vec3V&>(boneData->GetRotationMax()) = Vec3V(  0.14f, 0.f, -0.14f);

#if __BANK
		AddWidgetsIK();
#endif

	}


	void ShutdownClient()
	{
		for(int i=0; i<NUM_ANIMATIONS; i++)
		{
			delete m_Animations[i];
		}
		delete m_FrameA;
		delete m_FrameB;

		delete m_Body;
		delete m_IK;
		delete m_HeadSolver;
		delete m_SpineSolver;
		delete m_ArmSolver;

		ragesamples::craSampleManager::ShutdownClient();
	}

#if __BANK
	void AddWidgetsIK()
	{
		bkBank &ikBank=BANKMGR.CreateBank("IK",50,500);
		ikBank.AddSlider("Animation Index", &m_animationIndex, 0, 2, 1);
		ikBank.AddToggle("Enable IK", &m_EnableIK);
		ikBank.AddToggle("Enforce Limits", &m_EnableLimits);
		ikBank.AddToggle("Enable Head", &m_EnableHead);
		ikBank.AddToggle("Enable Spine", &m_EnableSpine);
		ikBank.AddToggle("Enable Arm", &m_EnableArm);
		ikBank.AddToggle("Enable Hand", &m_EnableHand);

		ikBank.AddSlider("Max Spine Accel", &m_MaxSpineAccel,   0.f, 1000.f, 5.f);
		ikBank.AddSlider("Max Spine Speed", &m_MaxSpineSpeed,  0.f, 100.f, 0.5f);
		ikBank.AddSlider("Max Head Accel", &m_MaxHeadAccel,    0.f, 1000.f, 5.f);
		ikBank.AddSlider("Max Head Speed", &m_MaxHeadSpeed,    0.f, 100.f, 0.5f);
		ikBank.AddSlider("Spine Track Ratio", &m_SpineRatio, 0.f, 1.f, 0.01f);

		ikBank.AddSlider("Iters", &m_NumIter, 0, 100, 1);
		ikBank.AddSlider("Goal Pos", &RC_VECTOR3(m_GoalPos), -1000.f, 1000.f, 0.05f);
		ikBank.AddSlider("Goal Rot", &RC_VECTOR3(m_GoalRotInDegs), -180.0f, 180.0f, 0.05f);

	}

#endif

	void UpdateClient()
	{	
		crSkeleton& skeleton = GetSkeleton();
		skeleton.Reset();

		if (m_Animations[0] && m_Animations[1] && m_Animations[2])
		{
			m_Animations[m_animationIndex]->CompositeFrame(m_AnimTime, *m_FrameA);
		}
		else
		{
			skeleton.Reset();
		}

		// Does the translation track for the root bone dof exist ?
		Vec3V displacement;
		if (m_FrameA->GetVector3(kTrackMoverTranslation, 0, displacement))
		{
			// Apply the mover track translation that need to be reapplied to the root bone.
			Vec3V currentRootTranslation;
			m_FrameA->GetBoneTranslation(0, RC_VEC3V(currentRootTranslation));
			Vec3V newRootTranslation = currentRootTranslation + displacement;
			m_FrameA->SetBoneTranslation(0, RCC_VEC3V(newRootTranslation));
		}

		// Does the rotation track for the root bone dof exist ?
		QuatV rotation;
		if (m_FrameA->GetQuaternion(kTrackMoverRotation, 0, rotation))
		{
			// Apply the mover track rotation that need to be reapplied to the root bone.
			QuatV currentRootRotation;
			m_FrameA->GetBoneRotation(0, currentRootRotation);

			QuatV newRootRotation = Multiply(rotation, currentRootRotation);
			m_FrameA->SetBoneRotation(0, newRootRotation);
		}

		m_FrameA->Pose(skeleton);

		//m_Skeleton->GetLocalMtx(0).RotateFullX(-PI * 0.5f);
		//m_Skeleton->GetLocalMtx(0).d.y += 1.0f;

		skeleton.Update();
		skeleton.Reset();
		skeleton.Update();

		if(m_EnableIK)
		{
			// STEP #2. Update the IK goals every frame

			//-- Update the head goal
			crIKGoal& headGoal = m_HeadSolver->GetGoal();

			// Set blend ratio (head is always at 100% in this sample)
			headGoal.SetBlend(m_EnableHead ? 1.f : 0.f);

			if (m_EnableHead)
			{
				// Set the look at goal position
				headGoal.SetPrimaryPosition(m_GoalPos);

				// Set the max speed and acceleration
				m_Body->GetHead()->SetMaxAngularAcceleration(m_MaxHeadAccel);
				m_Body->GetHead()->SetMaxAngularVelocity(m_MaxHeadSpeed);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_HeadSolver->EnforceLimits(m_EnableLimits);


			//-- Update the spine goal
			crIKGoal& spineGoal = m_SpineSolver->GetGoal();

			// Set blend ratio (head is always at max spine ratio in this sample)
			spineGoal.SetBlend(m_EnableSpine ? m_SpineRatio : 0.0f);

			if (m_EnableSpine)
			{
				// Set the look at goal position
				spineGoal.SetPrimaryPosition(m_GoalPos);

				// Set the max speed and acceleration
				m_Body->GetSpine()->SetMaxAngularAcceleration(m_MaxSpineAccel);
				m_Body->GetSpine()->SetMaxAngularVelocity(m_MaxSpineSpeed);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_SpineSolver->EnforceLimits(m_EnableLimits);


			//-- Update the arm goal
			crIKGoal& armGoal = m_ArmSolver->GetGoal();

			if (m_EnableArm)
			{
				// Set the blend ratio
				armGoal.SetBlend(1);

				// Set the position to reach for
				armGoal.SetPrimaryPosition(m_GoalPos);

				// Set the desired hand orientation
				if (m_EnableHand)
				{
					armGoal.SetUseOrientation(true);
				}
				else
				{
					armGoal.SetUseOrientation(false);	
				}

				if (m_ArmSolver)
					m_ArmSolver->SetMaxIterations(m_NumIter);
			}
			else
			{
				armGoal.SetBlend(0.0f);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_ArmSolver->EnforceLimits(m_EnableLimits);


			// STEP #3. Execute the solvers

			//-- Do it

			// Execute all of the IK solvers
#if IKDEBUG
			grcBatcher::SetCurrent(&m_Batcher);
#endif
			m_IK->Iterate();
			m_IK->Solve();
#if IKDEBUG
			grcBatcher::SetCurrent(NULL);
#endif
		}

		//-- Update procedural roll bones
		UpdateRollBone(skeleton,	Neck,		Neck_Roll,			X_AXIS,		1.0f, false);
		UpdateRollBone(skeleton,	L_Forearm,	L_ForeTwist,		X_AXIS,		1.0f, false);
		UpdateRollBone(skeleton,	R_Forearm,	R_ForeTwist,		X_AXIS,		1.0f, false);
		UpdateRollBone(skeleton,	L_UpperArm,	L_UpperArmRoll,		X_AXIS,		0.5f, true);
		UpdateRollBone(skeleton,	R_UpperArm,	R_UpperArmRoll,		X_AXIS,		0.5f, true);

		skeleton.Update();
		UpdateMatrixSet();

		//-STOP

		if (GetPlay())
		{
			m_AnimTime += TIME.GetSeconds()*GetPlaybackRate();

			if(m_AnimTime > m_Animations[m_animationIndex]->GetDuration())
			{
				m_AnimTime = 0.f;
			}
		}
	}

	void UpdateMouse()
	{
		if((ioMouse::GetButtons() & (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT)) == (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT))
		{
			Vec3V mouseScreen, mouseFar;
			grcWorldIdentity();
			grcViewport::GetCurrent()->ReverseTransform(
				static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()),
				RC_VECTOR3(mouseScreen), RC_VECTOR3(mouseFar));

			grcWorldIdentity();
			grcBegin(drawLines,2);
			grcVertex3fv(&mouseScreen[0]);
			grcVertex3fv(&mouseFar[0]);
			grcEnd();

			Vec3V direction = Normalize(Subtract(mouseFar, mouseScreen));

			Vector4 plane;
			Vec3V dir(0.f, 1.f, 0.f);
			plane.ComputePlane(RCC_VECTOR3(m_GoalPos), RCC_VECTOR3(dir));

			float t;
			if(geomSegments::CollideRayPlane(RCC_VECTOR3(mouseScreen), RCC_VECTOR3(direction), plane, &t))
			{
				m_GoalPos = mouseScreen + direction * ScalarVFromF32(t);
			}
		}
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();

		crSkeleton& skeleton = GetSkeleton();

		Vec3V targetPosition = m_GoalPos;

		Mat34V boneMat;		
		static bool renderForearm = false;
		if (renderForearm)
		{
			// Render the forearm bone
			skeleton.GetGlobalMtx(R_Forearm, RC_MAT34V(boneMat));
			grcBindTexture(NULL);
			grcLightState::SetEnabled(false);
			grcWorldIdentity();
			grcDrawAxis(0.5f, boneMat, true);
		}

		skeleton.GetGlobalMtx(R_Hand, RC_MAT34V(boneMat));

		static bool renderHand = false;	
		if (renderHand)
		{
			// Render the hand bone
			grcBindTexture(NULL);
			grcLightState::SetEnabled(false);
			grcWorldIdentity();
			grcDrawAxis(0.5f, boneMat, true);
		}

		Vec3V bonePosition = boneMat.GetCol3();
		/*
		// Render lines to show the target direction and the bone direction
		Vec3V targetDirection = targetPosition - boneMat.d;
		Vec3V boneDirection = boneMat.a;
		targetDirection.Normalize();
		boneDirection.Normalize();
		targetDirection *= 20.0f;
		boneDirection *= 20.0f;
		grcWorldIdentity();
		grcColor(Color32(1.0f, 0.0f, 0.0f, 0.75f));
		grcBegin(drawLines,4);
		grcVertex3f(boneMat.d);	grcVertex3f(boneMat.d + targetDirection);
		grcVertex3f(boneMat.d);	grcVertex3f(boneMat.d + boneDirection);
		grcEnd();
		*/

		// Update the orientation of the arm goal
		Vec3V up(V_Y_AXIS_WZERO);
		Vec3V c = Normalize(Subtract(bonePosition, m_GoalPos));
		Vec3V a = Normalize(Cross(up, c));
		Vec3V b = Cross(c, a);

		boneMat = Mat34V(a,b,c,bonePosition);
		
		Mat34V rotX(V_IDENTITY), rotZ(V_IDENTITY);
		Mat34VRotateLocalX(rotX, ScalarVFromF32(-90.0f * DtoR));
		Mat34VRotateLocalZ(rotZ, ScalarVFromF32(90.0f * DtoR));
		Transform(boneMat, rotX, boneMat);
		Transform(boneMat, rotZ, boneMat);

		Vec3V eulers = Mat33VToEulersXYZ(boneMat.GetMat33());
		QuatV q;
		static bool menu = true;
		if (menu)
		{
			q = QuatVFromEulersXYZ(m_GoalRotInDegs * ScalarV(V_TO_RADIANS));
		}
		else
		{
			q = QuatVFromEulersXYZ(eulers);
		}

		crIKGoal& armGoal = m_ArmSolver->GetGoal();
		armGoal.SetOrientation(q);
		armGoal.SetBlend(1.0f);
	
		// Render the goal position and rotation
		DrawIKGoal(TransformV(q, targetPosition));

		// Draw the world axis for reference
		DrawIKGoal(TransformV(V_IDENTITY));

#if IKDEBUG
		m_Batcher.Render(true);
#endif

		UpdateMouse();
	}

	void DrawIKGoal(TransformV_In goal)
	{
		Mat34V m;
		Mat34VFromTransformV(m, goal);
		grcWorldMtx(m);

		float offset = 0.1f;

		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcColor(Color_red);

		grcBegin(drawLines, 2);
		grcVertex3f(Vec3V(0.0f, 0.f, 0.f));
		grcVertex3f(Vec3V(offset, 0.f, 0.f));
		grcEnd();

		grcColor(Color_green);
		grcBegin(drawLines, 2);
		grcVertex3f(Vec3V(0.f, 0.0f, 0.f));
		grcVertex3f(Vec3V(0.f, offset, 0.f));
		grcEnd();

		grcColor(Color_blue);
		grcBegin(drawLines, 2);
		grcVertex3f(Vec3V(0.f, 0.f, 0.0f));
		grcVertex3f(Vec3V(0.f, 0.f, offset));
		grcEnd();
	}

	const crAnimation* GetAnimation() const { return m_Animations[1]; }

	enum BoneIndex
	{
		Root,
		Pelvis,
		L_Thigh,
		L_Calf,
		L_Foot,
		L_Toe0,
		L_Toe0Nub,
		L_Calf_Roll,
		R_Thigh,
		R_Calf,
		R_Foot,
		R_Toe0,
		R_Toe0Nub,
		R_Calf_Roll,
		Spine,
		Spine1,
		Spine2,
		Neck,
		Head,
		HeadNub,
		Neck_Roll,
		L_Clavicle,
		L_UpperArm,
		L_Forearm,
		L_Hand,
		L_Finger0,
		L_Finger01,
		L_Finger02,
		L_Finger0Nub,
		L_Finger1,
		L_Finger11,
		L_Finger12,
		L_Finger1Nub,
		L_Finger2,
		L_Finger21,
		L_Finger22,
		L_Finger2Nub,
		L_Finger3,
		L_Finger31,
		L_Finger32,
		L_Finger3Nub,
		L_ForeTwist,
		L_ForeTwist1,
		L_UpperArmRoll,
		R_Clavicle,
		R_UpperArm,
		R_Forearm,
		R_Hand,
		R_Finger0,
		R_Finger01,
		R_Finger02,
		R_Finger0Nub,
		R_Finger1,
		R_Finger11,
		R_Finger12,
		R_Finger1Nub,
		R_Finger2,
		R_Finger21,
		R_Finger22,
		R_Finger2Nub,
		R_Finger3,
		R_Finger31,
		R_Finger32,
		R_Finger3Nub,
		R_ForeTwist,
		R_ForeTwist1,
		R_UpperArmRoll,
		Spine_2_roll
	};

	// Used to identify which axis a roll bone should use
	enum RollBoneAxis
	{
		X_AXIS =	0,
		Y_AXIS,
		Z_AXIS,
	};

	static float LimitAngle(float fLimitAngle)
	{
		while (fLimitAngle >= PI)
			fLimitAngle -= 2.0f*PI;

		while (fLimitAngle < -PI)
			fLimitAngle += 2.0f*PI;

		return(fLimitAngle);
	}

	static void UpdateRollBone( crSkeleton& skeleton, const int originalBoneIndex, const int rollBoneIndex, const RollBoneAxis rollBoneAxis, float percentageRoll, bool child)
	{
		// Check the axis is valid
		Assert(rollBoneAxis == X_AXIS || rollBoneAxis == Y_AXIS || rollBoneAxis == Z_AXIS);

		// Check the percentageRoll is valid
		Assert(percentageRoll >= 0.0f && percentageRoll <= 1.0f);

		// Get the original bone matrix
		Mat34V originalBoneMat = skeleton.GetLocalMtx(originalBoneIndex);

		//
		// Calculate the new rotation and position of the roll bone
		//

		// Convert the original bones rotation from matrix to Euler angle form
		Vec3V rotationInEulers = Mat33VToEulersXYZ(originalBoneMat.GetMat33());

		// Rotate the chosen axis by the specified percentage while conserving the other two axes
		switch(rollBoneAxis)
		{
		case X_AXIS: 
			if (child)
			{
				rotationInEulers[0] *= percentageRoll; 
				rotationInEulers[0] += PI; 
				rotationInEulers[0] = LimitAngle(rotationInEulers[0]);
				rotationInEulers[1] = 0.0f;
				rotationInEulers[2] = 0.0f;
			}
			else
			{
				rotationInEulers[0] *= percentageRoll; 
			}
			break;

		case Y_AXIS: 
			if (child)
			{
				rotationInEulers[0] = 0.0f; 
				rotationInEulers[1] *= percentageRoll;
				rotationInEulers[1] += PI; 
				rotationInEulers[1] = LimitAngle(rotationInEulers[1]);
				rotationInEulers[2] = 0.0f;
			}
			else
			{
				rotationInEulers[1] *= percentageRoll;
			}
			break;

		case Z_AXIS: 
			if (child)
			{
				rotationInEulers[0] = 0.0f; 
				rotationInEulers[1] = 0.0f; 
				rotationInEulers[2] *= percentageRoll;
				rotationInEulers[2] += PI; 
				rotationInEulers[2] = LimitAngle(rotationInEulers[2]);
			}
			else
			{
				rotationInEulers[2] *= percentageRoll; 
			}
			break;

		default:
			break;
		}

		// Get the roll bone matrix
		Mat34V& rollBoneMat = skeleton.GetLocalMtx(rollBoneIndex);

		// Convert the new rotation from Euler angles back to matrix form
		Mat34VFromEulersXYZ(rollBoneMat, rotationInEulers);
	
		if (!child)
		{
			// Retain the original position of the roll bone
			rollBoneMat.SetCol3(originalBoneMat.GetCol3());
		}
	}

private:
	crAnimation*			m_Animations[NUM_ANIMATIONS];
	crFrame*			m_FrameA;
	crFrame*			m_FrameB;
	float					m_AnimTime;

	Vec3V m_GoalPos;
	Vec3V m_GoalRotInDegs;

	float m_Height[NUM_ANIMATIONS];
	float m_StartFrame[NUM_ANIMATIONS];
	float m_EndFrame[NUM_ANIMATIONS];
	float m_BlendInFrames;
	float m_BlendOutFrames;

	float m_MaxHeadSpeed;
	float m_MaxHeadAccel;
	float m_MaxSpineSpeed;
	float m_MaxSpineAccel;
	float m_SpineRatio;

	int m_animationIndex;

#if IKDEBUG
	grcBatcher m_Batcher;
#endif

	crIKBodyHumanoid* m_Body;
	crKinematics* m_IK;
	crIKSolverSimpleHead* m_HeadSolver;
	crIKSolverSimpleSpine* m_SpineSolver;
	crIKSolverIterativeLimb* m_ArmSolver;

	bool m_EnableIK;
	bool m_EnableLimits;
	bool m_EnableHead;
	bool m_EnableSpine;
	bool m_EnableArm;
	bool m_EnableHand;

	int m_NumIter;
};

// main application
int Main()
{
	craSampleIKNorth sampleCra;
	sampleCra.Init("sample_cranimation\\NorthSkel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
