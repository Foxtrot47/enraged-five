// 
// sample_cranimation/sample_ikbody.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: IK Body
// PURPOSE:
//		This sample shows how to use the IK system to look at and pick up objects.

using namespace rage;

#include "sample_cranimation/sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crbody/ikbody.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crskeleton/skeletondata.h"
#include "data/callback.h"
#include "grcore/im.h"
#include "vector/geometry.h"
#include "input/mouse.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"


#define DEFAULT_ANIM_UPP "Gent_nor_hnd_lft_upp_pck"
#define DEFAULT_ANIM_MID "Gent_nor_hnd_lft_mid_pck"
#define DEFAULT_ANIM_DWN "Gent_nor_hnd_lft_dwn_pck"

#define NUM_ANIMATIONS (3)

PARAM(anim_upp,"[sample_ikbody] Upper animation file to load (default is \"" DEFAULT_ANIM_UPP "\")");
PARAM(anim_mid,"[sample_ikbody] Middle animation file to load (default is \"" DEFAULT_ANIM_MID "\")");
PARAM(anim_dwn,"[sample_ikbody] Lower animation file to load (default is \"" DEFAULT_ANIM_DWN "\")");


class craSampleIKBody : public ragesamples::craSampleManager
{
public:
	craSampleIKBody() : ragesamples::craSampleManager()
	{
		m_AnimTime = 0.f;
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		crSkeleton& skeleton = GetSkeleton();
		const crSkeletonData& skelData = skeleton.GetSkeletonData();

		const char *animNameUpp = DEFAULT_ANIM_UPP;
		const char *animNameMid = DEFAULT_ANIM_MID;
		const char* animNameDwn = DEFAULT_ANIM_DWN;
		PARAM_anim_upp.Get(animNameUpp);
		PARAM_anim_mid.Get(animNameMid);
		PARAM_anim_dwn.Get(animNameDwn);

		m_Animations[0] = crAnimation::AllocateAndLoad(animNameDwn);
		m_Animations[1] = crAnimation::AllocateAndLoad(animNameMid);
		m_Animations[2] = crAnimation::AllocateAndLoad(animNameUpp);

		m_FrameA = rage_new crFrame;
		m_FrameB = rage_new crFrame;

		if( m_Animations[0] && m_Animations[1] && m_Animations[2] )
		{
			m_FrameA->InitCreateBoneAndMoverDofs(skelData, false);
			m_FrameB->InitCreateBoneAndMoverDofs(skelData, false);
		}

		m_GoalPos = Vec3V(0.1f, 0.7f, -0.55f);
		m_GoalRotInDegs = Vec3V(90.f, 0.f, 0.f);

		// TODO --- if the animation was marked up with events, this could all be automatically extracted!
		m_Height[0] = 0.f; m_Height[1] = 1.1f; m_Height[2] = 2.f;
		m_StartFrame[0] = 12.f; m_EndFrame[0] = 17.f;
		m_StartFrame[1] = 10.f; m_EndFrame[1] = 16.f;
		m_StartFrame[2] = 10.f; m_EndFrame[2] = 16.f;
		m_BlendInFrames = 10.f;
		m_BlendOutFrames = 16.f;

		// STEP #1. Initialize the IK system

		//-- Create the core IK objects

		// Create a crIKBodyHumanoid to bind our IK system onto a human skeleton
		m_Body = rage_new crIKBodyHumanoid();

		// Initialize the crIKBodyHumanoid using the default R* San Diego bone names
		if(!m_Body->InitByDefaultNames(skelData))
		{
			Quitf("Couldn't initialize skeleton body!");
		}

		// Create the crKinematics object which stores our IK solvers and goals 
		m_IK = rage_new crKinematics();
		m_IK->Init(skeleton);

		m_HeadSolver = rage_new crIKSolverSimpleHead(crIKBodyHumanoid::IK_HUMANOID_PART_HEAD, *m_Body);
		m_SpineSolver = rage_new crIKSolverSimpleSpine(crIKBodyHumanoid::IK_HUMANOID_PART_SPINE, *m_Body);
		m_GleicherSolver = rage_new crIKSolverGleicherLimb(crIKBodyHumanoid::IK_HUMANOID_PART_ARM_LEFT, *m_Body);

		m_IK->AddSolver(m_HeadSolver);
		m_IK->AddSolver(m_SpineSolver);
		m_IK->AddSolver(m_GleicherSolver);

		//-- Set some default values for the sample

		// Use some sane default values for the head and spine acceleration and speed
		m_MaxHeadSpeed = 5.0f * PI;
		m_MaxHeadAccel = 30.0f * PI;
		m_MaxSpineSpeed = 5.0f * PI;
		m_MaxSpineAccel = 30.0f * PI;

		// Set a good default for the amount of the head turn that should be done with the spine
		m_SpineRatio = 0.25f;

		//-STOP 

		m_EnableIK = true; 
		m_EnableLimits = true;
		m_EnableHead = true;
		m_EnableSpine = true;
		m_EnableArm = true;
		m_EnableHand = true;

		m_TotalTime = 0;
		m_Frames = 0;

#if __BANK
		AddWidgetsIK();
#endif
	}

	void ShutdownClient()
	{
		for(int i=0; i<NUM_ANIMATIONS; i++)
		{
			delete m_Animations[i];
		}
		delete m_FrameA;
		delete m_FrameB;

		delete m_Body;
		delete m_IK;
		delete m_HeadSolver;
		delete m_SpineSolver;
		delete m_GleicherSolver;

		ragesamples::craSampleManager::ShutdownClient();
	}

#if __BANK
	void AddWidgetsIK()
	{
		bkBank &ikBank=BANKMGR.CreateBank("IK",50,500);
		ikBank.AddToggle("Enable IK", &m_EnableIK);
		ikBank.AddToggle("Enforce Limits", &m_EnableLimits);
		ikBank.AddToggle("Enable Head", &m_EnableHead);
		ikBank.AddToggle("Enable Spine", &m_EnableSpine);
		ikBank.AddToggle("Enable Arm", &m_EnableArm);
		ikBank.AddToggle("Enable Hand", &m_EnableHand);

		ikBank.AddSlider("Max Spine Accel", &m_MaxSpineAccel,   0.f, 1000.f, 5.f);
		ikBank.AddSlider("Max Spine Speed", &m_MaxSpineSpeed,  0.f, 100.f, 0.5f);
		ikBank.AddSlider("Max Head Accel", &m_MaxHeadAccel,    0.f, 1000.f, 5.f);
		ikBank.AddSlider("Max Head Speed", &m_MaxHeadSpeed,    0.f, 100.f, 0.5f);
		ikBank.AddSlider("Spine Track Ratio", &m_SpineRatio, 0.f, 1.f, 0.01f);


		ikBank.AddSlider("Goal Pos", &RC_VECTOR3(m_GoalPos), -1000.f, 1000.f, 0.05f);
		ikBank.AddSlider("Goal Hand Rot", &RC_VECTOR3(m_GoalRotInDegs), -1000.f, 1000.f, 1.f);
	}

#endif

	void UpdateClient()
	{	
		// reset is important when using Gleicher solver, as it adjusts offsets as well as rotations...
		crSkeleton& skeleton = GetSkeleton();
		skeleton.Reset();

		int idx0 = 1;
		int idx1 = 1;
		float blend = 1.f;

		if(m_GoalPos.GetYf() < m_Height[1])
		{
			idx1 = 0;
			blend =  Min(1.f, (m_Height[1] - m_GoalPos.GetYf()) / (m_Height[1] - m_Height[0]));
		}
		else
		{
			idx1 = 2;
			blend = Min(1.f, 1.f + ((m_GoalPos.GetYf() - m_Height[2]) / (m_Height[2] - m_Height[1])));
		}

		if (m_Animations[0] && m_Animations[1] && m_Animations[2])
		{
			m_Animations[idx0]->CompositeFrame(m_AnimTime, *m_FrameA);
			m_Animations[idx1]->CompositeFrame(m_AnimTime, *m_FrameB);

			m_FrameA->Blend(blend, *m_FrameB);
			m_FrameA->Pose(skeleton);
		}
		else
		{
			skeleton.Reset();
		}

		skeleton.Update();

		if(m_EnableIK)
		{
			// STEP #2. Update the IK goals every frame

			//-- Update the head goal
			crIKGoal& headGoal = m_HeadSolver->GetGoal();

			// Set blend ratio (head is always at 100% in this sample)
			headGoal.SetBlend(m_EnableHead ? 1.f : 0.f);

			if (m_EnableHead)
			{
				// Set the look at goal position
				headGoal.SetPrimaryPosition(m_GoalPos);

				// Set the max speed and acceleration
				m_Body->GetHead()->SetMaxAngularAcceleration(m_MaxHeadAccel);
				m_Body->GetHead()->SetMaxAngularVelocity(m_MaxHeadSpeed);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_HeadSolver->EnforceLimits(m_EnableLimits);


			//-- Update the spine goal
			crIKGoal& spineGoal = m_SpineSolver->GetGoal();

			// Set blend ratio (head is always at max spine ratio in this sample)
			spineGoal.SetBlend(m_EnableSpine ? m_SpineRatio : 0.0f);

			if (m_EnableSpine)
			{
				// Set the look at goal position
				spineGoal.SetPrimaryPosition(m_GoalPos);

				// Set the max speed and acceleration
				m_Body->GetSpine()->SetMaxAngularAcceleration(m_MaxSpineAccel);
				m_Body->GetSpine()->SetMaxAngularVelocity(m_MaxSpineSpeed);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_SpineSolver->EnforceLimits(m_EnableLimits);


			//-- Update the arm goal
			crIKGoal& armGoal = m_GleicherSolver->GetGoal();

			if (m_EnableArm)
			{
				// Calculate the IK solution blend ratio

				float ikStart = Lerp(blend, m_StartFrame[idx0], m_StartFrame[idx1]);
				float ikEnd = Lerp(blend, m_EndFrame[idx0], m_EndFrame[idx1]);

				float ikBlend = 1.f;
				if((m_AnimTime*30.f) < ikStart)
				{
					ikBlend = BellInOut(0.5f+fabs((m_AnimTime*30.f) - ikStart) / (m_BlendInFrames*2.f));
				}
				else if((m_AnimTime*30.f) > ikEnd)
				{
					ikBlend = BellInOut(0.5f+fabs((m_AnimTime*30.f) - ikEnd) / (m_BlendOutFrames*2.f));
				}

				armGoal.SetBlend(ikBlend);

				// Set the position to reach for
				armGoal.SetPrimaryPosition(m_GoalPos);
				
				// Set the desired hand orientation
				if (m_EnableHand)
				{
					QuatV q = QuatVFromEulersXYZ(m_GoalRotInDegs * ScalarV(V_TO_RADIANS));
					armGoal.SetOrientation(q);
				}
				else
				{
					armGoal.SetUseOrientation(false);
				}
			}
			else
			{
				armGoal.SetBlend(0.0f);
			}

			// Set whether to enforce the joint limits specified in the skeleton data
			m_GleicherSolver->EnforceLimits(m_EnableLimits);


			// STEP #3. Execute the solvers

			//-- Do it

			// Execute all of the IK solvers
			sysTimer timer;
			timer.Reset();
			m_IK->Iterate();
			m_IK->Solve();
			m_TotalTime += timer.GetMsTime();

			if ((++m_Frames % 100) == 0)
			{
                Printf("%f\n", m_TotalTime / m_Frames);
			}
		}

		//-STOP

		if (GetPlay())
		{
			m_AnimTime += TIME.GetSeconds()*GetPlaybackRate();

			if(m_AnimTime > m_Animations[1]->GetDuration())
			{
				m_AnimTime = 0.f;
			}
		}

		UpdateMatrixSet();
	}

	void UpdateMouse()
	{
		if((ioMouse::GetButtons() & (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT)) == (ioMouse::MOUSE_LEFT|ioMouse::MOUSE_RIGHT))
		{
			Vec3V mouseScreen, mouseFar;
			grcWorldIdentity();
			grcViewport::GetCurrent()->ReverseTransform(
				static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()),
				RC_VECTOR3(mouseScreen), RC_VECTOR3(mouseFar));

			grcWorldIdentity();
			grcBegin(drawLines,2);
			grcVertex3fv(&mouseScreen[0]);
			grcVertex3fv(&mouseFar[0]);
			grcEnd();

			Vec3V direction = Normalize(Subtract(mouseFar, mouseScreen));

			Vector4 plane;
			Vec3V dir(0.f, 0.f, 1.f);
			plane.ComputePlane(RCC_VECTOR3(m_GoalPos), RCC_VECTOR3(dir));

			float t;
			if(geomSegments::CollideRayPlane(RCC_VECTOR3(mouseScreen), RCC_VECTOR3(direction), plane, &t))
			{
				m_GoalPos = mouseScreen + direction * ScalarVFromF32(t);
			}
		}
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();

		if (1)
		{
			QuatV q = QuatVFromEulersXYZ(m_GoalRotInDegs * ScalarV(V_TO_RADIANS));
			DrawIKGoal(TransformV(q, m_GoalPos));
		}

		UpdateMouse();
	}

	void DrawIKGoal(TransformV_In goal)
	{
		Mat34V m;
		Mat34VFromTransformV(m, goal);
		grcWorldMtx(m);

		float offset = 0.1f;

		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcColor(Color_white);

		grcBegin(drawLines, 2);
		grcVertex3f(-offset, 0.f, 0.f);
		grcVertex3f(offset, 0.f, 0.f);
		grcEnd();

		grcBegin(drawLines, 2);
		grcVertex3f(0.f, -offset, 0.f);
		grcVertex3f(0.f, offset, 0.f);
		grcEnd();

		grcBegin(drawLines, 2);
		grcVertex3f(0.f, 0.f, -offset);
		grcVertex3f(0.f, 0.f, offset);
		grcEnd();
	}

private:
	crAnimation*			m_Animations[NUM_ANIMATIONS];
	crFrame*			m_FrameA;
	crFrame*			m_FrameB;
	float					m_AnimTime;

	Vec3V m_GoalPos;
	Vec3V m_GoalRotInDegs;

	float m_Height[NUM_ANIMATIONS];
	float m_StartFrame[NUM_ANIMATIONS];
	float m_EndFrame[NUM_ANIMATIONS];
	float m_BlendInFrames;
	float m_BlendOutFrames;

	float m_MaxHeadSpeed;
	float m_MaxHeadAccel;
	float m_MaxSpineSpeed;
	float m_MaxSpineAccel;

	float m_SpineRatio;

	float m_TotalTime;
	int m_Frames;

	crIKBodyHumanoid* m_Body;
	crKinematics* m_IK;
	crIKSolverSimpleHead * m_HeadSolver;
	crIKSolverSimpleSpine * m_SpineSolver;
	crIKSolverGleicherLimb * m_GleicherSolver;
	bool m_EnableIK;
	bool m_EnableLimits;
	bool m_EnableHead;
	bool m_EnableSpine;
	bool m_EnableArm;
	bool m_EnableHand;
};

// main application
int Main()
{
	craSampleIKBody sampleCra;
	sampleCra.Init("sample_cranimation\\NewSkel");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
