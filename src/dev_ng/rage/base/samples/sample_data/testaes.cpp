// 
// /testcompress.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:	Tests the AES encryption/decryption functionality.

#include "data/aes.h"

#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"

#if INTEL_AES_INSTRUCTIONS
#include "data/iaes_asm_interface.h"
#endif

using namespace rage;

static char test_data[16]  = "TEST-AES\x11\x22\x33\x44\xAA\xFF\xCC";

char temp[sizeof(test_data)] ;
char temp2[sizeof(test_data)+1] ;

static unsigned char test_key[32] = {
	0xb0, 0x5c, 0x84, 0x76, 0x4d, 0x8d, 0xe7, 0x83,
	0xa3, 0xf0, 0xab, 0x93, 0xc1, 0x55, 0x4f, 0xb0,
	0x8c, 0xf2, 0x2b, 0x75, 0x7e, 0x1d, 0x64, 0xe9,
    0xa9, 0x85, 0xe8, 0x02, 0x26, 0x0a, 0x22, 0x52
};

int Main()
{
	AES aes(test_key);

	memcpy(temp,test_data,sizeof(test_data));
	aes.Encrypt(temp,sizeof(test_data));
	if (!memcmp(temp,test_data,sizeof(test_data)))
		Quitf("encryption didn't do anything?");
	memcpy(temp2+1,temp,sizeof(temp));
	aes.Decrypt(temp,sizeof(test_data));
	if (memcmp(temp,test_data,sizeof(test_data)))
		Quitf("decryption mismatch");

#if INTEL_AES_INSTRUCTIONS
	if (check_for_aes_instructions()) {
		Displayf("Have Intel AES instructions");

		// The internal expanded code key representations don't match
		// between the two libraries.  The expanded key must be aligned.
		static ALIGNAS(16) UCHAR expanded_key[240];
		iDecExpandKey256(test_key, expanded_key);

		// Also verify that unaligned data is fine
		sAesData aesData;
		aesData.in_block = (UCHAR*)temp2+1;
		aesData.out_block = (UCHAR*)temp2+1;
		aesData.expanded_key = expanded_key;
		aesData.num_blocks = 1;

		iDec256(&aesData);
		if (memcmp(temp,temp2+1,sizeof(test_data)))
			Quitf("AES insns didn't produce same result.");
	}
	else
		Warningf("No Intel AES instructions detected");
#endif

	float bestTime = 1E10;
	for (int j=0; j<10; j++) {
		sysTimer t;
		for (int i=0; i<1000; i++)
			aes.Decrypt(temp,sizeof(test_data));
		float thisTime = t.GetUsTime();
		if (thisTime < bestTime)
			bestTime = thisTime;
	}
	Displayf("%fus to decrypt block 1000 times",bestTime);

	return 0;
}
