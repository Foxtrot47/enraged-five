// 
// /testcompress.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:	Tests the datCompress / datDecompress functionality.
//		They implement an LZSS variant which supports up to 64k
//		of data (intended for network packets, otherwise the limit
//		could be trivially raised).
#include "file/stream.h"

#include "data/compress.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

int Main()
{
	int argc = sysParam::GetArgCount();
	char **argv = sysParam::GetArgArray();

	if (argc != 3) {
		Errorf("usage: %s infile outfile\n",argv[0]);
		return 1;
	}
	fiStream *inStream = fiStream::Open(argv[1]);
	if (!inStream) {
		Errorf("Cannot open input file '%s'",argv[1]);
		return 1;
	}

	u32 inSize = (u32) inStream->Size();
	u8 *in = rage_new u8[inSize];
	u32 tempSize = inSize * 2 + 16;
	u8 *temp = rage_new u8[tempSize];
	u8 *check = rage_new u8[inSize];
	inStream->Read(in,inSize);
	inStream->Close();

	u32 compSize = datCompress(temp, tempSize, in, inSize);

	Displayf("Compressed %u to %u bytes",inSize,compSize);
	u32 checkSize = datDecompress(check, inSize, temp, compSize);

	int result = 1;

	if (checkSize != inSize)
		Errorf("Decompression failure (size mismatch)");
	else if (memcmp(in, check, inSize))
		Errorf("Decompression failure (data mismatch)");
	else {
		fiStream *outStream = fiStream::Create(argv[2]);
		if (!outStream) {
			Errorf("Cannot create output file '%s'",argv[2]);
			return 1;
		}
		outStream->Write(temp, compSize);
		outStream->Close();
		result = 0;
	}

	delete[] check;
	delete[] temp;
	delete[] in;

	return result;
}
