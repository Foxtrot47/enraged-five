// 
// sample_file/sample_thruput.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"

#include "file/stream.h"
#include "system/timer.h"

using namespace rage;

int Main()
{
	// const char *testFile = "t:\\testfile.dat";
#if __PPU
	const char *testFile = "/dev_hdd0/testfile.dat";
#elif __PSP2
	const char *testFile = "c:/testfile.dat";
#else
	const char *testFile = "c:/testfile.dat";
#endif
	const int testSize = __PSP2?  1024 * 1024 : 32 * 1024 * 1024;
	for (int shift = 9; shift < 16; shift++) {
		const int testBufferSize = 1 << shift;
		char *test = rage_aligned_new (128) char[testBufferSize];
		for (int i=0; i<testBufferSize; i++)
			test[i] = (char) i;
		Displayf("test buffer size = %d bytes",testBufferSize);

		const fiDevice *dev = fiDevice::GetDevice(testFile);
		// Displayf("device is %p",dev);
		sysTimer T;
		fiHandle h = dev->Create(testFile);
		if (!fiIsValidHandle(h))
			return 1;
		int accum = 0;
		while (accum < testSize) {
			int amt = dev->Write(h,test,testBufferSize);
			if (amt <= 0)
				Quitf("error during write");
			accum += amt;
		}
		dev->Close(h);
		Displayf("  %.2fms to write %dK file, %.1fM/sec",T.GetMsTime(),testSize>>10,testSize/T.GetUsTime());
		
		T.Reset();
		h = dev->Open(testFile,true);
		if (!fiIsValidHandle(h))
			return 1;
		accum = 0;
		while (accum < testSize) {
			int amt = dev->Read(h,test,testBufferSize);
			if (amt <= 0)
				Quitf("error during read");
			accum += amt;
		}
		dev->Close(h);
		Displayf("  %.2fs to read %dK file, %.1fM/sec",T.GetMsTime(),testSize>>10,testSize/T.GetUsTime());

		delete[] test;
	}

	return 0;
}
