//
// sample_file/filelistloader.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "filelistloader.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"

namespace ragesamples {

using namespace rage;

//#############################################################################

sfiFileListLoader::LoadFileFunctor sfiFileListLoader::NullLoadFileFunctor;
sfiFileListLoader::SaveFileFunctor sfiFileListLoader::NullSaveFileFunctor;

sfiFileListLoader::LoadTreeNodeFunctor sfiFileListLoader::NullLoadTreeNodeFunctor;
sfiFileListLoader::SaveTreeNodeFunctor sfiFileListLoader::NullSaveTreeNodeFunctor;

atMap<atString, sfiFileListLoader::FileFunctorStruct> sfiFileListLoader::s_FileFunctorsMap;
atMap<atString, sfiFileListLoader::TreeNodeFunctorStruct> sfiFileListLoader::s_TreeNodeFunctorsMap;

const int sfiFileListLoader::s_fileVersion = 1;
const int sfiFileListLoader::s_treeNodeVersion = 1;

sfiFileListLoader* sfiFileListLoader::s_pInstance = NULL;

//#############################################################################

sfiFileListLoader::sfiFileListLoader()
{
    NullLoadFileFunctor = MakeFunctorRet( NoLoadFile );
    NullSaveFileFunctor = MakeFunctorRet( NoSaveFile );

    NullLoadTreeNodeFunctor = MakeFunctorRet( NoLoadTreeNode );
    NullSaveTreeNodeFunctor = MakeFunctorRet( NoSaveTreeNode );
}

//#############################################################################

sfiFileListLoader::~sfiFileListLoader()
{
    s_FileFunctorsMap.Kill();
    s_TreeNodeFunctorsMap.Kill();
}

//#############################################################################

bool sfiFileListLoader::LoadFile( const char *pFile )
{
    Assert( pFile );
    fiStream *S = ASSET.Open( pFile, "filist" );
    if ( !S )
    {
        return false;
    }

    fiTokenizer T( pFile, S );

    // read version
    int version = T.MatchInt( "version" );
    if ( version != s_fileVersion )
    {
        Displayf( "Wrong version of '%s' found.", pFile );
        return false;
    }

    char fileType[64];
    char fileName[256];

    // keep reading name/value pairs until the 'end' token is reached.
    while ( 1 )
    {
        // read token
        T.GetToken( fileType, sizeof(fileType) );
        T.GetToken( fileName, sizeof(fileName) );

        if ( stricmp(fileType,"end") == 0 )
        {
            break;
        }

        // look up load function
        const FileFunctorStruct *pFuncs = s_FileFunctorsMap.Access( fileType );
        if ( pFuncs == NULL )
        {
            Displayf( "%s not found in s_FileFunctorsMap", fileType );
            continue;
        }

        // call load function
        bool success = pFuncs->fLoadFileFunctor( pFuncs->pObj, fileName );
        if ( success )
        {
            Displayf( "%s : %s Load Success!", fileType, fileName );
        }
        else
        {
            Displayf( "%s : %s Load Failed.", fileType, fileName );
        }
    }

    S->Close();

    return true;
}

//#############################################################################

bool sfiFileListLoader::SaveFile( const char *pFile )
{
    fiStream *S = ASSET.Create( pFile, "filist" );
    if ( !S )
    {
        return false;
    }

    // write version
    fiTokenizer T( pFile, S );
    T.PutStrLine( "version %d", s_fileVersion );

    // go through all of the registered functor types
    atMap<atString,FileFunctorStruct>::Iterator i = s_FileFunctorsMap.CreateIterator();
    i.Start();
    while ( !i.AtEnd() )
    {
        const FileFunctorStruct funcs = i.GetData();

        // if our save functor returns a filename, write it to our fiList file
        rage::atString fileName = funcs.fSaveFileFunctor( funcs.pObj, pFile );
        const char *pKey = i.GetKey();
        if ( fileName.GetLength() > 0 )
        {
            T.PutStrLine( "%s %s", pKey, (const char *)fileName );

            Displayf( "%s : %s Save Success!", pKey, (const char *)fileName );
        }
        else
        {
            Displayf( "%s Save Failed.", pKey );
        }

        i.Next();
    }

    T.PutStrLine( "end" );

    S->Flush();
    S->Close();

    return false;
}

//#############################################################################

void sfiFileListLoader::RegisterFile( const char *param, void *pObj, 
    const LoadFileFunctor &fLoadFileFunctor, const SaveFileFunctor &fSaveFileFunctor )
{
    FileFunctorStruct funcs = { pObj, fLoadFileFunctor, fSaveFileFunctor };
    s_FileFunctorsMap[atString(param)] = funcs;
}

//#############################################################################

bool sfiFileListLoader::LoadTree( const char *pFile )
{
    parTree* pTree = PARSER.LoadTree( pFile, "xml" );

    if ( pTree && LoadFromTree(pTree) )
    {
		delete pTree;
        return true;
    }
	delete pTree;
    return false;
}

//#############################################################################

bool sfiFileListLoader::SaveTree( const char *pFile )
{
	bool succeeded = false;
    parTree *pTree = rage_new parTree;

    if ( SaveToTree(pTree) )
    {
		succeeded = PARSER.SaveTree( pFile, "xml", pTree, parManager::XML );
    }
	delete pTree;

    return succeeded;
}

//#############################################################################

const parTreeNode* sfiFileListLoader::LoadFromTree( const parTree *pTree )
{
    parTreeNode *pRootNode = const_cast<parTree *>(pTree)->GetRoot();

    // verify this is the right type of file
    if ( strcmp(pRootNode->GetElement().GetName(),"fiXml") != 0 )
    {
        Errorf( "Failed to load fiXml file, file is not a valid fiXml file." );
        return NULL;
    }

    // verify the version number
    parAttribute* pRootAtt = pRootNode->GetElement().FindAttribute( "version" );
    Assert(pRootAtt);
    if ( pRootAtt )
    {
        if ( pRootAtt->FindIntValue() != s_treeNodeVersion )
        {
            Displayf( "Incorrect version %d.  Expected %d", pRootAtt->FindIntValue(), s_treeNodeVersion );
            return NULL;
        }
    }
    else
    {
        Displayf( "Unknown version number" );
        return NULL;
    }

    // now load each sibling node
    const parTreeNode *pNode = pRootNode->GetChild();
    while ( pNode )
    {
        const TreeNodeFunctorStruct *pFuncs = s_TreeNodeFunctorsMap.Access( pNode->GetElement().GetName() );
        if ( pFuncs == NULL )
        {
            Displayf( "%s not found in s_TreeNodeFunctorsMap", pNode->GetElement().GetName() );
            pNode = pNode->GetSibling();
            continue;
        }

        // call load function
        bool success = pFuncs->fLoadTreeNodeFunctor( pFuncs->pObj, pNode );
        if ( success )
        {
            Displayf( "%s Load Success!", pNode->GetElement().GetName() );
        }
        else
        {
            Displayf( "%s Load Failed.", pNode->GetElement().GetName() );
        }

        pNode = pNode->GetSibling();
    }

    return pNode;
}

//#############################################################################

parTreeNode* sfiFileListLoader::SaveToTree( parTree *pTree )
{
    // create root node, if needed
    parTreeNode *pRootNode = pTree->GetRoot();
    if ( pRootNode == NULL )
    {
        pRootNode = pTree->CreateRoot();
        pTree->SetRoot( pRootNode );
    }

    // set root name and version
    pRootNode->GetElement().SetName( "fiXml" );
    pRootNode->GetElement().AddAttribute( "version", s_treeNodeVersion );

    // go through every entry in the map and call its save function
    atMap<atString,TreeNodeFunctorStruct>::Iterator i = s_TreeNodeFunctorsMap.CreateIterator();
    i.Start();
    while ( !i.AtEnd() )
    {
        const TreeNodeFunctorStruct funcs = i.GetData();

        // create the node from which the callback can hang its children
        parTreeNode *pNode = rage_new parTreeNode;
        pNode->GetElement().SetName( (const char*)i.GetKey() );
        
        bool success = funcs.fSaveTreeNodeFunctor( funcs.pObj, pNode );
        if ( success )
        {
            // success, so insert this node in the tree
            pNode->AppendAsChildOf( pRootNode );

            Displayf( "%s Save Success!", pNode->GetElement().GetName() );
        }
        else
        {
            // failure so delete the node
            delete pNode;
            Displayf( "%s Save Failed.", (const char*) i.GetKey() );
        }

        i.Next();
    }

    return pRootNode;
}

//#############################################################################

void sfiFileListLoader::RegisterTreeNode( const char *param, void *pObj, 
    const LoadTreeNodeFunctor &fLoadTreeNodeFunctor, const SaveTreeNodeFunctor &fSaveTreeNodeFunctor )
{
    TreeNodeFunctorStruct funcs = { pObj, fLoadTreeNodeFunctor, fSaveTreeNodeFunctor };
    s_TreeNodeFunctorsMap[atString(param)] = funcs;
}

//#############################################################################

bool sfiFileListLoader::NoLoadFile( void *, const char * )
{ 
    Printf( "No load function provided.  " ); 
    return false; 
}

//#############################################################################

rage::atString sfiFileListLoader::NoSaveFile( void *, const char * ) 
{ 
    Printf( "No save function provided.  " ); 
    return atString(""); 
}

//#############################################################################

bool sfiFileListLoader::NoLoadTreeNode( void *, const parTreeNode * )
{
    Printf( "No load function provided.  " ); 
    return false; 
}

//#############################################################################

bool sfiFileListLoader::NoSaveTreeNode( void *, parTreeNode * )
{
    Printf( "No save function provided.  " ); 
    return false; 
}

//#############################################################################

void sfiFileListLoader::Init()
{
    Assert( s_pInstance == NULL );
    s_pInstance = rage_new sfiFileListLoader;
}

//#############################################################################

void sfiFileListLoader::Terminate()
{
    Assert( s_pInstance != NULL );
    delete s_pInstance;
    s_pInstance = NULL;
}

} // namespace ragesamples
