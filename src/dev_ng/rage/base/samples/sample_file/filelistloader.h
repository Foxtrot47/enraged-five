//
// sample_file/filelistloader.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SAMPLE_FILE_FILELISTLOADER_H
#define SAMPLE_FILE_FILELISTLOADER_H

#include "atl/functor.h"
#include "atl/map.h"
#include "atl/string.h"

namespace rage
{
    class parTree;
    class parTreeNode;
}

namespace ragesamples
{

/************************************************************************/
// sfiFileListLoader
// Idea:
//      Provide a method to save/load a collection of files and/or data
//      from a single file.
// Implementation:
//      The user provides Save and Load Functors and associates them with 
//      a tag that is similar to our commandline parameters.  In this 
//      first pass, the Functors are supposed to save/load their own file 
//      in their own format.  The name of this file is stored in the 
//      master "fiList" file.  The first pass was implemented this way
//      in order to take advantage of many of the save/load functions
//      that have already been written for entities, cameras, lights, etc.
//
//      In the second pass, functionality for save/loading to/from a
//      parTreeNode has been implemented.  The file saves as XML with the
//      .xml extension.  NOTE:  When saving, a parTreeNode is provided
//      to hang your children from.  All you need to know is that you
//      should not change the Name of that node.  To do so would break
//      the loading of the xml file.
/************************************************************************/
class sfiFileListLoader
{
public:
    /// Return bool, parameters: the calling object, fileName to load
    typedef rage::Functor2Ret<bool,void *,const char *> LoadFileFunctor;
    /// Return atString (saved fileName), parameters: the calling object, fileName (minus extension) to save
    typedef rage::Functor2Ret<rage::atString,void *,const char *> SaveFileFunctor;

    static LoadFileFunctor NullLoadFileFunctor;
    static SaveFileFunctor NullSaveFileFunctor;

    /// Return bool, parameters: the calling object, parTreeNode to read from
    typedef rage::Functor2Ret<bool,void*,const rage::parTreeNode*> LoadTreeNodeFunctor;
    /// Return bool, parameters: the calling object, the node to hang your children from (just don't change the name)
    typedef rage::Functor2Ret<bool,void*,rage::parTreeNode*> SaveTreeNodeFunctor;

    static LoadTreeNodeFunctor NullLoadTreeNodeFunctor;
    static SaveTreeNodeFunctor NullSaveTreeNodeFunctor;

private:
    struct FileFunctorStruct
    {
        void *pObj;
        LoadFileFunctor fLoadFileFunctor;
        SaveFileFunctor fSaveFileFunctor;
    };

    struct TreeNodeFunctorStruct
    {
        void *pObj;
        LoadTreeNodeFunctor fLoadTreeNodeFunctor;
        SaveTreeNodeFunctor fSaveTreeNodeFunctor;
    };

    static rage::atMap<rage::atString, FileFunctorStruct> s_FileFunctorsMap;
    static rage::atMap<rage::atString, TreeNodeFunctorStruct> s_TreeNodeFunctorsMap;

    static const int s_fileVersion;
    static const int s_treeNodeVersion;

    static sfiFileListLoader *s_pInstance;

    /*
    PURPOSE
        Empty function for those that don't provide a load file function
    PARAMS
        void *pObj
        const char *pFile: file to load
    RETURNS
        bool: true on success, false otherwise
    */
    static bool NoLoadFile( void *pObj, const char *pFile );

    /*
    PURPOSE
        Empty function for those that don't provide a save file function
    PARAMS
        void *pObj
        const char *pFile: file to save
    RETURNS
        rage::atString: the file that was saved.  "" if none was saved.
    */
    static rage::atString NoSaveFile( void *pObj, const char *pFile );

    /*
    PURPOSE
        Empty function those that don't provide a load tree node function
    PARAMS
        void *pObj
        const parTreeNode* 
    RETURNS
        bool
    */
    static bool NoLoadTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

    /*
    PURPOSE
        Empty function for those that don't provide a save tree node function
    PARAMS
        void *pObj
        parTreeNode*
    RETURNS
        bool
    */
    static bool NoSaveTreeNode( void *pObj, rage::parTreeNode *pTreeNode );

public:
    sfiFileListLoader();
    ~sfiFileListLoader();

    /*
    PURPOSE
        Loads the list file, identifying name-value pairs for loading and calling each LoadFileFunctor
    PARAMS
        const char *pFile
    RETURNS
        bool: true on success, false otherwise
    */
    bool LoadFile( const char *pFile );

    /*
    PURPOSE
        Saves the list file, calling each parameter's SaveFileFunctor
    PARAMS
        const char *pFile
    RETURNS
        bool: true on success, false otherwise
    */
    bool SaveFile( const char *pFile );

    /*
    PURPOSE
        Registers a parameter with an object and its corresponding save and load file functors
    PARAMS
        const atString &
        void *pObj: pointer to the object that owns the Functors
        LoadFileFunctor
        SaveFileFunctor
    */
    static void RegisterFile( const char *param, void *pObj, 
        const LoadFileFunctor &fLoadFileFunctor, const SaveFileFunctor &fSaveFileFunctor );

    /*
    PURPOSE
        Loads the xml tree file, identifying name-value pairs for loading and calling their LoadTreeNodeFunctors
    PARAMS
        const char *pFile
    RETURNS
        bool: true on success, false otherwise
    */
    bool LoadTree( const char *pFile );

    /*
    PURPOSE
        Saves the xml tree file, calling each parameter's SaveTreeNodeFunctor
    PARAMS
        const char *pFile
    RETURNS
        bool: true on success, false otherwise
    */
    bool SaveTree( const char *pFile );

    /*
    PURPOSE
        Loads the xml tree from the parTree, identifying name-value pairs for loading and calling their LoadTreeNodeFunctors
    PARAMS
        parTree *pTree
    RETURNS
       parTreeNode*: last tree node read from
    */
    const rage::parTreeNode* LoadFromTree( const rage::parTree *pTree );

    /*
    PURPOSE
        Saves the xml tree to the parTree, calling each parameter's SaveTreeNodeFunctor
    PARAMS
        parTree *pTree
    RETURNS
        parTreeNode*: last tree node saved to
    */
    rage::parTreeNode* SaveToTree( rage::parTree *pTree );

    /*
    PURPOSE
        Registers a parameter with an object and its corresponding save and load tree node functors
    PARAMS
        const atString &
        void *pObj: pointer to the object that owns the Functors
        LoadTreeNodeFunctor
        SaveTreeNodeFunctor
    */
    static void RegisterTreeNode( const char *param, void *pObj, 
        const LoadTreeNodeFunctor &fLoadTreeNodeFunctor, const SaveTreeNodeFunctor &fSaveTreeNodeFunctor );

    // static functions
    // keep track of the instance
    static void Init();
    static void Terminate();
    static sfiFileListLoader *GetInstance() { return s_pInstance; }
};

#define FILELISTLOADER (sfiFileListLoader::GetInstance())

} // namespace ragesamples

#endif // SAMPLE_FILE_FILELISTLOADER_H
