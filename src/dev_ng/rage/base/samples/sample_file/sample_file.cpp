//
// sample_file/sample_file.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC
#include  <io.h>
#endif

#include "sample_file.h"

#include "diag/tracker.h"
#include "file/asset.h"
#include "file/packfile.h"
#include "file/remote.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "system/bootmgr.h"
#include "system/param.h"

namespace ragesamples {

using namespace rage;

PARAM(archive, "Archive to mount");

filSampleManager::filSampleManager()  : m_pf(0)
{
	RAGE_TRACK(SampleManager);

#if __XENON && !__FINAL
	if (!fiRemoteServerIsRunning && !sysBootManager::IsBootedFromDisc())
		Quitf("Unable to connect to SysTrayRfs on host PC.  Run $(RAGE_DIR)\\base\\bin\\systrayrfs.exe and redeploy your application once");
#endif
}

filSampleManager::~filSampleManager()
{
	delete m_pf;
}

const char* filSampleManager::GetFullAssetPath() const
{
	return ASSET.GetPath();
}

void filSampleManager::SetFullAssetPath(const char* fullPath)
{
	if (fullPath)
	{
		char tempPath[RAGE_MAX_PATH];
		safecpy(tempPath,fullPath,sizeof(tempPath));
		StringNormalize(tempPath,tempPath,sizeof(tempPath));
		char *assets = strstr(tempPath,"/assets/");
        char *lastAssets = NULL;
        while ( assets )
        {
            lastAssets = assets;
            assets = strstr( (assets + 7), "/assets/" );
        }

		if ( lastAssets ) {
			lastAssets[7] = '\0';
			ASSET.SetPath(tempPath);
		}
	}
}

void filSampleManager::MountArchive()
{
    const char *archive = NULL;
    if ( PARAM_archive.Get( archive ) ) 
    {
        MountArchive( archive );
    }
}

void filSampleManager::MountArchive( const char* archive )
{
    if ( m_pf == NULL )
    {
        m_pf = rage_new fiPackfile;
        
        if ( !m_pf->Init( archive, true, fiPackfile::CACHE_HARDDRIVE ) )
        {
            Quitf("Cannot load archive '%s'",archive);
        }

        char dir[RAGE_MAX_PATH];
        ASSET.RemoveNameFromPath( dir, sizeof(dir), archive );
        strcat(dir,"/");
        if ( !fiDevice::Mount( dir, *m_pf, true ) )
        {
            Quitf( "Cannot mount archive '%s' at '%s'", archive, dir );
        }
    }
}

} // namespace ragesamples
