// 
// sample_file/sample_zlib.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE: This sample checks decompression speed of zlib.
//			It uses its own .sln file as a test input.
#include "system/main.h"

#include "file/stream.h"
#include "system/param.h"
#include "system/timer.h"
#include "zlib/zlib.h"

#include "zlib/inflateClient.h"
#define INFLATE_CLIENT	__PS3

using namespace rage;

int Main()
{
	// STEP #1.  Read the data file
	const char *testFile = sysParam::GetArgCount()>1? sysParam::GetArg(1) : "c:\\soft\\rage\\base\\samples\\sample_file\\sample_zlib_2005.sln";

	fiStream *S = fiStream::Open(testFile);
	if (!S) {
		Quitf("Cannot open '%s'",testFile);
	}
	int dataSize = S->Size();
	char *data = rage_new char[dataSize];
	if (S->Read(data, dataSize) != dataSize)
		Quitf("Cannot fully read '%s'",testFile);
	S->Close();

	Displayf("%d bytes in input.",dataSize);

	// STEP #2.  Compress the data
	sysTimer compTimer;

	z_stream c_stream;
	memset(&c_stream,0,sizeof(c_stream));
	if (deflateInit(&c_stream,Z_DEFAULT_COMPRESSION) < 0)
		Quitf("Error in deflateInit");
	char *dest = rage_new char[dataSize];
	c_stream.next_in = (Bytef*)data;
	c_stream.avail_in = (uInt) dataSize;
	c_stream.next_out = (Bytef*) dest;
	c_stream.avail_out = (uInt) dataSize;
	if (deflate(&c_stream, Z_FINISH) < 0)
		Quitf("Error in deflate");
	deflateEnd(&c_stream);
	int destSize = c_stream.total_out;

	Displayf("%d bytes in %fms",destSize,compTimer.GetMsTime());


	char *copy = rage_new char[dataSize+1];
	copy[dataSize] = '\0';	// so puts will terminate

	// STEP #3.  Decompress it again, leaving memory allocation out of the timing.

#if INFLATE_CLIENT
	zlibInflater zli;
	zli.Init();
	zlibInflateState d_stream;
	memset(&d_stream,0,sizeof(d_stream));
#else
	z_stream d_stream;
	memset(&d_stream,0,sizeof(d_stream));
	if (inflateInit(&d_stream) < 0)
		Quitf("Error in inflateInit");
#endif

	sysTimer decompTimer;
	for (int pass=0; pass<10; pass++) {
#if INFLATE_CLIENT
		zli.Reset(d_stream,destSize,dataSize);
#else
		inflateReset(&d_stream);
#endif
		d_stream.next_in = (Bytef*)dest;
		d_stream.avail_in = (uInt) destSize;
		d_stream.next_out = (Bytef*) copy;
		d_stream.avail_out = (uInt) dataSize;
#if INFLATE_CLIENT
		// Start inflate asynchronously
		zli.InflateBegin(d_stream);
		// and for now, just wait for it to finish.
		zli.InflateEnd(d_stream);
#else
		if (inflate(&d_stream, Z_SYNC_FLUSH) < 0)
			Quitf("Error in inflate");
#endif
	}
	int copySize = d_stream.total_out;
	Displayf("%d bytes in %fms (10 times)",copySize,decompTimer.GetMsTime());

#if INFLATE_CLIENT
	zli.Shutdown();
#else
	inflateEnd(&d_stream);
#endif

	//// for debug: puts(copy);

	delete[] copy;
	delete[] dest;
	delete[] data;

	return 0;
}
