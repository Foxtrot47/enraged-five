// 
// sample_file/sample_serialize.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_file.h"

#include "file/serialize.h"
#include "vector/color32.h"
#include "vector/matrix44.h"
#include "vector/matrix34.h"
#include "system/main.h"

using namespace rage;

// Simple class to serialize
class foo {
public:
	foo() : m_A(0.f), m_B(0), m_D(0) { /* empty */ }
	
	~foo() {
		delete[] m_D;
	}
	
	// Disable the global function operator<< overload below to enable this method
	void Serialize(datSerialize &ser) {
		ser << m_A << m_B << datString(m_C, STRING_SIZE) << datString(&m_D, 0);
	}
	
	float m_A;
	int m_B;
	static const int STRING_SIZE = 32;
	char m_C[STRING_SIZE];
	char *m_D;
	Color32 m_E;
	Matrix34 m_F;
	Matrix44 m_G;
};

// This is the serialization function that is used for the "foo" class
inline datSerialize & operator<< ( datSerialize &s, foo &f ) {
	s << f.m_A << datLabel("LabelTest") << f.m_B << datNewLine << datString(f.m_C, foo::STRING_SIZE);
	s << datNewLine << datString(&f.m_D, 0) << datNewLine << f.m_E << datNewLine << f.m_F << datNewLine << f.m_G;
	return s;
}

class fiSampleSerialize : public ragesamples::filSampleManager {
public:
	fiSampleSerialize() {
	}

	~fiSampleSerialize() {
		/* empty */
	}

	void DoTest() {
		// Do the first test with an ASCII file
		ResetTest();
		Displayf("Saving ascii test");
		SaveData(false);
		Displayf("Loading ascii test");
		LoadData();
		Displayf("Validating results...");
		if ( ValidateData() ) {
			Displayf("Success!");
		}
		else {
			Displayf("Serialization error!");
		}

		// Do the same test with a binary file
		ResetTest();
		Displayf("Saving binary test");
		SaveData(true);
		Displayf("Loading binary test");
		LoadData();
		Displayf("Validating results...");
		if ( ValidateData() ) {
			Displayf("Success!");
		}
		else {
			Displayf("Serialization error!");
		}
		
		ResetTest();
	}

	void SaveData(bool binary) {
		fiSerializeTo( "test", "ser", m_Control, binary );
	}

	void LoadData() {
		fiSerializeFrom( "test", "ser", m_Sample );
	}

	bool ValidateData() {
		Assert( m_Control.m_A == m_Sample.m_A );
		Assert( m_Control.m_B == m_Sample.m_B );
		Assert( strcmp(m_Control.m_C, m_Sample.m_C) == 0 );
		Assert( strcmp(m_Control.m_D, m_Sample.m_D) == 0 );
		Assert( m_Control.m_E == m_Sample.m_E );
		Assert( m_Control.m_F.IsEqual(m_Sample.m_F) );
		Assert( m_Control.m_G.a.IsEqual(m_Sample.m_G.a) );
		Assert( m_Control.m_G.b.IsEqual(m_Sample.m_G.b) );
		Assert( m_Control.m_G.c.IsEqual(m_Sample.m_G.c) );
		Assert( m_Control.m_G.d.IsEqual(m_Sample.m_G.d) );

		if ( m_Control.m_A != m_Sample.m_A ) 
			return false;
		if ( m_Control.m_B != m_Sample.m_B )
			return false;
		if ( strcmp(m_Control.m_C, m_Sample.m_C) )
			return false;
		if ( strcmp(m_Control.m_D, m_Sample.m_D) )
			return false;
		if ( m_Control.m_E != m_Sample.m_E ) 
			return false;
		if ( m_Control.m_F.IsNotEqual(m_Sample.m_F) )
			return false;
		if ( m_Control.m_G.a.IsNotEqual(m_Sample.m_G.a) )
			return false;
		if ( m_Control.m_G.b.IsNotEqual(m_Sample.m_G.b) )
			return false;
		if ( m_Control.m_G.c.IsNotEqual(m_Sample.m_G.c) )
			return false;
		if ( m_Control.m_G.d.IsNotEqual(m_Sample.m_G.d) )
			return false;
		return true;		
	}

	void ResetTest() {
		m_Control.m_A = -58.34f;
		m_Control.m_B = 1949;
		strcpy(m_Control.m_C, "testString");
		delete[] m_Control.m_D;
		m_Control.m_D = rage_new char[32];
		strcpy(m_Control.m_D, "Hello World");
		m_Control.m_E.Set(128, 234, 17, 200);
		m_Control.m_F.Identity();
		m_Control.m_G.Identity();
		
		m_Sample.m_A = 0.f;
		m_Sample.m_B = 0;
		memset(m_Sample.m_C, '\0', foo::STRING_SIZE);
		delete[] m_Sample.m_D;
		m_Sample.m_D = 0;
		m_Sample.m_E.Set(0,0,0,0);
		m_Sample.m_F.Zero();
		m_Sample.m_G.Zero();
	}

protected:
	foo		m_Control;
	foo		m_Sample;
};

int Main() {
	fiSampleSerialize ser;
	ser.DoTest();
	return 0;
}
