# -*- makefile -*- Time-stamp: <04/07/25 12:44:54 ptr>
# $Id$

include ${RULESBASE}/${USE_MAKE}/app/macro.mak
include ${RULESBASE}/${USE_MAKE}/app/${COMPILER_NAME}.mak
include ${RULESBASE}/${USE_MAKE}/app/rules.mak
include ${RULESBASE}/${USE_MAKE}/app/rules-install.mak
include ${RULESBASE}/${USE_MAKE}/app/clean.mak
