// 
// teststandardlibrary.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include <iterator>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
//#include <typeinfo>

#include "system/main.h"
#include "atl/array.h"
#include "system/typeinfo.h"
#include "vector/vector3.h"
#include "paging/streamer.h"
#include "paging/rscbuilder.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"



using namespace rage;

void SimpleAlgorithmTest();
void TestSortingAndSearching();
void TestIOStream();
void TestResourcing();
void ShrinkAVector();

namespace rage {
void TypeInfoTest(const type_info & /*type*/)
{
//	wmemmove(0, 0, 0);
}

void AnotherTypeInfoTest(const char *)
{
	
}
}

int Main()
{
	SimpleAlgorithmTest();
	TestSortingAndSearching();
	TestIOStream();
//	TestResourcing();														//If this is commented out it is probably because it creates a file on hard disk.
	ShrinkAVector();

	std::vector<Vector3>				myVector;
	rage::TypeInfoTest(typeid(myVector));
	rage::AnotherTypeInfoTest(typeid(myVector).name());
/*
	std::string			myString;
	myString= "bla";
	myString += " bla";
	Displayf("%s", myString.c_str());
*/
	return 0;
}


void SimpleAlgorithmTest()
{
	Displayf("\n\nstandard C array\n");

	const int					numElements=10;
	int							cArray[numElements];

	for(int i=0; i<numElements; i++)
		cArray[i]= i;

	std::reverse(&cArray[0], &cArray[numElements]);							//Upper limit is one beyond last element as is always the case with STL. Note, this does not actualy result in an out of bounds access.

	Displayf("\niterating from first to last element using indices:\n");
	for(int i=0; i<numElements; i++)
		Displayf("index: %d, value: %d", i, cArray[i]);


	Displayf("\n\nstd::vector\n");

	typedef std::vector<int>	Integer_vector;
	Integer_vector				intStdArray;
	const Integer_vector		&constIntStdArray= intStdArray;

	intStdArray.resize(numElements);

	for(int i=0; i<numElements; i++)
		intStdArray[i]= i;

	std::reverse(intStdArray.begin(), intStdArray.end());							//NOTE: end() returns iterator pointing just beyond last element.

	//Displayf("\niterating from first to last element using indices:\n");
	//for(int i=0; i<numElements; i++)
	//	Displayf("index: %d, value: %d", i, intStdArray[i]);

	//Displayf("\niterating from first to last element using forward iterator:\n");
	//for(Integer_vector::iterator it=intStdArray.begin(); it!=intStdArray.end(); it++)
	//	Displayf("value: %d", *it);

	Displayf("\niterating from last to first element using reverse iterator:\n");
	for(Integer_vector::reverse_iterator rit=intStdArray.rbegin(); rit!=intStdArray.rend(); rit++)
		Displayf("value: %d", *rit);

	Displayf("\niterating from last to first element using const reverse iterator:\n");
	for(Integer_vector::const_reverse_iterator rit=constIntStdArray.rbegin(); rit!=constIntStdArray.rend(); rit++)
		Displayf("value: %d", *rit);
}

void TestSortingAndSearching()
{
	typedef atArray<int>		Integer_atArray;
	Integer_atArray				intAtArray, intAtArray2;

	intAtArray.Reserve(10);
	intAtArray.Push(4);
	intAtArray.Push(7);
	intAtArray.Push(2);
	intAtArray.Push(3);
	intAtArray.Push(8);
	intAtArray.Push(1);
	intAtArray.Push(9);
	intAtArray.Push(0);
	intAtArray.Push(5);
	intAtArray.Push(6);
	
	Integer_atArray::iterator		result;
	result= std::find(intAtArray.begin(), intAtArray.end(), 9);									//find() operates on unsorted data.
	AssertVerify(result!=intAtArray.end() && *result==9);

	std::sort(intAtArray.begin(), intAtArray.begin()+5);										//Sort first half of array.
	std::sort(intAtArray.begin()+5, intAtArray.end());											//Sort second half of array.

	intAtArray2.Resize(10);																		//std::merge() expects the destination range to be already existent. 
	std::merge(intAtArray.begin(), intAtArray.begin()+5, intAtArray.begin()+5, intAtArray.end(), intAtArray2.begin());	//Merge 2 sorted ranges into one sorted range.
	std::random_shuffle(intAtArray2.begin(), intAtArray2.end());

	std::sort(intAtArray.begin(), intAtArray.end());											//Sort whole thing.

	AssertVerify(std::binary_search(intAtArray.begin(), intAtArray.end(), 9));					//Looks for the existence of the value in the range.
	AssertVerify(std::binary_search(intAtArray.begin(), intAtArray.end(), 11)==false);

	result= std::lower_bound(intAtArray.begin(), intAtArray.end(), 7);							//Search within ordered range. Actually searches for the first element that's equal or greater than 7.
	AssertVerify(result!=intAtArray.end() && *result==7);

	result= std::upper_bound(intAtArray.begin(), intAtArray.end(), 4);							//Search within ordered range. Searches for the first element that's greater than 4.
	AssertVerify(result!=intAtArray.end() && *(result-1)==4);
}



void TestIOStream()
{
	std::vector<int>		intVector;

	intVector.push_back(1);
	intVector.push_back(2);
	intVector.push_back(3);
//	std::cout << "bla" << std::endl;
//	std::cout << 'j';

    std::stringstream		debugText;
	debugText << "Hello World" << std::endl;
    std::copy(  intVector.begin(), intVector.end(), std::ostream_iterator<int>(debugText, "<->"));
	debugText << "end" << std::endl;
	Printf(debugText.str().c_str());

/*
	std::ofstream		osData("t:\\hallo.txt");
	if(osData)
	{
		osData << "Hallo Welt" << std::endl;
		osData.close();
	}

	std::ifstream		isData("c:\\hallo.txt");
	if(isData)
	{
		std::string		tmpString;
		while(!isData.eof())
		{
			isData >> tmpString;
			Printf(tmpString.c_str());
		}
	}*/
}

typedef std::vector<int>		intVector;

struct RscData: public pgBase
{
	intVector					*intVectorPtr;
	std::string					*shortStringPtr;
	std::string					*longStringPtr;

	IMPLEMENT_PLACE_INLINE(RscData)

	RscData(rage::datResource & /*rsc*/) 
	{
		datResource::Place(intVectorPtr);
		datResource::Place(shortStringPtr);
		datResource::Place(longStringPtr);
	}

	RscData(){};
};


void TestResourcing()
{
	const char						*fileName= "t:\\test";
	const int						version= 1;
	const int						numElements= 10;
	const char						*shortTestString= "Willi Wonka";		//Short enough so it fits into the local string buffer
	const char						*longTestString= "Billy Donka sits on the throne and plays with his ...";


#if !__FINAL
	//Create resources with dummy data:
//  bool needsRebuild = PARAM_regen.Get() || !pgRscBuilder::CheckBuild(srcFile,fileName, version);
  for (int pass=0; pass<2; pass++)
  {
		pgRscBuilder::BeginBuild(pass?true:false,1 * 1024 * 1024);

		RscData						*srcDataPtr= new RscData;

		srcDataPtr->intVectorPtr= new intVector;
		intVector					&tmpVector= *srcDataPtr->intVectorPtr;
		tmpVector.resize(numElements);
		for(int i=0; i<numElements; i++)
			tmpVector[i]= i;

		srcDataPtr->shortStringPtr= new std::string(shortTestString);
		srcDataPtr->longStringPtr= new std::string(longTestString);

		if (pass == 1)
			pgRscBuilder::SaveBuild(fileName, "#test", version);
//	    pgRscBuilder::DisableDelete();
		delete srcDataPtr->intVectorPtr;
		delete srcDataPtr->shortStringPtr;
		delete srcDataPtr->longStringPtr;
		delete srcDataPtr;
//	    pgRscBuilder::EnableDelete();
		pgRscBuilder::EndBuild(srcDataPtr);
  }
#endif // __FINAL


	//Read back the resources and check if the data is what we expect:
	RscData						*dstDataPtr;

	pgRscBuilder::Load(dstDataPtr, fileName, "#test", 1);
	Assert(dstDataPtr);

	for(int i=0; i<numElements; i++)
	{
		Displayf("value: %d", (*dstDataPtr->intVectorPtr)[i]);
		Assert((*dstDataPtr->intVectorPtr)[i] == i);
	}

	//The following tries to change a vector that has been resourced -> it will
	//cause a memory leak or heap corruption:
/*	dstDataPtr->intVectorPtr->reserve(100);
	for(int i=0; i<numElements; i++)
	{
		intVector						&tmpVector= *dstDataPtr->intVectorPtr;
//		Displayf("value: %d", tmpVector[i]);
		Assert(tmpVector[i] == i);
	}*/

	Displayf(dstDataPtr->shortStringPtr->c_str());
	Assert(strcmp(shortTestString, dstDataPtr->shortStringPtr->c_str())==0);

	Displayf(dstDataPtr->longStringPtr->c_str());
	Assert(strcmp(longTestString, dstDataPtr->longStringPtr->c_str())==0);


	//Call into the DeclareStruct() functions. Will not do much but it will
	//print out if padding bytes are missing or if member variables are
	//declared in the wrong order.
	datTypeStruct s;
	dstDataPtr->intVectorPtr->DeclareStruct(s);
	datTypeStruct t;
	dstDataPtr->shortStringPtr->DeclareStruct(t);
	datTypeStruct u;
	dstDataPtr->longStringPtr->DeclareStruct(u);
/*
	//Again the following tries to change a class that has been resourced -> it will
	//cause a memory leak or heap corruption:
	//Test if the string classes are still functional:
	*dstDataPtr->shortStringPtr= std::string("AnotherString");
	Displayf(dstDataPtr->shortStringPtr->c_str());
	Assert(strcmp("AnotherString", dstDataPtr->shortStringPtr->c_str())==0);

	const char					*anotherLongString= "Another really long string that doesn't really mean anything";
	*dstDataPtr->longStringPtr= std::string(anotherLongString);
	Displayf(dstDataPtr->longStringPtr->c_str());
	Assert(strcmp(anotherLongString, dstDataPtr->longStringPtr->c_str())==0);
*/
}

/*
	Demonstrates swap trick to shrink a vector.

	Asserts out if the swap trick doesn't work.

	NOTE: the swap trick is not guaranteed to work with all STL
	implementations but it should with most and definitely with the version
	in RAGE.
*/
void ShrinkAVector()
{
	std::vector<int>			intVector;
	Displayf("After constructor - capcity(): %d, size(): %d", int(intVector.capacity()), int(intVector.size()));
	intVector.reserve(10);
	Displayf("After reserve(10) - capcity(): %d, size(): %d", int(intVector.capacity()), int(intVector.size()));
	Assert(intVector.capacity()>=10);
	intVector.reserve(0);																		//Will not free any memory.
	intVector.clear();																			//Will not free any memory.
	Displayf("After reserve(0) and clear() - capcity(): %d, size(): %d", int(intVector.capacity()), int(intVector.size()));

	std::vector<int>().swap(intVector);
	Displayf("After swap trick - capcity(): %d, size(): %d", int(intVector.capacity()), int(intVector.size()));
	Assert(intVector.capacity()==0);															//This is not guaranteed behavior by all STL implementations, but it should work with the rage version and most others.


	intVector.reserve(10);
	intVector.resize(7);
	Displayf("After reserve(10) and resize(7) - capcity(): %d, size(): %d", int(intVector.capacity()), int(intVector.size()));
	std::vector<int>(intVector).swap(intVector);												//Causes all elements to be copied.
	Displayf("After swap trick - capcity(): %d, size(): %d", int(intVector.capacity()), int(intVector.size()));
	Assert(intVector.capacity()==7);															//This is not guaranteed behavior by all STL implementations, but it should work with the rage version and most others.
}
