// 
// stlport/rageerror.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include <stdarg.h>

// Outside of namespace rage
void WIN32_ONLY(__cdecl) __stl_debug_message(const char* ASSERT_ONLY(fmt),...)
{
#if __ASSERT
	va_list args;
	va_start(args,fmt);
	const char* expr = "STL Assertion";
	const char* file = "<unknown>";
	int line = -1;
	file = va_arg(args, const char*);
	line = va_arg(args, int);
	expr = va_arg(args, const char*);
	va_end(args);

	rage::diagAssertHelper(/*DIAG_SEVERITY_FATAL*/file,line,"%s",expr) || (__debugbreak(),0);
#endif
}

void WIN32_ONLY(__cdecl) __stl_debug_terminate()
{
	// do nothing. __stl_debug_message will handle termination
}
