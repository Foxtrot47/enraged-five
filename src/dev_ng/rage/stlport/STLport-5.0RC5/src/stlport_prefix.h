#ifndef STLPORT_PREFIX_H
#define STLPORT_PREFIX_H

#define __BUILDING_STLPORT 1

#if defined(_MSC_VER)
#  include "vc_warning_disable.h"
#  if defined(_XBOX_VER)
#    pragma warning( disable : 4062 )										//Suppress warning in d3d9.h: enumerator 'D3DRS_FORCE_DWORD' in switch of enum '_D3DRENDERSTATETYPE' is not handled
#    if !defined(_XBOX)
#      define _XBOX 1
#    endif
#  else
#    if !defined(_WINDOWS)
#      define _WINDOWS 1
#    endif
#  endif
#endif //_MSC_VER

#if defined (_WIN32) || defined (WIN32)
#  ifdef __cplusplus
#    ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN
#    endif
#    define NOSERVICE
#  endif
#  if !(defined (__CYGWIN__) || defined (_WIN32_WCE))
#    define _STLP_REAL_LOCALE_IMPLEMENTED
#  endif
#endif

#undef _STLP_NO_FORCE_INSTANTIATE

/* Please add extra compilation switches for particular compilers here */

#include <stl/_config.h>

#if defined (_STLP_USE_TEMPLATE_EXPORT) && defined (_STLP_USE_DECLSPEC) && !defined (_STLP_EXPOSE_GLOBALS_IMPLEMENTATION)
#  define _STLP_EXPOSE_GLOBALS_IMPLEMENTATION
#endif

#ifdef __cplusplus

#  include <ctime>
#  if defined (_STLP_USE_NAMESPACES) && !defined (_STLP_VENDOR_GLOBAL_CSTD)
using _STLP_VENDOR_CSTD::time_t;
#  endif

#  if defined (_STLP_FUNCTION_TMPL_PARTIAL_ORDER) || defined (__BORLANDC__)
#    define _STLP_OPERATOR_SPEC _STLP_DECLSPEC 
#  else
#    define _STLP_OPERATOR_SPEC _STLP_TEMPLATE_NULL _STLP_DECLSPEC
#  endif

#endif /* __cplusplus */

#endif /* PREFIX */


