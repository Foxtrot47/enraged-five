Project stlport

IncludePath $(RAGE_DIR)\base\src

Files {
	stdio_streambuf.cpp
	stdio_streambuf.h
	num_put.cpp
	num_put.h
	locale_impl.cpp
	locale_impl.h
	c_locale.cpp
	c_locale.h
	dll_main.cpp
	fstream.cpp
	strstream.cpp
	sstream.cpp
	ios.cpp
	istream.cpp
	ostream.cpp
	iostream.cpp
	codecvt.cpp
	collate.cpp
	ctype.cpp
	monetary.cpp
	num_get.cpp
	num_get_float.cpp
	num_put_float.cpp
	numpunct.cpp
	time_facets.cpp
	messages.cpp
	locale_catalog.cpp
	facets_byname.cpp
	complex.cpp
	complex_exp.cpp
	complex_io.cpp
	complex_trig.cpp
	complex_io_w.cpp
	string_w.cpp
	rage_err.cpp
}
