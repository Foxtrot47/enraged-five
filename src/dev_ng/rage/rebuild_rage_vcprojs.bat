@echo off
pushd %~dp0
set started=%time% 
@echo STARTED : %started%

call setenv

if "%1"=="VS2008" goto VS2008

@echo.
@echo.
@echo.
@echo ****************************************************************************
@echo ****************************************************************************
@echo *** VS2010 Project generation started ( New Project Generator ) %time% 
@echo *** https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 
@echo ****************************************************************************
@echo ****************************************************************************
@echo.

call %RS_TOOLSROOT%\script\util\projGen\sync.bat

set build_script=%RS_TOOLSROOT%\script\util\projgen\rebuildGameLibrary.bat

call %build_script% 	%RAGE_DIR%\base\src\vcproj\RageAudio\RageAudio.txt ^
			%RAGE_DIR%\base\src\vcproj\RageCore\RageCore.txt ^
			%RAGE_DIR%\base\src\vcproj\RageCreature\RageCreature.txt ^
			%RAGE_DIR%\base\src\vcproj\RageNet\RageNet.txt ^
			%RAGE_DIR%\base\src\vcproj\RagePhysics\RagePhysics.txt ^
			%RAGE_DIR%\script\src\vcproj\RageScript\RageScript.txt ^
			%RAGE_DIR%\suite\src\vcproj\RageSuiteCreature\RageSuiteCreature.txt ^
			%RAGE_DIR%\suite\src\vcproj\RageSuiteMisc\RageSuiteMisc.txt ^
			%RAGE_DIR%\stlport\STLport-5.0RC5\src\makefile.txt ^
			%RAGE_DIR%\framework\src\vcproj\RageFramework\RageFramework.txt^
			%RAGE_DIR%\scaleform\src\vcproj\Scaleformgfx\Scaleformgfx.txt ^
			%RAGE_DIR%\naturalmotion\vcproj\NaturalMotion\NaturalMotion.txt

pushd base\src\vcproj\RageGraphics
call rebuild_vcproj.bat VS2010
popd

@echo.
@echo ****************************************************************************
@echo ****************************************************************************
@echo *** VS2010 Project generation complete %time%
@echo ****************************************************************************
@echo ****************************************************************************
@echo.
@echo.
@echo.
if "%1"=="VS2010" goto END

:VS2008
@echo 2008 Project generation started ( Old Project Generator )
@echo https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder

pushd base\src\vcproj\RageAudio
@call rebuild_vcproj.bat VS2008
popd

pushd base\src\vcproj\RageCore
call rebuild_vcproj.bat VS2008
popd

pushd base\src\vcproj\RageCreature
call rebuild_vcproj.bat VS2008
popd

pushd base\src\vcproj\RageGraphics
call rebuild_vcproj.bat VS2008
popd

pushd base\src\vcproj\RageGraphics2
call rebuild_vcproj.bat VS2008
popd

pushd base\src\vcproj\RageNet
call rebuild_vcproj.bat VS2008
popd

pushd base\src\vcproj\RagePhysics
call rebuild_vcproj.bat VS2008
popd

pushd script\src\vcproj\RageScript
call rebuild_vcproj.bat VS2008
popd

pushd suite\src\vcproj\RageSuiteCreature
call rebuild_vcproj.bat VS2008
popd

pushd suite\src\vcproj\RageSuiteMisc
call rebuild_vcproj.bat VS2008
popd

pushd stlport\STLport-5.0RC5\src
call rebuild_vcproj.bat VS2008
popd

pushd framework\src\vcproj\RageFramework
call rebuild_vcproj.bat VS2008
popd

pushd scaleform\src\vcproj\Scaleformgfx
@call rebuild_vcproj.bat VS2008
popd

pushd naturalmotion\vcproj\NaturalMotion
@call rebuild_vcproj.bat VS2008
popd

:END
@echo STARTED  : %started% 
@echo FINISHED : %time% 
