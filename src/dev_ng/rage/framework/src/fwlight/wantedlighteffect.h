//
// fwlight/wantedlighteffect.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef FWLIGHT_WANTEDLIGHTEFFECT_H_
#define FWLIGHT_WANTEDLIGHTEFFECT_H_

#include "output/lighteffect.h"

namespace rage
{

// PURPOSE: Represents a wanted lighting effect.
// NOTES:   Avoid triggering seizure. (5Hz-70Hz, which is 14ms to 200ms, is bad,
//          http://www.birket.com/technical-library/144)
//          ioLightEffects are stateless and should not maintain state between calls
//          to Update(). This is so an ioLightEffect can be used on multiple devices.
class fwWantedLightEffect : public ioLightEffect
{
public:
	// PURPOSE: Default constructor represents no duration.
	fwWantedLightEffect();

	// PURPOSE: Constructor.
	// PARAMS:	duration - the duration of one cycle of the effect.
	fwWantedLightEffect(u32 duration);
	
	// PURPOSE:	Destructor.
	virtual ~fwWantedLightEffect() {}
	
	// PURPOSE: Update the light effect.
	// PARAMS:  progress - the progress through the light effect.
	//          device - the device the apply the effect to.
	// NOTES:   Update() is marked as a const method as ioLightEffects are stateless
	//          so the same affect can be applied to multiple devices.
	virtual void Update(float progress, ioLightDevice* device) const;
};

inline fwWantedLightEffect::fwWantedLightEffect()
	: ioLightEffect(0)
{
}

inline fwWantedLightEffect::fwWantedLightEffect(u32 duration)
	: ioLightEffect(duration)
{
}

}

#endif // FWLIGHT_WANTEDLIGHTEFFECT_H_
