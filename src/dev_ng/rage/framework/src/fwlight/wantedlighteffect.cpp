//
// fwlight/wantedlighteffect.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "wantedlighteffect.h"

// rage headers.
#include "output/lightdevice.h"
#include "vector/color32.h"

namespace rage
{

void fwWantedLightEffect::Update(float progress, ioLightDevice* device) const
{
	if(device != NULL)
	{
		const static Color32 RED(251, 1, 2, 255);
		const static Color32 BLUE(1, 2, 251, 255);
		
		// If there are left and right lights then alternate these to red and blue.
		if(device->HasLightSource(ioLightDevice::LEFT) && device->HasLightSource(ioLightDevice::RIGHT))
		{
			if(progress < 0.5f)
			{
				device->SetLightColor(ioLightDevice::LEFT, RED);
				device->SetLightColor(ioLightDevice::RIGHT, BLUE);
			}
			else
			{
				device->SetLightColor(ioLightDevice::LEFT, BLUE);
				device->SetLightColor(ioLightDevice::RIGHT, RED);
			}
		}
		else
		{
			// Fall back to setting every light the same color.
			Color32 current;
			if(progress < 0.5f)
			{
				current = RED;
			}
			else
			{
				current = BLUE;
			}
			
			for(u32 i = 0; i < ioLightDevice::MAX_LIGHT_SOURCES; ++i)
			{
				device->SetLightColor(static_cast<ioLightDevice::Source>(i), current);
			}
		}

		device->Update();
	}
}

}