#include "audmesh.h"
    

#include "file\stream.h"
#include "ai\navmesh\NavMesh.h"
#include "fwsys\fileExts.h"
#include "paging\rscbuilder.h"
#include "string\stringhash.h"
#include "grcore\debugdraw.h"

#ifdef GTA_ENGINE
//#include "debug\debug.h"
#ifndef RAGEBUILDER
//#include "debug/DebugDraw.h"
#endif // RAGEBUILDER
#endif

//#pragma optimize("", off)

namespace rage
{

static inline void vcopy(float* dest, const float* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}

void agPolyMesh::Place(agPolyMesh * that, datResource & rsc)
{
	::new (that) agPolyMesh(rsc);
}

agPolyMesh::agPolyMesh(datResource& rsc)
{
	rsc.PointerFixup(verts);
	rsc.PointerFixup(polys);
	rsc.PointerFixup(regs);
	rsc.PointerFixup(conns);
	rsc.PointerFixup(rcindexs);
	rsc.PointerFixup(localStitches[0]);
	rsc.PointerFixup(localStitches[1]);
	rsc.PointerFixup(localStitches[2]);
	rsc.PointerFixup(localStitches[3]);
	rsc.PointerFixup(neighbourStitches[0]);
	rsc.PointerFixup(neighbourStitches[1]);
	rsc.PointerFixup(neighbourStitches[2]);
	rsc.PointerFixup(neighbourStitches[3]);
	rsc.PointerFixup(nrconns);
	rsc.PointerFixup(regFlags);
}

agPolyMesh * agPolyMesh::Load(const char * pfileName)
{
	fiStream * pFile = fiStream::Open(pfileName);
	if(pFile)
	{
		agPolyMesh *mesh = rage_new agPolyMesh;
		if(LoadBinary(pFile, *mesh))
		{
			pFile->Close();
			return mesh;
		}

		pFile->Close();

		delete mesh;
	}

	return NULL;
}

#if __DECLARESTRUCT //&& !defined(NAVGEN_TOOL)
void agPolyMesh::DeclareStruct(datTypeStruct & s)
{
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(agPolyMesh);
		STRUCT_DYNAMIC_ARRAY(verts, nverts*3);
		STRUCT_DYNAMIC_ARRAY(polys, npolys*nvp*2);
		STRUCT_DYNAMIC_ARRAY(regs, npolys);
		STRUCT_DYNAMIC_ARRAY(conns, nconns);
		STRUCT_DYNAMIC_ARRAY(rcindexs, nregs);
		STRUCT_DYNAMIC_ARRAY(localStitches[0], nstitches[0]);
		STRUCT_DYNAMIC_ARRAY(localStitches[1], nstitches[1]);
		STRUCT_DYNAMIC_ARRAY(localStitches[2], nstitches[2]);
		STRUCT_DYNAMIC_ARRAY(localStitches[3], nstitches[3]);
		STRUCT_DYNAMIC_ARRAY(neighbourStitches[0], nstitches[0]);
		STRUCT_DYNAMIC_ARRAY(neighbourStitches[1], nstitches[1]);
		STRUCT_DYNAMIC_ARRAY(neighbourStitches[2], nstitches[2]);
		STRUCT_DYNAMIC_ARRAY(neighbourStitches[3], nstitches[3]);
		STRUCT_DYNAMIC_ARRAY(nrconns, nregs);
		STRUCT_DYNAMIC_ARRAY(regFlags, nregs);
		STRUCT_FIELD(nverts);
		STRUCT_FIELD(npolys);
		STRUCT_FIELD(nvp);
		STRUCT_FIELD(nconns);
		STRUCT_CONTAINED_ARRAY(bmin);
		STRUCT_CONTAINED_ARRAY(bmax);
		STRUCT_FIELD(cs);
		STRUCT_FIELD(ch);
		STRUCT_FIELD(nregs);
		STRUCT_CONTAINED_ARRAY(nstitches);
		STRUCT_IGNORE(padding);
	STRUCT_END();
}
#endif

bool agPolyMesh::LoadBinary(fiStream * pFile, agPolyMesh & mesh)
{
	if(mesh.verts || mesh.polys || mesh.regs || mesh.conns || mesh.rcindexs || mesh.nrconns)
	{
		Assertf(0, "agLoadSurfaceBinary: input mesh must be unpopulated");
		return false;
	}
	
	char header[8] = {0};

	pFile->Read(header, 8);

	//Read flags
	u32 flags=0;
	pFile->ReadInt(&flags, 1);

	//Read num verts
	u32 numVerts=0;
	pFile->ReadInt(&numVerts, 1);

	//Read num polys
	u32 numPolys=0;
	pFile->ReadInt(&numPolys, 1);

	//Read num regions
	u16 numRegs=0;
	pFile->Read(&numRegs, 2);
	ByteSwap16(&numRegs);

	//Read num vertexes/polygon
	u32 nvp = 0;
	pFile->ReadInt(&nvp, 1);

	//Read num connections
	u32 numConns=0;
	pFile->ReadInt(&numConns, 1);

	//Read bounding box
	float bmin[3] = {0}, bmax[3] = {0};
	pFile->ReadFloat(&bmin[0], 1);
	pFile->ReadFloat(&bmin[1], 1);
	pFile->ReadFloat(&bmin[2], 1);
	pFile->ReadFloat(&bmax[0], 1);
	pFile->ReadFloat(&bmax[1], 1);
	pFile->ReadFloat(&bmax[2], 1);

	//Read cell dimensions
	float cellSize = 0.f;
	pFile->ReadFloat(&cellSize, 1);
	float cellHeight = 0;
	pFile->ReadFloat(&cellHeight, 1);

	//******************************************************
	//Allocate arrays
	
	mesh.nverts = numVerts;
	mesh.npolys = numPolys;
	mesh.nregs = numRegs;
	mesh.nvp = nvp;
	mesh.nconns = numConns;
	vcopy(mesh.bmin, bmin);
	vcopy(mesh.bmax, bmax);
	mesh.cs = cellSize;
	mesh.ch = cellHeight;

	CNavMeshAlloc<u16> u16Alloc;	
	CNavMeshAlloc<u8> u8Alloc;	

	mesh.verts = u16Alloc.VectorNew(mesh.nverts*3);
	mesh.regs =  u16Alloc.VectorNew(mesh.npolys);
	mesh.polys = u16Alloc.VectorNew(mesh.npolys*mesh.nvp*2);
	mesh.conns = u16Alloc.VectorNew(mesh.nconns);
	mesh.rcindexs = u16Alloc.VectorNew(mesh.nregs);
	mesh.nrconns = u8Alloc.VectorNew(mesh.nregs);

	memset(mesh.verts, 0, sizeof(u16)*numVerts*3);
	memset(mesh.polys, 0, sizeof(u16)*numPolys*nvp*2);
	memset(mesh.regs, 0, sizeof(u16)*numPolys);
	memset(mesh.conns, 0, sizeof(u16)*numConns);
	memset(mesh.rcindexs, 0, sizeof(u16)*numRegs);
	memset(mesh.nrconns, 0, sizeof(u8)*numRegs);
	//*****************************************************
	
	//Read in the verts
	for(u32 v=0; v<numVerts; ++v)
	{
		pFile->Read(&mesh.verts[v*3], 2);
		Assert(mesh.verts[v*3] < 500);
		ByteSwap16(&mesh.verts[v*3]);
		Assert(mesh.verts[v*3] < 500);
		pFile->Read(&mesh.verts[v*3+1], 2);
		ByteSwap16(&mesh.verts[v*3+1]);
		pFile->Read(&mesh.verts[v*3+2], 2);
		Assert(mesh.verts[v*3+2] < 500);
		ByteSwap16(&mesh.verts[v*3+2]);
		Assert(mesh.verts[v*3+2] < 500);
	}

	//Read in the polys
	for(u32 p=0; p<numPolys*nvp; ++p)
	{
		pFile->Read(&mesh.polys[p*2], 2);
		ByteSwap16(&mesh.polys[p*2]);
		pFile->Read(&mesh.polys[p*2+1], 2);
		ByteSwap16(&mesh.polys[p*2+1]);
	}

	//Read in the regs
	for(u32 r=0; r<numPolys; ++r)
	{
		pFile->Read(&mesh.regs[r], 2);
		ByteSwap16(&mesh.regs[r]);
	}

	//read in the connections
	for(u32 c=0; c<numConns; ++c)
	{
		pFile->Read(&mesh.conns[c], 2);
		ByteSwap16(&mesh.conns[c]);
	}

	//read in the region connection indexes
	for(u32 rci=0; rci<numRegs; ++rci)
	{
		pFile->Read(&mesh.rcindexs[rci], 2);
		ByteSwap16(&mesh.rcindexs[rci]);
	}

	//read in the region connection counts
	for(u32 rc=0; rc<numRegs; ++rc)
	{
		pFile->Read(&mesh.nrconns[rc], 1);
	}

	//Read in the stitch counts
	pFile->Read(&mesh.nstitches[0], 2);
	ByteSwap16(&mesh.nstitches[0]);
	pFile->Read(&mesh.nstitches[1], 2);
	ByteSwap16(&mesh.nstitches[1]);
	pFile->Read(&mesh.nstitches[2], 2);
	ByteSwap16(&mesh.nstitches[2]);
	pFile->Read(&mesh.nstitches[3], 2);
	ByteSwap16(&mesh.nstitches[3]);

	//*********
	//Allocate arrays
	if(mesh.nstitches[0])
	{
		mesh.localStitches[0] = u16Alloc.VectorNew(mesh.nstitches[0]);
		mesh.neighbourStitches[0] = u16Alloc.VectorNew(mesh.nstitches[0]);
	}
	if(mesh.nstitches[1])
	{
		mesh.localStitches[1] = u16Alloc.VectorNew(mesh.nstitches[1]);
		mesh.neighbourStitches[1] = u16Alloc.VectorNew(mesh.nstitches[1]);
	}
	if(mesh.nstitches[2])
	{
		mesh.localStitches[2] = u16Alloc.VectorNew(mesh.nstitches[2]);
		mesh.neighbourStitches[2] = u16Alloc.VectorNew(mesh.nstitches[2]);
	}
	if(mesh.nstitches[3])
	{
		mesh.localStitches[3] = u16Alloc.VectorNew(mesh.nstitches[3]);
		mesh.neighbourStitches[3] = u16Alloc.VectorNew(mesh.nstitches[3]);
	}
	mesh.regFlags = u8Alloc.VectorNew(mesh.nregs);
	//*************************

	for(int i=0; i<mesh.nstitches[0]; ++i)
	{
		pFile->Read(&mesh.localStitches[0][i], 2);
		ByteSwap16(&mesh.localStitches[0][i]);
	} 
	for(int i=0; i<mesh.nstitches[1]; ++i)
	{
		pFile->Read(&mesh.localStitches[1][i], 2);
		ByteSwap16(&mesh.localStitches[1][i]);
	}
	for(int i=0; i<mesh.nstitches[2]; ++i)
	{
		pFile->Read(&mesh.localStitches[2][i], 2);
		ByteSwap16(&mesh.localStitches[2][i]);
	}
	for(int i=0; i<mesh.nstitches[3]; ++i)
	{
		pFile->Read(&mesh.localStitches[3][i], 2);
		ByteSwap16(&mesh.localStitches[3][i]);
	}
	for(int i=0; i<mesh.nstitches[0]; ++i)
	{
		pFile->Read(&mesh.neighbourStitches[0][i], 2);
		ByteSwap16(&mesh.neighbourStitches[0][i]);
	} 
	for(int i=0; i<mesh.nstitches[1]; ++i)
	{
		pFile->Read(&mesh.neighbourStitches[1][i], 2);
		ByteSwap16(&mesh.neighbourStitches[1][i]);
	}
	for(int i=0; i<mesh.nstitches[2]; ++i)
	{
		pFile->Read(&mesh.neighbourStitches[2][i], 2);
		ByteSwap16(&mesh.neighbourStitches[2][i]);
	}
	for(int i=0; i<mesh.nstitches[3]; ++i)
	{
		pFile->Read(&mesh.neighbourStitches[3][i], 2);
		ByteSwap16(&mesh.neighbourStitches[3][i]);
	}

	pFile->Read(mesh.regFlags, mesh.nregs);

	return true;
}
 
#if __BANK && !defined(NAVGEN_TOOL)
void agPolyMesh::DebugDraw()
{
//	Displayf("Num verts: %d", nverts);
//	Displayf("Num polys: %d", npolys);
//	Displayf("NVP: %d", nvp);  
//	Displayf("Num connections: %d", nconns);
//	Displayf("Num regions: %d", nregs);
	
	const float* orig = bmin;
	for(int i=0; i<npolys; i++)
	{
		const u16 * poly = &polys[i*nvp*2];
		for(int j=0; j<nvp; j++)
		{	
			if (poly[j] == 0xffff) break;
//			if (poly[nvp+j] == 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];

			Vector3 vertA;
			Vector3 vertB;

			u16* vA = &verts[vi[0]*3];
			Assert(vA[0] < 500);
			Assert(vA[2] < 500);
//			if(!(vA[0] < 500)) continue;
//			if(!(vA[2] < 500)) continue;

			vertA[0] = orig[0] + vA[0]*cs;
			vertA[1] = orig[2] + (vA[2]+1)*ch + 0.1f;
			vertA[2] = orig[1] + vA[1]*cs;
	 	
			u16* vB = &verts[vi[1]*3];
			Assert(vB[0] < 500);
			Assert(vB[2] < 500);
//			if(!(vB[0] < 500)) continue;
//			if(!(vB[2] < 500)) continue;

			vertB[0] = orig[0] + vB[0]*cs;
			vertB[1] = orig[2] + (vB[2]+1)*ch + 0.1f;
			vertB[2] = orig[1] + vB[1]*cs;

			static const Color32 col = Color32(0.0f, 0.0f, 1.f, 1.f);

			grcDebugDraw::Line(vertA, vertB, col, col);
		}
	}
}
#endif

}
