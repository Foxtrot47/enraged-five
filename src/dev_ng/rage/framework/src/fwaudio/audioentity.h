// 
// audio/audioentity.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
// 

#ifndef AUD_FWAUDIOENTITY_H
#define AUD_FWAUDIOENTITY_H

#include "audioengine/entity.h"
#include "audioengine/widgets.h"
#include "audiosoundtypes/sound.h"
class CEntity;

namespace rage
{

	class fwEntity;

#define AUDENTITY_NUM_ANIM_EVENTS 4
	//PURPOSE
	//An audAnimEvent is a container for audio data triggered at tagged points in an anim, at it's most basic this is just sound hash but can potentially contain other information too.
	struct audAnimEvent
	{
		audAnimEvent()
			:hash(0),
			isStart(false)
		{}

		audAnimEvent(u32 inHash)
		{
			hash = inHash;
		}
		audAnimEvent(u32 inHash, bool inStart)
		{
			hash = inHash;
			isStart = inStart;
		}
		u32 hash;
		bool isStart;
	};

	class fwAudioAnimHandlerInterface
	{
	public:
		fwAudioAnimHandlerInterface() {};
		virtual ~fwAudioAnimHandlerInterface();
		virtual void HandleEvent(const audAnimEvent & event,fwEntity *entity) = 0;
	};

	class fwAudioEntity : public audEntity
	{
	public:
		AUDIO_ENTITY_NAME(fwAudioEntity);

		fwAudioEntity()
		{ 
			m_AnimLoop = 0;
			m_AnimLoopTriggerTime = 0;
			m_AnimLoopWasStarted = false;
			m_AnimLoopWasStopped = false;
		};
		virtual ~fwAudioEntity()
		{
			if(m_AnimLoop)
			{
				m_AnimLoop->StopAndForget();
			}
		}

		virtual CEntity* GetOwningEntity(){return NULL;};

		void Init();
		
		static void HandleAnimEvent(const audAnimEvent & event,fwEntity *entity);
		static void SetDefaultAnimHandler(fwAudioAnimHandlerInterface *animHandler) {sm_DefaultAnimHandler = animHandler;}

		virtual void AddAnimEvent(const audAnimEvent & event);
		virtual void HandleLoopingAnimEvent(const audAnimEvent & event) =0;

		virtual void PreUpdateService(u32 timeInMs);

	protected:
		virtual void ProcessAnimEvents() =0;

		u32 m_AnimEvents[AUDENTITY_NUM_ANIM_EVENTS];
		audSound * m_AnimLoop;
		u32 m_AnimLoopHash;
		u32 m_AnimLoopTriggerTime;
		bool m_AnimLoopWasStarted;
		bool m_AnimLoopWasStopped;

	private:
		static fwAudioAnimHandlerInterface *sm_DefaultAnimHandler;
	};
}

#endif



