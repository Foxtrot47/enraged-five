#ifndef AUDMESH_H
#define AUDMESH_H


#include "vector/vector3.h"
#include "paging/base.h"

namespace rage
{

typedef rage::u32 AudMeshIndex;


static const AudMeshIndex AUDMESH_INDEX_NONE = 0xffff;


// Polymesh stores a connected mesh of polygons.
// The polygons are stored in an array where each polygons takes
// 'nvp*2' elements. The first 'nvp' elements are indices to vertices
// and the second 'nvp' elements are indices to neighbour polygons.
// If a polygon has less than 'nvp' vertices, the remaining indices
// are set as 0xffff. If an polygon edge does not have a neighbour
// the neighbour index is set to 0xffff.
// Vertices can be transformed into world space as follows:
//   x = bmin[0] + verts[i*3+0]*cs;
//   y = bmin[1] + verts[i*3+1]*ch;
//   z = bmin[2] + verts[i*3+2]*cs;


struct agPolyMesh
#if 1 //(!defined(NAVGEN_TOOL) )// || defined(RAGEBUILDER))
	: public rage::pgBase
#endif
{
	inline agPolyMesh() : verts(0), polys(0), regs(0), conns(0), rcindexs(0), nrconns(0), nverts(0), npolys(0), nvp(3), nregs(0), regFlags(0) 
	{ 
		memset(localStitches, 0, sizeof(localStitches[0])*4);
		memset(neighbourStitches, 0, sizeof(neighbourStitches[0])*4);
		memset(nstitches, 0, sizeof(u16)*4);
	}
	inline ~agPolyMesh()
	{ 
		delete [] verts; verts = 0;	
		delete [] polys; polys = 0;
		delete [] regs; regs = 0;
		delete [] localStitches[0]; localStitches[0] = 0;
		delete [] localStitches[1]; localStitches[1] = 0;
		delete [] localStitches[2]; localStitches[2] = 0;
		delete [] localStitches[3]; localStitches[3] = 0;
		delete [] neighbourStitches[0]; neighbourStitches[0] = 0;
		delete [] neighbourStitches[1]; neighbourStitches[1] = 0;
		delete [] neighbourStitches[2]; neighbourStitches[2] = 0;
		delete [] neighbourStitches[3]; neighbourStitches[3] = 0;
		delete [] regFlags; regFlags = 0;

	}
	//TODO: change the data arrays to u8's if appropriate and watch the memory savings roll in
	rage::u16* verts;	// Vertices of the mesh, 3 elements per vertex.
	rage::u16* polys;	// Polygons of the mesh, nvp*2 elements per polygon.
	rage::u16* regs;	// Regions of the polygons: uses npolys for its size
	rage::u16* conns;	//audio connections between regions
	rage::u16* rcindexs;	//indexes from the regions into the connections array, NB: this probably can't be dropped to a u8
	rage::u16* localStitches[4];	//Arrays of the local stitch regions for each side
	rage::u16* neighbourStitches[4]; //Arrays of the neighbour stitch regions for each side
	rage::u8* nrconns;	//num of aud connections for each region
	rage::u8* regFlags;		//region flags
	int nverts;	// Number of vertices.
	int npolys;	// Number of polygons.
	int nvp;	// Max number of vertices per polygon.
	int nconns;	//total number of connections
	float bmin[3], bmax[3];	// Bounding box of the mesh.
	float cs, ch;			// Cell size and height.
	rage::u16 nregs;		//Number of regions (used with rcindexs and nrconns, NOT regs as you might think) eventually this could be a u8 in theory; play it safe for now and change it late for consistency
	rage::u16 nstitches[4];	//Number of stitches in each direction
	
	rage::datPadding<2> padding; //padding for ragebuilder

	enum { RORC_VERSION = 1 };

	static agPolyMesh * Load(const char * pFileName);
	static bool LoadBinary(rage::fiStream * pFile, agPolyMesh & mesh);

	agPolyMesh(datResource & rsc);
	DECLARE_PLACE(agPolyMesh);

#if __DECLARESTRUCT // && !defined(NAVGEN_TOOL)
	void DeclareStruct(datTypeStruct & s);
#endif 

#if __BANK //&& !defined(NAVGEN_TOOl)
	void DebugDraw();
#endif

};


}

#endif
