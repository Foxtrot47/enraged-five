// 
// audio/audioentity.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
// 

#include "audioentity.h"
#include "audioengine/widgets.h"
#include "system/memops.h"


namespace rage {

fwAudioAnimHandlerInterface* fwAudioEntity::sm_DefaultAnimHandler = NULL;

void fwAudioEntity::Init()
{
#if !__RESOURCECOMPILER
	audEntity::Init();
#endif
	sysMemZeroBytes<sizeof(m_AnimEvents)>(m_AnimEvents);
	m_AnimLoop = NULL;
	m_AnimLoopHash = 0;
}

void fwAudioEntity::PreUpdateService(u32 UNUSED_PARAM(timeInMs))
{
	ProcessAnimEvents();
}



void fwAudioEntity::AddAnimEvent(const audAnimEvent & event)
{
	for(int i=0; i< AUDENTITY_NUM_ANIM_EVENTS; i++)
	{
		if(m_AnimEvents[i] == event.hash) //Don't stack up duplicate events, e.g. from multiple anim blends
		{
			return;
		}
		if(m_AnimEvents[i] == 0)
		{
			m_AnimEvents[i] = event.hash;
			break;
		}
	}
}



void fwAudioEntity::HandleAnimEvent(const audAnimEvent & event,fwEntity *entity)
{
	if(fwAudioEntity::sm_DefaultAnimHandler != NULL)
	{
		fwAudioEntity::sm_DefaultAnimHandler->HandleEvent(event,entity);
	}
}

fwAudioAnimHandlerInterface::~fwAudioAnimHandlerInterface()
{
}

}
