<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema" generate="class" >

<structdef type="::rage::fwTextDatabaseEntry" >
	<string name="m_Key" type="pointer" />
	<string name="m_DisplayText" type="pointer" />
</structdef>

<structdef type="::rage::fwTextDatabaseSource" >
	<array name="m_TextTable" type="atArray">
		<struct type="::rage::fwTextDatabaseEntry" />
	</array>
</structdef>

</ParserSchema>