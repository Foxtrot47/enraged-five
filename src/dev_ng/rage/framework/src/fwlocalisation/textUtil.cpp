/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : textUtil.cpp
// PURPOSE : Class to hold utility functions for text
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "textUtil.h"

// rage
#include "string/unicode.h"

namespace rage
{

#define WHITESPACE_CODEPOINT_COUNT ( 31 )

    static char16 const sc_whitespaceCharacters[ WHITESPACE_CODEPOINT_COUNT ] =
    {
        0x9,  //! CHARACTER TABULATION
        0xA,  //! LINE FEED
        0xB,  //! LINE TABLUATION
        0xC,  //! FORM FEED
        0xD,  //! CARRIAGE RETURN
        0x20,  //! SPACE
        0x85,  //! NEXT LINE
        0xA0,  //! NO-BREAK SPACE

        L'\u1680',  //! OGHAM SPACE MARK
        L'\u2000',  //! EN QUAD
        L'\u2001',  //! EM QUAD
        L'\u2002',  //! EN SPACE
        L'\u2003',  //! EM SPACE
        L'\u2004',  //! THREE-PER-EM SPACE
        L'\u2005',  //! FOUR-PER-EM SPACE
        L'\u2006',  //! SIX-PER-EM SPACE
        L'\u2007',  //! FIGURE SPACE
        L'\u2008',  //! PUNCTUATION SPACE
        L'\u2009',  //! THIN SPACE
        L'\u200A',  //! HAIR SPACE
        L'\u2028',  //! LINE SEPERATOR
        L'\u2029',  //! PARAGRAPH SEPERATOR
        L'\u202F',  //! NARROW NO-BREAK SPACE
        L'\u205F',  //! MEDIUM MATHEMATICAL SPACE
        L'\u3000',  //! IDEOGRAPHIC SPACE

        L'\u180E',  //! MONGOLIAN VOWEL SEPARATOR
        L'\u200B',  //! ZERO WIDTH SPACE
        L'\u200C',  //! ZERO WIDTH NON-JOINER
        L'\u200D',  //! ZERO WIDTH JOINER
        L'\u2060',  //! WORD JOINER
        L'\uFEFF',  //! ZERO WIDTH NON-BREAKING SPACE
    };

char* fwTextUtil::ShiftAndCopyString( char * const pOutString, char const * const pInString, u8 const shiftAmount )
{
	char *dest = pOutString;
	char const *src = pInString;

	// move along so many chars at start depending of var passed...
	for (u8 shifta = 0; shifta < shiftAmount; shifta++ )
	{
		src++;
	}

	return strcpy( dest, src );
}

char* fwTextUtil::StripHtml( char * const pOutString, char const * const pInString )
{
	char *dest = pOutString;
	char const *src = pInString;

	bool bInsideBrackets = false;

	while( src && *src )
	{
		if (*src == '<')
		{
			bInsideBrackets = true;
		}

		if (!bInsideBrackets)
		{
			*(dest++) = *src;
		}

		if (*src == '>')
		{
			bInsideBrackets = false;
		}

		src++;
	}

	*dest = rage::TEXT_CHAR_TERMINATOR;

	return pOutString;
}

void fwTextUtil::TruncateFileExtension( char* filename )
{
	char * period = strrchr( filename, '.' );
	if( period )
	{
		*period = rage::TEXT_CHAR_TERMINATOR;
	}
}

void fwTextUtil::TruncateFileName( char* filename )
{
    char const * const c_lastChar = filename + strlen( filename );
    char * lastSlash = strrchr( filename, '\\' );
    lastSlash = lastSlash == NULL ? strrchr( filename, '/' ) : lastSlash;

    if( lastSlash && lastSlash < c_lastChar )
    {
        lastSlash[1] = '\0';
    }
}

bool fwTextUtil::HasFileExtension( char const * const filename, char const * const extension )
{
	bool matches = false;

	char const * const c_period = filename ? strrchr( filename, '.' ) : NULL;
	if( c_period )
	{
		matches = strstr( c_period, extension ) != NULL;
	}

	return matches;
}

bool fwTextUtil::ComapreFileNamesIgnoreExtension( char const * const filenameA, char const * const filenameB )
{
    size_t const c_lenA = fwTextUtil::GetByteCount( filenameA ) + 1;
    size_t const c_lenB = fwTextUtil::GetByteCount( filenameB ) + 1;

    return ComapreFileNamesIgnoreExtension( filenameA, c_lenA, filenameB, c_lenB );
}

bool fwTextUtil::ComapreFileNamesIgnoreExtension( char const * const filenameA, size_t const byteCountA, char const * const filenameB, size_t const byteCountB )
{
    bool matches = filenameA == filenameB;

    if( !matches && filenameA && filenameB && byteCountA > 0 && byteCountB > 0 )
    {
        char const * posA = filenameA;
        char const * posB = filenameB;

        char const * const c_periodA = safestrrchr( filenameA, byteCountA, '.' );
        char const * const c_endA = c_periodA ? c_periodA : filenameA + byteCountA - 1; // Account for null terminator

        char const * const periodB = safestrrchr( filenameB, byteCountB, '.' );
        char const * const c_endB = periodB ? periodB : filenameB + byteCountB - 1;

        // TODO - preamble?
        matches = true;

        while( matches && posA != c_endA && posB != c_endB )
        {
            u32 const c_characterByteCountA = fwTextUtil::ByteCountForCharacter( posA );
            u32 const c_characterByteCountB = fwTextUtil::ByteCountForCharacter( posB );

            // We do a case insensitive comparison for bot as ASCII, otherwise we do a UTF-8 comparison
            bool const c_isAsciiA = c_characterByteCountA == 1;
            bool const c_isAsciiB = c_characterByteCountB == 1;
            bool const c_useAsciiComparison = c_isAsciiA && c_isAsciiB;

            matches = c_characterByteCountA == c_characterByteCountB &&
                c_useAsciiComparison ? tolower(*posA) == tolower(*posB) : fwTextUtil::CompareCharacter( posA, c_characterByteCountA, posB, c_characterByteCountB );

            posA += c_characterByteCountA;
            posB += c_characterByteCountB;
        }

        matches = matches && posA == c_endA && posB == c_endB;
    }   

    return matches;
}

char* fwTextUtil::StrcpyWithCharacterAndByteLimits( char * const pOutString, char const * const pInString, s32 const iMaxUtf8CharactersToCopy, s32 const iMaxBytesToCopy )
{
	if ( pInString && pOutString 
		&& Verifyf(iMaxUtf8CharactersToCopy != 0, "fwTextUtil::StrcpyWithCharacterAndByteLimits - Can't call with iMaxUtf8CharactersToCopy=0")
		&& Verifyf(iMaxBytesToCopy != 0, "fwTextUtil::StrcpyWithCharacterAndByteLimits - Can't call with iMaxBytesToCopy=0"))
	{
		char *dest = pOutString;
		const char *src = pInString;

		bool bFinished = false;

		s32 iNumberOfBytesCopied = 0;
		s32 iNumberOfCharactersCopied = 0;

		while (*src && !bFinished)
		{
			s32 numberOfBytesForCurrentCharacter = 0;
			if ((*src & 0x80) == 0)
			{
				numberOfBytesForCurrentCharacter = 1;
			}
			else if ((*src & 0xE0) == 0xC0)
			{
				// Detect invalid UTF-8 (which could be tricking us into skipping a \0) and bail
				if ((src[1] & 0xC0) != 0x80)
				{
					Assertf(0, "fwTextUtil::StrcpyWithCharacterAndByteLimits - invalid 2 byte UTF8 character");
					bFinished = true;
				}
				else
				{
					numberOfBytesForCurrentCharacter = 2;
				}
			}
			else if ((*src & 0xF0) == 0xE0)
			{
				// Detect invalid UTF-8 (which could be tricking us into skipping a \0) and bail
				if (((src[1] & 0xC0) != 0x80) || ((src[2] & 0xC0) != 0x80))
				{
					Assertf(0, "fwTextUtil::StrcpyWithCharacterAndByteLimits - invalid 3 byte UTF8 character");
					bFinished = true;
				}
				else
				{
					numberOfBytesForCurrentCharacter = 3;
				}
			}
			else	// ill-formed
			{
				bFinished = true;
			}

			if ((iMaxUtf8CharactersToCopy > 0) && ((iNumberOfCharactersCopied + 1) > iMaxUtf8CharactersToCopy))
			{
				bFinished = true;
			}

			if ((iMaxBytesToCopy > 0) && ((iNumberOfBytesCopied + numberOfBytesForCurrentCharacter) > (iMaxBytesToCopy-1)))
			{
				bFinished = true;
			}

			if (!bFinished)
			{
				while (numberOfBytesForCurrentCharacter)
				{
					*(dest++) = *(src++);
					numberOfBytesForCurrentCharacter--;
					iNumberOfBytesCopied++;
				}

				iNumberOfCharactersCopied++;
			}
		}		

		*dest = rage::TEXT_CHAR_TERMINATOR;
	}

	return(pOutString);
}

u32 fwTextUtil::ByteCountForCharacter( char const * const startingByte )
{
    return fwTextUtil::ByteCountForCharacter( (unsigned char const * const)startingByte );
}

u32 fwTextUtil::ByteCountForCharacter( unsigned char const * const startingByte )
{
	u32 iNumberOfBytesForCurrentCharacter = 1;
	if ((*startingByte & 0x80) == 0)
	{
		iNumberOfBytesForCurrentCharacter = 1;
	}
	else if ((*startingByte & 0xE0) == 0xC0)
	{
		// Detect invalid UTF-8 (which could be tricking us into skipping a \0) and bail
		if ((startingByte[1] & 0xC0) != 0x80)
		{
			Assertf(0, "fwTextUtil::ByteCountForCharacter - invalid 2 byte UTF8 character");
		}
		else
		{
			iNumberOfBytesForCurrentCharacter = 2;
		}
	}
	else if ((*startingByte & 0xF0) == 0xE0)
	{
		// Detect invalid UTF-8 (which could be tricking us into skipping a \0) and bail
		if (((startingByte[1] & 0xC0) != 0x80) || ((startingByte[2] & 0xC0) != 0x80))
		{
			Assertf(0, "fwTextUtil::ByteCountForCharacter - invalid 3 byte UTF8 character");
		}
		else
		{
			iNumberOfBytesForCurrentCharacter = 3;
		}
	}

	return iNumberOfBytesForCurrentCharacter; 
}

bool fwTextUtil::CompareCharacter( char const * const characterA, size_t const lenA, char const * const characterB, size_t const lenB )
{
    return fwTextUtil::CompareCharacter( (unsigned char const * const)characterA, lenA, (unsigned char const * const)characterB, lenB );
}

bool fwTextUtil::CompareCharacter( unsigned char const * const characterA, size_t const lenA, unsigned char const * const characterB, size_t const lenB )
{
    FastAssert( characterA );
    FastAssert( lenA > 0 );
    FastAssert( characterB );
    FastAssert( lenB > 0 );

    bool const c_matches = lenA == lenB && memcmp( characterA, characterB, lenA ) == 0;
    return c_matches;
}

u32 fwTextUtil::GetByteCount( char const * str )
{
	u32 iStringLength = 0;

	while( str && *str != rage::TEXT_CHAR_TERMINATOR)
	{
		iStringLength++;
		str++;
	}

	return iStringLength;
}

void fwTextUtil::MakeUpperCaseAsciiOnly(char *pString)
{
	while (*pString != 0)
	{
		if ( (*pString >= 'a' && *pString <= 'z') )
		{
			*pString -= 32;
		}
		pString++;
	}
}

void fwTextUtil::MakeLowerCaseAsciiOnly(char *pString)
{
	while (*pString != 0)
	{
		if ( (*pString >= 'A' && *pString <= 'Z') )
		{
			*pString += 32;
		}
		pString++;
	}
}

bool fwTextUtil::IsNumeric( char const aChar )
{
	bool bResult( aChar >= '0' && aChar <= '9' );
	return bResult;
}

bool fwTextUtil::IsSpace( char const * const firstByteOfCharacter )
{ 
    bool isSpace = false;

    char16 wideChar;
    int const c_byteCount = Utf8ToWideChar( firstByteOfCharacter, wideChar );

    for( u32 index = 0; c_byteCount > 0 && !isSpace && index < WHITESPACE_CODEPOINT_COUNT; ++index )
    {
        char16 const& c_characterToCompare = sc_whitespaceCharacters[ index ];
        isSpace = wideChar == c_characterToCompare;
    }

    return isSpace;
}

bool fwTextUtil::IsSpace( unsigned char const * const firstByteOfCharacter )
{
    return IsSpace( (char const * const)firstByteOfCharacter );
}

void fwTextUtil::CopyChar16StringIntoAsciiBuffer( char *pAsciiBuffer,  rage::char16 const * const pWCharString, s32 const asciiBufferLength )
{
	if ( pWCharString == NULL )
	{
		pAsciiBuffer[0] = '\0';
	}
	else
	{
		int char_loop = 0;
		while ( (char_loop < (asciiBufferLength - 1)) && (pWCharString[char_loop] != 0) )
		{
			pAsciiBuffer[char_loop] = char(pWCharString[char_loop]&0xff);
			char_loop++;
		}
		pAsciiBuffer[char_loop] = '\0';
	}
}

void fwTextUtil::CopyAsciiStringIntoChar16Buffer( rage::char16 *pWCharBuffer, char const * const pAsciiString, s32 const wcharBufferLength )
{
	if (pAsciiString == NULL)
	{
		pWCharBuffer[0] = '\0';
	}
	else
	{
		int char_loop = 0;
		while ( (char_loop < (wcharBufferLength - 1)) && (pAsciiString[char_loop] != '\0') )
		{
			pWCharBuffer[char_loop] = pAsciiString[char_loop];
			char_loop++;
		}
		pWCharBuffer[char_loop] = '\0';
	}
}

} // namespace rage
