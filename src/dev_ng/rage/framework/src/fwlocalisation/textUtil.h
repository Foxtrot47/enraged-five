/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : textUtil.h
// PURPOSE : Class to hold utility functions for text
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_TEXT_UTIL_H
#define FWLOCALISATION_TEXT_UTIL_H

// Rage Includes:
#include "string/unicode.h"

// Framework includes
#include "textTypes.h"

namespace rage
{

class fwTextUtil
{
public:
	// PURPOSE: Shift the input string by the amount given and copy to the output string
	// PARAMS:
	//		pOutString - Output string to copy to
	//		pInString - Input string to shift and copy from
	//		shiftAmount - Amount to shift the string by
	// RETURNS:
	//		Pointer to the output buffer
	static char* ShiftAndCopyString( char * const pOutString, char const * const pInString, u8 const shiftAmount );

	// PURPOSE: Strips out any content between angle brackets <>
	// PARAMS:
	//		pOutString - Buffer we are copying stripped string to
	//		pInString - Input string to read from
	// RETURNS:
	//		Pointer to output buffer
	static char* StripHtml( char * const pOutString, char const * const pInString );

	// PURPOSE: In-place truncate a string to remove the file extension
	// PARAMS:
	//		filename - Filename to truncate the extension
	// NOTES:
	//		If no extension is present, nothing will happen.
	static void TruncateFileExtension( char* filename );

    // PURPOSE: In-place truncate a string to remove the file name and extension
    // PARAMS:
    //		filename - Filename to truncate
    // NOTES:
    //		If no extension is present, nothing will happen.
    static void TruncateFileName( char* filename );

	// PURPOSE: Check if the given string ends with the given extension
	// PARAMS:
	//		filename - Filename to check
	//		extension - extension to check for
	// NOTES:
	//		If no extension is present, nothing will happen.
	static bool HasFileExtension( char const * const filename, char const * const extension );

	// PURPOSE: In-place compare two strings ignoring file extension
	// PARAMS:
	//		filenameA - Filename A to check
	//		filenameB - Filename B to check
	// NOTES:
	//		True if the same, false otherwise
	static bool ComapreFileNamesIgnoreExtension( char const * const filenameA, char const * const filenameB );

    // PURPOSE: In-place compare two strings ignoring file extension
    // PARAMS:
    //		filenameA - Filename A to check
    //      byteCountA - Count of the bytes in Filename A
    //		filenameB - Filename B to check
    //      byteCountB - Count of the bytes in Filename B
    // NOTES:
    //		True if the same, false otherwise
    static bool ComapreFileNamesIgnoreExtension( char const * const filenameA, size_t const byteCountA, char const * const filenameB, size_t const byteCountB );

	// PURPOSE: Acts like standard strcpy but allows character AND byte limits, to facilitate UTF-8 copying.
	// PARAMS:
	//		pOutString - Buffer we are copying string to
	//		pInString - Input string to read from
	//		iMaxUtf8CharactersToCopy - Maximum UTF-8 characters to copy
	//		iMaxBytesToCopy - Maximum bytes to copy
	// RETURNS:
	//		Pointer to output buffer
	// NOTES:
	//		Set iMaxUtf8CharactersToCopy to -1 if you don't want to set a limit on the number of characters to copy
	//		Set iMaxBytesToCopy to -1 if you don't want to set a limit on the number of bytes to copy
	static char* StrcpyWithCharacterAndByteLimits( char * const pOutString, char const * const pInString, s32 const iMaxUtf8CharactersToCopy, s32 const iMaxBytesToCopy );

	// PURPOSE: Get a count of the UTF8 characters in the given string
	// PARAMS:
	//		str - String to count characters in
	// RETURNS:
	//		Count of UTF8 characters in the string
	inline static u32 GetCharacterCount( char const * str )
	{
		return CountUtf8Characters( str );
	}

	// PURPOSE: Get the byte count for an individual character
	// PARAMS:
	//		startingByte - Pointer to the starting byte in the character
	// RETURNS:
	//		Count of bytes in the character
    static u32 ByteCountForCharacter( char const * const startingByte );
	static u32 ByteCountForCharacter( unsigned char const * const startingByte );

    // PURPOSE: Get the byte count for an individual character
    // PARAMS:
    //		characterA - Pointer to the starting byte in character A
    //		characterB - Pointer to the starting byte in character B
    // RETURNS:
    //		Count of bytes in the character
    static bool CompareCharacter( char const * const characterA, char const * const characterB )
    {
        size_t const c_lenA = ByteCountForCharacter( characterA );
        size_t const c_lenB = ByteCountForCharacter( characterB );

        return CompareCharacter( characterA, c_lenA, characterB, c_lenB );
    }

    static bool CompareCharacter( unsigned char const * const characterA, unsigned char const * const characterB )
    {
        size_t const c_lenA = ByteCountForCharacter( characterA );
        size_t const c_lenB = ByteCountForCharacter( characterB );

        return CompareCharacter( characterA, c_lenA, characterB, c_lenB );
    }

    // PURPOSE: Get the byte count for an individual character
    // PARAMS:
    //		characterA - Pointer to the starting byte in character A
    //      lenA       - Byte count of character A
    //		characterB - Pointer to the starting byte in character B
    //      lenB       - Byte count of character B
    // RETURNS:
    //		Count of bytes in the character
    static bool CompareCharacter( char const * const characterA, size_t const lenA, char const * const characterB, size_t const lenB );
    static bool CompareCharacter( unsigned char const * const characterA, size_t const lenA, unsigned char const * const characterB, size_t const lenB );

	// PURPOSE: Get the count of bytes in the given UTF8 string
	// PARAMS:
	//		str - String to get byte count for
	// RETURNS:
	//		Count of bytes in string EXCLUDING NULL TERMINATOR
	static u32 GetByteCount( char const * str );

	// PURPOSE: Convert all characters to upper case. Valid for ASCII characters only.
	// PARAMS:
	//		pString - Pointer to the string to alter, in-place.
	static void MakeUpperCaseAsciiOnly( char *pString );

	// PURPOSE: Convert all characters to lower case. Valid for ASCII characters only.
	// PARAMS:
	//		pString - Pointer to the string to alter, in-place.
	static void MakeLowerCaseAsciiOnly( char *pString );

	// PURPOSE: Check if a given character is numeric
	// PARAMS:
	//		aChar - char to checl
	// RETURNS:
	//		True if numeric, false otherwise
	static bool IsNumeric( char const aChar );

    // PURPOSE: Check if a given character is whitespace
    // PARAMS:
    //		firstByteOfCharacter - Pointer to the first byte of the character
    // RETURNS:
    //		True if whitespace, false otherwise
    static bool IsSpace( char const * const firstByteOfCharacter );
    static bool IsSpace( unsigned char const * const firstByteOfCharacter );

	// PURPOSE: Copy the given wide-char string into an ascii buffer.
	// PARAMS:
	//		pAsciiBuffer - Destination ASCII string
	//		pWCharString - Source Wide-char string
	//		asciiBufferLength - size of the ASCII string
	static void CopyChar16StringIntoAsciiBuffer( char *pAsciiBuffer,  rage::char16 const * const pWCharString, s32 const asciiBufferLength );

	// PURPOSE: Copy the given ascii string into an wide-char buffer.
	// PARAMS:
	//		pWCharBuffer - Destination wide-char string
	//		pAsciiString - Source ASCII string
	//		wcharBufferLength - size of the wide-char string
	static void CopyAsciiStringIntoChar16Buffer( rage::char16 *pWCharBuffer, char const * const pAsciiString, s32 const wcharBufferLength );
};

} // namespace rage

#endif // FWLOCALISATION_TEXT_UTIL_H
