/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : languagePack.h
// PURPOSE : Enum of supported languages along with helper functions for validation,
//			 conversion and basic checks.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWLOCALISATION_LANGUAGE_PACK_H_
#define FWLOCALISATION_LANGUAGE_PACK_H_

// rage includes:
#include "rline/socialclub/rlsocialclubcommon.h"
#include "system/language.h"

// framework
#include "fwlocalisation/templateString.h"

namespace rage
{

class fwLanguagePack
{
public:

// TODO - Move this to a seperate localization settings header
#define LOCALIZATION_KEY_LENGTH 16

	enum DATE_FORMAT
	{ 
		DATE_FORMAT_DMY = 0, 
		DATE_FORMAT_MDY, 
		DATE_FORMAT_YMD, 

		MAX_DATE_FORMATS
	};

	enum DATE_FORMAT_DELIMITER
	{ 
		DATE_FORMAT_DELIMITER_SLASH = 0, 
		DATE_FORMAT_DELIMITER_DASH, 
		DATE_FORMAT_DELIMITER_DOT, 
		DATE_FORMAT_DELIMITER_SPACE, 

		MAX_DATE_FORMAT_DELIMITERS
	};

#if __BANK

	enum UNIT_OVERRIDE
	{ 
		UNIT_OVERRIDE_NONE, 
		UNIT_OVERRIDE_METRIC, 
		UNIT_OVERRIDE_IMPERIAL,

		MAX_UNIT_OVERRIDE
	};

	

	static int m_eUnitOverride;

#endif // __BANK

	// PURPOSE: Get the plain-text name for a given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		String representation of the given language pack
	static char const * GetLanguagePackName( sysLanguage const language );

	// PURPOSE: Get the Social Club equivilent language for a given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		Equivilent Social Club Language, or English if error.
	static rlScLanguage GetScLanguageFromLanguage( sysLanguage const language );

	// PURPOSE: Returns a string of supported social club languages based on the given language
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		Delimited string of supported social club languages
	static char const * GetStringOfSupportedScLanguages( sysLanguage const language );

	// PURPOSE:	Returns the ISO code ( en,fr,de,it,jp,es, for example ) for a given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		ISO language code representing the given language
	static char const * GetIsoLanguageCode( sysLanguage const language );

	// PURPOSE: Return the name of the font library to use for a given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		Name of the font library used for the given language
	static char const * GetFontLibraryName( sysLanguage const language );

	// PURPOSE: Return the currency delimiter for a given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		String representing the currency delimiter
	static char const * GetCurrencyDelimiter( sysLanguage const language );

	// PURPOSE: Convert the given value in metres to an appropriate localized speed value
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	//		metres - Actual value in metres to convert
	// RETURNS:
	//		Localized unit value (Kilometres for metric, miles for non-metric)
	static float ConvertMetersToDisplaySpeed( sysLanguage const language, float const metres );

	// PURPOSE: Convert the given value in kilometers to an appropriate localized speed value
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	//		kilometres - Actual value in kilometres to convert
	// RETURNS:
	//		Localized unit value (Kilometres for metric, miles for non-metric)
	static float ConvertKilometersToDisplaySpeed( sysLanguage const language, float const kilometres );

	// PURPOSE: Convert the given value in metres to an appropriate localized distance value
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	//		metres - Actual value in metres to convert
	// RETURNS:
	//		Localized unit value (Kilometres for metric, miles for non-metric)
	static float ConvertMetersToDisplayDistance( sysLanguage const language, float const metres );

	// PURPOSE: Convert the given value in metres to an appropriate localized short distance value
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	//		metres - Actual value in metres to convert
	// RETURNS:
	//		Localized unit value (Metres for metric, feet for non-metric)
	static float ConvertMetersToDisplayDistanceShort( sysLanguage const language, float const metres );

	// PURPOSE: Format the given value as currency based on the given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	//		buffer    - Pre-allocated buffer to format the currency into
	//		valueToDisplay - Actual value to format and display
	static void FormatValueAsCurrency( sysLanguage const language, char * buffer, int const valueToDisplay );

	// PURPOSE: Format the given value as weight based on the given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	//		buffer    - Pre-allocated buffer to format the currency into
	//		weightInLbs - Actual value to format and display, in Lbs
	static void FormatValueAsWeight( sysLanguage const language, char * buffer, int const weightInLbs );

#if __BANK
	// PURPOSE: Convert a given readable language name back into a language pack ID
	// PARAMS:
	//		pLanguageName - Language name to convert
	// RETURNS:
	//		Language Pack ID for the given name, or undefined if no match found
	static sysLanguage GetLanguageFromName( char const * const pLanguageName );
#endif // __BANK

	// PURPOSE: Returns true if a given language is Asian
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		true if language pack is Asian
	static bool IsAsianLanguage( sysLanguage const language );

	// PURPOSE: Returns true if a given language is Russian
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		true if language pack is Russian
	static bool IsRussianLanguage( sysLanguage const language );

	// PURPOSE: Returns true if a given language uses metric representation
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		true if language pack uses metric units
	static bool IsMetric( sysLanguage const language );

	// PURPOSE: Returns the date format used for the given language
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		DATE_FORMAT enum value representing how to format dates for this language
	static DATE_FORMAT GetDateFormatType( sysLanguage const language );
	
	// PURPOSE: Returns true if in that language the month is written
	// in lowercase in a normal sentence
	// PARAMS:
	//		language - Identifier of the language pack we care about
	static bool NeedsLowerCaseMonth(sysLanguage const language);

	// PURPOSE: Returns the date format delimiter type used for the given language
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		DATE_FORMAT_DELIMITER enum value representing how to format dates for this language
	static DATE_FORMAT_DELIMITER GetDateFormatDelimiterType( sysLanguage const language );

	// PURPOSE: Returns the string representation of the given delimiter
	// PARAMS:
	//		delimiter - Identifier of the delimiter type we care about
	// RETURNS:
	//		Literal string representing the delimiter to use for the given language
	static char const * GetDateFormatDelimiter( DATE_FORMAT_DELIMITER const delimiter );

	// PURPOSE: Returns whether the language has the specific delimiter
	static bool HasDayDelimiter( sysLanguage const language );
	static bool HasMonthDelimiter( sysLanguage const language );
	static bool HasYearDelimiter( sysLanguage const language );
	static bool DoesPrefixDelimiterWithSpace( sysLanguage const language );
	static bool DoesPostfixDelimiterWithSpace( sysLanguage const language );

	// PURPOSE: Returns the date format delimiter string used for the given language
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		Literal string representing the delimiter to use for the given language
	static char const * GetDateFormatDelimiter( sysLanguage const language );

	// PURPOSE: Converts the given month (1-based index) to a string representation
	// PARAMS:
	//		month - Index of month we want
	//		shorthand - True if we want a short representation, false otherwise
	// RETURNS:
	//		Localization key string representing the month to use for the given language
	static void GetMonthString( char (&out_buffer)[ LOCALIZATION_KEY_LENGTH ], u32 const month, bool const shorthand, const bool lowerCase );

	// PURPOSE: Converts the given day of week (1-based index, starting Monday) to a string representation
	// PARAMS:
	//		day - Index of day we want
	//		shorthand - True if we want a short representation, false otherwise
	// RETURNS:
	//		Localization key string representing the day to use for the given language
	static void GetDayString( char (&out_buffer)[ LOCALIZATION_KEY_LENGTH ], u32 const day, bool const shorthand );
	
	// PURPOSE: Converts the given day of week (1-based index, starting 1 (1st), maximum of 31 (31st))
	// PARAMS:
	//		day - Index of day we want
	// RETURNS:
	//		Localization key string representing the day to use for the given language
	static void GetDayOfTheMonthString( char(&out_buffer)[LOCALIZATION_KEY_LENGTH], u32 const dayOfMonth );

private:
	static char const * const ms_TextLanguageFileNames[ MAX_LANGUAGES ];
	static char const * const ms_LanguageIsoCodes[ MAX_LANGUAGES ];
	static char const * const ms_dateDelimiters[ MAX_DATE_FORMAT_DELIMITERS ];

	static char const * const ms_weightFormatMetric;
	static char const * const ms_weightFormatImperial;

	static char const * const ms_currencyFormatLeft;
	static char const * const ms_currencyFormatRight;

	static char const * const ms_currencyDelimiterComma;
	static char const * const ms_currencyDelimiterPeriod;
	static char const * const ms_currencyDelimiterSpace;

	// PURPOSE: Return the format string for a given language pack
	// PARAMS:
	//		eLanguage - Identifier of the language pack we care about
	// RETURNS:
	//		Format string for displaying a currency value
	static char const * GetCurrencyFormatString( sysLanguage const language );
};

} // namespace rage

#endif // FWLOCALISATION_LANGUAGE_PACK_H_
