//
// localisation/localisation_channel.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __localisation_channel
#define __localisation_channel localisation_chan

#ifndef INC_FWLOCALISATION_CHANNEL_H
#define INC_FWLOCALISATION_CHANNEL_H

#include "diag/channel.h"

namespace rage
{
	RAGE_DECLARE_CHANNEL(localisation_chan)					// Defined in LanguagePack.cpp

	#define localisationAssert(cond)						RAGE_ASSERT(localisation_chan,cond)
	#define localisationAssertf(cond,fmt,...)				RAGE_ASSERTF(localisation_chan,cond,fmt,##__VA_ARGS__)
	#define localisationVerify(cond)						RAGE_VERIFY(localisation_chan, cond)
	#define localisationVerifyf(cond,fmt,...)				RAGE_VERIFYF(localisation_chan,cond,fmt,##__VA_ARGS__)
	#define localisationErrorf(fmt,...)						RAGE_ERRORF(localisation_chan,fmt,##__VA_ARGS__)
	#define localisationWarningf(fmt,...)					RAGE_WARNINGF(localisation_chan,fmt,##__VA_ARGS__)
	#define localisationDisplayf(fmt,...)					RAGE_DISPLAYF(localisation_chan,fmt,##__VA_ARGS__)
	#define localisationDebugf1(fmt,...)					RAGE_DEBUGF1(localisation_chan,fmt,##__VA_ARGS__)
	#define localisationDebugf2(fmt,...)					RAGE_DEBUGF2(localisation_chan,fmt,##__VA_ARGS__)
	#define localisationDebugf3(fmt,...)					RAGE_DEBUGF3(localisation_chan,fmt,##__VA_ARGS__)
	#define localisationLogf(severity,fmt,...)				RAGE_LOGF(localisation_chan,severity,fmt,##__VA_ARGS__)
	#define localisationCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,localisation_chan,severity,fmt,##__VA_ARGS__)

} // namespace rage

#endif // INC_FWLOCALISATION_CHANNEL_H
