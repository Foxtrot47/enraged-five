/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textFileConversion.cpp 
// PURPOSE : Class for converting between text as meta-data into the appropriate
//			 resourced format.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

// Framework headers
#include "fwscene/stores/textAsset.h"
#include "textFileConversion.h"
#include "textDatabaseSource.h"

namespace rage
{

bool fwTextFileConversion::populateTextAsset( rage::fwTextDatabaseSource const & sourceDatabase, rage::fwTextAsset& out_targetAsset )
{
	bool success = false;

	for ( int index = 0; index < sourceDatabase.m_TextTable.size(); ++index )
	{
		::rage::fwTextDatabaseEntry const& entry = sourceDatabase.m_TextTable[ index ];

		Assertf( entry.m_Key[0] != '\0', "Text record found with no hash value!" );
		if( entry.m_Key[0] != '\0' )
		{
			Assertf( entry.m_DisplayText && entry.m_DisplayText[0] != '\0', "Text record %s has no valid text!", entry.m_Key );
			if( entry.m_DisplayText )
			{
				u32 hashedKey = atStringHash( entry.m_Key );
				success = out_targetAsset.addText( hashedKey, entry.m_DisplayText ) || success;
			}
		}
	}

	return success;
}

} // namespace rage
