/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textTypes.h 
// PURPOSE : Common types used in the text localisation framework
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_TEXT_TYPES_H_
#define FWLOCALISATION_TEXT_TYPES_H_

namespace rage
{
	enum 
	{
		TEXT_CHAR_TERMINATOR = ('\0'),

		LANG_KEY_SIZE = 16
	};

	static char const * const TEXT_STRING_TERMINATOR = "\0";

} // namespace rage

#endif // FWLOCALISATION_TEXT_TYPES_H_
