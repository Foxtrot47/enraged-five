/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : localisedString.h
// PURPOSE : A wrapper class to manage dynamically sized strings of localized text and
//				is built upon the functionality of the fwTextUtil class.
// AUTHOR  : James Chagaris
// ADOPTED : James Strain
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_STRING_H
#define FWLOCALISATION_STRING_H

// rage
#include "atl/string.h"

// framework
#include "fwlocalisation/textTypes.h"
#include "fwlocalisation/textUtil.h"

namespace rage
{

// PURPOSE:
// While localized characters are defined as a char, they are UTF-8 so needs some differences to atString
// This class manages the differences and uses a dynamic array to store the data.
class fwLocalisedString
{
	fwLocalisedString& _Append( char const * const s, int linSizeBytes );

	inline fwLocalisedString& _Set( char const * const s, int inSizeBytes )
	{
		Reset();
		return _Append(s, inSizeBytes);
	}

public:

	fwLocalisedString()
	{
		Reset();
	}

	fwLocalisedString( char const * const str )
	{
		Append( str );
	}

	fwLocalisedString( const fwLocalisedString& rStr )
	{
		m_data = rStr.m_data;
	}

	inline fwLocalisedString& operator=(const fwLocalisedString& rStr)
	{
		if( &rStr != this )
		{
			m_data = rStr.m_data;
		}

		return *this;
	}

	inline fwLocalisedString& operator=(const atString& rStr)
	{
		return Set(rStr.c_str());
	}

	inline fwLocalisedString& operator+=(const fwLocalisedString& rStr)
	{
		return _Append(rStr.GetData(), rStr.GetSizeBytes());
	}

	inline fwLocalisedString& operator+=(const atString& rStr)
	{
		return Append(rStr.c_str());
	}

	inline fwLocalisedString& Append( char const * const str )
	{
		if(str)
		{
			_Append(str, rage::fwTextUtil::GetByteCount( str ) + 1 );
		}

		return *this;
	}

	inline fwLocalisedString& Append(const fwLocalisedString& rStr)
	{
		return _Append(rStr.GetData(), rStr.GetSizeBytes());
	}

	inline fwLocalisedString& Append(const atString& rStr)
	{
		return Append(rStr.c_str());
	}

	inline fwLocalisedString& Set( char const * const s)
	{
		Reset();
		return Append(s);
	}

	inline fwLocalisedString& Set(const atString& rStr)
	{
		return Set( rStr.c_str() );
	}

	inline bool operator==(const fwLocalisedString& rStr) const
	{
		return !this->operator!=(rStr);
	}

	inline bool operator==( char const * const data ) const
	{
		return !this->operator!=(data);
	}

	bool operator!=(const fwLocalisedString& rStr) const;
	bool operator!=(const char* data) const;

	inline bool IsNull() const
	{
		return m_data.empty();
	}

	inline bool IsNullOrEmpty() const
	{
		return ( IsNull() || m_data[0] == rage::TEXT_CHAR_TERMINATOR );
	}

	inline int GetSizeBytes() const
	{
		return m_data.GetCount();
	}

	inline u32 GetCharacterCount() const 
	{
		return rage::fwTextUtil::GetCharacterCount( GetData() );
	} 

	inline char* GetData() 
	{
		return m_data.GetElements();
	}

	inline const char* GetData() const 
	{
		return m_data.GetElements();
	}

	// The memory is still allocated, just sets the string to ""
	inline void Reset()
	{
		if( m_data.GetCapacity() > 0 )
		{
			m_data.Resize(1);
			m_data[0] = rage::TEXT_CHAR_TERMINATOR;
		}
	}

	// Frees the memory.
	inline void Clear()
	{
		m_data.Reset();
	}

	inline void Uppercase()
	{
		rage::fwTextUtil::MakeUpperCaseAsciiOnly( GetData() );
	}

private:
	atArray<char> m_data;
};

} // namespace rage

#endif // FWLOCALISATION_STRING_H
