/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textFileConversion.h 
// PURPOSE : Class for converting between text as meta-data into the appropriate
//			 resourced format.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_TEXT_CONVERSION_H_
#define FWLOCALISATION_TEXT_CONVERSION_H_

namespace rage
{
	class fwTextDatabaseSource;
	class fwTextAsset;

class fwTextFileConversion
{
public:

	// PURPOSE: Populates the target asset based on the contents of the given text database.
	// PARAMS:
	//		sourceDatabase - Text database we are reading from
	//		out_targetAsset - Target text asset we are populating
	// RETURNS:
	//		True if text asset has been populated with at least one entry.
	// NOTES:
	//		Function will still succeed if it encounters invalid entries in the database. These entries will
	//		be reported and not built into the target asset.
	static bool populateTextAsset( rage::fwTextDatabaseSource const & sourceDatabase, rage::fwTextAsset& out_targetAsset );

private:
};

} // namespace rage

#endif // FWLOCALISATION_TEXT_CONVERSION_H_
