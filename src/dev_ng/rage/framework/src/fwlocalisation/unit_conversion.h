/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : unit_conversion.h 
// PURPOSE : Common place for localised unit conversion
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_UNIT_CONVERSION_H_
#define FWLOCALISATION_UNIT_CONVERSION_H_

#include "atl/hashstring.h"

namespace rage
{
	// --- handy metric-to-imperial converters -------------------------------------
	#define KGS_TO_LBS(M) (float(M)*2.20462262f)
	#define LBS_TO_KGS(M) (float(M)*0.453592f)
	#define METERS_TO_MILES(M) (float(M)/1609.344f)
	#define KILOMETERS_TO_MILES(M) (float(M)/1.609344f)
	#define METERS_TO_FEET(M) (float(M)/0.3048f)
	#define METERS_TO_KILOMETERS(M) (float(M)/1000.0f)
	#define MILES_TO_KILOMETERS(M) (float(M)*1.609344f)

	// --- String key identifiers for unit types   -------------------------------------
	static const u32 UNIT_KEY_METERS		= ATSTRINGHASH( "UNIT_METERS", 0x280F5F9E );
	static const u32 UNIT_KEY_KILOMETERS	= ATSTRINGHASH( "UNIT_KILO", 0xD81E128 );
	static const u32 UNIT_KEY_FEET			= ATSTRINGHASH( "UNIT_FEET", 0x96BA70B8 );
	static const u32 UNIT_KEY_MILES			= ATSTRINGHASH( "UNIT_MILES", 0x9CF5EFF4 );
	static const u32 UNIT_KEY_KPH			= ATSTRINGHASH( "UNIT_KPH", 0xD3B109B );
	static const u32 UNIT_KEY_MPH			= ATSTRINGHASH( "UNIT_MPH", 0x1BED45ED );

} // namespace rage

#endif // FWLOCALISATION_UNIT_CONVERSION_H_
