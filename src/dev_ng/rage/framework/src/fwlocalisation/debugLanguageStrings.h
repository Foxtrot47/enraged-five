/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : debugLanguageStrings.h 
// PURPOSE : Maintains a collection of debug language strings not loaded from 
//			 language data.
//
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_DEBUGLANGUAGESTRINGS_H_
#define FWLOCALISATION_DEBUGLANGUAGESTRINGS_H_

#if __BANK

// rage headers
#include "atl/map.h"
#include "system/criticalsection.h"

namespace rage
{

class fwDebugLanguageStrings
{
private:

	// PURPOSE: Internal class for simple string wrapping
	class fwDebugStringRecord
	{
	public:
		fwDebugStringRecord() 
			: m_string( NULL ) 
			, m_bFlaggedForDeletion( true )
		{
		}

		~fwDebugStringRecord() 
		{ 
			if( m_string ) 
			{
				delete[] m_string; 
			}
		}

		// PURPOSE: Set the value of this string record
		// PARAMS:
		//		pText - Text to set
		void Set( char const * const pText);

		// PURPOSE: Flag that this string for deletion
		// PARAMS:
		//		bDelete - True if we can delete it, false otherwise
		inline void SetDeletionFlag( bool bDelete ) 
		{ 
			m_bFlaggedForDeletion = bDelete; 
		}

		// PURPOSE: Check if this record is flagged for deletion
		// RETURNS:
		//		True if flagged for deletion, false otherwise
		inline bool IsFlaggedForDeletion() const 
		{ 
			return m_bFlaggedForDeletion; 
		}

		// PURPOSE: Get the actual string value represented by this record
		// RETURNS:
		//		String representation. Null value returned if string has never been set, or set to null.
		inline char const * GetString() const 
		{ 
			return m_string; 
		}

	private:
		char*	m_string;
		bool	m_bFlaggedForDeletion;
	};

public:
	// PURPOSE: Adds the given text to the debug string list with the given identifier
	// PARAMS:
	//		pTextKey - String identifier to be added
	//		pTextToDisplay - Text to display for the given identifier
	// NOTES:
	//		If a string with this key already exists, it's value will be overwritten with 
	//		the new display value.
	static void Add( char const * const pTextKey, char const * const pTextToDisplay );

	// PURPOSE: Flag the given string for deletion
	// PARAMS:
	//		pTextKey - String identifier to be deleted
	static void FlagForDeletion( char const * const pTextKey );

	// PURPOSE: Flag all strings for deletion
	static void FlagAllForDeletion();

	// PURPOSE: Delete all strings that are flagged for deletion
	static void DeleteAllFlagged();

	// PURPOSE: Gets the display text for a given hashed string identifier
	// PARAMS:
	//		textHash - Hashed string identifier
	// RETURNS:
	//		String representation. Null value returned if string not found.
	static char const * GetString( u32 textHash );

private:
	typedef atMap<u32, fwDebugStringRecord>	fwDebugStringMap;
	typedef fwDebugStringMap::Iterator		fwDebugStringIterator;

	static fwDebugStringMap			ms_stringMap;
	static sysCriticalSectionToken	ms_criticalToken;
};

} // namespace rage

#endif // __BANK

#endif // FWLOCALISATION_DEBUGLANGUAGESTRINGS_H_
