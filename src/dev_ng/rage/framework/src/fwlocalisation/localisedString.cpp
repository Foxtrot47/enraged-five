/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : localisedString.cpp
// PURPOSE : A wrapper class to manage dynamically sized strings of localized text and
//				is built upon the functionality of the fwTextUtil class.
// AUTHOR  : James Chagaris
// ADOPTED : James Strain
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

//rage headers
#include "string/string.h"

// framework headers
#include "fwlocalisation/localisedString.h"

namespace rage
{

fwLocalisedString& fwLocalisedString::_Append( char const * const s,int inSizeBytes)
{
	if(s)
	{
		if (inSizeBytes > 0)
		{
			int len = IsNull() ? 0 : GetSizeBytes() - 1;
			m_data.ResizeGrow(len + inSizeBytes);

			// Need to clear out the garbage string that it currently being pointed to.
			if(IsNull())
			{
				m_data[0] = rage::TEXT_CHAR_TERMINATOR;
			}

			safecpy( GetData() + len, s, inSizeBytes);
		}
	}
	else
	{
		Reset();
	}

	return *this;
}

bool fwLocalisedString::operator!=(const fwLocalisedString& rStr) const
{
	if(GetSizeBytes() != rStr.GetSizeBytes())
	{
		return true;
	}

	if(GetData() && rStr.GetData())
	{
		return !strcmp(GetData(), rStr.GetData());
	}

	// If the lengths are the same, and we have a NULL, then they are both empty strings.
	return false;
}

bool fwLocalisedString::operator!=(const char* data) const
{
	if(GetData() && data)
	{
		return !strcmp(GetData(), data);
	}
	else if(GetData())
	{
		// data is NULL, so consider it to have a length of 0
		return GetSizeBytes() != 0;
	}
	else if(data)
	{
		return data[0] != rage::TEXT_CHAR_TERMINATOR;
	}

	return false; // Both are NULL pointers
}

} // namespace rage
