<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema" generate="class" >

<structdef type="::rage::fwExtraTextPackage" >
	<string name="m_PathFormatString" type="pointer" description="Format string to a localized text RPF. e.g. update://somewhere/%s" />
  <bool   name="m_ShouldOverlay" init="false" />
</structdef>

<structdef type="::rage::fwExtraTextFiles" >
	<array name="m_FileTable" type="atArray">
		<struct type="::rage::fwExtraTextPackage" />
	</array>
</structdef>

</ParserSchema>