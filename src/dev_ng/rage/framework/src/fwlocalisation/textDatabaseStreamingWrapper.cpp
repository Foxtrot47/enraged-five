/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textDatabaseStreamingWrapper.cpp 
// PURPOSE : Helper class to wrap our text streaming. Handles some additional
//			 mechanics to track streaming requests and hide it from other systems.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

// rage headers
#include "parser/macros.h"
#include "parser/manager.h"
#include "system/param.h"
#include "system/threadtype.h"

// framework headers
#include "debugLanguageStrings.h"
#include "extraTextFile.h"
#include "fwutil/xmacro.h"
#include "localisation_channel.h"
#include "streaming/streamingmodule.h"
#include "streaming/packfilemanager.h"
#include "textDatabaseStreamingWrapper.h"

PARAM(langfilesuffix, "[text] Suffix to append to the RPF filename of a language file, like '_rel'");

namespace rage
{

// global instance of the text database store
fwTextDatabaseStreamingWrapper g_TextDatabaseStore;

#if __BANK

bool fwTextDatabaseStreamingWrapper::ms_forceTextIds = false;

#endif // __BANK

char const * const fwTextDatabaseStreamingWrapper::msc_textDatabasePathFormat = "platform:/data/lang/%s%s.rpf";

rage::strStreamingObjectName const fwTextDatabaseStreamingWrapper::msc_coreTextFileName( "global" );

char fwTextDatabaseStreamingWrapper::ms_errorStringBuffer[2][64];

enum
{
	MAX_ACTIVE_TEXT_RPF = 1 // todo - Data-drive this from game config!
};

// PURPOSE: Minimal structure used for database reloading
struct fwTextDatabaseReloadingParams
{
	fwTextDatabaseReloadingParams()
		: m_name()
		, m_databaseHandle( -1 )
		, m_refCount( 0 )
		, m_flags( 0 )
	{

	}

	fwTextDatabaseReloadingParams( rage::strStreamingObjectName const& name, TextDatabaseHandle const handle, u64 const refCount, u16 const flags )
		: m_name( name )
		, m_databaseHandle( handle )
		, m_refCount( refCount )
		, m_flags( flags )
	{

	}

	rage::strStreamingObjectName	m_name;
	TextDatabaseHandle				m_databaseHandle;
	u64								m_refCount;
	u16								m_flags;
};

fwTextDatabaseStreamingWrapper::fwTextDatabaseStreamingWrapper()
	: m_textStore()
	, m_requests()
	, m_activeTextRpfs( 0, MAX_ACTIVE_TEXT_RPF )
	, m_activeLanguagePack( LANGUAGE_UNDEFINED )
	, m_coreTextHandle( -1 )
	, m_coreTextLoaded( false )
{
}

void fwTextDatabaseStreamingWrapper::Update()
{
	UpdateCoreTextLoading();

	while( !m_requests.IsEmpty() )
	{
		fwTextDatabaseStreamingRequest currentRequest( m_requests.Pop() );
		fwTextDatabaseStreamingRequest::ACTION const c_requestedAction = currentRequest.GetAction();
		TextDatabaseHandle dataSlot = currentRequest.GetTargetHandle();
		bool const c_databaseLoaded = IsTextDatabaseLoaded( dataSlot );

		if( c_requestedAction == fwTextDatabaseStreamingRequest::ACTION_LOAD_DATABASE || c_requestedAction == fwTextDatabaseStreamingRequest::ACTION_LOAD_DATABASE_IMMEDIATE )
		{
			RequestTextDatabaseInternal( dataSlot, c_requestedAction == fwTextDatabaseStreamingRequest::ACTION_LOAD_DATABASE_IMMEDIATE );
		}
		else if( c_requestedAction == fwTextDatabaseStreamingRequest::ACTION_ADD_REF && c_databaseLoaded )
		{
			AddTextDatabaseRefInternal( dataSlot );
		}
		else if ( c_requestedAction == fwTextDatabaseStreamingRequest::ACTION_REMOVE_REF && c_databaseLoaded )
		{
			RemoveTextDatabaseRefInternal( dataSlot );
		}
	}
}

bool fwTextDatabaseStreamingWrapper::Initialize( sysLanguage const requestedLanguagePack )
{
	localisationAssertf( requestedLanguagePack != LANGUAGE_UNDEFINED, "Attempting to load into an undefined language!" );

	bool success = false;

	if( !IsInitialized() )
	{
		s32 baseTextRpfIndex = loadTextRPF( msc_textDatabasePathFormat, requestedLanguagePack, false );
		if( baseTextRpfIndex >= 0 )
		{
			LoadCoreText();

			m_activeLanguagePack = requestedLanguagePack;
			success = IsInitialized();
		}
	}
	else
	{
		localisationWarningf( "Double initializing streaming text!" );
		success = ReloadDueToLanguageChange( requestedLanguagePack );
	}

	return success;
}

bool fwTextDatabaseStreamingWrapper::ReloadDueToLanguageChange( sysLanguage const requestedLanguagePack )
{
	localisationAssertf( IsInitialized(), "Attempting to reload language with an un-initialized language system!" );
	localisationAssertf( requestedLanguagePack != LANGUAGE_UNDEFINED, "Attempting to reload into an undefined language!" );

	// Early opt-out if we already have this pack loaded!
	if( m_activeLanguagePack == requestedLanguagePack )
		return true;

	bool success = false;

	//! Keep track of all the RPFs we had loaded
	ActiveRpfArray tempRPFArray( m_activeTextRpfs );

	// Track which databases were loaded in and store the reference count
	atArray< fwTextDatabaseReloadingParams > reloadParamArray( 0, m_textStore.GetNumUsedSlots() );
	for( int slotIndex = 0; slotIndex < m_textStore.GetNumUsedSlots(); ++slotIndex )
	{
		u64 refCount = m_textStore.GetRefCount( strLocalIndex(slotIndex) );
		u16 flags = m_textStore.GetFlags( strLocalIndex(slotIndex) );

		// Clear flags for the coming shutdown...
		m_textStore.SetRawFlags( strLocalIndex(slotIndex), 0 );
		m_textStore.SetRawRefCount( strLocalIndex(slotIndex), 0 );

		if( refCount > 0 )
		{
			reloadParamArray.Push( fwTextDatabaseReloadingParams( m_textStore.GetSlot( strLocalIndex(slotIndex) )->m_name, slotIndex, refCount, flags ) );
		}
	}

	Shutdown();

	success = Initialize( requestedLanguagePack );

	while( tempRPFArray.size() > 0 )
	{
		fwActiveTextRpf& topElement = tempRPFArray[0];
		loadTextRPF( topElement.m_pathFormat, requestedLanguagePack, topElement.m_isOverlay );

		tempRPFArray.erase( &topElement );
	}

	// Now we restore the ref count and loaded state of any previously loaded text databases
	for( s32 index = 0; success && index < reloadParamArray.size(); ++index )
	{
		fwTextDatabaseReloadingParams& topElement = reloadParamArray[index];

		// Don't try and reload the core text, as we'll have kicked that off in initialize
		if( topElement.m_databaseHandle != m_coreTextHandle )
		{
			TextDatabaseHandle handle = RequestTextDatabase( topElement.m_name, true );
			m_textStore.SetRawRefCount( strLocalIndex(handle), topElement.m_refCount );
			m_textStore.SetRawFlags( strLocalIndex(handle), topElement.m_flags );
		}
		else
		{
			m_textStore.SetRawRefCount( strLocalIndex(m_coreTextHandle), topElement.m_refCount );
			m_textStore.SetRawFlags( strLocalIndex(m_coreTextHandle), topElement.m_flags );
		}
	}

	// Force the load here, as other systems may be working under the assumption text was already loaded
	// and can cause issues when they attempt to use the hot-reloaded text assets
	strStreamingEngine::GetLoader().LoadAllRequestedObjects();

	return success;
}

bool fwTextDatabaseStreamingWrapper::LoadExtraTextMetafile( char const * const metafilePath )
{
	bool success = false;

	if( IsInitialized() )
	{
		fwExtraTextFiles extraTextBundle;

		if( PARSER.LoadObject( metafilePath, 0, extraTextBundle ) )
		{
			int loadedRPFCount = 0;
			for( int index = 0; index < extraTextBundle.m_FileTable.size(); ++index )
			{
				fwExtraTextPackage& currentElement = extraTextBundle.m_FileTable[ index ];
				loadedRPFCount += ( loadTextRPF( currentElement.m_PathFormatString, getLanguageOfCurrentTextDatabases(), currentElement.m_ShouldOverlay ) >= 0 ) ? 1 : 0;
			}

			success = ( loadedRPFCount == extraTextBundle.m_FileTable.size() );
			localisationAssertf( success, "FAILURE! Was only able to load %d of %d extra text RPFs. Check above log for failed loads.", loadedRPFCount, extraTextBundle.m_FileTable.size() );
		}
	}

	return success;
}

void fwTextDatabaseStreamingWrapper::UnloadExtraTextMetafile( char const * const metafilePath )
{
	if( IsInitialized() )
	{
		fwExtraTextFiles extraTextBundle;

		if( PARSER.LoadObject( metafilePath, 0, extraTextBundle ) )
		{
			for( int index = 0; index < extraTextBundle.m_FileTable.size(); ++index )
			{
				fwExtraTextPackage& currentElement = extraTextBundle.m_FileTable[ index ];
				unloadTextRPF( currentElement.m_PathFormatString );
			}
		}
	}
}

void fwTextDatabaseStreamingWrapper::Shutdown()
{
	if( !IsInitialized() )
		return;

	UnloadCoreText();
	m_textStore.RemoveAllAndUnregisterFromStreaming();

	while( m_activeTextRpfs.size() > 0 )
	{
		s32 rpfIndex = m_activeTextRpfs[0].m_activeIndex;
		unloadTextRPF( rpfIndex );
	}
	m_activeTextRpfs.ResetCount();

	m_activeLanguagePack = LANGUAGE_UNDEFINED;
}

void fwTextDatabaseStreamingWrapper::RegisterStreamingModule()
{
	m_textStore.RegisterStreamingModule();
}

TextDatabaseHandle fwTextDatabaseStreamingWrapper::RequestTextDatabase( char const * const databaseName, bool const immediateLoad )
{
	return RequestTextDatabase( rage::strStreamingObjectName( databaseName ), immediateLoad );
}

TextDatabaseHandle fwTextDatabaseStreamingWrapper::RequestTextDatabase( rage::strStreamingObjectName const & databaseName, bool const immediateLoad )
{
	u32 databaseSlot = m_textStore.FindSlot( databaseName ).Get();

	if( databaseSlot != -1 )
	{
		if( rage::sysThreadType::IsRenderThread() )
		{
			localisationVerifyf(  m_requests.Push( fwTextDatabaseStreamingRequest( databaseSlot,
				immediateLoad ? fwTextDatabaseStreamingRequest::ACTION_LOAD_DATABASE_IMMEDIATE : fwTextDatabaseStreamingRequest::ACTION_LOAD_DATABASE ) ),
				"Unable to request database, Request Queue is full!" );
		}
		else
		{
			RequestTextDatabaseInternal( databaseSlot, immediateLoad );
		}
	}

	return databaseSlot;
}

bool fwTextDatabaseStreamingWrapper::IsTextDatabaseLoading( TextDatabaseHandle const handle ) const
{
	bool isLoading = false;

	if( handle != -1 )
	{
		isLoading = m_textStore.IsObjectRequested( strLocalIndex(handle) ) || m_textStore.IsObjectLoading( strLocalIndex(handle) );
	}

	return isLoading;
}

bool fwTextDatabaseStreamingWrapper::IsTextDatabaseLoaded( TextDatabaseHandle const handle ) const
{
	bool isLoaded = false;

	if( handle != -1 )
	{
		isLoaded = m_textStore.HasObjectLoaded( strLocalIndex(handle) );
	}

	return isLoaded;
}

s32 fwTextDatabaseStreamingWrapper::GetDatabaseSlot( char const * const databaseName ) const
{
	return GetDatabaseSlot( atHashString( databaseName ) );
}

s32 fwTextDatabaseStreamingWrapper::GetDatabaseSlot( rage::strStreamingObjectName const & databaseName ) const
{
	return m_textStore.FindSlot( databaseName ).Get();
}

void fwTextDatabaseStreamingWrapper::AddTextDatabaseRef( TextDatabaseHandle const handle )
{
	if( handle != -1 )
	{
		if( rage::sysThreadType::IsRenderThread() )
		{
			localisationVerifyf( m_requests.Push( fwTextDatabaseStreamingRequest( handle, fwTextDatabaseStreamingRequest::ACTION_ADD_REF ) ),
				"Unable to add ref to database, Request Queue is full!" );
		}
		else
		{
			AddTextDatabaseRefInternal( handle );
		}
	}
}

void fwTextDatabaseStreamingWrapper::RemoveTextDatabaseRef( TextDatabaseHandle const handle )
{
	if( handle != -1 )
	{
		if( rage::sysThreadType::IsRenderThread() )
		{
			localisationVerifyf( m_requests.Push( fwTextDatabaseStreamingRequest( handle, fwTextDatabaseStreamingRequest::ACTION_REMOVE_REF ) ),
				"Unable to remove ref to database, Request Queue is full!" );
		}
		else
		{
			RemoveTextDatabaseRefInternal( handle );
		}
	}
}

bool fwTextDatabaseStreamingWrapper::DoesTextExist( rage::atHashString const& hashIdentifier ) const
{
	TextDatabaseHandle dummyOutParam;
	return FindTextInternal( -1, hashIdentifier, dummyOutParam ) != 0; // Use internal version to avoid error string and BANK overrides.
}

char const * fwTextDatabaseStreamingWrapper::FindText( rage::atHashString const& hashIdentifier ) const
{
	return FindText( -1, hashIdentifier );
}

char const * fwTextDatabaseStreamingWrapper::FindTextAndSlot( rage::atHashString const& hashIdentifier, TextDatabaseHandle& databaseFoundIn ) const
{
	return FindTextAndSlot( -1, hashIdentifier, databaseFoundIn );
}

char const * fwTextDatabaseStreamingWrapper::FindText( TextDatabaseHandle const handle, rage::atHashString const& hashIdentifier ) const
{
	TextDatabaseHandle dummyOutParam;
	return FindTextAndSlot( handle, hashIdentifier, dummyOutParam );
}

char const * fwTextDatabaseStreamingWrapper::FindTextAndSlot( TextDatabaseHandle const handle, rage::atHashString const& hashIdentifier, TextDatabaseHandle& databaseFoundIn ) const
{
	char const * pString;
#if __BANK

	if( ms_forceTextIds )
	{
		u32 const c_errorStringIndex = rage::sysThreadType::IsRenderThread() ? 0 : 1;

		char const * const pHashTextCode = hashIdentifier.TryGetCStr();
		formatf( ms_errorStringBuffer[c_errorStringIndex], "%s", pHashTextCode );
		return ms_errorStringBuffer[c_errorStringIndex];
	}

	pString = fwDebugLanguageStrings::GetString( hashIdentifier.GetHash() );
	if ( pString )
	{
		return pString;
	}

#endif //__BANK

	pString = FindTextInternal( handle, hashIdentifier, databaseFoundIn );

	if( pString == NULL )
	{
#if __ASSERT 
		if ( hashIdentifier.GetHash() != ATSTRINGHASH("Streetname",0x2dad6942))
		{	
			char const * const pHashTextCode = hashIdentifier.TryGetCStr();
			localisationDisplayf( "CLocalisation - %s label missing!", pHashTextCode );
			//localisationAssertf(0, "CLocalisation- %s label missing!", pHashTextCode );
		}
#endif // __ASSERT 

		u32 const c_errorStringIndex = rage::sysThreadType::IsRenderThread() ? 0 : 1;
#if __FINAL
		ms_errorStringBuffer[ c_errorStringIndex][0] = '\0';
#else	//	__FINAL
		char const * const pHashTextCode = hashIdentifier.TryGetCStr();
		formatf( ms_errorStringBuffer[c_errorStringIndex], "%s missing", pHashTextCode );
#endif	//	__FINAL

		pString = ms_errorStringBuffer[ c_errorStringIndex ];
	}

	return pString;
}

s32 fwTextDatabaseStreamingWrapper::loadTextRPF( char const * const pathFormat, sysLanguage const requestedLanguage, bool const overlay )
{
	s32 resultingIndex = -1;

	localisationAssertf( pathFormat, "Trying to load Text RPF with invalid path format!" );
	if( pathFormat )
	{
		char pathBuffer[ RAGE_MAX_PATH ];
		if( GenerateTextDatabasePath( requestedLanguage, pathBuffer, pathFormat ) )
		{
			resultingIndex = strPackfileManager::AddImageToList( pathBuffer, true, -1, false, 0, false, overlay );

			localisationAssertf( resultingIndex >= 0, "Unable to load requested text RPF %s!", pathBuffer );
			if( resultingIndex >= 0  )
			{
				//! We pass in the path format as we need this for dynamic reloading on language change, rather than the final path
				m_activeTextRpfs.Push( fwActiveTextRpf( pathFormat, resultingIndex, overlay ) );

				strPackfileManager::LoadImageIfUnloaded(resultingIndex, false);
				strPackfileManager::UnloadRpf( resultingIndex );
			}
		}
	}

	return resultingIndex;
}

void fwTextDatabaseStreamingWrapper::unloadTextRPF( s32 const rpfIndex )
{
	//! Remove from our active RPF listing
	for( int index = 0; index < m_activeTextRpfs.size(); ++index )
	{
		fwActiveTextRpf& element = m_activeTextRpfs[index];
		if( element.m_activeIndex == rpfIndex )
		{
			m_activeTextRpfs.erase( &element );
			break;
		}
	}

	strPackfileManager::RemoveUserLock(rpfIndex);
	strStreamingEngine::GetInfo().InvalidateFilesForArchive(rpfIndex, false );
	strPackfileManager::LoadImageIfUnloaded(rpfIndex, false);
	strPackfileManager::CloseArchive(rpfIndex);
	strPackfileManager::UnregisterArchive( rpfIndex );
}

void fwTextDatabaseStreamingWrapper::unloadTextRPF( char const * const pathFormat )
{
	s32 rpfIndex = -1;

	//! Remove from our active RPF listing
	for( int index = 0; index < m_activeTextRpfs.size(); ++index )
	{
		fwActiveTextRpf& element = m_activeTextRpfs[index];
		if( strcmp( element.m_pathFormat, pathFormat ) == 0 )
		{
			rpfIndex = element.m_activeIndex;
			break;
		}
	}

	if( rpfIndex >= 0 )
	{
		unloadTextRPF( rpfIndex );
	}
}

void fwTextDatabaseStreamingWrapper::LoadCoreText()
{
	if( m_coreTextHandle == -1 )
	{
		m_coreTextHandle = RequestTextDatabase( msc_coreTextFileName );
	}
}

void fwTextDatabaseStreamingWrapper::UpdateCoreTextLoading()
{
	if( m_coreTextHandle != -1 && !m_coreTextLoaded )
	{
		if( IsTextDatabaseLoaded( m_coreTextHandle ) )
		{
			AddTextDatabaseRef( m_coreTextHandle );
			m_coreTextLoaded = true;
		}
	}
}

void fwTextDatabaseStreamingWrapper::UnloadCoreText()
{
	if( m_coreTextHandle != -1 && m_coreTextLoaded )
	{
		RemoveTextDatabaseRef( m_coreTextHandle );
	}

	m_coreTextHandle = -1;
	m_coreTextLoaded = false;
}

void fwTextDatabaseStreamingWrapper::RequestTextDatabaseInternal( u32 const databaseSlot, bool const immediateLoad )
{
	if( !IsTextDatabaseLoaded( databaseSlot ) )
	{
		m_textStore.StreamingRequest( strLocalIndex(databaseSlot), STRFLAG_FORCE_LOAD );

		if( immediateLoad )
		{
			USE_MEMBUCKET(MEMBUCKET_STREAMING);
			strStreamingEngine::GetLoader().LoadAllRequestedObjects( false );
		}
	}
}

void fwTextDatabaseStreamingWrapper::AddTextDatabaseRefInternal( u32 const databaseSlot )
{
	m_textStore.AddRef( strLocalIndex(databaseSlot), REF_OTHER );
}

void fwTextDatabaseStreamingWrapper::RemoveTextDatabaseRefInternal( u32 const databaseSlot )
{
	m_textStore.RemoveRef( strLocalIndex(databaseSlot), REF_OTHER );
}

char const * fwTextDatabaseStreamingWrapper::FindTextInternal( TextDatabaseHandle const handle, rage::atHashString const& hashIdentifier, TextDatabaseHandle& databaseFoundIn ) const
{
	databaseFoundIn = -1;

	char const * result = 0;

	if( handle >= 0 )
	{
		result = IsInitialized() ? m_textStore.FindText( strLocalIndex(handle), hashIdentifier.GetHash() ) : NULL;
		databaseFoundIn = result != 0 ? handle : -1;
	}
	else
	{
		result = IsInitialized() ? m_textStore.FindTextAndSlot( hashIdentifier.GetHash(), databaseFoundIn ) : NULL;
	}

	return result;
}

bool fwTextDatabaseStreamingWrapper::GenerateTextDatabasePath( sysLanguage const requestedLanguagePack, char (&buffer)[RAGE_MAX_PATH], char const * const formatString )
{
	bool success = false;

	char const * const c_languagePackName = fwLanguagePack::GetLanguagePackName( requestedLanguagePack );
	if( c_languagePackName )
	{
		const char *suffix = "";
		PARAM_langfilesuffix.Get(suffix);

		formatf( buffer, RAGE_MAX_PATH, formatString, c_languagePackName, suffix );
		success = true;
	}

	return success;
}

void fwTextDatabaseStoreWrapper::Update()
{
	g_TextDatabaseStore.Update();
}

void fwTextDatabaseStoreWrapper::Shutdown(unsigned UNUSED_PARAM(shutdownMode) )
{
	g_TextDatabaseStore.Shutdown();
}

} // namespace rage
