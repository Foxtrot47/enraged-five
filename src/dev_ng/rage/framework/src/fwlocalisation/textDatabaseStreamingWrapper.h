/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textDatabaseStreamingWrapper.h 
// PURPOSE : Helper class to wrap our text streaming. Handles some additional
//			 mechanics to track streaming requests and hide it from other systems.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_TEXT_DATABASE_STREAMING_WRAPPER_H_
#define FWLOCALISATION_TEXT_DATABASE_STREAMING_WRAPPER_H_

// rage includes
#include "atl/queue.h"
#include "file/limits.h"

// framework includes:
#include "fwlocalisation/languagePack.h"
#include "fwscene/stores/textStore.h"
#include "streaming/streamingmodule.h"

namespace rage
{

//! Readability purposes
typedef int TextDatabaseHandle;

class fwTextDatabaseStreamingWrapper
{
public:
#if __BANK
	static bool ms_forceTextIds;
#endif // __BANK

	fwTextDatabaseStreamingWrapper();

	// PURPOSE: Update the current streaming request state
	// NOTES:
	//			Only to be called from the update thread!
	void Update();

	// PURPOSE: Register available text databases with the streaming system for the given language
	// PARAMS:
	//		requestedLanguagePack - Language pack we want text databases for
	// RETURNS:
	//		True if successful, false if failed to initialize with the given language
	bool Initialize( sysLanguage const requestedLanguagePack );

	// PURPOSE: Cleans up and re-loads the text streaming when we are switching to an new language
	// PARAMS:
	//		requestedLanguagePack - Language pack we are switching to
	// RETURNS:
	//		True if successful, false if failed to initialize with the given language
	bool ReloadDueToLanguageChange( sysLanguage const requestedLanguagePack );

	// PURPOSE: Loads extra text from the given meta-file
	// PARAMS:
	//		metafilePath - Path to the meta-file describing the extra text to load
	// RETURNS:
	//		True if successfully loaded, false otherwise.
	// NOTES:
	//		This just loads RPF files and registers there contents with streaming, you still need
	//		to request your specific text databases after calling this function
	bool LoadExtraTextMetafile( char const * const metafilePath );

	// PURPOSE: Unload a previously loaded extra-text meta-file
	// PARAMS:
	//		metafilePath - Path to a previously loaded extra-text meta-file
	void UnloadExtraTextMetafile( char const * const metafilePath );

	// PURPOSE: Cleanup text database registry
	void Shutdown();

	// PURPOSE: Register our streaming system
	void RegisterStreamingModule();

	// PURPOSE: Access to the streaming module we wrap
	strStreamingModule& GetStreamingModule()
	{
		return m_textStore;
	}

	// PURPOSE: Stream in the given text database
	// PARAMS:
	//		databaseName - Name of the database to stream in
	//		immediateLoad - Whether we should load this Database immediately. Used to replicate blocking loading
	//						of old text system for priority text. USE WITH CAUTION!
	// RETURNS:
	//		Int ID for the text database if successful, otherwise -1
	TextDatabaseHandle RequestTextDatabase( char const * const databaseName, bool const immediateLoad = false );
	TextDatabaseHandle RequestTextDatabase( rage::strStreamingObjectName const & databaseName, bool const immediateLoad = false );

	// PURPOSE: Check if the given database is in the process of loading
	// PARAMS:
	//		handle - Handle to a previously requested database
	// RETURNS:
	//		True if in the process of loading in, false otherwise
	bool IsTextDatabaseLoading( TextDatabaseHandle const handle ) const;

	// PURPOSE: Check if the given database is loaded.
	// PARAMS:
	//		handle - Handle to a previously requested database
	// RETURNS:
	//		True if streamed in, false otherwise
	bool IsTextDatabaseLoaded( TextDatabaseHandle const handle ) const;

	// PURPOSE: Return the handle to the core/persistent/global text
	// RETURNS:
	//		Text Database handle to the global text (-1 if not loaded)
	// NOTES:
	//		You can use this to directly query the global text block rather than traversing all
	//		loaded text blocks. Useful if you know the text you want is persistent.
	inline TextDatabaseHandle GetCoreTextHandle() const
	{
		return m_coreTextHandle;
	}

	// PURPOSE: Find the slot that the given database will be loaded in to
	// PARAMS:
	//		databaseName - Name of the database to look for in our RPFs
	// RETURNS:
	//		>= 0 if exists, -1 if no such database/slot exists
	// NOTES:
	//		Used for script resource loading.
	s32 GetDatabaseSlot( char const * const databaseName ) const;
	s32 GetDatabaseSlot( rage::strStreamingObjectName const & databaseName ) const;

	// PURPOSE: Add a reference to the given text database
	// PARAMS:
	//		handle - Handle to a previously requested database
	// NOTES:
	//		Assumes IsTextDatabaseLoaded has been used to check the database is indeed loaded.
	void AddTextDatabaseRef( TextDatabaseHandle const handle );

	// PURPOSE: Release a reference to the given text database
	// PARAMS:
	//		handle - Handle to a previously requested database
	// NOTES:
	//		Only call this if you previously called AddTextDatabaseRef for the given database!
	void RemoveTextDatabaseRef( TextDatabaseHandle const handle );

	// PURPOSE: Check if a given string exists in any text database
	// PARAMS:
	//		hashIdentifier - Hash identifier to search for
	// RETURNS:
	//		True if found, false otherwise
	bool DoesTextExist( rage::atHashString const& hashIdentifier ) const;

	// PURPOSE: Search through a specific text database and check if a given string exists
	// PARAMS:
	//		handle - Handle to a specific text database
	//		hashIdentifier - Hash identifier to search for
	// RETURNS:
	//		True if found, false otherwise
	bool DoesTextExist( TextDatabaseHandle const handle, rage::atHashString const& hashIdentifier ) const
	{
		return FindText( handle, hashIdentifier ) != 0;
	}

	// PURPOSE: Search through the all loaded text and try and find a string with the given
	//			identifier
	// PARAMS:
	//		hashIdentifier - Hash identifier to search for
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindText( rage::atHashString const& hashIdentifier ) const;

	// PURPOSE: Search through the all loaded text and try and find a string with the given
	//			identifier
	// PARAMS:
	//		hashIdentifier - Hash identifier to search for
	//		databaseFoundIn - Database this text was found in, if found
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindTextAndSlot( rage::atHashString const& hashIdentifier, TextDatabaseHandle& databaseFoundIn ) const;

	// PURPOSE: Search through a specific text database for text
	// PARAMS:
	//		handle - Handle to a specific text database
	//		hashIdentifier - Hash identifier to search for
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindText( TextDatabaseHandle const handle, rage::atHashString const& hashIdentifier ) const;

	// PURPOSE: Search through a specific text database for text
	// PARAMS:
	//		handle - Handle to a specific text database
	//		hashIdentifier - Hash identifier to search for
	//		databaseFoundIn - Database this text was found in, if found
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindTextAndSlot( TextDatabaseHandle const handle, rage::atHashString const& hashIdentifier, TextDatabaseHandle& databaseFoundIn ) const;

	// PURPOSE: Return if the persistent text is loaded.
	// RETURNS:
	//		True if loaded, false otherwise.
	inline bool IsCoreTextLoaded() const { return m_coreTextLoaded; }

	// PURPOSE: Get a Language Pack ID for the currently registered text databases
	// RETURNS:
	//		Language Pack ID for current text databases, or undefined if none is loaded.
	inline sysLanguage getLanguageOfCurrentTextDatabases() const { return m_activeLanguagePack; }

	// PURPOSE: Gets if we are initialized.
	// RETURNS:
	//		True if initialized, false otherwise
	inline bool IsInitialized() const { return getLanguageOfCurrentTextDatabases() != LANGUAGE_UNDEFINED && m_activeTextRpfs.size() > 0; }

private:

	// PURPOSE: Helper class for tracking streaming requests from render thread.
	class fwTextDatabaseStreamingRequest
	{
	public:
		enum ACTION
		{
			ACTION_LOAD_DATABASE,
			ACTION_LOAD_DATABASE_IMMEDIATE,
			ACTION_ADD_REF,
			ACTION_REMOVE_REF,

			MAX_ACTION
		};

		fwTextDatabaseStreamingRequest()
			: m_databaseHandle( -1 )
			, m_action( MAX_ACTION )
		{

		}

		fwTextDatabaseStreamingRequest( TextDatabaseHandle const targetHandle, ACTION const targetAction )
			: m_databaseHandle( targetHandle )
			, m_action( targetAction )
		{

		}

		explicit fwTextDatabaseStreamingRequest( fwTextDatabaseStreamingRequest const& other )
			: m_databaseHandle( other.m_databaseHandle )
			, m_action( other.m_action )
		{

		}

		fwTextDatabaseStreamingRequest& operator=( fwTextDatabaseStreamingRequest const& other )
		{
			if( this != &other )
			{
				m_databaseHandle = other.m_databaseHandle;
				m_action = other.m_action;
			}

			return *this;
		}

		inline TextDatabaseHandle GetTargetHandle() const { return m_databaseHandle; }
		inline ACTION GetAction() const { return m_action; }

	private:
		TextDatabaseHandle	m_databaseHandle;
		ACTION				m_action;
	};

	// PURPOSE: Helper for tracking and reloading RPF files
	struct fwActiveTextRpf
	{
		fwActiveTextRpf()
			: m_pathFormat()
			, m_activeIndex( -1 )
			, m_isOverlay( false )
		{

		}

		fwActiveTextRpf( char const * const pathFormat, s32 const activeIndex, bool const isOverlay )
			: m_pathFormat( pathFormat )
			, m_activeIndex( activeIndex )
			, m_isOverlay( isOverlay )
		{

		}

		atString	m_pathFormat;
		s32			m_activeIndex;
		bool		m_isOverlay;
	};

	// NOTES: Queue size was arbitrarily picked as best fit during testing. Adjust as needed.
	typedef atQueue< fwTextDatabaseStreamingRequest, 4 > StreamingRequestQueue;
	typedef atArray<fwActiveTextRpf> ActiveRpfArray;

	static char const * const msc_textDatabasePathFormat;
	static rage::strStreamingObjectName const msc_coreTextFileName;

	rage::fwTextStore			m_textStore;
	StreamingRequestQueue		m_requests;
	ActiveRpfArray				m_activeTextRpfs;
	sysLanguage					m_activeLanguagePack;
	TextDatabaseHandle			m_coreTextHandle;
	bool						m_coreTextLoaded;

	//! Buffer for temporary generation of error strings
	static char ms_errorStringBuffer[2][64];

	// PURPOSE: Loads the RPF from the given path and adds it to our array of active RPFs
	// PARAMS:
	//		pathFormat			- Path to the RPF we want to load
	//		requestedLanguage	- Language version we want to load
	//		overlay				- Whether we should overlay this RPF or not
	// RETURNS:
	//		Index of the RPF loaded if successful (0 or greater), Negative value if failed
	s32 loadTextRPF( char const * const pathFormat, sysLanguage const requestedLanguage, bool const overlay );

	// PURPOSE: Unloads the given RPF by index
	// PARAMS:
	//		rpfIndex - Index to a loaded RPF file
	void unloadTextRPF( s32 const rpfIndex );
	void unloadTextRPF( char const * const rpfPath );

	// PURPOSE: Loads persistent game text
	void LoadCoreText();

	// PURPOSE: Update the state of our persistent text being loaded
	void UpdateCoreTextLoading();

	// PURPOSE: Unload the persistent text
	void UnloadCoreText();

	// PURPOSE: Request the streaming in of a given database
	// PARAMS:
	//		databaseSlot - Internal slot of the database we are operating on
	//		immediateLoad - Whether we should load this Database immediately. Used to replicate blocking loading
	//						of old text system for priority text. USE WITH CAUTION!
	void RequestTextDatabaseInternal( u32 const databaseSlot, bool const immediateLoad );

	// PURPOSE: Add a reference to the given text database
	// PARAMS:
	//		databaseSlot - Internal slot of the database we are operating on
	void AddTextDatabaseRefInternal( u32 const databaseSlot );

	// PURPOSE: Release a reference to the given text database
	// PARAMS:
	//		databaseSlot - Internal slot of the database we are operating on
	void RemoveTextDatabaseRefInternal( u32 const databaseSlot );

	// PURPOSE: Search through a specific text database for text
	// PARAMS:
	//		handle - Handle to a specific text database
	//		hashIdentifier - Hash identifier to search for
	//		databaseFoundIn - Database this text was found in, if found
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindTextInternal( TextDatabaseHandle const handle, atHashString const& hashIdentifier, TextDatabaseHandle& databaseFoundIn ) const;

	// PURPOSE: Generate a path the given language
	// PARAMS:
	//		requestedLanguagePack	- Language pack we want the name for
	//		buffer					- Fixed size buffer to format the path into
	//		formatString			- Format string to use (Optional, has default)
	static bool GenerateTextDatabasePath( sysLanguage const requestedLanguagePack, char (&buffer)[RAGE_MAX_PATH], char const * const formatString );

};

// PURPOSE: wrapper class needed to interface with game skeleton code
class fwTextDatabaseStoreWrapper
{
public:
	static void Update();
	static void Shutdown(unsigned shutdownMode);
};

extern fwTextDatabaseStreamingWrapper g_TextDatabaseStore;

} // namespace rage

#endif // FWLOCALISATION_TEXT_DATABASE_STREAMING_WRAPPER_H_
