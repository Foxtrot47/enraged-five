/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : templateString.h
// PURPOSE : Template class for fixed-size string buffers
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWLOCALISATION_TEMPLATE_STRING_H
#define FWLOCALISATION_TEMPLATE_STRING_H

// Rage Includes:
#include "string/string.h"
#include "file/limits.h"

// Framework includes
#include "textTypes.h"
#include "textUtil.h"

namespace rage
{

	template< size_t const _BufferSize, size_t const _maxUtf8Bytes = 3 >
	class fwTemplateString
	{
	public:
		typedef fwTemplateString< _BufferSize > _ThisType;
		typedef char TextBuffer[ _BufferSize ];

		fwTemplateString() 
		{ 
			Clear(); 
		}

        fwTemplateString( char const * const string )
        {
            Clear();

            if( string )
            {
                append( string );
            }
        }

		fwTemplateString( char const * const string, va_list args )
		{
			FormatText( string, getWritableBuffer(), _BufferSize, args);
		}

		~fwTemplateString() {}

		bool operator==( char const * string ) const { return strcmpi( m_buffer, string ) == 0; }
		bool operator==( fwTemplateString const& string ) const { return strcmpi( m_buffer, string.getBuffer() ) == 0; }
		
		bool operator!=( char const * string ) const { return strcmpi( m_buffer, string ) != 0; }
		bool operator!=( fwTemplateString const& string ) const { return strcmpi( m_buffer, string.getBuffer() ) != 0; }

        fwTemplateString& operator+=( char const * string ) { append( string ); return *this; }
        fwTemplateString& operator+=( fwTemplateString const& string ) { append( string.getBuffer() ); return *this; }
		
		// PURPOSE: Array accessor
		// PARAMS: index - Index of (range-checked) character to return
		// RETURNS: Character at that address
		char& operator[] ( size_t index ) 
		{
			Assertf( index < _BufferSize, "CTemplateString::operator[] - Index out of bounds!" );
			return m_buffer[ index ];
		}

		// PURPOSE: Array accessor
		// PARAMS: index - Index of (range-checked) character to return
		// RETURNS: Character at that address
		char operator[] ( size_t index ) const 
		{
			Assertf( index < _BufferSize, "CTemplateString::operator[] - Index out of bounds!" );
			return m_buffer[ index ];
		}

		inline TextBuffer const& getBufferRef() const { return m_buffer; }
		inline TextBuffer& getBufferRef() { return m_buffer; }

		operator const char* () const { return getBuffer(); }
		inline const char* getBuffer() const { return m_buffer; }

		operator char*() { return getWritableBuffer(); }
		inline char* getWritableBuffer() { return m_buffer; }
		
		void operator=( const char* string ) 
		{ 
			Set( string );
		}

        void append( char const * string )
        {
            safecat( m_buffer, string );
        }

		// PURPOSE: Set the value of this string based on another given format string
		// PARAMS:
		//		string - String we are setting by
		void sprintf( char const * const format, ... )
		{
			if( format )
			{
				va_list	ap;
				va_start(ap, format);	
					FormatText( format, m_buffer, _BufferSize, ap);
				va_end(ap);
			}
			else
			{
				Clear();
			}
		}

		// PURPOSE: Set the value of this string based on another given string
		// PARAMS:
		//		string - String we are setting by
		void Set( char const * const string )
		{
			if( string )
			{
				safecpy( m_buffer, string );
				Utf8RemoveInvalidSurrogates(m_buffer);
			}
			else
			{
				Clear();
			}
		}

		// PURPOSE: Reset this string to be null/empty
		void Clear() 
		{ 
			m_buffer[0] = rage::TEXT_CHAR_TERMINATOR; 
		}

		// PURPOSE: Find the first index of the given character in this string.
		// PARAMS:
		//		character - character to look for
		//		startIndex - Starting index to search for
		// RETURNS:
		//		-1 if not found, otherwise the index the character was found at
		int IndexOf( const char character, size_t startIndex = 0 )
		{
			int result = -1;

			for( size_t index = startIndex; index < _BufferSize && m_buffer[ index ] != rage::TEXT_CHAR_TERMINATOR; ++index )
			{
				if( m_buffer[ index ] == character )
				{
					result = (int)index;
					break;
				}
			}

			return result;
		}

        // PURPOSE: Find the first index of the given string in this string.
        // PARAMS:
        //		string - string to look for
        //		startIndex - Starting index to search for
        // RETURNS:
        //		-1 if not found, otherwise the index the character was found at
        int IndexOf( char const * const string, size_t startIndex = 0 )
        {
            int result = -1;

            char const * const c_foundString = ( startIndex < GetLength() && string ) ? strstr( &m_buffer[ startIndex ], string ) : NULL;
            result = c_foundString ? c_foundString - m_buffer : result;

            return result;
        }

		// PURPOSE: In-place Truncate the string at the given index
		// PARAMS:
		//		index - Index to truncate at
		// NOTES:
		//		If index is out of bounds, the string will be untouched
		void Truncate( size_t index )
		{
			Assertf( index < _BufferSize, "CTemplateString::Truncate - Index out of bounds" );
			if( index < _BufferSize )
			{
				m_buffer[ index ] = rage::TEXT_CHAR_TERMINATOR;
			}
		}
        void TrimLeft()
        {
            int const c_length = (int)GetByteCount() - 1;
            int i;

            for( i = 0; i < c_length; )
            {
                u32 const c_characterByteCount = fwTextUtil::ByteCountForCharacter( m_buffer + i );
                if( !fwTextUtil::IsSpace( m_buffer + i ) )
                {
                    if ( i > 0 )
                    {
                        for ( size_t j = i; j < c_length; ++j )
                        {
                            m_buffer[j - i] = m_buffer[j];
                        }

                        Truncate( c_length - i );
                    }

                    break;
                }

                i += c_characterByteCount;
            }

            //! All characters were spaces, so just abandon and clear our string
            if( i == c_length )
            {
                Clear();
            }

            Utf8RemoveInvalidSurrogates( m_buffer );
        }

        void TrimRight()
        {
            int const c_length = (int)GetByteCount() - 1;
            int lastNonSpace = -1;
            u32 lastNonSpaceByteCount = 0;

            for( int i = 0; i < c_length; )
            {
                u32 const c_characterByteCount = fwTextUtil::ByteCountForCharacter( m_buffer + i );

                bool const c_isSpace = fwTextUtil::IsSpace( m_buffer + i );
                lastNonSpace = c_isSpace ? lastNonSpace : i;
                lastNonSpaceByteCount = c_isSpace ? lastNonSpaceByteCount : c_characterByteCount;

                i += c_characterByteCount;
            }

            if( lastNonSpace >= 0 )
            {
                Truncate( lastNonSpace + lastNonSpaceByteCount );
            }
            else
            {
                Clear();
            }

            Utf8RemoveInvalidSurrogates( m_buffer );
        }
        
        // PURPOSE: In-place trim whitespace from before/after contents
        void Trim()
        {
            TrimLeft();
            TrimRight();
        }

        /* Not tested yet, so left out for now.

        void Replace( char const * const search, char const * const replace )
        {
            int const c_searchLength = search ? (int)strlen(search) : NULL;
            int const c_replaceLength = replace ? (int)strlen( replace ) : NULL;

            int pos = IndexOf( search );
            while( c_searchLength > 0 && c_replaceLength > 0 && pos != -1 )
            {
                fwTemplateString temp1;
                safecpy( temp1.getWritableBuffer(), m_buffer, pos );

                fwTemplateString temp2;
                safecpy( temp2.getWritableBuffer(), m_buffer + pos + c_searchLength, temp2.GetMaxLength() );

                Set( temp1 );
                append( replace );
                append( temp2 );   

                pos = IndexOf( search, pos + c_replaceLength );
            }
        }

        void Replace( fwTemplateString const& search, fwTemplateString const& replace )
        {
            Replace( search.getBuffer(), replace.getBuffer() );
        }*/

		// PURPOSE: Makes the string upper/lowercase, in-place
		inline void Lowercase() { fwTextUtil::MakeLowerCaseAsciiOnly( m_buffer ); }
		inline void Uppercase() { fwTextUtil::MakeUpperCaseAsciiOnly( m_buffer ); }

        // TODO - This is incorrectly returning the byte count. That is bad. Fix it for BOB, as we aren't sure if it'll blow up current usage if we change it.
		inline size_t GetLength() const { return strlen(m_buffer); }
        inline size_t GetByteCount() const { return fwTextUtil::GetByteCount( m_buffer ) + 1; }

        inline static size_t GetMaxLength() { return ( _BufferSize - 1 ) / _maxUtf8Bytes; }
        inline static size_t GetMaxLengthWithTerminator() { return ( ( _BufferSize - 1 ) / _maxUtf8Bytes ) + 1; }
        inline static size_t GetMaxByteCount() { return _BufferSize; }

		inline bool IsNullOrEmpty() const 
		{ 
			return m_buffer[0] == rage::TEXT_CHAR_TERMINATOR;
		}

	protected:
		//CTemplateString( const CTemplateString& other);
		//CTemplateString& operator=( const CTemplateString& other);

		//! Formats the text string into the given buffer
		static void FormatText( char const * const format, char* destBuffer, int bufferSize, va_list varArgs)
		{
			if( format && destBuffer && ( bufferSize > 0 ) )
			{
				rage::vformatf_n( destBuffer, bufferSize, format, varArgs);
			}
		}

	private:
		TextBuffer m_buffer;
	};

	typedef fwTemplateString<8>						SimpleString_8;
	typedef fwTemplateString<16>					SimpleString_16;
	typedef fwTemplateString<32>					SimpleString_32;
	typedef fwTemplateString<64>					SimpleString_64;
	typedef fwTemplateString<128>					SimpleString_128;
	typedef fwTemplateString<256>					SimpleString_256;
	typedef fwTemplateString<rage::RAGE_MAX_PATH>	PathBuffer;
	typedef fwTemplateString<rage::LANG_KEY_SIZE>	LocalizationKey;

} // namespace rage

#endif // FWLOCALISATION_TEMPLATE_STRING_H
