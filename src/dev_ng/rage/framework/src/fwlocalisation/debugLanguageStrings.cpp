/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : debugLanguageStrings.cpp 
// PURPOSE : Maintains a collection of debug language strings not loaded from 
//			 language data.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#if __BANK

// rage includes
#include "string/stringhash.h"
#include "system/new.h"
#include "system/threadtype.h"

// Framework includes
#include "DebugLanguageStrings.h"
#include "localisation_channel.h"
#include "textUtil.h"

namespace rage
{

sysCriticalSectionToken fwDebugLanguageStrings::ms_criticalToken;
fwDebugLanguageStrings::fwDebugStringMap fwDebugLanguageStrings::ms_stringMap;

void fwDebugLanguageStrings::fwDebugStringRecord::Set( char const * const pText )
{
	if( m_string )
	{
		delete[] m_string;
		m_string = NULL;
	}

	if( pText )
	{
		s32 string_length = istrlen(pText) + 1;
		m_string = rage_aligned_new(16) char[ string_length ];	//	+1 for the NULL terminator
		m_string[0] = '\0';


		safecpy( m_string, pText, string_length );
	}

	SetDeletionFlag( m_string != 0 );
}

void fwDebugLanguageStrings::Add( char const * const pTextKey, char const * const pTextToDisplay )
{
	sysCriticalSection criticalSection( ms_criticalToken );

	u32 HashKey = atStringHash(pTextKey);
	if ( localisationVerifyf( rage::sysThreadType::IsUpdateThread(), "fwDebugLanguageStrings::Add - Didn't expect this to be called by the render thread") )
	{
		ms_stringMap[HashKey].Set( pTextToDisplay );
	}
}

void fwDebugLanguageStrings::FlagForDeletion( char const * const pTextKey )
{
	sysCriticalSection criticalSection( ms_criticalToken );

	if ( localisationVerifyf( rage::sysThreadType::IsUpdateThread(), "fwDebugLanguageStrings::FlagForDeletion - Didn't expect this to be called by the render thread") )
	{
		fwDebugStringRecord* pDebugString = ms_stringMap.Access( atStringHash( pTextKey ) );
		if ( pDebugString )
		{
			pDebugString->SetDeletionFlag(true);
		}
	}
}

void fwDebugLanguageStrings::FlagAllForDeletion()
{
	sysCriticalSection criticalSection( ms_criticalToken );

	if ( localisationVerifyf( rage::sysThreadType::IsUpdateThread(), "fwDebugLanguageStrings::FlagAllForDeletion - Didn't expect this to be called by the render thread") )
	{
		fwDebugStringIterator itr = ms_stringMap.CreateIterator();
		while (!itr.AtEnd())
		{
			itr.GetData().SetDeletionFlag(true);

			itr.Next();
		}
	}
}

void fwDebugLanguageStrings::DeleteAllFlagged()
{
	sysCriticalSection criticalSection( ms_criticalToken );

	fwDebugStringIterator itr = ms_stringMap.CreateIterator();
	while ( !itr.AtEnd() )
	{
		if ( itr.GetData().IsFlaggedForDeletion() )
		{
			u32 KeyOfEntryToBeDeleted = itr.GetKey();
			ms_stringMap.Delete( KeyOfEntryToBeDeleted );

			itr.Start();	//	Start from the beginning of the map again after deleting an entry
		}
		else
		{
			itr.Next();
		}
	}
}

char const * fwDebugLanguageStrings::GetString( u32 textHash )
{
	sysCriticalSection criticalSection( ms_criticalToken );

	const fwDebugStringRecord *pDebugString = ms_stringMap.Access( textHash );
	return pDebugString ? pDebugString->GetString() : NULL;
}

} // namespace rage

#endif // __BANK
