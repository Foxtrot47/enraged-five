/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : languagePack.cpp
// PURPOSE : Enum of supported languages along with helper functions for validation,
//			 conversion and basic checks.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

// rage includes:
#include "string/string.h"
#include "system/nelem.h"
#include "system/param.h"

// framework includes:
#include "LanguagePack.h"
#include "localisation_channel.h"
#include "unit_conversion.h"

#include <stdio.h>

namespace rage
{

RAGE_DEFINE_CHANNEL(localisation_chan)

PARAM(fontlib, "[text] font library to use (ppefigs, japanese, korrean, russian, trad_chinese, efigs)");

// Array contents needs to match the order in 
// X:\gta5\src\dev_ng_live\rage\base\src\system\language.h
char const * const fwLanguagePack::ms_TextLanguageFileNames[ MAX_LANGUAGES ] = 
{
	"american",
	"french",
	"german",
	"italian",
	"spanish",
	"portuguese",
	"polish",
	"russian",
	"korean",
	"chinese",
	"japanese",
    "mexican",
#if RSG_ORBIS || RSG_DURANGO
    "chinese"
#else
    "chinesesimp"
#endif // RSG_ORBIS || RSG_DURANGO
};

// Array contents needs to match the order in 
// X:\gta5\src\dev_ng_live\rage\base\src\system\language.h
char const * const fwLanguagePack::ms_LanguageIsoCodes[ MAX_LANGUAGES ] = 
{
	"en",
	"fr",
	"de",
	"it",
	"es",
	"pt",
	"pl",
	"ru",
	"ko",
	"zh",
	"ja",
    "mx",
    "zh",
};

char const * const fwLanguagePack::ms_dateDelimiters[ MAX_DATE_FORMAT_DELIMITERS ] =
{
	"/",
	"-",
	".",
	" "
};

#if __BANK

int fwLanguagePack::m_eUnitOverride = fwLanguagePack::UNIT_OVERRIDE_NONE;

#endif // __BANK

char const * const fwLanguagePack::ms_weightFormatMetric		= "%dkgs";
char const * const fwLanguagePack::ms_weightFormatImperial		= "%dlbs";

char const * const fwLanguagePack::ms_currencyFormatLeft		= "$%d";
char const * const fwLanguagePack::ms_currencyFormatRight		= "%d$";

char const * const fwLanguagePack::ms_currencyDelimiterComma	= ",";
char const * const fwLanguagePack::ms_currencyDelimiterPeriod	= ".";
char const * const fwLanguagePack::ms_currencyDelimiterSpace	= " ";

char const * fwLanguagePack::GetLanguagePackName( sysLanguage const eLanguage )
{
	localisationAssertf( eLanguage != LANGUAGE_UNDEFINED && eLanguage < MAX_LANGUAGES, "Language %d out of bounds", (u32)eLanguage );
	return (eLanguage != LANGUAGE_UNDEFINED && eLanguage < MAX_LANGUAGES) ? ms_TextLanguageFileNames[ eLanguage ] : ms_TextLanguageFileNames[0];
}

rage::rlScLanguage fwLanguagePack::GetScLanguageFromLanguage( sysLanguage const language )
{
	switch ( language )
	{
	case LANGUAGE_ENGLISH :
		return RLSC_LANGUAGE_ENGLISH;

	case LANGUAGE_FRENCH :
		return RLSC_LANGUAGE_FRENCH;

	case LANGUAGE_GERMAN :
		return RLSC_LANGUAGE_GERMAN;

	case LANGUAGE_ITALIAN :
		return RLSC_LANGUAGE_ITALIAN;

	case LANGUAGE_SPANISH :
		return RLSC_LANGUAGE_SPANISH;

	case LANGUAGE_PORTUGUESE :
		return RLSC_LANGUAGE_PORTUGUESE_BRAZILIAN;

	case LANGUAGE_POLISH :
		return RLSC_LANGUAGE_POLISH;

	case LANGUAGE_RUSSIAN :
		return RLSC_LANGUAGE_RUSSIAN;

	case LANGUAGE_KOREAN :
		return RLSC_LANGUAGE_KOREAN;

	case LANGUAGE_CHINESE_TRADITIONAL :
		return RLSC_LANGUAGE_CHINESE;

	case LANGUAGE_CHINESE_SIMPLIFIED :
		return RLSC_LANGUAGE_CHINESE_SIMPILIFIED;

	case LANGUAGE_JAPANESE :
		return RLSC_LANGUAGE_JAPANESE;

	case LANGUAGE_MEXICAN :
		return RLSC_LANGUAGE_SPANISH_MEXICAN;

	default :
		localisationAssertf(0, "Unsupported language setting %d so returning RLSC_LANGUAGE_ENGLISH", language );
	}

	return RLSC_LANGUAGE_ENGLISH;
}

char const * fwLanguagePack::GetStringOfSupportedScLanguages( sysLanguage const language )
{
	static char languagesString[128];

	rage::rlScLanguage nonAsianLanguages[] = {
		RLSC_LANGUAGE_ENGLISH, RLSC_LANGUAGE_FRENCH, RLSC_LANGUAGE_GERMAN,
		RLSC_LANGUAGE_ITALIAN, RLSC_LANGUAGE_SPANISH, RLSC_LANGUAGE_PORTUGUESE_BRAZILIAN,
		RLSC_LANGUAGE_POLISH, RLSC_LANGUAGE_RUSSIAN, RLSC_LANGUAGE_SPANISH_MEXICAN
	};

	rage::rlScLanguage scLanguage = GetScLanguageFromLanguage( language );
	formatf(languagesString, sizeof(languagesString), "'%s'", rage::rlScLanguageToString(scLanguage));

	switch (language)
	{
	case RLSC_LANGUAGE_ENGLISH :
	case RLSC_LANGUAGE_FRENCH :
	case RLSC_LANGUAGE_GERMAN :
	case RLSC_LANGUAGE_ITALIAN :
	case RLSC_LANGUAGE_SPANISH :
	case RLSC_LANGUAGE_PORTUGUESE_BRAZILIAN:
	case RLSC_LANGUAGE_POLISH :
	case RLSC_LANGUAGE_RUSSIAN :
	case RLSC_LANGUAGE_SPANISH_MEXICAN :
		//	Append all other non-Asian languages
		for (u32 loop = 0; loop < NELEM(nonAsianLanguages); loop++)
		{
			if (nonAsianLanguages[loop] != scLanguage)
			{
				safecat(languagesString, ",'");
				safecat(languagesString, rage::rlScLanguageToString(nonAsianLanguages[loop]));
				safecat(languagesString, "'");
			}
		}
		break;

	case RLSC_LANGUAGE_KOREAN :
    case RLSC_LANGUAGE_CHINESE :
    case RLSC_LANGUAGE_CHINESE_SIMPILIFIED :
	case RLSC_LANGUAGE_JAPANESE :
		//	Append English
		safecat(languagesString, ",'");
		safecat(languagesString, rage::rlScLanguageToString(RLSC_LANGUAGE_ENGLISH));
		safecat(languagesString, "'");
		break;

	default :
		break;
	}

	return languagesString;
}

char const * fwLanguagePack::GetIsoLanguageCode( sysLanguage const language )
{
	localisationAssertf( language != LANGUAGE_UNDEFINED && language < MAX_LANGUAGES, "Language %d out of bounds", (u32)language );
	return (language != LANGUAGE_UNDEFINED && language < MAX_LANGUAGES) ? ms_LanguageIsoCodes[ language ] : ms_LanguageIsoCodes[0];
}

char const * fwLanguagePack::GetFontLibraryName( sysLanguage const language )
{
#if !__FINAL
	const char* pFontLibName = NULL;
	PARAM_fontlib.Get(pFontLibName);

	if (pFontLibName)
	{
		return pFontLibName;
	}
#endif // !__FINAL

	switch ( language )
	{
		case LANGUAGE_JAPANESE:
		{
			return "japanese";
		}

		case LANGUAGE_CHINESE_TRADITIONAL:
		{
			return "chinese";
		}

		case LANGUAGE_CHINESE_SIMPLIFIED:
		{
#if RSG_ORBIS || RSG_DURANGO
            return "chinese";
#else
            return "chinesesimp";
#endif // RSG_ORBIS || RSG_DURANGO
		}

		case LANGUAGE_KOREAN:
		{
			return "korean";
		}

		default:
		{
			return "efigs";
		}
	}
}

char const * fwLanguagePack::GetCurrencyDelimiter( sysLanguage const language )
{
	switch( language )
	{
	case LANGUAGE_ITALIAN:
	case LANGUAGE_GERMAN:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_POLISH:
		return ms_currencyDelimiterPeriod;

	case LANGUAGE_SPANISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_RUSSIAN:
		return ms_currencyDelimiterSpace;

	case LANGUAGE_ENGLISH:
	case LANGUAGE_KOREAN:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	default:
		return  ms_currencyDelimiterComma;
	}
}

float fwLanguagePack::ConvertMetersToDisplaySpeed( sysLanguage const language, float const metres )
{
	bool const bMetric = IsMetric( language );
	return bMetric ? METERS_TO_KILOMETERS( metres ) : METERS_TO_MILES( metres );
}

float fwLanguagePack::ConvertKilometersToDisplaySpeed( sysLanguage const language, float const kilometres )
{
	bool const bMetric = IsMetric( language );
	return bMetric ? kilometres : KILOMETERS_TO_MILES( kilometres );
}

float fwLanguagePack::ConvertMetersToDisplayDistance( sysLanguage const language, float const metres )
{
	bool const bMetric = IsMetric( language );
	return bMetric ? METERS_TO_KILOMETERS( metres ) : METERS_TO_MILES( metres );
}

float fwLanguagePack::ConvertMetersToDisplayDistanceShort( sysLanguage const language, float const metres )
{
	bool const bMetric = IsMetric( language );
	return bMetric ? metres : METERS_TO_FEET( metres );
}

void fwLanguagePack::FormatValueAsCurrency( sysLanguage const language, char * buffer, int const valueToDisplay )
{
	localisationAssert( buffer );
	if( buffer )
	{
		char const * const currencyFormat = GetCurrencyFormatString( language );
		::sprintf( buffer, currencyFormat, valueToDisplay );
	}
}

void fwLanguagePack::FormatValueAsWeight( sysLanguage const language, char * buffer, int const weightInLbs )
{
	localisationAssert( buffer );
	if( buffer )
	{
		const bool bMetric = IsMetric( language );
		char const * const formatString = bMetric ? ms_weightFormatMetric : ms_weightFormatImperial;
		int convertedValue = bMetric ? (int)LBS_TO_KGS( weightInLbs ) : weightInLbs;

		::sprintf( buffer, formatString, convertedValue );
	}
}

#if __BANK

sysLanguage fwLanguagePack::GetLanguageFromName( char const * const pLanguageName )
{
	int result = LANGUAGE_UNDEFINED;

	for( int i = 0; i < MAX_LANGUAGES; ++i )
	{
		if( !strcmp( pLanguageName, ms_TextLanguageFileNames[i] ) )
		{
			result = i;
			break;
		}
	}

	return (sysLanguage)result;
}

#endif // __BANK

bool fwLanguagePack::IsAsianLanguage( sysLanguage const language )
{
	switch ( language )
	{
	case LANGUAGE_JAPANESE:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	case LANGUAGE_KOREAN:
		{
			return true;
		}

	default:
		{
			return false;
		}
	}
}

bool fwLanguagePack::IsRussianLanguage( sysLanguage const language )
{
	return language == LANGUAGE_RUSSIAN;
}

bool fwLanguagePack::IsMetric( sysLanguage const language )
{
#if __BANK
	if( fwLanguagePack::m_eUnitOverride != fwLanguagePack::UNIT_OVERRIDE_NONE )
		return ( fwLanguagePack::m_eUnitOverride == fwLanguagePack::UNIT_OVERRIDE_METRIC );
#endif

	return language != LANGUAGE_ENGLISH;
}

fwLanguagePack::DATE_FORMAT fwLanguagePack::GetDateFormatType( sysLanguage const language )
{
	fwLanguagePack::DATE_FORMAT resultFormat = DATE_FORMAT_MDY; // Assume American... because that's how we do it elsewhere

	switch( language )
	{
	case LANGUAGE_FRENCH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_MEXICAN:
	case LANGUAGE_POLISH:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_RUSSIAN:
	case LANGUAGE_SPANISH:
		resultFormat = DATE_FORMAT_DMY;
		break;

	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_KOREAN:
		resultFormat = DATE_FORMAT_YMD;
		break;

	case LANGUAGE_ENGLISH:
		break;
	default:
		localisationAssertf( false, "fwLanguagePack::GetDateFormatType - Date format requested for unknown language %u. Assuming English", language );
	}

	return resultFormat;
}

bool fwLanguagePack::NeedsLowerCaseMonth(sysLanguage const language)
{
	bool result = true;

	switch (language)
	{
	case LANGUAGE_ENGLISH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_KOREAN:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	case LANGUAGE_CHINESE_TRADITIONAL:
		result = false;
		break;
	default: {}
	}

	return result;
}

fwLanguagePack::DATE_FORMAT_DELIMITER fwLanguagePack::GetDateFormatDelimiterType( sysLanguage const language )
{
	fwLanguagePack::DATE_FORMAT_DELIMITER resultFormat = DATE_FORMAT_DELIMITER_SLASH; // Assume American... because that's how we do it elsewhere

	switch( language )
	{
	case LANGUAGE_GERMAN:
	case LANGUAGE_KOREAN:
	case LANGUAGE_POLISH:
		resultFormat = DATE_FORMAT_DELIMITER_DOT;
		break;

	case LANGUAGE_RUSSIAN:
		resultFormat = DATE_FORMAT_DELIMITER_SPACE;
		break;

	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	case LANGUAGE_ENGLISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_MEXICAN:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_SPANISH:
		break;

	default:
		localisationAssertf( false, "fwLanguagePack::GetDateFormatDelimiterType - Date delimiter format requested for unknown language %u. Assuming English", language );
	}

	return resultFormat;
}

char const * fwLanguagePack::GetDateFormatDelimiter( DATE_FORMAT_DELIMITER const delimiter )
{
	u32 const c_delimiterIndex = delimiter >= 0 && delimiter < MAX_DATE_FORMAT_DELIMITERS ? delimiter : DATE_FORMAT_DELIMITER_SLASH;
	return ms_dateDelimiters[ c_delimiterIndex ];
}

char const * fwLanguagePack::GetDateFormatDelimiter( sysLanguage const language )
{
	return GetDateFormatDelimiter( GetDateFormatDelimiterType( language) );
}

bool fwLanguagePack::HasDayDelimiter( sysLanguage const language )
{
	switch (language)
	{
	case LANGUAGE_ENGLISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_POLISH:
	case LANGUAGE_RUSSIAN:
	default:
		return false;

	case LANGUAGE_SPANISH:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_MEXICAN:
	case LANGUAGE_KOREAN:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_CHINESE_SIMPLIFIED:
		return true;
	}
}

bool fwLanguagePack::HasMonthDelimiter(sysLanguage const language)
{
	switch (language)
	{
	case LANGUAGE_ENGLISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_POLISH:
	case LANGUAGE_RUSSIAN:
	default:
		return false;

	case LANGUAGE_SPANISH:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_MEXICAN:
	case LANGUAGE_KOREAN:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_CHINESE_SIMPLIFIED:
		return true;
	}
}

bool fwLanguagePack::HasYearDelimiter(sysLanguage const language)
{
	switch (language)
	{
	case LANGUAGE_ENGLISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_SPANISH:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_MEXICAN:
	default:
		return false;

	case LANGUAGE_POLISH:
	case LANGUAGE_RUSSIAN:
	case LANGUAGE_KOREAN:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_JAPANESE:
	case LANGUAGE_CHINESE_SIMPLIFIED:
		return true;
	}
}

bool fwLanguagePack::DoesPrefixDelimiterWithSpace( sysLanguage const language )
{
	switch (language)
	{
	case LANGUAGE_ENGLISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_SPANISH:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_MEXICAN:
	case LANGUAGE_POLISH:
	case LANGUAGE_RUSSIAN:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	default:
		return true;

	case LANGUAGE_KOREAN:
	case LANGUAGE_JAPANESE:
		return false;
	}
}

bool fwLanguagePack::DoesPostfixDelimiterWithSpace( sysLanguage const language )
{
	switch (language)
	{
	case LANGUAGE_ENGLISH:
	case LANGUAGE_FRENCH:
	case LANGUAGE_GERMAN:
	case LANGUAGE_ITALIAN:
	case LANGUAGE_SPANISH:
	case LANGUAGE_PORTUGUESE:
	case LANGUAGE_MEXICAN:
	case LANGUAGE_POLISH:
	case LANGUAGE_RUSSIAN:
	case LANGUAGE_KOREAN:
	case LANGUAGE_CHINESE_TRADITIONAL:
	case LANGUAGE_CHINESE_SIMPLIFIED:
	default:
		return true;

	case LANGUAGE_JAPANESE:
		return false;
	}
}

char const * fwLanguagePack::GetCurrencyFormatString( sysLanguage const language )
{
	return ( language == LANGUAGE_SPANISH || language == LANGUAGE_FRENCH ) ?
		ms_currencyFormatRight : ms_currencyFormatLeft;
}

void fwLanguagePack::GetMonthString( char (&out_buffer)[ LOCALIZATION_KEY_LENGTH ], u32 const month, bool const shorthand, const bool lowerCase )
{
	out_buffer[0] = '\0';
	u32 const c_monthIndex = month > 12 ? 12 : month;
	formatf( out_buffer, "MONTH_%u%s", c_monthIndex, shorthand ? "S" : (lowerCase ? "L" : "") );
}

void fwLanguagePack::GetDayString( char (&out_buffer)[ LOCALIZATION_KEY_LENGTH ], u32 const day, bool const shorthand )
{
	out_buffer[0] = '\0';
	u32 const c_dayIndex = day > 7 ? 7 : day;
	formatf( out_buffer, "DAY_%u%s", c_dayIndex, shorthand ? "S" : "" );
}

void fwLanguagePack::GetDayOfTheMonthString( char(&out_buffer)[LOCALIZATION_KEY_LENGTH], u32 const dayOfMonth )
{
	out_buffer[0] = '\0';
	u32 const c_dayIndex = (dayOfMonth == 0) ? 1 : ((dayOfMonth > 31) ? 31 : dayOfMonth);
	formatf(out_buffer, "DAY_OF_MONTH_%u", c_dayIndex);
}

} // namespace rage
