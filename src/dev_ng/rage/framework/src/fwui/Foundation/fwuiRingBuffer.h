/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiRingBuffer.h
// PURPOSE : Dynamic Implementation of fwuiRingBufferBase
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_RING_BUFFER_H
#define FWUI_RING_BUFFER_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/noncopyable.h"

// framework
#include "fwui/Foundation/fwuiRingBufferBase.h"

namespace rage
{
    
    class fwuiRingBuffer : public fwuiRingBufferBase
    {
        typedef fwuiRingBufferBase superclass;

    public:

        fwuiRingBuffer();
        virtual ~fwuiRingBuffer();

        bool Initialize( size_t const bufferSize );
        bool IsInitialized() const;
        void Shutdown();

    protected:

        virtual char* AllocateBuffer( size_t const bufferSize );
        virtual void FreeBuffer( char*& inout_buffer );

    private:
        NON_COPYABLE(fwuiRingBuffer);
    };
        
} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_RING_BUFFER_H
