/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiRingBuffer.cpp
// PURPOSE : HDynamic Implementation of fwuiRingBufferBase
//
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiRingBuffer.h"

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/new.h"

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"

FW_UI_OPTIMISATIONS();

namespace rage
{

    fwuiRingBuffer::fwuiRingBuffer() 
        : superclass()
    { 

    }

    fwuiRingBuffer::~fwuiRingBuffer() 
    {
        if( IsInitialized() )
        {
            fwuiWarningf( "%s - Buffer not shutdown, Shutdown will not use the derived free call due to object destruction", __FUNCTION__ );
        }

        Shutdown();
    }

    bool fwuiRingBuffer::Initialize( size_t const bufferSize )
    {
        bool success = false;

        if( fwuiVerifyf( !IsInitialized(), "%s - Double initializing ringbuffer", __FUNCTION__ ) )
        {
            char * const dataPtr = AllocateBuffer( bufferSize );
            if( fwuiVerifyf( dataPtr, "%s - Failed to allocate buffer of size %" SIZETFMT "d", __FUNCTION__, bufferSize ) )
            {
                SetDataPtr( dataPtr, bufferSize );
                success = true;
            }
        }

        return success;
    }

    bool fwuiRingBuffer::IsInitialized() const
    {
        char const * const c_dataPtr = GetDataPtr();
        return c_dataPtr != nullptr;
    }

    void fwuiRingBuffer::Shutdown()
    {
        char * dataPtr = GetDataPtr();
        FreeBuffer( dataPtr );
        SetDataPtr( nullptr, 0 );
    }

    char* fwuiRingBuffer::AllocateBuffer( size_t const bufferSize )
    {
        char * const result = rage_new char[bufferSize];
        return result;
    }

    void fwuiRingBuffer::FreeBuffer( char*& inout_buffer )
    {
        if( inout_buffer )
        {
            delete[] inout_buffer;
            inout_buffer = nullptr;
        }
    }

} // end namespace rage

#endif // FW_UI_ENABLED
