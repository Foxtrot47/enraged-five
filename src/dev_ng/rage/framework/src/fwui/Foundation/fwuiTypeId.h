/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiTypeID.h
// PURPOSE : Helpers for creating lightweight for run-time type information
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_TYPE_ID_H
#define FWUI_TYPE_ID_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// Rage Includes
#include "atl/hashstring.h"

// Framework Includes
#include "fwui/fwui_channel.h"
#include "fwutil/rtti.h"

namespace rage
{
    typedef atHashWithStringNotFinal    fwuiTypeId;
    typedef atLiteralHashValue          fwuiParGenTypeId;
}

#define FWUI_INVALID_TYPE (rage::fwuiTypeId::Null())

#define FWUI_TYPE_FMT HASHFMT
#define FWUI_TYPE_OUT(x) HASHOUT(x)

// Dummy Implementation for when we have a typeless element that needs to go through a typed helper function
#define FWUI_DECLARE_DUMMY_BASE_TYPE( type )    public:                                                                                                 \
                                                    bool GetIsClassId( const rage::fwuiTypeId ClassId ) const { return GetStaticClassId() == ClassId; } \
                                                    static const rage::fwuiTypeId GetStaticClassId() { return rage::fwuiTypeId::Null(); }               \
		                                            static const rage::fwuiTypeId GetStaticBaseClassId() { return rage::fwuiTypeId::Null(); }           \
                                                    const rage::fwuiTypeId GetClassId() const { return GetStaticClassId(); }                            \
                                                    const rage::fwuiTypeId GetBaseClassId() const { return GetStaticBaseClassId(); }                    \
                                                    size_t getSize() const { return sizeof( type ); }                                                   \
                                                    rage::u32 getTypeDebugRGBAColour() const { return (rage::u32)GetClassId().GetHash() | 0xFF000000; } \
                                                    static rage::fwuiTypeId GetRootClassId() { return GetStaticClassId(); }                             \
                                                private:

#define FWUI_DECLARE_BASE_TYPE( type )  DECLARE_RTTI_BASE_CLASS_WITH_HASH_TYPE( type, rage::fwuiTypeId )                                        \
                                        public:                                                                                                 \
                                            virtual size_t getSize() const { return sizeof( type ); }                                           \
                                            rage::u32 getTypeDebugRGBAColour() const { return (rage::u32)GetClassId().GetHash() | 0xFF000000; } \
                                            static rage::fwuiTypeId GetRootClassId() { return GetStaticClassId(); }                             \
                                        private:

#define FWUI_DECLARE_DERIVED_TYPE( type, baseType ) DECLARE_RTTI_DERIVED_CLASS_WITH_HASH_TYPE( type, baseType, rage::fwuiTypeId )   \
                                                    public:                                                                         \
													    size_t getSize() const override { return sizeof( type ); }                  \
                                                    private:

#define FWUI_DEFINE_TYPE( type, hash ) INSTANTIATE_RTTI_CLASS_WITH_HASH_TYPE( type, (rage::u32)hash, rage::fwuiTypeId ) 

// TODO_UI - ParGen uses atLiteralHashValue. So if we want to look up a parGen type we need to track it separately
#define FWUI_DECLARE_BASE_WITH_PARGEN_HASH_TYPE( type ) FWUI_DECLARE_BASE_TYPE( type )                                                                  \
                                                        public:                                                                                         \
                                                            virtual rage::fwuiParGenTypeId getParGenHash() const { return getStaticParGenHash(); }      \
                                                            static rage::fwuiParGenTypeId getStaticParGenHash();

#define FWUI_DECLARE_DERIVED_WITH_PARGEN_HASH_TYPE( type, baseType )    FWUI_DECLARE_DERIVED_TYPE( type, baseType )                                                     \
                                                                        public:                                                                                         \
                                                                            rage::fwuiParGenTypeId getParGenHash() const override { return getStaticParGenHash(); }     \
                                                                            static rage::fwuiParGenTypeId getStaticParGenHash();

#define FWUI_DEFINE_TYPE_WITH_PARGEN_HASH_AND_PREFIX( prefix, type, hash )	FWUI_DEFINE_TYPE( type, hash ) \
																	rage::fwuiParGenTypeId type::getStaticParGenHash() { return rage::fwuiParGenTypeId( type::parser_Data.m_StructureNameHash ); }

#define FWUI_DEFINE_TYPE_WITH_PARGEN_HASH( type, hash ) FWUI_DEFINE_TYPE_WITH_PARGEN_HASH_AND_PREFIX( "", type, hash )
																			 


namespace rage
{
    template< typename t_castedType, typename t_objectType >
    static bool const fwuiIsType( t_objectType const * const objectToCheck )
    {
        bool const c_result = fwuiVerify( objectToCheck ) && objectToCheck->GetIsClassId( t_castedType::GetStaticClassId() );
        return c_result;
    }

    template< typename t_castedType, typename t_objectType >
    static bool const fwuiIsType( t_objectType const& objectToCheck )
    {
        bool const c_result = objectToCheck.GetIsClassId( t_castedType::GetStaticClassId() );
        return c_result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType* fwuiTryTypeCast( t_objectType * const objectToCast )
    {
        t_castedType* result = objectToCast && objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ) ? static_cast<t_castedType*>(objectToCast) : nullptr;
        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType const * fwuiTryTypeCast( t_objectType const * const objectToCast )
    {
        t_castedType const* result = objectToCast && objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ) ? static_cast<t_castedType const*>(objectToCast) : nullptr;
        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType* fwuiTryTypeCast( t_objectType& objectToCast )
    {
        t_castedType* result = objectToCast.GetIsClassId( t_castedType::GetStaticClassId() ) ? static_cast<t_castedType*>(&objectToCast) : nullptr;
        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType const* fwuiTryTypeCast( t_objectType const& objectToCast )
    {
        t_castedType const* result = objectToCast.GetIsClassId( t_castedType::GetStaticClassId() ) ? static_cast<t_castedType const*>(&objectToCast) : nullptr;
        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType* fwuiTypeCast( t_objectType * const objectToCast )
    {
        t_castedType* result = fwuiVerify( objectToCast ) && 
            fwuiVerifyf( objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ), "Type " FWUI_TYPE_FMT " cannot be cast to type " FWUI_TYPE_FMT,
            FWUI_TYPE_OUT( objectToCast->GetClassId() ), FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) ) ? static_cast<t_castedType*>(objectToCast) : nullptr;

        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType const * fwuiTypeCast( t_objectType const * const objectToCast )
    {
        t_castedType const* result = fwuiVerify( objectToCast ) && 
            fwuiVerifyf( objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ), "Type " FWUI_TYPE_FMT " cannot be cast to type " FWUI_TYPE_FMT,
            FWUI_TYPE_OUT( objectToCast->GetClassId() ), FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) ) ? static_cast<t_castedType const*>(objectToCast) : nullptr;

        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType* fwuiFlatTryTypeCast( t_objectType * const objectToCast )
    {
        t_castedType* const result = (t_castedType*)objectToCast;

        fwuiFatalAssertf( objectToCast == nullptr || objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ), "Type " FWUI_TYPE_FMT " cannot be cast to type " FWUI_TYPE_FMT,
            FWUI_TYPE_OUT( objectToCast->GetClassId() ), FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) );

        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType const * fwuiFlatTryTypeCast( t_objectType const * const objectToCast )
    {
        t_castedType const * const c_result = (t_castedType const*)objectToCast;

        fwuiFatalAssertf( objectToCast == nullptr || objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ), "Type " FWUI_TYPE_FMT " cannot be cast to type " FWUI_TYPE_FMT,
            FWUI_TYPE_OUT( objectToCast->GetClassId() ), FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) );

        return c_result;
    }

	template< typename t_castedType, typename t_objectType >
	static t_castedType* fwuiFlatTypeCast( t_objectType * const objectToCast )
	{
		t_castedType* const result = (t_castedType*)objectToCast;

		fwuiFatalAssertf( objectToCast != nullptr && objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ), "Type %s cannot be cast to type " FWUI_TYPE_FMT,
			objectToCast != nullptr ? objectToCast->GetClassId().TryGetCStr() : "nullptr", FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) );

		return result;
	}

	template< typename t_castedType, typename t_objectType >
	static t_castedType const * fwuiFlatTypeCast( t_objectType const * const objectToCast )
	{
		t_castedType const * const c_result = (t_castedType const*)objectToCast;

        fwuiFatalAssertf( objectToCast != nullptr && objectToCast->GetIsClassId( t_castedType::GetStaticClassId() ), "Type %s cannot be cast to type " FWUI_TYPE_FMT,
			objectToCast != nullptr ? objectToCast->GetClassId().TryGetCStr() : "nullptr", FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) );

		return c_result;
	}

    template< typename t_castedType, typename t_objectType >
    static t_castedType& fwuiFlatTypeCast( t_objectType& objectToCast )
    {
        t_castedType& result = (t_castedType&)objectToCast;

        fwuiFatalAssertf( objectToCast.GetIsClassId( t_castedType::GetStaticClassId() ), "Type %s cannot be cast to type " FWUI_TYPE_FMT,
            objectToCast.GetClassId().TryGetCStr(), FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) );

        return result;
    }

    template< typename t_castedType, typename t_objectType >
    static t_castedType const& fwuiFlatTypeCast( t_objectType const& objectToCast )
    {
        t_castedType const& result = (t_castedType const&)objectToCast;

        fwuiFatalAssertf( objectToCast.GetIsClassId( t_castedType::GetStaticClassId() ), "Type %s cannot be cast to type " FWUI_TYPE_FMT,
            objectToCast.GetClassId().TryGetCStr(), FWUI_TYPE_OUT( t_castedType::GetStaticClassId() ) );

        return result;
    }

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_TYPE_ID_H
