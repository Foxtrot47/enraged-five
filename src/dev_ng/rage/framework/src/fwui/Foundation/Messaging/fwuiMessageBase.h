/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiMessageBase.h
// PURPOSE : Base-class for fwUI messaging system
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_MESSAGE_BASE_H
#define FWUI_MESSAGE_BASE_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// Framework
#include "fwui/Foundation/Messaging/fwuiMessaging.h"

namespace rage
{
	class fwuiValueBase;

    // BIG-ASS-CAVEAT - If you are queuing messages into the standard queue mechanisms then
    // any atl container types will need CleanupOnQueuedCopy called when you queue the item.
    //
    // This is because the internal semantics of these containers will clean themselves up
    // when the message goes out of scope. So when you unqueue you'll be looking into GARBAGE!
    //
    // Alternatively you should queue messages as pointers, but then you cannot stack allocate them.
    // Your call.
    class fwuiMessageBase
    {
        FWUI_DECLARE_MESSAGE_BASE( fwuiMessageBase );
        FWUI_MESSAGE_DEBUG_DATA();

    public:
        fwuiMessageBase()
        {
            // TODO - We may want to remove this from here and make the queue a message is added to responsible
            // for this debug allocation? The debug data needs to carry with us for simplicity, but we don't really own it...
            FWUI_MESSGE_DEBUG_INIT();
        }
        
        virtual ~fwuiMessageBase()
        {
            // Don't call CleanupDebugInfo from destructor, since our queuing mechanism may have copied us
            // in a way we are not aware of
        }

        /* TODO -   For now we use dumb copies since a message is really owned by the queue, so we don't want to
                    copy the debug info. Code here is for future revisions (if we change queuing mechanisms)

        fwuiMessageBase( fwuiMessageBase const& other ) 
        {
            CopyDebugInfo( other );
        }

        fwuiMessageBase& operator=( fwuiMessageBase const& rhs )
        {			
            if( this != &rhs )
            {
                CopyDebugInfo( rhs );
            }

            return *this;
        }*/

        void CleanupDebugInfo()
        {
            FWUI_MESSGE_DEBUG_CLEANUP();
        }

        // When we are queued via a structure copy (i.e. not as a pointer) we may have dynamic data
        // that we want to avoid destructing (As it will be resurrected when un-queued and destructed then).
        //
        // This function is called to ensure dynamic data such as that is cleared correctly after queuing
        virtual void CleanupOnQueuedCopy()
        {
            // NOP
        }

        virtual void CleanupOnConsumed()
        {
            // NOP
        }

		virtual const rage::fwuiValueBase * TryGetValue( rage::atHashString /*valueName*/ ) const { return nullptr; }

    protected:

        void CopyDebugInfo( fwuiMessageBase const& other )
        {
            FWUI_MESSGE_DEBUG_COPY( other );
        }
        
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_MESSAGE_BASE_H
