/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiMessageBase.cpp
// PURPOSE : Base-class for fwUI messaging system
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwui/Foundation/Messaging/fwuiMessageBase.h"

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

namespace rage
{

    FWUI_DEFINE_MESSAGE( fwuiMessageBase, 0x6B4E5DD1 );   
    
} // end namespace rage

#endif // FW_UI_ENABLED
