/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiMultiProducerMessageQueue.h
// PURPOSE : Message queue that uses locking on queue, but lock-free on reads.
//           Used for multi-producer single consumer scenarios
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_MULTI_PRODUCER_MESSAGE_QUEUE_H
#define FWUI_MULTI_PRODUCER_MESSAGE_QUEUE_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/ipc.h"
#include "system/noncopyable.h"
#include "system/criticalsection.h"

// framework
#include "fwui/Foundation/Messaging/fwuiMessageQueue.h"

namespace rage
{
    template< typename t_messageType >
    class fwuiMultiProducerMessageQueue final
    {
    public:
        fwuiMultiProducerMessageQueue()
        {
        }
        
        ~fwuiMultiProducerMessageQueue()
        {
            Shutdown();
        }
        
        bool Initialize( size_t const bufferSize )
        {
            bool const c_result = m_queue.Initialize( bufferSize );
            return c_result;
        }

        bool IsInitialized() const
        {
            return m_queue.IsInitialized();
        }

        void Shutdown()
        {
            m_queue.Shutdown();
        }
        
        bool HasMessages() const { return m_queue.HasMessages(); }
        size_t GetMessageCount() const { return m_queue.GetMessageCount(); }

        bool QueueMessage( t_messageType const& msg )
        {
            SYS_CS_SYNC( m_producerCs );
            
            bool const c_queued = m_queue.QueueMessage( msg );
            return c_queued;
        }

        t_messageType* PeekFirstMessage()
        {
            return m_queue.PeekFirstMessage();
        }

        void ReleaseMessage()
        {
            m_queue.ReleaseMessage();
        }

        void Flush()
        {
            m_queue.Flush();
        }
        
    private:
        fwuiMessageQueue<t_messageType> m_queue;
        sysCriticalSectionToken	        m_producerCs;
        
    private:
        NON_COPYABLE( fwuiMultiProducerMessageQueue );
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // end FWUI_MULTI_PRODUCER_MESSAGE_QUEUE_H
