/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiMessageQueueBase.h
// PURPOSE : Base interface for all message queue implementations
//
// REMARKS : I experimented with a pImpl idiom based approach but it caused
//           performance issues with indirect/virtual calls. Have a go if you like,
//           but the template approach seems fastest.
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_MESSAGE_QUEUE_BASE_H
#define FWUI_MESSAGE_QUEUE_BASE_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/noncopyable.h"

// framework
#include "fwui/fwui_channel.h"

namespace rage
{

    //! Represents a message queue of a fixed buffer type
    template< typename t_messageType, typename t_bufferType >
    class fwuiMessageQueueBaseWithStorage
    {
    public:
        fwuiMessageQueueBaseWithStorage()
            : m_buffer()
        {

        }

        virtual ~fwuiMessageQueueBaseWithStorage()
        {
            Flush();
        }

        bool HasMessages() const { return m_buffer.HasContent(); }
        size_t GetMessageCount() const { return m_buffer.GetEntryCount(); }

        bool QueueMessage( t_messageType const& msg )
        {
            size_t const c_messageSize = msg.getSize();
            fwuiFatalAssertf( c_messageSize >= sizeof( t_messageType ), "Pushing unusually small message %" SIZETFMT "u, expected at least %" SIZETFMT "u. Sign of pushing wrong data type?", 
                c_messageSize, sizeof( t_messageType ) );

            bool const c_queued = m_buffer.Push( &msg, c_messageSize );
            return c_queued;
        }

        // Peek at the first message in our queue. Returns null if empty.
        t_messageType* PeekFirstMessage()
        {
            size_t dummySize(0);
            void* peekedMessage = nullptr;

            if( m_buffer.HasContent() )
            {
                m_buffer.GetFirst( peekedMessage, dummySize );
            }

            return (t_messageType*)peekedMessage;
        }

        // Release the first message in our queue. Used once a message is no longer relevant
        // or has been handled
        void ReleaseMessage()
        {
            t_messageType* const c_firstMessage = PeekFirstMessage();
            if( c_firstMessage )
            {
                c_firstMessage->CleanupDebugInfo();
                c_firstMessage->CleanupOnConsumed();
                m_buffer.FreeFirst();
            }
        }

        void Flush()
        {
            while( HasMessages() )
            {
                ReleaseMessage();
            }
        }

    protected:

        t_bufferType& GetBuffer()
        {
            return m_buffer;
        }

        t_bufferType const& GetBuffer() const
        {
            return m_buffer;
        }

    private:
        t_bufferType m_buffer;

    private:
        NON_COPYABLE( fwuiMessageQueueBaseWithStorage );
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // end FWUI_MESSAGE_QUEUE_BASE_H
