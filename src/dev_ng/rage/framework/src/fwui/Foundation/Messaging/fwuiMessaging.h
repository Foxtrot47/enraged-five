/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : Messaging.h
// PURPOSE : Helpers for fwUI Messaging system
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_MESSAGING_H
#define FWUI_MESSAGING_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/memory.h"

// Framework
#include "fwlocalisation/templateString.h"
#include "fwui/fwui_channel.h"
#include "fwui/Foundation/fwuiTypeId.h"
#include "fwutil/xmacro.h"

// Turning off currently since it's not needed for current usage
#define FWUI_MESSAGE_DEBUGGING (0)

// Backported from the future, where we have Rocky. 
// Using this stub incase we backport Rocky for Gen9
#define FWUI_MESSAGE_PROFILER_EVENT( NAME )

#if FWUI_MESSAGE_DEBUGGING

namespace rage
{
    //! So we can track who sent the message in debug builds
    struct fwuiMessageDebugData                
    {                                               
    public:                                         
        rage::SimpleString_256 m_file;     
        int m_line;     
                                                                                        
        fwuiMessageDebugData() 
            : m_file( nullptr )
            , m_line( -1 )
        {}

        fwuiMessageDebugData( char const * const file, int const line )
            : m_file( file )
            , m_line( line )
        {}

        fwuiMessageDebugData( fwuiMessageDebugData const& other ) 
            : m_file( other.m_file )
            , m_line( other.m_line )
        {

        }

        fwuiMessageDebugData& operator=( fwuiMessageDebugData const& rhs )
        {			
            setData( rhs.m_file.getBuffer(), rhs.m_line );
            return *this;
        }

        void setData( char const * const file, int const line )
        {
            m_file = file;
            m_line = line;
        }
    };
};

#define FWUI_MESSAGE_DEBUG_DATA() public:                                       \
                                    rage::fwuiMessageDebugData* m_debugData;    \
                                  private:        

#define FWUI_MESSGE_DEBUG_INIT()                do { USE_DEBUG_MEMORY(); m_debugData = rage_new rage::fwuiMessageDebugData(); } while(false)
#define FWUI_MESSGE_DEBUG_CLEANUP()             if( m_debugData ){ USE_DEBUG_MEMORY(); delete m_debugData; m_debugData = nullptr; }

#define FWUI_MESSGE_DEBUG_COPY( other )         do                                                                                                          \
                                                {                                                                                                           \
                                                    USE_DEBUG_MEMORY(); FWUI_MESSGE_DEBUG_CLEANUP();                                                        \
                                                    m_debugData = other.m_debugData ? rage_new rage::fwuiMessageDebugData( *other.m_debugData ) : nullptr;  \
                                                } while( false )

#define FWUI_SEND_MESSAGE( message, messageSender )     do{                                                                                         \
                                                            if( message.m_debugData )                                                               \
                                                            {                                                                                       \
                                                                message.m_debugData->setData( __FILE__, __LINE__);                                  \
                                                            }                                                                                       \
                                                                                                                                                    \
                                                            if( fwuiVerifyf( messageSender( message ), "%s - Message Send Failed", __FUNCTION__ ) ) \
                                                            {                                                                                       \
                                                                message.CleanupOnQueuedCopy();                                                      \
                                                            }                                                                                       \
                                                        }while( false )

#define FWUI_SEND_MESSAGE_WITH_RESULT( message, messageSender, resultVar )  do{                                                                             \
                                                                                if( message.m_debugData )                                                   \
                                                                                {                                                                           \
                                                                                    message.m_debugData->setData( __FILE__, __LINE__);                      \
                                                                                }                                                                           \
                                                                                                                                                            \
                                                                                resultVar = messageSender( message );                                       \
                                                                                if( fwuiVerifyf( resultVar, "%s - Message Send Failed", __FUNCTION__ ) )    \
                                                                                {                                                                           \
                                                                                    message.CleanupOnQueuedCopy();                                          \
                                                                                }                                                                           \
                                                                            }while( false )

#else // FWUI_MESSAGE_DEBUGGING

#define FWUI_MESSAGE_DEBUG_DATA() private:
#define FWUI_MESSGE_DEBUG_INIT()
#define FWUI_MESSGE_DEBUG_COPY( other ) UNUSED_VAR(other)
#define FWUI_MESSGE_DEBUG_CLEANUP()

#define FWUI_SEND_MESSAGE( message, messageSender )     do{                                                                                         \
                                                            if( fwuiVerifyf( messageSender( message ), "%s - Message Send Failed", __FUNCTION__ ) ) \
                                                            {                                                                                       \
                                                                message.CleanupOnQueuedCopy();                                                      \
                                                            }                                                                                       \
                                                        }while( false )

#define FWUI_SEND_MESSAGE_WITH_RESULT( message, messageSender, resultVar )  do{                                                                         \
                                                                                resultVar = messageSender( message );                                   \
                                                                                if( fwuiVerifyf( resultVar, "%s - Message Send Failed", __FUNCTION__ ) )\
                                                                                {                                                                       \
                                                                                    message.CleanupOnQueuedCopy();                                      \
                                                                                }                                                                       \
                                                                            }while( false )
#endif // FWUI_MESSAGE_DEBUGGING

#define FWUI_DECLARE_MESSAGE_BASE( messageType )                    FWUI_DECLARE_BASE_TYPE( messageType )
#define FWUI_DECLARE_MESSAGE_DERIVED( messageType, baseType )       FWUI_DECLARE_DERIVED_TYPE( messageType, baseType )
#define FWUI_DEFINE_MESSAGE( type, hash )                           FWUI_DEFINE_TYPE( type, hash ) 

#define FWUI_DECLARE_MESSAGE_SIMPLE( messageType )                  public:                                                                 \
                                                                        size_t getSize() const { return sizeof(messageType); }              \
                                                                        void CleanupDebugInfo() {}                                          \
                                                                        void CleanupOnQueuedCopy() {}                                       \
                                                                        void CleanupOnConsumed() {}                                         \
                                                                    private:

#define FWUI_MESSAGE_CALL_PTR( type, messagePtr, handlerFunc )	if( messagePtr && messagePtr->GetIsClassId( type::GetStaticClassId() ) )    \
																{                                                                           \
                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);                   \
																	handlerFunc( );                                                         \
																}

#define FWUI_MESSAGE_CALL_REF( type, messagePtr, handlerFunc )	if( messagePtr.GetIsClassId( type::GetStaticClassId() ) )                   \
																{                                                                           \
                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);                   \
																	handlerFunc( );                                                         \
																}

#define FWUI_MESSAGE_CALL_REF_SUCCESS( type, messagePtr, handlerFunc, resultVar )	if( messagePtr.GetIsClassId( type::GetStaticClassId() ) )                   \
                                                                                    {                                                                           \
                                                                                        FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);                   \
                                                                                        handlerFunc( );                                                         \
                                                                                        resultVar = true;                                                       \
                                                                                    }

#define FWUI_MESSAGE_BIND_PTR( type, messagePtr, handlerFunc )  if( messagePtr && messagePtr->GetIsClassId( type::GetStaticClassId() ) )    \
                                                                {                                                                           \
                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);					\
                                                                    handlerFunc( (type const * const)messagePtr );                          \
                                                                }

#define FWUI_MESSAGE_BIND_PTR_RESULT( type, messagePtr, handlerFunc, resultVar )    if( messagePtr && messagePtr->GetIsClassId( type::GetStaticClassId() ) )    \
                                                                                    {                                                                           \
																						FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);					\
                                                                                        resultVar = handlerFunc( (type const * const)messagePtr );              \
                                                                                    }

#define FWUI_MESSAGE_BIND_PTR_RETURN( type, messagePtr, handlerFunc, resultVar )    if( messagePtr && messagePtr->GetIsClassId( type::GetStaticClassId() ) )    \
                                                                                    {                                                                           \
																						FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);					\
                                                                                        resultVar = handlerFunc( (type const * const)messagePtr );              \
                                                                                        return resultVar;                                                       \
                                                                                    }

#define FWUI_MESSAGE_BIND_REF( type, messageRef, handlerFunc )  if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                {                                                           \
                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                    handlerFunc( (type&)messageRef );                       \
                                                                }

#define FWUI_MESSAGE_BIND_REF_CONST( type, messageRef, handlerFunc )    if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                        {                                                           \
                                                                            FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                            handlerFunc( (type const&)messageRef );                 \
                                                                        }

#define FWUI_MESSAGE_BIND_REF_RESULT( type, messageRef, handlerFunc, resultVar )    if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                    {                                                           \
                                                                                        FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                        resultVar = handlerFunc( (type&)messageRef );           \
                                                                                    }

#define FWUI_MESSAGE_BIND_REF_RETURN( type, messageRef, handlerFunc, resultVar )    if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                    {                                                           \
                                                                                        FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                        resultVar = handlerFunc( (type&)messageRef );           \
                                                                                        return resultVar;                                       \
                                                                                    }

#define FWUI_FINAL_MESSAGE_BIND_REF_RETURN( type, messageRef, handlerFunc, resultVar )  if( messageRef.GetClassId() == type::GetStaticClassId() )   \
                                                                                        {                                                           \
                                                                                            FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                            resultVar = handlerFunc( (type&)messageRef );           \
                                                                                            return resultVar;                                       \
                                                                                        }

#define FWUI_MESSAGE_BIND_REF_RESULT_WITH_PARAM( type, messageRef, handlerFunc, resultVar, paramVar )   if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                                        {                                                           \
                                                                                                            FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                                            resultVar = handlerFunc( (type&)messageRef, paramVar ); \
                                                                                                        }

#define FWUI_MESSAGE_BIND_REF_RETURN_WITH_PARAM( type, messageRef, handlerFunc, resultVar, paramVar )   if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                                        {                                                           \
                                                                                                            FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                                            resultVar = handlerFunc( (type&)messageRef, paramVar ); \
                                                                                                            return resultVar;                                       \
                                                                                                        }

#define FWUI_FINAL_MESSAGE_BIND_REF_RETURN_WITH_PARAM( type, messageRef, handlerFunc, resultVar, paramVar )     if ( messageRef.GetClassId() == type::GetStaticClassId() )    \
                                                                                                                {                                                             \
                                                                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);     \
                                                                                                                    resultVar = handlerFunc((type&)messageRef, paramVar);     \
                                                                                                                    return resultVar;                                         \
                                                                                                                }

#define FWUI_MESSAGE_BIND_REF_RESULT_CONST( type, messageRef, handlerFunc, resultVar )  if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                        {                                                           \
                                                                                            FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                            resultVar = handlerFunc( (type const&)messageRef );     \
                                                                                        }

#define FWUI_MESSAGE_BIND_REF_RETURN_CONST( type, messageRef, handlerFunc, resultVar )  if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                        {                                                           \
                                                                                            FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                            resultVar = handlerFunc( (type const&)messageRef );     \
                                                                                            return resultVar;                                       \
                                                                                        }

#define FWUI_FINAL_MESSAGE_BIND_REF_RETURN_CONST( type, messageRef, handlerFunc, resultVar )    if( messageRef.GetClassId() == type::GetStaticClassId() )   \
                                                                                                {                                                           \
                                                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                                    handlerFunc( (type const&)messageRef );                 \
                                                                                                    resultVar = true;                                       \
                                                                                                    return resultVar;                                       \
                                                                                                }

#define FWUI_MESSAGE_BIND_REF_RESULT_SUCCESS_CONST( type, messageRef, handlerFunc, resultVar )  if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                                {                                                           \
                                                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                                    handlerFunc( (type const&)messageRef );                 \
                                                                                                    resultVar = true;                                       \
                                                                                                }

#define FWUI_MESSAGE_BIND_REF_RETURN_SUCCESS_CONST( type, messageRef, handlerFunc, resultVar )  if( messageRef.GetIsClassId( type::GetStaticClassId() ) )   \
                                                                                                {                                                           \
                                                                                                    FWUI_MESSAGE_PROFILER_EVENT(#type "::" #handlerFunc);   \
                                                                                                    handlerFunc( (type const&)messageRef );                 \
                                                                                                    resultVar = true;                                       \
                                                                                                    return resultVar;										\
                                                                                                }

#define FWUI_VERIFY_TYPEID( type, hashValue )   fwuiAssertf( type::GetStaticClassId() == hashValue, "hashValue: " #hashValue " does not match class type: " #type )

#define FWUI_CASE_TYPEID( type, hashValue )     case hashValue : FWUI_VERIFY_TYPEID( type, hashValue );

#endif // FW_UI_ENABLED

#endif // FWUI_MESSAGING_H
