/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiMessageContentTemplates.h
// PURPOSE : Template classes for some common message content
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_MESSAGE_CONTENT_TEMPLATES_H
#define FWUI_MESSAGE_CONTENT_TEMPLATES_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "parser/memberarraydata.h"
#include "parser/memberstringdata.h"
#include "string/string.h"
#include "system/memops.h"

// Framework
#include "fwlocalisation/templateString.h"
#include "fwui/Foundation/fwuiParGenHelper.h"
#include "fwui/Foundation/Messaging/Messaging.h"

namespace rage
{
    // Base for messages containing strings
    template< typename t_baseMessageType, size_t t_stringBufferSize = 128 >
    class fwuiMessageStaticStringBase : public t_baseMessageType
    {
    public:
        fwuiMessageStaticStringBase()
            : t_baseMessageType()
            , m_string()
        {

        }

        explicit fwuiMessageStaticStringBase( char const * const stringContents )
            : t_baseMessageType()
            , m_string( stringContents )
        {

        }

        char const * const GetStringValue() const { return m_string.getBuffer(); }
    protected:
        void SetStringValue( char const * const stringValue ) { m_string.Set( stringValue ); }

    private: // members
        rage::fwTemplateString< t_stringBufferSize > m_string;
    };

    template< typename t_baseMessageType, size_t t_maxLen = 128 >
    class fwuiMessageDynamicStringBase : public t_baseMessageType
    {
    public:
        fwuiMessageDynamicStringBase()
            : t_baseMessageType()
            , m_stringData( nullptr )
        {

        }

        explicit fwuiMessageDynamicStringBase( char const * const stringContents )
            : t_baseMessageType()
            , m_stringData( nullptr )
        {
            SetStringValue( stringContents );
        }

        virtual ~fwuiMessageDynamicStringBase()
        {
            Cleanup();
        }

        char const * const GetStringValue() const { return m_stringData; }

        void CleanupOnQueuedCopy() override
        {
            // We have been copy-queued, so nullify the string without deleting as
            // the copy owns the pointer now
            m_stringData = nullptr;
        }

        void CleanupOnConsumed() override
        {
            Cleanup();
        }

    protected:
        void SetStringValue( char const * const stringValue ) 
        { 
            Cleanup();

            size_t const c_rawLen = StringLength( stringValue );
            size_t const c_upperBound = t_maxLen - 1;
            size_t const c_strLen = Min( c_rawLen, c_upperBound );
            fwuiAssertf( c_rawLen <= c_upperBound, "Truncating string from %" SIZETFMT "u down to %" SIZETFMT "u", c_rawLen, c_strLen );

            size_t const c_allocLen = c_strLen > 0 ? c_strLen + 1 : 0;
            m_stringData = c_strLen > 0 ? rage_new char[c_allocLen] : nullptr;

            if( m_stringData )
            {
                rage::safecpy( m_stringData, stringValue, c_allocLen);
            }
        }

        void Cleanup()
        {
            if( m_stringData )
            {
                delete[] m_stringData;
            }

            m_stringData = nullptr;
        }

    private: // members
        char * m_stringData;
    };

    // Base for value type messages
    template< typename t_baseMessageType, typename t_valueType >
    class fwuiMessageValueTypeBase : public t_baseMessageType
    {
    public:

        fwuiMessageValueTypeBase()
            : t_baseMessageType()
            , m_valueType( (t_valueType)0 )
        {

        }

        explicit fwuiMessageValueTypeBase( t_valueType const valueType )
            : t_baseMessageType()
            , m_valueType( valueType )
        {

        }

        virtual ~fwuiMessageValueTypeBase() {}

        t_valueType GetValueType() const { return m_valueType; }
        t_valueType& GetValueTypeRef() { return m_valueType; }

    protected:
        void SetValueType( t_valueType const valueType ) { m_valueType = valueType; }

    private: // members
        t_valueType m_valueType;
    };

    // Some basic value type messages
    template< typename t_baseMessageType >
    class fwuiMessageBoolBase : public fwuiMessageValueTypeBase< t_baseMessageType, bool > { };

    template< typename t_baseMessageType >
    class fwuiMessageIntBase : public fwuiMessageValueTypeBase< t_baseMessageType, int > { };

    template< typename t_baseMessageType >
    class fwuiMessageFloatBase : public fwuiMessageValueTypeBase< t_baseMessageType, float > { };

    template< typename t_baseMessageType >
    class fwuiMessageDoubleBase : public fwuiMessageValueTypeBase< t_baseMessageType, double > { };

    //! ParGen union content support
    template< typename t_baseMessageType >
    class fwuiMessageParGenPrimitiveTypeBase : public t_baseMessageType
    {
    public:

        fwuiMessageParGenPrimitiveTypeBase()
            : t_baseMessageType()
            , m_unionValue( (u64)0 )
            , m_type( parMemberType::INVALID_TYPE )
        {

        }

#if RSG_IOS

		void SetValue( size_t const newValue )
		{
			m_unionValue.sizetValue = newValue;
			m_type = parMemberType::TYPE_SIZET;
		}

#endif /* RSG_IOS */

        void SetValue( u64 const newValue )
        {
            m_unionValue.u64Value = newValue;
            m_type = parMemberType::TYPE_UINT64;
        }

        void SetValue( s64 const newValue )
        {
            m_unionValue.s64Value = newValue;
            m_type = parMemberType::TYPE_INT64;
        }

        void SetValue( double const newValue )
        {
            m_unionValue.doubleValue = newValue;
            m_type = parMemberType::TYPE_DOUBLE;
        }

        void SetValue( u32 const newValue )
        {
            m_unionValue.u32Value = newValue;
            m_type = parMemberType::TYPE_UINT;
        }

        void SetValue( s32 const newValue )
        {
            m_unionValue.s32Value = newValue;
            m_type = parMemberType::TYPE_INT;
        }

        void SetValue( float const newValue )
        {
            m_unionValue.floatValue = newValue;
            m_type = parMemberType::TYPE_FLOAT;
        }

        void SetValue( u8 const newValue )
        {
            m_unionValue.u8Value = newValue;
            m_type = parMemberType::TYPE_UCHAR;
        }

        void SetValue( s8 const newValue )
        {
            m_unionValue.s8Value = newValue;
            m_type = parMemberType::TYPE_CHAR;
        }

        void SetValue( bool const newValue )
        {
            m_unionValue.boolValue = newValue;
            m_type = parMemberType::TYPE_BOOL;
        }

		void SetValue( Vec2f const newValue )
		{
			m_unionValue.vectorValue[0] = newValue.GetX();
			m_unionValue.vectorValue[1] = newValue.GetY();
			m_type = parMemberType::TYPE_VECTOR2;
		}

        size_t GetValueSize() const 
        {
            size_t const c_size = parMemberType::GetSize( m_type );
            return c_size;
        }

        void SetValue( void const * const sourcePtr, parMemberType::Enum const type )
        {
            if( sourcePtr && ( fwuiParGenHelpers::IsPrimitiveParGenType( type ) || fwuiParGenHelpers::IsVector2ParGenType( type ) ) )
            {
                // All unions types share the same base address so we set the type
                // then just copy by the expected size
                m_type = type;
                sysMemCpy( &m_unionValue, sourcePtr, GetValueSize() );
            }
        }

        void const * GetValuePtr() const { return &m_unionValue; }
        parMemberType::Enum GetValueType() const { return m_type; }
        bool IsValid() const { return fwuiParGenHelpers::IsPrimitiveParGenType( m_type ) || fwuiParGenHelpers::IsVector2ParGenType( m_type ); }

    private: // declarations and variables
        union PrimitiveUnion
        {
            u64             u64Value;
            s64             s64Value;
            double          doubleValue;
            u32             u32Value;
            s32             s32Value;
            float           floatValue;
            u8              u8Value;
            s8              s8Value;
            bool            boolValue;
			float			vectorValue[2]; // TODO_UI: Change this back to Vec2f when the kingdom of vs2015 is back.

#if RSG_IOS
			size_t			sizetValue;
			PrimitiveUnion( size_t const newValue )    : sizetValue( newValue ) {}
#endif /* RSG_IOS */

            PrimitiveUnion( u64 const newValue )    : u64Value( newValue ) {}
            PrimitiveUnion( s64 const newValue )    : s64Value( newValue ) {}
            PrimitiveUnion( double const newValue ) : doubleValue( newValue ) {}
            PrimitiveUnion( u32 const newValue )    : u32Value( newValue ) {}
            PrimitiveUnion( s32 const newValue )    : s32Value( newValue ) {}
            PrimitiveUnion( float const newValue )  : floatValue( newValue ) {}
            PrimitiveUnion( u8 const newValue )     : u8Value( newValue ) {}
            PrimitiveUnion( s8 const newValue )     : s8Value( newValue ) {}
			PrimitiveUnion( bool const newValue )   : boolValue( newValue ) {}
			PrimitiveUnion( Vec2f const newValue )  { vectorValue[0] = newValue.GetX(); vectorValue[1] = newValue.GetY(); }
        };

        PrimitiveUnion      m_unionValue;
        parMemberType::Enum m_type;
    };

    //! ParGen Hash-type union content support
    template< typename t_baseMessageType >
    class fwuiMessageParGenHashTypeBase : public t_baseMessageType
    {
    public:

        fwuiMessageParGenHashTypeBase()
            : t_baseMessageType()
            , m_unionValue( atHashString::Null() )
            , m_type( INDEX_NONE )
        {

        }

        void SetValue( atFinalHashString const newValue )
        {
            m_unionValue.hashValuePartial64 = newValue;
            m_type = parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING;
        }

        void SetValue( atHashValue const newValue )
        {
            m_unionValue.hashValuePartial64 = newValue;
            m_type = parMemberStringSubType::SUBTYPE_ATHASHVALUE;
        }

        void SetValue( atNamespacedHashStringBase32 const newValue )
        {
            m_unionValue.hashValuePartial64 = newValue;
            m_type = parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING;
        }

        void SetValue( atNamespacedHashValueBase const newValue )
        {
            m_unionValue.hashValuePartial64 = newValue;
            m_type = parMemberStringSubType::SUBTYPE_ATNSHASHVALUE;
        }

        void SetValue( u32 const newValue )
        {
            m_unionValue.hashValuePartial64 = newValue;
            m_type = parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE;
        }

        void SetValue( void const * const sourcePtr, parMemberStringSubType::Enum const type )
        {
            if( sourcePtr && fwuiParGenHelpers::IsParGenStringSubTypeHashType( type ) )
            {
                // All unions types share the same base address so we set the type
                // then just copy by the expected size. Relies on hashed types storing their hash as the first member
                m_type = type;
                sysMemCpy( &m_unionValue, sourcePtr, GetValueSize() );
            }
        }

        size_t GetValueSize() const 
        {
            size_t size = 0;

            switch( m_type )
            {
            case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
            case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
            case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
            case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
            case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
            case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
                {
                    size = sizeof(m_unionValue.hashValuePartial);
                    break;
                }
            default:
                {
                    // NOP
                }
            }

            return size;
        }

        void const * GetValuePtr() const { return &m_unionValue; }
        parMemberStringSubType::Enum GetHashStringType() const { return (parMemberStringSubType::Enum)m_type; }
        bool IsValid() const { return m_type >= 0 && fwuiParGenHelpers::IsParGenStringSubTypeHashType( GetHashStringType() ); }

    private: // declarations and variables
        union HashUnion
        {
            // Cannot unionize hash types since they have copy constructors
            u64 hashValuePartial64;
            u32 hashValuePartial;

            HashUnion( u64 const newValue )                         : hashValuePartial64( newValue ) {}
            HashUnion( atFinalHashString const newValue )           : hashValuePartial( newValue.GetHash() ) {}
            HashUnion( atHashValue const newValue )                 : hashValuePartial( newValue.GetHash() ) {}
            HashUnion( atNamespacedHashStringBase32 const newValue )  : hashValuePartial( newValue.GetHash() ) {}
            HashUnion( atNamespacedHashValueBase const newValue )   : hashValuePartial( newValue.GetHash() ) {}
            HashUnion( u32 const newValue )                         : hashValuePartial( newValue ) {}
        };

        HashUnion   m_unionValue;
        s32         m_type;
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_MESSAGE_CONTENT_TEMPLATES_H
