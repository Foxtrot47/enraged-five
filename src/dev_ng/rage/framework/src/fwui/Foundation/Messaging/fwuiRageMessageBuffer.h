/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiRageMessageBuffer.h
// PURPOSE : Adapts the rage sysMessageQueue class to fit the interface we have 
//           for fwUI message queuing. Designed for multiple producers single
//           consumer usage.
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_RAGE_MESSAGE_BUFFER_H
#define FWUI_RAGE_MESSAGE_BUFFER_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/memops.h"
#include "system/messagequeue.h"
#include "system/noncopyable.h"
#include "system/interlocked.h"
#include "system/new.h"

// framework
#include "fwui/fwui_channel.h"

namespace rage
{

    template< int t_size, bool t_blockOnFull = false >
    class fwuiRageMessageBuffer
    {
    private: // declarations 

        // We want to keep our external semantics of only one copy, so we wrap the data into a blob
        class MessageBlob
        {
        public:
            MessageBlob()
                : m_data(nullptr)
                , m_size(0)
            {

            }

            // Shallow-copy is by design. This internal class is stored in a collection
            // by-value and as such the copy owns the data. Explicit call to Destruct must be
            // made on free.
            MessageBlob( MessageBlob const& other )
                : m_data( other.m_data )
                , m_size( other.m_size )
            {

            }

            MessageBlob( void const * const data, size_t const dataSize )
				: m_data( nullptr )     // ensure Destruct (called by Set) doesn't try to free anything
            {
                Set( data, dataSize);
            }

            void Set( void const * const data, size_t const dataSize)
            {
                Destruct();
                m_data = data && dataSize > 0 ? rage_new char[dataSize] : nullptr;

                if( m_data )
                {
                    sysMemCpy( m_data, data, dataSize );
                }

                m_size = m_data ? dataSize : 0;

            }

            void Destruct()
            {
                if( m_data )
                {
                    delete[] m_data;
                    m_data = nullptr;
                }

                m_size = 0;
            }

            void * GetData() { return m_data; }
            size_t GetSize() const { return m_size; }

            bool IsValid() const { return m_data != nullptr && m_size > 0; }

        private: // declarations and variables

            char*   m_data;
            size_t  m_size;

        private: // methods

        };
        
        typedef sysMessageQueue< MessageBlob, t_size, t_blockOnFull > RageMsgQueue;

    public:
        fwuiRageMessageBuffer()
            : m_queue()
            , m_entries( 0 )
        {
            
        }

        ~fwuiRageMessageBuffer()
        {
            
        }

        bool IsValid() const { return true; }
        bool HasContent() const { return !m_queue.IsEmpty(); }

        bool Push( void const * const data, size_t const size )
        {
            bool success = false;

            MessageBlob newBlob = MessageBlob( data, size );
            if( fwuiVerifyf( newBlob.IsValid(), "fwuiRageMessageBuffer::Push - Unable to create space for data!" ) )
            {
                success = m_queue.Push( newBlob ) != nullptr;
                if( success )
                {
                    sysInterlockedIncrement( &m_entries );
                }
            }

            return success;
        }

        void GetFirst( void*& out_element, size_t& out_size )
        {
            // get as blob
            fwuiAssertf( out_element == nullptr, "fwuiRageMessageBuffer::GetFirst - Non-null storage provided, could be a leak?" );
            out_element = nullptr;
            out_size = 0;

            if( HasContent() )
            {
                MessageBlob firstBlob;
                if (m_queue.GetTail(firstBlob))
                {
                    out_element = firstBlob.GetData();
                    out_size = firstBlob.GetSize();
                }
            }
        }

        void FreeFirst()
        {
            if( HasContent() )
            {
                MessageBlob firstBlob = m_queue.Pop();
                firstBlob.Destruct();
                sysInterlockedDecrement( &m_entries );
            }
        }

        // NOTE - The internal queue doesn't expose a count due to it's
        // nature of being supported across threads. Our count sits atop
        // the queue and should only be used on consumer threads, not producer thread
        size_t GetEntryCount() const
        {
            u64 const c_resultCount = sysInterlockedRead( &m_entries );
            return (size_t)c_resultCount;
        }

        //! Does not call any destructor! Be prepared if you have dynamic data in here!
        void Clear()
        {
            while( HasContent() )
            {
                FreeFirst();
            }

            m_entries = 0;
        }

    protected:
        NON_COPYABLE(fwuiRageMessageBuffer);

    private: // variables
        RageMsgQueue m_queue;
        u64 m_entries;
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // end FWUI_RAGE_MESSAGE_BUFFER_H
