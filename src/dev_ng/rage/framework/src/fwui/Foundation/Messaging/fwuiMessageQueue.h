/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiMessageQueue.h
// PURPOSE : Dynamically sized message queue interface
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_MESSAGE_QUEUE_H
#define FWUI_MESSAGE_QUEUE_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/noncopyable.h"

// framework
#include "fwui/Foundation/fwuiRingBuffer.h"
#include "fwui/Foundation/Messaging/fwuiMessageQueueBase.h"

namespace rage
{
    //! Template to speficy the concrete buffer type
    template< typename t_messageType, typename t_bufferType >
    class fwuiMessageQueueDynamicWithStorage : public rage::fwuiMessageQueueBaseWithStorage<t_messageType,t_bufferType>
    { 
        typedef rage::fwuiMessageQueueBaseWithStorage<t_messageType,t_bufferType> superclass;

    public:
        fwuiMessageQueueDynamicWithStorage()
            : superclass()
        {

        }

        virtual ~fwuiMessageQueueDynamicWithStorage()
        {
            Shutdown();
        }

        bool Initialize( size_t const bufferSize )
        {
            t_bufferType& buffer = superclass::GetBuffer();
            bool const c_result = buffer.Initialize( bufferSize );
            return c_result;
        }

        bool IsInitialized() const
        {
            t_bufferType const& c_buffer = superclass::GetBuffer();
            return c_buffer.IsInitialized();
        }

        void Shutdown()
        {
            t_bufferType& buffer = superclass::GetBuffer();
            buffer.Shutdown();
        }

    protected:
        NON_COPYABLE( fwuiMessageQueueDynamicWithStorage );
    };

    // Default Version, uses dynamic ring buffer
    template< typename t_messageType >
    class fwuiMessageQueue : public rage::fwuiMessageQueueDynamicWithStorage<t_messageType, fwuiRingBuffer>
    { 
        typedef rage::fwuiMessageQueueDynamicWithStorage<t_messageType, fwuiRingBuffer> superclass;

    public:
        fwuiMessageQueue()
            : superclass()
        {

        }

        virtual ~fwuiMessageQueue()
        {
            
        }

    protected:
        NON_COPYABLE( fwuiMessageQueue );
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // end FWUI_MESSAGE_QUEUE_H
