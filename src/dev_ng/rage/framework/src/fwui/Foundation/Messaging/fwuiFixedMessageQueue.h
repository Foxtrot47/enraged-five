/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiFixedMessageQueue.h
// PURPOSE : Message queue interface of a fixed size
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_FIXED_MESSAGE_QUEUE_H
#define FWUI_FIXED_MESSAGE_QUEUE_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// framework
#include "fwui/Foundation/fwuiFixedRingBuffer.h"
#include "fwui/Foundation/Messaging/fwuiMessageQueueBase.h"
#include "fwui/Foundation/Messaging/fwuiRageMessageBuffer.h"

namespace rage
{
 
    // Uses our fixed ring-buffer for lock-free semantics
    template< typename t_messageType, size_t t_size >
    class fwuiFixedMessageQueue : public rage::fwuiMessageQueueBaseWithStorage<t_messageType, rage::fwuiFixedRingBuffer< t_size > >
    { 
        typedef rage::fwuiMessageQueueBaseWithStorage<t_messageType, rage::fwuiFixedRingBuffer< t_size > > superclass;

    public:
        fwuiFixedMessageQueue()
            : superclass()
        {

        }

        virtual ~fwuiFixedMessageQueue()
        {
            
        }

    protected:
        NON_COPYABLE( fwuiFixedMessageQueue );
    };

    // Uses our RAGE message queue for critical section locking semantics
    template< typename t_messageType, size_t t_size >
    class fwuiLockingFixedMessageQueue : public rage::fwuiMessageQueueBaseWithStorage<t_messageType, rage::fwuiRageMessageBuffer< t_size, true > >
    { 
        typedef rage::fwuiMessageQueueBaseWithStorage<t_messageType, rage::fwuiRageMessageBuffer< t_size, true > > superclass;

    public:
        fwuiLockingFixedMessageQueue()
            : superclass()
        {

        }

        virtual ~fwuiLockingFixedMessageQueue()
        {

        }

    protected:
        NON_COPYABLE( fwuiLockingFixedMessageQueue );
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // end FWUI_FIXED_MESSAGE_QUEUE_H
