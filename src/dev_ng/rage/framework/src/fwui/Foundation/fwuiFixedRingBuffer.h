/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiFixedRingBuffer.h
// PURPOSE : Fixed size implementation of a fwuiRingBufferBase
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_FIXED_RING_BUFFER_H
#define FWUI_FIXED_RING_BUFFER_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/noncopyable.h"

// framework
#include "fwui/Foundation/fwuiRingBufferBase.h"

namespace rage
{

    template < size_t t_size >
    class fwuiFixedRingBuffer : public fwuiRingBufferBase
    {
        typedef fwuiRingBufferBase superclass;

    public:

        fwuiFixedRingBuffer() 
            : superclass()
        { 
            SetDataPtr( m_fixedData, t_size );
        }

        virtual ~fwuiFixedRingBuffer() 
        {
            SetDataPtr( nullptr, 0 );
        }

    private:
        NON_COPYABLE(fwuiFixedRingBuffer);
        char m_fixedData[ t_size ];
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_FIXED_RING_BUFFER_H
