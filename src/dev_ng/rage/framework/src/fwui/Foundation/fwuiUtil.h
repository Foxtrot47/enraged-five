/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiUtil.h
// PURPOSE : Smaller utility functions used through-out the UI codebase
// 
// AUTHOR  : UI Team
// 
// Copyright (C) 2016- Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_UTIL_H
#define FWUI_UTIL_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "string/stringhash.h"

// framework
#include "fwui/fwui_channel.h"

namespace rage
{
    namespace fwuiUtil
    {
        // Calculates a atDataHash of the given container
        //
        // NOTE: Default implementation assumes linear storage. Use specialization where required for 
        // non-linear storage
        template< typename t_containerType >
        u32 CalculateContainerHash( t_containerType const& sourceContainer )
        {
            u32 hash = 0;

            if( !sourceContainer.empty() )
            {
                char const * const c_hashableData = reinterpret_cast<const char*>(&sourceContainer[0]);
                size_t const c_hashableDataSize = sourceContainer.size() * sizeof( sourceContainer[0] );
                hash = atDataHash( c_hashableData, c_hashableDataSize );
            }

            return hash;
        }

    } // fwuiUtil
        
} // rage

#endif // FW_UI_ENABLED

#endif // FWUI_UTIL_H
