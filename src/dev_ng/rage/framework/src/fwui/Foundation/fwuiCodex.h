/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiCodex.h
// PURPOSE: Class acts as a storage mechanic for pointers to external/transient objects
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 2019+ Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_CODEX_H
#define FWUI_CODEX_H

// framework includes
#include "fwui/fwui_channel.h"
#include "fwui/Foundation/fwuiCodexScope.h"

namespace rage
{

    typedef fwuiCodexScope<eCodexScopes::GLOBAL_CODEX_SCOPE> fwuiGlobalCodexScope;

    template< class t_scopeType>
    class fwuiCodexBase
    {
    public:
        //! Accessor to the element, returns 0 if not set
        template< typename t_storageType >
        t_storageType* getPointer() const
        {
            t_storageType* element = getPointerStorage<t_storageType>();
            return element;
        }

		template< typename t_storageType >
		t_storageType& get() const
		{
			t_storageType* element = getPointerStorage<t_storageType>();
			fwuiFatalAssertf( element, "Element of type does not exist in this codex!" );
			return *element;
		}

        template< typename t_storageType >
        t_storageType* checked_getPointer() const
        {
            t_storageType* element = getPointerStorage<t_storageType>();
            fwuiAssertf( element, "Element of type does not exist in this codex!" );
            return element;
        }

        template< typename t_storageType >
        void setPointer( t_storageType* element )
        {
            t_storageType*& storageLocation = getPointerStorage<t_storageType>();
            storageLocation = element;
        }

    private:

        //! Accessor for the internal storage of a pointer type, allowing it to be template created
        template< typename t_storageType >
        t_storageType*& getPointerStorage() const
        {
            static t_storageType* s_staticStorageType = nullptr;
            return s_staticStorageType;
        }
    };

    // Global implementation, as on previous projects
    class fwuiCodex final : public fwuiCodexBase<fwuiGlobalCodexScope> {};
        
} // rage

#endif // FWUI_CODEX_H
