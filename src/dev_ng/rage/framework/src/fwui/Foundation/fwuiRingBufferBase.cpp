/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiRingBufferBase.cpp
// PURPOSE : Simple ring-buffer class aimed at specific UI use-cases
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiRingBufferBase.h"

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/interlocked.h"
#include "system/memory.h"

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"

FW_UI_OPTIMISATIONS();

namespace rage
{
    fwuiRingBufferPrivateBase::~fwuiRingBufferPrivateBase()
    {

    }

    u32 fwuiRingBufferPrivateBase::GetEntryCount() const
    {
        u32 const c_result = rage::sysInterlockedRead( &m_entryCount );
        return c_result;
    }

    fwuiRingBufferPrivateBase::fwuiRingBufferPrivateBase()
        : m_head( nullptr )
        , m_tail( nullptr )
        , m_entryCount( 0 )
    {

    }

    char * fwuiRingBufferPrivateBase::GetHead()
    {
        char * const c_head = (char*)rage::sysInterlockedReadPointer( (void**)&m_head );
        return c_head;
    }

    void fwuiRingBufferPrivateBase::SetHead( char const * const head )
    {
        // Interlocked functions want non-const, but I want to preserve a const interfaces
        void * const headNonConst = const_cast<void*>((void const *)head);
        rage::sysInterlockedExchangePointer( (void**)&m_head, headNonConst );
    }

    char * fwuiRingBufferPrivateBase::GetTail()
    {
        char * const c_tail = (char*)rage::sysInterlockedReadPointer( (void**)&m_tail );
        return c_tail;
    }

    void fwuiRingBufferPrivateBase::SetTail( char const * const tail )
    {
        // Interlocked functions want non-const, but I want to preserve a const interfaces
        void * const tailNonConst = const_cast<void*>((void const *)tail);
        rage::sysInterlockedExchangePointer( (void**)&m_tail, tailNonConst );
    }

    u32 fwuiRingBufferPrivateBase::IncrementEntryCount()
    {
        return rage::sysInterlockedIncrement( &m_entryCount );
    }

    u32 fwuiRingBufferPrivateBase::DecrementEntryCount()
    {
        return rage::sysInterlockedDecrement( &m_entryCount );
    }

    fwuiRingBufferBase::~fwuiRingBufferBase()
    {

    }

    bool fwuiRingBufferBase::Push( void const * const data, size_t const size )
    {
        bool success = false;

        if( fwuiVerifyf( data, "Pushing null data to buffer" ) && 
            fwuiVerifyf( size <= c_maxElementSize, "Pushing size %" SIZETFMT "d to buffer, but max element size is %" SIZETFMT "d", 
            size, c_maxElementSize ) &&
            fwuiVerifyf( IsValid(), "Pushing to uninitialized ring buffer" ) &&
            fwuiVerifyf( size > 0 && ( size + GetEntryHeaderSize() ) <= GetSize(), 
            "Pushing invalid data size %" SIZETFMT "u to buffer of size %" SIZETFMT "u", size, GetSize() ) )
        {
            size_t const c_sizeNeeded = size + GetEntryHeaderSize();
            char* rawHead(nullptr);
            char* bestHead(nullptr);
            
            FindHead( c_sizeNeeded, rawHead, bestHead );

			const bool c_foundHead = ( rawHead && bestHead );

			fwuiFatalAssertf( c_foundHead, "Unable to fit element of size %" SIZETFMT "u into buffer of size %" SIZETFMT "u. "
							  "Consider flushing the buffer more regularly or making the buffer larger!", c_sizeNeeded, GetSize() );

            if( c_foundHead )
            {
                // Raw head may be before we wrapped around, so zero it out to act as a sentinel for our tail, provided it isn't
                // at the very end anyway
                if( rawHead != GetEnd() )
                {
                    Entry rawHeadEntry( rawHead );
                    rawHeadEntry.SetDataSize( 0 );
                }

                Entry headEntry( bestHead );
                headEntry.SetDataSize( size );
                sysMemCpy( headEntry.GetDataPtr(), data, size );

                SetHead( headEntry.GetEndPtr() );
                IncrementEntryCount();

                success = true;
            }            
        }

        return success;
    }

    void fwuiRingBufferBase::GetFirst( void*& out_element, size_t& out_size )
    {
        // Ensure tail is up to date. The last write may have wrapped to the start of the buffer
        UpdateTail();

        fwuiAssertf( out_element == nullptr, "Non-null storage provided, could be a leak?" );
        bool const c_tailValid = ( fwuiVerifyf( IsValid(), "Uninitialized ring buffer" ) && HasContent() );

        Entry const c_tail( GetTail() );
        out_size = c_tailValid ? c_tail.GetDataSize() : 0;
        out_element = c_tailValid ? c_tail.GetDataPtr() : nullptr;

        fwuiAssertf( !c_tailValid || out_element >= GetStart() && (char const* const)out_element + out_size <= GetEnd(), 
            "Returning message outside bounds. This is bad" );
    }

    void fwuiRingBufferBase::FreeFirst()
    {
        if( fwuiVerifyf( IsValid(), "Uninitialized ring buffer" ) && HasContent() )
        {
            DecrementEntryCount();

            Entry const c_tail( GetTail() );
            SetTail( c_tail.GetEndPtr() );
        }
    }

    fwuiRingBufferBase::fwuiRingBufferBase()
        : superclass()
        , m_data( nullptr )
        , m_dataSize( 0 )
    {
    }

    void fwuiRingBufferBase::SetDataPtr( char* data, size_t const dataSize ) 
    { 
        m_data = data; 
        m_dataSize = dataSize;
        SetHead( data );
        SetTail( data );
    }

    void fwuiRingBufferBase::FindHead( size_t const sizeNeeded, char*& out_rawHead, char*& out_bestHead )
    {
        out_rawHead  = GetHead();
        char* const tail = GetTail();
        char* const start = GetStart();
        char* const end   = GetEnd();

        bool const c_hasEntries = HasContent();
        bool const c_headBeforeOrAtTail = out_rawHead <= tail;

        char const * const c_rangeMax = c_hasEntries && c_headBeforeOrAtTail ? tail : end;
        bool const c_isTruncatingMax = truncates( out_rawHead, sizeNeeded, c_rangeMax );
        bool const c_wouldStartTruncateTail = truncates( start, sizeNeeded, tail );

        bool const c_wrapAroundToStart = ( c_isTruncatingMax && ( (!c_headBeforeOrAtTail && !c_wouldStartTruncateTail ) || !c_hasEntries ) );

        // If we aren't truncating, then use the head we found!
        out_bestHead = c_wrapAroundToStart && !c_wouldStartTruncateTail ? start : !c_isTruncatingMax ? out_rawHead : nullptr;
    }

    void fwuiRingBufferBase::UpdateTail()
    {
        char const * const c_start = GetStart();
        char const * const c_end   = GetEnd();
        char * const c_tail = GetTail();

        Entry const c_tailEntry( c_tail );

        char const * const c_newTail = HasContent() && ( c_tail >= c_end || c_tailEntry.GetDataSize() == 0 ) ? c_start : c_tail;
        SetTail( c_newTail );
    }

} // end namespace rage


#endif // FW_UI_ENABLED
