/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiCodexScopes.h
// PURPOSE: Class acts as a storage mechanic for pointers to external/transient objects
// 
// AUTHOR  : cconlon
// 
// Copyright (C) 2020 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef __FWUI_CODEX_SCOPES_H__
#define __FWUI_CODEX_SCOPES_H__

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

namespace rage
{
	typedef int fwuiCodexScopeRawType;
	enum class eCodexScopes : fwuiCodexScopeRawType
	{
		GLOBAL_CODEX_SCOPE = -1,
		ACCESS_TRACKER_SCOPE = -430105022 // ATSTRINGHASH("fwuiAccessTracker", 0xE65D1E42)
	};

	template <eCodexScopes n>
	struct fwuiCodexScope final
	{
	private:
		fwuiCodexScope() {}
		~fwuiCodexScope() {}
	};
} // rage

#endif // FW_UI_ENABLED


#endif // __FWUI_CODEX_SCOPES_H__
