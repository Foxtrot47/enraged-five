/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInstanceID.h
// PURPOSE : Helpers for identifying objects at runtime (along with some debug
//           info)
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_INSTANCE_ID_H
#define FWUI_INSTANCE_ID_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "atl/hashstring.h"
#include "string/stringhash.h"
#include "vector/color32.h"

namespace rage
{
    typedef atHashWithStringNotFinal fwuiInstanceId; // This is a literal type in future projects for data-binding support, but we don't have that system ehre
    typedef atLiteralHashValue fwuiParGenInstanceId;
}

#define FWUI_INSTANCE_FMT HASHFMT
#define FWUI_INSTANCE_OUT(x) HASHOUT(x)

#if RSG_BANK

// Keeps the validation, but returns the raw string so we have the name in dev builds
#define FWUI_INSTANCE_NAME_HASH( instanceName, hash )       ( rage::fwuiInstanceId( instanceName, hash ) )

#else

#define FWUI_INSTANCE_NAME_HASH( instanceName, hash )       ( rage::fwuiInstanceId( hash ) )

#endif

#define FWUI_INSTANCE_NAME_HASH_DYNAMIC( instanceName )     ( rage::fwuiInstanceId( instanceName ) )

#define FWUI_PARGEN_INSTANCE_NAME_HASH( instanceName, hash )       rage::fwuiInstanceId( instanceName, hash )
#define FWUI_PARGEN_INSTANCE_NAME_HASH_DYNAMIC( instanceName )     ( rage::fwuiInstanceId( instanceName ) )

#define FWUI_INVALID_INSTANCE_FROM_TYPE( HashType )		( HashType::Null() )
#define FWUI_INVALID_INSTANCE							( FWUI_INVALID_INSTANCE_FROM_TYPE( rage::fwuiInstanceId ) )
#define FWUI_INVALID_PARGEN_INSTANCE					( FWUI_INVALID_INSTANCE_FROM_TYPE( rage::fwuiParGenInstanceId ) )

//! Only required on a base class as it doesn't use virtuals
#define FWUI_DECLARE_INSTANCE_ID_BASE_WITH_TYPE( HashType ) public:                                                                         \
                                                                HashType getId() const { return GetIdStorage(); }                           \
                                                                void setId( char const * const id ) { setId( HashType( id ) ); }		    \
                                                                void setId( HashType const id ) { GetIdStorage() = id; }                    \
                                                                rage::Color32 getInstanceDebugRGBAColour() const { rage::Color32 result( m_id.GetHash() ); result.SetAlpha( 0xFF ); return result; }    \
                                                            private: 

//! For protected ID changing (i.e. using class wants to gate ID with validation)
#define FWUI_DECLARE_INSTANCE_ID_PROTECTED_BASE_WITH_TYPE( HashType )   public:                                                                                 \
                                                                            HashType getId() const { return GetIdStorage(); }                                   \
                                                                            rage::Color32 getInstanceDebugRGBAColour() const { rage::Color32 result( getId().GetHash() ); result.SetAlpha( 0xFF ); return result; }         \
                                                                        protected:                                                                                          \
                                                                            void SetIdInternal( char const * const id ) { SetIdInternal( HashType( id ) ); }    \
                                                                            void SetIdInternal( HashType const id ) { GetIdStorage() = id; }  

// Helper that can take the protected or public macro
#define FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_STORAGE( HashType, BaseMacro )   BaseMacro( HashType )                                       \
                                                                            protected:                                                  \
                                                                                HashType& GetIdStorage() { return m_id; }               \
                                                                                HashType const& GetIdStorage() const { return m_id; }   \
                                                                            private:                                                    \
                                                                                HashType    m_id;

// With storage, Public
#define FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_STORAGE_WITH_TYPE( HashType )    FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_STORAGE( HashType, FWUI_DECLARE_INSTANCE_ID_BASE_WITH_TYPE )
#define FWUI_DECLARE_INSTANCE_ID()                                          FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_STORAGE_WITH_TYPE( rage::fwuiInstanceId )

// With storage, protected
#define FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_PROTECTED_STORAGE_WITH_TYPE( HashType )  FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_STORAGE( HashType, FWUI_DECLARE_INSTANCE_ID_PROTECTED_BASE_WITH_TYPE )
#define FWUI_DECLARE_INSTANCE_ID_PROTECTED()                                        FWUI_DECLARE_INSTANCE_ID_TYPE_WITH_PROTECTED_STORAGE_WITH_TYPE( rage::fwuiInstanceId )     

#define FWUI_INSTANCE_ID_CONSTRUCTOR( id )					        m_id( id )
#define FWUI_INSTANCE_ID_CONSTRUCTOR_DEFAULT()				        FWUI_INSTANCE_ID_CONSTRUCTOR( FWUI_INVALID_INSTANCE )
#define FWUI_INSTANCE_ID_CONSTRUCTOR_DEFAULT_FROM_TYPE( HashType )	FWUI_INSTANCE_ID_CONSTRUCTOR( FWUI_INVALID_INSTANCE_FROM_TYPE( HashType) )
#define FWUI_INSTANCE_ID_CONSTRUCTOR_NAMED( instanceName, hash )    FWUI_INSTANCE_ID_CONSTRUCTOR( FWUI_INSTANCE_NAME_HASH( instanceName, hash ) ) 

#endif // FW_UI_ENABLED

#endif // FWUI_TYPE_ID_H
