/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiRingBufferBase.h
// PURPOSE : Simple ring-buffer class aimed at specific UI use-cases
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_RING_BUFFER_BASE_H
#define FWUI_RING_BUFFER_BASE_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

#include <stdint.h>
#include <stdlib.h> // for size_t on ORBIS

// rage
#include "system/noncopyable.h"

namespace rage
{
    //! Private base to ensure volatile members are accessed via interlocked operations
    class fwuiRingBufferPrivateBase
    {
    public:
        virtual ~fwuiRingBufferPrivateBase();

        u32 GetEntryCount() const;

    protected:
        fwuiRingBufferPrivateBase();

        char * GetHead();
        void SetHead( char const * const head );

        char * GetTail();
        void SetTail( char const * const tail );

        u32 IncrementEntryCount();
        u32 DecrementEntryCount();

    private:
        char * m_head;
        char * m_tail;
        u32 m_entryCount;
    };

    //! Base for a ring buffer
    class fwuiRingBufferBase : public fwuiRingBufferPrivateBase
    {
        typedef fwuiRingBufferPrivateBase superclass;

    public: // declarations and variables
        typedef u32 HeaderChunk; 

    public:
        virtual ~fwuiRingBufferBase();

        bool IsValid() const { return m_data != nullptr && m_dataSize > 0; }
        bool HasContent() const { return IsValid() && GetEntryCount() > 0; }

        size_t GetSize() const { return m_dataSize; }

        bool Push( void const * const data, size_t const size );

        void GetFirst( void*& out_element, size_t& out_size );
        void FreeFirst();

    protected: // methods

        // Only want derived implementations to be able to create us
        fwuiRingBufferBase();

        void SetDataPtr( char* data, size_t const dataSize );

        char* GetDataPtr() { return m_data; }
        char const * GetDataPtr() const { return m_data; }

        NON_COPYABLE(fwuiRingBufferBase);
            
    private:
        static const size_t c_maxElementSize = UINT32_MAX;

        static size_t GetEntryHeaderSize() { return sizeof(HeaderChunk); }

        //! Union to simplify data access
        union Entry
        {
            Entry( char * const _data )
                : m_raw( _data )
            { }

            size_t  GetDataSize() const { return *m_size; }
            void    SetDataSize( size_t const size ) const { *m_size = (HeaderChunk)size; }

            size_t  GetEntrySize() const { return GetDataSize() + GetEntryHeaderSize(); }
            char*   GetRawPtr() const { return m_raw; }
            char*   GetDataPtr() const { return m_raw + GetEntryHeaderSize(); }
            char*   GetEndPtr() const { return m_raw + GetEntrySize(); }

        private:
            HeaderChunk*    m_size;
            char*           m_raw;
        };

        char*   m_data;
        size_t  m_dataSize;

        char* GetStart() const { return m_data; }
        char* GetEnd() const { return m_data + GetSize(); }

        // Buffer can only store objects in contiguous blocks to avoid extra copying, so we calculate head to account for that
        void FindHead( size_t const sizeNeeded, char*& out_rawHead, char*& out_bestHead );
        void UpdateTail();

        static bool truncates( char const * const start, size_t const size, char const * const end )
        {
            return start + size > end;
        }
        
    };
        
} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_RING_BUFFER_H
