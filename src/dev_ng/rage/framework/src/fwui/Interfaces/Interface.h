/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : Interface.h
// PURPOSE : Due to lacking a interface keyword in C++, this header adds some
//           supporting help to stop bad citizens
// 
// AUTHOR  : james.strain
// 
// Copyright (C) 2018+ Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_INTERFACE
#define FWUI_INTERFACE

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

namespace rage
{
    
#define FWUI_DECLARE_INTERFACE( interfaceClass )  public:                                                                   \
                                                    virtual ~interfaceClass() {}                                            \
                                                  protected:                                                                \
                                                    interfaceClass() {}                                                     \
                                                  private:                        

#define FWUI_DEFINE_INTERFACE( interfaceClass )  CompileTimeAssert( sizeof( interfaceClass ) == sizeof( ptrdiff_t ) );

} // namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_INTERFACE
