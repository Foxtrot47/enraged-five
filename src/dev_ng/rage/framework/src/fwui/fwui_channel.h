//
// fwui/fwui_channel.h
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __fwui_channel
#define __fwui_channel fwui

#ifndef INC_FWUI_CHANNEL_H
#define INC_FWUI_CHANNEL_H

#include "fwui/fwui_config.h"

#if FW_UI_ENABLED

#include "fwui/fwui_channel_common.h"

namespace rage
{
	RAGE_DECLARE_CHANNEL(fwui)					    // Defined in fwui_channel.cpp

	#define fwuiAssert(cond)						RAGE_ASSERT(fwui,cond)
    #define fwuiAssertf(cond,fmt,...)				RAGE_ASSERTF(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
    #define fwuiAssertf2(cond,fmt,...)				RAGE_ASSERTF2(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
    #define fwuiAssertf3(cond,fmt,...)				RAGE_ASSERTF3(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
	#define fwuiVerify(cond)						RAGE_VERIFY(fwui, cond)
	#define fwuiVerify2(cond)						RAGE_VERIFY2(fwui, cond)
	#define fwuiVerify3(cond)						RAGE_VERIFY3(fwui, cond)
	#define fwuiVerifyf(cond,fmt,...)				RAGE_VERIFYF(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
	#define fwuiVerifyf2(cond,fmt,...)				RAGE_VERIFYF2(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
	#define fwuiVerifyf3(cond,fmt,...)				RAGE_VERIFYF3(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
	#define fwuiErrorf(fmt,...)						RAGE_ERRORF(fwui,fmt,##__VA_ARGS__)
	#define fwuiWarningf(fmt,...)					RAGE_WARNINGF(fwui,fmt,##__VA_ARGS__)
	#define fwuiDisplayf(fmt,...)					RAGE_DISPLAYF(fwui,fmt,##__VA_ARGS__)
	#define fwuiDebugf1(fmt,...)					RAGE_DEBUGF1(fwui,fmt,##__VA_ARGS__)
	#define fwuiDebugf2(fmt,...)					RAGE_DEBUGF2(fwui,fmt,##__VA_ARGS__)
	#define fwuiDebugf3(fmt,...)					RAGE_DEBUGF3(fwui,fmt,##__VA_ARGS__)
	#define fwuiLogf(severity,fmt,...)				RAGE_LOGF(fwui,severity,fmt,##__VA_ARGS__)
	#define fwuiCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,fwui,severity,fmt,##__VA_ARGS__)
	#define fwuiFatalf(fmt,...)						RAGE_FATALF(fwui,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)
	#define fwuiFatalAssertf(cond,fmt,...)			RAGE_FATALASSERTF(fwui,cond,"%s - " fmt,FWUI_FUNC_NAME,##__VA_ARGS__)


    #define FWUI_NO_OUTPUT ( __NO_OUTPUT || __PROFILE )

#if FWUI_NO_OUTPUT
    #define FWUI_OUTPUT_ONLY(...)
#else
    #define FWUI_OUTPUT_ONLY(...) __VA_ARGS__
#endif

} // namespace rage

#endif // FW_UI_ENABLED

#endif // INC_FWUI_CHANNEL_H
