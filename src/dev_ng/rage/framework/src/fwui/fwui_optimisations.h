//
// fwui/fwui_optimisations.h
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#ifndef FW_UI_OPTIMISATIONS_H
#define FW_UI_OPTIMISATIONS_H 

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

#define FW_UI_OPTIMISATIONS_OFF	(0 && !__FINAL)

#if FW_UI_OPTIMISATIONS_OFF
#define FW_UI_OPTIMISATIONS() OPTIMISATIONS_OFF()
#else
#define FW_UI_OPTIMISATIONS()
#endif // FW_UI_OPTIMISATIONS_OFF

#if FW_UI_OPTIMISATIONS_OFF
#define FW_UI_OPTIMISATIONS_OFF_ONLY(x)	x
#else
#define FW_UI_OPTIMISATIONS_OFF_ONLY(x)
#endif // FW_UI_OPTIMISATIONS_OFF

#endif // FW_UI_ENABLED

#endif // FW_UI_OPTIMISATIONS_H
