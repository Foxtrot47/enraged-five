//
// fwui/fwui_config.h
//
// Copyright (C)2021+ Rockstar Games.  All Rights Reserved.
//

#ifndef FW_UI_CONFIG_H
#define FW_UI_CONFIG_H 

#include "fwutil/Gen9Settings.h"

#define FW_UI_ENABLED	( 1 && ( IS_GEN9_PLATFORM || GEN9_UI_SIMULATION_ENABLED ) )

#if FW_UI_ENABLED
#define FW_UI_ENABLED_ONLY(...)    __VA_ARGS__
#else
#define FW_UI_ENABLED_ONLY(...)
#endif

#endif // FW_UI_CONFIG_H
