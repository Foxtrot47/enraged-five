//
// fwui/fwui_channel.cpp
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
#include "fwui/fwui_channel.h"

#if FW_UI_ENABLED

namespace rage
{

	RAGE_DEFINE_CHANNEL(fwui);

} // namespace rage

#endif // FW_UI_ENABLED
