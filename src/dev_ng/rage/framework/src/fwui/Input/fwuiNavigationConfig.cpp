/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationConfig.cpp
// PURPOSE : Configuration information for a navigation container
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiNavigationConfig.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"
#include "fwutil/xmacro.h"

FW_UI_OPTIMISATIONS();

namespace rage
{
    
    // Nothing complex enough yet. We may parGen this eventually, like RDR

}

#endif // FW_UI_ENABLED
