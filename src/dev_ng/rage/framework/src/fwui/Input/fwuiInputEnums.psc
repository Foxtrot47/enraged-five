<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema" generate="class">

  <!-- 
        Flags to adjust how we navigate a scene
  -->
  <enumdef type="rage::fwuiInput::eNavFlags">
    <enumval name="NAV_FLAGS_NONE" value="0" />
    <enumval name="NAV_FLAGS_WRAPPING_X" />
    <enumval name="NAV_FLAGS_WRAPPING_Y" />
    <enumval name="NAV_FLAGS_RAYCAST" />
    <enumval name="NAV_FLAGS_BLOCK_SCROLL_WHEEL_TRAVERSAL" />
  </enumdef>

  <!-- 
        Results from input handler actions
  -->
  <enumdef type="rage::fwuiInput::eHandlerResult">
    <enumval name="ACTION_NOT_HANDLED" value="0" description="We do not support this action" />
    <enumval name="ACTION_IGNORED" description="We support this action, but did nothing meaningful" />
    <enumval name="ACTION_HANDLED" description="We support this action and did something meaningful with it" />
  </enumdef>

</ParserSchema>
