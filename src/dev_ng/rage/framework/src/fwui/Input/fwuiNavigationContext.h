/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationContext.h
// PURPOSE : Acts as a simple storage class for the structures we need to track
//           on an object that supports navigation
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_NAVIGATION_CONTEXT_H
#define FWUI_NAVIGATION_CONTEXT_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// framework
#include "fwui/Input/fwuiNavigationConfig.h"
#include "fwui/Input/fwuiNavigationResult.h"

namespace rage
{
    class fwuiNavigationContext final
    {
    public:
        fwuiNavigationContext() 
        {}
        ~fwuiNavigationContext() 
        {}

        bool HasFocusedItem() const;
        bool AssignFocusItem(  fwuiNavigationResult const& newFocus );
        void ClearFocus();

        fwuiNavigationConfig const& GetConfig() const { return m_config; }
        fwuiNavigationResult const& GetFocusedItemDetails() const { return m_focusedItem; }


    private: // declarations and variables
        fwuiNavigationConfig const m_config;
        fwuiNavigationResult m_focusedItem;
    private: // methods
    };
}

#endif // FW_UI_ENABLED

#endif // FWUI_NAVIGATION_CONTEXT_H
