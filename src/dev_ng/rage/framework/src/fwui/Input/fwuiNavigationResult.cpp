/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationResult.cpp
// PURPOSE : Object to bundle up results from a navigation handler
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiNavigationResult.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"

FW_UI_OPTIMISATIONS();

namespace rage
{

    bool fwuiNavigationResult::IsValid() const
    {
        return m_itemIndex != INDEX_NONE && ( m_xResult != INDEX_NONE || m_yResult != INDEX_NONE );
    }

    void fwuiNavigationResult::Invalidate()
    {
        m_itemIndex = m_xResult = m_yResult = INDEX_NONE;
    }

} // end namespace rage

#endif // FW_UI_ENABLED
