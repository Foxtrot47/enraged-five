/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationConfig.h
// PURPOSE : Configuration information for a navigation container
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_NAVIGATION_CONFIG_H
#define FWUI_NAVIGATION_CONFIG_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "system/bit.h"

// framework
#include "fwutil/Flags.h"
#include "fwui/Input/fwuiInputEnums.h"

namespace rage
{
    class fwuiNavigationConfig final
    {
    public: // declaractions and variables
        typedef rage::u16 NavFlagsPrimitiveType;
        typedef fwFlags<NavFlagsPrimitiveType> NavFlagsType;

    public:
        fwuiNavigationConfig()
            : m_flags( fwuiInput::NAV_FLAGS_NONE )
        {}
        fwuiNavigationConfig( NavFlagsPrimitiveType const primitiveFlags )
            : m_flags( primitiveFlags )
        {}
        fwuiNavigationConfig( NavFlagsType const asFlags )
            : m_flags( asFlags )
        {}
        ~fwuiNavigationConfig()
        {}

        bool IsValid() const;
        void Reset();

        bool IsWrappingX() const { return IsFlagSet( fwuiInput::NAV_FLAGS_WRAPPING_X ); }
        bool IsWrappingY() const { return IsFlagSet( fwuiInput::NAV_FLAGS_WRAPPING_Y ); }
        bool ShouldForceRaycast() const { return IsFlagSet( fwuiInput::NAV_FLAGS_RAYCAST ); }

    private: // declarations and variables
        NavFlagsType    m_flags;

    private:

        bool const IsFlagSet( fwuiInput::eNavFlags const flag ) const
        {
            return m_flags.IsFlagSet( BIT( (fwuiNavigationConfig::NavFlagsPrimitiveType)flag ) );
        }

        void SetFlag( fwuiInput::eNavFlags const flag )
        {
            m_flags.SetFlag( BIT( (fwuiNavigationConfig::NavFlagsPrimitiveType)flag ) );
        }

        void ClearFlag( fwuiInput::eNavFlags const flag )
        {
            m_flags.ClearFlag( BIT( (fwuiNavigationConfig::NavFlagsPrimitiveType)flag ) );
        }
    };
}

#endif // FW_UI_ENABLED

#endif // FWUI_NAVIGATION_CONFIG_H
