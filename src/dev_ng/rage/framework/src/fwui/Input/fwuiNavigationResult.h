/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationResult.h
// PURPOSE : Object to bundle up results from a navigation handler
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_NAVIGATION_RESULT_H
#define FWUI_NAVIGATION_RESULT_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// framework
#include "fwui/Input/fwuiInputCommon.h"
#include "fwutil/xmacro.h"

namespace rage
{
    class fwuiNavigationResult final
    {
    public:
        fwuiNavigationResult()
            : m_xResult( INDEX_NONE )
            , m_yResult( INDEX_NONE )
            , m_itemIndex( INDEX_NONE )
        {}
        fwuiNavigationResult( int const focusableX, int const focusableY, int const itemIndex )
            : m_xResult( focusableX )
            , m_yResult( focusableY )
            , m_itemIndex( itemIndex )
        {}
        ~fwuiNavigationResult() {}

        bool operator==( fwuiNavigationResult const& other ) const
        {
            return IsEqual( other );
        }

        bool operator!=( fwuiNavigationResult const& other ) const
        {
            return !IsEqual( other );
        }

        bool IsEqual( fwuiNavigationResult const& other ) const
        {
            return m_xResult == other.m_xResult && m_yResult == other.m_yResult && m_itemIndex == other.m_itemIndex;
        }

        // This is the "layout domain" position of an item. Used for further navigation
        int GetFocusableX() const { return m_xResult; }
        int GetFocusableY() const { return m_yResult; }

        // This is the linear index of an item within a collection. Useful for interaction
        int GetItemIndex() const { return m_itemIndex; }

        bool IsValid() const;
        void Invalidate();

    private: // declarations and variables
        int m_xResult;
        int m_yResult;
        int m_itemIndex;
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_NAVIGATION_RESULT_H
