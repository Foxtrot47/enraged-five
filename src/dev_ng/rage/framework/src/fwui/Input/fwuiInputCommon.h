/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputCommon.h
// PURPOSE : Common types or data to share between the input system
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_SCREEN_INPUT_COMMON
#define FWUI_SCREEN_INPUT_COMMON

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// framework
#include "fwui/Input/fwuiInputEnums.h"
#include "fwutil/Flags.h"

namespace rage
{
    namespace fwuiInput
    {
        typedef fwFlags<eNavFlags> NavFlagsType;

        union fwuiNavDelta
        {
            int asInt;
            float asFloat;

            fwuiNavDelta( int const typedParam )
                : asInt( typedParam )
            {

            }

            fwuiNavDelta( float const typedParam )
                : asFloat( typedParam )
            {

            }
        };

    } // end namespace fwuiInput

}// end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_SCREEN_INPUT_COMMON
