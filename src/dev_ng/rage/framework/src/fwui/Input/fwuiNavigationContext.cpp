/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationContext.cpp
// PURPOSE : Acts as a simple storage class for the structures we need to track
//           on an object that supports navigation
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiNavigationContext.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"
#include "fwutil/xmacro.h"

FW_UI_OPTIMISATIONS();

namespace rage
{
    bool fwuiNavigationContext::HasFocusedItem() const
    {
        return m_focusedItem.IsValid();
    }

    bool fwuiNavigationContext::AssignFocusItem( fwuiNavigationResult const& newFocus )
    {
        bool const c_focusChanged = newFocus != m_focusedItem && newFocus.IsValid();
        if( c_focusChanged )
        {
            m_focusedItem = newFocus;
        }
        return c_focusChanged;
    }

    void fwuiNavigationContext::ClearFocus()
    {
        m_focusedItem.Invalidate();
    }

}

#endif // FW_UI_ENABLED
