/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationParams.cpp
// PURPOSE : Object to bundle up params for navigation processing
// 
// AUTHOR  : james.strain
// STARTES : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiNavigationParams.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"
#include "fwutil/xmacro.h"

FW_UI_OPTIMISATIONS();

namespace rage
{

} // end namespace rage

#endif // FW_UI_ENABLED
