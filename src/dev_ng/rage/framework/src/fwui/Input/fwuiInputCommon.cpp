/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputCommon.cpp
// PURPOSE : Common types or data to share between the input system
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiInputCommon.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"
#include "fwutil/xmacro.h"

FW_UI_OPTIMISATIONS();

namespace rage
{
    namespace fwuiInput
    {

    } // end namespace fwuiInput

}// end namespace rage

#endif // FW_UI_ENABLED
