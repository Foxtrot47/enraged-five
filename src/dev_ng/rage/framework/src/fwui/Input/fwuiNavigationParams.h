/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiNavigationParams.h
// PURPOSE : Object to bundle up params for navigation processing
// 
// AUTHOR  : james.strain
// STARTES : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_NAVIGATION_PARAMS_H
#define FWUI_NAVIGATION_PARAMS_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// framework
#include "fwui/Input/fwuiInputCommon.h"
#include "fwui/Input/fwuiNavigationConfig.h"
#include "fwui/Input/fwuiNavigationResult.h"

namespace rage
{
    class fwuiNavigationParams final
    {
    public:
        fwuiNavigationParams( fwuiNavigationConfig const navConfig, fwuiNavigationResult const& previousResult, fwuiInput::fwuiNavDelta const deltaX, fwuiInput::fwuiNavDelta const deltaY )
            : m_previousResult( previousResult )
            , m_navConfig( navConfig )
            , m_deltaX( deltaX )
            , m_deltaY( deltaY )
        {}
        ~fwuiNavigationParams() {}

        fwuiNavigationConfig const GetNavConfig() const { return m_navConfig; }

        bool IsValidDigital() const { return GetDigitalDeltaX() != 0 || GetDigitalDeltaY() != 0; }
        int GetDigitalDeltaX() const { return m_deltaX.asInt; }
        int GetDigitalDeltaY() const { return m_deltaY.asInt; }

        fwuiNavigationResult const& GetPreviousResult() const { return m_previousResult; }

    private: // declarations and variables
        fwuiNavigationResult const& m_previousResult;
        fwuiNavigationConfig const m_navConfig;

        fwuiInput::fwuiNavDelta m_deltaX;
        fwuiInput::fwuiNavDelta m_deltaY;
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_NAVIGATION_PARAMS_H
