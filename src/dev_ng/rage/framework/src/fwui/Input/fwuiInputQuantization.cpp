/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputQuantization.cpp
// PURPOSE : Methods for analogue/digital input quantization
// 
// AUTHOR  : geoff.herbynchuk
// STOLEN BY: james.strain
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiInputQuantization.h"

#if FW_UI_ENABLED

// rage
#include "math/simplemath.h"

// framework
#include "fwui/fwui_channel.h"
#include "fwui/fwui_optimisations.h"
#include "fwutil/xmacro.h"

FW_UI_OPTIMISATIONS();

// Hidden defines in pad.cpp
#if RSG_ORBIS
#define RSG_DEADZONE	0.13f		// From pad.cpp.   
#else
#define RSG_DEADZONE	0.20f
#endif

#define QUANTIZED_RADIAL_MAG_VALUE ( 1000.f )

namespace rage
{
    namespace fwuiInput
    {

        void QuantizeAnalogInput( Vec2f_In axialDelta, int& out_deltaX, int& out_deltaY)
        {
            out_deltaX = QuantizeValue( axialDelta.GetX());
            out_deltaY = QuantizeValue( axialDelta.GetY());
        }

        int QuantizeValue(float const value)
        {
	        // Slightly more varied quantization.   Again, unsure if exposure via navData is desired/expected here to allow a more customized range.   
	        // Intention is to allow most of the control to run through animations however, where we can modify duration, ease type, etc to allow for
	        // varied scrolling.  This will provide control for non-anim based as well as some variation via playback speed for anim based.   
	        int result = 0;
	        float const c_absValue = rage::Abs(value);

	        if (InRange<float>(c_absValue, 0.0f, RSG_DEADZONE))
	        {
		        result = 0;
	        }
	        else if (InRange<float>(c_absValue, RSG_DEADZONE, 0.5f))
	        {
		        result = 2;
	        }
	        else if ((c_absValue) > 0.5f)
	        {
		        result = 4;
	        }

	        if (value < 0.0f)
	        {
		        result *= -1;
	        }
	        return result;
        }


        int QuantizeRadialMagnitude(float const value)
        {
	        // Simple quantization of radial magnitude.  
	        float const c_quantizedAsFloat = value * QUANTIZED_RADIAL_MAG_VALUE;
	        int const c_result =  static_cast<int> (c_quantizedAsFloat );

	        return c_result;
        }

        float DeQuantizeRadialMagnitude(int const value)
        {
	        // Simple dequantization of quantized radial magnitude. 
	        float const c_result = static_cast<float>( value / QUANTIZED_RADIAL_MAG_VALUE );

	        return c_result;
        }
    } // end namespace fwuiInput

} // end namespace rage

#endif // FW_UI_ENABLED
