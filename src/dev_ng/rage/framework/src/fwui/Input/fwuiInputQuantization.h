/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputQuantization.h
// PURPOSE : Methods for analogue/digital input quantization
// 
// AUTHOR  : geoff.herbynchuk
// STOLEN BY: james.strain
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_INPUT_QUANTIZATION_H
#define FWUI_INPUT_QUANTIZATION_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "vectormath/vec2f.h"

// framework
#include "fwutil/xmacro.h"

namespace rage
{
    namespace fwuiInput
    {
        void QuantizeAnalogInput( Vec2f_In axialDelta, int& out_deltaX, int& out_deltaY );
        int QuantizeValue( float const value );

        int QuantizeRadialMagnitude( float const value );
        float DeQuantizeRadialMagnitude( int const value );

        inline static int QuantizeScrollWheelValue( float const value )
        {
            int const c_result = value >= 0.5f ? 1 :
                value <= -0.5f ? -1 : 0;

            return c_result;
        }
    } // end namespace fwuiInput

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_INPUT_QUANTIZATION_H
