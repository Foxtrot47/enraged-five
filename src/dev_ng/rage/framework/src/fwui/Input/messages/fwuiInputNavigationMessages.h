/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputMessages.h
// PURPOSE : Navigation messages for UI
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_INPUT_NAVIGATION_MESSAGES_H
#define FWUI_INPUT_NAVIGATION_MESSAGES_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "vectormath/vec2f.h"

// framework
#include "fwui/Input/Messages/fwuiInputMessages.h"

namespace rage
{
	class fwuiInputMessageItemNavigateBase : public fwuiInputMessageBase
	{
		FWUI_DECLARE_MESSAGE_DERIVED(fwuiInputMessageItemNavigateBase, fwuiInputMessageBase );

	public:
		fwuiInputMessageItemNavigateBase( ) : superclass(), m_consumed( false ) { }
		virtual ~fwuiInputMessageItemNavigateBase() { }

		void SetConsumed( bool const consumed ) { m_consumed = consumed; }
        bool const IsConsumed() const { return m_consumed; }

	private: // declarations and variables
        bool    m_consumed;
	};

	class fwuiInputMessageItemNavigateAll final : public fwuiInputMessageItemNavigateBase
	{
		FWUI_DECLARE_MESSAGE_DERIVED(fwuiInputMessageItemNavigateAll, fwuiInputMessageItemNavigateBase);
	public:

		fwuiInputMessageItemNavigateAll() : superclass() { SetXAxis(0); SetYAxis(0); }
		fwuiInputMessageItemNavigateAll( s8 xAxis, s8 yAxis ) : superclass(), m_xAxis(xAxis), m_yAxis(yAxis)  { }
		virtual ~fwuiInputMessageItemNavigateAll() { }

        bool HasHorizontalComponent() const { return m_xAxis != 0; }
		bool HasVerticalComponent() const { return m_yAxis != 0; }
		
		int GetHorizontalValue() const { return m_xAxis; }
		int GetVerticalValue() const { return m_yAxis; }
	
		void SetXAxis(s8 value) { m_xAxis = value;  }
		void SetYAxis(s8 value) { m_yAxis = value; }
	private:

		s8 m_xAxis;
		s8 m_yAxis;
	};

	class fwuiInputMessageAnalogMove : public fwuiInputMessageItemNavigateBase
	{
		FWUI_DECLARE_MESSAGE_DERIVED( fwuiInputMessageAnalogMove, fwuiInputMessageItemNavigateBase);

	public:
		fwuiInputMessageAnalogMove() : superclass(), m_value(Vec2f() ) { }
		fwuiInputMessageAnalogMove( Vec2f_In newValue ) : superclass(), m_value( newValue ) { }
		virtual ~fwuiInputMessageAnalogMove() { }

        Vec2f_ConstRef GetValue() const { return m_value; }
		void SetValue(Vec2f_In value) { m_value = value; }

	private: // declarations and variables
		Vec2f m_value;
	};

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_INPUT_NAVIGATION_MESSAGES_H
