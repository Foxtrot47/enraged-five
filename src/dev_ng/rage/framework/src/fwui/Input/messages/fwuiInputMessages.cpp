/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputMessages.cpp
// PURPOSE : Core UI input messages
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiInputMessages.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_optimisations.h"

FW_UI_OPTIMISATIONS();

namespace rage
{
    FWUI_DEFINE_MESSAGE( fwuiInputMessageBase, 0x38F20885 );
	
} // end namespace rage

#endif // FW_UI_ENABLED
