/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputNavigationMessages.cpp
// PURPOSE : Navigation messages for UI
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "fwuiInputNavigationMessages.h"

#if FW_UI_ENABLED

// framework
#include "fwui/fwui_optimisations.h"

FW_UI_OPTIMISATIONS();

namespace rage
{
	
	FWUI_DEFINE_MESSAGE( fwuiInputMessageItemNavigateBase, 0xF4231FEE);
    FWUI_DEFINE_MESSAGE( fwuiInputMessageItemNavigateAll, 0x99C76446 );
    FWUI_DEFINE_MESSAGE( fwuiInputMessageAnalogMove, 0x2215E989 );

} // end namespace rage

#endif // FW_UI_ENABLED
