/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiInputMessages.h
// PURPOSE : Core UI input messages
// 
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_INPUT_MESSAGES_H
#define FWUI_INPUT_MESSAGES_H

#include "fwui/fwui_config.h"
#if FW_UI_ENABLED

// rage
#include "vectormath/vec2f.h"

// framework
#include "fwui/Foundation/Messaging/fwuiMessageBase.h"

namespace rage
{
    class fwuiInputMessageBase : public rage::fwuiMessageBase
    {
        FWUI_DECLARE_MESSAGE_DERIVED( fwuiInputMessageBase, fwuiMessageBase );

    public:
        fwuiInputMessageBase() { }
        virtual ~fwuiInputMessageBase() { }
    };

} // end namespace rage

#endif // FW_UI_ENABLED

#endif // FWUI_INPUT_MESSAGES_H
