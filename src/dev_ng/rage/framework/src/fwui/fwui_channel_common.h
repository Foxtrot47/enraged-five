//
// fwui/fwui_channel_common.h
//
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_FWUI_CHANNEL_COMMON_H
#define INC_FWUI_CHANNEL_COMMON_H

#include "diag/channel.h"

#ifdef __GNUC__
#define FWUI_FUNC_NAME			__PRETTY_FUNCTION__		// virtual void someClass::SomeMember(argType argName,...) (on GCC, __FUNCTION__ would just be SomeMember, which isn't very useful)
#else
#define FWUI_FUNC_NAME			__FUNCTION__			// someClass::SomeMember
#endif

#endif // INC_FWUI_CHANNEL_COMMON_H
