/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : fwuiScaling.h
// PURPOSE : Header for some scaling functions we share across a few features
//
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWUI_SCALING_H
#define FWUI_SCALING_H

// system
#include <limits.h>

// rage
#include "math/amath.h"
#include "vectormath/vec2f.h"

namespace rage
{
    namespace fwuiScaling
    {
        enum eMode
        {
            UI_SCALE_FIRST,							

            UI_SCALE_NONE = UI_SCALE_FIRST,	            // Don't do anything, just clip if it's too big or small

            // Ignore aspect ratio
            UI_SCALE_STRETCH,							// Stretch to fit exactly

            // Maintaining aspect ratio of source
            UI_SCALE_CONSTRAIN,						    // Center, constrain to source dimensions if output size is larger than source
            UI_SCALE_CLAMP,							    // Best-fit Clamp to fit within the output frame, 
            UI_SCALE_CLIP,								// Center and clip edges to fill, maintaining aspect ratio of source
            UI_SCALE_CENTRE_NO_SCALE,					// Center but apply no scaling

            UI_SCALE_DEFAULT = UI_SCALE_CLAMP,
        };

        template<typename t_inUnitType, typename t_outUnitType>
        inline static t_outUnitType ScalingMulDiv( t_inUnitType const number, t_inUnitType const numerator, t_inUnitType const denominator )
        {
            t_outUnitType const c_mulDivResult = ( number * numerator ) / denominator;
            return c_mulDivResult;
        }

        template<> // Specialization - Mimics some requirements for the Media Foundation exporter
        inline int ScalingMulDiv<rage::s64,int>( rage::s64 const number, rage::s64 const numerator, rage::s64 const denominator )
        {
            rage::s64 mulDivResult = ( number * numerator ) / denominator;
            mulDivResult = mulDivResult > INT_MAX || mulDivResult < INT_MIN ? -1 : mulDivResult;

            return (int)mulDivResult;
        }

        template<typename t_unitType>
        inline static void CalculateBestFitClampDimensions( t_unitType& out_width, t_unitType& out_height, t_unitType const frameSourceWidth, t_unitType const frameSourceHeight, t_unitType const destWidth, t_unitType const destHeight )
        {
            t_unitType const c_mulDivResult = ScalingMulDiv<t_unitType,t_unitType>( frameSourceWidth, destHeight, frameSourceHeight );

            bool const c_heightAuthoritive( c_mulDivResult <= destWidth );

            out_width = c_heightAuthoritive ? ScalingMulDiv<t_unitType,t_unitType>( destHeight, frameSourceWidth, frameSourceHeight ) : destWidth;
            out_height = c_heightAuthoritive ? destHeight : ScalingMulDiv<t_unitType,t_unitType>( destWidth, frameSourceHeight, frameSourceWidth );
        }

        template<> // int specialization, since we need a different in/out type for ScalingMulDiv
        inline void CalculateBestFitClampDimensions<int>( int& out_width, int& out_height, int const frameSourceWidth, int const frameSourceHeight, int const destWidth, int const destHeight )
        {
            int const c_mulDivResult = ScalingMulDiv<rage::s64,int>( frameSourceWidth, destHeight, frameSourceHeight );

            bool const c_heightAuthoritive( c_mulDivResult <= destWidth );

            out_width = c_heightAuthoritive ? ScalingMulDiv<rage::s64,int>( destHeight, frameSourceWidth, frameSourceHeight ) : destWidth;
            out_height = c_heightAuthoritive ? destHeight : ScalingMulDiv<rage::s64,int>( destWidth, frameSourceHeight, frameSourceWidth );
        }

        template<typename t_unitType>
        inline static void CalculateBestFitClipDimensions( t_unitType& out_width, t_unitType& out_height, t_unitType const frameSourceWidth, t_unitType const frameSourceHeight, t_unitType const destWidth, t_unitType const destHeight )
        {
            float const c_sourceAspectRatio = (float)frameSourceWidth / (float)frameSourceHeight;
            bool const c_heightAuthorative( frameSourceWidth < destWidth || frameSourceHeight > frameSourceWidth );

            out_width = c_heightAuthorative ? (t_unitType)( destHeight * c_sourceAspectRatio ) : destWidth;
            out_height = c_heightAuthorative ? destHeight : (t_unitType)( destWidth / c_sourceAspectRatio );
        }

        template<typename t_unitType>
        inline static void CalculateBestFitConstrainDimensions( t_unitType& out_width, t_unitType& out_height, t_unitType const frameSourceWidth, t_unitType const frameSourceHeight, t_unitType const destWidth, t_unitType const destHeight )
        {
            t_unitType const c_destWidthConstrained = frameSourceWidth < destWidth ? frameSourceWidth : destWidth;
            t_unitType const c_destHeightConstrained = frameSourceHeight < destHeight ? frameSourceHeight : destHeight;

            CalculateBestFitClampDimensions<t_unitType>( out_width, out_height, frameSourceWidth, frameSourceHeight, c_destWidthConstrained, c_destHeightConstrained );
        }

        inline static bool ShouldCenterImageForScaleMode( eMode const scaleMode )
        {
            bool const c_shouldCenter = scaleMode == UI_SCALE_CLIP || scaleMode == UI_SCALE_CLAMP || 
                scaleMode == UI_SCALE_CENTRE_NO_SCALE || scaleMode == UI_SCALE_CONSTRAIN;
            return c_shouldCenter;
        }

        template<typename t_unitType>
        inline static void CalculateDimensions( eMode const scaleMode, t_unitType& out_width, t_unitType& out_height, t_unitType const frameSourceWidth, t_unitType const frameSourceHeight, t_unitType const destWidth, t_unitType const destHeight )
        {
            switch( scaleMode )
            {
            case UI_SCALE_CLAMP:
                {
                    CalculateBestFitClampDimensions<t_unitType>( out_width, out_height, frameSourceWidth, frameSourceHeight, destWidth, destHeight );
                    break;
                }

            case UI_SCALE_CLIP:
                {
                    CalculateBestFitClipDimensions<t_unitType>( out_width, out_height, frameSourceWidth, frameSourceHeight, destWidth, destHeight );
                    break;
                }

            case UI_SCALE_CONSTRAIN:
                {
                    CalculateBestFitConstrainDimensions<t_unitType>( out_width, out_height, frameSourceWidth, frameSourceHeight, destWidth, destHeight );
                    break;
                }

            case UI_SCALE_STRETCH:
                {
                    out_width = destWidth;
                    out_height = destHeight;
                    break;
                }

            case UI_SCALE_NONE:
            case UI_SCALE_CENTRE_NO_SCALE:
            default:
                {
                    out_width = frameSourceWidth;
                    out_height = frameSourceHeight;
                    break;
                }
            }
        }

        // Helper for newer code to utlize calls here
        inline static Vec2f CalculateDimensions( eMode const scaleMode, Vec2f_In source, Vec2f_In dest )
        {
            float xResult = 0.f;
            float yResult = 0.f;
            CalculateDimensions<float>( scaleMode, xResult, yResult, source.GetX(), source.GetY(), dest.GetX(), dest.GetY() );
            return Vec2f( xResult, yResult );
        }
    }
}

#endif // FWUI_SCALING_H
