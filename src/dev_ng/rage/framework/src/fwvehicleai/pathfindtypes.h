/////////////////////////////////////////////////////////////////////////////////
// FILE :    PathfindTypes.h
// PURPOSE : 
// AUTHOR :  Adam Croston
// CREATED : 07-05-09
/////////////////////////////////////////////////////////////////////////////////
#ifndef _PATHFINDTYPES_H_
#define _PATHFINDTYPES_H_

// Rage headers
#include "data/bitfield.h"
#include "data/struct.h"
#include "paging/base.h"
#include "parser/macros.h"

#include "vector/geometry.h"
#include "vector/vector2.h"
#include "vector/color32.h"
#include "atl/binmap.h"

namespace rage {

//#include "debug/DebugScene.h"

/////////////////////////////////////////////////////////////////////////////////
// Now that the pathfind data is streamed in we need an address for each node. The highest
// 6 bits contain the Region on the map that it lives in. The lower 10 bits represent the
// index of the node within the Region. This implies we can only have 1000 nodes per Region.
/////////////////////////////////////////////////////////////////////////////////
#define	PATHFINDMAPSPLIT 					(32)	// The number of sectors in x and y direction that the map is split up in.
#define	PATHFINDREGIONS 					(PATHFINDMAPSPLIT * PATHFINDMAPSPLIT)
#define	MAXNODESPERREGION 					(10240)	//(8192)
#define	MAXLINKSPERREGION					(32768)	//(16384)	// This is only so that we know how much mem to allocate to generate the streaming files.
#define MAXJUNCTIONSPERREGION				(256)
#define MAXHEIGHTSAMPLESPERREGION			(65536)
#define	MAXHIGRESNODECOORSPERREGION 		(1000)
//#define	PATHFINDREGIONSIZEX					((WORLD_WIDTHINSECTORS * WORLD_WIDTHOFSECTOR) / PATHFINDMAPSPLIT)
//#define	PATHFINDREGIONSIZEY					((WORLD_DEPTHINSECTORS * WORLD_DEPTHOFSECTOR) / PATHFINDMAPSPLIT)
#define	PATHFINDREGIONSIZEX					((WORLDLIMITS_REP_XMAX - WORLDLIMITS_REP_XMIN) / PATHFINDMAPSPLIT)
#define	PATHFINDREGIONSIZEY					((WORLDLIMITS_REP_YMAX - WORLDLIMITS_REP_YMIN) / PATHFINDMAPSPLIT)
#define	MAXINTERIORS						(0)		// We can only have this many interiors active at any one time

// fixed-point multipliers to convert between s16 and float for XY & Z components of a pathnode's position
static const int PATHCOORD_XYSHIFT_INT		= 4;
static const float PATHCOORD_XYSHIFT		= (float)PATHCOORD_XYSHIFT_INT;
static const float PATHCOORD_XYSHIFT_INV	= (1.0f / PATHCOORD_XYSHIFT);
static const float PATHCOORD_ZSHIFT			= 32.0f;	// reduced from 64, to support maps +/- 1024 in height
static const float PATHCOORD_ZSHIFT_INV		= (1.0f / PATHCOORD_ZSHIFT);

// some macros to compress stuff a bit
#define DIR_TO_BYTE( X )					((s8) ((X) * 100.0f))
#define BYTE_TO_DIR( X )					((X) * 0.001f)
#define COORS_TO_INT16( X ) 				((s16)((X) * PATHCOORD_XYSHIFT))
#define INT16_TO_COORS( X ) 				((X) * PATHCOORD_XYSHIFT_INV)
#define COORSZ_TO_UINT16( X ) 				((u16)((s16)((X) * PATHCOORD_ZSHIFT)))
#define UINT16_TO_COORSZ( X ) 				(((s16)(X)) * PATHCOORD_ZSHIFT_INV)


/////////////////////////////////////////////////////////////////////////////////
// The location of the node in the regions.
/////////////////////////////////////////////////////////////////////////////////
class CNodeAddress
{
public:
	CNodeAddress() { SetEmpty(); }

	__forceinline u32	GetRegion() const { return m_1.m_region; };
	__forceinline u32	GetIndex() const { return m_1.m_Index; };
	inline void Set( const u32 Region, const u32 index )
	{
		Assert(index <= MAXNODESPERREGION);
		Assert(Region < PATHFINDREGIONS + MAXINTERIORS);
		m_1.m_region = static_cast<u16>(Region);
		m_1.m_Index = static_cast<u16>(index);
	};
	inline void Set( const CNodeAddress address )
	{
		Assert(address.GetIndex() < MAXNODESPERREGION);
		Assert(address.GetRegion() <= PATHFINDREGIONS + MAXINTERIORS);
		m_1.m_region = address.m_1.m_region;
		m_1.m_Index = address.m_1.m_Index;
	};
	__forceinline void	SetEmpty() { m_regionAndIndex = 0xFFFFFFFF; }
	__forceinline bool	IsEmpty() const { return (GetRegion() == 65535); };
	__forceinline bool	IsInterior() { return( (!IsEmpty()) && GetRegion() >= PATHFINDREGIONS); };

	void	ToInt(s32 &intVal) const { intVal = ((s32)m_regionAndIndex) + 1; }
	void	FromInt(const s32 &intVal) { m_regionAndIndex = ((const u32)intVal) - 1; }

	u32		RegionAndIndex() const {return m_regionAndIndex;};

	bool operator == (const CNodeAddress &other) const { 
	Assertf( ((m_1.m_region == 65535) || (m_1.m_region <=MAXNODESPERREGION)), "region %d other region %d", m_1.m_region, other.m_1.m_region);//make sure we dont get silly values in here
	Assertf( ((other.m_1.m_region == 65535) || (other.m_1.m_region <=MAXNODESPERREGION)), "region %d other region %d", m_1.m_region, other.m_1.m_region);//make sure we dont get silly values in here
	return ((m_1.m_region == other.m_1.m_region) && (m_1.m_Index == other.m_1.m_Index)); };


	bool operator != (const CNodeAddress &other) const { return ((m_1.m_region != other.m_1.m_region) || (m_1.m_Index != other.m_1.m_Index)); };
	bool operator < (const CNodeAddress &other) const { return (GetRegion() < other.GetRegion()) || (GetRegion() == other.GetRegion() && GetIndex() < other.GetIndex()); };

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(CNodeAddress);
		STRUCT_FIELD(m_regionAndIndex);
		STRUCT_END();
	}
#endif // __DECLARESTRUCT

protected:
	union
	{
		u32 m_regionAndIndex;

		struct  
		{
			u32 DECLARE_BITFIELD_2(
				m_region,16,
				m_Index,16
				);
		} m_1;
	};
};


enum PathNodeSpecialUse
{
	SPECIAL_USE_NONE = 0,
	SPECIAL_PARKING_PARALLEL,
	SPECIAL_PARKING_PERPENDICULAR,
	SPECIAL_DROPOFF_GOODS,
	SPECIAL_DRIVE_THROUGH,
	SPECIAL_DRIVE_THROUGH_WINDOW,
	SPECIAL_DROPOFF_GOODS_UNLOAD,
	SPECIAL_HIDING_NODE,
	SPECIAL_SMALL_WORK_VEHICLES,
	SPECIAL_PETROL_STATION,
	SPECIAL_PED_CROSSING, // peds
	SPECIAL_DROPOFF_PASSENGERS,
	SPECIAL_DROPOFF_PASSENGERS_UNLOAD,
	SPECIAL_OPEN_SPACE,
	SPECIAL_PED_ASSISTED_MOVEMENT, // peds
	SPECIAL_TRAFFIC_LIGHT,
	SPECIAL_GIVE_WAY,
	SPECIAL_FORCE_JUNCTION,
	SPECIAL_PED_DRIVEWAY_CROSSING, // peds
	SPECIAL_RESTRICTED_AREA,	//the player gets wanted for going in here
	SPECIAL_FALSE_JUNCTION,
	SPECIAL_DISABLE_VEHICLE_CREATION,

	SPECIAL_USE_LAST	// 31 is maximum!
};
CompileTimeAssert(SPECIAL_USE_LAST <= 31); // (see m_specialFunction, currently 5 bits)


/////////////////////////////////////////////////////////////////////////////////
// This struct contains the temporary information for a single node in the grid of paths.
// Used for cars and peds alike
/////////////////////////////////////////////////////////////////////////////////
class CTempNode
{
public:
	Vector3 		m_Coors;
	CNodeAddress	m_NodeAddress;

	s32			m_NodeIndex;
	u32			m_StreetNameHash;			// This is the hash key for the string (8chars) that corresponds to the street name
	u32			m_FileId;
	u8			m_SpecialFunction;			// For vehicles
	u8			m_Speed;					// For vehicles
	u8			m_Density;					// For peds
	u16			m_bSwitchedOff:1;			// For peds and vehicles
	u16			m_bWaterNode:1;				// For vehicles
	u16			m_bHighwayOrLowBridge:1;	// For cars
	u16			m_bNoGps:1;
	u16			m_bOpenSpace:1;
	u16			m_bAdditionalTunnelFlag:1;	// For cars
	u16			m_bLeftOnly:1;
	u16			m_bNoLeftTurns:1;
	u16			m_bNoRightTurns:1;
	u16			m_bOffroad:1;
	u16			m_bNoBigVehicles:1;
	u16			m_bIndicateKeepLeft:1;		// For GPS
	u16			m_bIndicateKeepRight:1;
	u16			m_bSlipNode:1;

	inline bool IsPedNode() const				{ return ( m_SpecialFunction == SPECIAL_PED_CROSSING || m_SpecialFunction == SPECIAL_PED_DRIVEWAY_CROSSING || m_SpecialFunction == SPECIAL_PED_ASSISTED_MOVEMENT ); }
};


/////////////////////////////////////////////////////////////////////////////////
// This struct contains the temporary information for a single node in the grid of paths.
// Used for cars and peds alike
/////////////////////////////////////////////////////////////////////////////////
class CTempLink
{
public:
	u32			m_FileId;
	s32			m_Node1, m_Node2;			// The 2 links this node connects
	u16			m_bOnRoad:1;				// Used by peds
	u16			m_NarrowRoad:1;				// Used by cars
	u16			m_bProcessed:1;				// Used whilst creating the distant lights.
	u16			m_bIgnore:1;				// Used to indicate that the link should be ignored (Used whilst creating the distant lights).
	u16			m_bGpsCanGoBothWays:1;		// Used by gps only.
	u16			m_bShortCut:1;				// Shortcut link, available for pathfinding but not used by wandering vehicles
	u16			m_bDontUseForNavigation:1;	// Available for pathfinding, but for path following it will be skipped
	u16			m_bBlockIfNoLanes:1;		// Used by cars
	u8			m_Lanes1To2, m_Lanes2To1;	// Used by cars
	u8			m_Width;					// Used by cars (0-15)
};

class CTempVirtualJunction
{
public:
	u32 m_FileId;
	float m_fGridSpace;	//right now this is just a constant, do we need it to be variable?
	u16	m_nHeightBaseWorld;	//the minimum height allowed
	//u8*	m_nHeightOffsets;	//(maxZ + 1.0 - minZ) / 256
	u8 m_nXSamples;
	u8 m_nYSamples;
};

/////////////////////////////////////////////////////////////////////////////////
// This struct contains the information for a single node in the grid of paths.
/////////////////////////////////////////////////////////////////////////////////
class CPathNode
{
	CPathNode		*m_pNext;
	CPathNode		*m_pPrevious;			// m_pNext has to be the first thing in the class or else the HashTable stuff fucks up.

public:

	CNodeAddress	m_address;
	u32			m_streetNameHash;		// This is the hash key for the string (8chars) that corresponds to the street name
	s16			m_distanceToTarget;		// Used by the actual search algorithm
	s16			m_startIndexOfLinks;

	s16			m_coorsX, m_coorsY;

	union
	{
		u32 m_iAsInteger1;
		struct{
			u32 DECLARE_BITFIELD_11(
				m_group, 3,					// Flood Fill group of this fellow (mostly debugging)
				m_Offroad, 1,				// Offroad
				m_onPlayersRoad, 1,		// Set during a scan when this node is linked to the player's road, for use in vehicle population
				m_noBigVehicles, 1,			//

				m_cannotGoRight, 1,
				m_cannotGoLeft, 1,				// this node is probably adjacent to a slip lane which handles the left turning
				m_slipLane, 1,					// this node is the slip-lane entrance to a junction
				m_indicateKeepLeft, 1,
				m_indicateKeepRight, 1,

				m_specialFunction, 5,		// Some nodes have special functions. (Nightclub, parking etc)
				m_coorsZ, 16				// Don't remove more...
				);
		} m_1;
	};

	union
	{
		u32 m_iAsInteger2;
		struct{
			u32 DECLARE_BITFIELD_16(
				m_noGps,1,					// This node is not to be used by the gps. (Can be used by alleyways and petrol stations)
				m_closeToCamera, 1,			// To determine whether a node has been processed for being close to the camera yet.
				m_slipJunction, 1,		// has 3 or more non-shortcut links. wandering cars may have to make a decision about which way to go here
				m_alreadyFound, 1,			// Used by FindNthNodeClosestToCoors
				m_switchedOffOriginal,1,	
				m_waterNode, 1,	
				m_highwayOrLowBridge, 1,	// Highway nodes are used by the gps to detect off ramps.
				m_switchedOff, 1,			// true if this node is switched off (no cars here please)

				m_qualifiesAsJunction, 1,	
				m_speed, 2,					// Can only be 0(slow) 1(normal) 2(fast), 3(double speed)
				m_numLinks, 5,				// was 4 (30 July 2008)

				m_inTunnel, 1,				// Updated when nodes are processed. True if node is below water (in a tunnel) cars will switch lights on.
				m_distanceHash, 7,			// Is used by the q creation code to space cars 7 meters apart.

				m_density, 4,				// 0 = empty, 15 = normal density
				m_deadEndness, 3,			// 0 = no dead end. Follow the numbers along junctions in descending order to get to the main road.
				m_leftOnly, 1			// This node is (probably) on a slip-lane
				);	
		} m_2;
	};

	CPathNode():m_pNext(NULL),m_pPrevious(NULL),m_address(),m_streetNameHash(0),m_distanceToTarget(0),m_startIndexOfLinks(0),m_coorsX(0),m_coorsY(0),m_iAsInteger1(0),m_iAsInteger2(0){;}

	inline class CPathNode *GetPrevious() { return (m_pPrevious); };
	inline class CPathNode *GetNext() { return (m_pNext); };
	inline void		SetPrevious(class CPathNode *pPtr) { m_pPrevious = pPtr; };
	inline void		SetNext(class CPathNode *pPtr)     { m_pNext = pPtr; };

	inline u32 GetAddrRegion(void) const{return m_address.GetRegion();}
	inline u32 GetAddrIndex(void) const{return m_address.GetIndex();}

	inline void			SetCoors(const Vector3& V) { m_coorsX = COORS_TO_INT16(V.x); m_coorsY = COORS_TO_INT16(V.y); m_1.m_coorsZ = COORSZ_TO_UINT16(V.z); /*m_1_NotUsed = true;*/ };
	inline void        	GetCoors(Vector3& v) const;
	inline void        	GetCoorsXY(Vector3& v) const;
	inline Vector3     	GetPos() const {Vector3 out(Vector3::ZeroType); GetCoors(out); return out;}
	inline void        	GetCoors2(Vector2& v) const {v.x=INT16_TO_COORS(m_coorsX); v.y=INT16_TO_COORS(m_coorsY);}

	inline u32	NumLinks() const {return m_2.m_numLinks;}

	s32 FindNumberNonSpecialNeighbours() const;// Defined in pathfind.cpp (as it needs ThePaths to determine this).
	s32 FindNumberNonShortcutLinks() const;
	bool HasShortcutLinks() const;
	

	inline bool HasSpecialFunction() const	{ return m_1.m_specialFunction != SPECIAL_USE_NONE; }
	inline bool IsHighway() const			{ return (m_2.m_highwayOrLowBridge && !m_2.m_waterNode); }
	inline bool IsWaterNode() const			{ return m_2.m_waterNode; }
	inline bool IsPedNode() const			{ return ( m_1.m_specialFunction == SPECIAL_PED_CROSSING || m_1.m_specialFunction == SPECIAL_PED_DRIVEWAY_CROSSING || m_1.m_specialFunction == SPECIAL_PED_ASSISTED_MOVEMENT ); }
	inline bool IsParkingNode() const		{ return ( m_1.m_specialFunction == SPECIAL_PARKING_PARALLEL || m_1.m_specialFunction == SPECIAL_PARKING_PERPENDICULAR ); }
	inline bool IsOpenSpaceNode() const		{ return m_1.m_specialFunction == SPECIAL_OPEN_SPACE; }
	inline bool IsTrafficLight() const		{ return m_1.m_specialFunction == SPECIAL_TRAFFIC_LIGHT; }
	inline bool IsGiveWay() const			{ return m_1.m_specialFunction == SPECIAL_GIVE_WAY; }
	inline bool IsJunctionNode() const		{ return m_2.m_qualifiesAsJunction; }
	inline bool IsSlipLane() const			{ return m_1.m_slipLane;}
	inline bool IsOffroad() const			{ return m_1.m_Offroad; }
	inline bool IsSwitchedOff() const		{ return m_2.m_switchedOff; }
	inline bool BigVehiclesProhibited() const { return m_1.m_noBigVehicles;}	
	inline bool IsRestrictedArea() const	{ return m_1.m_specialFunction == SPECIAL_RESTRICTED_AREA;}
	inline bool IsFalseJunction() const		{ return m_1.m_specialFunction == SPECIAL_FALSE_JUNCTION; }

	inline void SetFalseJunction()			{ m_1.m_specialFunction = SPECIAL_FALSE_JUNCTION; }
	inline void ClearFalseJunction()		{ m_1.m_specialFunction = SPECIAL_USE_NONE; }

	//returns false for nodes that have no special function,
	//or one of the special function values used for standard traffic
	//nodes such as SPECIAL_TRAFFIC_LIGHT or SPECIAL_GIVE_WAY
	inline bool HasSpecialFunction_Driving() const 
	{
		return m_1.m_specialFunction != SPECIAL_USE_NONE
			&& m_1.m_specialFunction != SPECIAL_TRAFFIC_LIGHT
			&& m_1.m_specialFunction != SPECIAL_GIVE_WAY
			&& m_1.m_specialFunction != SPECIAL_FORCE_JUNCTION
			&& m_1.m_specialFunction != SPECIAL_RESTRICTED_AREA
			&& m_1.m_specialFunction != SPECIAL_FALSE_JUNCTION
			&& m_1.m_specialFunction != SPECIAL_DISABLE_VEHICLE_CREATION;
	}

	inline bool HasSpecialFunction_VehicleCreation() const
	{
		return m_1.m_specialFunction != SPECIAL_USE_NONE
			&& m_1.m_specialFunction != SPECIAL_SMALL_WORK_VEHICLES
			&& m_1.m_specialFunction != SPECIAL_TRAFFIC_LIGHT
			&& m_1.m_specialFunction != SPECIAL_FALSE_JUNCTION;
	}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(CPathNode);
		STRUCT_IGNORE(m_pNext);						// Run time only: Used by the actual search algorithm
		STRUCT_IGNORE(m_pPrevious);					// Run time only: Used by the actual search algorithm
		STRUCT_FIELD(m_address);
		STRUCT_FIELD(m_streetNameHash);
		STRUCT_IGNORE(m_distanceToTarget);			// Run time only: Used by the actual search algorithm
		STRUCT_FIELD(m_startIndexOfLinks);
		STRUCT_FIELD(m_coorsX);
		STRUCT_FIELD(m_coorsY);
		STRUCT_FIELD(m_iAsInteger1);
		STRUCT_FIELD(m_iAsInteger2);
		STRUCT_END();
	}
#endif // __DECLARESTRUCT
};


#if (__XENON || __PPU) && VECTORIZED 
#if __XENON // gcc (psn) chokes on offsetof on non-POD types
// To use a single lvlx instruction the data to unpack can't cross a 16 byte boundary 
//CompileTimeAssert((offsetof(CPathNode, m_1.CoorsX) & 15) <= 10); 
#endif 
// To ensure this is always the case CPathNode also needs 16 byte alignment
CompileTimeAssert((sizeof(CPathNode) & 15) == 0);
CompileTimeAssert(sizeof(CPathNode) == 32);
inline void CPathNode::GetCoors(Vector3& v) const
{
	static const __vector4 scalar = {PATHCOORD_XYSHIFT_INV, PATHCOORD_XYSHIFT_INV, PATHCOORD_ZSHIFT_INV, 0.0f};
	_hvector4 ixyzHalf = (_hvector4)__lvlx(&m_coorsX, 0);
	_ivector4 ixyz = __vupkhsh(ixyzHalf);
	__vector4 xyz = __vcsxwfp(ixyz, 0);
	v = __vmulfp(xyz, scalar);
}
inline void CPathNode::GetCoorsXY(Vector3& v) const
{
	static const __vector4 scalar = {PATHCOORD_XYSHIFT_INV, PATHCOORD_XYSHIFT_INV, 0.0f, 0.0f};
	_hvector4 ixyzHalf = (_hvector4)__lvlx(&m_coorsX, 0);
	_ivector4 ixyz = __vupkhsh(ixyzHalf);
	__vector4 xyz = __vcsxwfp(ixyz, 0);
	v = __vmulfp(xyz, scalar);
}
#else
inline void CPathNode::GetCoors(Vector3& v) const {v.x=INT16_TO_COORS(m_coorsX); v.y=INT16_TO_COORS(m_coorsY); v.z=UINT16_TO_COORSZ(m_1.m_coorsZ);}
inline void CPathNode::GetCoorsXY(Vector3& v) const {v.x=INT16_TO_COORS(m_coorsX); v.y=INT16_TO_COORS(m_coorsY); v.z=0.f;}
#endif


/////////////////////////////////////////////////////////////////////////////////
// This struct contains the information for a single link between two nodes in the grid.
/////////////////////////////////////////////////////////////////////////////////
static const float	LANEWIDTH							=5.4f;	// A lane is 6 metres wide (used to be 5)
static const float	LANEWIDTH_NARROW					=4.0f;
static const u32	ALL_LANES_THROUGH_CENTRE_FLAG_VAL	=15;	// If m_1.m_Width has this value both lanes will go through the centre of the road (small alleyways not used for ambient traffic)
class CPathNodeLink
{
public:

	CNodeAddress	m_OtherNode;

	union
	{
		u32 m_iAsInteger1;

		struct  
		{
			u32 DECLARE_BITFIELD_13(
				m_bGpsCanGoBothWays, 1,					// If this is true the gps will go both directions even on a one-way street.	
				m_bBlockIfNoLanes, 1,						// If there are 0 lanes in a direction, prevent pathing through here even if allowed to drive against traffic
				m_Tilt, 5,								// Roads will store their sideways tilt. The idea is that the transition to fudged physics cars will be smoother.
				m_TiltFalloff, 2,						// Looks up into a table of falloff width / heights.  0 = no falloff, 1 to 3 are different widths and heights
				m_NarrowRoad, 1,						// Some roads are a bit more narrow and traffic has to drive closer together
				m_LeadsToDeadEnd, 1,					// Precalculation of ThisNodeWillLeadIntoADeadEnd()
				m_LeadsFromDeadEnd, 1,					// and the same for the reverse link (so that SPU code can be optimal)
				m_Width, 4,								// Only used by cars. The width of the bit in the middle of the road (in meters)
				m_bDontUseForNavigation, 1,				// These are connecting links used to create merges/offramps, but shouldn't be navigated on
				m_bShortCut, 1,							// Shortcut links are used to allow vehicles to cut corners between lanes which ambient vehicles wouldn't
				m_LanesFromOtherNode, 3,
				m_LanesToOtherNode, 3,					// Only used by cars. To and From the other node
				m_Distance, 8
				);
		} m_1;
	};

	/////////////////////////////////////////////////////////////////////////////////
	// FUNCTION : For this link works out what the offset is from the center of the road
	// PURPOSE :  
	/////////////////////////////////////////////////////////////////////////////////
	inline float InitialLaneCenterOffset() const
	{
		if(m_1.m_Width == ALL_LANES_THROUGH_CENTRE_FLAG_VAL)
		{	
			// If this is the case all lanes go though the centre of the road.
			// This can be used for little alleyways that don't have ambient traffic on them.
			return 0.0f;
		}
		else
		{
			float LaneWidth = GetLaneWidth();

			if (m_1.m_LanesToOtherNode == 0)
			{
				return LaneWidth*(0.5f - m_1.m_LanesFromOtherNode * 0.5f);
			}
			else if (m_1.m_LanesFromOtherNode == 0)
			{
				return LaneWidth*(0.5f - m_1.m_LanesToOtherNode * 0.5f);
			}
			else
			{
				return ((LaneWidth * 0.5f) + (m_1.m_Width * 0.5f) );		// m_1.m_Width is stored in meters
			}
		}
	}

	inline bool IsSingleTrack() const
	{
		return m_1.m_Width == ALL_LANES_THROUGH_CENTRE_FLAG_VAL;
	}
	inline bool IsShortCut() const { return m_1.m_bShortCut; }
	inline bool IsDontUseForNavigation() const { return m_1.m_bDontUseForNavigation; }
	inline bool IsOneWay() const { return (m_1.m_LanesFromOtherNode==0 && m_1.m_LanesToOtherNode!=0) || (m_1.m_LanesToOtherNode==0 && m_1.m_LanesFromOtherNode!=0); }

	// Get the with of a single driving lane along this links direction.
	inline float GetLaneWidth() const
	{
		//TUNE_GROUP_BOOL(RACING_AI, makeAllRoadsAsNarrow, false);
		static bool makeAllRoadsAsNarrow = false;

		return (makeAllRoadsAsNarrow || m_1.m_NarrowRoad)?LANEWIDTH_NARROW:LANEWIDTH;
	};

	inline bool LeadsToDeadEnd() const { return m_1.m_LeadsToDeadEnd; }
	inline bool LeadsFromDeadEnd() const { return m_1.m_LeadsFromDeadEnd; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(CPathNodeLink);
		STRUCT_FIELD(m_OtherNode);
		STRUCT_FIELD(m_iAsInteger1);
		STRUCT_END();
	}
#endif // __DECLARESTRUCT
};
CompileTimeAssert(sizeof(CPathNodeLink) == 8);
//CompileTimeAssert(offsetof(CPathNodeLink, CPathNodeLink::m_1)	==  4);

class CPathVirtualJunction
{
public:
	//float m_fGridSpace;	//right now this is just a constant, do we need it to be variable?
	u16 m_uMaxZ;
	s16 m_iMinX;
	s16 m_iMinY;
	u16	m_nHeightBaseWorld;	//the minimum height allowed
	u16	m_nStartIndexOfHeightSamples;
	//u8*	m_nHeightOffsets;	//(maxZ + 1.0 - minZ) / 256
	u8 m_nXSamples;
	u8 m_nYSamples;
	//u8 Pad0, Pad1;	//unused
	static const float ms_fRangeUp;
	static const float ms_fGridSpace;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		//const int iNumSamples = (m_nXSamples * m_nYSamples) + 3;
		STRUCT_BEGIN(CPathVirtualJunction);
		//STRUCT_FIELD(m_fGridSpace);
		STRUCT_FIELD(m_uMaxZ);
		STRUCT_FIELD(m_iMinX);
		STRUCT_FIELD(m_iMinY);
		STRUCT_FIELD(m_nHeightBaseWorld);
		//STRUCT_DYNAMIC_ARRAY(m_nHeightOffsets, iNumSamples);
		STRUCT_FIELD(m_nStartIndexOfHeightSamples);
		STRUCT_FIELD(m_nXSamples);
		STRUCT_FIELD(m_nYSamples);
		//STRUCT_FIELD(Pad0);
		//STRUCT_FIELD(Pad1);
		STRUCT_END();
	}
#endif // __DECLARESTRUCT
};

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
class CPathRegion : public pgBase
{
public:
	CPathRegion();
	CPathRegion(datResource& rsc);
	DECLARE_PLACE(CPathRegion);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

#if !__FINAL
	bool SaveXML(const char* pFileName);
#endif //!__FINAL
#if __DEV || defined(RAGEBUILDER) || __RESOURCECOMPILER
	bool LoadXML(const char* pFileName);
#ifndef RAGEBUILDER
	bool SaveBinary(const char* pFileName);
#endif // !RAGEBUILDER
#endif // __DEV
	void Unload(void);

	CPathNode*			aNodes; // Array of the nodes for this Region
	s32				NumNodes;
	s32				NumNodesCarNodes;
	s32				NumNodesPedNodes;

	CPathNodeLink*		aLinks;
	s32				NumLinks;

	//s32				NumHighResNodeCoors;
	//Vector3*			aHighResNodeCoors;

	CPathVirtualJunction*	aVirtualJunctions;
	u8*						aHeightSamples;
	struct JunctionMapContainer
	{
		JunctionMapContainer(){}
		JunctionMapContainer(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct &s);
#endif //__DECLARESTRUCT
		atBinaryMap<s32, u32> JunctionMap;
		PAR_SIMPLE_PARSABLE;
	};
	JunctionMapContainer JunctionMap;
	s32				NumJunctions;
	u32				NumHeightSamples;
};

} //namespace rage {

#endif // _PATHFINDTYPES_H_
