/////////////////////////////////////////////////////////////////////////////////
// FILE :    PathfindTypes.cpp
// PURPOSE : 
// AUTHOR :  Adam Croston
// CREATED : 07-05-09
/////////////////////////////////////////////////////////////////////////////////
#include "ai/aioptimisations.h"
#include "fwvehicleai/pathfindtypes.h"

// Rage headers
#include "data/struct.h"
#include "data/safestruct.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "system/memory.h"
#include "spatialdata/sphere.h"
#include "atl/array_struct.h"
#include "atl/binmap_struct.h"

// Framework headers
#include "fwmaths/vector.h"

using namespace rage;

// Game headers
//#include "system/filemgr.h"
#include "pathregion_parser.h"

/////////////////////////////////////////////////////////////////////////////////
// CPathRegion
/////////////////////////////////////////////////////////////////////////////////

FW_AI_OPTIMISATIONS()

/////////////////////////////////////////////////////////////////////////////////
// FUNCTION : CPathRegion
// PURPOSE :  Default constructor.
/////////////////////////////////////////////////////////////////////////////////
CPathRegion::CPathRegion()
	:
	aNodes				(NULL),
	NumNodes			(0),
	NumNodesCarNodes	(0),
	NumNodesPedNodes	(0),
	aLinks				(NULL),
	NumLinks			(0),
	aVirtualJunctions	(NULL),
	NumJunctions		(0),
	aHeightSamples		(NULL),
	NumHeightSamples	(0)
	//NumHighResNodeCoors	(0),
	//aHighResNodeCoors	(NULL)
{;}


/////////////////////////////////////////////////////////////////////////////////
// FUNCTION : CPathRegion
// PURPOSE :  Constructor for resourcing.
/////////////////////////////////////////////////////////////////////////////////
CPathRegion::CPathRegion(datResource& rsc)
: JunctionMap(rsc)
{
	rsc.PointerFixup(aNodes);
	rsc.PointerFixup(aLinks);
	rsc.PointerFixup(aVirtualJunctions);
	rsc.PointerFixup(aHeightSamples);
}

IMPLEMENT_PLACE(CPathRegion);

#if __DECLARESTRUCT
/////////////////////////////////////////////////////////////////////////////////
// FUNCTION : DeclareStruct
// PURPOSE :  Structure reflection for resourcing.
/////////////////////////////////////////////////////////////////////////////////
void CPathRegion::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(CPathRegion, pgBase)
		SSTRUCT_DYNAMIC_ARRAY(CPathRegion, aNodes, NumNodes)
		SSTRUCT_FIELD(CPathRegion, NumNodes)
		SSTRUCT_FIELD(CPathRegion, NumNodesCarNodes)
		SSTRUCT_FIELD(CPathRegion, NumNodesPedNodes)
		SSTRUCT_DYNAMIC_ARRAY(CPathRegion, aLinks, NumLinks)
		SSTRUCT_FIELD(CPathRegion, NumLinks)
		//SSTRUCT_DYNAMIC_ARRAY(CPathRegion, aHighResNodeCoors, NumHighResNodeCoors)
		//SSTRUCT_FIELD(CPathRegion, NumHighResNodeCoors)
		SSTRUCT_DYNAMIC_ARRAY(CPathRegion, aVirtualJunctions, NumJunctions)
		SSTRUCT_DYNAMIC_ARRAY(CPathRegion, aHeightSamples, NumHeightSamples)
		SSTRUCT_FIELD(CPathRegion, JunctionMap)
		SSTRUCT_FIELD(CPathRegion, NumJunctions)
		SSTRUCT_FIELD(CPathRegion, NumHeightSamples)
	SSTRUCT_END(CPathRegion)
}
#endif // __DECLARESTRUCT

#if !__FINAL
/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	SaveXML
// PURPOSE :	Saves the path region data to the file.
// PARAMETERS :	None
// RETURNS :	Nothing
// NOTE:		Below I refer to "nodes" quite often, sometimes I am referring to
//				the XML DOM nodes and other times I am referring to the path nodes
//				themselves...
/////////////////////////////////////////////////////////////////////////////////
static const int PathRegionXMLFileVersionNumber = 2;
bool CPathRegion::SaveXML(const char* pFileName)
{
	// Make sure we have a proper file path to work with.
	Assertf(pFileName, "Sorry, need a valid file path.");

	// Test for degenerate regions
	if( NumNodes == 0 && NumLinks == 0 && NumJunctions == 0 && NumHeightSamples == 0 )
	{
		Displayf("Not outputing %s - region is degenerate", pFileName);
		return false;
	}
	else
	{
		Displayf("Outputting %s", pFileName);
	}

	// Create the XML DOM (Document Object Model) tree we will put our data into.
	parTree *pTree = rage_new parTree();
	Assert(pTree);

	// Create and set the root DOM node.
	// It is of the form: <pathregion version="1" NumNodes="12" ... > ... </pathregion>.
	parTreeNode* pRootNode = pTree->CreateRoot();
	Assert(pRootNode);
	parElement& rootElm = pRootNode->GetElement();
		rootElm.SetName("pathregion");
		rootElm.AddAttribute("version",				PathRegionXMLFileVersionNumber, false);
		rootElm.AddAttribute("NumNodes",			NumNodes, false);
		rootElm.AddAttribute("NumNodesCarNodes",	NumNodesCarNodes, false);
		rootElm.AddAttribute("NumNodesPedNodes",	NumNodesPedNodes, false);
		rootElm.AddAttribute("NumLinks",			NumLinks, false);
		rootElm.AddAttribute("NumJunctions",		NumJunctions, false);
		rootElm.AddAttribute("NumHeightSamples",	(int)NumHeightSamples, false);
		//rootElm.AddAttribute("NumHighResNodeCoors", NumHighResNodeCoors, false);
	pTree->SetRoot(pRootNode);

	// Create the paths nodes list and append it to the root DOM node.
	// It is of the form: <nodes> ... </nodes>.
	parTreeNode* pPathNodeListXMLNode = rage_new parTreeNode();
	Assert(pPathNodeListXMLNode);
	pPathNodeListXMLNode->GetElement().SetName("nodes");
	pPathNodeListXMLNode->AppendAsChildOf(pRootNode);
	const int nodesCount = NumNodes;
	for(int i = 0; i < nodesCount; ++i)
	{
		CPathNode* pNode = &(aNodes[i]);

		// Add the path node to the DOM tree.
		// These are of the form: <node addressRegion="1" addressIndex="55" streetNameHash="2423" ... />.
		parTreeNode* pPathNodeXMLNode = rage_new parTreeNode();
		Assert(pPathNodeXMLNode);
			pPathNodeXMLNode->GetElement().SetName("node");
			pPathNodeXMLNode->GetElement().AddAttribute("addressRegion",			(int)pNode->GetAddrRegion(), false);
			pPathNodeXMLNode->GetElement().AddAttribute("addressIndex",				(int)pNode->GetAddrIndex(), false);
			pPathNodeXMLNode->GetElement().AddAttribute("streetNameHash",			(int)pNode->m_streetNameHash, false);
			pPathNodeXMLNode->GetElement().AddAttribute("startIndexOfLinks",		pNode->m_startIndexOfLinks, false);
			Vector3 pos(0.0f, 0.0f, 0.0f);
			pNode->GetCoors(pos);
			pPathNodeXMLNode->GetElement().AddAttribute("posX",						pos.x, false);
			pPathNodeXMLNode->GetElement().AddAttribute("posY",						pos.y, false);
			pPathNodeXMLNode->GetElement().AddAttribute("posZ",						pos.z, false);
			pPathNodeXMLNode->GetElement().AddAttribute("group",					(int)pNode->m_1.m_group, false);
			pPathNodeXMLNode->GetElement().AddAttribute("cannotGoLeft",				(int)pNode->m_1.m_cannotGoLeft, false);
			pPathNodeXMLNode->GetElement().AddAttribute("cannotGoRight",			(int)pNode->m_1.m_cannotGoRight, false);
			pPathNodeXMLNode->GetElement().AddAttribute("specialFunction",			(int)pNode->m_1.m_specialFunction, false);
			pPathNodeXMLNode->GetElement().AddAttribute("slipLane",					(int)pNode->m_1.m_slipLane, false);
			pPathNodeXMLNode->GetElement().AddAttribute("noGps",					(int)pNode->m_2.m_noGps, false);
			pPathNodeXMLNode->GetElement().AddAttribute("closeToCamera",			(int)pNode->m_2.m_closeToCamera, false);
			pPathNodeXMLNode->GetElement().AddAttribute("dontWanderHere",			(int)pNode->m_2.m_slipJunction, false);
			pPathNodeXMLNode->GetElement().AddAttribute("alreadyFound",				(int)pNode->m_2.m_alreadyFound, false);
			pPathNodeXMLNode->GetElement().AddAttribute("switchedOffOriginal",		(int)pNode->m_2.m_switchedOffOriginal, false);
			pPathNodeXMLNode->GetElement().AddAttribute("waterNode",				(int)pNode->m_2.m_waterNode, false);
			pPathNodeXMLNode->GetElement().AddAttribute("highwayOrLowBridge",		(int)pNode->m_2.m_highwayOrLowBridge, false);
			pPathNodeXMLNode->GetElement().AddAttribute("switchedOff",				(int)pNode->m_2.m_switchedOff, false);
			pPathNodeXMLNode->GetElement().AddAttribute("qualifiesAsJunction",		(int)pNode->m_2.m_qualifiesAsJunction, false);
			pPathNodeXMLNode->GetElement().AddAttribute("speed",					(int)pNode->m_2.m_speed, false);
			pPathNodeXMLNode->GetElement().AddAttribute("numLinks",					(int)pNode->NumLinks(), false);
			pPathNodeXMLNode->GetElement().AddAttribute("inTunnel",					(int)pNode->m_2.m_inTunnel, false);
			pPathNodeXMLNode->GetElement().AddAttribute("distanceHash",				(int)pNode->m_2.m_distanceHash, false);
			pPathNodeXMLNode->GetElement().AddAttribute("density",					(int)pNode->m_2.m_density, false);
			pPathNodeXMLNode->GetElement().AddAttribute("deadEndness",				(int)pNode->m_2.m_deadEndness, false);
			pPathNodeXMLNode->GetElement().AddAttribute("leftOnly",					(int)pNode->m_2.m_leftOnly, false);
			pPathNodeXMLNode->GetElement().AddAttribute("offroad",					(int)pNode->m_1.m_Offroad, false);
			pPathNodeXMLNode->GetElement().AddAttribute("noBigVehicles",			(int)pNode->m_1.m_noBigVehicles, false);
			pPathNodeXMLNode->GetElement().AddAttribute("indicateKeepLeft",			(int)pNode->m_1.m_indicateKeepLeft, false);
			pPathNodeXMLNode->GetElement().AddAttribute("indicateKeepRight",		(int)pNode->m_1.m_indicateKeepRight, false);
		pPathNodeXMLNode->AppendAsChildOf(pPathNodeListXMLNode);
	}

	// Create the node links list and append it to the root DOM node.
	// It is of the form: <links> ... </links>.
	parTreeNode* pPathLinkListXMLNode = rage_new parTreeNode();
	Assert(pPathLinkListXMLNode);
	pPathLinkListXMLNode->GetElement().SetName("links");
	pPathLinkListXMLNode->AppendAsChildOf(pRootNode);
	const int linksCount = NumLinks;
	for(int i = 0; i < linksCount; ++i)
	{
		CPathNodeLink* pLink = &(aLinks[i]);

		// Add the link to the DOM tree, making sure to convert the link's node
		// indexes into link node ids.
		// These are of the form: <link otherNodeAddressRegion="46" otherNodeAddressIndex="177" ... />.
		parTreeNode* pPathLinkXMLNode = rage_new parTreeNode();
		Assert(pPathLinkXMLNode);
			pPathLinkXMLNode->GetElement().SetName("link");
			pPathLinkXMLNode->GetElement().AddAttribute("otherNodeAddressRegion",			(int)pLink->m_OtherNode.GetRegion(), false);
			pPathLinkXMLNode->GetElement().AddAttribute("otherNodeAddressIndex",			(int)pLink->m_OtherNode.GetIndex(), false);
			pPathLinkXMLNode->GetElement().AddAttribute("bGpsCanGoBothWays",				(int)pLink->m_1.m_bGpsCanGoBothWays, false);
			pPathLinkXMLNode->GetElement().AddAttribute("tilt",								(int)pLink->m_1.m_Tilt, false);
			pPathLinkXMLNode->GetElement().AddAttribute("tiltFallsOff",						(int)pLink->m_1.m_TiltFalloff, false);
			pPathLinkXMLNode->GetElement().AddAttribute("narrowRoad",						(int)pLink->m_1.m_NarrowRoad, false);
			pPathLinkXMLNode->GetElement().AddAttribute("leadsToDeadEnd",					(int)pLink->m_1.m_LeadsToDeadEnd, false);			
			pPathLinkXMLNode->GetElement().AddAttribute("leadsFromDeadEnd",					(int)pLink->m_1.m_LeadsFromDeadEnd, false);			
			pPathLinkXMLNode->GetElement().AddAttribute("width",							(int)pLink->m_1.m_Width, false);
			pPathLinkXMLNode->GetElement().AddAttribute("dontUseForNavigation",				(int)pLink->m_1.m_bDontUseForNavigation, false);
			pPathLinkXMLNode->GetElement().AddAttribute("shortCut",							(int)pLink->m_1.m_bShortCut, false);
			pPathLinkXMLNode->GetElement().AddAttribute("blockIfNoLanes",					(int)pLink->m_1.m_bBlockIfNoLanes, false);
			pPathLinkXMLNode->GetElement().AddAttribute("lanesFromOtherNode",				(int)pLink->m_1.m_LanesFromOtherNode, false);
			pPathLinkXMLNode->GetElement().AddAttribute("lanesToOtherNode",					(int)pLink->m_1.m_LanesToOtherNode, false);
			pPathLinkXMLNode->GetElement().AddAttribute("distance",							(int)pLink->m_1.m_Distance, false);
		pPathLinkXMLNode->AppendAsChildOf(pPathLinkListXMLNode);
	}

	// Create the junctions list and append it to the root DOM node.
	// It is of the form: <junctions> ... </junctions>.
	parTreeNode* pPathJunctionListXMLNode = rage_new parTreeNode();
	Assert(pPathJunctionListXMLNode);
	pPathJunctionListXMLNode->GetElement().SetName("junctions");
	pPathJunctionListXMLNode->AppendAsChildOf(pRootNode);
	const int junctionsCount = NumJunctions;
	for(int i = 0; i < junctionsCount; ++i)
	{
		CPathVirtualJunction* pJunction = &(aVirtualJunctions[i]);
		
		parTreeNode* pPathJunctionXMLNode = rage_new parTreeNode();
		Assert(pPathJunctionXMLNode);
			pPathJunctionXMLNode->GetElement().SetName("junction");
			//pPathJunctionXMLNode->GetElement().AddAttribute("gridspace",					pJunction->m_fGridSpace, false);
			pPathJunctionXMLNode->GetElement().AddAttribute("maxZ",							(int)pJunction->m_uMaxZ, false);
			pPathJunctionXMLNode->GetElement().AddAttribute("minX",							(int)pJunction->m_iMinX, false);
			pPathJunctionXMLNode->GetElement().AddAttribute("minY",							(int)pJunction->m_iMinY, false);
			pPathJunctionXMLNode->GetElement().AddAttribute("baseheight",					(int)pJunction->m_nHeightBaseWorld, false);
			pPathJunctionXMLNode->GetElement().AddAttribute("startindexofheightsamples",	(int)pJunction->m_nStartIndexOfHeightSamples, false);

// 			parTreeNode* pPathJunctionSampleListXMLNode = rage_new parTreeNode();
// 			Assert(pPathJunctionSampleListXMLNode);
// 			const int iNumSamples = (pJunction->m_nXSamples * pJunction->m_nYSamples) + 3;
// 			//create another child element for the array of samples
// 			for (int j = 0; j < iNumSamples; j++)
// 			{
// 				parTreeNode* pPathJunctionSampleXMLNode = rage_new parTreeNode();
// 				Assert(pPathJunctionSampleXMLNode);
// 
// 				pPathJunctionSampleXMLNode->GetElement().SetName("heightsample");
// 				pPathJunctionSampleXMLNode->GetElement().AddAttribute("relativeheight",	(int)pJunction->m_nHeightOffsets[j], false);
// 
// 				pPathJunctionSampleXMLNode->AppendAsChildOf(pPathJunctionSampleListXMLNode);
// 			}
// 			pPathJunctionSampleListXMLNode->GetElement().SetName("heightsamples");
// 			pPathJunctionSampleListXMLNode->AppendAsChildOf(pPathJunctionXMLNode);


			pPathJunctionXMLNode->GetElement().AddAttribute("numxsamples",				(int)pJunction->m_nXSamples, false);
			pPathJunctionXMLNode->GetElement().AddAttribute("numysamples",				(int)pJunction->m_nYSamples, false);

		pPathJunctionXMLNode->AppendAsChildOf(pPathJunctionListXMLNode);
	}

	// Create the heightsamples list and append it to the root DOM node.
	// It is of the form: <heightsamples> ... </heightsamples>.
	parTreeNode* pPathHeightSampleListXMLNode = rage_new parTreeNode();
	Assert(pPathHeightSampleListXMLNode);
	pPathHeightSampleListXMLNode->GetElement().SetName("heightsamples");
	pPathHeightSampleListXMLNode->AppendAsChildOf(pRootNode);
	const int samplesCount = NumHeightSamples;
	for(int i = 0; i < samplesCount; ++i)
	{
		parTreeNode* pPathHeightSampleXMLNode = rage_new parTreeNode();
		Assert(pPathHeightSampleXMLNode);

		pPathHeightSampleXMLNode->GetElement().SetName("heightsample");
		pPathHeightSampleXMLNode->GetElement().AddAttribute("height",				(int)aHeightSamples[i], false);
		pPathHeightSampleXMLNode->AppendAsChildOf(pPathHeightSampleListXMLNode);
	}


	parTreeNode* pJunctionMapXMLNode = PARSER.BuildTreeNode(&JunctionMap, "junctionmap");
	Assert(pJunctionMapXMLNode);
	pJunctionMapXMLNode->AppendAsChildOf(pRootNode);

	// Save out the XML DOM tree into a file.
	const bool bSaveRes = PARSER.SaveTree(pFileName, "", pTree, parManager::XML);
	if(!bSaveRes)
	{
		Errorf("Failed to save path region file '%s'.", pFileName);
	}

	// Free all the memory created for the DOM tree (and its children).
	delete pTree;

	return true;
}

#endif //__FINAL

#if __DEV || __RESOURCECOMPILER || defined(RAGEBUILDER)

/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	LoadXML
// PURPOSE :	Loads the path region specified in the file.
// PARAMETERS :	None
// RETURNS :	Nothing
// NOTE:		Below I refer to "nodes" quite often, sometimes I am referring to
//				the XML DOM nodes and other times I am referring to the path nodes
//				themselves...
/////////////////////////////////////////////////////////////////////////////////
bool CPathRegion::LoadXML(const char* pFileName)
{
	// Make sure we have a proper file path to work with.
	Assertf(pFileName, "Sorry, need a valid file path.");

	// Make sure the path nodes and links are currently clear and ready to be
	// filled in.
	//ClearAllNodesAndLinks();

	// Attempt to parse the path region XML data file and put our data into an
	// XML DOM (Document Object Model) tree.
	sysMemStartTemp();
	parSettings newSettings = PARSER.Settings();
	newSettings.SetFlag(parSettings::USE_TEMPORARY_HEAP, true);
	parTree* pTree = PARSER.LoadTree(pFileName, "", &newSettings);
	sysMemEndTemp();
	if(!pTree)
	{
		Errorf("Failed to lode path region file '%s'.", pFileName);
	}

	// Try to get the root DOM node.
	// It should be of the form: <pathregion version="1" NumNodes="12" ... > ... </pathregion>.
	const parTreeNode* pRootNode = pTree->GetRoot();
	Assert(pRootNode);
	Assert(stricmp(pRootNode->GetElement().GetName(), "pathregion") == 0);

	// Make sure we have the correct version.
	int version = pRootNode->GetElement().FindAttributeIntValue("version", 0);
	Assert(version == PathRegionXMLFileVersionNumber);
	if(version != PathRegionXMLFileVersionNumber)
	{
		// Free all the memory created for the DOM tree (and its children).
		sysMemStartTemp();
		delete pTree;
		sysMemEndTemp();

		return false;
	}

	// Get the other attributes of the path region.
	NumNodes			= pRootNode->GetElement().FindAttributeIntValue("NumNodes", 0);
	NumNodesCarNodes	= pRootNode->GetElement().FindAttributeIntValue("NumNodesCarNodes", 0);
	NumNodesPedNodes	= pRootNode->GetElement().FindAttributeIntValue("NumNodesPedNodes", 0);
	NumLinks			= pRootNode->GetElement().FindAttributeIntValue("NumLinks", 0);
	NumJunctions		= pRootNode->GetElement().FindAttributeIntValue("NumJunctions", 0);
	NumHeightSamples	= pRootNode->GetElement().FindAttributeIntValue("NumHeightSamples", 0);
	//NumHighResNodeCoors	= pRootNode->GetElement().FindAttributeIntValue("NumHighResNodeCoors", 0);

	// Allocate space for the path nodes.
	if(NumNodes != 0)
	{
		u32	size=NumNodes * sizeof(CPathNode);
		if(size<64*1024*1024)
		{
			aNodes = rage_new CPathNode[NumNodes];//(CPathNode *) sysMemAllocator::GetMaster().Allocate(size, 16, MEMTYPE_GAME_VIRTUAL);
			Assertf(aNodes, "Alloc failed when loading path nodes");
			if (aNodes == NULL)
			{
				// Free all the memory created for the DOM tree (and its children).
				sysMemStartTemp();
				delete pTree;
				sysMemEndTemp();

				return false;
			}
		}
		else
		{
			Assertf(false, " PathFindData is > 64meg");
			// Free all the memory created for the DOM tree (and its children).
			sysMemStartTemp();
			delete pTree;
			sysMemEndTemp();

			return false;
		}
	}	
	else
	{
		aNodes = rage_new CPathNode[1];//(CPathNode *) sysMemAllocator::GetMaster().Allocate(sizeof(CPathNode), 16, MEMTYPE_GAME_VIRTUAL);
		Assertf(aNodes, "Alloc failed when loading path nodes");
		if (aNodes == NULL)
		{
			// Free all the memory created for the DOM tree (and its children).
			sysMemStartTemp();
			delete pTree;
			sysMemEndTemp();

			return false;
		}
	}

	// Allocate space for the path links.
	if(NumLinks != 0)
	{
		aLinks = rage_new CPathNodeLink[NumLinks];//(CPathNodeLink *) sysMemAllocator::GetMaster().Allocate( (NumLinks) * sizeof(CPathNodeLink), 16, MEMTYPE_GAME_VIRTUAL);
		Assertf(aLinks, "Alloc failed when loading path links");
		if (aLinks == NULL)
		{
			if (aNodes)
			{
				delete[] aNodes;//sysMemAllocator::GetMaster().Free(aNodes);
				aNodes = NULL;
			}

			// Free all the memory created for the DOM tree (and its children).
			sysMemStartTemp();
			delete pTree;
			sysMemEndTemp();

			return false;
		}
	}
	else
	{
		aLinks = NULL;
	}

	// Allocate space for the virtual junctions.
	if(NumJunctions != 0)
	{
		aVirtualJunctions = rage_new CPathVirtualJunction[NumJunctions];
		Assertf(aVirtualJunctions, "Alloc failed when loading virtual junctions");
		if (aVirtualJunctions == NULL)
		{
			if (aNodes)
			{
				delete[] aNodes;//sysMemAllocator::GetMaster().Free(aNodes);
				aNodes = NULL;
			}

			if (aLinks)
			{
				delete[] aLinks;
				aLinks = NULL;
			}

			// Free all the memory created for the DOM tree (and its children).
			sysMemStartTemp();
			delete pTree;
			sysMemEndTemp();

			return false;
		}
	}
	else
	{
		aVirtualJunctions = NULL;
	}

	// Allocate space for the virtual junction height samples.
	if(NumHeightSamples != 0)
	{
		aHeightSamples = rage_new u8[NumHeightSamples];
		Assertf(aHeightSamples, "Alloc failed when loading height samples");
		if (aHeightSamples == NULL)
		{
			if (aNodes)
			{
				delete[] aNodes;//sysMemAllocator::GetMaster().Free(aNodes);
				aNodes = NULL;
			}

			if (aLinks)
			{
				delete[] aLinks;
				aLinks = NULL;
			}

			if (aVirtualJunctions)
			{
				delete[] aVirtualJunctions;
				aVirtualJunctions = NULL;
			}

			// Free all the memory created for the DOM tree (and its children).
			sysMemStartTemp();
			delete pTree;
			sysMemEndTemp();

			return false;
		}
	}
	else
	{
		aHeightSamples = NULL;
	}

	// Go over any and all the DOM child trees and DOM nodes off of the root
	// DOM node.
	parTreeNode::ChildNodeIterator i = pRootNode->BeginChildren();
	for(; i != pRootNode->EndChildren(); ++i)
	{
		// Check if this DOM child is the path nodes list or node links list.
		// It should be of the form: <nodes> ... </nodes>.
		// Or it should be of the form: <links> ... </links>.
		if(stricmp((*i)->GetElement().GetName(), "nodes") == 0)
		{
			// Go over any and all the child DOM trees and DOM nodes off of the nodes
			// list.
			int nodeIndex = 0;
			parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
			for(; j != (*i)->EndChildren(); ++j)
			{
				Assert(stricmp((*j)->GetElement().GetName(), "node") == 0);
				Assertf(nodeIndex < NumNodes, "More nodes than expected encountered.");
				CPathNode* pNode = &(aNodes[nodeIndex]);

				// Add the path node to our local data.
				// These are of the form: <node addressRegion="1" addressIndex="55" streetNameHash="2423" ... />.
				u32 region						= (*j)->GetElement().FindAttributeIntValue("addressRegion", 0);
				u32 index						= (*j)->GetElement().FindAttributeIntValue("addressIndex", 0);
				pNode->m_address.Set(region, index);
				pNode->m_streetNameHash				= (*j)->GetElement().FindAttributeIntValue("streetNameHash", 0);
				pNode->m_startIndexOfLinks			= (s16)(*j)->GetElement().FindAttributeIntValue("startIndexOfLinks", 0);
				Vector3 pos(0.0f, 0.0f, 0.0f);
				pos.x								= (*j)->GetElement().FindAttributeFloatValue("posX", 0.0f);
				pos.y								= (*j)->GetElement().FindAttributeFloatValue("posY", 0.0f);
				pos.z								= (*j)->GetElement().FindAttributeFloatValue("posZ", 0.0f);
				pNode->SetCoors(pos);
				pNode->m_1.m_group					= (*j)->GetElement().FindAttributeIntValue("group", 0);	
				pNode->m_1.m_cannotGoLeft			= (*j)->GetElement().FindAttributeIntValue("cannotGoLeft", 0);
				pNode->m_1.m_cannotGoRight			= (*j)->GetElement().FindAttributeIntValue("cannotGoRight", 0);
				pNode->m_1.m_specialFunction		= (*j)->GetElement().FindAttributeIntValue("specialFunction", 0);
				pNode->m_1.m_slipLane				= (*j)->GetElement().FindAttributeIntValue("slipLane", 0);				
				pNode->m_2.m_noGps					= (*j)->GetElement().FindAttributeIntValue("noGps", 0);
				pNode->m_2.m_closeToCamera			= (*j)->GetElement().FindAttributeIntValue("closeToCamera", 0);
				const bool bSlipJunction = ((*j)->GetElement().FindAttributeIntValue("dontWanderHere", 0) != 0) 
					|| ((*j)->GetElement().FindAttributeIntValue("slipJunction", 0) != 0);
				pNode->m_2.m_slipJunction			= bSlipJunction;
				pNode->m_2.m_alreadyFound			= (*j)->GetElement().FindAttributeIntValue("alreadyFound", 0);
				pNode->m_2.m_switchedOffOriginal	= (*j)->GetElement().FindAttributeIntValue("switchedOffOriginal", 0);
				pNode->m_2.m_waterNode				= (*j)->GetElement().FindAttributeIntValue("waterNode", 0);
				pNode->m_2.m_highwayOrLowBridge		= (*j)->GetElement().FindAttributeIntValue("highwayOrLowBridge", 0);
				pNode->m_2.m_switchedOff			= (*j)->GetElement().FindAttributeIntValue("switchedOff", 0);
				pNode->m_2.m_qualifiesAsJunction	= (*j)->GetElement().FindAttributeIntValue("qualifiesAsJunction", 0);
				pNode->m_2.m_speed					= (*j)->GetElement().FindAttributeIntValue("speed", 0);
				pNode->m_2.m_numLinks				= (*j)->GetElement().FindAttributeIntValue("numLinks", 0);
				pNode->m_2.m_inTunnel				= (*j)->GetElement().FindAttributeIntValue("inTunnel", 0);
				pNode->m_2.m_distanceHash			= (*j)->GetElement().FindAttributeIntValue("distanceHash", 0);
				pNode->m_2.m_density				= (*j)->GetElement().FindAttributeIntValue("density", 0);
				pNode->m_2.m_deadEndness			= (*j)->GetElement().FindAttributeIntValue("deadEndness", 0);
				pNode->m_2.m_leftOnly				= (*j)->GetElement().FindAttributeIntValue("leftOnly", 0);
				pNode->m_1.m_Offroad				= (*j)->GetElement().FindAttributeIntValue("offroad", 0);
				pNode->m_1.m_noBigVehicles			= (*j)->GetElement().FindAttributeIntValue("noBigVehicles", 0);
				pNode->m_1.m_indicateKeepLeft		= (*j)->GetElement().FindAttributeIntValue("indicateKeepLeft", 0);
				pNode->m_1.m_indicateKeepRight		= (*j)->GetElement().FindAttributeIntValue("indicateKeepRight", 0);

				++nodeIndex; 
			}
		}
		else if(stricmp((*i)->GetElement().GetName(), "links") == 0)
		{
			// Go over any and all the child DOM trees and DOM nodes off of the node
			// links list.
			int linkIndex = 0;
			parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
			for(; j != (*i)->EndChildren(); ++j)
			{
				Assert(stricmp((*j)->GetElement().GetName(), "link") == 0);
				Assertf(linkIndex < NumLinks, "More links than expected encountered.");
				CPathNodeLink* pLink = &(aLinks[linkIndex]);

				// Add the link to our local data, but don't yet sure to convert the
				// link node id into a link node index, we will do that later (below)
				// after we are sure all the nodes and links have been parsed.
				// These are of the form: <link otherNodeAddressRegion="46" otherNodeAddressIndex="177" ... />.
				u32 region									= (*j)->GetElement().FindAttributeIntValue("otherNodeAddressRegion", 0);
				u32 index									= (*j)->GetElement().FindAttributeIntValue("otherNodeAddressIndex", 0);
				pLink->m_OtherNode.Set(region, index);
				pLink->m_1.m_bGpsCanGoBothWays					= (*j)->GetElement().FindAttributeIntValue("bGpsCanGoBothWays", 0);
				pLink->m_1.m_Tilt								= (*j)->GetElement().FindAttributeIntValue("tilt", 0);
				pLink->m_1.m_TiltFalloff						= (*j)->GetElement().FindAttributeIntValue("tiltFallsOff", 0);
				pLink->m_1.m_NarrowRoad							= (*j)->GetElement().FindAttributeIntValue("narrowRoad", 0);
				pLink->m_1.m_LeadsToDeadEnd						= (*j)->GetElement().FindAttributeIntValue("leadsToDeadEnd", 0);
				pLink->m_1.m_LeadsFromDeadEnd					= (*j)->GetElement().FindAttributeIntValue("leadsFromDeadEnd", 0);
				pLink->m_1.m_Width								= (*j)->GetElement().FindAttributeIntValue("width", 0);
				pLink->m_1.m_bDontUseForNavigation				= (*j)->GetElement().FindAttributeIntValue("dontUseForNavigation", 0);
				pLink->m_1.m_bShortCut							= (*j)->GetElement().FindAttributeIntValue("shortCut", 0);
				pLink->m_1.m_bBlockIfNoLanes					= (*j)->GetElement().FindAttributeIntValue("blockIfNoLanes", 0);
				pLink->m_1.m_LanesFromOtherNode					= (*j)->GetElement().FindAttributeIntValue("lanesFromOtherNode", 0);
				pLink->m_1.m_LanesToOtherNode					= (*j)->GetElement().FindAttributeIntValue("lanesToOtherNode", 0);
				pLink->m_1.m_Distance							= (*j)->GetElement().FindAttributeIntValue("distance", 0);

				++linkIndex;
			}
		}
		else if (stricmp((*i)->GetElement().GetName(), "junctions") == 0)
		{
			// Go over any and all the child DOM trees and DOM nodes off of the node
			// junctions list.
			int junctionIndex = 0;
			parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
			for(; j != (*i)->EndChildren(); ++j)
			{
				Assert(stricmp((*j)->GetElement().GetName(), "junction") == 0);
				Assertf(junctionIndex < NumJunctions, "More junctions than expected encountered.");
				CPathVirtualJunction* pJunction = &(aVirtualJunctions[junctionIndex]);

				//pJunction->m_fGridSpace							= (*j)->GetElement().FindAttributeFloatValue("gridspace", 0);
				pJunction->m_uMaxZ								= (u16)(*j)->GetElement().FindAttributeIntValue("maxZ", 0);
				pJunction->m_iMinX								= (s16)(*j)->GetElement().FindAttributeIntValue("minX", 0);
				pJunction->m_iMinY								= (s16)(*j)->GetElement().FindAttributeIntValue("minY", 0);
				pJunction->m_nHeightBaseWorld					= (u16)(*j)->GetElement().FindAttributeIntValue("baseheight", 0);
				pJunction->m_nStartIndexOfHeightSamples			= (u16)(*j)->GetElement().FindAttributeIntValue("startindexofheightsamples", 0);
				pJunction->m_nXSamples							= (u8)(*j)->GetElement().FindAttributeIntValue("numxsamples", 0);
				pJunction->m_nYSamples							= (u8)(*j)->GetElement().FindAttributeIntValue("numysamples", 0);

				//aVirtualJunctions = rage_new CPathVirtualJunction[NumJunctions];
				//Assertf(aVirtualJunctions, "Alloc failed when loading virtual junctions");
//				pJunction->m_nHeightOffsets = rage_new u8[(pJunction->m_nXSamples * pJunction->m_nYSamples) + 3];
//				Assertf(pJunction->m_nHeightOffsets, "Alloc failed when loading virtual junction height samples");

// 				//get the samples
// 				parTreeNode::ChildNodeIterator k = (*j)->BeginChildren();
// 				
// 				for (; k != (*j)->EndChildren(); ++k)
// 				{
// 					Assert(stricmp((*k)->GetElement().GetName(), "heightsamples") == 0);
// 				
// 					int sampleIndex = 0;
// 					parTreeNode::ChildNodeIterator l = (*k)->BeginChildren();
// 					for (; l != (*k)->EndChildren(); ++l)
// 					{
// 						Assert(stricmp((*l)->GetElement().GetName(), "heightsample") == 0);
// 						pJunction->m_nHeightOffsets[sampleIndex]	= (u8)(*l)->GetElement().FindAttributeIntValue("relativeheight", 0);
// 						++sampleIndex;
// 					}	
// 				}

				++junctionIndex;
			}
		}
		else if (stricmp((*i)->GetElement().GetName(), "heightsamples") == 0)
		{
			u32 iSampleIndex = 0;
			parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
			for(; j != (*i)->EndChildren(); ++j)
			{
				Assert(stricmp((*j)->GetElement().GetName(), "heightsample") == 0);
				Assertf(iSampleIndex < NumHeightSamples, "More height samples than expected encountered.");

				aHeightSamples[iSampleIndex] = (u8)(*j)->GetElement().FindAttributeIntValue("height", 0);
				++iSampleIndex;
			}
		}
		else if (stricmp((*i)->GetElement().GetName(), "junctionmap") == 0)
		{
			//PARSER.LoadObject((*i), JunctionMap);	
			const bool bOldTempHeapSetting = PARSER.Settings().GetFlag(parSettings::USE_TEMPORARY_HEAP);
			PARSER.Settings().SetFlag(parSettings::USE_TEMPORARY_HEAP, true);
			PARSER.LoadFromStructure((*i), *PARSER.GetStructure( &JunctionMap ), JunctionMap.parser_GetPointer(), false);
			PARSER.Settings().SetFlag(parSettings::USE_TEMPORARY_HEAP, bOldTempHeapSetting);
		}
		else
		{
			// An unknown and unsupported DOM child of the root has been encountered.
			Assert(false);
		}
	}

	// Free all the memory created for the DOM tree (and its children).
	sysMemStartTemp();
	delete pTree;
	sysMemEndTemp();

	return true;
}


#ifndef RAGEBUILDER
/////////////////////////////////////////////////////////////////////////////////
// FUNCTION : SaveBinary
// PURPOSE :  Saves the path region information in binary format.
// filename.
/////////////////////////////////////////////////////////////////////////////////
bool CPathRegion::SaveBinary(const char* pFileName)
{
#if 1

	fiStream* file = fiStream::Open(pFileName,false);

	Assertf(file != NULL, "%s:Could not open file", pFileName);		// Must have been able to open file
	Assert(aNodes);		// We need some data here.	

	file->Write((char *) (&NumNodes), 4);
	file->Write((char *) (&NumNodesCarNodes), 4);
	file->Write((char *) (&NumNodesPedNodes), 4);
	file->Write((char *) (&NumLinks), 4);
	file->Write((char *) (&NumJunctions), 4);
	file->Write((char *) (aNodes), NumNodes * sizeof(CPathNode));
	file->Write((char *) (aLinks), (NumLinks) * sizeof(CPathNodeLink));
	file->Write((char *) (aVirtualJunctions), NumJunctions * sizeof(CPathVirtualJunction));
	//file->Write((char *) (JunctionMap), sizeof(JunctionMap));

	file->Close();

#else

	FileHandle	fileId = CFileMgr::OpenFileForWriting(pFileName);

	Assertf(CFileMgr::IsValidFileHandle(fileId), "%s:Could not open file", pFileName);		// Must have been able to open file
	Assert(aNodes);		// We need some data here.

	// Any change here will have to be made one pages down as well (in the load function)
	int written=CFileMgr::Write(fileId, (char *) (&NumNodes), 4);
	Assert(written==4);
	written=CFileMgr::Write(fileId, (char *) (&NumNodesCarNodes), 4);
	Assert(written==4);
	written=CFileMgr::Write(fileId, (char *) (&NumNodesPedNodes), 4);
	Assert(written==4);
	written=CFileMgr::Write(fileId, (char *) (&NumLinks), 4);
	Assert(written==4);
	written=CFileMgr::Write(fileId, (char *) (aNodes), NumNodes * sizeof(CPathNode));
	Assert(written==(int)(NumNodes * sizeof(CPathNode)));
	written=CFileMgr::Write(fileId, (char *) (aLinks), (NumLinks) * sizeof(CPathNodeLink));
	Assert(written==(int)((NumLinks) * sizeof(CPathNodeLink)));

	CFileMgr::CloseFile(fileId);
#endif

	return true;
}
#endif // !RAGEBUILDER
#endif // __DEV


/////////////////////////////////////////////////////////////////////////////////
// FUNCTION : Unload
// PURPOSE :  Unloads the path region from from memory.
/////////////////////////////////////////////////////////////////////////////////
void CPathRegion::Unload(void)
{
	if(aNodes)
	{
		sysMemAllocator::GetMaster().Free(aNodes);
	}
	aNodes = NULL;

	if(aLinks)
	{
		sysMemAllocator::GetMaster().Free(aLinks);
	}
	aLinks = NULL;

	if (aVirtualJunctions)
	{
		sysMemAllocator::GetMaster().Free(aVirtualJunctions);
	}
	aVirtualJunctions = NULL;

	if (aHeightSamples)
	{
		sysMemAllocator::GetMaster().Free(aHeightSamples);
	}
	aHeightSamples = NULL;

	NumNodes = 0;
	NumNodesCarNodes = 0;
	NumNodesPedNodes = 0;
	NumLinks = 0;
	NumJunctions = 0;
	NumHeightSamples = 0;
}

const float CPathVirtualJunction::ms_fRangeUp = 1.5f;
const float CPathVirtualJunction::ms_fGridSpace = 2.0f;

CPathRegion::JunctionMapContainer::JunctionMapContainer(datResource& rsc)
: JunctionMap(rsc, atBinaryMap<s32, u32>::PLACE_NONE)
{

}

#if __DECLARESTRUCT
void CPathRegion::JunctionMapContainer::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(JunctionMapContainer);
	STRUCT_FIELD(JunctionMap);
	STRUCT_END();
}
#endif //__DECLARESTRUCT
