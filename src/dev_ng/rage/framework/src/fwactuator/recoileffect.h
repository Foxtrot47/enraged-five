//
// fwactuator/recoileffect.h : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef FWACTUATOR_RECOILEFFECT_H_
#define FWACTUATOR_RECOILEFFECT_H_

// rage headers.
#include "output/actuatoreffect.h"

namespace rage
{
// PURPOSE: A gun recoil effect for devices that support it.
// NOTES:   Devices that do not support it will use a basic rumble effect.
class fwRecoilEffect : public ioActuatorEffect
{
public:
	// PURPOSE: The controller trigger being used to fire the gun.
	enum Trigger
	{
		// PURPOSE: Represents no trigger is used.
		NONE,

		// PURPOSE: Represents the left trigger.
		LEFT,

		// PURPOSE: REpresents the right trigger.
		RIGHT,
	};

	// PURPOSE: Default constructor.
	// NOTES:   The default constructor does not apply any force to an effect.
	fwRecoilEffect();

	// PURPOSE: Constructor.
	// PARAMS:  duration -  the id of the device to apply the effect to.
	//          trigger - the trigger that is being used to represent the gun trigger.
	//          intensity - the intensity of the recoil effect.
	fwRecoilEffect(u32 duration, Trigger trigger, float intensity, float triggerIntensity);

	// PURPOSE: Destructor.
	virtual ~fwRecoilEffect() {}

	// PURPOSE: Update the force effect.
	// PARAMS:	progress - the progress of the effect. 0.0f is the start and 1.0f is the end.
	//          actuator - the actuator to apply the effect to.
	virtual void Update(float progress, ioAcutatorDevice* actuator) const;

private:
	// PURPOSE: The intensity of the effect.
	float m_Intensity;

	// PURPOSE: The intensity of the trigger effect.
	float m_TriggerIntensity;

	// PURPOSE: The trigger to apply the effect to.
	Trigger m_Trigger;
};

inline fwRecoilEffect::fwRecoilEffect()
	: ioActuatorEffect(0)
	, m_Intensity(0.0f)
	, m_TriggerIntensity(0.0f)
	, m_Trigger(NONE)
{
}

inline fwRecoilEffect::fwRecoilEffect(u32 duration, Trigger trigger, float intensity, float triggerIntensity)
	: ioActuatorEffect(duration)
	, m_Intensity(intensity)
	, m_TriggerIntensity(triggerIntensity)
	, m_Trigger(trigger)
{
	Assertf(intensity >= 0.0f && intensity <= 1.0f, "Invalid intensity in fwRecoilEffect! Check calling code.");
	Assertf(triggerIntensity >= 0.0f && triggerIntensity <= 1.0f, "Invalid trigger intensity in fwRecoilEffect! Check calling code.");
}

}

#endif // FWACTUATOR_RECOILEFFECT_H_
