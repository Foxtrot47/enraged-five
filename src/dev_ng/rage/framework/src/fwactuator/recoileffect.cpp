// fwactuator/recoileffect.cpp : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "recoileffect.h"

// rage headers.
#include "output/actuatordevice.h"
#include "output/rumbleeffect.h"

namespace rage
{
void fwRecoilEffect::Update( float progress, ioAcutatorDevice* actuator ) const
{
	if(actuator != NULL && actuator->SupportsRumble())
	{
		// By default we will fall back to a basic rumble unless the device
		// supports a recoil effect.
		bool basicRumbleFallback = true;

		// If no trigger is given then again we will fall back to a basic
		// rumble.
		if(m_Trigger != NONE)
		{
			// Get the trigger's actuator.
			ioAcutatorDevice::Actuator trigger;
			if(m_Trigger == LEFT)
			{
				trigger = ioAcutatorDevice::LEFT_TRIGGER_RUMBLE;
			}
			else
			{
				trigger = ioAcutatorDevice::RIGHT_TRIGGER_RUMBLE;
			}

			// If the device supports trigger rumble then we can do a recoil
			// effect, if not we will produce a basic rumble.
			if(actuator->HasActuator(trigger))
			{
				// If we are at the start/first call of the recoil affect then
				// rumble the trigger (this should be for one frame).
				if(progress == 0.0f)
				{
					actuator->SetActuator(trigger, m_TriggerIntensity);
				}
				// Leave a gap between the trigger rumble and the motor rumble
				// (between 0.0f to 0.1f). The remainder we will rumble the
				// heavy motor creating a pull/kick effect.
				else if(progress >= 0.1f && progress <= 1.0f)
				{
					actuator->SetActuator(ioAcutatorDevice::HEAVY_RUMBLE, m_Intensity);
				}

				basicRumbleFallback = false;
			}
		}

		// If for some reason we could not create a recoil effect we fall back
		// to a basic rumble.
		if(basicRumbleFallback)
		{
			ioRumbleEffect effect(GetDuration(), m_Intensity);
			effect.Update(progress, actuator);
		}
	}
}
}