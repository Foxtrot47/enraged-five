 

#ifndef ENTITY_ARCHETYPEMANAGER_H
#define ENTITY_ARCHETYPEMANAGER_H

#include "entity/factory.h"
#include "atl/map.h"
#include "atl/pool.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingmodule.h"

#include "entity/extensionlist.h"

namespace rage {

class bkBank;
class fwArchetype;
class fwArchetypeDef;
class fwModelId;
class fwExtension;

// This is an identifier for a factory. It is either an AF_ type defined here, or
// something defined by the game code. It is used to identify a fwArchetypeFactory.
typedef int fwArchetypeFactoryId;

typedef fwFactoryBase< fwArchetype >			fwArchetypeFactoryBase;
typedef fwFactoryBase< fwExtension >			fwArchetypeExtensionFactoryBase;

template <class T>
class fwArchetypeFactory : public fwFactoryWithDynamicStore< T, fwArchetype >
{ };

template <class T>
class fwArchetypeExtensionFactory : public fwFactoryWithDynamicStore< T, fwExtension >
{ };

// RAGE Archetype factory IDs, to be used for fwArchetypeFactoryId.
// Be sure to use AF_TYPE_USER + 0 and beyond for types defined in the game code.
enum {
	// System factories can go in here at this point
	// ...

	// This value needs to be added to any factory ID that is defined in the game code level.
	AF_TYPE_USER
};


/** PURPOSE: This is a subclass of fwArchetypeFactory that is meant for use with
 *  types of T that can contain member functions.
 */
// ToDo: move these methods down into factory & this layer of template can be completely removed.
template<class T>
class fwArchetypeDynamicFactory : public fwArchetypeFactory<T> {
public:
	// PURPOSE: Call a member function on every element that has been created
	// via Create() or GetNextItem().
	void ForAllItemsUsed(void (* func )(T*));
	void ForAllItemsUsed(void (T::*func)());

	void GatherPtrs(atArray<T*>& targetArray);
private:
};


class fwModelId {
public:
	enum{
		TF_INVALID = 0xfff,				// type file is invalid
		MI_INVALID = 0xffff,			// model index is invalid
	};

	fwModelId(void) { s.m_modelIndex = MI_INVALID; s.m_mapTypeDefIndex = TF_INVALID; s.m_bIsStreamed = false; s.m_bIsMemLocked =false; s.m_bPad1 = 0; s.m_bPad2 = 0;}
	fwModelId(const fwModelId& other) { s.m_modelIndex = other.GetModelIndex(); s.m_mapTypeDefIndex = other.s.m_mapTypeDefIndex; s.m_bIsMemLocked = false; s.m_bIsStreamed = other.s.m_bIsStreamed; s.m_bPad1 = 0; s.m_bPad2 = 0; }

	explicit fwModelId(strLocalIndex modelIdx) { FastAssert((modelIdx.Get() & 0xffff0000) == 0); s.m_modelIndex = modelIdx.Get(); s.m_bIsStreamed = false; s.m_bIsMemLocked = false; s.m_mapTypeDefIndex = TF_INVALID; s.m_bPad1 = 0; s.m_bPad2 = 0;}

	explicit fwModelId(strLocalIndex modelIdx, s32 mapTypeDefIndex, bool bIsStreamed) { { FastAssert((modelIdx.Get() & 0xffff0000) == 0); s.m_modelIndex = modelIdx.Get(); s.m_bIsStreamed = bIsStreamed; s.m_bIsMemLocked = false; s.m_mapTypeDefIndex = mapTypeDefIndex; }}


	template <typename T> explicit fwModelId(T) {  T::__Not_A_Valid_Function(); }			// trap for unsupported types. catch at compile time.

	~fwModelId() {}

	//bool operator == (const fwModelId &other) const { return( s.m_modelIndex == other.s.m_modelIndex); }
	bool operator == (const fwModelId &other) const { return( GetModelIndex() == other.GetModelIndex()); }
	fwModelId& operator=( const fwModelId& other)
	{
		s.m_modelIndex = other.GetModelIndex();
		s.m_mapTypeDefIndex = other.s.m_mapTypeDefIndex;
		s.m_bIsMemLocked = false;
		s.m_bIsStreamed = other.s.m_bIsStreamed;
		s.m_bPad1 = 0;
		s.m_bPad2 = 0;
		return *this;
	}

	inline void SetModelIndex(u32 val) { FastAssert((val & 0xffff0000) == 0); s.m_modelIndex = val; s.m_mapTypeDefIndex = TF_INVALID; s.m_bIsStreamed = false;}
	//inline u32 GetModelIndex(void) const {  return s.m_modelIndex; }
	u32 GetModelIndex(void) const;
	void MemLock(void);

	inline strLocalIndex GetMapTypeDefIndex(void) const { return strLocalIndex(static_cast<s32>(s.m_mapTypeDefIndex)); }

	// streaming index != model index!!!!!!!!   convert the fwModelId back and forward to a useful streaming index (preserves modelindex part _only_)
	inline void ConvertFromStreamingIndex(s32 SI) { s.m_modelIndex = (u16)( (SI==-1) ? MI_INVALID : SI ) ;}
	inline strLocalIndex ConvertToStreamingIndex(void) const { return( (s.m_modelIndex==MI_INVALID) ? strLocalIndex(-1) :strLocalIndex(s. m_modelIndex)); }

	inline u32 ConvertToU32 (void) { return(m_asU32); }
	static fwModelId	ConvertTofwModelId(u32 val) {fwModelId retVal; retVal.m_asU32 = val; return(retVal); }

	// 	void Set(s32 UNUSED_PARAM(val), u32 UNUSED_PARAM(fileIndex)) { FastAssert(false); }
	// 	void Set(s16 UNUSED_PARAM(val), u32 UNUSED_PARAM(fileIndex)) { FastAssert(false); }
	// 	void Set(u16 UNUSED_PARAM(val), u32 UNUSED_PARAM(fileIndex)) { FastAssert(false); }

	inline bool IsValid(void) const { return (s.m_modelIndex != MI_INVALID); }
	inline bool IsGlobal(void) const { return (s.m_bIsStreamed == false); }

	inline void Invalidate(void) { s.m_modelIndex = MI_INVALID; }

#if __ASSERT
	const char* GetTypeFileName() const;
#endif //__ASSERT

private:

	union
	{
		struct{
			u32		m_modelIndex	: 16;
			u32		m_mapTypeDefIndex		: 12;
			u32		m_bIsStreamed	: 1;
			u32		m_bIsMemLocked	: 1;
			u32		m_bPad1			: 1;
			u32		m_bPad2			: 1;
		} s;
		u32		m_asU32;
	};
};

CompileTimeAssert(sizeof(fwModelId) == 4);

template <>
struct atMapHashFn<fwModelId>
{
	unsigned operator ()(fwModelId key) const { return *((u32 *) &key); }
};

typedef atIteratablePool<fwArchetype*> fwArchetypePool;
typedef atSimplePooledMapType<u32, fwModelId, sizeof(void*)> fwArchetypeMap;

/*
 *  This is how an actual archetype factory should be implemented:
 *
 *  class DogPoopFactory : public fwArchetypeSimpleFactory<DogPoop> {
 *    public:
 *      enum { AF_TYPE_DOGPOOP };
 *  };
 */

/** PURPOSE: This is the main manager class for the archetype system.
 *  It will manage all factories as well as accessors to every single archetype itself.
 *
 *  First, the game should create fwArchetypeFactoryBase-derived classes and register them
 *  through RegisterArchetypeFactory.
 */
class fwArchetypeManager
{
public:

	enum {
		NUM_STREAMED_SLOTS		= 2000,				// first N slots in manager are streamed (interior only)
		NUM_STREAMED_SLOTS_FULL	= 35000,			// full .imap dependencies streamed
		INVALID_STREAM_SLOT		= 0xffff
	};

	/** PURPOSE: Initialize the archetype manager system.
	 *
	 * PARAMS:
	 *  maxFactoryCount - The highest possible ID for a factory.
	 *  archetypeMapSize - The number of slots to reserve for the archetype info lookup map.
	 */
	static void Init(int maxFactoryId/*, int archetypeMapSize*/);

	// PURPOSE: Reset the manager - this will currently just reset the archetype info map.
	static void Reset();

	// PURPOSE: Shut down the manager.
	static void Shutdown();

	static u16 GetNumStreamedSlots();

	// PURPOSE: Look up an archetype by its index.
	// want to hide these...
	static inline fwArchetype* GetArchetype(u32 index);
	static inline fwArchetype* GetStreamedArchetype(u32 index);

	// want access only through this...
	static fwArchetype* GetArchetype(fwModelId modelId) { return(GetArchetype(modelId.GetModelIndex())); }

	static const fwModelId&	LookupModelId(const fwArchetype*);

	// PURPOSE: Look up an archetype by its name.
	static fwArchetype* GetArchetype(const strStreamingObjectName name, u32* pIndex);
	static fwArchetype* GetArchetype(const strStreamingObjectName name, fwModelId* pModelId);

	// PURPOSE: Look up an archetype by its hash key.
	static fwArchetype* GetArchetypeFromHashKey(u32 key, u32* pIndex);
	static fwArchetype* GetArchetypeFromHashKey(u32 key, fwModelId& pModelId);

	// PURPOSE: Look up an archetype's index by its name.
	static bool GetArchetypeExistsForHashKey(u32 key);
	static u32 GetArchetypeIndex(const strStreamingObjectName name);
	static u32 GetArchetypeIndexFromHashKey(u32 key);

	// register a new archetype
	static u16 RegisterPermanentArchetype(fwArchetype* pArchetype, s32 mapTypeDefIdx, bool bMemlock);
	static void LockPermanentArchetypes()				{ ms_bArchetypesLocked = true; }

	static u16 RegisterStreamedArchetype(fwArchetype* pArchetype, s32 mapTypeDefIdx);
	static void UnregisterStreamedArchetype(fwArchetype* pArchetype);
	static bool IsArchetypePermanentDLC(fwArchetype* pArchetype);

	static s32 GetStreamingModuleId()					{ return ms_streamingId; }
	static void SetStreamingModuleId(s32 streamingId)	{ ms_streamingId = streamingId; }
	static strStreamingModule* GetStreamingModule();

	static void FreeArchetypes(s32 mapTypeDefIndex);

	static void ForAllArchetypesInFile(s32 mapTypeDefIndex, void callBack(fwArchetype*) );

	static unsigned int GetMaxArchetypeIndex()				{ ASSERT_ONLY(ms_MaxModelInfosHasBeenUsed = true); return ms_MaxArchetypes; } // accounts for the streamed archetypes being in their own pool
	static unsigned int GetStartArchetypeIndex()		{ return 0; }
	static void SetMaxArchetypes(unsigned int num);

	static u32 GetCurrentNumRegisteredArchetypes(void)			{ return((u32)ms_ArchetypePool.GetNoOfUsedSpaces()); }

	static void AddArchetypeToTypeFileRef(fwArchetype *pArchetype);
	static void ScheduleRemoveArchetypeToTypeFileRef(fwArchetype *pArchetype);

#if __BANK
	static fwArchetypePool& GetPool() {return ms_ArchetypePool;}
	static fwArchetypeMap& GetArchetypeMap() {return ms_ArchetypeMap;}
	static void GetFullArchetypeName(char* pName, fwModelId id);
	static void GetArchetypeFileName(char* pName, fwModelId id);

	static void AddWidgets(bkBank &bank);
	static void DebugDraw();
#endif // __BANK

#if !__NO_OUTPUT
	static void PoolFullCallback();
	static void DumpDynamicArchetypesComponents();
	static const char* GetArchetypeName(u32 index);
#endif	//!__FINAL

#if __ASSERT
	static bool VerifyNameClashes(const char* name);
#endif	//__ASSERT
	
	static void PostLoad() {}

	static void PrintArchetypeStoreUsage();
	static void ClearArchetypePtrStore();

	static fwFactoryBase<fwArchetype>*	GetArchetypeFactory(u32 idx) { return(ms_archetypeFactoryMgr.GetFactory(idx)); }
	static void							RegisterArchetypeFactory(const int factoryId, fwFactoryBase<fwArchetype>& factory) { ms_archetypeFactoryMgr.RegisterFactory(factoryId, factory); }
	
	static fwFactoryBase<fwExtension>*	Get2dEffectFactory(u32 idx) { return(ms_2dEffectFactoryMgr.GetFactory(idx)); }
	static void							Register2dEffectFactory(const int factoryId, fwFactoryBase<fwExtension>& factory) { ms_2dEffectFactoryMgr.RegisterFactory(factoryId, factory); }

private:
	static fwArchetypePool ms_ArchetypePool;
	static fwArchetypeMap ms_ArchetypeMap;
	static s32 ms_streamingId;

	// PURPOSE:	Number of elements allocated for ms_ArchetypePtrs (or the number that will
	//			be allocated, if used before Init()).
	// NOTES:	Replaces the old NUM_MODEL_INFOS #define. Users can change this by calling
	//			SetMaxArchetypes(), prior to any call to GetMaxModelInfos() or Init().
	//			Intended to be set to fwConfig::m_ArchMaxModelInfos.
	static u16 ms_MaxArchetypes;
	static bool ms_bArchetypesLocked;

	static fwFactoryManager< fwArchetype >				ms_archetypeFactoryMgr;
	static fwFactoryManager< fwExtension >				ms_2dEffectFactoryMgr;

#if __ASSERT
	static bool ms_MaxModelInfosHasBeenUsed;
#endif
};

template<typename T>
inline void fwArchetypeDynamicFactory<T>::ForAllItemsUsed(void (T::*func)()) {

	Assert((func != NULL));

	typedef atMap< s32, atArray<T> * > mapArray;
	mapArray& store = fwArchetypeDynamicFactory<T>::m_perFileStores;
	for(u32 i=0; i< store.GetNumSlots(); i++){
		atMapEntry< s32, atArray<T> *> *pEntry = store.GetEntry(i);

		while (pEntry){
			atArray< T >* pData = pEntry->data;
			if (pData){
				for(u32 i=0; i<pData->GetCount(); i++){
					T* pInst = &((*pData)[i]);
					if (pInst){
						(pInst->*func)();
					}
				}
			}
			pEntry = pEntry->next;
		}
	}
}


template<class T>
inline void fwArchetypeDynamicFactory<T>::ForAllItemsUsed(void (*func)(T*)) {

	Assert(func);

	typedef atMap< s32, atArray<T> * > mapArray;
	mapArray& store = fwArchetypeDynamicFactory<T>::m_perFileStores;
	for(u32 i=0; i< store.GetNumSlots(); i++){
		atMapEntry< s32, atArray<T> *> *pEntry = store.GetEntry(i);

		while (pEntry){
			atArray< T >* pData = pEntry->data;
			if (pData){
				for(u32 i=0; i<pData->GetCount(); i++){
					T* pElement = &((*pData)[i]);
					if (pElement){
						func(pElement);
					}
				}
			}
			pEntry = pEntry->next;
		}
	}
}

template<class T>
inline void fwArchetypeDynamicFactory<T>::GatherPtrs(atArray<T*>& targetArray) {

	targetArray.Reset();

	typedef atMap< s32, atArray<T> * > mapArray;
	mapArray& store = fwArchetypeDynamicFactory<T>::m_perFileStores;
	for(u32 i=0; i< store.GetNumSlots(); i++){
		atMapEntry< s32, atArray<T> *> *pEntry = store.GetEntry(i);

		while (pEntry){
			atArray< T >* pData = pEntry->data;
			if (pData){
				for(u32 j=0; j<pData->GetCount(); j++){
					T* pElement = &((*pData)[j]);
					if (pElement){
						targetArray.PushAndGrow(pElement);
					}
				}
			}
			pEntry = pEntry->next;
		}
	}
}

__forceinline fwArchetype* fwArchetypeManager::GetArchetype(u32 index) 
{
	// The pool returns **fwArchetype, which will be NULL if that index is not being used
	FastAssert(index != fwModelId::MI_INVALID && index < (u32) ms_ArchetypePool.GetSize());
	fwArchetype** pArchetype = ms_ArchetypePool.GetSlot(index);
	return pArchetype ? *pArchetype : NULL;
}

__forceinline strStreamingModule* fwArchetypeManager::GetStreamingModule()
{
	// this is a temporary method for this as eventually the Archetype will contain the module definition
	return strStreamingEngine::GetInfo().GetModuleMgr().GetModule(ms_streamingId);
}



} // namespace rage

#endif // ENTITY_ARCHETYPEMANAGER_H
