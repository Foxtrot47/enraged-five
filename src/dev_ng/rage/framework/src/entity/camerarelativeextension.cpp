
#include "entity/camerarelativeextension.h"

#include "entity/archetype.h"
#include "entity/entity.h"

#if __PPU
#include <sn/libsntuner.h>
#endif // __PU

namespace rage {

	AUTOID_IMPL(fwCameraRelativeExtension);

	atFixedArray<fwCameraRelativeExtension, fwCameraRelativeExtension::MAX_CAMERA_RELATIVE_EXTENSIONS> ms_CameraRelativeExtensions;

	fwCameraRelativeExtension* fwCameraRelativeExtension::GetOrAddExtension(fwEntity &entity)
	{
		Assert(sysThreadType::IsUpdateThread());

		// Does this entity already have a cam relative extension?
		fwCameraRelativeExtension *extension = entity.GetExtension<fwCameraRelativeExtension>();

		if (!extension)
		{
			// Nope - need to create one first.
			extension = &ms_CameraRelativeExtensions.Append();
			extension->m_entity = &entity;
			extension->m_cameraOffset = Mat34V(V_IDENTITY);
			entity.GetExtensionList().Add(*extension);
		}

		return extension;
	}

	void fwCameraRelativeExtension::RemoveExtension(fwEntity &entity)
	{
		Assert(sysThreadType::IsUpdateThread());

		fwCameraRelativeExtension *extension = entity.GetExtension<fwCameraRelativeExtension>();

		if (extension)
		{
			// If there's no reason for this extension to exist anymore,
			// kill it.
			entity.GetExtensionList().Unlink(extension);

			int index = ptrdiff_t_to_int(extension - ms_CameraRelativeExtensions.GetElements());

			Assert(&ms_CameraRelativeExtensions[index] == extension);

			// Keep a pointer to the last extension around, we might need it for relinking later.
			fwCameraRelativeExtension *lastExtension = &ms_CameraRelativeExtensions[ms_CameraRelativeExtensions.GetCount()-1];

			ms_CameraRelativeExtensions.DeleteFast(index);

			// If we just moved an extension around, we need to relink it.
			// The only exception is if we just deleted the last element in the list, in which case
			// nothing was shifted.
			if (ms_CameraRelativeExtensions.GetCount() > (int) index)
			{
				fwEntity *relinkedEntity = ms_CameraRelativeExtensions[index].m_entity;
				FastAssert(relinkedEntity);

				relinkedEntity->GetExtensionList().Unlink(lastExtension);
				relinkedEntity->GetExtensionList().Add(ms_CameraRelativeExtensions[index]);

				// Note that DeleteFast() already moved the extension around, so link from the
				// extension to the entity is correct. But hey, let's assert on that.
				Assert(ms_CameraRelativeExtensions[index].m_entity == relinkedEntity);
			}
		}
	}

} // namespace rage
