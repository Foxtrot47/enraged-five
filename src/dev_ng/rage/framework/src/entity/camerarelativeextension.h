
#ifndef CAMERA_RELATIVE_EXTENSION_H_
#define CAMERA_RELATIVE_EXTENSION_H_

#include "atl/array.h"
#include "entity/extensionlist.h"
#include "fwutil/PtrList.h"

#include "vectormath\mat34v.h"

namespace rage {

	class bkBank;
	class fwEntity;

	class fwCameraRelativeExtension : public fwExtension
	{
	public:
#if !__SPU
		EXTENSIONID_DECL( fwCameraRelativeExtension, fwExtension );
#endif // !__SPU

		fwEntity*   m_entity;
		Mat34V		m_cameraOffset;

		enum 
		{
			MAX_CAMERA_RELATIVE_EXTENSIONS = 3
		};

		static fwCameraRelativeExtension* GetOrAddExtension(fwEntity &entity);
		static void RemoveExtension(fwEntity &entity);
	};

} // namespace rage

#endif // CAMERA_RELATIVE_EXTENSION_H_
