
#ifndef ENTITY_DYNAMICENTITYCOMPONENT_H
#define ENTITY_DYNAMICENTITYCOMPONENT_H

#include "fwanimation/animdefines.h"
#include "fwtl/pool.h"
#include "fwtl/regdrefs.h"
#include "system/smallocator.h"
#include "vector/quaternion.h"
#include "vector/vector3.h"
#include "entity/attachmententityextension.h"

namespace rage {

	class crCreature;
	class crSkeleton;
	class fwAnimDirector;
	class fwAnimFrameRagDoll;
	class fwEntity;
	class fwEntityDesc;
	class grbTargetManager;
	class phInst;
	class phCollider;



	enum eNOCollisionFlags
	{
		NO_COLLISION_HIT_SHOULD_FIND_IMPACTS	= BIT(0),

		NO_COLLISION_NETWORK_OBJECTS			= BIT(1),
		NO_COLLISION_PERMENANT					= BIT(2),
		NO_COLLISION_RESET_WHEN_NO_BBOX			= BIT(3),
		NO_COLLISION_RESET_WHEN_NO_IMPACTS		= BIT(4),
		NO_COLLISION_NEEDS_RESET				= BIT(5)
		// only using a u8 so this is last bit available
	};


class fwDynamicEntityComponent
{
public:
	FW_REGISTER_CLASS_POOL(fwDynamicEntityComponent);

	fwDynamicEntityComponent();
	~fwDynamicEntityComponent();

	inline Vector3 GetAnimatedVelocity() const									{ return m_vAnimatedVelocity; }
	inline void SetAnimatedVelocity(Vector3::Vector3Param vAnimatedVelocity);

	inline crSkeleton *GetSkeleton() const					{ return m_pSkeleton; }
	inline void SetSkeleton(crSkeleton *skeleton)			
	{ 
		Assert( !skeleton || !m_pSkeleton );
		m_pSkeleton = skeleton; 
	}

	inline Vector3 GetPreviousPosition() const						{ return m_vecPrevPos; }
	inline void SetPreviousPosition(Vector3::Vector3Param prevPos)	{ m_vecPrevPos = prevPos; }

	inline void SetEntityDesc(fwEntityDesc* entityDesc)		{ m_pEntityDesc = entityDesc; }
	inline fwEntityDesc* GetEntityDesc() const				{ return m_pEntityDesc; }

	// ------ ANIMATION -------
	void CreateAnimDirector(fwEntity &entity, bool withRagDollComponent, bool withFacialRigComponent);
	fwAnimDirector* GetAnimDirector() const					{ return m_pAnimDirector; }
	void DeleteAnimDirector();


	inline float GetAnimatedAngularVelocity() const			{ return m_fAnimatedAngularVelocity; }
	inline void SetAnimatedAngularVelocity(float velocity)	{ m_fAnimatedAngularVelocity = velocity; }

	inline float GetForceAddToBoundRadius() const			{ return m_fForceAddToBoundRadius; }
	inline void SetForceAddToBoundRadius(float forceAdd)	{ m_fForceAddToBoundRadius = forceAdd; }

	inline crCreature *GetCreature() const					{ return m_pCreature; }
	inline void SetCreature(crCreature *creature)			{ m_pCreature = creature; }

	inline grbTargetManager *GetTargetManager() const		{ return m_pTargetManager; }
	inline void SetTargetManager(grbTargetManager *targetMgr)	{ m_pTargetManager = targetMgr; }


	// ------ COLLISION --------

	inline void SetNoCollision(fwEntity* pEntity, u8 nTypeFlags);
	inline void PrepareNoCollisionReset();
	inline void ResetNoCollision();
	inline bool TestNoCollision(const phInst *pOtherInst, bool bPreApplyImpacts);

	inline int GetNoCollisionFlags() const					{ return m_nNoCollisionFlags; }
	inline void SetNoCollisionFlags(u8 flags)				{ m_nNoCollisionFlags = flags; }

	inline const fwEntity* GetNoCollisionEntity() const		{ return m_pNoCollisionEntity; }
	inline void SetNoCollisionEntity(fwEntity* pEntity)		{ m_pNoCollisionEntity = pEntity; }

	fwAttachmentEntityExtension *CreateAttachmentExtensionIfMissing(fwEntity* thisEntity);
	void DeleteAttachmentExtension();
	inline fwAttachmentEntityExtension* GetAttachmentExtension() { return m_pAttachmentEntityExtension; }
	inline const fwAttachmentEntityExtension* GetAttachmentExtension() const { return m_pAttachmentEntityExtension; }

	void SetCollider(phCollider* pCollider) { m_pCollider = pCollider; }
	inline phCollider* GetCollider() { return m_pCollider; }
	inline const phCollider* GetCollider() const { return m_pCollider; }

protected:
	Vector3 m_vAnimatedVelocity;
	Vector3 m_vecPrevPos;

	fwEntityDesc* m_pEntityDesc;
	crSkeleton* m_pSkeleton;
	crCreature* m_pCreature;
	grbTargetManager* m_pTargetManager;
	fwAnimDirector* m_pAnimDirector;
	fwAttachmentEntityExtension* m_pAttachmentEntityExtension;
	phCollider* m_pCollider;

	fwRegdRef<class fwEntity> m_pNoCollisionEntity;

	float	m_fAnimatedAngularVelocity;					// Angular velocity extracted from the animation
	float	m_fForceAddToBoundRadius;

	u8 m_nNoCollisionFlags;

	// 7 padding bytes

public:
#if __BANK
	static int sm_DynCompCount;
#endif // __BANK
};

#if __DEV
template<> void fwPool<fwDynamicEntityComponent>::PoolFullCallback();
#endif

inline void fwDynamicEntityComponent::SetAnimatedVelocity(Vector3::Vector3Param vAnimatedVelocity)
{
	Assert((Vector3) vAnimatedVelocity == (Vector3) vAnimatedVelocity);
	m_vAnimatedVelocity = vAnimatedVelocity;
}

inline void fwDynamicEntityComponent::SetNoCollision(fwEntity* pEntity, u8 nTypeFlags)
{
	m_pNoCollisionEntity = pEntity;
	m_nNoCollisionFlags = (u8)(nTypeFlags | NO_COLLISION_HIT_SHOULD_FIND_IMPACTS);
}

inline void fwDynamicEntityComponent::PrepareNoCollisionReset()
{
	// if didn't get reset last frame do it here
	if((m_nNoCollisionFlags &NO_COLLISION_NEEDS_RESET) && !(m_nNoCollisionFlags &NO_COLLISION_NETWORK_OBJECTS))
		ResetNoCollision();

	// set flag to ensure reset gets called next frame
	m_nNoCollisionFlags |= NO_COLLISION_NEEDS_RESET;
}

inline void fwDynamicEntityComponent::ResetNoCollision()
{
	if(!(m_nNoCollisionFlags & NO_COLLISION_HIT_SHOULD_FIND_IMPACTS))
	{
		if ((m_nNoCollisionFlags & NO_COLLISION_RESET_WHEN_NO_BBOX) || (m_nNoCollisionFlags & NO_COLLISION_RESET_WHEN_NO_IMPACTS))
		{
			m_pNoCollisionEntity = NULL;
			m_nNoCollisionFlags = 0;
		}
	}

	// clear those two flags, plus the needs reset flag
	m_nNoCollisionFlags &= ~(NO_COLLISION_HIT_SHOULD_FIND_IMPACTS|NO_COLLISION_NEEDS_RESET);
}





#if !__SPU && !__64BIT
// Once we break this limit, we'll have to use a pool.
// Right now, we're depending on the smallocator to keep the overhead for
// construction and destruction manageable.
CompileTimeAssert( sizeof(fwDynamicEntityComponent) <= sysSmallocator::MAX_SMALLOCATOR_SIZE );
#endif

} // namespace rage

#endif // ENTITY_DYNAMICENTITYCOMPONENT_H
