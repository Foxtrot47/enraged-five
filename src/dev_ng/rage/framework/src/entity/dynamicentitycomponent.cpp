
#include "dynamicentitycomponent.h"

#include "fwanimation/animdirector.h"

namespace rage {

#if __BANK
int fwDynamicEntityComponent::sm_DynCompCount = 0;
#endif // __BANK


FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwDynamicEntityComponent, 500, 0.15f, atHashString("fwDynamicEntityComponent",0x94e57350));



fwDynamicEntityComponent::fwDynamicEntityComponent()
: m_vAnimatedVelocity(Vector3::ZeroType)
, m_vecPrevPos(Vector3::ZeroType)
, m_pEntityDesc(NULL)
, m_pSkeleton(NULL)
, m_pCreature(NULL)
, m_pTargetManager(NULL)
, m_pAnimDirector(NULL)
, m_fAnimatedAngularVelocity(0.0f)
, m_fForceAddToBoundRadius(0.0f)
, m_pNoCollisionEntity(NULL)
, m_nNoCollisionFlags(0)
, m_pAttachmentEntityExtension(NULL)
, m_pCollider(NULL)
{
#if __BANK
	sm_DynCompCount++;
#endif // __BANK
}

fwDynamicEntityComponent::~fwDynamicEntityComponent()
{
#if __BANK
	sm_DynCompCount--;
#endif // __BANK

	if(m_pNoCollisionEntity)
	{
		m_pNoCollisionEntity = NULL;
	}

	DeleteAnimDirector();
}

//
// name:		CDynamicEntity::CreateAnimDirector
// description:	Create an animation director for this entity
void fwDynamicEntityComponent::CreateAnimDirector(fwEntity &entity, bool withRagDollComponent, bool withFacialRigComponent)
{
	Assertf(entity.GetSkeleton(), "Skeleton ptr is null");

	if (m_pAnimDirector)
	{
		DeleteAnimDirector();
	}
	m_pAnimDirector = rage_new fwAnimDirectorPooledObject(entity);
	m_pAnimDirector->Init(withRagDollComponent, withFacialRigComponent);

	Assertf(m_pAnimDirector, "Failed to create AnimDirector");
}


//
// name:		CDynamicEntity::DeleteAnimDirector
// description:	Delete this entity's animation director
void fwDynamicEntityComponent::DeleteAnimDirector()
{
	delete m_pAnimDirector;
	m_pAnimDirector = NULL;
}

fwAttachmentEntityExtension *fwDynamicEntityComponent::CreateAttachmentExtensionIfMissing(fwEntity* thisEntity)
{
	if(!m_pAttachmentEntityExtension)
	{
		m_pAttachmentEntityExtension = rage_new fwAttachmentEntityExtension(thisEntity);
		Assertf(m_pAttachmentEntityExtension, "Fatal error: could not allocate a fwAttachmentEntityExtension because the pool is full");
	}

	return m_pAttachmentEntityExtension;
}

void fwDynamicEntityComponent::DeleteAttachmentExtension()
{ 
	Assert(m_pAttachmentEntityExtension);
	delete m_pAttachmentEntityExtension; 
	m_pAttachmentEntityExtension = NULL; 
}

#if __DEV

typedef struct  
{
	atString	entName;
	int			count;
}	fwDynamicEntityComponentEntityNameAndCount;


int fwDynamicEntityComponentList_SortByCount(const fwDynamicEntityComponentEntityNameAndCount *a, const fwDynamicEntityComponentEntityNameAndCount *b)
{
	if( a->count < b->count )
		return 1;
	else if( a->count > b->count )
		return -1;
	return 0;
}


int	fwDynamicEntityComponentFindEntityByNameInList(const char *pName, atArray<fwDynamicEntityComponentEntityNameAndCount> &theArray)
{
	u32	hash = atStringHash(pName);

	int	count = theArray.size();
	for(int i=0;i<count;i++)
	{	
		fwDynamicEntityComponentEntityNameAndCount &theElement = theArray[i];

		u32	elementHash = atStringHash(theElement.entName.c_str());
		if( elementHash == hash )
		{
			return i;
		}
	}
	return -1;
}

template<> void fwPool<fwDynamicEntityComponent>::PoolFullCallback() 
{
	Displayf("Tallied Contents of fwPool<fwDynamicEntityComponent>");
	s32 size = GetSize();
	int iIndex = 0;

	atArray<fwDynamicEntityComponentEntityNameAndCount> nameAndCountArray;
	while(size--)
	{
		fwDynamicEntityComponent* pComponent = GetSlot(size);
		if(pComponent)
		{
			fwEntityDesc *pDesc = pComponent->GetEntityDesc();

			if(pDesc)
			{
				int	listIndex = fwDynamicEntityComponentFindEntityByNameInList(pDesc->GetEntity()->GetModelName(), nameAndCountArray);
				if( listIndex == -1 )
				{
					fwDynamicEntityComponentEntityNameAndCount	newElement;
					newElement.entName = pDesc->GetEntity()->GetModelName();
					newElement.count = 1;
					nameAndCountArray.PushAndGrow(newElement);
				}
				else
				{
					nameAndCountArray[listIndex].count++;
				}
			}
		}
		iIndex++;
	}

	nameAndCountArray.QSort(0, -1, fwDynamicEntityComponentList_SortByCount);

	int	totalCount = 0;
	for(int i=0;i<nameAndCountArray.size();i++)
	{
		int count = nameAndCountArray[i].count;
		totalCount += count;
		Displayf("(ENTITY)%30s x %d", nameAndCountArray[i].entName.c_str(), count);
	}
	Displayf("Total Valid Slots Used: %d", totalCount);

}
#endif


} // namespace rage

