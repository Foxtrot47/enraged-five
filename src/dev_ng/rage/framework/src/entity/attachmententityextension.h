
#ifndef ENTITY_ATTACHMENTENTITYEXTENSION_H
#define ENTITY_ATTACHMENTENTITYEXTENSION_H

#include "fwtl/regdrefs.h"
#include "system/smallocator.h"
#include "vector/quaternion.h"
#include "vector/vector3.h"
#include "vectormath/mat34v.h"

#include "physics\constrainthandle.h"
#include "atl\queue.h"

namespace rage {

#define ATTACHMENT_MAX_CONSTRAINTS (4)
#define ATTACHMENT_ENABLE_UPDATE_RECORDS (0 && __BANK)
#define ATTACHMENT_NUM_UPDATE_RECORDS (8)

#if ATTACHMENT_ENABLE_UPDATE_RECORDS
#define ATTACHMENT_ENABLE_UPDATE_RECORDS_PARAM(X) X
#else
#define ATTACHMENT_ENABLE_UPDATE_RECORDS_PARAM(X)
#endif

	class crCreature;
	class crSkeleton;
	class fwAnimDirector;
	class fwAnimFrameRagDoll;
	class fwEntity;
	class grbTargetManager;
	class phInst;
	class phConstraintBase;



	//Update GetAttachFlagString and GetAttachFlagString and IsAttachStateBasicDerived if changing this enum
	enum eAttachFlags
	{
		ATTACH_STATE_NONE		= 0,

		//Attachment has been broken, but the objects won't be allowed to collide by ShouldFindImpacts
		ATTACH_STATE_DETACHING,

		//General purpose attachment types
		ATTACH_STATE_BASIC,
		ATTACH_STATE_PHYSICAL,
		ATTACH_STATE_WORLD,
		ATTACH_STATE_WORLD_PHYSICAL,

		//These special case attachment types extend the functionality of the general attachment types
		ATTACH_STATE_PED,				//ATTACH_STATE_BASIC or ATTACH_STATE_WORLD
		ATTACH_STATE_PED_WITH_ROT,		//ATTACH_STATE_BASIC or ATTACH_STATE_WORLD
		ATTACH_STATE_RAGDOLL,			//ATTACH_STATE_PHYSICAL
		ATTACH_STATE_PED_ENTER_CAR,		//ATTACH_STATE_BASIC
		ATTACH_STATE_PED_IN_CAR,		//ATTACH_STATE_BASIC
		ATTACH_STATE_PED_EXIT_CAR,		//ATTACH_STATE_BASIC
		ATTACH_STATE_PED_ON_MOUNT,		//ATTACH_STATE_BASIC
		ATTACH_STATE_PED_ON_GROUND,		//ATTACH_STATE_BASIC

		ATTACH_STATES			= 15,	// mask for states

		// ** EXTERNAL ATTACH FLAGS **
		ATTACH_FLAG_POS_CONSTRAINT				= BIT(4),
		ATTACH_FLAG_ROT_CONSTRAINT				= BIT(5),
		ATTACH_FLAG_DO_PAIRED_COLL				= BIT(6),
		ATTACH_FLAG_INITIAL_WARP				= BIT(7),
		ATTACH_FLAG_DONT_ASSERT_ON_NULL_ENTITY	= BIT(8),
		ATTACH_FLAG_AUTODETACH_ON_DEATH			= BIT(9),
		ATTACH_FLAG_AUTODETACH_ON_RAGDOLL		= BIT(10),
		ATTACH_FLAG_COL_ON						= BIT(11),	// turn collision on even for a non-physical attachment
		ATTACH_FLAG_DELETE_WITH_PARENT			= BIT(12),	// Delete this physical if the parent gets deleted
		ATTACH_FLAG_DONT_NETWORK_SYNC			= BIT(13),	// Will prevent network sync of attachment on peds
		ATTACH_FLAG_ACTIVATE_ON_DETACH			= BIT(14),
		ATTACH_FLAG_OFFSET_IS_ABSOLUTE			= BIT(15),
		ATTACH_FLAG_AUTODETACH_ON_TELEPORT		= BIT(16),

		// ***** REMEMBER TO UPDATE NUM_EXTERNAL_ATTACH_FLAGS IF ANY MORE FLAGS ARE ADDED!!! ****

		NUM_EXTERNAL_ATTACH_FLAGS				= 17,

		// ** INTERNAL ATTACH FLAGS **
		ATTACH_FLAG_ATTACHED_TO_WORLD_BASIC		= BIT(17),
		ATTACH_FLAG_NO_PHYSICS_INST_YET			= BIT(18),	// Will get set if phInst wasn't streamed in when entity was attached.
		ATTACH_FLAG_NOT_IN_LEVEL_INITIALLY		= BIT(19),
		ATTACH_FLAG_INST_ADDED_LATE				= BIT(20),
		ATTACH_FLAG_IN_DETACH_FUNCTION			= BIT(21),	// Prevents recursive detachment of on-ground attachment
		ATTACH_FLAG_DONT_ASSERT_ON_MATRIX_CHANGE= BIT(22),	// Allows us to assert on code other than the attachment code fiddling with the entity matrix.
		ATTACH_FLAG_NOT_NEEDED_ON_DETACH		= BIT(23),
	};


#if ATTACHMENT_ENABLE_UPDATE_RECORDS

struct fwAttachmentUpdateRecord
{
	fwAttachmentUpdateRecord() { m_strCodeFunction = NULL; m_strCodeFile = NULL; m_nCodeLine=0xFFFFFFFF; }

	//The function that updated the attachment
	const char * m_strCodeFunction;
	const char * m_strCodeFile;
	u32			 m_nCodeLine;

	u32			 m_frameNumber;

	//Data about the change
	float		 m_fNonOrthoParent;
	float		 m_fNonOrthoAttach;
	float		 m_fNonOrthoThisBefore;
	float		 m_fNonOrthoThisAfter;
};

#endif


class fwAttachmentEntityExtension
{
public:

	FW_REGISTER_CLASS_POOL(fwAttachmentEntityExtension)

	fwAttachmentEntityExtension(fwEntity* thisEntity);
	~fwAttachmentEntityExtension();

	inline Vector3 GetAttachOffset() const					{ return m_vecAttachOffset; }
	void SetAttachOffset(Vector3::Vector3Param offset);

	inline Vector3 GetAttachParentOffset() const					{ return m_vecAttachParentOffset; }
	inline void SetAttachParentOffset(Vector3::Vector3Param offset)	{ m_vecAttachParentOffset = offset; }

	inline Quaternion GetAttachQuat() const					{ return m_AttachQuat; }
	inline void SetAttachQuat(const Quaternion &quat)		
	{ 
#if __DEV
		Matrix34 matOffset;
		matOffset.Identity();
		matOffset.FromQuaternion(quat);
		Assert(matOffset.IsOrthonormal());
#endif
		Assertf(quat.Mag2() >= square(0.999f) && quat.Mag2() <= square(1.001f),"Quaternion <%f, %f, %f, %f> is not a pure rotation (%f).", quat.x, quat.y, quat.z,quat.w, quat.Mag2());
		m_AttachQuat = quat; 
	}

	inline s16 GetOtherAttachBone() const					{ return m_nOtherAttachBone; }
	inline void SetOtherAttachBone(s16 otherAttachBone)		{ m_nOtherAttachBone = otherAttachBone; }

	inline s16 GetMyAttachBone() const						{ return m_nMyAttachBone; }
	inline void SetMyAttachBone(s16 myAttachBone)			{ m_nMyAttachBone = myAttachBone; }


	inline s16 GetAttachBone() const						{ return m_nOtherAttachBone; }

	inline bool GetAttachFlag(eAttachFlags nFlag) const		{ Assert(nFlag > ATTACH_STATES); return (m_nAttachFlags & nFlag) != 0; }
	inline u32 GetAttachFlags() const						{ return m_nAttachFlags; }
	inline u32 GetExternalAttachFlags() const				{ return (m_nAttachFlags & ((1<<NUM_EXTERNAL_ATTACH_FLAGS)-1)); }
	inline void SetAttachFlag(eAttachFlags nFlag, bool bOn)	{ Assert(nFlag > ATTACH_STATES); (bOn ? m_nAttachFlags |= nFlag : m_nAttachFlags &= ~nFlag); }
	inline void SetAttachFlags(u32 attachFlags)				{ m_nAttachFlags = attachFlags; }

	inline int GetAttachState() const						{ return (m_nAttachFlags &ATTACH_STATES); }
	inline void SetAttachState(int nAttachState)			{ m_nAttachFlags &=  ~(u32)ATTACH_STATES; m_nAttachFlags |= nAttachState; }

	bool		IsAttachStateBasicDerived() const;

	inline bool GetIsAttached() const						{ return ((m_nAttachFlags &ATTACH_STATES) >= ATTACH_STATE_BASIC); }
	inline bool GetIsAttachedInCar() const					{ return (GetAttachState()>=ATTACH_STATE_PED_ENTER_CAR && GetAttachState()<=ATTACH_STATE_PED_EXIT_CAR); }
	inline bool GetIsAttachedOnMount() const				{ return (GetAttachState() == ATTACH_STATE_PED_ON_MOUNT); }
	inline bool GetIsAttachedToGround() const				{ return (GetAttachState() == ATTACH_STATE_PED_ON_GROUND); }
	inline bool GetShouldProcessAttached() const			{ return (GetAttachState() > ATTACH_STATE_NONE ); }//&& bPostPrerender == GetAttachFlag(ATTACH_FLAG_PROCESS_POST_PRERENDER)); }
	inline bool GetIsRootParentAttachment() const			{ return !GetAttachParent(); }

	inline fwEntity* GetSiblingAttachment() const			{ return m_pAttachSibling; }
	inline fwEntity* GetChildAttachment() const				{ return m_pAttachChild; }
	fwEntity*		GetAttachParent() const						{ return (GetIsAttached() ? m_pAttachParent : NULL); }	// only return pointer if we're actually attached
	fwEntity*		GetAttachParentForced() const					{ return m_pAttachParent; }						// always return attach pointer, even if we are in detaching state
	fwEntity*		GetThisEntity() const					{return m_pThisEntity; }

	void ClearParentAttachmentVars(bool bDestroyParentNodeIfZeroAdjacency=true, 
								   bool bDestroyChildNodeIfZeroAdjacency=true, 
								   fwAttachmentEntityExtension* parentExtension = NULL /*If this is NULL then GetExtension will be called, else this will be used*/);

	// This should only be called by the owning entity
	void SetParentAttachment(fwEntity *thisEntity, fwEntity* pNewParent);

	// Return the attach parent from the heirarchy that we need to calculate an object space attachment to directly
	fwEntity* FindParentWithCameraRelativeExtension();
	// PURPOSE: If an object space attach parent exists somewhere above us in the heirarchy, calculates
	// the correct offset in that parent objects space.
	bool FindParentAndOffsetWithCameraRelativeExtension(fwEntity*& pEnt, Mat34V_InOut objectSpaceOffset);

	// PURPOSE: Calculates the attach offset of this entity relative to its immediate parent.
	void GetOffsetFromParent(Mat34V_InOut objectSpaceOffset, bool useThirdPersonSkeleton=false);
	// PURPOSE: Calculates the attach offset of this entity relative to the specified parent entity,
	//			(this entity must be a child of the given entity, but not necessarily an immediate child,
	//			e.g. will calculate the offset from a parent's parent, etc)
	void GetOffsetFromParent(fwEntity* pParentEntity, Mat34V_InOut objectSpaceOffset, bool useThirdPersonSkeleton=false);
	
	void AttachToEntity(fwEntity *thisEntity, fwEntity *otherEntity, s16 nEntBone, u32 nAttachFlags, const Vector3* pVecOffset, const Quaternion* pQuatOrientation, const Vector3* pVecParentOffset = NULL, s16 nMyBone = -1);

	bool GetRequiresPostPreRender() const;

	//Storing constraint handles
	//instB is the parent or world
	bool AddConstraintHandle(phConstraintHandle& handle);
	phConstraintHandle GetConstraintHandle(int i);
	int  GetNumConstraintHandles();
	void RemoveAttachmentConstraints();
	phConstraintBase* FindConstraint(const phInst* pInstChild, const phInst* pInstParent, u16 componentChild, u16 componentParent);  //Returns temporary pointer

#if __BANK
	void RenderDebug(bool drawAttachmentExtensions, bool drawAttachmentEdges, bool displayAttachmentEdgeType, bool displayAttachmentEdgeData, 
		bool displayInvokingFunction, bool displayInvokingFile, bool displayPhysicsInfo, bool displayUpdateRecords, float nodeRadius, 
		bool nodeRelPosWorldUp, float nodeEntityDist) const;

	void DebugSetInvocationData(const char *strCodeFile, const char* strCodeFunction, u32 nCodeLine);
	char const * DebugGetInvokingFunction() const;

	static const char * GetAttachStateString(int nAttachState)
	{
		switch(nAttachState)
		{
			case ATTACH_STATE_NONE:
				return "State_None";
			case ATTACH_STATE_DETACHING:
				return "State_Detaching";
			case ATTACH_STATE_BASIC:
				return "State_Basic";
			case ATTACH_STATE_PHYSICAL:
				return "State_Physical";
			case ATTACH_STATE_WORLD:
				return "State_World";
			case ATTACH_STATE_WORLD_PHYSICAL:
				return "State_World_Physical";
			case ATTACH_STATE_PED:			
				return "State_Ped";
			case ATTACH_STATE_PED_WITH_ROT:	
				return "State_Ped_With_Rot";
			case ATTACH_STATE_RAGDOLL:		
				return "State_Ragdoll";
			case ATTACH_STATE_PED_ENTER_CAR:
				return "State_Ped_Enter_Car";
			case ATTACH_STATE_PED_IN_CAR:	
				return "State_Ped_In_Car";
			case ATTACH_STATE_PED_EXIT_CAR:	
				return "State_Ped_Exit_Car";
			case ATTACH_STATE_PED_ON_MOUNT:	
				return "State_Ped_On_Mount";
			case ATTACH_STATE_PED_ON_GROUND:
				return "State_Ped_On_Ground";
			default:
				return "State_String_Not_Set";
		}
	}
	static const char * GetAttachFlagString(u32 attachFlagBit)
	{
		switch(attachFlagBit)
		{
			case ATTACH_FLAG_ATTACHED_TO_WORLD_BASIC:
				return "Flag_Attached_To_World_Basic";
			case ATTACH_FLAG_POS_CONSTRAINT:	
				return "Flag_Pos_Constraint";
			case ATTACH_FLAG_ROT_CONSTRAINT:		
				return "Flag_Rot_Constraint";
			case ATTACH_FLAG_DO_PAIRED_COLL:		
				return "Flag_Do_Paired_Coll";
			case ATTACH_FLAG_INITIAL_WARP:		
				return "Flag_Initial_Warp";
			case ATTACH_FLAG_DONT_ASSERT_ON_NULL_ENTITY:
				return "Flag_Dont_Assert_Only_Null_Entity";
			case ATTACH_FLAG_AUTODETACH_ON_DEATH:		
				return "Flag_Autodetach_On_Death";
			case ATTACH_FLAG_AUTODETACH_ON_RAGDOLL:	
				return "Flag_Autodetach_On_Ragdoll";
			case ATTACH_FLAG_COL_ON:					
				return "Flag_Col_On";
			case ATTACH_FLAG_DELETE_WITH_PARENT:		
				return "Flag_Delete_With_Parent";
			case ATTACH_FLAG_NO_PHYSICS_INST_YET:		
				return "Flag_No_Physics_Inst_Yet";
			case ATTACH_FLAG_NOT_IN_LEVEL_INITIALLY:		
				return "Flag_Not_In_Level_Initially";
			case ATTACH_FLAG_INST_ADDED_LATE:
				return "Flag_Inst_Added_Late";
			case ATTACH_FLAG_DONT_NETWORK_SYNC:	
				return "Flag_Dont_Network_Sync";
			case ATTACH_FLAG_IN_DETACH_FUNCTION:
				return "Flag_In_Detach_Function";
			case ATTACH_FLAG_ACTIVATE_ON_DETACH:
				return "Flag_Activate_On_Detach";
			default:
				return "Flag_String_Not_Set";
		}
	}
#endif

#if ATTACHMENT_ENABLE_UPDATE_RECORDS
	void AddUpdateRecord(const char *strCodeFile, const char* strCodeFunction, u32 nCodeLine, float	fNonOrthoParent,
		float fNonOrthoAttach, float  fNonOrthoThisBefore, float fNonOrthoThisAfter);
#endif

private:

	void		SetAttachParent(fwEntity *parent)			{ m_pAttachParent = parent; }
	inline void SetSiblingAttachment(fwEntity* pNewSibling) { m_pAttachSibling = pNewSibling; } 
	inline void SetChildAttachment(fwEntity *pNewChild)		{ m_pAttachChild = pNewChild; }
	
	void NotifyChildDetachment(fwEntity* pDetachingEntity, fwEntity* pSiblingOfDetachingEntity, bool bDestroyThisNodeIfZeroAdjacency=true);
	void NotifySiblingDetachment(fwEntity* pDetachingEntity, fwEntity* pSiblingOfDetachingEntity);
	void NotifyChildAttachment(fwEntity* pAttachingEntity);
	void NotifySiblingAttachment(fwEntity* pAttachingEntity);

	bool IsNodeAdjacencyNonZero(); //Is there a parent or any children?

protected:

	//(vptr takes 4 bytes)

	fwEntity* m_pAttachParent; 
	fwEntity* m_pAttachChild;
	fwEntity* m_pAttachSibling;
	fwEntity* m_pThisEntity;

#if __BANK
	const char * m_strCodeFunction;
	const char * m_strCodeFile;
	u32			 m_nCodeLine;
#endif

#if ATTACHMENT_ENABLE_UPDATE_RECORDS
	atQueue<fwAttachmentUpdateRecord, ATTACHMENT_NUM_UPDATE_RECORDS> m_DebugUpdateRecords;
#endif

	Vector3 m_vecAttachOffset;	// This is world pos for constraints with world
	Vector3 m_vecAttachParentOffset;	// Attachment offset on parent	
	Quaternion m_AttachQuat;

	fwRegdRef<class fwEntity> m_pNoCollisionEntity;

	s16 m_nOtherAttachBone;
	s16 m_nMyAttachBone;
	u32 m_nAttachFlags;
	u16 m_nNumConstraintHandles;

	phConstraintHandle m_ParentAttachmentConstraints[ATTACHMENT_MAX_CONSTRAINTS]; //Only the child entity of the pair of constrained objects has a handle
	
public:
#if __BANK
	static int sm_AttachmentExtCount;
#endif // __BANK
};

// INLINE FUNCTIONS:
inline void fwAttachmentEntityExtension::SetAttachOffset(Vector3::Vector3Param offset)
{
	// Check for NaNs in the position vector.
	m_vecAttachOffset = offset;
	Assert(m_vecAttachOffset.IsEqual(m_vecAttachOffset));
}


} // namespace rage

#if __DEV
// Forward declare pool full callback so we don't get two versions of it
namespace rage { template<> void fwPool<fwAttachmentEntityExtension>::PoolFullCallback(); }
#endif // __DEV

#endif // ENTITY_ATTACHMENTENTITYEXTENSION_H
