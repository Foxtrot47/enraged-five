//
// entity/entity.cpp : base class for all entities in the world
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "entity.h"

#include "bank/group.h"
#include "creature/componentextradofs.h"
#include "creature/componentskeleton.h"
#include "creature/creature.h"
#include "diag/output.h"
#include "entity/archetype.h"
#include "entity/sceneupdate.h"
#include "fwanimation/animdirector.h"
#include "fwanimation/directorcomponentcreature.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/stores/blendshapestore.h"
#include "grcore/debugdraw.h"
#include "physics/simulator.h"
#include "streaming/streamingvisualize.h"
#include "vfx/decal/decalmanager.h"

#include "drawdata.h"
#include "transform.h"
#include "phframework/inst.h"
#include "fwnet/optimisations.h"
#include "parser/structure.h"

#include "diag/art_channel.h"

namespace rage {

#define ENTITY_TEST_TRANSFORMS 0

#if ENTITY_TEST_TRANSFORMS
#include "atl/array.h"
static atArray<fwEntity*> s_entityArray;
#endif

PARAM(noquattransform, "[entity] Don't use quaternion transforms for placed entities");
#if !__NO_OUTPUT
PARAM(initentityoutput,"[entity] Turn on output for InitEntityFromDefinition");
#endif

AUTOID_IMPL(fwExtension);
INSTANTIATE_RTTI_CLASS(fwEntity,0x9423CDDB);

#if __BANK
static bool s_ShowEntityStats = false;

#if ENABLE_FORCE_MATRICES
static bool s_OnlyUseMat34 = true;
#endif

static int s_EntityCount = 0;

#endif // __BANK

fwEntity::fwEntity()
{
#if RAGE_INSTANCED_TECH
	m_viewportInstancedRenderBit = 0xff;
#endif

#if ENABLE_MATRIX_MEMBER
	m_Transform.SetIdentity();	
	m_Transform.SetScale(1.0f,1.0f);
#else
	m_Transform = fwIdentityTransform::GetInstance();
#endif
	//m_modelId.Invalidate(); 
	m_pArchetype = NULL;

	m_type = 0;
	m_protectedFlags = 0;
	m_baseFlags = 0;
	m_RenderFlags = 0;

	m_phaseVisibilityMask.SetAllFlags();
	m_visibilityType = 0;

	m_CurrentPhysicsInst = NULL;

	m_container.as_OwnerEntityContainer = NULL;

	m_DynamicEntityComponent = NULL;

	SetBaseFlag(IS_VISIBLE);
	SetBaseFlag(IS_SEARCHABLE);

#if __BANK
	s_EntityCount++;
#endif // __BANK

#if ENTITY_TEST_TRANSFORMS
	s_entityArray.PushAndGrow(this);
#endif

}

fwEntity::~fwEntity()
{
#	if !__TOOL && !__RESOURCECOMPILER
		DECALMGR.OnDelete(this);
#	endif

//	TrapEQ((unsigned)m_Transform, NULL); 
#if !ENABLE_MATRIX_MEMBER
	if (m_Transform)
	{
		m_Transform->Delete();
		m_Transform = NULL;
	}
#endif

	if (m_pDrawHandler)
	{
		delete m_pDrawHandler;
	}

	delete m_DynamicEntityComponent;

	if (m_pArchetype && IsBaseFlagSet(PROTECT_ARCHETYPE))
	{
		fwArchetypeManager::ScheduleRemoveArchetypeToTypeFileRef(m_pArchetype);
	}

	Assert( m_container.as_OwnerEntityContainer == NULL );

	// Make sure the subclass properly destroyed the physics instance.
	Assert(GetCurrentPhysicsInst() == NULL);

	// Make sure we're not in the scene update anymore.
	Assert(!fwSceneUpdate::IsInSceneUpdate(*this));

	STRVIS_DELETE_ENTITY(this);

#if __BANK
	s_EntityCount--;
#endif // __BANK

#if ENTITY_TEST_TRANSFORMS
	s_entityArray.DeleteMatches(this);
#endif
}

void fwEntity::ProtectStreamedArchetype(void)
{
	Assert(m_pArchetype);
	if (m_pArchetype->IsStreamedArchetype() && !IsBaseFlagSet(PROTECT_ARCHETYPE))
	{
		fwArchetypeManager::AddArchetypeToTypeFileRef(m_pArchetype);
		SetBaseFlag(PROTECT_ARCHETYPE);
	}

	STRVIS_DELETE_ENTITY(this);
}

void fwEntity::UnprotectStreamedArchetype(void)
{
	Assert(m_pArchetype);
	if (m_pArchetype->IsStreamedArchetype() && IsBaseFlagSet(PROTECT_ARCHETYPE))
	{
		ClearBaseFlag(PROTECT_ARCHETYPE);
		fwArchetypeManager::ScheduleRemoveArchetypeToTypeFileRef(m_pArchetype);
	}
}

// The criteria for deciding whether or not to use a full matrix is duplicated in tools code.
// If you change these criteria you must inform tools as this check is done during static collision baking:
// //RAGE/gta5/dev/rage/framework/tools/src/cli/MapExport/BoundsProcessor/SceneXml/RefObject.cs
void fwEntity::InitTransformFromDefinition(fwEntityDef* definition, fwArchetype* archetype)
{
	// Setup transform
	const fwEntityDef& def = *definition;

	bool dynamic = false;
	if (archetype->GetClipDictionaryIndex() != -1 && archetype->GetHasAnimation())
	{
		dynamic = true;
	}

	bool scaled = ((def.m_scaleXY != 1.f) || (def.m_scaleZ != 1.f));
	scaled = scaled && archetype->GetDrawableType()!=fwArchetype::DT_FRAGMENT;

	Assertf((!scaled) || (def.m_scaleXY > 0.f && def.m_scaleXY < 4.f && def.m_scaleZ > 0.f && def.m_scaleZ < 4.f), 
			"Bad scale values xy: %f z: %f", def.m_scaleXY , def.m_scaleZ);

	// If quaternion axis is not the z-axis allocate a full matrix
#if ENABLE_MATRIX_MEMBER	
	Mat34V trans;
#else
	fwTransform* trans = NULL;	
#endif	
	if(dynamic || rage::Abs(def.m_rotation.x) > 0.05f || rage::Abs(def.m_rotation.y) > 0.05f ||
		((def.m_flags & fwEntityDef::FLAG_FULLMATRIX) && (def.m_rotation.x != 0.0f || def.m_rotation.y != 0.0f)))
	{
		QuatV q(-def.m_rotation.x, -def.m_rotation.y, -def.m_rotation.z, def.m_rotation.w);
		Vec3V pos(def.m_position);

#if ENABLE_FORCE_MATRICES
	#if ENABLE_MATRIX_MEMBER
		if(true)
	#else
		if(s_OnlyUseMat34)
	#endif
		{
			Mat34V m;
			Mat34VFromQuatV(m, q, pos);
			
			if (!scaled)
			{
				#if ENABLE_MATRIX_MEMBER
				trans = m;
				trans.GetCol1Ref().SetWf(1.0f);
				trans.GetCol2Ref().SetWf(1.0f);
				#else
				trans = rage_new fwMatrixScaledTransform(m);
				trans->SetScale(1.0f,1.0f);
				#endif //ENABLE_MATRIX_MEMBER

			}
			else
			{
				#if ENABLE_MATRIX_MEMBER
				trans = m;
				trans.GetCol1Ref().SetWf(def.m_scaleXY);
				trans.GetCol2Ref().SetWf(def.m_scaleZ);
				#else
				trans = rage_new fwMatrixScaledTransform(m);
				trans->SetScale(def.m_scaleXY, def.m_scaleZ);
				#endif //ENABLE_MATRIX_MEMBER
			}
		}		
		else
		{
#endif //ENABLE_FORCE_MATRICES
			#if !ENABLE_MATRIX_MEMBER
			if (!PARAM_noquattransform.Get() && !archetype->GetHasAnimation())
			{
				if (!scaled)
				{
					trans = rage_new fwQuaternionTransform(q, pos);
				}
				else
				{
					trans = rage_new fwQuaternionScaledTransform(q, pos);
					trans->SetScale(def.m_scaleXY, def.m_scaleZ);
				}
			}
			else
			{
				Mat34V m;
				Mat34VFromQuatV(m, q, pos);

				if (!scaled)
				{
					trans = rage_new fwMatrixTransform(m);
				}
				else
				{
					trans = rage_new fwMatrixScaledTransform(m);
					trans->SetScale(def.m_scaleXY, def.m_scaleZ);
				}
				
			}
		#endif
#if ENABLE_FORCE_MATRICES
		}
#endif //ENABLE_FORCE_MATRICES
	}
	else
	{
		
#if ENABLE_FORCE_MATRICES
#if ENABLE_MATRIX_MEMBER
		if(true)
#else
		if(s_OnlyUseMat34)
#endif
		{
			float heading =	0.0f;
			float cosHeading =	1.0f;
			float sinHeading =	0.0f;

			if(def.m_rotation.w != 1.0f)
			{
				float acos = rage::Acosf(def.m_rotation.w);
				heading = (def.m_rotation.z < 0.0f ? 2.0f * acos : -2.0f * acos);
				rage::cos_and_sin(cosHeading, sinHeading, heading);
			}
						
			Mat34V m;
			Vec3V constant(Vec::V4VConstant<0x80000000,U32_ZERO,U32_ZERO,U32_ZERO>());
			Vec3V a(cosHeading, sinHeading, 0.0f);
			Vec3V bperm = a.Get<Vec::Y, Vec::X, Vec::Z>();
			Vec3V c(V_Z_AXIS_WONE);
			Vec3V b = Xor(bperm, constant);
			Vec3V p = Vec3V(def.m_position.x,def.m_position.y,def.m_position.z);
			m.SetCols(a, b, c, p);

			if (!scaled)
			{
				#if ENABLE_MATRIX_MEMBER
				trans = m;
				trans.GetCol1Ref().SetWf(1.0f);
				trans.GetCol2Ref().SetWf(1.0f);
				#else
				trans = rage_new fwMatrixScaledTransform(m);
				trans->SetScale(1.0f,1.0f);
				#endif //ENABLE_MATRIX_MEMBER
			}
			else
			{
				#if ENABLE_MATRIX_MEMBER
				trans = m;
				trans.GetCol1Ref().SetWf(def.m_scaleXY);
				trans.GetCol2Ref().SetWf(def.m_scaleZ);				
				#else
				trans = rage_new fwMatrixScaledTransform(m);
				trans->SetScale(def.m_scaleXY, def.m_scaleZ);
				#endif //ENABLE_MATRIX_MEMBER
			}
		}
		else
		{
#endif //ENABLE_FORCE_MATRICES
			// We have around 65% tranformation with W==1 so we should skip the math on those cases to save performance
			// For any other value we keep doing the expensive calculation
			#if !ENABLE_MATRIX_MEMBER
			if(def.m_rotation.w == 1.0f)
			{
				static const float heading =	0.0f;
				static const float cosHeading =	1.0f;
				static const float sinHeading =	0.0f;

				if (!scaled)
				{
					trans = rage_new fwSimpleTransform(def.m_position.x, def.m_position.y, def.m_position.z, heading, cosHeading, sinHeading);
				}
				else
				{
					trans = rage_new fwSimpleScaledTransform(def.m_position.x, def.m_position.y, def.m_position.z, heading, cosHeading, sinHeading, def.m_scaleXY, def.m_scaleZ);
				}
			}
			else
			{
				float acos = rage::Acosf(def.m_rotation.w);
				float heading = (def.m_rotation.z < 0.0f ? 2.0f * acos : -2.0f * acos);

				if (!scaled)
				{
					trans = rage_new fwSimpleTransform(def.m_position.x, def.m_position.y, def.m_position.z, heading);
				}
				else
				{
					trans = rage_new fwSimpleScaledTransform(def.m_position.x, def.m_position.y, def.m_position.z, heading, def.m_scaleXY, def.m_scaleZ);
				}
			}
			#endif
#if ENABLE_FORCE_MATRICES
		}
#endif //ENABLE_FORCE_MATRICES	
	}
	SetTransform(trans);
}

#if !__NO_OUTPUT
const char *fwEntity::DiagCb(void *entity, void *)
{
	static char diagCb[256];

	fwEntity *entityPtr = static_cast<fwEntity *>(entity);
	Vec3V coords = entityPtr->GetTransform().GetPosition();
	formatf(diagCb, "%s (%.2f, %.2f, %.2f)", entityPtr->GetModelName(), coords.GetXf(), coords.GetYf(), coords.GetZf());

	return diagCb;
}
#endif // !__NO_OUTPUT

phCollider* fwEntity::GetCurrentColliderFromSimulator()
{
	if(GetCurrentPhysicsInst() && GetCurrentPhysicsInst()->IsInLevel())
	{
		return PHSIM->GetCollider(GetCurrentPhysicsInst());
	}
	else
	{
		return NULL;
	}
}

const phCollider* fwEntity::GetCurrentColliderFromSimulator() const
{
	if(GetCurrentPhysicsInst() && GetCurrentPhysicsInst()->IsInLevel())
	{
		return PHSIM->GetCollider(GetCurrentPhysicsInst());
	}
	else
	{
		return NULL;
	}
}

#if __DEV
void fwEntity::ValidateCurrentCollider() const
{
	const phCollider* cachedCollider = GetDynamicComponent() ? GetDynamicComponent()->GetCollider() : NULL;
	const phCollider* actualCollider = GetCurrentColliderFromSimulator();
	Assertf(cachedCollider == actualCollider,	"Cached phCollider is not valid on '%s'"
												"\n\tCached Collider: 0x%p"
												"\n\tActual Collider: 0x%p"
												"\n\tHas Dynamic Component: %s"
												"\n\tHas Physics Inst: %s"
												"\n\tInst In Level: %s"
												"\n\tHas User Data: %s",
												GetDebugName(),
												cachedCollider,
												actualCollider,
												GetDynamicComponent() ? "Yes" : "No",
												GetCurrentPhysicsInst() ? "Yes" : "No",
												(GetCurrentPhysicsInst() && GetCurrentPhysicsInst()->IsInLevel()) ? "Yes" : "No",
												(GetCurrentPhysicsInst() && GetCurrentPhysicsInst()->GetUserData()) ? "Yes" : "No");
}
#endif // __DEV

void fwEntity::OnActivate(phInst* pInst, phInst* UNUSED_PARAM(pOtherInst))
{
	if(pInst == GetCurrentPhysicsInst())
	{
		UpdateCurrentCollider();
	}
}

void fwEntity::OnDeactivate(phInst* pInst)
{
	if(pInst == GetCurrentPhysicsInst())
	{
		UpdateCurrentCollider();
	}
}

void fwEntity::InitExtensionsFromDefinition(fwEntityDef* definition, fwArchetype* UNUSED_PARAM(archetype), s32 mapDataDefIndex)
{
	DIAG_CONTEXT_MESSAGE("Entity %s", DiagCb, this, NULL);

	for (int i = 0; i < definition->m_extensions.GetCount(); ++i)
	{
		//Assertf(false, "JW - I don't think the code is getting in here at all. Problem : extensions are being created from a global factory");

		fwExtensionDef*				extDef = definition->m_extensions[i];
		const u32					nameHash = extDef->parser_GetStructure()->GetNameHash();
		s32							factoryID = fwMapData::GetExtensionFactory( nameHash );
		fwExtensionFactoryBase*		factory = fwExtensionManager::GetFactory(factoryID);

		fwExtension*				ext = factory->CreateBaseItem(mapDataDefIndex);

		if (ext)
		{
			ext->InitEntityExtensionFromDefinition( extDef, this );
			GetExtensionList().Add( *ext );
		} else
		{
			Assertf(false, "Unable to create extension %s for entity %s. Likely pool is full (capacity is %d).",factory->GetDebugName(), definition->m_archetypeName.GetCStr(), factory->GetCapacity(mapDataDefIndex));
		}
	}
}

void fwEntity::InitEntityFromDefinition(fwEntityDef* definition, fwArchetype* archetype, s32 mapDataDefIndex)
{
	fwEntityDef& def = *definition;	
#if __BANK
	if(PARAM_initentityoutput.Get())
		DIAG_CONTEXT_MESSAGE("InitEntityFromDefinition(%s)", def.m_archetypeName.GetCStr());
#endif
	InitTransformFromDefinition(definition, archetype);

	// if there is an override lod distance for this instance then use it
	artAssertf(rage::FPIsFinite(def.m_lodDist), "%s",def.m_archetypeName.GetCStr());
	float lodDistance = (def.m_lodDist > 0?def.m_lodDist:archetype->GetLodDistanceUnscaled());
	artAssertf(lodDistance > 1.0f, "%s", def.m_archetypeName.GetCStr());

	SetLodDistance(u32(lodDistance));
	GetLodData().SetLodType(def.m_lodLevel);

	GetLodData().SetIsParentOfInterior( (definition->m_flags & fwEntityDef::FLAG_IS_INTERIOR_LOD)!=0 );

	if (definition->m_flags & fwEntityDef::FLAG_DONT_INSTANCE_COLLISION)
		SetBaseFlag(NO_INSTANCED_PHYS);

	if (definition->m_flags & fwEntityDef::FLAG_STREAM_LOWPRIORITY)
		SetBaseFlag(LOW_PRIORITY_STREAM);

	if (definition->m_flags & fwEntityDef::FLAG_IS_FIXED)
		SetBaseFlag(IS_FIXED);

	InitExtensionsFromDefinition(definition, archetype, mapDataDefIndex);
}

//Special overloads for entity batch entities. Asserts by default so it's clear these shouldn't be used for non-batch entities.
void fwEntity::InitEntityFromDefinition(const fwPropInstanceListDef* UNUSED_PARAM(definition), fwArchetype* UNUSED_PARAM(archetype), s32 UNUSED_PARAM(mapDataDefIndex))
{
	Assert(false);
}

void fwEntity::InitEntityFromDefinition(fwGrassInstanceListDef* UNUSED_PARAM(definition), fwArchetype* UNUSED_PARAM(archetype), s32 UNUSED_PARAM(mapDataDefIndex))
{
	Assert(false);
}

void fwEntity::UpdateCurrentCollider()
{
	// Don't create a dynamic component unless we're going to give it a non-NULL collider. 
	// The result of GetCurrentCollider will be the same regardless.
	phCollider* newCollider = GetCurrentColliderFromSimulator();
	if(newCollider != NULL)
	{
		CreateDynamicComponentIfMissing();
	}
	if(fwDynamicEntityComponent* dynamicEntityComponent = GetDynamicComponent())
	{
		dynamicEntityComponent->SetCollider(newCollider);
	}
}

void fwEntity::SetCurrentPhysicsInstInternal(phInst* pInst)
{
	m_CurrentPhysicsInst = pInst;
	UpdateCurrentCollider();
}

void fwEntity::SetCurrentPhysicsInst(phInst* inst)
{
	Assert(inst);
	//AF: this should be a error as the referencing counting will all go to fuck if you start
	// replacing inst's
	Assert(m_CurrentPhysicsInst == NULL);
	SetCurrentPhysicsInstInternal(inst);
	// utilise userdata pointer in phInst to point back to the parent entity
	m_CurrentPhysicsInst->SetUserData(this);
	m_CurrentPhysicsInst->SetInstFlag(phfwInst::FLAG_USERDATA_ENTITY, true);
}

void fwEntity::ClearCurrentPhysicsInst()
{
	SetCurrentPhysicsInstInternal(NULL);
}

//
void fwEntity::DeleteSkeleton()
{
	crSkeleton *skeleton = GetEntitySkeleton();

	if (skeleton)
	{
		delete skeleton;
		SetSkeleton(NULL);
	}
}

const Matrix34& fwEntity::GetThirdPersonSkeletonObjectMtx(int boneIdx) const
{
	Assertf(false, "Shouldn't be calling into baseclass version of GetThirdPersonSkeletonObjectMtx!");
	return GetObjectMtx(boneIdx);
}

#if !__NO_OUTPUT
size_t fwEntity::GetSkeletonSize() const
{
	crSkeleton* pSkeleton = GetSkeleton();
	if (pSkeleton)
	{
		size_t bytes = pSkeleton->GetMemorySize();

		const fwDynamicEntityComponent* pDynamic = GetDynamicComponent();
		if (pDynamic)
		{			
			crSkeleton* pDynamicSkeleton = pDynamic->GetSkeleton();
			if (pDynamicSkeleton && pSkeleton != pDynamicSkeleton)
				bytes += pDynamicSkeleton->GetMemorySize();
		}

		return bytes;
	}

	return 0;	
}

void fwEntity::PrintSkeletonSummary() const
{
	crSkeleton* pSkeleton = GetSkeleton();			
	if (pSkeleton)
	{
		Displayf("%s, %d, %" SIZETFMT "d", GetModelName(), pSkeleton->GetBoneCount(), pSkeleton->GetMemorySize());

		const fwDynamicEntityComponent* pChildDynamic = GetDynamicComponent();
		if (pChildDynamic)
		{
			crSkeleton* pChildSkeleton = pChildDynamic->GetSkeleton();

			if (pChildSkeleton && pSkeleton != pChildSkeleton  && &pSkeleton->GetSkeletonData() != &pChildSkeleton->GetSkeletonData())
			{
				fwEntityDesc* pChildEntityDesc = pChildDynamic->GetEntityDesc();
				fwEntity* pChildEntity = pChildEntityDesc ? pChildEntityDesc->GetEntity() : NULL;
				const char* pszName = pChildEntity ? pChildEntity->GetModelName() : "?";
				Displayf("  * %s, %d, %" SIZETFMT "d", pszName, pChildSkeleton->GetBoneCount(), pChildSkeleton->GetMemorySize());
			}
		}
	}
}
#endif

#if ENABLE_BLENDSHAPES
void fwEntity::InitTargetManager()
{
	ShutdownTargetManager();

	Assert(m_pArchetype);
	s32 blendShapeFileIndex = m_pArchetype->GetBlendShapeFileIndex();
	if (blendShapeFileIndex != -1)
	{
		SetTargetManager(g_BlendShapeStore.Get(blendShapeFileIndex));
		if (GetTargetManager())
		{
			g_BlendShapeStore.AddRef(blendShapeFileIndex, REF_OTHER);
		}
	}
}

void fwEntity::ShutdownTargetManager()
{
	// Construction and destruction of the target manager is done by the blendshape store
	if (GetTargetManager())
	{
		Assert(m_pArchetype);
		s32 blendShapeFileIndex = m_pArchetype->GetBlendShapeFileIndex();
		if (blendShapeFileIndex != -1)
		{
			g_BlendShapeStore.RemoveRefWithoutDelete(blendShapeFileIndex, REF_OTHER);
		}
	}
	SetTargetManager(NULL);
}
#endif // ENABLE_BLENDSHAPES

void fwEntity::AddExtraDofsCreatureComponent(const crFrameData& frameData)
{
	crCreatureComponentExtraDofs* pMoverDofs = static_cast<crCreatureComponentExtraDofs*>(GetCreature()->AllocateComponent(crCreatureComponent::kCreatureComponentTypeExtraDofs));
	pMoverDofs->Init(*GetCreature(), frameData, &fwAnimDirectorComponentCreature::GetCommonPool());
	pMoverDofs->GetIdentityFrame().Zero();
	pMoverDofs->GetIdentityFrame().InvalidateDof(0xf0+kFormatTypeQuaternion, 0);
	pMoverDofs->GetPoseFrame().Zero();

	GetCreature()->AddComponent(*pMoverDofs);
}

void fwEntity::WaitForAnyActiveAnimUpdateToComplete(bool excludeMidPhysics) const
{
	if (GetDynamicComponent() && GetDynamicComponent()->GetAnimDirector())
	{
		GetDynamicComponent()->GetAnimDirector()->WaitForAnyActiveUpdateToComplete(excludeMidPhysics);
	}
}


#if !__SPU
void fwEntity::SetActivePoseFromSkel()
{
	fragInst* pFragInst = GetFragInst();
	if(pFragInst)
	{
		pFragInst->SetMusclesFromSkeleton(*GetSkeleton());
	}
}
#endif // !__SPU

void fwEntity::GetAABB(spdAABB& aabb) const
{
	aabb.Invalidate();

	const fwArchetype* pMI = GetArchetype();
	if (pMI)
	{
		Matrix34 mat;
		GetMatrixCopy(mat);
		aabb = pMI->GetBoundingBox();
		aabb.Transform(RCC_MAT34V(mat));
	}
}

bool fwEntity::IsEntityAParentAttachment(const fwEntity* entity) const
{
	fwEntity* pParent = GetAttachParentForced();
	bool bReturn = false;
	while(!bReturn && pParent)
	{
		bReturn = (pParent == entity);
		pParent = pParent->GetAttachParentForced();
	}

	return bReturn;
}

fwEntity* fwEntity::GetRootLod()
{
	if (!m_lodData.IsOrphanHd())
	{
		fwEntity* pLod = this;
		while (pLod->GetLodData().HasLod()) { pLod = pLod->GetLod(); }
		return pLod;
	}
	return NULL;
}

#if __BANK

#if ENTITY_TEST_TRANSFORMS

static void TransformTest(void)
{
	float fTime = 0.0f;
	int size = 0;
	unsigned int count = 0;
	typedef rage::atArray<fwEntity*>::iterator ListIterator;
	
	Vec3V p(1.0f,1.0f,1.0f);
	Vec3V p1(-1.0f,-1.0f,-1.0f);
	Vec3V t(1.0f,0.0f,0.0f);
	Vec3V r(0.0f,0.0f,0.0f);
		
	for (ListIterator it=s_entityArray.begin(); it!=s_entityArray.end(); ++it)
	{
		if(it)
		{	
			fwEntity* e = *it;			
			sysTimer tm;

			#if ENABLE_MATRIX_MEMBER
			e->GetTransform().Transform(p);
			r = e->GetTransform().Transform3x3(t);
			e->GetTransform().Transform(p1);
			#else
			e->GetTransform().Transform(p);
			r = e->GetTransform().Transform3x3(t);
			e->GetTransform().Transform(p1);
			#endif
			fTime += tm.GetUsTime();			
		}		
	}

	for (ListIterator it=s_entityArray.begin(); it!=s_entityArray.end(); ++it)
	{
		if(it)
		{	
#if ENABLE_MATRIX_MEMBER			
			size += sizeof(fwMatrixScaledTransform);
#else
			switch(e->m_Transform->GetTypeIdentifier())
			{
			case fwTransform::TYPE_IDENTITY:
				//size += sizeof(fwIdentityTransform);
				break;
			case fwTransform::TYPE_SIMPLE:
				size += sizeof(fwSimpleTransform);
				break;
			case fwTransform::TYPE_SIMPLE_SCALED:
				size += sizeof(fwSimpleScaledTransform);
				break;
			case fwTransform::TYPE_MATRIX:
				size += sizeof(fwMatrixTransform);				
				break;
			case fwTransform::TYPE_MATRIX_SCALED:
				size += sizeof(fwMatrixScaledTransform);				
				break;
			case fwTransform::TYPE_QUATERNION:
				size += sizeof(fwQuaternionTransform);
				break;
			case fwTransform::TYPE_QUATERNION_SCALED:
				size += sizeof(fwQuaternionScaledTransform);
				break;
			default:
				Displayf("Don't know the matrix type", count);
				break;
			}
#endif
			count++;
		}		
	}
		
#if ENABLE_MATRIX_MEMBER
	Displayf("Using fwMatrixScaledTransform as member");	
#else
	if (s_OnlyUseMat34)
	{
		Displayf("Using fwTransform* to fwMatrixScaledTransform");
	}
	else
	{
		Displayf("Using fwTransform* to varable");
	}
#endif
	Displayf("|elements %d\t\t| \ttime %f us\t\t|\t size %d|", count, fTime, size);	
}

#endif //ENABLE_FORCE_MATRICES

void fwEntity::AddWidgets(bkGroup &group)
{
	bkGroup *entityGroup = group.AddGroup("Entity Stats");
	entityGroup->AddToggle("Show Entity Stats", &s_ShowEntityStats);

#if ENABLE_FORCE_MATRICES
	entityGroup->AddToggle("Only Use Matrices", &s_OnlyUseMat34);		
#endif
#if ENTITY_TEST_TRANSFORMS
	entityGroup->AddButton("Run Transform Test", TransformTest);
#endif

}

void fwEntity::DebugDraw()
{
	if (s_ShowEntityStats)
	{
		grcDebugDraw::AddDebugOutput("Active entities: %d", s_EntityCount);
		grcDebugDraw::AddDebugOutput("Dynamic Components: %d", fwDynamicEntityComponent::sm_DynCompCount);
		grcDebugDraw::AddDebugOutput("Attachment Extensions: %d", fwAttachmentEntityExtension::sm_AttachmentExtCount);
	}
}

#endif // __BANK





} // namespace rage
