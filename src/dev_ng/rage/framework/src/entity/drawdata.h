//
// entity/drawdata.h : Interface class for all the drawdata attached to an entity
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef ENTITY_DRAWDATA_H_
#define ENTITY_DRAWDATA_H_

#include "entity/archetypemanager.h"
#include "entity/entity.h"
#include "fwdrawlist/drawlist.h"
#include "fwtl/LinkList.h"
#include "paging/handle.h"
#if !__SPU
#include "system/timemgr.h"
#endif

template<typename T> class fwLink;

struct CStreamingBucket;

namespace rage {

class dlCmdBase;
class fwEntity;
class grmShaderGroup;
class rmcDrawable;
class Vector2;
class Vector3;
class Vector4;

class fwCustomShaderEffect {
public:
	fwCustomShaderEffect() : m_size(0), m_type(0) {}
	virtual ~fwCustomShaderEffect() {}

	// --------------------------------------------------------------------------------------
	// - CustomShaderEffect Interface:														-
	// --------------------------------------------------------------------------------------
	// these must be overloaded by every children of this class:
	virtual void DeleteInstance()					{ delete this; }

	virtual void Update(fwEntity *) {}								

	virtual void SetShaderVariables(rmcDrawable* pDrawable) =0;				// RT: instance setting variables called in BeforeDraw()
	virtual void AddToDrawList(u32 modelIndex, bool bExecute) =0;			// DLC: instance called in BeforeAddToDrawList()
	virtual void AddToDrawListAfterDraw()			{}						// DLC: instance called in AfterAddToDrawList()

	virtual bool AddKnownRefs()						{return false;}			// DLC: Add known refs on a newly-copied object
	virtual void RemoveKnownRefs()					{}						// DLC: Remove known refs on a temporary copy that's about to die

	u32					Size() const				{ return m_size; }
	u8					GetType() const				{ return m_type; }
	dlSharedDataInfo&	GetSharedDataOffset() const	{ return m_SharedDataInfo; }

#if __BANK
	static bool SetFloatShaderGroupVar(  grmShaderGroup *pShaderGroup, const char *varName, float varVal);
	static bool SetVector2ShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, const Vector2& varVal);
	static bool SetVector3ShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, const Vector3& varVal);
	static bool SetVector4ShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, const Vector4& varVal);
#endif //__BANK...

	enum { CSE_MAX_TYPES = 16 };

protected:

	u32									m_size : 16;
	u32									m_type : 4; // [0 .. CSE_MAX_TYPES-1]
	u32									m_pad  : 12;

	mutable dlSharedDataInfo			m_SharedDataInfo;
};

class fwDrawDataAddParams
{
public:
	fwDrawDataAddParams() : AlphaFade(255) {}

	u8 AlphaFade;			// set by the caller of AddToDrawList
};

class ActiveEntity
{
public:

	ActiveEntity() {}
	~ActiveEntity() {};

	inline fwEntity* GetEntity() const { return m_Entity; }
	inline void SetEntity(fwEntity* entity, u32 entityID, float initialScore);

	inline void SetScore(float score)					{ m_Score = score; }
	inline float GetScore() const						{ return m_Score; }

	strIndex GetArchetypeIndex() const					{ return m_ArchetypeIndex; }

	inline u32 GetEntityID() const						{ return m_EntityID; }

	inline void MarkVisible(u32 lastVisibleTimeStamp)	{ m_LastVisibleTimestamp = lastVisibleTimeStamp; }
#if !__SPU
	inline void MarkVisible()							{ m_LastVisibleTimestamp = TIME.GetFrameCount(); }
	inline u32 GetTimeSinceLastVisible() const			{ return TIME.GetFrameCount() - m_LastVisibleTimestamp; }
#endif
	inline u32 GetLastVisibleTimestamp() const			{ return m_LastVisibleTimestamp; }

private:

	fwEntity* m_Entity;

	u32 m_EntityID;

	float m_Score;

	// Frame count at which this entity was last visible
	u32 m_LastVisibleTimestamp;								

	// The strIndex of the archetype for this entity
	strIndex m_ArchetypeIndex;
};

//
// PURPOSE
//	Interface class for all the drawdata attached to an entity
class fwDrawData
{
public:
	fwDrawData();
	virtual ~fwDrawData();

#if GRCTEXTUREHANDLE_IS_PGREF
	rmcDrawable*  GetDrawable() const							{ return m_pDrawable;}
	rmcDrawable** GetDrawablePtrPtr() const						{ return const_cast<rmcDrawable**>(&m_pDrawable.ptr);}
#elif !__SPU
	rmcDrawable*  GetDrawable() const							{ return m_pDrawable;}
	rmcDrawable** GetDrawablePtrPtr() const						{ return m_pDrawable.ptr;}
#else
	rmcDrawable*  GetDrawable() const							{ return *m_pDrawable;}
	rmcDrawable** GetDrawablePtrPtr() const						{ return  m_pDrawable;}
#endif

	// PURPOSE: update data in drawdata class
	virtual void Update(class fwEntity*);

	// PURPOSE: add entity to drawlist using this drawdata
	virtual dlCmdBase* AddToDrawList(fwEntity* entity, fwDrawDataAddParams* params);

    // shared entity draw data:
    dlSharedDataInfo& GetSharedDataOffset() const { return m_SharedDataInfo; }

	// shader effects:
	fwCustomShaderEffect* GetShaderEffect() const { return(this->m_pShaderEffect); }
	void SetShaderEffect(fwCustomShaderEffect* se) { this->m_pShaderEffect=se; }

	// occlusion query
	u32 GetOcclusionQueryId() const { return (u32)m_occlusionQueryId; }
	void SetOcclusionQueryId(u32 id) { m_occlusionQueryId = (u8)(id&0xff); }

	u32 GetPhaseLODs() const { return m_phaseLODs; }
	u32 GetLastLODIdx() const { return m_lastLODIdx; }
	bool GetHasBones() const { return (m_hasBones > 0); }

#if !__SPU
	fwLink<ActiveEntity>* GetActiveListLink() const	{ return m_pActiveListLink; }
#endif // !__SPU

protected:
#if !__SPU
#if GRCTEXTUREHANDLE_IS_PGREF
	pgRef<rmcDrawable> m_pDrawable;
#else
	pgHandle2<rmcDrawable> m_pDrawable;
#endif
	fwLink<ActiveEntity>* m_pActiveListLink;
protected:
#else
	rmcDrawable** m_pDrawable;
	void *m_pActiveListLink;
#endif
    mutable dlSharedDataInfo m_SharedDataInfo;
	fwCustomShaderEffect* m_pShaderEffect;					// local instance of ShaderEffect

	u32				m_phaseLODs : 15;
	u32				m_lastLODIdx : 2;
	u32				m_hasBones : 1;
	u32				m_pad : 6;
	u32				m_occlusionQueryId : 8;

	// note: management of drawable's instances is done at CEntity level:
	void SetDrawable(rmcDrawable* d);
	void RemoveDrawable();
};


class fwCustomShaderEffectBaseType
{
public:
	fwCustomShaderEffectBaseType(): m_type(0) {}
	virtual ~fwCustomShaderEffectBaseType() {}
	virtual void RemoveRef() = 0;

	u8 GetType() const { return m_type; }

protected:
	u8 m_type;

};

inline void ActiveEntity::SetEntity(fwEntity* entity, u32 entityID, float initialScore)
{
	m_Entity = entity;
	m_EntityID = entityID;
	m_Score = initialScore;
	strLocalIndex objIndex = strLocalIndex(entity->GetModelIndex());
	m_ArchetypeIndex = fwArchetypeManager::GetStreamingModule()->GetStreamingIndex(objIndex);
}


}

#endif //ENTITY_DRAWDATA_H_

