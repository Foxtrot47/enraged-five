
#ifndef ENTITY_DYNAMICARCHETYPE_H
#define ENTITY_DYNAMICARCHETYPE_H

#include "atl/hashstring.h"
#include "fwtl/pool.h"
#include "grblendshapes/blendshapes_config.h"
#include "spatialdata/aabb.h"

namespace rage {

class phArchetypeDamp;

class fwDynamicArchetypeComponent
{
public:
	AT_REGISTER_CLASS_POOL(fwDynamicArchetypeComponent);

	fwDynamicArchetypeComponent();

	void SetClipDictionaryIndex(const atHashString& clipDictionaryName);
	void SetClipDictionaryIndexIndex(s32 clipDictionaryIndex) { Assign(m_clipDictionaryIndex, clipDictionaryIndex); }
	inline s32 GetClipDictionaryIndex() const { return m_clipDictionaryIndex; }

#if ENABLE_BLENDSHAPES
	void SetBlendShapeFile(const atHashString& blendShapeName);
	void SetBlendShapeFileIndex(s32 blendShapeFileIndex) { Assign(m_blendShapeFileIndex, blendShapeFileIndex); }
	inline s32 GetBlendShapeFileIndex() const { return m_blendShapeFileIndex; }
#endif // ENABLE_BLENDSHAPES

	void SetExpressionDictionaryIndex(const atHashString& expressionDictionaryName);
	void SetExpressionDictionaryIndexIndex(s32 index) { Assign(m_expressionDictionaryIndex, index); }
	inline s32 GetExpressionDictionaryIndex() const { return m_expressionDictionaryIndex; }

	void SetExpressionHash(const char* pExpressionName) { m_expressionHash.SetFromString(pExpressionName); }
	void SetExpressionHashHash(atHashString hashString) { m_expressionHash = hashString; }
	inline atHashString GetExpressionHash() const { return m_expressionHash; }

	void SetPoseMatcherFile(const atHashString& poseMatcherName);
	void SetPoseMatcherFileIndex(s32 poseMatcherFileIndex) { Assign(m_poseMatcherFileIndex, poseMatcherFileIndex); }
	inline s32 GetPoseMatcherFileIndex() { return m_poseMatcherFileIndex; }

	void SetPoseMatcherProneFile(const atHashString& poseMatcherName);
	void SetPoseMatcherProneFileIndex(s32 poseMatcherFileIndex) { Assign(m_poseMatcherProneFileIndex, poseMatcherFileIndex); }
	inline s32 GetPoseMatcherProneFileIndex() { return m_poseMatcherProneFileIndex; }

	void SetCreatureMetadataFile(const atHashString& creatureMetadataName);
	void SetCreatureMetadataFileIndex(s32 creatureMetadataFileIndex)
	{ 
		Assert(creatureMetadataFileIndex < BIT(14));
		m_creatureMetadataFileIndex = creatureMetadataFileIndex;
		FastAssert(m_creatureMetadataFileIndex == creatureMetadataFileIndex);
	}
	inline s32 GetCreatureMetadataFileIndex() { return m_creatureMetadataFileIndex; }

	void SetPhysicsArch(phArchetypeDamp *arch) { m_pPhysicsArch = arch; }
	phArchetypeDamp *GetPhysicsArch() const { return m_pPhysicsArch; }

	void SetHasPhysicsInDrawable(bool bHasPhysicsInDrawable) { m_bHasPhysicsInDrawable = bHasPhysicsInDrawable; }
	bool GetHasPhysicsInDrawable() const { return m_bHasPhysicsInDrawable; }

	bool GetAutoStartAnim() const { return m_bAutoStartAnim != 0; }
	void SetAutoStartAnim(bool bAutoStartAnim) { m_bAutoStartAnim = bAutoStartAnim; }

	void ResetPhysicsArchRefs() { m_physicsArchRefs = 0; }
	void AddPhysicsArchRef() { m_physicsArchRefs++; }
	void RemovePhysicsArchRef() { m_physicsArchRefs--; }
	u16 GetNumPhysicsArchRefs() const { return m_physicsArchRefs; }

	const Vec4V& GetAvoidancePoint() {return m_AIAvoidancePoint;}
	void SetAvoidancePoint(Vec4V_In vPoint) { m_AIAvoidancePoint = vPoint;}

protected:
	Vec4V m_AIAvoidancePoint;
	
	atHashString m_expressionHash;			// +0
	phArchetypeDamp* m_pPhysicsArch;		// +4

	s16 m_clipDictionaryIndex;				// +8
	s16 m_blendShapeFileIndex;				// +10
	s16 m_expressionDictionaryIndex;		// +12
	u16 m_physicsArchRefs;					// +14
	
	s8 m_poseMatcherFileIndex;				// +16
	s8 m_poseMatcherProneFileIndex;			// +17
	s16 m_creatureMetadataFileIndex	: 14;	// +19
	u16 m_bAutoStartAnim			: 1;
	u16 m_bHasPhysicsInDrawable		: 1;
};


#if !__SPU
CompileTimeAssertSize(fwDynamicArchetypeComponent, 48, 48);
#endif


} // namespace rage

#endif // ENTITY_DYNAMICARCHETYPE_H
