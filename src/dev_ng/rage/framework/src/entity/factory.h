#ifndef ENTITY_FACTORY_H
#define ENTITY_FACTORY_H

#include "atl/array.h"
#include "atl/map.h"
#include "fwtl/pool.h"

namespace rage {

//special ID which is accessible globally
namespace fwFactory {
	enum	{
		GLOBAL				= (0xffff),
		EXTRA				= (0xf000)
	};
}


/** PURPOSE: This is the base class for any factory.
 *  On its own, it doesn't have much functionality other than the option to create a 
 *  an object using a generic Create() function. Using the subclasses
 *  is typically preferred.
 *
 *  See fwFactory for the main class.
 */
template <class T>
class fwFactoryBase {
public:

	virtual ~fwFactoryBase() {}

	// PURPOSE: Create an instance of an subclass of T.
	virtual T *CreateBaseItem(s32 /*storageBlockId*/) = 0;

	// PURPOSE: Return the number of used objects in this factory
	virtual u32 GetCount(s32 /*storageBlockId*/)  = 0;

	// PURPOSE: Return the capacity of this factory
	virtual u32 GetCapacity(s32 /*storageBlockId*/)  = 0;

	// PURPOSE: Allocate an array to handle a the archetypes of this type for an entire .ityp file (and tag it with file ID)
	virtual void	AddStorageBlock(s32 /*storageBlockId*/, u32 /*size*/) = 0;

	// free the allocation tagged with the given ID
	virtual void	FreeStorageBlock(s32) = 0;

	// for each element in a storage block, issue callback with the element as argument
	virtual void		ForAllUsed(s32 storageBlockId, void callback(T*)) = 0;

#if __BANK
	virtual const char *GetDebugName() const = 0;
#endif // __BANK
};

/** PURPOSE: This is the primary class that holds most of the factory functionality.
 *  It stores a pre-defined number of silos of objects but the silo size is specified during runtime, allows for access to each one of them,
 *  allocation of new elements, deletion of silos.
 *
 *  Note also that all elements in the store will be instantiated upon creation of the factory.
 */
template<class Type, class Base = Type>
class fwFactoryWithPoolStore : public fwFactoryBase< Base > {
public:
	void Init(const int /*count*/)				{ }  

	//virtual Base *CreateBaseItem(s32 mapTypeDefIdx)		{ return (Base *) CreateItem(mapTypeDefIdx);  }
	Type *CreateItem(s32 );

	void Shutdown()							{ m_pStoragePool->DeleteAll();}
	void Reset()							{ m_pStoragePool->Reset(); }

#if __BANK
	virtual const char *GetDebugName() const;
#endif // __BANK

	// spoof the required interface from factory base for now
	virtual Base*	CreateBaseItem(s32 /*storageBlockId*/);
	virtual u32		GetCount(s32 /*storageBlockId*/)  { return(m_pStoragePool->GetNoOfUsedSpaces()); }
	virtual u32		GetCapacity(s32 /*storageBlockId*/)  { return(m_pStoragePool->GetSize()); }
	virtual void	AddStorageBlock(s32 /*storageBlockId*/, u32 /*size*/) {}
	virtual void	FreeStorageBlock(s32) {}
	virtual void	ForAllUsed(s32 /*storageBlockId*/, void /*callback*/(Base*)) {}

	void	SetStoragePool(fwPool<Type>* pPool) { m_pStoragePool = pPool; }

protected:

	fwPool<Type>*					m_pStoragePool;
};

template<class Type, class Base>
Base* fwFactoryWithPoolStore<Type,Base>::CreateBaseItem(s32 storageBlockId) 
{
	if (GetCount(storageBlockId) == GetCapacity(storageBlockId))
	{
		return(NULL);
	}

	return((Base*)(rage_placement_new (m_pStoragePool->New()) Type() ));  // placement new required
}

/** PURPOSE: This is the primary class that holds most of the factory functionality.
 *  It stores a pre-defined number of silos of objects but the silo size is specified during runtime, allows for access to each one of them,
 *  allocation of new elements, deletion of silos.
 *
 *  Note also that all elements in the store will be instantiated upon creation of the factory.
 */
template<class Type, class Base = Type>
class fwFactoryWithDynamicStore : public fwFactoryBase< Base > {
public:
	void Init(const int /*count*/)				{ m_perFileStores.Create(100);}    // want to know how many _files_ do we need store info for?

	virtual Base *CreateBaseItem(s32 mapTypeDefIdx)		{ return (Base *) CreateItem(mapTypeDefIdx);  }
	Type *CreateItem(s32 mapTypeDefIdx);

	void Shutdown()							{ m_perFileStores.Reset();}
	void Reset()							{ m_perFileStores.Kill(); }

	virtual void		AddStorageBlock(s32 storageBlockId, u32 size);
	virtual void		FreeStorageBlock(s32 storageBlockId);

	virtual void		ForAllUsed(s32 storageBlockId, void callback(Base*));

#if __BANK
	virtual const char *GetDebugName() const;
#endif // __BANK

	virtual u32 GetCount(s32 storageBlockId)					{ return(GetStorageBlock(storageBlockId)->GetCount()); }
	virtual u32 GetCapacity(s32 storageBlockId)					{ return(GetStorageBlock(storageBlockId)->GetCapacity()); }
	Type& GetEntry(s32 storageBlockId, u32 index)				{ FastAssert(index < GetCount(storageBlockId)); return((*GetStorageBlock(storageBlockId))[index]); }

	// PURPOSE: Return the index of any particular instance. Must have been
	// created using GetNextItem() (or Create()).
	u32 GetElementIndex(s32 storageBlockId, Type* pElement);

protected:

	atArray<Type>*		GetStorageBlock(s32 storageBlockId);

	// per file storage of the archetypes
	atMap< s32, atArray< Type >* >		m_perFileStores;
};

template<class Type, class Base>
inline u32 fwFactoryWithDynamicStore<Type,Base>::GetElementIndex(s32 storageBlockId, Type* pElement) {

	atArray<Type>* pBlock = GetStorageBlock(storageBlockId);
	s32 index = ptrdiff_t_to_int(pElement - pBlock->GetElements());
	Assertf(index >=0 && index < pBlock->GetCount(), "Try to access element outside storage block");
	return index;
}

template<class Type, class Base>
void fwFactoryWithDynamicStore<Type,Base>::AddStorageBlock(s32 storageBlockId, u32 size) {

	Assert(storageBlockId > -1);
	Assert(size > 0);

	//printf("%s : %d : %d\n", GetDebugName(), storageBlockId, size);

	atArray<Type>** ppAlloc = m_perFileStores.Access(storageBlockId);
	Assertf(ppAlloc == NULL, "file id : %d already has a storage block allocated!", storageBlockId);
	if (!ppAlloc){
		atArray<Type>* pNewAlloc = rage_new(atArray<Type>);
		Assert(pNewAlloc);
		pNewAlloc->Reserve(size);

		m_perFileStores.Insert(storageBlockId, pNewAlloc);
	}
}

template<class Type, class Base>
void fwFactoryWithDynamicStore<Type,Base>::FreeStorageBlock(s32 storageBlockId) {

	Assert(storageBlockId > -1);

	atArray<Type>** ppAlloc = m_perFileStores.Access(storageBlockId);
	if (ppAlloc && *ppAlloc){
		delete (*ppAlloc);
		m_perFileStores.Delete(storageBlockId);
	}
}

template<class Type, class Base>
void fwFactoryWithDynamicStore<Type,Base>::ForAllUsed(s32 storageBlockId, void callBack(Base*) ) {

	Assert(storageBlockId > -1);

	atArray<Type>** ppAlloc = m_perFileStores.Access(storageBlockId);
	if (ppAlloc && *ppAlloc){
		for(u32 i = 0; i < (*ppAlloc)->GetCount(); i++){
			callBack((Base*) &(**ppAlloc)[i]);				// NASTY CAST! because the templates are fucked. extensions are not archetypes!
		}
	}
}


template<class Type, class Base>
atArray<Type>* fwFactoryWithDynamicStore<Type,Base>::GetStorageBlock(s32 storageBlockId) {

	Assert(storageBlockId > -1);

	atArray<Type>** ppAlloc = m_perFileStores.Access(storageBlockId);
	Assertf(ppAlloc, "file id : %d has no storage block allocated!", storageBlockId);
	Assertf(*ppAlloc, "file id : %d has invalid storage block!", storageBlockId);
	if (ppAlloc && *ppAlloc){
		return(*ppAlloc);
	}
	return(NULL);
}

template<class Type, class Base>
Type* fwFactoryWithDynamicStore<Type,Base>::CreateItem(s32 storageBlockId)
{ 
	atArray< Type >* pBlock = GetStorageBlock(storageBlockId);

	Assertf( pBlock, "Invalid storage block: blockId=%d", storageBlockId );
	Assertf( pBlock->GetCount() < pBlock->GetCapacity(), "Storage block full! blockId=%d, size=%d", storageBlockId, pBlock->GetCapacity());

	Type& ret = pBlock->Append();
	return (&ret);
}


/** PURPOSE: This class manages the registration and accessing to related factory classes.
 */
template <class T>
class fwFactoryManager
{
public:
	static void Init(const int maxFactoryId);

	static void RegisterFactory(const int factoryId, fwFactoryBase<T>& factory);
	
	// PURPOSE: Look up a factory by its ID that has previously been registered with RegisterFactory.
	static fwFactoryBase<T>* GetFactory(const int factoryId)	{ return ms_Factories[factoryId]; }
	static u32				GetCount(void) { return ms_Factories.GetCount(); }

protected:
	static atArray< fwFactoryBase<T> *>	ms_Factories;
};

template <class T>
atArray< fwFactoryBase<T>* >	fwFactoryManager<T>::ms_Factories;

template <class T>
void fwFactoryManager<T>::Init(const int maxFactoryId) {
	ms_Factories.Resize( maxFactoryId + 1 );
	sysMemSet( ms_Factories.GetElements(), 0, (maxFactoryId + 1) * sizeof(fwFactoryBase<T>*) );
}

template <class T>
void fwFactoryManager<T>::RegisterFactory(const int factoryId, fwFactoryBase<T>& factory) {
	Assert(factoryId < ms_Factories.GetCount());
	Assertf( ms_Factories[ factoryId ] == NULL, "Factory %d registered twice", factoryId );
	ms_Factories[ factoryId ] = &factory;
}

// ---debug

#if __BANK

template<class Type, class Base>
const char *fwFactoryWithDynamicStore<Type,Base>::GetDebugName() const
{
#if defined(_CPPRTTI)
	const char *name = (typeid(Type).name());

	// PS3 sometimes adds a letter and then a number at the beginning. Skip.
	if (*name && name[1] >= '0' && name[1] <= '9')
		name++;

	// PS3 adds useless garbage to the beginning of the typename. Skip.
	while (*name >= '0' && *name <= '9')
		name++;

	return name;
#else // defined(_CPPRTTI)
	return "[Unknown factory]";
#endif // defined(_CPPRTTI)
}

template<class Type, class Base>
const char *fwFactoryWithPoolStore<Type,Base>::GetDebugName() const
{
#if defined(_CPPRTTI)
	const char *name = (typeid(Type).name());

	// PS3 sometimes adds a letter and then a number at the beginning. Skip.
	if (*name && name[1] >= '0' && name[1] <= '9')
		name++;

	// PS3 adds useless garbage to the beginning of the typename. Skip.
	while (*name >= '0' && *name <= '9')
		name++;

	return name;
#else // defined(_CPPRTTI)
	return "[Unknown factory]";
#endif // defined(_CPPRTTI)
}

#endif // __BANK
} // namespace rage




#endif // !defined ENTITY_FACTORY_H
