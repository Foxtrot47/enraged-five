
#include "archetypestreamingmodule.h"

#include "archetype.h"
#include "archetypemanager.h"

#include "diag/art_channel.h"
#include "fwscene/stores/drawablestore.h"
#include "fwscene/stores/dwdstore.h" 
#include "fwscene/stores/fragmentstore.h"
#include "fwscene/stores/txdstore.h" 
#include "fwsys/metadatastore.h"
#include "streaming/streaming_channel.h"



namespace rage {


fwArchetypeStreamingModule::fwArchetypeStreamingModule(int numArchetypes)
: strStreamingModule("ModelInfo", -1, numArchetypes, false, false, 0, 1024)
{
}

#if !__FINAL
const char* fwArchetypeStreamingModule::GetName(strLocalIndex index) const
{
	return fwArchetypeManager::GetArchetypeName(index.Get());
}
#endif

strLocalIndex fwArchetypeStreamingModule::Register(const char* /*name*/)
{
	Assert(false);
	return strLocalIndex(-1);
}

void fwArchetypeStreamingModule::AddRef(strLocalIndex index, strRefKind /*refKind*/)
{
	fwArchetype* pModelInfo = fwArchetypeManager::GetArchetype(index.Get());
	pModelInfo->AddRef();
}
void fwArchetypeStreamingModule::RemoveRef(strLocalIndex index, strRefKind /*refKind*/)
{
	fwArchetype* pModelInfo = fwArchetypeManager::GetArchetype(index.Get());
	Assert(pModelInfo);
	pModelInfo->RemoveRef();
}
int fwArchetypeStreamingModule::GetNumRefs(strLocalIndex index) const
{
	fwArchetype* pModelInfo = fwArchetypeManager::GetArchetype(index.Get());
	return pModelInfo->GetNumRefs();
}
int fwArchetypeStreamingModule::GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const 
{
	s32 count=0;
	fwArchetype* pModelInfo = fwArchetypeManager::GetArchetype(index.Get());

	Assert(pModelInfo);

	strIndex streamingIndex;

	switch (pModelInfo->GetDrawableType()) {
		case fwArchetype::DT_DRAWABLEDICTIONARY:
		{
			strLocalIndex drawableDictFileIndex = strLocalIndex(pModelInfo->GetDrawDictIndex());
			Assertf(drawableDictFileIndex.IsValid(), "archetype <%s> is missing drawable dictionary '%s'",pModelInfo->GetModelName(), g_DwdStore.GetName(drawableDictFileIndex));
			AddDependencyOutput(pIndices, count, g_DwdStore.GetStreamingIndex(drawableDictFileIndex), indexArraySize, streamingIndex);
		}
		break;

		case fwArchetype::DT_FRAGMENT:
		{
			strLocalIndex fragmentFileIndex = strLocalIndex(pModelInfo->GetFragmentIndex());
			Assert(fragmentFileIndex.IsValid());
			Assertf(g_FragmentStore.IsObjectInImage(fragmentFileIndex), "archetype <%s> is missing frag '%s'",pModelInfo->GetModelName(), g_FragmentStore.GetName(fragmentFileIndex));
			AddDependencyOutput(pIndices, count, g_FragmentStore.GetStreamingIndex(fragmentFileIndex), indexArraySize, streamingIndex);
		}
		break;

		case fwArchetype::DT_DRAWABLE:
		{
			strLocalIndex drawableFileIndex = strLocalIndex(pModelInfo->GetDrawableIndex());
			Assert(drawableFileIndex.IsValid());
			Assertf(g_DrawableStore.IsObjectInImage(drawableFileIndex), "archetype <%s> is missing drawable '%s'", pModelInfo->GetModelName(), g_DrawableStore.GetName(drawableFileIndex));
			AddDependencyOutput(pIndices, count, g_DrawableStore.GetStreamingIndex(drawableFileIndex), indexArraySize, streamingIndex);
		}
		break;

		case fwArchetype::DT_UNINITIALIZED:
		case fwArchetype::DT_ASSETLESS:
		case fwArchetype::DT_COUNT:
			break;//rage/gta5/dev/rage/framework/src/streaming/packfilemanager.cpp
	}

	return count;
} 

void fwArchetypeStreamingModule::Remove(strLocalIndex /*index*/)
{
	Assert(0);
/*	fwArchetype* pModelInfo = fwArchetypeManager::GetModelInfo(index);
	Assertf(pModelInfo, "Trying to remove a model that doesn't exist");

	//Assert(pModelInfo->GetHasLoaded());

	// The following bit of code tells the PopulationStreaming a ped/vehicle has been loaded out.
	gPopStreaming.ModelHasBeenStreamedOut(index);

	pModelInfo->SetHasLoaded(false);

	// clean up the allocated stuff which we add on top of the base drawable and fragments
	switch (pModelInfo->GetDrawableType()) {
		case fwArchetype::DT_FRAGMENT:
			pModelInfo->DeleteMasterFragData();				// this will clear drawable too...
			break;

		case fwArchetype::DT_DRAWABLE:
		case fwArchetype::DT_DRAWABLEDICTIONARY:
			pModelInfo->DeleteMasterDrawableData();
			break;

		case fwArchetype::DT_UNINITIALIZED:
			break;
 	}*/
}

// the dependencies for this model info are satisfied. Initialise the master object and register with population
bool fwArchetypeStreamingModule::Load(strLocalIndex index, void* UNUSED_PARAM(object), int UNUSED_PARAM(size))
{
	fwArchetype* pModelInfo = fwArchetypeManager::GetArchetype(index.Get());

	switch (pModelInfo->GetDrawableType())
	{
		case fwArchetype::DT_DRAWABLEDICTIONARY:
		{
			strLocalIndex drawDictIndex = /*(s16)*/ strLocalIndex(pModelInfo->GetDrawDictIndex());
			Assert(drawDictIndex.IsValid());
			Dwd* pDict = g_DwdStore.Get(drawDictIndex);
			Assert(pDict);

			int nDrawableIdx = pDict->LookupLocalIndex(pModelInfo->GetHashKey());
			if(!artVerifyf(nDrawableIdx != -1, "Drawable %s is not in dictionary %s (Try a Re-Export)", pModelInfo->GetModelName(), g_DwdStore.GetName(drawDictIndex.Get())))
			{

				//add some debug info so the game side can ignore this object
				pModelInfo->FlagModelAsMissing();

				return false;
			}

			pModelInfo->SetDrawDictDrawIndex(nDrawableIdx);
		}
		return true;

		case fwArchetype::DT_FRAGMENT:
		{
			ASSERT_ONLY(strLocalIndex fragmentIndex = /*(s16)*/ strLocalIndex(pModelInfo->GetFragmentIndex());)
			Assert(fragmentIndex.IsValid());
			Assert(g_FragmentStore.Get(fragmentIndex));
		}
		return true;

		case fwArchetype::DT_DRAWABLE:
		{
			ASSERT_ONLY(strLocalIndex drawableIndex = /*(s16)*/ strLocalIndex(pModelInfo->GetDrawableIndex());)
			Assert(drawableIndex.IsValid());
			Assert(g_DrawableStore.Get(drawableIndex));
		}
		return true;

		case fwArchetype::DT_ASSETLESS:
			return true;

		case fwArchetype::DT_UNINITIALIZED:
		case fwArchetype::DT_COUNT:
			break;
	}

	streamAssertf(false, "Loading archetype %s with asset-less type %d", pModelInfo->GetModelName(), pModelInfo->GetDrawableType());

	return false;
}


} // namespace rage

