//
// entity/entity.h : base class for all entities in the world
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef SCENE_ENTITY_H_
#define SCENE_ENTITY_H_

#include "atl/dlist.h"
#include "crskeleton/skeleton.h"
#include "fragment/cache.h"
#include "fwanimation/animdefines.h"
#include "fwanimation/directorcomponent.h"
#include "grblendshapes/blendshapes_config.h"
#include "grmodel/model.h"
#include "physics/debugEvents.h"

#if !__SPU
#include "fwanimation/move.h"
#endif // !__SPU
#include "fwscene/lod/LodTypes.h"
#include "fwscene/world/EntityContainer.h"
#include "fwscene/world/WorldLimits.h"
#include "fwutil/flags.h"
#include "data/autoid.h"
#include "system/bit.h"

#include "vector/matrix34.h"
#include "vector/vector3.h"

#include "extensiblebase.h"
#include "transform.h"
#include "entity/archetype.h"
#include "entity/archetypemanager.h"
#include "entity/dynamicentitycomponent.h"

#define ENABLE_FORCE_MATRICES 1
#define ENABLE_MATRIX_MEMBER 1

namespace rage {

#if __SPU
#define PPUVIRTUAL
#else
#define PPUVIRTUAL virtual
#endif

class bkGroup;
class crCreature;
class crmtRequest;
class crmtRequestIk;
class crSkeletonData;
class crProperty;
class fragInst;
class fwAnimFrameRagDoll;
class fwMove;
class grbTargetManager;
class phInst;
class fwDrawData;
class fwEntityDef;
class fwArchetype;
class fwPropInstanceListDef;
class fwGrassInstanceListDef;
class fwAudioEntity;
class fwExpressionSet;

// intended to make some of the compile time asserts more readable

#if RAGE_INSTANCED_TECH
#define FWENTITY_INTENDED_SIZE (68)
#else
#if ENABLE_MATRIX_MEMBER
#define FWENTITY_INTENDED_SIZE (128)
#else
#define FWENTITY_INTENDED_SIZE (64)
#endif
#endif


//
// PURPOSE
//	fwEntity is the base class for all entities in the world.  
class fwEntity : public fwExtensibleBase
{
	DECLARE_RTTI_DERIVED_CLASS(fwEntity, fwExtensibleBase);

public:
	fwEntity();
	PPUVIRTUAL ~fwEntity();

	virtual void InitTransformFromDefinition(fwEntityDef* definition, fwArchetype* archetype);
	virtual void InitExtensionsFromDefinition(fwEntityDef* definition, fwArchetype* archetype, s32 mapDataDefIndex);
	virtual void InitEntityFromDefinition(fwEntityDef* definition, fwArchetype* archetype, s32 mapDataDefIndex);

	//Special overloads for entity batch entities. Asserts by default so it's clear these shouldn't be used for non-batch entities.
	virtual void InitEntityFromDefinition(const fwPropInstanceListDef* definition, fwArchetype* archetype, s32 mapDataDefIndex);
	virtual void InitEntityFromDefinition(fwGrassInstanceListDef* definition, fwArchetype* archetype, s32 mapDataDefIndex);

	// entity flags (must fit in 32 bits)
	enum flags{
		IS_VISIBLE								=	BIT(0),					// if set this entity will be ignored in visibility
		IS_SEARCHABLE							=	BIT(1),					// if set this entity will be ignored in world searches

		HAS_OPAQUE								=	BIT(2),					// draw object in opaque pass
		HAS_ALPHA								=	BIT(3),					// draw object in alpha pass
		HAS_DECAL								=	BIT(4),					// draw object in decal phase
		HAS_CUTOUT								=	BIT(5),					// draw object in cutout phase
		HAS_WATER								=	BIT(6),					// draw object in water phase
		HAS_DISPL_ALPHA							=	BIT(7),					// draw object in displacement alpha pass
		HAS_PRERENDER							=	BIT(8),					// Object has a pre-render effects attached to it
		IS_LIGHT								=	BIT(9),					// this is a light entity

		USE_SCREENDOOR							=	BIT(10),				// this includes MLO objects and small debris (leaves, cans etc)
		SUPPRESS_ALPHA							=	BIT(11),				// used for Skipping any alpha in the entity
		FORCE_ALPHA								=	BIT(12),				// Force Alpha
		RENDER_SMALL_SHADOW						=	BIT(13),				// don't cull shadow caster based on screen size
		OK_TO_PRERENDER							=	BIT(14),				// modify to suppress prerender later

		HAS_NON_WATER_REFLECTION_PROXY_CHILD	=	BIT(15),				// has a LOD child which renders in water reflection but is not a proxy
		HAS_HYBRID_ALPHA						=	BIT(15),				// SHARED(15) - has a solid part inside an alpha object
		IS_DYNAMIC								=	BIT(16),				// this is a dynamic entity (i.e. it can move in space)
		IS_FIXED								=	BIT(17),				// physics is fixed in position
		IS_FIXED_BY_NETWORK						=	BIT(18),				// this is used during a network game to stop clones having their physics updated between sync updates
		SPAWN_PHYS_ACTIVE						=	BIT(19),				// when adding to world, ensure that this object has activated physics
	
		IS_TIMED_ENTITY							=	BIT(20),				// If true, this is uses a model that's a timed entity

		DONT_STREAM								=	BIT(21),				// tell the streaming not to stream me
		LOW_PRIORITY_STREAM						=	BIT(22),				// set that this object is unimportant, if streaming is having problems

		HAS_PHYSICS_DICT						=	BIT(23),
		REMOVE_FROM_WORLD						=	BIT(24),				// remove this entity next time it should be processed

		FORCED_ZERO_ALPHA						=	BIT(25),				// this entity has forced alpha=0 (e.g. by the script)

		DRAW_LAST								=	BIT(26),				// entity is always pushed back in draw order as late as possible
		DRAW_FIRST_SORTED						=	BIT(27),				// entity is drawn first (only in sorted drawlists, i.e. SORT_ASCENDING or SORT_DESCENDING)

		NO_INSTANCED_PHYS						=	BIT(28),
		HAS_HD_TEX_DIST							=	BIT(29),				// entity has a texture dictionary that supports streaming in HD top mip

		PROTECT_ARCHETYPE						=	BIT(30),				// this entity has protected it's archetype with a ref & needs handled correctly on destruction
		HAS_ALPHA_SHADOW						=	BIT(31)
	};

	enum protectedFlags{
		IS_FIXED_UNTIL_COLLISION	=	BIT(0),		// this is used by script created entities - they are static until the collision is loaded below them
		USES_COLLISION				=	BIT(1),		// DO NOT SET THIS DIRECTLY! You can query it, but use CEntity::EnableCollision() and CEntity::DisableCollision().
		HAS_SLOD_REF				=	BIT(2),
		HAS_FPV						=	BIT(3),		// Is this a first person view object?
	};

	// Base render flags.
	// Applications can add their own bits, but they need to be shifted by
	// fwRenderFlag_USER_FLAG_SHIFT.
	enum fwRenderFlags {
		// All application-specific bits must be shifted by fwRenderFlag_USER_FLAG_SHIFT to
		// avoid a collision with the RAGE render flags.
		fwRenderFlag_USER_FLAG_SHIFT	= 10,
		fwRenderFlag_SUBPHASE_MASK		= (1<<fwRenderFlag_USER_FLAG_SHIFT) - 1
	};

	// PURPOSE: Set the type of object this entity is (building, vehicle etc)
	void SetType(int type) {Assign(m_type, type); UpdateRenderFlags(); }
	// PURPOSE: Get the type of object this entity is
	int GetType() const {return m_type;}

	const fwArchetype *GetArchetype() const { return m_pArchetype; }

	// PURPOSE: Get model identifier for this entity
	// - DO NOT CACHE : the model index of a streamed archetype is not necessarily constant at runtime
	inline u32 GetModelIndex() const { FastAssert(m_pArchetype); return (m_pArchetype->GetStreamSlot().Get()); }

	// Set model identifier for this entity
	PPUVIRTUAL void SetModelId(fwModelId modelId) { m_pArchetype = fwArchetypeManager::GetArchetype(modelId.GetModelIndex());}

	// Get model identifier for this entity
	fwModelId GetModelId() const		{ return (fwArchetypeManager::LookupModelId(m_pArchetype)); }

	//void SetArchetype(fwArchetype* pArchetype) { m_pArchetype = pArchetype; }
	bool IsArchetypeSet(void) const { return(m_pArchetype != NULL); }
	void ProtectStreamedArchetype(void);
	void UnprotectStreamedArchetype(void);

	u32 GetModelNameHash() const { return (m_pArchetype ? m_pArchetype->GetModelNameHash() : 0); }

#if !__NO_OUTPUT
	inline const char* GetModelName() const { return (m_pArchetype!=NULL ? m_pArchetype->GetModelName() : ""); }
	virtual const char* GetDebugName() const { return GetModelName(); }
#else
	inline const char* GetModelName() const { return ""; }
#endif
	
	// PURPOSE: Return all the flags for this entity
	inline u32 GetBaseFlags() const {return (m_baseFlags);}
	// PURPOSE: Return if a flag is set
 	inline bool IsBaseFlagSet(flags flag) const {return ((m_baseFlags & flag) != 0);}
// 	// PURPOSE: Set a flag or flags without affecting others
	inline void SetBaseFlag(flags flag) {BANK_ONLY(bool bOldVal = IsBaseFlagSet(flag);) m_baseFlags |= flag; BANK_ONLY(LogBaseFlagChange(flag, false, true, bOldVal));}
// 	// PURPOSE: Set all the flags
 	inline void SetAllBaseFlags(flags newFlags) {m_baseFlags = newFlags;}
// 	// PURPOSE: Clear a flag or flags without affecting others
 	inline void ClearBaseFlag(u32 flag) {BANK_ONLY(bool bOldVal = IsBaseFlagSet((flags)flag);) m_baseFlags &= ~flag; BANK_ONLY(LogBaseFlagChange(flag, false, false, bOldVal));}
	// PURPOSE : assign value to a flag
	inline void AssignBaseFlag(flags flag, bool bVal) { BANK_ONLY(bool bOldVal = IsBaseFlagSet(flag);) if (bVal) {m_baseFlags |= flag;} else {m_baseFlags &= ~flag;} BANK_ONLY(LogBaseFlagChange(flag, false, bVal, bOldVal)); }

#if __BANK
	virtual void LogBaseFlagChange(s32 UNUSED_PARAM(nFlag), bool UNUSED_PARAM(bProtected), bool UNUSED_PARAM(bNewVal), bool UNUSED_PARAM(bOldVal)) {};
#endif

	// PURPOSE: Return all the render flags for this entity
	inline u16 GetRenderFlags() const {return (m_RenderFlags);}
	// 	PURPOSE: Set a render flag or flags without affecting others
	inline void SetRenderFlag(u16 flags) {m_RenderFlags |= flags;}
	// 	PURPOSE: Set all the render flags
	inline void SetAllRenderFlags(u16 flags) {m_RenderFlags = flags;}
	// 	PURPOSE: Clear a render flag or flags without affecting others
	inline void ClearRenderFlags(u16 flags) {m_RenderFlags &= ~flags;}
	// PURPOSE : Set or clear certain render flags
	inline void AssignRenderFlag(u16 flags, bool bVal) { if (bVal) {m_RenderFlags |= flags;} else {m_RenderFlags &= ~flags;}}

	inline bool IsTimedEntity() const								{ return (m_baseFlags & IS_TIMED_ENTITY) != 0; }


	// Dynamic Entity Component

	// PURPOSE: Return the dynamic component, if available
	inline fwDynamicEntityComponent *GetDynamicComponent() const	{ return m_DynamicEntityComponent; }
	phCollider* GetCurrentColliderFromSimulator();
	const phCollider* GetCurrentColliderFromSimulator() const;
#if __DEV
	void ValidateCurrentCollider() const;
#endif // __DEV
	inline phCollider* GetCurrentCollider() const { DEV_ONLY(ValidateCurrentCollider();) return m_DynamicEntityComponent ? m_DynamicEntityComponent->GetCollider() : NULL; }
	inline phCollider* GetCurrentColliderFast() const { DEV_ONLY(ValidateCurrentCollider();) return m_DynamicEntityComponent->GetCollider(); }

	// PURPOSE: Create a dynamic component if there isn't one already.
	inline fwDynamicEntityComponent *CreateDynamicComponentIfMissing();

	inline fwAttachmentEntityExtension *GetAttachmentExtension()				{ return GetDynamicComponent() ? GetDynamicComponent()->GetAttachmentExtension() : NULL; }
	inline const fwAttachmentEntityExtension *GetAttachmentExtension() const	{ return GetDynamicComponent() ? GetDynamicComponent()->GetAttachmentExtension() : NULL; }
	inline fwAttachmentEntityExtension &GetAttachmentExtensionRef();
	inline fwAttachmentEntityExtension const &GetAttachmentExtensionRef() const;
	inline fwAttachmentEntityExtension *CreateAttachmentExtensionIfMissing() { return CreateDynamicComponentIfMissing()->CreateAttachmentExtensionIfMissing(this); }
	inline void DeleteAttachmentExtension() { Assert(GetDynamicComponent()); GetDynamicComponent()->DeleteAttachmentExtension(); }

	inline void SetAnimatedVelocity(Vector3::Vector3Param vAnimatedVelocity);
	inline Vector3 GetAnimatedVelocity() const;

	inline void SetSkeleton(crSkeleton *skeleton);
	inline crSkeleton *GetSkeleton() const;
	inline crSkeleton *GetEntitySkeleton() const;
	inline const crSkeletonData& GetSkeletonData() const;
	void DeleteSkeleton();

#if !__NO_OUTPUT
	size_t GetSkeletonSize() const;
	void PrintSkeletonSummary() const;
#endif

	void DeleteAnimFrame();

	inline fwAnimDirector *GetAnimDirector() const;

	PPUVIRTUAL void GetGlobalMtx(int boneIdx, Matrix34& matrix) const;
	inline void SetGlobalMtx( int boneIdx, const Matrix34& matrix );
	PPUVIRTUAL const Matrix34& GetThirdPersonSkeletonObjectMtx(int boneIdx) const;
	inline const Matrix34& GetLocalMtx(int boneIdx) const;

	inline const Matrix34& GetObjectMtx(int boneIdx) const;
	inline Matrix34& GetLocalMtxNonConst(int boneIdx);	
	inline Matrix34& GetObjectMtxNonConst(int boneIdx);

#if ENABLE_FRAG_OPTIMIZATION
	inline u32 GetBoneCount() const;
	inline void ComputeObjectMtx(int boneIdx, Mat34V_InOut matrix) const;		
#else
	inline Matrix34& GetLocalMtx(int boneIdx) { return GetLocalMtxNonConst(boneIdx); }
	inline Matrix34& GetObjectMtx(int boneIdx) { return GetObjectMtxNonConst(boneIdx); }
#endif

	PPUVIRTUAL fragInst* GetFragInst() const;

	inline float GetAnimatedAngularVelocity() const;
	inline void SetAnimatedAngularVelocity(float velocity);

	inline float GetForceAddToBoundRadius() const;
	inline void SetForceAddToBoundRadius(float forceAdd);

	inline crCreature *GetCreature() const;
	inline void SetCreature(crCreature *creature);

#if ENABLE_BLENDSHAPES
	void InitTargetManager();
	void ShutdownTargetManager();
#endif // ENABLE_BLENDSHAPES

	void AddExtraDofsCreatureComponent(const crFrameData& frameData);

	inline grbTargetManager *GetTargetManager() const;
	inline void SetTargetManager(grbTargetManager *targetMgr);

	inline Vector3 GetPreviousPosition() const;
	inline void SetPreviousPosition(Vector3::Vector3Param vecPos);

	bool IsEntityAParentAttachment(const fwEntity *entity) const;
	inline bool GetIsAttached() const;
	inline fwEntity* GetAttachParent() const;
	inline fwEntity* GetAttachParentForced() const;
	inline fwEntity* GetSiblingAttachment() const;
	inline fwEntity* GetChildAttachment() const;

	inline int GetNoCollisionFlags() const;
	inline void SetNoCollisionFlags(u8 flags);
	inline const fwEntity* GetNoCollisionEntity() const;
	inline void SetNoCollisionEntity(fwEntity* pEntity);

	inline void SetNoCollision(fwEntity* pEntity, u8 nTypeFlags);
	inline void PrepareNoCollisionReset();
	inline void ResetNoCollision();

	void WaitForAnyActiveAnimUpdateToComplete(bool excludeMidPhysics=true) const;

	// Move-related functions
#if !__SPU
	virtual fwMove *CreateMoveObject() = 0;
	virtual const fwMvNetworkDefId &GetAnimNetworkMoveInfo() const = 0;

	virtual void SetActivePoseFromSkel();

	virtual float GetUpdateFrequencyThreshold() const		{ return 0.0f; }

	virtual bool InitIkRequest(crmtRequest & /*request*/, crmtRequestIk & /*ikRequest*/)		{ return false; }
	
	virtual bool InitExpressions(crExpressionsDictionary&) { return false; }

	virtual fwExpressionSet* GetExpressionSet() { return NULL; }

	virtual u8 GetMotionTreePriority(fwAnimDirectorComponent::ePhase UNUSED_PARAM(phase)) const { return 0; }

	virtual crFrameFilter *GetLowLODFilter() const { return NULL; }

	virtual fwAudioEntity * GetAudioEntity() const { return NULL; } 
#endif // !__SPU

	// Process a property from animation.
	virtual void HandleFacialAnimEvent( const crProperty* UNUSED_PARAM(pProp) ) {}

	// PURPOSE: Return if a protected flag is set
	inline bool IsProtectedBaseFlagSet(protectedFlags flag) const {return ((m_protectedFlags & flag) != 0);}

	// PURPOSE: Set the transform used by this entity, with extra parameters to cause world/physics updates.
#if ENABLE_MATRIX_MEMBER
	virtual void			SetTransform					(Mat34V_In transform, bool bUpdateGameWorld = true, bool bUpdatePhysics = true, bool bWarp = false);
#else
	virtual void			SetTransform					(fwTransform* transform, bool bUpdateGameWorld = true, bool bUpdatePhysics = true, bool bWarp = false);
#endif	
	
	
#if ENABLE_MATRIX_MEMBER
	// PURPOSE: Returns a const reference to the entity's transform.
	const fwMatrixScaledTransform&		GetTransform					() const;
	void SetTransformScale( const float& xy, const float& z);
	// PURPOSE: Returns a const pointer to the entity's transform.	
	const fwMatrixScaledTransform*		GetTransformPtr					() const;
	// PURPOSE: Returns a const pointer to the entity's transform.		
#else
	// PURPOSE: Returns a const reference to the entity's transform.
	const fwTransform&		GetTransform					() const;
	// PURPOSE: Returns a const pointer to the entity's transform.	
	const fwTransform*		GetTransformPtr					() const;
#endif



	// PURPOSE: Set the matrix used by this entity, with extra parameters to cause world/physics updates.
	virtual void			SetMatrix						(const Matrix34& mat, bool bUpdateGameWorld = true, bool bUpdatePhysics = true, bool bWarp = false);

	// PURPOSE: Returns the entity's transform matrix by value.
	Mat34V_Out				GetMatrix						() const;

	// PURPOSE: Returns the entity's non orthogonal transform matrix (scale applied) by value.
	Mat34V_Out				GetNonOrthoMatrix				() const;

	// PURPOSE: Returns the entity's transform matrix into copy.
	inline void				GetMatrixCopy					(Matrix34& copy) const; 

	// PURPOSE: Returns the entity's non orthogonal transform matrix (scale applied) into copy.
	inline void				GetNonOrthoMatrixCopy			(Matrix34& copy) const; 

	// PURPOSE: Set the entity's position, with extra parameters to cause world/physics updates.
	virtual void			SetPosition						(const Vector3& vec, bool bUpdateGameWorld = true, bool bUpdatePhysics = true, bool bWarp = false);

	// PURPOSE: Set the entity's heading.
	virtual void			SetHeading						(float heading);

	// PURPOSE: Transforms v into world space, returns in result. 
	void					TransformIntoWorldSpace			(Vector3& result, const Vector3& v) const;

	// PURPOSE: Returns a vector transformed into world space.
	Vector3					TransformIntoWorldSpace			(const Vector3& v) const;

	// PURPOSE: Transforms v into world space, returns in result. 
	void					TransformIntoWorldSpace			(Vec3V_InOut result, Vec3V_In v) const;

	// PURPOSE: Returns a vector transformed into world space.
	Vec3V_Out				TransformIntoWorldSpace			(Vec3V_In v) const;

	// PURPOSE: Sets the visibility shape mask: a mask of bits specifying in which render phase subsets we want this entity to be rendered in
	void SetRenderPhaseVisibilityMask(const fwFlags16& value) { m_phaseVisibilityMask = value; }
	// PURPOSE: Gets the visibility shape mask: a mask of bits specifying in which render phase subsets we want this entity to be rendered in
	const fwFlags16& GetRenderPhaseVisibilityMask() const { return m_phaseVisibilityMask; }
	// PURPOSE: Gets the visibility shape mask: a mask of bits specifying in which render phase subsets we want this entity to be rendered in
	fwFlags16& GetRenderPhaseVisibilityMask() { return m_phaseVisibilityMask; }

	// PURPOSE: Sets the visibility entity subset, that is a arbitrary game-specific id of the 'kind' of an entity. Render phases may cull entities based on this.
	void SetVisibilityType(const u16 value) { m_visibilityType = value; }
	// PURPOSE: Gets the visibility entity subset, that is a arbitrary game-specific id of the 'kind' of an entity. Render phases may cull entities based on this.
	u16 GetVisibilityType() const { return m_visibilityType; }
	
	// PURPOSE: Get physics inst attached to this entity
	phInst* GetCurrentPhysicsInst() const									{ return m_CurrentPhysicsInst; }

#if __SPU
	phInst** GetCurrentPhysicsInstPtr()										{ return &m_CurrentPhysicsInst; }
#endif // __SPU

	// PURPOSE: Set physics inst attached to this entity
	void SetCurrentPhysicsInst(phInst* pInst);
	void ClearCurrentPhysicsInst();

	// PURPOSE: Change the current phys inst
	//			- inst - just switch pointer, assume inst is already set up and managed elsewhere
	//			- bMaintainNeverActivateFlag - transfer the never activate setting to the new inst, default to false as 
	//											its managed from several sources and i'm fixed an isolated bug, we may want to consider 
	//											changing this to true by default
	inline void SwitchCurrentPhysicsInst(phInst *inst, bool bMaintainNeverActivateFlag = false); 

	// PURPOSE: Set the entity container for this entity. An entity may either have an owner container or a retaining list id.
	void SetOwnerEntityContainer(fwBaseEntityContainer* ownerEntityContainer)	{ Assert( !IsRetainedFlagSet() ); m_container.as_OwnerEntityContainer = ownerEntityContainer; }
	// PURPOSE: Get the entity container for this entity. An entity may either have an owner container or a retaining list id.
	fwBaseEntityContainer* GetOwnerEntityContainer() const						{ Assert( !IsRetainedFlagSet() ); return (fwBaseEntityContainer*)((size_t)m_container.as_OwnerEntityContainer & ~0x1); }

	// PURPOSE: Set the retaining list id for this entity. An entity may either have an owner container or a retaining list id.
	void SetRetainingListId(const s32 listId)								{ Assert( IsRetainedFlagSet() ); m_container.as_RetainingListId = ((listId << 1) | 0x1); }
	// PURPOSE: Get the retaining list id for this entity. An entity may either have an owner container or a retaining list id.
	s32 GetRetainingListId() const											{ Assert( IsRetainedFlagSet() ); return m_container.as_RetainingListId >> 1; }
	
	bool IsRetainedFlagSet() const { return (m_container.as_RetainingListId & 0x1) != 0; }
	void SetRetainedFlag(bool val) { val ? m_container.as_RetainingListId |= 0x1 : m_container.as_RetainingListId &= ~0x1 ; }

	// PURPOSE: This function is called whenever certain parts of this entity are changed (like m_type)
	// that may cause render flags to change.
	virtual void UpdateRenderFlags() {};

	// PURPOSE: Gets the axis aligned bounding box for entity
	void GetAABB(spdAABB& aabb) const;

	// PURPOSE: Gets the draw handler object (the object responsible for adding the right commands to the draw list for this entity)
	inline fwDrawData* GetDrawHandlerPtr() const;

	// PURPOSE: Gets the draw handler object (the object responsible for adding the right commands to the draw list for this entity)
	inline fwDrawData& GetDrawHandler() const;

	// PURPOSE: Get the data related to LOD hierarchy access, alpha value etc
	fwLodData& GetLodData()											{ return m_lodData; }

	// PURPOSE: Get the data related to LOD hierarchy access, alpha value etc
	const fwLodData& GetLodData() const								{ return m_lodData; }

	// PURPOSE: Set the data related to LOD  hierarchy access, alpha value etc
	void SetLodData(const fwLodData& lodData)						{ m_lodData = lodData; }

	// PURPOSE: Get the alpha fade of entity
	u8 GetAlpha() const												{ return m_lodData.GetAlpha(); }

	// PURPOSE: Set the alpha fade of entity
	void SetAlpha(u32 alpha)										{ m_lodData.SetAlpha(alpha); }

	// PURPOSE: return the lod parent of entity
	fwEntity* GetLod() const										{ return (fwEntity*) m_lodData.GetLod(); }

	// PURPOSE: set the lod parent of entity
	void SetLod(fwEntity* pLod)										{ m_lodData.SetLod(pLod); }

	// PURPOSE:	returns the root parent of entity
	fwEntity* GetRootLod();

	// PURPOSE: Get the distance at which entity fades in/out
	u32 GetLodDistance() const										{ return m_lodData.GetLodDistance(); }

	// PURPOSE: Set the distance at which entity fades in/out
	void SetLodDistance(u32 lodDist)								{ m_lodData.SetLodDistance(lodDist); }

	// PURPOSE: Handle any special case patching of entities before LOD attachment
	virtual void PatchUpLODDistances(fwEntity* /*pLOD*/)				{}

#if __BANK
	virtual void SetDebugPriority(int, bool) {}

	virtual s32 GetNumInstances() { return 0 ; }
#endif	//__BANK

#if RAGE_INSTANCED_TECH
	u8 GetViewportInstancedRenderBit()	{ return m_viewportInstancedRenderBit; }
	void SetviewportInstancedRenderBit(u8 uBit)	{ m_viewportInstancedRenderBit = uBit; }
#endif

#if __BANK
	static void AddWidgets(bkGroup &group);

	static void DebugDraw();
#endif // __BANK


#if !__NO_OUTPUT
	// PURPOSE: General-purpose callback for DIAG_CONTEXT_MESSAGE to display entity name and coords
	static const char *DiagCb(void *entity, void *);
#endif // !__NO_OUTPUT

	PPUVIRTUAL void OnActivate(phInst* pInst, phInst* pOtherInst);
	PPUVIRTUAL void OnDeactivate(phInst* pInst);

private:
	void UpdateCurrentCollider();
	void SetCurrentPhysicsInstInternal(phInst* pInst);

public:

	// PURPOSE : assign value to a protected flag
	inline void AssignProtectedBaseFlag(protectedFlags flag, bool bVal) { BANK_ONLY(bool bOldVal = IsProtectedBaseFlagSet(flag);) if (bVal) {m_protectedFlags |= flag;} else {m_protectedFlags &= ~flag;} BANK_ONLY(LogBaseFlagChange(flag, true, bVal, bOldVal);)}

#if !__SPU
protected:
#endif // !__SPU
	// PURPOSE: Inform other systems of change in entity position and orientation
	virtual void UpdateWorldFromEntity() {}

	// 8 initial bytes (for vTablePtr and fwRefAwareBase::m_pKnownRefHolderHead)
	//fwModelId m_modelId;				//	4 bytes @ offset 16			
	fwArchetype* m_pArchetype;			//	4 bytes @ offset 20
	u8 m_type;							//	1 bytes @ offset 24
	u8 m_protectedFlags;				//	1 bytes @ offset 25


private:
	u16 m_RenderFlags;					//  2 bytes @ offset 26
	u32	m_baseFlags;					//	4 bytes @ offset 28
	phInst* m_CurrentPhysicsInst;		//	4 bytes @ offset 32

protected:
	fwFlags16 m_phaseVisibilityMask;	//	2 bytes @ offset 36
	u16 m_visibilityType;				//	2 bytes @ offset 38

	union
	{
		fwBaseEntityContainer*	as_OwnerEntityContainer;
		s32						as_RetainingListId;
	}	m_container;					//	4 bytes @ offset 42

	fwDrawData*			m_pDrawHandler;	//	4 bytes @ offset 46		// entity's drawable + local instance of shader effect for drawable

private:
	fwDynamicEntityComponent *m_DynamicEntityComponent;	// 4 bytes @ offset 50
#if ENABLE_MATRIX_MEMBER
	fwMatrixScaledTransform m_Transform; // 64 bytes? 
#else
	fwTransform* m_Transform;			//	4 bytes @ offset 54
#endif
protected:
	fwLodData m_lodData;				//	8 bytes @ offset 58
#if RAGE_INSTANCED_TECH
	u8 m_viewportInstancedRenderBit;		//  1 bytes @ offset 64
#endif
};

//CompileTimeAssert(offsetof(fwEntity, m_modelId) == 16);
//CompileTimeAssert(offsetof(fwEntity, m_type) == 10);
//CompileTimeAssert(offsetof(fwEntity, m_pad) == 11);
//CompileTimeAssert(offsetof(fwEntity, m_flags) == 12);
//CompileTimeAssert(offsetof(fwEntity, m_Transform) == 16);
//CompileTimeAssert(offsetof(fwEntity, m_CurrentPhysicsInst) == 20);
#if RAGE_INSTANCED_TECH
	#if ENABLE_MATRIX_MEMBER
	CompileTimeAssertSize(fwEntity,FWENTITY_INTENDED_SIZE, 192);
	#else
	CompileTimeAssertSize(fwEntity,FWENTITY_INTENDED_SIZE, 120);
	#endif
#else	
	#if ENABLE_MATRIX_MEMBER
	CompileTimeAssertSize(fwEntity,FWENTITY_INTENDED_SIZE, 192);
	#else
	CompileTimeAssertSize(fwEntity,FWENTITY_INTENDED_SIZE, 112);
	#endif
#endif

#if ENABLE_MATRIX_MEMBER
__forceinline const fwMatrixScaledTransform* fwEntity::GetTransformPtr() const 
#else
__forceinline const fwTransform* fwEntity::GetTransformPtr() const 
#endif
{ 
#if ENABLE_MATRIX_MEMBER
	return &m_Transform; 
#else
	return m_Transform; 
#endif
}

#if ENABLE_MATRIX_MEMBER
__forceinline const fwMatrixScaledTransform& fwEntity::GetTransform() const 
#else
__forceinline const fwTransform& fwEntity::GetTransform() const 
#endif
{ 
#if ENABLE_MATRIX_MEMBER
	return m_Transform;
#else
	TrapZ((size_t)m_Transform); // m_Transform is only NULL after destruction.
	return *m_Transform; 
#endif
} 

#if ENABLE_MATRIX_MEMBER
__forceinline void fwEntity::SetTransformScale( const float& xy, const float& z)
{ 
	m_Transform.SetScale(xy,z);
} 
#endif

#if ENABLE_MATRIX_MEMBER
__forceinline void fwEntity::SetTransform(Mat34V_In transform, bool bUpdateGameWorld, bool, bool)
#else
__forceinline void fwEntity::SetTransform(fwTransform* transform, bool bUpdateGameWorld, bool, bool)
#endif
{
#if !ENABLE_MATRIX_MEMBER
	TrapZ((size_t)m_Transform); // m_Transform is only NULL after destruction.
	TrapZ((size_t)transform); // m_Transform is not allowed to be set to NULL.
#endif
	

#if ENABLE_MATRIX_MEMBER
	m_Transform.SetMatrixWithScale( transform );
#endif

	// If there is a skeleton, then it will also be holding a pointer to the old
	// transform, so that needs to be updated.
	crSkeleton *skel = GetSkeleton();
	if (skel)
	{
		#if ENABLE_MATRIX_MEMBER		
		skel->SetParentMtx(m_Transform.GetMatrixPtr());
		#else
		fwMatrixTransform *mtx = static_cast<fwMatrixTransform*>(transform);
		skel->SetParentMtx(mtx->GetMatrixPtr());
		#endif
	}

#if !ENABLE_MATRIX_MEMBER
	m_Transform->Delete();
	m_Transform = transform;
#endif
	if (bUpdateGameWorld)
		UpdateWorldFromEntity();
}

inline Mat34V_Out fwEntity::GetMatrix() const
{
	return GetTransform().GetMatrix();
}

inline void fwEntity::GetMatrixCopy(Matrix34 &copy) const
{
	copy = MAT34V_TO_MATRIX34(GetTransform().GetMatrix());
}

inline Mat34V_Out fwEntity::GetNonOrthoMatrix() const
{
	return GetTransform().GetNonOrthoMatrix();
}

inline void fwEntity::GetNonOrthoMatrixCopy(Matrix34 &copy) const
{
	copy = MAT34V_TO_MATRIX34(GetTransform().GetNonOrthoMatrix());
}

inline void fwEntity::SetMatrix(const Matrix34& mat, bool bUpdateGameWorld, bool, bool)
{
	Assertf(!GetIsAttached() || GetAttachmentExtension()->GetAttachFlag(ATTACH_FLAG_DONT_ASSERT_ON_MATRIX_CHANGE)
		|| GetAttachmentExtension()->GetAttachFlag(ATTACH_FLAG_IN_DETACH_FUNCTION)
		|| !(GetAttachmentExtension()->GetAttachState()==ATTACH_STATE_BASIC || GetAttachmentExtension()->GetAttachState()==ATTACH_STATE_WORLD),
		"[fwEntity::SetMatrix()] - Trying to modify matrix on object (%s) with BASIC attachment. AttachFlags=0x%08x Debug Name=%s Attached To (%s)",
		GetModelName(), GetAttachmentExtension()->GetAttachFlags(), (GetDebugName() != NULL) ? GetDebugName() : "None", GetAttachmentExtension()->GetAttachParent() ? GetAttachmentExtension()->GetAttachParent()->GetModelName() : "None");

#if ENABLE_MATRIX_MEMBER
	m_Transform.SetMatrix(RCC_MAT34V(mat));
#else
	m_Transform->SetMatrix(RCC_MAT34V(mat));
#endif

#if PDR_ENABLED
	const phInst *pInst = GetCurrentPhysicsInst();
	if (pInst)
	{
		PDR_ONLY(debugPlayback::RecordTaggedMatrixValue(*pInst, RCC_MAT34V(mat), "fwEntity::SetMatrix"));
	}
#endif

	if (bUpdateGameWorld){
		UpdateWorldFromEntity();
	}
}

inline void fwEntity::SetPosition(const Vector3& vec, bool bUpdateGameWorld, bool, bool)
{
	Assertf(!GetIsAttached() || GetAttachmentExtension()->GetAttachFlag(ATTACH_FLAG_DONT_ASSERT_ON_MATRIX_CHANGE)
		|| GetAttachmentExtension()->GetAttachFlag(ATTACH_FLAG_IN_DETACH_FUNCTION)
		|| !(GetAttachmentExtension()->GetAttachState()==ATTACH_STATE_BASIC || GetAttachmentExtension()->GetAttachState()==ATTACH_STATE_WORLD),
		"[fwEntity::SetPosition()] - Trying to modify matrix on object (%s) with BASIC attachment. AttachFlags=0x%08x",
		GetModelName(), GetAttachmentExtension()->GetAttachFlags());

#if ENABLE_MATRIX_MEMBER
	m_Transform.SetPosition(RCC_VEC3V(vec));
#else
	m_Transform->SetPosition(RCC_VEC3V(vec));
#endif

#if PDR_ENABLED
	const phInst *pInst = GetCurrentPhysicsInst();
	if (pInst)
	{
		PDR_ONLY(debugPlayback::RecordTaggedVectorValue(*GetCurrentPhysicsInst(), RCC_VEC3V(vec), "fwEntity::SetPosition", debugPlayback::eVectorTypePosition));
	}
#endif

	if (bUpdateGameWorld){
		UpdateWorldFromEntity();
	}
}

inline void fwEntity::SetHeading(float heading)
{
	Assertf(!GetIsAttached() || GetAttachmentExtension()->GetAttachFlag(ATTACH_FLAG_DONT_ASSERT_ON_MATRIX_CHANGE)
		|| GetAttachmentExtension()->GetAttachFlag(ATTACH_FLAG_IN_DETACH_FUNCTION)
		|| !(GetAttachmentExtension()->GetAttachState()==ATTACH_STATE_BASIC || GetAttachmentExtension()->GetAttachState()==ATTACH_STATE_WORLD),
		"[fwEntity::SetHeading()] - Trying to modify matrix on object (%s) with BASIC attachment. AttachFlags=0x%08x",
		GetModelName(), GetAttachmentExtension()->GetAttachFlags());

#if ENABLE_MATRIX_MEMBER
	m_Transform.SetHeading(heading);
#else
	m_Transform->SetHeading(heading);
#endif
	UpdateWorldFromEntity();			
}

inline void fwEntity::TransformIntoWorldSpace(Vector3& result, const Vector3& vec) const
{
	result = VEC3V_TO_VECTOR3(GetTransform().Transform(RCC_VEC3V(vec)));
}

inline Vector3 fwEntity::TransformIntoWorldSpace(const Vector3& vec) const
{
	return VEC3V_TO_VECTOR3(GetTransform().Transform(RCC_VEC3V(vec)));
}

inline void fwEntity::TransformIntoWorldSpace(Vec3V_InOut result, Vec3V_In vec) const
{
	result = GetTransform().Transform(vec);
}

inline Vec3V_Out fwEntity::TransformIntoWorldSpace(Vec3V_In vec) const
{
	return GetTransform().Transform(vec);
}

#if !__SPU
inline fragInst* fwEntity::GetFragInst() const
{ 
	return (fragInst*)GetCurrentPhysicsInst(); 
}
#endif

inline void fwEntity::SwitchCurrentPhysicsInst(phInst *inst, bool bMaintainNeverActivateFlag)
{
	Assert(inst);
	if( bMaintainNeverActivateFlag && m_CurrentPhysicsInst && inst )
	{
		const bool bNeverActivate = m_CurrentPhysicsInst->GetInstFlag(phInst::FLAG_NEVER_ACTIVATE) != 0;
		inst->SetInstFlag(phInst::FLAG_NEVER_ACTIVATE, bNeverActivate);
	}
	SetCurrentPhysicsInstInternal(inst);
}


inline const crSkeletonData& fwEntity::GetSkeletonData() const
{
	const crSkeleton* pSkeleton = GetSkeleton();
	Assert(pSkeleton ENABLE_FRAG_OPTIMIZATION_ONLY(|| GetFragInst()));

#if ENABLE_FRAG_OPTIMIZATION
	if (Unlikely(!pSkeleton))
	{
		return GetFragInst()->GetType()->GetSkeletonData();
	}
#endif

	return pSkeleton->GetSkeletonData();
}



inline fwDrawData* fwEntity::GetDrawHandlerPtr() const {return m_pDrawHandler;}

inline fwDrawData& fwEntity::GetDrawHandler() const {FastAssert(m_pDrawHandler); return *m_pDrawHandler;}

inline fwDynamicEntityComponent *fwEntity::CreateDynamicComponentIfMissing()
{
	if (!m_DynamicEntityComponent)
	{
		m_DynamicEntityComponent = rage_new fwDynamicEntityComponent();
	}

	return m_DynamicEntityComponent;
}

inline void fwEntity::SetAnimatedVelocity(Vector3::Vector3Param vAnimatedVelocity)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetAnimatedVelocity(vAnimatedVelocity);
}

inline Vector3 fwEntity::GetAnimatedVelocity() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetAnimatedVelocity();
	}

	return VEC3_ZERO;
}

inline void fwEntity::SetSkeleton(crSkeleton *skeleton)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetSkeleton(skeleton);
}

inline crSkeleton *fwEntity::GetSkeleton() const
{
	if (const fragInst* pFragInst = GetFragInst())
	{
		return GetSkeletonFromFragInst(pFragInst);
	}

	return GetEntitySkeleton();
}

inline fwAnimDirector *fwEntity::GetAnimDirector() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetAnimDirector();
	}

	return NULL;
}

#if ENABLE_FRAG_OPTIMIZATION
inline u32 fwEntity::GetBoneCount() const
{
	const crSkeleton* pSkeleton = GetSkeleton();

	if (pSkeleton)
	{
		return pSkeleton->GetBoneCount();
	}
	else
	{
		const fragInst* pFragInst = GetFragInst();

		if (pFragInst)
		{
			return pFragInst->GetType()->GetSkeletonData().GetNumBones();
		}
	}

	return 0;
}

inline void fwEntity::ComputeObjectMtx(int boneIdx, Mat34V_InOut matrix) const
{
	const crSkeleton* pSkeleton = GetSkeleton();

	Assert(pSkeleton || GetFragInst());

	if(Unlikely(!pSkeleton))
	{
		const fragInst* pFragInst = GetFragInst();

		if (pFragInst)
		{
			pFragInst->GetType()->GetSkeletonData().ComputeObjectTransform(boneIdx, matrix);
		}
		else
		{
			matrix = Mat34V(V_IDENTITY);
		}

		return;
	}

	matrix = RCC_MAT34V(GetObjectMtx(boneIdx));
}
#endif

inline void fwEntity::GetGlobalMtx(int boneIdx, Matrix34& matrix) const
{
	const crSkeleton* pSkeleton = GetSkeleton();
	Assert(pSkeleton ENABLE_FRAG_OPTIMIZATION_ONLY(|| GetFragInst()));

#if ENABLE_FRAG_OPTIMIZATION
	if(Unlikely(!pSkeleton))
	{
		const fragInst* pFragInst = GetFragInst();

		if (pFragInst)
		{
			pFragInst->GetType()->GetSkeletonData().ComputeGlobalTransform(boneIdx, GetMatrix(), RC_MAT34V(matrix));
		}
		else
		{
			matrix.Identity();
		}

		return;
	}
#endif

	WaitForAnyActiveAnimUpdateToComplete();
	Assertf(boneIdx > -1 && boneIdx < (int)GetSkeleton()->GetBoneCount(), "Invalid bone index %d. Should be between 0 and %d", boneIdx, GetSkeleton()->GetBoneCount()-1);
	pSkeleton->GetGlobalMtx(boneIdx, RC_MAT34V(matrix));
}

inline void fwEntity::SetGlobalMtx( int boneIdx, const Matrix34& matrix )
{
	Assert(GetSkeleton());
	WaitForAnyActiveAnimUpdateToComplete();
	Assertf(boneIdx > -1 && boneIdx < (int)GetSkeleton()->GetBoneCount(), "Invalid bone index %d. Should be between 0 and %d", boneIdx, GetSkeleton()->GetBoneCount()-1);
	GetSkeleton()->SetGlobalMtx(boneIdx, RCC_MAT34V(matrix));
}

inline const Matrix34& fwEntity::GetLocalMtx(int boneIdx) const
{
	const crSkeleton* pSkeleton = GetSkeleton();
	Assert(pSkeleton ENABLE_FRAG_OPTIMIZATION_ONLY(|| GetFragInst()));

#if ENABLE_FRAG_OPTIMIZATION
	if(Unlikely(!pSkeleton))
	{
		const fragInst* pFragInst = GetFragInst();

		if (pFragInst)
		{
			return RCC_MATRIX34(pFragInst->GetType()->GetSkeletonData().GetDefaultTransform(boneIdx));
		}
		else
		{
			static const Matrix34 identity = Matrix34(Matrix34::IdentityType);
			return identity;
		}
	}
#endif

	WaitForAnyActiveAnimUpdateToComplete();
	Assertf(boneIdx > -1 && boneIdx < (int)pSkeleton->GetBoneCount(), "Invalid bone index %d. Should be between 0 and %d", boneIdx, pSkeleton->GetBoneCount()-1);
	return RCC_MATRIX34(pSkeleton->GetLocalMtx(boneIdx));
}

inline Matrix34& fwEntity::GetLocalMtxNonConst(int boneIdx)
{
	Assert(GetSkeleton());
	WaitForAnyActiveAnimUpdateToComplete();
	Assertf(boneIdx > -1 && boneIdx < (int)GetSkeleton()->GetBoneCount(), "Invalid bone index %d. Should be between 0 and %d", boneIdx, GetSkeleton()->GetBoneCount()-1);
	return RC_MATRIX34(GetSkeleton()->GetLocalMtx(boneIdx));
}

inline const Matrix34& fwEntity::GetObjectMtx(int boneIdx) const
{
	Assert(GetSkeleton());
	WaitForAnyActiveAnimUpdateToComplete();
	Assertf(boneIdx > -1 && boneIdx < (int)GetSkeleton()->GetBoneCount(), "Invalid bone index %d. Should be between 0 and %d", boneIdx, GetSkeleton()->GetBoneCount()-1);
	return RCC_MATRIX34(GetSkeleton()->GetObjectMtx(boneIdx));
}

inline Matrix34& fwEntity::GetObjectMtxNonConst(int boneIdx)
{
	Assert(GetSkeleton());
	WaitForAnyActiveAnimUpdateToComplete();
	Assertf(boneIdx > -1 && boneIdx < (int)GetSkeleton()->GetBoneCount(), "Invalid bone index %d. Should be between 0 and %d", boneIdx, GetSkeleton()->GetBoneCount()-1);
	return RC_MATRIX34(GetSkeleton()->GetObjectMtx(boneIdx));
}

inline crSkeleton *fwEntity::GetEntitySkeleton() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetSkeleton();
	}

	return NULL;
}

inline float fwEntity::GetAnimatedAngularVelocity() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetAnimatedAngularVelocity();
	}

	return 0.0f;
}

inline void fwEntity::SetAnimatedAngularVelocity(float velocity)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetAnimatedAngularVelocity(velocity);
}


inline float fwEntity::GetForceAddToBoundRadius() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetForceAddToBoundRadius();
	}

	return 0.0f;
}

inline void fwEntity::SetForceAddToBoundRadius(float forceAdd)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetForceAddToBoundRadius(forceAdd);
}


inline crCreature *fwEntity::GetCreature() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetCreature();
	}

	return NULL;
}

inline void fwEntity::SetCreature(crCreature *creature)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetCreature(creature);
}


inline grbTargetManager *fwEntity::GetTargetManager() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetTargetManager();
	}

	return NULL;
}

inline void fwEntity::SetTargetManager(grbTargetManager *targetMgr)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetTargetManager(targetMgr);
}

inline Vector3 fwEntity::GetPreviousPosition() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetPreviousPosition();
	}

	return VEC3V_TO_VECTOR3(GetTransform().GetPosition());
}

inline void fwEntity::SetPreviousPosition(Vector3::Vector3Param vecPos)
{
	CreateDynamicComponentIfMissing()->SetPreviousPosition(vecPos);
}

inline fwAttachmentEntityExtension & fwEntity::GetAttachmentExtensionRef()
{
	Assertf(GetDynamicComponent()->GetAttachmentExtension(), "Entity doesn't have an attachment extension although it is expected to have one.");
	return *GetDynamicComponent()->GetAttachmentExtension();
}

inline fwAttachmentEntityExtension const & fwEntity::GetAttachmentExtensionRef() const
{
	Assertf(GetDynamicComponent()->GetAttachmentExtension(), "Entity doesn't have an attachment extension although it is expected to have one.");
	return *GetDynamicComponent()->GetAttachmentExtension();
}

inline bool fwEntity::GetIsAttached() const
{
	if (const fwAttachmentEntityExtension *extension = GetAttachmentExtension())
	{
		return extension->GetIsAttached();
	}

	return false;
}

inline fwEntity* fwEntity::GetAttachParent() const
{
	if (const fwAttachmentEntityExtension *extension = GetAttachmentExtension())
	{
		return extension->GetAttachParent();
	}

	return NULL;
}

inline fwEntity* fwEntity::GetAttachParentForced() const
{
	if (const fwAttachmentEntityExtension *extension = GetAttachmentExtension())
	{
		return extension->GetAttachParentForced();
	}

	return NULL;
}

inline fwEntity* fwEntity::GetSiblingAttachment() const
{
	if (const fwAttachmentEntityExtension *extension = GetAttachmentExtension())
	{
		return extension->GetSiblingAttachment();
	}

	return NULL;
}

inline fwEntity* fwEntity::GetChildAttachment() const
{
	if (const fwAttachmentEntityExtension *extension = GetAttachmentExtension())
	{
		return extension->GetChildAttachment();
	}

	return NULL;
}

inline int fwEntity::GetNoCollisionFlags() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetNoCollisionFlags();
	}

	return 0;
}

inline void fwEntity::SetNoCollisionFlags(u8 flags)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetNoCollisionFlags(flags);
}


inline const fwEntity* fwEntity::GetNoCollisionEntity() const
{
	if (m_DynamicEntityComponent)
	{
		return m_DynamicEntityComponent->GetNoCollisionEntity();
	}

	return NULL;
}

inline void fwEntity::SetNoCollisionEntity(fwEntity* pEntity)
{
	if(m_DynamicEntityComponent)
	{
		m_DynamicEntityComponent->SetNoCollisionEntity(pEntity);
	}
}

inline void fwEntity::SetNoCollision(fwEntity* pEntity, u8 nTypeFlags)
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->SetNoCollision(pEntity, nTypeFlags);
}

inline void fwEntity::PrepareNoCollisionReset()
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->PrepareNoCollisionReset();
}

inline void fwEntity::ResetNoCollision()
{
	CreateDynamicComponentIfMissing();
	m_DynamicEntityComponent->ResetNoCollision();
}

} // namespace rage

#endif // SCENE_ENTITY_H_
