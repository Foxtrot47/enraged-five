
#ifndef ENTITY_ARCHETYPESTREAMINGMODULE_H
#define ENTITY_ARCHETYPESTREAMINGMODULE_H

#include "streaming/streamingmodule.h"

namespace rage {

class fwArchetypeStreamingModule : public strStreamingModule
{
public:
	fwArchetypeStreamingModule(int numArchetypes);

#if !__FINAL
	virtual const char* GetName(strLocalIndex index) const;
#endif
	virtual strLocalIndex Register(const char* name);

	virtual void AddRef(strLocalIndex index, strRefKind refKind);
	virtual void RemoveRef(strLocalIndex index, strRefKind refKind);
	virtual int GetNumRefs(strLocalIndex index) const;
	virtual int GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const;
	virtual void Remove(strLocalIndex index);
	virtual bool Load(strLocalIndex index, void* object, int size);
};	

} // namespace rage

#endif // ENTITY_ARCHETYPESTREAMINGMODULE_H
