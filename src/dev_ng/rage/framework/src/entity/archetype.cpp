//
// entity/archetype.cpp : base class for all entity archetypes
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "archetype.h"

#include "diag/art_channel.h"
#include "paging/dictionary.h"
#include "entity/archetypemanager.h"
#include "fragment/drawable.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/mapdata/maptypes.h"
#include "fwscene/mapdata/maptypesdef.h"
#include "fwscene/stores/drawablestore.h"
#include "fwscene/stores/dwdstore.h"
#include "fwscene/stores/fragmentstore.h"
#include "fwscene/stores/maptypesstore.h"
#include "fwscene/stores/txdstore.h"
#include "phbound/boundcomposite.h"

#include "dynamicarchetype.h"


namespace rage {

#if __DEV
	int fwArchetype::sm_DynamicComponentCount;
#endif // __DEV


fwArchetype::~fwArchetype()
{
	if (m_assignedStreamingSlot != fwArchetypeManager::INVALID_STREAM_SLOT)
	{
		DestroyDynamicComponent();
		Assert(fwArchetypeManager::GetStreamingModule()->GetNumRefs(strLocalIndex(m_assignedStreamingSlot)) == 0);

		fwArchetypeManager::UnregisterStreamedArchetype(this);

		// free up physics - after the name has been unregistered!
		if (GetDrawableType()!=fwArchetype::DT_FRAGMENT)
		{
			phArchetype *pPhysArchetype = GetPhysics();
			if (pPhysArchetype)
			{
				pPhysArchetype->Release(false);
				SetPhysics(NULL);
			}
		}
	}
	else
	{
		//Assertf(false,"Hit archetype which was skipped during init (probably [ART] duplicate model name)");
	}
}

// PURPOSE: Initialise archetype
void fwArchetype::Init()
{
	m_flags = 0;
	m_drawableDictionaryDrawableIndex = 0xFF;		// intentionally invalid value so we trap out instead of returning the wrong drawable.

	DestroyDynamicComponent();

	// Initialize the union of indices.
	m_fragmentIndex = INVALID_FRAG_IDX;
	m_DrawableType = DT_UNINITIALIZED;

	// bounding box
	const Vector4 zero4(Vector4::ZeroType);
	m_BoundingBox.Set(Vec3V(V_ZERO), Vec3V(V_ZERO));
	m_BoundingSphere = zero4;

	m_numRefs = 0;

	SetLodDistance(100.0f);

	m_bIsModelMissing = false;
}

const u32 nullHash = ATSTRINGHASH("null",0x3ADB3357);

void fwArchetype::InitArchetypeFromDefinition(strLocalIndex mapTypeDefIndex, fwArchetypeDef* definition, bool bHasAssetReferences)
{
#if __ASSERT
	if (definition && definition->m_assetType!=fwArchetypeDef::ASSET_TYPE_ASSETLESS)
	{
		Assertf(definition->m_assetName.GetHash() == definition->m_name.GetHash(),
			"fwArchetype name %s doesn't match model name %s! Tools or data fault? (from %s)", definition->m_name.GetCStr(), definition->m_assetName.GetCStr(),
			g_MapTypesStore.GetName(mapTypeDefIndex));
	}
	ASSERT_ONLY(fwArchetypeManager::VerifyNameClashes(definition->m_name.GetCStr()));
	Assert(mapTypeDefIndex != fwMapTypes::GLOBAL);		// need to know the type file these came from
#endif		//__ASSERT

	Init();
	SetModelName(definition->m_name.GetHash());

	fwMapTypesDef* pDef = g_MapTypesStore.GetSlot(mapTypeDefIndex);
	Assert(pDef);

	if (pDef && !pDef->GetIsPermanent()){
		 fwArchetypeManager::RegisterStreamedArchetype(this, mapTypeDefIndex.Get());
	} else {
		fwArchetypeManager::RegisterPermanentArchetype(this, mapTypeDefIndex.Get(), false);
	}

	// Displayf("Created archetype %s (0x%08x) ", GetModelName(), GetModelNameHash());
	
	SetLodDistance(definition->m_lodDist);
	SetBoundingBoxMin(definition->m_bbMin);
	SetBoundingBoxMax(definition->m_bbMax);
	SetBoundingSphere(definition->m_bsCentre, definition->m_bsRadius);
	SetHDTextureDistance(definition->m_hdTextureDist);

	if (definition->m_clipDictionary.GetHash())
		SetClipDictionaryIndex(definition->m_clipDictionary.GetHash());

	if (definition->m_physicsDictionary.GetHash())
	{
		SetHasBoundInDrawable();
	}

	if (bHasAssetReferences){
		s32 parentTxdSlot = FindTxdSlotIndex(definition->m_textureDictionary.GetHash()).Get();

		// is asset specified in the drawable dictionary name?
		atHashValue drawableHash = definition->m_drawableDictionary.GetHash();
		if (drawableHash.IsNotNull() && drawableHash != nullHash)
			SetDrawDictFile(definition->m_drawableDictionary, parentTxdSlot);
		else {
			// if we knew in advance - we could do this much faster! (asset is specified in drawable or fragment)
			SetDrawableOrFragmentFile(definition->m_name.GetHash(), parentTxdSlot, false);
		}
	} else {
		m_DrawableType = DT_ASSETLESS;
	}

#if __ASSERT
	//////////////////////////////////////////////////////////////////////////
	// sanity check asset type
	if (m_DrawableType != definition->m_assetType)
	{
		
		const char* assetTypes[] =
		{
			"UNINITIALIZED",
			"FRAGMENT",
			"DRAWABLE",
			"DRAWABLEDICTIONARY",
			"ASSETLESS"
		};

		const s32 dt1 = m_DrawableType;
		const s32 dt2 = definition->m_assetType;
		const char* assetType1 = ( (dt1>=0 && dt1<DT_COUNT) ? assetTypes[dt1] : "???" );
		const char* assetType2 = ( (dt2>=0 && dt2<DT_COUNT) ? assetTypes[dt2] : "???" );

		// special case for gen8 stub content throwing an assert for real gen9 only content
#if (RSG_ORBIS | RSG_DURANGO | RSG_PC)
		if (ATSTRINGHASH("exc_Prop_TR_Overlay_Meet", 0x4826a7b) != definition->m_assetName )
#endif
		{
			Assertf(
				m_DrawableType==definition->m_assetType,
				"fwArchetype <%s> in ITYP %s has incorrect asset type (was %s, should be %s)",
				definition->m_name.GetCStr(),
				pDef->m_name.GetCStr(),
				assetType2,
				assetType1
			);
		}
	}
	//////////////////////////////////////////////////////////////////////////
#endif	//__ASSERT
}
	

fwEntity* fwArchetype::CreateEntity()
{
	return NULL;
}

fwEntity* fwArchetype::CreateEntityFromDefinition(const fwPropInstanceListDef* /*definition*/)
{
	return NULL;
}

fwEntity* fwArchetype::CreateEntityFromDefinition(const fwGrassInstanceListDef* /*definition*/)
{
	return NULL;
}

// PURPOSE: Shutdown archetype
void fwArchetype::Shutdown()
{
	// Remove any reference to a physics object,
	// This allows the physics item to be unloaded
	if( HasPhysics() )
	{
		GetPhysics()->Release(false);
		SetPhysicsOnly(NULL);
	}

	m_extensionList.DestroyAll();
}

//
// name:		CBaseModelInfo::GetDrawable
// description:	Return the drawable object associated with this modelinfo
rmcDrawable* fwArchetype::GetDrawable() const
{
	if (GetHasLoaded() == false){
		return(NULL);
	}

	rmcDrawable* ret = NULL;

	switch (GetDrawableType()) {
		case DT_FRAGMENT:
			{
				Assert(m_fragmentIndex != INVALID_FRAG_IDX);
				fragType* pFragment = g_FragmentStore.Get(strLocalIndex(m_fragmentIndex));
				ret = (pFragment == NULL) ? NULL : pFragment->GetCommonDrawable();
			}
			break;
		case DT_DRAWABLE:
			Assert(m_drawableIndex != INVALID_DRAWABLE_IDX);
			ret = g_DrawableStore.Get(strLocalIndex(m_drawableIndex));
			break;
		case DT_DRAWABLEDICTIONARY:
			{
				Assert(m_drawableDictionaryIndex != INVALID_DRAWDICT_IDX);
				pgDictionary<rmcDrawable> * pDwd = g_DwdStore.Get(strLocalIndex(m_drawableDictionaryIndex));
				ret = (pDwd == NULL) ? NULL : pDwd->GetEntry(m_drawableDictionaryDrawableIndex);
			}
			break;
		default:
			break;
	} 

	return(ret);
}

fragType* fwArchetype::GetFragType() const
{
	if (GetHasLoaded() == false){
		return(NULL);
	}

	fragType* ret = NULL;
	if (m_DrawableType == DT_FRAGMENT){
		Assert(m_fragmentIndex != INVALID_FRAG_IDX);
		ret = static_cast<fragType*>(g_FragmentStore.Get(strLocalIndex(m_fragmentIndex)));
	}

	return(ret);
}




//
// name:		SetTexDictionary
// description:	Set texture dictionary for object
void fwArchetype::SetDrawDictFile(const strStreamingObjectName pName, s32 parentTxdSlotIndex)
{
	// check for NULL dictionary name - means there is no drawable dictionary associated (so no dependency)
	Assert( pName.IsNotNull() );

	if ( pName.GetHash() == nullHash)
	{
		m_drawableDictionaryIndex = INVALID_DRAWDICT_IDX;
		m_DrawableType = DT_UNINITIALIZED;
		return;
	}

	m_DrawableType = DT_DRAWABLEDICTIONARY;
	strLocalIndex storeIndex = g_DwdStore.FindSlot(pName);
	if (!Verifyf(storeIndex != -1, "Archetype :%s is missing .dwd file : %s", GetModelName(), pName.GetCStr())) {
		m_drawableDictionaryIndex = INVALID_DRAWDICT_IDX;
		m_DrawableType = DT_UNINITIALIZED;
		return;
	}

	m_drawableDictionaryIndex = static_cast<u16>(storeIndex.Get());

	ASSERT_ONLY(strLocalIndex currentParentTxdSlotIndex = g_DwdStore.GetParentTxdForSlot(strLocalIndex(m_drawableDictionaryIndex)));
	artAssertf(currentParentTxdSlotIndex == -1 || parentTxdSlotIndex == -1 || currentParentTxdSlotIndex == parentTxdSlotIndex, 
		"%s: Drawable dictionary entries cannot use different texture dictionaries. This is most likely because you have Super LODs whose children use different texture dictionaries", 
		pName.GetCStr());

	if (parentTxdSlotIndex != -1){
		g_DwdStore.SetParentTxdForSlot(strLocalIndex(m_drawableDictionaryIndex), strLocalIndex(parentTxdSlotIndex));
	}
}

//
// name:		SetDrawableOrFragmentFile
// description:	Set drawable or fragment for entity & set parent txd idx accordingly.
// if bIsFragment is set, then we can safely skip the drawable lookup (archetype known to be fragment type)
bool fwArchetype::SetDrawableOrFragmentFile(const strStreamingObjectName pName, s32 parentTxdIdx, bool bIsFragment)
{
	m_drawableIndex = INVALID_DRAWABLE_IDX;
	m_DrawableType = DT_UNINITIALIZED;

	// check for NULL dictionary name - means there is no drawable dictionary associated (so no dependency)
	Assert( pName.IsNotNull() );
	if ( pName.GetHash() == nullHash)
	{
		return false;
	}

	u32 nameHash = pName.GetHash();
	// need to lookup this name to determine if in the fragment store or the drawable store

	if (!bIsFragment){
		strLocalIndex storeIndex = strLocalIndex(g_DrawableStore.FindSlotFromHashKey(nameHash));

		if (storeIndex != -1){
			// drawable store entry
			m_DrawableType = DT_DRAWABLE;
			m_drawableIndex = storeIndex.Get();
			if (parentTxdIdx != -1){
				g_DrawableStore.SetParentTxdForSlot(storeIndex, strLocalIndex(parentTxdIdx));
			}
			return true;
		}
	}

	// fragment store entry
	strLocalIndex storeIndex = strLocalIndex(g_FragmentStore.FindSlotFromHashKey(nameHash));
	if (storeIndex != -1){
		// verify existence
		Assertf(g_FragmentStore.IsObjectInImage(storeIndex),"%s : can't find in .rpfs", g_FragmentStore.GetName(storeIndex));

		m_DrawableType = DT_FRAGMENT;
		m_drawableIndex = (u16)storeIndex.Get();
		if (parentTxdIdx != -1){
			g_FragmentStore.SetParentTxdForSlot(storeIndex, strLocalIndex(parentTxdIdx));
		}
		return true;
	}

	artWarningf("Archetype :%s is missing required matching drawable or fragment file : %s", GetModelName(), pName.TryGetCStr());
	return(false);
}

strLocalIndex fwArchetype::FindTxdSlotIndex(const strStreamingObjectName pName)
{
	// check for NULL txd name - means there is no txd (so no dependency)
	if ( pName.IsNull() || pName.GetHash() == nullHash)
	{
		return strLocalIndex(-1);
	}

	return g_TxdStore.FindSlot(pName);
}

s32 fwArchetype::GetAssetParentTxdIndex() const 
{
	s32 ret = -1;

	switch(m_DrawableType){
		case DT_ASSETLESS:
			break;
		case DT_DRAWABLE:
			ret = g_DrawableStore.GetParentTxdForSlot(strLocalIndex(GetDrawableIndex())).Get();
			break;
		case DT_DRAWABLEDICTIONARY:
			ret = g_DwdStore.GetParentTxdForSlot(strLocalIndex(GetDrawDictIndex())).Get();
			break;
		case DT_FRAGMENT:
			ret = g_FragmentStore.GetParentTxdForSlot(strLocalIndex(GetFragmentIndex())).Get();
			break;
		default:
			Warningf("Could not get parent txd index for %s : no asset has been set", GetModelName());
	}

	return(ret);
}

void fwArchetype::SetAssetParentTxdIndex(s32 txdSlot)
{
	if (txdSlot == -1)
	{
		return;
	}

	switch(m_DrawableType){
		case DT_ASSETLESS:
			break;
		case DT_DRAWABLE:
			g_DrawableStore.SetParentTxdForSlot(strLocalIndex(GetDrawableIndex()),strLocalIndex(txdSlot));
			break;
		case DT_DRAWABLEDICTIONARY:
			g_DwdStore.SetParentTxdForSlot(strLocalIndex(GetDrawDictIndex()),strLocalIndex(txdSlot));
			break;
		case DT_FRAGMENT:
			g_FragmentStore.SetParentTxdForSlot(strLocalIndex(GetFragmentIndex()),strLocalIndex(txdSlot));
			break;
		default:
			Warningf("Drawable asset type not recognised for this archetype : %s", GetModelName());
	}
}

//
// name:		CBaseModelInfo::UpdateBoundingVolumes
// description:	Update the bounding volumes of the modelinfo to include this
void fwArchetype::UpdateBoundingVolumes(const phBound& bound)
{
	// Preserve the .w components!
	ScalarV lodDistance = GetLodDistanceUnscaledV();
	float hdTexDistance = GetHDTextureDistance();

	// calculate bounding box containing both the original and the new bounding box
	m_BoundingBox.GrowAABB(spdAABB(bound.GetBoundingBoxMin(), bound.GetBoundingBoxMax()));

	SetLodDistanceV(lodDistance);
	SetHDTextureDistance(hdTexDistance);

	// calculate the bounding sphere of both the original bounding spheres. 
	// calculate vector from original offset to new
	Vector3 distV = VEC3V_TO_VECTOR3(bound.GetCentroidOffset()) - GetBoundingSphereOffset();
	float dist = distV.Mag();
	// calculate extents and then centre
	float min = rage::Min(-GetBoundingSphereRadius(), dist-bound.GetRadiusAroundCentroid());
	float max = rage::Max(GetBoundingSphereRadius(), dist+bound.GetRadiusAroundCentroid());
	float centre = (min + max)/2.0f;

	// radius is distance between max and min halved
	SetBoundingSphere(GetBoundingSphereOffset() + (distV * centre) / rage::Max(0.001f, dist), (max - min) / 2.0f);
}

fwDynamicArchetypeComponent &fwArchetype::CreateDynamicComponentIfMissing()
{
	if (m_DynamicComponent == NULL)
	{
#if __BANK
		atPool<fwDynamicArchetypeComponent>* dynArchCompPool = fwDynamicArchetypeComponent::_ms_pPool;
		if (dynArchCompPool->IsFull())
		{
			// print the whole pool for debug purposes
			Errorf("Attempting to create a dynamic component but atPool<fwDynamicArchetypeComponent> is full !");
			fwArchetypeManager::DumpDynamicArchetypesComponents();
		}
#endif
		m_DynamicComponent = rage_new fwDynamicArchetypeComponent();
#if __DEV
		sm_DynamicComponentCount++;
#endif // __DEV
	}

	return *m_DynamicComponent;
}

void fwArchetype::DestroyDynamicComponent()
{
#if __DEV
	if (m_DynamicComponent)
	{
		sm_DynamicComponentCount--;
	}
#endif // __DEV

	delete m_DynamicComponent;
	m_DynamicComponent = NULL;
}

//
// name:		SetPhysics
// description:	Set models physics bounds
//
#define DEFAULT_OBJECT_DRAG_COEFF (0.05f)
#define DEFAULT_OBJECT_DOOR_ROT_DAMP (0.01f)
//
void fwArchetype::SetPhysics(phArchetypeDamp* pPhysics)
{	
	// Don't bother creating a physics component if we're not adding any physics.
	if (pPhysics == NULL && !GetDynamicComponentPtr())
	{
		return;
	}

	CreateDynamicComponentIfMissing();

	SetPhysicsOnly(pPhysics); 
	if(pPhysics != NULL)
	{
		if(pPhysics->GetBound())
		{
			UpdateBoundingVolumes(*pPhysics->GetBound());
		}

#if __ASSERT
		if(pPhysics->GetBound()->GetType()==phBound::BVH)// || m_pPhysicsArch->GetBound()->GetType()==phBound::GEOMETRY)
		{
#if COMPRESSED_VERTEX_METHOD == 0
			Assertf(((phBoundPolyhedron*)pPhysics->GetBound())->GetVertexPointer()==NULL, "%s : Bound has uncompressed vertices", GetModelName());
#else
			Assertf(((phBoundPolyhedron*)pPhysics->GetBound())->GetCompressedVertexPointer()!=NULL, "%s : Bound has no compressed vertices", GetModelName());
#endif
		}
#endif	//__ASSERT
	}
}

static const atHashValue nullName("null");

//
// name:		SetClipDictionaryIndex
// description:	Set the index of the clip dictionary required when loading this model
void fwArchetype::SetClipDictionaryIndex(const atHashString& clipDictionaryName)
{
	if(!AssertVerify(clipDictionaryName) || clipDictionaryName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetClipDictionaryIndexIndex(-1);
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetClipDictionaryIndex(clipDictionaryName);
}

#if ENABLE_BLENDSHAPES
//
// name:		SetBlendShapeFile
// description:	Set the blendshape file required when loading this model
void fwArchetype::SetBlendShapeFile(const atHashString& blendShapeName)
{
	if(!AssertVerify(blendShapeName) || blendShapeName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetBlendShapeFileIndex(-1);
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetBlendShapeFile(blendShapeName);
}
#endif // ENABLE_BLENDSHAPES

//
// name:		SetExpressionDictionaryIndex
// description:	Set the expression dictionary index used by this model
void fwArchetype::SetExpressionDictionaryIndex(const atHashString& expressionDictionaryName)
{
	if(!AssertVerify(expressionDictionaryName) || expressionDictionaryName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetExpressionDictionaryIndexIndex(-1);
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetExpressionDictionaryIndex(expressionDictionaryName);
}

//
// name:		SetExpressionHashName
// description:	Set the expression hash name used by this model
void fwArchetype::SetExpressionHash(const atHashString& expressionName)
{
	if(!AssertVerify(expressionName) || expressionName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetExpressionHashHash(atHashString::Null());
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetExpressionHashHash(expressionName);
}

//
// name:		SetPoseMatcherFile
// description:	Set the posematcher file required when loading this model
void fwArchetype::SetPoseMatcherFile(const atHashString& poseMatcherName)
{
	if(!AssertVerify(poseMatcherName) || poseMatcherName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetPoseMatcherFileIndex(-1);
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetPoseMatcherFile(poseMatcherName);
}

//
// name:		SetPoseMatcherProneFile
// description:	Set the posematcher prone file required when loading this model
void fwArchetype::SetPoseMatcherProneFile(const atHashString& poseMatcherName)
{
	if(!AssertVerify(poseMatcherName) || poseMatcherName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetPoseMatcherProneFileIndex(-1);
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetPoseMatcherProneFile(poseMatcherName);
}

//
// name:		SetCreatureMetadataFile
// description:	Set the CreatureMetadata file required when loading this model
void fwArchetype::SetCreatureMetadataFile(const atHashString& creatureMetadataName)
{
	if(!AssertVerify(creatureMetadataName) || creatureMetadataName == nullName)
	{
		if (GetDynamicComponentPtr())
		{
			GetDynamicComponentPtr()->SetCreatureMetadataFileIndex(-1);
		}
		return;
	}

	fwDynamicArchetypeComponent &dynComp = CreateDynamicComponentIfMissing();
	dynComp.SetCreatureMetadataFile(creatureMetadataName);
}

} // namespace rage
