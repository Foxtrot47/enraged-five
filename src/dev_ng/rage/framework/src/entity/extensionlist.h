//
// scene/extensionlist.h : base class for all entities in the world
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef ENTITY_EXTENSIONLIST_H_
#define ENTITY_EXTENSIONLIST_H_

#include "entity/factory.h"
#include "fwutil/PtrList.h"
#if !__SPU
#include "data/autoid.h"
#endif


namespace rage {

class fwEntity;
class fwArchetype;
class fwExtension;
class fwExtensionDef;

#if !__SPU

#define EXTENSIONID_DECL_ROOT(name)\
	AUTOID_DECL_ROOT(name);\
	virtual unsigned GetExtensionId() const = 0;

// this must be included in any class derived from fwExtension:
#define EXTENSIONID_DECL( name, family )\
	AUTOID_DECL(name, family);\
	unsigned GetExtensionId() const { return name::GetAutoId(); }

#define EXTENSIONID_DECL_PRIORITY( name, family, priority )\
	AUTOID_DECL_PRIORITY(name, family, priority);\
	unsigned GetExtensionId() const { return name::GetAutoId(); }

typedef fwFactoryBase< fwExtension >		fwExtensionFactoryBase;
typedef fwFactoryManager< fwExtension >		fwExtensionManager;

template <class T>
class fwExtensionFactory : public fwFactoryWithDynamicStore< T, fwExtension > { };

template <class T>
class fwExtensionPoolFactory : public fwFactoryWithPoolStore< T, fwExtension > { };
#endif //!__SPU...


// this is a dumb base class - specifically to allow us to describe a factory for handling 2dEffects
// DO NOT PUT ANYTHING IN HERE!!!!
// At some point in the future we'd like C2dEffect to be derived from fwExtension instead!
// class fw2dEffectEmptyBase{
// public:
// 	fw2dEffectEmptyBase() {}
// 	virtual ~fw2dEffectEmptyBase() {}
// };



//
// PURPOSE
//	fwExtension is a way of extending a class.  
class fwExtension
{
public:
	virtual ~fwExtension() {}

	virtual void InitEntityExtensionFromDefinition(const fwExtensionDef* UNUSED_PARAM(definition), fwEntity* UNUSED_PARAM(entity)) {}
	virtual void InitArchetypeExtensionFromDefinition(const fwExtensionDef* UNUSED_PARAM(definition), fwArchetype* UNUSED_PARAM(archetype)) {}

#if !__SPU
	EXTENSIONID_DECL_ROOT(fwExtension);
#endif //!__SPU...
};

//
// PURPOSE
//	fwExtensionList link list storing multiple extensions.  
class fwExtensionList
{
	enum {
		// Maximum number of extensions that can be looked up quickly through a bit check.
		MAX_BIT_MANAGED_EXTENSIONS = 32,
	};

public:
	fwExtensionList() : m_Head(NULL) {}
	~fwExtensionList();

	// PURPOSE: Adds a new extension to this entity
	void Add(fwExtension& extension);

	// PURPOSE: Gets the extension on the extension list with the given extension id
	fwExtension* Get(unsigned extensionID);
	const fwExtension* Get(unsigned extensionID) const;

	template<typename _Type> const _Type* GetExtension() const { return static_cast<const _Type*>(Get(_Type::GetAutoId())); }
	template<typename _Type> _Type* GetExtension() { return static_cast<_Type*>(Get(_Type::GetAutoId())); }

	/** PURPOSE: Remove an extension from the extension list and delete it (via delete).
	 *  NOTE that the extension will not be deleted if it hadn't been in the extension
	 *  list.
	 *
	 *  PARAMS:
	 *    pExt - Extension to remove and delete.
	 *
	 *  RETURNS:
	 *    TRUE if the extension had been in the extension list, FALSE otherwise.
	 */
	bool Destroy(fwExtension* pExt);

	// PURPOSE: Remove an extension from the extension list
	bool Unlink(fwExtension* pExt);

	// PURPOSE: Destroy extension on the extension list with the given extension id
	bool Destroy(unsigned extensionID);

	// PURPOSE: Destroy all extensions on the extension list
	void DestroyAll(void);

private:
	const fwExtension* InternalGet(unsigned extensionID) const;

	struct LinkedListNode
	{
		fwExtension* m_Data;
		LinkedListNode* m_Next;
	};

	LinkedListNode* m_Head;
	atFixedBitSet<MAX_BIT_MANAGED_EXTENSIONS> m_AvailableExtensions;
};

} // namespace rage

#endif // ENTITY_EXTENSIONLIST_H_
