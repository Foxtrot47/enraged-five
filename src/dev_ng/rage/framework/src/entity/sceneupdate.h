
#ifndef ENTITY_PROCESSCONTROL_H_
#define ENTITY_PROCESSCONTROL_H_

#include "atl/array.h"
#include "atl/pool.h"
#include "entity/extensionlist.h"
#include "fwutil/PtrList.h"
#include "system/timer.h"



namespace rage {

	class bkBank;
	class fwEntity;


class fwSceneUpdateExtension : public fwExtension
{
public:
#if !__SPU
	EXTENSIONID_DECL( fwSceneUpdateExtension, fwExtension );
#endif // !__SPU

	fwEntity*   m_entity;
	u32         m_sceneUpdateFlags;
#if __BANK
	u16			m_timeSpent[32];
	u16			m_sum;
#endif // __BANK
};



class fwSceneUpdate {
public:
	enum {
		// NOTE: Changing this requires changing the type of fwSceneUpdateExtension::m_sceneUpdateFlags
		MAX_SCENE_UPDATE_TYPES = 32,
		MAX_SCENE_UPDATE_NODES = 1560,

		// RAGE-level scene update bitmasks. Each scene update flag must be one bit.
		// Any game-level must be greater or equal than USER_SCENE_UPDATE.
		SU_DYNAMIC			= BIT0,

		USER_SCENE_UPDATE	= BIT1,
	};

	typedef void (*SceneUpdateCallback)(fwEntity &entity, void *userData);

	static void Init();

	static void Shutdown();

	static void Update(u32 sceneUpdateBit, void *userData = NULL);
	static void UpdateEntityType(u32 entityType, u32 sceneUpdateBit, void *userData = NULL);

	static bool IsInSceneUpdate(const fwEntity &entity);

	static void RunCustomCallbackBits(u32 sceneUpdateBits, SceneUpdateCallback updateCb, void *userData = NULL);
	static void RunCustomCallbackBitsOnEntityType(u32 entityType, u32 sceneUpdateBits, SceneUpdateCallback updateCb, void *userData = NULL);

	static void RegisterSceneUpdate(u32 sceneUpdateBit, SceneUpdateCallback updateCb, const char *debugName);
	static void OverrideSceneUpdate(u32 sceneUpdateBit, SceneUpdateCallback updateCb, const char *debugName);

	static void AddToSceneUpdate(fwEntity &entity, u32 sceneUpdateBits);

	static void RemoveFromSceneUpdate(fwEntity &entity, u32 sceneUpdateBits, bool forceImmediateRemoval = false);

#if __BANK
	static void AddWidgets(bkBank &bank);
	static void DisplaySceneUpdateList();
	static void DebugDraw3D();
	static const char *GetSceneUpdateName(u32 sceneUpdateBit);

	static void SetSelectedEntity(fwEntity *entity)		{ ms_SelectedEntity = entity; }
	static fwEntity *GetSelectedEntity()				{ return ms_SelectedEntity; }
#endif // __BANK


#if !__FINAL
	static void PrintSceneUpdateList();
#endif

private:
	static void ExecuteDelayedRemovals(const atArray<fwEntity *> &delayedRemovals);

#if __BANK
	static void AddSceneUpdateWidgets(u32 sceneUpdateBit);
	static void DebugDrawSceneUpdateCostInWorld(fwEntity &entity, void *userData);
	static void BeginTrace(fwEntity &entity, int bitIndex);
	static void EndTrace(fwEntity &entity, fwSceneUpdateExtension &extension, int bitIndex);
#else // __BANK
	static inline void BeginTrace(fwEntity &/*entity*/, int /*bitIndex*/) {}
	static inline void EndTrace(fwEntity &/*entity*/, fwSceneUpdateExtension &/*extension*/, int /*bitIndex*/) {}
#endif // __BANK

	static atRangeArray<SceneUpdateCallback, MAX_SCENE_UPDATE_TYPES> ms_callbacks; // list of callbacks for each flag that make calls on an entity.

	static atFixedArray<fwSceneUpdateExtension, MAX_SCENE_UPDATE_NODES> ms_SceneUpdateExtensions;

#if __BANK
	static atRangeArray<ConstString, MAX_SCENE_UPDATE_TYPES> ms_SceneUpdateNames;

	// If this is non-NULL, only this entity will be profiled (or updated, based on flags)
	static fwEntity *ms_SelectedEntity;
	static sysTimer ms_Timer;
#endif // __BANK
};


} // namespace rage

#endif // ENTITY_PROCESSCONTROL_H_
