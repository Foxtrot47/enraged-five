
#include "entity/sceneupdate.h"

#include "bank/bank.h"
#include "bank/toggle.h"
#include "entity/archetype.h"
#include "entity/entity.h"
#include "grcore/debugdraw.h"
#include "profile/cputrace.h"
#include "grprofile/pix.h"
#include "system/threadtype.h"
#include "vector/colors.h"

#if __PPU
#include <sn/libsntuner.h>
#endif // __PU

namespace rage {


AUTOID_IMPL(fwSceneUpdateExtension);


atFixedArray<fwSceneUpdateExtension, fwSceneUpdate::MAX_SCENE_UPDATE_NODES> fwSceneUpdate::ms_SceneUpdateExtensions;

atRangeArray<fwSceneUpdate::SceneUpdateCallback, fwSceneUpdate::MAX_SCENE_UPDATE_TYPES> fwSceneUpdate::ms_callbacks; // list of callbacks for each flag that make calls on an entity.

// If non-NULL, we're in the middle of an update on the update thread, and are not allowed to remove an
// entity from the scene update list. Instead, we'll add it to this array.
static atArray<fwEntity *> *s_DelayedRemovals;

#if __BANK
atRangeArray<ConstString, fwSceneUpdate::MAX_SCENE_UPDATE_TYPES> fwSceneUpdate::ms_SceneUpdateNames;
fwEntity *fwSceneUpdate::ms_SelectedEntity;
sysTimer fwSceneUpdate::ms_Timer;

static bkGroup *s_SceneUpdateSelectGroup;

static bool s_DisplaySceneUpdateList = false;
static bool s_DisplaySceneUpdateListLock = false;
bool g_DisplaySceneUpdateListByType = false;

// INDEX (not bit) of the scene update job to display.
// Remember to perform "1 << n" on this value to get the actual scene update bit.
static int s_SceneUpdateCostStepIndex = 0;
static bool s_ShowSceneUpdateCostInWorld = false;
static bool s_AddMarkers = false;
static bool s_MarkersForSelectedOnly = false;



atFixedBitSet<fwSceneUpdate::MAX_SCENE_UPDATE_TYPES> s_UpdateOnlySelectedType;
#endif // __BANK

void fwSceneUpdate::Init()
{
	sysMemSet(&ms_callbacks, 0, sizeof(ms_callbacks));	
}

void fwSceneUpdate::Shutdown()
{
}

#if __BANK
void fwSceneUpdate::BeginTrace(fwEntity &entity, int bitIndex)
{
	ms_Timer.Reset();

	if (s_AddMarkers)
	{
		if (!s_MarkersForSelectedOnly || GetSelectedEntity() == &entity)
		{
			char markerName[128];
			formatf(markerName, "SceneUpdate %s - p=0x%p %s", ms_SceneUpdateNames[bitIndex].c_str(), &entity, entity.GetModelName());
#if __PPU
			snStartMarker(30, markerName);  
#else
			PIXBegin(0, markerName);  
#endif
		}
	}
}

void fwSceneUpdate::EndTrace(fwEntity &entity, fwSceneUpdateExtension &extension, int bitIndex)
{
	// Temporarily turned the Assign() into a normal assignment to fix the assert from triggering during
	// GPAD captures. The container either needs to be bigger, or we need to not perform the assignment
	// during performance captures. /MAK
	//			BANK_ONLY(if (!s_DisplaySceneUpdateListLock)Assign(extension.m_timeSpent[bitIndex],round(timer.GetUsTime())));
	if (!s_DisplaySceneUpdateListLock)
	{
		extension.m_timeSpent[bitIndex] = (u16) round(ms_Timer.GetUsTime());
	}

	if (s_AddMarkers)
	{
		if (!s_MarkersForSelectedOnly || GetSelectedEntity() == &entity)
		{
#if __PPU
			snStopMarker(30);  
#else
			PIXEnd();  
#endif
		}
	}
}
#endif // __BANK

void fwSceneUpdate::RegisterSceneUpdate(u32 sceneUpdateBit, SceneUpdateCallback updateCb, const char *debugName)
{
	// Get "n" in "1 << n"
	ASSERT_ONLY(int sceneUpdateIndex = CountOnBits(sceneUpdateBit-1);)

	Assertf(ms_callbacks[sceneUpdateIndex] == NULL, "Callback for scene update type %d registered twice - use OverrideSceneUpdate if that's intentional.", sceneUpdateIndex);
	OverrideSceneUpdate(sceneUpdateBit, updateCb, debugName);

#if __BANK
	AddSceneUpdateWidgets(sceneUpdateBit);
#endif // __BANK
}

void fwSceneUpdate::OverrideSceneUpdate(u32 sceneUpdateBit, SceneUpdateCallback updateCb, const char *BANK_ONLY(debugName))
{
	Assertf(sceneUpdateBit!= 0, "The scene update bit must be a bit, not an index");
	Assertf((sceneUpdateBit & (sceneUpdateBit-1)) == 0, "The scene update bit must be a single bit");

	// Get "n" in "1 << n"
	int sceneUpdateIndex = CountOnBits(sceneUpdateBit-1);

	ms_callbacks[sceneUpdateIndex] = updateCb;
#if __BANK
	ms_SceneUpdateNames[sceneUpdateIndex] = debugName;
#endif // __BANK
}

bool fwSceneUpdate::IsInSceneUpdate(const fwEntity &entity)
{
	return (entity.GetExtension<fwSceneUpdateExtension>()!= NULL);
}

void fwSceneUpdate::AddToSceneUpdate(fwEntity &entity, u32 sceneUpdateBits)
{
	Assert(sysThreadType::IsUpdateThread());

	// Does this entity already have a scene update extension?
	fwSceneUpdateExtension *extension = entity.GetExtension<fwSceneUpdateExtension>();

	if (!extension)
	{
		// Nope - need to create one first.
		extension = &ms_SceneUpdateExtensions.Append();
		extension->m_sceneUpdateFlags = 0;
		extension->m_entity = &entity;
		entity.GetExtensionList().Add(*extension);
	}

	extension->m_sceneUpdateFlags |= sceneUpdateBits;
}

void fwSceneUpdate::RemoveFromSceneUpdate(fwEntity &entity, u32 sceneUpdateBits, bool forceImmediateRemoval)
{
	Assert(sysThreadType::IsUpdateThread());

	fwSceneUpdateExtension *extension = entity.GetExtension<fwSceneUpdateExtension>();

	if (extension)
	{
		extension->m_sceneUpdateFlags &= ~sceneUpdateBits;

		if (extension->m_sceneUpdateFlags == 0)
		{
			if (!forceImmediateRemoval && s_DelayedRemovals)
			{
				// We're in the middle of an update! Don't remove it just yet, just make a note that we're
				// going to have to remove it later.
				s_DelayedRemovals->Append() = &entity;
			}
			else
			{
				if (s_DelayedRemovals)
				{
					// Make sure we're not on the delayed removal list anymore.
					int index = s_DelayedRemovals->Find(&entity);

					Warningf("Removing %s from scene update while update is in progress (index=%d)", entity.GetModelName(), index);

					if (index != -1)
					{
						s_DelayedRemovals->DeleteFast(index);
					}
				}

				// If there's no reason for this extension to exist anymore,
				// kill it.
				entity.GetExtensionList().Unlink(extension);

				int index = ptrdiff_t_to_int(extension - ms_SceneUpdateExtensions.GetElements());

				Assert(&ms_SceneUpdateExtensions[index] == extension);

				// Keep a pointer to the last extension around, we might need it for relinking later.
				fwSceneUpdateExtension *lastExtension = &ms_SceneUpdateExtensions[ms_SceneUpdateExtensions.GetCount()-1];

				ms_SceneUpdateExtensions.DeleteFast(index);

				// If we just moved an extension around, we need to relink it.
				// The only exception is if we just deleted the last element in the list, in which case
				// nothing was shifted.
				if (ms_SceneUpdateExtensions.GetCount() > (int) index)
				{
					fwEntity *relinkedEntity = ms_SceneUpdateExtensions[index].m_entity;
					FastAssert(relinkedEntity);

					relinkedEntity->GetExtensionList().Unlink(lastExtension);
					relinkedEntity->GetExtensionList().Add(ms_SceneUpdateExtensions[index]);

					// Note that DeleteFast() already moved the extension around, so link from the
					// extension to the entity is correct. But hey, let's assert on that.
					Assert(ms_SceneUpdateExtensions[index].m_entity == relinkedEntity);
				}
			}
		}
	}
}

void fwSceneUpdate::ExecuteDelayedRemovals(const atArray<fwEntity *> &delayedRemovals)
{
	int count = delayedRemovals.GetCount();

	for (int x=0; x<count; x++)
	{
		fwEntity *entity = delayedRemovals[x];

		// Should we unlink this extension? Even though entities only get added to this list
		// if they had an extension and if their flags are 0, it's possible that an entity
		// was added twice, or it has re-added scene update flags in the meantime.
		// Calling RemoveFromSceneUpdate() with 0 will do both checks for us, and remove
		// the entity if applicable.
		RemoveFromSceneUpdate(*entity, 0);
	}
}

void fwSceneUpdate::Update(u32 sceneUpdateBit, void *userData) {

	Assertf((sceneUpdateBit & (sceneUpdateBit-1)) == 0, "fwSceneUpdate::Update() must be called with one single scene update flag");
	Assertf(sceneUpdateBit != 0, "fwSceneUpdate::Update() must be called with a scene update bit, not an index");

	// Get "n" in "1 << n"
	int sceneUpdateIndex = CountOnBits(sceneUpdateBit-1);

	SceneUpdateCallback callback = ms_callbacks[sceneUpdateIndex];
	Assertf(callback != NULL, "Trying to use scene update type %d, but it was never registered with RegisterSceneUpdate.", sceneUpdateIndex);


#if __BANK
	// Update the selected one only?
	if (s_UpdateOnlySelectedType.IsSet(sceneUpdateIndex))
	{
		fwEntity *entity = GetSelectedEntity();

		if (entity)
		{
			fwSceneUpdateExtension *extension = entity->GetExtension<fwSceneUpdateExtension>();
			if (extension)
			{
				if (extension->m_sceneUpdateFlags & sceneUpdateBit)
				{
					BeginTrace(*entity, sceneUpdateIndex);
					callback(*entity, userData);
					EndTrace(*entity, *extension, sceneUpdateIndex);
				}
			}
		}

		return;
	}
#endif // __BANK

	RunCustomCallbackBits(sceneUpdateBit, callback, userData);
}

void fwSceneUpdate::UpdateEntityType(u32 entityType, u32 sceneUpdateBit, void *userData) {

	Assertf((sceneUpdateBit & (sceneUpdateBit-1)) == 0, "fwSceneUpdate::Update() must be called with one single scene update flag");
	Assertf(sceneUpdateBit != 0, "fwSceneUpdate::Update() must be called with a scene update bit, not an index");

	// Get "n" in "1 << n"
	int sceneUpdateIndex = CountOnBits(sceneUpdateBit-1);

	SceneUpdateCallback callback = ms_callbacks[sceneUpdateIndex];
	Assertf(callback != NULL, "Trying to use scene update type %d, but it was never registered with RegisterSceneUpdate.", sceneUpdateIndex);

#if __BANK
	// Update the selected one only?
	if (s_UpdateOnlySelectedType.IsSet(sceneUpdateIndex))
	{
		fwEntity *entity = GetSelectedEntity();

		if (entity && u32(entity->GetType()) == entityType)
		{
			fwSceneUpdateExtension *extension = entity->GetExtension<fwSceneUpdateExtension>();
			if (extension)
			{
				if (extension->m_sceneUpdateFlags & sceneUpdateBit)
				{
					BeginTrace(*entity, sceneUpdateIndex);
					callback(*entity, userData);
					EndTrace(*entity, *extension, sceneUpdateIndex);
				}
			}
		}

		return;
	}
#endif // __BANK

	RunCustomCallbackBitsOnEntityType(entityType, sceneUpdateBit, callback, userData);
}

void fwSceneUpdate::RunCustomCallbackBits(u32 sceneUpdateBits, SceneUpdateCallback updateCb, void *userData)
{
	Assert(sysThreadType::IsUpdateThread());

	// Walk through all registered scene update extensions.
	//const int count = ms_SceneUpdateExtensions.GetCount();

	u32 bitIndex = CountOnBits(sceneUpdateBits-1);

	atUserArray<fwEntity *> delayedRemovals;
	if (sysThreadType::IsUpdateThread())
	{
		fwEntity **delayedRemovalStore = Alloca(fwEntity *, MAX_SCENE_UPDATE_NODES);
		delayedRemovals.Assume(delayedRemovalStore, MAX_SCENE_UPDATE_NODES);

		s_DelayedRemovals = &delayedRemovals;
	}

	// We're re-reading the count after every iteration - it could have changed.
	// We could consider walking through the list backward if we know the order
	// doesn't matter.
	for (int x=0; x<ms_SceneUpdateExtensions.GetCount(); x++)
	{
		fwSceneUpdateExtension & extension = ms_SceneUpdateExtensions[x];

		if (extension.m_sceneUpdateFlags & sceneUpdateBits)
		{
			fwEntity *entity = extension.m_entity;
			FastAssert(entity);
			BeginTrace(*entity, bitIndex);
			updateCb(*entity, userData);
			EndTrace(*entity, extension, bitIndex);
		}
	}

	if (sysThreadType::IsUpdateThread())
	{
		atArray<fwEntity *> &delayedRemovals = *s_DelayedRemovals;
		s_DelayedRemovals = NULL;

		// Now that we're done updating, let's see if we need to remove anything from the scene update.
		ExecuteDelayedRemovals(delayedRemovals);
	}
}

void fwSceneUpdate::RunCustomCallbackBitsOnEntityType(u32 entityType, u32 sceneUpdateBits, SceneUpdateCallback updateCb, void *userData /* = NULL */)
{
	Assert(sysThreadType::IsUpdateThread());

	// Walk through all registered scene update extensions.
	//const int count = ms_SceneUpdateExtensions.GetCount();

	u32 bitIndex = CountOnBits(sceneUpdateBits-1);

	atUserArray<fwEntity *> delayedRemovals;
	if (sysThreadType::IsUpdateThread())
	{
		fwEntity **delayedRemovalStore = Alloca(fwEntity *, MAX_SCENE_UPDATE_NODES);
		delayedRemovals.Assume(delayedRemovalStore, MAX_SCENE_UPDATE_NODES);
		s_DelayedRemovals = &delayedRemovals;
	}

	// We're re-reading the count after every iteration - it could have changed.
	// We could consider walking through the list backward if we know the order
	// doesn't matter.
	for (int x=0; x<ms_SceneUpdateExtensions.GetCount(); x++)
	{
		fwSceneUpdateExtension& extension = ms_SceneUpdateExtensions[x];

		if ((u32(extension.m_entity->GetType()) == entityType) && (extension.m_sceneUpdateFlags & sceneUpdateBits))
		{
			fwEntity *entity = extension.m_entity;
			FastAssert(entity);
			BeginTrace(*entity, bitIndex);
			updateCb(*entity, userData);	
			EndTrace(*entity, extension, bitIndex);
		}
	}

	if (sysThreadType::IsUpdateThread())
	{
		atArray<fwEntity *> &delayedRemovals = *s_DelayedRemovals;
		s_DelayedRemovals = NULL;

		// Now that we're done updating, let's see if we need to remove anything from the scene update.
		ExecuteDelayedRemovals(delayedRemovals);
	}
}

#if __BANK
void fwSceneUpdate::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Scene Update");
	bank.AddToggle("Display Scene Update List", &s_DisplaySceneUpdateList);
	bank.AddToggle("Lock", &s_DisplaySceneUpdateListLock );
	bank.AddToggle("Timebar Update by Type", &g_DisplaySceneUpdateListByType );
	bank.AddButton("Dump Scene Update List", fwSceneUpdate::PrintSceneUpdateList);
	bank.AddToggle("Display Update Cost In World", &s_ShowSceneUpdateCostInWorld);
	bank.AddSlider("Scene Update Step To Visualize", &s_SceneUpdateCostStepIndex, 0, MAX_SCENE_UPDATE_TYPES, 1);
	bank.AddToggle("Add Profile Markers For Update", &s_AddMarkers);
	bank.AddToggle("Add Markers For Selected Object Only", &s_MarkersForSelectedOnly);

	s_SceneUpdateSelectGroup = bank.PushGroup("Scene Update Selection");

	// Add widgets for all update tasks that have been registered so far.
	for (int x=0; x<MAX_SCENE_UPDATE_TYPES; x++)
	{
		AddSceneUpdateWidgets((u32) (1 << x));
	}
	bank.PopGroup();

	bank.PopGroup();
}

void fwSceneUpdate::AddSceneUpdateWidgets(u32 sceneUpdateBit)
{
	// If this is NULL, widgets haven't been created yet.
	// In that case, don't worry - AddWidgets() will add everything that has been
	// registered up to this point.
	if (!s_SceneUpdateSelectGroup)
	{
		return;
	}

	int index = CountOnBits(sceneUpdateBit-1);

	const char *name = ms_SceneUpdateNames[index].c_str();
	bool enabled = (name && name[0] != 0);

	if (enabled)
	{
		char optionName[64];
		formatf(optionName, "%s - Update Selected Only", name);

		// Note that there is an AddToggle variant that takes an atFixedBitSet.
		// Unfortunately, it's broken in several levels.
		CompileTimeAssert(MAX_SCENE_UPDATE_TYPES <= 32);
		u32 *bitMask = (u32 *) &s_UpdateOnlySelectedType;
		s_SceneUpdateSelectGroup->AddToggle(optionName, bitMask, (u32) (1 << index));
	}
}

void fwSceneUpdate::DebugDraw3D()
{
	if (s_ShowSceneUpdateCostInWorld)
	{
		fwSceneUpdate::RunCustomCallbackBits(1 << s_SceneUpdateCostStepIndex, DebugDrawSceneUpdateCostInWorld, &s_SceneUpdateCostStepIndex);
	}
}

void fwSceneUpdate::DebugDrawSceneUpdateCostInWorld(fwEntity &entity, void *userData)
{
	int updateToProfile = *((int *) userData);
	Vec3V position = entity.GetTransform().GetPosition();

	fwSceneUpdateExtension *extension = entity.GetExtension<fwSceneUpdateExtension>();
	FastAssert(extension);	// If we get to this function, it must have an extension.

	float timeSpent = (float) extension->m_timeSpent[updateToProfile];

	float radius = timeSpent / 500.0f;
	float colorRange = timeSpent / 100.0f;
	colorRange = 1.0f - Clamp(colorRange, 0.0f, 1.0f);

	Color32 color(1.0f, colorRange, colorRange);

	grcDebugDraw::Sphere(position, radius, color, false);

	char label[128];
	formatf(label, "%s (%p)\n%s %.3fms", entity.GetModelName(), &entity, ms_SceneUpdateNames[updateToProfile].c_str(), timeSpent / 1000.0f);
	grcDebugDraw::Text(position, Color_white, -30, -5, label);
}


static int CompareExtension(fwSceneUpdateExtension* const* a, fwSceneUpdateExtension* const* b)
{
	return (*b)->m_sum - (*a)->m_sum;
}

void fwSceneUpdate::DisplaySceneUpdateList()
{
	if (s_DisplaySceneUpdateList)
	{
		// Sort and collect sum
		int count = ms_SceneUpdateExtensions.GetCount();
		int sum = 0;
		atArray<fwSceneUpdateExtension*> sorted;

		sorted.Reserve(count);
		for (int x=0; x<count; x++)
		{
			fwSceneUpdateExtension *pExtension = &ms_SceneUpdateExtensions[x];
			pExtension->m_sum = 0;

			for (int i = 0; i < 32; ++i)
			{
				pExtension->m_sum += pExtension->m_timeSpent[i];
			}
			sum += pExtension->m_sum;
			
			sorted.Push(pExtension);
		}
		sorted.QSort(0, -1, CompareExtension);

		char line[256];
		bool enabled[MAX_SCENE_UPDATE_TYPES];
		formatf(line, "%-32s", "Name");
		char element[12];
		for (int y=0; y<MAX_SCENE_UPDATE_TYPES; y++)
		{
			const char *name = ms_SceneUpdateNames[y].c_str();

			enabled[y] = (name && name[0] != 0);

			if (!enabled[y])
			{
				continue;
			}

			formatf(element, "%-8s", name);
			safecat(line, element);
		}

		formatf(element, "%7d ", sum);
		safecat(line, element);

		grcDebugDraw::AddDebugOutputEx(false, line);

		int counts[MAX_SCENE_UPDATE_TYPES];
		memset(counts, 0, sizeof(counts));

		for (int x=0; x<count; x++)
		{
			fwSceneUpdateExtension *pExtension = sorted[x];
			fwEntity *pEnt = pExtension->m_entity;

			const fwArchetype* pArchetype = pEnt->GetArchetype();
			if(pArchetype)
			{
				formatf(line, "%08X %-22s", pEnt, pArchetype->GetModelName());

				char element[12];
				for (int y=0; y<MAX_SCENE_UPDATE_TYPES; y++)
				{
					if (!enabled[y])
					{
						continue;
					}

					if ((pExtension->m_sceneUpdateFlags & (1 << y)))
					{
						formatf(element, "  %4d  ", pExtension->m_timeSpent[y]);
					}
					else
					{	
						sprintf(element, "        ");
					}
					
					safecat(line, element);

					if (pExtension->m_sceneUpdateFlags & (1 << y))
					{
						counts[y]++;
					}
				}

				formatf(element, "  %5d ", pExtension->m_sum);
				safecat(line, element);

				grcDebugDraw::AddDebugOutputEx(false, line);
			}
		}

		formatf(line, "%-32s", "Counts");

		for (int y=0; y<MAX_SCENE_UPDATE_TYPES; y++)
		{
			if (!enabled[y])
			{
				continue;
			}

			
			formatf(element, "  %3d   ", counts[y]);
			safecat(line, element);
		}

		formatf(element, "%7d ", sum);
		safecat(line, element);

		grcDebugDraw::AddDebugOutputEx(false, line);
	}
}

const char *fwSceneUpdate::GetSceneUpdateName(u32 sceneUpdateBit)
{
	int sceneUpdateIndex = CountOnBits(sceneUpdateBit-1);
	return ms_SceneUpdateNames[sceneUpdateIndex];
}
#endif // __BANK


#if !__FINAL
void fwSceneUpdate::PrintSceneUpdateList()
{
	Displayf("Scene update list spew");
	int count = ms_SceneUpdateExtensions.GetCount();

	Printf("Idx ,%-22s", "Name");
	for (int y=0; y<MAX_SCENE_UPDATE_TYPES; y++)
	{
		Printf(",%-2d", y);
	}
	Printf("\n");


	for (int x=0; x<count; x++)
	{
		fwSceneUpdateExtension *pExtension = &ms_SceneUpdateExtensions[x];
		fwEntity *pEnt = pExtension->m_entity;

		fwArchetype* pArchetype = fwArchetypeManager::GetArchetype(pEnt->GetModelIndex());
		if(pArchetype)
		{
			Printf("%-4i,%-22s", x, pArchetype->GetModelName());

			for (int y=0; y<MAX_SCENE_UPDATE_TYPES; y++)
			{
				Printf(",%c ", (pExtension->m_sceneUpdateFlags & (1 << y)) ? 'X' : ' ');
			}

			Printf("\n");
		}
	}
}
#endif // !__FINAL

} // namespace rage
