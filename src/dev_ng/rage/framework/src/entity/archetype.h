//
// scene/entity.h : base class for all entities in the world
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef ENTITY_ARCHETYPE_H_
#define ENTITY_ARCHETYPE_H_

#include "data/base.h"
#include "dynamicarchetype.h"
#include "extensionlist.h"
#include "spatialdata/aabb.h"
#include "streaming/streamingmodule.h"
#include "entity/archetypemanager.h"
#include "grblendshapes/blendshapes_config.h"


#if __SPU
#define PPUVIRTUAL
#else
#define PPUVIRTUAL virtual
#endif

#if __WIN32
#pragma warning (push)
#pragma warning (disable : 4800)
#endif

namespace rage {

class fragType;
class phArchetypeDamp;
class phBound;
class rmcDrawable;
class fwArchetypeDef;
class fwEntityDef;
class fwPropInstanceListDef;
class fwGrassInstanceListDef;
class fwEntity;

#define INVALID_DRAWABLE_IDX	(0xffffffff)
#define INVALID_FRAG_IDX		(0xffffffff)
#define INVALID_DRAWDICT_IDX	(0xffffffff)

//
// PURPOSE
//	fwArchetype is the base class for all archetypes
class fwArchetype : public datBase
{
public:
	enum DrawableType {
		DT_UNINITIALIZED,
		DT_FRAGMENT,
		DT_DRAWABLE,
		DT_DRAWABLEDICTIONARY,
		DT_ASSETLESS,
		DT_COUNT
	};

	enum {
		MODEL_HAS_LOADED =						(1<<0),
		MODEL_HAS_ANIMATION =					(1<<1),
		MODEL_DRAW_LAST	=						(1<<2)
	};

	fwArchetype() : m_assignedStreamingSlot(fwArchetypeManager::INVALID_STREAM_SLOT), m_streaming(false), m_DynamicComponent(NULL) {}
	PPUVIRTUAL ~fwArchetype();

	// PURPOSE: Initialise archetype
	virtual void Init();
	virtual void InitArchetypeFromDefinition(strLocalIndex mapTypeDefIndex, fwArchetypeDef* definition, bool bHasAssetReferences);
	virtual fwEntity* CreateEntity();
	virtual fwEntity* CreateEntityFromDefinition(const fwPropInstanceListDef* definition);
	virtual fwEntity* CreateEntityFromDefinition(const fwGrassInstanceListDef* definition);
#if __BANK
	virtual void DebugPostInit() const = 0;
#endif // __BANK

	virtual bool CheckIsFixed() = 0;
	virtual bool WillGenerateBuilding() = 0;

	virtual bool IsInteriorPhysicsBindingSupressed()  { return(false); }

	// PURPOSE: Shutdown archetype
	virtual void Shutdown();

	virtual const u16* GetLodSkeletonMap() const { return NULL; }
	virtual u16 GetLodSkeletonBoneNum() const { return 0; }

	inline void FlagModelAsMissing() { m_bIsModelMissing = true; } // not in the drawable dictionary its meant to be in, so stop the streaming getting stuck on it
	inline bool IsModelMissing() const {return m_bIsModelMissing;}

	inline u32 GetHashKey() const {return GetModelNameHash();}

	inline bool GetHasLoaded() const {return IsFlagSet(MODEL_HAS_LOADED);}
	inline void SetHasLoaded(u32 bLoaded) {if(bLoaded) SetFlag(MODEL_HAS_LOADED); else ClearFlag(MODEL_HAS_LOADED);}

	// PURPOSE: Set model name
	inline void SetModelName(const strStreamingObjectName name) { m_modelName = name; }
	// PURPOSE: Get hash key of model name. Used as an identifier for the archetype
	inline u32 GetModelNameHash() const {return m_modelName.GetHash();}
	// PURPOSE: Get model name. This is only available in non final builds

#if !__NO_OUTPUT
	inline const char* GetModelName() const {return m_modelName.TryGetCStr();}
	static const char* GetModelName(u32 index) { return fwArchetypeManager::GetArchetypeName(index); }
#endif

	// Streaming
	inline bool IsStreamedArchetype(void) const { return m_streaming; }
	inline void SetTempStreamSlot(u16 slotId) { m_streaming = true; m_assignedStreamingSlot = slotId; }
	inline void SetPermanentStreamSlot(u16 slotId) { m_streaming = false; m_assignedStreamingSlot = slotId; }
	inline void InvalidateStreamSlot() { m_streaming = false; m_assignedStreamingSlot = fwArchetypeManager::INVALID_STREAM_SLOT; }
	inline strLocalIndex GetStreamSlot() const { return strLocalIndex(m_assignedStreamingSlot); }

	// PURPOSE: Return archetype flags
	inline u32 GetArchetypeFlags() const {return m_flags;}
	// PURPOSE: Return if a flag is set
	inline bool IsFlagSet(u32 flag) const {return (m_flags & flag);}
	// PURPOSE: Set a specific flag 
	inline void SetFlag(u32 flag) {Assertf(flag < (u32) BIT(31), "MSB is reserved for m_streaming!"); m_flags |= flag;}
	// PURPOSE: Clear a specific flag
	inline void ClearFlag(u32 flag) {m_flags &= ~flag;}

	// PURPOSE: Get list of extensions attached to this archetype
	const fwExtensionList& GetExtensionList() const {return m_extensionList;}
	fwExtensionList& GetExtensionList() {return m_extensionList;}

	template<typename _Type> const _Type* GetExtension() const { return static_cast<const _Type*>(m_extensionList.Get(_Type::GetAutoId())); }
	template<typename _Type> _Type* GetExtension() { return static_cast<_Type*>(m_extensionList.Get(_Type::GetAutoId())); }

	// lod distance - should use the lod value stored per instance in preference to these
	inline float GetLodDistanceUnscaled() const		{ return m_BoundingBox.GetUserFloat1f(); }
	inline ScalarV_Out GetLodDistanceUnscaledV() const	{ return m_BoundingBox.GetUserFloat1(); }
	inline void SetLodDistance(float dist)			{ m_BoundingBox.SetUserFloat1f(dist); }
	inline void SetLodDistanceV(ScalarV_In dist)	{ m_BoundingBox.SetUserFloat1(dist); }

	inline float GetHDTextureDistance() const    { return m_BoundingBox.GetUserFloat2f(); }
	inline void SetHDTextureDistance(float dist) { m_BoundingBox.SetUserFloat2f(dist); }

	// set / get asset for this archetype
	void SetDrawDictFile(const strStreamingObjectName pName, s32 parentTxdIdx);
	bool SetDrawableOrFragmentFile(const strStreamingObjectName pName, s32 parentTxdIdx, bool bIsFragment);
	void SetDrawableTypeAsAssetless(void) { m_DrawableType = DT_ASSETLESS; }

	inline s32 GetDrawDictIndex() const { Assert(m_DrawableType == DT_DRAWABLEDICTIONARY); return m_drawableDictionaryIndex;}
	inline s32 GetDrawableIndex() const { Assert(m_DrawableType == DT_DRAWABLE); return m_drawableIndex; }
	inline s32 GetFragmentIndex() const { Assert(m_DrawableType == DT_FRAGMENT); return m_fragmentIndex; }

	void ResetAssetReference(void) { m_DrawableType = DT_UNINITIALIZED; m_drawableIndex = INVALID_DRAWABLE_IDX;}

	rmcDrawable* GetDrawable() const;			// get it from the store!
	fragType* GetFragType() const;			// get it from the store!

	DrawableType GetDrawableType() const	{ return (DrawableType) m_DrawableType; }
	
	inline s32 GetDrawDictDrawableIndex() const {return m_drawableDictionaryDrawableIndex;}
	void SetDrawDictDrawIndex(int s) {Assign(m_drawableDictionaryDrawableIndex,s);}

	s32 GetAssetParentTxdIndex() const;
	void SetAssetParentTxdIndex(s32 txdIdx);

	strLocalIndex FindTxdSlotIndex(const strStreamingObjectName pName);

	// PURPOSE: Add a reference to this archetype
	inline void AddRef()				{ m_numRefs++; }
	// PURPOSE: Remove a reference from this archetype
	inline void RemoveRef()				{ m_numRefs--; }
	// PURPOSE: Return number of references
	inline int GetNumRefs() const {return m_numRefs;}

	// reference counting
	// PURPOSE: Add a reference to this archetype and add ref to any HD assets loaded
	PPUVIRTUAL void AddHDRef(bool) { Assert(false); }
	// PURPOSE: Remove a reference from this archetype and remove ref from any HD assets loaded
	PPUVIRTUAL void RemoveHDRef(bool) { Assert(false); }

	inline u32 GetHasAnimation() const { return IsFlagSet(MODEL_HAS_ANIMATION); }
	inline void SetHasAnimation(u32 isFixedForNavigation) {if(isFixedForNavigation) SetFlag(MODEL_HAS_ANIMATION); else ClearFlag(MODEL_HAS_ANIMATION);}
	inline u32 GetDrawLast() const {return IsFlagSet(MODEL_DRAW_LAST);}
	inline void SetDrawLast(u32 drawlast) {if(drawlast) SetFlag(MODEL_DRAW_LAST); else ClearFlag(MODEL_DRAW_LAST);}


	// physics bounds
	inline bool GetHasBoundInDrawable() const				{ return m_DrawableType==DT_DRAWABLE && m_DynamicComponent && m_DynamicComponent->GetHasPhysicsInDrawable(); }
	inline void SetHasBoundInDrawable()						{ CreateDynamicComponentIfMissing(); m_DynamicComponent->SetHasPhysicsInDrawable(true); }
	inline bool HasPhysics() const							{ return (m_DynamicComponent && m_DynamicComponent->GetPhysicsArch() != NULL); }
	inline phArchetypeDamp* GetPhysics()					{ return (m_DynamicComponent) ? m_DynamicComponent->GetPhysicsArch() : NULL; }
	PPUVIRTUAL void SetPhysics(phArchetypeDamp* pPhysicsArchetype);
	void SetPhysicsOnly(phArchetypeDamp* pPhysicsArchetype)	{ CreateDynamicComponentIfMissing(); m_DynamicComponent->SetPhysicsArch(pPhysicsArchetype); }

	void SetClipDictionaryIndex(const atHashString& name);
	inline strLocalIndex GetClipDictionaryIndex() const;

	inline void SetAutoStartAnim(bool bAutoStartAnim);
	inline bool GetAutoStartAnim() const;

#if ENABLE_BLENDSHAPES
	void SetBlendShapeFile(const atHashString& blendShapeName);
	inline s32 GetBlendShapeFileIndex() const;
#endif // ENABLE_BLENDSHAPES

	void SetExpressionDictionaryIndex(const atHashString& expressionDictionaryName);
	inline strLocalIndex GetExpressionDictionaryIndex() const;

	void SetExpressionHash(const atHashString& expressionName);
	inline atHashString GetExpressionHash() const;

	void SetPoseMatcherFile(const atHashString& poseMatcherName);
	inline strLocalIndex GetPoseMatcherFileIndex() const;

	void SetPoseMatcherProneFile(const atHashString& poseMatcherName);
	inline s32 GetPoseMatcherProneFileIndex() const;

	void SetCreatureMetadataFile(const atHashString& creatureMetadataName);
	inline strLocalIndex GetCreatureMetadataFileIndex() const;

	void CopyMaterialFlagsToBound(phBound* pBound);

	fwDynamicArchetypeComponent &CreateDynamicComponentIfMissing();
	void DestroyDynamicComponent();

	fwDynamicArchetypeComponent *GetDynamicComponentPtr() const	{ return m_DynamicComponent; }
	fwDynamicArchetypeComponent &GetDynamicComponent()			{ FastAssert(m_DynamicComponent); return *m_DynamicComponent; }

	// bounding box
	inline const spdAABB& GetBoundingBox() const			{ return m_BoundingBox; }
	inline const Vector3& GetBoundingBoxMin() const			{ return m_BoundingBox.GetMinVector3(); }
	inline const Vector3& GetBoundingBoxMax() const			{ return m_BoundingBox.GetMaxVector3(); }
	inline Vector3 GetBoundingSphereOffset() const			{ return m_BoundingSphere.GetVector3(); }
	inline float GetBoundingSphereRadius() const			{ return m_BoundingSphere.w; }
	inline Vec4V_Out GetBoundingSphereV4() const			{ return RCC_VEC4V(m_BoundingSphere); }
	inline void SetBoundingBoxMin(const Vector3& vecMin);
	inline void SetBoundingBoxMax(const Vector3& vecMax);
	inline void SetBoundingBox(const spdAABB& aabb);

	inline void SetBoundingSphere(const Vector3& vecOffset, float fRadius);
	void UpdateBoundingVolumes(const phBound& bound);


#if __BANK
	// these return -1 instead of asserting ..
	inline s32 SafeGetDrawableIndex() const
	{
		return (m_DrawableType==DT_DRAWABLE && m_drawableIndex!=INVALID_DRAWABLE_IDX) ? m_drawableIndex : -1;
	}
	inline s32 SafeGetFragmentIndex() const
	{
		return (m_DrawableType==DT_FRAGMENT && m_fragmentIndex!=INVALID_FRAG_IDX) ? m_fragmentIndex : -1;
	}
	inline s32 SafeGetDrawDictIndex() const
	{
		return (m_DrawableType==DT_DRAWABLEDICTIONARY && m_drawableDictionaryIndex!=INVALID_DRAWDICT_IDX) ? m_drawableDictionaryIndex : -1;
	}
	inline s32 SafeGetDrawDictEntry() const
	{
		return m_DrawableType==DT_DRAWABLEDICTIONARY ? m_drawableDictionaryDrawableIndex : -1;
	}
#endif // __BANK


#if __DEV
	static int GetDynamicComponentCount()					{ return sm_DynamicComponentCount; }
#endif // __DEV


private:
	fwExtensionList m_extensionList;			// +4
	strStreamingObjectName m_modelName;			// +12

	// use system/findSize.h and system/findOffset.h to determine best placement of these aligned structures!
	Vector4	m_BoundingSphere;					// +16
	spdAABB m_BoundingBox;						// +32

	u32 m_flags : 31;							// +64
	u32 m_streaming : 1;

	fwDynamicArchetypeComponent *m_DynamicComponent;	// +68

	// This is of DrawableType to specify which index of the union to use
	u8 m_DrawableType;							// +72
	
	u8 m_drawableDictionaryDrawableIndex;		// +73

	union {
		u32 m_fragmentIndex;
		u32 m_drawableIndex;
		u32 m_drawableDictionaryIndex;
	};											// +74

	u16 m_numRefs			: 15;				// +76
	u16 m_bIsModelMissing	: 1;				// (this is debug, but #defining it as !FINAL makes things messy)

	u16	m_assignedStreamingSlot;				// +78

	
	// ---- 
	// 80 bytes
	// ----

#if __DEV
	static int sm_DynamicComponentCount;
#endif // __DEV
};

inline void fwArchetype::SetBoundingBoxMin(const Vector3& vecMin)
{
	// Preserve the .w component!
	m_BoundingBox.SetMinPreserveUserData(RCC_VEC3V(vecMin));
}

inline void fwArchetype::SetBoundingBoxMax(const Vector3& vecMax)
{
	// Preserve the .w component!
	m_BoundingBox.SetMaxPreserveUserData(RCC_VEC3V(vecMax));
}

inline void fwArchetype::SetBoundingBox(const spdAABB& aabb)
{
	// Preserve the .w component!
	m_BoundingBox.SetMinPreserveUserData(aabb.GetMin());
	m_BoundingBox.SetMaxPreserveUserData(aabb.GetMax());
}


// [SPHERE-OPTIMISE]
inline void fwArchetype::SetBoundingSphere(const Vector3& vecOffset, float fRadius)
{
	m_BoundingSphere.Set(vecOffset.x, vecOffset.y, vecOffset.z, fRadius);
}

inline strLocalIndex fwArchetype::GetClipDictionaryIndex() const
{
	return GetDynamicComponentPtr() ? strLocalIndex(GetDynamicComponentPtr()->GetClipDictionaryIndex()) : strLocalIndex(-1);
}

inline void fwArchetype::SetAutoStartAnim(bool bAutoStartAnim)
{
	if (bAutoStartAnim)
	{
		CreateDynamicComponentIfMissing();
	}
	if (GetDynamicComponentPtr())
	{
		GetDynamicComponentPtr()->SetAutoStartAnim(bAutoStartAnim);
	}
}

inline bool fwArchetype::GetAutoStartAnim() const
{
	return GetDynamicComponentPtr() ? GetDynamicComponentPtr()->GetAutoStartAnim() : false;
}

#if ENABLE_BLENDSHAPES
inline s32 fwArchetype::GetBlendShapeFileIndex() const
{
	return GetDynamicComponentPtr() ? GetDynamicComponentPtr()->GetBlendShapeFileIndex() : -1;
}
#endif // ENABLE_BLENDSHAPES

inline strLocalIndex fwArchetype::GetExpressionDictionaryIndex() const
{
	return GetDynamicComponentPtr() ? strLocalIndex(GetDynamicComponentPtr()->GetExpressionDictionaryIndex()) : strLocalIndex(-1);
}

inline atHashString fwArchetype::GetExpressionHash() const
{
	return GetDynamicComponentPtr() ? GetDynamicComponentPtr()->GetExpressionHash() : atHashString::Null();
}

inline strLocalIndex fwArchetype::GetPoseMatcherFileIndex() const
{
	return GetDynamicComponentPtr() ? strLocalIndex(GetDynamicComponentPtr()->GetPoseMatcherFileIndex()) : strLocalIndex(-1);	
}

inline s32 fwArchetype::GetPoseMatcherProneFileIndex() const
{
	return GetDynamicComponentPtr() ? GetDynamicComponentPtr()->GetPoseMatcherProneFileIndex() : -1;	
}

inline strLocalIndex fwArchetype::GetCreatureMetadataFileIndex() const
{
	return GetDynamicComponentPtr() ? strLocalIndex(GetDynamicComponentPtr()->GetCreatureMetadataFileIndex()) : strLocalIndex(-1);	
}


#if !__SPU
CompileTimeAssertSize(fwArchetype, 80, 112);
#endif

#if __WIN32
#pragma warning (pop)
#endif

} // namespace rage

#endif // SCENE_ENTITY_H_
