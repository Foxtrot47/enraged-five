//
// scene/transform.cpp : classes representing a 3d transform 
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "transform.h"

#include "vectormath/classfreefuncsv.h"
#include "system/timer.h"
#include "system/ipc.h"
#include "system/param.h"
#include "grcore/debugdraw.h"

#include "bank/bank.h"

namespace rage {

CompileTimeAssertSize(fwIdentityTransform,16,16);
CompileTimeAssertSize(fwMatrixTransform,64,64);
CompileTimeAssertSize(fwMatrixScaledTransform,64,64);
CompileTimeAssertSize(fwQuaternionTransform,48,48);
CompileTimeAssertSize(fwQuaternionScaledTransform,48,48);
CompileTimeAssertSize(fwSimpleTransform,32,32);
CompileTimeAssertSize(fwSimpleScaledTransform,32,32);

XPARAM(noquattransform);

fwIdentityTransform fwIdentityTransform::sm_Instance;

#if FWTRANSFORM_USE_PAGED_POOLS
	FW_INSTANTIATE_CLASS_PAGEDPOOL( fwMatrixTransform, CONFIGURED_FROM_FILE, 512, atHashString("fwMatrixTransform",0x1bfcef17) );
	FW_INSTANTIATE_CLASS_PAGEDPOOL( fwSimpleTransform, CONFIGURED_FROM_FILE, 512, atHashString("fwSimpleTransform",0x12c60340) );
	FW_INSTANTIATE_CLASS_PAGEDPOOL( fwQuaternionTransform, CONFIGURED_FROM_FILE, 682, atHashString("fwQuaternionTransform",0x30dc3503) );
#else // FWTRANSFORM_USE_PAGED_POOLS
	FW_INSTANTIATE_CLASS_POOL( fwMatrixTransform, CONFIGURED_FROM_FILE, atHashString("fwMatrixTransform",0x1bfcef17) );
	FW_INSTANTIATE_CLASS_POOL( fwSimpleTransform, CONFIGURED_FROM_FILE, atHashString("fwSimpleTransform",0x12c60340) );
	FW_INSTANTIATE_CLASS_POOL( fwQuaternionTransform, CONFIGURED_FROM_FILE, atHashString("fwQuaternionTransform",0x30dc3503) );
#endif // FWTRANSFORM_USE_PAGED_POOLS

// We're combining pools, so these classes need to have the same size.
CompileTimeAssert(sizeof(fwMatrixTransform) == sizeof(fwMatrixScaledTransform));
CompileTimeAssert(sizeof(fwSimpleTransform) == sizeof(fwSimpleScaledTransform));

#if !__FINAL
static const char *s_TransformClassNames[] = {
	"fwSimpleTransform",
	"fwMatrixTransform",
	"fwSimpleScaledTransform",
	"fwMatrixScaledTransform",
	"fwIdentityTransform",
	NULL,
	NULL,
	NULL,
	"fwQuaternionTransform",
	NULL,
	"fwQuaternionScaledTransform",
};
#endif // !__FINAL

void fwTransform::InitPools()
{
	fwMatrixTransform::InitPool(MEMBUCKET_WORLD);
	fwSimpleTransform::InitPool(MEMBUCKET_WORLD);
	fwQuaternionTransform::InitPool(MEMBUCKET_WORLD);


	//Removed noquattransforms from FINAL as requested in url:bugstar:2165153
	//The command line is only used when ENABLE_MATRIX_MEMBER isn't enabled and it currently is.
#if !__FINAL
	// TEMP HACK: If we have don't have a lot of quaternion transforms, it
	// means that we disabled them in the XML file. Don't use them then.
	if (fwQuaternionTransform::GetPool()->GetFreeCount() < 100)
	{
		PARAM_noquattransform.Set("");
	}
#endif //__FINAL
}

void fwTransform::ShutdownPools()
{
	fwQuaternionTransform::ShutdownPool();
	fwMatrixTransform::ShutdownPool();
	fwSimpleTransform::ShutdownPool();
}

QuatV_Out fwSimpleTransform::GetOrientation() const
{
#if 1
	return QuatVFromZAxisAngle(ScalarVFromF32(m_Heading));
#else
	// Something like this should work, since we already have m_SinHeading and m_CosHeading.
	// Need to measure timing more closely to see which is better.
	// Could also store cos(Heading/2) in m_Position.w to get rid of the sqrt

	// See QuatVFromZAxisAngle... we want [0, 0, s(H/2), c(H/2)]
	// We have s(H), c(H), H.
	// c(H) = 2 c^2(H/2) - 1
	// s(H) = s( 2 * (H/2) ) = 2 s(H/2) c(H/2)

	// c(H/2) = sqrt((c(H) + 1) / 2)
	// c(H/2) = -c(H/2) if H > pi

	// s(H/2) = s(H) / (2 * c(H/2) )

	QuatV out(V_ZERO);

	float cosHalfTheta = FPSqrt((m_CosHeading + 1.0f) * 0.5f);
	cosHalfTheta = Selectf(m_Heading - PI, -cosHalfTheta, cosHalfTheta);

	float sinHalfTheta = (m_SinHeading * 0.5f) / cosHalfTheta;

	out.SetZf(sinHalfTheta);
	out.SetWf(cosHalfTheta);

	return out; // can avoid LHS somehow?
#endif
}

#if TRANSFORM_PERFORMANCE_TEST

const int NUM_TESTS = 10000;

#if __PS3 || RSG_ORBIS
void foo( float ) __attribute__((noinline));
void foo( Vec3V_In ) __attribute__((noinline));
void foo( Vector3::Vector3Param ) __attribute__((noinline));
#else
__declspec(noinline) void foo( float );
__declspec(noinline) void foo( Vec3V_In );
__declspec(noinline) void foo( Vector3::Vector3Param );
#endif //
void foo( float ) {}
void foo( Vec3V_In ) {}
void foo( Vector3::Vector3Param ) {}

class Rig
{
public:
	Rig() : m_Transform(NULL) {}
	~Rig() { m_Transform->Delete(); }
	fwTransform& GetTransform() { return *m_Transform; }
	void SetTransform(fwTransform* transform) { m_Transform = transform; }
private:
	fwTransform* m_Transform;
};

float Mat34VSimple() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Vec3V v(V_X_AXIS_WZERO);

	tm.Reset();
	Mat34V m1(V_IDENTITY);
	Mat34V m2(V_IDENTITY);
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = Transform(m1,v);
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float Mat34VSimpleLoaded() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Vec3V v(V_X_AXIS_WZERO);

	tm.Reset();
	Mat34V m(V_IDENTITY);
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1(m);
		v = Transform(m1,v);
		Mat34V m2(m);
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float Matrix34Simple() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Vector3 v(1,0,0);

	tm.Reset();
	Matrix34 m1(Matrix34::IdentityType);
	Matrix34 m2(Matrix34::IdentityType);
	for(int i = 0; i < NUM_TESTS; i++)
	{
		m1.Transform(v);
		m2.Transform(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float Matrix34SimpleLoaded() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Vector3 v(1,0,0);

	tm.Reset();
	Matrix34 m(Matrix34::IdentityType);
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Matrix34 m1(m);
		m1.Transform(v);
		Matrix34 m2(m);
		m2.Transform(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

//////////////////////////////////////////////////////////////////////////

float SimpleTransformByValue() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));
	rig2.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = rig1.GetTransform().Transform(v);
		v = rig2.GetTransform().Transform(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}


float SimpleTransformByGetMatrix() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));
	rig2.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1 = rig1.GetTransform().GetMatrix();
		v = Transform(m1,v);
		Mat34V m2 = rig2.GetTransform().GetMatrix();
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float SimpleTransformByGetMatrixCopy() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));
	rig2.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1;
		rig1.GetTransform().GetMatrixCopy(m1);
		v = Transform(m1,v);
		Mat34V m2;
		rig2.GetTransform().GetMatrixCopy(m2);
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float SimpleTransformGetMatrixDirectly() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));
	rig2.SetTransform(rage_new fwSimpleTransform(1.0f, 1.0f, 1.0f, 1.0f));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1 = fwSimpleTransformGetMatrix(&rig1.GetTransform());
		v = Transform(m1,v);
		Mat34V m2 = fwSimpleTransformGetMatrix(&rig2.GetTransform());
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}


//////////////////////////////////////////////////////////////////////////

float MatrixTransformByValue() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));
	rig2.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = rig1.GetTransform().Transform(v);
		v = rig2.GetTransform().Transform(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float MatrixTransformByValueDirectly() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));
	rig2.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = fwMatrixTransformTransform(&rig1.GetTransform(), v);
		v = fwMatrixTransformTransform(&rig2.GetTransform(), v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float MatrixTransformByValue3x3() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));
	rig2.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = rig1.GetTransform().Transform3x3(v);
		v = rig2.GetTransform().Transform3x3(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float MatrixTransformByGetMatrix() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));
	rig2.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1 = rig1.GetTransform().GetMatrix();
		v = Transform(m1,v);
		Mat34V m2 = rig2.GetTransform().GetMatrix();
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float MatrixTransformByGetMatrixCopy() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));
	rig2.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1;
		rig1.GetTransform().GetMatrixCopy(m1);
		v = Transform(m1,v);
		Mat34V m2;
		rig2.GetTransform().GetMatrixCopy(m2);
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float MatrixTransformGetMatrixDirectly() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));
	rig2.SetTransform(rage_new fwMatrixTransform(Mat34V(V_IDENTITY)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1 = fwMatrixTransformGetMatrix(&rig1.GetTransform());
		v = Transform(m1,v);
		Mat34V m2 = fwMatrixTransformGetMatrix(&rig2.GetTransform());
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float QuaternionTransformByValue() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));
	rig2.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = rig1.GetTransform().Transform(v);
		v = rig2.GetTransform().Transform(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float QuaternionTransformByValueDirectly() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));
	rig2.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = fwQuaternionTransformTransform(&rig1.GetTransform(), v);
		v = fwQuaternionTransformTransform(&rig2.GetTransform(), v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float QuaternionTransformByValue3x3() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));
	rig2.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		v = rig1.GetTransform().Transform3x3(v);
		v = rig2.GetTransform().Transform3x3(v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float QuaternionTransformByGetMatrix() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));
	rig2.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1 = rig1.GetTransform().GetMatrix();
		v = Transform(m1,v);
		Mat34V m2 = rig2.GetTransform().GetMatrix();
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float QuaternionTransformByGetMatrixCopy() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));
	rig2.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1;
		rig1.GetTransform().GetMatrixCopy(m1);
		v = Transform(m1,v);
		Mat34V m2;
		rig2.GetTransform().GetMatrixCopy(m2);
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}

float QuaternionTransformGetMatrixDirectly() 
{
	float fTime = -1.0f;
	sysTimer tm;

	Rig rig1;
	Rig rig2;

	Vec3V v(V_X_AXIS_WZERO);
	rig1.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));
	rig2.SetTransform(rage_new fwQuaternionTransform(QuatV(V_IDENTITY), Vec3V(V_ZERO)));

	tm.Reset();
	for(int i = 0; i < NUM_TESTS; i++)
	{
		Mat34V m1 = fwQuaternionTransformGetMatrix(&rig1.GetTransform());
		v = Transform(m1,v);
		Mat34V m2 = fwQuaternionTransformGetMatrix(&rig2.GetTransform());
		v = Transform(m2,v);
	}
	fTime = tm.GetUsTime();
	foo(v);

	return fTime;
}



typedef float (*PerformanceTestFn)();
typedef struct PerformanceTest
{
	const char*			m_Name;
	PerformanceTestFn	m_Func;
	float				m_Time;
} PerformanceTest;

#define TEST(x)	{#x, x, FLT_MAX}

PerformanceTest s_performanceTests[] = {
	TEST(Mat34VSimple),
	TEST(Mat34VSimpleLoaded),
	TEST(Matrix34Simple),
	TEST(Matrix34SimpleLoaded),
	TEST(SimpleTransformByValue),
	TEST(SimpleTransformByGetMatrix),
	TEST(SimpleTransformByGetMatrixCopy),
	TEST(SimpleTransformGetMatrixDirectly),
	TEST(MatrixTransformByValue),
	TEST(MatrixTransformByValueDirectly),
	TEST(MatrixTransformByValue3x3),
	TEST(MatrixTransformByGetMatrix),
	TEST(MatrixTransformByGetMatrixCopy),
	TEST(MatrixTransformGetMatrixDirectly),
	TEST(QuaternionTransformByValue),
	TEST(QuaternionTransformByValueDirectly),
	TEST(QuaternionTransformByValue3x3),
	TEST(QuaternionTransformByGetMatrix),
	TEST(QuaternionTransformByGetMatrixCopy),
	TEST(QuaternionTransformGetMatrixDirectly),
};

void fwTransform::PerformanceTest()
{
	for (int i = 0; i < NELEM(s_performanceTests); ++i)
	{
		s_performanceTests[i].m_Time = rage::Min(s_performanceTests[i].m_Time, s_performanceTests[i].m_Func());
	}
}

#endif // __DEV


#if !__FINAL
// PURPOSE: Return the name of this subclass for debugging purposes
const char *fwTransform::GetTransformClassName() const
{
	return GetTransformClassName((Type) GetType());
}

const char *fwTransform::GetTransformClassName(Type type)
{
	return s_TransformClassNames[type];
}
#endif // !__FINAL


#if __BANK && TRANSFORM_PERFORMANCE_TEST
bool s_PerformanceTest;

void fwTransform::DisplayDebugInfo() 
{
	if (s_PerformanceTest) {

		PerformanceTest();

		grcDebugDraw::AddDebugOutputEx(false, "Performance test");
		for (int i = 0; i < NELEM(s_performanceTests); ++i)
		{
			grcDebugDraw::AddDebugOutputEx(false, "%2.3f uSec %s", s_performanceTests[i].m_Time, s_performanceTests[i].m_Name);
		}
	}
}

bool fwTransform::SetupWidgets(bkBank& bank)
{
	bank.AddToggle("Show Transform Stats", &s_PerformanceTest);

	return true;
}
#endif // !__BANK

} // namespace rage
