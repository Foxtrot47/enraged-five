
#ifndef ALT_SKELETON_EXTENSION_H_
#define ALT_SKELETON_EXTENSION_H_

#include "atl/array.h"
#include "entity/extensionlist.h"
#include "fwutil/PtrList.h"

#include "vectormath\mat34v.h"

namespace rage {

	class bkBank;
	class fwEntity;

	class fwAltSkeletonExtension : public fwExtension
	{
	public:
#if !__SPU
		EXTENSIONID_DECL( fwAltSkeletonExtension, fwExtension );
#endif // !__SPU

		fwEntity*   m_entity;
		Mat34V		m_offset;

		enum 
		{
			MAX_ALT_SKELETON_EXTENSIONS = 32
		};

		static fwAltSkeletonExtension* GetOrAddExtension(fwEntity &entity);
		static void RemoveExtension(fwEntity &entity);
	};

} // namespace rage

#endif // ALT_SKELETON_EXTENSION_H_
