//
// entity/drawdata.cpp : Interface class for all the drawdata attached to an entity
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "entity/drawdata.h"
#include "entity/entity.h"
#include "fwdrawlist/drawlistmgr.h"
#include "rmcore/drawable.h"
#include "system/threadtype.h"

using namespace rage;


#if __BANK
//
//
// quite slow, but useful for debug purposes - used by editable shader variables
//
bool fwCustomShaderEffect::SetFloatShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, float varVal)
{
	const s32 count = pShaderGroup->GetCount();
	Assert(count > 0);
	for(s32 i=0; i<count; i++)
	{
		grmShader *pShader = pShaderGroup->GetShaderPtr(i);
		grcEffectVar varId = pShader->LookupVar(varName, /*mustExist*/false);
		if(varId)
		{
			pShader->SetVar(varId, (float)varVal);
		}
	}
	return(true);
}

//
//
//
//
bool fwCustomShaderEffect::SetVector2ShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, const Vector2& varVal)
{
	const s32 count = pShaderGroup->GetCount();
	Assert(count > 0);
	for(s32 i=0; i<count; i++)
	{
		grmShader *pShader = pShaderGroup->GetShaderPtr(i);
		grcEffectVar varId = pShader->LookupVar(varName, /*mustExist*/false);
		if(varId)
		{
			pShader->SetVar(varId, varVal);
		}
	}
	return(true);
}

//
//
//
//
bool fwCustomShaderEffect::SetVector3ShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, const Vector3& varVal)
{
	const s32 count = pShaderGroup->GetCount();
	Assert(count > 0);
	for(s32 i=0; i<count; i++)
	{
		grmShader *pShader = pShaderGroup->GetShaderPtr(i);
		grcEffectVar varId = pShader->LookupVar(varName, /*mustExist*/false);
		if(varId)
		{
			pShader->SetVar(varId, varVal);
		}
	}
	return(true);
}

//
//
//
//
bool fwCustomShaderEffect::SetVector4ShaderGroupVar(grmShaderGroup *pShaderGroup, const char *varName, const Vector4& varVal)
{
	const s32 count = pShaderGroup->GetCount();
	Assert(count > 0);
	for(s32 i=0; i<count; i++)
	{
		grmShader *pShader = pShaderGroup->GetShaderPtr(i);
		grcEffectVar varId = pShader->LookupVar(varName, /*mustExist*/false);
		if(varId)
		{
			pShader->SetVar(varId, varVal);
		}
	}
	return(true);
}
#endif //__BANK...



fwDrawData::fwDrawData() 
	: m_pShaderEffect(NULL), 
	m_hasBones(0), 
	m_occlusionQueryId(0)
{
	m_pDrawable = NULL;
}

fwDrawData::~fwDrawData()
{
	if (m_pShaderEffect)
	{
		m_pShaderEffect->DeleteInstance();
	}
}

void fwDrawData::SetDrawable(rmcDrawable* d)
{
	Assert(m_pDrawable == NULL);
	m_pDrawable = d;
}

void fwDrawData::RemoveDrawable()
{
	this->m_pDrawable = NULL;
	if(this->m_pShaderEffect)
	{
		this->m_pShaderEffect->DeleteInstance();
		this->m_pShaderEffect = NULL;
	}
}

//
//
// usually called in CEntity::PreRender():
//
void fwDrawData::Update(fwEntity *pEntity)
{
	// update ShaderEffect stuff:
	if(this->m_pShaderEffect)
	{
		// no standard CBs when local shaders variables may be present
		this->m_pShaderEffect->Update(pEntity);
	}
}

dlCmdBase* fwDrawData::AddToDrawList(fwEntity* /*pEntity*/, fwDrawDataAddParams* /*params*/)
{
	// nothing needs the return value for this type atm
	return NULL;

}

