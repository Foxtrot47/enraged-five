
#include "dynamicarchetype.h"

#include "fwscene/stores/blendshapestore.h"
#include "fwscene/stores/clipdictionarystore.h"
#include "fwscene/stores/expressionsdictionarystore.h"
#include "fwscene/stores/posematcherstore.h"
#include "fwsys/metadatastore.h"


namespace rage {

FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwDynamicArchetypeComponent, 2900, 0.10f, atHashString("fwDynamicArchetypeComponent",0xe010202b));


fwDynamicArchetypeComponent::fwDynamicArchetypeComponent()
{
	m_bHasPhysicsInDrawable = false;
	m_pPhysicsArch = NULL;

	m_clipDictionaryIndex = -1;
	m_blendShapeFileIndex = -1;
	m_expressionDictionaryIndex = -1;
	m_expressionHash = atHashString::Null(); 
	m_poseMatcherFileIndex = -1; 
	m_poseMatcherProneFileIndex = -1;
	m_creatureMetadataFileIndex = -1;

	m_bAutoStartAnim = false;

	m_physicsArchRefs = 0;

	m_AIAvoidancePoint = Vec4V(V_FLT_MAX);
}

void fwDynamicArchetypeComponent::SetClipDictionaryIndex(const atHashString& clipDictionaryName)
{
	strLocalIndex index = strLocalIndex(g_ClipDictionaryStore.FindSlotFromHashKey(clipDictionaryName));
	// If cannot find slot then create one. 
	if(index == -1)
	{
		index = g_ClipDictionaryStore.AddSlot(clipDictionaryName);
	}
	else
	{
		Assertf(!stricmp(clipDictionaryName.GetCStr(), g_ClipDictionaryStore.GetName(index)), "%s:Hash name clash", clipDictionaryName.GetCStr());
	}

	Assign(m_clipDictionaryIndex, index.Get());
}

#if ENABLE_BLENDSHAPES
void fwDynamicArchetypeComponent::SetBlendShapeFile(const atHashString& blendShapeName)
{
	s32 index = g_BlendShapeStore.FindSlotFromHashKey(blendShapeName);
	// If cannot find slot then create one. 
	if(index == -1)
	{
		index = g_BlendShapeStore.AddSlot(blendShapeName);
	}
	else
	{
		Assertf(!stricmp(blendShapeName.GetCStr(), g_BlendShapeStore.GetName(index)), "%s:Hash name clash", blendShapeName.GetCStr());
	}

	Assign(m_blendShapeFileIndex, index.Get());
}
#endif // ENABLE_BLENDSHAPES

void fwDynamicArchetypeComponent::SetExpressionDictionaryIndex(const atHashString& expressionDictionaryName)
{
	strLocalIndex index = strLocalIndex(g_ExpressionDictionaryStore.FindSlotFromHashKey(expressionDictionaryName));
	// If cannot find slot then create one. 
	if(index == -1)
	{
		index = g_ExpressionDictionaryStore.AddSlot(expressionDictionaryName);
	}
	else
	{
		Assertf(!stricmp(expressionDictionaryName.GetCStr(), g_ExpressionDictionaryStore.GetName(index)), "%s:Hash name clash", expressionDictionaryName.GetCStr());
	}

	Assign(m_expressionDictionaryIndex, index.Get());
}

void fwDynamicArchetypeComponent::SetPoseMatcherFile(const atHashString& poseMatcherName)
{
	strLocalIndex index = strLocalIndex(g_PoseMatcherStore.FindSlotFromHashKey(poseMatcherName));
	// If cannot find slot then create one. 
	if(index == -1)
	{
		index = g_PoseMatcherStore.AddSlot(poseMatcherName);
	}
	else
	{
		Assertf(!stricmp(poseMatcherName.GetCStr(), g_PoseMatcherStore.GetName(index)), "%s:Hash name clash", poseMatcherName.GetCStr());
	}

	Assign(m_poseMatcherFileIndex, index.Get());
}

void fwDynamicArchetypeComponent::SetPoseMatcherProneFile(const atHashString& poseMatcherName)
{
	strLocalIndex index = strLocalIndex(g_PoseMatcherStore.FindSlotFromHashKey(poseMatcherName));
	// If cannot find slot then create one. 
	if(index == -1)
	{
		index = g_PoseMatcherStore.AddSlot(poseMatcherName);
	}
	else
	{
		Assertf(!stricmp(poseMatcherName.GetCStr(), g_PoseMatcherStore.GetName(index)), "%s:Hash name clash", poseMatcherName.GetCStr());
	}

	Assign(m_poseMatcherProneFileIndex, index.Get());
}

void fwDynamicArchetypeComponent::SetCreatureMetadataFile(const atHashString& creatureMetadataName)
{
	strLocalIndex index = strLocalIndex(g_fwMetaDataStore.FindSlotFromHashKey(creatureMetadataName));
	// If cannot find slot then create one. 
	if(index == -1)
	{
		index = g_fwMetaDataStore.AddSlot(creatureMetadataName);
	}
	else
	{
		Assertf(!stricmp(creatureMetadataName.GetCStr(), g_fwMetaDataStore.GetName(index)), "%s:Hash name clash", creatureMetadataName.GetCStr());
	}

	Assert(index.Get() < BIT(14));
	m_creatureMetadataFileIndex = index.Get();
	FastAssert(m_creatureMetadataFileIndex == index.Get());
}

} // namespace rage

