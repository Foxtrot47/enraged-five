//
// entity/entity.cpp : base class for all entities in the world
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "entity/extensionlist.h"
#include "system/threadtype.h"


namespace rage {

fwExtensionList::~fwExtensionList()
{
	DestroyAll();
	Assert(!m_Head);
}

void fwExtensionList::Add(fwExtension& extension)
{
	Assert(sysThreadType::IsUpdateThread());
	Assertf(!Get(extension.GetExtensionId()), "Entity already has an extension of type %s", extension.GetAutoIdNameFromId(extension.GetExtensionId()));
	LinkedListNode* node = rage_new LinkedListNode;
	node->m_Data = &extension;
	node->m_Next = m_Head;
	m_Head = node;
	if(extension.GetExtensionId() < MAX_BIT_MANAGED_EXTENSIONS)
	{
		m_AvailableExtensions.Set(extension.GetExtensionId());
	}
}

fwExtension* fwExtensionList::Get(unsigned extensionId) 
{
	return const_cast<fwExtension *>(InternalGet(extensionId));
}

const fwExtension* fwExtensionList::Get(unsigned extensionId) const
{
	return InternalGet(extensionId);
}

const fwExtension* fwExtensionList::InternalGet(unsigned extensionId) const
{
	// Early-out using the bit check, but only for those extensions that are managed through the bit set.
	if (extensionId < MAX_BIT_MANAGED_EXTENSIONS && !m_AvailableExtensions.IsSet(extensionId))
	{

		return NULL;
	}

	const LinkedListNode* pNode = m_Head;
	while (pNode)
	{
		fwExtension* pExtension = pNode->m_Data;
		Assertf(pExtension, "Somebody deleted an fwExtension object and is now attempting to use it! There might also be a memory leak. We're about to crash...");

		if (pExtension->GetExtensionId() == extensionId)
		{
			return pExtension;
		}
		pNode = pNode->m_Next;
	}

	Assertf(extensionId >= MAX_BIT_MANAGED_EXTENSIONS, "Inconsistent extension list bit mask - extension %d not present despite bit being set", extensionId);

	return NULL;
}


// PURPOSE: Destroy extension on the extension list
bool fwExtensionList::Destroy(fwExtension* pExtension)
{
	bool result = Unlink(pExtension);

	if (result) {
		delete pExtension;
	}

	return result;
}

bool fwExtensionList::Unlink(fwExtension *pExtension)
{
	Assert(sysThreadType::IsUpdateThread());

	unsigned extensionId = pExtension->GetExtensionId();

	if(extensionId < MAX_BIT_MANAGED_EXTENSIONS)
	{
		if (!m_AvailableExtensions.IsSet(extensionId))
		{
			return false;
		}

		m_AvailableExtensions.Clear(pExtension->GetExtensionId());
	}

	LinkedListNode** pNodeRef = &m_Head;
	LinkedListNode* pNode = m_Head;
	while (pNode)
	{
		if (pNode->m_Data == pExtension)
		{
			*pNodeRef = pNode->m_Next;
			delete pNode;
			return true;
		}
		pNodeRef = &pNode->m_Next;
		pNode = pNode->m_Next;
	}

	Assertf(extensionId >= MAX_BIT_MANAGED_EXTENSIONS, "Inconsistent extension list bit mask on clear - extension %d not present despite bit being set", pExtension->GetExtensionId());
	return false;
}

// PURPOSE: Destroy extension on the extension list with the given extension id
bool fwExtensionList::Destroy(unsigned extensionId)
{
	Assert(sysThreadType::IsUpdateThread());

	if(extensionId < MAX_BIT_MANAGED_EXTENSIONS)
	{
		if(!m_AvailableExtensions.IsSet(extensionId))
		{
			return false;
		}

		m_AvailableExtensions.Clear(extensionId);
	}

	LinkedListNode** pNodeRef = &m_Head;
	LinkedListNode* pNode = m_Head;
	while (pNode)
	{
		const fwExtension* pExtension = pNode->m_Data;
		if (pExtension->GetExtensionId() == extensionId)
		{
			*pNodeRef = pNode->m_Next;
			delete pExtension;
			delete pNode;
			return true;
		}
		pNodeRef = &pNode->m_Next;
		pNode = pNode->m_Next;
	}

	Assertf(extensionId >= MAX_BIT_MANAGED_EXTENSIONS, "Inconsistent extension list bit mask on clear - extension %d not present despite bit being set", extensionId);
	return false;
}

// PURPOSE: Destroy all extensions on the extension list
void fwExtensionList::DestroyAll(void)
{
	m_AvailableExtensions.Reset();

	LinkedListNode* pNode = m_Head;
	while (pNode)
	{
		LinkedListNode* pNext = pNode->m_Next;

		fwExtension* pNodeExtension = pNode->m_Data;
		delete pNodeExtension;
		delete pNode;
	
		pNode = pNext;
	}
	m_Head = NULL;
}

} // namespace rage
