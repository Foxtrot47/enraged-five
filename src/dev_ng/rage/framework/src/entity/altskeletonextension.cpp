#
#include "entity/altskeletonextension.h"

#include "entity/archetype.h"
#include "entity/entity.h"

#if __PPU
#include <sn/libsntuner.h>
#endif // __PPU

namespace rage {

	AUTOID_IMPL(fwAltSkeletonExtension);

	atFixedArray<fwAltSkeletonExtension, fwAltSkeletonExtension::MAX_ALT_SKELETON_EXTENSIONS> ms_AltSkeletonExtensions;

	fwAltSkeletonExtension* fwAltSkeletonExtension::GetOrAddExtension(fwEntity &entity)
	{
		Assert(sysThreadType::IsUpdateThread());

		// Does this entity already have a alt skeleton extension?
		fwAltSkeletonExtension *extension = entity.GetExtension<fwAltSkeletonExtension>();

		if (!extension)
		{
			// Nope - need to create one first.
			extension = &ms_AltSkeletonExtensions.Append();
			extension->m_entity = &entity;
			extension->m_offset = Mat34V(V_IDENTITY);
			entity.GetExtensionList().Add(*extension);
		}

		return extension;
	}

	void fwAltSkeletonExtension::RemoveExtension(fwEntity &entity)
	{
		Assert(sysThreadType::IsUpdateThread());

		fwAltSkeletonExtension *extension = entity.GetExtension<fwAltSkeletonExtension>();

		if (extension)
		{
			// If there's no reason for this extension to exist anymore,
			// kill it.
			entity.GetExtensionList().Unlink(extension);

			int index = ptrdiff_t_to_int(extension - ms_AltSkeletonExtensions.GetElements());

			Assert(&ms_AltSkeletonExtensions[index] == extension);

			// Keep a pointer to the last extension around, we might need it for relinking later.
			fwAltSkeletonExtension *lastExtension = &ms_AltSkeletonExtensions[ms_AltSkeletonExtensions.GetCount()-1];

			ms_AltSkeletonExtensions.DeleteFast(index);

			// If we just moved an extension around, we need to relink it.
			// The only exception is if we just deleted the last element in the list, in which case
			// nothing was shifted.
			if (ms_AltSkeletonExtensions.GetCount() > (int) index)
			{
				fwEntity *relinkedEntity = ms_AltSkeletonExtensions[index].m_entity;
				FastAssert(relinkedEntity);

				relinkedEntity->GetExtensionList().Unlink(lastExtension);
				relinkedEntity->GetExtensionList().Add(ms_AltSkeletonExtensions[index]);

				// Note that DeleteFast() already moved the extension around, so link from the
				// extension to the entity is correct. But hey, let's assert on that.
				Assert(ms_AltSkeletonExtensions[index].m_entity == relinkedEntity);
			}
		}
	}

} // namespace rage
