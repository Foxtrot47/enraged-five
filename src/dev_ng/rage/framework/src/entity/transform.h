//
// scene/transform.h : base class for 3d transform description
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#ifndef SCENE_TRANSFORM_H_
#define SCENE_TRANSFORM_H_

#include "atl/pagedpool.h"
#include "fwtl/pool.h"
#include "fwtl/pagedpool.h"
#include "math/amath.h"
#include "streaming/streamingengine.h"
#include "vector/vector3.h"
#include "vectormath/classes.h" 
#include "vectormath/legacyconvert.h" 

namespace rage {

#define ENABLE_FWQUATERNIONTRANSFORM 1

#define FWTRANSFORM_USE_PAGED_POOLS	1
#define FWTRANSFORM_USE_RSCVIRT_FOR_PAGED_POOLS 1

#if ENABLE_FWQUATERNIONTRANSFORM
#define FWQUATTRANS_ONLY(_x) _x
#else
#define FWQUATTRANS_ONLY(_x) _x
#endif

class bkBank;

#define ILLEGAL_OPERATION()	Assertf(false,"illegal operation")
#define TRANSFORM_PERFORMANCE_TEST (!__FINAL)

#if FWTRANSFORM_USE_PAGED_POOLS
	#if FWTRANSFORM_USE_RSCVIRT_FOR_PAGED_POOLS
		#define FWTRANSFORM_POOL_TYPE(_T)	fwRscMemPagedPool<_T>
	#else
		#define FWTRANSFORM_POOL_TYPE(_T)	atPagedPool<_T>
	#endif
#else // FWTRANSFORM_USE_PAGED_POOLS
	#define FWTRANSFORM_POOL_TYPE(_T)	atPool<_T>
#endif // FWTRANSFORM_USE_PAGED_POOLS

// Replacement for AT_REGISTER_CLASS_POOL. We can't use AT_REGISTER_CLASS_POOL out of the box;
// we're sharing different same-sized virtual classes in the same pool, so we have to call constructor
// after an allocation to ensure the proper vtable pointer is put in.
// _T is the class itself, _PoolT is the type containing the pool.
#define FWTRANSFORM_REGISTER_POOL(_T, _PoolT)							\
	typedef FWTRANSFORM_POOL_TYPE(_PoolT) Pool;										\
	static Pool* _ms_pPool;												\
	void* operator new(size_t /*nSize*/ RAGE_NEW_EXTRA_ARGS_UNUSED)		\
	{																	\
		Assert(_PoolT::_ms_pPool);										\
		if (_PoolT::_ms_pPool->IsFull()) {								\
			g_strStreamingInterface->DumpStreamingInterests();			\
			Quitf(ERR_GEN_ENT_TRANS,#_T " pool overrun (%d elements)", (int) _PoolT::_ms_pPool->GetCount());	\
		}																\
		void *result = (void*) _PoolT::_ms_pPool->New();				\
		::new (result) _T();											\
		return result;													\
	}																	\
	void operator delete(void *pVoid)									\
	{																	\
		Assert(_PoolT::_ms_pPool);										\
		_PoolT::_ms_pPool->Delete((_T*)pVoid);							\
	}																	\
	static void InitPool(const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);			\
	static void InitPool(int size, const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);	\
	static void ShutdownPool() {delete _ms_pPool; _ms_pPool = NULL;}	\
	static Pool* GetPool() {return _PoolT::_ms_pPool;}


__forceinline u16 ConvertF32ToFixed2_14(float val)
{
	Assert(val >= 0.f);

	float left = floor(val);
	float right = val - left;

	left = left > 3.f ? 3.f : left;
	right *= 10000;

	u32 iLeft = static_cast<u32>(left);
	u32 iRight = static_cast<u32>(right);

	u16 ret = (iLeft & 0x3) << 14 | (iRight & 0x3FFF);
	return ret;
}

#define FWTRANSFORM_VIRTUAL_FUNCTION_CONST(return_type, name, funcArgs, args)		\
	inline return_type fwTransform::name funcArgs const {								\
	switch (GetType()) {												\
			case TYPE_SIMPLE:										\
			return ((fwSimpleTransform *) this)->name args;			\
			case TYPE_MATRIX:										\
			return ((fwMatrixTransform *) this)->name args;			\
			FWQUATTRANS_ONLY(case TYPE_QUATERNION:									)	\
			FWQUATTRANS_ONLY(return ((fwQuaternionTransform *) this)->name args;	)	\
			case TYPE_SIMPLE_SCALED:									\
			return ((fwSimpleScaledTransform *) this)->name args;		\
			case TYPE_MATRIX_SCALED:									\
			return ((fwMatrixScaledTransform *) this)->name args;		\
			FWQUATTRANS_ONLY(case TYPE_QUATERNION_SCALED:)									\
			FWQUATTRANS_ONLY(return ((fwQuaternionScaledTransform *) this)->name args;)		\
			default:														\
			return ((fwIdentityTransform *) this)->name args;			\
}																	\
}

#define FWTRANSFORM_VIRTUAL_FUNCTION(return_type, name, funcArgs, args)		\
	inline return_type fwTransform::name funcArgs {								\
	switch (GetType()) {												\
			case TYPE_SIMPLE:										\
			return ((fwSimpleTransform *) this)->name args;			\
			case TYPE_MATRIX:										\
			return ((fwMatrixTransform *) this)->name args;			\
			FWQUATTRANS_ONLY(case TYPE_QUATERNION:)									\
			FWQUATTRANS_ONLY(return ((fwQuaternionTransform *) this)->name args;)		\
			case TYPE_SIMPLE_SCALED:									\
			return ((fwSimpleScaledTransform *) this)->name args;		\
			case TYPE_MATRIX_SCALED:									\
			return ((fwMatrixScaledTransform *) this)->name args;		\
			FWQUATTRANS_ONLY(case TYPE_QUATERNION_SCALED:							)		\
			FWQUATTRANS_ONLY(return ((fwQuaternionScaledTransform *) this)->name args;)		\
			default:														\
			return ((fwIdentityTransform *) this)->name args;			\
}																	\
}

#define FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE(return_type, name, funcArgs, args)		\
	inline return_type fwTransform::name funcArgs {								\
	switch (GetType() & ~TYPE_FLAG_SCALED) {						\
			case TYPE_SIMPLE:										\
			return ((fwSimpleTransform *) this)->name args;				\
			case TYPE_MATRIX:										\
			return ((fwMatrixTransform *) this)->name args;				\
			FWQUATTRANS_ONLY(case TYPE_QUATERNION:									)	\
			FWQUATTRANS_ONLY(return ((fwQuaternionTransform *) this)->name args;	)			\
			default:														\
			return ((fwIdentityTransform *) this)->name args;				\
}																	\
}

#define FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(return_type, name, funcArgs, args)		\
	inline return_type fwTransform::name funcArgs const {								\
	switch (GetType() & ~TYPE_FLAG_SCALED) {						\
	case TYPE_SIMPLE:										\
	return ((fwSimpleTransform *) this)->name args;				\
	case TYPE_MATRIX:										\
	return ((fwMatrixTransform *) this)->name args;				\
	FWQUATTRANS_ONLY(case TYPE_QUATERNION:									)	\
	FWQUATTRANS_ONLY(return ((fwQuaternionTransform *) this)->name args;	)			\
	default:														\
	return ((fwIdentityTransform *) this)->name args;				\
}																	\
}

#define FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST_INLINE(return_type, name, funcArgs, args)		\
	__forceinline return_type fwTransform::name funcArgs const {								\
	switch (GetType() & ~TYPE_FLAG_SCALED) {						\
	case TYPE_SIMPLE:										\
	return ((fwSimpleTransform *) this)->name args;				\
	case TYPE_MATRIX:										\
	return ((fwMatrixTransform *) this)->name args;				\
	FWQUATTRANS_ONLY(case TYPE_QUATERNION:									)	\
	FWQUATTRANS_ONLY(return ((fwQuaternionTransform *) this)->name args;	)			\
	default:														\
	return ((fwIdentityTransform *) this)->name args;				\
}																	\
}

struct fwTransformData
{
	// This is a Type enum
	u16 m_Type;

	// This is the offset within this class that has the position, given in quadwords.
	u16 m_PositionOffset;
};

struct fwTransformOverlay
{
	// Bytes that are used for other purposes
	u32 m_unused[3];
	fwTransformData m_Data;
};

//
// PURPOSE
//	fwTransform is the base class for 3d transform description.  
class fwTransform
{
public:
	enum Type
	{
		TYPE_FLAG_SCALED = 2,

		TYPE_SIMPLE = 0,
		TYPE_MATRIX = 1,
		TYPE_SIMPLE_SCALED = TYPE_SIMPLE | TYPE_FLAG_SCALED,
		TYPE_MATRIX_SCALED = TYPE_MATRIX | TYPE_FLAG_SCALED,
		TYPE_IDENTITY = 4,
		TYPE_QUATERNION = 8,
		TYPE_QUATERNION_SCALED = TYPE_QUATERNION | TYPE_FLAG_SCALED,

		TYPE_FULL_TRANSFORM_MASK = TYPE_MATRIX | TYPE_QUATERNION,
	};


#if __BANK
	// PURPOSE: Initialise transforms debug information.
	static void DisplayDebugInfo();

	// PURPOSE: Setup transform specific bank widgets.
	static bool SetupWidgets(bkBank& bank);
#endif 

	// PURPOSE: Delete the transform.
	void Delete();

	// PURPOSE: Get the matrix.
	Mat34V_Out GetMatrix() const;

	// PURPOSE: Get the non-orthogonal matrix (i.e. apply scale before return) if supported
	Mat34V_Out GetNonOrthoMatrix() const;

	// PURPOSE: Get a copy of the matrix.
	void GetMatrixCopy(Mat34V_InOut mat) const;

	// PURPOSE: Get a copy of the non-orthogonal matrix (i.e. apply scale before return) if supported
	void GetNonOrthoMatrixCopy(Mat34V_InOut mat) const;

	// PURPOSE: Get the position.
	Vec3V_Out GetPosition() const;

	// PURPOSE: Returns the transform's A vector, column 0, right axis. 
	Vec3V_Out GetA() const;

	// PURPOSE: Returns the transform's B vector, column 1, forward axis. 
	Vec3V_Out GetB() const;

	// PURPOSE: Returns the transform's C vector, column 2, up axis. 
	Vec3V_Out GetC() const;

	// PURPOSE: Returns the non-orthogonal transform's A vector, column 0, right axis. 
	Vec3V_Out GetNonOrthoA() const;

	// PURPOSE: Returns the non-orthogonal transform's B vector, column 1, forward axis. 
	Vec3V_Out GetNonOrthoB() const;

	// PURPOSE: Returns the non-orthogonal transform's C vector, column 2, up axis. 
	Vec3V_Out GetNonOrthoC() const;

	// PURPOSE: Returns the transform's right axis.
	__forceinline Vec3V_Out GetRight() const { return GetA(); }

	// PURPOSE: Returns the transform's forward axis.
	__forceinline Vec3V_Out GetForward() const { return GetB(); }

	// PURPOSE: Returns the transform's up axis.
	__forceinline Vec3V_Out GetUp() const { return GetC(); }

	// PURPOSE: Returns the transform's up roll (rotation around Y).
	float GetRoll() const;

	// PURPOSE: Returns the transform's up roll (rotation around X).
	float GetPitch() const;

	// PURPOSE: Returns the transform's up roll (rotation around Z).
	float GetHeading() const;

	// PURPOSE: Get the transform's XY scale.
	ScalarV_Out GetScaleXYV() const;

	// PURPOSE: Get the transform's Z scale.
	ScalarV_Out GetScaleZV() const;

	// PURPOSE: Get the transform's scale.
	Vec3V_Out GetScaleV() const;

	// PURPOSE: Set the transform to identity rotation and null translation.
	void SetIdentity();

	// PURPOSE: Set the transform's matrix.
	void SetMatrix(Mat34V_In);

	// PURPOSE: Set the transform's position as a vector.
	void SetPosition(Vec3V_In);

	// PURPOSE: Set the transform's position as floats.
	void SetPosition(float x, float y, float z);

	// PURPOSE: Set the transform's heading.
	void SetHeading(float);

	// PURPOSE: Set the two dimensional scale if the matrix supports it
	void SetScale(float, float);

	// PURPOSE: Transform the passed vector into world space.
	Vec3V_Out Transform(Vec3V_In position) const;

	// PURPOSE: Transform the passed vector into object space.
	Vec3V_Out UnTransform(Vec3V_In position) const;

	// PURPOSE: Rotate the passed vector into world space.
	Vec3V_Out Transform3x3(Vec3V_In position) const;

	// PURPOSE: Rotate the passed vector into object space.
	Vec3V_Out UnTransform3x3(Vec3V_In position) const;

	// PURPOSE: Return is the matrix is orthonormal. 
	bool IsOrthonormal() const;

	// PURPOSE: Return the orientation of the transform
	QuatV_Out GetOrientation() const;

#if !__FINAL
	// PURPOSE: Return the name of this subclass for debugging purposes
	const char *GetTransformClassName() const;
#endif // !__FINAL

	// PURPOSE: Allocate all the pools for the subclasses.
	static void InitPools();

	// PURPOSE: Free up all the pools for the subclasses.
	static void ShutdownPools();

	// HACK: Saves a lot of memory
	// ---------------------------------------------------------------------------------------
	//
	__forceinline void SetOverlay(u16 type, u16 offset)
	{
		fwTransformOverlay* pOverlay = reinterpret_cast<fwTransformOverlay*>(this);
		pOverlay->m_Data.m_Type = type;
		pOverlay->m_Data.m_PositionOffset = offset;
	}

	__forceinline u16 GetType() const
	{
		const fwTransformOverlay* pOverlay = reinterpret_cast<const fwTransformOverlay*>(this);
		return pOverlay->m_Data.m_Type;
	}

	__forceinline void SetType(const u16 type)
	{
		fwTransformOverlay* pOverlay = reinterpret_cast<fwTransformOverlay*>(this);
		pOverlay->m_Data.m_Type = type;
	}

	// HACK: Saves a lot of memory
	__forceinline u16 GetPositionOffset() const
	{
		const fwTransformOverlay* pOverlay = reinterpret_cast<const fwTransformOverlay*>(this);
		return pOverlay->m_Data.m_PositionOffset;
	}

	__forceinline void SetPositionOffset(const u16 offset)
	{
		fwTransformOverlay* pOverlay = reinterpret_cast<fwTransformOverlay*>(this);
		pOverlay->m_Data.m_PositionOffset = offset;
	}
	//
	// ---------------------------------------------------------------------------------------
	//

	__forceinline bool IsIdentityTransform() const			{ return GetType() == TYPE_IDENTITY; }
	__forceinline bool IsFullTransform() const				{ return (GetType() & TYPE_FULL_TRANSFORM_MASK) != 0; }
	__forceinline bool IsMatrixTransform() const			{ return (GetType() & ~TYPE_FLAG_SCALED) == TYPE_MATRIX; }
	__forceinline bool IsQuaternionTransform() const		{ return (GetType() & ~TYPE_FLAG_SCALED) == TYPE_QUATERNION; }
	__forceinline bool IsSimpleTransform() const			{ return (GetType() & ~TYPE_FLAG_SCALED) == TYPE_SIMPLE; }
	__forceinline bool IsMatrixScaledTransform() const		{ return GetType() == TYPE_MATRIX_SCALED; }
	__forceinline bool IsQuaternionScaledTransform() const	{ return GetType() == TYPE_QUATERNION_SCALED; }
	__forceinline bool IsSimpleScaledTransform() const		{ return GetType() == TYPE_SIMPLE_SCALED; }
	__forceinline Type GetTypeIdentifier() const			{ return (Type) GetType(); }	
	__forceinline bool IsMatrix() const					{ return IsMatrixTransform(); }

#if TRANSFORM_PERFORMANCE_TEST
	static void PerformanceTest();
#endif 

#if !__FINAL
	static const char *GetTransformClassName(Type type);
#endif // !__FINAL

protected:
	~fwTransform() {}
};

class fwTransformCore : public fwTransform
{
public:
	// PURPOSE: Returns the non-orthogonal transform's A vector, column 0, right axis. 
	Vec3V_Out GetNonOrthoA() const { return GetA(); }

	// PURPOSE: Returns the non-orthogonal transform's B vector, column 1, forward axis. 
	Vec3V_Out GetNonOrthoB() const { return GetB(); }

	// PURPOSE: Returns the non-orthogonal transform's C vector, column 2, up axis. 
	Vec3V_Out GetNonOrthoC() const { return GetC(); }

	__forceinline ScalarV_Out GetScaleXYV() const { return ScalarV(V_ONE); }
	__forceinline ScalarV_Out GetScaleZV() const { return ScalarV(V_ONE); }
	__forceinline Vec3V_Out GetScaleV() const { return Vec3V(V_ONE); }

	// PURPOSE: Set the two dimensional scale if the matrix supports it
	void SetScale(float, float) { ILLEGAL_OPERATION(); }

	// PURPOSE: Return is the matrix is orthonormal. 
	bool IsOrthonormal() const { return true; }

	// PURPOSE: Get the non-orthogonal matrix (i.e. apply scale before return) if supported
	Mat34V_Out GetNonOrthoMatrix() const { return GetMatrix(); }

	// PURPOSE: Get a copy of the non-orthogonal matrix (i.e. apply scale before return) if supported
	void GetNonOrthoMatrixCopy(Mat34V_InOut mat) const { GetMatrixCopy(mat); }
};

//
// PURPOSE
//	fwIdentityTransform always returns the identity
class fwIdentityTransform : public fwTransformCore
{
public:
	static fwIdentityTransform* GetInstance() { return &sm_Instance; }

	void Delete() { /* DO NOTHING*/ }

	__forceinline Vec3V_Out GetPosition() const { return Vec3V(V_ZERO); }
	__forceinline Mat34V_Out GetMatrix() const { return Mat34V(V_IDENTITY); }
	__forceinline void GetMatrixCopy(Mat34V_InOut mat) const { mat = Mat34V(V_IDENTITY); }
	__forceinline QuatV_Out GetOrientation() const {return QuatV(V_IDENTITY); }

	__forceinline Vec3V_Out GetA() const { return Vec3V(V_X_AXIS_WZERO); }
	__forceinline Vec3V_Out GetB() const { return Vec3V(V_Y_AXIS_WZERO); }
	__forceinline Vec3V_Out GetC() const { return Vec3V(V_Z_AXIS_WZERO); }

	__forceinline float GetRoll() const { return 0.0f; }
	__forceinline float GetPitch() const { return 0.0f; }
	__forceinline float GetHeading() const { return 0.0f; }

	__forceinline void SetIdentity() {}
	__forceinline void SetMatrix(Mat34V_In) { ILLEGAL_OPERATION(); }
	__forceinline void SetPosition(Vec3V_In) { ILLEGAL_OPERATION(); }
	__forceinline void SetPosition(float, float, float) { ILLEGAL_OPERATION(); }
	__forceinline void SetHeading(float) { ILLEGAL_OPERATION(); }

	__forceinline Vec3V_Out Transform(Vec3V_In position) const { return position; }
	__forceinline Vec3V_Out UnTransform(Vec3V_In position) const { return position; }

	__forceinline Vec3V_Out Transform3x3(Vec3V_In position) const { return position; }
	__forceinline Vec3V_Out UnTransform3x3(Vec3V_In position) const { return position; }
protected:
	__forceinline void InitBitSet()						{ SetOverlay(TYPE_IDENTITY, 0); }

	fwIdentityTransform() { m_Position = Vec3V(V_ZERO); InitBitSet();}

	// Dummy position, always 0/0/0.
	Vec3V m_Position;

	~fwIdentityTransform() {}

	static fwIdentityTransform sm_Instance;
};

//
// PURPOSE
//	fwMatrixTransform is a 3d transform represented by a matrix.  
class fwMatrixTransform : public fwTransformCore
{
public:
	fwMatrixTransform() { InitBitSet(); }
	fwMatrixTransform(Mat34V_In mat) { SetMatrix(mat); }

	void Delete();

	__forceinline Vec3V_Out GetPosition() const { return m_Matrix.GetCol3(); }
	__forceinline Mat34V_Out GetMatrix() const { return m_Matrix; }
	__forceinline void GetMatrixCopy(Mat34V_InOut mat) const { mat = m_Matrix; }
	__forceinline QuatV_Out GetOrientation() const { return QuatVFromMat34V(m_Matrix); }

	__forceinline Vec3V_Out GetA() const { return m_Matrix.GetCol0(); }
	__forceinline Vec3V_Out GetB() const { return m_Matrix.GetCol1(); }
	__forceinline Vec3V_Out GetC() const { return m_Matrix.GetCol2(); }

	float GetRoll() const;
	float GetPitch() const;
	float GetHeading() const;

	__forceinline void SetIdentity() { m_Matrix = Mat34V(V_IDENTITY); InitBitSet();	}
	__forceinline void SetMatrix(Mat34V_In matrix) { m_Matrix = matrix; InitBitSet(); }
	__forceinline void SetPosition(Vec3V_In position) { m_Matrix.SetCol3(position); }
	__forceinline void SetPosition(float, float, float) { ILLEGAL_OPERATION(); }
	void SetHeading(float heading);

	__forceinline Vec3V_Out Transform(Vec3V_In position) const { return rage::Transform(m_Matrix, position); }
	__forceinline Vec3V_Out UnTransform(Vec3V_In position) const { return rage::UnTransformOrtho(m_Matrix, position); }

	__forceinline Vec3V_Out Transform3x3(Vec3V_In position) const { return rage::Transform3x3(m_Matrix, position); }
	__forceinline Vec3V_Out UnTransform3x3(Vec3V_In position) const { return rage::UnTransform3x3Ortho(m_Matrix, position); }

	__forceinline bool IsOrthonormal() const { return m_Matrix.IsOrthonormal3x3(ScalarV(V_FLT_SMALL_2)); }

	__forceinline const Mat34V* GetMatrixPtr() const { return &m_Matrix; }
	
	__forceinline void InitBitSet()						{ SetOverlay(TYPE_MATRIX, 3); }


	~fwMatrixTransform() {}

	FWTRANSFORM_REGISTER_POOL( fwMatrixTransform, fwMatrixTransform );

protected:
	Mat34V m_Matrix;
};

//
// PURPOSE
//	fwMatrixScaledTransform is a 3d transform represented by a matrix with an additional 2d scale (XY + Z) stored int he w components
//	of the first three columns. XY is stored in col0 and col1 and Z is stored in col2.
class fwMatrixScaledTransform : public fwMatrixTransform
{
public:
	fwMatrixScaledTransform(Mat34V_In mat) { SetMatrixWithScale(mat); }
	fwMatrixScaledTransform() { InitBitSet(); }
	~fwMatrixScaledTransform() {}

	void SetHeading(float heading);

	inline Mat34V_Out GetNonOrthoMatrix() const;
	inline void GetNonOrthoMatrixCopy(Mat34V_InOut mat) const;

	__forceinline Vec3V_Out GetNonOrthoA() const { ScalarV scaleXY = GetScaleXYV(); return m_Matrix.GetCol0() * scaleXY; }
	__forceinline Vec3V_Out GetNonOrthoB() const { ScalarV scaleXY = GetScaleXYV(); return m_Matrix.GetCol1() * scaleXY; }
	__forceinline Vec3V_Out GetNonOrthoC() const { ScalarV scaleZ = GetScaleZV(); return m_Matrix.GetCol2() * scaleZ; }

	__forceinline float GetScaleXY() const { return m_Matrix.GetCol1ConstRef().GetWf(); }
	__forceinline float GetScaleZ() const { return m_Matrix.GetCol2ConstRef().GetWf(); }

	__forceinline ScalarV_Out GetScaleXYV() const { return m_Matrix.GetCol1ConstRef().GetW(); }
	__forceinline ScalarV_Out GetScaleZV() const { return m_Matrix.GetCol2ConstRef().GetW(); }

	inline Vec3V_Out GetScaleV() const;

	inline void SetIdentity();
	inline void SetMatrix(Mat34V_In matrix);
	__forceinline void SetMatrixWithScale(Mat34V_In matrix) { m_Matrix = matrix; InitBitSet(); }

	inline void SetScale(float scaleXY, float scaleZ);	
	inline void SetScaleV(ScalarV_In scaleXY, ScalarV_In scaleZ);

	__forceinline Vec3V_Out TransformNonOrtho(Vec3V_In position) const { return rage::Transform(GetNonOrthoMatrix(), position); }
	__forceinline Vec3V_Out UnTransformNonOrtho(Vec3V_In position) const { return rage::UnTransformFull(GetNonOrthoMatrix(), position); }

	FWTRANSFORM_REGISTER_POOL( fwMatrixScaledTransform, fwMatrixTransform );

protected:
	__forceinline void InitBitSet()						{ SetOverlay(TYPE_MATRIX_SCALED, 3); }	
};

//
// PURPOSE
//	fwQuaternionTransform is a 3d transform represented by a quaternion and position.
class fwQuaternionTransform : public fwTransformCore
{
public:
	fwQuaternionTransform() { InitBitSet(); }
	fwQuaternionTransform(Mat34V_In mat) { InitBitSet(); SetMatrix(mat); }
	fwQuaternionTransform(QuatV_In quaternion, Vec3V_In pos) { InitBitSet(); m_Position = pos; m_Quaternion = quaternion; }

	__forceinline void Delete();

	__forceinline Vec3V_Out GetPosition() const { return m_Position; }
	__forceinline Mat34V_Out GetMatrix() const { Mat34V result; Mat34VFromQuatV(result, m_Quaternion, m_Position); return result; }
	__forceinline void GetMatrixCopy(Mat34V_InOut mat) const {  Mat34VFromQuatV(mat, m_Quaternion, m_Position); }
	__forceinline QuatV_Out GetOrientation() const { return m_Quaternion; }

	__forceinline Vec3V_Out GetA() const { return GetMatrix().GetCol0(); }
	__forceinline Vec3V_Out GetB() const { return GetMatrix().GetCol1(); }
	__forceinline Vec3V_Out GetC() const { return GetMatrix().GetCol2(); }

	inline float GetRoll() const;
	inline float GetPitch() const;
	inline float GetHeading() const;

	__forceinline void SetIdentity() { m_Position = Vec3V(V_ZERO); m_Quaternion = QuatV(V_IDENTITY); }
	__forceinline void SetMatrix(Mat34V_In matrix) { m_Position = matrix.GetCol3(); m_Quaternion = QuatVFromMat34V(matrix); }
	__forceinline void SetPosition(Vec3V_In position) { m_Position = position; }
	__forceinline void SetPosition(float, float, float) { ILLEGAL_OPERATION(); }
	__forceinline void SetHeading(float heading);

	__forceinline Vec3V_Out Transform(Vec3V_In position) const { return rage::Transform(m_Quaternion, position)+ m_Position; }
	__forceinline Vec3V_Out UnTransform(Vec3V_In position) const { return rage::UnTransform(m_Quaternion, position - m_Position); }

	__forceinline Vec3V_Out Transform3x3(Vec3V_In position) const { return rage::Transform(m_Quaternion, position); }
	__forceinline Vec3V_Out UnTransform3x3(Vec3V_In position) const { return rage::UnTransform(m_Quaternion, position); }

	__forceinline bool IsOrthonormal() const { return true; }

	__forceinline void InitBitSet()						{ SetOverlay(TYPE_QUATERNION, 0); }

	~fwQuaternionTransform() {}

	FWTRANSFORM_REGISTER_POOL( fwQuaternionTransform, fwQuaternionTransform );

protected:
	float m_ScaleXY;
	float m_ScaleZ;
	float m_Unused;
	fwTransformData m_Data;
	Vec3V m_Position;
	QuatV m_Quaternion;
};

//
// PURPOSE
//	fwQuaternionScaledTransform is a 3d transform represented by a quaternion and position.
class fwQuaternionScaledTransform : public fwQuaternionTransform
{
public:
	fwQuaternionScaledTransform() { InitBitSet(); }
	fwQuaternionScaledTransform(Mat34V_In mat) { InitBitSet(); SetMatrix(mat); }
	fwQuaternionScaledTransform(QuatV_In quaternion, Vec3V_In pos) { InitBitSet(); m_Position = pos; SetScale(1.f, 1.f); m_Quaternion = quaternion; }

	inline Mat34V_Out GetNonOrthoMatrix() const;
	inline void GetNonOrthoMatrixCopy(Mat34V_InOut mat) const;

	__forceinline Vec3V_Out GetNonOrthoA() const { ScalarV scaleXY = GetScaleXYV(); return GetMatrix().GetCol0() * scaleXY; }
	__forceinline Vec3V_Out GetNonOrthoB() const { ScalarV scaleXY = GetScaleXYV(); return GetMatrix().GetCol1() * scaleXY; }
	__forceinline Vec3V_Out GetNonOrthoC() const { ScalarV scaleZ = GetScaleZV(); return GetMatrix().GetCol2() * scaleZ; }

	inline void SetIdentity();

	__forceinline Vec3V_Out TransformNonOrtho(Vec3V_In position) const { return rage::Transform(GetNonOrthoMatrix(), position); }
	__forceinline Vec3V_Out UnTransformNonOrtho(Vec3V_In position) const { return rage::UnTransformFull(GetNonOrthoMatrix(), position); }

	__forceinline bool IsOrthonormal() const { return false; }

	__forceinline void InitBitSet()						{ SetOverlay(TYPE_QUATERNION_SCALED, 0); }

	__forceinline void SetScale(float scaleXY, float scaleZ)	{ m_ScaleXY = scaleXY; m_ScaleZ = scaleZ; }

	__forceinline float GetScaleXY() const				{ return m_ScaleXY; }
	__forceinline float GetScaleZ() const				{ return m_ScaleZ; }
	__forceinline Vec3V_Out GetScaleV() const;

	__forceinline ScalarV_Out GetScaleXYV() const		{ return ScalarVFromF32(m_ScaleXY); }
	__forceinline ScalarV_Out GetScaleZV() const		{ return ScalarVFromF32(m_ScaleZ); }


	~fwQuaternionScaledTransform() {}
};


// PURPOSE
//	fwSimpleTransform is a 3d transform represented by a position and sin & cos of a heading.  
class fwSimpleTransform : public fwTransformCore
{
public:
	fwSimpleTransform() { InitBitSet(); }
	fwSimpleTransform(float x, float y, float z, float heading) { SetPosition(x, y, z); SetHeading(heading); InitBitSet(); }
	fwSimpleTransform(float x, float y, float z, float heading, float cosHeading, float sinHeading) { SetPosition(x, y, z); SetHeading(heading, cosHeading, sinHeading); InitBitSet(); }

	__forceinline void Delete();

	__forceinline Vec3V_Out GetPosition() const { return m_Position; }
	__forceinline Mat34V_Out GetMatrix() const;
	inline void GetMatrixCopy(Mat34V_InOut mat) const;
	QuatV_Out GetOrientation() const;

	__forceinline Vec3V_Out GetA() const { return Vec3V(m_CosHeading, m_SinHeading, 0.0f); }
	__forceinline Vec3V_Out GetB() const { return Vec3V(-m_SinHeading, m_CosHeading, 0.0f); }
	__forceinline Vec3V_Out GetC() const { return Vec3V(V_Z_AXIS_WZERO); }

	__forceinline float GetRoll() const { return 0.0f; } 
	__forceinline float GetPitch() const { return 0.0f; };
	__forceinline float GetHeading() const { return m_Heading; }

	__forceinline void SetIdentity() { m_Position.ZeroComponents(); m_CosHeading = 1.0f; m_SinHeading = 0.0f; m_Heading = 0.0f; }
	__forceinline void SetMatrix(Mat34V_In) { ILLEGAL_OPERATION(); }
	__forceinline void SetPosition(Vec3V_In position) { m_Position = position;  }
	__forceinline void SetPosition(float x, float y, float z) { m_Position = Vec3V(x, y, z); }
	__forceinline void SetHeading(float heading) { rage::cos_and_sin(m_CosHeading, m_SinHeading, heading); m_Heading = heading; }
	__forceinline void SetHeading(float heading, float cosHeading, float sinHeading) { m_CosHeading = cosHeading; m_SinHeading = sinHeading; m_Heading = heading; }

	__forceinline Vec3V_Out Transform(Vec3V_In position) const;
	__forceinline Vec3V_Out UnTransform(Vec3V_In position) const;

	__forceinline Vec3V_Out Transform3x3(Vec3V_In position) const;
	__forceinline Vec3V_Out UnTransform3x3(Vec3V_In position) const;

	FWTRANSFORM_REGISTER_POOL( fwSimpleTransform, fwSimpleTransform );

protected:
	~fwSimpleTransform() {}

	__forceinline void InitBitSet()						{ SetOverlay(TYPE_SIMPLE, 1); }

	__forceinline Mat34V_Out GetMatrixInternal() const;
	__forceinline void SetPositionInternal(Vec3V_In position);

	// Since this class is going into the smallocator, we're abusing the fact the class is aligned to 16by and its size will be be 32 by.
	// Otherwise we'd have gone for sin, cos, plus float x, y & z. 
	float m_SinHeading;
	float m_CosHeading;
	float m_Heading;
	fwTransformData m_Data;
	Vec3V m_Position;
};

CompileTimeAssert(sizeof(fwSimpleTransform) == 32);

// PURPOSE
//	fwSimpleScaledTransform is a 3d transform represented by a position, sin & cos of a heading and a 2d scale (XY + Z). The scale is stored
//	as fixed point 2:14 in the w component of the position vector. Scales in the range [0.0, 3.9999] should be accurately stored, larger scales
//	are clamped and negatives aren't supported.
class fwSimpleScaledTransform : public fwSimpleTransform
{
public:
	fwSimpleScaledTransform() { InitBitSet(); }
	fwSimpleScaledTransform(float x, float y, float z, float heading) { SetPosition(x, y, z); SetHeading(heading); SetScale(1.f, 1.f); InitBitSet(); }
	fwSimpleScaledTransform(float x, float y, float z, float heading, float scaleXY, float scaleZ) { SetPosition(x, y, z); SetHeading(heading); SetScale(scaleXY, scaleZ); InitBitSet(); }
	fwSimpleScaledTransform(float x, float y, float z, float heading, float cosHeading, float sinHeading, float scaleXY, float scaleZ) { SetPosition(x, y, z); SetHeading(heading, cosHeading, sinHeading); SetScale(scaleXY, scaleZ); InitBitSet(); }
	__forceinline Mat34V_Out GetNonOrthoMatrix() const;
	inline void GetNonOrthoMatrixCopy(Mat34V_InOut mat) const;

	__forceinline Vec3V_Out GetNonOrthoA() const { ScalarV scaleXY = GetScaleXYV(); return Vec3V(m_CosHeading, m_SinHeading, 0.0f) * scaleXY; }
	__forceinline Vec3V_Out GetNonOrthoB() const { ScalarV scaleXY = GetScaleXYV(); return Vec3V(-m_SinHeading, m_CosHeading, 0.0f) * scaleXY; }
	__forceinline Vec3V_Out GetNonOrthoC() const { ScalarV scaleZ = GetScaleZV(); return Vec3V(Vec2V(V_ZERO), scaleZ); }

	__forceinline void SetIdentity() { m_Position.SetWi(0x40004000); m_CosHeading = 1.0f; m_SinHeading = 0.0f; m_Heading = 0.0f; }
	__forceinline void SetPositionInternal(Vec3V_In position) { m_Position.SetIntrin128(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::Z1,Vec::W2>(position.GetIntrin128(), m_Position.GetIntrin128())); } // preserve m_Position.w
	__forceinline void SetPosition(Vec3V_In position) { SetPositionInternal(position); }
	__forceinline void SetPosition(float x, float y, float z) { SetPositionInternal(Vec3V(x, y, z)); }
	inline void SetScale(float scaleXY, float scaleZ);

	inline Vec3V_Out TransformNonOrtho(Vec3V_In position) const;
	__forceinline Vec3V_Out UnTransformNonOrtho(Vec3V_In position) const;

	__forceinline float GetScaleXY() const;
	__forceinline float GetScaleZ() const;
	__forceinline Vec3V_Out GetScaleV() const;

	__forceinline ScalarV_Out GetScaleXYV() const;
	__forceinline ScalarV_Out GetScaleZV() const;

	FWTRANSFORM_REGISTER_POOL( fwSimpleScaledTransform, fwSimpleTransform );

private:
	__forceinline void InitBitSet()						{ SetOverlay(TYPE_SIMPLE_SCALED, 1); }

	~fwSimpleScaledTransform() {}

	__forceinline Mat34V_Out GetNonOrthoMatrixInternal() const;
};

} // namespace rage

#include "transform.inl"


#endif // SCENE_TRANSFORM_H_
