//
// entity/extensiblebase.cpp : base class for all objects that need script guid support
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//
#include "extensiblebase.h"

namespace rage {

INSTANTIATE_RTTI_CLASS(fwExtensibleBase,0x87F50B35);

}