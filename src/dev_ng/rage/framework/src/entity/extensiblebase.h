//
// entity/extensiblebase.h : base class for all objects that need script guid support
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef ENTITY_BASE_H_
#define ENTITY_BASE_H_

#include "fwtl/regdrefs.h"
#include "fwutil/rtti.h"
#include "extensionlist.h"

namespace rage {

#if __SPU
#define PPUVIRTUAL
#else
#define PPUVIRTUAL virtual
#endif

class fwExtensibleBase : public fwRefAwareBase
{
	DECLARE_RTTI_BASE_CLASS(fwExtensibleBase);

public:
	PPUVIRTUAL ~fwExtensibleBase() {}

	// PURPOSE: Get list of extensions attached to this entity
	const fwExtensionList& GetExtensionList() const {return m_extensionList;}
	fwExtensionList& GetExtensionList() {return m_extensionList;}

	// PURPOSE: Get an extension by passing in a templatised class.
	template<typename _Type> const _Type* GetExtension() const { return static_cast<const _Type*>(m_extensionList.Get(_Type::GetAutoId())); }
	template<typename _Type> _Type* GetExtension() { return static_cast<_Type*>(m_extensionList.Get(_Type::GetAutoId())); }

    // PURPOSE: Destroy an extension by passing in a templatised class.
    template<typename _Type> void DestroyExtension() { m_extensionList.Destroy(_Type::GetAutoId()); }
protected:
	fwExtensionList m_extensionList;	//	8 bytes @ offset 8
};
CompileTimeAssertSize(fwExtensibleBase,16,32);//	Just enough room for the vtbl pointer, fwRefAwareBase and m_extensionList

} // namespace rage

#endif // ENTITY_BASE_H_