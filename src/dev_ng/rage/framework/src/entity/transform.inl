//
// scene/transform.h : base class for 3d transform description
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#ifndef SCENE_TRANSFORM_INL_
#define SCENE_TRANSFORM_INL_

#include "entity/transform.h"


namespace rage {


inline float fwMatrixTransform::GetRoll() const
{
	ScalarV roll = MagXY(m_Matrix.GetCol0());
	BoolV reverse = IsLessThan(SplatZ(m_Matrix.GetCol2()), ScalarV(V_ZERO));
	roll = SelectFT(reverse, roll, -roll);
	return Arctan2(SplatZ(m_Matrix.GetCol0()), roll).Getf();
}

inline float fwMatrixTransform::GetPitch() const
{
	ScalarV pitch = MagXY(m_Matrix.GetCol1());
	BoolV reverse = IsLessThan(SplatZ(m_Matrix.GetCol2()), ScalarV(V_ZERO));
	pitch = SelectFT(reverse, pitch, -pitch);
	return Arctan2(SplatZ(m_Matrix.GetCol1()), pitch).Getf();
}

__forceinline float fwMatrixTransform::GetHeading() const
{
	return Atan2f(m_Matrix.GetM10f(), m_Matrix.GetM11f()); 
}

inline void fwMatrixTransform::SetHeading(float heading)
{
	Mat33V rot;
	Mat33VFromZAxisAngle(rot, ScalarVFromF32(heading));
	m_Matrix.Set3x3(rot);

	// Type
	InitBitSet();
}




inline Mat34V_Out fwMatrixScaledTransform::GetNonOrthoMatrix() const
{
	Vec3V scale = GetScaleV();
	Mat34V nonOrthoMat;
	nonOrthoMat.SetCol0(m_Matrix.GetCol0() * scale);
	nonOrthoMat.SetCol1(m_Matrix.GetCol1() * scale);
	nonOrthoMat.SetCol2(m_Matrix.GetCol2() * scale);
	nonOrthoMat.SetCol3(m_Matrix.GetCol3());

	// NOTE: This code is probably not necessary
	//float w = m_Matrix.GetCol0ConstRef().GetWf();
	//nonOrthoMat.GetCol0Ref().SetWf(w);

	return nonOrthoMat;
}

inline void fwMatrixScaledTransform::GetNonOrthoMatrixCopy(Mat34V_InOut mat) const
{
	Vec3V scale = GetScaleV();
	mat.SetCol0(m_Matrix.GetCol0() * scale);
	mat.SetCol1(m_Matrix.GetCol1() * scale);
	mat.SetCol2(m_Matrix.GetCol2() * scale);
	mat.SetCol3(m_Matrix.GetCol3());

	// NOTE: This code is probably not necessary
	//float w = m_Matrix.GetCol0ConstRef().GetWf();
	//mat.GetCol0Ref().SetWf(w);
}

inline Vec3V_Out fwMatrixScaledTransform::GetScaleV() const
{
	ScalarV scaleXY = GetScaleXYV();
	ScalarV scaleZ = GetScaleZV();

	return Vec3V(Vec2V(scaleXY), scaleZ);
}

inline void fwMatrixScaledTransform::SetMatrix(Mat34V_In matrix)
{
	ScalarV scaleXY = GetScaleXYV();
	ScalarV scaleZ = GetScaleZV();
	m_Matrix = matrix;

	SetScaleV(scaleXY, scaleZ);

	// Type
	InitBitSet();
}

inline void fwMatrixScaledTransform::SetHeading(float heading)
{
	ScalarV scaleXY = GetScaleXYV();
	ScalarV scaleZ = GetScaleZV();

	Mat33V rot;
	Mat33VFromZAxisAngle(rot, ScalarVFromF32(heading));
	m_Matrix.Set3x3(rot);
	
	SetScaleV(scaleXY, scaleZ);

	// Type
	InitBitSet();
}

inline void fwMatrixScaledTransform::SetScale(float scaleXY, float scaleZ)
{
	m_Matrix.GetCol1Ref().SetWf(scaleXY);
	m_Matrix.GetCol2Ref().SetWf(scaleZ);
}

inline void fwMatrixScaledTransform::SetScaleV(ScalarV_In scaleXY, ScalarV_In scaleZ)
{
	m_Matrix.GetCol1Ref().SetW(scaleXY);
	m_Matrix.GetCol2Ref().SetW(scaleZ);
}

inline void fwMatrixScaledTransform::SetIdentity()
{
	m_Matrix = Mat34V(V_IDENTITY);

	const ScalarV one(V_ONE);
	m_Matrix.GetCol1Ref().SetW(one);
	m_Matrix.GetCol2Ref().SetW(one);

	// Type
	InitBitSet();
}






inline float fwQuaternionTransform::GetRoll() const
{
	Mat34V mat;

	Mat34VFromQuatV(mat, m_Quaternion);
	ScalarV roll = MagXY(mat.GetCol0());
	BoolV reverse = IsLessThan(SplatZ(mat.GetCol2()), ScalarV(V_ZERO));
	roll = SelectFT(reverse, roll, -roll);
	return Arctan2(SplatZ(mat.GetCol0()), roll).Getf();
}

inline float fwQuaternionTransform::GetPitch() const
{
	Mat34V mat;

	Mat34VFromQuatV(mat, m_Quaternion);
	ScalarV pitch = MagXY(mat.GetCol1());
	BoolV reverse = IsLessThan(SplatZ(mat.GetCol2()), ScalarV(V_ZERO));
	pitch = SelectFT(reverse, pitch, -pitch);
	return Arctan2(SplatZ(mat.GetCol1()), pitch).Getf();
}

inline float fwQuaternionTransform::GetHeading() const
{
	Mat34V mat;

	Mat34VFromQuatV(mat, m_Quaternion);
	return Atan2f(mat.GetM10f(), mat.GetM11f()); 
}

__forceinline void fwQuaternionTransform::SetHeading(float heading)
{
	m_Quaternion = QuatVFromZAxisAngle(ScalarV(heading));
}





inline Mat34V_Out fwQuaternionScaledTransform::GetNonOrthoMatrix() const
{
	Mat34V rotMat;
	Mat34VFromQuatV(rotMat, m_Quaternion, m_Position);

	Vec3V scale = GetScaleV();
	Mat34V nonOrthoMat;
	nonOrthoMat.SetCol0(rotMat.GetCol0() * scale);
	nonOrthoMat.SetCol1(rotMat.GetCol1() * scale);
	nonOrthoMat.SetCol2(rotMat.GetCol2() * scale);
	nonOrthoMat.SetCol3(rotMat.GetCol3());
	return nonOrthoMat;
}

inline void fwQuaternionScaledTransform::GetNonOrthoMatrixCopy(Mat34V_InOut mat) const
{
	Mat34V rotMat;
	Mat34VFromQuatV(rotMat, m_Quaternion, m_Position);

	Vec3V scale = GetScaleV();
	mat.SetCol0(rotMat.GetCol0() * scale);
	mat.SetCol1(rotMat.GetCol1() * scale);
	mat.SetCol2(rotMat.GetCol2() * scale);
	mat.SetCol3(rotMat.GetCol3());
}

inline Vec3V_Out fwQuaternionScaledTransform::GetScaleV() const
{
	ScalarV scaleXY = GetScaleXYV();
	ScalarV scaleZ = GetScaleZV();

	return Vec3V(Vec2V(scaleXY), scaleZ);
}

inline void fwQuaternionScaledTransform::SetIdentity()
{
	m_Quaternion = QuatV(V_IDENTITY);
	m_Position = Vec3V(V_ZERO);
	m_ScaleXY = 1.0f;
	m_ScaleZ = 1.0f;
}



__forceinline Mat34V_Out fwSimpleTransform::GetMatrixInternal() const
{
	struct fwSimpleTransformInternal
	{
		Vec4V m_Sin_Cos_Heading;
		Vec4V m_Position;
	};

	const fwSimpleTransformInternal *transform = (const fwSimpleTransformInternal *) this;

	Vec3V constant(Vec::V4VConstant<0x80000000,U32_ZERO,U32_ZERO,U32_ZERO>());

	// Let's put m_CosHeading and m_SinHeading into a's .x and .y respectively.
	// This only works if they are where we think they are. Let's verify.
	CompileTimeAssert(OffsetOf(fwSimpleTransform, m_SinHeading) == 0);
	CompileTimeAssert(OffsetOf(fwSimpleTransform, m_CosHeading) == 4);

	Vec3V a = And((transform->m_Sin_Cos_Heading.Get<Vec::Y, Vec::X, Vec::Z>()), Vec3V(V_MASKXY));
	Vec3V bperm = a.Get<Vec::Y, Vec::X, Vec::Z>();
	Vec3V c(V_Z_AXIS_WONE);
	Vec3V b = Xor(bperm, constant);

	return Mat34V(a, b, c, m_Position);
}


inline void fwSimpleTransform::GetMatrixCopy(Mat34V_InOut mat) const 
{ 
	Vec3V constant(Vec::V4VConstant<0x80000000,U32_ZERO,U32_ZERO,U32_ZERO>());
	Vec3V a(m_CosHeading, m_SinHeading, 0.0f);
	Vec3V bperm = a.Get<Vec::Y, Vec::X, Vec::Z>();
	Vec3V c(V_Z_AXIS_WONE);
	Vec3V b = Xor(bperm, constant);

	mat.SetCols(a, b, c, m_Position);
}

__forceinline Mat34V_Out fwSimpleTransform::GetMatrix() const
{
	Mat34V mat = GetMatrixInternal();
	return mat;
}

__forceinline Vec3V_Out fwSimpleTransform::Transform(Vec3V_In position) const 
{
	Mat34V mat = GetMatrixInternal();
	return rage::Transform(mat, position);
}

__forceinline Vec3V_Out fwSimpleTransform::Transform3x3(Vec3V_In position) const 
{
	Mat34V mat = GetMatrixInternal();
	return rage::Transform3x3(mat, position);
}

__forceinline Vec3V_Out fwSimpleTransform::UnTransform(Vec3V_In position) const 
{
	Mat34V mat = GetMatrixInternal();
	return rage::UnTransformOrtho(mat, position);
}

__forceinline Vec3V_Out fwSimpleTransform::UnTransform3x3(Vec3V_In position) const 
{
	Mat34V mat = GetMatrixInternal();
	return rage::UnTransform3x3Ortho(mat, position);
}




__forceinline Mat34V_Out fwSimpleScaledTransform::GetNonOrthoMatrixInternal() const
{
	ScalarV scaleXY = GetScaleXYV();
	ScalarV scaleZ = GetScaleZV();
	Vec3V constant(Vec::V4VConstant<0x80000000,U32_ZERO,U32_ZERO,U32_ZERO>());
	Vec3V a = Vec3V(m_CosHeading, m_SinHeading, 0.0f) * scaleXY;
	Vec3V bperm = a.Get<Vec::Y, Vec::X, Vec::Z>();
	Vec3V c(Vec2V(V_ZERO), scaleZ);
	Vec3V b = Xor(bperm, constant);

	return Mat34V(a, b, c, m_Position);
}

__forceinline float fwSimpleScaledTransform::GetScaleXY() const
{
	u32 iScale = m_Position.GetWi();
	iScale >>= 16;

	float scaleXY = (iScale >> 14) + (iScale & 0x3FFF) * 0.0001f;
	return scaleXY;
}

__forceinline float fwSimpleScaledTransform::GetScaleZ() const
{
	u32 iScale = m_Position.GetWi();
	iScale &= 0xFFFF;

	float scaleZ = (iScale >> 14) + (iScale & 0x3FFF) * 0.0001f;
	return scaleZ;
}

__forceinline Vec3V_Out fwSimpleScaledTransform::GetScaleV() const
{
	u32 iScale = m_Position.GetWi();

	u32 iScaleXY = iScale >> 16;
	float scaleXY = (iScaleXY >> 14) + (iScaleXY & 0x3FFF) * 0.0001f;

	u32 iScaleZ = iScale & 0xFFFF;
	float scaleZ = (iScaleZ >> 14) + (iScaleZ & 0x3FFF) * 0.0001f;

	return Vec3V(scaleXY, scaleXY, scaleZ);
}

__forceinline ScalarV_Out fwSimpleScaledTransform::GetScaleXYV() const
{
	return ScalarVFromF32(GetScaleXY());
}

__forceinline ScalarV_Out fwSimpleScaledTransform::GetScaleZV() const
{
	return ScalarVFromF32(GetScaleZ());
}

__forceinline Mat34V_Out fwSimpleScaledTransform::GetNonOrthoMatrix() const
{
	Mat34V mat = GetNonOrthoMatrixInternal();
	return mat;
}

inline void fwSimpleScaledTransform::GetNonOrthoMatrixCopy(Mat34V_InOut mat) const 
{
	ScalarV scaleXY = GetScaleXYV();
	ScalarV scaleZ = GetScaleZV();
	Vec3V constant(Vec::V4VConstant<0x80000000,U32_ZERO,U32_ZERO,U32_ZERO>());
	Vec3V a = Vec3V(m_CosHeading, m_SinHeading, 0.0f) * scaleXY;
	Vec3V bperm = a.Get<Vec::Y, Vec::X, Vec::Z>();
	Vec3V c(Vec2V(V_ZERO), scaleZ);
	Vec3V b = Xor(bperm, constant);

	mat.SetCols(a, b, c, m_Position);
}

inline void fwSimpleScaledTransform::SetScale(float scaleXY, float scaleZ)
{
	u16 scaleXY16 = ConvertF32ToFixed2_14(scaleXY);
	u16 scaleZ16 = ConvertF32ToFixed2_14(scaleZ);

	u32 packedScale = (scaleXY16 << 16) | scaleZ16;
	m_Position.SetWi(packedScale);
}

__forceinline Vec3V_Out fwSimpleScaledTransform::TransformNonOrtho(Vec3V_In position) const 
{
	Mat34V mat = GetNonOrthoMatrixInternal();
	return rage::Transform(mat, position);
}

__forceinline Vec3V_Out fwSimpleScaledTransform::UnTransformNonOrtho(Vec3V_In position) const 
{
	Mat34V mat = GetNonOrthoMatrixInternal();
	return rage::UnTransformFull(mat, position);
}



// Following here are a bunch of non-FWTRANSFORM_VIRTUAL class free functions to access fwTransform
// data without FWTRANSFORM_VIRTUAL function calls.
__forceinline Vec3V_Out fwTransformGetPosition(const fwTransform *transform)
{
	Vec3V result = ((Vec3V *) transform)[transform->GetPositionOffset()];

	return result;
}

__forceinline Vec3V_Out fwQuaternionTransformGetPosition(const fwTransform *transform)
{
	FastAssert(transform->IsQuaternionTransform());

	Vec3V result = ((fwQuaternionTransform *) transform)->fwQuaternionTransform::GetPosition();

	return result;
}


__forceinline Vec3V_Out fwMatrixTransformGetPosition(const fwTransform *transform)
{
	FastAssert(transform->IsMatrixTransform());

	Vec3V result = ((fwMatrixTransform *) transform)->fwMatrixTransform::GetMatrixPtr()->GetCol3();

	return result;
}

__forceinline Vec3V_Out fwMatrixTransformTransform(const fwTransform *transform, Vec3V_In v)
{
	FastAssert(transform->IsMatrixTransform());

	Vec3V result = ((fwMatrixTransform *) transform)->fwMatrixTransform::Transform(v);

	return result;
}

__forceinline Mat34V_Out fwMatrixTransformGetMatrix(const fwTransform *transform)
{
	FastAssert(transform->IsMatrixTransform());

	const Mat34V *result = ((fwMatrixTransform *) transform)->fwMatrixTransform::GetMatrixPtr();

	return *result;
}

__forceinline Mat34V_Out fwSimpleTransformGetMatrix(const fwTransform *transform)
{
	FastAssert(transform->IsSimpleTransform());

	const Mat34V &result = ((fwSimpleTransform *) transform)->fwSimpleTransform::GetMatrix();

	return result;
}

__forceinline Mat34V_Out fwQuaternionTransformGetMatrix(const fwTransform *transform)
{
	FastAssert(transform->IsQuaternionTransform());

	return ((fwQuaternionTransform *) transform)->fwQuaternionTransform::GetMatrix();
}

__forceinline Vec3V_Out fwQuaternionTransformTransform(const fwTransform *transform, Vec3V_In v)
{
	FastAssert(transform->IsQuaternionTransform());

	Vec3V result = ((fwQuaternionTransform *) transform)->fwQuaternionTransform::Transform(v);

	return result;
}

__forceinline Mat34V_Out fwTransformGetMatrixInternal(const fwTransform *transform)
{
	if (transform->IsMatrixTransform())
	{
		return fwMatrixTransformGetMatrix(transform);
	}

	if (transform->IsQuaternionTransform())
	{
		return fwQuaternionTransformGetMatrix(transform);
	}

	if (transform->IsSimpleTransform())
	{
		return fwSimpleTransformGetMatrix(transform);
	}

	return Mat34V(V_IDENTITY);
}

__forceinline Mat34V_Out fwTransformGetMatrix(const fwTransform *transform)
{
	return fwTransformGetMatrixInternal(transform);
}



// PURPOSE: Delete the transform.
__forceinline void fwSimpleTransform::Delete()
{
	delete this;
}

__forceinline void fwMatrixTransform::Delete()
{
	delete this;
}

__forceinline void fwQuaternionTransform::Delete()
{
	delete this;
}

FWTRANSFORM_VIRTUAL_FUNCTION(void, Delete, (), ());

// PURPOSE: Get the matrix.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Mat34V_Out, GetMatrix, (), ());

// PURPOSE: Get the non-orthogonal matrix (i.e. apply scale before return) if supported
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(Mat34V_Out, GetNonOrthoMatrix, (), ());

// PURPOSE: Get a copy of the matrix.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(void, GetMatrixCopy, (Mat34V_InOut mat), (mat));

// PURPOSE: Get a copy of the non-orthogonal matrix (i.e. apply scale before return) if supported
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(void, GetNonOrthoMatrixCopy, (Mat34V_InOut mat), (mat));

// PURPOSE: Get the position.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST_INLINE(Vec3V_Out, GetPosition, (), ());

// PURPOSE: Returns the transform's A vector, column 0, right axis. 
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, GetA, (), ());

// PURPOSE: Returns the transform's B vector, column 1, forward axis. 
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, GetB, (), ());

// PURPOSE: Returns the transform's C vector, column 2, up axis. 
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, GetC, (), ());

// PURPOSE: Returns the non-orthogonal transform's A vector, column 0, right axis. 
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(Vec3V_Out, GetNonOrthoA, (), ());

// PURPOSE: Returns the non-orthogonal transform's B vector, column 1, forward axis. 
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(Vec3V_Out, GetNonOrthoB, (), ());

// PURPOSE: Returns the non-orthogonal transform's C vector, column 2, up axis. 
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(Vec3V_Out, GetNonOrthoC, (), ());

// PURPOSE: Returns the transform's up roll (rotation around Y).
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(float, GetRoll, (), ());

// PURPOSE: Returns the transform's up roll (rotation around X).
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(float, GetPitch, (), ());

// PURPOSE: Returns the transform's up roll (rotation around Z).
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(float, GetHeading, (), ());

// PURPOSE: Returns the transform's XY scale
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(ScalarV_Out, GetScaleXYV, (), ());

// PURPOSE: Returns the transform's Z scale.
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(ScalarV_Out, GetScaleZV, (), ());

// PURPOSE: Returns the transform's scale.
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(Vec3V_Out, GetScaleV, (), ());

// PURPOSE: Set the transform to identity rotation and null translation.
FWTRANSFORM_VIRTUAL_FUNCTION(void, SetIdentity, (), ());

// PURPOSE: Set the transform's matrix.
FWTRANSFORM_VIRTUAL_FUNCTION(void, SetMatrix, (Mat34V_In mat), (mat));

// PURPOSE: Set the transform's position as a vector.
FWTRANSFORM_VIRTUAL_FUNCTION(void, SetPosition, (Vec3V_In pos), (pos));

// PURPOSE: Set the transform's position as floats.
FWTRANSFORM_VIRTUAL_FUNCTION(void, SetPosition, (float x, float y, float z), (x, y, z));

// PURPOSE: Set the transform's heading.
FWTRANSFORM_VIRTUAL_FUNCTION(void, SetHeading, (float heading), (heading));

// PURPOSE: Set the two dimensional scale if the matrix supports it
FWTRANSFORM_VIRTUAL_FUNCTION(void, SetScale, (float x, float y), (x, y));

// PURPOSE: Transform the passed vector into world space.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, Transform, (Vec3V_In position), (position));

// PURPOSE: Transform the passed vector into object space.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, UnTransform, (Vec3V_In position), (position));

// PURPOSE: Rotate the passed vector into world space.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, Transform3x3, (Vec3V_In position), (position));

// PURPOSE: Rotate the passed vector into object space.
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(Vec3V_Out, UnTransform3x3, (Vec3V_In position), (position));

// PURPOSE: Return is the matrix is orthonormal. 
FWTRANSFORM_VIRTUAL_FUNCTION_CONST(bool, IsOrthonormal, (), ());

// PURPOSE: Return the orientation of the transform
FWTRANSFORM_VIRTUAL_FUNCTION_NO_SCALE_CONST(QuatV_Out, GetOrientation, (), ());


} // namespace rage

#endif // SCENE_TRANSFORM_H_
