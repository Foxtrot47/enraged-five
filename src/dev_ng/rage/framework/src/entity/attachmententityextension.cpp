
#include "attachmententityextension.h"

//rage
#include "entity/camerarelativeextension.h"
#include "fwanimation/animdirector.h"
#include "physics/simulator.h"
#include "physics/constraintmgr.h"
#include "profile/profiler.h"
#include "vectormath\vec3v.h"
#include "grcore/debugdraw.h"
#include "vector\colors.h"

//framework
#include "entity\transform.h"
#include "fwsys\timer.h"

namespace rage {

#if COMMERCE_CONTAINER
FW_INSTANTIATE_CLASS_POOL_NO_FLEX_SPILLOVER(fwAttachmentEntityExtension, CONFIGURED_FROM_FILE, 0.39f, atHashString("AttachmentExtension",0x7e95953));
#else
FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwAttachmentEntityExtension, CONFIGURED_FROM_FILE, 0.39f, atHashString("AttachmentExtension",0x7e95953));
#endif

PF_PAGE(fwAttachmentExtension, "fwAttachmentExtension");
PF_GROUP(fwAttachmentExtensionGroup);
PF_LINK(fwAttachmentExtension, fwAttachmentExtensionGroup);

PF_VALUE_INT(AttachmentExtPoolCount, fwAttachmentExtensionGroup);

#if __BANK
int fwAttachmentEntityExtension::sm_AttachmentExtCount = 0;
#endif // __BANK

fwAttachmentEntityExtension::fwAttachmentEntityExtension(fwEntity* thisEntity)
: m_nOtherAttachBone(-1)
, m_nMyAttachBone(-1)
, m_nAttachFlags(0)
, m_pNoCollisionEntity(NULL)
{
	m_vecAttachOffset.Zero();
	m_AttachQuat.Identity();
	m_vecAttachParentOffset.Zero();

	m_pAttachParent = NULL;
	m_pAttachChild = NULL;
	m_pAttachSibling = NULL;
	m_pThisEntity = thisEntity;

	m_nNumConstraintHandles = 0;

#if __BANK
	
	m_strCodeFunction = "(unset)";
	m_strCodeFile = "(unset)";
	m_nCodeLine = 0xFFFFFFFF;

	sm_AttachmentExtCount++;
	PF_SET(AttachmentExtPoolCount, sm_AttachmentExtCount);
#endif // __BANK
}

fwAttachmentEntityExtension::~fwAttachmentEntityExtension()
{
#if 0 //DetachFromParentAndChildren should have done all of this work

	//Destroy all the tree edges with children
	while(fwEntity* attachChild = GetChildAttachment()) /* After calling ClearParentAttachmentVars on the first child, the child pointer will then point at the next child */
	{
		fwAttachmentEntityExtension* attachChildExtension = attachChild->GetAttachmentExtension();
		Assert(attachChildExtension);
		attachChildExtension->ClearParentAttachmentVars(	false /* bDestroyParentNodeIfZeroAdjacency is false because we are in the destructor of the parent */, 
															true /* bDestroyChildNodeIfZeroAdjacency */,
															this /* parentExtension needs to be passed in because it isn't in the extension list now */ );
	}

	//Destroy the tree edge with the parent
	ClearParentAttachmentVars(	true /* bDestroyParentNodeIfZeroAdjacency */, 
								false /* bDestroyChildNodeIfZeroAdjacency is false because we are in the destructor of the child */);
#endif

	Assert( m_pAttachChild == NULL && m_pAttachParent == NULL && m_pAttachSibling == NULL && m_nNumConstraintHandles == 0 );

#if __BANK
	sm_AttachmentExtCount--;
	PF_SET(AttachmentExtPoolCount, sm_AttachmentExtCount);
#endif // __BANK
}

bool fwAttachmentEntityExtension::IsAttachStateBasicDerived() const
{
	int attachState = GetAttachState();

	switch(attachState)
	{
		case ATTACH_STATE_BASIC:
		case ATTACH_STATE_PED:			
		case ATTACH_STATE_PED_WITH_ROT:	
		case ATTACH_STATE_PED_ENTER_CAR:
		case ATTACH_STATE_PED_IN_CAR:	
		case ATTACH_STATE_PED_EXIT_CAR:	
		case ATTACH_STATE_PED_ON_MOUNT:	
		case ATTACH_STATE_PED_ON_GROUND:
			return true;

		case ATTACH_STATE_WORLD:
		case ATTACH_STATE_WORLD_PHYSICAL:
		case ATTACH_STATE_RAGDOLL:		
		case ATTACH_STATE_PHYSICAL:
		case ATTACH_STATE_NONE:
		case ATTACH_STATE_DETACHING:
		default:
			return false;
	}
}

void fwAttachmentEntityExtension::NotifyChildDetachment(fwEntity* pDetachingEntity, fwEntity* pSiblingOfDetachingEntity,
														bool bDestroyThisNodeIfZeroAdjacency)
{
	fwEntity *childAttachment = GetChildAttachment();
	if(Verifyf(childAttachment,"Invalid call to notify child detachment: This entity has no children"))
	{
		if(childAttachment == pDetachingEntity)
		{
			SetChildAttachment(pSiblingOfDetachingEntity);

			if( bDestroyThisNodeIfZeroAdjacency && !IsNodeAdjacencyNonZero() )
			{
				//This must be the last thing that this fwAttachmentEntityExtension does
				m_pThisEntity->DeleteAttachmentExtension();
			}
		}
		else //We currently have two or more children so certainly don't need to consider destroying the fwAttachmentEntityExtension
		{
			fwAttachmentEntityExtension *childAttachExt = childAttachment->GetAttachmentExtension();
			
			if (Verifyf(childAttachExt, "Detaching child does not have an attachment extension"))
			{
				childAttachExt->NotifySiblingDetachment(pDetachingEntity, pSiblingOfDetachingEntity);
			}
		}
	}
}

//New sibling is passed in because we can't do GetAttachmentExtension() if we are in the destructor of that extension
void fwAttachmentEntityExtension::NotifySiblingDetachment(fwEntity* pDetachingEntity, fwEntity* pSiblingOfDetachingEntity)
{
	Assertf(pDetachingEntity,"pDetachingEntity is NULL");

	// Find the detaching entity in our list of siblings.
	fwAttachmentEntityExtension *thisExtension = this;

	while (fwEntity *nextSibling = thisExtension->GetSiblingAttachment())
	{
		if (nextSibling == pDetachingEntity)
		{
			thisExtension->SetSiblingAttachment(pSiblingOfDetachingEntity);
			return;
		}
		else
		{
			thisExtension = nextSibling->GetAttachmentExtension();
			Assertf(thisExtension, "Attached entity does not have an attachment extension");
		}
	}

	Assertf(false, "pDetachingEntity not found in this list of siblings");
}

void fwAttachmentEntityExtension::NotifyChildAttachment(fwEntity* pAttachingEntity)
{
	if(Verifyf(pAttachingEntity,"Unexpected NULL pointer in NotifyChildAttachment"))
	{
		fwEntity *childAttachment = GetChildAttachment();
		if(!childAttachment)
		{
			SetChildAttachment(pAttachingEntity);
		}
		else
		{
			if(Verifyf(pAttachingEntity != childAttachment,"Attempting to add the same child twice!"))
			{
				childAttachment->CreateAttachmentExtensionIfMissing()->NotifySiblingAttachment(pAttachingEntity);
			}
		}
	}
}

void fwAttachmentEntityExtension::NotifySiblingAttachment(fwEntity* pAttachingEntity)
{
	Assertf(pAttachingEntity,"pAttachingEntity is NULL");

	ASSERT_ONLY(fwAttachmentEntityExtension *attachingEntityExt =) pAttachingEntity->CreateAttachmentExtensionIfMissing();

	Assertf(this!=attachingEntityExt,"Attempting to add an entry to the attach tree twice!");

	fwAttachmentEntityExtension *nextSiblingExt = this;
	
	while (fwEntity *nextSibling = nextSiblingExt->GetSiblingAttachment())
	{
		Assertf(nextSibling!=pAttachingEntity,"Attempting to add an entry to the attach tree twice!");

		nextSiblingExt = nextSibling->GetAttachmentExtension();
		Assertf(nextSiblingExt, "Attached sibling doesn't have an extension");
	}

	nextSiblingExt->SetSiblingAttachment(pAttachingEntity);
}

//Can we safely delete the tree node?
bool fwAttachmentEntityExtension::IsNodeAdjacencyNonZero()
{
	if(    GetAttachState() == ATTACH_STATE_WORLD
		|| GetAttachState() == ATTACH_STATE_WORLD_PHYSICAL ) 
	{
		//Attachments to world have NULL parent (and the parent can't disappear)
		return true;
	}
	else
	{
		return m_pAttachParent != NULL || m_pAttachChild != NULL;
	}
}

void fwAttachmentEntityExtension::ClearParentAttachmentVars(bool bDestroyParentNodeIfZeroAdjacency, bool bDestroyChildNodeIfZeroAdjacency, 
															fwAttachmentEntityExtension* parentExtension)
{
	fwEntity *attachParent = GetAttachParentForced();

	if(attachParent)
	{
		if(parentExtension == NULL) 
			parentExtension = attachParent->GetAttachmentExtension();
		Assert(parentExtension);
		parentExtension->NotifyChildDetachment(m_pThisEntity, m_pAttachSibling, bDestroyParentNodeIfZeroAdjacency);
	}

	SetAttachParent(NULL);
	SetSiblingAttachment(NULL);

	SetOtherAttachBone(-1);
	SetMyAttachBone(-1);
	SetAttachFlags(0);

	RemoveAttachmentConstraints();

	if(bDestroyChildNodeIfZeroAdjacency && !IsNodeAdjacencyNonZero())
	{
		m_pThisEntity->DeleteAttachmentExtension();
	}
}

void fwAttachmentEntityExtension::SetParentAttachment(fwEntity *thisEntity, fwEntity* pNewParent)
{
	fwEntity *attachParentForced = GetAttachParentForced();

	if(attachParentForced != pNewParent)
	{
		// We already have an attachment, and it's different from the new one, so clean it up properly
		ClearParentAttachmentVars(true, /* don't destroy this node */ false);

		if(pNewParent)
		{
			// We've got a new parent and its not the same as the old
			pNewParent->CreateAttachmentExtensionIfMissing()->NotifyChildAttachment(thisEntity);
		}

		SetAttachParent(pNewParent);
	}
}

fwEntity* fwAttachmentEntityExtension::FindParentWithCameraRelativeExtension()
{
	fwAttachmentEntityExtension* pAttachExtention = this;

	while (pAttachExtention!=NULL)
	{
		fwEntity* pEnt = pAttachExtention->GetAttachParent();
		pAttachExtention = pEnt ? pEnt->GetAttachmentExtension() : NULL;
		if (pAttachExtention && pEnt && pEnt->GetExtension<fwCameraRelativeExtension>())
		{
			return pEnt;
		}
	}

	return NULL;
}

bool fwAttachmentEntityExtension::FindParentAndOffsetWithCameraRelativeExtension(fwEntity*& pEnt, Mat34V_InOut parentRelativeOffset)
{
	fwEntity* pCameraRelativeParent = FindParentWithCameraRelativeExtension();

	if(pCameraRelativeParent==NULL)
	{
		return false;
	}

	pEnt = pCameraRelativeParent;

	GetOffsetFromParent(pCameraRelativeParent, parentRelativeOffset);
	return true;
}

void fwAttachmentEntityExtension::GetOffsetFromParent(fwEntity* pParentEntity, Mat34V_InOut parentRelativeOffset, bool useThirdPersonSkeleton)
{
	Assertf(pParentEntity!=NULL, "No parent entity specified!");
	if (pParentEntity==NULL)
	{
		return;
	}

	// run up the attachment hierarchy transforming the offsets in object space.
	fwAttachmentEntityExtension* pAttachExtention = this;
	fwAttachmentEntityExtension* pParentAttachExtension = pParentEntity->GetAttachmentExtension();

	Assertf(pParentAttachExtension!=NULL, "Parent entity must have an attachment extension!");
	if (pParentAttachExtension==NULL)
	{
		return;
	}

	while (pAttachExtention && pAttachExtention!=pParentAttachExtension)
	{
		fwEntity* pParentEntity = pAttachExtention->GetAttachParent();
		if(!pParentEntity)
		{
			break;
		}
		Mat34V offsetFromParent(V_IDENTITY);
		pAttachExtention->GetOffsetFromParent(offsetFromParent, useThirdPersonSkeleton);
		Transform(parentRelativeOffset, offsetFromParent, parentRelativeOffset);
		pAttachExtention = pParentEntity->GetAttachmentExtension();
	}

	if (pAttachExtention==NULL)
	{
		Assertf(pAttachExtention, "The provided entity is not an attach parent of this extension!");
		parentRelativeOffset = Mat34V(V_IDENTITY);
		return;
	}	
}

void fwAttachmentEntityExtension::GetOffsetFromParent(Mat34V_InOut parentRelativeOffset, bool useThirdPersonSkeleton)
{
	Matrix34 matOffset;
	matOffset.Identity();
	fwEntity* pAttachParent = GetAttachParent();

	if (pAttachParent==NULL)
	{
		return;
	}

	if (!IsAttachStateBasicDerived())
	{
		// if we're not running a basic attachment, the actual offset will be determined by physical interactions / etc.
		// Return the actual untransformed offset in those cases.
		UnTransformOrtho(parentRelativeOffset, pAttachParent->GetMatrix(), m_pThisEntity->GetMatrix());
		return;
	}

	matOffset.FromQuaternion(GetAttachQuat());
	matOffset.d = GetAttachOffset();
	// Check for NaNs in the position vector.
	Assert(GetAttachOffset().IsEqual(GetAttachOffset()));

	if(GetOtherAttachBone() > -1 && pAttachParent->GetSkeleton())
	{
		// B*1853481: Replicating some logic from CVfxHelper::GetMatrixFromBoneIndex to ensure we get a valid matrix even for damaged frag objects.
		// CVfxHelper::GetMatrixFromBoneIndex provides a global matrix, but we need an object matrix here.
		Matrix34 matLocal;
		if(useThirdPersonSkeleton)
		{
			matLocal = pAttachParent->GetThirdPersonSkeletonObjectMtx(GetOtherAttachBone());
		}
		else
		{
			matLocal = pAttachParent->GetObjectMtx(GetOtherAttachBone());
		}

		if(pAttachParent->GetFragInst() && pAttachParent->GetFragInst()->GetCacheEntry() && pAttachParent->GetFragInst()->GetCacheEntry()->GetHierInst())
		{
			const crSkeleton *const pDamagedSkel =  pAttachParent->GetFragInst()->GetCacheEntry()->GetHierInst()->damagedSkeleton;				
			if(matLocal.a.IsZero() && pDamagedSkel)
			{
				Mat34V boneMatDmg;
				boneMatDmg = pDamagedSkel->GetObjectMtx(GetOtherAttachBone());
				matLocal.Set(MAT34V_TO_MATRIX34(boneMatDmg));
			}
		}

		// transform the offset by the bone matrix
		matOffset.Dot(matLocal);

		// find out the attaching position using scale and local matrix.
		// GetObjectMtx() doesn't return correct position if the parent is using a scaled transform.
		const fwTransform* pTransform = pAttachParent->GetTransformPtr();
		if(pTransform->IsMatrixScaledTransform())
		{
			Vector3 vScaledOffset = matOffset.d;
			float fScaleXY = ((fwMatrixScaledTransform*)pTransform)->GetScaleXY();
			float fScaleZ = ((fwMatrixScaledTransform*)pTransform)->GetScaleZ();
			vScaledOffset.x *= fScaleXY;
			vScaledOffset.y *= fScaleXY;
			vScaledOffset.z *= fScaleZ;
			matOffset.d = vScaledOffset;
		}
	}

	parentRelativeOffset = RC_MAT34V(matOffset);
}

bool fwAttachmentEntityExtension::GetRequiresPostPreRender() const
{
	bool bRequiresPostProcess = (GetOtherAttachBone() > -1);

	if(!bRequiresPostProcess)
	{
		fwEntity *child = GetChildAttachment();
		if(child)
		{
			const fwAttachmentEntityExtension *childExtension = child->GetAttachmentExtension();
			if(childExtension)
				bRequiresPostProcess = childExtension->GetRequiresPostPreRender();
		}

		if(!bRequiresPostProcess)
		{
			fwEntity *sibling = GetSiblingAttachment();
			if (sibling)
			{
				const fwAttachmentEntityExtension *siblingExtension = sibling->GetAttachmentExtension();
				if(siblingExtension)
					bRequiresPostProcess = siblingExtension->GetRequiresPostPreRender();
			}
		}
	}

	return bRequiresPostProcess;
}

void fwAttachmentEntityExtension::AttachToEntity(fwEntity *thisEntity, fwEntity *otherEntity, s16 nEntBone, u32 nAttachFlags, const Vector3* pVecOffset, const Quaternion* pQuatOrientation, const Vector3* pVecParentOffset, s16 nMyBone)
{
	SetParentAttachment(thisEntity, otherEntity);

#if ENABLE_FRAG_OPTIMIZATION
	if(otherEntity && otherEntity->GetBoneCount() > 0 && Verifyf(nEntBone < (int)otherEntity->GetBoneCount(), "Bone index [%d] is not valid for skeleton [%s], range [0..%d]", nEntBone, otherEntity->GetModelName(), otherEntity->GetBoneCount()))
#else
	if(otherEntity && otherEntity->GetSkeleton() && Verifyf(nEntBone < (int)otherEntity->GetSkeleton()->GetBoneCount(), "Bone index [%d] is not valid for skeleton [%s], range [0..%d]", nEntBone, otherEntity->GetModelName(), otherEntity->GetSkeleton()->GetBoneCount()))
#endif	
	{
		SetOtherAttachBone(nEntBone);
	}
	else
	{
		SetOtherAttachBone(-1);
	}
	SetMyAttachBone(nMyBone);
	SetAttachFlags(nAttachFlags);

	if(pVecOffset)
	{
		// Check for NaNs in the position vector.
		Assert(pVecOffset->IsEqual(*pVecOffset));
		SetAttachOffset(*pVecOffset);
	}
	else
		SetAttachOffset(VEC3_ZERO);

	if(pQuatOrientation)
		SetAttachQuat(*pQuatOrientation);
	else
		SetAttachQuat(Quaternion(Quaternion::IdentityType));

	if (pVecParentOffset)
		SetAttachParentOffset(*pVecParentOffset);
	else
		SetAttachParentOffset(VEC3_ZERO);
}

void fwAttachmentEntityExtension::RemoveAttachmentConstraints()
{
	for(int i = 0 ; i < m_nNumConstraintHandles ; i++)
	{
		PHCONSTRAINT->Remove(m_ParentAttachmentConstraints[i]);
		m_ParentAttachmentConstraints[i].Reset();
	}
	
	m_nNumConstraintHandles = 0;
}

//Storing constraint handles
bool fwAttachmentEntityExtension::AddConstraintHandle(phConstraintHandle& handle)
{
	if(m_nNumConstraintHandles == ATTACHMENT_MAX_CONSTRAINTS) return false;
	
	m_ParentAttachmentConstraints[m_nNumConstraintHandles++] = handle;
	return true;
}

phConstraintHandle fwAttachmentEntityExtension::GetConstraintHandle(int i)
{
	return m_ParentAttachmentConstraints[i];
}

int fwAttachmentEntityExtension::GetNumConstraintHandles()
{
	return m_nNumConstraintHandles;
}

phConstraintBase* fwAttachmentEntityExtension::FindConstraint(const phInst* pInstChild, const phInst* pInstParent, u16 componentChild, u16 componentParent)
{
	for(int i = 0 ; i < m_nNumConstraintHandles ; i++)
	{
		phConstraintBase* pConstraint = PHCONSTRAINT->GetTemporaryPointer(m_ParentAttachmentConstraints[i]);

		if( Verifyf(pConstraint, "Attachment extension contains a constraint handle to a removed constraint") )
		{
			if(		componentChild == pConstraint->GetComponentA()
				&&	componentParent == pConstraint->GetComponentB()
				&&	pInstChild == pConstraint->GetInstanceA()
				&&	pInstParent == pConstraint->GetInstanceB() )
			{
				return pConstraint;
			}
		}
	}

	return NULL;
}

#if __BANK

void fwAttachmentEntityExtension::RenderDebug(bool drawAttachmentExtensions, bool drawAttachmentEdges, bool displayAttachmentEdgeType, bool displayAttachmentEdgeData, 
											  bool displayInvokingFunction, bool displayInvokingFile, bool displayPhysicsInfo, bool ATTACHMENT_ENABLE_UPDATE_RECORDS_PARAM(displayUpdateRecords), 
											  float nodeRadius, bool nodeRelPosWorldUp, float nodeEntityDist) const
{
	fwTransform const& transform = m_pThisEntity->GetTransform();

	Vec3V drawExtensionPos = transform.GetPosition() + (ScalarV(nodeEntityDist) * (nodeRelPosWorldUp ? Vec3V(V_UP_AXIS_WZERO) : transform.GetUp()));
	Vec3V drawParentExtensionPos;

	if( GetAttachState() == ATTACH_STATE_NONE )
	{
		Assert(m_pAttachParent == NULL);
	}
	else if(	GetAttachState() == ATTACH_STATE_WORLD
			||	GetAttachState() == ATTACH_STATE_WORLD_PHYSICAL )
	{
		Assert(m_pAttachParent == NULL);

		drawParentExtensionPos = drawExtensionPos + (ScalarV(V_ONE) * transform.GetForward());
	}
	else
	{
		Assert(m_pAttachParent != NULL);

		fwAttachmentEntityExtension* parentExtension = m_pAttachParent ? m_pAttachParent->GetAttachmentExtension() : NULL;
		fwTransform const * parentTransform = parentExtension->m_pThisEntity->GetTransformPtr();
		drawParentExtensionPos = parentTransform->GetPosition() + (ScalarV(nodeEntityDist) * (nodeRelPosWorldUp ? Vec3V(V_UP_AXIS_WZERO) : parentTransform->GetUp()));
	}

	if( drawAttachmentExtensions )
		grcDebugDraw::Sphere(drawExtensionPos, nodeRadius, Color_DarkOrange, false);

	if( drawAttachmentEdges )
	{
		if(GetAttachState() != ATTACH_STATE_NONE)
		{
			grcDebugDraw::Arrow(drawExtensionPos, drawParentExtensionPos, 0.25f, Color_ForestGreen);
		}
	}

	int lineNum = 0;
	char printBuffer[512];
	Vec3V midPos = ScalarV(V_HALF) * (drawExtensionPos + drawParentExtensionPos);

	if( displayAttachmentEdgeType )
	{
		Vec3V midPos = ScalarV(V_HALF) * (drawExtensionPos + drawParentExtensionPos);

		if(GetAttachState() != ATTACH_STATE_NONE)
		{
			grcDebugDraw::Text(midPos, Color_DarkOrange, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, 
				fwAttachmentEntityExtension::GetAttachStateString(GetAttachState()), true);
		}
	}

	if( displayInvokingFunction )
	{
		if(GetAttachState() != ATTACH_STATE_NONE)
			grcDebugDraw::Text(midPos, Color_DarkOrange, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, m_strCodeFunction, true);
	}

	if( displayInvokingFile )
	{
		if(GetAttachState() != ATTACH_STATE_NONE)
		{
			snprintf(printBuffer, 512, "%s:%u", m_strCodeFile, m_nCodeLine);
			grcDebugDraw::Text(midPos, Color_DarkOrange, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
		}
	}

	if( displayAttachmentEdgeData )
	{
		if(GetAttachState() != ATTACH_STATE_NONE)
		{
			snprintf(printBuffer, 512, "this extension: 0x%p", this);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_pThisEntity: 0x%p(%s)", m_pThisEntity, fwArchetypeManager::GetArchetypeName(m_pThisEntity->GetModelIndex()));
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_pAttachParent: 0x%p(%s)", m_pAttachParent, m_pAttachParent ? fwArchetypeManager::GetArchetypeName(m_pAttachParent->GetModelIndex()) : "");
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_pAttachChild: 0x%p", m_pAttachChild);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_pAttachSibling: 0x%p", m_pAttachSibling);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_pNoCollisionEntity: 0x%p", m_pNoCollisionEntity.Get());
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			const char * myBoneName = "entity";
			const char * parentBoneName = m_pAttachParent ? "entity" : "null";
			if( m_nMyAttachBone > -1 )
			{
				myBoneName = m_pThisEntity->GetSkeleton()->GetSkeletonData().GetBoneData( m_nMyAttachBone )->GetName();
			}
			if( m_pAttachParent && m_nOtherAttachBone > -1 )
			{
				parentBoneName = m_pAttachParent->GetSkeleton()->GetSkeletonData().GetBoneData( m_nOtherAttachBone )->GetName();
			}
			snprintf(printBuffer, 512, "m_nMyAttachBone: %d(%s)", m_nMyAttachBone, myBoneName);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_nOtherAttachBone: %d(%s)", m_nOtherAttachBone, parentBoneName);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_nNumConstraintHandles: %u", m_nNumConstraintHandles);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_vecAttachOffset: (%f, %f, %f)", m_vecAttachOffset.GetX(), m_vecAttachOffset.GetY(), m_vecAttachOffset.GetZ());
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_vecAttachParentOffset: (%f, %f, %f)", m_vecAttachParentOffset.GetX(), m_vecAttachParentOffset.GetY(), m_vecAttachParentOffset.GetZ());
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "m_AttachQuat: (%f, %f, %f, %f)", m_AttachQuat.x, m_AttachQuat.y, m_AttachQuat.z, m_AttachQuat.w);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);

			u32 attachFlags = m_nAttachFlags & ~u32(ATTACH_STATES);
			u32 bitMask = 1u;

			while(attachFlags)
			{
				if( attachFlags & bitMask )
				{
					snprintf(printBuffer, 512, "%s", fwAttachmentEntityExtension::GetAttachFlagString(bitMask));
					grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
					attachFlags &= ~bitMask;
				}

				bitMask = bitMask << 1u;
			}
		}
	}

	if( displayPhysicsInfo )
	{
		if(GetAttachState() != ATTACH_STATE_NONE)
		{
			Matrix34 matOffset;
					 matOffset.Identity();
					 matOffset.FromQuaternion(GetAttachQuat());
					 matOffset.d = GetAttachOffset();

			Matrix34 matParent;
					 matParent.Identity();

			phInst* thisInst = m_pThisEntity->GetCurrentPhysicsInst();
			bool	thisInstIsFrag = (thisInst != NULL) && (thisInst == m_pThisEntity->GetFragInst());
			bool	thisInstInLevel = (thisInst != NULL) && thisInst->IsInLevel();
			bool	thisInstFlagFLAG_NEVER_ACTIVATE = (thisInst != NULL) && thisInst->GetInstFlag(phInst::FLAG_NEVER_ACTIVATE);
			bool	thisInstIsActive = (!thisInstInLevel) ? false : PHLEVEL->IsActive(thisInst->GetLevelIndex());
			u32		thisInstLevelInstanceIncludeFlags = (!thisInstInLevel) ? 0u : PHLEVEL->GetInstanceIncludeFlags(thisInst->GetLevelIndex());
			bool	thisEntityFlagUSE_COLLISION = m_pThisEntity->IsProtectedBaseFlagSet(fwEntity::USES_COLLISION);

			snprintf(printBuffer, 512, "[m_pThisEntity Physics Info]");
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "GetCurrentPhysicsInst: 0x%p(IsFragInst?%u,InLevel?%u,Active?%u)", thisInst, (u32)thisInstIsFrag, (u32)thisInstInLevel, (u32)thisInstIsActive);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "phInst::FLAG_NEVER_ACTIVATE: %u. fwEntity::USE_COLLISION: %u", (u32)thisInstFlagFLAG_NEVER_ACTIVATE, (u32)thisEntityFlagUSE_COLLISION);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "PHLEVEL->GetInstanceIncludeFlags: 0x%X", thisInstLevelInstanceIncludeFlags);
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);

			if(m_pAttachParent)
			{
				Matrix34 matParent = MAT34V_TO_MATRIX34(m_pAttachParent->GetMatrix());
				if(GetOtherAttachBone() > -1 && m_pAttachParent->GetSkeleton())
				{
					m_pAttachParent->GetGlobalMtx(GetOtherAttachBone(), matParent);
				}

				phInst* parentInst = m_pAttachParent->GetCurrentPhysicsInst();
				bool	parentInstIsFrag = (parentInst != NULL) && (parentInst == m_pAttachParent->GetFragInst());
				bool	parentInstInLevel = (parentInst != NULL) && parentInst->IsInLevel();
				bool	parentInstFlagFLAG_NEVER_ACTIVATE = (parentInst != NULL) && parentInst->GetInstFlag(phInst::FLAG_NEVER_ACTIVATE);
				bool	parentInstIsActive = (!parentInstInLevel) ? false : PHLEVEL->IsActive(parentInst->GetLevelIndex());
				u32		parentInstLevelInstanceIncludeFlags = (!parentInstInLevel) ? 0u : PHLEVEL->GetInstanceIncludeFlags(parentInst->GetLevelIndex());
				bool	parentEntityFlagUSE_COLLISION = m_pAttachParent->IsProtectedBaseFlagSet(fwEntity::USES_COLLISION);
			
				snprintf(printBuffer, 512, "[m_pAttachParent Physics Info]");
				grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
				snprintf(printBuffer, 512, "GetCurrentPhysicsInst: 0x%p(IsFragInst?%u,InLevel?%u,Active?%u)", parentInst, (u32)parentInstIsFrag, (u32)parentInstInLevel, (u32)parentInstIsActive);
				grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
				snprintf(printBuffer, 512, "phInst::FLAG_NEVER_ACTIVATE: %u. fwEntity::USE_COLLISION: %u", (u32)parentInstFlagFLAG_NEVER_ACTIVATE, (u32)parentEntityFlagUSE_COLLISION);
				grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
				snprintf(printBuffer, 512, "PHLEVEL->GetInstanceIncludeFlags: 0x%X", parentInstLevelInstanceIncludeFlags);
				grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			}

			snprintf(printBuffer, 512, "[Matrix Info]");
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			snprintf(printBuffer, 512, "Parent matrix non-orth: %f. Attachment matrix non-orth: %f", matParent.MeasureNonOrthonormality(), matOffset.MeasureNonOrthonormality());
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
		}
	}

#if ATTACHMENT_ENABLE_UPDATE_RECORDS
	if( displayUpdateRecords )
	{
		if(GetAttachState() != ATTACH_STATE_NONE)
		{
			snprintf(printBuffer, 512, "[Non-Orthonormality Update Records]");
			grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);

			for(int i = 0 ; i < m_DebugUpdateRecords.GetCount() ; i++)
			{
				fwAttachmentUpdateRecord const& record = m_DebugUpdateRecords[i];
				snprintf(printBuffer, 512, "%u:%s", record.m_frameNumber, record.m_strCodeFunction);
				grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
				snprintf(printBuffer, 512, " Par:%f. Att: %f. Bef: %f. Aft: %f", record.m_fNonOrthoParent, record.m_fNonOrthoAttach, record.m_fNonOrthoThisBefore, record.m_fNonOrthoThisAfter);
				grcDebugDraw::Text(midPos, Color_black, 0, (grcDebugDraw::GetScreenSpaceTextHeight()-1) * lineNum++, printBuffer, true);
			}
		}
	}
#endif
}

void fwAttachmentEntityExtension::DebugSetInvocationData(const char *strCodeFile, const char* strCodeFunction, u32 nCodeLine)
{
	m_strCodeFile = strCodeFile;
	m_strCodeFunction = strCodeFunction;
	m_nCodeLine = nCodeLine;
}

char const * fwAttachmentEntityExtension::DebugGetInvokingFunction() const
{
	return m_strCodeFunction;
}

#endif //__BANK

#if ATTACHMENT_ENABLE_UPDATE_RECORDS
void fwAttachmentEntityExtension::AddUpdateRecord(const char *strCodeFile, const char* strCodeFunction, u32 nCodeLine, float fNonOrthoParent,
		float fNonOrthoAttach, float  fNonOrthoThisBefore, float fNonOrthoThisAfter)
{
	if( m_DebugUpdateRecords.IsFull() )
	{
		m_DebugUpdateRecords.Drop();
	}

	fwAttachmentUpdateRecord& newRecord = m_DebugUpdateRecords.Append();
	
	newRecord.m_strCodeFunction = strCodeFunction;
	newRecord.m_strCodeFile = strCodeFile;
	newRecord.m_nCodeLine = nCodeLine;

	newRecord.m_frameNumber = fwTimer::GetFrameCount();

	newRecord.m_fNonOrthoParent = fNonOrthoParent;
	newRecord.m_fNonOrthoAttach = fNonOrthoAttach;
	newRecord.m_fNonOrthoThisBefore = fNonOrthoThisBefore;
	newRecord.m_fNonOrthoThisAfter = fNonOrthoThisAfter;
}

#endif

#if __DEV
template<> void fwPool<fwAttachmentEntityExtension>::PoolFullCallback() 
{
	s32 size = GetSize();
	int iIndex = 0;
	while(size--)
	{
		fwAttachmentEntityExtension* pExtension = GetSlot(size);
		if(pExtension)
		{
#if __BANK
			Displayf("|Attachment pool| %i, [%s], Entity: %s",
				iIndex,
				pExtension->DebugGetInvokingFunction(),
				pExtension->GetThisEntity()->GetModelName());
#else // __BANK
			Displayf("%i, Entity: %s",
				iIndex,
				pExtension->GetThisEntity()->GetModelName());
#endif // __BANK
		}
		else
		{
			Displayf("%i, NULL attachment extension", iIndex);
		}
		iIndex++;
	}
}
#endif // __DEV

} // namespace rage

