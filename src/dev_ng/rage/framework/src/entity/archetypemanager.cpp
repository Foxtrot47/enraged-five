//
//
//    Filename: Archetype.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description:
//
//
// Rage headers
#include "atl\binmap.h"
#include "diag\art_channel.h"
#include "file\asset.h"
#include "file\packfile.h"
#include "fragment\resourceversions.h"
#include "paging\rscbuilder.h"
#include "grcore/effect.h"
#if __BANK
#include "fragment/tune.h"
#endif	// __BANK

// Framework headers
#include "entity/archetype.h"
#include "entity/archetypemanager.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwscene/mapdata/mapdata.h"
#include "grcore/debugdraw.h"
#include "fwscene/stores/drawablestore.h"
#include "fwscene/stores/dwdstore.h" 
#include "fwscene/stores/fragmentstore.h"
#include "fwscene/stores/txdstore.h" 
#include "fwscene/stores/maptypesstore.h"
#include "fwsys/metadatastore.h"
#include "streaming/streaming_channel.h"
#include "streaming/streamingengine.h"
#include "streaming/streaminginfo.h"
#include "system/memmanager.h"
#include "vfx/ptfx/ptfxmanager.h"

#if __ASSERT
#include "atl/hashstring.h"
#include "string/stringhash.h"
#endif	//__ASSERT

STREAMING_OPTIMISATIONS();


PARAM(PartialDynArch, "[mapTypes] Disable dynamic streaming for all map .ityp files which are dependencies");
PARAM(numStreamedSlots, "Allow app to specify the value returned by GetNumStreamedSlots().  Workaround for collision exporter tools.");

namespace rage {

//RAGE_DEFINE_CHANNEL(modelinfo)

// static member variables
fwArchetypePool fwArchetypeManager::ms_ArchetypePool;
fwArchetypeMap fwArchetypeManager::ms_ArchetypeMap;
u16 fwArchetypeManager::ms_MaxArchetypes = 32200;		// Note: only meant as a default value. Will be overwritten with the value from fwConfig ('gameconfig.xml') when running the normal game, so increase it in the XML file instead if needed.
bool fwArchetypeManager::ms_bArchetypesLocked = false;

s32 fwArchetypeManager::ms_streamingId;
fwFactoryManager< fwArchetype >				fwArchetypeManager::ms_archetypeFactoryMgr;
fwFactoryManager< fwExtension >		fwArchetypeManager::ms_2dEffectFactoryMgr; 

#if !__NO_OUTPUT
extern fwMapTypesStore g_MapTypesStore;
#endif

///////////////////////////////////////////////////////////////////////

#if __ASSERT
const char* fwModelId::GetTypeFileName()	const		
{ 
	if (s.m_mapTypeDefIndex != TF_INVALID)
	{
		return(g_MapTypesStore.GetName(strLocalIndex(s.m_mapTypeDefIndex))); 
	} 

	return(NULL);
}
#endif //__ASSERT

#define __ENABLE_MEMLOCK	(0)
u16 obfuscate = 0x5C25;

void fwModelId::MemLock()
{
#if __ENABLE_MEMLOCK
	if (!s.m_bIsMemLocked)
	{
		s.m_bIsMemLocked = true;

	//	u32 thisNum = (u16)(((u32)this << 1)& 0x0000ffff) ;
		//u32 thisNum = (u16)(atDataHash())
		u32 thisNum = (((u32)this << 5) ^ ((u32)this >> 3)) & 0x0000ffff;
		u16 XOR_value = thisNum ^ obfuscate;
		s.m_modelIndex ^= XOR_value;
	}
#endif //__ENABLE_MEMLOCK
}

u32 fwModelId::GetModelIndex() const
{
#if __ENABLE_MEMLOCK
	if (!s.m_bIsMemLocked)
	{
		return(s.m_modelIndex);
	}
	else
	{
		//u32 thisNum = (u16)(((u32)this << 1)& 0x0000ffff) ;
		u32 thisNum = (((u32)this << 5) ^ ((u32)this >> 3)) & 0x0000ffff;
		u16 XOR_value = thisNum ^ obfuscate;
		return(s.m_modelIndex ^ XOR_value);
	}
#else //__ENABLE_MEMLOCK
	return(s.m_modelIndex);
#endif //__ENABLE_MEMLOCK
}
//////////////////////////////////////////////////////////////////////

#if __ASSERT
bool fwArchetypeManager::ms_MaxModelInfosHasBeenUsed;	// Intentionally not initialized to false - should be guaranteed to be 0 anyway, and initializing it may prevent catching of really early use.
#endif

#if __BANK
static bool s_ShowStatistics;
#endif // __BANK



//
// name:		PrintArchetypeStoreUsage
// description:	Print how many of each Archetype there are
void fwArchetypeManager::PrintArchetypeStoreUsage()
{
}

u16 fwArchetypeManager::GetNumStreamedSlots()
{
	if(PARAM_numStreamedSlots.Get())
	{
		s32 iNumSlots;
		PARAM_numStreamedSlots.Get(iNumSlots);
		iNumSlots = Min(iNumSlots, 0xfffe);
		return (u16)iNumSlots;
	}
	else if (!PARAM_PartialDynArch.Get())
	{
		return(NUM_STREAMED_SLOTS_FULL);
	} else {
		return(NUM_STREAMED_SLOTS);
	}
}

void fwArchetypeManager::Init(int maxFactoryId)
{
	fwFactoryManager< fwArchetype >::Init( maxFactoryId );

	ASSERT_ONLY(ms_MaxModelInfosHasBeenUsed = true);
	// According to an earlier comment, 16 byte alignment is needed "for DMA to SPU".
	RAGE_TRACK(ArchetypePtrs);

	ms_ArchetypePool.Init(ms_MaxArchetypes);
	ms_ArchetypeMap.Init(static_cast< u16>(ms_MaxArchetypes));

	ms_bArchetypesLocked = false;
}

void fwArchetypeManager::Reset()
{
	ms_ArchetypePool.Reset();
	ms_ArchetypeMap.Reset();
}

void fwArchetypeManager::ClearArchetypePtrStore()
{
	ms_ArchetypePool.Reset();
	ms_ArchetypeMap.Reset();
}

//
// name: fwArchetypeManager::ShutdownLevelWithMapUnloaded
// description: Remove all allocated memory after map has finished
//
void fwArchetypeManager::Shutdown()
{
	ms_ArchetypePool.Reset();
	ms_ArchetypeMap.Reset();
}

//
#if !__NO_OUTPUT
//
// name:		fwArchetypeManager::GetArchetypeName
// description:	Return Archetype name given an index
const char* fwArchetypeManager::GetArchetypeName(u32 index)
{
	if ((u32)index == fwModelId::MI_INVALID)
	{
		return "<invalid archetype index>";
	}

	fwArchetype* pArchetype = fwArchetypeManager::GetArchetype(index);
	if (streamVerifyf(pArchetype, "Missing archetype at index %d", (u32)index))
	{
		return pArchetype->GetModelName();
	}

	return "<<missing archetype>>";
}
#endif

void fwArchetypeManager::SetMaxArchetypes(unsigned int num)
{
	Assertf(num == ms_MaxArchetypes || !ms_MaxModelInfosHasBeenUsed,
			"Archetype limit changed after it's been used, try setting it earlier or using it later.");

	Assert(num < fwModelId::MI_INVALID);
	Assert(num <= 0xffff);
	ms_MaxArchetypes = static_cast<u16>(num);
}

const fwModelId&	fwArchetypeManager::LookupModelId(const fwArchetype* pArchetype)
{
	Assert(pArchetype);
	
	const fwModelId* pModelId = ms_ArchetypeMap.Access(pArchetype->GetModelNameHash());
	Assert(pModelId);

	return (*pModelId);
}

fwArchetype* fwArchetypeManager::GetArchetype(const strStreamingObjectName name, u32* pReturnIndex)
{
	fwModelId* pModelId = ms_ArchetypeMap.Access(name.GetHash());
 
	if (pModelId != NULL) 
	{
		u32 newIndex = pModelId->GetModelIndex();
		if (pReturnIndex)
			*pReturnIndex = newIndex;

		return GetArchetype(newIndex);
	}

	return NULL;
}

fwArchetype* fwArchetypeManager::GetArchetype(const strStreamingObjectName name, fwModelId* pReturnModelId)
{
	fwModelId* pModelId = ms_ArchetypeMap.Access(name.GetHash());

	if (pModelId != NULL) 
	{
		u32 newIndex = pModelId->GetModelIndex();
		if(pReturnModelId)
			*pReturnModelId = *pModelId;

		return GetArchetype(newIndex);
	}

	return NULL;
}

fwArchetype* fwArchetypeManager::GetArchetypeFromHashKey(u32 key, u32* pReturnIndex)
{
	fwModelId* pModelId = ms_ArchetypeMap.Access(key);

	if (pModelId != NULL)
	{
		u32 newIndex = pModelId->GetModelIndex();
		if(pReturnIndex)
			*pReturnIndex = newIndex;

		return GetArchetype(newIndex);
	}

	return NULL;
}

fwArchetype* fwArchetypeManager::GetArchetypeFromHashKey(u32 key, fwModelId& pReturnModelId)
{
	fwModelId* pModelId = ms_ArchetypeMap.Access(key);

	if (pModelId != NULL)
	{
		u32 newIndex = pModelId->GetModelIndex();
		pReturnModelId = *pModelId;
		return GetArchetype(newIndex);
	}

	return NULL;
}

bool fwArchetypeManager::GetArchetypeExistsForHashKey(u32 key)
{
	return ms_ArchetypeMap.Access(key) != NULL;
}

u32 fwArchetypeManager::GetArchetypeIndex(const strStreamingObjectName name)
{
	return GetArchetypeIndexFromHashKey(name.GetHash());
}

u32 fwArchetypeManager::GetArchetypeIndexFromHashKey(u32 key)
{
	fwModelId* pModelId = ms_ArchetypeMap.Access(key);
	return pModelId ? pModelId->GetModelIndex() : fwModelId::MI_INVALID;
}

u16 fwArchetypeManager::RegisterPermanentArchetype(fwArchetype* pArchetype, s32 mapTypeDefIndex, bool bMemLock)
{
	if (ms_bArchetypesLocked)
	{
		Assertf(false, "register %s from %s.ityp failed - registrar is locked", pArchetype->GetModelName(), g_MapTypesStore.GetName(strLocalIndex(mapTypeDefIndex)));
		return fwModelId::MI_INVALID;
	}

	if (ms_ArchetypePool.IsFull())
	{
#if !__NO_OUTPUT
		PoolFullCallback();
		Quitf("Ran out of permanent archetypes : %d", (int)ms_ArchetypePool.GetSize());
#endif //__FINAL

		return fwModelId::MI_INVALID;
	}

	fwArchetype** newArchetype = ms_ArchetypePool.New();
	*newArchetype = pArchetype;
	const strLocalIndex archetypeIndex = strLocalIndex(ms_ArchetypePool.GetIndex(newArchetype));
	fwModelId newModelId(archetypeIndex, mapTypeDefIndex, false);

	// Add Archetype & fwModelId into map
	RAGE_TRACK(ArchetypeMapRegPerm);

	if (bMemLock)
	{
		//yuck
		const atSimplePooledMapType<u32, fwModelId>::Map::Entry* pEntry = &(ms_ArchetypeMap.Insert(pArchetype->GetHashKey(), newModelId));
		const_cast<atSimplePooledMapType<u32, fwModelId>::Map::Entry*>(pEntry)->data.MemLock();
	}
	else
	{
		ms_ArchetypeMap.Insert(pArchetype->GetHashKey(), newModelId);
	}

	Assertf(pArchetype->GetStreamSlot() == INVALID_STREAM_SLOT, "pArchetype does not have an invalid slot, slot = %u %x", pArchetype->GetStreamSlot().Get(), pArchetype->GetStreamSlot().Get());
	pArchetype->SetPermanentStreamSlot((u16)archetypeIndex.Get()); // Padded again here for the locally stored index

	return (u16)archetypeIndex.Get();
}

#if !__NO_OUTPUT
void fwArchetypeManager::PoolFullCallback()
{
	Displayf("**************************************************");
	Displayf("** fwArchetypeManager POOL FULL CALLBACK OUTPUT **");

	Displayf("Index, Model, xPos, yPos");
	for (s32 i = 0; i < (s32) ms_ArchetypePool.GetSize(); ++i)
	{
		fwArchetype** ppArchetype = ms_ArchetypePool.GetSlot(i);	
		if (ppArchetype && *ppArchetype)
		{
			const char* pszModelName = (*ppArchetype)->GetModelName();
			Vector3 pos = (*ppArchetype)->GetBoundingSphereOffset();
			Displayf("%06d, %s, %f, %f", i, pszModelName, pos.GetX(), pos.GetY());
		}
	}

	Displayf("*************************");
	g_MapTypesStore.PrintStore();

	Displayf("*************************");
	Displayf("** Streaming interests **");

	g_strStreamingInterface->DumpStreamingInterests();
}

void fwArchetypeManager::DumpDynamicArchetypesComponents()
{
	Displayf("**********************************************************");
	Displayf("** fwArchetypeManager DUMP DYNAMIC ARCHETYPE COMPONENTS **");

#if __DEV
	Displayf("** fwArchetype DynamicComponentCount : %d", fwArchetype::GetDynamicComponentCount());
#endif
	Displayf("**********************************************************");

	Displayf("Index, Model, xPos, yPos");
	for (s32 i = 0; i < (s32) ms_ArchetypePool.GetSize(); ++i)
	{
		fwArchetype** ppArchetype = ms_ArchetypePool.GetSlot(i);
		if (ppArchetype && *ppArchetype)
		{
			if ((*ppArchetype)->GetDynamicComponentPtr())
			{
				const char* pszModelName = (*ppArchetype)->GetModelName();
				Vector3 pos = (*ppArchetype)->GetBoundingSphereOffset();
				Displayf("%06d, %s, %f, %f", i, pszModelName, pos.GetX(), pos.GetY());
			}
		}
	}
}
#endif

u16 fwArchetypeManager::RegisterStreamedArchetype(fwArchetype* pArchetype, s32 mapTypeDefIndex)
{
	Assert(pArchetype);

	if (ms_ArchetypePool.IsFull())
	{
#if !__NO_OUTPUT
		PoolFullCallback();
		Quitf("Ran out of streamed archetypes : %d", (int)ms_ArchetypePool.GetSize());
#endif //__FINAL

		return fwModelId::MI_INVALID;
	}

	//Displayf(">> REGISTER \"%s\" (0x%p)", pArchetype->GetModelName(), pArchetype);

	fwArchetype** newArchetype = ms_ArchetypePool.New();
	*newArchetype = pArchetype;
	const strLocalIndex archetypeIndex = strLocalIndex(ms_ArchetypePool.GetIndex(newArchetype));
	fwModelId newModelId(archetypeIndex, mapTypeDefIndex, true);

	// Add Archetype & fwModelId into map
	RAGE_TRACK(ArchetypeMapRegStreamed);
	ms_ArchetypeMap.Insert(pArchetype->GetHashKey(), newModelId);

	// Set streaming id
	Assertf(pArchetype->GetStreamSlot() == INVALID_STREAM_SLOT, "pArchetype does not have an invalid slot, slot = %u %x", pArchetype->GetStreamSlot().Get(), pArchetype->GetStreamSlot().Get());
	pArchetype->SetTempStreamSlot( (u16)archetypeIndex.Get());

	return static_cast<u16>(archetypeIndex.Get());
}

void fwArchetypeManager::UnregisterStreamedArchetype(fwArchetype* pArchetype)
{
	Assert(pArchetype && pArchetype->IsStreamedArchetype());
	//Displayf(">> UNREGISTER \"%s\" (0x%p)", pArchetype->GetModelName(), pArchetype);

	const fwModelId* pModelId = ms_ArchetypeMap.Access(pArchetype->GetHashKey());
	Assertf(pModelId, "%p : archetype is not registered! Cannot unregister.", pModelId);
	if (pModelId == NULL)
		return;

	const u32 slotIdx = pModelId->GetModelIndex();
	streamAssert(slotIdx < ms_ArchetypePool.GetSize());

	fwArchetype** poolPtr = ms_ArchetypePool.GetSlot(slotIdx);	
	streamAssert(poolPtr && *(poolPtr) == pArchetype);

	if (poolPtr && *poolPtr == pArchetype)
	{
		*poolPtr = NULL; // set value to NULL
		ms_ArchetypePool.Delete(poolPtr);
		ms_ArchetypeMap.Delete(pArchetype->GetHashKey());
	}

	strStreamingEngine::GetInfo().UnregisterObject(GetStreamingModule()->GetStreamingIndex(strLocalIndex(slotIdx)));
}

// this archetype is streamable *but* is also resident for the duration of the current content context (e.g. duration of being in MP)
bool fwArchetypeManager::IsArchetypePermanentDLC(fwArchetype* pArchetype)
{
	if (pArchetype && pArchetype->IsStreamedArchetype())
	{
		const fwModelId	modelID = fwArchetypeManager::LookupModelId((fwArchetype*)pArchetype);
		if (modelID.IsValid())
		{
			strLocalIndex mapTypeDefIndex = modelID.GetMapTypeDefIndex();
			fwMapTypesDef* pDef = g_MapTypesStore.GetSlot(mapTypeDefIndex);
			if (pDef)
			{
				return(pDef->GetIsPermanentDLC());
			}
		}
	}

	return(false);
}

void ShutDownArchetypesCB(fwArchetype* pArch)
{
	if (pArch)
	{
		pArch->Shutdown();
	}
}

// free the archetypes matching the given type def index (free a target .ityp file contents of archetypes)
void fwArchetypeManager::FreeArchetypes(s32 mapTypeDefIndex){

	// shutdown pass (archetypes)
	for (u32 x=0; x<ms_archetypeFactoryMgr.GetCount(); x++)
	{
		fwArchetypeFactoryBase *factory =  ms_archetypeFactoryMgr.GetFactory(x); //ms_Factories[x];

		if (factory)
		{
			factory->ForAllUsed(mapTypeDefIndex, ShutDownArchetypesCB);
		}
	}
	
	{
#if (__PS3 || __XENON) && !__TOOL && !__RESOURCECOMPILER
		sysMemAutoUseFragCacheMemory mem;
#endif
		// destruction pass (archetypes)
		for (u32 x=0; x<ms_archetypeFactoryMgr.GetCount(); x++)
		{
			fwArchetypeFactoryBase *factory = ms_archetypeFactoryMgr.GetFactory(x); //ms_Factories[x];

			if (factory)
			{
				factory->FreeStorageBlock(mapTypeDefIndex);
			}
		}
	}

	// destruction pass (extensions)
	for (u32 x=0; x<ms_2dEffectFactoryMgr.GetCount(); x++)
	{
		fwArchetypeExtensionFactoryBase *factory = ms_2dEffectFactoryMgr.GetFactory(x); //ms_Factories[x];

		if (factory)
		{
			factory->FreeStorageBlock(mapTypeDefIndex);
		}
	}
}

// once archetypes for a an .ityp are loaded, they may require some fixup before being used by runtime code
void fwArchetypeManager::ForAllArchetypesInFile(s32 mapTypeDefIndex, void callBack(fwArchetype*) ){

	for (u32 x=0; x<ms_archetypeFactoryMgr.GetCount(); x++)
	{
		fwArchetypeFactoryBase *factory = ms_archetypeFactoryMgr.GetFactory(x); //ms_Factories[x];

		if (factory)
		{
			factory->ForAllUsed(mapTypeDefIndex, callBack);
		}
	}
}

// if an object is created via a non .imap code path, then the .ityp file defining it must be ref'ed
void fwArchetypeManager::AddArchetypeToTypeFileRef(fwArchetype *pArchetype)
{
	Assert(pArchetype);

	// lookup the .ityp file which defines this archetype
	if (pArchetype->IsStreamedArchetype())
	{
		fwModelId modelId;
		modelId = LookupModelId(pArchetype);

		strLocalIndex mapTypeSlotIndex = modelId.GetMapTypeDefIndex();

		// otherwise DLC vehicle crashes on entering garage
		if(mapTypeSlotIndex != fwModelId::TF_INVALID)
		{
			g_MapTypesStore.AddRef(mapTypeSlotIndex, REF_OTHER);
		}
	}
}

// schedule removal of file type ref for this archetype (happens when archetype is known to be safely out of use by RT)
void fwArchetypeManager::ScheduleRemoveArchetypeToTypeFileRef(fwArchetype *pArchetype)
{
	Assert(pArchetype);

	if (pArchetype->IsStreamedArchetype())
	{
		fwModelId modelId;
		modelId = LookupModelId(pArchetype);

		strLocalIndex mapTypeSlotIndex = modelId.GetMapTypeDefIndex();
		gDrawListMgr->AddTypeFileReference(mapTypeSlotIndex.Get());		
		g_MapTypesStore.RemoveRef(mapTypeSlotIndex, REF_OTHER);			// AddTypeFileReference() adds a ref, so make sure we remove one too
	}
}

#if __BANK
void fwArchetypeManager::GetFullArchetypeName(char* pName, fwModelId id)
{
	Assert(id.IsValid());

	if (id.IsValid())
	{
		if (id.IsGlobal())
		{
			sprintf(pName, "%s", GetArchetypeName(id.GetModelIndex()));
		} 
		else
		{
			sprintf(pName, "%s : %s", g_MapTypesStore.GetName(strLocalIndex(id.GetMapTypeDefIndex())), GetArchetypeName(id.GetModelIndex()));
		}
	}
}

void fwArchetypeManager::GetArchetypeFileName(char* pName, fwModelId id)
{
	Assert(id.IsValid());

	if (id.IsValid())
	{
		if (id.IsGlobal())
		{
			sprintf(pName, ":");
		} 
		else
		{
			sprintf(pName, "%s", g_MapTypesStore.GetName(strLocalIndex(id.GetMapTypeDefIndex())));
		}
	}
}

void fwArchetypeManager::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Archetype Manager");
	bank.AddToggle("Show Statistics", &s_ShowStatistics);
	bank.PopGroup();
}

void fwArchetypeManager::DebugDraw()
{
	if (s_ShowStatistics)
	{
#if __DEV
		grcDebugDraw::AddDebugOutputEx(false, "Dynamic archetype components: %d", fwArchetype::GetDynamicComponentCount());
#endif // __DEV

		int archCount = 0;
		int archCapacity = 0;

		grcDebugDraw::AddDebugOutputEx(false, "%40s %20s", "Archetype Factory", "Used/Capacity");


		for (u32 x=0; x<ms_archetypeFactoryMgr.GetCount(); x++)
		{
			fwArchetypeFactoryBase *factory = ms_archetypeFactoryMgr.GetFactory(x); // ms_Factories[x];

			if (factory)
			{
				Assertf(false, "This is only showing use in the GLOBAL storage allocation!!!");

				int localArchCount = factory->GetCount(fwFactory::GLOBAL);
				int localArchCapacity = factory->GetCapacity(fwFactory::GLOBAL);
				archCount += localArchCount;
				archCapacity += localArchCapacity;

				grcDebugDraw::AddDebugOutputEx(false, "%-40s %d/%d", factory->GetDebugName(), localArchCount, localArchCapacity);
			}
		}

		grcDebugDraw::AddDebugOutputEx(false, "Total archetype count: %d/%d", archCount, archCapacity);
	}
}

#endif // __BANK

#if __ASSERT

bool fwArchetypeManager::VerifyNameClashes(const char* name)
{
	Assert(name);
	if(name && *name)
	{
		u32 hash = atStringHash(name);
		if (GetArchetypeExistsForHashKey(hash))
		{
			atHashString nameHash;
			nameHash.SetHash(hash);
			bool samename = !stricmp(name, nameHash.GetCStr());
			if (samename)
			{
	//			artAssertf(false, "Duplicate model for same name %s - REEXPORT NEEDED FOR MAPSECTION", name);
			}
			else
			{	
				artAssertf(false, "Model name clash between %s and %s - model will be ignored - CHANGE EITHER NAME AND REEXPORT", name, nameHash.GetCStr());
			}	
			return false;
		}
	}
	return true;
}

#endif	//__ASSERT

} // namespace rage

