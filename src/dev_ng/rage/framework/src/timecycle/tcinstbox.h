//
// timecycle/tcinstbox.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#ifndef _TIMECYCLE_TCINSTBOX_H_
#define _TIMECYCLE_TCINSTBOX_H_

#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/map.h"
#include "parser/macros.h"
#include "spatialdata/aabb.h"
#include "spatialdata/mnbvh.h"
#include "vectormath/mat34v.h"
#include "vectormath/vec3v.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#endif

namespace rage
{

#define TCINSTBOX_MAX_SEARCH_RESULTS (100)
#define TCINSTBOX_TYPE_DATA_FILE ("common:/data/tcinstbox_types")
#define TCINSTBOX_INST_DATA_FILE ("common:/data/tcinstbox_insts")


//
// type data
class tcInstBoxType
{
friend class tcInstBoxTypeLoader;
public:
	const atHashString& GetName() const			{ return m_name; }
	const atHashString& GetModifier() const		{ return m_modifier; }
	const spdAABB& GetBoxLocal() const			{ return m_aabb; }
	float GetPercentage() const					{ return m_percentage; }
	float GetRange() const						{ return m_range; }
	u8 GetStartHour() const						{ return m_startHour; }
	u8 GetEndHour() const						{ return m_endHour; }

	void SetIsValidNaturalAmbient(bool b)		{ m_validNatAmb = b; }
	bool GetIsValidNaturalAmbient() const		{ return m_validNatAmb; }

	void SetIsValidArtificialAmbient(bool b)	{ m_validArtAmb = b; }
	bool GetIsValidArtificialAmbient() const	{ return m_validArtAmb; }
	
	void SetNaturalAmbient(float f)				{ m_natAmb = f; }
	float GetNaturalAmbient() const				{ return m_natAmb; }
	void SetArtificialAmbient(float f) 			{ m_artAmb = f; }
	float GetArtificialAmbient() const			{ return m_artAmb; }

	float GetRecipRange() const					{ return m_recipRange; }


private:
	// Parsed
	spdAABB			m_aabb;
	atHashString	m_name;
	atHashString	m_modifier;
	float			m_percentage;
	float			m_range;
	u8				m_startHour;
	u8				m_endHour;

	// Massaged
	bool			m_validNatAmb;
	bool			m_validArtAmb;	
	float			m_natAmb;
	float			m_artAmb;

	float			m_recipRange;
	
	PAR_SIMPLE_PARSABLE;
};

//
// loads and stores type data
class tcInstBoxTypeLoader
{
public:
	void LoadData();
	void SaveData();

	tcInstBoxType* GetType(const atHashString& typeName)
	{
		s32* pResult = m_typeMap.Access(typeName.GetHash());
		if (pResult)
		{
			return &m_boxTypes[*pResult];
		}
		return NULL;
	}

	int GetTypeCount() const { return m_boxTypes.GetCount(); }
	tcInstBoxType* GetType(int idx) { return &m_boxTypes[idx]; }
private:
	void PostLoad();

	atArray<tcInstBoxType> m_boxTypes;
	atMap<u32, s32> m_typeMap;

	PAR_SIMPLE_PARSABLE;

#if __BANK
	friend class tcInstBoxMgr;
#endif	//__BANK
};


// instance of a template tc box
class ALIGNAS(16) tcInstBoxInst
{
public:
	Vec3V_Out GetPos() const { return Vec3V_Out(m_x, m_y, m_z); }

	float m_x, m_y, m_z;
	atHashString m_typeName;

	PAR_SIMPLE_PARSABLE;

} ;

// loads and stores instance data
class tcInstBoxInstLoader
{
public:
	void LoadData();
	void SaveData();

	atArray<tcInstBoxInst>	m_boxInstances;

	PAR_SIMPLE_PARSABLE;
};


//
// search result
struct tcInstBoxSearchResult
{
	spdAABB m_aabb;
	tcInstBoxType* m_pType;

	float CalcStrengthMult(Vec3V_In vPos, const int hours, const int minutes, const int seconds) const;
};


//
// loads meta data, supports queries
class tcInstBoxMgr
{
public:
	void Init();
	void Shutdown();
	void Search(const spdAABB& aabb, atArray<tcInstBoxSearchResult>& results);
	
	tcInstBoxTypeLoader &GetTypeStore() { return m_typeStore; }
	
private:
	void BuildTree();
	void GetInstData(const s32 instIndex, tcInstBoxSearchResult& result)
	{
		const tcInstBoxInst& inst		= m_instStore.m_boxInstances[instIndex];
		const Vec3V vPos				= inst.GetPos();
		tcInstBoxType* pBoxType			= m_typeStore.GetType(inst.m_typeName);
		Assert(pBoxType);

		const Vec3V vMin = Add( vPos, pBoxType->GetBoxLocal().GetMin() );
		const Vec3V vMax = Add( vPos, pBoxType->GetBoxLocal().GetMax() );

		result.m_aabb.Set(vMin, vMax);
		result.m_pType = pBoxType;
	}

	tcInstBoxInstLoader		m_instStore;
	tcInstBoxTypeLoader		m_typeStore;
	MNBVHSimpleInterface*	m_pTree;

	//////////////////////////////////////////////////////////////////////////
#if __BANK
public:
	void InitWidgets(bkBank* pBank);
	void UpdateDebug(Vec3V_In vCamPos);
#endif	//__BANK
	//////////////////////////////////////////////////////////////////////////
};

extern tcInstBoxMgr g_tcInstBoxMgr;

}	//namespace rage

#endif	//_TIMECYCLE_TCINSTBOX_H_
