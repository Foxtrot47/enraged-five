//
// timecycle/tcmanager.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_MANAGER_H
#define TC_MANAGER_H


// includes (rage)
#include "atl/binmap.h"
#include "system/criticalsection.h"
#include "vectormath/vec3v.h"

// includes (framework)
#include "timecycle/tcdebug.h"
#include "timecycle/tcmodifier.h"
#include "timecycle/tcbox.h"
#include "timecycle/tcboxtree.h"
#include "timecycle/tcconfig.h"


// namespaces
namespace rage {


#define TIMECYCLE_MAX_REGION_BOXES			(32)								// the maximum number of region boxes that can be loaded

// forward declarations
struct tcVarInfo;
struct tcCycleInfo;
struct tcRegionInfo;
struct tcTimeSampleInfo;
class spdAABB;
class tcCycle;

struct tcBoxRemovalInfo
{
	int m_iplId;
	bool m_bIsInterior;
};

// classes 

// PURPOSE
//  - this class is in charge of all things related to the time cycle
//
//  - it stores a list of cycle data that has been loaded in 
//    - this cycle data contains keyframe data for each defined time sample
// 
//  - the manager also stores an array of time cycle modifiers
//    - these contain information about how to modify the keyframe data
//   
//  - the game can also define modifier boxes around the game world
//    - the manager stores this array of modifier boxes and also creates a modifier box tree that
//      can be used for quickly querying which modifier boxes are active at a certain position
//    - an array of modifier strengths (based on a passed in position) is calculated by the manager 
//      so that the game can apply these if required to the base keyframe
class tcManager
{	
	// friends
	friend class tcDebug;

	// functions (public)
	public: 

		tcManager() : m_cycles(NULL)
#if TC_DEBUG

		,
		m_modifierBoxAdded(0),
		m_modifierBoxModified(0),
		m_modifierBoxRemoved(0),
		m_modifierBoxAddedMax(0),
		m_modifierBoxModifiedMax(0),
		m_modifierBoxRemovedMax(0)
#endif
		 {}

		// PURPOSE
		//  initialises the manager with information about the variables that the keyframes will contain
		// PARAMS
		//  numVars - the number of time cycle variables in the keyframe
		//  pVarInfos - a pointer to a game structure that contains information about each variable
		void Init(const int numVars, tcVarInfo* pVarInfos);

		// PURPOSE
		//  initialises the time cycle table information 
		// PARAMS
		//  numCycles - the number of cycles that need loaded
		//  pCycleInfos - a pointer to a structure defining the cycle information
		//  numRegions - the number of regions in each cycle
		//  pCycleInfos - a pointer to a structure defining the region information
		//  numTimeSamples - the number of time samples in each cycle
		//  pTimeSampleInfos - a pointer to a structure defining the time sample information
		void InitMap(const int numCycles, const tcCycleInfo* pCycleInfos, const int numRegions, const tcRegionInfo* pRegionInfos, const int numTimeSamples, const tcTimeSampleInfo* pTimeSampleInfos, const int numDebugGroups, const char** ppDebugGroupNames, const int numUserFlags, const char ** ppUserFlagsNames);

		// PURPOSE
		//  shuts down and tidies up any allocated memory 
		void ShutdownMap();

		// PURPOSE
		//  calculates the current base keyframe and modifier strengths based on position, time and cycle information from the game
		// PARAMS
		//  pos - the position at which to do the update (usually the active camera position)
		//  hours - the time (hours) at which to update the state
		//  minutes - the time (minutes) at which to update the state
		//  minutes - the time (minutes) at which to update the state
		void Update(Vec3V_In vPos, const u32 hours, const u32 minutes, const u32 seconds);

#if TC_DEBUG
		// PURPOSE
		//  renders time cycle debug information 
		void Render() const;
		const tcDebug& GetTCDebug() const {return m_debug;}
		tcDebug& GetTCDebugNonConst() {return m_debug;}
		
		void ShowMeBoxUse();
#endif

		// PURPOSE
		//  Add a new region boxes
		// PARAMS
		//  Region box definition and extra debug data
		void AddRegionBox(const spdAABB& bBox, const u32 hashKey, const float percentage, const float range, const int startHour, const int endHour, const int iplId, const char* pSourceFile);

		// PURPOSE
		//  loads in a file containing modifier data (can be called multiple times if there is more than one file)
		// PARAMS
		//  filename - the name of the modifier file to load
		//  isReload - whether the file is being reloaded from the widgets or not
		void LoadModifierFile(const char* filename);

		void UnloadModifierFile(const char* filename);

		// PURPOSE
		// finish the loading process: resize the strength array, sort the atBinMap
		void FinishModifierLoad();
		
		// PURPOSE
		//  tells the manager to add a new modifier box
		// PARAMS
		//  bBox - the bounding box defining the modifier box to add
		//  hashKey - the hash name of the modifier that this modifier box uses
		//  percentage - the maximum strength of the modifier box
		//  range - the range of the modifier box (modifiers will fade in over this range)
		//  startHour - the time at which the modifier box becomes active
		//  endHour - the time at which the modifier box becomes inactive
		//  iplId - the id of the ipl that owns this modifier box
		//  pSourceFile - a string that helps locate where this add has been called from in case of errors
		//	bTreatAsSphere - treat this new timecycle modifier box as a sphere for strength calculation purposes, etc.
		void AddModifierBox(const spdAABB& bBox, const u32 hashKey, const float percentage, const float range, const int startHour, const int endHour, const int iplId, const char* pSourceFile, bool bTreatAsSphere = false, bool isInterior = false);
		
		// PURPOSE
		//  removed all modifier boxes associated with a specific ipl id
		// PARAMS
		//  iplId - the id on the ipl whose modifier boxes want removed
		void RemoveModifierBoxes(const int iplId, bool isInterior = false);


		// PURPOSE
		//  rebuild modifier tree, needs to be called after AddModifierBox or RemoveModifierBoxes is called.
		void BuildModifierTree();

		// PURPOSE
		//  rebuild modifier tree if it's invalid
		void BuildModifierTreeIfNeeded();

		// PURPOSE
		// Add a modifier override, from then on, any use of modifier "modifier" will use modOverride
		void AddModifierOverride(atHashString modifier, atHashString modOverride);

		// PURPOSE
		// Remove a modifier override.
		void RemoveModifierOverride(atHashString modifier);
		
		// PURPOSE
		// Clear all overrides
		void ClearModifierOverrides();		
		
		// PURPOSE
		//  searches for a specific modifier and returns its index 
		// PARAMS
		//  name - the name of the modifier we are searching for
		//  pSourceFile - optional argument to help locate the origin of the request
		// RETURNS
		//  the index of the modifier in the array (or -1 if it isn't found)
		int FindModifierIndex(const atHashString name, const char *label, bool noAssert=false) const;

		// PURPOSE
		//  searches for a specific modifier and returns it
		// PARAMS
		//  hash - the hash name of the modifier we are searching for
		//  pSourceFile - optional argument to help locate the origin of the request
		// RETURNS
		//  the index of the modifier in the array (or -1 if it isn't found)
		const tcModifier *FindModifier(const atHashString name, const char *label, bool noAssert=false) const;
		
		// PURPOSE
		//  Access a modifier via its index
		const tcModifier *GetModifier(int idx) const { return m_modifiersArray[idx]; }
		tcModifier *GetModifier(int idx) { return  m_modifiersArray[idx]; }

		const atHashString GetModifierKey(int idx) const { return m_modifiersArray[idx]->GetName(); }
		
#if __BANK
		// PURPOSE
		//  Access a modifier's name
		const char *GetModifierName(int idx) const { return m_modifiersArray[idx]->GetName().GetCStr(); }
		
		// PURPOSE
		//  Find a modifier's ModifierRecord index.
		int FindModifierRecordIndex(int idx) const;
		
		// PURPOSE
		// Set a modifier's name
		void SetModifierName(int idx, const char *name) 
		{			
			int mapIdx = m_modifiersLookup.GetIndex(GetModifierName(idx));
			m_modifiersLookup.GetKey(mapIdx)->SetFromString(name);
			m_modifiersArray[idx]->SetName(name);
			m_modifiersLookup.Unsort(); 
		}
#endif // __BANK

		// PURPOSE
		// Get the index of a modifier
		int GetModifierIndex(tcModifier* mod) const 
		{
			return m_modifiersArray.Find(mod);	
		}

		// PURPOSE
		// Get the numbers of loaded modifiers
		int GetModifiersCount() const { return m_modifiersArray.GetCount(); }

		// PURPOSE
		//  calculates the indices of the two time samples and the distance between them from the actual time
		// PARAMS
		//  hours - the time (hours) of the query
		//  minutes - the time (minutes) of the query
		//  seconds - the time (seconds) of the query
		//  timeSample1 - the returned time sample index that we are going from 
		//  timeSample2 - the returned time sample index that we are going towards 
		//  timeInterp - the returned time sample interpolation distance between the two time samples (0.0 -> 1.0)
		void CalcTimeSampleInfo(const u32 hours, const u32 minutes, const u32 seconds, u32 &timeSample1, u32 &timeSample2, float &timeInterp);

		// PURPOSE
		//  Prohibit/allow rebuild of the modifier box tree.  There are times in the frame where we want to query the tree
		//  outside the update thread.  We want to make sure no one's modifying the tree while we're doing this
		// PARAMS
		//  bProhibited - true to prohibit rebuild, false to allow
		void SetModifierBoxTreeRebuildProhibited(bool bProhibited);

	// functions (protected)
	protected: 

		// PURPOSE
		//  searches for a specific region and returns its index 
		// PARAMS
		//  hash - the hash name of the region we are searching for
		//  pSourceFile - optional argument to help locate the origin of the request
		// RETURNS
		//  the index of the region in the array (or -1 if it isn't found)
		int FindRegionIndex(const u32 hash, const char* pSourceFile=NULL) const;

		// PURPOSE
		//  queries the modifier box tree at a certain position for a list of boxes that enclose this point
		// PARAMS
		//  pos - the position of the modifier box tree query
		//  ppBoxes - passed variable that will hold the list of modifier boxes that enclose this point
		//            must be declared with a size of TIMECYCLE_MAX_MODIFIER_BOX_QUERY
		//  numFound - the number of modifier boxes that suffice the query is returned in this variable
		//  pStartNode - the node to start the query from (NULL specifies the root node)
		void QueryModifierBoxTree(Vec3V_In vPos, int *indices, int maxfound, int& numfound);

		// PURPOSE
		//  get access to the internal cycle data
		// PARAMS
		//  cycleIndex - the index of the cycle that we want access to
		tcCycle& GetCycle(u32 cycleIndex);

		// PURPOSE
		//  get access to the internal cycle data
		// PARAMS
		//  cycleHashName - the hash name of the cycle that we want access to
		int GetCycleIndex(u32 cycleHashName);

		// PURPOSE
		//  initialises the region data
		void InitRegionData();

		// PURPOSE
		//  calculates the strengths of each region at a specific position and time
		// PARAMS
		//  pos - the position of the query
		//  hours - the time (hours) of the query
		//  minutes - the time (minutes) of the query
		//  seconds - the time (seconds) of the query
		void CalcRegionStrengths(Vec3V_In vPos, const u32 hours, const u32 minutes, const u32 seconds);

		// PURPOSE
		//  initialises the modifier data
		// PARAMS
		//  isReload - whether or not this is being called from a reload
		void InitModifierData(const bool BANK_ONLY(isReload));

		// PURPOSE
		//  calculates the strengths of each modifier at a specific position and time
		// PARAMS
		//  pos - the position of the query
		//  hours - the time (hours) of the query
		//  minutes - the time (minutes) of the query
		//  seconds - the time (seconds) of the query
		void CalcModifierStrengths(Vec3V_In vPos, const u32 hours, const u32 minutes, const u32 seconds);

	// variables (protected)	
	protected: 
		atRangeArray<float,TIMECYCLE_MAX_REGIONS> m_regionStrengths;							// the calculated region strengths (calculated when Update() is called)
		atArray<float> m_modifierStrengths;														// the calculated modifier strengths (calculated when Update() is called)

		struct TCModOverride{
			atHashString originalMod;
			atHashString overrideMod;
		};
		
		atFixedArray<TCModOverride, 4> m_modOverrides;
		atArray<tcModifier*> m_modifiersArray;
		atBinaryMap<tcModifier*, atHashString> m_modifiersLookup;
		
		// debug
#if TC_DEBUG
		tcDebug	m_debug;																		// the debug interface 
		int m_modifierBoxAdded;
		int m_modifierBoxModified;
		int m_modifierBoxRemoved;
		int m_modifierBoxAddedMax;
		int m_modifierBoxModifiedMax;
		int m_modifierBoxRemovedMax;
#endif

		// cycle data
		tcCycle* m_cycles;

		atFixedArray<tcBox,TIMECYCLE_MAX_REGION_BOXES> m_regionBoxes;							// array of loaded region boxes
	
		atArray<tcBoxRemovalInfo> m_DeferredBoxRemovals;

		tcBoxTree m_modifierBoxTree;															// the modifier box tree
		bool m_modifierBoxTreeRebuildProhibited;
		bool m_modifierBoxTreeIsValid;

};


} // namespace rage


#endif // TC_MANAGER_H

