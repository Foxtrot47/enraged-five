//
// timecycle/tcdebug.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_DEBUG_H
#define TC_DEBUG_H

#define TC_DEBUG (__BANK && !__XENON)

#if TC_DEBUG
#	define TC_DEBUG_ONLY(...)  __VA_ARGS__
#else
#	define TC_DEBUG_ONLY(...)
#endif

#if TC_DEBUG

// includes (rage)
#include "vector/color32.h"
#include "atl/hashstring.h"
#include "atl/string.h"
#include "atl/bitset.h"
#include "grcore/stateblock.h"

// includes (framework)
#include "timecycle/tckeyframe.h"
#include "timecycle/tcconfig.h"

#define TIMECYCLE_MAX_MODIFIER_NAME_LEN				(32)

// namespaces
namespace rage {


// forward declarations
class bkBank;
class bkGroup;
class bkWidget;
class bkSlider;
class bkText;
class bkCombo;
class tcManager;
class ptxKeyframe;
struct ptxKeyframeDefn;

// structures

// PURPOSE
//  the 'state' of each variable in the modifier that is being edited
struct tcModifierVarEditState
{
	bool isActive;																	// flag to tell whether or not the modifier variable is active 

	float valA;																		// the first modifier value of this variables (modifiers can have two pieces of data associated with them at present)
	float valB;																		// the second modifier value of this variables
	
	Color32 colour;																	// the colour of the variable (some data is colour based)

	bkWidget* pWidgetActive[MODMENU_NUM];											// pointers to the 'active' widget on each menu
	bkWidget* pWidgetValA[MODMENU_NUM];												// pointers to the 'val a' widget on each menu
	bkWidget* pWidgetValB[MODMENU_NUM];												// pointers to the 'val b' widget on each menu
};

// PURPOSE
//  the current 'state' of the modifier that is being edited
struct tcModifierEditState
{
	atArray<tcModifierVarEditState>	var;											// array of info about each modifier variable's edit state
	atFixedBitSet<32,u32> userFlags;
	int fileIdx;
	char modifierName[TIMECYCLE_MAX_MODIFIER_NAME_LEN];
};


// PURPOSE
//  Used to keep a list of the modifiers as they are loaded, in the order they are loaded.
// Allow us to sort the binmap while keeping the list order dandy. 
struct tcModifierEditRecord
{
	atHashString name;
	int fileIdx;
};

struct tcModifierFileInfo
{
	atHashString name;
	u32 checksum;
};

// classes

// PURPOSE
//  - class that holds the core time cycle debugging info
//  - this provides the user with the ability to tweak data in the loaded cycles and modifiers
//    via widget editors
//  - a widget group is also created for game specific debug info that is accessible to the game code
class tcDebug
{
	// functions (public)
	public:

		// PURPOSE
		//  initialises the debug class
		// PARAMS
		//  pManager - a pointer to the time cycle manager that owns this class
		//  ppDebugRegionsNames - an array of names of the debug regions
		//  numDebugGroups - the number of debug groups that the game defines
		//                   these are the groups that each variable in the keyframe belongs to
		//  ppDebugGroupNames - an array of names of the debug groups
		void Init(tcManager* pManager, const tcRegionInfo* pRegionInfos, const int numDebugGroups, const char** ppDebugGroupNames, const int numUserFlags, const char ** ppUserFlagsNames);

		// PURPOSE
		//  shutdown the debug class
		void Shutdown();

		// PURPOSE
		//  update the debug class
		void Update();

		// PURPOSE
		//  renders time cycle debug info (e.g. the modifier boxes)
		void Render() const;

		// PURPOSE
		//  creates the debug widgets
		static void CreateWidgets();

		static void RefreshTCWidgets();

		static void UpdateModifierEditor();

		// PURPOSE
		//  gets whether the keyframe editor is active or not
		// RETURNS
		//  whether the keyframe editor is active or not
		bool GetKeyframeEditActive() const {return m_keyframeEditActive;}
		void SetKeyframeEditActive(bool bActive) {m_keyframeEditActive = bActive; KeyframeEditStateChanged();}

		// PURPOSE
		//  gets whether the game should override the light direction with the edit settings
		// RETURNS
		//  whether the game should override the light direction with the edit settings
		bool GetKeyframeEditOverrideLightDirection() const {return m_keyframeEditOverrideLightDirection;}

		// PURPOSE
		//  gets whether the game should override the time sample with the edit settings
		// RETURNS
		//  whether the game should override the time sample with the edit settings
		bool GetKeyframeEditOverrideTimeSample() const {return m_keyframeEditOverrideTimeSample;}

		// PURPOSE
		//  gets whether the game should override region with the edit settings
		// RETURNS
		//  whether the game should override the region with the edit settings
		bool GetKeyframeEditOverrideRegion() const {return m_keyframeEditOverrideRegion;}

		// PURPOSE
		//  gets whether the game should override the cycle index with the edit settings
		// RETURNS
		//  whether the game should override the cycle index with the edit settings
		bool GetKeyframeEditOverrideCycle() const {return m_keyframeEditOverrideCycle;}

		// PURPOSE
		//  gets the index of the time sample currently being edited
		// RETURNS
		//  the index of the time sample currently being edited
		int GetKeyframeEditTimeSampleIndex() const {return m_keyframeEditTimeSampleIndex;}

		// PURPOSE
		//  gets the index of the region currently being edited
		// RETURNS
		//  the index of the region currently being edited
		int GetKeyframeEditRegionIndex() const {return m_keyframeEditRegionIndex;}

		// PURPOSE
		//  gets the index of the cycle currently being edited
		// RETURNS
		//  the index of the cycle currently being edited
		int GetKeyframeEditCycleIndex() const {return m_keyframeEditCycleIndex;}

		// PURPOSE
		//  gets access to the keyframe that is currently being edited
		// RETURNS
		//  the keyframe that is currently being edited
		tcKeyframeUncompressed& GetKeyframeEditData	() const {return m_keyframeEditData;}

		// PURPOSE
		//  gets the id of the modifier currently being edited
		// RETURNS
		//  the id of the modifier currently being edited
		int GetModifierEditId() const;

		// PURPOSE
		//  gets whether or not the modifier being edited is being applied
		// RETURNS
		//  whether or not the modifier being edited is being applied
		bool GetModifierEditApply() const {return m_modifierEditApply;}

		// PURPOSE
		//  gets the strength to apply the currently edited modifier
		// RETURNS
		//  the strength to apply the currently edited modifier
		float GetModifierEditStrength() const {return m_modifierEditStrength;}

		// PURPOSE
		//  sets information about a loaded modifier file
		// PARAMS
		//  name - the name of the modifier file
		void SetModifierFileInfo(const char* name);

		// PURPOSE
		//  gets information about a loaded modifier file
		// PARAMS
		//  name - the name of the modifier file
		int GetModifierFileInfoIdx(const char* name);

		// PURPOSE
		//  resets the number of loaded modifier files
		void ResetNumModifierFiles() {m_modifierFilesInfo.Reset();}

		// PURPOSE
		//  gets the number of debug groups (each var in the keyframe belongs to a group)
		// RETURNS
		//  the number of debug groups
		int GetDebugGroupCount() const {return m_debugGroupNames.GetCount();}

		// PURPOSE 
		//  gets the name of a specific debug group
		// PARAMS
		//  the index of the debug group being queried
		// RETURNS
		//  the name of the queried debug group
		const char* GetDebugGroupName(const int index) const {return m_debugGroupNames[index];}

		// PURPOSE
		//  gives access to the 'game' widget to the game code so that it can add its own debug info
		// RETURNS
		//  a pointer to the game widget
		bkGroup* GetGameGroup() const {return m_pGameGroup;}

		// PURPOSE
		//  Add a modifier record
		static void AddModifierRecord(atHashString name, int fileIdx);
		static void RemoveModifierRecord(atHashString name, int fileIdx);
		
		int FindModifierRecordIndex(atHashString name) const;
		
	// functions (private)
	private:

		// PURPOSE
		//  initialises the debug widgets
		void InitWidgets();

		// PURPOSE
		//  initialises the cycle editor widgets
		static void InitCycleEditorWidgets();

		// PURPOSE
		//  initialises the modifier editor widgets
		static void InitModifierEditorWidgets();

		// PURPOSE
		//  update the modifier editor widgets.
		static void UpdateModifierEditorWidgets();

		// PURPOSE
		//  callback for when the keyframe edit state is changed
		//  when active the time/cycle keyframe will be copied into the edit state
		static void KeyframeEditStateChanged();


		// PURPOSE
		//  callback for when the graph view variables are changed
		static void ChangeGraphs();
		
		// PURPOSE
		//  update graph views when variables have been edited
		static void UpdateGraphs();

		// PURPOSE
		// collect variable names for the current graph view group
		static void GetGraphVars();
		
		// PURPOSE
		// find the entry closest to the given time cycle time
		static int FindClosestEntry(ptxKeyframe * graph, int timeIndex);
		
		// PURPOSE
		// realign readjust entries to the match time cycle times
		static bool RealignEntry(ptxKeyframe * graph, int timeIndex, const Vec4V& data);

		// PURPOSE
		// update the time cycle values if the user has edited the keyframe graph
		static void ProcessRemoteGraphChange();

		// PURPOSE
		// flag that the graph variables have changed or need updating
		static void RequestFloatGraphUpdate();
		static void RequestColorGraphUpdate();
		static void RequestGraphChange();
		static void RequestRemoteFloatGraphChanged();
		static void RequestRemoteColorGraphChanged();
		
		// PURPOSE
		// get/set a variable, or set of variables from the timecycle to we can set the keyframe element
		static void GetTimeCycleFloatValues(int timeIndex, ScalarV& time, Vec4V& data);
		static void GetTimeCycleColorValues(int timeIndex, ScalarV& time, Vec4V& data);
		static void SetTimeCycleFloatValues(int timeIndex, const Vec4V& data);
		static void SetTimeCycleColorValues(int timeIndex, const Vec4V& data);

		// PURPOSE
		// remove entries in the keyframe that are not timecycle times.
		static bool CleanGraph(ptxKeyframe * graph);

		// PURPOSE
		//  callback for copying the currently edited keyframe into the copy keyframe
		static void KeyframeCopyData();

		// PURPOSE
		//  callback for pasting the current copied keyframe into the currently edited keyframe
		static void KeyframePasteData();

		// PURPOSE
		//  callback for pasting the selected group vars from the current copied keyframe into the currently edited keyframe
		static void KeyframePasteGroup();

		// PURPOSE
		//  callback that will store the currently edited keyframe into the cycle
		static void KeyframeStoreData();

		// PURPOSE
		//  callback for reloading the keyframe data back in
		static void KeyframeReloadData();

		// PURPOSE
		//  callback for saving the current keyframe table data to file
		static void KeyframeSaveData();

		// PURPOSE
		//  adds a modifier variable data to a modifier menu
		// PARAMS
		//  menuId - the id of the menu to add this modifier variable to
		//  varId - the id of the keyframe variable to add to the modifier menu
		static void ModifierAddVarToMenu(const tcModifierMenu_e menuId, const int varId);

		// PURPOSE
		//  initialises the modifier variables with default values
		static void ModifierInit();

		// PURPOSE
		//  initialises the editeable modifier variables with values from the edited modifier
		static void ModifierRecordEditIdChanged();

		// PURPOSE
		//  callback that updates the modifier widgets when a modifier gets changed
		static void ModifierChanged();

		// PURPOSE
		//  callback that updates the modifier widget and data when a modifier name gets changed
		static void ModifierNameChanged();

		// PURPOSE
		//  callback that is called when a modifier variable changes its active state
		static void ModifierActiveToggled();

		// PURPOSE
		//  callback that is called when a modifier variable's colour is changed
		static void ModifierColChanged();

		// PURPOSE
		//  callback that is called when a modifier variable's colour multiplier is changed
		static void ModifierColMultChanged();

		// PURPOSE
		//  copies a modifier state into another
		// PARAMS
		//  src - the modifier edit state to copy from 
		//  dst - the modifier edit state to copy to
		static void ModifierCopyState(const tcModifierEditState& src, tcModifierEditState& dst);

		// PURPOSE
		//  callback for adding a new modifier
		static void ModifierAdd();

		// PURPOSE
		//  callback for copying the edit keyframe into the copy keyframe
		static void ModifierCopy();

		// PURPOSE
		//  callback for pasting the copy keyframe into the edit keyframe
		static void ModifierPaste();

		// PURPOSE
		//  callback for reloading all modifier data from the files previously loaded
		static void ModifierReloadData();

		// PURPOSE
		//  callback for saving all the modifier data to the files they were previously loaded from
		static void ModifierSaveAll();

		// PURPOSE
		//  callback for asking the user if they are sure they want to save thier modifier data before
		//  actually saving it
		static void ModifierSaveAllCheck();
		
		// PURPOSE
		//  callback for asking the user if they are sure they want to save thier modifier data before
		//  actually saving it
		static void ModifierSaveAllCopy();
		

		// PURPOSE
		//	Allows the user to select the next closest/farthest modifier box based on proximity to the camera
		static void SelectClosestModifierBox();
		static void SelectNextClosestModifierBox();
		static void SelectNextFurthestModifierBox();

		// PURPOSE
		//  this causes everything to be loaded back in before writing out the current edit modifier data
		static void ModifierReloadDataSafeCurrent();

		// PURPOSE
		//  saves out a single modifier file 
		// PARAMS
		//  filename - the name of the file to save to
		static void ModifierSaveFile(int fileIdx, bool copy = false);

		// PURPOSE
		//  Calculate the checksum of a file.
		// PARAMS
		//  filename - the name of the file
		static u32 CalculateFileContentChecksum(const char* filename);

	// variables (private)
	private: 

		// owner class
		static tcManager* m_pManager;															// pointer to the time cycle manager that owns us

		// keyframe editor
		static bool m_keyframeEditActive;														// whether or not the keyframe editor is active
		static bool m_keyframeEditOverrideLightDirection;										// whether the game should override the light direction with the edit values
		static bool m_keyframeEditOverrideTimeSample;											// whether the game should override the time sample with the edit values
		static bool m_keyframeEditOverrideRegion;												// whether the game should override the time sample with the edit values
		static bool m_keyframeEditOverrideCycle;												// whether the game should override the cycle with the edit values
		static u32 m_keyframeEditCycleIndex;													// the cycle index currently being edited by the keyframe editor
		static int m_keyframeEditRegionIndex;													// the region index currently being edited by the keyframe editor
		static int m_keyframeEditTimeSampleIndex;												// the time sample index currently being edited by the keyframe editor
		static tcKeyframeUncompressed m_keyframeEditData;													// the edited keyframe data
		static bool m_keyframeCopyDataValid;													// flag to tell if the keyframe copy data is valid or not
		static tcKeyframeUncompressed m_keyframeCopyData;													// the copied keyframe data
		static int m_keyframeCurrPasteGroup;													// the index of the currently selected paste group combo box

		// modifier editor
		static int m_modifierRecordEditId;														// id of the modifier being edited
		static int m_modifierBoxEditId;
		static Vector3 m_cameraPosition;
		static bool m_modifierEditApply;														// whether or not the edited modifier should be applied
		static float m_modifierEditStrength;													// the strength with which to apply the edited modifier
		static float m_modifierEditScale;
		static float m_modifierEditRange;
		static float m_modifierPrevScale;
		static tcModifierEditState m_modifierEditState;											// the state of the modifier being edited
		static bool m_modifierCopyStateValid;													// flag to tell if the modifier copy state is valid or not
		static tcModifierEditState m_modifierCopyState;											// the state of the copied modifier
		static atArray<tcModifierEditRecord> m_modifiersInfo;
		
		// graph view
		static int m_GraphFloatVarId[4];
		static int m_GraphColorVarId;
		static int m_GraphFloatGroup;
		static int m_GraphColorGroup;
		static int m_GraphLastFloatGroup;
		static int m_GraphLastColorGroup;
		static bool m_NeedGraphChange;
		enum {kGraphFloatView=0x1, kGraphColorView=0x2};
		static u32 m_NeedGraphUpdate;
		static u32 m_RemoteGraphChanged;
		static int m_DrawColorType;

		// General
		static bool m_noDepthTest;
		static bool m_solidBoxes;
		static u8 m_boxesAlpha;
		
		// modifier debug
		static bool m_renderModifierBoxes;														// whether or not to render the modifier boxes
		static bool m_renderModifierNames;														// whether or not to render the modified name
		static bool m_renderModifierBoxNames;													// whether or not to render the modifier box names
		static bool m_renderSelectedOnly;

		// region debug
		static bool m_renderRegionBoxes;														// whether or not to render the region boxes

		// renderstate
		static grcDepthStencilStateHandle m_DSS;

		// file info
		static atArray<tcModifierFileInfo> m_modifierFilesInfo;
		
		// debug groups
		static atArray<const char*>	m_debugGroupNames;											// the names of the groups that the variables in the keyframe belong to

		// debug regions
		static atArray<const char*>	m_debugRegionNames;											// the names of the different regions in each cycle
	
		// debug groups
		static atArray<const char*>	m_debugFloatNames;											// the names of the float variables
		static atArray<const char*>	m_debugColorNames;											// the names of the color variables
		static atArray<int>	m_debugFloatVarIds;													// list of the float variable indexes
		static atArray<int>	m_debugColorVarIds;													// list of the Color variable indexes
		static ptxKeyframe *m_debugFloatGraph;
		static ptxKeyframe *m_debugColorGraph;
		static ptxKeyframeDefn *m_debugFloatGraphDefn;
		static ptxKeyframeDefn * m_debugColorGraphDefn;

		// widget data
		static bkBank* m_pBank;																	// pointer to the time cycle bank widget
		static bkGroup* m_pEditModDataGroup;													// pointer to the modifier editor bank group
		static bkGroup* m_pGameGroup;															// pointer to the game bank group
		static bkGroup* m_pCycleEditorGroup;
		static bkGroup* m_pGraphViewGroup;
		static bkText* m_pModifierNameWidget;
		static bkSlider* m_pModifierSliderWidget;
		static bkCombo* m_pModifierComboWidget;
		static bkCombo* m_pModifierFileWidget;
		static atArray<const char*>	m_userFlagsNameList;

};

} // namespace rage



#endif // TC_DEBUG

#endif // TC_DEBUG_H

