//
// timecycle/optimisations.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef TIMECYCLE_OPTIMISATIONS_H
#define TIMECYCLE_OPTIMISATIONS_H

#define TIMECYCLE_OPTIMISATIONS_OFF		0

#if TIMECYCLE_OPTIMISATIONS_OFF
#define TIMECYCLE_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define TIMECYCLE_OPTIMISATIONS()
#endif	

#endif // !TIMECYCLE_OPTIMISATIONS_H
