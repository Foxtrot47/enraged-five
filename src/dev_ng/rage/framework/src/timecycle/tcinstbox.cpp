//
// timecycle/tcinstbox.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#include "timecycle/tcinstbox.h"
#include "timecycle/tcinstbox_parser.h"

#include "parser/macros.h"
#include "parser/manager.h"
#include "timecycle/tcbox.h"
#include "vector/colors.h"
#include "grcore/debugdraw.h"

namespace rage {

tcInstBoxMgr g_tcInstBoxMgr;

float tcInstBoxSearchResult::CalcStrengthMult(Vec3V_In vPos, const int hours, const int minutes, const int seconds) const 
{
	float recipRange = m_pType->GetRecipRange();
	int startHour = m_pType->GetStartHour();
	int endHour = m_pType->GetEndHour();
	
	const ScalarV positionalStrengthMult = tcBox::CalcPosStrengthMult(vPos, m_aabb, recipRange, false);
	const float timeStrengthMult = tcBox::CalcTimeStrengthMult(startHour, endHour, hours, minutes, seconds);

	return  positionalStrengthMult.Getf() * timeStrengthMult;
}

void tcInstBoxInstLoader::LoadData()
{
	m_boxInstances.ResetCount();
	Verifyf(PARSER.LoadObject(TCINSTBOX_INST_DATA_FILE, "meta", *this), "Failed to load TC instance box data %s", TCINSTBOX_INST_DATA_FILE);
	Assert16(&m_boxInstances[0]);
}

void tcInstBoxInstLoader::SaveData()
{
#if __BANK
	 Verifyf(PARSER.SaveObject(TCINSTBOX_INST_DATA_FILE, "meta", this, parManager::XML), "Failed to save TC instance box data %s", TCINSTBOX_INST_DATA_FILE);
#endif
}

//
// load the type data for instanced tc boxes
//
void tcInstBoxTypeLoader::LoadData()
{
	m_boxTypes.ResetCount();
	Verifyf(PARSER.LoadObject(TCINSTBOX_TYPE_DATA_FILE, "meta", *this), "Failed to load TC instance box data %s", TCINSTBOX_TYPE_DATA_FILE);
	PostLoad();
}

void tcInstBoxTypeLoader::SaveData()
{
#if __BANK
	Verifyf(PARSER.SaveObject(TCINSTBOX_TYPE_DATA_FILE, "meta", this, parManager::XML), "Failed to save instanced timecycle box type data");
#endif
}

void tcInstBoxTypeLoader::PostLoad()
{
	m_typeMap.Reset();

	// build up a map of type name hashes to indices
	for (s32 i=0; i<m_boxTypes.GetCount(); i++)
	{
		tcInstBoxType& boxType = m_boxTypes[i];
#if __ASSERT
		Assertf( m_typeMap.Access(boxType.GetName().GetHash())==NULL, "Duplicate instanced timecycle box type (%s) in %s", boxType.GetName().GetCStr(), TCINSTBOX_TYPE_DATA_FILE);		
#endif	//__ASSERT
		m_typeMap.Insert(boxType.GetName().GetHash(), i);

		// Might as well get these ready.
		boxType.m_recipRange = 1.0f / boxType.m_range;
		boxType.m_percentage *= 0.01f;
	}
}

//
// loads all type and instance data, builds tree, sets everything up for use
//
void tcInstBoxMgr::Init()
{
	m_typeStore.LoadData();
	m_instStore.LoadData();
	BuildTree();
}

//
// destroy the tree
//
void tcInstBoxMgr::Shutdown()
{
	if (m_pTree)
	{
		delete m_pTree;
		m_pTree = NULL;
	}
}


//
// construct mnbvh tree for queries
//
void tcInstBoxMgr::BuildTree()
{
	if (m_pTree)
	{
		delete m_pTree;
		m_pTree = NULL;
	}

	// construct temp array of world-space aabbs for constructing mnbvh
	atArray<spdAABB> tempBoxList;
	tempBoxList.Reserve(m_instStore.m_boxInstances.GetCount());
	tcInstBoxSearchResult result;
	for (s32 i=0; i<m_instStore.m_boxInstances.GetCount(); i++)
	{
		GetInstData(i, result);
		spdAABB box = result.m_aabb;
		box.GrowUniform(ScalarV(result.m_pType->GetRange()));
		tempBoxList.Append() = box;
	}

	// construct MNBVH
	Assert(!m_pTree);
	m_pTree = rage_new MNBVHSimpleInterface(tempBoxList);
}

//
// return array of intersecting instances
//
void tcInstBoxMgr::Search(const spdAABB& aabb, atArray<tcInstBoxSearchResult>& results)
{
	results.ResetCount();

	void* tempResults[TCINSTBOX_MAX_SEARCH_RESULTS];
	int numPrelimResults = m_pTree->PerformBoxSearch(aabb, tempResults, TCINSTBOX_MAX_SEARCH_RESULTS);
	Assertf(TCINSTBOX_MAX_SEARCH_RESULTS-numPrelimResults>5, "tcInstBoxMgr: code need to increase TCINSTBOX_MAX_SEARCH_RESULTS" );
	for (s32 i=0; i<numPrelimResults; i++)
	{
		const s32 instIndex = m_pTree->GetSearchResult(tempResults, i);
		tcInstBoxSearchResult& result = results.Grow();
		GetInstData(instIndex, result);
	}
}

//////////////////////////////////////////////////////////////////////////
// DEBUG WIDGETS

#if __BANK



static bool s_displayInfo = false;
static bool s_drawBoxes = false;

void tcInstBoxMgr::InitWidgets(bkBank* pBank)
{
	if (pBank)
	{
		pBank->PushGroup("Instanced TC Boxes");
		{
			pBank->AddToggle("Display num instances", &s_displayInfo);
			pBank->AddToggle("Draw boxes", &s_drawBoxes);
		}
		pBank->PopGroup();
	}
}

void tcInstBoxMgr::UpdateDebug(Vec3V_In vCamPos)
{
	if (s_displayInfo)
	{
		grcDebugDraw::AddDebugOutput("tcInstBoxMgr: %d types, %d instances", m_typeStore.m_boxTypes.GetCount(), m_instStore.m_boxInstances.GetCount());

		atArray<tcInstBoxSearchResult> searchResults;
		spdSphere tempSphere(vCamPos, ScalarV(0.2f));
		Search( spdAABB(tempSphere), searchResults);
		for (s32 i=0; i<searchResults.GetCount(); i++)
		{
			grcDebugDraw::AddDebugOutput("tcInstBoxMgr: camera is inside %s", searchResults[i].m_pType->GetName().GetCStr() );
			grcDebugDraw::AddDebugOutput("... percentage:%.2f, range:%.2f, startHour:%d, endHour:%d, modifier:%d",
				searchResults[i].m_pType->GetPercentage(),
				searchResults[i].m_pType->GetRange(),
				searchResults[i].m_pType->GetStartHour(),
				searchResults[i].m_pType->GetEndHour(),
				searchResults[i].m_pType->GetModifier().GetCStr()
			);
		}
	}
	if (s_drawBoxes)
	{
		tcInstBoxSearchResult result;
		for (s32 i=0; i<m_instStore.m_boxInstances.GetCount(); i++)
		{
			GetInstData(i, result);

			const Vec3V vMin = result.m_aabb.GetMin();
			const Vec3V vMax = result.m_aabb.GetMax();

			const bool bCameraIsInside = result.m_aabb.ContainsPoint(vCamPos);
			Color32 boxColor = bCameraIsInside ? Color_cyan : Color_red;
			grcDebugDraw::BoxAxisAligned(vMin, vMax, boxColor, false, 1);

			if (bCameraIsInside)
			{
				const Vec3V vTextPos(vMin.GetX(), vMax.GetY(), vMax.GetZ());
				grcDebugDraw::Text( vTextPos, boxColor, result.m_pType->GetName().GetCStr() );
			}
		}
	}
}

#endif	//__BANK

//////////////////////////////////////////////////////////////////////////

} //namespace rage
