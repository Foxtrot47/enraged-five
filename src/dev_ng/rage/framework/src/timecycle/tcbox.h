//
// timecycle/tcbox.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_BOX_H
#define TC_BOX_H


// includes (rage)
#include "spatialdata/aabb.h"
#include "fwtl/Pool.h"

// includes (framework)


// namespaces 
namespace rage {



// classes

// PURPOSE
//  - holds information about a time cycle box
//  - these boxes can be placed around the game world and as the camera goes near or inside one it 
//    will cause the current time cycle keyframe to alter (either by a modifier or a region)
class tcBox
{		
	// functions (public)
	public:
		FW_REGISTER_CLASS_POOL(tcBox);

		// PURPOSE
		//  calculates the strength multiplier of this box based on a position and time
		// PARAMS
		//  pos - the position at which to query this box's strength multiplier (usually the current camera position)
		//  hours - the number of hours at which to query this box's strength multiplier
		//  minutes - the number of minutes at which to query this box's strength multiplier 
		//  seconds - the number of seconds at which to query this box's strength multiplier
		// RETURNS
		//  the strength multiplier of this box
		float CalcStrengthMult(Vec3V_In vPos, const int hours, const int minutes, const int seconds) const;

		// PURPOSE
		//  sets this box's bounding box volume
		// PARAMS
		//  box - the bounding box 
		void SetBoundingBox(const spdAABB& box) {m_bBox = box;}

		// PURPOSE
		//  gets this box's bounding box volume
		// RETURNS
		//  a constant reference to this box's bounding box
		const spdAABB& GetBoundingBox() const {return m_bBox;}

		// PURPOSE
		//  sets this box's index
		// PARAMS
		//  index - the index of this box
		void SetIndex(const int index) {m_index = index;}

		// PURPOSE
		//  gets this box's index
		// RETURNS
		//  the index of this box
		int GetIndex() const {return m_index;}

		// PURPOSE
		//  sets this box's strength
		// PARAMS
		//  strength - the maximum strength to apply with this box's modifier or region
		void SetStrength(const float strength) {m_strength = strength;}

		// PURPOSE
		//  gets this box's strength
		// RETURNS
		//  the maximum strength to apply with this box's modifier or region
		float GetStrength() const {return m_strength;}

		// PURPOSE
		//  sets this box's range
		// PARAMS
		//  range - the range of this box (the distance over which this box strength fades in)
		void SetRange(const float range) {m_range = range; m_rangeReciprocal = range == 0.0f ? 1.0f : 1.0f / range;}

		// PURPOSE
		//  gets this box's range
		// RETURNS
		//  the range of this box (the distance over which this box strength fades in)
		float GetRange() const {return m_range;}

		// PURPOSE
		//  sets the time at which this box becomes active
		// PARAMS
		//  startHour - the hour at which this box becomes active
		void SetStartHour(const int startHour) {m_startHour = startHour;}

		// PURPOSE
		//  gets the time at which this box becomes active
		// RETURNS
		//  the hour at which this box becomes active
		int GetStartHour() const {return m_startHour;}

		// PURPOSE
		//  sets the time at which this box becomes inactive
		// PARAMS
		//  endHour - the hour at which this box becomes inactive
		void SetEndHour(const int endHour) {m_endHour = endHour;}

		// PURPOSE
		//  gets the time at which this box becomes inactive
		// RETURNS
		//  the hour at which this box becomes inactive
		int GetEndHour() const {return m_endHour;}

		// PURPOSE
		//  sets the ipl id that created this box
		// PARAMS
		//  iplId - the id of the ipl that this box was created from
		void SetIplId(const int iplId) {m_iplId = iplId;}

		// PURPOSE
		//  gets the ipl id that created this box
		// RETURNS
		//  the id of the ipl that this box was created from
		int GetIplId() const {return m_iplId;}


		// PURPOSE
		//  sets the "treat as sphere" setting
		// PARAMS
		//  treatAsSphere - the "treat as sphere" setting (true/false).
		void SetTreatAsSphere(const bool treatAsSphere) {m_treatAsSphere = treatAsSphere;}

		// PURPOSE
		//  gets the "treat as sphere" setting
		// RETURNS
		//  the "treat as sphere" setting (true/false).
		bool GetTreatAsSphere() const {return m_treatAsSphere;}

		// PURPOSE
		//  sets if this box came from an interior.
		// PARAMS
		//  iplId - the id of the ipl that this box was created from
		void SetIsInterior(const bool interior) {m_isInterior = interior;}

		// PURPOSE
		//  gets interior state
		// RETURNS
		//  the id of the ipl that this box was created from
		bool GetIsInterior() const {return m_isInterior;}

#if __BANK
		void SetDebugFlags(int debugFlag) { m_bBox.SetUserInt1(debugFlag); }
		int GetDebugFlags() const { return m_bBox.GetUserInt1(); }
#endif // __BANK

		// PURPOSE
		//  helper function for CalcStrengthMult that calculates the strength based on position
		// PARAMS
		//  pos - the position at which to calculate the strength of this box
		// RETURNS
		//  the strength to apply
		static inline ScalarV_Out CalcPosStrengthMult(Vec3V_In vPos, const spdAABB &box, float rangeReciprocal, bool treatAsSphere);

		// PURPOSE
		//  helper function for CalcStrengthMult that calculates the strength based on time
		// PARAMS
		//  hours - the number of hours at which to calculate this box's strength multiplier
		//  minutes - the number of minutes at which to calculate this box's strength multiplier 
		//  seconds - the number of seconds at which to calculate this box's strength multiplier
		// RETURNS
		//  the strength to apply
		static inline float CalcTimeStrengthMult(const int startHour, const int endHour, const int hours, const int minutes, const int seconds);


	// variables (private)
	private:
		spdAABB m_bBox;		// the bounding box of this box
		int	m_index;			// the index into the parent list (of regions or modifiers) that this box refers to
		float m_strength;		// the strength of the box
		float m_range;			// the range of the box - the box strength will fade in during this range and be applied fully when inside the box
		float m_rangeReciprocal;// the reciprocal of m_range - pre-calculated to avoid a floating-point divide every time CalcPosStrengthMult() is called.
		int	m_startHour;		// the time (in hours) at which this box becomes active
		int	m_endHour;			// the time (in hours) at which this box becomes inactive
		int	m_iplId;			// the id of the ipl that has created this box
		bool m_treatAsSphere;	// whether or not this timecycle modifier box should be treated as a sphere for strength calculation purposes, etc.
		bool m_isInterior;		// whther or not this box came from an interior.

};


inline ScalarV_Out tcBox::CalcPosStrengthMult(Vec3V_In vPos, const spdAABB &box, float rangeReciprocal, bool treatAsSphere)
{
	ScalarV distanceToPoint(V_ZERO);

	if(treatAsSphere)
	{
		const Vec3V centre = box.GetCenter();
		const Vec3V extent = box.GetExtent();

		const u32 U32_SQRT_1_OVER_3 = 0x3F13CD3A; // sqrtf(1.0f/3.0f)
		const Vec3V radiusVector(extent.GetX()*ScalarVConstant<U32_SQRT_1_OVER_3>());

#if defined(BOX_STRENGTH_ZDISTMULT_V)
		const Vec3V   v      = Max(Vec3V(V_ZERO), Abs(vPos - centre) - radiusVector);
		const Vec3V   vscale = Vec3V(Vec2V(V_ONE), BOX_STRENGTH_ZDISTMULT_V); // {1,1,3}, if vscale was 1 we could just call m_bBox.DistanceToPoint(vPos)
		distanceToPoint = Mag(v*vscale);
#else
		const Vec3V   v	= Max(Vec3V(V_ZERO), Abs(vPos - centre) - radiusVector);
		distanceToPoint = Mag(v);
#endif
	}
	else
	{
#if defined(BOX_STRENGTH_ZDISTMULT_V)
		const Vec3V   centre = box.GetCenter();
		const Vec3V   extent = box.GetExtent();
		const Vec3V   v      = Max(Vec3V(V_ZERO), Abs(vPos - centre) - extent);
		const Vec3V   vscale = Vec3V(Vec2V(V_ONE), BOX_STRENGTH_ZDISTMULT_V); // {1,1,3}, if vscale was 1 we could just call m_bBox.DistanceToPoint(vPos)
		distanceToPoint = Mag(v*vscale);
#else
		distanceToPoint = box.DistanceToPoint(vPos);
#endif
	}

	return SubtractScaled(ScalarV(V_ONE), ScalarV(rangeReciprocal), distanceToPoint);
}

inline float tcBox::CalcTimeStrengthMult(const int startHour, const int endHour, const int hours, const int minutes, const int seconds)
{
	// if the start and end hour are the same then the time aren't being used - return full strength
	if (startHour == endHour)
	{
		return 1.0f;
	}

	// if we're in the hour before the start hour then fade in
	if (((hours+1)%24) == startHour)
	{		
		// interpolate up to start hour
		float t = (minutes*60 + seconds) / (60.0f*60.0f);
		Assertf((t>=-0.001f && t<=1.001f), "time strength multiplier out of range t:%f, %d:%d:%d", t, hours, minutes, seconds);
		return t;
	}
	// if we're in the end hour then fade out
	else if (hours == endHour)
	{		
		// interpolate down after end hour
		float t = 1.0f - ((minutes*60 + seconds) / (60.0f*60.0f));
		Assertf((t >= -0.001f && t <= 1.001f), "time strength multiplier out of range t:%f, %d:%d:%d", t, hours, minutes, seconds);
		return t;
	}
	// if the start hour is less that the end hour 
	else if (startHour < endHour)
	{
		// then return full strength if the time is between the two
		if (hours >= startHour && hours <= endHour)
		{
			return 1.0f;
		}
		else
		{
			// otherwise return zero strength
			return 0.0f;
		}
	}
	// the start hour is greater than the end hour - return zero strength if out of range
	else if (hours >= endHour && hours < startHour)
	{
		return 0.0f;
	}
	// we must be in range where the start hour is greater than the end hour
	else
	{
		return 1.0f;
	}
}

} // namespace rage


#endif // TC_BOX_H






