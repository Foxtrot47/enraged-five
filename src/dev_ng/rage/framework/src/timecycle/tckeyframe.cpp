//
// timecycle/tckeyframe.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (rage)
#include "bank/bank.h"
#include "math/float16.h"

// includes (framework)
#include "fwutil/xmacro.h"
#include "timecycle/channel.h"
#include "timecycle/tcconfig.h"
#include "timecycle/tckeyframe.h"
#include "timecycle/optimisations.h"

TIMECYCLE_OPTIMISATIONS()

#define TIMECYCLE_VECTORISED (1)

namespace rage {

tcKeyframeUncompressed::tcKeyframeUncompressed()
{
}

__COMMENT(explicit) tcKeyframeUncompressed::tcKeyframeUncompressed(const tcKeyframe& src)
{
#if TIMECYCLE_VECTORISED

	Vec::V4Float16StreamUnpack((Vec::Vector_4V*)GetStreamAddr(), (const Vec::Vector_4V*)src.GetStreamAddr(), (TIMECYCLE_VAR_COUNT + 7)&~7);

#else // not TIMECYCLE_VECTORISED

	for (int i = 0; i < TIMECYCLE_VAR_COUNT; i++)
	{
		SetVar(i, src.GetVarUncompressed_INIT_OR_DEBUG_ONLY(i));
	}

#endif // not TIMECYCLE_VECTORISED
}

void tcKeyframeUncompressed::SetDefault()
{
	const int numVars   = tcConfig::GetNumVars();
	const int arraySize = (numVars + 7)&~7;

	Assertf(numVars == TIMECYCLE_VAR_COUNT, "tcConfig::GetNumVars() must match TIMECYCLE_VAR_COUNT");

	for (int i = 0; i < numVars; i++)
	{
		SetVar(i, tcConfig::GetVarInfo(i).defaultValue);
	}

	for (int i = numVars; i < arraySize; i++)
	{
		SetVar(i, 0.0f);
	}
}

void tcKeyframeUncompressed::InterpolateKeyframeBlend(
	const tcKeyframeUncompressed& keyframe1,
	float t
	)
{
	InterpolateKeyframe(*this, keyframe1, t);
}

void tcKeyframeUncompressed::InterpolateKeyframe(
	const tcKeyframeUncompressed& keyframe0,
	const tcKeyframeUncompressed& keyframe1,
	float t
	)
{
#if TIMECYCLE_VECTORISED

	Vec::Vector_4V* RESTRICT dst = (Vec::Vector_4V*)GetStreamAddr();

	const Vec::Vector_4V* RESTRICT src0 = (const Vec::Vector_4V*)keyframe0.GetStreamAddr();
	const Vec::Vector_4V* RESTRICT src1 = (const Vec::Vector_4V*)keyframe1.GetStreamAddr();

	const Vec::Vector_4V tv = Vec::V4LoadScalar32IntoSplatted(t);

	for (int i = 0; i < TIMECYCLE_VAR_COUNT; i += 8)
	{
		Vec::Vector_4V a0, b0, a1, b1;

		a0 = *(src0++);
		b0 = *(src0++);
		a1 = *(src1++);
		b1 = *(src1++);

		*(dst++) = Vec::V4Lerp(tv, a0, a1);
		*(dst++) = Vec::V4Lerp(tv, b0, b1);
	}

#else // not TIMECYCLE_VECTORISED

	for (int i = 0; i < TIMECYCLE_VAR_COUNT; i++)
	{
		const float value0 = keyframe0.GetVar(i);
		const float value1 = keyframe1.GetVar(i);

		SetVar(i, value0 + (value1 - value0)*t);
	}

#endif // not TIMECYCLE_VECTORISED
}

void tcKeyframeUncompressed::InterpolateKeyframe(
	const tcKeyframe& keyframe0,
	const tcKeyframe& keyframe1,
	float t // interpolate between [keyframe0..keyframe1]
	)
{
	// uncompress and interpolate separately .. this is only called from the water i think
	InterpolateKeyframe(
		tcKeyframeUncompressed(keyframe0),
		tcKeyframeUncompressed(keyframe1),
		t
		);
}

void tcKeyframeUncompressed::InterpolateKeyframe(
	const tcKeyframe& keyframe00,
	const tcKeyframe& keyframe10,
	const tcKeyframe& keyframe01,
	const tcKeyframe& keyframe11,
	float tx, // interpolate between [keyframe00..keyframe10], [keyframe01..keyframe11]
	float ty  // interpolate between [keyframe00..keyframe01], [keyframe10..keyframe11]
	)
{
#if TIMECYCLE_VECTORISED
	// [SSE TODO] -- make use of _mm256_cvtph_ps, see Vec::V4Float16StreamUnpack
	//Assert((((uptr)keyframe00.GetStreamAddr())&0x1f) == 0);
	//Assert((((uptr)keyframe10.GetStreamAddr())&0x1f) == 0);
	//Assert((((uptr)keyframe01.GetStreamAddr())&0x1f) == 0);
	//Assert((((uptr)keyframe11.GetStreamAddr())&0x1f) == 0);
	//Assert((((uptr)GetStreamAddr())&0x1f) == 0);

	#define ADDR_INCR(v,i) v[i]
	#define ADDR_SKIP(v,i) v += (i)

	Vec::Vector_4V* RESTRICT dstV = (Vec::Vector_4V*)GetStreamAddr();

	const Vec::Vector_4V* RESTRICT srcV_00 = (const Vec::Vector_4V*)keyframe00.GetStreamAddr();
	const Vec::Vector_4V* RESTRICT srcV_10 = (const Vec::Vector_4V*)keyframe10.GetStreamAddr();
	const Vec::Vector_4V* RESTRICT srcV_01 = (const Vec::Vector_4V*)keyframe01.GetStreamAddr();
	const Vec::Vector_4V* RESTRICT srcV_11 = (const Vec::Vector_4V*)keyframe11.GetStreamAddr();

	const Vec::Vector_4V tvx = Vec::V4LoadScalar32IntoSplatted(tx);
	const Vec::Vector_4V tvy = Vec::V4LoadScalar32IntoSplatted(ty);

	const int count = ((TIMECYCLE_VAR_COUNT + 7)&~7)/8;
	int i = 0;

	for (const int block4 = count - 4; i <= block4; i += 4)
	{
		Vec::Vector_4V a0_00, b0_00, a0_10, b0_10, a0_01, b0_01, a0_11, b0_11;
		Vec::Vector_4V a1_00, b1_00, a1_10, b1_10, a1_01, b1_01, a1_11, b1_11;
		Vec::Vector_4V a2_00, b2_00, a2_10, b2_10, a2_01, b2_01, a2_11, b2_11;
		Vec::Vector_4V a3_00, b3_00, a3_10, b3_10, a3_01, b3_01, a3_11, b3_11;

		Vec::V4Float16Vec8Unpack(a0_00, b0_00, ADDR_INCR(srcV_00,0));
		Vec::V4Float16Vec8Unpack(a1_00, b1_00, ADDR_INCR(srcV_00,1));
		Vec::V4Float16Vec8Unpack(a2_00, b2_00, ADDR_INCR(srcV_00,2));
		Vec::V4Float16Vec8Unpack(a3_00, b3_00, ADDR_INCR(srcV_00,3));

		Vec::V4Float16Vec8Unpack(a0_10, b0_10, ADDR_INCR(srcV_10,0));
		Vec::V4Float16Vec8Unpack(a1_10, b1_10, ADDR_INCR(srcV_10,1));
		Vec::V4Float16Vec8Unpack(a2_10, b2_10, ADDR_INCR(srcV_10,2));
		Vec::V4Float16Vec8Unpack(a3_10, b3_10, ADDR_INCR(srcV_10,3));

		Vec::V4Float16Vec8Unpack(a0_01, b0_01, ADDR_INCR(srcV_01,0));
		Vec::V4Float16Vec8Unpack(a1_01, b1_01, ADDR_INCR(srcV_01,1));
		Vec::V4Float16Vec8Unpack(a2_01, b2_01, ADDR_INCR(srcV_01,2));
		Vec::V4Float16Vec8Unpack(a3_01, b3_01, ADDR_INCR(srcV_01,3));

		Vec::V4Float16Vec8Unpack(a0_11, b0_11, ADDR_INCR(srcV_11,0));
		Vec::V4Float16Vec8Unpack(a1_11, b1_11, ADDR_INCR(srcV_11,1));
		Vec::V4Float16Vec8Unpack(a2_11, b2_11, ADDR_INCR(srcV_11,2));
		Vec::V4Float16Vec8Unpack(a3_11, b3_11, ADDR_INCR(srcV_11,3));

		ADDR_SKIP(srcV_00,4);
		ADDR_SKIP(srcV_10,4);
		ADDR_SKIP(srcV_01,4);
		ADDR_SKIP(srcV_11,4);

		a0_00 = Vec::V4Lerp(tvx, a0_00, a0_10); b0_00 = Vec::V4Lerp(tvx, b0_00, b0_10);
		a1_00 = Vec::V4Lerp(tvx, a1_00, a1_10); b1_00 = Vec::V4Lerp(tvx, b1_00, b1_10);
		a2_00 = Vec::V4Lerp(tvx, a2_00, a2_10); b2_00 = Vec::V4Lerp(tvx, b2_00, b2_10);
		a3_00 = Vec::V4Lerp(tvx, a3_00, a3_10); b3_00 = Vec::V4Lerp(tvx, b3_00, b3_10);

		a0_01 = Vec::V4Lerp(tvx, a0_01, a0_11); b0_01 = Vec::V4Lerp(tvx, b0_01, b0_11);
		a1_01 = Vec::V4Lerp(tvx, a1_01, a1_11); b1_01 = Vec::V4Lerp(tvx, b1_01, b1_11);
		a2_01 = Vec::V4Lerp(tvx, a2_01, a2_11); b2_01 = Vec::V4Lerp(tvx, b2_01, b2_11);
		a3_01 = Vec::V4Lerp(tvx, a3_01, a3_11); b3_01 = Vec::V4Lerp(tvx, b3_01, b3_11);

		a0_00 = Vec::V4Lerp(tvy, a0_00, a0_01); b0_00 = Vec::V4Lerp(tvy, b0_00, b0_01);
		a1_00 = Vec::V4Lerp(tvy, a1_00, a1_01); b1_00 = Vec::V4Lerp(tvy, b1_00, b1_01);
		a2_00 = Vec::V4Lerp(tvy, a2_00, a2_01); b2_00 = Vec::V4Lerp(tvy, b2_00, b2_01);
		a3_00 = Vec::V4Lerp(tvy, a3_00, a3_01); b3_00 = Vec::V4Lerp(tvy, b3_00, b3_01);

		ADDR_INCR(dstV,0*2+0) = a0_00; ADDR_INCR(dstV,0*2+1) = b0_00;
		ADDR_INCR(dstV,1*2+0) = a1_00; ADDR_INCR(dstV,1*2+1) = b1_00;
		ADDR_INCR(dstV,2*2+0) = a2_00; ADDR_INCR(dstV,2*2+1) = b2_00;
		ADDR_INCR(dstV,3*2+0) = a3_00; ADDR_INCR(dstV,3*2+1) = b3_00;

		ADDR_SKIP(dstV,4*2+0);
	}

	for (const int block2 = count - 2; i <= block2; i += 2)
	{
		Vec::Vector_4V a0_00, b0_00, a0_10, b0_10, a0_01, b0_01, a0_11, b0_11;
		Vec::Vector_4V a1_00, b1_00, a1_10, b1_10, a1_01, b1_01, a1_11, b1_11;

		Vec::V4Float16Vec8Unpack(a0_00, b0_00, ADDR_INCR(srcV_00,0));
		Vec::V4Float16Vec8Unpack(a1_00, b1_00, ADDR_INCR(srcV_00,1));

		Vec::V4Float16Vec8Unpack(a0_10, b0_10, ADDR_INCR(srcV_10,0));
		Vec::V4Float16Vec8Unpack(a1_10, b1_10, ADDR_INCR(srcV_10,1));

		Vec::V4Float16Vec8Unpack(a0_01, b0_01, ADDR_INCR(srcV_01,0));
		Vec::V4Float16Vec8Unpack(a1_01, b1_01, ADDR_INCR(srcV_01,1));

		Vec::V4Float16Vec8Unpack(a0_11, b0_11, ADDR_INCR(srcV_11,0));
		Vec::V4Float16Vec8Unpack(a1_11, b1_11, ADDR_INCR(srcV_11,1));

		ADDR_SKIP(srcV_00,2);
		ADDR_SKIP(srcV_10,2);
		ADDR_SKIP(srcV_01,2);
		ADDR_SKIP(srcV_11,2);

		a0_00 = Vec::V4Lerp(tvx, a0_00, a0_10); b0_00 = Vec::V4Lerp(tvx, b0_00, b0_10);
		a1_00 = Vec::V4Lerp(tvx, a1_00, a1_10); b1_00 = Vec::V4Lerp(tvx, b1_00, b1_10);

		a0_01 = Vec::V4Lerp(tvx, a0_01, a0_11); b0_01 = Vec::V4Lerp(tvx, b0_01, b0_11);
		a1_01 = Vec::V4Lerp(tvx, a1_01, a1_11); b1_01 = Vec::V4Lerp(tvx, b1_01, b1_11);

		a0_00 = Vec::V4Lerp(tvy, a0_00, a0_01); b0_00 = Vec::V4Lerp(tvy, b0_00, b0_01);
		a1_00 = Vec::V4Lerp(tvy, a1_00, a1_01); b1_00 = Vec::V4Lerp(tvy, b1_00, b1_01);

		ADDR_INCR(dstV,0*2+0) = a0_00; ADDR_INCR(dstV,0*2+1) = b0_00;
		ADDR_INCR(dstV,1*2+0) = a1_00; ADDR_INCR(dstV,1*2+1) = b1_00;

		ADDR_SKIP(dstV,2*2+0);
	}

	for (; i < count; i++)
	{
		Vec::Vector_4V a0_00, b0_00, a0_10, b0_10, a0_01, b0_01, a0_11, b0_11;

		Vec::V4Float16Vec8Unpack(a0_00, b0_00, ADDR_INCR(srcV_00,0));

		Vec::V4Float16Vec8Unpack(a0_10, b0_10, ADDR_INCR(srcV_10,0));

		Vec::V4Float16Vec8Unpack(a0_01, b0_01, ADDR_INCR(srcV_01,0));

		Vec::V4Float16Vec8Unpack(a0_11, b0_11, ADDR_INCR(srcV_11,0));

		ADDR_SKIP(srcV_00,1);
		ADDR_SKIP(srcV_10,1);
		ADDR_SKIP(srcV_01,1);
		ADDR_SKIP(srcV_11,1);

		a0_00 = Vec::V4Lerp(tvx, a0_00, a0_10); b0_00 = Vec::V4Lerp(tvx, b0_00, b0_10);

		a0_01 = Vec::V4Lerp(tvx, a0_01, a0_11); b0_01 = Vec::V4Lerp(tvx, b0_01, b0_11);

		a0_00 = Vec::V4Lerp(tvy, a0_00, a0_01); b0_00 = Vec::V4Lerp(tvy, b0_00, b0_01);

		ADDR_INCR(dstV,0*2+0) = a0_00; ADDR_INCR(dstV,0*2+1) = b0_00;

		ADDR_SKIP(dstV,1*2+0);
	}

#undef ADDR_INCR
#undef ADDR_SKIP

#else // not TIMECYCLE_VECTORISED

	tcKeyframeUncompressed temp00(keyframe00);
	tcKeyframeUncompressed temp10(keyframe10);
	tcKeyframeUncompressed temp01(keyframe01);
	tcKeyframeUncompressed temp11(keyframe11);
	tcKeyframeUncompressed temp00_10;
	tcKeyframeUncompressed temp01_11;

	temp00_10.InterpolateKeyframe(temp00, temp10, tx);	
	temp01_11.InterpolateKeyframe(temp01, temp11, tx);

	InterpolateKeyframe(temp00_10, temp01_11, ty);

#endif // not TIMECYCLE_VECTORISED
}

// ================================================================================================

tcKeyframe::tcKeyframe()
{
	Assertf(TIMECYCLE_VAR_COUNT == tcConfig::GetNumVars(), "TIMECYCLE_VAR_COUNT must match TCVAR_NUM = tcConfig::GetNumVars()");
}

__COMMENT(explicit) tcKeyframe::tcKeyframe(const tcKeyframeUncompressed& src)
{
	Assertf(TIMECYCLE_VAR_COUNT == tcConfig::GetNumVars(), "TIMECYCLE_VAR_COUNT must match TCVAR_NUM = tcConfig::GetNumVars()");

#if TIMECYCLE_VECTORISED

	Vec::V4Float16StreamPack((Vec::Vector_4V*)GetStreamAddr(), (const Vec::Vector_4V*)src.GetStreamAddr(), (TIMECYCLE_VAR_COUNT + 7)&~7);

#else // not TIMECYCLE_VECTORISED

	for (int i = 0; i < TIMECYCLE_VAR_COUNT; i++)
	{
		SetVarCompressed_INIT_OR_DEBUG_ONLY(i, src.GetVar(i));
	}

#endif // not TIMECYCLE_VECTORISED
}

void tcKeyframe::SetDefault()
{
	tcKeyframeUncompressed defaultKeyframe;

	defaultKeyframe.SetDefault();

	(*this) = tcKeyframe(defaultKeyframe);
}

float tcKeyframe::GetVarUncompressed_INIT_OR_DEBUG_ONLY(int index) const
{
	Float16 temp;
	temp.SetBinaryData(m_vars[index]);
	return temp.GetFloat32_FromFloat16();
}

void tcKeyframe::SetVarCompressed_INIT_OR_DEBUG_ONLY(int index, float value)
{
	Float16 temp;
	temp.SetFloat16_FromFloat32(value);
	m_vars[index] = temp.GetBinaryData();
}

} // namespace rage
