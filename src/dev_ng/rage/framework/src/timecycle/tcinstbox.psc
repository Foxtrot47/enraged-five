<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<!--
		tcInstBoxType
	-->
	<structdef type="rage::tcInstBoxType">
		<string	name="m_name" type="atHashString" />
    <string	name="m_modifier" type="atHashString" />
		<struct name="m_aabb" type="rage::spdAABB"/>
		<float name="m_percentage"/>
		<float name="m_range" />
		<u8 name="m_startHour" />
		<u8 name="m_endHour" />
	</structdef>
			
	<!--
		tcInstBoxTypeLoader
	-->
	<structdef type="rage::tcInstBoxTypeLoader">
		<array name="m_boxTypes" type="atArray">
			<struct type="rage::tcInstBoxType" />
		</array>
	</structdef>

	<!--
		tcInstBoxInst
	-->
	<structdef type="rage::tcInstBoxInst">
		<float name="m_x"/>
		<float name="m_y"/>
		<float name="m_z"/>
		<string	name="m_typeName" type="atHashString" />
	</structdef>

	<!--
		tcInstBoxInstLoader
	-->
	<structdef type="rage::tcInstBoxInstLoader">
		<array name="m_boxInstances" type="atArray" align="16">
			<struct type="rage::tcInstBoxInst" />
		</array>
	</structdef>
	
</ParserSchema>