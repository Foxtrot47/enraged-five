//
// timecycle/tcconfig.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "timecycle/tcconfig.h"

// includes (framework)
#include "timecycle/channel.h"
#include "timecycle/optimisations.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()


// namespaces
namespace rage {


// static variables
int	tcConfig::ms_numVars = 0;
tcVarInfo* tcConfig::ms_pVarInfos = NULL;
atArray<tcCycleInfo> tcConfig::ms_cycleInfo;
atArray<tcRegionInfo> tcConfig::ms_regionInfo;
atArray<tcTimeSampleInfo> tcConfig::ms_timeSampleInfo;


// code
void tcConfig::Init(const int numVars, tcVarInfo* pVarInfos)
{
	ms_numVars = numVars;
	ms_pVarInfos = pVarInfos;
}

void tcConfig::InitMap(const int numCycles, const tcCycleInfo* pCycleInfos, const int numRegions, const tcRegionInfo* pRegionInfos, const int numTimeSamples, const tcTimeSampleInfo* pTimeSampleInfos/*, const int numDebugGroups, const char** ppDebugGroupNames*/)
{
	// time sample info
	tcAssertf(numTimeSamples<=TIMECYCLE_MAX_TIME_SAMPLES, "number of time samples exceeds the max");
	ms_timeSampleInfo.Reset();
	ms_timeSampleInfo.Reserve(numTimeSamples);

	for (int i=0; i<numTimeSamples; i++)
	{
		ms_timeSampleInfo.Append() = pTimeSampleInfos[i];
	}

	// cycle info
	tcAssertf(numCycles<=TIMECYCLE_MAX_CYCLES, "number of cycles exceeds the max");
	ms_cycleInfo.Reset();
	ms_cycleInfo.Reserve(numCycles);

	for (int i=0; i<numCycles; i++)
	{
		ms_cycleInfo.Append() = pCycleInfos[i];
	}

	// region info
	tcAssertf(numRegions<=TIMECYCLE_MAX_REGIONS, "number of regions exceeds the max");
	ms_regionInfo.Reset();
	ms_regionInfo.Reserve(numRegions);

	for (int i=0; i<numRegions; i++)
	{
		ms_regionInfo.Append() = pRegionInfos[i];
	}
}

void tcConfig::ShutdownMap()
{
	ms_timeSampleInfo.Reset();
	ms_cycleInfo.Reset();
	ms_regionInfo.Reset();
}

} // namespace rage

