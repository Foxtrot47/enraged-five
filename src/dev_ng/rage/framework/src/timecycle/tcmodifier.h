//
// timecycle/tcmodifier.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_MODIFIER_H
#define TC_MODIFIER_H


// includes (rage)
#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/bitset.h"
#include "math/amath.h"

// includes (framework)
#include "timecycle/channel.h"
#include "timecycle/tcconfig.h"

// namespaces
namespace rage {


// forward
class tcKeyframeUncompressed;

// structures

// PURPOSE
//  holds information about the modifier
struct tcModData
{
	int	varId;																	// the id of the variable to modify
	float valA;																	// the modifier data
	float valB;																	// the modifier data

	void Blend(const tcModData &data, tcModifierType_e type, float t, float tInv)
	{
		if (type==MODTYPE_INTERP)
		{
			valA = tInv*valA + t*data.valA;
			valB = tInv*valB + t*data.valB;
		}
		else if (type==MODTYPE_INTERP_MULT)
		{
			float localI = data.valB * t;
			float localcI = 1.0f - localI;

			valA = localcI*valA + localI*data.valA;
			valB = localcI*valB + localI*data.valB;
		}
		else if (type==MODTYPE_CLAMP)
		{
			valA = tInv*valA + t*data.valA;
			valB = tInv*valB + t*data.valB;
		}
		else if (type==MODTYPE_MAX)
		{
			valA = Max(tInv*valA, t*data.valA);
			valB = Max(tInv*valB, t*data.valB);
		}
	}

};

struct VarIdLruCacheEntry;

// classes

// PURPOSE
//  holds the information that is needed to modify a time cycle keyframe 
class tcModifier
{	
	// functions (public)
	public:
		tcModifier();
		~tcModifier();

		// PURPOSE
		//  initialises the modifier data (i.e. no active modifiers)
		void Init() {ResetModData();}

		// PURPOSE
		//  initialises the modifier data to that of a keyframe
		// PARAMS
		//  keyframe - a constant reference to a keyframe to use to initialise this modifier's data with
		void Init(const tcKeyframeUncompressed& keyframe);

		// PURPOSE
		//  initialises the modifier data to that of another modifier
		// PARAMS
		//  modiifer - a constant reference to a modifier to use to initialise this modifier's data with
		void Init(const tcModifier& modifier);

		// PURPOSE
		//  blends a modifier into this modifier
		// PARAMS
		//  mod - a constant reference of a modifier to blend into this one
		//  t - the ratio of the blend (0.0 -> 1.0)
		void Blend(const tcModifier& mod, const float t);

		// PURPOSE
		//  blends a modifier (and/or its override modifier) into this modifier
		// PARAMS
		//  mod - a constant reference of a modifier to blend into this one
		//  ovr - a constant reference of a modifier to override the mod modifier.
		//  t - the ratio of the blend (0.0 -> 1.0)
		void Blend(const tcModifier& mod,const tcModifier& ovr, const float t);

		// PURPOSE
		//  applies this modifier to a time cycle keyframe
		// PARAMS
		//  keyframe - the reference of the keyframe to apply this modifier to
		//  t - the ratio of the apply (0.0 -> 1.0)
		void Apply(tcKeyframeUncompressed& keyframe, const float t) const;


#if __BANK
		// PURPOSE
		//  dump the content of this modifier to TTY
		// PARAMS
		void Dump() const;
#endif

		// PURPOSE
		//  resets the modifier data array
		void ResetModData();

		atHashString GetName(){return m_name;}
		void SetName(atHashString name){m_name = name;}
		// PURPOSE
		//  adds data to the modifier array
		// PARAMS
		//  varId - the id of the variable that is being modified 
		//  valA - the data about how to modify this variable
		//  valB - the data about how to modify this variable
		void AddModData(const int varId, const float valA, const float valB);

		// PURPOSE
		//  gets the value of a specific modifier 
		// PARAMS
		//  varId - the id of the variable that is being queried
		// RETURNS
		//  the value of a specific modifier (will return the default value if there is no modifier for this variable)
		float GetModDataValAUnsafe(const int varId) const;

		// PURPOSE
		//  gets a pointer to the modifier data array
		// RETURNS
		//  a constant pointer to the modifier data array
		const tcModData* GetModDataArray() const {return m_modData.GetElements();}

		// PURPOSE
		//  gets the number of modifiers in the data array
		// RETURNS
		//  the number of modifiers in the data array
		int GetModDataCount() const {return m_modData.GetCount();}

		// PURPOSE
		//  sets the value of a modifier 
		// PARAMS
		//  modDataIndex - the index of the variable that is being set
		//  valA - the data to set on this modifier
		//  valB - the data to set on this modifier
		void SetModDataVal(int modDataIndex, const float valA, const float valB)	{m_modData[modDataIndex].valA = valA; m_modData[modDataIndex].valB = valB;}

		// post-release hack - don't use this. it will be refactored away.
		void ChangeModDataValIfPresent(int varId, const float valA, const float valB)
		{
			for (s32 i=0; i<m_modData.GetCount(); i++)
			{
				if (m_modData[i].varId == varId)
				{
					m_modData[i].valA = valA;
					m_modData[i].valB = valB;
				}
			}
		}

		// PURPOSE
		// Make sure this entry has a valid variable look-up cache entry and create it if necessary. The entry will
		// have its reference count incremented, so it's guaranteed to remain in memory until UnlockVarMap() is called.
		void CreateAndLockVarMap() const;

		// PURPOSE
		// Release a variable look-up cache entry. This function must be called after CreateAndLockVarMap()
		// had been called previously.
		void UnlockVarMap() const;

		// PURPOSE
		// Like GetModData(), but will crash if this modifier doesn't have a variable look-up map.
		const tcModData* GetModDataUnsafe(const int varId) const;

		// PURPOSE
		// Set user flag state
		void SetUserFlag(int bit, bool state) { m_userFlags.Set(bit,state); }
		
		// PURPOSE
		// Get user flag state
		bool GetUserFlag(int bit) const { return m_userFlags.IsSet(bit); }

		// PURPOSE
		// Get user flag
		const atFixedBitSet<32> &GetUserFlags() const { return m_userFlags; }
		atFixedBitSet<32> &GetUserFlags() { return m_userFlags; }

		// PURPOSE
		// Store the index of this modifier's VarIdLruCacheEntry
		void SetIndexOfCacheEntry(int index) { m_IndexOfCacheEntry = index; }

		static void SetOnlyMainThreadIsUpdating(bool bMainThreadOnly);
		// PURPOSE
		//  removes modifier data from the data array
		// PARAMS
		//  the index into the data array to remove 
		void RemoveModData(int idx);
#if __BANK
		// PURPOSE
		//  Display VarMap Cache use.
		static void ShowMeVarMapCacheUse();

		// PURPOSE
		//  Nuke VarMap Cache.
		static void ClearVarMapCache();
		
#endif

	private:
		// PURPOSE
		// Allocates a variable look-up map for this modifier by grabbing an entry from the cache (thereby potentially
		// invalidating an old one) and populates it.
		void AllocateVarMap() const;

		// PURPOSE
		// Invalidates the variable look-up map for this modifier, if there is one.
		void RemoveVarMap();

		// RETURNS
		//  True if this modifier has a variable look-up map.
		bool HasVarMap() const		{ return m_VarIdToDataMap != NULL; }

		VarIdLruCacheEntry *LookupCacheEntry() const;

		// PURPOSE
		// Like GetModDataId(), but will crash if this modifier doesn't have a variable look-up map.
		int GetModDataIdUnsafe(const int varId) const;

	// variables (private)
	private:
		atArray<tcModData> m_modData;											// array of modifier data
		atHashString m_name;
		mutable atArray<tcModData *> *m_VarIdToDataMap;							// Look-up map from varId to tcModData
		atFixedBitSet<32> m_userFlags;
		mutable int m_IndexOfCacheEntry;

};


} // namespace rage

#endif // TC_MODIFIER_H

