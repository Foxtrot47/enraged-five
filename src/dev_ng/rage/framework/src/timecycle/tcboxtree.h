//
// timecycle/tcboxtree.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_BOXTREE_H
#define TC_BOXTREE_H


// includes (rage)
#include "spatialdata/aabb.h"
#include "spatialdata/mnbvh.h"

// includes (framework)
#include "timecycle/channel.h"


// namespaces 
namespace rage {

#define	TIMECYCLE_MAX_MODIFIER_BOX_QUERY	(32)								// the maximum number of modifier boxes that can be returned in a tree query

// classes

// PURPOSE
//  - holds information about a time cycle box tree
class tcBoxTree
{		
	// functions (public)
	public:

		// PURPOSE
		//  constructs a box tree by resetting its data 
		tcBoxTree() { m_pTree = NULL; Reset();}

		// PURPOSE
		//  resets (initialises) the data in this box tree
		void Reset();

		// PURPOSE
		//  Initialise the node list container 
		// PARAMS
		//  numNodes - expected number of nodes to be stored.
		void Init(int numNodes);
		
		// PURPOSE
		//  inserts data into the box tree
		// PARAMS
		//  bBox - the bounding box of the new box being added into the tree
		//  range - the range of the new box being added into the tree
		//  index - the index of the box being added into the tree
		void Insert(const spdAABB& bBox, const float range, const int index);

		// PURPOSE
		//  Actually build the tree
		// PARAMS
		void BuildTree();

#if __ASSERT
		size_t GetListCount() const {return m_list.GetCount();}
#endif

		// PURPOSE
		//  inserts data into the box tree
		// PARAMS
		//  vPos - the bounding box of the new box being added into the tree
		//  extent - size of the box searched for.
		//  indices - pointer to an array of indices
		//  maxfound - size of the index array
		//  number of found indices
		void GetIndices(Vec3V_In vPos, float extent, int* indices, int maxfound, int& numfound);
		
	// variables (private)
	private:
		MNBVHSimpleInterface* m_pTree;
		atArray<spdMNBVHBounds> m_list;
		bool m_useTree;


};


} // namespace rage


#endif // TC_BOXTREE_H




