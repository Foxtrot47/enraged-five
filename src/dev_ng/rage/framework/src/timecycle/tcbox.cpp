//
// timecycle/tcbox.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "timecycle/tcbox.h"

// includes (framework)
#include "timecycle/channel.h"
#include "timecycle/optimisations.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()


// namespaces
namespace rage {

FW_INSTANTIATE_CLASS_POOL_SPILLOVER(tcBox, CONFIGURED_FROM_FILE, 0.22f, atHashString("tcBox",0x43ed4d14));

// defines
#define BOX_STRENGTH_ZDISTMULT_V ScalarV(V_THREE) // 3.0f

// code
float tcBox::CalcStrengthMult(Vec3V_In vPos, const int hours, const int minutes, const int seconds) const 
{
	// 9/12/12 - cthomas - Simply call CalcPosStrengthMult() and CalcTimeStrengthMult() always. We 
	// previously called CalcTimeStrengthMult() always and conditionally called CalcTimeStrengthMult(), 
	// but the condition was a floating-point compare that has a very nasty penalty (pipeline flush).
	// Now, just call them both - with the addition that CalcPosStrengthMult() returns the ScalarV that 
	// it calculates, and we don't do the .Getf() until after we call CalcTimeStrengthMult(), thereby 
	// avoiding another very costly penalty (LHS from accessing the ScalarV right after it was calculated).
	const ScalarV positionalStrengthMult = CalcPosStrengthMult(vPos, m_bBox, m_rangeReciprocal, m_treatAsSphere);
	const float timeStrengthMult = CalcTimeStrengthMult(m_startHour, m_endHour, hours, minutes, seconds);

	return  positionalStrengthMult.Getf() * timeStrengthMult;
}

} // namespace rage



