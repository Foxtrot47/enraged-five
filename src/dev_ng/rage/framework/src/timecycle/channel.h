//
// timecycle/channel.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef TIMECYCLE_CHANNEL_H 
#define TIMECYCLE_CHANNEL_H 

#include "diag/channel.h"

namespace rage{

RAGE_DECLARE_CHANNEL(timecycle)

#define tcAssert(cond)							RAGE_ASSERT(timecycle,cond)
#define tcAssertf(cond,fmt,...)					RAGE_ASSERTF(timecycle,cond,fmt,##__VA_ARGS__)
#define tcFatalAssertf(cond,fmt,...)			RAGE_FATALASSERTF(timecycle,cond,fmt,##__VA_ARGS__)
#define tcVerifyf(cond,fmt,...)					RAGE_VERIFYF(timecycle,cond,fmt,##__VA_ARGS__)
#define tcErrorf(fmt,...)						RAGE_ERRORF(timecycle,fmt,##__VA_ARGS__)
#define tcWarningf(fmt,...)						RAGE_WARNINGF(timecycle,fmt,##__VA_ARGS__)
#define tcDisplayf(fmt,...)						RAGE_DISPLAYF(timecycle,fmt,##__VA_ARGS__)
#define tcDebugf1(fmt,...)						RAGE_DEBUGF1(timecycle,fmt,##__VA_ARGS__)
#define tcDebugf2(fmt,...)						RAGE_DEBUGF2(timecycle,fmt,##__VA_ARGS__)
#define tcDebugf3(fmt,...)						RAGE_DEBUGF3(timecycle,fmt,##__VA_ARGS__)
#define tcLogf(severity,fmt,...)				RAGE_LOGF(timecycle,severity,fmt,##__VA_ARGS__)

} // namespace rage

#endif // TIMECYCLE_CHANNEL_H
