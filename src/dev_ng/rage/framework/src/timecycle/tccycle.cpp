//
// timecycle/tccycle.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "timecycle/tccycle.h"

// includes (rage)
#include "parser/manager.h"

// includes (framework)
#include "timecycle/channel.h"
#include "timecycle/tckeyframe.h"
#include "timecycle/optimisations.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()


// namespaces
namespace rage {


// code
void tcCycle::Init(const char* pFilename)
{
	for (int i=0; i<tcConfig::GetNumRegions(); i++)
	{
		for (int j=0; j<tcConfig::GetNumTimeSamples(); j++)
		{
			m_keyframes[i][j].SetDefault();
		}
	}

	Load(pFilename);
}

void tcCycle::Load(const char* pFilename)
{
	tcDebugf1("Trying to load cycle: %s", pFilename);

	parTree* pTree = PARSER.LoadTree(pFilename, "xml");

	if (pTree)
	{
		parTreeNode* pRootNode= pTree->GetRoot();

		if (pRootNode->GetElement().FindAttribute("version"))
		{
//			cycleFileVersion = (float)atof(pRootNode->GetElement().FindAttribute("version")->GetStringValue());
		}
		else
		{
			tcAssertf(0, "missing attribute \"version\" in %s", pFilename);
		}

		tcDebugf1("- opened ok");

		parTreeNode* pCycleNode = NULL;
		if ((pCycleNode = pRootNode->FindChildWithName("cycle", pCycleNode)) != NULL)
		{
			// get the name of this cycle
			m_name.SetFromString(pCycleNode->GetElement().FindAttribute("name")->GetStringValue());

			int numRegions = 0;
			numRegions = atoi(pCycleNode->GetElement().FindAttribute("regions")->GetStringValue());
			tcAssertf(numRegions==tcConfig::GetNumRegions(), "cycle file region mismatch (%s)", pFilename);

			parTreeNode* pRegionNode = NULL;
			for (int regId=0; regId<numRegions; regId++)
			{
				if ((pRegionNode = pCycleNode->FindChildWithName("region", pRegionNode)) != NULL)
				{
					// get the var data
					parTreeNode* pVarNode = NULL;
					for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
					{
						const char* varName = tcConfig::GetVarInfo(varId).name;
						if ((pVarNode = pRegionNode->FindChildWithName(varName, NULL)) != NULL)
						{
							char* varData = pVarNode->GetData();

							char* val;
							const char* delimit = " ,\t";

							int timeSample = 0;
							val = strtok(varData, delimit);
							while (val != NULL)
							{
								if (timeSample<tcConfig::GetNumTimeSamples())
								{
									m_keyframes[regId][timeSample++].SetVarCompressed_INIT_OR_DEBUG_ONLY(varId, (float)atof(val));
								}

								val = strtok(NULL, delimit);
							}

							tcAssertf(timeSample==tcConfig::GetNumTimeSamples(), "incorrect number of time samples for var %s in file %s", varName, pFilename);
						}
						else
						{
							tcDebugf1("- cannot find variable %s in file %s", varName, pFilename);
						}
					}
				}
			}

			// copy region 0 data over any other regions not loaded
#if __DEV
			for (int regId=numRegions; regId<tcConfig::GetNumRegions(); regId++)
			{
				for (int timeSample=0; timeSample<tcConfig::GetNumTimeSamples(); timeSample++)
				{
					m_keyframes[regId][timeSample] = m_keyframes[0][timeSample];
				}
			}
#endif
		}
		else
		{
			tcAssertf(0, "cycle file contains no valid data (%s)", pFilename);
		}

		delete pTree;
	}
	else
	{
		tcAssertf(false, "cannot open %s for reading", pFilename);	
	}
}

} // namespace rage
