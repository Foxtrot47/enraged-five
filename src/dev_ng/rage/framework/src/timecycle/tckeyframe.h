//
// timecycle/tckeyframe.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_KEYFRAME_H
#define TC_KEYFRAME_H

#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

#define TIMECYCLE_VAR_COUNT	(429) // must match TCVAR_NUM

namespace rage {

class tcKeyframeUncompressed;
class tcKeyframe;

class ALIGNAS(32) tcKeyframeUncompressed
{
public:
	friend class tcKeyframe;

	tcKeyframeUncompressed();
	explicit tcKeyframeUncompressed(const tcKeyframe& src);

	void SetDefault();

	void InterpolateKeyframeBlend(
		const tcKeyframeUncompressed& keyframe1,
		float t
		);

	void InterpolateKeyframe(
		const tcKeyframeUncompressed& keyframe0,
		const tcKeyframeUncompressed& keyframe1,
		float t
		);
	void InterpolateKeyframe(
		const tcKeyframe& keyframe0,
		const tcKeyframe& keyframe1,
		float t // interpolate between [keyframe0..keyframe1]
		);
	void InterpolateKeyframe(
		const tcKeyframe& keyframe00,
		const tcKeyframe& keyframe10,
		const tcKeyframe& keyframe01,
		const tcKeyframe& keyframe11,
		float tx, // interpolate between [keyframe00..keyframe10], [keyframe01..keyframe11]
		float ty  // interpolate between [keyframe00..keyframe01], [keyframe10..keyframe11]
		);

	__forceinline float* GetVarPtr(int index)
	{
		return &m_vars[index];
	}

	__forceinline float GetVar(int index) const
	{
		return m_vars[index];
	}

	__forceinline Vec3V_Out GetVarV3(int index0) const
	{
		return Vec3V(m_vars[index0], m_vars[index0 + 1], m_vars[index0 + 2]);
	}

	__forceinline Vec4V_Out GetVarV4(int index0) const
	{
		return Vec4V(m_vars[index0], m_vars[index0 + 1], m_vars[index0 + 2], m_vars[index0 + 3]);
	}

	__forceinline void SetVar(int index, float value)
	{
		m_vars[index] = value;
	}

	__forceinline void MultVar(int index, float value)
	{
		m_vars[index] *= value;
	}

private:
	__forceinline       void* GetStreamAddr()       { return &m_vars[0]; }
	__forceinline const void* GetStreamAddr() const { return &m_vars[0]; }

	float m_vars[(TIMECYCLE_VAR_COUNT + 15)&~15];
};

// 32-byte aligned so it can use faster codepath in V4Float16StreamUnpack
class ALIGNAS(32) tcKeyframe
{
public:
	friend class tcKeyframeUncompressed;

	tcKeyframe();
	explicit tcKeyframe(const tcKeyframeUncompressed& src);

	void SetDefault();

	float GetVarUncompressed_INIT_OR_DEBUG_ONLY(int index) const;
	void  SetVarCompressed_INIT_OR_DEBUG_ONLY  (int index, float value);

private:
	__forceinline       void* GetStreamAddr()       { return &m_vars[0]; }
	__forceinline const void* GetStreamAddr() const { return &m_vars[0]; }

	u16 m_vars[(TIMECYCLE_VAR_COUNT + 15)&~15];
};

} // namespace rage

#endif // TC_KEYFRAME_H
