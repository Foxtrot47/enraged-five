//
// timecycle/tcconfig.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_CONFIG_H
#define TC_CONFIG_H


// includes (rage)
#include "atl/array.h"


// namespaces 
namespace rage {


// defines
#define TIMECYCLE_MAX_TIME_SAMPLES		(13)									// the maximum number of time samples in each cycle (changed from 16 to 11 on 2010.09.17) 
#define TIMECYCLE_MAX_CYCLES			(16)									// the maximum number of cycles that can be loaded at once
#define TIMECYCLE_MAX_REGIONS			(2)										// the maximum number of regions that each cycle has (changed from 4 to 2 on 2010.09.17)
#define TIMECYCLE_MAX_VARIABLE_GROUPS	(24)									// the maximum number of variable groups allowed


// enumerations
enum tcVarType_e																// specifies the type of a variables in the keyframe variable list
{
	VARTYPE_NONE = 0,															// - no variables type - set if this is a continuation of a previous type (e.g. a green or blue component of a colour)
	VARTYPE_FLOAT,																// - a single float variable
	VARTYPE_COL3,																// - a colour variable with 3 components (r, g, b)
	VARTYPE_COL4,																// - a colour variable with 4 components (r, g, b, a)
	VARTYPE_COL3_LIN,															// - a colour variable with 3 components (r, g, b) but linearised from sRGB
	VARTYPE_COL4_LIN															// - a colour variable with 4 components (r, g, b, a) but linearised from sRGB

};

enum tcModifierType_e															// specifies how a variable in the keyframe variable list gets modified
{
	MODTYPE_NONE = 0,															// - no modifier applied
	MODTYPE_INTERP,																// - interpolates between existing value and modified value using the modifier strength
	MODTYPE_INTERP_MULT,														// - interpolates between existing value and modified value using the modifier strength times the multiplier
	MODTYPE_CLAMP,																// - clamps the existing value between two modified values
	MODTYPE_MAX																	// - takes the maximum value of the existing value and modified value using the modifier strength
};

#if __BANK
enum tcModifierMenu_e															// specifies how a variable in the keyframe is added to the editor widgets
{
	MODMENU_DEFAULT = 0,														// - no specified modifier menu - just add to advanced list
	MODMENU_BASIC,																// - add to the basic modifier menu (as well as the advanced list)
	MODMENU_SPECIAL,															// - add to special menu and not to any other

	// must be last
	MODMENU_NUM
};
#endif

// structures

// PURPOSE
//  holds the cycle info that the game sets
struct tcCycleInfo
{
	char				filename[64];											// - the filename of the cycle data
};

// PURPOSE
//  holds the region info that the game sets
struct tcRegionInfo
{
	u32					hashName;												// - the hash name of the region
#if __BANK
	char				name[32];												// - the name of the region (debug only)
#endif
};

// PURPOSE
//  holds the time sample info that the game sets
struct tcTimeSampleInfo
{
	float				hour;													// - the hour that this time sample info represents
	float				duration;												// - number of hours this should actually interpolate over.
#if __BANK
	const char*			name;													// - the name of the time sample (debug only)
#endif
};

// PURPOSE
//  the data that is required for each time cycle variable in the keyframe
struct tcVarInfo
{
	int					varId;													// - the id of the variable
	const char*			name;													// - the name of the variable - used when loading and saving data

	float				defaultValue;											// - the default value of the variable
	tcVarType_e			varType;												// - the type of the variable - e.g. float, colour3, colour4

	bool				replayOverride;											// - value allowed to be overriden by the replay

	tcModifierType_e	modType;												// - the type of modifier that this variable uses - e.g. interpolation, max, clamp

#if __BANK
	tcModifierMenu_e	debugModMenu;											// - the menu that this variables is added to 
	const char*			debugGroupName;											// - the name of the group that this variable belongs to
	const char*			debugName;												// - the name of this variable in the editor
	float				debugMin;												// - the minimum values that this variable can be
	float				debugMax;												// - the maximum values that this variable can be
	float				debugDelta;												// - the increments that this value changes between the min and max
#endif
};


// classes

// PURPOSE
//  static class that holds the time cycle configuration data
//  this data is defined by the game code and consists of:
//   - information about the time samples that each cycle has
//   - information about the cycles that can be loaded
//   - information about the variables that each entry of the time cycle table consists of
class tcConfig
{
	// functions (public)
	public: 
		// PURPOSE
		//  sets the time cycle variable information 
		// PARAMS
		//  numVars - the number of time cycle variables
		//  pVarInfos - a pointer to a game structure that contains information about each variable
		static void	Init(const int numVars, tcVarInfo* pVarInfos);

		// PURPOSE
		//  sets the time cycle table information 
		// PARAMS
		//  numTimeSamples - the number of time samples that each cycle defines
		//  pTimeSampleInfos - a pointer to a structure defining the time sample information
		//  numCycles - the number of cycles that will be loaded
		//  pCycleInfos - a pointer to a structure defining the cycle information
		static void	InitMap(const int numCycles, const tcCycleInfo* pCycleInfos, const int numRegions, const tcRegionInfo* pRegionInfos, const int numTimeSamples, const tcTimeSampleInfo* pTimeSampleInfos/*, const int numDebugGroups, const char** ppDebugGroupNames*/);

		// PURPOSE
		//  resets the time cycle table information 
		// PARAMS
		static void	ShutdownMap();

		// PURPOSE
		//  gets the number of variables that are defined in a time cycle keyframe
		// RETURNS
		//  the number of variables that are defined in a time cycle keyframe
		static int GetNumVars() {return ms_numVars;}

		// PURPOSE
		//  gets the information about the variables in time cycle keyframe
		// RETURNS
		//  a constant reference to the variable infomation
		static const tcVarInfo&	GetVarInfo(const int varId) { TrapGE((u32) varId, (u32) ms_numVars); /*tcAssertf(varId>=0 && varId<ms_numVars, "index out of range %d/%d", varId, ms_numVars);*/ return ms_pVarInfos[varId];}

		// PURPOSE
		//  gets the number of cycles that are defined
		// RETURNS
		//  the number of cycles that are defined
		static int GetNumCycles () {return ms_cycleInfo.GetCount();}

		// PURPOSE
		//  gets information about the a particular cycle
		// RETURNS
		//  a constant reference to the cycle information
		static const tcCycleInfo& GetCycleInfo (const int index) {return ms_cycleInfo[index];}

		// PURPOSE
		//  gets the number of regions that are defined for each cycle
		// RETURNS
		//  the number of regions that are defined for each cycle
		static int GetNumRegions () {return ms_regionInfo.GetCount();}

		// PURPOSE
		//  gets information about the a particular region
		// RETURNS
		//  a constant reference to the region information
		static const tcRegionInfo& GetRegionInfo (const int index) {return ms_regionInfo[index];}

		// PURPOSE
		//  gets the number of time samples that the time cycle table has
		// RETURNS
		//  the number of time samples that the time cycle table has
		static int GetNumTimeSamples() {return ms_timeSampleInfo.GetCount();}

		// PURPOSE
		//  gets the information about the time samples that the time cycle table uses
		// RETURNS
		//  a constant reference to the time sample information
		static const tcTimeSampleInfo& GetTimeSampleInfo(const int index) {return ms_timeSampleInfo[index];}

	// variables (private)
	private: 

		// variable info
		static int ms_numVars;													// the number of variables in a time cycle keyframe 
		static tcVarInfo* ms_pVarInfos;											// information about each variable in the time cycle keyframe (points back to a structure defined at the game level)

		// table data
		static atArray<tcTimeSampleInfo> ms_timeSampleInfo;						// the time sample information that each cycle has
		static atArray<tcCycleInfo> ms_cycleInfo;								// the cycle information that the manager owns
		static atArray<tcRegionInfo> ms_regionInfo;								// the region information that the manager owns

};


} // namespace rage


#endif // TC_CONFIG_H


