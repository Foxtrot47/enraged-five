//
// timecycle/tcdebug.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "timecycle/tcdebug.h"

#if TC_DEBUG

// includes (rage)
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/combo.h"
#include "bank/msgbox.h"
#include "bank/slider.h"
#include "bank/text.h"
#include "bank/widget.h"
#include "grcore/im.h"
#include "grcore/debugdraw.h"
#include "parser/manager.h"
#include "system/nelem.h"
#include "rmptfx/ptxkeyframe.h"
#include "rmptfx/ptxkeyframewidget.h"
#include "vector/colors.h"
#include "fwscene/stores/mapdatastore.h"

// includes (framework)
#include "timecycle/channel.h"
#include "timecycle/tccycle.h"
#include "timecycle/tcmanager.h"
#include "timecycle/optimisations.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()


// namespaces
namespace rage {


// defines
#define	TIMECYCLE_CURR_CYCLE_FILE_VERSION		(1.0f)
#define	TIMECYCLE_CURR_MODIFIER_FILE_VERSION	(1.0f)


// static variables
tcManager*				tcDebug::m_pManager;
atArray<const char*>	tcDebug::m_debugGroupNames;
atArray<const char*>	tcDebug::m_debugRegionNames;

atArray<const char*>	tcDebug::m_debugFloatNames;
atArray<const char*>	tcDebug::m_debugColorNames;	
atArray<int>			tcDebug::m_debugFloatVarIds;
atArray<int>			tcDebug::m_debugColorVarIds;
ptxKeyframe *			tcDebug::m_debugFloatGraph;
ptxKeyframe *			tcDebug::m_debugColorGraph;
ptxKeyframeDefn *		tcDebug::m_debugFloatGraphDefn;
ptxKeyframeDefn *		tcDebug::m_debugColorGraphDefn;

bool					tcDebug::m_keyframeEditActive;
bool					tcDebug::m_keyframeEditOverrideLightDirection;
bool					tcDebug::m_keyframeEditOverrideTimeSample;
bool					tcDebug::m_keyframeEditOverrideRegion;
bool					tcDebug::m_keyframeEditOverrideCycle;
u32						tcDebug::m_keyframeEditCycleIndex;
int						tcDebug::m_keyframeEditRegionIndex;
int 					tcDebug::m_keyframeEditTimeSampleIndex;
tcKeyframeUncompressed	tcDebug::m_keyframeEditData;
bool					tcDebug::m_keyframeCopyDataValid;
tcKeyframeUncompressed	tcDebug::m_keyframeCopyData;
int						tcDebug::m_keyframeCurrPasteGroup;

int						tcDebug::m_modifierRecordEditId;
int						tcDebug::m_modifierBoxEditId = -1;
Vector3					tcDebug::m_cameraPosition;

bool					tcDebug::m_modifierEditApply;
float					tcDebug::m_modifierEditStrength;
float					tcDebug::m_modifierEditScale = 1;
float					tcDebug::m_modifierEditRange;
float					tcDebug::m_modifierPrevScale = 1;

bool					tcDebug::m_modifierCopyStateValid;
tcModifierEditState		tcDebug::m_modifierCopyState;
atArray<tcModifierEditRecord> tcDebug::m_modifiersInfo;

int						tcDebug::m_GraphFloatVarId[4];
int						tcDebug::m_GraphColorVarId;
int						tcDebug::m_GraphFloatGroup;
int						tcDebug::m_GraphColorGroup;
int						tcDebug::m_GraphLastFloatGroup;
int						tcDebug::m_GraphLastColorGroup;
bool					tcDebug::m_NeedGraphChange;
u32						tcDebug::m_NeedGraphUpdate;
u32						tcDebug::m_RemoteGraphChanged;
int						tcDebug::m_DrawColorType; //0 = as a color gradient, 1 = a line graph

bool					tcDebug::m_noDepthTest;
bool					tcDebug::m_solidBoxes;
u8						tcDebug::m_boxesAlpha;

bool					tcDebug::m_renderModifierBoxes;
bool					tcDebug::m_renderModifierNames;
bool					tcDebug::m_renderModifierBoxNames;
bool					tcDebug::m_renderSelectedOnly;
tcModifierEditState		tcDebug::m_modifierEditState;

bool					tcDebug::m_renderRegionBoxes;

grcDepthStencilStateHandle tcDebug::m_DSS;

bkBank*					tcDebug::m_pBank;
bkGroup*				tcDebug::m_pEditModDataGroup;
bkGroup*				tcDebug::m_pGameGroup;
bkGroup*				tcDebug::m_pCycleEditorGroup;
bkGroup*				tcDebug::m_pGraphViewGroup;
bkText*					tcDebug::m_pModifierNameWidget;
bkSlider*				tcDebug::m_pModifierSliderWidget;
bkCombo*				tcDebug::m_pModifierComboWidget;
bkCombo*				tcDebug::m_pModifierFileWidget;
atArray<const char*>	tcDebug::m_userFlagsNameList;

atArray<tcModifierFileInfo>	tcDebug::m_modifierFilesInfo;


// code
void tcDebug::Init(tcManager* pManager, const tcRegionInfo* pRegionInfos, const int numDebugGroups, const char** ppDebugGroupNames, const int numUserFlags, const char ** ppUserFlagsNames)
{
	// set up the initial state and widgets
	m_pManager = pManager;

	// region names
	int numDebugRegions = tcConfig::GetNumRegions();
	tcAssertf(numDebugRegions<=TIMECYCLE_MAX_REGIONS, "number of debug regions exceeds the max");
	m_debugRegionNames.Reset();
	m_debugRegionNames.Reserve(numDebugRegions);
	m_debugRegionNames.Resize(numDebugRegions);

	for (int i=0; i<numDebugRegions; i++)
	{
		m_debugRegionNames[i] = pRegionInfos[i].name;
	}

	// group names
	tcAssertf(numDebugGroups<=TIMECYCLE_MAX_VARIABLE_GROUPS, "number of time debug groups exceeds the max");
	m_debugGroupNames.Reset();
	m_debugGroupNames.Reserve(numDebugGroups);
	m_debugGroupNames.Resize(numDebugGroups);

	for (int i=0; i<numDebugGroups; i++)
	{
		m_debugGroupNames[i] = ppDebugGroupNames[i];
	}

	// user flags names
	tcAssertf(numUserFlags<=32, "number of user flags exceeds the max");
	m_userFlagsNameList.Reset();
	m_userFlagsNameList.Reserve(numUserFlags);
	m_userFlagsNameList.Resize(numUserFlags);

	for (int i=0; i<numUserFlags; i++)
	{
		m_userFlagsNameList[i] = ppUserFlagsNames[i];
	}

	m_GraphFloatGroup = 0;
	m_GraphColorGroup = 0;
	m_GraphLastFloatGroup=-1;
	m_GraphLastColorGroup=-1;
	m_NeedGraphChange=false;
	m_NeedGraphUpdate=0x00;
	m_RemoteGraphChanged=0x00;
	m_DrawColorType=1; // should change to 0 once the color gradient widget is working better.

	// graph view

	m_debugFloatGraph=NULL;
	m_debugColorGraph=NULL;
	m_debugFloatGraphDefn=NULL;
	m_debugColorGraphDefn=NULL;


	// keyframe editor
	m_keyframeEditActive = false;
	m_keyframeEditOverrideLightDirection = true;
	m_keyframeEditOverrideTimeSample = true;
	m_keyframeEditOverrideRegion = true;
	m_keyframeEditOverrideCycle = true;
	m_keyframeEditCycleIndex = 0;
	m_keyframeEditRegionIndex = 0;
	m_keyframeEditTimeSampleIndex = 0;
	m_keyframeEditData.SetDefault();
	m_keyframeCopyDataValid = false;
	m_keyframeCopyData.SetDefault();
	m_keyframeCurrPasteGroup = 0;

	// modifier editor 
	m_modifierRecordEditId = 0;

	m_modifierEditApply = false;
	m_modifierEditStrength = 1.0f;
	m_modifierCopyStateValid = false;


	// Graph view 
	for (int i=0;i<4;i++)
		m_GraphFloatVarId[i] = 0;
	m_GraphColorVarId = 0;

	// Depth test our boxes
	m_noDepthTest = false;
	
	// Draw wireframe boxes
	m_solidBoxes = false;
	
	// alpha used for boxes
	m_boxesAlpha = 16;
	
	// modifier debug
	m_renderModifierBoxes = false;
	m_renderModifierNames = false;
	m_renderModifierBoxNames = false;

	// region debug
	m_renderRegionBoxes = false;

	// renderstate
	grcDepthStencilStateDesc dssd;
	dssd.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	dssd.DepthWriteMask = 0x00;
	m_DSS = grcStateBlock::CreateDepthStencilState(dssd);

	// widget data
	m_pBank	= NULL;
	m_pEditModDataGroup = NULL;
	m_pGameGroup = NULL;
	m_pCycleEditorGroup = NULL;
	m_pGraphViewGroup = NULL;

	// widgets
	InitWidgets();
}

void tcDebug::Shutdown()
{
	m_debugGroupNames.Reset();
	m_debugRegionNames.Reset();

	m_debugColorNames.Reset();		
	m_debugColorVarIds.Reset();
	m_debugFloatNames.Reset(); 
	m_debugFloatVarIds.Reset();
}

void tcDebug::Update()
{
	KeyframeStoreData();
	if(m_pBank)
	{
		if (m_NeedGraphChange)
			ChangeGraphs();
		if (m_RemoteGraphChanged)
			ProcessRemoteGraphChange();
		if (m_NeedGraphUpdate)
			UpdateGraphs();

		UpdateModifierEditor();
	}
}

void tcDebug::Render() const
{
	// update camera position
	m_cameraPosition = VEC3V_TO_VECTOR3( grcViewport::GetCurrent()->GetCameraPosition() );

	grcBlendStateHandle currentBS = grcStateBlock::BS_Active;
	grcStateBlock::SetBlendState(grcStateBlock::BS_AlphaAdd);

	grcDepthStencilStateHandle currentDSS = grcStateBlock::DSS_Active;
	if( m_noDepthTest )
	{
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
	}
	else
	{
		grcStateBlock::SetDepthStencilState(m_DSS);
	}

	// render the region boxes
	if (m_renderRegionBoxes)
	{
		for (int i=0; i<m_pManager->m_regionBoxes.GetCount(); i++)
		{
			const spdAABB& bBox = m_pManager->m_regionBoxes[i].GetBoundingBox();

			Color32 col(255, 255, 0);

			if( m_solidBoxes )
			{
				col.SetAlpha(m_boxesAlpha);
				
				grcDrawSolidBox(bBox.GetMinVector3(), bBox.GetMaxVector3(), col);
				
				col.SetAlpha(255);
			}

			grcDrawBox(bBox.GetMinVector3(), bBox.GetMaxVector3(), col);
		}
	}

	// render the modifier boxes
	if (m_renderModifierBoxes || m_renderSelectedOnly)
	{
		fwPool<tcBox>* pPool = tcBox::GetPool();
		Assert(pPool);

		for (int i = 0; i < pPool->GetSize(); i++)
		{
			tcBox* pBox = pPool->GetSlot(i);
			if (!pBox)
				continue;

			if( m_renderSelectedOnly && m_modifierBoxEditId != i )
				continue;

			const spdAABB& bBox = pBox->GetBoundingBox();

			Color32 col(255, 255, 255);

			int debugFlag = pBox->GetDebugFlags();
			bool isInterior = (debugFlag & 1 << 1) ? true : false;

			if( debugFlag != 0 )
			{
				bool isUnderwater = (debugFlag & 1 << 0) ? true : false;
				bool expandBox = (debugFlag & 1 << 2) ? true : false;

				if( isUnderwater && isInterior )
				{ // underwater interior
					col = Color_MediumPurple;
				}
				else if( isUnderwater )
				{ // underwater
					col = Color_LightSeaGreen; 
				}
				else if( isInterior )
				{ // interior
					col = Color_IndianRed;
				}
				else if ( expandBox )
				{
					col = Color_plum;
				}
			}

			if( m_modifierBoxEditId == i )
			{
				col = Color_magenta;
			}

			if( isInterior ) // draw spheres
			{
				if( m_solidBoxes )
				{
					col.SetAlpha(m_boxesAlpha);
					grcColor(col);
					grcDrawSphere(bBox.GetExtent().GetXf(),bBox.GetCenter(),8,true,true);
					col.SetAlpha(255);
				}

				grcColor(col);
				grcDrawSphere(bBox.GetExtent().GetXf(),bBox.GetCenter(),8,true,false);
			}
			else // draw boxes
			{
				if( m_solidBoxes )
				{
					col.SetAlpha(m_boxesAlpha);
					grcDrawSolidBox(bBox.GetMinVector3(), bBox.GetMaxVector3(), col);
					col.SetAlpha(255);
				}
			
				grcDrawBox(bBox.GetMinVector3(), bBox.GetMaxVector3(), col);
			}

			// render the modifier name and the modifier box names, if requested from toggles
			if (m_renderModifierNames || m_renderModifierBoxNames) 
			{
				char outputString[256];
				const char *modName = m_pManager->GetModifierName( pBox->GetIndex() );
				const char *boxName = "N/A";
				if (!pBox->GetIsInterior())
				{
					boxName = g_MapDataStore.GetName( strLocalIndex(pBox->GetIplId()) );
				}

				Vector3 center = VEC3V_TO_VECTOR3( bBox.GetCenter() );

				if (m_renderModifierNames && m_renderModifierBoxNames) 
				{
					sprintf( outputString, "%s - %s", boxName, modName );
				}
				else
				{
					sprintf( outputString, "%s", m_renderModifierNames ? modName : boxName );
				}

				grcDebugDraw::Text( center, col, 0, 0, outputString );
			}
		}
	}
	
	grcStateBlock::SetBlendState(currentBS);
	grcStateBlock::SetDepthStencilState(currentDSS);
}

void tcDebug::KeyframeEditStateChanged()
{
	// check if editing is active
	if (m_keyframeEditActive)
	{
		// set the edit keyframe
		m_keyframeEditData = tcKeyframeUncompressed(m_pManager->GetCycle(m_keyframeEditCycleIndex).GetKeyframe(m_keyframeEditRegionIndex, m_keyframeEditTimeSampleIndex));
	}
}

void tcDebug::ChangeGraphs()
{
	m_NeedGraphChange = false;

	// check if editing is active
	if (m_pBank && m_pCycleEditorGroup)
	{
		// open the graphics view group
		m_pBank->SetCurrentGroup(*m_pCycleEditorGroup);
		
		// remove the old group
		if (m_pGraphViewGroup)
			m_pBank->DeleteGroup(*m_pGraphViewGroup);

		//clean out the old keyframes
		if (m_debugFloatGraph)
		{
			delete m_debugFloatGraph;
			delete m_debugFloatGraphDefn;
			m_debugFloatGraph = NULL;
		}

		if (m_debugColorGraph)
		{
			delete m_debugColorGraph;
			delete m_debugColorGraphDefn;
			m_debugColorGraph = NULL;
		}

		//construct a new one with the current variables
		GetGraphVars();

		m_pGraphViewGroup = m_pBank->PushGroup("Graph View", false,"Editable 24 hour graph views of time cycle variables");
		
		m_pBank->PushGroup("Float Variable View",false);
		m_pBank->AddTitle("Pick a group and up to 4 float variables from it");
		m_pBank->AddCombo("Group:", &m_GraphFloatGroup, m_debugGroupNames.GetCount(), &m_debugGroupNames[0], datCallback(CFA1(RequestGraphChange)),"Select a group of timecycle variables");

		// start with the first 4 variables in the group. they can change them later.
		if (m_GraphFloatGroup != m_GraphLastFloatGroup)
		{
			m_GraphLastFloatGroup = m_GraphFloatGroup;
			for (int i=0;i<4;i++)
				m_GraphFloatVarId[i] = (i<m_debugFloatNames.GetCount()) ? i+1 : 0;
		}

		m_pBank->AddCombo("Variable 1:", &m_GraphFloatVarId[0], m_debugFloatNames.GetCount(), &m_debugFloatNames[0], datCallback(CFA1(RequestGraphChange)),"Pick a float variable from the selected group, or 'None' to disable this channel in the graph");
		m_pBank->AddCombo("Variable 2:", &m_GraphFloatVarId[1], m_debugFloatNames.GetCount(), &m_debugFloatNames[0], datCallback(CFA1(RequestGraphChange)),"Pick a float variable from the selected group, or 'None' to disable this channel in the graph");
		m_pBank->AddCombo("Variable 3:", &m_GraphFloatVarId[2], m_debugFloatNames.GetCount(), &m_debugFloatNames[0], datCallback(CFA1(RequestGraphChange)),"Pick a float variable from the selected group, or 'None' to disable this channel in the graph");
		m_pBank->AddCombo("Variable 4:", &m_GraphFloatVarId[3], m_debugFloatNames.GetCount(), &m_debugFloatNames[0], datCallback(CFA1(RequestGraphChange)),"Pick a float variable from the selected group, or 'None' to disable this channel in the graph");
		
#if RMPTFX_BANK
		int timeSamples = tcConfig::GetNumTimeSamples();
#endif
		Vec4V data;
		ScalarV hour;

		float minY= 10000;
		float maxY = -10000;
		float deltaY = 1000;
		const char * labels[4];
		int varCount = 0;

		for (int i=0;i<4;i++)
		{
			labels[i] = NULL;
			if (m_GraphFloatVarId[i]>0)
			{
				const tcVarInfo & varInfo = tcConfig::GetVarInfo(m_debugFloatVarIds[m_GraphFloatVarId[i]]);
				labels[i] = varInfo.debugName;

				if (minY > varInfo.debugMin) minY=varInfo.debugMin;
				if (maxY < varInfo.debugMax) maxY=varInfo.debugMax;
				if (deltaY > varInfo.debugDelta) deltaY=varInfo.debugDelta;
				varCount++;
			}
		}

#if RMPTFX_BANK
		if (varCount>0)
		{
			m_pBank->AddSeparator();
			m_debugFloatGraphDefn = rage_new ptxKeyframeDefn(m_debugGroupNames[m_GraphFloatGroup], 0, PTXKEYFRAMETYPE_FLOAT, 
				Vec4V(V_ZERO),
				Vec3V(0.0f, 24.0f, 1.0f),
				Vec3V(minY, maxY, deltaY),
				labels[0],labels[1],labels[2],labels[3],
				false);

			m_debugFloatGraph = rage_new ptxKeyframe;
			m_debugFloatGraph->SetDefn(m_debugFloatGraphDefn);
			for (int i=0;i<timeSamples;i++)
			{
				GetTimeCycleFloatValues (i, hour, data);
				m_debugFloatGraph->AddEntry(hour,data);
			}
			m_debugFloatGraph->AddWidgets(*m_pBank);
			m_debugFloatGraph->GetWidget()->SetCallback(datCallback(CFA1(RequestRemoteFloatGraphChanged)));
		}
#endif

		m_pBank->PopGroup();
		
		m_pBank->PushGroup("Color Variable View",false);
		m_pBank->AddTitle("Pick a group and color variable from it");
		m_pBank->AddCombo("Group:", &m_GraphColorGroup, m_debugGroupNames.GetCount(), &m_debugGroupNames[0], datCallback(CFA1(RequestGraphChange)),"Select a group of timecycle variables");
		


		if (m_GraphColorGroup != m_GraphLastColorGroup)
		{
			m_GraphLastColorGroup = m_GraphColorGroup;
			m_GraphColorVarId = (m_debugColorNames.GetCount()>0) ? 1: 0;
		}

		m_pBank->AddCombo("Variable:", &m_GraphColorVarId, m_debugColorNames.GetCount(), &m_debugColorNames[0], datCallback(CFA1(RequestGraphChange)),"Pick a color variable from the selected group");
		
		static const char * colorTypes[] = {"Color Gradient","Line Graph"};
		m_pBank->AddCombo("Draw Widget As:", &m_DrawColorType, 2, colorTypes, datCallback(CFA1(RequestGraphChange)),"Choose how the color should be displayed");

#if RMPTFX_BANK
		if (m_GraphColorVarId>0)
		{
			const tcVarInfo & varInfo = tcConfig::GetVarInfo(m_debugColorVarIds[m_GraphColorVarId]);
			
			m_debugColorGraphDefn = rage_new ptxKeyframeDefn(varInfo.debugName, 0, (varInfo.varType==VARTYPE_COL3 || varInfo.varType==VARTYPE_COL3_LIN) ? PTXKEYFRAMETYPE_FLOAT3 : PTXKEYFRAMETYPE_FLOAT4, 
														Vec4V(V_ZERO),
														Vec3V(0.0f, 24.0f, 1.0f),
														Vec3V(0.0f, 1.0f, .01f),
														"Red", "Green", "Blue",(varInfo.varType==VARTYPE_COL3 || varInfo.varType==VARTYPE_COL3_LIN) ? NULL : "Alpha",
														m_DrawColorType==0);

			m_debugColorGraph = rage_new ptxKeyframe;
			
			m_pBank->AddSeparator();
			m_debugColorGraph->SetDefn(m_debugColorGraphDefn);
			for (int i=0;i<timeSamples;i++)
			{
				GetTimeCycleColorValues (i, hour, data);
				m_debugColorGraph->AddEntry(hour,data);
			}
			m_debugColorGraph->AddWidgets(*m_pBank);
			m_debugColorGraph->GetWidget()->SetCallback(datCallback(CFA1(RequestRemoteColorGraphChanged)));
		}
#endif
		
		m_pBank->PopGroup();
		m_pBank->PopGroup();

		// close the group
		m_pBank->UnSetCurrentGroup(*m_pCycleEditorGroup);
		
#if RMPTFX_BANK
		if (m_debugFloatGraph)
			m_debugFloatGraph->UpdateWidget();
		if (m_debugColorGraph)
			m_debugColorGraph->UpdateWidget();
#endif
	}
}

void tcDebug::GetGraphVars()
{
	int colorCount = 1;
	int floatCount = 1;
	for (int i=0; i<tcConfig::GetNumVars(); i++)
	{
		const bool isColour = 
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL3 || 
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL4 ||
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL3_LIN ||
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL4_LIN;

		if (strcmp(tcConfig::GetVarInfo(i).debugGroupName,m_debugGroupNames[m_GraphFloatGroup])==0 && tcConfig::GetVarInfo(i).varType==VARTYPE_FLOAT)
			floatCount++;
		else if (strcmp(tcConfig::GetVarInfo(i).debugGroupName,m_debugGroupNames[m_GraphColorGroup]) == 0 && 
				 isColour) 
			colorCount++;
	}
	
	m_debugFloatNames.Reset(); 				m_debugFloatVarIds.Reset();
	m_debugFloatNames.Reserve(floatCount); 	m_debugFloatVarIds.Reserve(floatCount);
	m_debugFloatNames.Resize(floatCount);	m_debugFloatVarIds.Resize(floatCount);
	m_debugColorNames.Reset();				m_debugColorVarIds.Reset();
	m_debugColorNames.Reserve(colorCount);	m_debugColorVarIds.Reserve(colorCount);
	m_debugColorNames.Resize(colorCount);	m_debugColorVarIds.Resize(colorCount);

	colorCount = 1;
	floatCount = 1;
	m_debugFloatNames[0] = "None";
	m_debugColorNames[0] = "None";

	for (int i=0; i<tcConfig::GetNumVars(); i++)
	{
		const bool isColour = 
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL3 || 
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL4 ||
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL3_LIN ||
			tcConfig::GetVarInfo(i).varType==VARTYPE_COL4_LIN;

		if (strcmp(tcConfig::GetVarInfo(i).debugGroupName,m_debugGroupNames[m_GraphFloatGroup])==0 && tcConfig::GetVarInfo(i).varType==VARTYPE_FLOAT)
		{
			m_debugFloatNames[floatCount] = tcConfig::GetVarInfo(i).debugName;
			m_debugFloatVarIds[floatCount] = tcConfig::GetVarInfo(i).varId;
			floatCount++;
		}
		else if (strcmp(tcConfig::GetVarInfo(i).debugGroupName,m_debugGroupNames[m_GraphColorGroup])==0 && isColour)
		{
			m_debugColorNames[colorCount] = tcConfig::GetVarInfo(i).debugName;
			m_debugColorVarIds[colorCount] = tcConfig::GetVarInfo(i).varId;
			colorCount++;
		}
	}
}

void tcDebug::GetTimeCycleFloatValues(int timeIndex, ScalarV& time, Vec4V& data)
{
	time.Set((float)tcConfig::GetTimeSampleInfo(timeIndex).hour);

	for (int j=0;j<4;j++)
	{
		if (m_GraphFloatVarId[j]>0)
			data.SetElemf(j,m_pManager->GetCycle(m_keyframeEditCycleIndex).GetKeyframe(m_keyframeEditRegionIndex,timeIndex).GetVarUncompressed_INIT_OR_DEBUG_ONLY(m_debugFloatVarIds[m_GraphFloatVarId[j]]));
		else
			data.SetElemf(j,0.0f);
	}
}

void tcDebug::GetTimeCycleColorValues(int timeIndex, ScalarV& time, Vec4V& data)
{
	time.Set((float)tcConfig::GetTimeSampleInfo(timeIndex).hour);

	const tcVarInfo & varInfo = tcConfig::GetVarInfo(m_debugColorVarIds[m_GraphColorVarId]);
	
	data.SetW(ScalarV(V_ZERO));

	int count = (varInfo.varType==VARTYPE_COL3 || varInfo.varType==VARTYPE_COL3_LIN)?3:4;
	for (int j=0; j<count; j++)
		data.SetElemf(j,m_pManager->GetCycle(m_keyframeEditCycleIndex).GetKeyframe(m_keyframeEditRegionIndex,timeIndex).GetVarUncompressed_INIT_OR_DEBUG_ONLY(varInfo.varId+j));				
}

void tcDebug::SetTimeCycleFloatValues(int timeIndex, const Vec4V& data)
{
	// not the most efficient, but it's to only way given the keyframe interface.
	
	tcKeyframe keyFrame = m_pManager->GetCycle(m_keyframeEditCycleIndex).GetKeyframe(m_keyframeEditRegionIndex,timeIndex);
	
	for (int j=0;j<4;j++)
	{			
		if (m_GraphFloatVarId[j]>0)
		{
			keyFrame.SetVarCompressed_INIT_OR_DEBUG_ONLY(m_debugFloatVarIds[m_GraphFloatVarId[j]],data.GetElemf(j));
			if (timeIndex==m_keyframeEditTimeSampleIndex)
				m_keyframeEditData.SetVar(m_debugFloatVarIds[m_GraphFloatVarId[j]],data.GetElemf(j));
		}
	}

	m_pManager->GetCycle(m_keyframeEditCycleIndex).SetKeyframe(m_keyframeEditRegionIndex,timeIndex,keyFrame);
}

void tcDebug::SetTimeCycleColorValues(int timeIndex, const Vec4V& data)
{
	tcKeyframe keyFrame = m_pManager->GetCycle(m_keyframeEditCycleIndex).GetKeyframe(m_keyframeEditRegionIndex,timeIndex);
	const tcVarInfo & varInfo = tcConfig::GetVarInfo(m_debugColorVarIds[m_GraphColorVarId]);

	int count = (varInfo.varType==VARTYPE_COL3 || varInfo.varType==VARTYPE_COL3_LIN)?3:4;
	for (int j=0;j<count;j++)
	{
		keyFrame.SetVarCompressed_INIT_OR_DEBUG_ONLY(varInfo.varId+j,data.GetElemf(j));
		if (timeIndex==m_keyframeEditTimeSampleIndex)
			m_keyframeEditData.SetVar(varInfo.varId+j,data.GetElemf(j));
	}

	m_pManager->GetCycle(m_keyframeEditCycleIndex).SetKeyframe(m_keyframeEditRegionIndex,timeIndex,keyFrame);
}

void tcDebug::UpdateGraphs()
{
#if RMPTFX_BANK
	int timeSamples = tcConfig::GetNumTimeSamples();
	Vec4V data;
	ScalarV time;

	if (m_pBank)
	{
		if(m_debugFloatGraph && (m_NeedGraphUpdate&kGraphFloatView)!=0)
		{
			bool needsUpdate = false;
			for (int i=0; i<timeSamples; i++)
			{
				GetTimeCycleFloatValues(i,time,data);			
				needsUpdate = RealignEntry(m_debugFloatGraph, i, data) || needsUpdate;
			}

			// remove any stray entries 
			needsUpdate = CleanGraph(m_debugFloatGraph) || needsUpdate;

			if (needsUpdate)
				m_debugFloatGraph->UpdateWidget();

		}

		if(m_debugColorGraph && m_GraphColorVarId>0 && (m_NeedGraphUpdate&kGraphColorView)!=0)
		{

			bool needsUpdate = false;
			for (int i=0; i<timeSamples; i++)
			{
				GetTimeCycleColorValues(i,time,data);			
				needsUpdate = RealignEntry(m_debugColorGraph, i, data) || needsUpdate;
			}
			// remove any stray entries 
			needsUpdate =  CleanGraph(m_debugColorGraph) || needsUpdate;

			if (needsUpdate)
				m_debugColorGraph->UpdateWidget();
		}
	}
#endif

	m_NeedGraphUpdate = 0x0;
}

int tcDebug::FindClosestEntry(ptxKeyframe * graph, int timeIndex)
{
	int timeSamples = tcConfig::GetNumTimeSamples();

	if (graph==NULL)
		return -1;

	// look for an entry that is closer to this time than any other time cycle time.
	float bestDelta = 1000000;
	int	  bestIndex = -1;

	for (int entryId = 0;entryId<graph->GetNumEntries();entryId++)
	{
		float entryTime = graph->GetTime(entryId);

		float bestEntryDelta = 1000000;
		int	  bestEntryIndex = -1;
		
		// find the closest time cycle time to this entry;
		for (int i=0; i<timeSamples; i++)
		{
			float delta = Abs(entryTime - tcConfig::GetTimeSampleInfo(i).hour);
			if (delta<bestEntryDelta)
			{
				bestEntryDelta = delta;
				bestEntryIndex = i;
			}
		}
	
		// if the entry is closest to the cycle time requested, keep it
		if (bestEntryIndex==timeIndex)
		{
			if (bestEntryDelta<bestDelta)
			{
				bestDelta = bestEntryDelta;
				bestIndex = entryId;
			}
		}
	}	
	return bestIndex;

}

bool tcDebug::RealignEntry(	ptxKeyframe * graph, int timeIndex, const Vec4V& data)
{
	ScalarV time(tcConfig::GetTimeSampleInfo(timeIndex).hour);
	int entryId = FindClosestEntry(graph,timeIndex);

	if (entryId>=0)
	{
		if (IsEqualIntAll(graph->GetTimeV(entryId),time))  // are we exact?
		{
			// do we need to update the data?
			if (IsEqualIntAll(graph->GetValue(entryId),data))
				return false;	
			else
				graph->SetEntry(entryId,time.Getf(),data);
		}
		else	
		{
			graph->SetEntry(entryId,time.Getf(),data); // no , let's realign
		}		
	}
	else// did not find anything even close.
	{
		graph->AddEntry(time,data);	//  add a new one here
	}
	return true; // let the higher level code know we need to update the widgets
}

void tcDebug::ProcessRemoteGraphChange()
{
	// read the keyframe data and put it back in the time cycle...
	int timeSamples = tcConfig::GetNumTimeSamples();
	Vec4V data;
	
	if (m_debugFloatGraph && (m_RemoteGraphChanged&kGraphFloatView)!=0)
	{
		// first get the value from the keyframes
		for (int i=0;i<timeSamples;i++)
		{
			Vec4V data = m_debugFloatGraph->Query(ScalarV(tcConfig::GetTimeSampleInfo(i).hour));
			SetTimeCycleFloatValues(i,data);	
		}
	}

 	if (m_debugColorGraph && m_GraphColorVarId>0 && (m_RemoteGraphChanged&kGraphColorView)!=0)
	{
		// first get the value from the keyframes
		for (int i=0;i<timeSamples;i++)
		{
			Vec4V data = m_debugColorGraph->Query(ScalarV(tcConfig::GetTimeSampleInfo(i).hour));
			SetTimeCycleColorValues(i,data);
		}
	}

	m_RemoteGraphChanged = 0;
}

bool tcDebug::CleanGraph(ptxKeyframe * graph)
{
	bool changed = false;
	if (graph)
	{
		int timeSamples = tcConfig::GetNumTimeSamples();
		int entryCount = graph->GetNumEntries();
		float entryTime;

		for (int entryId = 0; entryId<entryCount; entryId++)
		{
			int sample;
			entryTime = graph->GetTime(entryId);
			for (sample=0;sample<timeSamples;sample++)
			{
				if (entryTime == tcConfig::GetTimeSampleInfo(sample).hour)
					break;
			}
			if (sample >= timeSamples) // could not find it so let's remove it
			{
				graph->DeleteEntry(entryId);
				changed=true;
				entryId--;
				entryCount--;
			}
		}
	}
	return changed;
}


void tcDebug::RequestFloatGraphUpdate()
{
	m_NeedGraphUpdate |= kGraphFloatView;
}

void tcDebug::RequestColorGraphUpdate()
{
	m_NeedGraphUpdate |= kGraphColorView;
}

void tcDebug::RequestGraphChange()
{
	m_NeedGraphChange = true;
}

void tcDebug::RequestRemoteFloatGraphChanged()
{
	m_RemoteGraphChanged |= kGraphFloatView;
	m_NeedGraphUpdate |= kGraphFloatView;
}

void tcDebug::RequestRemoteColorGraphChanged()
{
	m_RemoteGraphChanged |= kGraphColorView;
	m_NeedGraphUpdate |= kGraphColorView;

}

void tcDebug::KeyframeStoreData()
{
	if (m_keyframeEditActive)
	{
		m_pManager->GetCycle(m_keyframeEditCycleIndex).SetKeyframe(m_keyframeEditRegionIndex, m_keyframeEditTimeSampleIndex, tcKeyframe(m_keyframeEditData));
	}
}

void tcDebug::KeyframeReloadData()
{
	// stop the editor otherwise the edited data will just overwrite the re-loaded data after it's loaded in
	m_keyframeEditActive = false;

	for (int i=0; i<tcConfig::GetNumCycles(); i++)
	{
		m_pManager->GetCycle(i).Load(&tcConfig::GetCycleInfo(i).filename[0]);
	}
}

void tcDebug::KeyframeSaveData()
{
	for (int i=0; i<tcConfig::GetNumCycles(); i++)
	{
		const tcCycleInfo& cycleInfo = tcConfig::GetCycleInfo(i);

		char txt[256];

		// create the tree
		parTree* pTree = rage_new parTree();

		// set up the root element
		parTreeNode* pRootNode = pTree->CreateRoot();
		parElement& rootElement = pRootNode->GetElement();
		rootElement.SetName("timecycle_keyframe_data");

		rootElement.AddAttribute("version", TIMECYCLE_CURR_CYCLE_FILE_VERSION, false);
		pTree->SetRoot(pRootNode);

		// write out the cycle header
		parTreeNode* cycleNode = rage_new parTreeNode();
		cycleNode->GetElement().SetName("cycle");
		cycleNode->GetElement().AddAttribute("name", m_pManager->m_cycles[i].GetName().GetCStr(), false, false);
		cycleNode->GetElement().AddAttribute("regions", tcConfig::GetNumRegions(), false);
		cycleNode->AppendAsChildOf(pRootNode);

		// write out the region data
		for (int regId=0; regId<tcConfig::GetNumRegions(); regId++)
		{
			// write out the region header
			parTreeNode* regionNode = rage_new parTreeNode();
			regionNode->GetElement().SetName("region");
			regionNode->GetElement().AddAttribute("name", m_debugRegionNames[regId], false, false);
			regionNode->AppendAsChildOf(cycleNode);

			// write out the variable data
			for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
			{
				parTreeNode* pKeyframeVarNode = rage_new parTreeNode();

				pKeyframeVarNode->GetElement().SetName(tcConfig::GetVarInfo(varId).name);
				txt[0] = 0;
				for (int timeSample=0; timeSample<tcConfig::GetNumTimeSamples(); timeSample++)
				{
					sprintf(txt, "%s %.4f", txt, m_pManager->m_cycles[i].GetKeyframe(regId, timeSample).GetVarUncompressed_INIT_OR_DEBUG_ONLY(varId));
				} 
				pKeyframeVarNode->SetData(txt, (int)strlen(txt));
				pKeyframeVarNode->AppendAsChildOf(regionNode);
			}
		}

		// save out the data
		if (PARSER.SaveTree(&cycleInfo.filename[0], "", pTree, parManager::XML)==false)
		{
			tcAssertf(false, "cannot open %s for writing - is it checked out?", &cycleInfo.filename[0]);	
		}

		delete pTree;
	}
}

void tcDebug::KeyframeCopyData()
{
	m_keyframeCopyData = m_keyframeEditData;
	m_keyframeCopyDataValid = true;
}

void tcDebug::KeyframePasteData()
{
	if (m_keyframeCopyDataValid)
	{
		m_keyframeEditData = m_keyframeCopyData;
		RequestFloatGraphUpdate();
		RequestColorGraphUpdate();
	}
}

void tcDebug::KeyframePasteGroup()
{
	if (m_keyframeCopyDataValid)
	{
		for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
		{
			const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);
			if (strcmp(varInfo.debugGroupName, m_debugGroupNames[m_keyframeCurrPasteGroup])==0)
			{
				m_keyframeEditData.SetVar(varId, m_keyframeCopyData.GetVar(varId));
			}
		}
		RequestFloatGraphUpdate();
		RequestColorGraphUpdate();
	}
}

int tcDebug::GetModifierEditId() const
{
	return m_pManager->FindModifierIndex(m_modifiersInfo[m_modifierRecordEditId].name,"ModifierRecordEditIdChanged");
}

void tcDebug::SetModifierFileInfo(const char* name)
{
	int fileInfoIdx = GetModifierFileInfoIdx(name);
	
	if( fileInfoIdx == -1 )
	{ // Create a new record
		tcModifierFileInfo &modifier = m_modifierFilesInfo.Grow();
		modifier.name.SetFromString(name);
		modifier.checksum = CalculateFileContentChecksum(name);
	}
	else
	{ // Update current record
		tcModifierFileInfo &modifier = m_modifierFilesInfo[fileInfoIdx];
		modifier.checksum = CalculateFileContentChecksum(name);
	}
}


int tcDebug::GetModifierFileInfoIdx(const char* name)
{
	atHashString str(name);
	for (int i=0; i<m_modifierFilesInfo.GetCount(); i++)
	{
		if( m_modifierFilesInfo[i].name == str)
		{
			return i;
		}
	}
	
	return -1;
}

void tcDebug::ModifierInit()
{
	m_modifierEditState.var.Reset();
	m_modifierEditState.var.Reserve(tcConfig::GetNumVars());
	m_modifierEditState.var.Resize(tcConfig::GetNumVars());

	ModifierRecordEditIdChanged();
	
}

void tcDebug::ModifierRecordEditIdChanged()
{
	// initialise the edited keyframe state
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		m_modifierEditState.var[varId].isActive = false;

		tcModifierType_e modType = tcConfig::GetVarInfo(varId).modType;
		float defaultVal = tcConfig::GetVarInfo(varId).defaultValue;

		if (modType==MODTYPE_INTERP || modType==MODTYPE_MAX)
		{
			m_modifierEditState.var[varId].valA = defaultVal;
			m_modifierEditState.var[varId].valB = 0.0f;
			m_modifierEditState.var[varId].colour = Color32(1.0f, 1.0f, 1.0f, 1.0f);
		}
		else if (modType==MODTYPE_INTERP_MULT)
		{
			m_modifierEditState.var[varId].valA = defaultVal;
			m_modifierEditState.var[varId].valB = 1.0f;
			m_modifierEditState.var[varId].colour = Color32(1.0f, 1.0f, 1.0f, 1.0f);
		}
		else if (modType==MODTYPE_CLAMP)
		{
			m_modifierEditState.var[varId].valA = defaultVal;
			m_modifierEditState.var[varId].valB = defaultVal;
			m_modifierEditState.var[varId].colour = Color32(1.0f, 1.0f, 1.0f, 1.0f);
		}

		for (int j=0; j<MODMENU_NUM; j++)
		{
			m_modifierEditState.var[varId].pWidgetActive[j] = NULL;
			m_modifierEditState.var[varId].pWidgetValA[j] = NULL;
			m_modifierEditState.var[varId].pWidgetValB[j] = NULL;
		}
	}

	int modifierId = m_pManager->FindModifierIndex(m_modifiersInfo[m_modifierRecordEditId].name,"ModifierRecordEditIdChanged");
	tcModifier* pModifier = m_pManager->GetModifier(modifierId);


	m_modifierEditState.fileIdx = m_modifiersInfo[m_modifierRecordEditId].fileIdx;
	m_modifierEditState.userFlags = pModifier->GetUserFlags();
	strncpy(m_modifierEditState.modifierName,m_pManager->GetModifierName(modifierId),TIMECYCLE_MAX_MODIFIER_NAME_LEN);

	// copy over the data from the modifier being edited
	const tcModData* pModDataArray = pModifier->GetModDataArray();
	for (int i=0; i<pModifier->GetModDataCount(); i++)
	{
		int varId = pModDataArray[i].varId;

		m_modifierEditState.var[varId].isActive = true;
		m_modifierEditState.var[varId].valA = pModDataArray[i].valA;
		m_modifierEditState.var[varId].valB = pModDataArray[i].valB;
	}

	// fix up any var data
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);
		tcVarType_e varType = varInfo.varType;
		if (varType == VARTYPE_COL3 || varType == VARTYPE_COL3_LIN)
		{
			m_modifierEditState.var[varId].colour.Setf(m_modifierEditState.var[varId].valA, m_modifierEditState.var[varId+1].valA, m_modifierEditState.var[varId+2].valA, 1.0f);
		}
		else if (varType == VARTYPE_COL4 || varType == VARTYPE_COL4_LIN)
		{
			m_modifierEditState.var[varId].colour.Setf(m_modifierEditState.var[varId].valA, m_modifierEditState.var[varId+1].valA, m_modifierEditState.var[varId+2].valA, m_modifierEditState.var[varId+3].valA);
		}
	}
}

void tcDebug::RemoveModifierRecord(atHashString name, int /*fileIdx*/)
{
	for(int i=0;i<m_modifiersInfo.GetCount();i++)
	{
		if(m_modifiersInfo[i].name == name)
		{
			m_modifiersInfo.Delete(i);
			return;
		}
	}
}
void tcDebug::AddModifierRecord(atHashString name, int fileIdx)
{
	tcModifierEditRecord & record = m_modifiersInfo.Grow();
	record.name = name;
	record.fileIdx = fileIdx;
}

int tcDebug::FindModifierRecordIndex(atHashString name) const
{
	for(int i=0;i<m_modifiersInfo.GetCount();i++)
	{
		if( m_modifiersInfo[i].name == name )
			return i;
	}
	
	return -1;
}

void tcDebug::ModifierChanged()
{
	int modifierId = m_pManager->FindModifierIndex(m_modifiersInfo[m_modifierRecordEditId].name,"ModifierChanged");
	tcModifier* pModifier = m_pManager->GetModifier(modifierId);

	// Set file index.
	m_modifiersInfo[m_modifierRecordEditId].fileIdx = m_modifierEditState.fileIdx;
	pModifier->GetUserFlags() = m_modifierEditState.userFlags;
	
	// remove any modifier keyframe data that is now inactive
	for (int i=0; i<pModifier->GetModDataCount(); i++)
	{
		const tcModData* pModDataArray = pModifier->GetModDataArray();

		if (m_modifierEditState.var[pModDataArray[i].varId].isActive==false)
		{
			pModifier->RemoveModData(i);

			// an item has been removed from the array so decrement the index to reflect this
			i--;
		}
	}

	// copy any active keyframe state data back to the modifier
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		const tcModData* pModDataArray = pModifier->GetModDataArray();

		if (m_modifierEditState.var[varId].isActive)
		{
			// search to see if the modifier has this modifier data
			bool found = false;
			for (int j=0; j<pModifier->GetModDataCount(); j++)
			{
				if (pModDataArray[j].varId==varId)
				{
					found = true;
				
					pModifier->SetModDataVal(j, m_modifierEditState.var[varId].valA, m_modifierEditState.var[varId].valB);
				}
			}

			// add any un found modifier data
			if (!found)
			{
				pModifier->AddModData(varId, m_modifierEditState.var[varId].valA, m_modifierEditState.var[varId].valB);
			}
		}
	}

	// update the group fill colour
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		for (int j=0; j<MODMENU_NUM; j++)
		{
			if (m_modifierEditState.var[varId].isActive)
			{
				if (m_modifierEditState.var[varId].pWidgetActive[j])
				{
					m_modifierEditState.var[varId].pWidgetActive[j]->SetFillColor("ARGBColor:32:0:255:0");
				}

				if (m_modifierEditState.var[varId].pWidgetValA[j])
				{
					m_modifierEditState.var[varId].pWidgetValA[j]->SetFillColor("ARGBColor:32:0:255:0");
				}

				if (m_modifierEditState.var[varId].pWidgetValB[j])
				{
					m_modifierEditState.var[varId].pWidgetValB[j]->SetFillColor("ARGBColor:32:0:255:0");
				}
			}
			else
			{
				if (m_modifierEditState.var[varId].pWidgetActive[j])
				{
					m_modifierEditState.var[varId].pWidgetActive[j]->SetFillColor("ARGBColor:0:0:0:0");
				}

				if (m_modifierEditState.var[varId].pWidgetValA[j])
				{
					m_modifierEditState.var[varId].pWidgetValA[j]->SetFillColor("ARGBColor:0:0:0:0");
				}

				if (m_modifierEditState.var[varId].pWidgetValB[j])
				{
					m_modifierEditState.var[varId].pWidgetValB[j]->SetFillColor("ARGBColor:0:0:0:0");
				}
			}
		}
	}
}

void tcDebug::ModifierNameChanged()
{
	// Update Hash
	int modifierId = m_pManager->FindModifierIndex(m_modifiersInfo[m_modifierRecordEditId].name,"ModifierChanged");

	m_pManager->SetModifierName(modifierId,m_modifierEditState.modifierName);
	m_modifiersInfo[m_modifierRecordEditId].name.SetFromString(m_modifierEditState.modifierName);
	
	// Update combos
	UpdateModifierEditorWidgets();
}


void tcDebug::ModifierActiveToggled()
{
	// the modifier active flag has been toggled
	// if this is a red component on a colour that has been toggled then make sure the other components' flag matches
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);

		tcModifierType_e modType = varInfo.modType;
		if (modType!=MODTYPE_NONE)
		{
			tcVarType_e varType = varInfo.varType;
			if (varType == VARTYPE_COL3 || varType == VARTYPE_COL3_LIN)
			{
				m_modifierEditState.var[varId+1].isActive = m_modifierEditState.var[varId].isActive;
				m_modifierEditState.var[varId+2].isActive = m_modifierEditState.var[varId].isActive;
			}
			else if (varType == VARTYPE_COL4 || varType == VARTYPE_COL4_LIN)
			{
				m_modifierEditState.var[varId+1].isActive = m_modifierEditState.var[varId].isActive;
				m_modifierEditState.var[varId+2].isActive = m_modifierEditState.var[varId].isActive;
				m_modifierEditState.var[varId+3].isActive = m_modifierEditState.var[varId].isActive;
			}
		}
	}

	ModifierChanged();
}

void tcDebug::ModifierColChanged()
{	
	// the colour has been changed
	// copy this colour over into the valA variables 
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);

		tcModifierType_e modType = varInfo.modType;
		if (modType!=MODTYPE_NONE)
		{
			tcVarType_e varType = varInfo.varType;
			if (varType == VARTYPE_COL3 || varType == VARTYPE_COL3_LIN)
			{
				m_modifierEditState.var[varId+0].valA = m_modifierEditState.var[varId].colour.GetRedf();
				m_modifierEditState.var[varId+1].valA = m_modifierEditState.var[varId].colour.GetGreenf();
				m_modifierEditState.var[varId+2].valA = m_modifierEditState.var[varId].colour.GetBluef();
			}
			else if (varType == VARTYPE_COL4 || varType == VARTYPE_COL4_LIN)
			{
				m_modifierEditState.var[varId+0].valA = m_modifierEditState.var[varId].colour.GetRedf();
				m_modifierEditState.var[varId+1].valA = m_modifierEditState.var[varId].colour.GetGreenf();
				m_modifierEditState.var[varId+2].valA = m_modifierEditState.var[varId].colour.GetBluef();
				m_modifierEditState.var[varId+3].valA = m_modifierEditState.var[varId].colour.GetAlphaf();
			}
		}
	}

	ModifierChanged();
}

void tcDebug::ModifierColMultChanged()
{	
	// the colour multiplier has changed
	// this value only gets changed on the red component so we need to copy it to the other colour components
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);

		tcModifierType_e modType = varInfo.modType;
		if (modType==MODTYPE_INTERP_MULT)
		{
			tcVarType_e varType = varInfo.varType;
			if (varType == VARTYPE_COL3 || varType == VARTYPE_COL3_LIN)
			{
				m_modifierEditState.var[varId+1].valB = m_modifierEditState.var[varId].valB;
				m_modifierEditState.var[varId+2].valB = m_modifierEditState.var[varId].valB;
			}
			else if (varType == VARTYPE_COL4 || varType == VARTYPE_COL4_LIN)
			{
				m_modifierEditState.var[varId+1].valB = m_modifierEditState.var[varId].valB;
				m_modifierEditState.var[varId+2].valB = m_modifierEditState.var[varId].valB;
				m_modifierEditState.var[varId+3].valB = m_modifierEditState.var[varId].valB;
			}
		}
	}

	ModifierChanged();
}

void tcDebug::ModifierReloadData()
{
	// reload in the modifier data
	m_pManager->InitModifierData(true);
	m_modifiersInfo.Reset();
	
	for (int i=0; i<m_modifierFilesInfo.GetCount(); i++)
	{
		m_pManager->LoadModifierFile(m_modifierFilesInfo[i].name.GetCStr());
	}
	
	m_pManager->FinishModifierLoad();
	
	// Update editing data
	UpdateModifierEditorWidgets();

	ModifierRecordEditIdChanged();
}

void tcDebug::ModifierReloadDataSafeCurrent()
{
	// reload in the modifier data
	m_pManager->InitModifierData(true);
	m_modifiersInfo.Reset();
	
	for (int i=0; i<m_modifierFilesInfo.GetCount(); i++)
	{
		m_pManager->LoadModifierFile(m_modifierFilesInfo[i].name.GetCStr());
	}
	m_pManager->FinishModifierLoad();
	
	// Update editing data
	UpdateModifierEditorWidgets();

	// update the current edited modifier so write over the actual modifier data
	ModifierChanged();
	
	ModifierRecordEditIdChanged();
}

void tcDebug::ModifierSaveAll()
{
	for (int i=0; i<m_modifierFilesInfo.GetCount(); i++)
	{
		// save the data
		ModifierSaveFile(i);
	}
}

void tcDebug::ModifierSaveAllCheck()
{
	if (bkMessageBox("Are you sure?", "Save All TimeCycle Modifiers?", bkMsgYesNo, bkMsgQuestion))
	{	
		ModifierSaveAll();
	}
}

void tcDebug::ModifierSaveAllCopy()
{
	for (int i=0; i<m_modifierFilesInfo.GetCount(); i++)
	{
		// save the data
		ModifierSaveFile(i,true);
	}
}

u32 tcDebug::CalculateFileContentChecksum(const char* filename)
{
	u32 result = 0;
	
	fiStream *file = fiStream::Open(filename,true);
	if(file)
	{
			u32 data;
			const u32 confusion[4] = { 1845620387U, 2157105837U, 185693728U, 964928992U };
			int num = 0;

			while (file->Read((char *)&data, 4) == 4)
			{
				data ^= confusion[data & 3];

				int shift = num%29;
				data = (data << shift) | (data >> (32-shift));

				result ^= data;

				if ( ((num++) % 17) == 6)
				{
					result = (result << 16) | (result >> 16);
				}
			}

		file->Close();
	}
	
	return result;
}

void tcDebug::ModifierSaveFile(int fileIdx, bool copy)
{
	char txt[256];

	// create the tree
	parTree* pTree = rage_new parTree();

	// set up the root element
	parTreeNode* pRootNode = pTree->CreateRoot();
	parElement& rootElement = pRootNode->GetElement();
	rootElement.SetName("timecycle_modifier_data");

	rootElement.AddAttribute("version", TIMECYCLE_CURR_MODIFIER_FILE_VERSION, false);
	pTree->SetRoot(pRootNode);

	// loop through the modifiers
	for (int ii=0; ii<m_modifiersInfo.GetCount(); ii++)
	{
		if( m_modifiersInfo[ii].fileIdx == fileIdx )
		{
			const tcModifier* pModifier = m_pManager->FindModifier(m_modifiersInfo[ii].name,"ModifierSaveFile");

			parTreeNode* modifierNode = rage_new parTreeNode();
			modifierNode->GetElement().SetName("modifier");
			modifierNode->GetElement().AddAttribute("name", m_modifiersInfo[ii].name.GetCStr(), false, false);
			modifierNode->GetElement().AddAttribute("numMods", pModifier->GetModDataCount(), false);

			u32 size = 1;
			u32 *pUserFlags;
			atFixedBitSet<32> userFlags = pModifier->GetUserFlags();
			userFlags.GetRaw(&pUserFlags,&size);
			modifierNode->GetElement().AddAttribute("userFlags", (int)*pUserFlags, false);

			modifierNode->AppendAsChildOf(pRootNode);

			// add the modifier's keyframe data to the tree
			parTreeNode* pModifierVarNode = NULL;

			// go through the keyframe modifiers
			const tcModData* pModData = pModifier->GetModDataArray();
			for (int i=0; i<pModifier->GetModDataCount(); i++)
			{
				pModifierVarNode = rage_new parTreeNode();

				pModifierVarNode->GetElement().SetName(tcConfig::GetVarInfo(pModData[i].varId).name);

				sprintf(txt, "%.3f %.3f", pModData[i].valA, pModData[i].valB);

				pModifierVarNode->SetData(txt, (int)strlen(txt));
				pModifierVarNode->AppendAsChildOf(modifierNode);
			}
		}
	}

	// write the tree
	char filename[512];
	tcModifierFileInfo &modFile = m_modifierFilesInfo[fileIdx];
	
	bool doSave = false;
	
	if( copy )
	{
		snprintf(filename,512,"%s.copy.xml",modFile.name.GetCStr());
		doSave = true; // always overwrite copies.
	}
	else
	{
		snprintf(filename,512,"%s",modFile.name.GetCStr());
		
		// Check file changes
		u32 checksum = CalculateFileContentChecksum(modFile.name.GetCStr());
	
		if( checksum != modFile.checksum )
		{
			if (bkMessageBox("Are you sure?", "Local files have changed since last loaded, saving will overwrite any local changes", bkMsgOkCancel, bkMsgQuestion) == 1)
			{
				doSave = true;
			}
		}
		else
		{
			doSave = true;
		}
	}
	
	if( doSave )
	{
		if (PARSER.SaveTree(filename, "", pTree, parManager::XML)==false)
		{
			tcAssertf(false, "cannot open %s for writing - is it checked out?", modFile.name.GetCStr());	
		}
		else
		{
			// Update checksum
			u32 checksum = CalculateFileContentChecksum(modFile.name.GetCStr());
			modFile.checksum = checksum;
		}
	}
	
	delete pTree;
}

void tcDebug::ModifierCopyState(const tcModifierEditState& src, tcModifierEditState& dst)
{
	// copy keyframe data 
	if(dst.var.GetCapacity() != tcConfig::GetNumVars())
	{
		dst.var.Reset();
		dst.var.Reserve(tcConfig::GetNumVars());
		dst.var.Resize(tcConfig::GetNumVars());
	}
	
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		dst.var[varId].isActive	= src.var[varId].isActive;
		dst.var[varId].valA		= src.var[varId].valA;
		dst.var[varId].valB		= src.var[varId].valB;
		dst.var[varId].colour	= src.var[varId].colour;
	}
}

void tcDebug::ModifierAdd()
{
	tcModifier::ClearVarMapCache();
	tcModifier* mod = rage_new tcModifier();
	mod->SetName(atHashString("NewMod",0x867A9224));
	m_pManager->m_modifiersArray.PushAndGrow(mod);
	m_pManager->m_modifiersLookup.Insert(atHashString("NewMod",0x867A9224),mod);
	
	tcModifierEditRecord & record = m_modifiersInfo.Grow();
	record.name = atHashString("NewMod",0x867A9224);
	record.fileIdx = 0;
	
	m_modifierRecordEditId = m_modifiersInfo.GetCount() - 1;

	m_pManager->m_modifierStrengths.Grow();
	
	ModifierRecordEditIdChanged();
	UpdateModifierEditorWidgets();
}

void tcDebug::ModifierCopy()
{
	// copy data from the edit to copy state
	ModifierCopyState(m_modifierEditState, m_modifierCopyState);

	// we're now copied
	m_modifierCopyStateValid = true;
}

void tcDebug::ModifierPaste()
{
	if (m_modifierCopyStateValid)
	{
		// copy data from the copy to edit state
		ModifierCopyState(m_modifierCopyState, m_modifierEditState);											

		// update the current edited modifier so write over the actual modifier data
		ModifierChanged();
	}
}

void tcDebug::SelectClosestModifierBox()
{
	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	int closestIndex = 0;
	float closestDist = 500000.0f;

	for (int i = 0; i < pPool->GetSize(); i++)
	{
		tcBox* pBox = pPool->GetSlot(i);
		if (!pBox)
			continue;
		
		Vector3 center = VEC3V_TO_VECTOR3(pBox->GetBoundingBox().GetCenter());
		float dist = (center - m_cameraPosition).Mag();

		if( dist < closestDist )
		{
			closestIndex = i;
			closestDist = dist;
		}
	}

	m_modifierBoxEditId = closestIndex;

	tcBox* pBox = pPool->GetSlot(m_modifierBoxEditId);
	m_modifierEditStrength = pBox->GetStrength();
	m_modifierEditScale = m_modifierPrevScale = 1.0f;
	m_modifierEditRange = pBox->GetRange();
}

void tcDebug::SelectNextClosestModifierBox()
{
	if( m_modifierBoxEditId == -1 )
		return;

	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	tcBox* pCurrentBox = pPool->GetSlot( m_modifierBoxEditId );
	const Vector3 currentCenter = VEC3V_TO_VECTOR3(pCurrentBox->GetBoundingBox().GetCenter());
	const float currentDist = (currentCenter - m_cameraPosition).Mag();

	int nextClosestIndex = m_modifierBoxEditId;
	float nextClosestDist = -1.0f;

	for (int i = 0; i < pPool->GetSize(); i++)
	{
		if( m_modifierBoxEditId == i )
			continue;

		tcBox* pBox = pPool->GetSlot(i);
		if (!pBox)
			continue;
		
		Vector3 center = VEC3V_TO_VECTOR3(pBox->GetBoundingBox().GetCenter());
		float dist = (center - m_cameraPosition).Mag();

		if( dist < currentDist &&
			dist > nextClosestDist )
		{
			nextClosestIndex = i;
			nextClosestDist = dist;
		}
	}

	m_modifierBoxEditId = rage::Min( nextClosestIndex, (int)pPool->GetSize() );

	tcBox* pBox = pPool->GetSlot(m_modifierBoxEditId);
	m_modifierEditStrength = pBox->GetStrength();
	m_modifierEditScale = m_modifierPrevScale = 1.0f;
	m_modifierEditRange = pBox->GetRange();
}

void tcDebug::SelectNextFurthestModifierBox()
{
	if( m_modifierBoxEditId == -1 )
		return;

	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	tcBox* pCurrentBox = pPool->GetSlot( m_modifierBoxEditId );
	const Vector3 currentCenter = VEC3V_TO_VECTOR3(pCurrentBox->GetBoundingBox().GetCenter());
	const float currentDist = (currentCenter - m_cameraPosition).Mag();

	int nextFurthestIndex = m_modifierBoxEditId;
	float nextFurthestDist = 500000.0f; // some large value

	for (int i = 0; i < pPool->GetSize(); i++)
	{
		if( m_modifierBoxEditId == i )
			continue;

		tcBox* pBox = pPool->GetSlot(i);
		if (!pBox)
			continue;
		
		Vector3 center = VEC3V_TO_VECTOR3(pBox->GetBoundingBox().GetCenter());
		float dist = (center - m_cameraPosition).Mag();

		if( dist > currentDist &&
			dist < nextFurthestDist )
		{
			nextFurthestIndex = i;
			nextFurthestDist = dist;
		}
	}

	m_modifierBoxEditId = rage::Min( nextFurthestIndex, (int)pPool->GetSize() );

	tcBox* pBox = pPool->GetSlot(m_modifierBoxEditId);
	m_modifierEditStrength = pBox->GetStrength();
	m_modifierEditScale = m_modifierPrevScale = 1.0f;
	m_modifierEditRange = pBox->GetRange();
}

void tcDebug::InitModifierEditorWidgets()
{
	// remove any old widgets
	bool createOpen = false;
	if (m_pEditModDataGroup)
	{
		m_pBank->DeleteGroup(*m_pEditModDataGroup);
		m_pEditModDataGroup = NULL;
		createOpen = true;
	}

	bkGroup* pDebugGroups[TIMECYCLE_MAX_VARIABLE_GROUPS];	
	for (int i=0; i<TIMECYCLE_MAX_VARIABLE_GROUPS; i++)
	{
		pDebugGroups[i] = NULL;
	}

	bkGroup* pGroupBasic = NULL;
	bkGroup* pGroupSpecial = NULL;

	// create the new group
	tcAssertf(m_pEditModDataGroup==NULL, "modifier editor group already exists");
	m_pEditModDataGroup = m_pBank->PushGroup("Modifier Editor", createOpen);
	tcAssertf(m_pEditModDataGroup, "modifier editor group creation failed");
	{
		// edit controls
		m_pBank->PushGroup("Modifier Box Editing", false);
		{
			m_pBank->AddToggle("Render Selected Only", &m_renderSelectedOnly);
			m_pBank->AddToggle("No Depth Test", &m_noDepthTest);
			m_pBank->AddToggle("Render Modifier Boxes", &m_renderModifierBoxes);
			m_pBank->AddToggle("Render Modifier Name", &m_renderModifierNames);
			m_pBank->AddToggle("Render Modifier Box Names", &m_renderModifierBoxNames);
			m_pBank->AddButton("Select Closest Modifier Box", datCallback(CFA1(SelectClosestModifierBox)));
			m_pBank->AddButton("Select Next Closest Modifier Box", datCallback(CFA1(SelectNextClosestModifierBox)));
			m_pBank->AddButton("Select Next Furthest Modifier Box", datCallback(CFA1(SelectNextFurthestModifierBox)));
			m_pBank->AddSlider("Strength", &m_modifierEditStrength, 0.0f, 1.0f, 0.01f);
			m_pBank->AddSlider("Size/Scale", &m_modifierEditScale, 0.01f, 5.0f, 0.01f);
			m_pBank->AddSlider("Range", &m_modifierEditRange, 0.01f, 500.0f, 0.01f);

			m_modifierPrevScale = 1.0f;
		}
		m_pBank->PopGroup();

		m_pBank->PushGroup("Edit Controls", false);
		{
			m_pModifierSliderWidget = m_pBank->AddSlider("Modifier Id", &m_modifierRecordEditId, 0, m_pManager->GetModifiersCount()-1, 1, datCallback(CFA1(ModifierRecordEditIdChanged)));
			m_pModifierComboWidget = m_pBank->AddCombo("Modifier",&m_modifierRecordEditId,0,NULL,datCallback(CFA1(ModifierRecordEditIdChanged)));

			m_pBank->AddToggle("Apply", &m_modifierEditApply);

			m_pBank->AddSeparator();
			
			m_pBank->AddButton("New", datCallback(CFA1(ModifierAdd)),"");
			m_pBank->AddButton("Copy", datCallback(CFA1(ModifierCopy)),"");
			m_pBank->AddButton("Paste", datCallback(CFA1(ModifierPaste)),"");

			m_pBank->AddSeparator();

			m_pBank->AddButton("Reload All Modifiers", datCallback(CFA1(ModifierReloadData)), "");
			m_pBank->AddButton("Reload All Modifier But Current", datCallback(CFA1(ModifierReloadDataSafeCurrent)), "");
			m_pBank->AddButton("Save All Modifiers", datCallback(CFA1(ModifierSaveAllCheck)), "");
			m_pBank->AddButton("Save All Modifiers As Copy", datCallback(CFA1(ModifierSaveAllCopy)), "");
		}
		m_pBank->PopGroup();

		m_pBank->PushGroup("Data");
		{
			m_pModifierNameWidget = m_pBank->AddText("Name", m_modifierEditState.modifierName, TIMECYCLE_MAX_MODIFIER_NAME_LEN,datCallback(CFA1(ModifierNameChanged)));
			m_pModifierFileWidget = m_pBank->AddCombo("File",&m_modifierEditState.fileIdx,0,NULL,datCallback(CFA1(ModifierChanged)));

			// basic data
			pGroupBasic = m_pBank->PushGroup("Basic", createOpen);
			{	
			}
			m_pBank->PopGroup();

			// advanced data
			m_pBank->PushGroup("Advanced", createOpen);
			{	
				u32 size = 1;
				u32 *pUserFlags;
				m_modifierEditState.userFlags.GetRaw(&pUserFlags,&size);
				
				for(int i=0;i<m_userFlagsNameList.GetCount();i++)
				{
					m_pBank->AddToggle(m_userFlagsNameList[i],pUserFlags,1U<<i,datCallback(CFA1(ModifierChanged)));
				}
				
				for (int i=0; i<m_debugGroupNames.GetCount(); i++)
				{
					pDebugGroups[i] = m_pBank->PushGroup(m_debugGroupNames[i], false);
					m_pBank->PopGroup();
				}
			}
			m_pBank->PopGroup();

			// special data
			pGroupSpecial = m_pBank->PushGroup("Special", createOpen);
			{	
			}
			m_pBank->PopGroup();

		}
		m_pBank->PopGroup();
	}
	m_pBank->PopGroup();

	// add the keyframe modifier data
	ModifierInit();

	// Setup Combos
	UpdateModifierEditorWidgets();

	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		// set the correct group
		bkGroup* pCurrGroup = NULL;

		const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);

		const char* debugGroupName = varInfo.debugGroupName;
		
		for (int j=0; j<m_debugGroupNames.GetCount(); j++)
		{
			if (strcmp(debugGroupName, m_debugGroupNames[j])==0)
			{
				pCurrGroup = pDebugGroups[j];
			}
		}

		tcAssertf(pCurrGroup, "Unknown debug group %s", debugGroupName);
	
		if (pCurrGroup)
		{
			tcModifierType_e modType = varInfo.modType;
			tcVarType_e varType = varInfo.varType;
			tcModifierMenu_e modMenu = varInfo.debugModMenu;
			if (modType!=MODTYPE_NONE && varType!=VARTYPE_NONE)
			{
				if (modMenu==MODMENU_SPECIAL)
				{
					m_pBank->SetCurrentGroup(*pGroupSpecial);
					{
						ModifierAddVarToMenu(MODMENU_SPECIAL, varId);
					}
					m_pBank->UnSetCurrentGroup(*pGroupSpecial);
				}
				else
				{
					// add to the global group
					m_pBank->SetCurrentGroup(*pCurrGroup);
					{
						ModifierAddVarToMenu(MODMENU_DEFAULT, varId);
					}
					m_pBank->UnSetCurrentGroup(*pCurrGroup);

					// add to the basic group
					if (modMenu==MODMENU_BASIC)
					{
						m_pBank->SetCurrentGroup(*pGroupBasic);
						{
							ModifierAddVarToMenu(MODMENU_BASIC, varId);
						}
						m_pBank->UnSetCurrentGroup(*pGroupBasic);
					}
				}
			}
		}
	}

	// Remove empty groups
	for (int i=0; i<m_debugGroupNames.GetCount(); i++)
	{
		bkWidget *child = pDebugGroups[i]->GetChild();
		
		if( child == NULL )
		{
			pDebugGroups[i]->Destroy();
		}
	}

	ModifierChanged();
}

void tcDebug::UpdateModifierEditorWidgets()
{
	if( m_pModifierFileWidget )
	{
		int fileCount = m_modifierFilesInfo.GetCount();
		atArray<const char*> nameList(fileCount,fileCount);
		for (int i = 0; i < fileCount; ++i)
		{
			nameList[i] = m_modifierFilesInfo[i].name.GetCStr();
		}
		m_pModifierFileWidget->UpdateCombo("File",&m_modifierEditState.fileIdx,nameList.GetCount(),nameList.GetElements(),datCallback(CFA1(ModifierChanged)));
	}
	
	
	if( m_pModifierSliderWidget )
	{
		m_pModifierSliderWidget->SetRange(0.0f,(float)(m_pManager->GetModifiersCount()-1.0f));
	}
	
	if( m_pModifierComboWidget )
	{
		int modCount = m_pManager->GetModifiersCount();
		atArray<const char*> nameList(modCount,modCount);
		for(int i=0;i<modCount;i++)
		{
			nameList[i] = m_modifiersInfo[i].name.GetCStr();
		}
		m_pModifierComboWidget->UpdateCombo("Modifier",&m_modifierRecordEditId,nameList.GetCount(),nameList.GetElements(),datCallback(CFA1(ModifierRecordEditIdChanged)));
	}
}

void tcDebug::ModifierAddVarToMenu(const tcModifierMenu_e menuId, const int varId)
{
	const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);
	tcModifierType_e modType = varInfo.modType;
	tcVarType_e varType = varInfo.varType;

	char txtVarName[128];
	char txtVarActive[128];
	char txtVarMult[128];
	char txtVarMin[128];
	char txtVarMax[128];

	sprintf(txtVarName, "%s", varInfo.debugName);
	sprintf(txtVarActive, "%s Active", varInfo.debugName);
	sprintf(txtVarMult, "%s Multiplier", varInfo.debugName);
	sprintf(txtVarMin, "%s Min", varInfo.debugName);
	sprintf(txtVarMax, "%s Max", varInfo.debugName);

	m_modifierEditState.var[varId].pWidgetActive[menuId] = m_pBank->AddToggle("Active", &m_modifierEditState.var[varId].isActive, ModifierActiveToggled);


	if (modType==MODTYPE_INTERP || modType==MODTYPE_MAX)
	{
		if (varType==VARTYPE_FLOAT)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarName, &m_modifierEditState.var[varId].valA, varInfo.debugMin, varInfo.debugMax, varInfo.debugDelta, ModifierChanged);
		}
		else if (varType==VARTYPE_COL3 || varType==VARTYPE_COL3_LIN)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddColor(txtVarName, &m_modifierEditState.var[varId].colour, ModifierColChanged, 0, NULL, (varType == VARTYPE_COL3_LIN));
		}
		else if (varType==VARTYPE_COL4 || varType==VARTYPE_COL4_LIN)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddColor(txtVarName, &m_modifierEditState.var[varId].colour, ModifierColChanged, 0, NULL, (varType == VARTYPE_COL4_LIN));
		}
	}
	else if (modType==MODTYPE_INTERP_MULT)
	{
		if (varType==VARTYPE_FLOAT)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarName, &m_modifierEditState.var[varId].valA, varInfo.debugMin, varInfo.debugMax, varInfo.debugDelta, ModifierChanged);
			m_modifierEditState.var[varId].pWidgetValB[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarMult, &m_modifierEditState.var[varId].valB, 0.0f, 1.0f, 0.01f, ModifierChanged);
		}
		else if (varType==VARTYPE_COL3 || varType==VARTYPE_COL3_LIN)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddColor(txtVarName, &m_modifierEditState.var[varId].colour, ModifierColChanged, 0, NULL, (varType == VARTYPE_COL3_LIN));
			m_modifierEditState.var[varId].pWidgetValB[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarMult, &m_modifierEditState.var[varId].valB, 0.0f, 1.0f, 0.01f, ModifierColMultChanged);
		}
		else if (varType==VARTYPE_COL4 || varType==VARTYPE_COL4_LIN)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddColor(txtVarName, &m_modifierEditState.var[varId].colour, ModifierColChanged, 0, NULL, (varType == VARTYPE_COL4_LIN));
			m_modifierEditState.var[varId].pWidgetValB[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarMult, &m_modifierEditState.var[varId].valB, 0.0f, 1.0f, 0.01f, ModifierColMultChanged);
		}
	}
	else if (modType==MODTYPE_CLAMP)
	{
		if (varType==VARTYPE_FLOAT)
		{
			m_modifierEditState.var[varId].pWidgetValA[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarMin, &m_modifierEditState.var[varId].valA, varInfo.debugMin, varInfo.debugMax, varInfo.debugDelta, ModifierChanged);
			m_modifierEditState.var[varId].pWidgetValB[menuId] = (bkWidget*)m_pBank->AddSlider(txtVarMax, &m_modifierEditState.var[varId].valB, varInfo.debugMin, varInfo.debugMax, varInfo.debugDelta, ModifierChanged);
		}
	}

	m_pBank->AddSeparator();
}

void tcDebug::InitCycleEditorWidgets()
{
	bkGroup* pDebugGroups[TIMECYCLE_MAX_VARIABLE_GROUPS];	
	for (int i=0; i<TIMECYCLE_MAX_VARIABLE_GROUPS; i++)
	{
		pDebugGroups[i] = NULL;
	}

	const char* debugGroupNames[TIMECYCLE_MAX_VARIABLE_GROUPS];
	for (int i=0; i<m_debugGroupNames.GetCount(); i++)
	{
		debugGroupNames[i] = m_debugGroupNames[i];
	}

	const char* cycleNames[TIMECYCLE_MAX_CYCLES];
	for (int i=0; i<tcConfig::GetNumCycles(); i++)
	{
		cycleNames[i] = m_pManager->GetCycle(i).GetName().GetCStr();
	}

	const char* regionNames[TIMECYCLE_MAX_REGIONS];
	for (int i=0; i<tcConfig::GetNumRegions(); i++)
	{
		regionNames[i] = m_debugRegionNames[i];
	}

	const char* timeSampleNames[TIMECYCLE_MAX_TIME_SAMPLES];
	for (int i=0; i<tcConfig::GetNumTimeSamples(); i++)
	{
		timeSampleNames[i] = tcConfig::GetTimeSampleInfo(i).name;
	}

	m_pCycleEditorGroup = m_pBank->PushGroup("Cycle Editor", false);
	{
		// edit controls
		m_pBank->PushGroup("Edit Controls", false);
		{
			m_pBank->AddToggle("Edit", &m_keyframeEditActive, datCallback(CFA1(KeyframeEditStateChanged)), "");
			m_pBank->AddToggle("Override With Edit Light Direction", &m_keyframeEditOverrideLightDirection);
			m_pBank->AddToggle("Override With Edit Time Sample", &m_keyframeEditOverrideTimeSample);
			m_pBank->AddToggle("Override With Edit Region", &m_keyframeEditOverrideRegion);
			m_pBank->AddToggle("Override With Edit Cycle", &m_keyframeEditOverrideCycle);
			m_pBank->AddCombo("Cycle", (int*)(&m_keyframeEditCycleIndex), tcConfig::GetNumCycles(), cycleNames, datCallback(CFA1(KeyframeEditStateChanged)));
			m_pBank->AddCombo("Region", (int*)(&m_keyframeEditRegionIndex), tcConfig::GetNumRegions(), regionNames, datCallback(CFA1(KeyframeEditStateChanged)));
			m_pBank->AddCombo("Time Sample", &m_keyframeEditTimeSampleIndex, tcConfig::GetNumTimeSamples(), timeSampleNames, datCallback(CFA1(KeyframeEditStateChanged)));
		}
		m_pBank->PopGroup();

		// copy / paste
		m_pBank->PushGroup("Copy/Paste", false);
		{
			m_pBank->AddButton("Copy", datCallback(CFA1(KeyframeCopyData)),"");
			m_pBank->AddButton("Paste", datCallback(CFA1(KeyframePasteData)),"");
			m_pBank->AddButton("Paste Group", datCallback(CFA1(KeyframePasteGroup)),"");
			m_pBank->AddCombo("Current Paste Group", &m_keyframeCurrPasteGroup, m_debugGroupNames.GetCount(), debugGroupNames, 0);
			//m_pBank->AddButton("Undo Paste", datCallback(CFA1(KeyframePasteUndo)),"");
		}
		m_pBank->PopGroup();

		// data file
		m_pBank->PushGroup("Data File", false);
		{
			m_pBank->AddButton("Save Keyframes", datCallback(CFA1(KeyframeSaveData)),"");
			m_pBank->AddButton("Reload Keyframes", datCallback(CFA1(KeyframeReloadData)));
		}
		m_pBank->PopGroup();

		// settings
		m_pBank->PushGroup("Settings", false);
		{
			for (int i=0; i<m_debugGroupNames.GetCount(); i++)
			{
				pDebugGroups[i] = m_pBank->PushGroup(m_debugGroupNames[i], false);
				m_pBank->PopGroup();
			}
		}
		m_pBank->PopGroup();

	}
	m_pBank->PopGroup();

	ChangeGraphs();

	// add the keyframe debug widgets to the correct groups
	for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
	{
		const tcVarInfo& varInfo = tcConfig::GetVarInfo(varId);

		#if __BANK
		if (varInfo.debugModMenu == MODMENU_SPECIAL)
		{
			continue;
		}
		#endif

		tcVarType_e varType = varInfo.varType;
		if (varType!=VARTYPE_NONE)
		{
			// set the correct group
			bkGroup* pCurrGroup = NULL;

			const char* debugGroupName = varInfo.debugGroupName;

			for (int j=0; j<m_debugGroupNames.GetCount(); j++)
			{
				if (strcmp(debugGroupName, m_debugGroupNames[j])==0)
				{
					pCurrGroup = pDebugGroups[j];
				}
			}

			tcAssertf(pCurrGroup, "Unknown debug group %s", debugGroupName);

			// check we have a valid group to add the widget to
			if (pCurrGroup)
			{
				// set the current group 
				m_pBank->SetCurrentGroup(*pCurrGroup);

				// add the debug widget
				if (varType==VARTYPE_FLOAT)
				{
					float min = varInfo.debugMin; 
					float max = varInfo.debugMax; 
					float delta = varInfo.debugDelta;
					m_pBank->AddSlider(varInfo.debugName, m_keyframeEditData.GetVarPtr(varId), min, max, delta, datCallback(CFA1(RequestFloatGraphUpdate)));
				}
				else if (varType==VARTYPE_COL3 || varType==VARTYPE_COL3_LIN)
				{
					m_pBank->AddColor(varInfo.debugName, m_keyframeEditData.GetVarPtr(varId), 3, datCallback(CFA1(RequestColorGraphUpdate)), 0, NULL, (varType == VARTYPE_COL3_LIN));
				}
				else if (varType==VARTYPE_COL4 || varType==VARTYPE_COL4_LIN)
				{
					m_pBank->AddColor(varInfo.debugName, m_keyframeEditData.GetVarPtr(varId), 4, datCallback(CFA1(RequestColorGraphUpdate)), 0, NULL, (varType == VARTYPE_COL4_LIN));
				}

				// unset the correct group
				m_pBank->UnSetCurrentGroup(*pCurrGroup);
			}
		}
	}
}

void tcDebug::InitWidgets()
{
	// destroy any previous timecycle bank
	m_pBank = BANKMGR.FindBank("TimeCycle");
	if (m_pBank)
	{
		BANKMGR.DestroyBank(*m_pBank);
		m_pBank = NULL;
	}

	// create a new bank widget
	tcAssertf(m_pBank==NULL, "the timecycle bank widget already exists");
	m_pBank = &BANKMGR.CreateBank("TimeCycle", 0, 0, false);
}

void tcDebug::RefreshTCWidgets()
{
	UpdateModifierEditorWidgets();
}

void tcDebug::CreateWidgets()
{
	// game widget group
	m_pGameGroup = m_pBank->PushGroup("Game", false);
	m_pBank->PopGroup();

	// framework widgets
	m_pBank->PushGroup("Debug", false);
	{
		m_pBank->AddToggle("No Depth Test", &m_noDepthTest);
		m_pBank->AddToggle("Solid Boxes", &m_solidBoxes);
		m_pBank->AddSlider("Solid Boxes Alpha", &m_boxesAlpha, 0, 255, 1);
		m_pBank->AddToggle("Render Region Boxes", &m_renderRegionBoxes);
		m_pBank->AddToggle("Render Modifier Boxes", &m_renderModifierBoxes);
		m_pBank->AddToggle("Render Modifier Name", &m_renderModifierNames);
		m_pBank->AddToggle("Render Modifier Box Names", &m_renderModifierBoxNames);
	}
	m_pBank->PopGroup();

	// keyframe editor
	InitCycleEditorWidgets();

	// modifier editor
	InitModifierEditorWidgets();
}

void tcDebug::UpdateModifierEditor() 
{
	if( m_modifierBoxEditId == -1 )
		return;

	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	tcBox* pBox = pPool->GetSlot( m_modifierBoxEditId );
	if( !pBox )
		return;

	if( pBox->GetStrength() != m_modifierEditStrength ||
		pBox->GetRange() != m_modifierEditRange ||
		m_modifierPrevScale != m_modifierEditScale )
	{
		// if anything has changed, update the box and notify we need to rebuild the graph
		pBox->SetStrength( m_modifierEditStrength );
		pBox->SetRange( m_modifierEditRange );

		spdAABB box( pBox->GetBoundingBox() );
		Vector3 center = VEC3V_TO_VECTOR3(box.GetCenter());
		const Vector3 min = box.GetMinVector3();
		const Vector3 max = box.GetMaxVector3();
		
		float scale = m_modifierEditScale / m_modifierPrevScale;

		box.SetMinPreserveUserData( VECTOR3_TO_VEC3V(((min - center) * scale) + center) );
		box.SetMaxPreserveUserData( VECTOR3_TO_VEC3V(((max - center) * scale) + center) );

		pBox->SetBoundingBox( box );

		RequestFloatGraphUpdate();
		RequestColorGraphUpdate();
		RequestGraphChange();

		m_modifierPrevScale = m_modifierEditScale;
	}
}

} // namespace rage

#endif // #if TC_DEBUG
