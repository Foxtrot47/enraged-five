//
// timecycle/tcboxtree.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//


// includes
#include "timecycle/tcboxtree.h"

// includes (rage)
#include "vectormath/legacyconvert.h"

// includes (framework)
#include "timecycle/optimisations.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()

static const int UseTreeThreshold = 32; // should it be more ? less ?

#define MAX_TCBOXTREE_SEARCH_RESULTS	(TIMECYCLE_MAX_MODIFIER_BOX_QUERY + MNBVH_NODESIZE)

// namespaces
namespace rage {


// code
void tcBoxTree::Reset()
{
	m_list.Reset();
	m_useTree = false;
	if(m_pTree)
	{
		delete m_pTree;
		m_pTree = NULL;
	}
}

void tcBoxTree::Init(int numNodes)
{
	m_list.Reset();
	m_list.Reserve(numNodes);
	if(m_pTree)
	{
		delete m_pTree;
		m_pTree = NULL;
	}
	m_useTree = false;
}

void tcBoxTree::Insert(const spdAABB& bBox, float range, int index)
{
	spdAABB box = bBox;
	box.GrowUniform(ScalarV(range));

	// Go native
	spdMNBVHBounds bound;
	bound.m_bmin = box.GetMin();
	bound.m_bmax = box.GetMax();
	bound.m_data = (void*) (ptrdiff_t) (index << 2);	//NB: mnbvh makes use of LSB internally
	m_list.Push(bound);
}

void tcBoxTree::BuildTree()
{
	m_useTree = m_list.GetCount() > UseTreeThreshold;

	if (m_useTree)
	{
		Assertf(!m_pTree, "Tree already allocated");
		m_pTree = rage_new MNBVHSimpleInterface(m_list);
		// Only need to keep the list for debug purposes.
	}
}

void tcBoxTree::GetIndices(Vec3V_In vPos, float extent, int* indices, int maxfound, int& numfound)
{
	spdAABB box(vPos,vPos);
	box.GrowUniform(ScalarV(extent));	

	if ( m_useTree )
	{	
		void* aBoxSearchResults[MAX_TCBOXTREE_SEARCH_RESULTS];
		int resultCount = m_pTree->PerformBoxSearch(box, aBoxSearchResults, MAX_TCBOXTREE_SEARCH_RESULTS);
		numfound = Min(resultCount, maxfound);
		for (s32 j=0; j<numfound; j++)
		{
			s32 index = m_pTree->GetSearchResult(aBoxSearchResults, j);
			indices[j] = index;
		}
	}
	else
	{
		numfound = 0;
		int listCount = m_list.GetCount();
		for(int i = 0; i < listCount; i++)
		{
			if(numfound < maxfound)
			{
				spdMNBVHBounds& bound = m_list[i];
				spdAABB aabb(bound.m_bmin, bound.m_bmax);
				int increment = aabb.IntersectsAABB(box);
				indices[numfound] = ( (s32)(ptrdiff_t)bound.m_data >> 2 );
				numfound += increment;
			}
		}
	}

#if __ASSERT
	if( numfound == maxfound )
	{
		static int maxIntersection = 0;

		int count = 0;
		int listCount = m_list.GetCount();
		for(int i = 0; i < listCount; i++)
		{
			spdMNBVHBounds& bound = m_list[i];
			spdAABB aabb(bound.m_bmin, bound.m_bmax);
			int increment = aabb.IntersectsAABB(box);
			count += increment;
		}

		if( count > maxIntersection )
		{
			maxIntersection = count;
			Warningf("Out of result space in tcBoxTree::GetIndices : Found %d indices for %d slots",maxIntersection,maxfound);
		}
	}
#endif	
}

} // namespace rage

