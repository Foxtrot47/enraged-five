//
// timecycle/tccycle.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef TC_CYCLE_H
#define TC_CYCLE_H


#include "atl/array.h"
#include "atl/hashstring.h"

// includes (framework)
#include "timecycle/tcconfig.h"
#include "timecycle/tckeyframe.h"


// namespaces 
namespace rage {


// classes

// PURPOSE
class tcCycle
{
	// friends
	friend class tcDebug;

	// functions (public)
	public: 

		// PURPOSE
		//  initialises the cycle 
		// PARAMS
		//  pFilename - the name of the file from which this cycle's data is to be loaded 
		void Init(const char* pFilename);

		// PURPOSE
		//  gets access to a region's keyframe in this cycle.
		// PARAMS
		//  regionIndex - the index of the keyframe's region
		//  timeSampleIndex - the index of the keyframe's time sample 
		// RETURNS
		//  the keyframe data at this required index of this cycle
		const tcKeyframe& GetKeyframe(u32 regionIndex, u32 timeSampleIndex) const { return m_keyframes[regionIndex][timeSampleIndex]; }

		// PURPOSE
		//  sets the particular keyframe in this cycle
		// PARAMS
		//  regionIndex - the index of the keyframe's region
		//  timeSampleIndex - the index of the keyframe's time sample 
		//  keyframe - the keyframe data to set
		void SetKeyframe(u32 regionIndex, u32 timeSampleIndex, const tcKeyframe& keyframe) { m_keyframes[regionIndex][timeSampleIndex] = keyframe; }

		// PURPOSE
		//  gets the name of this cycle
		// RETURNS
		//  the name of this cycle
		const atHashWithStringBank &GetName() const { return m_name; }

	// functions (private)
	private: 

		// PURPOSE
		//  loads in the cycle data from a file
		// PARAMS
		//  pFilename - the name of the cycle file to load
		void Load(const char* pFilename);
			
	// variables (private)
	private: 

		// keyframe data
		atMultiRangeArray<tcKeyframe,TIMECYCLE_MAX_REGIONS,TIMECYCLE_MAX_TIME_SAMPLES> m_keyframes;			// array of keyframe data that this cycle contains per region

		// identification
		atHashWithStringBank m_name;
};


} // namespace rage


#endif // TC_CYCLE_H


