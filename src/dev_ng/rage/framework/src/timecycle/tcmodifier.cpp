//
// timecycle/tcmodifier.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//


// includes
#include "timecycle/tcmodifier.h"

// includes (rage)
#include "math/amath.h"
#include "system/criticalsection.h"
#include "system/memops.h"
#include "system/threadtype.h"
#include "grcore/debugdraw.h"

// includes (framework)
#include "timecycle/tcconfig.h"
#include "timecycle/optimisations.h"
#include "timecycle/tckeyframe.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()


// namespaces
namespace rage {

// Number of concurrent variable look-up maps. Each one costs ([max number of variables] * sizeof(tcModData *))
// bytes. Not having enough will cause bad cache thrashing when multiple modifiers are accessed every frame.
const int VAR_ID_CACHE_SIZE = 6;

struct VarIdLruCacheEntry {
	// Modifier that this cache entry is for
	tcModifier *m_Modifier;

	// The actual cache map
	atArray<tcModData *> m_VarIdToDataMap;

	// Reference count - cannot be evicted while this is 1
	u32 m_RefCount;
};

static VarIdLruCacheEntry s_CacheEntries[VAR_ID_CACHE_SIZE];

// This is the index of the next cache entry when allocating a new one
static int s_NextCacheEntry = 0;

static bool s_OnlyMainThreadIsUpdating = true;
sysCriticalSectionToken s_VarMapCriticalSectionToken;

tcModifier::tcModifier()
: m_VarIdToDataMap(NULL)
, m_IndexOfCacheEntry(-1)
{
}

tcModifier::~tcModifier()
{
	RemoveVarMap();
}


// code
void tcModifier::Init(const tcKeyframeUncompressed& keyframe)
{
	Init();

	// copy the keyframe data into the modifier 
	for (int i=0; i<tcConfig::GetNumVars(); i++)
	{
		tcModifierType_e modType = tcConfig::GetVarInfo(i).modType;
		if (modType==MODTYPE_INTERP || modType==MODTYPE_MAX)
		{
			AddModData(i, keyframe.GetVar(i), 0.0f);
		}
		else if (modType==MODTYPE_INTERP_MULT)
		{
			AddModData(i, keyframe.GetVar(i), 1.0f);
		}
		else if (modType==MODTYPE_CLAMP)
		{
			AddModData(i, keyframe.GetVar(i), keyframe.GetVar(i));
		}
	}
}

void tcModifier::Init(const tcModifier& modifier)
{
	Init();

	const tcModData* modData = modifier.GetModDataArray();
	int modDataCount = modifier.GetModDataCount();

	for (int i = 0; i < modDataCount; i++)
	{
		AddModData(modData[i].varId, modData[i].valA, modData[i].valB);
	}
}

void tcModifier::ResetModData()
{
	m_userFlags.Reset();
	m_modData.Resize(0);
	RemoveVarMap();
}

void tcModifier::CreateAndLockVarMap() const
{
	Assert(sysThreadType::IsUpdateThread() || !s_OnlyMainThreadIsUpdating);

	if(!s_OnlyMainThreadIsUpdating)
	{
		s_VarMapCriticalSectionToken.Lock();
	}

	if (!HasVarMap())
	{
		AllocateVarMap();
	}

	LookupCacheEntry()->m_RefCount++;

	if(!s_OnlyMainThreadIsUpdating)
	{
		s_VarMapCriticalSectionToken.Unlock();
	}
}

void tcModifier::UnlockVarMap() const
{
	if(!s_OnlyMainThreadIsUpdating)
	{
		s_VarMapCriticalSectionToken.Lock();
	}

	VarIdLruCacheEntry *cacheEntry = LookupCacheEntry();
	Assert(cacheEntry);
	Assert(cacheEntry->m_RefCount > 0);
	
	cacheEntry->m_RefCount--;

	if(!s_OnlyMainThreadIsUpdating)
	{
		s_VarMapCriticalSectionToken.Unlock();
	}
}

void tcModifier::Blend(const tcModifier& mod, const float t)
{
	tcAssertf(t>=0.0f && t<=1.0f, "invalid blend amount passed in");

	if (t>0.0f)
	{
		float tInv = 1.0f-t;

		// blend the modifier's keyframe data into this modifier
		const tcModData* modData = mod.GetModDataArray();
		int modDataCount = mod.GetModDataCount();

		CreateAndLockVarMap();
		mod.CreateAndLockVarMap();

		for (int i=0; i<modDataCount; i++)
		{
			// get the keyframe variable to blend
			int varId = modData[i].varId;
		
			// check if this modifier has this keyframe modifier
			int modId = GetModDataIdUnsafe(varId);
			if (modId>=0)
			{
				// it does - do the blend
				tcModifierType_e modType = tcConfig::GetVarInfo(varId).modType;
				m_modData[modId].Blend(modData[i],modType,t,tInv);
			}
		}

		UnlockVarMap();
		mod.UnlockVarMap();
	}
}

void tcModifier::Blend(const tcModifier& mod, const tcModifier& ovr, const float t)
{
	tcAssertf(t>=0.0f && t<=1.0f, "invalid blend amount passed in");

	if (t>0.0f)
	{
		float tInv = 1.0f-t;

		// blend the modifier's keyframe data into this modifier
		int modDataCount = GetModDataCount();

		CreateAndLockVarMap();
		mod.CreateAndLockVarMap();
		ovr.CreateAndLockVarMap();
		
		for (int i=0; i<modDataCount; i++)
		{
			// get the keyframe variable to blend
			int varId = m_modData[i].varId;
		
			// check if this modifier has this keyframe modifier
			int modId = GetModDataIdUnsafe(varId);
			if (modId>=0)
			{
				// either use the override, then the source mod or nothing.
				const tcModData* modData = mod.GetModDataUnsafe(varId);
				const tcModData* ovrData = ovr.GetModDataUnsafe(varId);
				const tcModData* finData = ovrData ? ovrData : modData;
				
				if (finData)
				{
					tcModifierType_e modType = tcConfig::GetVarInfo(varId).modType;
					m_modData[modId].Blend(*finData,modType,t,tInv);
				}
			}
		}

		UnlockVarMap();
		mod.UnlockVarMap();
		ovr.UnlockVarMap();
	}
}

void tcModifier::Apply(tcKeyframeUncompressed& keyframe, const float t) const
{
	tcAssertf(t>=0.0f && t<=1.0f, "invalid blend amount passed in");

	if (t>0.0f)
	{
		float tInv = 1.0f-t;

		const tcModData* pModDataArray = GetModDataArray();

		for (int i=0; i<GetModDataCount(); i++)
		{
			int varId = pModDataArray[i].varId;
			float var = keyframe.GetVar(varId);

			tcModifierType_e modType = tcConfig::GetVarInfo(varId).modType;
			if (modType==MODTYPE_INTERP)
			{
				keyframe.SetVar(varId, tInv*var + t*pModDataArray[i].valA);
			}
			else if (modType==MODTYPE_INTERP_MULT)
			{
				float localI = pModDataArray[i].valB * t;
				float localcI = 1.0f - localI;

				keyframe.SetVar(varId, localcI*var + localI*pModDataArray[i].valA);
			}
			else if (modType==MODTYPE_CLAMP)
			{
				if (var<pModDataArray[i].valA)
				{
					keyframe.SetVar(varId, tInv*var + t*pModDataArray[i].valA);
				}

				if (var>pModDataArray[i].valB)
				{
					keyframe.SetVar(varId, tInv*var + t*pModDataArray[i].valB);
				}
			}
			else if (modType==MODTYPE_MAX)
			{
				keyframe.SetVar(varId, Max(tInv*var, t*pModDataArray[i].valA));
			}
		}
	}
}

#if  __BANK
void tcModifier::Dump() const
{
	const tcModData* pModDataArray = GetModDataArray();

	for (int i=0; i<GetModDataCount(); i++)
	{
		int varId = pModDataArray[i].varId;

		const char *name = tcConfig::GetVarInfo(varId).name;

		Displayf("%s\t%.9f\t%.9f",name,pModDataArray[i].valA,pModDataArray[i].valB);
	}
}
#endif

void tcModifier::AddModData(const int varId, const float valA, const float valB) 
{
	// grow the array if more space is needed
	tcModData* pModData = NULL;
	pModData = &m_modData.Grow();

	// add the modifier data
	pModData->varId = varId;
	pModData->valA = valA;
	pModData->valB = valB;

	RemoveVarMap();
}


void tcModifier::RemoveModData(int idx)
{
	if (tcVerifyf(idx>=0 && idx<m_modData.GetCount(), "index out of range (%d, %d)", idx, m_modData.GetCount())) 
	{
		m_modData.Delete(idx);
	}

	RemoveVarMap();
}


VarIdLruCacheEntry *tcModifier::LookupCacheEntry() const
{
	if(m_IndexOfCacheEntry == -1)
	{
		return NULL;
	}

	Assert(s_CacheEntries[m_IndexOfCacheEntry].m_Modifier == this);
	return &s_CacheEntries[m_IndexOfCacheEntry];
}

#if __BANK
static int maxVarMapCacheUse = 0;
static bool collectVarMapCacheUse = false;
void tcModifier::ShowMeVarMapCacheUse()
{
	Assert(sysThreadType::IsUpdateThread());

	collectVarMapCacheUse = true;
		
	for(int i=0;i<VAR_ID_CACHE_SIZE;i++)
	{
		grcDebugDraw::AddDebugOutput("%d: %p %d",i,s_CacheEntries[i].m_Modifier,s_CacheEntries[i].m_RefCount);
	}
	grcDebugDraw::AddDebugOutput("%d - %d - %d",s_NextCacheEntry,maxVarMapCacheUse);
}

void tcModifier::ClearVarMapCache()
{
	Assert(sysThreadType::IsUpdateThread());

	for(int i=0;i<VAR_ID_CACHE_SIZE;i++)
	{
		tcModifier *modifier = s_CacheEntries[i].m_Modifier;
		if( modifier )
			modifier->RemoveVarMap();
	}
}

#endif // __BANK

void tcModifier::AllocateVarMap() const
{
	// Invalidate the previous association.
	int nextEntry = 0;

	// Go fishing for an unused cache entry.
	while (nextEntry < VAR_ID_CACHE_SIZE)
	{
		if (!s_CacheEntries[nextEntry].m_Modifier)
		{
			// Unused - let's take it!
			break;
		}

		nextEntry++;
	}

	if (nextEntry == VAR_ID_CACHE_SIZE)
	{
		// That didn't work. Let's find one that had been used but can be recycled.
		nextEntry = s_NextCacheEntry;
		int count = VAR_ID_CACHE_SIZE;

		while (s_CacheEntries[nextEntry].m_RefCount > 0 && count-- > 0)
		{
			nextEntry++;
			nextEntry %= VAR_ID_CACHE_SIZE;
	}
		
		Assertf(count >= 0, "All cache entries are in use. VAR_ID_CACHE_SIZE needs to be increased. (That, or somebody isn't unlocking a cache entry after use.)");
	}

#if __BANK
	if( collectVarMapCacheUse )
	{
		int inUse=0;
		for(int i=0;i<VAR_ID_CACHE_SIZE;i++)
		{
			inUse+= (s_CacheEntries[i].m_RefCount > 0) ? 1 : 0;
		}

		if( inUse > maxVarMapCacheUse )
			maxVarMapCacheUse = inUse;
			
		collectVarMapCacheUse = false;
	}
#endif // __BANK
		
	tcModifier *prevModifier = s_CacheEntries[nextEntry].m_Modifier;

	if (prevModifier)
	{
		prevModifier->m_VarIdToDataMap = NULL;
		prevModifier->SetIndexOfCacheEntry(-1);
	}

	// Associate us.
	s_CacheEntries[nextEntry].m_Modifier = (tcModifier *) this;
	m_IndexOfCacheEntry = nextEntry;
	m_VarIdToDataMap = &s_CacheEntries[nextEntry].m_VarIdToDataMap;

	nextEntry = (nextEntry + 1) % VAR_ID_CACHE_SIZE;
	s_NextCacheEntry = nextEntry;

	atArray<tcModData *> *varIdToDataMap = m_VarIdToDataMap;
	Assert(varIdToDataMap != NULL);

	int numVars = tcConfig::GetNumVars();

	if (varIdToDataMap->GetCount() != numVars)
	{
		varIdToDataMap->Reset();
		varIdToDataMap->Resize(numVars);
	}

	sysMemSet(varIdToDataMap->GetElements(), 0, sizeof(tcModData *) * numVars);

	int varCount = m_modData.GetCount();

	for (int x=0; x<varCount; x++)
	{
		tcModData *modData = (tcModData *) &m_modData[x];
		(*varIdToDataMap)[modData->varId] = modData;
	}
}

void tcModifier::RemoveVarMap()
{
	if (m_VarIdToDataMap)
	{
		// Unregister ourselves.
		VarIdLruCacheEntry *cacheEntry = LookupCacheEntry();
		Assert(cacheEntry);

		cacheEntry->m_Modifier = NULL;
		cacheEntry->m_RefCount = 0;
		m_VarIdToDataMap = NULL;
	}
	m_IndexOfCacheEntry = -1;
}

const tcModData* tcModifier::GetModDataUnsafe(const int varId) const 
{
	return (*m_VarIdToDataMap)[varId];
}

int	tcModifier::GetModDataIdUnsafe(const int varId) const
{	
	const tcModData* modData = GetModDataUnsafe(varId);

	if (modData == NULL)
	{
		return -1;
	}

	int index = ptrdiff_t_to_int(modData - m_modData.GetElements());

	return index;
}

float tcModifier::GetModDataValAUnsafe(const int varId) const
{	
	const tcModData* pModData = GetModDataUnsafe(varId);

	if (pModData)
	{
		return pModData->valA;
	}

	// return the default value
	return tcConfig::GetVarInfo(varId).defaultValue;
}

void tcModifier::SetOnlyMainThreadIsUpdating(bool bMainThreadOnly)
{
	Assert(sysThreadType::IsUpdateThread());	
	s_OnlyMainThreadIsUpdating = bMainThreadOnly;
}


} // namespace rage

