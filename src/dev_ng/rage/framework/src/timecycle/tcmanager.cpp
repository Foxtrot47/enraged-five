//
// timecycle/tcmanager.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "timecycle/tcmanager.h"

// includes (rage)
#include "diag/art_channel.h"
#include "parser/manager.h"
#include "system/param.h"
#include "system/threadtype.h"
#include "vectormath/legacyconvert.h"
#include "grcore/debugdraw.h"

// includes (framework)
#include "timecycle/channel.h" 
#include "timecycle/optimisations.h"
#include "timecycle/tccycle.h"


// optimisations
TIMECYCLE_OPTIMISATIONS()

PARAM(enableStreetLightsTC,"Enable loading of street lights TC");

// namespaces
namespace rage {


// code
void tcManager::Init(const int numVars, tcVarInfo* pVarInfos)
{
	// initialise the keyframe variables
	tcConfig::Init(numVars, pVarInfos);
	m_modifiersLookup.Reset();
	for(int i=0;i<m_modifiersArray.GetCount();i++)
	{
		delete m_modifiersArray[i];
	}
	m_modifiersArray.Reset();
	m_modifierStrengths.Reset();
	m_modOverrides.Reset();
	tcBox::InitPool(MEMBUCKET_WORLD);
}

void tcManager::InitMap(const int numCycles, const tcCycleInfo* pCycleInfos, const int numRegions, const tcRegionInfo* pRegionInfos, const int numTimeSamples, const tcTimeSampleInfo* pTimeSampleInfos, const int TC_DEBUG_ONLY(numDebugGroups), const char** TC_DEBUG_ONLY(ppDebugGroupNames), const int TC_DEBUG_ONLY(numUserFlags), const char ** TC_DEBUG_ONLY(ppUserFlagsNames))
{
	if (m_cycles == NULL)
	{
		m_cycles = rage_new tcCycle[TIMECYCLE_MAX_CYCLES];
	}

	tcConfig::InitMap(numCycles, pCycleInfos, numRegions, pRegionInfos, numTimeSamples, pTimeSampleInfos);

	// initialise and load in the cycle data
	for (int i=0; i<numCycles; i++)
	{
		m_cycles[i].Init(&pCycleInfos[i].filename[0]);
	}

	// debug
#if TC_DEBUG
	m_debug.Init(this, pRegionInfos, numDebugGroups, ppDebugGroupNames, numUserFlags, ppUserFlagsNames);
#endif
}

void tcManager::ShutdownMap()
{
#if TC_DEBUG
	m_debug.Shutdown();
#endif

	// free up any memory that the modifiers have allocated
	m_modifiersLookup.Reset();
	for(int i=0;i<m_modifiersArray.GetCount();i++)
	{
		delete m_modifiersArray[i];
	}
	m_modifiersArray.Reset();
	m_modifierStrengths.Reset();
	
	if (m_cycles)
	{
		delete[] m_cycles;
		m_cycles = NULL;
	}

	tcConfig::ShutdownMap();

	InitRegionData();
	InitModifierData(false);
}

void tcManager::Update(Vec3V_In vPos, const u32 hours, const u32 minutes, const u32 seconds)
{
	CalcRegionStrengths(vPos, hours, minutes, seconds);
	CalcModifierStrengths(vPos, hours, minutes, seconds);

#if TC_DEBUG
	m_debug.Update();
#endif
}

#if TC_DEBUG
void tcManager::Render() const
{
	m_debug.Render();
}

void tcManager::ShowMeBoxUse()
{
	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	grcDebugDraw::AddDebugOutput("ModBox Use : %d(%d) - %d - %d - %d", pPool->GetNoOfUsedSpaces(), pPool->GetSize(), m_modifierBoxAddedMax, m_modifierBoxModifiedMax, m_modifierBoxRemovedMax);

	m_modifierBoxAdded = 
	m_modifierBoxModified =
	m_modifierBoxRemoved = 0;
}

#endif // TC_DEBUG

tcCycle& tcManager::GetCycle(u32 cycleIndex)
{
	if (tcVerifyf(cycleIndex<TIMECYCLE_MAX_CYCLES, "cycle index out of range %d", cycleIndex))
	{
		return m_cycles[cycleIndex];
	}
	else
	{
		return m_cycles[0];
	}
}

int tcManager::GetCycleIndex(u32 cycleHashName)
{
	for (int i=0; i<tcConfig::GetNumCycles(); i++)
	{
		if (cycleHashName == m_cycles[i].GetName().GetHash())
		{
			return i;
		}
	}

	tcAssertf(0, "there is no loaded cycle with hash name %d", cycleHashName);
	return -1;
}

void tcManager::InitRegionData()
{
	// initialise the region data
	m_regionBoxes.Reset();
}

void tcManager::InitModifierData(const bool TC_DEBUG_ONLY(isReload))
{
#if TC_DEBUG
	if (!isReload)
#endif // TC_DEBUG
	{
		fwPool<tcBox>* pPool = tcBox::GetPool();
		Assert(pPool);
		pPool->Reset();

		m_modifierBoxTree.Reset();
		m_modifierBoxTreeIsValid = true;
	}

	// reset any modifier keyframe arrays
	m_modifiersLookup.Reset();
	for(int i=0;i<m_modifiersArray.GetCount();i++)
	{
		delete m_modifiersArray[i];
	}
	m_modifiersArray.Reset();
	m_modifierStrengths.Reset();
	m_modifierStrengths.Reset();
	
#if TC_DEBUG
	if (!isReload)
	{
		m_debug.ResetNumModifierFiles();
	}
#endif
}

void tcManager::CalcTimeSampleInfo(const u32 hours, const u32 minutes, const u32 seconds, u32 &timeSample1, u32 &timeSample2, float &timeInterp)
{
	int numActualTimeSamples = tcConfig::GetNumTimeSamples();
	if (!numActualTimeSamples)
	{
		return;
	}

	// get the current game time as a float
	float gameTimeFloat = float(hours) + float(minutes)/60.0f + float(seconds)/(60.0f*60.0f);

	// make sure the game time isn't out of range - it can be after a mission
	gameTimeFloat = Min(gameTimeFloat, 23.999f);

	// for ease of calculation create a time sample array with 2 extra elements
	// the first entry is the last time sample - 24
	// the last entry is the first time sample + 24

	float localTimeSamples[TIMECYCLE_MAX_TIME_SAMPLES+2];
	float localInterpSamples[TIMECYCLE_MAX_TIME_SAMPLES+2];
	localTimeSamples[0] = tcConfig::GetTimeSampleInfo(numActualTimeSamples-1).hour - 24.0f;
	localInterpSamples[0] = tcConfig::GetTimeSampleInfo(numActualTimeSamples-1).duration;
	for (int i=0; i<numActualTimeSamples; i++)
	{
		localTimeSamples[i+1] = tcConfig::GetTimeSampleInfo(i).hour;
		localInterpSamples[i+1] = tcConfig::GetTimeSampleInfo(i).duration;
	}
	localTimeSamples[numActualTimeSamples+1] = tcConfig::GetTimeSampleInfo(0).hour + 24.0f;
	localInterpSamples[numActualTimeSamples+1] = tcConfig::GetTimeSampleInfo(0).duration;

	// calc the first time sample 
	// start and the end of the time sample array and work backwards
	int beforeIndex = numActualTimeSamples+1;
	while (gameTimeFloat < localTimeSamples[beforeIndex])
	{
		beforeIndex--;
	}

	tcAssertf(beforeIndex>=0 && beforeIndex<=numActualTimeSamples, "invalid before index found: index=%d num=%d", beforeIndex, numActualTimeSamples);
	tcAssertf(gameTimeFloat>=localTimeSamples[beforeIndex] && gameTimeFloat<=localTimeSamples[beforeIndex+1], "out of range before index found: gameTime=%.2f hour1=%f hour2=%f", gameTimeFloat, localTimeSamples[beforeIndex], localTimeSamples[beforeIndex+1]);

	if (beforeIndex==0)
	{
		// set to the last index in the actual list
		timeSample1 = numActualTimeSamples-1;
	}
	else
	{
		timeSample1 = beforeIndex-1;
	}

	if (beforeIndex==numActualTimeSamples)
	{
		// set to the first index in the actual list
		timeSample2 = 0;
	}
	else
	{
		timeSample2 = beforeIndex;
	}

	// calc the interp value between this hour and the next

	float beforeTime = localTimeSamples[beforeIndex] + localInterpSamples[beforeIndex];
	timeInterp = Clamp((gameTimeFloat-beforeTime)/(localTimeSamples[beforeIndex+1] - beforeTime),0.0f,1.0f);
}

void tcManager::SetModifierBoxTreeRebuildProhibited(bool bProhibited)
{
	Assert(sysThreadType::IsUpdateThread());
	m_modifierBoxTreeRebuildProhibited = bProhibited;

	// If we're unlocking, then process any deferred box removals that we queued up
	if(!bProhibited)
	{
		for(int i = 0; i < m_DeferredBoxRemovals.GetCount(); i++)
		{
			RemoveModifierBoxes(m_DeferredBoxRemovals[i].m_iplId, m_DeferredBoxRemovals[i].m_bIsInterior);
		}
		m_DeferredBoxRemovals.ResetCount();
	}
}

void tcManager::AddRegionBox(const spdAABB& bBox, const u32 hashKey, const float percentage, const float range, const int startHour, const int endHour, const int iplId, const char* pSourceFile)
{
	tcAssertf(bBox.IsValid(), "bounding box min values are not less than max values");

	// find the region that this box refers to
	int regionIndex = FindRegionIndex(hashKey, pSourceFile);
	if (regionIndex>=0)
	{
		// fix up end hour.
		int aEndHour = endHour;
		if( startHour != endHour )
		{
			aEndHour = ((endHour+1)%24);
		}
		
		// if the box already exists replace with new values
		for (int b=0; b<m_regionBoxes.GetCount(); b++)
		{
			if (bBox==m_regionBoxes[b].GetBoundingBox())
			{
				tcBox& regionBox = m_regionBoxes[b];
				regionBox.SetStrength(percentage*0.01f);
				regionBox.SetRange(range);
				regionBox.SetIndex(regionIndex);
				regionBox.SetStartHour(startHour);
				regionBox.SetEndHour(aEndHour);
				regionBox.SetIplId(iplId);

#if TC_DEBUG
				m_modifierBoxModified++;
				if( m_modifierBoxModified > m_modifierBoxModifiedMax )		m_modifierBoxModifiedMax = m_modifierBoxModified;
#endif

				return;
			}
		}

		// add a new modifier box and set it up
		tcBox& regionBox = m_regionBoxes.Append();
		regionBox.SetBoundingBox(bBox);
		regionBox.SetStrength(percentage*0.01f);
		regionBox.SetRange(range);
		regionBox.SetIndex(regionIndex);
		regionBox.SetStartHour(startHour);
		regionBox.SetEndHour(aEndHour);
		regionBox.SetIplId(iplId);

#if TC_DEBUG
		m_modifierBoxAdded++;
		if( m_modifierBoxAdded > m_modifierBoxAddedMax )		m_modifierBoxAddedMax = m_modifierBoxAdded;
#endif
	}
}
void tcManager::UnloadModifierFile(const char* filename)
{
	// set the debug file count
#if TC_DEBUG
	m_debug.SetModifierFileInfo(filename);
#endif

	parTree* pTree = PARSER.LoadTree(filename, "xml");
#if TC_DEBUG
	int fileId = m_debug.GetModifierFileInfoIdx(filename);
#endif
	if (pTree)
	{
		parTreeNode* pRootNode= pTree->GetRoot();
		parTreeNode* pModifierNode = NULL;
		while ((pModifierNode = pRootNode->FindChildWithName("modifier", pModifierNode)) != NULL)
		{
			// set the modifier name
			const char* modifierName = pModifierNode->GetElement().FindAttribute("name")->GetStringValue();

			atHashString modifierNameHash(modifierName);
			if(tcModifier** ppModifier = m_modifiersLookup.Access(modifierNameHash))
			{
				if(tcModifier* pModifier = *ppModifier)
				{
					for(int i=0;i<pModifier->GetModDataCount();i++)
						pModifier->RemoveModData(i);
					int arrayIndex = m_modifiersArray.Find(pModifier);
					m_modifiersArray.Delete(arrayIndex);
					delete pModifier;
					m_modifiersLookup.RemoveKey(modifierNameHash);
					m_modifiersLookup.FinishInsertion();
				}
			}

#if TC_DEBUG
			tcDebug::RemoveModifierRecord(atHashString(modifierName), fileId);
#endif 
		}
		delete pTree;
	}
	else
	{
		tcAssertf(false, "cannot open %s for reading", filename);	
	}
}

void tcManager::LoadModifierFile(const char* filename)
{
	// set the debug file count
#if TC_DEBUG
	m_debug.SetModifierFileInfo(filename);
#endif

	parTree* pTree = PARSER.LoadTree(filename, "xml");
#if TC_DEBUG
	int fileId = m_debug.GetModifierFileInfoIdx(filename);
#endif
	if (pTree)
	{
		parTreeNode* pRootNode= pTree->GetRoot();

		if (pRootNode->GetElement().FindAttribute("version"))
		{
			// float modifierFileVersion = (float)atof(pRootNode->GetElement().FindAttribute("version")->GetStringValue());
		}
		else
		{
			tcAssertf(0, "missing attribute \"version\" in %s", filename);
		}

		parTreeNode* pModifierNode = NULL;
		while ((pModifierNode = pRootNode->FindChildWithName("modifier", pModifierNode)) != NULL)
		{
			// set the modifier name
			const char* modifierName = pModifierNode->GetElement().FindAttribute("name")->GetStringValue();

			atHashString modifierNameHash(modifierName);

#if __ASSERT	
			artAssertf(!m_modifiersLookup.Has(modifierNameHash), "Timecycle file %s has duplicate modifier %s - art bug", filename, modifierName);
#endif	//__ASSERT

			tcModifier *pModifier = rage_new tcModifier();
			pModifier->SetName(modifierNameHash);
			m_modifiersArray.PushAndGrow(pModifier);
			m_modifiersLookup.Insert(modifierNameHash,pModifier);
			
#if TC_DEBUG
			tcDebug::AddModifierRecord(atHashString(modifierName), fileId);
#endif 

			// pre allocate the keyframe modifier data array if the information is available
			if (parAttribute* pAttr = pModifierNode->GetElement().FindAttribute("userFlags"))
			{
				const char* userFlagsStr = pAttr->GetStringValue();
				u32 userFlags = atoi(userFlagsStr);

				u32 size = 1;
				u32 *pUserFlags=NULL;
				pModifier->GetUserFlags().GetRaw(&pUserFlags,&size);
				*pUserFlags = userFlags;
			}

			// load in modifier's keyframe data
			parTreeNode* pVarNode = NULL;
			for (int varId=0; varId<tcConfig::GetNumVars(); varId++)
			{
				const char* varName = tcConfig::GetVarInfo(varId).name;
				if ((pVarNode = pModifierNode->FindChildWithName(varName, NULL)) != NULL)
				{
					float valA, valB;
					char* varData = pVarNode->GetData();
					sscanf(varData, "%f %f", &valA, &valB);

					pModifier->AddModData(varId, valA, valB);
				}
			}
		}

		delete pTree;
	}
	else
	{
		tcAssertf(false, "cannot open %s for reading", filename);	
	}
}


void tcManager::FinishModifierLoad() 
{ 
	m_modifiersLookup.FinishInsertion(); 
	m_modifierStrengths.Reset();
	m_modifierStrengths.Resize(m_modifiersArray.GetCount());
}

void tcManager::AddModifierBox(const spdAABB& bBox, const u32 hashKey, const float percentage, const float range, const int startHour, const int endHour, const int iplId, const char* pSourceFile, bool bTreatAsSphere, bool isInterior)
{
	tcAssertf(bBox.IsValid(), "bounding box min values are not less than max values");
	Assert(sysThreadType::IsUpdateThread());

	// Temp fix to allow the game to load.
	if( !PARAM_enableStreetLightsTC.Get() )
	{
		static const atHashString streetLighting("StreetLighting",0xA502C509);
		static const atHashString streetLightingTraffic("StreetLightingTraffic",0x77F1BF19);
		static const atHashString streetLightingJunction("StreetLightingJunction",0x2D909E01);
		
		if( hashKey == streetLighting ||
			hashKey == streetLightingTraffic ||
			hashKey == streetLightingJunction )
			return;
	}
	
	// find the modifier that this box refers to
	int modifierIndex = FindModifierIndex(hashKey, pSourceFile);
	if (modifierIndex>=0)
	{
		// fix up end hour.
		int aEndHour = endHour;
		if( startHour != endHour )
		{
			aEndHour = ((endHour+1)%24);
		}
	
		// add a new modifier box and set it up
		tcBox* pBox = rage_new tcBox;
		Assert(pBox);

		pBox->SetBoundingBox(bBox);
		pBox->SetStrength(percentage*0.01f);
		pBox->SetRange(range);
		pBox->SetIndex(modifierIndex);
		pBox->SetStartHour(startHour);
		pBox->SetEndHour(aEndHour);
		pBox->SetIplId(iplId);
		pBox->SetTreatAsSphere(bTreatAsSphere);
		pBox->SetIsInterior(isInterior);
#if TC_DEBUG
		unsigned int debugFlag = 0;
		
		const tcModifier *mod = GetModifier(modifierIndex);
		if( mod )
		{
			u32 size = 1;
			const u32 *pUserFlags;
			mod->GetUserFlags().GetRaw(&pUserFlags,&size);
			debugFlag = *pUserFlags;
		}

		pBox->SetDebugFlags((int)debugFlag);
#endif // __BANK

		m_modifierBoxTreeIsValid = false;
	}
}

void tcManager::RemoveModifierBoxes(const int iplId, bool isInterior)
{
	Assert(sysThreadType::IsUpdateThread());
	
	// Adding this assert so we can figure out why tcBoxes are being deleted
	// while the async CalcAmbientScales jobs are running.  The code below
	// will protect against a crash but I'm still interested in seeing why
	// this is happening
	Assert(!m_modifierBoxTreeRebuildProhibited);

	// if the tree is locked, we need to defer the removal of these boxes
	// because other threads may be accessing them
	if(m_modifierBoxTreeRebuildProhibited)
	{
		tcBoxRemovalInfo& info = m_DeferredBoxRemovals.Grow();
		info.m_iplId = iplId;
		info.m_bIsInterior = isInterior;
		return;
	}

	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	for (int i = 0; i < pPool->GetSize(); i++)
	{
		tcBox* pBox = pPool->GetSlot(i);

		if (pBox && pBox->GetIplId() == iplId && pBox->GetIsInterior() == isInterior)
		{	
			m_modifierBoxTreeIsValid = false;
			delete pBox;

#if TC_DEBUG
			if( m_modifierBoxRemoved > m_modifierBoxRemovedMax )	m_modifierBoxRemovedMax = m_modifierBoxRemoved;
			m_modifierBoxRemoved++;
#endif
			
		}
	}
}

void tcManager::BuildModifierTree()
{
	Assert(sysThreadType::IsUpdateThread());
	Assert(!m_modifierBoxTreeRebuildProhibited);

	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	m_modifierBoxTree.Init(pPool->GetNoOfUsedSpaces());
	
	for (int i = 0; i < pPool->GetSize(); i++)
	{
		const tcBox* pBox = pPool->GetSlot(i);
		if (pBox)
		{
			const spdAABB& bBox = pBox->GetBoundingBox();
			if (Verifyf(bBox == bBox,"Invalid tcBox in slot %d",i)) 
			{
				float range = pBox->GetRange();
				m_modifierBoxTree.Insert(bBox, range, i);
			}
		}

		Assert(m_modifierBoxTree.GetListCount() <= static_cast<size_t>(pPool->GetNoOfUsedSpaces()));
	}
	
	m_modifierBoxTree.BuildTree();
	m_modifierBoxTreeIsValid = true;

}

void tcManager::BuildModifierTreeIfNeeded()
{
	if(!m_modifierBoxTreeIsValid)
	{
		BuildModifierTree();
	}
}
void tcManager::AddModifierOverride(atHashString modifier, atHashString modOverride)
{
	tcManager::TCModOverride *over = NULL;
	
	for(int i=0;i<m_modOverrides.GetCount();i++)
	{
		if( m_modOverrides[i].originalMod == modifier )
		{
			over = &m_modOverrides[i];
			break;
		}
	}

	if( over == NULL && !m_modOverrides.IsFull() )
	{
		over = &m_modOverrides.Append();
	}

	if( over )
	{
		over->originalMod = modifier;
		over->overrideMod = modOverride;
	}
	
	tcAssertf(over, "tcManager::AddModifierOverride : No available modifier override slot");
}

void tcManager::RemoveModifierOverride(atHashString modifier)
{
	for(int i=0;i<m_modOverrides.GetCount();i++)
	{
		if( m_modOverrides[i].originalMod == modifier )
		{
			m_modOverrides.DeleteFast(i);
			return;
		}
	}

	tcAssertf(false, "tcManager::RemoveModifierOverride : Couldn't find modifer %s",modifier.GetCStr());
}
		
void tcManager::ClearModifierOverrides()
{
	m_modOverrides.Reset();
}

void tcManager::CalcRegionStrengths(Vec3V_In vPos, const u32 hours, const u32 minutes, const u32 seconds)
{
	// initialise the region strength to zero
	for (int i=0; i<TIMECYCLE_MAX_REGIONS; i++)
	{
		m_regionStrengths[i] = 0.0f;
	}

	// go through the region boxes calculating the region strengths based on the position passed in
	for (int i=0; i<m_regionBoxes.GetCount(); i++)
	{
		tcBox* pRegionBox = &m_regionBoxes[i];

		float strengthMult = pRegionBox->CalcStrengthMult(vPos, hours, minutes, seconds);
		if (strengthMult>0.0f)
		{
			m_regionStrengths[pRegionBox->GetIndex()] = Max(strengthMult, m_regionStrengths[pRegionBox->GetIndex()]);
		}
	}
}

void tcManager::CalcModifierStrengths(Vec3V_In vPos, const u32 hours, const u32 minutes, const u32 seconds)
{
	// initialise the modifier strength to zero
	sysMemSet(m_modifierStrengths.GetElements(), 0, sizeof(float) * GetModifiersCount());

	// go through the modifier boxes calculating the modifier strengths based on the position passed in
	int numBoxes = 0;
	int boxIndices[TIMECYCLE_MAX_MODIFIER_BOX_QUERY];

	QueryModifierBoxTree(vPos, boxIndices, TIMECYCLE_MAX_MODIFIER_BOX_QUERY, numBoxes);

	fwPool<tcBox>* pPool = tcBox::GetPool();
	Assert(pPool);

	for (int i=0; i<numBoxes; i++)
	{
		const tcBox* pBox = pPool->GetSlot(boxIndices[i]);
		Assert(pBox);

		float strengthMult = pBox ? pBox->CalcStrengthMult(vPos, hours, minutes, seconds) * pBox->GetStrength() : 0.0f;
		
		if (strengthMult>0.0f)
		{
			m_modifierStrengths[pBox->GetIndex()] = Max(strengthMult, m_modifierStrengths[pBox->GetIndex()]);
		}
	}

#if 0
	// We use tc inst box for ambient multiplier only for now, so this code is disabled.
	atArray<tcInstBoxSearchResult> searchResults;
	spdSphere tempSphere(vPos, ScalarV(0.1f));
	g_tcInstBoxMgr.Search( spdAABB(tempSphere), searchResults);
	for (s32 i=0; i<searchResults.GetCount(); i++)
	{
		const tcInstBoxSearchResult &result = searchResults[i];
		
		float strengthMult = result.CalcStrengthMult(vPos, hours, minutes, seconds);
		strengthMult *= result.m_pType->GetPercentage();
		
		if (strengthMult>0.0f)
		{
			int modifierIndex = FindModifierIndex(result.m_pType->GetModifier(), "Instanced TC Box");
			m_modifierStrengths[modifierIndex] = Max(strengthMult, m_modifierStrengths[modifierIndex]);
		}
	}
#endif
	
#if TC_DEBUG
	// Apply the edit modifier
	if (m_debug.GetModifierEditApply())		
	{
		int editModId = m_debug.GetModifierEditId();
		tcAssertf(editModId>=0 && editModId<GetModifiersCount(), "edit modifier id out of range");
		m_modifierStrengths[editModId] = m_debug.GetModifierEditStrength();
	}
#endif // TC_DEBUG
}

int	tcManager::FindRegionIndex(const u32 hash, const char* ASSERT_ONLY(pSourceFile)) const
{
	// go through the regions looking for the requested one
	for (int r=0; r<tcConfig::GetNumRegions(); r++)
	{
		if (tcConfig::GetRegionInfo(r).hashName == hash)
		{
			// return the modifier index
			return r;
		}
	}

	// modifier not found
	tcAssertf(0, "Didn't find region (%s)", pSourceFile);
	return -1;
}

int	tcManager::FindModifierIndex(const atHashString name, const char *label, bool noAssert) const
{
	tcModifier * mod = (tcModifier*)FindModifier(name,label,noAssert);
	
	int res = GetModifierIndex(mod);
	//int res = mod ? m_modifiers.GetIndexFromDataPtr(mod) : -1;
	
	return res;
}

const tcModifier *tcManager::FindModifier(const atHashString name_, const char *ASSERT_ONLY(label), bool ASSERT_ONLY(noAssert)) const
{
	if (name_.IsNull() ){
		return NULL;
	}

	tcModifier * mod = NULL;
	
	atHashString actualName = name_;

	if( m_modOverrides.GetCount() )	
	{
		for(int i=0;i<m_modOverrides.GetCount();i++)
		{
			if( m_modOverrides[i].originalMod == name_ )
			{
				actualName = m_modOverrides[i].overrideMod;
				break;
			}
		}
	}
	
#if TC_DEBUG
	if( !m_modifiersLookup.IsSorted() )
	{
		// go through the modifiers looking for the requested one
		for (int m=0; m<m_modifiersArray.GetCount(); m++)
		{
			if (m_modifiersArray[m]->GetName() == actualName )
			{
				mod = m_modifiersArray[m];
				break;
			}
		}
	}
	else
#endif

	{
		mod = m_modifiersLookup.SafeGet(actualName)!=NULL? *(m_modifiersLookup.SafeGet(actualName)):NULL;
	}
	
	// modifier not found
	tcAssertf(noAssert || (mod != NULL), "(Label: %s) Didn't find modifier %s (%d) in timecyclemodifiers files", label, actualName.GetCStr(), actualName.GetHash());
	return mod;
}

#if TC_DEBUG
int tcManager::FindModifierRecordIndex(int idx) const 
{
	if(idx == -1)
	{
		return -1;
	}

	atHashString name(GetModifierName(idx));
	
	return m_debug.FindModifierRecordIndex(name);
}
#endif // TC_DEBUG

void tcManager::QueryModifierBoxTree(Vec3V_In vPos, int *indices, int maxfound, int& numfound)
{
	Assert(sysThreadType::IsUpdateThread() || m_modifierBoxTreeRebuildProhibited);
	Assert(m_modifierBoxTreeIsValid || !m_modifierBoxTreeRebuildProhibited);

	if(!m_modifierBoxTreeIsValid && !m_modifierBoxTreeRebuildProhibited)
		BuildModifierTree();
	m_modifierBoxTree.GetIndices(vPos,0.1f,indices,maxfound,numfound);
}

} // namespace rage

