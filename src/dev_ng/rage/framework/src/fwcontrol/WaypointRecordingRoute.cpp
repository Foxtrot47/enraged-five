#include "fwcontrol\WaypointRecordingRoute.h"

// Rage headers
#include "file\stream.h"

// Framework headers
#include "fwmaths\Angle.h"
#include "vector\geometry.h"

using namespace rage;

static dev_float s_fMaxDistFromCentre = 8.0f;
u8 fwWaypointRecordingRoute::SideDisttoUNIT8(const float fSideDist)
{
	return (u8) ((fSideDist / s_fMaxDistFromCentre) * 255.0f);
}
float fwWaypointRecordingRoute::UINT8toSideDist(const u8 i)
{
	return (((float)i) / 255.0f) * s_fMaxDistFromCentre;
}
u8 fwWaypointRecordingRoute::MBRtoUINT8(const float fMBR)
{
	return (u8) (fMBR * 64.0f);
}
float fwWaypointRecordingRoute::UINT8toMBR(const u8 i)
{
	return (((float)i) / 64.0f);
}
u8 fwWaypointRecordingRoute::HDGtoUINT8(const float fHeading)
{
	float fHDG = ( RtoD * fHeading);
	while(fHDG < 0.0f) fHDG += 360.f;
	while(fHDG >= 360.0f) fHDG -= 360.0f;

	return (u8) ((fHDG / 360.0f) * 255.0f);
}
float fwWaypointRecordingRoute::UINT8toHDG(const u8 i)
{
	float fHDG = (((float)i) / 255.0f) * 360.0f;
	fHDG = ( DtoR * fHDG);
	fHDG = fwAngle::LimitRadianAngle(fHDG);
	return fHDG;
}
static dev_float s_fMaxCruiseSpeedInRecording = 64.0f;
u8 fwWaypointRecordingRoute::CruiseSpeedtoUINT8(const float fCruiseSpeed)
{
	return (u8) ((fCruiseSpeed / s_fMaxCruiseSpeedInRecording) * 255.0f);
}
float fwWaypointRecordingRoute::UINT8toCruiseSpeed(const u8 i)
{
	return (((float)i) / 255.0f) * s_fMaxCruiseSpeedInRecording;
}
int fwWaypointRecordingRoute::GetClosestWaypointIndex(const Vector3 & vPos) const
{
	int iClosest = -1;
	float fClosest = FLT_MAX;
	for(int i=0; i<m_iNumWaypoints; i++)
	{
		const float fMag2 = (m_pWaypoints[i].GetPosition() - vPos).Mag2();
		if(fMag2 < fClosest)
		{
			fClosest = fMag2;
			iClosest = i;
		}
	}
	return iClosest;
}
Vector3 fwWaypointRecordingRoute::GetClosestPosition(const Vector3 & vPos) const
{
	const int iClosest = GetClosestWaypointIndex(vPos);
	Assert(iClosest != -1);
	if(iClosest == -1)
		return vPos;
	
	Vector3 vClosest = m_pWaypoints[iClosest].GetPosition();
	Vector3 vClosest1(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vClosest2(FLT_MAX, FLT_MAX, FLT_MAX);

	if(iClosest > 0)
	{
		Vector3 vPrev = m_pWaypoints[iClosest-1].GetPosition();
		vClosest1 = VEC3V_TO_VECTOR3( geomPoints::FindClosestPointSegToPoint(VECTOR3_TO_VEC3V(vPrev), VECTOR3_TO_VEC3V(vClosest-vPrev), VECTOR3_TO_VEC3V(vPos)) );
	}
	if(iClosest < m_iNumWaypoints-1)
	{
		Vector3 vNext = m_pWaypoints[iClosest+1].GetPosition();
		vClosest2 = VEC3V_TO_VECTOR3( geomPoints::FindClosestPointSegToPoint(VECTOR3_TO_VEC3V(vClosest), VECTOR3_TO_VEC3V(vNext-vClosest), VECTOR3_TO_VEC3V(vPos)) );
	}

	return (vClosest1-vPos).Mag2() < (vClosest2-vPos).Mag2() ? vClosest1 : vClosest2;
}
float fwWaypointRecordingRoute::GetWaypointDistanceFromStart(int iWaypoint) const
{
	Assert(iWaypoint >= 0 && iWaypoint < m_iNumWaypoints);
	iWaypoint = Clamp(iWaypoint, 0, m_iNumWaypoints);

	Vector3 vLastPos = (m_iNumWaypoints > 0) ? GetWaypoint(0).GetPosition() : VEC3_ZERO;
	float fDistanceFromStart = 0.0f;

	for(s32 p=0; p<=iWaypoint && p<m_iNumWaypoints; p++)
	{
		const RouteData & waypointA = GetWaypoint(p);
		const Vector3 vA = waypointA.GetPosition();
		fDistanceFromStart += (vA - vLastPos).Mag();

		vLastPos = vA;
	}

	return fDistanceFromStart;
}
fwWaypointRecordingRoute * fwWaypointRecordingRoute::Load(const char * pFilename)
{
	fiStream * pFile = fiStream::Open(pFilename);
	if(pFile)
	{
		fwWaypointRecordingRoute * pWptRec = rage_new fwWaypointRecordingRoute();
		if(pWptRec->Load(pFile))
		{
			pFile->Close();
			return pWptRec;
		}
		pFile->Close();
		delete pWptRec;
	}
	return NULL;
}

bool fwWaypointRecordingRoute::Load(fiStream * pFile, RouteData * pRouteData)
{
	Assert(pFile);
	
	// Read header
	char iHeader[4];
	pFile->Read(&iHeader, 4);

	// Read flags
	int iFlags1, iFlags2;
	pFile->ReadInt(&iFlags1, 1);
	pFile->ReadInt(&iFlags2, 1);

	// Read num waypoints
	pFile->ReadInt(&m_iNumWaypoints, 1);

	// Alloc waypoint data
	// Unless passed in, in which case we use this without questioning the length (it will be the editor passing in an array allocated to the max num wpts)
	if(pRouteData)
		m_pWaypoints = pRouteData;
	else
		m_pWaypoints = rage_new RouteData[m_iNumWaypoints];

	Assert(m_pWaypoints);

	// Read waypoints
	for(int w=0; w<m_iNumWaypoints; w++)
	{
		pFile->ReadFloat(&m_pWaypoints[w].m_fXYZ[0], 1);
		pFile->ReadFloat(&m_pWaypoints[w].m_fXYZ[1], 1);
		pFile->ReadFloat(&m_pWaypoints[w].m_fXYZ[2], 1);
		pFile->ReadInt(&m_pWaypoints[w].m_iPackedStruct1AsInteger, 1);
		pFile->ReadInt(&m_pWaypoints[w].m_iPackedStruct2AsInteger, 1);
	}

	return true;
}

bool fwWaypointRecordingRoute::Save(fiStream * pFile) const
{
	Assert(pFile);

	// Write header
	char iHeader[] = "WPTR";
	pFile->Write(iHeader, 4);

	// Write flags.  These are for future expansion.
	int iFlags1 = 0;
	pFile->WriteInt(&iFlags1, 1);
	int iFlags2 = 0;
	pFile->WriteInt(&iFlags2, 1);

	// Write num waypoints
	pFile->WriteInt(&m_iNumWaypoints, 1);

	// Write waypoints
	for(int w=0; w<m_iNumWaypoints; w++)
	{
		pFile->WriteFloat(&m_pWaypoints[w].m_fXYZ[0], 1);
		pFile->WriteFloat(&m_pWaypoints[w].m_fXYZ[1], 1);
		pFile->WriteFloat(&m_pWaypoints[w].m_fXYZ[2], 1);
		pFile->WriteInt(&m_pWaypoints[w].m_iPackedStruct1AsInteger, 1);
		pFile->WriteInt(&m_pWaypoints[w].m_iPackedStruct2AsInteger, 1);
	}

	return true;
}

void fwWaypointRecordingRoute::Place(fwWaypointRecordingRoute* that, datResource& rsc)
{
	::new (that) fwWaypointRecordingRoute(rsc);
}

fwWaypointRecordingRoute::fwWaypointRecordingRoute(datResource& rsc)
{
	rsc.PointerFixup(m_pWaypoints);
}

#if __DECLARESTRUCT
void fwWaypointRecordingRoute::DeclareStruct(datTypeStruct & s)
{
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(fwWaypointRecordingRoute);
		STRUCT_FIELD(m_iFlags1);
		STRUCT_FIELD(m_iFlags2);
		STRUCT_DYNAMIC_ARRAY(m_pWaypoints, m_iNumWaypoints);
		STRUCT_FIELD(m_iNumWaypoints);
	STRUCT_END();
}
void fwWaypointRecordingRoute::RouteData::DeclareStruct(datTypeStruct & s)
{
	STRUCT_BEGIN(fwWaypointRecordingRoute::RouteData);
	STRUCT_FIELD(m_fXYZ[0]);
	STRUCT_FIELD(m_fXYZ[1]);
	STRUCT_FIELD(m_fXYZ[2]);
	STRUCT_FIELD(m_iPackedStruct1AsInteger);
	STRUCT_FIELD(m_iPackedStruct2AsInteger);
	STRUCT_END();
}
#endif