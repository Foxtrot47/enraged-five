#ifndef FWWAYPOINT_RECORDING_ROUTE_H
#define FWWAYPOINT_RECORDING_ROUTE_H

#include "vector/vector3.h"
#include "paging/base.h"

//************************************************************
// fwWaypointRecordingRoute

namespace rage {

class fwWaypointRecordingRoute : public pgBase
{
public:
	struct RouteData
	{
		float m_fXYZ[3];

		union
		{
			struct  
			{
				u32 DECLARE_BITFIELD_3(
					m_iFlags,			16,
					m_iHeading,			8,
					m_iMoveBlendRatio,	8
				);

			} m_PackedStruct1;
			u32 m_iPackedStruct1AsInteger;
		};
		// A second u32, unused currently
		union
		{
			struct  
			{
				u32 DECLARE_BITFIELD_3(
					m_iFreeSpaceOnLeft, 8,
					m_iFreeSpaceOnRight, 8,
					m_iUnused, 16
					);
			} m_PackedStruct2;	
			u32 m_iPackedStruct2AsInteger;
		};

		inline Vector3 GetPosition() const { return Vector3(m_fXYZ[0], m_fXYZ[1], m_fXYZ[2]); }
		inline float GetHeading() const { return UINT8toHDG(m_PackedStruct1.m_iHeading); }
		inline u32 GetFlags() const { return m_PackedStruct1.m_iFlags; }
		inline float GetSpeed() const
		{
			if (GetFlags() & WptFlag_Vehicle)
			{
				return GetSpeedAsCruiseSpeed();
			}
			else
			{
				return GetSpeedAsMBR();
			}
		}
		inline float GetSpeedAsMBR() const { return UINT8toMBR(m_PackedStruct1.m_iMoveBlendRatio); }
		inline float GetSpeedAsCruiseSpeed() const {return UINT8toCruiseSpeed(m_PackedStruct1.m_iMoveBlendRatio);}
		inline float GetFreeSpaceToLeft() const { return UINT8toSideDist(m_PackedStruct2.m_iFreeSpaceOnLeft);}
		inline float GetFreeSpaceToRight() const { return UINT8toSideDist(m_PackedStruct2.m_iFreeSpaceOnRight);}
#if __BANK
		inline void SetPosition(const Vector3 & vPos) { m_fXYZ[0] = vPos.x; m_fXYZ[1] = vPos.y; m_fXYZ[2] = vPos.z; }
		inline void SetHeading(const float fHdg) { m_PackedStruct1.m_iHeading = HDGtoUINT8(fHdg); }
		inline void SetFlags(const u32 f) { m_PackedStruct1.m_iFlags = f; }
		inline void SetMBR(const float fMBR) { m_PackedStruct1.m_iMoveBlendRatio = MBRtoUINT8(fMBR); }
		inline void SetCruiseSpeed(const float fCruiseSpeed) {m_PackedStruct1.m_iMoveBlendRatio = CruiseSpeedtoUINT8(fCruiseSpeed); }
		inline void SetFreeSpaceToLeft(const float fDist) { m_PackedStruct2.m_iFreeSpaceOnLeft = SideDisttoUNIT8(fDist);}
		inline void SetFreeSpaceToRight(const float fDist){ m_PackedStruct2.m_iFreeSpaceOnRight = SideDisttoUNIT8(fDist);}
		inline void SetSpeed(const float fSpeed)
		{
			if (GetFlags() & WptFlag_Vehicle)
			{
				SetCruiseSpeed(fSpeed);
			}
			else
			{
				SetMBR(fSpeed);
			}
		}
#endif
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct & s);
#endif
	};
	enum
	{
		WptFlag_Jump					= 0x01,
		WptFlag_Land					= 0x02,
		WptFlag_SkiFlipBackwards		= 0x04,
		WptFlag_SkiFlipForwards			= 0x08,
		WptFlag_DropDown				= 0x10,
		WptFlag_NonInterrupt			= 0x20,
		WptFlag_SurfacingFromUnderwater	= 0x40,
		WptFlag_Vehicle					= 0x80,
		WptFlag_ApplyJumpForce			= 0x100,
		WptFlag_Unused7					= 0x200,
		WptFlag_Unused6					= 0x400,
		WptFlag_Unused5					= 0x800,
		WptFlag_Unused4					= 0x1000,
		WptFlag_Unused3					= 0x2000,
		WptFlag_Unused2					= 0x4000,
		WptFlag_Unused1					= 0x8000
	};
	static const int ms_iMaxNumWaypoints = 512;

	fwWaypointRecordingRoute()
	{
		m_iFlags1 = 0;
		m_iFlags2 = 0;
		m_pWaypoints = NULL;
		m_iNumWaypoints = 0;
	}
	fwWaypointRecordingRoute(datResource & rsc);
	DECLARE_PLACE(fwWaypointRecordingRoute);

	~fwWaypointRecordingRoute()
	{
		delete[] m_pWaypoints;
	}

#if __BANK
	bool AddWaypoint(const Vector3 & vPos, const float fMBR, const float fHeading, const u16 iFlags);
	bool InsertWaypoint(const int iIndex, const Vector3 & vPos, const float fMBR, const float fHeading, const u16 iFlags);
	void RemoveWaypoint(const int iWpt);
	void AllocForRecording();
	bool SanityCheck();
	void Draw(const bool bRecordingRoute);
	RouteData * GetWaypointsBuffer() { return m_pWaypoints; }
#endif

	inline RouteData & GetWaypoint(const int i) const
	{
		Assert(m_pWaypoints);
		Assert(i >= 0);
		Assert(i < m_iNumWaypoints);

		return m_pWaypoints[i];
	}

	int GetClosestWaypointIndex(const Vector3 & vPos) const;
	Vector3 GetClosestPosition(const Vector3 & vPos) const;
	float GetWaypointDistanceFromStart(int iWaypoint) const;

	inline int GetSize() const { return m_iNumWaypoints; }

	void Clear() { m_iNumWaypoints = 0; }

	bool Save(fiStream * pFile) const;
	bool Load(fiStream * pFile, RouteData * pRouteData=NULL);
	static fwWaypointRecordingRoute * Load(const char * pFilename);

	enum { RORC_VERSION = 1 };

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct & s);
#endif

	enum
	{
		RouteFlag_AssistedMovement	=	0x01
	};

	u32 GetFlags() const { return m_iFlags1; }
	void SetFlags(const u32 i) { m_iFlags1 = i; }
	
protected:
	
	// Two sets of flags, unused currently but here for future expansion
	u32 m_iFlags1;
	u32 m_iFlags2;
	RouteData * m_pWaypoints;
	int m_iNumWaypoints;

	static u8 MBRtoUINT8(const float fMBR);
	static float UINT8toMBR(const u8 i);
	static u8 HDGtoUINT8(const float fMBR);
	static float UINT8toHDG(const u8 i);
	static u8 CruiseSpeedtoUINT8(const float fCruiseSpeed);
	static float UINT8toCruiseSpeed(const u8 i);
	static u8 SideDisttoUNIT8(const float fSideDist);
	static float UINT8toSideDist(const u8 i);
};

} //namespace rage {

#endif // FWWAYPOINT_RECORDING_ROUTE_H