#ifndef FW_CONTROL_PEDRECORDING_H
#define FW_CONTROL_PEDRECORDING_H

#include "paging/base.h"

#define PED_RECORDING_BUFFER_SIZE	(300000)	// Recordings take 2K per char per second

/**
 * Class: CPedRecordingRsc
 *
 * CPedRecordingRsc wraps the raw ped recording data for
 * the purposes of resource loading.
 *
 * Ideally in the future this class could be replaced by a
 * proper class representation of a ped recording, as
 * opposed to just raw serialized data. Naturally that would 
 * require a more substantial re-write of CPedRecordingMgr.
 *
 */
class CPedRecordingRsc : public pgBase
{
public:
	CPedRecordingRsc() { }

	/* resource ctor */
	CPedRecordingRsc(datResource& rsc) : m_RawData(rsc) { }

	enum { RORC_VERSION = 1 };

	/* returns pointer to raw data */
	u32 * GetData() { return m_RawData.GetElements(); }

	/* returns number of chars */
	u32 GetNumBytes() { return 4 * m_RawData.GetCount(); }

	/* loads data from file - this fn is used by ragebuilder */
#if __RESOURCECOMPILER
	void Load(const char * filename)
	{
		fiStream * pStream = ASSET.Open(filename, "", true);
		Assert(pStream);

		u8 * pRawData;

		sysMemStartTemp();
		pRawData = rage_new u8[pStream->Size()];
		sysMemEndTemp();

		Assert(pRawData);
		s32 udwSize = pStream->Read(pRawData, pStream->Size());
		Assert(udwSize == pStream->Size());
		pStream->Close();

		u32 numElements = udwSize / 4;

		Assert(numElements < PED_RECORDING_BUFFER_SIZE);
		m_RawData.Resize(numElements);

		for (u32 i=0; i<numElements; i++)
		{
			u32 * pData = (u32 *) pRawData;
			pData += i;
			memcpy(&m_RawData[i], pData, sizeof(u32));
		}

	}
#else
	void Load(const char*);
#endif

	/* declare place function which calls rsc ctor */
	DECLARE_PLACE(CPedRecordingRsc);

#if __DECLARESTRUCT
	/* declare fields for resource serialization */
	void DeclareStruct(datTypeStruct &s)
	{
		pgBase::DeclareStruct(s);
		STRUCT_BEGIN(CPedRecordingRsc);
		STRUCT_FIELD(m_RawData);
		STRUCT_END();
	}
#endif

private:
	/* array containing raw serialized recording data */
	atArray<u32, 0, u32> m_RawData;
};

#endif //FW_CONTROL_PEDRECORDING_H