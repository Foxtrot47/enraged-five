#ifndef FW_CONTROL_RECORD_H
#define FW_CONTROL_RECORD_H

#define SPEEDMULT (32767.0f / 120.0f)	// Max speed = 120 m/s = 432km/h

class CVehicleStateEachFrame
{
public:
	u32	TimeInRecording;
	s16	SpeedX, SpeedY, SpeedZ;
	s8	Matrix_a_x, Matrix_a_y, Matrix_a_z;
	s8	Matrix_b_x, Matrix_b_y, Matrix_b_z;
	s8	SteerAngle, Gas, Brake, HandBrake;
	float	CoorsX, CoorsY, CoorsZ;

	Vector3 GetCoors()	{ return Vector3(CoorsX, CoorsY, CoorsZ);}

	float	GetSpeedX() { return(SpeedX / SPEEDMULT); };
	float	GetSpeedY() { return(SpeedY / SPEEDMULT); };
	float	GetSpeedZ() { return(SpeedZ / SPEEDMULT); };

	void	SetSpeedX(float Speed) { SpeedX = (s16)(Speed * SPEEDMULT); };
	void	SetSpeedY(float Speed) { SpeedY = (s16)(Speed * SPEEDMULT); };
	void	SetSpeedZ(float Speed) { SpeedZ = (s16)(Speed * SPEEDMULT); };

	void	EndianSwap();

#if __DECLARESTRUCT
	/* declare fields for resource serialization */
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(CVehicleStateEachFrame);
		STRUCT_FIELD(TimeInRecording);
		STRUCT_FIELD(SpeedX);
		STRUCT_FIELD(SpeedY);
		STRUCT_FIELD(SpeedZ);
		STRUCT_FIELD(Matrix_a_x);
		STRUCT_FIELD(Matrix_a_y);
		STRUCT_FIELD(Matrix_a_z);
		STRUCT_FIELD(Matrix_b_x);
		STRUCT_FIELD(Matrix_b_y);
		STRUCT_FIELD(Matrix_b_z);
		STRUCT_FIELD(SteerAngle);
		STRUCT_FIELD(Gas);
		STRUCT_FIELD(Brake);
		STRUCT_FIELD(HandBrake);
		STRUCT_FIELD(CoorsX);
		STRUCT_FIELD(CoorsY);
		STRUCT_FIELD(CoorsZ);
		STRUCT_END();
	}
#endif

};

#define MAX_CARS_PLAYBACK_NUMBER (24)
#define MAX_CARS_RECORDED_NUMBER (24)


/**
 * Class: CVehicleRecording
 *
 * CVehicleRecording encapsulates a single vehicle recording
 * represented as an array of states (CVehicleStateEachFrame)
 *
 */
class CVehicleRecording : public pgBase
{
public:

	CVehicleRecording() { }

	/* resource ctor */
	CVehicleRecording(datResource& rsc) : m_States(rsc) { }

	enum { RORC_VERSION = 1 };

	u32 GetNumStates() { return m_States.GetCount(); }

	CVehicleStateEachFrame * GetStates() { return m_States.GetElements(); }

	/* smooth out recording time steps */
	void Smooth()
	{
		u8	*pRecording = (u8 *) GetStates();

		s32 recordingSize = GetNumStates() * sizeof(CVehicleStateEachFrame);
		s32	IndexInArray = sizeof(CVehicleStateEachFrame);
		while (IndexInArray < recordingSize - (s32)sizeof(CVehicleStateEachFrame))
		{
			CVehicleStateEachFrame	*pOldState = (CVehicleStateEachFrame *) &(pRecording[IndexInArray-sizeof(CVehicleStateEachFrame)]);
			CVehicleStateEachFrame	*pCurrentState = (CVehicleStateEachFrame *) &(pRecording[IndexInArray]);
			CVehicleStateEachFrame	*pNewState = (CVehicleStateEachFrame *) &(pRecording[IndexInArray+sizeof(CVehicleStateEachFrame)]);

			pCurrentState->TimeInRecording = (u32)((pOldState->TimeInRecording + pNewState->TimeInRecording) * 0.5f);

			IndexInArray += sizeof(CVehicleStateEachFrame);
		}
	}

	/* loads data from file - this fn is used by ragebuilder */
#if __RESOURCECOMPILER
	void Load(const char * filename)
	{
		fiStream * pStream = ASSET.Open(filename, "", true);
		Assert(pStream);

		u8 * pRawData;
	
		sysMemStartTemp();
		pRawData = rage_new u8[pStream->Size()];
		sysMemEndTemp();

		Assert(pRawData);
		s32 udwSize = pStream->Read(pRawData, pStream->Size());
		Assert(udwSize == pStream->Size());
		pStream->Close();

		/* get rid of trailing zeroes */
		s32 IndexInFile = 0;
		s32 numElements = 0;
		while (IndexInFile < udwSize)
		{
			CVehicleStateEachFrame	*pVehState = (CVehicleStateEachFrame *) pRawData;
			pVehState += IndexInFile / sizeof(CVehicleStateEachFrame);
			if (pVehState->TimeInRecording == 0 && IndexInFile != 0)
			{	// We've found the end of the file. (fix end and jump out of loop)
				udwSize = IndexInFile;
			}
			else
			{
				numElements++;
			}

			IndexInFile += sizeof(CVehicleStateEachFrame);
		}

		m_States.Resize(numElements);

		for (int i=0; i<numElements; i++)
		{
			CVehicleStateEachFrame * pState = (CVehicleStateEachFrame *) pRawData;
			pState += i;
			memcpy(&m_States[i], pState, sizeof(CVehicleStateEachFrame));
		}
		//this causes problems with high speed recordings
		//Smooth();
	}
#else
	void Load(const char *) {}
#endif
	
	/* declare place function which calls rsc ctor */
	DECLARE_PLACE(CVehicleRecording);

#if __DECLARESTRUCT
	/* declare fields for resource serialization */
	void DeclareStruct(datTypeStruct &s)
	{
		pgBase::DeclareStruct(s);
		STRUCT_BEGIN(CVehicleRecording);
		STRUCT_FIELD(m_States);
		STRUCT_END();
	}
#endif

private:
	/* array of states for each recording sample */
	atArray<CVehicleStateEachFrame> m_States;

};

#endif //#define FW_CONTROL_RECORD_H
