
#include "ropedatamanager.h"
#include "bank/bank.h"
#include "vfx/vfxutil.h"

#include "ropedatamanager_parser.h"

namespace rage
{


ropeDataManager ropeDataManager::ms_Instance;
int	ropeDataManager::txdStatus = ROPE_TXD_UNLOADED;
strLocalIndex ropeDataManager::txdSlot = strLocalIndex(-1);
int ropeDataManager::txdLoadRefCount = 0;

const char* s_ropeTxdName = "platform:/textures/rope";

ropeData* ropeDataManager::GetTypeDataByIndex(s32 iIndex)
{
	if(iIndex >= 0 && iIndex < ms_Instance.m_TypeData.GetCount())
	{
		return ms_Instance.m_TypeData[iIndex];
	}
	return NULL;
}

void ropeDataManager::Init()
{
	ropeData::InitPool( MEMBUCKET_PHYSICS );
	txdStatus = ROPE_TXD_UNLOADED;
	txdLoadRefCount = 0;
	txdSlot = -1;
	Load();
}

void ropeDataManager::Shutdown()
{
	Reset();
	ropeData::ShutdownPool();
}

void ropeDataManager::Reset()
{
	if(txdSlot != -1)
	{
		// Force refcount
		txdLoadRefCount = 1;
		UnloadRopeTexures();
	}

	for(s32 i = 0; i < ms_Instance.m_TypeData.GetCount(); i++)
	{
		delete ms_Instance.m_TypeData[i];
	}
	ms_Instance.m_TypeData.Reset();
}

void ropeDataManager::Load()
{

#if __BANK
	atArray<u32> indices(0, ms_Instance.m_TypeData.GetCount());
	for(s32 i = 0; i < ms_Instance.m_TypeData.GetCount(); i++)
	{
/*		indices.Push(ms_Instance.m_TypeData[i]->GetHash());*/
	}
#endif // __BANK

	Reset();

	PARSER.LoadObject("common:/data/ropedata", "xml", ms_Instance, &parSettings::sm_StrictSettings);

#if __BANK		
//	ropeDataDebug::InitWidgets();
#endif // __BANK
}

void	ropeDataManager::LoadRopeTexures()
{
	if(	txdSlot == -1 )
	{
		txdSlot = g_TxdStore.FindSlot(s_ropeTxdName);
		Assertf( txdSlot != -1, "ropeDataManager::LoadRopeTexures() - Texture dictionary 'rope' not found" );
		strIndex index = g_TxdStore.GetStreamingIndex(txdSlot);
		strStreamingEngine::GetInfo().RequestObject(index, STRFLAG_DONTDELETE|STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD);
		txdStatus = ROPE_TXD_LOADING;
	}
	if(txdSlot != -1)
	{
		txdLoadRefCount++;
	}
}

void ropeDataManager::UpdateRopeTextures()
{
	if(txdStatus == ROPE_TXD_LOADING)
	{
		strIndex index = g_TxdStore.GetStreamingIndex(txdSlot);
		if( strStreamingEngine::GetInfo().GetStreamingInfoRef(index).GetStatus() == STRINFO_LOADED )
		{
			g_TxdStore.AddRef(txdSlot, REF_OTHER);
			strStreamingEngine::GetInfo().ClearRequiredFlag(index, STRFLAG_DONTDELETE);

			for(s32 i = 0; i < ms_Instance.m_TypeData.GetCount(); i++)
			{
#if ENABLE_DEFRAGMENTATION
				grcTexture **ppTexture;
#endif
				grcTexture *pTexture = g_TxdStore.Get(strLocalIndex(ropeDataManager::GetTxdSlot()))->Lookup(ms_Instance.m_TypeData[i]->m_DiffuseTextureName.GetHash());
				if( pTexture )
				{
#if ENABLE_DEFRAGMENTATION
					ppTexture = (grcTexture**)pgHandleBase::Register(g_TxdStore.Get(strLocalIndex(ropeDataManager::GetTxdSlot()))->Lookup(ms_Instance.m_TypeData[i]->m_DiffuseTextureName.GetHash()));
					ms_Instance.m_TypeData[i]->m_DiffuseTexture = *ppTexture;
					(*ppTexture)->AddRef();
#else
					ms_Instance.m_TypeData[i]->m_DiffuseTexture = pTexture;
					pTexture->AddRef();
#endif
				}

				pTexture = g_TxdStore.Get(strLocalIndex(ropeDataManager::GetTxdSlot()))->Lookup(ms_Instance.m_TypeData[i]->m_NormalMapName.GetHash());
				if( pTexture )
				{
#if ENABLE_DEFRAGMENTATION
					ppTexture = (grcTexture**)pgHandleBase::Register(g_TxdStore.Get(strLocalIndex(ropeDataManager::GetTxdSlot()))->Lookup(ms_Instance.m_TypeData[i]->m_NormalMapName.GetHash()));
					ms_Instance.m_TypeData[i]->m_NormalMap = *ppTexture;
					(*ppTexture)->AddRef();
#else
					ms_Instance.m_TypeData[i]->m_NormalMap = pTexture;
					pTexture->AddRef();
#endif
				}
			}

			txdStatus = ROPE_TXD_LOADED;
		}
	}
}

void	ropeDataManager::UnloadRopeTexures()
{
	if( txdSlot != -1)
	{
		txdLoadRefCount--;
		Assert(txdLoadRefCount >= 0);

		if(txdLoadRefCount <= 0)
		{
			strIndex index = g_TxdStore.GetStreamingIndex(txdSlot);
			strStreamingEngine::GetInfo().ClearRequiredFlag(index, STRFLAG_DONTDELETE);

			if(txdStatus == ROPE_TXD_LOADED  )
			{
				// Clear out references to textures
				for(s32 i = 0; i < ms_Instance.m_TypeData.GetCount(); i++)
				{
					if( ms_Instance.m_TypeData[i]->m_DiffuseTexture )
					{
						ms_Instance.m_TypeData[i]->m_DiffuseTexture->Release();
						ms_Instance.m_TypeData[i]->m_DiffuseTexture = NULL;
					}

					if( ms_Instance.m_TypeData[i]->m_NormalMap )
					{
						ms_Instance.m_TypeData[i]->m_NormalMap->Release();
						ms_Instance.m_TypeData[i]->m_NormalMap = NULL;
					}
				}
				// Clear out reference to txd
				g_TxdStore.RemoveRef(txdSlot, REF_OTHER);
			}
			txdSlot = -1;
			txdStatus = ROPE_TXD_UNLOADED;
		}
	}
}

const char* ropeDataManager::GetRopeTxdName()
{
	return s_ropeTxdName;
}

#if __BANK

void ropeDataManager::AddWidgets(bkBank& bank)
{
	bank.PushGroup("Rope Texture Loading");
	bank.AddButton("Load", LoadRopeTexures);
	bank.AddButton("Unload", UnloadRopeTexures);
	bank.PopGroup();

	bank.PushGroup("RopeDataList");

	bank.AddButton("Load", Load);
	bank.AddButton("Save", Save);

	bank.PushGroup("List");
	for(s32 i = 0; i < ms_Instance.m_TypeData.GetCount(); i++)
	{
 		char title[128];
 		sprintf(title,"Rope Type %d",i);
 		bank.PushGroup(title);
 		PARSER.AddWidgets(bank, ms_Instance.m_TypeData[i]);
 		bank.PopGroup();
	}
	bank.PopGroup();
	bank.PopGroup();
}

void ropeDataManager::Save()
{
	AssertVerify(PARSER.SaveObject("common:/data/ropedata", "xml", &ms_Instance, parManager::XML));
}
#endif // __BANK


} // namespace rage