#ifndef FW_CLOTHDEBUG_H
#define FW_CLOTHDEBUG_H

#include "atl/array.h"
#include "vector/color32.h"

#define		USE_CLOTH_DEBUG				1 && (__BANK && !__RESOURCECOMPILER)
#define		USE_CLOTH_GIZMO				1 && (__BANK && !__RESOURCECOMPILER)

namespace rage
{
class Vector2;
class Vector3;
class grcViewport;
class clothController;

#if USE_CLOTH_DEBUG
class fwClothDebug
{
public:
	enum enBRUSH_SHAPE
	{
		BRUSH_RECTANGLE = 0,
		BRUSH_CIRCLE	= 1,
	};

public:
	static int sm_ClearSelectionKey;
	static int sm_ActivateSelectionKey;
	static int sm_ConfirmSelectionKey;

	static bool sm_bBrushShape;
	static bool sm_bBrushSet;

	static bool sm_AccumulateVerts;
	static bool sm_DistributeVerts;

	static Vector2 sm_StartPos;
	static Vector2 sm_EndPos;
	static Vector2 sm_CursorOffset;

	static int sm_SelectedVertsCount;
	static int sm_LastSelectedVertsCount;

	static atArray< int >		sm_SelectedVerts;
	static atArray< int >		sm_LastSelectedVerts;
	static atArray< Color32 >	sm_LastSelectedVertsColors;
	
public:

	static int GetConfirmSelectionKey();

	static Vector2 GetMouseInScreenSpace();
	static void DrawBrush(enBRUSH_SHAPE brushShape);

	static void UpdateSelection(bool &bAccumulated, bool& bDistributed, int& iSelectedCount, int& iLastSelectedCount,
		atArray<int>& iSelected, atArray<int>& iLastSelected, atArray<Color32>& iLastSelectedColors);

	static int PickVerticesScreenSpace( fwClothDebug::enBRUSH_SHAPE brushShape, const Vector3* RESTRICT verts, const Vector3& offset, const grcViewport* pViewport, const int 
		numVertices, const Vector2& v0, const Vector2& v1, int beginVertex, int endVertex, atArray<int>& vertsIndicesOut, int vertsStart );

	static bool IsVertexSelected(int vertexIndex);
	static void DrawPrimitivesInfo(int selectedCount, int lastSelectedCount, const char* pPrimitiveType);
	static void GetScreenPosFromWorldPoint( const grcViewport* pViewport, const Vector3& vWorldPos, Vector2& screenPos );

	static void SelectVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool pin );
	static void DrawSelection( const Vector3& offset, const Vector3* RESTRICT pClothVertices, float sphereRadius, float sphereRadiusSelected, bool bDrawInfo = true );

	static void DragVerts( const Vector3& offset, clothController* pClothController, const grcViewport* pViewport );

	static void PinUnPinVerts( clothController* pClothController, const grcViewport* pViewport, bool pin );
};

extern bool BoxVsSegment2D(const Vector2& boxCenter, const Vector2& boxExtents, const Vector2& boxAxisX, const Vector2& boxAxisY, const Vector2& segmentPt0, const Vector2& segmentPt1 );

#endif // USE_CLOTH_DEBUG


#if USE_CLOTH_GIZMO

class fwClothGizmo
{
public:

	fwClothGizmo() 
		: m_OriginalMat(NULL)
		, m_GismoMat(V_IDENTITY)
		, m_AxisSelected(0)
		, m_MouseDrag(false)
		, m_fRotation(0.0f)
		, m_Mode(false)
		, m_Translation(Vec3V(V_ZERO))
	{
	}

	void Draw();
	void Update(const grcViewport* pViewport);
	void SetMatrix(const Mat34V* mat) { m_OriginalMat = mat; }
	const Mat34V* GetMatrix() const { return m_OriginalMat; }

	void ResetGizmo()
	{
		m_OriginalMat = NULL;
		m_GismoMat.SetIdentity3x3();
		m_GismoMat.SetCol3( Vec3V(V_ZERO) );
		m_AxisSelected = 0;
		m_MouseDrag = false;
		m_Mode = false;
		m_fRotation = 0.0f;
		m_Translation = Vec3V(V_ZERO);
	}	

	void SetGizmoMatrixPos(Vec3V_In pos)
	{
		m_GismoMat.SetCol3( pos );
	}

	void PickAxisScreenSpace( const grcViewport* pViewport, const Vector2& v0, const Vector2& v1 );
	void GetTransform(Mat34V_InOut m, Vec3V_InOut translation);
	void ToggleMode() { m_Mode = !m_Mode; }

	Vector4& GetGizmoPlaneColor() { return m_GizmoPlaneColor; }

public:
	static Vector4 sm_GismoPlaneColor;

protected:

	Vector4 m_GizmoPlaneColor;
	const Mat34V* m_OriginalMat;
	Mat34V	m_GismoMat;
	Vec3V m_Translation;
	int m_AxisSelected;	
	bool m_Mode;			//	0 - translation, 1 - rotation
	Vector2 m_MouseStart;
	Vector2 m_MouseEnd;
	float m_fRotation;
	bool m_MouseDrag;
};

#endif // USE_CLOTH_GIZMO

} // namespace rage

#endif // FW_CLOTHDEBUG_H
