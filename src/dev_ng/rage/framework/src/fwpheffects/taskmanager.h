#ifndef FW_TASKMANAGER_H
#define FW_TASKMANAGER_H

#include "data/base.h"
#include "cloth/environmentcloth.h" 
#include "cloth/charactercloth.h" 
#include "ropemanager.h"

namespace rage
{

#define		FRAMES_TO_WAIT		300


class verletTaskManager : public datBase
{
public:

	verletTaskManager() : m_NumTasks(0)
#if (__BANK)
		, m_Countdown(FRAMES_TO_WAIT) 
#endif
	{}
	~verletTaskManager() {}

	struct verletTask 
	{ 
		sysTaskHandle h; 		
		void*	data; 
		bool	isRope;
	};

	void AddTask(sysTaskHandle h, void* data = 0, bool isRope = false )
	{
		if (!h)
		{
			Assert( 0 );
			return;	
		}

		Assert( m_NumTasks < MAX_TASKS );
		verletTask& t	= m_Tasks[m_NumTasks];
		t.h				= h;
		t.data			= data;
		t.isRope		= isRope;

		++m_NumTasks;
	}

	// NOTE: Must be done before grmModelQB buffers are swapped
	void WaitForTasks()
	{
#if (__BANK)
		utimer_t startTime = sysTimer::GetTicks();
#endif
		for(int i = 0; i < m_NumTasks; ++i )
		{
			verletTask& t = m_Tasks[i];
			sysTaskManager::Wait( t.h );
			
#if __PS3
			PostSpuUpdate(t);
#endif
		}
		m_NumTasks = 0;
#if (__BANK)
		float fWaitTime = (sysTimer::GetTicksToMicroseconds() * (sysTimer::GetTicks() - startTime));
		m_WaitTime = (fWaitTime > m_WaitTime ? fWaitTime : m_WaitTime);

		m_Countdown--;
		if( m_Countdown < 0 )
		{
			m_Countdown = FRAMES_TO_WAIT;
			m_WaitTime = 0.0f;
		}
#endif
	}

#if __PS3
	void PostSpuUpdate(verletTask& t)
	{
		Assert( t.data );
		if( t.isRope )
#if UPDATE_BATCH
			((ropeManager*)t.data)->UpdateBatchPostSpu();
#else
			((clothBase*)t.data)->UpdateRopePostSpu();
#endif
		else
			((clothBase*)t.data)->UpdateClothPostSpu();
	}
#endif //__PS3

	static const int MAX_TASKS = 128;
	int GetTasksCount() const { return m_NumTasks; }
#if (__BANK)
	float GetWaitTime() const { return m_WaitTime; }
#endif

protected:	
	verletTask m_Tasks[MAX_TASKS];
	int m_NumTasks;
#if (__BANK)
	float m_WaitTime;			// used to measure the stall on the update thread
	int m_Countdown;
#endif
};



class clothInstanceTaskManager : public datBase
{
public:

	clothInstanceTaskManager() : m_NumTasks(0)
#if (__BANK)
		, m_WaitTime(0.0f), m_Countdown(FRAMES_TO_WAIT) 
#endif
	{}
	~clothInstanceTaskManager() {}

	struct taskParams
	{ 
		sysTaskHandle h; 		
		void*	data; 
	};

	void AddTask(sysTaskHandle h, void* data = 0 )
	{
		if (!h)
		{
			Assert( 0 );
			return;	
		}

		FatalAssertf( m_NumTasks < MAX_INSTANCE_TASKS, "%d overflow", MAX_INSTANCE_TASKS);
		taskParams& t	= m_Tasks[m_NumTasks];
		t.h				= h;
		t.data			= data;

		++m_NumTasks;
	}

	void WaitForTasks()
	{
#if (__BANK)
		utimer_t startTime = sysTimer::GetTicks();
#endif
		for(int i = 0; i < m_NumTasks; ++i )
		{
			taskParams& t = m_Tasks[i];
			sysTaskManager::Wait( t.h );
		}
		m_NumTasks = 0;
#if (__BANK)
		float fWaitTime = (sysTimer::GetTicksToMicroseconds() * (sysTimer::GetTicks() - startTime));
		m_WaitTime = (fWaitTime > m_WaitTime ? fWaitTime : m_WaitTime);

		m_Countdown--;
		if( m_Countdown < 0 )
		{
			m_Countdown = FRAMES_TO_WAIT;
			m_WaitTime = 0.0f;
		}
#endif
	}

	static const int MAX_INSTANCE_TASKS = 320;
	int GetTasksCount() const { return m_NumTasks; }
#if (__BANK)
	float GetWaitTime() const { return m_WaitTime; }
#endif

protected:	
	taskParams m_Tasks[MAX_INSTANCE_TASKS];
	int m_NumTasks;
#if (__BANK)
	float m_WaitTime;			// used to measure the stall on the update thread
	int m_Countdown;
#endif
};



} // namespace rage

#endif // FW_TASKMANAGER_H
