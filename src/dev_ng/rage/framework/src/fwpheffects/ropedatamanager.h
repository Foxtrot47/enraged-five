#ifndef FW_ROPEDATAMGR_H
#define FW_ROPEDATAMGR_H

#include "parser/manager.h"
#include "ropemanager.h"
#include "fwscene/stores/txdstore.h"


namespace rage
{

class ropeDataManager : public datBase
{
public:

	static void Init();
	static void Shutdown();

	static ropeData* GetTypeDataByIndex(s32 iIndex);
	static s32 GetTypeDataCount()
	{
		return ms_Instance.m_TypeData.GetCount();
	}

#if __BANK
	static void AddWidgets(bkBank& bank);
#endif

	static int GetTxdSlot() { return txdSlot.Get(); }

	// The class actually implements a double-indriect texture pointer, so handle that here.
	static const char* GetTextureNameCb(grcTexture** in)
	{
		return NULL == in || NULL == *in ? "NONE" : (*in)->GetName();
	}

#if ENABLE_DEFRAGMENTATION
	// This is a bit magical -- once we look up the texture, we need to register it with the pgHandle
	// system and cast the result.  This lets the parser store the pointer-to-pointer in the destination
	// instead of being off by a level of indirection.  In a future, better world, parCodeGen will have
	// native support for both types of defragmentable pointers.
	static grcTexture** FindTextureCb(const char* name)
	{
		return (grcTexture**)pgHandleBase::Register(g_TxdStore.Get(strLocalIndex(ropeDataManager::GetTxdSlot()))->Lookup(name));
	}
#endif

	const char* GetTypeDataName(int idx)
	{
		Assert( m_TypeData[idx] );
		return m_TypeData[idx]->GetName();
	}

	static ropeDataManager* GetRopeDataMgr() { return &ms_Instance; }


	static void	LoadRopeTexures();
	static void UpdateRopeTextures();
	static void	UnloadRopeTexures();
	static bool AreRopeTexturesLoaded()	{ return txdStatus == ROPE_TXD_LOADED; }

	static const char* GetRopeTxdName();

private:

	enum
	{
		ROPE_TXD_UNLOADED = 0,
		ROPE_TXD_LOADING,
		ROPE_TXD_LOADED
	};

	static void Reset();
	static void Load();

#if __BANK
	static void Save();
#endif

	static int	txdStatus;
	static strLocalIndex txdSlot;
	static int	txdLoadRefCount;

	atArray<ropeData*> m_TypeData;
	static ropeDataManager ms_Instance;

	PAR_SIMPLE_PARSABLE;
};

} // namespace rage

#endif // FW_ROPEDATAMGR_H
