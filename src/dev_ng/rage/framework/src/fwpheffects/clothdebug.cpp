#include "clothdebug.h"
#if __BANK
#include "grcore/debugdraw.h"
#if !__RESOURCECOMPILER
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/pad.h"
#include "input/mouse.h"
#include "pheffects/mouseinput.h"
#endif
#endif

#include "grcore/viewport.h"
#include "vector/colors.h"
#include "cloth/clothcontroller.h"
#include "cloth/environmentcloth.h"

#include "grprofile/drawmanager.h"

namespace rage
{

#if USE_CLOTH_GIZMO
	EXT_PFD_DECLARE_ITEM( ClothGizmo );
#endif



#if USE_CLOTH_DEBUG

float fTextVerticalOffset		= 0.012f;

int	s_ConfirmBrushKey						= KEY_Y;
int fwClothDebug::sm_ClearSelectionKey		= KEY_E;
int fwClothDebug::sm_ConfirmSelectionKey	= KEY_Z;
int fwClothDebug::sm_ActivateSelectionKey	= KEY_LCONTROL;

bool fwClothDebug::sm_bBrushShape		= BRUSH_RECTANGLE;
bool fwClothDebug::sm_bBrushSet			= false;
bool fwClothDebug::sm_AccumulateVerts	= false;
bool fwClothDebug::sm_DistributeVerts	= false;

Vector2		fwClothDebug::sm_StartPos;
Vector2 	fwClothDebug::sm_EndPos;
Vector2		fwClothDebug::sm_CursorOffset;

int			fwClothDebug::sm_SelectedVertsCount		= 0;
int			fwClothDebug::sm_LastSelectedVertsCount	= 0;

atArray< int >		fwClothDebug::sm_SelectedVerts;
atArray< int >		fwClothDebug::sm_LastSelectedVerts;
atArray< Color32 >	fwClothDebug::sm_LastSelectedVertsColors;


Vector4 fwClothGizmo::sm_GismoPlaneColor = Vector4(0.54f,0.17f,0.89f, 0.75f);		// Color_BlueViolet


// NOT USED AT THIS TIME
bool BoxVsLine2D(const Vector2& boxCenter, const Vector2& boxExtents, const Vector2& boxAxisX, const Vector2& boxAxisY, const Vector2& line0, const Vector2& line1 )
{
	const Vector2 diff = line0 - boxCenter;
	const Vector2 lineDir = line1 - line0;
	const Vector2 perp( lineDir.y, -lineDir.x );

	const float leftr = fabsf( perp.Dot(diff) );
	const float part0 = fabsf( perp.Dot(boxAxisX) );
	const float part1 = fabsf( perp.Dot(boxAxisY) );
	const float rightr = boxExtents.x*part0 + boxExtents.y*part1;
	return leftr <= rightr;
}

// Used
bool BoxVsSegment2D(const Vector2& boxCenter, const Vector2& boxExtents, const Vector2& boxAxisX, const Vector2& boxAxisY, const Vector2& segmentPt0, const Vector2& segmentPt1 )
{
	const Vector2 segmentCenter = (segmentPt0 + segmentPt1) * 0.5f;
	Vector2 segmentDir = (segmentPt1 - segmentPt0);
	float segmentLen = segmentDir.Mag();
	float segmentExtent = segmentLen * 0.5f;

	const Vector2 diff = segmentCenter - boxCenter;

	float AWdU[2], ADdU[2], rhs;
	AWdU[0] = fabsf(segmentDir.Dot(boxAxisX));
	ADdU[0] = fabsf(diff.Dot(boxAxisX));
	rhs = boxExtents.x + segmentExtent*AWdU[0];
	if (ADdU[0] > rhs)
	{
		return false;
	}

	AWdU[1] = fabsf(segmentDir.Dot(boxAxisY));
	ADdU[1] = fabsf(diff.Dot(boxAxisY));
	rhs = boxExtents.y + segmentExtent*AWdU[1];
	if (ADdU[1] > rhs)
	{
		return false;
	}

	const Vector2 perp( segmentDir.y, -segmentDir.x );
	float lhs = fabsf(perp.Dot(diff));
	float part0 = fabsf(perp.Dot(boxAxisX));
	float part1 = fabsf(perp.Dot(boxAxisY));
	rhs = boxExtents.x*part0 + boxExtents.y*part1;
	return lhs <= rhs;
}

int fwClothDebug::GetConfirmSelectionKey()
{
	return sm_ConfirmSelectionKey;
}

bool fwClothDebug::IsVertexSelected(int vertexIndex)
{
	for( int k = 0; k < sm_LastSelectedVertsCount; ++k )
	{
		if( vertexIndex == sm_LastSelectedVerts[k] )
		{
			sm_LastSelectedVertsColors[k] = Color_red;
			return true;
		}
	}
	return false;
}

void fwClothDebug::GetScreenPosFromWorldPoint( const grcViewport* pViewport, const Vector3& vWorldPos, Vector2& screenPos )
{
	float screenX, screenY;
	Assert( pViewport );
	pViewport->Transform((Vec3V&)vWorldPos, screenX, screenY);

	screenPos.x = ( screenX /* * grcViewport::GetDefaultScreen()->GetWidth()*/ ) / pViewport->GetWidth();
	screenPos.y = ( screenY /* * grcViewport::GetDefaultScreen()->GetHeight()*/ ) / pViewport->GetHeight();
}

Vector2 fwClothDebug::GetMouseInScreenSpace()
{
	return Vector2(	static_cast<float>(ioMouse::GetX()) / grcViewport::GetDefaultScreen()->GetWidth(), 
		static_cast<float>(ioMouse::GetY()) / grcViewport::GetDefaultScreen()->GetHeight()	);
}

void fwClothDebug::DrawBrush(enBRUSH_SHAPE brushShape)
{
	if( ioMapper::DebugKeyDown(s_ConfirmBrushKey) )
	{
		if( !sm_bBrushSet )
		{			
			sm_bBrushSet = true;
			sm_StartPos = GetMouseInScreenSpace();
		}

		if( sm_bBrushSet )
		{
			sm_EndPos = GetMouseInScreenSpace();
			sm_CursorOffset = sm_EndPos - sm_StartPos;
		}
	}
	else
	{
		sm_bBrushSet = false;
		sm_StartPos = GetMouseInScreenSpace();
		sm_EndPos = sm_StartPos - sm_CursorOffset;
	}

	if( brushShape == fwClothDebug::BRUSH_CIRCLE )
	{
		Vector2 brushCenter = (sm_StartPos + sm_EndPos) * 0.5f;
		float brushRadius = (sm_EndPos - sm_StartPos).Mag() * 0.5f;
		grcDebugDraw::Circle( brushCenter, brushRadius, Color_white, false );
	}
	else if( brushShape == fwClothDebug::BRUSH_RECTANGLE )
	{
		Vector2 v0 = sm_StartPos,
				v1 = sm_StartPos,
				v2 = sm_EndPos,
				v3 = sm_EndPos;
		v1.x = v3.x;
		v2.x = v0.x;
		grcDebugDraw::Quad(v0, v2, v3, v1, Color_white, false, 1 );
// 		grcDebugDraw::Text(v0, Color_white, "v0");
// 		grcDebugDraw::Text(v1, Color_white, "v1");
// 		grcDebugDraw::Text(v2, Color_white, "v2");
// 		grcDebugDraw::Text(v3, Color_white, "v3");
	}
}


void fwClothDebug::UpdateSelection(bool &bAccumulated, bool& bDistributed, int& iSelectedCount, int& iLastSelectedCount,
	atArray<int>& iSelected, atArray<int>& iLastSelected, atArray<Color32>& iLastSelectedColors)
{
	if( ioMapper::DebugKeyPressed(sm_ConfirmSelectionKey) )
	{
		if( bAccumulated )
		{
			// NOTE: weed out dup primitives
			for(int i = 0; i < iSelectedCount; ++i )
			{
				int k = 0;
				for( k = 0; k < iLastSelectedCount; ++k )
				{
					if( iSelected[i] == iLastSelected[k] )
						break;
				}
				if( k == iLastSelectedCount )
				{
					iLastSelected[iLastSelectedCount] = iSelected[i];
					iLastSelectedColors[iLastSelectedCount] = Color_green;
					iLastSelectedCount++;
				}
			}			
			bAccumulated = false;
		}

		if( bDistributed )
		{
			// NOTE: de-select dup primitives
			for(int i = 0; i < iSelectedCount; ++i )
			{
				for( int k = 0; k < iLastSelectedCount; ++k )
				{
					if( iSelected[i] == iLastSelected[k] )
					{
						Assert( iLastSelectedCount > 0 );
						--iLastSelectedCount;
						Assert( iLastSelectedCount || !k );		// NOTE: consistency check
						iLastSelected[k] = iLastSelected[iLastSelectedCount];
						break;
					}
				}
			}
			bDistributed = false;
		}
		iSelectedCount = 0;
	}	
}


int fwClothDebug::PickVerticesScreenSpace( fwClothDebug::enBRUSH_SHAPE brushShape, const Vector3* RESTRICT verts, const Vector3& offset, const grcViewport* pViewport, const int 
#if __ASSERT
	numVertices
#endif
	, const Vector2& v0, const Vector2& v1, int beginVertex, int endVertex, atArray<int>& vertsIndicesOut, int vertsStart )
{
	Assert( pViewport );
	Assert( beginVertex > -1 );
	Assert( endVertex <= numVertices );
	//	Assert( !m_UseDebugPose || (m_DebugPose.GetCount() == numVertices) );
	Assert( verts );
	int vertsInArea = 0;

	for(int i = beginVertex; i < endVertex; ++i )
	{
		Vector3 worldPos = verts[i] + offset;
		Vector2 screenPos;
		GetScreenPosFromWorldPoint( pViewport, worldPos, screenPos );

		bool bVertPicked = false;
		if( brushShape == fwClothDebug::BRUSH_CIRCLE )
		{
			const float brushRadius = (v0 - v1).Mag() * 0.5f;
			Vector2 brushCenter = (v0 + v1)*0.5f;

			const float distToCenter = (screenPos - brushCenter).Mag();
			if( distToCenter < brushRadius )
			{
				bVertPicked = true;
			}
		}
		else if( brushShape == fwClothDebug::BRUSH_RECTANGLE )
		{		
			if(		(screenPos.x > v0.x && screenPos.x < v1.x && screenPos.y > v0.y && screenPos.y < v1.y) 
				||  (screenPos.x > v1.x && screenPos.x < v0.x && screenPos.y > v1.y && screenPos.y < v0.y) 
				||	(screenPos.x > v0.x && screenPos.x < v1.x && screenPos.y < v0.y && screenPos.y > v1.y) 
				||  (screenPos.x > v1.x && screenPos.x < v0.x && screenPos.y < v1.y && screenPos.y > v0.y) 
				)
			{
				bVertPicked = true;
			}
		}

		if( bVertPicked )
		{
			Assert( vertsInArea < numVertices );
			Assert( (vertsStart + vertsInArea) < vertsIndicesOut.GetCapacity() );
			vertsIndicesOut[ vertsStart + vertsInArea ] = i;
			vertsInArea++;
		}

	}
	return vertsInArea;
}


void fwClothDebug::DrawPrimitivesInfo(int selectedCount, int lastSelectedCount, const char* pPrimitiveType)
{
	Assert( pPrimitiveType );
	const int totalSelected = (selectedCount + lastSelectedCount);
	if( totalSelected )
	{
		Vector2 mousePos = GetMouseInScreenSpace();
		char buff[64];

		mousePos.y -= fTextVerticalOffset;					
		formatf( buff, "selected %s: %d", pPrimitiveType, selectedCount );
		grcDebugDraw::Text(mousePos, Color_white, buff);

		mousePos.y -= fTextVerticalOffset;					
		formatf( buff, "total selected %s: %d", pPrimitiveType, (selectedCount + lastSelectedCount)) ;
		grcDebugDraw::Text(mousePos, Color_white, buff);
	}
}

void fwClothDebug::SelectVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool pin )
{
	Assert( pClothVertices );

	UpdateSelection(sm_AccumulateVerts, sm_DistributeVerts, sm_SelectedVertsCount, sm_LastSelectedVertsCount, sm_SelectedVerts, sm_LastSelectedVerts, sm_LastSelectedVertsColors);
	DrawBrush((fwClothDebug::enBRUSH_SHAPE)sm_bBrushShape);

	if ( pViewport )
	{				
		const int startVertex = !pin ? numPinVerts: 0;	
		const int endVertex = !pin ? numVerts: numPinVerts;

		if( ioMapper::DebugKeyPressed(sm_ClearSelectionKey) )
		{
			sm_SelectedVertsCount = 0;
			sm_LastSelectedVertsCount = 0;
		}
		else
		{
			sm_DistributeVerts = ioMapper::DebugKeyDown(sm_ActivateSelectionKey) ? true: false;
			sm_AccumulateVerts = !sm_DistributeVerts;
			sm_SelectedVertsCount = PickVerticesScreenSpace( (fwClothDebug::enBRUSH_SHAPE)sm_bBrushShape, pClothVertices, offset, pViewport, numVerts, sm_StartPos, sm_EndPos, startVertex, endVertex, sm_SelectedVerts, 0 );
		}
	}
}

void fwClothDebug::DrawSelection(const Vector3& offset, const Vector3* RESTRICT pClothVertices, float sphereRadius, float sphereRadiusSelected, bool bDrawInfo)
{
	if( bDrawInfo )
	{
		DrawPrimitivesInfo(sm_SelectedVertsCount, sm_LastSelectedVertsCount, "vertices");
	}

	for( int i = 0; i < sm_SelectedVertsCount; ++i )
	{
		const int vertIndex = sm_SelectedVerts[i];
		Vector3 clothVert = pClothVertices[vertIndex] + offset;
		if( !sm_DistributeVerts || !IsVertexSelected(vertIndex))
			grcDebugDraw::Sphere( clothVert, sphereRadiusSelected, Color_blue, false );
	}

	for( int k = 0; k < sm_LastSelectedVertsCount; ++k )
	{
		grcDebugDraw::Sphere( (pClothVertices[sm_LastSelectedVerts[k]] + offset), sphereRadius, sm_LastSelectedVertsColors[k], false );
		sm_LastSelectedVertsColors[k] = Color_green;
	}
}

void fwClothDebug::PinUnPinVerts( clothController* pClothController, const grcViewport* /*pViewport*/, bool pin )
{
	Assert( pClothController );
//	Assert( pViewport );

	const int iCurrentLOD = pClothController->GetLOD();
	phVerletCloth* pCloth = pClothController->GetCloth(iCurrentLOD);
	Assert( pCloth );	
	if( fwClothDebug::sm_LastSelectedVertsCount )
	{
		atFunctor4< void, int, int, int, bool > funcSwap;
		funcSwap.Reset< clothController, &clothController::OnClothVertexSwap >( pClothController );

// TODO: make it work for character cloth
// 		if( m_Type == clothManager::Environment || m_Type == clothManager::Vehicle )
// 		{
		environmentCloth* pEnvCloth = (environmentCloth*)pClothController->GetOwner();
		Assert( pEnvCloth );
		if( pin )
		{			
			pCloth->DynamicPinVerts( pEnvCloth->GetClothFrame(), iCurrentLOD, sm_LastSelectedVertsCount, sm_LastSelectedVerts, &funcSwap );
		}
		else
		{
			sysMemStartDebug();
			// TODO: ... unlock all verts ?
			const int numVerts = pCloth->GetNumVertices();
			atBitSet tempStateList;
			tempStateList.Init( numVerts );
			tempStateList.Reset();
			for(int i = 0; i < numVerts; ++i )
			{
				tempStateList.Set( i, true );
			}
			pCloth->DynamicUnPinVerts( pEnvCloth->GetClothFrame(), tempStateList, iCurrentLOD, sm_LastSelectedVertsCount, sm_LastSelectedVerts, &funcSwap );
			sysMemEndDebug();
		}
// 		else if( m_Type == clothManager::Character )
// 		{
// 			// TODO: do something
// 		}
	}
}

Vector2 oldMousePos(0.0f, 0.0f);
Vector2 curMousePos(0.0f, 0.0f);
bool bMousePressed = false;

#define MAX_DRAG_VERTS		16
ScalarV vDistV[MAX_DRAG_VERTS];


void fwClothDebug::DragVerts( const Vector3& offset, clothController* pClothController, const grcViewport* pViewport )
{
	Assert( pClothController );
	Assert( pViewport );

	phVerletCloth* pVerletCloth = pClothController->GetCloth( pClothController->GetLOD() );
	Assert( pVerletCloth );
	const Vector3* RESTRICT pClothVertices = (const Vector3* RESTRICT)pVerletCloth->GetClothData().GetVertexPointer();
	Assert( pClothVertices );

	oldMousePos = curMousePos;
	curMousePos = fwClothDebug::GetMouseInScreenSpace();

//	Vector2 diff = (oldMousePos - curMousePos);
//	float len = diff.Mag();

	// NOTE: screeen space coords need to be scaled back to teh viewport size
	Vec3V nearPos, farPos;
	pViewport->ReverseTransform( curMousePos.x*pViewport->GetWidth(), curMousePos.y*pViewport->GetHeight(), nearPos, farPos );

	fwClothDebug::SelectVerts( offset, pClothVertices, pVerletCloth->GetNumVertices(), 0, pViewport, false);
	const int totalSelectedVerts = (fwClothDebug::sm_SelectedVertsCount+fwClothDebug::sm_LastSelectedVertsCount);
	if( totalSelectedVerts > 0 )
	{	
		fwClothDebug::DrawSelection(offset, pClothVertices, 0.06f, 0.05f);

		if( ioMapper::DebugKeyDown(sm_ActivateSelectionKey) )
		{
			const int vertIndexDrag = fwClothDebug::sm_LastSelectedVerts[0];
			Vector3 clothVertDrag = pClothVertices[vertIndexDrag] + offset;

			const Mat34V camMat = pViewport->GetCameraMtx();

			Vec3V camPos = camMat.GetCol3();
			Vec3V planePos = VECTOR3_TO_VEC3V(clothVertDrag);
			Vec3V planeNormal =  Normalize( Subtract( planePos, camPos) );

			ScalarV signOnly = Dot( Subtract( nearPos, planePos), planeNormal );
			ScalarV d = Subtract( Dot(planeNormal, planePos), Dot(planeNormal, nearPos) );

			Vec3V projVert = SelectFT( IsLessThan( signOnly, ScalarV(V_ZERO) ), nearPos, AddScaled( nearPos, planeNormal, d ) );	// use the projected vtx on the plane if there is penetration

//			grcDebugDraw::Sphere( projVert, 0.05f, Color_red, false );
//			grcDebugDraw::Line(planePos, Add(planePos, planeNormal), Color_yellow );
//			grcDebugDraw::Line(planePos, Add(planePos, rightV), Color_yellow2 );
//			grcDebugDraw::Line(planePos, nearPos, Color_yellow2 );

			int h = 0;
			if( !bMousePressed )
			{
				bMousePressed = (ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT);
				if( bMousePressed )
				{			
					h = 0;
					for( int i = 0; i < fwClothDebug::sm_SelectedVertsCount; ++i )
					{
						const int vertIdx = fwClothDebug::sm_SelectedVerts[i];
						Vector3 clothV = pClothVertices[vertIdx] + offset;
						vDistV[h++] = Mag( Subtract( VECTOR3_TO_VEC3V(clothV), projVert ) );
					}

					for( int k = 0; k < fwClothDebug::sm_LastSelectedVertsCount; ++k )
					{
						const int vertIdx = fwClothDebug::sm_LastSelectedVerts[k];
						Vector3 clothV = pClothVertices[vertIdx] + offset;
						vDistV[h++] = Mag( Subtract( VECTOR3_TO_VEC3V(clothV), projVert ) );
					}

// TODO: this is ugly, fix it
					if( !pVerletCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) )
					{

					}

				}
			}									
			else if ( (ioMouse::GetReleasedButtons()&ioMouse::MOUSE_LEFT) )
			{
				bMousePressed = false;
				for( int i = 0; i < MAX_DRAG_VERTS; ++i )
				{
					vDistV[i] = ScalarV(V_ZERO);
				}						
			}

			ScalarV _zero = ScalarVFromF32(MAGIC_ZERO);

			h = 0;
			for( int i = 0; i < fwClothDebug::sm_SelectedVertsCount; ++i )
			{
				const int vertIndex = fwClothDebug::sm_SelectedVerts[i];
				Vector3 clothVert = pClothVertices[vertIndex] + offset;
//				if( !fwClothDebug::sm_DistributeVerts || !fwClothDebug::IsVertexSelected(vertIndex))
				{
					grcDebugDraw::Line(clothVert, VEC3V_TO_VECTOR3(nearPos), Color_yellow2 );

					if( bMousePressed )
					{					
						Vec3V vTemp = Subtract( VECTOR3_TO_VEC3V(clothVert), projVert );
						vTemp = Normalize(vTemp);
						Vec3V newPos = AddScaled(projVert, vTemp, vDistV[h++] );
						newPos.SetW(_zero);
						pVerletCloth->GetClothData().SetVertexPosition(vertIndex, newPos);
					}
				}
			}

			for( int k = 0; k < fwClothDebug::sm_LastSelectedVertsCount; ++k )
			{
				const int vertIndex = fwClothDebug::sm_LastSelectedVerts[k];
				Vector3 clothVert = pClothVertices[vertIndex] + offset;

				grcDebugDraw::Line(clothVert, VEC3V_TO_VECTOR3(nearPos), Color_yellow2 );

				if( bMousePressed )
				{					
					Vec3V vTemp = Subtract( VECTOR3_TO_VEC3V(clothVert), projVert );
					vTemp = Normalize(vTemp);
					Vec3V newPos = AddScaled(projVert, vTemp, vDistV[h++] );
					newPos.SetW(_zero);
					pVerletCloth->GetClothData().SetVertexPosition(vertIndex, newPos);
				}
			}
		}
	}
}


#endif // USE_CLOTH_DEBUG




#if USE_CLOTH_GIZMO


void fwClothGizmo::Draw()
{
#if __PFDRAW
	if( PFD_ClothGizmo.Begin(true) )
	{
		Mat34V m = m_OriginalMat ? *m_OriginalMat: m_GismoMat;
		{			
			ScalarV _half(V_HALF);
			//Transform( m, *m_OriginalMat, m_GismoMat );

			if( !m_OriginalMat )
			{
				Mat34V mRot(V_IDENTITY), mTemp;
				Vec3V translation;
				GetTransform(mRot, translation);

				Transform( mTemp, m, mRot );				
				m.Set3x3( mTemp );
			}


			Vec3V vOrigin = Add( m.GetCol3(), m_Translation );

			if( m_Mode == true || !m_OriginalMat)
			{
				static float rotRad = 0.1f;

				grcColor(Color_red);
				grcDrawCircle( rotRad, VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(m.GetCol0()), VEC3V_TO_VECTOR3(m.GetCol2()), 11, false, m_AxisSelected == 0 ? true: false );

				grcColor(Color_green);
				grcDrawCircle( rotRad, VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(m.GetCol0()), VEC3V_TO_VECTOR3(m.GetCol1()), 11, false, m_AxisSelected == 1 ? true: false );

				grcColor(Color_yellow);
				grcDrawCircle( rotRad, VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(m.GetCol1()), VEC3V_TO_VECTOR3(m.GetCol2()), 11, false, m_AxisSelected == 2 ? true: false );
			}
			if( m_Mode == false || !m_OriginalMat) 
			{
				Vec3V vAt = rage::AddScaled( vOrigin, m.GetCol0(), _half );
				Vec3V vRight = rage::AddScaled( vOrigin, m.GetCol1(), _half );
				Vec3V vUp = rage::AddScaled( vOrigin, m.GetCol2(), _half );

				Color32 atAxisColor		= (m_AxisSelected == 0 ? Color_red: Color_white);
				Color32 rightAxisColor	= (m_AxisSelected == 1 ? Color_red: Color_white);
				Color32 upAxisColor		= (m_AxisSelected == 2 ? Color_red: Color_white);
				grcDrawLine( VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(vAt), atAxisColor );
				grcDrawLine( VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(vRight), rightAxisColor );
				grcDrawLine( VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(vUp), upAxisColor );

/*
				if( ioMapper::DebugKeyDown(KEY_RCONTROL) )
				{
					Vec3V xAxiz = (m_AxisSelected == 0 ? m.GetCol1() : (m_AxisSelected == 1 ? m.GetCol2() : m.GetCol0()) );
					Vec3V yAxiz = (m_AxisSelected == 0 ? m.GetCol2() : (m_AxisSelected == 1 ? m.GetCol0() : m.GetCol1()) );

					grcColor( Color32(sm_GismoPlaneColor) );
					grcDrawCircle( 1.0f, VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(xAxiz), VEC3V_TO_VECTOR3(yAxiz), 11, false, true );
				}
*/
			}

			grcColor( Color_white );

			Vec3V circlePos = vOrigin;

			char buf[256];
			formatf( buf, "Position: %f %f %f", vOrigin.GetXf(), vOrigin.GetYf(), vOrigin.GetZf() );
			grcDrawLabelf( VEC3V_TO_VECTOR3(circlePos), buf );

			Vec3V vNormal = ( m_AxisSelected == 0 ? m.GetCol0() : (m_AxisSelected == 1) ? m.GetCol1(): m.GetCol2() );

			float rowStep = 0.08f;
			circlePos.SetZf( circlePos.GetZf() - rowStep );
			formatf( buf, "Normal: %f %f %f", vNormal.GetXf(), vNormal.GetYf(), vNormal.GetZf() );
			grcDrawLabelf( VEC3V_TO_VECTOR3(circlePos), buf );

//			static float scaleV = 1.5f;
//			Vec3V boxHalfExt = (m_AxisSelected == 0 ? Add(m.GetCol1(), m.GetCol2()) : (m_AxisSelected == 1 ? Add(m.GetCol0(), m.GetCol2()): Add(m.GetCol0(), m.GetCol1()) ));
//			boxHalfExt = Scale( boxHalfExt, ScalarVFromF32(scaleV));
//			Vec3V minBB = Subtract( AddScaled(vOrigin, vOrigin, ScalarV(-0.0001f) ), boxHalfExt );
//			Vec3V maxBB = Add( vOrigin, boxHalfExt );
//			grcDrawSolidBox( VEC3V_TO_VECTOR3(minBB), VEC3V_TO_VECTOR3(maxBB), Color32(sm_GismoPlaneColor) );

			if( !m_OriginalMat )
			{
				Vec3V xAxiz = (m_AxisSelected == 0 ? m.GetCol0() : (m_AxisSelected == 1 ? m.GetCol0() : m.GetCol1()) );
				Vec3V yAxiz = (m_AxisSelected == 0 ? m.GetCol2() : (m_AxisSelected == 1 ? m.GetCol1() : m.GetCol2()) );

				grcColor( Color32(sm_GismoPlaneColor) );
				grcDrawCircle( 2.0f, VEC3V_TO_VECTOR3(vOrigin), VEC3V_TO_VECTOR3(xAxiz), VEC3V_TO_VECTOR3(yAxiz), 11, false, true );
			}
		}
		PFD_ClothGizmo.End();
	}
#endif
}

void fwClothGizmo::GetTransform(Mat34V_InOut m, Vec3V_InOut tr)
{
	if( m_AxisSelected == 0 )
		Mat34VRotateLocalX( m, ScalarVFromF32(m_fRotation) );
	else if( m_AxisSelected == 1 )
		Mat34VRotateLocalY( m, ScalarVFromF32(m_fRotation) );
	else if( m_AxisSelected == 2 )
		Mat34VRotateLocalZ( m, ScalarVFromF32(m_fRotation) );

	tr = m_Translation;
}

void fwClothGizmo::Update(const grcViewport* /*pViewport*/)
{
#if __PFDRAW
	if( PFD_ClothGizmo.GetEnabled() )
	{
		Mat34V& mat = m_OriginalMat ? (*const_cast<Mat34V*>(m_OriginalMat)): m_GismoMat;

		if( ioMouse::HasWheel() )
		{
			if( ioMouse::GetChangedButtons() & ioMouse::MOUSE_MIDDLE )
			{
				m_AxisSelected = ((++m_AxisSelected) % 3);
			}
		}

		if( ioMouse::GetReleasedButtons() & ioMouse::MOUSE_LEFT )
		{
			if( !m_MouseDrag )
			{
				m_MouseStart = fwClothDebug::GetMouseInScreenSpace();
			}
			else
			{				
				m_fRotation = 0.0f;
			}
			m_MouseDrag = !m_MouseDrag;
		}
		
		if( m_MouseDrag )
		{
			m_MouseEnd = fwClothDebug::GetMouseInScreenSpace();
			if( m_Mode == true || ioMapper::DebugKeyDown(KEY_LCONTROL) )
			{
				m_fRotation = m_MouseStart.x - m_MouseEnd.x;
			}
			else
			{
				ScalarV s = ScalarVFromF32(m_MouseStart.x - m_MouseEnd.x);
				
// TODO: use gizmo matrix instead
				if( m_AxisSelected == 0 )
					m_Translation = Scale( mat.GetCol0(), s );
				else if( m_AxisSelected == 1 )
					m_Translation = Scale( mat.GetCol1(), s );
				else if( m_AxisSelected == 2 )
					m_Translation = Scale( mat.GetCol2(), s );				
			}
			
		}

		if( ioMapper::DebugKeyPressed(fwClothDebug::sm_ConfirmSelectionKey) )
		{
			if( m_Mode == true || !m_OriginalMat )
			{
				Mat34V mRot(V_IDENTITY), m;
				Vec3V translation;
				GetTransform(mRot, translation);

				Transform( m, mat, mRot );				
				mat.Set3x3( m );
			}
			if( m_Mode == false || !m_OriginalMat )
			{
				mat.SetCol3( Add(mat.GetCol3(), m_Translation ) );
			}

			m_Translation = Vec3V(V_ZERO);			
			m_fRotation = 0.0f;
			m_MouseDrag = false;
		}
/*
		static float quadSize = -0.007f;
		Vector2 mouseStart = fwClothDebug::GetMouseInScreenSpace();
		Vector2 mouseEnd( mouseStart.x + quadSize, mouseStart.y + quadSize );

		Vector2 v0 = mouseStart,
				v1 = mouseStart,
				v2 = mouseEnd,
				v3 = mouseEnd;
		v1.x = v3.x;
		v2.x = v0.x;
		grcDebugDraw::Quad(v0, v2, v3, v1, Color_white, false, 1 );	

		PickAxisScreenSpace( pViewport, mouseStart, mouseEnd );
*/
	}
#endif
	
}

void fwClothGizmo::PickAxisScreenSpace( const grcViewport* pViewport, const Vector2& v0, const Vector2& v1 )
{
	Assert( m_OriginalMat );
	Assert( pViewport );

	const Vector2 boxCenter = (v0 + v1) * 0.5f;
	const Vector2 extents = (v0 - v1) * 0.5;
	const float extentX = fabsf(extents.x);
	const float extentY = fabsf(extents.y);
	const Vector2 boxExtents( extentX, extentY );
	const Vector2 boxAxisX(1.0f, 0.0f);
	const Vector2 boxAxisY(0.0f, 1.0f);

	Mat34V m;
	ScalarV _half(V_HALF);
	Transform( m, *m_OriginalMat, m_GismoMat );

	Vec3V vOrigin = m.GetCol3();
	Vec3V vAt = rage::AddScaled( vOrigin, m.GetCol0(), _half );
	Vec3V vRight = rage::AddScaled( vOrigin, m.GetCol1(), _half );
	Vec3V vUp = rage::AddScaled( vOrigin, m.GetCol2(), _half );

	Vector2 screenPos0, screenPos1, screenPos2, screenPos3;
	fwClothDebug::GetScreenPosFromWorldPoint( pViewport, VEC3V_TO_VECTOR3(vOrigin), screenPos0 );
	fwClothDebug::GetScreenPosFromWorldPoint( pViewport, VEC3V_TO_VECTOR3(vAt), screenPos1 );
	fwClothDebug::GetScreenPosFromWorldPoint( pViewport, VEC3V_TO_VECTOR3(vRight), screenPos2 );
	fwClothDebug::GetScreenPosFromWorldPoint( pViewport, VEC3V_TO_VECTOR3(vUp), screenPos3 );

//	grcDebugDraw::Line(screenPos0, screenPos1, Color_red );

	m_AxisSelected = BoxVsSegment2D( boxCenter, boxExtents, boxAxisX, boxAxisY, screenPos0, screenPos1 ) ? 0: -1;
	m_AxisSelected = (m_AxisSelected == -1 ? (BoxVsSegment2D( boxCenter, boxExtents, boxAxisX, boxAxisY, screenPos0, screenPos2 ) ? 1: -1) : m_AxisSelected);
	m_AxisSelected = (m_AxisSelected == -1 ? (BoxVsSegment2D( boxCenter, boxExtents, boxAxisX, boxAxisY, screenPos0, screenPos3 ) ? 2: -1) : m_AxisSelected);
}


#endif // USE_CLOTH_GIZMO

} // namespace rage

