
#if __BANK
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "bank/combo.h"
#include "bank/slider.h"
#include "grcore/debugdraw.h"
#if !__RESOURCECOMPILER
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/pad.h"
#include "input/mouse.h"
#include "pheffects/mouseinput.h"
#endif
#endif

#include "clothmanager.h"
#include "grcore/matrix43.h"
#include "grcore/indexbuffer.h"
#include "taskmanager.h" 
#include "parser/restparserservices.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundplane.h"
#include "phbullet/TriangleShape.h"
#include "pheffects/wind.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwscene/stores/clothstore.h"
#include "fwscene/stores/fragmentstore.h"
#include "fwsys/timer.h"
#include "streaming/streamingengine.h"
#include "system/memory.h"

#include "profile/page.h"
#include "profile/group.h"
#include "profile/element.h"
#include "grprofile/drawmanager.h"
#include "profile/profiler.h"

#include "clothmanager_parser.h"

// #pragma optimize("", off)

PARAM(noclothupdate, "disable cloth update, cloth is still created");
PARAM(forceclothweight, "overwrite all cloth weight");
PARAM(clothinfo, "show extra info at the cloth widget title");
PARAM(saveclothstate, "save cloth data when cloth is instanced");
PARAM(loadclothstate, "load cloth data when cloth is instanced");
PARAM(initclothuserdata, "init cloth user data when cloth is instanced");
PARAM(noclothmtinstance, "don't do multithreaded instancing");


namespace rage
{
#if !__SPU && !__RESOURCECOMPILER
AUTOID_IMPL(fwClothCollisionsExtension);
FW_INSTANTIATE_CLASS_POOL(fwClothCollisionsExtension, CONFIGURED_FROM_FILE, atHashString("fwClothCollisionsExtension",0xd6f6816e));
#endif
extern bool BoxVsSegment2D(const Vector2& boxCenter, const Vector2& boxExtents, const Vector2& boxAxisX, const Vector2& boxAxisY, const Vector2& segmentPt0, const Vector2& segmentPt1 );

sysCriticalSectionToken		s_LockActiveList;

const float	s_timeStepCloth = 0.01666667f;


extern bool g_PauseClothSimulation;

#if __PFDRAW
bool s_ShowStats = false;
bool s_ShowStatsActiveCloth = false;
bool s_ShowStatsInstanceCloth = false;
//	size_t s_MemRequested = 0;
size_t s_MemAllocated = 0;
size_t s_MemAllocatedChar = 0;
#endif


#if __PS3
const bool	sm_SpuUpdate = true;
const bool	sm_SpuCharacterClothUpdate = true;
#endif


EXT_PFD_DECLARE_ITEM( ClothVerts );
EXT_PFD_DECLARE_ITEM( ClothEdges );
EXT_PFD_DECLARE_ITEM( ClothCustomEdges );
EXT_PFD_DECLARE_ITEM( ClothInstanceCustomEdges );
EXT_PFD_DECLARE_ITEM( ClothVertIndices );
EXT_PFD_DECLARE_ITEM( ClothOriginalVertIndices );
EXT_PFD_DECLARE_ITEM( VerletCollision );
EXT_PFD_DECLARE_ITEM( VerletBounds );
EXT_PFD_DECLARE_ITEM( PickVerts );
EXT_PFD_DECLARE_ITEM( PickEdges );
EXT_PFD_DECLARE_ITEM( VertexValues );
EXT_PFD_DECLARE_ITEM( Ropes );
EXT_PFD_DECLARE_ITEM( SpuDebug );
EXT_PFD_DECLARE_ITEM( SkinnedMesh );
EXT_PFD_DECLARE_ITEM( CustomBounds );
EXT_PFD_DECLARE_ITEM( DebugRecords );

#if USE_CLOTH_GIZMO
EXT_PFD_DECLARE_ITEM( ClothGizmo );

fwClothGizmo g_ClothGizmo;
#endif // USE_CLOTH_GIZMO

#if RECORDING_VERTS

dbgRecords<recordLine> g_DbgRecordLines;
dbgRecords<recordSphere> g_DbgRecordSpheres;
dbgRecords<recordTriangle> g_DbgRecordTriangles;
dbgRecords<recordCapsule> g_DbgRecordCapsules;
dbgRecords<recordCustomClothEvent> g_DbgRecordCustomClothEvents;

bool g_EnableRecording = false;
bool g_PlayRecording = false;

#endif // RECORDING_VERTS


namespace phClothStats
{
	EXT_PF_TIMER(ClothManager);
};

using namespace phClothStats;


datCallback	g_ClothManagerCallback				= NullCB;
u32 clothManager::m_BuildBufferIndex			= 0;
u32 clothManager::m_DrawBufferIndex				= 0;

void (*clothManager::sm_pCaptureVerticesFunc)(void *pClothProvideAndCollectGlobalParam, void *pClothInstance, int numVertices, Vec3V *pVertices) = NULL;
void *clothManager::sm_pCaptureVerticesFuncGlobalParam = NULL;

clothManager::enPostInjectVerticesAction (*clothManager::sm_pInjectVerticesFunc)(void *pGlobalParam, void *pClothInstance, int numVertices, Vec3V *pVertices) = NULL;
void *clothManager::sm_pInjectVerticesFuncGlobalParam = NULL;

#if USE_CLOTHMANAGER_BANK

clothController* g_LastPickedClothController	= NULL;
bool s_RegisterInstanceRestInterface			= false;

bkBank*		pClothBank							= NULL;
bkButton*	pClothBankButton					= NULL;
clothManager*	pClothManager					= NULL;

const Mat34V* clothManager::ms_VehicleFrame		= NULL;
const crSkeleton* clothManager::ms_PedSkeleton	= NULL;
const rmcDrawable* clothManager::ms_pPedDrawable = NULL;
clothVariationData* clothManager::ms_PedClothVarData = NULL;


bool clothManager::ms_ClampAllForces			= false;
bool clothManager::ms_EnableAlpha				= false;
bool clothManager::ms_FlipAlpha					= false;
bool clothManager::ms_EnableVehiclePick			= false;
bool clothManager::ms_EnablePedPick				= false;
bool clothManager::ms_SceneIsFlushing			= false;

phMouseInput* clothManager::ms_MouseInput		= NULL;
bkGroup* clothManager::ms_RAGBankGroup			= NULL;
bkGroup* clothManager::ms_RAGBankSliders		= NULL;	

Vector3 clothManager::ms_DebugVertsColor		= (Color32(Color_red).GetRGB()).GetIntrin128();

const char* s_ComponentTags[] = { "PV_COMP_HEAD", "PV_COMP_BERD", "PV_COMP_HAIR", "PV_COMP_UPPR", "PV_COMP_LOWR", "PV_COMP_HAND",
	"PV_COMP_FEET", "PV_COMP_TEEF", "PV_COMP_ACCS", "PV_COMP_TASK", "PV_COMP_DECL", "PV_COMP_JBIB",};

int   clothManager::ms_DebugMatrixIndex			= 0;
int	  clothManager::ms_GeometryIndex			= 1;
int   clothManager::ms_ComponentIndex			= 3; // PV_COMP_UPPR

float clothManager::ms_DebugSphereRadius		= 0.005f;
float clothManager::ms_DebugEdgeWeight			= 0.5f;
float clothManager::ms_DebugEdgeCompWeight		= 0.25f;
float clothManager::ms_DebugEdgeLenSqr			= 0.005f;

float clothManager::ms_DebugBendEdgeWeight		= 0.5f;
float clothManager::ms_DebugBendEdgeCompWeight	= 0.7f;
float clothManager::ms_DebugBendEdgeLenSqr		= 0.005f;

float clothManager::ms_PinRadius				= 1.0f;
float clothManager::ms_VertexWeight				= 0.0025f;	
float clothManager::ms_ClothWeight				= DEFAULT_CLOTH_WEIGHT;
float clothManager::ms_InflationScale			= 0.0f;

float clothManager::ms_VertsCapacity			= 256.0f;
float clothManager::ms_EdgesCapacity			= 2048.0f;

char g_ClothNameBuf[MAX_CHARS_CLOTH_NAME];
char* clothManager::ms_ClothName = (char*)g_ClothNameBuf;


Vector3 s_GismoPosition(0.0f,0.0f,0.0f);
Vector3 s_DrawOffset(0.0f,0.0f,0.0f);
Vector3 s_BoundPosition(0.0f, 0.0f, 0.0f);
Vector3 s_BoundRotation(0.0f, 0.0f, 0.0f);
Vector3 s_InstanceOffset(0.0f, 1.0f, 0.0f);
Vector3 s_PinVertsOffset(0.0f, 0.0f, 0.0f);

#if ENABLE_SPU_DEBUG_DRAW
#define			MAX_ROPE_BATCH_SPU_DEBUG		1
#define			MAX_CHAR_CLOTH_SPU_DEBUG		15
#define			MAX_ENVT_CLOTH_SPU_DEBUG		16

#if (MAX_VERLET_SPU_DEBUG < (MAX_ROPE_BATCH_SPU_DEBUG + MAX_CHAR_CLOTH_SPU_DEBUG + MAX_ENVT_CLOTH_SPU_DEBUG))
#error "Can't debug more than MAX_VERLET_SPU_DEBUG verlets on the SPU!"
#endif

// NOTE: slot 0 is reserved for rope batching
int	s_VerletSpuDebugCounter = 1;
phVerletSPUDebug s_VerletSpuDebug[ MAX_ROPE_BATCH_SPU_DEBUG + MAX_CHAR_CLOTH_SPU_DEBUG + MAX_ENVT_CLOTH_SPU_DEBUG ] ;
#endif // ENABLE_SPU_DEBUG_DRAW


#define		MAX_CLOTH_DEBUG_VERTS		1024
#define		MAX_CLOTH_DEBUG_EDGES		8024


atArray< int >		s_SelectedEdges;
atArray< int >		s_LastSelectedEdges;
atArray< Color32 >	s_LastSelectedEdgesColors;

int s_SelectedEdgesCount		= 0;
int s_LastSelectedEdgesCount	= 0;

bool s_AccumulateEdges			= false;
bool s_DistributeEdges			= false;


const atArray<phEdgeData>* s_PickedEdgeData = NULL;

const char* s_PinRadiusSets[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };

#define		MAX_POSE_VERTS				2048 * 4
Vec3V* s_PoseOut = NULL;
Color32* s_PoseColor = NULL;
phMorphDataAoS*	s_MorphData = NULL;
phMorphData2AoS* s_OriginalMorphData = NULL;
int s_MorphDataCount = 0;
int s_VertexCount = 0;

// debug functions and primitive selection 
int s_ConfirmBlendSelectionKey = KEY_X;
int s_ToBlendVertexIndex = -1;

bool IsEdgeSelected(int edgeIndex)
{
	for( int k = 0; k < s_LastSelectedEdgesCount; ++k )
	{
		if( edgeIndex == s_LastSelectedEdges[k] )
		{
			s_LastSelectedEdgesColors[k] = Color_red;
			return true;
		}
	}
	return false;
}

void DrawEdgeSelectionInfo(const atArray<phEdgeData>& edgeData)
{
	char buf[128];
	float _x = 0.1f;
	float _y = 0.2f;
	float verticaloffset = 0.015f;
	Vector2 vTextRenderPos(_x, _y);

	for (int i=0; i < s_SelectedEdgesCount; ++i)
	{
		const int edgeIndex = s_SelectedEdges[i];
		const phEdgeData* edge = &edgeData[ edgeIndex ];
		Assert( edge );

		formatf( buf, "Edge - index: %d, Weight: %f, CompWeight: %f, LenSqr: %f, v0: %d, v1: %d", edgeIndex, edge->m_Weight0, edge->m_CompressionWeight, edge->m_EdgeLength2, edge->m_vertIndices[0], edge->m_vertIndices[1] );
		grcDebugDraw::Text(vTextRenderPos, Color_white, buf);
		vTextRenderPos.y += verticaloffset;
	}

	for (int i=0; i < s_LastSelectedEdgesCount; ++i)
	{
		const int edgeIndex = s_LastSelectedEdges[i];
		const phEdgeData* edge = &edgeData[ edgeIndex ];
		Assert( edge );

		formatf( buf, "Edge - index: %d, Weight: %f, CompWeight: %f, LenSqr: %f, v0: %d, v1: %d", edgeIndex, edge->m_Weight0, edge->m_CompressionWeight, edge->m_EdgeLength2, edge->m_vertIndices[0], edge->m_vertIndices[1] );
		grcDebugDraw::Text(vTextRenderPos, Color_white, buf);
		vTextRenderPos.y += verticaloffset;
	}
}

int PickEdgesScreenSpace( fwClothDebug::enBRUSH_SHAPE brushShape, const Vector3* RESTRICT verts, const Vector3& offset, const grcViewport* pViewport, const atArray<phEdgeData>& edgeData, const Vector2& v0, const Vector2& v1, atArray<int>& edgesOut, int edgeStart )
{
	Assert( pViewport );
	Assert( verts );

	const Vector2 boxCenter = (v0 + v1) * 0.5f;
	const Vector2 extents = (v0 - v1) * 0.5;
	const float extentX = fabsf(extents.x);
	const float extentY = fabsf(extents.y);
	const Vector2 boxExtents( extentX, extentY );
	const Vector2 boxAxisX(1.0f, 0.0f);
	const Vector2 boxAxisY(0.0f, 1.0f);

	int edgesInArea = 0;
	const int numEdges = edgeData.GetCount();
	for(int i = 0; i < numEdges; ++i )
	{
		const phEdgeData& edge = edgeData[i];
		const u16 vertIdx0 = edge.m_vertIndices[0];
		const u16 vertIdx1 = edge.m_vertIndices[1];
		if( vertIdx0 == vertIdx1 )
			continue;
		Vector3 worldPos0 = verts[vertIdx0] + offset;
		Vector3 worldPos1 = verts[vertIdx1] + offset;

		Vector2 screenPos0, screenPos1;
		fwClothDebug::GetScreenPosFromWorldPoint( pViewport, worldPos0, screenPos0 );
		fwClothDebug::GetScreenPosFromWorldPoint( pViewport, worldPos1, screenPos1 );

		bool pEdgePicked = false;
		if( brushShape == fwClothDebug::BRUSH_CIRCLE )
		{
			const float brushRadius = (v0 - v1).Mag() * 0.5f;
			Vector2 brushCenter = v0*0.5f + v1*0.5f;

			const float distToCenter0 = (screenPos0 - brushCenter).Mag();
			const float distToCenter1 = (screenPos1 - brushCenter).Mag();
			if( distToCenter0 < brushRadius || distToCenter1 < brushRadius )
			{
				pEdgePicked = true;
			}
		}
		else if( brushShape == fwClothDebug::BRUSH_RECTANGLE )
		{
			if( BoxVsSegment2D( boxCenter, boxExtents, boxAxisX, boxAxisY, screenPos0, screenPos1) )
			{
				pEdgePicked = true;
			}
		}
		if( pEdgePicked )
		{
			Assert( edgesInArea < numEdges );
			Assert( (edgeStart + edgesInArea) < edgesOut.GetCapacity() );
			edgesOut[ edgeStart + edgesInArea ] = i;
			edgesInArea++;
		}
	}
	return edgesInArea;
}

int PickEdgesByVerts2( const int selectedVertsCount, const atArray<int>& selectedVerts, const atArray<phEdgeData>& edgeData, atArray<int>& edgesOut, int edgeStart, const int numEdges )
{
	int edgesInArea = 0;

	for(int i = 0; i < numEdges; ++i )
	{
		const phEdgeData& edge = edgeData[i];
		const u16 vertIdx0 = edge.m_vertIndices[0];
		const u16 vertIdx1 = edge.m_vertIndices[1];
		if( vertIdx0 == vertIdx1 )
			continue;

		for(int j = 0; j < selectedVertsCount; ++ j)
		{
			if( selectedVerts[j] == vertIdx0 || selectedVerts[j] == vertIdx1 )
			{
				Assert( edgesInArea < numEdges );
				Assert( (edgeStart + edgesInArea) < edgesOut.GetCapacity() );
				edgesOut[ edgeStart + edgesInArea ] = i;
				edgesInArea++;
			}
		}
	}
	return edgesInArea;
}


#endif //USE_CLOTHMANAGER_BANK



#if __RESOURCECOMPILER
void LoadClothCustomBounds(environmentCloth* pEnvCloth, const char* fileName, const char* fileNameAppendix, const char* pClothControllerName, bool bSetFlag )
{
	Assert( pEnvCloth );
	Assert( pClothControllerName );

	char buff[256];
	formatf( buff, "%s%s", fileName, fileNameAppendix );

	if( ASSET.Exists(buff, "xml") )
	{		
		sysMemStartTemp();
		phVerletClothCustomBounds oCustomBounds;
		oCustomBounds.Load("", fileName, fileNameAppendix );		
		sysMemEndTemp();

		for( int i = 0; i < LOD_COUNT; ++i )
		{
			phVerletCloth* pCloth = pEnvCloth->GetCloth(i);
			if( pCloth )
			{
				bool res = clothManager::AttachCustomBound( pCloth, pClothControllerName, oCustomBounds );
				if( res && bSetFlag )
				{
					pEnvCloth->SetFlag( environmentCloth::CLOTH_HasLocalBounds, true );
				}
			}
		}
		sysMemStartTemp();
		oCustomBounds.m_CollisionData.Reset();
		sysMemEndTemp();
	}
}

CustomBoundsCB	g_CustomBoundsCB = LoadClothCustomBounds;

#endif // __RESOURCECOMPILER





// ----------  clothVariationData

clothVariationData::clothVariationData()
	: m_clothDictionarySlotId(-1)
	, m_clothPtr(NULL)	
	, m_clothNumVertices(0)
{
	m_Node.Data=this; 

	for( int i = 0; i < NUM_CLOTH_BUFFERS; ++i )
	{
#if __XENON
		m_clothVtxBuffer[i] = NULL;
#endif
		m_clothVertices[i] = NULL;
	}
}

void clothVariationData::DeleteBuffers()
{
	for( int i = 0; i < NUM_CLOTH_BUFFERS; ++i )
	{
		if( m_clothVertices[i] )
		{
#if __PFDRAW
			s_MemAllocatedChar -= strStreamingEngine::GetAllocator().GetSize( m_clothVertices[i] );
#endif

			MEM_USE_USERDATA(MEMUSERDATA_CLOTH);
			strStreamingEngine::GetAllocator().Free( m_clothVertices[i] );
			//		delete[] m_clothVertices[i];
			m_clothVertices[i] = NULL;

#if __XENON
			Assert( m_clothVtxBuffer[i] );
			delete m_clothVtxBuffer[i];
			m_clothVtxBuffer[i] = NULL;
#endif
		}
	}

	Assert( m_clothNumVertices ); 
	m_clothNumVertices = 0;
}

void clothVariationData::DeleteClothData()
{
	Assert( m_clothPtr );
	delete m_clothPtr;
	m_clothPtr = NULL;

	DeleteBuffers();

	clothDebugf1("Deleted clothVariationData");
}

void clothVariationData::Set(characterCloth* pCharCloth)
{
	Assert( pCharCloth );
	Assert( !m_clothPtr );
	m_clothPtr = pCharCloth;
}

void clothVariationData::Set(int numVerts, characterCloth* clothPtr, s32 dictionarySlotId )
{
	Assert( numVerts > 0 );
	Assert( clothPtr );
	Assert( dictionarySlotId > -1 );

	Assert( m_clothDictionarySlotId == -1 );
	Assert( !m_clothPtr );
	Assert( !m_clothNumVertices );


	// NOTE: important to add ref to the cloth store, otherwise defrag will kick it out and game will crash
	g_ClothStore.AddRef( strLocalIndex(dictionarySlotId), REF_OTHER );

	m_clothDictionarySlotId = dictionarySlotId;
	m_clothPtr = clothPtr;

	AllocateBuffers(numVerts);

/*
	m_clothNumVertices = numVerts;

	Assertf( numVerts < MAX_CHAR_CLOTH_VERTS, "character cloth has more verts (%d) than the limit (%d). Please reduce the number of verts in the asset!", numVerts, MAX_CHAR_CLOTH_VERTS-1 );		

#if __XENON				// Setup secondary vertex stream...
	grcFvf fvf;
	fvf.SetPosChannel(true, grcFvf::grcdsFloat4);
#endif
	for( int i = 0; i < NUM_CLOTH_BUFFERS; ++i )
	{
		Assert( !m_clothVertices[i] );
		Assert( numVerts > 0 );
		MEM_USE_USERDATA(MEMUSERDATA_CLOTH);
		m_clothVertices[i] = (Vec3V*)(strStreamingEngine::GetAllocator().Allocate( numVerts*sizeof(Vec3V), RAGE_VERTEXBUFFER_ALIGNMENT, MEMTYPE_RESOURCE_VIRTUAL ) );
//		m_clothVertices[i] = rage_new Vec3V[ numVerts ];
		AssertMsg( m_clothVertices[i], "The game run out of streaming memory. Cloth vertex buffers won't be created." );
#if __PFDRAW
		s_MemAllocatedChar += strStreamingEngine::GetAllocator().GetSize( m_clothVertices[i] );
#endif
#if __XENON				// Setup secondary vertex stream...
		// NOTE: important, if out of streaming memory then do not poke any other memory 
		if( m_clothVertices[i] )
		{
			Assert( !m_clothVtxBuffer[i] );
			const bool bReadWrite = true;
			const bool bDynamic = true;
			m_clothVtxBuffer[i] = grcVertexBuffer::Create( numVerts, fvf, bReadWrite, bDynamic, (void*)m_clothVertices[i]);
			Assert( m_clothVtxBuffer[i] );
		}
#endif
	}
*/
}

void clothVariationData::AllocateBuffers(int numVerts)
{
	Assert( numVerts > 0 );
	Assert( !m_clothNumVertices );
	m_clothNumVertices = numVerts;

	Assertf( numVerts < MAX_CHAR_CLOTH_VERTS, "character cloth has more verts (%d) than the limit (%d). Please reduce the number of verts in the asset!", numVerts, MAX_CHAR_CLOTH_VERTS-1 );		

#if __XENON				// Setup secondary vertex stream...
	grcFvf fvf;
	fvf.SetPosChannel(true, grcFvf::grcdsFloat4);
#endif
	for( int i = 0; i < NUM_CLOTH_BUFFERS; ++i )
	{
		Assert( !m_clothVertices[i] );
		Assert( numVerts > 0 );
		MEM_USE_USERDATA(MEMUSERDATA_CLOTH);
		m_clothVertices[i] = (Vec3V*)(strStreamingEngine::GetAllocator().Allocate( numVerts*sizeof(Vec3V), RAGE_VERTEXBUFFER_ALIGNMENT, MEMTYPE_RESOURCE_VIRTUAL ) );
		//		m_clothVertices[i] = rage_new Vec3V[ numVerts ];
		AssertMsg( m_clothVertices[i], "The game run out of streaming memory. Cloth vertex buffers won't be created." );
#if __PFDRAW
		s_MemAllocatedChar += strStreamingEngine::GetAllocator().GetSize( m_clothVertices[i] );
#endif
#if __XENON				// Setup secondary vertex stream...
		// NOTE: important, if out of streaming memory then do not poke any other memory 
		if( m_clothVertices[i] )
		{
			Assert( !m_clothVtxBuffer[i] );
			const bool bReadWrite = true;
			const bool bDynamic = true;
			m_clothVtxBuffer[i] = grcVertexBuffer::Create( numVerts, fvf, bReadWrite, bDynamic, (void*)m_clothVertices[i]);
			Assert( m_clothVtxBuffer[i] );
		}
#endif
	}
}

void clothVariationData::Reset()
{
	Assert( m_clothDictionarySlotId != -1 );
	Assert( m_clothPtr );
	g_ClothStore.RemoveRef( strLocalIndex(m_clothDictionarySlotId), REF_OTHER );

	m_clothDictionarySlotId = -1;	

	DeleteClothData();
}
//---------------------------------


// phVerletClothCustomBounds
const char* phVerletClothCustomBounds::sm_MetaDataFileAppendix = "_custombounds";

void phVerletClothCustomBounds::Load(const char* filePath, const char* clothName, const char* fileNameAppendix)
{
	char buff[256];
	formatf( buff, "%s%s%s", filePath, clothName, fileNameAppendix );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);	

	clothDebugf1("Loaded cloth instance custom bounds from file: %s", buff);
}


// clothInstance
clothInstance::clothInstance() 
	: m_Type((int)clothManager::Unknown)
	, m_Skeleton(NULL)
	, m_ClothController(NULL)
	, m_IsActive( false )
	, m_IsReady( false )
	, m_LastPosition(Vec3V(V_ZERO))
	, m_FramesToRotate(0)
	, m_RotationSign(ScalarV(V_ONE))
#if RSG_PC
	, m_ElapsedTimeSinceLastUpdate(0.0f)
#endif
	, m_UserData1(0)
	, m_UserData2(0)
	, m_UserData3(0)
#if USE_CLOTHMANAGER_BANK
	, m_RAGGroup(NULL)
	, m_VirtualBoundGroup(NULL)
	, m_TogglesGroup(NULL)
	, m_DynamicPinUnpinWidget(NULL)
	, m_DynamicPickVertsWidget(NULL)
	, m_DynamicPickPinVertsWidget(NULL)
	, m_SpuDebugIdx(-1)
#if CLOTH_PROFILE
	, m_LastUpdateTime(0.0f)
	, m_UpdateTime(0.0f)
	, m_InstanceTime(0.0f)
#endif
	, m_SpuDebug(false)
	, m_DebugDraw(false)	
	, m_DebugDrawPinRadiuses(false)
	, m_DebugDrawAirResistance(false)
	, m_DebugDrawEdgeInfo(false)
	, m_DebugDragVerts(false)
	, m_DebugPickVerts(false)
	, m_DebugPickPinVerts(false)
	, m_DebugPickEdges(false)
	, m_DebugPickEdgesByVerts(false)
	, m_DebugPickCustomEdges(false)
	, m_DebugPickInstanceCustomEdges(false)
	, m_DebugUnPinVerts(false)
	, m_DebugPinVerts(false)
	, m_DebugDeleteVerts(false)
	, m_DebugSelectedVertex(0)
	, m_DebugCurrentPinRadiusSet(0)
	, m_DebugCurrentPose(0)
	, m_DebugDrawCustomBound(false)
	, m_DebugPreviewEdgeSplit(false)
	, m_RestInterface(false)
#endif
{ 
	m_Node.Data=this; 
}

#if USE_CLOTHMANAGER_BANK
void clothInstance::RegisterRestInterface(const char* controllerName)
{
	if( !s_RegisterInstanceRestInterface )
	{		
		s_RegisterInstanceRestInterface = true;

		parRestRegisterSingleton("Physics/Cloth/BindPose", *this, NULL);
		clothDebugf1("Registered Physics/Cloth/BindPose REST interface for: %s", controllerName);

		if( GetType() == clothManager::Character )
		{
			characterClothController* pCCController = (characterClothController*)g_LastPickedClothController;
			characterCloth* pCCloth = (characterCloth*)pCCController->GetOwner();
			Assert( pCCloth );
			pCCloth->RegisterRestInterface(controllerName);		

			Assert( g_LastPickedClothController );
			phVerletCloth* cloth = g_LastPickedClothController->GetCloth(0);
			Assert( cloth );

			const int numVerts = cloth->GetNumVertices();

			sysMemStartDebug();
			m_BindPose.Resize(numVerts);

			characterClothController* cccontroller = (characterClothController*)g_LastPickedClothController;

			const atArray<Vec3V>* originalPos = cccontroller->GetOriginalPos();
			Assert( originalPos );
			Assert( originalPos->GetCount() );

			const Vec3V* pos = originalPos->GetElements();
			for( int i = 0; i < numVerts; ++i )
			{
				m_BindPose[i] = pos[i];
			}

			sysMemEndDebug();
		}
	}
}
void clothInstance::UnregisterRestInterface(const char* controllerName)
{
	if( s_RegisterInstanceRestInterface )
	{		
		s_RegisterInstanceRestInterface = false;

		REST.RemoveAndDeleteService("Physics/Cloth/BindPose");
		clothDebugf1("Unregistered Physics/Cloth/BindPose REST interface for: %s", controllerName);

		if( GetType() == clothManager::Character )
		{
			Assert( g_LastPickedClothController );
			characterClothController* pCCController = (characterClothController*)g_LastPickedClothController;
			characterCloth* pCCloth = (characterCloth*)pCCController->GetOwner();
			Assert( pCCloth );
			pCCloth->UnregisterRestInterface(controllerName);
			

			sysMemStartDebug();
			m_BindPose.Reset();
			sysMemEndDebug();
		}
	}	
}
#endif // USE_CLOTHMANAGER_BANK


// clothManager

#if __RESOURCECOMPILER

bool clothManager::AttachCustomBound(phVerletCloth* pCloth, const char* pClothName, phVerletClothCustomBounds& refCustomBounds)
{
	Assert( pCloth );
	Assert( pClothName );

	int foundBoundsIdx[MAX_CUSTOM_BOUNDS_PER_CLOTH] = { -1 };

	int foundBounds = clothManager::SearchBoundsByName( pClothName, foundBoundsIdx, refCustomBounds );
	if( !foundBounds )
		return false;

	phBoundComposite* pCustomBound = rage_aligned_new(16) phBoundComposite;	
	Assert( pCustomBound );
	pCustomBound->Init( foundBounds, true );
	pCustomBound->SetNumBounds( foundBounds );	

	for( int i = 0; i < foundBounds; ++i )
	{
		const phCapsuleBoundDef& capsuleData = refCustomBounds.m_CollisionData[ foundBoundsIdx[i] ];
		clothManager::LoadCustomBound( pCustomBound, capsuleData, i );		
	}

	Assert( !pCloth->m_CustomBound );
	pCloth->m_CustomBound = pCustomBound;
	pCloth->SetFlag(phVerletCloth::FLAG_COLLIDE_EDGES, true);

	return true;
}

int clothManager::SearchBoundsByName(const char* pClothName, int* pBoundsIndices, phVerletClothCustomBounds& refCustomBounds)
{
	Assert( pClothName );
	Assert( pBoundsIndices );

	int foundBounds = 0;
	const int boundsCount = refCustomBounds.m_CollisionData.GetCount();

	for( int i = 0; i < boundsCount; ++i )
	{
		const phCapsuleBoundDef& capsuleData = refCustomBounds.m_CollisionData[i];
		const char* pBoundOwnerName = capsuleData.m_OwnerName.c_str();
		Assert( pBoundOwnerName );		// no empty names allowed. owner name should be set to "any" in the case of shared bound 
		if(	!strcmp(pBoundOwnerName, pClothName) )
		{
			Assert( foundBounds < MAX_CUSTOM_BOUNDS_PER_CLOTH );
			pBoundsIndices[foundBounds] = i;
			++foundBounds;
		}
	}	
	return foundBounds;
}

#endif // __RESOURCECOMPILER

#if !__SPU
void clothManager::LoadCustomBound( phBoundComposite* pCustomBound, const phCapsuleBoundDef& capsuleData, const int boundIdx )
{
	Assert( pCustomBound );

	phBound* pColBound;
	Mat34V boundMat(V_IDENTITY);

	Vec3V normal = capsuleData.m_Normal;
	if( IsEqualIntAll(Dot(normal,normal), ScalarV(V_ZERO)) == 0  )	// i.e. normal is not 0,0,0
	{
		pColBound = rage_new phBoundPlane();
		phBoundPlane* pPlaneBound = (phBoundPlane*)pColBound;
		pPlaneBound->SetNormal( normal );
		pPlaneBound->SetPosition( capsuleData.m_Position );
	}
	else
	{
		boundMat.SetCol3( capsuleData.m_Position );

		QuatV q =  SelectFT( IsEqual(capsuleData.m_Rotation, QuatV(V_ZERO)), Normalize( capsuleData.m_Rotation ), QuatV(V_ZERO) );
		Mat34V rotMat;
		Mat34VFromQuatV( rotMat, q );
		Transform( boundMat, boundMat, rotMat );	

		const float capsuleRadius = capsuleData.m_CapsuleRadius;
		const float capsuleLen = capsuleData.m_CapsuleLen;

		pColBound = rage_new phBoundCapsule();
		Assert( pColBound );
		((phBoundCapsule*)pColBound)->SetCapsuleSize( capsuleRadius, capsuleLen );	

#if USE_CAPSULE_EXTRA_EXTENTS
		((phBoundCapsule*)pColBound)->SetHalfHeight( capsuleData.m_CapsuleHalfHeight );
#endif
	}

	pCustomBound->SetBound( boundIdx, pColBound );
	pCustomBound->SetCurrentMatrix( boundIdx, boundMat );
	pCustomBound->SetLastMatrix( boundIdx, boundMat );

	Assert( pColBound->GetRefCount() == 2 );
	pColBound->Release();
}
#endif //!__SPU

void clothManager::LoadGlobalCustomBounds()
{
	char buff[128];
	const char* fileName = "global_cloth";

	ASSET.PushFolder( "common:/data/cloth/" );

	formatf( buff, "%s%s", fileName, phVerletClothCustomBounds::sm_MetaDataFileAppendix );
	if( ASSET.Exists(buff, "xml") )
	{
		m_GlobalCustomBounds.Load("", fileName, phVerletClothCustomBounds::sm_MetaDataFileAppendix );
	}

	ASSET.PopFolder();
}

bool clothManager::AttachGlobalCustomBound(phVerletCloth* pCloth, Vec3V_In posV, ScalarV_In radiusV, const char* pClothName)
{
	Assert( pCloth );
	Assert( pClothName );

	int foundBoundsIdx[MAX_CUSTOM_BOUNDS_PER_CLOTH] = { -1 };

	int foundBounds = SearchBoundsByName(posV, radiusV, pClothName, foundBoundsIdx);
	if( !foundBounds )
		return false;

	phBoundComposite* pCustomBound = rage_aligned_new(16) phBoundComposite;	
	Assert( pCustomBound );
	pCustomBound->Init( foundBounds, true );
	pCustomBound->SetNumBounds( foundBounds );	

	for( int i = 0; i < foundBounds; ++i )
	{
		const phCapsuleBoundDef& capsuleData = m_GlobalCustomBounds.m_CollisionData[ foundBoundsIdx[i] ];
		clothManager::LoadCustomBound( pCustomBound, capsuleData, i );		
	}

	Assertf( !pCloth->m_CustomBound, "Cloth has already custom bounds attached ! This will cause a leak !" );
	pCloth->m_CustomBound = pCustomBound;
	pCloth->SetFlag( phVerletCloth::FLAG_COLLIDE_EDGES, true );

	return true;
}

int clothManager::SearchBoundsByName(Vec3V_In posV, ScalarV_In radiusV, const char* pClothName, int* pBoundsIndices)
{
	Assert( pClothName );
	Assert( pBoundsIndices );

	int foundBounds = 0;
	const int boundsCount = m_GlobalCustomBounds.m_CollisionData.GetCount();
	ScalarV radiusSqrV = Scale(radiusV, radiusV);

	for( int i = 0; i < boundsCount; ++i )
	{
		const phCapsuleBoundDef& capsuleData = m_GlobalCustomBounds.m_CollisionData[i];
		Vec3V tempV = Subtract( capsuleData.m_Position, posV );
		if( strcmp(pClothName, "Prop_Flag_US_SL" ) ||
			IsLessThanAll( Dot(tempV, tempV), radiusSqrV ) != 0 
			)
		{
			const char* pBoundOwnerName = capsuleData.m_OwnerName.c_str();
			Assert( pBoundOwnerName );		// no empty names allowed. owner name should be set to "any" in the case of shared bound 
			if(	!strcmp(pBoundOwnerName, pClothName) )
			{
				Assert( foundBounds < MAX_CUSTOM_BOUNDS_PER_CLOTH );
				pBoundsIndices[foundBounds] = i;
				++foundBounds;
			}
		}
	}	
	return foundBounds;
}

#define		TEMP_BOX_SIZE		0.8f

void clothManager::FindClosestClothInstance(Vec3V_In pos, int typeToUpdate, FoundClothCB responseCB)
{
	Assert( typeToUpdate != clothManager::Unknown );

#if USE_AABB_VS_AABB_PED_VS_CLOTH
	Vec3V aabbMin = Subtract( pos, Vec3V(TEMP_BOX_SIZE, TEMP_BOX_SIZE, 1.0f) );
	Vec3V aabbMax = rage::Add( pos, Vec3V(TEMP_BOX_SIZE, TEMP_BOX_SIZE, 1.0f) );
  #if __PFDRAW && !__RESOURCECOMPILER
	if( PFD_VerletBounds.Begin(true) )
	{
		grcDrawBox( VEC3V_TO_VECTOR3(aabbMin), VEC3V_TO_VECTOR3(aabbMax), Color_white );
		PFD_VerletBounds.End();
	}
  #endif
#endif

	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{		
		if( (node->Data->GetType() & typeToUpdate) && node->Data->IsActive() )
		{
			environmentCloth* envCloth = (environmentCloth*)node->Data->GetClothController()->GetOwner();
			Assert( envCloth );
			Assert( envCloth->GetClothController() );

			const clothInstanceTuning* pClothTuning = envCloth->GetTuning();
			if( (pClothTuning && pClothTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_NO_PED_COLLISION))
				)
				continue;

			//phEnvClothVerletBehavior* clothBehavior = envCloth->GetBehavior();
			//Assert( clothBehavior );
			// B*2049803 - Enable collision between peds and vehicle cloth
			//if( clothBehavior->IsMotionSeparated() )
			//	continue;

#if USE_AABB_VS_AABB_PED_VS_CLOTH			
			if( geomBoxes::TestAABBtoAABB(VEC3V_TO_VECTOR3(envCloth->GetBBMinWorldSpace()), VEC3V_TO_VECTOR3(envCloth->GetBBMaxWorldSpace()), VEC3V_TO_VECTOR3(aabbMin), VEC3V_TO_VECTOR3(aabbMax)) )	
#else
			Vec3V boundingCenter = pCloth->GetCenter();
			ScalarV radius = ScalarVFromF32(pCloth->GetRadius(boundingCenter));
			ScalarV distSqr = Scale(radius, radius);

			Vec3V tempV = Subtract( boundingCenter, pos);
			ScalarV tempDistSqr = Dot(tempV, tempV);
			if( IsLessThanAll(tempDistSqr, distSqr) != 0 )
#endif
			{
				responseCB( envCloth->GetCloth() );
			}
		}
	}
}

void clothManager::FindPedInClothInstanceShutdown( phBoundComposite* pCompositeBound, int cType )
{
	Assert( pCompositeBound );

	// Block until all aync tasks have been completed.  This code can be called
	// from script when tasks are still running.
	m_TasksManager->WaitForTasks();
	m_InstanceTasksManager->WaitForTasks();

	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->GetType() & cType )
		{
			environmentCloth* pEnvCloth = (environmentCloth*)node->Data->GetClothController()->GetOwner();
			Assert( pEnvCloth );

			phVerletCloth* pCloth = pEnvCloth->GetCloth();
			Assert( pCloth );

			if( pCloth->m_PedBound0 == pCompositeBound )
			{
				clothDebugf1("[clothManager::FindPedInClothInstanceShutdown] Detach bound from m_PedBound0");

				pCloth->m_PedBound0 = NULL;
				pCloth->m_PedBoundMatrix0 = NULL;
			}
			else if( pCloth->m_PedBound1 == pCompositeBound )
			{
				clothDebugf1("[clothManager::FindPedInClothInstanceShutdown] Detach bound from m_PedBound1");

				pCloth->m_PedBound1 = NULL;
				pCloth->m_PedBoundMatrix1 = NULL;
			}
		}
	}
}


void clothManager::FindPedInClothInstance( Vec3V_In pos, phBoundComposite* pCompositeBound, int cType )
{
	Assert( pCompositeBound );

#if USE_AABB_VS_AABB_PED_VS_CLOTH
	Vec3V aabbMin = Subtract( pos, Vec3V(TEMP_BOX_SIZE, TEMP_BOX_SIZE, 1.0f) );
	Vec3V aabbMax = rage::Add( pos, Vec3V(TEMP_BOX_SIZE, TEMP_BOX_SIZE, 1.0f) );
#endif

	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{		
		if( node->Data->GetType() & cType )
		{
			environmentCloth* pEnvCloth = (environmentCloth*)node->Data->GetClothController()->GetOwner();
			Assert( pEnvCloth );

			phVerletCloth* pCloth = pEnvCloth->GetCloth();
			Assert( pCloth );

			if( pCloth->m_PedBound0 == pCompositeBound )
			{
#if USE_AABB_VS_AABB_PED_VS_CLOTH			
				if( geomBoxes::TestAABBtoAABB(VEC3V_TO_VECTOR3(pEnvCloth->GetBBMinWorldSpace()), VEC3V_TO_VECTOR3(pEnvCloth->GetBBMaxWorldSpace()), VEC3V_TO_VECTOR3(aabbMin), VEC3V_TO_VECTOR3(aabbMax)) )	
#else
				ScalarV radius = ScalarVFromF32(pCloth->GetRadius(pCloth->GetCenter()));
				ScalarV distSqr = Scale(radius, radius);

				Vec3V framePos = VECTOR3_TO_VEC3V(pEnvCloth->GetFramePosition());
				Vec3V tempV = Subtract(framePos, pos);
				ScalarV tempDistSqr = Dot(tempV, tempV);
				if( IsLessThanAll(tempDistSqr, distSqr) != 0 )
#endif
				{
					continue;
				}

				clothDebugf1("[clothManager::FindPedInClothInstance] Detach bound from m_PedBound0");

				pCloth->m_PedBound0 = NULL;
				pCloth->m_PedBoundMatrix0 = NULL;
			}
			else if( pCloth->m_PedBound1 == pCompositeBound )
			{
#if USE_AABB_VS_AABB_PED_VS_CLOTH			
				if( geomBoxes::TestAABBtoAABB(VEC3V_TO_VECTOR3(pEnvCloth->GetBBMinWorldSpace()), VEC3V_TO_VECTOR3(pEnvCloth->GetBBMaxWorldSpace()), VEC3V_TO_VECTOR3(aabbMin), VEC3V_TO_VECTOR3(aabbMax)) )	
#else
				ScalarV radius = ScalarVFromF32(pCloth->GetRadius(pCloth->GetCenter()));
				ScalarV distSqr = Scale(radius, radius);

				Vec3V framePos = VECTOR3_TO_VEC3V(pEnvCloth->GetFramePosition());
				Vec3V tempV = Subtract(framePos, pos);
				ScalarV tempDistSqr = Dot(tempV, tempV);
				if( IsLessThanAll(tempDistSqr, distSqr) != 0 )
#endif
				{
					continue;
				}

				clothDebugf1("[clothManager::FindPedInClothInstance] Detach bound from m_PedBound1");

				pCloth->m_PedBound1 = NULL;
				pCloth->m_PedBoundMatrix1 = NULL;
			}

			if( !(pCloth->m_PedBound0 || pCloth->m_PedBound1) )
			{
				pCloth->m_nIterations = DEFAULT_VERLET_ITERS;
			}
		}
	}
}

// TODO: note used
#if 0

bool clothManager::FindClothInstance( phVerletCloth* pCloth, int cType, Functor0 cleanupCB)
{
	Assert( pCloth );
	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{		
		if( (node->Data->GetType() & cType) && ((environmentCloth*)node->Data->GetClothController()->GetOwner())->GetCloth() == pCloth )
		{
			cleanupCB();
			return true;
		}
	}
	return false;
}

bool clothManager::FindClothInstance( Vec3V_In pos, phVerletCloth* pCloth, int cType, Functor0 cleanupCB)
{
	Assert( pCloth );
	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{		
		if( (node->Data->GetType() & cType) && ((environmentCloth*)node->Data->GetClothController()->GetOwner())->GetCloth() == pCloth )
		{
			environmentCloth* pEnvCloth = (environmentCloth*)node->Data->GetClothController()->GetOwner();
			Assert( pEnvCloth );

			ScalarV radius = ScalarVFromF32(pEnvCloth->GetCloth()->GetRadius(pEnvCloth->GetCloth()->GetCenter()));
			ScalarV distSqr = Scale(radius, radius);

			Vec3V framePos = VECTOR3_TO_VEC3V(pEnvCloth->GetFramePosition());
			Vec3V tempV = Subtract(framePos, pos);
			ScalarV tempDistSqr = Dot(tempV, tempV);
			if( IsLessThanAll(tempDistSqr, distSqr) != 0 )
				return true;

			// cloth is out of range
			cleanupCB();
			break;
		}
	}
	return false;
}

#endif

void clothManager::PauseClothSimulation()
{
	g_PauseClothSimulation = true;
}

void clothManager::UnpauseClothSimulation()
{
	g_PauseClothSimulation = false;
}

void clothManager::Init(verletTaskManager* pTasksManager, clothInstanceTaskManager* pInstanceTaskManager)
{
	Assert( pInstanceTaskManager );
	Assert( pTasksManager );
	m_TasksManager = pTasksManager;
	m_InstanceTasksManager = pInstanceTaskManager;

#if RSG_PC
	// Create a task scheduler for handling cloth instancing on PC.
	const unsigned stackSize = __DEV ? 0x18000 : 0x10000;
	const unsigned scratchSize = 0x8000;

	u32 cpuMask = (1 << SYS_DEPENDENCY_TASKACTIVENUMWORKERS) - 1;

	m_clothInstanceScheduler = sysTaskManager::AddScheduler("ClothInstanceSched", stackSize, scratchSize, PRIO_LOWEST, cpuMask, 0);
#else
	// Just use the default scheduler (0)
	m_clothInstanceScheduler = 0;
#endif

	//@@: location CLOTHMGR_INIT
	AssertMsg(m_FreeList.GetHead()==NULL , "Leaked a clothInstance in the free list");
	AssertMsg(m_FreeVarList.GetHead()==NULL , "Leaked a clothVariationData in the free list");
	m_ActiveCount = 0;
	m_FreeCount = 0;

	m_ActiveVarCount = 0;
	m_FreeVarCount = 0;

	for (int i=0; i<sm_MaxClothCount; i++)
	{
		clothInstance *cloth = rage_new clothInstance();
		m_FreeList.Append(cloth->GetNodeRef());
		m_FreeCount++;

		clothVariationData* clothVar = rage_new clothVariationData();
		m_FreeVarList.Append(clothVar->GetNodeRef());
		m_FreeVarCount++;

	}

	grcRasterizerStateDesc RSDesc;
	RSDesc.CullMode = grcRSV::CULL_NONE;
	m_passRS = grcStateBlock::CreateRasterizerState(RSDesc);

	grcDepthStencilStateDesc DSSDesc;
	DSSDesc.StencilEnable = 1;
	DSSDesc.BackFace.StencilPassOp = grcRSV::STENCILOP_REPLACE;
	DSSDesc.FrontFace.StencilPassOp = grcRSV::STENCILOP_REPLACE;
	DSSDesc.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	m_passDSS = grcStateBlock::CreateDepthStencilState(DSSDesc,"m_passDSS");

	DSSDesc.DepthFunc = grcRSV::CMP_LESSEQUAL;
	m_invertPassDSS = grcStateBlock::CreateDepthStencilState(DSSDesc,"m_invertPassDSS");
	
#if USE_CLOTHMANAGER_BANK

	pClothManager = this;

	// Note: use call parameter, cloth controller will be passed from the caller
	g_ClothManagerCallback = datCallback( MFA1(clothManager::SetupEnvClothWidget), this, 0, true );

	InitWidgets();

	sysMemStartDebug();
	fwClothDebug::sm_SelectedVerts.Resize(MAX_CLOTH_DEBUG_VERTS);
	fwClothDebug::sm_LastSelectedVerts.Resize(MAX_CLOTH_DEBUG_VERTS);
	fwClothDebug::sm_LastSelectedVertsColors.Resize(MAX_CLOTH_DEBUG_VERTS);

	s_SelectedEdges.Resize(MAX_CLOTH_DEBUG_EDGES);
	s_LastSelectedEdges.Resize(MAX_CLOTH_DEBUG_EDGES);
	s_LastSelectedEdgesColors.Resize(MAX_CLOTH_DEBUG_EDGES);

	Assert( !s_PoseOut );	
	s_PoseOut = rage_new Vec3V[MAX_POSE_VERTS];
	Assert( s_PoseOut );

	Assert( !s_PoseColor );
	s_PoseColor = rage_new Color32[MAX_POSE_VERTS];	
	Assert( s_PoseColor );

	Assert( !s_MorphData );
	s_MorphData = rage_new phMorphDataAoS[MAX_POSE_VERTS];
	Assert( s_MorphData );

	Assert( !s_OriginalMorphData );
	s_OriginalMorphData = rage_new phMorphData2AoS[MAX_POSE_VERTS];
	Assert( s_OriginalMorphData );

	sysMemEndDebug();
#endif // USE_CLOTHMANAGER_BANK
}

void clothManager::Shutdown()
{
	AssertMsg(m_ActiveList.GetHead()==NULL , "Leaked a clothInstance in the active list");
	AssertMsg(m_ActiveVarList.GetHead()==NULL , "Leaked a clothVariationData in the active list");

	Reset();
#if USE_CLOTHMANAGER_BANK
	DestroyWidgets();

	if( ms_RAGBankGroup )
		pClothBank->DeleteGroup( *ms_RAGBankGroup );	

	if( ms_RAGBankSliders )
		pClothBank->DeleteGroup( *ms_RAGBankSliders );

	sysMemStartDebug();
	fwClothDebug::sm_SelectedVerts.Reset();
	fwClothDebug::sm_LastSelectedVerts.Reset();
	fwClothDebug::sm_LastSelectedVertsColors.Reset();

	s_SelectedEdges.Reset();
	s_LastSelectedEdges.Reset();
	s_LastSelectedEdgesColors.Reset();

	Assert( s_PoseOut );
	delete[] s_PoseOut;

	Assert( s_PoseColor );
	delete[] s_PoseColor;

	Assert( s_MorphData );
	delete[] s_MorphData;

	Assert( s_OriginalMorphData );
	delete[] s_OriginalMorphData;

	sysMemEndDebug();
#endif // USE_CLOTHMANAGER_BANK
}

void clothManager::Reset()
{
	atDNode<clothInstance*> *node = m_ActiveList.GetHead();
	while( node )
	{
		Remove( node->Data );
		node = m_ActiveList.GetHead();
	}
	Assert( m_ActiveList.GetHead()==NULL );
	Assert( m_ActiveList.GetTail()==NULL );
	Assert( m_ActiveCount==0 );


	atDNode<clothVariationData*> *nodeVar = m_ActiveVarList.GetHead();
	while( nodeVar )
	{
// TODO: what should we do with var data??
//		Remove( nodeVar->Data );
		nodeVar = m_ActiveVarList.GetHead();
	}
	Assert( m_ActiveVarList.GetHead()==NULL );
	Assert( m_ActiveVarList.GetTail()==NULL );
	Assert( m_ActiveVarCount==0 );

}

clothInstance* clothManager::GetClothInst(rage::clothController* inst)
{
	Assert( inst );
	return (clothInstance*)inst->GetClothInstance();
}

void clothManager::InstanceClothDrawableTask(sysTaskParameters& p)
{
	clothManager* pThis	= (clothManager*)p.Input.Data;
	Assert( pThis );

	clothDrawableParams drawableParams;
	drawableParams.pTypeDrawable = (rmcDrawable*)p.UserData[0].asPtr;
	drawableParams.pPreAllocatedMemory = p.UserData[1].asPtr;
	drawableParams.iMemoryAvailable = p.UserData[3].asInt;
	drawableParams.pEnvCloth = (environmentCloth*)p.UserData[2].asPtr;
	drawableParams.translationV = Vec3V( p.UserData[6].asFloat, p.UserData[7].asFloat, p.UserData[8].asFloat);
	drawableParams.iTotalBufferSize = p.UserData[4].asInt;
	drawableParams.iTotalMemoryRequested = p.UserData[5].asInt;

	pThis->InstanceClothDrawable( &drawableParams );
}


void clothManager::UpdateTask(sysTaskParameters& p)
{
#if RSG_PC &&__D3D11 && !__RESOURCECOMPILER
	// This means dlCmdGrcDeviceUpdateBuffer::AddBufferUpdateCommandToDrawList() is called whenever a vertex/index buffer
	// is altered on this thread. The render thread then passes the new contents to the GPU by executing a DL command.
	grcBufferD3D11::SetInsertUpdateCommandIntoDrawListFunc(dlCmdGrcDeviceUpdateBuffer::AddBufferUpdate);
	// Occasionally a buffer is updated and deleted in the same frame. This mechanism avoids operating on a deleted buffer.
	grcBufferD3D11::SetCancelPendingUpdateGPUCopy(dlCmdGrcDeviceUpdateBuffer::CancelBufferUpdate);
	grcBufferD3D11::SetAllocTempMemory(dlCmdGrcDeviceUpdateBuffer::AllocTempMemory);
#endif

	float timeStep				= p.UserData[0].asFloat;
	rage::clothController* inst	= (rage::clothController*)p.UserData[1].asPtr;
	float timeScale				= p.UserData[2].asFloat;

	clothManager* pThis			= (clothManager*)p.Input.Data;

	if(inst->GetFlag(clothController::CLOTH_CollectVertsFromReplay))
	{
		Assertf(sm_pInjectVerticesFunc != NULL, "clothManager::UpdateTask()...Expecting an cloth vertices inject function!");
		int lod = inst->GetLOD();
		phVerletCloth *pVerletCloth = inst->GetCloth(lod);
		
		enPostInjectVerticesAction injectAction =  sm_pInjectVerticesFunc(sm_pInjectVerticesFuncGlobalParam, inst->GetClothInstance(), pVerletCloth->GetNumVertices(), pVerletCloth->GetClothData().GetVertexPointer());

		if(injectAction & (enPostInjectVerticesAction)(checkAganstCollisionInsts | updateDrawableGeometry | resetDeltas))
			pThis->UpdateAgainstCollisionInsts(inst, injectAction);

		if(injectAction & runSimulation)
			pThis->UpdateVerlet(inst, timeStep, timeScale);
	}
	else
	{
		pThis->UpdateVerlet(inst, timeStep, timeScale);
	}

	if(inst->GetFlag(clothController::CLOTH_CaptureVertsForReplay))
	{
		if(sm_pCaptureVerticesFunc)
		{
			int lod = inst->GetLOD();
			phVerletCloth *pVerletCloth = inst->GetCloth(lod);
			sm_pCaptureVerticesFunc(sm_pCaptureVerticesFuncGlobalParam, inst->GetClothInstance(), pVerletCloth->GetNumVertices(), pVerletCloth->GetClothData().GetVertexPointer());
		}
	}
}

#define TIME_SLOWDOWN			0.5f

bool clothManager::Update( rage::clothController* ccontroller, environmentCloth* pCloth )
{
	if( PARAM_noclothupdate.Get() )
		return false;

	Assert( ccontroller );

#if __BANK
	float instWeight;
	if( !PARAM_forceclothweight.Get( instWeight ) )
	{
		instWeight = ccontroller->GetClothWeight();
	}
#else
	float instWeight = ccontroller->GetClothWeight();
#endif

	const float timeStep = s_timeStepCloth * instWeight;

	static float slowdown = TIME_SLOWDOWN;

#if __BANK
	const float ffTimeScale = fwTimer::GetFixedFrameTimeScale();
	const float timeScale = ffTimeScale * ((m_Slowdown && !m_SkipSlowdown) ? slowdown: 1.0f);
#else
	const float timeScale = (m_Slowdown && !m_SkipSlowdown) ? slowdown: 1.0f;
#endif


#if !__PS3

	static sysTaskParameters p;
	p.Input.Data = this;
	p.Output.Data = ccontroller;
	p.UserData[0].asFloat = timeStep;	
	p.UserData[1].asPtr = ccontroller;
	p.UserData[2].asFloat = timeScale;

	p.UserDataCount = 3;

	Assert(m_TasksManager);
	if( m_TasksManager->GetTasksCount() < verletTaskManager::MAX_TASKS )
	{
		if( pCloth )
		{
			pCloth->SetFlag( environmentCloth::CLOTH_IsUpdating, true);
		}

		sysTaskHandle h = sysTaskManager::Create(TASK_INTERFACE(UpdateTask), p);
		if( h )
		{
			m_TasksManager->AddTask( h );
		}
		else
		{
			if( pCloth )
			{
				pCloth->SetFlag( environmentCloth::CLOTH_IsUpdating, false);
			}
		}
	}
	else
	{
		Warningf("Active Verlet Tasks reached the maximum of %d. You can increase MAX_TASKS but something is wrong!", verletTaskManager::MAX_TASKS);
		AssertMsg( m_TasksManager->GetTasksCount() < verletTaskManager::MAX_TASKS, "Reached the limit of active verlet tasks. You can increase MAX_TASKS but something is wrong!" );
	}
#else
	UpdateVerlet( ccontroller, timeStep, timeScale );
#endif

	return false;
}

void clothManager::PostUpdate()
{
	m_SkipSlowdown -= m_SkipSlowdown? 1: 0;
#if USE_CLOTHMANAGER_BANK
	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{		
		UpdateDebugPose( node->Data );
	}
#endif
}


void clothManager::PreUpdate(const grcViewport& _viewPort, float /*elapsed*/)
{
	PF_FUNC( ClothManager );

	if( PARAM_noclothupdate.Get() )
		return;

#if USE_CLOTH_TELEMETRY
	//	clothDebugf1("m_UpdatedCount = %d   m_TotalLoadedEnvVertexCount = %d   m_TotalActiveEnvVertexCount = %d   m_TotalCharVertexCount = %d",m_UpdatedCount,m_TotalLoadedEnvironmentClothVertexCount,m_TotalActiveEnvironmentClothVertexCount,m_TotalCharacterClothVertexCount);

	m_UpdatedCount = 0;
	m_TotalLoadedEnvironmentClothVertexCount = 0;
	m_TotalActiveEnvironmentClothVertexCount = 0;
	m_TotalCharacterClothVertexCount = 0;
#endif


#if ENABLE_SPU_DEBUG_DRAW && USE_CLOTHMANAGER_BANK
	s_VerletSpuDebugCounter = 1;
#endif

	m_ViewPort = &_viewPort;
}

void clothManager::ComputeClothImpacts(Vec3V_In vOldPosition, Vec3V_In vPosition, float impulse)
{
	atDNode<clothInstance*>* headNode =  GetHead();
	for (atDNode<clothInstance*>* node = headNode; node; node=node->GetNext())
	{
		clothInstance* cinst = (clothInstance*)(node->Data);
		Assert( cinst );
		if( cinst->IsActive() && (cinst->GetType() == clothManager::Environment) )
		{
			rage::clothController* inst = cinst->GetClothController();
			Assert( inst );

			environmentCloth* ecloth = (environmentCloth*)inst->GetOwner();
			Assert( ecloth );
			phVerletCloth* cloth = ecloth->GetCloth();
			Assert( cloth );			
			if( geomBoxes::TestSegmentToBoxMinMax(VEC3V_TO_VECTOR3(vOldPosition), VEC3V_TO_VECTOR3(vPosition), VEC3V_TO_VECTOR3(ecloth->GetBBMinWorldSpace()), VEC3V_TO_VECTOR3(ecloth->GetBBMaxWorldSpace())) )
			{					
				Vector3 vDir = VEC3V_TO_VECTOR3( Subtract(vPosition,vOldPosition) );
				vDir.Normalize();

				// Note: if we want to unpin verts with the shot this need to be here - svetli
				rage::atFunctor4< void, int, int, int, bool > funcSwap;
				funcSwap.Reset< rage::clothController, &rage::clothController::OnClothVertexSwap >( inst );

				const atBitSet& stateList = ecloth->GetClothController()->GetBridge()->GetStateList();

				SkipSlowdown();
				cloth->ApplyImpulse( stateList, VECTOR3_TO_VEC3V(impulse * vDir), vOldPosition, &funcSwap, ecloth->GetOffset() );
			}
		}
	}
}

void clothManager::DisableInstancesWithCommonSkeleton(const crSkeleton* pSkel)
{
	sysCriticalSection critSec(s_LockActiveList);

	Assert( pSkel );
	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{		
		clothInstance* pClothInstance = node->Data;
		if( pClothInstance->GetType() == clothManager::Character && pClothInstance->GetSkeleton() == pSkel )
		{
			pClothInstance->SetActive(false);
			clothDebugf1("Disabled character cloth 0x%p with skeleton 0x%p", pClothInstance, pSkel);
		}
	}
}

void clothManager::ShiftBufferIndex()
{
	m_DrawBufferIndex = m_BuildBufferIndex;
	m_BuildBufferIndex = (m_BuildBufferIndex+1) % grmGeometry::GetVertexMultiBufferCount();
}

void clothManager::SwapBuffers()
{
	for (atDNode<clothVariationData*> *node = m_ActiveVarList.GetHead(); node; node=node->GetNext())
	{				
		clothVariationData* clothVarData = (clothVariationData*)node->Data;
		Assert( clothVarData );
		clothVarData->GetCloth()->SetEntityVerticesPtr( clothVarData->GetClothVertices( m_BuildBufferIndex ) );
		clothVarData->GetCloth()->SetEntityParentMtxPtr( clothVarData->GetClothParentMatrixPtr( m_BuildBufferIndex ) );
	}
}

void clothManager::Update( int typeToUpdate )
{
	m_Slowdown = fwTimer::GetTimeWarpActive() < 0.9f ? true: false;
	for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{	
		const int iClothType = node->Data->GetType();
		const int iClothModifiedType = node->Data->GetTypeWithModifiers();

		if( iClothModifiedType & typeToUpdate )
		{
			if( iClothType == clothManager::Environment || iClothType == clothManager::Vehicle )
			{
				if( !node->Data->IsReady() )
				{
					continue;
				}
				environmentCloth* pEnvCloth = (environmentCloth*)node->Data->GetClothController()->GetOwner();
				Assert( pEnvCloth );
				const clothInstanceTuning* pTuning = pEnvCloth->GetTuning();
				
				pEnvCloth->CheckDistance( m_CamPos );

				phEnvClothVerletBehavior* pClothBehavior = pEnvCloth->GetBehavior();
				Assert( pClothBehavior );

				const bool bUpdateIfVisible = (pTuning && pTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_UPDATE_IF_VISIBLE)) ? true: false;
				bool shouldUpdate = (!pClothBehavior->IsMotionSeparated() || bUpdateIfVisible ) ? pEnvCloth->GetIsVisible(): true;
				node->Data->SetActive( shouldUpdate );
				if( !shouldUpdate )
				{
					continue;
				}
#if RSG_PC
				float timeStepToUse = fwTimer::GetTimeStep();

				// If the game time step value is set artificially low by Replay (when rewinding) use real time.
				if(timeStepToUse < 0.0015f)
					timeStepToUse = fwTimer::GetSystemTimeStep();

				float fElapsedTime = node->Data->GetElapsedTime() - timeStepToUse;

				if( fElapsedTime > 0.0f && iClothType == clothManager::Environment )
				{
					node->Data->SetElapsedTime( fElapsedTime );
					pEnvCloth->SetFlag( environmentCloth::CLOTH_IsSkipSimulation, true );
				}
				else
				{
					node->Data->SetElapsedTime( s_timeStepCloth * 1.5f );
				}
#endif			
				Update( node->Data->GetClothController(), pEnvCloth );
			}
			else
			{
				Update( node->Data->GetClothController() );
			}
		}
	}
}

void clothManager::UpdateVerlet(rage::clothController* ccontroller, float timeStep, float timeScale)
{
	Assert( ccontroller );
	clothInstance* cinst = (clothInstance*)ccontroller->GetClothInstance();
	Assert( cinst );

#if CLOTH_PROFILE && USE_CLOTHMANAGER_BANK
	utimer_t startTime = sysTimer::GetTicks();
#endif


	// NOTE: Environment cloth
	if( cinst->GetType() == clothManager::Environment || cinst->GetType() == clothManager::Vehicle )
	{
		environmentCloth* pEnvCloth = (environmentCloth*)ccontroller->GetOwner();
		Assert( pEnvCloth );
		phVerletCloth* pCloth = pEnvCloth->GetCloth();
		Assert( pCloth );
		const clothInstanceTuning* pTuning = pEnvCloth->GetTuning();

#if USE_CLOTH_TELEMETRY
		int numVerts = pCloth->GetNumVertices();
		sysInterlockedAdd( (volatile u32*)&m_TotalLoadedEnvironmentClothVertexCount, numVerts );
#endif

#if USE_CLOTHMANAGER_BANK
		if( !pClothBankButton )
			cinst->m_RAGGroup->SetFillColor( "ARGBColor:128:255:255:255" );
#endif

#if USE_CLOTH_TELEMETRY
		sysInterlockedIncrement( (volatile u32*)&m_UpdatedCount );
		sysInterlockedAdd( (volatile u32*)&m_TotalActiveEnvironmentClothVertexCount, numVerts );
#endif		

		Vector3 vWindSpeed; 
		Vector3 framePos = pEnvCloth->GetFramePosition();
		Vec3V vQueryPos = RCC_VEC3V(framePos);

		WIND.GetLocalVelocity( vQueryPos, RC_VEC3V(vWindSpeed), true, true );

		// TODO: the flag has now different meaning, so just rename it
		vWindSpeed = (pTuning && pTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_IGNORE_DISTURBANCES)) ? -vWindSpeed: vWindSpeed;

		if( pTuning )
		{
			vWindSpeed = vWindSpeed + VEC3V_TO_VECTOR3(pTuning->m_ExtraForce);

			// TODO: the float check is temporary till new ragebuilder goes in
			if( pTuning->GetFlag( clothInstanceTuning::CLOTH_TUNE_CLAMP_HORIZONTAL_FORCE) 
				|| pTuning->m_ExtraForce.GetZf() > 5.0f
				)
			{
				vWindSpeed.x = 0.0f;
				vWindSpeed.y = 0.0f;
			}
		}

		if( cinst->GetType() == clothManager::Vehicle )
		{
			Vec3V vLastPosition = cinst->GetLastPosition();
			Vec3V vFrameDelta = Subtract( VECTOR3_TO_VEC3V(framePos), vLastPosition);
			static float fHighRange = 10.0f;
			ScalarV vHighRange = ScalarVFromF32(fHighRange);
			ScalarV lenDelta = Clamp( Mag(vFrameDelta), ScalarV(V_ZERO), ScalarV(vHighRange) );
			if( lenDelta.Getf() > 0.1f )
			{
				vFrameDelta = Scale( NormalizeFast( vFrameDelta ), lenDelta );
			}

			static float fFakeScale = 1.0f;
			vWindSpeed = vWindSpeed + VEC3V_TO_VECTOR3( Scale(vFrameDelta,ScalarVFromF32(fFakeScale)) );
#if RSG_PC
			vWindSpeed = vWindSpeed * (fwTimer::GetSystemTimeStep() * 15.0f);
#endif
			cinst->SetLastPosition( VECTOR3_TO_VEC3V(framePos) );
		}

		bool indicesWindingOrder = (pTuning && pTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_FLIP_INDICES_ORDER)) ? true: false;
		ccontroller->SetIndicesWindingOrder( indicesWindingOrder );

		// TODO: not final, should be moved out of this file somewhere to the game code
		if( pTuning && pTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_WIND_FEEDBACK) )
		{
			Assert( pEnvCloth->GetBehavior() );
			phInst* phinst = pEnvCloth->GetBehavior()->GetInstance();
			Assert( phinst );
			Mat34V* pInstMat = const_cast<Mat34V*>(&phinst->GetMatrix() );		
			Assert( pInstMat );

			int& framesToRotate = cinst->GetFramesToRotate();
			if( framesToRotate > 0 )
			{
				/*static*/ float smoothConst = 0.02f;
				float smooth = (float)(framesToRotate * framesToRotate) * smoothConst;
				ScalarV rot = Scale(cinst->GetRotationSign(), ScalarVFromF32( pTuning->m_RotationRate * timeStep * smooth ));				
				Mat34VRotateLocalZ( *pInstMat, rot );

				--framesToRotate;
			}
			else
			{
				ScalarV _scalarZero(V_ZERO);
				ScalarV _scalarOne(V_ONE);
				ScalarV _scalarNegOne = Negate(_scalarOne );

				const phClothData& clothdata = pCloth->GetClothData();

				Assert( pTuning->m_NonPinVert0 && pTuning->m_NonPinVert1 );

				Vec3V targetVert = Scale( clothdata.GetVertexPosition(pTuning->m_NonPinVert0) + clothdata.GetVertexPosition(pTuning->m_NonPinVert1), ScalarV(V_HALF));
				Vec3V pinVert = clothdata.GetVertexPosition(pTuning->m_PinVert);
				Vec3V dirFlat = NormalizeFast( Subtract( targetVert, pinVert ) );
				dirFlat.SetZ( _scalarZero);
				Vec3V atFlat = pInstMat->GetCol1();
				atFlat.SetZ( _scalarZero );						

				ScalarV ang = Dot( atFlat, dirFlat );
				ScalarV signV = SelectFT( IsGreaterThan(ang,_scalarZero), _scalarNegOne, _scalarOne );
				float absAng = Abs(ang).Getf();

				if( pTuning->m_AngleThreshold < absAng )
				{
					framesToRotate = (int)( absAng / (pTuning->m_RotationRate*timeStep) );
					cinst->SetRotationSign(signV);
				}
			}		
		}

		// TODO: decide what to do with this ...	for now: if cloth is marked as interior then kill the wind
		// svetli
		if( pEnvCloth->GetFlag(environmentCloth::CLOTH_IsInInterior) 
#if USE_CLOTHMANAGER_BANK
			||clothManager::ms_ClampAllForces 
#endif
			)
		{
			vWindSpeed.Zero();
		}

#if __PS3
		if( sm_SpuUpdate )
		{
			Vec3V force = VECTOR3_TO_VEC3V(vWindSpeed);
			force.SetWf(timeScale);
			// NOTE: ApplyAirResistance is on the SPU now.
			pEnvCloth->SetForce( force );
		}
		else
#endif
		{
#if CLOTH_INSTANCE_FROM_DATA
			if( pEnvCloth->GetDrawable() )
#endif
			{
#if RSG_PC
				if( !pEnvCloth->GetFlag( environmentCloth::CLOTH_IsSkipSimulation ) )
#endif
				{
		 			Vec3V* RESTRICT pNormals = Alloca(Vec3V, pCloth->GetNumVertices());
					Assert( pNormals );
					ccontroller->GetVertexNormalsFromDrawable( (Vector3*)pNormals, pEnvCloth->GetDrawable() );
					pEnvCloth->ApplyAirResistance(pNormals, vWindSpeed);
				}
			}
#if CLOTH_INSTANCE_FROM_DATA && USE_CLOTHMANAGER_BANK
			else
			{
				if( cinst->m_Indices.GetCount() )
				{
					const int numVerts = pCloth->GetNumVertices();
					Vec3V* RESTRICT pNormals = Alloca(Vec3V, numVerts);
					Assert( pNormals );
					sysMemSet(pNormals, 0, sizeof(Vec3V) * numVerts);
					Vec3V* RESTRICT pVerts = pCloth->GetClothData().GetVertexPointer();
					Assert( pVerts );
					for( int i = 0; i < cinst->m_Indices.GetCount(); i += 3 )
					{
						const int i0 = cinst->m_Indices[i];
						const int i1 = cinst->m_Indices[i+1];
						const int i2 = cinst->m_Indices[i+2];

						Vec3V vEdge1 = Subtract( pVerts[i1], pVerts[i0] );
						Vec3V vEdge2 = Subtract( pVerts[i2], pVerts[i0] );

						Vec3V normal = /*NormalizeFast*/(Cross(vEdge1, vEdge2));	// if you don't want vertex normals weighted by the size of the triangle then use NormalizeFast

						pNormals[i0] = rage::Add( pNormals[i0], normal );
						pNormals[i1] = rage::Add( pNormals[i1], normal );
						pNormals[i2] = rage::Add( pNormals[i2], normal );
					}

					for(int i = 0; i < numVerts; i++ )
					{
						pNormals[i] = NormalizeFast(pNormals[i]);		
					}

					pEnvCloth->ApplyAirResistance(pNormals, vWindSpeed);
				}
			}
#endif // #if CLOTH_INSTANCE_FROM_DATA
		}

#if __PS3
		if( sm_SpuUpdate )
		{
			phVerletSPUDebug* verletSpuDebug = 0;
#if ENABLE_SPU_DEBUG_DRAW && USE_CLOTHMANAGER_BANK
			if( cinst->m_SpuDebugIdx > -1 )
			{	
				verletSpuDebug = &s_VerletSpuDebug[cinst->m_SpuDebugIdx];
				verletSpuDebug->debugIdx = cinst->m_SpuDebugIdx;
			}
#endif
			m_TasksManager->AddTask( pEnvCloth->UpdateSpu(timeStep, verletSpuDebug), pEnvCloth);
		}
		else
#endif
		{
			pEnvCloth->UpdateCloth(timeStep, timeScale);
		}

		pEnvCloth->SetFlag( environmentCloth::CLOTH_IsUpdating, false);
	}
	else if( cinst->GetType() == clothManager::Character && cinst->IsActive() )
	{
		characterCloth* pCharCloth = (characterCloth*)ccontroller->GetOwner();
		Assert( pCharCloth );

		u8 lockCounter = pCharCloth->GetLockCounter();
		pCharCloth->SetLockCounter(  lockCounter - (lockCounter? 1:0) );

		Vec3V lastPos = pCharCloth->GetParentLastPos();

		characterClothController* pCharClothController = pCharCloth->GetClothController();
		Assert( pCharClothController );

#if USE_CLOTH_TELEMETRY
		sysInterlockedAdd( (volatile u32*)&m_TotalCharacterClothVertexCount, pCharClothController->GetCloth(0)->GetNumVertices() );
#endif	

		const crSkeleton* pSkeleton = cinst->GetSkeleton();
		Assert( pSkeleton );
		const Mat34V* pParentMtx = pSkeleton->GetParentMtx();
		Assert( pParentMtx );

// TODO: "patch"(pin) excessive rotations between frames
		Vec3V vAtOld = pCharCloth->GetParentAtVec();
		Vec3V vAtNew = pParentMtx->GetCol0();
		ScalarV ang = Dot(vAtOld, vAtNew);
		if( ang.Getf() < 0.1f )
		{
			pCharClothController->SetForcePin(1);
		}
//

		pCharCloth->SetParentMtx( *pParentMtx );

		if( !pCharCloth->IsActive() )
		{
			clothDebugf1("Inactive character cloth! Cloth controller ptr: 0x%p ", ccontroller);
			return;
		}

		Vec3V currPos = pParentMtx->GetCol3();

		Vec3V parentMatrixDelta = Subtract( currPos, lastPos );
		if( pCharClothController->GetFlag(characterClothController::enIsEndCutscene) && pCharClothController->GetWindScale() > 0.9f && Mag(parentMatrixDelta).Getf() > 2.0f )
		{
			pCharClothController->AddForcePin(1);
		}
		pCharClothController->SetFlag( characterClothController::enIsEndCutscene, false );

		Vec3V vWind; 
		WIND.GetLocalVelocity( currPos, vWind );

		Vec3V combinedforce = pCharCloth->IsForceNegated() ? Negate(vWind): vWind;

		combinedforce = rage::Scale( combinedforce, ScalarVFromF32(pCharClothController->GetWindScale()) );

		if( pCharClothController->GetFlag(characterClothController::enIsSkydiving) )
		{
			/*static*/ const float extraWind = 1.0f;
			combinedforce = Scale(Negate( combinedforce ), ScalarVFromF32(extraWind));
		}

#if __PS3
		if( sm_SpuCharacterClothUpdate )
		{
			combinedforce.SetWf(1.0f);
			// NOTE: ApplyAirResistance is on the SPU now.
			pCharCloth->SetForce( combinedforce );
		}
		else
#endif
		{
			Vec3V* RESTRICT normalsBuffer = pCharClothController->GetCloth(0)->GetClothData().GetNormalsPointer();
			Assert( normalsBuffer );

			// no LODs for character cloth yet

			clothBridgeSimGfx* clothBridge = pCharClothController->GetBridge();
			Assert( clothBridge );

			const float* RESTRICT inflationScale = clothBridge->GetInflationScale( 0 );
			Assert( inflationScale );
			const float* RESTRICT vertWeights = clothBridge->GetVertexWeights( 0 );
			Assert( vertWeights );
			pCharCloth->ApplyAirResistance( inflationScale, normalsBuffer, VEC3V_TO_VECTOR3(combinedforce), vertWeights ); 
		}

#if __PS3
		if( sm_SpuCharacterClothUpdate )
		{
			phVerletSPUDebug* verletSpuDebug = 0;
#if ENABLE_SPU_DEBUG_DRAW && USE_CLOTHMANAGER_BANK
			if( cinst->m_SpuDebugIdx > -1 )
			{	
				verletSpuDebug = &s_VerletSpuDebug[cinst->m_SpuDebugIdx];
				verletSpuDebug->debugIdx = cinst->m_SpuDebugIdx;
			}
#endif
			m_TasksManager->AddTask( pCharCloth->UpdateSpu( pSkeleton, timeStep, verletSpuDebug), pCharCloth );
		}
		else
#endif
		{
			pCharCloth->Update( 1.0f, pSkeleton, timeStep);
		}
	}

#if CLOTH_PROFILE && USE_CLOTHMANAGER_BANK
	float fUpdateTime = (sysTimer::GetTicksToMicroseconds() * (sysTimer::GetTicks() - startTime));
	cinst->m_UpdateTime = (fUpdateTime + cinst->m_LastUpdateTime) * 0.5f;
	cinst->m_LastUpdateTime = fUpdateTime;
#endif

}


void clothManager::UpdateAgainstCollisionInsts(rage::clothController* ccontroller, enPostInjectVerticesAction injectAction)
{
	Assert( ccontroller );
	clothInstance* cinst = (clothInstance*)ccontroller->GetClothInstance();
	Assert( cinst );

#if CLOTH_PROFILE && USE_CLOTHMANAGER_BANK
	utimer_t startTime = sysTimer::GetTicks();
#endif

	// NOTE: Environment cloth
	if( cinst->GetType() == clothManager::Environment || cinst->GetType() == clothManager::Vehicle )
	{
		environmentCloth* pEnvCloth = (environmentCloth*)ccontroller->GetOwner();
		Assert( pEnvCloth );
		phVerletCloth* pCloth = pEnvCloth->GetCloth();
		Assert( pCloth );

#if USE_CLOTH_TELEMETRY
		int numVerts = pCloth->GetNumVertices();
		sysInterlockedIncrement( (volatile u32*)&m_UpdatedCount );
		sysInterlockedAdd( (volatile u32*)&m_TotalActiveEnvironmentClothVertexCount, numVerts );
#endif		

		clothAssertf((injectAction & (enPostInjectVerticesAction)(checkAganstCollisionInsts | updateDrawableGeometry)) || ((injectAction & checkAganstCollisionInsts) == 0), "clothManager::UpdateAgainstCollisionInsts()....Invalid action flags");
		pEnvCloth->UpdateAgainstCollisionInsts((injectAction & checkAganstCollisionInsts) != 0, (injectAction & runSimulation) == 0);

		if(injectAction & resetDeltas)
		{
			cinst->SetLastPosition(VECTOR3_TO_VEC3V(pEnvCloth->GetFramePosition()));
			pEnvCloth->GetCloth()->ZeroDeltaPositions_WithCompression();
		}
		// Say we've finished updating if the simulation will not be ran.
		if((injectAction & runSimulation) == 0)
			pEnvCloth->SetFlag( environmentCloth::CLOTH_IsUpdating, false);
	}
	else if( cinst->GetType() == clothManager::Character && cinst->IsActive() )
	{
		// TODO:- Heed injection action flags when we add ped cloth in general.
		characterCloth* pCharCloth = (characterCloth*)ccontroller->GetOwner();	
		Assert( pCharCloth );

		u8 lockCounter = pCharCloth->GetLockCounter();
		pCharCloth->SetLockCounter(  lockCounter - (lockCounter? 1:0) );

		characterClothController* pCharClothController = pCharCloth->GetClothController();
		Assert( pCharClothController );

#if USE_CLOTH_TELEMETRY
		sysInterlockedAdd( (volatile u32*)&m_TotalCharacterClothVertexCount, pCharClothController->GetCloth(0)->GetNumVertices() );
#endif	

		const crSkeleton* pSkeleton = cinst->GetSkeleton();
		Assert( pSkeleton );
		const Mat34V* pParentMtx = pSkeleton->GetParentMtx();
		Assert( pParentMtx );

		pCharCloth->SetParentMtx( *pParentMtx );
		pCharCloth->UpdateAgainstCollisionInsts(pSkeleton);
	}

#if CLOTH_PROFILE && USE_CLOTHMANAGER_BANK
	float fUpdateTime = (sysTimer::GetTicksToMicroseconds() * (sysTimer::GetTicks() - startTime));
	cinst->m_UpdateTime = (fUpdateTime + cinst->m_LastUpdateTime) * 0.5f;
	cinst->m_LastUpdateTime = fUpdateTime;
#endif
}


#if CLOTH_INSTANCE_FROM_DATA && USE_CLOTHMANAGER_BANK

clothVariationData* clothManager::InstanceCharacterCloth( const crSkeleton* pSkel )
{
	Assert( pSkel );
	characterCloth* pCharCloth = rage_new characterCloth();
	Assert( pCharCloth );

	pCharCloth->InstanceFromData(254/*vertsCapacity*/, 2048/*edgesCapacity*/, 16/*boneIDsCapacity*/);
	
	characterClothController* pCharClothController = pCharCloth->GetClothController();
	Assert( pCharClothController );
	Assert( pCharClothController->GetCurrentCloth() );
	pCharClothController->SetOwner( pCharCloth );
	pCharClothController->ResetW();
	pCharClothController->SetForcePin(1);
	pCharClothController->SetName( clothManager::ms_ClothName );

	clothInstance* pClothInstance = Add(pCharClothController, clothManager::Character);
	Assert( pClothInstance );

	pClothInstance->SetSkeletonOnly( pSkel );

	pClothInstance->SetActive( false );

#if USE_CLOTHMANAGER_BANK
	SetupWidget( pCharClothController, clothManager::Character, false );
#endif

	clothDebugf1("Add character cloth. Number of active cloth: %d", m_ActiveCount );


	clothVariationData* pClothVarData = AllocateVarData();
	Assert( pClothVarData );

// TODO: temp
	pClothVarData->Set( pCharCloth);

// TODO: needed only for rendering
//	const int numVerts = pCharClothController->GetCurrentCloth()->GetNumVertices();
//	pClothVarData->Set( numVerts, pCharCloth, dictSlotId );
//	pCharCloth->SetEntityVerticesPtr( pClothVarData->GetClothVertices(clothManager::GetBufferIndex()) );
//	pCharCloth->SetEntityParentMtxPtr( pClothVarData->GetClothParentMatrixPtr() );

//	pCharClothController->SkinMesh( pSkel );

// TODO: needed only for rendering
/*
	const Mat34V* parentMtx = pSkel->GetParentMtx();
	Assert( parentMtx );
	pCharCloth->SetParentMtx( *parentMtx );

	for(int i = 0; i < NUM_CLOTH_BUFFERS; ++i)
	{
		Vec3V* RESTRICT verts = pClothVarData->GetClothVertices(i);
		Assert( verts );
		// NOTE: don't want to crash if the game run out of streaming memory 
		if( verts )
		{
			Vec3V* RESTRICT origVerts = pCharClothController->GetCloth(0)->GetClothData().GetVertexPointer();
			Assert( origVerts );
			// origVerts should exists, if this fails there is bug somewhere in cloth instancing
			for(int j=0; j<numVerts; ++j)
			{
				verts[j] = origVerts[j];
#if RSG_PC || RSG_DURANGO || RSG_ORBIS
				// clear out W channel to prevent NaNs in shader vars
				verts[j].SetW(MAGIC_ZERO);
#endif
			}
		}
	}
*/

// TODO: needed only for rendering
//	gDrawListMgr->CharClothAdd( clothVarData );

	return pClothVarData;
}


environmentCloth* clothManager::InstanceEnvironmentCloth( Mat34V_In frame, bool activateOnHit)
{
#if !__RESOURCECOMPILER 
	sysMemAllocator& oldAllocator = sysMemAllocator::GetCurrent();
	sysMemAllocator::SetCurrent( sysMemAllocator::GetMaster() );
#endif

	environmentCloth* pEnvCloth = rage_new environmentCloth();
	Assert( pEnvCloth );
	clothDebugf1("Environment cloth instanced: 0x%p", pEnvCloth);

	pEnvCloth->InstanceFromData((int)clothManager::ms_VertsCapacity, (int)clothManager::ms_EdgesCapacity);	
	pEnvCloth->SetAttachedToFrame( RCC_MATRIX34(frame) );
	pEnvCloth->SetInitialPosition( frame.GetCol3() );	
	
	phClothVerletBehavior* pClothBehavior = pEnvCloth->CreateBehavior();	
	Assert( pClothBehavior );
	pClothBehavior->SetActivateOnHit( activateOnHit );
	


	Vec3V translationV = frame.GetCol3();
	Mat34V tMat = frame;
	tMat.SetCol3( Vec3V(V_ZERO) );
	for( int i = 0; i < LOD_COUNT; ++i )
	{
		phVerletCloth* pCloth = pEnvCloth->GetCloth(i);
		if( pCloth )
		{
			int numPinnedVerts = pCloth->GetClothData().GetNumPinVerts();
			pCloth->GetClothData().TransformVertexPositions( tMat, numPinnedVerts );
		}
	}

// TODO: no support for cloth lods at the moment
// 	{
// 		pEnvCloth->CheckDistance( m_CamPos );
// 	}

	if( !activateOnHit )
	{
		Mat34V tMat(V_IDENTITY);
		tMat.SetCol3( translationV );
		for( int i = 0; i < LOD_COUNT; ++i )
		{
			phVerletCloth* pCloth = pEnvCloth->GetCloth(i);
			if( pCloth )
			{
 				int numPinnedVerts = pCloth->GetClothData().GetNumPinVerts();
 				pCloth->GetClothData().TransformVertexPositions( tMat, numPinnedVerts );
				pCloth->ComputeClothBoundingVolume();
				pCloth->SetBBMin( rage::Add(pCloth->GetBBMin(), translationV) );
				pCloth->SetBBMax( rage::Add(pCloth->GetBBMax(), translationV) );
			}
		}			
	} 

// TODO: change this to true once the mesh generation is working
	pEnvCloth->SetFlag(environmentCloth::CLOTH_IsVisible, false);

	pEnvCloth->AllocateDamageArray();

	clothController* pClothController = pEnvCloth->GetClothController();
	Assert( pClothController );
	pClothController->SetName(clothManager::ms_ClothName);

	Add(pClothController, activateOnHit ? Vehicle: Environment);

#if USE_CLOTHMANAGER_BANK
	SetupWidget( pClothController, activateOnHit ? Vehicle: Environment, false );
#endif

#if !__RESOURCECOMPILER 
	sysMemAllocator::SetCurrent(oldAllocator);
#endif // !__RESOURCECOMPILER 

	return pEnvCloth;
}
#endif // CLOTH_INSTANCE_FROM_DATA


environmentCloth* clothManager::InstanceCloth( Mat34V_In phInstMat, const environmentCloth* pClothTemplate, bool activateOnHit, bool isSubInst)
{
#if !__RESOURCECOMPILER 
	sysMemAllocator& oldAllocator = sysMemAllocator::GetCurrent();
	sysMemAllocator::SetCurrent( sysMemAllocator::GetMaster() );
#endif

	if(!gDrawListMgr->GetClothPoolFreeCount() || !(gDrawListMgr->GetClothPoolFreeCount() - 1))
		return NULL;

	void* preAllocatedMemory = 0;
	
#if !__RESOURCECOMPILER

	bool bIsVehicleCloth =  activateOnHit;

	int memoryAvailable = 0;

	Assert( pClothTemplate );
	rmcDrawable* pTypeDrawable = pClothTemplate->GetDrawable();
	Assert( pTypeDrawable );

#if USE_PRE_ALLOC_MEMORY || 1//__BANK

	rmcLod& lod = pTypeDrawable->GetLodGroup().GetLod( 0 );
// NOTE: model idx and geom idx are always 0 and 0
	Assert( lod.GetModel(0) );
	grmGeometry& geomType = lod.GetModel(0)->GetGeometry(0);
	grcVertexBuffer* vb = ((grmGeometryQB*)&geomType)->GetVertexBufferByIndex(0);
	Assert( vb );
	Assert( vb->GetFvf() );

#endif // USE_PRE_ALLOC_MEMORY || __BANK

#if 1//__BANK	

	Assert( pClothTemplate->GetClothController() );
	Assert( pClothTemplate->GetClothController()->GetCloth(0) );

	const int clothSimVerts = pClothTemplate->GetClothController()->GetCloth(0)->GetNumVertices();
	const int clothDrawableVerts = vb->GetVertexCount();
	if( clothSimVerts != clothDrawableVerts )
	{
		Assertf( clothSimVerts == clothDrawableVerts, "The number of verts in the cloth %s: %d doesn't match the number of the verts in the drawable: %d. Please correct the asset (game will crash in release builds) ! ", pClothTemplate->GetClothController()->GetName(), clothSimVerts, clothDrawableVerts );
		return NULL;
	}

	#if NO_PIN_VERTS_IN_VERLET
		const int clothPinVerts = pClothTemplate->GetClothController()->GetCloth(0)->GetClothData().GetNumPinVerts();
	#else
		const int clothPinVerts = pClothTemplate->GetClothController()->GetCloth(0)->GetPinCount();
	#endif
		if( (clothSimVerts-1) < clothPinVerts )
		{
			Assertf( (clothSimVerts-1) > clothPinVerts, "The number of pin verts in cloth %s: %d is too high relative to total number of verts: %d. This cloth is just wasting memory and CPU. Please correct the asset (game will crash in release builds) ! ", pClothTemplate->GetClothController()->GetName(), clothPinVerts, clothSimVerts );
			return NULL;
		}

#endif // __BANK

	// NOTE: env cloth shouldn't have more than one drawable ( no LODs ) ... i.e. not interested in the rest if any !
#if USE_PRE_ALLOC_MEMORY

	if( vb->GetVertexCount() > MAX_CLOTH_VERTS_ON_SPU )
	{			
		Displayf("[InstanceCloth] Failed. Verts count not correct! ");
		Assertf( vb->GetVertexCount() <= MAX_CLOTH_VERTS_ON_SPU, "Too many verts in the cloth(%s): %d . The maximum/suggested vertex limit for env cloth is %d. Please reduce number of verts! ", pClothTemplate->GetClothController()->GetName(), vb->GetVertexCount(), MAX_CLOTH_VERTS_ON_SPU );
		return NULL;
	}

	const s32 vbsize = vb->GetVertexCount() * vb->GetFvf()->GetTotalSize();
	grcIndexBuffer* ib = ((grmGeometryQB*)&geomType)->GetIndexBuffer();
	Assert( ib );
	const s32 ibsize = (ib->GetIndexCount() * sizeof(u16) + 15) & (~15);		// align to 16 bytes

	const int totalBufferSize = vbsize + ibsize;
	const int totalMemoryRequested = totalBufferSize * grmGeometry::GetVertexMultiBufferCount();
	memoryAvailable = totalMemoryRequested;
	preAllocatedMemory = AllocClothVB( totalMemoryRequested );

	// NOTE: if there is no streaming memory available then just don't create cloth instance
	if( !preAllocatedMemory )
	{
#if !__RESOURCECOMPILER 
		sysMemAllocator::SetCurrent(oldAllocator);
#endif
		Displayf("[InstanceCloth] Failed. Out of streaming memory! ");
		clothWarningf( "The game run out of streaming memory. Cloth instance won't be created." );
		return NULL;
	}
#endif // USE_PRE_ALLOC_MEMORY



#endif //!__RESOURCECOMPILER

	environmentCloth* envCloth = rage_new environmentCloth();
	Assert( envCloth );
	clothDebugf1("Environment cloth instanced: 0x%p", envCloth);

	envCloth->InstanceFromTemplate(pClothTemplate);

	clothController* pClothController = envCloth->GetClothController();
	Assert( pClothController );

	// TODO: still in experimental state, cloth vertex data should be in local space !
#if __BANK
	const char* clothName;
	if( PARAM_saveclothstate.Get(clothName) ) 
	{
		const char* controllerName = pClothController->GetName();
		Assert( controllerName );
		if( !strcmpi(clothName, controllerName) )
		{
			envCloth->GetCloth(0)->GetClothData().Save( const_cast<char*>(controllerName) );
		}
	}

	if( PARAM_loadclothstate.Get(clothName) ) 
	{
		const char* controllerName = pClothController->GetName();
		Assert( controllerName );
		if( !strcmpi(clothName, controllerName) )
		{
			envCloth->GetCloth(0)->GetClothData().Load( phClothData::sm_tuneFilePath, controllerName );
		}
	}
#endif

	envCloth->SetAttachedToFrame( RCC_MATRIX34(phInstMat) );

	const clothInstanceTuning* pClothTuning = envCloth->GetTuning();
	if( pClothTuning )
	{
		activateOnHit |= pClothTuning->GetFlag( clothInstanceTuning::CLOTH_TUNE_ACTIVATE_ON_HIT );
	}

	phClothVerletBehavior* pClothBehavior = envCloth->CreateBehavior();	
	Assert( pClothBehavior );
	pClothBehavior->SetActivateOnHit( activateOnHit );
	envCloth->SetInitialPosition( phInstMat.GetCol3() );	

#if __BANK
	if( PARAM_initclothuserdata.Get(clothName) ) 
	{
		if( !strcmpi(clothName, pClothController->GetName()) )
		{
			envCloth->InitUserData();
		}
	}
#endif

#if __PS3
	atArray<int>& clothUserData = envCloth->GetUserData();
	if( clothUserData.GetCount() )
		pClothBehavior->SetUserDataAddress( (u32)clothUserData.GetElements() );
	else
		pClothBehavior->SetUserDataAddress( 0 );
#endif


	Vec3V translationV(V_ZERO);
	for( int i = 0; i < LOD_COUNT; ++i )
	{
		Mat34V tMat = phInstMat;
		{
			// NOTE: for vehicles we need the cloth in local space
			tMat.SetCol3( Vec3V(V_ZERO) );
		}
		phVerletCloth* pCloth = envCloth->GetCloth(i);
		if( pCloth )
		{
			pCloth->GetClothData().TransformVertexPositions( tMat, 0 );
			pCloth->ComputeClothBoundingVolume();
		}
	}		

	envCloth->AllocateDamageArray();

	DIAG_CONTEXT_MESSAGE("InstanceCloth_%s", pClothController->GetName()); 

	if( !activateOnHit )
	{
		for( int i = 0; i < LOD_COUNT; ++i )
		{
			phVerletCloth* pCloth = envCloth->GetCloth(i);
			if( pCloth )
			{
				static float scaleRadius = 3.0f;
				/*bool res = */ AttachGlobalCustomBound( pCloth, phInstMat.GetCol3(), ScalarVFromF32(pCloth->GetRadius(pCloth->GetCenter()) * scaleRadius), pClothController->GetName() );
			}
		}
	}

// TODO: transform the custom bounds (which are in the template) once 
// Step 2 clone the bounds and don't touch template
#if !__RESOURCECOMPILER
	if( pClothTemplate->GetFlag( environmentCloth::CLOTH_HasLocalBounds ) )
	{
		(const_cast<environmentCloth*>(pClothTemplate))->SetFlag( environmentCloth::CLOTH_HasLocalBounds, false );
		envCloth->SetFlag( environmentCloth::CLOTH_HasLocalBounds, false );

		Vec3V vInitialPos = envCloth->GetInitialPosition();

		for( int i = 0; i < LOD_COUNT; ++i )
		{
			phVerletCloth* pCloth = (const_cast<environmentCloth*>(envCloth))->GetCloth(i);
			if( pCloth )
			{
				phBoundComposite* pCompositeBound = (phBoundComposite*)(const_cast<phBound*>( pCloth->GetCustomBound() ));
				if( pCompositeBound )
				{
					for( int k = 0; k < pCompositeBound->GetNumBounds(); ++k )
					{
					// TODO: add check for plane bounds
						Mat34V currentMat = pCompositeBound->GetCurrentMatrix(k);
						currentMat.SetCol3( rage::Add( currentMat.GetCol3(), vInitialPos) );
						pCompositeBound->SetCurrentMatrix( k, currentMat );
						pCompositeBound->SetLastMatrix( k, currentMat );
					}
				}
			}
		}
	}
#endif

	AddEnvironmentCloth( pClothController, isSubInst, preAllocatedMemory, activateOnHit ? Vehicle: Environment );

#if !__RESOURCECOMPILER 
	sysMemAllocator::SetCurrent(oldAllocator);
#endif // !__RESOURCECOMPILER 

#if !__RESOURCECOMPILER 
	Assert(m_InstanceTasksManager);
	if( !bIsVehicleCloth && !PARAM_noclothmtinstance.Get() && !m_DontUseInstanceManager && m_InstanceTasksManager->GetTasksCount() < clothInstanceTaskManager::MAX_INSTANCE_TASKS )
	{
		static sysTaskParameters p;
		p.Input.Data = this;
//		p.Output.Data = envCloth;
		p.UserData[0].asPtr = pTypeDrawable;
		p.UserData[1].asPtr = preAllocatedMemory;
		p.UserData[2].asPtr = envCloth;
		p.UserData[3].asInt = memoryAvailable;
#if USE_PRE_ALLOC_MEMORY
		p.UserData[4].asInt = totalBufferSize;
		p.UserData[5].asInt = totalMemoryRequested;
#endif
		p.UserData[6].asFloat = translationV.GetXf();
		p.UserData[7].asFloat = translationV.GetYf();
		p.UserData[8].asFloat = translationV.GetZf();
		
		p.UserDataCount = 9;

		m_InstanceTasksManager->AddTask( sysTaskManager::Create(TASK_INTERFACE(InstanceClothDrawableTask), p, m_clothInstanceScheduler) );
	}
	else
#endif // __RESOURCECOMPILER
	{
#if !__RESOURCECOMPILER && __ASSERT
		if( !PARAM_noclothmtinstance.Get() && !m_DontUseInstanceManager )
		{
			Warningf("Instance Tasks reached the maximum of %d. You can increase MAX_INSTANCE_TASKS but something is wrong!", clothInstanceTaskManager::MAX_INSTANCE_TASKS);
			AssertMsg( m_InstanceTasksManager->GetTasksCount() < clothInstanceTaskManager::MAX_INSTANCE_TASKS, "Reached the limit of instance tasks. You can increase MAX_INSTANCE_TASKS but something is wrong!" );
		}
#endif

		clothDrawableParams drawableParams;

		drawableParams.pPreAllocatedMemory = preAllocatedMemory;
#if !__RESOURCECOMPILER
		drawableParams.pTypeDrawable = pTypeDrawable;
		drawableParams.iMemoryAvailable = memoryAvailable;
#endif
		drawableParams.pEnvCloth = envCloth;
		drawableParams.translationV = translationV;
#if USE_PRE_ALLOC_MEMORY
		drawableParams.iTotalBufferSize = totalBufferSize;
		drawableParams.iTotalMemoryRequested = totalMemoryRequested;
#endif

		InstanceClothDrawable( &drawableParams );
	}	

	return envCloth;
}


void clothManager::InstanceClothDrawable( clothDrawableParams* pDrawableParams )
{	
	Assert( pDrawableParams );

#if CLOTH_PROFILE && USE_CLOTHMANAGER_BANK
	utimer_t startTime = sysTimer::GetTicks();
#endif

#if !__RESOURCECOMPILER
	rmcDrawable* pTypeDrawable	= pDrawableParams->pTypeDrawable;
	void* pPreAllocatedMemory	= pDrawableParams->pPreAllocatedMemory;
	int iMemoryAvailable		= pDrawableParams->iMemoryAvailable;
#endif

	environmentCloth* pEnvCloth	= pDrawableParams->pEnvCloth;
	Vec3V_In translationV		= pDrawableParams->translationV;
#if USE_PRE_ALLOC_MEMORY
	const int iTotalBufferSize	= pDrawableParams->iTotalBufferSize;
 #if __ASSERT
	const int iTotalMemoryRequested = pDrawableParams->iTotalMemoryRequested;
 #endif
#endif // USE_PRE_ALLOC_MEMORY

	Assert( pEnvCloth );
	clothController* pClothController = pEnvCloth->GetClothController();
	Assert( pClothController );

#if !__RESOURCECOMPILER 
	sysMemAllocator& oldAllocator = sysMemAllocator::GetCurrent();
	sysMemAllocator::SetCurrent( sysMemAllocator::GetMaster() );
#endif

	rmcDrawable* drawme = NULL;	

#if !__RESOURCECOMPILER 

#if USE_PRE_ALLOC_MEMORY

	rmcLod& lod = pTypeDrawable->GetLodGroup().GetLod( 0 );
// NOTE: model idx and geom idx are always 0 and 0
	Assert( lod.GetModel(0) );
	grmGeometry& geomType = lod.GetModel(0)->GetGeometry(0);

#endif // USE_PRE_ALLOC_MEMORY || __BANK


 #if USE_PRE_ALLOC_MEMORY
	Assert( pPreAllocatedMemory );
	Assert( iMemoryAvailable );
 #endif
	Assert( pTypeDrawable );
	
	int tempModelIdx;
	int tempGeomIdx;
	atUserArray<int> tempModelIdxs(&tempModelIdx, 1, true);
	atUserArray<int> tempGeomIdxs(&tempGeomIdx, 1, true);

	tempModelIdx = 0 /*clothT.GetClothController()->GetModelIdx()*/;
	tempGeomIdx = 0 /*clothT.GetClothController()->GetGeometryIdx()*/;

	drawme = rage_new rmcDrawable;	
	Assert( drawme );
	drawme->CloneWithNewVertexData( *pTypeDrawable, 0, tempModelIdxs, tempGeomIdxs, true, pPreAllocatedMemory, iMemoryAvailable);

	grmGeometry& geom = drawme->GetLodGroup().GetLod(0).GetModel( 0 /*tempModelIdxs[0]*/ )->GetGeometry( 0/*tempGeomIdxs[0]*/ );

 #if USE_PRE_ALLOC_MEMORY
	int countVB = 0;
	for( int i = 0; i < MAX_VERT_BUFFERS; ++i)
	{
		if( ((grmGeometryQB*)&geomType)->GetVertexBufferByIndex(i) )
			++countVB;
	}

	Assert( (countVB*iTotalBufferSize) == (iTotalMemoryRequested - iMemoryAvailable) );
	geom.EnableTripleBuffer( grmGeometryQB::TRIPLE_VERTEX_BUFFER | grmGeometryQB::TRIPLE_INDEX_BUFFER 
		, pPreAllocatedMemory ? (((char*)pPreAllocatedMemory) + countVB*iTotalBufferSize): NULL
		, iMemoryAvailable );

	Assert( iMemoryAvailable == 0 );
 #else
	geom.EnableTripleBuffer( grmGeometryQB::TRIPLE_VERTEX_BUFFER, NULL, iMemoryAvailable );
 #endif

	grcVertexBuffer* vb0 = ((grmGeometryQB*)&geom)->GetVertexBufferByIndex(0);
	Assert(vb0);
	const char normalOffset	= (char)vb0->GetFvf()->GetOffset(grcFvf::grcfcNormal);
	const char uvOffset		= (char)vb0->GetFvf()->GetOffset(grcFvf::grcfcTexture0);

	for( int i = 0; i < MAX_VERT_BUFFERS; ++i)
	{
		grcVertexBuffer* vbInstance = ((grmGeometryQB*)&geom)->GetVertexBufferByIndex(i);
		if( vbInstance )
		{
			Assert( vbInstance->GetFvf() );
			grcFvf* fvfInstance = const_cast<grcFvf*>(vbInstance->GetFvf());
			Assert(fvfInstance);
			fvfInstance->SetDynamicOrder(true);
		}
	}

	pClothController->SetNormalOffset( normalOffset );
	pClothController->SetUVOffset( uvOffset );
#endif // !__RESOURCECOMPILER 

	pEnvCloth->SetReferencedDrawable( drawme, true );

	pEnvCloth->CheckDistance( m_CamPos );

	rmcLodGroup& lodGroup = drawme->GetLodGroup();
	int renderableLodIndex = lodGroup.GetCurrentLOD();
	const grmModel* pModel = lodGroup.GetLod( renderableLodIndex ).GetModel( 0 /*GetModelIdx()*/);
	Assert(pModel);

	pClothController->ApplySimulationToMesh( (grmGeometryQB&)pModel->GetGeometry( 0 /*GetGeometryIdx()*/), VEC3V_TO_VECTOR3(translationV), renderableLodIndex, translationV );

#if !__RESOURCECOMPILER && !__GAMETOOL
	// NOTE: scope it so vertex buffer is unlocked properly
	{
		grcVertexBuffer* pBuildBuffer = ((grmGeometryQB*)&geom)->GetVertexBuffer();
		Assert( pBuildBuffer );
		grcVertexBuffer* pDrawBuffer = ((grmGeometryQB*)&geom)->GetVertexBuffer(true);
		Assert( pDrawBuffer );
		grcVertexBufferEditor vertexBufferEditor( pDrawBuffer );
		vertexBufferEditor.Set( pBuildBuffer );
	}
#endif



#if !__RESOURCECOMPILER 
	sysMemAllocator::SetCurrent(oldAllocator);

	gDrawListMgr->ClothAdd( pEnvCloth );
#endif // !__RESOURCECOMPILER 

	clothInstance* pClothInstance = (clothInstance*)pClothController->GetClothInstance();
	Assert( pClothInstance );
#if CLOTH_PROFILE && USE_CLOTHMANAGER_BANK	
	pClothInstance->m_InstanceTime = sysTimer::GetTicksToMicroseconds() * (sysTimer::GetTicks() - startTime);
#endif
	pClothInstance->SetReady();
}


void clothManager::RemoveCloth(environmentCloth* pEnvCloth, const fragType* pFragType)
{
	(void)pFragType;

	Assert( pEnvCloth );

#if USE_CLOTH_PHINST
	pEnvCloth->ClearBound();	
	pEnvCloth->ClearInst();
#endif


#if !__RESOURCECOMPILER 
 #if USE_PRE_ALLOC_MEMORY
	void* preAllocatedMemory = ((clothInstance*)(pEnvCloth->GetClothController()->GetClothInstance()))->preAllocatedMemory;
 #else
	void* preAllocatedMemory = NULL;
 #endif

	// pFragType may validly be null, as the plan for cloth is to not require a frag for instancing
	if (pFragType BANK_ONLY(&& !clothManager::ms_SceneIsFlushing))
	{
		const strIndex strIdx = strStreamingEngine::GetStrIndex(pFragType);
		Assert(strIdx.IsValid());
		const u32 objIdx = g_FragmentStore.GetObjectIndex(strIdx).Get();
		gDrawListMgr->AddFragReference(objIdx);
	}

// TODO: if statement is just temporary here because the function is used and for removing pure cloth sim meshes ( i.e without drawables )
	if( pEnvCloth->GetDrawable() )
	{
		gDrawListMgr->ClothRemove( pEnvCloth, preAllocatedMemory );
	}
#endif

#if USE_CLOTHMANAGER_BANK
	sysMemStartTemp();
	((clothInstance*)(pEnvCloth->GetClothController()->GetClothInstance()))->m_Indices.Reset();
	sysMemEndTemp();
#endif


	if( !pEnvCloth->GetDrawable() )
	{
		clothController* pClothController = pEnvCloth->GetClothController();
		Assert( pClothController );
		clothInstance* pClothInstance = GetClothInst(pClothController);
		Assert( pClothInstance );
		pEnvCloth->Shutdown();

#if USE_CLOTHMANAGER_BANK		
		RemoveWidget( pClothController );
#endif
		
		Remove( pClothInstance );
	}
	else
	{
		RemoveEnvironmentCloth( pEnvCloth->GetClothController() );
	}
}

void clothManager::AddCharacterCloth(rage::clothController* ccontroller, const crSkeleton* skeleton)
{
	clothInstance* verletInst = Add(ccontroller, clothManager::Character);

	Assert( verletInst );
	Assert( skeleton );
	verletInst->SetSkeleton( skeleton );

	verletInst->SetActive( true );

#if USE_CLOTHMANAGER_BANK
	SetupWidget( ccontroller, clothManager::Character, false );
#endif

	clothDebugf1("Add character cloth. Number of active cloth: %d", m_ActiveCount );
}

void clothManager::AddEnvironmentCloth(rage::clothController* ccontroller
	, bool 
#if USE_CLOTHMANAGER_BANK
	isSubInst
#endif
	, void* 
#if USE_PRE_ALLOC_MEMORY
	memPtr 
#endif
	, clothType cType
	)
{
#if USE_PRE_ALLOC_MEMORY
	Assert( memPtr );
#endif

	Add(ccontroller, cType);

#if USE_PRE_ALLOC_MEMORY		
	clothInstance* cinst = (clothInstance*)ccontroller->GetClothInstance();
	Assert( cinst );	
	cinst->preAllocatedMemory = memPtr;
#endif

#if USE_CLOTHMANAGER_BANK
	SetupWidget( ccontroller, cType, isSubInst );
#endif
}

void clothManager::RemoveCharacterCloth(rage::clothController* ccontroller) 
{ 
#if USE_CLOTHMANAGER_BANK
	RemoveWidget( ccontroller );
#endif

	Assert( ccontroller );
	Remove( GetClothInst(ccontroller) ); 

	clothDebugf1("Remove character cloth. Number of active cloth: %d", m_ActiveCount );
}

void clothManager::RemoveEnvironmentCloth(rage::clothController* ccontroller) 
{ 
	Assert( ccontroller );

	clothInstance* cinst = GetClothInst(ccontroller);
	Assert( cinst );
	environmentCloth* envCloth = (environmentCloth*)ccontroller->GetOwner();
	Assert( envCloth );

	while( envCloth->GetFlag( environmentCloth::CLOTH_IsUpdating) )
	{
		sysIpcSleep(1);
	}

	envCloth->Shutdown();

#if USE_PRE_ALLOC_MEMORY && 0
	Assert( cinst->preAllocatedMemory );

	rmcDrawable* pClothDrawable = envCloth->GetDrawable();
	Assert( pClothDrawable );

	// NOTE: env cloth shouldn't have more than one drawable ( no LODs ) ... i.e. not interested in the rest if any !
	const int lodIndex = 0;
	rmcLod* lod = &pClothDrawable->GetLodGroup().GetLod(lodIndex);
	Assert( lod );
	// TODO: are model idx and geom idx always 0 and 0?
	grmGeometry& geom = lod->GetModel( 0 )->GetGeometry( 0 );

	atFixedArray<grcVertexBuffer*, MAX_VERT_BUFFERS> nullVB;
	for( int i = 0; i < MAX_VERT_BUFFERS; ++i)
		nullVB.Append() = NULL;

	((grmGeometryQB*)&geom)->ReplaceVertexBuffers( nullVB );
	cinst->preAllocatedMemory = NULL;

#endif		
// TODO: this is from MaxPayne times and was for PC only purposes, but doesn't work now. should be deleted
//	envCloth->CleanUpDrawable();

#if USE_CLOTHMANAGER_BANK
	RemoveWidget( ccontroller );
#endif

	Remove( cinst );
}

clothInstance* clothManager::Add(rage::clothController* ccontroller, int verletType)
{
	Assert( ccontroller );
	clothInstance* verlet = AllocateVerlet();
	Assert( verlet );
	verlet->Init(ccontroller, verletType);
	return verlet;
}

void clothManager::Remove( clothInstance *cloth )
{
	Assert( cloth );
	cloth->Shutdown();
	DeallocateVerlet(cloth);
}

clothInstance* clothManager::AllocateVerlet()
{
	sysCriticalSection critSec(s_LockActiveList);

	atDList<clothInstance*> *pActiveList = &m_ActiveList;
	atDList<clothInstance*> *pFreeList = &m_FreeList;

	//	Assert(CheckDListSanity(*pActiveList));
	//	Assert(CheckDListSanity(*pFreeList));

	atDNode<clothInstance*> *node = pFreeList->GetHead();
	AssertMsg( node, "Cloth manager run out of free nodes ( It shouldn't )!" );

	pFreeList->PopNode(*node);
	pActiveList->Append(*node);

	Assert(m_FreeCount>0);
	m_FreeCount--;
	m_ActiveCount++;

	clothDebugf1("[AllocateVerlet].   Active: %d   Free: %d", m_ActiveCount, m_FreeCount );

	//	Assert(CheckDListSanity(*pActiveList));
	//	Assert(CheckDListSanity(*pFreeList));

	return node->Data;
}

template<class T> bool IsInDList(const rage::atDList<T> &list, const rage::atDNode<T> &node)
{
	const rage::atDNode<T> *p1 = list.GetHead();
	while (p1)
	{
		if (p1==&node)
			return true;
		p1 = p1->GetNext();
	}
	return false;
}

void clothManager::DeallocateVerlet(const clothInstance *pCloth)
{
	sysCriticalSection critSec(s_LockActiveList);

	Assert( pCloth );

	atDList<clothInstance*> *pActiveList = &m_ActiveList;
	atDList<clothInstance*> *pFreeList = &m_FreeList;

	Assert( IsInDList(*pActiveList, pCloth->GetNodeRef()) );

	//	Assert(CheckDListSanity(*pActiveList));
	//	Assert(CheckDListSanity(*pFreeList));

	atDNode<clothInstance*> &node = pCloth->GetNodeRef();
	pActiveList->PopNode(node);
	pFreeList->Append(node);

	Assert(m_ActiveCount>0);
	m_FreeCount++;
	m_ActiveCount--;

	clothDebugf1("[DeallocateVerlet].   Active: %d   Free: %d", m_ActiveCount, m_FreeCount );

	//	Assert(CheckDListSanity(*pActiveList));
	//	Assert(CheckDListSanity(*pFreeList));
}


clothVariationData* clothManager::AllocateVarData()
{
	sysCriticalSection critSec(s_LockActiveList);

	atDList<clothVariationData*> *pActiveVarList = &m_ActiveVarList;
	atDList<clothVariationData*> *pFreeVarList = &m_FreeVarList;

	atDNode<clothVariationData*> *node = pFreeVarList->GetHead();
	AssertMsg( node, "Cloth manager run out of free nodes ( It shouldn't )!" );

	pFreeVarList->PopNode(*node);
	pActiveVarList->Append(*node);

	Assert( m_FreeVarCount > 0 );
	m_FreeVarCount--;
	m_ActiveVarCount++;

	clothDebugf1("[AllocateVarData].   Active: %d   Free: %d", m_ActiveVarCount, m_FreeVarCount );

	return node->Data;
}

void clothManager::DeallocateVarData(const clothVariationData* varData)
{
	sysCriticalSection critSec(s_LockActiveList);

	Assert( varData );

	atDList<clothVariationData*> *pActiveVarList = &m_ActiveVarList;
	atDList<clothVariationData*> *pFreeVarList = &m_FreeVarList;

	Assert( IsInDList(*pActiveVarList, varData->GetNodeRef()) );

	atDNode<clothVariationData*> &node = varData->GetNodeRef();
	pActiveVarList->PopNode(node);
	pFreeVarList->Append(node);

	Assert( m_ActiveVarCount > 0 );
	m_FreeVarCount++;
	m_ActiveVarCount--;

	clothDebugf1("[DeallocateVarData].   Active: %d   Free: %d", m_ActiveVarCount, m_FreeVarCount );
}


void clothManager::InitInterfaceCallbacks( fragManager::ClothInterface& clothInterface )
{
	clothInterface.m_InstanceEnvironmentCloth.Bind( this, &clothManager::InstanceCloth );
	clothInterface.m_RemoveEnvironmentCloth.Bind( this, &clothManager::RemoveCloth );
}

void* clothManager::AllocClothVB(int size)
{
	Assert( size > 0 );
	MEM_USE_USERDATA(MEMUSERDATA_CLOTH);

//	datResourceMap map;
//	sysMemSet(&map, 0, sizeof(map));
//	map.VirtualCount = 1;
//	map.Chunks[0].Size =  _RoundUpPowerOf2((u32)size);
//	strStreamingEngine::GetAllocator().StreamingAllocate( map, strIndex(), MEMBUCKET_RENDER, MEMUSERDATA_CLOTH );
//	void* ptr = map.Chunks[0].DestAddr;		
	USE_MEMBUCKET(MEMBUCKET_DEFAULT);
	return strStreamingEngine::GetAllocator().Allocate( size, RAGE_VERTEXBUFFER_ALIGNMENT, MEMTYPE_RESOURCE_VIRTUAL );
#if 0//__PFDRAW
	//	s_MemRequested += size;
	s_MemAllocated += strStreamingEngine::GetAllocator().GetSize(ptr);
	//	strStreamingEngine::GetAllocator().SetUserData(ptr,(u32)size);
#endif
//	return ptr;
}

void clothManager::FreeClothVB(void* ptr)
{
	Assert( ptr );
#if 0//__PFDRAW
	//	u32 size = strStreamingEngine::GetAllocator().GetUserData(ptr);
	//	s_MemRequested -= size;
	s_MemAllocated -= strStreamingEngine::GetAllocator().GetSize(ptr);
	//	Assert( s_MemAllocated > -1 );
	//	Assert( s_MemRequested > -1 );
#endif
	MEM_USE_USERDATA(MEMUSERDATA_CLOTH);
	USE_MEMBUCKET(MEMBUCKET_DEFAULT);
	strStreamingEngine::GetAllocator().Free( ptr );

//	strStreamingEngine::GetAllocator().StreamingFree( ptr );
}


#if __PFDRAW && !__RESOURCECOMPILER

void clothManager::ProfileDraw()
{
	static float xPos = 0.1f;
	static float yPos = 0.1f;
	static float fTextVerticalOffset = 0.012f;
	Vector2 mousePos(xPos, yPos);

	char buf[128];

	if( ms_VehicleFrame != NULL )
	{
		mousePos.y += fTextVerticalOffset;
		formatf(buf, " Vehicle picked 0x%x ", ms_VehicleFrame );
		grcDebugDraw::Text(mousePos, Color_white, buf);
	}

	if( ms_PedSkeleton )
	{
		mousePos.y += fTextVerticalOffset;
		formatf(buf, " Ped skeleton picked 0x%x ", ms_PedSkeleton );
		grcDebugDraw::Text(mousePos, Color_white, buf);

		if( s_VertexCount )
		{
			if( PFD_ClothVerts.WillDraw() )
			{
				if( PFD_ClothVerts.Begin() )
				{
					Assert( s_PoseOut );
					Assert( s_PoseColor );

					static float radius1 = 0.003f;
					Vec3V offset = ms_PedSkeleton->GetParentMtx()->GetCol3();

					for(int i = 0; i < s_VertexCount; ++i )
					{
						Vec3V v = s_PoseOut[i] + offset;
						grcColor( s_PoseColor[i] );
						grcDrawSphere( radius1, v );

						//char buff[16];
						//formatf( buff, "%d", i );
						//grcDrawLabelf( VEC3V_TO_VECTOR3(v), buff );
						
					}
					PFD_ClothVerts.End();
				}
			}			
		}
	}

	if( s_ShowStats )
	{		
		int activeClothCount = 0;

		mousePos.y += fTextVerticalOffset;
		formatf(buf, "Number of free cloth slots: %d       Number of used cloth slots: %d", m_FreeCount, m_ActiveCount );		
		grcDebugDraw::Text(mousePos, Color_white, buf);

#if CLOTH_PROFILE
		if( s_ShowStatsInstanceCloth )
		{
			Assert( m_TasksManager );

			mousePos.y += fTextVerticalOffset;
			formatf(buf, "Highest UPDATE THREAD stall (in last %d frames) from cloth instancing wait:   %f", FRAMES_TO_WAIT, m_TasksManager->GetWaitTime() );
			grcDebugDraw::Text(mousePos, Color_white, buf);
		}
		else
		{
			Assert( m_InstanceTasksManager );

			mousePos.y += fTextVerticalOffset;
			formatf(buf, "Highest UPDATE THREAD stall (in last %d frames) from verlet update wait:   %f", FRAMES_TO_WAIT, m_InstanceTasksManager->GetWaitTime() );
			grcDebugDraw::Text(mousePos, Color_white, buf);
		}
#endif

		for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
		{
			if( node->Data->GetType() == clothManager::Environment || node->Data->GetType() == clothManager::Vehicle )
			{
				activeClothCount += (int)(node->Data->IsActive());
			}
			
#if CLOTH_PROFILE
			if( !s_ShowStatsInstanceCloth )
			{
				if( !s_ShowStatsActiveCloth || node->Data->IsActive() )
				{
					mousePos.y += fTextVerticalOffset;
					formatf(buf, "%s:   %f", node->Data->GetClothController()->GetName(), node->Data->m_UpdateTime );
					grcDebugDraw::Text(mousePos, Color_white, buf);
				}
			}
			else if( node->Data->IsActive() )
			{
				mousePos.y += fTextVerticalOffset;
				formatf(buf, "%s:   %f", node->Data->GetClothController()->GetName(), node->Data->m_InstanceTime );
				grcDebugDraw::Text(mousePos, Color_white, buf);
			}
#endif // CLOTH_PROFILE
		}

		formatf(buf, "Number of active environment cloth pieces: %d", activeClothCount );
		grcDebugDraw::AddDebugOutput(buf);
		formatf(buf, "Number of active character cloth pieces: %d", m_ActiveVarCount );
		grcDebugDraw::AddDebugOutput(buf);
		formatf(buf, "Total environment cloth vertex buffers memory allocated: %dB", s_MemAllocated );
		grcDebugDraw::AddDebugOutput(buf);
		formatf(buf, "Total character cloth vertex buffers memory allocated: %dB", s_MemAllocatedChar );	
		grcDebugDraw::AddDebugOutput(buf);
	}


#if __PPU
	phVerletCloth::DebugDrawSPU();
#endif

	if (	PFD_ClothVerts.WillDraw() || 
		PFD_ClothEdges.WillDraw() || 
		PFD_ClothCustomEdges.WillDraw() || 
		PFD_PickVerts.WillDraw() || 
		PFD_PickEdges.WillDraw() || 
		PFD_VertexValues.WillDraw() || 
		PFD_ClothVertIndices.WillDraw() || 			
		PFD_ClothOriginalVertIndices.WillDraw() ||
		PFD_VerletBounds.WillDraw() || 
		PFD_CustomBounds.WillDraw() || 
		PFD_DebugRecords.WillDraw() )
	{
		for (atDNode<clothInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
		{
#if USE_CLOTHMANAGER_BANK
			node->Data->DebugUpdate( m_ViewPort );
#endif
			node->Data->ProfileDraw( m_ViewPort->GetCameraPosition() );
		}

#if RECORDING_VERTS
		if( PFD_DebugRecords.Begin(true) )
		{
			Assert( !g_EnableRecording );
			if( !g_EnableRecording )
			{	
				static u32 frameToDraw = 0;
				for( u32 i = 0; i < g_DbgRecordLines.m_Size; ++i )
				{
					recordLine& rec = g_DbgRecordLines.Get( i );
					if( rec.frame == frameToDraw )
						rec.Draw();
				}
				for( u32 i = 0; i < g_DbgRecordSpheres.m_Size; ++i )
				{
					recordSphere& rec = g_DbgRecordSpheres.Get( i );
					if( rec.frame == frameToDraw )
						rec.Draw();
				}
				for( u32 i = 0; i < g_DbgRecordTriangles.m_Size; ++i )
				{
					recordTriangle& rec = g_DbgRecordTriangles.Get( i );
					if( rec.frame == frameToDraw )
						rec.Draw();
				}
				for( u32 i = 0; i < g_DbgRecordCapsules.m_Size; ++i )
				{
					recordCapsule& rec = g_DbgRecordCapsules.Get( i );
					if( rec.frame == frameToDraw )
						rec.Draw();
				}
			}

			static float _x = 0.05f;
			static float _y = 0.3f;
			static float _yOffset  = 0.015f;
			const int queueSize = g_DbgRecordCustomClothEvents.m_Size;
			for( int idx = 0; idx < queueSize; ++idx )
			{
				recordCustomClothEvent& rec = g_DbgRecordCustomClothEvents.Get( idx );
				rec.Draw(_x, _y + _yOffset * idx);
				//				Assert( rec.m_Frames > -1 );
				//				if( rec.m_Frames == 0 )
				//				{
				//					g_DbgRecordCustomClothEvents.PopFirst();
				//				}
			}

			PFD_DebugRecords.End();
		}


#endif //RECORDING_VERTS

	}

#if USE_CLOTH_GIZMO
	g_ClothGizmo.Update(m_ViewPort);
	g_ClothGizmo.Draw();
#endif

}
#endif // __PFDRAW


// --------------- clothInstance --------------- 

void clothInstance::Init(rage::clothController* inst, int verletType)
{
	m_Type = verletType;

	Assert( inst );
	m_ClothController = inst;
	m_ClothController->SetClothInstance( this );

	if( m_Type == clothManager::Character )
	{
		// TODO: do something with character cloth 

	}
	else if( m_Type == clothManager::Environment || m_Type == clothManager::Vehicle )
	{
		// TODO: do something with env cloth 

	}
	m_UserData1 = 0;
	m_UserData2 = 0;
	m_UserData3 = 0;
}

void clothInstance::Shutdown()
{
	m_Type				= clothManager::Unknown;
	m_IsActive			= false;
	m_IsReady			= false;
	m_Skeleton			= NULL;
	m_LastPosition		= Vec3V(V_ZERO);
	m_FramesToRotate	= 0;
	m_RotationSign		= ScalarV(V_ONE);
	if( m_ClothController )
		m_ClothController->Shutdown();
	m_ClothController	= NULL;

#if RSG_PC
	m_ElapsedTimeSinceLastUpdate = 0.0f;
#endif

#if USE_CLOTHMANAGER_BANK
	m_RAGGroup = NULL;
	m_VirtualBoundGroup = NULL;
	m_TogglesGroup = NULL;
	m_DynamicPinUnpinWidget = NULL;
	m_DynamicPickVertsWidget = NULL;
	m_DynamicPickPinVertsWidget = NULL;
	m_SpuDebugIdx = -1;
#if CLOTH_PROFILE
	m_LastUpdateTime = 0.0f;
	m_UpdateTime = 0.0f;
	m_InstanceTime = 0.0f;
#endif
	m_SpuDebug = false;
	m_DebugDraw = false;
	m_DebugDrawPinRadiuses = false;
	m_DebugDrawAirResistance = false;
	m_DebugDrawEdgeInfo = false;
	m_DebugDragVerts = false;
	m_DebugPickVerts = false;
	m_DebugPickPinVerts = false;
	m_DebugPickEdges = false;
	m_DebugPickEdgesByVerts = false;
	m_DebugPickCustomEdges = false;
	m_DebugPickInstanceCustomEdges = false;
	m_DebugUnPinVerts = false;
	m_DebugPinVerts = false;
	m_DebugDeleteVerts = false;
	m_DebugSelectedVertex = 0;
	m_DebugCurrentPinRadiusSet = 0;
	m_DebugCurrentPose = 0;
	m_DebugDrawCustomBound = false;
	m_DebugPreviewEdgeSplit = false;
	m_RestInterface = false;
#endif
}

int clothInstance::GetType() const 
{ 
	return (m_Type & ~clothManager::ReplayControlled); 
}

void clothInstance::SetTypeModifiers(int modifier)
{
	clothAssertf(modifier == clothManager::ReplayControlled, "clothInstance::SetTypeModifiers()...Illegal modifier");
	m_Type |= modifier;
}

void clothInstance::ClearTypeModifiers(int modifier)
{
	clothAssertf(modifier == clothManager::ReplayControlled, "clothInstance::SetTypeModifiers()...Illegal modifier");
	m_Type &= ~modifier;
}

void clothInstance::SetSkeletonOnly(const crSkeleton* pSkeleton)
{
	Assert( !m_Skeleton );
	m_Skeleton = pSkeleton;
}

void clothInstance::SetSkeleton(const crSkeleton* pSkeleton)
{
	Assert( !m_Skeleton );
	m_Skeleton = pSkeleton;

#if USE_BONEID
	characterClothController* cccontroller = (characterClothController*)m_ClothController;
	Assert( cccontroller );

	characterCloth* pCCloth = (characterCloth*)cccontroller->GetOwner();
	Assert( pCCloth );

// NOTE: bones used in simulation
	atArray<s32>& boneIndexMap = cccontroller->GetBoneIndexMapRef();	
	const atArray<s32>& boneIDMap = cccontroller->GetBoneIDMapRef();
	Assert( boneIndexMap.GetCount() == boneIDMap.GetCount() );
	for( int i = 0; i < boneIDMap.GetCount(); ++i )
	{
		const u16 boneId = (u16)boneIDMap[i];
		const crSkeletonData& skelData = pSkeleton->GetSkeletonData();
		int boneIdx;
 #if __ASSERT
		bool res = 
 #endif
			skelData.ConvertBoneIdToIndex( boneId, boneIdx );
 #if __ASSERT
		AssertMsg( res, "Can't find boneIndex ?!" );
 #endif
		boneIndexMap[i] = boneIdx;
	}

	
// NOTE: bones used in collision
	atArray<int>& boneIndices = pCCloth->GetBoneIndexMapRef();
	const atArray<int>& boneIDs = pCCloth->GetBoneIDMapRef();
	Assert( boneIndices.GetCount() == boneIDs.GetCount() );
	boneIndices.Resize( boneIDs.GetCount() );
	for( int i = 0; i < boneIDs.GetCount(); ++i )
	{
		const u16 boneId = (u16)boneIDs[i];
		const crSkeletonData& skelData = pSkeleton->GetSkeletonData();
		int boneIdx;
 #if __ASSERT
		bool res = 
 #endif
			skelData.ConvertBoneIdToIndex( boneId, boneIdx );
 #if __ASSERT
		AssertMsg( res, "Can't find boneIndex ?!" );
 #endif
		boneIndices[i] = boneIdx;
	}
#endif // USE_BONEID
}


#if __PFDRAW
void clothInstance::ProfileDraw(Vec3V_In camPos)
{
 #if USE_CLOTHMANAGER_BANK
	Vector3 offset(Vector3::ZeroType);
	phVerletCloth* pVerletCloth = GetVerletClothAndOffset(offset);
	if( pVerletCloth )
	{
		if( m_DebugPose.GetCount() )
		{
			pVerletCloth->SetFlag(phVerletCloth::FLAG_ENABLE_DEBUG_DRAW, m_DebugDraw);
			const Vec3V* RESTRICT pVerts = (const Vec3V* RESTRICT)GetClothVertices(pVerletCloth);
			Assert( pVerts );
			DrawClothWireframe( pVerts, *pVerletCloth, m_ClothController, offset, camPos.GetIntrin128(), false, pClothManager->ms_EnableAlpha, pClothManager->ms_FlipAlpha, pClothManager->ms_DebugSphereRadius, pClothManager->ms_DebugVertsColor );
		}
	}
#else
	(void)camPos;
 #endif // USE_CLOTHMANAGER_BANK
}
#endif // __PFDRAW

#if USE_CLOTHMANAGER_BANK

phVerletCloth* clothInstance::GetVerletClothAndOffset(Vector3& offset)
{
	phVerletCloth* pCloth = NULL;

	if (m_Type == clothManager::Environment || m_Type == clothManager::Vehicle)
	{
		environmentCloth* pEnvCloth = (environmentCloth*)m_ClothController->GetOwner();
		Assert( pEnvCloth );
		pCloth = pEnvCloth->GetCloth();
		Assert( pEnvCloth->GetBehavior() );

		offset = VEC3V_TO_VECTOR3(pEnvCloth->GetOffset());
		offset = offset + s_DrawOffset;
	}
	else if (m_Type == clothManager::Character )
	{
		pCloth = m_ClothController->GetCloth(0);
		characterCloth* pCharCloth = (characterCloth*)m_ClothController->GetOwner();
		Assert( pCharCloth );
		offset = VEC3V_TO_VECTOR3( pCharCloth->GetParentLastPos() ) + s_DrawOffset;

		// TODO: do something else
		// bounds and collision stuff ?!?
	}

	return pCloth;
}

bool clothInstance::IsDebugPoseActive() const
{
	return (m_DebugDraw || m_DebugDrawPinRadiuses || m_DebugDrawAirResistance || m_DebugDrawEdgeInfo || m_DebugDragVerts || m_DebugPickVerts ||
			m_DebugPickPinVerts || m_DebugPickEdges || m_DebugPickEdgesByVerts || m_DebugPickCustomEdges || m_DebugPickInstanceCustomEdges || m_DebugUnPinVerts || m_DebugPinVerts || m_DebugDrawCustomBound || m_DebugPreviewEdgeSplit);
}

const Vector3* clothInstance::GetClothVertices(const phVerletCloth* verletCloth) const
{
	Assert( verletCloth );
	return (Vector3*)( IsDebugPoseActive() ? m_DebugPose.GetElements(): verletCloth->GetClothData().GetVertexPointer());
}

void clothInstance::DrawOriginalVertexIndices(const Vector3& PF_DRAW_ONLY(camPos), const u16* RESTRICT PF_DRAW_ONLY(vertexValues), const char* PF_DRAW_ONLY(stringFormat) )
{
#if __PFDRAW
	if( PFD_ClothOriginalVertIndices.Begin(true) )
	{
		Vector3 offset(Vector3::ZeroType);
		if( phVerletCloth* verletCloth = GetVerletClothAndOffset(offset) )
		{
			Assert( vertexValues );
			const int numVerts = m_ClothController->GetMeshVertCount(0);
			const Vector3* RESTRICT verts = GetClothVertices(verletCloth);
			Assert( verts );

			grcColor(Color_white);
			if( verletCloth->GetClothData().GetNormalsCount() )
			{
				const Vector3* RESTRICT normals = (Vector3*)verletCloth->GetClothData().GetNormalsPointer();				
				Assert( normals );
				for( int i = 0; i < numVerts; ++i)
				{
					Vector3 v = verts[ vertexValues[i] ]+offset;
					char buf[16];
					formatf( buf, stringFormat, i );
					Vector3 vT = camPos - v;
					vT.Normalize();
					if( vT.Dot(normals[i]) > 0.0f )
						grcDrawLabelf( v, buf );
				}
			}
			else
			{
				for( int i = 0; i < numVerts; ++i)
				{
					Vector3 v = verts[ vertexValues[i] ]+offset;
					char buf[16];
					formatf( buf, stringFormat, i );
					grcDrawLabelf( v, buf );
				}
			}
		}
		PFD_ClothOriginalVertIndices.End();
	}
#endif
}


template<class T>
void clothInstance::DrawPerVertexValues(const Vector3& PF_DRAW_ONLY(camPos), const T* RESTRICT PF_DRAW_ONLY(vertexValues), const char* PF_DRAW_ONLY(stringFormat), const Color32* RESTRICT PF_DRAW_ONLY(colorValues) )
{
	// NOTE: should be used only by character cloth
#if __PFDRAW
	if( PFD_VertexValues.Begin(true) )
	{
		Vector3 offset(Vector3::ZeroType);
		if( phVerletCloth* pCloth = GetVerletClothAndOffset(offset) )
		{
			Assert( colorValues );
			Assert( vertexValues );
			const int numVerts = m_ClothController->GetMeshVertCount(0);

			const Vector3* RESTRICT verts = GetClothVertices(pCloth);
			Assert( verts );
#if NO_PIN_VERTS_IN_VERLET
			int numPinnedVerts = pCloth->GetClothData().GetNumPinVerts();
#else
			int numPinnedVerts = pCloth->GetPinCount();
#endif
			if( pCloth->GetClothData().GetNormalsCount() )
			{
				const Vector3* RESTRICT normals = (Vector3*)pCloth->GetClothData().GetNormalsPointer();
				Assert( normals );
				for( int i = numPinnedVerts; i < numVerts; ++i)
				{				
					grcColor( colorValues[i] );

					Vector3 v = verts[i]+offset;
					Vector3 vT = camPos - v;					
					vT.Normalize();				
					float vTdot = vT.Dot( normals[i] );					
					if( vTdot > 0.0f )
					{
						char buf[16];
						formatf( buf, stringFormat, vertexValues[i] );
						grcDrawLabelf( v, buf );
					}				
				}
			}
			else
			{
				for( int i = numPinnedVerts; i < numVerts; ++i)
				{				
					grcColor( colorValues[i] );
					char buf[16];
					formatf( buf, stringFormat, vertexValues[i] );

					Vector3 v = verts[i]+offset;
					grcDrawLabelf( v, buf );
				}
			}

		}
		PFD_VertexValues.End();
	}
#endif
}

void clothInstance::DrawBound( const phBound* pCustomBound, Mat34V* 
#if __PFDRAW
	pBoundPose,
#endif
	bool isGizmoAttached
	)
{
	if( !pCustomBound )
		return;

#if __PFDRAW
	// TODO: func is not complete
	switch( pCustomBound->GetType() )
	{
	case phBound::COMPOSITE:
		{
			const phBoundComposite* pCompositeBound =  static_cast<const phBoundComposite*>(pCustomBound);
			Mat34V boundPartPose;
			for( int i = 0; i < pCompositeBound->GetNumBounds(); ++i )
			{
				const phBound* compositePart = pCompositeBound->GetBound(i);
				if( compositePart )
				{
					Transform( boundPartPose, *pBoundPose, pCompositeBound->GetCurrentMatrix(i) );
					isGizmoAttached = (&pCompositeBound->GetCurrentMatrix(i) == g_ClothGizmo.GetMatrix()) ? true: false;
					DrawBound( compositePart, &boundPartPose, isGizmoAttached );
				}				
			}	
			break;
		}
	case phBound::BOX:
		{
			if( PFD_CustomBounds.Begin(true) )
			{
				// TODO: this code is dup on  void phVerletCloth::DetectAndEnforceOneBox
				// clean it up
				Vec3V vBoxMin = pCustomBound->GetBoundingBoxMin();
				Vec3V vBoxMax = pCustomBound->GetBoundingBoxMax();				
				Vec3V vCentrePos = pCustomBound->GetCentroidOffset();

				ScalarV vMinX = SplatX(vBoxMin);
				ScalarV vMinY = SplatY(vBoxMin);
				ScalarV vMinZ = SplatZ(vBoxMin);
				ScalarV vMaxX = SplatX(vBoxMax);
				ScalarV vMaxY = SplatY(vBoxMax);
				ScalarV vMaxZ = SplatZ(vBoxMax);

				Vec3V vRight	= Vec3V(V_X_AXIS_WZERO);
				Vec3V vForward	= Vec3V(V_Y_AXIS_WZERO);
				Vec3V vUp		= Vec3V(V_Z_AXIS_WZERO);

				Vec3V vRMin = Scale(vRight,vMinX);
				Vec3V vFMin = Scale(vForward,vMinY);
				Vec3V vUMin = Scale(vUp,vMinZ);
				Vec3V vRMax = Scale(vRight,vMaxX);				
				Vec3V vFMax = Scale(vForward,vMaxY);
				Vec3V vUMax = Scale(vUp,vMaxZ);				

				Vec3V vBoxVerts[8];
				vBoxVerts[0] = Add(vCentrePos, Add(vRMin, Add(vFMin,vUMin)));
				vBoxVerts[1] = Add(vCentrePos, Add(vRMax, Add(vFMin,vUMin)));
				vBoxVerts[2] = Add(vCentrePos, Add(vRMax, Add(vFMax,vUMin)));
				vBoxVerts[3] = Add(vCentrePos, Add(vRMin, Add(vFMax,vUMin)));
				vBoxVerts[4] = Add(vCentrePos, Add(vRMin, Add(vFMin,vUMax)));
				vBoxVerts[5] = Add(vCentrePos, Add(vRMax, Add(vFMin,vUMax)));
				vBoxVerts[6] = Add(vCentrePos, Add(vRMax, Add(vFMax,vUMax)));
				vBoxVerts[7] = Add(vCentrePos, Add(vRMin, Add(vFMax,vUMax)));

				int boxTris[12][3] = {	{0, 2, 1}, 
				{0, 3, 2},
				{1, 6, 5}, 
				{1, 2, 6},
				{4, 3, 0}, 
				{4, 7, 3},
				{4, 1, 5}, 
				{4, 0, 1},
				{3, 6, 2}, 
				{3, 7, 6},
				{7, 5, 6}, 
				{7, 4, 5}};	
				///////////
				for (int i=0; i<12; i++)
				{
					Vec3V vVtx0 = Transform(*pBoundPose,vBoxVerts[boxTris[i][0]]);
					Vec3V vVtx1 = Transform(*pBoundPose,vBoxVerts[boxTris[i][1]]);
					Vec3V vVtx2 = Transform(*pBoundPose,vBoxVerts[boxTris[i][2]]);

					grcDrawLine( vVtx0, vVtx1, Color_white );
					grcDrawLine( vVtx0, vVtx2, Color_white );
					grcDrawLine( vVtx1, vVtx2, Color_white );
				}
				PFD_CustomBounds.End();
			}
			break;
		}
	case phBound::CAPSULE:
		{
			if( PFD_CustomBounds.Begin(true) )
			{
				Assert( pCustomBound );
				const phBoundCapsule* capsuleBound = (const phBoundCapsule*)pCustomBound;
				/// ScalarV boundRadius = capsuleBound->GetRadiusAroundCentroidV();
				/// ScalarV combinedRadiusSquared = Scale(boundRadius,boundRadius);

				Mat34V boundCapsuleMat = *pBoundPose;			// rotate or translate the boundCapsule by some custom offset if needed ... shouldn't be needed

#if USE_CLOTH_GIZMO
				if( isGizmoAttached )
				{
					Mat34V gizmoMat(V_IDENTITY);
					Vec3V translation;
					g_ClothGizmo.GetTransform(gizmoMat, translation);
					Transform( boundCapsuleMat, boundCapsuleMat, gizmoMat );
					boundCapsuleMat.SetCol3( rage::Add( boundCapsuleMat.GetCol3(), translation) );
				}
#endif

				/// Vec3V boundCentroid = capsuleBound->GetWorldCentroid(boundCapsuleMat);	
				/// Vec3V unitAxis = boundCapsuleMat.GetCol1();

				ScalarV capsuleLength = capsuleBound->GetLengthV();
				ScalarV capsuleRadius = capsuleBound->GetRadiusV();
				/// Vec3V end0 = SubtractScaled(boundCentroid,unitAxis,Scale(ScalarV(V_HALF),capsuleLength));
				/// ScalarV combinedCapsuleRadius2 = Scale(capsuleRadius,capsuleRadius);

				grcDrawCapsule( capsuleLength.Getf(), capsuleRadius.Getf(), MAT34V_TO_MATRIX34(boundCapsuleMat), 8, false
#if USE_CAPSULE_EXTRA_EXTENTS
					, capsuleBound->GetHalfHeight() 
#endif
					);

				Vec3V boundPos = boundCapsuleMat.GetCol3();
				char buf[256];
				formatf( buf, "Position: %f %f %f", boundPos.GetXf(), boundPos.GetYf(), boundPos.GetZf() );
				grcDrawLabelf( VEC3V_TO_VECTOR3(boundPos), buf );

				QuatV qV = QuatVFromMat34V( boundCapsuleMat );
				float rowStep = 0.08f;
				boundPos.SetZf( boundPos.GetZf() - rowStep );
				formatf( buf, "Quaternion: %f %f %f %f", qV.GetXf(), qV.GetYf(), qV.GetZf(), qV.GetWf() );
				grcDrawLabelf( VEC3V_TO_VECTOR3(boundPos), buf );

				Vec3V euler = QuatVToEulersXYZ(qV);
				boundPos.SetZf( boundPos.GetZf() - rowStep );
				formatf( buf, "Euler: %f %f %f ", euler.GetXf(), euler.GetYf(), euler.GetZf() );
				grcDrawLabelf( VEC3V_TO_VECTOR3(boundPos), buf );

				PFD_CustomBounds.End();
			}
			break;
		}
	case phBound::TRIANGLE:
		break;
	case phBound::PLANE:
		{	
			if( PFD_CustomBounds.Begin(true) )
			{
				const phBoundPlane* pPlane = (const phBoundPlane*)(pCustomBound);

				Vec3V n = pPlane->GetNormal();
				Vec3V planePos = pPlane->GetPosition();

				Vec3V upV(V_Z_AXIS_WZERO);
				Vec3V rightV = Normalize( Cross(n, upV) );
				upV = Normalize( Cross(n, rightV) );

				Mat34V planeMat;
				planeMat.SetCol0( n );
				planeMat.SetCol1( rightV );
				planeMat.SetCol2( upV );
				planeMat.SetCol3( planePos );

				Mat34V boundMat = *pBoundPose;
#if USE_CLOTH_GIZMO
				if( isGizmoAttached )
				{
					Mat34V gizmoMat(V_IDENTITY);
					Vec3V translation;
					g_ClothGizmo.GetTransform(gizmoMat, translation);
					Transform( boundMat, boundMat, gizmoMat );
					boundMat.SetCol3( rage::Add( boundMat.GetCol3(), translation) );
				}
#endif

				Transform( planeMat, planeMat, boundMat );

				n		= planeMat.GetCol0();
				rightV	= planeMat.GetCol1();
				upV		= planeMat.GetCol2();
				planePos= planeMat.GetCol3();

				static float planeScaleRightF = 3.0f;
				static float planeScaleUpF = 3.0f;
				ScalarV planeScaleRightV = ScalarVFromF32(planeScaleRightF);
				ScalarV planeScaleUpV = ScalarVFromF32(planeScaleUpF);

				// draw two sided polygon
				const int totalNumVerts = 10;
				Vector3 pt[totalNumVerts];
				pt[0] = VEC3V_TO_VECTOR3(planePos);
				pt[1] = VEC3V_TO_VECTOR3( AddScaled(planePos, upV, planeScaleUpV) );
				pt[2] = VEC3V_TO_VECTOR3( AddScaled(planePos, Negate(rightV), planeScaleRightV ) );
				pt[3] = VEC3V_TO_VECTOR3( AddScaled(planePos, Negate(upV), planeScaleUpV ) );
				pt[4] = VEC3V_TO_VECTOR3( AddScaled(planePos, rightV, planeScaleRightV) );
				pt[5] = VEC3V_TO_VECTOR3( AddScaled(planePos, upV, planeScaleUpV) );
				pt[6] = VEC3V_TO_VECTOR3( AddScaled(planePos, rightV, planeScaleRightV) );
				pt[7] = VEC3V_TO_VECTOR3( AddScaled(planePos, Negate(upV), planeScaleUpV ) );
				pt[8] = VEC3V_TO_VECTOR3( AddScaled(planePos, Negate(rightV), planeScaleRightV ) );
				pt[9] = VEC3V_TO_VECTOR3( AddScaled(planePos, upV, planeScaleUpV) );

				Color32 cWhite(0.0f, 0.0f, 1.0f, 1.0f);
				Color32 cWhiteish(0.0f, 0.0f, 1.0f, 1.0f);

				Color32 colr[totalNumVerts];
				colr[0] = cWhite;
				colr[1] = cWhiteish;
				colr[2] = cWhiteish;
				colr[3] = cWhiteish;
				colr[4] = cWhiteish;
				colr[5] = cWhiteish;
				colr[6] = cWhiteish;
				colr[7] = cWhiteish;
				colr[8] = cWhiteish;
				colr[9] = cWhiteish;				

				grcDrawPolygon(pt, totalNumVerts, NULL, true, colr);
				grcDrawLine(pt[0], pt[0] + VEC3V_TO_VECTOR3(n), Color_red);

				char buf[256];
				formatf( buf, "Position: %f %f %f", planePos.GetXf(), planePos.GetYf(), planePos.GetZf() );
				grcDrawLabelf( VEC3V_TO_VECTOR3(planePos), buf );

				float rowStep = 0.08f;
				planePos.SetZf( planePos.GetZf() - rowStep );
				formatf( buf, "Normal: %f %f %f", n.GetXf(), n.GetYf(), n.GetZf() );
				grcDrawLabelf( VEC3V_TO_VECTOR3(planePos), buf );

				PFD_CustomBounds.End();
			}
			break;
		}

	case phBound::GEOMETRY:
	case phBound::SPHERE:
		USE_TAPERED_CAPSULE_ONLY(case phBound::TAPERED_CAPSULE:)
			USE_GEOMETRY_CURVED_ONLY(case phBound::GEOMETRY_CURVED:)
			USE_GRIDS_ONLY(case phBound::GRID:)
			USE_RIBBONS_ONLY(case phBound::RIBBON:)
			case phBound::BVH:
				USE_SURFACES_ONLY(case phBound::SURFACE:)
				default:
					break;		// do something
	}
#endif // #if __PFDRAW
}

void clothInstance::DrawCustomBound()
{
	Assert(m_ClothController);
	Vector3 offset(Vector3::ZeroType);
	if( phVerletCloth* pVerletCloth = GetVerletClothAndOffset(offset) )
	{
		Mat34V iMat(V_IDENTITY);
		if (m_Type == clothManager::Character )
		{
			//			iMat.SetCol3( VECTOR3_TO_VEC3V(offset) );
			//			characterCloth* cloth = (characterCloth*)m_ClothController->GetOwner();
			//			Assert( cloth );
			//			const phBoundComposite* customBound = cloth->GetBoundComposite();
			//			if( customBound )
			//				DrawBound( customBound, iMat );

			if( pVerletCloth->m_VirtualBound.ptr )
				DrawBound( pVerletCloth->m_VirtualBound, &iMat, false );
		}
		else
		{
			DrawBound( pVerletCloth->GetCustomBound(), &iMat, false );
		}		
	}	
}

void clothInstance::DebugUpdate( const grcViewport* pViewport )
{
	Vector3 offset(Vector3::ZeroType);
	if( phVerletCloth* pVerletCloth = GetVerletClothAndOffset(offset) )
	{
#if ENABLE_SPU_DEBUG_DRAW
		if( m_SpuDebug )
		{
			m_SpuDebugIdx = s_VerletSpuDebugCounter++;
		}
		else
#endif
		{
			m_SpuDebugIdx = -1;
		}

		if( m_DebugDragVerts )
		{
			fwClothDebug::DragVerts( Vector3(Vector3::ZeroType), m_ClothController, pViewport );
		}
		if( m_DebugPickVerts || m_DebugPickPinVerts )
		{
#if NO_PIN_VERTS_IN_VERLET
			int numPinnedVerts = pVerletCloth->GetClothData().GetNumPinVerts();
#else
			int numPinnedVerts = pVerletCloth->GetPinCount();
#endif
			const int numPickedVerts = PickVerts( offset, GetClothVertices(pVerletCloth), pVerletCloth->GetNumVertices(), numPinnedVerts, pViewport, m_DebugPickPinVerts );
			if( numPickedVerts > 0 )
				g_LastPickedClothController = m_ClothController;
		}
		if( m_DebugPreviewEdgeSplit )
		{
			static int edgeIdx0 = -1;
			static int edgeIdx1 = -1;
			static int vtxIdx = -1;
			if(edgeIdx0 > -1 && edgeIdx1 > -1 && vtxIdx > -1 )
			{
				PreviewEdgesSplit(vtxIdx, edgeIdx0, edgeIdx1);
			}
		}
		if( m_DebugPickEdgesByVerts )
		{
#if NO_PIN_VERTS_IN_VERLET
			int numPinnedVerts = pVerletCloth->GetClothData().GetNumPinVerts();
#else
			int numPinnedVerts = pVerletCloth->GetPinCount();
#endif
			const int numPickedVerts = PickEdgesByVerts( offset, GetClothVertices(pVerletCloth), pVerletCloth->GetNumVertices(), numPinnedVerts, pViewport, m_DebugPickPinVerts );
			if( numPickedVerts > 0 )
				g_LastPickedClothController = m_ClothController;
		}
		if( m_DebugPickEdges )
		{
			PickEdges( offset, pVerletCloth, pViewport, pVerletCloth->GetEdgeList() );
			if( m_DebugDrawEdgeInfo )
				DrawEdgeSelectionInfo( pVerletCloth->GetEdgeList() );
		}
		if( m_DebugPickCustomEdges )
		{
			PickEdges( offset, pVerletCloth, pViewport, pVerletCloth->GetCustomEdgeList() );
			if( m_DebugDrawEdgeInfo )
				DrawEdgeSelectionInfo( pVerletCloth->GetCustomEdgeList() );
		}
		if( m_DebugPickInstanceCustomEdges )
		{
			PickEdges( offset, pVerletCloth, pViewport, pVerletCloth->GetInstanceCustomEdgeList() );
			if( m_DebugDrawEdgeInfo )
				DrawEdgeSelectionInfo( pVerletCloth->GetInstanceCustomEdgeList() );
		}
		if( m_DebugUnPinVerts || m_DebugPinVerts )
		{
			PinUnPinVerts( offset, pVerletCloth, pViewport, m_DebugPinVerts );
		}
		if( m_DebugDeleteVerts )
		{
			// TODO: do something
		}
		if( m_DebugDrawCustomBound )
		{
			DrawCustomBound();
		}

		if( m_DebugDrawPinRadiuses )
		{
			// TODO: character cloth doesn't support lods yet
			int lodIndex = 0;
			const int numVerts = pVerletCloth->GetNumVertices();
			const char* memPtr = ((const char*)m_ClothController->GetBridge()->GetVertexBlends(lodIndex));
			Assert( memPtr );
			const float* pVertexBlendsSet0 = (const float*)( memPtr );
			const float* pVertexBlendsSet = (const float*)( memPtr + (m_DebugCurrentPinRadiusSet*numVerts*sizeof(float)) );
			Assert( pVertexBlendsSet0 );
			Assert( pVertexBlendsSet );

			sysMemStartTemp();
			Color32* colorValues = Alloca(Color32, numVerts);
			Assert( colorValues );
			sysMemEndTemp();
			for( int i = 0 ; i < numVerts; ++i)
			{
				colorValues[i] = (m_DebugCurrentPinRadiusSet == 0) ? Color_white: (pVertexBlendsSet0[i] != pVertexBlendsSet[i] ? Color_red: Color_white);
			}			

			DrawPerVertexValues<float>( pViewport->GetCameraPosition().GetIntrin128(), pVertexBlendsSet, "%4.2f", colorValues );
		}
		if( m_DebugDraw )
		{
			DrawOriginalVertexIndices( pViewport->GetCameraPosition().GetIntrin128(), m_ClothController->GetBridge()->GetClothDisplayMap(0), "%d" );
		}
		if( m_DebugDrawAirResistance )
		{
			const int numVerts = pVerletCloth->GetNumVertices();
			sysMemStartTemp();
			Color32* colorValues = Alloca(Color32, numVerts);
			Assert( colorValues );
			sysMemEndTemp();
			for( int i = 0 ; i < numVerts; ++i)
			{
				colorValues[i] = Color_white;
			}

			DrawPerVertexValues<float>( pViewport->GetCameraPosition().GetIntrin128(), m_ClothController->GetBridge()->GetVertexWeights(0), "%6.4f", colorValues );
		}
	}
}

#if CLOTH_INSTANCE_FROM_DATA

void clothInstance::DetachFromVehicle()
{
	Assert( m_ClothController );		
	environmentCloth* pEnvCloth = (environmentCloth*)m_ClothController->GetOwner();
	Assert( pEnvCloth );
	phClothVerletBehavior* pClothBehavior = pEnvCloth->GetBehavior();	
	Assert( pClothBehavior );
	if( pClothBehavior->IsMotionSeparated() )
	{
		Mat34V temp_Mat(V_IDENTITY);
// TODO: check if offset is not 0,0,0	??
		Vec3V instOffset = VECTOR3_TO_VEC3V(s_InstanceOffset);
		Vec3V newPos = rage::Add(temp_Mat.GetCol3(), instOffset);
		temp_Mat.SetCol3( newPos );
		SetInstanceMat(temp_Mat);

		pEnvCloth->SetAttachedToFrame( RCC_MATRIX34(GetInstanceMat()) );
		pClothBehavior->SetActivateOnHit( false );

		m_Type = clothManager::Environment;
	}
}

void clothInstance::AttachToVehicle()
{
	if(clothManager::ms_VehicleFrame)
	{
		Assert( m_ClothController );		
		environmentCloth* pEnvCloth = (environmentCloth*)m_ClothController->GetOwner();
		Assert( pEnvCloth );
		pEnvCloth->SetAttachedToFrame( RCC_MATRIX34(*clothManager::ms_VehicleFrame) );

		phClothVerletBehavior* pClothBehavior = pEnvCloth->GetBehavior();	
		Assert( pClothBehavior );
		pClothBehavior->SetActivateOnHit( true );

		m_Type = clothManager::Vehicle;

		clothManager::ms_VehicleFrame = NULL;
		clothManager::ms_PedSkeleton = NULL;
		clothManager::ms_pPedDrawable = NULL;
		clothManager::ms_PedClothVarData = NULL;
	}
}

void clothInstance::LoadTopology()
{
	char buff[256];
	formatf( buff, "%s%s%s", "common:/data/cloth/", (char*)m_ClothController->GetName(), "_topology"  );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);

	clothDebugf1("Loaded cloth topology from file: %s", buff);
}

void clothInstance::SaveTopology()
{	
	environmentCloth* pEnvCloth = (environmentCloth*)m_ClothController->GetOwner();
	Assert( pEnvCloth );

	const grmModel* pModel = pEnvCloth->GetDrawable()->GetLodGroup().GetLod( 0 ).GetModel( 0 );
	Assert(pModel);
	grmGeometryQB& geometry = (grmGeometryQB&)pModel->GetGeometry( 0 );

	const u16* RESTRICT pClothDisplayMap = m_ClothController->GetClothDisplayMap( 0 );
	Assert( pClothDisplayMap );

	grcIndexBuffer* RESTRICT pIndexBuffer = geometry.GetIndexBuffer();
	Assert( pIndexBuffer );
	const int nIndices = pIndexBuffer->GetIndexCount();

#if USE_CLOTHMANAGER_BANK
	sysMemStartTemp();
	m_Indices.Resize( nIndices );
	sysMemEndTemp();
#endif

	const u16* RESTRICT pIndexPtr = pIndexBuffer->LockRO();
	WIN32PC_ONLY(if (pIndexPtr == NULL) return;)
	grcAssertf(pIndexPtr, "Couldn't get locked index buffer pointer");

	for( int i = 0; i < nIndices; i += 3 )
	{
		const u16 index0 = pIndexPtr[i];
		const u16 index1 = pIndexPtr[i+1];
		const u16 index2 = pIndexPtr[i+2];

		const int i0 = pClothDisplayMap[index0];
		const int i1 = pClothDisplayMap[index1];
		const int i2 = pClothDisplayMap[index2];

		m_Indices[i] = i0;
		m_Indices[i+1] = i1;
		m_Indices[i+2] = i2;
	}

	geometry.GetIndexBuffer()->UnlockRO();

#if __BANK
	char buff[256];
	formatf( buff, "%s%s%s", "common:/data/cloth/", (char*)m_ClothController->GetName(), "_topology" );
	AssertVerify(PARSER.SaveObject( buff, "xml", this, parManager::XML));

	clothDebugf1("Saved cloth topology to file: %s", buff);
#endif
}

#endif // CLOTH_INSTANCE_FROM_DATA


void clothInstance::PinUnPinVerts( const Vector3& offset, phVerletCloth* cloth, const grcViewport* pViewport, bool pin )
{
#if NO_PIN_VERTS_IN_VERLET
	int numPinnedVerts = cloth->GetClothData().GetNumPinVerts();
#else
	int numPinnedVerts = cloth->GetPinCount();
#endif
	const int numPickedVerts = PickVerts(offset, GetClothVertices(cloth), cloth->GetNumVertices(), numPinnedVerts, pViewport, pin);
	if( numPickedVerts > 0 )
		g_LastPickedClothController = m_ClothController;

	if( fwClothDebug::sm_LastSelectedVertsCount )
	{
		Assert( m_ClothController );
		atFunctor4< void, int, int, int, bool > funcSwap;
		funcSwap.Reset< clothController, &clothController::OnClothVertexSwap >( m_ClothController );

		if( ioMapper::DebugKeyPressed( fwClothDebug::GetConfirmSelectionKey() ) )
		{				
			if( m_Type == clothManager::Environment || m_Type == clothManager::Vehicle )
			{
				Assert( cloth );
				environmentCloth* pEnvCloth = (environmentCloth*)m_ClothController->GetOwner();
				Assert( pEnvCloth );
				if( pin )
				{					
					cloth->DynamicPinVerts( pEnvCloth->GetClothFrame(), m_ClothController->GetLOD(), fwClothDebug::sm_LastSelectedVertsCount, fwClothDebug::sm_LastSelectedVerts, &funcSwap );
				}
				else
				{
					sysMemStartDebug();
			// TODO: ... unlock all verts ? ... i.e. allow all verts to be pinned unpinned ?
					atBitSet tempStateList;
					tempStateList.Init( cloth->GetNumVertices() );
					tempStateList.Reset();
					for(int i = 0; i < cloth->GetNumVertices(); ++i )
						tempStateList.Set( i, true );

					cloth->DynamicUnPinVerts( pEnvCloth->GetClothFrame(), tempStateList, m_ClothController->GetLOD(), fwClothDebug::sm_LastSelectedVertsCount, fwClothDebug::sm_LastSelectedVerts, &funcSwap );
					sysMemEndDebug();
				}

				fwClothDebug::sm_LastSelectedVertsCount = 0;
				fwClothDebug::sm_SelectedVertsCount = 0;
			}
			else if( m_Type == clothManager::Character )
			{
				// TODO: do something
			}
		}
	}
}


void clothInstance::DrawSelection(const Vector3& offset, const Vector3* RESTRICT pClothVertices, bool bDrawInfo )
{
	float sphereRadius = 0.02f;
	float sphereRadiusSelected = 0.01f;
	if( m_Type == clothManager::Character )
		sphereRadius = 0.01f;

	fwClothDebug::DrawSelection(offset, pClothVertices, sphereRadius, sphereRadiusSelected, bDrawInfo);

	if( fwClothDebug::sm_SelectedVertsCount == 1 )
	{
		char buf[256];
		static float _x = 0.2f;
		static float _y = 0.3f;

		if( m_Type == clothManager::Character && fwClothDebug::sm_SelectedVerts[0] != -1 )
		{
			characterClothController* cccontroller = (characterClothController*)m_ClothController;
			const characterClothController::BindingInfo* bindingInfo = cccontroller->GetBindingInfo()->GetElements();
			Assert( bindingInfo );

			Assert( fwClothDebug::sm_SelectedVerts[0] > -1 && fwClothDebug::sm_SelectedVerts[0] < m_ClothController->GetCurrentCloth()->GetNumVertices() );
			const characterClothController::BindingInfo &bInfo = bindingInfo[ fwClothDebug::sm_SelectedVerts[0] ];
			Assert( GetSkeleton() );
			const crSkeletonData& skelData = GetSkeleton()->GetSkeletonData();

			const atArray<s32>& boneIndexMap = cccontroller->GetBoneIndexMapRef();

			float verticaloffset = 0.015f;

			formatf( buf, "Vertex index: %d, weights: %f %f %f %f ", fwClothDebug::sm_SelectedVerts[0], bInfo.weights.x, bInfo.weights.y, bInfo.weights.z,  bInfo.weights.w );								
			grcDebugDraw::Text( Vector2(_x, _y), Color_white, buf);

			const s32 bone0 = (bInfo.weights.x < 0.000001f) ? 0: boneIndexMap[bInfo.blendIndices[0]];
			const s32 bone1 = (bInfo.weights.y < 0.000001f) ? 0: boneIndexMap[bInfo.blendIndices[1]];
			const s32 bone2 = (bInfo.weights.z < 0.000001f) ? 0: boneIndexMap[bInfo.blendIndices[2]];
			const s32 bone3 = (bInfo.weights.w < 0.000001f) ? 0: boneIndexMap[bInfo.blendIndices[3]];
			formatf( buf, "blend indices: %d %d %d %d", bone0, bone1, bone2, bone3 );
			grcDebugDraw::Text( Vector2(_x, _y + verticaloffset ), Color_white, buf);

			const char* bone0name = bone0 ? skelData.GetBoneData( bone0 )->GetName() : "----";
			const char* bone1name = bone1 ? skelData.GetBoneData( bone1 )->GetName() : "----";
			const char* bone2name = bone2 ? skelData.GetBoneData( bone2 )->GetName() : "----";
			const char* bone3name = bone3 ? skelData.GetBoneData( bone3 )->GetName() : "----";

			formatf( buf, "BoneName: %s = %d", bone0name, bone0 );
			grcDebugDraw::Text( Vector2(_x, _y + 2.0f*verticaloffset ), Color_white, buf);
			formatf( buf, "BoneName: %s = %d", bone1name, bone1 );
			grcDebugDraw::Text( Vector2(_x, _y + 3.0f*verticaloffset ), Color_white, buf);
			formatf( buf, "BoneName: %s = %d", bone2name, bone2 );
			grcDebugDraw::Text( Vector2(_x, _y + 4.0f*verticaloffset ), Color_white, buf);
			formatf( buf, "BoneName: %s = %d", bone3name, bone3 );
			grcDebugDraw::Text( Vector2(_x, _y + 5.0f*verticaloffset ), Color_white, buf);
		}
		else
		{
			formatf( buf, "Vertex index: %d", fwClothDebug::sm_SelectedVerts[0] );
			grcDebugDraw::Text( Vector2(_x, _y), Color_white, buf);
		}						
	}
}

int clothInstance::PickVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool bPin )
{
	fwClothDebug::SelectVerts( offset, pClothVertices, numVerts, numPinVerts, pViewport, bPin);

	const int totalSelectedVerts = (fwClothDebug::sm_SelectedVertsCount+fwClothDebug::sm_LastSelectedVertsCount);
	if( totalSelectedVerts )
	{
		DrawSelection(offset, pClothVertices);
	}

	return totalSelectedVerts;
}

void clothInstance::PreviewEdgesSplit(int vertexIdx, int edgeIdx0, int edgeIdx1)
{
	if( PFD_PickEdges.Begin(true) )
	{
		const int clothLod = m_ClothController->GetLOD();
// 		const u16* pClothDisplayMap = m_ClothController->GetClothDisplayMap(clothLod);
// 		Assert( pClothDisplayMap );

		phVerletCloth* pCloth = m_ClothController->GetCloth(clothLod);
		Assert( pCloth );

		Vec3V* RESTRICT pClothVertices = pCloth->GetClothData().GetVertexPointer();	
		Assert( pClothVertices );

//		int numVerts = pCloth->GetNumVertices();

		for( int i = 0; i < pCloth->GetNumEdges(); ++i )
		{
			const phEdgeData& edge = pCloth->GetEdge(i);
			if( edge.m_vertIndices[0] != vertexIdx && edge.m_vertIndices[1] != vertexIdx )
				continue;

			Vec3V v = pClothVertices[ (edge.m_vertIndices[0] == vertexIdx ? edge.m_vertIndices[1] : edge.m_vertIndices[0] ) ];
			if(i == edgeIdx0 || i == edgeIdx1) 
			{
				grcDrawLine( VEC3V_TO_VECTOR3( pClothVertices[vertexIdx] ), VEC3V_TO_VECTOR3(v), Color_blue, Color_white );
			}
			else
			{
				Vec3V v0 = pClothVertices[vertexIdx];
				Color32 c = (v0.GetZf() > v.GetZf() ? Color_red: Color_green);
				grcDrawLine( VEC3V_TO_VECTOR3( v0 ), VEC3V_TO_VECTOR3(v), c );
			}
		}
		PFD_PickEdges.End();
	}
}

int clothInstance::PickEdgesByVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool bPin )
{
	phVerletCloth* pCloth = m_ClothController->GetCloth( m_ClothController->GetLOD());
	Assert( pCloth );
	const atArray<phEdgeData>& edgeData = pCloth->GetEdgeList();

// Verts
	fwClothDebug::SelectVerts( offset, pClothVertices, numVerts, numPinVerts, pViewport, bPin);
	const int totalSelectedVerts = (fwClothDebug::sm_SelectedVertsCount+fwClothDebug::sm_LastSelectedVertsCount);
	if( totalSelectedVerts )
	{
		DrawSelection(offset, pClothVertices, false);
	}

// Edges
	fwClothDebug::UpdateSelection(s_AccumulateEdges, s_DistributeEdges, s_SelectedEdgesCount, s_LastSelectedEdgesCount, s_SelectedEdges, s_LastSelectedEdges, s_LastSelectedEdgesColors);
	if ( pViewport )
	{
		if( ioMapper::DebugKeyPressed(KEY_E) )
		{
			s_SelectedEdgesCount = 0;
			s_LastSelectedEdgesCount = 0;
		}
		else
		{
			s_DistributeEdges = ioMapper::DebugKeyDown(KEY_CONTROL) ? true: false;
			s_AccumulateEdges = !s_DistributeEdges;
			s_SelectedEdgesCount = PickEdgesByVerts2( fwClothDebug::sm_SelectedVertsCount, fwClothDebug::sm_SelectedVerts , edgeData, s_SelectedEdges, 0, pCloth->GetNumEdges() );
		}
	}

	const int totalSelected = (s_SelectedEdgesCount+s_LastSelectedEdgesCount);
	if( totalSelected )
	{
		fwClothDebug::DrawPrimitivesInfo(s_SelectedEdgesCount, s_LastSelectedEdgesCount, "edges");

		for( int i = 0; i < s_SelectedEdgesCount; ++i )
		{
			const int edgeIndex = s_SelectedEdges[i];
			if( !s_DistributeEdges || !IsEdgeSelected(edgeIndex) )
			{
				char buf[8];
				const phEdgeData& edge = edgeData[ edgeIndex ];
				const int idx0 = edge.m_vertIndices[0];
				const int idx1 = edge.m_vertIndices[1];

				Vector3 v0 = pClothVertices[ idx0 ] + offset;
				Vector3 v1 = pClothVertices[ idx1 ] + offset;
				grcDrawLine( v0, v1, Color_blue );

				formatf( buf, "%d", edgeIndex );
				Vector3 mid = (v0 + v1) * 0.5f;
				grcColor( Color_blue );
				grcDrawLabelf( mid, buf );
			}
		}

#if __PFDRAW
		pCloth->DrawPickedEdges( pClothVertices, offset, s_LastSelectedEdgesCount, s_LastSelectedEdges.GetElements(), edgeData, s_LastSelectedEdgesColors.GetElements() );		
#endif
	}

	if( ioMapper::DebugKeyPressed(fwClothDebug::sm_ConfirmSelectionKey) )
	{
		s_PickedEdgeData = &edgeData;
	}


	return totalSelectedVerts;
}

void clothInstance::PickEdges( const Vector3& offset, phVerletCloth* cloth, const grcViewport* pViewport, const atArray<phEdgeData>& edgeData )
{
	Assert( cloth );
	const Vector3* RESTRICT clothVertices = GetClothVertices(cloth);
	Assert( clothVertices );

	fwClothDebug::UpdateSelection(s_AccumulateEdges, s_DistributeEdges, s_SelectedEdgesCount, s_LastSelectedEdgesCount, s_SelectedEdges, s_LastSelectedEdges, s_LastSelectedEdgesColors);

	fwClothDebug::DrawBrush((fwClothDebug::enBRUSH_SHAPE)fwClothDebug::sm_bBrushShape);

	if ( pViewport )
	{
		if( ioMapper::DebugKeyPressed(KEY_E) )
		{
			s_SelectedEdgesCount = 0;
			s_LastSelectedEdgesCount = 0;
		}
		else
		{
			s_DistributeEdges = ioMapper::DebugKeyDown(KEY_CONTROL) ? true: false;
			s_AccumulateEdges = !s_DistributeEdges;
			s_SelectedEdgesCount = PickEdgesScreenSpace( (fwClothDebug::enBRUSH_SHAPE)fwClothDebug::sm_bBrushShape, clothVertices, offset, pViewport, edgeData, fwClothDebug::sm_StartPos, fwClothDebug::sm_EndPos, s_SelectedEdges, 0 );
		}
	}

	const int totalSelected = (s_SelectedEdgesCount+s_LastSelectedEdgesCount);
	if( totalSelected )
	{
		fwClothDebug::DrawPrimitivesInfo(s_SelectedEdgesCount, s_LastSelectedEdgesCount, "edges");

		for( int i = 0; i < s_SelectedEdgesCount; ++i )
		{
			const int edgeIndex = s_SelectedEdges[i];
			if( !s_DistributeEdges || !IsEdgeSelected(edgeIndex) )
			{
				char buf[8];
				const phEdgeData& edge = edgeData[ edgeIndex ];
				const int idx0 = edge.m_vertIndices[0];
				const int idx1 = edge.m_vertIndices[1];

				Vector3 v0 = clothVertices[ idx0 ] + offset;
				Vector3 v1 = clothVertices[ idx1 ] + offset;
				grcDrawLine( v0, v1, Color_blue );

				formatf( buf, "%d", edgeIndex );
				Vector3 mid = (v0 + v1) * 0.5f;
				grcColor( Color_blue );
				grcDrawLabelf( mid, buf );
			}
		}

#if __PFDRAW
		cloth->DrawPickedEdges( clothVertices, offset, s_LastSelectedEdgesCount, s_LastSelectedEdges.GetElements(), edgeData, s_LastSelectedEdgesColors.GetElements() );		
#endif
	}

	if( ioMapper::DebugKeyPressed(fwClothDebug::sm_ConfirmSelectionKey) )
	{
		s_PickedEdgeData = &edgeData;
	}
}

void clothInstance::SetBoundCapsuleData(void* boundData)
{
	Assert( boundData );
	clothInstance::debugBoundData* bdata = (clothInstance::debugBoundData*)boundData;

	bkGroup* instGroup = bdata->instGroup;
	Assert( instGroup );
	pClothBank->SetCurrentGroup( *instGroup );

	phBoundCapsule* bndCapsule = (phBoundCapsule*)bdata->boundPart;
	Assert( bndCapsule );
	bndCapsule->SetCapsuleSize( pClothManager->m_DebugBoundRadius, pClothManager->m_DebugBoundLength );

#if USE_CAPSULE_EXTRA_EXTENTS
	bndCapsule->SetHalfHeight( pClothManager->m_DebugBoundHalfHeight );
#endif

	Assert( bdata->bndButton );
	pClothBank->Remove( *(bkWidget*)bdata->bndButton );

	const crSkeleton* pSkel = GetSkeleton();
	Assert( pSkel );
	const crSkeletonData& skelData = pSkel->GetSkeletonData();
	const crBoneData* pBoneData = skelData.GetBoneData(bdata->boneIdx);
	Assert( pBoneData );
	const char* pBoneName = pBoneData->GetName();
	Assert( pBoneName );

	char buttonTitle[256];
	formatf( buttonTitle, "Radius: %4.3f, Length: %4.3f,   BoneName: '%s'    BoneIndex:%d", bndCapsule->GetRadius(), bndCapsule->GetLength(), pBoneName, bdata->boneIdx );	
	bdata->bndButton = pClothBank->AddButton( buttonTitle, datCallback( MFA1(clothInstance::SetBoundCapsuleData), this, (CallbackData)boundData, false));

	pClothBank->UnSetCurrentGroup( *instGroup );
}


// clothManager

int indices[] = { LOD_HIGH, LOD_MED, LOD_LOW, LOD_VLOW, };

void clothManager::SetupEnvClothWidget( rage::clothController* controller )
{
	SetupWidget( controller, Environment, false );
}

// rebuild button's list in RAG dynamically when cloth lod selection has changed
void clothManager::SetupWidget( rage::clothController* controller, int verletType, bool isSubInst )
{
	if( pClothBank && !pClothBankButton )
	{
		Assert( controller );
		RemoveWidget( controller );
		AddWidget( controller, verletType, isSubInst );
	}
}

void clothManager::RemoveWidget( rage::clothController* controller )
{
	if( pClothBank && !pClothBankButton )
	{
		Assert( controller );
		clothInstance* cInstance = (clothInstance*)controller->GetClothInstance();
		if( cInstance && cInstance->m_RAGGroup )
		{
			pClothBank->DeleteGroup( *cInstance->m_RAGGroup );
			cInstance->m_RAGGroup				= NULL;
			cInstance->m_VirtualBoundGroup		= NULL;
			cInstance->m_TogglesGroup			= NULL;
			cInstance->m_DynamicPinUnpinWidget	= NULL;
			cInstance->m_DynamicPickVertsWidget	= NULL;
			cInstance->m_DynamicPickPinVertsWidget = NULL;
		}		
	}
}

void clothManager::UpdateDebugPose(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	if( cInstance->IsDebugPoseActive() )
	{
		clothController* pController = cInstance->GetClothController();
		Assert( pController );
		phVerletCloth* pCloth = pController->GetCloth(pController->GetLOD());
		Assert( pCloth );
		const Vec3V* RESTRICT pVerts = pCloth->GetClothData().GetVertexPointer();
		Assert( pVerts );

		const int numVerts = pCloth->GetNumVertices();
		sysMemStartDebug();
		cInstance->m_DebugPose.Resize(numVerts);
		for(int i = 0; i < numVerts; ++i)
		{
			cInstance->m_DebugPose[i] = pVerts[i];
		}
		sysMemEndDebug();
	}
	else
	{
		sysMemStartDebug();
		cInstance->m_DebugPose.Reset();
		sysMemEndDebug();
	}
}

void clothManager::CreateCustomBoundGroup(const phBoundComposite* pCustomBound, const char* pButtonTitleStr, clothInstance* pClothInstance )
{
	Assert( pCustomBound );
	Assert( pButtonTitleStr );
	Assert( pClothInstance );	
	Assert( pClothInstance->m_RAGGroup );

	bkGroup* pVirtualBoundGroup = pClothInstance->m_RAGGroup->AddGroup( pButtonTitleStr ,false );			
	Assert( pVirtualBoundGroup );
	pVirtualBoundGroup->AddButton( "Toggle custom bound debug draw", datCallback( MFA1(clothManager::ToggleDrawCustomBound), this, (CallbackData)pClothInstance, false ), "[Work in progress]. Show custom bounds." );
#if USE_CLOTH_GIZMO
	const int numBounds = pCustomBound->GetNumBounds();
	for( int i = 0; i < numBounds; ++i )
	{
		phBound* pBound = pCustomBound->GetBound(i);
		if( pBound )
		{
			char buff[128];
			formatf(buff, "Attach gizmo to bound 0x%p", pBound );
			pVirtualBoundGroup->AddButton( buff, datCallback( MFA1(clothManager::AttachBoundToGizmo), this, (CallbackData)&pCustomBound->GetCurrentMatrix(i), false ), "[Work in progress]. Attach/Detach gizmo to custom bound." );

			formatf(buff, "Set custom bound 0x%p capsule data", pBound );
			pVirtualBoundGroup->AddButton( buff, datCallback( MFA1(clothManager::SetCustomBoundCapsuleData), this, (CallbackData)pBound, false ), "" );
		}
	}
#endif // USE_CLOTH_GIZMO
	Assert( !pClothInstance->m_VirtualBoundGroup );
	pClothInstance->m_VirtualBoundGroup = pVirtualBoundGroup;
}

Mat34V iMatTemp(V_IDENTITY);
void clothManager::AttachVirtualBound(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	Assert( cInstance->GetType() == clothManager::Character );
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );

	// TODO: character cloth doesn't support lods ...yet
	int lodIndex = 0;
	phVerletCloth* pCloth = pController->GetCloth(lodIndex);
	Assert( pCloth );

	if( !pCloth->m_VirtualBound )
	{
		Mat34V boundMat;
		Mat34VFromEulersXYZ( boundMat, VECTOR3_TO_VEC3V( s_BoundRotation ) );
		boundMat.SetCol3( VECTOR3_TO_VEC3V( s_BoundPosition ) );

		pCloth->SetFlag(phVerletCloth::FLAG_COLLIDE_EDGES, true);
		pCloth->SetFlag(phVerletCloth::FLAG_IGNORE_OFFSET, true);

		pCloth->CreateVirtualBound( 1, &iMatTemp );
		pCloth->AttachVirtualBoundCapsule( pClothManager->m_DebugBoundRadius, pClothManager->m_DebugBoundLength, boundMat, 0 );


		Assert( !cInstance->m_VirtualBoundGroup );
		const phBound* pCustomBound = pCloth->m_VirtualBound;

		char buttonTitle[256];
		formatf( buttonTitle, "Debug custom bound 0x%p", pCustomBound );

		CreateCustomBoundGroup( (const phBoundComposite*)pCustomBound, buttonTitle, cInstance );
	}
	else
	{
		clothWarningf("Cloth has already virtual bound attached: 0x%p", pCloth->m_VirtualBound.ptr );
	}
}

void clothManager::DetachVirtualBound(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	Assert( cInstance->GetType() == Character );
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );

	// TODO: character cloth doesn't support lods ...yet
	int lodIndex = 0;
	phVerletCloth* verletCloth = pController->GetCloth(lodIndex);
	Assert( verletCloth );

	verletCloth->DetachVirtualBound();
	verletCloth->SetFlag( phVerletCloth::FLAG_COLLIDE_EDGES, false );
	verletCloth->SetFlag( phVerletCloth::FLAG_IGNORE_OFFSET, false );

	if( cInstance->m_VirtualBoundGroup )
	{
		pClothBank->DeleteGroup( *cInstance->m_VirtualBoundGroup );
		cInstance->m_VirtualBoundGroup = NULL;
	}
}

#if CLOTH_INSTANCE_FROM_DATA

void clothManager::DetachFromDrawable(void* /*pInstance*/)
{
	Assert( m_DetachFromDrawableCB );
	Assert( ms_PedClothVarData );
	m_DetachFromDrawableCB( ms_PedClothVarData );


	Assert( clothManager::ms_pPedDrawable );
	grmModel* pPedModel = clothManager::ms_pPedDrawable->GetLodGroup().GetLod(0).GetModel(0);
	Assert( pPedModel );

	const int geometryIdx = clothManager::ms_GeometryIndex; //1	
	grmGeometry* pPedGeometry = &pPedModel->GetGeometry(geometryIdx);
	Assert( pPedGeometry );
	grcVertexBuffer* pMeshBuffer = pPedGeometry->GetVertexBuffer();
	Assert( pMeshBuffer != NULL );

	const grcFvf* pFvf = pMeshBuffer->GetFvf();
	Assert( pFvf );

	// NOTE: buffereditor lock gets released at the end of the scope
	{
		grcVertexBufferEditor meshBufferEditor(pMeshBuffer);

		u8* pPtr = ((u8*)(pMeshBuffer->GetFastLockPtr()));
		Assert( pPtr );
		u32 uVertStride = pMeshBuffer->GetVertexStride();
		Assert( uVertStride > 0 );

		Assert( s_OriginalMorphData );

		for( int i = 0; i < s_MorphDataCount; ++i )
		{			
			u8* pVertPtr = pPtr + (uVertStride * s_OriginalMorphData[i].vtxIndex );
			Assert( pVertPtr );

			// set binding
			int mtx0 = s_OriginalMorphData[i].index0;
			int mtx1 = s_OriginalMorphData[i].index1;
			int mtx2 = s_OriginalMorphData[i].index2;
			int mtx3 = s_OriginalMorphData[i].index3;
			Color32* pBindingPtr = (Color32*)(pVertPtr + pFvf->GetOffset(grcFvf::grcfcBinding));
			Assert( pBindingPtr );

			pBindingPtr->Set(mtx0,mtx1,mtx2,mtx3);		


			// set weight
			float fWeight0 = s_OriginalMorphData[i].weights.GetXf();
			float fWeight1 = s_OriginalMorphData[i].weights.GetYf();
			float fWeight2 = s_OriginalMorphData[i].weights.GetZf();
			float fWeight3 = s_OriginalMorphData[i].weights.GetWf();

			Color32* pWeightsPtr = (Color32*)(pVertPtr + pFvf->GetOffset(grcFvf::grcfcWeight));
			Assert( pWeightsPtr );

			pWeightsPtr->Setf(fWeight0, fWeight1, fWeight2, fWeight3);
		}
	}

}



void clothManager::AttachToDrawable(void* /*pInstance*/)
{
	Assert( m_AttachToDrawableCB );
	Assert( ms_PedClothVarData );
	m_AttachToDrawableCB( ms_PedClothVarData );


	Assert( clothManager::ms_pPedDrawable );
	grmModel* pPedModel = clothManager::ms_pPedDrawable->GetLodGroup().GetLod(0).GetModel(0);
	Assert( pPedModel );

	const int geometryIdx = clothManager::ms_GeometryIndex; //1	
	grmGeometry* pPedGeometry = &pPedModel->GetGeometry(geometryIdx);
	Assert( pPedGeometry );
	grcVertexBuffer* pMeshBuffer = pPedGeometry->GetVertexBuffer();
	Assert( pMeshBuffer != NULL );

	const grcFvf* pFvf = pMeshBuffer->GetFvf();
	Assert( pFvf );

	// NOTE: buffereditor lock gets released at the end of the scope
	{
		grcVertexBufferEditor meshBufferEditor(pMeshBuffer);

		u8* pPtr = ((u8*)(pMeshBuffer->GetFastLockPtr()));
		Assert( pPtr );
		u32 uVertStride = pMeshBuffer->GetVertexStride();
		Assert( uVertStride > 0 );

		Assert( s_MorphData );

		for( int i = 0; i < s_MorphDataCount; ++i )
		{			
			u8* pVertPtr = pPtr + (uVertStride * s_MorphData[i].vtxIndex );
			Assert( pVertPtr );

			// set binding
			int mtx0 = 255;
			int mtx1 = s_MorphData[i].index0;
			int mtx2 = s_MorphData[i].index1;
			int mtx3 = s_MorphData[i].index2;
			Color32* pBindingPtr = (Color32*)(pVertPtr + pFvf->GetOffset(grcFvf::grcfcBinding));
			Assert( pBindingPtr );

			s_OriginalMorphData[i].vtxIndex = s_MorphData[i].vtxIndex;
			s_OriginalMorphData[i].index0 = pBindingPtr->GetRed();
			s_OriginalMorphData[i].index1 = pBindingPtr->GetGreen();
			s_OriginalMorphData[i].index2 = pBindingPtr->GetBlue();
			s_OriginalMorphData[i].index3 = pBindingPtr->GetAlpha();

			pBindingPtr->Set(mtx0,mtx1,mtx2,mtx3);		

		
			// set weight
			float fWeight0 = Min(s_MorphData[i].weights.GetXf(),1.0f);
			float fWeight1 = Min(s_MorphData[i].weights.GetYf(),1.0f);
			float fWeight2 = Min(s_MorphData[i].weights.GetZf(),1.0f);
			float fWeight3 = Min((s_MorphData[i].weights.GetWf() * 10.0f) + 0.5f,1.0f);  // Scale it, offset it. pack it.

			Color32* pWeightsPtr = (Color32*)(pVertPtr + pFvf->GetOffset(grcFvf::grcfcWeight));
			Assert( pWeightsPtr );

			s_OriginalMorphData[i].weights = Vec4V( pWeightsPtr->GetRedf(), pWeightsPtr->GetGreenf(), pWeightsPtr->GetBluef(), pWeightsPtr->GetAlphaf() );

			pWeightsPtr->Setf(fWeight0, fWeight1, fWeight2, fWeight3);
		}
	}

}

void clothManager::MapToDrawable(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );

	(const_cast<crSkeleton*>(ms_PedSkeleton))->Reset();
	(const_cast<crSkeleton*>(ms_PedSkeleton))->Update();	
	pController->SkinMesh(ms_PedSkeleton);


	phVerletCloth* pCloth = pController->GetCloth(0);
	Assert( pCloth );

	int iClothVertsCount = pCloth->GetNumVertices();
	Vector3* RESTRICT pClothVerts = (Vector3*)pCloth->GetClothData().GetVertexPointer();
	Assert( pClothVerts );

	const atArray<u16>* pTriIndices = pController->GetTriIndices();
	Assert( pTriIndices );

	int iTriIndicesCount = pTriIndices->GetCount();

	// set default morph/mapping distances
	sysMemStartDebug();
	float* pMappingThreshold = rage_new float[iClothVertsCount];
	float* pErrorThreshold = rage_new float[iClothVertsCount];
	sysMemEndDebug();

	for( int i = 0; i < iClothVertsCount; ++i )
	{
		pMappingThreshold[i] = 0.05f/*DEFAULT_MAPPING_THRESHOLD*/;
	}

	for( int i = 0; i < iClothVertsCount; ++i )
	{
		pErrorThreshold[i] = 1.04f /*DEFAULT_ERROR_THRESHOLD*/;
	}

	Assert( s_MorphData );
	s_MorphDataCount = 0;

#if NO_PIN_VERTS_IN_VERLET
	const int pinnedCount = pCloth->GetClothData().GetNumPinVerts();
#else
	const int pinnedCount = pCloth->GetPinCount();
#endif
	int pinnedMatch = 0;
	for(int i = 0; i < s_VertexCount; ++i )
	{
		if( characterClothController::FindCoordinates( pMappingThreshold, pErrorThreshold, pinnedMatch, pinnedCount, i, s_PoseOut[i], false, iTriIndicesCount, pTriIndices->GetElements(), (Vec3V*)pClothVerts, &s_MorphData[s_MorphDataCount] /*pMorphData[iMorphDataCount]*/ ) )
		{			
			++s_MorphDataCount;

			s_PoseColor[i] = Color_red;
		}
//		Assert( pinnedMatch < pinnedCount );	// why more verts are mapped to the pinned than initial pinned count
	}
//	Assert( pinnedMatch == pinnedCount );	// pinned count from cloth sim should match what has been found in the drawable
	

	sysMemStartDebug();
	delete[] pMappingThreshold;
	delete[] pErrorThreshold;
	sysMemEndDebug();
}

#endif // CLOTH_INSTANCE_FROM_DATA

void clothManager::LoadCharacterCloth(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );
	characterCloth* pCharCloth = (characterCloth*)(pController->GetOwner());
	Assert( pCharCloth );
	pCharCloth->LoadAll();

	cInstance->SetActive( true );
}

void clothManager::SaveCharacterCloth(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );
	characterCloth* pCharCloth = (characterCloth*)(pController->GetOwner());
	Assert( pCharCloth );
	pCharCloth->SaveAll();
}

void clothManager::SetSelectedPinRadiusSet(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );
	pController->SetPackageIndex( (u8)cInstance->m_DebugCurrentPinRadiusSet );
}

void clothManager::SetPose(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	characterClothController* pController = (characterClothController*)cInstance->GetClothController();
	Assert( pController );
	characterCloth* pCharCloth = (characterCloth*)pController->GetOwner();
	Assert( pCharCloth ); 
	pCharCloth->SetPose( (u8)cInstance->m_DebugCurrentPose );
}

void clothManager::SaveBridgeToFile(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	clothController* pController = cInstance->GetClothController();
	Assert( pController );
	clothBridgeSimGfx* pClothBridge = pController->GetBridge();
	Assert( pClothBridge );
	pClothBridge->Save( (const char*)pController->GetName() );
}

void clothManager::ConnectToRest(void* pInstance)
{
	Assert( pInstance );
	clothInstance* cInstance = (clothInstance*)pInstance;
	if( cInstance->m_RestInterface )
	{
		clothController* controller = cInstance->GetClothController();
		Assert( controller );
		if( g_LastPickedClothController && g_LastPickedClothController != controller )
		{
			clothInstance* pLastPickedInstance = (clothInstance*)(g_LastPickedClothController->GetClothInstance());
			Assert( pLastPickedInstance );
			pLastPickedInstance->m_RestInterface = false;
			pLastPickedInstance->UnregisterRestInterface( g_LastPickedClothController->GetName() );
			g_LastPickedClothController->UnregisterRestInterface();			
		}

		ResetSelection( controller );

		if ( g_LastPickedClothController )
		{
			g_LastPickedClothController->RegisterRestInterface();
			cInstance->RegisterRestInterface( g_LastPickedClothController->GetName() );
		}
	}
	else
	{
		if( g_LastPickedClothController )
		{
			g_LastPickedClothController->UnregisterRestInterface();
			cInstance->UnregisterRestInterface( g_LastPickedClothController->GetName() );
		}

		ClearSelection();
	}
}

void clothManager::DragVertsCB(void* pInstance)
{
	ResetSelection0(pInstance);
}

void clothManager::PickPinVertsCB(void* pInstance)
{
	clothInstance* pClothInstance = ((clothInstance*)pInstance);
	Assert( pClothInstance->m_TogglesGroup );
	if( pClothInstance->m_DynamicPinUnpinWidget )	
	{
		pClothInstance->m_TogglesGroup->Remove(*pClothInstance->m_DynamicPinUnpinWidget);
	}

	if( pClothInstance->m_DebugPickVerts )
	{	
		if( pClothInstance->m_DynamicPickPinVertsWidget )
		{
			pClothInstance->m_TogglesGroup->Remove(*pClothInstance->m_DynamicPickPinVertsWidget);
			pClothInstance->m_DynamicPickPinVertsWidget = NULL;
		}
		pClothInstance->m_DynamicPinUnpinWidget = pClothInstance->m_TogglesGroup->AddToggle("Pin Verts", &pClothInstance->m_DebugPinVerts, NullCB, "[Work in progress]. Activate pinning of vertex selection for this cloth." );
	}
	else if( pClothInstance->m_DebugPickPinVerts )
	{
		if( pClothInstance->m_DynamicPickVertsWidget )
		{
			pClothInstance->m_TogglesGroup->Remove(*pClothInstance->m_DynamicPickVertsWidget);
			pClothInstance->m_DynamicPickVertsWidget = NULL;
		}
		pClothInstance->m_DynamicPinUnpinWidget = pClothInstance->m_TogglesGroup->AddToggle("UnPin Verts", &pClothInstance->m_DebugUnPinVerts, NullCB, "[Work in progress]. Activate unpinning of vertex selection for this cloth." );
	}
	else
	{	
		if( pClothInstance->m_DynamicPickPinVertsWidget )
		{
			pClothInstance->m_TogglesGroup->Remove(*pClothInstance->m_DynamicPickPinVertsWidget);
			pClothInstance->m_DynamicPickPinVertsWidget = NULL;
		}
		if( pClothInstance->m_DynamicPickVertsWidget )
		{
			pClothInstance->m_TogglesGroup->Remove(*pClothInstance->m_DynamicPickVertsWidget);
			pClothInstance->m_DynamicPickVertsWidget = NULL;
		}
		Assert( !pClothInstance->m_DynamicPickVertsWidget );
		pClothInstance->m_DynamicPickVertsWidget = pClothInstance->m_TogglesGroup->AddToggle("Pick Verts", &pClothInstance->m_DebugPickVerts, datCallback( MFA1( clothManager::PickPinVertsCB ), this, pClothInstance ), "Activate vertex/vertices selection for this cloth." );
		Assert( pClothInstance->m_DynamicPickVertsWidget );

		Assert( !pClothInstance->m_DynamicPickPinVertsWidget );
		pClothInstance->m_DynamicPickPinVertsWidget = pClothInstance->m_TogglesGroup->AddToggle("Pick Pin Verts", &pClothInstance->m_DebugPickPinVerts, datCallback( MFA1( clothManager::PickPinVertsCB ), this, pClothInstance ), "Activate pinned vertex/vertices selection for this cloth." );
		Assert( pClothInstance->m_DynamicPickPinVertsWidget );

		pClothInstance->m_DynamicPinUnpinWidget = NULL;
	}

	ResetSelection0(pInstance);
}

void clothManager::ResetSelection0(void* pInstance)
{
	Assert( pInstance );
	if( ((clothInstance*)pInstance)->m_DebugPickVerts )
		ResetSelection( ((clothInstance*)pInstance)->GetClothController() );
	else
		ClearSelection();
}

void clothManager::ResetSelection1(void* pInstance)
{
	Assert( pInstance );
	if( ((clothInstance*)pInstance)->m_DebugPickEdges || ((clothInstance*)pInstance)->m_DebugPickEdgesByVerts )
		ResetSelection( ((clothInstance*)pInstance)->GetClothController() );
	else
		ClearSelection();
}

void clothManager::ResetSelection2(void* pInstance)
{
	Assert( pInstance );	
	if( ((clothInstance*)pInstance)->m_DebugPickInstanceCustomEdges )
		ResetSelection( ((clothInstance*)pInstance)->GetClothController() );
	else
		ClearSelection();
}

void clothManager::ResetSelection3(void* pInstance)
{
	Assert( pInstance );	
	if( ((clothInstance*)pInstance)->m_DebugPickCustomEdges )
		ResetSelection( ((clothInstance*)pInstance)->GetClothController() );
	else
		ClearSelection();
}

void clothManager::ResetSelection( rage::clothController* controller )
{
	Assert( controller );
	if ( g_LastPickedClothController && g_LastPickedClothController != controller )
	{
		clothInstance* pLastPickedInstance = (clothInstance*)(g_LastPickedClothController->GetClothInstance());
		Assert( pLastPickedInstance );
		pLastPickedInstance->m_DebugDragVerts = false;
		pLastPickedInstance->m_DebugPickVerts = false;
		pLastPickedInstance->m_DebugPickPinVerts = false;
		pLastPickedInstance->m_DebugPickEdges = false;
		pLastPickedInstance->m_DebugPickEdgesByVerts = false;
		pLastPickedInstance->m_DebugPickCustomEdges = false;
		pLastPickedInstance->m_DebugPickInstanceCustomEdges = false;
		pLastPickedInstance->m_DebugPreviewEdgeSplit = false;
	}

	ClearSelection();
	g_LastPickedClothController = controller;
}

void clothManager::ClearSelection()
{
	fwClothDebug::sm_SelectedVertsCount = 0;
	fwClothDebug::sm_LastSelectedVertsCount = 0;

	s_SelectedEdgesCount = 0;
	s_LastSelectedEdgesCount = 0;
}

void clothManager::SetEdgeDataInternal( int idx, char offset, float newValue )
{
	Assert( idx != -1 );

	const int edgeIdx = s_LastSelectedEdges[idx];
	Assert( edgeIdx != -1 );
	Assert( s_PickedEdgeData );
	phEdgeData* edge = const_cast<phEdgeData*>(&((*s_PickedEdgeData)[edgeIdx]));
	Assert( edge );
	float* ptr = (float*)(((char*)edge) + offset);
	*ptr =  newValue;
}

void clothManager::SetEdgeWeight()
{
	for( int i = 0; i < s_LastSelectedEdgesCount; ++i )
	{
		SetEdgeDataInternal( i, OffsetOf(phEdgeData, m_Weight0), clothManager::ms_DebugEdgeWeight  );
	}
}

void clothManager::SetEdgeCompWeight()
{
	for( int i = 0; i < s_LastSelectedEdgesCount; ++i )
	{
		SetEdgeDataInternal( i, OffsetOf(phEdgeData, m_CompressionWeight), clothManager::ms_DebugEdgeCompWeight);
	}
}

void clothManager::SetEdgeLenSqr()
{
	for( int i = 0; i < s_LastSelectedEdgesCount; ++i )
	{
		SetEdgeDataInternal( i, OffsetOf(phEdgeData, m_EdgeLength2), clothManager::ms_DebugEdgeLenSqr  );
	}
}

void clothManager::SetPinRadius()
{
	if( fwClothDebug::sm_LastSelectedVertsCount > 0 )
	{
		Assert( g_LastPickedClothController );

		// TODO: fix it - svetli
		int lodIndex = 0;

		phVerletCloth* pCloth = g_LastPickedClothController->GetCloth(lodIndex);
		Assert( pCloth );
		const int numVertices = pCloth->GetNumActiveEdges();

		clothInstance* pClothInstance = (clothInstance*)g_LastPickedClothController->GetClothInstance();
		Assert( pClothInstance );

		float* vertexBlends = const_cast<float*>((const float*)(((char*)g_LastPickedClothController->GetBridge()->GetVertexBlends(lodIndex)) + (pClothInstance->m_DebugCurrentPinRadiusSet*numVertices*sizeof(float))));
		if( vertexBlends )
		{
			// TODO: for now only character cloth has pin radiuses
			for( int i = 0; i < fwClothDebug::sm_LastSelectedVertsCount; ++i )
			{
				vertexBlends[ fwClothDebug::sm_LastSelectedVerts[i] ] = clothManager::ms_PinRadius;
			}
		}
	}
}

void clothManager::SetVertexResistance()
{
	if( fwClothDebug::sm_LastSelectedVertsCount > 0 )
	{
		Assert( g_LastPickedClothController );

		// TODO: fix it - svetli
		int lodIndex = 0;

		float* RESTRICT vertexWeights = const_cast<float*>( g_LastPickedClothController->GetBridge()->GetVertexWeights(lodIndex) );
		Assert( vertexWeights );
		for( int i = 0; i < fwClothDebug::sm_LastSelectedVertsCount; ++i )
			vertexWeights[ fwClothDebug::sm_LastSelectedVerts[i] ] = clothManager::ms_VertexWeight;		
	}
}

void clothManager::SetInflationScale()
{
	if( fwClothDebug::sm_LastSelectedVertsCount > 0 )
	{
		Assert( g_LastPickedClothController );

		// TODO: fix it - svetli
		int lodIndex = 0;

		float* RESTRICT inflationScale = const_cast<float*>( g_LastPickedClothController->GetBridge()->GetInflationScale(lodIndex) );
		Assert( inflationScale );
		for( int i = 0; i < fwClothDebug::sm_LastSelectedVertsCount; ++i )
			inflationScale[ fwClothDebug::sm_LastSelectedVerts[i] ] = clothManager::ms_InflationScale;		
	}
}

//////////// TODO: test test test - for prototype and debug purposes
void clothManager::SetMatrixIndices()
{
	if( fwClothDebug::sm_LastSelectedVertsCount > 0 )
	{
		if( g_LastPickedClothController )
		{
			environmentCloth* envCloth = (environmentCloth*)g_LastPickedClothController->GetOwner();
			Assert(envCloth);
			atArray<int>& clothUserData = envCloth->GetUserData();
			Assert( clothUserData.GetCount() );
			if( clothUserData.GetCount() )
			{
				for( int i = 0; i < fwClothDebug::sm_LastSelectedVertsCount; ++i )
					clothUserData[ fwClothDebug::sm_LastSelectedVerts[i] ] = ms_DebugMatrixIndex;
			}
		}
	}
}
////////////


void clothManager::AddCustomEdge()
{
	if( fwClothDebug::sm_LastSelectedVertsCount == 2 && g_LastPickedClothController )
	{
		phVerletCloth* cloth = g_LastPickedClothController->GetCloth(0);
		Assert( cloth );

		clothInstance* cInstance = (clothInstance*)g_LastPickedClothController->GetClothInstance();
		Assert( cInstance );
		const Vec3V* RESTRICT vPos = (const Vec3V* RESTRICT)(cInstance->GetClothVertices(cloth));
		Assert( vPos );
		const u16 v0Idx = (u16)(fwClothDebug::sm_LastSelectedVerts[0]);
		const u16 v1Idx = (u16)(fwClothDebug::sm_LastSelectedVerts[1]);
		float len = Mag( Subtract( vPos[v0Idx], vPos[v1Idx] ) ).Getf();
		cloth->AddCustomEdge( v0Idx, v1Idx, len*len, DEFAULT_VERLET_COMPRESSION_STIFFNESS );

		ClearSelection();
	}
}

void clothManager::RemoveCustomEdge()
{
	if( s_LastSelectedEdgesCount == 1 && g_LastPickedClothController )
	{
		const int edgeIdx = s_LastSelectedEdges[0];
		Assert( edgeIdx != -1 );

		phVerletCloth* pCloth = g_LastPickedClothController->GetCloth(0);		// TODO: fix it to work with LODs
		Assert( pCloth );
		pCloth->RemoveCustomEdge( (u32)edgeIdx );

		ClearSelection();
	}
}

void clothManager::RemoveLastCustomEdge()
{
	if( g_LastPickedClothController )
	{
		phVerletCloth* pCloth = g_LastPickedClothController->GetCloth(0);		// TODO: fix it to work with LODs
		Assert( pCloth );
		int edgeIdx = pCloth->m_CustomEdgeData.GetCount() - 1;
		if( edgeIdx > -1 )
		{
			pCloth->RemoveCustomEdge( (u32)edgeIdx );
		}

		ClearSelection();
	}
}


void clothManager::SetClothWeight()
{
	if( g_LastPickedClothController )
		g_LastPickedClothController->SetClothWeight( clothManager::ms_ClothWeight );
}

void clothManager::SetPinVertsOffset()
{
	if( g_LastPickedClothController )
	{
		phVerletCloth* pCloth = g_LastPickedClothController->GetCurrentCloth();
		Assert( pCloth );

		const Vec3V* RESTRICT vertsOrig = pCloth->GetVerletClothType()->GetClothData().GetVertexPointer();
		Assert( vertsOrig );

		phClothData& clothData = pCloth->GetClothData();
		Vec3V* RESTRICT verts = clothData.GetPinVertexPointer();
		Assert( verts );
#if __ASSERT
#if NO_PIN_VERTS_IN_VERLET
		int numPinVerts = pCloth->GetClothData().GetNumPinVerts();
#else
		int numPinVerts = pCloth->GetPinCount();
#endif
#endif
		for( int k = 0; k < fwClothDebug::sm_LastSelectedVertsCount; ++k )
		{
			const int vtxIndex = fwClothDebug::sm_LastSelectedVerts[k];
			Assert( vtxIndex > -1 && vtxIndex < numPinVerts );
			verts[vtxIndex] = vertsOrig[vtxIndex] + VECTOR3_TO_VEC3V(s_PinVertsOffset);
		}
	}
}

void clothManager::ToggleRecordingMode()
{
#if RECORDING_VERTS
	// do something ?
#endif
}

void clothManager::ToggleRecording()
{
#if RECORDING_VERTS
	if( g_EnableRecording )
	{
		g_DbgRecordLines.Reset();
		g_DbgRecordSpheres.Reset();
		g_DbgRecordTriangles.Reset();
		g_DbgRecordCapsules.Reset();
		g_DbgRecordCustomClothEvents.Reset();
	}
#endif
}

void clothManager::ToggleDrawCustomBound(void* ptr)
{
	Assert(ptr);
	clothInstance* cinst = (clothInstance*)ptr;
	Assert(cinst);
	cinst->m_DebugDrawCustomBound = !cinst->m_DebugDrawCustomBound;
}


#if CLOTH_INSTANCE_FROM_DATA

void clothManager::InstanceCharacterClothFromData()
{
	if( ms_PedSkeleton && ms_pPedDrawable )
	{
		const crSkeleton* pSkeleton = ms_PedSkeleton;

		clothVariationData* pClothVarData = InstanceCharacterCloth( pSkeleton );
		if( pClothVarData )
		{
			pgRef<characterCloth>& pCharCloth = pClothVarData->GetCloth();
			Assert( pCharCloth );

			clothController* pClothController = pCharCloth->GetClothController();
			Assert( pClothController );

			clothInstance* pClothInstance = (clothInstance*)pClothController->GetClothInstance();
			Assert( pClothInstance );

			ms_PedClothVarData = pClothVarData;

			bkGroup* pRagGroup = pClothInstance->m_RAGGroup;
			Assert( pRagGroup );

			pRagGroup->AddButton("Remove cloth", datCallback( MFA1(clothManager::InitiateCharacterClothRemove), this, (CallbackData)pClothVarData, false ), "" );
			pRagGroup->AddButton("Load all", datCallback( MFA1( clothManager::LoadCharacterCloth ), this, pClothInstance ), "..." );

#if CLOTH_ASSIGN_TO_DRAWABLE

			pRagGroup->AddButton("Map to Drawable", datCallback( MFA1( clothManager::MapToDrawable ), this, pClothInstance ), "..." );
			pRagGroup->AddButton("Attach to Drawable", datCallback( MFA1( clothManager::AttachToDrawable ), this, pClothInstance ), "..." );
			pRagGroup->AddButton("Detach from Drawable", datCallback( MFA1( clothManager::DetachFromDrawable ), this, pClothInstance ), "..." );

			// Reset the skeleton to the bind pose
			(const_cast<crSkeleton*>(pSkeleton))->Reset();
			(const_cast<crSkeleton*>(pSkeleton))->Update();

			Mat34V skParentMat = *pSkeleton->GetParentMtx();
			skParentMat.SetCol3( Vec3V(V_ZERO) );


			Assert( clothManager::ms_pPedDrawable );
			grmModel* pPedModel = clothManager::ms_pPedDrawable->GetLodGroup().GetLod(0).GetModel(0);
			Assert( pPedModel );
			static int geometryIdx = 1;
			grmGeometry* pPedGeometry = &pPedModel->GetGeometry(geometryIdx);
			Assert( pPedGeometry );
			Assert( pPedGeometry->GetType() == grmGeometry::GEOMETRYQB );			
			grcVertexBuffer* pMeshBuffer = pPedGeometry->GetVertexBuffer();
			Assert( pMeshBuffer != NULL );
			pMeshBuffer->SetDynamic();

			const int numBonesBase = pSkeleton->GetBoneCount();

			sysMemStartTemp();

			Matrix43* pMtx = rage_new Matrix43[numBonesBase];
			Assert( pMtx );

			pSkeleton->Attach( true, pMtx );

			const int drawableVtxCount = pMeshBuffer->GetVertexCount();			
			Assert( s_PoseOut );
			Assert( drawableVtxCount < MAX_POSE_VERTS );


			s_VertexCount = drawableVtxCount;


			const grcFvf* pFvf = pMeshBuffer->GetFvf();
			Assert( pFvf );

			// NOTE: buffereditor lock gets released at the end of the scope
			{
				grcVertexBufferEditor meshBufferEditor(pMeshBuffer);

				u8* pPtr = ((u8*)(pMeshBuffer->GetFastLockPtr()));
				Assert( pPtr );
				u32 uVertStride = pMeshBuffer->GetVertexStride();
				Assert( uVertStride > 0 );

//				grcFvf::grcDataSize posSizeType = pFvf->GetDataSizeType(grcFvf::grcfcPosition);
					
				const float inv255f = 1.0f / 255.0f;
				for( int i = 0; i < drawableVtxCount; ++i )
				{
					u8* pVertPtr = pPtr + (uVertStride * i);
					Assert( pVertPtr );

					u8* pVertPosPtr = pVertPtr + pFvf->GetOffset(grcFvf::grcfcPosition);
					Vector3 v;

//					if (posSizeType == grcFvf::grcdsHalf4 || posSizeType == grcFvf::grcdsHalf3)
//					{
//						Float16* f = (Float16*)pVertPosPtr;
//						v.Set(f[0].GetFloat32_FromFloat16(), f[1].GetFloat32_FromFloat16(), f[2].GetFloat32_FromFloat16());
//					}
//					else if (posSizeType == grcFvf::grcdsFloat4 || posSizeType == grcFvf::grcdsFloat3)
					{
						float *f = (float*)pVertPosPtr;
						v.Set(f[0], f[1], f[2]);
					}

					Color32* pBindingPtr = (Color32*)(pVertPtr + pFvf->GetOffset(grcFvf::grcfcBinding));
					Assert( pBindingPtr );

					const int blendIndices[4] = { pBindingPtr->GetRed(), pBindingPtr->GetGreen(), pBindingPtr->GetBlue(), pBindingPtr->GetAlpha(), };

					Color32* pWeightsPtr = (Color32*)(pVertPtr + pFvf->GetOffset(grcFvf::grcfcWeight));
					Assert( pWeightsPtr );
					Vector4 blendWeights;
					blendWeights.Set( ((float)pWeightsPtr->GetRed())*inv255f, ((float)pWeightsPtr->GetGreen())*inv255f, ((float)pWeightsPtr->GetBlue())*inv255f, ((float)pWeightsPtr->GetAlpha())*inv255f );

					Matrix34 skinMtx;
					Matrix34 temp;
					pMtx[ blendIndices[0] ].ToMatrix34(RC_MAT34V(skinMtx));
					skinMtx.ScaleFull(blendWeights.x);				
					pMtx[ blendIndices[1] ].ToMatrix34(RC_MAT34V(temp));
					temp.ScaleFull(blendWeights.y);
					skinMtx.Add(temp);
					pMtx[ blendIndices[2] ].ToMatrix34(RC_MAT34V(temp));;
					temp.ScaleFull(blendWeights.z);
					skinMtx.Add(temp);
					pMtx[ blendIndices[3] ].ToMatrix34(RC_MAT34V(temp));
					temp.ScaleFull(blendWeights.w);
					skinMtx.Add(temp);			

					skinMtx.Transform( v );

					s_PoseOut[i] = Transform( skParentMat, VECTOR3_TO_VEC3V(v) );
					s_PoseColor[i] = Color_white;
				}

//				pMeshBuffer->SetNoDynamic();
//				pMeshBuffer->SetNoReadWrite();
			}			

			delete[] pMtx;
			sysMemEndTemp();

#endif // __XENON

			clothManager::ms_VehicleFrame = NULL;
//			clothManager::ms_PedSkeleton = NULL;
//			clothManager::ms_pPedDrawable = NULL;
		}
	}
}

void clothManager::InstanceEnvironmentClothFromData()
{
	Mat34V temp_Mat(V_IDENTITY);
	environmentCloth* pEnvCloth = InstanceEnvironmentCloth( temp_Mat, false );
	if( pEnvCloth )
	{
		clothInstance* cInstance = (clothInstance*)(pEnvCloth->GetClothController()->GetClothInstance());
		Assert( cInstance );

// TODO: check if offset is not 0,0,0		
		Vec3V instOffset = VECTOR3_TO_VEC3V(s_InstanceOffset);
		Vec3V newPos = rage::Add(temp_Mat.GetCol3(), instOffset);
		temp_Mat.SetCol3( newPos );
		cInstance->SetInstanceMat(temp_Mat);
		pEnvCloth->SetAttachedToFrame( RCC_MATRIX34(cInstance->GetInstanceMat()) ); 
		pEnvCloth->SetInitialPosition( newPos );
	}
}

#endif // CLOTH_INSTANCE_FROM_DATA

void clothManager::InitiateClothInstance(void* ptr)
{
	Assert(ptr);
	environmentCloth* pEnvClothType = (environmentCloth*) ptr;
	grmGeometry& geom = pEnvClothType->GetDrawable()->GetLodGroup().GetLod(LOD_HIGH).GetModel(0)->GetGeometry(0);

	/// SET FVF on the type drawable to non-dynamic, so the stride on the instanced drawable stay the same
	for( int i = 0; i < MAX_VERT_BUFFERS; ++i)
	{
		grcVertexBuffer* vbInstance = ((grmGeometryQB*)&geom)->GetVertexBufferByIndex(i);
		if( vbInstance )
		{
			Assert( vbInstance->GetFvf() );
			grcFvf* fvfInstance = const_cast<grcFvf*>(vbInstance->GetFvf());
			Assert(fvfInstance);
			fvfInstance->SetDynamicOrder(false);
		}
	}
	////////

	Mat34V temp_Mat(V_IDENTITY);
	environmentCloth* pEnvCloth = InstanceCloth( temp_Mat, pEnvClothType, false, true );
	if( pEnvCloth )
	{
		clothInstance* cInstance = (clothInstance*)(pEnvCloth->GetClothController()->GetClothInstance());
		Assert( cInstance );

		temp_Mat = pEnvClothType->GetAttachedToFrame();
// TODO: check if offset is not 0,0,0
		Vec3V instOffset = VECTOR3_TO_VEC3V(s_InstanceOffset);
		Vec3V newPos = rage::Add(temp_Mat.GetCol3(), instOffset);
		temp_Mat.SetCol3( newPos );
		cInstance->SetInstanceMat(temp_Mat);
		pEnvCloth->SetAttachedToFrame( RCC_MATRIX34(cInstance->GetInstanceMat()) ); 
		Vec3V newInitialPos = rage::Add( pEnvClothType->GetInitialPosition(),instOffset );
		pEnvCloth->SetInitialPosition( newInitialPos );
	}

	// restore FVF on the type drawable to dynamic
	for( int i = 0; i < MAX_VERT_BUFFERS; ++i)
	{
		grcVertexBuffer* vbInstance = ((grmGeometryQB*)&geom)->GetVertexBufferByIndex(i);
		if( vbInstance )
		{
			Assert( vbInstance->GetFvf() );
			grcFvf* fvfInstance = const_cast<grcFvf*>(vbInstance->GetFvf());
			Assert(fvfInstance);
			fvfInstance->SetDynamicOrder(true);
		}
	}
}

void clothManager::InitiateCharacterClothRemove(void* ptr)
{
	clothVariationData* pClothVarData = (clothVariationData*) ptr;
	Assert( pClothVarData );

	pgRef<characterCloth>& pCharCloth = pClothVarData->GetCloth();
	Assert( pCharCloth );

	characterClothController* pCharClothController = pCharCloth->GetClothController();
	Assert( pCharClothController );
	RemoveCharacterCloth( pCharClothController );

//	pClothVarData->DeleteClothDataNoBuffers();
	pClothVarData->DeleteClothData();

	DeallocateVarData( pClothVarData );
}

void clothManager::InitiateEnvironmentClothRemove(void* ptr)
{
	RemoveCloth( (environmentCloth*) ptr, NULL );
}

void clothManager::AddWidget( rage::clothController* pClothController, int verletType, bool isSubInst )
{
	Assert( pClothController );
	Assert( pClothBank );

	clothInstance* cInstance = (clothInstance*)pClothController->GetClothInstance();
	if( !cInstance )
		return;			// switch distance happened inside InstanceCloth

	char clothTag[128];
	const char* clothName = pClothController->GetName();
	Assert( clothName );		

#if NO_VERTS_IN_VERLET
	phVerletCloth* pCloth = pClothController->GetCloth( pClothController->GetLOD() );
	Assert( pCloth );
	phClothData& clothdata = pCloth->GetClothData();
	if( !PARAM_clothinfo.Get() )
	{
		formatf( clothTag, "%s   Verts: %d  Edges: %d  ", clothName, clothdata.GetNumVerts(), pCloth->GetNumEdges() );
	}
	else
	{
		formatf( clothTag, "%s   Verts: %d  Verts max: %d  Edges: %d  Edges max: %d  Address: 0x%x", clothName, clothdata.GetNumVerts(), clothdata.GetVertexCapacity(), pCloth->GetNumEdges(), pCloth->GetEdgeCapacity(), pClothController );
	}
#else
	if( !PARAM_clothinfo.Get() )
	{
		formatf( clothTag, "%s", clothName );
	}
	else
	{
		formatf( clothTag, "%s  Address: 0x%x", clothName, pClothController );
	}
#endif

	bkGroup* ragGroup = pClothBank->AddGroup( clothTag ,false );
	Assert( ragGroup );
	cInstance->m_RAGGroup = ragGroup;

	
	bkGroup* pTogglesGroup = ragGroup->AddGroup( "Toggles" ,false );			
	Assert( pTogglesGroup );

	pTogglesGroup->AddToggle("Spu Debug",  &cInstance->m_SpuDebug,	NullCB, "(PS3 only). Activate for this cloth." );
	pTogglesGroup->AddToggle("Debug Draw", &cInstance->m_DebugDraw, NullCB, "Activate drawing of edges and bend edges for this cloth." );
	pTogglesGroup->AddToggle("Drag Verts", &cInstance->m_DebugDragVerts, datCallback( MFA1( clothManager::DragVertsCB ), this, cInstance ), "Activate vertex/vertices dragging for this cloth." );
	pTogglesGroup->AddToggle("Pick Edges", &cInstance->m_DebugPickEdges, datCallback( MFA1( clothManager::ResetSelection1 ), this, cInstance ), "Activate edge(s) selection for this cloth." );
	pTogglesGroup->AddToggle("Pick Edges By Verts", &cInstance->m_DebugPickEdgesByVerts, datCallback( MFA1( clothManager::ResetSelection1 ), this, cInstance ), "Activate edge(s) selection for this cloth." );
	pTogglesGroup->AddToggle("Pick Instance Custom Edges",	&cInstance->m_DebugPickInstanceCustomEdges, datCallback( MFA1( clothManager::ResetSelection2 ), this, cInstance ), "Activate instanc custom edge(s) selection for this cloth." );	
	pTogglesGroup->AddToggle("Pick Custom Edges",	&cInstance->m_DebugPickCustomEdges, datCallback( MFA1( clothManager::ResetSelection3 ), this, cInstance ), "Activate type custom edge(s) selection for this cloth." );	
	pTogglesGroup->AddToggle("Edge Selection Info", &cInstance->m_DebugDrawEdgeInfo, NullCB, "Draw edge selection data." );
	pTogglesGroup->AddToggle("Draw Air Resistance Values",  &cInstance->m_DebugDrawAirResistance,	NullCB, "Draw air resistance values." );
	pTogglesGroup->AddToggle("Preview edge split", &cInstance->m_DebugPreviewEdgeSplit, datCallback( MFA1( clothManager::ResetSelection1 ), this, cInstance ), "" );

	Assert( !cInstance->m_DynamicPickVertsWidget );
	cInstance->m_DynamicPickVertsWidget = pTogglesGroup->AddToggle("Pick Verts", &cInstance->m_DebugPickVerts, datCallback( MFA1( clothManager::PickPinVertsCB ), this, cInstance ), "Activate vertex/vertices selection for this cloth." );
	Assert( cInstance->m_DynamicPickVertsWidget );
	Assert( !cInstance->m_DynamicPickPinVertsWidget );
	cInstance->m_DynamicPickPinVertsWidget = pTogglesGroup->AddToggle("Pick Pin Verts", &cInstance->m_DebugPickPinVerts, datCallback( MFA1( clothManager::PickPinVertsCB ), this, cInstance ), "Activate pinned vertex/vertices selection for this cloth." );
	Assert( cInstance->m_DynamicPickPinVertsWidget );
	
	Assert( !cInstance->m_TogglesGroup );
	cInstance->m_TogglesGroup = pTogglesGroup;

	char buttonTitle[128];
	if( verletType == Environment || verletType == Vehicle )
	{
		pTogglesGroup->AddToggle("Delete verts",  &cInstance->m_DebugDeleteVerts,	NullCB, "" );

		environmentCloth* pEnvCloth = (environmentCloth*)pClothController->GetOwner();
		Assert( pEnvCloth );
		if( !pEnvCloth->GetDrawable() )
		{
			// NOTE: this is most likely only cloth sim mesh
			ragGroup->AddButton( "Remove cloth", datCallback( MFA1(clothManager::InitiateEnvironmentClothRemove), this, (CallbackData)pEnvCloth, false ), "" );
#if CLOTH_INSTANCE_FROM_DATA
			ragGroup->AddButton( "Load topology", datCallback( MFA( clothInstance::LoadTopology), cInstance) );

			ragGroup->AddButton( "Attach to vehicle", datCallback( MFA( clothInstance::AttachToVehicle), cInstance) );
			ragGroup->AddButton( "Detach from vehicle", datCallback( MFA( clothInstance::DetachFromVehicle), cInstance) );			
#endif
		}
		else
		{
#if CLOTH_INSTANCE_FROM_DATA
			ragGroup->AddButton( "Save topology", datCallback( MFA( clothInstance::SaveTopology), cInstance) );
#endif
			ragGroup->AddButton( "Instance cloth", datCallback( MFA1(clothManager::InitiateClothInstance), this, (CallbackData)pEnvCloth, false ), "" );
			if( isSubInst )
			{
				ragGroup->AddButton( "Remove cloth", datCallback( MFA1(clothManager::InitiateEnvironmentClothRemove), this, (CallbackData)pEnvCloth, false ), "" );
			}

			rmcLodGroup* lodGroup = &pEnvCloth->GetDrawable()->GetLodGroup();
			int currentDrawable = lodGroup->GetCurrentLOD();

			if( lodGroup->ContainsLod(LOD_MED) || lodGroup->ContainsLod(LOD_LOW) || lodGroup->ContainsLod(LOD_VLOW) )
			{
				for( int i = 0; i < LOD_COUNT; ++i)
				{
					if( lodGroup->ContainsLod(i) && currentDrawable != i )
					{				
						formatf( buttonTitle, "Switch to drawable %d and to simulation %d (direct mapping)", i, i );
						ragGroup->AddButton( buttonTitle, datCallback( MFA1(environmentCloth::SwitchToDrawable), pEnvCloth, (CallbackData)&indices[i], false ), "[Work in progress]. Programmers only. Switch between cloth drawable LODs." );
					}
				}
			}

			const phMorphController* pMorphController = pClothController->GetMorphController();
			Assert( pMorphController );

			int currentLOD = pClothController->GetLOD();
			int tempIndices[LOD_COUNT];
			const int lodsCount = pMorphController->GetLODsCount( tempIndices );		

			for( int i = 0; i < lodsCount; ++i)
			{
				if( currentDrawable != currentLOD && currentDrawable == indices[ tempIndices[i] ] )
				{	
					formatf( buttonTitle, "Deform drawable %d by simulation %d (direct mapping)", currentDrawable, indices[ tempIndices[i] ] );
					ragGroup->AddButton( buttonTitle, datCallback( MFA1(environmentCloth::SwitchToLOD), pEnvCloth, (CallbackData)&indices[ tempIndices[i] ], false ), "[Work in progress]. Programmers only. Switch between cloth simulation LODs." );
				}
				else if( currentLOD > indices[i] && currentDrawable < indices[ tempIndices[i] ] )
				{
					formatf( buttonTitle, "Deform drawable %d by simulation %d (morphing)", currentDrawable, indices[i] );
					ragGroup->AddButton( buttonTitle, datCallback( MFA1(environmentCloth::SwitchToLOD), pEnvCloth, (CallbackData)&indices[ tempIndices[i] ], false ), "[Work in progress]. Programmers only. Switch between cloth simulation LODs." );
				}
				else if( currentLOD < indices[i] && currentDrawable < indices[ tempIndices[i] ] )
				{
					formatf( buttonTitle, "Deform drawable %d by simulation %d (morphing)", currentDrawable, indices[i] );
					ragGroup->AddButton( buttonTitle, datCallback( MFA1(environmentCloth::SwitchToLOD), pEnvCloth, (CallbackData)&indices[ tempIndices[i] ], false ), "[Work in progress]. Programmers only. Switch between cloth simulation LODs." );
				}
			}
		}

//		ragGroup->AddVector("Extra force", &cInstance->m_Tuning.m_ExtraForce,  -100000.0f, 100000.0f, 0.00001f );
		ragGroup->AddSlider("Verlet/Simulation Iterations", &pEnvCloth->GetCloth(pClothController->GetLOD() )->m_nIterations, 1, 10, 1, NullCB, "[Work in progress]. Cloth simulation iterations for this cloth." );

		ragGroup->AddToggle("Rest Interface", &cInstance->m_RestInterface, datCallback( MFA1( clothManager::ConnectToRest ), this, cInstance ), "Connect to Rest interface." );
#if !__SPU
		pEnvCloth->AddWidgets( ragGroup );
#endif

		if( const phBound* pCustomBound = pEnvCloth->GetCloth()->GetCustomBound() )
		{
			Assert( !cInstance->m_VirtualBoundGroup );
			formatf( buttonTitle, "Debug custom bound 0x%p", pCustomBound );
			CreateCustomBoundGroup( (const phBoundComposite*)pCustomBound, buttonTitle, cInstance );
		}

	}
	else if( verletType == Character )
	{
		characterCloth* pCharCloth = (characterCloth*)pClothController->GetOwner();
		Assert( pCharCloth );

		// TODO: character cloth doesn't support LODs yet
		int lodIndex = 0;

		phVerletCloth* pCloth = pClothController->GetCloth(lodIndex);
		Assert( pCloth );
#if NO_PIN_VERTS_IN_VERLET
		int numPinnedVerts = pCloth->GetClothData().GetNumPinVerts();
#else
		int numPinnedVerts = pCloth->GetPinCount();
#endif
		cInstance->m_DebugSelectedVertex = numPinnedVerts;

#if CLOTH_INSTANCE_FROM_DATA
		ragGroup->AddButton("Save all", datCallback( MFA1( clothManager::SaveCharacterCloth ), this, cInstance ), "..." );
#endif

		const int pinRadiusSetsCount = pClothController->GetBridge()->GetVertexBlendsCount(lodIndex);
		if( pinRadiusSetsCount > 1 )
		{
			ragGroup->AddCombo("PinRadius Sets", &cInstance->m_DebugCurrentPinRadiusSet, pinRadiusSetsCount/pCloth->GetNumVertices(), s_PinRadiusSets );
			ragGroup->AddButton("Set selected pin radius set", datCallback( MFA1( clothManager::SetSelectedPinRadiusSet ), this, cInstance ), "..." );
		}

		const int poseCount = pCharCloth->GetPosesVertsCount();
		if( poseCount > 1)
		{
			ragGroup->AddCombo("Poses", &cInstance->m_DebugCurrentPose, (poseCount/2)/pCloth->GetNumVertices(), s_PinRadiusSets );
			ragGroup->AddButton("Set pose", datCallback( MFA1( clothManager::SetPose ), this, cInstance ), "..." );
		}

		ragGroup->AddButton("Attach collision capsule", datCallback( MFA1( clothManager::AttachVirtualBound ), this, cInstance ), "..." );
		ragGroup->AddButton("Detach collision capsule", datCallback( MFA1( clothManager::DetachVirtualBound ), this, cInstance ), "..." );

		ragGroup->AddSlider("Scale Pin Radiuses", ((characterClothController*)pClothController)->GetPinningRadiusScalePtr(), 0.0f, 10.0f, 0.01f, NullCB, "[Work in progress]. Scale pin radiuses." );
		ragGroup->AddButton("Save Bridge To File", datCallback( MFA1( clothManager::SaveBridgeToFile ), this, cInstance ), "Save bridge data to file." );
		ragGroup->AddToggle("Rest Interface", &cInstance->m_RestInterface, datCallback( MFA1( clothManager::ConnectToRest ), this, cInstance ), "Connect to Rest interface." );
		ragGroup->AddToggle("Draw Pin Radiuses",  &cInstance->m_DebugDrawPinRadiuses,	NullCB, "Draw pin radius values." );		

#if __BANK
		ragGroup->AddToggle("Custom & Bend edges ON/OFF",  (bool*)&pCloth->m_Pad0[0], NullCB, "Switch ON/OFF simulation on bend edges and custom edges." );
#endif

		const phBoundComposite* customBound = pCharCloth->GetBoundComposite();
		if( customBound )
		{
			const int numBounds = customBound->GetNumBounds();
			Assert( numBounds <= MAX_NUM_CHAR_CLOTH_BOUNDS );

			const crSkeleton* pSkel = cInstance->GetSkeleton();
			Assert( pSkel );
			const crSkeletonData& skelData = pSkel->GetSkeletonData();

			for(int i = 0; i < numBounds; ++i)
			{
				phBound* boundPart = customBound->GetBound(i);
				Assert( boundPart );
				switch(boundPart->GetType())
				{
				case phBound::CAPSULE:
					{
						const int boneIdx = pCharCloth->GetBoneIndex(i);
						const crBoneData* pBoneData = skelData.GetBoneData(boneIdx);
						Assert( pBoneData );
						const char* pBoneName = pBoneData->GetName();
						Assert( pBoneName );

						formatf( buttonTitle, "Radius: %4.3f, Length: %4.3f,   BoneName: '%s'    BoneIndex:%d", ((phBoundCapsule*)boundPart)->GetRadius(), ((phBoundCapsule*)boundPart)->GetLength(), pBoneName, boneIdx );
						cInstance->m_DebugBoundButton[i].boneIdx	= boneIdx;
						cInstance->m_DebugBoundButton[i].bndButton	= ragGroup->AddButton( buttonTitle, datCallback( MFA1(clothInstance::SetBoundCapsuleData), cInstance, (CallbackData)&cInstance->m_DebugBoundButton[i], false), "Set the radius and length of this capsule bound." );
						cInstance->m_DebugBoundButton[i].boundPart	= boundPart;		
						cInstance->m_DebugBoundButton[i].instGroup	= cInstance->m_RAGGroup;
						break;
					}
				case phBound::SPHERE:
					USE_TAPERED_CAPSULE_ONLY(case phBound::TAPERED_CAPSULE:)
					case phBound::BOX:
					case phBound::GEOMETRY:
						USE_GEOMETRY_CURVED_ONLY(case phBound::GEOMETRY_CURVED:)
							USE_GRIDS_ONLY(case phBound::GRID:)
							USE_RIBBONS_ONLY(case phBound::RIBBON:)
							case phBound::BVH:
								USE_SURFACES_ONLY(case phBound::SURFACE:)
								case phBound::COMPOSITE:
								case phBound::TRIANGLE:
								case phBound::PLANE:
								default:
									break; // do nothing for now
				}
			}

#if !__SPU
			pCharCloth->AddWidgets( ragGroup );
#endif

			if( const phBound* pCustomBound = pClothController->GetCloth(0)->m_VirtualBound )
			{
				formatf( buttonTitle, "Toggle custom bound 0x%x debug draw", pCustomBound );
				CreateCustomBoundGroup( (const phBoundComposite*)pCustomBound, buttonTitle, cInstance );
			}
		}
	}
	else
	{
		// do something
	}

	ragGroup->AddButton( "Add custom edge", datCallback( MFA( clothManager::AddCustomEdge ), pClothManager) );
	ragGroup->AddButton( "Remove last custom edge", datCallback( MFA( clothManager::RemoveLastCustomEdge ), pClothManager) );
	ragGroup->AddButton( "Remove selected custom edge", datCallback( MFA( clothManager::RemoveCustomEdge ), pClothManager) );
}

void clothManager::SetCustomBoundCapsuleData(void* pBound)
{
	Assert( pBound );
	phBoundCapsule* pBoundCapsule = (phBoundCapsule*) pBound;	
	pBoundCapsule->SetCapsuleSize( m_DebugBoundRadius, m_DebugBoundLength );
#if USE_CAPSULE_EXTRA_EXTENTS
	pBoundCapsule->SetHalfHeight( m_DebugBoundHalfHeight );
#endif
}

bool clothManager::InitWidgets(void)
{	
	if( !pClothBank )
	{
		pClothBank = &BANKMGR.CreateBank("Cloth Management");
		Assert( pClothBank );
		pClothBankButton = pClothBank->AddButton( "Create Widgets", clothManager::AddBank );
	}

	return true;
}

void clothManager::AddBank()
{
	Assert( pClothManager );
	Assert( pClothBank );
	Assert( pClothBankButton );
	pClothBank->Remove( *(bkWidget*)pClothBankButton );
	pClothBankButton = NULL;

	ms_RAGBankGroup = pClothBank->AddGroup( "Draw Options", false );
	Assert( ms_RAGBankGroup );

#if __PFDRAW
	ms_RAGBankGroup->AddToggle( "DrawVerts",			PFD_ClothVerts.GetEnabledPtr(), NullCB, "Enable drawing of cloth verts." );
	ms_RAGBankGroup->AddToggle( "DrawEdges",			PFD_ClothEdges.GetEnabledPtr(), NullCB, "Enable drawing of cloth edges(cloth mesh)." );
	ms_RAGBankGroup->AddToggle( "DrawCustomEdges",		PFD_ClothCustomEdges.GetEnabledPtr(), NullCB, "Enable drawing of cloth custom edges." );
	ms_RAGBankGroup->AddToggle( "DrawInstanceCustomEdges",	PFD_ClothInstanceCustomEdges.GetEnabledPtr(), NullCB, "Enable drawing of cloth instance custom edges." );
	ms_RAGBankGroup->AddToggle( "DrawVertIndices",			PFD_ClothVertIndices.GetEnabledPtr(), NullCB, "Enable drawing of cloth vert indices." );
	ms_RAGBankGroup->AddToggle( "DrawOriginalVertIndices",	PFD_ClothOriginalVertIndices.GetEnabledPtr(), NullCB, "Enable drawing of cloth original vert indices." );
	ms_RAGBankGroup->AddToggle( "DrawPickedVerts",		PFD_PickVerts.GetEnabledPtr(), NullCB, "Enable drawing of cloth selected(picked) verts." );
	ms_RAGBankGroup->AddToggle( "DrawPickedEdges",		PFD_PickEdges.GetEnabledPtr(), NullCB, "Enable drawing of cloth selected(picked) edges." );
	ms_RAGBankGroup->AddToggle( "DrawVertexValues",		PFD_VertexValues.GetEnabledPtr(), NullCB, "Enable drawing of character cloth per vertex value (pin radiuses, air resistance, index mapping)." );
	ms_RAGBankGroup->AddToggle( "DrawCollisions",		PFD_VerletCollision.GetEnabledPtr(), NullCB, "Draw cloth collisions." );	
	ms_RAGBankGroup->AddToggle( "DrawVerletBounds",		PFD_VerletBounds.GetEnabledPtr(), NullCB, "Draw cloth bounds." );
	ms_RAGBankGroup->AddToggle( "DrawSpuDebug",			PFD_SpuDebug.GetEnabledPtr(), NullCB, "(PS3 only). Enable drawing of primitives(triangles/spheres/capsules/boxes) from SPU code (mostly for programmers)." );
	ms_RAGBankGroup->AddToggle( "DrawSkinnedMesh",		PFD_SkinnedMesh.GetEnabledPtr(), NullCB, "Enable drawing of vertices of character cloth in local space (at 0,0,0) (mostly for programmers)." );
	ms_RAGBankGroup->AddToggle( "DrawCustomBounds",		PFD_CustomBounds.GetEnabledPtr(), NullCB, "Enable drawing of custom bounds of env cloth." );
	ms_RAGBankGroup->AddToggle( "DrawDebugRecords",		PFD_DebugRecords.GetEnabledPtr(), NullCB, "Enable debug records draw.");
	ms_RAGBankGroup->AddToggle( "Clamp(Zero out) all forces", &pClothManager->ms_ClampAllForces, NullCB, "Clamp(Zero out) all forces" );
	ms_RAGBankGroup->AddToggle( "Enable Debug Alpha",	&pClothManager->ms_EnableAlpha, NullCB, "Enable debug alpha" );
	ms_RAGBankGroup->AddToggle( "Enable Vehicle Pick",	&pClothManager->ms_EnableVehiclePick, NullCB, "" );
	ms_RAGBankGroup->AddToggle( "Enable Ped Pick",		&pClothManager->ms_EnablePedPick, NullCB, "" );	
	ms_RAGBankGroup->AddToggle( "Flip Debug Alpha",		&pClothManager->ms_FlipAlpha, NullCB, "Flip debug alpha" );
	ms_RAGBankGroup->AddToggle( "Pause simulation",		&g_PauseClothSimulation, NullCB, "Pause entire cloth simulation. (game is not paused)" );
	ms_RAGBankGroup->AddToggle( "Show all stats",			&s_ShowStats, NullCB, "Show all cloth stats." );
	ms_RAGBankGroup->AddToggle( "Show active cloth only stats",	&s_ShowStatsActiveCloth, NullCB, "Show active cloth only stats." );	
	ms_RAGBankGroup->AddToggle( "Show cloth instance stats",	&s_ShowStatsInstanceCloth, NullCB, "Show cloth instance stats." );		

#if USE_CLOTH_GIZMO
	ms_RAGBankGroup->AddToggle( "DrawGizmo",			PFD_ClothGizmo.GetEnabledPtr(), datCallback( MFA(clothManager::EnableClothGizmo), pClothManager ), "" );
	ms_RAGBankGroup->AddButton( "Toggle Gizmo Mode", datCallback( MFA(clothManager::ToggleClothGizmoMode), pClothManager ), "" );
#endif

	ms_RAGBankGroup->AddToggle( "Toggle brush shape",	&fwClothDebug::sm_bBrushShape, NullCB, "" );

#endif // __PFDRAW

#if RECORDING_VERTS
	ms_RAGBankGroup->AddToggle( "Toggle recording", &g_EnableRecording, datCallback( MFA( clothManager::ToggleRecording ), pClothManager) );
	ms_RAGBankGroup->AddToggle( "Play recording frame by frame ", &g_PlayRecording, datCallback( MFA( clothManager::ToggleRecordingMode ), pClothManager), "NOT WORKING YET" );
#endif

	ms_RAGBankGroup->AddVector( "Draw offset", &s_DrawOffset, -100000.0f, 100000.0f, 0.00001f );
	ms_RAGBankGroup->AddVector( "Gismo position", &s_GismoPosition, -100000.0f, 100000.0f, 0.00001f );
	ms_RAGBankGroup->AddColor( "Gismo plane color", &fwClothGizmo::sm_GismoPlaneColor, 0.00001f );

	ms_RAGBankSliders = pClothBank->AddGroup( "Common", false );
	Assert( ms_RAGBankSliders );

	/*ms_PinRadiusSlider =*/ ms_RAGBankSliders->AddSlider( "Pin Radius", &clothManager::ms_PinRadius, 0.0f, 1.0f, 0.001f, NullCB, "(Character cloth only). Value used for blending between skinned and simulated position of cloth vertices. 1.0f: 100% simulated position, 0.0: 100% skinned position" );
	/*ms_VertexWeightSlider =*/ ms_RAGBankSliders->AddSlider( "Vertex Resistance", &clothManager::ms_VertexWeight, 0.0f, 1.0f, 0.0001f, NullCB, "Value used for vertex resistance to external forces (wind, impulses/bullet shots)." );
	/*ms_VertexWeightSlider =*/ ms_RAGBankSliders->AddSlider( "Cloth Weight", &clothManager::ms_ClothWeight, 0.0f, 10.0f, 0.0001f, NullCB, "Value used for cloth weight." );
	/*ms_VertexWeightSlider =*/ ms_RAGBankSliders->AddSlider( "Inflation Scale", &clothManager::ms_InflationScale, 0.0f, 10.0f, 0.0001f, NullCB, "Value used for inflation scale." );

	ms_RAGBankSliders->AddSlider( "Debug verts radius", &clothManager::ms_DebugSphereRadius, 0.0f, 5.0f, 0.0001f, NullCB, "Debug sphere radius" );	
	ms_RAGBankSliders->AddColor( "Debug verts color", &clothManager::ms_DebugVertsColor );

#if CLOTH_INSTANCE_FROM_DATA
	ms_RAGBankSliders->AddButton( "Instance environment cloth from data", datCallback( MFA(clothManager::InstanceEnvironmentClothFromData), pClothManager ), "" );
	ms_RAGBankSliders->AddButton( "Instance character cloth from data", datCallback( MFA(clothManager::InstanceCharacterClothFromData), pClothManager ), "" );
#endif
	
	ms_RAGBankSliders->AddText( "Instance name", clothManager::ms_ClothName, MAX_CHARS_CLOTH_NAME );
	ms_RAGBankSliders->AddCombo( "Instance component", &clothManager::ms_ComponentIndex, 12, s_ComponentTags );
	ms_RAGBankSliders->AddSlider( "Instance geometry index", &clothManager::ms_GeometryIndex, 0, 20, 1 );
	ms_RAGBankSliders->AddVector( "Instance offset", &s_InstanceOffset, -100000.0f, 100000.0f, 0.00001f );
	ms_RAGBankSliders->AddSlider( "Instance verts capacity", &clothManager::ms_VertsCapacity, 32.0f, 768.0f, 1.0f, NullCB, "" );
	ms_RAGBankSliders->AddSlider( "Instance edges capacity", &clothManager::ms_EdgesCapacity, 256.0f, 4096.0f, 1.0f, NullCB, "" );

	ms_RAGBankSliders->AddSlider( "Capsule radius", &pClothManager->m_DebugBoundRadius, 0, 5.0f, 0.001f, NullCB, "Value used for updating(through button 'Radius') the radius of capsules used in cloth custom collision bounds." );
	ms_RAGBankSliders->AddSlider( "Capsule length", &pClothManager->m_DebugBoundLength, 0, 20.0f, 0.001f, NullCB, "Value used for updating(through button 'Length') the length of capsules used in cloth custom collision bounds." );
#if USE_CAPSULE_EXTRA_EXTENTS
	ms_RAGBankSliders->AddSlider( "Capsule half height", &pClothManager->m_DebugBoundHalfHeight, 0, 20.0f, 0.001f, NullCB, "Value used for updating(through button 'HalfHeight') the half height of capsules used in cloth custom collision bounds." );
#endif

	ms_RAGBankSliders->AddVector( "Capsule position", &s_BoundPosition, -100000.0f, 100000.0f, 0.00001f );
	ms_RAGBankSliders->AddVector( "Capsule rotation", &s_BoundRotation, -100000.0f, 100000.0f, 0.00001f );

	ms_RAGBankSliders->AddSlider( "Edge weight", &clothManager::ms_DebugEdgeWeight, 0, 1.0f, 0.001f, NullCB, "Value used for updating edge(s) or bend edge(s) weight in edge or bend edge selection." );
	ms_RAGBankSliders->AddSlider( "Edge compression weight", &clothManager::ms_DebugEdgeCompWeight, 0, 1.0f, 0.001f, NullCB, "Value used for updating edge(s) or bend edge(s) compression weight in edge or bend edge selection." );
	ms_RAGBankSliders->AddSlider( "Edge length(squared)", &clothManager::ms_DebugEdgeLenSqr, 0, 1.0f, 0.001f, NullCB, "Value used for updating edge(s) or bend edge(s) length(squared) in edge or bend edge selection." );

	ms_RAGBankSliders->AddButton( "Set edge weight", datCallback( MFA(clothManager::SetEdgeWeight), pClothManager), "Set edge(s) or bend edge(s) weight in edge or bend edge selection." );
	ms_RAGBankSliders->AddButton( "Set edge compression weight", datCallback( MFA(clothManager::SetEdgeCompWeight), pClothManager), "Set edge(s) or bend edge(s) compression weight in edge or bend edge selection." );
	ms_RAGBankSliders->AddButton( "Set edge length(squared)", datCallback( MFA(clothManager::SetEdgeLenSqr), pClothManager), "Set edge(s) or bend edge(s) length(squared) in edge or bend edge selection." );

	ms_RAGBankSliders->AddButton( "Set vertex resistance", datCallback( MFA(clothManager::SetVertexResistance), pClothManager), "Set vertex resistance of selected vertices." );
	ms_RAGBankSliders->AddButton( "Set cloth weight", datCallback( MFA(clothManager::SetClothWeight), pClothManager), "Set cloth weight of the last selected cloth." );
	ms_RAGBankSliders->AddButton( "Set inflation scale", datCallback( MFA(clothManager::SetInflationScale), pClothManager), "Set inflation scale of the last selected vertices." );
	ms_RAGBankSliders->AddButton( "Set pin radius", datCallback( MFA(clothManager::SetPinRadius), pClothManager), "Set pin radius of selected vertices." );
	ms_RAGBankSliders->AddButton( "Clear vertex selection", datCallback( MFA(clothManager::ClearSelection), pClothManager), "De-select all vertices (Clear selection)." );

	ms_RAGBankSliders->AddVector( "Pin verts offset", &s_PinVertsOffset, -100000.0f, 100000.0f, 0.00001f );


	//////////// TODO: test test test - for prototype and debug purposes
	ms_RAGBankSliders->AddButton( "Set matrix index", datCallback( MFA(clothManager::SetMatrixIndices), pClothManager), "" );
	ms_RAGBankSliders->AddSlider( "Matrix index", &clothManager::ms_DebugMatrixIndex, 0, 10, 1 );

	////////////

	atDNode<clothInstance*>* activeListHead = pClothManager->GetHead();
	for (atDNode<clothInstance*> *node = activeListHead; node; node=node->GetNext())
	{
		pClothManager->SetupWidget( node->Data->GetClothController(), node->Data->GetType(), false );
	}
}


void clothManager::DestroyWidgets(void)
{
	if( pClothBank )
	{
		BANKMGR.DestroyBank( *pClothBank );
		pClothBank = 0;
	}
}

#endif //  USE_CLOTHMANAGER_BANK


#if USE_CLOTH_GIZMO

void clothManager::AttachBoundToGizmo(void* 
#if __PFDRAW
	ptr
#endif
	)
{
#if __PFDRAW	
	g_ClothGizmo.ResetGizmo();
	if( !PFD_ClothGizmo.GetEnabled() )
	{
		g_ClothGizmo.SetMatrix(NULL);		
	}
	else
	{
		Assert( ptr );
		g_ClothGizmo.SetMatrix( (Mat34V*)ptr );
	}
#endif
}

void clothManager::EnableClothGizmo()
{
#if __PFDRAW
	if( PFD_ClothGizmo.GetEnabled() )
	{
		g_ClothGizmo.ResetGizmo();
		g_ClothGizmo.SetGizmoMatrixPos( RC_VEC3V(s_GismoPosition) );
	}	
#endif
}


void clothManager::ToggleClothGizmoMode()
{
#if __PFDRAW
	if( PFD_ClothGizmo.GetEnabled() )
	{
		g_ClothGizmo.ToggleMode();
	}	
#endif
}

#endif // USE_CLOTH_GIZMO


void clothManager::SetDontDoClothOnSeparateThread(bool onOff)
{
	m_DontUseInstanceManager = onOff;
}

} // namespace rage

