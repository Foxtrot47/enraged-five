<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ropeDataManager" autoregister="true">
	<array name="m_TypeData" type="atArray">
		<pointer type="::rage::ropeData" policy="owner"/>
	</array>
</structdef>

</ParserSchema>