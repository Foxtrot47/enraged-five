#ifndef FW_ROPEMGR_H
#define FW_ROPEMGR_H

#include "fwtl/pool.h"
#include "atl/dlist.h"
#include "data/base.h"
#include "cloth/environmentcloth.h" 
#include "grrope/grroperender.h"
#include "parser/manager.h"
#include "phcore/phmath.h"
#include "physics/constraintdistance.h"
#include "physics/inst.h"
#include "physics/simulator.h"
#include "rmcore/drawable.h"
#include "system/interlocked.h"
#include "vectormath/legacyconvert.h"
#include "grcore/effect.h"

#include "fwscript/scripthandler.h"
#include "clothdebug.h"

namespace rage
{

#define		USE_PHHANDLE						1

#define		USE_ROPE_DEBUG						(__BANK && !__RESOURCECOMPILER)
#define		MAX_ATTACHED_TO_SINGLE_ROPE			5
#define		MAX_ATTACHEDPAIRS_TO_SINGLE_ROPE	8
#define		ROPE_LEN_ERROR_THRESHOLD			0.00001f

	// NOTE: rope definitions count in rope_data.xml, make sure the count match the definitions
#define		NUM_ROPE_TYPES_IN_XML				8

#define		UPDATE_BATCH						0//__PS3


#if USE_ROPE_DEBUG
class phMouseInput;
#endif

class verletTaskManager;


class ropeData : public datBase
{
public:
	ropeData();
	virtual const char* GetName() { return ""; }

	FW_REGISTER_CLASS_POOL(ropeData);

	int m_NumSections;
	float m_Radius;	

	atHashString m_DiffuseTextureName;
	atHashString m_NormalMapName;

	grcTextureHandle m_DiffuseTexture;
	grcTextureHandle m_NormalMap;

	float m_distanceMappingScale;
	Vector2 m_UVScale;

	float m_specularFresnel;
	float m_specularFalloff;
	float m_specularIntensity;
	float m_bumpiness;

	Color32 m_color;

	PAR_PARSABLE;
};

class ropeThin: public ropeData
{
public:

	const char* GetName() { return "ropeThin"; }
	// do something 

	PAR_PARSABLE;
};

class ropeWire6: public ropeData
{
public:
	const char* GetName() { return "ropeWire6"; }
	// do something 

	PAR_PARSABLE;
};

class ropeWire32: public ropeData
{
public:
	const char* GetName() { return "ropeWire32"; }
	// do something 

	PAR_PARSABLE;
};

class ropeMesh: public ropeData
{
public:
	const char* GetName() { return "ropeMesh"; }
	// do something 

	PAR_PARSABLE;
};

class ropeThinWire32: public ropeData
{
public:
	const char* GetName() { return "ropeThinWire32"; }
	// do something 

	PAR_PARSABLE;
};

class ropeReins: public ropeData
{
public:
	const char* GetName() { return "ropeReins"; }
	// do something 

	PAR_PARSABLE;
};

class ropeThin4: public ropeData
{
public:
	const char* GetName() { return "ropeThin4"; }
	// do something 

	PAR_PARSABLE;
};



enum enRopeTypes
{
	ROPE_INVALID		= -1,
	ROPE_THIN			= 0,
	ROPE_DEFAULT		= 1,
	ROPE_DEFAULT_32		= 2,
	ROPE_DEFAULT_WIRE	= 3,
	ROPE_THIN_WIRE_32	= 4,
	ROPE_REINS			= 5,
};

enum enRopeUpdateOrder
{
	ROPE_UPDATE_EARLY	= 0,
	ROPE_UPDATE_LATE	= 1,
	ROPE_UPDATE_WITHSIM	= 2,
};


struct ropeAttachment
{
	phConstraintHandle hConstraint;
	int vertexIndex;
	float distRatio;				// this is the percentage distance to the next vertex from the rope
	float constraintRate;			// meters(units) per second 
	bool enableMovement;
	bool markedForDetach;
	char padding[14];
};

struct ropeAttachmentBone
{
	Mat34V boneMat;
	Vec3V offset;
	int	vertexIndex;
	int boneIndex;
	crSkeleton* skel;
	bool markedForDetach;
	char padding[3];
};


class ropeInstance
{
	mutable rage::atDNode<ropeInstance*> m_Node;
	mutable rage::atDNode<phVerletCloth*> m_BatchNode ;

public:

#if __BANK
	bkGroup*	m_RAGGroup;
	bool		m_DebugInfo;
	void ToggleDebugInfo() { m_DebugInfo = !m_DebugInfo; }
#endif

#if !__SPU
	void Load(const char* fileName);
	void Save(const char* fileName);
#endif

#if __PFDRAW && !__RESOURCECOMPILER
	void ProfileDraw();
#endif

	ropeInstance();
	~ropeInstance();

	void Init( Vec3V_In pos, Vec3V_In rot, float length, float minLength, float maxLength, float lengthChangeRate, int rtype, bool pinned/* = true*/, bool ppuonly /*= false*/, int numSections /*= -1*/, bool lockFromFront /*= true*/, float timeMultiplier /*= 1.0f*/, bool breakable /*= false*/, int numIterations /*= DEFAULT_ROPE_ITERATIONS*/ );
	void Shutdown();
	void ShutdownPhysics();
	void UpdatePhysics(float timeStep);
	void UpdateArray(float timeStep);

	void DisableCollision(bool disable);

#if USE_CLOTH_DEBUG
	void DebugUpdate( const grcViewport* pViewport );
#endif

	void UpdateControlVertices();

	void Draw(const Matrix34 &camMtx, float lodScale);
	void ShadowDraw(const Matrix34 &camMtx);

	void InitPhys(phInst* pPhInst, Mat34V_In mtx);
	void SwapConstraint( phConstraintDistance::Params& params );
	void CopyConstraintParams( phConstraintDistance::Params& params );

	rmcRopeDrawable* GetDrawable() const { return m_Drawable; }
	void SetDrawable( rmcRopeDrawable* newDrawableObject ) { m_Drawable = newDrawableObject; }
	phInst* GetPhysInst() const { return m_PhysInst; }

	void SetPhysInstFlags( int typeFlags, int includeFlags );
	void GetPhysInstFlags( int& typeFlags, int& includeFlags ) const;

	rage::atDNode<ropeInstance*> *GetNode() const { return &m_Node; }
	rage::atDNode<ropeInstance*> &GetNodeRef() const { return m_Node; }
	rage::atDNode<phVerletCloth*> &GetBatchNodeRef() const { return m_BatchNode; }

	environmentCloth* GetEnvCloth() const { return m_EnvCloth; }
	int GetType() const { return m_Type; }

	Vec3V_Out GetWorldPositionA() const
	{
		Assert(m_EnvCloth);
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const phClothData& clothData = pCloth->GetClothData();
		return clothData.GetVertexPosition( pCloth->GetNumLockedEdgesFront() );
		//		return m_EnvCloth->GetVertexWorldPosition( m_EnvCloth->GetCloth()->GetNumLockedEdgesFront() );
	}

	Vec3V_Out GetWorldPositionB() const
	{
		Assert(m_EnvCloth);
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const phClothData& clothData = pCloth->GetClothData();
		return clothData.GetVertexPosition( m_EnvCloth->GetNumRopeVertices() + pCloth->GetNumLockedEdgesFront() - 1 );
		//		return m_EnvCloth->GetVertexWorldPosition( m_EnvCloth->GetNumRopeVertices() + m_EnvCloth->GetCloth()->GetNumLockedEdgesFront() - 1 );
	}

	Vec3V_Out GetWorldPosition( int vtxIndex ) const
	{
		Assert(m_EnvCloth);
		Assert( vtxIndex > -1 && vtxIndex < m_EnvCloth->GetNumRopeVertices() );
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const phClothData& clothData = pCloth->GetClothData();
		return clothData.GetVertexPosition( vtxIndex );
		//		return m_EnvCloth->GetVertexWorldPosition( vtxIndex );
	}

	Vec3V_Out GetRopeFirstVtxWorldPosition() const
	{
		Assert(m_EnvCloth);

		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const phClothData& clothData = pCloth->GetClothData();
		return clothData.GetVertexPosition( pCloth->GetNumLockedEdgesFront() );
		//		return m_EnvCloth->GetVertexWorldPosition( cloth->GetNumLockedEdgesFront() );
	}

	Vec3V_Out GetRopeLastVtxWorldPosition() const
	{
		Assert(m_EnvCloth);
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const phClothData& clothData = pCloth->GetClothData();
		const int totalNumVerts = pCloth->GetNumVertices();
		const int lockedBack = pCloth->GetNumLockedEdgesBack();
		const int lastVertexIndex = (totalNumVerts-lockedBack)-1;
		return clothData.GetVertexPosition( lastVertexIndex );
		//		return m_EnvCloth->GetVertexWorldPosition( lastVertexIndex );
	}

	bool IsAttached(const phInst* phinstToCheck) const
	{
		Assert( phinstToCheck );
		if( m_Constraint.IsValid() )
		{
			if( phConstraintBase* constraint = PHCONSTRAINT->GetTemporaryPointer(m_Constraint) )
				return (constraint->GetInstanceA()==phinstToCheck || constraint->GetInstanceB()==phinstToCheck);
		}
#if USE_PHHANDLE
		return (phinstToCheck == ((!m_AttachedTo.GetLevelIndexAndGenerationId()) ? NULL: PHLEVEL->GetInstance(m_AttachedTo)));
#else
		return (phinstToCheck == m_AttachedTo);
#endif
	}

	phConstraintBase* GetConstraint() const { return (m_Constraint.IsValid() ? PHCONSTRAINT->GetTemporaryPointer(m_Constraint): NULL); }

	phInst* GetInstanceA() const
	{
		phConstraintBase* pConstraint = GetConstraint();
		return (pConstraint ? pConstraint->GetInstanceA(): NULL);
	}

	phInst* GetInstanceB() const
	{
		phConstraintBase* pConstraint = GetConstraint();
		return (pConstraint ? pConstraint->GetInstanceB(): NULL);
	}

	phInst* GetAttachedTo() const 
	{ 
#if USE_PHHANDLE
		return (!m_AttachedTo.GetLevelIndexAndGenerationId()) ? NULL: PHLEVEL->GetInstance(m_AttachedTo);
#else
		return m_AttachedTo;
#endif
	}

	bool HasAttachmentsInArray() const { return m_Attachments.GetCount() > 0; }

	// Note: these are the functions called by the scripts

	void Pin( int idx, Vec3V_In pos )
	{
		Assert( m_EnvCloth );
		m_EnvCloth->ControlVertex( idx, pos );
	}

	void UnPin( int idx )
	{
		Assert( m_EnvCloth );
		Assert( m_EnvCloth->GetCloth() );
		m_EnvCloth->GetCloth()->DynamicUnpinVertex( idx );
	}

	ropeAttachment& AttachObjectsToConstraintArray( Vec3V_In worldPositionA, Vec3V_In worldPositionB, phInst* instanceA, phInst* instanceB, int componentA=0, int componentB=0, float length=0.0f, float constraintChangeRate = 1.0f, float allowedPenetration=0.0f, float massInvScaleA=1.0f, float massInvScaleB=1.0f, bool usePushes = true );
	void CheckForDetachFromConstraintArray();	

	void AttachObjects( Vec3V_In worldPositionA, Vec3V_In worldPositionB, phInst* instanceA, phInst* instanceB, int componentA=0, int componentB=0, float length=0.0f, float allowedPenetration=0.0f, float massInvScaleA=1.0f, float massInvScaleB=1.0f, bool usePushes = true, const crSkeleton* skelPartA = NULL, const crSkeleton* skelPartB = NULL, const char* boneNamePartA = NULL, const char* boneNamePartB = NULL );
	void AttachToObject( Vec3V_In worldPosition, phInst* inst, int componentPart, const crSkeleton* pSkel, const char* boneName );
	void DetachFromObject( const phInst* inst );

	int SetNewUniqueId();
	void SetParentUniqueId(int parentId) 
	{ 
		clothDebugf1("Set Rope parent unique ID:  %d ", parentId );
		Assertf(m_ParentUniqueId == 0, "RopeInstances can only have one parent at most, and this parent shouldn't be changed after being set!"); 
		m_ParentUniqueId = parentId;
	}
	int GetUniqueID() const { return m_UniqueID; }
	int GetParentUniqueID() const { return m_ParentUniqueId; }
	float GetLength() const	{ return m_Length; }
	float GetMaxLength() const { return m_MaxLength; }
	float GetDistanceBetweenEnds() const;

	float GetConstraintLengthBuffer() const { return m_ConstraintLengthBuffer; }
	void SetConstraintLengthBuffer(float fConstraintLengthBuffer) { m_ConstraintLengthBuffer = fConstraintLengthBuffer; }

	int GetVertsCount() const
	{
		Assert( m_EnvCloth );
		return m_EnvCloth->GetNumRopeVertices();
	}

	void StartWindingFront(float fLengthRate = -1.0f)
	{
		if(fLengthRate > 0.0f)
			m_LengthRate = fLengthRate;
		if( !m_RopeUnwindBack && !m_RopeUnwindFront && !m_RopeWindFront )		// don't wind and unwind at the same time, make sure last action is finished
			m_RopeWindFront = true;
	}

	void StartUnwindingFront(float fLengthRate = -1.0f)
	{
		if(fLengthRate > 0.0f)
			m_LengthRate = fLengthRate;
		if( !m_RopeUnwindBack && !m_RopeUnwindFront && !m_RopeWindFront )		// don't wind and unwind at the same time, make sure last action is finished
			m_RopeUnwindFront = true;
	}

	void StartUnwindingBack(float fLengthRate = -1.0f)
	{
		if(fLengthRate > 0.0f)
			m_LengthRate = fLengthRate;
		if( !m_RopeUnwindBack && !m_RopeUnwindFront && !m_RopeWindFront )		// don't wind and unwind at the same time, make sure last action is finished
			m_RopeUnwindBack = true;
	}

	bool GetIsUnwindingFront() const { return m_RopeUnwindFront; }
	bool GetIsUnwindingBack() const { return m_RopeUnwindBack; }
	bool GetIsWindingFront() const { return m_RopeWindFront; }

	void StopWindingFront() { m_RopeWindFront = false; }
	void StopUnwindingFront() { m_RopeUnwindFront = false; }
	void StopUnwindingBack() { m_RopeUnwindBack = false; }
	float GetLengthChangeRate() const { return m_LengthRate; }

	void SetSmoothReelIn(bool SmoothReelIn) { m_SmoothReelIn = SmoothReelIn; }

	void ForceLength( float ropeLen )
	{
		m_ExternalLength = Min( ropeLen, m_MaxLength );
	}

	void RopeWindFrontDirect(float length);

	bool IsPPUOnly() const 
	{ 
#if __PS3
		return m_PPUonly; 
#else
		return true;
#endif
	}

	void SetVertexComponentA( int vtxIdx ) { m_VertexComponentA = vtxIdx; }
	void SetVertexComponentB( int vtxIdx ) { m_VertexComponentB = vtxIdx; }

	void UnpinAllVerts()
	{
		Assert( m_EnvCloth );
		phVerletCloth* cloth = m_EnvCloth->GetCloth();
		Assert( cloth );
		cloth->DynamicUnpinAll();
	}

	float GetGravityScale() const { return m_GravityScale; }

	void SetGravityFactor(float fGravityFactor)
	{
		if(m_PhysInst)
		{
			phArchetype * pArchetype = m_PhysInst->GetArchetype();
			if(pArchetype)
			{
				pArchetype->SetGravityFactor(fGravityFactor);
			}
		}
	}

	void SetDrawEnabled(bool isDrawEnabled) { m_IsDrawEnabled = isDrawEnabled; }
	void SetDrawShadowEnabled(bool isDrawEnabled) { m_IsDrawShadowEnabled = isDrawEnabled; }
	void SetActive(bool isActive) 
	{ 
		if( isActive )
			sysInterlockedOr( &m_Flags, (u32)IS_ACTIVE ); 
		else
			sysInterlockedAnd( &m_Flags, (u32)~IS_ACTIVE ); 
	}
	void SetIsUpdating(bool isUpdating ) 
	{ 
		if( isUpdating )
			sysInterlockedOr( &m_Flags, (u32)IS_UPDATING ); 
		else
			sysInterlockedAnd( &m_Flags, (u32)~IS_UPDATING ); 
	}
	void SetSpecial(bool isSpecial) { m_bIsSpecial = isSpecial; }
	void SetIsTense(bool isTense) { m_bIsTense = isTense; }
	void SetIsResetLength(bool isResetLength) { m_bIsResetLength = isResetLength; }
	void SetActivateObjects(bool activateObjects) { m_bActivateObjects = activateObjects; }

	bool IsDrawEnabled() const { return m_IsDrawEnabled; }
	bool IsDrawShadowEnabled() const { return m_IsDrawShadowEnabled; }
	bool IsActive() const { return (m_Flags & IS_ACTIVE); }
	bool IsBreakable() const { return m_IsBreakable; }
	bool IsSpecial() const { return m_bIsSpecial; }
	bool IsTense() const { return m_bIsTense; }
	bool GetActivateObjects() { return m_bActivateObjects; }

	bool IsAttachedAtBothEnds() const { return m_AttachmentType == ATTACH_BOTH ? true: false; }

	float GetMinLength() const { return m_MinLength; }
	float GetMaxEdgeLength() const { return m_MaxEdgeLength; }

	void SetActivableInstA(bool bActivable) { m_bActivableInstA = bActivable; }
	void SetActivableInstB(bool bActivable) { m_bActivableInstB = bActivable; }

	void AttachVirtualBoundCapsule(const float capsuleRadius, const float capsuleLen, Mat34V_In boundMat, const int boundIdx /*, const Mat34V* transformMat */);
	void AttachVirtualBoundGeometry(const int numVerts, const u32 numPolys, const Vec3V* RESTRICT verts, const phPolygon::Index* RESTRICT triangles, const int boundIdx);
	void CreateVirtualBound(int numBounds, const Mat34V* transformMat);
	void DetachVirtualBound();

	void SetCustomCallback(datCallback cbFunc) { m_CustomCallback = cbFunc; }
	bool HasCustomCallback() const { return m_CustomCallback == NullCB ? false: true; }
	void RunCustomCallback() { m_CustomCallback.Call(); } 

	void SetUpdateOrder( enRopeUpdateOrder updateOrder ) { m_UpdateOrder = updateOrder; }
	int GetUpdateOrder() const { return m_UpdateOrder; }

	void ResetLength();
	void SetUpdateRefFrameVelocityOrder(int collidersOrder) 
	{ 
		clothDebugf1("[Rope] SetUpdateRefFrameVelocityOrder  RopeID: %d   CollidersOrder: %d", m_UniqueID, collidersOrder);
		m_UpdateRefFrameVelocity = collidersOrder; 
	}

	void SetScriptHandler(scriptHandler* pScriptHandler) { m_ScriptHandler = pScriptHandler; }
	scriptHandler* GetScriptHandler() const { return m_ScriptHandler; }

	void ConvertToSimple();

	void SetAmbScales(u32 natAmb, u32 artAmb) { m_natAmbScale = natAmb & 0xff; m_artAmbScale = artAmb & 0xff; }
	u32 GetNatAmbScale() const { return m_natAmbScale; }
	u32 GetArtAmbScale() const { return m_artAmbScale; }

#if USE_CLOTH_DEBUG
	int  PickVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool pin );
#endif

	void SetLocalOffset(const Vector3& offset) { m_LocalOffset = offset; }
	Vector3 GetLocalOffset() { return m_LocalOffset; }

private:
	void Draw(const Matrix34 &camMtx, ropeData *ropeData, float lodScale);
	void ShadowDraw(const Matrix34 &camMtx, const ropeData *ropeData);

	void GetLocalPosition(Vec3V_In worldPosition, const phInst& instance, int component, Vector3& localPosition) const;
	Vec3V_Out GetWorldPosition(Vec3V_In localPosition, const phInst& instance, int component = 0) const;
	void AdjustVerts( Vec3V_In ropeDirection );

protected:
	datCallback			m_CustomCallback;

	// Note: mandatory for the rope
	rmcRopeDrawable*	m_Drawable;
	phInst*				m_PhysInst;
	environmentCloth*	m_EnvCloth;	

	// Note: rope attached to two phinst objects
	phConstraintHandle	m_Constraint;
	int					m_ComponentA;
	int					m_ComponentB;
	int					m_VertexComponentA;
	int					m_VertexComponentB;
	const crSkeleton*	m_SkeletonPartA;
	const crSkeleton*	m_SkeletonPartB;
	int					m_BoneIndexPartA;
	int					m_BoneIndexPartB;
	const char*			m_BoneNamePartA;
	const char*			m_BoneNamePartB;
	Vec3V				m_LocalOffsetPartA;
	Vec3V				m_LocalOffsetPartB;

	// Note: rope attached to single object, no constraint
	Vector3				m_LocalOffset;
#if USE_PHHANDLE
	phHandle			m_AttachedTo;
#else
	phInst*				m_AttachedTo;
#endif
	// Note: rope attached to skeleton, no constraint
	const crSkeleton*	m_Skeleton;
	int					m_BoneIndex;


	// Note: used by rope attached to multiple objects through constraints
	atFixedArray<ropeAttachment, MAX_ATTACHED_TO_SINGLE_ROPE>	m_Attachments;	

	enum en_ATTACHMENT_TYPE
	{
		ATTACH_INVALID	= -1,
		ATTACH_BOTH		= 0,		// rope has attached objects at both ends
		ATTACH_FRONT,				// object is attached at front of the rope
		ATTACH_BACK,				// object is attached at back of the rope
		ATTACH_ARRAY,				// rope attached to multiple objects through constraints
	};	

	scriptHandler* m_ScriptHandler;
	int m_UpdateOrder;
	int m_UniqueID;					// used by scripts to identify the rope
	int	m_ParentUniqueId;			// should not be set to zero or NULL if it ever had a parent!  This allows script GUIDs to delete children safely!
	int m_Type;
	int m_UpdateRefFrameVelocity;				// 0 - don't set ref frame velocity , 1 - set vel A to B, 2 - set vel B to A
	en_ATTACHMENT_TYPE	m_AttachmentType;		// 0 - rope attached at both ends, 1 - attached at start of the rope, 2 - attached at end of the rope
	float m_Length;					// Length of the rope
	float m_ConstraintLengthBuffer;	// Lengthens (or shortens) the constraint relative to the rope length (Constraint length = m_Length + m_ConstraintLengthBuffer)
	float m_ExternalLength;
	float m_MaxLength;
	float m_MinLength;
	float m_LengthRate;
	float m_MaxEdgeLength;
	float m_GravityScale;			// default - 1.0f ... to make rope heavy set it higher than 1.0f ... to make it lighter set it between 0.0f ... 1.0f

	enum enFlags
	{
		IS_ACTIVE		= 1 << 0,
		IS_UPDATING		= 1 << 1,
	};

	volatile u32 m_Flags;

	u8 m_natAmbScale;
	u8 m_artAmbScale;

	bool  m_IsDrawEnabled : 1;
	bool  m_IsDrawShadowEnabled : 1;
	bool  m_IsBreakable : 1;
	bool  m_RopeUnwindFront : 1;
	bool  m_RopeUnwindBack : 1;
	bool  m_RopeWindFront : 1;	
	bool  m_SmoothReelIn : 1;
	bool  m_bActivableInstA : 1;
	bool  m_bActivableInstB : 1;
	bool  m_bIsSpecial : 1;
	bool  m_bIsTense : 1;
	bool  m_bIsResetLength : 1;
	bool  m_bActivateObjects : 1;

#if __PS3
	bool  m_PPUonly : 1;
#endif

public:
#if USE_CLOTH_DEBUG
	bool  m_DebugPickVerts;
#endif

#if __BANK
	static bool sm_lodColorize;
	static int sm_forceLodLevel;
#endif // __BANK

	static u32  sm_NextUniqueIdForScript;		// Next rope ID

	friend class ropeManager;
};

typedef void (*ropeCallback)(ropeInstance*);

class ropeManager : public datBase
{
public:

	ropeManager() : m_TasksManager(NULL) {}
	~ropeManager() {}

	void Init(verletTaskManager* tManager, grmShader *shader);
	void Shutdown();
	void Reset();
	void Flush();
	void Deactivate();
	void ProcessShutdownList();

	void UpdateBatch( float timeStep );
	void UpdateBatchPostSpu();

	void UpdateVerlet(ropeInstance* inst, float timeStep);
	bool StartTask(ropeInstance* inst);
	void PostUpdate();

	void UpdateViewport(const grcViewport& viewPort);	
	void UpdatePhysics(float elapsed, enRopeUpdateOrder updateOrderType);	
	void UpdateCloth(enRopeUpdateOrder updateOrderType);
	void UpdateControlVertices(enRopeUpdateOrder updateOrderType);
	void ResetLength(enRopeUpdateOrder updateOrderType);
	void SafeUpdate();
	void Draw(const Matrix34 &camMtx, float lodScale, ropeCallback shaderCallback = NULL);
	void ShadowDraw(const Matrix34 &camMtx);

	void Apply(ropeCallback callback);

	// Note: these are the functions called by the scripts

	// Helper function to calculate a rotation vector (axis*angle) from a direction vector
	static Vec3V_Out CalcRotAxisAngleFromDir(Vec3V_In vDir);

	ropeInstance* AddRope(Vec3V_In pos, Vec3V_In rot, float length, float minLength, float maxLength, float lengthChangeRate, int ropeType, int numSections, bool ppuOnly, bool lockFromFront, float weightScale, bool breakable, bool pinned, int numIterations = DEFAULT_ROPE_ITERATIONS )
	{
		return Add( pos, rot, length, minLength, maxLength, lengthChangeRate, ropeType, pinned, ppuOnly, numSections, lockFromFront, weightScale, breakable, numIterations );
	}

	void RemoveRope(ropeInstance* inst)	{ Remove( inst ); }
	int RemoveRope(int ropeId);
	int RemoveChildRope(int ropeId);

	void ConvertToSimple( const phInst* inst );

	bool HasChildRope(int ropeID);
	ropeInstance* FindRope(int ropeID);	
	ropeInstance* FindRope(phInst* phInstToCheck);

	void BreakRope( ropeInstance* rope, ropeInstance*& ropeA, ropeInstance*& ropeB, float lenA, float lenB, float minLen, int numSections );

	static inline grcEffectVar GetSpecularFresnelVar() { return sm_specularFresnelVar; }
	static inline grcEffectVar GetSpecularFalloffVar() { return sm_specularFalloffVar; }
	static inline grcEffectVar GetSpecularIntensityVar() { return sm_specularIntensityVar; }
	static inline grcEffectVar GetBumpinessVar() { return sm_bumpinessVar; }
	static inline grcEffectVar GetColorVar() { return sm_colorVar; }
	static inline grcEffectVar GetDiffuseTextureVar() { return sm_diffuseTextureVar; }
	static inline grcEffectVar GetNormalMapVar() { return sm_normalMapVar; }

	static __forceinline float GetLODDistance(int i) { FastAssert(i<3); return ms_lodLevels[i]; }
#if __BANK
	static void AddWidgets(bkBank* bank);
#endif 

	rage::atDList<phVerletCloth*>* GetBatchList() {	return &m_BatchList; }
	rage::atDList<ropeInstance*>* GetActiveList() { return &m_ActiveList; }
	atDNode<ropeInstance*>* GetHead()	{ return m_ActiveList.GetHead(); }

#if __PFDRAW && !__RESOURCECOMPILER
	void ProfileDraw();
#endif

protected:

	const grcViewport* m_ViewPort;

	atDNode<ropeInstance*>* AllocateVerlet();
	void ActivateVerlet(atDNode<ropeInstance*>* node);

	ropeInstance* Add( Vec3V_In pos, Vec3V_In rot, float length, float minLength, float maxLength, float lengthChangeRate, int ropeType, bool pinned, bool ppuOnly, int numSegments, bool lockFromFront, float timeMultiplier, bool breakable, int numIterations );
	void Remove( ropeInstance* inst );

	bool ShouldUpdate(ropeInstance* inst);

	static const int sm_MaxRopeCount = 128;
	verletTaskManager* m_TasksManager;
	int m_ActiveCount;
	int m_FreeCount;
	int m_ShutdownCount;

	rage::atDList<ropeInstance*> m_ActiveList;
	rage::atDList<ropeInstance*> m_FreeList;
	rage::atDList<ropeInstance*> m_ShutdownList;

	rage::atDList<phVerletCloth*> m_BatchList;

	static void UpdateTask(sysTaskParameters& p);

	static grcEffectVar sm_specularFresnelVar;
	static grcEffectVar sm_specularFalloffVar;
	static grcEffectVar sm_specularIntensityVar;
	static grcEffectVar sm_bumpinessVar;
	static grcEffectVar sm_colorVar;
	static grcEffectVar sm_diffuseTextureVar;
	static grcEffectVar sm_normalMapVar;

	static float ms_lodLevels[3];

#if USE_ROPE_DEBUG

	static bool ms_lodColorize;

public:

	void PickEdges( const Vector3& offset, phVerletCloth* cloth );
	static phMouseInput* ms_MouseInput;

#endif // USE_ROPE_DEBUG

};


} // namespace rage

#endif // FW_ROPEMGR_H
