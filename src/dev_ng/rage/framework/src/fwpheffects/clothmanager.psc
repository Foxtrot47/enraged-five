<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::clothInstance" cond="USE_CLOTHMANAGER_BANK">
	<array name="m_BindPose" type="atArray">
		<Vec3V/>
	</array>
  <array name="m_Indices" type="atArray">
    <int/>
  </array>
</structdef>

<enumdef type="::rage::phCapsuleBoundDef::enCollisionBoundDef">
  <enumval name="BOUND_DEF_IS_PLANE"/>
</enumdef>

<structdef type="::rage::phCapsuleBoundDef" simple="true">
  <string name="m_OwnerName" type="atString"/>
  <Vec4V name="m_Rotation"/>
  <Vec3V name="m_Position"/>
  <Vec3V name="m_Normal"/>
  <float name="m_CapsuleRadius"/>
  <float name="m_CapsuleLen"/>
  <float name="m_CapsuleHalfHeight"/>
  <float name="m_CapsuleHalfWidth"/>
  <bitset name="m_Flags" type="fixed32" numBits="32" values="::rage::phCapsuleBoundDef::enCollisionBoundDef"/>
</structdef>

<structdef type="::rage::phVerletClothCustomBounds" base="::rage::fwExtensionDef">
  <array name="m_CollisionData" type="atArray">
    <struct type="::rage::phCapsuleBoundDef"/>
  </array>
</structdef>  
  
</ParserSchema>