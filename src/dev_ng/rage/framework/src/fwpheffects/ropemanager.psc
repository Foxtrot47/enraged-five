<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ropeData">
	<int name="m_NumSections"/>
	<float name="m_Radius"/>
  <string name="m_DiffuseTextureName" type="atHashString"/>
  <string name="m_NormalMapName" type="atHashString"/>
	<float name="m_distanceMappingScale" init="1.0f"/>
	<Vector2 name="m_UVScale" init="1.0f,1.0f"/>
	<float name="m_specularFresnel" init="0.75f" min="0.0f" max="1.0f"/>
	<float name="m_specularFalloff" init="100.0f" min="0.0f" max="512.0f"/>
	<float name="m_specularIntensity" init="0.125f" min="0.0f" max="1.0f"/>
	<float name="m_bumpiness" init="1.0f" min="0.0f" max="10.0f"/>
	<Color32 name="m_color"/>
</structdef>

<structdef type="::rage::ropeThin" base="::rage::ropeData">

</structdef>

<structdef type="::rage::ropeWire6" base="::rage::ropeData">
	
</structdef>

<structdef type="::rage::ropeWire32" base="::rage::ropeData">

</structdef>

<structdef type="::rage::ropeMesh" base="::rage::ropeData">

</structdef>

<structdef type="::rage::ropeThinWire32" base="::rage::ropeData">

</structdef>

<structdef type="::rage::ropeReins" base="::rage::ropeData">

</structdef>

<structdef type="::rage::ropeThin4" base="::rage::ropeData">

</structdef>

</ParserSchema>