#include "ropemanager.h"
#include "grrope/grroperender.h"
#include "taskmanager.h"
#include "system/timemgr.h"
#include "grcore/allocscope.h"
#include "grprofile/pix.h"
#include "pheffects/cloth_verlet_spu.h"
#include "pheffects/wind.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundgeom.h"
#include "physics/constraintdistance.h"
#include "fwdrawlist/drawlist.h"
#include "fwscene/scan/Scan.h"
#include "fwscene/scan/ScanCascadeShadows.h"
#include "system/ipc.h"
#include "clothmanager.h"

#include "ropedatamanager.h"
#include "ropemanager_parser.h"

#include "system/task.h"

#if __BANK
#include "grcore/debugdraw.h"
#endif

#if USE_ROPE_DEBUG
	#include "input/keyboard.h"
	#include "input/keys.h"
	#include "input/pad.h"
	#include "input/mouse.h"
	#include "pheffects/mouseinput.h"
#endif

//#pragma optimize("", off)

DECLARE_TASK_INTERFACE(UpdateEnvironmentRope);

#define		USE_3LODS				0

namespace rage
{

const float	s_timeStepRope = 0.01666667f;

EXT_PFD_DECLARE_ITEM(Ropes);

#if __PS3
const	bool	sm_RopeSpuUpdate = true;
#endif

grcEffectVar ropeManager::sm_specularFresnelVar;
grcEffectVar ropeManager::sm_specularFalloffVar;
grcEffectVar ropeManager::sm_specularIntensityVar;
grcEffectVar ropeManager::sm_bumpinessVar;
grcEffectVar ropeManager::sm_colorVar;
grcEffectVar ropeManager::sm_diffuseTextureVar;
grcEffectVar ropeManager::sm_normalMapVar;

float ropeManager::ms_lodLevels[3] = { 1.0f, 100.0f, 200.0f };

#if __BANK
bool ropeInstance::sm_lodColorize = false;
int ropeInstance::sm_forceLodLevel = -1;
#endif // __BANK

u32 ropeInstance::sm_NextUniqueIdForScript = 1;		// Next rope ID

#if USE_ROPE_DEBUG
phMouseInput* ropeManager::ms_MouseInput = NULL;
#endif

sysCriticalSectionToken		s_LockShutdownList;


// ropeData

FW_INSTANTIATE_CLASS_POOL(ropeData, NUM_ROPE_TYPES_IN_XML, atHashString("ropeData",0x1bd2624f));

ropeData::ropeData() :	m_NumSections(6), m_Radius(0.03f), 
m_distanceMappingScale(1.0f), m_UVScale(1.0f,1.0f),
m_specularFresnel(0.75f), m_specularFalloff(100.0f), m_specularIntensity(0.125f), m_bumpiness(1.0f),
m_color(255,0,0)
{
	// Because grcTextureHandle's can appear in unions, they don't have ctor's.
	// However, explicitly assigning them to NULL works.
	m_DiffuseTexture = NULL;
	m_NormalMap = NULL;
}

// ropeInstance

void ropeInstance::Draw(const Matrix34 &camMtx, ropeData *ropeData, float lodScale)
{
	if( !m_IsDrawEnabled )
		return;

	Assertf(ropeData->m_DiffuseTexture != NULL, "Rope %s: No Textures loaded!", ropeData->GetName() );
	Assertf(ropeData->m_NormalMap != NULL, "Rope %s: No Textures loaded!", ropeData->GetName() );

	Vector2 uvScale = ropeData->m_UVScale;
	uvScale.y *= ropeData->m_distanceMappingScale*m_Length;
	
	RopeModel::Instance().SetUVScale(uvScale);

	grmShader *shader = RopeModel::Instance().GetShader();
	shader->SetVar(ropeManager::GetSpecularFresnelVar(),ropeData->m_specularFresnel);
	shader->SetVar(ropeManager::GetSpecularFalloffVar(),ropeData->m_specularFalloff);
	shader->SetVar(ropeManager::GetSpecularIntensityVar(),ropeData->m_specularIntensity);
	shader->SetVar(ropeManager::GetBumpinessVar(),ropeData->m_bumpiness);
	Vector4 color = VEC4V_TO_VECTOR4(ropeData->m_color.GetRGBA());
	shader->SetVar(ropeManager::GetColorVar(),color);
	shader->SetVar(ropeManager::GetDiffuseTextureVar(), ropeData->m_DiffuseTexture);
	shader->SetVar(ropeManager::GetNormalMapVar(), ropeData->m_NormalMap);

	int pass = (ropeData->m_NormalMap != NULL) ? 0 : ( (ropeData->m_DiffuseTexture != NULL) ? 1 : 2 );
	
	PIXBegin(0, "DrawRope");

	Assert( m_Drawable );
	float dist2toCamera = m_Drawable->GetDistToCamera2(camMtx);
	float ropeSize2 = m_Length*m_Length; //m_Drawable->GetRopeSize2();

	int lod = -1;
	if (dist2toCamera<ropeSize2)
	{
		// The camera is approximately within the rope's bounding sphere, so use high lod.
		lod = 0;
	}
	else
	{
		for (int lodIndex=0; lodIndex<2; lodIndex++)
		{
			float z = ropeManager::GetLODDistance(lodIndex) * lodScale;
			if (dist2toCamera < square(z))
			{
				lod = lodIndex;
				break;
			}
		}
	}
	
#if __BANK
	lod = ropeInstance::sm_forceLodLevel > -1 ? ropeInstance::sm_forceLodLevel : lod;
	
	if( ropeInstance::sm_lodColorize )
	{
		Vector4 lodColor = color;
		switch(lod)
		{
			case 0:
				lodColor = Vector4(1,0,0,1);
				break;
			case 1:
				lodColor = Vector4(0,1,0,1);
				break;
			case 2:
				lodColor = Vector4(0,0,1,1);
				break;
			case 3:
				lodColor = Vector4(1,0,1,1);
				break;
			default:
				break;
		}

		shader->SetVar(ropeManager::GetDiffuseTextureVar(), (grcTexture*)NULL);
		shader->SetVar(ropeManager::GetNormalMapVar(), (grcTexture*)NULL);
		shader->SetVar(ropeManager::GetColorVar(),lodColor);
		pass = 2;
	}	
#endif // __BANK
	if( lod != -1 )
	{
		m_Drawable->Draw(camMtx,0,lod,0,pass);
	}

	PIXEnd();
}

#if __PFDRAW && !__RESOURCECOMPILER

static bool bDrawBreakEdgeVert = false;
static bool bBreakEdge = false;
static bool bMergeEdges = false;
static int iEdgeToBreakIndex = 1;
static int iExtraVerts = 2;
static float fRatio = 0.5f;

void ropeInstance::ProfileDraw()
{
	if( PFD_Ropes.WillDraw() && !m_DebugPickVerts )
	{
		Assert( m_EnvCloth );
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const Vec3V* RESTRICT verts = pCloth->GetClothData().GetVertexPointer();
		DrawClothWireframe( pCloth->GetClothData().GetVertexPointer(), *pCloth, m_EnvCloth->GetClothController() );

		for( int i = 0; i < pCloth->GetNumVertices(); ++i)
		{
			grcDebugDraw::Sphere( verts[i], 0.05f, Color_red, false );

			char vertText[16];
			formatf( vertText, "%d", i );
			grcDebugDraw::Text( verts[i], Color_white, vertText );
		}

		
		if( bDrawBreakEdgeVert )
		{			
			static float step = 0.05f;
			if( ioMouse::HasWheel() )
			{
				 if(ioMouse::GetDZ() > 0)
				 {
					 fRatio += step;
				 }
				 else if(ioMouse::GetDZ() < 0)
				 {
					 fRatio -= step;
				 }
				 fRatio = Clamp( fRatio, 0.0f, 1.0f );
			}

			phEdgeData& edge = pCloth->GetEdge(iEdgeToBreakIndex);
			
			Vec3V v0 = verts[ edge.m_vertIndices[0] ];
			Vec3V v1 = verts[ edge.m_vertIndices[1] ];

			grcDebugDraw::Sphere( Add(v0, Scale(Subtract(v1, v0), ScalarVFromF32(fRatio))), 0.05f, Color_green, false );
		}
	}
}
#else

static int iExtraVerts = 0;

#endif // __PFDRAW

void ropeInstance::InitPhys(phInst* pPhInst, Mat34V_In mtx)
{		
	Assert( pPhInst );
	Assert( !m_PhysInst );
	m_PhysInst = pPhInst;
	m_PhysInst->SetMatrix( mtx );
}


ropeInstance::ropeInstance() 
	: m_Type(-1)	
	, m_AttachmentType(ATTACH_INVALID)
	, m_ExternalLength(-1.0f)
	, m_Drawable(NULL)
	, m_PhysInst(NULL)
#if USE_PHHANDLE
	, m_AttachedTo(0)
#else
	, m_AttachedTo(NULL)
#endif
	, m_Skeleton(NULL)
	, m_BoneIndex(-1)
	, m_SkeletonPartA(NULL)
	, m_SkeletonPartB(NULL)
	, m_BoneIndexPartA(-1)
	, m_BoneIndexPartB(-1)
	, m_BoneNamePartA(NULL)
	, m_BoneNamePartB(NULL)	
	, m_UniqueID(0)
	, m_ParentUniqueId(0)
	, m_ComponentA(0)
	, m_ComponentB(0)
	, m_VertexComponentA(-1)
	, m_VertexComponentB(-1)
	, m_GravityScale(1.0f)
	, m_RopeUnwindFront(false)
	, m_RopeUnwindBack(false)
	, m_RopeWindFront(false)
	, m_IsBreakable(false)	
	, m_SmoothReelIn(false)
	, m_bActivableInstA(true)
	, m_bActivableInstB(true)
	, m_bIsSpecial(false)
	, m_bIsTense(false)
	, m_UpdateRefFrameVelocity(0)
	, m_Flags(IS_ACTIVE)
	, m_bIsResetLength(false)
	, m_IsDrawEnabled(true)
	, m_IsDrawShadowEnabled(true)
	, m_bActivateObjects(true)
	, m_CustomCallback(NullCB)
	, m_UpdateOrder(ROPE_UPDATE_EARLY)
	, m_ScriptHandler(NULL)
#if __PS3
	, m_PPUonly(false)
#endif

#if USE_CLOTH_DEBUG
	, m_DebugPickVerts(false)
#endif

#if __BANK
	, m_RAGGroup(NULL)
	, m_DebugInfo(false)
#endif
{ 
	m_Node.Data			= this; 
	m_BatchNode.Data	= NULL;
	m_EnvCloth			= NULL;
	m_LocalOffset.Zero();
	
	m_natAmbScale = 255;
	m_artAmbScale = 0;
}

ropeInstance::~ropeInstance() 
{
	Assert( !m_Drawable );
	Assert( !m_PhysInst );
}

void ropeInstance::Init( Vec3V_In pos, Vec3V_In rot, float length, float minLength, float maxLength, float lengthChangeRate, int iRopeType, bool bPinned
	, bool PS3_ONLY(ppuOnly), int numSections, bool lockFromFront, float gravityScale, bool breakable, int numIterations )
{
	Assert( iRopeType != -1 );
	const ropeData* pRopeData = ropeDataManager::GetTypeDataByIndex( iRopeType );
	Assert( pRopeData );
	if( !pRopeData )
		return;

//sysMemUseMemoryBucket b(MEMBUCKET_CLOTH);


	m_EnvCloth = rage_new environmentCloth;
	Assert( m_EnvCloth );
	m_Type			= iRopeType;
	m_Length		= length;
	m_ConstraintLengthBuffer = 0.0f;
	m_MaxLength		= maxLength;
	m_MinLength		= minLength;
	m_LengthRate	= lengthChangeRate;
	m_GravityScale	= gravityScale;
	m_IsBreakable	= breakable;

#if __PS3
	m_PPUonly	= ppuOnly;
#endif

	Matrix34 ropePose( CreateRotatedMatrix( VEC3V_TO_VECTOR3(pos), VEC3V_TO_VECTOR3(rot) ) );

// NOTE: numSections/edges is loaded from ropeData.xml 
// rope of same length can be more saggy if has more edges	
	int numEdges = pRopeData->m_NumSections;
	if( numSections > 0 )
		numEdges = numSections;

	Assert( numEdges > 0 );
	m_MaxEdgeLength = maxLength/(float)numEdges;
	float coilingAngle = 0.0f;
	float coilAnglePerEdge = 0.0f;

// TODO/NOTE: optimize the vertex creation ?! - use the arc algorithm to avoid sin/cos- svetli
// implement creation of ropes with non-uniform edges 
	int numVertices = numEdges+1;
	Vector3* vertices = Alloca(Vector3,numVertices);
	Vector3 vertexPosition(ORIGIN);

// TODO: work in progress
/*
	static bool nonUniformEdges = false;
	if( nonUniformEdges )
	{
		int x = 0, k = 1;
		for (int i=1; i<numEdges; ++i)
		{
			k <<= 1;
			x += k;
		}

//		float edgeLen = maxLength / (float)x;
		float edgeLen = (float)(k) * maxLength / (float)(x) ;
		for (int vertexIndex=0; vertexIndex<numVertices; vertexIndex++)
		{
			ropePose.Transform(vertexPosition,vertices[vertexIndex]);			
			vertexPosition.x += edgeLen*cosf(coilingAngle);
			vertexPosition.y -= edgeLen*sinf(coilingAngle);
			coilingAngle += coilAnglePerEdge;
			//edgeLen *= 2.0f;
			edgeLen *= 0.5f;
		}
	}
	else
*/
	{
		for (int vertexIndex=0; vertexIndex<numVertices; vertexIndex++)
		{
			ropePose.Transform(vertexPosition,vertices[vertexIndex]);
			vertexPosition.x += m_MaxEdgeLength*cosf(coilingAngle);
			vertexPosition.y -= m_MaxEdgeLength*sinf(coilingAngle);
			coilingAngle += coilAnglePerEdge;
		}
	}

	// Initialize one-dimensional cloth.
	m_EnvCloth->AllocateClothController();
#if !__FINAL
	m_EnvCloth->GetClothController()->SetName("ROPE");
#endif

// TODO: NEW NEW NEW, still in test
	// I will assume verts are not 0, also rope is one dimensional mesh	
	int extraEdges = iExtraVerts;

	Assert( iExtraVerts >= 0 );
	Assert( extraEdges >= 0 );
	const int totalNumVerts = numVertices + iExtraVerts;

// TODO: create display mapping to the rope and use it when copying verts to drawable
#if ROPE_USE_BRIDGE
	clothBridgeSimGfx* pClothBridge = m_EnvCloth->GetClothController()->GetBridge();
	Assert( pClothBridge );
	pClothBridge->SetMeshVertCount( totalNumVerts, 0 );
	pClothBridge->CreateDisplayMapping( totalNumVerts, 0 );
#endif

	phVerletCloth* pCloth = m_EnvCloth->GetCloth();	
	Assert( pCloth );

	sysMemStartTemp();
	atArray<indxA2>	edgeToVertexIndices;
	edgeToVertexIndices.Resize(numVertices-1);
	sysMemEndTemp();

	pCloth->InitStrand( totalNumVerts, edgeToVertexIndices, (Vec3V*)vertices, numVertices, true, 0, phSimulator::GetGravityV() );
	pCloth->InitEdgeData( NULL, edgeToVertexIndices, extraEdges );

	phInst* pPhInst = m_EnvCloth->CreatePhysics( NULL, pRopeData->m_Radius );
	Assert( pPhInst );
// TODO: the rope bound is updated in createPhysics, the following bound compute is not needed ... most likely
	pCloth->ComputeBoundingVolume();

	// rope physics stuff
	Matrix34 matrix( CreateRotatedMatrix( VEC3V_TO_VECTOR3(pos), VEC3V_TO_VECTOR3(rot) ) );
	matrix.d.AddScaled(matrix.a, 0.5f * length );
	InitPhys( pPhInst, MATRIX34_TO_MAT34V(matrix) );	

	PHSIM->AddInactiveObject(pPhInst);
	PHSIM->AddInstBehavior(*m_EnvCloth->GetBehavior());

	int levelIndex = GetPhysInst()->GetLevelIndex();
	PHLEVEL->SetInactiveCollidesAgainstFixed(levelIndex,true);
	PHLEVEL->SetInactiveCollidesAgainstInactive(levelIndex,true);

	if( bPinned )
		pCloth->DynamicPinVertex(0); 

	rmcRopeDrawable* pRopeDrawable = rage_new rmcRopeDrawable( m_EnvCloth->GetCloth(), pRopeData->m_Radius );
	Assert( pRopeDrawable );
	SetDrawable(pRopeDrawable);
	m_EnvCloth->SetReferencedDrawable(pRopeDrawable,true);
	m_EnvCloth->CopyVerletDrawVertsForRope();

	SetGravityFactor( m_GravityScale );
//	pCloth->m_GravityFactor = m_GravityScale;

	pCloth->m_nIterations = (u16)numIterations;

	int numEdgesToLock = numEdges - (int)ceilf((length/m_MaxEdgeLength));
	if( numEdgesToLock > 0 )
	{
		if( lockFromFront )
			pCloth->SetNumLockedEdgesFront( numEdgesToLock );
		else
			pCloth->SetNumLockedEdgesBack( numEdgesToLock );
	}

	Assert( !m_RopeUnwindBack && !m_RopeUnwindFront && !m_RopeWindFront );

	sysMemStartTemp();
	edgeToVertexIndices.Reset();
	sysMemEndTemp();
}


void ropeInstance::SetPhysInstFlags( int typeFlags, int includeFlags )
{
	Assert( m_PhysInst );
	const int levelIndex = m_PhysInst->GetLevelIndex();
	PHLEVEL->SetInstanceTypeFlags( levelIndex, (u32)typeFlags );
	PHLEVEL->SetInstanceIncludeFlags( levelIndex, (u32)includeFlags );
}

void ropeInstance::GetPhysInstFlags( int& typeFlags, int& includeFlags ) const
{
	Assert( m_PhysInst );
	const int levelIndex = m_PhysInst->GetLevelIndex();
	typeFlags = (int)PHLEVEL->GetInstanceTypeFlags( levelIndex );
	includeFlags = (int)PHLEVEL->GetInstanceIncludeFlags( levelIndex );
}

void ropeInstance::ShutdownPhysics()
{
	clothDebugf1("[Rope] ShutdownPhysics");

	PHCONSTRAINT->Remove(m_Constraint);
	m_Constraint.Reset();

	SetActive( false );

	while( m_Flags & IS_UPDATING )
	{
		sysIpcSleep(1);
	};

	if( m_EnvCloth )
	{
		PHSIM->RemoveInstBehavior(*m_EnvCloth->GetBehavior());

		phInst* pRopePhInst = GetPhysInst();
		if( pRopePhInst )
		{
			PHSIM->DeleteObject(pRopePhInst->GetLevelIndex());
#if !USE_CLOTH_PHINST
			m_EnvCloth->DeletePhInst(pRopePhInst);
#endif
		}
	}
	

// NOTE: force detach if there is anything still attached ( on scene flush/ level change )
	for( int i = 0; i < m_Attachments.GetCount(); ++i )
	{
		ropeAttachment& rAttachment = m_Attachments[i];
		rAttachment.markedForDetach = true;
	}

	if ( m_EnvCloth )
		CheckForDetachFromConstraintArray();
}

void ropeInstance::Shutdown()
{
	clothDebugf1("[Rope] Shutdown");

	if( !m_EnvCloth )
	{
// rope has been shutdown already by invoking new game through the frontend ??
		Assert( !GetDrawable() );
		return;
	}

	delete m_EnvCloth;
	m_EnvCloth = NULL;

	SetDrawable(NULL);		// cloth owns the drawable. drawable is deleted in the envcloth destructor

	Assert( !m_Attachments.GetCount() );			// all attachements should have been detached already

	m_PhysInst = NULL;
	m_Type = -1;
	m_AttachmentType = ATTACH_INVALID;
	m_ComponentA = 0;
	m_ComponentB = 0;
	m_VertexComponentA = -1;
	m_VertexComponentB = -1;

#if USE_PHHANDLE
	m_AttachedTo.SetLevelIndexAndGenerationId(0);
#else
	m_AttachedTo		= NULL;
#endif
	m_Skeleton			= NULL;
	m_SkeletonPartA		= NULL;
	m_SkeletonPartB		= NULL;
	m_BoneIndexPartA	= -1;
	m_BoneIndexPartB	= -1;
	m_BoneNamePartA		= NULL;
	m_BoneNamePartB		= NULL;

	m_RopeUnwindFront	= false;
	m_RopeUnwindBack	= false;
	m_RopeWindFront		= false;
	m_IsBreakable		= false;	
	m_GravityScale		= 1.0f;
	m_Flags				= IS_ACTIVE;
	m_bIsResetLength	= false;
	m_IsDrawEnabled		= true;
	m_IsDrawShadowEnabled = true;
	m_UniqueID			= 0;
	m_ParentUniqueId	= 0;
	m_bIsSpecial		= false;
	m_bIsTense			= false;
	m_bActivateObjects  = true;
	m_UpdateRefFrameVelocity = 0;
	m_bActivableInstA	= true;
	m_bActivableInstB	= true;
	m_CustomCallback	= NullCB;
	m_UpdateOrder		= ROPE_UPDATE_EARLY;
	m_ScriptHandler		= NULL;

#if __BANK
	m_RAGGroup			= NULL;
#endif

#if __PS3
	m_PPUonly			= false;
#endif
}


#define UNUSED_LOCAL_PARAM(x)	x

#if USE_CLOTH_DEBUG

void ropeInstance::DebugUpdate( const grcViewport* pViewport )
{
	if( m_DebugPickVerts )
	{
		Assert( m_EnvCloth->GetClothController() );
		fwClothDebug::DragVerts( Vector3(Vector3::ZeroType), m_EnvCloth->GetClothController(), pViewport );
	}
	
//////////////////// WORK IN PROGRESS
	if(bMergeEdges)
	{
#if 0 // CLOTH_TEAR
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		const int numVerts = pCloth->GetNumVertices();

		clothController* pClothController = m_EnvCloth->GetClothController();
		Assert( pClothController );
		clothBridgeSimGfx* pClothBridge = pClothController->GetBridge();
		Assert( pClothBridge );

		u16* RESTRICT pDisplayMap = pClothBridge->GetClothDisplayMap(0);
		Assert( pDisplayMap );

		// re-order display map		
		for(int i = iDrawOrderVertexIndex; i < (numVerts-1); ++i )
		{
			pDisplayMap[i] = pDisplayMap[i+1];
		}		

		phEdgeData& edge0 = pCloth->GetEdge(iEdgeToMergeIndex0);
		phEdgeData& edge1 = pCloth->GetEdge(iEdgeToMergeIndex1);

		float edgeLen0 = sqrtf(edge0.m_EdgeLength2);
		float edgeLen1 = sqrtf(edge1.m_EdgeLength2);
		float edgeLen = edgeLen0 + edgeLen1;

		pCloth->MergeEdges( (u16)iEdgeToMergeIndex0, (u16)iEdgeToMergeIndex1, edgeLen*edgeLen );
#endif // 
		bMergeEdges = false;
	}

	if(bBreakEdge)
	{
#if 0 // CLOTH TEAR
		phVerletCloth* pCloth = m_EnvCloth->GetCloth();
		Assert( pCloth );
		phEdgeData& edge = pCloth->GetEdge(iEdgeToBreakIndex);

		const Vec3V* RESTRICT verts = pCloth->GetClothData().GetVertexPointer();
		Assert( verts );
		Vec3V v0 = verts[ edge.m_vertIndices[0] ];
		Vec3V v1 = verts[ edge.m_vertIndices[1] ];

		Vec3V v = Add(v0, Scale(Subtract(v1, v0), ScalarVFromF32(fRatio)));

		float edgeLen = sqrtf(edge.m_EdgeLength2);
		float edgeLenA = edgeLen*fRatio;
		float edgeLenB = edgeLen - edgeLenA;
		pCloth->BreakEdge( (u16)iEdgeToBreakIndex, edgeLenA * edgeLenA, edgeLenB * edgeLenB );

		const int numVerts = pCloth->GetNumVertices();

		pCloth->GetClothData().SetVertexPosition( numVerts-1, v );
		pCloth->GetClothData().SetVertexPrevPosition( numVerts-1, v );

		clothController* pClothController = m_EnvCloth->GetClothController();
		Assert( pClothController );
		clothBridgeSimGfx* pClothBridge = pClothController->GetBridge();
		Assert( pClothBridge );

		u16* RESTRICT pDisplayMap = pClothBridge->GetClothDisplayMap(0);
		Assert( pDisplayMap );

		// re-order display map
		int i = numVerts;
		for(; i > iEdgeToBreakIndex; --i )
		{
			pDisplayMap[i] = pDisplayMap[i-1];
		}
		pDisplayMap[i+1] = (u16)(numVerts-1);
#endif //

		bBreakEdge = false;
		bDrawBreakEdgeVert = false;
	}
//////////////
}


int ropeInstance::PickVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool /*pin*/ )
{
	fwClothDebug::SelectVerts( offset, pClothVertices, numVerts, numPinVerts, pViewport, false);

	const int totalSelectedVerts = (fwClothDebug::sm_SelectedVertsCount+fwClothDebug::sm_LastSelectedVertsCount);
	if( totalSelectedVerts )
	{
		fwClothDebug::DrawSelection(offset, pClothVertices, 0.06f, 0.05f);
	}
	return totalSelectedVerts;
}


#endif // USE_CLOTH_DEBUG


void ropeInstance::CheckForDetachFromConstraintArray()
{
	// Currently this is only called from UpdateArray so not doing any assert/null checks
	// on m_EnvCloth or the resulting cloth as it will already have been done
	phVerletCloth* cloth = m_EnvCloth->GetCloth();

	int count = m_Attachments.GetCount();
	for( int i = 0; i < count; ++i)
	{
		ropeAttachment& rAttachment = m_Attachments[i];
		if( rAttachment.markedForDetach )
		{
			PHCONSTRAINT->Remove( rAttachment.hConstraint );
			cloth->DynamicUnpinVertex( rAttachment.vertexIndex );

			rAttachment.distRatio		= -1.0f;
			rAttachment.vertexIndex		= -1;
			rAttachment.constraintRate	= -0.0001f;
			rAttachment.enableMovement	= false;
			rAttachment.markedForDetach = false;

			m_Attachments.Delete(i);
			--count;
			--i;
		}
	}

	if( !m_Attachments.GetCount() )
	{
		m_AttachmentType = ATTACH_FRONT;
	}
}

void ropeInstance::UpdateArray(float elapsed)
{
	if( m_AttachmentType == ATTACH_ARRAY )
	{
// TODO: work in progress ... related to rappelling
		static float compensateStretch = 1.0f;

		Assert( m_Attachments.GetCount() );
		Assert( m_EnvCloth );
		phVerletCloth* cloth = m_EnvCloth->GetCloth();
		Assert( cloth );
		const int totalNumVerts = cloth->GetNumVertices();
		const int lockedFront = cloth->GetNumLockedEdgesFront();
		const int lockedBack = cloth->GetNumLockedEdgesBack();

// TODO: enforce rope which is not locked at all ??
//		Assert( !lockedFront );									
//		Assert( !lockedBack );

		phConstraintBase* constraint = NULL;
		if( m_Attachments[0].hConstraint.IsValid() )
			constraint = PHCONSTRAINT->GetTemporaryPointer( m_Attachments[0].hConstraint );

		if( constraint )
		{
			phConstraintDistance* distanceConstraint = static_cast<phConstraintDistance*>(constraint);
			distanceConstraint->SetSeparationVelocity(0);
			phInst* instA = distanceConstraint->GetInstanceA();
			phInst* instB = distanceConstraint->GetInstanceB();
			if( instA && instB )
			{
				if( GetActivateObjects() )
				{
					PHSIM->ActivateObject( instA );
					PHSIM->ActivateObject( instB );
				}

				// TODO: for now the constraint is only unwinding	
				if( m_Attachments[0].enableMovement )
				{
					float constraintLenChange = elapsed * m_Attachments[0].constraintRate;
					bool reelingIn = constraintLenChange < 0.0f;
					// Smoothly change rope length if reeling out or if smoothReelIn is enabled
					distanceConstraint->SetMaxDistance( distanceConstraint->GetMaxDistance() + constraintLenChange );
					if(!reelingIn || m_SmoothReelIn)
					{
						distanceConstraint->SetSeparationVelocity(m_Attachments[0].constraintRate);
					}

					m_Attachments[0].distRatio += (constraintLenChange)/(m_MaxEdgeLength*compensateStretch);
					if( m_Attachments[0].distRatio > 1.0f )
					{
						if(m_Attachments[0].vertexIndex < (totalNumVerts-lockedBack-1))
						{
							m_Attachments[0].distRatio -= 1.0f;
							cloth->DynamicUnpinVertex( m_Attachments[0].vertexIndex );
							++m_Attachments[0].vertexIndex;
						}
						else
						{
							m_Attachments[0].distRatio = 1.0f;
						}

						// For now let the mark for detachment be handled on the game side (CR 10/23/12)
						/*if( m_Attachments[0].vertexIndex >= (totalNumVerts-lockedBack-1) )
						{
							m_Attachments[0].markedForDetach = true;
						}*/
					}
				}

				const int frontVertex = lockedFront;
				m_EnvCloth->ControlVertex( frontVertex, distanceConstraint->GetWorldPosA() );
			}
		}

		for( int i = 1; i < m_Attachments.GetCount(); ++i )
		{			
			if( m_Attachments[i].hConstraint.IsValid() )
				constraint = PHCONSTRAINT->GetTemporaryPointer( m_Attachments[i].hConstraint );

			if( constraint )
			{
				phConstraintDistance* distanceConstraint = static_cast<phConstraintDistance*>(constraint);
				distanceConstraint->SetSeparationVelocity(0);
				phInst* instA = distanceConstraint->GetInstanceA();
				phInst* instB = distanceConstraint->GetInstanceB();
				if( instA && instB )
				{
					if( GetActivateObjects() )
					{
						PHSIM->ActivateObject( distanceConstraint->GetInstanceA() );
						PHSIM->ActivateObject( distanceConstraint->GetInstanceB() );
					}

			// TODO: dup code , see above ... fix it
			// TODO: for now the constraint is only unwinding
					if( m_Attachments[i].enableMovement )
					{
						float constraintLenChange = elapsed * m_Attachments[i].constraintRate;
						bool reelingIn = constraintLenChange < 0;
						// Smoothly change rope length if reeling out or if smoothReelIn is enabled
						distanceConstraint->SetMaxDistance( distanceConstraint->GetMaxDistance() + constraintLenChange );
						if(!reelingIn || m_SmoothReelIn)
						{
							distanceConstraint->SetSeparationVelocity(m_Attachments[i].constraintRate);
						}
						m_Attachments[i].distRatio += (constraintLenChange)/(m_MaxEdgeLength*compensateStretch);
						if( m_Attachments[i].distRatio > 1.0f )
						{
							if(m_Attachments[i].vertexIndex < (totalNumVerts-lockedBack-1))
							{
								m_Attachments[i].distRatio -= 1.0f;
								++m_Attachments[i].vertexIndex;
							}
							else
							{
								m_Attachments[i].distRatio = 1.0f;
							}

							// For now let the mark for detachment be handled on the game side (CR 10/23/12)
							/*if( m_Attachments[i].vertexIndex >= (totalNumVerts-lockedBack-1) )
							{
								m_Attachments[i].markedForDetach = true;
							}*/
						} // end if
					}
				}
			}
		} // end for

		CheckForDetachFromConstraintArray();
	} // end if
}

void ropeInstance::DisableCollision(bool disable)
{
	int levelIndex = GetPhysInst()->GetLevelIndex();
	PHLEVEL->SetInactiveCollidesAgainstFixed(levelIndex,!disable);
	PHLEVEL->SetInactiveCollidesAgainstInactive(levelIndex,!disable);
}

void ropeInstance::ResetLength()
{
	if( IsActive() && m_bIsResetLength )
	{
		phConstraintDistance* distanceConstraint = m_Constraint.IsValid() ? static_cast<phConstraintDistance*>(PHCONSTRAINT->GetTemporaryPointer(m_Constraint)) : NULL;
		if( distanceConstraint )
		{
			if( m_AttachmentType == ATTACH_BOTH )
			{
				m_Length = Selectf( m_ExternalLength, m_ExternalLength, Mag( Subtract(distanceConstraint->GetWorldPosA(), distanceConstraint->GetWorldPosB() ) ).Getf() );
			}
		}
	}
}

float ropeInstance::GetDistanceBetweenEnds() const
{
	phConstraintDistance* distanceConstraint = m_Constraint.IsValid() ? static_cast<phConstraintDistance*>(PHCONSTRAINT->GetTemporaryPointer(m_Constraint)) : NULL;
	if( distanceConstraint )
	{
		if( m_AttachmentType == ATTACH_BOTH )
		{
			return Mag( Subtract(distanceConstraint->GetWorldPosA(), distanceConstraint->GetWorldPosB() ) ).Getf();
		}
	}
	return -1.0f;
}

void ropeInstance::UpdateControlVertices()
{
	if( !IsActive() )
		return;

	Assert( m_EnvCloth );
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	const int totalNumVerts = cloth->GetNumVertices();
	const int lockedFront = cloth->GetNumLockedEdgesFront();
	const int lockedBack = cloth->GetNumLockedEdgesBack();
	const int lastVertexIndex = (totalNumVerts-lockedBack)-1;

	phConstraintDistance* distanceConstraint = m_Constraint.IsValid() ? static_cast<phConstraintDistance*>(PHCONSTRAINT->GetTemporaryPointer(m_Constraint)) : NULL;
	if( distanceConstraint )
	{
		if( m_AttachmentType == ATTACH_BOTH )
		{
			int frontVertex = m_VertexComponentA > -1 ? m_VertexComponentA : lockedFront;
			int backVertex = m_VertexComponentB > -1 ? m_VertexComponentB : lastVertexIndex;

			if( m_SkeletonPartA )
			{
				Mat34V boneMat;
				Assert( m_BoneIndexPartA > -1 );
				m_SkeletonPartA->GetGlobalMtx(m_BoneIndexPartA, boneMat);
				Vec3V pos = boneMat.GetCol3() + m_LocalOffsetPartA;
				m_EnvCloth->ControlVertex( frontVertex, pos );			
			}
			else
				m_EnvCloth->ControlVertex( frontVertex, distanceConstraint->GetWorldPosA() );

			if( m_SkeletonPartB )
			{
				Mat34V boneMat;
				Assert( m_BoneIndexPartB > -1 );
				m_SkeletonPartB->GetGlobalMtx(m_BoneIndexPartB, boneMat);
				Vec3V pos = boneMat.GetCol3() + m_LocalOffsetPartB;
				m_EnvCloth->ControlVertex( backVertex, pos );			
			}
			else
				m_EnvCloth->ControlVertex( backVertex, distanceConstraint->GetWorldPosB() );
		}
	}
	else if( m_AttachmentType == ATTACH_FRONT || m_AttachmentType == ATTACH_BACK )
	{
#if USE_PHHANDLE
		phInst* pAttachedInst = ((!m_AttachedTo.GetLevelIndexAndGenerationId()) ? NULL: PHLEVEL->GetInstance( m_AttachedTo ));
#else
		phInst* pAttachedInst = m_AttachedTo;
#endif
		if( pAttachedInst )
		{
			Vec3V pos = GetWorldPosition( VECTOR3_TO_VEC3V(m_LocalOffset), *pAttachedInst, m_ComponentA );
			if( m_AttachmentType == ATTACH_FRONT )
				m_EnvCloth->ControlVertex( lockedFront, pos );
			else
				m_EnvCloth->ControlVertex( lastVertexIndex, pos );
		}
		else if( m_Skeleton )
		{
			Mat34V boneMat;
			Assert( m_BoneIndex > -1 );
			m_Skeleton->GetGlobalMtx(m_BoneIndex, boneMat);
			Vec3V pos = boneMat.GetCol3() + VECTOR3_TO_VEC3V(m_LocalOffset);
			if( m_AttachmentType == ATTACH_FRONT )
				m_EnvCloth->ControlVertex( lockedFront, pos );
			else
				m_EnvCloth->ControlVertex( lastVertexIndex, pos );
		}
	}
}

void ropeInstance::RopeWindFrontDirect(float length)
{
	Assert( m_EnvCloth );
	phVerletCloth* pCloth = m_EnvCloth->GetCloth();
	Assert( pCloth );

	m_Length = length;	
	m_ExternalLength = -1.0f;

	const int lockedFront = pCloth->GetNumLockedEdgesFront();

	phEdgeData& edge = pCloth->GetEdge( lockedFront );

	int numActiveEdges = pCloth->GetNumActiveEdges();
	float verletLength =  (float)numActiveEdges * m_MaxEdgeLength;
	float edgeLen = (verletLength - m_Length);
	if( edgeLen > m_MaxEdgeLength )
	{
		edge.m_EdgeLength2 = m_MaxEdgeLength * m_MaxEdgeLength;
		const int newLocked = lockedFront + 1;
		pCloth->DynamicPinVertex( newLocked );
		pCloth->SetNumLockedEdgesFront( newLocked );
	}
	else
	{
		const float edgeNewLen = m_MaxEdgeLength-edgeLen;
		edge.m_EdgeLength2 = edgeNewLen * edgeNewLen;
	}
}

void ropeInstance::UpdatePhysics(float /*UNUSED_LOCAL_PARAM*/ elapsed )
{
	if( !IsActive() )
		return;

// rappel rope
	UpdateArray(elapsed);



	Assert( m_EnvCloth );
	phVerletCloth* pCloth = m_EnvCloth->GetCloth();
	Assert( pCloth );
	const phClothData& clothData = pCloth->GetClothData();

 	const int totalNumVerts = pCloth->GetNumVertices();
	const int lockedFront = pCloth->GetNumLockedEdgesFront();
	const int lockedBack = pCloth->GetNumLockedEdgesBack();
	const int lastVertexIndex = (totalNumVerts-lockedBack)-1;	

	if( m_RopeWindFront )
	{
		float length = Selectf( m_ExternalLength, m_ExternalLength, m_Length - m_LengthRate * elapsed );	

		m_RopeWindFront = length > m_MinLength;
		if( !m_RopeWindFront )
		{
			length = m_MinLength;
		}
				
		RopeWindFrontDirect(length);
	}

	phConstraintDistance* distanceConstraint = m_Constraint.IsValid() ? static_cast<phConstraintDistance*>(PHCONSTRAINT->GetTemporaryPointer(m_Constraint)) : NULL;
   	if( distanceConstraint )
  	{		
		distanceConstraint->SetSeparationVelocity(0);
		if( m_AttachmentType == ATTACH_BOTH )
		{
			int frontVertex = m_VertexComponentA > -1 ? m_VertexComponentA : lockedFront;
			int backVertex = m_VertexComponentB > -1 ? m_VertexComponentB : lastVertexIndex;
			
			if( m_SkeletonPartA )
			{
				Mat34V boneMat;
				Assert( m_BoneIndexPartA > -1 );
				m_SkeletonPartA->GetGlobalMtx(m_BoneIndexPartA, boneMat);
				Vec3V pos = boneMat.GetCol3() + m_LocalOffsetPartA;
				m_EnvCloth->ControlVertex( frontVertex, pos );			
			}
			else
  				m_EnvCloth->ControlVertex( frontVertex /*lockedFront*/, distanceConstraint->GetWorldPosA() );


			if( m_SkeletonPartB )
			{
				Mat34V boneMat;
				Assert( m_BoneIndexPartB > -1 );
				m_SkeletonPartB->GetGlobalMtx(m_BoneIndexPartB, boneMat);
				Vec3V pos = boneMat.GetCol3() + m_LocalOffsetPartB;
				m_EnvCloth->ControlVertex( backVertex, pos );			
			}
			else
 				m_EnvCloth->ControlVertex( backVertex /*lastVertexIndex*/, distanceConstraint->GetWorldPosB() );


			if( m_UpdateRefFrameVelocity > 0 )
			{
				phInst* pInstA = distanceConstraint->GetInstanceA();				
				phInst* pInstB = distanceConstraint->GetInstanceB();
				if( pInstA && pInstB )
				{
					phCollider* pColliderA = PHSIM->GetCollider(pInstA);
					phCollider* pColliderB = PHSIM->GetCollider(pInstB);
					if( pColliderA && pColliderB )
					{
						if( m_UpdateRefFrameVelocity == 1 )
							pColliderA->SetReferenceFrameVelocity( pColliderB->GetVelocity().GetIntrin128() );
						else
							pColliderB->SetReferenceFrameVelocity( pColliderA->GetVelocity().GetIntrin128() );
					}
				}
			}
		}

		if( m_RopeUnwindBack )
		{
			Vec3V v0 = clothData.GetVertexPosition( lockedFront );
			Vec3V vL = clothData.GetVertexPosition( lastVertexIndex );

			AssertMsg( lastVertexIndex < pCloth->GetNumEdges(), "Rope edges most likely are not locked at correct end. Game might crash" );

			phEdgeData& edge = pCloth->GetEdge( lastVertexIndex );

			Vec3V temp = Subtract( vL, v0 );
			ScalarV verletLen2 = Dot(temp, temp);
			ScalarV ropeLen2 = ScalarVFromF32(m_Length*m_Length);
			if( IsLessThanAll(ropeLen2, verletLen2) != 0 )
			{
				edge.m_EdgeLength2 = m_MaxEdgeLength * m_MaxEdgeLength;
				m_Length += m_MaxEdgeLength;
				pCloth->DynamicUnpinVertex( lastVertexIndex );

				const int newLocked = pCloth->GetNumLockedEdgesBack() - 1;
				pCloth->SetNumLockedEdgesBack( newLocked );

				m_EnvCloth->ControlVertex( lastVertexIndex + 1, vL );
				if( !newLocked )
				{
					m_RopeUnwindBack = false;
				}
			}
			else
			{
				edge.m_EdgeLength2 = Subtract( ropeLen2, verletLen2 ).Getf();
			}
		}

// TODO: the following scope code is very similar to the one below for single rope - combine it - svetli
		if( m_RopeUnwindFront )
		{
			phEdgeData& edge = pCloth->GetEdge( lockedFront );

			m_Length = Selectf( m_ExternalLength, m_ExternalLength, m_Length + m_LengthRate * elapsed );	
			m_ExternalLength = -1.0f;

			int numActiveEdges = pCloth->GetNumActiveEdges();
			float verletLength =  (float)numActiveEdges * m_MaxEdgeLength;
			float edgeLen = (verletLength - m_Length);
// TODO: 2 float compares ... fix it - svetli
			if( edgeLen < 0.0f || m_Length > m_MaxLength )		// NOTE: rope should be always locked from front, unless is completely detached from both sides
			{
				edge.m_EdgeLength2 = m_MaxEdgeLength * m_MaxEdgeLength;

				if( !lockedFront )
				{
					m_Length = m_MaxLength;
					if( distanceConstraint->GetInstanceA() && m_bActivableInstA && GetActivateObjects() )
						PHSIM->ActivateObject( distanceConstraint->GetInstanceA() );  
					if( distanceConstraint->GetInstanceB() && m_bActivableInstB && GetActivateObjects() )
						PHSIM->ActivateObject( distanceConstraint->GetInstanceB() ); 
					distanceConstraint->SetMaxDistance( m_Length + m_ConstraintLengthBuffer );
					distanceConstraint->SetSeparationVelocity(m_LengthRate);
					m_RopeUnwindFront = false;
				}
				else
				{
					pCloth->DynamicUnpinVertex( lockedFront );
					const int newLocked = lockedFront - 1;
					Assert( newLocked > -1 );
					pCloth->SetNumLockedEdgesFront( newLocked );

					m_EnvCloth->ControlVertex( newLocked, clothData.GetVertexPosition( lockedFront ) );
				}
			}
			else
			{
				const float newEdgeLen = m_MaxEdgeLength - edgeLen;
				edge.m_EdgeLength2 = newEdgeLen * newEdgeLen;
			}
		}

		if( m_RopeWindFront || m_RopeUnwindFront || lockedBack || lockedFront )
		{
			if( distanceConstraint->GetInstanceA() && m_bActivableInstA && GetActivateObjects() )
				PHSIM->ActivateObject( distanceConstraint->GetInstanceA() );  
			if( distanceConstraint->GetInstanceB() && m_bActivableInstB && GetActivateObjects() )
				PHSIM->ActivateObject( distanceConstraint->GetInstanceB() ); 
			// Smoothly change rope length if reeling out or if smoothReelIn is enabled
			if( !m_RopeUnwindBack )
			{
				distanceConstraint->SetMaxDistance( m_Length + m_ConstraintLengthBuffer );
				if(m_RopeUnwindFront || m_SmoothReelIn)
				{
					distanceConstraint->SetSeparationVelocity(m_RopeUnwindFront ? m_LengthRate : -m_LengthRate);
				}
			}
		}

		const int totalNumVerts = pCloth->GetNumVertices();
		const int lockedFront = pCloth->GetNumLockedEdgesFront();
		const int lockedBack = pCloth->GetNumLockedEdgesBack();
		const int lastVertexIndex = (totalNumVerts-lockedBack)-1;

		//////////// TODO: fake tense rope				
		if( (lockedFront + 1) < lastVertexIndex )
		{
			Vec3V v0 = clothData.GetVertexPosition( lockedFront );
			Vec3V vL = clothData.GetVertexPosition( lastVertexIndex );
			Vec3V temp = Subtract( vL, v0 );
			ScalarV ropeLen = Mag(temp);
			ScalarV ratio = Scale( ropeLen, ScalarVFromF32( FPInvertFast(m_Length) ) );

			static float tenseThreshold = 0.5f;
			bool fakeTense = (m_bIsSpecial || 
					(	!(m_RopeWindFront || m_RopeUnwindFront || m_RopeUnwindBack)
					&& ( IsGreaterThanAll(ratio, ScalarVFromF32(tenseThreshold)) != 0 ) 
					)
				);

			if( m_bIsSpecial && !m_bIsTense )
			{
				Assert( m_MinLength > 0.1f );
				static float minLenTenseThreshold = 1.1f;
				float lenRatio =  m_Length * FPInvertFast(m_MinLength);
				if( lenRatio > minLenTenseThreshold )
				{
					Vec3V temp2 = temp;
					temp2.SetZ(V_ZERO);
					static float errAngleThreshold = 0.16f;
					ScalarV flattenV = Dot(temp2,temp2);
					fakeTense = (IsLessThanAll(flattenV, ScalarVFromF32(errAngleThreshold)) != 0)? false: true;
				}
			}

			if( fakeTense )
			{
				Vec3V tempAxis = NormalizeFast(temp);
				for( int i = (lockedFront + 1); i < lastVertexIndex; ++i )
				{
					Vec3V newPos = AddScaled( v0, tempAxis, ScalarVFromF32( m_MaxEdgeLength * (float)i) );
					(const_cast<phClothData*>(&clothData))->SetVertexPosition( i, newPos );
					(const_cast<phClothData*>(&clothData))->SetVertexPrevPosition( i, newPos );
				}
			}
		}
		////////////

  	}
	else if( m_AttachmentType != ATTACH_INVALID && m_AttachmentType != ATTACH_ARRAY )
	{
#if USE_PHHANDLE
		phInst* pAttachedInst = ((!m_AttachedTo.GetLevelIndexAndGenerationId()) ? NULL: PHLEVEL->GetInstance(m_AttachedTo));
#else
		phInst* pAttachedInst = m_AttachedTo;
#endif
		if( pAttachedInst && (m_AttachmentType > ATTACH_BOTH) )
		{
			Vec3V pos = GetWorldPosition( VECTOR3_TO_VEC3V(m_LocalOffset), *pAttachedInst, m_ComponentA );
			if( m_AttachmentType == ATTACH_FRONT )
				m_EnvCloth->ControlVertex( lockedFront, pos );
			else
				m_EnvCloth->ControlVertex( lastVertexIndex, pos );
		}
		else if( m_Skeleton )
		{
			Mat34V boneMat;
			Assert( m_BoneIndex > -1 );
			m_Skeleton->GetGlobalMtx(m_BoneIndex, boneMat);
			Vec3V pos = boneMat.GetCol3() + VECTOR3_TO_VEC3V(m_LocalOffset);
			if( m_AttachmentType == ATTACH_FRONT )
				m_EnvCloth->ControlVertex( lockedFront, pos );
			else
				m_EnvCloth->ControlVertex( lastVertexIndex, pos );
		}

		if( m_RopeUnwindFront )
		{
			phEdgeData& edge = pCloth->GetEdge( lockedFront );

			m_Length = Selectf( m_ExternalLength, m_ExternalLength, m_Length + m_LengthRate * elapsed );	
			m_ExternalLength = -1.0f;

			int numActiveEdges = pCloth->GetNumActiveEdges();
			float verletLength =  (float)numActiveEdges * m_MaxEdgeLength;
			float edgeLen = (verletLength - m_Length);
			// TODO: 2 float compares ... fix it - svetli
			if( edgeLen < 0.0f || m_Length > m_MaxLength )		// NOTE: rope should be always locked from front, unless is completely detached from both sides
			{
				edge.m_EdgeLength2 = m_MaxEdgeLength * m_MaxEdgeLength;

				if( !lockedFront )
				{
					m_Length = m_MaxLength;
					m_RopeUnwindFront = false;
				}
				else
				{
					pCloth->DynamicUnpinVertex( lockedFront );
					const int newLocked = lockedFront - 1;
					Assert( newLocked > -1 );
					pCloth->SetNumLockedEdgesFront( newLocked );

					m_EnvCloth->ControlVertex( newLocked, clothData.GetVertexPosition( lockedFront ) );
				}
			}
			else
			{
				const float newEdgeLen = m_MaxEdgeLength - edgeLen;
				edge.m_EdgeLength2 = newEdgeLen * newEdgeLen;
			}
		}

	}

#if __BANK
	if( m_DebugInfo )
	{				
		char buff[128];
		static float _x = 0.0f;
		static float _y = 0.0f;
		static float _z = -0.2f;
		Vec3V drawStep(_x, _y, _z);

		
		Vec3V v0 = clothData.GetVertexPosition( lockedFront ); //m_EnvCloth->GetVertexWorldPosition( lockedFront );
		sprintf(buff, "Max Length: %f", m_MaxLength );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Min Length: %f", m_MinLength );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Length: %f", m_Length );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Constraint Length Buffer: %f", m_ConstraintLengthBuffer );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Max Edge Length: %f", m_MaxEdgeLength );
		grcDebugDraw::Text(v0, Color_white, buff);
		
		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Length Rate: %f", m_LengthRate );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Locked edges front: %d", pCloth->GetNumLockedEdgesFront() );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Locked edges back: %d", pCloth->GetNumLockedEdgesBack() );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Vertex capacity: %d", clothData.GetVertexCapacity() );
		grcDebugDraw::Text(v0, Color_white, buff);

		v0 = Subtract( v0, drawStep );
		sprintf(buff, "Edge capacity: %d", pCloth->GetEdgeCapacity() );
		grcDebugDraw::Text(v0, Color_white, buff);
	}
#endif
}


Vec3V_Out ropeInstance::GetWorldPosition(Vec3V_In localPosition, const phInst& instance, int component) const
{
	Vec3V instLocalPos = localPosition;
	Assert(instance.GetArchetype() && instance.GetArchetype()->GetBound());
	const phBound& bound = *instance.GetArchetype()->GetBound();
	if (bound.GetType()==phBound::COMPOSITE)
	{
		const phBoundComposite& compositeBound = *static_cast<const phBoundComposite*>(&bound);
		Mat34V componentPose = compositeBound.GetCurrentMatrix(component);
		instLocalPos = Transform(componentPose,localPosition);
	}

	return Transform(instance.GetMatrix(),instLocalPos);
}


void ropeInstance::GetLocalPosition (Vec3V_In worldPosition, const phInst& instance, int component, Vector3& localPosition) const
{
	if( !instance.IsInLevel() )
		return;

	Mat34V tempMat = instance.GetMatrix();
	(reinterpret_cast<Matrix34*>(&tempMat))->UnTransform(VEC3V_TO_VECTOR3(worldPosition),localPosition);
	Assert(instance.GetArchetype() && instance.GetArchetype()->GetBound());
	const phBound& bound = *instance.GetArchetype()->GetBound();
	if (bound.GetType()==phBound::COMPOSITE)
	{
		// The object has a composite bound, so find the local position in the coordinates of the composite bound part.
		const phBoundComposite& compositeBound = *static_cast<const phBoundComposite*>(&bound);
		RCC_MATRIX34(compositeBound.GetCurrentMatrix(component)).UnTransform(localPosition);
	}
}

ropeAttachment& ropeInstance::AttachObjectsToConstraintArray( Vec3V_In worldPositionA, Vec3V_In worldPositionB, phInst* instanceA, phInst* instanceB, int componentPartA, int componentPartB, float constraintLength, float constraintChangeRate, float allowedPenetration, float massInvScaleA, float massInvScaleB, bool usePushes )
{
	clothDebugf1("[Rope] AttachObjectsToConstraintArray");

	Assert( instanceA );
	Assert( instanceA->GetArchetype() );
	Assert( instanceA->GetArchetype()->GetBound() );			// instance should have archetype with bound, if not the game will crash later
	Assert( componentPartA > -1 && componentPartA < 99 );		// sanity check, are there more than 99 parts ?
	Assert( componentPartB > -1 && componentPartB < 99 );		// sanity check, are there more than 99 parts ?
	if( !instanceB )
	{
		instanceB = m_PhysInst;				// if instanceB is NULL then attach the rope to its end
		Assert( componentPartB == 0 );		// attaching to the rope means 0 for component part B
	}

	Assert( instanceB->GetArchetype()->GetBound() );
	Assert( constraintLength < (m_Length+0.00001f) );

	m_ComponentA = componentPartA;
	m_ComponentB = componentPartB;

	phConstraintDistance::Params params;
	params.instanceA = instanceA;
	params.instanceB = instanceB;
	params.componentA = (u16)componentPartA;
	params.componentB = (u16)componentPartB;
	params.worldAnchorA = worldPositionA;
	params.worldAnchorB = worldPositionB;
	params.minDistance = m_MinLength;
	params.maxDistance = constraintLength;
	params.allowedPenetration = allowedPenetration;

	params.usePushes = usePushes;

	params.massInvScaleA = massInvScaleA;
	params.massInvScaleB = massInvScaleB;

	Assert(		(!m_Attachments.GetCount() && m_AttachmentType == ATTACH_FRONT) 
			||	( m_Attachments.GetCount() && m_AttachmentType == ATTACH_ARRAY) );
	m_AttachmentType = ATTACH_ARRAY;

// TODO: add check which will search through the attachments and either sort or assert - svetli

	ropeAttachment& attachment = m_Attachments.Append();


	attachment.hConstraint = PHCONSTRAINT->Insert(params);

	float attachedVtx = constraintLength/m_MaxEdgeLength;
	float closestCeilVtx = ceilf( attachedVtx );
	float closestFloorVtx = floorf( attachedVtx );

	int vtxIndex = -1;
	float distRatio = -1.0f;
	if( (closestCeilVtx - attachedVtx) < 0.0001f )
	{
		vtxIndex = (int)(closestCeilVtx); // NOTE: the attached pos is very close to a vertex from the rope
		distRatio = 0.0f;
	} 
	else if( (attachedVtx - closestFloorVtx) < 0.0001f )
	{
		vtxIndex = (int)(closestFloorVtx); // NOTE: the attached pos is very close to a vertex from the rope
		distRatio = 0.0f;
	}
	else
	{
		vtxIndex = (int)closestFloorVtx;
		distRatio = (constraintLength - closestFloorVtx*m_MaxEdgeLength)/m_MaxEdgeLength;
	}

	Assert( vtxIndex != -1 );
	Assert( distRatio > -0.0001f );
	Assert( constraintChangeRate > -0.0001f );

	attachment.vertexIndex		= vtxIndex;
	attachment.distRatio		= distRatio;
	attachment.constraintRate	= constraintChangeRate;
	attachment.enableMovement	= false;
	attachment.markedForDetach	= false;

	return attachment;
}

void ropeInstance::CopyConstraintParams( phConstraintDistance::Params& params )
{
	Assert( m_AttachmentType == ATTACH_BOTH );

	Assert( m_Constraint.IsValid() );
	if( m_Constraint.IsValid() )
	{
		phConstraintBase* oldConstraint = PHCONSTRAINT->GetTemporaryPointer(m_Constraint);
		Assert( oldConstraint );
		if( oldConstraint )
		{
			phConstraintDistance* distanceConstraint = static_cast<phConstraintDistance*>(oldConstraint);

			params.instanceA = oldConstraint->GetInstanceA();
			params.instanceB = oldConstraint->GetInstanceB();
			params.componentA = oldConstraint->GetComponentA();
			params.componentB = oldConstraint->GetComponentB();
			params.worldAnchorA = distanceConstraint->GetWorldPosA();
			params.worldAnchorB = distanceConstraint->GetWorldPosB();
			params.massInvScaleA = oldConstraint->GetMassInvScaleA();
			params.massInvScaleB = oldConstraint->GetMassInvScaleB();
			params.minDistance = m_MinLength;
			params.maxDistance = distanceConstraint->GetMaxDistance();
			params.allowedPenetration = distanceConstraint->GetAllowedPenetration();
			params.usePushes = oldConstraint->GetUsePushes();
		}
	}
}

void ropeInstance::SwapConstraint( phConstraintDistance::Params& params )
{
	clothDebugf1("[Rope] SwapConstraint");

// NOTE: for now swap phinst only when rope is attached at both ends
	Assert( m_AttachmentType == ATTACH_BOTH );
	PHCONSTRAINT->Remove(m_Constraint);

	m_Constraint.Reset();
	m_Constraint = PHCONSTRAINT->Insert(params);
}


// TODO: split the function in 2, one only for using bones + local offsets and another for using worldpositions
// also fix m_LocalOffsetPartA,...B to be in local space of the bone ... i.e. localoffsetA..B should be transformed every time those are used,  now those are in world space

void ropeInstance::AttachObjects( Vec3V_In worldPositionA, Vec3V_In worldPositionB, phInst* instanceA, phInst* instanceB, int componentPartA, int componentPartB, float constraintLength, float allowedPenetration, float massInvScaleA, float massInvScaleB, bool usePushes
	 , const crSkeleton* pSkelPartA, const crSkeleton* pSkelPartB, const char* boneNamePartA, const char* boneNamePartB )
{
	clothDebugf1("[Rope] AttachObjects");

	Assert( instanceA );
	Assert( instanceA->GetArchetype() );
	Assert( instanceA->GetArchetype()->GetBound() );			// instance should have archetype with bound, if not the game will crash later
	Assert( componentPartA > -1 && componentPartA < 99 );		// sanity check, are there more than 99 parts ?
	Assert( componentPartB > -1 && componentPartB < 99 );		// sanity check, are there more than 99 parts ?
	if( !instanceB )
	{
		instanceB = m_PhysInst;				// if instanceB is NULL then attach the rope to its end
		Assert( componentPartB == 0 );		// attaching to the rope means 0 for component part B
	}

	Assert( instanceB->GetArchetype()->GetBound() );

	m_ComponentA = componentPartA;
	m_ComponentB = componentPartB;

	phConstraintDistance::Params params;
	params.instanceA = instanceA;
	params.instanceB = instanceB;
	params.componentA = (u16)componentPartA;
	params.componentB = (u16)componentPartB;
	params.worldAnchorA = worldPositionA;
	params.worldAnchorB = worldPositionB;
	params.minDistance = m_MinLength;
	params.maxDistance = constraintLength;
	params.allowedPenetration = allowedPenetration;

    params.usePushes = usePushes;

    params.massInvScaleA = massInvScaleA;
    params.massInvScaleB = massInvScaleB;

	if( pSkelPartA )
	{
		Assert( boneNamePartA );
		const crSkeletonData& skelData = pSkelPartA->GetSkeletonData();
		const crBoneData* boneData = skelData.FindBoneData( boneNamePartA );
		Assertf( boneData, "There is no bone with name: %s in the skeleton!", boneNamePartA );
		if( boneData )
		{
			m_BoneIndexPartA = boneData->GetIndex();
			m_SkeletonPartA	= pSkelPartA;
			m_BoneNamePartA = boneNamePartA;

			Mat34V boneMat;
			Assert( m_BoneIndexPartA > -1 );
			m_SkeletonPartA->GetGlobalMtx(m_BoneIndexPartA, boneMat);
			params.worldAnchorA = worldPositionA + boneMat.GetCol3();
			m_LocalOffsetPartA = worldPositionA;
		}
	}
	if( pSkelPartB )
	{
		Assert( boneNamePartB );
		const crSkeletonData& skelData = pSkelPartB->GetSkeletonData();
		const crBoneData* boneData = skelData.FindBoneData( boneNamePartB );
		Assertf( boneData, "There is no bone with name: %s in the skeleton!", boneNamePartB );
		if( boneData )
		{
			m_BoneIndexPartB = boneData->GetIndex();
			m_SkeletonPartB	= pSkelPartB;
			m_BoneNamePartB = boneNamePartB;

			Mat34V boneMat;
			Assert( m_BoneIndexPartB > -1 );
			m_SkeletonPartB->GetGlobalMtx(m_BoneIndexPartB, boneMat);
			params.worldAnchorB = worldPositionB + boneMat.GetCol3();
			m_LocalOffsetPartB = worldPositionB;
		}
	}

	m_AttachmentType = ATTACH_BOTH;

	if(m_Constraint.IsValid())
	{
		SwapConstraint(params);
	}
	else
	{
		m_Constraint = PHCONSTRAINT->Insert(params);
	}

	if( GetActivateObjects() )
	{
		PHSIM->ActivateObject( instanceA );
		PHSIM->ActivateObject( instanceB );
	}
}


void ropeInstance::AttachToObject( Vec3V_In worldPosition, phInst* inst, int componentPart, const crSkeleton* pSkel, const char* boneName )
{
	clothDebugf1("[Rope] AttachToObject");
	
	m_AttachmentType = ATTACH_FRONT;	// when attaching to single object always attach from FRONT of the rope
	if( pSkel )
	{
		Assert( boneName );
		const crSkeletonData& skelData = pSkel->GetSkeletonData();
		const crBoneData* boneData = skelData.FindBoneData( boneName );
		Assertf( boneData, "There is no bone with name: %s in the skeleton!", boneName );
		if( boneData )
		{
			m_BoneIndex	= boneData->GetIndex();
			m_Skeleton	= pSkel;
			m_LocalOffset = VEC3V_TO_VECTOR3(worldPosition);
		}
	}
	else
	{
		Assert( inst );
#if USE_PHHANDLE
		m_AttachedTo = PHLEVEL->GetHandle(inst);
#else
		m_AttachedTo = inst;
#endif
		Assert( componentPart > -1 && componentPart < 99 );		// sanity check, are there more than 99 parts ?
		m_ComponentA = componentPart;
		GetLocalPosition( worldPosition, *inst, componentPart, m_LocalOffset );
	}
}

void ropeInstance::ConvertToSimple()
{
	if( m_ParentUniqueId > 0 )
	{
		m_UniqueID = m_ParentUniqueId;
		m_ParentUniqueId = 0;
	}

	phInst* pAttachedInst = ((!m_AttachedTo.GetLevelIndexAndGenerationId()) ? NULL: PHLEVEL->GetInstance( m_AttachedTo ));
	if( pAttachedInst )
	{
		Assert( m_EnvCloth );
		phVerletCloth* cloth = m_EnvCloth->GetCloth();
		Assert( cloth );
		const int totalNumVerts = cloth->GetNumVertices();
		const int lockedFront = cloth->GetNumLockedEdgesFront();
		const int lockedBack = cloth->GetNumLockedEdgesBack();
		const int lastVertexIndex = (totalNumVerts-lockedBack)-1;

		Vec3V pos = GetWorldPosition( VECTOR3_TO_VEC3V(m_LocalOffset), *pAttachedInst, m_ComponentA );
		if( m_AttachmentType == ATTACH_FRONT )
			m_EnvCloth->ControlVertex( lockedFront, pos );
		else
			m_EnvCloth->ControlVertex( lastVertexIndex, pos );

		m_AttachedTo.SetLevelIndexAndGenerationId(0);

		clothDebugf1("Converted rope with ID: %d to simple one", m_UniqueID);
	}
}

void ropeInstance::DetachFromObject( const phInst* instObj )
{
	clothDebugf1("[Rope] DetachFromObject	RopeID: %d", m_UniqueID );

	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	phConstraintBase* constraint = NULL;
	if( m_Constraint.IsValid() )
		constraint = PHCONSTRAINT->GetTemporaryPointer(m_Constraint);
		
	if( constraint )
	{
		phConstraintDistance* distanceConstraint = static_cast<phConstraintDistance*>(constraint);

		phInst* instA = constraint->GetInstanceA();
		phInst* instB = constraint->GetInstanceB();

		Assert( m_AttachmentType == ATTACH_BOTH );
		if( instA == instObj )
		{
			// detach from A
			clothDebugf1("[Rope] DetachFromObject	Detach from A  pInst: 0x%p  ", instA );

			m_AttachmentType = ATTACH_BACK;	
			if( m_SkeletonPartB )
			{
				Assert( m_BoneNamePartB );
				const crSkeletonData& skelData = m_SkeletonPartB->GetSkeletonData();
				const crBoneData* boneData = skelData.FindBoneData( m_BoneNamePartB );
				Assertf( boneData, "There is no bone with name: %s in the skeleton!", m_BoneNamePartB );
				if( boneData )
					m_BoneIndex = boneData->GetIndex();
				m_Skeleton = m_SkeletonPartB;
				m_LocalOffset = VEC3V_TO_VECTOR3(m_LocalOffsetPartB);
			}
			else 
			{
				Assertf( instB, "DetachFromObject trying to detach from instB which is NULL" );

				if( instB )
				{
#if USE_PHHANDLE
					m_AttachedTo = PHLEVEL->GetHandle(instB);
#else
					m_AttachedTo = instB;
#endif
					GetLocalPosition( distanceConstraint->GetWorldPosB(), *instB, m_ComponentB, m_LocalOffset );
				}
			}
		}
		else if( instB == instObj )
		{	
			// detach from B
			clothDebugf1("[Rope] DetachFromObject	Detach from B  pInst: 0x%p  ", instB );

			m_AttachmentType = ATTACH_FRONT;
			if( m_SkeletonPartA )
			{
				Assert( m_BoneNamePartA );
				const crSkeletonData& skelData = m_SkeletonPartA->GetSkeletonData();
				const crBoneData* boneData = skelData.FindBoneData( m_BoneNamePartA );
				Assertf( boneData, "There is no bone with name: %s in the skeleton!", m_BoneNamePartA );
				if( boneData )
					m_BoneIndex = boneData->GetIndex();
				m_Skeleton = m_SkeletonPartA;
				m_LocalOffset = VEC3V_TO_VECTOR3(m_LocalOffsetPartA);
			}
			else
			{
				Assertf( instA, "DetachFromObject trying to detach from instA which is NULL" );

				if( instA )
				{
#if USE_PHHANDLE
					m_AttachedTo = PHLEVEL->GetHandle(instA);
#else
					m_AttachedTo = instA;
#endif
					GetLocalPosition( distanceConstraint->GetWorldPosA(), *instA, m_ComponentA, m_LocalOffset );
				}
			}
		}
		else
		{
			AssertMsg( 0, "Rope is not attached to this object." );
			return;
		}
		PHCONSTRAINT->Remove(m_Constraint);
		m_Constraint.Reset();
	}
	else
	{
		m_AttachmentType = ATTACH_INVALID;		// detach completely, and rope is not attached to anything
#if USE_PHHANDLE
		m_AttachedTo.SetLevelIndexAndGenerationId(0);
#else
		m_AttachedTo = NULL;
#endif
		m_Skeleton	 = NULL;
		m_BoneIndex	 = -1;
		m_ComponentA = 0;
		m_ComponentB = 0;

		m_LocalOffset.Zero();
	}

	m_UpdateRefFrameVelocity = 0;
	m_SkeletonPartA = NULL;		
	m_SkeletonPartB = NULL;
	m_BoneIndexPartA = -1;
	m_BoneIndexPartB = -1;
	m_BoneNamePartA = NULL;
	m_BoneNamePartB = NULL;

// TODO: it is not good idea to unpin everything , this will work correctly when only 2 verts are pinned
	cloth->DynamicUnpinAll();
}

void ropeInstance::Draw(const Matrix34 &camMtx, float lodScale)
{
	ropeData *rdata = ropeDataManager::GetTypeDataByIndex(m_Type);
	Draw(camMtx,rdata, lodScale);
}

void ropeInstance::ShadowDraw(const Matrix34 &camMtx)
{
	if( !m_IsDrawEnabled || !m_IsDrawShadowEnabled )
		return;

	PIXBegin(0, "ShadowDrawRope");

	int lod = 1;
	
#if __BANK
	lod = ropeInstance::sm_forceLodLevel > -1 ? ropeInstance::sm_forceLodLevel : lod;
#endif // __BANK
	Assert( m_Drawable );

	grmShader *shader = RopeModel::Instance().GetShader();
	shader->SetVar(ropeManager::GetDiffuseTextureVar(), (grcTexture*)NULL);
	shader->SetVar(ropeManager::GetNormalMapVar(), (grcTexture*)NULL);
	int pass = 2;

	m_Drawable->Draw(camMtx,0,lod,0,pass);

	PIXEnd();
}

sysCriticalSectionToken		s_LockIDs;
int ropeInstance::SetNewUniqueId()
{
	sysCriticalSection critSec(s_LockIDs);

	m_UniqueID = sm_NextUniqueIdForScript++;
	clothDebugf1("Set Rope unique ID:  %d ", m_UniqueID );
	return m_UniqueID;
}

void ropeInstance::AttachVirtualBoundCapsule( const float capsuleRadius, const float capsuleLen, Mat34V_In boundMat, const int boundIdx/*, const Mat34V* transformMat*/ )
{
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	cloth->AttachVirtualBoundCapsule( capsuleRadius, capsuleLen, boundMat, boundIdx /*, transformMat*/ );
}

void ropeInstance::AttachVirtualBoundGeometry(const int numVerts, const u32 numPolys, const Vec3V* RESTRICT verts, const phPolygon::Index* RESTRICT triangles, const int boundIdx )
{
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	cloth->AttachVirtualBoundGeometry( numVerts, numPolys, verts, triangles, boundIdx );
}

void ropeInstance::CreateVirtualBound(int numBounds, const Mat34V* transformMat)
{
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	cloth->CreateVirtualBound( numBounds, transformMat );
}

void ropeInstance::DetachVirtualBound()
{
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	cloth->DetachVirtualBound();
}

#if !__SPU

void ropeInstance::Load( const char* fileName )
{
	Assert( m_EnvCloth );
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	cloth->GetClothData().Load( phClothData::sm_tuneFilePath, const_cast<char*>(fileName) );
}

void ropeInstance::Save( const char*
#if !__FINAL
				 fileName 
#endif
						)
{
#if !__FINAL
	Assert( m_EnvCloth );
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	cloth->GetClothData().Save( const_cast<char*>(fileName) );
#endif
}

#endif // !__SPU


template<class T> bool IsNodeInDList(const rage::atDList<T> &list, const rage::atDNode<T> &node)
{
	const rage::atDNode<T> *p1 = list.GetHead();
	while (p1)
	{
		if (p1==&node)
			return true;
		p1 = p1->GetNext();
	}
	return false;
}

// ropeManager

void ropeManager::Init(verletTaskManager* tManager, grmShader *shader)
{
#if ENABLE_SPU_DEBUG_DRAW
	phVerletCloth::InitDebugSPU();
#endif

	Assert(tManager);
	m_TasksManager = tManager;

	AssertMsg(m_FreeList.GetHead()==NULL , "Leaked a ropeInstance in the free list");
	m_ActiveCount = 0;
	m_FreeCount = 0;
	m_ShutdownCount = 0;

	AssertMsg(m_BatchList.GetHead()==NULL , "Leaked a phVerletCloth in the batch list");
	
	for (int i=0; i<sm_MaxRopeCount; i++)
	{
		ropeInstance *cloth = rage_new ropeInstance();
		Assert(cloth);
		m_FreeList.Append(cloth->GetNodeRef());
		m_FreeCount++;
	}

	ropeDataManager::Init();

	Assert(!RopeModel::Instance().HasLods());	

	// Init procedural rope model
	RopeModel::Instance().AddLod( 4.0f, 100, 12, Vector2(1.0f, 1.0f) );
	RopeModel::Instance().AddLod( 8.0f, 45, 7, Vector2(1.0f, 1.0f)  );
#if USE_3LODS
	RopeModel::Instance().AddLod( 30.0f, 20, 5, Vector2(1.0f, 1.0f) );
#endif // USE_3LODS

	ms_lodLevels[0] = 1.0f;
	ms_lodLevels[1] = 100.0f;
	ms_lodLevels[2] = 200.0f;
	
	RopeModel::Instance().SetShader(shader);
	
	sm_specularFresnelVar = shader->LookupVar("Fresnel");
	sm_specularFalloffVar = shader->LookupVar("Specular");
	sm_specularIntensityVar = shader->LookupVar("SpecularColor");
	//@@: location ROPEMANAGER_INIT_SET_SHADERS
	sm_bumpinessVar = shader->LookupVar("Bumpiness");
	sm_colorVar = shader->LookupVar("ropeColor");

	sm_diffuseTextureVar = shader->LookupVar( "DiffuseTex" );
	sm_normalMapVar = shader->LookupVar( "BumpTex", false );
	if( sm_normalMapVar == grcevNONE )
	{
		sm_normalMapVar = shader->LookupVar( "NormalMap", false);
	}
}

Vec3V_Out ropeManager::CalcRotAxisAngleFromDir(Vec3V_In vDir)
{
	Mat34V lookMtx34V;
	LookDown(lookMtx34V,vDir,Vec3V(V_Z_AXIS_WZERO));
	Mat34VRotateLocalY(lookMtx34V,ScalarV(-PI/2.0f));
	QuatV quatV = QuatVFromMat34V(lookMtx34V);
	Vec3V rotV;
	ScalarV rotAngle;
	QuatVToAxisAngle(rotV,rotAngle,quatV);
	rotV *= rotAngle;
	return rotV;
}

#if __BANK
void ropeManager::AddWidgets(bkBank* bank)
{
	bank->PushGroup("Rendering");
	bank->AddSlider( "Force rope LOD", &ropeInstance::sm_forceLodLevel, -1, 1, 1 );
	bank->AddToggle( "Colorize rope Lod", &ropeInstance::sm_lodColorize );
	bank->AddSlider( "LOD Distance 0", &ms_lodLevels[0], 0.0f, 3000.0f, 1.0f);
	bank->AddSlider( "LOD Distance 1", &ms_lodLevels[1], 0.0f, 3000.0f, 1.0f);
	bank->AddSlider( "LOD Distance 2", &ms_lodLevels[2], 0.0f, 3000.0f, 1.0f);
	bank->PopGroup();
}
#endif // __BANK

void ropeManager::Deactivate()
{
	for( atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext() )
	{
		node->Data->SetActive(false);		
	}
}

void ropeManager::Flush()
{
	atDNode<ropeInstance*>* node = m_ActiveList.GetHead();
	while( node )
	{
		Remove( node->Data );
		node = m_ActiveList.GetHead();
	}
	Assert( m_ActiveList.GetHead()==NULL );
	Assert( m_ActiveList.GetTail()==NULL );
	Assert( m_ActiveCount==0 );
}

void ropeManager::ProcessShutdownList()
{
	sysCriticalSection critSec(s_LockShutdownList);

	atDNode<ropeInstance*>* node;
	atDList<ropeInstance*>* pFreeList = &m_FreeList;
	while( (node = m_ShutdownList.PopHead()) != 0 )
	{
		node->Data->Shutdown();
		Assert( m_ShutdownCount > 0 );
		m_ShutdownCount--;

		pFreeList->Append( node->Data->GetNodeRef() );
		m_FreeCount++;	

		clothDebugf1("Rope shutdown. Number of active ropes: %d. Number of ropes ready to shutdown: %d", m_ActiveCount, m_ShutdownCount );
	}
	Assert( m_ShutdownList.GetHead()==NULL );
	Assert( m_ShutdownList.GetTail()==NULL );
	Assert( m_ShutdownCount == 0 );
}

void ropeManager::Reset()
{
	Flush();
	ProcessShutdownList();
	Assert( m_FreeCount == sm_MaxRopeCount );
}

void ropeManager::Shutdown()
{
	RopeModel::Instance().Destroy();

	AssertMsg(m_BatchList.GetHead()==NULL , "Leaked phVerletCloth in the batch list");
	AssertMsg(m_ActiveList.GetHead()==NULL , "Leaked ropeInstance in the active list");
	AssertMsg(m_ShutdownList.GetHead()==NULL , "Leaked ropeInstance in the shutdown list");

	atDNode<ropeInstance*> *node = m_FreeList.PopHead();
	while (node)
	{
		node->Data->ShutdownPhysics();
		node->Data->Shutdown();
		delete node->Data;
		node->Data = NULL;
		node = m_FreeList.PopHead();
	}

	ropeDataManager::Shutdown();

#if ENABLE_SPU_DEBUG_DRAW
	phVerletCloth::ShutdownDebugSPU();
#endif
}

void ropeManager::UpdateViewport(const grcViewport& _viewPort)
{
	m_ViewPort = &_viewPort;
}

void ropeManager::UpdatePhysics(float elapsed, enRopeUpdateOrder updateOrderType)
{
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->GetUpdateOrder() != updateOrderType )
			continue;

		node->Data->UpdatePhysics(elapsed);
		if( node->Data->HasCustomCallback() )
			node->Data->RunCustomCallback();
	}
}

void ropeManager::ResetLength(enRopeUpdateOrder updateOrderType)
{
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->GetUpdateOrder() != updateOrderType )
			continue;

		node->Data->ResetLength();
	}
}

void ropeManager::UpdateCloth(enRopeUpdateOrder updateOrderType)
{
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->GetUpdateOrder() != updateOrderType )
			continue;

		node->Data->UpdateControlVertices();

#if __PS3 && UPDATE_BATCH
		if( !sm_RopeSpuUpdate || node->Data->IsPPUOnly() )
#endif
			StartTask( node->Data );		
	}
#if UPDATE_BATCH
	const float timeStep = s_timeStepRope;
	UpdateBatch( timeStep );
#endif
}

void ropeManager::PostUpdate()
{

}

void ropeManager::SafeUpdate()
{
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		node->Data->GetEnvCloth()->CopyVerletDrawVertsForRope();
	}
}

void ropeManager::ConvertToSimple( const phInst* pInst )
{
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->IsAttached(pInst) )
		{
			node->Data->ConvertToSimple();
		}
	}
}

bool ropeManager::HasChildRope(int ropeParentID)
{
	Assert( ropeParentID > -1 && ropeParentID < (int)ropeInstance::sm_NextUniqueIdForScript );
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->GetParentUniqueID() == ropeParentID )
			return true;
	}
	return false;
}

ropeInstance* ropeManager::FindRope(int ropeID)
{
// TODO: slow linear search ... do something
	Assert( ropeID > -1 && ropeID < (int)ropeInstance::sm_NextUniqueIdForScript );
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->GetUniqueID() == ropeID )
			return node->Data;
	}
	return 0;
}

ropeInstance* ropeManager::FindRope(phInst* phInstToCheck)
{
	Assert( phInstToCheck );
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( node->Data->IsAttached( phInstToCheck ) )
			return node->Data;
	}
	return 0;
}

int ropeManager::RemoveChildRope(int ropeID)
{
	clothDebugf1("[ropeManager::RemoveChildRope] ropeID: %d", ropeID );
	Assert( ropeID > -1 && ropeID < (int)ropeInstance::sm_NextUniqueIdForScript );		// searching for ropeID not in the range is wrong
	int numFound = 0;
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; )
	{
		if( node->Data->GetParentUniqueID() == ropeID )
		{
			node->Data->DetachVirtualBound();
			RemoveRope(node->Data);
// TODO: fix this. Need better approach 
			node = m_ActiveList.GetHead();			
			numFound++;
			continue;
		}
		node=node->GetNext();
	}

	clothDebugf1("[ropeManager::RemoveChildRope] Deleted ropes count: %d", numFound );
	return numFound;
}

int ropeManager::RemoveRope(int ropeID)
{
	clothDebugf1("[ropeManager::RemoveRope] ropeID: %d", ropeID );
	Assert( ropeID > -1 && ropeID < (int)ropeInstance::sm_NextUniqueIdForScript );		// searching for ropeID not in the range is wrong
	int numFound = 0;
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; )
	{
		if(node->Data->GetUniqueID() == ropeID || node->Data->GetParentUniqueID() == ropeID)
		{
			node->Data->DetachVirtualBound();
			RemoveRope(node->Data);
// TODO: fix this. Need better approach 
			node = m_ActiveList.GetHead();			
			numFound++;
			continue;
		}
		node=node->GetNext();
	}

	clothDebugf1("[ropeManager::RemoveRope] Deleted ropes count: %d", numFound );
	return numFound;
}

void ropeManager::Draw(const Matrix34 &camMtx, float lodScale, ropeCallback shaderCallback)
{
	GRC_ALLOC_SCOPE_AUTO_PUSH_POP()	

	atDNode<ropeInstance*>* node;
	for (node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		if( shaderCallback )
		{
			(*shaderCallback)(node->Data);
		}
			
		node->Data->Draw(camMtx,lodScale);
	}
}

void ropeManager::ShadowDraw(const Matrix34 &camMtx)
{
	GRC_ALLOC_SCOPE_AUTO_PUSH_POP()

	atDNode<ropeInstance*>* node;
	for (node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		node->Data->ShadowDraw(camMtx);
	}
}

void ropeManager::Apply(ropeCallback callback)
{
	atDNode<ropeInstance*>* node;
	for (node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		(*callback)(node->Data);
	}
}

ropeInstance* ropeManager::Add(Vec3V_In pos, Vec3V_In rot, float length, float minLength, float maxLength, float lengthChangeRate, int ropeType, bool pinned
, bool ppuOnly, int numSegments, bool lockFromFront, float gravityScale, bool breakable, int numIterations ) 
{
	atDNode<ropeInstance*>* node = AllocateVerlet();
	AssertMsg( node, "Failed to allocate verlet node!");
	if( !node )
		return NULL;
	ropeInstance* verlet = node->Data;
	AssertMsg( verlet, "Failed to allocate an instance!");
	verlet->Init(pos, rot, length, minLength, maxLength, lengthChangeRate, ropeType, pinned, ppuOnly, numSegments, lockFromFront, gravityScale, breakable, numIterations );

// NOTE: It is important to run activation after initialization
	ActivateVerlet( node );

	atDList<phVerletCloth*>* pBatchList = &m_BatchList;
	atDNode<phVerletCloth*> &batchNode = verlet->GetBatchNodeRef();	
	batchNode.Data = verlet->GetEnvCloth()->GetCloth();
	Assert( batchNode.Data );
#if __PS3
	if( !verlet->IsPPUOnly() )
#endif
		pBatchList->Append(batchNode);

	clothDebugf1("Rope added. Number of active ropes: %d. Number of ropes ready to shutdown: %d", m_ActiveCount, m_ShutdownCount );

	return verlet;
}

void ropeManager::Remove( ropeInstance* pInst ) 
{		
	sysCriticalSection critSec(s_LockShutdownList);

	Assert( pInst );

	atDList<ropeInstance*>* pActiveList = &m_ActiveList;
	atDList<ropeInstance*>* pShutdownList = &m_ShutdownList;

	atDNode<ropeInstance*> &node = pInst->GetNodeRef();

	Assertf(  IsNodeInDList(*pActiveList, node), "Removing rope instance/node not in the active list!" );
	Assertf( !IsNodeInDList(*pShutdownList, node), "Adding rope instance/node already in the shutdown list!" );

	Assert( node.Data );
	node.Data->ShutdownPhysics();

	atDList<phVerletCloth*>* pBatchList = &m_BatchList;
	atDNode<phVerletCloth*> &batchNode = pInst->GetBatchNodeRef();	
	batchNode.Data = NULL;
#if __PS3
	if( !pInst->IsPPUOnly() )
#endif
		pBatchList->PopNode(batchNode);

	pActiveList->PopNode(node);
	Assert( m_ActiveCount > 0 );
	m_ActiveCount--;

	pShutdownList->Append(node);	
	m_ShutdownCount++;	

	clothDebugf1("Rope with ID: %d removed. Number of active ropes: %d. Number of ropes ready to shutdown: %d", pInst->GetUniqueID(), m_ActiveCount, m_ShutdownCount );
}


void ropeManager::BreakRope( ropeInstance* rope, ropeInstance*& ropeA, ropeInstance*& ropeB, float lenA, float lenB, float minLen, int /*numSections*/ )
{
	Assert( rope );

// TODO: find what is rot useful for
	Vec3V rot( 0.0f, 0.0f, -0.5f*PI );

	Vec3V posA = rope->GetWorldPositionA();
	Vec3V posB = rope->GetWorldPositionB();

	phInst* instA = rope->GetInstanceA();
	phInst* instB = rope->GetInstanceB();

	int ropeUniqueId = rope->GetUniqueID();
	int typeFlags, includeFlags;
	rope->GetPhysInstFlags( typeFlags, includeFlags );

	Assert( rope->GetPhysInst() );
	Assert( rope->GetPhysInst()->GetArchetype() );
	Assert( rope->GetPhysInst()->GetArchetype()->GetBound() );

//	Assert( lenA >= 0.0f );
	if( lenA > 0.1f )
	{
		ropeA = Add( posA, rot, lenA, minLen, lenA, rope->GetLengthChangeRate(), rope->GetType(), true, rope->IsPPUOnly(), (int)Clamp(lenA, 4.0f, 16.0f), true, rope->GetGravityScale(), false /*rope->IsBreakable()*/, rope->GetEnvCloth()->GetCloth()->m_nIterations );
		ropeA->SetParentUniqueId(ropeUniqueId);
		ropeA->SetNewUniqueId();
		if( instA )
		{
			ropeA->AttachToObject( posA, instA, 0, NULL, NULL );
			if( ropeA->GetActivateObjects() )
			{
				PHSIM->ActivateObject( instA );	
			}
		}
	}

//	Assert( lenB >= 0.0f );
	if( lenB > 0.1f )
	{
		ropeB = Add( posB, rot, lenB, minLen, lenB, rope->GetLengthChangeRate(), rope->GetType(), (instB?true:false), rope->IsPPUOnly(), (int)Clamp(lenB, 4.0f, 16.0f), true, rope->GetGravityScale(), false /*rope->IsBreakable()*/, rope->GetEnvCloth()->GetCloth()->m_nIterations );
		ropeB->SetParentUniqueId(ropeUniqueId); 
		ropeB->SetNewUniqueId();
		if( instB )
		{
			ropeB->AttachToObject( posB, instB, 0, NULL, NULL );
			if( ropeB->GetActivateObjects() )
			{
				PHSIM->ActivateObject( instB );
			}
		}
	}

	Assert( rope->GetEnvCloth() );
	phVerletCloth* pCloth = rope->GetEnvCloth()->GetCloth();
	Assert( pCloth );
	const int totalNumVerts = pCloth->GetNumVertices();
	const int lockedFront = pCloth->GetNumLockedEdgesFront();
	const int lockedBack = pCloth->GetNumLockedEdgesBack();
	const int lastVertexIndex = (totalNumVerts-lockedBack)-1;
	phClothData& clothdata = pCloth->GetClothData();

	Vec3V ropeDirection = Subtract( clothdata.GetVertexPosition( lastVertexIndex ), clothdata.GetVertexPosition( lockedFront ) );

	if( ropeA )
	{
		ropeA->AdjustVerts( ropeDirection );
		ropeA->SetPhysInstFlags( typeFlags, includeFlags );
	}
	
	if( ropeB )
	{
		ropeB->AdjustVerts( Negate(ropeDirection) );	
		ropeB->SetPhysInstFlags( typeFlags, includeFlags );
	}
}

void ropeInstance::AdjustVerts( Vec3V_In ropeDirection )
{
	Assert( m_EnvCloth );
	phVerletCloth* pCloth = m_EnvCloth->GetCloth();
	Assert( pCloth );
	const int totalNumVerts = pCloth->GetNumVertices();
	const int lockedFront = pCloth->GetNumLockedEdgesFront();
	const int lockedBack = pCloth->GetNumLockedEdgesBack();
	phClothData& clothdata = pCloth->GetClothData();

	Vec3V tempAxis = NormalizeFast(ropeDirection);
	Vec3V v0 = clothdata.GetVertexPosition( lockedFront );

	for( int i = (lockedFront + 1); i < (totalNumVerts-lockedBack); ++i )
	{
		Vec3V newPos = AddScaled( v0, tempAxis, ScalarVFromF32( m_MaxEdgeLength * (float)i) );
		clothdata.SetVertexPosition( i, newPos );
		clothdata.SetVertexPrevPosition( i, newPos );
	}
}

atDNode<ropeInstance*>* ropeManager::AllocateVerlet() 
{ 	
	atDList<ropeInstance*>* pFreeList = &m_FreeList;
// 	Assert(CheckDListSanity(*pFreeList));

	atDNode<ropeInstance*>* node = pFreeList->GetHead();
	if (!node)
	{
		clothErrorf("Rope manager run out of free nodes!");
		return NULL;
	}

	pFreeList->PopNode(*node);

	Assert(m_FreeCount>0);
	m_FreeCount--;
// 	Assert(CheckDListSanity(*pFreeList));
	return node;
}

void ropeManager::ActivateVerlet(atDNode<ropeInstance*> *node)
{
	Assert( node );
	atDList<ropeInstance*>* pActiveList = &m_ActiveList;
	// 	Assert(CheckDListSanity(*pActiveList));
	pActiveList->Append(*node);
	m_ActiveCount++;
}

bool ropeManager::ShouldUpdate(ropeInstance* inst) 
{
	Assert( inst );
	Assert( inst->GetEnvCloth() );
	phVerletCloth* pCloth = inst->GetEnvCloth()->GetCloth();
	Assert( pCloth );

	Assert( m_ViewPort );
#if NO_BOUND_CENTER_RADIUS
	Vec3V center = pCloth->GetCenter();
	Vec4V boundCenterAndRadius;
	boundCenterAndRadius.SetXYZ( center );
	boundCenterAndRadius.SetWf( pCloth->GetRadius(center) );
	if( m_ViewPort->IsSphereVisible(boundCenterAndRadius) )
#else
	if( m_ViewPort->IsSphereVisible(VECTOR4_TO_VEC4V(pCloth->GetBoundingCenterAndRadius())) )
#endif
		return true;

#if !__TOOL
	const spdAABB aabb( inst->GetEnvCloth()->GetBBMinWorldSpace(), inst->GetEnvCloth()->GetBBMaxWorldSpace() );
	if (fwScan::GetScanCascadeShadowInfo().IsShadowVisible( aabb ))
		return true;
#endif // !__TOOL

	return false;
}


bool ropeManager::StartTask( ropeInstance *inst )
{
	Assert( inst );
	const float timeStep = s_timeStepRope;

#if !__PS3

	static sysTaskParameters p;
	p.Input.Data = this;
	p.Output.Data = inst;
	p.UserData[0].asFloat = timeStep;
	p.UserData[1].asPtr = inst;

	p.UserDataCount = 2;

	Assert(m_TasksManager);
	if( m_TasksManager->GetTasksCount() < verletTaskManager::MAX_TASKS )
	{
		m_TasksManager->AddTask( sysTaskManager::Create(TASK_INTERFACE(UpdateTask), p) );
	}
	else
	{
		Warningf("Active Verlet Tasks reached the maximum of %d. You can increase MAX_TASKS but something is wrong!", verletTaskManager::MAX_TASKS);
		AssertMsg( m_TasksManager->GetTasksCount() < verletTaskManager::MAX_TASKS, "Reached the limit of active verlet tasks. You can increase MAX_TASKS but something is wrong!" );
	}
#else
	UpdateVerlet( inst, timeStep );
#endif

	return false;
}


void ropeManager::UpdateTask(sysTaskParameters& p)
{
#if RSG_PC &&__D3D11 && !__RESOURCECOMPILER
	// This means dlCmdGrcDeviceUpdateBuffer::AddBufferUpdateCommandToDrawList() is called whenever a vertex/index buffer
	// is altered on this thread. The render thread then passes the new contents to the GPU by executing a DL command.
	grcBufferD3D11::SetInsertUpdateCommandIntoDrawListFunc(dlCmdGrcDeviceUpdateBuffer::AddBufferUpdate);
	// Occasionally a buffer is updated and deleted in the same frame. This mechanism avoids operating on a deleted buffer.
	grcBufferD3D11::SetCancelPendingUpdateGPUCopy(dlCmdGrcDeviceUpdateBuffer::CancelBufferUpdate);
	grcBufferD3D11::SetAllocTempMemory(dlCmdGrcDeviceUpdateBuffer::AllocTempMemory);
#endif

	float timeStep = p.UserData[0].asFloat;
	ropeInstance *inst = (ropeInstance*)p.UserData[1].asPtr;
	ropeManager* pThis = (ropeManager*)p.Input.Data;
	pThis->UpdateVerlet(inst, timeStep);
}

void ropeManager::UpdateVerlet(ropeInstance* inst, float timeStep )
{
	Assert( inst );
	inst->SetIsUpdating( true );
	if( !inst->IsActive() )
	{
		inst->SetIsUpdating( false );
		return;
	}

	environmentCloth* envCloth = inst->GetEnvCloth();
	Assert( envCloth );

// TODO: curretnly rope is not affected by wind 
// 	Vector3 vWindSpeed; 
// 	WIND.GetLocalWindVel( envCloth->GetVertexWorldPosition(0), vWindSpeed );
// 
// #if 0//__PS3		// NOTE: ApplyAirResistance is on the SPU now.	
// 	envCloth->SetForce( VECTOR3_TO_VEC3V(vWindSpeed) );
// #else
// 	static f32 sfWindDragCoeff = 0.0025f;
// 	envCloth->ApplyAirResistanceRope(vWindSpeed, sfWindDragCoeff);   
// #endif

#if __PS3	
	if( sm_RopeSpuUpdate && !inst->IsPPUOnly() )
	{
		m_TasksManager->AddTask( envCloth->UpdateRopeSpu(timeStep), envCloth, true );
	}
	else
#endif
	{
		if( envCloth )
		{
			envCloth->UpdateRope(timeStep);
		}
		inst->SetIsUpdating( false );
	}
}

#if ENABLE_SPU_DEBUG_DRAW
phVerletSPUDebug s_RopesSpuDebug ;
#endif

#if (__PPU)
sysTaskHandle UpdateEnvironmentRopeBatchSpu( ScalarV_In timeStep, ropeManager* ropeMgr )
{
	const int nodeSize = sizeof(atDNode<phVerletCloth*>);
	int inputsize = ((nodeSize+15)&~15);		
	int stacksize = 24* 1024;		// BVH uses walkStacklessTree which require alot of stacksize
	int scratchSize = kMaxSPUJobSize - stacksize - inputsize - (int)TASK_INTERFACE_SIZE(UpdateEnvironmentRope) - 1024;	

	sysTaskContext c( TASK_INTERFACE(UpdateEnvironmentRope), 0, scratchSize, stacksize);
	c.SetInputOutput();

	c.AddInput( ropeMgr->GetBatchList()->GetHead(),((nodeSize+15)&~15) );

	phVerletRopeUpdate& u = *c.AllocUserDataAs<phVerletRopeUpdate>();
	u.m_Gravity = VEC3V_TO_VECTOR3( phSimulator::GetGravityV() );
	StoreScalar32FromScalarV(u.m_Gravity.w, timeStep);

#if ENABLE_SPU_DEBUG_DRAW
	int dbgIdx = 0;

	Vec4V* v = (Vec4V*)&gDebugDrawPrimCount[dbgIdx]; 
	v->ZeroComponents();

	s_RopesSpuDebug.debugDrawCountOut		= (u32)v;
	s_RopesSpuDebug.debugDrawBuf			= gDebugDrawBuf + dbgIdx*VERLET_DEBUG_BUFFER;

	u.verletSpuDebugAddr 					= (u32)&s_RopesSpuDebug;
#endif

	u.instLastMtxIdxAddrMM = PHLEVEL->GetLastInstanceMatrixIndexMapBaseAddr();
	u.instLastMatricsAddrMM = PHLEVEL->GetLastInstanceMatricesBaseAddr();

	return c.Start();	
}	
#else
void UpdateEnvironmentRopeBatchTask(sysTaskParameters& p)
{	
	float timeStep					= p.UserData[0].asFloat;
	atDNode<ropeInstance*>* head	= (atDNode<ropeInstance*>*)p.Input.Data;

	for (atDNode<ropeInstance*> *node = head; node; node=node->GetNext())
	{
		environmentCloth* envCloth = ((ropeInstance*)node->Data)->GetEnvCloth();
		Assert( envCloth );
		envCloth->UpdateRope( timeStep );
	}
}	
#endif // (__PPU)

void ropeManager::UpdateBatch( float timeStep )
{
	if( m_BatchList.GetHead() )
	{
#if __PS3
		m_TasksManager->AddTask( UpdateEnvironmentRopeBatchSpu( ScalarVFromF32(timeStep), this), this, true);
#else
		static sysTaskParameters p;
		p.Input.Data = m_ActiveList.GetHead();
		p.Output.Data = this;
		
		p.UserData[0].asFloat = timeStep;
		p.UserDataCount = 1;

		Assert(m_TasksManager);
		if( m_TasksManager->GetTasksCount() < verletTaskManager::MAX_TASKS )
		{
			m_TasksManager->AddTask( sysTaskManager::Create(TASK_INTERFACE(UpdateEnvironmentRopeBatchTask), p) );
		}
		else
		{
			Warningf("Active Verlet Tasks reached the maximum of %d. You can increase MAX_TASKS but something is wrong!", verletTaskManager::MAX_TASKS);
			AssertMsg( m_TasksManager->GetTasksCount() < verletTaskManager::MAX_TASKS, "Reached the limit of active verlet tasks. You can increase MAX_TASKS but something is wrong!" );
		}
#endif // (__PPU)
	}
}

void ropeManager::UpdateBatchPostSpu()
{
	for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
	{
		Assert( node->Data );
		Assert( node->Data->GetEnvCloth() );
		node->Data->GetEnvCloth()->UpdateRopePostSpu();
	}
}


#if __PFDRAW && !__RESOURCECOMPILER
void ropeManager::ProfileDraw()
{
	if (	PFD_Ropes.WillDraw()
		)
	{
		for (atDNode<ropeInstance*> *node = m_ActiveList.GetHead(); node; node=node->GetNext())
		{
	#if USE_CLOTHMANAGER_BANK
 			node->Data->DebugUpdate( m_ViewPort );
 	#endif
			node->Data->ProfileDraw();
		}
	}
}
#endif



#if USE_ROPE_DEBUG

void ropeManager::PickEdges( const Vector3& offset, phVerletCloth* cloth )
{
	Assert( cloth );
	const bool notInSpringMode = ropeManager::ms_MouseInput && (ropeManager::ms_MouseInput->GetGrabSpring().instLevelIndex != phInst::INVALID_INDEX);
	if( notInSpringMode )
	{
		if ( m_ViewPort )
		{				
			Vector3 mouseScreen, mouseFar;
			m_ViewPort->ReverseTransform( static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()), RC_VEC3V(mouseScreen), RC_VEC3V(mouseFar) );			

			const int MAX_EDGES_TO_DEBUG = 48;
			int edgesFound[MAX_EDGES_TO_DEBUG];
			int vtxIndex = -1;
			/*int edgesFoundCount =*/ cloth->PickEdges( (const Vector3* RESTRICT)cloth->GetClothData().GetVertexPointer(), offset, mouseScreen, mouseFar, vtxIndex, edgesFound, cloth->GetEdgeList() );

// 			char buf[128];
// 			float _x = 0.1f;
// 			float _y = 0.2f;
// 			float verticaloffset = 0.015f;
// 			Vector2 vTextRenderPos(_x, _y);
// 
// 			for (int edgeIndex=0; edgeIndex<edgesFoundCount; ++edgeIndex)
// 			{
// 				const phEdgeData& edge = cloth->GetEdgeData(edgesFound[edgeIndex]);
// 
// 				sprintf( buf, "Edge index: %d, Edge Len2: %f, Edge v0, v1: %d, %d", edgesFound[edgeIndex], edge.m_EdgeLength2, edge.m_vertIndices[0], edge.m_vertIndices[1] );
// 				grcDebugDraw::Text(vTextRenderPos, Color_white, buf);
// 				vTextRenderPos.y += verticaloffset;
// 			}
		}
	}
}
#endif // USE_ROPE_DEBUG


} // namespace rage
