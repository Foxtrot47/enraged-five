#ifndef FW_CLOTHMANAGER_H
#define FW_CLOTHMANAGER_H

#include "atl/dlist.h"
#include "data/base.h"
#include "fragment/manager.h"
#include "cloth/clothcontroller.h"
#include "clothdebug.h"

#include "fwscene/mapdata/maptypes.h"

namespace rage
{
	class verletTaskManager;
	class clothInstanceTaskManager;

#define		USE_CLOTHMANAGER_BANK		1 && (__BANK && !__RESOURCECOMPILER)
#define		USE_CLOTH_TELEMETRY			( 1 )
#define		MAX_NUM_CHAR_CLOTH_BOUNDS		16

#define		CLOTH_PROFILE					(!__PS3 && __PFDRAW)
#define		MAX_CHARS_CLOTH_NAME			(32)
#define		CLOTH_ASSIGN_TO_DRAWABLE		(__XENON && __BANK && 1)
#define		USE_AABB_VS_AABB_PED_VS_CLOTH			(1)

#if __XENON || __WIN32PC
#define		NUM_CLOTH_BUFFERS				4
#else
#define		NUM_CLOTH_BUFFERS				3
#endif

#if USE_CLOTHMANAGER_BANK

	class bkButton;
	class bkGroup;
	class bkSlider;
	class phMouseInput;

#endif


#if !__SPU
// TODO: this is not definition about bounds anymore, planes can be defined too
struct phCapsuleBoundDef
{
	enum enCollisionBoundDef
	{
		BOUND_DEF_IS_PLANE		= 0,
	};

	atString	m_OwnerName;
	QuatV		m_Rotation;
	Vec3V		m_Position;
	Vec3V		m_Normal;
	float		m_CapsuleRadius;
	float		m_CapsuleLen;
	float		m_CapsuleHalfHeight;
	float		m_CapsuleHalfWidth;			// not supported at this time
	atFixedBitSet<32, u32>		m_Flags;

	PAR_SIMPLE_PARSABLE;
};

struct phVerletClothCustomBounds : public fwExtensionDef
{
	void Load(const char* filePath, const char* clothName, const char* fileNameAppendix);
	atArray<phCapsuleBoundDef> m_CollisionData;
	static const char* sm_MetaDataFileAppendix;

	PAR_PARSABLE;
};

#if !__RESOURCECOMPILER

class fwClothCollisionsExtension : public fwExtension
{
public:

	fwClothCollisionsExtension() {}

	FW_REGISTER_CLASS_POOL(fwClothCollisionsExtension);
	EXTENSIONID_DECL(fwClothCollisionsExtension, fwExtension);

	virtual void InitEntityExtensionFromDefinition(const fwExtensionDef* pDefinition, fwEntity* /*pEntity*/)
	{
		Assert( pDefinition );
		const phVerletClothCustomBounds* pRefCustomBounds = (const phVerletClothCustomBounds*)pDefinition;
		Assert( pRefCustomBounds );
		int numBounds = pRefCustomBounds->m_CollisionData.GetCount();
		Assert( numBounds > 0 );
		m_ClothCollisions.m_CollisionData.Resize( numBounds );

		for( int i = 0; i < numBounds; ++i )
		{
			m_ClothCollisions.m_CollisionData[i] = pRefCustomBounds->m_CollisionData[i];
		}
	}

	phVerletClothCustomBounds m_ClothCollisions;
};
#endif // !__RESOURCECOMPILER

#endif // !__SPU


class clothInstance : public datBase
{
	mutable rage::atDNode<clothInstance*> m_Node;  // the node from clothManager.

public:

	clothInstance();
	~clothInstance() {}

	void Init(rage::clothController *inst, int cType);
	void Shutdown();
#if USE_CLOTHMANAGER_BANK
	void DebugUpdate( const grcViewport* pViewport );
#endif

#if __PFDRAW
	void ProfileDraw(Vec3V_In camPos);
#endif

	rage::atDNode<clothInstance*> *GetNode() const { return &m_Node; }
	rage::atDNode<clothInstance*> &GetNodeRef() const { return m_Node; }

	rage::clothController* GetClothController() const { return m_ClothController; }

	int GetType() const; //{ return (m_Type & ~clothManager::ReplayControlled); }
	int GetTypeWithModifiers() const { return m_Type; }
	void SetTypeModifiers(int modifier);
	void ClearTypeModifiers(int modifier);

	// Note: active check is used by bullet functionality
	void SetActive(bool isActive) { m_IsActive = isActive; }
	void SetReady() { m_IsReady = true; }
	bool IsActive() const { return m_IsActive; }
	bool IsReady() const { return m_IsReady; }
	void SetSkeleton(const crSkeleton* pSkeleton);
	void SetSkeletonOnly(const crSkeleton* pSkeleton);

	const crSkeleton* GetSkeleton() const { return m_Skeleton; }

	Vec3V_Out GetLastPosition() const { return m_LastPosition; }
	void SetLastPosition(Vec3V_In pos) { m_LastPosition = pos; }

#if USE_PRE_ALLOC_MEMORY
	void*	preAllocatedMemory;
#endif

	int& GetFramesToRotate() { return m_FramesToRotate; }
	ScalarV_Out GetRotationSign() const { return m_RotationSign; }
	void SetRotationSign(ScalarV_In signValue) { m_RotationSign = signValue; }

	Mat34V_Ref GetInstanceMat() { return m_Matrix; }
	void SetInstanceMat(Mat34V_In mat) { m_Matrix = mat; }

#if RSG_PC
	void SetElapsedTime(float fElapsedTime) { m_ElapsedTimeSinceLastUpdate = fElapsedTime; }
	float GetElapsedTime() const { return m_ElapsedTimeSinceLastUpdate; }
#endif

	void SetUserData1And2(u32 val1,u32 val2) { m_UserData1 = val1, m_UserData2 = val2; }
	void SetUserData3(u32 val3) { m_UserData3 = val3; }
	u32 GetUserData1() { return m_UserData1; }
	u32 GetUserData2() { return m_UserData2; }
	u32 GetUserData3() { return m_UserData3; }

protected:	

	Mat34V	m_Matrix;							// matrix of pure instance cloth ... TODO: still in test
	rage::clothController*	m_ClothController;
	datRef<const crSkeleton> m_Skeleton;

	Vec3V	m_LastPosition;
	ScalarV m_RotationSign;
	int		m_FramesToRotate;
	int		m_Type;	

#if RSG_PC
	float	m_ElapsedTimeSinceLastUpdate;
#endif

	bool	m_IsActive;
	bool	m_IsReady;

	u32		m_UserData1;
	u32		m_UserData2;
	u32		m_UserData3;

	friend class clothManager;
#if USE_CLOTHMANAGER_BANK
	phVerletCloth* GetVerletClothAndOffset(Vector3& offset);

public:

	void DrawSelection(const Vector3& offset, const Vector3* RESTRICT pClothVertices, bool bDrawInfo = true);

	int  PickVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool pin );
	int  PickEdgesByVerts( const Vector3& offset, const Vector3* RESTRICT pClothVertices, const int numVerts, const int numPinVerts, const grcViewport* pViewport, bool pin );
	void PickEdges( const Vector3& offset, phVerletCloth* cloth, const grcViewport* pViewport, const atArray<phEdgeData>& edgeData );
	void PinUnPinVerts( const Vector3& offset, phVerletCloth* cloth, const grcViewport* pViewport, bool pin );
	void PreviewEdgesSplit(int vertexIdx, int edgeIdx0, int edgeIdx1);

#if CLOTH_INSTANCE_FROM_DATA
// TODO: test, related to pure cloth meshes
	void LoadTopology();
	void SaveTopology();
	void AttachToVehicle();
	void DetachFromVehicle();
#endif

	void DrawCustomBound();
	void DrawBound(const phBound* customBound, Mat34V* boundPose, bool isGizmoAttached);
	template<class T> void DrawPerVertexValues(const Vector3& camPos, const T* RESTRICT vertexValues, const char* stringFormat, const Color32* RESTRICT PF_DRAW_ONLY(colorValues) );
	void DrawOriginalVertexIndices(const Vector3& camPos, const u16* RESTRICT vertexValues, const char* stringFormat );

	bool IsDebugPoseActive() const;
	const Vector3* GetClothVertices(const phVerletCloth* verletCloth) const;
	void SetBoundCapsuleData(void* boundPtr);

	// NOTE: used only for character cloth Live update purposes
	void RegisterRestInterface(const char* controllerName);
	void UnregisterRestInterface(const char* controllerName);
	atArray<Vec3V> m_BindPose;
	atArray<Vec3V> m_DebugPose;
#if 1 //CLOTH_INSTANCE_FROM_DATA
	atArray<int>   m_Indices;
#endif

	bkGroup*	m_RAGGroup;
	bkGroup*	m_VirtualBoundGroup;
	bkGroup*	m_TogglesGroup;
	bkToggle*	m_DynamicPinUnpinWidget;
	bkToggle*	m_DynamicPickVertsWidget;
	bkToggle*	m_DynamicPickPinVertsWidget;
	struct debugBoundData
	{
		bkGroup*	instGroup;
		bkButton*	bndButton;
		phBound*	boundPart;
		int			boneIdx;
	};
	debugBoundData	m_DebugBoundButton[MAX_NUM_CHAR_CLOTH_BOUNDS];

	int			m_DebugCurrentPinRadiusSet;
	int			m_DebugCurrentPose;
	int			m_DebugSelectedVertex;
	int			m_SpuDebugIdx;

#if CLOTH_PROFILE
	float		m_LastUpdateTime;
	float		m_UpdateTime;
	float		m_InstanceTime;
#endif

	bool		m_SpuDebug;
	bool		m_DebugDraw;
	bool		m_DebugDrawPinRadiuses;
	bool		m_DebugDrawAirResistance;
	bool		m_DebugDrawEdgeInfo;
	bool		m_DebugDragVerts;
	bool		m_DebugPickVerts;
	bool		m_DebugPickPinVerts;
	bool		m_DebugPickEdges;
	bool		m_DebugPickEdgesByVerts;
	bool		m_DebugPickCustomEdges;
	bool		m_DebugPickInstanceCustomEdges;
	bool		m_DebugUnPinVerts;
	bool		m_DebugPinVerts;
	bool		m_DebugDrawCustomBound;
	bool		m_DebugPreviewEdgeSplit;
	bool		m_DebugDeleteVerts;
	bool		m_RestInterface;

	PAR_SIMPLE_PARSABLE;
#endif		
};


class clothVariationData
{
	mutable rage::atDNode<clothVariationData*> m_Node;

public:
	clothVariationData();

	rage::atDNode<clothVariationData*> *GetNode() const { return &m_Node; }
	rage::atDNode<clothVariationData*> &GetNodeRef() const { return m_Node; }
	
	void AllocateBuffers(int numVerts);
	void DeleteBuffers();
	void DeleteClothData();

// TODO: temp
	void Set(characterCloth* pCharCloth);

	void Set( int numVerts, characterCloth* clothPtr, s32 dictionarySlotId );	
	void Reset();

	pgRef<characterCloth>&	GetCloth() { return m_clothPtr; }
	s32						GetClothDictionarySlotId() const { return m_clothDictionarySlotId; }
	Mat34V*					GetClothParentMatrixPtr(int bufferIdx) { return &m_clothParentMatrix[bufferIdx]; }
	Mat34V_Out				GetClothParentMatrix(int bufferIdx) const { return m_clothParentMatrix[bufferIdx]; }
	Vec3V*					GetClothVertices(int bufferIdx) { Assert(bufferIdx < NUM_CLOTH_BUFFERS ); return m_clothVertices[bufferIdx]; }
	int						GetClothNumVertices() const { return m_clothNumVertices; }

#if __XENON	
	grcVertexBuffer*		GetClothVtxBuffer(int bufferIdx) const { Assert(bufferIdx < NUM_CLOTH_BUFFERS ); return m_clothVtxBuffer[bufferIdx]; }
#endif	

protected:
	Mat34V					m_clothParentMatrix[NUM_CLOTH_BUFFERS];
	s32						m_clothDictionarySlotId;	// id of the cloth sloth in the dictionary, used for reference counting
	pgRef<characterCloth>	m_clothPtr;					// ptr to character cloth 
	Vec3V*					m_clothVertices[NUM_CLOTH_BUFFERS];
	int						m_clothNumVertices;
#if __XENON	
	grcVertexBuffer*		m_clothVtxBuffer[NUM_CLOTH_BUFFERS];			// ptr to character cloth vertex buffers
#endif
};

typedef Functor1<phVerletCloth*>	FoundClothCB;
#if CLOTH_INSTANCE_FROM_DATA
typedef void (*AttachDetachCB)(clothVariationData*);
#endif

class clothManager : public datBase
{
	struct clothDrawableParams
	{
		rmcDrawable* pTypeDrawable;
		void* pPreAllocatedMemory;
		int iMemoryAvailable;
		environmentCloth* pEnvCloth;
		Vec3V translationV;
		int iTotalBufferSize;
		int iTotalMemoryRequested;
	};

public:
	enum clothType
	{
		Unknown	=  0,
		Environment	=  1 << 0,
		Character	=  1 << 1,
		Vehicle		=  1 << 2,
		ReplayControlled =  1 << 4, // Used to OR into the above types.
		
	};

	enum enPostInjectVerticesAction
	{
		checkAganstCollisionInsts	=	1 << 0,
		updateDrawableGeometry		=	1 << 1,
		resetDeltas					=	1 << 2,
		runSimulation				=	1 << 3,
	};

	clothManager() 
		: m_TasksManager(NULL)
		, m_clothInstanceScheduler(0)
		, m_InstanceTasksManager(NULL)
		, m_DontUseInstanceManager(false)
		, m_CamPos(Vec3V(V_ZERO))
		, m_Slowdown(false)
		, m_SkipSlowdown(0)		
#if USE_CLOTHMANAGER_BANK
		, m_DebugBoundRadius(0.1f)
		, m_DebugBoundLength(0.1f)
#if USE_CAPSULE_EXTRA_EXTENTS
		, m_DebugBoundHalfHeight(0.0f)
#endif
#endif
#if USE_CLOTH_TELEMETRY
		, m_UpdatedCount(0)
		, m_TotalLoadedEnvironmentClothVertexCount(0)
		, m_TotalActiveEnvironmentClothVertexCount(0)
		, m_TotalCharacterClothVertexCount(0)
#endif
	{}
	~clothManager() {}

	void Init(verletTaskManager* pTasksManager, clothInstanceTaskManager* pInstanceTaskManager);
	void Shutdown();
	void Reset();

	static u32  GetBufferIndex(bool bDrawBuffer = false) { return bDrawBuffer? m_DrawBufferIndex: m_BuildBufferIndex; }
	static void ShiftBufferIndex();

	void SwapBuffers();
	void PreUpdate(const grcViewport& viewPort, float elapsed);
	void Update( int typeToUpdate );
	void PostUpdate();

	void InitInterfaceCallbacks( fragManager::ClothInterface& clothInterface );
	clothInstance* GetClothInst(rage::clothController *inst);

	atDNode<clothInstance*>* GetHead()	{ return m_ActiveList.GetHead(); }

	void AddCharacterCloth(rage::clothController* inst, const crSkeleton* skeleton);
	void AddEnvironmentCloth(rage::clothController* inst, bool isSubInst, void* memPtr, clothType cType );

#if CLOTH_INSTANCE_FROM_DATA && USE_CLOTHMANAGER_BANK
	environmentCloth* InstanceEnvironmentCloth( Mat34V_In phInstMat, bool activateOnHit);
	clothVariationData* InstanceCharacterCloth(  const crSkeleton* skel );
#endif

	environmentCloth* InstanceCloth( Mat34V_In phInstMat, const environmentCloth* clothTemplate, bool activateOnHit, bool isSubInst);
	void InstanceClothDrawable( clothDrawableParams* pParams );
	void RemoveCloth(environmentCloth* pEnvCloth, const fragType* pFragType);

	void RemoveCharacterCloth(rage::clothController* inst);
	void RemoveEnvironmentCloth(rage::clothController* inst);

	void DisableInstancesWithCommonSkeleton(const crSkeleton* pSkel);

	void SetCamPos( Vec3V_In pos ) { m_CamPos = pos; }
	void SetSlowdown(bool isActive) { m_Slowdown = isActive; }
	void SkipSlowdown() { m_SkipSlowdown = 5; }

	void ComputeClothImpacts(Vec3V_In vOldPosition, Vec3V_In vPosition, float impulse);

	void* AllocClothVB(int size);
	void  FreeClothVB(void*);

#if __PFDRAW && !__RESOURCECOMPILER
	void ProfileDraw();
#endif	

	void LoadGlobalCustomBounds();
	int SearchBoundsByName(Vec3V_In posV, ScalarV_In radiusV, const char* pClothName, int* pBoundsIndices);
	bool AttachGlobalCustomBound(phVerletCloth* pCloth, Vec3V_In posV, ScalarV_In radiusV, const char* pClothName);

	void FindPedInClothInstance( Vec3V_In pos, phBoundComposite* pCompositeBound, int cType );
	void FindPedInClothInstanceShutdown( phBoundComposite* pCompositeBound, int cType );

#if 0
	bool FindClothInstance( phVerletCloth* pCloth, int cType, Functor0 cleanupCB);
	bool FindClothInstance( Vec3V_In pos, phVerletCloth* pCloth, int cType, Functor0 responseCB);
#endif
	void FindClosestClothInstance(Vec3V_In pos, int typeToUpdate, FoundClothCB responseCB);

	clothVariationData* AllocateVarData();
	void DeallocateVarData(const clothVariationData* pVarData);

	inline 	grcRasterizerStateHandle GetPassRS() { return m_passRS; }
	inline	grcDepthStencilStateHandle GetPassDSS() { return m_passDSS; }
	inline	grcDepthStencilStateHandle GetInvertPassDSS() { return m_invertPassDSS; }
	static void PauseClothSimulation();
	static void UnpauseClothSimulation();

#if USE_CLOTH_TELEMETRY
	u32 GetTotalLoadedEnvironmentClothVertexCount() const { return m_TotalLoadedEnvironmentClothVertexCount; }
	u32 GetTotalLoadedCharacterClothVertexCount() const { return m_TotalCharacterClothVertexCount; }
	u32 GetTotalActiveEnvironmentClothVertexCount() const { return m_TotalActiveEnvironmentClothVertexCount; }
	u32 GetTotalActiveCharacterClothVertexCount() const { return m_TotalCharacterClothVertexCount; }
	u32 GetTotalLoadedEnvironmentClothCount() const { return m_ActiveCount; }
	u32 GetTotalLoadedCharacterClothCount() const { return m_ActiveVarCount; }
	u32 GetTotalActiveEnvironmentClothCount() const { return m_UpdatedCount; }
	u32 GetTotalActiveCharacterClothCount() const { return m_ActiveVarCount; }
#endif

#if !__SPU
	static void LoadCustomBound( phBoundComposite* pCustomBound, const phCapsuleBoundDef& capsuleData, const int boundIdx );
#endif

#if __RESOURCECOMPILER
	static bool AttachCustomBound(phVerletCloth* pCloth, const char* pClothName, phVerletClothCustomBounds& refCustomBounds);
	static int SearchBoundsByName( const char* pClothName, int* pBoundsIndices, phVerletClothCustomBounds& refCustomBounds);
#endif

	void SetDontDoClothOnSeparateThread(bool onOff);

protected:

	bool Update(rage::clothController* inst, environmentCloth* pCloth = NULL);
	void UpdateVerlet(rage::clothController* inst, float timeStep, float timeScale );
	void UpdateAgainstCollisionInsts(rage::clothController* inst, enPostInjectVerticesAction injectAction);

	const grcViewport* m_ViewPort;

	Vec3V m_CamPos;	

	clothInstance* AllocateVerlet();
	void DeallocateVerlet(const clothInstance* pCloth);


	clothInstance* Add(rage::clothController* inst, int cType);
	void Remove( clothInstance* inst);

	static const int sm_MaxClothCount = 600;
	verletTaskManager* m_TasksManager;
	int m_clothInstanceScheduler;
	clothInstanceTaskManager* m_InstanceTasksManager;
	bool m_DontUseInstanceManager;

	static u32 m_BuildBufferIndex;
	static u32 m_DrawBufferIndex;

	int m_ActiveCount;
	int m_FreeCount;

	int m_ActiveVarCount;
	int m_FreeVarCount;

	bool m_Slowdown;
	char m_SkipSlowdown;

	rage::atDList<clothInstance*> m_ActiveList;
	rage::atDList<clothInstance*> m_FreeList;

	rage::atDList<clothVariationData*> m_ActiveVarList;
	rage::atDList<clothVariationData*> m_FreeVarList;

	grcRasterizerStateHandle m_passRS;
	grcDepthStencilStateHandle m_passDSS;
	grcDepthStencilStateHandle m_invertPassDSS;
	phVerletClothCustomBounds m_GlobalCustomBounds;


#if USE_CLOTH_TELEMETRY
	u32 m_UpdatedCount;
	u32 m_TotalLoadedEnvironmentClothVertexCount;
	u32 m_TotalActiveEnvironmentClothVertexCount;
	u32 m_TotalCharacterClothVertexCount;
#endif


	static void UpdateTask(sysTaskParameters& p);
	static void InstanceClothDrawableTask(sysTaskParameters& p);


public:
	static void SetCaptureVerticesFunc(void (*pFunc)(void *pGlobalParam, void *pClothInstance, int numVertices, Vec3V *pVertices), void *pGlobalParam)
	{
		sm_pCaptureVerticesFunc = pFunc;
		sm_pCaptureVerticesFuncGlobalParam = pGlobalParam;
	}

	static void SetInjectVerticesFunc(enPostInjectVerticesAction (*pFunc)(void *pGlobalParam, void *pClothInstance, int numVertices, Vec3V *pVertices), void *pGlobalParam)
	{
		sm_pInjectVerticesFunc = pFunc;
		sm_pInjectVerticesFuncGlobalParam = pGlobalParam;
	}

private:
	static void (*sm_pCaptureVerticesFunc)(void *pGlobalParam, void *pClothInstance, int numVertices, Vec3V *pVertices);
	static void *sm_pCaptureVerticesFuncGlobalParam;

	static enPostInjectVerticesAction (*sm_pInjectVerticesFunc)(void *pGlobalParam, void *pClothInstance, int numVertices, Vec3V *pVertices);
	static void *sm_pInjectVerticesFuncGlobalParam;

#if USE_CLOTHMANAGER_BANK
public:

#if CLOTH_INSTANCE_FROM_DATA
	AttachDetachCB	m_AttachToDrawableCB;
	AttachDetachCB	m_DetachFromDrawableCB;
#endif

	float m_DebugBoundRadius;
	float m_DebugBoundLength;
#if USE_CAPSULE_EXTRA_EXTENTS
	float m_DebugBoundHalfHeight;
#endif

	static Vector3 ms_DebugVertsColor;
	static int	 ms_DebugMatrixIndex;
	static int	 ms_GeometryIndex;
	static int	 ms_ComponentIndex;

	static float ms_DebugSphereRadius;
	static float ms_DebugEdgeWeight;
	static float ms_DebugEdgeCompWeight;
	static float ms_DebugEdgeLenSqr;
	static float ms_DebugBendEdgeWeight;
	static float ms_DebugBendEdgeCompWeight;
	static float ms_DebugBendEdgeLenSqr;	

	static float ms_WindCharClothThreshold;
	static float ms_PinRadius;
	static float ms_VertexWeight;
	static float ms_ClothWeight;
	static float ms_InflationScale;
	static float ms_VertsCapacity;
	static float ms_EdgesCapacity;

	static char* ms_ClothName;

	static phMouseInput* ms_MouseInput;
	static const Mat34V* ms_VehicleFrame;

	static const crSkeleton* ms_PedSkeleton;
	static const rmcDrawable* ms_pPedDrawable;
	static clothVariationData* ms_PedClothVarData;

	static bkGroup*	ms_RAGBankGroup;
	static bkGroup*	ms_RAGBankSliders;
	static bool	ms_ClampAllForces;
	static bool ms_EnableAlpha;
	static bool	ms_FlipAlpha;

	static bool ms_EnableVehiclePick;
	static bool ms_EnablePedPick;
	static bool ms_SceneIsFlushing;

	static void AddBank();

	void LoadCharacterCloth(void* pInstance);
	void SaveCharacterCloth(void* pInstance);

#if CLOTH_INSTANCE_FROM_DATA
	void MapToDrawable(void* pInstance);
	void AttachToDrawable(void* pInstance);
	void DetachFromDrawable(void* pInstance);
	void InstanceEnvironmentClothFromData();
	void InstanceCharacterClothFromData();
#endif

	void InitiateClothInstance(void* ptr);
	void InitiateEnvironmentClothRemove(void* ptr);
	void InitiateCharacterClothRemove(void* ptr);

	void ToggleRecording();
	void ToggleRecordingMode();
	void ToggleDrawCustomBound(void*);

	bool InitWidgets(void);
	void DestroyWidgets(void);

	void SetupEnvClothWidget( rage::clothController* controller );
	void SetupWidget( rage::clothController* controller, int verletType, bool isSubInst );
	void AddWidget( rage::clothController* controller, int verletType, bool isSubInst );
	void RemoveWidget( rage::clothController* controller );

	void ResetSelection( rage::clothController* controller );
	void DragVertsCB(void* pInstance);
	void PickPinVertsCB(void* pInstance);
	void ResetSelection0(void* pInstance);
	void ResetSelection1(void* pInstance);
	void ResetSelection2(void* pInstance);
	void ResetSelection3(void* pInstance);

	void ConnectToRest(void* pInstance);
	void UpdateDebugPose(void* pInstance);

	void SaveBridgeToFile(void* pInstance);
	void SetSelectedPinRadiusSet(void* pInstance);
	void SetPose(void* pInstance);

	void CreateCustomBoundGroup(const phBoundComposite* pCustomBound, const char* pButtonTitleStr, clothInstance* pClothInstance );
	void AttachVirtualBound(void* pInstance);
	void DetachVirtualBound(void* pInstance);

	void SetEdgeWeight();
	void SetEdgeCompWeight();
	void SetEdgeLenSqr();
	void SetEdgeDataInternal( int idx, char offset, float newValue );


	//////////// TODO: test test test - for prototype and debug purposes
	void SetMatrixIndices();
	////////////

	void AddCustomEdge();
	void RemoveCustomEdge();
	void RemoveLastCustomEdge();
	void SetPinRadius();
	void SetVertexResistance();
	void SetClothWeight();
	void SetInflationScale();
	void ClearSelection();

	void SetPinVertsOffset();

	void SetCustomBoundCapsuleData(void* boundPtr);

#if USE_CLOTH_GIZMO
	void AttachBoundToGizmo(void*);
	void ToggleClothGizmoMode();
	void EnableClothGizmo();
#endif

	static void SetSceneIsFlushing(bool isFlushing){ms_SceneIsFlushing = isFlushing;}
#endif // __BANK
};



} // namespace rage

#endif // FW_CLOTHMANAGER_H
