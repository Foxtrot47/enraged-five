//
// ai/navmesh/requests.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_REQUESTS_H
#define AI_NAVMESH_REQUESTS_H

#include "datatypes.h"
#include "navmeshoptimisations.h"		// For NAVMESH_OPTIMISATIONS_OFF
#include "fwtl\regdrefs.h"
#include "system\spinlock.h"

namespace rage
{

#if NAVMESH_OPTIMISATIONS_OFF && __DEV
#define __STORE_POLYS_IN_PATH_REQUEST	1
#elif defined(NAVGEN_TOOL)
#define __STORE_POLYS_IN_PATH_REQUEST	1
#else
#define __STORE_POLYS_IN_PATH_REQUEST	0
#endif


enum EPathServerRequestType
{
	ENoRequest				= -1,

	EPath,
	EGrid,
	ELineOfSight,
	EAudioProperties,
	EFloodFill,
	EClearArea,
	EClosestPosition,
	ENumRequestTypes
};

enum EPathServerRequestResult
{
	ERequest_NotReady,
	ERequest_Ready,
	ERequest_NotFound
};

class CPathFindMovementCosts
{
public:

	void SetDefault();

	float m_fClimbHighPenalty;
	float m_fClimbLowPenalty;
	float m_fDropDownPenalty;
	float m_fDropDownPenaltyPerMetre;
	float m_fClimbLadderPenalty;
	float m_fClimbLadderPenaltyPerMetre;
	float m_fEnterWaterPenalty;
	float m_fLeaveWaterPenalty;
	float m_fBeInWaterPenalty;
	float m_fClimbObjectPenaltyPerMetre;
//	float m_fFixedCostToClimbObject;
//	float m_fFixedCostToPushObject;
	float m_fWanderPenaltyForZeroPedDensity;
	float m_fMoveOntoSteepSurfacePenalty;
	float m_fPenaltyForNoDirectionalCover;
	float m_fPenaltyForFavourCoverPerUnsetBit;
	s32 m_iPenaltyForMovingToLowerPedDensity;
	float m_fAvoidTrainTracksPenalty;
	s32 m_iPenaltyPerPolyFreeSpaceDifference;

	// TODO: Decide how we want to deal with BankFloat, TweakFloat, and other things
	// in GTA's 'basetypes.h' as we move code down to the framework.
#if !__BANK
	typedef const float BankFloat;
	typedef const s32 BankInt32;
#else // !__BANK
	typedef float BankFloat;
	typedef s32 BankInt32;
#endif // !__BANK

	static BankFloat ms_fDefaultCostFromTargetMultiplier;
	static BankFloat ms_fDefaultDistanceTravelledMultiplier;
	static BankFloat ms_fDefaultNonStraightMovementPenalty;
	static BankFloat ms_fDefaultClimbHighPenalty;
	static BankFloat ms_fDefaultClimbLowPenalty;
	static BankFloat ms_fDefaultDropDownPenalty;
	static BankFloat ms_fDefaultDropDownPenaltyPerMetre;
	static BankFloat ms_fDefaultClimbLadderPenalty;
	static BankFloat ms_fDefaultClimbLadderPenaltyPerMetre;
	static BankFloat ms_fDefaultEnterWaterPenalty;
	static BankFloat ms_fDefaultLeaveWaterPenalty;
	static BankFloat ms_fDefaultBeInWaterPenalty;
	static BankFloat ms_fDefaultClimbObjectPenaltyPerMetre;
	static BankFloat ms_fDefaultFixedCostToClimbObject;
	static BankFloat ms_fDefaultFixedCostToPushObject;
	static BankFloat ms_fDefaultWanderPenaltyForZeroPedDensity;
	static BankFloat ms_fDefaultMoveOntoSteepSurfacePenalty;
	static BankFloat ms_fDefaultPenaltyForNoDirectionalCover;
	static BankFloat ms_fDefaultPenaltyForFavourCoverPerUnsetBit;
	static BankInt32 ms_iDefaultPenaltyForMovingToLowerPedDensity;
	static BankFloat ms_fDefaultAvoidTrainTracksPenalty;

	static BankFloat ms_fFleePathDoubleBackCost;
	static BankFloat ms_fFleePathDoubleBackCostSofter;
	static BankFloat ms_fFleePathDoubleBackMultiplier;
};

//******************************************************************
//	CPathServerRequestBase
//	This struct is the base class for any request to the PathServer.
//	Requests will accumulate in a queue until the CPathServerThread
//	can process them and make the results available for the client.
//******************************************************************

#define PATHREQUEST_MAX_INCLUDE_OBJECTS						8
#define PATHREQUEST_MAX_EXCLUDE_OBJECTS						8

class CPathServerRequestBase
{
public:
	CPathServerRequestBase()
	{
		m_NavDomain = kNavDomainRegular;
	}
	virtual ~CPathServerRequestBase() { }

	aiNavDomain GetMeshDataSet() const
	{	return m_NavDomain;	}


	inline void Reset()
	{
		m_hHandle = PATH_HANDLE_NULL;
		m_bRequestPending = false;
		m_bRequestActive = false;
		m_bComplete = true;
		m_bSlotEmpty = true;
		m_bWasAborted = false;
	};

	virtual void Clear()
	{
		m_hHandle = PATH_HANDLE_NULL;
		m_iCompletionCode = PATH_NO_ERROR;
		m_iTimeRequestIssued = 0;

		m_bRequestPending = false;
		m_bRequestActive = false;
		m_bComplete = false;
		m_bSlotEmpty = true;
		m_bWasAborted = false;
		m_bWaitingToAbort = false;

		m_NavDomain = kNavDomainRegular;

		m_iNumExcludeObjects = 0;
		memset(m_ExcludeObjects, 0, sizeof(TDynamicObjectIndex)*PATHREQUEST_MAX_EXCLUDE_OBJECTS);

		m_fMaxAngle = 0.0f;				
		m_fDistAheadOfPed = 0.0f;

#ifndef NAVGEN_TOOL
		m_PedWaitingForThisRequest = NULL;
		m_EntityEndPosition = NULL;
#endif
	};

	inline bool IsReadyForUse() const
	{
		return (m_bSlotEmpty && !m_bWaitingToAbort);
	}

	static const u32 ms_iRequestTimeoutValInMillisecs;

public:
	sysCriticalSectionToken m_CriticalSectionToken;	// Changed from spinlock to critical section, in an attempt to foil bugs like url:bugstar:1932005
public:
	TPathHandle m_hHandle;						// Unique identifier of this request
	s32 m_iType;								// One of the 'EPathServerRequestType' enumeration types
	EPathServerErrorCode m_iCompletionCode;		// Result code
	u32 m_iTimeRequestIssued;					// Time in msecs that this request was issued

	u32 m_bRequestPending	:1;				// Whether this request is pending
	u32 m_bRequestActive	:1;				// Whether this request is currently being processed
	u32 m_bComplete			:1;				// Whether this request has been processed
	u32 m_bSlotEmpty		:1;				// Set when this slot can be re-used
	u32 m_bWasAborted		:1;				// Path was aborted
	u32 m_bWaitingToAbort	:1;				// This request has been cancelled, but the pathsever thread has not yet aborted processing it

	aiNavDomain m_NavDomain;				// Data set to use for the request.
	
	float m_fMaxAngle;						// Why is this here?  It should be in CPathRequest, and not this base class
	float m_fDistAheadOfPed;				// Why is this here?  It should be in CPathRequest, and not this base class
	s32 m_iNumExcludeObjects;
	TDynamicObjectIndex m_ExcludeObjects[PATHREQUEST_MAX_EXCLUDE_OBJECTS];
#ifndef NAVGEN_TOOL
	fwRegdRef<fwEntity> m_PedWaitingForThisRequest;
	fwRegdRef<const fwEntity> m_EntityEndPosition;
	//RegdPed m_PedWaitingForThisRequest;			// We have to store this so that we can turn off AI timeslicing when the request is ready
#endif
#if !__FINAL
	u32 m_iNumTimesSlept;						// Number of times that this request called sysIpcYield() during processing
	void * m_pContext;							// This can hold a ped's address for debugging purposes..
	TPathHandle m_hHandleThisWas;				// Handle which was in this slot
	float m_fTotalProcessingTimeInMillisecs;	// The total time that this request took to process, successful or not
	u32 m_iFrameRequestIssued;					// The frame the request was issued on
	u32 m_iFrameRequestStarted;					// The frame the request was started proceesing on
	u32 m_iFrameRequestCompleted;				// The frame the request was completed
#endif
};


class CGridRequest : public CPathServerRequestBase
{
public:
	CGridRequest(void) { }
	virtual ~CGridRequest(void) { }

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		memset(&m_WalkRndObjGrid, 0, sizeof(CWalkRndObjGrid));
	}

	CWalkRndObjGrid m_WalkRndObjGrid;				// Grid of walkable cells centered around a ped

#if !__FINAL
	float m_fMillisecsToFindPolys;					// How long it took to flood-fill outwards & find the polys for the operation
	float m_fMillisecsToSampleGrid;					// How long it took to sample points from the polys into the grid
#endif
};


#define MAX_LINEOFSIGHT_POINTS	16

class CLineOfSightRequest : public CPathServerRequestBase
{
public:
	CLineOfSightRequest(void) { }
	virtual ~CLineOfSightRequest(void) { }

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		memset(m_vPoints, 0, sizeof(Vector3)*MAX_LINEOFSIGHT_POINTS);
		memset(m_bLosResults, 0, sizeof(bool)*MAX_LINEOFSIGHT_POINTS);

		m_iNumPts = 0;
		m_fRadius = 0.0f;
		m_bQuitAtFirstLosFail = false;
		m_bDynamicObjects = false;
		m_bNoLosAcrossWaterBoundary = false;
		m_bStartsInWater = false;
		m_bLineOfSightExists = false;
		m_vFirstIntersectPoint.Zero();

	}

	Vector3 m_vPoints[MAX_LINEOFSIGHT_POINTS];		// A poly-line of points to test.  (eg. 3 points define two line-tests : [0->1] then [1->2])
	bool m_bLosResults[MAX_LINEOFSIGHT_POINTS];		// The result of whether each individual line segments was clear

	s32 m_iNumPts;								// How many points.  2 points define a single line segment, every point thereafter equals another lineseg
	float m_fRadius;								// Radius, or thickness of the lines (NB : NOT YET IMPLEMENTED)
	bool m_bQuitAtFirstLosFail;						// Whether to quit as soon as the first line-test fails, saving CPU time on subsequent tests
	bool m_bDynamicObjects;							// Whether to include dynamic objects in the test
	bool m_bNoLosAcrossWaterBoundary;
	bool m_bStartsInWater;

	bool m_bLineOfSightExists;						// Overall result of LOS, for all input points.

	Vector3 m_vFirstIntersectPoint;					// The point of the first intersection (NB : NOT YET IMPLEMENTED)

#if !__FINAL
	float m_fMillisecsToFindPolys;					// How long it took to find the polygons under the start & end points of the line
	float m_fMillisecsToFindLineOfSight;			// How long it took to perform the LOS itself
#endif
};

#define MAX_PATH_POLYS										512

class CPathRequest : public CPathServerRequestBase
{
public:
	CPathRequest()
	{

	}
	virtual ~CPathRequest() { }

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		m_bUseBestAlternateRouteIfNoneFound = false;
		m_bPreferPavements = false;
		m_bNeverLeavePavements = false;
		m_bNeverClimbOverStuff = false;
		m_bNeverDropFromHeight = false;
		m_bMayUseFatalDrops = false;
		m_bNeverUseLadders = false;
		m_bAllowFirstPointToBeInsideDynamicObject = false;
		m_bFleeTarget = false;
		m_bWander = false;
		m_bNeverEnterWater = false;
		m_bClimbObjects = false;
		m_bPushObjects = false;
		m_bDontAvoidDynamicObjects = false;
		m_bEndPointWasAdjusted = false;
		m_bProblemPathStartsAndEndsOnSamePoly = false;
		m_bPreferDownHill = false;
		m_bGoAsFarAsPossibleIfNavMeshNotLoaded = false;
		m_bSmoothSharpCorners = false;
		m_bDoPostProcessToPreserveSlopeInfo = false;
		m_bScriptedRoute = false;
		m_bReduceObjectBoundingBoxes = false;
		m_bUseLargerSearchExtents = false;
		m_bDontLimitSearchExtents = false;
		m_bDynamicNavMeshRoute = false;
		m_bIfStartNotOnPavementAllowDropsAndClimbs = false;
		m_bNeverStartInWater = false;
		m_bIgnoreNonSignificantObjects = false;
		m_bIgnoreTypeVehicles = false;
		m_bIgnoreTypeObjects = false;
		m_bFleeNeverEndInWater = false;
		m_bConsiderFreeSpaceAroundPoly = false;
		m_bUseMaxSlopeNavigable = false;
		//
		m_bRandomisePoints = false;
		m_bUseDirectionalCover = false;
		m_bFavourEnclosedSpaces = false;
		m_bUseVariableEntityRadius = false;
		m_bAvoidPotentialExplosions = false;
		m_bAvoidTearGas = false;	
		m_bAllowToNavigateUpSteepPolygons = false;
		m_bAllowToPushVehicleDoorsClosed = false;
		m_bMissionPed = false;
		m_bDeactivateObjectsIfCantResolveEndPoints = false;
		m_bDontAvoidFire = false;
		m_bHighPrioRoute = false;
		m_bNeverLeaveWater = false;
		m_bNeverLeaveDeepWater = false;
		m_bEnsureLosBeforeEnding = false;
		m_bExpandStartEndPolyTessellationRadius = false;
		m_bKeepUpdatingPedStartPosition = false;
		m_bHasAdjustedDynamicObjectsMinMaxForWidth = false;
		m_bCoverFinderPath = false;
		m_bPullFromEdgeExtra = false;
		m_bSofterFleeHeuristics = false;
		m_bAvoidTrainTracks = false;

		m_iTimeRequestIssued = 0;
		m_iNumPoints = 0;
		m_vUnadjustedStartPoint.Zero();
		m_vUnadjustedEndPoint.Zero();
		m_vPathStart.Zero();
		m_vPathEnd.Zero();
		m_fPathSearchCompletionRadius = 0.0f;

		m_fEntityRadius = 0.0f;

		m_StartNavmeshAndPoly.Reset();

		m_fDistanceBelowStartToLookForPoly = 0.0f;
		m_fDistanceAboveStartToLookForPoly = 0.0f;
		m_fDistanceBelowEndToLookForPoly = 0.0f;
		m_fDistanceAboveEndToLookForPoly = 0.0f;

		m_vCoverOrigin.Zero();
		m_vPolySearchDir.Zero();
		m_vReferenceVector.Zero();
		m_fInitialReferenceDistance = 0.0f;
		m_fReferenceDistance = 0.0f;

		m_fMaxSlopeNavigable = 0.0f;
		m_fClampMaxSearchDistance = 0.0f;

		m_fMaxDistanceToAdjustPathStart = 0.0f;
		m_fMaxDistanceToAdjustPathEnd = 0.0f;

		memset(m_PathPoints, 0, sizeof(Vector3)*(MAX_NUM_PATH_POINTS+1));
		memset(m_WaypointFlags, 0, sizeof(TNavMeshWaypointFlag)*(MAX_NUM_PATH_POINTS+1));
		memset(m_PathPolys, 0, sizeof(TNavMeshPoly)*(MAX_NUM_PATH_POINTS+1));

		m_iNumInfluenceSpheres = 0;
		memset(m_InfluenceSpheres, 0, sizeof(TInfluenceSphere)*MAX_NUM_INFLUENCE_SPHERES);

		m_iNumIncludeObjects = 0;
		memset(m_IncludeObjects, 0, sizeof(TDynamicObjectIndex)*PATHREQUEST_MAX_INCLUDE_OBJECTS);

		m_iIndexOfDynamicNavMesh = NAVMESH_NAVMESH_INDEX_NONE;
		m_iPedRandomSeed = 0;

		memset(&m_PathResultInfo, 0, sizeof(TPathResultInfo));
		memset(&m_MovementCosts, 0, sizeof(CPathFindMovementCosts));
	}

	u32 m_bUseBestAlternateRouteIfNoneFound			:1;
	u32 m_bPreferPavements							:1;
	u32 m_bNeverLeavePavements						:1;
	u32 m_bNeverClimbOverStuff						:1;
	u32 m_bNeverDropFromHeight						:1;
	u32 m_bMayUseFatalDrops							:1;
	u32 m_bNeverUseLadders							:1;
	u32 m_bAllowFirstPointToBeInsideDynamicObject	:1;	// [IGNORED] Useful for wander paths, when a ped has just got up from a seated attractor (park benched, etc)
	u32 m_bFleeTarget								:1;	// The path is to flee from the start point, away from the reference point
	u32 m_bWander									:1;	// The path is to wander is direction of reference vector, by a minimum reference amount
	u32 m_bNeverEnterWater							:1; // The path must not pass from land onto water
	u32 m_bClimbObjects								:1;	// The path will pass over objects which may be climbed or walked over, at a penalty
	u32 m_bPushObjects								:1;	// [IGNORED] The path will push past objects which light enough to be moved aside, at a penalty
	u32 m_bDontAvoidDynamicObjects					:1;	// The path will not avoid dynamic objects (but the ped may still do their own entity-avoidance)
	u32 m_bEndPointWasAdjusted						:1;	// [IGNORED] Whether the endpoint was adjusted
	u32 m_bProblemPathStartsAndEndsOnSamePoly		:1;	// [OBSOLETE?] We can handle this now via tessellation
	u32 m_bPreferDownHill							:1;	// Skiers like to navigate downhill
	u32 m_bGoAsFarAsPossibleIfNavMeshNotLoaded		:1; // If the endpos is not on a loaded navmesh, then adjust it to be on the closest loaded navmesh to the endpoint
	u32 m_bSmoothSharpCorners						:1; // Smooth the sharp corners off a path using line-of-sight tests
	u32 m_bDoPostProcessToPreserveSlopeInfo			:1; // Normally we lose z-change info during string-pulling.  If this is set, then we reintroduce waypoints where the slope under the path changes	
	u32 m_bScriptedRoute							:1;
	u32 m_bReduceObjectBoundingBoxes				:1;	// If there is a problem finding a path, we can reduce the dynamic objects' bboxes to try again
	u32 m_bUseLargerSearchExtents					:1;	// Use a larger min/max search extents than normal.
	u32 m_bDontLimitSearchExtents					:1;	// Don't set the min/max extents for this path.  Use with care, as this could bog down the pathfinder!
	u32 m_bDynamicNavMeshRoute						:1;	// This route is on a dynamic navmesh
	u32 m_bIfStartNotOnPavementAllowDropsAndClimbs	:1; // Special-case flag for wander paths, to prevent peds getting stranded on things after fleeing
	u32 m_bNeverStartInWater						:1;	// Quit the path search immediately if the peds starts in water
	u32 m_bIgnoreNonSignificantObjects				:1;	// Ignore all objects which aren't stuck down!
	u32 m_bIgnoreTypeVehicles						:1;	// Ignore all CVehicle
	u32 m_bIgnoreTypeObjects						:1;	// Ignore all CObject
	u32 m_bFleeNeverEndInWater						:1;	// If this is a flee a path it should never end in water
	u32 m_bConsiderFreeSpaceAroundPoly				:1; // Whether this path's heuristic should factor in each poly's "free space around poly" integer value
	u32 m_bUseMaxSlopeNavigable						:1; // If this is set then paths cannot navigate from below onto slopes steeper than m_fMaxSlopeNavigable
	//
	u32 m_bRandomisePoints							:1; // Randomises the selection of points based on m_iPedRandomSeed - done within MinimisePathDist()
	u32 m_bUseDirectionalCover						:1; // Factor the directional cover value per-polygon into the pathsearch
	u32 m_bFavourEnclosedSpaces						:1;	// Favour enclosed spaces - defined by the number of m_iCoverDirectionsBitfield bits set
	u32 m_bUseVariableEntityRadius					:1; // If set then m_fEntityRadius is greater than PATHSERVER_PED_RADIUS, the default radius for navigation
	u32 m_bAvoidPotentialExplosions					:1; // Influence spheres will be set up around explosive fires
	u32 m_bAvoidTearGas								:1; // Influence spheres will be set up around tear gas						
	u32 m_bAllowToNavigateUpSteepPolygons			:1; // If specified allows the pathsearch to traverse up steep polygons
	u32 m_bAllowToPushVehicleDoorsClosed			:1; // Path may pass through vehicle doors if coming from the front
	u32 m_bMissionPed								:1;	// Mission ped, so enable extra points in each polygon when navigating
	u32 m_bDeactivateObjectsIfCantResolveEndPoints	:1;	// [IGNORED] If we can't move start/end points outside of objects, deactivate those intersecting (with some exceptions)
	u32 m_bDontAvoidFire							:1; // If this is set, peds will not bother to pathfind around fire objects
	u32 m_bHighPrioRoute							:1; // Route has a high priority and will be processed in preference of those with lower priorities
	u32 m_bNeverLeaveWater							:1; // Path will never leave water
	u32 m_bNeverLeaveDeepWater						:1; // Path will never leave water, or transition to a poly marked shallow
	u32 m_bEnsureLosBeforeEnding					:1; // 
	u32 m_bExpandStartEndPolyTessellationRadius		:1; // Tessellate polys in an increased area around the path start/end points
	u32 m_bKeepUpdatingPedStartPosition				:1; // Update the path's start position per-frame whilst the path request is still pending
	u32 m_bHasAdjustedDynamicObjectsMinMaxForWidth	:1; // 
	u32 m_bCoverFinderPath							:1; // Path request was issued as part of the AI cover-finding
	u32 m_bPullFromEdgeExtra						:1; 
	u32 m_bSofterFleeHeuristics						:1; 
	u32 m_bAvoidTrainTracks							:1; // Path prefers not to use polygons marked as train tracks (but doesn't completely block their use)
	// NOTE: If adding new bitflag members here, make sure you reset them in the Reset() function above also


	u32 m_iTimeRequestIssued;							// Time in millisecs that this request was issued
	s32 m_iNumPoints;
	Vector3 m_vUnadjustedStartPoint;					// The unadjusted endpoint
	Vector3 m_vUnadjustedEndPoint;						// The unadjusted endpoint
	Vector3 m_vPathStart;
	Vector3 m_vPathEnd;
	float m_fPathSearchCompletionRadius;				// If an exact path can't be found, one within this radius of the target will do

	float m_fEntityRadius;								// If zero, the entity is PED_RADIUS.  If greater than zero then it's radius is equal to (PED_RADIUS + m_fEntityRadius)

	TNavMeshAndPoly m_StartNavmeshAndPoly;				// This may be set if known, to bypass the search for a starting polygon

	float m_fDistanceBelowStartToLookForPoly;			// Distances to look up/down for navmesh polys
	float m_fDistanceAboveStartToLookForPoly;
	float m_fDistanceBelowEndToLookForPoly;
	float m_fDistanceAboveEndToLookForPoly;

	Vector3 m_vCoverOrigin;									// The direction from which to look for cover
	Vector3 m_vPolySearchDir;								// If we want to find closest poly weighting the direction
	Vector3 m_vReferenceVector;								// This can be used to steer the search algorithm
	float m_fInitialReferenceDistance;						// The distance value passed in
	float m_fReferenceDistance;								// The current distance value, this may be modifed within the search

	float m_fMaxSlopeNavigable;								// The maximum slope which can be moved onto from below (from above is fine).  The angle deviation from (0,1,0) in radians.

	float m_fClampMaxSearchDistance;						// If non-zero, this defines the maximum search distance which this path may go.
															// Regardless of this value, the path will never extend above the maximum clamp decided internally by the pathfinder.
															// So this intention of this value is to allow path-searches to be cut off at small distances when required.
	float m_fMaxDistanceToAdjustPathStart;
	float m_fMaxDistanceToAdjustPathEnd;

	Vector3 m_PathPoints[MAX_NUM_PATH_POINTS+1];					// The final points
	TNavMeshWaypointFlag m_WaypointFlags[MAX_NUM_PATH_POINTS+1];	// Flags for each of the above points (eg. whether to climb, jump, open a door etc at this point)
	TNavMeshPoly * m_PathPolys[MAX_NUM_PATH_POINTS+1];				// The polys which each of the above points sits upon

	s32 m_iNumInfluenceSpheres;										// Spheres of influence to avoid or favour when creating the path
	TInfluenceSphere m_InfluenceSpheres[MAX_NUM_INFLUENCE_SPHERES];

	s32 m_iNumIncludeObjects;
	TDynamicObjectIndex m_IncludeObjects[PATHREQUEST_MAX_INCLUDE_OBJECTS];

	TNavMeshIndex m_iIndexOfDynamicNavMesh;							// Dynamic navmesh within which this path exists
	u32 m_iPedRandomSeed;											// The unique random seed for this ped, used to randomize path points

	// This is some extra data (bitflags) about the path which was found.  It is passed back to the user when
	// the path result is obtained.
	TPathResultInfo m_PathResultInfo;
	CPathFindMovementCosts m_MovementCosts;

#if !__FINAL
	float m_fMillisecsToFindPolys;								// How long the initial quadtree raycasts took to find the polys underneath m_vPathStart & m_vPathEnd
	float m_fMillisecsToFindPath;								// How long the A* triangle path took to find, including tessellation, etc
	float m_fMillisecsToRefinePath;								// How long it took to refine the path (ie. perform 'string-pulling' and removal of unnecessary points)
	float m_fMillisecsToPostProcessZHeights;					// how long it took to add points where the height changed
	float m_fMillisecsToSmoothPath;								// How long it took to smooth the path; chopping off sharp corners
	float m_fMillisecsToMinimisePathLength;						// How long it took to minimise the path length by repositioning vertex placement within the larger polygons
	float m_fMillisecsToPullOutFromEdges;
	float m_fMillisecsSpentInTessellation;						// How long was spent specifically in the tessellation function
	float m_fMillisecsSpentInDetessellation;					// How long was spend in DetesellateAllPolys at the end of the path request?
	u32 m_iNumInitiallyFoundPolys;							// The polys which make up the initial path which the pathfinder found
	u32 m_iNumVisitedPolygons;								// How many polygons were visited during the path search
	u32 m_iNumTessellations;
	u32 m_iNumTessellatedPolys;
	u32 m_iNumTestDynamicObjectLOS;
	u32 m_iNumTestNavMeshLOS;
	u32 m_iNumGetObjectsIntersectingRegion;
	u32 m_iNumCacheHitsOnDynamicObjects;
	u32 m_iNumFindPathIterations;
	u32 m_iNumRefinePathIterations;
	const char * m_pSavePathFileName;
#endif

#if __STORE_POLYS_IN_PATH_REQUEST
	TNavMeshPoly * m_InitiallyFoundPathPolys[MAX_PATH_POLYS];
	Vector3 m_InitiallyFoundPathPointInPolys[MAX_PATH_POLYS];
#endif

};	// 875 bytes unpadded (in __FINAL build)

// EPathFindRetVal - used internally within the pathfinder
enum EPathFindRetVal
{
	EPathNotFound,
	EPathFound,
	EPathFoundEarlyOut
};

#define MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES	24

class CAudioRequest : public CPathServerRequestBase
{
public:
	CAudioRequest() { }
	virtual ~CAudioRequest() { }

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		m_vPosition.Zero();
		m_vDirection.Zero();
		m_KnownNavmeshPosition.Reset();
		m_fRadius = 0.0f;
		m_iAudioReqType = 0;
		m_iNumAudioProperties = 0;
		m_bPriorityRequest = false;

		memset(m_iAudioProperties, 0, sizeof(u32)*MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES);
		memset(m_fPolyAreas, 0, sizeof(float)*MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES);
		memset(m_vPolyCentroids, 0, sizeof(Vector3)*MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES);
		memset(m_fDistSqrFromOrigin, 0, sizeof(float)*MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES);
		memset(m_iAdditionalFlags, 0, sizeof(u8)*MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES);
	}


	enum AudioReqType
	{
		NO_DIRECTION_NO_RADIUS		= 0,
		DIRECTION_NO_RADIUS			= 1,
		RADIUS_NO_DIRECTION			= 2,
		DIRECTION_AND_RADIUS		= 3
	};

	enum Flags
	{
		FLAG_POLYGON_IS_SHELTERED	=	0x01
	};

	Vector3 m_vPosition;
	Vector3 m_vDirection;
	TNavMeshAndPoly m_KnownNavmeshPosition;
	float m_fRadius;

	s32 m_iAudioReqType;

	s32 m_iNumAudioProperties;
	bool m_bPriorityRequest;

	// The first entry is always the poly directly under the m_vPosition, or closest to it
	u32 m_iAudioProperties[MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES];
	float m_fPolyAreas[MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES];
	Vector3 m_vPolyCentroids[MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES];
	float m_fDistSqrFromOrigin[MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES];
	u8 m_iAdditionalFlags[MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES];

	bool InsertAudioProperty(const float fDistSqr, const u32 iValue, const Vector3 & vPosition, const float fPolyArea, const u8 iAdditionalFlags);
};

#define AUDIO_PROPERTIES_FLOODFILL_MAXPOLYS		32

struct TAudioPropertiesFloodFill
{
	s32 m_iNumPolys;
	float m_fPolyCentroids[AUDIO_PROPERTIES_FLOODFILL_MAXPOLYS*3];
	float m_fPolyAreas[AUDIO_PROPERTIES_FLOODFILL_MAXPOLYS];
	s8 m_fPolyAudioProperties[AUDIO_PROPERTIES_FLOODFILL_MAXPOLYS];
};

struct TFindClosestCarNode
{
	inline Vector3 GetCarNodePos() { return Vector3(m_fCarNodePosition[0], m_fCarNodePosition[1], m_fCarNodePosition[2]); }
	inline void SetCarNodePos(const Vector3 & vPos) { m_fCarNodePosition[0] = vPos.x; m_fCarNodePosition[1] = vPos.y; m_fCarNodePosition[2] = vPos.z; }
	bool m_bFoundCarNode;
	float m_fCarNodePosition[3];
	float m_fClosestCarNodeDistSqr;
};

struct TFindClosestShelteredPoly
{
	inline Vector3 GetPos() { return Vector3(m_fShelteredPosition[0], m_fShelteredPosition[1], m_fShelteredPosition[2]); }
	inline void SetPos(const Vector3 & vPos) { m_fShelteredPosition[0] = vPos.x; m_fShelteredPosition[1] = vPos.y; m_fShelteredPosition[2] = vPos.z; }
	bool m_bFound;
	float m_fShelteredPosition[3];
	float m_fClosestDistSqr;
};

struct TFindClosestUnshelteredPoly
{
	inline Vector3 GetPos() { return Vector3(m_fShelteredPosition[0], m_fShelteredPosition[1], m_fShelteredPosition[2]); }
	inline void SetPos(const Vector3 & vPos) { m_fShelteredPosition[0] = vPos.x; m_fShelteredPosition[1] = vPos.y; m_fShelteredPosition[2] = vPos.z; }
	float m_fShelteredPosition[3];
	float m_fClosestDistSqr;
	u32 m_startPolyIndex;
	TNavMeshIndex m_startNavMeshIndex;
};

struct TFindClosestPositionOnLand
{
	inline Vector3 GetPos() { return Vector3(m_fPosition[0], m_fPosition[1], m_fPosition[2]); }
	inline void SetPos(const Vector3 & vPos) { m_fPosition[0] = vPos.x; m_fPosition[1] = vPos.y; m_fPosition[2] = vPos.z; }
	bool m_bFound;
	float m_fPosition[3];
	float m_fClosestDistSqr;
};

struct TCalcAreaUnderfoot
{
	float m_fArea;
};

struct TFindNearbyPavement
{
	bool m_bFound;
};

union FloodFillData
{
	TAudioPropertiesFloodFill AudioProperties;
	TFindClosestCarNode	ClosestCarNode;
	TFindClosestShelteredPoly ClosestShelteredPoly;
	TCalcAreaUnderfoot CalcAreaUnderfoot;
	TFindClosestUnshelteredPoly ClosestUnshelteredPoly;
	TFindClosestPositionOnLand ClosestPositionOnLand;
	TFindNearbyPavement			NearbyPavement;

};


class CFloodFillRequest;
typedef bool (*FloodFill_ShouldVisitThisPolyFn)(const TNavMeshPoly * pAdjPoly, const CNavMesh * pAdjNavMesh, const TNavMeshPoly * pFromPoly, const CNavMesh * pFromNavMesh, const TAdjPoly * adjacency);
typedef bool (*FloodFill_ShouldEndOnThisPolyFn)(TNavMeshPoly * pAdjPoly, CNavMesh * pAdjNavMesh, CFloodFillRequest * pFloodFillRequest, const float fDistanceTravelled);

class CFloodFillRequest : public CPathServerRequestBase
{
public:
	enum EType
	{
		EAudioPropertiesFloodFill,
		EFindClosestCarNodeFloodFill,
		EFindClosestShelteredPolyFloodFill,
		EFindClosestUnshelteredPolyFloodFill,
		ECalcAreaUnderfoot,
		EFindClosestPositionOnLand,
		EFindNearbyPavementFloodFill
	};

	CFloodFillRequest() { }
	virtual ~CFloodFillRequest() { }

	TAudioPropertiesFloodFill * GetAudioPropertiesData() { return &(m_Data.AudioProperties); }
	TFindClosestCarNode * GetFindClosestCarNodeData() { return &(m_Data.ClosestCarNode); }
	TFindClosestShelteredPoly * GetFindClosestShelteredPolyData() { return &(m_Data.ClosestShelteredPoly); }
	TCalcAreaUnderfoot * GetCalcAreaUnderfootData() { return &(m_Data.CalcAreaUnderfoot); }
	TFindClosestUnshelteredPoly * GetFindClosestUnshelteredPolyData() { return &(m_Data.ClosestUnshelteredPoly); }
	TFindClosestPositionOnLand * GetFindClosestPositionOnLandData() { return &(m_Data.ClosestPositionOnLand); }
	TFindNearbyPavement*			GetFindNearbyPavementPolyData()		{ return &(m_Data.NearbyPavement);}

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		m_vStartPos.Zero();
		m_fMaxRadius = 0.0f;
		m_FloodFillType = 0;
		m_pShouldVisitPolyFn = NULL;
		m_pShouldEndOnPolyFn = NULL;

		m_bConsiderDynamicObjects = false;
		m_bUseClimbsAndDrops = false;
		m_bUseLadders = false;

		memset(&m_Data, 0, sizeof(FloodFillData));
	}

	Vector3 m_vStartPos;
	float m_fMaxRadius;
	int m_FloodFillType;
	FloodFill_ShouldVisitThisPolyFn m_pShouldVisitPolyFn;
	FloodFill_ShouldEndOnThisPolyFn m_pShouldEndOnPolyFn;

	u32 m_bConsiderDynamicObjects		:1;
	u32 m_bUseClimbsAndDrops			:1;
	u32 m_bUseLadders					:1;

	FloodFillData m_Data;
};


class CClearAreaRequest : public CPathServerRequestBase
{
public:
	CClearAreaRequest() { }
	virtual ~CClearAreaRequest() { }

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		m_vSearchOrigin.Zero();
		m_fSearchRadiusXY = 0.0f;
		m_fSearchDistZ = 0.0f;
		m_fDesiredClearAreaRadius = 0.0f;
		m_fMinimumDistanceFromOrigin = 0.0f;

		m_vResultOrigin.Zero();

		m_bConsiderDynamicObjects = false;
		m_bConsiderInterior = false;
		m_bConsiderExterior = false;
		m_bConsiderWater = false;
		m_bConsiderSheltered = false;
	}

	Vector3 m_vSearchOrigin;
	float m_fSearchRadiusXY;
	float m_fSearchDistZ;
	float m_fDesiredClearAreaRadius;
	float m_fMinimumDistanceFromOrigin;

	Vector3 m_vResultOrigin;

	u32 m_bConsiderDynamicObjects	:1;
	u32 m_bConsiderInterior			:1;
	u32 m_bConsiderExterior			:1;
	u32 m_bConsiderWater			:1;
	u32 m_bConsiderSheltered		:1;


#if !__FINAL

#endif
};


class CClosestPositionRequest : public CPathServerRequestBase
{
public:
	static const s32 MAX_NUM_IGNORE_SPHERES	= 4;
	static const s32 MAX_NUM_RESULTS		= 16;

	enum Flags
	{
		Flag_ConsiderDynamicObjects		= 0x01,
		Flag_ConsiderInterior			= 0x02,
		Flag_ConsiderExterior			= 0x04,
		Flag_ConsiderOnlyLand			= 0x08,
		Flag_ConsiderOnlyPavement		= 0x10,
		Flag_ConsiderOnlyNonIsolated	= 0x20,
		Flag_ConsiderOnlySheltered		= 0x40,
		Flag_ConsiderOnlyNetworkSpawn	= 0x80,
		Flag_ConsiderOnlyWater			= 0x100
	};

	struct TResults
	{
		s32 m_iNumResults;
		atRangeArray<Vector3,MAX_NUM_RESULTS> m_vResults;
	};

public:
	CClosestPositionRequest() { }
	virtual ~CClosestPositionRequest() { }

	virtual void Clear()
	{
		CPathServerRequestBase::Clear();

		m_vSearchOrigin.Zero();
		m_fSearchRadius = 0.0f;

		memset(m_AvoidSpheres, 0, sizeof(spdSphere)*MAX_NUM_IGNORE_SPHERES);
		m_iNumAvoidSpheres = 0;

		m_bConsiderDynamicObjects = false;
		m_bConsiderInterior = false;
		m_bConsiderExterior = false;
		m_bConsiderOnlyLand = false;
		m_bConsiderOnlyWater = false;
		m_bConsiderOnlyPavement = false;
		m_bConsiderOnlyNonIsolated = false;
		m_bConsiderOnlySheltered = false;
		m_bConsiderOnlyNetworkSpawn = false;

		m_fZWeightingAbove = 0.0f;
		m_fZWeightingAtOrBelow = 0.0f;

		m_fMinimumSpacing = 0.0f;

		m_iMaxResults = 0;

		memset(&m_Results, 0, sizeof(TResults));
	}

	Vector3 m_vSearchOrigin;
	float m_fSearchRadius;

	spdSphere m_AvoidSpheres[MAX_NUM_IGNORE_SPHERES];
	s32 m_iNumAvoidSpheres;

	u32 m_bConsiderDynamicObjects		:1;
	u32 m_bConsiderInterior				:1;
	u32 m_bConsiderExterior				:1;
	u32 m_bConsiderOnlyLand				:1;
	u32 m_bConsiderOnlyWater			:1;
	u32 m_bConsiderOnlyPavement			:1;
	u32 m_bConsiderOnlyNonIsolated		:1;
	u32 m_bConsiderOnlySheltered		:1;
	u32 m_bConsiderOnlyNetworkSpawn		:1;

	float m_fZWeightingAbove;
	float m_fZWeightingAtOrBelow;

	float m_fMinimumSpacing;

	s32 m_iMaxResults;

	TResults m_Results;

#if !__FINAL

#endif
};

}	// namespace rage

#endif	// AI_NAVMESH_REQUESTS_H
