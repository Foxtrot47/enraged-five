#ifndef NAVMESHBASE_H
#define NAVMESHBASE_H

//*********************************************************************************
//
//	Filename : NavMesh.h
//	Author : James Broad
//	RockstarNorth (c) 2005
//
//	------------------------------------------------------------------------------
//	
//	Overview:
//	---------  
//
//	This implements a navigation mesh which defines all the areas upon which
//	characters can walk.  Polygon adjacency info is used to allow us to perform
//	an approximate path search across the mesh, and then optimised 2d line-of-sight
//	tests are performed across the mesh to refine the path to a minimal set of
//	waypoints.
//	Each CNavMesh occupies the space of a small block of sectors (4x4, 8x8) and
//	they are managed by the CPathServer class.
//	CNavMesh classes may be streamed in/out at any time between path requests,
//	but never whilst a search is active.
//
//******************************************************************************

#include "ai/navmesh/navmeshoptimisations.h"
#include "ai/navmesh/shortminmax.h"
#include "ai/navmesh/splitarray.h"

// define NAVGEN_TOOL in preprocessor settings to be able to run CPathServer without the whole GTA codebase
#ifndef NAVGEN_TOOL
#define GTA_ENGINE
#endif

#ifndef COMPILE_TOOL_MESH

namespace rage {

class CHierarchicalNode;
class CNavGen;
class CNavMesh;
class spdSphere;

}

#ifdef GTA_ENGINE
#include "navmeshchannel.h"
#else
#define pathAssertf	Assertf
#endif

// Rage includes
#include "atl/array.h"
#include "data/base.h"
#include "data/struct.h"
#include "file/stream.h"
#include "math/amath.h"
#include "paging/base.h"
#include "spatialdata/sphere.h"
#include "system/cache.h"
#include "system/memory.h"
#include "system/performancetimer.h"
#include "vector/color32.h"
#include "vector/matrix34.h"
#include "vector/vector3.h"

namespace rage {

class fiStream;

#endif	// COMPILE_TOOL_MESH

#define NAMESH_ALLOCATION_BLOCKSIZE									16384

#define __ENSURE_THAT_POLY_QUICK_CENTROIDS_ARE_REALLY_WITHIN_POLYS	0	//NAVMESH_OPTIMISATIONS_OFF
#define __NAVMESH_USE_PREFETCHING									0
#define __UPDATE_PATHSERVER_DYNOBJECTS_IN_UPDATEENTITYMATRIX		1
#define __TRACK_PEDS_IN_NAVMESH										1

// If __CHECK_NAVMESH_FILE_VERSION is non-zero then LoadBinary() returns NULL & asserts for mismatched versions
#define __CHECK_NAVMESH_FILE_VERSION								__DEV && 0 // << disabled for now to enable backward compatibility
#define __NAVMESH_SANITY_CHECK_ADJACENCIES							__DEV && 0
#define __HIERARCHICAL_NODES_ENABLED								0
#define __DEFRAG_NAVMESHES											1

#ifndef GTA_ENGINE
//#include "system\xtl.h"	// Solves masses of compile errors (warning C4668). Read the RAGE 0.4 "readme.txt" for why this is needed.
#ifndef AI_OPTIMISATIONS_OFF
#define AI_OPTIMISATIONS_OFF				1
#endif
#ifndef AI_VEHICLE_OPTIMISATIONS_OFF
#define AI_VEHICLE_OPTIMISATIONS_OFF		1
#endif
#ifndef AI_NAVIGATION_OPTIMSATIONS_OFF
#define AI_NAVIGATION_OPTIMSATIONS_OFF		1
#endif
#ifndef NAVMESH_OPTIMISATIONS_OFF
#define NAVMESH_OPTIMISATIONS_OFF			0
#endif
#endif	// GTA_ENGINE

#if NAVMESH_OPTIMISATIONS_OFF
#define NAVMESH_OPTIMISATIONS_OFF_ONLY(n)	n
#else
#define NAVMESH_OPTIMISATIONS_OFF_ONLY(n)
#endif

#define GCC_VERSION (__GNUC__ * 10000 \
	+ __GNUC_MINOR__ * 100 \
	+ __GNUC_PATCHLEVEL__)

//  Data type of a navmesh index.  Note that this really only has a maximum of 'NUM_BITS_FOR_NAVMESH_INDEX' bit range.

typedef u32 TNavMeshIndex;

#define NUM_BITS_FOR_NAVMESH_INDEX		14

#if defined(NAVGEN_TOOL) && !defined(RAGEBUILDER) && defined(NAVMESHMAKER)
#define NUM_BITS_FOR_1ST_VERTEX_INDEX	18
#else
#define NUM_BITS_FOR_1ST_VERTEX_INDEX	16
#endif

#define NAVMESH_MAX_INGAME_1ST_VERTEX_INDEX		(1<<16)-1
#define NAVMESH_MAX_INGAME_POOLSIZE				65535

static const TNavMeshIndex NAVMESH_NAVMESH_INDEX_NONE	=	0x3FFF;							// (16383)
static const TNavMeshIndex NAVMESH_POLY_INDEX_NONE		=	0x7FFF;							// 32767

static const TNavMeshIndex NAVMESH_MAX_INDEX			=	16383;
static const TNavMeshIndex NAVMESH_MAX_MAP_INDEX		=	9999;
static const TNavMeshIndex NAVMESH_INDEX_FIRST_DYNAMIC	=	10000;
static const TNavMeshIndex NAVMESH_INDEX_FINAL_DYNAMIC	=	10099;
static const TNavMeshIndex NAVMESH_INDEX_TESSELLATION	=	10100;

static const TNavMeshIndex NAVMESH_NODE_INDEX_NONE		=	0x7FFF;

typedef u16 TDynamicObjectIndex;
#define DYNAMIC_OBJECT_INDEX_NONE				0xFFFF
#define DYNAMIC_OBJECT_INDEX_UNABLE_TO_ADD		0xFFFE
inline bool GetIsValidPathServerDynamicObjectIndex(const TDynamicObjectIndex i) { return (i < 0xFFFE); }

inline float Dot(const Vector2 & v1, const Vector2 & v2) { return v1.Dot(v2); }
inline float Dot(const Vector3 & v1, const Vector3 & v2) { return v1.Dot(v2); }
inline bool Eq(const float f1, const float f2, const float fEps) { return (Abs(f1 - f2) < fEps) ? true : false; }

#if RSG_BE
#define BITFIELD2(_type,a,as,b,bs)																				_type b:bs; _type a:as;
#define BITFIELD3(_type,a,as,b,bs,c,cs)																			_type c:cs; BITFIELD2(_type,a,as,b,bs)
#define BITFIELD4(_type,a,as,b,bs,c,cs,d,ds)																	_type d:ds; BITFIELD3(_type,a,as,b,bs,c,cs)
#define BITFIELD5(_type,a,as,b,bs,c,cs,d,ds,e,es)																_type e:es; BITFIELD4(_type,a,as,b,bs,c,cs,d,ds)
#define BITFIELD6(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs)															_type f:fs; BITFIELD5(_type,a,as,b,bs,c,cs,d,ds,e,es)
#define BITFIELD7(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs)														_type g:gs; BITFIELD6(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs)
#define BITFIELD8(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs)												_type h:hs; BITFIELD7(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs)
#define BITFIELD9(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is)											_type i:is; BITFIELD8(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs)
#define BITFIELD10(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js)										_type j:js; BITFIELD9(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is)
#define BITFIELD11(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks)								_type k:ks; BITFIELD10(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js)
#define BITFIELD12(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls)							_type l:ls; BITFIELD11(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks)
#define BITFIELD13(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms)						_type m:ms; BITFIELD12(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls)
#define BITFIELD14(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns)					_type n:ns; BITFIELD13(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms)
#define BITFIELD15(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os)			_type o:os; BITFIELD14(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns)	
#define BITFIELD16(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os,p,ps)		_type p:ps; BITFIELD15(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os)
#define BITFIELD17(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os,p,ps,q,qs)	_type q:qs; BITFIELD16(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os,p,ps)
#else
#define BITFIELD2(_type,a,as,b,bs)																				_type a:as; _type b:bs;
#define BITFIELD3(_type,a,as,b,bs,c,cs)																			BITFIELD2(_type,a,as,b,bs) _type c:cs;
#define BITFIELD4(_type,a,as,b,bs,c,cs,d,ds)																	BITFIELD3(_type,a,as,b,bs,c,cs) _type d:ds;
#define BITFIELD5(_type,a,as,b,bs,c,cs,d,ds,e,es)																BITFIELD4(_type,a,as,b,bs,c,cs,d,ds) _type e:es;
#define BITFIELD6(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs)															BITFIELD5(_type,a,as,b,bs,c,cs,d,ds,e,es) _type f:fs;
#define BITFIELD7(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs)														BITFIELD6(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs) _type g:gs;
#define BITFIELD8(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs)												BITFIELD7(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs) _type h:hs;
#define BITFIELD9(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is)											BITFIELD8(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs) _type i:is;
#define BITFIELD10(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js)										BITFIELD9(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is) _type j:js;
#define BITFIELD11(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks)								BITFIELD10(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js) _type k:ks;
#define BITFIELD12(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls)							BITFIELD11(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks) _type l:ls;
#define BITFIELD13(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms)						BITFIELD12(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls) _type m:ms;
#define BITFIELD14(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns)					BITFIELD13(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms) _type n:ns;
#define BITFIELD15(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os)			BITFIELD14(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns) _type o:os;
#define BITFIELD16(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os,p,ps)		BITFIELD15(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os) _type p:ps;
#define BITFIELD17(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os,p,ps,q,qs)	BITFIELD16(_type,a,as,b,bs,c,cs,d,ds,e,es,f,fs,g,gs,h,hs,i,is,j,js,k,ks,l,ls,m,ms,n,ns,o,os,p,ps) _type q:qs;
#endif


struct TNavMeshAndPoly
{
	inline TNavMeshAndPoly() { }
	inline TNavMeshAndPoly(const TNavMeshIndex iNavMesh, const u32 iPoly) : m_iNavMeshIndex(iNavMesh), m_iPolyIndex(iPoly) { }

	TNavMeshIndex m_iNavMeshIndex;
	u32 m_iPolyIndex;

	inline void Reset() { m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE; m_iPolyIndex = NAVMESH_POLY_INDEX_NONE; }
};

enum eNavMeshPolyAdjacencyType
{
	// A normal adjacency - ie. navmesh polygons which are connected edge-to-edge
	ADJACENCY_TYPE_NORMAL		= 0,
	// A low climb connects this edge to the adjacent polygon
	ADJACENCY_TYPE_CLIMB_LOW	= 1,
	// A high climb connects this edge to the adjacent polygon
	ADJACENCY_TYPE_CLIMB_HIGH	= 2,
	// A drop down connects this edge to the adjacent polygon
	ADJACENCY_TYPE_DROPDOWN		= 3
};

class CNavMeshCompressedVertex
{
public:

	u16 data[3];

	CNavMeshCompressedVertex() { }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct & s);
#endif
};

// CLASS : CAdjacentNavmeshLookups
// PURPOSE : Stores a table of navmesh indices, which are used to define which
// for a navmesh which other navmeshes are available as adjacencies from polygons.
// This enables a smaller index value to be stored in TAdjPoly, rather than the
// full 14 bits required for a navmesh index.  The downside is an added level of
// indirection when querying adjacent navmeshes from a TAdjPoly.
// We must ensure that MAX_ADJACENT_NAVMESHES_PER_NAVMESH is enough to cover the
// range of m_NavMeshIndex & m_OriginalNavMeshIndex in TAdjPoly; this value must
// also be enough to address all of the navmeshes we may wish to visit in a single
// search (for reasons of setting up adjacencies in the tessellation navmesh)


class CAdjacentNavmeshLookups
{
public:
	static const s32 ms_iMaxAdjacentNavMeshes = 32;
	static const s32 ms_iNumBitsPerAdjacencyIndex = 5;
public:
#ifdef NAVGEN_TOOL
	CAdjacentNavmeshLookups()
	{
		m_iNumIndices = 0;
		for(s32 i=0; i<ms_iMaxAdjacentNavMeshes; i++)
			m_iNavmeshIndices[i] = 0;
	}
#endif
	inline u32 Get(const s32 i) const
	{
		Assertf(i >=0 && i < ((s32)m_iNumIndices) && i < ms_iMaxAdjacentNavMeshes, "%i/%i", i, m_iNumIndices);
		return m_iNavmeshIndices[i];
	}
	inline s32 Find(const u32 iNavMesh) const
	{
		for(u32 i=0; i<m_iNumIndices; i++)
			if(m_iNavmeshIndices[i] == iNavMesh)
				return i;
		return -1;
	}
	inline s32 Set(const u32 iNavMesh)
	{
		const s32 iIndex = Find(iNavMesh);
		if(iIndex >= 0)
			return iIndex;

		Assert(m_iNumIndices < ms_iMaxAdjacentNavMeshes);
		if(m_iNumIndices < ms_iMaxAdjacentNavMeshes)
		{
			m_iNavmeshIndices[m_iNumIndices] = iNavMesh;
			return m_iNumIndices++;
		}
		FastAssert(m_iNumIndices < ms_iMaxAdjacentNavMeshes);
		return 0;
	}
	inline void Reset() { m_iNumIndices = 0; }

	u32 m_iNumIndices;
	u32 m_iNavmeshIndices[ms_iMaxAdjacentNavMeshes];

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT
};

//************************************************************
//	TAdjPoly
//	This defines a polygon edge - ie. the definition of an
//	adjacent poly, and some flags to describe the adjacency
//************************************************************

struct TAdjPoly
{
	friend class CNavMesh;
	friend class CNavGen;

public:

	// Free space around vertex is stored in 4:3 fixed point allowing for
	// up to 7.75m radius, at a resolution of 0.25m
	static const float ms_fFreeSpaceFixedToFloatMult;
	static const float ms_fFreeSpaceFloatToFixedMult;

	static const float ms_fMaxFreeSpaceBeyondEdge;
	static const float ms_fMaxFreeSpaceAroundVertex;

	inline TAdjPoly()
	{
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
		memset(this, 0, sizeof(TAdjPoly));
		m_fEdgeNormalX = 0.0f;
		m_fEdgeNormalY = 0.0f;
		m_bEdgeMightProvideLowCover = false;
		m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
#endif
#endif

		m_Struct1.m_NavMeshIndex = 0; // NAVMESH_NAVMESH_INDEX_NONE;
		SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		SetHighDropOverEdge(false);
		SetEdgeProvidesCover(false);

		m_Struct2.m_OriginalNavMeshIndex = 0; // NAVMESH_NAVMESH_INDEX_NONE;
		SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
		m_Struct2.m_bExternalEdge = false;

		SetAdjacencyDisabled(false);
	}
	inline void MakeCopy(TAdjPoly & dest) const
	{
		dest.m_Struct1.m_NavMeshIndex = m_Struct1.m_NavMeshIndex;
		dest.m_Struct1.m_PolyIndex = m_Struct1.m_PolyIndex;
		dest.m_Struct1.m_AdjacencyType = m_Struct1.m_AdjacencyType;
		dest.m_Struct1.m_SpaceAroundVertex = m_Struct1.m_SpaceAroundVertex;
		dest.m_Struct1.m_SpaceBeyondEdge = m_Struct1.m_SpaceBeyondEdge;

		dest.m_Struct2.m_OriginalNavMeshIndex = m_Struct2.m_OriginalNavMeshIndex;
		dest.m_Struct2.m_OriginalPolyIndex = m_Struct2.m_OriginalPolyIndex;
		dest.m_Struct2.m_bAdjacencyDisabled = m_Struct2.m_bAdjacencyDisabled;
		dest.m_Struct2.m_bEdgeProvidesCover = m_Struct2.m_bEdgeProvidesCover;
		dest.m_Struct2.m_bHighDropOverEdge = m_Struct2.m_bHighDropOverEdge;
		dest.m_Struct2.m_OriginalNavMeshIndex = m_Struct2.m_OriginalNavMeshIndex;
		dest.m_Struct2.m_bExternalEdge = m_Struct2.m_bExternalEdge;

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
		dest.m_fEdgeNormalX = m_fEdgeNormalX;
		dest.m_fEdgeNormalY = m_fEdgeNormalY;
		dest.m_bEdgeMightProvideLowCover = m_bEdgeMightProvideLowCover;
		dest.m_iAdjacentNavMeshIndex = m_iAdjacentNavMeshIndex;
#endif
#endif
	}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	inline u32 GetNavMeshIndex(const CAdjacentNavmeshLookups * pLookups) const
	{
		return pLookups->Get(m_Struct1.m_NavMeshIndex);
	}
	inline bool GetEdgeProvidesCover() const { return m_Struct2.m_bEdgeProvidesCover; }
	inline u32 GetAdjacencyType() const { return m_Struct1.m_AdjacencyType; }
	inline bool GetHighDropOverEdge() const { return m_Struct2.m_bHighDropOverEdge; }
	inline u32 GetPolyIndex() const { return m_Struct1.m_PolyIndex; }
	inline u32 GetOriginalNavMeshIndex(const CAdjacentNavmeshLookups * pLookups) const
	{
		return pLookups->Get(m_Struct2.m_OriginalNavMeshIndex);
	}
	inline u32 GetOriginalPolyIndex() const { return m_Struct2.m_OriginalPolyIndex; }
	inline bool GetAdjacencyDisabled() const { return m_Struct2.m_bAdjacencyDisabled; }

	inline void SetNavMeshIndex(const u32 i, CAdjacentNavmeshLookups * pLookups)
	{
		m_Struct1.m_NavMeshIndex = pLookups->Set(i);
	}
	inline void SetEdgeProvidesCover(const bool b) { m_Struct2.m_bEdgeProvidesCover = b; }
	inline void SetAdjacencyType(const u32 t) { m_Struct1.m_AdjacencyType = t; }
	inline void SetHighDropOverEdge(const bool b) { m_Struct2.m_bHighDropOverEdge = b; }
	inline void SetPolyIndex(const u32 i) { m_Struct1.m_PolyIndex = i; }
	inline void SetOriginalNavMeshIndex(const u32 i, CAdjacentNavmeshLookups * pLookups)
	{
		m_Struct2.m_OriginalNavMeshIndex = pLookups->Set(i);
	}
	inline void SetOriginalPolyIndex(const u32 i) { m_Struct2.m_OriginalPolyIndex = i; }
	inline void SetAdjacencyDisabled(const bool b) { m_Struct2.m_bAdjacencyDisabled = b; }

	inline u32 GetNavMeshLookupIndex() const { return m_Struct1.m_NavMeshIndex; }
	inline void SetNavMeshLookupIndex(const u32 i)
	{
		Assert(i < CAdjacentNavmeshLookups::ms_iMaxAdjacentNavMeshes);
		m_Struct1.m_NavMeshIndex = i;
	}
	inline u32 GetOriginalNavMeshLookupIndex() const { return m_Struct2.m_OriginalNavMeshIndex; }
	inline void SetOriginalNavMeshLookupIndex(const u32 i)
	{
		Assert(i < CAdjacentNavmeshLookups::ms_iMaxAdjacentNavMeshes);
		m_Struct2.m_OriginalNavMeshIndex = i;
	}

	inline float GetFreeSpaceAroundVertex() const { return ((float)m_Struct1.m_SpaceAroundVertex) * ms_fFreeSpaceFixedToFloatMult; }
	inline float GetFreeSpaceBeyondEdge() const { return ((float)m_Struct1.m_SpaceBeyondEdge) * ms_fFreeSpaceFixedToFloatMult; }

	inline void SetFreeSpaceAroundVertex(const float f)
	{
		Assert(f <= ms_fMaxFreeSpaceBeyondEdge);
		m_Struct1.m_SpaceAroundVertex = (int) (f*ms_fFreeSpaceFloatToFixedMult);
	}
	inline void SetFreeSpaceBeyondEdge(const float f)
	{
		Assert(f <= ms_fMaxFreeSpaceBeyondEdge);
		m_Struct1.m_SpaceBeyondEdge = (int) (f*ms_fFreeSpaceFloatToFixedMult);
	}
	inline bool GetIsExternalEdge() const { return m_Struct2.m_bExternalEdge; } 
	inline void SetIsExternalEdge(const bool b) { m_Struct2.m_bExternalEdge = b; }

	static void InitAdjacentNavmeshOffsets(const s32 iNumMeshesInX);

private:

	union
	{
		u32 m_iAsInteger1;

		struct  
		{
			BITFIELD5(
				u32,
				m_NavMeshIndex,			CAdjacentNavmeshLookups::ms_iNumBitsPerAdjacencyIndex,		// The index lookup into CAdjacentNavmeshes which stores linked navmeshes 
				m_PolyIndex,			15,	// The index of the poly within the navmesh
				m_AdjacencyType,		2,	// Flags indicating whether this adjacency is 'non-standard' (ie. climb-up, drop-down, etc)
				m_SpaceAroundVertex,	5,	// Navigable space around the first vertex of this edge
				m_SpaceBeyondEdge,		5	// Navigable space beyond this edge
			);
		} m_Struct1;
	};

	union
	{
		u32 m_iAsInteger2;

		struct
		{
			BITFIELD7(
				u32,
				m_OriginalNavMeshIndex,		CAdjacentNavmeshLookups::ms_iNumBitsPerAdjacencyIndex,	// The original adjacent navmesh prior to m_NavMeshIndex being overwritten during tessellation
				m_OriginalPolyIndex,		15,	// The original adjacent navmesh poly prior to m_PolyIndex being overwritten during tessellation
				m_bAdjacencyDisabled,		1,	// Whether this adjacency has been disabled due to it being impossible to traverse during route-finding (only relevant for climbs)
				m_bEdgeProvidesCover,		1,	// Whether the length of this edge provides cover from the direction perpendicular to it
				m_bHighDropOverEdge,		1,	// One bit to indicate whether there is a high drop associated with the 'non-standard' adjacency
				m_bExternalEdge,			1,	// Whether this polygon is on the edge of the navmesh and a candidate for connecting to an adjacent navmesh
				m_Reserved,					8
			);
		} m_Struct2;
	};

	enum
	{
		ADJACENT_OFFSET_NONE		= 0,
		ADJACENT_OFFSET_NEGX_NEGY	= 1,
		ADJACENT_OFFSET_NEGY		= 2,
		ADJACENT_OFFSET_POSX_NEGY	= 3,
		ADJACENT_OFFSET_NEGX		= 4,
		ADJACENT_OFFSET_POSX		= 5,
		ADJACENT_OFFSET_NEGX_POSY	= 6,
		ADJACENT_OFFSET_POSY		= 7,
		ADJACENT_OFFSET_POSX_POSY	= 8,

		ADJACENT_OFFSET_NUM
	};
	static s32 ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NUM];

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
public:
	float m_fEdgeNormalX;
	float m_fEdgeNormalY;
	bool m_bEdgeMightProvideLowCover;
	u32 m_iAdjacentNavMeshIndex;
#endif
#endif
};

static const float NAVMESH_SMALL_POLY_AREA	= 1.0f;
static const float NAVMESH_LARGE_POLY_AREA	= 40.0f;

#define UNDERWATER_DEFAULT_RAISE_UP_Z_AMT	2.5f

enum ePolySize
{
	EPolySize_Unclassified	= 0,
	EPolySize_Small			= 1,
	EPolySize_Large			= 2,
	EPolySize_Medium		= 3

	// NB : Note that this enumeration is not in order of size!
	// This should be fixed throughout the navmesh compiler to provide 4 poly sizes -
	// 0,1,2,3 : for small, medium, large & very-large
};

//*************************************************
// Flags which are loaded & saved with the navmesh

#define NAVMESHPOLY_SMALL									0x01	// Whether this poly only needs one point in centre, since it is so small
#define NAVMESHPOLY_LARGE									0x02	// Whether this poly will need more points on surface than usual, due to large size (NB : we could combine these two flags to indicate an extra-large size)
#define NAVMESHPOLY_IS_PAVEMENT								0x04	// If this polygon is to be favoured for ped wandering
#define NAVMESHPOLY_IN_SHELTER								0x08	// Whether this polygon is sheltered from above
#define NAVMESHPOLY_RESERVED1								0x10
#define NAVMESHPOLY_RESERVED2								0x20
#define NAVMESHPOLY_TOO_STEEP_TO_WALK_ON					0x40	// This poly is too steep for normal movement.  Movement onto these polys should for normal path queries.
#define NAVMESHPOLY_IS_WATER								0x80	// maximum bit which will be loaded/saved with navmesh.  NB. are gonna need some more bits..

//*****************************************************
// Flags which are transient (ie. not loaded & saved)

#define NAVMESHPOLY_OPEN									0x100	// Poly is in the A* open list
#define NAVMESHPOLY_CLOSED									0x200	// Poly is in the A* closed list
#define NAVMESHPOLY_IS_WITHIN_INFLUENCE_SPHERE				0x400	// Poly was within a supplied influence-sphere
#define NAVMESHPOLY_IS_IN_OBJECT_CACHE						0x800	// Polygon has a cached list of objects which intersect it
#define NAVMESHPOLY_TESSELLATED_FRAGMENT					0x1000	// This poly is a triangle created from tessellation, and should have extra point-in-poly tests done
#define NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS			0x2000	// This stops peds from being generated on this poly or wandering onto it
#define NAVMESHPOLY_WAS_REACHED_VIA_NONSTANDARD_ADJACENCY	0x4000	// Poly was reached via a climb-up/climb-over/drop-down
#define NAVMESHPOLY_DISABLED								0x8000	// Polygon is completely disabled from all processing (treated as though it doesn't exist)
#define NAVMESHPOLY_DEGENERATE_CONNECTION_POLY				0x10000	// This poly is a zero-area polygon used to implement the polygon tessellation scheme to get around dynamic objects
//#define NAVMESHPOLY_WAS_REACHED_VIA_OBJECT_CLIMBING			0x20000	// Poly was reached by climbing/walking-over a dynamic object
#define NAVMESHPOLY_RESERVED3								0x20000
#define NAVMESHPOLY_REPLACED_BY_TESSELLATION				0x40000	// Poly has been replaced by fragments within m_pTessellationNavMesh
#define NAVMESHPOLY_ALTERNATIVE_STARTING_POLY				0x80000	// Poly is an alternative starting poly in the pathsearch (we can have multiple start polys).  The *real* m_pStartPoly must never have this set.
#define NAVMESHPOLY_WAS_REACHED_VIA_SPECIAL_LINK			0x100000 // Poly was reached via a special link (see m_SpecialLinks), eg. ladder, etc.


static const u32 NAVMESHPOLY_REACHEDBY_MASK = (NAVMESHPOLY_WAS_REACHED_VIA_NONSTANDARD_ADJACENCY|/*NAVMESHPOLY_WAS_REACHED_VIA_OBJECT_CLIMBING|*/NAVMESHPOLY_WAS_REACHED_VIA_SPECIAL_LINK);

static const u32 NAVMESHPOLY_VISITED_RESET_FLAGS_MASK = ~((u32)(NAVMESHPOLY_OPEN | NAVMESHPOLY_CLOSED | NAVMESHPOLY_REACHEDBY_MASK | NAVMESHPOLY_ALTERNATIVE_STARTING_POLY | NAVMESHPOLY_IS_IN_OBJECT_CACHE));


//********************************************************************************************************************************
#define NAVMESHPOLY_NAVGEN_POLY_STITCHED					0x800	// NB : Only used by NavGen code, so this value may be reused in-game (same val as NAVMESHPOLY_CHECKED_FOR_OBJECT_INTERSECTION)
#define NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL				0x800	// NB : Only used by NavGen code, so this value may be reused in-game (same val as NAVMESHPOLY_INTERSECTS_OBJECTS)
//********************************************************************************************************************************

#define NAVMESHPOLY_MAX_NUM_VERTICES						16
#define NAVMESHPOLY_POINTENUM_CENTROID						127
#define NAVMESHPOLY_POINTENUM_SPECIAL_LINK_ENDPOS			126
#define NAVMESHPOLY_POINTENUM_UNUSED						125
static const int MAX_POINT_ENUM_VALUE						= ((NAVMESHPOLY_MAX_NUM_VERTICES*4)+1);
static const int NAVMESHPOLY_POINTENUM_VERTSANDEDGES_FIRST	= MAX_POINT_ENUM_VALUE+1;
#define MAX_PED_DENSITY										7	// range is (0..MAX_PED_DENSITY) inclusive
#define MAX_FREE_SPACE_AROUND_POLY_VALUE					31
#define NUM_BITS_FOR_FREE_SPACE_AROUND_POLY_VALUE			5
#define NAVMESHPOLY_MAX_SPECIAL_LINKS						7

static const float g_fVeryLargeValue						= 10000000.0f;
// The maximum number of points-in-poly that we can ever have, based upon our point-creation algorithm
static const u32 NUM_CLOSE_PTS_IN_POLY					= (NAVMESHPOLY_MAX_NUM_VERTICES*4) + 1;

//***********************************************************************
//	The following 8bit flags are used when pathfinding in immediate-mode.
//	That is using the set of very cut-down functions which hook straight
//	into the navmeshes from the main game thread, carefully bypassing and
//	coexisting with the CPathServerThread's operation.

#define NAVMESHPOLY_IMMEDIATE_MODE_FLAG_OPEN				0x01
#define NAVMESHPOLY_IMMEDIATE_MODE_FLAG_CLOSED				0x02
typedef u8 TNavMeshPolyImmediateModeFlags;

enum ePolyObjectIntersection
{
	POBJ_UNKNOWN,		// No knowledge of whether this polygon intersects any objects
	POBJ_INTERSECTS,	// This polygon is definitely known to intersect some objects
	POBJ_DISJOINT		// This polygon is definitely known not to intersect any objects
};

#define DIRECTIONAL_COVER_NUM_DIRS	8
extern u8 g_iCountCoverBitsTable[256];

//*********************************************************************************
//	TNavMeshPoly
//	This is the poly structure, as referenced by TAdjPoly. The 'm_PathParentPoly'
//	pointer is used in pathfinding to trace the A* route back to the beginning.
//	The 'm_iPointEnum' member stores the point placement for this polygon - one of
//	a fixed number of positions.
//
//  Navmesh polygons have an clockwise vertex winding when seen from above.
//  The navmesh edge N, is bounded by vertex N and (N+1)&iNumVertices.
//
//	NB : Only the bottom 8bits of the m_Flags field is loaded/saves with the mesh,
//	so care must be taken to only use this for bits which are absolutely necessary.
//	NB : The 'm_iNumVertices' and 'm_iPedDensity' members are combined into one
//	u8 for loading/saving.
//
//*********************************************************************************

#if !__64BIT
#pragma pack(push, r1, 1)
#endif
struct TNavMeshPoly
{
public:
	TNavMeshPoly() :
	  m_PathParentPoly(NULL)
	  {
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
		  memset(this, 0, sizeof(TNavMeshPoly));
#endif
#endif
	  }

	  inline u32 GetNumVertices() const { return m_Struct1.m_iNumVertices; }
	  inline int GetFirstVertexIndex() const { return m_Struct2.m_iFirstVertexIndex; }
	  inline TNavMeshIndex GetNavMeshIndex() const { return (TNavMeshIndex) m_Struct2.m_iNavMeshIndex; }
	  inline int GetPedDensity() const { return m_Struct3.m_iPedDensity; }
	  inline u32 GetTimeStamp() const { return m_TimeStamp; }
	  inline TNavMeshPoly * GetPathParentPoly() const { return m_PathParentPoly; }
	  inline u32 GetPointEnum() const { return m_Struct1.m_iPointEnum; }
	  inline bool GetDebugMarked() const { return m_Struct3.m_bDebugMarked; }

	  inline void SetNumVertices(const u32 i) { Assert(i < NAVMESHPOLY_MAX_NUM_VERTICES); m_Struct1.m_iNumVertices = i; }
	  inline void SetFirstVertexIndex(const u32 i) { m_Struct2.m_iFirstVertexIndex = i; }
	  inline void SetNavMeshIndex(const TNavMeshIndex i) { m_Struct2.m_iNavMeshIndex = i; }
	  inline void SetPedDensity(const int pd) { m_Struct3.m_iPedDensity = pd; }
	  inline void SetTimeStamp(const u32 t) { m_TimeStamp = (u16) t; }
	  inline void SetPathParentPoly(TNavMeshPoly * pPoly) { m_PathParentPoly = pPoly; }
	  inline void SetPointEnum(const u32 i) { m_Struct1.m_iPointEnum = i; }
	  inline void SetDebugMarked(const bool b) { m_Struct3.m_bDebugMarked = b; }

	  inline bool GetIsSmall() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_SMALL)!=0); }
	  inline bool GetIsLarge() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_LARGE)!=0); }
	  inline bool GetIsPavement() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_IS_PAVEMENT)!=0); }
	  inline bool GetIsWater() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_IS_WATER)!=0); }
	  inline bool GetIsOpen() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_OPEN)!=0); }
	  inline bool GetIsClosed() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_CLOSED)!=0); }
	  inline bool GetIsSwitchedOffForPeds() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS)!=0); }
	  inline bool GetIsDisabled() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_DISABLED)!=0); }
	  inline bool GetWasReachedByNonStandardAdjacency() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_WAS_REACHED_VIA_NONSTANDARD_ADJACENCY)!=0); }
	  inline bool GetIsWithinInfluenceSphere() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_IS_WITHIN_INFLUENCE_SPHERE)!=0); }
	  inline bool GetReplacedByTessellation() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_REPLACED_BY_TESSELLATION)!=0); }
	  inline bool GetIsTessellatedFragment() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_TESSELLATED_FRAGMENT)!=0); }
	  inline bool GetIsDegenerateConnectionPoly() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_DEGENERATE_CONNECTION_POLY)!=0); }
	  inline bool GetIsZeroAreaStichPolyDLC() const { return m_Struct3.m_bZeroAreaStichPolyDLC; }
	  inline bool GetLiesAlongEdgeOfMesh() const { return m_Struct3.m_bLiesAlongEdgeOfMesh; }
	  inline bool GetIsInObjectCache() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_IS_IN_OBJECT_CACHE)!=0); }
	  inline bool GetIsSheltered() const { return ((m_Struct1.m_Flags & NAVMESHPOLY_IN_SHELTER)!=0); }
	  inline u32 GetAudioProperties() const { return m_Struct3.m_iAudioProperties; }
	  inline bool GetIsNearCarNode() const { return m_Struct3.m_bNearCarNode; }
	  inline bool GetIsInterior() const { return m_Struct3.m_bInterior; }
	  inline bool GetIsIsolated() const { return m_Struct3.m_bIsolated; }
	  inline bool GetIsNetworkSpawnCandidate() const { return m_Struct3.m_bNetworkSpawnCandidate; }
	  inline bool GetIsRoad() const { return m_Struct3.m_bIsRoad; }
	  inline bool GetIsTrainTrack() const { return m_Struct3.m_bIsTrainTrack; }
	  inline bool GetIsShallow() const { return m_Struct3.m_bShallow; }
	  inline s32 GetNumSpecialLinks() const { return m_Struct4.m_iNumSpecialLinks; }
	  inline s32 GetSpecialLinksStartIndex() const { return m_Struct4.m_iSpecialLinksStartIndex; }
	  inline u32 GetCoverDirections() const { return m_iCoverDirectionsBitfield; }

	  inline void SetIsSmall(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_SMALL; else m_Struct1.m_Flags &= ~NAVMESHPOLY_SMALL; }
	  inline void SetIsLarge(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_LARGE; else m_Struct1.m_Flags &= ~NAVMESHPOLY_LARGE; }
	  inline void SetIsPavement(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_IS_PAVEMENT; else m_Struct1.m_Flags &= ~NAVMESHPOLY_IS_PAVEMENT; }
	  inline void SetIsWater(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_IS_WATER; else m_Struct1.m_Flags &= ~NAVMESHPOLY_IS_WATER; }
	  inline void SetIsOpen(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_OPEN; else m_Struct1.m_Flags &= ~NAVMESHPOLY_OPEN; }
	  inline void SetIsClosed(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_CLOSED; else m_Struct1.m_Flags &= ~NAVMESHPOLY_CLOSED; }
	  inline void SetIsSwitchedOffForPeds(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS; else m_Struct1.m_Flags &= ~NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS; }
	  inline void SetIsDisabled(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_DISABLED; else m_Struct1.m_Flags &= ~NAVMESHPOLY_DISABLED; }
	  inline void SetWasReachedByNonStandardAdjacency(const bool b)  { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_WAS_REACHED_VIA_NONSTANDARD_ADJACENCY; else m_Struct1.m_Flags &= ~NAVMESHPOLY_WAS_REACHED_VIA_NONSTANDARD_ADJACENCY; }
	  inline void SetIsWithinInfluenceSphere(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_IS_WITHIN_INFLUENCE_SPHERE; else m_Struct1.m_Flags &= ~NAVMESHPOLY_IS_WITHIN_INFLUENCE_SPHERE; }
	  inline void SetReplacedByTessellation(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_REPLACED_BY_TESSELLATION; else m_Struct1.m_Flags &= ~NAVMESHPOLY_REPLACED_BY_TESSELLATION; }
	  inline void SetIsTessellatedFragment(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_TESSELLATED_FRAGMENT; else m_Struct1.m_Flags &= ~NAVMESHPOLY_TESSELLATED_FRAGMENT; }
	  inline void SetIsDegenerateConnectionPoly(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_DEGENERATE_CONNECTION_POLY; else m_Struct1.m_Flags &= ~NAVMESHPOLY_DEGENERATE_CONNECTION_POLY; }
	  inline void SetIsZeroAreaStichPolyDLC(const bool b) { m_Struct3.m_bZeroAreaStichPolyDLC = b; }
	  inline void SetLiesAlongEdgeOfMesh(const bool b) { m_Struct3.m_bLiesAlongEdgeOfMesh = b; }
	  inline void SetIsInObjectCache(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_IS_IN_OBJECT_CACHE; else m_Struct1.m_Flags &= ~NAVMESHPOLY_IS_IN_OBJECT_CACHE; }  
	  inline void SetIsSheltered(const bool b) { if(b) m_Struct1.m_Flags |= NAVMESHPOLY_IN_SHELTER; else m_Struct1.m_Flags &= ~NAVMESHPOLY_IN_SHELTER; }
	  inline void SetAudioProperties(const u32 a) { Assert(a<16); m_Struct3.m_iAudioProperties = a; }
	  inline void SetIsNearCarNode(const bool b) { m_Struct3.m_bNearCarNode = b; }
	  inline void SetIsInterior(const bool b) { m_Struct3.m_bInterior = b; }
	  inline void SetIsIsolated(const bool b) { m_Struct3.m_bIsolated = b; }
	  inline void SetIsNetworkSpawnCandidate(const bool b) { m_Struct3.m_bNetworkSpawnCandidate = b; }
	  inline void SetIsRoad(const bool b) { m_Struct3.m_bIsRoad = b; }
	  inline void SetIsTrainTrack(const bool b) { m_Struct3.m_bIsTrainTrack = b; }
	  inline void SetIsShallow(const bool b) { m_Struct3.m_bShallow = b; }
	  inline void SetNumSpecialLinks(const s32 i) { m_Struct4.m_iNumSpecialLinks = i; }
	  inline void SetSpecialLinksStartIndex(const s32 i) { m_Struct4.m_iSpecialLinksStartIndex = i; }

	  inline bool TestFlags(const u32 flags) const { return ((m_Struct1.m_Flags & flags)!=0); }

	  inline u32 GetFlags() const { return m_Struct1.m_Flags; }
	  inline void SetFlags(const u32 flags) { m_Struct1.m_Flags = flags; }

	  inline void OrFlags(const s32 flags) { m_Struct1.m_Flags |= flags; }
	  inline void AndFlags(const s32 flags) { m_Struct1.m_Flags &= flags; }

	  inline u32 GetStruct1AsInteger() const { return m_iAsInteger1; }
	  inline u32 GetStruct2AsInteger() const { return m_iAsInteger2; }
	  inline u32 GetStruct3AsInteger() const { return m_iAsInteger3; }
	  inline void SetStruct1AsInteger(const u32 i) { m_iAsInteger1 = i; }
	  inline void SetStruct2AsInteger(const u32 i) { m_iAsInteger2 = i; }
	  inline void SetStruct3AsInteger(const u32 i) { m_iAsInteger3 = i; }

#if __DECLARESTRUCT
	  void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	  inline void CopyDataIntoTessellatedPoly(TNavMeshPoly & destPoly) const
	  {
		  destPoly.SetFlags(GetFlags());
		  destPoly.m_iAsInteger3 = m_iAsInteger3;

		  destPoly.m_iCoverDirectionsBitfield = m_iCoverDirectionsBitfield;

		  // Clean out some flags which we don't wish to inherit from parent
		  const u32 iMaskFlags = ( NAVMESHPOLY_SMALL | NAVMESHPOLY_LARGE | NAVMESHPOLY_OPEN | NAVMESHPOLY_CLOSED | NAVMESHPOLY_REPLACED_BY_TESSELLATION );
		  destPoly.AndFlags(~iMaskFlags);
		  destPoly.OrFlags(NAVMESHPOLY_TESSELLATED_FRAGMENT);
	  }

private:

	union
	{
		u32 m_iAsInteger1;
		struct
		{
			BITFIELD3(
				u32,
				m_Flags,			21,		// Keep any flags needed to be loaded/saved within the first 8 bits!
				m_iNumVertices,		4,		// NAVMESHPOLY_MAX_NUM_VERTICES = 16
				m_iPointEnum,		7		// (NAVMESHPOLY_MAX_NUM_VERTICES*4)+1
			);
		} m_Struct1;
	};

	union
	{
		u32 m_iAsInteger2;

		struct  
		{
			BITFIELD3(
				u32,
				m_iFirstVertexIndex,			NUM_BITS_FOR_1ST_VERTEX_INDEX,	// 17 bits if NAVGEN_TOOL define, otherwise 15 bits
				m_iNavMeshIndex,				NUM_BITS_FOR_NAVMESH_INDEX,
				m_iReserved,					2
			);
		} m_Struct2;
	};

public:

	// This timestamp is used to control recursion when refining the triangle path.
	// It may not need to be a full 32bit value - 16bits will suffice, provided that
	// all loaded triangles have their timestamps reset when the counter reaches max.
	u16 m_TimeStamp;	
	// This second timestamp is used so that we know if a poly is being looked at for
	// the 1st time during a particular path-search.  If so, all its temporary flags
	// and data should be cleared at that point.
	u16 m_AStarTimeStamp;	

	TNavMeshPoly * m_PathParentPoly;

	TShortMinMax m_MinMax;

	union
	{
		u32 m_iAsInteger3;
		struct
		{
			BITFIELD13(
				u32,
				m_iAudioProperties,				4,	// (0..15) values to store audio properties.
				m_bDebugMarked,					1,
				m_bNearCarNode,					1,	// Whether this poly has a car node upon it
				m_bInterior,					1,	// Whether this poly is in an interior
				m_bIsolated,					1,	// Set when this poly is a small region of navmesh isolated from the main area
				m_bZeroAreaStichPolyDLC,		1,	// This polygon is a zero-area polygon used to connect DLC to the main map seamlessly (NB: this is *not* the same thing as a 'degenerate connection poly' NAVMESHPOLY_DEGENERATE_CONNECTION_POLY)
				m_bNetworkSpawnCandidate,		1,	// This poly may potentially be used for network spawning
				m_bIsRoad,						1,	// This polygon intersects a road
				m_bLiesAlongEdgeOfMesh,			1,	// This polygon lies along the edge of the navmesh (an optimisation when stitching together adjacent navmeshes due to map-swap/DLC)
				m_bIsTrainTrack,				1,	// This polygon intersects a train track
				m_bShallow,						1,	// This polygon is shallow water (must be in conjunction with GetIsWater())
				m_iPedDensity,					3,	// How many peds spawn on this navmesh poly, a value from (0..7)

				// Reserved for expansion
				// I would like store 16 bits here for custom data to be stored in the navmesh:
				// 8 bits for a material ID, where the materials to be preserved are determined by fwNavMeshMaterialInterface
				// 8 bits for additional flags, again defined in a game agnostic way
				// An interface in navmesh compiler will determine which bits/materials to preserve as hard boundaries in the navmesh
				m_iReserved2,					15
			);
		} m_Struct3;
	};

	// The poly's centre in compressed format - same as compressed navmesh vertices.
	// These are not used for tessellated polygons, only for original polys.  The z
	// height can be approx obtained by the m_MinMax.  It is best to use the function
	// CNavMesh::GetPolyCentroidQuick() to obtain the poly's centroid using these.
	u8 m_iCentroidX;
	u8 m_iCentroidY;

	//u8 m_iBakedInDataBitfield;

	// A bitfield which describes which directions cover exists around this poly, as anaylsed during navmesh creation.
	// This is only an approximation - see ms_vCoverDirections for the directions.  If a bit in m_iCoverDirectionsBitfield
	// is set (going from LSB to MSB) then some possible cover exists in this approximate direction.
	u8 m_iCoverDirectionsBitfield;

	// Whether this poly is being visited by an immediate-mode function.
	// NB : Watch out - there could be thread access problems here as the 8-bit value will need masking before storing.
	u8 m_iImmediateModeFlags;

	union
	{
		u32 m_iAsInteger4;
		struct
		{
			BITFIELD5(
				u32,
				m_iParentExitEdge,				4,
				m_iPolyArrayIndex,				8,	// Which poly array this polygon resides in.  Imposes a max num polygons of 87,296
				m_iNumSpecialLinks,				3,	// Number of special links which start or end on this polygon (max is 7)
				m_iSpecialLinksStartIndex,		9,	// Index into start position of special link indices for this polygon
				m_Unused,						8
			);
		} m_Struct4;
	};

	static const Vector3 ms_vCoverDirections[DIRECTIONAL_COVER_NUM_DIRS];

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
public:

	// This following union/bitset is saved in the .inv file format so that this data can be
	// passed right down to the final stages of navmesh construction.
	union
	{
		u32 m_iNonResourcedAsInteger;
		struct
		{
			BITFIELD2(
				u32,
				m_bIntersectsEntities,		1,			// Used by NavMeshGenerator to prevent polygon merging across this boundary
				m_bNoNetworkSpawn,			1			// Used by NavMeshGenerator to prevent polygon merging across this boundary
			);
		} m_NonResourced;
	};

	// These following members are used during navmesh construction, but are not loaded/saved at any point

	s16 m_iFreeSpaceTopZ;						// Used by NavMeshGenerator to record how much free space exists above this poly
	u16 m_iRemappedPolyIndex;					// Used by NavMeshGenerator when flood-filling navmeshes to remove inaccessible areas
	CHierarchicalNode * m_pHierarchicalNode;	// Used by NavMeshGenerator to store which nodes are on which polys
	float m_fPathCost;							// Used by NavGenTest to help debugging pathfinding in-game pathfinder
	float m_fDistanceTravelled;					// Used by NavGenTest to help debugging pathfinding in-game pathfinder

	u32 m_bDoNotOptimise		:1;

#endif
#endif
};	// 40 bytes in-game
#if !__64BIT
#pragma pack(pop, r1)
#endif

inline u8 CoverDirFromVector3(const Vector3 & vCoverDir)
{
	const float fMultiplier = (255.0f / (2.0f * PI));
	float fAngle = rage::Atan2f(-vCoverDir.x, vCoverDir.y);
	u8 iVal = (u8)(fAngle * fMultiplier);
	return iVal;
}

inline void Vector3FromCoverDir(Vector3 & vCoverDir, u8 iCoverDir)
{
	const float fMultiplier = ((2.0f * PI) / 255.0f);
	float fAngle = ((float)iCoverDir) * fMultiplier;

	vCoverDir.z = 0.0f;
	vCoverDir.x = -rage::Sinf(fAngle);
	vCoverDir.y = rage::Cosf(fAngle);
}

inline float CoverAngleFromCoverDir(u8 iCoverDir)
{
	const float fMultiplier = ((2.0f * PI) / 255.0f);
	float fAngle = ((float)iCoverDir) * fMultiplier;
	return fAngle;
}

class CNavMeshCoverPoint;

struct TNavMeshQuadTreeLeafData
{
	TNavMeshQuadTreeLeafData();
	TNavMeshQuadTreeLeafData(datResource & rsc);	// resource constructor
	DECLARE_PLACE(TNavMeshQuadTreeLeafData);

	// The index of the 1st coverpoint (below) into the entire list of coverpoints in this navmesh.
	// This value is needed so we can make a UID for each coverpoint in the world by combining it
	// with the navmesh index.
	u16 m_iIndexOfFirstCoverPointInList;
	u8 m_iUnused;		// TODO: Consider removing this if it saves memory and you don't mind having to rebuild resources, or recycle it. /FF
	u8 m_iReserved;

	u16 * m_Polys;
	CNavMeshCoverPoint * m_CoverPoints;

	u16 m_iNumPolys;
	u16 m_iNumCoverPoints;

#if __DECLARESTRUCT
	void DeclareStruct();
#endif

};	// 16 bytes

//#if !__PS3 || GCC_VERSION < 40101
#pragma pack(push, r1, 1)
//#endif
class CNavMeshQuadTree
{
public:
	CNavMeshQuadTree();
	~CNavMeshQuadTree();

	CNavMeshQuadTree(datResource & rsc);	// resource constructor
	DECLARE_PLACE(CNavMeshQuadTree);

#if __DECLARESTRUCT
	void DeclareStruct();
#endif

	Vector3 m_Mins;
	Vector3 m_Maxs;
	TShortMinMax m_MinMax;
	TNavMeshQuadTreeLeafData * m_pLeafData;
	CNavMeshQuadTree * m_pChildren[4];
};	// 64 bytes
//#if !__PS3 || GCC_VERSION < 40101
#pragma pack(pop, r1)
//#endif

// NB : These must match the enumeration in Cover.h
#define NAVMESH_COVERPOINT_LOW_WALL				0
#define NAVMESH_COVERPOINT_LOW_WALL_TO_LEFT		1
#define NAVMESH_COVERPOINT_LOW_WALL_TO_RIGHT	2
#define NAVMESH_COVERPOINT_WALL_TO_LEFT			3
#define NAVMESH_COVERPOINT_WALL_TO_RIGHT		4
#define NAVMESH_COVERPOINT_WALL_TO_NEITHER		5

//****************************************************************
//	CNavMeshCoverPoint
//	This defines a cover-point as analysed for a navmesh.
//	The main game has an interface which adds cover-points to the
//	game, by converting them from this representation to the
//	in-game CCoverPoint class
//****************************************************************

struct TCoverPointFlags
{
public:

	inline u32 GetCoverDir() const { return m_Struct.m_iCoverDir; }
	inline u32 GetCoverType() const { return m_Struct.m_iCoverType; }
	inline bool GetIsDisabled() const { return m_Struct.m_bDisabled; }

	inline void SetCoverDir(const u32 i) { m_Struct.m_iCoverDir = i; }
	inline void SetCoverType(const u32 i) { m_Struct.m_iCoverType = i; }
	inline void SetIsDisabled(const bool b) { m_Struct.m_bDisabled = b; }

	inline u16 GetAsInteger() const { return m_iAsInteger; }
	inline void SetAsInteger(const u16 i) { m_iAsInteger = i; }

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	TCoverPointFlags()
	{
		memset(this, 0, sizeof(TCoverPointFlags));
	}
#endif
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(TCoverPointFlags);
		STRUCT_FIELD(m_iAsInteger);
		STRUCT_END();
	}
#endif

private:

	union
	{
		u16 m_iAsInteger;

		struct 
		{
			BITFIELD4(
				u16,
				// 0..7 direction values.  This is expanded out to 0..255 for use by CCover.
				m_iCoverDir,		8,
				// The type of the cover.  This has the following meaning (see Task\Cover.h)
				// 0) LowWall cover	- CCoverPoint::COVUSE_WALLTOBOTH & CCoverPoint::COVHEIGHT_LOW
				// 1) Corner cover	- CCoverPoint::COVUSE_WALLTOLEFT & CCoverPoint::COVHEIGHT_LOW 
				// 2) Corner cover	- CCoverPoint::COVUSE_WALLTORIGHT & CCoverPoint::COVHEIGHT_LOW
				// 3) Corner cover	- CCoverPoint::COVUSE_WALLTOLEFT & CCoverPoint::COVHEIGHT_TOOHIGH
				// 4) Corner cover	- CCoverPoint::COVUSE_WALLTORIGHT & CCoverPoint::COVHEIGHT_TOOHIGH
				// 5) Corner cover	- CCoverPoint::COVUSE_WALLTONEITHER & CCoverPoint::COVHEIGHT_TOOHIGH
				// 6) UNUSED - available for future use
				// 7) UNUSED - available for future use
				m_iCoverType,		3,
				m_bDisabled,		1,
				m_iReserved,		4
			);

		} m_Struct;
	};
};


#define MAX_NUM_COVERPOINTS_PER_NAVMESH		8191

enum eAdjNavMeshCode
{
	AdjNav_None		= 0,
	AdjNav_This		= 1,
	AdjNav_PlusY	= 2,
	AdjNav_NegY		= 3,
	AdjNav_PlusX	= 4,
	AdjNav_NegX		= 5
};

class CNavMeshCoverPoint
{
public:
	CNavMeshCoverPoint();

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	~CNavMeshCoverPoint();
#endif
#endif

	// XYZ are stored as compressed floats
	u16 m_iX;
	u16 m_iY;
	u16 m_iZ;

	TCoverPointFlags m_CoverPointFlags;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	// Within NavMeshMaker.exe & NavGenTest.exe this class has some extra members
	// which come in handy when constructing navmeshes & linking up coverpoints..
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	CNavMesh * m_pOwningNavMesh;
	u32 m_iCoverPointIndexWithinNavMesh;

	float m_fPosOnNavMeshX;
	float m_fPosOnNavMeshY;
	float m_fPosOnNavMeshZ;
	float m_fRatioClearToBlocked;
	bool m_bRemoveMe;
#endif
#endif
};	// 12 bytes in-game

enum ESWITCH_NAVMESH_POLYS
{
	SWITCH_NAVMESH_ENABLE_AMBIENT_PEDS	= 1,
	SWITCH_NAVMESH_DISABLE_AMBIENT_PEDS	= 2,
	SWITCH_NAVMESH_ENABLE_POLYGONS		= 4,
	SWITCH_NAVMESH_DISABLE_POLYGONS		= 8
};



#define NAVMESH_LINKTYPE_CLIMB_LADDER		0x01
#define NAVMESH_LINKTYPE_DESCEND_LADDER		0x02
#define NAVMESH_LINKTYPE_CLIMB_OBJECT		0x03

class CSpecialLinkInfo
{
	friend class CNavGen;
	friend class CNavMeshMaker;

public:

	CSpecialLinkInfo();

	inline s32 GetLinkType() const { return m_iLinkType; }
	inline u32 GetLinkFlags() const { return m_iLinkFlags; }
	inline u16 GetAStarTimeStamp() const { return m_iAStarTimeStamp; }
	inline void SetAStarTimeStamp(const u16 t) { m_iAStarTimeStamp = t; }
 
	inline u16 GetLinkFromPosX() const { return m_iLinkFromPosX; }
	inline u16 GetLinkFromPosY() const { return m_iLinkFromPosY; }
	inline u16 GetLinkFromPosZ() const { return m_iLinkFromPosZ; }
	inline u16 GetLinkToPosX() const { return m_iLinkToPosX; }
	inline u16 GetLinkToPosY() const { return m_iLinkToPosY; }
	inline u16 GetLinkToPosZ() const { return m_iLinkToPosZ; }

	inline u32 GetLinkFromPoly() const { return m_iLinkFromPoly; }
	inline u32 GetLinkToPoly() const { return m_iLinkToPoly; }

	inline u32 GetLinkFromNavMesh() const { return m_Struct1.m_bLinkFromTessellationMesh ? NAVMESH_INDEX_TESSELLATION : m_Struct1.m_iLinkFromNavMesh; }
	inline u32 GetLinkToNavMesh() const { return m_Struct1.m_bLinkToTessellationMesh ? NAVMESH_INDEX_TESSELLATION : m_Struct1.m_iLinkToNavMesh; }

	inline u32 GetOriginalLinkFromNavMesh() const { return m_Struct1.m_iLinkFromNavMesh; }
	inline u32 GetOriginalLinkToNavMesh() const { return m_Struct1.m_iLinkToNavMesh; }
	inline u32 GetOriginalLinkFromPoly() const { return m_iOriginalLinkFromPoly; }
	inline u32 GetOriginalLinkToPoly() const { return m_iOriginalLinkToPoly; }

	inline bool GetIsDisabled() const { return m_Struct1.m_bDisabled; }
	inline void SetIsDisabled(const bool b) { m_Struct1.m_bDisabled = b; }

	void Reset();

	inline void SetLinksFromTessellationMesh(const u16 iFromPoly)
	{
		m_Struct1.m_bLinkFromTessellationMesh = true;
		m_iLinkFromPoly = iFromPoly;
	}
	inline void SetLinksToTessellationMesh(const u16 iToPoly)
	{
		m_Struct1.m_bLinkToTessellationMesh = true;
		m_iLinkToPoly = iToPoly;
	}

	void Read(fiStream * pStream);
	void Write(fiStream * pStream) const;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

protected:

	// The type of the link
	u8 m_iLinkType;
	// Flags associated with the link.
	// NB: For ladders this now contains the get-on heading for the link, compressed into 0..255
	u8 m_iLinkFlags;
	// Timestamp
	u16	m_iAStarTimeStamp;

	// Link-from position in compressed format
	u16 m_iLinkFromPosX, m_iLinkFromPosY, m_iLinkFromPosZ;
	// Link-to position in compressed format
	u16 m_iLinkToPosX, m_iLinkToPosY, m_iLinkToPosZ;

	u16 m_iOriginalLinkFromPoly;
	u16 m_iLinkFromPoly;
	u16 m_iOriginalLinkToPoly;
	u16 m_iLinkToPoly;

	union
	{
		u32 m_iAsInteger1;
		struct
		{
			BITFIELD6(
				u32,
				m_iLinkFromNavMesh,				NUM_BITS_FOR_NAVMESH_INDEX,
				m_iLinkToNavMesh,				NUM_BITS_FOR_NAVMESH_INDEX,
				m_bLinkFromTessellationMesh,	1,
				m_bLinkToTessellationMesh,		1,
				m_bDisabled,					1,
				m_Unused,						1 );

		} m_Struct1;
	};

};	// 28 bytes


class CNavMeshNonResourcedData
{
public:
	CNavMeshNonResourcedData()
	{
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
		memset(this, 0, sizeof(CNavMeshNonResourcedData));
#endif
#endif
		m_vMinOfNavMesh.Zero();
		m_DynamicNavMeshLastUpdateMatrix.Identity();
		m_iNumPedDensityPolys = -1;
		m_iAdjoinedDLCGroup[0] = m_iAdjoinedDLCGroup[1] = m_iAdjoinedDLCGroup[2] = m_iAdjoinedDLCGroup[3] = -1;
	}
	~CNavMeshNonResourcedData() { }

	// We store this here in preference to using m_pQuadTree->m_vMins, because for dynamic navmeshes
	// there is no quadtree (we delete it upon loading).
	Vector3 m_vMinOfNavMesh;

	// This is set when a dynamic navmesh's poly MinMax's are recalculated.  If it differs
	// from the current m_Matrix, then another update will be required.
	Matrix34 m_DynamicNavMeshLastUpdateMatrix;

	s32 m_iNumPedDensityPolys;

	// The index of the DLC group which each navmesh edge has been adjoined to, of -1 if none.
	// Numbering follows the edge ordering in the eNavMeshEdge enumeration.
	s32 m_iAdjoinedDLCGroup[4];
};

enum EGetClosestPosFlags
{
	// Only return a position which is on a pavement navmesh polygon
	GetClosestPos_OnlyPavement			= 0x1,
	// Rule out generating positions on polygons whose AABB intersects the AABB of a dynamic object in the pathfinder
	GetClosestPos_ClearOfObjects		= 0x2,
	// Only return a position which is on a navmesh polygon flagged as "non isolated"
	GetClosestPos_OnlyNonIsolated		= 0x4,
	// Consider multiple points in each polygon, rather than just each polygon centroid
	GetClosestPos_ExtraThorough			= 0x8,
	// Internal flag to specify whether this call was made from within the pathserver itself
	GetClosestPos_CalledFromPathfinder	= 0x10,
	// Don't return positions which are on navmesh polygons which originated in an interior
	GetClosestPos_NotInteriors			= 0x20,
	// Don't return positions which are on navmesh polygons which are flagged as water
	GetClosestPos_NotWater				= 0x40,
	// Prefer positions which are at a similar height to the input position's Z coordinate
	GetClosestPos_PreferSameHeight		= 0x80,
	// Specify whether to use a flood-fill from the starting position, as opposed to scanning all polygons within the search volume
	GetClosestPos_UseFloodFill			= 0x100,
	// Only return a position which is on a polygon marked as a network spawn candidate
	GetClosestPos_OnlyNetworkSpawn		= 0x200,
	// Only return positions which are on navmesh polygons which originated in an interior
	GetClosestPos_OnlyInteriors			= 0x400,
	// Don't return positions which are on navmesh polygons which are flagged as sheltered
	GetClosestPos_NotSheltered			= 0x800,
	// Only return positions which are on navmesh polygons which are flagged as sheltered
	GetClosestPos_OnlySheltered			= 0x1000
};


typedef bool (*AcceptPolyCB)(const CNavMesh * pNavMesh, const TNavMeshPoly * pPoly);
typedef float (*AcceptPointCB)(const CNavMesh * pNavMesh, const TNavMeshPoly * pPoly, const Vector3 & vPos);


struct TGetPolyEdgeStruct
{
	Vector3 g_vSearchDirection;
	Vector3 g_vGetPolyEdgeMins;
	Vector3 g_vGetPolyEdgeMaxs;
	Vector3 g_vGetPolyEdgeInputPos;
	Vector3 g_vGetClosestPointInPoly;
	Vector3 g_vGetPolyEdge[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 g_vGetClosestPointInPolyPts[NUM_CLOSE_PTS_IN_POLY];
	float g_fGetPolyEdgeClosestDistSqr;
	float g_fGetPolyEdgeMaxDistSqr;
	u32 g_iGetPolyEdgeBestPoly;
	u32 g_iGetPolyEdgeBestEdge;
	u32 g_iGetClosestPolyNumAvoidSpheres;
	const spdSphere * g_iGetClosestPolyAvoidSpheresArray;

	inline TGetPolyEdgeStruct()
	{
		g_vSearchDirection = Vector3(0.f,0.f,0.f);
		g_vGetPolyEdgeMins = Vector3(0.0f,0.0f,0.0f);
		g_vGetPolyEdgeMaxs = Vector3(0.0f,0.0f,0.0f);
		g_vGetPolyEdgeInputPos = Vector3(0.0f,0.0f,0.0f);
		g_vGetClosestPointInPoly = Vector3(0.0f,0.0f,0.0f);
		g_fGetPolyEdgeClosestDistSqr = FLT_MAX;
		g_fGetPolyEdgeMaxDistSqr = 0.0f;
		g_iGetPolyEdgeBestPoly = NAVMESH_POLY_INDEX_NONE;
		g_iGetPolyEdgeBestEdge = 0;
		g_iGetClosestPolyNumAvoidSpheres = 0;
		g_iGetClosestPolyAvoidSpheresArray = NULL;
	}
};

struct TForAllPolysStruct
{
	Vector3 m_vPolyVertices[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 m_vPointsInPolygon[NUM_CLOSE_PTS_IN_POLY];

	Vector3 m_vSearchOrigin;
	float m_fSearchRadius;
	TShortMinMax m_SearchMinMax;
	AcceptPolyCB m_AcceptPolyFn;
	AcceptPointCB m_AcceptPointFn;
	u32 m_iFlags;

	enum SearchFlags
	{
		FLAG_MULTIPLE_POINTS_PER_POLYGON	=	0x1
	};
};

//*********************************************************************************
//	TPolyVolumeInfo
//
//	Describes the amount of free space available for movement above the surface
//	of a polygon.  It also defines which poly may be reachable above and below
//	the poly.
//	This is intended for use in underwater navigation (and maybe in future aerial
//	navigation) - where peds can pathfind vertically through water & air.
//	For the water case, the link-below is used for water-surface polys to connect
//	down to ocean-bed polys and the link-above is for ocean-bed polys to connect
//	up to the water surface.
//
//*********************************************************************************

struct TPolyVolumeInfo
{
	friend class CNavGen;
	friend class CNavMesh;

public:

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	TPolyVolumeInfo()
	{
		memset(this, 0, sizeof(TPolyVolumeInfo));
	}
#endif
#endif
	inline s16 GetFreeSpaceTopZ() const { return m_iFreeSpaceTopZ; }
	inline u16 GetPolyLinkedToAbove() const { return m_iPolyLinkedToAbove; }
	inline u16 GetPolyLinkedToBelow() const { return m_iPolyLinkedToBelow; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	static const s32 ms_iFreeSpaceNone = -32000;

private:
	// The top of the free space above this polygon.  Resolution is at 1 metre.
	s16 m_iFreeSpaceTopZ;
	// Polys linked above/below will always be in the same navmesh. NAVMESH_POLY_INDEX_NONE indicates no link.
	u16 m_iPolyLinkedToAbove;
	u16 m_iPolyLinkedToBelow;
};

#if __DEV
struct TNavmeshMemStats
{
	TNavmeshMemStats()
	{
		memset(this, 0, sizeof(TNavmeshMemStats));
	}
	s32 iMemUsedForVertexData;
	s32 iMemUsedForCoverPoints;
	s32 iMemUsedForQuadtrees;			// includes the coverpoints memory as well
	s32 iMemUsedForPolys;
	s32 iMemUsedForAdjPolys;
	s32 iMemUsedForVertexIndices;
	s32 iMemUsedForSpecialLinks;
	s32 iMemUsedForVolumePolys;
};
#endif


static const u32 NAVMESH_POLY_ARRAY_BLOCKSIZE = (u32)(16384 / sizeof(TNavMeshPoly));
static const u32 NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE = (u32)(16384 / sizeof(TAdjPoly));
static const u32 NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE = (u32)(16384 / 6);		// 2730 on all platforms
static const u32 NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE = (u32)(16384 / 2);			// 8192 on all platforms

#ifndef NAVGEN_TOOL
CompileTimeAssertSize( TNavMeshPoly, 40, 48 );
CompileTimeAssertSize( TAdjPoly, 8, 8 );
#if __64BIT
CompileTimeAssert( (sizeof(TNavMeshPoly)*NAVMESH_POLY_ARRAY_BLOCKSIZE) == 16368 );	// 341
CompileTimeAssert( (sizeof(TAdjPoly)*NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE) == 16384 );	// 2048
CompileTimeAssert( (sizeof(u16)*3*NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE) == 16380 );	// 2730
#else // __64BIT
CompileTimeAssert( (sizeof(TNavMeshPoly)*NAVMESH_POLY_ARRAY_BLOCKSIZE) == 16360 );	// 409
CompileTimeAssert( (sizeof(TAdjPoly)*NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE) == 16384 );	// 2048
CompileTimeAssert( (sizeof(u16)*3*NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE) == 16380 );	// 2730
#endif
#endif // NAVGEN_TOOL




#define EDGEFLAG_NEG_X		0x01
#define EDGEFLAG_POS_X		0x02
#define EDGEFLAG_NEG_Y		0x04
#define EDGEFLAG_POS_Y		0x08

enum eNavMeshEdge
{
	eNegX,
	ePosX,
	eNegY,
	ePosY
};

inline eNavMeshEdge GetOppositeEdge(eNavMeshEdge edge)
{
	switch(edge)
	{
	case eNegX: return ePosX;
	case ePosX: return eNegX;
	case eNegY: return ePosY;
	case ePosY: return eNegY;
	}
	Assert(0);
	return eNegX;
}


// Whether this navmesh's vertex data is in compressed format
#define NAVMESH_COMPRESSED_VERTEX_DATA			0x01
// Whether this navmesh has a "special links" section (ladders, etc)
#define NAVMESH_HAS_SPECIAL_LINKS_SECTION		0x02
// Whether this navmesh is dynamic (it has a matrix which may change)
#define NAVMESH_IS_DYNAMIC						0x04
// Whether this navmesh has any water within it (an optimisation for water-edge extraction)
#define NAVMESH_HAS_WATER						0x08
// Whether this navmesh was added as DLC
#define NAVMESH_DLC								0x10
// Whether this navmesh was modified for DLC swap
#define NAVMESH_MODIFIED_FOR_DLC_SWAP			0x20

// We use the top 8 bits of the navmesh flags to identify which DLC this navmesh belongs to.
// This will only be valid if the NAVMESH_DLC is also set.
#define NAVMESH_DLC_GROUP_MASK					0xFF000000

class CNavMesh
#if (!defined(NAVGEN_TOOL) || __RESOURCECOMPILER)
	: public pgBase
#endif
{
	friend class CNavGen;
	friend class CNavMeshMaker;

public:
	CNavMesh();
	~CNavMesh();

	CNavMesh(datResource & rsc);	// resource constructor
	DECLARE_PLACE(CNavMesh);
	static CNavMesh * StreamingPlaceFn(s32 index, datResourceMap& map, datResourceInfo& header);

//	tests are performed across the mesh to refine the path to a minimal set of

	enum { RORC_VERSION = 2 };

	void Release() { delete this; }	// for ragebuilder

	u32 GetBuildID() const { return m_iBuildID; }
	void SetBuildID(const u32 iBuildID) { m_iBuildID = iBuildID; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

#if !__FINAL && !__PPU
	static CNavMesh * LoadBinary(const char * pFileName, sysPerformanceTimer * pPerfTimer = NULL);
	static CNavMesh * LoadFromMemory(void * pMemBuffer, int iBufferSize, sysPerformanceTimer * pPerfTimer = NULL);
#else
	static CNavMesh * LoadBinary(const char * pFileName);
	static CNavMesh * LoadFromMemory(void * pMemBuffer, int iBufferSize);
#endif

	static bool SaveBinary(CNavMesh * pNavMesh, char * pFileName);

#ifndef GTA_ENGINE
	void PostPlace() { }
#endif

	CNavMesh * Clone();

	static bool ms_bDisplayTimeTakenToLoadNavMeshes;
	static bool ms_bForceAllNavMeshesToUseUnCompressedVertices;

	inline u32 GetNumPolys() const { return m_iNumPolys; }
	inline u32 GetNumVertices() const { return m_iNumVertices; }
	inline u32 GetNumCoverPoints() const { return m_iNumCoverPoints; }
	inline u32 GetFlags() const { return m_iFlags; }
	inline void SetFlags(const u32 f) { m_iFlags = f; }

	// PURPOSE: Returns whether this navmesh originates from DLC
	inline bool GetIsDLC() const { return (m_iFlags & NAVMESH_DLC)!=0; }
	// PURPOSE: Retrieve the DLC group, or -1 if this is not a DLC navmesh
	inline s32 GetDLCGroup() const { return GetIsDLC() ? (s32)(((m_iFlags & NAVMESH_DLC_GROUP_MASK) >> 24)) : -1; }
	// PURPOSE: Set the DLC group
	inline void SetDLCGroup(const u32 i) { Assert(GetIsDLC()); Assert(i<=255); m_iFlags |= ((i << 24)&NAVMESH_DLC_GROUP_MASK); }

	inline u32 GetSizeOfPools() const { return m_iSizeOfPools; }
	inline u32 GetNumSpecialLinks() const { return m_iNumSpecialLinks; }
	inline u32 GetTotalMemoryUsed() const { return m_iTotalMemoryUsed; }

	inline const Matrix34 & GetMatrix() const { return m_Matrix; }
	inline const Vector3 & GetExtents() const { return m_vExtents; }

	inline void SetMatrix(const Matrix34 & m) { m_Matrix = m; }
	inline void SetExtents(const Vector3 & v) { m_vExtents = v; }

	inline CSpecialLinkInfo* GetSpecialLinks() { return m_SpecialLinks; }
	inline aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE> & GetVertexIndexArray() { return *m_VertexIndexArray; }

	inline aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE> & GetCompressedVertexArray() { return *m_CompressedVertexArray; }
	inline Vector3 * GetVertexPool() const { return m_VertexPool; }

	inline aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE> & GetAdjacentPolysArray() { return *m_AdjacentPolysArray; }

	static u32 GetNumPolysInTesselationMesh() { return ms_iNumPolysInTesselationNavMesh; }

	inline u16 GetSpecialLinkIndex(const u32 iLookupIndex) const
	{
		Assert(iLookupIndex < m_iNumSpecialLinksIndices);
		return m_SpecialLinksIndices[iLookupIndex];
	}
	inline void SetSpecialLinks( const u32 iNumLinks, CSpecialLinkInfo * pSpecialLinksArray )
	{
		if(m_SpecialLinks != NULL)
			delete[] m_SpecialLinks;

		m_iNumSpecialLinks = iNumLinks;
		m_SpecialLinks = pSpecialLinksArray;
	}
	inline void SetSpecialLinksIndices( const u32 iNumIndices, u16 * pIndices )
	{
		if(m_SpecialLinksIndices != NULL)
			delete[] m_SpecialLinksIndices;

		m_iNumSpecialLinksIndices = iNumIndices;
		m_SpecialLinksIndices = pIndices;
	}

protected:
//public:

	u32 m_iFlags;
	u32 m_iFileVersion;

	// This matrix places dynamic navmeshes from their local coords into worldspace
	Matrix34 m_Matrix;

	// The extents are used when rescaling compressed vertices back into worldspace
	Vector3 m_vExtents;

	//u16 * m_CompressedVertexData;
	datOwner< aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE> > m_CompressedVertexArray;
	Vector3 * m_VertexPool;

	// These two arrays store the vertex indices & polygon adjacency for every poly in the mesh
	//u16 * m_VertexIndexPool;
	datOwner< aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE> > m_VertexIndexArray;

	//TAdjPoly * m_AdjacentPolyPool;
	datOwner< aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE> > m_AdjacentPolysArray;

	u32 m_iSizeOfPools;

	CAdjacentNavmeshLookups m_AdjacentMeshes;


	// Array of polygon arrays, split to make best use of memory allocation (16k blocks)
	datOwner< aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE> > m_PolysArray;

	// The quadtree helps us swiftly find which triangle is underneath a given point
	datOwner<CNavMeshQuadTree> m_pQuadTree;


	// The array of special links
	CSpecialLinkInfo * m_SpecialLinks;

	// An array of indices, which point to which links are used by which polygons.  With this we can quickly determine
	// which links are to be found on which polygons.
	u16 * m_SpecialLinksIndices;

	u32 m_iNumVertices;
	u32 m_iNumPolys;

	// This is the index of the mesh within the game's entire array of meshes
	u32 m_iIndexOfMesh;

	// The sum total of memory used by this CNavMesh, including all allocations
	u32 m_iTotalMemoryUsed;

	// The number of coverpoints stored in this navmesh's quadtrees
	u32 m_iNumCoverPoints;

	// The number of special links existing within this navmesh (ladders, etc)
	u32 m_iNumSpecialLinks;
	u32 m_iNumSpecialLinksIndices;

	// A pointer to data which we don't want to resource/load/save, and which may change during development.
	// This pointer is skipped during class construction & resourcing.  It avoids the need to completely
	// re-resource all the navmeshes & tools every time I need to add a new variable to CNavMesh.
	// NB: Klaas suggested a better way of doing this, which is to have a stub class which contains the
	// resourced data, but I'm too lazy/scared to implement this just now (next game?)  ;-)
	CNavMeshNonResourcedData * m_pNonResourcedData;

	// The build ID is used to ensure that all navmeshes were build at the same time
	// and identifies errors where the R*N asset builder includes stale files in platform RPFs
	u32 m_iBuildID;

	// Padding to round class up to a 16 byte boundary
	datPadding<4> m_Padding;

public:

	inline u32 GetIndexOfMesh() const { return m_iIndexOfMesh; }
	inline void SetIndexOfMesh(const u32 i) { m_iIndexOfMesh = i; }

	inline const CAdjacentNavmeshLookups * GetAdjacentMeshes() const { return &m_AdjacentMeshes; }
	inline CAdjacentNavmeshLookups * GetAdjacentMeshes() { return &m_AdjacentMeshes; }

	// Returns the vertex from the vertex pool.  This may involve decompressing it.
	inline void GetVertex(u32 index, Vector3 & vOutputVertex) const;

	inline TNavMeshPoly * GetPoly(u32 iIndex) const
	{
#if __DEV
		if(iIndex >= m_iNumPolys)
			printf("GetPoly() : poly index %i out of range. (m_iIndexOfMesh=%i, m_NumPolys=%i)", iIndex, m_iIndexOfMesh, m_iNumPolys);
		Assertf(iIndex < m_iNumPolys, "GetPoly() : poly index %i out of range. (m_iIndexOfMesh=%i, m_NumPolys=%i)", iIndex, m_iIndexOfMesh, m_iNumPolys);
#endif
		//return &m_Polys[iIndex];
		return m_PolysArray->Get(iIndex);
	}

#if __DEV
	u16 GetPolyVertexIndex(const TNavMeshPoly * pPoly, int iPolyVertex) const;
#else
	// Get the index of a poly's vertex
	inline u16 GetPolyVertexIndex(const TNavMeshPoly * pPoly, int iPolyVertex) const
	{
		Assert(pPoly->GetNavMeshIndex() == (TNavMeshIndex)m_iIndexOfMesh);
		Assert(pPoly->GetFirstVertexIndex() + iPolyVertex < (s32)m_iSizeOfPools);
		Assert(iPolyVertex < pPoly->GetNumVertices());

		//return m_VertexIndexPool[pPoly->GetFirstVertexIndex() + iPolyVertex];
		return *m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex() + iPolyVertex);
	}	
#endif

#ifdef NAVGEN_TOOL
	void SetPolysSubArrayIndices();
#endif

	// Gets the adjacent poly to a given poly.
	// iEdgeNum : The index of the edge, in the range ( 0 .. pPoly->GetNumVertices() ).  The edge N is defined by the vector v[N] to v[(N+1)%num_poly_verts]

	inline const TAdjPoly & GetAdjacentPoly(const u32 iIndex) const
	{
		return *m_AdjacentPolysArray->Get(iIndex);
	}

	// Gets the index of a poly within this navmesh.
	inline u32 GetPolyIndex(const TNavMeshPoly * pPoly) const
	{
		Assert(pPoly->GetNavMeshIndex() == (TNavMeshIndex)m_iIndexOfMesh);
		return m_PolysArray->GetIndex(pPoly, pPoly->m_Struct4.m_iPolyArrayIndex);
	}

	inline s32 GetLinkBackAdjacency(const TNavMeshPoly * pFromPoly, const TNavMeshIndex iToNavMesh, const u32 iToPoly, TAdjPoly ** pLinkBackAdjacency=NULL, const bool bNormalAdjacenciesOnly=false)
	{
		Assert(pFromPoly->GetNavMeshIndex()==m_iIndexOfMesh);
		const s32 iNumPts = pFromPoly->GetNumVertices();
		const s32 iFirstVert = pFromPoly->GetFirstVertexIndex();
		for(s32 i=0; i<iNumPts; i++)
		{
			const s32 iIndex = iFirstVert+i;
			const TAdjPoly & adjPoly = *m_AdjacentPolysArray->Get(iIndex);

			if(adjPoly.GetNavMeshIndex(GetAdjacentMeshes())==iToNavMesh &&
				adjPoly.GetPolyIndex()==iToPoly &&
				(!bNormalAdjacenciesOnly || adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_NORMAL) )
			{
				if(pLinkBackAdjacency)
					*pLinkBackAdjacency = m_AdjacentPolysArray->Get(iIndex);
				return i;
			}
		}
		return -1;
	}

	void GetPolyCentroid(u32 iPolyIndex, Vector3 & vCentroid) const;
	void GetPolyCentroidQuick(const TNavMeshPoly * pPoly, Vector3 & vCentroid) const;

	inline bool GetIsDynamic() const { return ((m_iFlags & NAVMESH_IS_DYNAMIC)!=0); }

	inline void MakeLocal(const Vector3 & vPos, Vector3 & vOut)
	{
		Assert(m_iFlags & NAVMESH_IS_DYNAMIC);
		Matrix34 mInvNavMeshMat;
		mInvNavMeshMat.FastInverse(m_Matrix);
		mInvNavMeshMat.Transform(vPos, vOut);
	}
	inline void MakeLocal(Vector3 & vInAndOut)
	{
		Vector3 vTmp = vInAndOut;
		MakeLocal(vTmp, vInAndOut);
	}

	// These functions only meant for during navmesh construction, as they are not really efficient enough for in-game use
	float GetPolyArea(u32 iPolyIndex) const;
	bool GetPolyNormal(Vector3 & vNormal, const TNavMeshPoly * pPoly, const float fDotEps=0.95f);
	void GetPolyEdgeNormal(Vector3 & vEdgeNormal, const TNavMeshPoly * pPoly, int iEdgeNum);
	void GetPolyEdgeMidpoint(Vector3 & vEdgeMidpoint, const TNavMeshPoly * pPoly, int iEdgeNum);

	// Returns the index of the first triangle directly below the given point, or NAVMESH_TRI_NONE if none
	u32 GetPolyBelowPoint(const Vector3 & vPoint, Vector3 & vIntersectPoint, float fDistance);
	u32 GetPolyAbovePoint(const Vector3 & vPoint, Vector3 & vIntersectPoint, float fDistance);
	u32 GetIntersectPoly(const Vector3 & vPoint, const Vector3 & vEndPoint, Vector3 & vIntersectPoint, bool bVerticalLineTest = false);
	u32 GetIntersectPolyNoQuadTree(const Vector3 & vPoint, const Vector3 & vEndPoint, Vector3 & vIntersectPoint, bool bVerticalLineTest = false);

	u32 GetPolyMostlyWithinVolume(const Vector3 & vMin, const Vector3 & vMax, bool bOnlyOnPavement = false, bool bClosestToCentre = false);
	void GetPolyMostlyWithinVolumeR(const CNavMeshQuadTree * pTree, const Vector3 & vMin, const Vector3 & vMax);

	TNavMeshPoly * GetClosestNavMeshPolyEdge(const Vector3 & vPos, const float fMaxDistToLook, Vector3 & vOutClosestPosOnEdge, AcceptPolyCB acceptPolyFn=NULL, AcceptPointCB acceptPointFn=NULL, bool bGetCentroidInsteadOfEdge=true, const u32 iGetClosestFlags=0, const int iNumAvoidSpheres=0, const spdSphere * pAvoidSpheresArray=NULL, const Vector3* pvSearchDir = NULL);
	TNavMeshPoly * GetClosestPointInPoly(const Vector3 & vPos, const float fMaxDistToLook, Vector3 & vOutClosestPosOnEdge, AcceptPolyCB acceptPolyFn=NULL, AcceptPointCB acceptPointFn=NULL, const u32 iGetClosestFlags=0, const int iNumAvoidSpheres=0, const spdSphere * pAvoidSpheresArray=NULL);

	int GetPolyClosestEdgeMidToPoint(const Vector3 & vInputPoint, const TNavMeshPoly * pPoly, Vector3 * pOutEdgeMidVec = NULL);
	int GetPolyClosestEdgePointToPoint(const Vector3 & vInputPoint, const TNavMeshPoly * pPoly, Vector3 & vOutClosestPointOnEdgeVec);

	void ForAllPolys(const Vector3 & vSearchOrigin, const float fSearchDist, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, u32 iForAllPolysFlags);

	bool DisableNonStandardAdjacancyAtPosition(const Vector3 & vPos, const float fMaxDistToLook);
	bool GetNonStandardAdjacancyAtPosition(const Vector3 & vPos, const float fMaxDistToLook, TAdjPoly *& adjPoly);

	bool DisableSpecialLinksFromPosition(const Vector3 & vPos, const float fMaxDistToLook);
	bool DisableSpecialLinksSpanningPosition(const Vector3 & vPos, const float fMaxDistToLook);

	inline CNavMeshQuadTree * GetQuadTree() const { return m_pQuadTree; }
	inline void SetQuadTree(CNavMeshQuadTree * pPtr) { m_pQuadTree = pPtr; }

	inline CNavMeshNonResourcedData * GetNonResourcedData() { return m_pNonResourcedData; }

	inline s32 GetAdjoinedDLCGroup(const eNavMeshEdge edge) const { return m_pNonResourcedData->m_iAdjoinedDLCGroup[edge]; }
	inline void SetAdjoinedDLCGroup(const eNavMeshEdge edge, const s32 s) { m_pNonResourcedData->m_iAdjoinedDLCGroup[edge] = s; }

	void AllocateAsTessellationMesh(const u32 iNumPolys, const u32 iNumVertices);

#if __XENON && !__FINAL
	static bool ms_bUseVmxOptimisations;
#endif
	static bool ms_bUsePrefetching;

#if 1
	static float ms_fSmallestTessellatedEdgeThresholdSqr;
#else
	static const float ms_fSmallestTessellatedEdgeThresholdSqr;
#endif

//protected:

	static const u32 ms_iFileVersion;
	static const u32 ms_iNumPolysInTesselationNavMesh;
	// We set this to false when loading navmeshes for analysis, otherwise we'll accumulate cover-points!
	static bool ms_bLoadCoverPoints;


	static float ms_fGetPolyBestOverlap;
	static u32 ms_iGetPolyBestIndex;
	static bool ms_bGetPolyOnlyOnPavement;
	static bool ms_bGetPolyClosestToCentre;
	static Vector3 ms_vGetPolyVolumeCentre;
	static float ms_fGetPolyLeastDistSqr;
	static bool ms_bGetPolyFoundAnyPoly;

	CNavMeshQuadTree * GetQuadTreeLeaf(const Vector3 & vPos);
	CNavMeshQuadTree * GetQuadTreeLeaf(CNavMeshQuadTree * pTree, const Vector3 & vPos);

	// Establishes poly adjacency within the polys of this mesh
	int EstablishPolyAdjacancy();

#if __NAVMESH_SANITY_CHECK_ADJACENCIES
	void SanityCheckAdjacencies();
#endif

	// Sets the SMALL & LARGE flags accordingly for polys < fSmallSize or > fLargeSize
	void SetPolysSizeFlags(float fSmallSize=2.0f, float fLargeSize=40.0f);
	void SetPolySizeFlags(u32 iPolyIndex, float fSmallSize=2.0f, float fLargeSize=40.0f);

	void SetTessellatedTriangleSizeFlags(TNavMeshPoly * pPoly);

	// Identifies which polys are on the boundary of the mesh, and flags them accordingly
	void IdentifyEdgePolys(const float fEdgeEps=0.125f);

	// Connects the polys of this mesh to the polys in the other mesh (one way only)
	void StitchEdgePolysAcrossMeshes(CNavMesh & otherNavMesh);

	s32 GetSpecialLinksOnPoly(const TNavMeshIndex iNavMesh, const u16 iPolyIndex, CSpecialLinkInfo ** ppOut_SpecialLinks, const int iMaxNum, const u32 iAStarTimeStamp);

//public:

	// Switched polys in the area on/off for ped generation and ped wandering.
	// This is done by setting/reset the NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS flag.
	// eSwitch is a combination of flags from the ESWITCH_NAVMESH_POLYS enumeration
	void SwitchPolysInArea(const Vector3 & vMin, const Vector3 & vMax, const u32 eSwitch);
	void SwitchPolysInAreaInQuadree(const CNavMeshQuadTree * pTree, const Vector3 & vMin, const Vector3 & vMax, const u32 eSwitch);
	void SwitchAllPolysOnOffForPeds(const u32 eSwitch);

	// Do some preprocessing on a newly loaded quadtree.
	void PreProcessNavMeshQuadTree();

#if __ENSURE_THAT_POLY_QUICK_CENTROIDS_ARE_REALLY_WITHIN_POLYS
	void CheckAllQuickPolyCentroidsAreWithinPolys() const;
	void CheckQuickPolyCentroidIsWithinPoly(const int iPolyIndex) const;
#endif

	void CalculateAllPolyMinMaxesForDynamicNavMesh();
	void CalculatePolyMinMaxForDynamicNavMesh(TNavMeshPoly * pPoly);

	// Intersects a ray with this nav mesh
	static Vector3 m_vIntersectRayStart;
	static Vector3 m_vIntersectRayEnd;
	static Vector3 m_vIntersectRayMins;
	static Vector3 m_vIntersectRayMaxs;
	static Vector3 m_fClosestIntersectPos;
	static float m_fClosestIntersectDistSqr;
	static u16 m_iClosestIntersectPolyIndex;

	bool IntersectRay(const Vector3 & vRayStart, const Vector3 & vRayEnd, Vector3 & vecIntersect, float & fIntersectDist, u16 & iHitPolyIndex);
	void IntersectRayR(const CNavMeshQuadTree * pTree);
	bool RayIntersectsPoly(const Vector3 & vRayStart, const Vector3 & vRayEnd, const TNavMeshPoly * pPoly, Vector3 & vecIntersect) const;
	TNavMeshPoly * GetClosestNavMeshPolyImmediate(const Vector3 & vPos, const float fTestRadius, const bool bMustHavePedDensityOrPavement, const bool bMustNotBeIsolated);

	static bool RayIntersectsPoly(const Vector3 & vRayStart, const Vector3 & vRayEnd, int iNumPts, Vector3 * pVerts, Vector3 & vecIntersect);
	static bool RayIntersectsTriangle(const Vector3 & vRayStart, const Vector3 & vRayEnd, Vector3 * pVerts, Vector3 & vecIntersect);

	inline static bool LineSegsIntersect2D(const Vector3 & start1, const Vector3 & end1, const Vector3 & start2, const Vector3 & end2);
	inline static bool LineSegsIntersect2D_LOS(const Vector3 & start1, const Vector3 & end1, const Vector3 & start2, const Vector3 & end2);

	static int LineSegsIntersect2D(const Vector3 & A, const Vector3 & B, const Vector3 & C, const Vector3 & D, Vector3 * pvIsectPos);
	static inline bool LineSegsIntersect2D(const float fStart1X, const float fStart1Y, const float fEnd1X, const float fEnd1Y, const float fStart2X, const float fStart2Y, const float fEnd2X, const float fEnd2Y, float & fT1);

	static inline void DecompressVertex(Vector3 & vOutputVertex, const u16 & iX, const u16 & iY, const u16 & iZ, const Vector3 & vNavMeshMin, const Vector3 & vNavMeshSize)
	{
		const float fRecip65536 = ((float)(1.0 / 65536.0));

		// unpack into 0..1 range
		vOutputVertex.x = (((float)iX) * fRecip65536);
		vOutputVertex.y = (((float)iY) * fRecip65536);
		vOutputVertex.z = (((float)iZ) * fRecip65536);

		// scale by the total extents of the block of sectors
		vOutputVertex.x *= vNavMeshSize.x;
		vOutputVertex.y *= vNavMeshSize.y;
		vOutputVertex.z *= vNavMeshSize.z;

		// offset by mins of entire block
		vOutputVertex.x += vNavMeshMin.x;
		vOutputVertex.y += vNavMeshMin.y;
		vOutputVertex.z += vNavMeshMin.z;
	}
	inline void DecompressVertex(Vector3 & vOutputVertex, const u16 & iX, const u16 & iY, const u16 & iZ)
	{
		CNavMesh::DecompressVertex(vOutputVertex, iX, iY, iZ, m_pNonResourcedData->m_vMinOfNavMesh, m_vExtents);
	}

	static inline void CompressVertex(const Vector3 & vSrcVertex, u16 & iX, u16 & iY, u16 & iZ, const Vector3 & vMin, const Vector3 & vSize)
	{
		float fTmp;
		s32 iTmp;

		// X Component
		fTmp = vSrcVertex.x - vMin.x;
		fTmp /= vSize.x;
		iTmp = (s32)(fTmp * 65536.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 65535)
			iTmp = 65535;
		iX = (u16)iTmp;

		// Y Component
		fTmp = vSrcVertex.y - vMin.y;
		fTmp /= vSize.y;
		iTmp = (s32)(fTmp * 65536.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 65535)
			iTmp = 65535;
		iY = (u16)iTmp;

		// Z Component
		fTmp = vSrcVertex.z - vMin.z;
		fTmp /= vSize.z;
		iTmp = (s32)(fTmp * 65536.0f);

		if(iTmp < 0) iTmp = 0;
		else if(iTmp > 65535) iTmp = 65535;
		iZ = (u16)iTmp;
	}

	static inline void CompressVertex(const Vector3 & vSrcVertex, u8 & iX, u8 & iY, u8 & iZ, const Vector3 & vMin, const Vector3 & vSize)
	{
		float fTmp;
		s32 iTmp;

		// X Component
		fTmp = vSrcVertex.x - vMin.x;
		fTmp /= vSize.x;
		iTmp = (s32)(fTmp * 256.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 255)
			iTmp = 255;
		iX = (u8)iTmp;

		// Y Component
		fTmp = vSrcVertex.y - vMin.y;
		fTmp /= vSize.y;
		iTmp = (s32)(fTmp * 256.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 255)
			iTmp = 255;
		iY = (u8)iTmp;

		// Z Component
		fTmp = vSrcVertex.z - vMin.z;
		fTmp /= vSize.z;
		iTmp = (s32)(fTmp * 256.0f);

		if(iTmp < 0) iTmp = 0;
		else if(iTmp > 255) iTmp = 255;
		iZ = (u8)iTmp;
	}

protected:

	void GetClosestNavMeshPolyImmediateR(const CNavMeshQuadTree * pTree, const Vector3 & vPos, const TShortMinMax & searchMinMax, float & fBestDistSqr, TNavMeshPoly *& pClosestPoly);

	void GetClosestNavMeshPolyEdgeR(const TShortMinMax & minMax, const CNavMeshQuadTree * pTree, AcceptPolyCB acceptFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es);
	void GetClosestNavMeshPolyEdgeNoQuadTree(TShortMinMax & minMax, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es);

	void GetClosestPointInPolyR(const TShortMinMax & minMax, const CNavMeshQuadTree * pTree, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es) const;
	void GetClosestPointInPolyNoQuadTree(const TShortMinMax & minMax, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es) const;

	void ForAllPolys_R(const CNavMeshQuadTree * pTree, TForAllPolysStruct * pSearchStruct) const;
	void ForAllPolys_NoQuadTree(TForAllPolysStruct * pSearchStruct) const;

	bool GetNonStandardAdjacancyAtPositionR(const CNavMeshQuadTree * pTree, const TShortMinMax & minMax, TAdjPoly *& pAdjPoly);

	static u32 ClassifyEdge(CNavMesh * pNavMesh, const Vector3 & vVert1, const Vector3 & vVert2);
	static u32 GetPolyEdgeFlags(CNavMesh * pNavMesh, const u32 iTriIndex);

	void DuplicateAllVerticesAsUncompressed();

	static void SaveQuadtree(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, fiStream * pFilePtr);

	static CNavMeshQuadTree * LoadQuadtree(CNavMesh * pNavMesh, rage::fiStream * pStream);

	void PrefetchNavMeshPoly(u32 iPolyIndex);
	void PrefetchNavMeshPoly_VertexIndices(const TNavMeshPoly * pPoly);
	void PrefetchNavMeshPoly_AdjacentPolys(const TNavMeshPoly * pPoly);
	void PrefetchNavMeshPoly_VertexData(const TNavMeshPoly * pPoly);
	void PrefetchAllNavMeshPolyData(const TNavMeshPoly * pPoly);
	void PrefetchAllNavMeshPolyData(const u32 iPolyIndex);

	CNavMeshQuadTree * CloneQuadTree(CNavMeshQuadTree * pTree);
};

enum EPointsInPolyType
{
	POINTSINPOLY_CENTROID_ONLY,
	POINTSINPOLY_NORMAL,
	POINTSINPOLY_EXTRA,
	POINTSINPOLY_VERTICESANDCENTROID
};


inline int CreatePointsInPolyNormal(const CNavMesh * pNavMesh, const TNavMeshPoly * pPoly, const Vector3 * pPolyPts, const EPointsInPolyType ePointInPolyType, Vector3 * pOutPts, const bool bCreateOnOpenEdges=false)
{
	static const float fOneThird = ((float)(1.0f / 3.0f));

	const int iNumPts = pPoly->GetNumVertices();
	int iNumPtsCreated = 0;
	int lastv, v;

	if(ePointInPolyType==POINTSINPOLY_VERTICESANDCENTROID)
	{
		Vector3 vCentroid;

		if(pPoly->GetIsTessellatedFragment())
		{
			Assert(pPoly->GetNumVertices()==3);
			vCentroid = (pPolyPts[0] + pPolyPts[1] + pPolyPts[2]) * fOneThird;
		}
		else
		{
			pNavMesh->GetPolyCentroidQuick(pPoly, vCentroid);
		}

		pOutPts[iNumPtsCreated] = vCentroid;
		iNumPtsCreated++;

		lastv = iNumPts-1;
		for(v=0; v<iNumPts; v++)
		{
			pOutPts[iNumPtsCreated++] = (pPolyPts[v] + vCentroid) * 0.5f;

			lastv = v;
		}
		return iNumPtsCreated;
	}

	Vector3 vCentroid(0.0f, 0.0f, 0.0f);
	const float fRecip = 1.0f / ((float)iNumPts);

	// Generate centroid
	for(v=0; v<iNumPts; v++)
	{
		vCentroid += pPolyPts[v];
	}
	vCentroid *= fRecip;

	if(ePointInPolyType==POINTSINPOLY_NORMAL || ePointInPolyType==POINTSINPOLY_EXTRA)
	{
		// points halfway between the centroid & each vertex
		for(v=0; v<iNumPts; v++)
		{
			pOutPts[iNumPtsCreated++] = (pPolyPts[v] + vCentroid) * 0.5f;
			// NB : Using an actual corner point may fix paths which cannot move onto adjacent polys because the
			// choice of points-in-polys are all obscured.  But it can lead to errors in the refinement..
		}

		// Points halfway along each edge, and averaged with centroid
		lastv = iNumPts-1;
		for(v=0; v<iNumPts; v++)
		{
			if( !bCreateOnOpenEdges && pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex()+lastv ).GetPolyIndex() == NAVMESH_POLY_INDEX_NONE )
			{
				// If 'pAdjacentPolys' is provided, then we don't create points along edges with no adjacent poly, so set to a large value..
				pOutPts[iNumPtsCreated++] = Vector3(g_fVeryLargeValue, g_fVeryLargeValue, g_fVeryLargeValue);
			}
			else
			{
				// points halfway along each edge & nudged towards centroid
				pOutPts[iNumPtsCreated++] = (pPolyPts[lastv] + pPolyPts[v] + vCentroid) * fOneThird;
			}

			lastv = v;
		}
	}

	// Add the centroid
	pOutPts[iNumPtsCreated++] = vCentroid;

	if(ePointInPolyType==POINTSINPOLY_EXTRA)
	{
		// points closer to the vertex than the centroid
		for(v=0; v<iNumPts; v++)
		{
			pOutPts[iNumPtsCreated++] = (pPolyPts[v] * 0.95f) + (vCentroid * 0.05f);
		}

		// Points halfway along each edge, and closer to the edge than the
		lastv = iNumPts-1;
		for(v=0; v<iNumPts; v++)
		{
			if( !bCreateOnOpenEdges && pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex()+lastv ).GetPolyIndex() == NAVMESH_POLY_INDEX_NONE )
			{
				// If 'pAdjacentPolys' is provided, then we don't create points along edges with no adjacent poly, so set to a large value..
				pOutPts[iNumPtsCreated++] = Vector3(g_fVeryLargeValue, g_fVeryLargeValue, g_fVeryLargeValue);
			}
			else
			{
				// points halfway along each edge & nudged towards centroid
				pOutPts[iNumPtsCreated++] = (((pPolyPts[lastv] + pPolyPts[v]) * 0.5f) * 0.8f) + (vCentroid * 0.2f);
			}

			lastv = v;
		}
	}

	return iNumPtsCreated;
}

inline int CreatePointsInPoly(const CNavMesh * pNavMesh, const TNavMeshPoly * pPoly, const Vector3 * pPolyPts, const EPointsInPolyType ePointInPolyType, Vector3 * pOutPts, const bool bCreateOnOpenEdges=false)
{
	return CreatePointsInPolyNormal(pNavMesh, pPoly, pPolyPts, ePointInPolyType, pOutPts, bCreateOnOpenEdges);
}

inline void GetPointInPolyFromPointEnum(const CNavMesh * pNavMesh, const TNavMeshPoly * pPoly, const Vector3 * pPolyPts, Vector3 * vPoint, const int iPointEnum, const bool bUseMorePointsInPoly)
{
	Assert(iPointEnum != NAVMESHPOLY_POINTENUM_CENTROID);
	Assert(iPointEnum != NAVMESHPOLY_POINTENUM_SPECIAL_LINK_ENDPOS);
	
	static const float fOneThird = ((float)(1.0f / 3.0f));

	if(iPointEnum >= NAVMESHPOLY_POINTENUM_VERTSANDEDGES_FIRST)
	{
		Vector3 vCentroid;
		if(pNavMesh->GetIndexOfMesh()!=NAVMESH_INDEX_TESSELLATION)
			pNavMesh->GetPolyCentroidQuick(pPoly, vCentroid);
		else
			pNavMesh->GetPolyCentroid(pNavMesh->GetPolyIndex(pPoly), vCentroid);

		if(iPointEnum == NAVMESHPOLY_POINTENUM_VERTSANDEDGES_FIRST)
		{
			*vPoint = vCentroid;
			return;
		}
		else
		{
			int iPt = iPointEnum - (NAVMESHPOLY_POINTENUM_VERTSANDEDGES_FIRST+1);
			*vPoint = (pPolyPts[iPt>>1] + vCentroid) * 0.5f;
			return;
		}
	}

	const bool bLessPoints = false;
	const bool bMorePoints = bUseMorePointsInPoly; //ShouldUseMorePointsForPoly(*pPoly);

	const int iNumPts = pPoly->GetNumVertices();

	Vector3 vCentroid(0,0,0);
	const float fRecip = 1.0f / ((float)iNumPts);
	int v;

	// Get the centroid
	for(v=0; v<iNumPts; v++)
	{
		vCentroid += pPolyPts[v];
	}
	vCentroid *= fRecip;

	// For polys flagged as NAVMESHPOLY_SMALL, there is only one option - the poly centroid.
	// Unless ALSO flagged as NAVMESHPOLY_TESSELLATED_FRAGMENT - in which case we will create
	// the same amount as a poly flagged as neither SMALL or LARGE (ie. of 'normal' size).
	// This is because the poly is likely to be tessellated around a dynamic object, and we'll
	// need more choice of points here.

	if(bLessPoints)
	{
		Assert(iPointEnum == 0);

		*vPoint = vCentroid;
		return;
	}

	int iTestIndex = iNumPts;

	// Point just inside a vertex ?
	if(iPointEnum < iTestIndex)
	{
		// points halfway between the centroid & each vertex
		*vPoint = (pPolyPts[iPointEnum] + vCentroid) * 0.5f;
		return;
	}

	// Point just inside an edge ?
	iTestIndex += iNumPts;
	if(iPointEnum < iTestIndex)
	{
		int iPt = iPointEnum - iNumPts;
		int iLastPt = iPt-1;
		if(iLastPt < 0) iLastPt += iNumPts;

		// points halfway along each edge & nudged towards centroid
		*vPoint = (pPolyPts[iLastPt] + pPolyPts[iPt] + vCentroid) * fOneThird;
		return;
	}

	if(!bMorePoints || iPointEnum == iNumPts*2)
	{
		*vPoint = vCentroid;
		return;
	}

	// If we have a poly flagged as NAVMESHPOLY_LARGE, then we have a further set
	// of points to choose from - so as to increase the granularity for large polys
	iTestIndex = (iNumPts*3)+1;
	if(iPointEnum < iTestIndex)
	{
		// Points between the vertices & centroid, but weighted 0.8/0.2 towards the vertex
		int iPt = iPointEnum - ((iNumPts*2)+1);
		*vPoint = (pPolyPts[iPt] * 0.95f) + (vCentroid * 0.05f);
		return;
	}

	iTestIndex += iNumPts;
	if(iPointEnum < iTestIndex)
	{
		// Points halfway along each edge, and closer to the edge than the centroid
		int iPt = iPointEnum - ((iNumPts*3)+1);
		int iLastPt = iPt-1;
		if(iLastPt < 0) iLastPt += iNumPts;

		// points halfway along each edge & nudged towards centroid
		*vPoint = (((pPolyPts[iLastPt] + pPolyPts[iPt]) * 0.5f) * 0.8f) + (vCentroid * 0.2f);
		return;
	}

	// Should never get here..
	Assert(0);
	*vPoint = vCentroid;
}




enum LINE_CLASSIFICATION
{
//	COLLINEAR,			// both lines are parallel and overlap each other
	LINES_INTERSECT,	// lines intersect, but their segments do not
	SEGMENTS_INTERSECT,	// both line segments bisect each other
//	A_BISECTS_B,		// line segment B is crossed by line A
//	B_BISECTS_A,		// line segment A is crossed by line B
	PARALLEL			// the lines are parallel
};


#if __RESOURCECOMPILER || defined(NAVGEN_TOOL)
#define __NAVMESH_USE_RAGE_MEM_ALLOCATOR	0		// Always disable when compiling Ragebuilder or the navmesh generation tools
#else
#define __NAVMESH_USE_RAGE_MEM_ALLOCATOR	1		// Use the rage virtual mem allocator in-game
#endif

template <typename T> class CNavMeshAlloc
{
public:
	CNavMeshAlloc(u32 iAlignment=16) { m_iAlignment = iAlignment; }
	inline T* New()
	{
#if __NAVMESH_USE_RAGE_MEM_ALLOCATOR
		void * pMem = sysMemAllocator::GetMaster().Allocate(sizeof(T), m_iAlignment, MEMTYPE_GAME_VIRTUAL/*MEMTYPE_RESOURCE_VIRTUAL*/);
		T * pObj = new(pMem) T;
		return pObj;
#else
		return rage_new T;
#endif
	}
	inline void Delete(T*pObj)
	{
#if __NAVMESH_USE_RAGE_MEM_ALLOCATOR
		pObj->~T();
		sysMemAllocator::GetMaster().Free(pObj);
#else
		delete pObj;
#endif
	}
	inline T* VectorNew(int iNumElements)
	{
#if __NAVMESH_USE_RAGE_MEM_ALLOCATOR
		// JB : 21/1/07 - have had some trouble with Rage memory allocation returning addresses which overlap already allocated address ranges -
		// meaning that I end up overwriting some of my data.  I couldn't figure out why this was happening, but in an attempt to remedy it I am
		// now allocating an extra "m_iAlignment" bytes at the end of each allocation.
		void * pMem = sysMemAllocator::GetMaster().Allocate((sizeof(T)*iNumElements) + m_iAlignment, m_iAlignment, MEMTYPE_GAME_VIRTUAL/*MEMTYPE_RESOURCE_VIRTUAL*/);
		T * pObj = new(pMem) T[iNumElements];
		return &pObj[0];
#else
		return rage_new T[iNumElements];
#endif
	}
	inline void VectorDelete(T*pArray, int iNumElements, bool bDestructEachElement=true)
	{
#if __NAVMESH_USE_RAGE_MEM_ALLOCATOR
		if(bDestructEachElement)
		{
			int t;
			for(t=0; t<iNumElements; t++)
				pArray[t].~T();
		}
		sysMemAllocator::GetMaster().Free(pArray);
#else
		bDestructEachElement = bDestructEachElement;	// Shut up the compiler
		iNumElements=iNumElements;
		delete[] pArray;
#endif
	}
private:
	u32 m_iAlignment;
};

//*************************************************************************************
//	Some functions for byte-reordering.
//*************************************************************************************

#ifndef COMPILE_TOOL_MESH
extern const bool g_bEndianSwap;
#endif

inline void ByteSwap16(void * instance)
{
	if(g_bEndianSwap)
	{
		u16 temp = *(u16*)instance;
		*(u16*)instance = (temp >> 8) | (temp << 8);
	}
}

inline void ByteSwap32(void * instance)
{
	if(g_bEndianSwap)
	{
		u32 temp = *(u32*)instance;
		*(u32*)instance = (temp >> 24) | ((temp >> 8) & 0xFF00) | ((temp << 8) & 0xFF0000) | (temp << 24);
	}
}

//*************************************************************************************
//	Some macros & functions for prefetching navmesh data on different platforms
//*************************************************************************************

#define NAVMESH_PREFETCH_MEMORY(x)			PrefetchDC(x)

// Prefetch all the unique 128 byte cache lines in the block of data
// NB : This could be made much smarter by avoiding stepping across each element one-by-one.
inline void PrefetchContiguousMemory(const u8 * pAddress, const u32 iElementSize, u32 iNumElements)
{
	Assert(pAddress && iElementSize && iNumElements);

	iNumElements = (iNumElements*iElementSize)/128;

	NAVMESH_PREFETCH_MEMORY((void*)pAddress);

	while(iNumElements--)
	{
		pAddress += iElementSize;
		NAVMESH_PREFETCH_MEMORY(pAddress);
	}
}

#define PREFETCH_CONTIGUOUS_MEMORY(addr, datasize, numelements)	PrefetchContiguousMemory(addr, datasize, numelements);

inline void CNavMesh::PrefetchNavMeshPoly(const u32 iPolyIndex)
{
	//NAVMESH_PREFETCH_MEMORY(&m_Polys[iPolyIndex]);
	NAVMESH_PREFETCH_MEMORY( m_PolysArray->Get(iPolyIndex) );
}

inline void CNavMesh::PrefetchNavMeshPoly_VertexIndices(const TNavMeshPoly * pPoly)
{
	//u16 * iIndices = &m_VertexIndexPool[pPoly->GetFirstVertexIndex()];
	u16 * iIndices = m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex());
	PREFETCH_CONTIGUOUS_MEMORY((u8*)iIndices, sizeof(u16), pPoly->GetNumVertices());
}

inline void CNavMesh::PrefetchNavMeshPoly_AdjacentPolys(const TNavMeshPoly * pPoly)
{
	TAdjPoly * pAdjPolys = m_AdjacentPolysArray->Get(pPoly->GetFirstVertexIndex());
	PREFETCH_CONTIGUOUS_MEMORY((u8*)pAdjPolys, sizeof(TAdjPoly), pPoly->GetNumVertices());
}

inline void CNavMesh::PrefetchNavMeshPoly_VertexData(const TNavMeshPoly * pPoly)
{
	s32 iNumVertices = pPoly->GetNumVertices();
	//u16 * iIndices = &m_VertexIndexPool[pPoly->GetFirstVertexIndex()];
	s32 i;

	if(m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
	{
		for(i=0; i<iNumVertices; i++)
		{
			CNavMeshCompressedVertex * pCompressedData = m_CompressedVertexArray->Get( *m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex()+i) );
			NAVMESH_PREFETCH_MEMORY(pCompressedData);
#if __PPU	// please fixme!
			pCompressedData=pCompressedData;
#endif
		}
	}
	else
	{
		for(i=0; i<iNumVertices; i++)
		{
			Vector3 * pUncompressedData = &m_VertexPool[ *m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex()+i) ];
			NAVMESH_PREFETCH_MEMORY(pUncompressedData);
#if __PPU	// please fixme!
			pUncompressedData=pUncompressedData;
#endif
		}
	}
}

inline void CNavMesh::PrefetchAllNavMeshPolyData(const TNavMeshPoly * pPoly)
{
	PrefetchNavMeshPoly_VertexIndices(pPoly);
	PrefetchNavMeshPoly_AdjacentPolys(pPoly);
	PrefetchNavMeshPoly_VertexData(pPoly);
}
inline void CNavMesh::PrefetchAllNavMeshPolyData(const u32 iPolyIndex)
{
	//TNavMeshPoly * pPoly = &m_Polys[iPolyIndex];
	TNavMeshPoly * pPoly = m_PolysArray->Get(iPolyIndex);
	PrefetchNavMeshPoly_VertexIndices(pPoly);
	PrefetchNavMeshPoly_AdjacentPolys(pPoly);
	PrefetchNavMeshPoly_VertexData(pPoly);
}


//******************************************************************************************************************

#if __NAVMESH_USE_PREFETCHING

#define PREFETCH_NAVMESH_POLY(pNavMesh, iPolyIndex)	pNavMesh->PrefetchNavMeshPoly(iPolyIndex);
#define PREFETCH_NAVMESH_POLY_VERTEX_INDICES(pNavMesh, pPoly)	pNavMesh->PrefetchNavMeshPoly_VertexIndices(pPoly);
#define PREFETCH_NAVMESH_POLY_ADJACENT_POLYS(pNavMesh, pPoly)	pNavMesh->PrefetchNavMeshPoly_AdjacentPolys(pPoly);
#define PREFETCH_NAVMESH_POLY_VERTEX_DATA(pNavMesh, pPoly)	pNavMesh->PrefetchNavMeshPoly_VertexData(pPoly);
#define PREFETCH_ALL_NAVMESH_POLY_DATA(pNavMesh, pPoly)	pNavMesh->PrefetchAllNavMeshPolyData(pPoly);

#else	// __NAVMESH_USE_PREFETCHING

#endif	// __NAVMESH_USE_PREFETCHING

//******************************************************************************************************************


#if __XENON || __PS3
inline void CNavMesh::GetVertex(u32 index, Vector3 & vOutputVertex) const
{
	if(m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
	{
		const u16 * pCompressedData = m_CompressedVertexArray->Get(index)->data;

		// load integers into __vector4, dividing by 65536
		_hvector4 vShorts = _hvector4(__vor(__vector4(__lvlx(pCompressedData, 0)), __vector4(__lvrx(pCompressedData, 16))));
		_uvector4 vIntegers = _uvector4(__vmrghh(_hvector4(_vzero), vShorts));
		const __vector4 vUnitValues = __vcfux(vIntegers, 16);
		// scale by navmesh extents & add on min of navmesh
		vOutputVertex.xyzw = __vmaddfp(vUnitValues, m_vExtents, m_pNonResourcedData->m_vMinOfNavMesh);
	}
	else
	{
		vOutputVertex = m_VertexPool[index];
	}

	if(m_iFlags & NAVMESH_IS_DYNAMIC)
	{
		m_Matrix.Transform(vOutputVertex);
	}
}

#else	// __XENON

inline void
CNavMesh::GetVertex(u32 index, Vector3 & vOutputVertex) const
{
	if(m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
	{
		const float fRecip65536 = ((float)(1.0 / 65536.0));

		const u16 * pCompressedData = m_CompressedVertexArray->Get(index)->data;

		// unpack into 0..1 range
		vOutputVertex.x = (((float)pCompressedData[0]) * fRecip65536);
		vOutputVertex.y = (((float)pCompressedData[1]) * fRecip65536);
		vOutputVertex.z = (((float)pCompressedData[2]) * fRecip65536);

		// scale by the total extents of the block of sectors
		vOutputVertex.x *= m_vExtents.x;
		vOutputVertex.y *= m_vExtents.y;
		vOutputVertex.z *= m_vExtents.z;

		// offset by mins of entire block
		vOutputVertex.x += m_pNonResourcedData->m_vMinOfNavMesh.x;
		vOutputVertex.y += m_pNonResourcedData->m_vMinOfNavMesh.y;
		vOutputVertex.z += m_pNonResourcedData->m_vMinOfNavMesh.z;
	}
	else
	{
		vOutputVertex = m_VertexPool[index];
	}

	if(m_iFlags & NAVMESH_IS_DYNAMIC)
	{
		m_Matrix.Transform(vOutputVertex);
	}
}
#endif



#ifndef NAVGEN_TOOL

inline bool CNavMesh::LineSegsIntersect2D(const Vector3 & start1, const Vector3 & end1, const Vector3 & start2, const Vector3 & end2)
{
//	Vec2f u = end1 - start1;
//	Vec2f v = end2 - start2;
	const Vec2f u( end1.x - start1.x, end1.y - start1.y);
	const Vec2f v( end2.x - start2.x, end2.y - start2.y);
	const Vec2f vPerp(-v.GetY(), v.GetX());
	const Vec2f uPerp(-u.GetY(), u.GetX());
//	Vec2f w = (start1 - start2);
	const Vec2f w( start1.x - start2.x, start1.y - start2.y );

	const float vPerpDotU = -Dot(vPerp, u);

	if (fabsf(vPerpDotU) < FLT_EPSILON)
	{
		// lines are parallel, no intersection
		return false;
	}

	const float s = Dot(vPerp, w) / vPerpDotU;
	const float t = Dot(uPerp, w) / vPerpDotU;

	if (s >= 0.0f && s <= 1.0f && t >= 0.0f && t <= 1.0f)
	{
//		hitT1Out = s;
//		hitT2Out = t;
		return true;
	}

	return false;
}

#else // NAVGEN_TOOL

inline bool CNavMesh::LineSegsIntersect2D(const Vector3 & start1, const Vector3 & end1, const Vector3 & start2, const Vector3 & end2)
{
	//	Vec2f u = end1 - start1;
	//	Vec2f v = end2 - start2;
	const double u[2] = { end1.x - start1.x, end1.y - start1.y };
	const double v[2] = { end2.x - start2.x, end2.y - start2.y };
	const double vPerp[2] = { -v[1], v[0] };
	const double uPerp[2] = { -u[1], u[0] };

	const double w[2] = { start1.x - start2.x, start1.y - start2.y };

	//const float vPerpDotU = -Dot(vPerp, u);
	const double vPerpDotU = -((vPerp[0] * u[0]) + (vPerp[1] * u[1]));

	if (abs(vPerpDotU) < FLT_EPSILON)
	{
		// lines are parallel, no intersection
		return false;
	}

//	const float s = Dot(vPerp, w) / vPerpDotU;
//	const float t = Dot(uPerp, w) / vPerpDotU;

	const double s = ( (vPerp[0] * w[0]) + (vPerp[1] * w[1]) ) / vPerpDotU;
	const double t = ( (uPerp[0] * w[0]) + (uPerp[1] * w[1]) ) / vPerpDotU;

	if (s >= 0.0 && s <= 1.0 && t >= 0.0 && t <= 1.0)
	{
		//		hitT1Out = s;
		//		hitT2Out = t;
		return true;
	}

	return false;
}

#endif // NAVGEN_TOOL
/*
#define	TVAL_THRESHOLD			1.0e-2f
static const float lowerT = -TVAL_THRESHOLD;
static const float upperT = 1.0f+TVAL_THRESHOLD;

inline bool CNavMesh::LineSegsIntersect2D(const Vector3 & start1, const Vector3 & end1, const Vector3 & start2, const Vector3 & end2)
{
	const float L1 = end1.x - start1.x;
	const float M1 = end1.y - start1.y;
	const float L2 = end2.x - start2.x;
	const float M2 = end2.y - start2.y;
	static const float threshold = 0.00001f;

	const float denominator = L1 * M2 - L2 * M1;
	if(Abs(denominator) < threshold) // Threshold for parallel lines
	{
		return false;
	}

	if(Abs(L2) < threshold)// Parallel to Y Axis
	{
		if(Abs(L1) < threshold)
		{
			// Line 1 is also close to parallel to the y-axis.  
			// It might still be possible to get a good answer,
			// so further investigation may be warranted.
			return false;
		}
		const float hitT1 = (start2.x - start1.x) / L1;
		const float hitT2 = (M1 * hitT1 + start1.y - start2.y) / M2;
		return (hitT1>=lowerT && hitT1<=upperT && hitT2>=lowerT && hitT2<=upperT);
	}

	if(Abs(M2) < threshold)// Parallel to X Axis
	{
		if(Abs(M1) < threshold)
		{
			// Line 1 is also close to parallel to the x-axis.  
			// See note in above block.
			return false;
		}
		const float hitT1 = (start2.y - start1.y) / M1;
		const float hitT2 = (L1 * hitT1 + start1.x - start2.x) / L2;
		return (hitT1>=lowerT && hitT1<=upperT && hitT2>=lowerT && hitT2<=upperT);
	}

	const float hitT1 = (M2 * (start2.x - start1.x) + L2 * (start1.y - start2.y)) / denominator;
	const float hitT2 = (L1 * hitT1 + start1.x - start2.x) / L2;
	return (hitT1>=lowerT && hitT1<=upperT && hitT2>=lowerT && hitT2<=upperT);
}
*/

// NAME : LineSegsIntersect2D_LOS
// PURPOSE : Specialised intersection function for use in TestNavMeshLineOfSight
// The first line segment's intersection ranges of T are allowed to be slightly outside of 0.0 to 1.0
// The first line segment is always the vStartPos -> vEndPos (wheras the second is the edge being tested).
// This allows for the cases where a polygon is extremely narrow, and thus the endpoint being tested is barely
// on the edge itself.
inline bool
CNavMesh::LineSegsIntersect2D_LOS(const Vector3 & start1, const Vector3 & end1, const Vector3 & start2, const Vector3 & end2)
{
	static const float fMinT = -0.01f;
	static const float fMaxT = 1.01f;

	const float L1 = end1.x - start1.x;
	const float M1 = end1.y - start1.y;
	const float L2 = end2.x - start2.x;
	const float M2 = end2.y - start2.y;
	const float threshold = 0.00001f;

	const float denominator = L1 * M2 - L2 * M1;
	if(Abs(denominator) < threshold) // Threshold for parallel lines
	{
		return false;
	}

	if(Abs(L2) < threshold)// Parallel to Y Axis
	{
		if(Abs(L1) < threshold)
		{
			// Line 1 is also close to parallel to the y-axis.  
			// It might still be possible to get a good answer,
			// so further investigation may be warranted.
			return false;
		}
		const float hitT1 = (start2.x - start1.x) / L1;
		const float hitT2 = (M1 * hitT1 + start1.y - start2.y) / M2;
		return (hitT1>=fMinT && hitT1<=fMaxT && hitT2>=fMinT && hitT2<=fMaxT);
	}

	if(Abs(M2) < threshold)// Parallel to X Axis
	{
		if(Abs(M1) < threshold)
		{
			// Line 1 is also close to parallel to the x-axis.  
			// See note in above block.
			return false;
		}
		const float hitT1 = (start2.y - start1.y) / M1;
		const float hitT2 = (L1 * hitT1 + start1.x - start2.x) / L2;
		return (hitT1>=fMinT && hitT1<=fMaxT && hitT2>=fMinT && hitT2<=fMaxT);
	}

	const float hitT1 = (M2 * (start2.x - start1.x) + L2 * (start1.y - start2.y)) / denominator;
	const float hitT2 = (L1 * hitT1 + start1.x - start2.x) / L2;
	return (hitT1>=fMinT && hitT1<=fMaxT && hitT2>=fMinT && hitT2<=fMaxT);
}

inline bool
CNavMesh::LineSegsIntersect2D(
	const float fStart1X, const float fStart1Y,
	const float fEnd1X, const float fEnd1Y,
	const float fStart2X, const float fStart2Y,
	const float fEnd2X, const float fEnd2Y,
	float & fT1)
{
	const float firstLineSlopeX = fEnd1X - fStart1X;
	const float firstLineSlopeY = fEnd1Y - fStart1Y;
	const float secondLineSlopeX = fEnd2X - fStart2X;
	const float secondLineSlopeY = fEnd2Y - fStart2Y;

	const float s = (-firstLineSlopeY * (fStart1X - fStart2X) + firstLineSlopeX * (fStart1Y - fStart2Y)) / (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);
	const float t = (secondLineSlopeX * (fStart1Y - fStart2Y) - secondLineSlopeY * (fStart1X - fStart2X)) / (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);

	if (s >= 0.0f && s <= 1.0f && t >= 0.0f && t <= 1.0f)
	{
		fT1 = t;
		//float intersectionPointX = fStart1X + (t * firstLineSlopeX);
		//float intersectionPointY = fStart1Y + (t * firstLineSlopeY);
		return true;
	}
	return false; // No collision
}

#define SIZE_TEST_LOS_STACK					256
#define SIZE_IMMEDIATEMODE_TEST_LOS_STACK	SIZE_TEST_LOS_STACK

struct TTestLosStack
{
	TNavMeshPoly * pTestPoly;
	TNavMeshPoly * pLastPoly;
	int iVertex;
};

inline u8 CompressFixed8(const float fVal, const float fMinVal, const float fRange)
{
	float fTmp = fVal - fMinVal;
	fTmp /= fRange;
	s32 iTmp = (s32)(fTmp * 256.0f);
	if(iTmp < 0) iTmp = 0;
	else if(iTmp > 255) iTmp = 255;
	return (u8) iTmp;
}
inline float DecompressFixed8(const u8 iVal, const float fMinVal, const float fRange)
{
	const float fRecip256 = ((float)(1.0 / 256.0));
	return ( ( (((float)iVal) * fRecip256) * fRange) + fMinVal);
}
inline u16 CompressFixed12(const float fVal, const float fMinVal, const float fRange)
{
	float fTmp = fVal - fMinVal;
	fTmp /= fRange;
	s32 iTmp = (s32)(fTmp * 4096.0f);
	if(iTmp < 0) iTmp = 0;
	else if(iTmp > 4095) iTmp = 4095;
	return (u16) iTmp;
}
inline float DecompressFixed12(const u16 iVal, const float fMinVal, const float fRange)
{
	const float fRecip4096 = ((float)(1.0 / 4096.0));
	return ( ( (((float)iVal) * fRecip4096) * fRange) + fMinVal);
}
inline u16 CompressFixed16(const float fVal, const float fMinVal, const float fRange)
{
	float fTmp = fVal - fMinVal;
	fTmp /= fRange;
	s32 iTmp = (s32)(fTmp * 65536.0f);
	if(iTmp < 0) iTmp = 0;
	else if(iTmp > 65535) iTmp = 65535;
	return (u16) iTmp;
}
inline float DecompressFixed16(const u16 iVal, const float fMinVal, const float fRange)
{
	const float fRecip65536 = ((float)(1.0 / 65536.0));
	return ( ( (((float)iVal) * fRecip65536) * fRange) + fMinVal);
}

#define MAX_NUM_HIERACHICAL_LINKS	NAVMESHPOLY_MAX_NUM_VERTICES

//***************************************************************
//	CHierarchicalNavNode
//	This is a single node in the hierarchical node-graph.

class CHierarchicalNavNode
{
	friend class CNavGen;
	friend class CHierarchicalNavData;
public:

	inline u32 GetCompactedX() const { return ((u32)m_Struct1.m_iX); }
	inline u32 GetCompactedY() const { return ((u32)m_Struct1.m_iY); }
	inline u32 GetCompactedZ() const { return ((u32)m_Struct1.m_iZ); }
	inline u32 GetFlags() const { return ((u32)m_Struct1.m_iFlags); }
	inline u32 GetNavMeshIndex() const { return ((u32)m_Struct2.m_iNavMeshIndex); }
	// Num links is stored in 4 bits as 0..15, but actually counts 1..16 (since a node can
	// never have zero links) therefore we add one to the number when accessing it.
	inline u32 GetNumLinks() const { return (((u32)m_Struct3.m_iNumLinks)+1); }
	inline u32 GetStartOfLinkData() const { return ((u32)m_Struct2.m_iStartOfLinkData); }

	inline void GetNodePosition(Vector3 & vPos, const Vector3 & vNavMeshMins, const Vector3 & vNavMeshSize) const
	{
		vPos.x = DecompressFixed8(m_Struct1.m_iX, vNavMeshMins.x, vNavMeshSize.x);
		vPos.y = DecompressFixed8(m_Struct1.m_iY, vNavMeshMins.y, vNavMeshSize.y);
		vPos.z = DecompressFixed12(m_Struct1.m_iZ, vNavMeshMins.z, vNavMeshSize.z);
	}

	inline s32 GetPedDensity() const { return ((s32)m_Struct3.m_iPedDensity); }
	inline u32 GetAreaRepresented() const { return ((u32)m_Struct3.m_iAreaRepresented); }
	inline u32 GetNodeRadius() const { return ((u32)m_Struct3.m_iNodeRadius); }
	
	inline u32 GetPedSpawningEnabled() const { return ((u32)m_Struct3.m_bPedSpawningEnabled); }
	inline void SetPedSpawningEnabled(const bool b) { m_Struct3.m_bPedSpawningEnabled = b; }
	
	inline u32 GetParentNavMeshIndex() const { return ((u32)m_Struct4.m_iParentNavMeshIndex); }
	inline u32 GetParentNodeIndex() const { return ((u32)m_Struct4.m_iParentNodeIndex); }
	inline u32 GetIsOpen() const { return ((u32)m_Struct4.m_bIsOpen); }
	inline u32 GetIsClosed() const { return ((u32)m_Struct4.m_bIsClosed); }
	inline void SetParentNavMeshIndex(const u32 i) { m_Struct4.m_iParentNavMeshIndex = i; }
	inline void SetParentNodeIndex(const u32 i) { m_Struct4.m_iParentNodeIndex = i; }
	inline void SetIsOpen(const bool b) { m_Struct4.m_bIsOpen = b; }
	inline void SetIsClosed(const bool b) { m_Struct4.m_bIsClosed = b; }

	inline void SetIsDebugMarked(const bool b) { m_Struct4.m_bDebugMarked = b; }
	inline bool GetIsDebugMarked() { return m_Struct4.m_bDebugMarked; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct & s);
#endif

private:

	//*********************************************************************************
	// Compacted (x,y,z) coordinates.  X and Y can be represented to 0.4m in one byte.
	// Z needs a larger integer to store it, since its range can vary per-navmesh.

	union
	{
		u32 m_iAsInteger1;
		struct  
		{
			BITFIELD4(
				u32,
				m_iX,		8,
				m_iY,		8,
				m_iZ,		12,
				m_iFlags,	4
			);
		} m_Struct1;
	};

	union
	{
		u32 m_iAsInteger2;
		struct  
		{
			BITFIELD3(
				u32,
				m_iNavMeshIndex,		14,
				m_iStartOfLinkData,		16,
				m_iReserved,			2
			);
		} m_Struct2;
	};

	union
	{
		u32 m_iAsInteger3;
		struct  
		{
			BITFIELD6(
				u32,
				m_iPedDensity,			3,
				m_iAreaRepresented,		10,
				m_iNodeRadius,			6,
				m_iNumLinks,			4,
				m_bPedSpawningEnabled,	1,
				m_iReserved,			8
			);
		} m_Struct3;
	};

	union
	{
		u32 m_iAsInteger4;
		struct  
		{
			BITFIELD5(
				u32,
				m_iParentNavMeshIndex,	14,
				m_iParentNodeIndex,		15,
				m_bIsOpen,				1,
				m_bIsClosed,			1,
				m_bDebugMarked,			1
			);
		} m_Struct4;
	};

	static const u32 ms_iMaxPedDensity = 7;
	static const u32 ms_iMinAreaRepresented = 64;
	static const u32 ms_iMaxAreaRepresented = 1023;
	static const u32 ms_iMaxNodeRadius = 63;
};

//***************************************************************
//	CHierarchicalNavLink
//	This is a one-way link in the hierarchical node-graph.

//#if !__PS3 || GCC_VERSION < 40101
#pragma pack(push, r1, 1)	// Don't let the Rage resource system try to bump this up to 64 bytes.. 48 is quite fine thanks!
//#endif

#define HIERARCHICAL_LINK_MAX_WIDTH		15.0f

class CHierarchicalNavLink
{
	friend class CNavGen;
	friend class CHierarchicalNavData;
public:

	inline u32 GetNavMeshIndex() const { return m_Struct.m_iNavMeshIndex; }
	inline u32 GetNodeIndex() const { return m_Struct.m_iNodeIndex; }
	inline void SetNavMeshIndex(const u32 i) { m_Struct.m_iNavMeshIndex = i; }
	inline void SetNodeIndex(const u32 i) { m_Struct.m_iNodeIndex = i; }

	inline void SetWidthToLeft(const u32 w) { m_Struct2.m_iWidthToLeft = w; }
	inline void SetWidthToRight(const u32 w) { m_Struct2.m_iWidthToRight = w; }
	inline u32 GetWidthToLeft() const { return m_Struct2.m_iWidthToLeft; }
	inline u32 GetWidthToRight() const { return m_Struct2.m_iWidthToRight; }

	void GetLinkExtents(const Vector3 & vNodePos, const Vector3 & vLinkedNodePos, Vector3 & vOut_NearLeft, Vector3 & vOut_NearRight, Vector3 & vOut_FarLeft, Vector3 & vOut_FarRight);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct & s);
#endif

private:

	union
	{
		u32 m_iAsInteger1;
		struct
		{
			BITFIELD3(
				u32,
				m_iNavMeshIndex,	14,
				m_iNodeIndex,		15,
				m_iReserved,		3
			);
		} m_Struct;
	};
	union
	{
		u16 m_iAsInteger2;	
		struct
		{
			BITFIELD3(
				u16,
				m_iWidthToLeft,			4,
				m_iWidthToRight,		4,
				m_iReserved,			8
			);
		} m_Struct2;
	};
};

//#if !__PS3 || GCC_VERSION < 40101
#pragma pack(pop, r1)
//#endif

typedef void (*NavNodeIntersectingCB)(CHierarchicalNavNode * pNode, const Vector3 & vNodePos, void * pData);

class CHierarchicalNavData : public pgBase
{
	friend class CNavGen;
public:

	CHierarchicalNavData();

	CHierarchicalNavData(datResource & rsc);
	DECLARE_PLACE(CHierarchicalNavData);

	enum { RORC_VERSION = 2 };


#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct & s);
#endif

	static CHierarchicalNavData * StreamingPlaceFn(s32 index, datResourceMap& map, datResourceInfo& header);

	inline s32 CalcSize()
	{
		s32 iNodesMem = (sizeof(CHierarchicalNavNode) * m_iNumNodes);
		s32 iLinksMem = (sizeof(CHierarchicalNavLink) * m_iNumLinks);
		return sizeof(CHierarchicalNavData) + iNodesMem + iLinksMem;
	}

	static bool Save(const char * pFilename, const CHierarchicalNavData * pHierNav);
	static CHierarchicalNavData * Load(const char * pFilename);
	static CHierarchicalNavData * LoadFromMemory(void * pMemBuffer, int iBufferSize);

	inline u32 GetNumNodes() const { return m_iNumNodes; }
	inline u32 GetNumLinks() const { return m_iNumLinks; }
	inline CHierarchicalNavNode * GetNode(const u32 n) { return &m_pNodes[n]; }
	inline CHierarchicalNavLink * GetLink(const u32 l) { return &m_pLinks[l]; }
	inline const Vector3 & GetMin() const { return m_vMins; }
	inline const Vector3 & GetMax() const { return m_vMaxs; }
	inline const Vector3 & GetSize() const { return m_vSize; }

	// Gets the index of a node within this navdata
	inline u32 GetNodeIndex(const CHierarchicalNavNode * pNode) const
	{
		// NB : beware of the pointer arithmetic here..
		const u32 iIndex = ptrdiff_t_to_int(pNode - m_pNodes);
		Assert(iIndex < m_iNumNodes);
		return iIndex;
	}

	void ForAllNodesInArea(const Vector3 & vMin, const Vector3 & vMax, NavNodeIntersectingCB callBack, void * pData);

	void SwitchPedSpawningInArea(const Vector3 & vMin, const Vector3 & vMax, const bool bSwitchedOn);

private:
	CHierarchicalNavNode * m_pNodes;
	u32 m_iNumNodes;
	Vector3 m_vMins, m_vMaxs, m_vSize;
	CHierarchicalNavLink * m_pLinks;
	u32 m_iNumLinks;
	u32 m_iFlags;
	datPadding<4> m_Padding;
	
	static const u32 ms_iFileVersion;
};

struct THierNavAndNode
{
	inline void Set(const u32 iNavMesh, const u32 iNodeIndex) { m_iNavMeshIndex = iNavMesh; m_iNodeIndex = iNodeIndex; }

	u32 m_iNavMeshIndex;
	u32 m_iNodeIndex;
};

struct THierNodeAndLink
{
	CHierarchicalNavNode * m_pNode;
	CHierarchicalNavLink * m_pLink;
};

#ifndef COMPILE_TOOL_MESH
} //namespace rage
#endif

#endif	// NAVMESHBASE_H
