//
// ai/navmesh/datatypes.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_DATATYPES_H
#define AI_NAVMESH_DATATYPES_H

#include "ai/navmesh/navmesh.h"			// For TDynamicObjectIndex, and maybe other things.
#include "fwgeovis/geovis.h"
#include "fwdebug/debugdraw.h"
#include "fwmaths/angle.h"
#include "math/simplemath.h"
#include "vector/vector3.h"

// TODO: These classes at the GTA level should not be forward declared in
// the framework - add accessors, or move more code down to RAGE.
class CPathServer;
class CPathServerThread;
class CCarDoor;

namespace rage
{

// PURPOSE:	The navigation domains here are different data sets that we can choose to perform
//			navigation operations on. This concept was introduced to support the "heightmeshes"
//			used for aerial navigation.
enum aiNavDomain
{
	kNavDomainRegular,
	kNavDomainHeightMeshes,

	kNumNavDomains
};

class fwEntity;

//  Data type of the handle to a path
typedef u32 TPathHandle;

// Null path handle.  Handle zero will never be returned by the path-server as response to a successful path request.
#define PATH_HANDLE_NULL	TPathHandle(0)

//*****************************************************
//	Return/Error codes for pathfinding operations
//*****************************************************

enum EPathServerErrorCode
{
	PATH_FOUND								=	 0,
	PATH_NO_ERROR							=	 0,
	PATH_NOT_FOUND							=	-1,
	PATH_TIMED_OUT							=	-2,
	PATH_REACHED_MAX_PATH_POLYS				=	-3,
	PATH_INVALID_COORDINATES				=	-4,
	PATH_NAVMESH_NOT_LOADED					=	-5,
	PATH_ENDPOINTS_INSIDE_OBJECTS			=	-6,
	PATH_NO_SURFACE_AT_COORDINATES			=	-7,
	PATH_RAN_OUT_OF_PATH_STACK_SPACE		=	-8,
	PATH_RAN_OUT_VISITED_POLYS_SPACE		=	-9,
	PATH_CANCELLED							=	-10,
	PATH_ERROR_INVALID_HANDLE				=	-11,
	PATH_NO_POINTS							=	-12,
	PATH_STILL_PENDING						=	-13,
	PATH_ABORTED_DUE_TO_ANOTHER_THREAD		=	-14,

	PATH_NAVNODES_NOT_LOADED				=	-15,
	PATH_ENDNODES_NOT_FOUND					=	-16
};


//*******************************************************************************
//	Waypoint actions for each segment of the returned route, indicating if that
//	route segment involves a climb-over/climb-up/drop-down/ladder-climb/etc.
//*******************************************************************************

enum eWaypointActionFlags
{
	WAYPOINT_FLAG_NOTHING_TO_DO			= 0,
	WAYPOINT_FLAG_CLIMB_HIGH			= 0x01,
	WAYPOINT_FLAG_DROPDOWN				= 0x02,
	WAYPOINT_FLAG_CLIMB_LOW				= 0x04,

	WAYPOINT_FLAG_OPEN_DOOR				= 0x08,
	WAYPOINT_FLAG_CLIMB_LADDER			= 0x10,
	WAYPOINT_FLAG_DESCEND_LADDER		= 0x20,
	WAYPOINT_FLAG_CLIMB_OBJECT			= 0x40,

	WAYPOINT_FLAG_ADJUSTED_TARGET_POS	= 0x80,
	WAYPOINT_FLAG_HIERARCHICAL_PATHFIND	= 0x100,

	WAYPOINT_FLAG_IS_INTERIOR			= 0x200,
	WAYPOINT_FLAG_IS_ON_WATER_SURFACE	= 0x400,

	WAYPOINT_FLAG_IS_NARROW_IN			= 0x800,	// Very possible we only need one of these
	WAYPOINT_FLAG_IS_NARROW_OUT			= 0x1000,

};

inline unsigned int /*bool*/ IsSpecialActionClimbOrDrop(const u32 iWaypointActionFlags)
{
	// Note: we intentionally return this as an integer so the compiler is not required
	// to branch to produce a 0/1 result, which in most cases would be unnecessary work.

	return iWaypointActionFlags &
			( WAYPOINT_FLAG_CLIMB_HIGH
			| WAYPOINT_FLAG_DROPDOWN
			| WAYPOINT_FLAG_CLIMB_LOW
			| WAYPOINT_FLAG_CLIMB_LADDER
			| WAYPOINT_FLAG_DESCEND_LADDER
			| WAYPOINT_FLAG_CLIMB_OBJECT );
}

//**********************************************************************************
//	The TNavMeshWaypointFlag structure contains a special action & a heading,
//	associated with a waypoint along a navmesh route.
//**********************************************************************************
struct TNavMeshWaypointFlag
{
	u16 m_iSpecialActionFlags;
	u8 m_iHeading;
	u8 m_iReserved;

	inline void Clear()
	{
		m_iSpecialActionFlags = WAYPOINT_FLAG_NOTHING_TO_DO;
		m_iHeading = 0;
		m_iReserved = 0;
	};

	inline u32 GetSpecialActionFlags() const { return (u32)m_iSpecialActionFlags; }

	inline u32 GetSpecialAction() const
	{
		static const u32 iMask = WAYPOINT_FLAG_CLIMB_HIGH|WAYPOINT_FLAG_DROPDOWN|WAYPOINT_FLAG_CLIMB_LOW|WAYPOINT_FLAG_CLIMB_LADDER|WAYPOINT_FLAG_DESCEND_LADDER|WAYPOINT_FLAG_CLIMB_OBJECT;
		Assertf(CountOnBits(m_iSpecialActionFlags & iMask) <= 1, "More than one waypoint special action type");
		
		return (m_iSpecialActionFlags & iMask);
	}

	inline float GetHeading() const
	{
		float fHeading = (((float)m_iHeading) / 255.0f) * TWO_PI;
		fHeading = fwAngle::LimitRadianAngle(fHeading);
		return fHeading;
	}

	inline bool IsClimb() const { return IsClimb(m_iSpecialActionFlags); }
	inline bool IsDrop() const { return IsDrop(m_iSpecialActionFlags); }
	inline bool IsLadderClimb() const { return IsLadderClimb(m_iSpecialActionFlags); }
	inline bool IsNarrow() const { return IsNarrow(m_iSpecialActionFlags); }
	inline bool IsNarrowIn() const { return IsNarrowIn(m_iSpecialActionFlags); }

	static inline bool IsClimb(const u32 iWptFlag)
	{
		static const u32 iMask = WAYPOINT_FLAG_CLIMB_HIGH | WAYPOINT_FLAG_CLIMB_LOW | WAYPOINT_FLAG_CLIMB_LADDER | WAYPOINT_FLAG_DESCEND_LADDER | WAYPOINT_FLAG_CLIMB_OBJECT;
		return (iWptFlag & iMask) != 0;
	}
	static inline bool IsDrop(const u32 iWptFlag)
	{
		return (iWptFlag & WAYPOINT_FLAG_DROPDOWN) != 0;
	}
	static inline bool IsLadderClimb(const u32 iWptFlag)
	{
		static const u32 iMask = WAYPOINT_FLAG_CLIMB_LADDER | WAYPOINT_FLAG_DESCEND_LADDER;
		return (iWptFlag & iMask) != 0;	
	}
	static inline bool IsNarrow(const u32 iWptFlag)
	{
		static const u32 iMask = WAYPOINT_FLAG_IS_NARROW_IN | WAYPOINT_FLAG_IS_NARROW_OUT;
		return (iWptFlag & iMask) != 0;	
	}
	static inline bool IsNarrowIn(const u32 iWptFlag)
	{
		static const u32 iMask = WAYPOINT_FLAG_IS_NARROW_IN;
		return (iWptFlag & iMask) != 0;	
	}
};

#define MAX_NUM_PATH_POINTS							16

struct TPathResultInfo
{
	inline TPathResultInfo() { Clear(); }
	inline void Clear()
	{
		m_bLastRoutePointIsTarget = true;
		m_bPathStartedOnPavement = false;
		m_bUsedCompletionRadius = false;
		m_bFoundDynamicEntityPathIsOn = false;
		m_bPathIncludesWater = false;
		m_bWentFarAsPossibleTowardsTarget = false;
		m_bStartPositionWasInsideAnObject = false;
		m_iDynObjIndex = DYNAMIC_OBJECT_INDEX_NONE;

		for(int i=0; i<MAX_NUM_PATH_POINTS+1; i++)
		{
			m_iPathSplineDistances[i] = -1;
			m_fPathCtrlPt2TVals[i] = 0.0f;
			m_iApproxFreeSpaceAroundEachWaypoint[i] = 0;
			m_iMinApproxFreeSpaceApproachingEachWaypoint[i] = 0;
		}
	}

	// If this is false, then we may need to issue another path-request after this one in order to complete the route
	u32 m_bLastRoutePointIsTarget			:1;
	// Whether this path started on a pavement poly.  Can be useful to know for wandering behaviour.
	u32 m_bPathStartedOnPavement			:1;
	// Whether the found path went exactly to the endpoint, or if it only terminated within the completion radius.
	u32 m_bUsedCompletionRadius				:1;
	// Whether we found the dynamic navmesh instance which this path is on
	u32 m_bFoundDynamicEntityPathIsOn		:1;
	// Whether this path includes water polygons at all, either partially or completely
	u32 m_bPathIncludesWater				:1;
	// Whether this path traveled underwater at all
//	u32 m_bPathIncludesUnderwater			:1;
	// Target navmesh wasn't loaded, and path request went as far as it could towards it (only if instructed to in the path request)
	u32 m_bWentFarAsPossibleTowardsTarget	:1;
	// Whether the start position was contained in a dynamic object (and moved outside)
	u32 m_bStartPositionWasInsideAnObject	:1;

	TDynamicObjectIndex m_iDynObjIndex;

	//***************************************************************************************************
	// If the path exists on a dynamic navmesh, then we need to know the exact matrix of the entity
	// at the time the path was calculated - so that anyone using the path can backrotate it into local
	// space & then retransform it into worldspace by the entity's current matrix (probably every frame).

	Matrix34 m_MatrixOfEntityAtPathCreationTime;

	// Value which indexes into g_fPathSplineDists[], for smoothing at corners.
	// Or -1 if no spline at that corner.
	int m_iPathSplineDistances[MAX_NUM_PATH_POINTS+1];
	
	// A floating point value t-val which defines how far along the vector vLast->vCurrent the
	// vCtrlPt2 lies.  This is specified in order to prevent overlapping the start of the current
	// corner's spline with the end of the last corner's spline (which causes hard-to-handle
	// situations where the path will double back suddenly).
	float m_fPathCtrlPt2TVals[MAX_NUM_PATH_POINTS+1];

	// An approximate measure of the free space surrounding each waypoint.
	// This is the integer value from (0..MAX_FREE_SPACE_AROUND_POLY_VALUE) inclusive stored in each poly.
	int m_iApproxFreeSpaceAroundEachWaypoint[MAX_NUM_PATH_POINTS+1];

	// An approximate measure of the minimum free space value in all of the polys from [n-1] to [n].
	// Calculated during pathfinding from all the polys we crossed from the last waypoint to the current.
	// This is calculated from the integer value from (0..MAX_FREE_SPACE_AROUND_POLY_VALUE) inclusive stored in each poly.
	int m_iMinApproxFreeSpaceApproachingEachWaypoint[MAX_NUM_PATH_POINTS+1];

	// Returns the closest point which this pathsearch achieved in reaching to the target
	Vector3 m_vClosestPointFoundToTarget;
};

// The default radius which is baked into the navmesh
#define PATHSERVER_PED_RADIUS		0.35f
// The maximum allowable radius of a pathfinding character
#define PATHSERVER_MAX_PED_RADIUS	TAdjPoly::ms_fMaxFreeSpaceAroundVertex


#if __TRACK_PEDS_IN_NAVMESH
//*************************************************************************
//	CNavMeshTrackedObject
//	This class defines an object whose position is tracked in the navmesh
//	system, for example peds.  Call CPathServer::UpdateTrackedObject to
//	track the object and update with the current navmesh index & poly.

class CNavMeshTrackedObject
{
	friend class ::CPathServer;

	struct TNavPolyData
	{
		u16 m_bPavement						:1;
		u16 m_bSheltered					:1;
		u16 m_bIsolated						:1;
		u16 m_bNetworkSpawn					:1;
		u16 m_bIsRoad						:1;
		u16 m_bInterior						:1;
		u16 m_bIsWater						:1;
		u16 m_iAudioProperties				:4;
		u16 m_bTrainTracks					:1;
	};

public:

    static const float DEFAULT_RESCAN_PROBE_LENGTH;

	CNavMeshTrackedObject();
	~CNavMeshTrackedObject();

	bool IsLastNavMeshIntersectionValid() const;
	bool IsUpToDate(const Vector3 & vPosition, const float fEps=ms_fDefaultMaxErrorForPathSearches) const;

	inline const Vector3 & GetLastPosition() const { return m_vLastPosition; }
	inline const TNavMeshAndPoly & GetNavMeshAndPoly() const { return m_NavMeshAndPoly; }
	inline const Vector3 & GetLastNavMeshIntersection() const { return m_vLastNavMeshIntersection; }
	inline const Vector3 & GetPolyNormal() const { return m_vPolyNormal; }

	inline bool GetIsValid() const { return m_NavMeshAndPoly.m_iNavMeshIndex!=NAVMESH_NAVMESH_INDEX_NONE && m_NavMeshAndPoly.m_iPolyIndex!=NAVMESH_POLY_INDEX_NONE; }
	inline bool GetIsUpToDate() const { return m_bUpToDate; }
	inline const Vector3 & GetNavMeshIntersectEdge() const { return m_vNavMeshIntersectEdge; }
	inline bool GetHitNavMeshEdge() const { return m_bHitNavMeshEdge; }
	inline int GetHitEdgeIndex() const { return m_iHitEdgeIndex; }
	inline float GetZPosFromTask() const { return m_fZPosFromTask; }
	inline void SetZPosFromTask(float f) { m_fZPosFromTask = f; }
	inline u32 GetLastRescanTime() const { return m_iLastRescanTime; }
	inline void SetLastRescanTime(const u32 t) { m_iLastRescanTime = t; }
	
	inline void SetPerformAvoidanceLineTests(const bool bState) { m_bPerformAvoidanceLineTests = bState; }
	inline void ResetAvoidanceLineTest() { m_bPerformAvoidanceLineTests = false; }
	inline bool GetWasAvoidanceTestClearLeft() const { return m_bAvoidanceLeftIsBlocked == 0; }
	inline bool GetWasAvoidanceTestClearRight() const { return m_bAvoidanceRightIsBlocked == 0; }

	inline float GetFreeSpaceMetric() const { return m_fFreeSpaceMetric; }
	inline void SetFreeSpaceMetric(const float f) { m_fFreeSpaceMetric = f; }

	inline bool IsOnPavement() const { return GetIsValid() && GetNavPolyData().m_bPavement; }

	struct sNavMeshLosCheck
	{
		void Reset()
		{
			m_iTimeChecked = 0;
		}
		Vector3 m_Vertex1;
		Vector3 m_Vertex2;
		u32 m_iTimeChecked;
	};

	bool QueryLosCheck(Vector3& o_Intersection, Vector3& o_Vertex1, Vector3& o_Vertex2, const Vector3& in_Position, const Vector3& in_TestVec) const;
	void RequestLosCheck(const Vector3& in_Vector);

	typedef rage::atFixedArray<sNavMeshLosCheck, 8> NavMeshLosChecksArray;
	typedef rage::atFixedArray<Vector3, 3> NavMeshLosRequestArray;
	NavMeshLosRequestArray m_NavMeshLosRequests;
	NavMeshLosChecksArray m_NavMeshLosChecks;
	
	inline void SetPerformLosAheadLineTest(const bool bState) { m_bPerformLosAheadLineTest = bState; }
	inline void ResetLookAheadLineTest() { m_bPerformLosAheadLineTest = false; }
	inline bool GetWasLosAheadTestClear() const { return m_bLosAheadIsBlocked == 0; }
#if __BANK
    inline void SetPerformedRescanLastUpdate() { m_bPerformedRescanLastFrame = true; }
    inline void ResetPerformedRescanLastUpdate() { m_bPerformedRescanLastFrame = false; }
    inline bool PerformedRescanLastUpdate() const { return m_bPerformedRescanLastFrame; }
#endif // __BANK

	Vector3 SlideAlongNavMeshEdge(const Vector3 & vLastPos, const Vector3 & vMoveSpeed);

	const TNavPolyData GetNavPolyData() const { Assert(GetIsValid()); return m_NavPolyData; }

	void SetInvalid();
	void Teleport(const Vector3 & vNewPosition);
	void Rescan(const Vector3 & vObjectPos, const float rescanProbeLength = DEFAULT_RESCAN_PROBE_LENGTH);
	
#if __BANK && defined(DEBUG_DRAW)
	static bank_bool ms_bDrawPedTrackingProbes;
#endif

private:

	// The last position of this object
	Vector3 m_vLastPosition;
	// The last location on the navmesh
	Vector3 m_vLastNavMeshIntersection;
	// The edge which has been bumped into whilst tracking in the navmesh
	Vector3 m_vNavMeshIntersectEdge;
	// The normal of the navmesh poly
	Vector3 m_vPolyNormal;

	float m_fZPosFromTask;
	// Estimation of how much space is around the ped (0.0 to 1.0)
	float m_fFreeSpaceMetric;
	u32 m_iLastRescanTime;

	TNavMeshAndPoly m_NavMeshAndPoly;

	s16 m_iHitEdgeIndex;

	u16 m_bTeleported						:1;
	u16 m_bUpToDate							:1;
	u16 m_bHasValidLastPositionOnNavMesh	:1;
	u16 m_bHitNavMeshEdge					:1;
	u16 m_bPerformAvoidanceLineTests		:1;
	u16 m_bPerformLosAheadLineTest			:1;
	u16 m_bAvoidanceLeftIsBlocked			:2;
	u16 m_bAvoidanceRightIsBlocked			:2;
	u16 m_bLosAheadIsBlocked				:2;
#if __BANK
    u16 m_bPerformedRescanLastFrame         :1;
#endif // __BANK

	TNavPolyData m_NavPolyData;

	// If the object's position change exceeds this amount, we will have to search for the poly under
	// the ped - instead of more efficiently sliding the object's position across the connectivity graph
	static const float ms_fMaxTrackingPositionChangeSqr;

	// The maximum difference between the ped's current position & the object's position, which will
	// allow us to start the path using the cached position & navmesh-index/polygon
	static const float ms_fDefaultMaxErrorForPathSearches;

	// Never rescan more frequently than this, or we risk inundating the pathserver
	static const u32 ms_iMaxRescanFreq;

	// Large number which we can safely assume is off the map; used for initializing positions to invalid
	static const float ms_fOffMap;
};

#endif	// __TRACK_PEDS_IN_NAVMESH

//**********************************************************************************
//	TInfluenceSphere
//	This struct defines a sphere of influence, which the pathfinder should avoid or
//	favour when generating paths.  The default behaviour is not to have any of these
//	spheres, but paths may be requested specifying up to MAX_NUM_INFLUENCE_SPHERES.
//	The weighting factor may be positive (in which case the sphere will be avoided),
//	or negative (in which case the sphere will be favoured).
//**********************************************************************************

// The maximum number which may be used in a path request.
#define MAX_NUM_INFLUENCE_SPHERES	8
// A multiplier which can be applied to unit values.  This multiplier will be changed
// so that it works with the path-finding heuristics, which are subject to change.
#define INFLUENCE_SPHERE_MULTIPLIER	100.0f


struct TInfluenceSphere
{
public:

	static float ms_fMaxWeighting;
	static float ms_fMinWeighting;

	inline void Init(const Vector3 & vOrigin, float fRadius, float fInnerInfluence, float fOuterInfluence)
	{
		SetOrigin(vOrigin);
		SetRadius(fRadius);
		SetInnerWeighting(fInnerInfluence);
		SetOuterWeighting(fOuterInfluence);
	}

	inline const Vector3 & GetOrigin() const { return m_vOrigin; }
	inline float GetRadius() const { return m_fRadius; }
	inline float GetRadiusSqr() const { return m_fRadiusSqr; }
	inline float GetOuterWeighting() { return m_fOuterWeighting; }
	inline float GetInnerWeighting() { return m_fInnerWeighting; }

	inline void SetOrigin(const Vector3 & v) { m_vOrigin = v; }
	inline void SetRadius(const float f) { m_fRadius = f; m_fRadiusSqr = m_fRadius*m_fRadius; }
	inline void SetOuterWeighting(const float f)
	{
		m_fOuterWeighting = Clamp(f, ms_fMinWeighting, ms_fMaxWeighting);
	}
	inline void SetInnerWeighting(const float f)
	{
		m_fInnerWeighting = Clamp(f, ms_fMinWeighting, ms_fMaxWeighting);
	}

private:

	Vector3 m_vOrigin;
	float m_fRadius;
	float m_fRadiusSqr;

	// The penalty which is incurred whenever the pathfinder chooses a point which is inside
	// the radius of this sphere.  NB : Instead of a fixed penalty, perhaps there should be a
	// min/max weighting, and a linear / inv dist-sqr falloff ?
	float m_fOuterWeighting;
	float m_fInnerWeighting;
};

//************************************************************************************
//	CWalkRndObjGrid - this class is to enable Gordon's wavefront stuff to work
//	efficiently.  For querying, the central cell is addressed with coordinates (0,0)
//	The (-X,-Y) corner is at (-8,-8) and the (+X,+Y) at (8,8)
//************************************************************************************

#define WALKRNDOBJGRID_MAXSIZE				65
static const int WALKRNDOBJGRID_NUMBYTES	= ((WALKRNDOBJGRID_MAXSIZE*WALKRNDOBJGRID_MAXSIZE)/8)+1;


class CWalkRndObjGrid
{
	friend class ::CPathServer;
	friend class ::CPathServerThread;
public:
	CWalkRndObjGrid(void) { m_iLastUsedTime = 0; }
	~CWalkRndObjGrid(void) { }

	inline void SetAllAsWalkable(void)
	{
		memset(m_Data, 255, ((m_iSize*m_iSize)/8)+1);
	}
	inline void SetAllAsNotWalkable(void)
	{
		memset(m_Data, 0, ((m_iSize*m_iSize)/8)+1);
	}

	inline bool IsWalkable(int x, int y)
	{
		if(x >= -m_iCentreCell && x <= m_iCentreCell && y >= -m_iCentreCell && y <= m_iCentreCell)
		{
			int index = ((y + m_iCentreCell) * m_iSize) + (x + m_iCentreCell);
			return (m_Data[index/8] & (1<<(index%8))) != 0;
		}
		return false;
	}
	inline void SetWalkable(int x, int y)
	{
		if(x >= -m_iCentreCell && x <= m_iCentreCell && y >= -m_iCentreCell && y <= m_iCentreCell)
		{
			int index = ((y + m_iCentreCell) * m_iSize) + (x + m_iCentreCell);
			m_Data[index/8] |= (1<<(index%8));
		}
	}
	inline void SetNotWalkable(int x, int y)
	{
		if(x >= -m_iCentreCell && x <= m_iCentreCell && y >= -m_iCentreCell && y <= m_iCentreCell)
		{
			int index = ((y + m_iCentreCell) * m_iSize) + (x + m_iCentreCell);
			m_Data[index/8] &= ~(1<<(index%8));
		}
	}

private:

	Vector3 m_vOrigin;
	float m_fResolution;

	u16 m_iSize;
	u16 m_iCentreCell;

	// This is only used by CPathServer, which maintains an LRU cache of these grids
	u32 m_iLastUsedTime;

	u8 m_Data[WALKRNDOBJGRID_NUMBYTES];

};	// 550 bytes unpadded

#define WALKRNDOBJGRID_CACHESIZE		1	//32	// JB: NOT CURRENTLY USED


//***********************************************************************
//	TDynamicObject
//	Represents a dynamic object (CObject, CVehicle, etc) in the world.
//	Due to the cost overheads (and also largely due to multi-threading
//	issues) we don't actully use the CWorld class to test for entity LOS
//	intersections.  Instead we use this compact representation.
//	TDynamicObjects will be added/removed/updated from the main game
//	by calls to CPathServer.
//***********************************************************************

#define NUM_DYNAMIC_OBJECT_PLANES				6		// was 4

struct TDynObjBounds
{
protected:
	// The m_vEntityOrigin is used *only* for determining whether to update this bounds,
	// and may be quite different from the m_vOrigin below.
	// Note : W component stores m_fTopZ
	Vector3 m_vEntityOrigin;
	// The m_vOrigin is the actual centre of the bounds for this dynamic object
	// Note : W component stores m_fBottomZ
	Vector3 m_vOrigin;

public:

	Vector4 m_vEdgePlaneNormals[NUM_DYNAMIC_OBJECT_PLANES];

	float m_fHeading;
	float m_fRoll;
	float m_fPitch;
	float m_fUpZ;		// the Z of up component of the entity's matrix (1.0f for something which is upright)

	inline const Vector3 & GetEntityOrigin() const { return m_vEntityOrigin; }
	inline const Vector3 & GetOrigin() const { return m_vOrigin; }
	inline void SetEntityOrigin(const Vector3 & v) { m_vEntityOrigin.x = v.x; m_vEntityOrigin.y = v.y; m_vEntityOrigin.z = v.z; }
	inline void SetOrigin(const Vector3 & v) { m_vOrigin.x = v.x; m_vOrigin.y = v.y; m_vOrigin.z = v.z; }

	inline float GetTopZ() const { return m_vEntityOrigin.w; }
	inline float GetBottomZ() const { return m_vOrigin.w; }
	inline void SetTopZ(const float f) { m_vEntityOrigin.w = f; }
	inline void SetBottomZ(const float f) { m_vOrigin.w = f; }

	//void ExpandVertices(const float fDistance, Vector2 * vCornerVerts);

	// Calculate the segment planes from this bound; an array four Vector3 is expected as input
	// Planes are 2d, plane normal is stored in XY and distance from origin is in Z
	void CalculateSegmentPlanes(Vector3 * pPlane2d, Vector2 * vCornerVerts) const;

};

//************************************************************************************************

class CDynamicObjectsGridCell;


struct TDynamicObject
{
	/*
	enum EFilter
	{
		DOF_Vehicle			= 0x1,
		DOF_Motorbike		= 0x2,
		DOF_Object			= 0x4,
		DOF_VehicleDoor		= 0x8,
		DOF_HasUprootLimit	= 0x10,
		DOF_Openable		= 0x20
	};
	*/
	enum ObjectFilterFlags
	{
		// Not an obstacle; perhaps lying flat on the ground
		FLAG_IGNORE_NOT_OBSTACLE			= 0x01,
		// Openable by pushing or basic interaction
		FLAG_IGNORE_OPENABLE				= 0x02
	};
	enum BlockingObjectFlags
	{
		BLOCKINGOBJECT_WANDERPATH				= 0x1,
		BLOCKINGOBJECT_SHORTESTPATH				= 0x2,
		BLOCKINGOBJECT_FLEEPATH					= 0x4,

		BLOCKINGOBJECT_ALL_PATH_TYPES			= BLOCKINGOBJECT_WANDERPATH | BLOCKINGOBJECT_SHORTESTPATH | BLOCKINGOBJECT_FLEEPATH
	};

	union
	{
		fwEntity * m_pEntity;
		CCarDoor * m_pCarDoor;
		u32 m_iScriptThreadId;
	};

	TDynObjBounds m_Bounds;
	TDynObjBounds m_NewBounds;

	Vector2 m_vVertices[4];

	// The next dynamic object in the straight linked-list of TDynamicObjects
	TDynamicObject * m_pNext;

	// The prev & next dynamic object in the CDynamicObjectsGridCell linked-list
	TDynamicObject * m_pPrevObjInGridCell;
	TDynamicObject * m_pNextObjInGridCell;
	// The grid-cell in which this dynamic object currently resides
	CDynamicObjectsGridCell * m_pOwningGridCell;

	// For early-out culling.
	TShortMinMax m_MinMax;

	// Fills out the 'TShortMinMax' member from the current m_Bounds.  Extends the min/max Z a little.
	inline void CalcMinMaxForObject()
	{
		Vector2 vMin(FLT_MAX,FLT_MAX);
		Vector2 vMax(-FLT_MAX,-FLT_MAX);
		for(s32 v=0; v<4; v++)
		{
			vMin.x = Min(vMin.x, m_vVertices[v].x);
			vMin.y = Min(vMin.y, m_vVertices[v].y);
			vMax.x = Max(vMax.x, m_vVertices[v].x);
			vMax.y = Max(vMax.y, m_vVertices[v].y);
		}

		m_MinMax.SetFloat(
			vMin.x - MINMAX_RESOLUTION,
			vMin.y - MINMAX_RESOLUTION,
			m_Bounds.GetBottomZ() - 1.0f,
			vMax.x + MINMAX_RESOLUTION,
			vMax.y + MINMAX_RESOLUTION,
			m_Bounds.GetTopZ() + 1.0f );
	}
	// Fills out the 'TShortMinMax' member from the current m_Bounds.  Extends the min/max Z a little.
	inline void CalcMinMaxForObject(Vector3 & vOut_Min, Vector3 & vOut_Max)
	{
		Vector2 vMin(FLT_MAX,FLT_MAX);
		Vector2 vMax(-FLT_MAX,-FLT_MAX);
		for(s32 v=0; v<4; v++)
		{
			vMin.x = Min(vMin.x, m_vVertices[v].x);
			vMin.y = Min(vMin.y, m_vVertices[v].y);
			vMax.x = Max(vMax.x, m_vVertices[v].x);
			vMax.y = Max(vMax.y, m_vVertices[v].y);
		}

		vOut_Min.x = vMin.x - MINMAX_RESOLUTION;
		vOut_Min.y = vMin.y - MINMAX_RESOLUTION;
		vOut_Min.z = m_Bounds.GetBottomZ() - 1.0f;
		vOut_Max.x = vMax.x + MINMAX_RESOLUTION;
		vOut_Max.y = vMax.y + MINMAX_RESOLUTION;
		vOut_Max.z = m_Bounds.GetTopZ() + 1.0f;

		m_MinMax.SetFloat(vOut_Min.x, vOut_Min.y, vOut_Min.z, vOut_Max.x, vOut_Max.y, vOut_Max.z);
	}

	void SetFlagsForNewObject();

	inline bool IsObjectSlotAvailable()	
	{
		return (!m_pEntity && !m_bFlaggedForDeletion && IsObjectCustomAdded());
	}
	inline bool IsObjectCustomAdded()
	{
		return (!m_bIsUserAddedBlockingObject && !m_bScriptedBlockingObject);	// Maybe this shhould also check for fire
	}

	// A bitset which defines the type of object; is compared with an input bitset to determine which objects to consdier during path queries.
	//u32 m_iFilterBits;

	// Whether this object is active.  Initially added objects are not activated until the next path request.
	u32 m_bIsActive							:1;
	// Whether this objects is currently an obstacle, given its current orientation etc.  (eg. Doors will not be an obstacle if fallen over & flat on ground)
	u32 m_bIsCurrentlyAnObstacle			:1;
	// Whether this object can be opened (ie. doors, etc.)
	u32 m_bIsOpenable						:1;
	// Whether the entity is an instance of a CDoor (NB: this doesn't imply m_bIsOpenable)
	u32 m_bIsDoor							:1;
	// If this flag is set, then the next time we do Process() we must copy the m_NewBounds over the m_CurrentBounds (ie. lazy update)
	u32 m_bNewBounds						:1;
	// When this flag is set, this objects needs reclassifying wrt its grid cell (and the gridcell needs its extents recalculation)
	u32 m_bNeedsReInsertingIntoGridCells	:1;
	// This object may be involved in an intersection, used by various intersection test functions
	u32 m_bPossibleIntersection				:1;
	// This object might be climbable, or walkable over
	u32 m_bIsClimbable						:1;
	// This object could be pushed out the way by a ped
	u32 m_bIsPushable						:1;
	// This object is a CVehicle
	u32 m_bIsVehicle						:1;
	// This object is a CObject
	u32 m_bIsObject							:1;
	// This is a CFire object
	u32 m_bIsFire							:1;
	// This is a blocking object added by code/script
	u32 m_bIsUserAddedBlockingObject		:1;
	// If the following flag is set as well as 'm_bIsUserAddedBlockingObject', then this is a scripted dynamic object
	u32 m_bScriptedBlockingObject			:1;
	// We might use a special case for motorbikes to ignore them if they're on their side (helps with getting on)
	u32 m_bIsMotorbike						:1;
	// This object is pinned down and cannot be easily pushed by a ped (some fences, etc)
	u32 m_bHasUprootLimit					:1;
	// This object is breakable glass; after it has been first broken, the object is removed (peds can walk through)
	u32 m_bIsBreakableGlass					:1;
	// Whether this object has had its extents (m_MinMax) adjusted to compensate for a variable-width actor pathsearch (just for asserting)
	u32 m_bBoundsAdjustedForEntityWidth		:1;
	// This dynamic object represents a vehicle door
	u32 m_bVehicleDoor						:1;
	// If true this vehicle door is aligned with the object X axis; otherwise it is assumed to align with the -X axis
	u32 m_bVehicleDoorAlignedWithXAxis		:1;
	// This object is moving too fast for us to consider during pathfinding
	u32 m_bInactiveDueToVelocity			:1;
	// Is to big and heavy to be ignored ever
	u32 m_bIsSignificant					:1;
	// 
	u32 m_bForceReducedBoundingBox			:1;
	// 
	u32 m_bActiveForNavigation				:1;
	// When 'm_bIsUserAddedBlockingObject' is set, this defines for which types of path this object is active
	// Taken from the EBlockingObjectFlags enumeration (thus only 3 bits are required)
	u32 m_iBlockingObjectFlags				:3;
	// The dynamic object is outside the world (will be ignored during pathfinding)
	u32 m_bIsOutsideWorld					:1;

	//----------------------------------------------------------------
	// Lockless IPC - the following need to be an atomic 32bit values
	// TODO: add fences either side of using these

	// Whether this objects is ready for deleting.  This will occur at start of next path request.
	bool m_bFlaggedForDeletion;
	// If this flag is set, then the pathserver thread is copying the m_NewBounds to the m_Bounds
	bool m_bCurrentlyCopyingBounds;
	// If this flag is set, then the main game thread is currently updating the m_NewBounds member
	bool m_bCurrentlyUpdatingNewBounds;

};	// 68 bytes unpadded

//----------------------------------------------------------------------------

struct TScriptDeferredAddDynamicObject
{
public:
	u32 m_iThreadId;
	s32 m_iObjectHandle;
	Vector3 m_vPosition;
	Vector3 m_vSize;
	float m_fHeading;
	s32 m_iBlockingFlags;

	static const s32 ms_iMaxNum = 32;
};

#define MAX_NUM_SCRIPTED_DYNAMIC_OBJECTS	64

struct TScriptObjHandlePair
{
public:
	s32 m_iScriptObjectHandle;
	TDynamicObjectIndex m_iObjectIndex;
};
//----------------------------------------------------------------------------

struct TTessInfo
{
	inline TTessInfo(void)
	{
		m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		m_iPolyIndex = NAVMESH_POLY_INDEX_NONE;
		m_bInUse = false;
	}

	u16 m_iNavMeshIndex	: NUM_BITS_FOR_NAVMESH_INDEX;
	u16 m_bInUse						: 1;
	u16 m_bReserved						: 1;

	u16 m_iPolyIndex;
};


//********************************************************************************************

#define MAX_PATH_STACK_SIZE				2048	//1024



class CTestNavMeshLosVars
{
public:
	enum ENavMeshTestLosFlags
	{
		FLAG_NO_LOS_ACROSS_PAVEMENT_BOUNDARY						= 0x01,
		FLAG_NO_LOS_ACROSS_WATER_BOUNDARY							= 0x02,
		FLAG_NO_LOS_ACROSS_TOOSTEEP_BOUNDARY						= 0x04,
		FLAG_NO_LOS_ACROSS_FREESPACE_BOUNDARY						= 0x08,
		FLAG_NO_LOS_ACROSS_MAXSLOPE_BOUNDARY						= 0x10,
		FLAG_NO_LOS_ACROSS_INFLUENCESPHERES_BOUNDARY				= 0x20,
		FLAG_NO_LOS_ACROSS_ACROSS_INTERIOR_EXTERIOR_BOUNDARY		= 0x40,
		FLAG_NO_LOS_ACROSS_ACROSS_SHELTERED_BOUNDARY				= 0x80,
		FLAG_RECORD_MINIMUM_FREESPACE_VALUE_ENCOUNTERED				= 0x100,
		FLAG_USE_ENTITY_RADIUS										= 0x200
	};

public:
	CTestNavMeshLosVars() { }
	~CTestNavMeshLosVars() { }

	void Init(const Vector3 & vStartPos, const Vector3 & vEndPos, const u32 iLosFlags)
	{
		m_vStartPos = vStartPos;
		m_vEndPos = vEndPos;
		m_vLosVec = vEndPos - vStartPos;
		m_iLosFlags = iLosFlags;
		m_iMinFreeSpaceValue = MAX_FREE_SPACE_AROUND_POLY_VALUE;
	}
	void Init(const Vector3 & vStartPos, const Vector3 & vEndPos, const TNavMeshPoly * pToPoly, const u32 iLosFlags)
	{
		m_vStartPos = vStartPos;
		m_vEndPos = vEndPos;
		m_vLosVec = vEndPos - vStartPos;
		m_iLosFlags = iLosFlags;
		m_pToPoly = pToPoly;
		m_iMinFreeSpaceValue = MAX_FREE_SPACE_AROUND_POLY_VALUE;
	}
	void Init(const Vector3 & vStartPos, const Vector2 & vLosDir, const u32 iLosFlags)
	{
		m_vStartPos = vStartPos;
		m_vEndPos = Vector3(vStartPos.x + vLosDir.x, vStartPos.y + vLosDir.y, vStartPos.z);
		m_vLosVec = m_vEndPos - vStartPos;
		m_vAboveEndPos = m_vEndPos;
		m_vAboveEndPos.z = 4000.0f;
		m_vBelowEndPos = m_vEndPos;
		m_vBelowEndPos.z = -4000.0f;

		m_EndPosMinMax.SetFloat(m_vEndPos.x - MINMAX_RESOLUTION, m_vEndPos.y - MINMAX_RESOLUTION, m_vBelowEndPos.z, m_vEndPos.x + MINMAX_RESOLUTION, m_vEndPos.y + MINMAX_RESOLUTION, m_vAboveEndPos.z);

		m_iLosFlags = iLosFlags;
		m_iMinFreeSpaceValue = MAX_FREE_SPACE_AROUND_POLY_VALUE;
		m_pToPoly = NULL;	
	}
	//private:

	TTestLosStack m_TestLosStack[SIZE_TEST_LOS_STACK];

	int m_iTestLosStackNumEntries;
	u32 m_iMinFreeSpaceValue;
	u32 m_iLosFlags;
	float m_fEntityRadius;
	Vector3 m_vStartPos;
	Vector3 m_vEndPos;
	Vector3 m_vLosVec;
	Vector3 m_vAboveEndPos;
	Vector3 m_vBelowEndPos;
	Vector3 m_vIntersectionPosition;
	const TNavMeshPoly * m_pToPoly;
	TNavMeshPoly * m_pPolyEndedUpon;
	TShortMinMax m_EndPosMinMax;
};


struct TNavProcessClearAreaEntry
{
	inline TNavProcessClearAreaEntry() { }
	inline TNavProcessClearAreaEntry(TNavMeshPoly * pPoly, const float fDistSqr) : m_pPoly(pPoly), m_fDistSqr(fDistSqr) { }
	TNavMeshPoly * m_pPoly;
	float m_fDistSqr;
};

// CLASS : CSpatialHashTable
// PURPOSE :
// See "Optimized Spatial Hashing for Collision Detection of Deformable Objects" for a description of this spatial hashing algorithm
template <typename T> class CSpatialHashTable
{
public:
	CSpatialHashTable(const s32 iGridSize, const s32 iHashTableSize, const s32 iMaxNumItems)
	{
		m_iGridSize = iGridSize;
		m_iHashTableSize = iHashTableSize;
		m_iMaxNumItems = iMaxNumItems;

		m_pItemsPool = rage_new TEntry[m_iMaxNumItems];
		m_aHashTable = rage_new TEntry*[m_iHashTableSize];

		Reset();
	}
	~CSpatialHashTable()
	{
		Shutdown();
	}
	inline void Reset()
	{
		m_iCurrentNumItems = 0;

		s32 i;
		for(i=0; i<m_iMaxNumItems-1; i++)
		{
			m_pItemsPool[i].m_pNext = &m_pItemsPool[i+1];
		}
		m_pItemsPool[i-1].m_pNext = NULL;

		for(i=0; i<m_iHashTableSize; i++)
		{
			m_aHashTable[i] = NULL;
		}
		m_pFirstFreeItem = &m_pItemsPool[0];
	}
	inline void Shutdown()
	{
		if(m_pItemsPool)
			delete[] m_pItemsPool;
		if(m_aHashTable)
			delete[] m_aHashTable;
		m_pItemsPool = NULL;
		m_aHashTable = NULL;
	}

	// NAME : CalcHash
	// PURPOSE : Calculate a hash value for XYZ tuple, already scaled by grid size
	static inline u32 CalcHash(const s32 iX, const s32 iY, const s32 iZ, const u32 iHashTableSize)
	{
		static const u32 p1 = 73856093;
		static const u32 p2 = 19349663;
		static const u32 p3 = 83492791;

		const u32 h = (iX * p1) ^ (iY * p2) ^ (iZ * p3);
		return h % iHashTableSize;
	}

	// NAME : CalcHash
	// PURPOSE : Calculate a hash value for XYZ coordinates
	static inline u32 CalcHash(const float fX, const float fY, const float fZ, const float fGridSize, const u32 iHashTableSize)
	{
		const s32 iX = (s32) (fX / fGridSize);
		const s32 iY = (s32) (fY / fGridSize);
		const s32 iZ = (s32) (fZ / fGridSize);

		return CalcHash(iX, iY, iZ, iHashTableSize);
	}

	// NAME : CalcHash
	// PURPOSE : Calculate a hash value for Vector3
	/*
	static inline u32 CalcHash(const Vector3 & vPos, const float fGridSize, const u32 iHashTableSize)
	{
		const s32 iX = (s32) (vPos.x / fGridSize);
		const s32 iY = (s32) (vPos.y / fGridSize);
		const s32 iZ = (s32) (vPos.z / fGridSize);

		return CalcHash(iX, iY, iZ, iHashTableSize);
	}
	*/
	// NAME : Add
	// PURPOSE : Add an item
	inline void Add(T * pItem, const Vector3 & vPos)
	{
		Assert(m_iCurrentNumItems < m_iMaxNumItems);
		Assertf(m_pFirstFreeItem, "Item will be absent from hash table");

		if(m_pFirstFreeItem)
		{
			const u32 iHash = CalcHash(vPos, (float)m_iGridSize, m_iHashTableSize);

			TEntry * pEntry = m_pFirstFreeItem;
			m_pFirstFreeItem = m_pFirstFreeItem->m_pNext;
			m_iCurrentNumItems++;

			pEntry->m_pItem = pItem;
			pEntry->m_pNext = m_aHashTable[iHash];
			m_aHashTable[iHash] = pEntry;
		}
	}

	// NAME : AddSmall
	// PURPOSE : Add an item which is known to be completely within the extents of a single grid cell
	inline void AddSmall(T * pItem, const TShortMinMax & iMinMax)
	{
		const s32 iHash = CalcHash( MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinX)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinY)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinZ)/m_iGridSize, m_iHashTableSize );

		TEntry * pEntry = m_pFirstFreeItem;
		m_pFirstFreeItem = m_pFirstFreeItem->m_pNext;
		m_iCurrentNumItems++;

		pEntry->m_pItem = pItem;
		pEntry->m_pNext = m_aHashTable[iHash];
		m_aHashTable[iHash] = pEntry;
	}
	// NAME : AddMedium
	// PURPOSE : Add an item which is less than m_iGridSize in all dimensions, so that only its corners need to be tested for unique hashes
	inline void AddMedium(T * pItem, const TShortMinMax & iMinMax)
	{
		const s32 iMin[3] = { MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinX)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinY)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinZ)/m_iGridSize };
		const s32 iMax[3] = { MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxX)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxY)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxZ)/m_iGridSize };

		// If all corners are within the same grid cell, then we can use AddSmall
		if(iMin[0] == iMin[1] == iMin[2] == iMax[0] == iMax[1] == iMax[2])
		{
			AddSmall(pItem, iMinMax);
			return;
		}

		const s32 iCornerHashes[8] =
		{
			CalcHash(iMin[0], iMin[1], iMin[2], m_iHashTableSize),
			CalcHash(iMax[0], iMin[1], iMin[2], m_iHashTableSize),
			CalcHash(iMax[0], iMax[1], iMin[2], m_iHashTableSize),
			CalcHash(iMin[0], iMax[1], iMin[2], m_iHashTableSize),
			CalcHash(iMin[0], iMin[1], iMax[2], m_iHashTableSize),
			CalcHash(iMax[0], iMin[1], iMax[2], m_iHashTableSize),
			CalcHash(iMax[0], iMax[1], iMax[2], m_iHashTableSize),
			CalcHash(iMin[0], iMax[1], iMax[2], m_iHashTableSize)
		};
		s32 iUniqueHashes[8];
		s32 iNumUnique = 0;
		s32 c, u;

		for(c=0; c<8; c++)
		{
			const s32 h = iCornerHashes[c];
			for(u=0; u<iNumUnique; u++)
				if(iUniqueHashes[u] == h)
					break;
			if(u == iNumUnique)
				iUniqueHashes[iNumUnique++] = h;
		}

		for(u=0; u<iNumUnique; u++)
		{
			const s32 iHash = iUniqueHashes[u];

			if(!m_pFirstFreeItem)
			{
				Assertf(false, "Ran out of entries - item will be absent from hash table");
				return;
			}

			TEntry * pEntry = m_pFirstFreeItem;
			m_pFirstFreeItem = m_pFirstFreeItem->m_pNext;
			m_iCurrentNumItems++;

			pEntry->m_pItem = pItem;
			pEntry->m_pNext = m_aHashTable[iHash];
			m_aHashTable[iHash] = pEntry;
		}
	}
	// NAME : AddLarge
	// PURPOSE : Add an item which is larger than m_iGridSize and therefore may intersect multiple hashes
	// This may still resolve to calling AddMedium, if certain conditions are met (ie. minmax corners being wholly contained within 2 grids along each axis)
	inline void AddLarge(T * pItem, const TShortMinMax & iMinMax)
	{
		#define NUM_UNIQUE_HASHES	128

		s32 x,y,z,u;
		s32 iUniqueHashes[NUM_UNIQUE_HASHES];
		s32 iNumUnique=0;

		const s32 iMin[3] = { (MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinX)/m_iGridSize), (MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinY)/m_iGridSize), (MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinZ)/m_iGridSize) };
		const s32 iMax[3] = { (MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxX)/m_iGridSize)+1, (MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxY)/m_iGridSize)+1, (MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxZ)/m_iGridSize)+1 };

		for(z=iMin[2]; z<iMax[2]; z++)
		{
			for(y=iMin[1]; y<iMax[1]; y++)
			{
				for(x=iMin[0]; x<iMax[0]; x++)
				{
					const s32 iHash = CalcHash(x, y, z, m_iHashTableSize);

					for(u=0; u<iNumUnique; u++)
					{
						if(iUniqueHashes[u] == iHash)
							break;
					}
					if(u == iNumUnique)
					{
						iUniqueHashes[iNumUnique++] = iHash;
						Assert(iNumUnique < NUM_UNIQUE_HASHES);

						if(!m_pFirstFreeItem)
						{
							Assertf(false, "Ran out of entries - item will be absent from hash table");
							return;
						}

						TEntry * pEntry = m_pFirstFreeItem;
						m_pFirstFreeItem = m_pFirstFreeItem->m_pNext;
						m_iCurrentNumItems++;

						pEntry->m_pItem = pItem;
						pEntry->m_pNext = m_aHashTable[iHash];
						m_aHashTable[iHash] = pEntry;
					}
				}
			}
		}
	}

	// NAME : Add
	// PURPOSE : Add an item
	inline void Add(T * pItem, const TShortMinMax & iMinMax)
	{
		const s32 iGridSizeFixedPt = MINMAX_INT_TO_FIXEDPT(m_iGridSize);
		const s32 iSize[3] =
		{
			iMinMax.m_iMaxX - iMinMax.m_iMinX,
			iMinMax.m_iMaxY - iMinMax.m_iMinY,
			iMinMax.m_iMaxZ - iMinMax.m_iMinZ
		};

		if( iSize[0] < iGridSizeFixedPt && iSize[1] < iGridSizeFixedPt && iSize[2] < iGridSizeFixedPt )
		{
			AddMedium(pItem, iMinMax);
		}
		else
		{
			AddLarge(pItem, iMinMax);
		}
	}

	// NAME : Add
	// PURPOSE : Add an item potentially spanning a range of hashes
	inline void AddRegion(T * pItem, const TShortMinMax & iMinMax)
	{

	}

	// NAME : GetItemsAtPosition
	// PURPOSE : Return array of items intersected by the given position
	inline s32 GetItemsAtPosition(const Vector3 & vPos, T ** ppReturnedItems, const s32 iMaxNumItems)
	{
		const u32 iHash = CalcHash(vPos, (float)m_iGridSize, m_iHashTableSize);
		s32 iCount = 0;
		TEntry * pEntry = m_aHashTable[iHash];
		while(pEntry && iCount < iMaxNumItems)
		{
			ppReturnedItems[iCount++] = pEntry->m_pItem;
			pEntry = pEntry->m_pNext;
		}
		return iCount;
	}

	// NAME : GetItemsIntersectingRegion
	// PURPOSE : Return array of items intersecting the given region
	/*
	inline s32 GetItemsIntersectingRegion(const Vector3 & vMin, const Vector3 & vMax, T ** ppReturnedItems, const s32 iMaxNumItems)
	{
		const float fGridSize = (float)m_iGridSize;
		const s32 iMin[3] = { (s32)(vMin.x / fGridSize), (s32)(vMin.y / fGridSize), (s32)(vMin.z / fGridSize) };
		const s32 iMax[3] = { (s32)(vMax.x / fGridSize), (s32)(vMax.y / fGridSize), (s32)(vMax.z / fGridSize) };

		return GetItemsIntersectingRegion(iMin, iMax, ppReturnedItems, iMaxNumItems);
	}
	*/

	// NAME : GetItemsIntersectingRegion
	// PURPOSE : Return array of items intersecting the given region
	inline s32 GetItemsIntersectingRegion(const TShortMinMax & iMinMax, T ** ppReturnedItems, const s32 iMaxNumItems)
	{
		const s32 iMin[3] = { MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinX)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinY)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMinZ)/m_iGridSize };
		const s32 iMax[3] = { MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxX)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxY)/m_iGridSize, MINMAX_FIXEDPT_TO_INT(iMinMax.m_iMaxZ)/m_iGridSize };

		return GetItemsIntersectingRegion(iMin, iMax, ppReturnedItems, iMaxNumItems);
	}

	// NAME : GetItemsIntersectingRegion
	// PURPOSE : Return array of items intersecting the given region
	inline s32 GetItemsIntersectingRegion(const s32 iMin[3], const s32 iMax[3], T ** ppReturnedItems, const s32 iMaxNumItems)
	{
		const s32 iCornerHashes[8] =
		{
			CalcHash(iMin[0], iMin[1], iMin[2], m_iHashTableSize),
			CalcHash(iMax[0], iMin[1], iMin[2], m_iHashTableSize),
			CalcHash(iMax[0], iMax[1], iMin[2], m_iHashTableSize),
			CalcHash(iMin[0], iMax[1], iMin[2], m_iHashTableSize),
			CalcHash(iMin[0], iMin[1], iMax[2], m_iHashTableSize),
			CalcHash(iMax[0], iMin[1], iMax[2], m_iHashTableSize),
			CalcHash(iMax[0], iMax[1], iMax[2], m_iHashTableSize),
			CalcHash(iMin[0], iMax[1], iMax[2], m_iHashTableSize)
		};
		s32 iUniqueHashes[8];
		s32 iNumUnique = 0;
		s32 iCount = 0;
		s32 c, u;

		for(c=0; c<8; c++)
		{
			const s32 h = iCornerHashes[c];
			for(u=0; u<iNumUnique; u++)
				if(iUniqueHashes[u] == h)
					break;
			if(u == iNumUnique)
				iUniqueHashes[iNumUnique++] = h;
		}

		for(u=0; u<iNumUnique; u++)
		{
			const s32 h = iUniqueHashes[u];
			TEntry * pEntry = m_aHashTable[h];	
			while(pEntry && iCount < iMaxNumItems)
			{
				ppReturnedItems[iCount++] = pEntry->m_pItem;
				pEntry = pEntry->m_pNext;
			}
		}

		return iCount;
	}

protected:

	struct TEntry
	{
		T * m_pItem;
		TEntry * m_pNext;
	};

	s32 m_iGridSize;
	s32 m_iHashTableSize;
	s32 m_iMaxNumItems;	
	s32 m_iCurrentNumItems;

	TEntry ** m_aHashTable;

	TEntry * m_pFirstFreeItem;
	TEntry * m_pItemsPool;
};

//******************************************************************************
//	TColPolyData - used when exporting collision data for navmesh creation.
//	Stores some basic info common to all surfaces in the world.  This data is
//	exported with the collision triangles so that we don't need to use the
//	material-manager during the navmesh construction.
//******************************************************************************

// Flags which are written at the start of the ".tri" files exported by the game
#define NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH			0x01

// Some defines for where elements are found within the ".tri" file
#define NAVMESH_EXPORT_TRIFILE_OFFSET_OF_FLAGS			8
#define NAVMESH_EXPORT_TRIFILE_OFFSET_OF_NUMVERTICES	12

class fwNavTriData
{
public:
	fwNavTriData();

#if HEIGHTMAP_GENERATOR_TOOL
	u32			m_iArchetypeFlags;
	u8			m_iPolyDensity;
	u8			m_iPad;
	u8			m_iPrimDensity_MOVER;
	u8			m_iPrimDensity_WEAPON;
	u32			m_iRegionHash;
	u32			m_iBoundHash;
	u64			m_iMaterialId; // phMaterialMgr::Id
	u8			m_iMaterialVfxGroupId; // VfxGroup_e
	u8			m_iMaterialMaskIndex; // index into mask list, or 0xff
	u8			m_iMaterialProcIndex; // procedural tag
	const char*	m_pBoundName;

	union
	{
		u32 m_iFlags;
		struct
		{
			BITFIELD14(
				u32,
				m_iBoundType,				5,
				m_bIsOwnedByStaticBounds,	1,
				m_bIsScriptManaged,			1,
				m_bIsBoundCompositeChild,	1,
				m_iBvhPrimitiveType,		4,
				m_bIsOccluder,				1,
				m_bIsBoxOccluder,			1,
				m_iOccluderFlags,			3,
				m_bIsDistantLODLight,		1,
				m_bIsProp,					1,
				m_bIsProxy,					1,
				m_bContainsMoverBounds,		1,
				m_bContainsWeaponBounds,	1,
				m_bContainsVehicleBounds,	1
			);
		} m_iFlagsStruct;
	};

	/*
	bound types:
		phBound::BOX
		phBound::CYLINDER
		phBound::SPHERE // not yet supported
		phBound::CAPSULE // not yet supported
		phBound::GEOMETRY
		phBound::BVH
			PRIM_TYPE_BOX
			PRIM_TYPE_CYLINDER
			PRIM_TYPE_SPHERE // not yet supported
			PRIM_TYPE_CAPSULE
			PRIM_TYPE_POLYGON
		phBound::COMPOSITE
	*/
	int GetPrimitiveType() const;

	bool IsBox     () const;
	bool IsCylinder() const;
	bool IsCapsule () const;
	bool IsSphere  () const;
#else
	u16 m_unused;
#endif
};

struct TColPolyData
{
	friend class fwExportCollisionBaseTool;

public:

	static const char m_TriFileHeader[8];

public:

	inline TColPolyData()
	{
		m_iBitField1 = 0;
		m_iCollisionTypeFlags = 0;
		m_iPedDensity = 0;
	}

	enum CollisionTypeFlags
	{
		COLLISION_TYPE_MOVER			= BIT(0),
		COLLISION_TYPE_WEAPON			= BIT(1),
		COLLISION_TYPE_RIVER			= BIT(2),
		COLLISION_TYPE_COVER			= BIT(3),
		COLLISION_TYPE_STAIRSLOPE		= BIT(4),
#if HEIGHTMAP_GENERATOR_TOOL
		COLLISION_TYPE_VEHICLE			= BIT(5),
		COLLISION_TYPE_UNUSED_6			= BIT(6),
		COLLISION_TYPE_DEFAULT			= BIT(7),
#endif
	};

	inline bool GetIsStairs() const { return m_Struct1.m_bIsStairs; }
	inline bool GetIsPavement() const { return m_Struct1.m_bIsPavement; }
	inline bool GetIsSeeThrough() const { return m_Struct1.m_bIsSeeThrough; }
	inline bool GetIsShootThrough() const { return m_Struct1.m_bIsShootThrough; }
	inline bool GetIsWalkable() const { return m_Struct1.m_bIsWalkable; }
	inline bool GetIsFixedObject() const { return m_Struct1.m_bThisIsAFixedObject; }
	inline bool GetDontCreateNavMeshOn() const { return m_Struct1.m_bDontCreateNavMeshOn; }
	inline bool GetIsInterior() const { return m_Struct1.m_bIsInterior; }
	inline bool GetDoesntProvideCover() const { return m_Struct1.m_bDoesntProvideCover; }
	inline bool GetIsRoad() const { return m_Struct1.m_bIsRoad; }
	inline bool GetIsTrainTracks() const { return m_Struct1.m_bIsTrainTracks; }
	inline bool GetIsNoNetworkSpawn() const { return m_Struct1.m_bNoNetworkSpawn; }
	inline bool GetIsClimbableObject() const { return m_Struct1.m_bClimbableObject; }
	inline bool GetIsExteriorPortal() const { return m_Struct1.m_bIsExteriorPortal; }
	inline bool GetIsNotClimbableCollision() const { return m_Struct1.m_bNotClimbableCollision; }

	inline void SetIsStairs(bool b) { m_Struct1.m_bIsStairs = b; }
	inline void SetIsPavement(bool b) { m_Struct1.m_bIsPavement = b; }
	inline void SetIsSeeThrough(bool b) { m_Struct1.m_bIsSeeThrough = b; }
	inline void SetIsShootThrough(bool b) { m_Struct1.m_bIsShootThrough = b; }
	inline void SetIsWalkable(bool b) { m_Struct1.m_bIsWalkable = b; }
	inline void SetIsFixedObject(bool b) { m_Struct1.m_bThisIsAFixedObject = b; }
	inline void SetDontCreateNavMeshOn(bool b) { m_Struct1.m_bDontCreateNavMeshOn = b; }
	inline void SetIsInterior(bool b) { m_Struct1.m_bIsInterior = b; }
	inline void SetDoesntProvideCover(bool b) { m_Struct1.m_bDoesntProvideCover = b; }
	inline void SetIsRoad(bool b) { m_Struct1.m_bIsRoad = b; }
	inline void SetIsTrainTracks(bool b) { m_Struct1.m_bIsTrainTracks = b; }
	inline void SetIsNoNetworkSpawn(bool b) { m_Struct1.m_bNoNetworkSpawn = b; }
	inline void SetIsClimbableObject(bool b) { m_Struct1.m_bClimbableObject = b; }
	inline void SetIsExteriorPortal(bool b) { m_Struct1.m_bIsExteriorPortal = b; }
	inline void SetIsNotClimbableCollision(bool b) { m_Struct1.m_bNotClimbableCollision = b; }

	inline u32 GetCollisionTypeFlags() const { return m_iCollisionTypeFlags; }
	inline bool TestCollisionTypeFlags(const u32 f) const { return ((m_iCollisionTypeFlags & f)!=0); }

	inline bool GetIsRiverBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_RIVER)!=0); }
	inline bool GetIsMoverBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_MOVER)!=0); }
	inline bool GetIsWeaponBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_WEAPON)!=0); }
	inline bool GetIsCoverBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_COVER)!=0); }
	inline bool GetIsStairSlopeBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_STAIRSLOPE)!=0); }
#if HEIGHTMAP_GENERATOR_TOOL
	inline bool GetIsVehicleBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_VEHICLE)!=0); }
	inline bool GetIsDefaultBound() const { return ((m_iCollisionTypeFlags & COLLISION_TYPE_DEFAULT)!=0); }
#endif

	inline void SetIsRiverBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_RIVER; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_RIVER; }
	inline void SetIsMoverBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_MOVER; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_MOVER; }
	inline void SetIsWeaponBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_WEAPON; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_WEAPON; }
	inline void SetIsCoverBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_COVER; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_COVER; }
	inline void SetIsStairSlopeBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_STAIRSLOPE; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_STAIRSLOPE; }
#if HEIGHTMAP_GENERATOR_TOOL
	inline void SetIsVehicleBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_VEHICLE; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_VEHICLE; }
	inline void SetIsDefaultBound(bool b) { if(b) m_iCollisionTypeFlags |= COLLISION_TYPE_DEFAULT; else m_iCollisionTypeFlags &= ~COLLISION_TYPE_DEFAULT; }
#endif

	inline s32 GetPedDensity() const { return (s32)m_iPedDensity; }
	inline void SetPedDensity(s32 f) { m_iPedDensity = (u8) f; }

#if HEIGHTMAP_GENERATOR_TOOL
	inline u8 GetPolyDensity() const { return m_triData.m_iPolyDensity; }
	inline u8 GetPrimDensity_MOVER() const { return GetIsMoverBound() ? m_triData.m_iPrimDensity_MOVER : 0; }
	inline u8 GetPrimDensity_WEAPON() const { return GetIsWeaponBound() ? m_triData.m_iPrimDensity_WEAPON : 0; }
#endif

	void Read(fiStream* pStream);
	void Write(fiStream* pStream) const;

	union
	{
		u16 m_iBitField1;
		struct
		{
			BITFIELD15(
				u16,
				m_bIsStairs,				1,
				m_bIsPavement,				1,
				m_bIsSeeThrough,			1,
				m_bIsShootThrough,			1,
				m_bIsWalkable,				1,
				m_bThisIsAFixedObject,		1,
				m_bDontCreateNavMeshOn,		1,
				m_bIsInterior,				1,
				m_bDoesntProvideCover,		1,
				m_bIsRoad,					1,
				m_bNoNetworkSpawn,			1,
				m_bClimbableObject,			1,
				m_bIsExteriorPortal,		1,
				m_bNotClimbableCollision,	1,
				m_bIsTrainTracks,			1
			);
		} m_Struct1;
	};

	u8 m_iCollisionTypeFlags;
	u8 m_iPedDensity;

	fwNavTriData m_triData;
};

}	// namespace rage

#endif	// AI_NAVMESH_DATATYPES_H

// End of file ai/navmesh/datatypes.h
