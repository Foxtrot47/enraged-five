//
// ai/navmesh/priqueue.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "priqueue.h"
#include "navmeshoptimisations.h"

NAVMESH_OPTIMISATIONS()

using namespace rage;

//-----------------------------------------------------------------------------

void CPathServerBinHeap::Insert(float key, float fDistanceTravelled, void * pItem, const Vector3 & vPointInPoly, const Vector3 & vDirFromPrevious, u32 iFlags, u32 iReachedByConnectionPoly, Node** node)
{
#ifdef GTA_ENGINE
	DEV_ONLY( Assert(rage::FPIsFinite(key)); )
#endif

	if(Size<MaxSize)
	{
		Size++;
		Assert(Size<=MaxSize);
		Nodes[Size].iFlags						= (u16)iFlags;
		Nodes[Size].iReachedByConnectionPoly	= (u16)iReachedByConnectionPoly;
		Nodes[Size].vPointInPoly[0]				= vPointInPoly.x;
		Nodes[Size].vPointInPoly[1]				= vPointInPoly.y;
		Nodes[Size].vPointInPoly[2]				= vPointInPoly.z;
		Nodes[Size].vDirFromPrevious[0]			= vDirFromPrevious.x;
		Nodes[Size].vDirFromPrevious[1]			= vDirFromPrevious.y;
		Nodes[Size].vDirFromPrevious[2]			= vDirFromPrevious.z;
		Nodes[Size].Key							= key;
		Nodes[Size].fDistanceTravelled			= fDistanceTravelled;
		Nodes[Size].item.pItem					= pItem;
		Nodes[Size].Handle						= node ? node : &NodeDump;

		*(Nodes[Size].Handle)	=&(Nodes[Size]);
		unsigned int scan		=HeapifyUp(Size);
		*(Nodes[scan].Handle)	=&Nodes[scan];

#if PSVR__QUALITY_ASSURANCE
#if PSVR__CHECKSUM
		CheckSum+=key;
#endif
		VerifyHeap();
#endif
	}
}

void CPathServerBinHeap::DecreaseKey(Node * node, const float newKey) 
{
#ifdef GTA_ENGINE
	DEV_ONLY( Assert(rage::FPIsFinite(newKey)); )
#endif

	Assertf(!(node->Key < newKey), "node->Key = %.4f newKey = %.4f", node->Key, newKey);

#if PSVR__QUALITY_ASSURANCE && PSVR__CHECKSUM
	CheckSum += (newKey - node->Key);
#endif
	node->Key = newKey;
	unsigned int index = ptrdiff_t_to_int(node-Nodes);
	HeapifyUp(index);
#if PSVR__QUALITY_ASSURANCE
	VerifyHeap();
#endif
}

//-----------------------------------------------------------------------------

//*********************************************************
//	This implementation of CPathSearchPriorityQueue uses
//	a binary heap to speed up access.
//*********************************************************

CPathSearchPriorityQueue::CPathSearchPriorityQueue(s32 iMaxSize)	// MAX_PATH_STACK_SIZE
{
	m_iMaxSize = iMaxSize;
	m_pBinHeap = rage_new CPathServerBinHeap(m_iMaxSize);
}

CPathSearchPriorityQueue::~CPathSearchPriorityQueue(void)
{
	if(m_pBinHeap)
	{
		delete m_pBinHeap;
		m_pBinHeap = NULL;
	}
}

void
CPathSearchPriorityQueue::Clear(void)
{
	m_pBinHeap->Empty();
}

bool CPathSearchPriorityQueue::Insert(const float fCost, const float fDistanceTravelled, TNavMeshPoly * pPoly, const Vector3 & vPointInPoly, const Vector3 & vDirFromPrevious, const u32 iFlags, const u32 iReachedByConnectionPoly)
{
	if(m_pBinHeap->Size <= m_pBinHeap->MaxSize)
	{
		m_pBinHeap->Insert(fCost, fDistanceTravelled, pPoly, vPointInPoly, vDirFromPrevious, iFlags, iReachedByConnectionPoly);
		return true;
	}
	else
	{
		return false;
	}
}
bool CPathSearchPriorityQueue::Insert(const float fCost, const float fDistanceTravelled, CHierarchicalNavNode * pNode, const Vector3 & vPointInPoly, const Vector3 & vDirFromPrevious, const u32 iFlags)
{
	if(m_pBinHeap->Size <= m_pBinHeap->MaxSize)
	{
		m_pBinHeap->Insert(fCost, fDistanceTravelled, pNode, vPointInPoly, vDirFromPrevious, iFlags|PATHSERVERBINHEAP_ENTRY_IS_HIERARCHICAL_NODE, 0);
		return true;
	}
	else
	{
		return false;
	}
}

bool CPathSearchPriorityQueue::RemoveTop(float & fCost, float & fDistanceTravelled, TNavMeshPoly *& pPoly, Vector3 & vPointInPoly, Vector3 & vDirFromPrevious, u32 & iFlags, u32 & iReachedByConnectionPoly)
{
	return m_pBinHeap->ExtractMin(fCost, fDistanceTravelled, (void*&)pPoly, vPointInPoly, vDirFromPrevious, iFlags, iReachedByConnectionPoly);
}
bool CPathSearchPriorityQueue::RemoveTop(float & fCost, float & fDistanceTravelled, CHierarchicalNavNode *& pNode, Vector3 & vPointInPoly, Vector3 & vDirFromPrevious, u32 & iFlags)
{
	u32 iTmp;
	return m_pBinHeap->ExtractMin(fCost, fDistanceTravelled, (void*&)pNode, vPointInPoly, vDirFromPrevious, iFlags, iTmp);
}

bool CPathSearchPriorityQueue::RemoveIfLessCost(TNavMeshPoly * pPoly, const float fNewCost)
{
	CPathServerBinHeap::Node * pNode = m_pBinHeap->FindNode(pPoly);

	if(pNode && fNewCost < pNode->Key)
	{
		m_pBinHeap->Delete(pNode);
		return true;
	}
	return false;
}
bool CPathSearchPriorityQueue::RemoveIfLessCost(CHierarchicalNavNode * pNavNode, const float fNewCost)
{
	CPathServerBinHeap::Node * pNode = m_pBinHeap->FindNode(pNavNode);

	if(pNode && fNewCost < pNode->Key)
	{
		m_pBinHeap->Delete(pNode);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------

// End of file ai/navmesh/priqueue.cpp
