//
// ai/navmesh/tessellation.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_TESSELLATION_H
#define AI_NAVMESH_TESSELLATION_H

#include "ai/navmesh/datatypes.h"
#include "atl/bitset.h"
#include "vector/vector3.h"


namespace rage
{
	class CNavMesh;
	struct TNavMeshPoly;
	struct TDynamicObject;

	// CLASS : CNavResolutionAreaEditor
	// PURPOSE : In-game editor of AABB regions in which navmesh sampling resolution may be controlled
	class CNavResolutionAreaEditor
	{

	};
}

#endif // AI_NAVMESH_TESSELLATION_H
