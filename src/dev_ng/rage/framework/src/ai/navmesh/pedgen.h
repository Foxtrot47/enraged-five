//
// ai/navmesh/pedgen.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_PEDGEN_H
#define AI_NAVMESH_PEDGEN_H

#include "navmesh.h"		// Perhaps only for GTA_ENGINE now.
#include "atl/binheap.h"

namespace rage
{

// This is the maximum number of ped generation coords which may be found each time the pathserver thread
// looks for pedgen coordinates.
#define MAX_POTENTIAL_PEDGEN_COORDS			128

#ifdef GTA_ENGINE

class CPedGenCoords
{
	friend class CPedGenSortedList;
public:
	enum EPedGenFlags
	{
		PEDGENFLAG_INTERIOR				= 0x01,
		PEDGENFLAG_USABLE_IF_OCCLUDED	= 0x02
	};
	struct TItem
	{
		Vector3 m_vPosition;
		u32 m_iFlags;
#if __BANK
		float m_fPedDeficit;
#endif

		inline bool IsInInterior() const { return (m_iFlags & PEDGENFLAG_INTERIOR) != 0; }
		inline bool IsUsableIfOccluded() const { return (m_iFlags & PEDGENFLAG_USABLE_IF_OCCLUDED) != 0; }
	};

	inline void Reset()
	{
		m_iCurrentItem = 0;
		m_iNumItems = 0;
#if __BANK
		m_fMaxDeficit = 0.0f;
#endif
	}

	inline int GetNumRemaining() const { return m_iNumItems-m_iCurrentItem; }

	inline bool GetNext(CPedGenCoords::TItem & coord)
	{
		if(m_iCurrentItem < m_iNumItems)
		{
			coord = m_Coords[m_iCurrentItem++];
			return true;
		}
		return false;
	}

#if __BANK
	void Visualise() const;
	float m_fMaxDeficit;
#endif

protected:
	int m_iNumItems;
	int m_iCurrentItem;
	TItem m_Coords[MAX_POTENTIAL_PEDGEN_COORDS];
};


class CPedGenSortedList
{
	friend class CPathServer;
public:

	class Item
	{
	public:
		Item() { }
		Item(const Vector3 & v, float fDeficit, u32 iFlags)
		{
			m_vPosition = v;
			m_fDeficit = fDeficit;
			m_iFlags = iFlags;
		}

		Vector3 m_vPosition;
		float m_fDeficit;
		u32 m_iFlags;
	};

	CPedGenSortedList();
	~CPedGenSortedList();

	void Reset();

	inline bool GetInUse() const { return m_bInUse; }
	inline void SetInUse(bool b) { m_bInUse = b; }
	
	inline bool GetIsComplete() const { return m_bComplete; }
	inline void SetIsComplete(bool b) { m_bComplete = b; }
	
	inline bool GetIsConsumed() const { return m_bConsumed; }
	inline void SetIsConsumed(bool b) { m_bConsumed = b; }

	inline s32 GetNumCoords() const { return m_Array.GetCount(); }
	inline s32 HasSpaceLeft() const { return m_Array.GetCount() < m_Array.GetMaxCount(); }

	bool Insert(const Vector3 & vPos, const s32 iFlags, const float fPedDeficit);

	void CopyToPedGenCoords(CPedGenCoords & destCoords);

private:

	bool m_bInUse;
	bool m_bComplete;
	bool m_bConsumed;
	float m_fMinDeficit;
	float m_fMaxDeficit;
	atFixedArray<Item, MAX_POTENTIAL_PEDGEN_COORDS> m_Array;
};


class fwPedGenNavMeshIterator
{
public:
	static const s32 m_iMaxNumEntries = 64;
	int m_iNumEntries;

	class CMeshContainer
	{
	public:
		// The index of the navmesh
		s32 m_iNavMesh;
		// The current polygon being processed
		s32 m_iCurrentPoly;
		// The number of polygons we'll process (may be less than the number of polygons in the navmesh -
		// especially in the case of ambient ped generation: polygons with ped density are stored at the
		// start of the navmesh, so we'll only process that subset of the polygons).
		s32 m_iNumPolysFromMesh;
	};

	fwPedGenNavMeshIterator();
	virtual ~fwPedGenNavMeshIterator();

	virtual void Reset();
	virtual void AddNavMesh(CNavMesh * pNavMesh);
	virtual void StartNewInteration(const int iPercentOfCandidatePolysPerNavmesh);
	virtual bool GetNext(CNavMesh ** pOutNavMesh, TNavMeshPoly ** pOutPoly);
	virtual bool IsAtEnd();

	s32 GetTotalNumPolys() const { return m_iTotalNumPolys; }

#if __BANK
	s32 GetNumEntries() { return m_iNumEntries; }
	s32 GetCurrentIndex() { return m_iCurrentIndex; }
	s32 GetNumRemainingInNavmesh() { return m_iNumRemainingInNavMesh; }
	s32 GetTotalNumPolys() { return m_iTotalNumPolys; }
	const fwPedGenNavMeshIterator::CMeshContainer & GetContainer(s32 i) { return m_Containers[i]; }
#endif

protected:
	CMeshContainer m_Containers[m_iMaxNumEntries];
	s32 m_iPercentage;
	s32 m_iCurrentIndex;
	s32 m_iTotalNumPolys;
	s32 m_iNumRemainingInNavMesh;
};



//************************************************************************
//	CPedGenBlockedArea
//	This describes an area within which ped generation is to be briefly
//	disabled.  This can be used to prevent peds from being spawned in the
//	middle of gunfights, etc.
//************************************************************************

#define MAX_PEDGEN_BLOCKED_AREAS	64

struct TAngledArea
{
	float m_CornerPts[4][2];
	float m_PlaneNormals[4][2];
	float m_PlaneDists[4];
	float m_fBottomZ;
	float m_fTopZ;

	inline Vector2 GetPoint2(const int i) { return Vector2(m_CornerPts[i][0], m_CornerPts[i][1]); }
	inline Vector2 GetNormal2(const int i) { return Vector2(m_PlaneNormals[i][0], m_PlaneNormals[i][1]); }
	inline Vector3 GetPoint3(const int i) { return Vector3(m_CornerPts[i][0], m_CornerPts[i][1], 0.0f); }
	inline Vector3 GetNormal3(const int i) { return Vector3(m_PlaneNormals[i][0], m_PlaneNormals[i][1], 0.0f); }
	inline void SetPoint(const int i, const Vector2 & p) { m_CornerPts[i][0] = p.x; m_CornerPts[i][1] = p.y; }
	inline void SetNormal(const int i, const Vector2 & p) { m_PlaneNormals[i][0] = p.x; m_PlaneNormals[i][1] = p.y; }

	bool LiesWithin(const Vector3 & pt);
};

struct TSphereArea
{
	float m_fOrigin[3];
	float m_fActiveRadius;
	float m_fRadiusSqr;
	float m_fOriginalRadius;

	inline Vector3 GetOrigin() const { return Vector3(m_fOrigin[0], m_fOrigin[1], m_fOrigin[2]); }
	inline void SetOrigin(const Vector3 & p) { m_fOrigin[0] = p.x; m_fOrigin[1] = p.y; m_fOrigin[2] = p.z; }

	bool LiesWithin(const Vector3 & p);
};

class CPedGenBlockedArea
{
public:

	enum EType
	{
		ENone		= 0,
		ESphere,
		EAngledArea
	};

	enum EOwner
	{
		EUnspecified,
		ECutscene,
		EShockingEvent
	};

	void Init(const Vector3 & vOrigin, const float fRadius, const u32 iDuration, EOwner owner = EUnspecified, const bool bShrink=false);
	void Init(const Vector3 * pCornerPts, const float fTopZ, const float fBottomZ, const u32 iDuration, EOwner owner = EUnspecified);

	bool IsCutScene() const { return m_iOwner == ECutscene; }

	EOwner GetOwner() const { return (EOwner)m_iOwner; }

	void Clear()
	{
		m_bActive = false;
		m_iType = ENone;
		m_iStartTime = 0;
		m_iDuration = 0;
	}

	u32 m_bActive		:1;
	u32 m_iOwner		:2;
	u32 m_bShrink		:1; // If set for ESphere type, blocking area shrinks over the second half of its duration [3/4/2013 mdawe]
	u32 m_iType			:4;

	u32 m_iStartTime;
	u32 m_iDuration;

	union
	{
		TSphereArea m_Sphere;
		TAngledArea m_Area;
	};

};	// 32 bytes

#endif	// GTA_ENGINE

}	// namespace rage

#endif	// AI_NAVMESH_PEDGEN_H

// End of file ai/navmesh/pedgen.h
