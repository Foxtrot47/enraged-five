//
// ai/navmesh/datatypes.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "datatypes.h"
#if HEIGHTMAP_GENERATOR_TOOL
#include "phbound/bound.h"
#include "phbound/primitives.h"
#endif

NAVMESH_OPTIMISATIONS()

using namespace rage;

#if !HEIGHTMAP_GENERATOR_TOOL
CompileTimeAssertSize(TColPolyData,6,6);
#endif

#define INVALID_BOUND_TYPE 0x1f

fwNavTriData::fwNavTriData()
{
	sysMemSet(this, 0, sizeof(*this));
#if HEIGHTMAP_GENERATOR_TOOL
	m_iFlagsStruct.m_iBoundType = INVALID_BOUND_TYPE; // set bound type to an invalid value
#endif
}

#if HEIGHTMAP_GENERATOR_TOOL
int fwNavTriData::GetPrimitiveType() const
{
	if (m_iFlagsStruct.m_iBoundType == phBound::BVH)
	{
		return m_iFlagsStruct.m_iBvhPrimitiveType;
	}
	else
	{
		switch (m_iFlagsStruct.m_iBoundType)
		{
		case phBound::BOX      : return PRIM_TYPE_BOX;
		case phBound::CYLINDER : return PRIM_TYPE_CYLINDER;
		case phBound::CAPSULE  : return PRIM_TYPE_CAPSULE;
		case phBound::SPHERE   : return PRIM_TYPE_SPHERE;
		case phBound::GEOMETRY : return PRIM_TYPE_POLYGON;
		}
	}

	return -1;
}

bool fwNavTriData::IsBox     () const { return m_iFlagsStruct.m_iBoundType == phBound::BOX      || (m_iFlagsStruct.m_iBoundType == phBound::BVH && m_iFlagsStruct.m_iBvhPrimitiveType == PRIM_TYPE_BOX     ); }
bool fwNavTriData::IsCylinder() const { return m_iFlagsStruct.m_iBoundType == phBound::CYLINDER || (m_iFlagsStruct.m_iBoundType == phBound::BVH && m_iFlagsStruct.m_iBvhPrimitiveType == PRIM_TYPE_CYLINDER); }
bool fwNavTriData::IsCapsule () const { return m_iFlagsStruct.m_iBoundType == phBound::CAPSULE  || (m_iFlagsStruct.m_iBoundType == phBound::BVH && m_iFlagsStruct.m_iBvhPrimitiveType == PRIM_TYPE_CAPSULE ); }
bool fwNavTriData::IsSphere  () const { return m_iFlagsStruct.m_iBoundType == phBound::SPHERE   || (m_iFlagsStruct.m_iBoundType == phBound::BVH && m_iFlagsStruct.m_iBvhPrimitiveType == PRIM_TYPE_SPHERE  ); }
#endif

void TColPolyData::Read(fiStream* pStream)
{
	pStream->ReadShort(&m_iBitField1, 1);
	pStream->ReadByte(&m_iCollisionTypeFlags, 1);
	pStream->ReadByte(&m_iPedDensity, 1);
#if HEIGHTMAP_GENERATOR_TOOL
	pStream->ReadInt(&m_triData.m_iArchetypeFlags, 1);
	pStream->ReadByte(&m_triData.m_iPolyDensity, 1);
	pStream->ReadByte(&m_triData.m_iPrimDensity_MOVER, 1);
	pStream->ReadByte(&m_triData.m_iPrimDensity_WEAPON, 1);
	pStream->ReadInt(&m_triData.m_iRegionHash, 1);
	pStream->ReadInt(&m_triData.m_iBoundHash, 1);
	pStream->ReadInt(&m_triData.m_iFlags, 1);
	pStream->ReadLong(&m_triData.m_iMaterialId, 1);
	pStream->ReadByte(&m_triData.m_iMaterialVfxGroupId, 1);
	pStream->ReadByte(&m_triData.m_iMaterialMaskIndex, 1);
	pStream->ReadByte(&m_triData.m_iMaterialProcIndex, 1);
	m_triData.m_pBoundName = NULL;
#endif
}

void TColPolyData::Write(fiStream* pStream) const
{
	pStream->WriteShort(&m_iBitField1, 1);
	pStream->WriteByte(&m_iCollisionTypeFlags, 1);
	pStream->WriteByte(&m_iPedDensity, 1);
#if HEIGHTMAP_GENERATOR_TOOL
	pStream->WriteInt(&m_triData.m_iArchetypeFlags, 1);
	pStream->WriteByte(&m_triData.m_iPolyDensity, 1);
	pStream->WriteByte(&m_triData.m_iPrimDensity_MOVER, 1);
	pStream->WriteByte(&m_triData.m_iPrimDensity_WEAPON, 1);
	pStream->WriteInt(&m_triData.m_iRegionHash, 1);
	pStream->WriteInt(&m_triData.m_iBoundHash, 1);
	pStream->WriteInt(&m_triData.m_iFlags, 1);
	pStream->WriteLong(&m_triData.m_iMaterialId, 1);
	pStream->WriteByte(&m_triData.m_iMaterialVfxGroupId, 1);
	pStream->WriteByte(&m_triData.m_iMaterialMaskIndex, 1);
	pStream->WriteByte(&m_triData.m_iMaterialProcIndex, 1);
#endif
}

#if HEIGHTMAP_GENERATOR_TOOL
const char TColPolyData::m_TriFileHeader[8] = "HEIGHT1"; // heightmap tool uses a different format
#else
const char TColPolyData::m_TriFileHeader[8] = "NAVTRI9";
#endif

float TInfluenceSphere::ms_fMaxWeighting = 100.0f;
float TInfluenceSphere::ms_fMinWeighting = -100.0f;
/*
void TDynObjBounds::ExpandVertices(const float fDistance, Vector2 * vCornerVerts )
{
	s32 lasti = 3;
	for(s32 i=0; i<4; i++)
	{
		vCornerVerts[i].x += m_vEdgePlaneNormals[lasti].x * fDistance;
		vCornerVerts[i].y += m_vEdgePlaneNormals[lasti].y * fDistance;
		vCornerVerts[i].x += m_vEdgePlaneNormals[i].x * fDistance;
		vCornerVerts[i].y += m_vEdgePlaneNormals[i].y * fDistance;

		lasti = i;
	}
}
*/
void TDynObjBounds::CalculateSegmentPlanes(Vector3 * pPlanes, Vector2 * vCornerVerts ) const
{
	//Compute the segment-planes
	for( s32 i=0; i<4; i++ )
	{
		Vector2 vDir(vCornerVerts[i].x - m_vOrigin.x, vCornerVerts[i].y - m_vOrigin.y);
		vDir.Normalize();
		pPlanes[i].x = -vDir.y;
		pPlanes[i].y = vDir.x;
		pPlanes[i].z = - ((pPlanes[i].x * vCornerVerts[i].x) + (pPlanes[i].y * vCornerVerts[i].y));
	}
}

void TDynamicObject::SetFlagsForNewObject()
{
#ifdef GTA_ENGINE
	m_bIsActive = false;
#else
	m_bIsActive = true;
#endif

	m_pEntity = NULL;
	m_pNext = NULL;

	m_pPrevObjInGridCell = NULL;
	m_pNextObjInGridCell = NULL;
	m_pOwningGridCell = NULL;

	m_bIsOpenable = false;
	m_bIsDoor = false;
	m_bNewBounds = false;
	m_bPossibleIntersection = false;
	m_bIsClimbable = false;
	m_bIsPushable = false;
	m_bIsVehicle = false;
	m_bIsObject = false;
	m_bIsFire = false;
	m_bIsUserAddedBlockingObject = false;
	m_bScriptedBlockingObject = false;
	m_iBlockingObjectFlags = TDynamicObject::BLOCKINGOBJECT_ALL_PATH_TYPES;
	m_bIsMotorbike = false;
	m_bHasUprootLimit = false;
	m_bIsBreakableGlass = false;
	m_bBoundsAdjustedForEntityWidth = false;
	m_bVehicleDoor = false;
	m_bInactiveDueToVelocity = false;
	m_bIsSignificant = false;
	m_bForceReducedBoundingBox = false;
	m_bActiveForNavigation = true;
	m_bIsOutsideWorld = false;

	m_bFlaggedForDeletion = false;
	m_bCurrentlyCopyingBounds = false;
	m_bCurrentlyUpdatingNewBounds = false;

	m_bIsCurrentlyAnObstacle = true;
	m_bNeedsReInsertingIntoGridCells = true;
}


// End of file ai/navmesh/datatypes.cpp


