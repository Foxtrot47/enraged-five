#ifndef AI_NAVMESHEXTENTS_H
#define AI_NAVMESHEXTENTS_H

#include "vector/vector3.h"
#include "math/amath.h"

#include "fwgeovis/geovis.h"

namespace rage
{

//-----------------------------------------------------------------------------

class CPathServerExtents
{
public:
	CPathServerExtents() { }
	~CPathServerExtents() { }

//	static s32 NavMeshIndexToNodesBlockIndex(s32 iNavMesh);

	static s32 m_iNumSectorsPerNavMesh;
	static s32 m_iNumNavMeshesInX;
	static s32 m_iNumNavMeshesInY;
	static s32 m_iHierarchicalNodesBlockSize;

	static Vector3 m_vWorldMin;
	static Vector3 m_vWorldMax;

	static s32 GetNumSectorsPerNavMesh() { return m_iNumSectorsPerNavMesh; }

#if HEIGHTMAP_GENERATOR_TOOL
	static float GetWorldWidthOfSector() { return (float)gv::WORLD_CELL_SIZE; }
#else
	static float GetWorldWidthOfSector() { return 50.0f; }
#endif
	static float GetWorldBorderXMin() { return m_vWorldMin.x; }
	static float GetWorldBorderYMin() { return m_vWorldMin.y; }
	static float GetWorldBorderXMax() { return m_vWorldMax.x; }
	static float GetWorldBorderYMax() { return m_vWorldMax.y; }

	static float GetSizeOfNavMesh() { return m_iNumSectorsPerNavMesh * GetWorldWidthOfSector(); }

	static int GetWorldWidthInSectors() { return (int) (m_iNumNavMeshesInX * m_iNumSectorsPerNavMesh); }
	static int GetWorldDepthInSectors() { return (int) (m_iNumNavMeshesInY * m_iNumSectorsPerNavMesh); }

	static int GetWorldToSectorX(const float f) {
		const float d = f - m_vWorldMin.x;
		return (int) (Floorf(d / GetWorldWidthOfSector()));
	}
	static int GetWorldToSectorY(const float f) {
		const float d = f - m_vWorldMin.y;
		return (int) (Floorf(d / GetWorldWidthOfSector()));
	}

	static int GetWorldToSectorXRoundUp(const float f) {
		const float d = f - m_vWorldMin.x;
		return (int) (ceilf(d / GetWorldWidthOfSector()));
	}
	static int GetWorldToSectorYRoundUp(const float f) {
		const float d = f - m_vWorldMin.y;
		return (int) (ceilf(d / GetWorldWidthOfSector()));
	}

	static float GetSectorToWorldX(const int s) {  return m_vWorldMin.x + (s * GetWorldWidthOfSector()); }
	static float GetSectorToWorldY(const int s) {  return m_vWorldMin.y + (s * GetWorldWidthOfSector()); }
};

//-----------------------------------------------------------------------------

}	// namespace rage

#endif	// AI_NAVMESHEXTENTS_H
