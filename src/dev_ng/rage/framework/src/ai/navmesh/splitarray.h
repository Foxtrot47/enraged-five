#ifndef SPLITARRAY_H
#define SPLITARRAY_H

#include "data\resource.h"
#include "data\struct.h"
#include "math/amath.h"

#ifndef COMPILE_TOOL_MESH
namespace rage
{
#endif

// CLASS : aiSplitArray
// PURPOSE : Convenient way to abstract the splitting of a block allocation into multiple sections

template <typename T, int _BlockSize> class aiSplitArray
{
	class SubArray
	{
	public:
		T * m_pElements;
		u32 m_iCount;	// required for resourcing

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct & s)
		{
			STRUCT_DYNAMIC_ARRAY(m_pElements, m_iCount);
			STRUCT_FIELD(m_iCount);
		}
#endif // __DECLARESTRUCT

	};

public:

	aiSplitArray() : m_iNumElements(0), m_iNumSubArrays(0), m_SubArrays(NULL) { }

	aiSplitArray(const u32 iNumElements)
	{
		Init(iNumElements);
	}
	aiSplitArray(const u32 iNumElements, T* pElements)
	{
		Init(iNumElements);

		// Copy elements from input array
		for(u32 i=0; i<iNumElements; i++)
		{
			T* pSrcItem = &pElements[i];
			T* pDestItem = Get(i);
			memcpy(pDestItem, pSrcItem, sizeof(T));
		}
	}

	// PURPOSE : Destructor
	virtual ~aiSplitArray()
	{
		Shutdown();
	}

	// PURPOSE : Resource constructor
	aiSplitArray(datResource & rsc)
	{
		rsc.PointerFixup(m_SubArrays);
		rsc.PointerFixup(m_StartIndices);

		for(u32 i=0; i<m_iNumSubArrays; i++)
		{
			rsc.PointerFixup(m_SubArrays[i].m_pElements);
		}
	}

	// NAME : Place
	// PURPOSE : Resource placement function
	void Place(aiSplitArray<T, _BlockSize> * that, datResource& rsc)
	{
		::new (that) aiSplitArray<T, _BlockSize>(rsc);
	}

	// NAME : DeclareStruct
	// PURPOSE : Class layout definition for resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct & s)
	{
		STRUCT_BEGIN(aiSplitArray);
		STRUCT_FIELD(m_iNumElements);
		STRUCT_DYNAMIC_ARRAY(m_SubArrays, m_iNumSubArrays);
		STRUCT_DYNAMIC_ARRAY(m_StartIndices, m_iNumSubArrays);
		STRUCT_FIELD(m_iNumSubArrays);
		STRUCT_END();
	}
#endif // __DECLARESTRUCT

	// NAME : Shutdown
	// PURPOSE : Deallocate memory
	inline void Shutdown()
	{
		for(u32 i=0; i<m_iNumSubArrays; i++)
		{
			T * pSubArray = m_SubArrays[i].m_pElements;
			delete[] pSubArray;
		}
		delete[] m_SubArrays;
		delete[] m_StartIndices;
	}

	// NAME : Get
	// PURPOSE : Return item pointer given an index
	inline T* Get(const u32 iIndex) const
	{
		FastAssert(iIndex < m_iNumElements);
		const u32 iSubArray = iIndex / _BlockSize;
		FastAssert(iSubArray < m_iNumSubArrays);
		return &m_SubArrays[iSubArray].m_pElements[iIndex-(iSubArray*_BlockSize)];
	}

	// NAME : GetIndex
	// PURPOSE : Return the index, given an item (which must exist within the data structure).
	// NOTES : This is not an efficient query method; see the overloaded GetIndex() for a known subarray for a more efficient version
	inline u32 GetIndex(const T * pItemAddress) const
	{
		for(u32 i=0; i<m_iNumSubArrays; i++)
		{
			const T* pFirst = &m_SubArrays[i].m_pElements[0];
			const s32 iIndex = ptrdiff_t_to_int(pItemAddress - pFirst);
			if(iIndex >= 0 && iIndex < _BlockSize)
				return (i*_BlockSize)+iIndex;
		}
		FastAssert(false);
		return 0;
	}

	// NAME : GetIndex
	// PURPOSE : Returns the index of an item, where its subarray is known
	inline u32 GetIndex(const T * pItemAddress, const u32 iArray)
	{
		FastAssert(iArray < m_iNumSubArrays);
		const s32 iIndex = ptrdiff_t_to_int(pItemAddress - (T*)&m_SubArrays[iArray].m_pElements[0]);
		Assert(iIndex >= 0 && iIndex < _BlockSize);
		return m_StartIndices[iArray] + iIndex;
	}

	// NAME : GetSubArray
	// PURPOSE : Returns which subarray a given element index exists within
	inline u32 GetSubArrayIndex(const u32 iIndex)
	{
		return iIndex / _BlockSize;
	}

	// NAME : GetSubArray
	// PURPOSE : Returns start of storage for specified subarray
	inline T * GetSubArray(const u32 iArrayIndex)
	{
		return m_SubArrays[iArrayIndex].m_pElements;
	}

	// NAME : SetZero
	// PURPOSE : Fill all subarrays with zero
	inline void SetZero()
	{
		u32 iRemaining = m_iNumElements;
		for(u32 i=0; i<m_iNumSubArrays; i++)
		{
			memset(m_SubArrays[i].m_pElements, 0, sizeof(T) * Min((u32)iRemaining, (u32)_BlockSize) );
			iRemaining -= _BlockSize;
		}
	}

	// NAME : Clone
	// PURPOSE : Return a copy
	inline aiSplitArray * Clone() const
	{
		aiSplitArray * pNew = rage_new aiSplitArray<T, _BlockSize>(m_iNumElements);
		u32 iRemaining = m_iNumElements;
		for(u32 i=0; i<m_iNumSubArrays; i++)
		{
			T * pDestElements = pNew->GetSubArray(i);
			memcpy( pDestElements, m_SubArrays[i].m_pElements, sizeof(T) * Min((u32)iRemaining, (u32)_BlockSize) );
			iRemaining -= _BlockSize;
		}
		return pNew;
	}

protected:

	// NAME : Init
	// PURPOSE : Init class and allocate for given number of elements
	inline void Init(const u32 iNumElements)
	{
		m_iNumElements = iNumElements;

		m_iNumSubArrays = m_iNumElements / _BlockSize;
		if(m_iNumElements % _BlockSize) m_iNumSubArrays++;

		m_StartIndices = rage_new u32[m_iNumSubArrays];
		m_SubArrays = rage_new SubArray[m_iNumSubArrays];

		u32 iRemaining = m_iNumElements;
		u32 iStart = 0;

		for(u32 i=0; i<m_iNumSubArrays; i++)
		{
			const u32 iNum = Min((u32)iRemaining, (u32)_BlockSize);
			
			m_StartIndices[i] = iStart;
			m_SubArrays[i].m_pElements = rage_new T[iNum];
			m_SubArrays[i].m_iCount = iNum;

			iRemaining -= _BlockSize;
			iStart += _BlockSize;
		}
	}

	// Total number of elements stored in all subarrays
	u32 m_iNumElements;
	// The array of subarrays, all except the last of which will be _BlockSize in length
	SubArray * m_SubArrays;
	// The starting index of each subarray
	u32 * m_StartIndices;
	// Number of subarrays
	u32 m_iNumSubArrays;
};

#ifndef COMPILE_TOOL_MESH
}	// namespace rage
#endif

#endif // SPLITARRAY_H

