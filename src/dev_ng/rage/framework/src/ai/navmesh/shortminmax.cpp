#include "ai/navmesh/shortminmax.h"

#ifndef COMPILE_TOOL_MESH
namespace rage
{
#endif

	const float TShortMinMax::ms_fResolution = 0.25f;
	const float TShortMinMax::ms_fMaxRepresentableValue = 8191.0f;
	const Vector3 TShortMinMax::ms_vExtendMinMax(0.0f, 0.0f, 0.0f);

#ifndef COMPILE_TOOL_MESH
}	// namespace rage
#endif


