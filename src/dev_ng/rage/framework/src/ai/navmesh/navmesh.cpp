#ifndef COMPILE_TOOL_MESH	// Don't do the #includes if building through 'fwnavgen/toolmesh.cpp', otherwise symbols will end up in the wrong namespace. /FF

#include "ai/navmesh/navmesh.h"

// Rage headers
#include "math/simpleMath.h"
#include "spatialdata/sphere.h"

// Framework headers
#include "fwmaths/vector.h"

// Game headers
#include "data/safestruct.h"
#include "data/struct.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "paging/rscbuilder.h"
#include "vector/geometry.h"
#include "vector/vector2.h"
#include "vector/color32.h"

using namespace rage;

// warning C4800: 'int' : forcing value to bool 'true' or 'false' (performance warning)
#if __WIN32
#pragma warning(disable: 4800)
#endif

#endif	// COMPILE_TOOL_MESH

// Define "__ISECT_RAY_USE_MINMAX" as 1, to optimise intersection tests using the TShortMinMax class
#define __ISECT_RAY_USE_MINMAX				1

#ifdef GTA_ENGINE
NAVMESH_OPTIMISATIONS()
#endif

const u32 CNavMesh::ms_iFileVersion = 0x00010011;
const u32 CNavMesh::ms_iNumPolysInTesselationNavMesh = 4096;
bool CNavMesh::ms_bDisplayTimeTakenToLoadNavMeshes = false;
bool CNavMesh::ms_bForceAllNavMeshesToUseUnCompressedVertices = false;
float CNavMesh::ms_fGetPolyBestOverlap = 0.0f;
u32 CNavMesh::ms_iGetPolyBestIndex = NAVMESH_POLY_INDEX_NONE;
bool CNavMesh::ms_bGetPolyOnlyOnPavement = false;
bool CNavMesh::ms_bGetPolyClosestToCentre = false;
bool CNavMesh::ms_bGetPolyFoundAnyPoly = false;
Vector3 CNavMesh::ms_vGetPolyVolumeCentre(0,0,0);
float CNavMesh::ms_fGetPolyLeastDistSqr = FLT_MAX;

Vector3 CNavMesh::m_vIntersectRayStart(0,0,0);
Vector3 CNavMesh::m_vIntersectRayEnd(0,0,0);
Vector3 CNavMesh::m_vIntersectRayMins(0,0,0);
Vector3 CNavMesh::m_vIntersectRayMaxs(0,0,0);
Vector3 CNavMesh::m_fClosestIntersectPos(0,0,0);
float CNavMesh::m_fClosestIntersectDistSqr = 0.0f;
u16 CNavMesh::m_iClosestIntersectPolyIndex = NAVMESH_POLY_INDEX_NONE;

bool CNavMesh::ms_bLoadCoverPoints = true;

#ifndef COMPILE_TOOL_MESH
u8 rage::g_iCountCoverBitsTable[256];
#endif

#if __XENON && !__FINAL
bool CNavMesh::ms_bUseVmxOptimisations = false;
#endif
bool CNavMesh::ms_bUsePrefetching = false;

#if 1
float CNavMesh::ms_fSmallestTessellatedEdgeThresholdSqr = 5.0f*5.0f;
#else
const float CNavMesh::ms_fSmallestTessellatedEdgeThresholdSqr = 5.0f*5.0f;
#endif

#ifndef NAVGEN_TOOL
CompileTimeAssertSize(TAdjPoly,8,8);
CompileTimeAssertSize(TShortMinMax,12,12);
CompileTimeAssertSize(TNavMeshPoly,40,48);
CompileTimeAssertSize(TNavMeshQuadTreeLeafData,16,32);
CompileTimeAssertSize(CNavMeshQuadTree,64,RSG_ORBIS? 84 : 96);
CompileTimeAssertSize(TCoverPointFlags,2,2);
CompileTimeAssertSize(CNavMeshCoverPoint,8,8);
CompileTimeAssertSize(CSpecialLinkInfo,28,28);
#endif

#if __DECLARESTRUCT
void CAdjacentNavmeshLookups::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(CAdjacentNavmeshLookups);
	STRUCT_FIELD(m_iNumIndices);
	STRUCT_CONTAINED_ARRAY(m_iNavmeshIndices);
	STRUCT_END();
}
#endif // !__FINAL


const float TAdjPoly::ms_fFreeSpaceFixedToFloatMult = 0.25f;
const float TAdjPoly::ms_fFreeSpaceFloatToFixedMult = 4.0f;

const float TAdjPoly::ms_fMaxFreeSpaceBeyondEdge = 7.75f;
const float TAdjPoly::ms_fMaxFreeSpaceAroundVertex = 7.75f;

s32 TAdjPoly::ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NUM];

void TAdjPoly::InitAdjacentNavmeshOffsets(const s32 iNumMeshesInX)
{
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NONE] = 0;

	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NEGX_NEGY] = - (iNumMeshesInX+1);
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NEGY] = -iNumMeshesInX;
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_POSX_NEGY] = - (iNumMeshesInX-1);
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NEGX] = -1;
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_POSX] = 1;
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_NEGX_POSY] = (iNumMeshesInX-1);
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_POSY] = iNumMeshesInX;
	ms_iAdjacentNavmeshOffsets[ADJACENT_OFFSET_POSX_POSY] = (iNumMeshesInX+1);
}

#if __DECLARESTRUCT
void TAdjPoly::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(TAdjPoly);
	STRUCT_FIELD(m_iAsInteger1);
	STRUCT_FIELD(m_iAsInteger2);
	STRUCT_END();
}
#endif // !__FINAL



#if __DECLARESTRUCT
void TNavMeshPoly::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(TNavMeshPoly);

	STRUCT_FIELD(m_iAsInteger1);
	STRUCT_FIELD(m_iAsInteger2);
	STRUCT_FIELD(m_TimeStamp);
	STRUCT_FIELD(m_AStarTimeStamp);
	STRUCT_FIELD_VP(m_PathParentPoly);
	STRUCT_FIELD(m_MinMax);
	STRUCT_FIELD(m_iAsInteger3);
	STRUCT_FIELD(m_iCentroidX);
	STRUCT_FIELD(m_iCentroidY);
	STRUCT_FIELD(m_iCoverDirectionsBitfield);
	STRUCT_FIELD(m_iImmediateModeFlags);
	STRUCT_FIELD(m_iAsInteger4);

	STRUCT_END();
}
#endif // __DECLARESTRUCT

#if __DECLARESTRUCT
void CNavMeshCompressedVertex::DeclareStruct(datTypeStruct & s)
{
	STRUCT_BEGIN(CNavMeshCompressedVertex);
	STRUCT_CONTAINED_ARRAY(data);
	STRUCT_END();
}
#endif

const Vector3 TNavMeshPoly::ms_vCoverDirections[DIRECTIONAL_COVER_NUM_DIRS] =
{
	Vector3(0.0f, 1.0f, 0.0f),
	Vector3(-0.70712f, 0.70712f, 0.0f),
	Vector3(-1.0f, 0.0f, 0.0f),
	Vector3(-0.70712f, -0.70712f, 0.0f),
	Vector3(0.0f, -1.0f, 0.0f),
	Vector3(0.70712f, -0.70712f, 0.0f),
	Vector3(1.0f, 0.0f, 0.0f),
	Vector3(0.70712f, 0.70712f, 0.0f)
};

CNavMeshCoverPoint::CNavMeshCoverPoint(void)
{
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	memset(this, 0, sizeof(CNavMeshCoverPoint));
#endif
#endif

	m_iX = m_iY = m_iZ = 0;

	m_CoverPointFlags.SetCoverDir(0);
	m_CoverPointFlags.SetCoverType(0);

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	m_pOwningNavMesh = NULL;
	m_iCoverPointIndexWithinNavMesh = 0;

	m_fPosOnNavMeshX = 0.0f;
	m_fPosOnNavMeshY = 0.0f;
	m_fPosOnNavMeshZ = 0.0f;
	m_bRemoveMe = false;
#endif
#endif
}

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
CNavMeshCoverPoint::~CNavMeshCoverPoint(void)
{

}
#endif
#endif

#if __DECLARESTRUCT
void CNavMeshCoverPoint::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(CNavMeshCoverPoint);
	STRUCT_FIELD(m_iX);
	STRUCT_FIELD(m_iY);
	STRUCT_FIELD(m_iZ);
	STRUCT_FIELD(m_CoverPointFlags);
	STRUCT_END();
}
#endif



CNavMesh::CNavMesh(void)
{
	m_iTotalMemoryUsed = sizeof(CNavMesh);

	m_iFlags = 0;
	m_iFileVersion = 0;
	m_Matrix.Identity();
	m_vExtents = Vector3(0.0f, 0.0f, 0.0f);
	m_CompressedVertexArray = NULL;
	m_VertexPool = NULL;
	m_VertexIndexArray = NULL;
	m_AdjacentPolysArray = NULL;
	m_iSizeOfPools = 0;
	m_PolysArray = NULL;
	m_pQuadTree = NULL;
	m_SpecialLinks = NULL;
	m_SpecialLinksIndices = NULL;
	m_iNumVertices = 0;
	m_iNumPolys = 0;
	m_iIndexOfMesh = NAVMESH_NAVMESH_INDEX_NONE;
	m_iTotalMemoryUsed = 0;	
	m_iNumCoverPoints = 0;
	m_iNumSpecialLinks = 0;
	m_iNumSpecialLinksIndices = 0;
	m_pNonResourcedData = 0;

#if __RESOURCECOMPILER
	sysMemStartTemp();
#endif
	m_pNonResourcedData = rage_new CNavMeshNonResourcedData;
#if __RESOURCECOMPILER
	sysMemEndTemp();
#endif
}

CNavMesh::~CNavMesh(void)
{
	if(m_CompressedVertexArray)
	{
		delete m_CompressedVertexArray;
		m_CompressedVertexArray = NULL;
	}
	if(m_VertexPool)
	{
		CNavMeshAlloc<Vector3> vec3Alloc;
		vec3Alloc.VectorDelete(m_VertexPool, 0, false);
		m_VertexPool = NULL;
	}
	if(m_PolysArray)
	{
		delete m_PolysArray;
		m_PolysArray = NULL;
	}
	if(m_VertexIndexArray)
	{
		delete m_VertexIndexArray;
		m_VertexIndexArray = NULL;
	}
	if(m_AdjacentPolysArray)
	{
		delete m_AdjacentPolysArray;
		m_AdjacentPolysArray = NULL;
	}
	if(m_pQuadTree)
	{
		CNavMeshAlloc<CNavMeshQuadTree> treeAlloc;
		// This will delete all children as well
		treeAlloc.Delete(m_pQuadTree);
		m_pQuadTree = NULL;
	}
	if(m_SpecialLinks)
	{
		CNavMeshAlloc<CSpecialLinkInfo> linkAlloc;
		linkAlloc.VectorDelete(m_SpecialLinks, 0, false);
		m_SpecialLinks = NULL;
	}
	if(m_SpecialLinksIndices)
	{
		delete[] m_SpecialLinksIndices;
		m_SpecialLinksIndices = NULL;
	}
	if(m_pNonResourcedData)
	{
		delete m_pNonResourcedData;
		m_pNonResourcedData = NULL;
	}
}

CNavMesh::CNavMesh(datResource& rsc) :
	m_pQuadTree(rsc),
	m_PolysArray(rsc),
	m_AdjacentPolysArray(rsc),
	m_CompressedVertexArray(rsc),
	m_VertexIndexArray(rsc)
{
	if(m_VertexPool)
		rsc.PointerFixup(m_VertexPool);
	if(m_SpecialLinks)
		rsc.PointerFixup(m_SpecialLinks);
	if(m_SpecialLinksIndices)
		rsc.PointerFixup(m_SpecialLinksIndices);

	if(datResource_IsDefragmentation)
	{
#if NAVMESH_OPTIMISATIONS_OFF
		Printf("Defrag CNavMesh\n");
#endif
	}
	else
	{
		m_pNonResourcedData = rage_new CNavMeshNonResourcedData;
	}
}


#if __DECLARESTRUCT && (!defined(NAVGEN_TOOL) || defined(RAGEBUILDER))
void
CNavMesh::DeclareStruct(datTypeStruct & s)
{
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(CNavMesh);

	STRUCT_FIELD(m_iFlags);
	STRUCT_FIELD(m_iFileVersion);
	STRUCT_FIELD(m_Matrix);
	STRUCT_FIELD(m_vExtents);

	STRUCT_FIELD(m_CompressedVertexArray);
	STRUCT_IGNORE(m_VertexPool);
	STRUCT_FIELD(m_VertexIndexArray);

	STRUCT_FIELD(m_AdjacentPolysArray);

	STRUCT_FIELD(m_iSizeOfPools);
	STRUCT_FIELD(m_AdjacentMeshes);

	STRUCT_FIELD(m_PolysArray);

	STRUCT_SKIP(m_pQuadTree, 4);

	STRUCT_DYNAMIC_ARRAY(m_SpecialLinks, m_iNumSpecialLinks);
	STRUCT_DYNAMIC_ARRAY(m_SpecialLinksIndices, m_iNumSpecialLinksIndices);

	STRUCT_FIELD(m_iNumVertices);
	STRUCT_FIELD(m_iNumPolys);
	STRUCT_FIELD(m_iIndexOfMesh);
	STRUCT_FIELD(m_iTotalMemoryUsed);
	STRUCT_FIELD(m_iNumCoverPoints);
	STRUCT_FIELD(m_iNumSpecialLinks);
	STRUCT_FIELD(m_iNumSpecialLinksIndices);
	STRUCT_SKIP(m_pNonResourcedData, 4);

	STRUCT_FIELD(m_iBuildID);
	STRUCT_IGNORE(m_Padding);
	STRUCT_END();

	if(m_pQuadTree)
	{
		m_pQuadTree->DeclareStruct();
	}
	datSwapper((void*&) m_pQuadTree);
}
#endif


CNavMesh *
CNavMesh::StreamingPlaceFn(s32 NOTFINAL_ONLY(index), datResourceMap& map, datResourceInfo& header)
{
	CNavMesh * pNavMesh = NULL;

#if __FINAL
	pgRscBuilder::PlaceStream(pNavMesh, header, map, "<unknown>");
#else
	char tmp[64];
	sprintf(tmp, "NavMesh StreamingIndex:%i", index);
	pgRscBuilder::PlaceStream(pNavMesh, header, map, tmp);
#endif

	return pNavMesh;
}

void
CNavMesh::Place(CNavMesh* that, datResource& rsc)
{
	::new (that) CNavMesh(rsc);

	//Assertf(that->m_iFileVersion == ms_iFileVersion, "The game is trying to load an incompatible navmesh resource version.\nYou need to either get latest navmeshes, or recompile with latest code.\n");
}



void
CNavMesh::DuplicateAllVerticesAsUncompressed()
{
	Assert(m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA);
	Assert(m_pQuadTree);
	Assert(m_CompressedVertexArray);

	Vector3 * pVerts = rage_new Vector3[m_iNumVertices];
	for(u32 v=0; v<m_iNumVertices; v++)
	{
		GetVertex(v, pVerts[v]);
	}

	delete[] m_CompressedVertexArray;
	m_CompressedVertexArray = NULL;
	m_VertexPool = pVerts;

	m_iFlags &= ~NAVMESH_COMPRESSED_VERTEX_DATA;
}

//******************************************************************
// Identifies which polys are at the edge of the mesh
//******************************************************************

void CNavMesh::IdentifyEdgePolys(const float fEdgeEps)
{
	Assert(m_pQuadTree);

	u32 t;
	u32 v;
	Vector3 vec1, vec2;
	Vector3 vMin = m_pQuadTree->m_Mins;
	Vector3 vMax = m_pQuadTree->m_Maxs;
	int iNumEdgesNegY, iNumEdgesPosY, iNumEdgesNegX, iNumEdgesPosX;

	for(t=0; t<m_iNumPolys; t++)
	{
		TNavMeshPoly * pPoly = GetPoly(t);
		if(pPoly->GetIsDisabled())
			continue;

		iNumEdgesNegY = 0;
		iNumEdgesPosY = 0;
		iNumEdgesNegX = 0;
		iNumEdgesPosX = 0;

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			GetVertex( *m_VertexIndexArray->Get( pPoly->GetFirstVertexIndex() + v), vec1);
			GetVertex( *m_VertexIndexArray->Get( pPoly->GetFirstVertexIndex() + ((v+1)%pPoly->GetNumVertices())), vec2);

			TAdjPoly * adjPoly = m_AdjacentPolysArray->Get( pPoly->GetFirstVertexIndex()+v );
			adjPoly->SetIsExternalEdge(false);

			const TNavMeshIndex iAdjMesh = adjPoly->GetNavMeshIndex( GetAdjacentMeshes() );

			if(iAdjMesh != GetIndexOfMesh())
			{
				// -Y edge
				if(Eq(vec1.y, vMin.y, fEdgeEps) && Eq(vec2.y, vMin.y, fEdgeEps))
				{
					iNumEdgesNegY++;
					adjPoly->SetIsExternalEdge(true);
				}
				// +Y edge
				if(Eq(vec1.y, vMax.y, fEdgeEps) && Eq(vec2.y, vMax.y, fEdgeEps))
				{
					iNumEdgesPosY++;
					adjPoly->SetIsExternalEdge(true);
				}
				// -X edge
				if(Eq(vec1.x, vMin.x, fEdgeEps) && Eq(vec2.x, vMin.x, fEdgeEps))
				{
					iNumEdgesNegX++;
					adjPoly->SetIsExternalEdge(true);
				}
				// +X edge
				if(Eq(vec1.x, vMax.x, fEdgeEps) && Eq(vec2.x, vMax.x, fEdgeEps))
				{
					iNumEdgesPosX++;
					adjPoly->SetIsExternalEdge(true);
				}
			}
		}

		// If there are at least two vertices along any edge, flag as an edge tri
		if(iNumEdgesNegY > 0 || iNumEdgesPosY > 0 || iNumEdgesNegX > 0 || iNumEdgesPosX > 0)
		{
			pPoly->SetLiesAlongEdgeOfMesh(true);
		}
		else
		{
			pPoly->SetLiesAlongEdgeOfMesh(false);
		}
	}
}

#ifndef GTA_ENGINE
u32 CNavMesh::ClassifyEdge(CNavMesh * pNavMesh, const Vector3 & vVert1, const Vector3 & vVert2)
{
	static const float fEdgeEpsilon = 1.11f;//0.05f;
	
	Vector3 vVerts[2] = { vVert1, vVert2 };
	const Vector3 & vMin = pNavMesh->m_pQuadTree->m_Mins;
	const Vector3 & vMax = pNavMesh->m_pQuadTree->m_Maxs;

	int iNumNegY = 0;
	int iNumPosY = 0;
	int iNumNegX = 0;
	int iNumPosX = 0;

	for(int v=0; v<2; v++)
	{
		const Vector3 & vec = vVerts[v];
		// -Y edge
		if(Eq(vec.y, vMin.y, fEdgeEpsilon))
			iNumNegY++;
		// +Y edge
		if(Eq(vec.y, vMax.y, fEdgeEpsilon))
			iNumPosY++;
		// -X edge
		if(Eq(vec.x, vMin.x, fEdgeEpsilon))
			iNumNegX++;
		// +X edge
		if(Eq(vec.x, vMax.x, fEdgeEpsilon))
			iNumPosX++;
	}

	u32 iEdgeFlags = 0;
	if(iNumNegX == 2)
		iEdgeFlags |= EDGEFLAG_NEG_X;
	if(iNumPosX == 2)
		iEdgeFlags |= EDGEFLAG_POS_X;
	if(iNumNegY == 2)
		iEdgeFlags |= EDGEFLAG_NEG_Y;
	if(iNumPosY == 2)
		iEdgeFlags |= EDGEFLAG_POS_Y;

	return iEdgeFlags;
}
#endif

#if __ENSURE_THAT_POLY_QUICK_CENTROIDS_ARE_REALLY_WITHIN_POLYS

void CNavMesh::CheckAllQuickPolyCentroidsAreWithinPolys() const
{
//	for(u32 p=0; p<m_iNumPolys; p++)
//	{
//		CheckQuickPolyCentroidsIsWithinPoly(p);
//	}
}

void CNavMesh::CheckQuickPolyCentroidIsWithinPoly(int iPolyIndex) const
{
	TNavMeshPoly * pPoly = &m_Polys[iPolyIndex];

	Vector3 vRealCentroid, vQuickCentroid, vDiff, vIsectPos;
	GetPolyCentroid(iPolyIndex, vRealCentroid);
	GetPolyCentroidQuick(pPoly, vQuickCentroid);
	vDiff = vRealCentroid - vQuickCentroid;

	const float fArea = GetPolyArea(iPolyIndex);

	const Vector3 vRealAbove = vRealCentroid + Vector3(0.0f, 0.0f, 5.0f);
	const Vector3 vRealBelow = vRealCentroid - Vector3(0.0f, 0.0f, 5.0f);
	const bool bRealIntersects = RayIntersectsPoly(vRealAbove, vRealBelow, pPoly, vIsectPos);
	pathAssertf(bRealIntersects, "Actual centroid doesn't appear to be within poly!");

	const Vector3 vAbove = vQuickCentroid + Vector3(0.0f, 0.0f, 5.0f);
	const Vector3 vBelow = vQuickCentroid - Vector3(0.0f, 0.0f, 5.0f);

	const bool bIntersects = RayIntersectsPoly(vAbove, vBelow, pPoly, vIsectPos);
	pathAssertf(bIntersects, "GetPolyCentroidQuick() - wasn't inside poly (vDiff = %.3f,%.3f,%.3f)", vDiff.x, vDiff.y, vDiff.z);
	const float fZdiff = Abs(vIsectPos.z - vQuickCentroid.z);
	pathAssertf(fZdiff <= 1.0f, "GetPolyCentroidQuick() - z diff was %.3f", vDiff.z);
}

#endif


u32
CNavMesh::GetPolyEdgeFlags(CNavMesh * pNavMesh, const u32 iPolyIndex)
{
	Assert(pNavMesh->m_pQuadTree);
	TNavMeshPoly * pPoly = pNavMesh->GetPoly(iPolyIndex);
	
	Vector3 vec;
	Vector3 vMin = pNavMesh->m_pQuadTree->m_Mins;
	Vector3 vMax = pNavMesh->m_pQuadTree->m_Maxs;
	static const float fEdgeEpsilon = 1.11f;//0.05f;

	int iNumNegY = 0;
	int iNumPosY = 0;
	int iNumNegX = 0;
	int iNumPosX = 0;

	u32 v;

	for(v=0; v<pPoly->GetNumVertices(); v++)
	{
		pNavMesh->GetVertex( *pNavMesh->m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex()+v), vec);

		// -Y edge
		if(Eq(vec.y, vMin.y, fEdgeEpsilon))
		{
			iNumNegY++;
		}
		// +Y edge
		if(Eq(vec.y, vMax.y, fEdgeEpsilon))
		{
			iNumPosY++;
		}
		// -X edgei
		if(Eq(vec.x, vMin.x, fEdgeEpsilon))
		{
			iNumNegX++;
		}
		// +X edge
		if(Eq(vec.x, vMax.x, fEdgeEpsilon))
		{
			iNumPosX++;
		}
	}

	u32 iEdgeFlags = 0;

	// If there are at least two vertices along any edge, flag as an edge tri
	if(iNumNegX >= 2)
	{
		iEdgeFlags |= EDGEFLAG_NEG_X;
	}
	if(iNumPosX >= 2)
	{
		iEdgeFlags |= EDGEFLAG_POS_X;
	}
	if(iNumNegY >= 2)
	{
		iEdgeFlags |= EDGEFLAG_NEG_Y;
	}
	if(iNumPosY >= 2)
	{
		iEdgeFlags |= EDGEFLAG_POS_Y;
	}

	return iEdgeFlags;
}

//******************************************************************
// Establishes poly adjacency within the polys of this mesh
//******************************************************************

int
CNavMesh::EstablishPolyAdjacancy()
{
	u32 t1,t2;
	u32 v1, lastv1;
	u32 v2, lastv2;

	int poly1_Index1, poly1_Index2;
	int poly2_Index1, poly2_Index2;

	// Firstly set all adjacency data to be invalid
	for(t1=0; t1<m_iNumPolys; t1++)
	{
		TNavMeshPoly * pPoly1 = GetPoly(t1);
		if(pPoly1->GetIsDisabled())
			continue;

		for(v1=0; v1<pPoly1->GetNumVertices(); v1++)
		{
			TAdjPoly * pAdj = m_AdjacentPolysArray->Get(pPoly1->GetFirstVertexIndex() + v1);
			pAdj->SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, GetAdjacentMeshes());
			pAdj->SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
			pAdj->SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, GetAdjacentMeshes());
			pAdj->SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);

			pPoly1->SetNavMeshIndex(m_iIndexOfMesh);
		}
	}

	int iNumAdjacencies = 0;

	// Iterate over triangles, matching up edges with shared vertices
	for(t1=0; t1<m_iNumPolys; t1++)
	{
		TNavMeshPoly * pPoly1 = GetPoly(t1);
		if(pPoly1->GetIsDisabled())
			continue;

		// Go through all edges of pPoly1, and try to match with another poly..
		lastv1 = pPoly1->GetNumVertices()-1;
		for(v1=0; v1<pPoly1->GetNumVertices(); v1++)
		{
			poly1_Index1 = GetPolyVertexIndex( pPoly1, lastv1 );
			poly1_Index2 = GetPolyVertexIndex( pPoly1, v1 );

			// Start from t1+1, since we are double-linking each time we find an adjacency
			for(t2=t1+1; t2<m_iNumPolys; t2++)
			{
				TNavMeshPoly * pPoly2 = GetPoly(t2);

				lastv2 = pPoly2->GetNumVertices()-1;
				for(v2=0; v2<pPoly2->GetNumVertices(); v2++)
				{
					poly2_Index1 = GetPolyVertexIndex( pPoly2, lastv2 );
					poly2_Index2 = GetPolyVertexIndex( pPoly2, v2 );

					// Adjacent triangles
					if((poly1_Index1 == poly2_Index1 && poly1_Index2 == poly2_Index2) || (poly1_Index1 == poly2_Index2 && poly1_Index2 == poly2_Index1))
					{
						TAdjPoly & p1Adj = *m_AdjacentPolysArray->Get(pPoly1->GetFirstVertexIndex() + lastv1);
						p1Adj.SetNavMeshIndex(m_iIndexOfMesh, GetAdjacentMeshes());
						p1Adj.SetPolyIndex(t2);
						p1Adj.SetOriginalNavMeshIndex(m_iIndexOfMesh, GetAdjacentMeshes());
						p1Adj.SetOriginalPolyIndex(t2);

						TAdjPoly & p2Adj = *m_AdjacentPolysArray->Get(pPoly2->GetFirstVertexIndex() + lastv2);
						p2Adj.SetNavMeshIndex(m_iIndexOfMesh, GetAdjacentMeshes());
						p2Adj.SetPolyIndex(t1);
						p2Adj.SetOriginalNavMeshIndex(m_iIndexOfMesh, GetAdjacentMeshes());
						p2Adj.SetOriginalPolyIndex(t1);

#if defined(NAVGEN_TOOL) && !defined(RAGEBUILDER)
						p1Adj.m_iAdjacentNavMeshIndex = m_iIndexOfMesh;
						p2Adj.m_iAdjacentNavMeshIndex = m_iIndexOfMesh;
#endif
						iNumAdjacencies++;

						break;
					}

					lastv2 = v2;
				}

				if(v2 != pPoly2->GetNumVertices())
					break;
			}

			lastv1 = v1;
		}
	}

	//*************************************************************************************************
	// Now go through all faces and check that none have identical adjacency across 2 or more edges
	// NB : I've commented out this code for now, but it should remain here as it is a useful tool for
	// identifying hard-to-find errors..
	//*************************************************************************************************

#if __NAVMESH_SANITY_CHECK_ADJACENCIES

	SanityCheckAdjacencies();

#endif

	return iNumAdjacencies;
}

#if __NAVMESH_SANITY_CHECK_ADJACENCIES

void CNavMesh::SanityCheckAdjacencies()
{
	for(u32 t1=0; t1<m_iNumPolys; t1++)
	{
		TNavMeshPoly * pPoly = GetPoly(t1);

		TAdjPoly adjPoly1, adjPoly2, adjPoly3;
		adjPoly1 = GetAdjacentPoly( pPoly->GetFirstVertexIndex() + 0 );
		adjPoly2 = GetAdjacentPoly( pPoly->GetFirstVertexIndex() + 1 );
		adjPoly3 = GetAdjacentPoly( pPoly->GetFirstVertexIndex() + 2 );

		TNavMeshPoly * pOtherPoly1 = NULL;
		TNavMeshPoly * pOtherPoly2 = NULL;
		int iOtherPoly1Index = NAVMESH_POLY_INDEX_NONE;
		int iOtherPoly2Index = NAVMESH_POLY_INDEX_NONE;

		int badEdge1 = -1;
		int badEdge2 = -1;

		if(adjPoly1.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE && adjPoly2.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
		{
			if(adjPoly1.GetPolyIndex() == adjPoly2.GetPolyIndex())
			{
				badEdge1 = 0;
				badEdge2 = 1;

				iOtherPoly1Index = adjPoly1.GetPolyIndex();
				iOtherPoly2Index = adjPoly2.GetPolyIndex();

				pOtherPoly1 = GetPoly( adjPoly1.GetPolyIndex() );
				pOtherPoly2 = GetPoly( adjPoly2.GetPolyIndex() );
			}
		}
		else if(adjPoly1.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE && adjPoly3.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
		{
			if(adjPoly1.GetPolyIndex() == adjPoly3.GetPolyIndex())
			{
				badEdge1 = 0;
				badEdge2 = 2;

				iOtherPoly1Index = adjPoly1.GetPolyIndex();
				iOtherPoly2Index = adjPoly3.GetPolyIndex();

				pOtherPoly1 = GetPoly( adjPoly1.GetPolyIndex() );
				pOtherPoly2 = GetPoly( adjPoly3.GetPolyIndex() );
			}
		}
		else if(adjPoly2.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE && adjPoly3.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
		{
			if(adjPoly2.GetPolyIndex() == adjPoly3.GetPolyIndex())
			{
				badEdge1 = 1;
				badEdge2 = 2;

				iOtherPoly1Index = adjPoly2.GetPolyIndex();
				iOtherPoly2Index = adjPoly3.GetPolyIndex();

				pOtherPoly1 = GetPoly( adjPoly2.GetPolyIndex() );
				pOtherPoly2 = GetPoly( adjPoly3.GetPolyIndex() );
			}
		}

		if(badEdge1 != -1 && badEdge2 != -1)
		{
			printf("CNavMesh - poly is adjacent to another poly on > 1 edge.\n");

			if(t1 == (u32)iOtherPoly1Index)
				printf("ERROR - poly is adjacent to itself!.\n");
			if(t1 == (u32)iOtherPoly2Index)
				printf("ERROR - poly is adjacent to itself!.\n");

			printf("Poly %i,\t\t\t(%i, %i, %i)\n",
				t1,
				GetPolyVertexIndex(pPoly, 0),
				GetPolyVertexIndex(pPoly, 1),
				GetPolyVertexIndex(pPoly, 2)
				);
			printf("Poly1 %i [edge %i]\t(%i, %i, %i)\n",
				iOtherPoly1Index,
				badEdge1,
				GetPolyVertexIndex(pOtherPoly1, 0),
				GetPolyVertexIndex(pOtherPoly1, 1),
				GetPolyVertexIndex(pOtherPoly1, 2)
				);
			printf("Poly2 %i [edge %i]\t(%i, %i, %i)\n",
				iOtherPoly2Index,
				badEdge2,
				GetPolyVertexIndex(pOtherPoly2, 0),
				GetPolyVertexIndex(pOtherPoly2, 1),
				GetPolyVertexIndex(pOtherPoly2, 2)
				);
		}

		for(u32 a=0; a<pPoly->GetNumVertices(); a++)
		{
			TAdjPoly adj = GetAdjacentPoly( pPoly->GetFirstVertexIndex() + a );

			if(adj.GetNavMeshIndex(&m_AdjacentMeshes) == m_iIndexOfMesh && adj.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
			{
				TNavMeshPoly * pAdjPoly = GetPoly(adj.GetPolyIndex());
				int iFoundLinkBack = 0;
				for(u32 a2=0; a2<pAdjPoly->GetNumVertices(); a2++)
				{
					TAdjPoly linkBackAdj = GetAdjacentPoly( pAdjPoly->GetFirstVertexIndex() + a2 );
					if(linkBackAdj.GetNavMeshIndex(&m_AdjacentMeshes) == m_iIndexOfMesh && linkBackAdj.GetPolyIndex() == t1)
					{
						iFoundLinkBack++;
					}
				}
				Assert(iFoundLinkBack==1);
			}
		}
	}
}

#endif // __NAVMESH_SANITY_CHECK_ADJACENCIES

bool CNavMesh::GetPolyNormal(Vector3 & vNormal, const TNavMeshPoly * pPoly, const float fDotEps)
{
	Assert(pPoly->GetNavMeshIndex() == m_iIndexOfMesh);

	Vector3 v1,v2,v3;
	Vector3 vEdge1, vEdge2;
	u32 iStartVert=0;

	while(1)
	{
		GetVertex( GetPolyVertexIndex(pPoly, iStartVert), v1 );
		GetVertex( GetPolyVertexIndex(pPoly, (iStartVert+1) % pPoly->GetNumVertices()), v2 );
		GetVertex( GetPolyVertexIndex(pPoly, (iStartVert+2) % pPoly->GetNumVertices()), v3 );

		vEdge1 = v1 - v2;
		vEdge2 = v3 - v2;

		vEdge1.Normalize();
		vEdge2.Normalize();

		float fDot = DotProduct(vEdge1, vEdge2);

		// Watch out! This edge could be colinear with the last.
		if(Abs(fDot) > fDotEps) 
		{
			iStartVert++;
			// If we've been all round the poly verts then we're fucked.
			if(iStartVert==pPoly->GetNumVertices())
			{
				vNormal.Zero();
				//Assert(0);
				return false;
			}
			// Try from the next vertex
			continue;
		}

		vNormal = CrossProduct(vEdge2, vEdge1);
		vNormal.Normalize();
		return true;
	}
}

void CNavMesh::GetPolyEdgeNormal(Vector3 & vEdgeNormal, const TNavMeshPoly * pPoly, int iEdgeNum)
{
	Assert(pPoly->GetNavMeshIndex() == m_iIndexOfMesh);
	Assert(pPoly->GetFirstVertexIndex() + iEdgeNum < (s32)m_iSizeOfPools);

	Vector3 v1,v2;
	GetVertex( GetPolyVertexIndex(pPoly, iEdgeNum), v1 );
	GetVertex( GetPolyVertexIndex(pPoly, (iEdgeNum+1) % pPoly->GetNumVertices()), v2 );

	Vector3 vEdgeVec = v2-v1;
	vEdgeVec.Normalize();

	Vector3 vTmp;
	vTmp.Cross(vEdgeVec, Vector3(0.0f,0.0f,1.0f));
	vTmp.Normalize();

	Vector3 vUp3d;
	vUp3d.Cross(vEdgeVec, vTmp);
	vUp3d.Normalize();

	vEdgeNormal.Cross(vUp3d, vEdgeVec);
	vEdgeNormal.Normalize();
}

void CNavMesh::GetPolyEdgeMidpoint(Vector3 & vEdgeMidpoint, const TNavMeshPoly * pPoly, const int iEdgeNum)
{
	Vector3 vVert, vNextVert;
	GetVertex( GetPolyVertexIndex(pPoly, iEdgeNum), vVert);
	GetVertex( GetPolyVertexIndex(pPoly, (iEdgeNum+1) % pPoly->GetNumVertices()), vNextVert);
	vEdgeMidpoint = (vVert + vNextVert) * 0.5f;
}
int CNavMesh::GetPolyClosestEdgeMidToPoint(const Vector3 & vInputPoint, const TNavMeshPoly * pPoly, Vector3 * pOutEdgeMidVec)
{
	Assert(pPoly->GetNavMeshIndex()==m_iIndexOfMesh);

	const u32 iNumPts = pPoly->GetNumVertices();
	float fClosestDist = FLT_MAX;
	s32 iClosestEdge = -1;

	Vector3 vVert, vNextVert;
	for(u32 v=0; v<iNumPts; v++)
	{
		GetVertex( GetPolyVertexIndex(pPoly, v), vVert);
		GetVertex( GetPolyVertexIndex(pPoly, (v+1) % pPoly->GetNumVertices()), vNextVert);		
		const Vector3 vEdgeMidpoint = (vVert + vNextVert) * 0.5f;
		const float fDist2 = (vEdgeMidpoint - vInputPoint).Mag2();
		if(fDist2 < fClosestDist)
		{
			fClosestDist = fDist2;
			iClosestEdge = v;
			if(pOutEdgeMidVec)
				*pOutEdgeMidVec = vEdgeMidpoint;
		}
	}
	return iClosestEdge;
}
int CNavMesh::GetPolyClosestEdgePointToPoint(const Vector3 & vInputPoint, const TNavMeshPoly * pPoly, Vector3 & vOutClosestPointOnEdgeVec)
{
	Assert(pPoly->GetNavMeshIndex()==m_iIndexOfMesh);

	Vec3V vOutPos;
	const u32 iNumPts = pPoly->GetNumVertices();
	float fClosestDist = FLT_MAX;
	s32 iClosestEdge = -1;

	Vector3 vVert, vNextVert;
	for(u32 v=0; v<iNumPts; v++)
	{
		GetVertex( GetPolyVertexIndex(pPoly, v), vVert);
		GetVertex( GetPolyVertexIndex(pPoly, (v+1) % pPoly->GetNumVertices()), vNextVert);

		Vector3 vEdgeVec = vNextVert-vVert;

		const float T = geomTValues::FindTValueSegToPoint(RCC_VEC3V(vVert), RCC_VEC3V(vEdgeVec), RCC_VEC3V(vInputPoint)).Getf();
		const Vector3 vEdgePoint = vVert + (vEdgeVec * T);
		const float fDist2 = (vEdgePoint - vInputPoint).Mag2();
		if(fDist2 < fClosestDist)
		{
			fClosestDist = fDist2;
			iClosestEdge = v;
			vOutClosestPointOnEdgeVec = vEdgePoint;
		}
	}
	return iClosestEdge;
}

#if __DEV
u16 CNavMesh::GetPolyVertexIndex(const TNavMeshPoly * pPoly, int iPolyVertex) const
{
	Assert(pPoly->GetNavMeshIndex() == (TNavMeshIndex)m_iIndexOfMesh);
	Assert(pPoly->GetFirstVertexIndex() + iPolyVertex < (s32)m_iSizeOfPools);

	return *m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex() + iPolyVertex);
}
#endif

#ifdef NAVGEN_TOOL
void CNavMesh::SetPolysSubArrayIndices()
{
	for(u32 p=0; p<m_iNumPolys; p++)
	{
		const u32 iSubArray = m_PolysArray->GetSubArrayIndex(p);
		Assert(iSubArray <= 255);
		GetPoly(p)->m_Struct4.m_iPolyArrayIndex = iSubArray;
	}
}
#endif

float
CNavMesh::GetPolyArea(u32 iPolyIndex) const
{
	TNavMeshPoly * pPoly = GetPoly(iPolyIndex);

	Vector3 v0, v1, v2;
	Vector3 e0, e1, e2;
	float l0, l1, l2;
	float fArea = 0.0f;

	Vector3 vCentroid;
	GetPolyCentroid(iPolyIndex, v0);

	GetVertex( GetPolyVertexIndex(pPoly, pPoly->GetNumVertices()-1), v1);

	for(u32 v=0; v<pPoly->GetNumVertices(); v++)
	{
		GetVertex( GetPolyVertexIndex(pPoly, v), v2);

		e0 = v0 - v2;
		e1 = v1 - v0;
		e2 = v2 - v1;

		l0 = e0.Mag();
		l1 = e1.Mag();
		l2 = e2.Mag();

		const float p = l0 + l1 + l2;
		const float val = p * (p - (2.0f * l0)) * (p - (2.0f * l1)) * (p - (2.0f * l2));
		if(val > 0.0001f)
		{
			const float a = 0.25f * sqrtf(val);
			fArea += a;
		}

		v1 = v2;
	}

	return fArea;
}

void
CNavMesh::SetPolySizeFlags(u32 iPolyIndex, float fSmallSize, float fLargeSize)
{
	TNavMeshPoly * pPoly = GetPoly(iPolyIndex);

	pPoly->SetIsSmall(false);
	pPoly->SetIsLarge(false);

	float fArea = GetPolyArea(iPolyIndex);

	if(fArea < fSmallSize)
		pPoly->SetIsSmall(true);
	else if(fArea > fLargeSize)
		pPoly->SetIsLarge(true);
}

void
CNavMesh::SetPolysSizeFlags(float fSmallSize, float fLargeSize)
{
	for(u32 p=0; p<m_iNumPolys; p++)
	{
		SetPolySizeFlags(p, fSmallSize, fLargeSize);
	}
}

void CNavMesh::SetTessellatedTriangleSizeFlags(TNavMeshPoly * pPoly)
{
	Vector3 v0, v1, v2;
	GetVertex( GetPolyVertexIndex(pPoly, 0), v0);
	GetVertex( GetPolyVertexIndex(pPoly, 1), v1);
	GetVertex( GetPolyVertexIndex(pPoly, 2), v2);

	if( (v0-v2).Mag2()>ms_fSmallestTessellatedEdgeThresholdSqr ||
		(v1-v0).Mag2()>ms_fSmallestTessellatedEdgeThresholdSqr ||
		(v2-v1).Mag2()>ms_fSmallestTessellatedEdgeThresholdSqr )
	{
		pPoly->SetIsSmall(false);
		pPoly->SetIsLarge(true);
	}
	else
	{
		pPoly->SetIsSmall(true);
		pPoly->SetIsLarge(false);
	}
}


//******************************************************************************************
// Connects the polys of this mesh to the polys in the other mesh (one way only)
//******************************************************************************************

void CNavMesh::StitchEdgePolysAcrossMeshes(CNavMesh & otherNavMesh)
{
	u32 t1, t2;
	u32 iPoly1EdgeFlags, iPoly2EdgeFlags;
	TNavMeshPoly * pPoly1, * pPoly2;
	//TAdjPoly adjPoly;

	static const float fSameVectorDot = cosf( DtoR * 2.0f );

	static const Vector3 vXAxis(1.0f, 0.0f, 0.0f);
	static const Vector3 vYAxis(0.0f, 1.0f, 0.0f);

	for(t1=0; t1<m_iNumPolys; t1++)
	{
		pPoly1 = GetPoly(t1);
		if(pPoly1->GetIsDisabled())
			continue;

		if(!pPoly1->GetLiesAlongEdgeOfMesh())
			continue;

		iPoly1EdgeFlags = GetPolyEdgeFlags(this, t1);


		for(t2=0; t2<otherNavMesh.m_iNumPolys; t2++)
		{
			pPoly2 = otherNavMesh.GetPoly(t2);

			if(!pPoly2->GetLiesAlongEdgeOfMesh())
			{
				continue;
			}

			iPoly2EdgeFlags = GetPolyEdgeFlags(&otherNavMesh, t2);

			// See whether the map edges which these triangles are on present the
			// possibility that they may match up..
			int iNumPossibleSharedEdges = 0;

			if((iPoly1EdgeFlags & EDGEFLAG_NEG_X) && (iPoly2EdgeFlags & EDGEFLAG_POS_X))
				iNumPossibleSharedEdges++;

			if((iPoly1EdgeFlags & EDGEFLAG_POS_X) && (iPoly2EdgeFlags & EDGEFLAG_NEG_X))
				iNumPossibleSharedEdges++;

			if((iPoly1EdgeFlags & EDGEFLAG_NEG_Y) && (iPoly2EdgeFlags & EDGEFLAG_POS_Y))
				iNumPossibleSharedEdges++;

			if((iPoly1EdgeFlags & EDGEFLAG_POS_Y) && (iPoly2EdgeFlags & EDGEFLAG_NEG_Y))
				iNumPossibleSharedEdges++;

			if(!iNumPossibleSharedEdges)
				continue;

			// Okay, these triangles are at least bordering a shared nav-mesh edge
			// now lets do a comprehensive test to find the shared vertices..
			// If we find a shared edge, we'll only fill in the adjacency for pTri1..

			u32 v1, lastv1, v2, lastv2;
			Vector3 pVertex1, pLastVertex1, pVertex2, pLastVertex2;
			Vector3 vEdge1Vec, vEdge2Vec;
			float fTri1_MinExtent, fTri1_MaxExtent;
			float fTri2_MinExtent, fTri2_MaxExtent;
			float fTri1_MinHeight, fTri1_MaxHeight;
			float fTri2_MinHeight, fTri2_MaxHeight;
			float fTri1_ClosePoint, fTri2_ClosePoint;
			float fDot;
			int iCardinalAxis = 0;

			enum
			{
				XAxis = 1,
				YAxis = 2
			};

			lastv1 = pPoly1->GetNumVertices()-1;
			for(v1=0; v1<pPoly1->GetNumVertices(); v1++)
			{
				const TAdjPoly & adjPoly = GetAdjacentPoly( pPoly1->GetFirstVertexIndex()+lastv1 );
				if(adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
				{
					lastv1 = v1;
					continue;
				}

				GetVertex( GetPolyVertexIndex(pPoly1, v1), pVertex1);
				GetVertex( GetPolyVertexIndex(pPoly1, lastv1), pLastVertex1);

				vEdge1Vec = pVertex1 - pLastVertex1;
				vEdge1Vec.z = 0;

				vEdge1Vec.Normalize();

				// This edge must be along a cardinal axis..
				iCardinalAxis = 0;

				fDot = Abs(Dot(vEdge1Vec, vXAxis));
				if(fDot >= fSameVectorDot) iCardinalAxis = XAxis;
				fDot = Abs(Dot(vEdge1Vec, vYAxis));
				if(fDot >= fSameVectorDot) iCardinalAxis = YAxis;

				if(iCardinalAxis)
				{
					lastv2 = pPoly2->GetNumVertices()-1;
					for(v2=0; v2<pPoly2->GetNumVertices(); v2++)
					{
						otherNavMesh.GetVertex( otherNavMesh.GetPolyVertexIndex(pPoly2, v2), pVertex2);
						otherNavMesh.GetVertex( otherNavMesh.GetPolyVertexIndex(pPoly2, lastv2), pLastVertex2);

						vEdge2Vec = pVertex2 - pLastVertex2;
						vEdge2Vec.z = 0;

						vEdge2Vec.Normalize();

						// Are the 2 edge vectors very similar ?
						fDot = Dot(vEdge1Vec, vEdge2Vec);
						if(Abs(fDot) < fSameVectorDot)
						{
							lastv2 = v2;
							continue;
						}

						// Now lets test that the triangle edge overlaps in the cardinal axis
						// with a simple extents test.  If so then at least we can walk from
						// one triangle to another (assuming they're at the same height).
						// We'll also test whether the edge points overlap in Z.

						switch(iCardinalAxis)
						{
							case XAxis:
								fTri1_MinExtent = Min(pVertex1.x, pLastVertex1.x);
								fTri1_MaxExtent = Max(pVertex1.x, pLastVertex1.x);
								fTri2_MinExtent = Min(pVertex2.x, pLastVertex2.x);
								fTri2_MaxExtent = Max(pVertex2.x, pLastVertex2.x);
								fTri1_ClosePoint = pVertex1.y;
								fTri2_ClosePoint = pVertex2.y;
								break;
							case YAxis:
								fTri1_MinExtent = Min(pVertex1.y, pLastVertex1.y);
								fTri1_MaxExtent = Max(pVertex1.y, pLastVertex1.y);
								fTri2_MinExtent = Min(pVertex2.y, pLastVertex2.y);
								fTri2_MaxExtent = Max(pVertex2.y, pLastVertex2.y);
								fTri1_ClosePoint = pVertex1.x;
								fTri2_ClosePoint = pVertex2.x;
								break;
							default:
								lastv2 = v2;
								continue;
						}

						// No overlap is possible along the adjoining axis ?
						if(fTri1_MaxExtent <= fTri2_MinExtent || fTri1_MinExtent >= fTri2_MaxExtent)
						{
							lastv2 = v2;
							continue;
						}

						float fTri1EdgeLength = fTri1_MaxExtent - fTri1_MinExtent;
						float fTri2EdgeLength = fTri2_MaxExtent - fTri2_MinExtent;

						// Get the actual overlap size
						float fOverlapSize = Min(fTri1_MaxExtent, fTri2_MaxExtent) - Max(fTri1_MinExtent, fTri2_MinExtent);

						static const float fLateralProximity = 0.2f;

						// Edges are too far apart laterally ?
						if(!Eq(fTri1_ClosePoint, fTri2_ClosePoint, fLateralProximity))
						{
							lastv2 = v2;
							continue;
						}

						fTri1_MinHeight = Min(pVertex1.z, pLastVertex1.z);
						fTri1_MaxHeight = Max(pVertex1.z, pLastVertex1.z);
						fTri2_MinHeight = Min(pVertex2.z, pLastVertex2.z);
						fTri2_MaxHeight = Max(pVertex2.z, pLastVertex2.z);

						static const float fHeightTolerance = 1.0f;	//0.1f;

						// Is no overlap is possible vertically ?
						if(fTri1_MaxHeight < (fTri2_MinHeight - fHeightTolerance) || (fTri1_MinHeight - fHeightTolerance) > fTri2_MaxHeight)
						{
							lastv2 = v2;
							continue;
						}

						// Ensure that the two vertices from poly2 lie close to the plane of poly1
						/*
						// NB: Removed this because it was causing stitching between adjacent navmeshes
						// to sometimes fail.  If you think about it, there should be no requirement that
						// adjacent joined polys have similar normals.
						if(bGotNormal)
						{
							static const float fMaxDist = 0.25f;
							const float fD1 = DotProduct(vPoly1_Normal, pVertex2) + fPoly1_PlaneDist;
							const float fD2 = DotProduct(vPoly1_Normal, pLastVertex2) + fPoly1_PlaneDist;

							if(rage::Abs(fD1) > fMaxDist || rage::Abs(fD2) > fMaxDist)
							{
								lastv2 = v2;
								continue;
							}
						}
						*/

						// What we should do here, is record the extent of the overlap in the
						// common cardinal axis - and then continue until we've found the largest
						// overlap.  This will make the adjacency more accurate.
						// However for now we'll just go with the first adjacent triangle we find

						const bool bEdgesMatch =
							IsClose(fTri1EdgeLength, fOverlapSize, 0.05f) &&
							IsClose(fTri2EdgeLength, fOverlapSize, 0.05f);

						if(bEdgesMatch)
						{
							TAdjPoly * pAdj = m_AdjacentPolysArray->Get(pPoly1->GetFirstVertexIndex() + lastv1);
							pAdj->SetNavMeshIndex(otherNavMesh.m_iIndexOfMesh, GetAdjacentMeshes());
							pAdj->SetPolyIndex(t2);
							pAdj->SetOriginalNavMeshIndex(otherNavMesh.m_iIndexOfMesh, GetAdjacentMeshes());
							pAdj->SetOriginalPolyIndex(t2);

#if defined(NAVGEN_TOOL) && !defined(RAGEBUILDER)
							pAdj->m_iAdjacentNavMeshIndex = otherNavMesh.m_iIndexOfMesh;
#endif
							break;
						}

						lastv2 = v2;
					}

				} // iCardinalAxis

				lastv1 = v1;
			}
		}
	}
}

// NAME : GetSpecialLinksOnPoly
// PURPOSE : Obtain the special links within this navmesh, starting or ending on the given polygon
// The reason that we pass in an "iNavMesh" index to use, instead of using m_iIndexOfMesh, is that special-links
// can get retargetted during tessellation that occurs whilst pathfinding - and may therefore point to the
// navmesh NAVMESH_INDEX_TESSELLATION.
s32 CNavMesh::GetSpecialLinksOnPoly(const TNavMeshIndex iNavMesh, const u16 iPolyIndex, CSpecialLinkInfo ** ppOut_SpecialLinks, const int iMaxNum, const u32 iAStarTimeStamp)
{
	if(m_iNumSpecialLinks==0 || iMaxNum==0)
		return 0;

	TNavMeshPoly * pPoly = GetPoly(iPolyIndex);

	s32 iNum = 0;
	s32 iLinkLookupIndex = pPoly->GetSpecialLinksStartIndex();

	for(s32 s=0; s<pPoly->GetNumSpecialLinks(); s++)
	{
		u16 iLinkIndex = GetSpecialLinkIndex(iLinkLookupIndex++);
		Assert(iLinkIndex < GetNumSpecialLinks());
		CSpecialLinkInfo & sp = GetSpecialLinks()[iLinkIndex];

		if(sp.GetAStarTimeStamp() != iAStarTimeStamp)
		{
			sp.Reset();
			sp.SetAStarTimeStamp((u16)iAStarTimeStamp);
		}

		if(sp.GetLinkFromNavMesh()==iNavMesh && sp.GetLinkFromPoly()==iPolyIndex)
		{
			ppOut_SpecialLinks[iNum++] = &sp;
			if(iNum==iMaxNum)
				break;
		}
		else if(sp.GetLinkToNavMesh()==iNavMesh && sp.GetLinkToPoly()==iPolyIndex)
		{
			ppOut_SpecialLinks[iNum++] = &sp;
			if(iNum==iMaxNum)
				break;
		}
	}
	return iNum;
}

// eSwitch is a combination of flags from the ESWITCH_NAVMESH_POLYS enumeration

void CNavMesh::SwitchAllPolysOnOffForPeds(const u32 eSwitch)
{
	Assert( !((eSwitch&SWITCH_NAVMESH_ENABLE_AMBIENT_PEDS) && (eSwitch&SWITCH_NAVMESH_DISABLE_AMBIENT_PEDS)) );
	Assert( !((eSwitch&SWITCH_NAVMESH_ENABLE_POLYGONS) && (eSwitch&SWITCH_NAVMESH_DISABLE_POLYGONS)) );

	u32 p;
	for(p=0; p<m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = GetPoly(p);

		if((eSwitch & SWITCH_NAVMESH_ENABLE_AMBIENT_PEDS)!=0)
			pPoly->AndFlags(~NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS);

		if((eSwitch & SWITCH_NAVMESH_DISABLE_AMBIENT_PEDS)!=0)
			pPoly->OrFlags(NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS);

		if((eSwitch & SWITCH_NAVMESH_ENABLE_POLYGONS)!=0)
			pPoly->AndFlags(~NAVMESHPOLY_DISABLED);

		if((eSwitch & SWITCH_NAVMESH_DISABLE_POLYGONS)!=0)
			pPoly->OrFlags(NAVMESHPOLY_DISABLED);
	}	
}

// Switched polys in the area on/off for ped generation and ped wandering.
// This is done by setting/reset the NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS flag.

TShortMinMax gSwitchPolysMinMax;

void CNavMesh::SwitchPolysInArea(const Vector3 & vMin, const Vector3 & vMax, const u32 eSwitch)
{
	Assert( !((eSwitch&SWITCH_NAVMESH_ENABLE_AMBIENT_PEDS) && (eSwitch&SWITCH_NAVMESH_DISABLE_AMBIENT_PEDS)) );
	Assert( !((eSwitch&SWITCH_NAVMESH_ENABLE_POLYGONS) && (eSwitch&SWITCH_NAVMESH_DISABLE_POLYGONS)) );

	Assert(m_pQuadTree);
	if(!m_pQuadTree)
	{
		return;
	}
	if(vMin.x >= m_pQuadTree->m_Maxs.x || vMin.y >= m_pQuadTree->m_Maxs.y || vMin.z >= m_pQuadTree->m_Maxs.z ||
		vMax.x <= m_pQuadTree->m_Mins.x || vMax.y <= m_pQuadTree->m_Mins.y || vMax.z <= m_pQuadTree->m_Mins.z)
	{
		// No intersection of regions
		return;
	}

	gSwitchPolysMinMax.SetFloat(vMin.x, vMin.y, vMin.z, vMax.x, vMax.y, vMax.z);
	SwitchPolysInAreaInQuadree(m_pQuadTree, vMin, vMax, eSwitch);
}

void CNavMesh::SwitchPolysInAreaInQuadree(const CNavMeshQuadTree * pTree, const Vector3 & vMin, const Vector3 & vMax, const u32 eSwitch)
{
	Assert( !((eSwitch&SWITCH_NAVMESH_ENABLE_AMBIENT_PEDS) && (eSwitch&SWITCH_NAVMESH_DISABLE_AMBIENT_PEDS)) );
	Assert( !((eSwitch&SWITCH_NAVMESH_ENABLE_POLYGONS) && (eSwitch&SWITCH_NAVMESH_DISABLE_POLYGONS)) );

	if(pTree->m_pLeafData)
	{
		u16 iIndex;
		TNavMeshPoly * pPoly;
		Vector3 vCentroid;

		for(int p=0; p<pTree->m_pLeafData->m_iNumPolys; p++)
		{
			iIndex = pTree->m_pLeafData->m_Polys[p];
			pPoly = GetPoly(iIndex);

			if(gSwitchPolysMinMax.Intersects(pPoly->m_MinMax))
			{
				if((eSwitch & SWITCH_NAVMESH_ENABLE_AMBIENT_PEDS)!=0)
					pPoly->AndFlags(~NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS);

				if((eSwitch & SWITCH_NAVMESH_DISABLE_AMBIENT_PEDS)!=0)
					pPoly->OrFlags(NAVMESHPOLY_SWITCHED_OFF_FOR_AMBIENT_PEDS);

				if((eSwitch & SWITCH_NAVMESH_ENABLE_POLYGONS)!=0)
					pPoly->AndFlags(~NAVMESHPOLY_DISABLED);

				if((eSwitch & SWITCH_NAVMESH_DISABLE_POLYGONS)!=0)
					pPoly->OrFlags(NAVMESHPOLY_DISABLED);
			}
		}

		return;
	}

	for(int c=0; c<4; c++)
	{
		// Early-out if no bbox intersection between quadtree node & min/max of area
		if(vMax.x < pTree->m_pChildren[c]->m_Mins.x || vMax.y < pTree->m_pChildren[c]->m_Mins.y ||
			vMin.x > pTree->m_pChildren[c]->m_Maxs.x || vMin.y > pTree->m_pChildren[c]->m_Maxs.y)
		{
			continue;
		}
		else
		{
			SwitchPolysInAreaInQuadree(pTree->m_pChildren[c], vMin, vMax, eSwitch);
		}
	}
}

// Do some preprocessing on a newly loaded quadtree.
void CNavMesh::PreProcessNavMeshQuadTree()
{
	Assert(m_pQuadTree);
	Assert(m_pNonResourcedData);

	m_pNonResourcedData->m_vMinOfNavMesh = m_pQuadTree->m_Mins;

#if 0	// TODO: Strip out.
	if(m_pQuadTree)
		PreProcessNavMeshQuadTree(m_pQuadTree);
#endif
}

#if 0	// Unused. TODO: Strip this out later if we don't need it back. /FF

void CNavMesh::PreProcessNavMeshQuadTree(CNavMeshQuadTree * pTree)
{
	if(pTree->m_pLeafData)
	{
		s32 iMaxPedDensity = 0;
		s32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;
		s32 c;
		for(c=0; c<iNumPolys; c++)
		{
			u32 iPolyIndex = pTree->m_pLeafData->m_Polys[c];
			TNavMeshPoly * pPoly = &m_Polys[iPolyIndex];

			if(pPoly->GetPedDensity() > iMaxPedDensity)
			{
				iMaxPedDensity = pPoly->GetPedDensity();
			}
		}

		pTree->m_pLeafData->m_iMaxPedDensityOfAnyPolyInThisLeaf = (u8) iMaxPedDensity;
	}
	else
	{
		s32 c;
		for(c=0; c<4; c++)
		{
			Assert(pTree->m_pChildren[c]);

			if(pTree->m_pChildren[c])
				PreProcessNavMeshQuadTree(pTree->m_pChildren[c]);
		}
	}
}

#endif	// 0



#if __ISECT_RAY_USE_MINMAX
TShortMinMax g_RayMinMax;
#endif

bool CNavMesh::IntersectRay(const Vector3 & vRayStart, const Vector3 & vRayEnd, Vector3 & vecIntersect, float & fIntersectDist, u16 & iHitPolyIndex)
{
	m_vIntersectRayStart = vRayStart;
	m_vIntersectRayEnd = vRayEnd;

	m_vIntersectRayMins.x = Min(m_vIntersectRayStart.x, m_vIntersectRayEnd.x);
	m_vIntersectRayMins.y = Min(m_vIntersectRayStart.y, m_vIntersectRayEnd.y);
	m_vIntersectRayMins.z = Min(m_vIntersectRayStart.z, m_vIntersectRayEnd.z);

	m_vIntersectRayMaxs.x = Max(m_vIntersectRayStart.x, m_vIntersectRayEnd.x);
	m_vIntersectRayMaxs.y = Max(m_vIntersectRayStart.y, m_vIntersectRayEnd.y);
	m_vIntersectRayMaxs.z = Max(m_vIntersectRayStart.z, m_vIntersectRayEnd.z);

#if __ISECT_RAY_USE_MINMAX
	g_RayMinMax.SetFloat(m_vIntersectRayMins.x - TShortMinMax::ms_fResolution, m_vIntersectRayMins.y - TShortMinMax::ms_fResolution, m_vIntersectRayMins.z - TShortMinMax::ms_fResolution, m_vIntersectRayMaxs.x + TShortMinMax::ms_fResolution, m_vIntersectRayMaxs.y + TShortMinMax::ms_fResolution, m_vIntersectRayMaxs.z + TShortMinMax::ms_fResolution);
#endif

    m_fClosestIntersectDistSqr = FLT_MAX;
	m_iClosestIntersectPolyIndex = NAVMESH_POLY_INDEX_NONE;

	IntersectRayR(m_pQuadTree);

	if(m_iClosestIntersectPolyIndex != NAVMESH_POLY_INDEX_NONE)
	{
		vecIntersect = m_fClosestIntersectPos;
		fIntersectDist = sqrtf(m_fClosestIntersectDistSqr);
		iHitPolyIndex = m_iClosestIntersectPolyIndex;

		return true;
	}

	return false;
} 


void CNavMesh::IntersectRayR(const CNavMeshQuadTree * pTree)
{
	if(pTree->m_pLeafData)
	{
		Vector3 vIsectVec;

		for(int p=0; p<pTree->m_pLeafData->m_iNumPolys; p++)
		{
			u16 iIndex = pTree->m_pLeafData->m_Polys[p];
			TNavMeshPoly * pPoly = GetPoly(iIndex);
			if(pPoly->GetIsDisabled())
				continue;

#if __ISECT_RAY_USE_MINMAX
			if(!g_RayMinMax.Intersects(pPoly->m_MinMax))
				continue;
#endif

			if(RayIntersectsPoly(m_vIntersectRayStart, m_vIntersectRayEnd, pPoly, vIsectVec))
			{
				float fDistSqr = (vIsectVec - m_vIntersectRayStart).Mag2();
				if(fDistSqr < m_fClosestIntersectDistSqr)
				{
					m_fClosestIntersectDistSqr = fDistSqr;
					m_iClosestIntersectPolyIndex = iIndex;
					m_fClosestIntersectPos = vIsectVec;
				}
			}
		}
		return;
	}

	for(int c=0; c<4; c++)
	{
		// Early-out if no bbox intersection between quadtree node & min/max of ray
		if(m_vIntersectRayMaxs.x < pTree->m_pChildren[c]->m_Mins.x || m_vIntersectRayMaxs.y < pTree->m_pChildren[c]->m_Mins.y ||
			m_vIntersectRayMins.x > pTree->m_pChildren[c]->m_Maxs.x || m_vIntersectRayMins.y > pTree->m_pChildren[c]->m_Maxs.y)
		{
			continue;
		}
		else
		{
			IntersectRayR(pTree->m_pChildren[c]);
		}
	}
}

void CNavMesh::CalculateAllPolyMinMaxesForDynamicNavMesh()
{
	static const float fAngleEps = 0.9f;
	static const float fDistEps = 0.2f;
	float fUpDot = DotProduct(m_pNonResourcedData->m_DynamicNavMeshLastUpdateMatrix.c, m_Matrix.c);
	float fRightDot = DotProduct(m_pNonResourcedData->m_DynamicNavMeshLastUpdateMatrix.a, m_Matrix.a);

	bool bUpdateNeeded = (fUpDot < fAngleEps);
	if(!bUpdateNeeded)
		bUpdateNeeded = (fRightDot < fAngleEps);
	if(!bUpdateNeeded)
		bUpdateNeeded = !m_Matrix.d.IsClose(m_pNonResourcedData->m_DynamicNavMeshLastUpdateMatrix.d, fDistEps);

	if(bUpdateNeeded)
	{
		for(u32 p=0; p<m_iNumPolys; p++)
		{
			TNavMeshPoly * pPoly = GetPoly(p);
			if(pPoly->GetIsDisabled())
				continue;
			CalculatePolyMinMaxForDynamicNavMesh(pPoly);
		}

		m_pNonResourcedData->m_DynamicNavMeshLastUpdateMatrix = m_Matrix;
	}
}

void CNavMesh::CalculatePolyMinMaxForDynamicNavMesh(TNavMeshPoly * pPoly)
{
	Assert(m_iFlags & NAVMESH_IS_DYNAMIC);
	Assert(pPoly->GetNavMeshIndex() >= NAVMESH_INDEX_FIRST_DYNAMIC && pPoly->GetNavMeshIndex() <= NAVMESH_INDEX_FINAL_DYNAMIC);

	Vector3 vPolyVert;
	float fPolyMinX = 32000.0f;
	float fPolyMinY = 32000.0f;
	float fPolyMinZ = 32000.0f;
	float fPolyMaxX = -32000.0f;
	float fPolyMaxY = -32000.0f;
	float fPolyMaxZ = -32000.0f;

	int iNumVerts = pPoly->GetNumVertices();
	int v;
	u16 iVertIndex;

	for(v=0; v<iNumVerts; v++)
	{
		iVertIndex = GetPolyVertexIndex(pPoly, v);
		GetVertex(iVertIndex, vPolyVert);

		if(vPolyVert.x < fPolyMinX) fPolyMinX = vPolyVert.x;
		if(vPolyVert.x > fPolyMaxX) fPolyMaxX = vPolyVert.x;
		if(vPolyVert.y < fPolyMinY) fPolyMinY = vPolyVert.y;
		if(vPolyVert.y > fPolyMaxY) fPolyMaxY = vPolyVert.y;
		if(vPolyVert.z < fPolyMinZ) fPolyMinZ = vPolyVert.z;
		if(vPolyVert.z > fPolyMaxZ) fPolyMaxZ = vPolyVert.z;
	}

	// Quick short minmax extents
	pPoly->m_MinMax.SetFloat(fPolyMinX, fPolyMinY, fPolyMinZ, fPolyMaxX, fPolyMaxY, fPolyMaxZ);
}


void CNavMesh::SaveQuadtree(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, fiStream * stream)
{
	fwrite(&pTree->m_Mins.x, sizeof(float), 1, stream);
	fwrite(&pTree->m_Mins.y, sizeof(float), 1, stream);
	fwrite(&pTree->m_Mins.z, sizeof(float), 1, stream);
	fwrite(&pTree->m_Maxs.x, sizeof(float), 1, stream);
	fwrite(&pTree->m_Maxs.y, sizeof(float), 1, stream);
	fwrite(&pTree->m_Maxs.z, sizeof(float), 1, stream);

	// Write u8 to indicate if this is a leaf node
	u8 bLeafData = (pTree->m_pLeafData != NULL);
	fwrite(&bLeafData, sizeof(u8), 1, stream);

	if(bLeafData)
	{
		// Write num polys in this leaf
		fwrite(&pTree->m_pLeafData->m_iNumPolys, sizeof(u16), 1, stream);

		// Write the poly indices in this leaf
		for(int i=0; i<pTree->m_pLeafData->m_iNumPolys; i++)
		{
			fwrite(&pTree->m_pLeafData->m_Polys[i], sizeof(u16), 1, stream);
		}

		// Write num cover points in this leaf
		fwrite(&pTree->m_pLeafData->m_iNumCoverPoints, sizeof(u16), 1, stream);

		// Write the cover points in this leaf
		for(int i=0; i<pTree->m_pLeafData->m_iNumCoverPoints; i++)
		{
			// The position of the coverpoint
			fwrite(&pTree->m_pLeafData->m_CoverPoints[i].m_iX, sizeof(u16), 1, stream);
			fwrite(&pTree->m_pLeafData->m_CoverPoints[i].m_iY, sizeof(u16), 1, stream);
			fwrite(&pTree->m_pLeafData->m_CoverPoints[i].m_iZ, sizeof(u16), 1, stream);

			// The usage flags, etc
			u16 iAsInt = pTree->m_pLeafData->m_CoverPoints[i].m_CoverPointFlags.GetAsInteger();
			u16 * iCoverPointFlags = (u16*)&iAsInt;
			fwrite(iCoverPointFlags, sizeof(u16), 1, stream);
		}
	}
	else
	{
		// Write the child nodes
		for(int c=0; c<4; c++)
		{
			SaveQuadtree(pNavMesh, pTree->m_pChildren[c], stream);
		}
	}
}

bool CNavMesh::SaveBinary(CNavMesh * pNavMesh, char * pFileName)
{
	if(!pNavMesh || !pFileName || !*pFileName)
		return false;

	fiStream * stream = fiStream::Create(pFileName);
	if(!stream)
	{
		Errorf("fiStream::Create(%s) was unsuccessful.", pFileName);
		return false;
	}

	// Write "NAVM" header
	char header[] = "NAVM";
	stream->Write(&header, sizeof(char) * 4);

	// Write the file version
	stream->Write(&ms_iFileVersion, sizeof(u32));
	pNavMesh->m_iFileVersion = ms_iFileVersion;

	// Write the flags
	stream->Write(&pNavMesh->m_iFlags, sizeof(u32));

	// Write the build ID
	stream->Write(&pNavMesh->m_iBuildID, sizeof(u32));

	// Write index of the mesh
	stream->Write(&pNavMesh->m_iIndexOfMesh, sizeof(u32));

	// Write the adjacent meshes
	stream->Write(&pNavMesh->m_AdjacentMeshes.m_iNumIndices, sizeof(u32));
	for(s32 a=0; a<CAdjacentNavmeshLookups::ms_iMaxAdjacentNavMeshes; a++)
		stream->Write(&pNavMesh->m_AdjacentMeshes.m_iNavmeshIndices[a], sizeof(u32));

	// Write the quadtree structure
	SaveQuadtree(pNavMesh, pNavMesh->m_pQuadTree, stream);

	// Write num vertices & polys
	stream->Write(&pNavMesh->m_iNumVertices, sizeof(u32));
	stream->Write(&pNavMesh->m_iNumPolys, sizeof(u32));

	// Write vertices
	if(pNavMesh->m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
	{
		for(u32 v=0; v<pNavMesh->m_iNumVertices; v++)
		{
			CNavMeshCompressedVertex * vert = pNavMesh->GetCompressedVertexArray().Get(v);
			stream->Write(&vert->data[0], sizeof(u16));
			stream->Write(&vert->data[1], sizeof(u16));
			stream->Write(&vert->data[2], sizeof(u16));
			//stream->Write(&pNavMesh->m_CompressedVertexData[v*3], sizeof(u16));
			//stream->Write(&pNavMesh->m_CompressedVertexData[(v*3)+1], sizeof(u16));
			//stream->Write(&pNavMesh->m_CompressedVertexData[(v*3)+2], sizeof(u16));
		}
	}
	else
	{
		for(u32 v=0; v<pNavMesh->m_iNumVertices; v++)
		{
			stream->Write(&pNavMesh->m_VertexPool[v].x, sizeof(float));
			stream->Write(&pNavMesh->m_VertexPool[v].y, sizeof(float));
			stream->Write(&pNavMesh->m_VertexPool[v].z, sizeof(float));
		}

	}

	// Write the size of the pools
	stream->Write(&pNavMesh->m_iSizeOfPools, sizeof(u32));

	// Write polys
	u8 flags, numVertices;
	u16 firstVertexIndex;
	u32 iStruct3;
	u8 coverDirections;

	for(u32 t=0; t<pNavMesh->m_iNumPolys; t++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(t);
		flags = (u8)(pPoly->GetFlags() & 0xFF);	// NB : We only save the bottom 8 bits of the flags!
		stream->Write(&flags, sizeof(u8));

		numVertices = (u8)pPoly->GetNumVertices();
		iStruct3 = pPoly->GetStruct3AsInteger();
		coverDirections = (u8)pPoly->GetCoverDirections();

		stream->Write(&numVertices, sizeof(u8));

		stream->Write(&iStruct3, sizeof(u32));

		stream->Write(&coverDirections, sizeof(u8));

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
		stream->Write(&pPoly->m_iNonResourcedAsInteger, sizeof(u32));
#endif
#endif
		firstVertexIndex = (u16)pPoly->GetFirstVertexIndex();
		stream->Write(&firstVertexIndex, sizeof(u16));

		// Write vertex indices
		for(int v=0; v<numVertices; v++)
		{
			stream->Write(pNavMesh->m_VertexIndexArray->Get(firstVertexIndex+v), sizeof(u16));
		}

		// Write adjacency
		for(int v=0; v<numVertices; v++)
		{
			stream->Write(&pNavMesh->m_AdjacentPolysArray->Get(firstVertexIndex+v)->m_iAsInteger1, sizeof(u32));
			stream->Write(&pNavMesh->m_AdjacentPolysArray->Get(firstVertexIndex+v)->m_iAsInteger2, sizeof(u32));
		}

		u16 firstSpecialLinkIndex = (u16)pPoly->GetSpecialLinksStartIndex();
		stream->Write(&firstSpecialLinkIndex, sizeof(u16));

		u8 numSpecialLinks = (u8)pPoly->GetNumSpecialLinks();
		stream->Write(&numSpecialLinks, sizeof(u8));
	}

	// Write special links section
	if(pNavMesh->m_iFlags & NAVMESH_HAS_SPECIAL_LINKS_SECTION)
	{
		stream->Write(&pNavMesh->m_iNumSpecialLinks, sizeof(u32));
		for(u32 s=0; s<pNavMesh->m_iNumSpecialLinks; s++)
		{
			pNavMesh->m_SpecialLinks[s].Write(stream);
		}

		stream->Write(&pNavMesh->m_iNumSpecialLinksIndices, sizeof(u32));
		stream->Write(pNavMesh->m_SpecialLinksIndices, sizeof(u16) * pNavMesh->m_iNumSpecialLinksIndices );
	}

	stream->Close();

	return true;
}

DEV_ONLY(s32 g_iMemUsedForCoverPoints=0;)

#ifndef COMPILE_TOOL_MESH

#ifdef GTA_ENGINE
#if (__XENON) || (__PPU)
const bool rage::g_bEndianSwap = true;
#else	// Any other platforms ?
const bool rage::g_bEndianSwap = false;
#endif
#else // GTA_ENGINE
const bool rage::g_bEndianSwap = false;
#endif

#endif	// COMPILE_TOOL_MESH

CNavMeshQuadTree * CNavMesh::LoadQuadtree(CNavMesh * pNavMesh, fiStream * pStream)
{
	CNavMeshAlloc<CNavMeshQuadTree> quadTreeAlloc;
	CNavMeshQuadTree * pTree = quadTreeAlloc.New();

	pNavMesh->m_iTotalMemoryUsed += sizeof(CNavMeshQuadTree);

	// Read the extents
	pStream->ReadFloat(&pTree->m_Mins.x, 1);
	pStream->ReadFloat(&pTree->m_Mins.y, 1);
	pStream->ReadFloat(&pTree->m_Mins.z, 1);
	pStream->ReadFloat(&pTree->m_Maxs.x, 1);
	pStream->ReadFloat(&pTree->m_Maxs.y, 1);
	pStream->ReadFloat(&pTree->m_Maxs.z, 1);

	pTree->m_MinMax.SetFloat(pTree->m_Mins.x, pTree->m_Mins.y, pTree->m_Mins.z, pTree->m_Maxs.x, pTree->m_Maxs.y, pTree->m_Maxs.z);

	u8 bLeafData;
	pStream->Read(&bLeafData, 1);

	if(bLeafData)
	{
		CNavMeshAlloc<TNavMeshQuadTreeLeafData> leafDataAlloc;
		pTree->m_pLeafData = leafDataAlloc.New();

		pNavMesh->m_iTotalMemoryUsed += sizeof(TNavMeshQuadTreeLeafData);

		// Read num of poly indices in this leaf
		pStream->Read(&pTree->m_pLeafData->m_iNumPolys, 2);
		ByteSwap16(&pTree->m_pLeafData->m_iNumPolys);

		if(pTree->m_pLeafData->m_iNumPolys)
		{
			CNavMeshAlloc<u16> u16Alloc;
			pTree->m_pLeafData->m_Polys = u16Alloc.VectorNew(pTree->m_pLeafData->m_iNumPolys);

			for(int i=0; i<pTree->m_pLeafData->m_iNumPolys; i++)
			{
				u16 iPolyIndex;
				pStream->ReadShort(&iPolyIndex, 1);
				pTree->m_pLeafData->m_Polys[i] = iPolyIndex;
			}
		}
		else
		{
			pTree->m_pLeafData->m_Polys = NULL;
		}

		pNavMesh->m_iTotalMemoryUsed += (sizeof(u16) * pTree->m_pLeafData->m_iNumPolys);

		// Read the num of cover points in this leaf
		pStream->Read(&pTree->m_pLeafData->m_iNumCoverPoints, 2);
		ByteSwap16(&pTree->m_pLeafData->m_iNumCoverPoints);

		CNavMeshAlloc<CNavMeshCoverPoint> cpAlloc;

		// Each leaf has to have the index (within this navmesh) of the first coverpoint in the m_CoverPoints list
		pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList = (u16)pNavMesh->m_iNumCoverPoints;

		pNavMesh->m_iNumCoverPoints += pTree->m_pLeafData->m_iNumCoverPoints;

		if(pTree->m_pLeafData->m_iNumCoverPoints)
		{
			if(!ms_bLoadCoverPoints)
			{
				u16 iTmp16;
				for(int c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
				{
					// Pos
					pStream->ReadShort(&iTmp16, 1);
					pStream->ReadShort(&iTmp16, 1);
					pStream->ReadShort(&iTmp16, 1);
					// Flags
					pStream->ReadShort(&iTmp16, 1);

					u8 iTmp8;
					// Left/Right Link Indices
					pStream->ReadShort(&iTmp16, 1);
					pStream->ReadShort(&iTmp16, 1);
					// Left/Right Adj Info
					pStream->ReadByte(&iTmp8, 1);
					pStream->ReadByte(&iTmp8, 1);
				}
				pTree->m_pLeafData->m_iNumCoverPoints = 0;
				pTree->m_pLeafData->m_CoverPoints = NULL;
			}
			else
			{
				pTree->m_pLeafData->m_CoverPoints = cpAlloc.VectorNew(pTree->m_pLeafData->m_iNumCoverPoints);
				memset(pTree->m_pLeafData->m_CoverPoints, 0, sizeof(CNavMeshCoverPoint)*pTree->m_pLeafData->m_iNumCoverPoints);

				for(int c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
				{
					CNavMeshCoverPoint & cpt = pTree->m_pLeafData->m_CoverPoints[c];

					pStream->ReadShort(&cpt.m_iX, 1);
					pStream->ReadShort(&cpt.m_iY, 1);
					pStream->ReadShort(&cpt.m_iZ, 1);

					u16 iCoverPointFlags;
					pStream->ReadShort(&iCoverPointFlags, 1); 

					if(g_bEndianSwap)
					{
						cpt.m_CoverPointFlags.SetCoverDir( (iCoverPointFlags & 0x00FF) );
						cpt.m_CoverPointFlags.SetCoverType( ((iCoverPointFlags & 0x0700) >> 8) );
					}
					else
					{
						cpt.m_CoverPointFlags.SetAsInteger(iCoverPointFlags);
					}
				}
			}
		}
		else
		{
			pTree->m_pLeafData->m_CoverPoints = NULL;
		}

		pNavMesh->m_iTotalMemoryUsed += (sizeof(CNavMeshCoverPoint) * pTree->m_pLeafData->m_iNumCoverPoints);
		DEV_ONLY(g_iMemUsedForCoverPoints += (sizeof(CNavMeshCoverPoint) * pTree->m_pLeafData->m_iNumCoverPoints);)
	}
	else
	{
		for(int c=0; c<4; c++)
		{
			pTree->m_pChildren[c] = LoadQuadtree(pNavMesh, pStream);
		}
	}

	return pTree;
}

#if !__FINAL && !__PPU
CNavMesh * CNavMesh::LoadBinary(const char * pFileName, sysPerformanceTimer * pPerfTimer)
#else
CNavMesh * CNavMesh::LoadBinary(const char * pFileName)
#endif
{
	if(!pFileName || !*pFileName)
		return NULL;

	fiStream * pStream = ASSET.Open(pFileName, "", true);
	if(!pStream)
		return NULL;

	// Read "NAVM" header
	char header[4];
	pStream->Read(&header[0], 1);
	pStream->Read(&header[1], 1);
	pStream->Read(&header[2], 1);
	pStream->Read(&header[3], 1);

	if(header[0] != 'N' || header[1] != 'A' || header[2] != 'V' || header[3] != 'M')
	{
		pStream->Close();
		return NULL;
	}

	// Read the file version
	u32 iFileVersion;
	pStream->Read(&iFileVersion, 4);
	ByteSwap32(&iFileVersion);

#if __CHECK_NAVMESH_FILE_VERSION
	if(iFileVersion < ms_iFileVersion)
	{
		// Loading previous versions of the ".inv" file is not supported..
		Errorf("Error - loading incompatible version of NavMesh");
		Assertf(iFileVersion == ms_iFileVersion, "The navmesh file(s) are older than is supported by this executable.\nTry getting a newer version of the navmeshes image, or using a previous exe file.\n");
		pStream->Close();
		return NULL;
	}
	if(iFileVersion > ms_iFileVersion)
	{
		// Loading newer versions of the ".inv" file is not supported..
		Errorf("Error - loading incompatible version of NavMesh");
		Assertf(iFileVersion == ms_iFileVersion, "The navmesh file(s) are newer than is supported by this executable.\nTry getting a newer exe file, or using a previous version of the navmeshes image.\n");
		pStream->Close();
		return NULL;
	}
#endif	// __CHECK_NAVMESH_FILE_VERSION

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		printf("************************************************************\n");
		printf("CNavMesh::LoadBinary()\n");
		pPerfTimer->Reset();
		pPerfTimer->Start();
	}
#endif

	DEV_ONLY(TNavmeshMemStats memStats;)

	CNavMeshAlloc<u16> u16Alloc;
	CNavMeshAlloc<TAdjPoly> adjPolyAlloc;
	CNavMeshAlloc<CNavMesh> navMeshAlloc;
	CNavMeshAlloc<TNavMeshPoly> polyAlloc;

	CNavMesh * pNavMesh = navMeshAlloc.New();

	pNavMesh->m_iFileVersion = iFileVersion; 

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Stop();
		printf(" - alloc new took %.4fms\n", (float)pPerfTimer->GetTimeMS());
	}
#endif

	// Read the flags
	pStream->Read(&pNavMesh->m_iFlags, 4);
	ByteSwap32(&pNavMesh->m_iFlags);

	// Read the build ID
	u32 iBuildID;
	pStream->Read(&iBuildID, 4);
	ByteSwap32(&iBuildID);
	pNavMesh->m_iBuildID = iBuildID;

	// Read index of mesh
	pStream->Read(&pNavMesh->m_iIndexOfMesh, 4);
	ByteSwap32(&pNavMesh->m_iIndexOfMesh);

#if __ASSERT
	const bool bDynamic = pNavMesh->m_iFlags&NAVMESH_IS_DYNAMIC;
	if(bDynamic)
		Assert(pNavMesh->m_iIndexOfMesh > NAVMESH_MAX_MAP_INDEX);
	else
		Assert(pNavMesh->m_iIndexOfMesh <= NAVMESH_MAX_MAP_INDEX);
#endif // __ASSERT

	// Read the adjacent meshes
	pStream->Read(&pNavMesh->m_AdjacentMeshes.m_iNumIndices, 4);
	ByteSwap32(&pNavMesh->m_AdjacentMeshes.m_iNumIndices);

	for(s32 a=0; a<CAdjacentNavmeshLookups::ms_iMaxAdjacentNavMeshes; a++)
	{
		pStream->Read(&pNavMesh->m_AdjacentMeshes.m_iNavmeshIndices[a], 4);
		ByteSwap32(&pNavMesh->m_AdjacentMeshes.m_iNavmeshIndices[a]);
	}

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Reset();
		pPerfTimer->Start();
	}
#endif

	DEV_ONLY(s32 iTmpMemoryUsed = 0;)
	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)
	DEV_ONLY(g_iMemUsedForCoverPoints=0;)

	pNavMesh->m_iNumCoverPoints = 0;

	pNavMesh->m_pQuadTree = LoadQuadtree(pNavMesh, pStream);

	if(pNavMesh->m_pQuadTree)
		pNavMesh->m_pNonResourcedData->m_vMinOfNavMesh = pNavMesh->m_pQuadTree->m_Mins;

	DEV_ONLY(memStats.iMemUsedForQuadtrees = pNavMesh->m_iTotalMemoryUsed - iTmpMemoryUsed;)
	DEV_ONLY(memStats.iMemUsedForCoverPoints = g_iMemUsedForCoverPoints;)

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Stop();
		printf("loading quadtree took %.4fms\n", (float)pPerfTimer->GetTimeMS());
	}
#endif

	// Set extents from quadtree dimensions
	pNavMesh->m_vExtents.x = pNavMesh->m_pQuadTree->m_Maxs.x - pNavMesh->m_pQuadTree->m_Mins.x;
	pNavMesh->m_vExtents.y = pNavMesh->m_pQuadTree->m_Maxs.y - pNavMesh->m_pQuadTree->m_Mins.y;
	pNavMesh->m_vExtents.z = pNavMesh->m_pQuadTree->m_Maxs.z - pNavMesh->m_pQuadTree->m_Mins.z;

	// Read num vertices & triangles
	pStream->Read(&pNavMesh->m_iNumVertices, 4);
	ByteSwap32(&pNavMesh->m_iNumVertices);
	Assert(pNavMesh->m_iNumVertices > 0);

	pStream->Read(&pNavMesh->m_iNumPolys, 4);
	ByteSwap32(&pNavMesh->m_iNumPolys);
	Assert(pNavMesh->m_iNumPolys > 0);

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Reset();
		pPerfTimer->Start();
	}
#endif

	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)

	// Optionally disable compressed vertices
	if(ms_bForceAllNavMeshesToUseUnCompressedVertices)
	{
		pNavMesh->m_iFlags &= ~NAVMESH_COMPRESSED_VERTEX_DATA;
	}

	// Read the vertices
	if(pNavMesh->m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
	{
		pNavMesh->m_CompressedVertexArray = rage_new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(pNavMesh->m_iNumVertices);
		pNavMesh->m_CompressedVertexArray->SetZero();

		pNavMesh->m_iTotalMemoryUsed += (sizeof(u16) * (pNavMesh->m_iNumVertices * 3));

		u16 iWord;
		for(u32 v=0; v<pNavMesh->m_iNumVertices; v++)
		{
			pStream->Read(&iWord, 2);
			ByteSwap16(&iWord);
			pNavMesh->GetCompressedVertexArray().Get(v)->data[0] = iWord;

			pStream->Read(&iWord, 2);
			ByteSwap16(&iWord);
			pNavMesh->GetCompressedVertexArray().Get(v)->data[1] = iWord;

			pStream->Read(&iWord, 2);
			ByteSwap16(&iWord);
			pNavMesh->GetCompressedVertexArray().Get(v)->data[2] = iWord;
		}
	}
	else
	{
		CNavMeshAlloc<Vector3> vecAlloc;
		pNavMesh->m_VertexPool = vecAlloc.VectorNew(pNavMesh->m_iNumVertices);
		memset(pNavMesh->m_VertexPool, 0, sizeof(Vector3)*pNavMesh->m_iNumVertices);

		pNavMesh->m_iTotalMemoryUsed += (sizeof(Vector3) * pNavMesh->m_iNumVertices);

		for(u32 v=0; v<pNavMesh->m_iNumVertices; v++)
		{
			pStream->ReadFloat(&pNavMesh->m_VertexPool[v].x, 1);
			pStream->ReadFloat(&pNavMesh->m_VertexPool[v].y, 1);
			pStream->ReadFloat(&pNavMesh->m_VertexPool[v].z, 1);
		}
	}

	DEV_ONLY(memStats.iMemUsedForVertexData = pNavMesh->m_iTotalMemoryUsed - iTmpMemoryUsed;)

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Stop();
		printf("loading %i vertices took %.4fms\n", pNavMesh->m_iNumVertices, (float)pPerfTimer->GetTimeMS());
	}
#endif

	// Read the size of the pools
	pStream->Read(&pNavMesh->m_iSizeOfPools, 4);
	ByteSwap32(&pNavMesh->m_iSizeOfPools);

	pNavMesh->m_VertexIndexArray = rage_new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools);
	pNavMesh->m_VertexIndexArray->SetZero();

	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)
	pNavMesh->m_iTotalMemoryUsed += (sizeof(u16) * pNavMesh->m_iSizeOfPools);
	DEV_ONLY(memStats.iMemUsedForVertexIndices = pNavMesh->m_iTotalMemoryUsed - iTmpMemoryUsed;)


	pNavMesh->m_AdjacentPolysArray = rage_new aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools);
	pNavMesh->m_AdjacentPolysArray->SetZero();
//	pNavMesh->m_AdjacentPolyPool = adjPolyAlloc.VectorNew(pNavMesh->m_iSizeOfPools);
//	memset(pNavMesh->m_AdjacentPolyPool, 0, sizeof(TAdjPoly)*pNavMesh->m_iSizeOfPools);

	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)
	pNavMesh->m_iTotalMemoryUsed += (sizeof(TAdjPoly) * pNavMesh->m_iSizeOfPools);
	DEV_ONLY(memStats.iMemUsedForAdjPolys = pNavMesh->m_iTotalMemoryUsed - iTmpMemoryUsed;)

	// Read the polys into a temporay buffer
//	TNavMeshPoly * pPolys = polyAlloc.VectorNew(pNavMesh->m_iNumPolys);
//	memset(pPolys, 0, sizeof(TNavMeshPoly)*pNavMesh->m_iNumPolys);
	// Create aiSplitArray from temporary buffer
	pNavMesh->m_PolysArray = rage_new aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE>(pNavMesh->m_iNumPolys);
	pNavMesh->m_PolysArray->SetZero();
	// Delete the temp buffer
//	delete[] pPolys;

	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)
	pNavMesh->m_iTotalMemoryUsed += (sizeof(TNavMeshPoly) * pNavMesh->m_iNumPolys);
	DEV_ONLY(memStats.iMemUsedForPolys = pNavMesh->m_iTotalMemoryUsed - iTmpMemoryUsed;)

	u8 iFlags;
	u8 iNumVertices;
	Vector3 vPolyVert;

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Reset();
		pPerfTimer->Start();
	}
#endif

	for(u32 t=0; t<pNavMesh->m_iNumPolys; t++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(t);
		pStream->Read(&iFlags, 1);
		pPoly->SetFlags(iFlags);

		//------------------------------------------------------
		// Pre v1.17

		if(iFileVersion < 0x00010011)
		{
			// Ped density & num vertices are combined into one byte
			u8 iVerticesAndDensity;
			pStream->Read(&iVerticesAndDensity, 1);

			iNumVertices = iVerticesAndDensity & 0xF;
			u8 iPedDensity = ((iVerticesAndDensity & 0xF0) >> 4);

			pPoly->SetNumVertices(iNumVertices);
			pPoly->SetPedDensity(iPedDensity);

			Assert(iNumVertices >= 3);

			u16 iAudioPropertiesEtc;
			pStream->Read(&iAudioPropertiesEtc, 2);
			ByteSwap16(&iAudioPropertiesEtc);
			pPoly->SetStruct3AsInteger(iAudioPropertiesEtc);
		}

		//-------------------------------------------------------
		// v1.17 and later

		else
		{
			pStream->Read(&iNumVertices, 1);
			pPoly->SetNumVertices(iNumVertices);

			u32 iStruct3;
			pStream->ReadInt(&iStruct3, 1);
			pPoly->SetStruct3AsInteger(iStruct3);

			Assert(iNumVertices >= 3 || pPoly->GetIsZeroAreaStichPolyDLC());
		} 

		//--------------------------------------------------


		u8 iCoverDirections;
		pStream->Read(&iCoverDirections, 1);
		pPoly->m_iCoverDirectionsBitfield = iCoverDirections;

		u32 iNonResourced;
		pStream->Read(&iNonResourced, 4);

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
		ByteSwap32(&iNonResourced);
		pPoly->m_iNonResourcedAsInteger = iNonResourced;
#endif
#endif
		u16 iFirstVertexIndex;
		pStream->Read(&iFirstVertexIndex, 2);
		ByteSwap16(&iFirstVertexIndex);

#if __ASSERT
		const s32 iMaxValue = (1<<NUM_BITS_FOR_1ST_VERTEX_INDEX)-1;
		Assertf(iFirstVertexIndex < iMaxValue, "iFirstVertexIndex is about to overflow. Navmesh will be broken.\nThe fix for this is to have less polygons in this navmesh, which could be achieved by lowering the sampling resolution in the .lst file during construction.");
#endif

		Assert(iFirstVertexIndex + pPoly->GetNumVertices() <= pNavMesh->GetSizeOfPools());

		pPoly->SetFirstVertexIndex(iFirstVertexIndex);

		u16 iShort;

		// TODO : All of this min/max & centroid calculation should be done in the
		// CPathServerThread's Run() function so as not to hold up the streaming
		Vector3 vCentroid(0.0f,0.0f,0.0f);
		float fPolyMinX = 32000.0f;
		float fPolyMinY = 32000.0f;
		float fPolyMinZ = 32000.0f;
		float fPolyMaxX = -32000.0f;
		float fPolyMaxY = -32000.0f;
		float fPolyMaxZ = -32000.0f;

		// Read vertex indices
		for(u32 v=0; v<pPoly->GetNumVertices(); v++)
		{
			pStream->Read(&iShort, 2);
			ByteSwap16(&iShort);
			*pNavMesh->m_VertexIndexArray->Get(pPoly->GetFirstVertexIndex()+v) = iShort;

			// Update poly's minmax extents
			pNavMesh->GetVertex(iShort, vPolyVert);

			vCentroid += vPolyVert;

			if(vPolyVert.x < fPolyMinX) fPolyMinX = vPolyVert.x;
			if(vPolyVert.x > fPolyMaxX) fPolyMaxX = vPolyVert.x;
			if(vPolyVert.y < fPolyMinY) fPolyMinY = vPolyVert.y;
			if(vPolyVert.y > fPolyMaxY) fPolyMaxY = vPolyVert.y;
			if(vPolyVert.z < fPolyMinZ) fPolyMinZ = vPolyVert.z;
			if(vPolyVert.z > fPolyMaxZ) fPolyMaxZ = vPolyVert.z;
		}

		// Quick short minmax extents
		fPolyMinX -= TShortMinMax::ms_vExtendMinMax.x;
		fPolyMinY -= TShortMinMax::ms_vExtendMinMax.y;
		fPolyMinZ -= TShortMinMax::ms_vExtendMinMax.z;
		fPolyMaxX += TShortMinMax::ms_vExtendMinMax.x;
		fPolyMaxY += TShortMinMax::ms_vExtendMinMax.y;
		fPolyMaxZ += TShortMinMax::ms_vExtendMinMax.z;

		pPoly->m_MinMax.SetFloat(fPolyMinX, fPolyMinY, fPolyMinZ, fPolyMaxX, fPolyMaxY, fPolyMaxZ);

		Vector3 vShortMin, vShortMax;
		pPoly->m_MinMax.GetAsFloats(vShortMin, vShortMax);

		// Poly centroid stored as compressed shorts (same as navmesh verts) for quick retrieval w/o averaging all poly points
		vCentroid /= (float)pPoly->GetNumVertices();
		u8 iVertZ;
		CompressVertex(vCentroid, pPoly->m_iCentroidX, pPoly->m_iCentroidY, iVertZ, vShortMin, vShortMax - vShortMin);

		// Clear immediate-mode flags & members
		pPoly->m_iImmediateModeFlags = 0;
		pPoly->m_iAsInteger4 = 0;

		// Store which subarray this poly belongs to, which increased efficiency of splitarray lookup
		pPoly->m_Struct4.m_iPolyArrayIndex = pNavMesh->m_PolysArray->GetSubArrayIndex(t);


		// Read adjacency
		for(u32 v=0; v<pPoly->GetNumVertices(); v++)
		{
			//TAdjPoly & adjPoly = pNavMesh->m_AdjacentPolyPool[pPoly->GetFirstVertexIndex()+v];
			TAdjPoly * pAdjPoly = pNavMesh->m_AdjacentPolysArray->Get(pPoly->GetFirstVertexIndex()+v);

			pStream->ReadInt(&pAdjPoly->m_iAsInteger1, 1);
			pStream->ReadInt(&pAdjPoly->m_iAsInteger2, 1);

			pAdjPoly->m_Struct2.m_OriginalNavMeshIndex = pAdjPoly->m_Struct1.m_NavMeshIndex;
			pAdjPoly->SetOriginalPolyIndex(pAdjPoly->GetPolyIndex());

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
			pAdjPoly->m_iAdjacentNavMeshIndex = pAdjPoly->GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
#endif
#endif
		}

		pPoly->SetNavMeshIndex(pNavMesh->m_iIndexOfMesh);
		pPoly->m_TimeStamp = 0;
		pPoly->m_AStarTimeStamp = 0;

		pPoly->SetDebugMarked(false);

#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
#if __ASSERT
		static bool bTestCentroid = false;
		Vector3 vTestCentroid;
		if(bTestCentroid)
		{
			pNavMesh->GetPolyCentroidQuick(pPoly, vTestCentroid);
		}
#endif

		pPoly->m_iRemappedPolyIndex = 0xFFFF;

#endif
#endif

		u16 firstSpecialLinkIndex;
		pStream->ReadShort(&firstSpecialLinkIndex, 1);
		pPoly->SetSpecialLinksStartIndex(firstSpecialLinkIndex);

		u8 numSpecialLinks;
		pStream->ReadByte(&numSpecialLinks, 1);
		pPoly->SetNumSpecialLinks(numSpecialLinks);
	}

	//**************************************************************************************************************
	// Read the special links section, if present (The navmesh's m_Flags has NAVMESH_HAS_SPECIAL_LINKS_SECTION set)

	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)

	if(pNavMesh->m_iFlags & NAVMESH_HAS_SPECIAL_LINKS_SECTION)
	{
		CNavMeshAlloc<CSpecialLinkInfo> linkAlloc;

		// Read special links
		pStream->Read(&pNavMesh->m_iNumSpecialLinks, 4);
		ByteSwap32(&pNavMesh->m_iNumSpecialLinks);

		pNavMesh->m_SpecialLinks = linkAlloc.VectorNew(pNavMesh->m_iNumSpecialLinks);
		memset(pNavMesh->m_SpecialLinks, 0, sizeof(CSpecialLinkInfo)*pNavMesh->m_iNumSpecialLinks);
		pNavMesh->m_iTotalMemoryUsed += (sizeof(CSpecialLinkInfo) * pNavMesh->m_iNumSpecialLinks);

		for(u32 s=0; s<pNavMesh->m_iNumSpecialLinks; s++)
		{
			CSpecialLinkInfo * pLink = &pNavMesh->m_SpecialLinks[s];
			pLink->Read(pStream);
		}

		// Read special links indices
		pStream->Read(&pNavMesh->m_iNumSpecialLinksIndices, 4);
		ByteSwap32(&pNavMesh->m_iNumSpecialLinksIndices);

		pNavMesh->m_SpecialLinksIndices = new u16[pNavMesh->m_iNumSpecialLinksIndices];
		pStream->ReadShort(pNavMesh->m_SpecialLinksIndices, pNavMesh->m_iNumSpecialLinksIndices);
	}

	DEV_ONLY(memStats.iMemUsedForSpecialLinks = pNavMesh->m_iTotalMemoryUsed - iTmpMemoryUsed;)

	DEV_ONLY(iTmpMemoryUsed = pNavMesh->m_iTotalMemoryUsed;)


	//********************************************************************************************************
	//	Finished loading

#if !__FINAL && !__PPU
	if(ms_bDisplayTimeTakenToLoadNavMeshes && pPerfTimer)
	{
		pPerfTimer->Stop();
		printf("loading %i polys took %.4fms\n", pNavMesh->m_iNumPolys, (float)pPerfTimer->GetTimeMS());
	}
#endif

	pStream->Close();

#if  __RESOURCECOMPILER
	sysMemStartTemp();
	delete pNavMesh->m_pNonResourcedData;
	pNavMesh->m_pNonResourcedData = NULL;
	sysMemEndTemp();
#endif

#if 0
	u32 p;
	for(p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly & poly = pNavMesh->m_Polys[p];
		AssertMsg(!(poly.GetFlags()&NAVMESHPOLY_DEGENERATE_CONNECTION_POLY), "ERROR - poly flag set : NAVMESHPOLY_DEGENERATE_CONNECTION_POLY");
		AssertMsg(!(poly.GetFlags()&NAVMESHPOLY_TESSELLATED_FRAGMENT), "ERROR - poly flag set : NAVMESHPOLY_TESSELLATED_FRAGMENT");
		AssertMsg(!(poly.GetFlags()&NAVMESHPOLY_REPLACED_BY_TESSELLATION), "ERROR - poly flag set : NAVMESHPOLY_REPLACED_BY_TESSELLATION");
	}
	u32 a;
	for(a=0; a<pNavMesh->GetSizeOfPools(); a++)
	{
		TAdjPoly & adjPoly = pNavMesh->m_AdjacentPolyPool[a];
		AssertMsg(adjPoly.GetNavMeshIndex() == adjPoly.GetOriginalNavMeshIndex(), "ERROR - adjpoly mesh index doesnt match original mesh index");
		if(!bDynamic)
		{
			Assertf( (adjPoly.GetNavMeshIndex() <= NAVMESH_MAX_MAP_INDEX || adjPoly.GetNavMeshIndex()==NAVMESH_NAVMESH_INDEX_NONE), "ERROR - nav mesh index %u is out of acceptable range", adjPoly.GetNavMeshIndex() );
			Assertf( (adjPoly.GetOriginalNavMeshIndex() <= NAVMESH_MAX_MAP_INDEX || adjPoly.GetOriginalNavMeshIndex()==NAVMESH_NAVMESH_INDEX_NONE), "ERROR - original nav mesh index %u is out of acceptable range", adjPoly.GetOriginalNavMeshIndex() );
		}
	}
#endif

	return pNavMesh;
}

#if !__FINAL && !__PPU
CNavMesh * CNavMesh::LoadFromMemory(void * pMemBuffer, int iBufferSize, sysPerformanceTimer * pPerfTimer)
#else
CNavMesh * CNavMesh::LoadFromMemory(void * pMemBuffer, int iBufferSize)
#endif
{
	char filename[256];
	fiDeviceMemory::MakeMemoryFileName(filename, 256, pMemBuffer, iBufferSize, false, "NavMesh");

#if !__FINAL && !__PPU
	return CNavMesh::LoadBinary(filename, pPerfTimer);
#else
	return CNavMesh::LoadBinary(filename);
#endif
}


CSpecialLinkInfo::CSpecialLinkInfo()
{
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	memset(this, 0, sizeof(CSpecialLinkInfo));
#endif
#endif
	m_iLinkFromPosX = m_iLinkFromPosY = m_iLinkFromPosZ = 0;
	m_Struct1.m_iLinkFromNavMesh = NAVMESH_NAVMESH_INDEX_NONE;
	m_Struct1.m_bLinkFromTessellationMesh = false;
	m_iLinkFromPoly = m_iOriginalLinkFromPoly = NAVMESH_POLY_INDEX_NONE;

	m_iLinkToPosX = m_iLinkToPosY = m_iLinkToPosZ = 0;
	m_Struct1.m_iLinkToNavMesh = NAVMESH_NAVMESH_INDEX_NONE;
	m_Struct1.m_bLinkToTessellationMesh = false;
	m_iLinkToPoly = m_iOriginalLinkToPoly = NAVMESH_POLY_INDEX_NONE;

	m_iLinkType = 0;
	m_iLinkFlags = 0;
	m_iAStarTimeStamp = 0;
}

#if __DECLARESTRUCT
void CSpecialLinkInfo::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(CSpecialLinkInfo);
	
	STRUCT_FIELD(m_iLinkType);
	STRUCT_FIELD(m_iLinkFlags);
	STRUCT_FIELD(m_iAStarTimeStamp);

	STRUCT_FIELD(m_iLinkFromPosX);
	STRUCT_FIELD(m_iLinkFromPosY);
	STRUCT_FIELD(m_iLinkFromPosZ);
	STRUCT_FIELD(m_iLinkToPosX);
	STRUCT_FIELD(m_iLinkToPosY);
	STRUCT_FIELD(m_iLinkToPosZ);

	STRUCT_FIELD(m_iOriginalLinkFromPoly);
	STRUCT_FIELD(m_iLinkFromPoly);
	STRUCT_FIELD(m_iOriginalLinkToPoly);
	STRUCT_FIELD(m_iLinkToPoly);

	STRUCT_FIELD(m_iAsInteger1);

	STRUCT_END();
}
void
TPolyVolumeInfo::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(TPolyVolumeInfo);
	STRUCT_FIELD(m_iFreeSpaceTopZ);
	STRUCT_FIELD(m_iPolyLinkedToAbove);
	STRUCT_FIELD(m_iPolyLinkedToBelow);
	STRUCT_END();
}

#endif // !__FINAL


// NAME : Reset
// PURPOSE : Reset special link to initial after pathsearch
void CSpecialLinkInfo::Reset()
{
	m_Struct1.m_bLinkFromTessellationMesh = false;
	m_Struct1.m_bLinkToTessellationMesh = false;
	m_iLinkFromPoly = m_iOriginalLinkFromPoly;
	m_iLinkToPoly = m_iOriginalLinkToPoly;
}

void CSpecialLinkInfo::Read(fiStream * pStream)
{
	pStream->Read(&m_iLinkType, 1);
	pStream->Read(&m_iLinkFlags, 1);

	u16 iLinkFromNavMesh;
	pStream->Read(&iLinkFromNavMesh, 2);
	ByteSwap16(&iLinkFromNavMesh);
	m_Struct1.m_iLinkFromNavMesh = iLinkFromNavMesh;
	m_Struct1.m_bLinkFromTessellationMesh = false;

	pStream->Read(&m_iLinkFromPoly, 2);
	ByteSwap16(&m_iLinkFromPoly);
	m_iOriginalLinkFromPoly = m_iLinkFromPoly;

	pStream->Read(&m_iLinkFromPosX, 2);
	ByteSwap16(&m_iLinkFromPosX);
	pStream->Read(&m_iLinkFromPosY, 2);
	ByteSwap16(&m_iLinkFromPosY);
	pStream->Read(&m_iLinkFromPosZ, 2);
	ByteSwap16(&m_iLinkFromPosZ);

	u16 iLinkToNavMesh;
	pStream->Read(&iLinkToNavMesh, 2);
	ByteSwap16(&iLinkToNavMesh);
	m_Struct1.m_iLinkToNavMesh = iLinkToNavMesh;
	m_Struct1.m_bLinkToTessellationMesh = false;

	pStream->Read(&m_iLinkToPoly, 2);
	ByteSwap16(&m_iLinkToPoly);
	m_iOriginalLinkToPoly = m_iLinkToPoly;

	pStream->Read(&m_iLinkToPosX, 2);
	ByteSwap16(&m_iLinkToPosX);
	pStream->Read(&m_iLinkToPosY, 2);
	ByteSwap16(&m_iLinkToPosY);
	pStream->Read(&m_iLinkToPosZ, 2);
	ByteSwap16(&m_iLinkToPosZ);
}

void CSpecialLinkInfo::Write(fiStream * pStream) const
{
	pStream->Write(&m_iLinkType, sizeof(u8));
	pStream->Write(&m_iLinkFlags, sizeof(u8));
	u16 iLinkFromNavMesh = m_Struct1.m_iLinkFromNavMesh;
	pStream->Write(&iLinkFromNavMesh, sizeof(u16));
	pStream->Write(&m_iOriginalLinkFromPoly, sizeof(u16));
	pStream->Write(&m_iLinkFromPosX, sizeof(u16));
	pStream->Write(&m_iLinkFromPosY, sizeof(u16));
	pStream->Write(&m_iLinkFromPosZ, sizeof(u16));
	u16 iLinkToNavMesh = m_Struct1.m_iLinkToNavMesh;
	pStream->Write(&iLinkToNavMesh, sizeof(u16));
	pStream->Write(&m_iOriginalLinkToPoly, sizeof(u16));
	pStream->Write(&m_iLinkToPosX, sizeof(u16));
	pStream->Write(&m_iLinkToPosY, sizeof(u16));
	pStream->Write(&m_iLinkToPosZ, sizeof(u16));
}

CNavMeshQuadTree::CNavMeshQuadTree() :
	m_Mins(0.0f, 0.0f, 0.0f),
	m_pLeafData(NULL)
{
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	memset(this, 0, sizeof(CNavMeshQuadTree));
#endif
#endif
	m_MinMax.SetInt(0,0,0,0,0,0);
	m_pChildren[0] = m_pChildren[1] = m_pChildren[2] = m_pChildren[3] = NULL;
}

CNavMeshQuadTree::~CNavMeshQuadTree()
{
	CNavMeshAlloc<CNavMeshQuadTree> treeAlloc;
	CNavMeshAlloc<u16> u16Alloc;
	CNavMeshAlloc<CNavMeshCoverPoint> cpAlloc;
	CNavMeshAlloc<TNavMeshQuadTreeLeafData> leafDataAlloc;

	if(m_pLeafData)
	{
		u16Alloc.VectorDelete(m_pLeafData->m_Polys, m_pLeafData->m_iNumPolys, false);
		m_pLeafData->m_Polys = NULL;

#ifdef NAVGEN_TOOL	// When constructing navmeshes, coverpoint destructor need calling..
		cpAlloc.VectorDelete(m_pLeafData->m_CoverPoints, m_pLeafData->m_iNumCoverPoints);
#else
		cpAlloc.VectorDelete(m_pLeafData->m_CoverPoints, m_pLeafData->m_iNumCoverPoints, false);
#endif
		m_pLeafData->m_CoverPoints = NULL;

		leafDataAlloc.Delete(m_pLeafData);
		m_pLeafData = NULL;
		return;
	}
	for(int c=0; c<4; c++)
	{
		if(m_pChildren[c])
		{
			treeAlloc.Delete(m_pChildren[c]);
			m_pChildren[c] = NULL;
		}
	}
}

CNavMeshQuadTree::CNavMeshQuadTree(datResource & rsc)
{
	rsc.PointerFixup(m_pLeafData);
	rsc.PointerFixup(m_pChildren[0]);
	rsc.PointerFixup(m_pChildren[1]);
	rsc.PointerFixup(m_pChildren[2]);
	rsc.PointerFixup(m_pChildren[3]);

	for(int c=0; c<4; c++)
	{
		if(m_pChildren[c])
		{
			CNavMeshQuadTree::Place(m_pChildren[c],rsc);
		}
	}

	if(m_pLeafData)
	{
		TNavMeshQuadTreeLeafData::Place(m_pLeafData, rsc);
	}
}

void
CNavMeshQuadTree::Place(CNavMeshQuadTree* that, datResource & rsc)
{
	::new (that) CNavMeshQuadTree(rsc);
}


#if __DECLARESTRUCT
void CNavMeshQuadTree::DeclareStruct()
{
	datTypeStruct s;

	STRUCT_BEGIN(CNavMeshQuadTree);

	STRUCT_FIELD(m_Mins);
	STRUCT_FIELD(m_Maxs);
	STRUCT_FIELD(m_MinMax);

	STRUCT_SKIP(m_pLeafData, 4);
	STRUCT_SKIP(m_pChildren[0], 4);
	STRUCT_SKIP(m_pChildren[1], 4);
	STRUCT_SKIP(m_pChildren[2], 4);
	STRUCT_SKIP(m_pChildren[3], 4);

	STRUCT_END();

	for(int c=0; c<4; c++)
	{
		if(m_pChildren[c])
			m_pChildren[c]->DeclareStruct();
	}

	if(m_pLeafData)
		m_pLeafData->DeclareStruct();

	datSwapper((void*&) m_pChildren[0]);
	datSwapper((void*&) m_pChildren[1]);
	datSwapper((void*&) m_pChildren[2]);
	datSwapper((void*&) m_pChildren[3]);

	datSwapper((void*&) m_pLeafData);
}
#endif

#if __DECLARESTRUCT
void TNavMeshQuadTreeLeafData::DeclareStruct()
{
	datTypeStruct s;

	STRUCT_BEGIN(TNavMeshQuadTreeLeafData);

	STRUCT_FIELD(m_iIndexOfFirstCoverPointInList);
	STRUCT_FIELD(m_iUnused);
	STRUCT_FIELD(m_iReserved);

	STRUCT_DYNAMIC_ARRAY(m_Polys, m_iNumPolys);
	STRUCT_DYNAMIC_ARRAY(m_CoverPoints, m_iNumCoverPoints);

	STRUCT_FIELD(m_iNumPolys);
	STRUCT_FIELD(m_iNumCoverPoints);

	STRUCT_END();
}
#endif

TNavMeshQuadTreeLeafData::TNavMeshQuadTreeLeafData()
{
#ifdef NAVGEN_TOOL
#ifndef RAGEBUILDER
	memset(this, 0, sizeof(TNavMeshQuadTreeLeafData));
#endif
#endif

	m_iIndexOfFirstCoverPointInList = 0;
	m_iUnused = 0;
	m_iReserved = 0;
	m_Polys = NULL;
	m_CoverPoints = NULL;
	m_iNumPolys = 0;
	m_iNumCoverPoints = 0;
}

TNavMeshQuadTreeLeafData::TNavMeshQuadTreeLeafData(datResource & rsc)
{
	rsc.PointerFixup(m_Polys);
	rsc.PointerFixup(m_CoverPoints);
}

void
TNavMeshQuadTreeLeafData::Place(TNavMeshQuadTreeLeafData* that, datResource & rsc)
{
	::new (that) TNavMeshQuadTreeLeafData(rsc);
}


CNavMesh * CNavMesh::Clone()
{
	CNavMesh * pNewMesh = rage_new CNavMesh;

	Assertf(pNewMesh, "Couldn't allocate memory for new navmesh");

	pNewMesh->m_iFlags = m_iFlags;
	pNewMesh->m_iNumVertices = m_iNumVertices;
	pNewMesh->m_iNumPolys = m_iNumPolys;
	pNewMesh->m_iSizeOfPools = m_iSizeOfPools;
	pNewMesh->m_vExtents = m_vExtents;
	pNewMesh->m_iIndexOfMesh = m_iIndexOfMesh;
	pNewMesh->m_Matrix = m_Matrix;
	pNewMesh->m_iTotalMemoryUsed = m_iTotalMemoryUsed;
	pNewMesh->m_iNumCoverPoints = m_iNumCoverPoints;
	pNewMesh->m_iNumSpecialLinks = m_iNumSpecialLinks;

	// Vertices
	if((pNewMesh->m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)!=0)
	{
		// Compressed
		pNewMesh->m_CompressedVertexArray = m_CompressedVertexArray->Clone();
	}
	else
	{
		// Uncompressed
		pNewMesh->m_VertexPool = rage_new Vector3[pNewMesh->m_iNumVertices];
		memcpy(pNewMesh->m_VertexPool, m_VertexPool, sizeof(Vector3) * pNewMesh->m_iNumVertices*3);
	}

	// Vertex indices
	pNewMesh->m_VertexIndexArray = m_VertexIndexArray->Clone();
	//pNewMesh->m_VertexIndexPool = rage_new u16[pNewMesh->m_iSizeOfPools];
	//memcpy(pNewMesh->m_VertexIndexPool, m_VertexIndexPool, sizeof(u16) * pNewMesh->m_iSizeOfPools);

	// Adjacent polys
	//pNewMesh->m_AdjacentPolyPool = rage_new TAdjPoly[pNewMesh->m_iSizeOfPools];
	//memcpy(pNewMesh->m_AdjacentPolyPool, m_AdjacentPolyPool, sizeof(TAdjPoly) * pNewMesh->m_iSizeOfPools);

	pNewMesh->m_AdjacentPolysArray = m_AdjacentPolysArray->Clone();

	// Polys
	//pNewMesh->m_Polys = rage_new TNavMeshPoly[pNewMesh->m_iNumPolys];
	//memcpy(pNewMesh->m_Polys, m_Polys, sizeof(TNavMeshPoly)*pNewMesh->m_iNumPolys);

	pNewMesh->m_PolysArray = m_PolysArray->Clone();

	// Special links
	if(m_iFlags & NAVMESH_HAS_SPECIAL_LINKS_SECTION)
	{
		pNewMesh->m_SpecialLinks = rage_new CSpecialLinkInfo[pNewMesh->m_iNumSpecialLinks];
		memcpy(pNewMesh->m_SpecialLinks, m_SpecialLinks, sizeof(CSpecialLinkInfo) * pNewMesh->m_iNumSpecialLinks);
	}

	// Quadtree
	if(m_pQuadTree)
	{
		pNewMesh->m_pQuadTree = CloneQuadTree(m_pQuadTree);
	}

	// Non-resourced data
	Assert(m_pNonResourcedData);
	Assert(pNewMesh->m_pNonResourcedData);
	memcpy(pNewMesh->m_pNonResourcedData, m_pNonResourcedData, sizeof(CNavMeshNonResourcedData));

	return pNewMesh;
}


CNavMeshQuadTree * CNavMesh::CloneQuadTree(CNavMeshQuadTree * pTree)
{
	CNavMeshQuadTree * pNewNode = rage_new CNavMeshQuadTree();

	pNewNode->m_Mins = pTree->m_Mins;
	pNewNode->m_Maxs = pTree->m_Maxs;
	pNewNode->m_MinMax = pTree->m_MinMax;

	// Copy data in each leaf
	if(pTree->m_pLeafData)
	{
		TNavMeshQuadTreeLeafData * pLeafData = rage_new TNavMeshQuadTreeLeafData();
		pNewNode->m_pLeafData = pLeafData;

		// Poly indices
		pLeafData->m_iNumPolys = pTree->m_pLeafData->m_iNumPolys;
		pLeafData->m_Polys = rage_new u16[pLeafData->m_iNumPolys];
		memcpy(pLeafData->m_Polys, pTree->m_pLeafData->m_Polys, sizeof(u16) * pLeafData->m_iNumPolys);

		pLeafData->m_iNumCoverPoints = pTree->m_pLeafData->m_iNumCoverPoints;
		pLeafData->m_iIndexOfFirstCoverPointInList = pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList;
		pLeafData->m_iUnused = pTree->m_pLeafData->m_iUnused;

		pLeafData->m_CoverPoints = rage_new CNavMeshCoverPoint[pLeafData->m_iNumCoverPoints];
		memcpy(pLeafData->m_CoverPoints, pTree->m_pLeafData->m_CoverPoints, sizeof(CNavMeshCoverPoint) * pLeafData->m_iNumCoverPoints);

		Assert(!pTree->m_pChildren[0] && !pTree->m_pChildren[1] && !pTree->m_pChildren[2] && !pTree->m_pChildren[3]);
	}
	else
	{
		pNewNode->m_pLeafData = NULL;

		// Recurse all 4 quadrants
		int c;
		for(c=0; c<4; c++)
		{
			if(pTree->m_pChildren[c])
			{
				pNewNode->m_pChildren[c] = CloneQuadTree(pTree->m_pChildren[c]);
			}
			else
			{
				pNewNode->m_pChildren[c] = NULL;
			}
		}
	}

	return pNewNode;
}


//*********************************************************
//	GetPolyBelowPoint
//	Gets the first poly directly below the given point.
//	At the moment this is a really inefficient search
//	through all the triangles in the mesh.  Eventually
//	a small quadtree should be used to discard 90% of
//	the triangles early on..
//*********************************************************

void CNavMesh::GetPolyCentroid(u32 iPolyIndex, Vector3 & vCentroid) const
{
	TNavMeshPoly * pPoly = GetPoly(iPolyIndex);
	vCentroid.x = vCentroid.y = vCentroid.z = 0.0f;
	Vector3 vec;
	float fRecip = (1.0f / ((float)(pPoly->GetNumVertices() )));

	for(u32 v=0; v<pPoly->GetNumVertices(); v++)
	{
		GetVertex( GetPolyVertexIndex(pPoly, v), vec);
		vCentroid += vec;
	}

	vCentroid *= fRecip;
}
void CNavMesh::GetPolyCentroidQuick(const TNavMeshPoly * pPoly, Vector3 & vCentroid) const
{
	Assert(pPoly->GetNavMeshIndex() == m_iIndexOfMesh);
	Assert(m_iIndexOfMesh != NAVMESH_INDEX_TESSELLATION);
	Assert(!pPoly->GetIsTessellatedFragment());

	if(GetIsDynamic())
	{
		GetPolyCentroid(GetPolyIndex(pPoly), vCentroid);
		return;
	}

	static const float fRecip256 = (1.0f / 256.0f);

	// unpack into 0..1 range
	vCentroid.x = (((float)pPoly->m_iCentroidX) * fRecip256);
	vCentroid.y = (((float)pPoly->m_iCentroidY) * fRecip256);

	Vector3 vShortMin, vShortMax, vShortExtents;
	pPoly->m_MinMax.GetAsFloats(vShortMin, vShortMax);

	vShortExtents = vShortMax - vShortMin;

	// scale by the total extents of the block of sectors
	vCentroid.x *= vShortExtents.x;
	vCentroid.y *= vShortExtents.y;

	// offset by mins of entire block
	vCentroid.x += vShortMin.x;
	vCentroid.y += vShortMin.y;

	vCentroid.z = (vShortMin.z + vShortMax.z) * 0.5f;
}
CNavMeshQuadTree * CNavMesh::GetQuadTreeLeaf(const Vector3 & vPos)
{
	if(!m_pQuadTree)
		return NULL;
	return GetQuadTreeLeaf(m_pQuadTree, vPos);
}

CNavMeshQuadTree * CNavMesh::GetQuadTreeLeaf(CNavMeshQuadTree * pTree, const Vector3 & vPos)
{
	if(vPos.x >= pTree->m_Mins.x && vPos.x < pTree->m_Maxs.x &&
		vPos.y >= pTree->m_Mins.y && vPos.y < pTree->m_Maxs.y)
	{
		if(pTree->m_pLeafData)
		{
			return pTree;
		}

		for(int c=0; c<4; c++)
		{
			if(pTree->m_pChildren[c])
			{
				CNavMeshQuadTree * pLeaf = GetQuadTreeLeaf(pTree->m_pChildren[c], vPos);
				if(pLeaf)
					return pLeaf;
			}
		}
	}

	return NULL;
}


u32 CNavMesh::GetPolyBelowPoint(const Vector3 & vPoint, Vector3 & vIntersectPoint, float fDistance)
{
	if(m_iFlags & NAVMESH_IS_DYNAMIC)
		CalculateAllPolyMinMaxesForDynamicNavMesh();

	Vector3 vPointBelow;
	vPointBelow.x = vPoint.x;
	vPointBelow.y = vPoint.y;
	vPointBelow.z = vPoint.z - fDistance;

	if(m_pQuadTree)
	{
		return GetIntersectPoly(vPoint, vPointBelow, vIntersectPoint, true);
	}
	else
	{
		return GetIntersectPolyNoQuadTree(vPoint, vPointBelow, vIntersectPoint, true);
	}
}

u32 CNavMesh::GetPolyAbovePoint(const Vector3 & vPoint, Vector3 & vIntersectPoint, float fDistance)
{
	if(m_iFlags & NAVMESH_IS_DYNAMIC)
		CalculateAllPolyMinMaxesForDynamicNavMesh();

	Vector3 vPointAbove;
	vPointAbove.x = vPoint.x;
	vPointAbove.y = vPoint.y;
	vPointAbove.z = vPoint.z + fDistance;

	if(m_pQuadTree)
	{
		return GetIntersectPoly(vPoint, vPointAbove, vIntersectPoint, true);
	}
	else
	{
		return GetIntersectPolyNoQuadTree(vPoint, vPointAbove, vIntersectPoint, true);
	}
}

u32 CNavMesh::GetIntersectPoly(const Vector3 & vPoint, const Vector3 & vEndPoint, Vector3 & vIntersectPoint, bool UNUSED_PARAM(bVerticalLineTest))
{
	if(m_iFlags & NAVMESH_IS_DYNAMIC)
		CalculateAllPolyMinMaxesForDynamicNavMesh();

	Vector3 vTriPts[3];
	Vector3 vEdge1, vEdge2;
	Vector3 vPlaneNormal;

	float fDistSqr;
	float fClosestDistSqr = FLT_MAX;
	u32 iClosestIndex = NAVMESH_POLY_INDEX_NONE;
	Vector3 vTmpIntersection;
	bool bHit;
	u32 lastv, v;

	static const float fExtendZ = 1.0f;
	static const float fExtendXY = 1.0f;

	TShortMinMax minMax;
	float fMinX, fMinY, fMinZ, fMaxX, fMaxY, fMaxZ;

	if(vPoint.x < vEndPoint.x) { fMinX = vPoint.x; fMaxX = vEndPoint.x; }
	else { fMaxX = vPoint.x; fMinX = vEndPoint.x; }
	if(vPoint.y < vEndPoint.y) { fMinY = vPoint.y; fMaxY = vEndPoint.y; }
	else { fMaxY = vPoint.y; fMinY = vEndPoint.y; }
	if(vPoint.z < vEndPoint.z) { fMinZ = vPoint.z; fMaxZ = vEndPoint.z; }
	else { fMaxZ = vPoint.z; fMinZ = vEndPoint.z; }

	minMax.SetFloat(fMinX - fExtendXY, fMinY - fExtendXY, fMinZ - fExtendZ, fMaxX + fExtendXY, fMaxY + fExtendXY, fMaxZ + fExtendZ);

	// Find which quadtree leaf the vPoint & vEndPoint lie in.
	// Since we are only interested in vertical rays for the purpose of this
	// algorithm, both points will be have identical X & Y coords.

	CNavMeshQuadTree * pTree = GetQuadTreeLeaf(vPoint);
	if(!pTree)
		return NAVMESH_POLY_INDEX_NONE;

	Vector3 vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 vMin, vMax;

	//Prefetch the indices, hopefully 2 cache lines is enough?
	//Might want to assert NUM_INDICES_TO_PREFETCH < pTree->m_pLeafData->m_iNumPolys.
	const u32 NUM_INDICES_TO_PREFETCH = 128;
	PrefetchBuffer<sizeof(u16) * NUM_INDICES_TO_PREFETCH>(pTree->m_pLeafData->m_Polys);

	const u32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;

	//Prefetch the first few polys, based on how far we are looking ahead.
	const u32 NUM_POLYS_TO_LOOK_AHEAD_FOR_PREFETCH = 5;
	for(u32 i = 0; (i < NUM_POLYS_TO_LOOK_AHEAD_FOR_PREFETCH + 1) && (i < iNumPolys); ++i)
	{
		Assert(i < m_iNumPolys);
		PrefetchDC(GetPoly(pTree->m_pLeafData->m_Polys[i])); 
	}
	
	u32 t=0;
	u32 index;
	while(t<iNumPolys)
	{
		index = (u32)pTree->m_pLeafData->m_Polys[t++];

		Assert(index < m_iNumPolys);

		//Prefetch the next poly (may be a few iterations into the future).
		const u32 iFutureIndex = t + NUM_POLYS_TO_LOOK_AHEAD_FOR_PREFETCH;
		if(iFutureIndex < iNumPolys) 
		{
			Assert(iFutureIndex < m_iNumPolys);
			PrefetchDC(GetPoly(pTree->m_pLeafData->m_Polys[iFutureIndex])); 
		}

		TNavMeshPoly * pPoly = GetPoly(index);
		if(pPoly->GetIsDisabled())
			continue;

		// Get the points for this polygon.
		// If this is a vertical test, then we can early out if the test point's (x,y) coords
		// are outside the bbox of the polygon.  This should save some effort with the triangle
		// intersection tests.

		// Do approx integer bbox test between poly's XY extents and that of the ray
		if(!minMax.Intersects(pPoly->m_MinMax))
		{
			continue;
		}

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			GetVertex( GetPolyVertexIndex(pPoly, v), vPolyPts[v] );
		}

		vTriPts[0] = vPolyPts[0];

		lastv = 1;
		for(v=2; v<pPoly->GetNumVertices(); v++)
		{
			vTriPts[1] = vPolyPts[v];
			vTriPts[2] = vPolyPts[lastv];

			bHit = CNavMesh::RayIntersectsTriangle(
				vPoint,
				vEndPoint,
				vTriPts,
				vTmpIntersection
			);

			if(bHit)
			{
				fDistSqr = (vTmpIntersection - vPoint).Mag2();
				if(fDistSqr < fClosestDistSqr)
				{
					fClosestDistSqr = fDistSqr;
					iClosestIndex = index;
					vIntersectPoint = vTmpIntersection;
				}
			}

			lastv = v;
		}
	}

	return iClosestIndex;
}

u32 CNavMesh::GetIntersectPolyNoQuadTree(const Vector3 & vPoint, const Vector3 & vEndPoint, Vector3 & vIntersectPoint, bool UNUSED_PARAM(bVerticalLineTest))
{
	if(m_iFlags & NAVMESH_IS_DYNAMIC)
		CalculateAllPolyMinMaxesForDynamicNavMesh();

	static const float fExtendZ = 1.0f;
	static const float fExtendXY = 1.0f;

	TShortMinMax minMax;
	float fMinX, fMinY, fMinZ, fMaxX, fMaxY, fMaxZ;

	if(vPoint.x < vEndPoint.x) { fMinX = vPoint.x; fMaxX = vEndPoint.x; }
	else { fMaxX = vPoint.x; fMinX = vEndPoint.x; }
	if(vPoint.y < vEndPoint.y) { fMinY = vPoint.y; fMaxY = vEndPoint.y; }
	else { fMaxY = vPoint.y; fMinY = vEndPoint.y; }
	if(vPoint.z < vEndPoint.z) { fMinZ = vPoint.z; fMaxZ = vEndPoint.z; }
	else { fMaxZ = vPoint.z; fMinZ = vEndPoint.z; }

	minMax.SetFloat(fMinX - fExtendXY, fMinY - fExtendXY, fMinZ - fExtendZ, fMaxX + fExtendXY, fMaxY + fExtendXY, fMaxZ + fExtendZ);

	Vector3 vTriPts[3];
	Vector3 vEdge1, vEdge2;
	Vector3 vPlaneNormal;

	float fDistSqr;
	float fClosestDistSqr = FLT_MAX;
	u32 iClosestIndex = NAVMESH_POLY_INDEX_NONE;
	bool bHit;
	u32 lastv, v;

	Vector3 vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 vMin, vMax;

	for(u32 index=0; index<m_iNumPolys; index++)
	{
		TNavMeshPoly * pPoly = GetPoly(index);
		if(pPoly->GetIsDisabled())
			continue;

		if(pPoly->TestFlags(NAVMESHPOLY_REPLACED_BY_TESSELLATION))
			continue;

		if(pPoly->TestFlags(NAVMESHPOLY_DEGENERATE_CONNECTION_POLY))
			continue;

		// Do approx integer bbox test between poly's XY extents and that of the ray
		if(!minMax.Intersects(pPoly->m_MinMax))
		{
			continue;
		}

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			GetVertex( GetPolyVertexIndex(pPoly, v), vPolyPts[v] );
		}

		vTriPts[0] = vPolyPts[0];

		lastv = 1;
		for(v=2; v<pPoly->GetNumVertices(); v++)
		{
			vTriPts[1] = vPolyPts[v];
			vTriPts[2] = vPolyPts[lastv];

			bHit = CNavMesh::RayIntersectsTriangle(
				vPoint,
				vEndPoint,
				vTriPts,
				vIntersectPoint
			);

			if(bHit)
			{
				fDistSqr = (vIntersectPoint - vPoint).Mag2();
				if(fDistSqr < fClosestDistSqr)
				{
					fClosestDistSqr = fDistSqr;
					iClosestIndex = index;
				}
			}

			lastv = v;
		}
	}

	return iClosestIndex;
}

u32 CNavMesh::GetPolyMostlyWithinVolume(const Vector3 & vMin, const Vector3 & vMax, bool bOnlyOnPavement, bool bClosestToCentre)
{
	ms_fGetPolyBestOverlap = 0.0f;
	ms_iGetPolyBestIndex = NAVMESH_POLY_INDEX_NONE;
	ms_bGetPolyOnlyOnPavement = bOnlyOnPavement;
	ms_bGetPolyClosestToCentre = bClosestToCentre;
	ms_bGetPolyFoundAnyPoly = false;
	ms_vGetPolyVolumeCentre = (vMin + vMax) * 0.5f;
	ms_fGetPolyLeastDistSqr = FLT_MAX;

	Assert(m_pQuadTree);

	GetPolyMostlyWithinVolumeR(m_pQuadTree, vMin, vMax);

	return ms_iGetPolyBestIndex;
}

// The input vMin & vMax, are the mins & maxs of the volume which we are interested in

void CNavMesh::GetPolyMostlyWithinVolumeR(const CNavMeshQuadTree * pTree, const Vector3 & vMin, const Vector3 & vMax)
{
	if(pTree->m_pLeafData)
	{
		for(int p=0; p<pTree->m_pLeafData->m_iNumPolys; p++)
		{
			u16 iPolyIndex = pTree->m_pLeafData->m_Polys[p];
			TNavMeshPoly * pPoly = GetPoly(iPolyIndex);

			if(pPoly->GetIsDisabled())
				continue;

			// Check if we only want polys which are on a pavement
			if(ms_bGetPolyOnlyOnPavement && !(pPoly->TestFlags(NAVMESHPOLY_IS_PAVEMENT)))
			{
				continue;
			}

			// Are we getting the 'best' poly based upon proximity to the centre of the passed-in volume?
			if(ms_bGetPolyClosestToCentre)
			{
				Vector3 vCentroid;
				GetPolyCentroid(iPolyIndex, vCentroid);

				float fDistSqr = (vCentroid - ms_vGetPolyVolumeCentre).Mag2();
				if(fDistSqr < ms_fGetPolyLeastDistSqr)
				{
					ms_fGetPolyLeastDistSqr = fDistSqr;
					ms_iGetPolyBestIndex = iPolyIndex;
					ms_bGetPolyFoundAnyPoly = true;
				}
			}
			// Or are we basing it upon the greatest axis-aligned overlap with the passed-in volume
			else
			{
				Vector3 vPolyMin(FLT_MAX, FLT_MAX, FLT_MAX);
				Vector3 vPolyMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
				Vector3 vec;

				for(u32 v=0; v<pPoly->GetNumVertices(); v++)
				{
					GetVertex( GetPolyVertexIndex(pPoly, v), vec );
					if(vec.x < vPolyMin.x) vPolyMin.x = vec.x;
					if(vec.y < vPolyMin.y) vPolyMin.y = vec.y;
					if(vec.z < vPolyMin.z) vPolyMin.z = vec.z;
					if(vec.x > vPolyMax.x) vPolyMax.x = vec.x;
					if(vec.y > vPolyMax.y) vPolyMax.y = vec.y;
					if(vec.z > vPolyMax.z) vPolyMax.z = vec.z;
				}

				if(vPolyMin.x > vMax.x || vPolyMin.y > vMax.y || vPolyMin.z > vMax.z ||
					vMin.x > vPolyMax.x || vMin.y > vPolyMax.y || vMin.z > vPolyMax.z)
				{
					continue;
				}

				// Now make an estimate of the extent of the overlap
				Vector3 vOverlapMin, vOverlapMax;
				vOverlapMin.x = Max(vPolyMin.x, vMin.x);
				vOverlapMin.y = Max(vPolyMin.y, vMin.y);
				vOverlapMax.x = Min(vPolyMax.x, vMax.x);
				vOverlapMax.y = Min(vPolyMax.y, vMax.y);

				float fOverlapSize = (vOverlapMax.x - vOverlapMin.x) * (vOverlapMax.y - vOverlapMin.y);
				if(fOverlapSize > ms_fGetPolyBestOverlap)
				{
					ms_fGetPolyBestOverlap = fOverlapSize;
					ms_iGetPolyBestIndex = iPolyIndex;
					ms_bGetPolyFoundAnyPoly = true;
				}
			}
		}
		return;
	}

	for(int c=0; c<4; c++)
	{
		if(pTree->m_pChildren[c]->m_Mins.x > vMax.x || pTree->m_pChildren[c]->m_Mins.y > vMax.y ||
			vMin.x > pTree->m_pChildren[c]->m_Maxs.x || vMin.y > pTree->m_pChildren[c]->m_Maxs.y)
		{
			// No overlap
		}
		else
		{
			GetPolyMostlyWithinVolumeR(pTree->m_pChildren[c], vMin, vMax);
		}
	}			
}

bool CNavMesh::RayIntersectsPoly(const Vector3 & vRayStart, const Vector3 & vRayEnd, const TNavMeshPoly * pPoly, Vector3 & vecIntersect) const
{
	if(pPoly->GetNavMeshIndex() != (u32)m_iIndexOfMesh)
	{
		Assert(0);
		return false;
	}

	TShortMinMax minMax;
	float fMinX, fMinY, fMinZ, fMaxX, fMaxY, fMaxZ;

	static dev_float fExtend = 1.0f;

	if(vRayStart.x < vRayEnd.x) { fMinX = vRayStart.x; fMaxX = vRayEnd.x; }
	else { fMaxX = vRayStart.x; fMinX = vRayEnd.x; }
	if(vRayStart.y < vRayEnd.y) { fMinY = vRayStart.y; fMaxY = vRayEnd.y; }
	else { fMaxY = vRayStart.y; fMinY = vRayEnd.y; }
	if(vRayStart.z < vRayEnd.z) { fMinZ = vRayStart.z; fMaxZ = vRayEnd.z; }
	else { fMaxZ = vRayStart.z; fMinZ = vRayEnd.z; }

	minMax.SetFloat(fMinX-fExtend, fMinY-fExtend, fMinZ-fExtend, fMaxX+fExtend, fMaxY+fExtend, fMaxZ+fExtend);

	if(!minMax.Intersects(pPoly->m_MinMax))
	{
		return false;
	}

	Vector3 vPolyIntersectPts[NAVMESHPOLY_MAX_NUM_VERTICES];
	for(u32 v=0; v<pPoly->GetNumVertices(); v++)
	{
		GetVertex( GetPolyVertexIndex(pPoly, v), vPolyIntersectPts[v]);
	}

	return RayIntersectsPoly(vRayStart, vRayEnd, pPoly->GetNumVertices(), vPolyIntersectPts, vecIntersect);
}

bool CNavMesh::RayIntersectsPoly(const Vector3 & vRayStart, const Vector3 & vRayEnd, int iNumPts, Vector3 * pVerts, Vector3 & vecIntersect)
{
	Vector3 vTriPts[3];
	Vector3 vEdge1, vEdge2;
	int lastv, v;

	vTriPts[0] = pVerts[0];

	lastv = 1;
	for(v=2; v<iNumPts; v++)
	{
		vTriPts[1] = pVerts[v];
		vTriPts[2] = pVerts[lastv];

		if(CNavMesh::RayIntersectsTriangle(vRayStart, vRayEnd, vTriPts, vecIntersect))
		{
			return true;
		}

		lastv = v;
	}

	return false;
}


Vector3 gGetClosestPolyImmediateMin(0.0f,0.0f,0.0f);
Vector3 gGetClosestPolyImmediateMax(0.0f,0.0f,0.0f);
Vector3 gGetClosestPolyImmediatePolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
bool gGetClosestPolyImmediateMustHavePedDensityOrPavement = false;
bool gGetClosestPolyImmediateMustNotBeIsolated = false;

TNavMeshPoly * CNavMesh::GetClosestNavMeshPolyImmediate(const Vector3 & vPos, const float fTestRadius, const bool bMustHavePedDensityOrPavement, const bool bMustNotBeIsolated)
{
	Assertf(m_pQuadTree, "CNavMesh::GetClosestNavMeshPolyImmediate - this function can only be called on navmeshes with quadtrees");

	TShortMinMax minMax;
	minMax.SetFloat(vPos.x - fTestRadius, vPos.y - fTestRadius, vPos.z - fTestRadius, vPos.x + fTestRadius, vPos.y + fTestRadius, vPos.z + fTestRadius);

	gGetClosestPolyImmediateMin = vPos - Vector3(fTestRadius,fTestRadius,fTestRadius);
	gGetClosestPolyImmediateMax = vPos + Vector3(fTestRadius,fTestRadius,fTestRadius);
	gGetClosestPolyImmediateMustHavePedDensityOrPavement = bMustHavePedDensityOrPavement;
	gGetClosestPolyImmediateMustNotBeIsolated = bMustNotBeIsolated;

	float fBestDistSqr = FLT_MAX;
	TNavMeshPoly * pClosestPoly = NULL;

	GetClosestNavMeshPolyImmediateR(m_pQuadTree, vPos, minMax, fBestDistSqr, pClosestPoly);

	return pClosestPoly;
}

void CNavMesh::GetClosestNavMeshPolyImmediateR(const CNavMeshQuadTree * pTree, const Vector3 & vPos, const TShortMinMax & searchMinMax, float & fBestDistSqr, TNavMeshPoly *& pClosestPoly)
{
	if(pTree->m_pLeafData)
	{
		s32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;
		u16 * pPolyIndices = pTree->m_pLeafData->m_Polys;

		for(int p=0; p<iNumPolys; p++)
		{
			TNavMeshPoly * pPoly = m_PolysArray->Get( pPolyIndices[p] );
			if(pPoly->GetIsDisabled())
				continue;

			if(gGetClosestPolyImmediateMustHavePedDensityOrPavement && (pPoly->GetPedDensity()==0 && !pPoly->GetIsPavement()))
				continue;

			if(gGetClosestPolyImmediateMustNotBeIsolated && pPoly->GetIsIsolated())
				continue;

			if(pPoly->m_MinMax.Intersects(searchMinMax))
			{
				u32 v;

				for(v=0; v<pPoly->GetNumVertices(); v++)
				{
					GetVertex( GetPolyVertexIndex(pPoly, v), gGetClosestPolyImmediatePolyPts[v]);
				}

				u32 lastv = pPoly->GetNumVertices()-1;
				for(v=0; v<pPoly->GetNumVertices(); v++)
				{
					const Vector3 vEdge = gGetClosestPolyImmediatePolyPts[v] - gGetClosestPolyImmediatePolyPts[lastv];
					const float t = (vEdge.Mag2() > 0.0f) ? geomTValues::FindTValueSegToPoint(gGetClosestPolyImmediatePolyPts[lastv], vEdge, vPos) : 0.0f;
					const Vector3 vClosePos = gGetClosestPolyImmediatePolyPts[lastv] + (vEdge * t);
					const Vector3 vDiff = vClosePos - vPos;
					const float fDistSqr = vDiff.Mag2();

					if(fDistSqr < fBestDistSqr)
					{
						fBestDistSqr = fDistSqr;
						pClosestPoly = pPoly;
					}
				}
			}
		}

		return;
	}
	else
	{
		for(s32 c=0; c<4; c++)
		{
			if(pTree->m_pChildren[c]->m_Mins.x > gGetClosestPolyImmediateMax.x || pTree->m_pChildren[c]->m_Mins.y > gGetClosestPolyImmediateMax.y ||
				gGetClosestPolyImmediateMin.x > pTree->m_pChildren[c]->m_Maxs.x || gGetClosestPolyImmediateMin.y > pTree->m_pChildren[c]->m_Maxs.y)
			{
				// No overlap
			}
			else
			{
				GetClosestNavMeshPolyImmediateR( pTree->m_pChildren[c], vPos, searchMinMax, fBestDistSqr, pClosestPoly );
			}
		}
	}
}

bool GetClosestPoly_IsPosClearOfAvoidSpheres(const Vector3 & vPos, TGetPolyEdgeStruct & es)
{
	const Vec3V pos = RCC_VEC3V(vPos);
	u32 s;
	for(s=0; s<es.g_iGetClosestPolyNumAvoidSpheres; s++)
	{
		// Close in XY
		if (es.g_iGetClosestPolyAvoidSpheresArray[s].ContainsPointFlat(pos))
		{
			// Closer than a ped's height
			const ScalarV dz = Abs(pos - es.g_iGetClosestPolyAvoidSpheresArray[s].GetCenter()).GetZ();
			if (IsLessThanAll(dz, ScalarV(V_THREE)) != 0)
				break;
		}
	}
	return(s==es.g_iGetClosestPolyNumAvoidSpheres);
}


TNavMeshPoly * CNavMesh::GetClosestNavMeshPolyEdge(const Vector3 & vPos, const float fMaxDistToLook, Vector3 & vOutClosestPosOnEdge, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, bool bGetCentroidInsteadOfEdge, const u32 iGetClosestFlags, const int iNumAvoidSpheres, const spdSphere * pAvoidSpheresArray, const Vector3* pvPolySearchDir)
{
	if(m_iFlags & NAVMESH_IS_DYNAMIC)
		CalculateAllPolyMinMaxesForDynamicNavMesh();

	TGetPolyEdgeStruct es;

	es.g_vGetPolyEdgeMins = Vector3(vPos.x-fMaxDistToLook, vPos.y-fMaxDistToLook, vPos.z-fMaxDistToLook);
	es.g_vGetPolyEdgeMaxs = Vector3(vPos.x+fMaxDistToLook, vPos.y+fMaxDistToLook, vPos.z+fMaxDistToLook);

	es.g_vGetPolyEdgeMins.x = Max(es.g_vGetPolyEdgeMins.x - MINMAX_RESOLUTION, -MINMAX_MAX_FLOAT_VAL);
	es.g_vGetPolyEdgeMins.y = Max(es.g_vGetPolyEdgeMins.y - MINMAX_RESOLUTION, -MINMAX_MAX_FLOAT_VAL);
	es.g_vGetPolyEdgeMins.z = Max(es.g_vGetPolyEdgeMins.z - MINMAX_RESOLUTION, -MINMAX_MAX_FLOAT_VAL);
	es.g_vGetPolyEdgeMaxs.x = Min(es.g_vGetPolyEdgeMaxs.x + MINMAX_RESOLUTION, MINMAX_MAX_FLOAT_VAL);
	es.g_vGetPolyEdgeMaxs.y = Min(es.g_vGetPolyEdgeMaxs.y + MINMAX_RESOLUTION, MINMAX_MAX_FLOAT_VAL);
	es.g_vGetPolyEdgeMaxs.z = Min(es.g_vGetPolyEdgeMaxs.z + MINMAX_RESOLUTION, MINMAX_MAX_FLOAT_VAL);

	if (pvPolySearchDir)
		es.g_vSearchDirection = *pvPolySearchDir;

	TShortMinMax minMax;
	minMax.SetFloat(es.g_vGetPolyEdgeMins.x, es.g_vGetPolyEdgeMins.y, es.g_vGetPolyEdgeMins.z, es.g_vGetPolyEdgeMaxs.x, es.g_vGetPolyEdgeMaxs.y, es.g_vGetPolyEdgeMaxs.z);

	es.g_vGetPolyEdgeInputPos = vPos;
	es.g_fGetPolyEdgeClosestDistSqr = FLT_MAX;
	es.g_fGetPolyEdgeMaxDistSqr = fMaxDistToLook*fMaxDistToLook;
	es.g_iGetPolyEdgeBestPoly = NAVMESH_POLY_INDEX_NONE;
	es.g_iGetPolyEdgeBestEdge = 0;
	es.g_iGetClosestPolyNumAvoidSpheres = iNumAvoidSpheres;
	es.g_iGetClosestPolyAvoidSpheresArray = pAvoidSpheresArray;

	if(!m_pQuadTree)
	{
		Assert((m_iFlags & NAVMESH_IS_DYNAMIC)!=0);
		GetClosestNavMeshPolyEdgeNoQuadTree(minMax, acceptPolyFn, acceptPointFn, iGetClosestFlags, es);
	}
	else
	{
		GetClosestNavMeshPolyEdgeR(minMax, m_pQuadTree, acceptPolyFn, acceptPointFn, iGetClosestFlags, es);
	}

	if(es.g_iGetPolyEdgeBestPoly == NAVMESH_POLY_INDEX_NONE)
	{
		return NULL;
	}
	
	TNavMeshPoly * pPoly = m_PolysArray->Get( es.g_iGetPolyEdgeBestPoly );

	if(bGetCentroidInsteadOfEdge)
	{
		GetPolyCentroidQuick(pPoly, vOutClosestPosOnEdge);
	}
	else
	{
		Vector3 vLastVert, vVert;
		const int lastv = es.g_iGetPolyEdgeBestEdge;
		const int v = (es.g_iGetPolyEdgeBestEdge+1) % pPoly->GetNumVertices();
		GetVertex(GetPolyVertexIndex(pPoly, lastv), vLastVert);
		GetVertex(GetPolyVertexIndex(pPoly, v), vVert);
		const Vector3 vEdge = vVert - vLastVert;
		const float t = (vEdge.Mag2() > SMALL_FLOAT) ? geomTValues::FindTValueSegToPoint(vLastVert, vEdge, vPos) : 0.0f;
		vOutClosestPosOnEdge = vLastVert + (vEdge * t);
	}

	return pPoly;
}

void CNavMesh::GetClosestNavMeshPolyEdgeR(const TShortMinMax & minMax, const CNavMeshQuadTree * pTree, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es)
{
	if(pTree->m_pLeafData)
	{
		if(pTree->m_pLeafData->m_iNumPolys)
		{
			s32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;
			u16 * pPolyIndices = pTree->m_pLeafData->m_Polys;
			float fDistSqr;

			for(s32 p=0; p<iNumPolys; p++)
			{
				u16 iIndex = pPolyIndices[p];
				TNavMeshPoly * pPoly = m_PolysArray->Get(iIndex);

				if(pPoly->GetIsDisabled())
					continue;

				if(!pPoly->m_MinMax.Intersects(minMax))
					continue;

				// Optional per-poly callback to reject polys
				if(acceptPolyFn && !acceptPolyFn(this, pPoly))
					continue;

				u32 v;
				for(v=0; v<pPoly->GetNumVertices(); v++)
				{
					GetVertex( GetPolyVertexIndex(pPoly, v), es.g_vGetPolyEdge[v] );
				}

				s32 lastv = pPoly->GetNumVertices()-1;
				for(v=0; v<pPoly->GetNumVertices(); v++)
				{
					const Vector3 vEdge = es.g_vGetPolyEdge[v] - es.g_vGetPolyEdge[lastv];
					const float t = (vEdge.Mag2() > 0.0f) ? geomTValues::FindTValueSegToPoint(es.g_vGetPolyEdge[lastv], vEdge, es.g_vGetPolyEdgeInputPos) : 0.0f;
					const Vector3 vClosePos = es.g_vGetPolyEdge[lastv] + (vEdge * t);

					Vector3 vDiff = vClosePos - es.g_vGetPolyEdgeInputPos;
					if(vDiff.Dot(es.g_vSearchDirection) < -0.01f) // Epsilon
						vDiff *= 2.0f;	// Might want to use tune
					if(iGetClosestFlags & GetClosestPos_PreferSameHeight)
						vDiff.z *= 2.0f;

					fDistSqr = vDiff.Mag2();
					if(fDistSqr < es.g_fGetPolyEdgeMaxDistSqr && fDistSqr < es.g_fGetPolyEdgeClosestDistSqr)
					{
						if(es.g_iGetClosestPolyNumAvoidSpheres && !GetClosestPoly_IsPosClearOfAvoidSpheres(vClosePos, es))
						{
							// do nothing
						}
						// Optional per-point callback to reject points
						else if(!acceptPointFn || acceptPointFn(this, pPoly, vClosePos))
						{
							es.g_fGetPolyEdgeClosestDistSqr = fDistSqr;
							es.g_iGetPolyEdgeBestPoly = iIndex;
							es.g_iGetPolyEdgeBestEdge = lastv;
						}
					}
					lastv = v;
				}
			}
		}

		return;
	}
	else
	{
		for(s32 c=0; c<4; c++)
		{
			CNavMeshQuadTree * pChild = pTree->m_pChildren[c];
			if(!pChild || es.g_vGetPolyEdgeMins.x > pChild->m_Maxs.x || es.g_vGetPolyEdgeMins.y > pChild->m_Maxs.y ||
				pChild->m_Mins.x > es.g_vGetPolyEdgeMaxs.x || pChild->m_Mins.y > es.g_vGetPolyEdgeMaxs.y)
			{

			}
			else
			{
				GetClosestNavMeshPolyEdgeR(minMax, pChild, acceptPolyFn, acceptPointFn, iGetClosestFlags, es);
			}
		}
	}
}

void CNavMesh::GetClosestNavMeshPolyEdgeNoQuadTree(TShortMinMax & minMax, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es)
{
	for(u32 p=0; p<m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = m_PolysArray->Get(p);

		if(pPoly->GetIsDisabled())
			continue;

		if(!pPoly->m_MinMax.Intersects(minMax))
			continue;

		// Optional per-poly callback to reject polys
		if(acceptPolyFn && !acceptPolyFn(this, pPoly))
			continue;

		u32 v;
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			GetVertex( GetPolyVertexIndex(pPoly, v), es.g_vGetPolyEdge[v] );
		}

		s32 lastv = pPoly->GetNumVertices()-1;
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			const Vector3 vEdge = es.g_vGetPolyEdge[v] - es.g_vGetPolyEdge[lastv];
			const float t = (vEdge.Mag2() > 0.0f) ? geomTValues::FindTValueSegToPoint(es.g_vGetPolyEdge[lastv], vEdge, es.g_vGetPolyEdgeInputPos) : 0.0f;
			const Vector3 vClosePos = es.g_vGetPolyEdge[lastv] + (vEdge * t);

			Vector3 vDiff = vClosePos - es.g_vGetPolyEdgeInputPos;
			if(vDiff.Dot(es.g_vSearchDirection) < -0.01f) // Epsilon
				vDiff *= 2.0f;	// Might want to use tune
			if(iGetClosestFlags & GetClosestPos_PreferSameHeight)
				vDiff.z *= 2.0f;

			const float fDistSqr = vDiff.Mag2();
			if(fDistSqr < es.g_fGetPolyEdgeMaxDistSqr && fDistSqr < es.g_fGetPolyEdgeClosestDistSqr)
			{
				if(es.g_iGetClosestPolyNumAvoidSpheres && !GetClosestPoly_IsPosClearOfAvoidSpheres(vClosePos, es))
				{
					// do nothing
				}

				// Optional per-point callback to reject points
				else if(!acceptPointFn || acceptPointFn(this, pPoly, vClosePos))
				{
					es.g_fGetPolyEdgeClosestDistSqr = fDistSqr;
					es.g_iGetPolyEdgeBestPoly = p;
					es.g_iGetPolyEdgeBestEdge = lastv;
				}
			}

			lastv = v;
		}
	}
}


TNavMeshPoly * CNavMesh::GetClosestPointInPoly(const Vector3 & vPos, const float fMaxDistToLook, Vector3 & vOutClosestPointInPoly, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn ,const u32 iGetClosestFlags, const int iNumAvoidSpheres, const spdSphere * pAvoidSpheresArray)
{
	if(m_iFlags & NAVMESH_IS_DYNAMIC)
		CalculateAllPolyMinMaxesForDynamicNavMesh();

	TGetPolyEdgeStruct es;

	es.g_vGetPolyEdgeMins = Vector3(vPos.x-fMaxDistToLook, vPos.y-fMaxDistToLook, vPos.z-fMaxDistToLook);
	es.g_vGetPolyEdgeMaxs = Vector3(vPos.x+fMaxDistToLook, vPos.y+fMaxDistToLook, vPos.z+fMaxDistToLook);

	TShortMinMax minMax;
	minMax.SetFloat(es.g_vGetPolyEdgeMins.x, es.g_vGetPolyEdgeMins.y, es.g_vGetPolyEdgeMins.z, es.g_vGetPolyEdgeMaxs.x, es.g_vGetPolyEdgeMaxs.y, es.g_vGetPolyEdgeMaxs.z);

	es.g_vGetPolyEdgeInputPos = vPos;
	es.g_fGetPolyEdgeClosestDistSqr = FLT_MAX;
	es.g_fGetPolyEdgeMaxDistSqr = fMaxDistToLook*fMaxDistToLook;
	es.g_iGetPolyEdgeBestPoly = NAVMESH_POLY_INDEX_NONE;
	es.g_iGetPolyEdgeBestEdge = 0;
	es.g_iGetClosestPolyNumAvoidSpheres = iNumAvoidSpheres;
	es.g_iGetClosestPolyAvoidSpheresArray = pAvoidSpheresArray;

	if(!m_pQuadTree)
	{
		Assert((m_iFlags & NAVMESH_IS_DYNAMIC)!=0);
		GetClosestPointInPolyNoQuadTree(minMax, acceptPolyFn, acceptPointFn, iGetClosestFlags, es);
	}
	else
	{
		GetClosestPointInPolyR(minMax, m_pQuadTree, acceptPolyFn, acceptPointFn, iGetClosestFlags, es);
	}

	if(es.g_iGetPolyEdgeBestPoly == NAVMESH_POLY_INDEX_NONE)
	{
		return NULL;
	}

	vOutClosestPointInPoly = es.g_vGetClosestPointInPoly;

	return m_PolysArray->Get( es.g_iGetPolyEdgeBestPoly );
}

void CNavMesh::GetClosestPointInPolyR(const TShortMinMax & minMax, const CNavMeshQuadTree * pTree, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es) const
{
	if(pTree->m_pLeafData)
	{
		if(pTree->m_pLeafData->m_iNumPolys)
		{
			const s32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;
			const u16 * pPolyIndices = pTree->m_pLeafData->m_Polys;
			float fDistSqr;

			for(s32 p=0; p<iNumPolys; p++)
			{
				const u16 iIndex = pPolyIndices[p];
				const TNavMeshPoly * pPoly = m_PolysArray->Get( iIndex );

				if(pPoly->GetIsDisabled())
					continue;

				if(!pPoly->m_MinMax.Intersects(minMax))
					continue;

				// Optional per-poly callback to reject polys
				if(acceptPolyFn && !acceptPolyFn(this, pPoly))
					continue;

				u32 v;
				for(v=0; v<pPoly->GetNumVertices(); v++)
				{
					GetVertex( GetPolyVertexIndex(pPoly, v), es.g_vGetPolyEdge[v] );
				}

				const u32 iNumPts = CreatePointsInPoly(
					this,
					pPoly,
					es.g_vGetPolyEdge,
					POINTSINPOLY_EXTRA,
					es.g_vGetClosestPointInPolyPts,
					true
				);

				for(v=0; v<iNumPts; v++)
				{
					Vector3 vDiff = es.g_vGetPolyEdgeInputPos - es.g_vGetClosestPointInPolyPts[v];
					if(iGetClosestFlags & GetClosestPos_PreferSameHeight)
						vDiff.z *= 2.0f;
					fDistSqr = vDiff.Mag2();

					if(fDistSqr < es.g_fGetPolyEdgeMaxDistSqr && fDistSqr < es.g_fGetPolyEdgeClosestDistSqr)
					{
						if(es.g_iGetClosestPolyNumAvoidSpheres && !GetClosestPoly_IsPosClearOfAvoidSpheres(es.g_vGetClosestPointInPolyPts[v], es))
						{

						}
						// Optional per-poly callback to reject polys
						else if(!acceptPointFn || acceptPointFn(this, pPoly, es.g_vGetClosestPointInPolyPts[v]))
						{
							es.g_fGetPolyEdgeClosestDistSqr = fDistSqr;
							es.g_vGetClosestPointInPoly = es.g_vGetClosestPointInPolyPts[v];
							es.g_iGetPolyEdgeBestPoly = iIndex;
						}
					}
				}
			}
		}

		return;
	}
	else
	{
		for(s32 c=0; c<4; c++)
		{
			const CNavMeshQuadTree * pChild = pTree->m_pChildren[c];
			if(!pChild || es.g_vGetPolyEdgeMins.x > pChild->m_Maxs.x || es.g_vGetPolyEdgeMins.y > pChild->m_Maxs.y ||
				pChild->m_Mins.x > es.g_vGetPolyEdgeMaxs.x || pChild->m_Mins.y > es.g_vGetPolyEdgeMaxs.y)
			{

			}
			else
			{
				GetClosestPointInPolyR(minMax, pChild, acceptPolyFn, acceptPointFn, iGetClosestFlags, es);
			}
		}
	}
}

void CNavMesh::GetClosestPointInPolyNoQuadTree(const TShortMinMax & minMax, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, const u32 iGetClosestFlags, TGetPolyEdgeStruct & es) const
{
	for(u32 p=0; p<m_iNumPolys; p++)
	{
		const TNavMeshPoly * pPoly = m_PolysArray->Get(p);

		if(pPoly->GetIsDisabled())
			continue;

		if(!pPoly->m_MinMax.Intersects(minMax))
			continue;

		// Optional per-poly callback to reject polys
		if(acceptPolyFn && !acceptPolyFn(this, pPoly))
			continue;

		u32 v;
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			GetVertex( GetPolyVertexIndex(pPoly, v), es.g_vGetPolyEdge[v] );
		}

		const u32 iNumPts = CreatePointsInPoly(
			this,
			pPoly,
			es.g_vGetPolyEdge,
			POINTSINPOLY_NORMAL,
			es.g_vGetClosestPointInPolyPts,
			true);

		for(v=0; v<iNumPts; v++)
		{
			Vector3 vDiff = es.g_vGetPolyEdgeInputPos - es.g_vGetClosestPointInPolyPts[v];
			if(iGetClosestFlags & GetClosestPos_PreferSameHeight)
				vDiff.z *= 2.0f;
			const float fDistSqr = vDiff.Mag2();

			if(fDistSqr < es.g_fGetPolyEdgeMaxDistSqr && fDistSqr < es.g_fGetPolyEdgeClosestDistSqr)
			{
				if(es.g_iGetClosestPolyNumAvoidSpheres && !GetClosestPoly_IsPosClearOfAvoidSpheres(es.g_vGetClosestPointInPolyPts[v], es))
				{

				}
				else if( !acceptPointFn || acceptPointFn(this, pPoly, es.g_vGetClosestPointInPolyPts[v]) )
				{
					es.g_fGetPolyEdgeClosestDistSqr = fDistSqr;
					es.g_vGetClosestPointInPoly = es.g_vGetClosestPointInPolyPts[v];
					es.g_iGetPolyEdgeBestPoly = p;
				}
			}
		}
	}
}

void CNavMesh::ForAllPolys(const Vector3 & vSearchOrigin, const float fSearchDist, AcceptPolyCB acceptPolyFn, AcceptPointCB acceptPointFn, u32 iForAllPolysFlags)
{
	TForAllPolysStruct searchStruct;
	searchStruct.m_vSearchOrigin = vSearchOrigin;
	searchStruct.m_fSearchRadius = fSearchDist;
	searchStruct.m_AcceptPolyFn = acceptPolyFn;
	searchStruct.m_AcceptPointFn = acceptPointFn;
	searchStruct.m_iFlags = iForAllPolysFlags;

	searchStruct.m_SearchMinMax.SetFloat(
		vSearchOrigin.x - fSearchDist, vSearchOrigin.y - fSearchDist, vSearchOrigin.z - fSearchDist,
		vSearchOrigin.x + fSearchDist, vSearchOrigin.y + fSearchDist, vSearchOrigin.z + fSearchDist);

	if(m_pQuadTree)
	{
		ForAllPolys_R(m_pQuadTree, &searchStruct);
	}
	else
	{
		ForAllPolys_NoQuadTree(&searchStruct);
	}
}
void CNavMesh::ForAllPolys_R(const CNavMeshQuadTree * pTree, TForAllPolysStruct * pSearchStruct) const
{
	if(pTree->m_pLeafData)
	{
		if(pTree->m_pLeafData->m_iNumPolys)
		{
			const s32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;
			const u16 * pPolyIndices = pTree->m_pLeafData->m_Polys;

			for(s32 p=0; p<iNumPolys; p++)
			{
				const u16 iIndex = pPolyIndices[p];
				const TNavMeshPoly * pPoly = m_PolysArray->Get( iIndex );

				if(pPoly->GetIsDisabled())
					continue;

				if(!pPoly->m_MinMax.Intersects(pSearchStruct->m_SearchMinMax))
					continue;

				// Optional per-poly callback to reject polys
				if(pSearchStruct->m_AcceptPolyFn && !pSearchStruct->m_AcceptPolyFn(this, pPoly))
					continue;

				// Only process any further if we have been given a per-point callback to invoke
				if(pSearchStruct->m_AcceptPointFn)
				{
					u32 v;
					for(v=0; v<pPoly->GetNumVertices(); v++)
					{
						GetVertex( GetPolyVertexIndex(pPoly, v), pSearchStruct->m_vPolyVertices[v] );
					}

					u32 iNumPts;

					if((pSearchStruct->m_iFlags & TForAllPolysStruct::FLAG_MULTIPLE_POINTS_PER_POLYGON)!=0)
					{
						iNumPts = CreatePointsInPoly(
							this,
							pPoly,
							pSearchStruct->m_vPolyVertices,
							POINTSINPOLY_EXTRA,
							pSearchStruct->m_vPointsInPolygon,
							true);
					}
					else
					{
						iNumPts = 1;
						GetPolyCentroidQuick(pPoly, pSearchStruct->m_vPointsInPolygon[0]);
					}

					for(v=0; v<iNumPts; v++)
					{
						pSearchStruct->m_AcceptPointFn(this, pPoly, pSearchStruct->m_vPointsInPolygon[v]);
					}
				}
			}
		}

		return;
	}
	else
	{
		for(s32 c=0; c<4; c++)
		{
			const CNavMeshQuadTree * pChild = pTree->m_pChildren[c];
			if(pChild && pChild->m_MinMax.IntersectsXY(pSearchStruct->m_SearchMinMax))
			{
				ForAllPolys_R(pChild, pSearchStruct);
			}
		}
	}
}

void CNavMesh::ForAllPolys_NoQuadTree(TForAllPolysStruct * pSearchStruct) const
{
	u32 iNumPolys = GetNumPolys();
	for(u32 p=0; p<iNumPolys; p++)
	{
		const TNavMeshPoly * pPoly = m_PolysArray->Get(p);

		if(pPoly->GetIsDisabled())
			continue;

		if(!pPoly->m_MinMax.Intersects(pSearchStruct->m_SearchMinMax))
			continue;

		// Optional per-poly callback to reject polys
		if(pSearchStruct->m_AcceptPolyFn && !pSearchStruct->m_AcceptPolyFn(this, pPoly))
			continue;

		// Only process any further if we have been given a per-point callback to invoke
		if(pSearchStruct->m_AcceptPointFn)
		{
			u32 v;
			for(v=0; v<pPoly->GetNumVertices(); v++)
			{
				GetVertex( GetPolyVertexIndex(pPoly, v), pSearchStruct->m_vPolyVertices[v] );
			}

			u32 iNumPts;

			if((pSearchStruct->m_iFlags & TForAllPolysStruct::FLAG_MULTIPLE_POINTS_PER_POLYGON)!=0)
			{
				iNumPts = CreatePointsInPoly(
					this,
					pPoly,
					pSearchStruct->m_vPolyVertices,
					POINTSINPOLY_EXTRA,
					pSearchStruct->m_vPointsInPolygon,
					true);
			}
			else
			{
				iNumPts = 1;
				GetPolyCentroidQuick(pPoly, pSearchStruct->m_vPointsInPolygon[0]);
			}

			for(v=0; v<iNumPts; v++)
			{
				pSearchStruct->m_AcceptPointFn(this, pPoly, pSearchStruct->m_vPointsInPolygon[v]);
			}
		}
	}
}

// NAME : DisableSpecialLinksFromPosition
// PURPOSE : Disable any special links whose 'from' position is within 'fMaxFistToLook' distance of 'vPos'
bool CNavMesh::DisableSpecialLinksFromPosition(const Vector3 & vPos, const float fMaxDistToLook)
{
	int iNumDisabled = 0;
	Vector3 vFromPos;
	for(u32 s=0; s<m_iNumSpecialLinks; s++)
	{
		if(!m_SpecialLinks[s].GetIsDisabled())
		{
			DecompressVertex(vFromPos, m_SpecialLinks[s].GetLinkFromPosX(), m_SpecialLinks[s].GetLinkFromPosY(), m_SpecialLinks[s].GetLinkFromPosZ());

			if(vPos.IsClose(vFromPos, fMaxDistToLook))
			{
				m_SpecialLinks[s].SetIsDisabled(true);
				iNumDisabled++;
			}
		}
	}
	return (iNumDisabled != 0);
}

// NAME : DisableSpecialLinksSpanningPosition
// PURPOSE : Disable any special links for which vPos is spanned by their 'from' and 'to' positions
bool CNavMesh::DisableSpecialLinksSpanningPosition(const Vector3 & vPos, const float fMaxDistToLook)
{
	int iNumDisabled = 0;
	Vector3 vFromPos, vToPos;
	for(u32 s=0; s<m_iNumSpecialLinks; s++)
	{
		if(!m_SpecialLinks[s].GetIsDisabled())
		{
			DecompressVertex(vFromPos, m_SpecialLinks[s].GetLinkFromPosX(), m_SpecialLinks[s].GetLinkFromPosY(), m_SpecialLinks[s].GetLinkFromPosZ());
			DecompressVertex(vToPos, m_SpecialLinks[s].GetLinkToPosX(), m_SpecialLinks[s].GetLinkToPosY(), m_SpecialLinks[s].GetLinkToPosZ());

			if(vPos.IsClose(vFromPos, fMaxDistToLook) && vPos.IsClose(vToPos, fMaxDistToLook))
			{
				Vector3 vVec = vToPos - vFromPos;
				vVec.z = 0.0f;
				vVec.Normalize();
				const float fD = - DotProduct(vVec, vFromPos);			// plane eq
				const float fToDist = DotProduct(vVec, vToPos) + fD;	// distance of vToPos
				const float fPosDist = DotProduct(vVec, vPos) + fD;		// planar distance of input vPos

				if(fPosDist > 0.0f && fPosDist < fToDist)
				{
					m_SpecialLinks[s].SetIsDisabled(true);
					iNumDisabled++;
				}
			}
		}
	}
	return (iNumDisabled != 0);
}

Vector3 g_vGetNonStandardAdjacencyMins(0.0f,0.0f,0.0f);
Vector3 g_vGetNonStandardAdjacencyMaxs(0.0f,0.0f,0.0f);
Vector3 g_vGetNonStandardAdjacencyInputPos(0.0f,0.0f,0.0f);

bool CNavMesh::DisableNonStandardAdjacancyAtPosition(const Vector3 & vPos, const float fMaxDistToLook)
{
	TAdjPoly * pAdjPoly = NULL;
	bool bFoundAdjacency = GetNonStandardAdjacancyAtPosition(vPos, fMaxDistToLook, pAdjPoly);
	if(bFoundAdjacency && pAdjPoly)
	{
		// Set the adjacency for this edge to be disabled : it will not be used during routes.
		pAdjPoly->SetAdjacencyDisabled(true);
	}

	return bFoundAdjacency;
}

bool CNavMesh::GetNonStandardAdjacancyAtPosition(const Vector3 & vPos, const float fMaxDistToLook, TAdjPoly *& pAdjPoly)
{
	g_vGetNonStandardAdjacencyMins = Vector3(vPos.x-fMaxDistToLook, vPos.y-fMaxDistToLook, vPos.z-fMaxDistToLook);
	g_vGetNonStandardAdjacencyMaxs = Vector3(vPos.x+fMaxDistToLook, vPos.y+fMaxDistToLook, vPos.z+fMaxDistToLook);

	TShortMinMax minMax;
	minMax.SetFloat(
		g_vGetNonStandardAdjacencyMins.x, g_vGetNonStandardAdjacencyMins.y, g_vGetNonStandardAdjacencyMins.z,
		g_vGetNonStandardAdjacencyMaxs.x, g_vGetNonStandardAdjacencyMaxs.y, g_vGetNonStandardAdjacencyMaxs.z
		);

	g_vGetNonStandardAdjacencyInputPos = vPos;

	bool bFoundAdjacency = GetNonStandardAdjacancyAtPositionR(m_pQuadTree, minMax, pAdjPoly);

	return bFoundAdjacency;
}

bool CNavMesh::GetNonStandardAdjacancyAtPositionR(const CNavMeshQuadTree * pTree, const TShortMinMax & minMax, TAdjPoly *& pAdjPoly)
{
	if(pTree->m_pLeafData)
	{
		if(pTree->m_pLeafData->m_iNumPolys)
		{
			static const float fSameAdjacencyEps = 0.1f;
			static const float fSameAdjacencyEpsZ = 2.0f;
			Vector3 vVert, vLastVert, vMid;
			s32 iNumPolys = pTree->m_pLeafData->m_iNumPolys;
			u16 * pPolyIndices = pTree->m_pLeafData->m_Polys;

			for(s32 p=0; p<iNumPolys; p++)
			{
				u16 iIndex = pPolyIndices[p];
				TNavMeshPoly * pPoly = m_PolysArray->Get(iIndex);

				if(pPoly->GetIsDisabled())
					continue;

				if(!pPoly->m_MinMax.Intersects(minMax))
					continue;

				u32 lastv = pPoly->GetNumVertices()-1;
				for(u32 v=0; v<pPoly->GetNumVertices(); v++)
				{
					const TAdjPoly & adjPoly = GetAdjacentPoly( pPoly->GetFirstVertexIndex() + lastv );
					if(adjPoly.GetAdjacencyType()!=ADJACENCY_TYPE_NORMAL)
					{
						GetVertex( GetPolyVertexIndex(pPoly, lastv), vLastVert );
						GetVertex( GetPolyVertexIndex(pPoly, v), vVert );
						vMid = (vLastVert + vVert) * 0.5f;

						if(rage::IsNearZero(vMid.x - g_vGetNonStandardAdjacencyInputPos.x, fSameAdjacencyEps) &&
							rage::IsNearZero(vMid.y - g_vGetNonStandardAdjacencyInputPos.y, fSameAdjacencyEps) &&
							rage::IsNearZero(vMid.z - g_vGetNonStandardAdjacencyInputPos.z, fSameAdjacencyEpsZ))
						{
							// Set the adjacency for this edge to be disabled : it will not be used during routes.
							pAdjPoly = m_AdjacentPolysArray->Get(pPoly->GetFirstVertexIndex() + lastv);
							return true;
						}
					}
					lastv = v;
				}
			}
		}
	}
	else
	{
		for(s32 c=0; c<4; c++)
		{
			CNavMeshQuadTree * pChild = pTree->m_pChildren[c];
			if(g_vGetNonStandardAdjacencyMins.x > pChild->m_Maxs.x || g_vGetNonStandardAdjacencyMins.y > pChild->m_Maxs.y ||
				pChild->m_Mins.x > g_vGetNonStandardAdjacencyMaxs.x || pChild->m_Mins.y > g_vGetNonStandardAdjacencyMaxs.y)
			{

			}
			else
			{
				if(GetNonStandardAdjacancyAtPositionR(pChild, minMax, pAdjPoly))
					return true;	// return true if found
			}
		}
	}
	return false;
}


//****************************************************
// Returns whether the ray intersects the triangle
//****************************************************
#define SMALL_NUM		0.00000001f

bool CNavMesh::RayIntersectsTriangle(const Vector3 & vRayStart, const Vector3 & vRayEnd, Vector3 * pVerts, Vector3 & vecIntersect)
{
	Vector3 u, v, n;
	Vector3 dir, w0, w;
	float r, a, b;

	u = pVerts[1] - pVerts[0];
	v = pVerts[2] - pVerts[0];
	n.Cross(u, v);
	if(n.Mag2() < SMALL_NUM)
	{
		return false;
	}

	dir = vRayEnd - vRayStart;
	w0 = vRayStart - pVerts[0];
	a = - Dot(n, w0);
	b = Dot(n, dir);
	if(Abs(b) < SMALL_NUM)
	{
		// NB : Could be parallel
		return false;
	}

	r = a / b;
	if(r < 0.0f || r > 1.0f)
	{
		return false;
	}

	vecIntersect = vRayStart + r * dir;

	float uu,uv,vv,wu,wv,D;
	uu = Dot(u,u);
	uv = Dot(u,v);
	vv = Dot(v,v);
	w = vecIntersect - pVerts[0];
	wu = Dot(w,u);
	wv = Dot(w,v);
	D = uv * uv - uu * vv;

	float s,t;
	s = (uv * wv - vv * wu) / D;
	if(s < 0.0f || s > 1.0f)
		return false;
	
	t = (uv * wu - uu * wv) / D;
	if(t < 0.0f || (s+t) > 1.0f)
		return false;

	return true;
}



//*******************************************************************************
//	Returns whether two line-segments intersect
//	NB: this isn't actually accurate - it ignores the colinear & parallel cases,
//  but for our purposes it is sufficient.
int CNavMesh::LineSegsIntersect2D(const Vector3 & A, const Vector3 & B, const Vector3 & C, const Vector3 & D, Vector3 * pvIsectPos)
{
	// Have an epsilon test for points A & B, which are always the 'waypoints-in-triangle'.
	// If signs are the same, and either is very close to the triangle edge - then force a result.
	static const float fOnLineEps = 0.001f;

	const Vector2 vA(A.x, A.y);
	const Vector2 vB(B.x, B.y);
	const Vector2 vC(C.x, C.y);
	const Vector2 vD(D.x, D.y);

	Vector2 vABEdge = vB - vA;
	vABEdge.Normalize();

	Vector2 vCDEdge = vD - vC;
	vCDEdge.Normalize();

	const Vector2 vABNormal(vABEdge.y, -vABEdge.x);
	const Vector2 vCDNormal(vCDEdge.y, -vCDEdge.x);

	const float ABPlaneDist = - Dot(vABNormal, vA);
	const float CDPlaneDist = - Dot(vCDNormal, vC);

	// classify A wrt CD
	const float ADist = Dot(vA, vCDNormal) + CDPlaneDist;

	// classify B wrt CD
	const float BDist = Dot(vB, vCDNormal) + CDPlaneDist;

	// classify C wrt AB
	const float CDist = Dot(vC, vABNormal) + ABPlaneDist;

	// classify D wrt AB
	const float DDist = Dot(vD, vABNormal) + ABPlaneDist;

	// get signs
	int iSignA = (int)Sign(ADist);
	int iSignB = (int)Sign(BDist);
	int iSignC = (int)Sign(CDist);
	int iSignD = (int)Sign(DDist);

	if(iSignA == 0) iSignA = -1;
	if(iSignB == 0) iSignB = -1;
	if(iSignC == 0) iSignC = -1;
	if(iSignD == 0) iSignD = -1;

	// If both points are on same side of line & are very close, then force an intersection
	if(iSignA != 0 && (iSignA == iSignB) && ((Abs(ADist) < fOnLineEps) || (Abs(BDist) < fOnLineEps)))
	{
		iSignB = -iSignA;
	}

	if(pvIsectPos)
	{
		const float fDivisor = (CDist - DDist);
		if(Abs(fDivisor) > 0.00001f)
		{
			const float s = (CDist / fDivisor);
			*pvIsectPos = C + ((D-C) * s);
		}
		else
		{
			return PARALLEL;
		}
	}

	if(iSignA == 0 || iSignB == 0 || iSignC == 0 || iSignD == 0)
	{
		return LINES_INTERSECT;
	}

	if(iSignA != iSignB && iSignC != iSignD)
	{
		return SEGMENTS_INTERSECT;
	}

	return LINES_INTERSECT;
}


void CNavMesh::AllocateAsTessellationMesh(const u32 iNumPolys, const u32 iNumVertices)
{
	m_iFlags = 0;
	m_iNumPolys = iNumPolys;
	m_iNumVertices = iNumVertices;

	m_CompressedVertexArray = NULL;	// we don't use compressed vertices in the tessellation mesh

	m_iSizeOfPools = iNumVertices;

	m_VertexPool = rage_new Vector3[iNumVertices];

	m_AdjacentMeshes.Reset();

	m_VertexIndexArray = rage_new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(m_iSizeOfPools);
	m_AdjacentPolysArray = rage_new aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(m_iSizeOfPools);	// TODO: Do we perhaps want to make a special case aiSplitArray here with no limit on blocksize?
	m_PolysArray = rage_new aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE>(iNumPolys);	// TODO: Do we perhaps want to make a special case aiSplitArray here with no limit on blocksize?

	m_pQuadTree = NULL;
	m_vExtents = Vector3(0,0,0);
	m_iIndexOfMesh = NAVMESH_INDEX_TESSELLATION;
	m_iTotalMemoryUsed = 0;	// NB : Must fix this

	m_iTotalMemoryUsed += sizeof(TNavMeshPoly) * iNumPolys;

	memset(m_VertexPool, 0, sizeof(Vector3) * iNumVertices);

	m_VertexIndexArray->SetZero();
	m_AdjacentPolysArray->SetZero();
	m_PolysArray->SetZero();

	for(u32 v=0; v<iNumVertices; v++)
	{
		*m_VertexIndexArray->Get(v) = (u16) v;
	}

	int iVertIndex = 0;
	for(u32 p=0; p<iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = GetPoly(p);

		pPoly->SetFirstVertexIndex(iVertIndex);
		pPoly->SetNumVertices(3);
		pPoly->SetNavMeshIndex(NAVMESH_INDEX_TESSELLATION);
		pPoly->SetFlags(0);

		// Store which subarray this poly belongs to, which increases efficiency of splitarray lookup
		pPoly->m_Struct4.m_iPolyArrayIndex = m_PolysArray->GetSubArrayIndex(p);

		iVertIndex += 3;
	}
}













//***********************************************************************************************************
// I had to move this CHierarchicalNavData stuff into "Navmesh.cpp" instead of "Pathserver_Hierarchical.cpp"
// because Ragebuilder needs this stuff, and we can't include "Pathserver_Hierarchical.cpp" without also
// including half the know universe.  Yes, its a pain.

void CHierarchicalNavLink::GetLinkExtents(const Vector3 & vNodePos, const Vector3 & vLinkedNodePos, Vector3 & vOut_NearLeft, Vector3 & vOut_NearRight, Vector3 & vOut_FarLeft, Vector3 & vOut_FarRight)
{
	Vector3 vLinkVec = vLinkedNodePos - vNodePos;
	vLinkVec.Normalize();
	const Vector3 vTangent = CrossProduct(vLinkVec, ZAXIS);

	const float fWidthLeft = (float) GetWidthToLeft();
	vOut_NearLeft = vNodePos - (vTangent*fWidthLeft);
	vOut_FarLeft = vLinkedNodePos - (vTangent*fWidthLeft);

	const float fWidthRight = (float) GetWidthToRight();
	vOut_NearRight = vNodePos + (vTangent*fWidthRight);
	vOut_FarRight = vLinkedNodePos + (vTangent*fWidthRight);
}

const u32 CHierarchicalNavData::ms_iFileVersion = 0x00010003;

CHierarchicalNavData::CHierarchicalNavData()
{
	m_iFlags = 0;
	m_iNumNodes = 0;
	m_iNumLinks = 0;
	m_pNodes = NULL;
	m_pLinks = NULL;
	m_vMins = Vector3(0.0f, 0.0f, 0.0f);
	m_vMaxs = Vector3(0.0f, 0.0f, 0.0f);
	m_vSize = Vector3(0.0f, 0.0f, 0.0f);
}

bool CHierarchicalNavData::Save(const char * pFilename, const CHierarchicalNavData * pHierNav)
{
	if(!pFilename || !*pFilename)
		return false;
	fiStream * pStream = fiStream::Create(pFilename);
	if(!pStream)
	{
		Errorf("fiStream::Create(%s) was unsuccessful.", pFilename);
		return false;
	}

	// Write the file version
	pStream->Write(&ms_iFileVersion, sizeof(u32));

	// Write the flags, num nodes & links
	pStream->Write(&pHierNav->m_iFlags, sizeof(u32));
	pStream->Write(&pHierNav->m_iNumNodes, sizeof(u32));
	pStream->Write(&pHierNav->m_iNumLinks, sizeof(u32));

	// Write the extents
	pStream->Write(&pHierNav->m_vMins.x, sizeof(float));
	pStream->Write(&pHierNav->m_vMins.y, sizeof(float));
	pStream->Write(&pHierNav->m_vMins.z, sizeof(float));
	pStream->Write(&pHierNav->m_vMaxs.x, sizeof(float));
	pStream->Write(&pHierNav->m_vMaxs.y, sizeof(float));
	pStream->Write(&pHierNav->m_vMaxs.z, sizeof(float));
	pStream->Write(&pHierNav->m_vSize.x, sizeof(float));
	pStream->Write(&pHierNav->m_vSize.y, sizeof(float));
	pStream->Write(&pHierNav->m_vSize.z, sizeof(float));

	// Write the nodes & links
	u32 n,l;

	for(n=0; n<pHierNav->m_iNumNodes; n++)
	{
		CHierarchicalNavNode & node = pHierNav->m_pNodes[n];
		pStream->Write(&node.m_iAsInteger1, 4);
		pStream->Write(&node.m_iAsInteger2, 4);
		pStream->Write(&node.m_iAsInteger3, 4);
		pStream->Write(&node.m_iAsInteger4, 4);

		const u32 iNumLinks = node.GetNumLinks();
		for(l=0; l<iNumLinks; l++)
		{
			CHierarchicalNavLink & link = pHierNav->m_pLinks[node.GetStartOfLinkData()+l];
			pStream->Write(&link.m_iAsInteger1, 4);
			pStream->Write(&link.m_iAsInteger2, 2);
		}
	}
	pStream->Close();
	return true;
}

CHierarchicalNavData * CHierarchicalNavData::LoadFromMemory(void * pMemBuffer, int iBufferSize)
{
	char filename[256];
	fiDeviceMemory::MakeMemoryFileName(filename, 256, pMemBuffer, iBufferSize, false, "HierarchicalNodes");
	return CHierarchicalNavData::Load(filename);
}

CHierarchicalNavData * CHierarchicalNavData::Load(const char * pFilename)
{
	if(!pFilename || !*pFilename)
		return NULL;
	fiStream * pStream = ASSET.Open(pFilename, "", true);
	if(!pStream)
		return NULL;

	// Read the file version
	u32 iFileVersion;
	pStream->Read(&iFileVersion, 4);
	ByteSwap32(&iFileVersion);

#if __CHECK_NAVMESH_FILE_VERSION
	if(iFileVersion < ms_iFileVersion)
	{
		// Loading previous versions of the ".inh" file is not supported..
		Errorf("Error - loading incompatible version of CHierarchicalNavData");
		Assertf(iFileVersion == ms_iFileVersion, "The hierarchical nav file(s) are older than is supported by this executable.\nTry getting a newer version of the navmeshes image, or using a previous exe file.\n");
		pStream->Close();
		return NULL;
	}
	if(iFileVersion > ms_iFileVersion)
	{
		// Loading newer versions of the ".inh" file is not supported..
		Errorf("Error - loading incompatible version of CHierarchicalNavData");
		Assertf(iFileVersion == ms_iFileVersion, "The hierarchical nav file(s) are newer than is supported by this executable.\nTry getting a newer exe file, or using a previous version of the navmeshes image.\n");
		pStream->Close();
		return NULL;
	}
#endif	// __CHECK_NAVMESH_FILE_VERSION

	CNavMeshAlloc<CHierarchicalNavData> navAlloc;
	CHierarchicalNavData * pHierNav = navAlloc.New();

	pStream->Read(&pHierNav->m_iFlags, 4);
	ByteSwap32(&pHierNav->m_iFlags);
	pStream->Read(&pHierNav->m_iNumNodes, 4);
	ByteSwap32(&pHierNav->m_iNumNodes);
	pStream->Read(&pHierNav->m_iNumLinks, 4);
	ByteSwap32(&pHierNav->m_iNumLinks);

	// Read the extents
	pStream->Read(&pHierNav->m_vMins.x, 4);		ByteSwap32(&pHierNav->m_vMins.x);
	pStream->Read(&pHierNav->m_vMins.y, 4);		ByteSwap32(&pHierNav->m_vMins.y);
	pStream->Read(&pHierNav->m_vMins.z, 4);		ByteSwap32(&pHierNav->m_vMins.z);
	pStream->Read(&pHierNav->m_vMaxs.x, 4);		ByteSwap32(&pHierNav->m_vMaxs.x);
	pStream->Read(&pHierNav->m_vMaxs.y, 4);		ByteSwap32(&pHierNav->m_vMaxs.y);
	pStream->Read(&pHierNav->m_vMaxs.z, 4);		ByteSwap32(&pHierNav->m_vMaxs.z);
	pStream->Read(&pHierNav->m_vSize.x, 4);		ByteSwap32(&pHierNav->m_vSize.x);
	pStream->Read(&pHierNav->m_vSize.y, 4);		ByteSwap32(&pHierNav->m_vSize.y);
	pStream->Read(&pHierNav->m_vSize.z, 4);		ByteSwap32(&pHierNav->m_vSize.z);

	// Read nodes & links
	CNavMeshAlloc<CHierarchicalNavNode> nodeAlloc;
	CNavMeshAlloc<CHierarchicalNavLink> linkAlloc;

	pHierNav->m_pNodes = nodeAlloc.VectorNew(pHierNav->m_iNumNodes);
	pHierNav->m_pLinks = linkAlloc.VectorNew(pHierNav->m_iNumLinks);

	u32 n,l;

	for(n=0; n<pHierNav->m_iNumNodes; n++)
	{
		CHierarchicalNavNode & node = pHierNav->m_pNodes[n];

		// Read node position data & flags. These are all packed into one u32
		pStream->Read(&node.m_iAsInteger1, 4);
		ByteSwap32(&node.m_iAsInteger1);

		// Read navmesh index, start of link data & num links. These are also all packed into one u32
		pStream->Read(&node.m_iAsInteger2, 4);
		ByteSwap32(&node.m_iAsInteger2);

		// Read ped-density, node area, node radius, and other attributes
		pStream->Read(&node.m_iAsInteger3, 4);
		ByteSwap32(&node.m_iAsInteger3);

		// Read parent navmesh, node index, flags, etc.
		// This data needn't be resourced at all - but the alternative it to allocate a separate
		// struct on load-time and this is a pain in the ass & fucks with the cache access patterns.
		pStream->Read(&node.m_iAsInteger4, 4);
		ByteSwap32(&node.m_iAsInteger4);

		// BUGGED - James knows about it, also happens on dev branch
		// Assert(node.GetNumLinks() > 0 && node.GetNumLinks() <= MAX_NUM_HIERACHICAL_LINKS);

		// Read the links for this node
		const u32 iNumLinks = node.GetNumLinks();
		Assert(iNumLinks>0 && iNumLinks <= MAX_NUM_HIERACHICAL_LINKS);

		for(l=0; l<iNumLinks; l++)
		{
			const int iLinkIndex = node.GetStartOfLinkData()+l;
			CHierarchicalNavLink & link = pHierNav->m_pLinks[iLinkIndex];

			// Read the link's navmesh index, poly index etc.  All in one u32.
			pStream->Read(&link.m_iAsInteger1, 4);
			ByteSwap32(&link.m_iAsInteger1);

			// Read the link's width to left/right (u16)
			pStream->Read(&link.m_iAsInteger2, 2);
			ByteSwap16(&link.m_iAsInteger2);
		}

		// Causes a PS3 compile error as GetNumLinks can never be 0
// #if __DEV
// 		Assert(node.GetNumLinks()!=0);
// #endif
	}

	pStream->Close();
	return pHierNav;
}

CHierarchicalNavData *
CHierarchicalNavData::StreamingPlaceFn(s32 NOTFINAL_ONLY(index), datResourceMap& map, datResourceInfo& header)
{
	CHierarchicalNavData * pNavData = NULL;

#if __FINAL
	pgRscBuilder::PlaceStream(pNavData, header, map, "<unknown>");
#else
	char tmp[64];
	sprintf(tmp, "HierarchicalNavData StreamingIndex:%i", index);
	pgRscBuilder::PlaceStream(pNavData, header, map, tmp);
#endif

	return pNavData;
}

void CHierarchicalNavData::Place(CHierarchicalNavData* that, datResource& rsc)
{
	::new (that) CHierarchicalNavData(rsc);
}

CHierarchicalNavData::CHierarchicalNavData(datResource& rsc)
{
	rsc.PointerFixup(m_pNodes);
	rsc.PointerFixup(m_pLinks);
}

#if __DECLARESTRUCT
void CHierarchicalNavData::DeclareStruct(datTypeStruct & s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(CHierarchicalNavData, pgBase)
		SSTRUCT_DYNAMIC_ARRAY(CHierarchicalNavData, m_pNodes, m_iNumNodes)
		SSTRUCT_FIELD(CHierarchicalNavData,m_iNumNodes)
		SSTRUCT_FIELD(CHierarchicalNavData,m_vMins)
		SSTRUCT_FIELD(CHierarchicalNavData,m_vMaxs)
		SSTRUCT_FIELD(CHierarchicalNavData,m_vSize)
		SSTRUCT_DYNAMIC_ARRAY(CHierarchicalNavData, m_pLinks, m_iNumLinks)
		SSTRUCT_FIELD(CHierarchicalNavData,m_iNumLinks)
		SSTRUCT_FIELD(CHierarchicalNavData,m_iFlags)
		SSTRUCT_IGNORE(CHierarchicalNavData,m_Padding)
	SSTRUCT_END(CHierarchicalNavData)
}
void CHierarchicalNavNode::DeclareStruct(datTypeStruct & s)
{
	STRUCT_BEGIN(CHierarchicalNavNode);
	STRUCT_FIELD(m_iAsInteger1);
	STRUCT_FIELD(m_iAsInteger2);
	STRUCT_FIELD(m_iAsInteger3);
	STRUCT_FIELD(m_iAsInteger4);
	STRUCT_END();
}

void CHierarchicalNavLink::DeclareStruct(datTypeStruct & s)
{
	STRUCT_BEGIN(CHierarchicalNavLink);
	STRUCT_FIELD(m_iAsInteger1);
	STRUCT_FIELD(m_iAsInteger2);
	STRUCT_END();
}

#endif	// __DECLARESTRUCT


void CHierarchicalNavData::ForAllNodesInArea(const Vector3 & vMin, const Vector3 & vMax, NavNodeIntersectingCB callBack, void * pData)
{
	Vector3 vNodePos;
	for(u32 n=0; n<m_iNumNodes; n++)
	{
		CHierarchicalNavNode * pNode = &m_pNodes[n];
		pNode->GetNodePosition(vNodePos, m_vMins, m_vSize);
		if(vNodePos.x >= vMin.x && vNodePos.y >= vMin.y && vNodePos.z >= vMin.z &&
			vNodePos.x < vMax.x && vNodePos.y < vMax.y && vNodePos.z < vMax.z)
		{
			callBack(pNode, vNodePos, pData);
		}
	}
}

void SwitchNodeOnForPedsCB(CHierarchicalNavNode * pNode, const Vector3 & UNUSED_PARAM(vNodePos), void * UNUSED_PARAM(pData))
{
	pNode->SetPedSpawningEnabled(true);
}
void SwitchNodeOffForPedsCB(CHierarchicalNavNode * pNode, const Vector3 & UNUSED_PARAM(vNodePos), void * UNUSED_PARAM(pData))
{
	pNode->SetPedSpawningEnabled(false);
}

void CHierarchicalNavData::SwitchPedSpawningInArea(const Vector3 & vMin, const Vector3 & vMax, const bool bSwitchedOn)
{
	if(vMin.x > m_vMaxs.x || vMin.y > m_vMaxs.y || vMin.z > m_vMaxs.z || m_vMins.x > vMax.x || m_vMins.y > vMax.y || m_vMins.z > vMax.z)
		return;

	if(bSwitchedOn)
	{
		ForAllNodesInArea(vMin, vMax, SwitchNodeOnForPedsCB, NULL);
	}
	else
	{
		ForAllNodesInArea(vMin, vMax, SwitchNodeOffForPedsCB, NULL);
	}
}


