//
// ai/navmesh/requests.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "requests.h"

NAVMESH_OPTIMISATIONS()

using namespace rage;

//-----------------------------------------------------------------------------

const u32 CPathServerRequestBase::ms_iRequestTimeoutValInMillisecs = 30000;

bool CAudioRequest::InsertAudioProperty(const float fDistSqr, const u32 iValue, const Vector3 & vPosition, const float fPolyArea, const u8 iAdditionalFlags)
{
	s32 i;
	for(i=0; i<m_iNumAudioProperties; i++)
	{
		if(fDistSqr < m_fDistSqrFromOrigin[i])
		{
			s32 iEnd = m_iNumAudioProperties;
			if(m_iNumAudioProperties == MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES)
				iEnd--;
			while(iEnd > i)
			{
				m_iAudioProperties[iEnd] = m_iAudioProperties[iEnd-1];
				m_fPolyAreas[iEnd] = m_fPolyAreas[iEnd-1];
				m_vPolyCentroids[iEnd] = m_vPolyCentroids[iEnd-1];
				m_fDistSqrFromOrigin[iEnd] = m_fDistSqrFromOrigin[iEnd-1];
				m_iAdditionalFlags[iEnd] = m_iAdditionalFlags[iEnd-1];
				iEnd--;
			}

			m_iAudioProperties[i] = iValue;
			m_fPolyAreas[i] = fPolyArea;
			m_vPolyCentroids[i] = vPosition;
			m_fDistSqrFromOrigin[i] = fDistSqr;
			m_iAdditionalFlags[i] = iAdditionalFlags;

			m_iNumAudioProperties = Min(m_iNumAudioProperties+1, MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES);
			return true;
		}
	}
	if(i==MAX_SURROUNDING_POLYS_FOR_AUDIO_PROPERTIES)
		return false;

	m_iAudioProperties[m_iNumAudioProperties] = iValue;
	m_fPolyAreas[m_iNumAudioProperties] = fPolyArea;
	m_vPolyCentroids[m_iNumAudioProperties] = vPosition;
	m_fDistSqrFromOrigin[m_iNumAudioProperties] = fDistSqr;
	m_iAdditionalFlags[m_iNumAudioProperties] = iAdditionalFlags;
	m_iNumAudioProperties++;

	return true;
}

//-----------------------------------------------------------------------------

// End of file ai/navmesh/requests.cpp
