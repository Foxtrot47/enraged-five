#include "tessellation.h"
#include "pathserverbase.h"
#include "navmeshoptimisations.h"
#include "requests.h"

#include "vector/geometry.h"
#include "fwmaths/vector.h"

NAVMESH_OPTIMISATIONS()

using namespace rage;


#define __TESSELLATE_USE_EXPANDED_CONNECTION_POLYS		0
#define __TESSELLATED_POLYS_USE_PARENTS_MINMAX			1
#define __CONNECTION_POLYS_USE_PARENTS_MINMAX			1
#define __REUSE_TESSELLATION_POLYS						0	// Makes better use of tessellation pool, makes for very complex code (NB: not working)

//-------------------------------------------------------------------------------
// FUNCTION : CreateTessellationNavMesh
// PURPOSE : Creates the single instance of CNavMesh used to allocate temporary
// polygons during the tessellation of the navmesh around dynamic object.

bool fwPathServer::CreateTessellationNavMesh()
{
	if(m_pTessellationNavMesh)
	{
		delete m_pTessellationNavMesh;
		m_pTessellationNavMesh = NULL;
	}
	if(m_PolysTessellatedFrom)
	{
		delete[] m_PolysTessellatedFrom;
		m_PolysTessellatedFrom = NULL;
	}

#if __DEV
	const int iMemUsedBeforeAllocation = m_iTotalMemoryUsed;
#endif

	int iNumPolys = CNavMesh::ms_iNumPolysInTesselationNavMesh;
	int iNumVertices = iNumPolys * 3;

	m_pTessellationNavMesh = rage_new CNavMesh();
	m_iTotalMemoryUsed += sizeof(CNavMesh);

	m_pTessellationNavMesh->AllocateAsTessellationMesh(iNumPolys, iNumVertices);

	m_iTotalMemoryUsed += sizeof(Vector3) * iNumVertices;
	m_iTotalMemoryUsed += sizeof(u16) * iNumVertices;
	m_iTotalMemoryUsed += sizeof(TAdjPoly) * iNumVertices;

	m_PolysTessellatedFrom = rage_new TTessInfo[CNavMesh::ms_iNumPolysInTesselationNavMesh];
	m_iTotalMemoryUsed += sizeof(TTessInfo) * CNavMesh::ms_iNumPolysInTesselationNavMesh;

	for(int p=0; p<iNumPolys; p++)
	{
		TTessInfo * pTessInfo = GetTessInfo(p);
		pTessInfo->m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		pTessInfo->m_iPolyIndex = NAVMESH_POLY_INDEX_NONE;
		pTessInfo->m_bInUse = false;
	}

	m_iCurrentNumTessellationPolys = 0;
	m_iCurrentTessellationArrayIndex = 0;
	m_bHasBeenThroughEntireTessellationNavMesh = false;

#if __DEV
	const int iMemUsedByTessellationNavmesh = m_iTotalMemoryUsed - iMemUsedBeforeAllocation;
	Displayf("Memory used by m_pTessellationNavMesh = %i Kb\n", iMemUsedByTessellationNavmesh/1024);
#endif

	return true;
}


#if __TESSELLATE_USE_EXPANDED_CONNECTION_POLYS
	static const float fExpandWeighting = 0.2f;
	static const float fOtherVertWeighting = (1.0f - fExpandWeighting) * 0.5f;
#endif


//-----------------------------------------------------------------------------------
// FUNCTION : DetessellateAllPolys
// PURPOSE : Removes all navmesh tessellation restore adjacencies to original state

void fwPathServer::DetessellateAllPolys(void)
{
#if !__FINAL
	ms_iPeakNumTessellationPolys = Max(m_iCurrentNumTessellationPolys, ms_iPeakNumTessellationPolys);
#endif

	u32 p,a,b;

	// At this time, this function is only set up to operate in the regular mesh domain.
	const aiNavDomain domain = kNavDomainRegular;

	const u32 iNumTessPolys = m_bHasBeenThroughEntireTessellationNavMesh ? 
		CNavMesh::ms_iNumPolysInTesselationNavMesh : m_iCurrentNumTessellationPolys;

	for(p=0; p<iNumTessPolys; p++)
	{
		TTessInfo * pTessInfo = GetTessInfo(p);
		pTessInfo->m_bInUse = false;

		TNavMeshPoly * pFragment = m_pTessellationNavMesh->GetPoly(p);

		if(pFragment->GetIsDegenerateConnectionPoly())
		{
			pFragment->SetIsDegenerateConnectionPoly(false);
		}
		else
		{
			u32 iOriginalNavMesh = pTessInfo->m_iNavMeshIndex;
			u32 iOriginalPoly = pTessInfo->m_iPolyIndex;

			Assert((pFragment->GetIsTessellatedFragment() && iOriginalNavMesh != NAVMESH_NAVMESH_INDEX_NONE && iOriginalPoly != NAVMESH_POLY_INDEX_NONE) ||
				(!pFragment->GetIsTessellatedFragment() && iOriginalNavMesh == NAVMESH_NAVMESH_INDEX_NONE && iOriginalPoly == NAVMESH_POLY_INDEX_NONE));

			CNavMesh * pNavMesh = GetNavMeshFromIndex(iOriginalNavMesh, domain);

			if(pNavMesh)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(iOriginalPoly);

				// There may be several fragments taken from this original poly.
				// Only the first time we see it, do we want to process it.

				if(pPoly->GetReplacedByTessellation())
				{
					Assert(pPoly->GetNavMeshIndex() != NAVMESH_INDEX_TESSELLATION);

					pPoly->AndFlags(~NAVMESHPOLY_REPLACED_BY_TESSELLATION);

					// Reset adjacency
					for(a=0; a<pPoly->GetNumVertices(); a++)
					{
						TAdjPoly & adjPoly = *(pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + a));
						const u32 iAdjOriginalNavMesh = adjPoly.GetOriginalNavMeshIndex(pNavMesh->GetAdjacentMeshes());
						adjPoly.SetNavMeshIndex(iAdjOriginalNavMesh, pNavMesh->GetAdjacentMeshes());
						adjPoly.SetPolyIndex(adjPoly.GetOriginalPolyIndex());

						const u32 iAdjNavMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());

						Assert(iAdjNavMesh != NAVMESH_INDEX_TESSELLATION);

						if(iAdjNavMesh != NAVMESH_NAVMESH_INDEX_NONE)
						{
							CNavMesh * pAdjNavMesh = GetNavMeshFromIndex(iAdjNavMesh, domain);

							if(pAdjNavMesh)
							{
								TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());
								for(b=0; b<pAdjPoly->GetNumVertices(); b++)
								{
									TAdjPoly & adjPoly2 = *pAdjNavMesh->GetAdjacentPolysArray().Get(pAdjPoly->GetFirstVertexIndex() + b);

									const u32 iAdjOriginalNavMesh = adjPoly2.GetOriginalNavMeshIndex(pAdjNavMesh->GetAdjacentMeshes());
									adjPoly2.SetNavMeshIndex(iAdjOriginalNavMesh, pAdjNavMesh->GetAdjacentMeshes());
									adjPoly2.SetPolyIndex(adjPoly2.GetOriginalPolyIndex());
								}
							}
						}
					}
				}
			}
		}

		pFragment->SetNumSpecialLinks(0);
		pFragment->SetSpecialLinksStartIndex(0xFFFF);

		pFragment->SetReplacedByTessellation(false);
		pFragment->SetIsTessellatedFragment(false);
	}

	m_pTessellationNavMesh->GetAdjacentMeshes()->Reset();

	m_iCurrentNumTessellationPolys = 0;
	m_iCurrentTessellationArrayIndex = 0;
	m_iTessellationNavmeshCurrentNumVerts = 0;
	m_iTessellationNavmeshCurrentPoolSize = 0;
	m_bHasBeenThroughEntireTessellationNavMesh = false;

#if SANITY_CHECK_TESSELLATION

	for(p=0; p<CNavMesh::ms_iNumPolysInTesselationNavMesh; p++)
	{
		TNavMeshPoly & poly = *fwPathServer::m_pTessellationNavMesh->GetPoly(p);
		poly.SetFlags(0);
		poly.SetNavMeshIndex(NAVMESH_INDEX_TESSELLATION);
	}

#endif

}

//-----------------------------------------------------------------------------------
// FUNCTION : UnTessellateAllPolys
// PURPOSE : Removes all navmesh tessellation restore adjacencies to original state
// NOTES: Deprecated

bool fwPathServer::IsRoomToTessellateThisPoly(const TNavMeshPoly * pPoly)
{
	return m_iCurrentNumTessellationPolys + (pPoly->GetNumVertices()*3) <= CNavMesh::ms_iNumPolysInTesselationNavMesh;
}

bool fwPathServer::CanTessellateThisPoly(const TNavMeshPoly * pPoly)
{
	const bool bCantBeTessellated =
		pPoly->GetIsDegenerateConnectionPoly() ||
		pPoly->GetIsZeroAreaStichPolyDLC() ||
		(pPoly->GetIsSmall() && pPoly->GetIsTessellatedFragment()) ||
		(pPoly->GetIsWater() && !(pPoly->GetIsLarge())) ||	// Water polys stop tessellating earlier
		pPoly->GetIsClosed();
	return !bCantBeTessellated;
}

//-----------------------------------------------------------------------------------
// FUNCTION : GetNextFreeTessellationPolyIndex
// PURPOSE : Get index of next free polygon in tessellation navmesh
// NOTES: Deprecated

u32 fwPathServer::GetNextFreeTessellationPolyIndex()
{
#if __REUSE_TESSELLATION_POLYS

	// If we've not yet gone right through the mesh, then we can simply return the next sequential index

	if(!CPathServer::ms_bHasBeenThroughEntireTessellationNavMesh)
	{
		if(CPathServer::m_iCurrentTessellationArrayIndex < CNavMesh::ms_iNumPolysInTesselationNavMesh)
		{
			return CPathServer::m_iCurrentTessellationArrayIndex++;
		}
		CPathServer::ms_bHasBeenThroughEntireTessellationNavMesh = true;
	}

	// Otherwise things could get slow :(  Lets iterate through the mesh and return the index of the
	// first poly we find which has been 'released' from its duties as a tessellation fragment.

	for(u32 t=0; t<CNavMesh::ms_iNumPolysInTesselationNavMesh; t++)
	{
		if(!CPathServer::GetTessInfo(t)->m_bInUse)
		{
			return t;
		}
	}

	return NAVMESH_POLY_INDEX_NONE;

#else

	if(m_iCurrentNumTessellationPolys < CNavMesh::ms_iNumPolysInTesselationNavMesh)
	{
		return m_iCurrentNumTessellationPolys;
	}
	else
	{
		return NAVMESH_POLY_INDEX_NONE;
	}

#endif
}

//-----------------------------------------------------------------------------------
// FUNCTION : AllocTessellationPoly
// PURPOSE : 'Allocates' a tessellation polygon of the given number vertices

TNavMeshPoly * fwPathServer::AllocTessellationPoly(const int iNumVerts)
{
	Assert(iNumVerts < NAVMESHPOLY_MAX_NUM_VERTICES);

	Assert(m_iTessellationNavmeshCurrentNumVerts + iNumVerts < m_pTessellationNavMesh->GetNumVertices());
	Assert(m_iTessellationNavmeshCurrentPoolSize + iNumVerts < m_pTessellationNavMesh->GetSizeOfPools());
	Assert(m_iCurrentNumTessellationPolys < CNavMesh::ms_iNumPolysInTesselationNavMesh);

	TNavMeshPoly * pPoly = m_pTessellationNavMesh->GetPoly(m_iCurrentNumTessellationPolys);
	m_iCurrentNumTessellationPolys++;

	pPoly->SetNumVertices(iNumVerts);
	pPoly->SetFirstVertexIndex(m_iTessellationNavmeshCurrentPoolSize);

	for(int i=0; i<iNumVerts; i++)
	{
		*m_pTessellationNavMesh->GetVertexIndexArray().Get(m_iTessellationNavmeshCurrentPoolSize) = (u16)m_iTessellationNavmeshCurrentPoolSize;
		m_iTessellationNavmeshCurrentPoolSize++;
		m_iTessellationNavmeshCurrentNumVerts++;
	}

	return pPoly;
}
//-----------------------------------------------------------------------------------
// FUNCTION : GrowTessellationPoly
// PURPOSE : 'Grows' a tessellation polygon by the given num verts.
// This has to be the last poly which was allocated, as pools need to be continguous

bool fwPathServer::GrowTessellationPoly(TNavMeshPoly * pPoly, const int iGrowNumVerts)
{
	const s32 iNewNumVerts = pPoly->GetNumVertices()+iGrowNumVerts;
	Assert(iNewNumVerts < NAVMESHPOLY_MAX_NUM_VERTICES);
	if(iNewNumVerts >= NAVMESHPOLY_MAX_NUM_VERTICES)
		return false;

	Assert(m_iTessellationNavmeshCurrentNumVerts + iGrowNumVerts < m_pTessellationNavMesh->GetNumVertices());
	Assert(m_iTessellationNavmeshCurrentPoolSize + iGrowNumVerts < m_pTessellationNavMesh->GetSizeOfPools());

	if(m_iTessellationNavmeshCurrentNumVerts + iGrowNumVerts >= m_pTessellationNavMesh->GetNumVertices())
		return false;
	if(m_iTessellationNavmeshCurrentPoolSize + iGrowNumVerts >= m_pTessellationNavMesh->GetSizeOfPools())
		return false;

	pPoly->SetNumVertices( iNewNumVerts );

	m_iTessellationNavmeshCurrentNumVerts += iGrowNumVerts;
	m_iTessellationNavmeshCurrentPoolSize += iGrowNumVerts;

	return true;
}


