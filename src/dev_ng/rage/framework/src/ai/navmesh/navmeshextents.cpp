#include "navmeshextents.h"

using namespace rage;

#if HEIGHTMAP_GENERATOR_TOOL
s32 CPathServerExtents::m_iNumSectorsPerNavMesh = gv::WORLD_CELLS_PER_TILE;
s32 CPathServerExtents::m_iNumNavMeshesInX = (gv::NAVMESH_BOUNDS_MAX_X - gv::NAVMESH_BOUNDS_MIN_X)/gv::WORLD_TILE_SIZE;
s32 CPathServerExtents::m_iNumNavMeshesInY = (gv::NAVMESH_BOUNDS_MAX_Y - gv::NAVMESH_BOUNDS_MIN_Y)/gv::WORLD_TILE_SIZE;
Vector3 CPathServerExtents::m_vWorldMin((float)gv::NAVMESH_BOUNDS_MIN_X, (float)gv::NAVMESH_BOUNDS_MIN_Y, 0.0f);
Vector3 CPathServerExtents::m_vWorldMax((float)gv::NAVMESH_BOUNDS_MAX_X, (float)gv::NAVMESH_BOUNDS_MAX_Y, 0.0f);
#else
s32 CPathServerExtents::m_iNumSectorsPerNavMesh = 3;
s32 CPathServerExtents::m_iNumNavMeshesInX = 100;
s32 CPathServerExtents::m_iNumNavMeshesInY = 100;
Vector3 CPathServerExtents::m_vWorldMin(-6000.0f, -6000.0f, 0.0f);
Vector3 CPathServerExtents::m_vWorldMax(9000.0f, 9000.0f, 0.0f);
#endif
s32 CPathServerExtents::m_iHierarchicalNodesBlockSize = 4;
