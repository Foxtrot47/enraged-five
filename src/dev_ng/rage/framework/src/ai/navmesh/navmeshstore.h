//
// ai/navmesh/navmeshstore.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_NAVMESHSTORE_H
#define AI_NAVMESH_NAVMESHSTORE_H

#include "ai/navmesh/datatypes.h"	// For aiNavDomain
#include "ai/navmesh/navmesh.h"
#include "ai/navmesh/navmeshextents.h"
#include "entity/archetypeManager.h"
#include "grcore/debugdraw.h"
#include "fwtl/assetstore.h"

#if __BANK
#include "fwsys/timer.h"
#include "grcore/device.h"
#endif

namespace rage
{

class CModelInfoNavMeshRef;

//-----------------------------------------------------------------------------

class aiMeshLoadRegion
{
public:
	Vector2 m_vOrigin;
	float m_fRadius;
};

//-----------------------------------------------------------------------------

typedef void (*PrepareToUnloadCB)(void * pMeshObject);

//-----------------------------------------------------------------------------

template <typename T> class aiMeshAssetDef : public fwAssetDef<T>
{
public:
	aiMeshAssetDef<T>() : fwAssetDef<T>()
	{
		fwAssetDef<T>::m_pObject = NULL;
		fwAssetDef<T>::m_refCount = 0;
		m_bRequired = false;
	}

	// Initialize this definition. This function is not called Init() to avoid confusion with Init() in the base class.
	void InitDef()
	{
		m_bRequired = false;
		fwAssetDef<T>::m_name.Clear();
		fwAssetDef<T>::m_pObject = NULL;
#if DETAILED_MODULE_REF_COUNT
		fwAssetDef<T>::SetSubRefCount(REF_RENDER, 0);
		fwAssetDef<T>::SetSubRefCount(REF_SCRIPT, 0);
		//def->SetSubRefCount(REF_DEFRAG, 0);
		fwAssetDef<T>::SetSubRefCount(REF_OTHER, 1);
#else
		fwAssetDef<T>::m_refCount = 1;
#endif // DETAILED_MODULE_REF_COUNT
	}

	bool m_bRequired;
};

//-----------------------------------------------------------------------------

template <typename T, typename S> class aiMeshStore : public fwAssetRscStore<T, S>
{
public:

	aiMeshStore(const char* pModuleName, int moduleTypeIndex, int size, u32 elementsPerPage, bool UNUSED_PARAM(requiresTempMemory), bool canDefragment = false, int rscVersion = 0)
		: fwAssetRscStore<T, S>(pModuleName, moduleTypeIndex, size, elementsPerPage, canDefragment, rscVersion),
		m_LoadedMeshes(0, 128), m_CombinedMeshes(0, 128)
	{
		m_iMaxMeshIndex = 0;
		m_pIndexMappingArray = NULL;

		m_iMeshIndexNone = 0xFFFF;
		m_vMin = Vector2(0.0f, 0.0f);
		m_fMeshSize = 0.0f;
		m_iNumMeshesInX = 0;
		m_iNumMeshesInY = 0;
		m_iMaxMeshesInAnyLevel = 0;
		m_iNumSectorsPerMesh = 0;
	}

	virtual ~aiMeshStore()
	{
		fwAssetStore<T,S>::Shutdown();
#if !USE_PAGED_POOLS_FOR_STREAMING
		fwAssetStore<T,S>::m_pool.DeleteAll();
#endif // !USE_PAGED_POOLS_FOR_STREAMING
		if(m_pIndexMappingArray)
			delete[] m_pIndexMappingArray;
	}

	virtual void Init()
	{
#if !USE_PAGED_POOLS_FOR_STREAMING
		for(int s=0; s<fwAssetStore<T,S>::m_pool.GetSize(); s++)
		{
			fwAssetStore<T,S>::m_pool.New()->InitDef();
		}
#endif // !USE_PAGED_POOLS_FOR_STREAMING

		m_LoadedMeshes.clear();
		m_CombinedMeshes.clear();
	}
	virtual void Shutdown()
	{
#if !USE_PAGED_POOLS_FOR_STREAMING
		for(int s=0; s<fwAssetStore<T,S>::m_pool.GetSize(); s++)
		{
			S * def = fwAssetStore<T,S>::m_pool.GetSlot(s);
			if(def)
				fwAssetStore<T,S>::m_pool.Delete(def);
		}
#endif // !USE_PAGED_POOLS_FOR_STREAMING
	}

	virtual void RequestAndEvict(atArray<aiMeshLoadRegion> & loadRegions, PrepareToUnloadCB fnPrepareToUnloadMesh);

	virtual void* GetPtr(strLocalIndex UNUSED_PARAM(index)) { Assertf(false, "aiMeshStore::GetPtr() must be implemented in the derived class."); return NULL; }
	virtual strLocalIndex Register(const char* UNUSED_PARAM(name)) { Assertf(false, "aiMeshStore::Register() must be implemented in the derived class."); return strLocalIndex(-1); }
	virtual void Remove(strLocalIndex UNUSED_PARAM(index)) { Assertf(false, "aiMeshStore::Remove() must be implemented in the derived class."); }

#if !__FINAL
	virtual const char* GetName(strLocalIndex UNUSED_PARAM(index)) const { return strStreamingModule::m_ModuleName; }
#endif

#if __BANK
	void Visualise(const Vector3 * playerPos = NULL);
#endif

	virtual bool Load(strLocalIndex UNUSED_PARAM(index), void* UNUSED_PARAM(pData), int UNUSED_PARAM(size))
	{
		Quitf(ERR_GEN_NAV,"Non-resourced navigation data loading is not supported or expected.");
		return false;
	}

	void AllocateMapping(const s32 iMaxIndex)
	{
		if(m_pIndexMappingArray && iMaxIndex != m_iMaxMeshIndex)
		{
			delete[] m_pIndexMappingArray;
			m_pIndexMappingArray = NULL;
		}

		m_iMaxMeshIndex = iMaxIndex;

		m_pIndexMappingArray = rage_new s16[m_iMaxMeshIndex+1];
		for(int i=0; i<=m_iMaxMeshIndex; i++)
			m_pIndexMappingArray[i] = -1;
	}
	
	void SetMapping(const u32 iMeshIndex, const s16 iStreamingIndex)
	{
		m_pIndexMappingArray[iMeshIndex] = iStreamingIndex;
	}
	strLocalIndex GetStreamingIndex(const strLocalIndex iMeshIndex) const
	{
		if(!m_pIndexMappingArray)
			return iMeshIndex;

		Assert(iMeshIndex.Get() <= m_iMaxMeshIndex && m_pIndexMappingArray);
		return strLocalIndex(m_pIndexMappingArray[iMeshIndex.Get()]);
	}
	bool GetIsMeshInImage(const u32 iMeshIndex)
	{
		Assert(iMeshIndex <= m_iMaxMeshIndex);
		const strLocalIndex iStreamingIndex = GetStreamingIndex(strLocalIndex(iMeshIndex));
		S * def = fwAssetStore<T, S>::GetSlot(iStreamingIndex);
		return (def && def->m_name.IsNotNull());
	}
	bool GetDoesMeshExist(const u32 iMeshIndex)
	{
		Assert(iMeshIndex <= m_iMaxMeshIndex);
		const strLocalIndex iStreamingIndex = GetStreamingIndex(strLocalIndex(iMeshIndex));
		return(iStreamingIndex.Get() >= 0 && GetPtr(iStreamingIndex)!=NULL);
	}
	bool GetIsMeshRequired(const u32 iMeshIndex)
	{
		Assert((int)iMeshIndex <= m_iMaxMeshIndex);
		const strLocalIndex iStreamingIndex = GetStreamingIndex(strLocalIndex(iMeshIndex));
		return(iStreamingIndex.Get() >= 0 && fwAssetStore<T, S>::GetSlot(iStreamingIndex) && fwAssetStore<T, S>::GetSlot(iStreamingIndex)->m_bRequired);
	}
	void SetIsMeshRequired(const u32 iMeshIndex, const bool bRequired)
	{
		Assert((int)iMeshIndex <= m_iMaxMeshIndex);
		const strLocalIndex iStreamingIndex = GetStreamingIndex(strLocalIndex(iMeshIndex));
		if(iStreamingIndex.Get() >= 0)
		{
			S * def = fwAssetStore<T, S>::GetSlot(iStreamingIndex);
			if(def)
				def->m_bRequired = bRequired;
		}
	}
	T * GetMeshByIndex(const u32 iMeshIndex)
	{
		const strLocalIndex iStreamingIndex = GetStreamingIndex(strLocalIndex(iMeshIndex));
		if(iStreamingIndex.Get() >= 0)
		{
			S * def = fwAssetStore<T, S>::GetSlot(iStreamingIndex);
			if(def)
			{
				return def->m_pObject;
			}
		}
		return NULL;
	}
	T * GetMeshByStreamingIndex(const u32 iStreamingIndex)
	{
		return (T*) GetPtr(strLocalIndex(iStreamingIndex));
	}

	u32 MeshCoordsToMeshIndex(const int x, const int y)
	{
		const int iMeshIndex = (y * GetNumMeshesInX()) + x;
		
		Assert(iMeshIndex <= m_iMaxMeshIndex);
		
		if(iMeshIndex > m_iMaxMeshIndex)
			return m_iMeshIndexNone;
		return iMeshIndex;
	}
	void MeshIndexToMeshCoords(const s32 iMeshIndex, int & iX, int & iY)
	{
		Assert(iMeshIndex != m_iMeshIndexNone);

		iY = iMeshIndex / GetNumMeshesInX();
		iX = iMeshIndex - (GetNumMeshesInX() * iY);
	}
	void GetMeshExtentsFromIndex(const u32 iMeshIndex, Vector2 & vMin, Vector2 & vMax)
	{
		const int iY = iMeshIndex / GetNumMeshesInY();
		const int iX = iMeshIndex - (iY * GetNumMeshesInY());
		const float fMeshSize = GetMeshSize();

		vMin.x = ((((float)iX) * fMeshSize) + CPathServerExtents::m_vWorldMin.x);
		vMin.y = ((((float)iY) * fMeshSize) + CPathServerExtents::m_vWorldMin.y);

		vMax.x = vMin.x + fMeshSize;
		vMax.y = vMin.y + fMeshSize;
	}
	u32 GetMeshIndexFromPosition(const Vector3 & vPos)
	{
		const int iSectorX = CPathServerExtents::GetWorldToSectorX(vPos.x);
		const int iSectorY = CPathServerExtents::GetWorldToSectorY(vPos.y);

		return GetMeshIndexFromSectorCoords(iSectorX, iSectorY);
	}
	u32 GetMeshIndexFromSectorCoords(const s32 iSectorX, const s32 iSectorY)	// TODO: phase out sector usage when we get time
	{
		const int iNavMeshBlockX = (int) (iSectorX / (float) m_iNumSectorsPerMesh);
		const int iNavMeshBlockY = (int) (iSectorY / (float) m_iNumSectorsPerMesh);

		if(iNavMeshBlockX < 0 || iNavMeshBlockX >= m_iNumMeshesInX || iNavMeshBlockY < 0 || iNavMeshBlockY >= m_iNumMeshesInY)
			return m_iMeshIndexNone;

		const u32 iIndex = (iNavMeshBlockY * m_iNumMeshesInX) + iNavMeshBlockX;
		return iIndex;
	}
	void GetSectorCoordsFromMeshIndex(const s32 iMeshIndex, s32 & iSectorX, s32 & iSectorY)
	{
		MeshIndexToMeshCoords(iMeshIndex, iSectorX, iSectorY);
		iSectorX *= m_iNumSectorsPerMesh;
		iSectorY *= m_iNumSectorsPerMesh;
	}
	inline s32 GetMaxMeshIndex() const { return m_iMaxMeshIndex; }
	inline s32 GetMeshIndexNone() const { return m_iMeshIndexNone; }
	inline s32 GetNumMeshesInX() const { return m_iNumMeshesInX; }
	inline s32 GetNumMeshesInY() const { return m_iNumMeshesInY; }
	inline s32 GetNumMeshesInAnyLevel() const { return m_iMaxMeshesInAnyLevel; }
	inline float GetMeshSize() const { return m_fMeshSize; }
	inline const Vector2 & GetMin() const { return m_vMin; }

	inline void SetMaxMeshIndex(const s32 i) { m_iMaxMeshIndex = i; }
	inline void SetMeshIndexNone(const s32 i) { m_iMeshIndexNone = i; }
	inline void SetNumMeshesInX(const s32 x) { m_iNumMeshesInX = x; }
	inline void SetNumMeshesInY(const s32 y) { m_iNumMeshesInY = y; }
	inline void SetNumMeshesInAnyLevel(const int n) { m_iMaxMeshesInAnyLevel = n; }
	inline void SetMeshSize(const float f) { m_fMeshSize = f; }
	inline void SetMin(const Vector2 & v) { m_vMin = v; }

	inline s32 GetNumSectorsPerMesh() const { return m_iNumSectorsPerMesh; }
	inline void SetNumSectorsPerMesh(const s32 i) { m_iNumSectorsPerMesh = i; }

	// Accessing loaded meshes from any thread other than the main thread, is not threadsafe - use LOCK_STORE_LOADED_MESHES to protect
	inline const atArray<s32> & GetLoadedMeshes() { return m_LoadedMeshes; }

protected:

	s32 m_iMaxMeshIndex;				// The maximum mesh index addressable
	s32 m_iMeshIndexNone;				// A special index to be used to indicate 'not found'
	s16 * m_pIndexMappingArray;			// A table used to map the mesh index to the streaming index
	Vector2 m_vMin;						// The minimum XY coordinate of any mesh.
	float m_fMeshSize;					// The XY size of each mesh
	s32 m_iNumMeshesInX;				// How many meshes across the whole map in XY (starting from m_vMin)
	s32 m_iNumMeshesInY;
	s32 m_iMaxMeshesInAnyLevel;			// The maximum across all levels (because we can't reinitialise streaming modules)

	s32 m_iNumSectorsPerMesh;			// Legacy - to be removed

	atArray<s32> m_LoadedMeshes;		// Array of which meshes are loaded
	atArray<s32> m_CombinedMeshes;		// Temporary array, used when requesting/evicting meshes
};

template <typename T, typename S> void aiMeshStore<T,S>::RequestAndEvict(atArray<aiMeshLoadRegion> & loadRegions, PrepareToUnloadCB fnPrepareToUnloadMesh)
{
	Assertf(loadRegions.GetCount() > 0, "aiMeshStore::RequestAndEvict - there has to be at least one loadRegion!");
	if(loadRegions.GetCount() <= 0)
		return;

	const float fMeshHalfSize = GetMeshSize() * 0.5f;
	const float fMeshRadius = rage::Sqrtf((fMeshHalfSize*fMeshHalfSize)+(fMeshHalfSize*fMeshHalfSize));

	Vector2 vCenter, vDiff;
	Vector2 vMin, vMax;
	s32 i, r;
	float x, y;

	//********************************************************************
	// Firstly mark any loaded mesh outside all regions, as not required

	m_CombinedMeshes.Resize(0);

	for(i=0; i<m_LoadedMeshes.GetCount(); i++)
	{
		const u32 iLoadedMeshIndex = m_LoadedMeshes[i];

		Assert((int)iLoadedMeshIndex < m_iMaxMeshIndex);
		Assert(iLoadedMeshIndex != NAVMESH_INDEX_TESSELLATION); // magic check, pathserver will go ape-shit
	
		GetMeshExtentsFromIndex(iLoadedMeshIndex, vMin, vMax);
		vCenter = (vMin + vMax) * 0.5f;

		for(r=0; r<loadRegions.GetCount(); r++)
		{
			vDiff = Vector2(vCenter.x - loadRegions[r].m_vOrigin.x, vCenter.y - loadRegions[r].m_vOrigin.y);
			
			// Add 10m onto the load proximity, to provide some 'historysis'
			const float fUnloadProximity = loadRegions[r].m_fRadius + fMeshRadius + 10.0f;

			if(vDiff.Mag2() < fUnloadProximity*fUnloadProximity)
			{
				break;
			}
		}
		// If was not within any region, then mark as not required
		if(r == loadRegions.GetCount())
		{
			SetIsMeshRequired(iLoadedMeshIndex, false);
		}

		if(m_CombinedMeshes.GetCount() < m_CombinedMeshes.GetCapacity())
		{
			m_CombinedMeshes.Append() = iLoadedMeshIndex;
		}
		else
		{
			m_CombinedMeshes.Grow() = iLoadedMeshIndex;

			Displayf("aiMeshStore::RequestAndEvict() - grew m_CombinedMeshes array to %i", m_CombinedMeshes.GetCount());
		}
	}

	//**********************************************************************************
	// Now go through all the regions for which we want meshes, and set as required

	for(r=0; r<loadRegions.GetCount(); r++)
	{
		const float fLoadProximity = loadRegions[r].m_fRadius + fMeshRadius;
		const Vector3 vRegionMin(loadRegions[r].m_vOrigin.x - fLoadProximity, loadRegions[r].m_vOrigin.y - fLoadProximity, 0.0f);
		const Vector3 vRegionMax(loadRegions[r].m_vOrigin.x + fLoadProximity, loadRegions[r].m_vOrigin.y + fLoadProximity, 0.0f);

		for(y=vRegionMin.y; y<vRegionMax.y; y+=m_fMeshSize)
		{
			for(x=vRegionMin.x; x<vRegionMax.x; x+=m_fMeshSize)
			{
				const s32 iMeshIndex = GetMeshIndexFromPosition( Vector3(x,y,0.0f) );
				if(iMeshIndex != m_iMeshIndexNone)
				{
					Assert(iMeshIndex < m_iMaxMeshIndex);

					const int iY = iMeshIndex / m_iNumMeshesInX;
					const int iX = iMeshIndex - (iY * m_iNumMeshesInY);

					vCenter.x = ((((float)iX) * m_fMeshSize) + CPathServerExtents::m_vWorldMin.x) + fMeshHalfSize;
					vCenter.y = ((((float)iY) * m_fMeshSize) + CPathServerExtents::m_vWorldMin.y) + fMeshHalfSize;

					vDiff.x = vCenter.x - loadRegions[r].m_vOrigin.x;
					vDiff.y = vCenter.y - loadRegions[r].m_vOrigin.y;

					if(vDiff.Mag2() < fLoadProximity*fLoadProximity)
					{
						for(i=0; i<m_CombinedMeshes.GetCount(); i++)
						{
							if(m_CombinedMeshes[i]==iMeshIndex)
							{
								SetIsMeshRequired(iMeshIndex, true);
								break;
							}
						}
						if(i==m_CombinedMeshes.GetCount())
						{
							if(m_CombinedMeshes.GetCount() < m_CombinedMeshes.GetCapacity())
							{
								m_CombinedMeshes.Append() = iMeshIndex;
							}
							else
							{
								m_CombinedMeshes.Grow() = iMeshIndex;

								Displayf("aiMeshStore::RequestAndEvict() - grew m_CombinedMeshes array to %i", m_CombinedMeshes.GetCount());
							}
							SetIsMeshRequired(iMeshIndex, true);
						}
					}
				}
			}
		}
	}

	//*******************************************************************
	// Go through the combined list, and load/unload meshes accordingly

	for(i=0; i<m_CombinedMeshes.GetCount(); i++)
	{
		const strLocalIndex iMeshIndex = strLocalIndex(m_CombinedMeshes[i]);
		const strLocalIndex iStreamingIndex = strLocalIndex(GetStreamingIndex(iMeshIndex));

		if( iStreamingIndex.Get() >= 0 && strStreamingModule::IsObjectInImage(iStreamingIndex) )
		{
			if(GetIsMeshRequired(iMeshIndex.Get()))
			{
				//*********
				// Request

				if(!GetMeshByStreamingIndex(iStreamingIndex.Get()))
				{
					// If the mesh is not already requested or loaded, then request it
					if(!strStreamingModule::IsObjectRequested(iStreamingIndex) &&
						!strStreamingModule::HasObjectLoaded(iStreamingIndex) &&
						!strStreamingModule::IsObjectLoading(iStreamingIndex))
					{
						strStreamingModule::StreamingRequest(iStreamingIndex, STRFLAG_DONTDELETE);
					}
				}
			}

			//********************************
			// Handle unloading of navmeshes

			else
			{
				if(GetMeshByStreamingIndex(iStreamingIndex.Get()))
				{
					if(fwAssetRscStore<T, S>::GetNumRefs(iStreamingIndex) > 1)
					{
						// If num refs > 1 this indicates that defrag is in progress
						// We cannot remove object at this time
					}
					else
					{
						if(fnPrepareToUnloadMesh)
							fnPrepareToUnloadMesh(GetMeshByIndex(iMeshIndex.Get()));

						// NB: Could this instead reset the required & force-load flags?
						// Might let unload to occur more gracefully (otherwise this will require
						// a lock on navmesh data to happen right now)
						strStreamingModule::ClearRequiredFlag(iStreamingIndex.Get(), STRFLAG_DONTDELETE);
						strStreamingModule::StreamingRemove(iStreamingIndex);
					}
				}
			}
		}
	}

	//************************************************************
	// Ensure that everything is set as not-required afterwards

	for(i=0; i<m_CombinedMeshes.GetCount(); i++)
	{
		const u32 iMeshIndex = m_CombinedMeshes[i];
		SetIsMeshRequired(iMeshIndex, false);
	}
}


#if __BANK
template <typename T, typename S> void aiMeshStore<T,S>::Visualise(const Vector3 * playerPos)
{
	char line[256];
	int x,y;

	const float fMaxNumLines = ((float)grcDevice::GetHeight()) / ((float)grcDebugDraw::GetScreenSpaceTextHeight());
	const float fDesiredNumLines = ((float)m_iNumMeshesInY);
	const float fScale = Min(fMaxNumLines / fDesiredNumLines, 1.0f);

	const Vector2 vScale(fScale, fScale);
	const float fScreenWidth = (float) grcDevice::GetWidth();
	const float fScreenHeight = (float) grcDevice::GetHeight();
	const float fLeft = 16.0f;
	float fTop = 16.0f;
	int iCount = 0;

	static bool bFlash = false;
	static u32 iLastFlashTime = 0;
	u32 currentTime = fwTimer::GetTimeInMilliseconds();
	const int iNumSectorsInX = m_iNumMeshesInX * m_iNumSectorsPerMesh;
	const int iNumSectorsInY = m_iNumMeshesInY * m_iNumSectorsPerMesh;

	if((currentTime - iLastFlashTime) > 500)
	{
		iLastFlashTime = currentTime;
		bFlash = bFlash ? false : true;
	}

	int playerMeshIndex = -1;

	if(playerPos)
	{
		playerMeshIndex = GetMeshIndexFromPosition(*playerPos);
	}

	for(y=0; y<iNumSectorsInY; y+=m_iNumSectorsPerMesh)
	{
		iCount = 0;

		for(x=0; x<iNumSectorsInX; x+=m_iNumSectorsPerMesh)
		{
			const strLocalIndex iMeshIndex = strLocalIndex(GetMeshIndexFromSectorCoords(x, y));
			
			if(iMeshIndex != m_iMeshIndexNone)
			{
				const int iStreamingIndex = GetStreamingIndex(iMeshIndex).Get();

				if(bFlash)
				{
					if(iStreamingIndex==-1)
					{
						line[iCount] = ' ';
					}
					else if(playerMeshIndex == iMeshIndex.Get())
					{
						line[iCount] = 'P';
					}
					else if(m_LoadedMeshes.Find(iMeshIndex.Get())!=-1)
					{
						line[iCount] = 'L';
					}
					else if(!GetIsMeshInImage(iMeshIndex.Get()))
					{
						line[iCount] = 'X';
					}
					else
					{
						line[iCount] = '.';
					}
				}
				else
				{
					if(iStreamingIndex==-1)
					{
						line[iCount] = ' ';
					}
					else if(!GetIsMeshInImage(iMeshIndex.Get()))
					{
						line[iCount] = 'X';
					}
					else if(GetMeshByIndex(iMeshIndex.Get()))
					{
						line[iCount] = 'O';
					}
					else if(GetIsMeshRequired(iMeshIndex.Get()))
					{
						line[iCount] = 'o';
					}
					else
					{
						line[iCount] = '.';
					}
				}
				iCount++;
			}
		}

		line[iCount] = 0;

		grcDebugDraw::Text(
			Vector2((fLeft/fScreenWidth)*vScale.x, (fTop/fScreenHeight)*vScale.y),
			Color32(1.00f, 0.97f, 0.86f),
			line,
			false,
			vScale.x, vScale.y
			);

		fTop += ((float)grcDebugDraw::GetScreenSpaceTextHeight()) * vScale.y;
	}
}
#endif

//-----------------------------------------------------------------------------

/*
PURPOSE
	Used by aiNavMeshStore and aiNavNodesStore to interface with code that's
	game-specific or not yet moved to the framework.
NOTES:
	As we continue to move code over to the framework (in particular CPathServer, in this case),
	some or all functions here may become possible to remove.
*/
class fwNavMeshStoreInterface
{
public:
	virtual ~fwNavMeshStoreInterface() {}

	virtual void ApplyAreaSwitchesToNewlyLoadedNavMesh(CNavMesh &mesh, aiNavDomain domain) const = 0;
#if __HIERARCHICAL_NODES_ENABLED
	virtual void ApplyAreaSwitchesToNewlyLoadedNavNodes(CHierarchicalNavData &navNodes) const = 0;
#endif
	virtual void FindVehicleForDynamicNavMesh(atHashString iNameHash, CModelInfoNavMeshRef &dynNavInf) const = 0;
	virtual void NavMeshesHaveChanged() const = 0;
	virtual bool IsDefragmentCopyBlocked() const = 0;

	virtual void AdjoinNavmeshesAcrossMapSwap(CNavMesh * pMainMapMesh, CNavMesh * pSwapMesh, eNavMeshEdge iSideOfMainMapMesh) const = 0;
	virtual void RestoreNavmeshesAcrossMapSwap(CNavMesh * pMainMapMesh, CNavMesh * pSwappedBackMesh, eNavMeshEdge iSideOfMainMapMesh) const = 0;

	virtual void ApplyHacksToNewlyLoadedNavMesh(CNavMesh * pNavMesh, aiNavDomain domain) const = 0;
};

//-----------------------------------------------------------------------------

typedef aiMeshAssetDef<CNavMesh> aiNavMeshAssetDef;

class aiNavMeshStore : public aiMeshStore<CNavMesh, aiNavMeshAssetDef>
{
public:
	aiNavMeshStore(aiNavDomain domain, const fwNavMeshStoreInterface &storeInterface,
		const char* pModuleName, 
		int moduleTypeIndex,
		int size,
		u32 elementsPerPage,
		bool requiresTempMemory,
		bool canDefragment = false,
		int rscVersion = 0);
	virtual ~aiNavMeshStore();

	void Init();
	virtual void Shutdown();

	virtual void* GetPtr(strLocalIndex index);
	virtual strLocalIndex Register(const char* name);
	virtual strLocalIndex FindSlot(const char* name) const;
	virtual void Remove(strLocalIndex index);
	virtual void PlaceResource(strLocalIndex index, datResourceMap & map, datResourceInfo & header);

	virtual bool IsDefragmentCopyBlocked() const;

	void SetDefragmentCopyBlocked(const bool b);
	void ResetBuildID() { m_iBuildID = 0; }

#if !__FINAL
	virtual const char* GetName(strLocalIndex index) const;
#endif

	// No async placement yet - we need to separate out the Set() call and possibly some other things that can
	// only happen in the main thread.
	virtual bool CanPlaceAsynchronously( strLocalIndex UNUSED_PARAM(objIndex) ) const { return false; }
	
	// We currently still call Set() during the PlaceResource() call, so we can't do it here again.
	// Calling Set() twice will assert.
	virtual void SetResource(strLocalIndex /*index*/, datResourceMap& /*map*/) {}

	void InitDynamicNavMeshesAfterVehSetup();
	void FindVehicleForDynamicNavMesh(CModelInfoNavMeshRef &dynNavInf) const;

	aiNavDomain GetDataSet() const
	{	return m_DataSet;	}

	void CreateNameHashTable();
	atHashString GetNavMeshIndexFromNameHash(const atHashString iNameHash) const;

public:

	struct TNameHashMapping
	{
		u32 m_iNameHash;
		u32 m_iNavMeshIndex;
	};

protected:
	// PURPOSE:	Pointer to interface object to use for calling higher level code.
	// NOTES:	Not owned!
	const fwNavMeshStoreInterface*	m_Interface;

	aiNavDomain	m_DataSet;

	u32 m_iBuildID;

	bool m_bDefragmentCopyBlocked;


	// PURPOSE: array of mappings between navmesh filename and the navmesh index to assign it
	// This array is always sorted by ascending 'm_iNameHash' enabling a fast binary-search lookup on hashkey
	atFixedArray<TNameHashMapping, NAVMESH_MAX_MAP_INDEX+1> m_NameHashTable;
};

//******************************************************************************
//	TModelInfoNavMeshRef
//	A mapping between a model-index & a dynamic navmesh.

class CModelInfoNavMeshRef
{
public:
	CModelInfoNavMeshRef() { m_iModelIndex=fwModelId::MI_INVALID; m_iNumRefs=0; m_pBackUpNavmeshCopy=NULL; m_iStreamingIndex=-1; m_iNavFileNameHash.Clear(); }
	~CModelInfoNavMeshRef() { Shutdown(); }

	inline void Shutdown() {
		if(m_pBackUpNavmeshCopy) { delete m_pBackUpNavmeshCopy; m_pBackUpNavmeshCopy = NULL; }
		m_iNumRefs = 0;
		m_iModelIndex = fwModelId::MI_INVALID;
		m_iStreamingIndex = -1;
	}

	// The model info which has this dynamic navmesh
	strLocalIndex m_iModelIndex;
	// The number of entities in existence which use this type of navmesh
	int m_iNumRefs;
	// A single backup copy of the navmesh, from which instances are copied
	CNavMesh * m_pBackUpNavmeshCopy;
	// The index that this navmesh is assigned within the streaming system
	strLocalIndex m_iStreamingIndex;
	// The hash of the nav filename (minus extenstion) within the navmeshes.img file
	atHashString m_iNavFileNameHash;
};

//-----------------------------------------------------------------------------

class CDynamicNavMeshStore
{
public:
	CDynamicNavMeshStore();
	~CDynamicNavMeshStore();
	void Init(const int iMaxDynamicNavmeshTypes);
	void Shutdown();
	inline int GetNum() { return m_iNumNavMeshes; }
	inline int GetMaxNum() { return m_iMaxDynamicNavmeshTypes; }
	CModelInfoNavMeshRef * Add();

	inline CModelInfoNavMeshRef & Get(int i) { Assert(i < m_iNumNavMeshes); return m_NavMeshes[i]; }
	inline const CModelInfoNavMeshRef & Get(int i) const { Assert(i < m_iNumNavMeshes); return m_NavMeshes[i]; }

	const CModelInfoNavMeshRef * GetByModelIndex(u32 mi) const;

private:
	
	CModelInfoNavMeshRef * m_NavMeshes;
	int m_iMaxDynamicNavmeshTypes;
	int m_iNumNavMeshes;
};

//-----------------------------------------------------------------------------

#if __HIERARCHICAL_NODES_ENABLED

typedef aiMeshAssetDef<CHierarchicalNavData> aiNavNodesAssetDef;

class aiNavNodesStore : public aiMeshStore<CHierarchicalNavData, aiNavNodesAssetDef>
{
public:
	aiNavNodesStore(const fwNavMeshStoreInterface& storeInterface,
		const char* pModuleName, 
		int moduleTypeIndex,
		int size,
		bool requiresTempMemory,
		bool canDefragment = false,
		int rscVersion = 0);
	virtual ~aiNavNodesStore();

	void Init();
	virtual void Shutdown();

	virtual void* GetPtr(int index);
	virtual strLocalIndex Register(const char* name);
	virtual void Remove(int index);
	virtual void PlaceResource(s32 iStreamingIndex, datResourceMap & map, datResourceInfo & header);
#if !__FINAL
	virtual const char* GetName(int index) const;
#endif

protected:
	// PURPOSE:	Pointer to interface object to use for calling higher level code.
	// NOTES:	Not owned!
	const fwNavMeshStoreInterface*	m_Interface;
};

#endif	// __HIERARCHICAL_NODES_ENABLED

//-----------------------------------------------------------------------------

}	// namespace rage

#endif	// AI_NAVMESH_NAVMESHSTORE_H

// End of file ai/navmesh/navmeshstore.h
