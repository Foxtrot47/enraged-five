//
// ai/navmeshoptimisations.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//`

#ifndef AI_NAVMESHOPTIMISATIONS_H
#define AI_NAVMESHOPTIMISATIONS_H

#define NAVMESH_OPTIMISATIONS_OFF	0

#if NAVMESH_OPTIMISATIONS_OFF
#define NAVMESH_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define NAVMESH_OPTIMISATIONS()
#endif // NAVMESH_OPTIMISATIONS_OFF

#endif // AI_NAVMESHOPTIMISATIONS_H
