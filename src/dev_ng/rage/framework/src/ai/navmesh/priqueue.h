//
// ai/navmesh/priqueue.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_PRIQUEUE_H
#define AI_NAVMESH_PRIQUEUE_H

#include "vector/vector3.h"

namespace rage
{

class CHierarchicalNavNode;
struct TNavMeshPoly;

//*********************************************************
//	This implementation of CPathSearchPriorityQueue uses
//	a binary heap to speed up access.
//*********************************************************

// Define these for sanity-checking, if anything suspect is happening with the binheap
#define PSVR__QUALITY_ASSURANCE		0
#define PSVR__CHECKSUM				0

//**********************************************************************
//	Flags which are stored with each bin-heap node


//************************************************************************
//	Indicates that this node's pointer is in fact a CHierarchicalNavNode
#define PATHSERVERBINHEAP_ENTRY_IS_HIERARCHICAL_NODE	0x01

//-----------------------------------------------------------------------------

struct TBinHeapNodeVars
{
	u32 iFlags;
	float fCost;
	float fDistanceTravelled;
	CHierarchicalNavNode * pNode;
	Vector3 vPosition;
	Vector3 vDirFromPrev;
};

//-----------------------------------------------------------------------------

class CPathServerBinHeap
{
	friend class CPathSearchPriorityQueue;

public:

	struct THeapItem
	{
		union
		{
			TNavMeshPoly * pPoly;
			CHierarchicalNavNode * pNode;
			void * pItem;
		};
	};
	
	class Node
	{
	public:
		u16 iFlags;
		u16 iReachedByConnectionPoly;
		float vPointInPoly[3];
		float vDirFromPrevious[3];
		float Key;
		float fDistanceTravelled;
		THeapItem item;
		// Our pointer to the client's pointer to us, so the client can find this node as it moves around.
		Node**	Handle;
	};

	// PURPOSE: Default constructor
	explicit CPathServerBinHeap(int maxSize) : MaxSize(maxSize), Size(0)
	{
		Nodes = rage_new Node[maxSize+1]; 
#if PSVR__QUALITY_ASSURANCE && PSVR__CHECKSUM
		CheckSum=0.0f;
#endif
	}

	// copy constructor
	CPathServerBinHeap(const CPathServerBinHeap &original)
	{
		MaxSize = original.MaxSize;
		Size = original.Size;
		Nodes = rage_new Node[original.MaxSize+1];
#if PSVR__QUALITY_ASSURANCE && PSVR__CHECKSUM
		CheckSum=0.0f;
#endif

		for (unsigned int n=0;n<Size;n++)
		{
			Assertf(original.Nodes[n].Handle == &original.NodeDump,"Node **Handle is evil so don't use it");
			Nodes[n]=original.Nodes[n];
		}
	}

	// PURPOSE: Destructor
	~CPathServerBinHeap() 
	{
		Kill();
	}

	// PURPOSE: Free all storage associated with the atBinHeap object
	inline void Kill() 
	{
		delete[] Nodes;
		Nodes = NULL;
	}

	// PURPOSE: set the heap to be empty
	inline void Empty() 
	{
		Size=0;

#if PSVR__QUALITY_ASSURANCE && PSVR__CHECKSUM
		CheckSum=0.0f;
#endif
	}
	
	// PURPOSE:	Insert a new node (with key and data) into the heap. 
	// PARAMS:	key - Value of the key to insert
	//			data - User-defined data associated with the key
	//			node - VERY IMPORTANT!!!  You have two options:  
	//
	//			1)  IFF you do not need to keep a pointer to the Node you just
	//				inserted, simply pass in NULL for the "node" paramaeter.
	//
	//				[Note that NULL is the default value for that paramaeter.]
	//
	//			2)  IFF you want to keep a pointer to the Node you just inserted,
	//				pass in the address of a Node*.  This Node* MUST be allocated 
	//				by the caller, and MUST stay in scope for as long as this heap
	//				exists.  Here's why:
	//		
	//				The newly-inserted node is going to move around in the heap.
	//				If the client wants to keep a pointer to the Node, that
	//				pointer will have to be updated whenever the Node is 
	//				repositioned in the heap.
	//
	//				Therefore, the heap keeps a Node** Handle, which points back
	//				at the client's Node*.  Every time the Node moves, the heap
	//				uses the Node's Handle to inform the client of where it moved to.
	//				
	//				So, IF you pass in the /address/ of a client-allocated Node*
	//				that will ALWAYS remain in scope, the heap will ensure
	//				that you can always use that Node* to find the inserted Node.
	//
	//				IF THE PASSED-IN Node* GOES OUT OF SCOPE, THE HEAP
	//				WILL TRAMPLE ON ARBITRARY MEMORY.  Consider yourself warned!
	//				

	// WARNING:  read the explanation before passing in a non-NULL "node"!
	void Insert(float key, float fDistanceTravelled, void * pItem, const Vector3 & vPointInPoly, const Vector3 & vDirFromPrevious, const u32 iFlags, const u32 iReachedByConnectionPoly, Node** node=NULL);	

	// PURPOSE:	Finds the node of minimum value 
	// RETURNS:	Pointer to the node of minimum value, or NULL if empty
	inline Node* FindMin() 
	{
		if (Size<=0) //lint !e775 non-negative quantity cannot be less than zero
			return NULL;

		return &Nodes[1];
	}

	// PURPOSE:	Finds the node of minimum value 
	// RETURNS:	Pointer to the node of minimum value, or NULL if empty
	inline const Node* FindMin() const
	{
		if (Size<=0) //lint !e775 non-negative quantity cannot be less than zero
			return NULL;

		return &Nodes[1];
	}

	// PURPOSE:	Informs interested parties of how many nodes we contain 
	// RETURNS:	NodeCount
	inline unsigned int GetNodeCount() const
	{
		return Size;
	}

	// PURPOSE:	Maximum number of nodes in the queue.  This is set by the constructor.
	// RETURNS:	MaxSize
	unsigned int GetMaxSize() const
	{
		return MaxSize;
	}

	// PURPOSE:	Merges another heap into this one.
	// RETURNS:	Pointer to associated data node, or NULL if not found
	// NOTES: The other heap will be emptied
	inline void Union(CPathServerBinHeap& /*otherHeap*/) 
	{
#if PSVR__QUALITY_ASSURANCE
		Quitf("atBinHeap: Union not implemented\n");
#endif
	}

	// PURPOSE:	Removes node of minimum value from heap, and returns its information. 
	//			The node itself is deallocated.
	// RETURNS:	Pointer to associated data node, or NULL if not found

	inline bool ExtractMin(float & key, float & fDistanceTravelled, void *& pItem, Vector3 & vPointInPoly, Vector3 & vDirFromPrevious, u32 & iFlags, u32 & iReachedByConnectionPoly)
	{
		if (Size<=0) //lint !e775 non-negative quantity cannot be less than zero
			return false;

		iFlags						= (u32)Nodes[1].iFlags;
		iReachedByConnectionPoly	= (u32)Nodes[1].iReachedByConnectionPoly;
		vPointInPoly.x				= Nodes[1].vPointInPoly[0];
		vPointInPoly.y				= Nodes[1].vPointInPoly[1];
		vPointInPoly.z				= Nodes[1].vPointInPoly[2];
		vDirFromPrevious.x			= Nodes[1].vDirFromPrevious[0];
		vDirFromPrevious.y			= Nodes[1].vDirFromPrevious[1];
		vDirFromPrevious.z			= Nodes[1].vDirFromPrevious[2];
		key							= Nodes[1].Key;
		fDistanceTravelled			= Nodes[1].fDistanceTravelled;
		pItem						= Nodes[1].item.pItem;

		Swap(1, Size);
		Size--;
		HeapifyDown(1);
#if PSVR__QUALITY_ASSURANCE
#if PSVR__CHECKSUM
		CheckSum-=key;
#endif
		VerifyHeap();
#endif
		return true;
	}

	// PURPOSE:  Assigns a new, smaller key value to a node; 
	// PARAMS:  node: the node whose key we're decreasing.
	//			newKey: the value to decrease it to.
	// NOTE:  MUST BE A DECREASE!  We can't use this to increase a key's value
	void DecreaseKey(Node * node, const float newKey);
			
	// PURPOSE:  Delete an arbitrary node from the heap. 
	// PARAMS:	node: the node to delete
	inline void Delete(Node * node) 
	{
		if (Size<=0) //lint !e775 non-negative quantity cannot be less than zero
			return;
		unsigned int which = ptrdiff_t_to_int(node-Nodes);

#if PSVR__QUALITY_ASSURANCE && PSVR__CHECKSUM
		CheckSum -= node->Key;
#endif
		Swap(which, Size);
		Size--;
		HeapifyUp(which);
		HeapifyDown(which);
#if PSVR__QUALITY_ASSURANCE
		VerifyHeap();
#endif
	}

	// PURPOSE: Find a node by its Data
	// PARAMS:  reference to the _Data you're looking for.
	inline Node *FindNode(void * pItem)
	{
		for (unsigned i=1;i<=Size;i++)
			if (Nodes[i].item.pItem == pItem)
				return &Nodes[i];
		return NULL;
	}

private:
	inline unsigned int	LeftChild(unsigned int i) const		{ return (i<<1); }
	inline unsigned int	RightChild(unsigned int i) const	{ return (i<<1)+1; }
	inline unsigned int	Parent(unsigned int i) const		{ return (i>>1); }

#if PSVR__QUALITY_ASSURANCE
#if PSVR__CHECKSUM
	float CheckSum;
#endif
	
	inline void VerifyHeap()
	{
		unsigned int i;

		for (i=2; i<=Size; i++)
		{
			Assert(Nodes[i].Key >= Nodes[Parent(i)].Key);
		}
		for (i=1; i<=Size; i++)
		{
			Assert(*(Nodes[i].Handle) == &(Nodes[i]) || Nodes[i].Handle == &NodeDump);
		}
		
#if PSVR__CHECKSUM
		if (Size > 0 && CheckSum)
		{
			float count = (float)(0.0f);
			for (i=1; i<=Size; i++)
				count += Nodes[i].Key;
			float diff = count - CheckSum;
			float ratio = diff / CheckSum;
			if(ratio > 0.01f || ratio < -0.01f)
				Warningf("Discrpenceny of %.4f (%.2f%%) in key total.  Heap size is %d.\n", diff, ratio*100, Size);
		}
#endif
	}
#endif

	// PURPOSE:  "Bubble down" changes to node i
	// PARAMS:	i: index of the node from which to "bubble down"
	inline void HeapifyDown(unsigned int i)
	{
		const float key						= Nodes[i].Key;
		const float fDistanceTravelled		= Nodes[i].fDistanceTravelled;
		void * pItem						= Nodes[i].item.pItem;
		const u16 iFlags					= Nodes[i].iFlags;
		const u16 iReachedByConnectionPoly	= Nodes[i].iReachedByConnectionPoly;
		const Vector3 vPointInPoly(Nodes[i].vPointInPoly[0], Nodes[i].vPointInPoly[1], Nodes[i].vPointInPoly[2]);
		const Vector3 vDirFromPrevious(Nodes[i].vDirFromPrevious[0], Nodes[i].vDirFromPrevious[1], Nodes[i].vDirFromPrevious[2]);

		Node** Handle		= Nodes[i].Handle;

		unsigned int scan	= i;
		unsigned int child	= i;
		unsigned int left	= LeftChild(scan);
		unsigned int right	= RightChild(scan);
		Nodes[scan].Key		= key;
		if (left <= Size && Nodes[left].Key < Nodes[child].Key)
			child = left;
		if (right <= Size && Nodes[right].Key < Nodes[child].Key)
			child = right;

		while (child != scan)
		{
			Nodes[scan].Key							= Nodes[child].Key;
			Nodes[scan].fDistanceTravelled			= Nodes[child].fDistanceTravelled;
			Nodes[scan].item.pItem					= Nodes[child].item.pItem;
			Nodes[scan].iFlags						= Nodes[child].iFlags;
			Nodes[scan].iReachedByConnectionPoly	= Nodes[child].iReachedByConnectionPoly;
			Nodes[scan].vPointInPoly[0]				= Nodes[child].vPointInPoly[0];
			Nodes[scan].vPointInPoly[1]				= Nodes[child].vPointInPoly[1];
			Nodes[scan].vPointInPoly[2]				= Nodes[child].vPointInPoly[2];
			Nodes[scan].vDirFromPrevious[0]			= Nodes[child].vDirFromPrevious[0];
			Nodes[scan].vDirFromPrevious[1]			= Nodes[child].vDirFromPrevious[1];
			Nodes[scan].vDirFromPrevious[2]			= Nodes[child].vDirFromPrevious[2];
			Nodes[scan].Handle						= Nodes[child].Handle;
			*(Nodes[scan].Handle)					= &(Nodes[scan]);
			
			scan				= child;
			left				= LeftChild(scan);
			right				= RightChild(scan);
			Nodes[scan].Key		= key;

			if (left <= Size && Nodes[left].Key < Nodes[child].Key)
				child = left;
			if (right <= Size && Nodes[right].Key < Nodes[child].Key)
				child = right;
		}
		
		if (scan != i)
		{
			Nodes[scan].fDistanceTravelled			= fDistanceTravelled;
			Nodes[scan].item.pItem					= pItem;
			Nodes[scan].iFlags						= iFlags;
			Nodes[scan].iReachedByConnectionPoly	= iReachedByConnectionPoly;
			Nodes[scan].vPointInPoly[0]				= vPointInPoly.x;
			Nodes[scan].vPointInPoly[1]				= vPointInPoly.y;
			Nodes[scan].vPointInPoly[2]				= vPointInPoly.z;
			Nodes[scan].vDirFromPrevious[0]			= vDirFromPrevious.x;
			Nodes[scan].vDirFromPrevious[1]			= vDirFromPrevious.y;
			Nodes[scan].vDirFromPrevious[2]			= vDirFromPrevious.z;
			Nodes[scan].Handle						= Handle;
			*(Nodes[scan].Handle)					= &(Nodes[scan]);
		}
	}

	// PURPOSE:  "Bubble up" changes to node i
	// PARAMS:	i: index of the node from which to "bubble up"
	// RETURNS: the index where we stopped "bubbling".
	inline unsigned int HeapifyUp(unsigned int i)
	{
		const float key						= Nodes[i].Key;
		const u16 iFlags					= Nodes[i].iFlags;
		const u16 iReachedByConnectionPoly	= Nodes[i].iReachedByConnectionPoly;
		const float fDistanceTravelled		= Nodes[i].fDistanceTravelled;
		void * pItem						= Nodes[i].item.pItem;
		const Vector3 vPointInPoly(Nodes[i].vPointInPoly[0], Nodes[i].vPointInPoly[1], Nodes[i].vPointInPoly[2]);
		const Vector3 vDirFromPrevious(Nodes[i].vDirFromPrevious[0], Nodes[i].vDirFromPrevious[1], Nodes[i].vDirFromPrevious[2]);

		Node** Handle = Nodes[i].Handle;

		unsigned int scan	=i;
		unsigned int parent	=Parent(scan);
		while (scan>1 && Nodes[parent].Key > key)
		{
			Nodes[scan].Key							= Nodes[parent].Key;
			Nodes[scan].fDistanceTravelled			= Nodes[parent].fDistanceTravelled;
			Nodes[scan].item.pItem					= Nodes[parent].item.pItem;
			Nodes[scan].iFlags						= Nodes[parent].iFlags;
			Nodes[scan].iReachedByConnectionPoly	= Nodes[parent].iReachedByConnectionPoly;
			Nodes[scan].vPointInPoly[0]				= Nodes[parent].vPointInPoly[0];
			Nodes[scan].vPointInPoly[1]				= Nodes[parent].vPointInPoly[1];
			Nodes[scan].vPointInPoly[2]				= Nodes[parent].vPointInPoly[2];
			Nodes[scan].vDirFromPrevious[0]			= Nodes[parent].vDirFromPrevious[0];
			Nodes[scan].vDirFromPrevious[1]			= Nodes[parent].vDirFromPrevious[1];
			Nodes[scan].vDirFromPrevious[2]			= Nodes[parent].vDirFromPrevious[2];

			Nodes[scan].Handle		= Nodes[parent].Handle;
			*(Nodes[scan].Handle)	= &(Nodes[scan]);
			scan					= parent;
			parent					= Parent(scan);
		}
		if (scan != i)
		{
			Nodes[scan].Key							= key;
			Nodes[scan].fDistanceTravelled			= fDistanceTravelled;
			Nodes[scan].item.pItem					= pItem;
			Nodes[scan].iFlags						= iFlags;
			Nodes[scan].iReachedByConnectionPoly	= iReachedByConnectionPoly;
			Nodes[scan].vPointInPoly[0]				= vPointInPoly.x;
			Nodes[scan].vPointInPoly[1]				= vPointInPoly.y;
			Nodes[scan].vPointInPoly[2]				= vPointInPoly.z;
			Nodes[scan].vDirFromPrevious[0]			= vDirFromPrevious.x;
			Nodes[scan].vDirFromPrevious[1]			= vDirFromPrevious.y;
			Nodes[scan].vDirFromPrevious[2]			= vDirFromPrevious.z;
			Nodes[scan].Handle						= Handle;
			*(Nodes[scan].Handle)					= &(Nodes[scan]);
		}
		return scan;
	}

	// PURPOSE:  Swap the info in one heap nodes with the info in another
	// PARAMS:	a, b:  the nodes to swap
	inline void Swap(unsigned int a, unsigned int b)
	{
		Assert(a<=Size && b<=Size);

		Node& nodeA = Nodes[a];
		Node& nodeB = Nodes[b];

		const float tempKey					= nodeA.Key;
		const float fDistanceTravelled		= nodeA.fDistanceTravelled;
		void * pItem						= nodeA.item.pItem;
		const u16 iFlags					= nodeA.iFlags;
		const u16 iReachedByConnectionPoly	= nodeA.iReachedByConnectionPoly;
		const Vector3 vPointInPoly(nodeA.vPointInPoly[0], nodeA.vPointInPoly[1], nodeA.vPointInPoly[2]);
		const Vector3 vDirFromPrevious(nodeA.vDirFromPrevious[0], nodeA.vDirFromPrevious[1], nodeA.vDirFromPrevious[2]);
		Node**	tempHandle					= nodeA.Handle;

		nodeA.Key						= nodeB.Key;
		nodeA.fDistanceTravelled		= nodeB.fDistanceTravelled;
		nodeA.item.pItem				= nodeB.item.pItem;
		nodeA.iFlags					= nodeB.iFlags;
		nodeA.iReachedByConnectionPoly	= nodeB.iReachedByConnectionPoly;
		nodeA.vPointInPoly[0]			= nodeB.vPointInPoly[0];
		nodeA.vPointInPoly[1]			= nodeB.vPointInPoly[1];
		nodeA.vPointInPoly[2]			= nodeB.vPointInPoly[2];
		nodeA.vDirFromPrevious[0]		= nodeB.vDirFromPrevious[0];
		nodeA.vDirFromPrevious[1]		= nodeB.vDirFromPrevious[1];
		nodeA.vDirFromPrevious[2]		= nodeB.vDirFromPrevious[2];
		nodeA.Handle					= nodeB.Handle;

		nodeB.Key						= tempKey;	
		nodeB.fDistanceTravelled		= fDistanceTravelled;
		nodeB.item.pItem				= pItem;
		nodeB.iFlags					= iFlags;
		nodeB.iReachedByConnectionPoly	= iReachedByConnectionPoly;
		nodeB.vPointInPoly[0]			= vPointInPoly.x;
		nodeB.vPointInPoly[1]			= vPointInPoly.y;
		nodeB.vPointInPoly[2]			= vPointInPoly.z;
		nodeB.vDirFromPrevious[0]		= vDirFromPrevious.x;
		nodeB.vDirFromPrevious[1]		= vDirFromPrevious.y;
		nodeB.vDirFromPrevious[2]		= vDirFromPrevious.z;
		nodeB.Handle					= tempHandle;

		*(nodeA.Handle)	=&nodeA;
		*(nodeB.Handle)	=&nodeB;
	}

private:

	unsigned int	MaxSize;	// The maximum number of nodes available in this heap.
	unsigned int	Size;		// How many nodes are currently used in the heap (use 1 thru Size, not 0 thru Size-1)
	Node*			Nodes;		// The array of heap nodes
	Node*			NodeDump;	// See the explanation for Insert.  Nodes who don't need handles update this instead!
};

//-----------------------------------------------------------------------------

class CPathSearchPriorityQueue
{
public:
	CPathSearchPriorityQueue(s32 iMaxSize);
	~CPathSearchPriorityQueue(void);

	void Clear(void);

	bool Insert(float fCost, float fDistanceTravelled, TNavMeshPoly * pPoly, const Vector3 & vPointInPoly, const Vector3 & vDirectionFromPrevious, const u32 iFlags, const u32 iReachedByConnectionPoly);
	bool RemoveTop(float & fCost, float & fDistanceTravelled, TNavMeshPoly *& pPoly, Vector3 & vPointInPoly, Vector3 & vDirFromPrevious, u32 & iFlags, u32 & iReachedByConnectionPoly);
	// If the new cost is less than the stored cost, then remove from the queue
	bool RemoveIfLessCost(TNavMeshPoly * pPoly, float fNewCost);
	// If the new cost is greater than the stored cost, then remove from the queue
	bool RemoveIfGreaterCost(TNavMeshPoly * pPoly, float fNewCost);

	bool Insert(float fCost, float fDistanceTravelled, CHierarchicalNavNode * pNode, const Vector3 & vPointInPoly, const Vector3 & vDirectionFromPrevious, const u32 iFlags);
	bool RemoveTop(float & fCost, float & fDistanceTravelled, CHierarchicalNavNode *& pNode, Vector3 & vPointInPoly, Vector3 & vDirFromPrevious, u32 & iFlags);
	// If the new cost is less than the stored cost, then remove from the queue
	bool RemoveIfLessCost(CHierarchicalNavNode * pNode, float fNewCost);
	// If the new cost is greater than the stored cost, then remove from the queue
	bool RemoveIfGreaterCost(CHierarchicalNavNode * pNode, float fNewCost);


	inline CPathServerBinHeap * GetBinHeap(void) { return m_pBinHeap; }
	s32 GetMemoryUsed(void) { return sizeof(CPathServerBinHeap::Node) * m_iMaxSize; }
	inline s32 GetNodeCount() { Assert(m_pBinHeap); return m_pBinHeap->GetNodeCount(); }

private:

	s32 m_iMaxSize;
	CPathServerBinHeap * m_pBinHeap;
};

//-----------------------------------------------------------------------------

}	// namespace rage

#endif	// AI_NAVMESH_PRIQUEUE_H

// End of file ai/navmesh/priqueue.h
