//
// ai/navmesh/pathserverbase.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_PATHSERVERBASE_H
#define AI_NAVMESH_PATHSERVERBASE_H

#include "navmesh.h"				// For GTA_ENGINE...
#include "datatypes.h"
#include "requests.h"
#include "pathserverthread.h"

#ifdef GTA_ENGINE
#include "navmeshstore.h"			// CDynamicNavMeshStore
// TODO: This is only for scrThreadId, and we need to decide what we want to do here. The framework
// navigation classes could either be allowed to depend on scrThreadId (which is a RAGE concept), or
// we could change to a generic user data int instead, or, refactor so that this part stays above the
// framework.
#include "script/thread.h"
#else
typedef u32 scrThreadId;
#include "..\..\..\tools\src\GTA_Tools\navgen\DummyPathServerStore.h"
#endif

#include "system/criticalsection.h"	// sysCriticalSectionToken

// TODO: Remove this forward declaration of non-RAGE classes (probably by actually moving
// them to RAGE).
class CPathServer;
class CPathServerThread;

namespace rage
{

//**************************************************************************************************
// Define as 1 for heavy-duty checking on all the poly tessellation code
#define SANITY_CHECK_TESSELLATION	0
//**************************************************************************************************

#ifdef GTA_ENGINE
// m_NavMeshImmediateAccessCriticalSectionToken critical section must surround all code which touches the navmeshes from an ImmediateMode function from the MAIN GAME THREAD!
extern sysCriticalSectionToken m_NavMeshImmediateAccessCriticalSectionToken;
// m_NavMeshDataCriticalSectionToken critical section must surround all code which touches the navmeshes from any other function or thread.
extern sysCriticalSectionToken m_NavMeshDataCriticalSectionToken;
// m_DynamicObjectsCriticalSectionToken critical section must surround all code which touches the dynamic objects (except for the unprotected object update function)
extern sysCriticalSectionToken m_DynamicObjectsCriticalSectionToken;
// m_RequestsCriticalSectionToken critical section must surround all code which touches the pathserver requests (oaths, grids, los, audio, floodfills, etc, etc)
extern sysCriticalSectionToken m_RequestsCriticalSectionToken;
// m_NavMeshStoreLoadedMeshes critical section must surround all non-update-thread code which uses the aiMeshStore::GetLoadedMeshes() array, because this will be modifed ad-hoc from the main thread
extern sysCriticalSectionToken m_NavMeshStoreLoadedMeshesCriticalSectionToken;

#endif	// GTA_ENGINE

#define NAVMESH_INDEX_GAP_BETWEEN_STATIC_AND_DYNAMIC	10

//-----------------------------------------------------------------------------

class fwPathServer
{
public:

	static bool GetIsNavDomainEnabled(aiNavDomain domain) { return m_NavDomainsEnabled[domain]; }
	static aiNavMeshStore * GetNavMeshStore(aiNavDomain domain) { return m_pNavMeshStores[domain]; }

	static void ForceAbortCurrentPathRequest(const bool bYield=true);

#if !__FINAL
	static sysPerformanceTimer	* m_NavMeshLoadingTimer;
	static sysPerformanceTimer	* m_NavMesh2ndLoadingTimer;
	static u32 ms_iPeakNumTessellationPolys;
#endif

	static u32 m_iMaxDynamicNavmeshTypes;
	static bool m_bFindCover_NavMeshesHaveChanged;

	static u32 GetNavMeshIndexFromSector(int iSectorX, int iSectorY, aiNavDomain domain);
	static void GetSectorFromNavMeshIndex(const u32 iNavMesh, s32 & iSectorXOut, s32 & iSectorYOut, aiNavDomain domain);
	static u32 GetNavMeshIndexFromFilename(aiNavDomain domain, const char * pFilename, s32 * pSectorX=NULL, s32 * pSectorY=NULL);
	static void GetNavMeshExtentsFromIndex(const TNavMeshIndex iMeshIndex, Vector2 & vMin, Vector2 & vMax, aiNavDomain domain);

	static CDynamicNavMeshStore& GetDynamicNavMeshStore()
	{	return m_DynamicNavMeshStore;	}

	static inline CNavMesh * GetNavMeshFromIndex(const u32 index, aiNavDomain domain)
	{
		if(index == NAVMESH_INDEX_TESSELLATION)
		{
			return m_pTessellationNavMesh;
		}
		else if(index < (u32)m_pNavMeshStores[domain]->GetMaxMeshIndex())
		{
			return m_pNavMeshStores[domain]->GetMeshByIndex(index);
		}
		else if(index >= NAVMESH_INDEX_FIRST_DYNAMIC && index <= NAVMESH_INDEX_FINAL_DYNAMIC)
		{
			Assert(index-NAVMESH_INDEX_FIRST_DYNAMIC < m_iMaxDynamicNavmeshTypes);

			CModelInfoNavMeshRef & ref = m_DynamicNavMeshStore.Get(index-NAVMESH_INDEX_FIRST_DYNAMIC);
			return ref.m_pBackUpNavmeshCopy;
		}

		return NULL;
	}

	static inline bool GetExcludeVehicleDoorsFromNavigation() { return ms_bExcludeVehicleDoorsFromNavigation; }
	static inline float GetDefaultMaxPathFindDistFromOrigin() { return ms_fDefaultMaxPathFindDistFromOrigin; }

	static void SignalRequest() { sysIpcSignalSema(m_PathRequestSema); }

protected:
	friend class aiNavMeshStore;
	friend class ::CPathServerThread;

#if __HIERARCHICAL_NODES_ENABLED
	friend class aiNavNodesStore;
#endif

	static u32 m_iTotalMemoryUsed;

	//------------------------------------------------------------
	// Runtime tessellation/triangulation around dynamic objects

	static inline CNavMesh * GetTessellationNavMesh() { return m_pTessellationNavMesh; }
	static inline u32 GetCurrentNumTessellationPolys() { return m_iCurrentNumTessellationPolys; }
	static bool CreateTessellationNavMesh();
	static void DetessellateAllPolys();
	static bool IsRoomToTessellateThisPoly(const TNavMeshPoly * pPoly);
	static u32 GetNextFreeTessellationPolyIndex();
	static TNavMeshPoly * AllocTessellationPoly(const int iNumVerts);
	static bool GrowTessellationPoly(TNavMeshPoly * pPoly, const int iGrowNumVerts);
	static bool CanTessellateThisPoly(const TNavMeshPoly * pPoly);

	inline static TTessInfo * GetTessInfo(const u32 iIndex)
	{
		Assert(iIndex < CNavMesh::GetNumPolysInTesselationMesh());
		return &m_PolysTessellatedFrom[iIndex];
	}
	inline static TTessInfo * GetTessInfo(const TNavMeshPoly * pPoly)
	{
		Assert(pPoly->GetNavMeshIndex()==NAVMESH_INDEX_TESSELLATION);
		const u32 iIndex = m_pTessellationNavMesh->GetPolyIndex(pPoly);
		Assert(iIndex < CNavMesh::GetNumPolysInTesselationMesh());
		return &m_PolysTessellatedFrom[iIndex];
	}

	static sysIpcSema m_PathRequestSema;

	static bool m_bPathServerThreadIsActive;

	// Storage class for dynamic navmeshes, uses ref-counting to know when to stream in/out
	static CDynamicNavMeshStore m_DynamicNavMeshStore;

	// If this variable is set, then the current request (if taking a long time) wil
	// be aborted.  This is used by the streaming thread in the event of a navmesh
	// being loaded but unable to be placed due to the pathfinding being busy.
	// JB: I have had to add 'm_bPathServerThreadSleepingDuringSearch' because their
	// is no fwPathServerThead yet; really the worker thread(s) should own that
	// variable.
	static bool m_bForceAbortCurrentRequest;
	static bool m_bPathServerThreadSleepingDuringSearch;

	static atRangeArray<aiNavMeshStore *, kNumNavDomains> m_pNavMeshStores;
	static bool m_NavDomainsEnabled[kNumNavDomains];

	// A separate navmesh which holds all the polys tessellated from the actual navmeshes
	static CNavMesh * m_pTessellationNavMesh;

	// A record of which original polys the fragments in m_pTessellationNavMesh were first tessellated from
	static TTessInfo * m_PolysTessellatedFrom;

	// The total number of tessellation polys in use
	static u32 m_iCurrentNumTessellationPolys;
	static u32 m_iCurrentTessellationArrayIndex;
	static u32 m_iTessellationNavmeshCurrentNumVerts;
	static u32 m_iTessellationNavmeshCurrentPoolSize;
	static u32 m_iCurrentConnectionPolyIndex;

	static bool m_bHasBeenThroughEntireTessellationNavMesh;
	static bool ms_bExcludeVehicleDoorsFromNavigation;

	static float ms_fDefaultMaxPathFindDistFromOrigin;
};

//-----------------------------------------------------------------------------

#define __THREAD_STALL_CHECK	0

#if !__FINAL && defined(GTA_ENGINE) && __THREAD_STALL_CHECK
#define CHECK_FOR_THREAD_STALLS(criticalSectionToken) if(CPathServer::m_bCheckForThreadStalls) CPathServer::CheckForThreadStall(criticalSectionToken);
#else
#define CHECK_FOR_THREAD_STALLS(criticalSectionToken)
#endif

#if !__FINAL && defined(GTA_ENGINE) && __THREAD_STALL_CHECK
#define START_THREAD_STALL_TIMER if(CPathServer::m_bCheckForThreadStalls) { CPathServer::m_MainGameThreadStallTimer->Reset(); CPathServer::m_MainGameThreadStallTimer->Start(); }
#else
#define START_THREAD_STALL_TIMER
#endif

#if !__FINAL && defined(GTA_ENGINE) && __THREAD_STALL_CHECK
#define END_THREAD_STALL_TIMER if(CPathServer::m_bCheckForThreadStalls) { CPathServer::m_fMainGameTimeSpendOnThreadStalls += CPathServer::m_MainGameThreadStallTimer->GetTimeMS(); CPathServer::m_MainGameThreadStallTimer->Reset(); }
#else
#define END_THREAD_STALL_TIMER
#endif

//************************************************************************************************
//	Lock/unlock macros to wait for critical-sections protecting different parts of the pathfinder

#define LOCK_NAVMESH_DATA																			\
	CHECK_FOR_THREAD_STALLS(m_NavMeshDataCriticalSectionToken)										\
	sysCriticalSection navMeshDataCriticalSection(m_NavMeshDataCriticalSectionToken);				\

#define UNLOCK_NAVMESH_DATA																			\
	navMeshDataCriticalSection.Exit();																\

#define LOCK_IMMEDIATE_DATA																			\
	CHECK_FOR_THREAD_STALLS(m_NavMeshImmediateAccessCriticalSectionToken)							\
	sysCriticalSection immediateDataCriticalSection(m_NavMeshImmediateAccessCriticalSectionToken);	\

#define UNLOCK_IMMEDIATE_DATA																		\
	immediateDataCriticalSection.Exit();															\

#define LOCK_DYNAMIC_OBJECTS_DATA																	\
	CHECK_FOR_THREAD_STALLS(m_DynamicObjectsCriticalSectionToken)									\
	sysCriticalSection dynamicObjectsCriticalSection(m_DynamicObjectsCriticalSectionToken);			\

#define UNLOCK_DYNAMIC_OBJECTS_DATA																	\
	dynamicObjectsCriticalSection.Exit();															\

#define LOCK_REQUESTS																				\
	CHECK_FOR_THREAD_STALLS(m_RequestsCriticalSectionToken)											\
	sysCriticalSection requestsCriticalSection(m_RequestsCriticalSectionToken);						\

#define UNLOCK_REQUESTS																				\
	requestsCriticalSection.Exit();																	\


#define LOCK_STORE_LOADED_MESHES																	\
	CHECK_FOR_THREAD_STALLS(m_NavMeshStoreLoadedMeshesCriticalSectionToken)							\
	sysCriticalSection loadedMeshesCriticalSection(m_NavMeshStoreLoadedMeshesCriticalSectionToken);	\

#define UNLOCK_STORE_LOADED_MESHES																	\
	loadedMeshesCriticalSection.Exit();																\


#if !__FINAL
#define START_PERF_TIMER(t)			{ t->Reset(); t->Start(); }
#define GET_PERF_TIMER(t, f)			{ f = t->GetTimeMS(); }
#define STOP_PERF_TIMER(t, f)		{ t->Stop(); f = t->GetTimeMS(); }
#define STOPANDADD_PERF_TIMER(t, f)	{ t->Stop(); f += t->GetTimeMS(); }
#else
#define START_PERF_TIMER(t)
#define GET_PERF_TIMER(t, f)
#define STOP_PERF_TIMER(t, f)
#define STOPANDADD_PERF_TIMER(t, f)
#endif

//-----------------------------------------------------------------------------

class CPathRequest;
struct TDynamicObject;
struct TRequestPathStruct;

// #define this to force the pathserver thread to Sleep(0) when it has passed a certain number of iterations
//#define PATHSERVER_YIELDS_THREADTIME_EXPLICITLY

#define __VALIDATE_PEDGEN_COORDS_ARE_ON_CORRECT_POLYS	0

#define __IGNORE_VEHICLES_FOR_WANDER_PATHS			1

//**********************************************************************
//	Path-Flags.  These flags govern how the path is generated, and are
//	passed into the RequestPath() function.
//	Flags are now passed as u64
//******************************************************************
#define PATH_FLAG_PREFER_PAVEMENTS								0x01
#define PATH_FLAG_NEVER_CLIMB_OVER_STUFF						0x02
#define PATH_FLAG_NEVER_DROP_FROM_HEIGHT						0x04
#define PATH_FLAG_ENSURE_LOS_BEFORE_ENDING						0x08
#define PATH_FLAG_NEVER_USE_LADDERS								0x10
#define PATH_FLAG_FLEE_TARGET									0x20
#define PATH_FLAG_WANDER										0x40
#define PATH_FLAG_NEVER_ENTER_WATER								0x80
#define PATH_FLAG_DYNAMIC_NAVMESH_ROUTE							0x100
#define PATH_FLAG_DONT_AVOID_DYNAMIC_OBJECTS					0x200
#define PATH_FLAG_PREFER_DOWNHILL								0x400
#define PATH_FLAG_SELECT_CLOSEST_LOADED_NAVMESH_TO_TARGET		0x800
#define PATH_FLAG_PRESERVE_SLOPE_INFO_IN_PATH					0x1000
#define PATH_FLAG_CUT_SHARP_CORNERS								0x2000
#define PATH_FLAG_USE_LARGER_SEARCH_EXTENTS						0x4000
#define PATH_FLAG_DONT_LIMIT_SEARCH_EXTENTS						0x8000
#define PATH_FLAG_SCRIPTED_ROUTE								0x10000
#define PATH_FLAG_REDUCE_OBJECT_BBOXES							0x20000
#define PATH_FLAG_MAY_USE_FATAL_DROPS							0x40000
#define PATH_FLAG_IF_NOT_ON_PAVEMENT_ALLOW_DROPS_AND_CLIMBS		0x80000
#define PATH_FLAG_NEVER_START_IN_WATER							0x100000
#define PATH_FLAG_IGNORE_NON_SIGNIFICANT_OBJECTS				0x200000
#define PATH_FLAG_FLEE_NEVER_END_IN_WATER						0x400000
#define PATH_FLAG_ALLOW_TO_NAVIGATE_UP_STEEP_POLYGONS			0x800000
#define PATH_FLAG_ALLOW_TO_PUSH_VEHICLE_DOORS_CLOSED			0x1000000
#define PATH_FLAG_MISSION_PED									0x2000000
#define PATH_FLAG_RANDOMISE_POINTS								0x4000000
#define PATH_FLAG_DEACTIVATE_OBJECTS_IF_CANT_RESOLVE_ENDPOINTS	0x8000000
#define PATH_FLAG_USE_DIRECTIONAL_COVER							0x10000000
#define PATH_FLAG_FAVOUR_ENCLOSED_SPACES						0x20000000
#define PATH_FLAG_AVOID_POTENTIAL_EXPLOSIONS					0x40000000
#define PATH_FLAG_NEVER_LEAVE_PAVEMENTS							0x80000000
#define PATH_FLAG_DONT_AVOID_FIRE								0x100000000
#define PATH_FLAG_HIGH_PRIO_ROUTE								0x200000000
#define PATH_FLAG_NEVER_LEAVE_WATER								0x400000000
#define PATH_FLAG_EXPAND_START_END_TESSELLATION_RADIUS			0x800000000
#define PATH_FLAG_KEEP_UPDATING_PED_START_POSITION				0x1000000000
#define PATH_FLAG_AVOID_TEAR_GAS								0x2000000000
#define PATH_FLAG_USE_BEST_ALTERNATE_ROUTE_IF_NONE_FOUND		0x4000000000
#define PATH_FLAG_COVERFINDER									0x8000000000
#define PATH_FLAG_PULL_FROM_EDGE_EXTRA							0x10000000000
#define PATH_FLAG_SOFTER_FLEE_HEURISTICS						0x20000000000
#define PATH_FLAG_AVOID_TRAIN_TRACKS							0x40000000000
#define PATH_FLAG_NEVER_LEAVE_DEEP_WATER						0x80000000000

#define TIMESTAMP_MAX_VALUE										0xBFFF	// purposefully 16384 less than the max u16 value of 0xFFFF, a safeguard since overflow is now only handled after a request has completed

#define __BREAK_UPON_PROCESSING_CONNECTION_POLY					0

//*********************************************************************************************

#define SHORT_LINE_OF_SIGHT_MAXDIST										16.0f
#define IMMEDIATE_MODE_QUERY_MAXNUMVISITEDPOLYS							256

//*********************************************************************************************

class CDynamicObjectsGridCell
{
public:

	static const float ms_fSizeOfGrid;

	CDynamicObjectsGridCell()
	{
		m_MinMaxOfGrid.SetInvalid();
		m_MinMaxOfObjectsInGrid.SetInvalid();
		m_pFirstDynamicObject = NULL;
		m_bMinMaxOfObjectsNeedsRecalculating = false;
		m_iTimeStamp = 0;
	}
	~CDynamicObjectsGridCell() { }

	void RecalculateMinMaxOfObjectsInGrid(void);

	TShortMinMax m_MinMaxOfGrid;
	TShortMinMax m_MinMaxOfObjectsInGrid;
	TDynamicObject * m_pFirstDynamicObject;

	u32 m_bMinMaxOfObjectsNeedsRecalculating		:1;
	u32 m_iTimeStamp;
};


class CGridCellsCache
{
public:

	static const float ms_fMaxRange;
	static const float ms_fCacheCellSize;

	static const s32 ms_iNumCellsAcross = 32;
	static const s32 ms_iNumCacheCells = 256;

	static s32 m_iNextFree;

	class CEntry
	{
	public:
		CDynamicObjectsGridCell * m_pGridCell;
		CEntry * m_pNextVertical;
	};

	static void Reset(const Vector3 & vOrigin);
	static bool Add(CDynamicObjectsGridCell * pGridCell);
	static s32 GetGridCellsIntersectingRegion(const TShortMinMax & region, CDynamicObjectsGridCell ** ppGridCells, const s32 iMaxNumItems);

	// NxN array of entry pointers for all grid cells intersecting each XY region
	static CEntry * m_pEntries[ CGridCellsCache::ms_iNumCellsAcross * CGridCellsCache::ms_iNumCellsAcross ];

	// Pool storage for entries
	static CEntry m_pPool[CGridCellsCache::ms_iNumCacheCells];

	static Vector3 m_vMin;
	static Vector3 m_vMax;

	static TShortMinMax m_MinMax;
};

//***************************************************************************
//	CDynamicObjectsContainer
//	This is a container which holds a number of grid-cells, each of which
//	contains dynamic objects.
//***************************************************************************

class CDynamicObjectsContainer
{
	// TODO: Fix this friend declaration - shouldn't friend declare classes in GTA from RAGE.
	friend class ::CPathServer;
	friend class ::CPathServerThread;

public:

	typedef bool (*PerObjectCB)(TDynamicObject * pObject, void * pData);

public:

	CDynamicObjectsContainer()
	{
		Init();
	}
	~CDynamicObjectsContainer()
	{
		Shutdown();
	}

	static void Init(void);
	static void Shutdown(void);
	static void Clear(void);

	static void RemoveObjectFromGridCell(TDynamicObject * pObject);
	static CDynamicObjectsGridCell * AddObjectToGridCell(TDynamicObject * pObject);
	static CDynamicObjectsGridCell * MoveObjectToAnotherGridCellIfRequired(TDynamicObject * pObject);

	static void ForAllObjectsIntersectingRegion(const TShortMinMax & minMax, PerObjectCB callBackFn, void * pData = NULL);
	static bool DoesRegionIntersectAnyObjects(const TShortMinMax & minMax);
	static void GetObjectsIntersectingRegion(const TShortMinMax & minMax, atArray<TDynamicObject*> & objectsList, const u32 iFlags=0, const s32 iMaxCount=0xFFFF);
	static void GetObjectsIntersectingRegionUsingCache(const TShortMinMax & minMax, atArray<TDynamicObject*> & objectsList, const u32 iFlags=0, const s32 iMaxCount=0xFFFF);

	static void RecalculateExtentsOfAllMarkedGrids();

	static void InitGridCellsCache(const Vector3 & vSearchOrigin, const TShortMinMax & searchExtents);
	static void InvalidateCache() { ms_bCacheEnabled = false; }

	static void AdjustAllBoundsByAmount(const TShortMinMax & minMax, const float fRadiusDelta, const bool bInitialAdjustment);

	inline static int GetNumGrids(void) { return ms_DynamicObjectGrids.GetCount(); }

	inline static CDynamicObjectsGridCell * GetGridCell(const s32 g) { return ms_DynamicObjectGrids[g]; }

	static const int ms_iMaxNumberOfGrids = 64;

	static inline bool GetCacheEnabled() { return ms_bCacheEnabled; }

private:

	static atArray<CDynamicObjectsGridCell*> ms_DynamicObjectGrids;

	// During the setup phase of a path request, this NxN array of grid cells is created
	// allowing for very quick lookup of which cells intersect a given region
	static CGridCellsCache m_GridCellsCache;

	// Cache can be disabled on some path requests, if there was not enough space to cache all of the object grids intersecting the path request
	static bool ms_bCacheEnabled;
};

//***********************************************************************
//	TPathRegionSwitch
//	This structure handles swithcing on/off parts of navmeshes, so that
//	designers can prevent peds from being created in these areas.
//***********************************************************************

struct TPathRegionSwitch
{
	Vector3 m_vMin;
	Vector3 m_vMax;

	// Whether this switch is on (peds are allowed in area), or off (peds are not allowed)
	ESWITCH_NAVMESH_POLYS m_Switch;

	// Unique identifier of the script which created this region (or 0xFFFFFFFF if not from a script).
	// Script mission cleanup code can call CPathServer::RemoveAllPathRegionSwitchesForMission() to
	// remove all the switches created with that UID.
	scrThreadId m_iScriptUID;
};

#define MAX_NUM_PATH_REGION_SWITCHES	64

inline void
CompressVertex8(Vector3 & vertex, u8 & iX, u8 & iY, u8 & iZ, const Vector3 & vMin, const Vector3 & vSize)
{
	float fTmp;
	s32 iTmp;

	// X Component
	fTmp = vertex.x - vMin.x;
	fTmp /= vSize.x;
	iTmp = (s32)(fTmp * 256.0f);

	if(iTmp < 0) iTmp = 0;
	else if(iTmp > 255) iTmp = 255;
	iX = (u8)iTmp;

	// Y Component
	fTmp = vertex.y - vMin.y;
	fTmp /= vSize.y;
	iTmp = (s32)(fTmp * 256.0f);

	if(iTmp < 0) iTmp = 0;
	else if(iTmp > 255) iTmp = 255;
	iY = (u8)iTmp;

	// Z Component
	fTmp = vertex.z - vMin.z;
	fTmp /= vSize.z;
	iTmp = (s32)(fTmp * 256.0f);

	if(iTmp < 0) iTmp = 0;
	else if(iTmp > 255) iTmp = 255;
	iZ = (u8)iTmp;
}


inline void
DecompressVertex8(Vector3 & vertex, u8 & iX, u8 & iY, u8 & iZ, const Vector3 & vMin, const Vector3 & vSize)
{
	const float fRecip256 = ((float)(1.0 / 256.0));

	// unpack into 0..1 range
	vertex.x = (((float)iX) * fRecip256);
	vertex.y = (((float)iY) * fRecip256);
	vertex.z = (((float)iZ) * fRecip256);

	// scale by the total extents of the block of sectors
	vertex.x *= vSize.x;
	vertex.y *= vSize.y;
	vertex.z *= vSize.z;

	// offset by mins of entire block
	vertex.x += vMin.x;
	vertex.y += vMin.y;
	vertex.z += vMin.z;
}

struct CNavMeshEdgeGroup
{
	s8 m_iAdjacentEdgeGroup;		// The index of the connecting edge in the adjacent navmesh (-1 = none)
	u8 m_Pos[3];					// Compressed into 8bits per float
};	// 4bytes

// Defines the connectivity within a single navmesh.
struct CNavMeshEdgeGroups
{
	u8 m_iNumEdgeGroups[4];				// How many edge groups exist for each navmesh edge
	CNavMeshEdgeGroup * m_pEdgeGroupPtr;	// Pointer into the pool of all the edge-groups in this navmesh
	u8 * m_pConnectivityBits;			// Pointer to the start of a bit field, detailing which edges connect to which others within this navmesh (*)

	s16 m_iPathfindParentNavMeshIndex;	// The navmesh which led to this one
	s16 m_iPathfindThisNavMeshEntryEdge;	// The edge-group in this navmesh, by which we entered into this navmesh

	inline bool CanEdgeGetToEdge(u32 iEdge1, u32 iEdge2)
	{
		u32 iNumEdgeGroups = m_iNumEdgeGroups[0] + m_iNumEdgeGroups[1] + m_iNumEdgeGroups[2] + m_iNumEdgeGroups[3];
		u32 iBitIndex = (iNumEdgeGroups*iEdge1)+iEdge2;
		bool bCanEdgeGetToEdge = (m_pConnectivityBits[iBitIndex/8] & (1<<(iBitIndex%8))) != 0;

#ifdef DEBUG
		u32 iBitIndexRev = (iNumEdgeGroups*iEdge2)+iEdge1;
		bool bCanEdgeGetToEdgeInReverse = (m_pConnectivityBits[iBitIndexRev/8] & (1<<(iBitIndexRev%8)));
		Assert(bCanEdgeGetToEdge == bCanEdgeGetToEdgeInReverse);
#endif

		return bCanEdgeGetToEdge;
	}

};	// 16 bytes

//************************************************************************************
// (*) to save memory, I only store the minimum amount of data.
// For 'n' edges, the amount of unique combinations is : (n! / ((n-2)!2!))
// For example, with 5 edges - there are 10 combinations.
// With 6 edges, there are 15 combinations, etc..
//************************************************************************************



//************************************************************************************
//	TCachedCoverPoint and the following #define's are used to control the extraction
//	of coverpoints from navmeshes, and their insertion into the CCover class.
//	A timesliced system is used since this could be a major performance hit.
//	Cover points are extracted in the CPathServerThread, and put into a buffer of
//	TCachedCoverPoints.  This buffer is then processed and copies into the main game.
//************************************************************************************
#define	MAX_NUM_NAVMESHES_FOR_COVERAREAS	128
#define SIZE_OF_COVERPOINTS_BUFFER			128
#define SIZE_OF_WATEREDGES_BUFFER			16

struct TCachedCoverPoint
{
	float m_fXPos, m_fYPos, m_fZPos;
	TCoverPointFlags m_CoverPointFlags;	// u16
};


// This is the maximum number of individual regions (origin + radius) around which to load in navmeshes
//#define NAVMESH_MAX_REQUIRED_REGIONS		3
#define MAX_LOADED_NAVMESHES				256
#define NUM_STRESS_TEST_PATHS				10

enum NavMeshRequiredRegion
{
	NMR_GameplayOrigin,
	NMR_NetworkRespawnMgr,
	NMR_Script,

	NAVMESH_MAX_REQUIRED_REGIONS
};

// Info read from the "nav.dat" file, and used to initialise the aiNavMeshStore
class CNavDatInfo
{
public:
	int iMaxMeshIndex;
	int iSectorsPerMesh;
	float fMeshSize;
	int iNumMeshesInX;
	int iNumMeshesInY;
	int iNumMeshesInAnyLevel;

	CNavDatInfo()
	{
		iMaxMeshIndex = 0;
		iSectorsPerMesh = 0;
		fMeshSize = 0.0f;
		iNumMeshesInX = 0;
		iNumMeshesInY = 0;
		iNumMeshesInAnyLevel = 0;
	}
};

//**************************************************************
//	CPathServer
//	This is the interface which clients use to find paths
//	asynchronously using the CPathServerThread class.
//**************************************************************

struct TNavMeshRequiredRegion
{
	Vector2 m_vOrigin;
	float m_fNavMeshLoadRadius;
	float m_fHierarchicalNodesLoadRadius;
	scrThreadId m_iThreadId;
	bool m_bActive;
#if __BANK
	char m_ScriptName[128];
#endif
};

struct TLoadedNavMeshInfo
{
	inline TLoadedNavMeshInfo()
	{
		m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		m_iHierarchicalNodesIndex = NAVMESH_NAVMESH_INDEX_NONE;
		m_bNavMeshRequired = false;
		m_bHierarchicalNodesRequired = false;
	}
	inline TLoadedNavMeshInfo(const TNavMeshIndex iMesh, const TNavMeshIndex iNodes, const bool bNavMesh, const bool bHierarchicalNodes)
	{
		m_iNavMeshIndex = iMesh;
		m_iHierarchicalNodesIndex = iNodes;
		m_bNavMeshRequired = bNavMesh;
		m_bHierarchicalNodesRequired = bHierarchicalNodes;
	}
	TNavMeshIndex m_iNavMeshIndex;
	// The nodes index may be different from the navmesh index, since it will point the start of an NxN block
	TNavMeshIndex m_iHierarchicalNodesIndex;
	bool m_bNavMeshRequired;
	bool m_bHierarchicalNodesRequired;
};
enum EGetClosestPosRet
{
	ENoPositionFound,
	EPositionFoundOnMainMap,
	EPositionFoundInAnInterior
};

/*
PURPOSE
	Interface class to give games a chance to extend the functionality of CPathServer.
*/
class fwPathServerGameInterface
{
public:
	virtual ~fwPathServerGameInterface() {}

	virtual void OverridePathRequestParameters(const TRequestPathStruct& reqStruct, CPathRequest& pathRequest) const = 0;
};

}	// namespace rage

#endif	// AI_NAVMESH_PATHSERVERBASE_H

// End of file ai/navmesh/pathserverbase.h
