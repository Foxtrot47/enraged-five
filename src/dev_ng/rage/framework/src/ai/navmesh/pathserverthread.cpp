//
// ai/navmesh/pathserverthread.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "pathserverthread.h"
#include "pathserverbase.h"
#include "navmeshoptimisations.h"

NAVMESH_OPTIMISATIONS()

using namespace rage;

const float TRequestPathStruct::ms_fDefaultMaxDistanceToAdjustPathEndPoints = 10.0f;

void TPathFindVars::Init()
{
	m_fMaxPathFindDistFromOrigin = fwPathServer::GetDefaultMaxPathFindDistFromOrigin();
	m_fMinPathFindDistFromOrigin = 30.0f;
	m_fMinSlopeDotProductWithUpVector = 0.0f;
}

bool TPathFindVars::InsertPoly(int iPos, const Vector3& vPoint, TNavMeshPoly* pPoly, const TNavMeshWaypointFlag& Flags)
{
	if (iPos >= MAX_PATH_POLYS || m_iNumPathPolys >= MAX_PATH_POLYS)	// Note, only since in the struct we got + 2 and + 4 elements allocated
		return false;

	// I am guessing this is the more efficient way, rather than copying to a separate buffer
	for (int i = m_iNumPathPolys + 1; i >= iPos; --i)
	{
		m_vClosestPointInPathPolys[i] = m_vClosestPointInPathPolys[i - 1];
		m_PathPolys[i] = m_PathPolys[i - 1];
		m_iPolyWaypointFlags[i] = m_iPolyWaypointFlags[i - 1];
	}

	m_vClosestPointInPathPolys[iPos] = vPoint;
	m_PathPolys[iPos] = pPoly;
	m_iPolyWaypointFlags[iPos] = Flags;
	++m_iNumPathPolys;
	return true;
}

fwPathServerThread::fwPathServerThread() :
	m_iThreadID(0),
	m_pCurrentActiveRequest(NULL),
	m_bForcePerformThreadHousekeeping(false)
{

}


// End of file ai/navmesh/pathserverthread.cpp
