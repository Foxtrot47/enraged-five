//
// ai/navmesh/pathserverthread.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_PATHSERVERTHREAD_H
#define AI_NAVMESH_PATHSERVERTHREAD_H

#include "requests.h"

// TODO: check back later to see what's actually used from here (at least TNavMeshAndPoly is).
#include "ai/navmesh/navmesh.h"

#include "system/ipc.h"		// For PRE_DECLARE_THREAD_FUNC, check back later if needed. TODO.

// TODO: should probably be moved to namespace rage once the definition is in place.
#ifndef GTA_ENGINE
DWORD WINAPI PathServerThreadFunc(LPVOID ptr);
#else
PRE_DECLARE_THREAD_FUNC(PathServerThreadFunc);
#endif

namespace rage
{

class fwEntity;

#define MAX_PATH_VISITED_POLYS								8192 //(32k)	was 4096

#define MAX_HIERARCHICAL_PATH_NODES							256
#define MAX_PATH_VISITED_NODES								4096 //(16k)

#define PRESERVE_HEIGHT_CHANGE_MAX_NUM_PTS					32

//******************************************************************
#define PATHSERVER_MAX_NUM_DYNAMIC_OBJECTS					640
//******************************************************************

struct TRequestPathStruct
{
public:
	static const float ms_fDefaultMaxDistanceToAdjustPathEndPoints;
public:
	TRequestPathStruct()
	{
		m_vPathStart = Vector3(0.0f, 0.0f, 0.0f);
		m_vPathEnd = Vector3(0.0f, 0.0f, 0.0f);
		m_vReferenceVector = Vector3(0.0f, 0.0f, 0.0f);
		m_vCoverOrigin = Vector3(0.0f, 0.0f, 0.0f);
		m_vPolySearchDir = Vector3(0.0f, 0.0f, 0.0f);
		m_fReferenceDistance = 0.0f;
		m_iFlags = 0;
		m_fEntityRadius = PATHSERVER_PED_RADIUS;
		m_fCompletionRadius = 0.0f;
		m_iNumInfluenceSpheres = 0;
		m_pPed = NULL;
#ifndef NAVGEN_TOOL
		m_pPositionEntity = NULL;
#endif
		m_pEntityPathIsOn = NULL;
		m_bIgnoreTypeVehicles = false;
		m_bIgnoreTypeObjects = false;
		m_iNumIncludeObjects = 0;
		m_iNumExcludeObjects = 0;
		m_iFreeSpaceReferenceValue = 0;
		m_iFreeSpaceHardLimitValue = 0;
		m_fMaxSlopeNavigable = 0.0f;
		m_fClampMaxSearchDistance = 0.0f;
		m_fMaxDistanceToAdjustPathStart = ms_fDefaultMaxDistanceToAdjustPathEndPoints;
		m_fMaxDistanceToAdjustPathEnd = ms_fDefaultMaxDistanceToAdjustPathEndPoints;
		m_fDistAheadOfPed = 0.0f;
		m_StartNavmeshAndPoly.m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		m_StartNavmeshAndPoly.m_iPolyIndex = NAVMESH_POLY_INDEX_NONE;
		m_NavDomain = kNavDomainRegular;
	}
	Vector3 m_vPathStart;
	Vector3 m_vPathEnd;
	Vector3 m_vReferenceVector;
	Vector3 m_vCoverOrigin;
	Vector3 m_vPolySearchDir;
	TNavMeshAndPoly m_StartNavmeshAndPoly;
	float m_fEntityRadius;
	float m_fReferenceDistance;
	u64 m_iFlags;
	float m_fCompletionRadius;
	u32 m_iDynamicObjectFilter;
	s32 m_iNumInfluenceSpheres;
	TInfluenceSphere m_InfluenceSpheres[MAX_NUM_INFLUENCE_SPHERES];
	fwEntity* m_pPed;
#ifndef NAVGEN_TOOL
	fwRegdRef<const fwEntity> m_pPositionEntity;
#endif
	const fwEntity* m_pEntityPathIsOn;
	bool m_bIgnoreTypeVehicles;
	bool m_bIgnoreTypeObjects;
	s32 m_iNumIncludeObjects;
	s32 m_iNumExcludeObjects;
	const fwEntity* m_IncludeObjects[PATHREQUEST_MAX_INCLUDE_OBJECTS];
	const fwEntity* m_ExcludeObjects[PATHREQUEST_MAX_EXCLUDE_OBJECTS];
	s32 m_iFreeSpaceReferenceValue;
	s32 m_iFreeSpaceHardLimitValue;
	float m_fMaxSlopeNavigable;
	float m_fClampMaxSearchDistance;
	float m_fMaxDistanceToAdjustPathStart;
	float m_fMaxDistanceToAdjustPathEnd;
	float m_fDistAheadOfPed;
	aiNavDomain	m_NavDomain;
};


#define MAX_NUM_PATH_REQUESTS				32
#define MAX_NUM_GRID_REQUESTS				8
#define MAX_NUM_LOS_REQUESTS				24
#define MAX_NUM_AUDIO_REQUESTS				16
#define MAX_NUM_FLOODFILL_REQUESTS			8
#define MAX_NUM_CLEARAREA_REQUESTS			4
#define MAX_NUM_CLOSESTPOSITION_REQUESTS	4

//************************************************************************************
//	CClippedFragmentPoly is used when clipping polygons against TDynamicObjects.
//	JB (6/11/06) - this is currently not used in-game, it would likely be far to
//	expensive to clip every poly to every object w/o some a complex cacheing system.
//************************************************************************************

#define CLIPPOLY_MAX_NUM_VERTS							32
#define CLIPPOLY_MAX_NUM_IN_POOL						128
#define CLIPPOLY_MAX_INTERSECTING_OBJECTS				16

class CClippedFragmentPoly
{
public:
	inline CClippedFragmentPoly(void) { iNumVerts = 0; }

	Vector3 m_Verts[CLIPPOLY_MAX_NUM_VERTS];
	TAdjPoly m_Adjacencies[CLIPPOLY_MAX_NUM_VERTS];
	s32 iNumVerts;

	static inline CClippedFragmentPoly * AllocNew(void) { return rage_new CClippedFragmentPoly; }
	static inline void Release(CClippedFragmentPoly * pFrag) { if(pFrag) delete pFrag; }
};

//****************************************************************************************

// Stack size for the thread
// Needs to be reasonably large due to recursive nature of some algorithms
#define PATH_SERVER_THREAD_STACKSIZE	32768

struct TPathServerThreadFuncParams
{
	CPathServer		*m_pPathServer;
	CPathServerThread	*m_pPathServerThread;
};

// All the variables used by the main pathfinding code are to be packaged up in this struct.
// Hopefully this will make the actual ProcessPathRequest() code easier to implement on SPUs.
// (Currently variables are randomly accessed throughout CPathServer & CPathServerThread).
struct TPathFindVars
{
	void Init();
	bool InsertPoly(int iPos, const Vector3& vPoint, TNavMeshPoly* pPoly, const TNavMeshWaypointFlag& Flags);

	// The extents of the search
	TShortMinMax m_PathSearchDistanceMinMax;	// the TShortMinMax integer bounds
	Vector3 m_vSearchExtentsMin;				// min/max of search area
	Vector3 m_vSearchExtentsMax;		

	// This array holds the closest point in each poly to the target (as calculated by the A* algorithm)
	// During the length minimisation step, these will be moved to make the path shorter
	Vector3 m_vClosestPointInPathPolys[MAX_PATH_POLYS+2];
	Vector3 m_vUnadjustedPathStart;
	Vector3 m_vUnadjustedPathEnd;
	Vector3 m_vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 g_vClosestPtsInPoly[NUM_CLOSE_PTS_IN_POLY];
	Vector3 m_vLastClosestPoint;
	Vector3 m_vDirFromPrevious;
	Vector3 m_vVecFromLastPointToCurrentPoint;
	Vector3 m_vWanderOrigin;
	Vector3 m_vWanderPlaneNormal;
	Vector3 m_vWanderPlaneRightNormal;

	TNavMeshPoly * m_pStartPoly;
	TNavMeshPoly * m_pEndPoly;

	// If this path is underwater - then we may end either on a any linked surface/seabed poly as well
	TNavMeshPoly * m_pUnderwaterSecondEndPoly;

	// This array holds the polys in the polygon path as found by the FindPath() function
	TNavMeshPoly * m_PathPolys[MAX_PATH_POLYS+4];
	// These flags indicate special ways in which each poly was reached (climb-ups/drop-downs/etc)
	TNavMeshWaypointFlag m_iPolyWaypointFlags[MAX_PATH_POLYS+4];

	TShortMinMax m_MultipleNavMeshPolysMinMax;

	// The basic LOS flags which are passed in to TestNavMeshLos() whilst doing the pathsearch
	u32 m_iLosFlags;
	// The number of polygons visited in FindPath()
	u32 m_iNumVisitedPolys;
	// This is the number of polys in the path which we found in the FindPath() function
	u32 m_iNumPathPolys;

	bool m_bAdjPolyPointsCalculated;
	bool m_bConsiderDistanceFromWanderPlanes;
	bool m_bHitDynamicObjectWhilstConsideringPathPoly;
	// This is the final flag used by the pathfinder to determine whether a door must be opened
	bool m_bMustOpenDoor;
	float m_fSurfaceNotPavementPenalty;
	float m_fWanderPlaneDist;
	float m_fInitialWanderPlaneDist;
	float m_fWanderTurnPenalty;
	float m_fWander180TurnMultiplier;
	float m_fPathDistanceSoFar;
	float m_fDistanceFromLastPointToChosenPoint;
	float m_fMinPathFindDistFromOrigin;
	float m_fMaxPathFindDistFromOrigin;
	float m_fWanderPlaneRightDist;
	float m_fMinSlopeDotProductWithUpVector;
	float m_fInvRefDist;
	float m_fDistToTargetSqr;
	float m_fParentDistanceTravelled;
};

//----------------------------------------------------------------------------------------
// TVisitPolyStruct
// This structure avoids passing in too many parameters to the poly-visiting functions
// It is called when exploring adjacent polygons during A* search.

struct TVisitPolyStruct
{
	// The navmesh & polygon which we are moving from
	CNavMesh * m_pFromNavMesh;
	TNavMeshPoly * m_pFromPoly;
	// The navmesh & polygon which we are considering moving into
	CNavMesh * m_pToNavMesh;
	TNavMeshPoly * m_pToPoly;
	// The vertices of the polygon which we are moving into
	Vector3 * m_pToPolyPoints;
	// The adjacent polygons of the poly which we are moving into, or NULL
//	TAdjPoly * m_pToAdjacentPolys;
	// The previous position which was chosen in the from poly
	Vector3 m_vPointInFromPoly;
	// The target position for the whole pathsearch
	Vector3 m_vTargetPos;
	// The edge of m_pFromPoly which we are leaving
	s32 m_iAdjacencyIndex;

	enum
	{
		FLAG_CONSIDER_ONLY_CENTROID_FOR_FRAGMENTS	=	0x01,
		FLAG_CONSIDER_ONLY_VERTICES_AND_EDGES		=	0x02,
		FLAG_CONSIDER_MAXIMUM_NUM_VERTICES			=	0x04,
		FLAG_ABORT_IF_HIT_ANY_OBJECT				=	0x08,
		FLAG_PERFORM_DYNAMIC_OBJECT_LOS				=	0x10,
		FLAG_PERFORM_DYNAMIC_OBJECT_LOS_FURTHER		=	0x20,
	};
};


#ifdef GTA_ENGINE
typedef sysIpcThreadId TPathserverThreadId;
#else
typedef DWORD TPathserverThreadId;
#endif

class fwPathServerThread
{
public:
	fwPathServerThread();
	virtual ~fwPathServerThread() { }

	inline TPathserverThreadId GetThreadID() { return m_iThreadID; }

protected:

	TPathserverThreadId m_iThreadID;

	CPathServerRequestBase * m_pCurrentActiveRequest;

	bool m_bForcePerformThreadHousekeeping;
};

}	// namespace rage

#endif	// AI_NAVMESH_PATHSERVERTHREAD_H

// End of file ai/navmesh/pathserverthread.h
