//
// ai/navmesh/shortminmax.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//


#ifndef AI_NAVMESH_SHORTMINMAX_H
#define AI_NAVMESH_SHORTMINMAX_H

#include "vector/vector3.h"
#include "ai/navmesh/navmeshoptimisations.h"

#define __CATCH_SETTING_INVALID_MINMAX	NAVMESH_OPTIMISATIONS_OFF

#ifndef COMPILE_TOOL_MESH
namespace rage
{
#endif

//--------------------------------------------------------------------------------
//	TShortMinMax - stores min/max XYZ extents in 13:2 fixed-point.  In a s16 we
//	can accommodate a range of -8191 to +8191, with 2 fractional bits -
//	i.e. a resolution of 0.25f.  This is pretty good for quickly testing navmesh
//	objects/polygons/etc.

#define MINMAX_RESOLUTION					0.25f
#define MINMAX_INV_RESOLUTION				4.0f
#define MINMAX_HALF_RESOLUTION				0.125f

#define MINMAX_FIXEDPT_FROM_FLOAT(n)		(s16)(((float)n) * MINMAX_INV_RESOLUTION)
#define MINMAX_FIXEDPT_TO_FLOAT(n)			(float)(((float)n) * MINMAX_RESOLUTION)
#define MINMAX_FIXEDPT_TO_FLOAT_DIV2(n)		(float)(((float)n) * MINMAX_HALF_RESOLUTION)
#define MINMAX_FIXEDPT_TO_INT(n)			(n >> 2)
#define MINMAX_INT_TO_FIXEDPT(n)			(n << 2)
#define MINMAX_MAX_FLOAT_VAL				8191.0f
#define MINMAX_ONE							4

struct TShortMinMax
{
	s16 m_iMinX;
	s16 m_iMaxX;
	s16 m_iMinY;
	s16 m_iMaxY;
	s16 m_iMinZ;
	s16 m_iMaxZ;

	static const float ms_fResolution;
	static const float ms_fMaxRepresentableValue;
	static const Vector3 ms_vExtendMinMax;

	TShortMinMax() { }
	inline void SetInt(const s32 iMinX, const s32 iMinY, const s32 iMaxX, const s32 iMaxY, const s32 iMinZ, const s32 iMaxZ)
	{
#if __CATCH_SETTING_INVALID_MINMAX
		Assert(((float)iMinX) >= -MINMAX_MAX_FLOAT_VAL && ((float)iMaxX) <= MINMAX_MAX_FLOAT_VAL && ((float)iMinY) >= -MINMAX_MAX_FLOAT_VAL && ((float)iMaxY) <= MINMAX_MAX_FLOAT_VAL && ((float)iMinZ) >= -MINMAX_MAX_FLOAT_VAL && ((float)iMaxZ) <= MINMAX_MAX_FLOAT_VAL);
#endif
		m_iMinX = (s16)iMinX;
		m_iMaxX = (s16)iMaxX;
		m_iMinY = (s16)iMinY;
		m_iMaxY = (s16)iMaxY;
		m_iMinZ = (s16)iMinZ;
		m_iMaxZ = (s16)iMaxZ;
	}

	inline void SetFloat(const float fMinX, const float fMinY, const float fMinZ, const float fMaxX, const float fMaxY, const float fMaxZ)
	{
#if __CATCH_SETTING_INVALID_MINMAX
		Assert(fMinX >= -MINMAX_MAX_FLOAT_VAL && fMaxX <= MINMAX_MAX_FLOAT_VAL && fMinY >= -MINMAX_MAX_FLOAT_VAL && fMaxY <= MINMAX_MAX_FLOAT_VAL && fMinZ >= -MINMAX_MAX_FLOAT_VAL && fMaxZ <= MINMAX_MAX_FLOAT_VAL);
#endif
		m_iMinX = MINMAX_FIXEDPT_FROM_FLOAT(fMinX);
		m_iMinY = MINMAX_FIXEDPT_FROM_FLOAT(fMinY);
		m_iMinZ = MINMAX_FIXEDPT_FROM_FLOAT(fMinZ);
		m_iMaxX = MINMAX_FIXEDPT_FROM_FLOAT(fMaxX);
		m_iMaxY = MINMAX_FIXEDPT_FROM_FLOAT(fMaxY);
		m_iMaxZ = MINMAX_FIXEDPT_FROM_FLOAT(fMaxZ);
	}
	inline void Set(const Vector3 & vMin, const Vector3 & vMax)
	{
		SetFloat(vMin.x, vMin.y, vMin.z, vMax.x, vMax.y, vMax.z);
	}
	inline void SetInvalid()
	{
		m_iMinX = m_iMinY = m_iMinZ = 32000;
		m_iMaxX = m_iMaxY = m_iMaxZ = -32000;
	}
	inline void Union(const TShortMinMax & minMax)
	{
		if(minMax.m_iMinX < m_iMinX) m_iMinX = minMax.m_iMinX;
		if(minMax.m_iMinY < m_iMinY) m_iMinY = minMax.m_iMinY;
		if(minMax.m_iMinZ < m_iMinZ) m_iMinZ = minMax.m_iMinZ;
		if(minMax.m_iMaxX > m_iMaxX) m_iMaxX = minMax.m_iMaxX;
		if(minMax.m_iMaxY > m_iMaxY) m_iMaxY = minMax.m_iMaxY;
		if(minMax.m_iMaxZ > m_iMaxZ) m_iMaxZ = minMax.m_iMaxZ;
	}
	inline void Union(const TShortMinMax & minMax1, const TShortMinMax & minMax2)
	{
		m_iMinX = (minMax1.m_iMinX < minMax2.m_iMinX) ? minMax1.m_iMinX : minMax2.m_iMinX;
		m_iMinY = (minMax1.m_iMinY < minMax2.m_iMinY) ? minMax1.m_iMinY : minMax2.m_iMinY;
		m_iMinZ = (minMax1.m_iMinZ < minMax2.m_iMinZ) ? minMax1.m_iMinZ : minMax2.m_iMinZ;
		m_iMaxX = (minMax1.m_iMaxX > minMax2.m_iMaxX) ? minMax1.m_iMaxX : minMax2.m_iMaxX;
		m_iMaxY = (minMax1.m_iMaxY > minMax2.m_iMaxY) ? minMax1.m_iMaxY : minMax2.m_iMaxY;
		m_iMaxZ = (minMax1.m_iMaxZ > minMax2.m_iMaxZ) ? minMax1.m_iMaxZ : minMax2.m_iMaxZ;
	}

	inline bool Intersects(const TShortMinMax & minMax) const
	{
		if(m_iMinX >= minMax.m_iMaxX || m_iMinY >= minMax.m_iMaxY || m_iMinZ >= minMax.m_iMaxZ || minMax.m_iMinX >= m_iMaxX || minMax.m_iMinY >= m_iMaxY || minMax.m_iMinZ >= m_iMaxZ)
			return false;
		return true;
	}
	inline bool Intersects(const s32 iMinX, const s32 iMinY, const s32 iMaxX, const s32 iMaxY, const s32 iMinZ, const s32 iMaxZ) const
	{
		if(m_iMinX >= iMaxX || m_iMinY >= iMaxY || m_iMinZ >= iMaxZ || iMinX >= m_iMaxX || iMinY >= m_iMaxY || iMinZ >= m_iMaxZ)
			return false;
		return true;
	}
	inline bool IntersectsXY(const TShortMinMax & minMax) const
	{
		if(m_iMinX >= minMax.m_iMaxX || m_iMinY >= minMax.m_iMaxY || minMax.m_iMinX >= m_iMaxX || minMax.m_iMinY >= m_iMaxY)
			return false;
		return true;
	}
	inline bool IntersectsXY(const s32 iMinX, const s32 iMinY, const s32 iMaxX, const s32 iMaxY) const
	{
		if(m_iMinX >= iMaxX || m_iMinY >= iMaxY || iMinX >= m_iMaxX || iMinY >= m_iMaxY)
			return false;
		return true;
	}
	inline bool LiesWithin(const s32 iX, const s32 iY, const s32 iZ) const
	{
		if(iX < m_iMinX || iY < m_iMinY || iZ < m_iMinZ || iX >= m_iMaxX || iY >= m_iMaxY || iZ >= m_iMaxZ)
			return false;
		return true;
	}
	inline void Expand(const s16 iAmt)
	{
		m_iMinX -= iAmt;
		m_iMinY -= iAmt;
		m_iMinZ -= iAmt;
		m_iMaxX += iAmt;
		m_iMaxY += iAmt;
		m_iMaxZ += iAmt;
	}
	inline void Expand(const s16 iAmtX, const s16 iAmtY, const s16 iAmtZ)
	{
		m_iMinX -= iAmtX;
		m_iMinY -= iAmtY;
		m_iMinZ -= iAmtZ;
		m_iMaxX += iAmtX;
		m_iMaxY += iAmtY;
		m_iMaxZ += iAmtZ;
	}
	// This gets the centre of the min/max bounding box
	inline void GetCentre(Vector3 & vCentre) const
	{
		const s32 iMidX = ((s32)(m_iMinX + m_iMaxX)) >> 1;
		const s32 iMidY = ((s32)(m_iMinY + m_iMaxY)) >> 1;
		const s32 iMidZ = ((s32)(m_iMinZ + m_iMaxZ)) >> 1;

		vCentre.x = MINMAX_FIXEDPT_TO_FLOAT(iMidX);
		vCentre.y = MINMAX_FIXEDPT_TO_FLOAT(iMidY);
		vCentre.z = MINMAX_FIXEDPT_TO_FLOAT(iMidZ);
	}

	// This gets a point offset from the mins of the bounding box.
	// It is used to get the centre-point of navmesh polys, which internally store
	// fixed-point offsets from the min (x,y) to their centroids
	inline void GetPointOffsetFromMins(Vector3 & vCentre, const s16 iOffsetX, const s16 iOffsetY) const
	{
		const s32 iMidZ = ((s32)(m_iMinZ + m_iMaxZ)) >> 1;

		vCentre.x = MINMAX_FIXEDPT_TO_FLOAT(m_iMinX + iOffsetX);
		vCentre.y = MINMAX_FIXEDPT_TO_FLOAT(m_iMinY + iOffsetY);
		vCentre.z = MINMAX_FIXEDPT_TO_FLOAT(iMidZ);
	}

	// This function calculates the XY offset in fixedpt from the XY min/max, to the
	// input point.  This needs to each be storable in the bottom 8 bits (ie. 5:3 format)
	// of a byte and put in each navmesh poly.
	inline void CalculateXYOffsetsToPoint(const float fPosX, const float fPosY, u8 & iOffsetX, u8 & iOffsetY)
	{
		const float fMinX = MINMAX_FIXEDPT_TO_FLOAT(m_iMinX);
		const float fMinY = MINMAX_FIXEDPT_TO_FLOAT(m_iMinY);
		const float fDx = fPosX - fMinX;
		const float fDy = fPosY - fMinY;
		const s16 iX = MINMAX_FIXEDPT_FROM_FLOAT(fDx);
		const s16 iY = MINMAX_FIXEDPT_FROM_FLOAT(fDy);

		// Offsets must be positive
		Assert(iX >= 0 && iY >= 0);
		// Offsets must each be storable in a byte (ie. never more than 32.875)
		Assert(iX < 256 && iY < 256);

		iOffsetX = (u8)iX;
		iOffsetY = (u8)iY;
	}

	// Converts the min/max coords to Vector3's.  Not to be used except for debugging, as
	// the whole point of this system is to avoid lots of floating-point compares, etc.
	inline void GetAsFloats(Vector3 & vMins, Vector3 & vMaxs) const
	{
		vMins.x = MINMAX_FIXEDPT_TO_FLOAT(m_iMinX);
		vMins.y = MINMAX_FIXEDPT_TO_FLOAT(m_iMinY);
		vMins.z = MINMAX_FIXEDPT_TO_FLOAT(m_iMinZ);
		vMaxs.x = MINMAX_FIXEDPT_TO_FLOAT(m_iMaxX);
		vMaxs.y = MINMAX_FIXEDPT_TO_FLOAT(m_iMaxY);
		vMaxs.z = MINMAX_FIXEDPT_TO_FLOAT(m_iMaxZ);
	}
	inline float GetAreaApprox() const
	{
		const float fSizeX = MINMAX_FIXEDPT_TO_FLOAT(m_iMaxX - m_iMinX);
		const float fSizeY = MINMAX_FIXEDPT_TO_FLOAT(m_iMaxY - m_iMinY);
		return fSizeX*fSizeY;
	}

	// Returns the mid z-value as a float.  Used in CNavMesh::GetPolyCentroidQuick(), amongst other things.
	inline float GetMidZAsFloat() const
	{
		return MINMAX_FIXEDPT_TO_FLOAT_DIV2( ((s32)m_iMinZ) + ((s32)m_iMaxZ) );
	}
	// Returns the mim z-value as a float.  Used in volume navigation.
	inline float GetMinZAsFloat() const
	{
		return MINMAX_FIXEDPT_TO_FLOAT(m_iMinZ);
	}
	// Returns the max z-value as a float.  Used in volume navigation.
	inline float GetMaxZAsFloat() const
	{
		return MINMAX_FIXEDPT_TO_FLOAT(m_iMaxZ);
	}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(TShortMinMax);
		STRUCT_FIELD(m_iMinX);
		STRUCT_FIELD(m_iMaxX);
		STRUCT_FIELD(m_iMinY);
		STRUCT_FIELD(m_iMaxY);
		STRUCT_FIELD(m_iMinZ);
		STRUCT_FIELD(m_iMaxZ);
		STRUCT_END();
	}
#endif

#if __XENON
	inline void GetMinsVMX(__vector4 vec)
	{
		vec.u[0] = m_iMinX;
		vec.u[1] = m_iMinX;
		vec.u[2] = m_iMinX;
		vec.u[3] = 0;
	}
#endif
};

#ifndef COMPILE_TOOL_MESH
}	// namespace rage
#endif

#endif // AI_NAVMESH_SHORTMINMAX_H


