//
// ai/navmesh/searchbase.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESH_SEARCHBASE_H
#define AI_NAVMESH_SEARCHBASE_H

#include "vector/vector3.h"

namespace rage
{
	class CPathRequest;
	class CNavMesh;
	struct TNavMeshPoly;
	struct TAdjPoly;

	class fwPathSeachBase
	{
	public:

		fwPathSeachBase() { }
		virtual ~fwPathSeachBase() { }
		
		s32 m_iSearchType;
		const CPathRequest * m_pPathRequest;

	public:

		// FUNCTION : CheckForCompletion
		// PURPOSE : Checks whether the path can complete on this polygon
		virtual bool CheckForCompletion(TNavMeshPoly * pCurrentPoly, CNavMesh * pCurrentNavMesh);

		// FUNCTION : CanMoveToAdjacentPoly
		// PURPOSE : Determines whether the path search can more to the adjacent polygon, from the current
		// polygon, using the adjacency specified
		virtual bool CanMoveToAdjacentPoly(
			TNavMeshPoly * pCurrentPoly, CNavMesh * pCurrentNavMesh,
			TNavMeshPoly * pAdjacentPoly, CNavMesh * pAdjacentNavMesh,
			const TAdjPoly & adjacency);

		// FUNCTION : EvaluateMoveToAdjacentPoly
		// PURPOSE : Evaluates the cost of moving into the adjacent polygon, and returns the A* heuristic cost
		virtual float EvaluateMoveToAdjacentPoly(
			TNavMeshPoly * pCurrentPoly, CNavMesh * pCurrentNavMesh,
			TNavMeshPoly * pAdjacentPoly, CNavMesh * pAdjacentNavMesh,
			const TAdjPoly & adjacency, const Vector3 & vPointInAdjacent, const Vector3 & vPointInCurrent);

		// FUNCTION : EnumerateAndScorePointsInAdjacentPoly
		// PURPOSE : Examines points in the adjacent polygon, and returns the index of the point chosen
		virtual int EnumerateAndScorePointsInAdjacentPoly(const CPathRequest * pRequest);

	};
}

#endif // AI_NAVMESH_SEARCHBASE_H

// End of file ai/navmesh/searchbase.h

