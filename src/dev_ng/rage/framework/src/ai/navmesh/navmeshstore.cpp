//
// ai/navmesh/navmeshstore.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "navmeshstore.h"
#include "pathserverbase.h"

using namespace rage;

#define CHECK_POLY_FLAGS			__DEV && 0
#define __SANITY_CHECK_DLC_SWAPS	__DEV && 0
#define __USE_NAME_HASH		USE_HASH_ONLY_FOR_OBJECT_NAMES

NAVMESH_OPTIMISATIONS();

//-----------------------------------------------------------------------------

aiNavMeshStore::aiNavMeshStore(
	aiNavDomain domain,
	const fwNavMeshStoreInterface &storeInterface,
	const char* pModuleName, 
	int moduleTypeIndex,
	int size,
	u32 elementsPerPage,
	bool requiresTempMemory,
	bool canDefragment,
	int rscVersion)
	: aiMeshStore<CNavMesh, aiNavMeshAssetDef>(pModuleName, moduleTypeIndex, size, elementsPerPage, requiresTempMemory, canDefragment, rscVersion)
	, m_Interface(&storeInterface)
	, m_DataSet(domain)
	, m_bDefragmentCopyBlocked(false)
{
	aiMeshStore<CNavMesh, aiNavMeshAssetDef>::m_iMeshIndexNone = NAVMESH_NAVMESH_INDEX_NONE;
}


aiNavMeshStore::~aiNavMeshStore()
{

}

void aiNavMeshStore::Init()
{
	aiMeshStore::Init();

	m_iBuildID = 0;
}


void aiNavMeshStore::Shutdown()
{
	aiMeshStore::Shutdown();
}

#if !__FINAL

const char* aiNavMeshStore::GetName(strLocalIndex iStreamingIndex) const
{
	static char s_szName[256] = "\0";

	// Find the streaming index from the remapping table.
	// Super-inefficient.. this is only done when Asserting, right?
	s32 iNavMesh;

	if(m_pIndexMappingArray)
	{
		iNavMesh = m_iMeshIndexNone;

		for(int i=0; i<m_iMaxMeshIndex; i++)
		{
			if(m_pIndexMappingArray[i]==iStreamingIndex.Get())
			{
				iNavMesh = i;
				break;
			}
		}
	}
	else
	{
		iNavMesh = iStreamingIndex.Get();
	}

	//-------------------------------------------------------------

	if(iNavMesh >= 0)
	{
		if(iNavMesh < m_iMaxMeshIndex)
		{
			// Work out the original filename from the navmesh index.
			s32 iY = iNavMesh / GetNumMeshesInX();
			s32 iX = (iNavMesh - (iY * GetNumMeshesInY()));
			iX *= GetNumSectorsPerMesh();
			iY *= GetNumSectorsPerMesh();

			sprintf( s_szName, "navmesh[%i][%i]", iX, iY );
			return s_szName;
		}
		// Otherwise this is a dynamic navmesh
		else
		{
			int i;
			for(i=0; i<fwPathServer::m_DynamicNavMeshStore.GetNum(); i++)
			{
				const CModelInfoNavMeshRef & ref = fwPathServer::m_DynamicNavMeshStore.Get(i);
				if(ref.m_iStreamingIndex==iStreamingIndex)
				{
					sprintf( s_szName, "%s", ref.m_iNavFileNameHash.TryGetCStr());
					return s_szName;
				}
			}
		}
	}

	return NULL;
}

#endif	// !__FINAL

//****************************************************************************************

void * aiNavMeshStore::GetPtr(strLocalIndex index)
{
	if (index.Get() >= NAVMESH_INDEX_FIRST_DYNAMIC)
	{
		for (int i=0; i<fwPathServer::m_DynamicNavMeshStore.GetNum(); ++i)
		{
			CModelInfoNavMeshRef & ref = fwPathServer::m_DynamicNavMeshStore.Get(i);
			if (ref.m_iStreamingIndex==index)
			{
				return ref.m_pBackUpNavmeshCopy;
			}
		}
		return NULL;	
	}
	else
	{
		return fwAssetRscStore::GetPtr(strLocalIndex(index));
	}
}

strLocalIndex aiNavMeshStore::FindSlot(const char* name) const
{
	Assert(name);
	strStreamingObjectName strName( name );

#if __USE_NAME_HASH
	const strLocalIndex iNavMeshIndex = strLocalIndex(GetNavMeshIndexFromNameHash(strName));
#else
	const strLocalIndex iNavMeshIndex = strLocalIndex(fwPathServer::GetNavMeshIndexFromFilename(m_DataSet, strName.TryGetCStr()));
#endif

	//--------------------------------------------
	// Is this one of the main map navmeshes?

	if(iNavMeshIndex.Get() <= NAVMESH_MAX_MAP_INDEX)
	{		
		const strLocalIndex iStreamingIndex = GetStreamingIndex(iNavMeshIndex);
		return iStreamingIndex;
	}

	//--------------------------------------------------------------------
	// Otherwise this navmesh must be a dynamic navmesh for a vehicle.

	int iNumDynamic = fwPathServer::m_DynamicNavMeshStore.GetNum();
	Assert(iNumDynamic < (int)fwPathServer::m_iMaxDynamicNavmeshTypes);

	// See if we already have a dynamic navmesh with this name:
	for(int n=0; n<iNumDynamic; n++)
	{
		CModelInfoNavMeshRef & pDynNavInf = fwPathServer::m_DynamicNavMeshStore.Get(n);
		if(pDynNavInf.m_iNavFileNameHash && pDynNavInf.m_iNavFileNameHash == strName)
		{
			return strLocalIndex(pDynNavInf.m_iStreamingIndex);
		}
	}

	return strLocalIndex(-1);
}


int CompareNameHash(const aiNavMeshStore::TNameHashMapping * p1, const aiNavMeshStore::TNameHashMapping * p2)
{
	if(p1->m_iNameHash < p2->m_iNameHash)
		return -1;
	else if(p1->m_iNameHash > p2->m_iNameHash)
		return 1;
	else return 0;
}

// PURPOSE: Generates a table mapping the hash of the navmesh asset name, to the streaming index it should be assigned.
// If a navmesh's name hash is not in this table it is assumed to be a dynamic navmesh and is assigned a dynamic index accordingly
void aiNavMeshStore::CreateNameHashTable()
{
#if __USE_NAME_HASH
	Assert(!m_NameHashTable.GetCount());

	m_NameHashTable.clear();

	char filename[256];
	u32 iIndex = 0;

	for(s32 y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(s32 x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			formatf( filename, "navmesh[%i][%i]", x * CPathServerExtents::m_iNumSectorsPerNavMesh, y * CPathServerExtents::m_iNumSectorsPerNavMesh );

			atHashString iNameHash = atStringHash(filename);

			TNameHashMapping item;
			item.m_iNameHash = iNameHash;
			item.m_iNavMeshIndex = iIndex;

			m_NameHashTable.Push(item);

			iIndex++;
		}
	}

	m_NameHashTable.QSort(0, -1, CompareNameHash);
#endif
}

atHashString aiNavMeshStore::GetNavMeshIndexFromNameHash(const atHashString iNameHash) const
{
	int low = 0;
	int high = m_NameHashTable.GetCount()-1;
	while (low <= high) {
		int mid = (low + high) >> 1;
		if (iNameHash == m_NameHashTable[mid].m_iNameHash)
			return m_NameHashTable[mid].m_iNavMeshIndex;
		else if (iNameHash < m_NameHashTable[mid].m_iNameHash)
			high = mid-1;
		else
			low = mid+1;
	}
	return NAVMESH_NAVMESH_INDEX_NONE;
}

strLocalIndex aiNavMeshStore::Register(const char* filename)
{
	Assert(filename);

	strStreamingObjectName name( filename );

#if __USE_NAME_HASH
	const strLocalIndex iNavMeshIndex = strLocalIndex(GetNavMeshIndexFromNameHash(name));
#else
	const strLocalIndex iNavMeshIndex = strLocalIndex(fwPathServer::GetNavMeshIndexFromFilename(m_DataSet, name.TryGetCStr()));
#endif

	//*******************************************
	// Is this one of the main map navmeshes?

	if(iNavMeshIndex.Get() <= NAVMESH_MAX_MAP_INDEX)
	{		
		const strLocalIndex iStreamingIndex = strLocalIndex(GetStreamingIndex(iNavMeshIndex));

#if USE_PAGED_POOLS_FOR_STREAMING
		if (iStreamingIndex != -1)
		{
			AllocateSlot(iStreamingIndex);
			GetSlot(iStreamingIndex)->InitDef();
		}
#endif // USE_PAGED_POOLS_FOR_STREAMING

		if(iStreamingIndex != -1)
			GetSlot(iStreamingIndex)->m_name = name;

		return iStreamingIndex;
	}

	//*******************************************************************************************************
	// Otherwise this navmesh must be a dynamic navmesh for a vehicle.

	strLocalIndex iStreamingIndex;
	int iNumDynamic = fwPathServer::m_DynamicNavMeshStore.GetNum();
	Assert(iNumDynamic < (int)fwPathServer::m_iMaxDynamicNavmeshTypes);

	// See if we already have a dynamic navmesh with this name:
	for(int n=0; n<iNumDynamic; n++)
	{
		CModelInfoNavMeshRef & pDynNavInf = fwPathServer::m_DynamicNavMeshStore.Get(n);
		if(pDynNavInf.m_iNavFileNameHash && pDynNavInf.m_iNavFileNameHash==name)
		{
#if USE_PAGED_POOLS_FOR_STREAMING
			if (pDynNavInf.m_iStreamingIndex != -1)
			{
				AllocateSlot(pDynNavInf.m_iStreamingIndex);
				GetSlot(pDynNavInf.m_iStreamingIndex)->InitDef();
			}
#endif // USE_PAGED_POOLS_FOR_STREAMING

			if(pDynNavInf.m_iStreamingIndex != -1)
				GetSlot(strLocalIndex(pDynNavInf.m_iStreamingIndex))->m_name = name;

			return strLocalIndex(pDynNavInf.m_iStreamingIndex);
		}
	}

	CModelInfoNavMeshRef * pDynNavInf = fwPathServer::m_DynamicNavMeshStore.Add();
	if(pDynNavInf)
	{
		iStreamingIndex = NAVMESH_INDEX_FIRST_DYNAMIC + iNumDynamic;

		pDynNavInf->m_iStreamingIndex = iStreamingIndex;

		// NB : Only storing m_pNavFileName in order to implement the Streaming_GetNameFn() - is this really necessary?
		// - Update: in response to the note above, this is now also used by FindVehicleForDynamicNavMesh(). Could easily
		//   store a hash for that, though (without the extension). /FF
		pDynNavInf->m_iNavFileNameHash = name;

#if USE_PAGED_POOLS_FOR_STREAMING
		if (pDynNavInf->m_iStreamingIndex != -1)
		{
			AllocateSlot(pDynNavInf->m_iStreamingIndex);
			GetSlot(pDynNavInf->m_iStreamingIndex)->InitDef();
		}
#endif // USE_PAGED_POOLS_FOR_STREAMING

		if(pDynNavInf->m_iStreamingIndex != -1)
			GetSlot(strLocalIndex(pDynNavInf->m_iStreamingIndex))->m_name = name;
	}
	else
	{
		Assertf(false, "Too many dynamic navmeshes in navmeshes.img\n");
		return strLocalIndex(-1);
	}

#if USE_PAGED_POOLS_FOR_STREAMING
	AllocateSlot(iStreamingIndex);
	GetSlot(iStreamingIndex)->InitDef();
#endif // USE_PAGED_POOLS_FOR_STREAMING

	return iStreamingIndex;
}


void aiNavMeshStore::Remove(strLocalIndex index)
{
	USE_MEMBUCKET(MEMBUCKET_GAMEPLAY);

	//********************************************************************
	//	Try to enter the critical section which protects the navmesh data.
	//*******************************************************************

	LOCK_NAVMESH_DATA;

	// Wait for access from any "immediate-mode" function to finish
	LOCK_IMMEDIATE_DATA;


	//*********************************************************************************************
	// If the streaming-index is higher than NAVMESH_MAX_MAP_INDEX, then this is a dynamic navmesh

	if(index.Get() >= NAVMESH_INDEX_FIRST_DYNAMIC)
	{
		int i;
		for(i=0; i<fwPathServer::m_DynamicNavMeshStore.GetNum(); i++)
		{
			CModelInfoNavMeshRef & ref = fwPathServer::m_DynamicNavMeshStore.Get(i);
			if(ref.m_iStreamingIndex==index)
			{
				Assertf(ref.m_pBackUpNavmeshCopy, "Streaming_DestroyFn Dynamic NavMesh isn't loaded");

				if(ref.m_pBackUpNavmeshCopy)
				{
					REMOVE_KNOWN_REF(ref.m_pBackUpNavmeshCopy);
					fwAssetRscStore::Remove(strLocalIndex(index));

// #if !RSG_ORBIS
// 					// I think this delete is superfluous, because it's crashing on Orbis because its memory is filled with 0xEEEEEEEE.
// 					// But I don't want to risk breaking something on other platforms right now.  Discussed it briefly with James B
// 					// and he agreed it was probably unnecessary, but neither of us were sure.  We fixed a similar redundant delete
// 					// in related code not to long ago.
// 					delete ref.m_pBackUpNavmeshCopy;
// #endif

					ref.m_pBackUpNavmeshCopy = NULL;

					Assertf(!(ref.m_iNumRefs>0), "NavMesh_Streaming_DestroyFn removed navmesh, but (m_iNumRefs > 0)");
					Assertf(!(ref.m_iNumRefs<0), "NavMesh_Streaming_DestroyFn removed navmesh, but (m_iNumRefs < 0)");
				}
			}
		}
	}
	//********************************************************************
	// Otherwise this is a main-map navmesh
	else
	{
		// Add to loaded navmeshes array
		CNavMesh * pNavMesh = GetMeshByStreamingIndex(index.Get());
		Assert(pNavMesh);

		s32 iArrayIndex = m_LoadedMeshes.Find(pNavMesh->GetIndexOfMesh());
		Assert(iArrayIndex != -1);

		if(iArrayIndex != -1)
		{
			m_LoadedMeshes.Delete(iArrayIndex);
		}

		fwAssetRscStore::Remove(strLocalIndex(index));

		// delete pNavMesh;	//already deleted by Remove(index)
	}

	//*******************************************************************
	//	Leave the critical section which protects the navmesh data
	//*******************************************************************

	fwPathServer::m_bFindCover_NavMeshesHaveChanged = true;

	m_Interface->NavMeshesHaveChanged();
}


void aiNavMeshStore::PlaceResource(strLocalIndex iStreamingIndex, datResourceMap & map, datResourceInfo & header)
{
	USE_MEMBUCKET(MEMBUCKET_GAMEPLAY);

	Assert(iStreamingIndex.Get() < NAVMESH_INDEX_FINAL_DYNAMIC);
	Assert(GetPtr(iStreamingIndex)==NULL);

#if __DEV
	float fTimeToObtainCriticalSection = 0.0f;
	float fTimeToLoadFromMemory = 0.0f;
	float fTimeToUnlockCriticalSection = 0.0f;

	if(CNavMesh::ms_bDisplayTimeTakenToLoadNavMeshes)
	{
		START_PERF_TIMER(fwPathServer::m_NavMeshLoadingTimer);
	}
#endif

	//********************************************************************
	//	Try to enter the critical section which protects the navmesh data.
	//	This may cause a brief stall of either the CStreaming process, or
	//	the pathfinding process..  We must make sure our load function is
	//	optimal.
	//*******************************************************************

	// This call will force any long path-requests to abort quickly.
	// For short paths it will have no effect.

	fwPathServer::ForceAbortCurrentPathRequest();

	LOCK_NAVMESH_DATA;
	LOCK_IMMEDIATE_DATA;

#if __DEV
	if(CNavMesh::ms_bDisplayTimeTakenToLoadNavMeshes)
	{
		STOP_PERF_TIMER(fwPathServer::m_NavMeshLoadingTimer, fTimeToObtainCriticalSection);
		START_PERF_TIMER(fwPathServer::m_NavMeshLoadingTimer);
	}
#endif

	//**************************************************************************************************************
	// If the streaming-index is higher than or equals the max static navmesh index, then this is a dynamic navmesh

	if(iStreamingIndex.Get() >= NAVMESH_INDEX_FIRST_DYNAMIC)
	{
		int i;
		for(i=0; i<fwPathServer::m_DynamicNavMeshStore.GetNum(); i++)
		{
			CModelInfoNavMeshRef & ref = fwPathServer::m_DynamicNavMeshStore.Get(i);
			if(ref.m_iStreamingIndex==iStreamingIndex)
			{
				Assertf(!ref.m_pBackUpNavmeshCopy, "Requested Dynamic NavMesh is already loaded");

				if(!ref.m_pBackUpNavmeshCopy)
				{
					ref.m_pBackUpNavmeshCopy = CNavMesh::StreamingPlaceFn(iStreamingIndex.Get(), map, header);
					
					Assert(ref.m_pBackUpNavmeshCopy);

					if(ref.m_pBackUpNavmeshCopy)
					{
						ADD_KNOWN_REF(ref.m_pBackUpNavmeshCopy);

						CNavMesh * pNavMesh = ref.m_pBackUpNavmeshCopy;
						fwAssetRscStore::Set(strLocalIndex(iStreamingIndex), pNavMesh);

						//CPathServer::m_iTotalMemoryUsedByNavMeshes += pNavMesh->GetTotalMemoryUsed();

						const TNavMeshIndex iIndexOfMesh = NAVMESH_INDEX_FIRST_DYNAMIC + i;
						pNavMesh->SetIndexOfMesh(iIndexOfMesh);

						// We need to correctly set the navmesh index for all the polys in the navmesh
						u32 p;
						for(p=0; p<pNavMesh->GetNumPolys(); p++)
						{
							TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
							poly.SetNavMeshIndex(iIndexOfMesh);
						}
						// We need to make sure that all the adjacencies point to this navmesh too
						u32 a;
						for(a=0; a<pNavMesh->GetSizeOfPools(); a++)
						{
							TAdjPoly & adjPoly = *pNavMesh->GetAdjacentPolysArray().Get(a);
							if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE)
							{
								adjPoly.SetNavMeshIndex(iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
								adjPoly.SetOriginalNavMeshIndex(iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
							}
						}
						// Do other preprocessing on the navmesh & its quadtree.
						// TODO : Preprocess this stuff, or move to pathserver thread?

						pNavMesh->GetNonResourcedData()->m_vMinOfNavMesh = pNavMesh->GetQuadTree()->m_Mins;
						// It might not be safe to NULL this resourced pointer.  Speak to Klass, he knows these things.
						pNavMesh->SetQuadTree(NULL);

						pNavMesh->SetFlags( pNavMesh->GetFlags() | NAVMESH_IS_DYNAMIC );

						#if SANITY_CHECK_TESSELLATION
						for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
						{
							CPathServer::m_PathServerThread.SanityCheckPolyConnections(pNavMesh, pNavMesh->GetPoly(p));
						}
						#endif
					}
				}
				break;
			}
		}
	}

	//*********************************************************************************
	//	Otherwise this is a main-map navmesh

	else
	{
		CNavMesh * pNavMesh = CNavMesh::StreamingPlaceFn(iStreamingIndex.Get(), map, header);
		fwAssetRscStore::Set(strLocalIndex(iStreamingIndex), pNavMesh);

		if(pNavMesh)
		{
			const u32 iNavMesh = pNavMesh->GetIndexOfMesh();
			Assert(m_LoadedMeshes.Find(iNavMesh)==-1);

			// Add to loaded navmeshes array
			if(m_LoadedMeshes.GetCount() >= m_LoadedMeshes.GetCapacity())
			{
				m_LoadedMeshes.Grow() = iNavMesh;

				Displayf("aiNavMeshStore::PlaceResource() - grew m_LoadedMeshes array to %i", m_LoadedMeshes.GetCount());
			}
			else
			{
				m_LoadedMeshes.Append() = iNavMesh;
			}

			// Ensure that the Build ID of this navmesh matches the build ID of the others
			if(m_iBuildID == 0)
				m_iBuildID = pNavMesh->GetBuildID();

			if( !pNavMesh->GetIsDLC() )
			{
				Assertf(pNavMesh->GetBuildID()==m_iBuildID, "BUILD ERROR - assets for navmeshes are mismatched.");
				Assertf(pNavMesh->GetBuildID()==m_iBuildID, "This is likely caused by the build being grabbed with incomplete data, or the asset-builder not converting all the exported ZIPs into platform RPFs.");
			}

			m_Interface->ApplyAreaSwitchesToNewlyLoadedNavMesh(*pNavMesh, m_DataSet);

			// Do other preprocessing on the navmesh & its quadtree.  TODO : Preprocess this stuff, or move to pathserver thread
			pNavMesh->PreProcessNavMeshQuadTree();

			m_Interface->ApplyHacksToNewlyLoadedNavMesh(pNavMesh, m_DataSet);

#if CHECK_POLY_FLAGS
			for(int p=0; p<(int)pNavMesh->GetNumPolys(); p++)
			{
				TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
				Assert(!(poly.GetFlags()&NAVMESHPOLY_DEGENERATE_CONNECTION_POLY));
				Assert(!(poly.GetFlags()&NAVMESHPOLY_TESSELLATED_FRAGMENT));
				Assert(!(poly.GetFlags()&NAVMESHPOLY_REPLACED_BY_TESSELLATION));
			}
			for(int a=0; a<(int)pNavMesh->GetSizeOfPools(); a++)
			{
				const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(a);
				Assert(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) == adjPoly.GetOriginalNavMeshIndex(pNavMesh->GetAdjacentMeshes()));
				Assert(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) <= NAVMESH_MAX_MAP_INDEX || adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())==NAVMESH_NAVMESH_INDEX_NONE);
				Assert(adjPoly.GetOriginalNavMeshIndex(pNavMesh->GetAdjacentMeshes()) <= NAVMESH_MAX_MAP_INDEX || adjPoly.GetOriginalNavMeshIndex(pNavMesh->GetAdjacentMeshes())==NAVMESH_NAVMESH_INDEX_NONE);
			}
#endif // CHECK_POLY_FLAGS

#if SANITY_CHECK_TESSELLATION
			for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
			{
				CPathServer::m_PathServerThread.SanityCheckPolyConnections(pNavMesh, pNavMesh->GetPoly(p));
			}
#endif

#if __ENSURE_THAT_POLY_QUICK_CENTROIDS_ARE_REALLY_WITHIN_POLYS
			pNavMesh->CheckAllQuickPolyCentroidsAreWithinPolys();
#endif

			//********************************************************************
			// If this navmesh borders any which are DLC, or vica versa, then we
			// must perform some operation to ensure that the edge adjacencies
			// are retargetted appropriately.

#if __BANK
			sysPerformanceTimer dlcStitchTimer("dlcStitchTimer");
			dlcStitchTimer.Start();
#endif
			s32 iAdjacenciesStitched = 0;
			s32 iAdjacenciesRestored = 0;

			s32 iMeshX = 0, iMeshY = 0;			
			fwPathServer::GetSectorFromNavMeshIndex(pNavMesh->GetIndexOfMesh(), iMeshX, iMeshY, GetDataSet());

			CNavMesh * pAdjacentMeshes[4] =
			{
				fwPathServer::GetNavMeshFromIndex( fwPathServer::GetNavMeshIndexFromSector(iMeshX - CPathServerExtents::GetNumSectorsPerNavMesh(), iMeshY, GetDataSet()), GetDataSet()),
				fwPathServer::GetNavMeshFromIndex( fwPathServer::GetNavMeshIndexFromSector(iMeshX + CPathServerExtents::GetNumSectorsPerNavMesh(), iMeshY, GetDataSet()), GetDataSet()),
				fwPathServer::GetNavMeshFromIndex( fwPathServer::GetNavMeshIndexFromSector(iMeshX, iMeshY - CPathServerExtents::GetNumSectorsPerNavMesh(), GetDataSet()), GetDataSet()),
				fwPathServer::GetNavMeshFromIndex( fwPathServer::GetNavMeshIndexFromSector(iMeshX, iMeshY + CPathServerExtents::GetNumSectorsPerNavMesh(), GetDataSet()), GetDataSet())
			};
			const eNavMeshEdge iAdjSides[4] = { eNegX, ePosX, eNegY, ePosY };

			for(s32 a=0; a<4; a++)
			{
				if(pAdjacentMeshes[a])
				{
					const s32 iAdjacentMeshDLCGroup = pAdjacentMeshes[a]->GetDLCGroup();
					const s32 iNewMeshDLCGroup = pNavMesh->GetDLCGroup();

					const s32 iAdjacentToNewDLCGroup = pAdjacentMeshes[a]->GetAdjoinedDLCGroup( GetOppositeEdge(iAdjSides[a]) );

					//------------------------------------------------------------------------------------------------------------------

					//Assertf(iAdjacentMeshSwapGroup == -1 || iNewMeshSwapGroup == -1, "We cannot yet support an active mapswap bordering another active mapswap (it must border the main map)" );

					// Handle the general case where a new navmesh is loaded which is from a different swap group wrt a neighbour
					if(iAdjacentMeshDLCGroup != iNewMeshDLCGroup)
					{					
						m_Interface->AdjoinNavmeshesAcrossMapSwap(
							(iNewMeshDLCGroup!=-1) ? pAdjacentMeshes[a] : pNavMesh,
							(iNewMeshDLCGroup!=-1) ? pNavMesh : pAdjacentMeshes[a],
							(iNewMeshDLCGroup!=-1) ? GetOppositeEdge(iAdjSides[a]) : iAdjSides[a] );

						iAdjacenciesStitched++;
					}

					// Handle the case where the two swap groups are the main map, but our edge adjacencies are not resolved to that swap group.
					// (This could happen if player walks away from a swap, which is then deactivated the player returns)
					else
					{
						//Assertf(iAdjacentMeshSwapGroup == -1 && iNewMeshSwapGroup == -1, "Was expecting both swap groups to be the main map");

						if( iAdjacentToNewDLCGroup != iNewMeshDLCGroup && iNewMeshDLCGroup == -1)
						{
							m_Interface->RestoreNavmeshesAcrossMapSwap( pAdjacentMeshes[a], pNavMesh, GetOppositeEdge(iAdjSides[a]) );

							iAdjacenciesRestored++;
						}
						else
						{
							Assertf( iAdjacentToNewDLCGroup==-1, "Was not expecting an adjacency between same DLC group to be initialised to anything");
						}
					}
				}
			}

#if __SANITY_CHECK_DLC_SWAPS
			// Sanity check
			for(s32 n=0; n<NAVMESH_MAX_MAP_INDEX; n++)
			{
				CNavMesh * pTestNavMesh = fwPathServer::GetNavMeshFromIndex(n, m_DataSet);
				if(pTestNavMesh)
				{
					for(u32 a=0; a<pTestNavMesh->GetSizeOfPools(); a++)
					{
						const TAdjPoly & adjPoly = pTestNavMesh->GetAdjacentPoly(a);
						TNavMeshIndex adjNavIndex = adjPoly.GetOriginalNavMeshIndex(pTestNavMesh->GetAdjacentMeshes());
						if(adjNavIndex != NAVMESH_NAVMESH_INDEX_NONE)
						{
							CNavMesh * pAdjTestNavMesh = fwPathServer::GetNavMeshFromIndex(adjNavIndex, m_DataSet);
							if(pAdjTestNavMesh)
							{
								Assertf(adjPoly.GetOriginalPolyIndex() < pAdjTestNavMesh->GetNumPolys(), "Sanity Fail : poly index out of range");
								FastAssert(adjPoly.GetOriginalPolyIndex() < pAdjTestNavMesh->GetNumPolys());
							}
						}
					}
				}
			}
#endif

#if __BANK
			dlcStitchTimer.Stop();

			if(iAdjacenciesStitched)
			{
				Displayf("NAVMESH DLC [frame %i] : Placing navmesh %s; stitching %i DLC adjacencies took %.3f ms\n", fwTimer::GetFrameCount(), GetName(iStreamingIndex), iAdjacenciesStitched, dlcStitchTimer.GetElapsedTimeMS());
			}
			if(iAdjacenciesRestored)
			{
				Displayf("NAVMESH DLC [frame %i] : Placing navmesh %s; restoring %i DLC adjacencies took %.3f ms\n", fwTimer::GetFrameCount(), GetName(iStreamingIndex), iAdjacenciesRestored, dlcStitchTimer.GetElapsedTimeMS());
			}
#endif
		}
	}

	//*******************************************************************
	//	Leave the critical section which protects the navmesh data
	//*******************************************************************

	fwPathServer::m_bFindCover_NavMeshesHaveChanged = true;

	m_Interface->NavMeshesHaveChanged();

#if __DEV
	if(CNavMesh::ms_bDisplayTimeTakenToLoadNavMeshes)
	{
		STOP_PERF_TIMER(fwPathServer::m_NavMeshLoadingTimer, fTimeToLoadFromMemory);
		START_PERF_TIMER(fwPathServer::m_NavMeshLoadingTimer);
	}
#endif

#if __DEV
	if(CNavMesh::ms_bDisplayTimeTakenToLoadNavMeshes)
	{
		STOP_PERF_TIMER(fwPathServer::m_NavMeshLoadingTimer, fTimeToUnlockCriticalSection);

		Displayf("Time taken to OBTAIN the critical section was %.4f ms\n", fTimeToObtainCriticalSection);
		Displayf("Time taken to UNLOCK the critical section was %.4f ms\n", fTimeToLoadFromMemory);
		Displayf("Time taken to load from memory was %.4f\n", fTimeToUnlockCriticalSection);

		float fTotalTime = fTimeToObtainCriticalSection + fTimeToLoadFromMemory + fTimeToUnlockCriticalSection;

		Displayf("In total CNavMesh::LoadFromMemory() took : %.4f\n", fTotalTime);
		Displayf("************************************************************\n");
	}
#endif
}


void aiNavMeshStore::InitDynamicNavMeshesAfterVehSetup()
{
	for(int i = 0; i < fwPathServer::m_DynamicNavMeshStore.GetNum(); i++)
	{
		CModelInfoNavMeshRef& ref = fwPathServer::m_DynamicNavMeshStore.Get(i);
		FindVehicleForDynamicNavMesh(ref);
	}
}


void aiNavMeshStore::FindVehicleForDynamicNavMesh(CModelInfoNavMeshRef &dynNavInf) const
{
	m_Interface->FindVehicleForDynamicNavMesh(dynNavInf.m_iNavFileNameHash, dynNavInf);
}

void aiNavMeshStore::SetDefragmentCopyBlocked(const bool b)
{
	m_bDefragmentCopyBlocked = b;
}

bool aiNavMeshStore::IsDefragmentCopyBlocked() const
{
	if(m_Interface->IsDefragmentCopyBlocked())
		return true;

	return m_bDefragmentCopyBlocked;
}

//-----------------------------------------------------------------------------



CDynamicNavMeshStore::CDynamicNavMeshStore()
{
	m_iMaxDynamicNavmeshTypes = 0;
	m_iNumNavMeshes = 0;
	m_NavMeshes = NULL;
}
CDynamicNavMeshStore::~CDynamicNavMeshStore()
{

}
void CDynamicNavMeshStore::Init(const int iMaxDynamicNavmeshTypes)
{
	m_iMaxDynamicNavmeshTypes = iMaxDynamicNavmeshTypes;
	Assert(m_iMaxDynamicNavmeshTypes >= 0);
	if(m_iMaxDynamicNavmeshTypes > 0)
	{
		m_NavMeshes = rage_new CModelInfoNavMeshRef[m_iMaxDynamicNavmeshTypes];
	}
	m_iNumNavMeshes = 0;
}
void CDynamicNavMeshStore::Shutdown()
{
	for(int i=0; i<m_iNumNavMeshes; i++)
	{
		m_NavMeshes[i].Shutdown();
	}
	m_iNumNavMeshes = 0;
}
CModelInfoNavMeshRef * CDynamicNavMeshStore::Add()
{
	if(m_iNumNavMeshes < m_iMaxDynamicNavmeshTypes)
		return &m_NavMeshes[m_iNumNavMeshes++];
	else return NULL;
}
const CModelInfoNavMeshRef * CDynamicNavMeshStore::GetByModelIndex(u32 mi) const
{
	for(int n=0; n<m_iNumNavMeshes; n++)
	{
		if(m_NavMeshes[n].m_iModelIndex==strLocalIndex(mi))
			return &m_NavMeshes[n];
	}
	return NULL;
}




//-----------------------------------------------------------------------------


#if __HIERARCHICAL_NODES_ENABLED

aiNavNodesStore::aiNavNodesStore(const fwNavMeshStoreInterface& storeInterface,
	const char* pModuleName, 
	int moduleTypeIndex,
	int size,
	bool requiresTempMemory,
	bool canDefragment,
	int rscVersion)
	: aiMeshStore<CHierarchicalNavData, aiNavNodesAssetDef>(pModuleName, moduleTypeIndex, size, requiresTempMemory, canDefragment, rscVersion)
	, m_Interface(&storeInterface)
{
	aiMeshStore<CHierarchicalNavData, aiNavNodesAssetDef>::m_iMeshIndexNone = NAVMESH_NODE_INDEX_NONE;
}
aiNavNodesStore::~aiNavNodesStore()
{

}

void aiNavNodesStore::Init()
{
	aiMeshStore::Init();
}

void aiNavNodesStore::Shutdown()
{
	aiMeshStore::Shutdown();
}
void * aiNavNodesStore::GetPtr(int index)
{
	return fwAssetStore::GetPtr(index);
}

#if !__FINAL
const char* aiNavNodesStore::GetName(strLocalIndex iStreamingIndex) const
{
	Assert(iStreamingIndex.Get() <= (s32)m_iMaxMeshIndex);
	Assert(m_pIndexMappingArray);
	static char s_szName[64] = "\0";

	// Find the streaming index from the remapping table.
	// Super-inefficient.. this is only done when Asserting, right?
	s32 iNavNodes = m_iMeshIndexNone;
	for(int i=0; i<m_iMaxMeshIndex; i++)
	{
		if(m_pIndexMappingArray[i]==iStreamingIndex)
		{
			iNavNodes = i;
			break;
		}
	}
	if(iNavNodes != m_iMaxMeshIndex)
	{
		// Work out the original filename from the navmesh index.
		s32 iY = iNavNodes / GetNumMeshesInX();
		s32 iX = (iNavNodes - (iY * GetNumMeshesInY()));
		iX *= GetNumSectorsPerMesh();
		iY *= GetNumSectorsPerMesh();

		sprintf( s_szName, "%i_%i", iX, iY );
		return s_szName;
	}
	return NULL;
}
#endif

strLocalIndex aiNavNodesStore::Register(const char* name)
{
	Assert(name);
	s32 iSectorX, iSectorY;
	sscanf(name, "%i_%i", &iSectorX, &iSectorY);
	//const TNavMeshIndex iNavMeshIndex = CPathServer::GetNavMeshIndexFromSector(iSectorX, iSectorY);
	const s32 iNodesIndex = GetMeshIndexFromSectorCoords(iSectorX, iSectorY);
	if(iNodesIndex != m_iMeshIndexNone)
	{
		const strLocalIndex iStreamingIndex = GetStreamingIndex(iNodesIndex);
		Assertf(iStreamingIndex!=-1, "Couldn't find streaming index for navnodes.  The index remapping is probably out of sync with the navmeshes/navnodes.");

		// Is this streaming index one of the main map navmeshes, which has a one-to-one relationship with the navmesh index
		if(iStreamingIndex >= 0 && iStreamingIndex <= GetNumMeshesInAnyLevel())
		{
#if USE_PAGED_POOLS_FOR_STREAMING
			AllocateSlot(iStreamingIndex);
			GetSlot(iStreamingIndex)->InitDef();
#endif // USE_PAGED_POOLS_FOR_STREAMING
			return (s32)iStreamingIndex;
		}
	}
	Assert(false);
	return -1;
}

void aiNavNodesStore::Remove(int index)
{
	USE_MEMBUCKET(MEMBUCKET_GAMEPLAY);
	Assert(index <= GetNumMeshesInAnyLevel());

	//********************************************************************
	//	Try to enter the critical section which protects the navmesh data.
	//*******************************************************************

	LOCK_NAVMESH_DATA;

	// Wait for access from any "immediate-mode" function to finish
	LOCK_IMMEDIATE_DATA;


	fwAssetStore::Remove(index);

	//*******************************************************************
	//	Leave the critical section which protects the navmesh data
	//*******************************************************************

	fwPathServer::m_bFindCover_NavMeshesHaveChanged = true;
	m_Interface->NavMeshesHaveChanged();
}
void aiNavNodesStore::PlaceResource(s32 iStreamingIndex, datResourceMap & map, datResourceInfo & header)
{
	USE_MEMBUCKET(MEMBUCKET_GAMEPLAY);

	Assert(iStreamingIndex < (s32)GetNumMeshesInAnyLevel());
	ASSERT_ONLY( CHierarchicalNavData * pNavData = GetMeshByStreamingIndex(iStreamingIndex); )
	Assert(!pNavData);

	//**************************
	// Synchronisation issues..

	fwPathServer::ForceAbortCurrentPathRequest();
	LOCK_NAVMESH_DATA;
	LOCK_IMMEDIATE_DATA;

	CHierarchicalNavData * pNodes = NULL;

	pNodes = CHierarchicalNavData::StreamingPlaceFn(iStreamingIndex, map, header);

	Set(iStreamingIndex, pNodes);

	m_Interface->ApplyAreaSwitchesToNewlyLoadedNavNodes(*pNodes);
}

#endif	// __HIERARCHICAL_NODES_ENABLED

//-----------------------------------------------------------------------------

// End of file ai/navmesh/navmeshstore.cpp
