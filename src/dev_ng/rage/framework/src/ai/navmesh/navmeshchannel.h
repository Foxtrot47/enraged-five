//
// ai/navmeshchannel.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_NAVMESHCHANNEL_H
#define AI_NAVMESHCHANNEL_H

// Rage includes
#include "ai/aichannel.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(ai, path)

#define pathAssert(cond)				RAGE_ASSERT(ai_path,cond)
#define pathAssertf(cond,fmt,...)		RAGE_ASSERTF(ai_path,cond,fmt,##__VA_ARGS__)
#define pathFatalAssertf(cond,fmt,...)	RAGE_FATALASSERTF(ai_path,cond,fmt,##__VA_ARGS__)
#define pathVerifyf(cond,fmt,...)		RAGE_VERIFYF(ai_path,cond,fmt,##__VA_ARGS__)
#define pathErrorf(fmt,...)				RAGE_ERRORF(ai_path,fmt,##__VA_ARGS__)
#define pathWarningf(fmt,...)			RAGE_WARNINGF(ai_path,fmt,##__VA_ARGS__)
#define pathDisplayf(fmt,...)			RAGE_DISPLAYF(ai_path,fmt,##__VA_ARGS__)
#define pathDebugf1(fmt,...)			RAGE_DEBUGF1(ai_path,fmt,##__VA_ARGS__)
#define pathDebugf2(fmt,...)			RAGE_DEBUGF2(ai_path,fmt,##__VA_ARGS__)
#define pathDebugf3(fmt,...)			RAGE_DEBUGF3(ai_path,fmt,##__VA_ARGS__)

} // namespace rage

#endif // AI_NAVMESHCHANNEL_H
