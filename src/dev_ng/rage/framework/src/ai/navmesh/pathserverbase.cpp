//
// ai/navmesh/pathserverbase.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "pathserverbase.h"

using namespace rage;

//-----------------------------------------------------------------------------

#ifdef GTA_ENGINE

namespace rage
{
	sysCriticalSectionToken m_NavMeshDataCriticalSectionToken;
	sysCriticalSectionToken m_NavMeshImmediateAccessCriticalSectionToken;
	sysCriticalSectionToken m_DynamicObjectsCriticalSectionToken;
	sysCriticalSectionToken m_RequestsCriticalSectionToken;
	sysCriticalSectionToken m_NavMeshStoreLoadedMeshesCriticalSectionToken;
}

#endif	// GTA_ENGINE

//-----------------------------------------------------------------------------

u32 fwPathServer::m_iTotalMemoryUsed = 0;

atRangeArray<aiNavMeshStore *, kNumNavDomains> fwPathServer::m_pNavMeshStores;
bool fwPathServer::m_NavDomainsEnabled[kNumNavDomains] = { true, true };
CDynamicNavMeshStore fwPathServer::m_DynamicNavMeshStore;

CNavMesh * fwPathServer::m_pTessellationNavMesh = NULL;
TTessInfo * fwPathServer::m_PolysTessellatedFrom = NULL;
u32 fwPathServer::m_iCurrentNumTessellationPolys = 0;
u32 fwPathServer::m_iCurrentTessellationArrayIndex = 0;
u32 fwPathServer::m_iTessellationNavmeshCurrentNumVerts = 0;
u32 fwPathServer::m_iTessellationNavmeshCurrentPoolSize = 0;
u32 fwPathServer::m_iCurrentConnectionPolyIndex = 0;
bool fwPathServer::m_bHasBeenThroughEntireTessellationNavMesh = false;
bool fwPathServer::ms_bExcludeVehicleDoorsFromNavigation = true;
float fwPathServer::ms_fDefaultMaxPathFindDistFromOrigin = 100.0f;
#if !__FINAL
sysPerformanceTimer	* fwPathServer::m_NavMeshLoadingTimer = NULL;
sysPerformanceTimer	* fwPathServer::m_NavMesh2ndLoadingTimer = NULL;
u32 fwPathServer::ms_iPeakNumTessellationPolys = 0;
#endif
sysIpcSema fwPathServer::m_PathRequestSema = 0;
u32 fwPathServer::m_iMaxDynamicNavmeshTypes = 0;
bool fwPathServer::m_bForceAbortCurrentRequest = false;
bool fwPathServer::m_bPathServerThreadSleepingDuringSearch = false;

#ifdef GTA_ENGINE
bool fwPathServer::m_bFindCover_NavMeshesHaveChanged = true;
#endif

bool fwPathServer::m_bPathServerThreadIsActive = false;

const float CDynamicObjectsGridCell::ms_fSizeOfGrid				= 32.0f;
atArray<CDynamicObjectsGridCell*> CDynamicObjectsContainer::ms_DynamicObjectGrids;
bool CDynamicObjectsContainer::ms_bCacheEnabled = false;

//-----------------------------------------------------------------------------

const float CGridCellsCache::ms_fMaxRange = 256.0f;
const float CGridCellsCache::ms_fCacheCellSize = 16.0f;
s32 CGridCellsCache::m_iNextFree = 0;

CGridCellsCache::CEntry * CGridCellsCache::m_pEntries[CGridCellsCache::ms_iNumCellsAcross * CGridCellsCache::ms_iNumCellsAcross];
CGridCellsCache::CEntry CGridCellsCache::m_pPool[CGridCellsCache::ms_iNumCacheCells];

Vector3 CGridCellsCache::m_vMin = VEC3_ZERO;
Vector3 CGridCellsCache::m_vMax = VEC3_ZERO;
TShortMinMax CGridCellsCache::m_MinMax;

//-----------------------------------------------------------------------------

//****************************************************************************
//	GetNavMeshIndexFromSector
//	Given the coordinates of a CWorld sector, this function returns the index
//	of the navmesh at that location.  This indexes into the navmeshes array.
//****************************************************************************
u32 fwPathServer::GetNavMeshIndexFromSector(int iSectorX, int iSectorY, aiNavDomain domain)
{
	aiNavMeshStore* pStore = m_pNavMeshStores[domain];

	int iNavMeshBlockX = (int) (iSectorX / (float) pStore->GetNumSectorsPerMesh());
	int iNavMeshBlockY = (int) (iSectorY / (float) pStore->GetNumSectorsPerMesh());

	if(iNavMeshBlockX < 0 || iNavMeshBlockX >= pStore->GetNumMeshesInX() || iNavMeshBlockY < 0 || iNavMeshBlockY >= pStore->GetNumMeshesInY())
		return NAVMESH_NAVMESH_INDEX_NONE;

	u32 iIndex = (iNavMeshBlockY * pStore->GetNumMeshesInX()) + iNavMeshBlockX;

	return iIndex;
}

void fwPathServer::GetSectorFromNavMeshIndex(const u32 iNavMesh, s32 & iSectorXOut, s32 & iSectorYOut, aiNavDomain domain)
{
	aiNavMeshStore* pStore = m_pNavMeshStores[domain];

	iSectorYOut = (iNavMesh / pStore->GetNumMeshesInX());
	iSectorXOut = iNavMesh - (iSectorYOut*pStore->GetNumMeshesInX());

	iSectorXOut *= pStore->GetNumSectorsPerMesh();
	iSectorYOut *= pStore->GetNumSectorsPerMesh();
}

u32 fwPathServer::GetNavMeshIndexFromFilename(aiNavDomain domain, const char * pFilename, s32 * pSectorX, s32 * pSectorY)
{
	//******************************************************************
	// NavMesh filenames are currently in the following format:
	// NB : This is the *independent* format (".inv")
	//
	// "navmesh[X][Y].inv"		(eg. "navmesh[108][24].inv")
	//	X = Starting Sector X
	//	Y = Starting Sector Y
	//
	// All other files in the "navmeshes.img" file are assumed to be
	// dynamic navmeshes belonging to vehicles, etc.
	//
	// Note that these dynamic navmeshes also need to have a naming
	// convention worked out at some point.  Note also that the main map
	// navmeshes naming format above should at some point soon be
	// simplified to something like "M_108_24" to save space.
	//******************************************************************

	char tmpFilename[256];
	strcpy(tmpFilename, pFilename);
	strlwr(tmpFilename);

	u32 iIndex;

	//char * ptr = strstr(tmpFilename, "navmesh[");
	//if(ptr)
	{
		// Skip to first '[' character
		char * ptr = strstr(tmpFilename, "[");
		//Assertf(ptr, "NavMesh filename was in wrong format");
		if(!ptr)
			return NAVMESH_NAVMESH_INDEX_NONE;

		// The character after this is the X position
		ptr++;
		char * pX = ptr;

		// Skip to first ']' character
		ptr = strstr(pX, "]");
		Assertf(ptr, "NavMesh exterior filename was in wrong format");
		// Terminate the XPos string here
		*ptr = 0;

		// Skip to the second '[' character
		ptr++;
		if(*ptr=='[')
		{
			ptr++;
			char * pY = ptr;

			// Skip to second ']' character
			ptr = strstr(pY, "]");

			// Terminate the YPos string here
			*ptr = 0;

			// Read in X & Y
			s32 iXPos, iYPos;
			sscanf(pX, "%i", &iXPos);
			sscanf(pY, "%i", &iYPos);

			if(pSectorX)
				*pSectorX = iXPos;
			if(pSectorY)
				*pSectorY = iYPos;

			aiNavMeshStore* pStore = m_pNavMeshStores[domain];
			iXPos /= pStore->GetNumSectorsPerMesh();
			iYPos /= pStore->GetNumSectorsPerMesh();

			// Return the navmesh index
			iIndex = (iYPos * pStore->GetNumMeshesInX()) + iXPos;
			return iIndex;
		}
	}

	// We currently assume that all other files in the image are dynamic navmeshes for vehicles
	return NAVMESH_NAVMESH_INDEX_NONE;
}

void fwPathServer::GetNavMeshExtentsFromIndex(const TNavMeshIndex iMeshIndex, Vector2 & vMin, Vector2 & vMax, aiNavDomain domain)
{
	aiNavMeshStore* pStore = m_pNavMeshStores[domain];

	const int iY = iMeshIndex / pStore->GetNumMeshesInY();
	const int iX = iMeshIndex - (iY * pStore->GetNumMeshesInY());
	const float fMeshSize = pStore->GetMeshSize();

	//	vMin.x = ((((float)iX) * fMeshSize) + WORLD_BORDERXMIN);
	//	vMin.y = ((((float)iY) * fMeshSize) + WORLD_BORDERYMIN);
	vMin.x = ((((float)iX) * fMeshSize) + CPathServerExtents::m_vWorldMin.x);
	vMin.y = ((((float)iY) * fMeshSize) + CPathServerExtents::m_vWorldMin.y);

	vMax.x = vMin.x + fMeshSize;
	vMax.y = vMin.y + fMeshSize;
}

//-----------------------------------------------------------------------------

void fwPathServer::ForceAbortCurrentPathRequest(const bool bYield)
{
	m_bForceAbortCurrentRequest = true;

	// If the pathserver thread is busy processing a request, then yield the main game's timeslice
	// to increase the likelihood that the pathserver thread will abort it's request immediately &
	// release its critical section lock on the navmesh data; at which point it will also yield,
	// allowing this main game thread to continue to place a new navmesh.
	if(m_bPathServerThreadIsActive && bYield)
	{
		sysIpcYield(0);
	}
}

//-----------------------------------------------------------------------------

// End of file ai/navmesh/pathserverbase.cpp

