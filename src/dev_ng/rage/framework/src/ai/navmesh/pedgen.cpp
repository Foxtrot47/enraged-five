//
// ai/navmesh/pedgen.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "pedgen.h"
#include "ai/navmesh/datatypes.h"
#include "ai/navmesh/navmesh.h"
#include "ai/navmesh/pathserverbase.h"
#include "fwmaths/random.h"

using namespace rage;

fwPedGenNavMeshIterator::fwPedGenNavMeshIterator()
{
	Reset();
}
fwPedGenNavMeshIterator::~fwPedGenNavMeshIterator()
{

}
void fwPedGenNavMeshIterator::Reset()
{
	m_iNumEntries = 0;
	m_iPercentage = 0;
	m_iCurrentIndex = -1;
	m_iNumRemainingInNavMesh = 0;
	m_iTotalNumPolys = 0;
}
void fwPedGenNavMeshIterator::AddNavMesh(CNavMesh * pNavMesh)
{
	Assert(pNavMesh);
	Assert(m_iNumEntries < m_iMaxNumEntries);

	if(m_iNumEntries < m_iMaxNumEntries)
	{
		m_Containers[m_iNumEntries].m_iNavMesh = pNavMesh->GetIndexOfMesh();
		m_Containers[m_iNumEntries].m_iCurrentPoly = 0;
		m_Containers[m_iNumEntries].m_iNumPolysFromMesh = pNavMesh->GetNumPolys();
		m_iTotalNumPolys += (s32) ( ((float)pNavMesh->GetNumPolys()) * (((float)m_iPercentage)/100.0f) );

		m_iNumEntries++;
	}
}
void fwPedGenNavMeshIterator::StartNewInteration(const int iPercentOfCandidatePolysPerNavmesh)
{
	m_iPercentage = iPercentOfCandidatePolysPerNavmesh;
	m_iCurrentIndex = -1;
	m_iNumRemainingInNavMesh = 0;
}
bool fwPedGenNavMeshIterator::IsAtEnd()
{
	return (m_iCurrentIndex==-1);
}
bool fwPedGenNavMeshIterator::GetNext(CNavMesh ** pOutNavMesh, TNavMeshPoly ** pOutPoly)
{
	CNavMesh * pNavMesh = NULL;

	// This iterator is only for the regular mesh at this point. Would be easy enough to
	// have this be an input parameter when setting up the iterator, if there is a need.
	static const aiNavDomain domain = kNavDomainRegular;

	if(m_iCurrentIndex==-1 || m_iNumRemainingInNavMesh<=0)
	{
		m_iCurrentIndex++;

		while(!pNavMesh && m_iCurrentIndex < m_iNumEntries)
		{
			pNavMesh = fwPathServer::GetNavMeshFromIndex(m_Containers[m_iCurrentIndex].m_iNavMesh, domain);

			if(pNavMesh)
			{
				if(m_iPercentage == 100)
				{
					m_iNumRemainingInNavMesh = m_Containers[m_iCurrentIndex].m_iNumPolysFromMesh;
				}
				else
				{
					int iNumInSequence = m_Containers[m_iCurrentIndex].m_iNumPolysFromMesh;
					float fNum = ( ((float)iNumInSequence) / 100.0f) * ((float)m_iPercentage);
					m_iNumRemainingInNavMesh = (s32) fNum;
				}
			}

			if(!m_iNumRemainingInNavMesh)
			{
				pNavMesh = NULL;
				m_iCurrentIndex++;
			}
		}
	}
	else
	{
		pNavMesh = fwPathServer::GetNavMeshFromIndex(m_Containers[m_iCurrentIndex].m_iNavMesh, domain);
	}

	if(!pNavMesh)
	{
		m_iCurrentIndex = -1;
		m_iNumRemainingInNavMesh = 0;
		return false;
	}
	if(m_iNumRemainingInNavMesh)
	{
		if(m_iPercentage == 100)
		{
			const s32 iPolyIndex = m_Containers[m_iCurrentIndex].m_iNumPolysFromMesh - m_iNumRemainingInNavMesh;
			*pOutPoly = pNavMesh->GetPoly(iPolyIndex);
			*pOutNavMesh = pNavMesh;
			m_iNumRemainingInNavMesh--;
		}
		else
		{
			const s32 iPolyIndex = fwRandom::GetRandomNumberInRange(0, m_Containers[m_iCurrentIndex].m_iNumPolysFromMesh);
			*pOutPoly = pNavMesh->GetPoly(iPolyIndex);
			*pOutNavMesh = pNavMesh;
			m_iNumRemainingInNavMesh--;
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------------------------

CPedGenSortedList::CPedGenSortedList()
{
	m_bComplete = false;
}
CPedGenSortedList::~CPedGenSortedList()
{

}
void CPedGenSortedList::Reset()
{
	m_fMinDeficit = 0.0f;
	m_fMaxDeficit = 0.0f;
	m_bComplete = false;
	m_bConsumed = false;
	m_bInUse = false;
	m_Array.clear();
}

bool CPedGenSortedList::Insert(const Vector3 & vPos, const s32 iFlags, const float fPedDeficit)
{
	if(!HasSpaceLeft())
	{
		// If deficit if lower than the lowest we have, then just ignore
		if( fPedDeficit < m_fMinDeficit )
			return true;

		// Otherwise delete our lowest
		m_Array.Delete( m_Array.GetCount()-1 );
		m_fMinDeficit = m_Array[ m_Array.GetCount()-1 ].m_fDeficit;
	}

	Assert(m_Array.GetCount() < m_Array.GetMaxCount());

	m_fMinDeficit = Min(m_fMinDeficit, fPedDeficit);
	m_fMaxDeficit = Min(m_fMaxDeficit, fPedDeficit);

	for(s32 i=0; i<m_Array.GetCount(); i++)
	{
		if(fPedDeficit > m_Array[i].m_fDeficit)
		{
			m_Array.Insert(i) = Item(vPos, fPedDeficit, iFlags);
			return true;
		}
	}

	m_Array.Append() = Item(vPos, fPedDeficit, iFlags);
	return true;
}

void CPedGenSortedList::CopyToPedGenCoords(CPedGenCoords & destCoords)
{
	destCoords.Reset();

#if __BANK
	destCoords.m_fMaxDeficit = m_fMaxDeficit;
#endif

	for(s32 i=0; i<m_Array.GetCount() && destCoords.m_iNumItems<MAX_POTENTIAL_PEDGEN_COORDS; i++)
	{
		Item & item = m_Array[i];

		destCoords.m_Coords[destCoords.m_iNumItems].m_vPosition = item.m_vPosition;
		destCoords.m_Coords[destCoords.m_iNumItems].m_iFlags = item.m_iFlags;
#if __BANK
		destCoords.m_Coords[destCoords.m_iNumItems].m_fPedDeficit = item.m_fDeficit;
#endif
		destCoords.m_iNumItems++;
	}
}


// End of file ai/navmesh/pedgen.cpp
