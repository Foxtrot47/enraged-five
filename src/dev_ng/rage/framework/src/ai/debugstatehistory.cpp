//
// debuginfopool.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//
#include "debugstatehistory.h"
#include "system/stack.h"
#include "diag/trap.h"

#if !__FINAL

namespace rage
{
/////////////////////////////////////////////////////////////////////////////////
// fwDebugStateHistory
/////////////////////////////////////////////////////////////////////////////////

void fwDebugStateHistory::Stamp( u32 previousState, u32 newState,
								 int /*calleeIgnoreLevels*/,
								 u32 altCallstack )
{
	if ( !altCallstack )
	{
		u32 trace = 
#if __ASSERT 
			sysStack::RegisterBacktrace();
#else		
			0;
#endif
		SetAndIncrement( trace, previousState, newState );
	}
	else
	{
		SetAndIncrement( altCallstack, previousState, newState );
	}

}

void fwDebugStateHistory::SetAndIncrement( u32 callstack, 
										   u32 previousState, 
										   u32 newState )
{

	Cursor().m_Callstack = callstack;
	Assign( Cursor().m_OldState, previousState );
	Assign( Cursor().m_NewState, newState );

	++m_CountAndFull;
	if ( m_CountAndFull >= MAX_SIZE * 2 )
	{
		m_CountAndFull -= MAX_SIZE;
	}
	Assign( m_Cursor, Inc( m_Cursor, 1 ) );
}


void fwDebugStateHistory::Pop()
{
	// Cursor stays the same...
	// Count and full needs to be thresholded at MAX_SIZE-1
	TrapLE( m_CountAndFull, 0 );
	--m_CountAndFull;
	if ( m_CountAndFull > MAX_SIZE-1 )
	{
		m_CountAndFull = MAX_SIZE-1;
	}
	Assign( m_Cursor, Inc(m_Cursor,-1) );
}

int fwDebugStateHistory::Inc( int cursor, int delta )
{
	cursor += delta;
	while (cursor < 0 )
	{
		cursor += MAX_SIZE;
	}
	cursor = cursor % MAX_SIZE;
	return cursor;

}


} // namespace rage

#endif		// !__FINAL