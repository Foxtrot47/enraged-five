//
// ai/aichannel.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_AICHANNEL_H
#define AI_AICHANNEL_H

// Rage includes
#include "diag/channel.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(ai)

#define aiAssert(cond)						RAGE_ASSERT(ai,cond)
#define aiAssertf(cond,fmt,...)				RAGE_ASSERTF(ai,cond,fmt,##__VA_ARGS__)
#define aiFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(ai,cond,fmt,##__VA_ARGS__)
#define aiVerify(cond)						RAGE_VERIFY(ai,cond)
#define aiVerifyf(cond,fmt,...)				RAGE_VERIFYF(ai,cond,fmt,##__VA_ARGS__)
#define aiErrorf(fmt,...)					RAGE_ERRORF(ai,fmt,##__VA_ARGS__)
#define aiWarningf(fmt,...)					RAGE_WARNINGF(ai,fmt,##__VA_ARGS__)
#define aiDisplayf(fmt,...)					RAGE_DISPLAYF(ai,fmt,##__VA_ARGS__)
#define aiDebugf1(fmt,...)					RAGE_DEBUGF1(ai,fmt,##__VA_ARGS__)
#define aiDebugf2(fmt,...)					RAGE_DEBUGF2(ai,fmt,##__VA_ARGS__)
#define aiDebugf3(fmt,...)					RAGE_DEBUGF3(ai,fmt,##__VA_ARGS__)
#define aiLogf(severity,fmt,...)			RAGE_LOGF(ai,severity,fmt,##__VA_ARGS__)
#define aiCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,ai,severity,fmt,##__VA_ARGS__)

} // namespace rage

#endif // AI_AICHANNEL_H
