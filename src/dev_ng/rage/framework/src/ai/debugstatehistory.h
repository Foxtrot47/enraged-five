/////////////////////////////////////////////////////////////////////////////////
// FILE :		DebugStateHistory.h
// PURPOSE :	Provides support for debugging SetState style histories.
// AUTHOR :		Brett Laming.
// CREATED :	05/07/2011
/////////////////////////////////////////////////////////////////////////////////
#ifndef _DEBUGSTATEHISTORY_H_
#define _DEBUGSTATEHISTORY_H_

namespace rage
{

/////////////////////////////////////////////////////////////////////////////////
// fwDebugStateHistory
//
// Light record of state changes and locations
/////////////////////////////////////////////////////////////////////////////////
class fwDebugStateHistory
{
public:

	class cStateRecord
	{
	public:
		rage::u32		m_Callstack;
		rage::u16		m_OldState;
		rage::u16		m_NewState;
	};

	enum Constants
	{
		// PURPOSE: Kept at a nice number so that the smAllocators inside the heap
		// can keep info from de-fragging.
		DEBUG_DATA_STORE_SIZE_BYTES = 80,
		MAX_SIZE		  = (DEBUG_DATA_STORE_SIZE_BYTES - sizeof(int)) / 
							sizeof(cStateRecord)
	};
public:

	// PURPOSE: Puts this data store back to it's initial condition
	// (nothing store)
	inline void Init();

	// PURPOSE: Stamp us with a state change and increment cursor
	void						Stamp( u32 previousState, u32 newState,
									   int calleeIgnoreLevels,
									   u32 altCallstack );

	// PURPOSE: Get the data record at the given index
	// (index can be potentially > MAX_SIZE [just internal indication that its full])
	inline const cStateRecord &	GetData( int index ) const;

	// PURPOSE: Number of items in the data record, starting from GetRingStart
	inline int					GetCount() const;

	// PURPOSE: First entry for the state history
	inline int					GetRingStart() const;

	// PURPOSE: Get the last entry added into the start history [for spew purposes]
	inline int					GetLast() const;

	// PURPOSE: Pop the last entry off
	void						Pop();

private:
	// Helper access to the cursor point
	inline cStateRecord &		Cursor() ;
	static int					Inc( int cursor, int delta );

	// Set the state and increment the cursor
	void	SetAndIncrement( u32 callstack,
							 u32 previousState,
							 u32 newState );

	cStateRecord				m_Data[MAX_SIZE];

	// Ring buffer mechanics:
	// Example is MAX_SIZE 4
	// Cursor + CountAndFull must remain independent for pop to work
	//
	// Push->
	// Cursor:			0 1 2 3 0 1 2 3 0 1 2 3 0 1 2 3
	// Last:			x 0 1 2 3 0 1 2 3 0 1 2 3 0 1 2	// derived from above
	// CountAndFull:	0 1 2 3 4 5 6 7 4 5 6 7 4 5 6 7
	// Count:			0 1 2 3 4 4 4 4 4 4 4 4 4 4 4 4 // derived from above

	// <-Pop
	// Cursor:			x 0 1 2 3 0 1 2 3 0 1 2 3 0 1 2
	// Count And Full:  x 0 1 2 3 3 3 3 3 3 3 3 3 3 3 3 
	// Count:			x 0 1 2 3 3 3 3 3 3 3 3 3 3 3 3 // derived from above

	s16							m_Cursor;
	s16							m_CountAndFull; // Allows Pop
};

// Inline functions /////////////////////////////////////////////////////////////

inline void fwDebugStateHistory::Init()
{
	m_Cursor = 0;
	m_CountAndFull = 0;
}

inline int fwDebugStateHistory::GetCount() const
{
	return ( m_CountAndFull > MAX_SIZE ) ? MAX_SIZE : m_CountAndFull;
}

inline int fwDebugStateHistory::GetRingStart() const
{
	return Inc(m_Cursor, -GetCount() );
}

inline const fwDebugStateHistory::cStateRecord & 
fwDebugStateHistory::GetData( int index ) const
{
	return m_Data[ (index % MAX_SIZE) ];
}

inline int fwDebugStateHistory::GetLast() const
{
	Assert( m_CountAndFull != 0 );
	return Inc(m_Cursor, -1 );
}

inline fwDebugStateHistory::cStateRecord &fwDebugStateHistory::Cursor()
{
	return m_Data[m_Cursor];
}

/////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif // _DEBUGSTATEHISTORY_H_
