//
// ai/aichannel.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/aichannel.h"

// Rage headers
#include "system/param.h"

namespace rage
{

RAGE_DEFINE_CHANNEL(ai)

} // namespace rage
