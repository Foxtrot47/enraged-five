//
// ai/event.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_EVENT_H
#define AI_EVENT_H

#include "fwEvent/Event.h"

namespace rage
{

class aiEvent : public fwEvent
{
public:

	// PURPOSE:	Register that we are about to start updating AI events, which should
	//			use the given time step.
	// NOTES:	This would have to change if we wanted to multithread event updates,
	//			then we couldn't use static time step variables like this.
	static void BeginEventUpdates(float timeStep)
	{
		Assert(sm_TimeStep == 0.0f);	// Called BeginEventUpdates() again, without EndEventUpdates()?
		Assert(timeStep >= 0.0f);
		sm_TimeStep = timeStep;
	}

	// PURPOSE:	End a sequence of event updates previously initiated by BeginEventUpdates().
	static void EndEventUpdates()
	{
		Assert(sm_TimeStep >= 0.0f);		// Maybe called EndEventUpdates() without BeginEventUpdates()?
		sm_TimeStep = 0.0f;
	}

	// PURPOSE:	Register that we are about to look at an ai event, but don't want to update any internal timers.
	static void BeginEventQuery()
	{
		sm_QueryOnly = true;
	}

	// PURPOSE:	End a sequence of event updates previously initiated by BeginEventQuery().
	static void EndEventQuery()
	{
		sm_QueryOnly = false;
	}


	// PURPOSE:	Get the time step to use for updating this event, to be called from within
	//			the event during an update. Returns the time step in seconds.
	// NOTES:	Meant as a replacement for fwTimer::GetTimeStep(), from within event updates.
	float GetTimeStep() const { Assert(sm_TimeStep >= 0.0f || sm_QueryOnly); return sm_TimeStep; }

private:

	// PURPOSE:	Time step in s to use for event currently being updated.
	static float sm_TimeStep;
	// PURPOSE: Denotes that we are about to querying the event outside of the main event update.
	//			Allows us to disable update logic where necessary.
	static bool sm_QueryOnly;

};

} // namespace rage

#endif // AI_EVENT_H
