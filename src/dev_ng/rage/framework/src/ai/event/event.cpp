//
// ai/event.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/event/event.h"

namespace rage
{

float aiEvent::sm_TimeStep = 0.0f;
bool aiEvent::sm_QueryOnly = false;

} // namespace rage
