//
// ai/aioptimisations.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef FW_AI_AIOPTIMISATIONS_H
#define FW_AI_AIOPTIMISATIONS_H

#define FW_AI_OPTIMISATIONS_OFF	0

#if FW_AI_OPTIMISATIONS_OFF
#define FW_AI_OPTIMISATIONS() OPTIMISATIONS_OFF()
#else
#define FW_AI_OPTIMISATIONS()
#endif // FW_AI_OPTIMISATIONS_OFF

#if FW_AI_OPTIMISATIONS_OFF
#define FW_AI_OPTIMISATIONS_OFF_ONLY(x)	x
#else
#define FW_AI_OPTIMISATIONS_OFF_ONLY(x)
#endif // FW_AI_OPTIMISATIONS_OFF

#endif // FW_AI_AIOPTIMISATIONS_H
