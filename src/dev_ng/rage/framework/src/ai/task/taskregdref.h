//
// ai/taskregdref.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASK_REGDREF_H
#define AI_TASK_REGDREF_H

// Framework headers
#include "fwtl/regdrefs.h"


///////////////////////////////////////////////////////////////////////////////



namespace rage
{

// If we are using extended ref count info, then our task base class supports
// the new features.
typedef fwRefAwareBase aiTaskBaseClass;

// Forward declarations
class aiTask;

// PURPOSE: Forms a layer of abstraction between taskRegdRef's and a potential
// different AI task ref aware base class.  Most functions can be allowed to
// come through. Only constructors + anything return *this really needs to 
// be altered.
template <typename T>
class taskRegdRef : public fwRegdRef<T, aiTaskBaseClass>
{
private:
	typedef fwRegdRef<T, aiTaskBaseClass> tBase;

public:
	inline taskRegdRef();
	inline explicit taskRegdRef(T* p);
	inline explicit taskRegdRef( taskRegdRef<T> const & rhs );
	inline taskRegdRef& operator=(taskRegdRef const &rhs );
	inline taskRegdRef& operator=(T* rhs);

};

// PURPOSE: Type defines for standard framework aiTask* pointers
typedef	taskRegdRef<aiTask>										RegdaiTask;
typedef	taskRegdRef<const aiTask>								RegdConstAITask;

///////////////////////////////////////////////////////////////////////////////
// taskRegdRef inline - really just pass through to the fwRegdRef base class

template <typename T>
taskRegdRef<T>::taskRegdRef() : tBase()
{
}

template <typename T>
taskRegdRef<T>::taskRegdRef(T* p) : tBase(p)
{
}

template <typename T>
taskRegdRef<T>::taskRegdRef(taskRegdRef<T> const & rhs ) : tBase( rhs )
{
}

template <typename T>
taskRegdRef<T>& taskRegdRef<T>::operator=(taskRegdRef<T> const &rhs )
{
	tBase::operator=(rhs);
	return *this;
}

template <typename T>
taskRegdRef<T>& taskRegdRef<T>::operator=(T* rhs )
{
	tBase::operator=(rhs);
	return *this;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // AI_TASK_REGDREF_H
