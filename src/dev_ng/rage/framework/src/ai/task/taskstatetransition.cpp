//
// ai/taskstatetransition.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/task/taskstatetransition.h"

// Framework headers
#include "ai/task/taskchannel.h"
#include "ai/aioptimisations.h"
#include "fwmaths/random.h"

FW_AI_OPTIMISATIONS()

namespace rage
{

aiTaskStateTransition::aiTaskStateTransition()
: m_StartState(-1)
, m_EndState(-1)
, m_Probability(0.0f)
, m_ConditionFlags(0)
{
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateTransition::Set(s32 iStartState, s32 iEndState, float fProbability, s64 iConditionFlags)
{
	m_StartState     = iStartState;
	m_EndState       = iEndState;
	m_Probability    = fProbability;
	m_ConditionFlags = iConditionFlags;
}

////////////////////////////////////////////////////////////////////////////////

bool aiTaskStateTransition::CheckTransition(s32 iStartState, s64 iConditionFlags)
{
	if(m_StartState != iStartState)
	{
		return false;
	}

	if((iConditionFlags & m_ConditionFlags ) != m_ConditionFlags)
	{
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateTransitionFallback::aiTaskStateTransitionFallback()
	: m_StartState(-1)
	, m_EndState(-1)
{
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateTransitionFallback::Set(s32 iStartState, s32 iEndState)
{
	m_StartState     = iStartState;
	m_EndState       = iEndState;
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateTransitionTable::aiTaskStateTransitionTable()
: m_NoTransitions(0)
, m_NoFallbacks(0)
{
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateTransitionTable::AddTransition(s32 iStartState, s32 iEndState, float fProbability, s64 iConditionFlags)
{
	if(m_NoTransitions >= MAX_TRANSITIONS)
	{
		taskAssertf(0, "EXCEEDED MAX TRANSITIONS!");
		return;
	}

	m_Transitions[m_NoTransitions].Set(iStartState, iEndState, fProbability, iConditionFlags);
	++m_NoTransitions;
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateTransitionTable::AddFallback(s32 iStartState, s32 iEndState)
{
	if(m_NoFallbacks >= MAX_FALLBACKS)
	{
		taskAssertf(0, "EXCEEDED MAX FALLBACKS!");
		return;
	}

	m_Fallbacks[m_NoFallbacks].Set(iStartState, iEndState);
	++m_NoFallbacks;
}
////////////////////////////////////////////////////////////////////////////////

s32 aiTaskStateTransitionTable::CheckTransitions(fwEntity* UNUSED_PARAM(pEntity), s32 iInitialState, s64 iConditionFlags)
{
	// build a list of transitions that meet the conditions
	s32 aiValidTransitions[MAX_TRANSITIONS];
	s32 iNoValidConditions = 0;
	float fProbabilitySum = 0.0f;
	for(s32 iTranstion = 0; iTranstion < m_NoTransitions; iTranstion++)
	{
		if(m_Transitions[iTranstion].CheckTransition(iInitialState, iConditionFlags))
		{
			aiValidTransitions[iNoValidConditions++] = iTranstion;
			fProbabilitySum += m_Transitions[iTranstion].GetProbability();
		}
	}

	// If there are any valid conditions, randomly choose one
	if(iNoValidConditions > 0)
	{
		float fRandom = fwRandom::GetRandomNumberInRange(0.0f, 1.0f);
		// Randomly don't choose
		if(fRandom > fProbabilitySum)
		{
			return -1;
		}
		else
		{
			float fProbabilityIncrement = 0.0f;
			fRandom = fwRandom::GetRandomNumberInRange(0.0f, fProbabilitySum);
			for(s32 i = 0; i < iNoValidConditions; i++)
			{
				s32 iIndex = aiValidTransitions[i];
				fProbabilityIncrement += m_Transitions[iIndex].GetProbability();
				if(fRandom < fProbabilityIncrement)
				{
					return m_Transitions[iIndex].GetEndState();
				}
			}
		}
	}
	// If there are no valid conditions, look for a fallback for the state
	else
	{
		for(s32 iFallback = 0; iFallback < m_NoFallbacks; ++iFallback)
		{
			const aiTaskStateTransitionFallback& rFallback = m_Fallbacks[iFallback];
			if(rFallback.GetStartState() == iInitialState)
			{
				return rFallback.GetEndState();
			}
		}
	}

	return -1;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
