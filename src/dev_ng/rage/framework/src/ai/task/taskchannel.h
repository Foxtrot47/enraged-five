//
// ai/taskchannel.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASKCHANNEL_H
#define AI_TASKCHANNEL_H

// Rage headers
#include "ai/aichannel.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(ai, task)

#define taskAssert(cond)				RAGE_ASSERT(ai_task,cond)
#define taskAssertf(cond,fmt,...)		RAGE_ASSERTF(ai_task,cond,fmt,##__VA_ARGS__)
#define taskFatalAssertf(cond,fmt,...)	RAGE_FATALASSERTF(ai_task,cond,fmt,##__VA_ARGS__)
#define taskVerifyf(cond,fmt,...)		RAGE_VERIFYF(ai_task,cond,fmt,##__VA_ARGS__)
#define taskErrorf(fmt,...)				RAGE_ERRORF(ai_task,fmt,##__VA_ARGS__)
#define taskWarningf(fmt,...)			RAGE_WARNINGF(ai_task,fmt,##__VA_ARGS__)
#define taskDisplayf(fmt,...)			RAGE_DISPLAYF(ai_task,fmt,##__VA_ARGS__)
#define taskDebugf1(fmt,...)			RAGE_DEBUGF1(ai_task,fmt,##__VA_ARGS__)
#define taskDebugf2(fmt,...)			RAGE_DEBUGF2(ai_task,fmt,##__VA_ARGS__)
#define taskDebugf3(fmt,...)			RAGE_DEBUGF3(ai_task,fmt,##__VA_ARGS__)

} // namespace rage

#endif // AI_TASKCHANNEL_H
