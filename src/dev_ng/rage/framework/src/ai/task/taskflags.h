//
// ai/taskflags.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASKFLAGS_H
#define AI_TASKFLAGS_H

// Rage headers
#include "system/bit.h"

// Framework headers
#include "fwutil/flags.h"

namespace rage
{

// PURPOSE: Wrapper class for aiTask flag settings
class aiTaskFlags
{
public:

	// Flags that can be queried to check the state of the task
	enum InformationFlags
	{
		// Has the task ever ran?
		HasBegun							= BIT0,

		// Has the task terminated?
		HasFinished							= BIT1,

		// Is, or was the task aborting / aborted?
		IsAborted							= BIT2,

		// Has the sub task finished?
		SubTaskFinished						= BIT3,

		// Has the animation finished?
		AnimFinished						= BIT4,

		// Has task termination been requested?
		TerminationRequested				= BIT5,

		// Has the current state not been started yet?
		CurrentStateNotYetEntered			= BIT6,

		// Are we currently in MakeAbortable?
		InMakeAbortable						= BIT7,

		// Can this task skip ProcessPreFSM?
		SkipProcessPreFSM					= BIT8,

		// Can this task skip ProcessPostFSM?
		SkipProcessPostFSM					= BIT9
	};

	// Flags that can be set to alter the behaviour of the task
	enum AdjustableFlags
	{
		// Allow the current sub task to persist after a state change - 
		// Default behaviour is to abort the sub task on a change of state
		KeepCurrentSubtaskAcrossTransition	= BIT0,

		// Restart the current state
		RestartCurrentState					= BIT1,

		// Restart the current state after we've aborted
		RestartStateOnResume				= BIT2,

		// Quit the task after we've aborted
		QuitOnResume						= BIT3,

		// Trigger deletion of the active sub task
		RemoveSubtask						= BIT4,

		// Trigger creation of a new sub task
		// Should only be set/queried internally to the task system
		Internal_NewSubtask					= BIT5,

		//When the next task to be applied to a clone
		//is the same as its current running task
		HandleCloneSwapToSameTaskType			= BIT6,

		// When the current tasks at this priority have finished,
		// process the next task immediately rather than wait a frame
		ProcessNextActiveTaskImmediately	= BIT7,

		// If this flag is set, skip the virtual function call to FSM_SetAnimationFlags()
		SkipSetAnimationFlags				= BIT8
	};

	aiTaskFlags();
	aiTaskFlags(const s16 iInformationFlags, const s16 iAdjustableFlags);

	bool IsFlagSet(InformationFlags flag) const;
	bool IsFlagSet(AdjustableFlags flag) const;

	void SetFlag(InformationFlags flag);
	void SetFlag(AdjustableFlags flag);

	void ClearFlag(InformationFlags flag);
	void ClearFlag(AdjustableFlags flag);

private:

	fwFlags16 m_iInformationFlags;
	fwFlags16 m_iAdjustableFlags;
};

////////////////////////////////////////////////////////////////////////////////

inline aiTaskFlags::aiTaskFlags()
{
}

////////////////////////////////////////////////////////////////////////////////

inline aiTaskFlags::aiTaskFlags(const s16 iInformationFlags, const s16 iAdjustableFlags)
: m_iInformationFlags(iInformationFlags)
, m_iAdjustableFlags(iAdjustableFlags)
{
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTaskFlags::IsFlagSet(InformationFlags flag) const
{
	return m_iInformationFlags.IsFlagSet((s16)flag);
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTaskFlags::IsFlagSet(AdjustableFlags flag) const 
{
	return m_iAdjustableFlags.IsFlagSet((s16)flag);
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskFlags::SetFlag(InformationFlags flag)
{
	m_iInformationFlags.SetFlag((s16)flag);
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskFlags::SetFlag(AdjustableFlags flag)
{
	m_iAdjustableFlags.SetFlag((s16)flag);
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskFlags::ClearFlag(InformationFlags flag)
{
	m_iInformationFlags.ClearFlag((s16)flag);
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskFlags::ClearFlag(AdjustableFlags flag)
{
	m_iAdjustableFlags.ClearFlag((s16)flag);
}

} // namespace rage

#endif // AI_TASKFLAGS_H
