//
// ai/taskspew.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASKSPEW_H
#define AI_TASKSPEW_H

#include "physics/debugevents.h"
#include "atl/hashstring.h"
#include "parser/macros.h"

namespace rage
{
	namespace aiTaskSpew
	{
		enum SpewType
		{
			SPEW_STD,
			SPEW_OVERFLOW,
			SPEW_ASSERT,
		};

	} // namespace aiTaskSpew

#if DR_ENABLED
	class fwEntity;
	class aiTask;

	namespace debugPlayback
	{	
		void TrackEntity(const fwEntity *pEntity, bool bForce);
		void RecordSwitchSubTask(const aiTask &rTask, const aiTask *pOldTask, const aiTask &rSubTask);
		void RecordTaskEvent(const aiTask* pTask, const char *pEvent);
		void RecordNewTaskEvent(const aiTask* pTask, const aiTask* pNewTask);
		void RecordQuitTaskEvent(const aiTask* pTask);
		void RecordSetTask(const aiTask &rTask, int iPriority);
		void RecordAddSubTask(const aiTask &rTask, const aiTask &rSubTask);
		void RecordStateChange(const aiTask &rTask, int iPreviousState, int iState);
		void UpdateTaskTransitionHighLevelGate();
	}
#endif // DR_ENABLED

} // namespace rage

#endif // AI_TASKSPEW_H
