//
// ai/task.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/task/task.h"
#include "ai/debugstatehistory.h"

// Framework headers
#include "ai/aioptimisations.h"
#include "ai/task/taskstatetransition.h"
#include "fwmaths/random.h"
#include "fwsys/timer.h"
#include "system/stack.h"

FW_AI_OPTIMISATIONS()

#if !__FINAL
#include "entity/entity.h"
#endif

namespace rage
{

#if !__FINAL
bool aiTask::sm_bDRRecordTaskTransitions=false;
#endif

float aiTask::sm_TimeStep = 0.0f;
float aiTask::sm_TimeStepInMilliSeconds = 0.0f;

////////////////////////////////////////////////////////////////////////////////

aiTask::aiTask()
: m_pEntity(NULL)
, m_pParent(NULL)
, m_pSubTask(NULL)
, m_pNewTask(NULL)
, m_Flags(aiTaskFlags::CurrentStateNotYetEntered, aiTaskFlags::SkipSetAnimationFlags)
, m_State(0)
, m_PreviousState(0)
, m_TimeInState(0.0f)
, m_TimeRunning(0.0f)
, m_TaskType(-1)
#if TASK_DETAILED_INFINITE_LOOP_TRAP
, m_pSetStateData(NULL)
, m_TaskRecordingFlags(0)
#endif // TASK_DETAILED_INFINITE_LOOP_TRAP
#if __ASSERT
, m_CanChangeState(true)
#endif // __ASSERT
{
#if ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
	if (aiTaskStateHistoryManager::GetInstance())
	{
		aiTaskStateHistoryManager::GetInstance()->AddStateHistory(this);
	}
#endif // ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
}

////////////////////////////////////////////////////////////////////////////////

aiTask::~aiTask()
{
	SetCallstackRecordingFlagState(0);
	if(m_pSubTask)
	{
		delete m_pSubTask;
		m_pSubTask = NULL;
	}
	if(m_pNewTask)
	{
		delete m_pNewTask;
		m_pNewTask = NULL;
	}

#if ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
	if (aiTaskStateHistoryManager::GetInstance())
	{
		aiTaskStateHistoryManager::GetInstance()->RemoveStateHistory(this);
	}
#endif // ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
}


////////////////////////////////////////////////////////////////////////////////

bool aiTask::MakeAbortable(const AbortPriority priority, const aiEvent* pEvent)
{
	// Are we aborting?
	bool bAbort = true;

	// Task already finished, no need to abort
	if(!m_Flags.IsFlagSet(aiTaskFlags::HasFinished))
	{
		aiTask* pTask = this;
		while(pTask && bAbort)
		{
			if(!pTask->ShouldAbort(priority, pEvent))
			{
				bAbort = false;
			}

			pTask = pTask->GetSubTask();
		}

		if(bAbort)
		{
			// Clean up FSM and return true
			pTask = this;

			// Some variables used to keep track of what we can delete, if anything.
			aiTask* pTaskOwner = NULL;
			aiTask* pFirstTaskToDelete = NULL;
			aiTask* pFirstTaskOwner = NULL;

			while(pTask)
			{
				// Ensure we are not already aborting
				taskAssert(!pTask->m_Flags.IsFlagSet(aiTaskFlags::InMakeAbortable));

				// MakeAbortable guard
				pTask->m_Flags.SetFlag(aiTaskFlags::InMakeAbortable);

				pTask->m_Flags.SetFlag(aiTaskFlags::IsAborted);
				pTask->UpdateFSM(pTask->GetState(), OnExit);
				pTask->DoAbort(priority, pEvent);
				pTask->CleanUp();

				// Validate state changes for aborting
				ASSERT_ONLY(pTask->m_CanChangeState = true);
				//const int oldState = pTask->GetState();
				const int newState = pTask->GetDefaultStateAfterAbort();
				pTask->SetState(newState);
				ASSERT_ONLY(pTask->m_CanChangeState = false);

				pTask->m_Flags.SetFlag(aiTaskFlags::RestartStateOnResume);

				aiTask* pSubTask = pTask->GetSubTask();

				// If we are not a top level task, and we haven't already decided to delete the
				// whole branch of the task tree, check if we want to be deleted if aborted,
				// and if our owner allows it.
				if(pTaskOwner && !pFirstTaskToDelete && pTask->MayDeleteOnAbort() && pTaskOwner->MayDeleteSubTaskOnAbort())
				{
					//taskDisplayf("%d: Deleting subtask %s (%p) after aborting parent (%s, %p), (ped %p), MayDeleteOnAbort() returned true.", TIME.GetFrameCount(), pTask->GetTaskName(), (void*)pTask, pTaskOwner->GetTaskName(), (void*)pTaskOwner, (void*)pTaskOwner->GetEntity());

					// Store the task to delete.
					pFirstTaskToDelete = pTask;
					pFirstTaskOwner = pTaskOwner;
				}

				// If we haven't decided to delete anything yet, and we have a subtask, and the task says
				// that it's OK to delete the subtask, store this as the top-most task we will delete.
				if(!pFirstTaskToDelete && pSubTask && pTask->ShouldAlwaysDeleteSubTaskOnAbort())
				{
					//taskDisplayf("%d: Deleting subtask %s (%p) after aborting parent (%s, %p), (ped %p), ShouldAlwaysDeleteSubTaskOnAbort() returned true, states were %s vs. %s.", TIME.GetFrameCount(), pSubTask->GetTaskName(), (void*)pSubTask, pTask->GetTaskName(), (void*)pTask, (void*)pTask->GetEntity(),
					//		pTask->GetStateName(oldState), pTask->GetStateName(newState));

					pFirstTaskToDelete = pSubTask;
					pFirstTaskOwner = pTask;
				}

				// MakeAbortable guard
				pTask->m_Flags.ClearFlag(aiTaskFlags::InMakeAbortable);

				pTaskOwner = pTask;
				pTask = pSubTask;
			}

			// If we found a task to delete, do so. This should also kill all the subtasks
			// in that branch of the tree.
			if(pFirstTaskToDelete)
			{
				taskAssert(pFirstTaskToDelete != this);
				aiTask* pTaskToDelete = pFirstTaskOwner->RelinquishSubTask();
				taskAssert(pTaskToDelete == pFirstTaskToDelete);
				delete pTaskToDelete;
			}
		}
	}

	return bAbort;
}

////////////////////////////////////////////////////////////////////////////////

bool aiTask::RequestTermination()
{
	if(SupportsTerminationRequest())
	{
		m_Flags.SetFlag(aiTaskFlags::TerminationRequested);
		return true;
	}
	else
	{
		taskErrorf("Task (%s) doesn't support clean termination", GetName().c_str());
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTask::FindParentTaskOfType(const s32 iType) const
{
	if(m_pParent)
	{
		if(m_pParent->GetTaskType() == iType)
		{
			return m_pParent;
		}

		return m_pParent->FindParentTaskOfType(iType);
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTask::FindSubTaskOfType(const s32 iType) const
{
	if(m_pSubTask)
	{
		if(m_pSubTask->GetTaskType() == iType)
		{
			return m_pSubTask;
		}

		return m_pSubTask->FindSubTaskOfType(iType);
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

s32 aiTask::FindNewStateTransition(s32 iStateIfNonFound, bool bStateEnded)
{
	taskFatalAssertf(GetTransitionTableData(), "Task (%s) doesn't have associated transition table data!", GetName().c_str());
	// Pick the transition set to be used
	aiTaskStateTransitionTable* pTransitionSet = GetTransitionSet();

	// If the transition set changes, restart from the beginning
	s32 iTransitionStartState = GetState();
	if(GetTransitionTableData()->GetTransitionSet() != pTransitionSet)
	{
		iTransitionStartState = 0; // Starting state
		GetTransitionTableData()->SetTransitionSet(pTransitionSet);
	}

	// Generate conditional flags
	s64 iConditionFlags = GenerateTransitionConditionFlags(bStateEnded);

	// Check for transitions
	s32 iState = GetTransitionTableData()->GetTransitionSet()->CheckTransitions(GetEntity(), iTransitionStartState, iConditionFlags);

	// If none are found, use the state passed in
	if(iState == -1)
	{
		iState = iStateIfNonFound;
	}

	// A transition has been found, return the state
	return iState;
}

////////////////////////////////////////////////////////////////////////////////

bool aiTask::PickNewStateTransition(s32 iStateIfNonFound, bool bStateEnded)
{
	s32 iState = FindNewStateTransition(iStateIfNonFound, bStateEnded);

	// A transition has been found, set the state and return
	if(iState != GetState())
	{
		SetState(iState);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool aiTask::PeriodicallyCheckForStateTransitions(float fAverageTimeBetweenChecks)
{
	taskFatalAssertf(GetTransitionTableData(), "Task (%s) doesn't have associated transition table data!", GetName().c_str());
	bool bCheckThisFrame = false;

	// Check for conditions changing across frames
	s64 iConditionalFlags = GenerateTransitionConditionFlags(false);

	// Initialise the search timer if it has not yet been set
	if(GetTransitionTableData()->GetLastStateSearchTimer() <= 0.0f)
	{
		GetTransitionTableData()->SetLastStateSearchTimer(fwRandom::GetRandomNumberInRange(0.8f, 1.2f) * fAverageTimeBetweenChecks);
	}

	// Count down the timer
	GetTransitionTableData()->SetLastStateSearchTimer(GetTransitionTableData()->GetLastStateSearchTimer()-GetTimeStep());

	// If the timer is exhausted, check for a new state
	if(GetTransitionTableData()->GetLastStateSearchTimer() <= 0.0f)
	{
		bCheckThisFrame = true;
	}
	// If the conditions have changed, force a state check this frame
	else if(GetTransitionTableData()->GetLastStateFlags() != iConditionalFlags && GetTransitionTableData()->GetLastStateFlags() != 0)
	{
		bCheckThisFrame = true;
	}

	// Store this frames flags to be checked next frame
	GetTransitionTableData()->SetLastStateFlags(iConditionalFlags);

	if(bCheckThisFrame)
	{
		GetTransitionTableData()->SetLastStateSearchTimer(fwRandom::GetRandomNumberInRange(0.8f, 1.2f) * fAverageTimeBetweenChecks);
		return PickNewStateTransition(GetState(), false);
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::SetState(const s32 iState)
{
	taskAssert(m_CanChangeState && iState <= 0xFF);

	if (iState != m_State)
	{
		m_Flags.SetFlag(aiTaskFlags::CurrentStateNotYetEntered);
	}

	m_TimeInState = 0.0f;
	m_PreviousState = m_State;
	m_State = iState;
	
#if DR_ENABLED
	if (sm_bDRRecordTaskTransitions)
	{
		debugPlayback::RecordStateChange(*this, m_PreviousState, m_State);
	}
#endif
	// Reset variables for tasks using periodically check for state changes
	if(GetTransitionTableData())
	{
		GetTransitionTableData()->SetLastStateFlags(0);
		GetTransitionTableData()->SetLastStateSearchTimer(0.0f);
	}
#if TASK_DETAILED_INFINITE_LOOP_TRAP

	// At end of the day, this controls whether we can actually record.
	// The flags may be set, but there might not have been enough records for
	// example
	if ( m_pSetStateData )
	{
		AddSetStateCallstack();
		// Are we simply spewing?
	}
#endif // TASK_DETAILED_INFINITE_LOOP_TRAP

#if ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
	if (aiTaskStateHistoryManager::GetInstance())
	{
		aiTaskStateHistoryManager::GetInstance()->AddStateToHistory(this, iState);
	}
#endif // ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
}


////////////////////////////////////////////////////////////////////////////////

void aiTask::SetNewTask(aiTask* pNewTask)
{
	DR_ONLY(debugPlayback::RecordNewTaskEvent(this, pNewTask));
	if(m_pNewTask)
	{
		delete m_pNewTask;
	}

	m_pNewTask = pNewTask;
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::SetSubTask(aiTask* pSubTask)
{
	if(m_pSubTask == pSubTask)
	{
		return;
	}

	if(m_pSubTask)
	{
		taskAssert(m_pSubTask->GetSubTask() == NULL);
		delete m_pSubTask;
	}

	m_pSubTask = pSubTask;

	if(m_pSubTask)
	{
		m_pSubTask->SetParent(this);
#if TASK_DETAILED_INFINITE_LOOP_TRAP
		// If we should record our subtask
		if ( GetTaskRecordingFlags() & aiTask::TRF_MaskShouldRecordSubTask )
		{
			// Get it to start spewing.
			m_pSubTask->SetCallstackRecordingFlag(aiTask::TRF_ToldToSpew);
		}
#endif
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::DeleteSubTask()
{
	if(m_pSubTask)
	{
		aiAssert(m_pSubTask->GetIsFlagSet(aiTaskFlags::IsAborted)||m_pSubTask->GetIsFlagSet(aiTaskFlags::HasFinished));
		m_pSubTask->DeleteSubTask();
		delete m_pSubTask;
		m_pSubTask = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTask::FSM_Return aiTask::ProcessFSM( const s32 iState, const FSM_Event iEvent )
{
	return UpdateFSM(iState, iEvent);
}

////////////////////////////////////////////////////////////////////////////////

#if __DEV // make this non-inline in __DEV to reduce code bloat created by the assert being inlined
s32 aiTask::GetTaskType() const
{	
	taskAssertf(m_TaskType != -1, "Task type has not been set. Please use SetInternalTaskType in the constructor for task %s", GetName().c_str());
	taskAssertf(m_TaskType == GetTaskTypeInternal(), "Task type set incorrectly: %u != %u. Please use SetInternalTaskType in the constructor for task %u", (u32)m_TaskType, (u32)GetTaskTypeInternal(), (u32)GetTaskTypeInternal());
	return m_TaskType;
}
#endif // Dev

////////////////////////////////////////////////////////////////////////////////

#if TASK_DETAILED_INFINITE_LOOP_TRAP

void aiTask::DebugIndicateAContinueFSMPoint( /*FSM_Return val*/)
{
	if ( m_pSetStateData )
	{
		fwDebugStateHistory &store = *m_pSetStateData;
		// Just mark that we are staying in the same state
		store.Stamp( m_State, m_State, 2, 0 );

		// Spewing?
		if ( m_TaskRecordingFlags & TRF_MaskSpew )
		{
			// Just print out that element of the store
			PrintSetStateCallstacks( store.GetLast(), 1, fwTimer::GetTimeInMilliseconds());
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::UpdateFromCallstackRecordingFlags()
{
	if ( m_TaskRecordingFlags )
	{
		// Weren't before?
		if ( !m_pSetStateData )
		{
			// Start now
			m_pSetStateData = rage_new fwDebugStateHistory();
		}
	}
	else
	{
		// Just get rid, if we have one as we are not needed anymore..
		if ( m_pSetStateData )
		{
			delete m_pSetStateData;
			m_pSetStateData = NULL;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::SetCallstackRecordingFlag( eTaskRecordingFlags taskRecordingFlag )
{
	m_TaskRecordingFlags |= taskRecordingFlag;
	UpdateFromCallstackRecordingFlags();
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::ClearCallstackRecordingFlag( eTaskRecordingFlags taskRecordingFlag )
{
	m_TaskRecordingFlags &=~taskRecordingFlag;
	UpdateFromCallstackRecordingFlags();
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::SetCallstackRecordingFlagState( u32 state )
{
	Assign( m_TaskRecordingFlags, state );
	UpdateFromCallstackRecordingFlags();
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::AddSetStateCallstack() const
{
	fwDebugStateHistory &store = *m_pSetStateData;
	// First point of call - get rid of any continue states, because they will have
	// been legitimate
	while ( store.GetCount() )
	{
		const fwDebugStateHistory::cStateRecord &rec = store.GetData(store.GetLast());
		if ( rec.m_NewState == rec.m_OldState )
		{
			store.Pop();
		}
		else
		{
			break;
		}
	}

	// Not us, not the actual SetState, the thing before [2]
	store.Stamp( m_PreviousState, m_State, 2, 0);

	// Just spewing?
	if ( m_TaskRecordingFlags & TRF_MaskSpew )
	{
		// Just print out that element of the store
		PrintSetStateCallstacks( store.GetLast(), 1, fwTimer::GetTimeInMilliseconds());
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::PrintSetStateCallstacks() const
{
	if ( !m_pSetStateData )
	{
		diagLoggedPrintf( "SetState history record looks to have not been allocated.\n");
		return;
	}
	fwDebugStateHistory &store = *m_pSetStateData;
	int count = store.GetCount();
	if ( count == 0 )
	{
		diagLoggedPrintf( "No SetState history.  We are almost certainly looping on the same state (%s).\n",
			GetStateName(GetState()));
		return;
	}

	PrintSetStateCallstacks( store.GetRingStart(), count, 0 );
}

////////////////////////////////////////////////////////////////////////////////

void aiTask::PrintSetStateCallstacks( int current, int count, rage::u32 time ) const
{
	fwDebugStateHistory &store = *m_pSetStateData;
	// F - focus entity spew
	// I - infinite loop
	// I+ - subtask spew from an infinite loop situation
	static const char *flagIndicator[] =
	{
		"      ",
		"  I   ",
		"F:    ",
		"F:I   ",
		"    I+",
		"  I I+",
		"F:  I+",
		"F:I I+"
	};
	char buffer[512];
	char newbuffer[512];
	for ( int i = 0; i < count; ++i )
	{
		const fwDebugStateHistory::cStateRecord &record = store.GetData(current);
#if __ASSERT
		sysStack::PrintRegisteredBacktrace( static_cast<u16>(record.m_Callstack) );
#endif

		if ( count != 1 )
		{
			sprintf( buffer, "  [  %d ]", i );
		}
		else
		{
			sprintf( buffer, "  <spew>");
		}

		sprintf( newbuffer, "%s <%s> %s: %s", buffer, flagIndicator[m_TaskRecordingFlags], 
											   GetName().c_str(), GetStateName(record.m_OldState) );
		if ( record.m_OldState != record.m_NewState )
		{
			sprintf( buffer, "%s -> %s", newbuffer, GetStateName(record.m_NewState ) );
		}
		else
		{
			sprintf( buffer, "%s (continue FSM)", newbuffer );
		}
		if ( time )
		{
			diagLoggedPrintf( "%s (@ %i msec)\n", buffer, time );
		}
		else
		{
			diagLoggedPrintf( "%s\n", buffer );
		}
		++current;
	}
}

#endif // TASK_DETAILED_INFINITE_LOOP_TRAP

////////////////////////////////////////////////////////////////////////////////

#if ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG

aiTaskStateHistory::aiTaskStateHistory(const aiTask* pTask)
: m_pAssociatedTask(pTask)
, m_iFreeSlotStateIndex(0)
{
	for (s32 i=0; i<MAX_TASK_STATES; ++i)
	{
		m_aPreviousStates[i] = TASK_STATE_INVALID;
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateHistory::~aiTaskStateHistory()
{

}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateHistory::AddState(s32 iState)
{
	m_aPreviousStateFrame[m_iFreeSlotStateIndex] = fwTimer::GetFrameCount();
	m_aPreviousStates[m_iFreeSlotStateIndex++] = (s8)iState;

	// Wrap round once we've filled up the array
	if (m_iFreeSlotStateIndex == MAX_TASK_STATES)
	{
		m_iFreeSlotStateIndex = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateHistoryManager* aiTaskStateHistoryManager::ms_pTaskStateHistoryMgr = NULL;

////////////////////////////////////////////////////////////////////////////////

aiTaskStateHistoryManager* aiTaskStateHistoryManager::GetInstance()
{
	if (!ms_pTaskStateHistoryMgr)
	{
		// Allocate from debug memory
		USE_DEBUG_MEMORY();
		ms_pTaskStateHistoryMgr = rage_new aiTaskStateHistoryManager();
	}

	return ms_pTaskStateHistoryMgr;
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateHistoryManager::aiTaskStateHistoryManager()
{
	for (s32 i=0; i<MAX_TASK_STATE_HISTORIES; ++i)
	{
		m_apStateHistoriesList[i] = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateHistoryManager::~aiTaskStateHistoryManager()
{
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateHistoryManager::AddStateHistory(const aiTask* pTask)
{
	// Allocate from debug memory
	USE_DEBUG_MEMORY();

	// Find the first available slot which is empty to assign to
	s16 iCurrentIndex = 0;
	while (iCurrentIndex < MAX_TASK_STATE_HISTORIES-1)
	{
		if (m_apStateHistoriesList[iCurrentIndex] == NULL)
		{
			aiTaskStateHistory* pTaskStateHistory = rage_new aiTaskStateHistory(pTask);
			m_apStateHistoriesList[iCurrentIndex] = pTaskStateHistory;
			break;
		}
		else
		{
			++iCurrentIndex;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateHistoryManager::RemoveStateHistory(const aiTask* pTask)
{
	s32 iTaskStateIndex = FindTaskStateHistoryIndexForTask(pTask);

	if (iTaskStateIndex > -1)
	{
		delete m_apStateHistoriesList[iTaskStateIndex];
		m_apStateHistoriesList[iTaskStateIndex] = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskStateHistoryManager::AddStateToHistory(const aiTask* pTask, s32 iState)
{
	s32 iTaskStateIndex = FindTaskStateHistoryIndexForTask(pTask);

	if (iTaskStateIndex > -1)
	{
		m_apStateHistoriesList[iTaskStateIndex]->AddState(iState);
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTaskStateHistory* aiTaskStateHistoryManager::GetTaskStateHistoryForTask(const aiTask* pTask)
{
	s32 iTaskStateIndex = FindTaskStateHistoryIndexForTask(pTask);

	if (iTaskStateIndex > -1)
	{
		return m_apStateHistoriesList[iTaskStateIndex];
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

s32 aiTaskStateHistoryManager::FindTaskStateHistoryIndexForTask(const aiTask* pTask)
{
	for (s32 i=0; i<MAX_TASK_STATE_HISTORIES; ++i)
	{
		if (m_apStateHistoriesList[i] && m_apStateHistoriesList[i]->GetAssociatedTask() == pTask)
		{
			return i;
		}
	}
	return -1;
}
#endif // ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
