//
// ai/taskchannel.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/task/taskchannel.h"

// Rage headers
#include "system/param.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ai, task)

} // namespace rage
