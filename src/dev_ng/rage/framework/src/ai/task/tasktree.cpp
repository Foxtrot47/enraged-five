//
// ai/tasktree.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/task/tasktree.h"
#include "ai/task/taskspew.h"

// Framework headers
#include "ai/aioptimisations.h"
#include "ai/task/task.h"
#include "ai/debugstatehistory.h"
#if !__FINAL
#include "entity/entity.h"
#endif // !__FINAL
#include "fwsys/timer.h"

FW_AI_OPTIMISATIONS()

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

	// Static Initialisation
	bool			aiTaskTree::sm_AllowDelete			= true;
	atFixedArray<aiTask*, aiTaskTree::TASK_DELETION_QUEUE_SIZE> aiTaskTree::sm_TaskDeletionQueue;
#if !__FINAL
	bool			aiTaskTree::ms_bSpewStateTransToTTY = false;
	fwEntity*		aiTaskTree::ms_pFocusEntity			= NULL;
#endif

////////////////////////////////////////////////////////////////////////////////

aiTaskTree::aiTaskTree(fwEntity* pEntity, const s32 iNumberOfPriorities)
: m_pEntity(pEntity)
, m_iActivePriority(-1)
, m_iPriorityBeingProcessed(-1)
{
	m_Tasks.Resize(iNumberOfPriorities);

	for(s32 i = 0; i < iNumberOfPriorities; i++)
	{
		m_Tasks[i] = NULL;
	}

#if !__FINAL
	for(s32 i = 0; i < MAX_TASK_HISTORY; i++)
	{
		m_TaskHistory[i] = -1;
	}
#endif // !__FINAL
}

////////////////////////////////////////////////////////////////////////////////

aiTaskTree::~aiTaskTree()
{
	for(s32 i = 0; i < m_Tasks.GetCount(); i++)
	{
		if(m_Tasks[i])
		{
			DeleteTask(m_Tasks[i]);
		}

		m_Tasks[i] = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::AbortTasksAbovePriority(const s32 iPriority)
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());

	for(int i = m_iActivePriority; i < iPriority; i++)
	{
		AbortTasksWithPriority(i);
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::SetTask(aiTask* pTask, const s32 iPriority, const bool UNUSED_PARAM(bForceNewTask))
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());
	
#if !__FINAL
    if(m_iPriorityBeingProcessed == iPriority)
    {
        SpewAllTasks(aiTaskSpew::SPEW_STD);
    }
#endif // !__FINAL

    taskFatalAssertf(m_iPriorityBeingProcessed != iPriority, "About to delete the task that is currently being processed (%i)!", m_iPriorityBeingProcessed);

#if !__FINAL 
	if(pTask)
	{
		for(s32 i = MAX_TASK_HISTORY-1; i > 0; i--)
		{
			m_TaskHistory[i] = m_TaskHistory[i-1];
		}

		m_TaskHistory[0] = pTask->GetTaskType();

// #if AI_OPTIMISATIONS_OFF
// 		Assertf(pTask == NULL || CTaskNames::TaskNameExists(pTask->GetTaskType()), "Task (%s) doesn't have a definition", pTask->GetName().c_str());
// #endif
	}
#endif // !__FINAL

// #if __ASSERT
// 	if(pTask && pTask->IsMoveTask())
// 	{
// 		//TODO
// 		//m_pEntity->GetPedIntelligence()->PrintTasks();
// 		Assertf(!pTask || !pTask->IsMoveTask(), "Move task %s is being added to the main task tree! Please include the debug spew with the bug report, it will help us find out how this happened.", (const char*)pTask->GetName());
// 	}
// #endif

	// Make sure any lower priority tasks are aborted:
	for(s32 i = iPriority; i < GetPriorityCount(); i++)
	{
		//! Catch MakeAbortable() modifying task tree.
		s32 iPrevPriorityBeingProcessed = m_iPriorityBeingProcessed;
		if(m_iPriorityBeingProcessed == -1)
		{
			m_iPriorityBeingProcessed = iPriority;
		}

//		CEventNewTask event(pTask, iPriority);
		if(m_Tasks[i] && !m_Tasks[i]->GetIsFlagSet(aiTaskFlags::IsAborted))
		{
			if(!m_Tasks[i]->MakeAbortable(aiTask::ABORT_PRIORITY_URGENT, /*&event*/NULL))
			{
				m_Tasks[i]->MakeAbortable(aiTask::ABORT_PRIORITY_IMMEDIATE, /*&event*/NULL);
			}
			taskAssertf(m_Tasks[i]->GetIsFlagSet(aiTaskFlags::IsAborted) || m_Tasks[i]->GetIsFlagSet(aiTaskFlags::HasFinished), "Task %s not aborted before a new one is added!", m_Tasks[i]->GetTaskName());
		}

		m_iPriorityBeingProcessed = iPrevPriorityBeingProcessed;
	}

	// If the new task is a null task then delete the current task and set the current task to null.
	if(!pTask && m_Tasks[iPriority])
	{
		DeleteTask(m_Tasks[iPriority]);
		m_Tasks[iPriority] = NULL;
		return;
	}

	// Don't set the same task.
	if(m_Tasks[iPriority] == pTask)
	{
		return;
	} 

	if(m_Tasks[iPriority])
	{
		DeleteTask(m_Tasks[iPriority]);
	}

	m_Tasks[iPriority] = pTask;
	m_Tasks[iPriority]->SetEntity(m_pEntity);

	// Ensure the active priority is up to date
	m_iActivePriority = ComputeActiveTaskPriority();

	//Uses data from SetEntity(m_pEntity) call above
#if DR_ENABLED
	if (aiTask::sm_bDRRecordTaskTransitions && pTask)
	{
		debugPlayback::RecordSetTask(*pTask, iPriority);
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTaskTree::GetLeafTask(const s32 iPriority) const
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());

	aiTask* pSimplestTask = NULL;

	aiTask* pTask = m_Tasks[iPriority];
	while(pTask)
	{
		pSimplestTask = pTask;
		pTask = pTask->GetSubTask();
	}

	return pSimplestTask;
}

////////////////////////////////////////////////////////////////////////////////

bool aiTaskTree::ClearTask(const s32 iPriority)
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());

	if(m_Tasks[iPriority])
	{
		DR_ONLY(debugPlayback::RecordTaskEvent(m_Tasks[iPriority], "ClearTask"));
		SetTask(NULL, iPriority);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTaskTree::GetActiveLeafTask() const
{
	s32 iPriority = GetActiveTaskPriority();
	if(iPriority > -1)
	{
		return GetLeafTask(iPriority);
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

s32 aiTaskTree::ComputeActiveTaskPriority() const
{
	for(s32 i = 0; i < m_Tasks.GetCount(); i++)
	{
		if(m_Tasks[i])
		{   
			return i;
		}
	}

	return -1;
}

////////////////////////////////////////////////////////////////////////////////

bool aiTaskTree::GetHasTask(const aiTask* pTask) const
{
	for(s32 i = 0; i < m_Tasks.GetCount(); i++)
	{
		if(pTask == m_Tasks[i])
		{
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::AbortTasksWithPriority(const s32 iPriority)
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());
	s32 iPrevPriority = m_iPriorityBeingProcessed;
	m_iPriorityBeingProcessed = iPriority;
	if(m_Tasks[iPriority])
	{
		if(m_Tasks[iPriority]->GetIsFlagSet( aiTaskFlags::IsAborted ) || m_Tasks[iPriority]->MakeAbortable(aiTask::ABORT_PRIORITY_IMMEDIATE, NULL))
		{
			DeleteTask(m_Tasks[iPriority]);
			m_Tasks[iPriority] = NULL;
		}
#if !__FINAL    		
		else
		{
			taskAssertf(0, "Failed to abort task (%s) immediately\n", m_Tasks[iPriority]->GetName().c_str());
			SpewTaskHierarchy(m_Tasks[iPriority], aiTaskSpew::SPEW_ASSERT);
		}
#endif // !__FINAL
	}
	m_iPriorityBeingProcessed = iPrevPriority;
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::AbortTasks()
{
	for(s32 i = 0; i < m_Tasks.GetCount(); i++)
	{
		AbortTasksWithPriority(i);
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTaskTree::FindTaskByTypeActive(const s32 iTaskType) const
{
	s32 iActivePriority = GetActiveTaskPriority();
	if(iActivePriority != -1)
	{
		return FindTaskByTypeWithPriority(iTaskType, iActivePriority);
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTaskTree::FindTaskByTypeWithAnyPriority(const s32 iTaskType) const
{
	for(s32 i = 0; i < m_Tasks.GetCount(); i++)
	{
		aiTask* pTask = FindTaskByTypeWithPriority(iTaskType, i);
		if(pTask)
		{
			return pTask;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

aiTask* aiTaskTree::FindTaskByTypeWithPriority(const s32 iTaskType, const s32 iPriority) const
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());

	// Scan the task of specified priority.
	aiTask* pTask = m_Tasks[iPriority];
	while(pTask)
	{
		if(pTask->GetTaskType() == iTaskType)
		{
			return pTask;
		}

		pTask = pTask->GetSubTask();
	}

	return NULL;
}

#if !__FINAL

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::SpewAllTasks(aiTaskSpew::SpewType type) const
{
	for(s32 i = 0; i < m_Tasks.GetCount(); i++)
	{
        taskDisplayf("Task priority: %d", i);

		SpewTaskHierarchy(m_Tasks[i], type);
	}
}

#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::UpdateTasks(const bool bDontDeleteHeadTask, const s32 iDefaultTaskPriority, float timeStep)
{
	aiTask::BeginTaskUpdates(timeStep);

	m_iDeadman = 1000;

	// Compute the priority of the main task to process.      
	s32 iPriority = GetActiveTaskPriority();

	// Keep track of the current priority of task being processed.
	m_iPriorityBeingProcessed = iPriority;

	// Process the highest priority task
	if(iPriority > -1)
	{
 		aiTask* pCurrentTask = m_Tasks[iPriority];
		while(pCurrentTask)
		{
			UpdateResult result = UpdateTask(pCurrentTask, timeStep);

			if(result == UR_QUIT)
			{	
				// If the current task is the top-most parent task and it quits, we'll be left with no current task,
				// tasks can optionally force the next active task to update this frame
				const bool bProcessNextActiveTaskImmediately = pCurrentTask->GetIsFlagSet(aiTaskFlags::ProcessNextActiveTaskImmediately);

				// The movement tasks have to be kept for one frame, so don't delete them if requested not to
				if(bDontDeleteHeadTask == false)
				{
					DeleteTask(pCurrentTask);
				}

				// Create a new default task if the last one was just deleted.
				if(iPriority == iDefaultTaskPriority)
				{
					if(m_pEntity)
					{
						// Replace the default task
						SetTask(GetDefaultTask(), iDefaultTaskPriority);

						// Process the default task straight away
						pCurrentTask = m_Tasks[iDefaultTaskPriority];
					}
					else
					{
						pCurrentTask = NULL;
					}
				}
				else
				{
					// Process the next active task straight away after we've finished our current task if flagged to
					if(bProcessNextActiveTaskImmediately)
					{
						iPriority = GetActiveTaskPriority();
						pCurrentTask = m_Tasks[iPriority];
					}
					else
					{
						pCurrentTask = NULL;
					}
				}
			}
			else
			{
				pCurrentTask = NULL;
			}
		}
	}

	// Reset the priority being processed
	m_iPriorityBeingProcessed = -1;

	aiTask::EndTaskUpdates();
}

////////////////////////////////////////////////////////////////////////////////

aiTaskTree::UpdateResult aiTaskTree::UpdateTask(aiTask* pTask, float timeStep)
{
#if TASK_DETAILED_INFINITE_LOOP_TRAP
	u32 taskRecordingFlags(0);

	// If we are to spew
	if (ms_bSpewStateTransToTTY)
	{
		// And we are the focus entity
		if(ms_pFocusEntity == m_pEntity )
		{
			// Set us again
			taskRecordingFlags |= aiTask::TRF_FocusEntitySpew;
		}
	}


	// Have we got a parent?
	if ( pTask->GetParent() )
	{
		// Is it saying we should record sub tasks?
		if ( pTask->GetParent()->GetTaskRecordingFlags() & aiTask::TRF_MaskShouldRecordSubTask )
		{
			// Do this early for benefit ProcessPreFSM
			taskRecordingFlags |= aiTask::TRF_ToldToSpew;
		}
	}

	// Set this here early...
	NOTFINAL_ONLY(pTask->SetCallstackRecordingFlagState( taskRecordingFlags ));
#endif

	// As soon as the task is updated for the first time, block any state chnages.
	ASSERT_ONLY(pTask->m_CanChangeState = false);

	// Catch for a task infinite recursive loop
	--m_iDeadman;
	taskAssertf(m_iDeadman, "aiTaskTree::UpdateTasks got stuck after 1000 iterations.");
	if(m_iDeadman <= 0)
	{
#if !__FINAL
		static bool SPEWED = false;
		if(!SPEWED)
		{
			taskDisplayf("Task stuck processing task... ");
			SpewTaskHierarchy(pTask, aiTaskSpew::SPEW_OVERFLOW);
			taskDisplayf("Task stuck entire tree... (priority %d)", m_iPriorityBeingProcessed);
			if(m_iPriorityBeingProcessed == -1)
			{
				taskDisplayf("Lost active priority being processed, printing all task trees.. (priority %d)", m_iPriorityBeingProcessed);
				for(int i = 0; i < kMaxPriorities; ++i)
				{
					taskDisplayf("Task Tree (priority %d)", i);
					if(m_Tasks[i])
					{
						SpewTaskHierarchy(m_Tasks[i], aiTaskSpew::SPEW_OVERFLOW);
					}
				}
			}
			else
			{
				SpewTaskHierarchy(m_Tasks[m_iPriorityBeingProcessed], aiTaskSpew::SPEW_OVERFLOW);
			}
			SPEWED = true;
		}
#endif //!__FINAL
		return UR_CONTINUED;
	}

	// Process Pre FSM
	if(Likely(!pTask->m_Flags.IsFlagSet(aiTaskFlags::SkipProcessPreFSM)))
	{
		if(pTask->ProcessPreFSM() == aiTask::FSM_Quit)
		{
			pTask->m_Flags.SetFlag(aiTaskFlags::HasFinished);
			return UR_QUIT;
		}
	}

	// Increase the time spent in the task
	pTask->m_TimeRunning += timeStep;

	if(Likely(!pTask->m_Flags.IsFlagSet(aiTaskFlags::SkipSetAnimationFlags)))
	{
		pTask->FSM_SetAnimationFlags();
	}

	bool bContinueFsmUpdate				= true;
	bool bUpdateSubtasks				= false;
	bool bQuit							= false;
	s32 iLoopCount = 0;

	// Reset the maintain task flag
	if(pTask->m_Flags.IsFlagSet((aiTaskFlags::AdjustableFlags)(aiTaskFlags::KeepCurrentSubtaskAcrossTransition | aiTaskFlags::RestartCurrentState)))
	{
		pTask->m_Flags.ClearFlag((aiTaskFlags::AdjustableFlags)(aiTaskFlags::KeepCurrentSubtaskAcrossTransition | aiTaskFlags::RestartCurrentState));
	}

	// Increase the time spent in the current state
	pTask->m_TimeInState += timeStep;

	// Call the main update function
	while(bContinueFsmUpdate)
	{
		// By default, don't repeat the FSM update, this will be set below if the state changes
		bContinueFsmUpdate = false;

		// Keep track of any state changes
		s32 iOldState = pTask->GetState();

		// Tell the task about it's recording status
#if TASK_DETAILED_INFINITE_LOOP_TRAP
		NOTFINAL_ONLY(pTask->SetCallstackRecordingFlagState( taskRecordingFlags ));
#endif

		// If the state is being restarted after being aborted, simply restart the state then continue as normal
		// OR if the state hasn't yet started, enter the state then continue as normal
		if(pTask->m_Flags.IsFlagSet(aiTaskFlags::RestartStateOnResume) || pTask->m_Flags.IsFlagSet(aiTaskFlags::CurrentStateNotYetEntered))
		{
			// Enter the new state, reset the task pointer in case it is non-null 
			pTask->SetNewTask(NULL);

			if(pTask->ProcessFSM(pTask->GetState(), aiTask::OnEnter) == aiTask::FSM_Quit)
			{
				// Exit the state
				pTask->ProcessFSM(pTask->GetState(), aiTask::OnExit);
				bQuit = true;
			}
			else
			{
				bContinueFsmUpdate = true;
				pTask->DebugIndicateAContinueFSMPoint();
			}

			pTask->m_Flags.ClearFlag(aiTaskFlags::CurrentStateNotYetEntered);
			pTask->m_Flags.ClearFlag(aiTaskFlags::RestartStateOnResume);
			pTask->m_Flags.ClearFlag(aiTaskFlags::IsAborted);
			pTask->m_Flags.SetFlag(aiTaskFlags::HasBegun);
		}
		else
		{
			Assertf(!pTask->m_Flags.IsFlagSet(aiTaskFlags::RestartCurrentState), "If encountered, please assign a bug to Dustin Russell.  Thanks!");
			pTask->m_Flags.ClearFlag(aiTaskFlags::RestartCurrentState);

			// Validate state changes while performing the OnUpdate of the current state
			ASSERT_ONLY(pTask->m_CanChangeState = true);
			aiTask::FSM_Return updateReturn = pTask->ProcessFSM(pTask->GetState(), aiTask::OnUpdate);
			ASSERT_ONLY(pTask->m_CanChangeState = false);

			// Reset the task flags after an update.
			if(pTask->m_Flags.IsFlagSet((aiTaskFlags::InformationFlags)(aiTaskFlags::SubTaskFinished | aiTaskFlags::AnimFinished)))
			{
				pTask->m_Flags.ClearFlag((aiTaskFlags::InformationFlags)(aiTaskFlags::SubTaskFinished | aiTaskFlags::AnimFinished));
			}

			// Quit immediately if returned from the update
			switch(updateReturn)
			{
			case aiTask::FSM_Continue:
				if(iOldState != pTask->GetState() || pTask->m_Flags.IsFlagSet(aiTaskFlags::RestartCurrentState))
				{
					// If restarting the current state, make sure the previous state is registered
					if( pTask->m_Flags.IsFlagSet(aiTaskFlags::RestartCurrentState) )
					{
						pTask->SetPreviousState(pTask->GetState());
					}
					// Exit the old state
					pTask->ProcessFSM(iOldState, aiTask::OnExit);
					if(!pTask->m_Flags.IsFlagSet(aiTaskFlags::KeepCurrentSubtaskAcrossTransition))
					{
						// Enter the new state, reset the task pointer in case it is non-null 
						pTask->SetNewTask(NULL);
					}

					// Explicitly reset the current state timer - it may have not been set using the restart flag
					pTask->m_TimeInState = 0.0f;

					if(pTask->ProcessFSM(pTask->GetState(), aiTask::OnEnter) == aiTask::FSM_Quit)
					{
						// Exit the state
						pTask->ProcessFSM(pTask->GetState(), aiTask::OnExit);
						bQuit = true;
					}
					else
					{
						bContinueFsmUpdate = true;
						pTask->DebugIndicateAContinueFSMPoint();
						// If a new task hasn't been specified and the flag isn't set to keep the currently
						// running subtask, flag to remove the subtask here
						if(pTask->GetNewTask() == NULL && !pTask->m_Flags.IsFlagSet(aiTaskFlags::KeepCurrentSubtaskAcrossTransition))
						{
							pTask->SetFlag(aiTaskFlags::RemoveSubtask);
						}
					}

					pTask->m_Flags.ClearFlag(aiTaskFlags::KeepCurrentSubtaskAcrossTransition);
					pTask->m_Flags.ClearFlag(aiTaskFlags::CurrentStateNotYetEntered);
				}
				else
				{
					// If this task has settled on a state, update the subtasks if they haven't already been updated.
					bUpdateSubtasks = true;
				}
				break;

			case aiTask::FSM_Quit:
				// Exit the old state
				pTask->ProcessFSM(pTask->GetState(), aiTask::OnExit);
				bQuit = true;
				break;
			}

			// Clear any used request to restart the current state
			if(pTask->m_Flags.IsFlagSet(aiTaskFlags::RestartCurrentState))
			{
				pTask->m_Flags.ClearFlag(aiTaskFlags::RestartCurrentState);
			}
		}

		// If a new task has been specified, replace the subtask here
		if(pTask->GetNewTask())
		{
			DR_ONLY(debugPlayback::RecordSwitchSubTask(*pTask, pTask->GetSubTask(), *pTask->GetNewTask()));
			// Abort the old task informing it of the new task, implemented at the game level
			if(pTask->GetSubTask() && !pTask->GetIsFlagSet(aiTaskFlags::HasFinished) && !pTask->GetSubTask()->GetIsFlagSet(aiTaskFlags::IsAborted))
			{
				pTask->FSM_AbortPreviousTask();
			}
			aiAssertf(pTask->GetNewTask() != pTask->GetSubTask(), "New subtask [%s] and current subtasks [%s] are the same!", pTask->GetNewTask()->GetName().c_str(), pTask->GetSubTask() ? pTask->GetSubTask()->GetName().c_str() : "null");
			DeleteTask(pTask->RelinquishSubTask());
			AddSubTask(pTask, pTask->RelinquishNewTask());
			pTask->ClearFlag(aiTaskFlags::RemoveSubtask);
            OnNewSubTaskCreated(pTask, pTask->GetSubTask());
		}
		// Remove the subtask if the flag is set, if the tasks are going to be updated with a new task
		// Allow the task to be removed below so it can be aborted properly with knowledge of the new task
		else if(pTask->GetIsFlagSet(aiTaskFlags::RemoveSubtask))
		{
			// If there is a new task, updating the subtasks should be called  below
			aiAssertf(pTask->GetNewTask() == NULL, "Unexpected new task!");
			if(pTask->m_pSubTask)
			{
				if(!pTask->m_pSubTask->MakeAbortable(aiTask::ABORT_PRIORITY_URGENT, NULL))
				{
					pTask->m_pSubTask->MakeAbortable(aiTask::ABORT_PRIORITY_IMMEDIATE, NULL);
				}
			}			
			DeleteTask(pTask->RelinquishSubTask());
			pTask->ClearFlag(aiTaskFlags::RemoveSubtask);
		}

		if(pTask->m_Flags.IsFlagSet(aiTaskFlags::SubTaskFinished))
		{
			pTask->m_Flags.ClearFlag(aiTaskFlags::SubTaskFinished);
		}

		// Update the subtasks, this is triggered after a new subtask has been specified or 
		// after this task has settles on a state
		if(bUpdateSubtasks)
		{
			bUpdateSubtasks = false;
			
			if(pTask->GetSubTask())
			{
				UpdateResult result = UpdateTask(pTask->GetSubTask(), timeStep);
				if(result == UR_QUIT)
				{
					pTask->m_Flags.SetFlag(aiTaskFlags::SubTaskFinished);
					pTask->m_Flags.SetFlag(aiTaskFlags::RemoveSubtask);
					bContinueFsmUpdate = true;
					pTask->DebugIndicateAContinueFSMPoint();
				}
			}
			if(pTask->m_Flags.IsFlagSet(aiTaskFlags::KeepCurrentSubtaskAcrossTransition))
			{
				pTask->m_Flags.ClearFlag(aiTaskFlags::KeepCurrentSubtaskAcrossTransition);
			}
		}

		static dev_s32 MAX_STATE_LOOPS = 30;
		++iLoopCount;

#if TASK_DETAILED_INFINITE_LOOP_TRAP
		static bool SPEWED = false;
		if(!SPEWED)
		{
			if(iLoopCount == MAX_STATE_LOOPS)
			{
				// Indicate we've potentially reached an infinite loop - keep on trying
				taskRecordingFlags |= aiTask::TRF_InfiniteLoop;
			}
			if(iLoopCount == MAX_STATE_LOOPS + fwDebugStateHistory::MAX_SIZE)
			{
				// Print, then get us out
				pTask->PrintSetStateCallstacks();
				taskRecordingFlags &= ~aiTask::TRF_InfiniteLoop;
			}
			if(iLoopCount >= MAX_STATE_LOOPS + fwDebugStateHistory::MAX_SIZE)
			{
				taskAssertf(0, "Possible state machine infinite loop detected in task (%s)!  "
					"Previous state: %s.  Loop counter is likely in a register ($r26;Beta PS3SNC;should = %d).  Set it to 0 to make another pass.", 
					pTask->GetName().c_str(), pTask->GetStateName(pTask->GetPreviousState()),MAX_STATE_LOOPS + fwDebugStateHistory::MAX_SIZE);
				SPEWED = true;
			}
		}
#endif // TASK_DETAILED_INFINITE_LOOP_TRAP

		if(iLoopCount >= MAX_STATE_LOOPS + fwDebugStateHistory::MAX_SIZE + 2)
		{
			break;
		}
	}

	// Reset the task flags after an update.
	if(pTask->m_Flags.IsFlagSet(aiTaskFlags::AnimFinished))
	{
		pTask->m_Flags.ClearFlag(aiTaskFlags::AnimFinished);
	}

	// If set to quit, quit immediately
	if(bQuit || pTask->GetIsFlagSet(aiTaskFlags::HasFinished))
	{
		pTask->m_Flags.SetFlag(aiTaskFlags::HasFinished);
		if(pTask->m_pSubTask && !pTask->m_pSubTask->GetIsFlagSet(aiTaskFlags::HasFinished) && !pTask->m_pSubTask->GetIsFlagSet(aiTaskFlags::IsAborted))
		{
			if(!pTask->m_pSubTask->MakeAbortable(aiTask::ABORT_PRIORITY_URGENT, NULL))
			{
				pTask->m_pSubTask->MakeAbortable(aiTask::ABORT_PRIORITY_IMMEDIATE, NULL);
			}
		}

		pTask->CleanUp();
		pTask->m_Flags.SetFlag(aiTaskFlags::HasFinished);
		return UR_QUIT;
	}

	aiAssertf(pTask->GetNewTask() == NULL, "Unexpected new task left dangling!");

	// Process post FSM
	if(Unlikely(!pTask->m_Flags.IsFlagSet(aiTaskFlags::SkipProcessPostFSM)))
	{
		if(pTask->ProcessPostFSM() == aiTask::FSM_Quit)
		{
			pTask->m_Flags.SetFlag(aiTaskFlags::HasFinished);
			return UR_QUIT;
		}
	}

	if (pTask->GetIsFlagSet(aiTaskFlags::RemoveSubtask) && m_pEntity)
	{
		taskAssertf(0, "pTask->IsFlagSet(aiTaskFlags::RemoveSubtask) not expected to be set for %s, entity 0x%p (%s)", pTask->GetTaskName(), m_pEntity, m_pEntity->GetModelName());
	}

	pTask->ClearFlag(aiTaskFlags::Internal_NewSubtask);

	return UR_CONTINUED;
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::AddSubTask(aiTask* pTask, aiTask* pSubTask)
{
#if DR_ENABLED
	if (aiTask::sm_bDRRecordTaskTransitions && pTask && pSubTask)
	{
		debugPlayback::RecordAddSubTask(*pTask, *pSubTask);
	}
#endif
	taskFatalAssertf(pTask->GetSubTask() == NULL, "aiTaskTree::AddSubTask: Task still has a sub task associated with it");
	taskFatalAssertf(pSubTask, "aiTaskTree::AddSubTask: pSubTask is NULL");

	pTask->SetSubTask(pSubTask);
	pSubTask->SetEntity(m_pEntity);
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskTree::DeleteTask(aiTask* pTask)
{
	DR_ONLY(debugPlayback::RecordTaskEvent(pTask, "DeleteTask"));

	// Clear the entry in the task list
	s32 i;
	for(i = 0; i < m_Tasks.GetCount(); i++)
	{
		if(m_Tasks[i] == pTask)
		{
			m_Tasks[i] = NULL;

			// Update the active task priority
			m_iActivePriority = ComputeActiveTaskPriority();
			break;
		}
	}

	aiTask* pTaskToDelete = pTask;
	while(pTaskToDelete)
	{
		OnTaskDeleted(pTaskToDelete);
		pTaskToDelete->CleanUp();
		aiTask* pNextTaskToDelete = pTaskToDelete->RelinquishSubTask();
		
		if(sm_AllowDelete)
		{
			Assert(sysThreadType::IsUpdateThread());
			delete pTaskToDelete;
		}
		else
		{
			Assert(sysThreadType::IsUpdateThread());
			pTaskToDelete->ClearAllKnownRefs();
			if(sm_TaskDeletionQueue.GetCount() < sm_TaskDeletionQueue.GetMaxCount())
			{
				sm_TaskDeletionQueue.Push(pTaskToDelete);
			}
			else
			{
				Quitf(ERR_DEFAULT, "Need to increase the TaskDeletionQueue size! Not safe to continue!");
			}
		}

		pTaskToDelete = pNextTaskToDelete;
	}
}

void aiTaskTree::SetAllowDelete(bool allowDelete)
{
	Assert(sysThreadType::IsUpdateThread());
	taskAssert(sm_AllowDelete != allowDelete);
	sm_AllowDelete = allowDelete;

	if(sm_AllowDelete)
	{
		for(s32 i = 0; i < sm_TaskDeletionQueue.GetCount(); ++i)
		{
			delete sm_TaskDeletionQueue[i];
		}

		sm_TaskDeletionQueue.Reset();
	}
}

#if !__FINAL

void aiTaskTree::SpewTaskHierarchy(aiTask* pTask, aiTaskSpew::SpewType type) const
{
	switch(type)
	{
	case aiTaskSpew::SPEW_ASSERT:
		taskDisplayf("aiTaskTree::ManageTasks() for entity %p (%s) - loop bigger than 10\n", m_pEntity, m_pEntity->GetModelName());
		break;
	case aiTaskSpew::SPEW_OVERFLOW:
		taskDisplayf("aiTaskTree::ManageTasks() for entity %p (%s) - loop about to overflow\n", m_pEntity, m_pEntity->GetModelName());
		break;
	default:
		break;
	}

	while(pTask)
	{
		taskDisplayf("%s, Task ptr: %p", pTask->GetName().c_str(), pTask);
		pTask = pTask->GetSubTask();
	}

	if(type == aiTaskSpew::SPEW_ASSERT)
	{
		taskAssertf(false, "Task Spew Assert - Check TTY for details");
	}
}

#endif // !__FINAL

} // namespace rage
