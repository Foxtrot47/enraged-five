//
// ai/taskspew.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/task/taskspew.h"

//////////////////////////////////////////////////////////////////////////
// CTaskFlowDebugEvent
//////////////////////////////////////////////////////////////////////////

#if DR_ENABLED
#include "ai/task/task.h"
#include "ai/task/taskmanager.h"
#include "entity/entity.h"
#include "grcore/im.h"

using namespace rage;
using namespace debugPlayback;

//TODO: PARSER
namespace rage
{
	namespace debugPlayback
	{
		class CTaskFlowDebugEvent : public debugPlayback::PhysicsEvent
		{
			atHashString m_LabelString;
		public:
			CTaskFlowDebugEvent(){}
			virtual ~CTaskFlowDebugEvent(){}
			CTaskFlowDebugEvent(const CallstackHelper rCallstack, const fwEntity *pEntity, const char *pString)
				:debugPlayback::PhysicsEvent(rCallstack, pEntity->GetCurrentPhysicsInst())
				,m_LabelString(pString)
			{
			}
			virtual const char* GetEventSubType() const
			{
				return m_LabelString.GetCStr();
			}
			virtual void DebugDraw3d(eDrawFlags drawFlags) const;
			PAR_PARSABLE;
			DR_EVENT_DECL(CTaskFlowDebugEvent);
		};

		DR_EVENT_IMP(CTaskFlowDebugEvent);

		void TrackEntity(const fwEntity *pEntity, bool bForce)
		{
			SetCurrentSelectedEntity( pEntity->GetCurrentPhysicsInst(), bForce );
		}

		void CTaskFlowDebugEvent::DebugDraw3d(eDrawFlags drawFlags) const
		{
			Color32 col(200,0,200,255);
			if (drawFlags & eDrawSelected)
			{
				col.SetRed(0);
			}
			if (drawFlags & eDrawHovered)
			{
				col.SetGreen(255);
			}
			
			grcColor(col);
			grcDrawSphere(0.1f,m_Pos,4,true,false);
			grcWorldIdentity();

			//Draw text for the tasks
			if (drawFlags & (eDrawSelected))
			{
				SetTaggedFloatEvent::LineUpdate();
				grcColor(Color32(0,0,0,255));
				grcDrawLabelf(RCC_VECTOR3(m_Pos), 1, SetTaggedFloatEvent::Get3dLineOffset()+1, "%d - %s", m_iEventIndex, GetEventSubType());
				grcColor(col);
				grcDrawLabelf(RCC_VECTOR3(m_Pos), 0, SetTaggedFloatEvent::Get3dLineOffset(), "%d - %s", m_iEventIndex, GetEventSubType());
			}
		}

		bool ValidateEntity(const fwEntity *pEntity)
		{
			//TMS:	The IsInLevel check mainly filters out destructions
			//		however sometimes peds get tasks on creation (scenario spawning for instance)
			//		which will then be missed.
			return pEntity->GetCurrentPhysicsInst() 
				&& PhysicsEvent::IsSelected(*pEntity->GetCurrentPhysicsInst()) 
				&& pEntity->GetCurrentPhysicsInst()->IsInLevel();
		}

		void RecordTaskEvent(const aiTask* pTask, const char *pEvent)
		{
			if (pTask && ValidateEntity(pTask->GetEntity()))
			{
				if (DR_EVENT_ENABLED(CTaskFlowDebugEvent))
				{
					CallstackHelper ch;

					DebugRecorder *pRecorder = DebugRecorder::GetInstance();
					if (pRecorder && pRecorder->IsRecording())
					{
						DR_MEMORY_HEAP();
						char formatted[256];
						formatf(formatted, "%s - %s", pTask->GetTaskName(), pEvent);
						pRecorder->AddEvent(*rage_new CTaskFlowDebugEvent(ch, pTask->GetEntity(), formatted));
					}
				}
			}
		}

		void RecordNewTaskEvent(const aiTask* pTask, const aiTask* pNewTask)
		{
			if (pTask && pNewTask && ValidateEntity(pTask->GetEntity()))
			{
				if (DR_EVENT_ENABLED(CTaskFlowDebugEvent))
				{
					CallstackHelper ch;

					DebugRecorder *pRecorder = DebugRecorder::GetInstance();
					if (pRecorder && pRecorder->IsRecording())
					{
						DR_MEMORY_HEAP();
						char formatted[256];
						formatf(formatted, "%s - start '%s'", pTask->GetTaskName(), pNewTask->GetTaskName());
						pRecorder->AddEvent(*rage_new CTaskFlowDebugEvent(ch, pTask->GetEntity(), formatted));
					}
				}
			}
		}

		void RecordQuitTaskEvent(const aiTask* pTask)
		{
			if (pTask && ValidateEntity(pTask->GetEntity()))
			{
				if (DR_EVENT_ENABLED(CTaskFlowDebugEvent))
				{
					CallstackHelper ch;

					DebugRecorder *pRecorder = DebugRecorder::GetInstance();
					if (pRecorder && pRecorder->IsRecording())
					{
						DR_MEMORY_HEAP();
						char formatted[256];
						formatf(formatted, "%s - QUIT", pTask->GetTaskName());
						pRecorder->AddEvent(*rage_new CTaskFlowDebugEvent(ch, pTask->GetEntity(), formatted));
					}
				}
			}
		}

		void RecordSetTask(const aiTask &rTask, int iPriority)
		{
			debugPlayback::FilterToEvent<CTaskFlowDebugEvent> filter;
			if (filter.IsSet())
			{
				const fwEntity *pEntity = rTask.GetEntity();
				if (ValidateEntity(pEntity))
				{
					char namebuffer[64];
					formatf(namebuffer, "%s - SetTask", rTask.GetTaskName());
					debugPlayback::SetTaggedDataCollectionEvent *pCollection 
						= debugPlayback::RecordDataCollection( pEntity->GetCurrentPhysicsInst(), namebuffer );
					if (pCollection)
					{
						DR_MEMORY_HEAP();
						pCollection->AddString("name", rTask.GetTaskName());
						pCollection->AddInt("priority", iPriority);
					}
				}
			}
		}

		void RecordAddSubTask(const aiTask &rTask, const aiTask &rSubTask)
		{
			debugPlayback::FilterToEvent<CTaskFlowDebugEvent> filter;
			if (filter.IsSet())
			{
				const fwEntity *pEntity = rTask.GetEntity();
				if (ValidateEntity(pEntity))
				{
					char namebuffer[64];
					formatf(namebuffer, "%s - AddSubTask", rTask.GetTaskName());
					debugPlayback::SetTaggedDataCollectionEvent *pCollection 
						= debugPlayback::RecordDataCollection( pEntity->GetCurrentPhysicsInst(), namebuffer );
					if (pCollection)
					{
						DR_MEMORY_HEAP();
						pCollection->AddString("subtask", rSubTask.GetTaskName());
						pCollection->AddString("fromstate", rTask.GetStateName(rTask.GetState()));
					}
				}
			}
		}

		void RecordSwitchSubTask(const aiTask &rTask, const aiTask *pOldTask, const aiTask &rNewSubTask)
		{
			debugPlayback::FilterToEvent<CTaskFlowDebugEvent> filter;
			if (filter.IsSet())
			{
				const fwEntity *pEntity = rTask.GetEntity();
				if (ValidateEntity(pEntity))
				{
					char namebuffer[64];
					formatf(namebuffer, "%s - Switch Subtask", rTask.GetTaskName());
					debugPlayback::SetTaggedDataCollectionEvent *pCollection 
						= debugPlayback::RecordDataCollection( pEntity->GetCurrentPhysicsInst(), namebuffer );
					if (pCollection)
					{
						DR_MEMORY_HEAP();
						pCollection->AddString("new", rNewSubTask.GetTaskName());
						if (pOldTask)
						{
							pCollection->AddString("old", pOldTask->GetTaskName());
						}
					}
				}
			}
		}

		void RecordStateChange(const aiTask &rTask, int iPreviousState, int iState)
		{
			debugPlayback::FilterToEvent<CTaskFlowDebugEvent> filter;
			if (filter.IsSet())
			{
				const fwEntity *pEntity = rTask.GetEntity();
				if (pEntity)
				{
					debugPlayback::SetTaggedDataCollectionEvent *pCollection 
						= debugPlayback::RecordDataCollection( pEntity->GetCurrentPhysicsInst(), "%s - StateChange", rTask.GetTaskName() );
					if (pCollection)
					{
						DR_MEMORY_HEAP();
						pCollection->AddString("from", rTask.GetStateName(iPreviousState));
						pCollection->AddString("to", rTask.GetStateName(iState));
					}
				}
			}
		}

		void UpdateTaskTransitionHighLevelGate()
		{
			aiTask::sm_bDRRecordTaskTransitions = DR_EVENT_ENABLED(CTaskFlowDebugEvent);
		}
	}
}

#include "taskspew_parser.h"

#endif // DR_ENABLED

////////////////////////////////////////////////////////////////////////////////
