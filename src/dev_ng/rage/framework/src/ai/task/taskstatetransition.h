//
// ai/taskstatetransition.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASKSTATETRANSITION_H
#define AI_TASKSTATETRANSITION_H

namespace rage
{

// Forward declarations
class fwEntity;

// PURPOSE: State transition tables.  These can be used to define a transition set that can be hooked into the FSM update
class aiTaskStateTransition
{
public:

	aiTaskStateTransition();

	// PURPOSE: Set up the transition
	void Set(s32 iStartState, s32 iEndState, float fProbability, s64 iConditionFlags);

	// PURPOSE: Returns true if this transitions conditions are met
	bool CheckTransition(s32 iStartState, s64 iConditionFlags);

	float GetProbability() const;
	s32 GetEndState() const;

private:
	s32 m_StartState;
	s32 m_EndState;
	float m_Probability;
	s64 m_ConditionFlags;
};

// PURPOSE: State transition tables.  These can be used to define a transition set that can be hooked into the FSM update
class aiTaskStateTransitionFallback
{
public:

	aiTaskStateTransitionFallback();

	// PURPOSE: Set up the transition
	void Set(s32 iStartState, s32 iEndState);

	s32 GetStartState() const { return m_StartState; }
	s32 GetEndState() const { return m_EndState; }

private:
	s32 m_StartState;
	s32 m_EndState;
};

////////////////////////////////////////////////////////////////////////////////

inline float aiTaskStateTransition::GetProbability() const
{
	return m_Probability;
}

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTaskStateTransition::GetEndState() const
{
	return m_EndState;
}

////////////////////////////////////////////////////////////////////////////////

class aiTaskStateTransitionTable
{
public:

	aiTaskStateTransitionTable();

	virtual ~aiTaskStateTransitionTable() {};

	// PURPOSE: Add a transition with the given start and end state
	void AddTransition(s32 iStartState, s32 iEndState, float fProbability, s64 iConditionFlags);

	// PURPOSE: Set the state used in case no valid transition is found
	void AddFallback(s32 iStartState, s32 iFallbackState);

	s32 CheckTransitions(fwEntity* pEntity, s32 iInitialState, s64 iConditionFlags);

	virtual void Init() = 0;

	void Clear() { m_NoTransitions = 0; m_NoFallbacks = 0; }

private:
	enum { MAX_TRANSITIONS = 128 };
	aiTaskStateTransition m_Transitions[MAX_TRANSITIONS];
	s32	m_NoTransitions;

	enum { MAX_FALLBACKS = 32 };
	aiTaskStateTransitionFallback m_Fallbacks[MAX_FALLBACKS];
	s32 m_NoFallbacks;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Transition table data - stores variables used by the base transition table implementation
class aiTaskTransitionTableData
{
public:

	aiTaskTransitionTableData()
		: m_pTransitionSet(NULL)
		, m_LastStateSearchTimer(0.0f)
		, m_LastStateFlags(0)
	{
	}

	aiTaskStateTransitionTable* GetTransitionSet();
	void SetTransitionSet(aiTaskStateTransitionTable* pTableSet);

	float GetLastStateSearchTimer() const;
	void SetLastStateSearchTimer(float val);

	s64 GetLastStateFlags() const;
	void SetLastStateFlags(s64 val);

private:
	// Optionally used transition table
	aiTaskStateTransitionTable* m_pTransitionSet;
	// Optionally stores the last time a state transition search was attempted, used by the periodic search
	float m_LastStateSearchTimer;
	// Optionally stores the last conditional flags, if they differ across frames this can be used to trigger an immediate search
	s64 m_LastStateFlags;
};

////////////////////////////////////////////////////////////////////////////////

inline aiTaskStateTransitionTable* aiTaskTransitionTableData::GetTransitionSet()
{
	return m_pTransitionSet;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskTransitionTableData::SetTransitionSet(aiTaskStateTransitionTable* pTableSet)
{
	m_pTransitionSet = pTableSet;
}

////////////////////////////////////////////////////////////////////////////////

inline float aiTaskTransitionTableData::GetLastStateSearchTimer() const
{
	return m_LastStateSearchTimer;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskTransitionTableData::SetLastStateSearchTimer(float val)
{
	m_LastStateSearchTimer = val;
}

////////////////////////////////////////////////////////////////////////////////

inline s64 aiTaskTransitionTableData::GetLastStateFlags() const
{
	return m_LastStateFlags;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskTransitionTableData::SetLastStateFlags(s64 val)
{
	m_LastStateFlags = val;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // AI_TASKSTATETRANSITION_H
