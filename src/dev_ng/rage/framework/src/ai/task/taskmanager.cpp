//
// ai/taskmanager.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "ai/task/taskmanager.h"

// Framework headers
#include "ai/aioptimisations.h"

FW_AI_OPTIMISATIONS()

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

aiTaskManager::aiTaskManager(s32 iNumberOfTaskTreesRequired, bool treesAreExternallyOwned)
: m_TreesAreExternallyOwned(treesAreExternallyOwned)
{
	// Create some empty task trees
	m_Trees.Resize(iNumberOfTaskTreesRequired);
	for(s32 i = 0; i < iNumberOfTaskTreesRequired; i++)
	{
		m_Trees[i] = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

aiTaskManager::~aiTaskManager()
{
	if(!m_TreesAreExternallyOwned)
	{
		for(s32 i = 0; i < m_Trees.GetCount(); i++)
		{
			if(m_Trees[i])
			{
				delete m_Trees[i];
				m_Trees[i] = NULL;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskManager::Process(float fTimeStep)
{
	// Process all task trees
	for(s32 i = 0; i < m_Trees.GetCount(); i++)
	{
		GetTree(i)->Process(fTimeStep);
	}
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskManager::AbortTasksAtTreeIndex( const s32 iTreeIndex )
{
	//Assert(iTreeIndex >= 0 && iTreeIndex < m_Trees.GetCount());
	GetTree(iTreeIndex)->AbortTasks();
}

////////////////////////////////////////////////////////////////////////////////

void aiTaskManager::AbortTasks()
{
	for(s32 i = 0; i < m_Trees.GetCount(); i++)
	{
		GetTree(i)->AbortTasks();
	}
}

#if !__FINAL

////////////////////////////////////////////////////////////////////////////////

void aiTaskManager::SpewAllTasks(aiTaskSpew::SpewType type)
{
	taskDisplayf("");
	taskDisplayf("");
	taskDisplayf("TASK MANAGER SPEW:");

	for(s32 i = 0; i < m_Trees.GetCount(); i++)
	{
		taskDisplayf("");
		taskDisplayf("TASK TREE SPEW: %d!", i);
		GetTree(i)->SpewAllTasks(type);
		taskDisplayf("END TASK TREE SPEW!");
		taskDisplayf("");
	}

	taskDisplayf("END TASK MANAGER SPEW!");
	taskDisplayf("");
	taskDisplayf("");
}

#endif // !__FINAL

} // namespace rage
