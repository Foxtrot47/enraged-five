//
// ai/task.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASK_H
#define AI_TASK_H

// Rage headers
#include "atl/string.h"

// Framework headers
#include "ai/aioptimisations.h"
#include "ai/task/taskchannel.h"
#include "ai/task/taskflags.h"
#include "ai/task/tasktree.h"
#include "ai/task/taskregdref.h"

// Only available if we have enabled the debug data store
#define TASK_DETAILED_INFINITE_LOOP_TRAP __ASSERT
#define ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG (FW_AI_OPTIMISATIONS_OFF && 1)

namespace rage
{

// Forward declarations
class aiEvent;
class aiTaskStateTransitionTable;
class aiTaskTransitionTableData;
class fwEntity;
class fwDebugStateHistory;

// State machine generation macros
#define FSM_Begin if(iState<0){if(0){
#define FSM_End }}else{Assertf(0, "Didn't handle state %i in UpdateFSM", iState);\
	return(FSM_Quit);}return(FSM_Continue);
#define FSM_State(a)	}}else if(a == iState) {if(0){
#define FSM_OnEvent(a)	}else if(a == iEvent) {
#define FSM_OnEnter		FSM_OnEvent(OnEnter)
#define FSM_OnUpdate	FSM_OnEvent(OnUpdate)
#define FSM_OnExit		FSM_OnEvent(OnExit)

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: aiTask is a class that provides state machine support for control of entities
//
// UpdateFSM
//  This is the core function that controls the progression of the FSM
//  All inherited classes should implement this using the defines above, in the format:
//  aiTask::FSM_Return aiTask::UpdateFSM( s32 iState, FSM_Event iEvent )
// {
// FSM_Begin
// 		// State A, does State A things
// 		FSM_State(State_A)
// 			FSM_OnUpdate
// 				if(StateShouldChange())
// 					SetState(State_B);
// 
//		// State B, does State B things
// 		FSM_State(State_B)
// 			FSM_OnEnter
// 				SetNewTask(rage_new CTaskStateBTask());
// 			FSM_OnUpdate
// 				if( IsFlagSet(Flag_subTaskFinished) )
// 				{
// 					// IF the weapon swap failed, quit
// 					if( ShouldQuit() )
// 						return Quit;
//		
// 					// weapon swap succeeded, move to throwing
// 					SetState(State_C);
// 				}
// 
// 		// State C, does State C things
// 		FSM_State(ThrowState_throwing)
// 			FSM_OnEnter
// 				return StateC_OnEnter(pPed);
// 			FSM_OnUpdate
// 				return StateC_OnUpdate(pPed);
// 			FSM_OnExit
// 				return StateC_OnExit(pPed);
// 		FSM_End
// } 
//
// MakeAbortable
//  The FSM task implements a basic MakeAbortable that always succeeds and simply exits the current state
//  when aborted, GetDefaultStateAfterAbort can be implemented to specify the state the task should resume
//  in if the task ever resumes. The task will then restart in this state.  Alternatively MakeAbortable
//  can be overridden if a custom implementation is required.
class aiTask : public aiTaskBaseClass
{
	friend class aiTaskTree;
	typedef aiTask*		aiTaskPtr;
public:

	// PURPOSE:	Register that we are about to start updating AI tasks, which should
	//			use the given time step.
	// NOTES:	This would have to change if we wanted to multithread task updates,
	//			then we couldn't use static time step variables like this.
	static void BeginTaskUpdates(float timeStep)
	{
		taskAssert(sm_TimeStepInMilliSeconds == 0);	// Called BeginTaskUpdates() again, without EndTaskUpdates()?
		taskAssert(timeStep >= 0.0f);
		sm_TimeStep = timeStep;
		sm_TimeStepInMilliSeconds = Max(timeStep*1000.0f, 1.f);
	}

	// PURPOSE:	End a sequence of task updates previously initiated by BeginTaskUpdates().
	static void EndTaskUpdates()
	{
		taskAssert(sm_TimeStepInMilliSeconds);		// Maybe called EndTaskUpdates() without BeginTaskUpdates()?
		sm_TimeStep = 0.0f;
		sm_TimeStepInMilliSeconds = 0.0f;
	}

	// PURPOSE:	Get the time step to use for updating this task, to be called from within
	//			the task during an update. Returns the time step in seconds.
	// NOTES:	Meant as a replacement for fwTimer::GetTimeStep(), from within task updates.
	float GetTimeStep() const { Assert(sm_TimeStepInMilliSeconds); return sm_TimeStep; }

	// PURPOSE:	Get the time step to use for updating this task, to be called from within
	//			the task during an update. Returns the time step in milliseconds.
	// NOTES:	Meant as a replacement for fwTimer::GetTimeStepInMilliseconds(), from within task updates.
	u32 GetTimeStepInMilliseconds() const { Assert(sm_TimeStepInMilliSeconds); return (u32)sm_TimeStepInMilliSeconds; }

	aiTask();
	virtual ~aiTask();

#if !__FINAL
	virtual const char* GetTaskName() const { return "TASK_INVALID"; }
#endif // !__FINAL

	// RETURNS: The task Id. Each task class has an Id for quick identification
	virtual s32 GetTaskTypeInternal() const { return -1;}

	// non virtual GetTaskType
	// RETURNS: The task Id. Each task class has an Id for quick identification
	s32 GetTaskType() const;	

	// PURPOSE: Macro to set task type
	#define SetInternalTaskType(x) taskAssert(x <= 0xFFFF); m_TaskType = x

	// RETURNS: The current FSM state
	s32 GetState() const;

	// RETURNS: The previous FSM state
	s32 GetPreviousState() const;

	// RETURNS: The time spent in the current state
	float GetTimeInState() const;

	// RETURNS: The overall time spent in this task
	float GetTimeRunning() const;

	// PURPOSE: Used to produce a copy of this task
	// RETURNS: A copy of this task
	virtual aiTask* Copy() const = 0;

	// PURPOSE: The level of urgency required
	enum AbortPriority
	{
		ABORT_PRIORITY_URGENT = 0,
		ABORT_PRIORITY_IMMEDIATE
	};

	// PURPOSE: Force a task to clean itself up and thereby get into a state where it could be safely deleted.
	// PARAMS
	//	priority - The required abort urgency level
	//	pEvent - The event that caused the abort, could be NULL
	// RETURNS: If the task aborted
	virtual bool MakeAbortable(const AbortPriority priority, const aiEvent* pEvent);

	// PURPOSE:	If this function returns true, the task says it's OK to delete if it gets aborted,
	//			because it lacks the ability to resume. There is however no guarantee that this will
	//			happen, so the tasks still have to behave properly (i.e. quit) if they do get
	//			restarted after abort.
	virtual bool MayDeleteOnAbort() const { return false; }

	// PURPOSE:	If a subtask's MayDeleteOnAbort() returns true, this task has the chance to reject
	//			the deletion by letting this function return false.
	virtual bool MayDeleteSubTaskOnAbort() const { return true; }

	// PURPOSE:	Should return true if we know that the aborted subtasks won't be needed
	//			if we resume after getting aborted.
	// NOTES:	By default, this returns MayDeleteOnAbort(), i.e. if a task says it could be deleted
	//			if aborted, we assume that it's also OK to delete its subtasks if we can't delete the
	//			itself. If that assumption is incorrect for some tasks, they can implement
	//			ShouldAlwaysDeleteSubTaskOnAbort() to explicitly return false.
	virtual bool ShouldAlwaysDeleteSubTaskOnAbort() const { return MayDeleteOnAbort(); }

	// PURPOSE: Termination requests - does this task support a request to terminate
	//	If it does a parent task can call RequestTermination and the task will
	//	begin exiting in a clean way and is guaranteed to terminate of its own means 
	//	with in an undetermined duration
	virtual bool SupportsTerminationRequest();

	// PURPOSE: Handle termination request
	virtual bool RequestTermination();

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Get the const entity
	const fwEntity* GetEntity() const;

	// PURPOSE: Get the entity
	fwEntity* GetEntity();

	// PURPOSE: Return the const parent to this task in the hierarchy
	const aiTask* GetParent() const;

	// PURPOSE: Return the parent to this task in the hierarchy
	aiTask* GetParent();

	// PURPOSE: Return the current const child task
	const aiTask* GetSubTask() const;

	// PURPOSE: Return the current child task
	aiTask* GetSubTask();

	// PURPOSE: Get the const next child task
	const aiTask* GetNewTask() const;

	// PURPOSE: Get the next child task
	aiTask* GetNewTask();

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Set an adjustable flag
	void SetFlag(const aiTaskFlags::AdjustableFlags flag);

	// PURPOSE: Clear an adjustable flag
	void ClearFlag(const aiTaskFlags::AdjustableFlags flag);

	// PURPOSE: Query if a particular adjustable flag is set
	bool GetIsFlagSet(const aiTaskFlags::AdjustableFlags flag) const;

	// PURPOSE: Query if a particular information flag is set
	bool GetIsFlagSet(const aiTaskFlags::InformationFlags flag) const;

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Search up the hierarchy for a parent task of a particular type
	aiTask* FindParentTaskOfType(const s32 iType) const;

	// PURPOSE: Search down the hierarchy for a child task of a particular type
	aiTask* FindSubTaskOfType(const s32 iType) const;

	// PURPOSE: Check if the sub task of the specified type is finished
	// RETURNS: True if no sub task is running, or it is not of the specified type
	bool GetIsSubtaskFinished(s32 iTaskType) const;

	////////////////////////////////////////////////////////////////////////////////

	// OPTIONAL
	// State transition table implementation
	// these can be used to define a transition set that can be hooked into the FSM update
	// PURPOSE: Pick the most appropriate state given the current circumstances
	bool PickNewStateTransition(s32 iStateIfNonFound, bool bStateEnded = false);

	// PURPOSE: Find the most appropriate state given the current circumstances
	s32 FindNewStateTransition(s32 iStateIfNonFound, bool bStateEnded = false);

	// PURPOSE: Periodically check for a new state, maintains a local variable
	virtual bool PeriodicallyCheckForStateTransitions(float fAverageTimeBetweenChecks);

	// PURPOSE: Return the valid transitional set
	virtual aiTaskStateTransitionTable* GetTransitionSet();

	// PURPOSE: Generate the conditional flags to choose a transition
	virtual s64 GenerateTransitionConditionFlags(bool bStateEnded);

	// PURPOSE: Returns a pointer to the transition table data, overridden by tasks that use transitional sets
	virtual aiTaskTransitionTableData* GetTransitionTableData();

	////////////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT
	// PURPOSE: Display debug information specific to this task
	virtual void Debug() const;

	// PURPOSE: Get the name of this task as a string
	virtual atString GetName() const = 0;

	// PURPOSE: Get the name of the specified state
	virtual const char* GetStateName(s32 iState) const = 0;
#endif // !__FINAL

protected:

public: // TEMPORARILY PUBLIC - still some remaining public references in game code
	// PURPOSE: Sets the FSM state
	void SetState(const s32 iState);

	// PURPOSE: Set a new child task
	void SetNewTask(aiTask* pNewTask);
protected:

	// PURPOSE: State return values, continue or quit
	enum FSM_Return
	{
		FSM_Continue = 0,
		FSM_Quit,
	};

	// PURPOSE: Basic FSM operation events
	enum FSM_Event
	{
		OnEnter = 0,
		OnUpdate,
		OnExit,
	};


	// PURPOSE: ProcessFSM
	virtual FSM_Return ProcessFSM(const s32 iState, const FSM_Event iEvent);

	// PURPOSE: FSM implementation
	virtual FSM_Return UpdateFSM(const s32 iState, const FSM_Event iEvent) = 0;

private:
	// PURPOSE: Task update that is called before FSM update, once per frame.
	// RETURNS: If FSM_Quit is returned the current state will be terminated
	// If the base version is ever called it is assumed there is no implementation
	// higher up and all susequent calls are disabled
	virtual FSM_Return ProcessPreFSM();

	// PURPOSE: Task update that is called after FSM update, once per frame.
	// RETURNS: If FSM_Quit is returned the current state will be terminated
	// If the base version is ever called it is assumed there is no implementation
	// higher up and all susequent calls are disabled
	virtual FSM_Return ProcessPostFSM();

protected:
	// PURPOSE: Generic cleanup function, called when the task quits itself or is aborted
	virtual	void CleanUp();

	// PURPOSE: Determine whether or not the task should abort
	// RETURNS: True if should abort
	virtual bool ShouldAbort(const AbortPriority priority, const aiEvent* pEvent);

	// PURPOSE: Handle aborting the task - this will be called in addition to CleanUp when we are aborting
	virtual void DoAbort(const AbortPriority priority, const aiEvent* pEvent);

	// RETURNS: Returns the default state that the task should restart in when resuming after being aborted (can be the current state)
	virtual s32 GetDefaultStateAfterAbort() const = 0;

#if __ASSERT
	void SetCanChangeState(bool canChangeState);
#endif // __ASSERT
	
private:
	
	// PURPOSE: Sets the previous FSM state
	void SetPreviousState(const s32 iState)
	{
		taskAssert(iState <= 0xFF);
		m_PreviousState = iState; 
	}

public: // TEMPORARILY PUBLIC - still some remaining public references in game code
	// PURPOSE: Set the entity
	void SetEntity(fwEntity* pEntity);

	// PURPOSE: Set the parent of this task
	void SetParent(aiTask *pParent);

private:
	// PURPOSE: Set the current child task. If there is already a child task then delete the previous child task
	void SetSubTask(aiTask *pSubTask);

	// PURPOSE: Delete the child task, assumes its been aborted.
	void DeleteSubTask();

	// PURPOSE: Relinquish ownership of the child task
	aiTask* RelinquishSubTask();

	// PURPOSE: Relinquish ownership of the new task
	aiTask* RelinquishNewTask();

	// PURPOSE: Reset animation status flags
	virtual void FSM_SetAnimationFlags() {}

	// PURPOSE: Game level implementation called to abort tasks as they switch
	virtual void FSM_AbortPreviousTask() {}

private:

	// PURPOSE: Entity task is operating on
	fwEntity* m_pEntity;

	// PURPOSE: Parent task
	// NOTES:   Deliberately not a regdref otherwise a constantly kept around task will be flooded with
	//          SetParent references from children.  Delete protection should prevent you accidentally 
	//			deleting through it anyway.
	aiTask	  *m_pParent;

	// PURPOSE: Child task
	RegdaiTask m_pSubTask;

	// PURPOSE: New child task - will be activated by the task manager
	RegdaiTask m_pNewTask;

public: // TEMPORARILY PUBLIC - still some remaining public references in game code
	// PURPOSE: Flags
	aiTaskFlags	m_Flags;

protected:
	// PURPOSE: Task type
	s32 m_TaskType : 16;

private:
	// PURPOSE: Current state
	s32 m_State : 8;

	// PURPOSE: Previous state
	s32 m_PreviousState : 8;

	// PURPOSE:	Time step in s to use for tasks currently being updated.
	static float sm_TimeStep;

	// PURPOSE:	Time step in ms to use for tasks currently being updated.
	static float sm_TimeStepInMilliSeconds;

protected: // CS: TEMPORARY WHILE BaseUpdateFSM issues
	// PURPOSE: Time spent in current state
	float m_TimeInState;

	// PURPOSE: Time spent running task
	float m_TimeRunning;

public:

	// PURPOSE: Passed to SetStateCallstackRecording, to indicate why we
	// are recording
	enum eTaskRecordingFlags
	{
		TRF_InfiniteLoop					= BIT0,
		TRF_FocusEntitySpew					= BIT1,
		TRF_ToldToSpew						= BIT2,

		// Start spewing in all cases
		TRF_MaskSpew = TRF_FocusEntitySpew | TRF_ToldToSpew | TRF_InfiniteLoop,

		// We start spewing a new subtask if we are in an infinite loop,
		// or we are part of the subtask chain from something in an infinite
		// loop
		// This allows us to trap FSM continue problems based around sub
		// task trees.
		TRF_MaskShouldRecordSubTask = TRF_InfiniteLoop | TRF_ToldToSpew
	};

private:

	// PURPORSE: Prevents copy construction (there should be clone functions).  Not doing this
	// means per task debug information and child task's etc... also propagate unchecked causing
	// memory issues.
	aiTask( const aiTask & )
	{
	}
	aiTask & operator= (const aiTask & ) { return *this; }

public:

#if !__FINAL	//Really only DR_ONLY but avoiding header dependency
	static bool sm_bDRRecordTaskTransitions;
#endif

#if TASK_DETAILED_INFINITE_LOOP_TRAP

	// PURPOSE: Set a call-stack recording flag
	// This will start a fwDebugStateHistory record, if we don't have one
	void SetCallstackRecordingFlagState(u32 state);
	void SetCallstackRecordingFlag(eTaskRecordingFlags taskRecordingFlag);
	void ClearCallstackRecordingFlag(eTaskRecordingFlags taskRecordingFlag);

	// PURPOSE: Returns what we should start sub tasks with
	inline int GetTaskRecordingFlags() const;

	// PURPOSE: Output any collected set state call-stacks to the TTY.
	void PrintSetStateCallstacks() const;

	// PURPOSE: Indicates a continue FSM point - indicates loops that don't move state
	void DebugIndicateAContinueFSMPoint( /*FSM_Return val*/ );

private:

	// Private helpers to the above //////////////////////////////////////////////////////
	// Add a call-stack to the ring buffer
	void AddSetStateCallstack() const;

	// Just update our debug record (as to whether we allocate/deallocate it)
	void UpdateFromCallstackRecordingFlags();

	// Helper function - just prints from a cursor for count
	void PrintSetStateCallstacks(int current, int count, u32 time) const;

	// fwDebugStateHistory entry
	fwDebugStateHistory* m_pSetStateData;

	// Reasons why we are currently recording.  
	u8 m_TaskRecordingFlags;												

#else // TASK_DETAILED_INFINITE_LOOP_TRAP

	// Should be dead stripped
public:
	inline void SetCallstackRecordingFlagState( u32 ) 								{}
	inline int GetTaskRecordingFlags() const										{ return 0; }
	inline void SetCallstackRecordingFlag( u32 ) 									{}
	inline void ClearCallstackRecordingFlag( u32 ) 									{}
	inline void PrintSetStateCallstacks() const										{}
	inline void DebugIndicateAContinueFSMPoint( /*FSM_Return val*/ )				{}

#endif // TASK_DETAILED_INFINITE_LOOP_TRAP

#if __ASSERT
	// Alert when calling SetState in invalid conditions
	u8 m_CanChangeState : 1;														
#endif // __ASSERT
};

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTask::GetState() const
{
	return m_State;
}

////////////////////////////////////////////////////////////////////////////////
#if !__DEV // find the dev version in task.cpp
inline s32 aiTask::GetTaskType() const
{
	return m_TaskType;
}
#endif // !__Dev
////////////////////////////////////////////////////////////////////////////////

inline s32 aiTask::GetPreviousState() const
{
	return m_PreviousState;
}

////////////////////////////////////////////////////////////////////////////////

inline float aiTask::GetTimeInState() const
{
	return m_TimeInState;
}

////////////////////////////////////////////////////////////////////////////////

inline float aiTask::GetTimeRunning() const
{
	return m_TimeRunning;
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTask::SupportsTerminationRequest()
{
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline const fwEntity* aiTask::GetEntity() const
{
	return m_pEntity;
}

////////////////////////////////////////////////////////////////////////////////

inline fwEntity* aiTask::GetEntity()
{
	return m_pEntity;
}

////////////////////////////////////////////////////////////////////////////////

inline const aiTask* aiTask::GetParent() const
{
	return m_pParent;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTask::GetParent()
{
	return m_pParent;
}

////////////////////////////////////////////////////////////////////////////////

inline const aiTask* aiTask::GetSubTask() const
{
	return m_pSubTask;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTask::GetSubTask()
{
	return m_pSubTask;
}

////////////////////////////////////////////////////////////////////////////////

inline const aiTask* aiTask::GetNewTask() const
{
	return m_pNewTask;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTask::GetNewTask()
{
	return m_pNewTask;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTask::SetFlag(aiTaskFlags::AdjustableFlags flag)
{
	m_Flags.SetFlag(flag);
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTask::ClearFlag(aiTaskFlags::AdjustableFlags flag)
{
	m_Flags.ClearFlag(flag);
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTask::GetIsFlagSet(aiTaskFlags::AdjustableFlags flag) const
{
	return m_Flags.IsFlagSet(flag);
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTask::GetIsFlagSet(aiTaskFlags::InformationFlags flag) const
{
	return m_Flags.IsFlagSet(flag);
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTask::GetIsSubtaskFinished(s32 iTaskType) const
{
	// If the subtask has finished or never started, finish the task
	if(m_Flags.IsFlagSet(aiTaskFlags::SubTaskFinished) || GetSubTask() == NULL || GetSubTask()->GetTaskType() != iTaskType)
	{
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTaskStateTransitionTable* aiTask::GetTransitionSet()
{
	taskAssertf(0, "GetTransitionSet must be overriden if transition tables are to be used!");
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline s64 aiTask::GenerateTransitionConditionFlags(bool UNUSED_PARAM(bStateEnded))
{
	taskAssertf(0, "GenerateTransitionConditionFlags must be overriden if transition tables are to be used!");
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTaskTransitionTableData* aiTask::GetTransitionTableData()
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT

inline void aiTask::Debug() const
{
	if(GetSubTask())
	{
		GetSubTask()->Debug();
	}
}

#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

inline aiTask::FSM_Return aiTask::ProcessPreFSM()
{
	// DONT PUT ANY CODE HERE, ANY CODE THAT NEEDS EXECUTED NEEDS TO BE LOCATED IN THE OVERRIDEN FN
	m_Flags.SetFlag(aiTaskFlags::SkipProcessPreFSM);
	return FSM_Continue;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask::FSM_Return aiTask::ProcessPostFSM()
{
	// DONT PUT ANY CODE HERE, ANY CODE THAT NEEDS EXECUTED NEEDS TO BE LOCATED IN THE OVERRIDEN FN
	m_Flags.SetFlag(aiTaskFlags::SkipProcessPostFSM);
	return FSM_Continue;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTask::CleanUp()
{
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTask::ShouldAbort(const AbortPriority UNUSED_PARAM(priority), const aiEvent* UNUSED_PARAM(pEvent))
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTask::DoAbort(const AbortPriority UNUSED_PARAM(priority), const aiEvent* UNUSED_PARAM(pEvent))
{
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTask::SetEntity(fwEntity* pEntity)
{
	taskAssertf(!m_pEntity, "Entity should not be changed");
	m_pEntity = pEntity;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTask::SetParent(aiTask *pParent)
{
	taskAssertf(!m_pParent, "Parent should not be changed");
	m_pParent = pParent;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTask::RelinquishSubTask()
{
	aiTask *pSubTask = m_pSubTask;
	if ( m_pSubTask )
	{
		m_pSubTask->m_pParent	= NULL;
		m_pSubTask				= NULL;
	}
	return pSubTask;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTask::RelinquishNewTask()
{
	aiTask *pNewTask	= m_pNewTask;
	if ( m_pNewTask )
	{
		m_pNewTask->m_pParent	= NULL;
		m_pNewTask	= NULL;
	}
	return pNewTask;
}

////////////////////////////////////////////////////////////////////////////////

#if __ASSERT

inline void aiTask::SetCanChangeState(bool canChangeState)
{
	m_CanChangeState = canChangeState;
}

#endif //__ASSERT

////////////////////////////////////////////////////////////////////////////////

#if TASK_DETAILED_INFINITE_LOOP_TRAP

inline int aiTask::GetTaskRecordingFlags() const
{
	return m_TaskRecordingFlags;
}

#endif // TASK_DETAILED_INFINITE_LOOP_TRAP

////////////////////////////////////////////////////////////////////////////////

#if ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG
struct aiTaskStateHistory
{
public: 

	static const s8		TASK_STATE_INVALID = -99;
	static const u32	MAX_TASK_STATES = 10;

	aiTaskStateHistory(const aiTask* pTask);
	~aiTaskStateHistory();

	void AddState(s32 iState);
	const aiTask* GetAssociatedTask() { return m_pAssociatedTask; }
	s32 GetStateAtIndex(s32 iIndex) const { return (s32) m_aPreviousStates[iIndex]; }
	u32 GetStateFrameCountAtIndex(s32 iIndex) const { return (u32) m_aPreviousStateFrame[iIndex]; }
	s32	GetFreeSlotStateIndex() const { return m_iFreeSlotStateIndex; }

private:

	s8					m_aPreviousStates[MAX_TASK_STATES];
	u32					m_aPreviousStateFrame[MAX_TASK_STATES];
	s8					m_iFreeSlotStateIndex;
	const aiTask*		m_pAssociatedTask;
};

////////////////////////////////////////////////////////////////////////////////

class aiTaskStateHistoryManager
{
public:

	static aiTaskStateHistoryManager* GetInstance();

	aiTaskStateHistoryManager();
	~aiTaskStateHistoryManager();

	void AddStateHistory(const aiTask* pTask);
	void RemoveStateHistory(const aiTask* pTask);
	void AddStateToHistory(const aiTask* pTask, s32 iState);
	aiTaskStateHistory* GetTaskStateHistoryForTask(const aiTask* pTask);

private:

	s32 FindTaskStateHistoryIndexForTask(const aiTask* pTask);

	static const u32					MAX_TASK_STATE_HISTORIES = 400;
	aiTaskStateHistory*					m_apStateHistoriesList[MAX_TASK_STATE_HISTORIES];
	static aiTaskStateHistoryManager*	ms_pTaskStateHistoryMgr;

};
#endif // ENABLE_SIMPLE_TASK_STATE_HISTORY_DEBUG

} // namespace rage

#endif // AI_TASK_H
