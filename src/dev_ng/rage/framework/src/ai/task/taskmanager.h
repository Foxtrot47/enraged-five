//
// ai/taskmanager.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASKMANAGER_H
#define AI_TASKMANAGER_H

// Framework headers
#include "ai/task/tasktree.h"


// Enable this to get slightly more helpful asserts for bounds-checking in tree-related functions.
#define VERBOSE_AITREE_ASSERTS 0

namespace rage
{

class aiTaskManager : public datBase
{
public:

	aiTaskManager(s32 iNumberOfTaskTreesRequired, bool treesAreExternallyOwned = false);
	virtual ~aiTaskManager();

	// PURPOSE: Manages the tasks trees attached to this manager. 
	void Process(float fTimeStep);

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Add a new task tree to the task manager in a certain position
	void SetTree(const s32 iTreeIndex, aiTaskTree* pTree);
	void SetExternallyOwnedTree(const s32 iTreeIndex, aiTaskTree* pTree);

	// PURPOSE: Get the task tree from task tree array
	aiTaskTree* GetTree(const s32 iTreeIndex) const;

    //PURPOSE: Deletes the task tree from the task tree array
    void DeleteTree(const s32 iTreeIndex);

	// PURPOSE: Get the number of task trees associated with this manager
	s32 GetTreeCount() const;

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Sets a new task in a specified slot and specified task tree. It takes as parameters the task you want
	// to set, the task priority (slot) and if you want to force the task to be set.
	// This function will delete the previous task. If NULL is sent as the new
	// task then this function just deletes the old task. If the task is the same
	// type then the new task is ignored unless bForceNewTask is set to true.
	void SetTask(const s32 iTreeIndex, aiTask* pTask, const s32 iPriority, const bool bForceNewTask = false);

	// PURPOSE: Get the current task at supplied priority
	aiTask* GetTask(const s32 iTreeIndex, const s32 iPriority) const;

	// PURPOSE: Returns the task at the bottom of the task chain of the default task
	aiTask* GetLeafTask(const s32 iTreeIndex, const s32 iPriority) const;

	// PURPOSE: If the task is set then clear it, returns true if it manages to clear it
	bool ClearTask(const s32 iTreeIndex, const s32 iPriority);

	// PURPOSE: Returns the active task. The Active task is the task with the highest
	// priority with event response being the highest priority
	aiTask* GetActiveTask(const s32 iTreeIndex) const;

	// PURPOSE: Returns the task at the bottom of the task chain of the active task
	aiTask* GetActiveLeafTask(const s32 iTreeIndex) const;

	// PURPOSE: Gets the priority level of the current active task
	s32 GetActiveTaskPriority(const s32 iTreeIndex) const;

	// PURPOSE: Returns true if the task is found in the task tree specified
	bool GetHasTask(const s32 iTreeIndex, const aiTask* pTask) const;

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Abort and delete all tasks in the specified tree at the specified priority
	void AbortTasksWithPriority(const s32 iTreeIndex, const s32 iPriority);

	// PURPOSE: Abort and delete all tasks associated with this task tree
	void AbortTasksAtTreeIndex(const s32 iTreeIndex);

	// PURPOSE: Abort and delete all tasks on all trees
	void AbortTasks();

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Find task within active tasks, task chain
	aiTask* FindTaskByTypeActive(const s32 iTreeIndex, const s32 iTaskType) const;

	// PURPOSE: Find task of specified priority within task chain
	aiTask* FindTaskByTypeWithPriority(const s32 iTreeIndex, const s32 iTaskType, const s32 iPriority) const;

#if !__FINAL
	// PURPOSE: Output task hierarchy to TTY
	void SpewAllTasks(aiTaskSpew::SpewType type);

	// PURPOSE: Get the task history of the specified tree
	s32 GetHistory(const s32 iTreeIndex, const s32 iHistoryIndex) const;
#endif // !__FINAL

protected:

	// PURPOSE: Called from SetTask to perform any additional processing needed by derived classes
	virtual void OnTaskChanged() {}

private:

	static const s32 MAX_TREES_SUPPORTED = 4;
	typedef atFixedArray<aiTaskTree*, MAX_TREES_SUPPORTED> TaskTreeArray;
	// PURPOSE: Storage for task trees
	TaskTreeArray m_Trees;
	bool m_TreesAreExternallyOwned;
};

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskManager::SetTree(const s32 iTreeIndex, aiTaskTree* pTree)
{
#if VERBOSE_AITREE_ASSERTS
	taskFatalAssertf(iTreeIndex >= 0 && iTreeIndex < m_Trees.GetCount(), "iTreeIndex [%d] is out of range [0,%d]", iTreeIndex, m_Trees.GetCount());
	taskFatalAssertf(m_Trees[iTreeIndex] == NULL, "Cannot replace an allocated tree index [%d]", iTreeIndex);
#endif // VERBOSE_AITREE_ASSERTS
	taskAssert(!m_TreesAreExternallyOwned);
	m_Trees[iTreeIndex] = pTree;
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskManager::SetExternallyOwnedTree(const s32 iTreeIndex, aiTaskTree* pTree)
{
#if VERBOSE_AITREE_ASSERTS
	taskFatalAssertf(iTreeIndex >= 0 && iTreeIndex < m_Trees.GetCount(), "iTreeIndex [%d] is out of range [0,%d]", iTreeIndex, m_Trees.GetCount());
	taskFatalAssertf(m_Trees[iTreeIndex] == NULL, "Cannot replace an allocated tree index [%d]", iTreeIndex);
#endif // VERBOSE_AITREE_ASSERTS
	taskAssert(m_TreesAreExternallyOwned);
	m_Trees[iTreeIndex] = pTree;
}

////////////////////////////////////////////////////////////////////////////////

inline aiTaskTree* aiTaskManager::GetTree(const s32 iTreeIndex) const
{
#if VERBOSE_AITREE_ASSERTS
	taskFatalAssertf(iTreeIndex >= 0 && iTreeIndex < m_Trees.GetCount(), "iTreeIndex [%d] is out of range [0,%d]", iTreeIndex, m_Trees.GetCount());
	taskFatalAssertf(m_Trees[iTreeIndex], "Tree [%d] is not allocated", iTreeIndex);
#else // VERBOSE_AITREE_ASSERTS
	TrapZ((size_t) m_Trees[iTreeIndex]);
#endif // VERBOSE_AITREE_ASSERTS
	return m_Trees[iTreeIndex];
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskManager::DeleteTree(const s32 iTreeIndex)
{
	if(!m_TreesAreExternallyOwned)
	{
		aiTaskTree *pTree = GetTree(iTreeIndex);
		delete pTree;
	}

    m_Trees[iTreeIndex] = NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTaskManager::GetTreeCount() const
{
	return m_Trees.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskManager::SetTask(const s32 iTreeIndex, aiTask* pTask, const s32 iPriority, const bool bForceNewTask)
{
	GetTree(iTreeIndex)->SetTask(pTask, iPriority, bForceNewTask);
	// Perform any additional processing needed by derived classes
	OnTaskChanged();
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskManager::GetTask(const s32 iTreeIndex, const s32 iPriority) const
{
	return GetTree(iTreeIndex)->GetTask(iPriority);
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskManager::GetLeafTask(const s32 iTreeIndex, const s32 iPriority) const
{
	return GetTree(iTreeIndex)->GetLeafTask(iPriority);
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTaskManager::ClearTask(const s32 iTreeIndex, const s32 iPriority)
{
	return GetTree(iTreeIndex)->ClearTask(iPriority);
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskManager::GetActiveTask(const s32 iTreeIndex) const
{
	return GetTree(iTreeIndex)->GetActiveTask();
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskManager::GetActiveLeafTask(const s32 iTreeIndex) const
{
	return GetTree(iTreeIndex)->GetActiveLeafTask();
}

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTaskManager::GetActiveTaskPriority(const s32 iTreeIndex) const
{
	return GetTree(iTreeIndex)->GetActiveTaskPriority();
}

////////////////////////////////////////////////////////////////////////////////

inline bool aiTaskManager::GetHasTask(const s32 iTreeIndex, const aiTask* pTask) const
{
	return GetTree(iTreeIndex)->GetHasTask(pTask);
}

////////////////////////////////////////////////////////////////////////////////

inline void aiTaskManager::AbortTasksWithPriority(const s32 iTreeIndex, const s32 iPriority)
{
	GetTree(iTreeIndex)->AbortTasksWithPriority(iPriority);
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskManager::FindTaskByTypeActive(const s32 iTreeIndex, const s32 iTaskType) const
{
	return GetTree(iTreeIndex)->FindTaskByTypeActive(iTaskType);
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskManager::FindTaskByTypeWithPriority(const s32 iTreeIndex, const s32 iTaskType, const s32 iPriority) const
{
	return GetTree(iTreeIndex)->FindTaskByTypeWithPriority(iTaskType, iPriority);
}

#if !__FINAL

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTaskManager::GetHistory(const s32 iTreeIndex, const s32 iHistoryIndex) const
{
	return GetTree(iTreeIndex)->GetHistory(iHistoryIndex);
}

#endif // !__FINAL

} // namespace rage

#endif // AI_TASK_MANAGER_H
