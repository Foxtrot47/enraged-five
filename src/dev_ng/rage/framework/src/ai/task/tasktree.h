//
// ai/tasktree.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef AI_TASKTREE_H
#define AI_TASKTREE_H

// Rage headers
#include "atl/array.h"

// Framework headers
#include "ai/task/taskchannel.h"
#include "ai/task/taskspew.h"
#include "ai/task/taskregdref.h"

namespace rage
{

// Forward declarations
class aiTask;
class fwEntity;

class aiTaskTree
{
public:

#if !__FINAL
	// PURPOSE: Debug spew state transitions to tty
	static bool ms_bSpewStateTransToTTY;
	static fwEntity* ms_pFocusEntity;
#endif // !__FINAL

	aiTaskTree(fwEntity* pEntity, const s32 iNumberOfPriorities);
	virtual ~aiTaskTree();

	// PURPOSE: Manages the tasks attached to this task tree. If a Task is finished then its 
	// parent finds a new task and so on. If the active task finishes then the task is deleted completely.
	virtual void Process(float fTimeStep) = 0;

	// PURPOSE: Sets a new task in a specified slot. It takes as parameters the task you want
	// to set, the task priority (slot) and if you want to force the task to be set.
	// This function will delete the previous task. If NULL is sent as the new
	// task then this function just deletes the old task. If the task is the same
	// type then the new task is ignored unless bForceNewTask is set to true.
	virtual void SetTask(aiTask* pTask, const s32 iPriority, const bool bForceNewTask = false);

	// PURPOSE: Get the current task at supplied priority
	aiTask* GetTask(const s32 iPriority) const;

	// PURPOSE: Returns the task at the bottom of the task chain of the default task
	aiTask* GetLeafTask(const s32 iPriority) const;

	// PURPOSE: If the  task is set then clear it, returns true if a task is cleared
	bool ClearTask(const s32 iPriority);

	// PURPOSE: Returns the active task. The Active task is the task with the highest
	// priority with event response being the highest priority
	aiTask* GetActiveTask() const;

	// PURPOSE: Returns the task at the bottom of the task chain of the active task
	aiTask* GetActiveLeafTask() const;

	// PURPOSE: Gets the priority level of the current active task
	s32 GetActiveTaskPriority() const;

	// PURPOSE: Calculates the priority level of the current active task
	s32 ComputeActiveTaskPriority() const;

	// PURPOSE: Gets the total number of priority levels
	s32 GetPriorityCount() const;

	// PURPOSE: Query if the specified task is owned by this task tree
	bool GetHasTask(const aiTask* pTask) const;

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Abort and delete all tasks above the specified priority.
	void AbortTasksAbovePriority(const s32 iPriority);

	// PURPOSE: Abort and delete all tasks at the specified priority
	void AbortTasksWithPriority(const s32 iPriority);

	// PURPOSE: Abort and delete all tasks associated with this task tree
	void AbortTasks();

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: Find task within active tasks, task chain
	aiTask* FindTaskByTypeActive(const s32 iTaskType) const;

	// PURPOSE: Find task of specified type at any priority.
	aiTask* FindTaskByTypeWithAnyPriority(const s32 iTaskType) const;

	// PURPOSE: Find task of specified priority within task chain
	aiTask* FindTaskByTypeWithPriority(const s32 iTaskType, const s32 iPriority) const;

	////////////////////////////////////////////////////////////////////////////////

	// PURPOSE: returns true if the tree is a clone tree
	virtual const bool IsCloneTree() const { return false; }

#if !__FINAL
	// PURPOSE: Output task hierarchy to TTY
	void SpewAllTasks(aiTaskSpew::SpewType type) const;

	enum { MAX_TASK_HISTORY = 8 };
	// PURPOSE: Get the task history entry specified
	s32 GetHistory(const s32 iHistoryIndex) const;
#endif // !__FINAL

	// PURPOSE: Defer task deletions to a later time
	static void SetAllowDelete(bool allowDelete);

protected:

	// PURPOSE: Updates the FSM tasks
	virtual void UpdateTasks(const bool bDontDeleteHeadTask, const s32 iDefaultTaskPriority /* = -1 */, float timeStep);

	enum UpdateResult
	{
		UR_CONTINUED = 0,
		UR_QUIT,
	};

	// PURPOSE: Update an individual task
	virtual UpdateResult UpdateTask(aiTask* pTask, float timeStep);

	// PURPOSE: Add sub task to task
	void AddSubTask(aiTask* pTask, aiTask* pSubTask);

	// PURPOSE: Delete task and all sub tasks
	void DeleteTask(aiTask* pTask);

	//PURPOSE: Returns the default task for m_pEntity
	virtual aiTask* GetDefaultTask();

    //PURPOSE: Callback informing derived class when a new subtask has been added
    virtual void OnNewSubTaskCreated(aiTask* UNUSED_PARAM(pTask), aiTask* UNUSED_PARAM(pSubTask)) {}

	virtual void OnTaskDeleted(aiTask* UNUSED_PARAM(pTask)) {}

#if !__FINAL
	// PURPOSE: Output task hierarchy to TTY
	void SpewTaskHierarchy(aiTask* pTask, aiTaskSpew::SpewType type) const;
#endif // !__FINAL

//private:
protected:

	// PURPOSE: Owner entity
	fwEntity* m_pEntity; 

	// PURPOSE: Active priority
	s32 m_iActivePriority;

	// PURPOSE: Priority being processed if we are currently performing UpdateTask
	s32 m_iPriorityBeingProcessed;

	// PURPOSE: Used to detect infinite task updates
	s32 m_iDeadman;

	static const int kMaxPriorities = 5;	// Note: increase if needed.
	typedef atFixedArray<RegdaiTask, kMaxPriorities> TaskArray;

	// PURPOSE: Tasks running
	TaskArray m_Tasks;

	static const u32 TASK_DELETION_QUEUE_SIZE = 1280;
	static atFixedArray<aiTask*, TASK_DELETION_QUEUE_SIZE> sm_TaskDeletionQueue;
	static bool sm_AllowDelete;

#if !__FINAL 
	s32 m_TaskHistory[MAX_TASK_HISTORY];
#endif // !__FINAL
};

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskTree::GetTask(const s32 iPriority) const
{
	taskFatalAssertf(iPriority >= 0 && iPriority < m_Tasks.GetCount(), "iPriority [%d] out of range [0,%d]", iPriority, m_Tasks.GetCount());
	return m_Tasks[iPriority];
}

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskTree::GetActiveTask() const
{
	s32 iPriority = GetActiveTaskPriority();
	if(iPriority > -1)
	{
		return m_Tasks[iPriority];
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTaskTree::GetActiveTaskPriority() const
{
	return m_iActivePriority;
}

////////////////////////////////////////////////////////////////////////////////

inline s32 aiTaskTree::GetPriorityCount() const
{
	return m_Tasks.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

#if !__FINAL

inline s32 aiTaskTree::GetHistory(const s32 iHistoryIndex) const
{
	taskFatalAssertf(iHistoryIndex >= 0 && iHistoryIndex < MAX_TASK_HISTORY, "aiTaskTree::GetHistory: iHistoryIndex [%d] out of range [0,%d]", iHistoryIndex, MAX_TASK_HISTORY);
	return m_TaskHistory[iHistoryIndex];
}

#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

inline aiTask* aiTaskTree::GetDefaultTask()
{
	return NULL;
}

} // namespace rage

#endif // AI_TASKTREE_H
