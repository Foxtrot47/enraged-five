//
// phframework/inst.cpp : framework version of physics inst
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "inst.h"

namespace rage {

phfwInst::phfwInst(int type) : m_type(type)
{
	SetEntity(NULL);
}

phfwInst::~phfwInst()
{
	Assert(!IsInLevel());
}

// framework instances use the user data pointer to store their parent entity (may be null)
void phfwInst::SetEntity(fwEntity *entity) 
{ 
	SetUserData((void*)entity); 
	SetInstFlag(phfwInst::FLAG_USERDATA_ENTITY, (entity != NULL));
}

} // namespace rage