//
// phframework/fraginst.cpp : framework version of fragment inst
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fraginst.h"
#include "inst.h"

namespace rage {

// --- phfwFragInst -------------------------------------------------------------------

phfwFragInst::phfwFragInst(const fragType* type, const Matrix34& matrix, u32 guid) : fragInst(type, matrix, guid)
{
	SetEntity(NULL);
}

phfwFragInst::phfwFragInst() : fragInst()
{
	SetEntity(NULL);
}

phfwFragInst::~phfwFragInst()
{
	Assert(!IsInLevel());
}

// framework instances use the user data pointer to store their parent entity (may be null)
void phfwFragInst::SetEntity(fwEntity *entity) 
{ 
	SetUserData((void*)entity); 
	SetInstFlag(phfwInst::FLAG_USERDATA_ENTITY, (entity != NULL));
}

} // namespace rage