//
// phframework/fraginst.h : framework version of fragment inst
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef PHFRAMEWORK_FRAGINST_H_
#define PHFRAMEWORK_FRAGINST_H_

#include "fragment/instance.h"

namespace rage {

class fwEntity;

//
// PURPOSE
//	Version of fragment inst used by the framework
class phfwFragInst : public fragInst
{
public:
	phfwFragInst (const fragType* type, const Matrix34& matrix, u32 guid = 0);
	phfwFragInst ();
	virtual ~phfwFragInst();

	// PURPOSE: Set entity linked to this physics inst
	// NOTES: entity pointer is stored in the userdata pointer in phInst
	void SetEntity(fwEntity *entity);

	// PURPOSE: physics inst class type accessor
	virtual int GetClassType () const = 0;
};

} // namespace rage

#endif // PHFRAMEWORK_INST_H_