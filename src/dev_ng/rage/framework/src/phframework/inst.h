//
// phframework/inst.h : framework version of physics inst
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef PHFRAMEWORK_INST_H_
#define PHFRAMEWORK_INST_H_

#include "physics/inst.h"

namespace rage {

class fwEntity;

//
// PURPOSE
//	Version of physics inst used by the framework
class phfwInst : public phInst
{
public:
	phfwInst (int type);
	virtual ~phfwInst ();

	enum
	{
		FLAG_USERDATA_ENTITY			= BIT(9),	// user data contains owner entity pointer
		FLAG_USER_0						= BIT(10)
	};

	// PURPOSE: Set entity linked to this physics inst
	// NOTES: entity pointer is stored in the userdata pointer in phInst
	void SetEntity(fwEntity *entity);

	// PURPOSE: physics inst class type accessor
	virtual int GetClassType () const { return m_type; }

private:
	int m_type;
};

// check we're using the correct first available user data flag
CompileTimeAssert(phfwInst::eInstFlags(phfwInst::FLAG_USERDATA_ENTITY) == phInst::FLAG_USER_0);

} // namespace rage

#endif // PHFRAMEWORK_INST_H_