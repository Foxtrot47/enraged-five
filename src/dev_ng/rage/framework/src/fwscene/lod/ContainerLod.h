/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/lod/ContainerLod.h
// PURPOSE : stores lod parent information for multi-container lodding
// AUTHOR :  Ian Kiigan
// CREATED : 08/04/10
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _FWSCENE_LOD_CONTAINERLOD_H_
#define _FWSCENE_LOD_CONTAINERLOD_H_

#include "atl/map.h"
#include "fwtl/Pool.h"
#include "string/stringhash.h"

namespace rage {

// describes the location of a container LOD parent, for auto-attachment
class fwContainerLod
{
public:
	FW_REGISTER_CLASS_POOL(fwContainerLod);

	fwContainerLod() : m_iplIndex(-1), m_instIndex(0) {}
	fwContainerLod(s32 iplIndex, u32 instIndex) : m_iplIndex(iplIndex), m_instIndex(instIndex) {}

	s32	m_iplIndex;
	u32	m_instIndex;
};

// stores the map between IPL and corresponding container LOD parents
class fwContainerLodMap
{
public:
	inline void Reset();
	bool AddParentLink(u32 nIplNameHash, s32 iplIndex, u32 instIndex);
	fwContainerLod* GetContainerLod(const char* pszIplFileName) const { return GetContainerLod(atStringHash(pszIplFileName)); }
	fwContainerLod* GetContainerLod(u32 nIplNameHash) const;

private:
	atMap<u32, s32> m_parentMap;	// map of ipl file name hashes to container lod pool indices
};

inline void fwContainerLodMap::Reset()
{
	fwContainerLod::GetPool()->Reset();
	m_parentMap.Reset();
}

extern fwContainerLodMap g_ContainerLodMap;

};	//namespace rage

#endif //_FWSCENE_LOD_CONTAINERLOD_H_