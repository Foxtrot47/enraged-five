/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/lod/ContainerLod.cpp
// PURPOSE : stores lod parent information for multi-container lodding
// AUTHOR :  Ian Kiigan
// CREATED : 08/04/10
//
/////////////////////////////////////////////////////////////////////////////////

// game  headers
#include "fwscene/lod/ContainerLod.h"

namespace rage {

FW_INSTANTIATE_CLASS_POOL(fwContainerLod, 104, atHashString("fwContainerLod",0xd29eb05b));

fwContainerLodMap g_ContainerLodMap;

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddParentLink
// PURPOSE:		adds a link between a parent entity and the specified IPL file name hash.
//				returns true if successful (ie if no prior link already exists) or false otherwise
//////////////////////////////////////////////////////////////////////////
bool fwContainerLodMap::AddParentLink(u32 nIplNameHash, s32 iplIndex, u32 instIndex)
{
	// make sure it isn't already in the hash map
	const s32* pResult = m_parentMap.Access(nIplNameHash);
	if (!pResult)
	{
		fwContainerLod* pContainerLod = rage_new fwContainerLod(iplIndex, instIndex);
		s32 poolIndex = fwContainerLod::GetPool()->GetJustIndex(pContainerLod);
		m_parentMap.Insert(nIplNameHash, poolIndex);
		return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetContainerLod
// PURPOSE:		Retrieve the container LOD for a specified IPL file name, or NULL if none exists
//////////////////////////////////////////////////////////////////////////
fwContainerLod* fwContainerLodMap::GetContainerLod(u32 nIplNameHash) const
{
	const s32* pResult = m_parentMap.Access(nIplNameHash);
	if (pResult)
	{
		Assert(*pResult != -1);
		fwContainerLod* pLod = fwContainerLod::GetPool()->GetSlot(*pResult);
		return pLod;
	}
	return NULL;
}

};	//namespace rage