/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/lod/LodAttach.h
// PURPOSE : Responsible for allocation of nodes and attachment / detachment wrt LOD hierarchies
// AUTHOR :  Ian Kiigan
// CREATED : 04/03/10
//
/////////////////////////////////////////////////////////////////////////////////

#include "fwscene/lod/LodAttach.h"
#include "fwscene/world/EntityDesc.h"
#include "diag/art_channel.h"
#include "entity/archetypemanager.h"
#include "entity/archetype.h"
#include "streaming/streamingvisualize.h"

#define NG_HACK_UPSCALE_ORPHANS (1)

namespace rage
{

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddEntityToLodHierarchy
// PURPOSE:		Adds new instance entity to lod hierarchy and sets up data
//////////////////////////////////////////////////////////////////////////
void fwLodAttach::AddEntityToLodHierarchy(fwEntity* pEntity, float fLodDist, float fChildLodDist, u32 numChildren, fwEntity* pLod, bool bContainerLod, bool bAllowOrphanScaling)
{
	if (pEntity)
	{
		const fwArchetype* pArchetype = pEntity->GetArchetype();

		fwLodData& lodData = pEntity->GetLodData();

#if __ASSERT
		if (!lodData.IsHighDetail() && !numChildren && !bContainerLod)
		{
			artAssertf( false, "%s is low-lod type %s yet has no children. Children set to not export? Data/Tools bug",
				pEntity->GetModelName(), lodData.GetLodTypeName() );
		}
#endif
		// patch up broken data to be an orphan HD
		if (lodData.IsLoddedHd())
		{
			if (!Verifyf(pLod!=NULL, "%s should have a lod but does not", pEntity->GetModelName()))
			{
				lodData.SetLodType(LODTYPES_DEPTH_ORPHANHD);
			}
		}
		Assertf(numChildren<=LODTYPES_MAX_CHILDREN, "%s has %d lod children, but max supported is %d",
			pEntity->GetModelName(), numChildren, LODTYPES_MAX_CHILDREN);


		// if no lod dist specified in instance, use the one in model info
		float fInstanceLodDist = (fLodDist>0.0f) ? fLodDist : pArchetype->GetLodDistanceUnscaled();
		
#if NG_HACK_UPSCALE_ORPHANS
		// TODO - remove post GTAV
		// (or at least make it data driven)
		if (bAllowOrphanScaling && lodData.IsOrphanHd()) { fInstanceLodDist *= 1.5f; }
#endif

		u32 lodDist = (u32) fInstanceLodDist;

		// if parent already specifies the correct child lod distance to use, use that instead
		if (pLod && pLod->GetLodData().GetKnowsChildLodDist())
		{
			lodDist = pLod->GetLodData().GetChildLodDistance();
		}
		pEntity->SetLodDistance(lodDist);

		// if a part of a lod hierarchy
		if (pLod || numChildren || bContainerLod)
		{
			// set lod distance, position etc
			lodData.SetNumChildren( numChildren );
			lodData.SetKnowsChildLodDist( fChildLodDist > 0.0f );
			lodData.SetChildLodDistance( (u32) fChildLodDist );
	
			// if attaching to a parent
			if (pLod)
			{
				pLod->GetLodData().AttachChild( lodData );
				pEntity->SetLod( pLod );
				STRVIS_SET_LOD_PARENT(pEntity, pLod);

				const fwFlags16& visMask = pEntity->GetRenderPhaseVisibilityMask();
				if (visMask.IsFlagSet( _GTA5_VIS_PHASE_MASK_WATER_REFLECTION ) && !visMask.IsFlagSet( _GTA5_VIS_PHASE_MASK_WATER_REFLECTION_PROXY ))
				{
					pLod->SetBaseFlag(fwEntity::HAS_NON_WATER_REFLECTION_PROXY_CHILD);
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveEntityFromLodHierarchy
// PURPOSE:		Detaches an entity's lod node from its hierarchy and destroys the node
//////////////////////////////////////////////////////////////////////////
void fwLodAttach::RemoveEntityFromLodHierarchy(fwEntity* pEntity)
{
	fwLodData& lodData = pEntity->GetLodData();
	if ( lodData.HasLod() )
	{
		fwEntity* pParent = (fwEntity*) lodData.GetLod();
		pParent->GetLodData().DetachChild();
		lodData.SetLod( NULL );

		Assertf(lodData.GetNumAttachedChildren()==0, "LodAttach: removing %s from hierarchy, but it still has %d active children",
			pEntity->GetModelName(), lodData.GetNumAttachedChildren());
	}
}

}	//namespace rage
