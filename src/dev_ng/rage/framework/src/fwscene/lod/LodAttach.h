/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/lod/LodAttach.h
// PURPOSE : Responsible for allocation of nodes and attachment / detachment wrt LOD hierarchies
// AUTHOR :  Ian Kiigan
// CREATED : 04/03/10
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _FWSCENE_LOD_LODATTACH_H_ 
#define _FWSCENE_LOD_LODATTACH_H_

#include "entity/entity.h"
#include "fwscene/lod/LodTypes.h"

namespace rage
{

// simple interface for lod node attachment / detachment
class fwLodAttach
{
public:
	// node attach / detach
	static void AddEntityToLodHierarchy(fwEntity* pEntity, float fLodDist, float fChildLodDist, u32 numChildren, fwEntity* pLod, bool bContainerLod, bool bAllowOrphanScaling);
	static void	RemoveEntityFromLodHierarchy(fwEntity* pEntity);
};



} //namespace rage

#endif	//_FWSCENE_LOD_LODATTACH_H_
