//
// fwscene/lod/LodTypes.h : types types and defines related to LOD hierarchies and alpha update
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef _FWSCENE_LOD_LODTYPES_H_
#define _FWSCENE_LOD_LODTYPES_H_

#include "math/amath.h"

namespace rage
{

// currently supporting 6 levels of detail.
// if you change this be sure allocate more/less bits
// for the m_lodLevel value in fwLodData
#define LODTYPES_MAX_NUM_LEVELS				(6)

#define LODTYPES_MAX_SPEED_TIMEFADE			(80.0f)
#define LODTYPES_FADE_SMALL_DIST			(5)
#define LODTYPES_FADE_SMALL_DISTF			(5.0f)
#define LODTYPES_FADE_DIST					(20)
#define LODTYPES_FADE_DISTF					(20.0f)
#define LODTYPES_ALPHA_VISIBLE				(255)
#define LODTYPES_ALPHA_INVISIBLE			(0)
#define LODTYPES_ALPHA_VISIBLEF				(255.0f)
#define LODTYPES_ALPHA_INVISIBLEF			(0.0f)
#define LODTYPES_ALPHA_STEP					(32)
#define LODTYPES_FADETOZERO_ALPHA_STEP		(16)
#define LODTYPES_LODDIST_BIT_COUNT			(16)
#define LODTYPES_MAX_LODDIST				((1 << LODTYPES_LODDIST_BIT_COUNT) - 1)
#define LODTYPES_MAX_CHILDREN				(255)
#define LODTYPES_TARGET_MILLIS				(33)
#define LODTYPES_SORTVAL_BITS				(3)

enum eLodType
{
	LODTYPES_DEPTH_HD = 0,			// highest level of detail in lod hierarchy
	LODTYPES_DEPTH_LOD,				// LOD
	LODTYPES_DEPTH_SLOD1,			// SLOD
	LODTYPES_DEPTH_SLOD2,			// multi-container LOD
	LODTYPES_DEPTH_SLOD3,			// multi-container LOD
	LODTYPES_DEPTH_ORPHANHD,
	LODTYPES_DEPTH_SLOD4,

	LODTYPES_DEPTH_TOTAL
};

enum eLodTypeFlags
{
	LODTYPES_FLAG_ORPHANHD	= 1 << LODTYPES_DEPTH_ORPHANHD,
	LODTYPES_FLAG_HD		= 1 << LODTYPES_DEPTH_HD,
	LODTYPES_FLAG_LOD		= 1 << LODTYPES_DEPTH_LOD,
	LODTYPES_FLAG_SLOD1		= 1 << LODTYPES_DEPTH_SLOD1,
	LODTYPES_FLAG_SLOD2		= 1 << LODTYPES_DEPTH_SLOD2,
	LODTYPES_FLAG_SLOD3		= 1 << LODTYPES_DEPTH_SLOD3,
	LODTYPES_FLAG_SLOD4		= 1 << LODTYPES_DEPTH_SLOD4
};

#define LODTYPES_MASK_ALL	 ((1 << LODTYPES_DEPTH_TOTAL) - 1)

class fwLodData
{
public:
	fwLodData() :
		m_lodType(LODTYPES_DEPTH_ORPHANHD),
		m_bVisibleInGbufPhase(false),
		m_bLoaded(false),
		m_bKnowsCorrectChildDist(false),
		m_lodDistance(0),
		m_childLodDistance(0),
		m_alpha(LODTYPES_ALPHA_VISIBLE),
		m_numChildren(0),
		m_numAttachedChildren(0),
		m_bIsTree(false),
		m_bForceOnInCascadeShadows(false),
		m_bResetAlphaNextUpdate(false),
		m_bDisableReset(false),
		m_bFadeToZero(false),
		m_bHasChildDrawing(false),
		m_bInteriorLod(false),
		m_pLod(NULL)
		{}

	// lod type
	u32			GetLodType() const							{ return m_lodType; }
	u32			GetLodFlag() const							{ return ( 1 << m_lodType ); }
	void		SetLodType(u32 lodType)						{ m_lodType = lodType; }
	u32			GetChildLodType() const						{ return ms_childDepths[m_lodType]; }
	bool		IsOrphanHd() const							{ return GetLodType()==LODTYPES_DEPTH_ORPHANHD; }
	bool		IsLoddedHd() const							{ return GetLodType()==LODTYPES_DEPTH_HD; }
	bool		IsLod() const								{ return GetLodType()==LODTYPES_DEPTH_LOD; }
	bool		IsSlod1() const								{ return GetLodType()==LODTYPES_DEPTH_SLOD1; }
	bool		IsSlod2() const								{ return GetLodType()==LODTYPES_DEPTH_SLOD2; }
	bool		IsSlod3() const								{ return GetLodType()==LODTYPES_DEPTH_SLOD3; }
	bool		IsSlod4() const								{ return GetLodType()==LODTYPES_DEPTH_SLOD4; }
	bool		IsHighDetail() const						{ return IsLoddedHd()||IsOrphanHd(); }
	bool		IsContainerLod() const						{ return IsSlod2()||IsSlod3()||IsSlod4(); }
	bool		IsRootLod() const							{ return !HasLod() && HasChildren(); }


	// alpha and lod distance
	u8			GetAlpha() const							{ return (u8) m_alpha; }
	void		SetAlpha(u32 alpha)							{ m_alpha = alpha; }
	u32			GetLodDistance() const						{ return m_lodDistance; }
	void		SetLodDistance(u32 lodDist)					{ m_lodDistance = lodDist; }
	u32			GetChildLodDistance() const					{ return m_childLodDistance; }
	void		SetChildLodDistance(u32 lodDist)			{ m_childLodDistance = lodDist; }
	void		SetKnowsChildLodDist(bool bKnowsDist)		{ m_bKnowsCorrectChildDist=bKnowsDist;}
	bool		GetKnowsChildLodDist() const				{ return m_bKnowsCorrectChildDist; }

	// lod attachment
	bool		HasLod() const								{ return m_pLod!=NULL; }
	void		SetLod(void* pLod)							{ m_pLod = pLod; }
	void*		GetLod() const								{ return m_pLod; }
	bool		HasChildren() const							{ return !IsHighDetail(); }
	u32			GetNumChildren() const						{ return m_numChildren; }
	void		SetNumChildren(u32 numChildren)				{ FastAssert(numChildren<=LODTYPES_MAX_CHILDREN); m_numChildren = numChildren; }
	u32			GetNumAttachedChildren() const				{ return m_numAttachedChildren; }
	void		SetNumAttachedChildren(u32 numChildren)		{ m_numAttachedChildren = numChildren; }
	bool		AllChildrenAttached()						{ return GetNumAttachedChildren()==GetNumChildren(); }


	void AttachChild(fwLodData& childLodData)
	{
		if ( !GetKnowsChildLodDist() )
		{
			m_childLodDistance = Max( m_childLodDistance, childLodData.GetLodDistance() );
		}
		m_numAttachedChildren++;
		m_numChildren = Max( m_numAttachedChildren, m_numChildren );
	}

	void DetachChild()
	{
		FastAssert(m_numAttachedChildren > 0);
		m_numAttachedChildren--;
	}
	
	// general
	inline bool		IsLoaded() const							{ return m_bLoaded; }
	inline void		SetLoaded(bool bLoaded)						{ m_bLoaded = bLoaded; }
	inline bool		IsVisible() const							{ return m_bVisibleInGbufPhase; }
	inline void		SetVisible(bool bVisible)					{ m_bVisibleInGbufPhase = bVisible; }
	inline bool		IsTree() const								{ return m_bIsTree; }
	inline void		SetIsTree(bool bIsTree)						{ m_bIsTree = bIsTree; }
	inline bool		GetForceOnInCascadeShadows() const			{ return m_bForceOnInCascadeShadows; }
	inline void		SetForceOnInCascadeShadows(bool bForceOn)	{ m_bForceOnInCascadeShadows = bForceOn; }
	inline bool		GetResetAlpha() const						{ return m_bResetAlphaNextUpdate; }
	inline void		SetResetAlpha(bool bReset)					{ m_bResetAlphaNextUpdate = bReset; }
	inline bool		IsResetDisabled() const						{ return m_bDisableReset; }
	inline void		SetResetDisabled(bool bDisable)				{ m_bDisableReset = bDisable; }
	inline bool		GetFadeToZero() const						{ return m_bFadeToZero; }
	inline void		SetFadeToZero(bool bFadeToZero)				{ m_bFadeToZero = bFadeToZero; }
	inline bool		GetChildDrawing() const						{ return m_bHasChildDrawing; }
	inline void		SetChildDrawing(bool bChildDrawing)			{ m_bHasChildDrawing = bChildDrawing; }
	inline bool		IsFading() const							{ return ( m_alpha>LODTYPES_ALPHA_INVISIBLE && m_alpha<LODTYPES_ALPHA_VISIBLE ); }
	inline bool		IsParentOfInterior() const					{ return m_bInteriorLod; }
	inline void		SetIsParentOfInterior(bool bInteriorLod)	{ m_bInteriorLod = bInteriorLod; }

	static u32		GetSortVal(u32 lodType)						{ return ms_sortVals[lodType]; }

private:

	void* m_pLod;

	u32 m_lodDistance					: LODTYPES_LODDIST_BIT_COUNT; // allows up to LODTYPES_MAX_LODDIST meters
	u32 m_childLodDistance				: LODTYPES_LODDIST_BIT_COUNT; // allows up to LODTYPES_MAX_LODDIST meters

	u32 m_alpha							: 8;
	u32 m_numChildren					: 8;	//NB allows up to LODTYPES_MAX_CHILDREN
	u32 m_numAttachedChildren			: 8;	//NB allows up to LODTYPES_MAX_CHILDREN
	u32 m_lodType						: 8;	// could be smaller - values specified by eLodType

	u32 m_bLoaded						: 1;
	u32 m_bForceOnInCascadeShadows		: 1;
	u32 m_bResetAlphaNextUpdate			: 1;
	u32 m_bDisableReset					: 1;
	u32 m_bVisibleInGbufPhase			: 1;
	u32 m_bHasChildDrawing				: 1;
	u32 m_bFadeToZero					: 1;
	u32 m_bKnowsCorrectChildDist		: 1;
	u32 m_bIsTree						: 1;
	u32 m_bInteriorLod					: 1;

	static u32 ms_childDepths[LODTYPES_DEPTH_TOTAL];
	static u32 ms_sortVals[LODTYPES_DEPTH_TOTAL];

	//////////////////////////////////////////////////////////////////////////
#if !__FINAL
public:
	static const char* ms_apszLevelNames[LODTYPES_DEPTH_TOTAL];
	const char* GetLodTypeName() const { return ms_apszLevelNames[m_lodType]; }
#endif	//!__FINAL
	//////////////////////////////////////////////////////////////////////////

};


}	//namespace rage

#endif //_FWSCENE_LOD_LODTYPES_H_
