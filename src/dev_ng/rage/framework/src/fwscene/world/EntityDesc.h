/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/world/EntityDesc.h
// PURPOSE : Descriptor for entities within world representation
// AUTHOR :  John Whyte, Ian Kiigan, Alessandro Piva
// CREATED : 11/02/09
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _INC_ENTITYDESC_H_
#define _INC_ENTITYDESC_H_

#include "vector/vector3.h"
#include "math/float16.h"
#include "fwutil/flags.h"
#include "fwscene/lod/LodTypes.h"
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"

namespace rage {

class fwEntity;

enum
{
	_GTA5_VIS_PHASE_GBUF										= 1,
	_GTA5_VIS_PHASE_CASCADE_SHADOWS								= 2,
	_GTA5_VIS_PHASE_PARABOLOID_SHADOWS							= 3,
	_GTA5_VIS_PHASE_PARABOLOID_REFLECTION						= 4,
	_GTA5_VIS_PHASE_WATER_REFLECTION							= 5,
	_GTA5_VIS_PHASE_MIRROR_REFLECTION							= 6,

	_GTA5_VIS_PHASE_FLAG_SHADOW_PROXY							= 10,
	_GTA5_VIS_PHASE_FLAG_WATER_REFLECTION_PROXY					= 11,
	_GTA5_VIS_PHASE_FLAG_WATER_REFLECTION_PROXY_PRE_REFLECTED	= 12,

	_GTA5_VIS_PHASE_MASK_GBUF									= 1 << _GTA5_VIS_PHASE_GBUF,
	_GTA5_VIS_PHASE_MASK_CASCADE_SHADOWS						= 1 << _GTA5_VIS_PHASE_CASCADE_SHADOWS,
	_GTA5_VIS_PHASE_MASK_PARABOLOID_SHADOWS						= 1 << _GTA5_VIS_PHASE_PARABOLOID_SHADOWS,
	_GTA5_VIS_PHASE_MASK_PARABOLOID_REFLECTION					= 1 << _GTA5_VIS_PHASE_PARABOLOID_REFLECTION,
	_GTA5_VIS_PHASE_MASK_WATER_REFLECTION						= 1 << _GTA5_VIS_PHASE_WATER_REFLECTION,
	_GTA5_VIS_PHASE_MASK_MIRROR_REFLECTION						= 1 << _GTA5_VIS_PHASE_MIRROR_REFLECTION,

	_GTA5_VIS_PHASE_MASK_SHADOW_PROXY							= 1 << _GTA5_VIS_PHASE_FLAG_SHADOW_PROXY,
	_GTA5_VIS_PHASE_MASK_WATER_REFLECTION_PROXY					= 1 << _GTA5_VIS_PHASE_FLAG_WATER_REFLECTION_PROXY,
	_GTA5_VIS_PHASE_MASK_WATER_REFLECTION_PROXY_PRE_REFLECTED	= 1 << _GTA5_VIS_PHASE_FLAG_WATER_REFLECTION_PROXY_PRE_REFLECTED,

	_GTA5_VIS_PHASE_MASK_SHADOWS =
		_GTA5_VIS_PHASE_MASK_CASCADE_SHADOWS |
		_GTA5_VIS_PHASE_MASK_PARABOLOID_SHADOWS,

	_GTA5_VIS_PHASE_MASK_REFLECTIONS =
		_GTA5_VIS_PHASE_PARABOLOID_REFLECTION |
		_GTA5_VIS_PHASE_MIRROR_REFLECTION,
};

struct fwEntityDescFlags
{
	u16		m_nEntityType				:	4;		// cached entity type value from CEntity
	u16		m_bDontCullSmallShadow		:	1;
	u16		m_bNeverDummy				:	1;		// fed through to post-vis code
	u16		m_bIsADoor					:	1;		
	u16		m_bIsVisible				:	1;
	u16		m_bIsSearchable				:	1;
	u16		m_bLodType					:	3;		// NOTE -- these are accessed by vector code, so they cannot be moved! talk to David Ely
	u16		m_bPedInVehicle				:	1;
	u16		m_bRootLod					:	1;
	u16		m_bIsHDTexCapable			:	1;
	u16		m_bIsStraddlingPortal		:	1;
};
CompileTimeAssert(sizeof(fwEntityDescFlags) == sizeof(u16));

// 16 byte alignment is very desirable for copy purposes - these are going to get a lot of copying about
class fwEntityDesc
{
	friend class fwSoAEntityContainer;

	enum eFlags2
	{
		FLAGS2_USE_MAX_DISTANCE_FOR_WATER_REFLECTION	= BIT(0),
		FLAGS2_IS_PROCEDURAL_ENTITY						= BIT(1)
	};

public:
	// entity
	void		SetEntity(fwEntity* const pEntity)				{ m_pEntity = pEntity; }
	fwEntity*	GetEntity() const								{ return m_pEntity; }
	void		SetEntityType(const u32 nType)					{ sFlags.m_nEntityType = nType; }
	u32			GetEntityType() const							{ return sFlags.m_nEntityType; }

	void				SetRenderPhaseVisibilityMask(const fwFlags16& value)	{ m_phaseVisibilityMask = value; }
	const fwFlags16&	GetRenderPhaseVisibilityMask() const					{ return m_phaseVisibilityMask; }
	fwFlags16&			GetRenderPhaseVisibilityMask()							{ return m_phaseVisibilityMask; }

	void				SetVisibilityType(const u16 value)						{ m_visibilityType = value; }
	u16					GetVisibilityType() const								{ return m_visibilityType; }

	// visibility
	void		SetIsVisible(const bool isVisible)				{ sFlags.m_bIsVisible = isVisible ? 1 : 0; }
	bool		IsVisible() const								{ return sFlags.m_bIsVisible; }

	// searchable-ness
	void		SetIsSearchable(const bool isSearchable)		{ sFlags.m_bIsSearchable = isSearchable ? 1 : 0; }
	bool		IsSearchable() const							{ return sFlags.m_bIsSearchable; }

	// shadows
	void		SetDontCullSmallShadow(const bool bFlag)		{ sFlags.m_bDontCullSmallShadow = bFlag; }
	u32			GetDontCullSmallShadow() const					{ return sFlags.m_bDontCullSmallShadow; }

	// proxies
	bool		GetIsShadowProxy() const						{ return m_phaseVisibilityMask.IsFlagSet(_GTA5_VIS_PHASE_MASK_SHADOW_PROXY); }
	bool		GetIsWaterReflectionProxy() const				{ return m_phaseVisibilityMask.IsFlagSet(_GTA5_VIS_PHASE_MASK_WATER_REFLECTION_PROXY); }
#if __BANK
	bool		GetIsReflectionProxy() const					{ return m_phaseVisibilityMask.IsFlagSet(_GTA5_VIS_PHASE_MASK_REFLECTIONS|_GTA5_VIS_PHASE_MASK_WATER_REFLECTION) && !m_phaseVisibilityMask.IsFlagSet(_GTA5_VIS_PHASE_MASK_GBUF); }
#endif // __BANK

	// bounds
	__forceinline void			SetApproxBoundingBoxAndSphereAndVecToLod(const spdAABB& aabb, const spdSphere& sphere, Vec3V_In vLodPos);
	__forceinline void			GetApproxBoundingBox(spdAABB& aabb) const;
	__forceinline void			GetApproxBoundingSphere(spdSphere& sphere) const;
	__forceinline Vec3V_Out		GetLodPosition() const;

	__forceinline Vec3V_Out		GetApproxCentre() const			{ return m_positionAndUnused.GetXYZ(); }
	__forceinline spdAABB		GetApproxBoundingBox() const	{ spdAABB box; GetApproxBoundingBox( box ); return box; }
	__forceinline spdSphere		GetApproxBoundingSphere() const	{ spdSphere sphere; GetApproxBoundingSphere( sphere ); return sphere; }

	// lod / alpha
	u32					GetLodDistCached() const						{ return m_nLodDistBackup; }
	void				SetLodDistCached(const u32 lodDist)				{ m_nLodDistBackup = static_cast<u16>(lodDist); }
	bool				IsHighDetail() const							{ return ( GetLodType()==LODTYPES_DEPTH_ORPHANHD || GetLodType()==LODTYPES_DEPTH_HD ); }
	bool				IsContainerLod() const							{ return ( GetLodType()==LODTYPES_DEPTH_SLOD2 || GetLodType()==LODTYPES_DEPTH_SLOD3 || GetLodType()==LODTYPES_DEPTH_SLOD4); }
	bool				IsLowDetail() const								{ return !IsHighDetail(); }
	u32					GetLodType() const								{ return sFlags.m_bLodType; }
	void				SetLodType(const u32 lodType)					{ sFlags.m_bLodType = lodType; }
	bool                GetIsPedInVehicle()	const						{ return sFlags.m_bPedInVehicle != 0; }
	void                SetIsPedInVehicle(bool pedIsInVehilce)			{ sFlags.m_bPedInVehicle = (u16)pedIsInVehilce; }
	bool				IsRootLod() const								{ return sFlags.m_bRootLod; }
	void				SetIsRootLod(bool bIsRoot)						{ sFlags.m_bRootLod = bIsRoot; }
	bool				IsHDTexCapable() const							{ return sFlags.m_bIsHDTexCapable; }
	void				SetHDTexCapable(bool hdTexCapable)				{ sFlags.m_bIsHDTexCapable = hdTexCapable; }
	bool				IsStraddlingPortal() const						{ return sFlags.m_bIsStraddlingPortal; }
	void				SetStraddlingPortal(bool straddlingPortal)		{ sFlags.m_bIsStraddlingPortal = straddlingPortal; }
	bool				IsSetNeverDummy() const							{ return sFlags.m_bNeverDummy; }
	void				SetNeverDummy(bool bNeverDummy)					{ sFlags.m_bNeverDummy = bNeverDummy; }
	void                SetIsADoor(bool bIsADoor)						{ sFlags.m_bIsADoor = bIsADoor;}
	bool                GetIsADoor() const								{ return sFlags.m_bIsADoor;}
	fwFlags16&			GetFlags2()										{ return ((fwFlags16*)&m_extentAndRadiusAndVecToLodAndFlags2)[7]; }
	const fwFlags16&	GetFlags2() const								{ return ((const fwFlags16*)&m_extentAndRadiusAndVecToLodAndFlags2)[7]; }
	void				SetUseMaxDistanceForWaterReflection(bool b)		{ GetFlags2().ChangeFlag(FLAGS2_USE_MAX_DISTANCE_FOR_WATER_REFLECTION, b); }
	bool				GetUseMaxDistanceForWaterReflection() const		{ return GetFlags2().IsFlagSet(FLAGS2_USE_MAX_DISTANCE_FOR_WATER_REFLECTION); }
	void				SetIsProcedural(bool b)							{ GetFlags2().ChangeFlag(FLAGS2_IS_PROCEDURAL_ENTITY, b); }
	bool				GetIsProcedural() const							{ return GetFlags2().IsFlagSet(FLAGS2_IS_PROCEDURAL_ENTITY); }

private:
	fwEntity*			m_pEntity;						// want to avoid using this! _Only_ used for prerender & final draw request
	fwFlags16			m_phaseVisibilityMask;
	u16					m_visibilityType;
	u16					m_nLodDistBackup;				// Note we don't need all 16 bits for the lod distance as it only use LODTYPES_LODDIST_BIT_COUNT (13)
	fwEntityDescFlags	sFlags;
	ATTR_UNUSED u32		m_padding1[1];

	Vec4V		m_positionAndUnused; // w-component is unused
	Vec4V		m_extentAndRadiusAndVecToLodAndFlags2; // packed 'extent,radius' and 'vecToLod,flags2' vectors
};

CompileTimeAssertSize(fwEntityDescFlags,2,2);
CompileTimeAssertSize(fwEntityDesc,48,64);

__forceinline void fwEntityDesc::SetApproxBoundingBoxAndSphereAndVecToLod(const spdAABB& aabb, const spdSphere& sphere, Vec3V_In vLodPos)
{
	const Vec3V		boxMin = aabb.GetMin();
	const Vec3V		boxMax = aabb.GetMax();
	const Vec3V		centre = (boxMax + boxMin) * ScalarV(V_HALF);
	const Vec3V		extent = (boxMax - boxMin) * ScalarV(V_HALF);
	const Vec3V		vToLod = vLodPos - centre;
	const ScalarV	radius = sphere.GetRadius() + Mag( centre - sphere.GetCenter() );
	const fwFlags16 flags2 = GetFlags2();

	m_positionAndUnused = Vec4V(centre, ScalarV(V_ZERO));
	m_extentAndRadiusAndVecToLodAndFlags2 = Float16Vec8Pack( Vec4V(extent, radius), Vec4V(vToLod, ScalarV(V_ZERO)) );

	GetFlags2() = flags2; // restore flags2
}

__forceinline void fwEntityDesc::GetApproxBoundingBox(spdAABB& aabb) const
{
	const Vec3V centre			= m_positionAndUnused.GetXYZ();
	const Vec4V extentAndRadius	= Float16Vec4UnpackFromXY( m_extentAndRadiusAndVecToLodAndFlags2 );
	const Vec3V extent			= extentAndRadius.GetXYZ();
	const Vec3V boxMin			= centre - extent;
	const Vec3V boxMax			= centre + extent;

	aabb.Set( boxMin, boxMax );
}

__forceinline void fwEntityDesc::GetApproxBoundingSphere(spdSphere& sphere) const
{
	const Vec3V   centre			= m_positionAndUnused.GetXYZ();
	const Vec4V   extentAndRadius	= Float16Vec4UnpackFromXY( m_extentAndRadiusAndVecToLodAndFlags2 );
	const ScalarV radius			= extentAndRadius.GetW();

	sphere.Set(centre, radius);
}

__forceinline Vec3V_Out fwEntityDesc::GetLodPosition() const
{
	const Vec4V vToLodAndUnused		= Float16Vec4UnpackFromZW( m_extentAndRadiusAndVecToLodAndFlags2 );
	return (m_positionAndUnused + vToLodAndUnused).GetXYZ();
}

} // namespace rage

#endif //_INC_ENTITYDESC_H_
