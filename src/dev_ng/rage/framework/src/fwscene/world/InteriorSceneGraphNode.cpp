#include "fwscene/world/InteriorSceneGraphNode.h"

namespace rage {

FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwInteriorSceneGraphNode, fwSceneGraph::INTERIOR_SCENE_NODE_POOL_SIZE, 0.65f, atHashString("fwInteriorSceneGraphNode",0x7b0420f2));
FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwRoomSceneGraphNode, fwSceneGraph::ROOM_SCENE_NODE_POOL_SIZE, 0.43f, atHashString("fwRoomSceneGraphNode",0x9f0123e3));
FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwPortalSceneGraphNode, fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE,	0.44f, atHashString("fwPortalSceneGraphNode",0x92f2be3f));

fwInteriorSceneGraphNode::fwInteriorSceneGraphNode() :
	fwSceneGraphNode( SCENE_GRAPH_NODE_TYPE_INTERIOR )
{
	m_interiorIndex = -1;
}

fwRoomSceneGraphNode::fwRoomSceneGraphNode(fwInteriorSceneGraphNode* ownerInteriorSceneNode) :
	fwSceneGraphNode( SCENE_GRAPH_NODE_TYPE_ROOM )
{
	m_ownerInteriorSceneNode = ownerInteriorSceneNode;
	m_container = rage_new fwEntityContainer( this );
	m_interiorLocation.MakeInvalid();
}

fwRoomSceneGraphNode::~fwRoomSceneGraphNode()
{
	delete m_container;
	m_container = NULL;
}

fwPortalSceneGraphNode::fwPortalSceneGraphNode(fwInteriorSceneGraphNode* ownerInteriorSceneNode) :
	fwSceneGraphNode( SCENE_GRAPH_NODE_TYPE_PORTAL )
{
	m_ownerInteriorSceneNode = ownerInteriorSceneNode;
	m_container = rage_new fwEntityContainer( this );
	m_interiorLocation.MakeInvalid();
	m_negativePortalEnd = NULL;
	m_positivePortalEnd = NULL;
	Enable();
}

fwPortalSceneGraphNode::~fwPortalSceneGraphNode()
{
	delete m_container;
	m_container = NULL;
}

}
