#ifndef _INC_SCENEGRAPH_H_
#define _INC_SCENEGRAPH_H_

#include "fwtl/Pool.h"
#include "system/eaptr.h"
#include "diag/channel.h"
#include "audioengine/enginedefs.h"

#define SceneGraphAssert	Assert
#define SceneGraphAssertf	Assertf
RAGE_DECLARE_CHANNEL(sceneGraph)

#define SceneGraphFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(sceneGraph,cond,fmt,##__VA_ARGS__)

#define SceneGraphLockedFatalAssert() SceneGraphFatalAssertf(!fwSceneGraph::IsSceneGraphLocked(), "Attempted to modify the scenegraph while its locked")

namespace rage {

class fwSceneGraphNode;

class fwEntityContainer;
class fwFixedEntityContainer;
class fwSoAEntityContainer;
class fwExteriorSceneGraphNode;
class fwStreamedSceneGraphNode;
class fwInteriorSceneGraphNode;
class fwRoomSceneGraphNode;
class fwPortalSceneGraphNode;

class fwPointerFixer
{
private:

	u8*				m_spuMemory;
	sysEaPtr<u8>	m_ppuMemory;
	u32				m_memorySize;

public:

	fwPointerFixer()
	{ }

	fwPointerFixer(u8* spuMemory, sysEaPtr<u8> ppuMemory, const u32 memorySize) {
		Set( spuMemory, ppuMemory, memorySize );
	}

	void Set(u8* spuMemory, sysEaPtr<u8> ppuMemory, const u32 memorySize)
	{
		m_spuMemory = spuMemory;
		m_ppuMemory = ppuMemory;
		m_memorySize = memorySize;
	}

	template <typename T>
	void Fix(T*& ptr) const
	{
		const size_t	intPpuMemory = m_ppuMemory.ToUint();
		const size_t	intSpuMemory = reinterpret_cast< size_t >( m_spuMemory );

		const size_t	intPpuPtr = reinterpret_cast< size_t >( ptr );
		const size_t	offset = intPpuPtr - intPpuMemory;
		const size_t	intSpuPtr = intSpuMemory + offset;
		T*				spuPtr = reinterpret_cast< T* >( intSpuPtr );

		if ( Likely( intPpuPtr != 0 && offset < m_memorySize ) )
			ptr = spuPtr;
	}

	template <typename T>
	void Unfix(T*& ptr) const
	{
		const size_t	intPpuMemory = m_ppuMemory.ToUint();
		const size_t	intSpuMemory = reinterpret_cast< size_t >( m_spuMemory );

		const size_t	intSpuPtr = reinterpret_cast< size_t >( ptr );
		const size_t	offset = intSpuPtr - intSpuMemory;
		const size_t	intPpuPtr = intPpuMemory + offset;
		T*				ppuPtr = reinterpret_cast< T* >( intPpuPtr );

		if ( Likely( intSpuPtr != 0 && offset < m_memorySize ) )
			ptr = ppuPtr;
	}
};

class fwSceneGraph
{
private:

	static int							ms_sceneGraphLockCount;
	static u8*							ms_poolAllocatedMemory;
	static u8*							ms_poolStorage;
	static u32							ms_poolStorageSize;
	static fwPointerFixer				ms_fixer;

	static fwEntityContainer*			ms_entityContainerStorage;
	static fwFixedEntityContainer*		ms_fixedEntityContainerStorage;
	static fwSoAEntityContainer*		ms_soaEntityContainerStorage;
	static fwExteriorSceneGraphNode*	ms_exteriorSceneNodeStorage;
	static fwStreamedSceneGraphNode*	ms_streamedSceneNodeStorage;
	static fwInteriorSceneGraphNode*	ms_interiorSceneNodeStorage;
	static fwRoomSceneGraphNode*		ms_roomSceneNodeStorage;
	static fwPortalSceneGraphNode*		ms_portalSceneNodeStorage;

	static void AssignStorageMemory(u8* memory, const u32 memorySize);

public:

	enum
	{
		ENTITY_CONTAINER_POOL_SIZE			= 800,
		FIXED_ENTITY_CONTAINER_POOL_SIZE	= 2,
		SOA_ENTITY_CONTAINER_POOL_SIZE		= 1000,

		EXTERIOR_SCENE_NODE_POOL_SIZE		= 1,
		STREAMED_SCENE_NODE_POOL_SIZE		= 1000,
		INTERIOR_SCENE_NODE_POOL_SIZE		= 150,
		ROOM_SCENE_NODE_POOL_SIZE			= 256,
		PORTAL_SCENE_NODE_POOL_SIZE			= 400,

		FIRST_EXTERIOR_SCENE_NODE_INDEX		= 0,
		FIRST_STREAMED_SCENE_NODE_INDEX		= FIRST_EXTERIOR_SCENE_NODE_INDEX + EXTERIOR_SCENE_NODE_POOL_SIZE,
		FIRST_INTERIOR_SCENE_NODE_INDEX		= FIRST_STREAMED_SCENE_NODE_INDEX + STREAMED_SCENE_NODE_POOL_SIZE,
		FIRST_ROOM_SCENE_NODE_INDEX			= FIRST_INTERIOR_SCENE_NODE_INDEX + INTERIOR_SCENE_NODE_POOL_SIZE,
		FIRST_PORTAL_SCENE_NODE_INDEX		= FIRST_ROOM_SCENE_NODE_INDEX + ROOM_SCENE_NODE_POOL_SIZE,
		MAX_SCENE_NODE_COUNT				= FIRST_PORTAL_SCENE_NODE_INDEX + PORTAL_SCENE_NODE_POOL_SIZE,
	};

	static void LockSceneGraph()					{ ++ms_sceneGraphLockCount; SceneGraphAssert( ms_sceneGraphLockCount >= 0 ); }
	static void UnlockSceneGraph()					{ --ms_sceneGraphLockCount; SceneGraphAssert( ms_sceneGraphLockCount >= 0 ); }
	static bool IsSceneGraphLocked()				{ return ms_sceneGraphLockCount > 0; }
	static int GetSceneGraphLockCount()				{ return ms_sceneGraphLockCount; }

#if __SPU
	static void InitSpuPools(u8* spuMemory, sysEaPtr<u8> ppuMemory, const u32 memorySize);
	static const fwPointerFixer& GetPointerFixer()	{ return ms_fixer; }
#else
	static void InitPools();
	static void ShutdownPools();
#endif

	static u8* GetPoolStorage()						{ return ms_poolStorage; }
	static u32 GetPoolStorageSize()					{ return ms_poolStorageSize; }

	static s16 ComputeSceneNodeIndex(const fwSceneGraphNode* sceneNode);
};

} // namespace rage

#endif // !defined _INC_SCENEGRAPH_H_
