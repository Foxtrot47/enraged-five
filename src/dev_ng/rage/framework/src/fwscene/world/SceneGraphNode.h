#ifndef _INC_WORLDCONTAINER_H_
#define _INC_WORLDCONTAINER_H_

#include "fwmaths/Rect.h"
#include "fwutil/Flags.h"
#include "fwscene/world/SceneGraph.h"

namespace rage {

enum eSceneGraphNodeType
{
	SCENE_GRAPH_NODE_TYPE_EXTERIOR,
	SCENE_GRAPH_NODE_TYPE_STREAMED,
	SCENE_GRAPH_NODE_TYPE_INTERIOR,
	SCENE_GRAPH_NODE_TYPE_ROOM,
	SCENE_GRAPH_NODE_TYPE_PORTAL,
	SCENE_GRAPH_NODE_TYPE_COUNT
};

class fwSceneGraphNode
{
private:

	enum {
		SCENE_NODE_FLAG_CONTAINS_SPAWN_POINTS	= 1 << 0,
		SCENE_NODE_FLAG_IS_IN_WORLD				= 1 << 1,
		SCENE_NODE_FLAG_ENABLED					= 1 << 2,
		// .. continues with fwPortalSceneGraphNode flags
	};

	fwSceneGraphNode*		m_next;
	fwSceneGraphNode*		m_firstChild;
	u8						m_type;
	fwFlags8				m_flags;
	s16						m_index;

protected:

	fwSceneGraphNode(const eSceneGraphNodeType type)
	{
		SceneGraphLockedFatalAssert();

		m_next = NULL;
		m_firstChild = NULL;
		m_type = static_cast< u8 >( type );
		m_index = fwSceneGraph::ComputeSceneNodeIndex( this );
		m_flags.ClearAllFlags();
	}

	fwFlags8& GetFlagsRef()									{ return m_flags; }
	const fwFlags8& GetFlagsRef() const						{ return m_flags; }

public:

	void FixupPointers(const fwPointerFixer& fixer)
	{
		fixer.Fix( m_firstChild );
		fixer.Fix( m_next );
	}

	eSceneGraphNodeType GetType() const						{ return static_cast< eSceneGraphNodeType >( m_type ); }
	bool IsTypeExterior() const								{ return GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR; }
	bool IsTypeStreamed() const								{ return GetType() == SCENE_GRAPH_NODE_TYPE_STREAMED; }
	bool IsTypeInterior() const								{ return GetType() == SCENE_GRAPH_NODE_TYPE_INTERIOR; }
	bool IsTypeRoom() const									{ return GetType() == SCENE_GRAPH_NODE_TYPE_ROOM; }
	bool IsTypePortal() const								{ return GetType() == SCENE_GRAPH_NODE_TYPE_PORTAL; }

	s16 GetIndex() const									{ return m_index; }

	void SetNext(fwSceneGraphNode* next)					{ SceneGraphLockedFatalAssert(); m_next = next; }
	fwSceneGraphNode* GetNext() const						{ return m_next; }

	void SetFirstChild(fwSceneGraphNode* firstChild)		{ SceneGraphLockedFatalAssert(); m_firstChild = firstChild; }
	fwSceneGraphNode* GetFirstChild() const					{ return m_firstChild; }

	u32 GetChildrenCount() const;
	fwSceneGraphNode* GetChild(const u32 position);
	bool IsChild(const fwSceneGraphNode* child) const;
	void AddChild(fwSceneGraphNode* child);
	void RemoveChild(fwSceneGraphNode* child);

	void SetContainsSpawnPoints(const bool value)			{ m_flags.ChangeFlag( SCENE_NODE_FLAG_CONTAINS_SPAWN_POINTS, value ); }
	bool GetContainsSpawnPoints() const						{ return m_flags.IsFlagSet( SCENE_NODE_FLAG_CONTAINS_SPAWN_POINTS ); }

	void SetIsInWorld(const bool value)						{ m_flags.ChangeFlag( SCENE_NODE_FLAG_IS_IN_WORLD, value ); }
	bool GetIsInWorld() const								{ return m_flags.IsFlagSet( SCENE_NODE_FLAG_IS_IN_WORLD ); }

	void Enable(const bool enabled = true)					{ m_flags.ChangeFlag( SCENE_NODE_FLAG_ENABLED, enabled ); }
	bool IsEnabled() const									{ return m_flags.IsFlagSet( SCENE_NODE_FLAG_ENABLED ); }

#if __BANK && !__SPU
	typedef void (*GetSceneGraphNodeDebugNameFuncType)(char name[256], const fwSceneGraphNode* node, const fwSceneGraphNode* from, bool bShowInteriorName, bool bIsMirror);
	static GetSceneGraphNodeDebugNameFuncType sm_GetSceneGraphNodeDebugNameFunc;
#endif // __BANK && !__SPU
};

CompileTimeAssertSize( fwSceneGraphNode, 12, 24 );

} // namespace rage

#endif // !defined _INC_WORLDCONTAINER_H_
