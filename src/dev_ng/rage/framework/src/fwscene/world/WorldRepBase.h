
/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    scene/world/worldRepBase.h
// PURPOSE : specify required interface for all world representations
// AUTHOR :  John W.
// CREATED : 13/01/09
//
/////////////////////////////////////////////////////////////////////////////////


#ifndef __INC_WORLDREPBASE_H_
#define __INC_WORLDREPBASE_H_

// forward declarations
class CRenderPhase;

namespace rage {

class fwEntity;
class fwRect;
class fwSearchVolume;
class fwIsSphereInside;
class fwIsSphereIntersecting;
class fwIsSphereIntersectingBBApprox;
class fwIsBoxInside;
class fwIsBoxIntersectingApprox;
class fwIsBoxIntersectingBB;
class fwPtrListSingleLink;
class fwSceneGraphNode;
class bkBank;

typedef bool (*fwIntersectingCB)(fwEntity* pEntity, void* data);

enum {
	OP_ADD_REMOVE		= 1,
	OP_PRESCAN			= 2,
	OP_SCAN				= 3,
	OP_POSTSCAN			= 4,
	OP_SEARCH			= 5,
	OP_UPDATE			= 6,
	OP_MISC				= 7,
	OP_DEBUG			= 8,
};


// mask to determine which operations are to be executed by a world representation
enum{
	USAGE_MASK_ADD_REMOVE	= 1<<OP_ADD_REMOVE,
	USAGE_MASK_PRESCAN		= 1<<OP_PRESCAN,
	USAGE_MASK_SCAN			= 1<<OP_SCAN,
	USAGE_MASK_POSTSCAN		= 1<<OP_POSTSCAN,
	USAGE_MASK_SEARCH		= 1<<OP_SEARCH,
	USAGE_MASK_UPDATE		= 1<<OP_UPDATE,
	USAGE_MASK_MISC			= 1<<OP_MISC,
	USAGE_MASK_DEBUG		= 1<<OP_DEBUG,
};

enum
{
	WORLDREP_SEARCHMODULE_AUDIO				= 1 << 0,
	WORLDREP_SEARCHMODULE_DEFAULT			= 1 << 1,
	WORLDREP_SEARCHMODULE_PEDS				= 1 << 2,
	WORLDREP_SEARCHMODULE_PORTALS			= 1 << 4,
	WORLDREP_SEARCHMODULE_SCRIPT			= 1 << 5,
	WORLDREP_SEARCHMODULE_STREAMING			= 1 << 6,
	WORLDREP_SEARCHMODULE_VEHICLEAI			= 1 << 7,
	WORLDREP_SEARCHMODULE_DEBUG				= 1 << 8,
	WORLDREP_SEARCHMODULE_REPLAY			= 1 << 9,

	WORLDREP_SEARCHMODULE_NONE				= 0,
	WORLDREP_SEARCHMODULE_ALL				= ~0
};

class fwWorldRepBase{
public:
	fwWorldRepBase(void) { m_HandlingTypes= 0; m_Operations = 0;}
	virtual ~fwWorldRepBase(void) {}

	virtual void	AddToWorld(fwEntity * pEntity, const fwRect& rect) = 0;
	virtual void	RemoveFromWorld(fwEntity * pEntity) = 0;
	virtual void	UpdateInWorld(fwEntity* pEntity, const fwRect& rect) = 0;

	virtual void	AddSceneNodeToWorld(fwSceneGraphNode* pSceneNode) = 0;
	virtual void	RemoveSceneNodeFromWorld(fwSceneGraphNode* pSceneNode) = 0;

	// -------------------------------
	// --- search interface stuff ----
	// -------------------------------
public:
	virtual void ForAllEntitiesIntersecting(fwSearchVolume* , fwIntersectingCB ,void* , s32 , s32, s32, s32, u32 ) {}

	// ----------------------------
	// --- scan interface stuff ---
	// ----------------------------
public:
	virtual void PreScan(void) = 0;
	virtual void PostScan(void) = 0;

	// ----------------------------
	// --- Misc interface stuff ---
	// ----------------------------
public:
	virtual void GetAll(fwPtrListSingleLink& Matches) = 0;
	virtual void GetAll(fwEntity** entityArray, u32* entityCount) = 0;

	// debug stuff
#if __BANK
	virtual void AddBankWidgets(bkBank* pBank) = 0;
	virtual void UpdateDebug() = 0;
	virtual void DrawDebug() = 0;
#endif // __BANK

	void	SetHandlingTypes(u32 types)		{ m_HandlingTypes = types; }
	void	SetOperations(u32 ops)			{ m_Operations = ops; }
	u32		GetOperations(void)				{ return(m_Operations); }

	bool	IsTypeHandled(u32 type)			{ return((m_HandlingTypes & (1<<type)) > 0); }
	bool	AreTypesHandled(u32 types)		{ return((m_HandlingTypes & types) > 0); }
	bool	IsOperationHandled(u32 op)		{ return((m_Operations & (1<<op)) > 0); }
private:
	u32	m_HandlingTypes;
	u32	m_Operations;
};

} // namespace rage

#endif //__INC_WORLDREPBASE_H_

