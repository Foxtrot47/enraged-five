
#include "atl/pool.h"
#include "fwscene/world/EntityContainer.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwtl/StepAllocator.h"
#include "entity/entity.h"

namespace rage {

FW_INSTANTIATE_CLASS_POOL_SPILLOVER( fwEntityContainer, fwSceneGraph::ENTITY_CONTAINER_POOL_SIZE, 0.62f, atHashString("fwEntityContainer",0x160993aa) );
FW_INSTANTIATE_CLASS_POOL( fwFixedEntityContainer, fwSceneGraph::FIXED_ENTITY_CONTAINER_POOL_SIZE, atHashString("fwFixedEntityContainer",0x558e5af3) );
FW_INSTANTIATE_CLASS_POOL_SPILLOVER( fwSoAEntityContainer, fwSceneGraph::SOA_ENTITY_CONTAINER_POOL_SIZE, 0.27f, atHashString("fwSoAEntityContainer",0x72543b31) );

fwDescPool*										fwBaseEntityContainer::ms_entityDescPool;
fwBaseEntityContainer::UpdateEntityDescFunc		fwBaseEntityContainer::ms_updateEntityDescFunc;

//==================== fwBaseEntityContainer ====================//

void fwBaseEntityContainer::CreateDataHandle(const u32 amount)
{
	SceneGraphLockedFatalAssert();

	m_dataHandle = ms_entityDescPool->New( amount );
	if ( !m_dataHandle )
	{
		Quitf(ERR_GEN_ENT_CONT_1,"Could not allocate data handle for the entity container." );
	}

	UpdateDataHandleCache();
}

void fwBaseEntityContainer::DeleteDataHandle()
{
	SceneGraphLockedFatalAssert();

	ms_entityDescPool->Delete( m_dataHandle );
	m_dataHandle = NULL;
	
	UpdateDataHandleCache();
}

DefragPool_eResizeResult fwBaseEntityContainer::ResizeDataHandle(const u32 amount)
{
	SceneGraphLockedFatalAssert();

	FatalAssert(amount);
	DefragPool_eResizeResult	result = ms_entityDescPool->Resize( m_dataHandle, amount );
	if ( result == DEFRAGPOOL_RESIZERESULT_FAIL )
	{
		Quitf(ERR_GEN_ENT_CONT_2,"fwEntityDesc array allocation failed - pool is either full or fragmented." );
	}
	FatalAssertf(result != DEFRAGPOOL_RESIZERESULT_DELETED, "calling code does not currently support this");

	UpdateDataHandleCache();
	return result;
}

DefragPool_eResizeResult fwBaseEntityContainer::GrowDataHandle(const u32 amount)
{
	SceneGraphLockedFatalAssert();

	DefragPool_eResizeResult	result = ms_entityDescPool->Grow( m_dataHandle, amount );
	if ( result == DEFRAGPOOL_RESIZERESULT_FAIL )
	{
		Quitf(ERR_GEN_ENT_CONT_3,"fwEntityDesc array allocation failed - pool is either full or fragmented." );
	}

	UpdateDataHandleCache();
	return result;
}

#if !__FINAL
volatile float gDummyFloatToForceRead;
#endif

void fwBaseEntityContainer::UpdateDataHandleCache()
{
	SceneGraphLockedFatalAssert();
	const fwDescPoolAlloc* const dataHandle = m_dataHandle;
	if (dataHandle)
	{
		m_cachedEntityDescArray = dataHandle->GetEntries();
		Assign(m_cachedStorageSize, dataHandle->GetNumEntries() );
#		if !__FINAL
			// Force a dereference of m_cachedEntityDescArray to attempt to catch GTAV B*1560796,
			// "Crash on boot - [dependency_spu] HALT: $3fc80$ SPU DMA Assertion: bad ppu: 0xcdcdcdcd during (null)"
			// much closer to the cause of the problem.
			// Note that on PS3 0xcdcdcdcd is valid memory (RSX local),
			// so lets use a float load which will give an alignment exception.
			gDummyFloatToForceRead = *(const volatile float*)m_cachedEntityDescArray;
#		endif
	}
	else
	{
		m_cachedEntityDescArray = NULL;
		m_cachedStorageSize = 0;
	}
}

void fwBaseEntityContainer::UpdateDataHandleCacheForAll()
{
	{
		fwEntityContainer::Pool*		pool = fwEntityContainer::GetPool();

		for (s32 i = 0; i < (s32)pool->GetSize(); i++)
			if ( pool->GetSlot(i) )
				pool->GetSlot(i)->UpdateDataHandleCache();
	}

	{
		fwSoAEntityContainer::Pool*		pool = fwSoAEntityContainer::GetPool();

		for (s32 i = 0; i < (s32)pool->GetSize(); i++)
			if ( pool->GetSlot(i) )
				pool->GetSlot(i)->UpdateDataHandleCache();
	}
}

fwBaseEntityContainer::fwBaseEntityContainer(const ContainerType type, fwSceneGraphNode* ownerSceneNode)
{
	SceneGraphLockedFatalAssert();

	m_dataHandle = NULL;
	m_cachedEntityDescArray = NULL;
	m_next = NULL;
	m_ownerSceneNode = ownerSceneNode;
	m_entityCount = 0;
	m_containerType = static_cast< u16 >( type );
	m_cachedStorageSize = 0;
}

void fwBaseEntityContainer::Delete(fwBaseEntityContainer* container)
{
	switch( container->m_containerType )
	{
		case CONTAINER_TYPE_DEFAULT: delete container->AsTypeDefault(); return;
		case CONTAINER_TYPE_FIXED: delete container->AsTypeFixed(); return;
		case CONTAINER_TYPE_SOA: delete container->AsTypeSoA(); return;
		default: SceneGraphAssertf( false, "Unexpected container type" ); return;
	}
}

void fwBaseEntityContainer::RemoveAllEntities()
{
	switch( m_containerType )
	{
		case CONTAINER_TYPE_DEFAULT: AsTypeDefault()->RemoveAllEntities(); return;
		case CONTAINER_TYPE_FIXED: AsTypeFixed()->RemoveAllEntities(); return;
		case CONTAINER_TYPE_SOA: AsTypeSoA()->RemoveAllEntities(); return;
		default: SceneGraphAssertf( false, "Unexpected container type" ); return;
	}
}

#if __BANK
fwEntityDesc* fwBaseEntityContainer::GetEntityDescForEntity(const fwEntity* entity)
{
	switch( m_containerType )
	{
		case CONTAINER_TYPE_DEFAULT: return AsTypeDefault()->GetEntityDescForEntity( entity );
		case CONTAINER_TYPE_FIXED: return AsTypeFixed()->GetEntityDescForEntity( entity );
		case CONTAINER_TYPE_SOA: return AsTypeSoA()->GetEntityDescForEntity( entity );
		default: SceneGraphAssertf( false, "Unexpected container type" ); return NULL;
	}
}
#endif



void fwBaseEntityContainer::UpdateEntityDescriptorInNode(fwEntity* entity)
{
	switch( m_containerType )
	{
		case CONTAINER_TYPE_DEFAULT:
		{
			static_cast<fwEntityContainer*>(this)->UpdateEntityDescriptorInNodeInternal(entity);
			break;
		}
		case CONTAINER_TYPE_FIXED: 
		{
			SceneGraphAssertf( false, "Fixed container type cannot update descriptor" );
			break;

		}
		case CONTAINER_TYPE_SOA: 
		{
			static_cast<fwSoAEntityContainer*>(this)->UpdateEntityDescriptorInNodeInternal(entity);
			break;
		}
		default: 
		{
			SceneGraphAssertf( false, "Unexpected container type" );
		}
	}

}


u32 fwBaseEntityContainer::GetEntityDescIndex(const fwEntity* pEntity) 
{ 
	if (!pEntity)
	{
		return INVALID_ENTITY_INDEX; 
	}

	switch( m_containerType )
	{
	case CONTAINER_TYPE_DEFAULT:
		{
			fwEntityContainer *container = static_cast<fwEntityContainer*>(this);
			for (u32 i = 0; i < m_entityCount; ++i)
			{
				if (container->GetEntity(i) == pEntity)
				{
					return i;
				}
			}
			break;
		}
	case CONTAINER_TYPE_FIXED: 
		{
			for (int i = 0; i < m_cachedStorageSize; ++i)
			{
				if (m_cachedEntityDescArray[i].GetEntity() == pEntity)
				{
					return i;
				}
			}
			break;
		}
	case CONTAINER_TYPE_SOA: 
		{
			// I can make this faster, I don't need all the offsets setup 
			fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount );

			for (u32 i = 0; i < m_entityCount; ++i)
			{
				if (soa.m_entityPointers[i] == pEntity)
				{
					return i;
				}
			}
			break;
		}
	default: 
		{
			SceneGraphAssertf( false, "Unexpected container type" );
		}
	}

	return INVALID_ENTITY_INDEX; 
}

void fwBaseEntityContainer::GetApproxBoundingBox(u32 index, spdAABB &aSpdAABB)
{
	if (index == INVALID_ENTITY_INDEX)
	{
		return;
	}

	switch( m_containerType )
	{
	case CONTAINER_TYPE_DEFAULT:
		{
			fwEntityContainer *container = static_cast<fwEntityContainer*>(this);
			container->GetEntityDesc(index)->GetApproxBoundingBox(aSpdAABB);
			break;
		}
	case CONTAINER_TYPE_FIXED: 
		{			
			Assertf(index < m_cachedStorageSize, "invalid index %d", m_cachedStorageSize);
			m_cachedEntityDescArray[index].GetApproxBoundingBox(aSpdAABB);
			break;
		}
	case CONTAINER_TYPE_SOA: 
		{
			// I can make this faster, I don't need all the offsets setup 
			fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount );
			fwSoAEntityContainer::GetApproxBoundingBox(soa, index, aSpdAABB);
			break;
		}
	default: 
		{
			SceneGraphAssertf( false, "Unexpected container type" );
		}
	}	
}

void fwBaseEntityContainer::GetApproxBoundingSphere(u32 index, spdSphere &aSpdSphere)
{
	if (index == INVALID_ENTITY_INDEX)
	{
		return;
	}

	switch( m_containerType )
	{
	case CONTAINER_TYPE_DEFAULT:
		{
			fwEntityContainer *container = static_cast<fwEntityContainer*>(this);
			container->GetEntityDesc(index)->GetApproxBoundingSphere(aSpdSphere);
			break;
		}
	case CONTAINER_TYPE_FIXED: 
		{			
			Assertf(index < m_cachedStorageSize, "invalid index %d", m_cachedStorageSize);
			m_cachedEntityDescArray[index].GetApproxBoundingSphere(aSpdSphere);
			break;
		}
	case CONTAINER_TYPE_SOA: 
		{
			// I can make this faster, I don't need all the offsets setup 
			fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount );
			fwSoAEntityContainer::GetApproxBoundingSphere(soa, index, aSpdSphere);
			break;
		}
	default: 
		{
			SceneGraphAssertf( false, "Unexpected container type" );
		}
	}
}

void fwBaseEntityContainer::SetLodDistCached(u32 index, u32 dist)
{
	if (index == INVALID_ENTITY_INDEX)
	{
		return;
	}

	switch( m_containerType )
	{
	case CONTAINER_TYPE_DEFAULT:
		{
			fwEntityContainer *container = static_cast<fwEntityContainer*>(this);
			container->GetEntityDesc(index)->SetLodDistCached(dist);
			break;
		}
	case CONTAINER_TYPE_FIXED: 
		{			
			Assertf(index < m_cachedStorageSize, "invalid index %d", m_cachedStorageSize);
			m_cachedEntityDescArray[index].SetLodDistCached(dist);
			break;
		}
	case CONTAINER_TYPE_SOA: 
		{
			// I can make this faster, I don't need all the offsets setup 
			fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount );
			fwSoAEntityContainer::SetLodDistCached(soa, index, dist);
			break;
		}
	default: 
		{
			SceneGraphAssertf( false, "Unexpected container type" );
		}
	}
}

void fwBaseEntityContainer::RemoveEntity(fwEntity* pEntity)
{
	if (!pEntity)
	{
		return;
	}

	switch( m_containerType )
	{
	case CONTAINER_TYPE_DEFAULT:
		{
			fwEntityContainer *container = static_cast<fwEntityContainer*>(this);
			container->RemoveEntityInternal(pEntity);
			break;
		}
	case CONTAINER_TYPE_FIXED: 
		{			
			SceneGraphAssertf( false, "Fixed container type does not currently support remove" );
			break;
		}
	case CONTAINER_TYPE_SOA: 
		{
			fwSoAEntityContainer *container = static_cast<fwSoAEntityContainer*>(this);
			container->RemoveEntityInternal(pEntity);
			break;
		}
	default: 
		{
			SceneGraphAssertf( false, "Unexpected container type" );
		}
	}
}

//==================== fwEntityContainer ====================//

fwEntityContainer::fwEntityContainer(fwSceneGraphNode* ownerSceneNode)
	: fwBaseEntityContainer( CONTAINER_TYPE_DEFAULT, ownerSceneNode )
{
	m_storageStep = DEFAULT_ENTITY_CONTAINER_STORAGE_STEP;
}

void fwEntityContainer::Preallocate(const u16 count)
{
	if ( !m_dataHandle )
		CreateDataHandle( count );
	else if ( m_cachedStorageSize - m_entityCount < count )
		ResizeDataHandle( m_entityCount + count );
}

fwEntityDesc* fwEntityContainer::AppendEntity(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() == NULL );

	if ( !m_dataHandle )
	{
		CreateDataHandle( m_storageStep );
		m_entityCount = 0;
	}
	else if ( m_entityCount == m_cachedStorageSize )
		GrowDataHandle( m_storageStep );

	m_entityCount++;
	fwEntityDesc*	entityDesc = GetEntityDesc( m_entityCount - 1 );
	entityDesc->SetEntity( entity );

	if ( entity )
	{
		entity->SetOwnerEntityContainer( this );
		fwEntityContainer::ms_updateEntityDescFunc( entity, entityDesc );
	}

	return entityDesc;
}

void fwEntityContainer::RemoveEntityInternal(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() == this );

	SceneGraphAssertf( m_dataHandle, "Trying to remove an entity from a node that is already empty!" );
	if ( !m_dataHandle )
		return;

	fwEntityDesc*	entityDescs = m_cachedEntityDescArray;

	for (u32 i = 0; i < m_entityCount; ++i)
	{
		if ( entityDescs[i].GetEntity() == entity )
		{
			entity->SetOwnerEntityContainer( NULL );

			entityDescs[i] = entityDescs[ m_entityCount - 1 ];
			m_entityCount--;

			if ( m_entityCount == 0 )
				DeleteDataHandle();
			else if ( m_cachedStorageSize - m_entityCount >= m_storageStep )
				ResizeDataHandle( m_entityCount );

			return;
		}
	}

	SceneGraphAssertf( false, "Tried to remove an entity from a node that didn't contain the entity itself" );
}

void fwEntityContainer::PopEntity()
{
	SceneGraphLockedFatalAssert();

	SceneGraphAssertf( m_dataHandle, "Trying to remove an entity from a node that is already empty!" );
	if ( !m_dataHandle )
		return;

	fwEntityDesc*	entityDescs = m_cachedEntityDescArray;
	entityDescs[ m_entityCount - 1 ].GetEntity()->SetOwnerEntityContainer( NULL );
	m_entityCount--;

	if ( m_entityCount == 0 )
		DeleteDataHandle();
	else if ( m_cachedStorageSize - m_entityCount >= m_storageStep )
		ResizeDataHandle( m_entityCount );
}

void fwEntityContainer::RemoveAllEntities()
{
	SceneGraphLockedFatalAssert();

	if ( !m_dataHandle )
		return;

	for (int i = 0; i < static_cast< int >( GetEntityCount() ); ++i)
	{
		SceneGraphAssert( GetEntity(i)->GetOwnerEntityContainer() == this );
		GetEntity(i)->SetOwnerEntityContainer( NULL );
	}

	DeleteDataHandle();
	m_entityCount = 0;
}

void fwEntityContainer::GetEntityArray(atArray< fwEntity* >& entityArray)
{
	entityArray.Reset();

	entityArray.Resize( GetEntityCount() );
	for (u32 i = 0; i < GetEntityCount(); ++i)
		entityArray[i] = GetEntity(i);
}

#if __BANK
fwEntityDesc* fwEntityContainer::GetEntityDescForEntity(const fwEntity* entity)
{
	for (u32 i = 0; i < GetEntityCount(); ++i)
		if ( GetEntity(i) == entity )
			return GetEntityDesc(i);

	return NULL;
}
#endif


void fwEntityContainer::UpdateEntityDescriptorInNodeInternal(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();

	for (u32 i = 0; i < GetEntityCount(); i++)
	{
		if ( GetEntity(i) == entity )
		{
			fwBaseEntityContainer::ms_updateEntityDescFunc( entity, GetEntityDesc(i) );
			return;
		}
	}

	SceneGraphAssertf( false, "The entity to be updated was not found in the node!" );
}

//==================== fwFixedEntityContainer ====================//

fwFixedEntityContainer::fwFixedEntityContainer(fwSceneGraphNode* ownerSceneNode, const int size)
	: fwBaseEntityContainer( CONTAINER_TYPE_FIXED, ownerSceneNode )
{
	CreateDataHandle( size );
	Reset();
}

fwFixedEntityContainer::~fwFixedEntityContainer() {
	DeleteDataHandle();
}

void fwFixedEntityContainer::Reset()
{
	Entry*		entries = reinterpret_cast< Entry* >( m_cachedEntityDescArray );
	const int	entryCount = m_cachedStorageSize;

	m_firstFreeIndex = 0;
	for (int i = 0; i < entryCount - 1; ++i)
	{
		entries[i].Invalidate();
		entries[i].SetNextFreeIndex( static_cast< u16 >(i + 1) );
	}

	entries[ entryCount - 1 ].Invalidate();
	entries[ entryCount - 1 ].SetNextFreeIndex( INVALID_NEXT_INDEX );
}

fwEntityDesc* fwFixedEntityContainer::AppendEntity(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() == NULL );

	SceneGraphAssertf( m_firstFreeIndex != INVALID_NEXT_INDEX, "This fixed entity container has no free descriptors" );
	if ( m_firstFreeIndex == INVALID_NEXT_INDEX )
		return NULL;

	fwEntityDesc*	entityDesc = &m_cachedEntityDescArray[ m_firstFreeIndex ];
	Entry*			entry = reinterpret_cast< Entry* >( entityDesc );
	m_firstFreeIndex = entry->GetNextFreeIndex();

	++m_entityCount;

	if ( entity )
	{
		entity->SetOwnerEntityContainer( this );
		fwBaseEntityContainer::ms_updateEntityDescFunc( entity, entityDesc );
	}

	return entityDesc;
}

void fwFixedEntityContainer::RemoveEntityFromDescriptor(fwEntityDesc* entityDesc)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entityDesc->GetEntity()->GetOwnerEntityContainer() == this );

	entityDesc->GetEntity()->SetOwnerEntityContainer( NULL );

	const u16	index = static_cast< u16 >( entityDesc - m_cachedEntityDescArray );
	Entry*		entry = reinterpret_cast< Entry* >( entityDesc );
	entry->Invalidate();
	entry->SetNextFreeIndex( m_firstFreeIndex );
	m_firstFreeIndex = index;
	
	--m_entityCount;
}

void fwFixedEntityContainer::RemoveAllEntities()
{
	Reset();
	m_entityCount = 0;
}

#if __BANK
fwEntityDesc* fwFixedEntityContainer::GetEntityDescForEntity(const fwEntity* entity)
{
	if ( !m_dataHandle || !entity )
		return NULL;

	for (int i = 0; i < m_cachedStorageSize; ++i)
		if ( m_cachedEntityDescArray[i].GetEntity() == entity )
			return &m_cachedEntityDescArray[i];
	
	return NULL;
}
#endif

fwStraddlingPortalsContainer::fwStraddlingPortalsContainer(fwSceneGraphNode* ownerSceneNode, const u8 size)
: fwFixedEntityContainer( ownerSceneNode, size )
{
}

u8 fwStraddlingPortalsContainer::AppendEntity(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssertf( m_firstFreeIndex != INVALID_NEXT_INDEX, "This fixed entity container has no free descriptors" );
	if ( m_firstFreeIndex == INVALID_NEXT_INDEX )
	{
		return 0xFF;
	}

	u8 index = (u8)m_firstFreeIndex;
	fwEntityDesc*	entityDesc = &m_cachedEntityDescArray[ m_firstFreeIndex ];
	Entry*			entry = reinterpret_cast< Entry* >( entityDesc );
	m_firstFreeIndex = entry->GetNextFreeIndex();

	++m_entityCount;

	if ( entity )
	{
		fwBaseEntityContainer::ms_updateEntityDescFunc( entity, entityDesc );
	}

	return index;
}

void fwStraddlingPortalsContainer::RemoveEntityFromDescriptorIndex(u8 index)
{
	SceneGraphLockedFatalAssert();

	Assertf(index < 255, "index must be less than 255");
	fwEntityDesc*	entityDesc = &m_cachedEntityDescArray[ index ];
	Entry*		entry = reinterpret_cast< Entry* >( entityDesc );
	entry->Invalidate();
	entry->SetNextFreeIndex( m_firstFreeIndex );
	m_firstFreeIndex = index;

	--m_entityCount;
}

fwEntityDesc *fwStraddlingPortalsContainer::GetEntityDesc(u8 index)
{
	Assertf(index < 0xFF, "invalid index");
	return &m_cachedEntityDescArray[ index ];
}

//==================== fwEntityDescSoA ====================//

fwSoAEntityContainer::fwSoAEntityContainer(fwSceneGraphNode* ownerSceneNode, fwEntity** entities, int containerSize, int entityCount, int &numberEntitiesProcessed)
	: fwBaseEntityContainer( CONTAINER_TYPE_SOA, ownerSceneNode )
{
	Assertf( containerSize <= SCAN_MAX_ENTITIES_PER_JOB, "A single SoA entity container is limited to the number of "
			"entities a single scan/search job can handle" );

	int roundedContainerSize = (containerSize + 3) & ~3;

	int memorySize = (roundedContainerSize * SIZEOF_ENTITY_DESC_SOA + sizeof(fwEntityDesc) - 1) / sizeof(fwEntityDesc);

	CreateDataHandle( memorySize );

	fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, roundedContainerSize );

	const int blockLength = 4;
	fwEntity *entityBlock[blockLength];

	int i = 0;
	int currentBlock = 0;
	const int maxBlockCount = roundedContainerSize / blockLength;

	while (i < entityCount && currentBlock < maxBlockCount)
	{
		int count = 0;
		while (i < entityCount && count < blockLength)
		{
			if (entities[i])
			{
				entityBlock[count] = entities[i];
				count++;
			}
			++i;
		}
		
		// Found none so break out of loop
		if (count == 0)
		{
			break;
		}

		fwEntityDesc	entityDescA, entityDescB, entityDescC, entityDescD;

		entityDescB.m_pEntity = NULL;
		entityDescC.m_pEntity = NULL;
		entityDescD.m_pEntity = NULL;

		entityBlock[0]->SetOwnerEntityContainer(this);
		fwBaseEntityContainer::ms_updateEntityDescFunc( entityBlock[0], &entityDescA );
		if (count > 1)
		{
			entityBlock[1]->SetOwnerEntityContainer(this);
			fwBaseEntityContainer::ms_updateEntityDescFunc( entityBlock[1], &entityDescB );
		}
		if (count > 2)	
		{
			entityBlock[2]->SetOwnerEntityContainer(this);
			fwBaseEntityContainer::ms_updateEntityDescFunc( entityBlock[2], &entityDescC );
		}

		if (count > 3)	
		{
			entityBlock[3]->SetOwnerEntityContainer(this);
			fwBaseEntityContainer::ms_updateEntityDescFunc( entityBlock[3], &entityDescD );
		}

		SetEntityDescBlock( soa, currentBlock, entityDescA, entityDescB, entityDescC, entityDescD );
		++currentBlock;
	}

	numberEntitiesProcessed = i;
	
	m_entityCount = roundedContainerSize;
}

fwSoAEntityContainer::~fwSoAEntityContainer() {
	RemoveAllEntities();
}

void fwSoAEntityContainer::SetEntityDescBlock(const fwEntityDescSoA& soa, const int b, const fwEntityDesc& entityDescA, const fwEntityDesc& entityDescB, const fwEntityDesc& entityDescC, const fwEntityDesc& entityDescD)
{
	{
		const int	i = b << 2;

		soa.m_entityPointers[i] = entityDescA.m_pEntity;
		soa.m_entityPointers[i+1] = entityDescB.m_pEntity;
		soa.m_entityPointers[i+2] = entityDescC.m_pEntity;
		soa.m_entityPointers[i+3] = entityDescD.m_pEntity;

		soa.m_phaseVisibilityMasks[i] = entityDescA.m_phaseVisibilityMask;
		soa.m_phaseVisibilityMasks[i+1] = entityDescB.m_phaseVisibilityMask;
		soa.m_phaseVisibilityMasks[i+2] = entityDescC.m_phaseVisibilityMask;
		soa.m_phaseVisibilityMasks[i+3] = entityDescD.m_phaseVisibilityMask;

		soa.m_visibilityTypes[i] = entityDescA.m_visibilityType;
		soa.m_visibilityTypes[i+1] = entityDescB.m_visibilityType;
		soa.m_visibilityTypes[i+2] = entityDescC.m_visibilityType;
		soa.m_visibilityTypes[i+3] = entityDescD.m_visibilityType;

		soa.m_lodDistBackups[i] = entityDescA.m_nLodDistBackup;
		soa.m_lodDistBackups[i+1] = entityDescB.m_nLodDistBackup;
		soa.m_lodDistBackups[i+2] = entityDescC.m_nLodDistBackup;
		soa.m_lodDistBackups[i+3] = entityDescD.m_nLodDistBackup;

		soa.m_flags[i] = entityDescA.sFlags;
		soa.m_flags[i+1] = entityDescB.sFlags;
		soa.m_flags[i+2] = entityDescC.sFlags;
		soa.m_flags[i+3] = entityDescD.sFlags;
	}

	{
		Vec4V	unused(V_ZERO);

		Transpose4x4(
			soa.m_x[b],
			soa.m_y[b],
			soa.m_z[b],
			unused,
			entityDescA.m_positionAndUnused,
			entityDescB.m_positionAndUnused,
			entityDescC.m_positionAndUnused,
			entityDescD.m_positionAndUnused );

		Transpose4x4(
			soa.m_extentXY[b],
			soa.m_extentZAndRadius[b],
			soa.m_vecToLodXY[b],
			soa.m_vecToLodZandFlags2[b],
			entityDescA.m_extentAndRadiusAndVecToLodAndFlags2,
			entityDescB.m_extentAndRadiusAndVecToLodAndFlags2,
			entityDescC.m_extentAndRadiusAndVecToLodAndFlags2,
			entityDescD.m_extentAndRadiusAndVecToLodAndFlags2 );
	}
}

void fwSoAEntityContainer::GetEntityDescBlock(const fwEntityDescSoA& soa, const int b, fwEntityDesc& entityDescA, fwEntityDesc& entityDescB, fwEntityDesc& entityDescC, fwEntityDesc& entityDescD)
{
	{
		const int	i = b << 2;

		entityDescA.m_pEntity = soa.m_entityPointers[i];
		entityDescB.m_pEntity = soa.m_entityPointers[i+1];
		entityDescC.m_pEntity = soa.m_entityPointers[i+2];
		entityDescD.m_pEntity = soa.m_entityPointers[i+3];

		entityDescA.m_phaseVisibilityMask = soa.m_phaseVisibilityMasks[i];
		entityDescB.m_phaseVisibilityMask = soa.m_phaseVisibilityMasks[i+1];
		entityDescC.m_phaseVisibilityMask = soa.m_phaseVisibilityMasks[i+2];
		entityDescD.m_phaseVisibilityMask = soa.m_phaseVisibilityMasks[i+3];

		entityDescA.m_visibilityType = soa.m_visibilityTypes[i];
		entityDescB.m_visibilityType = soa.m_visibilityTypes[i+1];
		entityDescC.m_visibilityType = soa.m_visibilityTypes[i+2];
		entityDescD.m_visibilityType = soa.m_visibilityTypes[i+3];

		entityDescA.m_nLodDistBackup = soa.m_lodDistBackups[i];
		entityDescB.m_nLodDistBackup = soa.m_lodDistBackups[i+1];
		entityDescC.m_nLodDistBackup = soa.m_lodDistBackups[i+2];
		entityDescD.m_nLodDistBackup = soa.m_lodDistBackups[i+3];

		entityDescA.sFlags = soa.m_flags[i];
		entityDescB.sFlags = soa.m_flags[i+1];
		entityDescC.sFlags = soa.m_flags[i+2];
		entityDescD.sFlags = soa.m_flags[i+3];
	}

	{
		Vec4V	unused(V_ZERO);

		Transpose4x4(
			entityDescA.m_positionAndUnused,
			entityDescB.m_positionAndUnused,
			entityDescC.m_positionAndUnused,
			entityDescD.m_positionAndUnused,
			soa.m_x[b],
			soa.m_y[b],
			soa.m_z[b],
			unused );

		Transpose4x4(
			entityDescA.m_extentAndRadiusAndVecToLodAndFlags2,
			entityDescB.m_extentAndRadiusAndVecToLodAndFlags2,
			entityDescC.m_extentAndRadiusAndVecToLodAndFlags2,
			entityDescD.m_extentAndRadiusAndVecToLodAndFlags2,
			soa.m_extentXY[b],
			soa.m_extentZAndRadius[b],
			soa.m_vecToLodXY[b],
			soa.m_vecToLodZandFlags2[b] );
	}
}

fwEntityDescSoA fwSoAEntityContainer::InterpretAsEntityDescSoa(fwEntityDesc* entityDescs, const int entityCount)
{
	AlignedAssert( entityCount, 4 );
	AlignedAssert( entityDescs, 16);

	fwEntityDescSoA				soa;
	const int					blockCount = entityCount >> 2;
	u8*							temp = reinterpret_cast< u8* >(entityDescs);

	soa.m_entityCount			= entityCount;
	soa.m_blockCount			= blockCount;
	soa.m_x						= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_x,						16 );
	soa.m_y						= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_y,						16 );
	soa.m_z						= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_z,						16 );
	soa.m_extentXY				= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_extentXY,				16 );
	soa.m_extentZAndRadius		= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_extentZAndRadius,		16 );
	soa.m_vecToLodXY			= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_vecToLodXY,			16 );
	soa.m_vecToLodZandFlags2	= (Vec4V*)temp;		temp += blockCount*sizeof(Vec4V);		AlignedAssert( soa.m_vecToLodZandFlags2,	16 );
	soa.m_entityPointers		= (fwEntity**)temp;	temp += entityCount*sizeof(fwEntity*);	AlignedAssert( soa.m_entityPointers,		4 );
	soa.m_phaseVisibilityMasks	= (fwFlags16*)temp;	temp += entityCount*sizeof(fwFlags16);	AlignedAssert( soa.m_phaseVisibilityMasks,	2 );
	soa.m_visibilityTypes		= (u16*)temp;		temp += entityCount*sizeof(u16);		AlignedAssert( soa.m_visibilityTypes,		2 );
	soa.m_lodDistBackups		= (u16*)temp;		temp += entityCount*sizeof(u16);		AlignedAssert( soa.m_lodDistBackups,		2 );
	soa.m_flags					= (fwEntityDescFlags*)temp;									AlignedAssert( soa.m_flags,					2 );

	return soa;
}

void fwSoAEntityContainer::RemoveAllEntities()
{
	if ( !m_dataHandle )
		return;

	fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount );

	for (int i = 0; i < soa.m_entityCount; ++i)
	{
		if ( soa.m_entityPointers[i] )
		{
			soa.m_entityPointers[i]->SetOwnerEntityContainer( NULL );
		}
	}
	DeleteDataHandle();
}

#if __BANK
fwEntityDesc* fwSoAEntityContainer::GetEntityDescForEntity(const fwEntity*)
{
	Assertf( false, "GetEntityDescForEntity is currently not implemented for SoA entity containers, sorry!" );
	return NULL;
}
#endif

void fwSoAEntityContainer::UpdateEntityDescriptorInNodeInternal(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();

	// REPLACE CALL TO THIS WITH FAST IMPLEMENTATION OF IT 
	fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount );
	for (u32 i = 0; i < m_entityCount; ++i)
	{
		if (soa.m_entityPointers[i] == entity)
		{
			fwEntityDesc	entityDesc;
			fwBaseEntityContainer::ms_updateEntityDescFunc(entity, &entityDesc);
			UpdateSingleSOAEntityDesc(&soa, i, entityDesc);
			return;
		}
	}

	SceneGraphAssertf( false, "The entity to be updated was not found in the node!" );
}


void fwSoAEntityContainer::UpdateSingleSOAEntityDesc(fwEntityDescSoA *pSoa, u32 i, const fwEntityDesc& entityDesc)
{
	pSoa->m_entityPointers[i]		= entityDesc.m_pEntity;
	pSoa->m_phaseVisibilityMasks[i] = entityDesc.m_phaseVisibilityMask;
	pSoa->m_visibilityTypes[i]		= entityDesc.m_visibilityType;
	pSoa->m_lodDistBackups[i]		= entityDesc.m_nLodDistBackup;
	pSoa->m_flags[i]				= entityDesc.sFlags;

	float *x = ((float*)&pSoa->m_x[0]);
	float *y = ((float*)&pSoa->m_y[0]);
	float *z = ((float*)&pSoa->m_z[0]);

	u32 *extentXY   = ((u32*)&pSoa->m_extentXY[0]);
	u32 *extentZR	= ((u32*)&pSoa->m_extentZAndRadius[0]);
	u32 *vecToLodXY = ((u32*)&pSoa->m_vecToLodXY[0]);
	u32 *vecToLodZF = ((u32*)&pSoa->m_vecToLodZandFlags2[0]);
	
	x[i] = entityDesc.m_positionAndUnused.GetXf();
	y[i] = entityDesc.m_positionAndUnused.GetYf();
	z[i] = entityDesc.m_positionAndUnused.GetZf();
	
	extentXY[i]   = entityDesc.m_extentAndRadiusAndVecToLodAndFlags2.GetXi();
	extentZR[i]   = entityDesc.m_extentAndRadiusAndVecToLodAndFlags2.GetYi();
	vecToLodXY[i] = entityDesc.m_extentAndRadiusAndVecToLodAndFlags2.GetZi();
	vecToLodZF[i] = entityDesc.m_extentAndRadiusAndVecToLodAndFlags2.GetWi();
}

void fwSoAEntityContainer::GetApproxBoundingBox(const fwEntityDescSoA& soa, u32 index, spdAABB& aabb)
{
	float *x = ((float*)&soa.m_x[0]);
	float *y = ((float*)&soa.m_y[0]);
	float *z = ((float*)&soa.m_z[0]);

	u32 *extentXY   = ((u32*)&soa.m_extentXY[0]);
	u32 *extentZR	= ((u32*)&soa.m_extentZAndRadius[0]);

	const Vec3V centre = VECTOR3_TO_VEC3V(Vector3(x[index], y[index], z[index]));

	Vec4V extentAndRadiusInit(V_ZERO);
	extentAndRadiusInit.SetXi(extentXY[index]);
	extentAndRadiusInit.SetYi(extentZR[index]);

	const Vec4V extentAndRadius	= Float16Vec4UnpackFromXY( extentAndRadiusInit );
	const Vec3V extent			= extentAndRadius.GetXYZ();

	const Vec3V boxMin			= centre - extent;
	const Vec3V boxMax			= centre + extent;

	aabb.Set(boxMin, boxMax);
}


void fwSoAEntityContainer::GetApproxBoundingSphere(const fwEntityDescSoA& soa, u32 index, spdSphere& sphere)
{
	float *x = ((float*)&soa.m_x[0]);
	float *y = ((float*)&soa.m_y[0]);
	float *z = ((float*)&soa.m_z[0]);

	u32 *extentXY   = ((u32*)&soa.m_extentXY[0]);
	u32 *extentZR	= ((u32*)&soa.m_extentZAndRadius[0]);

	const Vec3V centre = VECTOR3_TO_VEC3V(Vector3(x[index], y[index], z[index]));

	Vec4V extentAndRadiusInit(V_ZERO);
	extentAndRadiusInit.SetXi(extentXY[index]);
	extentAndRadiusInit.SetYi(extentZR[index]);

	const Vec4V   extentAndRadius	= Float16Vec4UnpackFromXY( extentAndRadiusInit );
	const ScalarV radius			= extentAndRadius.GetW();

	sphere.Set(centre, radius);
}

void fwSoAEntityContainer::SetLodDistCached(const fwEntityDescSoA& soa, u32 index, u32 lodDist)
{
	soa.m_lodDistBackups[index] = static_cast<u16>(lodDist);
}
	
void fwSoAEntityContainer::RemoveEntityInternal(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() == this );

	SceneGraphAssertf( m_dataHandle, "Trying to remove an entity from a node that is already empty!" );
	if ( !m_dataHandle )
		return;

	fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount);
	u32 i;
	bool found = false;
	for (i = 0; i < m_entityCount; ++i)
	{
		if (soa.m_entityPointers[i] == entity)
		{
			found = true;
			break;
		}
	}

	if (found)
	{
		// find last valid entity desc
		int lastEntry;
		for (lastEntry = m_entityCount - 1; lastEntry >= 0; --lastEntry)
		{
			if (soa.m_entityPointers[lastEntry])
			{
				break;
			}
		}
		
		Assertf(lastEntry >= 0, "Failed to find entry");

		entity->SetOwnerEntityContainer( NULL );

		soa.m_entityPointers[i]		  = soa.m_entityPointers[lastEntry];
		soa.m_phaseVisibilityMasks[i] = soa.m_phaseVisibilityMasks[lastEntry];
		soa.m_visibilityTypes[i]	  = soa.m_visibilityTypes[lastEntry];
		soa.m_lodDistBackups[i]		  = soa.m_lodDistBackups[lastEntry];
		soa.m_flags[i]				  = soa.m_flags[lastEntry];

		float *x = ((float*)&soa.m_x[0]);
		float *y = ((float*)&soa.m_y[0]);
		float *z = ((float*)&soa.m_z[0]);

		u32 *extentXY   = ((u32*)&soa.m_extentXY[0]);
		u32 *extentZR	= ((u32*)&soa.m_extentZAndRadius[0]);
		u32 *vecToLodXY = ((u32*)&soa.m_vecToLodXY[0]);
		u32 *vecToLodZF = ((u32*)&soa.m_vecToLodZandFlags2[0]);

		x[i] = x[lastEntry];
		y[i] = y[lastEntry];
		z[i] = z[lastEntry];

		extentXY[i]   = extentXY[lastEntry];
		extentZR[i]   = extentZR[lastEntry];  
		vecToLodXY[i] = vecToLodXY[lastEntry];
		vecToLodZF[i] = vecToLodZF[lastEntry];

		// I am not decrementing m_entityCount on purpose here because for SoA the count must remain a multiple of 4
		// Setting the entity pointer to NULL should do effectively the same thing

		soa.m_entityPointers[lastEntry] = NULL;

		return;
	}
	SceneGraphAssertf( false, "Tried to remove an entity from a node that didn't contain the entity itself" );

}
#if __BANK
bool fwSoAEntityContainer::ContainsEntityDesc(fwEntity *entity)
{
	fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_cachedEntityDescArray, m_entityCount);
	bool found = false;
	for (int i = 0; i < m_entityCount; ++i)
	{
		if (soa.m_entityPointers[i] == entity)
		{
			found = true;
			break;
		}
	}
	return found;
}
#endif

} // namespace rage
