#include "fwscene/world/SceneGraphNode.h"
#include "entity/entity.h"

namespace rage {

u32 fwSceneGraphNode::GetChildrenCount() const
{
	u32					childrenCount = 0;
	fwSceneGraphNode*	current = m_firstChild;

	while ( current )
	{
		childrenCount++;
		current = current->GetNext();
	}

	return childrenCount;
}

fwSceneGraphNode* fwSceneGraphNode::GetChild(const u32 position)
{
	u32					currentPosition = 0;
	fwSceneGraphNode*	current = m_firstChild;

	while ( currentPosition < position )
	{
		currentPosition++;
		if ( current )
			current = current->GetNext();
	}

	return current;
}

bool fwSceneGraphNode::IsChild(const fwSceneGraphNode* child) const
{
	fwSceneGraphNode*	current = m_firstChild;

	while ( current )
	{
		if ( current == child )
			return true;

		current = current->GetNext();
	}

	return false;
}

void fwSceneGraphNode::AddChild(fwSceneGraphNode* child)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssertf( !child->GetIsInWorld(), "Attempting to add a scene graph node to the world, but it is already in the world" );

	if ( !m_firstChild )
		m_firstChild = child;
	else
	{
		fwSceneGraphNode*	current = m_firstChild;

		while ( current->GetNext() )
			current = current->GetNext();

		current->SetNext( child );
		child->SetNext( NULL );
	}

	child->SetIsInWorld(true);
}

void fwSceneGraphNode::RemoveChild(fwSceneGraphNode* child)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssertf( child->GetIsInWorld(), "Attempting to remove a scene graph node from the world, but it is not in the world" );

	if ( m_firstChild == child )
	{
		m_firstChild = m_firstChild->GetNext();
		child->SetNext( NULL );
	}
	else
	{
		fwSceneGraphNode*	current = m_firstChild;
		fwSceneGraphNode*	prev = NULL;

		while ( current && current != child )
		{
			prev = current;
			current = current->GetNext();
		}

		SceneGraphAssertf( current, "Couldn't find the specified child in the scene graph node" );
		if ( current )
		{
			prev->SetNext( child->GetNext() );
			child->SetNext( NULL );
		}
	}

	child->SetIsInWorld(false);
}

#if __BANK && !__SPU
fwSceneGraphNode::GetSceneGraphNodeDebugNameFuncType fwSceneGraphNode::sm_GetSceneGraphNodeDebugNameFunc = NULL;
#endif // __BANK && !__SPU

} // namespace rage
