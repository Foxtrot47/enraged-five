/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/world/WorldLimits.h
// PURPOSE : defines the dimensions of the world representation and other related limits
// AUTHOR :  Ian Kiigan
// CREATED : 31/03/10
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _INC_WORLDLIMITS_H_
#define _INC_WORLDLIMITS_H_

#include "spatialdata/aabb.h"

// ultimate limits for the world - the code does not support going beyond these limits
#define WORLDLIMITS_XMIN			(-16000.0f)
#define WORLDLIMITS_XMAX			(16000.0f)
#define WORLDLIMITS_YMIN			(-16000.0f)
#define WORLDLIMITS_YMAX			(16000.0f)
#define WORLDLIMITS_ZMIN			(-1700.0f)
#define WORLDLIMITS_ZMAX			(2700.0f)

// the dimensions of the world representation - anything outside of this is pushed into top quad tree node
#define WORLDLIMITS_REP_YMAX		(8192.0f)
#define WORLDLIMITS_REP_YMIN		(-8192.0f)
#define WORLDLIMITS_REP_XMAX		(8192.0f)
#define WORLDLIMITS_REP_XMIN		(-8192.0f)

namespace rage {

extern spdAABB g_WorldLimits_MapDataExtentsAABB;	// aabb for entire map, built up based on IPL bounds at load-time
extern spdAABB g_WorldLimits_AABB;					// aabb for ultimate world limits

} // namespace rage

#endif //_INC_WORLDLIMITS_H_
