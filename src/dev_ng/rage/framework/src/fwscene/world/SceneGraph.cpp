#include "fwscene/world/SceneGraph.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"
#include "fwscene/world/InteriorSceneGraphNode.h"
#include "fwscene/world/StreamedSceneGraphNode.h"

RAGE_DEFINE_CHANNEL(sceneGraph)

namespace rage {

int							fwSceneGraph::ms_sceneGraphLockCount;
u8*							fwSceneGraph::ms_poolAllocatedMemory;
u8*							fwSceneGraph::ms_poolStorage;
u32							fwSceneGraph::ms_poolStorageSize;
fwPointerFixer				fwSceneGraph::ms_fixer;

fwEntityContainer*			fwSceneGraph::ms_entityContainerStorage;
fwFixedEntityContainer*		fwSceneGraph::ms_fixedEntityContainerStorage;
fwSoAEntityContainer*		fwSceneGraph::ms_soaEntityContainerStorage;
fwExteriorSceneGraphNode*	fwSceneGraph::ms_exteriorSceneNodeStorage;
fwStreamedSceneGraphNode*	fwSceneGraph::ms_streamedSceneNodeStorage;
fwInteriorSceneGraphNode*	fwSceneGraph::ms_interiorSceneNodeStorage;
fwRoomSceneGraphNode*		fwSceneGraph::ms_roomSceneNodeStorage;
fwPortalSceneGraphNode*		fwSceneGraph::ms_portalSceneNodeStorage;

template <typename Type>
int GetStorageSize();

template <> int GetStorageSize<fwEntityContainer>()			{ return sizeof(fwEntityContainer) * fwSceneGraph::ENTITY_CONTAINER_POOL_SIZE; }
template <> int GetStorageSize<fwFixedEntityContainer>()	{ return sizeof(fwFixedEntityContainer) * fwSceneGraph::FIXED_ENTITY_CONTAINER_POOL_SIZE; }
template <> int GetStorageSize<fwSoAEntityContainer>()		{ return sizeof(fwSoAEntityContainer) * fwSceneGraph::SOA_ENTITY_CONTAINER_POOL_SIZE; }
template <> int GetStorageSize<fwExteriorSceneGraphNode>()	{ return sizeof(fwExteriorSceneGraphNode) * fwSceneGraph::EXTERIOR_SCENE_NODE_POOL_SIZE; }
template <> int GetStorageSize<fwStreamedSceneGraphNode>()	{ return sizeof(fwStreamedSceneGraphNode) * fwSceneGraph::STREAMED_SCENE_NODE_POOL_SIZE; }
template <> int GetStorageSize<fwInteriorSceneGraphNode>()	{ return sizeof(fwInteriorSceneGraphNode) * fwSceneGraph::INTERIOR_SCENE_NODE_POOL_SIZE; }
template <> int GetStorageSize<fwRoomSceneGraphNode>()		{ return sizeof(fwRoomSceneGraphNode) * fwSceneGraph::ROOM_SCENE_NODE_POOL_SIZE; }
template <> int GetStorageSize<fwPortalSceneGraphNode>()	{ return sizeof(fwPortalSceneGraphNode) * fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE; }

void fwSceneGraph::AssignStorageMemory(u8* memory, const u32 ASSERT_ONLY(memorySize))
{
	u8*		startMemory = memory;

#if __WIN32PC || RSG_DURANGO || RSG_ORBIS
	// On PC, ms_portalSceneNodeStorage needs to be aligned to 16 bytes.
	CompileTimeAssert16(sizeof(fwEntityContainer) * fwSceneGraph::ENTITY_CONTAINER_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwFixedEntityContainer) * fwSceneGraph::FIXED_ENTITY_CONTAINER_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwSoAEntityContainer) * fwSceneGraph::SOA_ENTITY_CONTAINER_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwExteriorSceneGraphNode) * fwSceneGraph::EXTERIOR_SCENE_NODE_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwStreamedSceneGraphNode) * fwSceneGraph::STREAMED_SCENE_NODE_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwInteriorSceneGraphNode) * fwSceneGraph::INTERIOR_SCENE_NODE_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwRoomSceneGraphNode) * fwSceneGraph::ROOM_SCENE_NODE_POOL_SIZE);
	CompileTimeAssert16(sizeof(fwPortalSceneGraphNode) * fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE);
#endif

	ms_entityContainerStorage = (fwEntityContainer*) memory;			memory += GetStorageSize<fwEntityContainer>();
	ms_fixedEntityContainerStorage = (fwFixedEntityContainer*) memory;	memory += GetStorageSize<fwFixedEntityContainer>();
	ms_soaEntityContainerStorage = (fwSoAEntityContainer*) memory;		memory += GetStorageSize<fwSoAEntityContainer>();
	ms_exteriorSceneNodeStorage = (fwExteriorSceneGraphNode*) memory;	memory += GetStorageSize<fwExteriorSceneGraphNode>();
	ms_streamedSceneNodeStorage = (fwStreamedSceneGraphNode*) memory;	memory += GetStorageSize<fwStreamedSceneGraphNode>();
	ms_interiorSceneNodeStorage = (fwInteriorSceneGraphNode*) memory;	memory += GetStorageSize<fwInteriorSceneGraphNode>();
	ms_roomSceneNodeStorage = (fwRoomSceneGraphNode*) memory;			memory += GetStorageSize<fwRoomSceneGraphNode>();
	ms_portalSceneNodeStorage = (fwPortalSceneGraphNode*) memory;		memory += GetStorageSize<fwPortalSceneGraphNode>();

	ms_poolStorage = startMemory;
	ms_poolStorageSize = ptrdiff_t_to_int(memory - startMemory);
	
	SceneGraphAssertf( ms_poolStorageSize <= memorySize, "Assigning more scene graph pool memory than actually available" );
}

#if __SPU

void fwSceneGraph::InitSpuPools(u8* spuMemory, sysEaPtr<u8> ppuMemory, const u32 memorySize)
{
	ms_poolAllocatedMemory = NULL;
	AssignStorageMemory( spuMemory, memorySize );
	ms_fixer.Set( spuMemory, ppuMemory, memorySize );

	for (int i = 0; i < ENTITY_CONTAINER_POOL_SIZE; ++i)
		ms_entityContainerStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < FIXED_ENTITY_CONTAINER_POOL_SIZE; ++i)
		ms_fixedEntityContainerStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < SOA_ENTITY_CONTAINER_POOL_SIZE; ++i)
		ms_soaEntityContainerStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < EXTERIOR_SCENE_NODE_POOL_SIZE; ++i)
		ms_exteriorSceneNodeStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < STREAMED_SCENE_NODE_POOL_SIZE; ++i)
		ms_streamedSceneNodeStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < INTERIOR_SCENE_NODE_POOL_SIZE; ++i)
		ms_interiorSceneNodeStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < ROOM_SCENE_NODE_POOL_SIZE; ++i)
		ms_roomSceneNodeStorage[i].FixupPointers( ms_fixer );

	for (int i = 0; i < PORTAL_SCENE_NODE_POOL_SIZE; ++i)
		ms_portalSceneNodeStorage[i].FixupPointers( ms_fixer );
}

#else // __SPU

void fwSceneGraph::InitPools()
{
	const int	poolStorageSize =
		GetStorageSize<fwEntityContainer>()			+
		GetStorageSize<fwFixedEntityContainer>()	+
		GetStorageSize<fwSoAEntityContainer>()		+
		GetStorageSize<fwExteriorSceneGraphNode>()	+
		GetStorageSize<fwStreamedSceneGraphNode>()	+
		GetStorageSize<fwInteriorSceneGraphNode>()	+
		GetStorageSize<fwRoomSceneGraphNode>()		+
		GetStorageSize<fwPortalSceneGraphNode>();

	const int	poolFlagsSize =
		ENTITY_CONTAINER_POOL_SIZE			+
		FIXED_ENTITY_CONTAINER_POOL_SIZE	+
		SOA_ENTITY_CONTAINER_POOL_SIZE		+
		EXTERIOR_SCENE_NODE_POOL_SIZE		+
		STREAMED_SCENE_NODE_POOL_SIZE		+
		INTERIOR_SCENE_NODE_POOL_SIZE		+
		ROOM_SCENE_NODE_POOL_SIZE			+
		PORTAL_SCENE_NODE_POOL_SIZE;

	ms_poolAllocatedMemory = rage_aligned_new(16) u8[ poolStorageSize + poolFlagsSize ];
	Assert16(ms_poolAllocatedMemory);

	u8*		memory = ms_poolAllocatedMemory;
	AssignStorageMemory( memory, poolStorageSize );
	memory += ms_poolStorageSize;

	u8*		entityContainerFlags = memory;			memory += ENTITY_CONTAINER_POOL_SIZE;
	u8*		fixedEntityContainerFlags = memory;		memory += FIXED_ENTITY_CONTAINER_POOL_SIZE;
	u8*		soaEntityContainerFlags = memory;		memory += SOA_ENTITY_CONTAINER_POOL_SIZE;
	u8*		exteriorSceneNodeFlags = memory;		memory += EXTERIOR_SCENE_NODE_POOL_SIZE;
	u8*		streamedSceneNodeFlags = memory;		memory += STREAMED_SCENE_NODE_POOL_SIZE;
	u8*		interiorSceneNodeFlags = memory;		memory += INTERIOR_SCENE_NODE_POOL_SIZE;
	u8*		roomSceneNodeFlags = memory;			memory += ROOM_SCENE_NODE_POOL_SIZE;
	u8*		portalSceneNodeFlags = memory;

	fwEntityContainer::_ms_pPool			= rage_new atIteratablePool<fwEntityContainer>(			ENTITY_CONTAINER_POOL_SIZE,			ms_entityContainerStorage,		entityContainerFlags,		"fwEntityContainer" );
	fwFixedEntityContainer::_ms_pPool		= rage_new atIteratablePool<fwFixedEntityContainer>(	FIXED_ENTITY_CONTAINER_POOL_SIZE,	ms_fixedEntityContainerStorage,	fixedEntityContainerFlags,	"fwFixedEntityContainer" );
	fwSoAEntityContainer::_ms_pPool			= rage_new atIteratablePool<fwSoAEntityContainer>(		SOA_ENTITY_CONTAINER_POOL_SIZE,		ms_soaEntityContainerStorage,	soaEntityContainerFlags,	"fwSoAEntityContainer" );
	fwExteriorSceneGraphNode::_ms_pPool		= rage_new fwPool<fwExteriorSceneGraphNode>(			EXTERIOR_SCENE_NODE_POOL_SIZE,		ms_exteriorSceneNodeStorage,	exteriorSceneNodeFlags,		"fwExteriorSceneGraphNode" );
	fwStreamedSceneGraphNode::_ms_pPool		= rage_new fwPool<fwStreamedSceneGraphNode>(			STREAMED_SCENE_NODE_POOL_SIZE,		ms_streamedSceneNodeStorage,	streamedSceneNodeFlags,		"fwStreamedSceneGraphNode" );
	fwInteriorSceneGraphNode::_ms_pPool		= rage_new fwPool<fwInteriorSceneGraphNode>(			INTERIOR_SCENE_NODE_POOL_SIZE,		ms_interiorSceneNodeStorage,	interiorSceneNodeFlags,		"fwInteriorSceneGraphNode" );
	fwRoomSceneGraphNode::_ms_pPool			= rage_new fwPool<fwRoomSceneGraphNode>(				ROOM_SCENE_NODE_POOL_SIZE,			ms_roomSceneNodeStorage,		roomSceneNodeFlags,			"fwRoomSceneGraphNode" );
	fwPortalSceneGraphNode::_ms_pPool		= rage_new fwPool<fwPortalSceneGraphNode>(				PORTAL_SCENE_NODE_POOL_SIZE,		ms_portalSceneNodeStorage,		portalSceneNodeFlags,		"fwPortalSceneGraphNode" );

	RegisterInPoolTracker(fwEntityContainer::_ms_pPool, "fwEntityContainer");
	RegisterInPoolTracker(fwFixedEntityContainer::_ms_pPool, "fwFixedEntityContainer");
	RegisterInPoolTracker(fwSoAEntityContainer::_ms_pPool, "fwSoAEntityContainer");

	// Prevent overrides of the size of the fwRoomSceneGraphNode pool from the configuration file, and make
	// sure the error message if we run out doesn't talk about that file. Normally, just using
	// FW_INSTANTIATE_CLASS_POOL_LOCKED_SIZE instead of FW_INSTANTIATE_CLASS_POOL would do this
	// automatically, but for some reason the code above bypasses the InitPool() functions from
	// the macro. /FF
	fwConfigManager::GetInstance().LockPoolSize(atHashString("fwRoomSceneGraphNode",0x9f0123e3));
	fwRoomSceneGraphNode::_ms_pPool->SetSizeIsFromConfigFile(false);

	// TODO: Should we lock the rest of the pools defined above as well? /FF
}

void fwSceneGraph::ShutdownPools()
{
	UnregisterInPoolTracker(fwEntityContainer::_ms_pPool);

	delete fwEntityContainer::_ms_pPool;
	fwEntityContainer::_ms_pPool = NULL;

	delete fwFixedEntityContainer::_ms_pPool;
	fwFixedEntityContainer::_ms_pPool = NULL;

	delete fwSoAEntityContainer::_ms_pPool;
	fwSoAEntityContainer::_ms_pPool = NULL;

	delete fwExteriorSceneGraphNode::_ms_pPool;
	fwExteriorSceneGraphNode::_ms_pPool = NULL;

	delete fwStreamedSceneGraphNode::_ms_pPool;
	fwStreamedSceneGraphNode::_ms_pPool = NULL;

	delete fwInteriorSceneGraphNode::_ms_pPool;
	fwInteriorSceneGraphNode::_ms_pPool = NULL;

	delete fwRoomSceneGraphNode::_ms_pPool;
	fwRoomSceneGraphNode::_ms_pPool = NULL;

	delete fwPortalSceneGraphNode::_ms_pPool;
	fwPortalSceneGraphNode::_ms_pPool = NULL;

	delete ms_poolAllocatedMemory;
	ms_poolAllocatedMemory = NULL;

	ms_poolStorage = NULL;
	ms_poolStorageSize = 0;
}

#endif // else __SPU

s16 fwSceneGraph::ComputeSceneNodeIndex(const fwSceneGraphNode* sceneNode)
{
	const int	exteriorSceneNodeOffset = 0;
	const int	streamedSceneNodeOffset = exteriorSceneNodeOffset + EXTERIOR_SCENE_NODE_POOL_SIZE;
	const int	interiorSceneNodeOffset = streamedSceneNodeOffset + STREAMED_SCENE_NODE_POOL_SIZE;
	const int	roomSceneNodeOffset = interiorSceneNodeOffset + INTERIOR_SCENE_NODE_POOL_SIZE;
	const int	portalSceneNodeOffset = roomSceneNodeOffset + ROOM_SCENE_NODE_POOL_SIZE;
	ptrdiff_t	index;

	switch ( sceneNode->GetType() )
	{
		case SCENE_GRAPH_NODE_TYPE_EXTERIOR:	index = exteriorSceneNodeOffset + ( static_cast< const fwExteriorSceneGraphNode* >( sceneNode ) - ms_exteriorSceneNodeStorage ); break;
		case SCENE_GRAPH_NODE_TYPE_STREAMED:	index = streamedSceneNodeOffset + ( static_cast< const fwStreamedSceneGraphNode* >( sceneNode ) - ms_streamedSceneNodeStorage ); break;
		case SCENE_GRAPH_NODE_TYPE_INTERIOR:	index = interiorSceneNodeOffset + ( static_cast< const fwInteriorSceneGraphNode* >( sceneNode ) - ms_interiorSceneNodeStorage ); break;
		case SCENE_GRAPH_NODE_TYPE_ROOM:		index = roomSceneNodeOffset + ( static_cast< const fwRoomSceneGraphNode* >( sceneNode ) - ms_roomSceneNodeStorage ); break;
		case SCENE_GRAPH_NODE_TYPE_PORTAL:		index = portalSceneNodeOffset + ( static_cast< const fwPortalSceneGraphNode* >( sceneNode ) - ms_portalSceneNodeStorage ); break;
		default:								index = -1; break;
	}

	return static_cast< s16 >( index );
}

} // namespace rage
