#include "fwscene/world/SceneGraphVisitor.h"

namespace rage {

fwSceneGraphVisitor::fwSceneGraphVisitor(fwSceneGraphNode* root)
{
	m_top = 0;
	m_stack[ m_top ] = root;
	m_childrenVisited[ m_top ] = false;
}

void fwSceneGraphVisitor::Next()
{
	Assert( !AtEnd() );

	while ( m_top >= 0 )
	{
		fwSceneGraphNode*	current = m_stack[ m_top ];
		fwSceneGraphNode*	firstChild = current->GetFirstChild();

		if ( !m_childrenVisited[ m_top ] && firstChild )
		{
			m_childrenVisited[ m_top ] = true;

			++m_top;
			m_stack[ m_top ] = firstChild;
			m_childrenVisited[ m_top ] = false;
			break;
		}
		else
		{
			fwSceneGraphNode*	sibling = current->GetNext();

			if ( sibling )
			{
				m_stack[ m_top ] = sibling;
				m_childrenVisited[ m_top ] = false;
				break;
			}
			else
			{
				--m_top;
				continue;
			}
		}
	}
}

fwSceneGraphNode* fwSceneGraphVisitor::GetStackElement(const s16 position) const
{
	if ( position >= 0 )
		return m_stack[ position ];
	else
		return m_stack[ m_top + 1 + position ];
}

} // namespace rage
