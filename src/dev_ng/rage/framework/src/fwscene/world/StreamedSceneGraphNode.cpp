#include "fwscene/world/StreamedSceneGraphNode.h"

namespace rage {

FW_INSTANTIATE_CLASS_POOL_SPILLOVER( fwStreamedSceneGraphNode, fwSceneGraph::STREAMED_SCENE_NODE_POOL_SIZE, 0.54f, atHashString("fwStreamedSceneGraphNode",0x94be9e58) );

fwStreamedSceneGraphNode::fwStreamedSceneGraphNode(const bool containsHighDetails, const bool disabled) :
	fwSceneGraphNode( SCENE_GRAPH_NODE_TYPE_STREAMED )
{
	m_aabb.Invalidate();
	m_firstContainer = NULL;
	GetFlagsRef().ChangeFlag( SCENE_NODE_FLAG_CONTAINS_HIGH_DETAILS, containsHighDetails );
	Enable( !disabled );
}

fwBaseEntityContainer* fwStreamedSceneGraphNode::GetLastStreamedContainer() const
{
	fwBaseEntityContainer*		container = m_firstContainer;

	while (true)
	{
		if ( !container || !container->GetNext() )
			return container;

		container = container->GetNext();
	}
}

void fwStreamedSceneGraphNode::AddStreamedContainer(fwBaseEntityContainer* container)
{
	if ( !m_firstContainer )
		m_firstContainer = container;
	else
		GetLastStreamedContainer()->SetNext( container );
}

void fwStreamedSceneGraphNode::RemoveStreamedContainer(fwBaseEntityContainer* container)
{
	fwBaseEntityContainer*	current = m_firstContainer;
	fwBaseEntityContainer*	prev = NULL;

	while ( true )
	{
		if ( current == container )
		{
			if ( prev )
				prev->SetNext( current->GetNext() );
			else
				m_firstContainer = current;

			return;
		}

		prev = current;
		current = prev->GetNext();
	}
}

void fwStreamedSceneGraphNode::DestroyAllStreamedContainers()
{
	fwBaseEntityContainer*	current = m_firstContainer;
	fwBaseEntityContainer*	next;

	while ( current )
	{
		next = current->GetNext();
		fwBaseEntityContainer::Delete( current );
		current = next;
	}

	m_firstContainer = NULL;
}

} // namespace rage
