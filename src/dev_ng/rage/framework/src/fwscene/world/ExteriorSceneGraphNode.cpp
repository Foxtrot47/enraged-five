#include "fwscene/world/ExteriorSceneGraphNode.h"
#include "fwscene/world/WorldLimits.h"
#include "entity/entity.h"
#include <algorithm>

namespace rage {

FW_INSTANTIATE_CLASS_POOL( fwExteriorSceneGraphNode, fwSceneGraph::EXTERIOR_SCENE_NODE_POOL_SIZE, atHashString("fwExteriorSceneGraphNode",0x3789cbfe) );

enum
{
	DYNAMIC_ENTITY_CONTAINER_SIZE	= 2560,
	UPDATE_LIST_DEFAULT_CAPACITY	= 512,
	STRADDLING_PORTALS_ENTITY_CONTAINER_SIZE = 64,
};

//==================== Utilities ====================//

int FindInsertPosition(fwEntity* entity, const fwEntityDesc* entityDescArray, const int entityDescCount)
{
	int		index;
	int		lo = 0, hi = entityDescCount - 1;

	while ( lo != hi )
	{
		index = (lo + hi) / 2;
		if ( entityDescArray[index].GetEntity() < entity )
			lo = index + 1;
		else
			hi = index;
	}

	return hi;
}

fwEntityDesc* InsertEntityIntoSortedContainer(fwEntity* entity, fwEntityContainer* container)
{
	SceneGraphLockedFatalAssert();

	fwEntityDesc		entityDescCopy = *( container->AppendEntity( entity ) );
	fwEntityDesc*		entityDescArray = container->GetEntityDescArray();
	const u32			entityDescCount = container->GetEntityCount();
	const int			insertPos = FindInsertPosition( entity, entityDescArray, entityDescCount );

	memmove( entityDescArray + insertPos + 1, entityDescArray + insertPos, sizeof(fwEntityDesc) * (entityDescCount - insertPos - 1) );
	entityDescArray[insertPos] = entityDescCopy;

	return entityDescArray + insertPos;
}

void RemoveEntityFromSortedContainer(fwEntity* entity, fwEntityContainer* container)
{
	SceneGraphLockedFatalAssert();

	fwEntityDesc*		entityDescArray = container->GetEntityDescArray();
	const u32			entityDescCount = container->GetEntityCount();
	const int			insertPos = FindInsertPosition( entity, entityDescArray, entityDescCount );
	fwEntityDesc		entityDescCopy = entityDescArray[ insertPos ];

	memmove( entityDescArray + insertPos, entityDescArray + insertPos + 1, sizeof(fwEntityDesc) * (entityDescCount - insertPos - 1) );
	entityDescArray[ entityDescCount - 1 ] = entityDescCopy;
	container->PopEntity();
}

void FlushUpdateListForContainer(atArray< fwEntity* >& updateList, fwEntityContainer* container)
{
	if ( updateList.GetCount() == 0 )
		return;

	SceneGraphLockedFatalAssert();

	// Sort and remove duplicates from the update list

	fwEntity**		updateListStorage = &updateList[0];
	const u32		updateListOldCount = updateList.GetCount();
	u32				updateListNewCount;

	std::sort( updateListStorage, updateListStorage + updateListOldCount );
	updateListNewCount = ptrdiff_t_to_int(std::unique( updateListStorage, updateListStorage + updateListOldCount ) - updateListStorage);

	// Update the entity descriptors

	fwEntityDesc*		entityDescArray = container->GetEntityDescArray();
	const u32			entityDescCount = container->GetEntityCount();
	u32					entityDescIndex = 0;
	fwEntityContainer::UpdateEntityDescFunc	updateFunc = fwEntityContainer::GetUpdateEntityDescFunc();

	for (u32 i = 0; i < updateListNewCount; ++i)
	{
		fwEntity*	updateEntity = updateList[i];

		while ( entityDescIndex < entityDescCount && entityDescArray[entityDescIndex].GetEntity() < updateEntity )
			entityDescIndex++;

		if( entityDescIndex < entityDescCount && updateEntity == entityDescArray[entityDescIndex].GetEntity() )
			updateFunc( updateEntity, &entityDescArray[entityDescIndex] );
	}

	updateList.Reset();
	updateList.Reserve( UPDATE_LIST_DEFAULT_CAPACITY );
}

//==================== fwExteriorSceneGraphNode methods ====================//

fwExteriorSceneGraphNode::fwExteriorSceneGraphNode() :
	fwSceneGraphNode( SCENE_GRAPH_NODE_TYPE_EXTERIOR )
{
	m_staticsContainer = rage_new fwEntityContainer( this );
	m_staticsContainer->SetStorageStep( 100 );

	m_dynamicsContainer = rage_new fwFixedEntityContainer( this, DYNAMIC_ENTITY_CONTAINER_SIZE );

	m_lightsContainer = rage_new fwEntityContainer( this );
	m_lightsContainer->SetStorageStep( 64 );

	m_stradlingPortalsContainer = rage_new fwStraddlingPortalsContainer( this, STRADDLING_PORTALS_ENTITY_CONTAINER_SIZE );
}

fwExteriorSceneGraphNode::~fwExteriorSceneGraphNode()
{
	delete m_staticsContainer;
	m_staticsContainer = NULL;

	delete m_dynamicsContainer;
	m_dynamicsContainer = NULL;

	delete m_lightsContainer;
	m_lightsContainer = NULL;

	delete m_stradlingPortalsContainer;
	m_stradlingPortalsContainer = NULL;
}

fwEntityDesc* fwExteriorSceneGraphNode::AppendEntity(fwEntity* entity, const fwRect& /*rect*/)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() == NULL );

	if ( entity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		fwEntityDesc*				entityDesc = m_dynamicsContainer->AppendEntity( entity );
		fwDynamicEntityComponent*	dynamicComponent = entity->CreateDynamicComponentIfMissing();
		dynamicComponent->SetEntityDesc( entityDesc );
		return entityDesc;
	}
	else if ( entity->IsBaseFlagSet( fwEntity::IS_LIGHT ) )
		return InsertEntityIntoSortedContainer( entity, m_lightsContainer );
	else
		return m_staticsContainer->AppendEntity( entity );
}

void fwExteriorSceneGraphNode::RemoveEntity(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() );
	SceneGraphAssert( entity->GetOwnerEntityContainer()->GetOwnerSceneNode() == this );

	if ( entity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		SceneGraphAssert( entity->GetDynamicComponent() );

		fwDynamicEntityComponent*	dynamicComponent = entity->GetDynamicComponent();
		fwEntityDesc*				entityDesc = dynamicComponent->GetEntityDesc();
		m_dynamicsContainer->RemoveEntityFromDescriptor( entityDesc );
		dynamicComponent->SetEntityDesc( NULL );
	}
	else if ( entity->IsBaseFlagSet( fwEntity::IS_LIGHT ) )
		RemoveEntityFromSortedContainer( entity, m_lightsContainer );
	else
		m_staticsContainer->RemoveEntity( entity );
}
u8 fwExteriorSceneGraphNode::AppendToStraddlingPortalContainer(fwEntity* entity)
{
	SceneGraphLockedFatalAssert();
	return m_stradlingPortalsContainer->AppendEntity(entity);
}

void fwExteriorSceneGraphNode::RemoveFromStraddlingPortalContainer(u8 index)
{
	SceneGraphLockedFatalAssert();
	m_stradlingPortalsContainer->RemoveEntityFromDescriptorIndex(index);
}



void fwExteriorSceneGraphNode::RemoveAllEntities()
{
	SceneGraphLockedFatalAssert();

	m_staticsContainer->RemoveAllEntities();
	m_dynamicsContainer->RemoveAllEntities();
	m_lightsContainer->RemoveAllEntities();
}

void fwExteriorSceneGraphNode::UpdateEntity(fwEntity* entity, const fwRect& /*rect*/)
{
	SceneGraphLockedFatalAssert();
	SceneGraphAssert( entity->GetOwnerEntityContainer() );
	SceneGraphAssert( entity->GetOwnerEntityContainer()->GetOwnerSceneNode() == this );

	if ( entity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		SceneGraphAssert( entity->GetDynamicComponent() );
		
		fwEntityContainer::UpdateEntityDescFunc		updateFunc = fwEntityContainer::GetUpdateEntityDescFunc();
		fwDynamicEntityComponent*					dynamicComponent = entity->GetDynamicComponent();

		updateFunc( entity, dynamicComponent->GetEntityDesc() );
	}
	else if ( entity->IsBaseFlagSet( fwEntity::IS_LIGHT ) )
		m_lightsUpdateList.Grow() = entity;
	else
		m_staticsContainer->UpdateEntityDescriptorInNode( entity );
}

void fwExteriorSceneGraphNode::FlushUpdateList() {
	FlushUpdateListForContainer( m_lightsUpdateList, m_lightsContainer );
}

} // namespace rage
