#ifndef __INC_EXTERIORSCENEGRAPHNODE_H_
#define __INC_EXTERIORSCENEGRAPHNODE_H_

#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/world/EntityContainer.h"
#include "system/eaptr.h"
#include "atl/array.h"

namespace rage {

class fwEntity;

class fwExteriorSceneGraphNode : public fwSceneGraphNode
{
private:
	
	atArray< fwEntity* >		m_lightsUpdateList;
	fwEntityContainer*			m_staticsContainer;
	fwFixedEntityContainer*		m_dynamicsContainer;
	fwEntityContainer*			m_lightsContainer;
	fwStraddlingPortalsContainer*	m_stradlingPortalsContainer;
	ATTR_UNUSED u32						    m_pad[__64BIT ? 2 : 3];
public:

	FW_REGISTER_CLASS_POOL( fwExteriorSceneGraphNode );

	fwExteriorSceneGraphNode();
	~fwExteriorSceneGraphNode();

	void FixupPointers(const fwPointerFixer& fixer)
	{
		fwSceneGraphNode::FixupPointers( fixer );
		fixer.Fix( m_staticsContainer );
		fixer.Fix( m_dynamicsContainer );
		fixer.Fix( m_lightsContainer );
		fixer.Fix( m_stradlingPortalsContainer );
	}

	s16 GetInteriorNodeIndex() const						{ return GetIndex() - fwSceneGraph::FIRST_EXTERIOR_SCENE_NODE_INDEX; }

	fwEntityContainer* GetStaticsContainer() const			{ return m_staticsContainer; }
	fwFixedEntityContainer* GetDynamicsContainer() const	{ return m_dynamicsContainer; }
	fwEntityContainer* GetLightsContainer() const			{ return m_lightsContainer; }
	fwStraddlingPortalsContainer* GeStradlingPortalsContainer() const	{ return m_stradlingPortalsContainer; }

	fwEntityDesc* AppendEntity(fwEntity* entity, const fwRect& rect);
	void RemoveEntity(fwEntity* entity);
	void RemoveAllEntities();
	void UpdateEntity(fwEntity* entity, const fwRect& rect);
	u8 AppendToStraddlingPortalContainer(fwEntity* entity);
	void RemoveFromStraddlingPortalContainer(u8 index);

	void FlushUpdateList();
};

CompileTimeAssertSize( fwExteriorSceneGraphNode, 48, 80 );

} // namespace rage

#endif // !defined __INC_EXTERIORSCENEGRAPHNODE_H_
