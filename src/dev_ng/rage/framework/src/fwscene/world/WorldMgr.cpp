
/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    scene/world/worldMgr.cpp
// PURPOSE : Owns and manages the world representations used by the game. Acts
//			as sole gateway in/out of the world reps.
// AUTHOR :  John W.
// CREATED : 1/10/08
//
/////////////////////////////////////////////////////////////////////////////////

#include "fwscene/world/worldMgr.h"

// Rage includes
#include "profile/timebars.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "fwsys/timer.h"
#include "profile/group.h"
#include "profile/page.h"
#endif // __BANK

#include "fwutil/PtrList.h"


namespace rage {

#if __BANK

static int debugWorldRepRender = 0;

#endif

//////////////////////////////////////////////////////////////////////////
// new stats
#if WORLDMGR_STATS

	PF_PAGE(WorldMgr_AddRemovePage, "World AddRemoveUpdate");
	PF_GROUP(WorldMgr_StaticAddRemoveUpdate);
	PF_GROUP(WorldMgr_DynamicAddRemoveUpdate);
	PF_LINK(WorldMgr_AddRemovePage, WorldMgr_StaticAddRemoveUpdate);
	PF_LINK(WorldMgr_AddRemovePage, WorldMgr_DynamicAddRemoveUpdate);

	PF_TIMER(TotalStaticAddRemoveUpdate, WorldMgr_StaticAddRemoveUpdate);
	PF_TIMER(TotalStaticAdd, WorldMgr_StaticAddRemoveUpdate);
	PF_TIMER(TotalStaticRemove, WorldMgr_StaticAddRemoveUpdate);
	PF_TIMER(TotalStaticUpdate, WorldMgr_StaticAddRemoveUpdate);

	PF_TIMER(TotalDynamicAddRemoveUpdate, WorldMgr_DynamicAddRemoveUpdate);
	PF_TIMER(TotalDynamicAdd, WorldMgr_DynamicAddRemoveUpdate);
	PF_TIMER(TotalDynamicRemove, WorldMgr_DynamicAddRemoveUpdate);
	PF_TIMER(TotalDynamicUpdate, WorldMgr_DynamicAddRemoveUpdate);

#endif //WORLDMGR_STATS
//////////////////////////////////////////////////////////////////////////

fwWorldMgr	gWorldMgr;

fwWorldMgr::fwWorldMgr(){
}

fwWorldMgr::~fwWorldMgr(){
}


void fwWorldMgr::Initialise(fwWorldRepBase * defaultWorldRep){

	m_worldReps.DeleteAll();
	AddWorldRep(defaultWorldRep);


}

void fwWorldMgr::AddWorldRep(fwWorldRepBase * rep) {
	Assert(rep);
	if (rep){

		atSNode<fwWorldRepBase*> * pNode = rage_new atSNode<fwWorldRepBase*>;
		pNode->Data = rep;
		m_worldReps.Append(*pNode);
	}
}



void fwWorldMgr::Shutdown(){

	for(int i=0; i<m_worldReps.GetNumItems();i++){
		fwWorldRepBase* pRep = m_worldReps[i];
		if (pRep){
			delete pRep;
		}
	}

	m_worldReps.DeleteAll();

}

// --- Utils ----
//useful helper func - build a list from the for all entities intersecting result
bool fwWorldMgr::AppendToLinkedListCB(fwEntity* pEntity, void* pData){

	if (pData){
		fwPtrListSingleLink* pPedList = static_cast<fwPtrListSingleLink*>(pData);
		pPedList->Add(pEntity);
	}

	return(true);
}

// add/remove interface into the representations

void	fwWorldMgr::AddToWorld(fwEntity* pEntity, const fwRect& rect){
#if WORLDMGR_STATS
	if ( pEntity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		PF_START( TotalDynamicAddRemoveUpdate );
		PF_START( TotalDynamicAdd );
	}
	else
	{
		PF_START( TotalStaticAddRemoveUpdate );
		PF_START( TotalStaticAdd );
	}
#endif

	// route the entity to the appropriate world representation base on the type of the object
	u32 entityType = pEntity->GetType();

	if (AssertVerify(pEntity))
	{
		for(int i=0; i<m_worldReps.GetNumItems();i++)
		{
			fwWorldRepBase* pWorldRep = m_worldReps[i];
			if (pWorldRep)
			{
				if (pWorldRep->IsOperationHandled(OP_ADD_REMOVE))
				{
					if (pWorldRep->IsTypeHandled(entityType))
					{
						pWorldRep->AddToWorld(pEntity, rect);
					}
				}
			}
		}
	}
#if WORLDMGR_STATS
	if ( pEntity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		PF_STOP( TotalDynamicAddRemoveUpdate );
		PF_STOP( TotalDynamicAdd );
	}
	else
	{
		PF_STOP( TotalStaticAddRemoveUpdate );
		PF_STOP( TotalStaticAdd );
	}
#endif
}

void	fwWorldMgr::RemoveFromWorld(fwEntity* pEntity){

#if WORLDMGR_STATS
	if ( pEntity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		PF_START( TotalDynamicAddRemoveUpdate );
		PF_START( TotalDynamicRemove );
	}
	else
	{
		PF_START( TotalStaticAddRemoveUpdate );
		PF_START( TotalStaticRemove );
	}
#endif

	// route the entity to the appropriate world representation base on the type of the object
	u32 entityType = pEntity->GetType();

	if (AssertVerify(pEntity))
	{
		for(int i=0; i<m_worldReps.GetNumItems();i++)
		{
			fwWorldRepBase* pWorldRep = m_worldReps[i];
			if (pWorldRep)
			{
				if (pWorldRep->IsOperationHandled(OP_ADD_REMOVE))
				{
					if (pWorldRep->IsTypeHandled(entityType))
					{
						pWorldRep->RemoveFromWorld(pEntity);
					}
				}
			}
		}
	}
#if WORLDMGR_STATS
	if ( pEntity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		PF_STOP( TotalDynamicAddRemoveUpdate );
		PF_STOP( TotalDynamicRemove );
	}
	else
	{
		PF_STOP( TotalStaticAddRemoveUpdate );
		PF_STOP( TotalStaticRemove );
	}
#endif
}


void	fwWorldMgr::UpdateInWorld(fwEntity* pEntity, const fwRect& rect){

#if WORLDMGR_STATS
	if ( pEntity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		PF_START( TotalDynamicAddRemoveUpdate );
		PF_START( TotalDynamicUpdate );
	}
	else
	{
		PF_START( TotalStaticAddRemoveUpdate );
		PF_START( TotalStaticUpdate );
	}
#endif

	// route the entity to the appropriate world representation base on the type of the object
	u32 entityType = pEntity->GetType();

	if (AssertVerify(pEntity))
	{
		for(int i=0; i<m_worldReps.GetNumItems();i++)
		{
			fwWorldRepBase* pWorldRep = m_worldReps[i];
			if (pWorldRep)
			{
				if (pWorldRep->IsOperationHandled(OP_UPDATE))
				{
					if (pWorldRep->IsTypeHandled(entityType))
					{
						pWorldRep->UpdateInWorld(pEntity, rect);
					}
				}
			}
		}
	}
#if WORLDMGR_STATS
	if ( pEntity->IsBaseFlagSet( fwEntity::IS_DYNAMIC ) )
	{
		PF_STOP( TotalDynamicAddRemoveUpdate );
		PF_STOP( TotalDynamicUpdate );
	}
	else
	{
		PF_STOP( TotalStaticAddRemoveUpdate );
		PF_STOP( TotalStaticUpdate );
	}
#endif
}

// ----- Container add/remove -----

void	fwWorldMgr::AddSceneNodeToWorld(fwSceneGraphNode* pSceneNode){

	if (AssertVerify(pSceneNode))
	{
		for(int i=0; i<m_worldReps.GetNumItems();i++)
		{
			fwWorldRepBase* pWorldRep = m_worldReps[i];
			if (pWorldRep)
			{
				if (pWorldRep->IsOperationHandled(OP_ADD_REMOVE))
				{
					pWorldRep->AddSceneNodeToWorld(pSceneNode);
				}
			}
		}
	}
}

void	fwWorldMgr::RemoveSceneNodeFromWorld(fwSceneGraphNode* pSceneNode){

	if (AssertVerify(pSceneNode))
	{
		for(int i=0; i<m_worldReps.GetNumItems();i++)
		{
			fwWorldRepBase* pWorldRep = m_worldReps[i];
			if (pWorldRep)
			{
				if (pWorldRep->IsOperationHandled(OP_ADD_REMOVE))
				{
					pWorldRep->RemoveSceneNodeFromWorld(pSceneNode);
				}
			}
		}
	}
}

// ----- Search functions ----

void fwWorldMgr::ForAllEntitiesIntersecting(fwSearchVolume* pColVol, fwIntersectingCB cb, void* data, s32 typeFlags, s32 locationFlags, s32 lodFlags, s32 optionFlags, u32 module){

	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_SEARCH))
			{
				// type flags are identical to 
				if (pWorldRep->AreTypesHandled(typeFlags))
				{
					pWorldRep->ForAllEntitiesIntersecting(pColVol, cb, data, typeFlags, locationFlags, lodFlags, optionFlags, module);
				}
			}
		}
	}

}

//-------------------------------------------
// scan functions
//---------------------------------------------

// need this for the new representation - it builds an array of entities per task for scanning
void	fwWorldMgr::PreScan(void){

	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_PRESCAN))
			{
				pWorldRep->PreScan();
			}
		}
	}
}

void	fwWorldMgr::PostScan(void){

	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_POSTSCAN))
			{
				pWorldRep->PostScan();
			}
		}
	}
}


void fwWorldMgr::GetAll(fwPtrListSingleLink& Matches){
	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_MISC))
			{
				pWorldRep->GetAll(Matches);
			}
		}
	}
}

void fwWorldMgr::GetAll(fwEntity** entityArray, u32* entityCount){
	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_MISC))
			{
				pWorldRep->GetAll(entityArray, entityCount);
			}
		}
	}
}

//--------------------------------------------

#if __BANK

void updateWorldRepRenderCB(void*){

	for(int i=0; i<gWorldMgr.GetNumWorldReps();i++)
	{
		fwWorldRepBase* pWorldRep = gWorldMgr.GetWorldRep(i);
		if (pWorldRep)
		{
			u32 newOps = pWorldRep->GetOperations();
			newOps &= ~USAGE_MASK_SCAN;
			newOps &= ~USAGE_MASK_SEARCH;
			pWorldRep->SetOperations(newOps);

			if (i == debugWorldRepRender){
				pWorldRep->SetOperations(newOps | USAGE_MASK_SCAN | USAGE_MASK_SEARCH);
			}
		}
	}
}

//
// name:		AddBankWidgets
// description:	Create debug widgets for each world rep type
void fwWorldMgr::AddBankWidgets(bkBank* pBank)
{
	Assert(pBank);

	pBank->PushGroup("World representation");
	pBank->AddSlider("Current world rep", &debugWorldRepRender, 0, (m_worldReps.GetNumItems() - 1), 1, updateWorldRepRenderCB);

	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_DEBUG))
			{
				pWorldRep->AddBankWidgets(pBank);
			}
		}
	}

	pBank->PopGroup();
};

// name:		DrawDebug
// description:	Handles any debug UPDATE required by each world rep
void fwWorldMgr::UpdateDebug()
{
	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_DEBUG))
			{
				pWorldRep->UpdateDebug();
			}
		}
	}
}

//
// name:		DrawDebug
// description:	Handles any debug DRAWING required by each world rep
void fwWorldMgr::DrawDebug()
{
	for(int i=0; i<m_worldReps.GetNumItems();i++)
	{
		fwWorldRepBase* pWorldRep = m_worldReps[i];
		if (pWorldRep)
		{
			if (pWorldRep->IsOperationHandled(OP_DEBUG))
			{
				pWorldRep->DrawDebug();
			}
		}
	}
}

#endif // __BANK

} // namespace rage
