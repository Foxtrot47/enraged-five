
/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    scene/world/worldMgr.h
// PURPOSE : Owns and manages the world representations used by the game. Acts
//			 as sole gateway in/out of the world reps.
// AUTHOR :  John W.
// CREATED : 1/10/08
//
/////////////////////////////////////////////////////////////////////////////////


#ifndef INC_WORLDMGR_H_
#define INC_WORLDMGR_H_

// Rage headers
#include "atl/slist.h"
#include "entity/entity.h"
#include "fwscene/world/EntityContainer.h"

#if __BANK
#include "profile/element.h"
#include "profile/page.h"
#endif

// Game includes
#include "fwscene/world/WorldRepBase.h"


namespace rage {
	class fwRect;
	class fwPtrList;
	class fwSearchVolume;


#define WORLDMGR_STATS (__BANK & 1)

#if WORLDMGR_STATS
EXT_PF_PAGE(WorldMgr_AddRemovePage);
#endif

class fwWorldMgr {

public:
	fwWorldMgr();
	~fwWorldMgr();

	void Initialise(fwWorldRepBase * defaultWorldRep);
	void Shutdown(void);

	void AddWorldRep(fwWorldRepBase * defaultWorldRep);

	// --- add/remove interface into the representations --- 
	void	AddToWorld(fwEntity* pEntity, const fwRect&);
	void	RemoveFromWorld(fwEntity* pEntity);
	void	UpdateInWorld(fwEntity* pEntity, const fwRect& rect);

	void	AddSceneNodeToWorld(fwSceneGraphNode* pSceneNode);
	void	RemoveSceneNodeFromWorld(fwSceneGraphNode* pSceneNode);

	// --- search interface into the representations ---
	void ForAllEntitiesIntersecting(fwSearchVolume* pColVol, fwIntersectingCB cb, void* data, s32 typeFlags, s32 locationFlags, s32 lodFlags, s32 optionFlags, u32 module);


	// --- world scanning interface ---
	void	PreScan(void);
	void	PostScan(void);


	// -- miscellaneous functions which game required of old representation
	void GetAll(fwPtrListSingleLink& Matches);
	void GetAll(fwEntity** entityArray, u32* entityCount);

	int		GetNumWorldReps(void) { return(m_worldReps.GetNumItems()); }

	static bool AppendToLinkedListCB(fwEntity* pEntity, void* pData);
private:

	atSList <fwWorldRepBase*>	m_worldReps;
	// debug widgets
#if __BANK
public:
	void	AddBankWidgets(bkBank* pBank);
	void	UpdateDebug();
	void	DrawDebug();

	fwWorldRepBase*		GetWorldRep(int i) {return(m_worldReps[i]);}
#endif // __BANK
};

extern fwWorldMgr	gWorldMgr;

} // namespace rage

#endif //INC_WORLDMGR_H_

