#include "fwscene/world/InteriorLocation.h"

namespace rage {

fwInteriorLocation::fwInteriorLocation(const s32 interiorProxyIndex, const s32 roomIndex)
{
	if ( interiorProxyIndex == INTLOC_INVALID_INDEX || roomIndex == INTLOC_INVALID_INDEX )
		MakeInvalid();
	else
	{
		SetInteriorProxyIndex( interiorProxyIndex );
		as_interiorLocation.m_isPortalToExterior = 0;
		SetRoomIndex( roomIndex );
	}
}

void fwInteriorLocation::MakeInvalid()
{
	as_uint32 = 0;

	as_interiorLocation.m_interiorIndex = INTLOC_INVALID_INDEX;
	as_interiorLocation.m_roomOrPortalIndex = INTLOC_INVALID_INDEX;
}

bool fwInteriorLocation::IsValid() const
{
	if ( as_interiorLocation.m_interiorIndex == INTLOC_INVALID_INDEX )
		return false;

	if ( !as_interiorLocation.m_isAttachedToPortal )
		return as_interiorLocation.m_roomOrPortalIndex >= 0 && as_interiorLocation.m_roomOrPortalIndex < INTLOC_MAX_INTERIOR_ROOMS_COUNT;
	else
		return as_interiorLocation.m_roomOrPortalIndex >= 0 && as_interiorLocation.m_roomOrPortalIndex < INTLOC_MAX_INTERIOR_PORTALS_COUNT;
}

void fwInteriorLocation::SetRoomIndex(const s32 roomIndex)
{
	Assert( roomIndex != INTLOC_INVALID_INDEX && roomIndex < INTLOC_MAX_INTERIOR_ROOMS_COUNT );
	as_interiorLocation.m_isAttachedToPortal = 0;
	as_interiorLocation.m_roomOrPortalIndex = static_cast< s16 >( roomIndex );
}

s32 fwInteriorLocation::GetRoomIndex() const
{
	Assert( !as_interiorLocation.m_isAttachedToPortal );
	return static_cast< s32 >( as_interiorLocation.m_roomOrPortalIndex );
}

void fwInteriorLocation::SetPortalIndex(const s32 portalIndex)
{
	Assert( portalIndex != INTLOC_INVALID_INDEX && portalIndex < INTLOC_MAX_INTERIOR_PORTALS_COUNT );
	as_interiorLocation.m_isAttachedToPortal = 1;
	as_interiorLocation.m_roomOrPortalIndex = static_cast< s16 >( portalIndex );
}

s32 fwInteriorLocation::GetPortalIndex() const
{
	Assert( as_interiorLocation.m_isAttachedToPortal );
	return static_cast< s32 >( as_interiorLocation.m_roomOrPortalIndex );
}

} // namespace rage
