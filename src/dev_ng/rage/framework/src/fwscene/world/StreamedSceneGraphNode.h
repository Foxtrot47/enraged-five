#ifndef __INC_STREAMEDSCENEGRAPHNODE_H_
#define __INC_STREAMEDSCENEGRAPHNODE_H_

#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/world/EntityContainer.h"
#include "spatialdata/aabb.h"

namespace rage {

class fwStreamedSceneGraphNode : public fwSceneGraphNode
{
private:

	enum {
		// continues from flags in fwSceneGraphNode
		SCENE_NODE_FLAG_CONTAINS_HIGH_DETAILS	= 1 << 3
	};

	fwBaseEntityContainer*	m_firstContainer;
	spdAABB					m_aabb;

public:

	FW_REGISTER_CLASS_POOL( fwStreamedSceneGraphNode );

	fwStreamedSceneGraphNode(const bool containsHighDetails, const bool disabled);

	void FixupPointers(const fwPointerFixer& fixer)
	{
		fwSceneGraphNode::FixupPointers( fixer );
		fixer.Fix( m_firstContainer );
	}

	void SetBoundingBox(const spdAABB& aabb)						{ m_aabb = aabb; }
	const spdAABB& GetBoundingBox() const							{ return m_aabb; }

	bool GetContainsHighDetails() const								{ return GetFlagsRef().IsFlagSet( SCENE_NODE_FLAG_CONTAINS_HIGH_DETAILS ); }
	bool GetContainsLowDetails() const								{ return !GetFlagsRef().IsFlagSet( SCENE_NODE_FLAG_CONTAINS_HIGH_DETAILS ); }

	fwBaseEntityContainer* GetFirstContainer() const				{ return m_firstContainer; }
	fwBaseEntityContainer* GetLastStreamedContainer() const;
	void AddStreamedContainer(fwBaseEntityContainer* container);
	void RemoveStreamedContainer(fwBaseEntityContainer* container);
	void DestroyAllStreamedContainers();
};

CompileTimeAssertSize(fwStreamedSceneGraphNode, 48, 64 );

} // namespace rage

#endif // !defined __INC_STREAMEDSCENEGRAPHNODE_H_
