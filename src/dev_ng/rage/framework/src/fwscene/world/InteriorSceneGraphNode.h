#ifndef __INC_INTERIORSCENEGRAPHNODE_H__
#define __INC_INTERIORSCENEGRAPHNODE_H__

#include "fwmaths/PortalCorners.h"
#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/world/EntityContainer.h"
#include "fwscene/world/InteriorLocation.h"

namespace rage {

class fwInteriorSceneGraphNode : public fwSceneGraphNode
{
private:

	s16		m_interiorIndex;
	ATTR_UNUSED s16		m_padding;
	
public:

	FW_REGISTER_CLASS_POOL( fwInteriorSceneGraphNode );

	fwInteriorSceneGraphNode();

	s16 GetInteriorNodeIndex() const					{ return GetIndex() - fwSceneGraph::FIRST_INTERIOR_SCENE_NODE_INDEX; }

	void SetInteriorIndex(const s16 interiorIndex)		{ m_interiorIndex = interiorIndex; }
	s16 GetInteriorIndex() const						{ return m_interiorIndex; }
};

class fwRoomSceneGraphNode : public fwSceneGraphNode
{
private:

	fwEntityContainer*			m_container;
	fwInteriorSceneGraphNode*	m_ownerInteriorSceneNode;
	fwInteriorLocation			m_interiorLocation;
	s16							m_exteriorVisibilityDepth;
	bool						m_directionalLightEnabled : 1;
	bool						m_exteriorDisabled : 1;
	ATTR_UNUSED bool						m_boolPad : 6;
	ATTR_UNUSED u8							m_pad;
public:

	FW_REGISTER_CLASS_POOL( fwRoomSceneGraphNode );

	fwRoomSceneGraphNode(fwInteriorSceneGraphNode* ownerInteriorSceneNode);
	~fwRoomSceneGraphNode();

	void FixupPointers(const fwPointerFixer& fixer)
	{
		fwSceneGraphNode::FixupPointers( fixer );
		fixer.Fix( m_container );
		fixer.Fix( m_ownerInteriorSceneNode );
	}

	s16 GetRoomNodeIndex() const											{ return GetIndex() - fwSceneGraph::FIRST_ROOM_SCENE_NODE_INDEX; }

	fwInteriorSceneGraphNode* GetOwnerInteriorSceneNode() const				{ return m_ownerInteriorSceneNode; }

	fwEntityContainer* GetEntityContainer() const							{ return m_container; }

	void SetInteriorLocation(const fwInteriorLocation interiorLocation)		{ m_interiorLocation = interiorLocation; }
	fwInteriorLocation GetInteriorLocation() const							{ return m_interiorLocation; }

	void SetExteriorVisibilityDepth(int exteriorVisibilityDepth) { m_exteriorVisibilityDepth = (s16)exteriorVisibilityDepth; }
	int  GetExteriorVisibilityDepth() const { return (int)m_exteriorVisibilityDepth; }

	void  SetDirectionalLightEnabled(bool directionLightEnabled) { m_directionalLightEnabled = directionLightEnabled; }
	bool  GetDirectionalLightEnabled() const { return m_directionalLightEnabled; }

	void  SetExteriorDisabled(bool exteriorDisabled) { m_exteriorDisabled = exteriorDisabled; }
	bool  GetExteriorDisabled() const { return m_exteriorDisabled; }

	// Compatibility methods
	u32 GetEntityCount() const			{ return m_container->GetEntityCount(); }
	fwEntity* GetEntity(const int i)	{ return m_container->GetEntity(i); }
};

class fwPortalSceneGraphNode : public fwSceneGraphNode
{
public:

	enum
	{
		SCENE_NODE_MIRROR_TYPE_NONE = 0,
		SCENE_NODE_MIRROR_TYPE_MIRROR, // standard mirror (priority=0)
		SCENE_NODE_MIRROR_TYPE_MIRROR_PRIORITY_1,
		SCENE_NODE_MIRROR_TYPE_MIRROR_PRIORITY_2,
		SCENE_NODE_MIRROR_TYPE_MIRROR_PRIORITY_3,
		SCENE_NODE_MIRROR_TYPE_MIRROR_FLOOR,
		SCENE_NODE_MIRROR_TYPE_WATER_SURFACE,
		SCENE_NODE_MIRROR_TYPE_UNUSED,
		SCENE_NODE_MIRROR_TYPE_COUNT
	};

private:

	enum
	{
		// continues from flags in fwSceneGraphNode
		SCENE_NODE_FLAG_IS_ONE_WAY			= 1 << 3,
		SCENE_NODE_FLAG_RENDER_LODS_ONLY	= 1 << 4,
		SCENE_NODE_FLAG_MIRROR_SHIFT		= 5,
		SCENE_NODE_FLAG_MIRROR_MASK			= SCENE_NODE_MIRROR_TYPE_COUNT - 1,
	};

	enum
	{
		PORTAL_CORNER_INDEX_OPACITY	= 0, // portal corner which stores opacity in w
		PORTAL_CORNER_INDEX_FLAGS	= 1, // portal corner which stores flags in w
	};

	enum
	{
		PORTAL_SCENE_NODE_FLAG_LINK_PORTAL						= 1 << 0,
		PORTAL_SCENE_NODE_FLAG_MIRROR_CAN_SEE_DIRECTIONAL_LIGHT	= 1 << 1,
		PORTAL_SCENE_NODE_FLAG_MIRROR_CAN_SEE_EXTERIOR_VIEW		= 1 << 2,
		PORTAL_SCENE_NODE_FLAG_ALLOW_CLOSING					= 1 << 3 //	Currently only used in debug 
	};

	CompileTimeAssert((SCENE_NODE_MIRROR_TYPE_COUNT & (SCENE_NODE_MIRROR_TYPE_COUNT - 1)) == 0); // must be a power of 2
	CompileTimeAssert((SCENE_NODE_FLAG_MIRROR_MASK << SCENE_NODE_FLAG_MIRROR_SHIFT) <= 0xff); // must fit in fwFlags8

	fwInteriorLocation			m_interiorLocation;
	fwPortalCorners				m_corners;
	fwEntityContainer*			m_container;
	fwInteriorSceneGraphNode*	m_ownerInteriorSceneNode;
	fwSceneGraphNode*			m_negativePortalEnd;
	fwSceneGraphNode*			m_positivePortalEnd;

public:

	FW_REGISTER_CLASS_POOL( fwPortalSceneGraphNode );

	fwPortalSceneGraphNode(fwInteriorSceneGraphNode* ownerInteriorSceneNode);
	~fwPortalSceneGraphNode();

	void FixupPointers(const fwPointerFixer& fixer)
	{
		fwSceneGraphNode::FixupPointers( fixer );
		fixer.Fix( m_container );
		fixer.Fix( m_ownerInteriorSceneNode );
		fixer.Fix( m_negativePortalEnd );
		fixer.Fix( m_positivePortalEnd );
	}

	s16 GetPortalNodeIndex() const											{ return GetIndex() - fwSceneGraph::FIRST_PORTAL_SCENE_NODE_INDEX; }

	fwInteriorSceneGraphNode* GetOwnerInteriorSceneNode() const				{ return m_ownerInteriorSceneNode; }

	fwEntityContainer* GetEntityContainer() const							{ return m_container; }

	void SetInteriorLocation(const fwInteriorLocation interiorLocation)		{ m_interiorLocation = interiorLocation; }
	fwInteriorLocation GetInteriorLocation() const							{ return m_interiorLocation; }

	void SetCorners(const fwPortalCorners& corners)							{ m_corners = corners; }
	fwPortalCorners& GetCorners()											{ return m_corners; }
	const fwPortalCorners& GetCorners() const								{ return m_corners; }

	void SetNegativePortalEnd(fwSceneGraphNode* negativePortalEnd)			{ m_negativePortalEnd = negativePortalEnd; }
	fwSceneGraphNode* GetNegativePortalEnd() const							{ return m_negativePortalEnd; }

	void SetPositivePortalEnd(fwSceneGraphNode* positivePortalEnd)			{ m_positivePortalEnd = positivePortalEnd; }
	fwSceneGraphNode* GetPositivePortalEnd() const							{ return m_positivePortalEnd; }

	void  SetOpacity(float opacity)											{ m_corners.SetCornerW(PORTAL_CORNER_INDEX_OPACITY, opacity); }
	float GetOpacity() const												{ return m_corners.GetCornerW(PORTAL_CORNER_INDEX_OPACITY); }

	void SetMirrorType(u8 mirrorType)										{ GetFlagsRef().SetAllFlags((GetFlagsRef().GetAllFlags() & ~(SCENE_NODE_FLAG_MIRROR_MASK << SCENE_NODE_FLAG_MIRROR_SHIFT)) | (u8)mirrorType << SCENE_NODE_FLAG_MIRROR_SHIFT); }
	u8   GetMirrorType() const												{ return (GetFlagsRef().GetAllFlags() >> SCENE_NODE_FLAG_MIRROR_SHIFT) & SCENE_NODE_FLAG_MIRROR_MASK; }

	bool GetIsMirrorTypeIsStandardMirror() const							{ return GetMirrorType() >= SCENE_NODE_MIRROR_TYPE_MIRROR && GetMirrorType() <= SCENE_NODE_MIRROR_TYPE_MIRROR_PRIORITY_3; }
	bool GetIsMirror() const												{ return GetMirrorType() >= SCENE_NODE_MIRROR_TYPE_MIRROR; }
	bool GetIsMirrorFloor() const											{ return GetMirrorType() >= SCENE_NODE_MIRROR_TYPE_MIRROR_FLOOR; }
	bool GetIsWaterSurface() const											{ return GetMirrorType() >= SCENE_NODE_MIRROR_TYPE_WATER_SURFACE; }

	void SetMirrorPriority(u8 priority)										{ Assert(GetIsMirrorTypeIsStandardMirror() && priority < 4); SetMirrorType(SCENE_NODE_MIRROR_TYPE_MIRROR + priority); }
	u8   GetMirrorPriority() const											{ Assert(GetIsMirrorTypeIsStandardMirror()); return GetMirrorType() - SCENE_NODE_MIRROR_TYPE_MIRROR; }

	void SetMirrorCanSeeDirectionalLight(const bool canSee)					{ Assert(GetIsMirror()); m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).ChangeFlag( PORTAL_SCENE_NODE_FLAG_MIRROR_CAN_SEE_DIRECTIONAL_LIGHT, canSee ); }
	bool GetMirrorCanSeeDirectionalLight() const							{ Assert(GetIsMirror()); return m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).IsFlagSet( PORTAL_SCENE_NODE_FLAG_MIRROR_CAN_SEE_DIRECTIONAL_LIGHT ); }

	void SetMirrorCanSeeExteriorView(const bool canSee)						{ Assert(GetIsMirror()); m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).ChangeFlag( PORTAL_SCENE_NODE_FLAG_MIRROR_CAN_SEE_EXTERIOR_VIEW, canSee ); }
	bool GetMirrorCanSeeExteriorView() const								{ Assert(GetIsMirror()); return m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).IsFlagSet( PORTAL_SCENE_NODE_FLAG_MIRROR_CAN_SEE_EXTERIOR_VIEW ); }

	void SetIsOneWay(const bool isOneWay)									{ GetFlagsRef().ChangeFlag( SCENE_NODE_FLAG_IS_ONE_WAY, isOneWay ); }
	bool GetIsOneWay() const												{ return GetFlagsRef().IsFlagSet( SCENE_NODE_FLAG_IS_ONE_WAY ); }

	void SetIsLinkPortal(const bool isLink)									{ m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).ChangeFlag( PORTAL_SCENE_NODE_FLAG_LINK_PORTAL, isLink ); }
	bool GetIsLinkPortal() const											{ return m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).IsFlagSet( PORTAL_SCENE_NODE_FLAG_LINK_PORTAL ); }

	void SetRenderLodsOnly(const bool renderLodsOnly)						{ GetFlagsRef().ChangeFlag( SCENE_NODE_FLAG_RENDER_LODS_ONLY, renderLodsOnly ); }
	bool GetRenderLodsOnly() const											{ return GetFlagsRef().IsFlagSet( SCENE_NODE_FLAG_RENDER_LODS_ONLY ); }

	void SetAllowClosing(const bool allowClosing)							{ m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).ChangeFlag( PORTAL_SCENE_NODE_FLAG_ALLOW_CLOSING, allowClosing ); }
	bool GetAllowClosing() const											{ return m_corners.GetCornerWFlagsRef(PORTAL_CORNER_INDEX_FLAGS).IsFlagSet( PORTAL_SCENE_NODE_FLAG_ALLOW_CLOSING ); }

	// Compatibility methods
	u32 GetEntityCount() const												{ return m_container->GetEntityCount(); }
	fwEntity* GetEntity(const int i)										{ return m_container->GetEntity(i); }
	const fwEntity* GetEntity(const int i) const							{ return m_container->GetEntity(i); }
};

CompileTimeAssertSize( fwInteriorSceneGraphNode, 16, RSG_ORBIS? 24 : 32 );
CompileTimeAssertSize( fwRoomSceneGraphNode, 28, 48 );
CompileTimeAssertSize( fwPortalSceneGraphNode, 96, 128 );

} // namespace rage

#endif // !defined __INC_INTERIORSCENEGRAPHNODE_H__
