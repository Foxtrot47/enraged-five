#ifndef __INC_WORLDREPMULTI_H_
#define __INC_WORLDREPMULTI_H_

#include "fwscene/world/WorldRepBase.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#endif

namespace rage {

class fwWorldRepMulti : public fwWorldRepBase
{
	static fwExteriorSceneGraphNode*	m_sceneGraphRoot;

public:

	fwWorldRepMulti();
	~fwWorldRepMulti();

	static fwSceneGraphNode* GetSceneGraphRoot()					{ return m_sceneGraphRoot; }

	void AddToWorld(fwEntity* entity, const fwRect& rect);
	void RemoveFromWorld(fwEntity* entity);
	void UpdateInWorld(fwEntity* entity, const fwRect& rect);

	void AddSceneNodeToWorld(fwSceneGraphNode* sceneNode);
	void RemoveSceneNodeFromWorld(fwSceneGraphNode* sceneNode);

	void ForAllEntitiesIntersecting(fwSearchVolume* collisionVoluem, fwIntersectingCB callback, void* data, s32 typeFlags, s32 locationFlags, s32 lodFlags, s32 optionFlags, u32 module);

	void PreScan();
	void PostScan();

	void GetAll(fwPtrListSingleLink& matches);
	void GetAll(fwEntity** entityArray, u32* entityCount);

private:
	bool m_bNewDefrag;

#if __BANK

public:
	void AddBankWidgets(bkBank* pBank)	{
		pBank->AddToggle("Use new defrag", &m_bNewDefrag);
		pBank->AddToggle("Slow down defrag", &m_bSlowDefrag);
	}
	void UpdateDebug()				{ }
	void DrawDebug()				{ }

private:
	bool m_bSlowDefrag;

#endif // __BANK

};

} // namespace rage

#endif // !defined __INC_WORLDREPMULTI_H_
