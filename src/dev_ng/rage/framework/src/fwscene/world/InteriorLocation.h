#ifndef _INC_INTLOC_H_
#define _INC_INTLOC_H_

namespace rage {

enum
{
	INTLOC_INVALID_INDEX				= -1,
	INTLOC_ROOMINDEX_LIMBO				= 0,
	INTLOC_MAX_INTERIOR_INDEX			= 1200,
	INTLOC_MAX_INTERIOR_ROOMS_COUNT		= 32,
	INTLOC_MAX_INTERIOR_PORTALS_COUNT	= 256,
	INTLOC_MAX_ENTITIES_IN_ROOM_0		= 12
};

union fwInteriorLocation
{
private:

	u32		as_uint32;

	struct
	{
		s16		m_interiorIndex;
		u16		m_isAttachedToPortal	: 1;
		u16		m_isPortalToExterior	: 1;
		s16		m_roomOrPortalIndex		: 14;
	}	as_interiorLocation;

public:

	fwInteriorLocation()
		: as_uint32(0)
	{
		as_interiorLocation.m_interiorIndex = INTLOC_INVALID_INDEX;
		as_interiorLocation.m_roomOrPortalIndex = INTLOC_INVALID_INDEX;
	}

	fwInteriorLocation(const s32 interiorIndex, const s32 roomIndex);

	void SetAsUint32(const u32 value)				{ as_uint32 = value; }
	u32 GetAsUint32() const							{ return as_uint32; }

	void MakeInvalid();
	bool IsValid() const;

	bool IsSameLocation(const fwInteriorLocation loc) const		{ return as_uint32 == loc.as_uint32; }
	bool operator==(const fwInteriorLocation& loc) const		{ return as_uint32 == loc.as_uint32; }
	
	void SetInteriorProxyIndex(const s32 index)			{ Assert( index < INTLOC_MAX_INTERIOR_INDEX ); as_interiorLocation.m_interiorIndex = static_cast< s16 >( index ); }
	s32 GetInteriorProxyIndex() const					{ return static_cast< s32 >( as_interiorLocation.m_interiorIndex ); }

	void SetRoomIndex(const s32 roomIndex);
	s32 GetRoomIndex() const;

	void SetPortalIndex(const s32 portalIndex);
	s32 GetPortalIndex() const;

	bool IsAttachedToRoom() const					{ return !as_interiorLocation.m_isAttachedToPortal && as_interiorLocation.m_roomOrPortalIndex != INTLOC_INVALID_INDEX; }
	bool IsAttachedToPortal() const					{ return as_interiorLocation.m_isAttachedToPortal && as_interiorLocation.m_roomOrPortalIndex != INTLOC_INVALID_INDEX; }

	void SetPortalToExterior(const bool value)		{ Assert( as_interiorLocation.m_isAttachedToPortal ); as_interiorLocation.m_isPortalToExterior = value; }
	bool IsPortalToExterior() const					{ Assert( as_interiorLocation.m_isAttachedToPortal ); return as_interiorLocation.m_isPortalToExterior; }

	static bool IsValidRoomIndex(const s32 roomIndex) { return (roomIndex >= INTLOC_INVALID_INDEX && roomIndex < INTLOC_MAX_INTERIOR_ROOMS_COUNT);}
};

CompileTimeAssert( sizeof(fwInteriorLocation) == 4 );

} // namespace rage

#endif // !defined _INC_INTLOC_H_
