#include "fwscene/world/WorldRepMulti.h"
#include "fwscene/search/Search.h"
#include "entity/entity.h"

#if __BANK
#include "fwsys/timer.h"
#endif

namespace rage {

fwExteriorSceneGraphNode*	fwWorldRepMulti::m_sceneGraphRoot;

fwWorldRepMulti::fwWorldRepMulti()
{
	m_bNewDefrag = true;

#if __BANK
	m_bSlowDefrag = false;
#endif	//__BANK

	fwEntityContainer::ResetEntityDescPool();
	m_sceneGraphRoot = rage_new fwExteriorSceneGraphNode;
}

fwWorldRepMulti::~fwWorldRepMulti() {
	delete m_sceneGraphRoot;
}

void fwWorldRepMulti::AddToWorld(fwEntity* entity, const fwRect& rect) {
	m_sceneGraphRoot->AppendEntity( entity, rect );
}

void fwWorldRepMulti::RemoveFromWorld(fwEntity* entity) {
	m_sceneGraphRoot->RemoveEntity( entity );
}

void fwWorldRepMulti::UpdateInWorld(fwEntity* entity, const fwRect& rect)
{
	if ( entity->GetOwnerEntityContainer()->GetOwnerSceneNode()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR )
		m_sceneGraphRoot->UpdateEntity( entity, rect );
	else
		entity->GetOwnerEntityContainer()->UpdateEntityDescriptorInNode( entity );
}

void fwWorldRepMulti::AddSceneNodeToWorld(fwSceneGraphNode* sceneNode) {
	m_sceneGraphRoot->AddChild( sceneNode );
}

void fwWorldRepMulti::RemoveSceneNodeFromWorld(fwSceneGraphNode* sceneNode) {
	m_sceneGraphRoot->RemoveChild( sceneNode );
}

void fwWorldRepMulti::ForAllEntitiesIntersecting(fwSearchVolume* collisionVolume, fwIntersectingCB callback, void* data, s32 typeFlags, s32 locationFlags, s32 lodFlags, s32 optionFlags, u32 module)
{
	m_sceneGraphRoot->FlushUpdateList();
	fwSearch::ForAllEntitiesIntersecting( m_sceneGraphRoot, collisionVolume, callback, data, typeFlags, locationFlags, lodFlags, optionFlags, module );
}

void fwWorldRepMulti::PreScan(void){

#if __BANK
	if (!m_bSlowDefrag || !(fwTimer::GetSystemFrameCount()%16))
#endif
	{
		fwEntityContainer::DefragEntityDescPool(m_bNewDefrag, false);
	}

	m_sceneGraphRoot->FlushUpdateList();
}

void fwWorldRepMulti::PostScan(void) {
}

/*
WARNING: the two following methods suck balls very hard, since they only return dynamic entities and MLOs,
that is quite different from what the name "GetAll" suggests. However, this happens to be what the
calling code needs at the moment, so "it works" (tm). We will remove these methods altogether shortly.
*/

void fwWorldRepMulti::GetAll(fwPtrListSingleLink& matches)
{
	for (u32 i = 0; i < m_sceneGraphRoot->GetStaticsContainer()->GetEntityCount(); ++i)
		matches.Add( m_sceneGraphRoot->GetStaticsContainer()->GetEntity(i) );

	{
		fwEntityDesc*	entityDescs = m_sceneGraphRoot->GetDynamicsContainer()->GetEntityDescArray();
		const int		entityDescCount = m_sceneGraphRoot->GetDynamicsContainer()->GetStorageSize();

		for (int i = 0; i < entityDescCount; ++i)
			if ( entityDescs[i].GetEntity() != NULL )
				matches.Add( entityDescs[i].GetEntity() );
	}
}

void fwWorldRepMulti::GetAll(fwEntity** entityArray, u32* entityCount)
{
	*entityCount = 0;

	for (u32 i = 0; i < m_sceneGraphRoot->GetStaticsContainer()->GetEntityCount(); ++i)
		entityArray[ (*entityCount)++ ] = m_sceneGraphRoot->GetStaticsContainer()->GetEntity(i);

	{
		fwEntityDesc*	entityDescs = m_sceneGraphRoot->GetDynamicsContainer()->GetEntityDescArray();
		const int		entityDescCount = m_sceneGraphRoot->GetDynamicsContainer()->GetStorageSize();

		for (int i = 0; i < entityDescCount; ++i)
			if ( entityDescs[i].GetEntity() != NULL )
				entityArray[ (*entityCount)++ ] = entityDescs[i].GetEntity();
	}
}

} // namespace rage
