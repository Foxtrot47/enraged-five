#include "fwscene/world/WorldLimits.h"

namespace rage {

spdAABB g_WorldLimits_MapDataExtentsAABB;
spdAABB g_WorldLimits_AABB(Vec3V(WORLDLIMITS_XMIN, WORLDLIMITS_YMIN, WORLDLIMITS_ZMIN), Vec3V(WORLDLIMITS_XMAX, WORLDLIMITS_YMAX, WORLDLIMITS_ZMAX));

} // namespace rage