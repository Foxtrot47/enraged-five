#ifndef _INC_ENTITYCONTAINERVISITOR_H_
#define _INC_ENTITYCONTAINERVISITOR_H_

#include "fwscene/world/SceneGraphNode.h"

namespace rage {

/*
PURPOSE:

This is an helper class to traverse a container hierarchy in a depth-first fashion.

The implementation is stack-based, and allows the user to store an arbitrary u32 "tag" for each entry
in the stack.
*/

class fwSceneGraphVisitor
{
private:

	enum {
		MAX_STACK_SIZE	= 24
	};

	s16						m_top;
	fwSceneGraphNode*		m_stack[ MAX_STACK_SIZE ];
	u32						m_tagStack[ MAX_STACK_SIZE ];
	bool					m_childrenVisited[ MAX_STACK_SIZE ];
	
public:

	// PURPOSE:	Starts the visit of a container hierarchy
	// PARAMS:	root - the root container
	fwSceneGraphVisitor(fwSceneGraphNode* root);

	// PURPOSE:	Goes to the next container
	void Next();

	// PURPOSE: Gets the current entity container
	fwSceneGraphNode* GetCurrent() const								{ return m_stack[ m_top ]; }

	// PURPOSE: Returns true if we're at the end of the visit.
	bool AtEnd() const													{ return m_top < 0; }

	// PURPOSE:	Gets the current stack size of the visit.
	s16 GetStackSize() const											{ return m_top + 1; }
	
	// PURPOSE:	Gets the container at a specified position of the visit stack.
	fwSceneGraphNode* GetStackElement(const s16 position) const;
	
	// PURPOSE:	Avoid visiting the children of the container currently being visited.
	void SkipChildren()													{ m_childrenVisited[ m_top ] = true; }
	
	// PURPOSE:	Sets the tag of the container currently being visited.
	void SetCurrentTag(const u32 tag)									{ m_tagStack[ m_top ] = tag; }

	// PURPOSE:	Gets the tag of the current container.
	u32 GetCurrentTag() const											{ return m_tagStack[ m_top ]; }

	// PURPOSE: Gets the tag of the parent of the container currently being visited. Result is undefined if
	//			currently visiting the root of the hierarchy.
	u32 GetParentTag() const											{ return m_tagStack[ m_top - 1 ]; }
};

} // namespace rage

#endif // !defined _INC_ENTITYCONTAINERVISITOR_H_
