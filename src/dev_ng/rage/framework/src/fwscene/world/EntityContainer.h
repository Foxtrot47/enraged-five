#ifndef _INC_ENTITYCONTAINER_H_
#define _INC_ENTITYCONTAINER_H_

#include "fwscene/world/SceneGraph.h"
#include "fwscene/world/EntityDesc.h"
#include "fwtl/DefragPool.h" 

namespace rage {

class fwSceneGraphNode;
class fwEntityContainer;
class fwFixedEntityContainer;
class fwSoAEntityContainer;

class fwBaseEntityContainer
{
public:

	typedef void (*UpdateEntityDescFunc)(fwEntity* entity, fwEntityDesc* entityDesc);

protected:

	enum
	{
		DEFAULT_ENTITY_DESC_POOL_SIZE			= 64000,
		ENTITY_DESC_POOL_ALIGNMENT				= 16,
		DEFAULT_ENTITY_CONTAINER_STORAGE_STEP	= 1,
	};

	enum ContainerType
	{
		CONTAINER_TYPE_DEFAULT,
		CONTAINER_TYPE_FIXED,
		CONTAINER_TYPE_SOA
	};

	static fwDescPool*				ms_entityDescPool;
	static UpdateEntityDescFunc		ms_updateEntityDescFunc;

	fwDescPoolAlloc*				m_dataHandle;
	fwEntityDesc*					m_cachedEntityDescArray;
	fwBaseEntityContainer*			m_next;
	fwSceneGraphNode*				m_ownerSceneNode;
	u16								m_entityCount			: 14;
	u16								m_containerType			: 2;
	u16								m_cachedStorageSize;
	
	//==================== Data handle functions ====================//

	bool HasDataHandle() const {
		return m_dataHandle != NULL;
	}

	void CreateDataHandle(const u32 amount);
	void DeleteDataHandle();
	DefragPool_eResizeResult ResizeDataHandle(const u32 amount);
	DefragPool_eResizeResult GrowDataHandle(const u32 amount = 1);
	void UpdateDataHandleCache();
	static void UpdateDataHandleCacheForAll();

	//==================== Constructor ====================//

	fwBaseEntityContainer(const ContainerType type, fwSceneGraphNode* ownerSceneNode);

public:

	//==================== Safe delete method ====================//

	static void Delete(fwBaseEntityContainer* container);

	//==================== Defrag pool methods ====================//

	static void InitEntityDescPool(const u32 poolSize = DEFAULT_ENTITY_DESC_POOL_SIZE) {
		ms_entityDescPool = rage_new fwDescPool( poolSize, ENTITY_DESC_POOL_ALIGNMENT, MEMBUCKET_WORLD );
	}

	static void ShutdownEntityDescPool()
	{
		SceneGraphLockedFatalAssert();
		delete ms_entityDescPool;
		ms_entityDescPool = NULL;
	}

	static void ResetEntityDescPool()
	{
		SceneGraphLockedFatalAssert();
		ms_entityDescPool->Reset();
	}

	static void DefragEntityDescPool(bool bUseNewDefrag, bool bFull)
	{
		SceneGraphLockedFatalAssert();

		if (bFull)
		{
			ms_entityDescPool->DefragFull();
		}
		else
		{
			if (bUseNewDefrag)
			{
				ms_entityDescPool->DefragPartialNew();
				ms_entityDescPool->DefragPartialNew();
			}
			else
			{
				ms_entityDescPool->DefragPartial();
			}
		}
		
		fwBaseEntityContainer::UpdateDataHandleCacheForAll();
	}

	//==================== UpdateEntityDesc func setter and getter ====================//

	static void SetUpdateEntityDescFunc(UpdateEntityDescFunc func) {
		ms_updateEntityDescFunc = func;
	}

	static UpdateEntityDescFunc GetUpdateEntityDescFunc() {
		return ms_updateEntityDescFunc;
	}

	//==================== Type method ====================//

	bool IsTypeDefault() const {
		return m_containerType == CONTAINER_TYPE_DEFAULT;
	}

	bool IsTypeFixed() const {
		return m_containerType == CONTAINER_TYPE_FIXED;
	}

	bool IsTypeSoA() const {
		return m_containerType == CONTAINER_TYPE_SOA;
	}

	fwEntityContainer* AsTypeDefault();
	fwFixedEntityContainer* AsTypeFixed();
	fwSoAEntityContainer* AsTypeSoA();

	//==================== Pointer fixup method ====================//

	void FixupPointers(const fwPointerFixer& fixer)
	{
		fixer.Fix( m_next );
		fixer.Fix( m_ownerSceneNode );
	}

	//==================== Next and owner scene node accessors ====================//

	void SetNext(fwBaseEntityContainer* next) {
		m_next = next;
	}

	fwBaseEntityContainer* GetNext() const {
		return m_next;
	}

	fwSceneGraphNode* GetOwnerSceneNode() const {
		return m_ownerSceneNode;
	}

	//==================== Other getters ====================//

	u16 GetStorageSize() const {
		return m_cachedStorageSize;
	}

	u32 GetEntityCount() const {
		return m_entityCount;
	}

	fwEntityDesc* GetEntityDescArray() const {
		return m_cachedEntityDescArray;
	}

	//==================== Generic entity add / remove methods ====================//

	void RemoveAllEntities();

	void UpdateEntityDescriptorInNode(fwEntity* entity);
	u32 GetEntityDescIndex(const fwEntity* pEntity);
	void RemoveEntity(fwEntity* pEntity);

	enum { INVALID_ENTITY_INDEX = 0xFFFF };

	void GetApproxBoundingBox(u32 index, spdAABB &aSpdAABB);
	void GetApproxBoundingSphere(u32 index, spdSphere &aSpdSphere);
	void SetLodDistCached(u32 index, u32 dist);

#if __BANK

	//==================== Debug methods ====================//

	static fwDescPool* GetDefragPool() {
		return ms_entityDescPool;
	}

	fwDescPoolAlloc* GetDataHandle() const {
		return m_dataHandle;
	}

	fwEntityDesc* GetEntityDescForEntity(const fwEntity* entity);

#endif
};

class fwEntityContainer : public fwBaseEntityContainer
{
private:

	u16		m_storageStep;
	ATTR_UNUSED u8		m_padding[2];

public:

	AT_REGISTER_CLASS_ITPOOL( fwEntityContainer );

	fwEntityContainer(fwSceneGraphNode* ownerSceneNode);

	void SetStorageStep(const u16 storageStep)		{ m_storageStep = storageStep; }
	u16 GetStorageStep() const						{ return m_storageStep; }
	void Preallocate(const u16 count);

	fwEntityDesc* AppendEntity(fwEntity* entity);
	void RemoveEntityInternal(fwEntity* entity);
	void PopEntity();
	void RemoveAllEntities();

	fwEntityDesc* GetEntityDesc(const u32 index)
	{
		SceneGraphAssert( index < m_entityCount );
		return ( index < m_entityCount ) ? ( m_cachedEntityDescArray + index ) : NULL;
	}

	fwEntity* GetEntity(const u32 index)
	{
		fwEntityDesc *desc = GetEntityDesc( index );
		return ( desc ) ? static_cast< fwEntity* >( desc->GetEntity() ) : NULL;
	}

	void GetEntityArray(atArray< fwEntity* >& entityArray);
	BANK_ONLY( fwEntityDesc* GetEntityDescForEntity(const fwEntity* entity); )

	void UpdateEntityDescriptorInNodeInternal(fwEntity* entity);
};

class fwFixedEntityContainer : public fwBaseEntityContainer
{
protected:

	enum {
		INVALID_NEXT_INDEX	= 0xffff
	};

	struct Entry : private fwEntityDesc
	{
		void Invalidate()						{ SetEntity( NULL ); }
		void SetNextFreeIndex(const u16 index)	{ SetLodDistCached( index ); }
		u16 GetNextFreeIndex() const			{ return static_cast< u16 >( GetLodDistCached() ); }
	};

	u16		m_firstFreeIndex;
	u8		m_padding[2];

	void Reset();

public:

	AT_REGISTER_CLASS_ITPOOL( fwFixedEntityContainer );

	fwFixedEntityContainer(fwSceneGraphNode* ownerSceneNode, const int size);
	~fwFixedEntityContainer();

	fwEntityDesc* AppendEntity(fwEntity* entity);
	void RemoveEntityFromDescriptor(fwEntityDesc* entityDesc);
	void RemoveAllEntities();

	BANK_ONLY( fwEntityDesc* GetEntityDescForEntity(const fwEntity* entity); )
};

class fwStraddlingPortalsContainer : public fwFixedEntityContainer
{
public:
	fwStraddlingPortalsContainer(fwSceneGraphNode* ownerSceneNode, const u8 size);
	u8 AppendEntity(fwEntity* entity);
	void RemoveEntityFromDescriptorIndex(u8 index);
	fwEntityDesc *GetEntityDesc(u8 index);
};

struct fwEntityDescSoA
{
	int					m_entityCount;
	int					m_blockCount;

	fwEntity**			m_entityPointers;
	fwFlags16*			m_phaseVisibilityMasks;
	u16*				m_visibilityTypes;
	u16*				m_lodDistBackups;
	fwEntityDescFlags*	m_flags;

	Vec4V*				m_x;
	Vec4V*				m_y;
	Vec4V*				m_z;
	Vec4V*				m_extentXY;
	Vec4V*				m_extentZAndRadius;
	Vec4V*				m_vecToLodXY;
	Vec4V*				m_vecToLodZandFlags2;
};

#define SIZEOF_ENTITY_DESC_SOA (__64BIT ? 44 : 40)

class fwSoAEntityContainer : public fwBaseEntityContainer
{
public:

	AT_REGISTER_CLASS_ITPOOL( fwSoAEntityContainer );

	fwSoAEntityContainer(fwSceneGraphNode* ownerSceneNode, fwEntity** entities, int containerSize, int entityCount, int &numberEntitiesProcessed);
	~fwSoAEntityContainer();
	
	static fwEntityDescSoA InterpretAsEntityDescSoa(fwEntityDesc* entityDescs, const int entityCount);
	static void SetEntityDescBlock(const fwEntityDescSoA& soa, const int b, const fwEntityDesc& entityDescA, const fwEntityDesc& entityDescB, const fwEntityDesc& entityDescC, const fwEntityDesc& entityDescD);
	static void GetEntityDescBlock(const fwEntityDescSoA& soa, const int b, fwEntityDesc& entityDescA, fwEntityDesc& entityDescB, fwEntityDesc& entityDescC, fwEntityDesc& entityDescD);
	static void UpdateSingleSOAEntityDesc(fwEntityDescSoA *pSoa, u32 index, const fwEntityDesc& entityDesc);

	static void GetApproxBoundingBox(const fwEntityDescSoA& soa, u32 index, spdAABB& aabb);
	static void GetApproxBoundingSphere(const fwEntityDescSoA& soa, u32 index, spdSphere& sphere);
	static void SetLodDistCached(const fwEntityDescSoA& soa, u32 index, u32 lodDist);

	void UpdateEntityDescriptorInNodeInternal(fwEntity* entity);
	void RemoveAllEntities();
	void RemoveEntityInternal(fwEntity* entity);
	
	BANK_ONLY( bool ContainsEntityDesc(fwEntity *entity);)

	BANK_ONLY( fwEntityDesc* GetEntityDescForEntity(const fwEntity* entity); )
};

inline fwEntityContainer* fwBaseEntityContainer::AsTypeDefault()
{
	SceneGraphAssert( !this || IsTypeDefault() );
	return static_cast< fwEntityContainer* >( this );
}

inline fwFixedEntityContainer* fwBaseEntityContainer::AsTypeFixed()
{
	SceneGraphAssert( !this || IsTypeFixed() );
	return static_cast< fwFixedEntityContainer* >( this );
}

inline fwSoAEntityContainer* fwBaseEntityContainer::AsTypeSoA()
{
	SceneGraphAssert( !this || IsTypeSoA() );
	return static_cast< fwSoAEntityContainer* >( this );
}

CompileTimeAssertSize(fwBaseEntityContainer, 20, 40);
CompileTimeAssertSize(fwEntityContainer, 24, RSG_ORBIS? 40 : 48);
CompileTimeAssertSize(fwFixedEntityContainer, 24, RSG_ORBIS? 40 : 48);
CompileTimeAssertSize(fwSoAEntityContainer, 20, 40);

CompileTimeAssertSize(fwEntityDescSoA, 56, 104);  // If you get this error you must update #define SIZEOF_ENTITY_DESC_SOA to the new size

namespace SoAUtils {
static __forceinline void Float16Unpack(Vec4V_In packed, Vec4V_InOut unpackedA, Vec4V_InOut unpackedB)
{
	Vec4V			unpacked01, unpacked23;
	Float16Vec8Unpack( unpacked01, unpacked23, packed );

	unpackedA = Vec4V( unpacked01.GetX(), unpacked01.GetZ(), unpacked23.GetX(), unpacked23.GetZ() );
	unpackedB = Vec4V( unpacked01.GetY(), unpacked01.GetW(), unpacked23.GetY(), unpacked23.GetW() );
}
}

} // namespace rage

#endif // !defined _INC_ENTITYCONTAINER_H_
