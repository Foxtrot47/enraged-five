#ifndef FWSCENE_STORES_STATICBOUNDSSTORE_H
#define FWSCENE_STORES_STATICBOUNDSSTORE_H

#include "atl/map.h"
#include "atl/slist.h"
#include "fwscene/stores/boxstreamer.h"
#include "fwtl/assetstore.h"
#include "phbound/bound.h"
#include "physics/inst.h"

// --- Defines ------------------------------------------------------------------

#define MOVER_BOUNDS_PRESTREAM				(150.0f)
#define WEAPON_BOUNDS_PRESTREAM				(80.0f)
#define MATERIAL_BOUNDS_PRESTREAM			(50.0f)

#define MAX_INSTANCED_STATIC_BOUNDS			(1800)

#define InteriorProxyIndex					s16

namespace rage {

// PURPOSE: Specialized hash function functor for strLocalIndex keys.
template <>
struct atMapHashFn<strLocalIndex>
{
	u64 operator ()(strLocalIndex key) const { return key.Get(); }
};

//
// name:		BoundsDef
// description:	bounds definition
struct fwBoundData
{
	phInst** m_physicsInstArray;
	u16	m_physInstCount;
	u16 m_defSlot;

	fwBoundData()
	{
		m_physicsInstArray = NULL;
		m_physInstCount = 0;
		m_defSlot = static_cast<u16>(~0);
	}

	fwBoundData(const u16 defSlot) : m_defSlot(defSlot)
	{
		m_physicsInstArray = NULL;
		m_physInstCount = 0;
	}

	bool operator==(const fwBoundData& data) { return m_defSlot == data.m_defSlot; }
};

class fwBoundDef
{
public:
	typedef strStreamingObjectName strObjectNameType;

	void Init(const strStreamingObjectName name) 
	{
		m_pObject = NULL;
#if DETAILED_MODULE_REF_COUNT
		m_refsUnified = 0;
#else // DETAILED_MODULE_REF_COUNT
		m_refCount = 0;
#endif //  DETAILED_MODULE_REF_COUNT
		memset( &m_name, 0, sizeof(strStreamingObjectName) );
		m_name = name;

		m_bIsDummy = 0;
		m_bIsDependency = 0;
		m_bHasRiverBounds = 0;
	}

	bool IsDummy()	const				{ return(m_bIsDummy); }
	bool IsDependency() const			{ return(m_bIsDependency); }
	bool HasRiverBounds() const			{ return m_bHasRiverBounds; }

	phBound* m_pObject;
	strStreamingObjectName m_name;

#if DETAILED_MODULE_REF_COUNT
	union {
		struct {
			u64 m_refCounts0 : REF_RENDER_BITS;	// Rendering
			u64 m_refCounts1 : REF_SCRIPT_BITS;	// Script
			//u64 m_refCounts2 : REF_DEFRAG_BITS;	// Defrag
			u64 m_refCounts3 : REF_OTHER_BITS;	// Other
		} m_refs;

		u64 m_refsUnified;
	};

	int GetSubRefCount(strRefKind refKind)	const {
		switch (refKind) {
		case REF_RENDER:
			return m_refs.m_refCounts0;
		case REF_SCRIPT:
			return m_refs.m_refCounts1;
			//case REF_DEFRAG:
			//return m_refs.m_refCounts2;
		default:
			return m_refs.m_refCounts3;
		}
	}

	void SetSubRefCount(strRefKind refKind, int refCount) {
		switch (refKind) {
		case REF_RENDER:
			m_refs.m_refCounts0 = (u64) refCount;
			break;
		case REF_SCRIPT:
			m_refs.m_refCounts1 = (u64) refCount;
			break;
			//case REF_DEFRAG:
			//m_refs.m_refCounts2 = (u32) refCount;
			//break;
		default:
			m_refs.m_refCounts3 = (u64) refCount;
		}

#if TRACE_REFCOUNT
		const char *trackRefCountName;
		if (PARAM_trackrefcount.Get(trackRefCountName) && ((1 << refKind) & g_TrackRefCountKindBitmask) && m_name.GetCStr() && strstr(m_name.GetCStr(), trackRefCountName))
		{
			Displayf("Trace history: SET: Ref count kind %d for %s set to %d (%p)", refKind, m_name.GetCStr(),
				refCount, this);
			sysStack::PrintStackTrace();
		}
#endif // TRACE_REFCOUNT

	}
#else // DETAILED_MODULE_REF_COUNT
	u16 m_refCount;	
#endif // DETAILED_MODULE_REF_COUNT	

	u16	m_padding : 13;
	u16	m_bIsDummy : 1;
	u16	m_bIsDependency : 1;
	u16	m_bHasRiverBounds : 1;
};

struct fwStaticBoundsStoreInterface
{
	virtual ~fwStaticBoundsStoreInterface() {};

	virtual phInst* AddBoundToPhysicsLevel(phBound* /*pBound*/, s32 /*index*/, bool& /*bContainsMover*/, u32 /*nTypeFlags*/ = 0,
																			u32 /*nIncludeFlags*/ = 0,
																			bool /*slotIsDummy*/ = false, bool /*slotIsDependency*/ = false) { return NULL; }

	virtual void RemoveInstFromPhysicsWorld(phInst* /*pInst*/) {}
};

struct BoundsStoreCacheEntry 
{
	BoundsStoreCacheEntry() {}
	BoundsStoreCacheEntry(const u32 hashKey,const Vector3& min, const Vector3& max, u8 boxStreamerAssetType) : 
	m_hashKey(hashKey),
	m_minX(min.x),m_minY(min.y),m_minZ(min.z),
	m_maxX(max.x),m_maxY(max.y),m_maxZ(max.z),
	m_assetType(boxStreamerAssetType) { }
	// Copy to local structure to ensure alignment - don't skip this step because it looks unnecessary, you might just be lucky that the structure is aligned!
	BoundsStoreCacheEntry(const void* const pEntry) { memcpy(this,pEntry,sizeof(BoundsStoreCacheEntry)); }

#if __DEV
	void Print();
#endif // __DEV

	u32 m_hashKey;
	float m_minX, m_minY, m_minZ;
	float m_maxX, m_maxY, m_maxZ;
	u8 m_assetType;
};

class fwStaticBoundsStore : public fwAssetRscStore<phBound, fwBoundDef>
{
public:
	fwStaticBoundsStore();
	~fwStaticBoundsStore();

	void Init();

	virtual void Set(strLocalIndex index, phBound* pObject);
	virtual void Remove(strLocalIndex index);
	virtual void Shutdown();
	virtual strLocalIndex AddSlot(const strStreamingObjectName name);
	virtual strLocalIndex AddSlot(strLocalIndex index, const strStreamingObjectName name);
	virtual void RemoveSlot(strLocalIndex index);
	virtual bool Load(strLocalIndex index, void* pData, int size);

	virtual int GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const;

	void SetAsDummyBound(strLocalIndex index, InteriorProxyIndex proxyID, strLocalIndex dependencySlot1);
	void GetDummyBoundData(strLocalIndex index, InteriorProxyIndex& proxyID, strLocalIndex& dependencySlot1);

	void RemoveAll();
	virtual bool ModifyHierarchyStatus(strLocalIndex index, eHierarchyModType modType);

	fwBoxStreamer& GetBoxStreamer() { return m_boxStreamer; }

#if __BANK
	void InitWidgets(bkBank& bank);
	void PrintExtraInfo(strLocalIndex index, char* extraInfo, size_t maxSize) const {m_boxStreamer.Print(index.Get(),extraInfo,maxSize);}
	void DumpStaticBoundsInfo(void);
	static void PoolFullCallback(void* pItem);
#endif

	// cache file 
	bool ReadFromCacheFile(const void* const pLine, fiStream* pDebugTextFileToWriteTo);
	void AddToCacheFile(fiStream* pDebugTextFileToWriteTo);
	void UpdateChangedStaticBounds();

	void SetInterface(fwStaticBoundsStoreInterface *storeInterface)		{ m_StaticBoundsStoreInterface = storeInterface; }

	void SetBoxStreamerEntry(s32 index, const spdAABB& aabb, u8 assetType);
	void StoreImapGroupBoundsList(const atHashString& imapGroup, const atArray<atHashString>& boundsList);
	void SetEnabled(const u32 nameHash, bool bEnabled);

	bool IsScriptManaged(s32 index);

	void SetWeaponBounds(bool bEnabled)
	{
		fwBoxStreamerAssetFlags requiredAssets = fwBoxStreamerAsset::FLAG_STATICBOUNDS_MOVER;
		if (bEnabled)
		{
			requiredAssets |= fwBoxStreamerAsset::FLAG_STATICBOUNDS_WEAPONS;
		}
		m_boxStreamer.SetRequiredAssetFlags(requiredAssets);
	}

	void RequestAndRemove(const atMap<s32, bool>& validArchives);
	void PostRegistration();

	__forceinline bool NearRiverBounds(Vec3V_In vPos, float fRejectionAboveWaterMargin=0.0f) const
	{
		const spdSphere sphere(vPos, ScalarV(fRejectionAboveWaterMargin));
		for(s32 i=0; i<m_loadedRiverBounds.GetCount(); i++)
		{
			const spdAABB& aabb = m_boxStreamer.GetBoundsConst( m_loadedRiverBounds[i] );
			if (aabb.IntersectsSphere(sphere)) { return true; }
		}
		return false;
	}

	// Memory optimization
	fwBoundData* GetSlotData(const s32 index, atSNode<fwBoundData>** pPrev = NULL);
	int GetDefDataSize() const { return m_boundData.GetNumItems(); }

	void UpdateStreamingRanges(bool bExpandMoverRange);

private:
	// Add bound to the physics world (returns true if the bound was "high-detail", false otherwise).
	fwBoxStreamerAsset::eType AddSlotToPhysicsLevel(s32 index);
	fwBoxStreamerAsset::eType AddBoundToPhysicsLevel(s32 ownerIndex, phBound* boundData);
	void RemoveBoundFromPhysicsLevel(s32 index);

	// we maintain a list of bounds files containing river bounds, as a simple way to avoid probing when not needed
	void AddToRiverBoundsList(s32 index) { m_loadedRiverBounds.Grow() = index; }
	void RemoveFromRiverBoundsList(s32 index)
	{
		for(s32 i=0; i<m_loadedRiverBounds.GetCount(); i++)
		{
			if(m_loadedRiverBounds[i] == index)
			{
				m_loadedRiverBounds.DeleteFast(i);
				--i;
			}
		}
	}
	atArray<s32> m_loadedRiverBounds;

	fwBoxStreamer m_boxStreamer;
	fwStaticBoundsStoreInterface *m_StaticBoundsStoreInterface;

	atMap< u32, atArray<u32>* > m_imapGroupMap; // map of imap group names to their list of bounds
	atSList<fwBoundData> m_boundData;	// Memory Optimization hack for PS3/360
	
	typedef atMap<strLocalIndex, InteriorProxyIndex>	dependantProxyMap; // this is mapping dummy bounds slots to proxy ID
	atMap<strLocalIndex, dependantProxyMap>		m_proxyTable; // mapping parent (dependency) to a number of dummyslot/proxyID pairs
	atMap<strLocalIndex, strLocalIndex>			m_dependencyTable; // mapping dummy slot to parent

	phBound										m_emptyphBound;	
};

// wrapper class needed to interface with game skeleton code
class fwStaticBoundsStoreWrapper
{
public:
	static void Init(unsigned initMode);
	static void Shutdown(unsigned shutdownMode);
};

extern fwStaticBoundsStore g_StaticBoundsStore;

} // namespace rage

#endif // FWSCENE_STORES_STATICBOUNDSSTORE_H
