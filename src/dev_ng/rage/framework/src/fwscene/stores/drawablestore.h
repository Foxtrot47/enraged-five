// 
// stores/drawablestore.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWSCENE_STORES_DRAWABLESTORE_H_
#define FWSCENE_STORES_DRAWABLESTORE_H_

#include "fwtl/assetstore.h"

#include "rmcore/drawable.h"

namespace rage {

typedef rmcDrawable Drawable;

#if !__SPU

class fwDrawableDef : public fwAssetDef<Drawable>
{
public:
	void Init(const strStreamingObjectName name)
	{
		fwAssetDef<Drawable>::Init(name);

		WIN32PC_ONLY(m_deviceResources = NULL);

		m_txdIndex			= -1;

		m_bMatchError		= 0;
		m_bIsBeingDefragged	= 0;
		m_bHasHD			= 0;
		m_bIsBoundHD		= 0;
	}

	strLocalIndex GetTxdIndex() const
	{
		return strLocalIndex(m_txdIndex);
	}

	void SetTxdIndex(s32 txdIndex)
	{
		m_txdIndex = txdIndex;
	}

	WIN32PC_ONLY(void *m_deviceResources);
private:
	s32 m_txdIndex;
public:

	u16	m_bMatchError		: 1;
	u16 m_bIsBeingDefragged : 1;
	u16 m_bHasHD			: 1;
	u16 m_bIsBoundHD		: 1;
	u16 m_pad				: 12;
};


class fwDrawableStore : public fwAssetRscStore<Drawable, fwDrawableDef>
{
public:
	fwDrawableStore();

	virtual void Set(strLocalIndex index, Drawable* m_pObject);
	virtual void Remove(strLocalIndex index);
	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);

	virtual void* Defragment(strLocalIndex index, datResourceMap& map, bool& flush);
	virtual void DefragmentComplete(strLocalIndex index);
	virtual void DefragmentPreprocess(strLocalIndex index);

	bool IsBeingDefragged(strLocalIndex index);
	bool GetIsHDCapable(strLocalIndex index);
	void SetIsHDCapable(strLocalIndex index, bool val);
	bool GetIsBoundHD(strLocalIndex index);
	void SetIsBoundHD(strLocalIndex index, bool val);

	virtual int GetDependencies(strLocalIndex UNUSED_PARAM(index), strIndex *UNUSED_PARAM(pIndices), int indexArraySize) const;
	void ShutdownLevel();

#if !__ASSERT
	void SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent) {GetSlot(index)->SetTxdIndex(parent.Get());}
#else // !__ASSERT
	void SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent);
#endif // !__ASSERT

	strLocalIndex GetParentTxdForSlot(strLocalIndex index) const {return GetSlot(index)->GetTxdIndex();}

	void SetPlaceFunctor(fwPlaceFunctor functor)
	{
		sm_PlaceFunctor=functor;
	}

	void SetDefragmentFunctor(fwDefragmentFunctor functor)
	{
		sm_DefragmentFunctor=functor;
	}
private:
	static fwPlaceFunctor sm_PlaceFunctor;
	static fwDefragmentFunctor sm_DefragmentFunctor;
};

// wrapper class needed to interface with game skeleton code
class fwDrawableStoreWrapper
{
public:
	static void Shutdown(unsigned shutdownMode);
};


extern fwDrawableStore g_DrawableStore;

#endif // !__SPU

}	// namespace rage 

#endif // FWSCENE_STORES_DRAWABLESTORE_H_
