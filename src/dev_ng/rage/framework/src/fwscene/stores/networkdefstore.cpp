
// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
#include "NetworkDefStore.h"

// Framework headers
#include "fwsys/fileExts.h"

//ANIM_OPTIMISATIONS ()

// --- Defines ------------------------------------------------------------------

namespace rage {

// --- Globals ------------------------------------------------------------------

fwNetworkDefStore g_NetworkDefStore;

// -- fwNetworkDefStore ----------------------------------------------------------

fwNetworkDefStore::fwNetworkDefStore()
: fwAssetStore<mvNetworkDef>("NetworkDefStore", PI_NETWORK_DEF_FILE_ID, CONFIGURED_FROM_FILE, 292, true)
{
}

bool fwNetworkDefStore::LoadFile(strLocalIndex index, const char *pFilename)
{
	//Make a store of mvNetworkDef objects
	fwAssetDef<mvNetworkDef>* pDef = GetSlot(index);

	Assertf(pDef, "No object at this slot");
	Assertf(pDef->m_pObject == NULL, "%s:Object is in memory already", pDef->m_name.GetCStr());

	pDef->m_pObject = rage_new mvNetworkDef;

	//Fill the store object with parsed data.
	bool rt = pDef->m_pObject->Load(pFilename);

	if(!rt)
	{
		delete pDef->m_pObject; 
		pDef->m_pObject = NULL;
	}
	return rt;
}

void fwNetworkDefStore::Remove(strLocalIndex index)
{
	ASSERT_ONLY(fwAssetDef<mvNetworkDef>* pDef = GetSlot(index);)
		Assertf(pDef, "No network def in slot");
	Assertf(pDef->m_pObject!=NULL, "Network Def not in memory");

	fwAssetStore<mvNetworkDef>::Remove(index);
}

} // namespace rage
