//
// fwscene/stores/imapgroup.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//
#ifndef _FWSCENE_STORES_IMAPGROUP_H_
#define	_FWSCENE_STORES_IMAPGROUP_H_

#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/string.h"
#include "script/thread.h"

namespace rage
{

class fwEntity;

// NOTES
// A managed transition (typically triggered by script or a rayfire sequence) from one
// manually streamed map section to another - e.g. an undamaged version of a location to
// a damaged version.
// Whereas these imap groups are typically switched on and off via script commands, it is
// sometimes desirable to pre-stream the end state imap and entities and perform the
// switch in full view (e.g. at the end of a rayfire animation).

// a transition from a start imap to an end imap
class fwImapGroupSwap
{
public:
	fwImapGroupSwap() : m_bActive(false), m_bLoaded(false), m_bAutomatic(false), m_startIndex(-1), m_endIndex(-1), m_bPersistent(false), m_bMarkedForCleanup(false) {}

	void Start(s32 startIndex, const atHashString &startNameHash, s32 endIndex, const atHashString &endNameHash, bool bAutomatic);
	void Stop();
	void Update() { if (m_bActive && m_bLoaded && m_bAutomatic) { Stop(); } }
	bool IsActive() const { return m_bActive; }
#if !__FINAL
	const char* GetStartName() const { return m_startName.GetCStr(); }
	const char* GetEndName() const { return m_endName.GetCStr(); }
#endif	//	!__FINAL
	void GetEntities(atArray<fwEntity*>& entityList);
	bool AreStartImapsLoaded() const;
	bool AreEndImapsLoaded() const;
	void SetIsLoaded(bool bLoaded) { m_bLoaded = bLoaded; }
	bool GetIsLoaded() const { return m_bLoaded; }
	bool GetIsAutomatic() const { return m_bAutomatic; }
	void AbandonEarly(bool bAllowWorldStateChange=true);
	void PreventFade();
	void RemoveStartState();
	bool UsesImap(s32 slotIndex) const { return (slotIndex==m_startIndex || slotIndex==m_endIndex); }

	s32	GetEndIndex(void) { return(m_endIndex); }

	bool GetIsScriptOwned() const { return m_bScriptOwned; }
	void SetIsScriptOwned(scrThreadId ownerThreadId) { m_bScriptOwned=true; m_ownerThreadId=ownerThreadId; }
	bool IsOwnedBy(scrThreadId ownerThreadId) const { return m_bScriptOwned && m_ownerThreadId==ownerThreadId; }

	void SetStart();
	void SetEnd();

	void MarkForCleanup() { m_bMarkedForCleanup=true; }
	bool IsMarkedForCleanup() const { return m_bMarkedForCleanup; }

	void SetIsPersistent(bool val)	{ m_bPersistent = val; }
	bool GetIsPersistent()	{ return m_bPersistent; }

private:
	void PostponeWorldAdd(bool bPostpone);
	void ProcessPostponedWorldAdd();

	void MakeVisible(s32 index, atHashString &name);
	void MakeInvisible(s32 index, atHashString &name);
	
	bool m_bActive;
	bool m_bLoaded;
	bool m_bScriptOwned;
	bool m_bAutomatic;
	bool m_bPersistent;
	bool m_bMarkedForCleanup;
	s32 m_startIndex;
	s32 m_endIndex;
	atHashString m_startName;
	atHashString m_endName;

	scrThreadId m_ownerThreadId;
};

// primary interface for creating, updating and stopping active imap group transitions
class fwImapGroupMgr
{
public:
	
	enum
	{
#ifdef GTA_REPLAY_RAGE
		// MAX_RAYFIRES_PRELOAD_COUNT (see ReplayFileManager.h, each replay slot potentially requires 2 swaps) doubled for replay, + 1 for script
		MAX_CONCURRENT_SWAPS	= (10*2)+1,
#else
		MAX_CONCURRENT_SWAPS	= 7,	//one is reserved for script
#endif

		SCRIPT_INDEX			= 0,
		INVALID_INDEX			= 0xffff
	};

	void Init();
	void Update();
	void UpdateCleanup();
	void Shutdown();
	
	void CreateNewSwapForScript(const atHashString& startNameHash, const atHashString& endNameHash, scrThreadId ownerThreadId);
	bool CreateNewSwap(const atHashString& startNameHash, const atHashString& endNameHash, u32& newSwapIndex, bool bAutomatic);
	bool CreateNewSwapPersistent(const atHashString& startNameHash, const atHashString& endNameHash, u32& newSwapIndex);

	bool GetIsSwapActive() const;
	bool IsUsingImap(s32 slotIndex) const;

	bool GetIsActive(u32 swapIndex) const { return m_aSwaps[swapIndex].IsActive(); }
	void GetSwapEntities(u32 swapIndex, atArray<fwEntity*>& entityList);
	void SetIsLoaded(u32 swapIndex, bool bLoaded);
	bool GetIsLoaded(u32 swapIndex);
	bool GetIsSwapImapLoaded(u32 swapIndex);
	void CompleteSwap(u32 swapIndex, bool bFadeUp=false);
	void MarkSwapForCleanup(u32 swapIndex);

	void SetIsPersistent(u32 swapIndex, bool val);
	bool GetIsPersistent(u32 swapIndex);

	void SetStart(u32 swapIndex);
	void SetEnd(u32 swapIndex);

	void AbandonSwap(u32 swapIndex);
	void RemoveStartState(u32 swapIndex);

	s32 GetFinalImapIndex(s32 swapIndex) { return(m_aSwaps[swapIndex].GetEndIndex()); }

	static bool IsValidIndex(u32 swapIndex) { return((swapIndex != INVALID_INDEX) && (swapIndex < MAX_CONCURRENT_SWAPS)); }

	void AbandonOwnedByScript(scrThreadId ownerId)
	{
		if (m_aSwaps[SCRIPT_INDEX].IsActive() && m_aSwaps[SCRIPT_INDEX].IsOwnedBy(ownerId))
		{
			AbandonSwap(SCRIPT_INDEX);
		}
	}

private:
	fwImapGroupSwap m_aSwaps[MAX_CONCURRENT_SWAPS];

#if __BANK
public:
	void DisplayActiveTransitions();
#endif	//__BANK
};

}	//namespace rage

#endif	//_FWSCENE_STORES_IMAPGROUP_H_
