// --- Include Files ------------------------------------------------------------

#include "expressionsdictionarystore.h"

#include "crExtra/expressions.h"
#include "fwsys/fileExts.h"


// --- Defines ------------------------------------------------------------------

// --- Globals ------------------------------------------------------------------

namespace rage {

fwAssetRscStore<crExpressionsDictionary> g_ExpressionDictionaryStore("ExprDictStore", 
																	EXPRESSIONSDICTIONARY_FILE_ID, 
																	CONFIGURED_FROM_FILE, 
																	292,
																	false, 
																	EXPRESSIONSDICTIONARY_FILE_VERSION);
} // namespace rage
