// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
#include "PoseMatcherStore.h"

// Framework headers
#include "fwsys/fileExts.h"

//ANIM_OPTIMISATIONS ()

// --- Defines ------------------------------------------------------------------

namespace rage {

// --- Globals ------------------------------------------------------------------

fwPoseMatcherStore g_PoseMatcherStore;

// -- fwNetworkDefStore ----------------------------------------------------------
fwPoseMatcherStore::fwPoseMatcherStore()
: fwAssetStore<crPoseMatcher>("PoseMatcherStore", POSEMATCHER_FILE_ID, CONFIGURED_FROM_FILE, 292, true)
{
}

bool fwPoseMatcherStore::LoadFile(strLocalIndex index, const char *pFilename)
{
	//Make a store of crPoseMatcher objects
	fwAssetDef<crPoseMatcher>* pDef = GetSlot(index);

	Assertf(pDef, "No object at this slot");
	Assertf(pDef->m_pObject == NULL, "%s:Object is in memory already", pDef->m_name.GetCStr());

	pDef->m_pObject = rage_new crPoseMatcher;

	//Fill the store object with parsed data.
	bool rt = pDef->m_pObject->Load(pFilename);

	if(!rt)
	{
		delete pDef->m_pObject;
		pDef->m_pObject = NULL;
	}
	return rt;
}

void fwPoseMatcherStore::Remove(strLocalIndex index)
{
	ASSERT_ONLY(fwAssetDef<crPoseMatcher>* pDef = GetSlot(index);)
	Assertf(pDef, "No PoseMatcher in slot");
	Assertf(pDef->m_pObject!=NULL, "PoseMatcher not in memory");

	fwAssetStore<crPoseMatcher>::Remove(index);
}

} // namespace rage
