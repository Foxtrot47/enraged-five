/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : imposedTextureDictionary.cpp
// PURPOSE : Wraps a bespoke TXD we can insert our own textures into.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

// framework
#include "imposedTextureDictionary.h"

rage::sysCriticalSectionToken fwImposedTextureDictionary::ms_csToken;
fwImposedTextureDictionary::CleanupQueue fwImposedTextureDictionary::ms_cleanupQueue;

fwImposedTextureDictionary::fwImposedTextureDictionary()
	: m_dictionaryName()
	, m_txdDetails()
{

}

fwImposedTextureDictionary::~fwImposedTextureDictionary()
{
	shutdown();
}

bool fwImposedTextureDictionary::initialize( char const * const name )
{
	SYS_CS_SYNC( ms_csToken );

	bool success = false;

	Assertf( !isInitialized(), "fwImposedTextureDictionary::initialize - Already initialized!" );
	Assertf( name, "fwImposedTextureDictionary::initialize - null name string!" );
	if( !isInitialized() &&  name )
	{
		// Create a bespoke TXD for our textures to go into
		m_txdDetails.m_txd = rage_new rage::fwTxd( (int)sc_maxTextureCount );
		if( Verifyf( m_txdDetails.m_txd, "fwImposedTextureDictionary::initialize - failed to allocate txd for %s", name ) )
		{
			rage::strStreamingObjectName const& dictionaryName( name );
			m_txdDetails.m_txdSlot = rage::g_TxdStore.AddSlot( dictionaryName );
			
			if( Verifyf(rage::g_TxdStore.IsValidSlot( m_txdDetails.m_txdSlot ), 
					"fwImposedTextureDictionary::initialize- failed to add a slot for %s texture dictionary", name ) )
			{
				rage::g_TxdStore.Set( m_txdDetails.m_txdSlot, m_txdDetails.m_txd );
				rage::g_TxdStore.AddRef( m_txdDetails.m_txdSlot, rage::REF_OTHER);
				rage::strIndex index = rage::strStreamingEngine::GetInfo().GetModuleMgr().GetModule(rage::g_TxdStore.GetStreamingModuleId())->GetStreamingIndex( m_txdDetails.m_txdSlot );
				rage::strStreamingEngine::GetInfo().SetDoNotDefrag( index );

				m_dictionaryName = name;
				success = true;
			}
			else
			{
				delete m_txdDetails.m_txd;
				m_txdDetails.m_txd = NULL;
			}
		}
	}

	return success;
}

bool fwImposedTextureDictionary::isInitialized() const
{
	SYS_CS_SYNC( ms_csToken );
	return m_txdDetails.IsInitialized() && m_dictionaryName.GetLength() > 0;
}

void fwImposedTextureDictionary::shutdown()
{
	SYS_CS_SYNC( ms_csToken );

	if( !isInitialized() )
		return;

	if( Verifyf( (rage::g_TxdStore.IsValidSlot( m_txdDetails.m_txdSlot ) != false && rage::g_TxdStore.GetNumRefs( m_txdDetails.m_txdSlot ) > 0), 
		"fwImposedTextureDictionary::shutdown - entry \"%s\" (%d) has been released too many times", m_dictionaryName.c_str(), rage::g_TxdStore.GetNumRefs( m_txdDetails.m_txdSlot ) ) )
	{
		ms_cleanupQueue.Push( m_txdDetails );
	}

	m_dictionaryName.Clear();
	m_txdDetails.Clear();
}

void fwImposedTextureDictionary::addTexture( rage::atFinalHashString const& textureName, rage::grcTexture const* texture )
{
	Assertf( isInitialized(), "fwImposedTextureDictionary::addTexture - Used before initialization!" );
	Assertf( !isFull(), "fwImposedTextureDictionary::addTexture - Only supports 1 texture!" );
	if( isInitialized() && !isFull() )
	{
		rage::grcTexture* nonConstTexture = const_cast<rage::grcTexture*>(texture);
		if( m_txdDetails.m_txd->AddEntry( textureName, nonConstTexture ) )
		{
			nonConstTexture->AddRef();
		}
		else
		{
			// Add failed, entry already existed. Overwrite!
			updateTexture( textureName, nonConstTexture );
		}
	}
}

bool fwImposedTextureDictionary::isFull() const
{
	bool isFull = false;

	if( isInitialized() )
	{
		isFull = m_txdDetails.m_txd->GetCount() == 1;
	}

	return isFull;
}

void fwImposedTextureDictionary::updateTexture( rage::atFinalHashString const& textureName, rage::grcTexture const* texture )
{
	Assertf( isInitialized(), "fwImposedTextureDictionary::updateTexture - Used before initialization!" );
	if( isInitialized() )
	{
		int localIndex = m_txdDetails.m_txd->LookupLocalIndex( textureName );
		Assertf( localIndex >= 0, "fwImposedTextureDictionary::updateTexture - Attempting to update texture entry %s that doesn't exist!", textureName.TryGetCStr() );
		if( localIndex >= 0 )
		{
			rage::grcTexture* nonConstTexture = const_cast<rage::grcTexture*>(texture);
			m_txdDetails.m_txd->SetEntryUnsafe( (rage::u32)localIndex, nonConstTexture );
		}
	}
}

rage::grcTexture const * fwImposedTextureDictionary::getTexture( rage::atFinalHashString const& textureName ) const
{
	rage::grcTexture const * result = NULL;

	if( Verifyf( isInitialized(), "fwImposedTextureDictionary::getTexture - Used before initialization!" ) )
	{
		result = isInitialized() ? m_txdDetails.m_txd->Lookup( textureName ) : NULL;
	}

	return result;
}

void fwImposedTextureDictionary::removeTexture( rage::atFinalHashString const& textureName )
{
	if( isInitialized() )
	{
		rage::grcTexture *pTextureInsideTxd = m_txdDetails.m_txd->Lookup( textureName );
		if( pTextureInsideTxd )
		{
			pTextureInsideTxd->DecRef();
			rage::u32 const localIndex = (rage::u32)m_txdDetails.m_txd->LookupLocalIndex( textureName );
			m_txdDetails.m_txd->SetEntryUnsafe( localIndex, NULL );
		}
	}
}

bool fwImposedTextureDictionary::IsAnyPendingCleanup()
{
    SYS_CS_SYNC( ms_csToken );
    bool const c_pending = ms_cleanupQueue.GetCount() > 0;
    return c_pending;
}

bool fwImposedTextureDictionary::IsPendingCleanup( char const * const name )
{
	bool pending = false;

	SYS_CS_SYNC( ms_csToken );

	rage::strLocalIndex slotIndex = rage::g_TxdStore.FindSlot( name );

	for( rage::s32 index = 0; slotIndex.IsValid() && index < ms_cleanupQueue.GetCount(); ++index )
	{
		TxdDetails details = ms_cleanupQueue[index];
		if( details.m_txdSlot == slotIndex )
		{
			pending = true;
			break;
		}
	}

	return pending;
}

void fwImposedTextureDictionary::UpdateCleanup()
{
	SYS_CS_SYNC( ms_csToken );

	if( ms_cleanupQueue.GetCount() > 0 )
	{
		TxdDetails& details = ms_cleanupQueue.Top();
		if( details.m_rtCleanupFlagged )
		{
			int const c_numRefs = rage::g_TxdStore.GetNumRefs( details.m_txdSlot );
			if( c_numRefs <= 1 )
			{
				rage::g_TxdStore.RemoveSlot( details.m_txdSlot );
				ms_cleanupQueue.Pop();
			}
		}
	}
}

void fwImposedTextureDictionary::RTUpdateCleanup()
{
	SYS_CS_SYNC( ms_csToken );

	int const c_cleanupCount = ms_cleanupQueue.GetCount();
	for( int index = 0; index < c_cleanupCount; ++index )
	{
		TxdDetails& details = ms_cleanupQueue[index];

		int const c_numRefs = rage::g_TxdStore.GetNumRefs( details.m_txdSlot );
		rage::grcTexture *pTextureInsideTxd = details.m_txd->GetCount() > 0 ? details.m_txd->GetEntry(0) : NULL;
		if( c_numRefs <= 1 && ( !pTextureInsideTxd || ( pTextureInsideTxd && pTextureInsideTxd->GetRefCount() <= 1 ) ) )
		{
			details.m_rtCleanupFlagged = true;
		}
	}
}

bool fwImposedTextureDictionary::containsTexture( rage::atFinalHashString const& textureName ) const
{
	bool contains = getTexture( textureName ) != NULL;
	return contains;
}
