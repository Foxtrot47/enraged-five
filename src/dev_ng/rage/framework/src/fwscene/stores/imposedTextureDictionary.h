/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : ImposedTextureDictionary.h
// PURPOSE : Wraps a bespoke TXD we can insert our own textures into.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWSCENE_STORES_IMPOSED_TEXTURE_DICTIONARY_H_
#define FWSCENE_STORES_IMPOSED_TEXTURE_DICTIONARY_H_

// rage
#include "atl/string.h"
#include "atl/hashstring.h"
#include "atl/queue.h"
#include "system/criticalsection.h"

// framework
#include "fwscene/stores/txdstore.h"
#include "streaming/streamingdefs.h"

namespace rage
{
	class grcTexture;
}

//! NOTE - Any textures added to an imposed dictionary MUST remain valid until a subsequent update/remove call
//!		   occurs. This is because the dictionary does _not_ take ownership of the textures, as it assumes callee
//!		   code is doing something cunning with the texture and will know better about the lifespan.
class fwImposedTextureDictionary
{
public: //declarations and variables
	static size_t const	sc_maxTextureCount = 8;

public:
	fwImposedTextureDictionary();
	~fwImposedTextureDictionary();

	bool initialize( char const * const name );
	bool isInitialized() const;
	void shutdown();

	char const * getDictionaryName() const { return m_dictionaryName.c_str(); }

	// PURPOSE: Add an entry into the texture dictionary for the given texture.
	// PARAMS:
	//		textureName - Name for the texture.
	//		texture - Texture to set
	void addTexture( rage::atFinalHashString const& textureName, rage::grcTexture const* texture );
	bool isFull() const;
	void updateTexture( rage::atFinalHashString const& textureName, rage::grcTexture const* texture );
	rage::grcTexture const * getTexture( rage::atFinalHashString const& textureName ) const;
	bool containsTexture( rage::atFinalHashString const& textureName ) const;
	void removeTexture( rage::atFinalHashString const& textureName );

    static bool IsAnyPendingCleanup();
	static bool IsPendingCleanup( char const * const name );
	static void UpdateCleanup();
	static void RTUpdateCleanup();

private: // declarations and variables

	struct TxdDetails
	{
		rage::strLocalIndex			m_txdSlot;
		rage::fwTxd*				m_txd;
		bool						m_rtCleanupFlagged;

		TxdDetails()
			: m_txdSlot( -1 )
			, m_txd( NULL )
			, m_rtCleanupFlagged( false )
		{

		}

		inline bool IsInitialized() const { return m_txd != NULL && m_txdSlot != -1; }
		inline void Clear() { m_txdSlot = -1; m_txd = NULL; m_rtCleanupFlagged = false; }
	};

	typedef rage::atQueue< TxdDetails, sc_maxTextureCount > CleanupQueue;

	rage::atString				m_dictionaryName;
	TxdDetails					m_txdDetails;

	static rage::sysCriticalSectionToken	ms_csToken;
	static CleanupQueue						ms_cleanupQueue;
};

#endif // FWSCENE_STORES_IMPOSED_TEXTURE_DICTIONARY_H_
