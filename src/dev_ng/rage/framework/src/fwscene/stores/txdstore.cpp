// 
// fwscene/stores/txdstore.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "grcore/texturedefault.h"

#include "diag/art_channel.h"

#include "fwscene/texLod.h"
#include "fwscene/stores/txdstore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"

#include "streaming/strindexdebugmanager.h"

#define MAX_TEXTURE_DIMENSION	(1024)
#define NUM_SPECIALTXD_SLOTS	(4)

#if __DEV
bool g_AllowUncompressedTexture = false;
#endif

PARAM(currenttxddebug, "Temporary debug param for SetCurrentTxd crash");

namespace rage 
{


//namespace rage {
//	extern grcEffect* g_DefaultEffect;
//}

fwTxd fwTxdStore::ms_emptyTxd(0);
__THREAD fwTxd* fwTxdStore::ms_pStoredTxd;

// global instance of the texture dictionary store
fwTxdStore g_TxdStore;

fwTxdStore::fwTxdStore() : fwAssetRscStore<fwTxd, fwTxdDef>("TxdStore", TXD_FILE_ID, CONFIGURED_FROM_FILE, 2048, true, TXD_FILE_VERSION)
{
}

#if __DEV
// Print warnings about large textures and bad format textures
bool CheckTextureSizeAndFormatCB(grcTexture& texture, u32, void* data)
{
	if(texture.GetWidth() > MAX_TEXTURE_DIMENSION || texture.GetHeight() > MAX_TEXTURE_DIMENSION)
		artWarningf("Found large texture %dx%d %s in txd %s",  texture.GetWidth(), texture.GetHeight(), texture.GetName(), (const char*)data);

	// added width and height checks so we don't complain about palette textures
	if(!g_AllowUncompressedTexture && texture.GetBitsPerPixel() > 8 && texture.GetWidth() > 32 && texture.GetHeight() > 32)
		artWarningf("Found texture with more than 8 bits per pixel (non DXT) %d %s in txd %s",  texture.GetBitsPerPixel(), texture.GetName(), (const char*)data);

	return true;
}
#endif // __DEV


// Set entry 
void fwTxdStore::Set(strLocalIndex index, fwTxd* m_pObject)
{
	fwTxdDef* pTxdDef = GetSlot(index);
	Assertf(pTxdDef, "No texture dictionary at this slot");

	if (pTxdDef->m_isDummy)
	{
		pTxdDef->m_pObject = NULL;
		pTxdDef->m_isDummy = false;
	}

	Assertf(pTxdDef->m_pObject==NULL, "Texture is already in memory");

	UPDATE_KNOWN_REF(pTxdDef->m_pObject , m_pObject);

	if(pTxdDef->m_pObject)
		fwTxdStore::SetupTxdParent(index);

#if __DEV
	pTxdDef->m_pObject->ForAll(&CheckTextureSizeAndFormatCB, (void*)GetName(index));
#endif
}

void fwTxdStore::RemoveSlot(strLocalIndex index) 
{ 
	fwAssetRscStore<fwTxd, fwTxdDef>::RemoveSlot(index); 
}


// Remove txd
void fwTxdStore::Remove(strLocalIndex index)
{
	fwTxdDef* pDef = GetSlot(index);
	Assertf(pDef, "No texture dictionary at this slot");
	Assertf(pDef->m_pObject!=NULL, "Texture dictionary is not in memory");
	Assertf(pDef->m_pObject->GetCurrent() != pDef->m_pObject, "Can't delete texture dictionary while it is the current one (%s)", pDef->m_name.TryGetCStr());

	if (pDef->m_isDummy)
	{
		pDef->m_pObject = NULL;
		return;
	}

	fwAssetRscStore<fwTxd, fwTxdDef>::Remove(index);
}

bool fwTxdStore::LoadFile(strLocalIndex index, const char* pFilename)
{
	AssertMsg(0,"fwTxdStore::LoadFile is deprecated, use CStreaming::LoadObject"); // this function bypasses memory tracking and other niceness
	return fwAssetRscStore<fwTxd, fwTxdDef>::LoadFile(index, pFilename);
}

// Return the objects this texture dictionary requires before it can be loaded. For texture 
// dictionaries this is any parent texture dictionaries
int fwTxdStore::GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const
{
	int count = 0;
	strLocalIndex parentTxd = fwTxdStore::GetParentTxdSlot(index);
	if(parentTxd != -1)
	{
		AddDependencyOutput(pIndices, count, GetStreamingIndex(parentTxd), indexArraySize, GetStreamingIndex(strLocalIndex(indexArraySize)));
	}
	return count;
}

bool fwTxdStore::Load(strLocalIndex index, void* pData, int size)
{
	Assert(index.Get() >= 0);

	fwTxdDef* def = GetSlot(index);
	if(def->m_isDummy)
	{
		Assert(!pData);
		def->m_pObject = &ms_emptyTxd;
		return true;
	}

	return fwAssetStore<fwTxd, fwTxdDef>::Load(index, pData, size);
}

// Registers a new name or removes the dummy flag if there's a dummy slot found
strLocalIndex fwTxdStore::Register(const char* name)
{
	strStreamingObjectName objectName( name );

	strLocalIndex index = FindSlot(objectName);
	// If cannot find slot then create one. 
	if(index == -1)
		index = AddSlot(objectName);
	else
	{
		fwTxdDef* pDef = GetSlot(strLocalIndex(index));
		Assert(pDef);
		Assertf(!stricmp(name, GetName(strLocalIndex(index))),"%s:Hash name clash", name);
		
		if (pDef->m_isDummy)
		{
			// tell the streaming system that this entry is no longer a dummy or bad things can happen (url:bugstar:3665688)
			strIndex streamIndex = GetStreamingIndex(index);
			strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(streamIndex);
			Assertf(info.GetStatus() == STRINFO_NOTLOADED, "Trying to re-register %s, but it's still %s", strStreamingEngine::GetObjectName(streamIndex), info.GetFriendlyStatusName());
			info.ClearFlag(streamIndex, STRFLAG_INTERNAL_DUMMY, false);
			STRINDEX_DEBUG_MANAGER( Remove( streamIndex ) );

			pDef->m_isDummy = false;
			pDef->m_pObject = NULL;
		}
	}
	return index;
}

// Remove all unreferenced dictionaries
void fwTxdStore::ShutdownLevel()
{
	// Call remove all twice to ensure the parent texture dictionaries get deleted
	RemoveAll();
	RemoveAll();
}

#if __ASSERT
void fwTxdStore::SetParentTxdSlot(strLocalIndex index, strLocalIndex parentId)
{
	if (GetSlot(index)->GetParentId() != parentId)
	{
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(GetStreamingIndex(index));

		Assertf(info.GetStatus() == STRINFO_NOTLOADED, "%s sets its parent TXD from %d to %d, but its status is %s", GetName(index), GetSlot(index)->GetParentId().Get(), parentId.Get(), info.GetFriendlyStatusName());
		GetSlot(index)->SetParentId(parentId.Get());
	}
}
#endif // __ASSERT

// Setup the parent fwTxd pointer
void fwTxdStore::SetupTxdParent(strLocalIndex index)
{
	fwTxdDef* pTxdDef = GetSlot(index);
	strLocalIndex parentId = strLocalIndex(pTxdDef->GetParentId());

	// Set TXD parent
	while (parentId != -1 && GetSlot(parentId)->m_isDummy)
	{
		fwTxdDef* pParentTxdDef = GetSlot(parentId);
		Assertf(pParentTxdDef, "No parent texture dictionary at this slot");

		parentId = pParentTxdDef->GetParentId();
	}

	if (parentId != -1)
	{
		Assertf(GetSlot(parentId)->m_pObject, "Parent Texture dictionary is not in memory");
		pTxdDef->m_pObject->SetParent(GetSlot(parentId)->m_pObject);
	}
}

fwTxd* fwTxdStore::GetSafeFromIndex(strLocalIndex index)
{
	if (index.Get() < 0)
	{
		return NULL;
	}

	fwTxdDef* pTxdDef = GetSlot(index);

	while (pTxdDef && pTxdDef->m_isDummy && pTxdDef->GetParentId() != -1)
	{
		pTxdDef = GetSlot(pTxdDef->GetParentId());
	}

	return pTxdDef ? pTxdDef->m_pObject : NULL;
}


// Set the current texture dictionary given an index
void fwTxdStore::SetCurrentTxd(strLocalIndex index)
{
	// Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));		// this command is forbidden outwith the main thread

#if __BANK
	if (PARAM_currenttxddebug.Get())
	{
		fwTxd* cur = fwTxd::GetCurrent();
		if (cur)
		{
			char buf[128] = {0};
			Displayf("B* 522875: SetCurrentTxd processing %p - vtbl: %p - ", ms_pStoredTxd, *(void**)ms_pStoredTxd);
			fwTxd::GetElementName(buf, sizeof(buf), cur);
			Displayf("B* 522875: SetCurrentTxd - Old current: %s, num refs: %d\n", buf, cur->GetRefCount());
		}
		else
			Displayf("B* 522875: SetCurrentTxd - Old current: none\n");
	}
#endif
	if (index != -1)
	{
		fwTxdDef* pTxdDef = GetSlot(index);

		Assertf(pTxdDef, "No texture dictionary at this slot");

#if __BANK
		if (PARAM_currenttxddebug.Get())
		{
			if (pTxdDef->m_isDummy)
				Displayf("B* 522875: SetCurrentTxd - New current is dummy\n");
			else
				Displayf("B* 522875: SetCurrentTxd - New current: %s, num refs: %d\n", pTxdDef->m_name.GetCStr(), pTxdDef->m_pObject->GetRefCount());
		}
#endif

		if (pTxdDef->m_isDummy)
		{
			SetCurrentTxd(pTxdDef->GetParentId());
		}
		else
		{
			Assertf(pTxdDef->m_pObject, "%s:Texture dictionary is not in memory", pTxdDef->m_name.GetCStr());
			fwTxd::SetCurrent(pTxdDef->m_pObject);
		}
	}
	else
	{
#if __BANK
		if (PARAM_currenttxddebug.Get())
			Displayf("B* 522875: SetCurrentTxd - New current: null\n");
#endif
		fwTxd::SetCurrent(NULL);
	}
}


// Push current texture dictionary onto stack
void fwTxdStore::PushCurrentTxd()
{
	// Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));		// this command is forbidden outwith the main thread
	Assertf(ms_pStoredTxd == NULL, "Texture dictionary stack is full");

	ms_pStoredTxd = fwTxd::GetCurrent();
	SAFE_ADD_KNOWN_REF(ms_pStoredTxd);

	if(ms_pStoredTxd)
	{
#if __BANK
		if (PARAM_currenttxddebug.Get())
		{
			fwTxd* cur = fwTxd::GetCurrent();
			if (cur)
			{
				char buf[128] = {0};
				Displayf("B* 522875: PushCurrentTxd processing %p - vtbl: %p - ", ms_pStoredTxd, *(void**)ms_pStoredTxd);
				fwTxd::GetElementName(buf, sizeof(buf), ms_pStoredTxd);
				Displayf("B* 522875: PushCurrentTxd %s, num refs: %d\n", buf, ms_pStoredTxd->GetRefCount());
			}
		}
#endif
		ms_pStoredTxd->AddRef(); 
	}
}


// Pop texture dictionary off stack
void fwTxdStore::PopCurrentTxd()
{
	// Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));		// this command is forbidden outwith the main thread

	fwTxd::SetCurrent(ms_pStoredTxd);
	if(ms_pStoredTxd)
	{
#if __BANK
		if (PARAM_currenttxddebug.Get())
		{
			fwTxd* cur = fwTxd::GetCurrent();
			if (cur)
			{
				char buf[128] = {0};
				Displayf("B* 522875: PopCurrentTxd processing %p - vtbl: %p - ", ms_pStoredTxd, *(void**)ms_pStoredTxd);
				fwTxd::GetElementName(buf, sizeof(buf), ms_pStoredTxd);
				Displayf("B* 522875: PopCurrentTxd %s, num refs: %d\n", buf, ms_pStoredTxd->GetRefCount());
			}
		}
#endif
		ms_pStoredTxd->Release(); 
	}
	SAFE_REMOVE_KNOWN_REF(ms_pStoredTxd);
	ms_pStoredTxd = NULL;
}

// Adds a new slot and marks it as a dummy
int fwTxdStore::AddDummySlot(const strStreamingObjectName name)
{	
	strLocalIndex slotId = strLocalIndex(AddSlot(name));
	fwTxdDef* txd = GetSlot(slotId);
	txd->m_isDummy = true;

	char dummyName[80] = "unknown";
#if !__FINAL
		sprintf(dummyName, "%s", name.GetCStr());
#endif

	strStreamingEngine::GetInfo().RegisterDummyObject(dummyName, slotId, GetStreamingModuleId());

	return slotId.Get();
}

// Defragment
void* fwTxdStore::Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& map, bool& flush)
{
	fwTxd* object = NULL;

	datResource rsc(map,"defrag",true);
	fwTxd::Place(object = (fwTxd*)rsc.GetBase(), rsc);

	flush = true; // txds need to wait until the drawable spu jobs are flushed.

	return object;
}

void fwTxdStore::DefragmentPreprocess(strLocalIndex index)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged= true;
		fwTexLod::SetSwapStateSingle(false, fwAssetLocation(STORE_ASSET_TXD,index.Get()));			// unswap before hitting defrag code
	}
}

void fwTxdStore::DefragmentComplete(strLocalIndex index)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged= false;
		fwTexLod::SetSwapStateSingle(true, fwAssetLocation(STORE_ASSET_TXD,index.Get()));			// reswap since we are unflagging the asset
	}
}

bool fwTxdStore::IsBeingDefragged(strLocalIndex index)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBeingDefragged);
	}
	return(false);
}

bool fwTxdStore::GetIsHDCapable(strLocalIndex index)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bHasHD);
	}
	return(false);
}

void fwTxdStore::SetIsHDCapable(strLocalIndex index, bool val)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bHasHD = val;
	}
}

bool fwTxdStore::GetIsBoundHD(strLocalIndex index)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBoundHD);
	}
	return(false);
}

void fwTxdStore::SetIsBoundHD(strLocalIndex index, bool val)
{
	fwTxdDef* pDef = fwTxdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBoundHD = val;
	}
}

void fwTxdStoreWrapper::Shutdown(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		g_TxdStore.Shutdown();
	}
	else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)	//	SHUTDOWN_SESSION)
	{
		g_TxdStore.ShutdownLevel();
	}
}

#if __BANK
void fwTxdStore::RemoveRef(strLocalIndex index, strRefKind refKind)
{
	fwAssetStore<fwTxd, fwTxdDef>::RemoveRef(index, refKind);

	fwTxdDef* pDef = GetSlot(index);
	int refCount = fwTxdStore::GetNumRefs(index);
	if (!Verifyf(refCount || pDef->m_pObject->GetCurrent() != pDef->m_pObject, "Ref of current txd '%s' reached 0!", pDef->m_name.GetCStr()))
	{
		Warningf("Ref of current txd '%s' reached zero!\n", pDef->m_name.GetCStr());
	}

#if __ASSERT
	if (!refCount && strStreamingEngine::GetInfo().GetStreamingInfoRef(GetStreamingIndex(strLocalIndex(index))).GetDependentCount() == 0)
	{
		// If the Txd hits a ref count of 0, then all of its texture better not have a ref count either.
		fwTxd *txd = pDef->m_pObject;
		int count = txd->GetCount();

		for (int x=0; x<count; x++)
		{
			int txRefCount = txd->GetEntry(x)->GetRefCount();
			Assertf(txRefCount == 1, "Texture %s inside %s has a ref count of %d, but the txd has a zero ref count.", txd->GetEntry(x)->GetName(), GetName(strLocalIndex(index)), txRefCount);
		}
	}
#endif // __ASSERT
}
#endif // __BANK

}	// namespace rage
