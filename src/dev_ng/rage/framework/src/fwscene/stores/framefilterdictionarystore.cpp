// --- Include Files ------------------------------------------------------------

#include "framefilterdictionarystore.h"

#include "cranimation/framefilters.h"
#include "fwanimation/animdirector.h"
#include "fwsys/fileExts.h"
#include "parser/manager.h"
#include "parser/tree.h"

// --- Defines ------------------------------------------------------------------

// --- Globals ------------------------------------------------------------------

namespace rage {

const fwMvFilterSetId FILTER_SET_PLAYER("player",0x6F0783F5);

const fwMvFilterId BONEMASK_ALL("BONEMASK_ALL",0x540BB62);

crFrameFilterDictionaryStore g_FrameFilterDictionaryStore("FrameFilterStore", FRAMEFILTERDICTIONARY_FILE_ID, CONFIGURED_FROM_FILE, 292, false, FRAMEFILTERDICTIONARY_FILE_VERSION);

crFrameFilter *crFrameFilterDictionaryStore::FindFrameFilter(const fwMvFilterId &filterId, const fwMvFilterSetId &filterSetId /*= FILTER_SET_PLAYER*/)
{
	if(filterId.IsNull() || filterSetId.IsNull() || filterId == BONEMASK_ALL)
	{
		return NULL;
	}

	// Find filter set slot
	strLocalIndex filterSetSlot = strLocalIndex(FindSlotFromHashKey(filterSetId.GetHash()));
	if(IsValidSlot(filterSetSlot))
	{
		// Get filter set
		crFrameFilterDictionary *frameFilterDictionary = Get(filterSetSlot);
		if(frameFilterDictionary)
		{
			// Find filter
			crFrameFilter* frameFilter = frameFilterDictionary->Lookup(filterId.GetHash());
			if(frameFilter)
			{
				return frameFilter;
			}
		}
	}

	return NULL;
}

#if __BANK

const char *crFrameFilterDictionaryStore::FindFrameFilterNameSlow(const crFrameFilter *frameFilter)
{
	if(frameFilter)
	{
		for(int i = 0; i < GetCount(); i ++)
		{
			if(IsValidSlot(strLocalIndex(i)) && IsObjectInImage(strLocalIndex(i)))
			{
				if(HasObjectLoaded(strLocalIndex(i)))
				{
					crFrameFilterDictionary *frameFilterDictionary = Get(strLocalIndex(i));
					if(frameFilterDictionary)
					{
						for(int j = 0; j < frameFilterDictionary->GetCount(); j ++)
						{
							const crFrameFilter *_frameFilter = frameFilterDictionary->GetEntry(j);

							if(_frameFilter == frameFilter)
							{
								atHashString name(frameFilterDictionary->GetCode(j));

								return name.TryGetCStr();
							}
						}
					}
				}
			}
		}
	}

	return NULL;
}

#endif

} // namespace rage
