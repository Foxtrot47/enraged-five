// 
// stores/dwdstore.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWSCENE_STORES_DWDSTORE_H_
#define FWSCENE_STORES_DWDSTORE_H_

#include "rmcore/drawable.h"
#include "fwtl/assetstore.h"

namespace rage {

typedef rmcDrawable Drawable;
typedef pgDictionary<Drawable> Dwd;

#if !__SPU


class DwdDef : public fwAssetDef<Dwd>
{
public:
	void Init(const strStreamingObjectName name)
	{
		fwAssetDef<Dwd>::Init(name);

		m_txdIndex			= -1;

		m_noAssignedTxd		= 0;
		m_bMatchError		= 0;
		m_bIsBeingDefragged	= 0;
		m_bHasHD			= 0;
		m_bIsBoundHD		= 0;
	}

	strLocalIndex GetTxdIndex() const
	{
		return strLocalIndex(m_txdIndex);
	}

	void SetTxdIndex(s32 txdIndex)
	{
		m_txdIndex = txdIndex;
	}

private:
	s32 m_txdIndex;
public:

	u16 m_noAssignedTxd		: 1;
	u16 m_bMatchError		: 1;
	u16 m_bIsBeingDefragged : 1;
	u16 m_bHasHD			: 1;
	u16 m_bIsBoundHD		: 1;
	u16 m_pad				: 11;
};

#define fwDwdDef DwdDef

class fwDwdStore : public fwAssetRscStore<Dwd, DwdDef>
{
public:
	fwDwdStore();


	virtual void Set(strLocalIndex index, Dwd* m_pObject);
	virtual void Remove(strLocalIndex index);
	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& info);

	virtual void* Defragment(strLocalIndex index, datResourceMap& map, bool& flush);
	virtual void DefragmentComplete(strLocalIndex index);
	virtual void DefragmentPreprocess(strLocalIndex index);

	rmcDrawable* GetDrawable(strLocalIndex index, int dictionaryEntry)
	{
		if (index.IsValid())
		{
			Dwd *dwd = Get(index);
			if (dwd)
				return dwd->GetEntry(dictionaryEntry);
		}
		return NULL;
	}

	bool IsBeingDefragged(strLocalIndex index);
	bool GetIsHDCapable(strLocalIndex index);
	void SetIsHDCapable(strLocalIndex index, bool val);
	bool GetIsBoundHD(strLocalIndex index);
	void SetIsBoundHD(strLocalIndex index, bool val);
	virtual int GetDependencies(strLocalIndex UNUSED_PARAM(index), strIndex *UNUSED_PARAM(pIndices), int indexArraySize) const;

	void ShutdownLevel();

#if !__ASSERT
	void SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent) {GetSlot(index)->SetTxdIndex(parent.Get());}
#else // !__ASSERT
	void SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent);
#endif // !__ASSERT

	strLocalIndex GetParentTxdForSlot(strLocalIndex index) const {
		const DwdDef* slot = GetSlot(index);
        if (slot)
        {
            return slot->GetTxdIndex();
        }
        return strLocalIndex(-1);
    }

	void SetPlaceFunctor(fwPlaceFunctor functor)
	{
		sm_PlaceFunctor=functor;
	}

	void SetDefragmentFunctor(fwDefragmentFunctor functor)
	{
		sm_DefragmentFunctor=functor;
	}

#if !__NO_OUTPUT
	void SetNoAssignedTxd(strLocalIndex index, bool val)
	{
		DwdDef* slot = GetSlot(index);
		if (slot)
		{
			slot->m_noAssignedTxd = val ? 1 : 0;
		}
	}

#endif
	
private:
	static fwPlaceFunctor sm_PlaceFunctor;
	static fwDefragmentFunctor sm_DefragmentFunctor;
};

// wrapper class needed to interface with game skeleton code
class fwDwdStoreWrapper
{
public:

	static void Shutdown(unsigned shutdownMode);
};


extern fwDwdStore g_DwdStore;

#endif // !__SPU

}	// namespace rage

#endif // FWSCENE_STORES_DWDSTORE_H_
