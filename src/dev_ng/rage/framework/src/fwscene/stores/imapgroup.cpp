//
// fwscene/stores/imapgroup.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "fwgeovis/geovis.h"
#include "fwscene/stores/imapgroup.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/world/WorldMgr.h"
#include "fwscene/world/StreamedSceneGraphNode.h"

#if __BANK
#include "grcore/debugdraw.h"
#endif	//__BANK

namespace rage
{

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		Stops any active swaps, ensures they are all inactive
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::Init()
{
	for (u32 i=0; i<MAX_CONCURRENT_SWAPS; i++)
	{
		m_aSwaps[i].Stop();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Update
// PURPOSE:		Processes any active swaps, stops any that have completed
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::Update()
{
	for (u32 i=0; i<MAX_CONCURRENT_SWAPS; i++)
	{
		m_aSwaps[i].Update();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	UpdateCleanup
// PURPOSE:		abandon swaps which are no longer required, without modifying world state
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::UpdateCleanup()
{
	for (u32 i=0; i<MAX_CONCURRENT_SWAPS; i++)
	{
		fwImapGroupSwap& swap = m_aSwaps[i];

		if (swap.IsActive() && swap.IsMarkedForCleanup())
		{
			swap.AbandonEarly(false);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Shutdown
// PURPOSE:		Clears any active swaps
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::Shutdown()
{
	Init();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CreateNewSwapForScript
// PURPOSE:		script can have a single active swap
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::CreateNewSwapForScript(const atHashString& startNameHash, const atHashString& endNameHash, scrThreadId ownerThreadId)
{
	if (Verifyf(!m_aSwaps[SCRIPT_INDEX].IsActive(), "Script owned IPL swap already running!"))
	{
		AbandonSwap(SCRIPT_INDEX);
	}

	// start new swap
	strLocalIndex startIndex = (startNameHash.GetHash()!=0) ? strLocalIndex(INSTANCE_STORE.FindSlotFromHashKey(startNameHash.GetHash())) : strLocalIndex(-1);
	strLocalIndex endIndex = strLocalIndex(INSTANCE_STORE.FindSlotFromHashKey(endNameHash));

	Assertf(endIndex!=-1, "Cannot find IMAP group %s", endNameHash.GetCStr());
	if (endIndex==-1)
	{
		return;
	}

#if __ASSERT
	if (startIndex != -1)
	{
		Assertf(INSTANCE_STORE.GetPtr(startIndex)!=NULL, "%s is not in memory", startNameHash.GetCStr());
	}
	Assertf(INSTANCE_STORE.GetPtr(endIndex)==NULL, "%s is already in memory", endNameHash.GetCStr());
#endif	//__ASSERT

	m_aSwaps[SCRIPT_INDEX].Start(startIndex.Get(), startNameHash, endIndex.Get(), endNameHash, false);
	m_aSwaps[SCRIPT_INDEX].SetIsScriptOwned(ownerThreadId);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CreateNewSwap
// PURPOSE:		Start a new imap group swap, by pre-streaming the end state
//				imaps and entities, and then switching immediately within a frame.
//				Returns true if successful in starting new swap, false otherwise.
//				If successful, writes the swap index into newSwapIndex.
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::CreateNewSwap(const atHashString &startNameHash, const atHashString &endNameHash, u32& newSwapIndex, bool bAutomatic)
{
	newSwapIndex = INVALID_INDEX;
	strLocalIndex startIndex = (startNameHash.GetHash()!=0) ? strLocalIndex(INSTANCE_STORE.FindSlotFromHashKey(startNameHash.GetHash())) : strLocalIndex(-1);
	strLocalIndex endIndex = strLocalIndex(INSTANCE_STORE.FindSlotFromHashKey(endNameHash));

	Assertf(endIndex!=-1, "Cannot find IMAP group %s", endNameHash.GetCStr());
	if (endIndex==-1)
	{
		newSwapIndex = INVALID_INDEX;
		return false;
	}

	bool bFoundFreeSwap = false;
	newSwapIndex = 1;

	for (u32 i=1; i<MAX_CONCURRENT_SWAPS; i++)	// start from 1 - don't try to allocate 0 which is for script
	{
		if (!m_aSwaps[i].IsActive())
		{
			bFoundFreeSwap = true;
			newSwapIndex = i;
			break;
		}
	}

	if (bFoundFreeSwap)
	{
#if __ASSERT
		if (startIndex != -1)
		{
			Assertf(INSTANCE_STORE.GetPtr(startIndex)!=NULL, "%s is not in memory", startNameHash.GetCStr());
		}
		Assertf(INSTANCE_STORE.GetPtr(endIndex)==NULL, "%s is already in memory", endNameHash.GetCStr());
#endif	//__ASSERT

		m_aSwaps[newSwapIndex].Start(startIndex.Get(), startNameHash, endIndex.Get(), endNameHash, bAutomatic);
	}

	Assertf(bFoundFreeSwap, "Too many concurrent IMAP group swaps (%d)", MAX_CONCURRENT_SWAPS);

	return bFoundFreeSwap;
}



//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CreateNewSwapPersistant
// PURPOSE:		Start a new imap group swap, by pre-streaming the end state
//				imaps and entities, and then switching immediately within a frame.
//				Returns true if successful in starting new swap, false otherwise.
//				If successful, writes the swap index into newSwapIndex.
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::CreateNewSwapPersistent(const atHashString &startNameHash, const atHashString &endNameHash, u32& newSwapIndex)
{
	newSwapIndex = INVALID_INDEX;
	strLocalIndex startIndex = (startNameHash.GetHash()!=0) ? strLocalIndex(INSTANCE_STORE.FindSlotFromHashKey(startNameHash.GetHash())) : strLocalIndex(-1);
	strLocalIndex endIndex = strLocalIndex(INSTANCE_STORE.FindSlotFromHashKey(endNameHash));

	Assertf(endIndex!=-1, "Cannot find IMAP group %s", endNameHash.GetCStr());
	if (endIndex==-1)
	{
		newSwapIndex = INVALID_INDEX;
		return false;
	}

	bool bFoundFreeSwap = false;
	newSwapIndex = 1;

	for (u32 i=1; i<MAX_CONCURRENT_SWAPS; i++)	// start from 1 - don't try to allocate 0 which is for script
	{
		if (!m_aSwaps[i].IsActive())
		{
			bFoundFreeSwap = true;
			newSwapIndex = i;
			break;
		}
	}

	if (bFoundFreeSwap)
	{
#if __ASSERT
		if (startIndex != -1)
		{
			Assertf(INSTANCE_STORE.GetPtr(startIndex)!=NULL, "%s is not in memory", startNameHash.GetCStr());
		}
#endif	//__ASSERT

		m_aSwaps[newSwapIndex].SetIsPersistent(true);
		m_aSwaps[newSwapIndex].Start(startIndex.Get(), startNameHash, endIndex.Get(), endNameHash, false);
	}
	Assertf(bFoundFreeSwap, "Too many concurrent IMAP group swaps (%d)", MAX_CONCURRENT_SWAPS);
	return bFoundFreeSwap;
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIsSwapActive
// PURPOSE:		Returns true if there is a currently active imap group swap, false otherwise
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::GetIsSwapActive() const
{
	for (u32 i=0; i<MAX_CONCURRENT_SWAPS; i++)
	{
		if (m_aSwaps[i].IsActive())
		{
			return true;
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	IsUsingImap
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::IsUsingImap(s32 slotIndex) const
{
	for (u32 i=0; i<MAX_CONCURRENT_SWAPS; i++)
	{
		if (m_aSwaps[i].IsActive() && m_aSwaps[i].UsesImap(slotIndex))
			return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetSwapEntities
// PURPOSE:		adds list of entities (used game-side for scene streaming etc)
//				for swap at specified index, if valid
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::GetSwapEntities(u32 swapIndex, atArray<fwEntity*>& entityList)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].GetEntities(entityList);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetIsLoaded
// PURPOSE:		Signal from game code that all required entities have been
//				successfully loaded by scene streaming, and the end state can
//				now be used.
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::SetIsLoaded(u32 swapIndex, bool bLoaded)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].SetIsLoaded(bLoaded);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIsLoaded
// PURPOSE:		Returns true if all assets have been streamed in and the switch
//				is ready for completion
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::GetIsLoaded(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		return m_aSwaps[swapIndex].GetIsLoaded();
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIsSwapImapLoaded
// PURPOSE:		Returns true if all required imap files are now loaded for
//				the end state of swap, false otherwise
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::GetIsSwapImapLoaded(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		return m_aSwaps[swapIndex].AreEndImapsLoaded();
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CompleteSwap
// PURPOSE:		manually end a (non-auto) IMAP group swap that is fully loaded
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::CompleteSwap(u32 swapIndex, bool bFadeUp)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		fwImapGroupSwap& swap = m_aSwaps[swapIndex];

		Assertf(swap.IsActive(), "Trying to manually end IMAP group swap index %d, but it isn't active", swapIndex);
		Assertf(!swap.GetIsAutomatic(), "Trying to manually end IMAP group swap index but it is set to AUTO");
		if (!swap.GetIsLoaded()) {
#if !HEIGHTMAP_GENERATOR_TOOL
			Warningf("Trying to manually end IMAP group swap, but it's graphics are not loaded yet");
#endif
		}

		if (!bFadeUp)
		{
			swap.PreventFade();
		}
		swap.Stop();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	MarkSwapForCleanup
// PURPOSE:		mark swap for destruction without altering world state
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::MarkSwapForCleanup(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		fwImapGroupSwap& swap = m_aSwaps[swapIndex];
		swap.MarkForCleanup();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AbandonSwap
// PURPOSE:		cleans up and abandons a swap in progress. resets to start state
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::AbandonSwap(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].AbandonEarly();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveStartState
// PURPOSE:		if there is a start state for the specified swap, remove it
//				useful for systems such as rayfire anims which want to use imap
//				groups but need to play animation between start and end states
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::RemoveStartState(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].RemoveStartState();
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetIsPersistant
// PURPOSE:		Sets the persistent state
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::SetIsPersistent(u32 swapIndex, bool val)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].SetIsPersistent(val);
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetStartState
// PURPOSE:		Get the persistent state
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupMgr::GetIsPersistent(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		return m_aSwaps[swapIndex].GetIsPersistent();
	}
	return false;
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetStartState
// PURPOSE:		swap to start state (or nothing if there is no start state)
//				only applicable to persistent swaps
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::SetStart(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].PreventFade();
		m_aSwaps[swapIndex].SetStart();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetStartState
// PURPOSE:		swap to end state (or nothing if there is no end state)
//				only applicable to persistent swaps
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::SetEnd(u32 swapIndex)
{
	if (swapIndex<MAX_CONCURRENT_SWAPS && m_aSwaps[swapIndex].IsActive())
	{
		m_aSwaps[swapIndex].PreventFade();
		m_aSwaps[swapIndex].SetEnd();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Start
// PURPOSE:		specifies slot index (from map data store) and names for
//				start and end of transition.
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::Start(s32 startIndex, const atHashString &startNameHash, s32 endIndex, const atHashString &endNameHash, bool bAutomatic)
{
	m_bActive = true;
	m_bLoaded = false;
	m_bMarkedForCleanup = false;
	m_bScriptOwned = false;
	m_bAutomatic = bAutomatic;
	m_startIndex = startIndex;
	m_endIndex = endIndex;
	m_startName = startNameHash;
	m_endName = endNameHash;

	// mark any required imap and static bounds are not added to the world immediately
	// we postpone it until all drawables are loaded to allow for an instant swap

	// Unless the map is already in that state
	bool dontPostpone = false;

	if( m_bPersistent )
	{
		// Persistant imap swaps may already be in the world (or in the process of loading and being set-up)
		INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(endIndex));
		while( pDef )
		{
			if (pDef && pDef->IsLoaded())
			{
				// Check if it's in the world?, later
				dontPostpone = true;
				break;
			}
			pDef = pDef->GetParentDef();
		}
	}

	if (dontPostpone == false)
	{
		PostponeWorldAdd(true);
	}

	// request imap and static bounds
	fwMapDataStore::GetStore().RequestGroup(strLocalIndex(endIndex), endNameHash, true);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Swap
// PURPOSE:		all entities have been streamed in for the end state, so end transition
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::Stop()
{
	if (m_bActive)
	{
		Assertf(!m_bPersistent, "fwImapGroupSwap::Stop: Error:- Stopping a swap that is persistent");
		RemoveStartState();
		PostponeWorldAdd(false);
		ProcessPostponedWorldAdd();
		m_bActive = false;
		m_bLoaded = false;
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AbandonEarly
// PURPOSE:		abort a swap in progress
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::AbandonEarly(bool bAllowWorldStateChange/*=true*/)
{
	if (m_bActive)
	{
		Assertf(!m_bPersistent, "fwImapGroupSwap::AbandonEarly: Error:- Abandoning a swap that is persistent");

		PostponeWorldAdd(false);

		// remove end state
		if (m_endIndex!=-1)
		{
			ProcessPostponedWorldAdd();

			if (bAllowWorldStateChange)
			{
				INSTANCE_STORE.RemoveGroup(strLocalIndex(m_endIndex), m_endName);
			}
		}

		// request start state
		if (m_startIndex!=-1 && !AreStartImapsLoaded())
		{
			if (bAllowWorldStateChange)
			{
				INSTANCE_STORE.RequestGroup(strLocalIndex(m_startIndex), m_startName);
			}
		}

		m_bActive = false;
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PreventFade
// PURPOSE:		disables time-based fading when end-state of transition is activated
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::PreventFade()
{
	atArray<fwEntity*> entityList;
	GetEntities(entityList);
	for (s32 i=0; i<entityList.GetCount(); i++)
	{
		fwEntity* pEntity = entityList[i];
		if (pEntity)
		{
			pEntity->GetLodData().SetResetDisabled(true);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveStartState
// PURPOSE:		removes the start imap group
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::RemoveStartState()
{
	if (m_startIndex!=-1 && AreStartImapsLoaded())
	{
		fwMapDataStore::GetStore().RemoveGroup(strLocalIndex(m_startIndex), m_startName);
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetStart
// PURPOSE:		Makes the start state (if there is one) visible and the end state invisible
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::SetStart()
{
	Assertf(m_bPersistent, "fwImapGroupSwap::SetStart() - Trying to set the start of a none persistant swap");

	// Make the end state invisible
	if(m_endIndex != -1)
	{
		MakeInvisible(m_endIndex, m_endName);
	}

	// Make the start state visible
	if(m_startIndex != -1)
	{
		MakeVisible(m_startIndex, m_startName);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetEnd
// PURPOSE:		Makes the start state (if there is one) visible and the end state invisible
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::SetEnd()
{
	Assertf(m_bPersistent, "fwImapGroupSwap::SetEnd() - Trying to set the end of a none persistant swap");

	// Make the start state invisible
	if(m_startIndex != -1)
	{
		MakeInvisible(m_startIndex, m_startName);
	}

	// Make the end state visible
	if(m_endIndex != -1)
	{
		MakeVisible(m_endIndex, m_endName);
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PostponeWorldAdd
// PURPOSE:		flag map defs (and any parent defs) to postponed add to world
//				for entities when they stream in, to be handled later when all
//				drawable data is available too
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::PostponeWorldAdd(bool bPostpone)
{
	// IMAPS
	INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(m_endIndex));
	while (pDef != NULL)
	{
		pDef->SetPostponeWorldAdd(bPostpone);
		pDef = pDef->GetParentDef();
	}

	// BOUNDS - TODO?
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ProcessPostponedWorldAdd
// PURPOSE:		performs the final step of adding entities from a map data file
//				to the world representation, where it has been postponed due to
//				imap group transition.
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::ProcessPostponedWorldAdd()
{
	MakeVisible(m_endIndex, m_endName);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	MakeVisible
// PURPOSE:		
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::MakeVisible(s32 index, atHashString &name)
{
	NOTFINAL_ONLY(Displayf("Making Visible: %s", name.GetCStr());)

	// IMAPS
	INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(index));
	while (pDef != NULL)
	{
		if (pDef->IsLoaded())
		{
			fwStreamedSceneGraphNode* pNode = pDef->m_pObject->GetSceneGraphNode();
			if (!pNode->GetIsInWorld())
			{
				gWorldMgr.AddSceneNodeToWorld(pNode);
				Assert(pNode->GetIsInWorld());
			}

			//////////////////////////////////////////////////////////////////////////
			// make sure entities are now available for dummy conversion etc
			u32 numEntities = pDef->GetNumEntities();
			fwEntity** entities = pDef->GetEntities();
			for (u32 i=0; i<numEntities; i++)
			{
				fwEntity* pEntity = entities[i];
				if (pEntity) { pEntity->SetBaseFlag(fwEntity::IS_VISIBLE); }
			}
			//////////////////////////////////////////////////////////////////////////
		}
		pDef = pDef->GetParentDef();
	}

	// BOUNDS
	if (index!=-1)
	{
		g_StaticBoundsStore.SetEnabled(name.GetHash(), true);
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	MakeInvisible
// PURPOSE:		
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::MakeInvisible(s32 index, atHashString &name)
{
	// IMAPS
	INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(index));
	while (pDef != NULL)
	{
		if (pDef->IsLoaded())
		{
			fwStreamedSceneGraphNode* pNode = pDef->m_pObject->GetSceneGraphNode();
			if (pNode->GetIsInWorld())
			{
				gWorldMgr.RemoveSceneNodeFromWorld(pNode);
				Assert(!pNode->GetIsInWorld());
			}

			//////////////////////////////////////////////////////////////////////////
			// make sure entities are now available for dummy conversion etc
			u32 numEntities = pDef->GetNumEntities();
			fwEntity** entities = pDef->GetEntities();
			for (u32 i=0; i<numEntities; i++)
			{
				fwEntity* pEntity = entities[i];
				if (pEntity) { pEntity->ClearBaseFlag(fwEntity::IS_VISIBLE); }
			}
			//////////////////////////////////////////////////////////////////////////
		}
		pDef = pDef->GetParentDef();
	}

	// BOUNDS
	if (index!=-1)
	{
		g_StaticBoundsStore.SetEnabled(name.GetHash(), false);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetEntities
// PURPOSE:		adds required entities for swap to entity list
//////////////////////////////////////////////////////////////////////////
void fwImapGroupSwap::GetEntities(atArray<fwEntity*>& entityList)
{
	INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(m_endIndex));
	while (pDef != NULL)
	{
		fwEntity** entities = pDef->GetEntities();
		u32 numEntities = pDef->GetNumEntities();

		for (u32 i=0; i<numEntities; i++)
		{
			fwEntity* pEntity = entities[i];
			if (pEntity)
			{
				entityList.PushAndGrow(pEntity);
			}
		}
		pDef = pDef->GetParentDef();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AreStartImapsLoaded
// PURPOSE:		returns true if all required imap files for the start state of
//				swap have been successfully loaded, false otherwise
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupSwap::AreStartImapsLoaded() const
{
	INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(m_startIndex));
	while (pDef != NULL)
	{
		if (!pDef->IsLoaded())
		{
			return false;
		}
		pDef = pDef->GetParentDef();
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AreEndImapsLoaded
// PURPOSE:		returns true if all required imap files for the end state of
//				swap have been successfully loaded, false otherwise
//////////////////////////////////////////////////////////////////////////
bool fwImapGroupSwap::AreEndImapsLoaded() const
{
	INSTANCE_DEF* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(m_endIndex));
	while (pDef != NULL)
	{
		if (!pDef->IsLoaded())
		{
			return false;
		}
		pDef = pDef->GetParentDef();
	}
	return true;
}

#if __BANK

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DisplayActiveTransitions
// PURPOSE:		debug widget to show active transitions
//////////////////////////////////////////////////////////////////////////
void fwImapGroupMgr::DisplayActiveTransitions()
{
	for (u32 i=0; i<MAX_CONCURRENT_SWAPS; i++)
	{
		if (m_aSwaps[i].IsActive())
		{
			grcDebugDraw::AddDebugOutputEx(false, "IMAP GROUP SWAP (index %d) from %s to %s (imaps=%s auto=%s loaded=%s)",
				i,
				m_aSwaps[i].GetStartName(), m_aSwaps[i].GetEndName(),
				( m_aSwaps[i].AreEndImapsLoaded() ? "Y" : "N" ),
				( m_aSwaps[i].GetIsAutomatic() ? "Y" : "N" ),
				( m_aSwaps[i].GetIsLoaded() ? "Y" : "N" )
			);
		}
		m_aSwaps[i].Update();
	}
}

#endif	//__BANK

}	//namespace rage
