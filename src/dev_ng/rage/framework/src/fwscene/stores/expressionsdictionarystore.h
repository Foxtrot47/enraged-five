
#ifndef FWSCENE_STORES_EXPRESSIONSDICTIONARY_H
#define FWSCENE_STORES_EXPRESSIONSDICTIONARY_H

// --- Include Files ------------------------------------------------------------

#include "fwtl\assetstore.h"
#include "crextra\expressions.h"


namespace rage {

extern fwAssetRscStore<crExpressionsDictionary> g_ExpressionDictionaryStore;

} // namespace rage


#endif // FWSCENE_STORES_EXPRESSIONSDICTIONARY_H
