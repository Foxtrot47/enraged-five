//
// fwscene/stores/mapdatastore.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "mapdatastore.h"

//rage includes
#include "diag/art_channel.h"
#include "profile/group.h"
#include "profile/page.h"
#include "profile/element.h"
#include "grprofile/ekg.h"
#include "system/param.h"

//fw includes
#include "entity/archetype.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"
#include "fwscene/lod/ContainerLod.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/stores/maptypesstore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/world/worldmgr.h"
#include "fwscene/world/WorldLimits.h"
#include "fwscene/world/scenegraphnode.h"
#include "fwscene/world/streamedscenegraphnode.h"
#include "parser/manager.h"
#include "parser/psoconsts.h"
#include "parser/psofile.h"
#include "profile/group.h"
#include "profile/timebars.h"
#include "system/timer.h"

#include "streaming/cacheloader.h"
#include "streaming/packfilemanager.h"
#include "streaming/streaming_channel.h"
#include "streaming/streamingvisualize.h"
#include "fwrenderer/renderthread.h"

#define IMAP_PRESTREAM_DIST					(30)
#define IMAP_TARGET_TO_REMOVE_PER_FRAME		(5)

PARAM(noinstancedveg, "disable instanced vegatation");
PARAM(instancedveg, "enable instanced vegetation");
PARAM(noocclusion, "disable occluder imap data");

PARAM(inflateimaphigh, "Load high detail IMAPs for larger range");
PARAM(inflateimapmedium, "Load medium detail IMAPs for larger range");
PARAM(inflateimaplow, "Load low detail IMAPs for larger range");

#if !__FINAL
// Profiling
PF_PAGE(CMapDataStore, "Map data store (fw)");

PF_GROUP(Create_MDS);
PF_TIMER(Parse, Create_MDS);
PF_TIMER(Contents, Create_MDS);
PF_TIMER(Creation, Create_MDS);

PF_GROUP(Destroy_MDS);
PF_TIMER(Delete, Destroy_MDS);
PF_TIMER(Cleanup, Destroy_MDS);

PF_LINK(CMapDataStore, Create_MDS);
PF_LINK(CMapDataStore, Destroy_MDS);
#endif //!__FINAL

XPARAM(nolodlights);

namespace rage 
{
atDelegate<bool (strLocalIndex)>* fwMapDataStore::m_getProxyDisabledCB;
atDelegate<void (strLocalIndex, bool)>* fwMapDataStore::m_setProxyDisabledCB;
fwMapDataStore fwMapDataStore::ms_mapDataStore;
atMap< s32, atArray<s32> >* fwMapDataStore::ms_pChildrenMap;
fwMapDataGameInterface* fwMapDataStore::ms_pGameInterface = NULL;

fwMapDataStore::fwMapDataStore() : fwAssetStore<fwMapDataContents, fwMapDataDef>("MapDataStore", MAPDATA_FILE_ID, CONFIGURED_FROM_FILE, 455, true)
{

}

void fwMapDataStore::RegisterStreamingModule()
{
	fwAssetStore<fwMapDataContents, fwMapDataDef>::RegisterStreamingModule();
}

// not doing anything here! just checking that the files I'm expecting are coming through successfully
strLocalIndex fwMapDataStore::Register(const char* name)
{
	strLocalIndex index = strLocalIndex(fwAssetStore<fwMapDataContents, fwMapDataDef>::Register(name));

	fwMapDataDef* pDef = GetSlot(index);
	Assertf(pDef, "No entry at this slot");
	pDef->SetIsValid(true);
	return(index);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		initialises streamer
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::Init(u32 initMode){

	if(initMode == INIT_CORE)
	{
		// initialise the box streamer quadtree
		m_boxStreamer.Init(this, 30.0f);
		m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_MAPDATA_HIGH,				LODTYPES_FADE_DIST+IMAP_PRESTREAM_DIST,				false,	true, false);
		m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_MAPDATA_HIGH_CRITICAL,	LODTYPES_FADE_DIST+IMAP_PRESTREAM_DIST,				false,	true, false);
		m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_MAPDATA_MEDIUM,			LODTYPES_FADE_DIST+IMAP_PRESTREAM_DIST,				false,	true, true);
		m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_MAPDATA_LOW,				LODTYPES_FADE_DIST+IMAP_PRESTREAM_DIST,				false,	true, true);
		InitCacheLoaderModule();


		// Configure the loader
		atFixedBitSet32 flags;
		flags.Set(fwPsoStoreLoader::LOAD_IN_PLACE).Set(fwPsoStoreLoader::WARN_ON_LAYOUT_MISMATCH).Set(fwPsoStoreLoader::ASSERT_ON_LAYOUT_MISMATCH);
		ms_mapDataStore.m_loader.SetFlags(flags);
		ms_mapDataStore.m_loader.SetInPlaceHeap(PSO_DONT_ALLOCATE);
		ms_mapDataStore.m_loader.SetNotInPlaceHeap(PSO_HEAP_DEBUG);
		ms_mapDataStore.m_loader.SetRootType<fwMapData>();
	}
	else if(initMode == INIT_BEFORE_MAP_LOADED)
	{
		strStreamingObjectNameString genericHash("generic");
		s32 index = FindSlotFromHashKey(genericHash.GetHash()).Get();
		if(index==-1)
		{
			index = AddSlot(strLocalIndex(0), genericHash).Get();
			GetSlot(strLocalIndex(0))->SetIsValid(false);
		}
	}

	m_loadedList.Reset();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ShutdownLevelWithMapLoaded
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::ShutdownLevelWithMapLoaded()
{
	m_boxStreamer.Shutdown();
	SafeRemoveAll();

#if !__FINAL && __XENON
	fwMapDataDef* pTargetDef = NULL;
	fwMapDataDef* pTestDef = NULL;

	if (m_loadedList.GetCount() != 0)
	{
		char buf[32];
		char finalStr[256];
		for(s32 i=0; i<m_loadedList.GetCount(); i++)
		{
			fwMapDataLoadedNode* pNode = m_loadedList[i];
			pTargetDef = GetSlot(strLocalIndex(pNode->GetSlotIndex()));

			formatf(finalStr, "map data store object at slot %i is loaded : SafeRemoveAll() left it (%s) (%s)\n", pNode->GetSlotIndex(), GetName(pNode->GetSlotIndex()), GetRefCountString(strLocalIndex(pNode->GetSlotIndex()), buf, NELEM(buf)));
			OutputDebugString(finalStr);

			for(s32 j=0; j<GetCount(); j++)
			{
				pTestDef = GetSlot(strLocalIndex(j));
				if (pTestDef)
				{
					if (pTestDef->GetParentDef() == pTargetDef)
					{
						formatf(finalStr, "		map data store object at slot %i (%s) has this as parent (%s) \n", j, GetName(j), GetRefCountString(strLocalIndex(j), buf, NELEM(buf)));
						OutputDebugString(finalStr);
					}
				}
			}
		}

		Quitf("fwMapDataStore::m_loadedList is not empty after SaveRemoveAll - this is very bad & will result in haywire ptrs!");
	}
#endif //!__FINAL

	m_loadedList.Reset();
	fwMapDataLoadedNode::GetPool()->DeleteAll();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ShutdownLevelWithMapUnloaded
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::ShutdownLevelWithMapUnloaded()
{
	m_boxStreamer.Shutdown();
	Shutdown();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Load
// PURPOSE:		notification from streaming that a requested file is now in memory
//////////////////////////////////////////////////////////////////////////
bool fwMapDataStore::Load(strLocalIndex index, void* pData, int size)
{
	PF_FUNC(Parse);
	const char *debugName = "";
#if !__NO_OUTPUT															
	char debugNameBuffer[RAGE_MAX_PATH];
	debugName = strStreamingEngine::GetInfo().GetObjectName(GetStreamingIndex(index), debugNameBuffer, NELEM(debugNameBuffer));
#endif
	fwPsoStoreLoadInstance loadInst;
	fwPsoStoreLoadResult loadRes;

	loadRes = m_loader.Load(pData, size, debugName, loadInst);

	FinishLoading(index, loadInst);
	return true;
}

void fwMapDataStore::FinishLoading(strLocalIndex index, fwPsoStoreLoadInstance& loadInst)
{
#if !__FINAL
	sysTimer timer;
	float fStartPrelude;
	float fEndPrelude;
	float fStartCreation;
	float fEndCreation;
	float fStartCleanup;
	float fEndCleanup;
#endif

	NOTFINAL_ONLY(fStartPrelude = timer.GetMsTime(); )
	fwMapData*		pMapData	= NULL;
	fwMapDataDef*	pDef		= GetSlot(index);

	pMapData = reinterpret_cast<fwMapData*>(loadInst.GetInstance());

	const bool bRequiresTempMem		= ( pDef->RequiresTempMemory() );
	const bool bFullyInitialised	= ( pDef->GetInitState()==fwMapDataDef::FULLY_INITIALISED );

	PF_FUNC(Contents);
	fwMapDataContents* contents = pMapData->CreateMapDataContents();
	contents->GetLoaderInstRef() = loadInst;
	Set(index, contents);
	pDef->SetIsInPlace(loadInst.IsInPlace());
	NOTFINAL_ONLY(fEndPrelude = timer.GetMsTime(); )

	NOTFINAL_ONLY(fStartCreation = timer.GetMsTime(); )
	if (!bFullyInitialised)
	{
		//////////////////////////////////////////////////////////////////////////
		// first time loading this file (and it wasn't already initialised by cacheloader)
		// so simply extract some info such as streaming bounds etc - don't do full load

		PreLoad(index, pDef, pMapData, contents);

		//////////////////////////////////////////////////////////////////////////
	}
	else
	{
		//////////////////////////////////////////////////////////////////////////
		// process full load of the .imap file - creating entities etc

		PF_FUNC(Creation);
		contents->Load( pDef, pMapData, index);

		// increment ref count in parent
		fwMapDataDef* pParentDef = pDef->GetParentDef();
		if (pParentDef)
		{
			pParentDef->AddChild();
		}

		AddToLoadedList(index, pDef);

		//////////////////////////////////////////////////////////////////////////
	}
	NOTFINAL_ONLY(fEndCreation = timer.GetMsTime(); )

	NOTFINAL_ONLY(fStartCleanup = timer.GetMsTime(); )
	if (!bFullyInitialised || bRequiresTempMem)
	{
		// Delete all the buffers again.
		pMapData->ReleaseDeviceResources();

		// delete pso allocs immediately. the streaming mem will be reclaimed automatically
		m_loader.Unload(contents->GetLoaderInstRef());
	}

	AddRef(index, REF_OTHER);			// protect against streaming cleanup removing .imap files
	NOTFINAL_ONLY(fEndCleanup = timer.GetMsTime(); )

#if !__FINAL
	if (pDef && timer.GetMsTime()>=2000.0f)
	{
		Displayf("*** fwMapDataStore::FinishLoading(%s) took %4.2f ms", pDef->m_name.GetCStr(), timer.GetMsTime());
		Displayf("**** Time spent in prelude: %4.2f ms", fEndPrelude-fStartPrelude);
		Displayf("**** Time spent in creation: %4.2f ms", fEndCreation-fStartCreation);
		Displayf("**** Time spent in cleanup: %4.2f ms", fEndCleanup-fStartCleanup);
	}
#endif
}

void fwMapDataStore::PlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex), strStreamingLoader::StreamingFile& file, datResourceInfo& resInfo)
{
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
	streamAssertf(pInfo->GetStatus() == STRINFO_LOADING, "Info status is %d", pInfo->GetStatus());
	pInfo->SetFlag(file.m_Index, STRFLAG_INTERNAL_PLACING);
	strStreamingEngine::GetLoader().RequestAsyncPlacement(file, file.m_LoadingMap, resInfo);
}

void fwMapDataStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header)
{
	#define DELAY_DELETE (1 && FREE_PHYSICAL_RESOURCES)

	psoResourceData* resource;
	pgRscBuilder::PlaceStream(resource, header, map, "PSO resource", false, !DELAY_DELETE); // Delay physical deallocation

	const char *debugName = "";
#if !__NO_OUTPUT															
	char debugNameBuffer[RAGE_MAX_PATH];
	debugName = strStreamingEngine::GetInfo().GetObjectName(GetStreamingIndex(index), debugNameBuffer, NELEM(debugNameBuffer));
#endif
	fwPsoStorePlacementCookies::Cookie cookie;
	/*fwPsoStoreLoadResult loadResult = */m_loader.LoadResource(resource, debugName, cookie.m_Instance);

	g_PsoPlacementCookies.Add(GetStreamingIndex(index), cookie);
#if DELAY_DELETE && RSG_PC && FREE_PHYSICAL_RESOURCES // Enable if delay is enabled
	pgRscBuilder::Cleanup(map);
	map.Reset();
#endif // RSG_PC
}


void fwMapDataStore::SetResource(strLocalIndex index, datResourceMap& /*map*/)
{
#if !__FINAL
	sysTimer timer;
	float fStartGetCookie = 0.0f;
	float fEndGetCookie = 0.0f;
	float fStartFinishLoading = 0.0f;
	float fEndFinishLoading = 0.0f;
	float fStartRemoveCookie = 0.0f;
	float fEndRemoveCookie = 0.0f;	
#endif

	NOTFINAL_ONLY(fStartGetCookie = timer.GetMsTime(); )
	fwPsoStorePlacementCookies::Cookie cookie = g_PsoPlacementCookies.Get(GetStreamingIndex(index));
	NOTFINAL_ONLY(fEndGetCookie = timer.GetMsTime(); )

	NOTFINAL_ONLY(fStartFinishLoading = timer.GetMsTime(); )
	FinishLoading(index, cookie.m_Instance);
	NOTFINAL_ONLY(fEndFinishLoading = timer.GetMsTime(); )

	NOTFINAL_ONLY(fStartRemoveCookie = timer.GetMsTime(); )
	g_PsoPlacementCookies.Remove(GetStreamingIndex(index));
	NOTFINAL_ONLY(fEndRemoveCookie = timer.GetMsTime(); )

#if !__FINAL
	if (timer.GetMsTime() > 2000.0f)
	{
		Displayf("* fwMapDataStore::SetResource(%s) took %4.2f ms to complete!", GetName(index), timer.GetMsTime());
		Displayf("** time spent retrieving fwPsoStorePlacementCookie: %4.2f ms", fEndGetCookie-fStartGetCookie);
		Displayf("** time spent in FinishLoading: %4.2f ms", fEndFinishLoading-fStartFinishLoading);
		Displayf("** time spent removing fwPsoStorePlacementCookie: %4.2f ms", fEndRemoveCookie-fStartRemoveCookie);
	}
#endif
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequiresTempMemory
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
bool fwMapDataStore::RequiresTempMemory(strLocalIndex index) const
{
	const fwMapDataDef* pDef = GetSlot(index);
	fwMapDataContents* pMapData = Get(index);

	// If the LOADER owns the storage, that means the STREAMER does not. Therefore the streamed
	// memory is considered "temp". If the loader thinks the file is not loaded, the streamer
	// should clean up any memory it holds too.
	if (pMapData && (!pMapData->GetLoaderInstRef().IsLoaded() || pMapData->GetLoaderInstRef().OwnsStorage()))
	{
		return true;
	}
	if (pDef && pDef->GetIsValid() && pDef->GetInitState()==fwMapDataDef::FULLY_INITIALISED)
	{
		return pDef->RequiresTempMemory();
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Remove
// PURPOSE:		stream out a map data slot
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::Remove(strLocalIndex index){

	fwMapDataDef* pDef = GetSlot(index);
	if (pDef)
	{
		//Assertf(!pDef->GetPostponeWorldAdd(), "Trying to delete %s which is not in world - rayfire anim was not shut down properly by script", pDef->m_name.GetCStr());
		Assert(GetNumRefs(index)==0);
		Assertf(!pDef->GetNumLoadedChildren(), "Trying to delete %s which is a dependency of another currently loaded mapdata entry", pDef->m_name.GetCStr());

		PF_FUNC(Delete);
		RemoveFromLoadedList(index, pDef);
		pDef->m_pObject->Remove(index);
		m_loader.Unload(pDef->GetContents()->GetLoaderInstRef(), true);
		fwAssetStore<fwMapDataContents, fwMapDataDef>::Remove(index);
		pDef->SetPostponeWorldAdd(false);

		// decrement ref count in parent
		PF_FUNC(Cleanup);
		fwMapDataDef* pParentDef = pDef->GetParentDef();
		if (pParentDef && pDef->GetInitState()==fwMapDataDef::FULLY_INITIALISED)
		{
			Assertf(
				pParentDef->GetNumLoadedChildren(),
				"fwMapData: removing %s and trying to decrement refcount in %s, but it is %d",
				pDef->m_name.GetCStr(), pParentDef->m_name.GetCStr(), pParentDef->GetNumLoadedChildren()
			);
			pParentDef->RemoveChild();
		}

		if (pDef->GetInitState()==fwMapDataDef::FULLY_INITIALISED)
		{
			// any .ityp dependencies need to hang around a little bit longer, until refs to the archetypes flush through the render thread
			for(u32 i=0; i<pDef->GetNumParentTypes(); i++){
				gDrawListMgr->AddTypeFileReference(pDef->GetParentTypeIndex(i).Get());
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SafeRemove
// PURPOSE:		reduces ref count and calls remove
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::SafeRemove(strLocalIndex index)
{
	if (HasObjectLoaded(index))
	{
		Assertf(GetNumRefs(index)==1, "Removing an IMAP %s with ref count %d", GetName(index), GetNumRefs(index));
		RemoveRef(index, REF_OTHER);
		if (!StreamingRemove(index))
		{
			AddRef(index, REF_OTHER);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	IsScriptManaged
// PURPOSE:		returns true if the specified slot corresponds to a script-managed imap
//				file (aka an "IMAP group")
//////////////////////////////////////////////////////////////////////////
bool fwMapDataStore::IsScriptManaged(strLocalIndex slotIndex)
{
	fwMapDataDef* pDef = GetSlot(slotIndex);
	return pDef ? pDef->GetIsScriptManaged() : false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PreLoad
// PURPOSE:		certain imap files requires a preload - a load that constructs no
//				entities but extracts mlo instances, container lod mappings etc
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::PreLoad(strLocalIndex index, fwMapDataDef* pDef, fwMapData* pMapData, fwMapDataContents* pContents)
{
	pDef->InitFromMapData( index.Get(), pMapData, m_boxStreamer.GetBounds(index.Get()), m_boxStreamer.GetStreamingBounds(index.Get()) );

	if (pDef->GetContentFlags() & fwMapData::CONTENTFLAG_MLO)
	{
		pContents->ConstructInteriorProxies(pDef, pMapData, index);
	}

#if __BANK
	pContents->LoadDebug(pDef, pMapData);
#endif	//__BANK
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddSlot
// PURPOSE:		adds a new slot with the specified name
//////////////////////////////////////////////////////////////////////////
strLocalIndex fwMapDataStore::AddSlot(const strStreamingObjectNameString name)
{
	strLocalIndex index = fwAssetStore<fwMapDataContents, fwMapDataDef>::AddSlot(name);
	m_boxStreamer.InitSlot(index.Get(), fwBoxStreamerAsset::ASSET_MAPDATA_HIGH);	// assume high detail by default
	SetStreamable(strLocalIndex(index), false);						// assume not streamable by default
	return index;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddSlot
// PURPOSE:		adds a new slot with the specified name and index
//////////////////////////////////////////////////////////////////////////
strLocalIndex fwMapDataStore::AddSlot(strLocalIndex index, const strStreamingObjectNameString name)
{
	fwAssetStore<fwMapDataContents, fwMapDataDef>::AddSlot(index, name);
	m_boxStreamer.InitSlot(index.Get(), fwBoxStreamerAsset::ASSET_MAPDATA_HIGH);	// assume high detail by default
	return strLocalIndex(index);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Update
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::Update()
{
	STRVIS_AUTO_CONTEXT(strStreamingVisualize::MAPDATASTORE);

	m_swapper.Update();
	RemoveUnrequired(true);
	m_swapper.UpdateCleanup();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CompareDepthCB
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
s32 fwMapDataStore::CompareDepthCB(fwMapDataLoadedNode * const * ppNode1, fwMapDataLoadedNode * const * ppNode2)
{
	const fwMapDataLoadedNode* pNode1 = *ppNode1;
	const fwMapDataLoadedNode* pNode2 = *ppNode2;
	return (s32) pNode2->GetDepth() - (s32) pNode1->GetDepth();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveUnrequired
// PURPOSE:		unloads any mapdatas which are not marked as required
//				(ie due to being out of range)
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::RemoveUnrequired(bool bAll)
{
	PF_PUSH_TIMEBAR_DETAIL("RemoveUnrequired");

	// compact loaded list
	for (s32 i=0; i<m_loadedList.GetCount(); i++)
	{
		fwMapDataLoadedNode* pNode = m_loadedList[i];
		if (pNode && pNode->IsMarkedDelete())
		{
			delete pNode;
			m_loadedList[i] = NULL;
			m_loadedList.DeleteFast(i);
			i--;
		}
	}

#if __ASSERT && 0
	for (s32 i=0; i<m_loadedList.GetCount(); i++)
	{
		Assert(m_loadedList[i] != NULL);
	}
#endif

	u32 numRemoved = 0;


	// first, sort the loaded list by depth in dependency hierarchy
	m_loadedList.QSort(0, -1, CompareDepthCB);

	// then run over the loaded files and try to remove
	for (s32 i=0; i<m_loadedList.GetCount(); i++)
	{
		fwMapDataLoadedNode* pNode = m_loadedList[i];
		strLocalIndex slotIndex = strLocalIndex(m_loadedList[i]->GetSlotIndex());

		fwMapDataDef* pDef = GetSlot(slotIndex);

#if __XENON
		// PIX reports a big cache miss here, which is a good win for 360.  Negative on PS3 though. - sls
		PrefetchObject(pDef);
#endif
		if ( !pNode->IsMarkedDelete()
			&& pDef->IsLoaded()
			&& !pDef->GetNumLoadedChildren()
			&& m_boxStreamer.CanRemoveAsset(slotIndex.Get())
			&& !m_swapper.IsUsingImap(slotIndex.Get())
			&& ( !(GetStreamingFlags(slotIndex)&STR_DONTDELETE_MASK) || m_boxStreamer.GetIsCulled(slotIndex.Get()) ) 
			&& ( GetNumRefs(slotIndex) == 1 )
			&& ( GetStreamingInfo(slotIndex)->GetDependentCount()==0 ) )
		{
			numRemoved++;

			ClearRequiredFlag(slotIndex.Get(), STRFLAG_DONTDELETE);
			SafeRemove(strLocalIndex(slotIndex));

			if (!bAll && numRemoved>=IMAP_TARGET_TO_REMOVE_PER_FRAME)
				break;
		}
	}

	PF_POP_TIMEBAR_DETAIL();
}

void fwMapDataStore::SaveScriptImapStates()
{
	s32 scriptImaps = 0;

	for (s32 i = 0; i < GetCount(); i++)
	{
		if (fwMapDataDef* nodeDef = GetSlot(strLocalIndex(i)))
		{
			if (nodeDef->GetIsScriptManaged())
				scriptImaps++;
		}
	}

	m_scriptImapSnapShot.Reset();
	m_scriptImapSnapShot.Reserve(scriptImaps);

	for (s32 i = 0; i < GetCount(); i++)
	{
		if (fwMapDataDef* nodeDef = GetSlot(strLocalIndex(i)))
		{
			const strStreamingInfo* strInfo = GetStreamingInfo(strLocalIndex(i));

			// Cache script controlled imap state so we can restore it.
			if (strInfo->IsInImage() && nodeDef->GetIsValid() && nodeDef->GetIsScriptManaged())
			{
				SScriptImapState imapToStore;

				imapToStore.m_slotIndex = strLocalIndex(i);
				imapToStore.m_isIgnored = m_boxStreamer.GetIsIgnored(i);
				imapToStore.m_flags = GetStreamingFlags(strLocalIndex(i));
				imapToStore.m_proxyDisabled = false;

				if (Verifyf(m_getProxyDisabledCB->IsBound(), "SaveScriptImapStates - m_getProxyDisabledCB is not bound!") && 
					(GetSlot(strLocalIndex(i))->GetContentFlags() & fwMapData::CONTENTFLAG_MLO))
				{
					imapToStore.m_proxyDisabled = m_getProxyDisabledCB->Invoke(strLocalIndex(i));
				}

				m_scriptImapSnapShot.Append() = imapToStore;
			}
		}
	}
}

void fwMapDataStore::RestoreScriptImapStates()
{
	// Initialise any new entries since the snapshot was taken
	for (s32 i = 0; i < GetCount(); i++)
	{
		if (fwMapDataDef* pDef = GetSlot(strLocalIndex(i)))
		{
			SScriptImapState tempState;

			tempState.m_slotIndex = strLocalIndex(i);

			if (pDef->GetIsScriptManaged() && m_scriptImapSnapShot.Find(tempState) == -1)
				RemoveGroup(tempState.m_slotIndex, pDef->m_name.GetHash());
		}
	}

	for (s32 i = 0; i < m_scriptImapSnapShot.GetCount(); i++)
	{
		strLocalIndex slotIndex = m_scriptImapSnapShot[i].m_slotIndex;
		s32 flags = m_scriptImapSnapShot[i].m_flags;
		bool isIgnored = m_scriptImapSnapShot[i].m_isIgnored;
		fwMapDataDef* nodeDef = GetSlot(slotIndex);
		const strStreamingInfo* strInfo = GetStreamingInfo(slotIndex);

		// Check in image so we don't try to restore something that has been removed.
		if (strInfo->IsInImage())
		{
			if (Verifyf(m_setProxyDisabledCB->IsBound(), "RestoreScriptImapStates - m_setProxyDisabledCB is not bound!") && 
				(GetSlot(slotIndex)->GetContentFlags() & fwMapData::CONTENTFLAG_MLO))
			{
				m_setProxyDisabledCB->Invoke(slotIndex, m_scriptImapSnapShot[i].m_proxyDisabled);
			}

			if (isIgnored)
			{
				RemoveGroup(slotIndex, nodeDef->m_name.GetHash());
			}
			else
			{
				RequestGroup(slotIndex, nodeDef->m_name.GetHash(), false);
			}

			SetFlag(slotIndex, flags);
		}
	}

	m_scriptImapSnapShot.Reset();
}

bool fwMapDataStore::ModifyHierarchyStatus(strLocalIndex index, eHierarchyModType modType)
{
	//PerformStreamingIntegrityCheck();

	if (index != -1 && IsValidSlot(index) && Verifyf(ms_pChildrenMap, "ModifyHierarchyStatus - Parent child map is not initialised!"))
	{
		fwMapDataDef* pRootNodeDef = GetSlot(index);

		if (pRootNodeDef)
		{
			m_boxStreamer.ClearRequiredList();

			fwMapDataDef* pParentDef = pRootNodeDef->GetParentDef();

			while (pParentDef)
			{
				pRootNodeDef = pParentDef;
				pParentDef = pParentDef->GetParentDef();
			}

			ModifyHierarchyStatusRecursive(GetSlotIndex(pRootNodeDef), modType);
		}
	}

	return true;
}

void fwMapDataStore::ModifyHierarchyStatusRecursive(strLocalIndex slotIndex, eHierarchyModType modType)
{
	if (slotIndex.IsValid() && IsValidSlot(slotIndex) && Verifyf(ms_pChildrenMap, "ModifyHierarchyStatusRecursive - Parent child map is not initialised!"))
	{
		fwMapDataDef* pRootNodeDef = GetSlot(slotIndex);

		if (modType == HMT_DISABLE)
		{
			if (m_boxStreamer.GetIsIgnored(slotIndex.Get()) && !pRootNodeDef->IsLoaded())
				return;
		}
		else if (modType == HMT_ENABLE)
		{
			if (!m_boxStreamer.GetIsIgnored(slotIndex.Get()))
				return;
		}

		atArray<s32>* pChildren = ms_pChildrenMap->Access(slotIndex.Get());

		if (pChildren)
		{
			s32 count = pChildren->GetCount();

			for (s32 i = 0; i < count; i++)
				ModifyHierarchyStatusRecursive(strLocalIndex((*pChildren)[i]), modType);
		}

		if (modType == HMT_DISABLE || modType == HMT_FLUSH)
		{
			ClearRequiredFlag(slotIndex.Get(), STRFLAG_DONTDELETE);

			if (GetNumRefs(slotIndex) > 1)
				gRenderThreadInterface.Flush();
			
			SafeRemove(slotIndex);
		}

		if (modType != HMT_FLUSH)
		{
			if (Verifyf(m_setProxyDisabledCB->IsBound(), "ModifyHierarchyStatusRecursive - m_interiorDisableCB is not bound!") && 
				(GetSlot(slotIndex)->GetContentFlags() & fwMapData::CONTENTFLAG_MLO))
			{
				m_setProxyDisabledCB->Invoke(slotIndex, modType == HMT_DISABLE);
			}

			m_boxStreamer.SetIsIgnored(slotIndex.Get(), modType == HMT_DISABLE);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PerformStreamingIntegrityCheck
// PURPOSE:		Check that the streaming system and MapDataStore are on 
//				the same page w.r.t. whether or not slots are NULL
// RETURNS:		True if everything is OK, Quitf otherwise.
// DISCUSSION:	Ideally we'd just return false on fail and the caller
//				could decide what to do. But, we're getting possible
//				double-free bugs (B*2220617) so we want to catch them
//				INSIDE this function in release bug reports.
//////////////////////////////////////////////////////////////////////////
bool fwMapDataStore::PerformStreamingIntegrityCheck() {
	s32 storeSize = GetSize();

	for (s32 slot = 0; slot < storeSize; ++slot) {
		strLocalIndex slotIdx = strLocalIndex(slot);
		fwMapDataDef *pDef = GetSlot(slotIdx);

		// Map Data Store slot not valid
		if (!slotIdx.IsValid() || !pDef || pDef->GetIsValid()) {
			continue;
		}

		// If NULL in slot, but streaming thinks slot has loaded, we got a problem!
		strIndex globalIdx = GetStreamingIndex(slotIdx);
		strStreamingInfo *info = strStreamingEngine::GetInfo().GetStreamingInfo(globalIdx);
		bool bObjectIsLoaded = info->GetStatus() == STRINFO_LOADED;
		
		if (!pDef->m_pObject && bObjectIsLoaded) {
#if !__NO_OUTPUT
			char bufObjName[RAGE_MAX_PATH] = { 0 };
			char const *szObjName = strStreamingEngine::GetInfo().GetObjectName(globalIdx, bufObjName, RAGE_MAX_PATH);

			Quitf(ERR_STR_FAILURE_3, "strStreamingEngine thinks slot %i is loaded but fwMapDataStore doesn't! Object name %s",
				slotIdx.Get(), szObjName);
#else
			Quitf(ERR_STR_FAILURE_3, "strStreamingEngine thinks slot %i is loaded but fwMapDataStore doesn't!",
				slotIdx.Get());
#endif
		}
	}
	
	return true;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	FixupAllEntriesAfterRegistration
// PURPOSE:		all files are registered, cache load is finished, but some IMAPs require
//				a full preload to extract info such as interior instances etc.
//				We also take this opportunity to populate boxstreamer entries and build up
//				the world limits AABB
//////////////////////////////////////////////////////////////////////////
#if __WIN32PC && !__FINAL
PARAM(ExportScriptStaticBounds, "enable script-managed static bounds export, used for heightmap tool");
#endif // __WIN32PC && !__FINAL
void fwMapDataStore::FixupAllEntriesAfterRegistration(bool bResetGroupState)
{
	g_WorldLimits_MapDataExtentsAABB.Invalidate();

	PF_START_STARTUPBAR("Get bounding boxes");

	fwMapDataDef* pDef = NULL;
	s32 i = 0;
	s32 storeSize = GetSize();
	s32 requestCount = 0;
	s32 maxReqCount = strStreamingEngine::GetInfo().GetRequestInfoPoolCapacity() - 
		strStreamingEngine::GetInfo().GetRequestInfoPoolCount();

	maxReqCount = maxReqCount < 0 ? 0 : maxReqCount;
	maxReqCount /= 2;

	for(i=0; i<storeSize; i++)
	{
		pDef = GetSlot(strLocalIndex(i));
		if(pDef && pDef->GetIsValid())
		{
			if (pDef->GetInitState()!=fwMapDataDef::FULLY_INITIALISED)
			{
				StreamingRequest(strLocalIndex(i), STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD);
				requestCount++;
			}
		}

		if ( requestCount > maxReqCount )
		{
			strStreamingEngine::GetLoader().LoadAllRequestedObjects();
			requestCount = 0;
			maxReqCount = strStreamingEngine::GetInfo().GetRequestInfoPoolCapacity() - 
				strStreamingEngine::GetInfo().GetRequestInfoPoolCount();
			maxReqCount = maxReqCount < 0 ? 0 : maxReqCount;
			maxReqCount /= 2;
		}
	}

	if (requestCount > 0)
		strStreamingEngine::GetLoader().LoadAllRequestedObjects();

	for(i=0; i<storeSize; i++)
	{
		pDef = GetSlot(strLocalIndex(i));

		if(pDef && pDef->GetIsValid())
		{
			if (pDef->GetInitState()!=fwMapDataDef::FULLY_INITIALISED)
			{
				SafeRemove(strLocalIndex(i));
				pDef->SetInitState(fwMapDataDef::FULLY_INITIALISED);
			}

			SetBoxStreamerSlot(strLocalIndex(i), pDef);

			if (bResetGroupState && pDef->GetIsScriptManaged() && (pDef->GetContentFlags()&fwMapData::CONTENTFLAG_ENTITIES_HD)
#if __WIN32PC && !__FINAL
				&& !PARAM_ExportScriptStaticBounds.Get()
#endif // __WIN32PC && !__FINAL
				)
			{
				g_StaticBoundsStore.SetEnabled(pDef->m_name.GetHash(), false);			// init bounds for IMAP groups to off
			}

			g_WorldLimits_MapDataExtentsAABB.GrowAABB( GetPhysicalBounds(strLocalIndex(i)) );			// add entity extents to world limits AABB

			STRVIS_REGISTER_MAPDATA(g_MapDataStore.GetStreamingIndex(strLocalIndex(i)), *pDef);

			pDef->CalcDepth();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// patch around some art bugs - this code should be invalidated if the map data is changed
	// as part of DLC
	if (1)
	{
		ProcessPostLaunchHacks();
	}
	//////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ProcessPostLaunchHacks
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::ProcessPostLaunchHacks()
{
	for (s32 i=0; i<GetSize(); i++)
	{
		fwMapDataDef* pDef = GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid() && (pDef->GetContentFlags()&fwMapData::CONTENTFLAG_DISTANT_LOD_LIGHTS)!=0)
		{
			spdAABB& streamingBounds = m_boxStreamer.GetStreamingBounds(i);
			streamingBounds.GrowAABB( g_WorldLimits_MapDataExtentsAABB );
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetDependencies
// PURPOSE:		returns the parent IPL, if there is one
//////////////////////////////////////////////////////////////////////////
int fwMapDataStore::GetDependencies(strLocalIndex index, strIndex* pIndices, int indexArraySize) const
{
	s32 count = 0;

	const fwMapDataDef* pDef = GetSlot(index);

	// deps are not known until the def has been initialised
	if (pDef && pDef->GetInitState()==fwMapDataDef::FULLY_INITIALISED)
	{
		// parent map data file
		if (pDef)
		{
			fwMapDataDef* pParentDef = pDef->GetParentDef();
			if (pParentDef)
			{
				strLocalIndex parentSlot = GetSlotIndex(pParentDef);
				AddDependencyOutput(pIndices, count, GetStreamingIndex(parentSlot), indexArraySize, GetStreamingIndex(strLocalIndex(indexArraySize)));
			}	
		}

		u32 numTypeFileDependencies = pDef->GetNumParentTypes();
		for(u32 i =0; i<numTypeFileDependencies; i++){
			strLocalIndex parentTypeIndex = pDef->GetParentTypeIndex(i);
			AddDependencyOutput(pIndices, count, g_MapTypesStore.GetStreamingModule()->GetStreamingIndex(parentTypeIndex), indexArraySize, GetStreamingIndex(strLocalIndex(indexArraySize)));
		}
	}

	return count;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetStreamable
// PURPOSE:		sets whether the specified map data can be dynamically streamed
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::SetStreamable(strLocalIndex slotIndex, bool bEnabled)
{
	m_boxStreamer.SetIsIgnored(slotIndex.Get(), !bEnabled);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetBoxStreamerSlot
// PURPOSE:		validates box streamer slot bounds and sets asset type
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::SetBoxStreamerSlot(strLocalIndex index, fwMapDataDef* pDef)
{
#if __ASSERT
	//////////////////////////////////////////////////////////////////////////
	// check the box streamer slot has valid bounds set already by now
	const spdAABB& physicalBounds = GetPhysicalBounds(index);
	const spdAABB& streamingBounds = GetStreamingBounds(index);
	artAssertf(physicalBounds.IsValid(), "%s has invalid bounds", pDef->m_name.GetCStr());
	artAssertf(streamingBounds.IsValid(), "%s has invalid streaming bounds", pDef->m_name.GetCStr());
	if (!streamingBounds.ContainsAABB(physicalBounds) && streamingBounds!=physicalBounds)
	{
		//Displayf("IMAP file %s has streaming bounds that do not encompass physical bounds", pDef->m_name.GetCStr());
		//artAssertf(false, "%s has smaller streaming bounds than entity bounds. CHECK LOD DISTANCES AND REEXPORT MAPSECTION", pDef->m_name.GetCStr());

		// patch around screwed IMAP data - this is currently bugged for tools in B* 1423838
		m_boxStreamer.GetStreamingBounds(index.Get()) = physicalBounds;
	}
	//////////////////////////////////////////////////////////////////////////
#endif	//__ASSERT

	// each imap is considered high detail unless it contains lods or container lods
	const u32 lowDetailMask =
		fwMapData::CONTENTFLAG_ENTITIES_CONTAINERLOD	|
		fwMapData::CONTENTFLAG_LOD_LIGHTS				|
		fwMapData::CONTENTFLAG_DISTANT_LOD_LIGHTS		|
		0;

	const u32 criticalMask =
		fwMapData::CONTENTFLAG_ENTITIES_CRITICAL		|
		fwMapData::CONTENTFLAG_INSTANCE_LIST			|
		0;

	u16 assetType = fwBoxStreamerAsset::ASSET_MAPDATA_HIGH;
	if ((pDef->GetContentFlags() & fwMapData::CONTENTFLAG_ENTITIES_LOD) != 0)
	{
		assetType = fwBoxStreamerAsset::ASSET_MAPDATA_MEDIUM;
	}
	else if ((pDef->GetContentFlags() & lowDetailMask)!=0)
	{
		assetType = fwBoxStreamerAsset::ASSET_MAPDATA_LOW;
	}
	else if ((pDef->GetContentFlags() & criticalMask) !=0)
	{
		assetType = fwBoxStreamerAsset::ASSET_MAPDATA_HIGH_CRITICAL;
	}

	m_boxStreamer.SetAssetType(index.Get(), assetType);

#if !__FINAL
	
	if (PARAM_noinstancedveg.Get())
	//if (!PARAM_instancedveg.Get())
	{
		if ((pDef->GetContentFlags() & fwMapData::CONTENTFLAG_INSTANCE_LIST) != 0)
		{
			SetStreamable(index, false);
		}
	}

	if (PARAM_noocclusion.Get())
	{
		if ((pDef->GetContentFlags() & fwMapData::CONTENTFLAG_OCCLUDER) != 0)
		{
			SetStreamable(index, false);
		}
	}
#endif
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CacheParentChildLink
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::CacheParentChildLink(strLocalIndex parentIndex, strLocalIndex childIndex)
{
	if (!ms_pChildrenMap)
	{
		ms_pChildrenMap = rage_new atMap< s32, atArray<s32> >();
	}

	atArray<s32>& children = (*ms_pChildrenMap)[parentIndex.Get()];
	children.PushAndGrow(childIndex.Get());
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CreateChildrenCache
// PURPOSE:		fills an atmap with parent-children links for faster testing during
//				cull box load - this avoids the slightly dumber brute force method
//				that was very slow indeed
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::CreateChildrenCache()
{
	// populate cached parent child links used by cull box loading (and later discarded)
	for (s32 i=0; i<GetSize(); i++)
	{
		fwMapDataDef* pDef = GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid() && pDef->GetParentDef())
		{
			CacheParentChildLink(strLocalIndex(GetSlotIndex(pDef->GetParentDef())), strLocalIndex(i));
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetChildren
// PURPOSE:		adds slot indices for any map data with the specified parent IPL
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::GetChildren(strLocalIndex slotIndex, atArray<s32>& results)
{
	Assert(ms_pChildrenMap);
	fwMapDataDef* pParentDef = GetSlot(slotIndex);
	Assert(pParentDef);

	if (pParentDef->GetIsParent())
	{
		atArray<s32>* pChildren = ms_pChildrenMap->Access(slotIndex.Get());
		if (pChildren)
		{
			results = *pChildren;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetEnabledIplGroups
// PURPOSE:		returns a list of IPL groups which are currently enabled
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::GetEnabledIplGroups(atArray<u32> &enabledList, bool ignoreCodeManagedCodeCheck)
{
	enabledList.Reset();

	for(s32 i=0; i<GetSize(); i++)
	{
		strLocalIndex slotIndex(i);
		fwMapDataDef* pDef = GetSlot(slotIndex);

		if ( pDef && pDef->GetIsValid()
			&& !pDef->GetIsParent()
			&& pDef->GetIsScriptManaged() && (!pDef->GetIsCodeManaged() || ignoreCodeManagedCodeCheck)
			&& GetIsStreamable(slotIndex) )
		{
			enabledList.Grow() = pDef->m_name.GetHash();
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetEnabledIpGroups
// PURPOSE:		allows bulk enabling / disabling of script-controlled IPL groups
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::SetEnabledIplGroups(const atArray<u32>& enabledList, bool ignoreCodeManagedCodeCheck)
{
	atMap<u32, s32> enabledMap;

	// build up sorted array of slots to be enabled
	for (s32 i=0; i<enabledList.GetCount(); i++)
	{
		const u32 nameHash = enabledList[i];
		strLocalIndex slotIndex = FindSlotFromHashKey(nameHash);
		if (slotIndex.IsValid())
		{
			enabledMap.Insert( nameHash, slotIndex.Get() );
		}
	}

	// run over all IPL groups (script controlled only) and change state as required
		for(s32 i=0; i<GetSize(); i++)
	{
		strLocalIndex slotIndex(i);
		fwMapDataDef* pDef = GetSlot(slotIndex);

		if ( pDef && pDef->GetIsValid()
			&& !pDef->GetIsParent()
			&& pDef->GetIsScriptManaged() && (!pDef->GetIsCodeManaged() || ignoreCodeManagedCodeCheck)
			&& m_swapper.IsUsingImap(i) == false )			// Don't mess with any that're the subject of a group swap
		{
			const bool bShouldBeEnabled = ( enabledMap.Access( pDef->m_name.GetHash() ) != NULL );

			if ( GetIsStreamable(slotIndex) )
			{
				if (!bShouldBeEnabled)
				{
					RemoveGroup(slotIndex, pDef->m_name );
				}
			}
			else
			{
				if (bShouldBeEnabled)
				{
					RequestGroup(slotIndex, pDef->m_name, false);
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequestGroup
// PURPOSE:		used by script to enable dynamic streaming for a script-managed
//				map section (aka an "IMAP group")
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::RequestGroup(strLocalIndex index, const u32 nameHash, bool bIssueRequests)
{
	if (index != -1)
	{
		Assertf(GetSlot(index)->GetIsScriptManaged(), "Requesting an imap group (%s), but it is set to be dynamically streamed", GetName(index));

		// enable box streamer to dynamically request specified imap and its parent when within range
		SetStreamable(index, true);
		strLocalIndex parentIndex = GetParentSlot(index);
		if (parentIndex != -1)
		{
			SetStreamable(parentIndex, true);

			strLocalIndex grandparentIndex = GetParentSlot(parentIndex);
			if (grandparentIndex != -1)
			{
				SetStreamable(grandparentIndex, true);
			}
		}

		// enable corresponding static collisions to dynamically stream when within range
		if (!GetSlot(index)->GetPostponeWorldAdd())
		{
			g_StaticBoundsStore.SetEnabled(nameHash, true);
		}

		if (bIssueRequests)
		{
			StreamingRequest(index, STRFLAG_DONTDELETE);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveGroup
// PURPOSE:		used by script to remove an imap not normally dynamically streamed in/out
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::RemoveGroup(strLocalIndex index, const u32 nameHash)
{
	if (index != -1)
	{
		Assertf(GetSlot(index)->GetIsScriptManaged(), "Removing an imap group (%s), but it is set to be dynamically streamed", GetName(index));

		SetStreamable(index, false);
		ClearRequiredFlag(index.Get(), STRFLAG_DONTDELETE);
		SafeRemove(index);
		m_boxStreamer.AddToDirtyList(index.Get());			// in case it is re-requested in the same frame!

		// check for imap group LOD parent
		strLocalIndex parentIndex = GetParentSlot(index);
		if (parentIndex != -1)
		{
			SetStreamable(parentIndex, false);
			ClearRequiredFlag(parentIndex.Get(), STRFLAG_DONTDELETE);
			SafeRemove(parentIndex);
			m_boxStreamer.AddToDirtyList(parentIndex.Get());	// in case it is re-requested in the same frame!

			strLocalIndex grandparentIndex = GetParentSlot(parentIndex);
			if (grandparentIndex != -1)
			{
				SetStreamable(grandparentIndex, false);
				ClearRequiredFlag(grandparentIndex.Get(), STRFLAG_DONTDELETE);
				SafeRemove(grandparentIndex);
				m_boxStreamer.AddToDirtyList(grandparentIndex.Get());	// in case it is re-requested in the same frame!
			}
		}

		g_StaticBoundsStore.SetEnabled(nameHash, false);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetParentSlot
// PURPOSE:		returns slot index of parent IMAP, or -1
//////////////////////////////////////////////////////////////////////////
strLocalIndex fwMapDataStore::GetParentSlot(strLocalIndex index) const
{
	const fwMapDataDef* pDef = GetSlot(index);
	Assert(pDef);
	const fwMapDataDef* pParent = pDef->GetParentDef();
	if (pParent)
	{
		return GetSlotIndex(pParent);
	}
	return strLocalIndex(-1);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetEntity
// PURPOSE:		utility for pulling a specific entity out of an imap, if it is loaded
//////////////////////////////////////////////////////////////////////////
fwEntity* fwMapDataStore::GetEntity(strLocalIndex slotIndex, strLocalIndex entityIndex)
{
	fwEntity* pEntity = NULL;
	fwMapDataDef* pDef = GetSlot(slotIndex);
	if (pDef && pDef->IsLoaded())
	{
		fwMapDataContents* pContents = pDef->GetContents();
		fwEntity** entities = pContents->GetEntities();
		s32 numEntities = (s32) pContents->GetNumEntities();
		if (entities && entityIndex.Get()>=0 && entityIndex.Get()<numEntities)
		{
			pEntity = entities[entityIndex.Get()];
		}
	}
	return pEntity;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SafeRemoveAll
// PURPOSE:		For removing all loaded imaps
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::SafeRemoveAll(bool bHdMapEntitiesOnly/*=false*/)
{
	const u32 entityMask = fwMapData::CONTENTFLAG_ENTITIES_HD;

	m_bIsRemovingAllMapData = true;

	//////////////////////////////////////////////////////////////////////////
	// compact loaded list
	for (s32 i=0; i<m_loadedList.GetCount(); i++)
	{
		fwMapDataLoadedNode* pNode = m_loadedList[i];
		if (pNode && pNode->IsMarkedDelete())
		{
			delete pNode;
			m_loadedList[i] = NULL;
			m_loadedList.DeleteFast(i);
			i--;
		}
	}

	// first, sort the loaded list by depth in dependency hierarchy
	m_loadedList.QSort(0, -1, CompareDepthCB);

	// then run over the loaded files and try to remove
	for (s32 i=0; i<m_loadedList.GetCount(); i++)
	{
		fwMapDataLoadedNode* pNode = m_loadedList[i];
		strLocalIndex slotIndex = strLocalIndex(m_loadedList[i]->GetSlotIndex());

		fwMapDataDef* pDef = GetSlot(slotIndex);

		bool bCanDelete = !bHdMapEntitiesOnly || ((pDef->GetContentFlags()&entityMask)!=0);

		if ( !pNode->IsMarkedDelete()
			&& pDef->IsLoaded()
			&& !pDef->GetNumLoadedChildren()
			&& bCanDelete )
		{
			// some imap files contain data that is rendered - e.g. lod lights imap files
			// so they add refs during render.
			// when removing absolutely all imap files we should make sure the refs are reduced
			while (GetNumRefs(slotIndex) > 1) { RemoveRef(slotIndex, REF_OTHER); }

			ClearRequiredFlag(slotIndex.Get(), STRFLAG_DONTDELETE);
			SafeRemove(strLocalIndex(slotIndex));
		}
	}

	m_boxStreamer.CleanRequiredList();
	
	//////////////////////////////////////////////////////////////////////////

	m_bIsRemovingAllMapData = false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddToLoadedList
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::AddToLoadedList(strLocalIndex index, fwMapDataDef* pDef)
{
	Assert(pDef->IsLoaded());
	fwMapDataLoadedNode* pNode = rage_new fwMapDataLoadedNode(index.Get(), pDef->GetDepth());
	Assert(pNode);
	pDef->m_pObject->SetLoadedNode(pNode);
	m_loadedList.PushAndGrow(pNode);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveFromLoadedList
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::RemoveFromLoadedList(strLocalIndex /*index*/, fwMapDataDef* pDef)
{
	Assert(pDef->IsLoaded());
	fwMapDataLoadedNode* pNode = pDef->m_pObject->GetLoadedNode();
	if (pNode)
	{
		pDef->m_pObject->SetLoadedNode(NULL);
		pNode->MarkDelete();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetDataPtr
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void* fwMapDataStore::GetDataPtr(strLocalIndex index)	
{
	Assert(index.Get() >= 0);
	fwMapDataDef* pDef = GetSlot(index);
	if(pDef && pDef->m_pObject)
		return pDef->m_pObject->GetLoaderInstRef().GetStorage();
	return NULL;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetHdStreaming
// PURPOSE:		set whether requesting of high detail imap files is permitted or not
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::SetHdStreaming(bool bAllowHdStreaming)
{
	fwBoxStreamerAssetFlags filterMask = fwBoxStreamerAsset::MASK_MAPDATA;

	if (!bAllowHdStreaming)
	{
		filterMask &= (~fwBoxStreamerAsset::FLAG_MAPDATA_HIGH);
	}

	m_boxStreamer.SetAssetFilter(filterMask);

	//grcDebugDraw::AddDebugOutput("Asset Filter: 0x%x", filterMask);
}

#if !__FINAL
void fwMapDataStore::PrintExtraInfo(strLocalIndex index, char* extraInfo, size_t /*maxSize*/) const
{
	switch (m_boxStreamer.GetAssetType(index.Get()))
	{
	case fwBoxStreamerAsset::ASSET_MAPDATA_LOW:
		sprintf(extraInfo, "LOW");
		break;
	case fwBoxStreamerAsset::ASSET_MAPDATA_MEDIUM:
		sprintf(extraInfo, "MEDIUM");
		break;
	case fwBoxStreamerAsset::ASSET_MAPDATA_HIGH:
		sprintf(extraInfo, "HIGH");
		break;
	case fwBoxStreamerAsset::ASSET_MAPDATA_HIGH_CRITICAL:
		sprintf(extraInfo, "HIGH CRIT");
		break;
	default:
		break;
	}
}
#endif	//!__FINAL

//////////////////////////////////////////////////////////////////////////
// CACHELOADER related

bool fwMapDataStore_ReadFromCacheFile(const void* const pLine, fiStream *pDebugTextFileToWriteTo)
{
	return fwMapDataStore::GetStore().ReadFromCacheFile(pLine, pDebugTextFileToWriteTo);
}

void fwMapDataStore_AddToCacheFile(fiStream* pDebugTextFileToWriteTo)
{
	fwMapDataStore::GetStore().AddToCacheFile(pDebugTextFileToWriteTo);
}

void WriteMapDataCacheEntryDetailsToTextFile(const fwMapDataCacheEntry *pCacheEntry, fiStream *pTextFile)
{
	if (pTextFile)
	{
		if (Verifyf(pCacheEntry, "WriteMapDataCacheEntryDetailsToTextFile - pCacheEntry is NULL"))
		{
			char outputString[256];

			formatf(outputString, "m_nameHash=%u\n", pCacheEntry->m_nameHash);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_parentNameHash=%u\n", pCacheEntry->m_parentNameHash);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_contentFlags=%u\n", pCacheEntry->m_contentFlags);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_strMin=%.2f,%.2f,%.2f\n", pCacheEntry->m_strMinX, pCacheEntry->m_strMinY, pCacheEntry->m_strMinZ);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_strMax=%.2f,%.2f,%.2f\n", pCacheEntry->m_strMaxX, pCacheEntry->m_strMaxY, pCacheEntry->m_strMaxZ);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_phyMin=%.2f,%.2f,%.2f\n", pCacheEntry->m_phyMinX, pCacheEntry->m_phyMinY, pCacheEntry->m_phyMinZ);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_phyMax=%.2f,%.2f,%.2f\n", pCacheEntry->m_phyMaxX, pCacheEntry->m_phyMaxY, pCacheEntry->m_phyMaxZ);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_bDynamicStreaming=%s\n", pCacheEntry->m_bDynamicStreaming?"true":"false");
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_bContainsBlockInfo=%s\n", pCacheEntry->m_bContainsBlockInfo?"true":"false");
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_bIsParent=%s\n", pCacheEntry->m_bIsParent?"true":"false");
			pTextFile->Write(outputString, (s32) strlen(outputString));

#if __BANK
			formatf(outputString, "m_blockInfo.m_flags=%u\n", pCacheEntry->m_blockInfo.m_flags);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_blockInfo.m_name_owner_exportedBy_time=%s\n\n", pCacheEntry->m_blockInfo.m_name_owner_exportedBy_time);
			pTextFile->Write(outputString, (s32) strlen(outputString));
#endif // __BANK
		}
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ReadFromCacheFile
// PURPOSE:		Processes a line taken from the cache file (called from within a loop for this module)
//////////////////////////////////////////////////////////////////////////
bool fwMapDataStore::ReadFromCacheFile(const void* const pEntry, fiStream* BANK_ONLY(pDebugTextFileToWriteTo))
{
	Vector3 vMin;
	Vector3 vMax;

	fwMapDataCacheEntry entry(pEntry);

#if __BANK
	if (pDebugTextFileToWriteTo)
	{
		WriteMapDataCacheEntryDetailsToTextFile(&entry, pDebugTextFileToWriteTo);
	}
#endif // __BANK

	strLocalIndex index = strLocalIndex(FindSlotFromHashKey(entry.m_nameHash));
	if (index != -1)
	{
		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(index));
		strStreamingFile* pFile = strPackfileManager::GetImageFileFromHandle(info->GetHandle());
		bool readEntry = !pFile->m_bNew;

		if (strCacheLoader::GetMode() == SCL_DLC)
		{
			atMap<s32, bool> dlcArchives = strCacheLoader::GetDLCArchives();
			int archiveIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();

			readEntry = dlcArchives.Access(archiveIndex) != NULL;
		}

		if ( Verifyf(pFile, "NULL packfile found for IMAP %s (slot %d) handle %d", GetName(index), index.Get(), info->GetHandle() )
			&& readEntry )
		{
			fwMapDataDef* pDef = GetSlot(index);
			Assert(pDef);
			// populate def from cache entry
			pDef->InitFromCacheEntry( index.Get(), &entry, m_boxStreamer.GetBounds(index.Get()), m_boxStreamer.GetStreamingBounds(index.Get()) );

#if __BANK
			if (ms_pGameInterface && entry.m_bContainsBlockInfo)
			{
				ms_pGameInterface->CreateBlockInfoFromCache(pDef, &(entry.m_blockInfo));
			}
#endif	//!__FINAL

			pDef->SetInitState(fwMapDataDef::FULLY_INITIALISED);
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddToCacheFile
// PURPOSE:		adds data based on the passed index into the cache file
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::AddToCacheFile(fiStream* pDebugTextFileToWriteTo)
{
	bool addEntry = false;

	ASSERT_ONLY(
	if (strCacheLoader::GetMode() != SCL_DLC)
		FatalAssertf(!m_boxStreamer.IsFullyInitialised(), "Writing out cache file AFTER the bounds have been inflated by PostProcess!");
	)

	for(s32 i=0; i<GetSize(); i++)
	{
		addEntry = false;

		if(IsValidSlot(strLocalIndex(i)))
		{
			if (m_boxStreamer.GetBounds(i).IsValid())
			{
				if (strCacheLoader::GetMode() == SCL_DLC)
				{
					atMap<s32, bool> dlcArchives = strCacheLoader::GetDLCArchives();
					strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(strLocalIndex(i)));
					int index = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();

					addEntry = dlcArchives.Access(index) != NULL;
				}
				else
				{
					addEntry = true;
				}

				if (addEntry)
				{
					fwMapDataDef* pDef = GetSlot(strLocalIndex(i));
					Assert(pDef);
					if(!GetPhysicalBounds(strLocalIndex(i)).IsValid() ||
					   !GetStreamingBounds(strLocalIndex(i)).IsValid())
					{
						Quitf(0, "Attempting to write invalid bounds data for valid slot! It isn't safe to continue, please discard any cache data that has been generated, redeploy, and try again. If the issue persists add a bug for *Code (Engine)*");
					}
					//
					fwMapDataCacheEntry entry(pDef, GetPhysicalBounds(strLocalIndex(i)), GetStreamingBounds(strLocalIndex(i)));

					if (pDebugTextFileToWriteTo)
					{
						WriteMapDataCacheEntryDetailsToTextFile(&entry, pDebugTextFileToWriteTo);
					}

					strCacheLoader::WriteDataToBuffer(&entry, sizeof(entry));
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	InitCacheLoaderModule
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataStore::InitCacheLoaderModule()
{
	strCacheLoader::GetModules().AddModule(
		"fwMapDataStore",
		&fwMapDataStore_ReadFromCacheFile,	// called when we read from the cache
		&fwMapDataStore_AddToCacheFile,		// called when we write to the cache
		sizeof(fwMapDataCacheEntry));
}

} // namespace rage
