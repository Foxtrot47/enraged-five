//
// fwscene/stores/maptypesstore.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//
//										Plan B. - Klaas
//

#ifndef __FWSCENE_MAPTYPESSTORE_H__
#define __FWSCENE_MAPTYPESSTORE_H__

#include "fwtl/assetstore.h"
#include "fwscene/mapdata/maptypes.h"
#include "fwscene/mapdata/maptypesdef.h"
#include "fwscene/mapdata/maptypescontents.h"

#define LAZY_ITYP_LOADING	(0)

namespace rage 
{
	enum{
		TYPE_FILE_BASE			=		0,					// base set of non-streamed archetype files (from the original disc)
		TYPE_FILE_STREAMED		=		1,					// set of streamed archetype files (mounted as dependencies of .imap files)
		TYPE_FILE_DLC			=		2,					// set of DLC archetype files (mounted for the lifetime of the DLC - like BASE but can be unloaded)
		TYPE_FILE_DLC_STREAMED	=		3,					// set of streamed DLC archetype files (mounted as dependencies of DLC .imap files)
	};

	// PURPOSE:	The store for the map types. 
	class fwMapTypesStore : public fwAssetStore<fwMapTypesContents, fwMapTypesDef>
	{	
	public:
		fwMapTypesStore();

		void Init(u32 initMode);
        void ShutdownLevelWithMapLoaded();
        void ShutdownLevelWithMapUnloaded();

		// streaming system interface
		virtual void RegisterStreamingModule();
		virtual bool Load(strLocalIndex index, void* pData, int size);

		bool ModifyHierarchyStatus(strLocalIndex index, eHierarchyModType modType);
		void FinishLoading(strLocalIndex mapTypeDefIndex, fwPsoStoreLoadInstance &loadInst );


		virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);
		virtual void SetResource(strLocalIndex index, datResourceMap& map);

		virtual bool CanPlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex)) const { return true; }
		virtual void PlaceAsynchronously(strLocalIndex objIndex, strStreamingLoader::StreamingFile& file, datResourceInfo& rsc);

		virtual strLocalIndex Register(const char* name);
		virtual void Remove(strLocalIndex iplIndex);
		virtual int GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const;

		virtual void* GetDataPtr(strLocalIndex index);
		virtual bool RequiresTempMemory(strLocalIndex index) const;

#if !__FINAL || __FINAL_LOGGING
		virtual const char* GetName(strLocalIndex index) const;
#endif

		bool IsBackwardCompatExportOn() { return (m_bEnableMapTypeOutput); }

		void RemoveUnrequired();
		void Update();

		void VerifyItypFiles();

#if !__NO_OUTPUT
		void PrintStore();
#endif

#if !__FINAL && 0 //disabled for now but may be useful
// handy for debugging
 		virtual void AddRef(strLocalIndex index, strRefKind strRefKind);
		virtual void RemoveRef(strLocalIndex index, strRefKind strRefKind);
#endif

	private:
		fwPsoStoreLoader m_loader;

		bool m_bEnableMapTypeOutput;

		bool m_bIsShuttingDown;

		void SafelyRemoveTypeSlots(u32 fileCategory);
		void SafeRemoveTypeSlot(strLocalIndex slotIdx);
		void SetDepth(atArray<s32>& typeDepths, strLocalIndex slotIndex, s32 currDepth);
	};

	extern fwMapTypesStore g_MapTypesStore;

    // wrapper class needed to interface with game skeleton code
    class CMapTypesStoreWrapper
    {
    public:
        static void Init(unsigned initMode)							{ g_MapTypesStore.Init(initMode); }
        static void Shutdown1(unsigned UNUSED_PARAM(shutdownMode))	{ g_MapTypesStore.ShutdownLevelWithMapLoaded(); }
        static void Shutdown2(unsigned UNUSED_PARAM(shutdownMode))	{ g_MapTypesStore.ShutdownLevelWithMapUnloaded(); }
    };
} // namespace rage

#endif // __FWSCENE_MAPTYPESSTORE_H__
