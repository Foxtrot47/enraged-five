/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textStore.h 
// PURPOSE : Class for streaming in localized text database files.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWTEXT_STORE_H_
#define FWTEXT_STORE_H_

#include "fwtl/assetstore.h"
#include "textAsset.h"

namespace rage
{

// PURPOSE: Base class for streaming in text databases
class fwTextStore : public fwAssetRscStore< rage::fwTextAsset, rage::fwTextAssetDef >
{
public:
	fwTextStore();

	// PURPOSE: Search through all loaded text and try and find a string with the given
	//			identifier
	// PARAMS:
	//		hashIdentifier - Hash identifier to search for
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindText( u32 const hashIdentifier ) const;

	// PURPOSE: Search through all loaded text and try and find a string with the given
	//			identifier
	// PARAMS:
	//		hashIdentifier		- Hash identifier to search for
	//		slotIndexFoundIn	-  Output parameter - Slot index for where our text was actually found. -1 if text not found.
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindTextAndSlot( u32 const hashIdentifier, int& slotIndexFoundIn ) const;

	// PURPOSE: Search through the given text database and try and find a string with the given
	//			identifier
	// PARAMS:
	//		slotIndex - Index of the slot we want to search
	//		hashIdentifier - Hash identifier to search for
	// RETURNS:
	//		Const string * if found, null if not found
	char const * FindText( strLocalIndex const slotIndex, u32 const hashIdentifier ) const;

	// PURPOSE: Return the ref count for the given slot
	// PARAMS:
	//		slotIndex - Index of the slot we want ref count of
	// RETURNS:
	//		u64 ref count for the given slot
	u64 GetRefCount( strLocalIndex const slotIndex ) const;

	// PURPOSE: Return the flags for a given slot
	// PARAMS:
	//		slotIndex - Index of the slot we want ref count of
	// RETURNS:
	//		u16 flags for the given slot
	u16 GetFlags( strLocalIndex const slotIndex ) const;

	// PURPOSE: Updates the reference count on an asset directly
	// PARAMS:
	//		slotIndex - Index of the slot we want to set ref count on
	//		rawRefCount - Raw reference value to override
	// NOTES:
	//		Added JUST to simplify text database reloading. Use at your own risk.
	void SetRawRefCount( strLocalIndex const slotIndex, u64 const rawRefCount );

	// PURPOSE: Updates the flags on an asset directly
	// PARAMS:
	//		slotIndex - Index of the slot we want to set ref count on
	//		rawFlags - Raw flags to apply
	// NOTES:
	//		Added JUST to simplify text database reloading. Use at your own risk.
	void SetRawFlags( strLocalIndex const slotIndex, u16 const rawFlags );

	void RemoveAllAndUnregisterFromStreaming();

private:
};

} // namespace rage

#endif // FWTEXT_STORE_H_
