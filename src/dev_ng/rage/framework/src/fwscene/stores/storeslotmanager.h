// 
// fwscene/stores/storeslotmanager.h 
// 
// small helper class for managing store slots outside of the streaming system
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FWSCENE_STORE_SLOT_MANAGER_H_
#define FWSCENE_STORE_SLOT_MANAGER_H_

#include "fwtl/assetstore.h"

template<typename T, typename S = fwAssetDef<T> > class fwStoreSlotManager
{
public:
	fwStoreSlotManager(fwAssetStore<T,S>& store) : m_store(store) {}

	int AddSlot(const char* pName, T* pEntry);
	void SetSlotNoLongerNeed(int slot);
	bool SlotExists(const char* pName) const;

private:
	atArray<int> m_garbage;
	fwAssetStore<T,S>& m_store;
};


template<typename T, typename S>
int fwStoreSlotManager<T,S>::AddSlot(const char* pName, T* pEntry)
{
	// Clear garbage
	for(int i = m_garbage.GetCount()-1; i >= 0; i--)
	{
		strLocalIndex slot = strLocalIndex(m_garbage[i]);
		if(m_store.Get(slot) == NULL)
		{
			m_store.RemoveSlot(slot);
			m_garbage.DeleteFast(i);
		}
		else if(m_store.GetNumRefs(slot) == 0)
		{
			m_store.Remove(slot);
			m_store.RemoveSlot(slot);
			m_garbage.DeleteFast(i);
		}

	}

	// Check if slot exists already
	if(!Verifyf(m_store.FindSlot(pName) == -1, "Can't add slot with name %s as it already exists", pName))
		return -1;

	// Check if there is any free slot in the store
#if !USE_PAGED_POOLS_FOR_STREAMING
	if (!Verifyf(m_store.GetNumUsedSlots() != m_store.GetSize(), "Can't add a new slot to store %s because the store is full (%d)", m_store.GetModuleName(), m_store.GetSize()))
		return -1;
#endif // !USE_PAGED_POOLS_FOR_STREAMING

	strLocalIndex slot = strLocalIndex(m_store.AddSlot(pName));
	m_store.Set(slot, pEntry);

	strIndex index = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_store.GetStreamingModuleId())->GetStreamingIndex(slot);
	strStreamingEngine::GetInfo().SetDoNotDefrag(index);

	m_store.AddRef(slot, REF_OTHER);

	return slot.Get();
}

template<typename T, typename S>
void fwStoreSlotManager<T,S>::SetSlotNoLongerNeed(int slot)
{
	m_store.RemoveRef(slot);
	m_garbage.PushAndGrow(slot);
}

template<typename T, typename S>
bool fwStoreSlotManager<T,S>::SlotExists(const char* pName) const
{
	return (m_store.FindSlot(pName) != -1);
}

#endif //FWSCENE_STORE_SLOT_MANAGER_H_
