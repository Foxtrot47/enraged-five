
// --- Include Files ------------------------------------------------------------

#include "blendshapestore.h"

#include "fwsys/fileexts.h"
#include "grblendshapes\manager.h"

#if ENABLE_BLENDSHAPES

namespace rage {


// --- Defines ------------------------------------------------------------------

// --- Globals ------------------------------------------------------------------

fwAssetRscStore<grbTargetManager> g_BlendShapeStore("BlendShapeStore", 
													BLENDSHAPE_FILE_ID, 
													CONFIGURED_FROM_FILE, 
													false, 
													BLENDSHAPE_FILE_VERSION);

// --- Code ---------------------------------------------------------------------

template<> void fwAssetStore<grbTargetManager>::Remove(int index)
{
	fwAssetDef<grbTargetManager>* pDef = m_pool.GetSlot(index);

	Assertf(pDef, "No blendshape at this slot");
	Assertf(pDef->m_pObject!=NULL, "blendshape is not in memory");

	// TODO: Not sure if this is necessary
	REMOVE_KNOWN_REF(pDef->m_pObject);

	pDef->m_pObject->Release();
	pDef->m_pObject = NULL;
}

} // namespace rage

#endif // ENABLE_BLENDSHAPES

