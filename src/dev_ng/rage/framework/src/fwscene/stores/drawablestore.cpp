// 
// stores/drawablestore.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "rmcore/drawable.h"

#include "fwscene/stores/drawablestore.h"

#include "fwscene/mapdata/mapinstancedata.h"
#include "fwscene/texLod.h"
#include "fwscene/stores/txdstore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"


namespace rage 
{

fwPlaceFunctor fwDrawableStore::sm_PlaceFunctor;
fwDefragmentFunctor fwDrawableStore::sm_DefragmentFunctor;

fwDrawableStore g_DrawableStore; 
	
fwDrawableStore::fwDrawableStore() : fwAssetRscStore<Drawable, fwDrawableDef>(
		"DrawableStore",
		DRAWABLE_FILE_ID, 
		CONFIGURED_FROM_FILE, 
		2048,
		true, 
		DRAWABLE_FILE_VERSION
	)
{
}


void fwDrawableStore::Set(strLocalIndex index, Drawable* m_pObject)
{
	fwDrawableDef* pfwDrawableDef = GetSlot(index);

	Assertf(pfwDrawableDef, "No drawable at this slot");
	Assertf(pfwDrawableDef->m_pObject==NULL, "Drawable is already in memory");

	UPDATE_KNOWN_REF(pfwDrawableDef->m_pObject, m_pObject);
}


void fwDrawableStore::Remove(strLocalIndex index)
{
	fwDrawableDef* pDef = GetSlot(index);
	Assertf(pDef, "No drawable at this slot");
	Assertf(pDef->m_pObject!=NULL, "Drawable is not in memory");

	fwGrassInstanceListDef::NotifyDrawableRemove(index);
	
	SAFE_REMOVE_KNOWN_REF(pDef->m_pObject);
	if (!pDef->m_bMatchError)
	{
		pDef->m_pObject->GetShaderGroup().Destroy();
		pDef->m_pObject->Destroy();
	}
	else
	{
		delete pDef->m_pObject;
	}
	pDef->m_pObject = NULL;
}



bool fwDrawableStore::LoadFile(strLocalIndex index, const char* pFilename)
{
	fwDrawableDef* pfwDrawableDef = GetSlot(index);

	Assertf(pfwDrawableDef, "No drawable at this slot");
	Assertf(pfwDrawableDef->m_pObject==NULL, "Drawable is already in memory");

	fwBackupCurrentTxd backup;
	rage::g_TxdStore.SetCurrentTxd(pfwDrawableDef->GetTxdIndex());

	Drawable* m_pObject;
	pgRscBuilder::Load(m_pObject, pFilename, DRAWABLE_FILE_EXT_PATTERN, DRAWABLE_FILE_VERSION);
	Set(index, m_pObject);

	return (pfwDrawableDef->m_pObject != NULL);
}

void fwDrawableStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& info)
{
	fwDrawableDef* pfwDrawableDef = GetSlot(index);

	Assertf(pfwDrawableDef, "No drawable at this slot");
	Assertf(pfwDrawableDef->m_pObject==NULL, "Drawable is already in memory");

	fwBackupCurrentTxd backup;
	g_TxdStore.SetCurrentTxd(pfwDrawableDef->GetTxdIndex());

	grcEffect::ResetMatchError();

	pgBase *drawable = 
#if __FINAL
	(sm_PlaceFunctor(info, map, NULL, false));
#else
	(sm_PlaceFunctor(info, map, GetName(index), false));
#endif

	pfwDrawableDef->m_bMatchError = true; // rage::grcEffect::GetMatchError() || RSG_PC;

	//Notify instance placement tech that drawable streamed in so device resources can be created on the placement thread. (for PC)
	fwGrassInstanceListDef::NotifyDrawablePlacement(index, static_cast<rmcDrawable *>(drawable));
}


void* fwDrawableStore::Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& map, bool& flush)
{
	Drawable* object = static_cast<Drawable*>(sm_DefragmentFunctor(map));
	flush = true;

	return object;
}


void fwDrawableStore::DefragmentPreprocess(strLocalIndex index)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged = true;
		fwTexLod::SetSwapStateSingle(false, fwAssetLocation(STORE_ASSET_DRB,index.Get()));				// unswap before hitting defrag code
	}
}


void fwDrawableStore::DefragmentComplete(strLocalIndex index)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged = false;
		fwTexLod::SetSwapStateSingle(true, fwAssetLocation(STORE_ASSET_DRB,index.Get()));				// unswap before hitting defrag code
	}
}


bool fwDrawableStore::IsBeingDefragged(strLocalIndex index)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBeingDefragged);
	}
	return(false);
}


bool fwDrawableStore::GetIsHDCapable(strLocalIndex index)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bHasHD);
	}
	return(false);
}


void fwDrawableStore::SetIsHDCapable(strLocalIndex index, bool val)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bHasHD = val;
	}
}


bool fwDrawableStore::GetIsBoundHD(strLocalIndex index)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBoundHD);
	}
	return(false);
}


void fwDrawableStore::SetIsBoundHD(strLocalIndex index, bool val)
{
	fwDrawableDef* pDef = fwDrawableStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBoundHD = val;
	}
}


int fwDrawableStore::GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const
{
	int count = 0;
	strLocalIndex parentTxd = fwDrawableStore::GetParentTxdForSlot(index);
	if(parentTxd != -1)
	{
		AddDependencyOutput(pIndices, count, g_TxdStore.GetStreamingModule()->GetStreamingIndex(parentTxd), indexArraySize, GetStreamingIndex(index));
	}
	return count;
}


#if __ASSERT
void fwDrawableStore::SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent)
{
	if (GetSlot(index)->GetTxdIndex() != parent)
	{
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(GetStreamingIndex(index));

		Assertf(info.GetStatus() == STRINFO_NOTLOADED, "%s sets its parent TXD from %d to %d, but its status is %s", GetName(index), GetSlot(index)->GetTxdIndex().Get(), parent.Get(), info.GetFriendlyStatusName());
		GetSlot(index)->SetTxdIndex(parent.Get());
	}
}
#endif // __ASSERT

void fwDrawableStore::ShutdownLevel()
{
	RemoveAll();
}


void fwDrawableStoreWrapper::Shutdown(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		g_DrawableStore.Shutdown();
	}
	else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
	{
		g_DrawableStore.ShutdownLevel();
	}
}

}	// namespace rage

