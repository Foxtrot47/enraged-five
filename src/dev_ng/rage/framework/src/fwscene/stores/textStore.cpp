/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textStore.cpp 
// PURPOSE : Class for streaming in localized text database files.
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#include "fwsys/fileExts.h"
#include "streaming/streaminginfo.h"
#include "textStore.h"

namespace rage
{

fwTextStore::fwTextStore()
	: fwAssetRscStore< rage::fwTextAsset, rage::fwTextAssetDef>( "TextStore", TEXT_DATABASE_FILE_ID, CONFIGURED_FROM_FILE, 1024, true, fwTextAsset::RORC_VERSION )
{

}

char const * fwTextStore::FindText( u32 const hashIdentifier ) const
{
	int dummyOutParam;
	return FindTextAndSlot( hashIdentifier, dummyOutParam );
}

char const * fwTextStore::FindTextAndSlot( u32 const hashIdentifier, int& slotIndexFoundIn ) const
{
	char const * result = NULL;
	slotIndexFoundIn = -1;

	for( int index = 0; index < GetNumUsedSlots(); ++index )
	{
		fwTextAssetDef const * const textAssetDef = GetSlot( strLocalIndex(index) );
		result = ( textAssetDef && textAssetDef->m_pObject ) ? textAssetDef->getTextField( hashIdentifier ) : NULL;
		if(result)
		{
			slotIndexFoundIn = index;
			break;
		}
	}

	return result;

}

char const * fwTextStore::FindText( strLocalIndex const slotIndex, u32 const hashIdentifier ) const
{
	char const * result;

	Assertf( IsValidSlot( slotIndex ), "Invalid slot passed used to find text database!" );
	fwTextAssetDef const * const textAssetDef = ( IsValidSlot( slotIndex ) ) ? GetSlot( slotIndex ) : 0;
	result = ( textAssetDef && textAssetDef->m_pObject ) ? textAssetDef->getTextField( hashIdentifier ) : NULL;
	
	return result;
}

u64 fwTextStore::GetRefCount( strLocalIndex const slotIndex ) const
{
	u64 result;

	Assertf( IsValidSlot( slotIndex ), "Invalid slot passed used to find text database!" );
	fwTextAssetDef const * const textAssetDef = ( IsValidSlot( slotIndex ) ) ? GetSlot( slotIndex ) : 0;
	result = ( textAssetDef && textAssetDef->m_pObject ) ? 
#if DETAILED_MODULE_REF_COUNT
		textAssetDef->m_refsUnified
#else // DETAILED_MODULE_REF_COUNT
		(u64)textAssetDef->m_refCount
#endif // DETAILED_MODULE_REF_COUNT
		: 0;

	return result;
}

u16 fwTextStore::GetFlags( strLocalIndex const slotIndex ) const
{
	u16 result;

	Assertf( IsValidSlot( slotIndex ), "Invalid slot passed used to find text database!" );
	strStreamingInfo const * const textAssetInfo = GetStreamingInfo( slotIndex );
	result = textAssetInfo ? textAssetInfo->GetFlags() : 0;

	return result;
}

void fwTextStore::SetRawRefCount( strLocalIndex const slotIndex, u64 const rawRefCount )
{
	Assertf( IsValidSlot( slotIndex ), "Invalid slot passed used to find text database!" );
	fwTextAssetDef * const textAssetDef = ( IsValidSlot( slotIndex ) ) ? GetSlot( slotIndex ) : 0;
	if( textAssetDef && textAssetDef->m_pObject )
	{
#if DETAILED_MODULE_REF_COUNT
		textAssetDef->m_refsUnified = rawRefCount;
#else // DETAILED_MODULE_REF_COUNT
		textAssetDef->m_refCount = (int)rawRefCount;
#endif // DETAILED_MODULE_REF_COUNT
	}
}

void fwTextStore::SetRawFlags( strLocalIndex const slotIndex, u16 const rawFlags )
{
	Assertf( IsValidSlot( slotIndex ), "Invalid slot passed used to find text database!" );
	strStreamingInfo * const textAssetInfo = GetStreamingInfo( slotIndex );
	if( textAssetInfo )
	{
		textAssetInfo->SetFlag( textAssetInfo->GetIndex(), rawFlags, false );
	}
}

void fwTextStore::RemoveAllAndUnregisterFromStreaming()
{
	for( int slot = 0; slot < GetSize(); ++slot )
	{
		strStreamingEngine::GetInfo().RemoveObject( GetStreamingIndex( strLocalIndex(slot) ) );
		strStreamingEngine::GetInfo().UnregisterObject( GetStreamingIndex( strLocalIndex(slot) ) );
	}

	// Assume if we are being shut down we are either shutdown and don't care about ref-counts,
	// or we are switching to another language and our ref-counts will be restored.{
	RemoveAll( true );
}

} // namespace rage
