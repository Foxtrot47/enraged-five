// 
// stores/fragmentstore.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWSCENE_STORES_FRAGMENTSTORE_H_
#define FWSCENE_STORES_FRAGMENTSTORE_H_

#include "fwtl/assetstore.h"

#include "fragment/type.h"

namespace rage {

typedef fragType Fragment;

#if !__SPU


class fwFragmentDef : public fwAssetDef<Fragment>
{
public:
	void Init(const strStreamingObjectName name)
	{
		fwAssetDef<Fragment>::Init(name);

		m_txdIndex			= -1;

		m_bIsBeingDefragged	= 0;
		m_bHasHD			= 0;
		m_bIsBoundHD		= 0;
	}


	strLocalIndex GetTxdIndex() const
	{
		return strLocalIndex(m_txdIndex);
	}

	void SetTxdIndex(s32 txdIndex)
	{
		m_txdIndex = txdIndex;
	}

private:
	s32 m_txdIndex;
public:

	u16 m_bIsBeingDefragged : 1;
	u16 m_bHasHD			: 1;
	u16 m_bIsBoundHD		: 1;
	u16 m_pad				: 13;
};


class fwFragmentStore : public fwAssetRscStore<Fragment, fwFragmentDef>
{
public:
	fwFragmentStore();


	virtual void Set(strLocalIndex index, Fragment* m_pObject);
	virtual void Remove(strLocalIndex index);
	//virtual void RemoveRef(strLocalIndex index, strRefKind strRefKind);
	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);

	virtual void* Defragment(strLocalIndex index, datResourceMap& map, bool& flush);
	virtual void DefragmentComplete(strLocalIndex index);
	virtual void DefragmentPreprocess(strLocalIndex index);

	bool IsBeingDefragged(strLocalIndex index);
	bool GetIsHDCapable(strLocalIndex index);
	void SetIsHDCapable(strLocalIndex index, bool val);
	bool GetIsBoundHD(strLocalIndex index);
	void SetIsBoundHD(strLocalIndex index, bool val);

	static void CheckIfInUse(fragType* pFrag);

	virtual int GetDependencies(strLocalIndex UNUSED_PARAM(index), strIndex *UNUSED_PARAM(pIndices), int indexArraySize) const;

	void ShutdownLevel();

#if !__ASSERT
	void SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent) {GetSlot(index)->SetTxdIndex(parent.Get());}
#else // !__ASSERT
	void SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent);
#endif // !__ASSERT

	strLocalIndex GetParentTxdForSlot(strLocalIndex index) const {return GetSlot(index)->GetTxdIndex();}

	inline void SetPlaceFunctor(fwPlaceFunctor functor)
	{
		sm_PlaceFunctor=functor;
	}

	void SetDefragmentFunctor(fwDefragmentFunctor functor)
	{
		sm_DefragmentFunctor=functor;
	}
private:
	static fwPlaceFunctor sm_PlaceFunctor;
	static fwDefragmentFunctor sm_DefragmentFunctor;
};

// wrapper class needed to interface with game skeleton code
class fwFragmentStoreWrapper
{
public:

	static void Shutdown(unsigned shutdownMode);
};


extern fwFragmentStore g_FragmentStore;

#endif // !__SPU

}	// namespace rage

#endif	// FWSCENE_STORES_FRAGMENTSTORE_H_