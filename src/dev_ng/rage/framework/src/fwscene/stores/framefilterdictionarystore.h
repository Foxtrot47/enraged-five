
#ifndef FWSCENE_STORES_FRAMEFILTERDICTIONARYSTORE_H
#define FWSCENE_STORES_FRAMEFILTERDICTIONARYSTORE_H

// --- Include Files ------------------------------------------------------------

#include "fwtl/assetstore.h"
#include "paging/dictionary.h"
#include "cranimation/framefilters.h"
#include "fwanimation/animdefines.h"

namespace rage {

extern const fwMvFilterSetId FILTER_SET_PLAYER;

typedef pgDictionary<crFrameFilter> crFrameFilterDictionary;

class crFrameFilterDictionaryStore : public fwAssetRscStore<crFrameFilterDictionary>
{
public:

	crFrameFilterDictionaryStore(const char* pModuleName, int moduleTypeIndex, int size, u32 elementsPerPage, bool canDefragment, int rscVersion)
	: fwAssetRscStore<crFrameFilterDictionary>(pModuleName, moduleTypeIndex, size, elementsPerPage, canDefragment, rscVersion)
	{
	}

	crFrameFilter *FindFrameFilter(const fwMvFilterId &filterId, const fwMvFilterSetId &filterSetId = FILTER_SET_PLAYER);

#if __BANK
	const char *FindFrameFilterNameSlow(const crFrameFilter *frameFilter);
#endif
};

extern crFrameFilterDictionaryStore g_FrameFilterDictionaryStore;

} // namespace rage

#endif // FWSCENE_STORES_FRAMEFILTERDICTIONARYSTORE_H
