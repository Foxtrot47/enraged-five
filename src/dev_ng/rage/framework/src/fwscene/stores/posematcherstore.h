
#ifndef FWSCENE_STORES_POSEMATCHERSTORE_H
#define FWSCENE_STORES_POSEMATCHERSTORE_H

// --- Include Files ------------------------------------------------------------

#include "fwtl\assetstore.h"
#include "fwanimation\pointcloud.h"

namespace rage {

class fwPoseMatcherStore : public fwAssetStore<crPoseMatcher>
{
public:
	fwPoseMatcherStore();

	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void Remove(strLocalIndex index);

};

extern fwPoseMatcherStore g_PoseMatcherStore;

} // namespace rage


#endif // FWSCENE_STORES_NETWORKDEFSTORE_H
