//
// fwscene/stores/boxstreamer.cpp : streams assets based on bounding boxes in world space
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "fwgeovis/geovis.h"
#include "fwscene/stores/boxstreamer.h"
#include "streaming/streamingallocator.h"
#include "streaming/streamingengine.h"
#include "streaming/streamingvisualize.h"
#include "system/nelem.h"
#include "system/param.h"
#include "vector/geometry.h"

#if __BANK
#include "entity/entity.h"
#include "fwdrawlist/drawlist.h"
#include "fwdebug/picker.h"
#include "fwdebug/vectormap.h"
#include "grcore/debugdraw.h"
#include "grcore/viewport.h"
#include "input/mouse.h"
#endif	//__BANK

namespace rage
{

PARAM(dumpinfoonblockload, "[fwscene] Don't dump debug stats when a blockload occurs");

#if __BANK
const char* fwBoxStreamer::apszBoxStreamerAssetTypes[fwBoxStreamerAsset::ASSET_TOTAL] = { "Mover bnds", "Weapon bnds", "Material bnds", "MapdataHigh", "MapdataHighCritical", "MapdataMedium", "MapdataLow", "Metadata" };
#endif

fwBoxStreamerInterfaceNew* fwBoxStreamer::ms_pGameInterface = NULL;
void* fwBoxStreamer::ms_tempResults[MAX_SEARCH_RESULTS];

#if __BANK
bool fwBoxStreamer::sm_BlockingPermitted = true;
#endif // __BANK


#if !__FINAL
const char *fwBoxStreamerSearch::sm_TypeNames[MAX_TYPE] = {

	"OTHER",
	"PRIMARY",
	"SECONDARY",
	"SCRIPTENTITY",
	"STREAMINGVOLUME",
	"NAVMESHEXPORT",
	"SWITCH",
	"DEBUG",
	"NETWORK",
	"SRL",
	"RECENTVEHICLE",
	"INFLATE"
};

const char *fwBoxStreamerSearch::GetTypeName() const
{
	CompileTimeAssert(NELEM(sm_TypeNames) == MAX_TYPE);
	return sm_TypeNames[m_type];
};

#if STREAMING_VISUALIZE
void fwBoxStreamerSearch::RegisterSearchTypesWithSV()
{
	for (int x=0; x<MAX_TYPE; x++)
	{
		STRVIS_REGISTER_NAMED_FLAG(strStreamingVisualize::FLAGTYPE_SEARCH, x, sm_TypeNames[x]);
	}
}
#endif // STREAMING_VISUALIZE


#endif // !__FINAL


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		resets all boxes and flags to default values
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::Init(strStreamingModule* pModule, float fPriorityRadius)
{
	Assert(m_pTree==NULL);
	Assert(pModule);

	m_pModule = pModule;
	m_fPriorityRadius = fPriorityRadius;

	m_bEntriesLocked = false;
	m_numEntries = pModule->GetMaxSize();

	m_aBoxes.Resize(m_numEntries);
	m_aFlags.Resize(m_numEntries);

	// Pre-size dirtyList
	m_dirtyList.SetAllowRecompute(true);
	m_dirtyList.Recompute(1031);

	// reset boxes and flags
	for (u32 i=0; i<m_numEntries; i++)
	{
		m_aBoxes[i].Invalidate();
		m_aFlags[i] = 0;
	}

	// clear asset flags
	m_supportedAssetFlags = 0;
	m_requiredAssetFlags = 0;
	m_ensuredAssetFlags = 0;
	m_priorityAssetFlags = 0;
	m_alwaysPriorityFlags = 0;
	m_filterRequestAssetFlags = 0;

	// clear stream ranges for each asset type
	for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
	{
		m_afSearchRadiusByType[i] = 0.0f;
		m_afSearchRadiusScaleByType[i] = 1.0f;
	}

	//////////////////////////////////////////////////////////////////////////
#if __BANK
	m_bDebugDisplayIntersectingButUnloaded = false;
	m_bDebugDisplayIncludesPreStream = false;
	m_bDebugDisplayInMemory = false;
	m_bDisplayLoadedList = false;
	m_bDisplaySearchList = false;
	m_bDisplayHasLoadedForSelected = false;
	m_bDumpInfoOnBlockload = PARAM_dumpinfoonblockload.Get();
	m_bDebugOverrideStreamRangeScale = false;
	for (u32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
	{
		m_afDebugStreamScale[i] = 1.0f;
	}
#endif	//__BANK
	//////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Shutdown
// PURPOSE:		clears any requested entries
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::Shutdown()
{
	for (u32 i=0; i<m_numEntries; i++)
	{
		if (GetFlags(i) & FLAG_REQ_CURR)
		{
			m_pModule->ClearRequiredFlag(i, STRFLAG_DONTDELETE);
			ClearFlags(i, FLAG_REQ_CURR);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddSupportedAsset
// PURPOSE:		registers a supported asset type
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::AddSupportedAsset(fwBoxStreamerAsset::eType assetType, float fStreamRadius, bool bEnsured, bool bPriorityWithinSphere, bool bAlwaysPriority)
{
	// stream radius
	m_afSearchRadiusByType[assetType] = fStreamRadius;
	m_afSearchRadiusScaleByType[assetType] = 1.0f;

	// asset flags
	fwBoxStreamerAssetFlags assetFlag = ( 1 << assetType );
	m_supportedAssetFlags |= assetFlag;		// supported asset; searches for this asset will proceed
	m_requiredAssetFlags |= assetFlag;		// required asset; primary and secondary searches look for this asset
	m_filterRequestAssetFlags |= assetFlag;	// filter streaming requests issued by asset type
	if (bEnsured)
	{
		m_ensuredAssetFlags |= assetFlag;	// ensured; we block until nearby assets of this type are loaded
	}
	if (bPriorityWithinSphere)
	{
		m_priorityAssetFlags |= assetFlag;
	}
	if (bAlwaysPriority)
	{
		m_alwaysPriorityFlags |= assetFlag;
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PostProcess
// PURPOSE:		construct tree with bounding boxes
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::PostProcess()
{
	m_bEntriesLocked = true;

	if (m_pTree)
		delete m_pTree;

	m_pTree = rage_new MNBVHSimpleInterface(m_aBoxes);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIntersectingAABB
// PURPOSE:		returns list of slots with boxes intersecting specified volume
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::GetIntersectingAABB(const spdAABB& bbox, fwBoxStreamerAssetFlags assetFlags, atArray<u32>& slotList, bool bIncludePreStream)
{
	if ((assetFlags & m_supportedAssetFlags) == 0)
	{
		return;
	}

	MNBVHSimpleInterface* pTree = m_pTree;

	for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
	{
		const fwBoxStreamerAssetFlags currentAssetFlag = (1 << i);
		if (assetFlags & currentAssetFlag)
		{
			// construct search box
			spdAABB searchBox = bbox;
			if (bIncludePreStream)
			{
				const float fStreamRadius = ( m_afSearchRadiusByType[i] * m_afSearchRadiusScaleByType[i] );
				searchBox.GrowUniform(ScalarV(fStreamRadius));
			}

			// perform search for this asset type
			int numPrelimResults = pTree->PerformBoxSearch(searchBox, ms_tempResults, MAX_SEARCH_RESULTS);

			// add results to the array, if they are not ignored and if the asset type is correct
			for (s32 j=0; j<numPrelimResults; j++)
			{
				s32 index = pTree->GetSearchResult(ms_tempResults, j);	// mnbvh makes use of the lsb
				if ( (GetAssetFlags(index) & currentAssetFlag)!=0 && !GetIsIgnored(index) )
				{
					slotList.PushAndGrow(index);
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIntersectingLine
// PURPOSE:		returns list of slots with boxes intersecting specified line seg
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::GetIntersectingLine(Vec3V_In vPos1, Vec3V_In vPos2, fwBoxStreamerAssetFlags assetFlags, atArray<u32>& slotList, bool ASSERT_ONLY(bIncludePreStream))
{
	if ((assetFlags & m_supportedAssetFlags) == 0)
	{
		return; 
	}

	Assertf(!bIncludePreStream, "Unsupported");

	MNBVHSimpleInterface* pTree = m_pTree;

	int numPrelimResults = pTree->PerformLineSearch(vPos1, vPos2, ms_tempResults, MAX_SEARCH_RESULTS);
	for (s32 i=0; i<numPrelimResults; i++)
	{
		s32 index = pTree->GetSearchResult(ms_tempResults, i);
		if ( (GetAssetFlags(index)&assetFlags)!=0 && !GetIsIgnored(index) )
		{
			slotList.PushAndGrow(index);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequestAboutPos
// PURPOSE:		searches boxes for specified primary search (and any other
//				required searches) and issues requests
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::RequestAboutPos(const fwBoxStreamerSearch& primarySearch, u32 extraStreamingFlags)
{
	//////////////////////////////////////////////////////////////////////////
	// PRESEARCH
	// flag stuff that was required previous frame
	atArray<s32>& previouslyRequired = m_requiredList.GetOldList();
	for (s32 i=0; i<previouslyRequired.GetCount(); i++)
	{
		//TODO incorporate prefetch
		s32 slotIndex = previouslyRequired[i];
		ClearFlags(slotIndex, FLAG_REQ_CURR);
		SetFlags(slotIndex, FLAG_REQ_PREV);
	}
	m_requiredList.Reset();	// clear new list

	// check for anything that has been manually been deleted this frame
	for (atMap<s32,u32>::Iterator iter = m_dirtyList.CreateIterator(); iter; ++iter)
	{
		s32 slotIndex = iter.GetKey();
		ClearFlags(slotIndex, REQ_MASK);
	}
	m_dirtyList.Reset();
	
	//////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////
	// SEARCH
	// perform searches, issue requests
	GenerateSearchList(primarySearch, m_searches);
	for (s32 i=0; i<m_searches.GetCount(); i++)
	{
#if !__FINAL
		if (ms_pGameInterface->IsExportingCollision())
		{
			SetRequired2D(m_searches[i]);
		}
		else
#endif
		{
			SetRequired(m_searches[i]);
		}
	}
	m_bSearchSecondaryPos = false;
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// POSTSEARCH
	RequestRequired(primarySearch.GetPos(), extraStreamingFlags);
	m_requiredList.Swap();
	//////////////////////////////////////////////////////////////////////////	
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequestAboutPos
// PURPOSE:		searches boxes for specified primary position (and any other
//				required searches) and issues requests
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::RequestAboutPos(Vec3V_In vPrimaryPos, u32 extraStreamingFlags)
{
	fwBoxStreamerSearch primarySearch( vPrimaryPos, fwBoxStreamerSearch::TYPE_PRIMARY, (m_bOverrideRequiredThisFrame ? m_overriddenRequiredAssetFlags : m_requiredAssetFlags) );
	RequestAboutPos(primarySearch, extraStreamingFlags);
	m_bOverrideRequiredThisFrame = false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	HasLoadedAboutPos
// PURPOSE:		checks a previously cached search results list for intersection
//				with a small volume around specified position
//////////////////////////////////////////////////////////////////////////
bool fwBoxStreamer::HasLoadedAboutPos(Vec3V_In vPos, fwBoxStreamerAssetFlags assetFlags)
{
	const spdSphere testSphere(vPos, ScalarV(m_fHasLoadedSearchRadius));
	return HasLoadedWithinAABB(spdAABB(testSphere), assetFlags);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	HasLoadedWithinAABB
// PURPOSE:		checks a previously cached search results list for intersection
//				with a small volume around specified position
//////////////////////////////////////////////////////////////////////////
bool fwBoxStreamer::HasLoadedWithinAABB(const spdAABB& aabb, fwBoxStreamerAssetFlags assetFlags)
{
	m_slotList.ResetCount();
	GetIntersectingAABB(aabb, assetFlags, m_slotList);
	for (s32 i=0; i<m_slotList.GetCount(); i++)
	{
		s32 boxIndex = (s32) m_slotList[i];
		if (!m_pModule->GetPtr(strLocalIndex(boxIndex)) && CanStream(GetFlags(boxIndex)))
		{
			return false;
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	EnsureLoadedAboutPos
// PURPOSE:		ensure that the assets for the specified position
//				(disregarding the pre-stream distance) are all loaded
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::EnsureLoadedAboutPos(Vec3V_In vPos)
{
	if (!m_ensuredAssetFlags)
		return;

	bool bRequestsIssued = false;

	STRVIS_AUTO_CONTEXT(strStreamingVisualize::BOXSTREAMER);

	const spdSphere ensureVol(vPos, ScalarV(m_fEnsureRadius));
	atArray<u32> assetList;
	GetIntersectingAABB(spdAABB(ensureVol), m_ensuredAssetFlags, assetList, false);

	for (s32 i=0; i<assetList.GetCount(); i++)
	{
		u32 index = assetList[i];

		if ( CanStream(GetFlags(index)) && (GetAssetFlags(index)&m_ensuredAssetFlags) && (m_filterRequestAssetFlags&GetAssetFlags(index)) && !m_pModule->GetPtr(strLocalIndex(index)) )
		{
			bRequestsIssued = true;
#if !HEIGHTMAP_GENERATOR_TOOL
			Displayf("EnsureLoadedAboutpos: requesting %s", strStreamingEngine::GetObjectName(m_pModule->GetStreamingIndex(strLocalIndex(index))));
#endif // !HEIGHTMAP_GENERATOR_TOOL
			STR_TELEMETRY_MESSAGE_ERROR("(streaming)Critical asset not loaded yet: %s", strStreamingEngine::GetObjectName(m_pModule->GetStreamingIndex(index)));
			STRVIS_MARK_BLOCKED_FOR_STREAMING();
			m_pModule->StreamingRequest(strLocalIndex(index), STRFLAG_PRIORITY_LOAD|STRFLAG_DONTDELETE);

#if __ASSERT
			// rare bug where we find ourselves ensure-loading something that really ought to have been requested already.
			// suggests bad data / bounds or odd behaviour on the part of the requesting code
			if (!GetIsRequiredCurr(index))
			{
				Warningf("EnsureLoadedAboutPos (%4.2f, %4.2f, %4.2f) is requesting %s but it wasn't already marked as required (flags:%d, type:%s)",
					vPos.GetXf(), vPos.GetYf(), vPos.GetZf(),
					strStreamingEngine::GetObjectName(m_pModule->GetStreamingIndex(strLocalIndex(index))),
					GetFlags(index), apszBoxStreamerAssetTypes[GetAssetType(index)]
				);
			}
#endif	//__ASSERT
		}
	}
	if (bRequestsIssued)
	{
		//Warningf("Not enough in memory around player. Pausing the game until there is");
#if __BANK
		static bool dumped;
		static sysTimer dumpTimer;

		if (!sm_BlockingPermitted && m_bDumpInfoOnBlockload)
		{
			if (dumped)
			{
				// Don't spam - let's only have those once every 30 seconds or so.
				if (dumpTimer.GetTime() > 30.0f)
				{
					dumped = false;
				}
			}

			if (!dumped)
			{
				dumped = true;
				dumpTimer.Reset();
				Displayf("Blocked waiting for objects around player. Current requests:");

				strStreamingEngine::GetInfo().PrintRequestList();
				g_strStreamingInterface->FullMemoryReport();
			}
		}
#endif // __BANK

		strStreamingEngine::GetLoader().LoadAllRequestedObjects(true);

#if __BANK
/*		if (!sm_BlockingPermitted && m_bDumpInfoOnBlockload)
		{
			Assertf(sm_BlockingPermitted, "The game just froze because it wasn't able to stream data fast enough. See the TTY for info on what was missing, and what was requested.");
		}*/
#endif // __BANK
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ClearRequiredList
// PURPOSE:		flushes all entries from the required list
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::ClearRequiredList()
{
	atArray<s32>& previouslyRequired = m_requiredList.GetOldList();
	for (s32 i=0; i<previouslyRequired.GetCount(); i++)
	{
		s32 slotIndex = previouslyRequired[i];
		ClearFlags(slotIndex, FLAG_REQ_CURR);
	}
	m_requiredList.Flush();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CleanRequiredList
// PURPOSE:		removes unrequired entries from required list
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::CleanRequiredList()
{
	atArray<s32>& previouslyRequired = m_requiredList.GetOldList();

	for (s32 i=previouslyRequired.GetCount()-1; i>=0; --i)
	{
		const s32 slotIndex = previouslyRequired[i];

		if ((m_pModule->GetStreamingFlags(strLocalIndex(slotIndex)) & STR_DONTDELETE_MASK)==0)
		{
			// no longer required so clean up from list
			ClearFlags(slotIndex, FLAG_REQ_CURR);
			previouslyRequired.DeleteFast(i);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GenerateSearchList
// PURPOSE:		starting with the specified primary stream position, generate
//				list of searches to be performed
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::GenerateSearchList(const fwBoxStreamerSearch& primarySearch, atArray<fwBoxStreamerSearch>& searchList)
{
	Assertf(primarySearch.GetType()==fwBoxStreamerSearch::TYPE_PRIMARY, "Invalid primary search");

	// 8/29/12 - cthomas - Don't Reset() the array every frame - this will cause un-necessary deletes and re-allocations.
	// According to Ian, the optimal searchList array size is fairly small and should be the default size of atArray, so 
	// we should have a fairly small, stable array. Simply call ResetCount() before generating the list again.
	searchList.ResetCount();
	searchList.Grow() = primarySearch;

	if (m_bSearchSecondaryPos)
	{
		searchList.Grow() = fwBoxStreamerSearch(m_vSecondaryPos, fwBoxStreamerSearch::TYPE_SECONDARY, m_requiredAssetFlags);
	}

	ms_pGameInterface->GetGameSpecificSearches(searchList, m_supportedAssetFlags);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetRequired
// PURPOSE:		performs search and marks required flags
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::SetRequired(fwBoxStreamerSearch& searchVolume)
{
	if ((searchVolume.GetAssetFlags() & m_supportedAssetFlags)==0) { return; }

	atArray<s32>& requiredList = m_requiredList.GetNewList();

	const spdAABB& searchBox = searchVolume.GetBox();

	m_slotList.ResetCount();

	// TODO tidy this up
	if (searchVolume.IsAABB())
	{
		GetIntersectingAABB(searchBox, searchVolume.GetAssetFlags(), m_slotList, searchVolume.IncludePreStream());

		//////////////////////////////////////////////////////////////////////////
		// burn this after V
		if (searchVolume.HasCamPosAndDir())
		{
			Vec3V vCamPos = searchVolume.GetLinePos1();
			Vec3V vCamDir = searchVolume.GetLinePos2();
			for (int i=0; i<m_slotList.GetCount(); i++)
			{
				const spdAABB& aabb = GetBounds( m_slotList[i] );
				const spdSphere& sphere = aabb.GetBoundingSphere();
				const Vec3V vTmp = Normalize( sphere.GetCenter() - vCamPos );
				ScalarV dot = Dot( vTmp, vCamDir);
				
				if (!sphere.ContainsPoint(vCamPos) && dot.Getf()<0.0f )
				{
					m_slotList.DeleteFast(i);
					i--;
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////
	}
	else if (searchVolume.IsLine())
	{
		GetIntersectingLine(searchVolume.GetLinePos1(), searchVolume.GetLinePos2(), searchVolume.GetAssetFlags(), m_slotList, searchVolume.IncludePreStream());
	}	

	if (0 && searchVolume.GetIgnoreCulling())
	{
		for (s32 i=0; i<m_slotList.GetCount(); i++)
		{
			s32 slotIndex = (s32) m_slotList[i];

			if ((m_aFlags[slotIndex] & FLAG_IGNORE) == 0)
			{
				SetFlags(slotIndex, FLAG_REQ_CURR);
				requiredList.PushAndGrow(slotIndex);
			}
		}
	}
	else
	{
		for (s32 i=0; i<m_slotList.GetCount(); i++)
		{
			s32 slotIndex = (s32) m_slotList[i];

			fwBoxFlags flags = m_aFlags[slotIndex];
			if (CanStream(flags))
			{
				SetFlags(slotIndex, FLAG_REQ_CURR);
				requiredList.PushAndGrow(slotIndex);
			}
		}
	}
}

#if !__FINAL

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SetRequired2D
// PURPOSE:		performs search and marks required flags
//				(2D version for nav mesh export)
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::SetRequired2D(fwBoxStreamerSearch& searchVolume)
{
	if ((searchVolume.GetAssetFlags() & m_supportedAssetFlags)==0) { return; }

	const spdAABB& searchBox = searchVolume.GetBox();
	atArray<s32>& requiredList = m_requiredList.GetNewList();

	for (u32 i=0; i<m_numEntries; i++)
	{
		if (m_aBoxes[i].IntersectsAABBFlat(searchBox) && m_aBoxes[i].IsValid())
		{
			if ((GetAssetFlags(i)&searchVolume.GetAssetFlags())!=0)
			{
				fwBoxFlags flags = m_aFlags[i];
				if (CanStream(flags))
				{
					SetFlags(i, FLAG_REQ_CURR);
					requiredList.PushAndGrow(i);
				}
			}
		}
	}
}

#endif	//!__FINAL

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequestRequired
// PURPOSE:		requests streamable assets marked with required flag
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::RequestRequired(Vec3V_In vPriorityPos, u32 extraStreamingFlags)
{
	STRVIS_AUTO_CONTEXT(strStreamingVisualize::BOXSTREAMER);

	// get priority sphere - anything being requested which intersects this sphere is made a priority request
	const spdSphere prioritySphere(vPriorityPos, ScalarV(m_fPriorityRadius));
	const spdAABB priorityAABB(prioritySphere);

	//////////////////////////////////////////////////////////////////////////
	atArray<s32>& oldList = m_requiredList.GetOldList();
	atArray<s32>& newList = m_requiredList.GetNewList();

	// request as DONTDELETE the assets that are needed now, but weren't needed in the previous frame
	for (s32 i=0; i<newList.GetCount(); i++)
	{
		s32 slotIndex = newList[i];
		strIndex streamingIndex = m_pModule->GetStreamingIndex( strLocalIndex(newList[i]) );

		if ( !strStreamingEngine::GetInfo().HasObjectLoaded(streamingIndex) && (m_filterRequestAssetFlags&GetAssetFlags(slotIndex)) )
		{
			// not yet in memory, so issue request
			strStreamingEngine::GetInfo().RequestObject(streamingIndex,
				extraStreamingFlags | STRFLAG_DONTDELETE | (IsPriorityAsset(slotIndex, priorityAABB) ? STRFLAG_PRIORITY_LOAD : 0)
			);

		}
		else if ( !GetIsRequiredPrev(slotIndex) )
		{
			// already in memory - so only change the DONTDELETE flag on the transition
			// from not required to required
			strStreamingEngine::GetInfo().SetRequiredFlag(streamingIndex, STRFLAG_DONTDELETE);
		}
	}

	// clear DONTDELETE flags for assets that were needed previous frame, but aren't now
	for (s32 i=0; i<oldList.GetCount(); i++)
	{
		u32 slotIndex = oldList[i];
		if (!GetIsRequiredCurr(slotIndex))
		{
			m_pModule->ClearRequiredFlag(slotIndex, STRFLAG_DONTDELETE);
		}
		ClearFlags(slotIndex, FLAG_REQ_PREV);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		initialise variable box streamer specific data
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamerVariable::Init(strStreamingModule* pModule, float fPriorityRadius)
{
	fwBoxStreamer::Init(pModule, fPriorityRadius);

	m_aInflatedBoxes.Resize(pModule->GetMaxSize());
	for (s32 i=0; i<pModule->GetMaxSize(); i++)
	{
		m_aInflatedBoxes[i].Invalidate();
	}

	m_bFullyInitialised = false;
}

void fwBoxStreamerVariable::InitSlot(s32 index, fwBoxStreamerAsset::eType assetType) 
{ 
	fwBoxStreamer::InitSlot(index, assetType);

	// Post process has already been called, but we are adding a new slot
	if (m_bFullyInitialised)
	{
		m_pendingPostProcess.PushAndGrow(index, 1);
	}
}

void fwBoxStreamerVariable::PostProcessPending()
{
	fwBoxStreamer::PostProcess();

	if (m_pInflatedTree)
	{
		delete m_pInflatedTree;
		m_pInflatedTree = NULL;
	}

	// Only inflate newly added slots here
	for (s32 i = 0; i < m_pendingPostProcess.GetCount(); i++)
	{
		u32 index = m_pendingPostProcess[i];
		const float fPreStream = GetPreStream(index);
		spdAABB& boxBeforeInflation = GetBounds(index);
		spdAABB& box = m_aInflatedBoxes[index];

		if (boxBeforeInflation.IsValid())
		{
			if (!box.IsValid())
			{
				box = boxBeforeInflation;
			}

			box.GrowUniform(ScalarV(fPreStream));
		}
	}

	m_pInflatedTree = rage_new MNBVHSimpleInterface(m_aInflatedBoxes);
	m_bFullyInitialised = true;
	m_pendingPostProcess.ResetCount();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PostProcess
// PURPOSE:		create array of inflated bounds, and tree structure etc
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamerVariable::PostProcess()
{
	fwBoxStreamer::PostProcess();

	if (m_pInflatedTree)
	{
		delete m_pInflatedTree;
		m_pInflatedTree = NULL;
	}
	
	for (u32 i=0; i<m_numEntries; i++)
	{
		const float fPreStream = GetPreStream(i);
		spdAABB& boxBeforeInflation = GetBounds(i);
		spdAABB& box = m_aInflatedBoxes[i];

		if (boxBeforeInflation.IsValid())
		{
			if (!box.IsValid())
			{
				box = boxBeforeInflation;
			}
			box.GrowUniform(ScalarV(fPreStream));
		}
	}

	m_pInflatedTree = rage_new MNBVHSimpleInterface(m_aInflatedBoxes);
	m_bFullyInitialised = true;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIntersectingAABB
// PURPOSE:		faster version of get intersecting that uses two trees - one for asset bounds, and one
//				for the inflated asset bounds
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamerVariable::GetIntersectingAABB(const spdAABB& bbox, fwBoxStreamerAssetFlags assetFlags,
													atArray<u32>& slotList, bool bIncludePreStream)
{
	if ((assetFlags & m_supportedAssetFlags) == 0)
	{
		return;
	}

	MNBVHSimpleInterface* pTree = bIncludePreStream ? m_pInflatedTree : m_pTree;
	int numPrelimResults = pTree->PerformBoxSearch(bbox, ms_tempResults, MAX_SEARCH_RESULTS);
	for (s32 i=0; i<numPrelimResults; i++)
	{
		s32 index = pTree->GetSearchResult(ms_tempResults, i);
		if ( (GetAssetFlags(index)&assetFlags)!=0 && !GetIsIgnored(index) )
		{
			slotList.PushAndGrow(index);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetIntersectingLine
// PURPOSE:		get the assets intersecting with line segment
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamerVariable::GetIntersectingLine(Vec3V_In vPos1, Vec3V_In vPos2, fwBoxStreamerAssetFlags assetFlags,
												atArray<u32>& slotList, bool bIncludePreStream)
{
	if ((assetFlags & m_supportedAssetFlags) == 0)
	{
		return;
	}

	MNBVHSimpleInterface* pTree = bIncludePreStream ? m_pInflatedTree : m_pTree;
	int numPrelimResults = pTree->PerformLineSearch(vPos1, vPos2, ms_tempResults, MAX_SEARCH_RESULTS);
	for (s32 i=0; i<numPrelimResults; i++)
	{
		s32 index = pTree->GetSearchResult(ms_tempResults, i);
		if ( (GetAssetFlags(index)&assetFlags)!=0 && !GetIsIgnored(index) )
		{
			slotList.PushAndGrow(index);
		}
	}
}

#if __BANK

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	InitWidgets
// PURPOSE:		debug widgets for box streamer
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::InitWidgets(bkBank& bank, const char* pName)
{
	bank.PushGroup(pName);
	{
		bank.AddToggle("Show active searches", &m_bDisplaySearchList);
		bank.AddToggle("Display loaded list", &m_bDisplayLoadedList);
		bank.AddToggle("Display loaded on vmap", &m_bDebugDisplayInMemory);
		bank.AddToggle("Display including prestream", &m_bDebugDisplayIncludesPreStream);
		bank.AddToggle("Display intersecting but unloaded", &m_bDebugDisplayIntersectingButUnloaded);
		bank.AddToggle("Dump information when blockloading", &m_bDumpInfoOnBlockload);
		bank.AddSeparator("");
		bank.AddToggle("Has loaded about selected?", &m_bDisplayHasLoadedForSelected);
		for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
		{
			bank.AddToggle(apszBoxStreamerAssetTypes[i], &m_abDebugSearchTypes[i]);
		}
		bank.AddSeparator("");
		bank.AddToggle("Override stream range scale", &m_bDebugOverrideStreamRangeScale);
		for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
		{
			bank.AddSlider(apszBoxStreamerAssetTypes[i], &m_afDebugStreamScale[i], 1.0f, 10.0f, 0.1f);
		}
	}
	bank.PopGroup();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetUnloadedAboutPosn
// PURPOSE:		debug fn to return list of intersecting boxes not currently loaded
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::GetUnloadedAboutPos(Vec3V_In posn, atArray<u32>& aIndices)
{
	spdSphere testSphere(posn, ScalarV(m_fHasLoadedSearchRadius));
	for (u32 i=0; i<m_numEntries; i++)
	{
		if (m_aBoxes[i].IntersectsSphere(testSphere) && m_aBoxes[i].IsValid())
		{
			if (CanStream(GetFlags(i)) && m_pModule->GetPtr(strLocalIndex(i))==NULL)
			{
				aIndices.PushAndGrow(i);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	UpdateDebug
// PURPOSE:		apply debug settings etc
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::UpdateDebug()
{
	if (m_bDebugOverrideStreamRangeScale)
	{
		for (u32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
		{
			m_afSearchRadiusScaleByType[i] = m_afDebugStreamScale[i];
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RenderDebug
// PURPOSE:		vmap display of objects in memory etc
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::RenderDebug(Color32* colours)
{
	if (m_bDisplayHasLoadedForSelected)
	{
		fwEntity* pEntity = g_PickerManager.GetSelectedEntity();
		if (pEntity)
		{
			fwBoxStreamerAssetFlags assetFlags = 0;
			for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
			{
				if (m_abDebugSearchTypes[i])
				{
					assetFlags |= (1 << i);
				}
			}

			Vec3V vPos = pEntity->GetTransform().GetPosition();
			if (HasLoadedAboutPos(vPos, assetFlags))
			{
				grcDebugDraw::AddDebugOutputEx( false, Color32(0,255,0),
					"%s HasLoadedAboutPos: all assets loaded around entity %s",
					m_pModule->GetModuleName(),
					pEntity->GetModelName() );
			}
			else
			{
				grcDebugDraw::AddDebugOutputEx( false, Color32(255,0,0),
					"%s HasLoadedAboutPos: all assets NOT loaded around entity %s",
					m_pModule->GetModuleName(),
					pEntity->GetModelName() );

				const spdSphere testSphere(vPos, ScalarV(m_fEnsureRadius));
				atArray<u32> slotList;
				GetIntersectingAABB(spdAABB(testSphere), assetFlags, slotList);
				for (s32 i=0; i<slotList.GetCount(); i++)
				{
					s32 slot = (s32) slotList[i];
					if (CanStream(GetFlags(slot)))
					{
						bool loaded = m_pModule->GetPtr(strLocalIndex(slot)) != NULL;

						grcDebugDraw::AddDebugOutputEx( false,
							loaded ? Color32(0,255,0) : Color32(255,0,0),
							"...%s loaded asset type %s name %s flags: %s%s%s",
							loaded ? "" : " NOT",
							apszBoxStreamerAssetTypes[GetAssetType(slot)],
							m_pModule->GetName(strLocalIndex(slot)),
							( GetIsIgnored(slot) ? " IGNORED" : "" ),
							( GetIsCulled(slot) ? " CULLED" : "" ),
							( GetIsPinned(slot) ? " PINNED" : "" )
							);
					}
				}
			}
		}
	}

	if (m_bDisplayLoadedList)
	{
		char achTmp[256];

		for(s32 i=0; i<m_pModule->GetCount(); i++)
		{
			if(m_pModule->GetPtr(strLocalIndex(i)))
			{
				fwBoxFlags flags = GetFlags(i);
				sprintf(achTmp, "%s%s", (flags&FLAG_PIN_VOL)?"PINNED_VOL":"", (flags&FLAG_PIN_USR)?"PINNED_USR":"");
				grcDebugDraw::AddDebugOutputEx(false, Color32(255,255,255),
					"Loaded (%d): %s (type: %s) %s",
					i, m_pModule->GetName(strLocalIndex(i)), apszBoxStreamerAssetTypes[GetAssetType(i)], achTmp
					);
			}
		}
	}

	if (m_bDisplaySearchList)
	{
		for (s32 i=0; i<m_searches.GetCount(); i++)
		{
			fwBoxStreamerSearch& currentSearch = m_searches[i];
			fwBoxStreamerSearch::eSearchType searchType = currentSearch.GetType();
			fwBoxStreamerAssetFlags flags = currentSearch.GetAssetFlags() & m_supportedAssetFlags;
			Vec3V vPos = currentSearch.GetBox().GetCenter();


			grcDebugDraw::AddDebugOutputEx(false, Color32(255,255,255),
				"%s active search %s at (%4.2f, %4.2f, %4.2f) for assets %s %s %s %s %s %s %s %s",
				m_pModule->GetModuleName(), fwBoxStreamerSearch::sm_TypeNames[searchType],
				vPos.GetXf(), vPos.GetYf(), vPos.GetZf(),
				( (flags & fwBoxStreamerAsset::FLAG_STATICBOUNDS_MOVER)!=0		? "ASSET_STATICBOUNDS_MOVER" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_STATICBOUNDS_WEAPONS)!=0	? "ASSET_STATICBOUNDS_WEAPONS" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_STATICBOUNDS_MATERIAL)!=0	? "ASSET_STATICBOUNDS_MATERIAL" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_MAPDATA_HIGH)!=0			? "ASSET_MAPDATA_HIGH" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_MAPDATA_HIGH_CRITICAL)!=0	? "ASSET_MAPDATA_HIGH_CRITICAL" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_MAPDATA_MEDIUM)!=0			? "ASSET_MAPDATA_MEDIUM" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_MAPDATA_LOW)!=0				? "ASSET_MAPDATA_LOW" : "" ),
				( (flags & fwBoxStreamerAsset::FLAG_METADATA)!=0				? "ASSET_METADATA" : "" )
			);
			if (currentSearch.HasCamPosAndDir())
			{
				const Vec3V vCamPos = currentSearch.GetLinePos1();
				const Vec3V vCamDir = currentSearch.GetLinePos2();

				grcDebugDraw::AddDebugOutputEx(false, Color32(255,255,255),
					"... has camera pos (%4.2f, %4.2f, %4.2f) and dir (%4.2f, %4.2f, %4.2f)",
					vCamPos.GetXf(), vCamPos.GetYf(), vCamPos.GetZf(),
					vCamDir.GetXf(), vCamDir.GetYf(), vCamDir.GetZf()
				);
			}
		}
	}

	if(m_bDebugDisplayInMemory || m_bDebugDisplayIntersectingButUnloaded)
	{
		// first search on list is always primary search
		fwBoxStreamerSearch& primarySearch = m_searches[0];
		Assert(primarySearch.GetType()==fwBoxStreamerSearch::TYPE_PRIMARY);

		char achTmp[256];

		// Get the world position of the mouse over the Vector Map.
		float worldX, worldY;

		// Get the raw mouse position.
		const float  mouseScreenX = static_cast<float>(ioMouse::GetX());
		const float  mouseScreenY = static_cast<float>(ioMouse::GetY());

		// Convert the mouse position into vector map coordinates.
		// Scale mouse position so x and y are between 0 and 1.
		const float mapX = mouseScreenX / grcViewport::GetDefaultScreen()->GetWidth();
		const float mapY = mouseScreenY / grcViewport::GetDefaultScreen()->GetHeight();

		// Convert the VectorMap coordinates into World coordinates.
		fwVectorMap::ConvertPointMapToWorld(mapX, mapY, worldX, worldY);

		// If in memory draw bounding box
		float screenY = mouseScreenY;
		for(s32 i=0; i<m_pModule->GetCount(); i++)
		{
			const spdAABB bb = m_bDebugDisplayIncludesPreStream ? GetStreamingBoundsCopy(i) : GetBoundsCopy(i);
			bool bDisplayDetails = false;
			bool bDisplayBox = false;
			Color32 boxCol = colours[GetAssetType(i)];
			s32 width = (s32) fabsf(bb.GetMaxVector3().x - bb.GetMinVector3().x);
			s32 height = (s32) fabsf(bb.GetMaxVector3().y - bb.GetMinVector3().y);

			if(m_bDebugDisplayInMemory && m_pModule->GetPtr(strLocalIndex(i)))
			{
				bDisplayBox = true;
				bDisplayDetails = bb.ContainsPointFlat(Vec2V(worldX,worldY));
				if (!m_pModule->IsObjectRequired(strLocalIndex(i)))
				{
					boxCol = Color32(128,128,128);
				}
				if(bDisplayDetails)
				{
					sprintf(achTmp, "%d:%s (w=%dm, h=%dm)", i, m_pModule->GetName(strLocalIndex(i)), width, height);
				}
			}
			else if (m_bDebugDisplayIntersectingButUnloaded && !m_pModule->GetPtr(strLocalIndex(i)) && primarySearch.Intersects(bb))
			{
				bDisplayBox = true;
				bDisplayDetails = bb.ContainsPointFlat(Vec2V(worldX,worldY));
				boxCol = Color32(0,0,255);
				if(bDisplayDetails)
				{
					sprintf(achTmp, "%d:%s (w=%dm, h=%dm) %s", i, m_pModule->GetName(strLocalIndex(i)), width, height,
						(GetIsCulled(i) ? "CULLED" : "") );
				}
			}

			if (bDisplayBox)
			{
				fwVectorMap::Draw3DBoundingBoxOnVMap(bb, boxCol);
			}
			if (bDisplayDetails)
			{
				const int DEBUG_CHAR_HEIGHT	= 11;
				const int DEBUG_CHAR_WIDTH = 9;

				grcDebugDraw::Text(mouseScreenX/DEBUG_CHAR_WIDTH+3.f, screenY/DEBUG_CHAR_HEIGHT, achTmp);
				screenY += DEBUG_CHAR_HEIGHT;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Print
// PURPOSE:		debug text about specified asset
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::Print(int index, char* text, size_t maxSize) const
{
	formatf(text, maxSize, "%s", apszBoxStreamerAssetTypes[GetAssetType(index)]);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetStreamingBoundsCopy
// PURPOSE:		debug copy of inflated bounds
//////////////////////////////////////////////////////////////////////////
const spdAABB fwBoxStreamer::GetStreamingBoundsCopy(s32 index)
{
	spdAABB aabb = m_aBoxes[index];
	u8 assetType = GetAssetType(index);
	aabb.GrowUniform( ScalarV(m_afSearchRadiusByType[assetType] * m_afSearchRadiusScaleByType[assetType]) );
	return aabb;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RebuildTrees
// PURPOSE:		debug widget to rebuild mnbvh trees (e.g. if assets have changed bounds)
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::RebuildTrees()
{
	if (m_pTree)
	{
		delete m_pTree;
		m_pTree = rage_new MNBVHSimpleInterface(m_aBoxes);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetCosts
// PURPOSE:		sums up memory costs of assets required
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamer::GetCosts(u32& main, u32& vram)
{
	for (u32 i=0; i<m_numEntries; i++)
	{
		if (GetIsRequiredCurr((s32) i))
		{
			strIndex streamingIndex = m_pModule->GetStreamingIndex(strLocalIndex((s32)i));
			strStreamingInfo& info = *strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex);
			main += info.ComputeOccupiedVirtualSize(streamingIndex, false);
			vram += info.ComputePhysicalSize(streamingIndex, false);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RebuildTrees
// PURPOSE:		debug widget to rebuild mnbvh trees (e.g. if assets have changed bounds)
//////////////////////////////////////////////////////////////////////////
void fwBoxStreamerVariable::RebuildTrees()
{
	fwBoxStreamer::RebuildTrees();

	if (m_pInflatedTree)
	{
		delete m_pInflatedTree;
		m_pInflatedTree = rage_new MNBVHSimpleInterface(m_aInflatedBoxes);
	}
}

#endif	//__BANK

}	//namespace rage

