
// --- Include Files ------------------------------------------------------------

#include "clipdictionarystore.h"

#include "crclip/clipanimation.h"
#include "crclip/clipdictionary.h"
#include "crclip/clipanimation.h"
#include "crclip/clipanimations.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "file\asset.h"
#include "file\packfile.h"
#include "fwsys/fileExts.h"
#include "fwsys\timer.h"
#include "paging\rscbuilder.h"
#include "streaming\defragmentation.h"
#include "streaming\streaming_channel.h"
#include "system/param.h"

namespace rage {


// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

fwClipDictionaryStore g_ClipDictionaryStore;
#if __BANK
atMap<int, atString>  g_ClipDictionarySource;
#endif

PARAM(nomtclipdictplace, "Place clip dictionary resources on a separate thread");
PARAM(validateclips, "Validate clips in ASSERT builds when loaded for possible errors");

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

// --- fwClipDictionaryDef -------------------------------------------------------

const crClip* fwClipDictionaryDef::GetClip(strLocalIndex clipIndex)
{
	const crClip* pClip = NULL;
	Assert(m_pObject);
	if (m_pObject)
	{
		Assert( clipIndex.Get() < (s32)m_pObject->GetNumClips() );
		pClip = m_pObject->FindClipByIndex(clipIndex.Get());
		Assert(pClip);
	}
	return pClip;
}

const crClip* fwClipDictionaryDef::GetClipIfExists(strLocalIndex clipIndex)
{
	const crClip* pClip = NULL;
	if (m_pObject)
	{
		if ( clipIndex.Get() < (s32)m_pObject->GetNumClips())
		{
			pClip = m_pObject->FindClipByIndex(clipIndex.Get());
		}
	}
	return pClip;
}

const crClip* fwClipDictionaryDef::FindClip(s32 hashKey)
{
	const crClip* pClip = NULL;
	Assert(m_pObject);
	if (m_pObject)
	{
		pClip = m_pObject->GetClip(hashKey);
		Assert(pClip);
	}
	return pClip;
}

const crClip* fwClipDictionaryDef::FindClipIfExists(s32 hashKey)
{
	const crClip* pClip = NULL;
	if (m_pObject)
	{
		pClip = m_pObject->GetClip(hashKey);
	}
	return pClip;
}

// --- fwClipDictionaryStore --------------------------------------------------------------------------------------

fwClipDictionaryStore::fwClipDictionaryStore() : fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>("AnimStore", ANIM_FILE_ID, CONFIGURED_FROM_FILE, 1170, true, ANIM_FILE_VERSION)
#if MAP_DICTIONARY_REFERENCES
 ,m_Map(atMapHashFn<const crAnimation*>(),atMapEquals<const crAnimation*>(), m_Pool.m_Functor)
#endif
{
#if MAP_DICTIONARY_REFERENCES
	m_Pool.Create<MapEntry>(1024);	//The pool is created with the relevant entry type.
	m_Map.Create(1024, false);	//Create our entry array and make sure it doesn't get resized to prevent dynamic access
#endif

	ResetGameInterface();
}

/*
//
// name:		ForAllAddRef
// description:	Add a ref to each clip in the anim dict
static bool ForAllAddRef(const crClip* pClip, u32, void*)
{
	if (pClip)
		pClip->AddRef();

	return true;
}

//
// name:		ForAllRelease
// description:	Release each clip in the anim dict
static bool ForAllRelease(const crClip* pClip, u32, void*)
{
	if (pClip)
		pClip->Release();

	return true;
}
*/

#if MAP_DICTIONARY_REFERENCES

static bool TallyAnimReferenceCounts(const crClip* pClip, u32, void* ptr)
{
	fwClipDictionaryStore* that = reinterpret_cast<fwClipDictionaryStore*>(ptr);

	switch(pClip->GetType())
	{
	case crClip::kClipTypeAnimation:
	case crClip::kClipTypeAnimationExpression:
		{
			const crClipAnimation* pClipAnim = static_cast<const crClipAnimation*>(pClip);
			const crAnimation* pAnim = pClipAnim->GetAnimation();
			int* count = that->m_Map.Access(pAnim);
			if (count) 
			{
				*count += 1;
			}
			else 
			{
				that->m_Map.Insert(pAnim, 2); // +1 is the reference for the dictionary
			}
		}
		break;

	default:
		break;
	}

	return true;
}

//
// name:		IsDictionaryUnused
// description:	Is the dictionary unreferenced
// note: Need to call TallyAnimReferenceCount for this to work properly now
static bool IsDictionaryUnused(const crClip* pClip, u32, void* ptr)
{
	fwClipDictionaryStore* that = reinterpret_cast<fwClipDictionaryStore*>(ptr);

	// Check that the clip isn't still referenced
	bool unused = true;

	if (pClip->GetRef() != 1)
	{
//		Displayf("Clip name (%s) being deleted with reference count %d ", pClip->GetName(), pClip->GetRef());
		unused = false;
	}

	switch(pClip->GetType())
	{
	case crClip::kClipTypeAnimation:
	case crClip::kClipTypeAnimationExpression:
		{
			const crClipAnimation* pClipAnim = static_cast<const crClipAnimation*>(pClip);
			const crAnimation* pAnim = pClipAnim->GetAnimation();
			
			if (Verifyf (pAnim, "crClipAnimation %s has no animation!", pClip->GetName()))
			{
				int* count = that->m_Map.Access(pAnim);
				Assert(count);

				if (*count != pAnim->GetRef())
				{
	//				Displayf("Anim name (%s) being deleted with reference count %d when it should be %d", pAnim->GetName(), pAnim->GetRef(), *count);
					unused = false;
				}
			}
		}
		break;

	default:
		break;
	}

	return unused;
}

#endif // MAP_DICTIONARY_REFERENCES


#if __ASSERT
struct ValidateAnimPayload
{
	const char* m_DictionaryName;
	const crClip* m_Clip;
};

bool ValidateAnimCB(const crAnimation* anim, float start, float end, float, void* data)
{
	ValidateAnimPayload& payload = *reinterpret_cast<ValidateAnimPayload*>(data);
	
	if(anim)
	{
		float duration = anim->GetDuration();

		Assertf(InRange(start, 0.f, duration), "ValidateClipCB - dictionary '%s' clip '%s' start %f outside anim '%s' duration %f", payload.m_DictionaryName, payload.m_Clip->GetName(), start, anim->GetName(), duration);
		Assertf(InRange(end, 0.f, duration), "ValidateClipCB - dictionary '%s' clip '%s' end %f outside anim '%s' duration %f", payload.m_DictionaryName, payload.m_Clip->GetName(), end, anim->GetName(), duration);
	}

	return true;
}

bool ValidateClipCB(const crClip* clip, crClipDictionary::ClipKey UNUSED_PARAM(key), void* data)
{
	const char* dictionaryName = (const char*)data;

	if(clip)
	{
		ValidateAnimPayload payload;
		payload.m_DictionaryName = dictionaryName;
		payload.m_Clip = clip;

		clip->ForAllAnimations(ValidateAnimCB, &payload);
		
		if(clip->GetTags())
		{
			const crTags& tags = *clip->GetTags();
			for(int n=0; n<tags.GetNumTags(); ++n)
			{
				const crTag& tag = *tags.GetTag(n);
				
				Assertf(InRange(tag.GetStart(), 0.f, 1.f) && InRange(tag.GetEnd(), 0.f, 1.f), "ValidateClipCB - dictionary '%s' clip '%s' tag[%d] illegal start %f or end %f", dictionaryName, clip->GetName(), n, tag.GetStart(), tag.GetEnd());
				//Assertf(tag.GetStart() <= tag.GetEnd(), "ValidateClipCB - dictionary '%s' clip '%s' tag[%d] start %f after end %f (this is legal in RAGE, but not used in GTA5)", dictionaryName, clip->GetName(), n, tag.GetStart(), tag.GetEnd());
			}
		}
	}

	return true;
}

void ValidateClipDictionary(const crClipDictionary& clipDict, const char* clipDictName)
{
	if(PARAM_validateclips.Get())
	{
		clipDict.ForAll(ValidateClipCB, (void*)clipDictName);
	}
}
#endif // __ASSERT

//
// name:		Set
// description:	Set clip dictionary 
void fwClipDictionaryStore::Set(strLocalIndex index, crClipDictionary* m_pObject)
{
	Assert(index.Get() >= 0);
	fwClipDictionaryDef* pDef = GetSlot(index);

	Assertf(pDef, "No anim dict at this slot");
	Assertf(pDef->m_pObject==NULL, "Anim dict is already in memory");

	UPDATE_KNOWN_REF(pDef->m_pObject, m_pObject);
	if (pDef->m_pObject)
	{
		pDef->m_pObject->AddRef();
		
#if __ASSERT
		ValidateClipDictionary(*m_pObject, pDef->m_name.GetCStr());
#endif // __ASSERT

		// Add a reference to each clip in the anim dict
		//pDef->m_pObject->ForAll(&ForAllAddRef, NULL);		

		//Displayf("Anim dict (%s) being added with reference count %d ", pDef->m_name.GetCStr(), pDef->m_pObject->GetRefCount());
	}
}

#if !USE_DICTIONARY_REF_COUNTS

int fwClipDictionaryStore::GetNumRefs(strLocalIndex index) const 
{
//	Displayf("fwClipDictionaryStore::GetNumRefs");

	Assert(index >= 0);
	const fwClipDictionaryDef* pDef = GetSlot(index);
	Assertf(pDef, "No object at this slot");

	// only do the costly ref check if it looks like this dictionary isn't in use anymore, and is streamed in
	if (pDef->m_refCount != 0 || !pDef->m_pObject) 
	{
		return pDef->m_refCount;
	}

	// now make sure something else isn't using the dictionary
	m_Map.Reset();
	pDef->m_pObject->ForAll(&TallyAnimReferenceCounts, const_cast<fwClipDictionaryStore*>(this));
	int used = pDef->m_pObject->ForAll(&IsDictionaryUnused, const_cast<fwClipDictionaryStore*>(this)) ? 0 : 1;

	return pDef->m_refCount + used;
}
#else 

int fwClipDictionaryStore::GetNumRefs(strLocalIndex index) const 
{
	Assert(index.Get() >= 0);
	const fwClipDictionaryDef* pDef = GetSlot(index);
	Assertf(pDef, "No object at this slot");

	int numRefs = fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>::GetNumRefs(index);

	crClipDictionary* object = pDef->m_pObject;
	if (object)
	{
		// fwClipDictionaryStore has a reference the crClipDictionary.
		// Take that into account (-1).
		Assert(object->GetRefCount() > 0);
		numRefs += object->GetRefCount()-1;
	}

	return numRefs;
}

#endif // USE_CLIP_REF_COUNTS

//
// name:		Remove
// description:	Delete clip dictionary
void fwClipDictionaryStore::Remove(strLocalIndex index)
{
	fwClipDictionaryDef* pDef = GetSlot(index);
	//Displayf("Anim dict (%s) being deleted with reference count %d %d", pDef->m_name.GetCStr(), GetNumRefs(index), pDef->m_pObject->GetRefCount());

	Assertf(pDef, "No anim dict at this slot");
	Assertf(pDef->m_pObject!=NULL, "Anim dict is not in memory");

	// Release each clip in the anim dict
	//pDef->m_pObject->ForAll(&ForAllRelease, NULL);

#if DICTIONARY_IN_USE_CHECK
	m_Map.Reset();
	pDef->m_pObject->ForAll(&TallyAnimReferenceCounts, this);
//	pDef->m_pObject->ForAll(&UpdateAnimReferenceCounts, this);
	Assertf(pDef->m_pObject->ForAll(&IsDictionaryUnused, this), "Anim dict (%s) being deleted when it is still in use!", pDef->m_name.GetCStr());
#endif
	SAFE_REMOVE_KNOWN_REF(pDef->m_pObject);
	(void)Verifyf(pDef->m_pObject->Release()==0, "Failed to delete the animation");
	pDef->m_pObject = NULL;

	//Notify the game interface.
	m_pGameInterface->OnRemove(index);
}


bool fwClipDictionaryStore::Load(strLocalIndex index, void* pData, int size)
{
	//Call the base class version.
	bool bResult = fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>::Load(index, pData, size);

	//Notify the game interface.
	m_pGameInterface->OnLoad(index);

	return bResult;
}

void fwClipDictionaryStore::AddRef(strLocalIndex index, strRefKind strRefKind)
{
	//Call the base class version.
	fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>::AddRef(index, strRefKind);

	//Notify the game interface.
	m_pGameInterface->OnAddRef(index);
}

void fwClipDictionaryStore::RemoveRef(strLocalIndex index, strRefKind strRefKind)
{
	//Call the base class version.
	fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>::RemoveRef(index, strRefKind);

	//Notify the game interface.
	m_pGameInterface->OnRemoveRef(index);
}

void fwClipDictionaryStore::RemoveRefWithoutDelete(strLocalIndex index, strRefKind strRefKind)
{
	//Call the base class version.
	fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>::RemoveRefWithoutDelete(index, strRefKind);

	//Notify the game interface.
	m_pGameInterface->OnRemoveRefWithoutDelete(index);
}

void fwClipDictionaryStore::ResetAllRefs(strLocalIndex index)
{
	//Call the base class version.
	fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>::ResetAllRefs(index);

	//Notify the game interface.
	m_pGameInterface->OnResetAllRefs(index);
}


fwClipDictionaryStore::~fwClipDictionaryStore()
{
#if MAP_DICTIONARY_REFERENCES
	m_Map.Kill();
#endif
}

bool fwClipDictionaryStore::CanPlaceAsynchronously(strLocalIndex UNUSED_PARAM(index)) const
{
	return !(PARAM_nomtclipdictplace.Get());
}


} // namespace rage

