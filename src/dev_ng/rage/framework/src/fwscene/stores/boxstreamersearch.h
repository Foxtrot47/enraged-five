#ifndef _FWSCENE_STORES_BOXSTREAMERSEARCH_H_
#define _FWSCENE_STORES_BOXSTREAMERSEARCH_H_

#include "fwscene/stores/boxstreamerassets.h"
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"
#include "streaming/streamingvisualize.h"
#include "vectormath/vec3v.h"

namespace rage
{
	// represents a box streamer search volume and storage for results
	class fwBoxStreamerSearch
	{
	public:
		// NOTE: Keep the search types below in sync with
		// sm_TypeNames!!
		enum eSearchType
		{
			TYPE_OTHER,
			TYPE_PRIMARY,
			TYPE_SECONDARY,
			TYPE_SCRIPTENTITY,
			TYPE_STREAMINGVOLUME,
			TYPE_NAVMESHEXPORT,
			TYPE_SWITCH,
			TYPE_DEBUG,
            TYPE_NETWORK,
			TYPE_SRL,
			TYPE_RECENTVEHICLE,
			TYPE_INFLATE,

			MAX_TYPE
		};

		enum eSearchShape
		{
			SHAPE_AABB,
			SHAPE_LINE
		};

		fwBoxStreamerSearch()
			: m_type(TYPE_OTHER), m_assetFlags(0), m_bIncludePreStream(true), m_shape(SHAPE_AABB), m_bHasCamPosAndDir(false), m_bIgnoreCulling(false)
		{ }

		fwBoxStreamerSearch(Vec3V_In pos, eSearchType searchType, fwBoxStreamerAssetFlags assetFlags, bool bIncludePreStream=true)
			: m_type(searchType), m_aabb(spdSphere(pos, ScalarV(0.1f))), m_assetFlags(assetFlags), m_bIncludePreStream(bIncludePreStream), m_shape(SHAPE_AABB), m_bHasCamPosAndDir(false), m_bIgnoreCulling(false)
		{ }

		fwBoxStreamerSearch(const spdAABB& aabb, eSearchType searchType, fwBoxStreamerAssetFlags assetFlags, bool bIncludePreStream=true)
			: m_type(searchType), m_aabb(aabb), m_assetFlags(assetFlags), m_bIncludePreStream(bIncludePreStream), m_shape(SHAPE_AABB), m_bHasCamPosAndDir(false), m_bIgnoreCulling(false)
		{ }

		fwBoxStreamerSearch(const spdSphere& sphere, eSearchType searchType, fwBoxStreamerAssetFlags assetFlags, bool bIncludePreStream=true)
			: m_type(searchType), m_aabb(sphere), m_assetFlags(assetFlags), m_bIncludePreStream(bIncludePreStream),  m_shape(SHAPE_AABB), m_bHasCamPosAndDir(false), m_bIgnoreCulling(false)
		{ }

		fwBoxStreamerSearch(Vec3V_In vPos1, Vec3V_In vPos2, eSearchType searchType, fwBoxStreamerAssetFlags assetFlags, bool bIncludePreStream=true)
			: m_type(searchType), m_vLineSegPos1(vPos1), m_vLineSegPos2(vPos2), m_assetFlags(assetFlags), m_bIncludePreStream(bIncludePreStream),  m_shape(SHAPE_LINE), m_bHasCamPosAndDir(false), m_bIgnoreCulling(false)
		{ }

		eSearchType GetType() const						{ return m_type; }
		eSearchShape GetShape() const					{ return m_shape; }
		const spdAABB& GetBox() const					{ return m_aabb; }
		fwBoxStreamerAssetFlags GetAssetFlags() const	{ return m_assetFlags; }
		
		Vec3V_Out GetPos() const						{ return m_aabb.GetCenter(); }
		bool IncludePreStream() const					{ return m_bIncludePreStream; }

		bool IsAABB() const								{ return m_shape==SHAPE_AABB; }
		bool IsLine() const								{ return m_shape==SHAPE_LINE; }
		Vec3V_Out GetLinePos1() const					{ return m_vLineSegPos1; }
		Vec3V_Out GetLinePos2() const					{ return m_vLineSegPos2; }

		void SetIgnoreCulling(bool bIgnoreCulling)		{ m_bIgnoreCulling = bIgnoreCulling; }
		bool GetIgnoreCulling() const					{ return m_bIgnoreCulling; }
		void SetCamPosAndDir(Vec3V_In vPos, Vec3V_In vDir)
		{
			if (m_shape!=SHAPE_LINE)
			{
				// burn this code after V
				m_bHasCamPosAndDir = true;
				m_vLineSegPos1 = vPos;
				m_vLineSegPos2 = Normalize( vDir );
			}
		}
		bool HasCamPosAndDir() const { return m_bHasCamPosAndDir; }

#if !__FINAL
		bool Intersects(const spdAABB& aabb) const		{ return aabb.IntersectsAABB(m_aabb); }
		bool IntersectsFlat(const spdAABB& aabb) const { return aabb.IntersectsAABBFlat(m_aabb); }
		const char *GetTypeName() const;

#if STREAMING_VISUALIZE
		static void RegisterSearchTypesWithSV();
#endif // STREAMING_VISUALIZE

#endif	//!__FINAL

	private:
		spdAABB					m_aabb;
		eSearchType				m_type;
		fwBoxStreamerAssetFlags	m_assetFlags;
		bool					m_bIncludePreStream : 1;
		bool					m_bHasCamPosAndDir : 1;
		bool					m_bIgnoreCulling : 1;

		eSearchShape			m_shape;		// TODO find a better way of handling this
		Vec3V					m_vLineSegPos1;	// TODO find a better way of handling this
		Vec3V					m_vLineSegPos2;	// TODO find a better way of handling this

#if !__FINAL
	public:
		static const char *		sm_TypeNames[MAX_TYPE];
#endif // !__FINAL
	};

}	//namespace rage

#endif	//_FWSCENE_STORES_BOXSTREAMERSEARCH_H_