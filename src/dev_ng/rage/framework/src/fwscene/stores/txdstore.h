// 
// fwscene/stores/txdstore.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 


#ifndef FWSCENE_STORES_TXDSTORE_H_
#define FWSCENE_STORES_TXDSTORE_H_

#include "paging/dictionary.h"
#include "grcore/texture.h"

#include "fwtl/assetstore.h"

namespace rage {

typedef pgDictionary<grcTexture> fwTxd;

#if !__SPU


// Texture dictionary definition
class fwTxdDef : public fwAssetDef<fwTxd>
{
public:
	void Init(const strStreamingObjectName name)
	{
		fwAssetDef<fwTxd>::Init(name);

		m_parentId			= -1;

		m_isDummy			= 0;
		m_bIsBeingDefragged	= 0;
		m_bHasHD			= 0;
		m_bIsBoundHD		= 0;
	}

	strLocalIndex GetParentId() const
	{
		return strLocalIndex(m_parentId);
	}

	void SetParentId(s32 parentId)
	{
		m_parentId = parentId;
	}

private:
	s32	m_parentId;
public:

	u16 m_isDummy			: 1;
	u16 m_bIsBeingDefragged	: 1;
	u16 m_bHasHD			: 1;
	u16 m_bIsBoundHD		: 1;
	u16 m_padding			: 12;
};


class fwTxdStore : public fwAssetRscStore<fwTxd, fwTxdDef>
{
public:
	fwTxdStore();

	virtual void Set(strLocalIndex index, fwTxd* m_pObject);
	virtual void Remove(strLocalIndex index);
	virtual void RemoveSlot(strLocalIndex index);

	virtual void* Defragment(strLocalIndex index, datResourceMap& map, bool& flush);
	virtual void DefragmentComplete(strLocalIndex index);
	virtual void DefragmentPreprocess(strLocalIndex index);

	bool IsBeingDefragged(strLocalIndex index);
	bool GetIsHDCapable(strLocalIndex index);
	void SetIsHDCapable(strLocalIndex index, bool val);
	bool GetIsBoundHD(strLocalIndex index);
	void SetIsBoundHD(strLocalIndex index, bool val);

	grcTexture *GetTexture(strLocalIndex index, int dictionaryEntry)
	{
		if (index.IsValid())
		{
			fwTxd *txd = Get(index);
			if (txd)
				return txd->GetEntry(dictionaryEntry);
		}
		return NULL;
	}

	virtual int GetDependencies(strLocalIndex UNUSED_PARAM(index), strIndex *UNUSED_PARAM(pIndices), int indexArraySize) const;
	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual bool Load(strLocalIndex index, void* pData, int size);

	virtual strLocalIndex Register(const char* name);


	void ShutdownLevel();

	strLocalIndex GetParentTxdSlot(strLocalIndex index) const {return strLocalIndex(GetSlot(index)->GetParentId());}
#if !__ASSERT
	void SetParentTxdSlot(strLocalIndex index, strLocalIndex parentId) { FastAssert(index.Get() != parentId.Get()); GetSlot(index)->SetParentId(parentId.Get());}
#else // !__ASSERT
	void SetParentTxdSlot(strLocalIndex index, strLocalIndex parentId);
#endif // !__ASSERT

	void SetupTxdParent(strLocalIndex index);

	fwTxd* GetSafeFromIndex(strLocalIndex index);

	void SetCurrentTxd(strLocalIndex index);
	void PushCurrentTxd();
	void PopCurrentTxd();

	int AddDummySlot(const strStreamingObjectName name);

#if __BANK
	//void RemoveRef(strLocalIndex index, strRefKind strRefKind);
	virtual void RemoveRef(strLocalIndex index, strRefKind strRefKind);
#endif

private:
	static __THREAD fwTxd* ms_pStoredTxd;

	static fwTxd ms_emptyTxd;
};

// wrapper class needed to interface with game skeleton code
class fwTxdStoreWrapper
{
public:
	static void Shutdown(unsigned shutdownMode);
};

extern fwTxdStore g_TxdStore;

//
// name:	CBackupCurrentTxd
// description: backup the current txd
class fwBackupCurrentTxd
{
public:
	fwBackupCurrentTxd() {g_TxdStore.PushCurrentTxd();}
	~fwBackupCurrentTxd() {g_TxdStore.PopCurrentTxd();}
};

#endif //!__SPU

}	// namespace rage

#endif // FWSCENE_STORES_TXDSTORE_H_
