// 
// fwscene/stores/psostore.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "psostore.h"

#include "file/asset.h"
#include "file/device.h"
#include "paging/rscbuilder.h"
#include "parser/psodebug.h"
#include "parser/visitorutils.h"
#include "string/stringutil.h"
#include "system/bootmgr.h"
#include "system/multiallocator.h"

#include "streaming/streaming_channel.h"

using namespace rage;

PARAM(psoMismatchIsFatal, "Crash when we find a PSO mismatch");


PARAM(loadreleasepsos, "Load the release PSO versions of certain meta files, on non-disc builds");
PARAM(noloadreleasepsos, "Load the original XML versions of certain meta files, on disc builds");

fwPsoStorePlacementCookies rage::g_PsoPlacementCookies;
fwPsoStoreLoadInstance* fwPsoStoreLoader::sm_CurrentTopLevelInst = NULL;

void fwPsoStorePlacementCookies::Add(strIndex index, const Cookie& cookie)
{
	SYS_CS_SYNC(m_CS);
	Assertf(!m_Map.Access(index), "Cookie entry with index %d already exists", index.Get());

	m_Map[index] = cookie;
}

fwPsoStorePlacementCookies::Cookie fwPsoStorePlacementCookies::Get(strIndex index)
{
	SYS_CS_SYNC(m_CS);
	Assertf(m_Map.Access(index), "Cookie entry with index %d isn't in the map", index.Get());
	return m_Map[index];
}

void fwPsoStorePlacementCookies::Set(strIndex index, const Cookie& cookie)
{
	SYS_CS_SYNC(m_CS);
	Assertf(m_Map.Access(index), "Cookie entry with index %d isn't in the map", index.Get());
	m_Map[index] = cookie;
}

void fwPsoStorePlacementCookies::Remove(strIndex index)
{
	SYS_CS_SYNC(m_CS);
	Assertf(m_Map.Access(index), "Cookie entry with index %d isn't in the map", index.Get());
	m_Map.Delete(index);
}

rage::fwPsoStoreLoader::fwPsoStoreLoader()
	: m_InPlaceHeap(PSO_DONT_ALLOCATE)
	, m_NotInPlaceHeap(PSO_HEAP_CURRENT)
	, m_RootType(NULL)
{
	m_Flags.Set(LOAD_XML_FILES).Set(LOAD_IN_PLACE).Set(WARN_ON_LAYOUT_MISMATCH);
}


fwPsoStoreLoadResult rage::fwPsoStoreLoader::Load( const char* filename, fwPsoStoreLoadInstance& inOutPsoData )
{
	// Need to tell PSO from XML
	// If xml, ok to load piecemeal
	// If PSO, need to preload
	// Don't want to open file twice

	fiStream* inStream = ASSET.Open(filename, "");
	if (!inStream)
	{
		inOutPsoData = fwPsoStoreLoadInstance();
		return fwPsoStoreLoadResult();
	}
	
	u32 magicNumber;
	inStream->Read(&magicNumber, 4);
	inStream->Seek(0); // Hopefully this is free because of buffering!
	sysEndian::NtoBMe(magicNumber);

	if (magicNumber == psoConstants::PSIN_MAGIC_NUMBER)
	{
		int size = 0; 
		char* storage = NULL;
		bool allocatedStorage = false;

		if (strncmp(filename, "memory:", 7) || sscanf(filename, "memory:$%p,%d", &storage, &size) != 2)
		{
			sysMemStartTemp();
			size = inStream->Size();
			storage = rage_new char[size];
			inStream->Read(storage, size);
			sysMemEndTemp();
			allocatedStorage = true; 
		}

		fwPsoStoreLoadResult result = LoadPsoFile(storage, size, filename, inOutPsoData);

		if (inOutPsoData.m_LoadState == fwPsoStoreLoadInstance::LOADED_IN_PLACE_NOALLOC && allocatedStorage)
		{
			Warningf("InPlaceHeap is set to PSO_DONT_ALLOCATE, but we had to allocate a temp buffer to hold the PSO data");
			// Change the load state and don't delete the buffer yet, so that the lifetime of the buffer is the lifetime of the PSO instance data.
			inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_IN_PLACE_ALLOC;
			allocatedStorage = false;
		}

		if (allocatedStorage)
		{
			sysMemStartTemp();
			delete storage;
			sysMemEndTemp();
		}

		inStream->Close();

		PARSER.LoadPatchFile(filename, "", inOutPsoData.GetInstance(), m_RootType);

		return result;
	}
	else if (magicNumber == datResourceFileHeader::c_MAGIC)
	{
		Errorf("Can't load resourced PSOs this way. Stream them in and use LoadResource instead");
		inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::NOT_LOADED;
		fwPsoStoreLoadResult result;
		return result;
	}
	else
	{
		fwPsoStoreLoadResult result = LoadXmlFile(inStream, inOutPsoData);
		inStream->Close();
	
		PARSER.LoadPatchFile(filename, "", inOutPsoData.GetInstance(), m_RootType);

		return result;
	}
}

fwPsoStoreLoadResult rage::fwPsoStoreLoader::Load( void* pData, int size, const char* debugName, fwPsoStoreLoadInstance& inOutPsoData )
{
	Assertf(inOutPsoData.m_Storage == NULL, "Can't load the file, it appears to be already loaded (or not unloaded)!");
	
	bool isPso = true;

	//////////////////////////////////////////////////////////////////////////
	// HACK need a more reliable way to detect PSO files
	parIffHeader* pIffData = reinterpret_cast<parIffHeader*>(pData);
	isPso = ( pIffData->GetMagic()==psoConstants::PSIN_MAGIC_NUMBER);
	//////////////////////////////////////////////////////////////////////////

	if (isPso)
	{
		return LoadPsoFile(reinterpret_cast<char*>(pData), size, debugName, inOutPsoData);
	}
	else if (m_Flags.IsSet(LOAD_XML_FILES))
	{
		char memFileName[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, pData, size, false, debugName);

		fiStream* S = ASSET.Open(memFileName, "");
		if (S)
		{
			fwPsoStoreLoadResult result = LoadXmlFile(S, inOutPsoData);
			S->Close();
			return result;
		}
	}

	return fwPsoStoreLoadResult(); // Invalid result
}

void rage::fwPsoStoreLoader::Unload( fwPsoStoreLoadInstance& inOutPsoData, bool deleteUnownedData )
{
	FastAssert(m_RootType);
	Unload(inOutPsoData, *m_RootType, deleteUnownedData);
}

void rage::fwPsoStoreLoader::Unload(fwPsoStoreLoadInstance& inOutPsoData, parStructure& str, bool deleteUnownedData)
{
	if (inOutPsoData.m_Instance)
	{
		str.Invoke(inOutPsoData.GetInstance(), "RemoveFromStore");

		if (!inOutPsoData.IsInPlace() || m_Flags.IsSet(ALWAYS_RUN_DESTRUCTORS))
		{
			sm_CurrentTopLevelInst = &inOutPsoData;
			str.GetConcreteStructure(inOutPsoData.m_Instance)->Destroy(inOutPsoData.m_Instance);
			sm_CurrentTopLevelInst = NULL;
		}
		inOutPsoData.m_Instance = NULL;
	}

	if (inOutPsoData.OwnsStorage() || (deleteUnownedData && inOutPsoData.IsLoaded()))
	{
		if (inOutPsoData.m_LoadedFromResource)
		{ 
			delete (pgBase*) inOutPsoData.m_Storage;
		}
		else
		{
			++sysMemBuddyFreeRscMem;
			delete (char*) inOutPsoData.m_Storage;
			--sysMemBuddyFreeRscMem;
		}
	}

	// Intentionally not NULLing storage here. We might need to know the storage pointer after Unload is called

	inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::NOT_LOADED;
}

void rage::fwPsoStoreLoader::PrintLoadStats()
{

}

fwPsoStoreLoadResult rage::fwPsoStoreLoader::LoadResource(psoResourceData* resource, const char* debugFilename, fwPsoStoreLoadInstance& fwInOutPsoData)
{
	psoFile* file = psoInitFromResource(resource, debugFilename, PSOLOAD_PREP_FOR_PARSER_LOADING);

	fwPsoStoreLoadResult res;

	if (file)
	{
		res = ReadObjectsFromPsoFile(*file, fwInOutPsoData);
		delete file;
	}

	if (fwInOutPsoData.IsInPlace() && !fwInOutPsoData.OwnsStorage())
	{
		fwInOutPsoData.m_Storage = resource;
		fwInOutPsoData.m_LoadedFromResource = true;
	}

	return res;
}

fwPsoStoreLoadResult rage::fwPsoStoreLoader::LoadPsoFile( char* pData, int size, const char* debugName, fwPsoStoreLoadInstance &inOutPsoData )
{
	fwPsoStoreLoadResult storeResult;

	parAssertf(!inOutPsoData.m_Instance || m_Flags.IsSet(ALLOW_PATCH_PSOS), "'%s' Found existing instance data and ALLOW_PATCH_PSOs isn't set. Loading anyways, but memory may leak", debugName);

	psoFile* pso = psoLoadFileFromBuffer(pData, size, debugName, PSOLOAD_PREP_FOR_PARSER_LOADING, false, atFixedBitSet32().Set(psoFile::IGNORE_CHECKSUM));
	Assert(pso);

	storeResult = ReadObjectsFromPsoFile(*pso, inOutPsoData);

	if (inOutPsoData.IsInPlace() && !inOutPsoData.OwnsStorage())
	{
		inOutPsoData.m_Storage = pData; // record the pointer we'd need to delete to free the memory
	}

	delete pso;

	return storeResult;
}

fwPsoStoreLoadResult rage::fwPsoStoreLoader::ReadObjectsFromPsoFile( psoFile& pso, fwPsoStoreLoadInstance &inOutPsoData )
{
	fwPsoStoreLoadResult storeResult;

	bool tryInPlaceLoad = m_Flags.IsSet(LOAD_IN_PLACE) && (!inOutPsoData.m_Instance); // If there was an existing instance, we should do a 'patch', and that can't be in-place

	parStructure* rootType = PARSER.FindStructure(pso.GetRootInstance().GetSchema().GetNameHash());
	if (rootType->GetFlags().IsSet(parStructure::NOT_IN_PLACE_LOADABLE))
	{
		tryInPlaceLoad = false;
	}

	parPtrToStructure newObj = NULL;

	if (tryInPlaceLoad)
	{
		psoLoadInPlaceResult result = psoLoadObjectInPlace(pso, m_InPlaceHeap, m_NotInPlaceHeap);
		if (parVerifyf(result.IsOk(), "Failed to convert PSO file to runtime objects "))
		{
			Assertf((!result.m_RootObject || !m_RootType || (result.m_RootObjectType && result.m_RootObjectType->IsSubclassOf(m_RootType))), "The root object in the PSO file (%s) isn't a subclass of %s",
				result.m_RootObjectType ? result.m_RootObjectType->GetName() : "null",
				m_RootType ? m_RootType->GetName() : "null"
				);

			if (result.m_Status == psoLoadInPlaceResult::PSOLOAD_IN_PLACE)
			{
				storeResult.m_Structure = result.m_RootObjectType;
				inOutPsoData.m_Instance = result.m_RootObject;
				if (m_InPlaceHeap == PSO_DONT_ALLOCATE)
				{
					inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_IN_PLACE_NOALLOC;
					inOutPsoData.m_Storage = NULL; // filled in by the caller
				}
				else
				{
					inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_IN_PLACE_ALLOC;
					inOutPsoData.m_Storage = result.m_InplaceBuffer;
				}
			}
			else if (result.m_Status == psoLoadInPlaceResult::PSOLOAD_NOT_IN_PLACE)
			{
				storeResult.m_Structure = result.m_RootObjectType;
				inOutPsoData.m_Instance = result.m_RootObject;
				inOutPsoData.m_Storage = result.m_RootObject;
				inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_NOT_IN_PLACE;


#if !__NO_OUTPUT
				if (g_psoLoadStats.m_LayoutMismatchLoads)
				{
#if __BANK
					const char* restUri = REST.GetWebServerUri();
#else
					const char* restUri = "";
#endif

					if (PARAM_psoMismatchIsFatal.Get())
					{
						char mismatchSubstrBuf[RAGE_MAX_PATH];
						const char* mismatchSubstrs[64];
						int numSubstrs = PARAM_psoMismatchIsFatal.GetArray(mismatchSubstrs, 64, mismatchSubstrBuf, RAGE_MAX_PATH);
						for(int i = 0; i < numSubstrs; i++)
						{
							if (stristr(pso.GetFileName(), mismatchSubstrs[i]))
							{
								Quitf("Crashing due to mismatched PSOs (and -psoMismatchIsFatal). See TTY above for details");
							}
						}
					}
					else if (m_Flags.IsSet(ASSERT_ON_LAYOUT_MISMATCH))
					{
						streamAssetAssertf(0, "Loading PSO file %s - layout mismatch for types %s, %s, %s, %s.\n"
							"See %scommon/non_final/www/parser/psomismatch.html to debug (once the game is running)", pso.GetFileName(),
							g_psoLoadStats.m_MismatchedStructures[0] ? g_psoLoadStats.m_MismatchedStructures[0]->GetName() : "",
							g_psoLoadStats.m_MismatchedStructures[1] ? g_psoLoadStats.m_MismatchedStructures[1]->GetName() : "",
							g_psoLoadStats.m_MismatchedStructures[2] ? g_psoLoadStats.m_MismatchedStructures[2]->GetName() : "",
							g_psoLoadStats.m_MismatchedStructures[3] ? g_psoLoadStats.m_MismatchedStructures[3]->GetName() : "",
							restUri
							);
					}
					else if (m_Flags.IsSet(WARN_ON_LAYOUT_MISMATCH))
					{
						streamAssetWarningf("Loading PSO file %s - layout mismatch for types %s, %s, %s, %s.\n"
							"See %scommon/non_final/www/parser/psomismatch.html to debug (once the game is running)", pso.GetFileName(),
							g_psoLoadStats.m_MismatchedStructures[0] ? g_psoLoadStats.m_MismatchedStructures[0]->GetName() : "",
							g_psoLoadStats.m_MismatchedStructures[1] ? g_psoLoadStats.m_MismatchedStructures[1]->GetName() : "",
							g_psoLoadStats.m_MismatchedStructures[2] ? g_psoLoadStats.m_MismatchedStructures[2]->GetName() : "",
							g_psoLoadStats.m_MismatchedStructures[3] ? g_psoLoadStats.m_MismatchedStructures[3]->GetName() : "",
							restUri
							);
					}

#if !__FINAL
					if(psoDebugMismatchedFiles::IsInitted())
					{
						psoDebugMismatchedFiles::GetInstance().AddMismatchedFile(pso);
					}
#endif
				}
#endif
			}
			else
			{
				inOutPsoData = fwPsoStoreLoadInstance();
			}
		}
	}
	else
	{
		parStructure* loadType = m_RootType;
		bool result = false;

		if (inOutPsoData.m_Instance)
		{
			// Patching an existing object
			psoStruct root = pso.GetRootInstance();
			result = psoLoadFromStructure(root, *loadType, inOutPsoData.m_Instance, true);
			if (result)
			{
				
				storeResult.m_Structure = loadType;
				inOutPsoData.m_Storage = NULL;
				//	m_Instance is already set
				inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_NOT_IN_PLACE;
			}
			else
			{
				inOutPsoData = fwPsoStoreLoadInstance();
			}
		}
		else
		{
			if (!loadType)
			{
				loadType = PARSER.FindStructure(pso.GetRootInstance().GetSchema().GetNameHash());
			}

			result = psoCreateAndLoadFromStructure(pso, loadType, newObj);
			if (result)
			{
				storeResult.m_Structure = loadType;
				inOutPsoData.m_Instance = newObj;
				inOutPsoData.m_Storage = newObj;
				inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_NOT_IN_PLACE;
			}
			else
			{
				inOutPsoData = fwPsoStoreLoadInstance();
			}
		}

	}

	if (m_Flags.IsSet(RUN_POSTLOAD_CALLBACKS))
	{
		RunPostloadCallbacks(storeResult.m_Structure, inOutPsoData);
	}

	return storeResult;
}

void fwPsoStoreLoader::RunPostloadCallbacks(parStructure* topLevelStructure, fwPsoStoreLoadInstance& inOutPsoData)
{
	class ForEachVisitor : public parNonGenericVisitor
	{
	public:
		ForEachVisitor()
		{
			parInstanceVisitor::TypeMask typeMask(false);
			typeMask.Set(parMemberType::TYPE_STRUCT);
			SetMemberTypeMask(typeMask);
		}

		virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
		{
			metadata.Invoke(dataPtr, "PostLoad");
			parNonGenericVisitor::VisitStructure(dataPtr, metadata);
		}
	};

	ForEachVisitor vis;
	parStructure* concreteType = topLevelStructure->GetConcreteStructure(inOutPsoData.m_Instance);
	vis.VisitToplevelStructure(inOutPsoData.m_Instance, *concreteType);
}

fwPsoStoreLoadResult rage::fwPsoStoreLoader::LoadXmlFile( fiStream* stream, fwPsoStoreLoadInstance &inOutPsoData )
{
	fwPsoStoreLoadResult storeResult;

	Assertf(!inOutPsoData.m_Instance || m_Flags.IsSet(ALLOW_PATCH_XMLS), "'%s' Found existing instance data and ALLOW_PATCH_XMLs isn't set. Loading anyways, but memory may leak", stream->GetName());

	Assertf(m_NotInPlaceHeap != PSO_DONT_ALLOCATE, "You're trying to load an XML/RBF file, but the not-in-place heap is PSO_DONT_ALLOCATE. We need to allocate _somewhere_");
	if (m_NotInPlaceHeap == PSO_HEAP_DEBUG)
	{
		sysMemStartDebug();
	}
	else if (m_NotInPlaceHeap == PSO_HEAP_TEMP)
	{
		sysMemStartTemp();
	}

	parPtrToStructure newObj = NULL;
	parStructure* rootStr = m_RootType; // may be NULL
	bool result;
	if (m_Flags.IsSet(ALLOW_PATCH_XMLS) && inOutPsoData.m_Instance && rootStr)
	{
		result = PARSER.LoadFromStructure(stream, *rootStr, inOutPsoData.m_Instance, true);
		newObj = inOutPsoData.m_Instance;
	}
	else
	{
		parSettings settings = PARSER.Settings();
		settings.SetFlag(parSettings::LOAD_UNKNOWN_TYPES, m_RootType == NULL);
		result = PARSER.CreateAndLoadAnyType(stream, rootStr, newObj, &settings);
	}
	if (result)
	{
		storeResult.m_Structure = rootStr;
		inOutPsoData.m_Instance = newObj;
		inOutPsoData.m_Storage = newObj;
		inOutPsoData.m_LoadState = fwPsoStoreLoadInstance::LOADED_AS_XML;
	}
	else
	{
		inOutPsoData = fwPsoStoreLoadInstance();
	}

	if (m_NotInPlaceHeap == PSO_HEAP_DEBUG)
	{
		sysMemEndDebug();
	}
	else if (m_NotInPlaceHeap == PSO_HEAP_TEMP)
	{
		sysMemEndTemp();
	}

	return storeResult;
}

bool rage::fwPsoStoreLoader::LoadDataIntoObjectCore( const char* filename, const char* ext, fwPsoStoreLoadInstance &inst )
{
	char fullName[RAGE_MAX_PATH];
	fullName[0] = '\0';
	ASSET.AddExtension(fullName, RAGE_MAX_PATH, filename, ext);
	Load(fullName, inst);
	return inst.IsLoaded();
}

fwPsoStoreLoader fwPsoStoreLoader::MakeSimpleFileLoaderCore()
{
	// Allow loading XML files, applying them as patches. Do not load in place and do not warn about mismatches
	fwPsoStoreLoader loader;
	atFixedBitSet32 flags = loader.GetFlags();
	flags.Set(fwPsoStoreLoader::LOAD_XML_FILES);
	flags.Set(fwPsoStoreLoader::ALLOW_PATCH_XMLS);
	flags.Set(fwPsoStoreLoader::ALLOW_PATCH_PSOS);

	bool loadReleasePsos = true;
	if(sysBootManager::IsBootedFromDisc())
	{
		loadReleasePsos = !PARAM_noloadreleasepsos.Get();
	}
	else
	{
		loadReleasePsos = PARAM_loadreleasepsos.Get();
	}

	loader.SetFlags(flags);

	return loader;
}

