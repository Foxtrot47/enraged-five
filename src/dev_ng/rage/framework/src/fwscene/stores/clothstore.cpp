// 
// fwscene/stores/clothstore.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "fwscene/stores/clothstore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"


namespace rage 
{

template<> DICTIONARY_THREAD pgDictionary<characterCloth>* pgDictionary<characterCloth>::sm_CurrentDict = NULL;

// global instance of the cloth dictionary store
fwClothStore g_ClothStore;

fwClothStore::fwClothStore() : fwAssetRscStore<fwCloth, fwClothDef>("ClothStore", CLOTH_FILE_ID, CONFIGURED_FROM_FILE, 256, false, CLOTH_FILE_VERSION)
{
}

/*
// Set entry 
void fwClothStore::Set(int index, fwCloth* m_pObject)
{
	fwClothDef* pClothDef = GetSlot(index);
	Assertf(pClothDef, "No cloth dictionary at this slot");
	Assertf(pClothDef->m_pObject==NULL, "Cloth is already in memory");

	UPDATE_KNOWN_REF(pClothDef->m_pObject , m_pObject);

	if(pClothDef->m_pObject)
		fwClothStore::SetupClothParent(index);
}
*/

// Remove cloth
void fwClothStore::Remove(strLocalIndex index)
{
	ASSERT_ONLY(fwClothDef* pDef = GetSlot(index);)
	Assertf(pDef, "No cloth dictionary at this slot");
	Assertf(pDef->m_pObject!=NULL, "Cloth dictionary is not in memory");
	Assertf(pDef->m_pObject->GetCurrent() != pDef->m_pObject, "Can't delete cloth dictionary while it is the current one");

	fwAssetRscStore<fwCloth, fwClothDef>::Remove(index);
}

/*
// Return the objects this cloth dictionary requires before it can be loaded. 
// For cloth dictionaries this is any parent cloth dictionaries
int fwClothStore::GetDependencies(int index, strIndex *pIndices, int indexArraySize) const
{
	int count = 0;
	int parentCloth = fwClothStore::GetParentClothSlot(index);
	if(parentCloth != -1)
	{
		pIndices[count++] = GetStreamingIndex(parentCloth);
	}
	return count;
}
*/


// Remove all unreferenced dictionaries
void fwClothStore::ShutdownLevel()
{
	// Call remove all twice to ensure the parent cloth dictionaries get deleted
	RemoveAll();
	RemoveAll();
}

/*
// Setup the parent fwCloth pointer
void fwClothStore::SetupClothParent(int index)
{
	fwClothDef* pClothDef = GetSlot(index);
	// Set TXD parent
	if(pClothDef->parentId != -1)
	{
		fwClothDef* pParentClothDef = GetSlot(pClothDef->parentId);
		Assertf(pParentClothDef, "No parent cloth dictionary at this slot");
		Assertf(pParentClothDef->m_pObject!=NULL, "Parent cloth dictionary is not in memory");

		pClothDef->m_pObject->SetParent(pParentClothDef->m_pObject);
	}
}
*/

// Set the current cloth dictionary given an index
void fwClothStore::SetCurrentCloth(strLocalIndex index)
{
	if (index != -1)
	{
		fwClothDef* pClothDef = GetSlot(index);

		Assertf(pClothDef, "No cloth dictionary at this slot");
		Assertf(pClothDef->m_pObject, "%s:Cloth dictionary is not in memory", pClothDef->m_name.GetCStr());
		fwCloth::SetCurrent(pClothDef->m_pObject);
	}
	else
	{
		fwCloth::SetCurrent(NULL);
	}
}


// Push current cloth dictionary onto stack
void fwClothStore::PushCurrentCloth()
{
	Assertf(m_pStoredCloth == NULL, "Cloth dictionary stack is full");

	m_pStoredCloth = fwCloth::GetCurrent();
	if(m_pStoredCloth)
	{
		m_pStoredCloth->AddRef(); 
	}
}


// Pop cloth dictionary off stack
void fwClothStore::PopCurrentCloth()
{
	fwCloth::SetCurrent(m_pStoredCloth);
	if(m_pStoredCloth)
	{
		m_pStoredCloth->Release(); 
	}
	m_pStoredCloth = NULL;
}


// Defragment
void* fwClothStore::Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& map, bool& flush)
{
	fwCloth* object = NULL;

	datResource rsc(map,"defrag",true);
	fwCloth::Place(object = (fwCloth*)rsc.GetBase(), rsc);

	flush = true; // cloth need to wait until the drawable spu jobs are flushed.

	return object;
}

void fwClothStoreWrapper::Shutdown(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		g_ClothStore.Shutdown();
	}
	else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)	//	SHUTDOWN_SESSION)
	{
		g_ClothStore.ShutdownLevel();
	}
}

}	// namespace rage