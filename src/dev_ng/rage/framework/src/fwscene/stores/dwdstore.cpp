// 
// stores/dwdstore.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "fwscene/stores/dwdstore.h"

#include "fwscene/texLod.h"

#include "fwscene/stores/txdstore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"

namespace rage {

fwPlaceFunctor fwDwdStore::sm_PlaceFunctor;
fwDefragmentFunctor fwDwdStore::sm_DefragmentFunctor;

fwDwdStore g_DwdStore;

fwDwdStore::fwDwdStore() : fwAssetRscStore<Dwd, DwdDef>(
		"DwdStore", 
		DWD_FILE_ID, 
		CONFIGURED_FROM_FILE, 
		512,
		true, 
		DWD_FILE_VERSION
	)
{
}

void fwDwdStore::Set(strLocalIndex index, Dwd* m_pObject)
{
	DwdDef* pDwdDef = GetSlot(index);

	Assertf(pDwdDef, "No drawable dictionary at this slot");
	Assertf(pDwdDef->m_pObject==NULL, "Drawable dictionary is already in memory");

	UPDATE_KNOWN_REF(pDwdDef->m_pObject , m_pObject);
}

void fwDwdStore::Remove(strLocalIndex index)
{
	DwdDef* pDef = GetSlot(index);
	Assertf(pDef, "No drawable at this slot");
	Assertf(pDef->m_pObject!=NULL, "Drawable is not in memory");

	SAFE_REMOVE_KNOWN_REF(pDef->m_pObject);
	if (!pDef->m_bMatchError)
	{
		Dwd* object = pDef->m_pObject;
		int count = object->GetCount();
		for (int i = 0; i != count; ++i)
		{
			Drawable* pDrawable = object->GetEntry(i);
			pDrawable->GetShaderGroup().Destroy();
		}
		pDef->m_pObject->Destroy();
	}
	else
	{
		delete pDef->m_pObject;
	}
	pDef->m_pObject = NULL;
}


bool fwDwdStore::LoadFile(strLocalIndex index, const char* pFilename)
{
	DwdDef* pDwdDef = GetSlot(index);

	Assertf(pDwdDef, "No drawable dictionary at this slot");
	Assertf(pDwdDef->m_pObject==NULL, "Drawable is already in memory");

	fwBackupCurrentTxd backup;
	
	g_TxdStore.SetCurrentTxd(pDwdDef->GetTxdIndex());

	Dwd* m_pObject;
	pgRscBuilder::Load(m_pObject, pFilename, DWD_FILE_EXT_PATTERN, DWD_FILE_VERSION);

	//Set(index, m_pObject);

	return (pDwdDef->m_pObject != NULL);
}

extern s32 grcTextureSuppressReferenceWarning;
void fwDwdStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& info)
{
	DwdDef* pDwdDef = GetSlot(index);

	Assertf(pDwdDef, "No drawable dictionary at this slot");
	Assertf(pDwdDef->m_pObject==NULL, "Drawable is already in memory");

	fwBackupCurrentTxd backup;
	
	g_TxdStore.SetCurrentTxd(pDwdDef->GetTxdIndex());

#if !__NO_OUTPUT
    if (pDwdDef && pDwdDef->m_noAssignedTxd)
        grcTextureSuppressReferenceWarning++;
#endif

	grcEffect::ResetMatchError();
#if __FINAL
	(sm_PlaceFunctor(info, map, NULL, false));
#else
	(sm_PlaceFunctor(info, map, GetName(index), false));
#endif

#if !__NO_OUTPUT
    if (pDwdDef && pDwdDef->m_noAssignedTxd)
        grcTextureSuppressReferenceWarning--;
#endif

	//Set(index, obj);

	pDwdDef->m_bMatchError = true; // rage::grcEffect::GetMatchError() || RSG_PC;
}



void* fwDwdStore::Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& map, bool& flush)
{
	datResource rsc(map,"defrag",true);
	Dwd* object = static_cast<Dwd*>(sm_DefragmentFunctor(map));

	for (int i = 0; i != object->GetCount(); ++i)
	{
		Drawable* pDrawable = object->GetEntry(i);
#if __ASSERT
		char buf[128];
		Assertf(!pDrawable || pDrawable->GetShaderGroupPtr(),"Drawable '%s' is missing a shader group somehow?",pDrawable->GetDebugName(buf,sizeof(buf)));
#endif
		if (pDrawable && pDrawable->GetShaderGroupPtr() && pDrawable->GetShaderGroup().GetTexDict()) {
			flush = true;
			break;
		}
	}

	return object;
}


void fwDwdStore::DefragmentPreprocess(strLocalIndex index)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged = true;
		fwTexLod::SetSwapStateSingle(false, fwAssetLocation(STORE_ASSET_DWD,index.Get()));				// unswap before hitting defrag code
	}
}


void fwDwdStore::DefragmentComplete(strLocalIndex index)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged = false;
		fwTexLod::SetSwapStateSingle(true, fwAssetLocation(STORE_ASSET_DWD,index.Get()));				// reswap since we are unflagging the asset
	}
}


bool fwDwdStore::IsBeingDefragged(strLocalIndex index)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBeingDefragged);
	}
	return(false);
}



bool fwDwdStore::GetIsHDCapable(strLocalIndex index)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bHasHD);
	}
	return(false);
}


void fwDwdStore::SetIsHDCapable(strLocalIndex index, bool val)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bHasHD = val;
	}
}



bool fwDwdStore::GetIsBoundHD(strLocalIndex index)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBoundHD);
	}
	return(false);
}



void fwDwdStore::SetIsBoundHD(strLocalIndex index, bool val)
{
	DwdDef* pDef = fwDwdStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBoundHD = val;
	}
}



#if __ASSERT
void fwDwdStore::SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent)
{
	if (GetSlot(index)->GetTxdIndex() != parent)
	{
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(GetStreamingIndex(index));

		Assertf(info.GetStatus() == STRINFO_NOTLOADED, "%s sets its parent TXD from %d to %d, but its status is %s", GetName(index), GetSlot(index)->GetTxdIndex().Get(), parent.Get(), info.GetFriendlyStatusName());
		GetSlot(index)->SetTxdIndex(parent.Get());
	}
}
#endif // __ASSERT


int fwDwdStore::GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const
{
	int count = 0;
	strLocalIndex parentTxd = fwDwdStore::GetParentTxdForSlot(index);
	if(parentTxd != -1)
	{
		AddDependencyOutput(pIndices, count, g_TxdStore.GetStreamingModule()->GetStreamingIndex(parentTxd), indexArraySize, GetStreamingIndex(strLocalIndex(indexArraySize)));
	}
	return count;
}



void fwDwdStore::ShutdownLevel()
{
	RemoveAll();
}

void fwDwdStoreWrapper::Shutdown(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		g_DwdStore.Shutdown();
	}
	else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
	{
		g_DwdStore.ShutdownLevel();
	}
}

}	// namespace rage
