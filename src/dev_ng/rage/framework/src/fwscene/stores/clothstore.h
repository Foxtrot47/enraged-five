// 
// fwscene/stores/clothstore.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 


#ifndef FWSCENE_STORES_CLOTHSTORE_H_
#define FWSCENE_STORES_CLOTHSTORE_H_

#include "paging/dictionary.h"
#include "cloth/charactercloth.h"

#include "fwtl/assetstore.h"

namespace rage {

typedef pgDictionary<characterCloth> fwCloth;

#if !__SPU


// Texture dictionary definition
class fwClothDef : public fwAssetDef<fwCloth>
{
public:
	void Init(const strStreamingObjectName name) 
	{
		fwAssetDef<fwCloth>::Init(name); 
		m_modelHashKey = name.GetHash(); 
	}
	u32 m_modelHashKey;
};


class fwClothStore : public fwAssetRscStore<fwCloth, fwClothDef>
{
public:
	fwClothStore();

//	virtual void Set(int index, fwCloth* m_pObject);
	virtual void Remove(strLocalIndex index);
	virtual void* Defragment(strLocalIndex index, datResourceMap& map, bool& flush);
//	virtual int GetDependencies(int UNUSED_PARAM(index), strIndex *UNUSED_PARAM(pIndices), int indexArraySize) const;

	void ShutdownLevel();

//	int GetParentClothSlot(int index) const {return m_pool.GetSlot(index)->parentId;}
//	void SetParentClothSlot(int index, int parent) {m_pool.GetSlot(index)->parentId = parent;}
//	void SetupClothParent(int index);

	void SetCurrentCloth(strLocalIndex index);
	void PushCurrentCloth();
	void PopCurrentCloth();

private:
	fwCloth* m_pStoredCloth;
};

// wrapper class needed to interface with game skeleton code
class fwClothStoreWrapper
{
public:
	static void Shutdown(unsigned shutdownMode);
};

extern fwClothStore g_ClothStore;


#endif //!__SPU

}	// namespace rage

#endif // FWSCENE_STORES_CLOTHSTORE_H_