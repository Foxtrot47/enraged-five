//
// filename:	StaticBoundsStore.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

#include "StaticBoundsStore.h"

// Rage headers
#include "diag/art_channel.h"
#include "fwscene/world/WorldLimits.h"
#include "fwsys/fileExts.h"
#include "fwsys/timer.h"
#include "phbound/boundcomposite.h"
#include "streaming/CacheLoader.h"
#include "streaming/packfilemanager.h"
#include "streaming/streaming_channel.h"
#include "system/bootmgr.h"

STREAMING_OPTIMISATIONS()

namespace rage {

// --- Defines ------------------------------------------------------------------
#if !RSG_FINAL
PARAM(breakOnBoundsRemoved, "Break when a bounds is removed");
PARAM(breakOnBoundsAdded, "Break when a bounds is added");
#define BreakOnBoundsAdded(index)																				\
const char* break_on_added;																						\
if (PARAM_breakOnBoundsAdded.Get(break_on_added))																\
{																												\
	if (atString(GetName(index)).IndexOf(break_on_added) != atString::npos)										\
	{																											\
		char message[RAGE_MAX_PATH];																			\
		sprintf_s(message, RAGE_MAX_PATH, "[bounds][added] %s:%d: %s\n", __FUNCTION__, __LINE__, GetName(index));	\
		BreakOn(message);																						\
	}																											\
}																												\

#define BreakOnBoundsRemoved(index)																				\
const char* break_on_removed;																					\
if (PARAM_breakOnBoundsRemoved.Get(break_on_removed))															\
{																												\
	if (atString(GetName(index)).IndexOf(break_on_removed) != atString::npos)									\
	{																											\
		char message[RAGE_MAX_PATH];																			\
		sprintf_s(message, RAGE_MAX_PATH, "[bounds][removed] %s:%d: %s\n", __FUNCTION__, __LINE__, GetName(index));	\
		BreakOn(message);																						\
	}																											\
}																												\

void BreakOn(const char* message = nullptr)
{
	if (message != nullptr)
	{
		Printf("%s", message);
	}
	sysStack::PrintStackTrace();
	if (sysBootManager::IsDebuggerPresent())
	{
		__debugbreak();
	}
}
#else
#define BreakOnBoundsAdded(index) 
#define BreakOnBoundsRemoved(index) 
#endif

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

fwStaticBoundsStore g_StaticBoundsStore;
static fwStaticBoundsStoreInterface g_DummyStaticBoundsStoreInterface;

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

#if __DEV

void BoundsStoreCacheEntry::Print()
{ 
	strLocalIndex index = strLocalIndex(g_StaticBoundsStore.FindSlotFromHashKey(m_hashKey));
	Displayf( "%s - 0x%08x - (%f, %f, %f), (%f, %f, %f)",
			g_StaticBoundsStore.GetName(index), m_hashKey,
			m_minX, m_minY, m_minZ,
			m_maxX, m_maxY, m_maxZ
		);
}

#endif // __DEV

//
// name:		StaticBoundsStore_ReadFromCacheFile, StaticBoundsStore_AddToCacheFile
// description:	Wrapper functions for cache loader
bool StaticBoundsStore_ReadFromCacheFile(const void* const pLine, fiStream *pDebugTextFileToWriteTo)
{
	return g_StaticBoundsStore.ReadFromCacheFile(pLine, pDebugTextFileToWriteTo);
}
void StaticBoundsStore_AddToCacheFile(fiStream* pDebugTextFileToWriteTo)
{
	g_StaticBoundsStore.AddToCacheFile(pDebugTextFileToWriteTo);
}

fwStaticBoundsStore::fwStaticBoundsStore() : 
fwAssetRscStore<phBound, fwBoundDef>("StaticBounds", BOUND_FILE_ID, CONFIGURED_FROM_FILE, 455, true, BOUND_FILE_VERSION)
{
	m_StaticBoundsStoreInterface = &g_DummyStaticBoundsStoreInterface;
}

fwStaticBoundsStore::~fwStaticBoundsStore()
{
	// Release the bound here so we don't get refcount asserts in the phBound destructor
	m_emptyphBound.Release(false);
}
//
// name:		fwStaticBoundsStore::Init
// description:	
void fwStaticBoundsStore::Init()
{
	// initialise the box streamer
	m_boxStreamer.Init(this, 30.0f);
	m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER, MOVER_BOUNDS_PRESTREAM, true, true, false);
	m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_STATICBOUNDS_WEAPONS, WEAPON_BOUNDS_PRESTREAM, false, false, false);
	m_boxStreamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MATERIAL, MATERIAL_BOUNDS_PRESTREAM, false, false, false);

	// set the cache loader module:
	strCacheLoader::GetModules().AddModule("BoundsStore",
										&StaticBoundsStore_ReadFromCacheFile,  // called when we read from the cache
										&StaticBoundsStore_AddToCacheFile,  // called when we write to the cache
										sizeof(BoundsStoreCacheEntry));
	m_proxyTable.Reset();
	m_dependencyTable.Reset();

#if __BANK
	m_pool.RegisterPoolCallback(fwStaticBoundsStore::PoolFullCallback);	
#endif //__BANK
}

//
// name:		fwStaticBoundsStore::Shutdown
// description:	Shutdown class
void fwStaticBoundsStore::Shutdown()
{
	m_boxStreamer.Shutdown();
	fwAssetRscStore<phBound, fwBoundDef>::Shutdown();
}

//
// name:		Set
// description:	Set a loaded bound and link in
void fwStaticBoundsStore::Set(strLocalIndex index, phBound* pObject)
{
	fwBoundDef* pDef = GetSlot(index);

	Assertf(pDef, "No bound at this slot");
	Assertf(pDef->m_pObject==NULL, "Bound is already in memory");

	UPDATE_KNOWN_REF(pDef->m_pObject , pObject);

	if(pDef->m_pObject)
	{
		fwBoxStreamerAsset::eType assetType = AddSlotToPhysicsLevel(index.Get());
		if (!m_boxStreamer.IsLocked() NOTFINAL_ONLY(|| IsDirty(index)))
		{
#if __BANK
			if (IsDirty(index))
			{
				m_boxStreamer.RebuildTrees();
				ClearDirtyFlag(index);
			}
#endif // __BANK

			const spdAABB aabb = m_boxStreamer.GetBounds(index.Get());
			SetBoxStreamerEntry(index.Get(), aabb, (u8)assetType);
	
		}

		if ( Unlikely(pDef->HasRiverBounds()) )
		{
			AddToRiverBoundsList(index.Get());
		}
	}
}

//
// name:		Remove
// description:	Remove a bound
void fwStaticBoundsStore::Remove(strLocalIndex index)
{
	fwBoundDef* pDef = GetSlot(index);

	Assertf(pDef, "No bound at this slot");
	RemoveBoundFromPhysicsLevel(index.Get());

	if (pDef->IsDummy())
	{
		pDef->m_pObject = NULL;
		return;
	}

	if (Verifyf(pDef->m_pObject!=NULL, "bound is not in memory"))
	{
		REMOVE_KNOWN_REF(pDef->m_pObject);

		if (!Verifyf(pDef->m_pObject->Release() == 0,"bound release failed, deleting anyway"))
			delete pDef->m_pObject;

		pDef->m_pObject = NULL;
	}

	if (Unlikely( pDef->HasRiverBounds()) )
	{
		RemoveFromRiverBoundsList(index.Get());
	}
}

//
// name:		fwStaticBoundsStore::AddSlot
// description:	Add a new slot with the specified name
strLocalIndex fwStaticBoundsStore::AddSlot(const strStreamingObjectName name)
{
	strLocalIndex index = fwAssetRscStore<phBound, fwBoundDef>::AddSlot(name);
	m_boxStreamer.InitSlot(index.Get(), fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER);
	BreakOnBoundsAdded(index);
	return index;
}

//
// name:		fwStaticBoundsStore::AddSlot
// description:	Add a new slot with the specified name
strLocalIndex fwStaticBoundsStore::AddSlot(strLocalIndex index, const strStreamingObjectName name)
{
	fwAssetRscStore<phBound, fwBoundDef>::AddSlot(index, name);
	m_boxStreamer.InitSlot(index.Get(), fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER);
	BreakOnBoundsAdded(index);
	return index;
}

//
// name:		fwStaticBoundsStore::RemoveSlot
// description:	Remove a slot from the store
void fwStaticBoundsStore::RemoveSlot(strLocalIndex index)
{
	Assert(index.Get() >= 0);
	fwBoundDef* pDef = GetSlot(index);
	BreakOnBoundsRemoved(index);

	// delete texture dictionary
	Assertf(pDef, "Slot %d does not exist", index.Get());
	RemoveHashKey(pDef->m_name.GetHash());
	if(pDef->m_pObject)
		Remove(index);

#if !USE_PAGED_POOLS_FOR_STREAMING
	m_pool.Delete(GetSlot(index));
#endif // !USE_PAGED_POOLS_FOR_STREAMING
	m_boxStreamer.RemoveSlot(index.Get());
}

bool fwStaticBoundsStore::Load(strLocalIndex slotIndex, void* pData, int size)
{
	Assert(slotIndex.Get() >= 0);

	fwBoundDef* def = GetSlot(slotIndex);
	if(def->IsDummy())
	{
		 fwBoundDef* pDepDef = GetSlot(m_dependencyTable[slotIndex]);
		 Assert(pDepDef);
		 Assert(pDepDef->IsDependency());
		 Assert(pDepDef->m_pObject);

		// dummy bound is 'loaded', which means we can now use the dependencies to populate the physics world for this entry
		AddBoundToPhysicsLevel(slotIndex.Get(), pDepDef->m_pObject);

		Assert(!pData);
		def->m_pObject = &m_emptyphBound;
		return true;
	}

	return fwAssetStore<phBound, fwBoundDef>::Load(slotIndex, pData, size);
}

//
// name:		fwStaticBoundsStore::AddDepenency
// description:	Add a new dependency to this static bounds entry
void fwStaticBoundsStore::SetAsDummyBound(strLocalIndex slotIndex, InteriorProxyIndex proxyID, strLocalIndex dependencyIndex)
{
	fwBoundDef* pDef = GetSlot(slotIndex);
	Assert(pDef);

	if (!pDef->IsDummy())
	{
		pDef->m_bIsDummy = 1;
		
		m_dependencyTable[slotIndex] = dependencyIndex;

		if (m_proxyTable.Access(dependencyIndex) == NULL)
		{
			m_proxyTable.Insert(dependencyIndex);
		}
		dependantProxyMap* entries = m_proxyTable.Access(dependencyIndex);
			
		entries->Insert(slotIndex, proxyID);

		//mark parent slot as dependency
		fwBoundDef* pDepDef = GetSlot(dependencyIndex);	
		Assert(pDepDef);
		pDepDef->m_bIsDependency = 1;

		char	modelName[80];
		sprintf(modelName, "static_MLO_%d",proxyID);

		if ( strStreamingEngine::GetInfo().RegisterDummyObject(
			modelName, slotIndex, GetStreamingModuleId()) == strIndex(strIndex::INVALID_INDEX))			// note: this is NOT -1
		{
			Assertf(false,"could not register dummy static bounds : %s", modelName);
		}
	}
}

int fwStaticBoundsStore::GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const 
{
	int count = 0;

	const fwBoundDef* pDef = GetSlot(index);

	if (pDef && pDef->IsDummy())
	{
		const strLocalIndex* parentIndex = m_dependencyTable.Access(index);
		Assertf( parentIndex != NULL, "GetDependencies: Data inconsistency - There's no entry in dependencyTable for %d but it's marked as dummy?!", index.Get());
		AddDependencyOutput(pIndices, count, GetStreamingIndex(*parentIndex), indexArraySize, GetStreamingIndex(index));		
	}

	return count;
}

void fwStaticBoundsStore::GetDummyBoundData(strLocalIndex index, InteriorProxyIndex& proxyID, strLocalIndex& dependencySlot)
{
 	const fwBoundDef* pDef = GetSlot(index);

	if (pDef && pDef->IsDummy())
	{
		strLocalIndex* parentIndex = m_dependencyTable.Access(index);
		Assertf( parentIndex != NULL, "GetDependencies: Data inconsistency - There's no entry in dependencyTable for %d but it's marked as dummy?!", index.Get());
		dependencySlot = *parentIndex;

		const dependantProxyMap* dependencies = m_proxyTable.Access(*parentIndex);
		Assertf(dependencies != NULL, "GetDummyBoundData: Data inconsistency - There's no entry in dependencyTable for %d but it's marked as dummy?!", index.Get());
		proxyID = *(dependencies->Access(index));
	}
}

//
// name:		fwBoxStreamer::RemoveAll
// description:	Remove all bounds from memory
void fwStaticBoundsStore::RemoveAll()
{
	m_boxStreamer.ClearRequiredList();

	// clear dummies first (to clear depenendcy references for next pass of remove)
	for (s32 i=0; i<GetSize(); i++)
	{
		if (IsValidSlot(strLocalIndex(i)))
		{
			const fwBoundDef* pDef = GetSlot(strLocalIndex(i));
			if (pDef && pDef->IsDummy())
			{
				ClearRequiredFlag(i, STRFLAG_DONTDELETE);
				StreamingRemove(strLocalIndex(i));
				BreakOnBoundsRemoved(strLocalIndex(i));
			}
		}
	}

	for (s32 i=0; i<GetSize(); i++)
	{
		if (IsValidSlot(strLocalIndex(i)))
		{
			ClearRequiredFlag(i, STRFLAG_DONTDELETE);
			StreamingRemove(strLocalIndex(i));
			BreakOnBoundsRemoved(strLocalIndex(i));
		}
	}
}

bool fwStaticBoundsStore::ModifyHierarchyStatus(strLocalIndex index, eHierarchyModType modType)
{
	if (index != -1 && IsValidSlot(index))
	{
		bool disabled = modType == HMT_DISABLE;
		bool flushOrDisable = modType == HMT_DISABLE || modType == HMT_FLUSH;

		m_boxStreamer.ClearRequiredList();

		const dependantProxyMap* entries = m_proxyTable.Access(index);
		if (entries != NULL)
		{
			for (dependantProxyMap::ConstIterator it = entries->CreateIterator(); it.AtEnd(); it.Next())
			{
				strLocalIndex currIdx(it.GetKey());

				if (flushOrDisable)
				{
					ClearRequiredFlag(currIdx.Get(), STRFLAG_DONTDELETE);
					StreamingRemove(currIdx);
				}

				if (modType != HMT_FLUSH)
				{
					if (disabled) { BreakOnBoundsRemoved(currIdx); }
					else { BreakOnBoundsAdded(currIdx); }
					m_boxStreamer.SetIsIgnored(currIdx.Get(), disabled);
				}
			}
		}

		if (flushOrDisable)
		{
			ClearRequiredFlag(index.Get(), STRFLAG_DONTDELETE);
			StreamingRemove(index);
		}

		if (modType != HMT_FLUSH)
		{
			if (disabled) { BreakOnBoundsRemoved(index); }
			else { BreakOnBoundsAdded(index); }
			m_boxStreamer.SetIsIgnored(index.Get(), disabled);
		}
	}

	return true;
}

//
// name:		fwStaticBoundsStore::AddSlotToPhysicsLevel
// description:	Add contents of slot to physics level
fwBoxStreamerAsset::eType fwStaticBoundsStore::AddSlotToPhysicsLevel(s32 index)
{
	fwBoundDef* pDef = GetSlot(strLocalIndex(index));

	//if(pDef->m_bb.IsInvalid())
	spdAABB& bb = m_boxStreamer.GetBounds(index);
	if(!bb.IsValid() NOTFINAL_ONLY(|| IsDirty(strLocalIndex(index))))
	{
#if __BANK
		if (IsDirty(strLocalIndex(index)))
		{
			m_boxStreamer.RebuildTrees();
			ClearDirtyFlag(strLocalIndex(index));
		}
#endif // __BANK

		if(pDef->m_pObject->GetType() == phBound::COMPOSITE)
		{
			phBoundComposite* pComposite = (phBoundComposite*)&*pDef->m_pObject;
			s32 num = pComposite->GetNumBounds();

			// add children bounds to level and store them in the array
			for(s32 i=0; i<num; i++)
			{
				phBound* pChild = pComposite->GetBound(i);
				if(pChild)
				{
					//pChild->GetBoundingBoxMin().Print();
					//pChild->GetBoundingBoxMax().Print();
					bb.GrowPoint(pChild->GetBoundingBoxMin());
					bb.GrowPoint(pChild->GetBoundingBoxMax());
				}
			}

		}
		else
		{
			//pDef->m_pObject->GetBoundingBoxMin().Print();
			//pDef->m_pObject->GetBoundingBoxMax().Print();
			bb.GrowPoint(pDef->m_pObject->GetBoundingBoxMin());
			bb.GrowPoint(pDef->m_pObject->GetBoundingBoxMax());
		}
	}

	return AddBoundToPhysicsLevel(index, pDef->m_pObject);
}

//
// name:		fwStaticBoundsStore::AddBoundToPhysicsLevel
// description:	Add bound to physics level
fwBoxStreamerAsset::eType fwStaticBoundsStore::AddBoundToPhysicsLevel(s32 ownerIndex, phBound* pBound)
{
	fwBoundDef* pDef = GetSlot(strLocalIndex(ownerIndex));

	ASSERT_ONLY(spdAABB& bb = m_boxStreamer.GetBounds(ownerIndex));

	BreakOnBoundsAdded(strLocalIndex(ownerIndex));
#if __DEV
	streamDebugf1("collision %s", pDef->m_name.GetCStr());
#endif

	bool bContainsAtLeastOneWeaponBound			= false;
	bool bContainsAtLeastOneMoverBound			= false;
	bool bContainsAtLeastOneMaterialBound		= false;		// used for procedural veg - not added to sim

	if(pBound->GetType() == phBound::COMPOSITE)
	{
		phBoundComposite* pComposite = (phBoundComposite*)&*pBound;
		Assert(pComposite->GetNumBounds() < 32767);
		s16 num = static_cast<s16>(pComposite->GetNumBounds());

		atSNode<fwBoundData>* pNode = rage_new atSNode<fwBoundData>;
		pNode->Data.m_defSlot = static_cast<u16>(ownerIndex);
		pNode->Data.m_physicsInstArray = rage_new phInst*[num];
		pNode->Data.m_physInstCount = num;
		// add children bounds to level and store them in the array
		for(s32 i=0; i<num; i++)
		{
			phBound* pChild = pComposite->GetBound(i);
			if(pChild)
			{
				u32* pFlags = pComposite->GetTypeAndIncludeFlags();
				u32 nTypeFlags = 0;
				u32 nIncludeFlags = 0;
				if(pFlags)
				{
					nTypeFlags = pComposite->GetTypeFlags(i);
					nIncludeFlags = pComposite->GetIncludeFlags(i);
				}

				bool bContainsMover = false;
				pNode->Data.m_physicsInstArray[i] = m_StaticBoundsStoreInterface->AddBoundToPhysicsLevel(pChild, ownerIndex, bContainsMover, nTypeFlags, nIncludeFlags, pDef->IsDummy(), pDef->IsDependency());
#if __DEV
				pNode->Data.m_physicsInstArray[i]->GetArchetype()->SetFilename(pDef->m_name.GetCStr());
#endif
				if (!pDef->IsDummy())
				{
					Assertf( (bb.ContainsPoint(pChild->GetBoundingBoxMin())) && (bb.ContainsPoint(pChild->GetBoundingBoxMax())),
						"Static collision file %s has incorrect bounding box - likely need to regenerate cache file", pDef->m_name.GetCStr() );
				}

				//////////////////////////////////////////////////////////////////////////
				// try to figure out what kind of file this is - mover, weapon, or material. this really needs to be formalised in data one of these days
				if (nTypeFlags)
				{
					if (bContainsMover)
					{
						bContainsAtLeastOneMoverBound = true;
					}
					else
					{
						bContainsAtLeastOneWeaponBound = true;
					}
				}
				else
				{
					bContainsAtLeastOneMaterialBound = true;
				}
				//////////////////////////////////////////////////////////////////////////

			}
			else
			{
				pNode->Data.m_physicsInstArray[i] = NULL;
			}
		}

 		artAssertf( !(bContainsAtLeastOneWeaponBound && bContainsAtLeastOneMoverBound), "%s contains a mixture of exclusively weapon collision and non-exclusively weapon collision - REEXPORT NEEDED FOR MAPSECTION", pDef->m_name.GetCStr());

		artAssertf( !(bContainsAtLeastOneMaterialBound && bContainsAtLeastOneMoverBound), "%s contains a mixture of MATERIAL collision and MOVER collision - likely tools bug", pDef->m_name.GetCStr());

		artAssertf( !(bContainsAtLeastOneMaterialBound && bContainsAtLeastOneWeaponBound), "%s contains a mixture of MATERIAL collision and WEAPON collision - likely tools bug", pDef->m_name.GetCStr());

		m_boundData.Append(*pNode);
	}

	
	if (bContainsAtLeastOneMoverBound)
	{
		return fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER;
	}
	else if (bContainsAtLeastOneWeaponBound)
	{
		return fwBoxStreamerAsset::ASSET_STATICBOUNDS_WEAPONS;
	}
	else if ( bContainsAtLeastOneMaterialBound )
	{
		return fwBoxStreamerAsset::ASSET_STATICBOUNDS_MATERIAL;
	}


	return fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER;
}

//
// name:		fwStaticBoundsStore::RemoveBoundFromPhysicsLevel
// description:	Add bound to physics level
fwBoundData* fwStaticBoundsStore::GetSlotData(const s32 index, atSNode<fwBoundData>** pPrev /*= NULL*/)
{
	atSNode<fwBoundData>* pNode = m_boundData.GetHead();

	while (pNode)
	{
		if (pNode->Data.m_defSlot == index)
			return &pNode->Data;

		if (pPrev)
			*pPrev = pNode;

		pNode = pNode->GetNext();
	}

	return NULL;
}

void fwStaticBoundsStore::RemoveBoundFromPhysicsLevel(s32 index)
{
	atSNode<fwBoundData>* pPrev = NULL;
	fwBoundData* pData = GetSlotData(index, &pPrev);

	Assertf(pData, "This is really bad! Unable to find fwBoundData for fwBoundDef %d", index);

	if (pData)
	{
		BreakOnBoundsRemoved(strLocalIndex(index));
		// remove children bounds from level
		for(s32 i=0; i<pData->m_physInstCount; i++)
		{
			if(pData->m_physicsInstArray[i])
				m_StaticBoundsStoreInterface->RemoveInstFromPhysicsWorld(pData->m_physicsInstArray[i]);
		}

		m_boundData.RemoveNext(pPrev);
		delete[] pData->m_physicsInstArray;
		delete pData;
	}
}

//
// name:		CBoundsStore::UpdateChangedStaticBounds
// description:	loads the static bounds, either from the images or from the cache file
void fwStaticBoundsStore::UpdateChangedStaticBounds()
{
	// if the cache was not loaded, load all bounds
	if (!strCacheLoader::GetLoadCache())
	{
		LoadAll(true);
	}
	else
	{
		LoadAllNew(true);
	}
}



void WriteBoundsStoreCacheEntryDetailsToTextFile(const BoundsStoreCacheEntry *pCacheEntry, fiStream *pTextFile)
{
	if (pTextFile)
	{
		if (Verifyf(pCacheEntry, "WriteBoundsStoreCacheEntryDetailsToTextFile - pCacheEntry is NULL"))
		{
			char outputString[256];

			formatf(outputString, "m_hashKey=%u\n", pCacheEntry->m_hashKey);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_min=%.2f,%.2f,%.2f\n", pCacheEntry->m_minX, pCacheEntry->m_minY, pCacheEntry->m_minZ);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_max=%.2f,%.2f,%.2f\n", pCacheEntry->m_maxX, pCacheEntry->m_maxY, pCacheEntry->m_maxZ);
			pTextFile->Write(outputString, (s32) strlen(outputString));

			formatf(outputString, "m_assetType=%u\n\n", pCacheEntry->m_assetType);
			pTextFile->Write(outputString, (s32) strlen(outputString));
		}
	}
}



//
// name:		CBoundsStore::ReadFromCacheFile
// description:	processes a line taken from the cache file (called from within a loop for this module)
bool fwStaticBoundsStore::ReadFromCacheFile(const void* const pEntry, fiStream* BANK_ONLY(pDebugTextFileToWriteTo))
{
	Vector3 vMin;
	Vector3 vMax;
	BoundsStoreCacheEntry entry(pEntry);

#if __BANK
	if (pDebugTextFileToWriteTo)
	{
		WriteBoundsStoreCacheEntryDetailsToTextFile(&entry, pDebugTextFileToWriteTo);
	}
#endif // __BANK

	strLocalIndex index = FindSlotFromHashKey(entry.m_hashKey);
	if (index != -1)
	{
		// if object is in downloadable content then don't set value
		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(index));
		strStreamingFile* pFile = strPackfileManager::GetImageFileFromHandle(info->GetHandle());

		if (strCacheLoader::GetMode() == SCL_DLC)
		{
			const atMap<s32, bool>& dlcArchives = strCacheLoader::GetDLCArchives();
			int archiveIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();

			if (dlcArchives.Access(archiveIndex) != NULL)
			{
				spdAABB boundingBox(Vec3V(entry.m_minX, entry.m_minY, entry.m_minZ), Vec3V(entry.m_maxX, entry.m_maxY, entry.m_maxZ));
				SetBoxStreamerEntry(index.Get(), boundingBox, entry.m_assetType);
			}
		}
		else
		{
			if (pFile)
			{
				if((pFile->m_extraDataIndex != 0 || pFile->m_bNew))
					return true;
			}
			// slot is valid at this point...

			spdAABB boundingBox(Vec3V(entry.m_minX, entry.m_minY, entry.m_minZ), Vec3V(entry.m_maxX, entry.m_maxY, entry.m_maxZ));
			SetBoxStreamerEntry(index.Get(), boundingBox, entry.m_assetType);
		}
	}
	return true;
}


//
// name:		CBoundsStore::AddToCacheFile
// description:	adds data based on the passed index into the cache file
void fwStaticBoundsStore::AddToCacheFile(fiStream* pDebugTextFileToWriteTo)
{
	const int boundsStorePoolSize = GetSize();
	bool addEntry = false;

	for(s32 i=0; i<boundsStorePoolSize; i++)
	{
		addEntry = false;

		if(IsValidSlot(strLocalIndex(i)))
		{
			if (strCacheLoader::GetMode() == SCL_DLC)
			{
				atMap<s32, bool> dlcArchives = strCacheLoader::GetDLCArchives();
				strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(strLocalIndex(i)));
				int index = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();

				addEntry = dlcArchives.Access(index) != NULL;
			}
			else
				addEntry = true;
		}

		if (addEntry)
		{
			if (!m_boxStreamer.GetBounds(i).IsValid())
			{
				Quitf(0, "Attempting to write invalid bounds data for valid slot! It isn't safe to continue, please discard any cache data that has been generated, redeploy, and try again. If the issue persists add a bug for *Code (Engine)*");
			}
			BoundsStoreCacheEntry entry(GetSlot(strLocalIndex(i))->m_name.GetHash(),
				m_boxStreamer.GetBounds(i).GetMinVector3(),
				m_boxStreamer.GetBounds(i).GetMaxVector3(),
				m_boxStreamer.GetAssetType(i));
			
			if (pDebugTextFileToWriteTo)
			{
				WriteBoundsStoreCacheEntryDetailsToTextFile(&entry, pDebugTextFileToWriteTo);
			}

			strCacheLoader::WriteDataToBuffer(&entry, sizeof(entry));
		}
	}
}

void fwStaticBoundsStoreWrapper::Init(unsigned /*initMode*/)
{
	g_StaticBoundsStore.Init();
}

void fwStaticBoundsStoreWrapper::Shutdown(unsigned /*shutdownMode*/)
{
	g_StaticBoundsStore.GetBoxStreamer().Shutdown();
}

//
// name:		StoreImapGroupBoundsList
// description:	store map entry for an imap group and its corresponding static bounds
void fwStaticBoundsStore::StoreImapGroupBoundsList(const atHashString& imapGroup, const atArray<atHashString>& boundsList)
{
	atArray<u32>** ppExistingList = m_imapGroupMap.Access(imapGroup.GetHash());
	if (ppExistingList)
		return;

	// store map entry from the imap group name hash to an array of static bounds name hashes
	if (boundsList.GetCount())
	{
		atArray<u32>* pList = rage_new atArray<u32>;
		for (s32 i=0; i<boundsList.GetCount(); i++)
		{
			pList->Grow() = boundsList[i].GetHash();
		}
		m_imapGroupMap.Insert(imapGroup.GetHash(), pList);
	}
}

//
// name:		fwStaticBoundsStore::SetEnabled
// description:	used by script-controlled IPL groups, to activate and deactivate bvh data
void fwStaticBoundsStore::SetEnabled(const u32 nameHash, bool bEnabled)
{
	atArray<s32> slotIndices;

	//////////////////////////////////////////////////////////////////////////
	// 1. find list of related bounds files for specified IPL group
	atArray<u32>** ppBoundsList = m_imapGroupMap.Access(nameHash);
	if (ppBoundsList)
	{
		const atArray<u32>* pList = *ppBoundsList;
		for (s32 i=0; i<pList->GetCount(); i++)
		{
			u32 boundHash = (*pList)[i];
			s32 slotIndex = FindSlotFromHashKey(boundHash).Get();
			if (slotIndex != -1)
			{
				slotIndices.PushAndGrow(slotIndex);
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// 2. set ignored flag as required
	for (s32 i=0; i<slotIndices.GetCount(); i++)
	{
		strLocalIndex index = strLocalIndex(slotIndices[i]);
		if (bEnabled)
		{
			// allow dynamic streaming for bounds
			m_boxStreamer.SetIsIgnored(index.Get(), false);
			BreakOnBoundsAdded(index);
		}
		else
		{
			// remove bounds if it is loaded, and ensure it doesn't stream in dynamically
			m_boxStreamer.SetIsIgnored(index.Get(), true);
			fwBoundDef* pDef = GetSlot(index);
			if (pDef->m_pObject)
			{
				ClearRequiredFlag(index.Get(), STRFLAG_DONTDELETE | STRFLAG_INTERIOR_REQUIRED);
				StreamingRemove(index);
				m_boxStreamer.AddToDirtyList(index.Get());
				BreakOnBoundsRemoved(index);
			}
		}
	}
}

bool fwStaticBoundsStore::IsScriptManaged(s32 index)
{
	for (atMap< u32, atArray<u32>* >::Iterator it = m_imapGroupMap.CreateIterator(); !it.AtEnd(); it.Next())
	{
		atArray<u32>* pList = (*it);
		if(pList)
		{
			for (s32 i=0; i<pList->GetCount(); i++)
			{
				u32 boundHash = (*pList)[i];
				s32 slotIndex = FindSlotFromHashKey(boundHash).Get();
				if (slotIndex == index)
				{
					return true;
				}
			}
		}
	}

	return false;
}

void fwStaticBoundsStore::SetBoxStreamerEntry(s32 index, const spdAABB& aabb, u8 assetType)
{
	if (Verifyf(!m_boxStreamer.IsLocked(), "SetBoxStreamerEntry - box streamer is locked!"))
	{
		BreakOnBoundsAdded(strLocalIndex(index));
		m_boxStreamer.SetAssetType(index, assetType);
		m_boxStreamer.GetBounds(index) = aabb;
	}
}

void fwStaticBoundsStore::RequestAndRemove(const atMap<s32, bool>& validArchives)
{
	s32 count = GetCount();
	s32 requestCount = 0;
	s32 maxReqCount = strStreamingEngine::GetInfo().GetRequestInfoPoolCapacity() - 
		strStreamingEngine::GetInfo().GetRequestInfoPoolCount();

	maxReqCount = maxReqCount < 0 ? 0 : maxReqCount;
	maxReqCount /= 2;

	for (s32 i = 0; i < count; i++)
	{
		strLocalIndex index(i);

		if (IsValidSlot(index))
		{
			strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(index));
			int archiveIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();

			if (validArchives.Access(archiveIndex) != NULL)
			{
				BreakOnBoundsAdded(strLocalIndex(index));
				StreamingRequest(index);
				requestCount++;
			}
		}

		if (requestCount >= maxReqCount)
		{
			g_strStreamingInterface->LoadAllRequestedObjects();

			requestCount = 0;
			maxReqCount = strStreamingEngine::GetInfo().GetRequestInfoPoolCapacity() - 
				strStreamingEngine::GetInfo().GetRequestInfoPoolCount();
			maxReqCount = maxReqCount < 0 ? 0 : maxReqCount;
			maxReqCount /= 2;
		}
	}

	if (requestCount > 0)
		g_strStreamingInterface->LoadAllRequestedObjects();

	for (s32 i = 0; i < count; i++)
	{
		strLocalIndex index(i);

		if (IsValidSlot(index))
		{
			strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(index));
			int archiveIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();

			if (validArchives.Access(archiveIndex) != NULL)
			{
				BreakOnBoundsRemoved(strLocalIndex(index));
				StreamingRemove(index);
			}
		}
	}
}

void fwStaticBoundsStore::PostRegistration()
{
	// set all IPL group collision to be un-defragmentable
	for (atMap< u32, atArray<u32>* >::Iterator it = m_imapGroupMap.CreateIterator(); !it.AtEnd(); it.Next())
	{
		atArray<u32>* pList = (*it);
		if(pList)
		{
			for (s32 i=0; i<pList->GetCount(); i++)
			{
				strLocalIndex slotIndex = FindSlotFromHashKey( (*pList) [i] );
				if (slotIndex != -1 &&  !GetSlot(slotIndex)->IsDummy())
				{
					strStreamingEngine::GetInfo().SetDoNotDefrag( GetStreamingIndex(slotIndex) );
				}
			
			}
		}
	}
}

// when streaming is idle and we have memory available etc, it may be desirable to scale up the
// range at which we load mover collision for gameplay reasons (e..g. sniping distant peds etc)
void fwStaticBoundsStore::UpdateStreamingRanges(bool bExpandStreamingRange)
{
	const float fMaxWarmUpDurationMs	= 2300.0f;
	const float fMaxCoolOffDurationMs	= 2800.0f;
	const float fBlendDurationMs		= 3700.0f;
	const float fMaxMoverRange			= 1.8f;

#if RSG_PC
	const float fMaxWeaponRange			= 1.5f;
	const float fMaxMaterialRange		= 1.4f;
#endif	//RSG_PC

	static bool bExpansionPermitted = false;
	static bool bExpanding = false;
	static sysTimer warmUpTimer;
	static sysTimer coolOffTimer;
	static sysTimer blendTimer;

	if (bExpanding && !bExpandStreamingRange)
	{
		if (bExpansionPermitted)
		{
			coolOffTimer.Reset();
		}
		if (coolOffTimer.GetMsTime() >= fMaxCoolOffDurationMs)
		{
			bExpanding = false;
		}
	}
	else if (!bExpanding && bExpandStreamingRange)
	{
		if (!bExpansionPermitted)
		{
			warmUpTimer.Reset();
		}
		if (warmUpTimer.GetMsTime() >= fMaxWarmUpDurationMs)
		{
			blendTimer.Reset();
			bExpanding = true;
		}
	}
	bExpansionPermitted = bExpandStreamingRange;

	if (bExpanding)
	{
		const float blendLevel = Min( 1.0f, (blendTimer.GetMsTime() / fBlendDurationMs) );
		const float fNewMoverRange = Lerp(blendLevel, 1.0f, fMaxMoverRange);
		m_boxStreamer.OverrideStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER, fNewMoverRange);

#if RSG_PC
		const float fNewWeaponRange = Lerp(blendLevel, 1.0f, fMaxWeaponRange);
		m_boxStreamer.OverrideStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_WEAPONS, fNewWeaponRange);

		const float fNewMaterialRange = Lerp(blendLevel, 1.0f, fMaxMaterialRange);
		m_boxStreamer.OverrideStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MATERIAL, fNewMaterialRange);
#endif
	}
	else
	{
		m_boxStreamer.ResetStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER);

#if RSG_PC
		m_boxStreamer.ResetStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_WEAPONS);
		m_boxStreamer.ResetStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MATERIAL);
#endif	//RSG_PC

	}

#if RSG_PC && 0
	grcDebugDraw::AddDebugOutput("Mover range = %4.2f", m_boxStreamer.GetStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MOVER) * 150.0f );
	grcDebugDraw::AddDebugOutput("Weapon range = %4.2f", m_boxStreamer.GetStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_WEAPONS) * 80.0f );
	grcDebugDraw::AddDebugOutput("Material range = %4.2f", m_boxStreamer.GetStreamRadiusScale(fwBoxStreamerAsset::ASSET_STATICBOUNDS_MATERIAL) * 50.0f );
#endif	//RSG_PC
}

#if __BANK

void fwStaticBoundsStore::PoolFullCallback(void* pItem)
{
	if(!pItem)
	{
		Displayf("Static Bounds Ran out of space, dumping info");
		g_StaticBoundsStore.DumpStaticBoundsInfo();
	}
	else
	{
		fwBoundDef* pDef = static_cast<fwBoundDef*>(pItem);
		(void)pDef;//will do something with this eventually
	}
}

void fwStaticBoundsStore::DumpStaticBoundsInfo()
{
	const int boundsStorePoolSize = GetSize();
	//print dummies
	unsigned int dcnt = 0;
	unsigned int ncnt = 0;
	Displayf("Dummy Static Bounds - - -");
	for(s32 i=0; i<boundsStorePoolSize; i++)
	{
		if(IsValidSlot(strLocalIndex(i)))
		{
			fwBoundDef* pDef = GetSlot(strLocalIndex(i));
			if(pDef->IsDummy())
			{
				Displayf( "%d: Name : %s", dcnt, pDef->m_name.GetCStr());
				dcnt++;
			}
		}
	}
	Displayf( "Dummy Total : %d", dcnt);

	//print others
	Displayf("Other Static Bounds - - -");
	for(s32 i=0; i<boundsStorePoolSize; i++)
	{
		if(IsValidSlot(strLocalIndex(i)))
		{
			fwBoundDef* pDef = GetSlot(strLocalIndex(i));
			if(!pDef->IsDummy())
			{
				Displayf( "%d: Name : %s", ncnt, pDef->m_name.GetCStr());
				ncnt++;
			}
		}
	}
	Displayf( "Normal Total : %d", ncnt);
	Displayf( "Dummy Total : %d", dcnt);

}

void PrintStaticBoundsInfo(void)
{
	g_StaticBoundsStore.DumpStaticBoundsInfo();
}

void fwStaticBoundsStore::InitWidgets(bkBank& bank)
{
	m_boxStreamer.InitWidgets(bank, "StaticBoundsStore");
	bank.AddButton("Dump StaticBounds Info", datCallback(PrintStaticBoundsInfo));	
}

#endif

} // namespace rage
