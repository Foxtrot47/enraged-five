
#ifndef FWSCENE_STORES_NETWORKDEFSTORE_H
#define FWSCENE_STORES_NETWORKDEFSTORE_H

// --- Include Files ------------------------------------------------------------

#include "fwtl\assetstore.h"
#include "move\move_network.h"

namespace rage {

class fwNetworkDefStore : public fwAssetStore<mvNetworkDef>
{
public:
	fwNetworkDefStore();

	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void Remove(strLocalIndex index);

};

extern fwNetworkDefStore g_NetworkDefStore;

} // namespace rage


#endif // FWSCENE_STORES_NETWORKDEFSTORE_H
