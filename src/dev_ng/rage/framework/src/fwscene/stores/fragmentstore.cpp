// 
// stores/fragmentstore.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "fragment/type.h"

#include "fwscene/stores/fragmentstore.h"

#include "fwscene/texLod.h"
#include "fragment/drawable.h"
#include "fwscene/stores/txdstore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"
#include "paging/rscbuilder.h"

#define GTAFRAGMENT_FILE_VERSION (rvFragObject + 18)	

namespace rage {

fwPlaceFunctor fwFragmentStore::sm_PlaceFunctor;
fwDefragmentFunctor fwFragmentStore::sm_DefragmentFunctor;

fwFragmentStore g_FragmentStore;


fwFragmentStore::fwFragmentStore() : fwAssetRscStore<Fragment, fwFragmentDef>(
		"FragmentStore", 
		FRAGMENT_FILE_ID, 
		CONFIGURED_FROM_FILE, 
		2048,
		true, 
		GTAFRAGMENT_FILE_VERSION
	)
{
}

void fwFragmentStore::Set(strLocalIndex index, Fragment* m_pObject)
{
	fwFragmentDef* pFragDef = GetSlot(index);

	Assertf(pFragDef, "No fragment at this slot");
	Assertf(pFragDef->m_pObject==NULL, "Fragment is already in memory");

#if __DEV
	// set filename on fragment archetypes
	// make assumption we have 3 level of physics detail as there is nowhere this is defined
	for(int i=0; i<3; i++)
	{
		if(m_pObject->GetPhysicsLODGroup())
		{
			fragPhysicsLOD* physicsLod = m_pObject->GetPhysics(i);
			if(physicsLod)
			{
				// get undamaged archetype
				phArchetype* archetype = physicsLod->GetArchetype(true);
				if(archetype)
					archetype->SetFilename(GetName(index));
				// get damaged archetype
				archetype = physicsLod->GetArchetype(false);
				if(archetype)
					archetype->SetFilename(GetName(index));
			}
		}
	}
#endif
	UPDATE_KNOWN_REF(pFragDef->m_pObject , m_pObject);
}


void fwFragmentStore::Remove(strLocalIndex index)
{
	ASSERT_ONLY(fwFragmentDef* pDef = GetSlot(index);)
	Assertf(pDef, "No fragment at this slot");
	Assertf(pDef->m_pObject!=NULL, "Fragment is not in memory");

	fwAssetRscStore<Fragment, fwFragmentDef>::Remove(index);
}



bool fwFragmentStore::LoadFile(strLocalIndex index, const char* pFilename)
{
	fwFragmentDef* pFragDef = GetSlot(index);

	Assertf(pFragDef, "No fragment type at this slot");
	Assertf(pFragDef->m_pObject==NULL, "Fragment is already in memory");

	fwBackupCurrentTxd backup;
	g_TxdStore.SetCurrentTxd(pFragDef->GetTxdIndex());

	Fragment* pFragment = NULL;
	pgRscBuilder::Load<Fragment>(pFragment, pFilename, FRAGMENT_FILE_EXT_PATTERN, GTAFRAGMENT_FILE_VERSION);

	Set(index, pFragment);

	return (pFragDef->m_pObject != NULL);
}


void fwFragmentStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& info)
{
	fwFragmentDef* pFragDef = GetSlot(index);

	Assertf(pFragDef, "No fragment at this slot");
	Assertf(pFragDef->m_pObject==NULL, "Fragment is already in memory");

	fwBackupCurrentTxd backup;
	g_TxdStore.SetCurrentTxd(pFragDef->GetTxdIndex());

	grcEffect::ResetMatchError();
#if __FINAL
	(sm_PlaceFunctor(info, map, NULL, false));
#else
	(sm_PlaceFunctor(info, map, GetName(index), false));
#endif
	//Set(index, obj);

	//Assert(pFragDef->m_pObject);
}



void* fwFragmentStore::Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& map, bool& flush)
{
	Fragment* object = static_cast<Fragment*>(sm_DefragmentFunctor(map));
	flush = (object && object->GetCommonDrawable() && object->GetCommonDrawable()->GetShaderGroup().GetTexDict());

	return object;
}


void fwFragmentStore::DefragmentPreprocess(strLocalIndex index)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged = true;
		fwTexLod::SetSwapStateSingle(false, fwAssetLocation(STORE_ASSET_FRG,index.Get()));			// unswap before hitting defrag code
	}
}


void fwFragmentStore::DefragmentComplete(strLocalIndex index)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBeingDefragged = false;
		fwTexLod::SetSwapStateSingle(true, fwAssetLocation(STORE_ASSET_FRG,index.Get()));			// reswap since we are unflagging the asset
	}
}


bool fwFragmentStore::IsBeingDefragged(strLocalIndex index)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBeingDefragged);
	}
	return(false);
}



bool fwFragmentStore::GetIsHDCapable(strLocalIndex index)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bHasHD);
	}
	return(false);
}


void fwFragmentStore::SetIsHDCapable(strLocalIndex index, bool val)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bHasHD = val;
	}
}



bool fwFragmentStore::GetIsBoundHD(strLocalIndex index)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		return(pDef->m_bIsBoundHD);
	}
	return(false);
}



void fwFragmentStore::SetIsBoundHD(strLocalIndex index, bool val)
{
	fwFragmentDef* pDef = fwFragmentStore::GetSlot(index);
	Assert(pDef);
	if (pDef)
	{
		pDef->m_bIsBoundHD = val;
	}
}

void fwFragmentStore::CheckIfInUse(fragType* pFrag)
{
	if (!pFrag)
	{
		return;
	}

	// check if we are attempting to destroy a frag with refs on it! BAD!

	strLocalIndex fragSlotIdx = strLocalIndex(strLocalIndex::INVALID_INDEX);
	u32 fragStoreCount = g_FragmentStore.GetCount();
	for(u32 i = 0; i<fragStoreCount; i++)
	{
		if (g_FragmentStore.GetPtr(strLocalIndex(i)) == pFrag)
		{
			fragSlotIdx = strLocalIndex(i);
			break;
		}
	}

	if (fragSlotIdx != strLocalIndex(strLocalIndex::INVALID_INDEX))
	{
		char buf[32];
		g_FragmentStore.GetRefCountString(fragSlotIdx, buf, NELEM(buf));
		Assertf((g_FragmentStore.GetNumRefs(fragSlotIdx) == 0), "frag: %s being destroyed with refs (%s)! BAD.", g_FragmentStore.GetName(fragSlotIdx), buf);
	}
}

/*
void fwFragmentStore::RemoveRef(strLocalIndex index, strRefKind strRefKind)
{
	ASSERT_ONLY(fwFragmentDef* pDef = GetSlot(index);)

//	if (GetNumRefs(index) == 1)
//	{
//		// it may be necessary to search through the shader effects and look for refs to this fragStore slot - because that would be bad.
//		Assertf(GetIsTextureUsedExternally(index) == false, "fragment : %s is still used externally!", GetName(index));
//	}

	Assertf(pDef, "No fragment at this slot");
	Assertf(pDef->m_pObject!=NULL, "Fragment is not in memory");

	fwAssetRscStore<Fragment, fwFragmentDef>::RemoveRef(index, strRefKind);
}*/

#if __ASSERT
void fwFragmentStore::SetParentTxdForSlot(strLocalIndex index, strLocalIndex parent)
{
	if (GetSlot(index)->GetTxdIndex() != parent)
	{
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(GetStreamingIndex(index));

		Assertf(info.GetStatus() == STRINFO_NOTLOADED, "%s sets its parent TXD from %d to %d, but its status is %s", GetName(index), GetSlot(index)->GetTxdIndex().Get(), parent.Get(), info.GetFriendlyStatusName());
		GetSlot(index)->SetTxdIndex(parent.Get());
	}
}
#endif // __ASSERT


int fwFragmentStore::GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const
{
	int count = 0;
	strLocalIndex parentTxd = fwFragmentStore::GetParentTxdForSlot(index);
	if(parentTxd != -1)
	{
		AddDependencyOutput(pIndices, count, g_TxdStore.GetStreamingModule()->GetStreamingIndex(parentTxd), indexArraySize, GetStreamingIndex(index));
	}
	return count;
}



void fwFragmentStore::ShutdownLevel()
{
	RemoveAll();
}

void fwFragmentStoreWrapper::Shutdown(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		g_FragmentStore.Shutdown();
	}
	else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
	{
		g_FragmentStore.ShutdownLevel();
	}
}

}	// namespace rage
