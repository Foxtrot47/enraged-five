/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textAsset.cpp 
// PURPOSE : Classes representing the localised text database asset and definitions
//			 required for streamed resource loading
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////
#include "atl/array_struct.h"
#include "atl/map_struct.h"
#include "textAsset.h"

namespace rage 
{

fwTextField::fwTextField()
	: pgBase()
	, m_textBlob( NULL )
	, m_textBlobSize( 0 )
{

}

fwTextField::~fwTextField()
{
	if( m_textBlob )
	{
		delete[] m_textBlob;
	}
}

fwTextField::fwTextField( datResource& rsc )
	: pgBase()
{
	rsc.PointerFixup( m_textBlob );
}

IMPLEMENT_PLACE( fwTextField );

#if __DECLARESTRUCT

void fwTextField::DeclareStruct( class datTypeStruct &s )
{
	pgBase::DeclareStruct( s );

	STRUCT_BEGIN( fwTextField );
	STRUCT_DYNAMIC_ARRAY( m_textBlob, m_textBlobSize );
	STRUCT_FIELD( m_textBlobSize );
	STRUCT_END();
}

#endif // __DECLARESTRUCT

// PURPOSE: Callback used to patch up data in our text record map
// PARAMS:
//		rsc - Resource to build from
//		data - Actual object we are patching up
void FixupDataFunc(datResource &rsc, datOwner<fwTextField>& data)
{
	data.Place( &data, rsc );
}

void ReleaseTextField( u32 const UNUSED_PARAM( key ), fwTextField* textField )
{
	Assertf( textField, "Null pointer passed to textfield cleanup function!" );
	delete textField;
}

fwTextAsset::fwTextAsset()
	: pgBase()
	, m_textRecords()
{

}

fwTextAsset::~fwTextAsset()
{
	Shutdown();
}

fwTextAsset::fwTextAsset( datResource& rsc )
	: pgBase()
	, m_textRecords( rsc, NULL, FixupDataFunc )
{

}

IMPLEMENT_PLACE( fwTextAsset );

#if __DECLARESTRUCT

void fwTextAsset::DeclareStruct( class datTypeStruct &s )
{
	pgBase::DeclareStruct(s);

	STRUCT_BEGIN(fwTextAsset);
	STRUCT_FIELD(m_textRecords);
	STRUCT_END();
}

#endif // __DECLARESTRUCT

char const * fwTextAsset::getTextField( u32 const key ) const
{
	char const * result = NULL;

	const rage::datOwner<fwTextField> * ppTextField = m_textRecords.Access( key );
	if( ppTextField && *ppTextField )
	{
		const fwTextField* pTextField = *ppTextField;
		result = pTextField->getText();
	}

	return result;
}

void fwTextAsset::Shutdown()
{
	ForAll( &ReleaseTextField );
	m_textRecords.Kill();
}

void fwTextAsset::ForAll( TextFieldCallback callbackFunction )
{
	Assertf( callbackFunction, "Invalid callback" );
	TextRecordCollection::Iterator itr = m_textRecords.CreateIterator();
	for( itr.Start(); callbackFunction && !itr.AtEnd(); itr.Next() )
	{
		callbackFunction( itr.GetKey(), *itr );
	}
}

char const * fwTextAssetDef::getTextField( u32 const key ) const
{
	char const* result = NULL;

	Assertf( m_pObject, "Attempting to get text field from an unloaded text database!" );
	if( m_pObject )
	{
		result = m_pObject->getTextField( key );
	}

	return result;
}

} // namespace rage
