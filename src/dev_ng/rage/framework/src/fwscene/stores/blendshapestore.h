
#ifndef FWSCENE_STORES_BLENDSHAPESTORE_H_
#define FWSCENE_STORES_BLENDSHAPESTORE_H_

// --- Include Files ------------------------------------------------------------

#include "grblendshapes\blendshapes_config.h"
#include "grblendshapes\manager.h"
#include "fwtl\assetstore.h"

#if ENABLE_BLENDSHAPES

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

namespace rage {

extern fwAssetRscStore<grbTargetManager> g_BlendShapeStore;

// have to specialise the template version of Remove as remove calls the destructor and the destructor
// for grbTargetManager is private
template<> void fwAssetStore<grbTargetManager>::Remove(int index);

} // namespace rage

#endif // ENABLE_BLENDSHAPES

#endif // FWSCENE_STORES_BLENDSHAPESTORE_H_
