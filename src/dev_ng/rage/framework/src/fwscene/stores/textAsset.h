/////////////////////////////////////////////////////////////////////////////////
// 
// FILE    : textAsset.h 
// PURPOSE : Classes representing the localised text database asset and definitions
//			 required for streamed resource loading
// 
// AUTHOR    : james.strain
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWTEXT_ASSET_H_
#define FWTEXT_ASSET_H_

#include "atl/array.h"
#include "atl/map.h"
#include "fwtl/assetstore.h"
#include "paging/base.h"
#include "paging/ref.h"
#include "fwlocalisation/textUtil.h"

namespace rage 
{

class datResource;
class fwTextAsset;
class fwTextFileConversion;

// PURPOSE: Represents a localized string
class fwTextField : public pgBase
{
public:
	fwTextField();
	virtual ~fwTextField();

	// resourcing
	explicit fwTextField( datResource& rsc );

	explicit fwTextField( fwTextField const& other );
	fwTextField& operator=( fwTextField const& other );

	DECLARE_PLACE(fwTextField);

#if __DECLARESTRUCT
	virtual void DeclareStruct(class datTypeStruct &s);
#endif

	inline char const* getText() const { return m_textBlob; }
	inline u32 getTextBlobSize() const { return m_textBlobSize; }

private:
	friend fwTextAsset; // For accessing the populate function

	char*		m_textBlob;
	u32				m_textBlobSize;

	// PURPOSE: Populate the text value of this entry
	// PARAMS:
	//		textValue - Text value to populate this object with
	// RETURNS:
	//		True if successful, false otherwise
	bool populate( char const * const textValue )
	{
		bool success = false;

		Assertf( m_textBlob == 0, "Textfield already populated!" );
		if( m_textBlob == 0 )
		{
			Assertf( textValue, "Null text string provided!" );
			if( textValue )
			{
				m_textBlobSize = fwTextUtil::GetByteCount( textValue ) + 1;

				Assertf( m_textBlobSize > 0, "Unable to determine text blob size!" );
				if( m_textBlobSize > 0 )
				{
					m_textBlob = rage_new char[ m_textBlobSize ];
					Assertf( m_textBlob, "Unable to allocate text blob!" );

					if( m_textBlob )
					{
						safecpy( m_textBlob, textValue, m_textBlobSize );
						success = true;
					}
				}
			}
		}

		return success;
	}
};

// PURPOSE: Represents a collection of key/value localized string data
class fwTextAsset : public pgBase
{
public:
	static const int RORC_VERSION = 3;

	fwTextAsset();
	virtual ~fwTextAsset();

	// resourcing
	explicit fwTextAsset( datResource& rsc );

	DECLARE_PLACE(fwTextAsset);

#if __DECLARESTRUCT
	virtual void DeclareStruct(class datTypeStruct &s);
#endif

	// PURPOSE: Search this text asset for the given string.
	// PARAMS:
	//		key - Hash key for the given string
	// RETURNS:
	//		String pointer if found, null otherwise
	char const * getTextField( u32 const key ) const;

private:
	friend class fwTextFileConversion; // For access to the "add" function

	// PURPOSE: Callback used for text field
	typedef void (*TextFieldCallback)( u32 const, fwTextField* );

	typedef atMap<u32, datOwner<fwTextField> >	TextRecordCollection;
	TextRecordCollection						m_textRecords;

	// PURPOSE: Cleanup this text asset
	void Shutdown();

	// PURPOSE: Executes the given callback against all textfields owned by this asset
	// PARAMS:
	//		callbackFunction - Callback to execute
	void ForAll( TextFieldCallback callbackFunction );

	// PURPOSE: Add a given key/text pair to this text asset
	// PARAMS:
	//		key - Hashed identifer for the text value
	//		textValue - Actual text value
	// RETURNS:
	//		True if added, false otherwise
	bool addText( u32 const key, char const * const textValue )
	{
		bool success = false;

		Assertf( key != 0, "Invalid hash key given!" );
		Assertf( textValue, "Null text string provided!" );
		if( key != 0 && textValue )
		{
			Assertf( m_textRecords.Access( key ) == 0, "Asset already contains a record with hash %u!", key );
			if( m_textRecords.Access( key ) == 0 )
			{
				fwTextField* pTextField = rage_new fwTextField();
				Assertf( pTextField, "Unable to allocate text field!" );
				if( pTextField )
				{
					success = pTextField->populate( textValue );
					if( success )
					{
						m_textRecords.Insert( key, pTextField );
						success = m_textRecords.Access( key ) != 0;
					}
					else
					{
						delete pTextField;
					}
				}

				success = true;
			}
		}

		return success;
	}
};

// PURPOSE: Streaming system definition for a text asset
class fwTextAssetDef : public fwAssetDef< fwTextAsset >
{
public:

	// PURPOSE: Search this text asset for the given string.
	// PARAMS:
	//		key - Hash key for the given string
	// RETURNS:
	//		String pointer if found, null otherwise
	char const * getTextField( u32 const key ) const;

private:
};

} // namespace rage

#endif // FWTEXT_ASSET_H_
