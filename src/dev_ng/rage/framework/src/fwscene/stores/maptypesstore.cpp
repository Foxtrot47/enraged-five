//
// fwscene/stores/maptypesstore.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "maptypesstore.h"

#include "fwdrawlist/drawlistmgr.h"
#include "fwsys/fileexts.h"
#include "entity/archetype.h"
#include "fwrenderer/renderthread.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/world/worldmgr.h"
#include "fwscene/world/scenegraphnode.h"
#include "fwscene/world/streamedscenegraphnode.h"
#include "fwsys/gameskeleton.h"
#include "parser/manager.h"
#include "parser/psoconsts.h"
#include "parser/psofile.h"
#include "streaming/streaming_channel.h"
#include "streaming/streamingvisualize.h"
#include "system/threadtype.h"
#include "math/math.h"
#include "fwgeovis/geovis.h"

#include "ai/navmesh/navmesh.h" // for HEIGHTMAP_GENERATOR_TOOL

XPARAM(PartialDynArch);


namespace rage 
{
fwMapTypesStore g_MapTypesStore;

fwMapTypesStore::fwMapTypesStore() : fwAssetStore<fwMapTypesContents, fwMapTypesDef>("MapTypesStore", MAPTYPES_FILE_ID, CONFIGURED_FROM_FILE, 256, true)
{
	m_bEnableMapTypeOutput = false;
	m_bIsShuttingDown = false;
}

void fwMapTypesStore::RegisterStreamingModule()
{
	fwAssetStore<fwMapTypesContents, fwMapTypesDef>::RegisterStreamingModule();
} 

// make sure we identify kosher .#typ files which will be streamed properly - not using backwards compat.
strLocalIndex fwMapTypesStore::Register(const char* name){

	strLocalIndex typeIndex = strLocalIndex(fwAssetStore<fwMapTypesContents, fwMapTypesDef>::Register(name));

	fwMapTypesDef* pDef = GetSlot(typeIndex);
	Assertf(pDef, "No entry at this slot");
	pDef->SetIsNativeFile(true);

	if (PARAM_PartialDynArch.Get())
	{
		pDef->SetIsPermanent(true);			// mark all .ityp files as global for now
	}

	return(typeIndex);
}

void fwMapTypesStore::Remove(strLocalIndex mapTypeDefIndex){

	fwMapTypesDef* pDef = GetSlot(mapTypeDefIndex);
//	Displayf("=== (-) streamed archetypes : %d\n", fwArchetypeManager::GetCurrentNumStreamedArchetypes());

	Assertf(pDef, "No entry at this slot");

	bool bFreeArchetypesNeeded = !pDef->GetIsPermanent() && !pDef->GetIsPermanentDLC();

	m_loader.Unload(pDef->m_pObject->GetLoaderInstRef(), true);

	fwAssetStore<fwMapTypesContents, fwMapTypesDef>::Remove(mapTypeDefIndex);
	/*printf("fwMapTypeStore::Remove : %d!\n", mapTypeDefIndex);*/

	// free the objects created by this mapTypeDef (marked by the mapTypeDefIdx)
	//if (pDef->GetIsDependency())
	if (bFreeArchetypesNeeded)
	{
		fwArchetypeManager::FreeArchetypes(mapTypeDefIndex.Get());

		//Warningf("=== (-) streamed archetypes : %d\n", fwArchetypeManager::GetCurrentNumStreamedArchetypes());

		if (!m_bIsShuttingDown)
		{
			// any .ityp dependencies need to hang around a little bit longer, until refs to the archetypes flush through the render thread
			for(s32 i=0; i<pDef->GetNumParentTypes(); i++){
				gDrawListMgr->AddTypeFileReference(pDef->GetParentTypeIndex(strLocalIndex(i)).Get());
			}
		}
	} else 
	{
#if !HEIGHTMAP_GENERATOR_TOOL
		Displayf("Removing permanent archetype file : %s.ityp", g_MapTypesStore.GetName(mapTypeDefIndex));
#endif
	}

	bool bHasObjectLoaded = HasObjectLoaded(mapTypeDefIndex);	
	bool bNumRefsIsOne = (GetNumRefs(mapTypeDefIndex) == 1);
	bool bObjectNotRequired = !IsObjectRequired(mapTypeDefIndex);
	bool bNoDontDeleteFlag = !(GetStreamingFlags(mapTypeDefIndex)&STR_DONTDELETE_MASK);

	Assert(!bHasObjectLoaded);
	Assert(!bNumRefsIsOne);
	Assert(bObjectNotRequired);
	Assert(bNoDontDeleteFlag);

	if (bHasObjectLoaded && bNumRefsIsOne && bObjectNotRequired && bNoDontDeleteFlag)
	{
		Quitf(ERR_GEN_MAPSTORE_1, "map type slot <%d> isn't cleaned up properly", mapTypeDefIndex.Get());
	}

	if (bNumRefsIsOne)
	{
		Quitf(ERR_GEN_MAPSTORE_2, "Ref should definitely not be 1 for a deleted type file %d", mapTypeDefIndex.Get());
	}
}

void fwMapTypesStore::Init(u32 initMode)
{
	if(initMode == INIT_BEFORE_MAP_LOADED)
	{
		// initialise the box streamer quadtree
		//m_boxStreamer.Init(this, MAPDATA_BB_EXTRASIZE, fwRect(WORLDLIMITS_REP_XMIN, WORLDLIMITS_REP_YMIN, WORLDLIMITS_REP_XMAX, WORLDLIMITS_REP_YMAX), 3);

		// configure the PSO loader
		atFixedBitSet32 flags;
		flags.Set(fwPsoStoreLoader::LOAD_IN_PLACE).Set(fwPsoStoreLoader::WARN_ON_LAYOUT_MISMATCH).Set(fwPsoStoreLoader::ASSERT_ON_LAYOUT_MISMATCH);
		g_MapTypesStore.m_loader.SetFlags(flags);
		g_MapTypesStore.m_loader.SetInPlaceHeap(PSO_DONT_ALLOCATE);
		g_MapTypesStore.m_loader.SetNotInPlaceHeap(PSO_HEAP_DEBUG);
		g_MapTypesStore.m_loader.SetRootType<fwMapTypes>();
	}
	m_bIsShuttingDown = false;
}

void fwMapTypesStore::ShutdownLevelWithMapLoaded()
{
	m_bIsShuttingDown = true;

	gRenderThreadInterface.Flush();			// allow all refs to protect archetype access from RT to clear

	SafelyRemoveTypeSlots(TYPE_FILE_STREAMED);

#if !__FINAL
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(strLocalIndex(i)) && Get(strLocalIndex(i)) && GetNumRefs(strLocalIndex(i)) > 0)
		{
			fwMapTypesDef* pDef = GetSlot(strLocalIndex(i));
			Displayf("%s : %d : %s\n", GetName(strLocalIndex(i)), GetNumRefs(strLocalIndex(i)), pDef->GetIsDependency() ? "DEP" : " ");
		}
	}
#endif

//    RemoveAll(); // does nothing now?
}

void fwMapTypesStore::ShutdownLevelWithMapUnloaded()
{
	// remove permanent entries
	for (s32 i = 0; i < GetSize(); ++i)
	{
		if (IsValidSlot(strLocalIndex(i)) && Get(strLocalIndex(i)) && GetNumRefs(strLocalIndex(i)) > 0)
		{
			Assert(GetSlot(strLocalIndex(i))->GetIsPermanent());
			SafeRemoveTypeSlot(strLocalIndex(i));
		}
	}

    Shutdown();
}

#if !__FINAL && 0 //disabled for now but may be useful
void fwMapTypesStore::RemoveRef(strLocalIndex index, strRefKind strRefKind)
{
	fwAssetStore<fwMapTypesContents, fwMapTypesDef>::RemoveRef(index, strRefKind);

	if (strstr(GetName(index), "hei_mp_residential_heist") != NULL)
	{
		Displayf("TOMW: %s - %i", GetName(index), GetNumRefs(index));
		diagPrintStackTrace();
	}
}

void fwMapTypesStore::AddRef(strLocalIndex index, strRefKind strRefKind)
{
	fwAssetStore<fwMapTypesContents, fwMapTypesDef>::AddRef(index, strRefKind);

	if (strstr(GetName(index), "hei_mp_residential_heist") != NULL)
	{
		Displayf("TOMW: %s - %i", GetName(index), GetNumRefs(index));
		diagPrintStackTrace();
	}
}
#endif

bool fwMapTypesStore::ModifyHierarchyStatus(strLocalIndex index, eHierarchyModType modType)
{
	if (index.IsValid() && IsValidSlot(index) && modType != HMT_ENABLE)
	{
		if (GetNumRefs(index) > 0)
			gRenderThreadInterface.Flush();

		SafeRemoveTypeSlot(index);
		
		if (GetNumRefs(index) > 0)
			gRenderThreadInterface.Flush();
	}

	return true;
}

void fwMapTypesStore::RemoveUnrequired(){

	s32 count = GetSize();

	for(s32 index=0; index<count; index++)
	{
		fwMapTypesDef* pDef = GetSlot(strLocalIndex(index));

		// delete object immediately when refs are all gone. (otherwise they are taking up valuable streaming archetype slots)
		if(			HasObjectLoaded(strLocalIndex(index))	
				&&	(GetNumRefs(strLocalIndex(index)) == 1)	
				&&	!IsObjectRequired(strLocalIndex(index))	
				&&	!(GetStreamingFlags(strLocalIndex(index))&STR_DONTDELETE_MASK) )
		{
			Assertf(pDef, "No entry at slot %d", index);
			Assertf(pDef->m_pObject,"No object, but attempting to remove contents of slot %d", index);

			if (!pDef->GetIsPermanent() && !pDef->GetIsPermanentDLC())
			{
				RemoveRef(strLocalIndex(index), REF_OTHER);

				strStreamingInfoManager &infoMgr = strStreamingEngine::GetInfo();
				if (infoMgr.IsObjectInUse(GetStreamingIndex(strLocalIndex(index))))
				{
					AddRef(strLocalIndex(index), REF_OTHER);
					continue;
				} else
				{
					strStreamingInfo& info = infoMgr.GetStreamingInfoRef(GetStreamingIndex(strLocalIndex(index)));
					// suggested by MK to catch dependents which are currently streaming in : bug 893753
					if (info.GetDependentCount())
					{
						AddRef(strLocalIndex(index), REF_OTHER);
						continue;
					}
				}

				if (!StreamingRemove(strLocalIndex(index)))
				{
					char refString[16];
					GetRefCountString(strLocalIndex(index), refString, sizeof(refString));

					Displayf("Dumping streaming flags : 0x%x", g_MapTypesStore.GetStreamingFlags(strLocalIndex(index)));
					Assertf(false, "Streaming remove failed! (but should be safe) : %s (%s)", g_MapTypesStore.GetName(strLocalIndex(index)), refString);
					AddRef(strLocalIndex(index), REF_OTHER);
				}
			}
		} 
	}
}

void fwMapTypesStore::Update(){

	STRVIS_AUTO_CONTEXT(strStreamingVisualize::MAPTYPESTORE);

	RemoveUnrequired();
}

void fwMapTypesStore::SafeRemoveTypeSlot(strLocalIndex slotIndex)
{
	// SafeRemove(slotIndex)
	if (HasObjectLoaded(slotIndex))
	{
		ClearRequiredFlag(slotIndex.Get(), STRFLAG_DONTDELETE);

		char refString[16];
		GetRefCountString(slotIndex, refString, sizeof(refString));

		Assertf(GetNumRefs(slotIndex)==1, "Removing an ITYP %s with ref count %d (%s)", GetName(slotIndex), GetNumRefs(slotIndex), refString);
		RemoveRef(slotIndex, REF_OTHER);
		if (!StreamingRemove(slotIndex))
		{
			AddRef(slotIndex, REF_OTHER);
		}
	}
}

s32 maxDepth = 0;

void fwMapTypesStore::SetDepth(atArray<s32>& typeDepths, strLocalIndex slotIndex, s32 currDepth)
{
	fwMapTypesDef* pDef = GetSlot(slotIndex);
	if (pDef)
	{
		for(s32 j=0; j< pDef->GetNumParentTypes(); j++)
		{
			strLocalIndex parentSlotIdx = strLocalIndex(pDef->GetParentTypeIndex(strLocalIndex(j)));
			SetDepth(typeDepths, parentSlotIdx, currDepth+1);
		}

		if (currDepth > typeDepths[slotIndex.Get()]){
			typeDepths[slotIndex.Get()] = currDepth;
		}

		maxDepth = rage::Max(maxDepth, currDepth);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SafeRemoveAll
// PURPOSE:		removing all loaded ityps
//////////////////////////////////////////////////////////////////////////


void fwMapTypesStore::SafelyRemoveTypeSlots(u32 typeFileCategory)
{
	s32 numDefs = GetNumUsedSlots();

	atArray<s32>	typeDefDepths;
	typeDefDepths.Reserve(numDefs);
	typeDefDepths.Resize(numDefs);

	maxDepth = 0;
	
	for(s32 i=0; i<numDefs; i++)
	{	
		fwMapTypesDef* pDef = GetSlot(strLocalIndex(i));
		if (pDef)
		{
			SetDepth(typeDefDepths, strLocalIndex(i), 0);
		}
	}

	for(s32 depth = 0 ; depth <= maxDepth; depth++)
	{
		for(s32 i=0; i<numDefs; i++)
		{
			fwMapTypesDef* pDef = GetSlot(strLocalIndex(i));

			if (pDef)
			{
				bool bShouldRemove = false;
			
				switch (typeFileCategory)
				{
				case TYPE_FILE_BASE:
					if (pDef->GetIsPermanent() && !pDef->GetIsPermanentDLC())
					{
						Assertf(false,"Not expecting to free base archetype files");
						bShouldRemove = true;
					}
					break;
				case TYPE_FILE_STREAMED:
					if (!pDef->GetIsPermanent() && !pDef->GetIsPermanentDLC())
					{
						bShouldRemove = true;
					}
					break;
				case TYPE_FILE_DLC:
					Assertf(false, "[code systems]Dead code path! This code should not be executed!");
// 					if (pDef->GetIsPermanent() && pDef->GetIsDLC())
// 					{
// 						bShouldRemove = true;
// 					}
					break;
				case TYPE_FILE_DLC_STREAMED:
					Assertf(false, "[code systems]Dead code path! This code should not be executed!");
// 					if (!pDef->GetIsPermanent() && pDef->GetIsDLC())
// 					{
// 						bShouldRemove = true;
// 					}
					break;
				}

				if (bShouldRemove)
				{
					if (typeDefDepths[i] == depth)
					{
						SafeRemoveTypeSlot(strLocalIndex(i));
					}
				}
			}
		}
	}


// 	for(s32 i=0; i<m_pool.GetSize(); i++)
// 	{	
// 		fwMapTypesDef* pDef = GetSlot(i);
// 		//s32 slotIndex = m_pool.GetJustIndex(pDef);
// 
//  		if (pDef)
//  		{
// 			ClearRequiredFlag(i, STRFLAG_DONTDELETE);
// 			SafeRemoveTypeSlot(i);				
// 		}
// 	}

}

#if !__NO_OUTPUT
void fwMapTypesStore::PrintStore()
{
	const int N = 4;
	for (int i = 0; i < GetSize(); i += N)
	{
		for (int j = 0; j < N; j++)
		{
			const strLocalIndex slot = strLocalIndex(i + j);

			if (slot.Get() < GetSize() && IsValidSlot(slot))
			{
				const fwMapTypesDef* pDef = GetSlot(slot);

				if (pDef)
				{
					const char* type = pDef->GetIsPermanent() ? "permanent" : (pDef->GetIsPermanentDLC() ? "permanent DLC" : "streamed");
					const fwMapTypesContents* pContents = Get(slot);
					const char* state;

					if (pContents)
						state = "loaded";
					else if (IsObjectInImage(slot))
						state = "requested";
					else // not in streaming image? wtf.
						state = "unknown";

					Displayf("%d,%s,%s,%s", slot.Get(), GetName(slot), type, state);
				}
			}
		}
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetDependencies
// PURPOSE:		returns the parent .ityps
//////////////////////////////////////////////////////////////////////////
int fwMapTypesStore::GetDependencies(strLocalIndex index, strIndex* pIndices, int indexArraySize) const
{
	s32 count = 0;

	const fwMapTypesDef* pDef = GetSlot(index);

	// deps are not known until the def has been initialised
	if (pDef)
	{
// 		fwMapDataDef* pParentDef = pDef->GetParentDef();
// 		if (pParentDef)
// 		{
// 			s32 parentSlot = m_pool.GetJustIndex(pParentDef);
// 				AddDependencyOutput(pIndices, count, GetStreamingIndex(parentSlot), indexArraySize, GetStreamingIndex(indexArraySize));
// 		}	

		u32 numTypeFileDependencies = pDef->GetNumParentTypes();
		for(u32 i =0; i<numTypeFileDependencies; i++){
			strLocalIndex parentTypeIndex = pDef->GetParentTypeIndex(strLocalIndex(i));
			AddDependencyOutput(pIndices, count, g_MapTypesStore.GetStreamingModule()->GetStreamingIndex(parentTypeIndex), indexArraySize, GetStreamingIndex(strLocalIndex(indexArraySize)));
		}
		// TODO: other deps? physics dictionaries?
	}

	return count;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetDataPtr
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void* fwMapTypesStore::GetDataPtr(strLocalIndex index)	
{
	Assert(index.Get() >= 0);
	fwMapTypesDef* pDef = GetSlot(index);
	if(pDef && pDef->m_pObject)
		return pDef->m_pObject->GetLoaderInstRef().GetStorage();
	return NULL;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequiresTempMemory
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
bool fwMapTypesStore::RequiresTempMemory(strLocalIndex index) const
{
	const fwMapTypesDef* pDef = GetSlot(index);
	fwMapTypesContents* pMapTypes = Get(index);

	// If the LOADER owns the storage, that means the STREAMER does not. Therefore the streamed
	// memory is considered "temp". If the loader thinks the file is not loaded, the streamer
	// should clean up any memory it holds too.
	if (pMapTypes && (!pMapTypes->GetLoaderInstRef().IsLoaded() || pMapTypes->GetLoaderInstRef().OwnsStorage()))
	{
		return true;
	}

	if (pDef  && pDef->RequiresTempMemory())
	{
		return true;
	}
	return false;
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Load
// PURPOSE:		notification from streaming that a requested file is now in memory
//////////////////////////////////////////////////////////////////////////
bool fwMapTypesStore::Load(strLocalIndex mapTypeDefIndex, void* pData, int size)
{
	const char *debugName = "";
#if !__NO_OUTPUT															
	char debugNameBuffer[RAGE_MAX_PATH];
	debugName = strStreamingEngine::GetInfo().GetObjectName(GetStreamingIndex(mapTypeDefIndex), debugNameBuffer, NELEM(debugNameBuffer));
#endif

	fwPsoStoreLoadResult loadResult;
	fwPsoStoreLoadInstance loadInst;
	loadResult = m_loader.Load(pData, size, debugName, loadInst);
	FinishLoading(mapTypeDefIndex, loadInst);

	//Displayf("=== (+) streamed archetypes : %d\n", fwArchetypeManager::GetCurrentNumStreamedArchetypes());

	return(true);
}


void fwMapTypesStore::FinishLoading(strLocalIndex mapTypeDefIndex, fwPsoStoreLoadInstance &loadInst )
{
	fwMapTypesDef* pDef = GetSlot(mapTypeDefIndex);
	fwMapTypes* maptypes = reinterpret_cast<fwMapTypes*>(loadInst.GetInstance());

	const bool bRequiresTempMem		= ( pDef->RequiresTempMemory() );

	fwMapTypesContents* contents = maptypes->CreateMapTypesContents();
	contents->GetLoaderInstRef() = loadInst;

	contents->m_name = atHashString(pDef->m_name.GetHash());

	pDef->SetIsFastPso(loadInst.IsInPlace());
	pDef->SetIsInPlace(loadInst.IsInPlace());
	pDef->SetIsPso(loadInst.IsPso());

	maptypes->Construct(mapTypeDefIndex.Get(), contents);

	Set(mapTypeDefIndex, contents);

	if (bRequiresTempMem)
	{
		m_loader.Unload(contents->GetLoaderInstRef());
	}

	AddRef(mapTypeDefIndex, REF_OTHER);	// protect against streaming cleanup removing ityps

	// check that items that need to store data have a valid data block
	if (!pDef->RequiresTempMemory() && !pDef->m_pObject->GetLoaderInstRef().GetStorage() )
	{
		Assert(false);
	}
}

void fwMapTypesStore::PlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex), strStreamingLoader::StreamingFile& file, datResourceInfo& resInfo)
{
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
	streamAssertf(pInfo->GetStatus() == STRINFO_LOADING, "Info status is %d", pInfo->GetStatus());
	pInfo->SetFlag(file.m_Index, STRFLAG_INTERNAL_PLACING);
	strStreamingEngine::GetLoader().RequestAsyncPlacement(file, file.m_LoadingMap, resInfo);
}


void fwMapTypesStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header)
{
	psoResourceData* resource;
	pgRscBuilder::PlaceStream(resource, header, map, "PSO resource");

	fwPsoStorePlacementCookies::Cookie cookie;

	const char *debugName = "";
#if !__NO_OUTPUT															
	char debugNameBuffer[RAGE_MAX_PATH];
	debugName = strStreamingEngine::GetInfo().GetObjectName(GetStreamingIndex(index), debugNameBuffer, NELEM(debugNameBuffer));
#endif
	/*fwPsoStoreLoadResult loadResult = */ m_loader.LoadResource(resource, debugName, cookie.m_Instance);

	g_PsoPlacementCookies.Add(GetStreamingIndex(index), cookie);
}

void fwMapTypesStore::SetResource(strLocalIndex index, datResourceMap& /*map*/)
{
	fwPsoStorePlacementCookies::Cookie cookie = g_PsoPlacementCookies.Get(GetStreamingIndex(index));

	FinishLoading(index, cookie.m_Instance);

	g_PsoPlacementCookies.Remove(GetStreamingIndex(index));
}

// check all Ityp files registered are either Permanent, or are dependencies
/*#if __DEV*/
void fwMapTypesStore::VerifyItypFiles()
{

	// check all .ityps
	for(s32 index=0; index<g_MapTypesStore.GetSize(); index++)
	{
		// every .ityp must be marked as permenent, or as a dependency
		fwMapTypesDef* pDef = g_MapTypesStore.GetSlot(strLocalIndex(index));
		if (pDef && !pDef->GetIsPermanent() && !pDef->GetIsDependency() && !pDef->GetIsPermanentDLC())
		{
			Warningf("%s : not currently used. Not set as Permanent in images.meta file, or set as any .imap file dependency", g_MapTypesStore.GetName(strLocalIndex(index)));
			Assertf(false,"%s : not currently used. Not set as Permanent in images.meta file, or set as any .imap file dependency", g_MapTypesStore.GetName(strLocalIndex(index)));
		}
	}


	// check all .imaps
	for(s32 index=0; index<INSTANCE_STORE.GetSize(); index++)
	{
		// every .imap which is marked as contains MLO, must have at least 1 MLO .ityp as a dependency
		fwMapDataDef* pDef = INSTANCE_STORE.GetSlot(strLocalIndex(index));
		if (pDef && pDef->GetIsMLOInstanceDef())
		{
			bool bHasMLOTypeDependency = false;
			for(u32 j=0;j<pDef->GetNumParentTypes(); j++)
			{
				strLocalIndex parentIdx = strLocalIndex(pDef->GetParentTypeIndex(j));
				if (g_MapTypesStore.GetSlot(parentIdx)->GetIsMLOType())
				{
					bHasMLOTypeDependency = true;
					continue;
				}
			}
		
			if (!bHasMLOTypeDependency)
			{
				Warningf("%s : marked as contains MLO, but has no MLO type as dependency", INSTANCE_STORE.GetName(strLocalIndex(index)));
				Assertf(bHasMLOTypeDependency,"%s : marked as contains MLO, but has not MLO type as dependency", INSTANCE_STORE.GetName(strLocalIndex(index)));
				for(u32 j=0;j<pDef->GetNumParentTypes(); j++)
				{
					Warningf("	-> patching dep : %s", g_MapTypesStore.GetName(strLocalIndex(pDef->GetParentTypeIndex(j))));
					fwMapTypesDef* pParentDef = g_MapTypesStore.GetSlot(strLocalIndex(pDef->GetParentTypeIndex(j)));
					Assert(pParentDef);
					pParentDef->SetIsMLOType(true);		// for the time being - patch up all these .ityps so that they have to allocate stream memory
				}
			}
			
		}
	}

// #else //__DEV
// void fwMapTypesStore::VerifyItypFiles() {
// #endif //__DEV
}

#if !__FINAL || __FINAL_LOGGING
const char* fwMapTypesStore::GetName(strLocalIndex index) const
{
	if (index == fwMapTypes::GLOBAL)
	{
		return "<GLOBAL>";
	}

	return fwAssetStore<fwMapTypesContents, fwMapTypesDef>::GetName(index);
}
#endif   // !__FINAL


} // namespace rage
