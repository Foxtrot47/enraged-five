//
// fwscene/stores/boxstreamer.h : streams assets based on bounding boxes in world space
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef _FWSCENE_STORES_BOXSTREAMERNEW_H_
#define _FWSCENE_STORES_BOXSTREAMERNEW_H_

#include "atl/array.h"
#include "fwscene/stores/boxstreamerassets.h"
#include "fwscene/stores/boxstreamersearch.h"
#include "spatialdata/aabb.h"
#include "spatialdata/mnbvh.h"
#include "spatialdata/sphere.h"
#include "streaming/streamingmodule.h"
#include "vectormath/vec3v.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "vector/color32.h"
#endif	//__BANK

namespace rage
{
	typedef u16 fwBoxFlags;	// 6 bits for flags, 7 bits for asset type

	class fwBoxStreamerRequiredList
	{
	public:
		fwBoxStreamerRequiredList() : m_oldList(false)
		{
			GetOldList().Reset();
			GetNewList().Reset();
		}

		void Flush()				{ GetOldList().ResetCount(); }
		void Reset()				{ GetNewList().ResetCount(); }
		void Swap()					{ m_oldList = !m_oldList; }

		atArray<s32>& GetOldList()	{ return m_requiredLists[m_oldList]; }
		atArray<s32>& GetNewList()	{ return m_requiredLists[!m_oldList]; }
		
	private:
		atArray<s32> m_requiredLists[2];
		bool m_oldList;
	};

	//
	// PURPOSE
	//  Interface between box streamer and game code
	// NOTES:
	//  The implementation of GetGameSpecificSearches can append a number of
	//  fwBoxStreamerSearch objects which will be processed in turn. For example
	//  a script entity in the game could be flagged to stream in static collision
	//  around itself etc
	// SEE ALSO
	//  fwBoxStreamerSearch
	//
	struct fwBoxStreamerInterfaceNew
	{
		virtual ~fwBoxStreamerInterfaceNew() {}
		virtual bool IsExportingCollision() = 0;
		virtual void GetGameSpecificSearches(atArray<fwBoxStreamerSearch>& searchList, fwBoxStreamerAssetFlags supportedAssetFlags) = 0;
	};

	//
	// PURPOSE
	//	Helper class to manage streaming of assets based on one or multiple positions in the world.
	// NOTES
	//	Bounding boxes, pre-stream distances and asset type flags are set up for each asset and then these
	//  assets are streamed based on a list of active searches. Calling RequestAboutPos() builds up a list
	//  of required searches, flags anything required, and issue requests accordingly.
	//	flagged
	// SEE ALSO
	//	strStreamingModule
	//
	class fwBoxStreamer
	{
	public:
		fwBoxStreamer() :
			m_slotList(), m_pModule(NULL), m_fEnsureRadius(10.0f), m_fHasLoadedSearchRadius(8.0f),
			m_supportedAssetFlags(0), m_ensuredAssetFlags(0), m_requiredAssetFlags(0), m_overriddenRequiredAssetFlags(0), m_filterRequestAssetFlags(0),
			m_numEntries(0), m_bSearchSecondaryPos(false), m_pTree(NULL), m_bOverrideRequiredThisFrame(false) { }

		virtual ~fwBoxStreamer() { if (m_pTree) { delete m_pTree; } }

		virtual void Init(strStreamingModule* pModule, float fPriorityRadius);
		virtual void PostProcess();
		void Shutdown();
		void AddSupportedAsset(fwBoxStreamerAsset::eType assetType, float fStreamRadius, bool bEnsured, bool bPriorityWithinSphere, bool bAlwaysPriority);
		void InitSlot(s32 index, fwBoxStreamerAsset::eType assetType) { m_aBoxes[index].Invalidate(); m_aFlags[index]=(fwBoxFlags)assetType<<NUM_FLAGS; }
		void RemoveSlot(s32 index) { m_aBoxes[index].Invalidate(); m_aFlags[index]=0; }
		
		virtual void GetIntersectingAABB(const spdAABB& bbox, fwBoxStreamerAssetFlags assetFlags, atArray<u32>& slotList, bool bIncludePreStream=false);
		virtual void GetIntersectingLine(Vec3V_In vPos1, Vec3V_In vPos2, fwBoxStreamerAssetFlags assetFlags, atArray<u32>& slotList, bool bIncludePreStream=false);

		void RequestAboutPos(Vec3V_In vPrimaryPos, u32 extraStreamingFlags = 0);
		void RequestAboutPos(const fwBoxStreamerSearch& primarySearch, u32 extraStreamingFlags = 0);
		bool HasLoadedAboutPos(Vec3V_In vPos, fwBoxStreamerAssetFlags assetFlags);
		bool HasLoadedWithinAABB(const spdAABB& aabb, fwBoxStreamerAssetFlags assetFlags);
		void EnsureLoadedAboutPos(Vec3V_In vPos);
		void Deprecated_AddSecondarySearch(Vec3V_In vPos)	{ m_vSecondaryPos=vPos; m_bSearchSecondaryPos=true; }
		void ClearRequiredList();
		void CleanRequiredList();
		void AddToDirtyList(s32 slotIndex)					{ 
																if(m_dirtyList.Access(slotIndex) == NULL)
																{
																	m_dirtyList.Insert(slotIndex,1); 
																}
															}

		void SetIsIgnored(s32 index, bool bIgnored)			{ UpdateFlags(index, FLAG_IGNORE, bIgnored); }
		void SetIsCulled(s32 index, bool bCulled)			{ UpdateFlags(index, FLAG_CULL, bCulled); }
		void SetIsPinnedByVolume(s32 index, bool bPinned)	{ UpdateFlags(index, FLAG_PIN_VOL, bPinned); }
		void SetIsPinnedByUser(s32 index, bool bPinned)		{ UpdateFlags(index, FLAG_PIN_USR, bPinned); }

		bool GetIsRequiredCurr(s32 index) const				{ return (GetFlags(index)&FLAG_REQ_CURR)!= 0; }
		bool GetIsRequiredPrev(s32 index) const				{ return (GetFlags(index)&FLAG_REQ_PREV)!= 0; }
		bool GetIsIgnored(s32 index) const					{ return (GetFlags(index)&FLAG_IGNORE)	!= 0; }
		bool GetIsCulled(s32 index) const					{ return (GetFlags(index)&FLAG_CULL)	!= 0; }
		bool GetIsPinnedByVolume(s32 index) const			{ return (GetFlags(index)&FLAG_PIN_VOL)	!= 0; }
		bool GetIsPinnedByUser(s32 index) const				{ return (GetFlags(index)&FLAG_PIN_USR)	!= 0; }
		bool GetIsPinned(s32 index) const					{ return (GetFlags(index)&PINNED_MASK)	!= 0; }

		void SetAssetType(s32 index, u16 assetType)			{ m_aFlags[index] = GetFlags(index) | ((fwBoxFlags)assetType<<NUM_FLAGS); }
		u8 GetAssetType(s32 index) const					{ return (u8)(m_aFlags[index] >> NUM_FLAGS); }
		bool CanRemoveAsset(s32 index)						{ return CanRemove(GetFlags(index)); }
		void SetNotRequired(s32 index)						{ UpdateFlags(index, FLAG_REQ_CURR, false); }
		bool IsLocked() const								{ return m_bEntriesLocked; }
		void SetLocked(bool value) 							{ m_bEntriesLocked = value; }
		spdAABB& GetBounds(s32 index)						{ return m_aBoxes[index]; }
		const spdAABB& GetBoundsConst(s32 index) const		{ return m_aBoxes[index]; }	//yuck

		// override what asset types box streamer requests by default
		void SetRequiredAssetFlags(fwBoxStreamerAssetFlags assetFlags) { m_requiredAssetFlags = assetFlags; }
		void OverrideRequiredAssets(fwBoxStreamerAssetFlags assetFlags) { m_overriddenRequiredAssetFlags=assetFlags; m_bOverrideRequiredThisFrame=true; }

		// scaling of stream range
		float GetStreamRadiusScale(fwBoxStreamerAsset::eType assetType) const { return m_afSearchRadiusScaleByType[assetType]; }
		void OverrideStreamRadiusScale(fwBoxStreamerAsset::eType assetType, float fScale) { m_afSearchRadiusScaleByType[assetType] = fScale; }
		void ResetStreamRadiusScale(fwBoxStreamerAsset::eType assetType) { m_afSearchRadiusScaleByType[assetType] = 1.0f; }

		void SetAssetFilter(fwBoxStreamerAssetFlags assetFlags) { m_filterRequestAssetFlags = assetFlags; }

		static void SetInterface(fwBoxStreamerInterfaceNew* pInterface) { ms_pGameInterface = pInterface; }

#if __BANK
		static void SetBlockingPermitted(bool blockingPermitted)			{ sm_BlockingPermitted = blockingPermitted; }
#endif // __BANK
	
#if !__FINAL
		const atArray<fwBoxStreamerSearch>& GetSearches() const		{ return m_searches; }
#endif

	protected:
		u32 GetNumEntries() const { return m_numEntries; }
		float GetPreStream(s32 index) { return m_afSearchRadiusByType[GetAssetType(index)]; }

		void GenerateSearchList(const fwBoxStreamerSearch& primarySearch, atArray<fwBoxStreamerSearch>& searchList);
		void RequestRequired(Vec3V_In vPriorityPos, u32 extraStreamingFlags = 0);
	
		void ClearFlags(s32 index, fwBoxFlags flags)				{ m_aFlags[index] &= ~flags; }
		void SetFlags(s32 index, fwBoxFlags flags)					{ m_aFlags[index] |= flags; }
		fwBoxFlags GetFlags(s32 index) const						{ return m_aFlags[index] & MASK_FLAGS; }
		void UpdateFlags(s32 index, fwBoxFlags flags, bool bSet)	{ if (bSet) { SetFlags(index, flags); } else { ClearFlags(index, flags); } }
		bool CanRemove(fwBoxFlags flags) const						{ return !(flags&DO_NOT_REMOVE_MASK); }
		bool CanStream(fwBoxFlags flags) const						{ return !(flags&FLAG_IGNORE) && ( !(flags&FLAG_CULL) || (flags&PINNED_MASK) ); }
		fwBoxStreamerAssetFlags GetAssetFlags(s32 index) const		{ return 1 << GetAssetType(index); }

		bool IsPriorityAsset(s32 index, const spdAABB& priorityAABB)
		{
			if (m_alwaysPriorityFlags && (m_alwaysPriorityFlags & GetAssetFlags(index))!=0)
			{
				return true;
			}
			if (m_priorityAssetFlags && (m_priorityAssetFlags & GetAssetFlags(index))!=0)
			{
				return priorityAABB.IntersectsAABB(GetBounds(index));
			}
			return false;
		}

		void SetRequired(fwBoxStreamerSearch& searchVolume);
#if !__FINAL
		void SetRequired2D(fwBoxStreamerSearch& searchVolume);
#endif	//!__FINAL


		enum eFlags
		{
			FLAG_REQ_CURR			= BIT(0),
			FLAG_REQ_PREV			= BIT(1),
			FLAG_IGNORE				= BIT(2),		// manually loaded & unloaded only
			FLAG_CULL				= BIT(3),		// culled by cull box
			FLAG_PIN_VOL			= BIT(4),		// required by streaming volume, cannot be culled
			FLAG_PIN_USR			= BIT(5)		// required by user, cannot be culled
		};

		enum
		{
			DO_NOT_REMOVE_MASK		= ( FLAG_IGNORE | FLAG_PIN_USR | FLAG_PIN_VOL ),
			PINNED_MASK				= ( FLAG_PIN_VOL | FLAG_PIN_USR ),
			REQ_MASK				= ( FLAG_REQ_CURR | FLAG_REQ_PREV),
			NUM_FLAGS				= 6,
			MASK_FLAGS				= (1 << NUM_FLAGS) - 1,
			HASLOADED_RADIUS		= 500,
			MAX_SEARCH_RESULTS		= 1000
		};

		atArray<u32>					m_slotList;
		strStreamingModule*				m_pModule;			// corresponding streaming module, typically an asset store
		atArray<spdAABB>				m_aBoxes;			// bounding boxes
		atArray<fwBoxFlags>				m_aFlags;
		u32								m_numEntries;

		atArray<fwBoxStreamerSearch>	m_searches;	// list of currently active searches
		fwBoxStreamerAssetFlags			m_supportedAssetFlags;
		fwBoxStreamerAssetFlags			m_ensuredAssetFlags;
		fwBoxStreamerAssetFlags			m_requiredAssetFlags;
		fwBoxStreamerAssetFlags			m_overriddenRequiredAssetFlags;
		fwBoxStreamerAssetFlags			m_priorityAssetFlags;
		fwBoxStreamerAssetFlags			m_alwaysPriorityFlags;
		fwBoxStreamerAssetFlags			m_filterRequestAssetFlags;
		bool							m_bEntriesLocked;

		const float						m_fEnsureRadius;
		const float						m_fHasLoadedSearchRadius;
		float							m_fPriorityRadius;

		float							m_afSearchRadiusByType[fwBoxStreamerAsset::ASSET_TOTAL];
		float							m_afSearchRadiusScaleByType[fwBoxStreamerAsset::ASSET_TOTAL];

		bool							m_bOverrideRequiredThisFrame;
		bool							m_bSearchSecondaryPos;
		Vec3V							m_vSecondaryPos;

		fwBoxStreamerRequiredList		m_requiredList;
		atMap<s32, u32>					m_dirtyList;

		MNBVHSimpleInterface*			m_pTree;
		static void* ms_tempResults[MAX_SEARCH_RESULTS];

		// used to get list of required game-side searches (script entities, streaming volumes etc)
		static fwBoxStreamerInterfaceNew* ms_pGameInterface;

		//////////////////////////////////////////////////////////////////////////
#if __BANK
	public:
		void InitWidgets(bkBank& bank, const char* pName);
		void GetUnloadedAboutPos(Vec3V_In posn, atArray<u32>& aIndices);
		void UpdateDebug();
		void RenderDebug(Color32* colours);
		void Print(int index, char* text, size_t maxSize) const;
		virtual void RebuildTrees();
		void GetCosts(u32& main, u32& vram);
		fwBoxStreamerRequiredList& GetRequiredList() { return m_requiredList; }

		static const char* apszBoxStreamerAssetTypes[fwBoxStreamerAsset::ASSET_TOTAL];

	private:
		const spdAABB GetBoundsCopy(s32 index) { return m_aBoxes[index]; }
		virtual const spdAABB GetStreamingBoundsCopy(s32 index);

 		bool m_bDisplaySearchList;
 		bool m_bDebugDisplayInMemory;
 		bool m_bDisplayLoadedList;
		bool m_bDisplayHasLoadedForSelected;
 		bool m_bDebugDisplayIncludesPreStream;
 		bool m_bDebugDisplayIntersectingButUnloaded;
		bool m_bDumpInfoOnBlockload;
		bool m_abDebugSearchTypes[fwBoxStreamerAsset::ASSET_TOTAL];
		bool m_bDebugOverrideStreamRangeScale;
		float m_afDebugStreamScale[fwBoxStreamerAsset::ASSET_TOTAL];

#endif	//__BANK


#if __BANK
		static bool sm_BlockingPermitted;
#endif // __BANK

		//////////////////////////////////////////////////////////////////////////
	};

	//
	// PURPOSE
	//	Helper class to manage streaming of assets based on one or multiple positions in the world.
	// NOTES
	//	Extends the basic box streamer to include storage and a tree structure for tailored streaming bounds
	//  specific to each slot. This comes at a memory cost but can be a more optimal solution for certain types
	//  of assets.
	// SEE ALSO
	//	fwBoxStreamer
	//
	class fwBoxStreamerVariable : public fwBoxStreamer
	{
	public:
		virtual ~fwBoxStreamerVariable() { if (m_pInflatedTree) { delete m_pInflatedTree; } }

		virtual void Init(strStreamingModule* pModule, float fPriorityRadius);
		virtual void InitSlot(s32 index, fwBoxStreamerAsset::eType assetType);
		virtual void PostProcess();
		virtual void PostProcessPending();
		virtual void GetIntersectingAABB(const spdAABB& bbox, fwBoxStreamerAssetFlags assetFlags, atArray<u32>& slotList, bool bIncludePreStream=false);
		virtual void GetIntersectingLine(Vec3V_In vPos1, Vec3V_In vPos2, fwBoxStreamerAssetFlags assetFlags, atArray<u32>& slotList, bool bIncludePreStream=false);
		
		spdAABB& GetStreamingBounds(s32 index)						{ return m_aInflatedBoxes[index]; }
		const spdAABB& GetStreamingBoundsConst(s32 index) const		{ return m_aInflatedBoxes[index]; }
		bool IsFullyInitialised() const								{ return m_bFullyInitialised; }

	private:
		atArray<spdAABB> m_aInflatedBoxes;
		atArray<s32> m_pendingPostProcess;
		MNBVHSimpleInterface* m_pInflatedTree;
		bool m_bFullyInitialised;

		//////////////////////////////////////////////////////////////////////////
#if __BANK
	public:
		virtual void RebuildTrees();
	private:
		virtual const spdAABB GetStreamingBoundsCopy(s32 index) { return m_aInflatedBoxes[index]; }
#endif
		//////////////////////////////////////////////////////////////////////////
	};
	
}	//namespace rage

#endif	//_FWSCENE_STORES_BOXSTREAMERNEW_H_
