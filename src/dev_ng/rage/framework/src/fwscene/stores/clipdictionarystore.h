
#ifndef FWSCENE_STORES_CLIPDICTIONARY_H
#define FWSCENE_STORES_CLIPDICTIONARY_H

// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
// Game headers
#include "fwtl\assetstore.h"
#include "crclip\clipdictionary.h"


namespace rage {

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Rage forward declaration

//
// name:		CClipDictionaryDef
// description:	A crClipDictionary + flags + functions
//
class fwClipDictionaryDef : public fwAssetDef<crClipDictionary>
{
public:
	void Init(const strStreamingObjectName name) 
	{
		fwAssetDef<crClipDictionary>::Init(name); 
	}

	const crClip* GetClip(strLocalIndex clipIndex);
	const crClip* GetClipIfExists(strLocalIndex clipIndex);
	const crClip* FindClip(s32 hashKey);
	const crClip* FindClipIfExists(s32 hashKey);
};

#define DICTIONARY_IN_USE_CHECK 0	// Thoroughly check a dictionary isn't in use when it's removed.
#define USE_DICTIONARY_REF_COUNTS 1 // Rather than calculate the references by iterating through the store, just check the dictionary ref count with is being tweaks by clips when they're AddRef/Released.

#define MAP_DICTIONARY_REFERENCES (DICTIONARY_IN_USE_CHECK || !USE_DICTIONARY_REF_COUNTS)

class fwClipDictionaryStoreGameInterface
{
public:
	virtual ~fwClipDictionaryStoreGameInterface() {}
	virtual void OnLoad(strLocalIndex UNUSED_PARAM(iIndex)) {}
	virtual void OnRemove(strLocalIndex UNUSED_PARAM(iIndex)) {}
	virtual void OnAddRef(strLocalIndex UNUSED_PARAM(iIndex)) {}
	virtual void OnRemoveRef(strLocalIndex UNUSED_PARAM(iIndex)) {}
	virtual void OnRemoveRefWithoutDelete(strLocalIndex UNUSED_PARAM(iIndex)) {}
	virtual void OnResetAllRefs(strLocalIndex UNUSED_PARAM(iIndex)) {}
};

//
// name:		CClipDictionaryStore
// description:	
class fwClipDictionaryStore : public fwAssetRscStore<crClipDictionary, fwClipDictionaryDef>
{
public:
	fwClipDictionaryStore();
	virtual ~fwClipDictionaryStore();

	virtual void Set(strLocalIndex index, crClipDictionary* pObject);
	virtual void Remove(strLocalIndex index);
	virtual bool Load(strLocalIndex index, void* pData, int size);
	virtual void AddRef(strLocalIndex index, strRefKind strRefKind);
	virtual void RemoveRef(strLocalIndex index, strRefKind strRefKind);
	virtual void RemoveRefWithoutDelete(strLocalIndex index, strRefKind strRefKind);
	virtual void ResetAllRefs(strLocalIndex index);

	virtual int GetNumRefs(strLocalIndex index) const;

	virtual bool CanPlaceAsynchronously(strLocalIndex index) const;

public:
#if MAP_DICTIONARY_REFERENCES
	typedef atMapMemoryPool<const crAnimation*, int, 4> MemoryPool;
	typedef atMap<const crAnimation*, int, atMapHashFn<const crAnimation*>, atMapEquals<const crAnimation*>, MemoryPool::MapFunctor > Map;
	typedef Map::Entry MapEntry;

	//Setup map and pool with relation use the functor object to tie them together.
	MemoryPool m_Pool;
	mutable Map m_Map;
#endif

	void SetGameInterface(fwClipDictionaryStoreGameInterface* pGameInterface)
	{
		Assert(pGameInterface);
		m_pGameInterface = pGameInterface;
	}

	void ResetGameInterface()
	{
		static fwClipDictionaryStoreGameInterface s_DefaultGameInterface;
		SetGameInterface(&s_DefaultGameInterface);
	}

private:

	fwClipDictionaryStoreGameInterface*	m_pGameInterface;

};

#if __ASSERT
extern void ValidateClipDictionary(const crClipDictionary& clipDict, const char* clipDictName);
#endif //  __ASSERT

// --- Globals ------------------------------------------------------------------

extern fwClipDictionaryStore g_ClipDictionaryStore;
#if __BANK
extern atMap<int, atString>  g_ClipDictionarySource;
#endif


} // namespace rage


#endif // FWSCENE_STORES_CLIPDICTIONARY_H
