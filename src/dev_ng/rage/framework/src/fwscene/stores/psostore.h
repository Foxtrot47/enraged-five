// 
// fwscene/stores/psostore.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCENE_PSOSTORE_H
#define SCENE_PSOSTORE_H

#include "atl/map.h"
#include "parser/psofile.h"
#include "streaming/streamingdefs.h"
#include "streaming/streaminginfo.h"
#include "system/criticalsection.h"

namespace rage
{

	// Need to cover 4 use cases here:

	// 1 - PSO is only needed temporarily - data is extracted from it, discarded at the end of the Load function
	//		a - In-place load
	//		b - Not-in-place load (what memory? gamevirtual? debug?)
	// 2 - PSO data needs to persist
	//		a - In-place load (always in resource memory)
	//		b - Not-in-place load (what memory? gamevirtual? debug?)

	// Optional features(?)
	// Run PostLoad callbacks
	// Load from XML?

	// Every asset store 'data' class needs to contain an instance of fwPsoStoreLoadResult. It serves two purposes.
	// 1) You can get a pointer to the instance data that you loaded
	// 2) It acts as a 'cookie' that you pass to the Unload function, so we know what to free later.
	class fwPsoStoreLoadInstance
	{
		friend class fwPsoStoreLoader;
	public:
		fwPsoStoreLoadInstance() : m_Instance(NULL), m_Storage(NULL), m_LoadState(NOT_LOADED), m_LoadedFromResource(false) {}
		explicit fwPsoStoreLoadInstance(parPtrToStructure inst) : m_Instance(inst), m_Storage(NULL), m_LoadState(NOT_LOADED), m_LoadedFromResource(false) {}

		template<typename _Type>
		explicit fwPsoStoreLoadInstance(_Type* inst) : m_Instance(inst->parser_GetPointer()), m_Storage(NULL), m_LoadState(NOT_LOADED), m_LoadedFromResource(false) {}


		parPtrToStructure GetInstance() const { return m_Instance; }
		void* GetStorage() const { return m_Storage; }

		bool IsLoaded() const { return m_LoadState != NOT_LOADED; }
		bool IsInPlace() const { return m_LoadState == LOADED_IN_PLACE_ALLOC || m_LoadState == LOADED_IN_PLACE_NOALLOC;}
		bool OwnsStorage() const { return m_LoadState == LOADED_AS_XML || m_LoadState == LOADED_IN_PLACE_ALLOC || m_LoadState == LOADED_NOT_IN_PLACE; }
		bool IsPso() const { return m_LoadState != NOT_LOADED && m_LoadState != LOADED_AS_XML; }
	private:
		enum LoadState
		{ NOT_LOADED, LOADED_AS_XML, LOADED_IN_PLACE_ALLOC, LOADED_IN_PLACE_NOALLOC, LOADED_NOT_IN_PLACE };

		parPtrToStructure m_Instance;
		void* m_Storage;
		u8 m_LoadState;
		bool m_LoadedFromResource;
	};

	// PURPOSE:
	// used to transfer a fwPsoStoreLoadInstance across threads when using the module PlaceResource, SetResource, etc functions
	// NOTES:
	// Individual modules need to do their own housekeeping and remove cookies when they no longer need them
	class fwPsoStorePlacementCookies 
	{
	public:
		struct Cookie
		{
			Cookie() : m_OtherData(NULL) {}
			fwPsoStoreLoadInstance	m_Instance;
			void*					m_OtherData;
		};

		void Add(strIndex index, const Cookie& cookie);
		Cookie Get(strIndex index);
		void Set(strIndex index, const Cookie& cookie);
		void Remove(strIndex index);

	private:
		atMap<strIndex, Cookie> m_Map;
		sysCriticalSectionToken m_CS;
	};

	extern fwPsoStorePlacementCookies g_PsoPlacementCookies;

	// This class provides additional information about the load that just happened, you don't have to keep it around though.
	struct fwPsoStoreLoadResult
	{
		fwPsoStoreLoadResult() : m_Structure(NULL) {}
		parStructure* m_Structure;
	};

	// Each asset store that loads PSO files should have an instance of this class. Each store can then 
	// set custom settings for the loader.
	class fwPsoStoreLoader
	{
	public:
		fwPsoStoreLoader();

		template<typename Type> static fwPsoStoreLoader MakeSimpleFileLoader();

		// Convenience function - does a blocking not-in-place load to populate an instance of 'type'. Uses the same settings
		// MakeSimpleFileLoader uses.
		// I.e. on disc builds we search for an adjacent #mt file before loading from the specified filename
		template<typename Type> static bool LoadDataIntoObject(const char* filename, const char* ext, Type& type);

		// Configuration options for the Loader
		template<typename Type> void SetRootType() { m_RootType = Type::parser_GetStaticStructure(); }
		void SetRootType(parStructure* str) { m_RootType = str; }

		enum Flags {
			RUN_POSTLOAD_CALLBACKS,		// After loading, when loading from PSO, still call the onPostLoad callbacks like PARSER.LoadObject does?
			LOAD_XML_FILES,				// Can we load XML files with this loader?
			LOAD_IN_PLACE,				// Can we load files in-place in one chunk, or do we need to make separate allocations for each object we load in
			WARN_ON_LAYOUT_MISMATCH,	// Warn or...
			ASSERT_ON_LAYOUT_MISMATCH,	// Assert when the object layout for any object in the file isn't isomorphic to the runtime object
			ALWAYS_RUN_DESTRUCTORS,		// When unloading, should we run destructors on the data first? Or only when we ran constructors?
			ALLOW_PATCH_XMLS,			// Allows you to pass in an existing instance and "patch" it based on the contents of another file (2nd file must be XML/RBF format)
			ALLOW_PATCH_PSOS,			// Ditto, but with PSOs (note that PSOs are a lot more limited in what you can provide for patch data)
		};

		atFixedBitSet32 GetFlags() { return m_Flags; }
		void SetFlags(atFixedBitSet32 flags) { m_Flags = flags; }
		atFixedBitSet32& GetFlagsRef() { return m_Flags; }

		void SetNotInPlaceHeap(psoHeap heap);
		void SetInPlaceHeap(psoHeap heap);
		
		fwPsoStoreLoadResult Load(void* pData, int size, const char* debugName, fwPsoStoreLoadInstance& fwInOutPsoData);
		fwPsoStoreLoadResult Load(const char* filename, fwPsoStoreLoadInstance& fwInOutPsoData);
		fwPsoStoreLoadResult LoadResource(psoResourceData* rsc, const char* debugName, fwPsoStoreLoadInstance& fwInOutPsoData);

		static void RunPostloadCallbacks(parStructure* topLevelStructure, fwPsoStoreLoadInstance& fwInOutPsoData);

		void Unload(fwPsoStoreLoadInstance& inOutPsoData, bool deleteUnownedData = false);
		void Unload(fwPsoStoreLoadInstance& inOutPsoData, parStructure& str, bool deleteUnownedData = false); // You MUST call this version, if you didn't specify a root type

		void PrintLoadStats();

		// PURPOSE: During DESTRUCTOR calls, you may need to know whether the thing being destructed was inplace loaded or not
		static fwPsoStoreLoadInstance* GetTopLevelInstance() { return sm_CurrentTopLevelInst; }

	protected:
		fwPsoStoreLoadResult LoadXmlFile( fiStream* stream, fwPsoStoreLoadInstance &inOutPsoData );
		fwPsoStoreLoadResult LoadPsoFile( char* pData, int size, const char* debugName, fwPsoStoreLoadInstance &inOutPsoData );

		fwPsoStoreLoadResult ReadObjectsFromPsoFile( psoFile& pso, fwPsoStoreLoadInstance &inOutPsoData);

		static fwPsoStoreLoader MakeSimpleFileLoaderCore();
		
		bool LoadDataIntoObjectCore(const char* filename, const char* ext, fwPsoStoreLoadInstance& inst);

		parStructure* m_RootType;
		atFixedBitSet32 m_Flags;
		psoHeap m_InPlaceHeap;
		psoHeap m_NotInPlaceHeap;
		static fwPsoStoreLoadInstance* sm_CurrentTopLevelInst;
	};

	inline void rage::fwPsoStoreLoader::SetInPlaceHeap( psoHeap heap )
	{
		m_InPlaceHeap = heap;
	}

	inline void rage::fwPsoStoreLoader::SetNotInPlaceHeap( psoHeap heap )
	{
		m_NotInPlaceHeap = heap;
	}

	template<typename Type>
	fwPsoStoreLoader fwPsoStoreLoader::MakeSimpleFileLoader()
	{
		fwPsoStoreLoader loader = MakeSimpleFileLoaderCore();
		loader.SetRootType(Type::parser_GetStaticStructure());
		return loader;
	}

	template<typename Type>
	bool fwPsoStoreLoader::LoadDataIntoObject(const char* filename, const char* ext, Type& type)
	{
		fwPsoStoreLoader loader = MakeSimpleFileLoader<Type>();
		fwPsoStoreLoadInstance inst(type.parser_GetPointer());
		return loader.LoadDataIntoObjectCore(filename, ext, inst);
	}

}

#endif
