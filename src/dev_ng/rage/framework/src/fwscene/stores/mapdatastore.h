//
// fwscene/stores/mapdatastore.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef __FWSCENE_MAPDATASTORE_H__
#define __FWSCENE_MAPDATASTORE_H__

#include "atl/array.h"
#include "atl/map.h"
#include "fwtl/assetstore.h"
#include "fwscene/stores/boxstreamer.h"
#include "fwscene/stores/imapgroup.h"
#include "fwscene/stores/psostore.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/mapdata/mapdatacontents.h"
#include "fwscene/mapdata/mapdatadef.h"

namespace rage 
{
	struct fwMapDataGameInterface;

	struct SScriptImapState
	{
		strLocalIndex m_slotIndex;
		s32 m_flags;
		bool m_isIgnored : 1;
		bool m_proxyDisabled : 1;

		bool operator == (const SScriptImapState& rhs) const { return rhs.m_slotIndex == m_slotIndex; }
	};

	// PURPOSE:	The store for the map data. 
	class fwMapDataStore : public fwAssetStore<fwMapDataContents, fwMapDataDef>
	{	
	public:
		fwMapDataStore();

		void Init(u32 initMode);
		void ShutdownLevelWithMapLoaded();
		void ShutdownLevelWithMapUnloaded();
		void FixupAllEntriesAfterRegistration(bool bResetGroupState);
		void Update();
		void RemoveUnrequired(bool bAll);

		void SaveScriptImapStates();
		void RestoreScriptImapStates();

		virtual bool ModifyHierarchyStatus(strLocalIndex index, eHierarchyModType modType);

		// streaming system interface
		virtual void RegisterStreamingModule();
		virtual bool Load(strLocalIndex index, void* pData, int size);

		virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);
		virtual void SetResource(strLocalIndex index, datResourceMap& map);

		virtual bool CanPlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex)) const { return true; }
		virtual void PlaceAsynchronously(strLocalIndex objIndex, strStreamingLoader::StreamingFile& file, datResourceInfo& rsc);

		void FinishLoading(strLocalIndex mapDataDefIndex, fwPsoStoreLoadInstance& loadInst);

		virtual strLocalIndex AddSlot(const strStreamingObjectNameString name);
		strLocalIndex AddSlot(strLocalIndex index, const strStreamingObjectNameString name);
		virtual strLocalIndex Register(const char* name);
		virtual int GetDependencies(strLocalIndex index, strIndex *pIndices, int indexArraySize) const;
		virtual void Remove(strLocalIndex index);
		virtual bool RequiresTempMemory(strLocalIndex index) const;

		virtual void* GetDataPtr(strLocalIndex index);

		void SafeRemoveAll(bool bHdMapEntitiesOnly=false);
		void SafeRemove(strLocalIndex index);

		bool IsScriptManaged(strLocalIndex slotIndex);
		void SetStreamable(strLocalIndex slotIndex, bool bEnabled);
		bool GetIsStreamable(strLocalIndex slotIndex)					{ return !(m_boxStreamer.GetIsIgnored(slotIndex.Get())); }

		fwBoxStreamerVariable& GetBoxStreamer()				{ return m_boxStreamer; }
		fwImapGroupMgr& GetGroupSwapper()					{ return m_swapper; }
		strLocalIndex GetParentSlot(strLocalIndex index) const;
		fwEntity* GetEntity(strLocalIndex slotIndex, strLocalIndex entityIndex);
		void RequestGroup(strLocalIndex index, const u32 nameHash, bool bIssueRequests=false);
		void RemoveGroup(strLocalIndex index, const u32 nameHash);

		void GetEnabledIplGroups(atArray<u32>& enabledList, bool ignoreCodeManagedCodeCheck = false);
		void SetEnabledIplGroups(const atArray<u32>& enabledList, bool ignoreCodeManagedCodeCheck = false);

		inline static fwMapDataStore& GetStore() { return(ms_mapDataStore); }

		// used by cull box loading code
		void GetChildren(strLocalIndex slotIndex, atArray<s32>& results);
		void CreateChildrenCache();
		void DestroyChildrenCache() { if (ms_pChildrenMap) { delete ms_pChildrenMap; ms_pChildrenMap=NULL; } }

		static fwMapDataGameInterface* GetInterface() { return ms_pGameInterface; }
		static void SetInterface(fwMapDataGameInterface* pInterface) { ms_pGameInterface = pInterface; }

		bool IsRemovingAllMapData(void) { return(m_bIsRemovingAllMapData); }

		void SetHdStreaming(bool bAllowHdStreaming);

		const spdAABB& GetPhysicalBounds(strLocalIndex slotIndex) { return m_boxStreamer.GetBoundsConst(slotIndex.Get()); }
		const spdAABB& GetStreamingBounds(strLocalIndex slotIndex) { return m_boxStreamer.GetStreamingBoundsConst(slotIndex.Get()); }

		void SetGetProxyDisabledCB(atDelegate<bool (strLocalIndex)>* value) { m_getProxyDisabledCB = value; }
		void SetSetProxyDisabledCB(atDelegate<void (strLocalIndex, bool)>* value) { m_setProxyDisabledCB = value; }

#if !__FINAL
		virtual void PrintExtraInfo(strLocalIndex index, char* extraInfo, size_t maxSize) const;
#endif	//!__FINAL

		bool PerformStreamingIntegrityCheck();

	private:
		void PreLoad(strLocalIndex index, fwMapDataDef* pDef, fwMapData* pMapData, fwMapDataContents* pContents);
		void CacheParentChildLink(strLocalIndex parentIndex, strLocalIndex childIndex);
		void SetBoxStreamerSlot(strLocalIndex index, fwMapDataDef* pDef);
		void ProcessPostLaunchHacks();
		void AddToLoadedList(strLocalIndex index, fwMapDataDef* pDef);
		void RemoveFromLoadedList(strLocalIndex index, fwMapDataDef* pDef);
		void ModifyHierarchyStatusRecursive(strLocalIndex slotIndex, eHierarchyModType modType);
		
		static s32 CompareDepthCB(fwMapDataLoadedNode * const * ppNode1, fwMapDataLoadedNode * const * ppNode2);

		bool								m_bIsRemovingAllMapData;
		atArray<fwMapDataLoadedNode*>		m_loadedList;
		fwBoxStreamerVariable				m_boxStreamer;
		fwImapGroupMgr						m_swapper;
		fwPsoStoreLoader					m_loader;
		atArray<SScriptImapState> m_scriptImapSnapShot;
		static atDelegate<bool (strLocalIndex)>* m_getProxyDisabledCB;
		static atDelegate<void (strLocalIndex, bool)>* m_setProxyDisabledCB;
		static fwMapDataStore				ms_mapDataStore;
		static fwMapDataGameInterface*		ms_pGameInterface;
		static atMap< s32, atArray<s32> >*	ms_pChildrenMap;

		//////////////////////////////////////////////////////////////////////////
		// CACHELOADER related
	public:
		void InitCacheLoaderModule();
		bool ReadFromCacheFile(const void* const pLine, fiStream* pDebugTextFileToWriteTo);
		void AddToCacheFile(fiStream* pDebugTextFileToWriteTo);


#if __BANK
		friend class fwMapDataDebug;
#endif	//__BANK
	};

	struct fwMapDataGameInterface
	{
		virtual void CreateBlockInfoFromCache(fwMapDataDef* pDef, fwMapDataCacheBlockInfo* pBlockInfo) = 0;
		virtual void CreateCacheFromBlockInfo(fwMapDataCacheBlockInfo& output, s32 blockIndex) = 0;
		virtual ~fwMapDataGameInterface() {}
	};

	#define INSTANCE_DEF	fwMapDataDef
	#define INSTANCE_STORE	(fwMapDataStore::GetStore())

	// wrapper class needed to interface with game skeleton code
	class CInstanceStoreWrapper
	{
	public:
		static void Init(unsigned initMode)							{ fwMapDataStore::GetStore().Init(initMode); }
		static void Shutdown1(unsigned UNUSED_PARAM(shutdownMode))	{ fwMapDataStore::GetStore().ShutdownLevelWithMapLoaded(); }
		static void Shutdown2(unsigned UNUSED_PARAM(shutdownMode))	{ fwMapDataStore::GetStore().ShutdownLevelWithMapUnloaded(); }
	};

} // namespace rage

#define g_MapDataStore fwMapDataStore::GetStore()

#endif // __FWSCENE_MAPDATASTORE_H__
