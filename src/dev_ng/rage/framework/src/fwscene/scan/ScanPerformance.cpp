#if !__SPU

#include "fwscene/scan/ScanPerformance.h"

#if ENABLE_SCAN_TIMERS
namespace rage {
const float				fwScanTimer::ms_avgFactor = 0.01f;
}
#endif

rage::fwScanTimer		g_scanNodesTimer( "visnode", "ScanNodes" );
rage::fwScanTimer		g_scanEntitiesTimer( "visent", "ScanEntities" );
rage::fwScanTimer		g_scanEntitiesEarlyRejectTimer( "visreject", "Early reject" );
rage::fwScanTimer		g_scanEntitiesClippingTimer( "visclip", "Clipping" );
rage::fwScanTimer		g_scanEntitiesOcclusionTimer( "visoccl", "Occlusion" );

rage::fwScanTimer		g_searchNodesTimer( "searchnodes", "Search Nodes" );
rage::fwScanTimer		g_searchEntitiesSoaIntersectTimer( "soavol", "Search SoA Intersect" );
rage::fwScanTimer		g_searchEntitiesSoaIterateTimer( "soaiter", "Search SoA Iterate" );
rage::fwScanTimer		g_searchEntitiesNonSoaTimer( "nosoa", "Search Non-SoA codepath" );
rage::fwScanTimer		g_searchCallbacksTimer( "clbs", "Search callbacks" );

#endif
