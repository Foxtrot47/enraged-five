//
#include "vectormath/legacyconvert.h"
#include "vector/color32.h"
#if !__SPU
#include "grcore/debugdraw.h"
#include "vector/colors.h"
#endif // !__SPU
#include "spatialdata/transposedplaneset.h"

#include "fwscene/scan/ScreenQuad.h"
#include "fwmaths/vectorutil.h"

namespace rage {

fwScreenQuad::fwScreenQuad(const fwPortalCorners& portalCorners, Mat44V_In compositeMatrix)
{
	Vec3V	portalVertices[4];
	for (int i = 0; i < 4; ++i)
		portalVertices[i] = VECTOR3_TO_VEC3V( portalCorners.GetCorner(i) );

	Vec3V	clippedVertices[4 + 5];
	int		clippedVerticesCount;
	clippedVerticesCount = PolyClipToFrustumWithoutNearPlane( clippedVertices, portalVertices, 4, compositeMatrix );

	*this = fwScreenQuad(SCREEN_QUAD_INVALID);
	Vec3V minTransformedVertex(V_FLT_MAX);
	for (int i = 0; i < clippedVerticesCount; ++i)
	{
		Vec3V out =  TransformProjective( compositeMatrix, clippedVertices[i] );
		minTransformedVertex = Min( out, minTransformedVertex );
		Grow( out.GetXY() );
	}

	// if any clipped vertex is extremely close to the near plane we can't build a valid screen quad so set it to be fullscreen
	if (Unlikely(IsLessThanAll(minTransformedVertex.GetZ(), ScalarV(V_FLT_SMALL_2))))
	{
		*this = fwScreenQuad(SCREEN_QUAD_FULLSCREEN);
		return;
	}

	Clamp();
}

void fwScreenQuad::GenerateFrustum(Mat44V_In compositeMatrix, spdTransposedPlaneSet8& frustum, bool bFlipPlanes) const
{
	fwScreenQuad	clampedQuad = *this;
	clampedQuad.Clamp();

	Mat44V			invTransform;
	InvertFull( invTransform, compositeMatrix );

	const ScalarV	nearV( 0.01f );
	const ScalarV	farV(V_ONE);
	const ScalarV	oneV(V_ONE);

	Vec4V			nearPoints[4] =
	{
		Vec4V( clampedQuad.Left(), clampedQuad.Bottom(), nearV, oneV ),
		Vec4V( clampedQuad.Left(), clampedQuad.Top(), nearV, oneV ),
		Vec4V( clampedQuad.Right(), clampedQuad.Top(), nearV, oneV ),
		Vec4V( clampedQuad.Right(), clampedQuad.Bottom(), nearV, oneV )
	};

	Vec4V			farPoints[4] =
	{
		Vec4V( clampedQuad.Left(), clampedQuad.Bottom(), farV, oneV ),
		Vec4V( clampedQuad.Left(), clampedQuad.Top(), farV, oneV ),
		Vec4V( clampedQuad.Right(), clampedQuad.Top(), farV, oneV ),
		Vec4V( clampedQuad.Right(), clampedQuad.Bottom(), farV, oneV )
	};

	nearPoints[0] = Multiply( invTransform, nearPoints[0] );
	nearPoints[1] = Multiply( invTransform, nearPoints[1] );
	nearPoints[2] = Multiply( invTransform, nearPoints[2] );
	nearPoints[3] = Multiply( invTransform, nearPoints[3] );

	farPoints[0] = Multiply( invTransform, farPoints[0] );
	farPoints[1] = Multiply( invTransform, farPoints[1] );
	farPoints[2] = Multiply( invTransform, farPoints[2] );
	farPoints[3] = Multiply( invTransform, farPoints[3] );

	nearPoints[0] = nearPoints[0] * Invert( nearPoints[0] ).GetW();
	nearPoints[1] = nearPoints[1] * Invert( nearPoints[1] ).GetW();
	nearPoints[2] = nearPoints[2] * Invert( nearPoints[2] ).GetW();
	nearPoints[3] = nearPoints[3] * Invert( nearPoints[3] ).GetW();

	farPoints[0] = farPoints[0] * Invert( farPoints[0] ).GetW();
	farPoints[1] = farPoints[1] * Invert( farPoints[1] ).GetW();
	farPoints[2] = farPoints[2] * Invert( farPoints[2] ).GetW();
	farPoints[3] = farPoints[3] * Invert( farPoints[3] ).GetW();

	Vec4V leftPlane		= BuildPlane( nearPoints[0].GetXYZ(), nearPoints[1].GetXYZ(), farPoints[0].GetXYZ() );
	Vec4V topPlane		= BuildPlane( nearPoints[1].GetXYZ(), nearPoints[2].GetXYZ(), farPoints[1].GetXYZ() );
	Vec4V rightPlane	= BuildPlane( nearPoints[2].GetXYZ(), nearPoints[3].GetXYZ(), farPoints[2].GetXYZ() );
	Vec4V bottomPlane	= BuildPlane( nearPoints[3].GetXYZ(), nearPoints[0].GetXYZ(), farPoints[3].GetXYZ() );
	Vec4V nearPlane		= BuildPlane( nearPoints[2].GetXYZ(), nearPoints[1].GetXYZ(), nearPoints[0].GetXYZ() );
	Vec4V farPlane		= BuildPlane( farPoints[0].GetXYZ(), farPoints[1].GetXYZ(), farPoints[2].GetXYZ() );

	if ( bFlipPlanes )
	{
		leftPlane = -leftPlane;
		topPlane = -topPlane;
		rightPlane = -rightPlane;
		bottomPlane = -bottomPlane;
		nearPlane = -nearPlane;
		farPlane = -farPlane;
	}
	const Vec4V	zeroPlane = Vec4V(V_ZERO);

	frustum.SetPlanes( leftPlane, rightPlane, bottomPlane, topPlane, nearPlane, farPlane, zeroPlane, zeroPlane );
}

#if !__SPU && DEBUG_DRAW
void fwScreenQuad::DebugDraw(const Color32& color) const
{
	Vec4V	normQuad = m_quad * Vec4V(V_HALF) + Vec4V(V_HALF);
	grcDebugDraw::Quad(
		Vector2( normQuad.GetXf(), 1 - normQuad.GetYf() ),
		Vector2( normQuad.GetZf(), 1 - normQuad.GetYf() ),
		Vector2( normQuad.GetZf(), 1 - normQuad.GetWf() ),
		Vector2( normQuad.GetXf(), 1 - normQuad.GetWf() ),
		color,
		false );
}
#else
void fwScreenQuad::DebugDraw(const Color32&) const { }
#endif

static fwScreenQuad ComputeUnion(const fwScreenQuad& a, const fwScreenQuad& b)
{
	fwScreenQuad	result = a;
	result.Grow( b );
	return result;
}

void fwScreenQuadPair::Grow(const fwScreenQuad& op)
{
	fwScreenQuad	quad0 = GetScreenQuad0();
	fwScreenQuad	quad1 = GetScreenQuad1();

	if ( ( quad0.IsValid() && quad0.Contains(op) ) ||
		( quad1.IsValid() && quad1.Contains(op) ) )
	{
		// do nothing
	}
	else if ( !quad0.IsValid() )
	{
		quad0 = op;
	}
	else if ( !quad1.IsValid() )
	{
		quad1 = op;
	}
	else // both q0 and q1 are valid, have to merge
	{
		/*
		choices are:
		a.
			q0 = union(q0, q2)
			q1 = q1
		b.
			q0 = q0
			q1 = union(q1, q2)
		c.
			q0 = union(q0, q1)
			q1 = q2
		*/

		const fwScreenQuad q0q1		= ComputeUnion( quad0, quad1 );
		const fwScreenQuad q0q2		= ComputeUnion( quad0, op );
		const fwScreenQuad q1q2		= ComputeUnion( quad1, op );
		const ScalarV area_q0		= quad0.ComputeArea();
		const ScalarV area_q1		= quad1.ComputeArea();
		const ScalarV area_q2		= op.ComputeArea();
		const ScalarV area_q0q1		= q0q1.ComputeArea();
		const ScalarV area_q0q2		= q0q2.ComputeArea();
		const ScalarV area_q1q2		= q1q2.ComputeArea();
		const ScalarV sum_a			= area_q0q2 + area_q1;
		const ScalarV sum_b			= area_q1q2 + area_q0;
		const ScalarV sum_c			= area_q0q1 + area_q2;
		const ScalarV sum_min		= Min( Min( sum_a, sum_b ), sum_c );

		if ( IsEqualAll( sum_a, sum_min ) )
		{
			quad0.Grow( op );
		}
		else if ( IsEqualAll( sum_b, sum_min ) )
		{
			quad1.Grow( op );
		}
		else
		{
			quad0.Grow( quad1 );
			quad1 = op;
		}
	}

	quad0.Clamp();
	quad1.Clamp();

	SetScreenQuad0( quad0 );
	SetScreenQuad1( quad1 );
}

fwScreenQuad fwScreenQuadPair::Union() const
{
	fwScreenQuad	result = GetScreenQuad0();
	result.Grow( GetScreenQuad1() );
	return result;
}

#if !__SPU && DEBUG_DRAW
void fwScreenQuadPair::DebugDraw(const Color32& color0, const Color32& color1) const
{
	const fwScreenQuad	quad0 = GetScreenQuad0();
	const fwScreenQuad	quad1 = GetScreenQuad1();

	if ( quad0.IsValid() )
		quad0.DebugDraw( color0 );

	if ( quad1.IsValid() )
		quad1.DebugDraw( color1 );
}
#else
void fwScreenQuadPair::DebugDraw(const Color32&, const Color32&) const { }
#endif

} // namespace rage
