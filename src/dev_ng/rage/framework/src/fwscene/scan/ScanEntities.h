#ifndef _INC_SCANENTITIES_H_
#define _INC_SCANENTITIES_H_

// framework headers
#include "fwtl/StepAllocator.h"
#include "fwscene/world/EntityDesc.h"
#include "fwscene/scan/VisibilityFlags.h"
#include "fwscene/scan/RenderPhaseCullShape.h"
#include "fwscene/scan/ScanResults.h"
#include "fwscene/scan/ScanCascadeShadows.h"
#include "fwscene/scan/ScreenQuad.h"
#include "fwscene/scan/ScanPerformance.h"

// rage headers
#include "system/eaptr.h"
#include "spatialdata/aabb.h"
#include "softrasterizer/scan.h"
#include "softrasterizer/tiler.h"
#if __SPU
	#include "softrasterizer/tiler.cpp"
#endif
#include "vectormath/vecboolv.h"

#define SCAN_STATS			( __BANK)
#if SCAN_STATS
#define SCAN_STATS_ONLY(x)	x
#else
#define SCAN_STATS_ONLY(x)
#endif

#define SCAN_ENTITIES_OPTIMIZE_OFF (0 && __PPU)

#if __DEV
#define SCAN_ENTITIES_FORCE_INLINE
#else
#define SCAN_ENTITIES_FORCE_INLINE __forceinline 
#endif //SCAN_ENTITIES_OPTIMIZE_OFF

namespace rage {

enum
{
	SCAN_RENDER_PHASE_COUNT		= 24,
	SCAN_MAX_ENTITIES_PER_JOB	= 320,		// Number of entity descriptors (48 bytes) that fit in a single
											// SPU input buffer (16k)
	SCAN_NUM_PVS_ENTRIES_PER_RESULT_BLOCK = (SCAN_MAX_ENTITIES_PER_JOB / 10), 
	SCAN_MAX_TRACE_ENTITIES		= 32,
};

enum
{
	SCAN_OCCLUSION_STORAGE_PRIMARY,
	SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS,
	SCAN_OCCLUSION_STORAGE_COUNT,

	SCAN_OCCLUSION_STORAGE_WATER_REFLECTION = SCAN_OCCLUSION_STORAGE_PRIMARY, // shares with primary occlusion storage
};

enum
{
	// the first transforms must match the storage enum
	SCAN_OCCLUSION_TRANSFORM_PRIMARY			= SCAN_OCCLUSION_STORAGE_PRIMARY,
	SCAN_OCCLUSION_TRANSFORM_CASCADE_SHADOWS	= SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS,

	// .. then we can have extra transforms
	SCAN_OCCLUSION_TRANSFORM_WATER_REFLECTION,
	SCAN_OCCLUSION_TRANSFORM_COUNT
};

enum
{
	SCAN_SCREENQUAD_STORAGE_PRIMARY,
	SCAN_SCREENQUAD_STORAGE_CASCADE_SHADOWS,
	SCAN_SCREENQUAD_STORAGE_STREAMING,
	SCAN_SCREENQUAD_STORAGE_MIRROR,
	SCAN_SCREENQUAD_STORAGE_COUNT
};

enum
{
	SCAN_ENTITIES_FLAG_INTERIOR						= 1 << 0,
	SCAN_ENTITIES_RENDER_LODS_ONLY					= 1 << 1,
	SCAN_ENTITIES_DONT_RUN_ON_PPU					= 1 << 2,
	SCAN_ENTITIES_FLAG_ENTITY_DESCS_SOA				= 1 << 3,
	SCAN_ENTITIES_FLAG_STRADDLING_PORTALS_CONTAINER = 1 << 4,
	SCAN_ENTITIES_FLAG_PORTAL_TO_EXTERIOR			= 1 << 5

};

enum
{
	_GTA5_VIS_ENTITY_PED			= 1,
	_GTA5_VIS_ENTITY_OBJECT			= 2,
	_GTA5_VIS_ENTITY_VEHICLE		= 4,
	_GTA5_VIS_ENTITY_LOLOD_BUILDING	= 6,
	_GTA5_VIS_ENTITY_HILOD_BUILDING = 7,
};

enum
{
	_GTA5_ENTITY_TYPE_VEHICLE		= 3,
	_GTA5_ENTITY_TYPE_PED			= 4,
	_GTA5_ENTITY_TYPE_OBJECT		= 5,
	_GTA5_ENTITY_TYPE_DUMMY_OBJECT	= 6,
	_GTA5_ENTITY_TYPE_LIGHT			= 11,
	_GTA5_ENTITY_TYPE_GRASS_BATCH	= 14,
};

struct fwTestResults
{
	Vec4V	m_slots[4];

	fwTestResults() {}
	
	explicit fwTestResults(eZEROInitializer)
	{
		m_slots[0] = Vec4V(V_FALSE);
		m_slots[1] = Vec4V(V_FALSE);
		m_slots[2] = Vec4V(V_FALSE);
		m_slots[3] = Vec4V(V_FALSE);
	}

	explicit fwTestResults(eMASKXYZWInitializer)
	{
		m_slots[0] = Vec4V(V_TRUE);
		m_slots[1] = Vec4V(V_TRUE);
		m_slots[2] = Vec4V(V_TRUE);
		m_slots[3] = Vec4V(V_TRUE);
	}
};

typedef fwTestResults				fwTestResults_Val;
typedef fwTestResults*				fwTestResults_Ptr;
typedef fwTestResults&				fwTestResults_Ref;
typedef const fwTestResults&		fwTestResults_ConstRef;

typedef fwTestResults_ConstRef		fwTestResults_In;
typedef const fwTestResults_Val		fwTestResults_Out;
typedef fwTestResults_Ref			fwTestResults_InOut;

struct fwScanBaseInfo
{
	Mat44V	m_transform[ SCAN_OCCLUSION_TRANSFORM_COUNT ];
	Vec4V	m_minMaxBounds[SCAN_OCCLUSION_TRANSFORM_COUNT];
	ScalarV	m_minZ[SCAN_OCCLUSION_TRANSFORM_COUNT];
	
	
	fwRenderPhaseCullShape		m_cullShapes[ SCAN_RENDER_PHASE_COUNT ];
	fwScanCascadeShadowInfo		m_cascadeShadowInfo;

	float						m_depthRescale[SCAN_OCCLUSION_TRANSFORM_COUNT ];
	float						m_rasterizerDepthTestBias[ SCAN_OCCLUSION_TRANSFORM_COUNT ];
	u16							m_cullShapeCount;
	u16							m_pad : 16;

	float						m_receiverShadowHeightMin;
	float						m_rendererPreStreamDistance;
	float						m_fadeDist;

	fwPvsEntry*					m_pvsStartingPtr;
	fwPvsEntry* volatile*		m_pvsCurrentPtr;
	u32							m_pvsMaxSize;

	fwScanResultBlock*			m_ResultBlocks;		// After scanning, this array will contain information about the results
	unsigned int volatile*		m_NextResultBlock;	// Next index in m_ResultBlocks to write data to
	unsigned int volatile*		m_ResultBlockCount;	// Actual number of result blocks that have been completed
	unsigned int				m_MaxResultBlocks;	// Number of elements in m_ResultBlocks

	float						m_fCullDistMin;
	float						m_fCullDistMax;

	float						m_useExactSelectionSize;

	union
	{
		rage::Vec::Vector_4V	m_vPerLodLevelScales[2];
		float					m_perLodLevelScales[ LODTYPES_DEPTH_TOTAL ];
	};
	float						m_fadeDistances [ LODTYPES_DEPTH_TOTAL ];
	u32							m_lodSortVals[ LODTYPES_DEPTH_TOTAL ];
	float						m_defaultLodScale;
	float						m_grassBatchLodScale;

	u32							m_lodFilterMask;
	u32							m_streamingVolumeMask;
	spdTransposedPlaneSet4*	    m_reflectionPlaneSets;
#if __BANK
	void*						m_traceEntityAddress;
	void*						m_traceEntityAddressList[ SCAN_MAX_TRACE_ENTITIES ];
	int							m_traceEntityAddressCount;
	ScalarV						m_newOcclussionAABBExactPixelsThresholdSimple;

	u32*						m_OccludedCount;
	u32*						m_OccludedTestedCount;
	u32*						m_TrivialAcceptActiveFrustumCount;
	u32*						m_TrivialAcceptMinZCount;
	u32*						m_TrivialAcceptVisiblePixelCount;
	
	u8							m_entitySelectRenderPhaseId;
	u8							m_useSimpleQueries:1;
	u8							m_useNewOcclusionExact:1;
	u8							m_useTrivialAcceptTest:1;
	u8							m_useTrivialAcceptVisiblePixelTest:1;
	u8							m_useForceExactOcclusionTest:1;
	u8							m_useProjectedShadowOcclusion:1;
	u8							m_useProjectedShadowOcclusionClipping:1;
	u8							m_useMinZClipping:1;

	fwScreenQuad				m_entitySelectQuad;
	
#endif // __BANK
	enum { MAX_CLOSED_INTERIORS = 16 };
	atFixedArray<u16, MAX_CLOSED_INTERIORS>	m_closedInteriors; // size MAX_CLOSED_INTERIORS * sizeof(u16) + sizeof(int).  Note int is the count.
};

class fwScanEntities
{
private:

	const fwScanBaseInfo*		m_scanBaseInfo;

	const fwRenderPhaseCullShape*	m_cullShapes[ SCAN_RENDER_PHASE_COUNT ];
	u32							m_cullShapeCount;

	u32							m_entityDescCount;
	fwEntityDesc*				m_entityDescs;

	void*						m_scratchMemory;
	fwStepAllocator				m_scratchAllocator;

	Vec4V*						m_cameraDistances;

	fwPvsEntry*					m_pvsEntryBuffer;
	u32							m_pvsEntryBufferSize;
	u32							m_pvsEntryCount;

	Vec3V						m_primaryCameraPosition;

	const fwRenderPhaseCullShape*	m_gbufCullShape;
	const fwRenderPhaseCullShape*	m_cascadeShadowCullShape;

	const fwScanResults*		m_scanResults;

	bool						m_isFrustumValid[ SCAN_SCREENQUAD_STORAGE_COUNT ][2];
	Vec4V						m_screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_COUNT ];
	spdTransposedPlaneSet8		m_restrictedFrusta[ SCAN_SCREENQUAD_STORAGE_COUNT ][2];
	spdTransposedPlaneSet8		m_visibleRoomsShadowFrustum;

	u32							m_controlFlags;
	u32							m_nodeVisTags;
	const Vec4V*				m_hiZBuffer[ SCAN_OCCLUSION_STORAGE_COUNT ];
	spdTransposedPlaneSet4*     m_reflectionPlaneSets;

#if __BANK
	spdTransposedPlaneSet8		m_entitySelectFrustum;
#endif

	fwScanLocalTimer			m_earlyRejectLocalTimer;
	fwScanLocalTimer			m_clippingLocalTimer;
	fwScanLocalTimer			m_occlusionLocalTimer;

	enum
	{
		FLAG_ISVISIBLE_RESULT_VISIBLE		= 1 << 0,
		FLAG_ISVISIBLE_RESULT_OCCLUDED		= 1 << 1
	};
		
	void ResetScanEntries();
	SCAN_ENTITIES_FORCE_INLINE void AddPvsEntry(const fwEntityDesc& entityDesc, const float fDistance, fwVisibilityFlags& visFlags);
	void SubmitScanEntries();
#if __BANK
	SCAN_ENTITIES_FORCE_INLINE bool TestOcclusion(const spdAABB& aabb, Vec3V_In cameraPos, const int occluderPhase, const int occlusionTransform, u32 entityType);
#else
	SCAN_ENTITIES_FORCE_INLINE bool TestOcclusion(const spdAABB& aabb, Vec3V_In cameraPos, const int occluderPhase, const int occlusionTransform);
#endif
	bool  TestProjectedOcclusion(const spdAABB& aabb, Vec3V_In cameraPos);

	SCAN_ENTITIES_FORCE_INLINE float CalcAlphaFade(const fwEntityDesc& entityDesc, const float fDistance);

	template <bool validCameraDistance> fwTestResults_Out AreEntitiesEarlyRejected(const fwEntityDescSoA& soa, Vec3V_In cameraPos, Vec4V* cameraDistance, bool isPrimary);
//	fwTestResults_Out AreEntitiesClippedDefault(const fwRenderPhaseCullShape& cullShape, const fwEntityDescSoA& soa, fwTestResults_In excluded);
	fwTestResults_Out AreEntitiesClippedPrimary(const fwRenderPhaseCullShape& cullShape, const fwEntityDescSoA& soa, fwTestResults_In excluded);

	bool IsEntityEarlyRejected(const fwEntityDesc& entityDesc, Vec3V_In cameraPosition, float *optCameraDist, bool isPrimary);
	bool IsEntityExcludedInRenderPhase(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc);
	bool IsEntityExcludedByLodRange(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc);
	bool IsEntityOccluded(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc);
	bool IsEntityClippedDefault(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc);
	bool IsEntityClippedScreenQuad(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, int storageId);
	bool IsEntityClippedShadows(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags);
	bool IsEntityClippedReflections(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags);
	bool IsEntityClipped(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags);
	SCAN_ENTITIES_FORCE_INLINE s32 IsEntityVisible(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags);
	SCAN_ENTITIES_FORCE_INLINE s32 IsEntityVisibleNoClipping(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags);

	void ProcessEntityDescs();

	void ProcessSingleEntityDescSoa(const int block, const int offset, const fwEntityDesc& entityDesc, VecBoolV_In resultOrMask, fwTestResults_In defaultRejected, fwTestResults** phaseRejected, fwTestResults_In primaryClipped);
	void ProcessEntityDescsSoA();
	#if __BANK
		bool ScanDebugDontEarlyReject(fwEntityDesc &entity);
	#endif
public:
	
	enum {
		SCRATCH_BUFFER_SIZE		= 16 * 1024
	};

	void SetScanBaseInfo(const fwScanBaseInfo* value)						{ m_scanBaseInfo = value; }
	void SetEntityDescCount(const u32 value)								{ m_entityDescCount = value; }
	void SetEntityDescs(fwEntityDesc* value)								{ m_entityDescs = value; }
	void SetScratchMemory(void* value)										{ m_scratchMemory = value; }
	void SetScanResults(const fwScanResults* value)							{ m_scanResults = value; }
	void SetScreenQuadStorage(const int portalPhase, Vec4V_In value)		{ m_screenQuadStorage[ portalPhase ] = value; }
	void SetControlFlags(const u32 value)									{ m_controlFlags = value; }
	void SetNodeVisTags(const u32 value)									{ m_nodeVisTags = value; }
	void SetHiZBuffer(const int occluderPhase, const Vec4V* hiZ)			{ m_hiZBuffer[ occluderPhase ] = hiZ; }
	void SetReflectionPlaneSets(spdTransposedPlaneSet4 *pPlanes)			{ m_reflectionPlaneSets = pPlanes; }

	static void CalculateProjectOcclusionOBB(const fwScanCascadeShadowInfo& cascadeShadowInfo, const spdAABB& aabb, Mat33V_In shadowToWorldMtx, ScalarV_In groundLevel, Vec3V_InOut obbMin_Out, Vec3V_InOut obbMax_Out, Mat34V_InOut OBBToWorldMtx_Out BANK_ONLY(, bool bUseProjectedShadowOcclusionClipping));
	static bool RunFromDependency(const sysDependency& dependency);
	bool Run();
};

CompileTimeAssert((sizeof(fwScanBaseInfo) & 15) == 0);

} // namespace rage

#endif // !defined _INC_SCANENTITIES_H_
