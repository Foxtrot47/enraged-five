rage::u32	g_auxDmaTag = 0;

#include "system/dma.h"
#include <cell/spurs.h>
#include "system/dependency.h"
#include "fwscene/scan/ScanNodes.cpp"
#include "fwscene/scan/ScreenQuad.cpp"
#include "fwscene/scan/ScanDebug.cpp"
#include "fwscene/world/SceneGraph.cpp"
#include "fwscene/world/SceneGraphNode.cpp"
#include "fwscene/world/InteriorLocation.cpp"
#include "fwscene/world/SceneGraphVisitor.cpp"
#include "fwmaths/PortalCorners.cpp"
#include "fwmaths/vectorutil.cpp"
#include "system/dependencyscheduler.cpp"

using rage::sysDependency;

SPUFRAG_DECL(bool, ScanNodes, sysDependency&);
SPUFRAG_IMPL(bool, ScanNodes, sysDependency& dependency) {
	return fwScanNodes::RunFromDependency( dependency );
}
