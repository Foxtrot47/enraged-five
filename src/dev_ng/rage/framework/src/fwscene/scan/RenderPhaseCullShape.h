#ifndef _INC_RENDERPHASECULLSHAPE_H_
#define _INC_RENDERPHASECULLSHAPE_H_

#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"
#include "spatialdata/transposedplaneset.h"

#include "fwutil/Flags.h"
#include "fwrenderer/RenderSettingsBase.h"
#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/lod/LodTypes.h"

namespace rage {

enum fwCullShapeFlags
{
	CULL_SHAPE_FLAG_GEOMETRY_FRUSTUM							= 1 << 0,
	CULL_SHAPE_FLAG_GEOMETRY_SPHERE								= 1 << 1,
	CULL_SHAPE_FLAG_PRIMARY										= 1 << 2,
	CULL_SHAPE_FLAG_RENDER_EXTERIOR								= 1 << 3,
	CULL_SHAPE_FLAG_RENDER_INTERIOR								= 1 << 4,
	CULL_SHAPE_FLAG_RENDER_ONLY_IF_PRIMARY_RENDERS				= 1 << 5,
	CULL_SHAPE_FLAG_RENDER_LOD_BUILDING_BASED_ON_INTERIOR		= 1 << 6,
	CULL_SHAPE_FLAG_TRAVERSE_PORTALS							= 1 << 7,
	CULL_SHAPE_FLAG_TRAVERSE_DISABLED_PORTALS					= 1 << 8,
	CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS						= 1 << 9,
	CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS						= 1 << 10,
	CULL_SHAPE_FLAG_USE_SEPARATE_INTERIOR_SPHERE_GEOMETRY		= 1 << 11,
	CULL_SHAPE_FLAG_USE_LOD_RANGES_IN_EXTERIOR					= 1 << 12,
	CULL_SHAPE_FLAG_USE_LOD_RANGES_IN_INTERIOR					= 1 << 13,
	CULL_SHAPE_FLAG_USE_MAX_PED_VEH_OBJ_DISTANCES				= 1 << 14,
	CULL_SHAPE_FLAG_ENABLE_OCCLUSION							= 1 << 15,
	CULL_SHAPE_FLAG_ENABLE_WATER_OCCLUSION						= 1 << 16,
	CULL_SHAPE_FLAG_CASCADE_SHADOWS								= 1 << 17,
	CULL_SHAPE_FLAG_REFLECTIONS									= 1 << 18,
	CULL_SHAPE_FLAG_MIRROR										= 1 << 19,
	CULL_SHAPE_FLAG_WATER_REFLECTION							= 1 << 20,
	CULL_SHAPE_FLAG_WATER_REFLECTION_USE_SEPARATE_SPHERE		= 1 << 21,
	CULL_SHAPE_FLAG_WATER_REFLECTION_PROXIES_ONLY				= 1 << 22,
	CULL_SHAPE_FLAG_STREAM_ENTITIES								= 1 << 23,
	CULL_SHAPE_FLAG_ONLY_STREAM_ENTITIES						= 1 << 24,
	CULL_SHAPE_FLAG_RENDER_ONLY_ROOT_LOD						= 1 << 25,
	CULL_SHAPE_FLAG_HEIGHT_MAP									= 1 << 26,
	CULL_SHAPE_FLAG_IGNORE_VERTICAL_MIRRORS						= 1 << 27,
	CULL_SHAPE_FLAG_IGNORE_HORIZONTAL_MIRRORS					= 1 << 28,
};

class fwRenderPhaseBase;

class fwRenderPhaseCullShape
{
private:

	enum
	{
		GEOMETRY_FRUSTUM,
		GEOMETRY_SPHERE
	};

	spdTransposedPlaneSet8	m_frustum;
	spdSphere				m_sphere;
	spdSphere				m_interiorSphere;
	Mat44V					m_compositeMatrix;
	Vec3V					m_cameraPosition;
	Vec4V					m_mirrorPlane;
	u32						m_renderPhaseId;
	fwSceneGraphNode*		m_startingSceneNode;
	fwFlags16				m_entityVisibilityMask;
	u16						m_visibilityType;
	u32						m_flags;
	s16						m_occlusionStorageId;
	s16						m_screenQuadStorageId;
	float					m_lodRanges[ LODTYPES_DEPTH_TOTAL ][2];
	float					m_maxPedDistanceSqr;
	float					m_maxVehicleDistanceSqr;
	float					m_maxObjectDistanceSqr;
	u8						m_lodMask;

public:

	void FixupPointers(const fwPointerFixer& fixer)					{ fixer.Fix( m_startingSceneNode ); }

	void SetCompositeMatrix(Mat44V_In value)						{ m_compositeMatrix = value; }
	Mat44V_Out GetCompositeMatrix() const							{ return m_compositeMatrix; }

	void SetCameraPosition(Vec3V_In value)							{ m_cameraPosition = value; }
	Vec3V_Out GetCameraPosition() const								{ return m_cameraPosition; }

	void SetMirrorPlane(Vec4V_In value)								{ m_mirrorPlane = value; }
	Vec4V_Out GetMirrorPlane() const								{ return m_mirrorPlane; }

	void SetRenderPhaseId(const u32 value)							{ m_renderPhaseId = value; }
	u32 GetRenderPhaseId() const									{ return m_renderPhaseId; }

	void SetOcclusionStorageId(const s16 value)						{ m_occlusionStorageId = value; }
	s16 GetOcclusionStorageId() const								{ return m_occlusionStorageId; }

	void SetScreenQuadStorageId(const s16 value)					{ m_screenQuadStorageId = value; }
	s16 GetScreenQuadStorageId() const								{ return m_screenQuadStorageId; }

	void InvalidateLodRanges();
	void EnableLodRange(const int level, const float min, const float max);
	void EnableLodRange(const int level)							{ EnableLodRange( level, 0, 1e6 ); }
	void GetLodRange(const int level, float* min, float* max) const;

	void SetMaxPedDistance(float distance)							{ m_maxPedDistanceSqr = distance*distance; }
	float GetMaxPedDistanceSqr() const								{ return m_maxPedDistanceSqr; }

	void SetMaxVehicleDistance(float distance)						{ m_maxVehicleDistanceSqr = distance*distance; }
	float GetMaxVehicleDistanceSqr() const							{ return m_maxVehicleDistanceSqr; }

	void SetMaxObjectDistance(float distance)						{ m_maxObjectDistanceSqr = distance*distance; }
	float GetMaxObjectDistanceSqr() const							{ return m_maxObjectDistanceSqr; }

	void SetStartingSceneNode(fwSceneGraphNode* value)				{ m_startingSceneNode = value; }
	fwSceneGraphNode* GetStartingSceneNode() const					{ return m_startingSceneNode; }

	void SetEntityVisibilityMask(const fwFlags16& value)			{ m_entityVisibilityMask = value; }
	const fwFlags16& GetEntityVisibilityMask() const				{ return m_entityVisibilityMask; }
	fwFlags16& GetEntityVisibilityMask()							{ return m_entityVisibilityMask; }

	void SetVisibilityType(const u16 value)							{ m_visibilityType = value; }
	u16 GetVisibilityType() const									{ return m_visibilityType; }

	void SetFlags(const u32 value)									{ m_flags |= value; }
	void ClearFlags(const u32 value)								{ m_flags &= ~value; }
	void ClearAllFlags()											{ m_flags = 0; }
	u32 GetFlags() const											{ return m_flags; }

	void SetFrustum(const spdTransposedPlaneSet8& value)			{ FastAssert( GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_FRUSTUM ); m_frustum = value; }
	const spdTransposedPlaneSet8& GetFrustum() const				{ FastAssert( GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_FRUSTUM ); return m_frustum; }
	spdTransposedPlaneSet8& GetFrustum()							{ FastAssert( GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_FRUSTUM ); return m_frustum; }

	void SetSphere(const spdSphere& value)							{ FastAssert( GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_SPHERE ); m_sphere = value; }
	const spdSphere& GetSphere() const								{ FastAssert( GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_SPHERE ); return m_sphere; }
	spdSphere& GetSphere()											{ FastAssert( GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_SPHERE ); return m_sphere; }

	void SetInteriorSphere(const spdSphere& value)					{ m_interiorSphere = value; }
	const spdSphere& GetInteriorSphere() const						{ return m_interiorSphere; }
	spdSphere& GetInteriorSphere()									{ return m_interiorSphere; }

	void SetLodMask(u8 lodMask)										{ m_lodMask = lodMask; }
	u8	 GetLodMask() const											{ return m_lodMask; }



	bool IsClipped(const spdAABB& box) const
	{
		bool	inside = false;

		if ( m_flags & CULL_SHAPE_FLAG_GEOMETRY_FRUSTUM )
			inside = inside || m_frustum.IntersectsAABB( box );

		if ( m_flags & CULL_SHAPE_FLAG_GEOMETRY_SPHERE )
			inside = inside || box.IntersectsSphere( m_sphere );

		return !inside;
	}
};

CompileTimeAssertSize( fwRenderPhaseCullShape, 352, 368 );

inline void fwRenderPhaseCullShape::InvalidateLodRanges()
{
	for (int i = 0; i < LODTYPES_DEPTH_TOTAL; ++i)
		m_lodRanges[i][0] = m_lodRanges[i][1] = -1;
}

inline void fwRenderPhaseCullShape::EnableLodRange(const int level, const float min, const float max)
{
	m_lodRanges[level][0] = min;
	m_lodRanges[level][1] = max;
}

inline void fwRenderPhaseCullShape::GetLodRange(const int level, float* min, float* max) const
{
	*min = m_lodRanges[level][0];
	*max = m_lodRanges[level][1];
}

} // namespace rage

#endif // _INC_RENDERPHASECULLSHAPE_H_
